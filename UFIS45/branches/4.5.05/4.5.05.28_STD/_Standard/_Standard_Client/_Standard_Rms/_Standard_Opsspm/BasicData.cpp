// basicdat.cpp CCSBasicData class for providing general used methods


#include <stdafx.h>
#include <resource.h>
#include <CCSObj.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaShiftTypeData.h>
#include <BasicData.h>
#include <AllocData.h>
#include <CedaCfgData.h>
#include <CedaRnkData.h>
#include <PrivList.h>
#include <CedaAloData.h>
#include <CedaSgrData.h>
#include <CedaSerData.h>
#include <CedaSgmData.h>
#include <CedaGatData.h>
#include <CedaPstData.h>
#include <CedaPolData.h>
#include <CedaCicData.h>
#include <CedaRpfData.h>
#include <CedaSpfData.h>
#include <CedaSpeData.h>
#include <CedaRpqData.h>
#include <CedaEmpData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <AllocData.h>
#include <CedaJobData.h>
#include <CedaJodData.h>
#include <CedaJtyData.h>
#include <CedaFlightData.h>
#include <CedaRudData.h>
#include <CedaRueData.h>
#include <CedaPfcData.h>
#include <CedaPerData.h>
#include <SelPoolDlg.h>
#include <CedaDrdData.h>
#include <CedaDelData.h>
#include <CedaAzaData.h>
#include <CedaAcrData.h>
#include <CedaShiftData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <dinfo.h>
#include <DebugInfoMsgBox.h>
#include <CedaCcaData.h>
#include <CedaSwgData.h>
#include <CedaDrgData.h>
#include <CedaWgpData.h>
#include <CedaPgpData.h>
#include <CedaDlgData.h>
#include <CedaCccData.h>
#include <CedaTplData.h>
#include <CedaRloData.h>
#include <TimePacket.h>
#include <VersionInfo.h>
#include <CedaReqData.h>
#include <DebugCountDlg.h>
#include <CedaSgrData.h>
#include <DlgSettings.h>
#include <iostream.h>
#include <fstream.h>
#include <CedaDpxData.h>

#define A1 "A1"
#define A2 "A2"

#define N_URNOS_AT_ONCE			100
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))

static int CompareDemandStartTime(const DEMANDDATA **e1, const DEMANDDATA **e2)
{
	int ilRc = (int)((**e1).Debe.GetTime() - (**e2).Debe.GetTime());
	if (ilRc == 0)
		ilRc = (int)((**e1).Urno - (**e2).Urno);
	return ilRc;
}

static int ByWeight(const MATCHDATA **pppMatchData1, const MATCHDATA **pppMatchData2);
static int ByWeight(const MATCHDATA **pppMatchData1, const MATCHDATA **pppMatchData2)
{
	return (int)((**pppMatchData1).Weight - (**pppMatchData2).Weight);
}

static int ByJobStartTime(const JOBDATA **pppJob1, const JOBDATA **pppJob2);
static int ByJobStartTime(const JOBDATA **pppJob1, const JOBDATA **pppJob2)
{
	return (int)((**pppJob1).Acfr.GetTime() - (**pppJob2).Acfr.GetTime());
}

extern CCSCedaCom ogCommHandler;
static void BasicDataCf(void *popInstance, int ipDDXType,  void *vpDataPointer, CString &ropInstanceName);

const CString CCSBasicData::TPL_APRON1 = "Apron 1"; //Singapore
const CString CCSBasicData::TPL_APRON2 = "Apron 2"; //Singapore

CCSBasicData::CCSBasicData(void)
{
	char pclTmpText[512];
	char pclKeyword[521];
	CString olConfigFileName = GetConfigFileName();

	omTimebandStart = TIMENULL;
	omTimebandEnd = TIMENULL;

	ogCCSDdx.Register(this, STAFFDIAGRAM_UPDATETIMEBAND, CString("BASICDATA"),CString("Update Time Band"), BasicDataCf);	// for updating the yellow lines


	pomLogFile = NULL;
	bmLoggingEnabled = false;
    GetPrivateProfileString(pcgAppName, "LOGFILE", "", pclTmpText, sizeof pclTmpText, olConfigFileName);
	if(strlen(pclTmpText) > 0)
	{
		CString olTmp;
		// turn backslash into double backslash so that CString::Format() works
		int ilLen = strlen(pclTmpText);
		for(int ilC = 0; ilC < ilLen; ilC++)
		{
			olTmp += pclTmpText[ilC];
			if(pclTmpText[ilC] == 92)
			{
				olTmp += 92;
			}
		}
		olTmp.TrimRight();
		int ilDot = olTmp.Find(".");
		if(ilDot != -1)
		{
			omLogFilePath.Format("%s%%s%s",olTmp.Left(ilDot),olTmp.Mid(ilDot));
		}
		else
		{
			omLogFilePath = olTmp + "%s";
		}
		bmLoggingEnabled = true;
		CheckLog();
	}

	Trace("Ceda.ini contents\n");
	Trace("[%s]\n",pcgAppName);

//	strcpy(pclKeyword,"MONITORCOUNT");
//	GetPrivateProfileString(pcgAppName, pclKeyword, "1",pclTmpText, sizeof pclTmpText, olConfigFileName);
//	imMonitorCount = atoi(pclTmpText);
//	Trace("%s=%s\n",pclKeyword,pclTmpText);

	strcpy(pclKeyword,"RESOLUTIONX");
	GetPrivateProfileString(pcgAppName, pclKeyword, "-1",pclTmpText, sizeof pclTmpText, olConfigFileName);
	imScreenResolutionX = atoi(pclTmpText);
	Trace("%s=%s\n",pclKeyword,pclTmpText);

	strcpy(pclKeyword,"EXTENDSHIFTOFFSET");
	GetPrivateProfileString(pcgAppName, pclKeyword, "-1",pclTmpText, sizeof pclTmpText, olConfigFileName);
	imExtendShiftOffset = atoi(pclTmpText);
	Trace("%s=%s\n",pclKeyword,pclTmpText);

//	strcpy(pclKeyword,"RESOLUTIONY");
//	GetPrivateProfileString(pcgAppName, pclKeyword, "768",pclTmpText, sizeof pclTmpText, olConfigFileName);
//	imScreenResolutionY = atoi(pclTmpText);
//	Trace("%s=%s\n",pclKeyword,pclTmpText);

	strcpy(pclKeyword,"DRUCKFORMULAR");
	GetPrivateProfileString(pcgAppName, pclKeyword, "A3",pcmPrintForm, sizeof pcmPrintForm, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pcmPrintForm);

	bmDeleteViews = FALSE;
	strcpy(pclKeyword,"DELETEVIEWS");
	GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmDeleteViews = TRUE;
	}

	bmDemandsOnly = FALSE;
	strcpy(pclKeyword,"DEMANDSONLY");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmDemandsOnly = TRUE;
	}

	bmTurnaroundBars = FALSE;
	strcpy(pclKeyword,"SHOWTURNAROUNDBARS");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO", pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmTurnaroundBars = TRUE;
	}

	omDepArrMarkerGate.Empty();
	strcpy(pclKeyword,"DISPLAY_DEPARRMARKER_GATE");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NONE",pclTmpText, sizeof pclTmpText, olConfigFileName);
	omDepArrMarkerGate = CString(pclTmpText);
	omDepArrMarkerGate.MakeUpper();

	omDepArrMarkerPst.Empty();
	strcpy(pclKeyword,"DISPLAY_DEPARRMARKER_POSITION");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NONE",pclTmpText, sizeof pclTmpText, olConfigFileName);
	omDepArrMarkerPst = CString(pclTmpText);
	omDepArrMarkerPst.MakeUpper();

	omDepArrMarkerStaff.Empty();
	strcpy(pclKeyword,"DISPLAY_DEPARRMARKER_STAFF");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NONE",pclTmpText, sizeof pclTmpText, olConfigFileName);
	omDepArrMarkerStaff = CString(pclTmpText);
	omDepArrMarkerStaff.MakeUpper();

	omDepArrMarkerEquipment.Empty();
	strcpy(pclKeyword,"DISPLAY_DEPARRMARKER_EQUIPMENT");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NONE",pclTmpText, sizeof pclTmpText, olConfigFileName);
	omDepArrMarkerEquipment = CString(pclTmpText);
	omDepArrMarkerEquipment.MakeUpper();

	bmShowMatchingEmpsByDefault = false;
	strcpy(pclKeyword,"SHOW_MATCHING_EMPS_DEFAULT");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO", pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmShowMatchingEmpsByDefault = true;
	}

	bmSwapShiftEnabled = false;
	strcpy(pclKeyword,"SWAP_SHIFT_ENABLED");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO", pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmSwapShiftEnabled = true;
	}

	bmIgnorePausesByDefault = false;
	strcpy(pclKeyword,"IGNORE_PAUSES_DEFAULT");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO", pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmIgnorePausesByDefault = true;
	}

	bmIgnoreOverlapByDefault = false;
	strcpy(pclKeyword,"IGNORE_OVERLAP_DEFAULT");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO", pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmIgnoreOverlapByDefault = true;
	}

	bmAssignErrorDlgShowAllDemands = false;
	strcpy(pclKeyword,"ASSIGN_ERROR_SHOW_ALL_DEMANDS");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO", pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmAssignErrorDlgShowAllDemands = true;
	}

	bmIgnoreOutsideShiftByDefault = false;
	strcpy(pclKeyword,"IGNORE_OUTSIDESHIFT_DEFAULT");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO", pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmIgnoreOutsideShiftByDefault = true;
	}

	bmAvailEmpsMultiAssign = false;
	strcpy(pclKeyword,"AVAILEMPS_MULTIASSIGN");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO", pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmAvailEmpsMultiAssign = true;
	}

	bmAvailEquMultiAssign = false;
	strcpy(pclKeyword,"AVAILEQU_MULTIASSIGN");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO", pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmAvailEquMultiAssign = true;
	}

	omDemandsToLoad.Empty();
	strcpy(pclKeyword,"LOAD_DEMAND_TYPES");
    GetPrivateProfileString(pcgAppName, pclKeyword, "ALL", pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(stricmp(pclTmpText,"ALL"))
	{
		omDemandsToLoad = CString(",") + CString(pclTmpText) + CString(",");
	}

//	bmShowShadowBars = FALSE;
//	strcpy(pclKeyword,"SHOWSHADOWBARS");
//	GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
//	Trace("%s=%s\n",pclKeyword,pclTmpText);
//	if(!stricmp(pclTmpText,"YES"))
//	{
//		bmShowShadowBars = TRUE;
//	}

	bmVerticalScaling = FALSE;
	strcpy(pclKeyword,"VERTICALSCALING");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmVerticalScaling = TRUE;
	}

	bmOfflineEnabled = FALSE;
	strcpy(pclKeyword,"OFFLINEENABLED");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmOfflineEnabled = TRUE;
	}

	imNumberOfUndoRedoActions = 0;
	strcpy(pclKeyword,"UNDOSTEPS");
    GetPrivateProfileString(pcgAppName, pclKeyword, "30",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	imNumberOfUndoRedoActions = atoi(pclTmpText);
	if(imNumberOfUndoRedoActions <= 0)
	{
		imNumberOfUndoRedoActions = 30;
	}

	int ilDisplayOffset = 0;
	strcpy(pclKeyword,"GANTTCHART_DISPLAY_OFFSET");
    GetPrivateProfileString(pcgAppName, pclKeyword, "0",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	ilDisplayOffset = atoi(pclTmpText);
	if(ilDisplayOffset > 10) ilDisplayOffset = 10;
	omDisplayOffset = CTimeSpan(0, ilDisplayOffset, 0, 0);

	imFreeBottomLines = 0;
	strcpy(pclKeyword,"FREEBOTTOMLINES");
    GetPrivateProfileString(pcgAppName, pclKeyword, "1",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	imFreeBottomLines = atoi(pclTmpText);
	if(imFreeBottomLines <= 0)
	{
		imFreeBottomLines = 1;
	}

	bmReassignOverlappingBreaks = false;
	strcpy(pclKeyword,"REASSIGN_OVERLAPPING_BREAKS");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmReassignOverlappingBreaks = true;
	}

	bmDisplayTeams = false;
	strcpy(pclKeyword,"DISPLAY_GROUPALLOC");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmDisplayTeams = true;
	}

	bmIgnoreDeactivatedDemandConflicts = false;
	strcpy(pclKeyword,"IGNORE_DEACTIVATED_DEMAND_CONFLICTS");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmIgnoreDeactivatedDemandConflicts = true;
	}


	bmDisplayPrePlanning = false;
	strcpy(pclKeyword,"DISPLAY_PREPLANNING");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmDisplayPrePlanning = true;
	}

	bmDisplayGates = false;
	strcpy(pclKeyword,"DISPLAY_GATES");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmDisplayGates = true;
	}

	bmDisplayFlightJobsTable = false;
	strcpy(pclKeyword,"DISPLAY_FLIGHTJOBSLIST");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmDisplayFlightJobsTable = true;
	}

	bmDisplayTurnaroundGates = false;
	strcpy(pclKeyword,"DISPLAY_TURNAROUND_GATES");
    GetPrivateProfileString(pcgAppName, pclKeyword, "YES",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmDisplayTurnaroundGates = true;
	}

	//added by MAX
	myNewConflict = false;
	strcpy(pclKeyword,"EnableJobCoverageConflict");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		myNewConflict = true;
	}
	
	bmDisplayCheckins = false;
	strcpy(pclKeyword,"DISPLAY_CHECKINS");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmDisplayCheckins = true;
	}

	bmDisplayPrm = false;
	strcpy(pclKeyword,"DISPLAY_PRM");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmDisplayPrm = true;
	}
		
	GetPrivateProfileString(pcgAppName, "ISPRM",  "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	if (!strcmp(pclTmpText,"YES")) {
		bgIsPrm = true;
	}
	else {
 		bgIsPrm = false;
	}

  // lli: read the PRMUSEREMA setting from ceda.ini and set the flag accordingly
	GetPrivateProfileString(pcgAppName, "PRM-USEREMA",  "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	if (!strcmp(pclTmpText,"YES")) {
		bgPrmUseRema = true;
	}
	else {
 		bgPrmUseRema = false;
	}

  // lli: read the PRM-SHOWFLIGHTINSTATUS setting from ceda.ini and set the flag accordingly
	GetPrivateProfileString(pcgAppName, "PRM-SHOWFLIGHTINSTATUS",  "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	if (!strcmp(pclTmpText,"YES")) {
		bgPrmShowFlightNumber = true;
	}
	else {
 		bgPrmShowFlightNumber = false;
	}

  // lli: read the PRM-RalatedTemplates setting from ceda.ini and set the flag accordingly
	GetPrivateProfileString(pcgAppName, "PRM-RELATEDTEMPLATES",  "PRM",pclTmpText, sizeof pclTmpText, olConfigFileName);
	char *pclToken  = strtok(pclTmpText,",");
	while (pclToken)
	{
		omPRMRelatedTemplates.Add(CString(pclToken));
		pclToken = strtok(NULL,",");
	}

  // lli: read the PRMUSESTAT setting from ceda.ini and set the flag accordingly
	GetPrivateProfileString(pcgAppName, "PRM-USESTAT",  "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	if (!strcmp(pclTmpText,"YES")) {
		bgPrmUseStat = true;
	}
	else {
 		bgPrmUseStat = false;
	}


	bmDisplayRegistrations = false;
	strcpy(pclKeyword,"DISPLAY_REGISTRATIONS");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmDisplayRegistrations = true;
	}

	bmDisplayPositions = false;
	strcpy(pclKeyword,"DISPLAY_POSITIONS");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmDisplayPositions = true;
	}

	bmDisplayEquipment = false;
	strcpy(pclKeyword,"DISPLAY_EQUIPMENT");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmDisplayEquipment = true;
	}


	bmUseDemandSortValues = false;
	strcpy(pclKeyword,"USE_DEMAND_SORT_VALUES");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmUseDemandSortValues = true;
	}

	bmDisplayTurnaroundPositions = false;
	strcpy(pclKeyword,"DISPLAY_TURNAROUND_POSITIONS");
    GetPrivateProfileString(pcgAppName, pclKeyword, "YES",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmDisplayTurnaroundPositions = true;
	}

	bmDisplayDemands = false;
	strcpy(pclKeyword,"DISPLAY_DEMANDS");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmDisplayDemands = true;
	}

	bmAutomaticPauseAllocation = false;
	strcpy(pclKeyword,"AUTOMATIC_PAUSE_ALLOCATION");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmAutomaticPauseAllocation = true;
	}

	bmDisplayAssignConflictMessage = false;
	strcpy(pclKeyword,"DISPLAYASSIGNCONFLICTMESSAGE");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmDisplayAssignConflictMessage = true;
	}



	bmDelegatingFlight = false;
	bmDelegateFlights = false;
	strcpy(pclKeyword,"DELEGATEFLIGHTS");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO", pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmDelegateFlights = true;
	}


	omTablesNotToLoad.Empty();
	strcpy(pclKeyword,"TABLES_NOT_TO_LOAD");
    GetPrivateProfileString(pcgAppName, pclKeyword, "",pclTmpText, sizeof pclTmpText, olConfigFileName);
	omTablesNotToLoad = CString(pclTmpText);

	omTelexTypesToLoad.Empty();
	omTelexTypes.Empty();
	strcpy(pclKeyword,"WORKCARD_TELEX_TYPES");
    GetPrivateProfileString(pcgAppName, pclKeyword, "MVT,LDM,PSM",pclTmpText, sizeof pclTmpText, olConfigFileName);
	omTelexTypes = CString(pclTmpText);

	omTelexTypesPrintOnlyLast.Empty();
	strcpy(pclKeyword,"WORKCARD_LAST_TELEX");
    GetPrivateProfileString(pcgAppName, pclKeyword, "",pclTmpText, sizeof pclTmpText, olConfigFileName);
	omTelexTypesPrintOnlyLast = CString(pclTmpText);

	omFlightFtypsNotToLoad.Empty();
	strcpy(pclKeyword,"EXCLUDE_FLIGHT_TYPES");
    GetPrivateProfileString(pcgAppName, pclKeyword, "PNJ",pclTmpText, sizeof pclTmpText, olConfigFileName);
	omFlightFtypsNotToLoad = CString(pclTmpText);

	strcpy(pclKeyword,"SINGLE_BAR_LEN");
	GetPrivateProfileString(pcgAppName, pclKeyword, "0",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	imSingleBarLen = atoi(pclTmpText);

	// Alitalia Parameters (fpa)
	strcpy(pclKeyword,"PRIORITY_FOR_DEMAND");
	GetPrivateProfileString(pcgAppName, pclKeyword, "5000",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	omPriorityForDemand = pclTmpText;

	strcpy(pclKeyword,"STANDARD_LENGTH_DEMAND");
	GetPrivateProfileString(pcgAppName, pclKeyword, "10",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	imStandardLenghtDemand = atoi(pclTmpText);
	// End Alitalia Parameters

	bmPrintWorkingCard = false;
	strcpy(pclKeyword,"PRINT_WORKING_CARD");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	if(!stricmp(pclTmpText,"YES"))
	{
		bmPrintWorkingCard = true;
	}


	// the clocking on/off times in SPRTAB have no SDAY value, so a shift is liked to
	// a clocking on/off time if the clocking on/off time is within the shift +/- a tolerance value
	int ilTmp;
	omShiftStartTolerance = CTimeSpan( (15 * 60) ); // 15 minutes default
	strcpy(pclKeyword,"SHIFTSTARTTOLERANCE");
    GetPrivateProfileString(pcgAppName, pclKeyword, "15",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	ilTmp = atoi(pclTmpText);
	if(ilTmp > 0)
	{
		omShiftStartTolerance = CTimeSpan( (ilTmp * 60) );
	}
	omShiftEndTolerance = CTimeSpan( (15 * 60) ); // 15 minutes default
	strcpy(pclKeyword,"SHIFTENDTOLERANCE");
    GetPrivateProfileString(pcgAppName, pclKeyword, "15",pclTmpText, sizeof pclTmpText, olConfigFileName);
	Trace("%s=%s\n",pclKeyword,pclTmpText);
	ilTmp = atoi(pclTmpText);
	if(ilTmp > 0)
	{
		omShiftEndTolerance = CTimeSpan( (ilTmp * 60) );
	}



//	strcpy(pclKeyword,"NUM_DAYS_TO_LOAD_SHIFTS");
//    GetPrivateProfileString(pcgAppName, pclKeyword, "3",pclTmpText, sizeof pclTmpText, olConfigFileName);
//	Trace("%s=%s\n",pclKeyword,pclTmpText);
//	int ilDays = atoi(pclTmpText);
//	if(ilDays <= 0) ilDays = 3;
//	omNumDaysToLoadShifts = CTimeSpan(ilDays,0,0,0);

//	bmUseUtc = false; // default - display everything in local time
//	strcpy(pclKeyword,"USE_UTC_TIME");
//	GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
//	Trace("%s=%s\n",pclKeyword,pclTmpText);
//	if(!stricmp(pclTmpText,"YES"))
//	{
//		bmUseUtc = true;
//	}


	CString olTmpTime;
	CTime olLoadStartTime = TIMENULL;
	CTime olLoadEndTime = TIMENULL;
	bool blLoadDates = true;

	GetPrivateProfileString(pcgAppName, "DEBUG_DATES", "",pclTmpText, sizeof pclTmpText, olConfigFileName);
	if(strlen(pclTmpText) > 0)
	{
		ExtractItemList(pclTmpText, &omLoadTimeValues,',');
		if(omLoadTimeValues.GetSize() == 2)
		{
			// Convert CString (in format YYYYMMDDHHMMSS) to a CTime field
			CCSCedaData::StoreDate(omLoadTimeValues[0],&olLoadStartTime);
			CCSCedaData::StoreDate(omLoadTimeValues[1],&olLoadEndTime);
			if(olLoadStartTime != TIMENULL && olLoadEndTime != TIMENULL && olLoadEndTime > olLoadStartTime)
			{
				bmLoadRel = false;
				blLoadDates = false;
			}
		}
	}

	if(blLoadDates) // dont load the dates if a debug timeframe was defined
	{
		GetPrivateProfileString(pcgAppName, "LOADREL", "YES",pclTmpText, sizeof pclTmpText, olConfigFileName);
		bmLoadRel = (strcmp(pclTmpText,"YES") == 0) ? true : false;

		GetPrivateProfileString(pcgAppName, "LOADOFFSET", "0",pclTmpText, sizeof pclTmpText, olConfigFileName);
		int ilTimeoffset = atoi(pclTmpText);
		olLoadStartTime = ZeroHHMMSS(GetTime());
		olLoadStartTime += CTimeSpan(ilTimeoffset,0,0,0);

		olTmpTime = olLoadStartTime.Format("%Y%m%d%H%M%S");
		omLoadTimeValues.Add(olTmpTime);

		GetPrivateProfileString(pcgAppName, "LOADDURATION", "3", pclTmpText, sizeof pclTmpText, olConfigFileName);
		int ilTimeduration = atoi(pclTmpText);
		olLoadEndTime = olLoadStartTime + CTimeSpan(ilTimeduration,0,0,0);

		olTmpTime = olLoadEndTime.Format("%Y%m%d%H%M%S");
		omLoadTimeValues.Add(olTmpTime);
	}

	GetPrivateProfileString(pcgAppName, "LOADBEFORE", "2", pclTmpText, sizeof pclTmpText, olConfigFileName);
	int ilTimebef = atoi(pclTmpText);
	olTmpTime = pclTmpText;
	omLoadTimeValues.Add(olTmpTime);

	GetPrivateProfileString(pcgAppName, "LOADAFTER", "6", pclTmpText, sizeof pclTmpText, olConfigFileName);
	int ilTimeaft = atoi(pclTmpText);

	olTmpTime = pclTmpText;
	omLoadTimeValues.Add(olTmpTime);


	SetTimeframe(olLoadStartTime, olLoadEndTime);

//	if(bmLoadRel)
//	{
//		omTimeframeStart -= CTimeSpan(0,ilTimebef , 0, 0);
//		omTimeframeEnd = omTimeframeStart + CTimeSpan(0, ilTimeaft, 0, 0);
//	}
//	else
//	{
//		omTimeframeStart = olLoadStartTime;
//		omTimeframeEnd = olLoadEndTime;
//	}
//
	
	SetUtcDifference(); // only used to initialize imUtcDiffence

	ConfirmTime = 120;
	GetPrivateProfileString(pcgAppName, "JOB_CONFIRM_TIME", "120", pclTmpText, sizeof pclTmpText, olConfigFileName);
	if (sscanf(pclTmpText,"%d",&ConfirmTime) != 1)
	{
		ConfirmTime = 120;
	}

	WORD ilRed	= 0;
	WORD ilGreen= 0;
	WORD ilBlue	= 0;
	cmEmployeeInformedColor = RED;
	GetPrivateProfileString(pcgAppName, "EMPLOYEE_INFORMED", "(255,0,0),2", pclTmpText, sizeof pclTmpText, olConfigFileName);
	if (sscanf(pclTmpText,"(%u,%u,%u),%u",&ilRed,&ilGreen,&ilBlue,&wmEmployeeInformedThickness) == 4)
	{
		if (wmEmployeeInformedThickness > 3)
			wmEmployeeInformedThickness = 3;
		if (ilRed < 256 && ilGreen < 256 && ilBlue < 256)
			cmEmployeeInformedColor = RGB(ilRed,ilGreen,ilBlue);
	}
	imDefaultBreakBuffer = 0;
}


static void BasicDataCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	CCSBasicData *polDiagram = (CCSBasicData *)popInstance;

	if(ipDDXType == STAFFDIAGRAM_UPDATETIMEBAND)
	{
		TIMEPACKET *polTimePacket = (TIMEPACKET *)vpDataPointer;
		polDiagram->omTimebandStart = polTimePacket->StartTime;
		polDiagram->omTimebandEnd = polTimePacket->EndTime;
	}
}


int CCSBasicData::GetUtcDifference(void)
{
	return imUtcDifference;
}

void CCSBasicData::CreateListOfManagers(void)
{
	CCSPtrArray <ALLOCUNIT> olFunctions;
	ogAllocData.GetUnitsByGroup(ALLOCUNITTYPE_FUNCGROUP,ALLOCUNITTYPE_TEAMLEADERS,olFunctions);
	omManagerFunctionMap.RemoveAll();
	int ilNumFunctions = olFunctions.GetSize();
	for(int ilF = 0; ilF < ilNumFunctions; ilF++)
	{
		ALLOCUNIT *prlFunc = &olFunctions[ilF];
		if(strlen(prlFunc->Name) > 0)
		{
			omManagerFunctionMap.SetAt(prlFunc->Name, NULL);
		}
	}
}

bool CCSBasicData::IsManager(const char *pcpFunction)
{
	void *pvlDummy;
	return omManagerFunctionMap.Lookup(pcpFunction, (void *&) pvlDummy) ? true : false;
}

int CCSBasicData::SetUtcDifference(void)
{
	static int ilUtcDifference = 1;
	static BOOL blIsInitialized = FALSE;

	if (blIsInitialized == FALSE)
	{
		struct tm *_tm;
		time_t    now;
		
		int hour_gm,hour_local;
		blIsInitialized = TRUE;

		now = time(NULL);
		_tm = (struct tm *)gmtime(&now);
		hour_gm = _tm->tm_hour;
		_tm = (struct tm *)localtime(&now);
		hour_local = _tm->tm_hour;
		if (hour_gm > hour_local)
		{
			ilUtcDifference = ((hour_local+24-hour_gm)*3600);
		}
		else
		{
			ilUtcDifference = ((hour_local-hour_gm)*3600);
		}
	}

	imUtcDifference = ilUtcDifference;
	return ilUtcDifference;

}

void CCSBasicData::GetDebugData(void)
{
	bool blRc = true;
	char pclWhere[100], pclFields[100], pclTable[100];

	struct CountDataStruct
	{
		CString DrrCount;
		CString AftCount;
		CString JobCount;
		CString DemCount;

		CountDataStruct(void)
		{
			DrrCount = "0";
			AftCount = "0";
			JobCount = "0";
			DemCount = "0";
		}
	};

	CMapStringToPtr olDaysMap;
	typedef struct CountDataStruct COUNTDATA;
	COUNTDATA *prlCountData;

	// DRRTAB //////////////////////////////////////////////////////////////////////////////////////////////////////////
	sprintf(pclFields,"DISTINCT(SUBSTR(AVFR,1,8)),COUNT(SUBSTR(AVFR,1,8))");
	sprintf(pclWhere,"WHERE ROSL='3' AND ROSS<>'L' AND HOPO='%s' GROUP BY SUBSTR(AVFR,1,8)",pcgHomeAirport);
	sprintf(pclTable,"DRR%s",pcgTableExt);

	if((blRc = CedaAction("RT", pclTable, pclFields, pclWhere, "", "BUF1")) != true)
	{
		Trace("Error reading DRRTAB\nomLastErrorMessage = <%s>\n",pclWhere,omLastErrorMessage);
	}
	else
	{
		CString olLine;
		int ilNumRecs = GetBufferSize();
		for(int ilI = 0; ilI < ilNumRecs; ilI++)
		{
			if(GetBufferLine(ilI, olLine))
			{
				char pclData[100];
				memset(pclData,0,100);
				strncpy(pclData,olLine,99);

				char pclDate[100],pclCount[100];
				GetItem(1, pclData, pclDate);
				GetItem(2, pclData, pclCount);

				if(olDaysMap.Lookup(pclDate, (void *&) prlCountData))
				{
					prlCountData->DrrCount = pclCount;
				}
				else
				{
					prlCountData = new COUNTDATA;
					prlCountData->DrrCount = pclCount;
					olDaysMap.SetAt(pclDate, (void *&) prlCountData);
				}
			}
		}
		GetDataBuff()->RemoveAll();
	}

	// JOBTAB //////////////////////////////////////////////////////////////////////////////////////////////////////////
	sprintf(pclFields,"DISTINCT(SUBSTR(ACFR,1,8)),COUNT(SUBSTR(ACFR,1,8))");
	sprintf(pclWhere,"WHERE HOPO='%s' GROUP BY SUBSTR(ACFR,1,8)",pcgHomeAirport);
	sprintf(pclTable,"JOB%s",pcgTableExt);

	if((blRc = CedaAction("RT", pclTable, pclFields, pclWhere, "", "BUF1")) != true)
	{
		Trace("Error reading JOBTAB\nomLastErrorMessage = <%s>\n",pclWhere,omLastErrorMessage);
	}
	else
	{
		CString olLine;
		int ilNumRecs = GetBufferSize();
		for(int ilI = 0; ilI < ilNumRecs; ilI++)
		{
			if(GetBufferLine(ilI, olLine))
			{
				char pclData[100];
				memset(pclData,0,100);
				strncpy(pclData,olLine,99);

				char pclDate[100],pclCount[100];
				GetItem(1, pclData, pclDate);
				GetItem(2, pclData, pclCount);

				if(olDaysMap.Lookup(pclDate, (void *&) prlCountData))
				{
					prlCountData->JobCount = pclCount;
				}
				else
				{
					prlCountData = new COUNTDATA;
					prlCountData->JobCount = pclCount;
					olDaysMap.SetAt(pclDate, (void *&) prlCountData);
				}
			}
		}
		GetDataBuff()->RemoveAll();
	}

	// DEMTAB //////////////////////////////////////////////////////////////////////////////////////////////////////////
	sprintf(pclFields,"DISTINCT(SUBSTR(DEBE,1,8)),COUNT(SUBSTR(DEBE,1,8))");
	sprintf(pclWhere,"WHERE HOPO='%s' GROUP BY SUBSTR(DEBE,1,8)",pcgHomeAirport);
	sprintf(pclTable,"DEM%s",pcgTableExt);

	if((blRc = CedaAction("RT", pclTable, pclFields, pclWhere, "", "BUF1")) != true)
	{
		Trace("Error reading DEMTAB\nomLastErrorMessage = <%s>\n",pclWhere,omLastErrorMessage);
	}
	else
	{
		CString olLine;
		int ilNumRecs = GetBufferSize();
		for(int ilI = 0; ilI < ilNumRecs; ilI++)
		{
			if(GetBufferLine(ilI, olLine))
			{
				char pclData[100];
				memset(pclData,0,100);
				strncpy(pclData,olLine,99);

				char pclDate[100],pclCount[100];
				GetItem(1, pclData, pclDate);
				GetItem(2, pclData, pclCount);

				if(olDaysMap.Lookup(pclDate, (void *&) prlCountData))
				{
					prlCountData->DemCount = pclCount;
				}
				else
				{
					prlCountData = new COUNTDATA;
					prlCountData->DemCount = pclCount;
					olDaysMap.SetAt(pclDate, (void *&) prlCountData);
				}
			}
		}
		GetDataBuff()->RemoveAll();
	}

	CDebugCountDlg olDlg;
	CString olDate;
	POSITION rlPos;
	for(rlPos = olDaysMap.GetStartPosition(); rlPos != NULL;)
	{
		olDaysMap.GetNextAssoc(rlPos, olDate,(void *& ) prlCountData);
		olDlg.AddLine(olDate, prlCountData->DrrCount, prlCountData->JobCount, prlCountData->DemCount, prlCountData->AftCount);
	}
	olDlg.DoModal();

	// clean up
	for(rlPos = olDaysMap.GetStartPosition(); rlPos != NULL;)
	{
		olDaysMap.GetNextAssoc(rlPos, olDate,(void *& ) prlCountData);
		delete prlCountData;
	}
	olDaysMap.RemoveAll();
}


void CCSBasicData::SetUtcDifferenceFromApttab(void)
{
	bool blRc = false;
	char pclWhere[100], pclFields[100] = "TICH,TDI1,TDI2";
	char pclTable[100];
	sprintf(pclWhere,"WHERE APC3='%s'",pcgHomeAirport);
	sprintf(pclTable,"APT%s",pcgTableExt);

	if(!CedaAction("RT", pclTable, pclFields, pclWhere, "", "BUF1"))
	{
		Trace("Error reading UTC/local difference from APTTAB %s\nomLastErrorMessage = <%s>\n",pclWhere,omLastErrorMessage);
	}
	else
	{
		CTime olLocalTime = CTime::GetCurrentTime();
		CStringArray *polDataBuf = GetDataBuff();
		if(polDataBuf->GetSize() <= 0)
		{
			Trace("Error reading UTC/local difference from APTTAB %s\nDataBuff is empty\n",pclWhere);
		}
		else
		{
			CString olText;
			char pclData[100];
			memset(pclData,0,100);
			CString olData = polDataBuf->GetAt(0);
			strncpy(pclData,olData,99);

			// TICH == 31/3
			// TDI1 = -60   TDI2 = -120
			//
			// TICH == 31/10
			// TDI1 = -120   TDI2 = -60

			char pclTich[100],pclTdi1[100],pclTdi2[100];
			GetItem(1, pclData, pclTich);
			GetItem(2, pclData, pclTdi1);
			GetItem(3, pclData, pclTdi2);
			if(strlen(pclTich) > 0 && strlen(pclTdi1) > 0 && strlen(pclTdi2) > 0)
			{
				CString olLocalStr = olLocalTime.Format("%Y%m%d%H%M");
				imUtcDifference = (((strncmp(olLocalStr,pclTich,12)<0) ? atoi(pclTdi1) : atoi(pclTdi2)) * 60);

				olText.Format("Difference between UTC and local read from APTTAB is %d minutes APT.TICH=<%s> APT.TDI1=<%s> APT.TDI2=<%s>\n",imUtcDifference/60,pclTich,pclTdi1,pclTdi2);
				Trace("%s\n",olText);
				AddInfo(olText);
			}
			else
			{
				olText.Format("Error reading UTC/local difference from APTTAB %s",pclWhere);
				Trace("%s\n",olText);
				AddInfo(olText);

				olText.Format("DataBuff contains <%s>",olData);
				Trace("%s\n",olText);
				AddInfo(olText);
			}
			// new method replacing the one above - although the one above will be retained for now
			int ilTdi1 = atoi(pclTdi1);
			omLocalDiff1 = CTimeSpan(0,(ilTdi1 / 60) ,(ilTdi1 % 60),0);
			int ilTdi2 = atoi(pclTdi2);
			omLocalDiff2 = CTimeSpan(0,(ilTdi2 / 60) ,(ilTdi2 % 60),0);
			CString olTich = pclTich;
			omTich = DBStringToDateTime(olTich);
			// end new method

			polDataBuf->RemoveAll();
		}
	}
}

CTime CCSBasicData::GetLocalTime()
{
	return CTime::GetCurrentTime();
}

CTime CCSBasicData::GetUtcTime()
{
	CTime olNow = GetLocalTime();
	olNow -= imUtcDifference;
	return (olNow);

}


void CCSBasicData::ConvertDateToUtc(CTime &ropDate)
{
	ropDate = GetUtcFromLocal(ropDate);
}

void CCSBasicData::ConvertDateToLocal(CTime &ropDate)
{
	ropDate = GetLocalFromUtc(ropDate);
}

CTime CCSBasicData::GetUtcFromLocal(CTime opDate)
{
	if((opDate != TIMENULL) && (omTich != TIMENULL))
	{
		if(opDate < omTich)
		{
			opDate -= omLocalDiff1;
		}
		else
		{
			opDate -= omLocalDiff2;
		}
	}
	return opDate;
}

CTime CCSBasicData::GetLocalFromUtc(CTime opDate)
{
	if((opDate != TIMENULL) && (omTich != TIMENULL))
	{
		if(opDate < omTich)
		{
			opDate += omLocalDiff1;
		}
		else
		{
			opDate += omLocalDiff2;
		}
	}
	return opDate;
}



CTime CCSBasicData::GetTime()
{
	return (bmUseUtc) ? GetUtcTime() : GetLocalTime();
}

CCSBasicData::~CCSBasicData(void)
{
	for ( int ilLc = 0; ilLc < omPermissions.GetSize(); ilLc++)
	{
		omPermissions[ilLc].Functions.DeleteAll();
	}
	omPermissions.DeleteAll();
	ClearAlocUnitToPoolMap();
    ogCCSDdx.UnRegister(this, NOTUSED);
}


void CCSBasicData::ClearAlocUnitToPoolMap(void)
{
	for(POSITION rlPos1 =  omAlocUnitToPoolMap.GetStartPosition(); rlPos1 != NULL; )
	{
		CMapStringToPtr *polAlocUnitMap;
		CString olAlocUnit;
		omAlocUnitToPoolMap.GetNextAssoc(rlPos1,olAlocUnit,(void *& )polAlocUnitMap);

		for(POSITION rlPos2 = polAlocUnitMap->GetStartPosition(); rlPos2 != NULL; )
		{
			CMapStringToString *polPoolMap;
			CString olPool;
			polAlocUnitMap->GetNextAssoc(rlPos2,olPool,(void *& )polPoolMap);
			polPoolMap->RemoveAll();
			delete polPoolMap;
		}
		polAlocUnitMap->RemoveAll();
		delete polAlocUnitMap;
	}
	omAlocUnitToPoolMap.RemoveAll();
}

void CCSBasicData::AddToAlocUnitToPoolMap(CString opAlocUnitType, CString opAlocUnit, CString opPool)
{
	Trace("Add \"%s\"-\"%s\" to %sMap\n",opAlocUnit,opPool,opAlocUnitType);
	CMapStringToPtr *polAlocUnitMap;
	if(omAlocUnitToPoolMap.Lookup(opAlocUnitType, (void *&) polAlocUnitMap))
	{
		AddToAlocUnitMap(*polAlocUnitMap,opAlocUnit,opPool);
	}
	else
	{
		polAlocUnitMap = new CMapStringToPtr;
		AddToAlocUnitMap(*polAlocUnitMap,opAlocUnit,opPool);
		omAlocUnitToPoolMap.SetAt(opAlocUnitType,polAlocUnitMap);
	}
}

void CCSBasicData::AddToAlocUnitMap(CMapStringToPtr &ropAlocUnitMap, CString opAlocUnit, CString opPool)
{
	CMapStringToString *polPoolMap;
	if(ropAlocUnitMap.Lookup(opAlocUnit, (void *&) polPoolMap))
	{
		polPoolMap->SetAt(opPool,"");
	}
	else
	{
		polPoolMap = new CMapStringToString;
		polPoolMap->SetAt(opPool,"");
		ropAlocUnitMap.SetAt(opAlocUnit,polPoolMap);
	}
	omListOfPoolsWithRestrictions.SetAt(opPool,NULL);
}

bool CCSBasicData::AssignmentIsAllowed(CString opAlocUnitType, CString opAlocUnit, CString opPool)
{
	// if no restrictions for the pool are defined then the assign is OK
	bool blAssignmentIsAllowed = true;

	CString olDummy;
	if(omListOfPoolsWithRestrictions.Lookup(opPool,olDummy))
	{
		// restrictions defined for this pool
		blAssignmentIsAllowed = IsInPool(opAlocUnitType, opAlocUnit, opPool);
	}

	return blAssignmentIsAllowed;
}

// return a list of pools which have no restrictions
void CCSBasicData::GetPoolsWithoutRestrictions(CStringArray &ropPools, bool bpReset /*true*/)
{
	if(bpReset)
	{
		ropPools.RemoveAll();
	}

	CString olDummy;
	int ilNumPols = ogPolData.omData.GetSize();
	for(int ilPol = 0; ilPol < ilNumPols; ilPol++)
	{
		POLDATA *prlPol = &ogPolData.omData[ilPol];
		if(!omListOfPoolsWithRestrictions.Lookup(prlPol->Name,olDummy))
		{
			ropPools.Add(prlPol->Name);
		}
	}
}


bool CCSBasicData::IsInPool(CString opAlocUnitType, CString opAlocUnit, CString opPool)
{
	bool blIsInPool = false;

	CString olDummy;
	if(!omListOfPoolsWithRestrictions.Lookup(opPool,olDummy))
	{
		// if no pool restrictions are defined then can be assigned
		blIsInPool = true;
	}
	else
	{
		CMapStringToPtr *polAlocUnitMap;
		if(omAlocUnitToPoolMap.Lookup(opAlocUnitType,(void *& )polAlocUnitMap))
		{
			CMapStringToString *polPoolMap;
			if(polAlocUnitMap->Lookup(opAlocUnit,(void *& )polPoolMap))
			{
				CString olDummy;
				blIsInPool = polPoolMap->Lookup(opPool,olDummy) ? true : false;
			}
		}
		else
		{
			// no restrictions for the alloc unit type
			blIsInPool = true;
		}
	}
	return blIsInPool;
}

int CCSBasicData::GetPoolsByAlocUnit(CString opAlocUnitType, CString opAlocUnit, CStringArray &ropPools, bool bpReset /*true*/)
{
	if(bpReset)
	{
		ropPools.RemoveAll();
	}

	CMapStringToPtr *polAlocUnitMap;
	if(omAlocUnitToPoolMap.Lookup(opAlocUnitType,(void *& )polAlocUnitMap))
	{
		CMapStringToString *polPoolMap;
		if(polAlocUnitMap->Lookup(opAlocUnit,(void *& )polPoolMap))
		{
			for(POSITION rlPos = polAlocUnitMap->GetStartPosition(); rlPos != NULL; )
			{
				CString olPool, olDummy;
				polPoolMap->GetNextAssoc(rlPos,olPool,olDummy);
				ropPools.Add(olPool);
			}
		}
	}

	// in addition, return all pools that have no restrictions
	GetPoolsWithoutRestrictions(ropPools,false);

	return ropPools.GetSize();
}

int CCSBasicData::GetAlocUnitsByPool(CString opAlocUnitType, CString opPool, CStringArray &ropAlocUnits, bool bpReset /*true*/)
{
	if(bpReset)
	{
		ropAlocUnits.RemoveAll();
	}

	CMapStringToPtr *polAlocUnitMap;
	if(omAlocUnitToPoolMap.Lookup(opAlocUnitType,(void *&)polAlocUnitMap))
	{
		for(POSITION rlPos = polAlocUnitMap->GetStartPosition(); rlPos != NULL; )
		{
			CMapStringToString *polPoolMap;
			CString olAlocUnit;
			polAlocUnitMap->GetNextAssoc(rlPos,olAlocUnit,(void *&)polPoolMap);
			bool blNotFound = true;

			for(POSITION rlPos = polPoolMap->GetStartPosition(); blNotFound && rlPos != NULL; )
			{
				CString olPool, olDummy;
				polPoolMap->GetNextAssoc(rlPos,olPool,olDummy);
				if(olPool == opPool)
				{
					ropAlocUnits.Add(olAlocUnit);
					blNotFound = false;
				}
			}
		}
	}
	return ropAlocUnits.GetSize();
}



// GetPool()
// Get a pool for the given allocation unit.
// If more than one pool is found ask the user to select one.
// If no pools found then ask user to select one of all the pools
// On cancel return an empty olPool.
CString CCSBasicData::GetPool(CWnd *popParent, CString opAlocUnitType, CString opAlocUnit)
{
	CString olPool("");
	CStringArray olPools;
	int ilNumPools = GetPoolsByAlocUnit(opAlocUnitType,opAlocUnit,olPools);
	if(ilNumPools == 1)
	{
		olPool = olPools.GetAt(0);
	}
	else
	{
		CSelPoolDlg olDlg(popParent);

		if(ilNumPools <= 0)
		{
			ogPolData.GetPoolNames(olPools);
			// "No valid pools were found for %s.\nPlease select one."
			olDlg.m_Title.Format(GetString(IDS_NO_VALID_POOLS),opAlocUnit);
		}
		else
		{
			// "More than one valid pool was found for %s.\nPlease select one."
			olDlg.m_Title.Format(GetString(IDS_STRING61399),opAlocUnit);
		}
		olDlg.pomPools = &olPools;
		if(olDlg.DoModal() == IDOK)
		{
			olPool = olDlg.omPool;
		}
	}
//	else
//	{
//		CString olText;
//		//olText.Format("No Pools found for %s",opAlocUnit);
//		olText.Format(GetString(IDS_STRING61400),opAlocUnit);
//		MessageBox(popParent,olText,"",MB_ICONSTOP);
//	}
	return olPool;
}

void CCSBasicData::CreateAlocUnitToPoolMap(void)
{
	ClearAlocUnitToPoolMap();

	CCSPtrArray <ALLOCUNIT> olPools;
	ogAllocData.GetGroupsByGroupType(ALLOCUNITTYPE_POOLGROUP,olPools);
	int ilNumPools = olPools.GetSize();
	for(int ilPool = 0; ilPool < ilNumPools; ilPool++)
	{
		ALLOCUNIT *prlPool = &olPools[ilPool];
		int ilNumGroups = prlPool->Members.GetSize();
		for(int ilGroup = 0; ilGroup < ilNumGroups; ilGroup++)
		{
			ALLOCUNIT *prlGroup = &prlPool->Members[ilGroup];
			AddToAlocUnitToPoolMap(prlGroup->Type, prlGroup->Name, prlPool->Name);

			CCSPtrArray <ALLOCUNIT> olUnits;
			ogAllocData.GetUnitsByGroup(prlGroup->Type,prlGroup->Name,olUnits);
			int ilNumUnits = olUnits.GetSize();
			for(int ilUnit = 0; ilUnit < ilNumUnits; ilUnit++)
			{
				ALLOCUNIT *prlUnit = &olUnits[ilUnit];
				AddToAlocUnitToPoolMap(prlUnit->Type, prlUnit->Name, prlPool->Name);
			}
		}
	}
}


void CCSBasicData::SetPermission(CString opPage,CString opFunction)
{
	PERM_PAGE *prlPage = FindPermissionPage(opPage);
	if (prlPage == NULL)
	{
		prlPage = new PERM_PAGE;
		prlPage->Name = opPage;
		omPermissions.Add(prlPage);
	}
	CString *polFunction = new CString;
	*polFunction = opFunction;
	prlPage->Functions.Add((void *)polFunction);
}


PERM_PAGE *CCSBasicData::FindPermissionPage(CString opPage)
{
	for ( int ilLc = 0; ilLc < omPermissions.GetSize(); ilLc++)
	{
		if (omPermissions[ilLc].Name == opPage)
			return &omPermissions[ilLc];
	}
	return NULL;
}


BOOL CCSBasicData::GetPermission(CString opPage,CString opFunction)
{
	/* nur bis ATHHDL wieder laeuft*/
	// return TRUE;

	PERM_PAGE *prlPage = FindPermissionPage(opPage);
	if (prlPage != NULL)
	{
		for ( int ilLc = 0; ilLc < prlPage->Functions.GetSize(); ilLc++)
		{
			CString olTestOnly = prlPage->Functions[ilLc];
			if (prlPage->Functions[ilLc] == opFunction)
				return TRUE;
		}

	}
	return FALSE;
}




/*************************
	AreaArray[CCI][0]	= "First         ";
	AreaArray[CCI][1]	= "Business      ";
	AreaArray[CCI][2]	= "Economy       ";
	AreaArray[CCI][3]	= "CCI Total     ";

	AreaArray[GATE][0]  = "Pool A        ";
	AreaArray[GATE][1]  = "Pool B1       ";
	AreaArray[GATE][2]  = "Pool B2       ";
	AreaArray[GATE][3]  = "B 1/2/10/90/91";
	AreaArray[GATE][4]  = "B11 - B16     ";
	AreaArray[GATE][5]  = "Terminal C    ";
	AreaArray[GATE][5]  = "Gates         ";

*****************************************/

//extern enum enumPoolsToGate {NOTFOUND=-1,POOLA,POOLB1,POOLB12,POOLB1116};


bool CCSBasicData::GetNurnos(int ipNrOfUrnos)
{
	bool blRc = false;
	char 	pclTmpDataBuf[12*N_URNOS_AT_ONCE];

	sprintf(pclTmpDataBuf, "%d", ipNrOfUrnos);

//	ilRc = CedaAction("GNN", "", "", pclTmpDataBuf);
	blRc = CedaAction("GMU", "", "", pclTmpDataBuf);

	if (blRc)
	{
		for ( int ilItemNo=1; (ilItemNo <= N_URNOS_AT_ONCE) ; ilItemNo++ )
		{
			char pclTmpBuf[64];

			GetItem(ilItemNo, pclTmpDataBuf, pclTmpBuf);

			long llNewUrno = atol(pclTmpBuf);

			omUrnos.Add(llNewUrno);
		}
	}

	return blRc;
}


long CCSBasicData::GetNextUrno(void)
{
	CCSReturnCode	olRc = RCSuccess;
	long				llNextUrno = 0L;
	

	if (omUrnos.GetSize() == 0)
	{
		olRc = GetNurnos(N_URNOS_AT_ONCE);
	}

	if (omUrnos.GetSize() > 0)
	{
		llNextUrno = omUrnos[0];
		omUrnos.RemoveAt(0);
		if ( (llNextUrno != 0L) && (olRc == RCSuccess) )
		{
			return(llNextUrno);
		}
	}
	//::MessageBox(NULL,"Keine Verbindung zum Server oder Timeout,\nProgramm wird beendet","Systemfehler",MB_OK);
	::MessageBox(NULL,GetString(IDS_STRING61220),GetString(IDS_STRING61221),MB_OK);
	ExitProcess(1);
	return -1;	
}


CString CCSBasicData::GetDeskType(const char *pcpCicName)
{
	CString olDeskType;
	ALLOCUNIT *prlDeskType = ogAllocData.GetGroupByUnitName(ALLOCUNITTYPE_CICDESKTYPE,pcpCicName);
	if(prlDeskType != NULL)
	{
		olDeskType = prlDeskType->Name;
	}
	return olDeskType;
}




CString CCSBasicData::GetTextById(CString opKey)
{
	if (strcmp(opKey,"CCIA") == 0)
	{
		//return CString("Terminal A");
		return GetString(IDS_STRING61620);
	}
	if (strcmp(opKey,"CCIB") == 0)
	{
		//return CString("Terminal B");
		return GetString(IDS_STRING61621);
	}
	if (strcmp(opKey,"CCIC") == 0)
	{
		//return CString("Terminal C");
		return GetString(IDS_STRING61622);
	}
	if (strcmp(opKey,"CCID") == 0)
	{
		//return CString("Terminal D");
		return GetString(IDS_STRING61623);
	}
	if (strcmp(opKey,"CCIE") == 0)
	{
		//return CString("Terminal E");
		return GetString(IDS_STRING61624);
	}
	if (strcmp(opKey,"CCIK") == 0)
	{
		//return CString("Keller");
		return GetString(IDS_STRING61633);
	}
	if (strcmp(opKey,"CCIX") == 0)
	{
		//return CString("Term.B+Sonst");
		return GetString(IDS_STRING61625);
	}
	if (strcmp(opKey,"F") == 0)
	{
		//return CString("First");
		return GetString(IDS_STRING61626);
	}
	if (strcmp(opKey,"B") == 0)
	{
		//return CString("Business");
		return GetString(IDS_STRING61627);
	}
	if (strcmp(opKey,"T") == 0)
	{
		//return CString("Tourist");
		return GetString(IDS_STRING61628);
	}
	if (strcmp(opKey,"P") == 0)
	{
		//return CString("PAD");
		return GetString(IDS_STRING61629);
	}
	if (strcmp(opKey,"G") == 0)
	{
		//return CString("Gruppen");
		return GetString(IDS_STRING61630);
	}
	if (strcmp(opKey,"E") == 0)
	{
		//return CString("Express");
		return GetString(IDS_STRING61631);
	}
	//return CString("not found");
	return GetString(IDS_STRING61632);
}


CString CCSBasicData::GetIdByText(CString opKey)
{
	//if (strcmp(opKey,"Terminal A") == 0)
	if (strcmp(opKey,GetString(IDS_STRING61620)) == 0)
	{
		return CString("CCIA");
	}
	//if (strcmp(opKey,"Terminal B") == 0)
	if (strcmp(opKey,GetString(IDS_STRING61621)) == 0)
	{
		return CString("CCIB");
	}
	//if (strcmp(opKey,"Terminal C") == 0)
	if (strcmp(opKey,GetString(IDS_STRING61622)) == 0)
	{
		return CString("CCIC");
	}
	//if (strcmp(opKey,"Terminal D") == 0)
	if (strcmp(opKey,GetString(IDS_STRING61623)) == 0)
	{
		return CString("CCID");
	}
	//if (strcmp(opKey,"Terminal E") == 0)
	if (strcmp(opKey,GetString(IDS_STRING61624)) == 0)
	{
		return CString("CCIE");
	}
	//if (strcmp(opKey,"Keller") == 0)
	if (strcmp(opKey,GetString(IDS_STRING61633)) == 0)
	{
		return CString("CCIK");
	}
	//if (strcmp(opKey,"Term.B+Sonst") == 0)
	if (strcmp(opKey,GetString(IDS_STRING61625)) == 0)
	{
		return CString("CCIX");
	}
	//if (strcmp(opKey,"First") == 0)
	if (strcmp(opKey,GetString(IDS_STRING61626)) == 0)
	{
		return CString("F");
	}
	//if (strcmp(opKey,"Business") == 0)
	if (strcmp(opKey,GetString(IDS_STRING61627)) == 0)
	{
		return CString("B");
	}
	//if (strcmp(opKey,"Tourist") == 0)
	if (strcmp(opKey,GetString(IDS_STRING61628)) == 0)
	{
		return CString("T");
	}
	//if (strcmp(opKey,"PAD") == 0)
	if (strcmp(opKey,GetString(IDS_STRING61629)) == 0)
	{
		return CString("P");
	}
	//if (strcmp(opKey,"Gruppen") == 0)
	if (strcmp(opKey,GetString(IDS_STRING61630)) == 0)
	{
		return CString("G");
	}
	//if (strcmp(opKey,"Express") == 0)
	if (strcmp(opKey,GetString(IDS_STRING61631)) == 0)
	{
		return CString("E");
	}
	//return CString("not found");
	return GetString(IDS_STRING61632);
}

bool CCSBasicData::GetWindowPosition(CRect& rlPos,CString opMonitor, CString opNumMonitors /*= 0*/)
{
	rlPos.top = 0;
	rlPos.bottom = ::GetSystemMetrics(SM_CYSCREEN);//1024;
	rlPos.left = 0;
	rlPos.right = ::GetSystemMetrics(SM_CXSCREEN);//1280;

	return true;

	//Commented PRF 8712
	/*int XResolution = 1024;
	int YResolution = 768;
	if (ogCfgData.rmUserSetup.RESO[0] == '8')
	{
		XResolution = 800;
		YResolution = 600;
	}
	else
	{	//  hag20000612
		if ( (ogCfgData.rmUserSetup.RESO[0] == '1')  && (ogCfgData.rmUserSetup.RESO[1] == '2') )
		{
			XResolution = 1280;
			YResolution = 1024;
		}
	} 
  

	int ilMonitor = 0;
	if (opMonitor[0] == 'L')
		ilMonitor = 0;
	else if (opMonitor[0] == 'M' || (opMonitor[0] == 'R' && opNumMonitors[0] == '2'))
		ilMonitor = 1;
	else if (opMonitor[0] == 'R')
		ilMonitor = 2;
	
	rlPos.top = ilMonitor == 0 ? 56 : 0;
	rlPos.bottom = YResolution;
	rlPos.left = XResolution * ilMonitor;
	rlPos.right = XResolution * (ilMonitor+1);

	// validity check
	CRect rlScreen(CPoint(0,0),CSize(::GetSystemMetrics(SM_CXSCREEN),::GetSystemMetrics(SM_CYSCREEN)));
	rlScreen.NormalizeRect();
	CRect rlTest(rlPos);
	rlTest.NormalizeRect();
	CRect rlCmp;
	BOOL blIntersect = rlCmp.IntersectRect(rlScreen,rlTest);
	int ilVisInPercent = int(double(rlCmp.Width()) / rlScreen.Width() * 100);

	if (!blIntersect || ilVisInPercent < 5)
	{
		//AfxMessageBox(GetString(IDS_INVALID_WINDOWPOS),MB_ICONSTOP);
		return false;
	}
	else
		return true;*/
}

long CCSBasicData::GetUniqeIndex()
{

	static long lmUniqeIndex = 0;

	return(++lmUniqeIndex);
}


//----------------------------------------------------------------------

void CCSBasicData::TrimLeft(char *s)
{
    for (const char *p = s; isspace(*p); p++)   // search for first non-space character
        ;
    while ((*s++ = *p++) != '\0')   // move the string to get rid of leading spaces
        ;
}

void CCSBasicData::TrimRight(char *s)
{
    for (int i = strlen(s); i > 0 && isspace(s[i-1]); i--)  // search for last non-space character
        ;
    s[i] = '\0';    // trim off right spaces
}

CString CCSBasicData::GetGateAreaName(const char *pcpGateAreaId)
{
	CString olGateAreaName;
	ALLOCUNIT *prlGateArea = ogAllocData.GetGroupByName(ALLOCUNITTYPE_GATEGROUP,pcpGateAreaId);
	if(prlGateArea != NULL)
	{
		olGateAreaName = prlGateArea->Desc;
	}

	return olGateAreaName;
}

CString CCSBasicData::GetPstAreaName(const char *pcpPstAreaId)
{
	CString olPstAreaName;
	ALLOCUNIT *prlPstArea = ogAllocData.GetGroupByName(ALLOCUNITTYPE_PSTGROUP,pcpPstAreaId);
	if(prlPstArea != NULL)
	{
		olPstAreaName = prlPstArea->Desc;
	}

	return olPstAreaName;
}


void CCSBasicData::FormatDemand(DEMANDDATA *prpDem, CString &ropString)
{
	if(prpDem != NULL)
	{
		RUDDATA *prlRud = ogRudData.GetRudByUrno(prpDem->Urud);
		if(prlRud == NULL)
		{
			ropString.Format("!!! Demand with invalid URUD received URNO<%ld> URUD<%ld> !!!",prpDem->Urno,prpDem->Urud);
		}
		else
		{
			ropString.Format("DEMAND URNO<%ld> URUD<%ld> URUE<%ld> RETY<%s> %s-%s",prpDem->Urno,prpDem->Urud,prlRud->Urue,prlRud->Rety,prpDem->Debe.Format("%d/%H:%M"),prpDem->Deen.Format("%d/%H:%M"));
			FLIGHTDATA *prlFlight = NULL;
			switch(*prpDem->Dety)
			{
			case '0':
				ropString += " Turnaround ";
				prlFlight = ogFlightData.GetFlightByUrno(prpDem->Ouro);
				break;
			case '1':
				ropString += " Inbound ";
				prlFlight = ogFlightData.GetFlightByUrno(prpDem->Ouri);
				break;
			default:
				ropString += " Outbound ";
				prlFlight = ogFlightData.GetFlightByUrno(prpDem->Ouro);
				break;
			}
			if(prlFlight != NULL)
			{
				ropString += "(" + CString(prlFlight->Alc2) + CString(prlFlight->Fltn) + CString(prlFlight->Flns) + ")";
			}
			else
			{
				ropString += "(Flight is NULL!)";
			}
		}
	}
}

CString CCSBasicData::GetPoolName(const char *pcpPoolId)
{
	CString olPoolName;
	CCSPtrArray <ALLOCUNIT> olPools;

	ogAllocData.GetGroupsByGroupType(ALLOCUNITTYPE_POOLGROUP,olPools);
	int ilNumPools = olPools.GetSize();
	for(int ilPool = 0; ilPool < ilNumPools; ilPool++)
	{
		ALLOCUNIT *prlPool = &olPools[ilPool];
		if (!strcmp(prlPool->Name,pcpPoolId))
		{
			olPoolName = prlPool->Desc;
		}
	}

	return olPoolName;

//	int n = ogPools.omMetaAllocUnitList.GetSize();
//    for (int i = 0; i < n; i++)
//	{
//		if (CString(ogPools.omMetaAllocUnitList[i].Alid) == pcpPoolId)
//			return ogPools.omMetaAllocUnitList[i].Alfn;
//	}
//
//	return "";
}

COLORREF CCSBasicData::GetCciDeskColor(CString opDeskTyp)
{
	if (opDeskTyp == "F")
		return RED;

	if (opDeskTyp == "B")
		return BLUE;
	if (opDeskTyp == "T")
		return OLIVE;

	return BLACK;
}

int CCSBasicData::MessageBox(CWnd *popWnd,LPCTSTR lpszText,LPCTSTR lpszCaption,UINT nType)
{

	if (popWnd != NULL)
	{
		bgModal++;
		int ilRc = popWnd->MessageBox(lpszText,lpszCaption,nType);
		bgModal--;
		return ilRc;
	}
	else
	{
		bgModal++;
		int ilRc = ::MessageBox(NULL,lpszText,lpszCaption,nType);
		bgModal--;
		return ilRc;
	}
	return IDCANCEL;
}

void CCSBasicData::SetWindowStat(char cpStat, CWnd *popWnd, bool bpEnabledInCedaIni /*=true*/)
{
	if(popWnd != NULL)
	{
		if(cpStat == '1' && bpEnabledInCedaIni) // displayed and enabled
		{
			popWnd->ShowWindow(SW_SHOW);
			popWnd->EnableWindow(TRUE);
		}
		else if(cpStat == '-') // hidden
		{
			popWnd->ShowWindow(SW_HIDE);
		}
		else // displayed and disabled
		{
			popWnd->ShowWindow(SW_SHOW);
			popWnd->EnableWindow(FALSE);
		}
	}
}

void CCSBasicData::SetWindowStat(const char *pcpKey, CWnd *popWnd, bool bpEnabledInCedaIni /*=true*/)
{
	SetWindowStat(ogPrivList.GetStat(pcpKey), popWnd, bpEnabledInCedaIni);
}

// return true if the control referenced by pcpKey has been set to 'enabled' in BDPSSEC
bool CCSBasicData::IsEnabled(const char *pcpKey)
{
	return (pcpKey != NULL && ogPrivList.GetStat(pcpKey) == '1') ? true : false;
}

// return true if the control referenced by pcpKey has been set to 'disactived' in BDPSSEC
bool CCSBasicData::IsDisabled(const char *pcpKey)
{
	return (pcpKey != NULL && ogPrivList.GetStat(pcpKey) == '0') ? true : false;
}

// return true if the control referenced by pcpKey has been set to 'hidden' in BDPSSEC
bool CCSBasicData::IsHidden(const char *pcpKey)
{
	return (pcpKey != NULL && ogPrivList.GetStat(pcpKey) == '-') ? true : false;
}

							 
void CCSBasicData::CheckLog(void)
{
	if(bmLoggingEnabled)
	{
		CString olLogName;
		olLogName.Format(omLogFilePath,GetTime().Format("%Y%m%d"));

		if(pomLogFile == NULL)
		{
			if(OpenLog(olLogName, CFile::modeNoTruncate))
			{
				omOldLogName = olLogName;
			}
		}
		else if(olLogName != omOldLogName) // handle day change
		{
			CloseLog(); // close yesterdays log file
			if(OpenLog(olLogName, CFile::modeNoTruncate)) // open todays log file
			{
				omOldLogName = olLogName;
			}
		}
	}

	DeleteOldLogs();
}

bool CCSBasicData::OpenLog(CString opFilename, UINT ilFlag /*0*/)
{
	bool blRc = true;

	if(bmLoggingEnabled && pomLogFile == NULL)
	{
		pomLogFile = new CFile;

		if(pomLogFile != NULL)
		{
			CFileException olErr;
			if(!pomLogFile->Open(opFilename,CFile::modeCreate|CFile::modeWrite|CFile::shareDenyNone|ilFlag, &olErr))
			{
				#ifdef _DEBUG
					afxDump << opFilename << " : File could not be opened " << GetFileErr(olErr.m_cause) << "\n";
				#endif

				pomLogFile->Abort();
				blRc = false;
				delete pomLogFile;
				pomLogFile = NULL;
			}
			else
			{
				pomLogFile->SeekToEnd();
				CTime olCurrTime = GetTime();
				CString olText;
				olText.Format("**************************** Opened File at %s ****************************\n%s Auf server (%s)\n",olCurrTime.Format("%H:%M:%S %d.%m.%Y"),GetVersionString(),ogCommHandler.pcmHostName);
				WriteLog(olText);
			}
		}
	}

	return blRc;
}

bool CCSBasicData::CloseLog(void)
{
	bool blRc = true;

	if(bmLoggingEnabled && pomLogFile != NULL)
	{
		TRY
		{
			pomLogFile->Close();
		}
		CATCH( CFileException, olErr )
		{
			#ifdef _DEBUG
				afxDump << "Error closing log, cause = " << GetFileErr(olErr->m_cause) << "\n";
			#endif

			pomLogFile->Abort();
			blRc = false;
		}
		END_CATCH

		delete pomLogFile;
		pomLogFile = NULL;
	}

	return blRc;
}

bool CCSBasicData::DeleteOldLogs()
{
	bool blRc = true;

	CString olLogsToDelete;
	olLogsToDelete.Format(omLogFilePath,"*");
	//CTime olOneWeekAgo = GetTime() - CTimeSpan(igDaysBeforeLogDeleted,0,0,0);
	CTime olOneWeekAgo = GetTime() - CTimeSpan(5,0,0,0);
	WIN32_FIND_DATA olFileInfo;
	HANDLE rlHandle = FindFirstFile(olLogsToDelete, &olFileInfo);
	BOOL blFileFound = (rlHandle != INVALID_HANDLE_VALUE) ? TRUE : FALSE;
	while(blFileFound)
	{
		CTime olFileCreationDate(olFileInfo.ftCreationTime);
		if(olFileCreationDate < olOneWeekAgo)
		{
			CString olFileToDelete;
			olFileToDelete.Format("%s\\%s",CCSLog::GetTmpPath(),olFileInfo.cFileName);
			if(_stricmp(olFileToDelete,omOldLogName))
			{
				TRY
				{
					CFile::Remove(olFileToDelete);
				}
				CATCH( CFileException, olErr )
				{
					#ifdef _DEBUG
						afxDump << "Error deleting " << olFileToDelete << "  cause = " << GetFileErr(olErr->m_cause) << "\n";
					#endif
					blRc = false;
				}
				END_CATCH
			}
		}
		blFileFound = FindNextFile(rlHandle, &olFileInfo);
	}

	if (rlHandle != INVALID_HANDLE_VALUE)
		FindClose(rlHandle);

	return blRc;
}

bool CCSBasicData::WriteLog(const char *pcpData)
{
	bool blRc = true;
	if(bmLoggingEnabled && pomLogFile != NULL)
	{
		TRY
		{
			pomLogFile->Write(pcpData,strlen(pcpData));
		}
		CATCH( CFileException, olErr )
		{
			#ifdef _DEBUG
				afxDump << "Error writing to log, cause = " << GetFileErr(olErr->m_cause) << "\n";
			#endif

			pomLogFile->Abort();

			blRc = false;
			delete pomLogFile;
			pomLogFile = NULL;
		}
		END_CATCH
	}

	return blRc;
}


CString CCSBasicData::GetFileErr(int ipErr)
{
	CString olErr;

	switch(ipErr)
	{
		case CFileException::none:
			olErr = "No error occurred.";
			break;
		case CFileException::generic:
			olErr = "An unspecified error occurred.";
			break;
		case CFileException::fileNotFound:
			olErr = "The file could not be located.";
			break;
		case CFileException::badPath:
			olErr = "All or part of the path is invalid.";
			break;
		case CFileException::tooManyOpenFiles:
			olErr = "The permitted number of open files was exceeded.";
			break;
		case CFileException::accessDenied:
			olErr = "The file could not be accessed.";
			break;
		case CFileException::invalidFile:
			olErr = "There was an attempt to use an invalid file handle.";
			break;
		case CFileException::removeCurrentDir:
			olErr = "The current working directory cannot be removed.";
			break;
		case CFileException::directoryFull:
			olErr = "There are no more directory entries.";
			break;
		case CFileException::badSeek:
			olErr = "There was an error trying to set the file pointer.";
			break;
		case CFileException::hardIO:
			olErr = "There was a hardware error.";
			break;
		case CFileException::sharingViolation:
			olErr = "SHARE.EXE was not loaded, or a shared region was locked.";
			break;
		case CFileException::lockViolation:
			olErr = "There was an attempt to lock a region that was already locked.";
			break;
		case CFileException::diskFull:
			olErr = "The disk is full.�CFileException::endOfFile���The end of file was reached.";
			break;
		default:
			olErr = "Uknown error";
			break;
	}

	return olErr;
}


// this is a safe version of TRACE (TRACE crashes when there are > 512 characters)
void CCSBasicData::Trace(char *pcpFormatList, ...)
{
	char pclText[512];
	memset(pclText,'\0',512);
	va_list args;
	va_start(args, pcpFormatList);
	_vsnprintf( pclText, 500, pcpFormatList, args);

	CString olDateAndText;
	olDateAndText.Format("%s: %s",GetLocalTime().Format("%H:%M:%S"),pclText);

	if(bmLoggingEnabled)
	{
		WriteLog(olDateAndText);
	}
	AddToDebugInfoLog(olDateAndText);
	TRACE(olDateAndText);
	TRACE("\n");

	va_end(args);
}


void CCSBasicData::AddToDebugInfoLog(CString &ropMessage)
{
	if(omDebugInfoLog.GetSize() > MAX_DEBUG_INFO_LOG_MESSAGES)
	{
		omDebugInfoLog.RemoveAt(0);
	}
	omDebugInfoLog.Add(ropMessage);
	ogCCSDdx.DataChanged((void *)this,DEBUG_INFO_LOG_UPDATE,(void *)&ropMessage);
}


CString CCSBasicData::GetAlidForJob(long lpUaid,long lpUalo)
{
	CString olAlid;
	ALODATA *prlAlo = ogAloData.GetAloByUrno(lpUalo);
	if(prlAlo != NULL)
	{
		SGRDATA *prlSgr = NULL;

		if(!strncmp(prlAlo->Reft,"GAT",3))
		{
			GATDATA *prlGat = ogGatData.GetGatByUrno(lpUaid);
			if(prlGat != NULL)
			{
				olAlid = prlGat->Gnam;
			}
		}
		else if(!strncmp(prlAlo->Reft,"PST",3))
		{
			PSTDATA *prlPst = ogPstData.GetPstByUrno(lpUaid);
			if(prlPst != NULL)
			{
				olAlid = prlPst->Pnam;
			}
		}
		else if(!strncmp(prlAlo->Reft,"CIC",3))
		{
			CICDATA *prlCic = ogCicData.GetCicByUrno(lpUaid);
			if(prlCic != NULL)
			{
				olAlid = prlCic->Cnam;
			}
		}
		else if(!strncmp(prlAlo->Reft,"POL",3))
		{
			POLDATA *prlPol = ogPolData.GetPolByUrno(lpUaid);
			if(prlPol != NULL)
			{
				olAlid = prlPol->Name;
			}
		}
		else
		{
			// check if this is a group record and by default in SGRTAB
			prlSgr = ogSgrData.GetSgrByUrno(lpUaid);
			if(prlSgr != NULL)
			{
				olAlid = prlSgr->Grpn;
			}
		}
	}
	return olAlid;
}

long CCSBasicData::GetUaidForJob(const char *pcpAlid,long lpUalo)
{
	long llUaid = 0L;
	ALODATA *prlAlo = ogAloData.GetAloByUrno(lpUalo);
	if(prlAlo != NULL)
	{
		SGRDATA *prlSgr = NULL;

		if(!strncmp(prlAlo->Reft,"GAT",3))
		{
			llUaid = ogGatData.GetGatUrnoByName(pcpAlid);
		}
		else if(!strncmp(prlAlo->Reft,"CIC",3))
		{
			llUaid = ogCicData.GetCicUrnoByName(pcpAlid);
		}
		else if(!strncmp(prlAlo->Reft,"PST",3))
		{
			llUaid = ogPstData.GetPstUrnoByName(pcpAlid);
		}
		else if(!strncmp(prlAlo->Reft,"POL",3))
		{
			llUaid = ogPolData.GetPolUrnoByName(pcpAlid);
		}
		else if(!strncmp(prlAlo->Reft,"ACR",3))
		{
			ACRDATA *prlAcr = ogAcrData.GetAcrByName(pcpAlid);
			if(prlAcr != NULL)
			{
				llUaid = prlAcr->Urno;
			}
		}
		else
		{
			// check if this is a group record and by default in SGRTAB
			llUaid = ogSgrData.GetSgrUrnoByName(pcpAlid);
		}
	}
	return llUaid;
}

DEMANDDATA *CCSBasicData::GetBestDemandForEquipment(CCSPtrArray <DEMANDDATA> &ropDemands, EQUDATA *prpEqu)
{
	// best demand is the first demand found with the correct equipment defined in REQTAB and no overlap conflicts
	// next best has an overlap conflict and is returned only if the best demand is not found
	DEMANDDATA *prlBestDemand = NULL, *prlNextBestDemand = NULL;
	int ilNumDems = ropDemands.GetSize();

	if(prpEqu != NULL && ilNumDems > 0)
	{
		// get existing jobs for this equipment so we can check if the new job overlaps
		CCSPtrArray <JOBDATA> olExistingJobsForEquipment;
		ogJobData.GetJobsByUequ(olExistingJobsForEquipment, prpEqu->Urno);
		
		for(int ilDem = 0; ilDem < ilNumDems; ilDem++)
		{
			DEMANDDATA *prlDemand = &ropDemands[ilDem];
			if(CheckDemandForCorrectEquipment(prlDemand, prpEqu))
			{
				if(prlNextBestDemand == NULL)
				{
					prlNextBestDemand = prlDemand;
				}
				if(NoOverlap(olExistingJobsForEquipment, prlDemand->Debe, prlDemand->Deen))
				{
					prlBestDemand = prlDemand;
					break;
				}
			}
		}
	}
	return (prlBestDemand != NULL) ? prlBestDemand : prlNextBestDemand;
}

// return true if opBegin/opEnd don't overlap with any of the jobs in ropJobs
bool CCSBasicData::NoOverlap(CCSPtrArray <JOBDATA> &ropJobs, CTime opBegin, CTime opEnd)
{
	bool blNoOverlap = true;
	int ilNumJobs = ropJobs.GetSize();
	for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
	{
		if(IsOverlapped(ropJobs[ilJob].Acfr, ropJobs[ilJob].Acto, opBegin, opEnd))
		{
			blNoOverlap = false;
			break;
		}
	}

	return blNoOverlap;
}

// check if the equipment defined for prpDemand match the equipment to be assigned (prpEqu)
bool CCSBasicData::CheckDemandForCorrectEquipment(DEMANDDATA *prpDemand, EQUDATA *prpEqu)
{
	bool blCorrect = false;

	if(prpDemand != NULL && prpEqu != NULL)
	{
		CCSPtrArray <EQUDATA> olEquList;
		GetEquipmentForDemand(prpDemand, olEquList);
		int ilNumEqu = olEquList.GetSize();
		for(int ilEqu = 0; ilEqu < ilNumEqu; ilEqu++)
		{
			if(olEquList[ilEqu].Urno == prpEqu->Urno)
			{
				blCorrect = true;
				break;
			}
		}
	}

	return blCorrect;
}

// get a list of which equipment are valid for the demand
// popText (optional) returns a list of which equipment are required
void CCSBasicData::GetEquipmentForDemand(DEMANDDATA *prpDemand, CCSPtrArray <EQUDATA> &ropEquList)
{
	if(prpDemand != NULL)
	{
		REQDATA *prlReq = ogReqData.GetReqByUrud(prpDemand->Urud);
		if(prlReq != NULL)
		{
			EQUDATA *prlEqu = NULL;

			if(strlen(prlReq->Eqco) > 0)
			{
				// single piece of equipment required for this demand
				if((prlEqu = ogEquData.GetEquByUrno(prlReq->Uequ)) != NULL)
				{
					ropEquList.Add(prlEqu);
				}
			}
			else
			{
				SGRDATA *prlSgr = ogSgrData.GetSgrByUrno(prlReq->Uequ);
				if(prlSgr != NULL)
				{
					// group of optional equipment
					CCSPtrArray <SGMDATA> olSgms;
					ogSgmData.GetSgmListByUsgr(prlSgr->Urno,olSgms);
					int ilNumSgms = olSgms.GetSize();
					for(int ilSgm = 0; ilSgm < ilNumSgms; ilSgm++)
					{
						if((prlEqu = ogEquData.GetEquByUrno(olSgms[ilSgm].Uval)) != NULL)
						{
							ropEquList.Add(prlEqu);
						}
					}
				}
			}
		}
	}
}



int CCSBasicData::GetItemCount(CString opList, char cpTrenner /*','*/)
{
	int ilCount = 0;
	if(!opList.IsEmpty())
	{
		int ilPos = 0;
		ilCount = 1;
		while((ilPos = opList.Find(cpTrenner, ilPos)) != -1)
		{
			ilPos++;
			ilCount++;
		}
	}
	return ilCount;
}


int CCSBasicData::ExtractItemList(CString opSubString, CStringArray *popStrArray, char cpTrenner /*','*/)
{

	CString olText;

	popStrArray->RemoveAll();

	BOOL blEnd = FALSE;
	
	if(!opSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		while(blEnd == FALSE)
		{
			pos = opSubString.Find(cpTrenner);
			if(pos == -1)
			{
				blEnd = true;
				olText = opSubString;
			}
			else
			{
				olText = opSubString.Mid(0, opSubString.Find(cpTrenner));
				opSubString = opSubString.Mid(opSubString.Find(cpTrenner)+1, opSubString.GetLength( )-opSubString.Find(cpTrenner)+1);
			}
			popStrArray->Add(olText);
		}
	}
	return	popStrArray->GetSize();

}


CString CCSBasicData::GetListItem(CString &opList, int ipPos, bool bpCut, char cpTrenner /*','*/)
{
	CStringArray olStrArray;
	int ilAnz = ExtractItemList(opList, &olStrArray, cpTrenner);
	CString olReturn;

	if(ipPos == -1)
		ipPos = ilAnz;

	if((ipPos <= ilAnz) && (ipPos > 0))
		olReturn = olStrArray[ipPos - 1];
	if(bpCut)
	{
		opList = "";
		for(int ilLc = 0; ilLc < ilAnz; ilLc++)
		{
			if(ilLc != (ipPos - 1))
			{
				opList = opList + olStrArray[ilLc] + cpTrenner;
			}
		}
	}
	if(bpCut)
		opList = opList.Left(opList.GetLength() - 1);
	return olReturn;
}



CString CCSBasicData::DeleteListItem(CString &opList, CString olItem)
{
	CStringArray olStrArray;
	int ilAnz = ExtractItemList(opList, &olStrArray);
	opList = "";
	for(int ilLc = 0; ilLc < ilAnz; ilLc++)
	{
			if(olStrArray[ilLc] != olItem)
				opList = opList + olStrArray[ilLc] + ",";
	}
	opList = opList.Left(opList.GetLength() - 1);
	return opList;
}
////////////////////////////////////////////////////////////////////////////////////////////////


// return the team that this pool job belongs to (if any)
CString CCSBasicData::GetTeamForPoolJob(JOBDATA *prpPoolJob)
{
	CString olTeam;
	if(prpPoolJob != NULL)
	{
		DLGDATA *prlDlg = ogDlgData.GetDlgByUjob(prpPoolJob->Urno);
		if(prlDlg != NULL)
		{
			// the pool job is the result of the emp being delegated to another team in OpssPm
			olTeam = prlDlg->Wgpc;
		}
		else
		{
			olTeam = GetTeamForShift(ogShiftData.GetShiftByUrno(prpPoolJob->Shur));
		}
	}

	return olTeam;
}

CString CCSBasicData::GetTeamForShift(SHIFTDATA *prpShift)
{
	CString olTeam = "";
	if(prpShift != NULL)
	{
		DRGDATA *prlDrg = ogDrgData.GetDrgByShift(prpShift);
		if(prlDrg != NULL)
		{
			// the shift is the result of the emp being delegated to another team in Rostering
			olTeam = prlDrg->Wgpc;
		}
		else
		{
			// the team is that defined in the basic data for the employee
			olTeam = ogSwgData.GetGroupBySurn(prpShift->Stfu, prpShift->Avfa);
		}
	}

	return olTeam;
}

int CCSBasicData::GetFunctionWeightWithinWorkgroup(JOBDATA *prpPoolJob)
{
	int ilFunctionWeight = 9999;

	if(prpPoolJob != NULL)
	{
		WGPDATA *prlWgp = ogWgpData.GetWgpByWgpc(GetTeamForPoolJob(prpPoolJob));
		if(prlWgp != NULL)
		{
			ilFunctionWeight = ogPgpData.GetFunctionWeight(prlWgp->Pgpu, ogBasicData.GetFunctionForPoolJob(prpPoolJob));
		}
	}

	return ilFunctionWeight;
}


void CCSBasicData::GetFunctionsForPoolJob(JOBDATA *prpPoolJob, CStringArray &ropFunctions)
{
	if(prpPoolJob != NULL)
	{
		CString olFunction;
		olFunction = GetTempFunctionForPoolJob(prpPoolJob->Urno);
		if(!olFunction.IsEmpty())
		{
			ropFunctions.Add(olFunction);
		}
		else
		{
			olFunction = GetNonEmployeeFunctionForPoolJob(prpPoolJob);
			if(!olFunction.IsEmpty())
			{
				ropFunctions.Add(olFunction);
			}
		}
		// get all the functions for the employee
		ogSpfData.GetFunctionsBySurn(prpPoolJob->Ustf, ropFunctions, prpPoolJob->Acfr);
	}
}

void CCSBasicData::SetTempFunctionForPoolJob(long lpPjUrno, CString opFunction)
{
	CString olPJUrno;
	olPJUrno.Format("%ld", lpPjUrno);
	omTempPoolJobFunction.SetAt(olPJUrno,opFunction);
}

void CCSBasicData::ClearTempFunctionForPoolJob()
{
	omTempPoolJobFunction.RemoveAll();
}

CString CCSBasicData::GetTempFunctionForPoolJob(long lpPjUrno)
{
	CString olFunction = "";
	CString olPJUrno;
	olPJUrno.Format("%ld", lpPjUrno);
	omTempPoolJobFunction.Lookup(olPJUrno,olFunction);
	return olFunction;
}

CString CCSBasicData::GetFunctionForPoolJob(JOBDATA *prpPoolJob)
{
	CString olFunction;

	if(prpPoolJob != NULL)
	{
		olFunction = GetNonEmployeeFunctionForPoolJob(prpPoolJob);

 		if(olFunction.IsEmpty())
		{
			// get the highest priority function for the employee
			olFunction = ogSpfData.GetFunctionBySurn(prpPoolJob->Ustf, prpPoolJob->Acfr);
		}
	}

	return olFunction;
}


// get functions not directly connected to the employee
CString CCSBasicData::GetNonEmployeeFunctionForPoolJob(JOBDATA *prpPoolJob)
{
	CString olFunction;

	if(prpPoolJob != NULL)
	{
		olFunction = GetTempFunctionForPoolJob(prpPoolJob->Urno);
		if(olFunction.IsEmpty())
		{
			olFunction = GetNonEmployeeFunction(prpPoolJob->Urno, prpPoolJob->Ustf, prpPoolJob->Acfr, prpPoolJob->Udrd, prpPoolJob->Udel, prpPoolJob->Shur);
		}
	}

	return olFunction;
}

void CCSBasicData::GetFunctionsForShift(SHIFTDATA *prpShift, CStringArray &ropFunctions)
{
	if(prpShift != NULL)
	{
		CString olFunction = GetNonEmployeeFunctionForShift(prpShift);
		if(!olFunction.IsEmpty())
		{
			ropFunctions.Add(olFunction);
		}
		// get all the functions for the employee
		ogSpfData.GetFunctionsBySurn(prpShift->Stfu, ropFunctions, prpShift->Avfa);
	}
}

CString CCSBasicData::GetFunctionForShift(SHIFTDATA *prpShift)
{
	CString olFunction;

	if(prpShift != NULL)
	{
		olFunction = GetNonEmployeeFunctionForShift(prpShift);

		if(olFunction.IsEmpty())
		{
			// get the highest priority function for the employee
			olFunction = ogSpfData.GetFunctionBySurn(prpShift->Stfu, prpShift->Avfa);
		}
	}

	return olFunction;
}

// get functions not directly connected to the employee
CString CCSBasicData::GetNonEmployeeFunctionForShift(SHIFTDATA *prpShift)
{
	CString olFunction;

	if(prpShift != NULL)
	{
		// get shift deviation
		long llDrdUrno = 0L;
//		DRDDATA *prlDrd = ogDrdData.GetDrdByShiftData(prpShift->Drrn, prpShift->Sday, prpShift->Stfu);
//		if(prlDrd != NULL)
//		{
//			llDrdUrno = prlDrd->Urno;
//		}

		// get shift deviation
		long llDelUrno = 0L;

		olFunction = GetNonEmployeeFunction(0L, prpShift->Stfu, prpShift->Avfa, llDrdUrno, llDelUrno, prpShift->Urno);
	}

	return olFunction;
}

CString CCSBasicData::GetNonEmployeeFunction(long lpPoolJobUrno, long lpEmpUrno, CTime opShiftStart, long lpDrdUrno, long lpDelUrno, long lpShiftUrno)
{
	CString olFunction;

	SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(lpShiftUrno);
	DRGDATA *prlDrg = NULL;
	DLGDATA *prlDlg = NULL;
	if(bmDisplayTeams && (prlDlg = ogDlgData.GetDlgByUjob(lpPoolJobUrno)) != NULL)
	{
		// group delegation - the emp is given a new function temporarily so that they can belong to a group
		// !! this was assigned in OPSS-PM
		olFunction = prlDlg->Fctc;
	}
	else if(bmDisplayTeams && (prlDrg = ogDrgData.GetDrgByShift(prlShift)) != NULL)
	{
		// group deviation - the emp is given a new function temporarily so that they can belong to a group
		// !! this was assigned in Rostering
		olFunction = prlDrg->Fctc;
	}
	else if(lpDrdUrno != 0L)
	{
		// deviations from the normal shift have their own functions and permits (optional)
		olFunction = ogDrdData.GetFunctionsByUrno(lpDrdUrno);
	}
	else if(lpDelUrno != 0L)
	{
		// delegations from basis shift have their own functions (optional)
		olFunction = ogDelData.GetFunctionByUrno(lpDelUrno);
	}
	else if(prlShift != NULL)
	{
		if(strlen(prlShift->Fctc) > 0)
		{
			// function code comes from the shift (DRRTAB)
			olFunction = prlShift->Fctc;
		}
		else
		{
			SHIFTTYPEDATA *prlShiftType = ogShiftTypes.GetShiftTypeByCode(prlShift->Sfca);
			if(prlShiftType != NULL)
			{
				// function code comes from the basic shift (BSDTAB)
				olFunction = prlShiftType->Fctc;
			}
		}
	}

	return olFunction;
}

void CCSBasicData::GetPermitsForPoolJob(JOBDATA *prpPoolJob, CStringArray &ropPermits)
{
	if(prpPoolJob != NULL)
	{
		if(prpPoolJob->Udrd != 0L)
		{
			// deviations from the normal shift have their own functions and permits (optional)
			ogDrdData.GetPermitsByUrno(prpPoolJob->Udrd,ropPermits);
		}

		// either no permits found or normal shift
		if(ropPermits.GetSize() <= 0)
		{
			ogSpeData.GetPermitsBySurn(prpPoolJob->Ustf,ropPermits);
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////

bool CCSBasicData::CheckFunctionsAndPermits(JOBDATA *prpPoolJob, DEMANDDATA *prpDemand, CStringArray &ropErrors)
{
	if(prpPoolJob == NULL)
	{
		ropErrors.Add("Internal error: CCSBasicData::CheckFunctionsAndPermits() prpPoolJob is NULL !");
		return false;
	}
	if(prpDemand == NULL)
	{
		ropErrors.Add("Internal error: CCSBasicData::CheckFunctionsAndPermits() prpDemand is NULL !");
		return false;
	}

	if(ogDemandData.RuleIndependant(prpDemand))
	{
		return CheckFunctionsAndPermitsInAzatab(prpPoolJob, prpDemand, ropErrors);
	}
	else
	{
		return CheckFunctionsAndPermitsInRule(prpPoolJob, prpDemand, ropErrors);
	}
}

// ropPoolDemands - this is a list of PoolJob<->Demand (assignments) - Demand is NULL if the pool job is not assigned
// bpAlwaysAssignSingleEmpAndDemand - if there is only a single emp and demand specified then always assign them even if they don't match
bool CCSBasicData::CheckFunctionsAndPermits2(JOBDATA *prpPoolJobs, DEMANDDATA *prpDemands, bool bpAlwaysAssignSingleEmpAndDemand /*=true*/)
{
	bool blUnassignedJobsOrDemandsFound = true;
	if(prpPoolJobs != NULL && prpDemands != NULL)
	{
		CCSPtrArray <JOBDATA> olPoolJobs;
		CCSPtrArray <DEMANDDATA> olDemands;
		olPoolJobs.Add(prpPoolJobs);
		olDemands.Add(prpDemands);
		CMapPtrToPtr olPoolDemands;
		blUnassignedJobsOrDemandsFound = CheckFunctionsAndPermits2(olPoolJobs, olDemands, olPoolDemands, bpAlwaysAssignSingleEmpAndDemand);
	}

	return blUnassignedJobsOrDemandsFound;
}

// ropPoolDemands - this is a list of PoolJob<->Demand (assignments) - Demand is NULL if the pool job is not assigned
// bpAlwaysAssignSingleEmpAndDemand - if there is only a single emp and demand specified then always assign them even if they don't match
bool CCSBasicData::CheckFunctionsAndPermits2(CCSPtrArray <JOBDATA> &ropPoolJobs, CCSPtrArray <DEMANDDATA> &ropDemands, CMapPtrToPtr &ropPoolDemands, bool bpAlwaysAssignSingleEmpAndDemand /*=true*/)
{
	bool blUnassignedJobsOrDemandsFound = false;	
	GetPrivateProfileString(pcgAppName,"INTERCHANGABLEFCTC","NO",pcmInterchangeable,sizeof(pcmInterchangeable),GetConfigFileName());
	// loop through all pool jobs and create a list for which demands they are valid
	CCSPtrArray <MATCHDATA> olMatchList;
	int ilNumPoolJobs, ilPoolJob, ilNumDemands, ilDemand;
	JOBDATA *prlPoolJob;
	DEMANDDATA *prlDemand;

	ilNumPoolJobs = ilNumPoolJobs = ropPoolJobs.GetSize();
	ilNumDemands = ilNumDemands = ropDemands.GetSize();
	int ilWeight;

	for(ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
	{
		prlPoolJob = &ropPoolJobs[ilPoolJob];

		for(ilDemand = 0; ilDemand < ilNumDemands; ilDemand++)
		{
			prlDemand = &ropDemands[ilDemand];
			if((ilWeight = GetAssignmentSuitabiltyWeighting(prlPoolJob, prlDemand)) != EMP_NOT_MATCH_DEMAND)
			{
				MATCHDATA *prlMatchData = new MATCHDATA;
				prlMatchData->Udem.Format("%d",prlDemand->Urno);
				prlMatchData->Upjb.Format("%d",prlPoolJob->Urno);
				prlMatchData->Stat = 0;
				prlMatchData->Weight = ilWeight;
				olMatchList.Add(prlMatchData);
			}
		}
	}

	// the weight indicates which function was used ie. weight=1 means that the main
	// function was used, a lower priority function has a lower weight.
	// sorting by weight means that emps with higher priority functions will be allocated first
	olMatchList.Sort(ByWeight);

	// given a list of all possible PoolJob-Demand combinations return the optimum combination
	// ropMatchList.Upjb = pool job URNO / ropMatchList.Udem = demand URNO / ropMatchList.Stat = 1 -> valid combination  0 -> invalid combination
//FSC changes start 16.07.01
	if(!SelectBestEmpDemandCombination(olMatchList,ilNumDemands))
	{
		CStringArray olFctcList;
		bool blDemFound = false;
		CString olActFctc = "";
		for(int ilMatch = 0; ilMatch < olMatchList.GetSize(); ilMatch++)
		{
			MATCHDATA *prlMatch = &olMatchList[ilMatch];
			prlMatch->Mark = 0;
			prlMatch->Tsta = 0;
		}


		CStringList olPoolUrnoList;
		char olTempUrno[12];
		for(ilMatch = 0; ilMatch < olMatchList.GetSize(); ilMatch++)
		{
			blDemFound = false; 			
			long llDemUrno = 0L;
			MATCHDATA *prlMatch = &olMatchList[ilMatch];
			if(prlMatch->Mark == -1)
				continue;
			for (int ilPoolJob = 0; ilPoolJob < ilNumPoolJobs && !blDemFound;ilPoolJob++)
			{				
				olFctcList.RemoveAll();
				prlPoolJob = &ropPoolJobs[ilPoolJob];
				
				//void* polPtr;
				if(olPoolUrnoList.Find(CString(ltoa(prlPoolJob->Urno,olTempUrno,10))) != NULL)
					continue;
				
				if(atol(prlMatch->Upjb) != prlPoolJob->Urno || (prlMatch->Mark == -1))
					continue;

				GetFunctionsForPoolJob(prlPoolJob,olFctcList);
				for(int ilFctIdx = 0;ilFctIdx < olFctcList.GetSize() && !blDemFound;ilFctIdx++)
				{
					olActFctc = olFctcList[ilFctIdx];					
					DEMANDDATA *prlDem = ogDemandData.GetDemandByUrno(atol(prlMatch->Udem));
					if(prlDem != NULL)
					{
						RPFDATA *prlDemandFunction = ogRpfData.GetFunctionByUrud(prlDem->Urud);
						if(prlDemandFunction != NULL)
						{
							CString olDemandFunction = CString(prlDemandFunction->Fcco);

							if(olDemandFunction == olActFctc)
							{
								prlMatch->Tsta = 1;
								blDemFound = true;
								llDemUrno = atol(prlMatch->Udem);
								olPoolUrnoList.AddTail(ltoa(prlPoolJob->Urno,olTempUrno,10));
							}
						}
					}
				}
				if(blDemFound)
				{
					for(int ilMatch = 0; ilMatch < olMatchList.GetSize(); ilMatch++)
					{	
						MATCHDATA *prlMatch = &olMatchList[ilMatch];
						if(llDemUrno == atol(prlMatch->Udem) && prlMatch->Tsta != 1)
						{
							prlMatch->Mark = -1;
						}
					}
				}
			}
		}
		int ilTsta = 0;
		int ilStat = 0;
		for( ilMatch = 0; ilMatch < olMatchList.GetSize(); ilMatch++)
		{	
			MATCHDATA *prlMatch = &olMatchList[ilMatch];
			if(prlMatch->Tsta == 1)
			{
				ilTsta++;
			}
			if(prlMatch->Stat == 1)
			{
				ilStat++;
			}
		}
		if(ilTsta >= ilStat)
		{
			for(int ilMatch = 0; ilMatch < olMatchList.GetSize(); ilMatch++)
			{	
				MATCHDATA *prlMatch = &olMatchList[ilMatch];
				prlMatch->Stat = prlMatch->Tsta;
			}
		}
	}
//FSC changes end 
	// add the assigned pool jobs to the results list
	CString olResult, olTmp;
	int ilNumMatches = olMatchList.GetSize();
	for(int ilMatch = 0; ilMatch < ilNumMatches; ilMatch++)
	{
		MATCHDATA *prlMatch = &olMatchList[ilMatch];

		if(prlMatch->Stat == 1)
		{
			ropPoolDemands.SetAt((void *) atol(prlMatch->Upjb), ogDemandData.GetDemandByUrno(atol(prlMatch->Udem)));
		}
	}
	int ilNumAssigned = ropPoolDemands.GetCount();
	if(ilNumPoolJobs == 1 && ilNumDemands == 1 && ilNumAssigned <= 0 && bpAlwaysAssignSingleEmpAndDemand)
	{
		// single demand and pool job so assign without checking permits and functions.
		// Any permit and function conflicts will be displayed in NewAssignConflictDlg
		ropPoolDemands.SetAt((void *) ropPoolJobs[0].Urno, &ropDemands[0]);
	}
	else
	{
		// add unassigned pool jobs to the results list
		for(ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
		{
			prlPoolJob = &ropPoolJobs[ilPoolJob];
			if(!ropPoolDemands.Lookup((void *) prlPoolJob->Urno, (void *& ) prlDemand))
			{
				ropPoolDemands.SetAt((void *) prlPoolJob->Urno, NULL);
			}
		}
	}

	olMatchList.DeleteAll();
	if(ilNumPoolJobs != ilNumAssigned)
	{
		blUnassignedJobsOrDemandsFound = true;
	}


	return blUnassignedJobsOrDemandsFound;
}

// return a weighting factor that indicates how suitable this employee is to be assigbned to demand
// NB: emp function refers to the functions defined in the basic data for an employee as a pose
//     to functions defined for a shift/group/delegation/deviation
// where weight = 10000 Cannot be assigned
//				= 1 Has correct permits and non-emp function
//				= 2 Has correct permits and emp functions with priority 1
//				= 3+ Has correct permits and emp functions with priority > 1 (weight = function priority+1)
// This function can be used to sort employees by their assignment suitability
int CCSBasicData::GetAssignmentSuitabiltyWeighting(JOBDATA *prpPoolJob, DEMANDDATA *prpDemand)
{
	int ilWeight = EMP_NOT_MATCH_DEMAND; // High number 

	if(prpPoolJob != NULL && prpDemand != NULL)
	{
		CStringArray olFunctions, olPermits, olErrors;
		GetPermitsForPoolJob(prpPoolJob, olPermits);

		// test for weight 1
		CString olFunction = GetNonEmployeeFunctionForPoolJob(prpPoolJob);
		olFunctions.Add(olFunction);

		if(ogDemandData.RuleIndependant(prpDemand))
		{
			if(CheckFunctionsAndPermitsInAzatab(olFunction, olPermits, prpDemand, olErrors))
			{
				ilWeight = 0;
			}
		}
		else
		{
			if(CheckFunctionsAndPermitsInRule(olFunctions, olPermits, prpDemand, olErrors))
			{
				ilWeight = 0;
			}
			else if(ogBasicData.IsSinApron1() == true || ogBasicData.IsSinApron2() == true)
			{
				olErrors.RemoveAll();
				if(prpPoolJob->Utpl != prpDemand->Utpl && strcmp(pcmInterchangeable,"YES") == 0)
				{
					if(olFunction.Find(A1) != -1)
						olFunction.Replace(A1,A2);
					else if(olFunction.Find(A2) != -1)
						olFunction.Replace(A2,A1);
					olFunctions.RemoveAll();
					olFunctions.Add(olFunction);
					if(CheckFunctionsAndPermitsInRule(olFunctions, olPermits, prpDemand, olErrors))
					{
						ilWeight = 0;
					}
				}
			}
		}

		if(ilWeight == EMP_NOT_MATCH_DEMAND)
		{
			// test the employee's basic functions (order them by priority)
			CCSPtrArray <SPFDATA> olSpfs;
			ogSpfData.GetSpfsBySurn(prpPoolJob->Ustf, olSpfs, prpPoolJob->Acfr);
			int ilNumSpfs = olSpfs.GetSize();
			for(int ilSpf = 0; ilSpf < ilNumSpfs && ilWeight == EMP_NOT_MATCH_DEMAND; ilSpf++)
			{
				int ilNewWeight = EMP_NOT_MATCH_DEMAND;

				SPFDATA *prlSpf = &olSpfs[ilSpf];
				olFunctions.RemoveAll();
				olErrors.RemoveAll();
				olFunctions.Add(prlSpf->Code);
				olFunction = prlSpf->Code;

				if(ogDemandData.RuleIndependant(prpDemand))
				{
					if(CheckFunctionsAndPermitsInAzatab(olFunction, olPermits, prpDemand, olErrors))
					{
						ilNewWeight = prlSpf->Priority;
					}
				}
				else
				{
					if(CheckFunctionsAndPermitsInRule(olFunctions, olPermits, prpDemand, olErrors))
					{
						ilNewWeight = prlSpf->Priority;
					}
				}

				if(ilNewWeight < ilWeight)
				{
					// found a matching function with a better priority
					ilWeight = ilNewWeight;
				}
			}
		}
	}

	return ilWeight;
}

bool CCSBasicData::CheckFunctionsAndPermitsInRule(JOBDATA *prpPoolJob, DEMANDDATA *prpDemand, CStringArray &ropErrors)
{
	// get the function and permits for this emp
	CStringArray olEmpPermits, olEmpFunctions;
	GetFunctionsForPoolJob(prpPoolJob,olEmpFunctions);
	GetPermitsForPoolJob(prpPoolJob, olEmpPermits);

	return CheckFunctionsAndPermitsInRule(olEmpFunctions, olEmpPermits, prpDemand, ropErrors);
}

bool CCSBasicData::CheckFunctionsAndPermitsInRule(CStringArray &ropEmpFunctions, CStringArray &ropEmpPermits, DEMANDDATA *prpDemand, CStringArray &ropErrors)
{
	bool blNotFound;
	CString olText;

	int ilNumEmpPermits = ropEmpPermits.GetSize();
	int ilNumEmpFunctions = ropEmpFunctions.GetSize();

	// get and check the function(s) for the demand to be assigned
	RPFDATA *prlDemandFunction = ogRpfData.GetFunctionByUrud(prpDemand->Urud);
	if(prlDemandFunction != NULL)
	{
		CString olDemandFunction = CString(prlDemandFunction->Fcco);
		if(olDemandFunction.IsEmpty())
		{
			// group of optional functions
			CCSPtrArray <SGMDATA> olSgms;
			ogSgmData.GetSgmListByUsgr(prlDemandFunction->Upfc,olSgms);
			int ilNumSgms = olSgms.GetSize();
			CString olMissingFunctions;

			blNotFound = true;
			for(int ilF = 0; ilF < ilNumEmpFunctions && blNotFound; ilF++)
			{
				for(int ilSgm = 0; blNotFound && ilSgm < ilNumSgms; ilSgm++)
				{
					PFCDATA *prlPfc = ogPfcData.GetPfcByUrno(olSgms[ilSgm].Uval);
					if(prlPfc != NULL)
					{
						if(prlPfc->Fctc == ropEmpFunctions[ilF])
						{
							blNotFound = false;
						}
						else
						{
							AddElem(olMissingFunctions,prlPfc->Fctc);
						}
					}
				}
			}
			if(blNotFound)
			{
				//olText.Format("Emp requires one of the following functions: %s",olMissingFunctions);
				olText.Format(GetString(IDS_STRING61369),olMissingFunctions);
				ropErrors.Add(olText);
			}
		}
		else
		{
			// check if one of emp's functions matches the single mandatory function for the demand
			blNotFound = true;
			for(int ilF = 0; ilF < ilNumEmpFunctions && blNotFound; ilF++)
			{
				if(olDemandFunction == ropEmpFunctions[ilF])
				{
					blNotFound = false;
				}
			}

			if(blNotFound)
			{
				//olText.Format("Emp requires the following function: '%s'",olDemandFunction);
				olText.Format(GetString(IDS_STRING61370),olDemandFunction);
				ropErrors.Add(olText);
			}
		}
	}

	// get and check the permit(s) for this demand
	CString olMissingMandatoryPermits, olMissingOptionalPermits;
	CCSPtrArray<RPQDATA> olDemandPermits;
	// lli: need to check the permits for udgr as well
	CCSPtrArray<RPQDATA> olDemandPermits2;
	ogRpqData.GetRpqsByUrud(prpDemand->Urud,olDemandPermits);
	ogRpqData.GetRpqsByUdgr(prpDemand->Udgr,olDemandPermits2);
	olDemandPermits.Append(olDemandPermits2);
	int ilNumDemandPermits = olDemandPermits.GetSize();

	for(int ilDP = 0; ilDP < ilNumDemandPermits; ilDP++)
	{
		RPQDATA *prlDemandPermit = &olDemandPermits[ilDP];
		CString olDemandPermit = prlDemandPermit->Quco;
		if(olDemandPermit.IsEmpty())
		{
			// this is a header for a group of permits, one of which the emp must have
			CCSPtrArray <SGMDATA> olSgms;
			ogSgmData.GetSgmListByUsgr(prlDemandPermit->Uper,olSgms);
			int ilNumSgms = olSgms.GetSize();
			olMissingOptionalPermits.Empty();
			blNotFound = true;
			for(int ilSgm = 0; blNotFound && ilSgm < ilNumSgms; ilSgm++)
			{
				PERDATA *prlPer = ogPerData.GetPerByUrno(olSgms[ilSgm].Uval);
				if(prlPer != NULL)
				{
					for(int ilEP = 0; blNotFound && ilEP < ilNumEmpPermits; ilEP++)
					{
						if(ropEmpPermits[ilEP] == prlPer->Prmc)
						{
							blNotFound = false;
						}
					}
					if(blNotFound)
					{
						AddElem(olMissingOptionalPermits,prlPer->Prmc);
					}
				}
			}
			if(blNotFound)
			{
				//olText.Format("Emp requires one of the following permits %s",olMissingOptionalPermits);
				olText.Format(GetString(IDS_STRING61373),olMissingOptionalPermits);
				ropErrors.Add(olText);
			}
		}
		else
		{
			// not a group of permits, so this is a must-have permit
			blNotFound = true;
			for(int ilEP = 0; blNotFound && ilEP < ilNumEmpPermits; ilEP++)
			{
				if(ropEmpPermits[ilEP] == olDemandPermit)
				{
					blNotFound = false;
				}
			}
			if(blNotFound)
			{
				AddElem(olMissingMandatoryPermits,olDemandPermit);
			}
		}
	}

	if(!olMissingMandatoryPermits.IsEmpty())
	{
		//olText.Format("Emp requires the following permits %s",olMissingMandatoryPermits);
		olText.Format(GetString(IDS_STRING61374),olMissingMandatoryPermits);
		ropErrors.Add(olText);
	}


	// this demand can be part of a group of demands which require a team of emps,
	// the following creates lists of permits/functions required by the team
	// get all other permits (RPQs) with the same group no.(Udgr) as this one
	if(prpDemand->Ulnk != 0L)
	{
		CStringArray olTeamRequiresAllOfThesePermits;
		CStringArray olTeamRequiresOneOfThesePermits;
		int ilNumOptionalTeamPermits, ilNumMandatoryTeamPermits;

		CCSPtrArray <RPQDATA> olRpqs;
		ogRpqData.GetRpqsByUdgr(prpDemand->Udgr,olRpqs);
		int ilNumRpqs = olRpqs.GetSize();
		for(int ilPermit = 0; ilPermit < ilNumRpqs; ilPermit++)
		{
			RPQDATA *prlPermit = &olRpqs[ilPermit];
			CString olTeamPermit = prlPermit->Quco;
			if(olTeamPermit.IsEmpty())
			{
				// this is a header for a group of permits, one of which the team must have
				CCSPtrArray <SGMDATA> olSgms;
				ogSgmData.GetSgmListByUsgr(prlPermit->Uper,olSgms);
				int ilNumSgms = olSgms.GetSize();
				for(int ilSgm = 0; ilSgm < ilNumSgms; ilSgm++)
				{
					PERDATA *prlPer = ogPerData.GetPerByUrno(olSgms[ilSgm].Uval);
					if(prlPer != NULL)
					{
						olTeamRequiresOneOfThesePermits.Add(prlPer->Prmc);
					}
				}
			}
			else
			{
				// not a group of permits, so this is a must-have permit for the team
				olTeamRequiresAllOfThesePermits.Add(olTeamPermit);
			}
		}


		int ilNumUnassignedDemandsInTeam = 0;

		// get other emps within this team and check off the permits
		// that are already present within the team
		if(olTeamRequiresAllOfThesePermits.GetSize() > 0 ||
			olTeamRequiresOneOfThesePermits.GetSize() > 0)
		{
			// get all demands for this team
			CCSPtrArray <DEMANDDATA> olTeamDemands;
			ogDemandData.GetTeamDemands(prpDemand,olTeamDemands);

			// for each of the demands get any assigned emps to check
			// which functions and permits are still required for the team
			int ilNumTeamDemands = olTeamDemands.GetSize();
			for(int ilD = 0; ilD < ilNumTeamDemands && ilNumUnassignedDemandsInTeam <= 1 &&
				(olTeamRequiresAllOfThesePermits.GetSize() > 0 ||
				 olTeamRequiresOneOfThesePermits.GetSize() > 0); ilD++)
			{
				DEMANDDATA *prlDem = &olTeamDemands[ilD];

				CCSPtrArray <JOBDATA> olAssignedJobs;
				ogJodData.GetJobsByDemand(olAssignedJobs,prlDem->Urno);
				int ilNumAssignedJobs = olAssignedJobs.GetSize();

				if(ilNumAssignedJobs <= 0 || prlDem->Urno == prpDemand->Urno)
				{
					// demand is unassigned or is the one to be assigned now
					ilNumUnassignedDemandsInTeam++;
				}
				else
				{
					for(int ilAJ = 0; ilAJ < ilNumAssignedJobs; ilAJ++)
					{
						long llEmpUrno = olAssignedJobs[ilAJ].Ustf;
						CStringArray ropEmpPermits;
						ogSpeData.GetPermitsBySurn(llEmpUrno,ropEmpPermits);
						int ilNumEmpPermits = ropEmpPermits.GetSize();
						for(int ilE = 0; ilE < ilNumEmpPermits; ilE++)
						{
							CString olEmpPermit = ropEmpPermits[ilE];
							for(int ilPermit = 0; ilPermit < olTeamRequiresOneOfThesePermits.GetSize(); ilPermit++)
							{
								CString olRequiredPermit = olTeamRequiresOneOfThesePermits[ilPermit];
								if(olEmpPermit == olRequiredPermit)
								{
									// if one emp in the team has this permit, then no more
									// emps with the permits in this list are required
									olTeamRequiresOneOfThesePermits.RemoveAll();
								}
							}
							for(int ilP2 = (olTeamRequiresAllOfThesePermits.GetSize()-1); ilP2 >= 0; ilP2--)
							{
								CString olRequiredPermit = olTeamRequiresAllOfThesePermits[ilP2];
								if(olEmpPermit == olRequiredPermit)
								{
									// if an emp in the team has one of the required permits
									// then remove the permit from the list
									olTeamRequiresAllOfThesePermits.RemoveAt(ilP2);
								}
							}
						}
					}
				}
			}
		}

		// if this is the last demand to be assigned within a team...
		if(ilNumUnassignedDemandsInTeam <= 1)
		{
			CString olMissingPermits;

			// check if the emp has one of the required optional permits for the team
			ilNumOptionalTeamPermits = olTeamRequiresOneOfThesePermits.GetSize();
			if(ilNumOptionalTeamPermits > 0)
			{
				olMissingPermits.Empty();
				blNotFound = true;
				for(int ilPermit = 0; blNotFound && ilPermit < ilNumOptionalTeamPermits; ilPermit++)
				{
					for(int ilE = 0; blNotFound && ilE < ilNumEmpPermits; ilE++)
					{
						if(ropEmpPermits[ilE] == olTeamRequiresOneOfThesePermits[ilPermit])
						{
							blNotFound = false;
						}
					}
				}
				if(blNotFound)
				{
					for(int ilPermit = 0; ilPermit < ilNumOptionalTeamPermits; ilPermit++)
					{
						AddElem(olMissingPermits,olTeamRequiresOneOfThesePermits[ilPermit]);
					}
					//olText.Format("Team requires one of the following optional permits %s",olMissingPermits);
					olText.Format(GetString(IDS_STRING61375),olMissingPermits);
					ropErrors.Add(olText);
				}
			}
			// check if the emp has all of the required mandatory permits for the team
			ilNumMandatoryTeamPermits = olTeamRequiresAllOfThesePermits.GetSize();
			if(ilNumMandatoryTeamPermits > 0)
			{
				olMissingPermits.Empty();
				for(int ilPermit = 0, blNotFound = true; blNotFound && ilPermit < ilNumMandatoryTeamPermits; ilPermit++)
				{
					blNotFound = true;
					for(int ilE = 0; blNotFound && ilE < ilNumEmpPermits; ilE++)
					{
						if(ropEmpPermits[ilE] == olTeamRequiresAllOfThesePermits[ilPermit])
						{
							blNotFound = false;
						}
					}
					if(blNotFound)
					{
						AddElem(olMissingPermits,olTeamRequiresAllOfThesePermits[ilPermit]);
					}
				}
				if(blNotFound)
				{
					//olText.Format("Team requires the following mandatory permits %s",olMissingPermits);
					olText.Format(GetString(IDS_STRING61381),olMissingPermits);
					ropErrors.Add(olText);
				}
			}
		}
	} // end team checking -> if(prlRud->Udgr != 0L)

	// return true if the emp has the correct functions and permits for this demand
	return (ropErrors.GetSize() <= 0);
}

bool CCSBasicData::CheckFunctionsAndPermitsInAzatab(JOBDATA *prpPoolJob, DEMANDDATA *prpDemand, CStringArray &ropErrors)
{
	CStringArray olPermits;
	CString olFunction;

	// get the function and permits for the new emp to be assigned
	olFunction = GetFunctionForPoolJob(prpPoolJob);
	GetPermitsForPoolJob(prpPoolJob, olPermits);

	return CheckFunctionsAndPermitsInAzatab(olFunction, olPermits, prpDemand, ropErrors);
}

bool CCSBasicData::CheckFunctionsAndPermitsInAzatab(CString &ropFunction, CStringArray &ropPermits, DEMANDDATA *prpDemand, CStringArray &ropErrors)
{
	FUNC_PERMITS olNewEmp;
	olNewEmp.Function = ropFunction;
	int ilNumPermits, ilPermit;

	ilNumPermits = ropPermits.GetSize();
	for(ilPermit = 0; ilPermit < ilNumPermits; ilPermit++)
	{
		olNewEmp.Permits += "|" + ropPermits[ilPermit] + "|";
	}

	return CheckFunctionsAndPermitsInAzatab(olNewEmp, prpDemand, ropErrors);
}

// bpInfoOnly - if true, then ropErrors contains a list of which functions and permits are required by the group and which are already satisfied
bool CCSBasicData::CheckFunctionsAndPermitsInAzatab(FUNC_PERMITS &ropNewEmp, DEMANDDATA *prpDemand, CStringArray &ropErrors)
{
	CStringArray olPermits;
	CString olFunction;
	int ilNumPermits, ilPermit;
	CString olMsg;

	/*
	// get the function and permits for the new emp to be assigned
	olFunction = GetFunctionForPoolJob(prpPoolJob);
	GetPermitsForPoolJob(prpPoolJob, olPermits);

	FUNC_PERMITS ropNewEmp;
	ropNewEmp.Function = olFunction;
	ilNumPermits = olPermits.GetSize();
	for(ilPermit = 0; ilPermit < ilNumPermits; ilPermit++)
	{
		ropNewEmp.Permits += "|" + olPermits[ilPermit] + "|";
	}
	*/

	// get the function and permits for emps that are already assigned to this team
	CCSPtrArray <FUNC_PERMITS> olExistingEmps;
	CCSPtrArray <DEMANDDATA> olTeamDemands;
	ogDemandData.GetTeamDemands(prpDemand,olTeamDemands);
	int ilNumTeamDemands = olTeamDemands.GetSize();
	for(int ilD = 0; ilD < ilNumTeamDemands; ilD++)
	{
		DEMANDDATA *prlDem = &olTeamDemands[ilD];
		CCSPtrArray <JOBDATA> olAssignedJobs;
		ogJodData.GetJobsByDemand(olAssignedJobs,prlDem->Urno);
		int ilNumAssignedJobs = olAssignedJobs.GetSize();
		for(int ilAJ = 0; ilAJ < ilNumAssignedJobs; ilAJ++)
		{
			long llEmpUrno = olAssignedJobs[ilAJ].Ustf;
			FUNC_PERMITS *prlExistingEmp = new FUNC_PERMITS;
			olPermits.RemoveAll();
			ogSpeData.GetPermitsBySurn(llEmpUrno,olPermits);
			ilNumPermits = olPermits.GetSize();
			for(ilPermit = 0; ilPermit < ilNumPermits; ilPermit++)
			{
				prlExistingEmp->Permits += "|" + olPermits[ilPermit] + "|";
			}
			prlExistingEmp->Function = ogSpfData.GetFunctionBySurn(llEmpUrno);
//			STAFFDATA *prlEmp = ogEmpData.GetEmpByUrno(llEmpUrno);
//			if(prlEmp != NULL)
//			{
//				prlExistingEmp->Name.Format("%s",ogEmpData.GetEmpName(prlEmp));
//			}
//			else
//			{
//				prlExistingEmp->Name.Format("Error employee not found for STF.URNO <%ld>",llEmpUrno);
//			}
			olExistingEmps.Add(prlExistingEmp);
		}
	}

	// AZA contains a list of functions and permits required for all demands in the team
	AZADATA *prlAza = ogAzaData.GetAzaByUrno(prpDemand->Uaza);
	if(prlAza == NULL)
	{
		ropErrors.Add("Internal error: CCSBasicData::CheckFunctionsAndPermits() AZA event record not found for the demand!");
	}
	else
	{
		bool blMatchFound;
		int ilRequiredPermit, ilNumRequiredPermits;
		int ilExistingEmp, ilNumExistingEmps = olExistingEmps.GetSize();
		CString olRequiredPermit;

		// check if the group permit is covered
		CString olRequiredGroupPermit;
		if(strlen(prlAza->Gqua) > 0)
		{
			olRequiredGroupPermit.Format("|%s|",prlAza->Gqua);
		}
		for(ilExistingEmp = 0; olRequiredGroupPermit.GetLength() > 0 && ilExistingEmp < ilNumExistingEmps; ilExistingEmp++)
		{
			if(olExistingEmps[ilExistingEmp].Permits.Find(olRequiredGroupPermit) != -1)
			{
				olRequiredGroupPermit.Empty();
			}
		}

		// create a list of functions and permits not already covered by existing emps in the team
		CCSPtrArray <FUNCTIONANDPERMITSDATA> olRequired;
		int ilRequired, ilNumRequired = prlAza->FunctionAndPermits.GetSize();
		for(ilRequired = 0; ilRequired < ilNumRequired; ilRequired++)
		{
			CString *prlRequiredFunction = &prlAza->FunctionAndPermits[ilRequired].Function;
			CStringArray *prlRequiredPermits = &prlAza->FunctionAndPermits[ilRequired].Permits;
			ilNumRequiredPermits = prlRequiredPermits->GetSize();

			// check if one of the existing team employees has this required function and permits
			blMatchFound = false;
/*			for(int ilExistingEmp = 0; !blMatchFound && ilExistingEmp < ilNumExistingEmps; ilExistingEmp++)
			{
				//if(prlRequiredFunction->GetLength() <= 0 ||	*prlRequiredFunction == olExistingEmps[ilExistingEmp].Function)

				// stupidly the both the functions AND the permits are found in SPETAB
				CString olRequiredFunction;
				olRequiredFunction.Format("|%s|",*prlRequiredFunction);
				if(olExistingEmps[ilExistingEmp].Permits.Find(olRequiredFunction) != -1)
				{
					bool blMissingPermit = false;
					for(ilRequiredPermit = 0; !blMissingPermit && ilRequiredPermit < ilNumRequiredPermits; ilRequiredPermit++)
					{
						olRequiredPermit.Format("|%s|",prlRequiredPermits->GetAt(ilRequiredPermit));
						if(olExistingEmps[ilExistingEmp].Permits.Find(olRequiredPermit) == -1)
						{
							blMissingPermit = true;
						}
					}
					if(!blMissingPermit)
					{
						blMatchFound = true;
					}
				}
			}*/
			if(!blMatchFound)
			{
				olRequired.Add(&prlAza->FunctionAndPermits[ilRequired]);
			}
		}

		// check if the new emp has one of the remaining function/permits
		blMatchFound = ((ilNumRequired = olRequired.GetSize()) <= 0) ? true : false;
		for(ilRequired = 0; !blMatchFound && ilRequired < ilNumRequired; ilRequired++)
		{
			CString *prlRequiredFunction = &olRequired[ilRequired].Function;
			CStringArray *prlRequiredPermits = &olRequired[ilRequired].Permits;
			ilNumRequiredPermits = prlRequiredPermits->GetSize();


			// check if one of the existing team employees has this required function and permits
//
//			// stupidly the both the functions AND the permits are found in SPETAB
			CString olRequiredFunction;
//			olRequiredFunction.Format("|%s|",*prlRequiredFunction);
			if(prlRequiredFunction->GetLength()  <= 0 || *prlRequiredFunction == ropNewEmp.Function)
//			if(ropNewEmp.Permits.Find(olRequiredFunction) != -1)
			{
				bool blMissingPermit = false;
				for(ilRequiredPermit = 0; !blMissingPermit && ilRequiredPermit < ilNumRequiredPermits; ilRequiredPermit++)
				{
					olRequiredPermit.Format("|%s|",prlRequiredPermits->GetAt(ilRequiredPermit));
					if(ropNewEmp.Permits.Find(olRequiredPermit) == -1)
					{
						blMissingPermit = true;
					}
				}
				if(!blMissingPermit)
				{
					blMatchFound = true;
				}
				if(ropNewEmp.Permits.Find(olRequiredGroupPermit) != -1)
				{
					olRequiredGroupPermit.Empty();
				}
			}
		}
		CString olError;
		if(!blMatchFound)
		{
			//CString olError = "Emp requires one of the following Function/Permits";
			olError = GetString(IDS_AZAFUNCPERMIT);
			ropErrors.Add(olError);
		
			ilNumRequired = olRequired.GetSize();
			for(ilRequired = 0; ilRequired < ilNumRequired; ilRequired++)
			{
				CString olPermits;
				ilNumRequiredPermits = olRequired[ilRequired].Permits.GetSize();
				for(ilRequiredPermit = 0; ilRequiredPermit < ilNumRequiredPermits; ilRequiredPermit++)
				{
					AddElem(olPermits,olRequired[ilRequired].Permits[ilRequiredPermit]);
				}
				olError.Format("%s/%s",olRequired[ilRequired].Function,olPermits);
				ropErrors.Add(olError);
			}
		}
		if(ilNumRequired == 1 && !olRequiredGroupPermit.IsEmpty())
		{
			//Format("Team requires the following mandatory permits %s",olMissingPermits);
			CString olTmp;
			AddElem(olTmp,prlAza->Gqua);
			olError.Format(GetString(IDS_STRING61381),olTmp);
			ropErrors.Add(olError);
		}
	}


	olExistingEmps.DeleteAll();

	// return true if the emp has the correct functions and permits for this demand
	return (ropErrors.GetSize() <= 0);
}

// return a comma seperated list of functions for the employee
CString CCSBasicData::GetMultiFunctionText(long lpEmpUrno)
{
	CString olMultiFunctionText,olTmpText;
	CString olLastPrio;

	CCSPtrArray <SPFDATA> olSpfs;
	ogSpfData.GetSpfsBySurn(lpEmpUrno, olSpfs);

	int ilNumSpfs = olSpfs.GetSize();
	for(int ilSpf = 0; ilSpf < ilNumSpfs; ilSpf++)
	{
		SPFDATA *prlSpf = &olSpfs[ilSpf];

		if(!olMultiFunctionText.IsEmpty())
		{
			olMultiFunctionText += ",";
		}

		if(strcmp(prlSpf->Prio,olLastPrio))
		{
			olTmpText.Format("%d: %s",prlSpf->Priority,prlSpf->Code);
			olLastPrio = prlSpf->Prio;
		}
		else
		{
			olTmpText = prlSpf->Code;
		}
		olMultiFunctionText += olTmpText;
	}
	return olMultiFunctionText;
}

//
// SelectBestEmpDemandCombination()
//
// HungarianAlgorithm -->
// given a list of all possible PoolJob-Demand combinations
// returns the optimum combination
// ropMatchList.Upjb = pool job URNO
// ropMatchList.Udem = demand URNO
// ropMatchList.Stat = 1 -> valid combination  0 -> invalid combination
// ropMatchList.Tsta and ropMatchList.Mark are reuired internally and are not set outside the function
//
// Example: given the possible combinations:
//
// PoolJob1 - Demand1
// PoolJob1 - Demand2
// PoolJob2 - Demand1
//
// the function will return
//
// PoolJob1 - Demand1  --> Status 0
// PoolJob1 - Demand2  --> Status 1
// PoolJob2 - Demand1  --> Status 1
//
bool CCSBasicData::SelectBestEmpDemandCombination(CCSPtrArray <MATCHDATA> &ropMatchList, int ipNumDemands)
{
	CString olPjobUrnos = "";
	CString olDemUrnos = "";

	CString olTmp1;
	CString olTmp2;

	int ilDemCount = 0;
	// Initialisierung des Matcharrays Start
	for(int illc = 0; illc < ropMatchList.GetSize();illc++)
	{
		olTmp1 = ropMatchList[illc].Upjb + "*";
		olTmp2 = ropMatchList[illc].Udem + "*";
		if(olPjobUrnos.Find(olTmp1) > -1)
		{
			ropMatchList[illc].Stat = -1;
			ropMatchList[illc].Tsta = -1;
			ropMatchList[illc].Mark = 0;
		}
		else
		{
			if(olDemUrnos.Find(olTmp2) > -1)
			{
				ropMatchList[illc].Stat = -1;
				ropMatchList[illc].Tsta = -1;
				ropMatchList[illc].Mark = 1;
			}
			else
			{
				ropMatchList[illc].Stat = 1;
				ropMatchList[illc].Tsta = 1;
				ropMatchList[illc].Mark = 0;
				olPjobUrnos += olTmp1;
				olDemUrnos += olTmp2;
				ilDemCount++;
			}
		}
	} // Initialisierung des Matcharrays Ende

	if(ilDemCount < ipNumDemands)
	{
		bool blMarkFound = true;
		int ilCurrentRow = 0;
		bool blStop = false;
		bool blBreak = false;
		bool blKey1Found = false;
		bool blKey2Found = false;
		CString olAllPjobUrnos = "";
		CString olUpjp,olUdem;
		while(blMarkFound)
		{
			olAllPjobUrnos = "";
			for(illc = 0; illc < ropMatchList.GetSize();illc++)
			{
				if(ropMatchList[illc].Stat == 1)
				{
					olTmp1 = ropMatchList[illc].Upjb + "*";
					olAllPjobUrnos += olTmp1;
				}
			}
			for(illc = 0; illc < ropMatchList.GetSize();illc++)
			{
				if(olAllPjobUrnos.Find(olTmp1) > -1)
				{
					ropMatchList[illc].Mark = 0;					
				}
			}
	

			blMarkFound = false;
			for(int illc = 0; illc < ropMatchList.GetSize() && !blMarkFound;illc++)
			{
				if(ropMatchList[illc].Mark == 1)
				{
					blMarkFound = true;
					ilCurrentRow = illc;
				}
			}
			if(blMarkFound)
			{
				ropMatchList[ilCurrentRow].Mark = 0;
				ropMatchList[ilCurrentRow].Tsta = 1;
				olUpjp = ropMatchList[ilCurrentRow].Upjb;
				olUdem = ropMatchList[ilCurrentRow].Udem;

				blKey1Found = false;
				for(int illc2 = 0; illc2 < ropMatchList.GetSize() && !blKey1Found; illc2++)
				{
					if(ropMatchList[illc2].Udem == olUdem && ropMatchList[illc2].Stat == 1)
					{
						ilCurrentRow = illc2;
						ropMatchList[ilCurrentRow].Tsta = -1;
						olUpjp = ropMatchList[ilCurrentRow].Upjb;
						blKey1Found = true;
					}
				}
				if(blKey1Found)
				{
					blKey2Found = false;
					for(int illc3 = 0; illc3 < ropMatchList.GetSize() && !blKey2Found; illc3++)
					{
						if(ropMatchList[illc3].Upjb == olUpjp && ropMatchList[illc3].Stat == -1)
						{
							ilCurrentRow = illc3;
							ropMatchList[ilCurrentRow].Tsta = 1;
							olUdem = ropMatchList[ilCurrentRow].Udem;
							blKey2Found = true;
						}
					}
					if(blKey2Found)
					{
						blBreak = true;
					}
					else
						blBreak = false;
				}
				else
				{
					blBreak = true;
				}

				for(int illc4 = 0; illc4 < ropMatchList.GetSize(); illc4++)
				{
					if(blBreak)
					{
						ropMatchList[illc4].Stat = ropMatchList[illc4].Tsta;
					}
					else
					{
						ropMatchList[illc4].Tsta = ropMatchList[illc4].Stat;
					}
				}
			}
		}
	}
//FSC changes start 13.07.01
	int ilMatchCount = 0;
	for(int illc5 = 0; illc5 < ropMatchList.GetSize(); illc5++)
	{
		if(ropMatchList[illc5].Stat == 1)
		{
			ilMatchCount++;
		}
	}
	
	return(ilMatchCount == ipNumDemands)?true:false;
//FSC changes end 13.07.01


}




// make a string format "'xxxx','yyyy','zzzz'"
void CCSBasicData::AddElem(CString &ropString, CString opElem)
{
	CString olElem;
	olElem.Format("'%s'",opElem);
	if(ropString.Find(olElem) == -1)
	{
		if(!ropString.IsEmpty())
		{
			ropString += ',';
		}
		ropString += olElem;
	}
}

// make a string format "xxxx,yyyy,zzzz"
void CCSBasicData::AddElem2(CString &ropString, CString opElem)
{
	if(!ropString.IsEmpty())
	{
		ropString += ',';
	}
	ropString += opElem;
}



// get a list of functions and qualifications required for a demand for informational purposes only
void CCSBasicData::GetDemandInfo(DEMANDDATA *prpDemand, CString &ropDemandInfo, bool bpFullInfo /* = true */)
{
	if(prpDemand == NULL)
	{
		ropDemandInfo = "Internal error: CCSBasicData::GetFunctionsAndPermits() prpDemand is NULL !";
		return;
	}

	CString olNewLine("\n");
	CString olText;
	CString olTmp1, olTmp2;
	FLIGHTDATA *prlFlight;
	long llFlightUrno = 0L;

	if(bpFullInfo)
	{
		if(*prpDemand->Dety == '0') // Turnaround
		{
			//olTmp1 = "Turnaround";
			olTmp1 = GetString(IDS_STRING61382);
			llFlightUrno = prpDemand->Ouro;
		}
		else if(*prpDemand->Dety == '1') // Inbound
		{
			//olTmp1 = "Inbound";
			olTmp1 = GetString(IDS_STRING61383);
			llFlightUrno = prpDemand->Ouri;
		}
		else if(*prpDemand->Dety == '2') // Outbound
		{
			//olTmp1 = "Outbound";
			olTmp1 = GetString(IDS_STRING61384);
			llFlightUrno = prpDemand->Ouro;
		}
		else
		{
			//olTmp1 = "Demand Type Unknown!";
			olTmp1 = GetString(IDS_STRING61386);
		}
		if((prlFlight = ogFlightData.GetFlightByUrno(llFlightUrno)) != NULL)
		{
			olTmp2 = prlFlight->Fnum;
		}
		else
		{
			//olTmp2 = "Flight Not Found!";
			olTmp2 = GetString(IDS_STRING61387);
		}
		//olText.Format("Demand ID <%ld> RUD ID <%ld>\n%s Demand for Flight %s    Begin %s End %s",prpDemand->Urno,prpDemand->Urud,olTmp1,olTmp2,prpDemand->Debe.Format("%d/%H%M"),prpDemand->Deen.Format("%d/%H%M"));
		olText.Format(GetString(IDS_STRING61389),prpDemand->Urno,prpDemand->Urud,prpDemand->Alid,prpDemand->Aloc,olTmp1,olTmp2,prpDemand->Debe.Format("%d/%H%M"),prpDemand->Deen.Format("%d/%H%M"));
		ropDemandInfo += olText + olNewLine + olNewLine;

		RUDDATA *prlRud = ogRudData.GetRudByUrno(prpDemand->Urud);
		if(prlRud != NULL)
		{
			RUEDATA *prlRue = ogRueData.GetRueByUrno(prlRud->Urue);
			if(prlRue != NULL)
			{
				olText.Format("Rule Event URNO <%ld> Short Name <%s>\nLong Name <%s>\nCondition <%s>",prlRue->Urno,prlRue->Rusn,prlRue->Runa,prlRue->Evrm);
				ropDemandInfo += olText + olNewLine + olNewLine;
			}
		}
	}

	if(ogRudData.DemandIsOfType(prpDemand->Urud, PERSONNELDEMANDS))
	{
		CCSPtrArray <JOBDATA> olJobs;
		ogJodData.GetJobsByDemand(olJobs,prpDemand->Urno);
		int ilNumJobs = olJobs.GetSize();
		if(ilNumJobs <= 0)
		{
			//olText = "Demand is unassigned";
	//		olText = GetString(IDS_STRING61390);
	//		ropDemandInfo += olText + olNewLine;
		}
		else
		{
			for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
			{
				JOBDATA *prlJob = &olJobs[ilJ];
				EMPDATA *prlEmp = ogEmpData.GetEmpByUrno(prlJob->Ustf);
				if(prlEmp == NULL)
				{
					prlEmp = ogEmpData.GetEmpByPeno(prlJob->Peno);
				}
				if(prlEmp != NULL)
				{
					olTmp1 = ogEmpData.GetEmpName(prlEmp);
					CString olPermits;
					CStringArray olPermitList;
					ogSpeData.GetPermitsBySurn(prlEmp->Urno,olPermitList);
					int ilNumPermits = olPermitList.GetSize();
					for(int ilPermit = 0; ilPermit < ilNumPermits; ilPermit++)
					{
						AddElem(olPermits,olPermitList[ilPermit]);
					}
					CString olFunction = ogSpfData.GetFunctionBySurn(prlEmp->Urno, prlJob->Acfr);
					//olText.Format("Assigned to %s From %s To %s\nFunction: %s Permits: %s",olTmp1,prlJob->Acfr.Format("%d/%H%M"),prlJob->Acto.Format("%d/%H%M"),olFunction,olPermits);
					olText.Format(GetString(IDS_STRING61391),olTmp1,prlJob->Acfr.Format("%d/%H%M"),prlJob->Acto.Format("%d/%H%M"),olFunction,olPermits);
				}
				else
				{
					//olText.Format("Error Assigned to Unknown Employee! (JOBTAB.USTF = %ld)",prlJob->Ustf);
					olText.Format(GetString(33154),prlJob->Ustf);
				}
				ropDemandInfo += olText + olNewLine;
			}
		}
	//	ropDemandInfo += olNewLine;
		

		// get and check the function(s) for the demand to be assigned
		RPFDATA *prlDemandFunction = ogRpfData.GetFunctionByUrud(prpDemand->Urud);
		if(prlDemandFunction != NULL)
		{
			CString olDemandFunction = CString(prlDemandFunction->Fcco);
			if(olDemandFunction.IsEmpty())
			{
				// group of optional functions
				CCSPtrArray <SGMDATA> olSgms;
				ogSgmData.GetSgmListByUsgr(prlDemandFunction->Upfc,olSgms);
				int ilNumSgms = olSgms.GetSize();
				CString olRequiredFunctions;
				for(int ilSgm = 0; ilSgm < ilNumSgms; ilSgm++)
				{
					PFCDATA *prlPfc = ogPfcData.GetPfcByUrno(olSgms[ilSgm].Uval);
					if(prlPfc != NULL)
					{
						AddElem(olRequiredFunctions,prlPfc->Fctc);
					}
				}
				//olText.Format("Optional Functions: %s",olRequiredFunctions);
				olText.Format(GetString(IDS_STRING61392),olRequiredFunctions);
				ropDemandInfo += olText + olNewLine;
			}
			else
			{
				// single mandatory function
				//olText.Format("Mandatory Functions: '%s'",olDemandFunction);
				olText.Format(GetString(IDS_STRING61393),olDemandFunction);
				ropDemandInfo += olText + olNewLine;
			}
		}

		// get and check the permit(s) for this demand
		CString olRequiredMandatoryPermits, olRequiredOptionalPermits;
		CCSPtrArray<RPQDATA> olDemandPermits;
		// lli: need to check the permits for udgr as well
		CCSPtrArray<RPQDATA> olDemandPermits2;
		ogRpqData.GetRpqsByUrud(prpDemand->Urud,olDemandPermits);
		ogRpqData.GetRpqsByUdgr(prpDemand->Udgr,olDemandPermits2);
		olDemandPermits.Append(olDemandPermits2);
		int ilNumDemandPermits = olDemandPermits.GetSize();

		for(int ilDP = 0; ilDP < ilNumDemandPermits; ilDP++)
		{
			RPQDATA *prlDemandPermit = &olDemandPermits[ilDP];
			CString olDemandPermit = prlDemandPermit->Quco;
			if(olDemandPermit.IsEmpty())
			{
				// this is a header for a group of permits, one of which the emp must have
				CCSPtrArray <SGMDATA> olSgms;
				ogSgmData.GetSgmListByUsgr(prlDemandPermit->Uper,olSgms);
				int ilNumSgms = olSgms.GetSize();
				olRequiredOptionalPermits.Empty();
				for(int ilSgm = 0; ilSgm < ilNumSgms; ilSgm++)
				{
					PERDATA *prlPer = ogPerData.GetPerByUrno(olSgms[ilSgm].Uval);
					if(prlPer != NULL)
					{
						AddElem(olRequiredOptionalPermits,prlPer->Prmc);
					}
				}
				//olText.Format("Optional Permits: %s",olRequiredOptionalPermits);
				olText.Format(GetString(IDS_STRING61394),olRequiredOptionalPermits);
				ropDemandInfo += olText + olNewLine;
			}
			else
			{
				// not a group of permits, so this is a must-have permit
				AddElem(olRequiredMandatoryPermits,olDemandPermit);
			}
		}

		if(!olRequiredMandatoryPermits.IsEmpty())
		{
			//olText.Format("Mandatory Permits: %s",olRequiredMandatoryPermits);
			olText.Format(GetString(IDS_STRING61396),olRequiredMandatoryPermits);
			ropDemandInfo += olText + olNewLine;
		}
	}
	else if(ogRudData.DemandIsOfType(prpDemand->Urud, EQUIPMENTDEMANDS))
	{
		CCSPtrArray <EQUDATA> olEquList;
		GetEquipmentForDemand(prpDemand, olEquList);
		int ilNumEqu = olEquList.GetSize();
		ropDemandInfo += GetString(IDS_REQUIREDEQUIPMENT);
		for(int ilEqu = 0; ilEqu < ilNumEqu; ilEqu++)
		{
			EQUDATA *prlEqu = &olEquList[ilEqu];
			if(ilEqu == 0)
			{
				ropDemandInfo += CString(": ");
			}
			else
			{
				ropDemandInfo += CString("/");
			}
			ropDemandInfo += prlEqu->Enam;
		}
	}

	
	// this demand can be part of a group of demands which require a team of emps,
	// the following creates lists of permits/functions required by the team
	// get all other permits (RPQs) with the same group no.(Udgr) as this one
	if(bpFullInfo && prpDemand->Udgr != 0L)
	{
		ropDemandInfo += olNewLine;
		//olText.Format("Demand is part of team ID <%ld>",prpDemand->Udgr);
		olText.Format(GetString(IDS_STRING61397),prpDemand->Udgr);
		ropDemandInfo += olText + olNewLine;

		CStringArray olTeamRequiresAllOfThesePermits;
		CStringArray olTeamRequiresOneOfThesePermits;
		int ilNumOptionalTeamPermits, ilNumMandatoryTeamPermits;

		CCSPtrArray <RPQDATA> olRpqs;
		ogRpqData.GetRpqsByUdgr(prpDemand->Udgr,olRpqs);
		if (prpDemand->Udgr != 0)
		{
			int ilNumRpqs = olRpqs.GetSize();
			for(int ilPermit = 0; ilPermit < ilNumRpqs; ilPermit++)
			{
				RPQDATA *prlPermit = &olRpqs[ilPermit];
				CString olTeamPermit = prlPermit->Quco;
				if(olTeamPermit.IsEmpty())
				{
					// this is a header for a group of permits, one of which the team must have
					CCSPtrArray <SGMDATA> olSgms;
					ogSgmData.GetSgmListByUsgr(prlPermit->Uper,olSgms);
					int ilNumSgms = olSgms.GetSize();
					for(int ilSgm = 0; ilSgm < ilNumSgms; ilSgm++)
					{
						PERDATA *prlPer = ogPerData.GetPerByUrno(olSgms[ilSgm].Uval);
						if(prlPer != NULL)
						{
							olTeamRequiresOneOfThesePermits.Add(prlPer->Prmc);
						}
					}
				}
				else
				{
					// not a group of permits, so this is a must-have permit for the team
					olTeamRequiresAllOfThesePermits.Add(olTeamPermit);
				}
			}
		}
	
		CString olRequiredPermits;

		// check if the emp has one of the required optional permits for the team
		ilNumOptionalTeamPermits = olTeamRequiresOneOfThesePermits.GetSize();
		if(ilNumOptionalTeamPermits > 0)
		{
			olRequiredPermits.Empty();
			for(int ilPermit = 0; ilPermit < ilNumOptionalTeamPermits; ilPermit++)
			{
				AddElem(olRequiredPermits,olTeamRequiresOneOfThesePermits[ilPermit]);
			}
			//olText.Format("Optional Team Permits %s",olRequiredPermits);
			olText.Format(GetString(IDS_STRING61517),olRequiredPermits);
			ropDemandInfo += olText + olNewLine;
		}
		// check if the emp has all of the required mandatory permits for the team
		ilNumMandatoryTeamPermits = olTeamRequiresAllOfThesePermits.GetSize();
		if(ilNumMandatoryTeamPermits > 0)
		{
			olRequiredPermits.Empty();
			for(int ilPermit = 0; ilPermit < ilNumMandatoryTeamPermits; ilPermit++)
			{
				AddElem(olRequiredPermits,olTeamRequiresAllOfThesePermits[ilPermit]);
			}
			//olText.Format("Mandatory Team Permits %s",olRequiredPermits);
			olText.Format(GetString(IDS_STRING61580),olRequiredPermits);
			ropDemandInfo += olText + olNewLine;
		}

	} // end team checking -> if(prlRud->Udgr != 0L)
}

// bpInfoOnly - if true, then ropErrors contains a list of which functions and permits are required by the group and which are already satisfied
void CCSBasicData::GetDemandInfoAzatab(DEMANDDATA *prpDemand, CString &ropDemandInfo)
{
	CStringArray olPermits;
	CString olFunction;
	int ilNumPermits, ilPermit;
	CString olMsg;
	CString olNewLine("\n");

//	// get the function and permits for the new emp to be assigned
//	FUNC_PERMITS olNewEmp;
//	GetPoolJobFunctionsAndPermits(prpPoolJob, olNewEmp.Function, olPermits);
//	ilNumPermits = olPermits.GetSize();
//	for(ilPermit = 0; ilPermit < ilNumPermits; ilPermit++)
//	{
//		olNewEmp.Permits += "|" + olPermits[ilPermit] + "|";
//	}

	// get the function and permits for emps that are already assigned to this team
	CCSPtrArray <FUNC_PERMITS> olExistingEmps;
	CCSPtrArray <DEMANDDATA> olTeamDemands;
	ogDemandData.GetTeamDemands(prpDemand,olTeamDemands);
	int ilNumTeamDemands = olTeamDemands.GetSize();
	for(int ilD = 0; ilD < ilNumTeamDemands; ilD++)
	{
		DEMANDDATA *prlDem = &olTeamDemands[ilD];
		CCSPtrArray <JOBDATA> olAssignedJobs;
		ogJodData.GetJobsByDemand(olAssignedJobs,prlDem->Urno);
		int ilNumAssignedJobs = olAssignedJobs.GetSize();
		for(int ilAJ = 0; ilAJ < ilNumAssignedJobs; ilAJ++)
		{
			long llEmpUrno = olAssignedJobs[ilAJ].Ustf;
			FUNC_PERMITS *prlExistingEmp = new FUNC_PERMITS;
			olPermits.RemoveAll();
			ogSpeData.GetPermitsBySurn(llEmpUrno,olPermits);
			ilNumPermits = olPermits.GetSize();
			for(ilPermit = 0; ilPermit < ilNumPermits; ilPermit++)
			{
				prlExistingEmp->Permits += "|" + olPermits[ilPermit] + "|";
			}
			prlExistingEmp->Function = ogSpfData.GetFunctionBySurn(llEmpUrno);
			olExistingEmps.Add(prlExistingEmp);
		}
	}

	// AZA contains a list of functions and permits required for all demands in the team
	AZADATA *prlAza = ogAzaData.GetAzaByUrno(prpDemand->Udgr);
	if(prlAza == NULL)
	{
		ropDemandInfo += "Internal error: CCSBasicData::CheckFunctionsAndPermits() AZA event record not found for the demand!" + olNewLine;
	}
	else
	{
		bool blMatchFound;
		int ilRequiredPermit, ilNumRequiredPermits;
		int ilExistingEmp, ilNumExistingEmps = olExistingEmps.GetSize();
		CString olRequiredPermit;

		// check if the group permit is covered
		CString olRequiredGroupPermit;
		if(strlen(prlAza->Gqua) > 0)
		{
			olRequiredGroupPermit.Format("|%s|",prlAza->Gqua);
		}
		for(ilExistingEmp = 0; olRequiredGroupPermit.GetLength() > 0 && ilExistingEmp < ilNumExistingEmps; ilExistingEmp++)
		{
			if(olExistingEmps[ilExistingEmp].Permits.Find(olRequiredGroupPermit) != -1)
			{
				olRequiredGroupPermit.Empty();
			}
		}

		// create a list of functions and permits not already covered by existing emps in the team
		CCSPtrArray <FUNCTIONANDPERMITSDATA> olRequired;
		int ilRequired, ilNumRequired = prlAza->FunctionAndPermits.GetSize();
		for(ilRequired = 0; ilRequired < ilNumRequired; ilRequired++)
		{
			CString *prlRequiredFunction = &prlAza->FunctionAndPermits[ilRequired].Function;
			CStringArray *prlRequiredPermits = &prlAza->FunctionAndPermits[ilRequired].Permits;
			ilNumRequiredPermits = prlRequiredPermits->GetSize();

			// check if one of the existing team employees has this required function and permits
			blMatchFound = false;
			for(int ilExistingEmp = 0; !blMatchFound && ilExistingEmp < ilNumExistingEmps; ilExistingEmp++)
			{
				if(prlRequiredFunction->GetLength() <= 0 ||
					*prlRequiredFunction == olExistingEmps[ilExistingEmp].Function)
				{
					bool blMissingPermit = false;
					for(ilRequiredPermit = 0; !blMissingPermit && ilRequiredPermit < ilNumRequiredPermits; ilRequiredPermit++)
					{
						olRequiredPermit.Format("|%s|",prlRequiredPermits->GetAt(ilRequiredPermit));
						if(olExistingEmps[ilExistingEmp].Permits.Find(olRequiredPermit) == -1)
						{
							blMissingPermit = true;
						}
					}
					if(!blMissingPermit)
					{
						blMatchFound = true;
					}
				}
			}
			if(!blMatchFound)
			{
				olRequired.Add(&prlAza->FunctionAndPermits[ilRequired]);
			}
		}

//		// check if the new emp has one of the remaining function/permits
//		blMatchFound = ((ilNumRequired = olRequired.GetSize()) <= 0) ? true : false;
//		for(ilRequired = 0; !blMatchFound && ilRequired < ilNumRequired; ilRequired++)
//		{
//			CString *prlRequiredFunction = &olRequired[ilRequired].Function;
//			CStringArray *prlRequiredPermits = &olRequired[ilRequired].Permits;
//			ilNumRequiredPermits = prlRequiredPermits->GetSize();
//
//			// check if one of the existing team employees has this required function and permits
//			if(prlRequiredFunction->GetLength() || *prlRequiredFunction == olNewEmp.Function)
//			{
//				bool blMissingPermit = false;
//				for(ilRequiredPermit = 0; !blMissingPermit && ilRequiredPermit < ilNumRequiredPermits; ilRequiredPermit++)
//				{
//					olRequiredPermit.Format("|%s|",prlRequiredPermits->GetAt(ilRequiredPermit));
//					if(olNewEmp.Permits.Find(olRequiredPermit) == -1)
//					{
//						blMissingPermit = true;
//					}
//				}
//				if(!blMissingPermit)
//				{
//					blMatchFound = true;
//				}
//				if(olNewEmp.Permits.Find(olRequiredGroupPermit) != -1)
//				{
//					olRequiredGroupPermit.Empty();
//				}
//			}
//		}
		CString olError;
//		if(!blMatchFound)
//		{
			//CString olError = "Emp requires one of the following Function/Permits";
			olError = GetString(IDS_AZAFUNCPERMIT);
			ropDemandInfo += olError + olNewLine;
		
			ilNumRequired = olRequired.GetSize();
			for(ilRequired = 0; ilRequired < ilNumRequired; ilRequired++)
			{
				CString olPermits;
				ilNumRequiredPermits = olRequired[ilRequired].Permits.GetSize();
				for(ilRequiredPermit = 0; ilRequiredPermit < ilNumRequiredPermits; ilRequiredPermit++)
				{
					AddElem(olPermits,olRequired[ilRequired].Permits[ilRequiredPermit]);
				}
				olError.Format("%s/%s",olRequired[ilRequired].Function,olPermits);
				ropDemandInfo += olError + olNewLine;
			}
//		}
		if(ilNumRequired == 1 && !olRequiredGroupPermit.IsEmpty())
		{
			//Format("Team requires the following mandatory permits %s",olMissingPermits);
			CString olTmp;
			AddElem(olTmp,prlAza->Gqua);
			olError.Format(GetString(IDS_STRING61381),olTmp);
			ropDemandInfo += olError + olNewLine;
		}
	}


	olExistingEmps.DeleteAll();

	// return true if the emp has the correct functions and permits for this demand
}

void CCSBasicData::GetJobInfo(JOBDATA *prpJob, CString &ropJobInfo)
{
	if(prpJob != NULL)
	{
		JOBDATA *prlPoolJob = prpJob;

		if(strcmp(prpJob->Jtco,JOBPOOL))
		{
			prlPoolJob = ogJobData.GetJobByUrno(prpJob->Jour);
		}
		if(prlPoolJob != NULL)
		{
			CString olPermits, olFunctions;
			CStringArray olPermitList, olFunctionList;
			GetFunctionsForPoolJob(prlPoolJob, olFunctionList);
			GetPermitsForPoolJob(prlPoolJob, olPermitList);
			int ilNumPermits = olPermitList.GetSize();
			for(int ilPermit = 0; ilPermit < ilNumPermits; ilPermit++)
			{
				AddElem(olPermits,olPermitList[ilPermit]);
			}
			int ilNumFunctions = olFunctionList.GetSize();
			for(int ilFunction = 0; ilFunction < ilNumFunctions; ilFunction++)
			{
				AddElem(olFunctions,olFunctionList[ilFunction]);
			}
			// "Functions: %s Permits: %s"
			ropJobInfo.Format(GetString(IDS_JOBINFO),olFunctions,olPermits);
		}
	}
}


CString CCSBasicData::GetDemandBarText(DEMANDDATA *prpDemand, bool bpDisplayGroupMembers /*= false*/)
{
	CString olDemandBarText;

	if(ogRudData.DemandIsOfType(prpDemand->Urud, PERSONNELDEMANDS))
	{
		// counter class 
		if (strcmp(prpDemand->Aloc,ALLOCUNITTYPE_CIC) == 0)
		{
			RUDDATA *prlRud = ogRudData.GetRudByUrno(prpDemand->Urud);
			if (prlRud)
			{
				CCCDATA *prlCcc = ogCccData.GetCccByUrno(prlRud->Vref);
				if (prlCcc)
				{
					olDemandBarText += prlCcc->Cicc;
					olDemandBarText +=                                                                                                                                                                                                            ": ";
				}
			}
		}
		
		// function
		AZADATA *prlAza = ogAzaData.GetAzaByUrno(prpDemand->Udgr);
		if(prlAza != NULL)
		{
			olDemandBarText += prlAza->Func + CString("/") + prlAza->Squa;
		}
		else
		{
			RPFDATA *prlDemandFunction = ogRpfData.GetFunctionByUrud(prpDemand->Urud);
			if(prlDemandFunction != NULL)
			{
				if(!strcmp(prlDemandFunction->Fcco,""))
				{
					if(bpDisplayGroupMembers)
					{
						// group of optional functions
						CCSPtrArray <SGMDATA> olSgms;
						ogSgmData.GetSgmListByUsgr(prlDemandFunction->Upfc,olSgms);
						int ilNumSgms = olSgms.GetSize();
						CString olRequiredFunctions;
						for(int ilSgm = 0; ilSgm < ilNumSgms; ilSgm++)
						{
							PFCDATA *prlPfc = ogPfcData.GetPfcByUrno(olSgms[ilSgm].Uval);
							if(prlPfc != NULL)
							{
								AddElem2(olRequiredFunctions,prlPfc->Fctc);
							}
						}
						olDemandBarText += olRequiredFunctions;	
					}
					else
					{
						SGRDATA *prlSgr = ogSgrData.GetSgrByUrno(prlDemandFunction->Upfc);
						if(prlSgr != NULL)
						{
							olDemandBarText += CString("*") + prlSgr->Grpn;
						}
					}
				}
				else
				{
					olDemandBarText += prlDemandFunction->Fcco;
				}
			}

			olDemandBarText += " (";
			CCSPtrArray<RPQDATA> olDemandPermits;
			// lli: need to check the permits for udgr as well
			CCSPtrArray<RPQDATA> olDemandPermits2;
			ogRpqData.GetRpqsByUrud(prpDemand->Urud,olDemandPermits);
			ogRpqData.GetRpqsByUdgr(prpDemand->Udgr,olDemandPermits2);
			olDemandPermits.Append(olDemandPermits2);
			int ilNumDemandPermits = olDemandPermits.GetSize();
			for(int ilDP = 0; ilDP < ilNumDemandPermits; ilDP++)
			{
				if(ilDP > 0)
				{
					olDemandBarText += ",";
				}

				RPQDATA *prlDemandPermit = &olDemandPermits[ilDP];
				if(!strcmp(prlDemandPermit->Quco,""))
				{
					// this is a header for a group of permits, one of which the emp must have
					CCSPtrArray <SGMDATA> olSgms;
					ogSgmData.GetSgmListByUsgr(prlDemandPermit->Uper,olSgms);
					int ilNumSgms = olSgms.GetSize();
					CString olRequiredOptionalPermits;
					for(int ilSgm = 0; ilSgm < ilNumSgms; ilSgm++)
					{
						PERDATA *prlPer = ogPerData.GetPerByUrno(olSgms[ilSgm].Uval);
						if(prlPer != NULL)
						{
							AddElem2(olRequiredOptionalPermits,prlPer->Prmc);
						}
					}
					olDemandBarText += olRequiredOptionalPermits;
				}
				else
				{
					// single permit
					olDemandBarText += prlDemandPermit->Quco;
				}
			}
			olDemandBarText += ")";
		}
	}
	else if(ogRudData.DemandIsOfType(prpDemand->Urud, EQUIPMENTDEMANDS))
	{
		REQDATA *prlReq = ogReqData.GetReqByUrud(prpDemand->Urud);
		if(prlReq != NULL)
		{
			EQUDATA *prlEqu = NULL;

			if(strlen(prlReq->Eqco) > 0)
			{
				// single piece of equipment required for this demand
				if((prlEqu = ogEquData.GetEquByUrno(prlReq->Uequ)) != NULL)
				{
					olDemandBarText += prlEqu->Enam;
				}
			}
			else
			{
				SGRDATA *prlSgr = ogSgrData.GetSgrByUrno(prlReq->Uequ);
				if(prlSgr != NULL)
				{
					olDemandBarText = prlSgr->Grpn;
				}
			}
		}
	}

	if (bgIsPrm && prpDemand->Uprm != 0L)
	{
		DPXDATA *prlDpx = ogDpxData.GetDpxByUrno(prpDemand->Uprm);
		if (prlDpx != NULL)
		{
			olDemandBarText.Format("%s",prlDpx->Name);
		}
	}
	return olDemandBarText;
}

CString CCSBasicData::GetDemandFunctionsString(DEMANDDATA *prpDemand, char cpSeperator /* = ','*/)
{
	CString olFunctionsString;
	CStringArray olFunctions;
	GetDemandFunctions(prpDemand, olFunctions);
	int ilNumFunctions = olFunctions.GetSize();
	for(int ilF = 0; ilF < ilNumFunctions; ilF++)
	{
		olFunctionsString += (ilF == 0) ? olFunctions[ilF] : (CString(cpSeperator) + olFunctions[ilF]);
	}

	return olFunctionsString;
}

int CCSBasicData::GetDemandFunctions(DEMANDDATA *prpDemand, CStringArray &ropFunctions, bool bpReset /* = true */)
{
	if(prpDemand != NULL)
	{
		if(bpReset)
		{
			ropFunctions.RemoveAll();
		}

		// function
		AZADATA *prlAza = ogAzaData.GetAzaByUrno(prpDemand->Udgr);
		if(prlAza != NULL)
		{
			CString olFunction = prlAza->Func + CString("/") + prlAza->Squa;
			ropFunctions.Add(olFunction);
		}
		else
		{
			RPFDATA *prlDemandFunction = ogRpfData.GetFunctionByUrud(prpDemand->Urud);
			if(prlDemandFunction != NULL)
			{
				if(!strcmp(prlDemandFunction->Fcco,""))
				{
					// group of optional functions
					CCSPtrArray <SGMDATA> olSgms;
					ogSgmData.GetSgmListByUsgr(prlDemandFunction->Upfc,olSgms);
					int ilNumSgms = olSgms.GetSize();
					for(int ilSgm = 0; ilSgm < ilNumSgms; ilSgm++)
					{
						PFCDATA *prlPfc = ogPfcData.GetPfcByUrno(olSgms[ilSgm].Uval);
						if(prlPfc != NULL)
						{
							ropFunctions.Add(prlPfc->Fctc);
						}
					}
				}
				else
				{
					ropFunctions.Add(prlDemandFunction->Fcco);
				}
			}
		}
	}

	return ropFunctions.GetSize();
}

CString CCSBasicData::GetDemandPermitsString(DEMANDDATA *prpDemand, char cpSeperator /* = ','*/)
{
	CString olPermitsString;
	CStringArray olPermits;
	GetDemandPermits(prpDemand, olPermits);
	int ilNumPermits = olPermits.GetSize();
	for(int ilP = 0; ilP < ilNumPermits; ilP++)
	{
		olPermitsString += (ilP == 0) ? olPermits[ilP] : (CString(cpSeperator) + olPermits[ilP]);
	}

	return olPermitsString;
}

int CCSBasicData::GetDemandPermits(DEMANDDATA *prpDemand, CStringArray &ropPermits, bool bpReset /* = true */)
{
	if(prpDemand != NULL)
	{
		if(bpReset)
		{
			ropPermits.RemoveAll();
		}

		CCSPtrArray<RPQDATA> olDemandPermits;
		// lli: need to check the permits for udgr as well
		CCSPtrArray<RPQDATA> olDemandPermits2;
		ogRpqData.GetRpqsByUrud(prpDemand->Urud,olDemandPermits);
		ogRpqData.GetRpqsByUdgr(prpDemand->Udgr,olDemandPermits2);
		olDemandPermits.Append(olDemandPermits2);
		int ilNumDemandPermits = olDemandPermits.GetSize();
		for(int ilDP = 0; ilDP < ilNumDemandPermits; ilDP++)
		{
			RPQDATA *prlDemandPermit = &olDemandPermits[ilDP];
			if(!strcmp(prlDemandPermit->Quco,""))
			{
				// this is a header for a group of permits, one of which the emp must have
				CCSPtrArray <SGMDATA> olSgms;
				ogSgmData.GetSgmListByUsgr(prlDemandPermit->Uper,olSgms);
				int ilNumSgms = olSgms.GetSize();
				for(int ilSgm = 0; ilSgm < ilNumSgms; ilSgm++)
				{
					PERDATA *prlPer = ogPerData.GetPerByUrno(olSgms[ilSgm].Uval);
					if(prlPer != NULL)
					{
						ropPermits.Add(prlPer->Prmc);
					}
				}
			}
			else
			{
				// single permit
				ropPermits.Add(prlDemandPermit->Quco);
			}
		}
	}

	return ropPermits.GetSize();
}

CString CCSBasicData::GetFmJobTypeByAloc(CString opAloc)
{
	CString olJobType;
	if(opAloc == ALLOCUNITTYPE_GATEGROUP || opAloc == ALLOCUNITTYPE_GATE)
	{
		olJobType = JOBFMGATEAREA;
	}
	else if(opAloc == ALLOCUNITTYPE_CICGROUP || opAloc == ALLOCUNITTYPE_CIC)
	{
		olJobType = JOBFMCCIAREA;
	}
	else if(opAloc == ALLOCUNITTYPE_REGNGROUP || opAloc == ALLOCUNITTYPE_REGN)
	{
		olJobType = JOBFMREGNAREA;
	}
	else if(opAloc == ALLOCUNITTYPE_PSTGROUP || opAloc == ALLOCUNITTYPE_PST)
	{
		olJobType = JOBFMPSTAREA;
	}
	else
	{
		olJobType = JOBFMGATEAREA;
	}
	return olJobType;
}

CString CCSBasicData::GetFlightAlidByAloc(long lpFlightUrno, CString opAloc)
{
	return GetFlightAlidByAloc(ogFlightData.GetFlightByUrno(lpFlightUrno), opAloc);
}

CString CCSBasicData::GetFlightAlidByAloc(FLIGHTDATA *prpFlight, CString opAloc)
{
	CString olFlightAlid;
	if(prpFlight != NULL)
	{
		if(opAloc == ALLOCUNITTYPE_GATEGROUP || opAloc == ALLOCUNITTYPE_GATE)
		{
			olFlightAlid = ogFlightData.GetGate(prpFlight);
		}
		else if(opAloc == ALLOCUNITTYPE_REGNGROUP || opAloc == ALLOCUNITTYPE_REGN)
		{
			olFlightAlid = prpFlight->Regn;
		}
		else if(opAloc == ALLOCUNITTYPE_PSTGROUP || opAloc == ALLOCUNITTYPE_PST)
		{
			olFlightAlid = ogFlightData.GetPosition(prpFlight);
		}
	}
	return olFlightAlid;
}


void CCSBasicData::SetTimeframe(CTime opTimeframeStart, CTime opTimeframeEnd)
{
	omTimeframeStart = opTimeframeStart;
	omTimeframeEnd = opTimeframeEnd;
	ogBasicData.Trace("Start Timeframe %s End Timeframe %s (Local)\n",omTimeframeStart.Format("%Y%m%d%H%M%S"),omTimeframeEnd.Format("%Y%m%d%H%M%S"));
	InitDisplayDate();
	InitShiftDate();
}

CTime CCSBasicData::GetTimeframeStart(void)
{
	return omTimeframeStart;
}

CTime CCSBasicData::GetTimeframeEnd(void)
{
	return omTimeframeEnd;
}

// return "From dd.mm.yyyy hh:mm   To dd.mm.yyyy hh:mm"
CString CCSBasicData::GetTimeframeString(void)
{
	CString olText;
	olText.Format(GetString(IDS_FROM_TO),omTimeframeStart.Format("%d.%m.%Y %H:%M"),omTimeframeEnd.Format("%d.%m.%Y %H:%M"));
	return olText;
}

CTime CCSBasicData::GetTimeframeStartUtc(void)
{
	return GetUtcFromLocal(omTimeframeStart);
}

CTime CCSBasicData::GetTimeframeEndUtc(void)
{
	return GetUtcFromLocal(omTimeframeEnd);
}

// return a list of all dates within the timeframe, format "dd.mm.yyyy"
void CCSBasicData::GetTimeframeList(CStringArray &ropTimeframeList)
{
	CTime olTimeframeEnd = GetTimeframeEnd();
	CTime olTimeframeStart = GetTimeframeStart();
	CTime olDay = CTime(olTimeframeStart.GetYear(),olTimeframeStart.GetMonth(),olTimeframeStart.GetDay(),0,0,0);
	CTimeSpan olOneDay(1,0,0,0);

	while(olDay < olTimeframeEnd)
	{
		ropTimeframeList.Add(olDay.Format("%d.%m.%Y"));
		olDay += olOneDay;
	}
}

// display date = either currentDate or if currentDate outside timeframe then the first day in the timeframe
void CCSBasicData::InitDisplayDate()
{
	CTime olCurrTime = GetTime();
	if(olCurrTime >= GetTimeframeStart() && olCurrTime <= GetTimeframeEnd())
	{
		SetDisplayDate(olCurrTime);
	}
	else
	{
		SetDisplayDate(omTimeframeStart);
	}
}

// sets the day to be displayed in tables and gantts as a CTime
void CCSBasicData::SetDisplayDate(CTime opDisplayDate)
{
	omDisplayDate = opDisplayDate;
	// sending this message results in all other gantts/tables being synchronised
	ogCCSDdx.DataChanged((void *)this,GLOBAL_DATE_UPDATE,(void *)&omDisplayDate);
}

// returns the day to be displayed in tables and gantts as an offset in days from omTimeframeStart
int CCSBasicData::GetDisplayDateOffset()
{
	return GetDisplayDateOffsetByDate(GetDisplayDate());
}

// returns the day to be displayed in tables and gantts as a CTime
CTime CCSBasicData::GetDisplayDate()
{
	return omDisplayDate;
}

// Gantt chart start time = StartTimeFrame - omDisplayOffset
CTimeSpan CCSBasicData::GetDisplayOffset()
{
	return omDisplayOffset;
}

// given a date return the offset in days from timeframe start
int CCSBasicData::GetDisplayDateOffsetByDate(CTime opDate)
{
	int ilDisplayDateOffset = -1;
	if(opDate != TIMENULL)
	{
		CTimeSpan olDiff = opDate - GetTimeframeStart();
		ilDisplayDateOffset = olDiff.GetDays();
	}
	return ilDisplayDateOffset;
}

// given ipOffset (offset in days from timeframe start) return a CTime
CTime CCSBasicData::GetDateByDisplayDateOffset(int ipOffset)
{
	return GetTimeframeStart() + CTimeSpan(ipOffset,0,0,0);
}

CTime CCSBasicData::ZeroHHMMSS(CTime opDate)
{
	return CTime(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),0,0,0);
}

// given a date set the global display date - ie set the display date of all gantts and tables
void CCSBasicData::SynchronizeDisplayDates(CTime opDay)
{
	CTime olDay = ZeroHHMMSS(opDay); // display gantt charts starting at 00:00
	CTime olCurrTime = GetTime();
	if(olCurrTime.GetYear() == olDay.GetYear() && olCurrTime.GetMonth() == olDay.GetMonth() && olCurrTime.GetDay() == olDay.GetDay())
	{
		olDay = olCurrTime - CTimeSpan(0,3,0,0);
	}

	SetDisplayDate(olDay);
}

long CCSBasicData::GetUrueForDemand(DEMANDDATA *prpDem)
{
	long lpUrue = 0L;
	if(prpDem != NULL)
	{
		RUDDATA *prlRud = ogRudData.GetRudByUrno(prpDem->Urud);
		if(prlRud != NULL)
		{
			lpUrue = prlRud->Urue;
		}
	}
	return lpUrue;
}

bool CCSBasicData::IsFinished(FLIGHTDATA *prpFlight, const char *pcpAloc /*= ""*/, int ipDemType /*= ALLDEMANDS*/, int ipReturnFlightType /*= ID_NOT_RETURNFLIGHT*/)
{
	bool blIsFinished = false;

	if(prpFlight != NULL)
	{
		if(ogFlightData.IsArrival(prpFlight) || ipReturnFlightType == ID_ARR_RETURNFLIGHT)
		{
			if(prpFlight->Onbl != TIMENULL)
			{
				blIsFinished = true;
			}
			else
			{
				FLIGHTDATA *prlRotation = ogFlightData.GetRotationFlight(prpFlight, ipReturnFlightType);
				if(prlRotation != NULL && prlRotation->Ofbl != TIMENULL)
				{
					blIsFinished = true;
				}
			}
		}
		else
		{
			if(prpFlight->Ofbl != TIMENULL)
			{
				blIsFinished = true;
			}
		}
		if(blIsFinished)
		{
			CCSPtrArray <JOBDATA> olJobs;
			ogJobData.GetJobsByFlur(olJobs, prpFlight->Urno, TRUE, "", pcpAloc, ipDemType, false, ipReturnFlightType);
			int ilNumJobs = olJobs.GetSize();
			for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
			{
				if(*olJobs[ilJob].Stat != 'F')
				{
					blIsFinished = false;
					break;
				}
			}
		}
		if(blIsFinished)
		{
			CCSPtrArray <DEMANDDATA> olDemands;
			ogDemandData.GetDemandsByFlur(olDemands, prpFlight->Urno, "", pcpAloc, ipDemType, ipReturnFlightType);
			int ilNumDemands = olDemands.GetSize();
			CTime olCurrTime = ogBasicData.GetTime();
			for(int ilD = 0; ilD < ilNumDemands; ilD++)
			{
				if(olDemands[ilD].Deen > olCurrTime && !ogJodData.DemandHasJobs(olDemands[ilD].Urno))
				{
					blIsFinished = false;
					break;
				}
			}
		}
	}

	return blIsFinished;
}

void CCSBasicData::AddInfo(CString opInfoString)
{
	omInfo.Add(opInfoString);
}


void CCSBasicData::LogCedaError(const char *pcpText, const char *pcpLastErrorMessage, const char *pcpCommand, const char *pcpFields, const char *pcpTable, const char *pcpWhere)
{
	if(stricmp(pcpLastErrorMessage,"No Data Found"))
	{
		CString olMsg;
		CString olErr = "Error accessing the database, please contact your system administrator with following information:\n";

		olMsg.Format("Start %s **********************",pcpText);
		ogBasicData.Trace("%s\n",olMsg);
		AddInfo(olMsg);
		olErr += "\n" + olMsg;

		olMsg.Format("omLastErrorMessage = <%s>",pcpLastErrorMessage);
		ogBasicData.Trace("%s\n",olMsg);
		AddInfo(olMsg);
		olErr += "\n" + olMsg;

		olMsg.Format("Command = <%s>",pcpCommand);
		ogBasicData.Trace("%s\n",olMsg);
		AddInfo(olMsg);
		olErr += "\n" + olMsg;

		olMsg.Format("Fields  = <%s>",pcpFields);
		ogBasicData.Trace("%s\n",olMsg);
		AddInfo(olMsg);
		olErr += "\n" + olMsg;

		olMsg.Format("Table   = <%s>",pcpTable);
		ogBasicData.Trace("%s\n",olMsg);
		AddInfo(olMsg);
		olErr += "\n" + olMsg;

		olMsg.Format("Where   = <%s>",pcpWhere);
		ogBasicData.Trace("%s\n",olMsg);
		AddInfo(olMsg);
		olErr += "\n" + olMsg;

		olMsg.Format("End %s ************************",pcpText);
		ogBasicData.Trace("%s\n",olMsg);
		AddInfo(olMsg);
		olErr += "\n" + olMsg;

		MessageBox(NULL,olErr,pcgAppName,MB_OK);
	}
}

CString CCSBasicData::GetConfigFileName(void)
{
	if(omConfigFileName.IsEmpty())
	{
		if(getenv("CEDA") == NULL)
		{
			omConfigFileName = "C:\\UFIS\\SYSTEM\\CEDA.INI";
		}
		else
		{
			omConfigFileName = getenv("CEDA");
		}
	}

	return omConfigFileName;
}

bool CCSBasicData::DisplayDebugInfo(void)
{
	bool blDisplayDebugInfo = false;
	char pclTmpText[512];
	char pclKeyword[521];

	CString olConfigFileName = GetConfigFileName();

	strcpy(pclKeyword,"DISPLAY_DEBUG_INFO");
    GetPrivateProfileString(pcgAppName, pclKeyword, "NO",pclTmpText, sizeof pclTmpText, olConfigFileName);
	if(!stricmp(pclTmpText,"YES"))
	{
		blDisplayDebugInfo = true;
	}
	return blDisplayDebugInfo;
}

void CCSBasicData::LogBroadcast(struct BcStruct *prpBcData)
{
	if(prpBcData != NULL)
	{
		ogBasicData.Trace("--> BROADCAST RECEIVED Tab <%s> Cmd <%s> Sel <%s>\n",prpBcData->Object,prpBcData->Cmd,prpBcData->Selection);
		ogBasicData.Trace("--> Fields <%s>\n",prpBcData->Fields);
		ogBasicData.Trace("--> Data <%s>\n",prpBcData->Data);
		if(!prpBcData->Attachment.IsEmpty())
			ogBasicData.Trace("--> Attachment <%s>",prpBcData->Attachment);
	}
}

void CCSBasicData::LogCedaAction(char *pcpTable, char *pcpCmd, char *pcpSelection, char *pcpFields, char *pcpData)
{
	ogBasicData.Trace("<-- DATABASE ACTION Tab <%s> Cmd <%s> Sel <%s>",
		pcpTable != NULL ? pcpTable : "",pcpCmd != NULL ? pcpCmd : "",pcpSelection != NULL ? pcpSelection : "");
	ogBasicData.Trace("<-- Fields <%s>",pcpFields != NULL ? pcpFields : "");
	ogBasicData.Trace("<-- Data <%s>",pcpData != NULL ? pcpData : "");
}

// if a new rule is found for a flight that has demands with jobs - 
// 1. new demands will be created for the new rule
// 2. old demands without jobs assigned will be deleted
// 3. old demands with jobs will be in conflict - "Job has expired demand"
bool CCSBasicData::JobHasExpiredDemand(long lpJobu)
{
	return ogDemandData.HasExpired(ogJodData.GetDemandForJob(lpJobu));
}

// omTablesNotToLoad contains a comma seperated list of tables not to be loaded
bool CCSBasicData::DontLoadTable(CString &ropTableName)
{
	return (ropTableName.IsEmpty() || omTablesNotToLoad.Find(ropTableName) == -1) ? false : true;
}


CString CCSBasicData::GetVersionString(void)
{
	VersionInfo rlInfo;
//	VersionInfo::GetVersionInfo(AfxGetInstanceHandle(),rlInfo);
	VersionInfo::GetVersionInfo(NULL,rlInfo);	// must use NULL (and it works), because AFX is not initialized yet and will cause an ASSERT

	CString olVersionString;
	olVersionString.Format("Version: %s %s  Compiled: %s",rlInfo.omFileVersion,rlInfo.omPrivateBuild,__DATE__);
	return olVersionString;
}

CString CCSBasicData::GetVersion(void)
{
	VersionInfo info;
	VersionInfo::GetVersionInfo(AfxGetInstanceHandle(),info);
	return info.omFileVersion;
}

CString CCSBasicData::GetUfisVersion(void)
{
	VersionInfo info;
	VersionInfo::GetVersionInfo(AfxGetInstanceHandle(),info);
	return info.omProductVersion;
}

CString CCSBasicData::GetSpecialBuild(void)
{
	VersionInfo info;
	VersionInfo::GetVersionInfo(AfxGetInstanceHandle(),info);
	return info.omPrivateBuild;
}

CString CCSBasicData::GetDemandAlid(DEMANDDATA *prpDemand)
{
	CString olAlid;
	if(prpDemand != NULL)
	{
		if(!strcmp(prpDemand->Obty,ACRURNO))
		{
			ACRDATA *prlAcr = ogAcrData.GetAcrByUrno(prpDemand->Uref);
			if(prlAcr != NULL)
			{
				olAlid = prlAcr->Regn;
			}
		}
		else
		{
			olAlid = CString(prpDemand->Alid);
		}
	}
	return olAlid;
}


void CCSBasicData::DebugInfoMsgBox(CString &ropMsg)
{
	CDebugInfoMsgBox olDebugInfoMsgBox;
	int ilMsgLen = ropMsg.GetLength();
	for(int ilC = 0; ilC < ilMsgLen; ilC++)
	{
		if(ropMsg[ilC] == '\n')
		{
			olDebugInfoMsgBox.m_MsgBox += "\r\n";
		}
		else
		{
			olDebugInfoMsgBox.m_MsgBox += ropMsg[ilC];
		}
	}
	olDebugInfoMsgBox.DoModal();
}

// check foreign keys in DB records
void CCSBasicData::CheckDatabaseIntegrity(void)
{
	// CCATAB
	int ilNumCca = ogCcaData.omData.GetSize();
	for(int ilCca = 0; ilCca < ilNumCca; ilCca++)
	{
		CCADATA *prlCca = &ogCcaData.omData[ilCca];
		ASSERT(prlCca);
		if (prlCca->IsDedicatedCheckin())
		{
			DEMANDDATA *prlLocationDemand = ogDemandData.GetDemandByUrno(prlCca->Udem);
			if(prlLocationDemand == NULL)
			{
				//ogBasicData.Trace("CCA record has invalid pointer to a demand! COUNTER <%s> FROM <%s> TO <%s> CCA.URNO <%ld> DEM.URNO <%ld>\n",prlCca->Cnam,prlCca->Ckbs.Format("%H:%M/%d"),prlCca->Ckes.Format("%H:%M/%d"),prlCca->Urno,prlCca->Udem);
			}
		}
	}
}

bool CCSBasicData::IsGroupLeader(JOBDATA *prpPoolJob)
{
	bool blIsGroupLeader = false;

	if(prpPoolJob != NULL)
	{
		WGPDATA *prlWgp = ogWgpData.GetWgpByWgpc(GetTeamForPoolJob(prpPoolJob));
		if(prlWgp != NULL)
		{
			CString olGroupLeaderFunction = ogPgpData.GetGroupLeaderFunction(prlWgp->Pgpu);
			if(!olGroupLeaderFunction.IsEmpty())
			{
				CString olFunction = ogBasicData.GetFunctionForPoolJob(prpPoolJob);
				if(olFunction == olGroupLeaderFunction)
				{
					blIsGroupLeader = true;
				}
			}
		}
	}

	return blIsGroupLeader;
}

bool CCSBasicData::IsGroupLeader(SHIFTDATA *prpShift)
{
	bool blIsGroupLeader = false;

	if(prpShift != NULL)
	{
		WGPDATA *prlWgp = ogWgpData.GetWgpByWgpc(GetTeamForShift(prpShift));
		if(prlWgp != NULL)
		{
			CString olGroupLeaderFunction = ogPgpData.GetGroupLeaderFunction(prlWgp->Pgpu);
			if(!olGroupLeaderFunction.IsEmpty())
			{
				CString olFunction = ogSpfData.GetFunctionBySurn(prpShift->Stfu, prpShift->Avfa);
				if(olFunction == olGroupLeaderFunction)
				{
					blIsGroupLeader = true;
				}
			}
		}
	}

	return blIsGroupLeader;
}

CString CCSBasicData::GetStringForAllocUnitType(CString opAllocUnitType)
{
	CString olAlocString("");

	if(opAllocUnitType == ALLOCUNITTYPE_CIC)
	{
		olAlocString = GetString(IDS_STRING61643); // "CIC Desk "
	}
	else if(opAllocUnitType == ALLOCUNITTYPE_GATE)
	{
		olAlocString = GetString(IDS_STRING61644); // "Gate "
	}
	else if(opAllocUnitType == ALLOCUNITTYPE_PST)
	{
		olAlocString = GetString(IDS_POSITION); // "Position "
	}
	else if(opAllocUnitType == ALLOCUNITTYPE_REGN)
	{
		olAlocString = GetString(IDS_STRING61655); // "Reg "
	}
	return olAlocString;
}

CString CCSBasicData::GetAllocUnitTypeFromString(CString opAllocUnitTypeString)
{
	CString olAllocUnitType("");

	if(opAllocUnitTypeString == GetString(IDS_STRING61643))
	{
		olAllocUnitType = ALLOCUNITTYPE_CIC;
	}
	else if(opAllocUnitTypeString == GetString(IDS_STRING61644))
	{
		olAllocUnitType = ALLOCUNITTYPE_GATE;
	}
	else if(opAllocUnitTypeString == GetString(IDS_POSITION))
	{
		olAllocUnitType = ALLOCUNITTYPE_PST;
	}
	else if(opAllocUnitTypeString == GetString(IDS_STRING61655))
	{
		olAllocUnitType = ALLOCUNITTYPE_REGN;
	}
	return olAllocUnitType;
}

BOOL CCSBasicData::IsDisplayDateInsideTimeFrame()
{
	CTime olCurrentTime = GetLocalTime();
	return olCurrentTime >= omTimeframeStart && olCurrentTime <= omTimeframeEnd;
}
/*
bool CCSBasicData::JobtabHasUequ(void)
{
	static int ilRc = -1;
	if (ilRc == -1)
	{
		char pclWhere[100], pclFields[100] = "FINA";
		char pclTable[100];
		sprintf(pclWhere,"WHERE Tana = 'JOB' AND Fina = 'UEQU'");
		sprintf(pclTable,"SYS%s",pcgTableExt);

		ilRc = CedaAction("RT", pclTable, pclFields, pclWhere, "", "BUF1");
	}

	if (ilRc)
		return true;
	else
		return false;
}

bool CCSBasicData::JobtabHasUtpl(void)
{
	static int ilRc = -1;
	if (ilRc == -1)
	{
		char pclWhere[100], pclFields[100] = "FINA";
		char pclTable[100];
		sprintf(pclWhere,"WHERE TANA = 'JOB' AND FINA = 'UTPL'");
		sprintf(pclTable,"SYS%s",pcgTableExt);

		ilRc = CedaAction("RT", pclTable, pclFields, pclWhere, "", "BUF1");
	}

	if (ilRc)
		return true;
	else
		return false;
}
*/
bool CCSBasicData::HandlingAgentsSupported(void)
{
	return DoesFieldExist("AFT", "HARA");
//	static int ilRc = -1;
//	if (ilRc == -1)
//	{
//		char pclWhere[100], pclFields[100] = "FINA";
//		char pclTable[100];
//		sprintf(pclWhere,"WHERE Tana = 'AFT' AND Fina = 'HARA'");
//		sprintf(pclTable,"SYS%s",pcgTableExt);
//
//		ilRc = CedaAction("RT", pclTable, pclFields, pclWhere, "", "BUF1");
//	}
//
//	if (ilRc)
//		return true;
//	else
//		return false;
}

bool CCSBasicData::DeadloadPerFlightSupported()
{
	return DoesFieldExist("AFT", "DDLF");
//	static int ilRc = -1;
//	if (ilRc == -1)
//	{
//		char pclWhere[100], pclFields[100] = "FINA";
//		char pclTable[100];
//		sprintf(pclWhere,"WHERE Tana = 'AFT' AND Fina = 'DDLF'");
//		sprintf(pclTable,"SYS%s",pcgTableExt);
//
//		ilRc = CedaAction("RT", pclTable, pclFields, pclWhere, "", "BUF1");
//	}
//
//	if (ilRc)
//		return true;
//	else
//		return false;
}

bool CCSBasicData::PremiumClassSupported(void)
{
	return DoesFieldExist("PAX", "PBLP");
//
//	static int ilRc = -1;
//	if (ilRc == -1)
//	{
//		char pclWhere[100], pclFields[100] = "FINA";
//		char pclTable[100];
//		sprintf(pclWhere,"WHERE Tana = 'PAX' AND Fina = 'PBLP'");
//		sprintf(pclTable,"SYS%s",pcgTableExt);
//
//		ilRc = CedaAction("RT", pclTable, pclFields, pclWhere, "", "BUF1");
//	}
//
//	if (ilRc)
//		return true;
//	else
//		return false;
}


bool CCSBasicData::SetTemplateFilters(const CDWordArray& ropTplUrnos)
{
	omTemplateFilterUrnos.RemoveAll();
	omTemplateFilterUrnos.Append(ropTplUrnos);	
	return true;
}

bool CCSBasicData::GetTemplateFilters(CDWordArray& ropTplUrnos)
{
	if (omTemplateFilterUrnos.GetSize() == 0)
	{
		if (!GetDefaultTemplateFilters(omTemplateFilterUrnos))
			return false;
	}

	ropTplUrnos.Append(omTemplateFilterUrnos);
	return true;
}

bool CCSBasicData::GetDefaultTemplateFilters(CDWordArray& ropTplUrnos)
{
	char pclTemplateFilter[512];
	char pclTemplateCopy[512];
	CString olConfigFileName = GetConfigFileName();


    GetPrivateProfileString(pcgAppName, "TEMPLATEFILTER", "ALL",pclTemplateFilter, sizeof pclTemplateFilter, olConfigFileName);
	
	bool blUseAllTemplates = stricmp(pclTemplateFilter,"ALL") == 0;

	if (ogTplData.omData.GetSize() == 0)
		ogTplData.ReadTplData();

	for (int i = 0; i < ogTplData.omData.GetSize(); i++)
	{
		TPLDATA *polTplData = &ogTplData.omData[i];
		if (!polTplData)
			continue;
		if (polTplData->Tpst[0] == '0')
			continue;

		if (blUseAllTemplates)
			ropTplUrnos.Add(polTplData->Urno);

		bool blSelected = false;
		strcpy(pclTemplateCopy,pclTemplateFilter);
		char *pclToken  = strtok(pclTemplateCopy,",");
		while (pclToken)
		{
			if (stricmp(pclToken,polTplData->Tnam) == 0)
			{
				blSelected = true;
				break;
			}

			pclToken = strtok(NULL,",");
		}

		if (!blSelected)
			continue;

		ropTplUrnos.Add(polTplData->Urno);
	}
	
	return true;
}

// return true if DRRTAB has the field FCTC defined
bool CCSBasicData::ShiftHasFunctionCode(void)
{
	return DoesFieldExist("DRR", "FCTC");
//	static int ilRc = -1;
//	if (ilRc == -1)
//	{
//		char pclWhere[100], pclFields[100] = "FINA";
//		char pclTable[100];
//		sprintf(pclWhere,"WHERE Tana = 'DRR' AND Fina = 'FCTC'");
//		sprintf(pclTable,"SYS%s",pcgTableExt);
//
//		ilRc = CedaAction("RT", pclTable, pclFields, pclWhere, "", "BUF1");
//	}
//
//	if (ilRc)
//		return true;
//	else
//		return false;
}

CString CCSBasicData::GetFunctionsForDemand(DEMANDDATA *prpDemand)
{
	CStringArray olFunctions;
	return GetFunctionsForDemand(prpDemand, olFunctions);
}

CString CCSBasicData::GetFunctionsForDemand(DEMANDDATA *prpDemand, CStringArray &ropFunctions)
{
	CString olFunctions;
	if(prpDemand != NULL)
	{
		RPFDATA *prlDemandFunction = ogRpfData.GetFunctionByUrud(prpDemand->Urud);
		if(prlDemandFunction != NULL)
		{
			olFunctions = CString(prlDemandFunction->Fcco);
			if(!olFunctions.IsEmpty())
			{
				ropFunctions.Add(olFunctions);
			}
			else
			{
				// group of optional functions
				CCSPtrArray <SGMDATA> olSgms;
				ogSgmData.GetSgmListByUsgr(prlDemandFunction->Upfc,olSgms);
				int ilNumSgms = olSgms.GetSize();
				for(int ilSgm = 0; ilSgm < ilNumSgms; ilSgm++)
				{
					PFCDATA *prlPfc = ogPfcData.GetPfcByUrno(olSgms[ilSgm].Uval);
					if(prlPfc != NULL)
					{
						ropFunctions.Add(CString(prlPfc->Fctc));
						if(!olFunctions.IsEmpty())
							olFunctions += ",";
						olFunctions += CString(prlPfc->Fctc);
					}
				}
			}
		}
	}
	return olFunctions;
}

void CCSBasicData::GetFunctionAndPermits(DEMANDDATA *prpDemand, CStringArray &ropDemandInfo, bool bpUseTeamInfo)
{
	if (!prpDemand)
		return;

	CString olDemandInfo;
	
	// get and check the function(s) for the demand to be assigned
	RPFDATA *prlDemandFunction = ogRpfData.GetFunctionByUrud(prpDemand->Urud);
	if(prlDemandFunction != NULL)
	{
		CString olDemandFunction = CString(prlDemandFunction->Fcco);
		if(olDemandFunction.IsEmpty())
		{
			// group of optional functions
			CCSPtrArray <SGMDATA> olSgms;
			ogSgmData.GetSgmListByUsgr(prlDemandFunction->Upfc,olSgms);
			int ilNumSgms = olSgms.GetSize();
			CString olRequiredFunctions;
			for(int ilSgm = 0; ilSgm < ilNumSgms; ilSgm++)
			{
				PFCDATA *prlPfc = ogPfcData.GetPfcByUrno(olSgms[ilSgm].Uval);
				if(prlPfc != NULL)
				{
					AddElem(olRequiredFunctions,prlPfc->Fctc);
				}
			}
			// optional functions
			olDemandInfo = olRequiredFunctions;
		}
		else
		{
			// single mandatory function
			olDemandInfo = olDemandFunction;
		}
	}

	// get and check the permit(s) for this demand
	CString olRequiredMandatoryPermits, olRequiredOptionalPermits;
	CCSPtrArray<RPQDATA> olDemandPermits;
	// lli: need to check the permits for udgr as well
	CCSPtrArray<RPQDATA> olDemandPermits2;
	ogRpqData.GetRpqsByUrud(prpDemand->Urud,olDemandPermits);
	ogRpqData.GetRpqsByUdgr(prpDemand->Udgr,olDemandPermits2);
	olDemandPermits.Append(olDemandPermits2);
	int ilNumDemandPermits = olDemandPermits.GetSize();

	for(int ilDP = 0; ilDP < ilNumDemandPermits; ilDP++)
	{
		RPQDATA *prlDemandPermit = &olDemandPermits[ilDP];
		CString olDemandPermit = prlDemandPermit->Quco;
		if(olDemandPermit.IsEmpty())
		{
			// this is a header for a group of permits, one of which the emp must have
			CCSPtrArray <SGMDATA> olSgms;
			ogSgmData.GetSgmListByUsgr(prlDemandPermit->Uper,olSgms);
			int ilNumSgms = olSgms.GetSize();
			olRequiredOptionalPermits.Empty();
			for(int ilSgm = 0; ilSgm < ilNumSgms; ilSgm++)
			{
				PERDATA *prlPer = ogPerData.GetPerByUrno(olSgms[ilSgm].Uval);
				if(prlPer != NULL)
				{
					AddElem(olRequiredOptionalPermits,prlPer->Prmc);
				}
			}
			//olText.Format("Optional Permits: %s",olRequiredOptionalPermits);
			olDemandInfo += CString("/") + olRequiredOptionalPermits;
		}
		else
		{
			// not a group of permits, so this is a must-have permit
			AddElem(olRequiredMandatoryPermits,olDemandPermit);
		}
	}

	if(!olRequiredMandatoryPermits.IsEmpty())
	{
		//olText.Format("Mandatory Permits: %s",olRequiredMandatoryPermits);
		olDemandInfo += CString("/") + olRequiredMandatoryPermits;
	}


	if (!olDemandInfo.IsEmpty())
	{
		ropDemandInfo.Add(olDemandInfo);
		olDemandInfo = "";
	}

	// this demand can be part of a group of demands which require a team of emps,
	// the following creates lists of permits/functions required by the team
	// get all other permits (RPQs) with the same group no.(Udgr) as this one
	if(prpDemand->Udgr != 0L && bpUseTeamInfo)
	{
		CStringArray olTeamRequiresAllOfThesePermits;
		CStringArray olTeamRequiresOneOfThesePermits;
		int ilNumOptionalTeamPermits, ilNumMandatoryTeamPermits;

		CCSPtrArray <RPQDATA> olRpqs;
		ogRpqData.GetRpqsByUdgr(prpDemand->Udgr,olRpqs);
		int ilNumRpqs = olRpqs.GetSize();
		for(int ilPermit = 0; ilPermit < ilNumRpqs; ilPermit++)
		{
			RPQDATA *prlPermit = &olRpqs[ilPermit];
			CString olTeamPermit = prlPermit->Quco;
			if(olTeamPermit.IsEmpty())
			{
				// this is a header for a group of permits, one of which the team must have
				CCSPtrArray <SGMDATA> olSgms;
				ogSgmData.GetSgmListByUsgr(prlPermit->Uper,olSgms);
				int ilNumSgms = olSgms.GetSize();
				for(int ilSgm = 0; ilSgm < ilNumSgms; ilSgm++)
				{
					PERDATA *prlPer = ogPerData.GetPerByUrno(olSgms[ilSgm].Uval);
					if(prlPer != NULL)
					{
						olTeamRequiresOneOfThesePermits.Add(prlPer->Prmc);
					}
				}
			}
			else
			{
				// not a group of permits, so this is a must-have permit for the team
				olTeamRequiresAllOfThesePermits.Add(olTeamPermit);
			}
		}


		CString olRequiredPermits;

		// check if the emp has one of the required optional permits for the team
		ilNumOptionalTeamPermits = olTeamRequiresOneOfThesePermits.GetSize();
		if(ilNumOptionalTeamPermits > 0)
		{
			olRequiredPermits.Empty();
			for(int ilPermit = 0; ilPermit < ilNumOptionalTeamPermits; ilPermit++)
			{
				AddElem(olRequiredPermits,olTeamRequiresOneOfThesePermits[ilPermit]);
			}
			//olText.Format("Optional Team Permits %s",olRequiredPermits);
			olDemandInfo += CString("/") + olRequiredPermits;
		}
		// check if the emp has all of the required mandatory permits for the team
		ilNumMandatoryTeamPermits = olTeamRequiresAllOfThesePermits.GetSize();
		if(ilNumMandatoryTeamPermits > 0)
		{
			olRequiredPermits.Empty();
			for(int ilPermit = 0; ilPermit < ilNumMandatoryTeamPermits; ilPermit++)
			{
				AddElem(olRequiredPermits,olTeamRequiresAllOfThesePermits[ilPermit]);
			}
			olDemandInfo += CString("/") + olRequiredPermits;
		}

		if (!olDemandInfo.IsEmpty())
			ropDemandInfo.Add(olDemandInfo);

	} // end team checking -> if(prlRud->Udgr != 0L)

}

BOOL CCSBasicData::GetCounterClass(DEMANDDATA *prpDem,CString& ropClass)
{
	if (!prpDem)
		return FALSE;

	RUDDATA *prlRud = ogRudData.GetRudByUrno(prpDem->Urud);
	if (prlRud)
	{
		CCCDATA *prlCcc = ogCccData.GetCccByUrno(prlRud->Vref);
		if (prlCcc)
		{
			ropClass = prlCcc->Cicc;
			return TRUE;
		}
	}

	return FALSE;
}

BOOL CCSBasicData::GetCounterClassName(DEMANDDATA *prpDem,CString& ropClassName)
{
	if (!prpDem)
		return FALSE;

	RUDDATA *prlRud = ogRudData.GetRudByUrno(prpDem->Urud);
	if (prlRud)
	{
		CCCDATA *prlCcc = ogCccData.GetCccByUrno(prlRud->Vref);
		if (prlCcc)
		{
			ropClassName = prlCcc->Cicn;
			return TRUE;
		}
	}

	return FALSE;
}

long CCSBasicData::GetCounterClassUrno(DEMANDDATA *prpDem)
{
	if (!prpDem)
		return 0L;

	RUDDATA *prlRud = ogRudData.GetRudByUrno(prpDem->Urud);
	if (prlRud)
	{
		CCCDATA *prlCcc = ogCccData.GetCccByUrno(prlRud->Vref);
		if (prlCcc)
		{
			return prlCcc->Urno;
		}
	}

	return 0L;
}

BOOL CCSBasicData::GetCounterGroups(DEMANDDATA *prpDem,CStringArray& ropCounterGroups,BOOL bpGroupsOnly)
{
	if (!prpDem)
		return FALSE;

	RUDDATA *prlRud = ogRudData.GetRudByUrno(prpDem->Urud);
	if (prlRud)
	{
		if (prlRud->Ulnk != 0)
		{
			CCSPtrArray<RUDDATA> olRuds;
			if (ogRudData.GetRudsByUlnk(prlRud->Ulnk,LOCATION_DEMAND,olRuds) > 0)
			{
				int ilSize = olRuds.GetSize();
				for (int ilC = 0; ilC < ilSize; ilC++)
				{
					RLODATA *polRlo = ogRloData.GetRloByUrud(olRuds[ilC].Urno);
					if (polRlo)
					{
						if (strlen(polRlo->Gtab) && strcmp(polRlo->Gtab,"SGR.GRPN") == 0)
						{
							SGRDATA *polSgr = ogSgrData.GetSgrByUrno(polRlo->Rloc);
							if (polSgr && strcmp(polSgr->Tabn,"CIC") == 0)
							{
								ropCounterGroups.Add(polSgr->Grpn);
							}
						}
						else if (!bpGroupsOnly && strlen(polRlo->Reft) && strcmp(polRlo->Reft,"CIC.CNAM") == 0)
						{
							CICDATA *polCic = ogCicData.GetCicByUrno(polRlo->Rloc);
							if (polCic)
							{
								ropCounterGroups.Add(polCic->Cnam);
							}
						}
					}
				}

				if (ropCounterGroups.GetSize())
					return TRUE;
				else
					return FALSE;
			}
		}
	}

	return FALSE;
}

BOOL CCSBasicData::GetCounters(DEMANDDATA *prpDem,CDWordArray& ropCounters)
{
	if (!prpDem)
		return FALSE;

	RUDDATA *prlRud = ogRudData.GetRudByUrno(prpDem->Urud);
	if (prlRud)
	{
		if (prlRud->Ulnk != 0)
		{
			CCSPtrArray<RUDDATA> olRuds;
			if (ogRudData.GetRudsByUlnk(prlRud->Ulnk,LOCATION_DEMAND,olRuds) > 0)
			{
				int ilSize = olRuds.GetSize();
				for (int ilC = 0; ilC < ilSize; ilC++)
				{
					if (olRuds[ilC].Urue != prpDem->Urue)
						continue;

					RLODATA *polRlo = ogRloData.GetRloByUrud(olRuds[ilC].Urno);
					if (polRlo)
					{
						if (strlen(polRlo->Gtab) && strcmp(polRlo->Gtab,"SGR.GRPN") == 0)
						{
							SGRDATA *polSgr = ogSgrData.GetSgrByUrno(polRlo->Rloc);
							if (polSgr && strcmp(polSgr->Tabn,"CIC") == 0)
							{
								CCSPtrArray<SGMDATA> olSgmList;
								ogSgmData.GetSgmListByUsgr(polSgr->Urno,olSgmList);
								for (int i = 0; i < olSgmList.GetSize(); i++)
								{
									ropCounters.Add(olSgmList[i].Uval);
								}
							}
						}
						else if (strlen(polRlo->Reft) && strcmp(polRlo->Reft,"CIC.CNAM") == 0)
						{
							CICDATA *polCic = ogCicData.GetCicByUrno(polRlo->Rloc);
							if (polCic)
							{
								ropCounters.Add(polCic->Urno);
							}
						}
					}
				}

				if (ropCounters.GetSize())
					return TRUE;
				else
					return FALSE;
			}
		}
	}

	return FALSE;
}


FLIGHTDATA *CCSBasicData::GetFlightForJob(JOBDATA *prpJob)
{
	FLIGHTDATA *prlFlight = NULL;
	if(prpJob != NULL)
	{
		if(strcmp(prpJob->Aloc,ALLOCUNITTYPE_REGN))
		{
			// normal flight jobs have the flight URNO in prpJob->Flur -
			prlFlight = ogFlightData.GetFlightByUrno(prpJob->Flur);
		}
		else
		{
			// registration jobs are not linked directly to the flight so we get the flight URNO from the demand
			DEMANDDATA *prlDemand = ogJodData.GetDemandForJob(prpJob);
			if(prlDemand)
			{
				prlFlight = ogFlightData.GetFlightByUrno(ogDemandData.GetFlightUrno(prlDemand));
			}
			else if(prpJob->Flur != 0L)
			{
				// third party flights when first assigned have flur
				// set but are not yet connected to the demand
				prlFlight = ogFlightData.GetFlightByUrno(prpJob->Flur);
			}
		}
	}
	return prlFlight;
}

long CCSBasicData::GetFlightUrnoForJob(JOBDATA *prpJob, long *plpArrUrno /* = NULL*/, long *plpDepUrno /* = NULL*/)
{
	long llFlur = 0L;
	if(prpJob != NULL)
	{
		if(strcmp(prpJob->Aloc,ALLOCUNITTYPE_REGN))
		{
			// normal flight jobs have the flight URNO in prpJob->Flur -
			llFlur = prpJob->Flur;
		}
		else
		{
			// registration jobs are not linked directly to the flight so we get the flight URNO from the demand
			DEMANDDATA *prlDemand = ogJodData.GetDemandForJob(prpJob);
			if(prlDemand)
			{
				llFlur = ogDemandData.GetFlightUrno(prlDemand);
			}
			else if(prpJob->Flur != 0L)
			{
				// third party flights when first assigned have flur
				// set but are not yet connected to the demand
				llFlur = prpJob->Flur;
			}
		}

		// need to differentiate between arr and dep URNO for return flights
		bool blArr = (*prpJob->Dety == '1') ? true : false;
		if(plpArrUrno != NULL)
			*plpArrUrno = (blArr) ? llFlur : 0L;
		if(plpDepUrno != NULL)
			*plpDepUrno = (blArr) ? 0L : llFlur;
	}
	return llFlur;
}


DEMANDDATA* CCSBasicData::GetFirstDemandWithoutJobByFlur(long lpFlur, const char *pcpAlocUnitType, int ipDemandType /* = PERSONNELDEMANDS */, int ipReturnFlightType /*ID_NOT_RETURNFLIGHT*/)
{
	CCSPtrArray<DEMANDDATA> olDemands;
	if (!ogDemandData.GetDemandsByFlur(olDemands,lpFlur,"",pcpAlocUnitType,ipDemandType,ipReturnFlightType))
		return false;

	olDemands.Sort(CompareDemandStartTime);
	for (int ilC = 0; ilC < olDemands.GetSize(); ilC++)
	{
		DEMANDDATA *prlDemand = &olDemands[ilC];						
		CCSPtrArray<JOBDATA> olJobs;
		if (!ogJodData.DemandHasJobs(prlDemand->Urno))
		{
			return prlDemand;
		}
	}

	return NULL;
}

bool CCSBasicData::DemandsToLoad(const char *pcpDety)
{
	bool blLoadDemand = true;
	if(!omDemandsToLoad.IsEmpty())
	{
		CString olDety;
		olDety.Format(",%s,",pcpDety);
		if(omDemandsToLoad.Find(olDety) == -1)
		{
			blLoadDemand = false;
		}
	}

	return blLoadDemand;
}

// use this function to check if demands have been loaded for this job
// - some demands may not be loaded because they are outside the timespan
// or because their template was filtered out. Jobs which are independent
// of demands (no JOD found) are always loaded.
bool CCSBasicData::LoadJob(long lpJobUrno)
{
	bool blLoadJob = false;

	CCSPtrArray <JODDATA> olJods;
	ogJodData.GetJodsByJob(olJods, lpJobUrno);
	int ilNumJods = olJods.GetSize();
	if(ilNumJods <= 0)
	{
		// job has no JODs so is a demand independent job
		blLoadJob = true;
	}
	else
	{
		for(int ilJod = 0; ilJod < ilNumJods; ilJod++)
		{
			if(ogDemandData.GetDemandByUrno(olJods[ilJod].Udem) != NULL)
			{
				// job has one or more demands loaded
				blLoadJob = true;
				break;
			}
		}
	}

	return blLoadJob;
}


// ipDemandType - { ALLDEMANDS | PERSONNELDEMANDS | EQUIPMENTDEMANDS | LOCATIONDEMANDS }
bool CCSBasicData::JobIsOfType(long lpJobUrno, int ipDemandType)
{
	bool blJobIsOfType = true;
	if(ipDemandType != ALLDEMANDS)
	{
		DEMANDDATA *prlDemand = ogJodData.GetDemandForJob(lpJobUrno);
		if(prlDemand != NULL && !ogRudData.DemandIsOfType(prlDemand->Urud, ipDemandType))
		{
			blJobIsOfType = false;
		}
	}

	return blJobIsOfType;
}

CString CCSBasicData::GetJobAlid(JOBDATA *prpJob)
{
	CString olJobAlid;

	if(prpJob != NULL)
	{
		if(!strcmp(prpJob->Jtco,JOBFLIGHT))
		{
			olJobAlid = "FlightNotFound";
			FLIGHTDATA *prlFlight = ogBasicData.GetFlightForJob(prpJob);
			if(prlFlight != NULL)
			{
				FLIGHTDATA *prlRotation = NULL;
				if(ogJobData.IsTurnaroundJob(prpJob) &&
					(prlRotation = ogFlightData.GetRotationFlight(prlFlight)) != NULL)
				{
					if(ogFlightData.IsArrival(prlFlight))
						olJobAlid.Format("%s/%s", prlFlight->Fnum, prlRotation->Fnum);
					else
						olJobAlid.Format("%s/%s", prlRotation->Fnum, prlFlight->Fnum);
				}
				else
				{
					olJobAlid = prlFlight->Fnum;
				}
			}
		}
		else if(!strcmp(prpJob->Jtco,JOBEQUIPMENTFASTLINK))
		{
			EQUDATA *prlEqu = ogEquData.GetEquByUrno(prpJob->Uequ);
			olJobAlid = (prlEqu == NULL)? "EquNotFound": prlEqu->Enam;
		}
		else if(!strcmp(prpJob->Jtco,JOBFID))
		{
			DEMANDDATA *prlDemand = ogJodData.GetDemandForJob(prpJob);
			if(prlDemand != NULL)
			{
				RUDDATA *prlRud = ogRudData.GetRudByUrno(prlDemand->Urud);
				if (prlRud)
				{
					SERDATA *prlSer = ogSerData.GetSerByUrno(prlRud->Ughs);
					if (prlSer)
					{
						olJobAlid = prlSer->Snam;
					}
				}
			}
		}
		else if(!strcmp(prpJob->Jtco, JOBTEAMDELEGATION))
		{
			JOBDATA *prlDelPoolJob = ogJobData.GetPoolJobForDetachJob(prpJob->Urno);
			if(prlDelPoolJob != NULL)
			{
				DLGDATA *prlDlg = ogDlgData.GetDlgByUjob(prlDelPoolJob->Urno);
				if(prlDlg != NULL && strlen(prlDlg->Wgpc))
				{
					olJobAlid = prlDlg->Wgpc; // group name
				}
				else
				{
					olJobAlid = CString("->") + GetJobAlid(prlDelPoolJob);
				}
			}
		}
		else if(!strcmp(prpJob->Jtco, JOBDETACH))
		{
			JOBDATA *prlDetPoolJob = ogJobData.GetPoolJobForDetachJob(prpJob->Urno);
			if(prlDetPoolJob != NULL)
			{
				olJobAlid = CString("->") + GetJobAlid(prlDetPoolJob);
			}
		}
		else if(!strcmp(prpJob->Jtco,JOBSPECIAL))
		{
			olJobAlid = CString(prpJob->Text);
		}
		else if(!strcmp(prpJob->Jtco,JOBBREAK))
		{
			olJobAlid = GetString(IDS_STRING61385);
		}
		else if(!strcmp(prpJob->Jtco,JOBCCC))
		{
			// common checkin counter job
			DEMANDDATA *prlDemand = ogJodData.GetDemandForJob(prpJob);
			if(prlDemand != NULL)
			{
				ogBasicData.GetCounterClass(prlDemand,olJobAlid);
			}
		}
		else
		{
			olJobAlid = prpJob->Alid;
		}
	}

	return olJobAlid;
}

// if opDate is same day as the display date return "HHMM" else "HHMM/dd"
CString CCSBasicData::FormatDate(CTime opDate, CTime opDisplayDate /* = TIMENULL*/)
{
	CString olDate;

	if(opDate != TIMENULL)
	{
		if(opDisplayDate == TIMENULL)
		{
			opDisplayDate = omDisplayDate; // the date set in the main comboBox
		}

		if(opDate.GetDay() == opDisplayDate.GetDay())
		{
			olDate = opDate.Format("%H%M");
		}
		else
		{
			olDate = opDate.Format("%H%M/%d");
		}
	}

	return olDate;
}

// if opDate is same day as the display date return "HHMM" else "HHMM/dd"
CString CCSBasicData::FormatDate2(CTime opDate, CTime opDisplayDate /* = TIMENULL*/)
{
	CString olDate;

	if(opDate != TIMENULL)
	{
		if(opDisplayDate == TIMENULL)
		{
			opDisplayDate = omDisplayDate; // the date set in the main comboBox
		}

		if(opDate.GetDay() == opDisplayDate.GetDay())
		{
			olDate = opDate.Format("%H:%M");
		}
		else
		{
			olDate = opDate.Format("%H:%M/%d");
		}
	}

	return olDate;
}



//the functions MarkTime() and DisplayElapsedTimeSinceLastMark() 
//allow you to time events see the example below:
//
//	ogBasicData.MarkTime();
//
//	:	:	:
//	your function calls go here
//	:	:	:
//
//	ogBasicData.DisplayElapsedTimeSinceLastMark();
//
//ouputs:
//
//		Time elapsed <0.35> millisecs.

void CCSBasicData::MarkTime()
{
	prmMarkTime = clock();	
}

void CCSBasicData::DisplayElapsedTimeSinceLastMark()
{
	TRACE("Time elapsed: %s\n",GetElapsedTimeSinceLastMark());
}

CString CCSBasicData::GetElapsedTimeSinceLastMark()
{
	clock_t prlCurrTime = clock();
	double time = (prlCurrTime - prmMarkTime)/(CLOCKS_PER_SEC / (double) 1000.0);
	CString olTime;
	olTime.Format("<%f> millisecs.", time);
	return olTime;
}

void CCSBasicData::GetUrnoFromString(CUIntArray &ropUrnos, CString &ropUrnoString)
{
	CStringArray olUrnoStringArray;
	ogBasicData.ExtractItemList(ropUrnoString, &olUrnoStringArray);
	GetUrnoFromStringArray(ropUrnos, olUrnoStringArray);
}

void CCSBasicData::GetUrnoFromStringArray(CUIntArray &ropUrnos, CStringArray &ropUrnoStringArray)
{
	int ilNumUrnos = ropUrnoStringArray.GetSize();
	for(int ilU = 0; ilU < ilNumUrnos; ilU++)
	{
		ropUrnos.Add(atoi(ropUrnoStringArray[ilU]));
	}
}

// format a CUIntArray of URNOS to: "'URNO1','URNO2',...,'URNOn'"
CString CCSBasicData::FormatUrnoList(CUIntArray &ropUrnos)
{
	CString olList, olTmp;

	int ilNumUrnos = ropUrnos.GetSize();
	if(ilNumUrnos > 0)
	{
		olList.Format("'%d'", ropUrnos[0]);
		for(int ilU = 1; ilU < ilNumUrnos; ilU++)
		{
			olTmp.Format(",'%d'", ropUrnos[ilU]);
			olList += olTmp;
		}
	}

	return olList;
}

// format a CStringArray of URNOS to: "'URNO1','URNO2',...,'URNOn'"
CString CCSBasicData::FormatUrnoList(CStringArray &ropUrnos)
{
	CString olList, olTmp;

	int ilNumUrnos = ropUrnos.GetSize();
	if(ilNumUrnos > 0)
	{
		olList.Format("'%s'", ropUrnos[0]);
		for(int ilU = 1; ilU < ilNumUrnos; ilU++)
		{
			olTmp.Format(",'%s'", ropUrnos[ilU]);
			olList += olTmp;
		}
	}

	return olList;
}

// get ropFrom/ropTo from the string "YYYYMMDDHHMMSS-YYYYMMDDHHMMSS" or from "YYYYMMDD-YYYYMMDD"
bool CCSBasicData::GetDatesFromString(CTime &ropFrom, CTime &ropTo, CString &ropDateString)
{
	bool blValidDate = false;
	int ilIdx1 = -1, ilLen;
	CString olDate;
	if((ilIdx1 = ropDateString.Find("-",0)) != -1)
	{
		olDate = ropDateString.Left(ilIdx1);
		if(olDate.GetLength() == 8)
			olDate += "000000";
		StoreDate(olDate, (CTime *)&ropFrom);

		ilLen = ropDateString.GetLength() - (ilIdx1+1);
		olDate = ropDateString.Right(ilLen);
		if(olDate.GetLength() == 8)
			olDate += "235959";
		StoreDate(olDate, (CTime *)&ropTo);

		if(ropFrom != TIMENULL && ropTo != TIMENULL && ropFrom <= ropTo)
		{
			blValidDate = true;
		}
	}

	return blValidDate;
}

bool CCSBasicData::JobAndDemHaveDiffFuncs(JOBDATA *prpJob)
{
	if(prpJob == NULL)
		return false;

	JOBDATA *prlPoolJob = ogJobData.GetJobByUrno(prpJob->Jour);
	if(prlPoolJob == NULL)
		return false;

	DEMANDDATA *prlDemand = ogJodData.GetDemandForJob(prpJob);
	if(prlDemand == NULL)
		return false;

	RPFDATA *prlDemandFunction = ogRpfData.GetFunctionByUrud(prlDemand->Urud);
	if(prlDemandFunction == NULL)
		return false;

	CString olEmpFunction = GetFunctionForPoolJob(prlPoolJob);
	if(strlen(prlDemandFunction->Fcco) == 0)
	{
		// group of optional functions
		CCSPtrArray <SGMDATA> olSgms;
		ogSgmData.GetSgmListByUsgr(prlDemandFunction->Upfc,olSgms);
		for(int ilSgm = 0; ilSgm < olSgms.GetSize(); ilSgm++)
		{
			PFCDATA *prlPfc = ogPfcData.GetPfcByUrno(olSgms[ilSgm].Uval);
			if(prlPfc != NULL && !strcmp(prlPfc->Fctc, olEmpFunction))
				return false;
		}
	}
	else
	{
		if(!strcmp(prlDemandFunction->Fcco, olEmpFunction))
			return false;
	}
	// MCU 20070519 for delegated or restricted flight check prio 1 function if
	// it matches independent from the Terminal (2. character)
	if((strcmp(prpJob->Jtco,JOBDELEGATEDFLIGHT) == 0) ||
   	   (strcmp(prpJob->Jtco,JOBRESTRICTEDFLIGHT) == 0))
	{
		// get function from shift, don't use function from pool job
		SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prpJob->Shur);
		if (prlShift != NULL)
		{
             olEmpFunction = prlShift->Efct;
		}
		if (*prlDemandFunction->Fcco != *olEmpFunction)
		{
			// 1. character is different
			return true;
		}
		// no check the function code after Terminal
		if(!strcmp(&prlDemandFunction->Fcco[2], olEmpFunction.Mid(2)))
			return false;
	}
	/*********
	if((strcmp(prpJob->Jtco,JOBDELEGATEDFLIGHT) == 0) ||
   	   (strcmp(prpJob->Jtco,JOBRESTRICTEDFLIGHT) == 0))
	{
		CString olFunctions = ogBasicData.GetMultiFunctionText(prpJob->Ustf);
		if (olFunctions.Find(prpJob->Fcco)  > -1)
		{
			// found job function in MultiFunctionText
			return false;
		}
	}
	******************/

	return true;
}

bool CCSBasicData::GetDiffFuncColour(COLORREF &ropColour)
{
	if(bmDiffFuncColourEnabled)
		ropColour = omDiffFuncColour;
	return bmDiffFuncColourEnabled;
}

void CCSBasicData::InitDiffFuncColour()
{
	bmDiffFuncColourEnabled = false;
	omDiffFuncColour = GRAY;

	CString olColour;
	if(ogDlgSettings.GetValue("DiffFuncColour", "DiffFuncColour", olColour))
	{
		omDiffFuncColour = (COLORREF) atol(olColour);
		bmDiffFuncColourEnabled = true;
	}
}

void CCSBasicData::DumpRereadStatistics()
{
	if(ogBasicData.DisplayDebugInfo() && omRereadDebugInfo.GetSize() > 0)
	{
		CString olPath;
		CString olFile;
		CTime olTime = CTime::GetCurrentTime();
		olPath = CCSLog::GetTmpPath("\\OpssPmRereadStatistics") + olTime.Format("%Y%m%d%H%M") + CString(".log");
		ofstream of;
		of.open(olPath, ios::out);

		for(int i = 0; i < omRereadDebugInfo.GetSize(); i++)
			of << omRereadDebugInfo[i] << endl;

		of.close();
	}
}

void CCSBasicData::SaveTimeTakenForReread(const char *pcpText, const char *pcpSelection, int ipNumRecsRead /* = -1*/)
{
	// write re-read statistics to a file is DISPLAY_DEBUG_INFO = YES
	CTime olCurrTime = CTime::GetCurrentTime();
	CString olDebugInfo;
	if(ipNumRecsRead == -1)
		olDebugInfo.Format("%s: %s (%s) SELECTION: %s", olCurrTime.Format("%Y%d%m%H%M%S"), pcpText, GetElapsedTimeSinceLastMark(), pcpSelection);
	else
		olDebugInfo.Format("%s: %s (%s) %d Recs Read SELECTION: %s", olCurrTime.Format("%Y%d%m%H%M%S"), pcpText, GetElapsedTimeSinceLastMark(), ipNumRecsRead, pcpSelection);
	ogBasicData.Trace("<-- REREAD %s",olDebugInfo);
	omRereadDebugInfo.Add(olDebugInfo);
}


void CCSBasicData::InitShiftDate()
{	
	CTimeSpan olOneDay(1,0,0,0);
	CTime olLoadShiftsFrom = omTimeframeStart + CTimeSpan(0,0,0,1); // PRF 4824
	CTime olLoadShiftsTo = omTimeframeEnd - CTimeSpan(0,0,0,1); // PRF 4824
	
	omShiftFirstSDAY = CTime(olLoadShiftsFrom.GetYear(), olLoadShiftsFrom.GetMonth(), olLoadShiftsFrom.GetDay(), 0, 0, 0);
	omShiftFirstSDAY -= olOneDay; // minus one day in case the shift from prev day overlaps with the timeframe start
	omShiftLastSDAY = CTime(olLoadShiftsTo.GetYear(), olLoadShiftsTo.GetMonth(), olLoadShiftsTo.GetDay(), 23, 59, 59);
}

bool CCSBasicData::DoesFieldExist(CString opTana, CString opFina)
{
	bool blDoesFieldExist = false;
	char pclWhere[100], pclFields[100] = "FINA", pclTable[100];
	sprintf(pclWhere,"WHERE TANA = '%s' AND FINA = '%s'", opTana, opFina);
	sprintf(pclTable,"SYS%s",pcgTableExt);
	if(CedaAction("RT", pclTable, pclFields, pclWhere, "", "BUF1"))
	{
		CString olLine;
		blDoesFieldExist = GetBufferLine(0, olLine);
	}

	return blDoesFieldExist;
}

CString CCSBasicData::FormatFieldForExport(const char *pcpField, const char *pcpSeperator)
{
	CString olField;

	// add inverted commas to prevent shift codes/job times etc being reformatted by excel to dates
	if(strlen(pcpField) > 0)
		olField.Format("'%s'", pcpField);
	else
		olField = CString(pcpField);

	// replace any occurances of the seperator with blanks
	olField.Replace(pcpSeperator," ");

	return olField;
}

void CCSBasicData::DisableAllMenuItems(CMenu &ropMenu)
{
	int ilNumItems = ropMenu.GetMenuItemCount();
	for(int ilPos = 0; ilPos < ilNumItems; ilPos++)
	{
		int ilItem = ropMenu.GetMenuItemID(ilPos);
		ropMenu.EnableMenuItem(ilItem, MF_BYCOMMAND || MF_GRAYED);
	}
}

CString CCSBasicData::GetDrrIndexHint()
{
	static char pchBuffer[256] = "";

	if (strlen(pchBuffer) == 0)
	{
		char pclConfigPath[512];

		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));

		::GetPrivateProfileString("Global","DrrIndexHint","/*+ INDEX(DRRTAB DRRTAB_SDAY_ROSL) */",pchBuffer,sizeof(pchBuffer),pclConfigPath);
	}

	return pchBuffer;
}

//Function meant for special handling of Apron T1 & T2 in Singapore airport
bool CCSBasicData::IsSinApron1()
{	
	bool blIsSinApron1 = false;
	CDWordArray olTplUrnos;
    GetTemplateFilters(olTplUrnos);
	TPLDATA* polTplData = NULL;

	if(strcmp(pcgHomeAirport,"SIN") == 0)
	{
		for(int i = 0; i < olTplUrnos.GetSize(); i++)
		{
			polTplData = ogTplData.GetTplByUrno(olTplUrnos.GetAt(i));
			if(polTplData != NULL)
			{
				if(strcmp(polTplData->Tnam, TPL_APRON1) == 0)
				{
					blIsSinApron1 = true;
				}
				else
				{
					blIsSinApron1 = false;
					break;
				}
			}
		}
	}
	return blIsSinApron1;
}

bool CCSBasicData::IsSinApron2()
{	
	bool blIsSinApron2 = false;
	CDWordArray olTplUrnos;
    GetTemplateFilters(olTplUrnos);
	TPLDATA* polTplData = NULL;

	if(strcmp(pcgHomeAirport,"SIN") == 0)
	{
		for(int i = 0; i < olTplUrnos.GetSize(); i++)
		{
			polTplData = ogTplData.GetTplByUrno(olTplUrnos.GetAt(i));
			if(polTplData != NULL)
			{
				if(strcmp(polTplData->Tnam, TPL_APRON2) == 0)
				{
					blIsSinApron2 = true;
				}
				else
				{
					blIsSinApron2 = false;
					break;
				}
			}
		}
	}
	return blIsSinApron2;
}

//Function meant for special handling of Terminal T2 in Singapore airport for "PAX Service T2" template
//Singapore-Begin
bool CCSBasicData::IsPaxServiceT2()
{	
	static int ilPaxServiceT2 = -1;	
	static bool blIsPaxServiceT2 = false;
	if(ilPaxServiceT2 != -1)
	{
		return blIsPaxServiceT2;
	}
	CDWordArray olTplUrnos;
    GetTemplateFilters(olTplUrnos);
	TPLDATA* polTplData = NULL;

	if(strcmp(pcgHomeAirport,"SIN") == 0)
	{
		for(int i = 0; i < olTplUrnos.GetSize(); i++)
		{
			polTplData = ogTplData.GetTplByUrno(olTplUrnos.GetAt(i));
			if(polTplData != NULL)
			{
				if(strcmp(polTplData->Tnam, "PAX Service T2") == 0)
				{
					blIsPaxServiceT2 = true;
				}
				else
				{
					blIsPaxServiceT2 = false;
					break;
				}
			}
		}
	}
	ilPaxServiceT2 = 0;
	return blIsPaxServiceT2;
}
//Singapore-End

//Function meant for special handling of PRM Related Templates in Singapore airport
//Singapore-Begin
bool CCSBasicData::IsPRMRelatedTemplate()
{	
	static int ilPRMRelatedTemplate = -1;	
	static bool blIsPRMRelatedTemplate = false;
	if(ilPRMRelatedTemplate != -1)
	{
		return blIsPRMRelatedTemplate;
	}
	CDWordArray olTplUrnos;
    GetTemplateFilters(olTplUrnos);
	TPLDATA* polTplData = NULL;

	for(int i = 0; i < olTplUrnos.GetSize(); i++)
	{
		polTplData = ogTplData.GetTplByUrno(olTplUrnos.GetAt(i));
		if(polTplData != NULL)
		{
			int ilPRMTemp = omPRMRelatedTemplates.GetSize();

			for(int ilF = 0; ilF < ilPRMTemp; ilF++)
			{
				if(strcmp(polTplData->Tnam, omPRMRelatedTemplates[ilF]) == 0)
				{
					blIsPRMRelatedTemplate = true;
					return blIsPRMRelatedTemplate;
				}
				else
				{
					blIsPRMRelatedTemplate = false;
				}
			}
		}
	}
	ilPRMRelatedTemplate = 0;
	return blIsPRMRelatedTemplate;
}
//Singapore-End

BOOL CCSBasicData::WriteDialogToReg(const CWnd* ropWnd,const  CString& ropKey,const CRect& ropRect,const BOOL& rbpMinimized)
{
	BOOL blWriteDialogToReg = FALSE;
	if(ropWnd && ::IsWindow(ropWnd->m_hWnd) )
	{
		CRect olRect;
		if(ropRect == NULL)
		{
			ropWnd->GetWindowRect(&olRect);
		}
		else
		{
			olRect = ropRect;
		}
		int ilMinimized = rbpMinimized == TRUE ? 1 : 0;

		CWinApp* pApp = AfxGetApp();

		if (pApp)
		{
			if ((pApp->WriteProfileInt(ropKey, "Left", olRect.left) == TRUE)
				&& (pApp->WriteProfileInt(ropKey, "Top", olRect.top) == TRUE)
				&& (pApp->WriteProfileInt(ropKey, "Width", olRect.Width()) == TRUE)
				&& (pApp->WriteProfileInt(ropKey, "Height",  olRect.Height()) == TRUE)
				&& (pApp->WriteProfileInt(ropKey,"Minimized", ilMinimized) == TRUE))
			{
				blWriteDialogToReg = TRUE;
			}
		}
	}

	return blWriteDialogToReg;
}

BOOL CCSBasicData::GetDialogFromReg(CRect& ropRect, const CString& ropKey,BOOL& rbpMinimized)
{
	BOOL blGetDialogFromReg = FALSE;
	CWinApp* pApp = AfxGetApp();
	if (pApp && !ropKey.IsEmpty())
	{
		UINT uilleft = pApp->GetProfileInt(ropKey, "Left",  ropRect.left);
		UINT uiltop  = pApp->GetProfileInt(ropKey, "Top",   ropRect.top);
		UINT uilcx   = pApp->GetProfileInt(ropKey, "Width", ropRect.Width());
		UINT uilcy   = pApp->GetProfileInt(ropKey, "Height",ropRect.Height());

		UINT uilMinimized = pApp->GetProfileInt(ropKey,"Minimized",0);
		rbpMinimized = uilMinimized == 1 ? TRUE : FALSE;
		ropRect = CRect(uilleft,uiltop,uilleft+uilcx,uiltop+uilcy);
		blGetDialogFromReg = TRUE;
	}
	return blGetDialogFromReg;
}

//Implemented for Singapore Multiple monitor setup
void CCSBasicData::GetWindowPositionCorrect(CRect& ropRect) const
{
	int ilNoOfHorzMonitors = ::GetSystemMetrics(SM_CXMAXTRACK)/::GetSystemMetrics(SM_CXSCREEN);
	int ilNoOfVertMonitors = ::GetSystemMetrics(SM_CYMAXTRACK)/::GetSystemMetrics(SM_CYSCREEN);

	int ilPrevHorzMonitorPosition = ropRect.left > 0 ? ropRect.left/::GetSystemMetrics(SM_CXSCREEN) + 1 : 1;
	int ilPrevVertMonitorPosition = ropRect.top > 0 ? ropRect.top/::GetSystemMetrics(SM_CYSCREEN) + 1 : 1;

	if((ilNoOfHorzMonitors == ilPrevHorzMonitorPosition)
		&& (ilNoOfVertMonitors == ilPrevVertMonitorPosition))
	{
		return;
	}

	if(ropRect.right > (::GetSystemMetrics(SM_CXMAXTRACK) - 12))
	{
		int ilHorzOffset = ropRect.right - (::GetSystemMetrics(SM_CXMAXTRACK) - 12);
		
		ropRect.left   = (ropRect.left - ilHorzOffset) > 0 ? (ropRect.left - ilHorzOffset) : ropRect.left;
		ropRect.right  = (ropRect.right - ilHorzOffset) > 0 ? (ropRect.right - ilHorzOffset) : ropRect.right;
	}

	if(ropRect.bottom > (::GetSystemMetrics(SM_CYMAXTRACK) - 12))
	{
		int ilVertOffset = ropRect.bottom - (::GetSystemMetrics(SM_CYMAXTRACK) - 12);
		
		ropRect.top    = (ropRect.top - ilVertOffset) > 0 ? (ropRect.top - ilVertOffset) : ropRect.top;
		ropRect.bottom = (ropRect.bottom - ilVertOffset) > 0 ? (ropRect.bottom - ilVertOffset) : ropRect.bottom;
	}
}

bool CCSBasicData::IsPrmHandlingAgentEnabled()
{
	static bool blIsPrmHandlingAgentEnabled = false;
	static char pclIsPrm[14] = "";
	static char pclUseHag[14] = "";

	if (*pclIsPrm == '\0')
	{
		char pclConfigPath[142];
		if (getenv("CEDA") == NULL)
		{
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		}
		else
		{
			strcpy(pclConfigPath, getenv("CEDA"));
		}
		GetPrivateProfileString("GLOBAL","ISPRM","NO",pclIsPrm,sizeof(pclIsPrm),pclConfigPath);
		GetPrivateProfileString("GLOBAL","USEHAG","NO",pclUseHag,sizeof(pclUseHag),pclConfigPath);
		GetPrivateProfileString("GLOBAL","HandlingTypeForPRM","XXX",cmHandlingTypeForPRM,sizeof(cmHandlingTypeForPRM),pclConfigPath);
		if(strcmp(pclIsPrm,"YES") == 0 && strcmp(pclUseHag,"YES") == 0)
		{
			blIsPrmHandlingAgentEnabled = true;
		}
	}
	return blIsPrmHandlingAgentEnabled;
}

char *CCSBasicData::GetHandlingAgentForPrm()
{
	return cmHandlingTypeForPRM;
}

CString CCSBasicData::GetConfirmedPSMEntry()
{	
	static char pclPrmEntry[100] = "";

	if (*pclPrmEntry == '\0')
	{
		char pclConfigPath[142];
		if (getenv("CEDA") == NULL)
		{
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		}
		else
		{
			strcpy(pclConfigPath, getenv("CEDA"));
		}
		GetPrivateProfileString(pcgAppName,"CONFIRMEDPSM","PSM",pclPrmEntry,sizeof(pclPrmEntry),pclConfigPath);
	}
	return pclPrmEntry;
}

COLORREF CCSBasicData::GetConfirmedPSMColor()
{
	static char pclPrmColorEntry[15] = "";
	static long rgbColor = 0L;

	if (*pclPrmColorEntry == '\0')
	{
		char pclConfigPath[142];
		if (getenv("CEDA") == NULL)
		{
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		}
		else
		{
			strcpy(pclConfigPath, getenv("CEDA"));
		}
		GetPrivateProfileString(pcgAppName,"CONFIRMEDPSMCOLOR","0,0,0",pclPrmColorEntry,sizeof(pclPrmColorEntry),pclConfigPath);
		char *token;
		int ilRed=0, ilGreen=0, ilBlue=0, ilCount = 0;
		token = strtok(pclPrmColorEntry,",");
		while(token != NULL)
		{
			switch(ilCount)
			{
			case 0:
				ilRed = atoi(token);
				break;
			case 1:
				ilGreen = atoi(token);
				break;
			case 2:
				ilBlue = atoi(token);
				break;
			default:
				break;
			}
			ilCount++;
			token = strtok(NULL, ",");
		}
		rgbColor = RGB(ilRed,ilGreen,ilBlue);

	}
	return rgbColor;
}