#if !defined(AFX_POOLJOBDEMANDASSIGNMENTS_H__700CC9A2_3B5E_4935_AE93_A26AEF1C53CE__INCLUDED_)
#define AFX_POOLJOBDEMANDASSIGNMENTS_H__700CC9A2_3B5E_4935_AE93_A26AEF1C53CE__INCLUDED_

#include <CCSPtrArray.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PoolJobDemandAssignments.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPoolJobDemandAssignments dialog
struct POOJJOBDEMANDASSIGNMENTS
{
	JOBDATA *PoolJob;
	DEMANDDATA *Demand;

	POOJJOBDEMANDASSIGNMENTS(void)
	{
		PoolJob = NULL;
		Demand = NULL;
	}
};

class CPoolJobDemandAssignments
{
public:
	CPoolJobDemandAssignments();
	~CPoolJobDemandAssignments();
	POOJJOBDEMANDASSIGNMENTS *Add(JOBDATA *prpPoolJob, DEMANDDATA *prpDemand);
	bool PoolJobAssignmentExists(long lpPoolJobUrno);
	int CPoolJobDemandAssignments::GetCount(void);
	POOJJOBDEMANDASSIGNMENTS &operator [](int n) { return omData[n]; }
	JOBDATA *GetPoolJob(int ilIndex);
	DEMANDDATA *GetDemand(int ilIndex);

private:
	CCSPtrArray <POOJJOBDEMANDASSIGNMENTS> omData;
};

#endif // !defined(AFX_POOLJOBDEMANDASSIGNMENTS_H__700CC9A2_3B5E_4935_AE93_A26AEF1C53CE__INCLUDED_)
