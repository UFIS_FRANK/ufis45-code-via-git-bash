// reviewer.cpp : implementation file
//  
//
// Modification History:
// 19-Sep-96	Damkerng	Change many places since our database is changed.
//							Change the GetFmJobsByPrid() to be GetFmgJobsByAlid().
//							Since we now have a new job type FMJ for flight manager,
//							we don't use the field PRID to check if this pool job
//							is of a flight manager or a normal staff to create
//							managers information for each Regn area anymore. Instead,
//							we have a new separate job type FMG to do this function.
//							Check these places out by searching for "Id 19-Sep-96"

#include <stdafx.h>
#include <CCSGlobl.h>
#include <OpssPm.h>
#include <CCSCedaData.h>
#include <CCSCedaCom.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaAcrData.h>
#include <CedaTplData.h>
#include <CedaRudData.h>
#include <CedaRueData.h>
#include <gantt.h>
#include <gbar.h>
#include <CedaFlightData.h>
#include <CCSPtrArray.h>
#include <CedaFlightData.h>
#include <AllocData.h>
#include <cviewer.h>
#include <ccsddx.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <conflict.h>
#include <RegnViewer.h>
#include <conflict.h>
#include <ConflictConfigTable.h>
#include <BasicData.h>
#include <DataSet.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsWithIn(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))

static int CompareJobStartTime(const JOBDATA **e1, const JOBDATA **e2)
{
	if ((strcmp((**e1).Jtco,JOBPOOL) != 0) && (strcmp((**e2).Jtco,JOBPOOL) == 0) )
	{
		return -1;
	}
	else
	{
		if ((strcmp((**e1).Jtco,JOBPOOL) == 0) && (strcmp((**e2).Jtco,JOBPOOL) != 0) )
		{
			return 1;
		}
		else
		{
			//if ((**e1).Acfr.GetTime() == (**e2).Acfr.GetTime() == 0)
			
			if ((**e2).Acfr.GetTime() < (**e1).Acfr.GetTime())
				return -1;
			if ((**e2).Acfr.GetTime() > (**e1).Acfr.GetTime())
				return 1;
				return 0;

	//		return (int)((**e1).Acfr.GetTime() - (**e2).Acfr.GetTime());
		}
	}
}



/////////////////////////////////////////////////////////////////////////////
// RegnDiagramViewer
//
RegnDiagramViewer::RegnDiagramViewer()
{
	pomAttachWnd = NULL;
	omStartTime = TIMENULL;
	omEndTime = TIMENULL;
	omBkBrush.CreateSolidBrush(NAVY);
	omArrBkBrush.CreateSolidBrush(BLUE);
	omDepBkBrush.CreateSolidBrush(BLUE);

	ogCCSDdx.Register(this, FLIGHT_CHANGE,CString("REVIEWER"), CString("Flight Change"), RegnDiagramCf);
	
	ogCCSDdx.Register(this, JOB_NEW,	CString("REVIEWER"), CString("Job New"), RegnDiagramCf);
	ogCCSDdx.Register(this, JOB_CHANGE,	CString("REVIEWER"), CString("Job Changed"), RegnDiagramCf);
	ogCCSDdx.Register(this, JOB_DELETE,	CString("REVIEWER"), CString("Job Delete"), RegnDiagramCf);
	ogCCSDdx.Register(this, DEMAND_NEW,CString("REVIEWER"), CString("Dem New"),RegnDiagramCf);
	ogCCSDdx.Register(this, DEMAND_CHANGE,CString("REVIEWER"), CString("Dem changed"),RegnDiagramCf);
	ogCCSDdx.Register(this, DEMAND_DELETE,CString("REVIEWER"), CString("Dem deleted"),RegnDiagramCf);
	ogCCSDdx.Register(this, REDISPLAY_ALL,CString("REVIEWER"), CString("Global Update"), RegnDiagramCf);
	ogCCSDdx.Register(this, UPDATE_CONFLICT_SETUP,CString("REVIEWER"), CString("Update Conflict Setup"), RegnDiagramCf);
	ogCCSDdx.Register(this, FLIGHT_SELECT,CString("REVIEWER"), CString("Flight Select"), RegnDiagramCf);
}

RegnDiagramViewer::~RegnDiagramViewer()
{
	int ilTicks = clock();
	int ilTestOnly = CLOCKS_PER_SEC;
	TRACE("%d REViewer unregister started\n",ilTicks);
	ogCCSDdx.UnRegister(this, NOTUSED);
	ilTicks = clock();
	TRACE("%d REViewer unregister finished\n",ilTicks);

	DeleteAll();
}

void RegnDiagramViewer::Attach(CWnd *popAttachWnd)
{
	pomAttachWnd = popAttachWnd;
}

/////////////////////////////////////////////////////////////////////////////
// RegnDiagramViewer -- Filtering, Sorting, and Grouping
//
// Requirements:
// In the Regn Gantt Diagram we have two filters, by Regn Areas and Template.
// It will be always groupped and sorted by Regn Areas.
//
// Methods for change the view:
// ChangeViewTo		Change view, reload everything from Ceda????Data
// UpdateManagers	Rebuild the manager lists for all groups
//
// PrepareFilter	Prepare CMapStringToPtr or CMapPtrToPtr for filtering
// PrepareSorter	Prepare sort criterias before sorting take place
//
// IsPassFilter		Return TRUE if the given record data satisfies the filter
// CompareGroup		Return 0 or -1 or 1 as the result of comparison of two groups
// CompareLine		Return 0 or -1 or 1 as the result of comparison of two lines
// CompareManager	Return 0 or -1 or 1 as the result of comparison of two managers
//
// Programmer notes:
// At the program start up, we will create a default view for each dialog. This will
// overwrite the <default> view saved in the last time execution.
//
// When the viewer is constructed or everytimes the view changes, we will clear
// everything from the viewer buffer and do the insertion sorting. We will insert
// the record only if it satisfies our filter. Conceptually we will filter the record
// first, then sort in (by insertion sort), and group in.
//
// To let the filter work fast, we will implement a CMapStringToPtr for every filter
// condition. For example, let's say that we have a CStringArray for every possible
// values of a filter. We will have a CMapStringToPtr which could tell us the given
// key value is selected in the filter or not.
//
void RegnDiagramViewer::ChangeViewTo(const char *pcpViewName,
	BOOL bpIncludeArrivalFlights, BOOL bpIncludeDepartureFlights,
	CTime opStartTime, CTime opEndTime)
{
	DeleteAll();	// remove everything

	ogCfgData.rmUserSetup.RECV = pcpViewName;
	SelectView(pcpViewName);
	PrepareFilter();
	bmIncludeArrivalFlights = bpIncludeArrivalFlights;
	bmIncludeDepartureFlights = bpIncludeDepartureFlights;
	omStartTime = opStartTime;
	omEndTime = opEndTime;

	MakeGroupsAndLines();
	MakeManagers();
	MakeBkBars();
	MakeBars();
}

void RegnDiagramViewer::UpdateManagers(CTime opStartTime, CTime opEndTime)
{
	omStartTime = opStartTime;
	omEndTime = opEndTime;

	int ilGroupCount = GetGroupCount();
	for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
	{
	    while (GetManagerCount(ilGroupno) > 0)
		    DeleteManager(ilGroupno, 0);
	}
	MakeManagers();

	// Notifies the attached window (if exist)
	if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
	{
		for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
		{
			LONG lParam = MAKELONG(-1, ilGroupno);
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATEGROUP, lParam);
		}
	}
}

void RegnDiagramViewer::PrepareFilter()
{
	CStringArray olFilterValues;
	int i, n;

	// Prepare filter for all possible Regn areas
	omCMapForRegnArea.RemoveAll();
	GetFilter("RegnArea", olFilterValues);
	n = olFilterValues.GetSize();
	for (i = 0; i < n; i++)
		omCMapForRegnArea[olFilterValues[i]] = NULL;
			// we can use NULL here since what the map mapped to is unimportant

	// Prepare filter for all possible Template
	olFilterValues.RemoveAll();
	omCMapForTemplate.RemoveAll();
	GetFilter("Template", olFilterValues);
	n = olFilterValues.GetSize();
	for (i = 0; i < n; i++)
		omCMapForTemplate[olFilterValues[i]] = NULL;

}

BOOL RegnDiagramViewer::IsPassFilter(const char *pcpRegnArea)
{
	void *p;
	return omCMapForRegnArea.Lookup(CString(pcpRegnArea), p);
}

BOOL RegnDiagramViewer::IsPassFilterTemplate(const char *pcpTemplate)
{
	void *p;
	CString olTemplate = strlen(pcpTemplate) <= 0 ? GetString(IDS_WITHOUT_TEMPLATE) : CString(pcpTemplate);
	return omCMapForTemplate.Lookup(olTemplate, p);
}


int RegnDiagramViewer::CompareGroup(REGN_GROUPDATA *prpGroup1, REGN_GROUPDATA *prpGroup2)
{
	// Groups in RegnDiagram always ordered by the displayed text, so let's compare them.
	CString &s1 = prpGroup1->Text;
	CString &s2 = prpGroup2->Text;
	return (s1 == s2)? 0: (s1 > s2)? 1: -1;
}

int RegnDiagramViewer::CompareLine(REGN_LINEDATA *prpLine1, REGN_LINEDATA *prpLine2)
{
	// Lines in RegnDiagram always ordered by the displayed text, so let's compare them.
	CString &s1 = prpLine1->Text;
	CString &s2 = prpLine2->Text;
	return (s1 == s2)? 0: (s1 > s2)? 1: -1;
}

int RegnDiagramViewer::CompareManager(REGN_MANAGERDATA *prpManager1, REGN_MANAGERDATA *prpManager2)
{
	// Compare manager -- we will sort them by name
	return  (prpManager1->EmpLnam == prpManager2->EmpLnam)? 0:
		(prpManager1->EmpLnam > prpManager2->EmpLnam)? 1: -1;
}


/////////////////////////////////////////////////////////////////////////////
// RegnDiagramViewer -- Data for displaying graphical objects
//
// The related graphical class: RegnDiagram, RegnChart, and RegnGantt will use this
// RegnViewer as the source of data for displaying GanttLine and GanttBar objects.
//
// We let the previous section handles all filtering, sorting, and grouping. In this
// section, we will provide a set of methods which will help us to create the data
// represent the graphical GanttLine and GanttChart as easiest as possible.
//
// Methods for creating graphical objects:
// MakeGroupsAndLines		Create groups and lines that has to be displayed.
// MakeManagers				Create managers for all groups
// MakeBars					Create bars for all lines in all groups
// MakeManager				Create a manager for the specified group
// MakeBar					Create a flight bar (determine its length with demands)
// MakeIndicators			Create indicators for flight bar
//
// GetDemands				Get all demands for the given flight
//
// ArrivalFlightStatus		Return a string displayed for arrival flight
// DepartureFlightStatus	Return a string displayed for departure flight
// TurnAroundFlightStatus	Return a string displayed for turn-around flight
//
void RegnDiagramViewer::MakeGroupsAndLines()
{
	// reset all flights (and shadow bars) to not displayed
	int ilFlightCount = ogFlightData.omData.GetSize();
	for ( int ilLc = 0; ilLc < ilFlightCount; ilLc++)
	{
		ogFlightData.omData[ilLc].IsDisplayed = FALSE;
		ogFlightData.omData[ilLc].IsShadoBarDisplayed = FALSE;
	}

	// ogRegnAreas contains all metawerte for Regns areas

	// Create a group for each Regn area (only areas which satisfy the condition)
	CCSPtrArray <ALLOCUNIT> olRegnAreas;
	CCSPtrArray <ALLOCUNIT> olRegns;

	ogAllocData.GetGroupsByGroupType(ALLOCUNITTYPE_REGNGROUP,olRegnAreas);
	int ilNumRegnAreas = olRegnAreas.GetSize();
	for(int ilRegnArea = 0; ilRegnArea < ilNumRegnAreas; ilRegnArea++)
	{
		ALLOCUNIT *prlRegnArea = &olRegnAreas[ilRegnArea];

		if (!IsPassFilter(prlRegnArea->Name))	// this Regn area is not in the filter?
			continue;

		// create a chart for each group
        REGN_GROUPDATA rlGroup;
		rlGroup.RegnAreaId = prlRegnArea->Name;
		rlGroup.Text = prlRegnArea->Name;
		int ilGroupno = CreateGroup(&rlGroup);

		ogAllocData.GetUnitsByGroup(ALLOCUNITTYPE_REGNGROUP,prlRegnArea->Name,olRegns);
		int ilNumRegns = olRegns.GetSize();
		for(int ilRegn = 0; ilRegn < ilNumRegns; ilRegn++)
		{
			ALLOCUNIT *prlRegn = &olRegns[ilRegn];
			REGN_LINEDATA rlLine;
			rlLine.Text = prlRegn->Name;
			ACRDATA * polAcr = ogAcrData.GetAcrByName(prlRegn->Name);
			if (polAcr && (polAcr->Apui[0] == 'X' || polAcr->Apui[0] == 'x'))
			{
				rlLine.ApuInOp = TRUE;
			}
			else
			{
				rlLine.ApuInOp = FALSE;
			}
			rlLine.ThirdPartyFlightUrno = 0L;
			rlLine.IsStairCaseRegn = false;
            CreateLine(ilGroupno, &rlLine);
		}
	}
	imThirdPartyGroup = -1;
	CString olThirdParty = GetString(IDS_THIRDPARTY); // "Third Party"
	if(IsPassFilter(olThirdParty))
	{
		REGN_GROUPDATA rlGroup;
		rlGroup.RegnAreaId = olThirdParty; 
		rlGroup.Text = olThirdParty;
		imThirdPartyGroup = CreateGroup(&rlGroup);
		omThirdPartyRkeyMap.RemoveAll();

		int ilNumFlights = ogFlightData.omData.GetSize();
		for(int ilFlight = 0; ilFlight < ilNumFlights; ilFlight++)
		{
			FLIGHTDATA *prlFlight = &ogFlightData.omData[ilFlight];
			if(IsPassThirdPartyFilter(prlFlight) && !RotationAlreadyDisplayed(prlFlight))
			{
				CreateThirdPartyLine(prlFlight);
			}
		}
	}
}

int RegnDiagramViewer::CreateThirdPartyLine(FLIGHTDATA *prpFlight)
{
	int ilNewLine = -1;
	if(prpFlight != NULL)
	{
		if(prpFlight->Rkey != 0L)
		{
			// don't display each half of a rotation singularly
			omThirdPartyRkeyMap.SetAt((void *)prpFlight->Rkey,NULL);
		}

		REGN_LINEDATA rlLine;
		FLIGHTDATA  *prlRotation = ogFlightData.GetRotationFlight(prpFlight);

		// display the arrival for arrivals and turnarounds else display the departure
		if(ogFlightData.IsArrival(prpFlight) || prlRotation == NULL) // arrival or single departure
		{
			CTime olTime = ogFlightData.GetFlightTime(prpFlight);
			rlLine.Text.Format("%s/%s",prpFlight->Fnum,olTime != TIMENULL ? olTime.Format("%d") : "");
			rlLine.ThirdPartyFlightUrno = prpFlight->Urno;
		}
		else if(prlRotation != NULL) // rotation is the arrival
		{
			CTime olTime = ogFlightData.GetFlightTime(prlRotation);
			rlLine.Text.Format("%s/%s",prlRotation->Fnum,olTime != TIMENULL ? olTime.Format("%d") : "");
			rlLine.ThirdPartyFlightUrno = prlRotation->Urno;
		}

		ilNewLine = CreateLine(imThirdPartyGroup, &rlLine);
	}

	return ilNewLine;
}

// update the list of third party flights after the selection filter has been changed in ThirdPartyFilterDlg
void RegnDiagramViewer::UpdateThirdPartyGroup()
{
	omThirdPartyRkeyMap.RemoveAll();
	if(imThirdPartyGroup >=0 && imThirdPartyGroup < omGroups.GetSize())
	{
		FLIGHTDATA *prlFlight;

		// first delete any lines that no longer satisfy the filter
		int ilNumLines = omGroups[imThirdPartyGroup].Lines.GetSize();
		for(int ilLine = (ilNumLines-1); ilLine >= 0; ilLine--)
		{
			prlFlight = ogFlightData.GetFlightByUrno(omGroups[imThirdPartyGroup].Lines[ilLine].ThirdPartyFlightUrno);
			if(prlFlight == NULL || !IsPassThirdPartyFilter(prlFlight))
			{
				DeleteThirdPartyFlight(ilLine);
			}
		}

		// add any flights added to the selection filter in ThirdPartyFilterDlg
		long llFlightUrno;
		void *pvlDummy;
		for(POSITION rlPos = omThirdPartyFlights.GetStartPosition(); rlPos != NULL; )
		{
			omThirdPartyFlights.GetNextAssoc(rlPos, (void *&)llFlightUrno, (void *&)pvlDummy);
			if(GetLineFlightInThirdPartyList(llFlightUrno) == -1 && (prlFlight = ogFlightData.GetFlightByUrno(llFlightUrno)) != NULL)
			{
				// line not already displayed so add it
				int ilNewLine = AddThirdPartyFlight(prlFlight);
			}
		}
	}
}

bool RegnDiagramViewer::RotationAlreadyDisplayed(FLIGHTDATA *prpFlight)
{
	bool blRotationAlreadyDisplayed = false;
	void *pvlDummy;
	if(prpFlight != NULL && omThirdPartyRkeyMap.Lookup((void *) prpFlight->Rkey, pvlDummy))
	{
		blRotationAlreadyDisplayed = true;
	}
	return blRotationAlreadyDisplayed;
}

void RegnDiagramViewer::DeleteThirdPartyFlight(int ipLine)
{
	if(ipLine != -1)
	{
		DeleteLine(imThirdPartyGroup, ipLine);
		LONG lParam = MAKELONG(ipLine, imThirdPartyGroup);
		pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_DELETELINE, lParam);
	}
}

int RegnDiagramViewer::AddThirdPartyFlight(FLIGHTDATA *prpFlight)
{
	int ilNewLine = -1;
	if(prpFlight != NULL)
	{
		if((ilNewLine = CreateThirdPartyLine(prpFlight)) != -1)
		{
			MakeBkBar(imThirdPartyGroup, ilNewLine, prpFlight);
			MakeBarsByLine(imThirdPartyGroup, ilNewLine);

			LONG lParam = MAKELONG(ilNewLine, imThirdPartyGroup);
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_INSERTLINE, lParam);
		}
	}

	return ilNewLine;
}

void RegnDiagramViewer::UpdateThirdPartyFlight(FLIGHTDATA *prpFlight, int ipLine)
{
	if(prpFlight != NULL && ipLine != -1)
	{
		REGN_LINEDATA *prlLine = &omGroups[imThirdPartyGroup].Lines[ipLine];
		prlLine->BkBars.DeleteAll();
		while (prlLine->Bars.GetSize() > 0)
		{
			DeleteBarFromBarMap(prlLine->Bars[0].DemandUrno);
			prlLine->Bars[0].Indicators.DeleteAll();
			prlLine->Bars.DeleteAt(0);
		}
		MakeBkBar(imThirdPartyGroup, ipLine, prpFlight);
		MakeBarsByLine(imThirdPartyGroup, ipLine);

		LONG lParam = MAKELONG(ipLine, imThirdPartyGroup);
		pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_UPDATELINE, lParam);
	}
}

void RegnDiagramViewer::ProcessThirdPartyFlightChange(FLIGHTDATA *prpFlight)
{
	if(prpFlight != NULL && imThirdPartyGroup >=0 && imThirdPartyGroup < omGroups.GetSize())
	{
		int ilLine = GetLineFlightInThirdPartyList(prpFlight->Urno);
		if(IsPassThirdPartyFilter(prpFlight))
		{
			// this is a third party flight with demands check if it needs to be added or updated
			if(ilLine == -1)
			{
				if(!RotationAlreadyDisplayed(prpFlight))
				{
					// line not already displayed so add it
					AddThirdPartyFlight(prpFlight);
				}
			}
			else
			{
				UpdateThirdPartyFlight(prpFlight,ilLine);
			}
		}
		else if(ilLine != -1)// no longer a third party flight but is displayed so remove it
		{
			DeleteThirdPartyFlight(ilLine);
		}
	}
}

// return true if the flight in lpFlightUrno is displayed in the list of third party flights
int RegnDiagramViewer::GetLineFlightInThirdPartyList(long lpFlightUrno)
{
	int ilLineFound = -1;

	if(imThirdPartyGroup >=0 && imThirdPartyGroup < omGroups.GetSize())
	{
		int ilNumLines = omGroups[imThirdPartyGroup].Lines.GetSize();
		for(int ilLine = 0; ilLine < ilNumLines; ilLine++)
		{
			if(omGroups[imThirdPartyGroup].Lines[ilLine].ThirdPartyFlightUrno == lpFlightUrno)
			{
				ilLineFound = ilLine;
				break;
			}
		}
	}
	return ilLineFound;
}

bool RegnDiagramViewer::IsPassThirdPartyFilter(FLIGHTDATA *prpFlight)
{
	bool blIsPassFilter = false;
	void *prlDummy;

	if(prpFlight != NULL)
	{
		if(omThirdPartyFlights.Lookup((void *) prpFlight->Urno, prlDummy))
		{
			blIsPassFilter = true;
		}
		else if(ogFlightData.IsThirdPartyFlight(prpFlight))
		{
			// create a list of third party flights (ALC2 <> "AZ") that don't
			// already have demands - select demands both by flight URNO and by
			// registration (some registration demands my not have a flight URNO)
			CCSPtrArray <DEMANDDATA> olDemands;
			ogDemandData.GetDemandsByFlur(olDemands,prpFlight->Urno);
/*			if(olDemands.GetSize() == 0)
			{
				ACRDATA *prlAcr = ogAcrData.GetAcrByName(prpFlight->Regn);
				if(prlAcr != NULL)
				{
					ogDemandData.GetDemandsByUref(olDemands,prlAcr->Urno, ACRURNO);
				}
			} */
			if(olDemands.GetSize() > 0)
			{
				blIsPassFilter = true;
			}
		}
	}

	return blIsPassFilter;
}

void RegnDiagramViewer::MakeManagers()
{
	int ilGroupCount = GetGroupCount();
	for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
	{
		// Create managers associated with this CCI area
		CCSPtrArray<JOBDATA> olFmgJobs;
		ogJobData.GetFmrJobsByAlid(olFmgJobs, GetGroup(ilGroupno)->RegnAreaId);
		int ilFmgCount = olFmgJobs.GetSize();
		for (int ilLc = 0; ilLc < ilFmgCount; ilLc++)
			MakeManager(ilGroupno, &olFmgJobs[ilLc]);
	}
}

void RegnDiagramViewer::MakeBars()
{
	int ilGroupCount = GetGroupCount();
	for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
	{
		int ilLineCount = GetLineCount(ilGroupno);
		for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
		{
			MakeBarsByLine(ilGroupno, ilLineno);
		}
	}
}

void RegnDiagramViewer::MakeBarsByLine(int ipGroupno, int ipLineno)
{
	int ilLc;
	DEMANDDATA* prlDemand;
	CString olTemplateName;
	CCSPtrArray<DEMANDDATA> olDemands;

	REGN_LINEDATA *prlLine = GetLine(ipGroupno, ipLineno);

	CString olRegn = "";
	
	if(GetGroup(ipGroupno)->Text == GetString(IDS_THIRDPARTY))
	{
		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prlLine->ThirdPartyFlightUrno);
		if(prlFlight != NULL)
		{
			ogDemandData.GetDemandsByFlur(olDemands,prlLine->ThirdPartyFlightUrno);
			olRegn = prlFlight->Regn;
			prlFlight = ogFlightData.GetRotationFlight(prlLine->ThirdPartyFlightUrno);
			if(prlFlight != NULL)
			{
				// for rotations ThirdPartyFlightUrno is for the arrival but turnaround demands
				// are linked to the departure so select the demands for the dep flight as well
				ogDemandData.GetDemandsByFlur(olDemands,prlFlight->Urno);
			}
		}
		if(olDemands.GetSize() == 0)
		{
			ACRDATA *prlAcr = ogAcrData.GetAcrByName(olRegn);
			if(prlAcr != NULL)
			{
//				ogDemandData.GetDemandsByUref(olDemands,prlAcr->Urno, ACRURNO);
			}
		}
	}
	else
	{
		olRegn = prlLine->Text;
		GetDemandsByRegn(olDemands, GetLineText(ipGroupno, ipLineno));
	}

	// for setting debug breakpoints
	if (olRegn == "IDATF")
	{
		int iiii = 0;
	}

	
	int ilNumDemands = olDemands.GetSize();
	CCSPtrArray<DEMANDDATA> olDisplayDemands;
	for (ilLc = 0;ilLc < ilNumDemands; ilLc++)
	{
		prlDemand = & olDemands[ilLc];

		olTemplateName.Empty();

		if (prlDemand->Debe == TIMENULL || prlDemand->Deen == TIMENULL)
		{
			TRACE("Demand URNO <%d>, Da scartare perch� Debe o Deen non validi.\n",prlDemand->Urno);
			continue;
		}
		
		if(*prlDemand->Dety == '3' || *prlDemand->Dety == '2' || *prlDemand->Dety == '1' || *prlDemand->Dety == '0')
		{
			GetTemplateNameByDemand(prlDemand, olTemplateName);
		}
		if(IsPassFilterTemplate(olTemplateName))
		{
			// for demands with flights - display one bar representing the start of the earliest
			// duty and the end of the latest duty
			// for demands without flight - display each demand
			bool blNotFound = true;
			if(prlDemand->Ouri != 0L || prlDemand->Ouro != 0L)
			{
				int ilNumDisplayDemands = olDisplayDemands.GetSize();
				for(int ilD = 0; blNotFound && ilD < ilNumDisplayDemands; ilD++)
				{
					DEMANDDATA *prlDisplayDemand = &olDisplayDemands[ilD];
					if(prlDisplayDemand->Ouri == prlDemand->Ouri && prlDisplayDemand->Ouro == prlDemand->Ouro )
					{
						if (prlDisplayDemand->Debe == TIMENULL || prlDisplayDemand->Debe > prlDemand->Debe)
							prlDisplayDemand->Debe = prlDemand->Debe;
						if (prlDisplayDemand->Deen == TIMENULL || prlDisplayDemand->Deen < prlDemand->Deen)
							prlDisplayDemand->Deen = prlDemand->Deen;
						blNotFound = false;
						AddToBarMap(prlDisplayDemand->Urno, prlDemand->Urno);
					}
				}
			}
			if(blNotFound)
			{
				DEMANDDATA *prlNewDemand = new DEMANDDATA;
				*prlNewDemand = *prlDemand;
				olDisplayDemands.Add(prlNewDemand);
				AddToBarMap(prlNewDemand->Urno, prlNewDemand->Urno);
			}
		}
	}

	if(olDisplayDemands.GetSize() > 0)
	{
		int ilNumDisplayDemands = olDisplayDemands.GetSize();
		for(int ilD = 0; ilD < ilNumDisplayDemands; ilD++)
		{
			MakeBar(ipGroupno, ipLineno, &olDisplayDemands[ilD]);
		}
		olDisplayDemands.DeleteAll();
	}

}

void RegnDiagramViewer::MakeBkBars()
{
	int ilLc;
	int NumFlights;
	FLIGHTDATA* pFlight;
	int ilGroupCount = GetGroupCount();
	for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
	{
		int ilLineCount = GetLineCount(ilGroupno);
		for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
		{
			CCSPtrArray<FLIGHTDATA> olFlights;
			bool blThirdPartyFlight = false;

			FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(GetLine(ilGroupno, ilLineno)->ThirdPartyFlightUrno);
			if(prlFlight != NULL)
			{
				olFlights.Add(prlFlight);
				blThirdPartyFlight = true;
			}
			else
			{
				ogFlightData.GetFlightsByRegn(olFlights, GetLineText(ilGroupno, ilLineno));
			}

			
			NumFlights = olFlights.GetSize();
			for (ilLc = 0; ilLc < NumFlights; ilLc++)
			{
				pFlight = (FLIGHTDATA *)&(olFlights[ilLc]);
				if (*pFlight->Ftyp != 'X') // check for cancellation
				{
					if (CString(pFlight->Regn) == GetLineText(ilGroupno, ilLineno) || blThirdPartyFlight)
					{
						MakeBkBar(ilGroupno, ilLineno, pFlight);
					}
				}
			} 
		}
	}
}


void RegnDiagramViewer::MakeManager(int ipGroupno, JOBDATA *prpJob)
{
	if (!IsOverlapped(prpJob->Acfr, prpJob->Acto, omStartTime, omEndTime))
		return;
	
	REGN_MANAGERDATA rlFm;
	rlFm.JobUrno = prpJob->Urno;
	SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prpJob->Shur);
	rlFm.ShiftStid = prlShift != NULL ? prlShift->Sfca : "";
	rlFm.ShiftTmid = prlShift != NULL ? prlShift->Egrp : "";
	EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prpJob->Peno);
	rlFm.EmpLnam = CString(prlEmp != NULL ? prlEmp->Lanm : "");
	if (prlShift != NULL)
	{
		if ((prlShift->Avfa < prpJob->Acfr) || (prlShift->Avta > prpJob->Acto))
		{
			rlFm.ShiftStid += "'";
		}
	}
	CreateManager(ipGroupno, &rlFm);
}

// NOTE: the demand received here is not a real demand but a conglomerate
// of all demands for the flight - DO NOT CALL this function directly - use MakeBarsByLine()
void RegnDiagramViewer::MakeBar(int ipGroupno, int ipLineno, DEMANDDATA *prpDemand)
{
	if(prpDemand != NULL)
	{
		REGN_BARDATA rlBar;
		BOOL blIsPlanned = FALSE;
		BOOL blIsInFuture = FALSE;
		bool blIsFinished = false;
		bool blIsOnlyDeparture = false;
		bool blIsTurnAround = false;


		FLIGHTDATA *prlFlightArr = ogFlightData.GetFlightByUrno(prpDemand->Ouri);
		FLIGHTDATA *prlFlightDep = ogFlightData.GetFlightByUrno(prpDemand->Ouro);
		int ilColorIndex = FIRSTCONFLICTCOLOR+(CFI_NOCONFLICT*2);

		if (prlFlightArr == NULL && prlFlightDep == NULL)
		{
			CCSPtrArray<JOBDATA> olJobs;
			ogJodData.GetJobsByDemand(olJobs, prpDemand->Urno);
			int ilNumJobs = olJobs.GetSize();
			for (int ilJob = 0; ilJob < ilNumJobs; ilJob++)
			{
				if(olJobs[ilJob].Stat[0] == 'P')
				{
					blIsPlanned = TRUE;
				}
			}

			// the conflict is "DemandWithoutFlight"
			ilColorIndex = ogConflicts.GetDemandConflictColor(prpDemand);

			//rlBar.StatusText.Format("Flight Not Found Ouri <%ld> Ouro <%ld> Urno <%ld>",prpDemand->Ouri,prpDemand->Ouro,prpDemand->Urno);
			rlBar.StatusText.Format(GetString(IDS_NOFLIGHTDEMAND),prpDemand->Ouri,prpDemand->Ouro,prpDemand->Urno);
			rlBar.Delay = 0;
			rlBar.IsDelayed = false;
			rlBar.DepartureUrno = 0;
			rlBar.FlightUrno = 0;
			rlBar.IsTmo = false;
		}
		else
		{
			if (prlFlightArr != NULL && prlFlightDep != NULL)
				blIsTurnAround = true;

			if(prlFlightArr == NULL)
			{
				if(bmIncludeDepartureFlights == false)
					return;
				prlFlightArr = prlFlightDep;
				blIsOnlyDeparture = true;
			}
			else if(prlFlightDep == NULL && bmIncludeArrivalFlights == false)
			{
				return;
			}

			if (blIsTurnAround)
			{
				ilColorIndex = ogConflicts.GetRotationConflictColor(prlFlightArr,prlFlightDep,ALLOCUNITTYPE_REGN,PERSONNELDEMANDS);
				blIsPlanned = prlFlightArr->RegnIsPlanned || prlFlightDep->RegnIsPlanned;
				blIsInFuture = ogFlightData.IsArrival(prlFlightArr) ? prlFlightArr->RegnIsPlanned : prlFlightDep->RegnIsPlanned;
				blIsFinished = ogBasicData.IsFinished(prlFlightArr,ALLOCUNITTYPE_REGN) && ogBasicData.IsFinished(prlFlightDep,ALLOCUNITTYPE_REGN);
			}
			else
			{
				ilColorIndex = ogConflicts.GetFlightConflictColor(prlFlightArr,ALLOCUNITTYPE_REGN, PERSONNELDEMANDS);
				blIsPlanned = prlFlightArr->RegnIsPlanned;
				blIsInFuture = prlFlightArr->RegnIsPlanned;
				blIsFinished = ogBasicData.IsFinished(prlFlightArr,ALLOCUNITTYPE_REGN);
			}
			if (blIsFinished)
			{
				ilColorIndex = 2; // gray for flights whith all jobs finished
			}

			rlBar.StatusText = TurnAroundFlightStatus(prlFlightArr, prlFlightDep);

			if (prlFlightDep)
			{
				rlBar.Delay = prlFlightDep->Etod - prlFlightDep->Stod;
				rlBar.IsDelayed = prlFlightDep->IsDelayed;
				rlBar.DepartureUrno = prlFlightDep->Urno;
			}
			else
			{
				rlBar.Delay = prlFlightArr->Etoa - prlFlightArr->Stoa;
				rlBar.IsDelayed = prlFlightArr->IsDelayed;
				rlBar.DepartureUrno = 0;
			}
			rlBar.IsTmo = prlFlightArr->IsTmo;
		}

		rlBar.StartTime = prpDemand->Debe;
		rlBar.EndTime = prpDemand->Deen;
		rlBar.DemandUrno = prpDemand->Urno;
		rlBar.Type = BAR_REGNFLIGHT;
		rlBar.Text = "";
		rlBar.NoDropOnThisBar = FALSE;
		rlBar.MarkerType = (blIsPlanned)? MARKLEFT: (blIsInFuture)? MARKNONE: MARKFULL;
		rlBar.MarkerBrush = ogBrushs[ilColorIndex];

		if(prlFlightArr != NULL)
		{
			rlBar.FlightUrno = prlFlightArr->Urno;
		}
		
		rlBar.FrameType = FRAMERECT;
		rlBar.IsShadowBar = FALSE;

		int ilBarno = CreateBar(ipGroupno, ipLineno, &rlBar);

		CCSPtrArray <DEMANDDATA> olDemands;
		GetDemandsForBar(rlBar.DemandUrno, olDemands);
//		ogDemandData.GetDemandsByFlur(olFlightDemands,prpDemand->Ouri, "", ALLOCUNITTYPE_REGN);
//		ogDemandData.GetDemandsByFlur(olFlightDemands,prpDemand->Ouro, "", ALLOCUNITTYPE_REGN);
//		ogDemandData.GetDemandsByFlur(olFlightDemands,prpDemand->Ouri);
//		ogDemandData.GetDemandsByFlur(olFlightDemands,prpDemand->Ouro);

		if(olDemands.GetSize() <= 0)
		{
			olDemands.Add(prpDemand);
		}
		MakeIndicators(ipGroupno, ipLineno, ilBarno, olDemands);
	}
}

bool RegnDiagramViewer::MakeBkBar(int ipGroupno, int ipLineno, FLIGHTDATA *prpFlight)
{
	bool blUpdated = false;

	if (prpFlight->IsDisplayed == TRUE)
	{
		return blUpdated;
	}
	
	// if we display the flight bar, we have also to display the corresponding shadow bar
	prpFlight->IsShadoBarDisplayed = FALSE;

	bool blIsArrival = ogFlightData.IsArrival(prpFlight);		// Inbound flight
	bool blIsDeparture = ogFlightData.IsDeparture(prpFlight);	// Outbount flight
	CTime olArrivalStartTime, olArrivalEndTime;
	CTime olDepartureStartTime, olDepartureEndTime;
	FLIGHTDATA *prlRotationFlight = ogFlightData.GetRotationFlight(prpFlight);
	CString olBarText;

	REGN_BKBARDATA rlBkBar;

	if (prlRotationFlight && blIsArrival)
	{
		if (prpFlight->Tifd == TIMENULL)
		{
			return blUpdated;
		}
		rlBkBar.FlightUrno = prpFlight->Urno;
		rlBkBar.FlightRotationUrno = prlRotationFlight->Urno;
		rlBkBar.TextArr = prpFlight->Psta;
		rlBkBar.TextDep = prlRotationFlight->Pstd;
		rlBkBar.StatusText = TurnAroundFlightStatus(prpFlight, prlRotationFlight);
		rlBkBar.StartTime = prpFlight->Tifa;
		rlBkBar.EndTime = prlRotationFlight->Tifd;
		rlBkBar.MarkerBrush = &omBkBrush;
		blUpdated = true;
	}
	else
	if (blIsArrival)
	{
		if (bmIncludeArrivalFlights == false)
		{
			return blUpdated;
		}
		rlBkBar.FlightUrno = prpFlight->Urno;
		rlBkBar.FlightRotationUrno = 0;
		rlBkBar.TextArr = prpFlight->Psta;  // Position arrival
		rlBkBar.TextDep = "";
		rlBkBar.StatusText = ArrivalFlightStatus(prpFlight, prlRotationFlight);
		rlBkBar.StartTime = prpFlight->Tifa;
		rlBkBar.EndTime = prpFlight->Tifa + (ogBasicData.imSingleBarLen == 0 ? CTimeSpan(3,0,0,0) : CTimeSpan(0,0,ogBasicData.imSingleBarLen,0));
		rlBkBar.MarkerBrush = &omArrBkBrush;
		blUpdated = true;
	}
	else
	if (blIsDeparture && !prlRotationFlight)
	{
		if (bmIncludeDepartureFlights == false)
		{
			return blUpdated;
		}
		rlBkBar.FlightUrno = prpFlight->Urno;
		rlBkBar.FlightRotationUrno = 0;
		rlBkBar.TextDep = prpFlight->Pstd;  // Position departure
		rlBkBar.TextArr = "";
		rlBkBar.StatusText = DepartureFlightStatus(prpFlight, prlRotationFlight);
		rlBkBar.EndTime = prpFlight->Tifd;
		rlBkBar.StartTime = prpFlight->Tifd - (ogBasicData.imSingleBarLen == 0 ? CTimeSpan(3,0,0,0) : CTimeSpan(0,0,ogBasicData.imSingleBarLen,0));
		rlBkBar.MarkerBrush = &omDepBkBrush;
		blUpdated = true;
	}

	if(blUpdated)
	{
		int ilBkBarno = CreateBkBar(ipGroupno, ipLineno, &rlBkBar);
	}

	return blUpdated;
}


void RegnDiagramViewer::MakeBarWithoutDemand(int ipGroupno, int ipLineno, FLIGHTDATA *prpFlight)
{
	if (ogBasicData.bmDemandsOnly)
		return;
	FLIGHTDATA *prlRotationFlight = ogFlightData.GetRotationFlight(prpFlight);

	bool blIsArrival = ogFlightData.IsArrival(prpFlight);// Inbound flight
	bool blIsDeparture = ogFlightData.IsDeparture(prpFlight);// Outbound flight

	REGN_BARDATA rlBar;
	rlBar.IsDelayed = CFST_NOTSET;
	if (blIsArrival)
	{
		if (prpFlight->Stoa == TIMENULL)
			return;
		rlBar.Text = prpFlight->Fnum;
		rlBar.StatusText = ArrivalFlightStatus(prpFlight, prlRotationFlight);
		rlBar.StartTime = prpFlight->Tifa;
		rlBar.EndTime = prpFlight->Stoa +  CTimeSpan(0,0,20,0);
		//rlBar.EndTime = prlRotationFlight->Tifd;
		rlBar.Delay = CTimeSpan(0);
		rlBar.IsTmo = prpFlight->IsTmo;
	}
	else if (blIsDeparture)
	{
		if (prpFlight->Stod == TIMENULL)
			return;
		rlBar.Text = prpFlight->Fnum;
		rlBar.StatusText = DepartureFlightStatus(prpFlight, prlRotationFlight);
		rlBar.StartTime = prpFlight->Stod - CTimeSpan(0,1,0,0);
		rlBar.EndTime = prpFlight->Etod != TIMENULL ? prpFlight->Etod : prpFlight->Stod;
		if (prpFlight->IsDelayed != CFST_NOTSET)
		{
			rlBar.IsDelayed = prpFlight->IsDelayed;
			rlBar.Delay = prpFlight->Etod - prpFlight->Stod;
		}
		else
			rlBar.Delay = CTimeSpan(0);
		rlBar.IsTmo = FALSE;
	}
	
	rlBar.MarkerType = MARKFULL;
	rlBar.FlightUrno = prpFlight->Urno;
	rlBar.DepartureUrno = prpFlight->Rkey;
	rlBar.FrameType = FRAMERECT;
	rlBar.IsShadowBar = FALSE;
	rlBar.Type = BAR_REGNFLIGHT;
	rlBar.NoDropOnThisBar = FALSE;
	rlBar.MarkerBrush = ogBrushs[prpFlight->ColorIndex];
	int ilBarno = CreateBar(ipGroupno, ipLineno, &rlBar);

	// Create indicators associated with this flight
	// not here, no demand, no indicator
}


void RegnDiagramViewer::MakeIndicators(int ipGroupno, int ipLineno, int ipBarno, CCSPtrArray<DEMANDDATA> &ropDemands)
{
	int i;
	long llJobu;
	CCSPtrArray<JOBDATA> olJobs;
	CCSPtrArray<DEMANDDATA> olDemands;

	for (i = 0; i < ropDemands.GetSize(); i++)	// for each arrival flight?
	{
		REGN_INDICATORDATA rlIndicator;
		rlIndicator.StartTime = ropDemands[i].Debe;
		rlIndicator.EndTime = ropDemands[i].Deen;
		rlIndicator.IsArrival = TRUE;
		rlIndicator.DemandUrno = ropDemands[i].Urno;
		rlIndicator.Color = 0;
		// MCU 07.07 the Indicator Color depends on the Color of the job
		// assigned to this demand, if no job is assigned, the color comes from
		// conflict CFI_NOTCOVERED
		if ((llJobu = ogJodData.GetFirstJobUrnoByDemand(ropDemands[i].Urno)) != 0)
		{
			JOBDATA *prlJob = ogJobData.GetJobByUrno(llJobu);
			if (prlJob != NULL)
			{
				rlIndicator.Color = ogColors[prlJob->ColorIndex];
				rlIndicator.StartTime = prlJob->Acfr;
				rlIndicator.EndTime = prlJob->Acto;
			}
		}
		if (rlIndicator.Color == 0)
		{
			rlIndicator.Color = ogColors[FIRSTCONFLICTCOLOR+(CFI_PST_NOTCOVERED*2)];	// just use simply red for now
		}
		CreateIndicator(ipGroupno, ipLineno, ipBarno, &rlIndicator);
	}
}


void RegnDiagramViewer::MakeSpecialBar(int ipGroupno, int ipLineno, JOBDATA *prpJob)
{
	REGN_BARDATA rlBar;
	EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prpJob->Peno);

	if (prlEmp != NULL)
	{
		CString olEmpNam = ogEmpData.GetEmpName(prlEmp);
		rlBar.Text = olEmpNam;
		rlBar.StatusText = prpJob->Acfr.Format("%H%M") + "-" + prpJob->Acto.Format("%H%M")
			+ olEmpNam; 
	}
	SHIFTDATA *prlShift;
	JOBDATA *prlDutyJob = ogJobData.GetJobByUrno(prpJob->Jour);
	if (prlDutyJob != NULL)
	{
		prlShift = ogShiftData.GetShiftByUrno(prlDutyJob->Shur);
		if (prlShift != NULL)
		{
			rlBar.StatusText += CString(prlShift->Sfca) + " " + prlShift->Egrp;
		}
	}
	rlBar.StartTime = prpJob->Acfr;
	rlBar.EndTime = prpJob->Acto;

	rlBar.MarkerType = (prpJob->Stat[0] == 'P')? MARKLEFT: MARKFULL;
	rlBar.MarkerBrush = ogBrushs[prpJob->ColorIndex];
	rlBar.FlightUrno = prpJob->Urno;
	rlBar.DepartureUrno = -1;
	rlBar.FrameType = FRAMERECT;
	rlBar.IsShadowBar = FALSE;
	rlBar.Type = BAR_REGNSPECIAL;
	rlBar.IsTmo = FALSE;
	rlBar.IsDelayed = FALSE;
	rlBar.Delay = CTimeSpan(0);

	int ilBarno = CreateBar(ipGroupno, ipLineno, &rlBar);
	
	// no Indicators
}

int RegnDiagramViewer::GetDemandsByRegn(CCSPtrArray<DEMANDDATA> &ropDemands, const char*pcpRegn)
{
	ropDemands.RemoveAll();
 	ACRDATA *pAcr = ogAcrData.GetAcrByName(pcpRegn);
	if (!pAcr)
		return -1;
	ogDataSet.GetDemandsByUref(ropDemands, pAcr->Urno, ACRURNO);


/*	// there are also normal demands with aloc REGN but not P05/RES/CAS
	// these are flight related so need to load them for each flight with this registration
	CCSPtrArray<FLIGHTDATA> olFlights;
	ogFlightData.GetFlightsByRegn(olFlights, pcpRegn);
	int ilNumFlights = olFlights.GetSize();
	for(int ilFlight = 0; ilFlight < ilNumFlights; ilFlight++)
	{
		ogDemandData.GetDemandsByFlur(ropDemands,olFlights[ilFlight].Urno, "", ALLOCUNITTYPE_REGN);
	}
*/

	if (ropDemands.GetSize() == 0)
		return -1;
	else
		return 0;

}

int RegnDiagramViewer::GetTemplateNameByDemand(DEMANDDATA *prpDemand, CString &TplName)
{
	RUDDATA *pRud = ogRudData.GetRudByUrno(prpDemand->Urud);
	if (!pRud)
		return -1;
	RUEDATA *pRue = ogRueData.GetRueByUrno(pRud->Urue);
	if (!pRue)
		return -1;
	TPLDATA *pTpl = ogTplData.GetTplByUrno(pRue->Utpl);
	if (!pTpl)
		return -1;
	TplName = pTpl->Tnam;

	return 0;

}

CString RegnDiagramViewer::ArrivalFlightStatus(FLIGHTDATA *prpFlight,
	FLIGHTDATA *prpRotationFlight)
{
	char buf[512];

	sprintf(buf, "%s / %s /         F%dC%dY%d          %s         %s %s/%s/%s            PAX:  %d               %s --> %s %s",
		prpFlight->Regn, prpFlight->Act3,
		prpFlight->VERF, prpFlight->VERC, prpFlight->VERM,
		prpFlight->Fnum, prpFlight->Org3,
		(const char *)prpFlight->Stoa.Format("%H%M"),
		(const char *)prpFlight->Etoa.Format("%H%M"),
		(const char *)prpFlight->Onbl.Format("%H%M"),
		0, prpFlight->Regn,
		prpRotationFlight? prpRotationFlight->Fnum: "",
		prpRotationFlight? prpRotationFlight->Regn: "");

	if (prpFlight->IsTmo)
	{
		CString olTmo = CString("  TMO --> ") + prpFlight->Tmoa.Format("%H%M");
		strcat(buf,olTmo);
	}

	return buf;
}

CString RegnDiagramViewer::DepartureFlightStatus(FLIGHTDATA *prpFlight,
	FLIGHTDATA *prpRotationFlight)
{
	char buf[512];
	sprintf(buf, "%s / %s           F%dC%dY%d       %s   %s --> %s   %s    %s        %s/%s/%s        PAX:%d(0)%d(0)%d(0)       %s",
		prpFlight->Regn, prpFlight->Act3,
		prpFlight->VERF, prpFlight->VERC, prpFlight->VERM,
		prpRotationFlight? prpRotationFlight->Fnum: "",
		prpRotationFlight? prpRotationFlight->Regn: "",
		prpFlight->Fnum, prpFlight->Des3, prpFlight->Via3,
		(const char *)prpFlight->Stod.Format("%H%M"),
		(const char *)prpFlight->Etod.Format("%H%M"),
		(const char *)prpFlight->Ofbl.Format("%H%M"),
		prpFlight->Bokf, prpFlight->Bokc, prpFlight->Bokm, prpFlight->Regn);

	return buf;
}

CString RegnDiagramViewer::TurnAroundFlightStatus(FLIGHTDATA *prpFlight,
	FLIGHTDATA *prpRotationFlight)
{
static	char buf[512];

	CString Stoa = 	prpFlight ? prpFlight->Stoa.Format("%H%M"): CString("");
	CString Etoa = 	prpFlight ? prpFlight->Etoa.Format("%H%M"): CString("");
	CString Onbl = 	prpFlight ? prpFlight->Onbl.Format("%H%M"): CString("");
	
	CString Stod = 	prpRotationFlight ? prpRotationFlight->Stod.Format("%H%M"): CString("");
	CString Etod = 	prpRotationFlight ? prpRotationFlight->Etod.Format("%H%M"): CString("");
	CString Ofbl = 	prpRotationFlight ? prpRotationFlight->Ofbl.Format("%H%M"): CString("");
	
	sprintf(buf, "%s / %s         F%dC%dY%d         %s            %s      %s/%s/%s      PAX:%d    %s     --> "
		"%s %s %s %s/%s/%s PAX:%d(0)%d(0)%d(0) %s",
		prpFlight->Regn, prpFlight->Act3,
		prpFlight->VERF, prpFlight->VERC, prpFlight->VERM,
		prpFlight->Fnum, prpFlight->Org3,
		(LPCSTR(Stoa)),(LPCSTR(Etoa)),(LPCSTR(Onbl)),
		0, prpFlight->Regn,
		prpRotationFlight? prpRotationFlight->Fnum: "",
		prpRotationFlight? prpRotationFlight->Org3: "",
		prpRotationFlight? prpRotationFlight->Des3: "",
		(LPCSTR(Stod)),(LPCSTR(Etod)),(LPCSTR(Ofbl)),
		prpRotationFlight? prpRotationFlight->Bokf: 0,
		prpRotationFlight? prpRotationFlight->Bokc: 0,
		prpRotationFlight? prpRotationFlight->Bokm: 0,
		prpRotationFlight? prpRotationFlight->Regn: "");

	if (prpFlight->IsTmo)
	{
		CString olTmo = CString("   TMO --> ") + prpFlight->Tmoa.Format("%H%M");
		strcat(buf,olTmo);
	}

	return buf;
}

BOOL RegnDiagramViewer::FindGroup(const char *pcpRegnAreaId, int &ripGroupno)
{
	for (ripGroupno = 0; ripGroupno < GetGroupCount(); ripGroupno++)
		if (GetGroup(ripGroupno)->RegnAreaId == pcpRegnAreaId)
			return TRUE;

	return FALSE;
}

BOOL RegnDiagramViewer::FindGroupAndLine(const char *pcpRegnId, int &ripGroupno, int &ripLineno)
{
	for (ripGroupno = 0; ripGroupno < GetGroupCount(); ripGroupno++)
		for (ripLineno = 0; ripLineno < GetLineCount(ripGroupno); ripLineno++)
               if ( GetLineText( ripGroupno, ripLineno) == pcpRegnId)
                    return TRUE;

	return FALSE;   // there is no such flight
}

BOOL RegnDiagramViewer::FindManager(long lpUrno, int &ripGroupno, int &ripManagerno)
{
	for (ripGroupno = 0; ripGroupno < GetGroupCount(); ripGroupno++)
		for (ripManagerno = 0; ripManagerno < GetManagerCount(ripGroupno); ripManagerno++)
			if (GetManager(ripGroupno, ripManagerno)->JobUrno == lpUrno)
				return TRUE;

	return FALSE;
}

BOOL RegnDiagramViewer::FindFlightBar(long lpUrno, int &ripGroupno, int &ripLineno, int &ripBkBarno)
{
	for (ripGroupno = 0; ripGroupno < GetGroupCount(); ripGroupno++)
		for (ripLineno = 0; ripLineno < GetLineCount(ripGroupno); ripLineno++)
			for (ripBkBarno = 0; ripBkBarno < GetBkBarCount(ripGroupno, ripLineno); ripBkBarno++)
				if (GetBkBar(ripGroupno, ripLineno, ripBkBarno)->FlightUrno == lpUrno)
					return TRUE;

    return FALSE;   // there is no such flight
}

/////////////////////////////////////////////////////////////////////////////
// RegnDiagramViewer -- Viewer basic operations

int RegnDiagramViewer::GetGroupCount()
{
    return omGroups.GetSize();
}

REGN_GROUPDATA *RegnDiagramViewer::GetGroup(int ipGroupno)
{
    return &omGroups[ipGroupno];
}

CString RegnDiagramViewer::GetGroupText(int ipGroupno)
{
    return omGroups[ipGroupno].Text;
}

CString RegnDiagramViewer::GetGroupTopScaleText(int ipGroupno)
{
	CString s;
	for (int i = 0; i < GetManagerCount(ipGroupno); i++)
	{
		REGN_MANAGERDATA *prlManager = GetManager(ipGroupno, i);
		s += s.IsEmpty()? "": " / ";
		s += CString(prlManager->EmpLnam) + " " + prlManager->ShiftTmid + " " + prlManager->ShiftStid;
	}
	return s;
}

int RegnDiagramViewer::GetGroupColorIndex(int ipGroupno,CTime opStart,CTime opEnd)
{
	int ilColorIndex = FIRSTCONFLICTCOLOR+(CFI_NOCONFLICT*2);
	CBrush *prlBrush = ogBrushs[ilColorIndex];
	int ilLineCount = GetLineCount(ipGroupno);
	for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
	{
		for (int ilBarno = 0; ilBarno < GetBarCount(ipGroupno, ilLineno); ilBarno++)
		{
			REGN_BARDATA *prlBar;
			if ((prlBar = GetBar(ipGroupno, ilLineno, ilBarno)) != NULL)
			{
				if (IsOverlapped(opStart, opEnd, prlBar->StartTime, prlBar->EndTime))
				{
					FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prlBar->FlightUrno);
					if (prlFlight != NULL)
					{
						if ((prlFlight->ConflictType !=  CFI_NOCONFLICT) && (!prlFlight->ConflictConfirmed))
						{
							return FIRSTCONFLICTCOLOR+(CFI_PST_NOTCOVERED*2);
						}
					}
				}
			}
		}
	}
	return ilColorIndex;
}

int RegnDiagramViewer::GetManagerCount(int ipGroupno)
{
    return omGroups[ipGroupno].Managers.GetSize();
}

REGN_MANAGERDATA *RegnDiagramViewer::GetManager(int ipGroupno, int ipManagerno)
{
    return &omGroups[ipGroupno].Managers[ipManagerno];
}

int RegnDiagramViewer::GetLineCount(int ipGroupno)
{
    return omGroups[ipGroupno].Lines.GetSize();
}

REGN_LINEDATA *RegnDiagramViewer::GetLine(int ipGroupno, int ipLineno)
{
    return &omGroups[ipGroupno].Lines[ipLineno];
}

CString RegnDiagramViewer::GetLineText(int ipGroupno, int ipLineno)
{
    return omGroups[ipGroupno].Lines[ipLineno].Text;
}

int RegnDiagramViewer::GetMaxOverlapLevel(int ipGroupno, int ipLineno)
{
    return omGroups[ipGroupno].Lines[ipLineno].MaxOverlapLevel;
}

int RegnDiagramViewer::GetVisualMaxOverlapLevel(int ipGroupno, int ipLineno)
{
	// For max level 0,1,2 we will use 2 levels, 3,4,5 we will use 5 levels, 6,7,8 we use 8 levels, and so on ...
	return GetMaxOverlapLevel(ipGroupno, ipLineno) / 3 * 3 + 2;
}

int RegnDiagramViewer::GetBkBarCount(int ipGroupno, int ipLineno)
{
    return omGroups[ipGroupno].Lines[ipLineno].BkBars.GetSize();
}

REGN_BKBARDATA *RegnDiagramViewer::GetBkBar(int ipGroupno, int ipLineno, int ipBkBarno)
{
    return &omGroups[ipGroupno].Lines[ipLineno].BkBars[ipBkBarno];
}

CString RegnDiagramViewer::GetBkBarTextArr(int ipGroupno, int ipLineno, int ipBkBarno)
{
    return omGroups[ipGroupno].Lines[ipLineno].BkBars[ipBkBarno].TextArr;
}

CString RegnDiagramViewer::GetBkBarTextDep(int ipGroupno, int ipLineno, int ipBkBarno)
{
    return omGroups[ipGroupno].Lines[ipLineno].BkBars[ipBkBarno].TextDep;
}

int RegnDiagramViewer::GetBarCount(int ipGroupno, int ipLineno)
{
    return omGroups[ipGroupno].Lines[ipLineno].Bars.GetSize();
}

REGN_BARDATA *RegnDiagramViewer::GetBar(int ipGroupno, int ipLineno, int ipBarno)
{
    return &omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno];
}

CString RegnDiagramViewer::GetBarText(int ipGroupno, int ipLineno, int ipBarno)
{
    return omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Text;
}

int RegnDiagramViewer::GetIndicatorCount(int ipGroupno, int ipLineno, int ipBarno)
{
    return omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators.GetSize();
}

REGN_INDICATORDATA *RegnDiagramViewer::GetIndicator(int ipGroupno, int ipLineno, int ipBarno, int ipIndicatorno)
{
    return &omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators[ipIndicatorno];
}

CString RegnDiagramViewer::GetTmo(int ipGroupno, int ipLineno)
{
	CString olTmoText = CString("");
    for (int ilBarno = 0; ilBarno < GetBarCount(ipGroupno, ipLineno); ilBarno++)
	{
		REGN_BARDATA *prlBar;
        if ((prlBar = GetBar(ipGroupno, ipLineno, ilBarno)) != NULL)
		{
			if (prlBar->IsTmo)
			{
				if (olTmoText.IsEmpty())
					olTmoText = CString(" TMO ");
				FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prlBar->FlightUrno);
				if (prlFlight != NULL)
				{
					if (ogFlightData.IsDeparture(prlFlight))
					{
						prlFlight = ogFlightData.GetRotationFlight(prlFlight);
					}
					if (prlFlight != NULL)
					{
						olTmoText += CString(prlFlight->Fnum) + " ";
					}
				}
			}
		}
	}
    return olTmoText;   
}

/////////////////////////////////////////////////////////////////////////////
// RegnDiagramViewer - REGN_BARDATA array maintenance (overlapping version)

void RegnDiagramViewer::DeleteAll()
{
    while (GetGroupCount() > 0)
        DeleteGroup(0);
}

int RegnDiagramViewer::CreateGroup(REGN_GROUPDATA *prpGroup)
{
    int ilGroupCount = omGroups.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
		if (CompareGroup(prpGroup, GetGroup(ilGroupno)) <= 0)
            break;  // should be inserted before Groups[ilGroupno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilGroupno = ilGroupCount; ilGroupno > 0; ilGroupno--)
		if (CompareGroup(prpGroup, GetGroup(ilGroupno-1)) >= 0)
            break;  // should be inserted after Groups[ilGroupno-1]
#endif
    
    omGroups.NewAt(ilGroupno, *prpGroup);
    return ilGroupno;
}

void RegnDiagramViewer::DeleteGroup(int ipGroupno)
{
    while (GetLineCount(ipGroupno) > 0)
        DeleteLine(ipGroupno, 0);
    while (GetManagerCount(ipGroupno) > 0)
        DeleteManager(ipGroupno, 0);

    omGroups.DeleteAt(ipGroupno);
}

int RegnDiagramViewer::CreateManager(int ipGroupno, REGN_MANAGERDATA *prpManager)
{
    int ilManagerCount = omGroups[ipGroupno].Managers.GetSize();
// first we check if this manager already exist
	for ( int ilLc = 0; ilLc < ilManagerCount; ilLc++)
	{
		if (omGroups[ipGroupno].Managers[ilLc].JobUrno == prpManager->JobUrno)
			return ilLc;
	}

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilManagerno = 0; ilManagerno < ilManagerCount; ilManagerno++)
		if (CompareManager(prpManager, GetManager(ipGroupno, ilManagerno)) <= 0)
            break;  // should be inserted before Lines[ilManagerno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilManagerno = ilManagerCount; ilManagerno > 0; ilManagerno--)
		if (CompareManager(prpManager, GetManager(ipGroupno, ilManagerno-1)) >= 0)
            break;  // should be inserted after Lines[ilManagerno-1]
#endif

    omGroups[ipGroupno].Managers.NewAt(ilManagerno, *prpManager);
    REGN_MANAGERDATA *prlManager = &omGroups[ipGroupno].Managers[ilManagerno];
    return ilManagerno;
}

void RegnDiagramViewer::DeleteManager(int ipGroupno, int ipManagerno)
{
    omGroups[ipGroupno].Managers.DeleteAt(ipManagerno);
}

int RegnDiagramViewer::CreateLine(int ipGroupno, REGN_LINEDATA *prpLine)
{
    int ilLineCount = omGroups[ipGroupno].Lines.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
		if (CompareLine(prpLine, GetLine(ipGroupno, ilLineno)) <= 0)
            break;  // should be inserted before Lines[ilLineno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
		if (CompareLine(prpLine, GetLine(ipGroupno, ilLineno-1)) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]
#endif

    omGroups[ipGroupno].Lines.NewAt(ilLineno, *prpLine);
    REGN_LINEDATA *prlLine = &omGroups[ipGroupno].Lines[ilLineno];
    prlLine->MaxOverlapLevel = 0;   // there is no bar in this line right now
    return ilLineno;
}

void RegnDiagramViewer::DeleteLine(int ipGroupno, int ipLineno)
{
    while (GetBkBarCount(ipGroupno, ipLineno) > 0)
        DeleteBkBar(ipGroupno, ipLineno, 0);
	// Id 14-Sep-1996
	// Don't simply call DeleteBar() for sake of efficiency
	REGN_LINEDATA *prlLine = &omGroups[ipGroupno].Lines[ipLineno];
	while (prlLine->Bars.GetSize() > 0)
	{
		DeleteBarFromBarMap(prlLine->Bars[0].DemandUrno);
		prlLine->Bars[0].Indicators.DeleteAll();	// delete associated indicators first
		prlLine->Bars.DeleteAt(0);
	}

    omGroups[ipGroupno].Lines.DeleteAt(ipLineno);
}

int RegnDiagramViewer::CreateBkBar(int ipGroupno, int ipLineno, REGN_BKBARDATA *prpBkBar)
{
    int ilBkBarCount = omGroups[ipGroupno].Lines[ipLineno].BkBars.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
// (2.89 seconds vs. 4.21 seconds) for 3000 bars creation.
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilBkBarno = 0; ilBkBarno < ilBarCount; ilBkBarno++)
        if (prpBar->StartTime <= GetBkBar(ipGroupno,ipLineno,ilBkBarno)->StartTime)
            break;  // should be inserted before Bars[ilBkBarno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilBkBarno = ilBkBarCount; ilBkBarno > 0; ilBkBarno--)
        if (prpBkBar->StartTime >= GetBkBar(ipGroupno,ipLineno,ilBkBarno-1)->StartTime)
            break;  // should be inserted after Bars[ilBkBarno-1]
#endif

    omGroups[ipGroupno].Lines[ipLineno].BkBars.NewAt(ilBkBarno, *prpBkBar);
    return ilBkBarno;
}

void RegnDiagramViewer::DeleteBkBar(int ipGroupno, int ipLineno, int ipBkBarno)
{
    omGroups[ipGroupno].Lines[ipLineno].BkBars.DeleteAt(ipBkBarno);
}

int RegnDiagramViewer::CreateBar(int ipGroupno, int ipLineno, REGN_BARDATA *prpBar, BOOL bpTopBar)
{
    REGN_LINEDATA *prlLine = GetLine(ipGroupno, ipLineno);
    int ilBarCount = prlLine->Bars.GetSize();

	// Id 14-Sep-1996
	// I use a new algorithm for bar insertion and new deletion.
	// This new algorithm will work somewhat slower than the previous version, but safer.
	// To give you a brief idea of this algorithm, here is the essential of insertion:
	//
	// Case A: Insert a new bar to be the topmost bar
	//		We will scan every bars, and for each bar which is in the same overlap group
	//		with the new bar, we will shift it downward one level, and update the field
	//		Line.MaxOverlapLevel. This new bar will be the last bar in painting order
	//		(for being topmost), and has overlapping level = 0.
	//
	// Case B: Insert a new bar to be the bottommost bar
	//		We will scan every bars for finding the maximum overlap level of bars which
	//		are in the same overlap group with this new bar (use level -1 if not found).
	//		Then use this maximum overlap level + 1 as overlap level of this new bar.
	//		This new bar will be the first bar in painting order (for being bottommost).
	//
	CUIntArray olBarnoList;
	GetOverlappedBarsFromTime(ipGroupno, ipLineno,
		prpBar->StartTime, prpBar->EndTime, olBarnoList);
	int ilListCount = olBarnoList.GetSize();

	if (bpTopBar)
	{
		for (int ilLc = 0; ilLc < ilListCount; ilLc++)
		{
			int ilBarno = olBarnoList[ilLc];
			REGN_BARDATA *prlBar = &prlLine->Bars[ilBarno];
			int ilNewOverlapLevel = ++prlBar->OverlapLevel;
			prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, ilNewOverlapLevel);
		}
		prlLine->Bars.NewAt(ilBarCount, *prpBar);	// to be the last bar in painting order
		prlLine->Bars[ilBarCount].OverlapLevel = 0;
		return ilBarCount;
	}
	else
	{
		int ilNewBarLevel = -1;
		for (int ilLc = 0; ilLc < ilListCount; ilLc++)
		{
			int ilBarno = olBarnoList[ilLc];
			REGN_BARDATA *prlBar = &prlLine->Bars[ilBarno];
			ilNewBarLevel = max(ilNewBarLevel, prlBar->OverlapLevel + 1);
				// the +1 means that we have to insert the new bar below this bar
		}
		prlLine->Bars.NewAt(0, *prpBar);	// to be the first bar in painting order
		prlLine->Bars[0].OverlapLevel = ilNewBarLevel;
		prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, ilNewBarLevel);
		return 0;
	}
}

void RegnDiagramViewer::DeleteBar(int ipGroupno, int ipLineno, int ipBarno)
{
	// Delete all indicators of this bar
	while (GetIndicatorCount(ipGroupno, ipLineno, ipBarno) > 0)
		DeleteIndicator(ipGroupno, ipLineno, ipBarno, 0);

    REGN_LINEDATA *prlLine = GetLine(ipGroupno, ipLineno);
	REGN_BARDATA *prlBar = &prlLine->Bars[ipBarno];


	// Id 14-Sep-1996
	// I use a new algorithm for bar insertion and new deletion.
	// This new algorithm will work somewhat slower than the previous version, but safer.
	// To give you a brief idea of this algorithm, here is the essential of deletion:
	//
	// We will scan every bars, and for each bar which is in the same overlap group with
	// the new bar, we will back up them to a temporary array. Then we remove every bars
	// in this overlapping group, and insert these bar from the temporary array except
	// the one which has to be deleted back to the viewer.
	//
	// In some situation, if this overlapping group controls the maximum overlap level of
	// the line, we may have to rescan every bars in this line to make sure that the
	// maximum overlap level still be correct.
	//
	CUIntArray olBarnoList;
	GetOverlappedBarsFromTime(ipGroupno, ipLineno,
		prlBar->StartTime, prlBar->EndTime, olBarnoList);
	int ilListCount = olBarnoList.GetSize();

	// Save the bar in this overlapping group to a temporary array
	CCSPtrArray<REGN_BARDATA> olSavedBars;
	BOOL blMustRecomputeMaxOverlapLevel = FALSE;
	for (int ilLc = 0; ilLc < ilListCount; ilLc++)
	{
		int ilBarno = olBarnoList[ilLc];
		REGN_BARDATA *prlBar = &prlLine->Bars[ilBarno];
		blMustRecomputeMaxOverlapLevel |= (prlBar->OverlapLevel == prlLine->MaxOverlapLevel);
			// check if deletion of this bar may change the line height
		if (ilBarno != ipBarno)	// must we save this bar?
			olSavedBars.NewAt(olSavedBars.GetSize(), prlLine->Bars[ilBarno]);
	}
	// Delete all bars (which are already backed up in a temporary array) from the viewer
	while (ilListCount-- > 0)
	{
		int ilBarno = olBarnoList[ilListCount];
//		DeleteBarFromBarMap(prlLine->Bars[ilBarno].DemandUrno);
		prlLine->Bars.DeleteAt(ilBarno);
	}
	// Then, we insert those bars back to the Gantt chart, except the specified one
	int ilSavedBarCount = olSavedBars.GetSize();
	for (int ilSavedBarno = 0; ilSavedBarno < ilSavedBarCount; ilSavedBarno++)
	{
		CreateBar(ipGroupno, ipLineno, &olSavedBars[ilSavedBarno]);
	}
	olSavedBars.DeleteAll();
	// Then recompute the MaxOverlapLevel if it's necessary
	if (blMustRecomputeMaxOverlapLevel)
	{
		prlLine->MaxOverlapLevel = 0;
	    int ilBarCount = prlLine->Bars.GetSize();
		for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
		{
			REGN_BARDATA *prlBar = &prlLine->Bars[ilBarno];
			prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, prlBar->OverlapLevel);
		}
	}
}

void RegnDiagramViewer::DeleteBarsForRegn(CString opRegn)
{
	for (int ilGroupno = 0; ilGroupno < GetGroupCount(); ilGroupno++)
		for (int ilLineno = 0; ilLineno < GetLineCount(ilGroupno); ilLineno++)
		{
			REGN_LINEDATA* prlLine = GetLine(ilGroupno, ilLineno);
			if (prlLine->Text == opRegn)
			{
				for (int ilBarno = 0; ilBarno < prlLine->Bars.GetSize(); ilBarno++)
				{
					prlLine->Bars[ilBarno].Indicators.DeleteAll();
					DeleteBarFromBarMap(prlLine->Bars[ilBarno].DemandUrno);
				}
				prlLine->Bars.DeleteAll();
			}
		}
}



int RegnDiagramViewer::CreateIndicator(int ipGroupno, int ipLineno, int ipBarno, REGN_INDICATORDATA *prpIndicator)
{
    int ilIndicatorCount = omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators.GetSize();

    // Search for the position which we want to insert this new indicator
    for (int ilIndicatorno = 0; ilIndicatorno < ilIndicatorCount; ilIndicatorno++)
	{
		REGN_INDICATORDATA *prlTmpIndicator = GetIndicator(ipGroupno,ipLineno,ipBarno,ilIndicatorno);
		if (prlTmpIndicator == NULL)
			break; // should not happen

		// Trying to place the indicator in the correct position.
		// I don't like this block of code, but it works fine.
		// Somebody should help me revise it.
		// -- Id, 29-Sep-96
		//
		if (prpIndicator->DemandUrno != 0)	// new indicator has a demand
		{
			if (prlTmpIndicator->DemandUrno == 0)
				break;	// should be inseted before a job without demand

			// the job in the buffer has some demand, so we use the old algorithm
			if (prpIndicator->IsArrival)	// new indicator is for arrival demand
			{
				if (!prlTmpIndicator->IsArrival)
					break;	// should be inserted before indicator for departure demand
				if (prpIndicator->StartTime < prlTmpIndicator->StartTime)
					break;	// should place indicator in time order
				if (prpIndicator->StartTime == prlTmpIndicator->StartTime)
				{
					if (prpIndicator->DemandUrno < prlTmpIndicator->DemandUrno)
						break;	// if the time is equal, we will use the Urno instead
				}
			}
			else	// new indicator is for departure demand
			{
				if (prlTmpIndicator->IsArrival)
					continue;	// bar in buffer is for arrival demand, step to the next one
				if (prpIndicator->StartTime < prlTmpIndicator->StartTime)
					break;	// should place indicator in time order
				if (prpIndicator->StartTime == prlTmpIndicator->StartTime)
				{
					if (prpIndicator->DemandUrno < prlTmpIndicator->DemandUrno)
						break;	// should place indicator in demand URNO order
				}
			}
		}
		else	// new indicator has no demand
		{
			if (prlTmpIndicator->DemandUrno != 0)
				continue;	// bar in buffer has demand, step to the next one
			if (prpIndicator->StartTime < prlTmpIndicator->StartTime)
				break;	// should place indicator in time order
				if (prpIndicator->StartTime == prlTmpIndicator->StartTime)
				{
					if (prpIndicator->JobUrno < prlTmpIndicator->JobUrno)
						break;	// if the time is equal, we will use the Urno instead
				}
		}
	}

    omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators.
		NewAt(ilIndicatorno, *prpIndicator);
    return ilIndicatorno;
}

void RegnDiagramViewer::DeleteIndicator(int ipGroupno, int ipLineno, int ipBarno, int ipIndicatorno)
{
    omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators.DeleteAt(ipIndicatorno);
}


/////////////////////////////////////////////////////////////////////////////
// RegnDiagramViewer - REGN_BARDATA calculation and searching routines

// Return the bar number of the list box based on the given period [time1, time2]
int RegnDiagramViewer::GetBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2,
    int ipOverlapLevel1, int ipOverlapLevel2)
{
    for (int i = GetBarCount(ipGroupno, ipLineno)-1; i >= 0; i--)
    {
        REGN_BARDATA *prlBar = GetBar(ipGroupno, ipLineno, i);
        if (opTime1 <= prlBar->EndTime && prlBar->StartTime <= opTime2 &&
            ipOverlapLevel1 <= prlBar->OverlapLevel && prlBar->OverlapLevel <= ipOverlapLevel2)
            return i;
    }
    return -1;
}

// Return the background bar number of the list box based on the given period [time1, time2]
int RegnDiagramViewer::GetBkBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2)
{
    for (int i = GetBkBarCount(ipGroupno, ipLineno)-1; i >= 0; i--)
    {
        REGN_BKBARDATA *prlBkBar = GetBkBar(ipGroupno, ipLineno, i);
        if (opTime1 <= prlBkBar->EndTime && prlBkBar->StartTime <= opTime2)
            return i;
    }
    return -1;
}

/////////////////////////////////////////////////////////////////////////////
// RegnDiagramViewer private helper methods

void RegnDiagramViewer::GetOverlappedBarsFromTime(int ipGroupno, int ipLineno,
	CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList)
{
    REGN_LINEDATA *prlLine = GetLine(ipGroupno, ipLineno);
    int ilBarCount = prlLine->Bars.GetSize();

	// Prepare the empty array of the involvement flag for each bar
	CCSPtrArray<BOOL> IsInvolvedBar;
	for (int i = 0; i < ilBarCount; i++)
		IsInvolvedBar.NewAt(i, FALSE);

	// We will loop again and again until there is no more bar in this overlapping found.
	// The overlapping group is at first determine by the given "opTime1" and "opTime2".
	// But if the bar we find is exceed this period of time, we will extend searching
	// to both left side and right side in the next round.
	//
	int nBarsFoundThisRound = -1;
	while (nBarsFoundThisRound != 0)
	{
		nBarsFoundThisRound = 0;
		for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
		{
			CTime olStartTime = prlLine->Bars[ilBarno].StartTime;
			CTime olEndTime = prlLine->Bars[ilBarno].EndTime;
			BOOL blIsOverlapped = IsReallyOverlapped(olStartTime, olEndTime, opTime1, opTime2);
			BOOL blIsWithIn = IsWithIn(olStartTime, olEndTime, opTime1, opTime2);
				// this IsWithIn() fix the real thin bar bug
			BOOL blIsInvolved = (blIsOverlapped || blIsWithIn);

			if (!IsInvolvedBar[ilBarno] && blIsInvolved)	// some more new involved bars?
			{
				IsInvolvedBar[ilBarno] = TRUE;
				nBarsFoundThisRound++;
				opTime1 = min(opTime1, prlLine->Bars[ilBarno].StartTime);
				opTime2 = max(opTime2, prlLine->Bars[ilBarno].EndTime);
			}
		}
	}

	// Create the list of involved bars, then store them to "ropBarnoList"
	ropBarnoList.RemoveAll();
	for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
	{
		if (IsInvolvedBar[ilBarno])
		{
			ropBarnoList.Add(ilBarno);
		}
	}
	IsInvolvedBar.DeleteAll();
}

void RegnDiagramViewer::AllowUpdates(BOOL bpNoUpdatesNow)
{
	bmNoUpdatesNow = bpNoUpdatesNow;
}

/////////////////////////////////////////////////////////////////////////////
// RegnDiagramViewer -- Methods which handle changes (from Data Distributor)

void RegnDiagramViewer::RegnDiagramCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
	RegnDiagramViewer *polViewer = (RegnDiagramViewer *)popInstance;

	if (polViewer->bmNoUpdatesNow == FALSE)
	{

		if (ipDDXType == FLIGHT_CHANGE)
			polViewer->ProcessFlightChange((FLIGHTDATA *)vpDataPointer);
		else if (ipDDXType == FLIGHT_SELECT)
			polViewer->ProcessFlightSelect((FLIGHTDATA *)vpDataPointer);
		else if (ipDDXType == JOB_NEW)
		{
			polViewer->ProcessFmgJobNew((JOBDATA *)vpDataPointer);
			polViewer->ProcessSpecialJobNew((JOBDATA *) vpDataPointer);
		}
		else if (ipDDXType == JOB_CHANGE)
		{
			// we have no ProcessFmgJobChange() for this moment, so I just use delete and add.
			polViewer->ProcessFmgJobDelete((JOBDATA *)vpDataPointer);
			polViewer->ProcessFmgJobNew((JOBDATA *)vpDataPointer);
			polViewer->ProcessSpecialJobChange((JOBDATA *) vpDataPointer);
		}
		else if (ipDDXType == JOB_DELETE)
		{
			polViewer->ProcessFmgJobDelete((JOBDATA *)vpDataPointer);
			polViewer->ProcessSpecialJobDelete((JOBDATA *) vpDataPointer);
		}
		else if (ipDDXType == DEMAND_CHANGE || ipDDXType == DEMAND_NEW)
		{
			polViewer->ProcessDemandChange((DEMANDDATA *)vpDataPointer);
		}
		else if (ipDDXType == DEMAND_DELETE)
		{
			polViewer->ProcessDemandDelete((DEMANDDATA *)vpDataPointer);
		}
		else if (ipDDXType == REDISPLAY_ALL || ipDDXType == UPDATE_CONFLICT_SETUP)
		{
			polViewer->ChangeViewTo(polViewer->SelectView(),polViewer->bmIncludeArrivalFlights,polViewer->bmIncludeDepartureFlights,polViewer->omStartTime,polViewer->omEndTime);
			polViewer->pomAttachWnd->Invalidate();
		}
	}
	else
	{
		// should never reach this point
	//	ASSERT( 0 );
	}
}

void RegnDiagramViewer::ProcessFlightChange(FLIGHTDATA *prpFlight)
{
	if(prpFlight == NULL)
	{
		return;
	}

	int ilGroupno, ilLineno, ilBkBarno;
	FLIGHTDATA *prlRotationFlight;

	if(ogFlightData.IsThirdPartyFlight(prpFlight))
	{
		ProcessThirdPartyFlightChange(prpFlight);
		if((prlRotationFlight = ogFlightData.GetRotationFlight(prpFlight)) != NULL)
		{
			ProcessThirdPartyFlightChange(prlRotationFlight);
		}
		return;
	}

    while (FindFlightBar(prpFlight->Urno, ilGroupno, ilLineno, ilBkBarno))
	{
		REGN_BKBARDATA *prlBar;
		
		if ((prlBar = GetBkBar(ilGroupno, ilLineno, ilBkBarno)) != NULL)
		{
			TRACE("Delete bar URNO <%d> REGN <%s> ADID <%s>\n",prpFlight->Urno,prpFlight->Regn,prpFlight->Adid);
			DeleteBkBar(ilGroupno, ilLineno, ilBkBarno);
			if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
			{
				LONG lParam = MAKELONG(ilLineno, ilGroupno);
				pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
			}
		}
	}


	// Change Job or Demand
	ogConflicts.CheckConflicts(*prpFlight,FALSE,FALSE,TRUE);
	DeleteBarsForRegn(prpFlight->Regn);
	for (ilGroupno = 0; ilGroupno < GetGroupCount(); ilGroupno++)
		for (ilLineno = 0; ilLineno < GetLineCount(ilGroupno); ilLineno++)
			if (GetLine(ilGroupno,ilLineno)->Text == CString(prpFlight->Regn))
				MakeBarsByLine(ilGroupno, ilLineno);


	prpFlight->IsDisplayed = FALSE;
	prpFlight->IsShadoBarDisplayed = FALSE;

	// Update the Viewer's data
	for(ilGroupno = 0; ilGroupno < GetGroupCount(); ilGroupno++)
	{
		for(ilLineno = 0; ilLineno < GetLineCount(ilGroupno); ilLineno++)
		{
			if(GetLineText(ilGroupno, ilLineno) == prpFlight->Regn)
			{
				ogConflicts.CheckConflicts(*prpFlight,FALSE,FALSE,TRUE);
				if(MakeBkBar(ilGroupno, ilLineno, prpFlight))
				{
					TRACE("Made new bar URNO <%d> REGN <%s> ADID <%s>\n",prpFlight->Urno,prpFlight->Regn,prpFlight->Adid);
					if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
					{
						LONG lParam = MAKELONG(ilLineno, ilGroupno);
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
					}
				}
			}
		}
	}
/* // the following was replaced with the above because if the regn occurs in
   // more than one group the second occurance isn't recreated
	if (FindGroupAndLine(prpFlight->Regn,ilGroupno,ilLineno))
	{
		ogConflicts.CheckConflicts(*prpFlight,FALSE,FALSE,TRUE);
		if(MakeBkBar(ilGroupno, ilLineno, prpFlight))
		{
			TRACE("Made new bar URNO <%d> REGN <%s> ADID <%s>\n",prpFlight->Urno,prpFlight->Regn,prpFlight->Adid);
			if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
			{
				LONG lParam = MAKELONG(ilLineno, ilGroupno);
				pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
			}
		}
	}
*/
	if((prlRotationFlight = ogFlightData.GetRotationFlight(prpFlight)) != NULL)
	{
		while (FindFlightBar(prlRotationFlight->Urno, ilGroupno, ilLineno, ilBkBarno))
		{
			TRACE("Delete bar Rotation URNO <%d> REGN <%s> ADID <%s>\n",prlRotationFlight->Urno,prlRotationFlight->Regn,prlRotationFlight->Adid);
			DeleteBkBar(ilGroupno, ilLineno, ilBkBarno);
			if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
			{
				LONG lParam = MAKELONG(ilLineno, ilGroupno);
				pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
			}
		}
		prlRotationFlight->IsDisplayed = FALSE;
		prlRotationFlight->IsShadoBarDisplayed = FALSE;

		ogConflicts.CheckConflicts(*prlRotationFlight,FALSE,FALSE,TRUE);

		for(ilGroupno = 0; ilGroupno < GetGroupCount(); ilGroupno++)
		{
			for(ilLineno = 0; ilLineno < GetLineCount(ilGroupno); ilLineno++)
			{
				if(GetLineText(ilGroupno, ilLineno) == prlRotationFlight->Regn)
				{
					if(MakeBkBar(ilGroupno, ilLineno, prlRotationFlight))
					{
						TRACE("Made new bar rotation URNO <%d> REGN <%s> ADID <%s>\n",prlRotationFlight->Urno,prlRotationFlight->Regn,prlRotationFlight->Adid);
						if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
						{
							LONG lParam = MAKELONG(ilLineno, ilGroupno);
							pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
						}
					}
				}
			}
		}
/* // the following was replaced with the above because if the regn occurs in
   // more than one group the second occurance isn't recreated
		if (FindGroupAndLine(prlRotationFlight->Regn,ilGroupno,ilLineno))
		{
			if(MakeBkBar(ilGroupno, ilLineno, prlRotationFlight))
			{
				TRACE("Made new bar rotation URNO <%d> REGN <%s> ADID <%s>\n",prlRotationFlight->Urno,prlRotationFlight->Regn,prlRotationFlight->Adid);
				if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
				{
					LONG lParam = MAKELONG(ilLineno, ilGroupno);
					pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
				}
			}
		}
*/		
	}
}





void RegnDiagramViewer::ProcessFlightSelect(FLIGHTDATA *prpFlight)
{
	if (!prpFlight)
		return;

	REGN_SELECTION	rolSelection;
	if (!FindFlightBar(prpFlight->Urno,rolSelection.imGroupno,rolSelection.imLineno,rolSelection.imBarno))
		return;

	if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
	{

		LONG lParam = reinterpret_cast<LONG>(&rolSelection);
		pomAttachWnd->SendMessage(WM_SELECTDIAGRAM,UD_SELECTBAR, lParam);
	}
}

void RegnDiagramViewer::ProcessDemandChange(DEMANDDATA *prpDemand)
{
	if(prpDemand != NULL)
	{
		UpdateBarsForDemand(prpDemand->Urno);
	}
}

void RegnDiagramViewer::ProcessDemandDelete(DEMANDDATA *prpDemand)
{
	if(prpDemand != NULL)
	{
		UpdateBarsForDemand(prpDemand->Urno);
	}
}

void RegnDiagramViewer::UpdateBarsForDemand(long lpDemandUrno)
{
	int ilGroupno, ilLineno, ilBarno;
	int ilNumGroups = GetGroupCount();
	for(ilGroupno = 0; ilGroupno < ilNumGroups; ilGroupno++)
	{
		int ilNumLines = GetLineCount(ilGroupno);
		for(ilLineno = 0; ilLineno < ilNumLines; ilLineno++)
		{
			int ilNumBars = GetBarCount(ilGroupno, ilLineno);
			bool blFound = false;
			for(ilBarno = 0 ; ilBarno < ilNumBars; ilBarno++)
			{
				if(BarContainsDemand(omGroups[ilGroupno].Lines[ilLineno].Bars[ilBarno].DemandUrno, lpDemandUrno))
				{
					blFound = true;
					break;
				}
			}
			if(blFound)
			{
				for(ilBarno = (ilNumBars-1); ilBarno >= 0 ; ilBarno--)
				{
					DeleteBar(ilGroupno, ilLineno, ilBarno);
				}
				MakeBarsByLine(ilGroupno, ilLineno);
				if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
				{
					LONG lParam = MAKELONG(ilLineno, ilGroupno);
					pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
				}
			}
		}
	}
}



void RegnDiagramViewer::ProcessSpecialJobChange(JOBDATA *prpJob)
{
	int ilGroupno, ilLineno, ilBarno;
	if (strcmp(prpJob->Jtco,JOBSTAIRCASE) != 0)
		return; // not interested in other jobtypes
	if (!FindFlightBar(prpJob->Urno, ilGroupno, ilLineno, ilBarno))
		return; // can't do anything

	// Update the Viewer's data
	int ilOldMaxOverlapLevel = GetVisualMaxOverlapLevel(ilGroupno, ilLineno);
	DeleteBar(ilGroupno, ilLineno, ilBarno);
	MakeSpecialBar(ilGroupno, ilLineno, prpJob);

	// Notifies the attached window (if exist)
	if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
	{
		LONG lParam = MAKELONG(ilLineno, ilGroupno);
		if (GetVisualMaxOverlapLevel(ilGroupno, ilLineno) == ilOldMaxOverlapLevel)
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
		else
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
	}
}

void RegnDiagramViewer::ProcessSpecialJobDelete(JOBDATA *prpJob)
{
	int ilGroupno, ilLineno, ilBarno;
	if (strcmp(prpJob->Jtco,JOBSTAIRCASE) != 0)
	{
		return; // not interested in other jobtypes
	}
	if (!FindFlightBar(prpJob->Urno, ilGroupno, ilLineno, ilBarno))
		return; // can't do anything

	// Update the Viewer's data
	int ilOldMaxOverlapLevel = GetVisualMaxOverlapLevel(ilGroupno, ilLineno);
	DeleteBar(ilGroupno, ilLineno, ilBarno);

	// Notifies the attached window (if exist)
	if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
	{
		LONG lParam = MAKELONG(ilLineno, ilGroupno);
		if (GetVisualMaxOverlapLevel(ilGroupno, ilLineno) == ilOldMaxOverlapLevel)
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
		else
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
	}
}

void RegnDiagramViewer::ProcessSpecialJobNew(JOBDATA *prpJob)
{
	int ilGroupno, ilLineno, ilBarno;
	if (strcmp(prpJob->Jtco,JOBSTAIRCASE) != 0)
	{
		return; // not interested in other jobtypes
	}
	if (FindFlightBar(prpJob->Urno, ilGroupno, ilLineno, ilBarno))
		ProcessSpecialJobChange(prpJob); // job already there

	// Update the Viewer's data
	//int ilOldMaxOverlapLevel = GetVisualMaxOverlapLevel(ilGroupno, ilLineno);
	if (! FindGroupAndLine(prpJob->Alid,ilGroupno,ilLineno))
		return; // no line, can't do anything jobtypes
	int ilOldMaxOverlapLevel = GetVisualMaxOverlapLevel(ilGroupno, ilLineno);
	MakeSpecialBar(ilGroupno, ilLineno, prpJob);

	// Notifies the attached window (if exist)
	if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
	{
		LONG lParam = MAKELONG(ilLineno, ilGroupno);
		if (GetVisualMaxOverlapLevel(ilGroupno, ilLineno) == ilOldMaxOverlapLevel)
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
		else
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
	}
}

void RegnDiagramViewer::ProcessFmgJobNew(JOBDATA *prpJob)
{
// Id 19-Sep-96
	// Don't interest it if it's not a flight manager Regn job
	if (CString(prpJob->Jtco) != JOBFMREGNAREA)
		return;

	int ilGroupno;
	if (!FindGroup(prpJob->Alid, ilGroupno))
        return; // no such group in the viewer buffer right now, just do nothing
// end of Id 19-Sep-96

	// Update the Viewer's data
	MakeManager(ilGroupno, prpJob);

	// Notifies the attached window (if exist)
	if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
	{
		LONG lParam = MAKELONG(-1, ilGroupno);
		pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATEGROUP, lParam);
	}

}

void RegnDiagramViewer::ProcessFmgJobDelete(JOBDATA *prpJob)
{
// Id 19-Sep-96
	// Don't interest it if it's not a flight manager Regn job
	if (CString(prpJob->Jtco) != JOBFMREGNAREA)
		return;
// end of Id 19-Sep-96

	int ilGroupno, ilManagerno;
	if (!FindManager(prpJob->Urno, ilGroupno, ilManagerno))
        return; // no such group in the viewer buffer right now, just do nothing

	// Update the Viewer's data
	DeleteManager(ilGroupno, ilManagerno);

	// Notifies the attached window (if exist)
	if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
	{
		LONG lParam = MAKELONG(-1, ilGroupno);
		pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATEGROUP, lParam);
	}

}


CString RegnDiagramViewer::GetApuInOp(int ipGroupno, int ipLineno)
{
	CString olApuInOpText = CString("");
	REGN_LINEDATA *prlLine = GetLine(ipGroupno, ipLineno);
	if(prlLine->ApuInOp)
	{
		olApuInOpText = " - APU-INOP "; // Questo lo dovresti mettere nella string table
	}

	return olApuInOpText;
}

///////////////////////////////////////////////////////////////////////////////////////////
// Printing routines

void RegnDiagramViewer::PrintRegnDiagramHeader(int ipGroupno)
{
	CString olRegnAreaName = GetGroupText(ipGroupno);
	CString omTarget;
//	omTarget.Format("%s  von: %s  bis: %s",olRegnAreaName,omStartTime.Format("%d.%m.%Y %H:%M"),omEndTime.Format("%d.%m.%Y  %H:%M"));
	omTarget.Format(GetString(IDS_STRING61285),olRegnAreaName,omStartTime.Format("%d.%m.%Y %H:%M"),omEndTime.Format("%d.%m.%Y  %H:%M"));
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");

	pomPrint->omCdc.StartPage();
//	pomPrint->PrintHeader(CString("Regndiagramm"),olPrintDate,omTarget);
	pomPrint->PrintHeader(GetString(IDS_STRING32810),olPrintDate,omTarget);
	pomPrint->PrintTimeScale(400,2800,omStartTime,omEndTime,olRegnAreaName);
}

void RegnDiagramViewer::PrintPrepareLineData(int ipGroupNo,int ipLineno,CCSPtrArray<PRINTBARDATA> &ropPrintLine)
{
	REGN_LINEDATA *prlLine = &omGroups[ipGroupNo].Lines[ipLineno];
	ropPrintLine.DeleteAll();
	int ilBarCount = prlLine->Bars.GetSize();
	for( int i = 0; i < ilBarCount; i++)
	{
		if (IsOverlapped(prlLine->Bars[i].StartTime,prlLine->Bars[i].EndTime,
			omStartTime,omEndTime))
		{
			PRINTBARDATA rlPrintBar;
			rlPrintBar.Text = prlLine->Bars[i].Text;
			rlPrintBar.StartTime = prlLine->Bars[i].StartTime;
			rlPrintBar.EndTime = prlLine->Bars[i].EndTime;
			rlPrintBar.FrameType = prlLine->Bars[i].FrameType;
			rlPrintBar.MarkerType = prlLine->Bars[i].MarkerType;
			rlPrintBar.IsShadowBar = prlLine->Bars[i].IsShadowBar;
			rlPrintBar.OverlapLevel = 0;
			rlPrintBar.IsBackGroundBar = FALSE;
			ropPrintLine.NewAt(ropPrintLine.GetSize(),rlPrintBar);
		}
	}
}


void RegnDiagramViewer::PrintManagers(int ipGroupno,int ipYOffset1,int ipYOffset2)
{
	CString olAllManagers;
	CCSPtrArray<JOBDATA> olFmgJobs;
	ogJobData.GetFmgJobsByAlid(olFmgJobs, GetGroup(ipGroupno)->RegnAreaId);
	int ilFmgCount = olFmgJobs.GetSize();
	for (int ilLc = 0; ilLc < ilFmgCount; ilLc++)
	{
		if (IsOverlapped(olFmgJobs[ilLc].Acfr,olFmgJobs[ilLc].Acto,omStartTime,omEndTime ))
		{
			if (olAllManagers.IsEmpty() == FALSE)
			{
				olAllManagers += "   /   ";
			}
			SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(olFmgJobs[ilLc].Shur);
			olAllManagers += CString((prlShift != NULL ? prlShift->Sfca : "")) + ", ";
			olAllManagers += CString((prlShift != NULL ? prlShift->Egrp : "")) + ", ";
			EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(olFmgJobs[ilLc].Peno);
			olAllManagers += CString(prlEmp != NULL ? prlEmp->Lanm : "");
		} 
	} 
	if (olAllManagers.IsEmpty() == FALSE)
	{
		int ilYOffset = 0;
		if (igFirstGroupOnPage == FALSE)
		{
			pomPrint->PrintText(420,0,800,40,PRINT_SMALLBOLD,"Flightmanager:",TRUE);
			pomPrint->PrintText(420,40,0,130,PRINT_SMALL,olAllManagers,TRUE);
			pomPrint->PrintText(420,0,0,50,PRINT_SMALL,"");
		}
		else
		{
			pomPrint->PrintText(420,ipYOffset1,800,ipYOffset1+40,PRINT_SMALLBOLD,"Flightmanager:");
			pomPrint->PrintText(420,ipYOffset2,0,ipYOffset2+90,PRINT_SMALL,olAllManagers);
		}
		igFirstGroupOnPage = FALSE;
		/********************
		pomPrint->PrintText(420,ipYOffset1+ilYOffset,800,ipYOffset1+40+ilYOffset,PRINT_SMALLBOLD,"Flightmanager:");
		pomPrint->PrintText(420,ipYOffset2+ilYOffset,2800,ipYOffset2+90+ilYOffset,PRINT_SMALL,olAllManagers);
		******************/
	}
}

void RegnDiagramViewer::PrintRegnArea(CPtrArray &opPtrArray,int ipGroupNo)
{

	if( (pomPrint->imLineNo+ogBasicData.imFreeBottomLines) > pomPrint->imMaxLines )
	{
		if ( pomPrint->imPageNo > 0)
		{
			pomPrint->PrintGanttBottom();
			pomPrint->PrintFooter("","");
			pomPrint->omCdc.EndPage();
			igFirstGroupOnPage = TRUE;
		}
		PrintManagers(ipGroupNo,380,420);
		PrintRegnDiagramHeader(ipGroupNo);
	}
	else
	{
		PrintManagers(ipGroupNo,0,0);
		CString olRegnAreaName = GetGroupText(ipGroupNo);
		pomPrint->PrintGanttHeader(400,2800,olRegnAreaName);
	}

	CCSPtrArray<PRINTBARDATA> ropPrintLine;
	int ilLineCount = GetLineCount(ipGroupNo);
    for( int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
	{
		if( (pomPrint->imLineNo+ogBasicData.imFreeBottomLines) > pomPrint->imMaxLines )
		{
			if ( pomPrint->imPageNo > 0)
			{
				pomPrint->PrintGanttBottom();
				pomPrint->PrintFooter("","");
				pomPrint->omCdc.EndPage();
				igFirstGroupOnPage = TRUE;
			}
			PrintRegnDiagramHeader(ipGroupNo);
		}

		PrintPrepareLineData(ipGroupNo,ilLineno,ropPrintLine);
		if (pomPrint->PrintGanttLine(GetLineText(ipGroupNo, ilLineno),ropPrintLine) != TRUE)
		{
			// page full
			pomPrint->PrintGanttBottom();
			pomPrint->PrintFooter("","");
			pomPrint->omCdc.EndPage();
			igFirstGroupOnPage = TRUE;
			pomPrint->imLineNo = 0;
			PrintRegnDiagramHeader(ipGroupNo);
			// try it again, but don't continue with this line if it don't fit on page again!
			pomPrint->PrintGanttLine(GetLineText(ipGroupNo, ilLineno),ropPrintLine);
		}

	}
	pomPrint->PrintGanttBottom();
	//pomPrint->imLineNo = 999;
}

void RegnDiagramViewer::PrintOneFm(SHIFTDATA *prpShift,EMPDATA *prpEmp,JOBDATA *prpJob,BOOL bpIsFirstLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	int ilTopFrame = bpIsFirstLine == TRUE ? PRINT_FRAMETHIN : PRINT_NOFRAME;

	pomPrint->imLineHeight = 82;

	rlElement.Alignment  = PRINT_LEFT;
	rlElement.Length     = 650;
	rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
	rlElement.FrameRight = PRINT_FRAMETHIN;
	rlElement.FrameTop   = bpIsFirstLine;
	rlElement.FrameBottom= PRINT_FRAMETHIN;
	rlElement.pFont       = &pomPrint->omMediumFont_Regular;
	rlElement.Text       = ogEmpData.GetEmpName(prpEmp);
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Alignment  = PRINT_CENTER;
	rlElement.Length     = 120;
	rlElement.FrameLeft  = PRINT_FRAMETHIN;
	rlElement.FrameRight = PRINT_FRAMETHIN;
	rlElement.Text       = prpShift != NULL ? CString(prpShift->Sfca) : "";
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 300;
	rlElement.Text       = prpShift != NULL ? 
								prpJob->Acfr.Format("%H:%M") + "-" +
								prpJob->Acto.Format("%H:%M") : "";
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	CCSPtrArray <JOBDATA> olJobs;
	ogJobData.GetJobsByPkno(olJobs,prpJob->Peno);
	olJobs.Sort(CompareJobStartTime);

	CString olFlightText;

	for (int ilLc = olJobs.GetSize()-1; ilLc >= 0; ilLc--)
	{
		JOBDATA *prlJob = &olJobs[ilLc];
	    // If it's not a special flightmanager job, ignore it
		if (strcmp(prlJob->Jtco,JOBFM) == 0)
		{
			FLIGHTDATA *prlFlight = ogBasicData.GetFlightForJob(prlJob);
			if (prlFlight != NULL)
			{
				if (olFlightText.IsEmpty() == FALSE)
				{
					olFlightText += CString(", ");
				}
				olFlightText += prlFlight->Fnum;
			}
		}

	}
	olJobs.RemoveAll();

	rlElement.Length     = 1550;
	rlElement.Alignment  = PRINT_LEFT;
	rlElement.FrameRight = PRINT_FRAMEMEDIUM;
	rlElement.Text       = olFlightText;
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
}

void RegnDiagramViewer::PrintFmHeader(int ipGroupNo)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	CString olRegnAreaName = GetGroupText(ipGroupNo);
	pomPrint->imLineHeight = 130;

	rlElement.Alignment  = PRINT_CENTER;
	rlElement.Length     = 2620;
	rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
	rlElement.FrameRight = PRINT_FRAMEMEDIUM;
	rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
	rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->omLargeFont_Bold;
	rlElement.Text       = olRegnAreaName;
	
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
}


void RegnDiagramViewer::PrintFmForRegnArea(CPtrArray &opPtrArray,int ipGroupNo)
{
	BOOL blIsFirstLine = FALSE;

	if(pomPrint->imLineTop > pomPrint->imHeight-400)
	{
		if ( pomPrint->imPageNo > 0)
		{
			pomPrint->PrintFooter("","");
			pomPrint->omCdc.EndPage();
			pomPrint->imLineTop = pomPrint->imFirstLine;
			pomPrint->omCdc.StartPage();
			pomPrint->PrintHeader();
		}
	}

	PrintFmHeader(ipGroupNo);
	
	CCSPtrArray<JOBDATA> olFmgJobs;
	ogJobData.GetFmgJobsByAlid(olFmgJobs, GetGroup(ipGroupNo)->RegnAreaId);
	olFmgJobs.Sort(CompareJobStartTime);
	int ilFmgCount = olFmgJobs.GetSize();
	//for (int ilLc = 0; ilLc < ilFmgCount; ilLc++)
	for(int ilLc = ilFmgCount-1; ilLc >= 0; ilLc--)
	{
		if (IsOverlapped(olFmgJobs[ilLc].Acfr,olFmgJobs[ilLc].Acto,omStartTime,omEndTime ))
		{
			SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(olFmgJobs[ilLc].Shur);
			EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(olFmgJobs[ilLc].Peno);
			if(pomPrint->imLineTop > pomPrint->imHeight-200)
			{
				if ( pomPrint->imPageNo > 0)
				{
					pomPrint->PrintFooter("","");
					pomPrint->omCdc.EndPage();
					pomPrint->imLineTop = pomPrint->imFirstLine;
					pomPrint->omCdc.StartPage();
					pomPrint->PrintHeader();
					blIsFirstLine = TRUE;
				}
			}
			PrintOneFm(prlShift,prlEmp,&olFmgJobs[ilLc],blIsFirstLine);
			blIsFirstLine = FALSE;
		} 
	} 
	
}


void RegnDiagramViewer::PrintDiagram(CPtrArray &opPtrArray)
{
	igFirstGroupOnPage = TRUE;
	CString omTarget = CString("Anzeigebegin: ") + CString(omStartTime.Format("%d.%m.%Y"))
		+  "     Ansicht: " + ogCfgData.rmUserSetup.RECV;
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");
	pomPrint = new CCSPrint(pomAttachWnd,PRINT_PORTRAIT,80,500,200,
		CString("Regndiagramm"),olPrintDate,omTarget);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(PRINT_PORTRAIT,ogBasicData.pcmPrintForm,DMPAPER_A3)  == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = "Regndiagramm";	
			pomPrint->omCdc.StartDoc( &rlDocInfo );

			int ilGroupCount = GetGroupCount();
			for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
			{
				PrintRegnArea(opPtrArray,ilGroupno);
			}
			pomPrint->PrintFooter("","");
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
}

void RegnDiagramViewer::PrintFm(CPtrArray &opPtrArray)
{
	igFirstGroupOnPage = TRUE;
	CString omTarget = CString("Ansicht: ") + ogCfgData.rmUserSetup.RECV;
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");
	pomPrint = new CCSPrint(pomAttachWnd,PRINT_LANDSCAPE,80,500,200,
		CString("Flightmanager"),olPrintDate,omTarget);
	if (pomPrint != NULL)
	{

		if (pomPrint->InitializePrinter(PRINT_LANDSCAPE,ogBasicData.pcmPrintForm,DMPAPER_A3)  == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = "Flightmanager";	
			pomPrint->imLineHeight = 82;
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->omCdc.StartPage();
			pomPrint->PrintHeader();

			int ilGroupCount = GetGroupCount();
			for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
			{
				PrintFmForRegnArea(opPtrArray,ilGroupno);
			}
			pomPrint->PrintFooter("","");
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
}


void RegnDiagramViewer::AddToBarMap(long lpBarUrno, long lpDemandUrno)
{
	CMapPtrToPtr *polSingleMap;
	if(omBarMap.Lookup((void *) lpBarUrno, (void *&) polSingleMap))
	{
		polSingleMap->SetAt((void *)lpDemandUrno,NULL);
	}
	else
	{
		polSingleMap = new CMapPtrToPtr;
		polSingleMap->SetAt((void *)lpDemandUrno,NULL);
		omBarMap.SetAt((void *)lpBarUrno,polSingleMap);
	}
}

bool RegnDiagramViewer::GetDemandsForBar(long lpBarUrno, CCSPtrArray <DEMANDDATA> &ropDemands)
{
	bool blFound = false;
	CMapPtrToPtr *polSingleMap;
	if(omBarMap.Lookup((void *) lpBarUrno, (void *&) polSingleMap))
	{
		for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			long llDemandUrno;
			void *pvlDummy;
			polSingleMap->GetNextAssoc(rlPos, (void *&) llDemandUrno, (void *&)pvlDummy);
			DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(llDemandUrno);
			if(prlDemand != NULL)
			{
				ropDemands.Add(prlDemand);
				blFound = true;
			}
		}
	}

	return blFound;
}

bool RegnDiagramViewer::BarContainsDemand(long lpBarUrno, long lpDemandUrno)
{
	bool blBarContainsDemand = false;
	CMapPtrToPtr *polSingleMap;
	if(omBarMap.Lookup((void *)lpBarUrno,(void *& )polSingleMap))
	{
		void *pvlDummy;
		if(polSingleMap->Lookup((void *)lpDemandUrno,(void *& )pvlDummy))
		{
			blBarContainsDemand = true;
		}
	}

	return blBarContainsDemand;
}

void RegnDiagramViewer::DeleteDemandFromBarMap(long lpBarUrno, long lpDemandUrno)
{
	CMapPtrToPtr *polSingleMap;
	if(omBarMap.Lookup((void *)lpBarUrno,(void *& )polSingleMap))
	{
		polSingleMap->RemoveKey((void *)lpDemandUrno);
	}
}

void RegnDiagramViewer::DeleteBarFromBarMap(long lpBarUrno)
{
	CMapPtrToPtr *polSingleMap;
	if(omBarMap.Lookup((void *)lpBarUrno,(void *& )polSingleMap))
	{
		polSingleMap->RemoveAll();
		omBarMap.RemoveKey((void *)lpBarUrno);
	}
}
