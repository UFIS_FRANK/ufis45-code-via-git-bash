// CiCDemandTablePropertySheet.h : header file
//
#ifndef _CICDEMANDTBPS_H_
#define _CICDEMANDTBPS_H_

/////////////////////////////////////////////////////////////////////////////
// DemandTablePropertySheet

class CicDemandTablePropertySheet : public BasePropertySheet
{
	DECLARE_DYNAMIC(CicDemandTablePropertySheet)

// Construction
public:
	CicDemandTablePropertySheet(CStringArray& popGroupNames,CWnd* pParentWnd = NULL,CViewer *popViewer = NULL, UINT iSelectPage = 0);
	~CicDemandTablePropertySheet();

// Attributes
public:
	DemandFilterPage		m_pageColumns;
	DemandFilterPage		m_pageDety;
	FilterPage				m_pageFunctions;
	FilterPage				m_pageAirline;
	FilterPage				m_pageClasses;
	FilterPage				m_pageGroups;
	CMapStringToPtr			m_DetyPageMap;
	CicDemandTableSortPage	m_pageSort;

// Operations
public:
	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
	virtual int	 QueryForDiscardChanges();

	// Generated message map functions
protected:
	//{{AFX_MSG(CicDemandTablePropertySheet)
	afx_msg LONG OnUpdateAllPages(UINT wParam, LONG lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif // _CICDEMANDTBPS_H_
/////////////////////////////////////////////////////////////////////////////

