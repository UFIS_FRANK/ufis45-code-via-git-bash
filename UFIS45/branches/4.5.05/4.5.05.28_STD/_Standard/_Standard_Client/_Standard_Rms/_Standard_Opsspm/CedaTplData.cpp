// CedaTplData.cpp - Class for gates
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaTplData.h>
#include <BasicData.h>

CedaTplData::CedaTplData()
{                  
    BEGIN_CEDARECINFO(TPLDATA, TplDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Dalo,"DALO")
		FIELD_CHAR_TRIM(Evor,"EVOR")
		FIELD_CHAR_TRIM(Fisu,"FISU")
		FIELD_CHAR_TRIM(Relx,"RELX")
		FIELD_CHAR_TRIM(Tnam,"TNAM")
		FIELD_CHAR_TRIM(Tpst,"TPST")
		FIELD_CHAR_TRIM(Appl,"APPL")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(TplDataRecInfo)/sizeof(TplDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&TplDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"TPLTAB");
    pcmFieldList = "URNO,DALO,EVOR,FISU,RELX,TNAM,TPST,APPL";
}
 
CedaTplData::~CedaTplData()
{
	TRACE("CedaTplData::~CedaTplData called\n");
	ClearAll();
}

void CedaTplData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omNameMap.RemoveAll();

	CMapPtrToPtr *prlTmp;
	CString olDalo;
	for(POSITION rlPos = omDaloMap.GetStartPosition(); rlPos != NULL; )
	{
		omDaloMap.GetNextAssoc(rlPos,olDalo,(void *& )prlTmp);
		delete prlTmp;
	}
	omDaloMap.RemoveAll();
	omData.DeleteAll();
}

CCSReturnCode CedaTplData::ReadTplData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];
	if (bgIsPrm)
	{
		strcpy(pclWhere,"WHERE TPST = '1' AND ( APPL = 'RULE_AFT' OR APPL = 'RULE_FIR') AND TNAM = 'PRM'");
	}
	else
	{
		strcpy(pclWhere,"WHERE TPST = '1' AND ( APPL = 'RULE_AFT' OR APPL = 'RULE_FIR')");
	}
    if((ilRc = CedaAction(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaTplData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		TPLDATA rlTplData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord(ilLc,&rlTplData)) == RCSuccess)
			{
				AddTplInternal(rlTplData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaTplData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(TPLDATA), pclWhere);
    return ilRc;
}


void CedaTplData::AddTplInternal(TPLDATA &rrpTpl)
{
	TPLDATA *prlTpl = new TPLDATA;
	*prlTpl = rrpTpl;
	omData.Add(prlTpl);
	omUrnoMap.SetAt((void *)prlTpl->Urno,prlTpl);
	omNameMap.SetAt(prlTpl->Tnam,prlTpl);

	CMapPtrToPtr *polDaloTplMap = NULL;

	if(omDaloMap.Lookup(prlTpl->Dalo, (void *&) polDaloTplMap))
	{
		polDaloTplMap->SetAt((void *)prlTpl->Urno,prlTpl);
	}
	else
	{
		polDaloTplMap = new CMapPtrToPtr;
		polDaloTplMap->SetAt((void *)prlTpl->Urno,prlTpl);
		omDaloMap.SetAt(prlTpl->Dalo,polDaloTplMap);
	}
}

TPLDATA* CedaTplData::GetTplByUrno(long lpUrno)
{
	TPLDATA *prlTpl = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlTpl);
	return prlTpl;
}

TPLDATA* CedaTplData::GetTplByName(const char *pcpName)
{
	TPLDATA *prlTpl = NULL;
	omNameMap.Lookup(pcpName, (void *&) prlTpl);
	return prlTpl;
}

bool CedaTplData::GetTplByDalo(CCSPtrArray<TPLDATA> &ropTpl, const char *pcpDalo)
{
	TPLDATA *prlTpl = NULL;
	ropTpl.RemoveAll();
	CMapPtrToPtr *polDaloTplMap;

	if(omDaloMap.Lookup(pcpDalo,(void *& )polDaloTplMap) == TRUE)
	{
		for(POSITION rlPos =  polDaloTplMap->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			polDaloTplMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlTpl);
			ropTpl.Add(prlTpl);
		}
	}

	return true;
}

// Dalo -> eg "A/C REG"
// templates are doubled because there is a record for flight dependent (APPL='RULE_AFT') and
// the same record for flight independent (APPL='RULE_FIR') so select distinct
void CedaTplData::GetTplNameByDalo(const char* pcpDalo, CStringArray &ropTnam)
{
	TPLDATA *prlTpl = NULL;
	ropTnam.RemoveAll();

	CMapPtrToPtr *polDaloTplMap;
	CMapStringToPtr olDistinctTplNames;
	void *prlDummy;

	if(omDaloMap.Lookup(pcpDalo,(void *& )polDaloTplMap) == TRUE)
	{
		for(POSITION rlPos = polDaloTplMap->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			polDaloTplMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlTpl);
			if(!olDistinctTplNames.Lookup(prlTpl->Tnam, (void *&) prlDummy))
			{
				olDistinctTplNames.SetAt(prlTpl->Tnam,NULL);
				ropTnam.Add(prlTpl->Tnam);
			}
		}
	}
}

long CedaTplData::GetTplUrnoByName(const char *pcpName)
{
	long llUrno = 0L;
	TPLDATA *prlTpl = GetTplByName(pcpName);
	if(prlTpl != NULL)
	{
		llUrno = prlTpl->Urno;
	}
	return llUrno;
}


CString CedaTplData::GetTableName(void)
{
	return CString(pcmTableName);
}
