// flplanps.cpp : implementation file
//

// These following include files are project dependent
#include <stdafx.h>
#include <OpssPm.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <AllocData.h>
#include <CedaSerData.h>
#include <CedaRudData.h>

// These following include files are necessary
#include <cviewer.h>
#include <BasicData.h>
#include <FilterPage.h>
#include <FlIndDemandTableSortPage.h>
#include <BasePropertySheet.h>
#include <FlIndDemandTablePropertySheet.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FlIndDemandTablePropertySheet
//

FlIndDemandTablePropertySheet::FlIndDemandTablePropertySheet(CWnd* pParentWnd,
	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
	(GetString(IDS_STRING61916), pParentWnd, popViewer, iSelectPage)
{
	AddPage(&m_pageServiceNames);
	AddPage(&m_pageServiceCodes);
	AddPage(&m_pageDety);
	AddPage(&m_pageSort);

	// Change the caption in tab control of the PropertySheet
	m_pageServiceNames.SetCaption(GetString(IDS_STRING61914));
	m_pageServiceCodes.SetCaption(GetString(IDS_STRING61915));
	m_pageDety.SetCaption(GetString(IDS_RETYTABCTRL));

	// Add all FID service names referenced by demands
	// don't just add all FID service names because some services may have been used
	// to create FIDs and then changed to flight dependent - PRF 5856
	RUDDATA *prlRud = NULL;
	SERDATA *prlSer = NULL;
	CString olFlightTypes = "012";
	CMapStringToPtr olServiceNames;
	void *pvlDummy = NULL;
	CString olServiceName;
	int ilNumRuds = ogRudData.omData.GetSize();
	for(int ilRud = 0; ilRud < ilNumRuds; ilRud++)
	{
		prlRud = &ogRudData.omData[ilRud];
		if(strlen(prlRud->Drty) <= 0 || olFlightTypes.Find((TCHAR)*prlRud->Drty) == -1)
		{
			if((prlSer = ogSerData.GetSerByUrno(prlRud->Ughs)) != NULL &&
				!olServiceNames.Lookup(prlSer->Snam, (void *&) pvlDummy))
			{
				olServiceNames.SetAt(prlSer->Snam, NULL);
				m_pageServiceNames.omPossibleItems.Add(prlSer->Snam);
				m_pageServiceCodes.omPossibleItems.Add(prlSer->Seco);
			}
		}
	}

	// load any FID services not referenced by demands
	int ilNumSers = ogSerData.omData.GetSize();
	for(int ilSer = 0; ilSer < ilNumSers; ilSer++)
	{
		prlSer = &ogSerData.omData[ilSer];
		if(prlSer->Ffis && !olServiceNames.Lookup(prlSer->Snam, (void *&) pvlDummy))
		{
			olServiceNames.SetAt(prlSer->Snam, NULL);
			m_pageServiceNames.omPossibleItems.Add(prlSer->Snam);
			m_pageServiceCodes.omPossibleItems.Add(prlSer->Seco);
		}
	}

	m_pageServiceNames.bmSelectAllEnabled = true;
	m_pageServiceCodes.bmSelectAllEnabled = true;

	m_pageDety.omPossibleItems.Add(GetString(IDS_PERSONNEL_DEM));
	m_pageDety.omPossibleItems.Add(GetString(IDS_EQUIPMENT_DEM));
	//m_pageDety.omPossibleItems.Add(GetString(IDS_LOCATION_DEM));
	m_pageDety.bmSelectAllEnabled = true;
}

void FlIndDemandTablePropertySheet::LoadDataFromViewer()
{
	pomViewer->GetFilter("Name", m_pageServiceNames.omSelectedItems);
	pomViewer->GetFilter("Code", m_pageServiceCodes.omSelectedItems);
	pomViewer->GetFilter("Dety", m_pageDety.omSelectedItems);
	pomViewer->GetSort(m_pageSort.omSortOrders);
	m_pageSort.bmDemStartAscending = pomViewer->GetUserData("DEMSTARTORDER") == "YES" ? true : false;
	m_pageSort.bmDemEndAscending = pomViewer->GetUserData("DEMENDORDER") == "YES" ? true : false;

	m_pageSort.bmDemTerminalAscending = pomViewer->GetUserData("DEMTERMINALORDER") == "YES" ? true : false;
}

void FlIndDemandTablePropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{
	pomViewer->SetFilter("Name", m_pageServiceNames.omSelectedItems);
	pomViewer->SetFilter("Code", m_pageServiceCodes.omSelectedItems);
	pomViewer->SetFilter("Dety", m_pageDety.omSelectedItems);
	pomViewer->SetSort(m_pageSort.omSortOrders);
	pomViewer->SetUserData("DEMSTARTORDER",m_pageSort.bmDemStartAscending ? "YES" : "NO");
	pomViewer->SetUserData("DEMENDORDER",m_pageSort.bmDemEndAscending ? "YES" : "NO");

	pomViewer->SetUserData("DEMTERMINALORDER",m_pageSort.bmDemTerminalAscending ? "YES" : "NO");

	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}
}

int FlIndDemandTablePropertySheet::QueryForDiscardChanges()
{
	CStringArray olSelectedNames;
	CStringArray olSelectedCodes;
	CStringArray olSelectedDetys;
	CStringArray olSortOrders;

	bool blDemStartAscending = pomViewer->GetUserData("DEMSTARTORDER") == "YES" ? true : false;
	bool blDemEndAscending = pomViewer->GetUserData("DEMENDORDER") == "YES" ? true : false;

	bool blDemTerminalAscending = pomViewer->GetUserData("DEMTERMINALORDER") == "YES" ? true : false;

	pomViewer->GetFilter("Name", olSelectedNames);
	pomViewer->GetFilter("Code", olSelectedCodes);
	pomViewer->GetFilter("Dety", olSelectedDetys);
	pomViewer->GetSort(olSortOrders);
	if (!IsIdentical(olSelectedNames, m_pageServiceNames.omSelectedItems) ||
		!IsIdentical(olSelectedCodes, m_pageServiceCodes.omSelectedItems) ||
		!IsIdentical(olSelectedDetys, m_pageDety.omSelectedItems) ||
		!IsIdentical(olSortOrders, m_pageSort.omSortOrders) ||
		m_pageSort.bmDemStartAscending != blDemStartAscending ||
		m_pageSort.bmDemEndAscending != blDemEndAscending ||
		m_pageSort.bmDemTerminalAscending != blDemTerminalAscending)
	{
		// Discard changes ?
		return MessageBox(GetString(IDS_STRING61583), NULL, MB_ICONQUESTION | MB_OKCANCEL);
	}

	return IDOK;
}

