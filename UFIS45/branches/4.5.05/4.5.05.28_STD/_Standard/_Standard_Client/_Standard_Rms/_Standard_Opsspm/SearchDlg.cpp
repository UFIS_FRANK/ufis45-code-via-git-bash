// SearchDlg.cpp : implementation file search dialog
// Brian Chandler March 2002

#include <stdafx.h>
#include <opsspm.h>
#include <SearchDlg.h>
#include <GUILng.h>
#include <CCSGlobl.h>
#include <BasicData.h>
#include <CedaCfgData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSearchDlg dialog


CSearchDlg::CSearchDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSearchDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSearchDlg)
	m_WhenTitle = _T("");
	m_WhatTitle = _T("");
	m_WhereTitle = _T("");
	//}}AFX_DATA_INIT
	omSearchFieldChoiceSeperator.Format("%c", SEARCH_FIELD_CHOICE_SEPERATOR);
	lmSelectedUserData = 0L;
	omSelectedWhen = TIMENULL;
	prmSelectedSearchType = NULL;

	olOldEditValues.Add("");
	olOldEditValues.Add("");
	olOldEditValues.Add("");
	olOldEditValues.Add("");
	olOldEditValues.Add("");
	olOldEditValues.Add("");
}

CSearchDlg::~CSearchDlg()
{
	int ilNumST = omSearchTypes.GetSize();
	for(int ilST = 0; ilST < ilNumST; ilST++)
	{
		SEARCHTYPE *prlSearchType = &omSearchTypes[ilST];
		int ilNumFields = prlSearchType->Fields.GetSize();
		for(int ilF = 0; ilF < ilNumFields; ilF++)
		{
			ogOldSearchValues.SetOldSearchValue(prlSearchType->What, prlSearchType->Fields[ilF], prlSearchType->InitialValues[ilF]);
		}

		prlSearchType->Data.DeleteAll();
	}

	omSearchTypes.DeleteAll();
}

void CSearchDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchDlg)
	DDX_Control(pDX, IDOK, m_OkButtonCtrl);
	DDX_Control(pDX, IDCANCEL, m_CancelButtonCtrl);
	DDX_Control(pDX, IDC_EDIT7, m_Edit7);
	DDX_Control(pDX, IDC_EDIT6, m_Edit6);
	DDX_Control(pDX, IDC_EDIT5, m_Edit5);
	DDX_Control(pDX, IDC_EDIT4, m_Edit4);
	DDX_Control(pDX, IDC_EDIT3, m_Edit3);
	DDX_Control(pDX, IDC_EDIT2, m_Edit2);
	DDX_Control(pDX, IDC_EDIT1, m_Edit1);
	DDX_Control(pDX, IDC_TITLE7, m_Title7);
	DDX_Control(pDX, IDC_TITLE6, m_Title6);
	DDX_Control(pDX, IDC_TITLE5, m_Title5);
	DDX_Control(pDX, IDC_TITLE4, m_Title4);
	DDX_Control(pDX, IDC_TITLE3, m_Title3);
	DDX_Control(pDX, IDC_TITLE2, m_Title2);
	DDX_Control(pDX, IDC_TITLE1, m_Title1);
	DDX_Control(pDX, IDC_WHERE_COMBO, m_WhereCombo);
	DDX_Control(pDX, IDC_WHEN_COMBO, m_WhenCombo);
	DDX_Control(pDX, IDC_WHAT_COMBO, m_WhatCombo);
	DDX_Control(pDX, IDC_RESULTSLIST, m_ResultsListCtrl);
	DDX_Text(pDX, IDC_WHEN_TITLE, m_WhenTitle);
	DDX_Text(pDX, IDC_WHAT_TITLE, m_WhatTitle);
	DDX_Text(pDX, IDC_WHERE_TITLE, m_WhereTitle);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSearchDlg, CDialog)
	//{{AFX_MSG_MAP(CSearchDlg)
	ON_WM_MOVE() //PRF 8712
	ON_CBN_SELCHANGE(IDC_WHAT_COMBO, OnSelchangeWhatCombo)
	ON_CBN_SELCHANGE(IDC_WHERE_COMBO, OnSelchangeWhereCombo)
	ON_CBN_SELCHANGE(IDC_WHEN_COMBO, OnSelchangeWhenCombo)
	ON_EN_CHANGE(IDC_EDIT1, OnChangeEdit1)
	ON_EN_CHANGE(IDC_EDIT2, OnChangeEdit2)
	ON_EN_CHANGE(IDC_EDIT3, OnChangeEdit3)
	ON_EN_CHANGE(IDC_EDIT4, OnChangeEdit4)
	ON_EN_CHANGE(IDC_EDIT5, OnChangeEdit5)
	ON_EN_CHANGE(IDC_EDIT6, OnChangeEdit6)
	ON_EN_CHANGE(IDC_EDIT7, OnChangeEdit7)
	ON_LBN_DBLCLK(IDC_RESULTSLIST, OnDblclkResultslist)
	ON_LBN_SELCHANGE(IDC_RESULTSLIST, OnSelchangeResultslist)
	ON_LBN_SELCANCEL(IDC_RESULTSLIST, OnSelcancelResultslist)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSearchDlg message handlers

BOOL CSearchDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_WhatTitle = GetString(IDS_SD_WHATTITLE);
	m_WhereTitle = GetString(IDS_SD_WHERETITLE);
	m_WhenTitle = GetString(IDS_SD_WHENTITLE);
	m_OkButtonCtrl.SetWindowText(GetString(IDS_SD_OKBUTTON));
	m_CancelButtonCtrl.SetWindowText(GetString(IDS_SD_CANCELBUTTON));
	SetWindowText(GetString(IDS_SD_TITLE));
	UpdateData(FALSE);

	DisplayWhatList();

	CString olOldWhat = ogOldSearchValues.GetOldSearchValue("GLOBAL", "WHAT");
	prmSelectedSearchType = GetSearchType(olOldWhat);
	if(prmSelectedSearchType == NULL && omSearchTypes.GetSize() > 0)
	{
		prmSelectedSearchType = &omSearchTypes[0];
	}

	ASSERT(prmSelectedSearchType != NULL);

	SelectWhat(prmSelectedSearchType->What);

	HandleWhatChange(prmSelectedSearchType);

	//Singapore, for setting the horizontal scrollbar in the list box
	char chConfExtent[10];
	GetPrivateProfileString(pcgAppName,"CONFIGURE_HORZEXTENT","NO",chConfExtent,sizeof(chConfExtent),ogBasicData.GetConfigFileName());
	if(strcmp(chConfExtent,"YES") == 0)
	{
		char chHorzExtent[10];
		GetPrivateProfileString(pcgAppName,"HORIZONTAL_EXTENT","",chHorzExtent,sizeof(chHorzExtent),ogBasicData.GetConfigFileName());
		if(strlen(chHorzExtent) > 0)
			m_ResultsListCtrl.SetHorizontalExtent(atoi(chHorzExtent));
	}

	CRect olRect;
	GetWindowRect(&olRect);
	BOOL blMinimized = FALSE;
	ogBasicData.GetDialogFromReg(olRect, COpssPmApp::SEARCH_DIALOG_WINDOWPOS_REG_KEY,blMinimized);

	//Multiple monitor setup
	ogBasicData.GetWindowPositionCorrect(olRect);

	MoveWindow(&olRect);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSearchDlg::HandleWhatChange(SEARCHTYPE *prpSearchType)
{
	DisplayWhereList(prpSearchType->WhereList);

	CString olOldWhere = ogOldSearchValues.GetOldSearchValue(prpSearchType->What, "WHERE");
	if(olOldWhere.IsEmpty() && prpSearchType->WhereList.GetSize() > 0)
	{
		olOldWhere = prpSearchType->WhereList[0];
	}

	SelectWhere(olOldWhere);

	DisplayWhenList();

	CString olOldWhen = ogOldSearchValues.GetOldSearchValue(prpSearchType->What, "WHEN");
	CTime olSelectedTime = olOldWhen.IsEmpty() ? TIMENULL : DBStringToDateTime(olOldWhen);
	SelectWhen(olSelectedTime);

	DisplaySearchFields(prpSearchType->Fields, prpSearchType->FieldLens, prpSearchType->InitialValues);

	UpdateResultsList();
}	


void CSearchDlg::DisplayWhatList()
{
	int ilNumSearchTypes = omSearchTypes.GetSize();
	for(int ilST = 0; ilST < ilNumSearchTypes; ilST++)
	{
		m_WhatCombo.AddString(omSearchTypes[ilST].What);
	}
}

int CSearchDlg::SelectWhat(const CString &ropWhat)
{
	int ilSelectedLine = m_WhatCombo.SelectString(-1, ropWhat);
	if(ilSelectedLine != CB_ERR)
	{
		m_WhatCombo.GetLBText(ilSelectedLine, omSelectedWhat);
	}
	return ilSelectedLine;
}

void CSearchDlg::DisplayWhereList(const CStringArray &ropWhereList)
{
	m_WhereCombo.ResetContent();
	int ilNumWheres = ropWhereList.GetSize();
	for(int ilW = 0; ilW < ilNumWheres; ilW++)
	{
		m_WhereCombo.AddString(ropWhereList[ilW]);
	}
}

int CSearchDlg::SelectWhere(const CString &ropWhere)
{
	int ilSelectedLine = m_WhereCombo.SelectString(-1, ropWhere);
	if(ilSelectedLine != CB_ERR)
	{
		m_WhereCombo.GetLBText(ilSelectedLine, omSelectedWhere);
	}
	return ilSelectedLine;
}

void CSearchDlg::DisplayWhenList()
{
	m_WhenCombo.ResetContent();
	CStringArray olTimeframeList;
	ogBasicData.GetTimeframeList(olTimeframeList);
	int ilNumDays = olTimeframeList.GetSize();
	for(int ilDay = 0; ilDay < ilNumDays; ilDay++)
	{
		m_WhenCombo.AddString(olTimeframeList[ilDay]);
	}
}

int CSearchDlg::SelectWhen(CTime opWhen)
{
	int ilSel = CB_ERR;
	int ilNumDays = m_WhenCombo.GetCount();
	if(ilNumDays > 0)
	{
		int ilDay;
		if(opWhen == TIMENULL || opWhen < ogBasicData.GetTimeframeStart() || opWhen > ogBasicData.GetTimeframeEnd() )
		{
			ilDay = ogBasicData.GetDisplayDateOffset();
		}
		else
		{
			CTimeSpan olDays = opWhen - ogBasicData.GetTimeframeStart();
			ilDay = olDays.GetDays();
		}

		if(ilDay >= 0 && ilDay < ilNumDays)
		{
			ilSel = m_WhenCombo.SetCurSel(ilDay);
		}
	}

	omSelectedWhen = ogBasicData.GetTimeframeStart() + CTimeSpan(m_WhenCombo.GetCurSel(),0,0,0);
	return ilSel;
}

void CSearchDlg::DisplaySearchFields(CStringArray &ropFields, CUIntArray &ropFieldLens, CStringArray &ropInitialValues)
{
	int ilNumFields = ropFields.GetSize();
	int ilNumFieldLens = ropFieldLens.GetSize();
	int ilCount = min(ilNumFields, ilNumFieldLens);
	int ilCharWidth = 15;

	CRect olRect, olRect2;
	m_Edit1.GetWindowRect(&olRect);
	ScreenToClient(olRect);

	m_Title1.GetWindowRect(&olRect2);
	ScreenToClient(olRect2);

	m_Edit1.ShowWindow(SW_HIDE);
	m_Title1.ShowWindow(SW_HIDE);
	m_Edit2.ShowWindow(SW_HIDE);
	m_Title2.ShowWindow(SW_HIDE);
	m_Edit3.ShowWindow(SW_HIDE);
	m_Title3.ShowWindow(SW_HIDE);
	m_Edit4.ShowWindow(SW_HIDE);
	m_Title4.ShowWindow(SW_HIDE);
	m_Edit5.ShowWindow(SW_HIDE);
	m_Title5.ShowWindow(SW_HIDE);
	m_Edit6.ShowWindow(SW_HIDE);
	m_Title6.ShowWindow(SW_HIDE);
	m_Edit7.ShowWindow(SW_HIDE);
	m_Title7.ShowWindow(SW_HIDE);

	for(int ilF = 0; ilF < ilCount; ilF++)
	{
		olRect.right = olRect.left + (ropFieldLens[ilF] * ilCharWidth);
		olRect2.right = olRect2.left + (ropFieldLens[ilF] * ilCharWidth);
		switch(ilF)
		{
			case 0:
				m_Edit1.MoveWindow(olRect);
				m_Edit1.ShowWindow(SW_SHOW);
				m_Edit1.SetWindowText(ropInitialValues[ilF]);
				m_Title1.MoveWindow(olRect2);
				m_Title1.ShowWindow(SW_SHOW);
				m_Title1.SetWindowText(ropFields[ilF]);
				break;
			case 1:
				m_Edit2.MoveWindow(olRect);
				m_Edit2.ShowWindow(SW_SHOW);
				m_Edit2.SetWindowText(ropInitialValues[ilF]);
				m_Title2.MoveWindow(olRect2);
				m_Title2.ShowWindow(SW_SHOW);
				m_Title2.SetWindowText(ropFields[ilF]);
				break;
			case 2:
				m_Edit3.MoveWindow(olRect);
				m_Edit3.ShowWindow(SW_SHOW);
				m_Edit3.SetWindowText(ropInitialValues[ilF]);
				m_Title3.MoveWindow(olRect2);
				m_Title3.ShowWindow(SW_SHOW);
				m_Title3.SetWindowText(ropFields[ilF]);
				break;
			case 3:
				m_Edit4.MoveWindow(olRect);
				m_Edit4.ShowWindow(SW_SHOW);
				m_Edit4.SetWindowText(ropInitialValues[ilF]);
				m_Title4.MoveWindow(olRect2);
				m_Title4.ShowWindow(SW_SHOW);
				m_Title4.SetWindowText(ropFields[ilF]);
				break;
			case 4:
				m_Edit5.MoveWindow(olRect);
				m_Edit5.ShowWindow(SW_SHOW);
				m_Edit5.SetWindowText(ropInitialValues[ilF]);
				m_Title5.MoveWindow(olRect2);
				m_Title5.ShowWindow(SW_SHOW);
				m_Title5.SetWindowText(ropFields[ilF]);
				break;
			case 5:
				m_Edit6.MoveWindow(olRect);
				m_Edit6.ShowWindow(SW_SHOW);
				m_Edit6.SetWindowText(ropInitialValues[ilF]);
				m_Title6.MoveWindow(olRect2);
				m_Title6.ShowWindow(SW_SHOW);
				m_Title6.SetWindowText(ropFields[ilF]);
				break;
			case 6:
				m_Edit7.MoveWindow(olRect);
				m_Edit7.ShowWindow(SW_SHOW);
				m_Edit7.SetWindowText(ropInitialValues[ilF]);
				m_Title7.MoveWindow(olRect2);
				m_Title7.ShowWindow(SW_SHOW);
				m_Title7.SetWindowText(ropFields[ilF]);
				break;
		}

		olRect.left = olRect.right + 5;
		olRect2.left = olRect2.right + 5;
	}

	if(ilCount > 0)
	{
		m_Edit1.SetFocus();
	}
}

void CSearchDlg::OnOK() 
{
	SaveOldValues();
	int ilSelectedLine = m_ResultsListCtrl.GetCurSel();
	if(ilSelectedLine == LB_ERR)
	{
		// nothing selected from list box
		MessageBox(GetString(IDS_SD_NOTHINGSELECTED), GetString(IDS_SD_TITLE), MB_ICONSTOP);
	}
	else
	{
		lmSelectedUserData = m_ResultsListCtrl.GetItemData(ilSelectedLine);
		CDialog::OnOK();
	}
}

void CSearchDlg::OnCancel() 
{
	ogBasicData.WriteDialogToReg(this,COpssPmApp::SEARCH_DIALOG_WINDOWPOS_REG_KEY,omWindowRect,this->IsIconic()); //PRF 8712
	SaveOldValues();
	CDialog::OnCancel();
}

SEARCHTYPE *CSearchDlg::GetSearchType(CString opWhat)
{
	SEARCHTYPE *prlSearchType = NULL;

	int ilNumSearchTypes = omSearchTypes.GetSize();
	for(int ilST = 0; ilST < ilNumSearchTypes; ilST++)
	{
		if(omSearchTypes[ilST].What == opWhat)
		{
			prlSearchType = &omSearchTypes[ilST];
			break;
		}
	}

	return prlSearchType;
}

SEARCHTYPE *CSearchDlg::AddSearchType(CString opWhat)
{
	SEARCHTYPE *prlSearchType = new SEARCHTYPE;
	if(prlSearchType != NULL)
	{
		prlSearchType->What = opWhat;
		omSearchTypes.Add(prlSearchType);
	}

	return prlSearchType;
}

void CSearchDlg::GetAllEditValues(CStringArray &ropEditValues)
{
	CString olText;

	m_Edit1.GetWindowText(olText);
	olText.MakeUpper();
	ropEditValues.Add(olText);
	m_Edit2.GetWindowText(olText);
	olText.MakeUpper();
	ropEditValues.Add(olText);
	m_Edit3.GetWindowText(olText);
	olText.MakeUpper();
	ropEditValues.Add(olText);
	m_Edit4.GetWindowText(olText);
	olText.MakeUpper();
	ropEditValues.Add(olText);
	m_Edit5.GetWindowText(olText);
	olText.MakeUpper();
	ropEditValues.Add(olText);
	m_Edit6.GetWindowText(olText);
	olText.MakeUpper();
	ropEditValues.Add(olText);
	m_Edit7.GetWindowText(olText);
	olText.MakeUpper();
	ropEditValues.Add(olText);
}

void CSearchDlg::SaveOldValues(void)
{
	if(prmSelectedSearchType != NULL)
	{
		CStringArray olEditValues;
		GetAllEditValues(olEditValues);

		// remember the data entered so that if these fields are re-selected we can display the old values entered
		int ilNumFields = prmSelectedSearchType->Fields.GetSize();
		for(int ilField = 0; ilField < ilNumFields; ilField++)
		{
			prmSelectedSearchType->InitialValues[ilField] = olEditValues[ilField];
		}
		ogOldSearchValues.SetOldSearchValue("GLOBAL", "WHAT", omSelectedWhat);
		ogOldSearchValues.SetOldSearchValue(omSelectedWhat, "WHERE", omSelectedWhere);
		ogOldSearchValues.SetOldSearchValue(omSelectedWhat, "WHEN", omSelectedWhen.Format("%Y%m%d000000"));
	}
}

void CSearchDlg::UpdateResultsList(void)
{
	m_ResultsListCtrl.ResetContent();

	CString olText;
	CStringArray olEditValues;
	GetAllEditValues(olEditValues);

	if(prmSelectedSearchType != NULL)
	{
		int ilNumFields = prmSelectedSearchType->Fields.GetSize(), ilField;

		CString olSearchString;
		CStringArray olSearchStringChoices;
		int ilSearchString, ilNumSearchStrings;
		bool blMatches, blValidChoiceFound;
		int ilDataCount = prmSelectedSearchType->Data.GetSize();
		for(int ilData = 0; ilData < ilDataCount; ilData++)
		{
			SEARCHDATA *prlSearchData = &prmSelectedSearchType->Data[ilData];

			// if the first date is not defined then any of the entries are OK eg positions/gate/equipment are date independent
			// otherwise at least one of the specified dates must match the date selected in the "When" comboBox
			if(prlSearchData->Date1 == TIMENULL  || 
				IsSameDay(omSelectedWhen,prlSearchData->Date1) || 
				 IsSameDay(omSelectedWhen,prlSearchData->Date2))
			{
				blMatches = true;
				for(ilField = 0; ilField < ilNumFields; ilField++)
				{
					olText = olEditValues[ilField];
					if(!olText.IsEmpty())
					{
						// the text should possibly be checked against a choice of results eg. airline code will be checked against ALC2 and ALC3
						ogBasicData.ExtractItemList(prlSearchData->SearchStrings[ilField], &olSearchStringChoices, SEARCH_FIELD_CHOICE_SEPERATOR);
						ilNumSearchStrings = olSearchStringChoices.GetSize();
						blValidChoiceFound = false;
						for(ilSearchString = 0; ilSearchString < ilNumSearchStrings; ilSearchString++)
						{
							if(!strncmp(olSearchStringChoices[ilSearchString],olText,olText.GetLength()))
							{
								blValidChoiceFound = true;
								break;
							}
						}
						if(!blValidChoiceFound)
						{
							blMatches = false;
							break;
						}
					}
				}
				if(blMatches)
				{
					int ilIndex = m_ResultsListCtrl.AddString(prlSearchData->DisplayString);
					if(ilIndex != LB_ERR)
					{
						m_ResultsListCtrl.SetItemData(ilIndex,prlSearchData->UserData);
					}
				}
			}
		}
	}
	if(m_ResultsListCtrl.GetCount() == 1)
	{
		m_ResultsListCtrl.SetCurSel(0);
	}

	SetOkButtonState();
}

bool CSearchDlg::IsSameDay(CTime &ropDate1, CTime &ropDate2)
{
	if(ropDate1 == TIMENULL || ropDate2 == TIMENULL)
		return false; // invalid dates

	return (ropDate1.GetDay() == ropDate2.GetDay() &&
			ropDate1.GetMonth() == ropDate2.GetMonth() &&
			ropDate1.GetYear() == ropDate2.GetYear()) ? true : false;
}

//////// Message handler functions //////////////////////////////////////////////////////

void CSearchDlg::OnSelchangeWhatCombo() 
{
	SaveOldValues();

	int ilSelectedLine = m_WhatCombo.GetCurSel();
	if(ilSelectedLine != CB_ERR && ilSelectedLine < omSearchTypes.GetSize())
	{
		if(ilSelectedLine != CB_ERR)
		{
			m_WhatCombo.GetLBText(ilSelectedLine, omSelectedWhat);
		}
		prmSelectedSearchType = GetSearchType(omSelectedWhat);
		HandleWhatChange(prmSelectedSearchType);
	}
}

void CSearchDlg::OnSelchangeWhereCombo()
{
	int ilSelectedLine = m_WhereCombo.GetCurSel();
	if(ilSelectedLine != CB_ERR)
	{
		if(ilSelectedLine != CB_ERR)
		{
			m_WhereCombo.GetLBText(ilSelectedLine, omSelectedWhere);
		}
	}
}

void CSearchDlg::OnSelchangeWhenCombo() 
{
	omSelectedWhen = ogBasicData.GetTimeframeStart() + CTimeSpan(m_WhenCombo.GetCurSel(),0,0,0);
	UpdateResultsList();
}

void CSearchDlg::OnChangeEdit1() 
{
	CString olText;
	m_Edit1.GetWindowText(olText);
	olText.MakeUpper();
	if(olOldEditValues[0] != olText)
	{
		olOldEditValues[0] = olText;
		UpdateResultsList();
	}
}

void CSearchDlg::OnChangeEdit2() 
{
	CString olText;
	m_Edit2.GetWindowText(olText);
	olText.MakeUpper();
	if(olOldEditValues[1] != olText)
	{
		olOldEditValues[1] = olText;
		UpdateResultsList();
	}
}

void CSearchDlg::OnChangeEdit3() 
{
	CString olText;
	m_Edit3.GetWindowText(olText);
	olText.MakeUpper();
	if(olOldEditValues[2] != olText)
	{
		olOldEditValues[2] = olText;
		UpdateResultsList();
	}
}

void CSearchDlg::OnChangeEdit4() 
{
	CString olText;
	m_Edit4.GetWindowText(olText);
	olText.MakeUpper();
	if(olOldEditValues[3] != olText)
	{
		olOldEditValues[3] = olText;
		UpdateResultsList();
	}
}

void CSearchDlg::OnChangeEdit5() 
{
	CString olText;
	m_Edit5.GetWindowText(olText);
	olText.MakeUpper();
	if(olOldEditValues[4] != olText)
	{
		olOldEditValues[4] = olText;
		UpdateResultsList();
	}
}

void CSearchDlg::OnChangeEdit6() 
{
	CString olText;
	m_Edit6.GetWindowText(olText);
	olText.MakeUpper();
	if(olOldEditValues[5] != olText)
	{
		olOldEditValues[5] = olText;
		UpdateResultsList();
	}
}

void CSearchDlg::OnChangeEdit7() 
{
	CString olText;
	m_Edit7.GetWindowText(olText);
	olText.MakeUpper();
	if(olOldEditValues[5] != olText)
	{
		olOldEditValues[5] = olText;
		UpdateResultsList();
	}
}

void CSearchDlg::OnSelchangeResultslist() 
{
	SetOkButtonState();
}

void CSearchDlg::OnSelcancelResultslist() 
{
	SetOkButtonState();
}

void CSearchDlg::SetOkButtonState(void)
{
	if(m_ResultsListCtrl.GetCurSel() != LB_ERR)
	{
		m_OkButtonCtrl.EnableWindow(TRUE);
	}
	else
	{
		m_OkButtonCtrl.EnableWindow(FALSE);
	}
}

void CSearchDlg::OnDblclkResultslist() 
{
	OnOK();
}
//////////// Interface with the calling environment ///////////////////////////////7

void CSearchDlg::AddSearchField(CString opWhat, CString opField, int ipFieldLen, CString opInitialValue /*= ""*/)
{
	SEARCHTYPE *prlSearchType = GetSearchType(opWhat);
	if(prlSearchType == NULL)
	{
		prlSearchType = AddSearchType(opWhat);
	}
	if(prlSearchType != NULL)
	{
		prlSearchType->Fields.Add(opField);
		prlSearchType->FieldLens.Add(ipFieldLen);
		if(opInitialValue.IsEmpty())
		{
			opInitialValue = ogOldSearchValues.GetOldSearchValue(opWhat, opField);
		}
		prlSearchType->InitialValues.Add(opInitialValue);
	}
}

void CSearchDlg::AddWhere(CString opWhat, CString opWhere)
{
	SEARCHTYPE *prlSearchType = GetSearchType(opWhat);
	if(prlSearchType == NULL)
	{
		prlSearchType = AddSearchType(opWhat);
	}
	if(prlSearchType != NULL)
	{
		prlSearchType->WhereList.Add(opWhere);
	}
}

void CSearchDlg::AddData(CString opWhat, CString opSearchString, CString opDisplayString /*= ""*/,
						 long lpUserData /*=0L*/, CTime opDate1 /*=TIMENULL*/, CTime opDate2 /*=TIMENULL*/)
{
	CStringArray olSearchStrings;
	olSearchStrings.Add(opSearchString);
	AddData(opWhat, olSearchStrings, opDisplayString, lpUserData, opDate1, opDate2);
}

void CSearchDlg::AddData(CString opWhat, CStringArray &ropSearchStrings, CString opDisplayString /*= ""*/, long lpUserData /*=0L*/, CTime opDate1 /*=TIMENULL*/, CTime opDate2 /*=TIMENULL*/)
{
	SEARCHTYPE *prlSearchType = GetSearchType(opWhat);
	if(prlSearchType == NULL)
	{
		prlSearchType = AddSearchType(opWhat);
	}
	if(prlSearchType != NULL)
	{
		SEARCHDATA *prlSearchData = new SEARCHDATA;
		if(prlSearchData != NULL)
		{
			CString olSS;
			int ilNumSearchStrings = ropSearchStrings.GetSize();
			for(int ilSS = 0; ilSS < ilNumSearchStrings; ilSS++)
			{
				olSS = ropSearchStrings[ilSS];
				olSS.MakeUpper();
				prlSearchData->SearchStrings.Add(olSS);
			}
			//prlSearchData->SearchStrings.Copy(ropSearchStrings);
			if(opDisplayString.IsEmpty())
			{
				CString olDisplayString;
				int ilNumSS = ropSearchStrings.GetSize();
				for(int ilSS = 0; ilSS < ilNumSS; ilSS)
				{
					olDisplayString += ropSearchStrings[ilSS] + CString(" ");
				}
				olDisplayString.Replace(SEARCH_FIELD_CHOICE_SEPERATOR,'/');

				CString olDateString;
				if(opDate1 != TIMENULL)
				{
					if(opDate2 != TIMENULL)
					{
						olDateString.Format(" %s-%s",opDate1.Format("%H%M/%d"),opDate2.Format("%H%M/%d"));
					}
					else
					{
						olDateString.Format(" %s",opDate1.Format("%H%M/%d"));
					}
					olDisplayString += olDateString;
				}
				prlSearchData->DisplayString = olDisplayString;
			}
			else
			{
				prlSearchData->DisplayString = opDisplayString;
			}
			prlSearchData->UserData = lpUserData;
			prlSearchData->Date1 = opDate1;
			prlSearchData->Date2 = opDate2;
			prlSearchType->Data.Add(prlSearchData);
		}
	}
}

void CSearchDlg::AddSearchFieldChoice(CString &ropSearchString, CString opNewString)
{
	ropSearchString += omSearchFieldChoiceSeperator + opNewString;
}

// return to the user the value selected in the results list
void CSearchDlg::GetSelectedData(CString &ropWhat, CString &ropWhere, CTime &ropWhen, long &rlpUserData , 
								 CStringArray &ropSearchStrings, CString &ropDisplayString, CTime &ropDate1, CTime &ropDate2)
{
	ropWhat = omSelectedWhat;
	ropWhere = omSelectedWhere;
	ropWhen = omSelectedWhen;
	rlpUserData = lmSelectedUserData;

	if(prmSelectedSearchType != NULL)
	{
		int ilDataCnt = prmSelectedSearchType->Data.GetSize();
		for(int ilD = 0; ilD < ilDataCnt; ilD++)
		{
			SEARCHDATA *prlSd = &prmSelectedSearchType->Data[ilD];
			if(prlSd->UserData == lmSelectedUserData)
			{
				int ilNumSS = prlSd->SearchStrings.GetSize();
				for(int ilSS = 0; ilSS < ilNumSS; ilSS++)
				{
					ropSearchStrings.Add(prlSd->SearchStrings[ilSS]);
				}
				ropDate1 = prlSd->Date1;
				ropDate2 = prlSd->Date2;
				ropDisplayString = prlSd->DisplayString;
				break;
			}
		}
	}
}

//PRF 8712
void CSearchDlg::OnMove(int x, int y)
{
	CDialog::OnMove(x,y);
	if(this->IsIconic() == FALSE)
	{
		GetWindowRect(&omWindowRect);
	}
}

/// ****** COldSearchValues ****** ////////////////////////////////////////////////////////////////////
COldSearchValues::COldSearchValues(void)
{
}

COldSearchValues::~COldSearchValues()
{
	DeleteOldSearchValues();
}

void COldSearchValues::DeleteOldSearchValues(void)
{
	CMapStringToString *polSingleMap;
	CString opWhat;
	POSITION rlPos;
	for(rlPos = omWhatMap.GetStartPosition(); rlPos != NULL; )
	{
		omWhatMap.GetNextAssoc(rlPos, opWhat,(void *& )polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	omWhatMap.RemoveAll();
}

void COldSearchValues::SetOldSearchValue(CString opWhat, CString opField, CString opOldValue)
{
	CMapStringToString *polSingleMap;
	if(!omWhatMap.Lookup(opWhat, (void *&) polSingleMap))
	{
		polSingleMap = new CMapStringToString;
		omWhatMap.SetAt(opWhat, (void *&) polSingleMap);
	}
	polSingleMap->SetAt(opField, opOldValue);
}

CString COldSearchValues::GetOldSearchValue(CString opWhat, CString opField)
{
	CString olOldSearchValue;

	CMapStringToString *polSingleMap;
	if(omWhatMap.Lookup(opWhat,(void *& )polSingleMap))
	{
		polSingleMap->Lookup(opField, olOldSearchValue);
	}

	return olOldSearchValue;
}

void COldSearchValues::SaveOldSearchValuesToDb()
{
	ogCfgData.DeleteByCtyp(SEARCHDLG_KEY);

	CMapStringToString *polSingleMap;
	CString olWhat, olField, olValue, olTmpText, olText;

	for(POSITION rlPos1 = omWhatMap.GetStartPosition(); rlPos1 != NULL; )
	{
		omWhatMap.GetNextAssoc(rlPos1, olWhat,(void *& )polSingleMap);
		olText.Empty();

		for(POSITION rlPos2 = polSingleMap->GetStartPosition(); rlPos2 != NULL; )
		{
			polSingleMap->GetNextAssoc(rlPos2, olField, olValue);
			olTmpText.Format("%s=%s", olField, olValue);
			if(!olText.IsEmpty())
			{
				olText += CString(SEARCHDLG_SEPERATOR);
			}
			olText += olTmpText;
		}

		ogCfgData.CreateCfg(SEARCHDLG_KEY, olWhat, olText);
	}
}

void COldSearchValues::LoadOldSearchValuesFromDb()
{
	DeleteOldSearchValues();

	CStringArray olFields;
	CString olField, olValue;

	CCSPtrArray <CFGDATA> olCfgs;
	int ilNumCfgs = ogCfgData.GetCfgsByCtyp(SEARCHDLG_KEY, olCfgs);
	for(int ilCfg = 0; ilCfg < ilNumCfgs; ilCfg++)
	{
		CFGDATA *prlCfg = &olCfgs[ilCfg];

		ogBasicData.ExtractItemList(prlCfg->Text, &olFields, SEARCHDLG_SEPERATOR);
		int ilNumFields = olFields.GetSize();
		for(int ilF = 0; ilF < ilNumFields; ilF++)
		{
			GetFieldAndValue(olFields[ilF], olField, olValue);
			SetOldSearchValue(prlCfg->Ckey, olField, olValue);
		}
	}

}

void COldSearchValues::GetFieldAndValue(CString &ropFieldAndValue, CString &ropField, CString &ropValue)
{
	ropField.Empty();
	ropValue.Empty();

	int ilLen = ropFieldAndValue.GetLength();
	bool blSeperatorFound = false;

	for(int ilC = 0; ilC < ilLen; ilC++)
	{
		if(blSeperatorFound)
		{
			ropValue += ropFieldAndValue[ilC];
		}
		else if(ropFieldAndValue[ilC] == '=')
		{
			blSeperatorFound = true;
		}
		else
		{
			ropField += ropFieldAndValue[ilC];
		}
	}
}
