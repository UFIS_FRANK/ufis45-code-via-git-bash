// CheckDataConsistencyDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <CheckDataConsistencyDlg.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaJtyData.h>
#include <CedaAloData.h>
#include <CedaFlightData.h>
#include <CedaShiftData.h>
#include <CedaDelData.h>
#include <CedaDrdData.h>
#include <CedaPolData.h>
#include <CedaEquData.h>
#include <CedaGatData.h>
#include <CedaPstData.h>
#include <CedaCicData.h>
#include <CedaEmpData.h>
#include <DataSet.h>
#include <BasicData.h>
#include <AllocData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCheckDataConsistencyDlg dialog


CCheckDataConsistencyDlg::CCheckDataConsistencyDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCheckDataConsistencyDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCheckDataConsistencyDlg)
	m_ErrList = _T("");
	//}}AFX_DATA_INIT

	omNewLine = "\r\n   --> ";
}


void CCheckDataConsistencyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCheckDataConsistencyDlg)
	DDX_Text(pDX, IDC_ERRLIST, m_ErrList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCheckDataConsistencyDlg, CDialog)
	//{{AFX_MSG_MAP(CCheckDataConsistencyDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCheckDataConsistencyDlg message handlers

BOOL CCheckDataConsistencyDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CStringArray olErrors;

	CheckDataConsistency(olErrors);

	int ilNumErrs = olErrors.GetSize();
	for(int ilE = 0; ilE < ilNumErrs; ilE++)
	{
		m_ErrList += olErrors[ilE] + CString("\r\n");
	}

	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCheckDataConsistencyDlg::CheckDataConsistency(CStringArray &ropErrors)
{
	CString olHeader, olErr, olTmp;
	bool blErrorsFound = false;
	int ilNumJobs = ogJobData.omData.GetSize();
	for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
	{
		JOBDATA *prlJob = &ogJobData.omData[ilJ];
		olHeader.Format("JOB URNO <%d> %s from %s to %s", prlJob->Urno, ogDataSet.JobBarText(prlJob),prlJob->Acfr.Format("%H:%M/%d"),prlJob->Acto.Format("%H:%M/%d"));
		olErr.Empty();

		JTYDATA *prlJty = ogJtyData.GetJtyByUrno(prlJob->Ujty);
		if(prlJty == NULL)
		{
			olErr += FormatError("Job Type UJTY <%d> not in JTYTAB.", prlJob->Ujty);
		}
		else
		{
			olTmp.Format("JobType <%s>", prlJty->Dscr);
			olHeader += olTmp;
		}


		EMPDATA *prlEmp = NULL;
		if(prlJob->Ustf != 0L)
		{
			if((prlEmp = ogEmpData.GetEmpByUrno(prlJob->Ustf)) == NULL)
			{
				olErr += FormatError("Employee URNO (JOBTAB.USTF) <%d> not in STFTAB.", prlJob->Ustf);
			}
			else
			{
				olTmp.Format("Emp: <%s>", ogEmpData.GetEmpName(prlJob->Ustf));
				olHeader += olTmp;
			}
		}

		if(prlJob->Ualo != 0 && ogAloData.GetAloByUrno(prlJob->Ualo) == NULL)
		{
			olErr += FormatError("Allocation Unit (JOBTAB.UALO) <%d> not in ALOTAB.", prlJob->Ualo);
		}

		if(prlJob->Acfr >= prlJob->Acto)
		{
			olErr += FormatError("Job begin (JOBTAB.ACFR) <%s> later than job end (JOBTAB.ACTO) <%s>.", prlJob->Acfr.Format("%Y%m%d%H%M%S"), prlJob->Acto.Format("%Y%m%d%H%M%S"));
		}

		if(prlJob->Jour != 0 && ogJobData.GetJobByUrno(prlJob->Jour) == NULL)
		{
			olErr += FormatError("Pool Job URNO (JOBTAB.JOUR) <%d> not in JOBTAB.", prlJob->Jour);
		}

		FLIGHTDATA *prlFlight = NULL;
		if(prlJob->Flur != 0 && (prlFlight = ogBasicData.GetFlightForJob(prlJob)) == NULL)
		{
			olErr += FormatError("Flight URNO (JOBTAB.UAFT) <%d> not in AFTTAB.", prlJob->Flur);
		}

		if(prlJob->Shur != 0 && ogShiftData.GetShiftByUrno(prlJob->Shur) == NULL)
		{
			olErr += FormatError("Shift URNO (JOBTAB.UDSR) <%d> not in DRRTAB.", prlJob->Shur);
		}

		if(prlJob->Udel != 0 && ogDelData.GetDelByUrno(prlJob->Udel) == NULL)
		{
			olErr += FormatError("Shift Delegation URNO (JOBTAB.UDEL) <%d> not in DELTAB.", prlJob->Udel);
		}

		if(prlJob->Udrd != 0 && ogDrdData.GetDrdByUrno(prlJob->Udrd) == NULL)
		{
			olErr += FormatError("Shift Deviation (JOBTAB.UDRD) <%d> not in DRDTAB.", prlJob->Udrd);
		}

		DEMANDDATA *prlDemand = ogJodData.GetDemandForJob(prlJob);
		if(prlDemand != NULL)
		{
			if(strcmp(prlDemand->Aloc, prlJob->Aloc))
			{
				olErr += FormatError("Different Allocation Unit Types for job (JOBTAB.ALOC) <%s> and demand (DEMTAB.ALOC) <%s> DEMTAB.URNO <%d>.", prlJob->Aloc, prlDemand->Aloc, prlDemand->Urno);
			}
			if(strcmp(prlDemand->Alid, prlJob->Alid))
			{
				olErr += FormatError("Different Allocation Units for job (JOBTAB.ALID) <%s> and demand (DEMTAB.ALID) <%s> DEMTAB.URNO <%d>.", prlJob->Alid, prlDemand->Alid, prlDemand->Urno);
			}
			if(strcmp(prlDemand->Dety, prlJob->Dety))
			{
				olErr += FormatError("Different Demand Types for job (JOBTAB.DETY) <%s> and demand (DEMTAB.DETY) <%s> DEMTAB.URNO <%d>.", prlJob->Dety, prlDemand->Dety, prlDemand->Urno);
			}
		}
		else if(!strcmp(prlJob->Jtco,JOBFID))
		{
			olErr += FormatError("Demand not found for flight independent job.");
		}

		if(!strcmp(prlJob->Jtco,JOBPOOL) || !strcmp(prlJob->Jtco,JOBDETACH) || !strcmp(prlJob->Jtco,JOBTEAMDELEGATION))
		{
			if(ogPolData.GetPolByUrno(prlJob->Uaid) == NULL)
			{
				olErr += FormatError("Pool URNO (JOBTAB.UAID) <%d> not in POLTAB.", prlJob->Uaid);
			}
		}
		else if(!strcmp(prlJob->Jtco,JOBEQUIPMENT) || !strcmp(prlJob->Jtco,JOBEQUIPMENTFASTLINK))
		{
			if(ogEquData.GetEquByUrno(prlJob->Uequ) == NULL)
			{
				olErr += FormatError("Equipment URNO (JOBTAB.UEQU) <%d> not in EQUTAB.", prlJob->Uequ);
			}
		}
		else if(!strcmp(prlJob->Jtco,JOBGATE))
		{
			if(prlJob->Uaid != 0 && ogGatData.GetGatByUrno(prlJob->Uaid) == NULL)
			{
				olErr += FormatError("Gate URNO (JOBTAB.UAID) <%d> not in GATTAB.", prlJob->Uaid);
			}
			if(prlFlight != NULL && strcmp(ogFlightData.GetGate(prlFlight->Urno), prlJob->Alid))
			{
				olErr += FormatError("Gate (JOBTAB.ALID) <%s> does not match flight gate <%s> for the flight <%s> AFTTAB.URNO <%s>", prlJob->Alid, ogFlightData.GetGate(prlFlight->Urno), ogFlightData.GetFlightNum(prlFlight->Urno),prlFlight->Urno);
			}
		}
		else if(!strcmp(prlJob->Jtco,JOBPST))
		{
			if(prlJob->Uaid != 0 && ogPstData.GetPstByUrno(prlJob->Uaid) == NULL)
			{
				olErr += FormatError("Position URNO (JOBTAB.UAID) <%d> not in PSTTAB.", prlJob->Uaid);
			}
			if(prlFlight != NULL && strcmp(ogFlightData.GetPosition(prlFlight->Urno), prlJob->Alid))
			{
				olErr += FormatError("Position (JOBTAB.ALID) <%s> does not match flight position <%s> for the flight <%s> AFT.URNO <%s>", prlJob->Alid, ogFlightData.GetPosition(prlFlight->Urno), ogFlightData.GetFlightNum(prlFlight->Urno),prlFlight->Urno);
			}
		}
		else if(!strcmp(prlJob->Jtco,JOBCIC))
		{
			if(prlJob->Uaid != 0 && ogCicData.GetCicByUrno(prlJob->Uaid) == NULL)
			{
				olErr += FormatError("Checkin Counter URNO (JOBTAB.UAID) <%d> not in CICTAB.", prlJob->Uaid);
			}
		}

		if(!olErr.IsEmpty())
		{
			olTmp.Format("Created: %s %s  Updated: %s %s", prlJob->Usec, prlJob->Cdat.Format("%H:%M/%d"), prlJob->Useu, prlJob->Lstu.Format("%H:%M/%d"));
			olHeader += olTmp;

			ropErrors.Add(olHeader + olErr);
			blErrorsFound = true;
		}
	}

	if(!blErrorsFound)
	{
		olErr.Format("JOBTAB - %d Jobs checked and no errors found!", ilNumJobs);
		ropErrors.Add(olErr);
	}


	blErrorsFound = false;
	int ilNumDems = ogDemandData.omData.GetSize();
	for(int ilD = 0; ilD < ilNumDems; ilD++)
	{
		DEMANDDATA *prlDem = &ogDemandData.omData[ilD];
		olHeader.Format("DEMAND URNO <%d> %s from %s to %s", prlDem->Urno, ogBasicData.GetDemandBarText(prlDem),prlDem->Debe.Format("%H:%M/%d"),prlDem->Deen.Format("%H:%M/%d"));
		olErr.Empty();

		if(prlDem->Debe >= prlDem->Deen)
		{
			olErr += FormatError("Demand Begin (DEMATAB.DEBE) <%s> later than Demand End (DEMTAB.DEEN) <%s>.", prlDem->Debe.Format("%Y%m%d%H%M%S"), prlDem->Deen.Format("%Y%m%d%H%M%S"));
		}

		if((*prlDem->Dety == INBOUND_DEMAND || *prlDem->Dety == TURNAROUND_DEMAND) && ogFlightData.GetFlightByUrno(prlDem->Ouri) == NULL)
		{
			olErr += FormatError("Inbound Flight URNO (DEMTAB.OURI) <%d> not in AFTTAB.", prlDem->Ouri);
		}

		if((*prlDem->Dety == OUTBOUND_DEMAND || *prlDem->Dety == TURNAROUND_DEMAND) && ogFlightData.GetFlightByUrno(prlDem->Ouro) == NULL)
		{
			olErr += FormatError("Outbound Flight URNO (DEMTAB.OURO) <%d> not in AFTTAB.", prlDem->Ouro);
		}

		FLIGHTDATA *prlFlight = ogDataSet.GetFlightByDemand(prlDem);

		if(!strcmp(prlDem->Aloc,ALLOCUNITTYPE_GATE))
		{
			if(strlen(prlDem->Alid) > 0 && ogGatData.GetGatByName(prlDem->Alid) == NULL)
			{
				olErr += FormatError("Gate (DEMTAB.ALID) <%s> not in GATTAB.", prlDem->Alid);
			}
			if(prlFlight != NULL && strcmp(ogFlightData.GetGate(prlFlight->Urno), prlDem->Alid))
			{
				olErr += FormatError("Gate (DEMTAB.ALID) <%s> does not match flight gate <%s> for the flight <%s> AFTTAB.URNO <%s>", prlDem->Alid, ogFlightData.GetGate(prlFlight->Urno), ogFlightData.GetFlightNum(prlFlight->Urno),prlFlight->Urno);
			}
		}
		else if(!strcmp(prlDem->Aloc,ALLOCUNITTYPE_PST))
		{
			if(strlen(prlDem->Alid) > 0 && ogPstData.GetPstByName(prlDem->Alid) == NULL)
			{
				olErr += FormatError("Position (DEMTAB.ALID) <%s> not in PSTTAB.", prlDem->Alid);
			}
			if(prlFlight != NULL && strcmp(ogFlightData.GetPosition(prlFlight->Urno), prlDem->Alid))
			{
				olErr += FormatError("Position (DEMTAB.ALID) <%s> does not match flight position <%s> for the flight <%s> AFTTAB.URNO <%s>", prlDem->Alid, ogFlightData.GetGate(prlFlight->Urno), ogFlightData.GetFlightNum(prlFlight->Urno),prlFlight->Urno);
			}
		}
		else if(!strcmp(prlDem->Aloc,ALLOCUNITTYPE_CIC))
		{
			if(strlen(prlDem->Alid) > 0 && ogCicData.GetCicByName(prlDem->Alid) == NULL)
			{
				olErr += FormatError("Checkin Counter (DEMTAB.ALID) <%s> not in CICTAB.", prlDem->Alid);
			}
		}


		if(!olErr.IsEmpty())
		{
			olTmp.Format("Created: %s %s  Updated: %s %s", prlDem->Usec, prlDem->Cdat.Format("%H:%M/%d"), prlDem->Useu, prlDem->Lstu.Format("%H:%M/%d"));
			olHeader += olTmp;

			ropErrors.Add(olHeader + olErr);
			blErrorsFound = true;
		}
	}

	if(!blErrorsFound)
	{
		olErr.Format("DEMTAB - %d Demands checked and no errors found!", ilNumDems);
		ropErrors.Add(olErr);
	}
}

CString CCheckDataConsistencyDlg::FormatError(char *pcpFormatList, ...)
{
	char pclText[1000];
	memset(pclText,'\0',1000);
	va_list args;
	va_start(args, pcpFormatList);
	_vsnprintf( pclText, 999, pcpFormatList, args);
	return omNewLine + CString(pclText);
}
