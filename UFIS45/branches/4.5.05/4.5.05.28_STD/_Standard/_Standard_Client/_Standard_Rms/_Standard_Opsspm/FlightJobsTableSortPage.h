// FlightJobsTableSortPage.h : header file
//
#ifndef __FLIGHTJOBSTABLE_SORTPAGE__
#define __FLIGHTJOBSTABLE_SORTPAGE__
/////////////////////////////////////////////////////////////////////////////
// FlightJobsTableSortPage dialog

class FlightJobsTableSortPage : public CPropertyPage
{
	DECLARE_DYNCREATE(FlightJobsTableSortPage)

// Construction
public:
	FlightJobsTableSortPage();
	~FlightJobsTableSortPage();

// Dialog Data
	CStringArray omSortOrders;
	CString omTitle;

	//{{AFX_DATA(FlightJobsTableSortPage)
	enum { IDD = IDD_FLIGHTJOBSTABLE_SORT_PAGE };
	int		m_SortOrder0;
	int		m_SortOrder1;
	int		m_SortOrder2;
	BOOL m_DisplayArr;
	BOOL m_DisplayDep;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(FlightJobsTableSortPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(FlightJobsTableSortPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnTime1Direction();
	afx_msg void OnTime2Direction();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Helper routines
private:
	int GetSortOrder(const char *pcpSortKey);
	CString GetSortKey(int ipSortOrder);
	void SetTitle(UINT ipControlId, UINT ipTextId);
	CBitmap omUpArrow, omDownArrow;
	bool SetDirectionButton(int ipButtonId, bool bpAscending);

public:
	bool bmTime1Ascending, bmTime2Ascending;
};

#endif // __FLIGHTJOBSTABLE_SORTPAGE__
