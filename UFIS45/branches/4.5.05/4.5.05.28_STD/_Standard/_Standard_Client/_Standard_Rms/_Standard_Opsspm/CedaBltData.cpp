// CedaBltData.cpp: Auto assignment parameters for line maintenence (Reduktionstufen)
//
//////////////////////////////////////////////////////////////////////

#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaBltData.h>
#include <BasicData.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CedaBltData::CedaBltData()
{                  
    BEGIN_CEDARECINFO(BLTDATA, BLTDATARecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Bnam,"BNAM")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(BLTDATARecInfo)/sizeof(BLTDATARecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&BLTDATARecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"BLTTAB");
    pcmFieldList = "URNO,BNAM";
}
 
CedaBltData::~CedaBltData()
{
	TRACE("CedaBltData::~CedaBltData called\n");
	ClearAll();
}

void CedaBltData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
}

CCSReturnCode CedaBltData::ReadBltData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];
    strcpy(pclWhere,"");
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaBltData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		BLTDATA rlBLTDATA;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlBLTDATA)) == RCSuccess)
			{
				AddBltInternal(rlBLTDATA);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaBltData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(BLTDATA), pclWhere);
    return ilRc;
}


void CedaBltData::AddBltInternal(BLTDATA &rrpBlt)
{
	BLTDATA *prlBlt = new BLTDATA;
	*prlBlt = rrpBlt;
	omData.Add(prlBlt);
	omUrnoMap.SetAt((void *)prlBlt->Urno,prlBlt);
}

BLTDATA* CedaBltData::GetBltByUrno(long lpUrno)
{
	BLTDATA *prlBlt = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlBlt);
	return prlBlt;
}

CString CedaBltData::GetTableName(void)
{
	return CString(pcmTableName);
}

void CedaBltData::GetAllBltNames(CStringArray &ropBltNames)
{
	int ilNumBlt = omData.GetSize();
	for(int ilBlt = 0; ilBlt < ilNumBlt; ilBlt++)
	{
		ropBltNames.Add(omData[ilBlt].Bnam);
	}
}
