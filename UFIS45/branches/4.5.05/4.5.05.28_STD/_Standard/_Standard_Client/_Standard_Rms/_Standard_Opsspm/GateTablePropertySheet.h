// GateTablePropertySheet.h : header file
//
#ifndef _GATETBPS_H_
#define _GATETBPS_H_
/////////////////////////////////////////////////////////////////////////////
// GateTablePropertySheet

class GateTablePropertySheet : public BasePropertySheet
{
// Construction
public:
	GateTablePropertySheet(CWnd* pParentWnd = NULL,
		CViewer *popViewer = NULL, UINT iSelectPage = 0);

// Attributes
public:
	FilterPage m_pageGate;
	FilterPage m_pageAirline;
	GateTableSortPage m_pageSort;
//	ZeitPage m_pageZeit;

// Operations
public:
	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
	virtual int QueryForDiscardChanges();
};

#endif // _GATETBPS_H_
/////////////////////////////////////////////////////////////////////////////

