// CedaShiftData.cpp - Class for handling Employee data
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaObject.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <BasicData.h>
#include <CedaShiftData.h>
#include <CedaSwgData.h>
#include <CedaBsdData.h>
#include <CedaOdaData.h>
#include <CedaDrgData.h>
#include <CedaShiftTypeData.h>
#include <resource.h>
#include <PrmConfig.h>


//20011022 Planned SCOD/AVFR/AVTO read from stufe ROSS='L'

CedaShiftData::CedaShiftData()
{                  
    // Create an array of CEDARECINFO for STAFFDATA
    BEGIN_CEDARECINFO(SHIFTDATA, ShiftDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_LONG(Stfu,"STFU")
		FIELD_CHAR_TRIM(Sday,"SDAY")
		FIELD_DATE(Avfa,"AVFR")
		FIELD_DATE(Avta,"AVTO")
		FIELD_CHAR_TRIM(Sfca,"SCOD")
		FIELD_CHAR_TRIM(Sfco,"SCOO")
		FIELD_LONG(Bsdu,"BSDU")
        FIELD_CHAR_TRIM(Bkdp,"BKDP")
		FIELD_DATE(Sbfr,"SBFR")
		FIELD_DATE(Sbto,"SBTO")
		FIELD_CHAR_TRIM(Rema,"REMA")
		FIELD_CHAR_TRIM(Rosl,"ROSL")
		FIELD_CHAR_TRIM(Ross,"ROSS")
		FIELD_CHAR_TRIM(Sblu,"SBLU")
		FIELD_CHAR_TRIM(Fctc,"FCTC")
        FIELD_CHAR_TRIM(Useu,"USEU")
        FIELD_DATE(Lstu,"LSTU")
		FIELD_CHAR_TRIM(Drs1,"DRS1")
		FIELD_CHAR_TRIM(Drs2,"DRS2")
		FIELD_CHAR_TRIM(Drs3,"DRS3")
		FIELD_CHAR_TRIM(Drs4,"DRS4")
		FIELD_CHAR_TRIM(Drrn,"DRRN")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(ShiftDataRecInfo)/sizeof(ShiftDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&ShiftDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"DRRTAB");

	strcpy(pcmInsertCommand,"IRT");
	strcpy(pcmInsertFields,"URNO,PENO,SDAY,AVFA,AVTA,SFCA,SFCS,EGRP,EFCT,REMA,CDAT,USEC,LSTU,USEU");

	strcpy(pcmUpdateCommand,"URT");
	strcpy(pcmUpdateFields,"STFU,AVFR,AVTO,SCOD,SCOO,BSDU,BKDP,REMA,USEU,LSTU,SBFR,SBTO,SBLU,FCTC");

	ogCCSDdx.Register((void *)this, BC_SHIFT_DELETE,	CString("BC_SHIFT_DELETE"), CString("ShiftDataDelete"),	ProcessShiftCf);
	ogCCSDdx.Register((void *)this, BC_SHIFT_CHANGE,	CString("BC_SHIFT_CHANGE"), CString("ShiftDataChange"),	ProcessShiftCf);
	ogCCSDdx.Register((void *)this, BC_SHIFT_INSERT,	CString("BC_SHIFT_INSERT"), CString("ShiftDataInsert"),	ProcessShiftCf);
	ogCCSDdx.Register((void *)this, BC_SHIFT_SBC,		CString("BC_SHIFT_SBC"),	CString("ShiftDataSbc"),	ProcessShiftCf);
	ogCCSDdx.Register((void *)this, SWG_INSERT,			CString("SWG_INSERT"),		CString("Swg Insert"),		ProcessShiftCf2);
	ogCCSDdx.Register((void *)this, SWG_UPDATE,			CString("SWG_UPDATE"),		CString("Swg Update"),		ProcessShiftCf2);
	ogCCSDdx.Register((void *)this, SWG_DELETE,			CString("SWG_DELETE"),		CString("Swg Deleted"),		ProcessShiftCf2);
	ogCCSDdx.Register((void *)this, DRG_INSERT,			CString("DRG_INSERT"),		CString("Drg Insert"),		ProcessShiftCf2);
	ogCCSDdx.Register((void *)this, DRG_CHANGE,			CString("DRG_UPDATE"),		CString("Drg Update"),		ProcessShiftCf2);
	ogCCSDdx.Register((void *)this, DRG_DELETE,			CString("DRG_DELETE"),		CString("Drg Deleted"),		ProcessShiftCf2);
}

CedaShiftData::~CedaShiftData()
{
	TRACE("CedaShiftData::~CedaShiftData called\n");
	ogCCSDdx.UnRegister(this,NOTUSED);

	DeleteData();
}

void CedaShiftData::DeleteShiftInternal(SHIFTDATA *prpShift)
{
	if(prpShift != NULL)
	{
		omUrnoMap.RemoveKey((void *)prpShift->Urno);
		omPenoMap.RemoveKey(prpShift->Peno);

		CMapStringToPtr *polSingleMap;
		if(omUstfMap.Lookup((void *)prpShift->Stfu,(void *& )polSingleMap))
		{
			polSingleMap->RemoveKey(FormatSdayDrrn(prpShift->Sday,prpShift->Drrn));
		}
		if(omUstfMapLangzeit.Lookup((void *)prpShift->Stfu,(void *& )polSingleMap))
		{
			polSingleMap->RemoveKey(FormatSdayDrrn(prpShift->Sday,prpShift->Drrn));
		}

		int ilNumShifts = omData.GetSize(), ilShift;
		for(ilShift = 0; ilShift < ilNumShifts; ilShift++)
		{
			if(omData[ilShift].Urno == prpShift->Urno)
			{
				omData.DeleteAt(ilShift);
				break;
			}
		}
		ilNumShifts = omDataLangzeit.GetSize();
		for(ilShift = 0; ilShift < ilNumShifts; ilShift++)
		{
			if(omDataLangzeit[ilShift].Urno == prpShift->Urno)
			{
				omDataLangzeit.DeleteAt(ilShift);
				break;
			}
		}
	}
}


bool CedaShiftData::SwapShifts(SHIFTDATA *prpShift1, CString opFunction1, CString opWorkGroup1, CString opSub1,
							   SHIFTDATA *prpShift2, CString opFunction2, CString opWorkGroup2, CString opSub2)
{
	bool blRc = true;
	SHIFTDATA olOldShift1 = *prpShift1;
	SHIFTDATA olOldShift2 = *prpShift2;
	CString olRema;

	prpShift1->Stfu = olOldShift2.Stfu;
	strcpy(prpShift1->Sfco, olOldShift2.Sfca);
	strcpy(prpShift1->Peno, olOldShift2.Peno);
	strcpy(prpShift1->Fctc, opFunction2);
	strcpy(prpShift1->Egrp, opWorkGroup2);
	strcpy(prpShift1->Drs2, opSub2);
	olRema.Format("%s %s", GetString(IDS_SWAPSHIFTREMARK), ogEmpData.GetEmpName(olOldShift1.Stfu, true));
	strcpy(prpShift1->Rema, olRema.Left(40));


	// need to update the work groups before AddShiftInternal() is called so that shift.egrp is set correctly
//	if(strcmp(prpShift1->Egrp, olOldShift1.Egrp))
		ogDrgData.UpdateDrgCodeForShift(prpShift1);
	
	prpShift2->Stfu = olOldShift1.Stfu;
	strcpy(prpShift2->Sfco, olOldShift1.Sfca);
	strcpy(prpShift2->Peno, olOldShift1.Peno);
	strcpy(prpShift2->Fctc, opFunction1);
	strcpy(prpShift2->Egrp, opWorkGroup1);
	strcpy(prpShift2->Drs2, opSub1);
	olRema.Format("%s %s", GetString(IDS_SWAPSHIFTREMARK), ogEmpData.GetEmpName(olOldShift2.Stfu, true));
	strcpy(prpShift2->Rema, olRema.Left(40));
	
	// need to update the work groups before AddShiftInternal() is called so that shift.egrp is set correctly
//	if(strcmp(prpShift2->Egrp, olOldShift2.Egrp))
		ogDrgData.UpdateDrgCodeForShift(prpShift2);

//	if(UpdateShift(prpShift1, &olOldShift1) == RCFailure || UpdateShift(prpShift2, &olOldShift2) == RCFailure)
	if(!ReleaseChanges(prpShift1, prpShift2))
	{
		*prpShift1 = olOldShift1;
		*prpShift2 = olOldShift2;
		blRc = false;
	}
	else
	{
		SHIFTDATA *prlShift1 = new SHIFTDATA;
		*prlShift1 = *prpShift1;
		DeleteShiftInternal(prpShift1);
		AddShiftInternal(prlShift1);
		ogCCSDdx.DataChanged((void *)this,SHIFT_CHANGE,(void *)prlShift1);

		SHIFTDATA *prlShift2 = new SHIFTDATA;
		*prlShift2 = *prpShift2;
		DeleteShiftInternal(prpShift2);
		AddShiftInternal(prlShift2);
		ogCCSDdx.DataChanged((void *)this,SHIFT_CHANGE,(void *)prlShift2);
	}

	return blRc;
}

bool CedaShiftData::ReleaseChanges(SHIFTDATA *prpShift1, SHIFTDATA *prpShift2)
{
	bool blRc = false;
	if(prpShift1 != NULL && prpShift2 != NULL)
	{
		int ilFields = ogBasicData.GetItemCount(CString(pcmFieldList)); 
		CString olUrt; 

		CString olData;
		CString olNewLine("\n"), olComma(","), olUrno;

		// update with unreal URNO first - to satisfy STFU/ROSL/SDAY/DRRN unique constraint
		olUrt.Format("*CMD*,%s,URT,%d,%s,[URNO=:VURNO]\n", pcmTableName, ilFields, CString(pcmFieldList));
		long llRealStfUrno = prpShift1->Stfu;
		prpShift1->Stfu = prpShift1->Urno;
		olUrno.Format("%ld", prpShift1->Urno);
		prpShift1->Lstu = ogBasicData.GetTime();
		strcpy(prpShift1->Useu,ogUsername.Left(31));
		ConvertDatesToUtc(prpShift1); // convert LSTU/CDAT to UTC (DB format)
		MakeCedaData(&omRecInfo,olData,prpShift1);
		ConvertDatesToLocal(prpShift1); // convert LSTU/CDAT to local
		olUrt += olData + olComma + olUrno + olNewLine;

		if(Release(olUrt, false))
		{
			olData.Empty();
			olUrt.Format("*CMD*,%s,URT,%d,%s,[URNO=:VURNO]\n", pcmTableName, ilFields, CString(pcmFieldList));

			olUrno.Format("%ld", prpShift2->Urno);
			prpShift2->Lstu = ogBasicData.GetTime();
			strcpy(prpShift2->Useu,ogUsername.Left(31));
			ConvertDatesToUtc(prpShift2); // convert LSTU/CDAT to UTC (DB format)
			MakeCedaData(&omRecInfo,olData,prpShift2);
			ConvertDatesToLocal(prpShift2); // convert LSTU/CDAT to local
			olUrt += olData + olComma + olUrno + olNewLine;

			prpShift1->Stfu = llRealStfUrno;
			olUrno.Format("%ld", prpShift1->Urno);
			prpShift1->Lstu = ogBasicData.GetTime();
			strcpy(prpShift1->Useu,ogUsername.Left(31));
			ConvertDatesToUtc(prpShift1); // convert LSTU/CDAT to UTC (DB format)
			MakeCedaData(&omRecInfo,olData,prpShift1);
			ConvertDatesToLocal(prpShift1); // convert LSTU/CDAT to local
			olUrt += olData + olComma + olUrno + olNewLine;

			blRc = Release(olUrt);
		}
	}

	return blRc;
}

bool CedaShiftData::Release(CString opReleaseString, bool bpBroadcast /*= true*/)
{
	bool blRc = false;

	int ilLen = opReleaseString.GetLength();
	if(ilLen > 0)
	{
		char *pclDataArea = (char *) malloc(ilLen + 1);
		strcpy(pclDataArea, opReleaseString);
		char *pclRelString = (bpBroadcast) ? "QUICK" : "NOBC";
		if((blRc = CedaAction("REL",pclRelString,"",pclDataArea)) == false)
		{
			ogBasicData.LogCedaError("CedaShiftData Error",omLastErrorMessage,"REL",pcmFieldList,pcmTableName,"Release()");
		}
		free((char *)pclDataArea);
	}

	return blRc;
}


void CedaShiftData::DeleteData()
{
	CMapStringToPtr *polSingleMap;
	long llUrno;
	POSITION rlPos;
	for(rlPos = omUstfMap.GetStartPosition(); rlPos != NULL; )
	{
		omUstfMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	omUstfMap.RemoveAll();

	for(rlPos = omUstfMapLangzeit.GetStartPosition(); rlPos != NULL; )
	{
		omUstfMapLangzeit.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	omUstfMapLangzeit.RemoveAll();

	omUrnoMap.RemoveAll();
	omPenoMap.RemoveAll();
	omData.DeleteAll();
	omDataLangzeit.DeleteAll();
}


CString CedaShiftData::Dump(long lpUrno)
{
	CString olDumpStr;
	SHIFTDATA *prlShift = GetShiftByUrno(lpUrno);
	if(prlShift == NULL)
	{
		olDumpStr.Format("No SHIFTDATA Found for SHIFT.URNO <%ld>",lpUrno);
	}
	else
	{
		CString olData;
		MakeCedaData(&omRecInfo,olData,prlShift);
		CStringArray olFieldArray, olDataArray;
		ogBasicData.ExtractItemList(pcmFieldList, &olFieldArray);
		ogBasicData.ExtractItemList(olData, &olDataArray);
		int ilNumData = min(olDataArray.GetSize(),olFieldArray.GetSize());
		for(int ilD = 0; ilD < ilNumData; ilD++)
		{
			olDumpStr += olFieldArray[ilD] + " = <" + olDataArray[ilD] + ">    ";
		}
	}
	return olDumpStr;
}


bool CedaShiftData::ReadAllShifts(CTime opStartTime, CTime opEndTime)
{
	if(ogBasicData.ShiftHasFunctionCode())
	{
		pcmFieldList = "URNO,STFU,SDAY,AVFR,AVTO,SCOD,SCOO,BSDU,BKDP,SBFR,SBTO,REMA,ROSL,ROSS,SBLU,FCTC,USEU,LSTU,DRS1,DRS2,DRS3,DRS4,DRRN";
	}
	else
	{
		pcmFieldList = "URNO,STFU,SDAY,AVFR,AVTO,SCOD,SCOO,BSDU,BKDP,SBFR,SBTO,REMA,ROSL,ROSS,SBLU,USEU,LSTU,DRS1,DRS2,DRS3,DRS4,DRRN";
	}

    char pclWhere[512];
	bool blRc = true;
	char pclCom[10] = "RT";

	omLoadShiftsFrom = opStartTime + CTimeSpan(0,0,0,1); // PRF 4824
	omLoadShiftsTo = opEndTime - CTimeSpan(0,0,0,1); // PRF 4824

	CTime olDay = ogBasicData.omShiftFirstSDAY;
	CTimeSpan olOneDay(1,0,0,0);
	CString olSdayList, olTmp;
	while(olDay < ogBasicData.omShiftLastSDAY )
	{
		if(!olSdayList.IsEmpty())
		{
			olSdayList += ",";
		}
		olTmp.Format("'%s'", olDay.Format("%Y%m%d"));
		olSdayList += olTmp;

		olDay += olOneDay;
	}
	
	imNumShiftsLoaded = imNumAbsencesLoaded = 0;


    // Select data from the database
    //sprintf(pclWhere, "WHERE ROSL='%s' AND ROSS<>'%s' AND AVTO >= '%s' AND AVFR <= '%s'",MITARBEITER_TAGESLISTE,LAST_RECORD,omLoadShiftsFrom.Format("%Y%m%d%H%M%S"),omLoadShiftsTo.Format("%Y%m%d%H%M%S"));
	// NOTE: above ROSS<>'L' -> records that have been superceeded by another rostering level, these are now also
	// loaded because the operator may want to see pool jobs/shifts that were in the past
    sprintf(pclWhere, "WHERE (SDAY IN (%s) AND (ROSL='%s' OR ROSL='%s'))",olSdayList,MITARBEITER_TAGESLISTE,LANGZEITSTUFE);
//    sprintf(pclWhere, "WHERE (ROSL='%s' OR ROSL='%s') AND AVTO >= '%s' AND AVFR <= '%s'",MITARBEITER_TAGESLISTE,LANGZEITSTUFE,omLoadShiftsFrom.Format("%Y%m%d%H%M%S"),omLoadShiftsTo.Format("%Y%m%d%H%M%S"));
//20011022		    sprintf(pclWhere, "WHERE ROSL='%s' AND AVTO >= '%s' AND AVFR <= '%s'",MITARBEITER_TAGESLISTE,omLoadShiftsFrom.Format("%Y%m%d%H%M%S"),omLoadShiftsTo.Format("%Y%m%d%H%M%S"));

	if (bgIsPrm)
	{
		char clPrmSelection[132];
		
		if (ogBasicData.IsPrmHandlingAgentEnabled()) 
		{
			sprintf(clPrmSelection," AND FILT = '%s' ",ogPrmConfig.getHandlingAgentShortName());
			strcat(pclWhere,clPrmSelection);
		}
	}


    if((blRc = CedaAction2(pclCom, pclWhere, NULL, pcgDataBuf, "BUF1", false, 0, true, NULL, NULL,ogBasicData.GetDrrIndexHint().GetBuffer(0))) == false)
	{
		ogBasicData.LogCedaError("CedaShiftData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		// Load data from CCSCedaData into the dynamic array of record
		DeleteData();

		for (int ilLc = 0; blRc; ilLc++)
		{
			SHIFTDATA *prlShift = new SHIFTDATA;
			if((blRc = GetBufferRecord2(ilLc,prlShift)) == true && LoadShift(prlShift))
			{
				AddShift(prlShift);
			}
			else
			{
				delete prlShift;
			}
		}
		blRc = true;
	}
	
	if(blRc)
	{
		ogBasicData.Trace("CedaShiftData Read %d Shifts and %d Absences (Rec Size %d) %s",imNumShiftsLoaded, imNumAbsencesLoaded, sizeof(SHIFTDATA), pclWhere);
	}

    return blRc;
}

bool CedaShiftData::LoadShift(SHIFTDATA *prpShift)
{
	bool blLoadShift = false;

	if(ogOdaData.GetOdaByType(prpShift->Sfca) != NULL)
	{
		imNumAbsencesLoaded++;
		blLoadShift = true;
	}
	else if(prpShift->Avta >= omLoadShiftsFrom)
	{
		imNumShiftsLoaded++;
		blLoadShift = true;
	}

	return blLoadShift;
}

// Prepare some shift data, not read from database
void CedaShiftData::PrepareShiftData(SHIFTDATA *prpShift)
{
	strcpy(prpShift->Peno,ogEmpData.GetPenoByUrno(prpShift->Stfu));
	strcpy(prpShift->Egrp,ogBasicData.GetTeamForShift(prpShift));
	strcpy(prpShift->Efct,ogBasicData.GetFunctionForShift(prpShift));

	if(strlen(prpShift->Sday) <= 0)
	{
		// if SDAY has already been set in Rostering, then do not change it even if the date of
		// the shift is changed because this can result in the shift being displayed for the wrong day in rostering
		if(prpShift->Avfa != TIMENULL)
			strcpy(prpShift->Sday,prpShift->Avfa.Format("%Y%m%d"));
		else if(prpShift->Avta != TIMENULL)
			strcpy(prpShift->Sday,prpShift->Avta.Format("%Y%m%d"));
	}

	// if the shift code is not defined in BSDTAB then this is an absence code (defined in ODATAB)
	SHIFTTYPEDATA *prlShiftType = NULL;
	ODADATA *prlOda = ogOdaData.GetOdaByType(prpShift->Sfca);
	if(prlOda != NULL)
	{
		if(*prlOda->Work == '1')
			strcpy(prpShift->Ctyp,"S"); // if set to '1' then the absence code acts as a shift
		else
			strcpy(prpShift->Ctyp,"A");
		prpShift->Bsdu = prlOda->Urno;
	}
	else if((prlShiftType = ogShiftTypes.GetShiftTypeByCode(prpShift->Sfca))!= NULL)
	{
		strcpy(prpShift->Ctyp,"S");
		prpShift->Bsdu = prlShiftType->Urno;
	}
	else
	{
		prpShift->Bsdu = 0;
	}

	if(!strcmp(prpShift->Rosl,MITARBEITER_TAGESLISTE))
	{
		// scheduled times come from the Langezeit Stufe
		SHIFTDATA *prlLangezeitShift = GetShiftByUstfAndSdayAndRosl(prpShift->Stfu, prpShift->Sday, LANGZEITSTUFE, prpShift->Drrn);
		if(prlLangezeitShift != NULL)
		{
			strcpy(prpShift->Sfcs,prlLangezeitShift->Sfca);
			prpShift->Avfs = prlLangezeitShift->Avfa;
			prpShift->Avts = prlLangezeitShift->Avta;
		}
	}
	else if(!strcmp(prpShift->Rosl,LANGZEITSTUFE))
	{
		// scheduled times come from the Langezeit Stufe
		SHIFTDATA *prlNormalShift = GetShiftByUstfAndSdayAndRosl(prpShift->Stfu, prpShift->Sday, LANGZEITSTUFE, prpShift->Drrn);
		if(prlNormalShift != NULL)
		{
			strcpy(prlNormalShift->Sfcs,prpShift->Sfca);
			prlNormalShift->Avfs = prpShift->Avfa;
			prlNormalShift->Avts = prpShift->Avta;
		}
	}
}

void CedaShiftData::AddShift(SHIFTDATA *prpShift)
{
	ConvertDatesToLocal(prpShift);
	PrepareShiftData(prpShift);
	AddShiftInternal(prpShift);
}

void CedaShiftData::AddShiftInternal(SHIFTDATA *prpShift)
{
	CMapStringToPtr *polSingleMap;
	if(!strcmp(prpShift->Rosl,MITARBEITER_TAGESLISTE))
	{
		omData.Add(prpShift);
		omPenoMap.SetAt(prpShift->Peno,prpShift);
		omUrnoMap.SetAt((void *)prpShift->Urno,prpShift);

		if(omUstfMap.Lookup((void *) prpShift->Stfu, (void *&) polSingleMap))
		{
			polSingleMap->SetAt(FormatSdayDrrn(prpShift->Sday,prpShift->Drrn),prpShift);
		}
		else
		{
			polSingleMap = new CMapStringToPtr;
			polSingleMap->SetAt(FormatSdayDrrn(prpShift->Sday,prpShift->Drrn),prpShift);
			omUstfMap.SetAt((void *)prpShift->Stfu,polSingleMap);
		}
	}
	else if(!strcmp(prpShift->Rosl,LANGZEITSTUFE))
	{
		omDataLangzeit.Add(prpShift);
		if(omUstfMapLangzeit.Lookup((void *) prpShift->Stfu, (void *&) polSingleMap))
		{
			polSingleMap->SetAt(FormatSdayDrrn(prpShift->Sday,prpShift->Drrn),prpShift);
		}
		else
		{
			polSingleMap = new CMapStringToPtr;
			polSingleMap->SetAt(FormatSdayDrrn(prpShift->Sday,prpShift->Drrn),prpShift);
			omUstfMapLangzeit.SetAt((void *)prpShift->Stfu,polSingleMap);
		}
	}
}

CString CedaShiftData::FormatSdayDrrn(const char *pcpSday, const char *pcpDrrn)
{
	CString olSdayDrrn;
	olSdayDrrn.Format("%s,%s", pcpSday, pcpDrrn);
	return olSdayDrrn;
}

SHIFTDATA *CedaShiftData::GetShiftByUstfAndSdayAndRosl(long lpUstf, const char *pcpSday, const char *pcpRosl, const char *pcpDrrn)
{
	ASSERT(pcpSday);
	ASSERT(pcpRosl);

	SHIFTDATA *prlShift = NULL;

	CMapStringToPtr *polSingleMap;

	if(!strcmp(pcpRosl,MITARBEITER_TAGESLISTE))
	{
		if(omUstfMap.Lookup((void *)lpUstf, (void *& )polSingleMap))
		{
			polSingleMap->Lookup(FormatSdayDrrn(pcpSday,pcpDrrn), (void *& )prlShift);
		}
	}
	else if(!strcmp(pcpRosl,LANGZEITSTUFE))
	{
		if(omUstfMapLangzeit.Lookup((void *)lpUstf, (void *& )polSingleMap))
		{
			polSingleMap->Lookup(FormatSdayDrrn(pcpSday,pcpDrrn), (void *& )prlShift);
		}
	}

	return prlShift;
}

int CedaShiftData::GetShiftsByUstf(long lpUstf, CCSPtrArray <SHIFTDATA> &ropShifts, bool bpReset /* = true */)
{
	if(bpReset)
	{
		ropShifts.RemoveAll();
	}

	SHIFTDATA *prlShift = NULL;
	CString olSdayDrrn;
	CMapStringToPtr *polSingleMap;
	if(omUstfMap.Lookup((void *)lpUstf, (void *& )polSingleMap))
	{
		for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			polSingleMap->GetNextAssoc(rlPos, olSdayDrrn, (void *&) prlShift);
			ropShifts.Add(prlShift);
		}
	}

	return ropShifts.GetSize();
}


/////////////////////////////////////////////////////////////////////////////
// Update data methods (called from PrePlanTable class and StaffTable class)

CCSReturnCode CedaShiftData::InsertShift(SHIFTDATA *prpShiftData)
{
	return InsertShiftRecord(prpShiftData);
}

CCSReturnCode CedaShiftData::UpdateShift(SHIFTDATA *prpShiftData, SHIFTDATA *prpOldShiftData)
{
    return UpdateShiftRecord(prpShiftData, prpOldShiftData);
}


BOOL CedaShiftData::ShiftExist(long lpUrno)
{
	// don't read from database anymore, just check internal array
	SHIFTDATA *prpData;

	return(omUrnoMap.Lookup((void *) &lpUrno,(void *&)prpData) );
}

CCSReturnCode CedaShiftData::InsertShiftRecord(SHIFTDATA *prpShiftData)
{
	CTime olCurrTime = ogBasicData.GetTime();

	sprintf(pcmData, "%ld,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s%s,%s",
			prpShiftData->Urno, prpShiftData->Peno,prpShiftData->Sday,
			prpShiftData->Avfa.Format("%Y%m%d%H%M%S"),prpShiftData->Avta.Format("%Y%m%d%H%M%S"),
			prpShiftData->Sfca,prpShiftData->Sfcs,prpShiftData->Egrp,prpShiftData->Efct,
			prpShiftData->Rema,olCurrTime.Format("%Y%m%d%H%M%S"),ogUsername,olCurrTime.Format("%Y%m%d%H%M%S"),ogUsername);


	return (CedaAction(pcmInsertCommand, pcmTableName, pcmInsertFields, "", "", pcmData));
}

CCSReturnCode CedaShiftData::UpdateShiftRecord(SHIFTDATA *prpShiftData, SHIFTDATA *prpOldShiftData)
{
	CCSReturnCode olRc = RCSuccess;
	PrepareShiftData(prpShiftData);

	char pclFieldList[500], pclData[1000];
	//if(prpShiftData->Avfa != prpOldShiftData->Avfa || prpShiftData->Avta != prpOldShiftData->Avta)
	if(1) // PRF 6078 -  rostering requires that all DRR fields are broadcasted
	{
		// when the time of the shift has change, the whole record needs to be sent
		// so that it can be inserted without re-reading in other instances of OpssPm
		// where it was outside the timeframe but is now inside
		prpShiftData->Lstu = ogBasicData.GetTime();
		strcpy(prpShiftData->Useu,ogUsername.Left(31));
		ConvertDatesToUtc(prpShiftData); // convert LSTU/CDAT to UTC (DB format)

		CString olData;
		MakeCedaData(&omRecInfo,olData,prpShiftData);
		char *pclData = new char[olData.GetLength() + 100];
		strcpy(pclData, olData);

		char pclSelection[100];
		sprintf(pclSelection," WHERE URNO = '%ld%'",prpShiftData->Urno);
		if((olRc = CedaAction(pcmUpdateCommand, pcmTableName, pcmFieldList, pclSelection,"", pclData)) == RCSuccess)
		{
			ogCCSDdx.DataChanged((void *)this,SHIFT_CHANGE,(void *)prpShiftData);
		}

		delete [] pclData;
		ConvertDatesToLocal(prpShiftData); // convert LSTU/CDAT to local
	}
	else if(GetChangedFields(prpShiftData, prpOldShiftData, pclFieldList, pclData))
	{
		// set lstu/cdat etc
		prpShiftData->Lstu = ogBasicData.GetTime();
		CTime olUtcLstu = prpShiftData->Lstu; 
		ogBasicData.ConvertDateToUtc(olUtcLstu);
		strcpy(prpShiftData->Useu,ogUsername.Left(31));
		strcat(pclFieldList,",LSTU,USEU");
		strcat(pclData,","); strcat(pclData,olUtcLstu.Format("%Y%m%d%H%M%S"));
		strcat(pclData,","); strcat(pclData,prpShiftData->Useu);

		char pclSelection[100];
		sprintf(pclSelection," WHERE URNO = '%ld%'",prpShiftData->Urno);
		if((olRc = CedaAction(pcmUpdateCommand, pcmTableName, pclFieldList, pclSelection,"", pclData)) == RCSuccess)
		{
			ogCCSDdx.DataChanged((void *)this,SHIFT_CHANGE,(void *)prpShiftData);
		}
	}
	return olRc;
}




// the callback function for storing broadcasted fligthdata in FLIGHTDATA array

void  ProcessShiftCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogShiftData.ProcessShiftBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void  ProcessShiftCf2(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogShiftData.ProcessShiftDdx(ipDDXType,vpDataPointer,ropInstanceName);
}

void  CedaShiftData::ProcessShiftDdx(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	SHIFTDATA *prlShift = NULL;
	if(ipDDXType == SWG_INSERT || ipDDXType == SWG_UPDATE || ipDDXType == SWG_DELETE)
	{
		SWGDATA *prlSwg = (SWGDATA *)vpDataPointer;
		if(prlSwg != NULL)
		{
			CCSPtrArray <SHIFTDATA> olShifts;
			int ilNumShifts = GetShiftsByUstf(prlSwg->Surn, olShifts);
			for(int ilS = 0; ilS < ilNumShifts; ilS++)
			{
				prlShift = &olShifts[ilS];
				CString olTeam = ogBasicData.GetTeamForShift(prlShift);
				strcpy(prlShift->Egrp, olTeam);
				ogCCSDdx.DataChanged((void *)this,SHIFT_CHANGE,(void *)prlShift);
			}
		}
	}
	else if(ipDDXType == DRG_INSERT || ipDDXType == DRG_CHANGE || ipDDXType == DRG_DELETE)
	{
		DRGDATA *prlDrg = (DRGDATA *)vpDataPointer;
		if(prlDrg != NULL)
		{
			CCSPtrArray <SHIFTDATA> olShifts;
			int ilNumShifts = GetShiftsByUstf(prlDrg->Stfu, olShifts);
			for(int ilS = 0; ilS < ilNumShifts; ilS++)
			{
				prlShift = &olShifts[ilS];
				CString olTeam = ogBasicData.GetTeamForShift(prlShift);
				strcpy(prlShift->Egrp, olTeam);
				ogCCSDdx.DataChanged((void *)this,SHIFT_CHANGE,(void *)prlShift);
			}
		}
	}
}

void  CedaShiftData::ProcessShiftBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	SHIFTDATA *prlShift = NULL;
	struct BcStruct *prlShiftData = NULL;
	long llUrno = 0L;
	
	if(ipDDXType != SWG_INSERT && ipDDXType != SWG_UPDATE && ipDDXType != SWG_DELETE &&
		ipDDXType != DRG_INSERT && ipDDXType != DRG_CHANGE && ipDDXType != DRG_DELETE)
	{
		prlShiftData = (struct BcStruct *) vpDataPointer;
		ogBasicData.LogBroadcast(prlShiftData);
		llUrno = GetUrnoFromSelection(prlShiftData->Selection);
	}

	if(ipDDXType == BC_SHIFT_DELETE)
	{
		ProcessShiftDelete(llUrno);
	}
	else if(ipDDXType == BC_SHIFT_CHANGE || ipDDXType == BC_SHIFT_INSERT)
	{
		if(!ProcessShiftUpdate(llUrno, prlShiftData->Fields, prlShiftData->Data))
			ProcessShiftInsert(prlShiftData->Fields, prlShiftData->Data);
	}
	else if(ipDDXType == BC_SHIFT_SBC)
	{
		ProcessDienstPlanUpdate(prlShiftData);
	}
}

bool CedaShiftData::ProcessShiftUpdate(long lpShiftUrno, char *pcpFields, char *pcpData)
{
	bool blFound = false;
	SHIFTDATA *prlShift;
	if((prlShift = GetShiftByUrno(lpShiftUrno)) != NULL)
	{
		blFound = true;
		GetRecordFromItemList(prlShift, pcpFields, pcpData);
		ConvertDatesToLocal(prlShift);
		PrepareShiftData(prlShift);
		ogCCSDdx.DataChanged((void *)this,SHIFT_CHANGE,(void *)prlShift);
	}

	return blFound;
}

void CedaShiftData::ProcessShiftInsert(char *pcpFields, char *pcpData)
{
	SHIFTDATA *prlShift = new SHIFTDATA;
	GetRecordFromItemList(prlShift, pcpFields, pcpData);
	if(!strcmp(prlShift->Rosl,MITARBEITER_TAGESLISTE) && strcmp(prlShift->Ross,LAST_RECORD) &&
		//hag 20040816:  prlShift->Avfa >= omLoadShiftsFrom && prlShift->Avta <= omLoadShiftsTo)
		IsOverlapped(prlShift->Avfa, prlShift->Avta, omLoadShiftsFrom, omLoadShiftsTo) )
	{
		AddShift(prlShift);
		ogCCSDdx.DataChanged((void *)this,SHIFT_NEW,(void *)prlShift);
	}
	else
	{
		delete prlShift;
	}
}

void CedaShiftData::ProcessShiftDelete(long lpShiftUrno)
{
	SHIFTDATA *prlShift;
	if((prlShift = GetShiftByUrno(lpShiftUrno)) != NULL)
	{
		DeleteShiftInternal(prlShift);
		ogCCSDdx.DataChanged((void *)this,SHIFT_DELETE,(void *) lpShiftUrno);
	}
}

void CedaShiftData::ProcessDienstPlanUpdate(struct BcStruct *prpShiftData)
{
	CString olData = prpShiftData->Data;
	// data list contains a list of STF.URNOs whose shifts need to be reloaded
	// data list format "ReloadFrom-ReloadTo-CommaSeperatedListOfStfUrnos-OldROSL,NewROSL"

	//olData = "20000401-20000406-10527103�10527014-2�3";
	MakeClientString(olData);

		
	bool blUpdated = false;
	char pclFrom[50], pclTo[50], pclRosl[50];
	int ilDataLen = olData.GetLength();
	int ilStrLength = 0;
	char *pclStfUrnoList = new char[ilDataLen];
	memset(pclStfUrnoList,0,sizeof(pclStfUrnoList));
	//sscanf(olData,"%[^-]%[^-]%[^-]%[^,]%s",pclFrom,pclTo,pclStfUrnoList,pclOldRosl,pclNewRosl);

	int ilWordCount = 0, ilX = 0, ilC;
	char *pclStr = pclFrom;
	for(ilC = 0; ilC < ilDataLen; ilC++)
	{
		if(olData[ilC] == '-')
		{
			*pclStr = '\0';
			ilWordCount++;
			if(ilWordCount == 1)
				pclStr = pclTo;
			else if(ilWordCount == 2)
				pclStr = pclStfUrnoList;
			else
				pclStr = pclRosl;
			ilStrLength = 0;
		}
		else
		{
			if (ilStrLength<50)
			{
				*pclStr = olData[ilC];
			}
			pclStr++;
			ilStrLength++;
		}
	}
	
	*pclStr = '\0';

	CTime olTmp = omLoadShiftsFrom - CTimeSpan(1, 0, 0, 0);
	CString olLoadShiftsFrom = olTmp.Format("%Y%m%d");
	CString olLoadShiftsTo = omLoadShiftsTo.Format("%Y%m%d");
	CString olTo = pclTo;
	CString olFrom = pclFrom;

	if(strlen(pclFrom) == 8 && strlen(pclTo) == 8 && olTo >= olLoadShiftsFrom && olFrom <= olLoadShiftsTo)
	{
		if(!ProcessAttachment(prpShiftData->Attachment) && strlen(pclStfUrnoList) > 0)
		{
			CString olTmpStfUrnoList = CString(pclStfUrnoList);

			CStringArray olStfUrnos;
			ogBasicData.ExtractItemList(olTmpStfUrnoList, &olStfUrnos);

			CString olStfUrnoList = CString("'") + olTmpStfUrnoList + CString("'");
			olStfUrnoList.Replace(",","','");

			CStringArray olRoslList;
			ogBasicData.ExtractItemList(CString(pclRosl), &olRoslList);
			for(int ilR = (olRoslList.GetSize()-1); ilR >= 0; ilR--)
			{
				if(strcmp(olRoslList[ilR],MITARBEITER_TAGESLISTE) && strcmp(olRoslList[ilR],LANGZEITSTUFE))
				{
					olRoslList.RemoveAt(ilR);
				}
			}

			//		Staff released in Rostering for the 25th (shift time 18.00-03.00h) does not show up in the 
			//		already loaded OPSS-PM for the 26th 00.00-23.59 though the time span 00.00-03.00 on the 
			//		26th should be soon. When both days are loaded staff shows up correctly.  
			//		The problem occurs when OpssPm receives a RELDRR which contains a from-to timeframe which 
			//		must overlap with OpssPm's loaded timeframe. The from-to time is only SDAY - the ddmmyyy 
			//		of the start of the shift. This means in the above example OpssPm receives 25062003 (SDAY 
			//		of the shift start) which does not overlap with 26062003. This problem only happens when a 
			//		previous day is released in rostering, so the so the solution is to check OpssPmTimeframe - 1 day.  

			if(olRoslList.GetSize() > 0)
			{
				CDWordArray olDeletedUrnos;
				DeleteShiftsByUstfSdayAndRosl(olStfUrnos, olRoslList, olFrom, olTo, olDeletedUrnos);
				ReloadShifts(olStfUrnoList, olRoslList);
				int ilNumDeletedUrnos = olDeletedUrnos.GetSize();
				for(int ilDU = 0; ilDU < ilNumDeletedUrnos; ilDU++)
				{
					if(GetShiftByUrno(olDeletedUrnos[ilDU]) == NULL)
					{
						ogCCSDdx.DataChanged((void *)this,SHIFT_DELETE,(void *) olDeletedUrnos[ilDU]);
					}
				}
				blUpdated = true;
			}
		}
	}
	if(!blUpdated)
		ogBasicData.Trace("NOT Reloading Shifts ROSL=<%s> Expected ROSL=<3> DATES=<%s-%s> Expected Dates <%s-%s>\n",pclRosl, olFrom, olTo, olLoadShiftsFrom, olLoadShiftsTo);

	if(pclStfUrnoList != NULL)
		delete [] pclStfUrnoList;
}

bool CedaShiftData::ProcessAttachment(CString &ropAttachment)
{
	bool blSuccess = false;
	if(!ropAttachment.IsEmpty())
	{
		CAttachment olAttachment(ropAttachment);
		if(olAttachment.bmAttachmentValid)
		{
			blSuccess = true;
			for(int ilI = 0; ilI < olAttachment.omInsertDataList.GetSize(); ilI++)
			{
				ProcessShiftInsert(olAttachment.omFieldList.GetBuffer(0), olAttachment.omInsertDataList[ilI].GetBuffer(0));
			}

			for(int ilU = 0; ilU < olAttachment.omUpdateDataList.GetSize(); ilU++)
			{
				SHIFTDATA rlShift;
				GetRecordFromItemList(&omRecInfo,&rlShift, olAttachment.omFieldList.GetBuffer(0), olAttachment.omUpdateDataList[ilU].GetBuffer(0));
				if(!ProcessShiftUpdate(rlShift.Urno, olAttachment.omFieldList.GetBuffer(0), olAttachment.omUpdateDataList[ilU].GetBuffer(0)))
				{
					ProcessShiftInsert(olAttachment.omFieldList.GetBuffer(0), olAttachment.omUpdateDataList[ilU].GetBuffer(0));
				}
			}

			for(int ilD = 0; ilD < olAttachment.omDeleteDataList.GetSize(); ilD++)
			{
				ProcessShiftDelete(atol(olAttachment.omDeleteDataList[ilD]));
			}
		}
	}
	return blSuccess;
}

void CedaShiftData::DeleteShiftsByUstfSdayAndRosl(CStringArray &ropStfUrnos, CStringArray &ropRoslList, CString opFrom, CString opTo, CDWordArray &ropDeletedUrnos)
{
	CTimeSpan olOneDay = CTimeSpan(1, 0, 0, 0);
	CTime olStart = CTime(atoi(opFrom.Left(4)), atoi(opFrom.Mid(4,2)), atoi(opFrom.Right(2)),  0,  0,  0);
	CTime olEnd   = CTime(atoi(opTo.Left(4)), atoi(opTo.Mid(4,2)), atoi(opTo.Right(2)), 23, 59, 59);

	int ilNumStfUrnos = ropStfUrnos.GetSize();
	int ilNumRosl = ropRoslList.GetSize();

	int ilDrrn;
	CString olDrrn;
	SHIFTDATA *prlShift = NULL;
	while(olStart <= olEnd)
	{
		CString olSday = olStart.Format("%Y%m%d");
		for(int ilSU = 0; ilSU < ilNumStfUrnos; ilSU++)
		{
			for(int ilR = 0; ilR < ilNumRosl; ilR++)
			{
				ilDrrn = 1;
				olDrrn.Format("%d", ilDrrn);
				while((prlShift = GetShiftByUstfAndSdayAndRosl(atol(ropStfUrnos[ilSU]), olSday, ropRoslList[ilR], olDrrn)) != NULL)
				{
					ropDeletedUrnos.Add(prlShift->Urno);
					DeleteShiftInternal(prlShift);
					olDrrn.Format("%d", ++ilDrrn);
				}
			}
		}
		olStart += olOneDay;
	}
}

void CedaShiftData::ReloadShifts(const char *pcpStfUrnoList, CStringArray &ropRoslList)
{
    char *pclWhere = new char[strlen(pcpStfUrnoList)+500];
	CCSReturnCode ilRc = RCSuccess;

    // Select data from the database
	CString olRosl, olTmpRosl;
	for(int ilR = 0; ilR < ropRoslList.GetSize(); ilR++)
	{
		if(olRosl.IsEmpty())
		{
			olRosl.Format("AND (ROSL = '%s'", ropRoslList[ilR]);
		}
		else
		{
			olTmpRosl.Format(" OR ROSL = '%s'", ropRoslList[ilR]);
			olRosl += olTmpRosl;
		}
	}
	if(!olRosl.IsEmpty())
		olRosl += ")";

	ogBasicData.MarkTime();
    sprintf(pclWhere, "WHERE STFU IN (%s) %s AND AVTO >= '%s' AND AVFR <= '%s'",
			pcpStfUrnoList, olRosl, omLoadShiftsFrom.Format("%Y%m%d%H%M%S"),
			omLoadShiftsTo.Format("%Y%m%d%H%M%S"));

    if (CedaAction2("RT", pclWhere) == RCSuccess)
	{
		ogBasicData.SaveTimeTakenForReread("UPDDRR (shift update)", pclWhere);

		SHIFTDATA rlShift, *prlShift;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlShift)) == RCSuccess)
			{
				if (omUrnoMap.Lookup((void *)rlShift.Urno,(void *& )prlShift) == TRUE)
				{
					*prlShift = rlShift;
					//ogBasicData.Trace("Update Shift URNO <%ld>\n",prlShift->Urno);
					PrepareShiftData(prlShift);
					ogCCSDdx.DataChanged((void *)this,SHIFT_CHANGE,(void *)prlShift);
				}
				else
				{
					if(!strcmp(rlShift.Rosl,MITARBEITER_TAGESLISTE) && strcmp(rlShift.Ross,LAST_RECORD) &&
						rlShift.Avta >= omLoadShiftsFrom && rlShift.Avfa <= omLoadShiftsTo)
					{
						prlShift = new SHIFTDATA;
						*prlShift = rlShift;
						//ogBasicData.Trace("Insert Shift URNO <%ld>\n",prlShift->Urno);
						AddShift(prlShift);
						ogCCSDdx.DataChanged((void *)this,SHIFT_NEW,(void *)prlShift);
					}
				}
			}
		}
	}
	
	delete [] pclWhere;
}

SHIFTDATA  *CedaShiftData::GetShiftByUrno(long lpUrno)
{
	SHIFTDATA  *prlShift;

	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlShift) == TRUE)
	{
		//TRACE("GetShiftByUrno %ld \n",prlShift->Urno);
		return prlShift;
	}
	return NULL;
}

SHIFTDATA  *CedaShiftData::GetShiftByPeno(CString opPkno)
{
	SHIFTDATA  *prlShift;

	if (omPenoMap.Lookup(opPkno,(void *& )prlShift) == TRUE)
	{
		//TRACE("GetShiftByUrno %ld \n",prlShift->Urno);
		return prlShift;
	}
	return NULL;
}

bool CedaShiftData::IsAbsent(SHIFTDATA *prpShift)
{
	return (*prpShift->Ctyp == 'A') ? true : false;
}


CString CedaShiftData::GetTableName(void)
{
	return CString(pcmTableName);
}


// compare prpShift with prpOldShift and return only the fields (pcpFieldList) and data (pcpData) which have changed
bool CedaShiftData::GetChangedFields(SHIFTDATA *prpShift, SHIFTDATA *prpOldShift, char *pcpFieldList, char *pcpData)
{
	bool blChangesFound = false;

	memset(pcpFieldList,0,sizeof(pcpFieldList));
	memset(pcpData,0,sizeof(pcpData));

	if(prpShift != NULL && prpOldShift != NULL)
	{
		SHIFTDATA rlShift;
		CString olData;

		memcpy(&rlShift,prpShift,sizeof(SHIFTDATA));
		MakeCedaData(&omRecInfo,olData,&rlShift);
		CStringArray olNewFieldArray, olNewDataArray;
		ogBasicData.ExtractItemList(pcmFieldList, &olNewFieldArray);
		ogBasicData.ExtractItemList(olData, &olNewDataArray);
		int ilNumNewFields = olNewFieldArray.GetSize();
		int ilNumNewData = olNewDataArray.GetSize();

		memcpy(&rlShift,prpOldShift,sizeof(SHIFTDATA));
		MakeCedaData(&omRecInfo,olData,&rlShift);
		CStringArray olOldDataArray;
		ogBasicData.ExtractItemList(olData, &olOldDataArray);
		int ilNumOldData = olOldDataArray.GetSize();

		bool blListsAreEmpty = true;
		if(ilNumNewFields == ilNumNewData && ilNumNewData == ilNumOldData)
		{
			for(int ilD = 0; ilD < ilNumNewData; ilD++)
			{
				// check if the data item has changed - always write LSTU and USEU whether they change or not
				if(olNewDataArray[ilD] != olOldDataArray[ilD] && 
					olNewFieldArray[ilD] != "CDAT" && olNewFieldArray[ilD] != "USEC" &&
					olNewFieldArray[ilD] != "LSTU" && olNewFieldArray[ilD] != "USEU")
				{
					CString olOldData = olOldDataArray[ilD];
					CString olNewData = olNewDataArray[ilD];
					if(blListsAreEmpty)
					{
						strcpy(pcpFieldList,olNewFieldArray[ilD]);
						strcpy(pcpData,olNewDataArray[ilD]);
						blListsAreEmpty = false;
					}
					else
					{
						CString olTmp;
						olTmp.Format(",%s",olNewFieldArray[ilD]);
						strcat(pcpFieldList,olTmp);
						olTmp.Format(",%s",olNewDataArray[ilD]);
						strcat(pcpData,olTmp);
					}

					blChangesFound = true;
				}
			}
		}
	}

	return blChangesFound;
}

void CedaShiftData::ConvertDatesToUtc(SHIFTDATA *prpShift)
{
	if(prpShift != NULL)
	{
		ogBasicData.ConvertDateToUtc(prpShift->Cdat);
		ogBasicData.ConvertDateToUtc(prpShift->Lstu);
	}
}

void CedaShiftData::ConvertDatesToLocal(SHIFTDATA *prpShift)
{
	if(prpShift != NULL)
	{
		ogBasicData.ConvertDateToLocal(prpShift->Cdat);
		ogBasicData.ConvertDateToLocal(prpShift->Lstu);
	}
}

int CedaShiftData::GetMaxFieldLength(const CString &ropField)
{
	return CedaObject::GetMaxFieldLength("DRR", ropField);
}
