// BcHandle.h : header file
//

/////////////////////////////////////////////////////////////////////////////
#ifndef _BCHANDLE
#define _BCHANDLE

#include <CCSCedaData.h>
/*
extern enum enumDDXTypes {NOTFOUND,BC_FLIGHT_CHANGE,
	BC_DEMAND_CHANGE,BC_DEMAND_DELETE,
	BC_JOB_CHANGE,BC_JOB_DELETE,
	BC_JOD_CHANGE,BC_JOD_DELETE} egDDXTypes;
*/

extern struct BcStruct
	{
		char ReqId[24];
		char Dest1[24];
		char Dest2[24];
		char Cmd[24];
		char Object[24];
		char Seq[240];
		char Tws[24];
		char Twe[24];
		char Selection[1024];
		char Fields[1240];
		char Data[2400];
		char BcNum[24];
	} rlBcData;


class BcHandle: public CCSCedaData
{
public:
	BcHandle(void);           

	void GetBc(void);
	CCSReturnCode ReReadBc(long *llBcNum,long llActualBcNum);
	CCSReturnCode DistributeBc(struct BcStruct &rlBcData);

public:
	char pcmReqId[24];
	int imReqIdLen;
};


extern BcHandle ogBcHandle;

/////////////////////////////////////////////////////////////////////////////
#endif
