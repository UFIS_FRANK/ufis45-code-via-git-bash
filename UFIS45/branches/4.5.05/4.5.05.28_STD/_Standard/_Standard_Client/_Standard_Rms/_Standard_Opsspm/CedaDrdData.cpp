// CedaDrdData.cpp - Class for shift deviations - these can have their own funtions and permits
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaDrdData.h>
#include <BasicData.h>
#include <Resource.h>

extern CCSDdx ogCCSDdx;
void  ProcessDrdCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaDrdData::CedaDrdData()
{                  
    BEGIN_CEDARECINFO(DRDDATA, DrdDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Fctc,"FCTC")
		FIELD_CHAR_TRIM(Prmc,"PRMC")
		FIELD_LONG(Stfu,"STFU")
		FIELD_CHAR_TRIM(Sday,"SDAY")
		FIELD_CHAR_TRIM(Drrn,"DRRN")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(DrdDataRecInfo)/sizeof(DrdDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&DrdDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"DRDTAB");
    pcmFieldList = "URNO,FCTC,PRMC,STFU,SDAY,DRRN";

	ogCCSDdx.Register((void *)this,BC_DRD_CHANGE,CString("DRDDATA"), CString("Drd changed"),ProcessDrdCf);
	ogCCSDdx.Register((void *)this,BC_DRD_DELETE,CString("DRDDATA"), CString("Drd deleted"),ProcessDrdCf);
}
 
CedaDrdData::~CedaDrdData()
{
	TRACE("CedaDrdData::~CedaDrdData called\n");
	ClearAll();
	ogCCSDdx.UnRegister(this,NOTUSED);
}


void  ProcessDrdCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaDrdData *)popInstance)->ProcessDrdBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void  CedaDrdData::ProcessDrdBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlDrdData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlDrdData);
	long llUrno = GetUrnoFromSelection(prlDrdData->Selection);
	DRDDATA *prlDrd = GetDrdByUrno(llUrno);
	//ogBasicData.Trace("DRDTAB BC Cmd <%s>\nFields <%s>\nData <%s>\nSelection <%s>\n",prlDrdData->Cmd,prlDrdData->Fields,prlDrdData->Data,prlDrdData->Selection);

	if(ipDDXType == BC_DRD_CHANGE && prlDrd != NULL)
	{
		// update
		//ogBasicData.Trace("BC_DRD_CHANGE - UPDATE DRDTAB URNO %ld\n",prlDrd->Urno);
		GetRecordFromItemList(prlDrd,prlDrdData->Fields,prlDrdData->Data);
	}
	else if(ipDDXType == BC_DRD_CHANGE && prlDrd == NULL)
	{
		// insert
		DRDDATA rlDrd;
		GetRecordFromItemList(&rlDrd,prlDrdData->Fields,prlDrdData->Data);
		//ogBasicData.Trace("BC_DRD_CHANGE - INSERT DRDTAB URNO %ld\n",rlDrd.Urno);
		AddDrdInternal(rlDrd);
	}
	else if(ipDDXType == BC_DRD_DELETE && prlDrd != NULL)
	{
		// delete
		//ogBasicData.Trace("BC_DRD_DELETE - DELETE DRDTAB URNO %ld\n",prlDrd->Urno);
		DeleteDrdInternal(prlDrd->Urno);
	}

}


void CedaDrdData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omData.DeleteAll();

	CMapPtrToPtr *polDrdMap;
	long llUrno;
	for(POSITION rlPos = omStfuMap.GetStartPosition(); rlPos != NULL; )
	{
		omStfuMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& ) polDrdMap);
		polDrdMap->RemoveAll();
		delete polDrdMap;
	}
	omStfuMap.RemoveAll();
}

bool CedaDrdData::ReadDrdData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512] = "";
    sprintf(pclWhere,"WHERE SDAY BETWEEN '%s' AND '%s'", ogBasicData.omShiftFirstSDAY.Format("%Y%m%d"), 
														 ogBasicData.omShiftLastSDAY.Format("%Y%m%d"));
	if ((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaDrdData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		DRDDATA rlDrdData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlDrdData)) == RCSuccess)
			{
				AddDrdInternal(rlDrdData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaDrdData Read %d Records (Rec Size %d) %s",omData.GetSize(),sizeof(DRDDATA), pclWhere);
    return ilRc;
}

CCSReturnCode CedaDrdData::DeleteDrdRecord(long lpUrno)
{
	CCSReturnCode olRc = RCSuccess;

	char pclFieldList[500], pclData[1000];
	char pclCommand[10] = "DRT";
	char pclSelection[100];
	sprintf(pclSelection," WHERE URNO = '%ld%'",lpUrno);
	if((olRc = CedaAction(pclCommand, pcmTableName, pclFieldList, pclSelection, "", pclData)) == RCSuccess)
	{
		DeleteDrdInternal(lpUrno);
		ogCCSDdx.DataChanged((void *)this,DRD_DELETE,(void *)lpUrno);
	}

	return olRc;
}

void CedaDrdData::AddDrdInternal(DRDDATA &rrpDrd)
{
	DRDDATA *prlDrd = new DRDDATA;
	*prlDrd = rrpDrd;
	omData.Add(prlDrd);
	omUrnoMap.SetAt((void *)prlDrd->Urno,prlDrd);

	if(rrpDrd.Stfu != 0L)
	{
		CMapPtrToPtr *polDrdMap;
		if(omStfuMap.Lookup((void *) rrpDrd.Stfu, (void *&) polDrdMap) == TRUE)
		{
			polDrdMap->SetAt((void *)rrpDrd.Urno,&rrpDrd);
		}
		else
		{
			polDrdMap = new CMapPtrToPtr;
			polDrdMap->SetAt((void *)rrpDrd.Urno,&rrpDrd);
			omStfuMap.SetAt((void *) rrpDrd.Stfu,polDrdMap);
		}
	}
}

void CedaDrdData::DeleteDrdInternal(long lpUrno)
{
	int ilNumDrds = omData.GetSize();
	for(int ilDel = (ilNumDrds-1); ilDel >= 0; ilDel--)
	{
		if(omData[ilDel].Urno == lpUrno)
		{
			omData.DeleteAt(ilDel);
		}
	}
	omUrnoMap.RemoveKey((void *)lpUrno);
}

DRDDATA* CedaDrdData::GetDrdByUrno(long lpUrno)
{
	DRDDATA *prlDrd = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlDrd);
	return prlDrd;
}

CString CedaDrdData::GetFunctionsAndPermitsByUrno(long lpUrno, CStringArray &ropPermits)
{
	CString olFunction;
	ropPermits.RemoveAll();
	DRDDATA *prlDrd = GetDrdByUrno(lpUrno);
	if(prlDrd != NULL)
	{
		olFunction = prlDrd->Fctc;
		CString olPermitList = prlDrd->Prmc;
		ogBasicData.ExtractItemList(olPermitList, &ropPermits,'|');
	}

	return olFunction;
}

CString CedaDrdData::GetFunctionsByUrno(long lpUrno)
{
	CString olFunction;
	DRDDATA *prlDrd = GetDrdByUrno(lpUrno);
	if(prlDrd != NULL)
	{
		olFunction = prlDrd->Fctc;
	}

	return olFunction;
}

void CedaDrdData::GetPermitsByUrno(long lpUrno, CStringArray &ropPermits)
{
	ropPermits.RemoveAll();
	DRDDATA *prlDrd = GetDrdByUrno(lpUrno);
	if(prlDrd != NULL)
	{
		CString olPermitList = prlDrd->Prmc;
		ogBasicData.ExtractItemList(olPermitList, &ropPermits,'|');
	}
}

CString CedaDrdData::GetTableName(void)
{
	return CString(pcmTableName);
}

DRDDATA *CedaDrdData::GetDrdByShift(long lpStfu, char *pcpSday, char *pcpDrrn)
{
	DRDDATA *prlDrd = NULL;

	CMapPtrToPtr *polDrdMap;

	if(omStfuMap.Lookup((void *)lpStfu,(void *& )polDrdMap) == TRUE)
	{
		for(POSITION rlPos = polDrdMap->GetStartPosition(); rlPos != NULL; )
		{
			DRDDATA *prlTmpDrd = NULL;
			long llUrno;
			polDrdMap->GetNextAssoc(rlPos,(void *&) llUrno, (void *& )prlTmpDrd);
			if(!strcmp(prlTmpDrd->Sday, pcpSday) && !strcmp(prlTmpDrd->Drrn, pcpDrrn))
			{
				prlDrd = prlTmpDrd;
				break;
			}
		}
	}

	return prlDrd;
}