#if !defined(__AVAILABLEEQUIPMENTDLG__)
#define __AVAILABLEEQUIPMENTDLG__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AvailableEquipmentDlg.h : header file
//
#include <TScale.h>

#include <CedaDemandData.h>
#include <CedaEquData.h>
#include <CedaBlkData.h>
#include <CedaEmpData.h>
#include <CedaJobData.h>
#include <AvailableEquipmentViewer.h>
#include <NewAssignConflictViewer.h>
#include <NewAssignConflictGantt.h>

/////////////////////////////////////////////////////////////////////////////
// CAvailableEquipmentDlg dialog


class CAvailableEquipmentDlg : public CDialog
{
// Construction
public:
	CAvailableEquipmentDlg(CWnd* pParent = NULL);   // standard constructor
	~CAvailableEquipmentDlg();

// Dialog Data
	//{{AFX_DATA(CAvailableEquipmentDlg)
	enum { IDD = IDD_AVAILABLEEMPSDLG };
	CButton	m_TeamAllocCtrl;
	CButton	m_DisplayOnlyEquWithCorrectFunctionsAndPermitsCtrl;
	CStatic	m_StatusCtrl;
	BOOL	m_DisplayOnlyEquWithCorrectFunctionsAndPermits;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAvailableEquipmentDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAvailableEquipmentDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void UpdateView();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnFilterfuncpermits();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	DEMANDDATA *prmSelectedDemand;
	CCSPtrArray <DEMANDDATA> omDemands; // demands indicated by the calling environment
	CDWordArray omSelectedEquUrnos; // the resulting employees to be assigned to the demand
	bool bmTeamAlloc;

private:
	// don't need these but are created because they are required in the Gantt/Viewer
	CCSPtrArray <JOBDATA> omJobs;
	CCSPtrArray <JODDATA> omJods;
	CCSPtrArray <JOBDATA> omNoDemandJobs;

	CCSPtrArray <AVAILABLE_EQU_LINEDATA> omEquipment; // created dynamically as single demands are selected
	void AddEquipment(EQUDATA *prpEqu);

	// The allocation unit is required when selecting pool jobs for pools with restrictions e.g. certain gates may only be 
	// assigned to certain pools. One or both of these are optional e.g. Allocation Unit may be empty when the flight has no gate.
	CString omAllocUnitType, omAllocUnit;

	CTable *pomEquTable;
	AvailableEquipmentViewer *pomEquViewer;
	CFGDATA* pomCfgData;
	CString omLastSavedViewName;

	CString omCaptionObject; // caption requires an object type e.g. flight name, registration or single demand
	void UpdateComboBox();

    CTimeScale omTimeScale;
    int imStartTimeScalePos;
    int imBottomPos;
    CTime omTSStartTime;
    CTimeSpan omTSDuration;
    CTimeSpan omTSInterval;
	NewAssignConflictViewer omDemandsViewer;
	NewAssignConflictGantt omDemandsGantt;

	void InitDemandsGantt(void);
	void InitEquipmentTable(void);
	void InitSelectViewComboBox(void);

public:
	void ProcessSelectDemand(DEMANDDATA *prpDemand);
	void AddDemand(DEMANDDATA *prpDemand);
	void SetAllocUnit(const char *pcpAllocUnitType, const char *pcpAllocUnit);
	void SetCaptionObject(CString opCaptionObject);
	bool SelectDemand(DEMANDDATA *prpDemand);
	int  GetAvailableEquipmentForDemand(DEMANDDATA *prpDemand, bool bpOnlyTestHowManyAvailable = false);

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(__AVAILABLEEQUIPMENTDLG__)
