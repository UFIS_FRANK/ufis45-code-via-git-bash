#if !defined(AFX_CONFIGTEXTDLG_H__D6067EE3_F097_11D6_81CD_00010215BFE5__INCLUDED_)
#define AFX_CONFIGTEXTDLG_H__D6067EE3_F097_11D6_81CD_00010215BFE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ConfigTextDlg.h : header file
//
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// CConfigTextDlg dialog
struct TextField
{
	CString Text;
	CString Key;
};

typedef TextField FIELD;

class CConfigTextDlg : public CDialog
{
// Construction
public:
	CConfigTextDlg(CWnd* pParent = NULL);   // standard constructor
	~CConfigTextDlg(void);

// Dialog Data
	//{{AFX_DATA(CConfigTextDlg)
	enum { IDD = IDD_CONFIGTEXT_DLG };
	CComboBox	m_FieldCombo;
	CRichEditCtrl	m_ResultsList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CConfigTextDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CConfigTextDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnMaxtextEdt1();
	afx_msg void OnStat1();
	afx_msg void OnUpdateEdt1();
	afx_msg void OnReset();
	afx_msg void OnAddField();
	virtual void OnOK();
	afx_msg void OnProtectedResultsList(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CHARFORMAT rmFieldFormat;
	void SetField(CString opText);
	void DeleteFields(void);
	bool bmDeleteOperation;
	bool IsField(long lpFrom, long lpTo);
	void CreateKeylist(void);

	CCSPtrArray <FIELD> omFields;
	CString omResult;
	CStringArray omKeys;

public:
	void SetTitle(CString opTitle);
	void AddField(CString opText, CString opKey);
	void GetResult(CString &ropResult, CStringArray &ropKeys);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONFIGTEXTDLG_H__D6067EE3_F097_11D6_81CD_00010215BFE5__INCLUDED_)
