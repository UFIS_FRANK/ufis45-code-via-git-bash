#if !defined(AFX_CICAUTOASSIGNDLG_H__93E826D3_120C_11D5_80E2_00010215BFDE__INCLUDED_)
#define AFX_CICAUTOASSIGNDLG_H__93E826D3_120C_11D5_80E2_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// cicAutoAssignDlg.h : header file
//
#include <ccsptrarray.h>

typedef struct DemandDataStruct DEMANDDATA;
typedef struct FlightDataStruct FLIGHTDATA;
typedef struct CicDataStruct	CICDATA;
struct CCI_LINEDATA;

class CciDiagramViewer;

/////////////////////////////////////////////////////////////////////////////
// CicAutoAssignDlg dialog

class CicAutoAssignDlg : public CDialog
{
// Construction
public:
	CicAutoAssignDlg(CciDiagramViewer *popViewer,CWnd* pParent = NULL);   // standard constructor
	void SetData(CTime opAssignStart,CTime opAssignEnd,CDWordArray& ropSelectedDemandUrnos);

// Dialog Data
	//{{AFX_DATA(CicAutoAssignDlg)
	enum { IDD = IDD_CIC_AUTOASSIGN };
	CListBox	m_CounterGroupsToList;
	CListBox	m_CounterGroupsFromList;
	CListBox	m_CounterClassesToList;
	CListBox	m_CounterClassesFromList;
	BOOL	m_AssignWithoutLink;
	BOOL	m_UseCommonCheckin;
	BOOL	m_UseFIDCheckin;
	BOOL	m_UseDedicatedCheckin;
	BOOL	m_UseAllocatedOnly;
	CString	m_FromDay;
	CString	m_FromTime;
	CString	m_ToDay;
	CString	m_ToTime;
	CString	m_FlightDest;
	CString	m_FlightNumber;
	CString	m_FlightSuffix;
	int		m_CurrentSelection;
	CString	m_FlightAirline;
	int		m_ParametersRadioButton;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CicAutoAssignDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CicAutoAssignDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnRadioUseAll();
	afx_msg void OnRadioUseSelection();
	afx_msg void OnButtonCounterClassesAdd();
	afx_msg void OnButtonCounterClassesRemove();
	afx_msg void OnButtonCounterGroupAdd();
	afx_msg void OnButtonCounterGroupRemove();
	afx_msg void OnButtonDeleteAllAssigns();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	bool		IsPassFilter(const CMapPtrToPtr& ropClassMap,const CMapPtrToPtr& ropGroupMap,DEMANDDATA *prpDemand);
	bool		IsPassFilter(DEMANDDATA *prpDemand);
	bool		IsPassFilter(FLIGHTDATA *prpFlight);
	void		EnableSelection(BOOL bpEnable);
	void		Filter(const CMapPtrToPtr& ropClassMap,const CMapPtrToPtr& ropGroupMap,CCSPtrArray<DEMANDDATA>& ropDemandList);
	void		FilterByLocation(CCSPtrArray<DEMANDDATA>& ropDemandList,CCSPtrArray<DEMANDDATA>& ropDemandWithLocationList,CCSPtrArray<DEMANDDATA>& ropDemandWithoutLocationList);
	int			AssignDemands(CCSPtrArray<DEMANDDATA>& ropDemandWithLocationList,CDWordArray& ropGroupCounters,const CMapPtrToPtr& ropGroupMap);
	int			AssignCcaDemands(CCSPtrArray<DEMANDDATA>& ropDemandWithLocationList,CDWordArray& ropGroupCounters,const CMapPtrToPtr& ropGroupMap);
	int			AssignNonCcaDemands(CCSPtrArray<DEMANDDATA>& ropDemandWithLocationList,CDWordArray& ropGroupCounters,const CMapPtrToPtr& ropGroupMap);

	BOOL		AssignDemandToLocation(DEMANDDATA *prlDem,long lpCounterUrno);
	void		GetSortedCicList(CDWordArray& ropCicUrnoList,CCSPtrArray<CICDATA>& ropCicList);
	CCI_LINEDATA *FindBestFit(DEMANDDATA *prpDem,CCSPtrArray<CICDATA>& ropCicList);
	CCI_LINEDATA *FindBestCcaFit(DEMANDDATA *prpDem,CCSPtrArray<CICDATA>& ropCicList);
private:
	CciDiagramViewer*	pomViewer;
	CTime				omAssignStart;
	CTime				omAssignEnd;
	bool				bmUseBestFit;
	CDWordArray			omSelectedDemandUrnos;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.
 
#endif // !defined(AFX_CICAUTOASSIGNDLG_H__93E826D3_120C_11D5_80E2_00010215BFDE__INCLUDED_)
