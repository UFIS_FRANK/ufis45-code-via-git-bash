#if !defined(AFX_ALLOCDLG_H__325BC283_35FD_11D4_93C7_0050DAE32E69__INCLUDED_)
#define AFX_ALLOCDLG_H__325BC283_35FD_11D4_93C7_0050DAE32E69__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AllocDlg.h : header file
//
#include <CedaFlightData.h>
#include <CedaJobData.h>
#include <CCSPtrArray.h>
#include <CedaDemandData.h>

/////////////////////////////////////////////////////////////////////////////
// CAllocDlg dialog
#define ALLOCDLG_ALLOCUNITTYPE "AllocDlgAllocUnitType"

typedef struct
{
	long Urno;
	int ReturnFlightType;
} ALLOCDLG_FLIGHTDATA;

class CAllocDlg : public CDialog
{
// Construction
public:
	CAllocDlg(CWnd* pParent = NULL);   // standard constructor
	~CAllocDlg();

// Dialog Data
	//{{AFX_DATA(CAllocDlg)
	enum { IDD = IDD_ALLOC_DLG };
	CEdit	m_DescriptionCtrl;
	CListBox	m_EmpList;
	CListBox	m_ResultsList;
	CString	m_FlightCarrier;
	CString	m_FlightNum;
	CString	m_FlightSuffix;
	int		m_AllocUnitRadioButtons;
	CString	m_Description;
	CTime		m_EndDate;
	CTime		m_EndTime;
	CTime		m_StartDate;
	CTime		m_StartTime;
	CString	m_Pool;
	CComboBox	m_AllocUnitList;
	BOOL	m_SelectTemplate;
	BOOL    m_CheckT2; //Singapore
	BOOL    m_CheckT3; //Singapore
	//}}AFX_DATA


	int imTopOfCciDeskRadioButton;
	CRect omDescriptionRect;
	bool bmLoadServicesForOtherJobs;
	bool bmValidateData;

	void SetEditControlFields(); 
	void OnChangeFlightFields();
	void UpdateListField(CString &ropText);
	void UpdateFlightList(CString opCarrier,CString opFlightNum,CString opSuffix);
	void AddPoolJobUrno(long lpPoolJobUrno);
	void SetEmpList(void);
	void SetResultsList(int ipStatus);
	int AddFlightToResultsList(FLIGHTDATA *prpFlight, int ipReturnFlightType = ID_NOT_RETURNFLIGHT);

	CRect omLargeRect, omSmallRect;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAllocDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAllocDlg)
	virtual BOOL OnInitDialog();
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	afx_msg void OnChangeDescription();
	afx_msg void OnChangeFlightcarrier();
	afx_msg void OnChangeFlightnum();
	afx_msg void OnChangeFlightsuffix();
	virtual void OnOK();
	afx_msg void OnDblclkResultsList();
	afx_msg void OnSelchangeResultsList();
	afx_msg void OnSelchangeAllocUnitList();
	afx_msg void OnCommandRange(UINT);
	afx_msg void OnKillfocusStartdate();
	afx_msg void OnKillfocusStarttime();
	afx_msg void OnKillfocusEnddate();
	afx_msg void OnKillfocusEndtime();
	afx_msg void OnCheckT2();
	afx_msg void OnCheckT3();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	CString omPool;
	CTime omStartTime, omEndTime;
	bool bmTeamAlloc; // true if a team is being allocated
	CMapPtrToPtr omEmpList;

private:
	int imRadioButtonID, imPrevRadioButtonID;
	CArray <long,long> omItemData; //Singapore
	CStringArray omAllocUnits;
	CCSPtrArray <FLIGHTDATA> omFlights;
	CCSPtrArray <ALLOCDLG_FLIGHTDATA> omFlightData;
	CCSPtrArray <JOBDATA> omPoolJobs;
	FLIGHTDATA *GetFlightByRegnAndTime(CString opRegn,CTime opStartTime,CTime opEndTime);
	CString GetSelectedText();
	CString omAllocUnitType;
	CCSPtrArray<DEMANDDATA> omDemands;
	void UpdateEndDateForOtherJob(void);
	void SetStartDate(void);
	void UpdateDateForFlightIndep(void);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ALLOCDLG_H__325BC283_35FD_11D4_93C7_0050DAE32E69__INCLUDED_)
