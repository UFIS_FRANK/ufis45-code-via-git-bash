// FunctionCodeSelectionDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <FunctionCodeSelectionDlg.h>
#include <CedaPfcData.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFunctionCodeSelectionDlg dialog


CFunctionCodeSelectionDlg::CFunctionCodeSelectionDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFunctionCodeSelectionDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFunctionCodeSelectionDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CFunctionCodeSelectionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFunctionCodeSelectionDlg)
	DDX_Control(pDX, IDC_FUNCTIONLIST, m_FunctionList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFunctionCodeSelectionDlg, CDialog)
	//{{AFX_MSG_MAP(CFunctionCodeSelectionDlg)
	ON_LBN_DBLCLK(IDC_FUNCTIONLIST, OnDblclkFunctionlist)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFunctionCodeSelectionDlg message handlers

void CFunctionCodeSelectionDlg::OnOK() 
{
	int ilIndex = m_FunctionList.GetCurSel();
	if(ilIndex < 0)
	{
		MessageBox(GetString(IDS_PLEASESELECTFUNC),"",MB_ICONSTOP);
	}
	else
	{
		DWORD llPfcUrno = m_FunctionList.GetItemData(ilIndex);
		PFCDATA *prlPfc = ogPfcData.GetPfcByUrno(llPfcUrno);
		if(prlPfc == NULL)
		{
			MessageBox("Invalid PFC Urno, please contact your system administrator!","Internal Error",MB_ICONSTOP);
		}
		else
		{
			omSelectedFunctionCode = prlPfc->Fctc;
			CDialog::OnOK();
		}
	}
}

BOOL CFunctionCodeSelectionDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CString olText;
	int ilPfcCnt = ogPfcData.omData.GetSize();
	for(int ilPfc = 0; ilPfc < ilPfcCnt; ilPfc++)
	{
		PFCDATA *prlPfc = &ogPfcData.omData[ilPfc];
		olText.Format("%s  -  %s",prlPfc->Fctc, prlPfc->Fctn);
		int ilIndex = m_FunctionList.AddString(olText);
		m_FunctionList.SetItemData(ilIndex, (DWORD) prlPfc->Urno);
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CFunctionCodeSelectionDlg::OnDblclkFunctionlist() 
{
	OnOK();
}
