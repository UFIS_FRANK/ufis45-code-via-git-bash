// Class for TimeFrameList
#ifndef _TIMEFRAMELIST_H_
#define _TIMEFRAMELIST_H_

#include <CCSPtrArray.h>

#ifndef	TIMENULL
#	define	TIMENULL	CTime((time_t)-1)
#endif

/////////////////////////////////////////////////////////////////////////////
// Class declaration

class TimeFrameList
{
public:
	enum State
	{
		NEW,
		CHANGED,
		UNCHANGED,
		DELETED,
	};

	typedef struct	TimeFrame	{
		CTime omStart;
		CTime omEnd;	
		State emState;	
		TimeFrame() { omStart = TIMENULL; omEnd = TIMENULL; emState = NEW;}
		TimeFrame(const CTime& opStart,const CTime& opEnd) { omStart = opStart; omEnd = opEnd; emState = NEW;}
	}	TimeFrame;

// Construction && Destruction
public:
	TimeFrameList();
	TimeFrameList(const CTime& opStartTime,const CTime& opEndTime);
	~TimeFrameList();

// Operations
public:
	BOOL Union(const CTime& opStartTime,const CTime& opEndTime);
	BOOL Subtract(const CTime& opStartTime,const CTime& opEndTime);
			
// Attributes
public:
	int		TimeFrames() const;
	BOOL	GetTimeFrame(int ipInd,CTime& opStartTime,CTime& opEndTime);
	
// Helpers
private:
			BOOL	Purge();
	static	int		ByStartTime(const TimeFrame **pppFrame1, const TimeFrame **pppFrame2);

// Implementation data
private:
	CCSPtrArray<TimeFrame>	omTimeList;	
};


#endif _TIMEFRAMELIST_H_
