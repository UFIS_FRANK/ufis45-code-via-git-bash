// ThirdPartFilterDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <ThirdPartyFilterDlg.h>
#include <BasicData.h>
#include <CedaFlightData.h>
#include <CedaDemandData.h>
#include <CedaAcrData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CThirdPartyFilterDlg dialog


CThirdPartyFilterDlg::CThirdPartyFilterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CThirdPartyFilterDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CThirdPartyFilterDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CThirdPartyFilterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CThirdPartyFilterDlg)
	DDX_Control(pDX, IDC_SELECTEDFLIGHTS, m_SelectedFlights);
	DDX_Control(pDX, IDC_POSSIBLEFLIGHTS, m_PossibleFlights);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CThirdPartyFilterDlg, CDialog)
	//{{AFX_MSG_MAP(CThirdPartyFilterDlg)
	ON_BN_CLICKED(IDC_REMOVE, OnRemoveFlight)
	ON_BN_CLICKED(IDC_ADD, OnAddFlight)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CThirdPartyFilterDlg message handlers

BOOL CThirdPartyFilterDlg::OnInitDialog() 
{

	CDialog::OnInitDialog();

	SetWindowText(GetString(IDS_THIRDPARTYTITLE));
	CWnd *polWnd = GetDlgItem(IDCANCEL);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_THIRDPARTYCANCEL));
	}

	int ilNumFlights = ogFlightData.omData.GetSize();
	for(int ilFlight = 0; ilFlight < ilNumFlights; ilFlight++)
	{
		FLIGHTDATA *prlFlight = &ogFlightData.omData[ilFlight];

		if(strcmp(prlFlight->Alc2,"AZ"))
		//		if(strcmp(prlFlight->Alc2,"AZ") && prlFlight->Tifa >= ogBasicData.GetTimeframeStart() && prlFlight->Tifa <= ogBasicData.GetTimeframeEnd())
		{
			// create a list of third party flights (ALC2 <> "AZ") that don't
			// already have demands - select demands both by flight URNO and by
			// registration (some registration demands my not have a flight URNO)
			CCSPtrArray <DEMANDDATA> olDemands;
			ogDemandData.GetDemandsByFlur(olDemands,prlFlight->Urno);
			if(olDemands.GetSize() == 0)
			{
				ACRDATA *prlAcr = ogAcrData.GetAcrByName(prlFlight->Regn);
				if(prlAcr != NULL)
				{
					ogDemandData.GetDemandsByUref(olDemands,prlAcr->Urno, ACRURNO);
				}
			}
			if(olDemands.GetSize() == 0)
			{
				FLIGHTDATA *prlRotation = ogFlightData.GetRotationFlight(prlFlight);
				if(prlRotation != NULL)
				{
					ogDemandData.GetDemandsByFlur(olDemands,prlRotation->Urno);
					if(olDemands.GetSize() == 0)
					{
						ACRDATA *prlAcr = ogAcrData.GetAcrByName(prlRotation->Regn);
						if(prlAcr != NULL)
						{
							ogDemandData.GetDemandsByUref(olDemands,prlAcr->Urno, ACRURNO);
						}
					}
				}
			}

			if(olDemands.GetSize() == 0)
			{
				CTime olTime = ogFlightData.GetFlightTime(prlFlight);
				CString olText;
				olText.Format("%s %s",ogFlightData.MakeFnum(prlFlight),olTime != TIMENULL ? olTime.Format("%d/%H%M") : "");

				if(omSelectedFlightUrnos.Lookup((void *) prlFlight->Urno, pvmDummy))
				{
					int ilIndex = m_SelectedFlights.AddString(olText);
					m_SelectedFlights.SetItemData(ilIndex,prlFlight->Urno);
				}
				else
				{
					int ilIndex = m_PossibleFlights.AddString(olText);
					m_PossibleFlights.SetItemData(ilIndex,prlFlight->Urno);
				}
			}
		}
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CThirdPartyFilterDlg::OnAddFlight() 
{
	CString olText;
	DWORD llFlightUrno;
	int ilIndex;

	// Move selected items from left list box to right list box
	for (int ilLc = m_PossibleFlights.GetCount()-1; ilLc >= 0 ; ilLc--)
	{
		if (!m_PossibleFlights.GetSel(ilLc))
			continue;
		m_PossibleFlights.GetText(ilLc, olText);
		llFlightUrno = m_PossibleFlights.GetItemData(ilLc);
		ilIndex = m_SelectedFlights.AddString(olText);
		m_SelectedFlights.SetItemData(ilIndex,llFlightUrno);
		m_PossibleFlights.DeleteString(ilLc);
	}
}


void CThirdPartyFilterDlg::OnRemoveFlight() 
{
	CString olText;
	DWORD llFlightUrno;
	int ilIndex;

	for (int ilLc = m_SelectedFlights.GetCount()-1; ilLc >= 0 ; ilLc--)
	{
		if (!m_SelectedFlights.GetSel(ilLc))
			continue;
		llFlightUrno = m_SelectedFlights.GetItemData(ilLc);
		m_SelectedFlights.GetText(ilLc, olText);
		ilIndex = m_PossibleFlights.AddString(olText);
		m_PossibleFlights.SetItemData(ilIndex,llFlightUrno);
		m_SelectedFlights.DeleteString(ilLc);
	}
}

void CThirdPartyFilterDlg::OnOK() 
{
	omSelectedFlightUrnos.RemoveAll();
	for (int ilLc = m_SelectedFlights.GetCount()-1; ilLc >= 0 ; ilLc--)
	{
		omSelectedFlightUrnos.SetAt((void *)m_SelectedFlights.GetItemData(ilLc),NULL);
	}
	CDialog::OnOK();
}

void CThirdPartyFilterDlg::SetSelectedFlights(CMapPtrToPtr &ropSelectedFlights)
{
	long llFlightUrno;
	omSelectedFlightUrnos.RemoveAll();
	for(POSITION rlPos = ropSelectedFlights.GetStartPosition(); rlPos != NULL; )
	{
		ropSelectedFlights.GetNextAssoc(rlPos, (void *&)llFlightUrno, (void *&)pvmDummy);
		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(llFlightUrno);
		if(prlFlight != NULL)
		{
			omSelectedFlightUrnos.SetAt((void *)llFlightUrno,NULL);
			FLIGHTDATA *prlRotation = ogFlightData.GetRotationFlight(prlFlight);
			void *pvlDummy;
			if(prlRotation != NULL && !omSelectedFlightUrnos.Lookup((void *) prlRotation->Urno, pvlDummy))
			{
				omSelectedFlightUrnos.SetAt((void *)prlRotation->Urno,NULL);
			}
		}
	}
}

void CThirdPartyFilterDlg::GetSelectedFlights(CMapPtrToPtr &ropSelectedFlights)
{
	long llFlightUrno;
	ropSelectedFlights.RemoveAll();
	for(POSITION rlPos =  omSelectedFlightUrnos.GetStartPosition(); rlPos != NULL; )
	{
		omSelectedFlightUrnos.GetNextAssoc(rlPos, (void *&)llFlightUrno, (void *&)pvmDummy);

		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(llFlightUrno);
		if(prlFlight != NULL)
		{
			// for rotations add only the arrival
			FLIGHTDATA *prlRotation = ogFlightData.GetRotationFlight(prlFlight);
			if(ogFlightData.IsArrival(prlFlight) || prlRotation == NULL)
			{
				// is arrival or departure without arrival
				ropSelectedFlights.SetAt((void *)prlFlight->Urno,NULL);
			}
			else if(prlRotation != NULL)
			{
				// rotation is the arrival
				ropSelectedFlights.SetAt((void *)prlRotation->Urno,NULL);
			}
		}
	}
}