// StaffDW.cpp : implementation file
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <CCSCedaData.h>
#include <OpssPm.h>
#include <CCSDragDropCtrl.h>
#include <tscale.h>
#include <CCSPtrArray.h>

#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaFlightData.h>
#include <CedaDemandData.h>
#include <CedaShiftData.h>
#include <CedaEmpData.h>
#include <cviewer.h>

#include <StaffDetailWindow.h>

#include <ccsglobl.h>
#include <OpssPm.h>
#include <CCSPtrArray.h>
#include <CLIENTWN.H>
#include <CCSCedaData.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaFlightData.h>
#include <tscale.h>
#include <cviewer.h>
#include <CCSDragDropCtrl.h>
#include <gbar.h>
#include <cxbutton.h>
#include <PrePlanTable.h>
#include <FlightPlan.h>
#include <StaffTable.h>
#include <StaffDiagram.h>
#include <GateDiagram.h>
#include <CciDiagram.h>
#include <StartDate.h>
#include <ConflictConfigTable.h>
#include <conflict.h>
#include <dataset.h>
#include <ufis.h>
#include <CCSBcHandle.h>
#include <BasicData.h>
#include <ConflictTable.h>
#include <CciTable.h>
#include <GateTable.h>
#include <ButtonList.h>

#include <ccsddx.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Prototypes
static void StaffDetailWindowCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// StaffDetailWindow

IMPLEMENT_DYNCREATE(StaffDetailWindow, CFrameWnd)

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

static BOOL GetMaxEndTime(CCSPtrArray<JOBDATA> *popJobs, CTime &ropMaxEndTime)
{
	int ilSize = popJobs->GetSize();
	if (ilSize == 0)
		return FALSE;

	CTime olMaxTime = (*popJobs)[0].Acto;
	CTime olTime;
	for (int ilIndex = 1; ilIndex < ilSize; ilIndex++)
	{
		olTime = (*popJobs)[ilIndex].Acto;
		if (olMaxTime < olTime)
			olMaxTime = olTime;
	}
	ropMaxEndTime = olMaxTime;
	return TRUE;
}

static BOOL GetMinStartTime(CCSPtrArray<JOBDATA> *popJobs, CTime &ropMinStartTime)
{
	BOOL blRc = FALSE;
	if(popJobs != NULL)
	{
		int ilSize = popJobs->GetSize();
		if (ilSize != 0)
		{
			CTime olMinTime = (*popJobs)[0].Acfr;
			CTime olTime;
			for (int ilIndex = 1; ilIndex < ilSize; ilIndex++)
			{
				olTime = (*popJobs)[ilIndex].Acfr;
				if (olMinTime > olTime)
					olMinTime = olTime;
			}
			ropMinStartTime = olMinTime; 
			blRc = TRUE;
		}
	}
	return blRc;
}

StaffDetailWindow::StaffDetailWindow()
{
}

StaffDetailWindow::StaffDetailWindow(CWnd* popParent, long lpJobPrimaryKey)
{
	pomEmp = NULL;
	pomShift = NULL;
	pomJobs = NULL;
    lmJobPrimaryKey = lpJobPrimaryKey;
    imStartTimeScalePos = 20;

	JOBDATA *polJob = ogJobData.GetJobByUrno(lmJobPrimaryKey);
	if (polJob != NULL)
	{
		EMPDATA *polEmp = ogEmpData.GetEmpByPeno(polJob->Peno);
		if (polEmp != NULL)
		{
			SHIFTDATA *polShift = ogShiftData.GetShiftByUrno(polJob->Shur);
			if (polShift != NULL)
			{
				if (polShift->Avfa == TIMENULL || polShift->Avta == TIMENULL)
				{
					return;
				}
			}
			else
			{
				return;
			}
		}
		else
		{
			return;
		}
	}
	else
	{
		return;
	}





    Create(NULL, "Staff Detail Window", WS_OVERLAPPED | WS_CAPTION | WS_BORDER | WS_VISIBLE | WS_POPUP | WS_SYSMENU,
        CRect(0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN)), popParent, NULL,0,NULL);
    CenterWindow();
//	SetWindowPos(&wndTopMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
}

StaffDetailWindow::~StaffDetailWindow()
{
	if(pomJobs != NULL)
	{
		pomJobs->RemoveAll();
	}
}

BEGIN_MESSAGE_MAP(StaffDetailWindow, CFrameWnd)
	//{{AFX_MSG_MAP(StaffDetailWindow)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_GETMINMAXINFO()
	ON_WM_ERASEBKGND()
	ON_WM_KEYDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// StaffDetailWindow message handlers

int StaffDetailWindow::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
    if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO: Add your specialized creation code here
    //
	omViewer.Attach(this);
	omViewer.Init(lmJobPrimaryKey,TRUE);
	pomEmp = omViewer.GetEmpPtr();
	pomShift = omViewer.GetShiftPtr();
	pomJobs = omViewer.GetJobsPtr();

    omStaffDetail.SetStaffPtr(pomEmp);
	omStaffDetail.SetShiftPtr(pomShift);
    omStaffDetail.Create(omStaffDetail.IDD, this);

	//char clBuf[255]; sprintf(clBuf, "Einsatzdaten <%s, %s>", pomEmp->Lnam, pomEmp->Fnam);
	char clBuf[255]; sprintf(clBuf, GetString(IDS_STRING61325), ogEmpData.GetEmpName(pomEmp));
	SetWindowText(clBuf);
    
    CRect olRect;
    int ilBottom = olRect.top;
    omStaffDetail.GetDlgItem(IDC_EINSALZE) -> GetWindowRect(&olRect); ScreenToClient(&olRect);
    imBottomPos = olRect.bottom;
    olRect.bottom -= 10;
    
    CRect olDRect; omStaffDetail.GetClientRect(&olDRect);
    CRect olWRect; GetWindowRect(&olWRect);

	int ilXSize = ::GetSystemMetrics(SM_CXBORDER) * 2;

	// calculate the default size of window
	CSize olSize(olDRect.Width(),olDRect.Height());	// is dialog size
	olSize.cx += ilXSize;							// + 2 * frame border in x - direction

	olSize.cy += ::GetSystemMetrics(SM_CYBORDER);	// + frame border
	olSize.cy += ::GetSystemMetrics(SM_CYCAPTION);	// + caption
	olSize.cy += ::GetSystemMetrics(SM_CYHSCROLL)*2;// + size for status bar
	olSize.cy += ::GetSystemMetrics(SM_CYBORDER);	// + frame border

	CWnd *polParent = GetParent();
	if (polParent)
	{
		olWRect = CRect(CPoint(0,0),olSize);
		polParent->ScreenToClient(olWRect);
	}
	else
	{
		olWRect = CRect(olWRect.TopLeft(),olSize);
	}

    omMaxTrackSize = CPoint(olWRect.Width(), olWRect.Height());
    MoveWindow(olWRect.left, olWRect.top, olWRect.Width(), olWRect.Height(), FALSE);

	CTime olTime, olMinStartTime, olMaxEndTime;

	olMinStartTime = pomShift->Avfa;
	if (GetMinStartTime(pomJobs, olTime) == TRUE)
	{
		if (olMinStartTime.GetTime() > olTime.GetTime())
			olMinStartTime = olTime;
	}
	olMaxEndTime = pomShift->Avta;
	if (GetMaxEndTime(pomJobs, olTime) == TRUE)
	{
		if (olMaxEndTime.GetTime() < olTime.GetTime())
			olMaxEndTime = olTime;
	}

    CTime olCurrentTime = ogBasicData.GetTime();
    CTime olCT = CTime(
        olCurrentTime.GetYear(), olCurrentTime.GetMonth(), olCurrentTime.GetDay(),
        olCurrentTime.GetHour(), olCurrentTime.GetMinute(), 0
    );
	omTSStartTime = olMinStartTime - CTimeSpan(0, 1, 0, 0);

	omTSDuration = olMaxEndTime - omTSStartTime + CTimeSpan(0, 1, 0, 0);

    omTSInterval = CTimeSpan(0, 0, 10, 0);
    olRect.InflateRect(-15, -15);
    omTimeScale.EnableDisplayCurrentTime(FALSE);
    omTimeScale.Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE,
        CRect(olRect.left, olRect.top, olRect.right, olRect.top + 34),
        this, 0, NULL);
    omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);

    // must be static
    static UINT ilIndicators[] = { ID_SEPARATOR };
    omStatusBar.Create(this, CBRS_BOTTOM | WS_CHILD | WS_VISIBLE,AFX_IDW_STATUS_BAR);
    omStatusBar.SetIndicators(ilIndicators, sizeof(ilIndicators) / sizeof(UINT));

    UINT nID, nStyle; int cxWidth;
    omStatusBar.GetPaneInfo(0, nID, nStyle, cxWidth);
    omStatusBar.SetPaneInfo(0, nID, SBPS_STRETCH | SBPS_NORMAL, cxWidth);
	
    omGantt.SetTimeScale(&omTimeScale);
    omGantt.SetViewer(&omViewer);
    omGantt.SetStatusBar(&omStatusBar);
    omGantt.SetDisplayWindow(omTSStartTime, omTSStartTime + omTimeScale.GetDisplayDuration());
    omGantt.SetVerticalScaleWidth(-2);
    omGantt.SetFonts(&ogMSSansSerif_Regular_8, &ogSmallFonts_Regular_6);
    //omGantt.SetVerticalScaleColors(NAVY, SILVER, YELLOW, SILVER);
	omGantt.SetGanttChartColors(NAVY, GRAY, YELLOW, GRAY);
    omGantt.Create(0, CRect(olRect.left, olRect.top + 34, olRect.right, olRect.bottom), &omStaffDetail);
	omGantt.SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
	
	// Register DDX call back function
	TRACE("StaffDetailWindow: DDX Registration\n");
	ogCCSDdx.Register(this, STAFFDIAGRAM_UPDATETIMEBAND, CString("STAFFDW"),CString("Update Time Band"), StaffDetailWindowCf);
	ogCCSDdx.Register(this, UPDATE_CONFLICT_SETUP,CString("STAFFDW"), CString("Global Update"), StaffDetailWindowCf);

	return 0;
}

void StaffDetailWindow::OnDestroy()
{
	if (bgModal == TRUE)
		return;
	// Unregister DDX call back function
	TRACE("StaffDetailWindow: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogCCSDdx.UnRegister(this, NOTUSED);

	CFrameWnd::OnDestroy();
}

void StaffDetailWindow::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
    // TODO: Add your message handler code here and/or call default
    CFrameWnd::OnGetMinMaxInfo(lpMMI);

    lpMMI->ptMaxTrackSize = omMaxTrackSize;
    lpMMI->ptMinTrackSize = omMinTrackSize;
}

BOOL StaffDetailWindow::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default
    CRect olRect; GetClientRect(&olRect);
    olRect.top = imBottomPos;
    CBrush olBrush(SILVER);
    pDC->FillRect(&olRect, &olBrush);

    return TRUE;
}

////////////////////////////////////////////////////////////////////////
// StaffDiagram -- implementation of yellow vertical time band lines

static void StaffDetailWindowCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	StaffDetailWindow *polDetailWindow = (StaffDetailWindow *)popInstance;

	switch (ipDDXType)
	{
		case STAFFDIAGRAM_UPDATETIMEBAND:
		{
			TIMEPACKET *polTimePacket = (TIMEPACKET *)vpDataPointer;
			if (::IsWindow(polDetailWindow->omGantt.GetSafeHwnd()))	// the window is still opened?
			{
				polDetailWindow->omGantt.SetMarkTime(polTimePacket->StartTime, polTimePacket->EndTime);
			}
			break;
		}
		default:
			break;
	}
}

void StaffDetailWindow::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	switch (nChar)
	{
	case VK_ESCAPE:
	    CFrameWnd::OnClose();
		break;
	default:
		CFrameWnd::OnKeyDown(nChar, nRepCnt, nFlags);
		break;
	}
}
