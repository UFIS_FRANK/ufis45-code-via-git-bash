// Class for Equipment Types
#ifndef _CEDAEQTDATA_H_
#define _CEDAEQTDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct EqtDataStruct
{
	long	Urno;		// Unique Record Number
	char	Name[41];	// Name
	char	Code[6];	// Code

	EqtDataStruct(void)
	{
		Urno = 0L;
		strcpy(Name,"");
		strcpy(Code,"");
	}
};

typedef struct EqtDataStruct EQTDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaEqtData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <EQTDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapStringToPtr omNameMap;

// Operations
public:
	CedaEqtData();
	~CedaEqtData();

	CCSReturnCode ReadEqtData();
	EQTDATA* GetEqtByUrno(long lpUrno);
	EQTDATA* GetEqtByName(CString opName);

	CString GetTableName(void);
	CString Dump(long lpUrno);
	void GetEquipmentTypeNames(CStringArray &ropEquipmentTypeNames);

private:
	void AddEqtInternal(EQTDATA &rrpEqt);
	void ClearAll();
};


extern CedaEqtData ogEqtData;
#endif _CEDAEQTDATA_H_
