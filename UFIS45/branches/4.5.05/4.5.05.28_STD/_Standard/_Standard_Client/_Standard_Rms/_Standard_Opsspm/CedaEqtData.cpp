// CedaEqtData.cpp - Class for Equipment Types
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaEqtData.h>
#include <BasicData.h>

CedaEqtData::CedaEqtData()
{                  
    BEGIN_CEDARECINFO(EQTDATA, EqtDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Name,"NAME")
		FIELD_CHAR_TRIM(Code,"CODE")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(EqtDataRecInfo)/sizeof(EqtDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&EqtDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"EQTTAB");
    pcmFieldList = "URNO,NAME,CODE";
}
 
CedaEqtData::~CedaEqtData()
{
	TRACE("CedaEqtData::~CedaEqtData called\n");
	ClearAll();
}

void CedaEqtData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omNameMap.RemoveAll();
	omData.DeleteAll();
}

CCSReturnCode CedaEqtData::ReadEqtData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];
    strcpy(pclWhere,"");
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaEqtData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		EQTDATA rlEqtData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlEqtData)) == RCSuccess)
			{
				AddEqtInternal(rlEqtData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaEqtData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(EQTDATA), pclWhere);
    return ilRc;
}


void CedaEqtData::AddEqtInternal(EQTDATA &rrpEqt)
{
	EQTDATA *prlEqt = new EQTDATA;
	*prlEqt = rrpEqt;
	omData.Add(prlEqt);
	omUrnoMap.SetAt((void *)prlEqt->Urno,prlEqt);
	omNameMap.SetAt(prlEqt->Name,prlEqt);
}


EQTDATA* CedaEqtData::GetEqtByUrno(long lpUrno)
{
	EQTDATA *prlEqt = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlEqt);
	return prlEqt;
}

EQTDATA* CedaEqtData::GetEqtByName(CString opName)
{
	EQTDATA *prlEqt = NULL;
	omNameMap.Lookup(opName, (void *&) prlEqt);
	return prlEqt;
}

CString CedaEqtData::GetTableName(void)
{
	return CString(pcmTableName);
}

CString CedaEqtData::Dump(long lpUrno)
{
	CString olDumpStr;
	EQTDATA *prlEqt = GetEqtByUrno(lpUrno);
	if(prlEqt == NULL)
	{
		olDumpStr.Format("No EQTDATA Found for EQT.URNO <%ld>",lpUrno);
	}
	else
	{
		EQTDATA rlEqt;
		memcpy(&rlEqt,prlEqt,sizeof(EQTDATA));
		CString olData;
		MakeCedaData(&omRecInfo,olData,&rlEqt);
		CStringArray olFieldArray, olDataArray;
		ogBasicData.ExtractItemList(pcmFieldList, &olFieldArray);
		ogBasicData.ExtractItemList(olData, &olDataArray);
		int ilNumFields = olFieldArray.GetSize();
		int ilNumData = olDataArray.GetSize();
		if(ilNumFields != ilNumData)
		{
			olDumpStr.Format("<%s>\n<%s>\n",pcmFieldList,olData);
		}
		else
		{
			for(int ilD = 0; ilD < ilNumData; ilD++)
			{
				olDumpStr += olFieldArray[ilD] + " = <" + olDataArray[ilD] + "> ";
			}
		}
	}
	return olDumpStr;
}

void CedaEqtData::GetEquipmentTypeNames(CStringArray &ropEquipmentTypeNames)
{
	int ilNumData = ogEqtData.omData.GetSize();
	for(int ilLc = 0; ilLc < ilNumData; ilLc++)
	{
		EQTDATA *prlEqt = &ogEqtData.omData[ilLc];
		ropEquipmentTypeNames.Add(prlEqt->Name);
	}
}
