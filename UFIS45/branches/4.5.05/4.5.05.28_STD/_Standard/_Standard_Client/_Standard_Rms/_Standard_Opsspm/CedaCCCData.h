// Class for counter classes

#ifndef _CEDACCCDATA_H_
#define _CEDACCCDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct CccDataStruct
{
	long	Urno;		// Unique Record Number
	char	Cicc[4];	// Counter class
	char	Cicn[31];	// Counter class name

	CccDataStruct(void)
	{
		Urno = 0L;
		strcpy(Cicc,"");
		strcpy(Cicn,"");
	}
};

typedef struct CccDataStruct CCCDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaCccData: public CCSCedaData
{

// Attributes
public:
	CString GetTableName(void);

// Operations
public:
	CedaCccData();
	~CedaCccData();

	CCSReturnCode	ReadCccData();
	CCCDATA*		GetCccByUrno(long lpUrno);
	CCCDATA*		GetCccByClass(const CString& ropClass);
	CCCDATA*		GetCccByName(const CString& ropName);
	int				GetCountOfRecords();
	int				GetClassList(CStringArray& ropClasses);
	int				GetClassNameList(CStringArray& ropNames);
	bool			GetClassNameList(const CStringArray& ropClasses,CStringArray& ropNames);
	bool			GetClassList(const CStringArray& ropNames,CStringArray& ropClasses);
private:
	void			AddCccInternal(CCCDATA &rrpCcc);
	void			ClearAll();
private:
    CCSPtrArray <CCCDATA>	omData;
	CMapPtrToPtr			omUrnoMap;
	CMapStringToPtr			omCiccMap;
	CMapStringToPtr			omCicnMap;
};


extern CedaCccData ogCccData;
#endif _CEDACCCDATA_H_
