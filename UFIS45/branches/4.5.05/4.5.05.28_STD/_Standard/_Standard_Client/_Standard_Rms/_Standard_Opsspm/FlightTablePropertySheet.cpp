// flplanps.cpp : implementation file
//

// These following include files are project dependent
#include <stdafx.h>
#include <OpssPm.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <AllocData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <CedaAltData.h>
#include <CedaGatData.h>
#include <CedaPstData.h>

// These following include files are necessary
#include <cviewer.h>
#include <BasicData.h>
#include <FilterPage.h>
#include <FlightTableSortPage.h>
#include <FlightTableBoundFilterPage.h>
//#include "ZeitPage.h"
#include <BasePropertySheet.h>
#include <FlightTablePropertySheet.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// FlightTablePropertySheet
//

//FlightTablePropertySheet::FlightTablePropertySheet(CWnd* pParentWnd,
//	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
//	(ID_SHEET_FLIGHT_TABLE, pParentWnd, popViewer, iSelectPage)
FlightTablePropertySheet::FlightTablePropertySheet(CWnd* pParentWnd,
	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
	(GetString(IDS_STRING61589), pParentWnd, popViewer, iSelectPage)
{
	AddPage(&m_pageAirline);
	AddPage(&m_pageArrival);
	AddPage(&m_pageDeparture);
	AddPage(&m_pagePosArr);
	AddPage(&m_pagePosDep);
	AddPage(&m_pageBound);
	AddPage(&m_pageSort);

	m_pageAirline.SetCaption(GetString(IDS_STRING61597));
	m_pageArrival.SetCaption(GetString(IDS_STRING61598));
	m_pageDeparture.SetCaption(GetString(IDS_STRING61602));
	m_pagePosArr.SetCaption(GetString(IDS_FILTERPOSARR));
	m_pagePosDep.SetCaption(GetString(IDS_FILTERPOSDEP));

	int ilNumGats = ogGatData.omData.GetSize();
	for(int ilGat = 0; ilGat < ilNumGats; ilGat++)
	{
		GATDATA *prlGat = &ogGatData.omData[ilGat];
		m_pageArrival.omPossibleItems.Add(prlGat->Gnam);
		m_pageDeparture.omPossibleItems.Add(prlGat->Gnam);
	}
	m_pageArrival.omPossibleItems.Add(GetString(IDS_WITHOUTGATE_FILTER));
	m_pageArrival.bmSelectAllEnabled = true;
	m_pageDeparture.omPossibleItems.Add(GetString(IDS_WITHOUTGATE_FILTER));
	m_pageDeparture.bmSelectAllEnabled = true;

	int ilNumPsts = ogPstData.omData.GetSize();
	for(int ilPst = 0; ilPst < ilNumPsts; ilPst++)
	{
		PSTDATA *prlPst = &ogPstData.omData[ilPst];
		m_pagePosArr.omPossibleItems.Add(prlPst->Pnam);
		m_pagePosDep.omPossibleItems.Add(prlPst->Pnam);
	}
	m_pagePosArr.omPossibleItems.Add(GetString(IDS_WITHOUTPOSITION_FILTER));
	m_pagePosArr.bmSelectAllEnabled = true;
	m_pagePosDep.omPossibleItems.Add(GetString(IDS_WITHOUTPOSITION_FILTER));
	m_pagePosDep.bmSelectAllEnabled = true;

	ogAltData.GetAlc2Alc3Strings(m_pageAirline.omPossibleItems);
	m_pageAirline.bmSelectAllEnabled = true;
}

void FlightTablePropertySheet::LoadDataFromViewer()
{
	if(!pomViewer->GetFilter("Airline", m_pageAirline.omSelectedItems))
	{
		m_pageAirline.omSelectedItems.Add(GetString(IDS_ALLSTRING));
	}

	if(!pomViewer->GetFilter("Gate Arrival", m_pageArrival.omSelectedItems))
	{
		m_pageArrival.omSelectedItems.Add(GetString(IDS_ALLSTRING));
	}

	if(!pomViewer->GetFilter("Gate Departure", m_pageDeparture.omSelectedItems))
	{
		m_pageDeparture.omSelectedItems.Add(GetString(IDS_ALLSTRING));
	}

	if(!pomViewer->GetFilter("PosArr", m_pagePosArr.omSelectedItems))
	{
		m_pagePosArr.omSelectedItems.Add(GetString(IDS_ALLSTRING));
	}

	if(!pomViewer->GetFilter("PosDep", m_pagePosDep.omSelectedItems))
	{
		m_pagePosDep.omSelectedItems.Add(GetString(IDS_ALLSTRING));
	}

	pomViewer->GetSort(m_pageSort.omSortOrders);
	m_pageSort.m_Group = pomViewer->GetGroup()[0] - '0';

	CStringArray olInbound, olOutbound;
	pomViewer->GetFilter("Inbound", olInbound);
	pomViewer->GetFilter("Outbound", olOutbound);
	m_pageBound.m_Inbound = olInbound[0][0] - '0';
	m_pageBound.m_Outbound = olOutbound[0][0] - '0';

	CStringArray olDisplayPax;
	pomViewer->GetFilter("DisplayPax", olDisplayPax);
	m_pageBound.m_DisplayPax = olDisplayPax.GetSize() > 0 ? olDisplayPax[0][0] - '0' : FALSE;
}

void FlightTablePropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{
	pomViewer->SetFilter("Airline", m_pageAirline.omSelectedItems);
	pomViewer->SetFilter("Gate Arrival", m_pageArrival.omSelectedItems);
	pomViewer->SetFilter("Gate Departure", m_pageDeparture.omSelectedItems);
	pomViewer->SetFilter("PosArr", m_pagePosArr.omSelectedItems);
	pomViewer->SetFilter("PosDep", m_pagePosDep.omSelectedItems);
	pomViewer->SetSort(m_pageSort.omSortOrders);
	pomViewer->SetGroup(CString(m_pageSort.m_Group + '0'));
//	pomViewer->SetFilter("Zeit", m_pageZeit.omFilterValues);

	CStringArray olInbound, olOutbound;
	olInbound.Add(CString(m_pageBound.m_Inbound + '0'));
	pomViewer->SetFilter("Inbound", olInbound);
	olOutbound.Add(CString(m_pageBound.m_Outbound + '0'));
	pomViewer->SetFilter("Outbound",olOutbound);

	CStringArray olDisplayPax;
	olDisplayPax.Add(CString(m_pageBound.m_DisplayPax + '0'));
	pomViewer->SetFilter("DisplayPax", olDisplayPax);
	
	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}
}

int FlightTablePropertySheet::QueryForDiscardChanges()
{
	CStringArray olSelectedAirlines;
	CStringArray olSelectedArrivals;
	CStringArray olSelectedDepartures;
	CStringArray olSelectedPosArr, olSelectedPosDep;
	CStringArray olSortOrders;
	CString olGroupBy;
	CStringArray olSelectedTime;
	pomViewer->GetFilter("Airline", olSelectedAirlines);
	pomViewer->GetFilter("Gate Arrival", olSelectedArrivals);
	pomViewer->GetFilter("Gate Departure", olSelectedDepartures);
	pomViewer->GetFilter("PosArr", olSelectedPosArr);
	pomViewer->GetFilter("PosDep", olSelectedPosDep);
	pomViewer->GetSort(olSortOrders);
	olGroupBy = pomViewer->GetGroup();
//	pomViewer->GetFilter("Zeit", olSelectedTime);

	CStringArray olInbound, olOutbound, olDisplayPax;
	pomViewer->GetFilter("Inbound", olInbound);
	pomViewer->GetFilter("Outbound", olOutbound);
	pomViewer->GetFilter("DisplayPax", olDisplayPax);
	if(olDisplayPax.GetSize() <= 0)
		olDisplayPax.Add("0"); 

	if (!IsIdentical(olSelectedAirlines, m_pageAirline.omSelectedItems) ||
		!IsIdentical(olSelectedArrivals, m_pageArrival.omSelectedItems) ||
		!IsIdentical(olSelectedDepartures, m_pageDeparture.omSelectedItems) ||
		!IsIdentical(olSelectedPosArr, m_pagePosArr.omSelectedItems) ||
		!IsIdentical(olSelectedPosDep, m_pagePosDep.omSelectedItems) ||
		!IsIdentical(olSortOrders, m_pageSort.omSortOrders) ||
		olGroupBy != CString(m_pageSort.m_Group + '0') ||
		olDisplayPax[0] != CString(m_pageBound.m_DisplayPax + '0') ||
		olInbound[0] != CString(m_pageBound.m_Inbound + '0') ||
		olOutbound[0] != CString(m_pageBound.m_Outbound + '0'))
	{
		// Discard changes ?
		return MessageBox(GetString(IDS_STRING61583), NULL, MB_ICONQUESTION | MB_OKCANCEL);
	}

	return IDOK;
}
