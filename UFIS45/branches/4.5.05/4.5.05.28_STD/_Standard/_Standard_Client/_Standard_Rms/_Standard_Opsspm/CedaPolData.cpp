// CedaPolData.cpp - Class for pools
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaPolData.h>
#include <BasicData.h>

CedaPolData::CedaPolData()
{                  
    BEGIN_CEDARECINFO(POLDATA, PolDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Name,"NAME")
		FIELD_CHAR_TRIM(Pool,"POOL")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(PolDataRecInfo)/sizeof(PolDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&PolDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"POLTAB");
    pcmFieldList = "URNO,NAME,POOL";
}
 
CedaPolData::~CedaPolData()
{
	TRACE("CedaPolData::~CedaPolData called\n");
	ClearAll();
}

void CedaPolData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omNameMap.RemoveAll();
	omData.DeleteAll();
}

CCSReturnCode CedaPolData::ReadPolData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

    char pclWhere[512];
    strcpy(pclWhere,"");
	char pclCom[10] = "RT";
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaPolData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		POLDATA rlPolData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlPolData)) == RCSuccess)
			{
				AddPolInternal(rlPolData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaPolData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(POLDATA), pclWhere);
    return ilRc;
}


void CedaPolData::AddPolInternal(POLDATA &rrpPol)
{
	POLDATA *prlPol = new POLDATA;
	*prlPol = rrpPol;
	omData.Add(prlPol);
	omUrnoMap.SetAt((void *)prlPol->Urno,prlPol);
	omNameMap.SetAt(prlPol->Name,prlPol);
}


POLDATA* CedaPolData::GetPolByUrno(long lpUrno)
{
	POLDATA *prlPol = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlPol);
	return prlPol;
}

POLDATA* CedaPolData::GetPolByName(const char *pcpName)
{
	POLDATA *prlPol = NULL;
	omNameMap.Lookup(pcpName, (void *&) prlPol);
	return prlPol;
}

long CedaPolData::GetPolUrnoByName(const char *pcpName)
{
	long llUrno = 0L;
	POLDATA *prlPol = GetPolByName(pcpName);
	if(prlPol != NULL)
	{
		llUrno = prlPol->Urno;
	}
	return llUrno;
}

void CedaPolData::GetPoolNames(CStringArray &ropPoolNames)
{
	int ilNumPols = omData.GetSize();
	for(int ilPol = 0; ilPol < ilNumPols; ilPol++)
	{
		POLDATA *prlPol = &omData[ilPol];
		ropPoolNames.Add(prlPol->Name);
	}
}

CString CedaPolData::GetTableName(void)
{
	return CString(pcmTableName);
}