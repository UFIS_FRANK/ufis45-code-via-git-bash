#include <stdafx.h>
#include <resource.h>
#include <CCSObj.h>
#include <ccsglobl.h>
#include <PrmConfig.h>
#include <PrivList.h>
#include <CedaAltData.h>

PrmConfig::PrmConfig(void)
{

//   GetPrivateProfileString("GLOBAL]", "PRM-HandlingAgents", "", cgPrmHandlingAgents, sizeof pclTmpText, olConfigFileName);
   UseHag = false;
   prmHag = NULL;
}

void PrmConfig::ReadHandlingAgentsList()
{
	CStringArray olShortNames;

	ogHagData.GetShortNameList(olShortNames);
	for (int i = 0; i < olShortNames.GetSize(); i++)
	{
		CString tmpString = olShortNames[i];
		if (CheckHandlingAgent(olShortNames[i])) 
		{
			prmHag = ogHagData.GetHagByHsna(olShortNames[i]);
			break;
		}
	}
}
                 

char *PrmConfig::getHandlingAgentShortName()
{
	if (prmHag == NULL)
	{
		if (*cmHandlingAgentsList =='\0')
		{
			ReadHandlingAgentsList();
		}
	}
	
	if (prmHag != NULL)
	{
		return(prmHag->Hsna);
	}
	return "XXX";
}

bool PrmConfig::CheckHandlingAgent(CString opHandlingAgent)
{
	return ogPrmPrivList.GetStat(CString("PRM-Handling Agents-" + opHandlingAgent)) == '1';
}
   
char *PrmConfig::getAirlineList()
{

	static char clAirlineList[2048] = "";

	if (*clAirlineList == '\0')
	{
		if (prmHag == NULL)
		{
			if (*cmHandlingAgentsList =='\0')
			{
				ReadHandlingAgentsList();
			}
		}
		if (prmHag != NULL)
		{
			ogAltData.GetAirlineList(prmHag->Hsna,clAirlineList);
		}
	}

	return(clAirlineList);
}