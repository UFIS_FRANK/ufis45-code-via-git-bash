// DemandTableSortPage.cpp : implementation file
//

#include <stdafx.h>
#include <OpssPm.h>
#include <CCSGlobl.h>
#include <DemandTableSortPage.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Change this key identifiers for name used in the registry database
#define NUMBER_OF_SORTKEYS	8
static CString ogSortKeys[NUMBER_OF_SORTKEYS] =
	{ "Aloc", "Debe", "Deen", "Dedu", "AFnum", "DFnum","Term","none" }; //Singapore
#define NOSORT	(NUMBER_OF_SORTKEYS - 1)	// must be "none"

/////////////////////////////////////////////////////////////////////////////
// DemandTableSortPage property page

IMPLEMENT_DYNCREATE(DemandTableSortPage, CPropertyPage)

DemandTableSortPage::DemandTableSortPage() : CPropertyPage(DemandTableSortPage::IDD)
{
	//{{AFX_DATA_INIT(DemandTableSortPage)
	m_Group = FALSE;
	m_SortOrder0 = -1;
	m_SortOrder1 = -1;
	m_SortOrder2 = -1;
	//}}AFX_DATA_INIT
	omTitle = GetString(IDS_STRING32900);
	m_psp.pszTitle = omTitle;
	m_psp.dwFlags |= PSP_USETITLE;

	omUpArrow.LoadBitmap(IDB_UPARROW);
	omDownArrow.LoadBitmap(IDB_DOWNARROW);
	bmDemStartAscending = true;
	bmDemEndAscending = true;

	
}

DemandTableSortPage::~DemandTableSortPage()
{
}

void DemandTableSortPage::DoDataExchange(CDataExchange* pDX)
{
	// Extended data exchange -- for member variables with Value type
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		omSortOrders.SetSize(3);
		m_SortOrder0 = GetSortOrder(omSortOrders[0]);
		m_SortOrder1 = GetSortOrder(omSortOrders[1]);
		m_SortOrder2 = GetSortOrder(omSortOrders[2]);
		SetDirectionButton(IDC_DEMENDDIRECTION, bmDemEndAscending);
		SetDirectionButton(IDC_DEMSTARTDIRECTION, bmDemStartAscending);
		SetDirectionButton(IDC_DEMTERMINALDIRECTION, bmDemTerminalAscending);
	}

	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DemandTableSortPage)
	DDX_Check(pDX, IDC_CHECK1, m_Group);
	DDX_Radio(pDX, IDC_RADIO1, m_SortOrder0);
	DDX_Radio(pDX, IDC_RADIO2, m_SortOrder1);
	DDX_Radio(pDX, IDC_RADIO3, m_SortOrder2);
	//}}AFX_DATA_MAP

	// Extended data validation
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		omSortOrders.RemoveAll();
		omSortOrders.Add(GetSortKey(m_SortOrder0));
		omSortOrders.Add(GetSortKey(m_SortOrder1));
		omSortOrders.Add(GetSortKey(m_SortOrder2));
	}
}


BEGIN_MESSAGE_MAP(DemandTableSortPage, CPropertyPage)
	//{{AFX_MSG_MAP(DemandTableSortPage)
	ON_BN_CLICKED(IDC_DEMENDDIRECTION, OnDemEndDirection)
	ON_BN_CLICKED(IDC_DEMSTARTDIRECTION, OnDemStartDirection)
	ON_BN_CLICKED(IDC_DEMTERMINALDIRECTION, OnDemTerminalDirection)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DemandTableSortPage message handlers

BOOL DemandTableSortPage::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	BOOL bResult = CPropertyPage::OnCommand(wParam, lParam);

	if (HIWORD(wParam) == BN_CLICKED)
	{
		UpdateData();	// caution: this assume there is always no error

		// Disallow using the same sorting order more than once
		if (m_SortOrder1 == m_SortOrder0)
			omSortOrders[1] = "none";
		if (m_SortOrder2 == m_SortOrder0 || m_SortOrder2 == m_SortOrder1)
			omSortOrders[2] = "none";
		UpdateData(FALSE);
	}
	return bResult;
}

/////////////////////////////////////////////////////////////////////////////
// DemandTableSortPage -- helper routines

int DemandTableSortPage::GetSortOrder(const char *pcpSortKey)
{
	for (int i = 0; i < NUMBER_OF_SORTKEYS; i++)
		if (ogSortKeys[i] == pcpSortKey)
			return i;

	// If there is no sorting order matched, assume "none" for no sorting
	return NOSORT;
}

CString DemandTableSortPage::GetSortKey(int ipSortOrder)
{
	if (0 <= ipSortOrder && ipSortOrder <= NUMBER_OF_SORTKEYS-1)
		return ogSortKeys[ipSortOrder];

	return "";	// invalid sort order
}

BOOL DemandTableSortPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	CWnd *polWnd = GetDlgItem(IDC_TXT_SORTALLOC); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61871));
	}
	polWnd = GetDlgItem(IDC_TXT_SORTDEBE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61872));
	}
	polWnd = GetDlgItem(IDC_TXT_SORTDEEN); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61873));
	}
	polWnd = GetDlgItem(IDC_TXT_SORTDEDU); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61874));
	}
	polWnd = GetDlgItem(IDC_TXT_SORTAFNUM); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61881));
	}
	polWnd = GetDlgItem(IDC_TXT_SORTDFNUM); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61882));
	}
	polWnd = GetDlgItem(IDC_TXT_SORTTERM);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING62531));
	}
	polWnd = GetDlgItem(IDC_TXT_SORTNONE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61328));
	}
	polWnd = GetDlgItem(IDC_CHECK1); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32902));
	}

	char pclTmpText[512];
	char pclConfigPath[512];
	char pclConfigCopy[512];
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));
	
	GetPrivateProfileString(pcgAppName, "DEMTERMORDER",  "NO",pclTmpText, sizeof pclTmpText, pclConfigPath);
	if (!strcmp(pclTmpText,"YES")) {
		polWnd = GetDlgItem(IDC_DEMTERMINALDIRECTION);
		if(polWnd != NULL)
	{
		polWnd->ShowWindow(SW_SHOW);
	}
		
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void DemandTableSortPage::OnDemEndDirection() 
{
	bmDemEndAscending = !bmDemEndAscending;
	SetDirectionButton(IDC_DEMENDDIRECTION, bmDemEndAscending);
	SetFocus();
}

void DemandTableSortPage::OnDemStartDirection() 
{
	bmDemStartAscending = !bmDemStartAscending;
	SetDirectionButton(IDC_DEMSTARTDIRECTION, bmDemStartAscending);
	SetFocus();
}

void DemandTableSortPage::OnDemTerminalDirection() 
{
	bmDemTerminalAscending = !bmDemTerminalAscending;
	SetDirectionButton(IDC_DEMTERMINALDIRECTION, bmDemTerminalAscending);
	SetFocus();
}

bool DemandTableSortPage::SetDirectionButton(int ipButtonId, bool bpAscending)
{
	bool blRc = false;
	CButton *prlButton = (CButton *) GetDlgItem(ipButtonId);
	if(prlButton != NULL)
	{
		blRc = true;
		if(bpAscending)
		{
			prlButton->SetBitmap(omDownArrow);
		}
		else
		{
			prlButton->SetBitmap(omUpArrow);
		}
	}

	return blRc;
}
