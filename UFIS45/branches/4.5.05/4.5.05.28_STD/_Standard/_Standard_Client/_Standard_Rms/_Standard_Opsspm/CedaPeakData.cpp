// CedaPeakData.cpp - Class for handling demand profile data
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaJobData.h>
#include <CedaShiftData.h>
#include <CedaShiftTypeData.h>
#include <CedaDemandData.h>
#include <CedaEmpData.h>
#include <CedaDprData.h>
#include <CedaAloData.h>
#include <AllocData.h>
#include <CedaPeakData.h>
#include <BasicData.h>
#include <DataSet.h>
#include <CedaFlightData.h>

#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))

void TrimRight(char *s)
{
    for (int i = strlen(s); i > 0 && isspace(s[i-1]); i--)  // search for last non-space character
        ;
    s[i] = '\0';    // trim off right spaces
}

CedaPeakData ogPeaks;


void CedaPeakData::GetAlidText(char *pcpStation,CString &ropAloText)
{
//	CCSPtrArray<ALODATA> olAlos;
//	ogAloData.GetAlosByAtid(olAlos, "STATION");
//
//	for (int ilLc = 0; ilLc < olAlos.GetSize(); ilLc++)
//	{
//		ALODATA *prlAlo = &olAlos[ilLc];
//		
//		if (strcmp(pcpStation,prlAlo->Alid) == 0)
//		{
//			CreateNewItem(prlAlo,opAloText);	
//		}
//	}

	CCSPtrArray <ALLOCUNIT> olStations;
	ogAllocData.GetUnitsByGroup("STATION",pcpStation,olStations);
	int ilNumUnits = olStations.GetSize();
	for(int ilUnit = 0; ilUnit < ilNumUnits; ilUnit++)
	{
		ALLOCUNIT *prlStation = &olStations[ilUnit];
		ropAloText += CString(":") + prlStation->Name + ":";
	}
}
//void CedaPeakData::CreateNewItem(ALODATA *prpAlo,CString &opAloText)
//{
//	opAloText += CString(":") + prpAlo->Alid + ":";
//
//	CCSPtrArray<METAALODATA> olMetaAlos;
//
//	ogMetaAloData.GetMetaAlosByMeta(olMetaAlos, prpAlo->Alid);
//	//olMetaAlos.Sort(CompareMeta);
//	for (int ilLc = 0; ilLc < olMetaAlos.GetSize(); ilLc++)
//	{
//		METAALODATA *prlMetaAlo = &olMetaAlos[ilLc];
//
//		ALODATA *prlAlo = ogAloData.GetAloByAlid(prlMetaAlo->Alid);
//		if (prlAlo != NULL)
//		{
//		  CreateNewItem(prlAlo,opAloText);	
//		}
//	}
//	return;
//}


CedaPeakData::CedaPeakData()
{                  
    // Create an array of CEDARECINFO for SHIFTTYPEDATA
    BEGIN_CEDARECINFO(PEAKDATA, PeakDataRecInfo)
        FIELD_LONG(Urno,"URNO")
        FIELD_CHAR_TRIM(Peid,"PEID")
        FIELD_CHAR_TRIM(Days,"DAYS")
        FIELD_CHAR_TRIM(Tmfr,"TMFR")
        FIELD_CHAR_TRIM(Tmto,"TMTO")
        FIELD_INT(Olpc,"OLPC")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(PeakDataRecInfo)/sizeof(PeakDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&PeakDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"PEACKI");
    pcmFieldList = "URNO,PEID,DAYS,TMFR,TMTO,OLPC";
}
 
CedaPeakData::~CedaPeakData()
{
	TRACE("CedaPeakData::~CedaPeakData called\n");
	int ilCount = omData.GetSize();
	for(int ilLc = 0; ilLc < ilCount; ilLc++)
	{
		omData[ilLc].ShiftMap->RemoveAll();
	}
	omData.DeleteAll();
}

CCSReturnCode CedaPeakData::ReadPeakData()
{
    char pclSelection[512];
	CCSReturnCode ilRc = RCSuccess;
	int ilLc;

    // Initialize table names and field names
    strcpy(pcmTableName,"PEACKI");
    pcmFieldList = "URNO,PEID,DAYS,TMFR,TMTO,OLPC";

	strcpy(pclSelection,"WHERE SZID = '0' ORDER BY PEID");
    if (CedaAction("RT",pclSelection) == RCFailure)
        return RCFailure;
    omData.DeleteAll();

	PEAKDATA rlPeakData;
	
    for (ilLc = 0; ilRc == RCSuccess; ilLc++)
    {
		if ((ilRc = GetBufferRecord(ilLc,&rlPeakData)) == RCSuccess)
		{
			rlPeakData.ShiftMap = new CMapPtrToPtr;
			omData.NewAt(ilLc,rlPeakData);
		}
	}
	
// now reading the shifts, cUovering each peak
    // Initialize table names and field names
    strcpy(pcmTableName,"SHPCKI");
    pcmFieldList = "STID";

	int ilCount = omData.GetSize();
	for(int ilPeakNo = 0; ilPeakNo < ilCount; ilPeakNo++)
	{
		sprintf(pclSelection,"WHERE URPE = '%ld'",omData[ilPeakNo].Urno);
		if (CedaAction((char *)"RT",(char *)pclSelection) == RCFailure)
			return RCFailure;
		int ilRecCount =  this->GetDataBuff()->GetSize();
		for ( ilLc = 0; ilLc < ilRecCount; ilLc++)
		{
			long llStiu;
			llStiu = atol(this->GetDataBuff()->GetAt(ilLc));
			SHIFTTYPEDATA *prlShiftType = ogShiftTypes.GetShiftTypeByUrno(llStiu);
			if (prlShiftType != NULL)
			{
				omData[ilPeakNo].ShiftMap->SetAt((void *)llStiu,(void *)prlShiftType);
			}
		}
	}


//    RC(RCSuccess);
    return RCSuccess;
}


void  CedaPeakData::ClearAllValues(int ipPeakNo)
{
	omData[ipPeakNo].SollA = 0;
	omData[ipPeakNo].DefaultFmA = 0;
	omData[ipPeakNo].DefaultMaA = 0;
	omData[ipPeakNo].PoolFmA = 0;
	omData[ipPeakNo].PoolMaA = 0;
	omData[ipPeakNo].SollB = 0;
	omData[ipPeakNo].DefaultFmB = 0;
	omData[ipPeakNo].DefaultMaB = 0;
	omData[ipPeakNo].PoolFmB = 0;
	omData[ipPeakNo].PoolMaB = 0;
	omData[ipPeakNo].SollHalle = 0;
	omData[ipPeakNo].DefaultFmHalle = 0;
	omData[ipPeakNo].DefaultMaHalle = 0;
	omData[ipPeakNo].PoolFmHalle = 0;
	omData[ipPeakNo].PoolMaHalle = 0;
	omData[ipPeakNo].SollSumme = 0;
	omData[ipPeakNo].DefaultFmSumme = 0;
	omData[ipPeakNo].DefaultMaSumme = 0;
	omData[ipPeakNo].PoolFmSumme = 0;
	omData[ipPeakNo].PoolMaSumme = 0;
	ClearArrays(ipPeakNo);
}


void  CedaPeakData::ClearArrays(int ipPeakNo)
{
	memset(omData[ipPeakNo].FmAArray,0,sizeof(omData[ipPeakNo].FmAArray));
	memset(omData[ipPeakNo].FmBArray,0,sizeof(omData[ipPeakNo].FmBArray));
	memset(omData[ipPeakNo].MaAArray,0,sizeof(omData[ipPeakNo].MaAArray));
	memset(omData[ipPeakNo].MaBArray,0,sizeof(omData[ipPeakNo].MaBArray));
	memset(omData[ipPeakNo].FmHalleArray,0,sizeof(omData[ipPeakNo].FmHalleArray));
	memset(omData[ipPeakNo].MaHalleArray,0,sizeof(omData[ipPeakNo].MaHalleArray));
	
}



void  CedaPeakData::AddToPeak(int *pipValues,CTime opFrom,CTime opTo)
{
	int ilStart = min((int)((opFrom.GetHour()*60)+opFrom.GetMinute())/5,MAXPEAKVALUES);
	int ilEnd   = min((int)((opTo.GetHour()*60)+opTo.GetMinute())/5,MAXPEAKVALUES);
	for(; ilStart <= ilEnd; ilStart++)
	{
		pipValues[ilStart]++;
	}
}

int CedaPeakData::GetMaxValueOfPeak(int *pipValues)
{
	int ilValue = 0;
	for(int ilStart = 0; ilStart < MAXPEAKVALUES; ilStart++)
	{
		ilValue = max(pipValues[ilStart],ilValue);
	}
	return ilValue;
}

void  CedaPeakData::Recalculate(CTime opDate)
{
	char pclFromHour[4];
	char pclFromMin[4];
	char pclToHour[4];
	char pclToMin[4];
	int ilLc;

	CString pclDate = opDate.Format("%Y%m%d");
	CTime olStart = CTime(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),0,0,0);
	CTime olEnd = CTime(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),23,59,0);
	CString olAloTextA;
	CString olAloTextB;

	GetAlidText("A",olAloTextA);
	GetAlidText("B",olAloTextB);

	int ilPeakCount = omData.GetSize();
	for(int ilPeakNo = 0; ilPeakNo < ilPeakCount; ilPeakNo++)
	{
		ClearAllValues(ilPeakNo);
		PEAKDATA  rlPeak = omData[ilPeakNo];
		strncpy(pclFromHour,rlPeak.Tmfr,2);
		pclFromHour[2] = '\0';
		strncpy(pclFromMin,&rlPeak.Tmfr[2],2);
		pclFromMin[2] = '\0';
		strncpy(pclToHour,rlPeak.Tmto,2);
		pclToHour[2] = '\0';
		strncpy(pclToMin,&rlPeak.Tmto[2],2);
		pclToMin[2] = '\0';
		CTime olPeakStart = CTime(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),
						atoi(pclFromHour),atoi(pclFromMin),0);
		CTime olPeakEnd(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),
						atoi(pclToHour),atoi(pclToMin),0);

	//	CTimeSpan olDemandDuration = olPeakEnd - olPeakStart;
	//	int ilPeakSeconds = olPeakDuration.GetTotalSeconds();
		CTimeSpan olDemandDuration;
		DEMANDDATA *prlDemand;
		CTimeSpan tmpDuration;
		memset(&pimDemandsA,0,sizeof(pimDemandsA));
		memset(&pimDemandsB,0,sizeof(pimDemandsB));
		// Demands for Pool-A and Pool-B
		for ( ilLc = 0 ; ilLc < ogDemandData.omData.GetSize(); ilLc++)
		{
			BOOL blFound = FALSE;
			prlDemand = &ogDemandData.omData[ilLc];
			
			if(IsOverlapped(olPeakStart,olPeakEnd,prlDemand->Debe,prlDemand->Deen))
			{
				FLIGHTDATA *prlFlight = ogDataSet.GetFlightByDemand(prlDemand);
				if (prlFlight != NULL)
				{
					if (strcmp(ogFlightData.GetGate(prlFlight),prlDemand->Alid) == 0)
					{
					//	if (*ogDemandData.omData[ilLc].Alid == 'A')
						if (olAloTextA.Find(CString(":")+ogDemandData.omData[ilLc].Alid+CString(":")) > -1)
						{
							AddToPeak(pimDemandsA,max(prlDemand->Debe,olPeakStart),min(prlDemand->Deen,olPeakEnd));
						//	omData[ilPeakNo].SollA++;
						}
						if (olAloTextB.Find(CString(":")+ogDemandData.omData[ilLc].Alid+CString(":")) > -1)
				//		if (*ogDemandData.omData[ilLc].Alid == 'B')
						{
							AddToPeak(pimDemandsB,max(prlDemand->Debe,olPeakStart),min(prlDemand->Deen,olPeakEnd));
						//	omData[ilPeakNo].SollB++;
						}
					}
				}
			}
		}
		omData[ilPeakNo].SollA = GetMaxValueOfPeak(pimDemandsA);
		omData[ilPeakNo].SollB = GetMaxValueOfPeak(pimDemandsB);

		// Demand for Pool Halle (from dpr)
		CTime olNow(ogBasicData.GetTime());
		CTimeSpan olDayOffset = olPeakStart - olNow;
		int ilDayOffset = olDayOffset.GetDays();
		int ilStart = (((olPeakStart.GetHour()*60)+olPeakStart.GetMinute())/5)+(ilDayOffset*288);
		int ilEnd   = (((olPeakEnd.GetHour()*60)+olPeakEnd.GetMinute())/5)+(ilDayOffset*288);
		int ilDemands = 0;
		int ilMaxDemands;
		DPROFILE *pclDProfileFirst = ogDprs.GetDProfileByAlid("CCI-F",olPeakStart.Format("%Y%m%d"));
		DPROFILE *pclDProfileBusiness = ogDprs.GetDProfileByAlid("CCI-C",olPeakStart.Format("%Y%m%d"));
		DPROFILE *pclDProfileEconomoy = ogDprs.GetDProfileByAlid("CCI-M",olPeakStart.Format("%Y%m%d"));
		if ((pclDProfileFirst != NULL) && (pclDProfileBusiness != NULL) && (pclDProfileEconomoy != NULL))
		{
			ilMaxDemands = 0;
			ilEnd = min(ilEnd,3*288); 
			for (ilLc = ilStart; ilLc < ilEnd; ilLc++)
			{
				ilDemands = 0;
				ilDemands += pclDProfileFirst->Empr[ilLc];
				ilDemands += pclDProfileBusiness->Empr[ilLc];
				ilDemands += pclDProfileEconomoy->Empr[ilLc];
				ilMaxDemands = max(ilMaxDemands,ilDemands);
			}
			if (ilEnd-ilStart != 0)
			{
				omData[ilPeakNo].SollHalle = ilMaxDemands; //(ilDemands/(ilEnd-ilStart))+(ilDemands % (ilEnd-ilStart) ? 1 : 0 );
			}
			else
			{
				omData[ilPeakNo].SollHalle = 0;
			}
		}
	}
	
	EMPDATA *prlEmp;
	SHIFTDATA *prlShift;

	int ilShiftCount = ogShiftData.omData.GetSize();
	for ( ilLc = 0; ilLc < ilShiftCount; ilLc++)
	{
		BOOL blIsFlightManager = FALSE;

		prlShift  = &ogShiftData.omData[ilLc];
		if (!ogShiftData.IsAbsent(prlShift) && prlShift->Sday == pclDate)
		{
			BOOL blFound = FALSE;
			char pclShiftCode[9];
			strcpy(pclShiftCode,strlen(prlShift->Sfca) > 0 ? prlShift->Sfca : prlShift->Sfcs);
			SHIFTTYPEDATA *prlShiftType = ogShiftTypes.GetShiftTypeById(pclShiftCode);
//			long llShiftUrno = prlShift->Stiv != 0L ? prlShift->Stiv : prlShift->Stiu;
			prlEmp = ogEmpData.GetEmpByPeno(prlShift->Peno);
//			SHIFTTYPEDATA *prlShiftType = ogShiftTypes.GetShiftTypeByUrno(llShiftUrno);
			if (prlShiftType != NULL && prlEmp != NULL)
			{
				if ((strcmp(prlShift->Avfa.Format("%H%M"),prlShiftType->Esbg) == 0) &&
					(strcmp(prlShift->Avta.Format("%H%M"),prlShiftType->Lsen) == 0))
				{
					blFound = TRUE; // Schicht nicht geaendert, default Shicht-Peak Zuordnung wird
									// benutzt
					for(ilPeakNo = 0; ilPeakNo < ilPeakCount; ilPeakNo++)
					{
						PEAKDATA  *prlPeak = &omData[ilPeakNo];

						if (prlPeak->ShiftMap->Lookup((void *)prlShiftType->Urno,(void *& )prlShiftType) == TRUE)
						{
							BOOL blIsFlightManager = ogBasicData.IsManager(prlShift->Efct);
//							if (strcmp(prlEmp->Disp,"A") == 0)
							{
								if (blIsFlightManager)
									omData[ilPeakNo].DefaultFmA++;
								else
									omData[ilPeakNo].DefaultMaA++;
							}
//							else if (strcmp(prlEmp->Disp,"B") == 0)
//							{
//								if (blIsFlightManager)
//									omData[ilPeakNo].DefaultFmB++;
//								else
//									omData[ilPeakNo].DefaultMaB++;
//							}
						}
					}
				}
			}
			if (blFound == FALSE  && prlEmp != NULL)
			{
				// Schichttyp nicht gefunden oder abweichende Zeit, Peakzuordnung ueber Anwesenheitszeit
				for(ilPeakNo = 0; ilPeakNo < ilPeakCount; ilPeakNo++)
				{
					PEAKDATA  *prlPeak = &omData[ilPeakNo];

					strncpy(pclFromHour,prlPeak->Tmfr,2);
					pclFromHour[2] = '\0';
					strncpy(pclFromMin,&prlPeak->Tmfr[2],2);
					pclFromMin[2] = '\0';
					strncpy(pclToHour,prlPeak->Tmto,2);
					pclToHour[2] = '\0';
					strncpy(pclToMin,&prlPeak->Tmto[2],2);
					pclToMin[2] = '\0';
					CTime olPeakStart = CTime(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),
									atoi(pclFromHour),atoi(pclFromMin),0);
					CTime olPeakEnd(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),
									atoi(pclToHour),atoi(pclToMin),0);


					if (IsOverlapped(olPeakStart,olPeakEnd,prlShift->Avfa,prlShift->Avta))
					{
						BOOL blIsFlightManager = ogBasicData.IsManager(prlShift->Efct);
//						if (strcmp(prlEmp->Disp,"A") == 0)
						{
							if (blIsFlightManager)
								AddToPeak(omData[ilPeakNo].FmAArray,max(prlShift->Avfa,olPeakStart),min(prlShift->Avta,olPeakEnd));
							else
								AddToPeak(omData[ilPeakNo].MaAArray,max(prlShift->Avfa,olPeakStart),min(prlShift->Avta,olPeakEnd));
						}
//						else if (strcmp(prlEmp->Disp,"B") == 0)
//						{
//							if (blIsFlightManager)
//								AddToPeak(omData[ilPeakNo].FmBArray,max(prlShift->Avfa,olPeakStart),min(prlShift->Avta,olPeakEnd));
//							else
//								AddToPeak(omData[ilPeakNo].MaBArray,max(prlShift->Avfa,olPeakStart),min(prlShift->Avta,olPeakEnd));
//						}
					}
				}
			}
		}
	}

	for(ilPeakNo = 0; ilPeakNo < ilPeakCount; ilPeakNo++)
	{
		PEAKDATA  *prlPeak = &omData[ilPeakNo];
		prlPeak->DefaultFmA += GetMaxValueOfPeak(prlPeak->FmAArray);
		prlPeak->DefaultFmB += GetMaxValueOfPeak(prlPeak->FmBArray);
		prlPeak->DefaultMaA += GetMaxValueOfPeak(prlPeak->MaAArray);
		prlPeak->DefaultMaB += GetMaxValueOfPeak(prlPeak->MaBArray);
	}

	for(ilPeakNo = 0; ilPeakNo < ilPeakCount; ilPeakNo++)
	{ 
		ClearArrays(ilPeakNo);
	}

	// get Ist-FM and Ist-MA for all shifts in this peak
	JOBDATA *prlJob;
	int ilJobCount = ogJobData.omData.GetSize();
	for ( ilLc = 0; ilLc < ilJobCount; ilLc++)
	{
		BOOL blFound = FALSE;
		prlJob = &ogJobData.omData[ilLc];
		if ( ! IsOverlapped(prlJob->Acfr,prlJob->Acto,olStart,olEnd))
		{
			continue;
		}
		if (strcmp(prlJob->Jtco,JOBPOOL) == 0)
		{
			SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prlJob->Shur);
			if (prlShift != NULL)
			{
				SHIFTTYPEDATA *prlShiftType = ogShiftTypes.GetShiftTypeById(prlShift->Sfca);
				if (prlShiftType != NULL)
				{
					CTimeSpan tmpDuration;
					{
						for(ilPeakNo = 0; ilPeakNo < ilPeakCount; ilPeakNo++)
						{
							PEAKDATA  *prlPeak = &omData[ilPeakNo];

							strncpy(pclFromHour,prlPeak->Tmfr,2);
							pclFromHour[2] = '\0';
							strncpy(pclFromMin,&prlPeak->Tmfr[2],2);
							pclFromMin[2] = '\0';
							strncpy(pclToHour,prlPeak->Tmto,2);
							pclToHour[2] = '\0';
							strncpy(pclToMin,&prlPeak->Tmto[2],2);
							pclToMin[2] = '\0';
							CTime olPeakStart = CTime(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),
											atoi(pclFromHour),atoi(pclFromMin),0);
							CTime olPeakEnd(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),
											atoi(pclToHour),atoi(pclToMin),0);


							if (IsOverlapped(olPeakStart,olPeakEnd,prlJob->Acfr,prlJob->Acto))
							{
								BOOL blIsFlightManager = ogBasicData.IsManager(prlShift->Efct);
								if (olAloTextA.Find(CString(":")+prlJob->Alid+CString(":")) > -1)
								{
									if (blIsFlightManager)
										AddToPeak(omData[ilPeakNo].FmAArray,max(prlJob->Acfr,olPeakStart),min(prlJob->Acto,olPeakEnd));
									else
										AddToPeak(omData[ilPeakNo].MaAArray,max(prlJob->Acfr,olPeakStart),min(prlJob->Acto,olPeakEnd));
								}
								else
								if (olAloTextB.Find(CString(":")+prlJob->Alid+CString(":")) > -1)
								{
									if (blIsFlightManager)
										AddToPeak(omData[ilPeakNo].FmBArray,max(prlJob->Acfr,olPeakStart),min(prlJob->Acto,olPeakEnd));
									else
										AddToPeak(omData[ilPeakNo].MaBArray,max(prlJob->Acfr,olPeakStart),min(prlJob->Acto,olPeakEnd));
								}
								else
								if (strcmp("POOLHALLE",prlJob->Alid) == 0)
								{
									if (blIsFlightManager)
										AddToPeak(omData[ilPeakNo].FmHalleArray,max(prlJob->Acfr,olPeakStart),min(prlJob->Acto,olPeakEnd));
									else
										AddToPeak(omData[ilPeakNo].MaHalleArray,max(prlJob->Acfr,olPeakStart),min(prlJob->Acto,olPeakEnd));
								}
							}
						}
					}
				}
			}
		}
	}
	for(ilPeakNo = 0; ilPeakNo < ilPeakCount; ilPeakNo++)
	{
		PEAKDATA  *prlPeak = &omData[ilPeakNo];
		prlPeak->PoolFmA += GetMaxValueOfPeak(prlPeak->FmAArray);
		prlPeak->PoolFmB += GetMaxValueOfPeak(prlPeak->FmBArray);
		prlPeak->PoolMaA += GetMaxValueOfPeak(prlPeak->MaAArray);
		prlPeak->PoolMaB += GetMaxValueOfPeak(prlPeak->MaBArray);
		prlPeak->PoolFmHalle += GetMaxValueOfPeak(prlPeak->FmHalleArray);
		prlPeak->PoolMaHalle += GetMaxValueOfPeak(prlPeak->MaHalleArray);
	}

}

void  CedaPeakData::RecalculatePoolJobs(CTime opDate)
{
	char pclFromHour[4];
	char pclFromMin[4];
	char pclToHour[4];
	char pclToMin[4];
	int ilLc;

	CString pclDate = opDate.Format("%Y%m%d");
	CTime olStart = CTime(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),0,0,0);
	CTime olEnd = CTime(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),23,59,0);
	CString olAloTextA;
	CString olAloTextB;

	GetAlidText("A",olAloTextA);
	GetAlidText("B",olAloTextB);


	int ilPeakCount = omData.GetSize();
	for(int ilPeakNo = 0; ilPeakNo < ilPeakCount; ilPeakNo++)
	{ 
		ClearArrays(ilPeakNo);
		omData[ilPeakNo].PoolFmA = 0;
		omData[ilPeakNo].PoolMaA = 0;
		omData[ilPeakNo].PoolFmB = 0;
		omData[ilPeakNo].PoolMaB = 0;
		omData[ilPeakNo].PoolFmHalle = 0;
		omData[ilPeakNo].PoolMaHalle = 0;
		omData[ilPeakNo].PoolFmSumme = 0;
		omData[ilPeakNo].PoolMaSumme = 0;
	}

	// get Ist-FM and Ist-MA for all shifts in this peak
	JOBDATA *prlJob;
	int ilJobCount = ogJobData.omData.GetSize();
	for ( ilLc = 0; ilLc < ilJobCount; ilLc++)
	{
		BOOL blFound = FALSE;
		prlJob = &ogJobData.omData[ilLc];
		if ( ! IsOverlapped(prlJob->Acfr,prlJob->Acto,olStart,olEnd))
		{
			continue;
		}
		if (strcmp(prlJob->Jtco,JOBPOOL) == 0)
		{
			SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prlJob->Shur);
			if (prlShift != NULL)
			{
				SHIFTTYPEDATA *prlShiftType = ogShiftTypes.GetShiftTypeById(prlShift->Sfca);
				if (prlShiftType != NULL)
				{
					CTimeSpan tmpDuration;
					{
						for(ilPeakNo = 0; ilPeakNo < ilPeakCount; ilPeakNo++)
						{
							PEAKDATA  *prlPeak = &omData[ilPeakNo];

							strncpy(pclFromHour,prlPeak->Tmfr,2);
							pclFromHour[2] = '\0';
							strncpy(pclFromMin,&prlPeak->Tmfr[2],2);
							pclFromMin[2] = '\0';
							strncpy(pclToHour,prlPeak->Tmto,2);
							pclToHour[2] = '\0';
							strncpy(pclToMin,&prlPeak->Tmto[2],2);
							pclToMin[2] = '\0';
							CTime olPeakStart = CTime(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),
											atoi(pclFromHour),atoi(pclFromMin),0);
							CTime olPeakEnd(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),
											atoi(pclToHour),atoi(pclToMin),0);


							if (IsOverlapped(olPeakStart,olPeakEnd,prlJob->Acfr,prlJob->Acto))
							{
								BOOL blIsFlightManager = ogBasicData.IsManager(prlShift->Efct);
								if (olAloTextA.Find(CString(":")+prlJob->Alid+CString(":")) > -1)
								{
									if (blIsFlightManager)
										AddToPeak(omData[ilPeakNo].FmAArray,max(prlJob->Acfr,olPeakStart),min(olPeakEnd,prlJob->Acto));
									else
										AddToPeak(omData[ilPeakNo].MaAArray,max(prlJob->Acfr,olPeakStart),min(olPeakEnd,prlJob->Acto));
								}
								else
								if (olAloTextB.Find(CString(":")+prlJob->Alid+CString(":")) > -1)
								{
									if (blIsFlightManager)
										AddToPeak(omData[ilPeakNo].FmBArray,max(prlJob->Acfr,olPeakStart),min(olPeakEnd,prlJob->Acto));
									else
										AddToPeak(omData[ilPeakNo].MaBArray,max(prlJob->Acfr,olPeakStart),min(olPeakEnd,prlJob->Acto));
								}
								else
								if (strcmp("POOLHALLE",prlJob->Alid) == 0)
								{
									if (blIsFlightManager)
										AddToPeak(omData[ilPeakNo].FmHalleArray,max(prlJob->Acfr,olPeakStart),min(olPeakEnd,prlJob->Acto));
									else
										AddToPeak(omData[ilPeakNo].MaHalleArray,max(prlJob->Acfr,olPeakStart),min(olPeakEnd,prlJob->Acto));
								}
							}
						}
					}
				}
			}
		}
	}
	for(ilPeakNo = 0; ilPeakNo < ilPeakCount; ilPeakNo++)
	{
		PEAKDATA  *prlPeak = &omData[ilPeakNo];
		prlPeak->PoolFmA += GetMaxValueOfPeak(prlPeak->FmAArray);
		prlPeak->PoolFmB += GetMaxValueOfPeak(prlPeak->FmBArray);
		prlPeak->PoolMaA += GetMaxValueOfPeak(prlPeak->MaAArray);
		prlPeak->PoolMaB += GetMaxValueOfPeak(prlPeak->MaBArray);
		prlPeak->PoolFmHalle += GetMaxValueOfPeak(prlPeak->FmHalleArray);
		prlPeak->PoolMaHalle += GetMaxValueOfPeak(prlPeak->MaHalleArray);
	}

/*********************************

	int ilPeakCount = omData.GetSize();
	for(int ilPeakNo = 0; ilPeakNo < ilPeakCount; ilPeakNo++)
	{
		omData[ilPeakNo].PoolFmA = 0;
		omData[ilPeakNo].PoolMaA = 0;
		omData[ilPeakNo].PoolFmB = 0;
		omData[ilPeakNo].PoolMaB = 0;
		omData[ilPeakNo].PoolFmHalle = 0;
		omData[ilPeakNo].PoolMaHalle = 0;
		omData[ilPeakNo].PoolFmSumme = 0;
		omData[ilPeakNo].PoolMaSumme = 0;
	}
	// get Ist-FM and Ist-MA for all shifts in this peak
	JOBDATA *prlJob;
	int ilJobCount = ogJobData.omData.GetSize();
	for ( ilLc = 0; ilLc < ilJobCount; ilLc++)
	{
		BOOL blFound = FALSE;
		prlJob = &ogJobData.omData[ilLc];
		if ( prlJob->IsChanged == DATA_DELETED || (! IsOverlapped(prlJob->Acfr,prlJob->Acto,olStart,olEnd)))
		{
			continue;
		}
		if (strcmp(prlJob->Jtco,JOBPOOL) == 0)
		{
			CTimeSpan tmpDuration;
			{
				//  **** Shifttime changed *************
				for(ilPeakNo = 0; ilPeakNo < ilPeakCount; ilPeakNo++)
				{
					PEAKDATA  *prlPeak = &omData[ilPeakNo];

					strncpy(pclFromHour,prlPeak->Tmfr,2);
					pclFromHour[2] = '\0';
					strncpy(pclFromMin,&prlPeak->Tmfr[2],2);
					pclFromMin[2] = '\0';
					strncpy(pclToHour,prlPeak->Tmto,2);
					pclToHour[2] = '\0';
					strncpy(pclToMin,&prlPeak->Tmto[2],2);
					pclToMin[2] = '\0';
					CTime olPeakStart = CTime(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),
									atoi(pclFromHour),atoi(pclFromMin),0);
					CTime olPeakEnd(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),
									atoi(pclToHour),atoi(pclToMin),0);


					if (IsOverlapped(olPeakStart,olPeakEnd,prlJob->Acfr,prlJob->Acto))
					{
						BOOL blIsFlightManager = FALSE;
						SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prlJob->Shur);
						if (prlShift != NULL)
						{
							blIsFlightManager = (ogBasicData.omManager.Find(CString(":")+prlShift->Rank+":") != -1);
						}

						if (olAloTextA.Find(CString(":")+prlJob->Alid+CString(":")) > -1)
						{
							if (blIsFlightManager)
								omData[ilPeakNo].PoolFmA++;
							else
								omData[ilPeakNo].PoolMaA++;
						}
						else if (olAloTextB.Find(CString(":")+prlJob->Alid+CString(":")) > -1)
						{
							if (blIsFlightManager)
								omData[ilPeakNo].PoolFmB++;
							else
								omData[ilPeakNo].PoolMaB++;
						}
						else if (strcmp("POOLHALLE",prlJob->Alid) == 0)
						{
							if (blIsFlightManager)
								omData[ilPeakNo].PoolFmHalle++;
							else
								omData[ilPeakNo].PoolMaHalle++;
						}
					}
				}
			}
		}
	}
	****************/
}


CString CedaPeakData::GetTableName(void)
{
	return CString(pcmTableName);
}



