// GateGantt.cpp : implementation file
//  

#include <stdafx.h>
#include <ccsglobl.h>
#include <OpssPm.h>
#include <CCSPtrArray.h>
#include <CLIENTWN.H>
#include <CCSCedaData.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaFlightData.h>
#include <tscale.h>
#include <cviewer.h>
#include <CCSDragDropCtrl.h>
#include <gbar.h>
#include <cxbutton.h>
#include <PrePlanTable.h>
#include <CCITable.h>
#include <gatetable.h>
#include <FlightPlan.h>
#include <StaffTable.h>
#include <StaffDiagram.h>
#include <GateDiagram.h>
#include <CciDiagram.h>
#include <StartDate.h>
#include <ConflictConfigTable.h>
#include <conflict.h>
#include <dataset.h>
#include <ufis.h>
#include <CCSBcHandle.h>
#include <BasicData.h>
#include <ConflictTable.h>
#include <ButtonList.h>
#include <FlightDetailWindow.h>
#include <StaffDetailWindow.h>
#include <ccsddx.h>
#include <GateViewer.h>
#include <GateDetailWindow.h>
#include <GateGantt.h>
#include <dgatejob.h>
#include <AvailableEmpsDlg.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsTotallyInside(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))


////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This block of code is used for substituing the scale calculation
// since the TimeScale has been implemented with a different pixel
// calculation algorithm (Pichet used the floating point calculation,
// which introduce +1 or -1 rounding effect. Furthermore, he still has
// some adjustments may be 2 or 3 pixels for makeing the TimeScale
// display nicely :-). Besides, there are still more few pixels which
// display inappropriately which introduced from the border of the
// GateChart or the other classes of the same kind). To fix all of these
// things, the only fastest way is just adopt the scaling algorithm
// written in the TimeScale and come back to clean up this sometime later.
//
// After a few though, just replace the macro GetX(time) and GetCTime(x)
// should be enough, still we had to update the "omDisplayStart" and
// "omDisplayEnd" everytime these value has been changed.
#define	FIX_TIMESCALE_ROUNDING_ERROR
#ifdef	FIX_TIMESCALE_ROUNDING_ERROR
#define GetX(time)	(pomTimeScale->GetXFromTime(time) + imVerticalScaleWidth)
#define GetCTime(x)	(pomTimeScale->GetTimeFromX((x) - imVerticalScaleWidth))
#else
////////////////////////////////////////////////////////////////////////

// Macro definition for get X-coordinate from the given time
#define GetX(time)  (omDisplayStart == omDisplayEnd? -1: \
            (imVerticalScaleWidth + \
            (((time) - omDisplayStart).GetTotalSeconds() * \
            (imWindowWidth - imVerticalScaleWidth) / \
            (omDisplayEnd - omDisplayStart).GetTotalSeconds())))

// Macro definition for get time from the given X-coordinate
#define GetCTime(x)  ((imWindowWidth - imVerticalScaleWidth) == 0? TIMENULL: \
            (omDisplayStart + \
            (time_t)(((x) - imVerticalScaleWidth) * \
            (omDisplayEnd - omDisplayStart).GetTotalSeconds() / \
            (imWindowWidth - imVerticalScaleWidth))))

////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
#endif
////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// GateGantt

GateGantt::GateGantt(GateDiagramViewer *popViewer, int ipGroupno,
    int ipVerticalScaleWidth, int ipVerticalScaleIndent,
    CFont *popVerticalScaleFont, CFont *popGanttChartFont,
    int ipGutterHeight, int ipOverlapHeight,
    COLORREF lpVerticalScaleTextColor, COLORREF lpVerticalScaleBackgroundColor,
    COLORREF lpHighlightVerticalScaleTextColor, COLORREF lpHighlightVerticalScaleBackgroundColor,
    COLORREF lpGanttChartTextColor, COLORREF lpGanttChartBackgroundColor,
    COLORREF lpHighlightGanttChartTextColor, COLORREF lpHighlightGanttChartBackgroundColor)
{
    SetViewer(popViewer, ipGroupno);
    SetVerticalScaleWidth(ipVerticalScaleWidth);
    SetVerticalScaleIndent(ipVerticalScaleIndent);
    SetGutters(ipGutterHeight, ipOverlapHeight);
	SetFonts(ogGateIndexes.VerticalScale, ogGateIndexes.Chart);
    SetVerticalScaleColors(lpVerticalScaleTextColor, lpVerticalScaleBackgroundColor,
        lpHighlightVerticalScaleTextColor, lpHighlightVerticalScaleBackgroundColor);
    SetGanttChartColors(lpGanttChartTextColor, lpGanttChartBackgroundColor,
        lpHighlightGanttChartTextColor, lpHighlightGanttChartBackgroundColor);

    // Initialize default values
    omDisplayStart = TIMENULL;
    omDisplayEnd = TIMENULL;
    imWindowWidth = 0;          // unessential -- WM_SIZE will initialize this
    bmIsFixedScaling = TRUE;    // unessential -- will be set in method SetDisplayWindow()
    omCurrentTime = TIMENULL;
    omMarkTimeStart = TIMENULL;
    omMarkTimeEnd = TIMENULL;
    pomStatusBar = NULL;
	pomTimeScale = NULL;
    bmIsMouseInWindow = FALSE;
    bmIsControlKeyDown = FALSE;
    imHighlightLine = -1;
    imCurrentBar = -1;
	imContextItem = -1;
	bmContextBarSet = FALSE;
	imContextBarNo = -1;
	bmActiveLineSet = FALSE;  // There is a selected line ?
	lmActiveUrno = -1;	// Urno of last selected line

	// Id 1-Oct-96
	// This will help us create a virtual double click on a double click after SetCursorPos().
	omLastClickedPosition = CPoint(-1, -1);

    // Required only if you allow moving/resizing
    SetBorderPrecision(5);
    umResizeMode = HTNOWHERE;
}

GateGantt::~GateGantt()
{
    pogButtonList->UnRegisterTimer(this);    // install the timer
}


void GateGantt::SetViewer(GateDiagramViewer *popViewer, int ipGroupno)
{
    pomViewer = popViewer;
    imGroupno = ipGroupno;
}

void GateGantt::SetVerticalScaleWidth(int ipWidth)
{
    imVerticalScaleWidth = ipWidth;
}

void GateGantt::SetVerticalScaleIndent(int ipIndent)
{
    imVerticalScaleIndent = ipIndent;
}

void GateGantt::SetFonts(int ipIndex1, int ipIndex2)
{
	LOGFONT rlLogFont;
    pomVerticalScaleFont = &ogScalingFonts[ipIndex1];//popVerticalScaleFont;
    pomGanttChartFont = &ogScalingFonts[ipIndex2];//&ogSmallFonts_Regular_4;//popGanttChartFont;
	pomGanttChartFont->GetLogFont(&rlLogFont);

	switch(ipIndex2)
	{	case 0 : SetGutters(imGutterHeight, 2);

		break;
	case 1 : SetGutters(imGutterHeight, 4);
		break;
	case 2 : SetGutters(imGutterHeight, 8);
		break;
	}
    // Calculate the normal height of a bar
    CDC dc;
    dc.CreateCompatibleDC(NULL);
    if (pomGanttChartFont)
        dc.SelectObject(pomGanttChartFont);
    TEXTMETRIC tm;
    dc.GetTextMetrics(&tm);
    imBarHeight = tm.tmHeight + tm.tmExternalLeading + 2;
    imLeadingHeight = (tm.tmExternalLeading + 2) / 2;
    dc.DeleteDC();
}

void GateGantt::SetGutters(int ipGutterHeight, int ipOverlapHeight)
{
    imGutterHeight = ipGutterHeight;
    imOverlapHeight = ipOverlapHeight;
}

void GateGantt::SetVerticalScaleColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
    COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor)
{
    lmVerticalScaleTextColor = lpTextColor;
    lmVerticalScaleBackgroundColor = lpBackgroundColor;
    lmHighlightVerticalScaleTextColor = lpHighlightTextColor;
    lmHighlightVerticalScaleBackgroundColor = lpHighlightBackgroundColor;
}

void GateGantt::SetGanttChartColors(COLORREF lpTextColor, COLORREF lpBackgroundColor,
    COLORREF lpHighlightTextColor, COLORREF lpHighlightBackgroundColor)
{
    lmGanttChartTextColor = lpTextColor;
    lmGanttChartBackgroundColor = lpBackgroundColor;
    lmHighlightGanttChartTextColor = lpHighlightTextColor;
    lmHighlightGanttChartBackgroundColor = lpHighlightBackgroundColor;
}

// Attentions:
// Return the height that it's expected for being displayed
// Be careful, this one may be called before the CListBox was created.
//
int GateGantt::GetGanttChartHeight()
{
    int ilHeight = 0;
    for (int ilLineno = 0; ilLineno < pomViewer->GetLineCount(imGroupno); ilLineno++)
        ilHeight += GetLineHeight(ilLineno);

    return ilHeight;
}

int GateGantt::GetLineHeight(int ilLineno)
{
	int ilVisualMaxOverlapLevel = pomViewer->GetVisualMaxOverlapLevel(imGroupno, ilLineno);
	return (2 * imGutterHeight) + imBarHeight + (ilVisualMaxOverlapLevel * imOverlapHeight);
}

BOOL GateGantt::Create(DWORD dwStyle, const RECT &rect, CWnd *pParentWnd, UINT nID)
{
    // Make sure that the viewer is already given
    if (pomViewer == NULL)
        return FALSE;

    // Make sure that all essential style is defined
    // WS_CLIPSIBLINGS is very important for the ListBox so it does not paint outside its window
    dwStyle |= WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_CLIPSIBLINGS;
    dwStyle |= LBS_NOINTEGRALHEIGHT | LBS_OWNERDRAWVARIABLE;
    dwStyle |= LBS_MULTIPLESEL;

    // Create the window
    if (CListBox::Create(dwStyle, rect, pParentWnd, nID) == FALSE)
        return FALSE;

    // Create items according to what's in the Viewer
    ResetContent();
    for (int ilLineno = 0; ilLineno < pomViewer->GetLineCount(imGroupno); ilLineno++)
    {
        AddString("");
        SetItemHeight(ilLineno, GetLineHeight(ilLineno));
    }

    return TRUE;
}

void GateGantt::SetStatusBar(CStatusBar *popStatusBar)
{
    pomStatusBar = popStatusBar;
}

void GateGantt::SetTimeScale(CTimeScale *popTimeScale)
{
    pomTimeScale = popTimeScale;
}

void GateGantt::SetBorderPrecision(int ipBorderPrecision)
{
    imBorderPreLeft = ipBorderPrecision / 2;
    imBorderPreRight = (ipBorderPrecision+1) / 2;
}

void GateGantt::SetDisplayWindow(CTime opDisplayStart, CTime opDisplayEnd, BOOL bpFixedScaling)
{
    omDisplayStart = opDisplayStart;
    omDisplayEnd = opDisplayEnd;
    bmIsFixedScaling = bpFixedScaling;

    if (m_hWnd == NULL)
        imWindowWidth = 0;  // window is still not opened
    else
    {
        CRect rect;
        GetWindowRect(&rect);   // can't use client rect (vertical scroll may less its width)
        imWindowWidth = rect.Width();   // however, this is correct only if window has no border
        RepaintGanttChart();    // redraw the entire screen
    }
}

void GateGantt::SetDisplayStart(CTime opDisplayStart)
{
    if (bmIsFixedScaling)   // must shift both start/end time while keeping the old time span
        omDisplayEnd = opDisplayStart + (omDisplayEnd - omDisplayStart).GetTotalSeconds();
    omDisplayStart = opDisplayStart;
    RepaintGanttChart();    // redraw the entire screen
}

void GateGantt::SetCurrentTime(CTime opCurrentTime)
{                               
    RepaintGanttChart(-1, omCurrentTime, opCurrentTime);
    omCurrentTime = opCurrentTime;
}

void GateGantt::SetMarkTime(CTime opMarkTimeStart, CTime opMarkTimeEnd)
{
	// There are two reasons for these min/max.
	// First, we have to make sure that the start time parameter for RepaintGanttChart()
	// should not be greater than the end time parameter.
	// Second, we should not refresh markers in the vertical scale, since this will
	// disturb the repainting of the vertical scale when we are drawing these marker lines
	// together with the focus rectangle when moving/resizing a bar.
    RepaintGanttChart(-1,
		max(omDisplayStart, min(omMarkTimeStart, opMarkTimeStart)),
		max(omDisplayStart, max(omMarkTimeStart, opMarkTimeStart)));
    omMarkTimeStart = opMarkTimeStart;
    RepaintGanttChart(-1,
		max(omDisplayStart, min(omMarkTimeEnd, opMarkTimeEnd)),
		max(omDisplayStart, max(omMarkTimeEnd, opMarkTimeEnd)));
    omMarkTimeEnd = opMarkTimeEnd;
}

void GateGantt::RepaintVerticalScale(int ipLineno, BOOL bpErase)
{
    if (m_hWnd == NULL) // window hasn't been opened?
        return;

    CRect rcPaint;
    if (GetItemRect(ipLineno, &rcPaint) != LB_ERR)  // valid item in list box?
    {
        rcPaint.right = imVerticalScaleWidth - 1;
        InvalidateRect(&rcPaint, bpErase);
    }
}

void GateGantt::RepaintGanttChart(int ipLineno, CTime opStartTime, CTime opEndTime, BOOL bpErase)
{
    if (m_hWnd == NULL) // window hasn't been opened?
        return;

    CRect rcPaint;
    if (ipLineno == -1) // repaint the whole chart?
        GetClientRect(&rcPaint);
    else
        GetItemRect(ipLineno, &rcPaint);

    // Update the GanttChart body
    if (opStartTime <= omDisplayStart)
        rcPaint.left = imVerticalScaleWidth;
    else
        rcPaint.left = (int)GetX(opStartTime);

    // Use the client width if user want to repaint to the end of time
    if (opEndTime != TIMENULL && opEndTime <= omDisplayEnd)
        rcPaint.right = (int)GetX(opEndTime) + 1;

    InvalidateRect(&rcPaint, bpErase);
}

void GateGantt::RepaintItemHeight(int ipLineno)
{
    if (m_hWnd == NULL) // window hasn't been opened?
        return;

    // This repaint method will recalculate the item height
    SetItemHeight(ipLineno, GetLineHeight(ipLineno));

    CRect rcItem, rcPaint;
    GetClientRect(&rcPaint);
    GetItemRect(ipLineno, &rcItem);
    rcPaint.top = rcItem.top;
    InvalidateRect(&rcPaint, TRUE);
}


/////////////////////////////////////////////////////////////////////////////
// GateGantt implementation

void GateGantt::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
    // We have to define MeasureItem() here since MFC places an ASSERT inside
    // the default MeasureItem() of an owner-drawn CListBox.
}

void GateGantt::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// To let the GanttChart know automatically those changes in TimeScale.
// We had to reset the value of omDisplayStart and omDisplayEnd everytime.
	omDisplayStart = GetCTime(imVerticalScaleWidth);
	omDisplayEnd = GetCTime(imWindowWidth);
////////////////////////////////////////////////////////////////////////
    
	int itemID = lpDrawItemStruct->itemID;

    // Attention:
    // This optimization will the speed of displaying GanttChart very a lot.
    // However, this works because we hadn't display any selected or focus items.
    // (We use the "imCurrentItem" variable instead.)
    // So, be careful since this assumption may be changed in the future.
    //
    if (itemID == -1)
        return;
    if (!(lpDrawItemStruct->itemAction & ODA_DRAWENTIRE))
        return; // nothing more to do if listbox just change its selection and/or focus item

    // Drawing routines start here
    CDC dc;
    dc.Attach(lpDrawItemStruct->hDC);
    CRect rcItem(&lpDrawItemStruct->rcItem);
    CRect rcClip;
    dc.GetClipBox(&rcClip);

    // Draw vertical scale if necessary
    if (rcClip.left < imVerticalScaleWidth)
        DrawVerticalScale(&dc, itemID, rcItem);

	DrawDottedLines(&dc, itemID, rcItem);

    // Draw time lines and bars if necessary
    if (imVerticalScaleWidth <= rcClip.right)
    {
        CRgn rgn;
        CRect rect(imVerticalScaleWidth, rcItem.top, rcItem.right, rcItem.bottom);
        rgn.CreateRectRgnIndirect(&rect);
        dc.SelectClipRgn(&rgn);
        DrawTimeLines(&dc, rcItem.top, rcItem.bottom);
        DrawGanttChart(&dc, itemID, rcItem, rcClip);
        if (umResizeMode != HTNOWHERE)
            DrawFocusRect(&dc, &omRectResize);
        dc.SelectClipRgn(NULL);
    }

    dc.Detach();
}


/////////////////////////////////////////////////////////////////////////////
// GateGantt implementation helper functions

void GateGantt::DrawVerticalScale(CDC *pDC, int itemID, const CRect &rcItem)
{
    CPoint point;
    ::GetCursorPos(&point);
    ScreenToClient(&point);

    // Check if we need to change the highlight text in VerticalScale
    BOOL blHighlight;
    if (!bmIsMouseInWindow || !rcItem.PtInRect(point))
        blHighlight = FALSE;
    else
    {
        blHighlight = TRUE;
        if (imHighlightLine != itemID)
            RepaintVerticalScale(imHighlightLine, FALSE);
        imHighlightLine = itemID;
    }

    // Drawing routines start here
    CString s = pomViewer->GetLineText(imGroupno, itemID);
//	s += pomViewer->GetTmo(imGroupno, itemID);
    CFont *pOldFont = pDC->SelectObject(pomVerticalScaleFont);
    COLORREF nOldTextColor = pDC->SetTextColor(blHighlight?
        lmHighlightVerticalScaleTextColor: lmVerticalScaleTextColor);
    COLORREF nOldBackgroundColor = pDC->SetBkColor(blHighlight?
        lmHighlightVerticalScaleBackgroundColor: lmVerticalScaleBackgroundColor);
    CRect rect(rcItem);
    int left = rect.left + imVerticalScaleIndent;
	rect.right = imVerticalScaleWidth - 2;
    pDC->ExtTextOut(left, rect.top, ETO_CLIPPED | ETO_OPAQUE, &rect, s, lstrlen(s), NULL);
    pDC->SetBkColor(nOldBackgroundColor);
    pDC->SetTextColor(nOldTextColor);
    pDC->SelectObject(pOldFont);
}

void GateGantt::DrawGanttChart(CDC *pDC, int itemID, const CRect &rcItem, const CRect &rcClip)
{
	int ilPixel = 1;

    if (omDisplayStart >= omDisplayEnd) // no space to display?
        return;

	bool blDisplayArrivalMarker = pomViewer->DisplayMarker("ARRIVAL");
	bool blDisplayDepartureMarker = pomViewer->DisplayMarker("DEPARTURE");
	bool blDisplayTurnaroundMarker = pomViewer->DisplayMarker("TURNAROUND");

    // Draw each bar if it in the range of the clipped box
    for (int i = 0; i < pomViewer->GetBarCount(imGroupno, itemID); i++)
    {
        GATE_BARDATA *prlBar = pomViewer->GetBar(imGroupno, itemID, i);

   //    if (!IsOverlapped(prlBar->StartTime, prlBar->EndTime, omDisplayStart, omDisplayEnd))
   //         continue;   // skip bar which is out of time range

		if(ogGateIndexes.Chart == MS_SANS12)
		{
		    ilPixel = 2;
		}
		else
		{
			ilPixel = 1;
		}

		CTime olEnd = prlBar->EndTime;

        int left = (int)GetX(prlBar->StartTime);
        int right = (int)GetX(olEnd);
        int top = rcItem.top + imGutterHeight + (prlBar->OverlapLevel * imOverlapHeight);
        int bottom = top + imBarHeight;

        if (!IsOverlapped(left, right, rcClip.left, rcClip.right))
            continue;   // skip bar which is out of clipping region


		CCSPtrArray<BARDECO> olDecoData;  //For additional decoration
		CPoint olPoints[3];
		int ilLeft = (int)GetX(prlBar->StartTime);
		int ilRight = (int)GetX(prlBar->EndTime);
		int ilTop = rcItem.top + (prlBar->OverlapLevel * imOverlapHeight)+2;
		int ilBottom = ilTop + imBarHeight;

		if(	(blDisplayArrivalMarker && prlBar->FlightType == GATEBAR_ARRIVAL) ||
			(blDisplayTurnaroundMarker && prlBar->FlightType == GATEBAR_TURNAROUND))
		{
			// create a black background triangle slightly bigger than the coloured
			// triangle created below so that a blak diagonal line is displayed
			olPoints[0].x = ilLeft;
			olPoints[0].y = ilTop;

			olPoints[1].x = ilLeft; 
			olPoints[1].y = ilBottom;
			
			olPoints[2].x = ilLeft+(ilBottom-ilTop);
			olPoints[2].y = ilTop;

			BARDECO *prlDeco = new BARDECO;
			prlDeco->type = BD_REGION;
			prlDeco->Color = BLACK;

			CRgn *polRgn = new CRgn;
			polRgn->CreatePolygonRgn( olPoints, 3,  WINDING);
			prlDeco->Region = polRgn;

			olDecoData.Add(prlDeco);

			// left triangle
			olPoints[0].x = ilLeft+1;
			olPoints[0].y = ilTop+1;

			olPoints[1].x = ilLeft+1; 
			olPoints[1].y = ilBottom-2;
			
			olPoints[2].x = ilLeft+(ilBottom-ilTop-2);
			olPoints[2].y = ilTop+1;

			prlDeco = new BARDECO;
			prlDeco->type = BD_REGION;
			prlDeco->Color = pomViewer->GetColourForArrFlight(prlBar->FlightUrno);

			polRgn = new CRgn;
			polRgn->CreatePolygonRgn( olPoints, 3,  WINDING);
			prlDeco->Region = polRgn;

			olDecoData.Add(prlDeco);
		}
		if(	(blDisplayDepartureMarker && prlBar->FlightType == GATEBAR_DEPARTURE) ||
			(blDisplayTurnaroundMarker && prlBar->FlightType == GATEBAR_TURNAROUND))
		{
			ilRight++;
			// create a black background triangle slightly bigger than the coloured
			// triangle created below so that a blak diagonal line is displayed
			olPoints[0].x = ilRight;
			olPoints[0].y = ilTop;

			olPoints[1].x = ilRight-(ilBottom-ilTop); 
			olPoints[1].y = ilBottom;
			
			olPoints[2].x = ilRight;
			olPoints[2].y = ilBottom;

			BARDECO *prlDeco = new BARDECO;
			prlDeco->type = BD_REGION;
			prlDeco->Color = BLACK;

			CRgn *polRgn = new CRgn;
			polRgn->CreatePolygonRgn( olPoints, 3,  WINDING);
			prlDeco->Region = polRgn;

			olDecoData.Add(prlDeco);

			// right triangle
			olPoints[0].x = ilRight-1;
			olPoints[0].y = ilTop+2;

			olPoints[1].x = ilRight-(ilBottom-ilTop-2); 
			olPoints[1].y = ilBottom-1;
			
			olPoints[2].x = ilRight-1;
			olPoints[2].y = ilBottom-1;

			prlDeco = new BARDECO;
			prlDeco->type = BD_REGION;
			prlDeco->Color = pomViewer->GetColourForDepFlight(prlBar->FlightUrno);

			polRgn = new CRgn;
			polRgn->CreatePolygonRgn( olPoints, 3,  WINDING);
			prlDeco->Region = polRgn;

			olDecoData.Add(prlDeco);
		}
			
			
		GanttBar paintbar(pDC, CRect(left, top, right, bottom),
			prlBar->FrameType, prlBar->MarkerType, prlBar->MarkerBrush,
			prlBar->Text, pomGanttChartFont, lmGanttChartTextColor, ilPixel,
			FALSE, CRect(0,0,0,0), NULL, &olDecoData,TA_CENTER,NULL,0,prlBar->FrameColor);

		int ilNumDeco = olDecoData.GetSize();
		for(int ilDeco = 0; ilDeco < ilNumDeco; ilDeco++)
		{
			delete olDecoData[ilDeco].Region;
		}
		olDecoData.DeleteAll();
    }
}

void GateGantt::DrawTimeLines(CDC *pDC, int top, int bottom)
{
    // Draw current time
    if (omCurrentTime != TIMENULL && IsBetween(omCurrentTime, omDisplayStart, omDisplayEnd))
    {
        CPen penRed(PS_SOLID, 0, RGB(255, 0, 0));
        CPen *pOldPen = pDC->SelectObject(&penRed);
        int x = (int)GetX(omCurrentTime);
        pDC->MoveTo(x, top);
        pDC->LineTo(x, bottom);
        pDC->SelectObject(pOldPen);
    }

    // Draw marked time start
    if (omMarkTimeStart != TIMENULL && IsBetween(omMarkTimeStart, omDisplayStart, omDisplayEnd))
    {
        CPen penYellow(PS_SOLID, 0, RGB(255, 255, 0));
        CPen *pOldPen = pDC->SelectObject(&penYellow);
        int x = (int)GetX(omMarkTimeStart);
        pDC->MoveTo(x, top);
        pDC->LineTo(x, bottom);
        pDC->SelectObject(pOldPen);
    }

    // Draw marked time end
    if (omMarkTimeEnd != TIMENULL && IsBetween(omMarkTimeEnd, omDisplayStart, omDisplayEnd))
    {
        CPen penYellow(PS_SOLID, 0, RGB(255, 255, 0));
        CPen *pOldPen = pDC->SelectObject(&penYellow);
        int x = (int)GetX(omMarkTimeEnd);
        pDC->MoveTo(x, top);
        pDC->LineTo(x, bottom);
        pDC->SelectObject(pOldPen);
    }
}

void GateGantt::DrawFocusRect(CDC *pDC, const CRect &rect)
{
	CRect rcFocus = rect;
	rcFocus.right++;	// extra pixel to help DrawFocusRect() work correctly
	pDC->DrawFocusRect(&rcFocus);

	// Before finish: please insert the correct code for status bar while moving/resizing
    if (pomStatusBar != NULL)
	{
		CString s = GetCTime(rcFocus.left).Format("%H%M") + " - "
			+ GetCTime(rcFocus.right - 1).Format("%H%M");
		pomStatusBar->SetPaneText(0, s);
	}
}


/////////////////////////////////////////////////////////////////////////////
// GateGantt message handlers

BEGIN_MESSAGE_MAP(GateGantt, CWnd)
    //{{AFX_MSG_MAP(GateGantt)
	ON_WM_CREATE()
	ON_WM_DESTROY()
    ON_WM_SIZE()
    ON_WM_ERASEBKGND()
	ON_WM_SETCURSOR()
    ON_WM_MOUSEMOVE()
    ON_WM_TIMER()
    ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONDOWN()
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
    ON_MESSAGE(WM_USERKEYDOWN, OnUserKeyDown)
    ON_MESSAGE(WM_USERKEYUP, OnUserKeyUp)
	ON_COMMAND(GG_EDITGATE_MENUITEM, OnMenuGateWorkOn)
	ON_COMMAND(GG_AVAILEMPS_MENUITEM, OnMenuFreeEmployees)
	ON_COMMAND(GG_ENDJOB_MENUITEM, OnMenuFlightEnd)
	ON_COMMAND(GG_INFORMJOB_MENUITEM, OnMenuFlightInform)
	ON_COMMAND(GG_CONFIRMJOB_MENUITEM, OnMenuFlightConfirm)
    ON_COMMAND(GG_EDITFLIGHT_MENUITEM, OnMenuFlightWorkOn)
	ON_COMMAND_RANGE(GG_ACCEPT_ALL_CONFLICTS, (GG_ACCEPT_ALL_CONFLICTS + GG_MAXNUMCONFLICTS), OnMenuFlightAcceptConflict)
	ON_COMMAND(GG_CHECKCONFLICT_MENUITEM, OnMenuCheckConflict)
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

int GateGantt::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
    m_DragDropTarget.RegisterTarget(this, this);
	
	return 0;
}

void GateGantt::OnDestroy() 
{
	if (bgModal == TRUE)
		return;
	m_DragDropTarget.Revoke();
	CWnd::OnDestroy();
}

void GateGantt::OnSize(UINT nType, int cx, int cy)
{
    CListBox::OnSize(nType, cx, cy);

    CRect rect;
    GetWindowRect(&rect);   // don't use cx (vertical scroll may less the width of client rect)

    if (bmIsFixedScaling)   // fixed-scaling mode?
    {
        if (imWindowWidth == 0) // first time initialization?
            imWindowWidth = rect.Width();
        omDisplayEnd = GetCTime(rect.Width());   // has to recalculate the right most boundary?
    }
    else    // must be automatic variable-scaling mode
    {
        if (rect.Width() != imWindowWidth) // display window changed?
            InvalidateRect(NULL);   // repaint whole GanttChart
    }
    imWindowWidth = rect.Width();
}

// Attention:
// Be careful, this may not work if you try to develop horizontal-scrolling in GanttChart.
// Generally, if the ListBox need to be repainted, it will send the message WM_ERASEBKGND.
// But if you allow horizontal-scrolling, WM_ERASEBKGND will be called before the new
// horizontal position could be detected by dc.GetWindowOrg().x in DrawItem().
// However, this version work fine since there's no such scrolling.
// Then, we can assume that the beginning offset will be 0 all the time.
//
BOOL GateGantt::OnEraseBkgnd(CDC* pDC)
{
    CRect rectErase;
    pDC->GetClipBox(&rectErase);

    // Draw the VerticalScale if the user specify it to be displayed
    if (imVerticalScaleWidth > 0)
    {
        CBrush brush(lmVerticalScaleBackgroundColor);
        CRect rect(0, rectErase.top, imVerticalScaleWidth, rectErase.bottom);
        pDC->FillRect(&rect, &brush);

        // Draw seperator line between VerticalScale and the body of GanttChart
        CPen penBlack(PS_SOLID, 0, ::GetSysColor(COLOR_WINDOWFRAME));
        CPen *pOldPen = pDC->SelectObject(&penBlack);
        pDC->MoveTo(imVerticalScaleWidth-2, rectErase.top);
        pDC->LineTo(imVerticalScaleWidth-2, rectErase.bottom);
        CPen penWhite(PS_SOLID, 0, ::GetSysColor(COLOR_BTNHIGHLIGHT));
        pDC->SelectObject(&penWhite);
        pDC->MoveTo(imVerticalScaleWidth-1, rectErase.top);
        pDC->LineTo(imVerticalScaleWidth-1, rectErase.bottom);
        pDC->SelectObject(pOldPen);
    }

    // Draw the background of the body of GanttChart
    CBrush brush(lmGanttChartBackgroundColor);
    CRect rect(imVerticalScaleWidth, rectErase.top, rectErase.right, rectErase.bottom);
    pDC->FillRect(&rect, &brush);

    // Draw the vertical current time and marked time lines
    DrawTimeLines(pDC, rectErase.top, rectErase.bottom);

    // Tell Windows there's nothing more to do
    return TRUE;
}

BOOL GateGantt::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	// Id 2-Oct-96
	// This override cursor setting will remove cursor flickering when the user press
	// control key but still not begin the moving/resizing yet.
	//
    if (bmIsControlKeyDown && imHighlightLine != -1 && imCurrentBar != -1)
		return TRUE;	// override the default processing

	return CWnd::OnSetCursor(pWnd, nHitTest, message);
}

void GateGantt::OnMouseMove(UINT nFlags, CPoint point)
{
    if (umResizeMode != HTNOWHERE)  // in SetCapture()?
    {
        OnMovingOrResizing(point, nFlags & MK_LBUTTON);
        return;
    }

    CListBox::OnMouseMove(nFlags, point);
    UpdateGanttChart(point, nFlags & MK_CONTROL);
}

void GateGantt::OnTimer(UINT nIDEvent)
{
	if(::IsWindow(this->GetSafeHwnd()))
	{
		if (umResizeMode != HTNOWHERE)  // in SetCapture()?
			return;

		if (nIDEvent == 0)  // the last timer message, clear everything
		{
			bmIsMouseInWindow = FALSE;
			RepaintVerticalScale(imHighlightLine, FALSE);
			imHighlightLine = -1;       // no more selected line
			UpdateBarStatus(-1, -1);    // no more status if mouse is outside of GanttChart
			return;
		}

		CPoint point;
		::GetCursorPos(&point);
		ScreenToClient(&point);    
		UpdateGanttChart(point, ::GetKeyState(VK_CONTROL) & 0x8080);
			// This statement has to be fixed for using both in Windows 3.11 and NT.
			// In Windows 3.11 the bit 0x80 will be turned on if control key is pressed.
			// In Windows NT, the bit 0x8000 will be turned on if control key is pressed.
	}
	else
	{
		ASSERT(0);
	}
}

void GateGantt::OnLButtonDown(UINT nFlags, CPoint point)
{
	// Id 1-Oct-96
	// This will help us create a virtual double click on a double click after SetCursorPos().
	CPoint olLastClickedPosition = omLastClickedPosition;
	omLastClickedPosition = point;
	if (olLastClickedPosition == point)
	{
		OnLButtonDblClk(nFlags, point);
		omLastClickedPosition = CPoint(-1, -1);
		return;
	}

    // Check if the user starts moving/resizing mouse action
    if ((nFlags & MK_LBUTTON) && (nFlags & MK_CONTROL) &&
        BeginMovingOrResizing(point))
        return; // not call the default after SetCapture()

	// Check dragging from the vertical scale (dragging DutyBar)
    imHighlightLine = GetItemFromPoint(point);
	if (IsBetween(point.x, 0, imVerticalScaleWidth-2) && (imHighlightLine != -1))	// from VerticalScale?
	{
		DragGateBegin(imHighlightLine);
		return;	// not call the default after dragging;
	}

    // Bring bar to front / Send bar to back
    imCurrentBar = GetBarnoFromPoint(imHighlightLine, point);
    if (imCurrentBar != -1)
    {
		// Update the time band (two vertical yellow lines)
		GATE_BARDATA *prlBar = pomViewer->GetBar(imGroupno, imHighlightLine, imCurrentBar);
		TIMEPACKET olTimePacket;
		DEMANDDATA *prlDem = ogBasicData.GetFirstDemandWithoutJobByFlur(prlBar->FlightUrno,ALLOCUNITTYPE_GATE,prlBar->ReturnFlightType);
		if(prlBar->DepartureUrno > 0)
		{
			// for turnarounds check both halves of the rotation for the earliest demand
			DEMANDDATA *prlRotationDem = ogBasicData.GetFirstDemandWithoutJobByFlur(prlBar->DepartureUrno,ALLOCUNITTYPE_GATE,prlBar->DepReturnFlightType);
			if(prlRotationDem != NULL)
			{
				if(prlDem == NULL || prlRotationDem->Debe < prlDem->Debe)
				{
					prlDem = prlRotationDem;
				}
			}
		}
		if (prlDem)
		{
			olTimePacket.StartTime = prlDem->Debe;
			olTimePacket.EndTime = prlDem->Deen;
		}
		else
		{
			olTimePacket.StartTime = prlBar->StartTime;
			olTimePacket.EndTime = prlBar->EndTime;
		}

		ogCCSDdx.DataChanged((void *)this, STAFFDIAGRAM_UPDATETIMEBAND, &olTimePacket);

		// Bring bar to front / Send bar to back
		int ilOldOverlapLevel = prlBar->OverlapLevel;
		int ilOldLineHeight = GetLineHeight(imHighlightLine);
        GATE_BARDATA rlBar = *prlBar;	// copy all data of this bar
		rlBar.Indicators.RemoveAll();	// remove indicators, will be dealloc in DeleteBar()
		for (int i = 0; i < prlBar->Indicators.GetSize(); i++)
			rlBar.Indicators.NewAt(i, prlBar->Indicators[i]);
        pomViewer->DeleteBar(imGroupno, imHighlightLine, imCurrentBar);
		BOOL blBringBarToFront = !(nFlags & MK_SHIFT);
        imCurrentBar = pomViewer->CreateBar(imGroupno, imHighlightLine, &rlBar, blBringBarToFront);
			// unnecessary to delete the NewAt() bars since create bar automatic copy it

		if(pomViewer->IsGroupWithoutGates(imGroupno))
		{
			// the bars in each line in the "without gates" group, needs to be sorted
			// otherwise they overlap. Clicking a bar causes it to be deleted and recreated.
			// so its line needs to be resorted
			imCurrentBar = pomViewer->SortLineOfGroupWithoutGates(imHighlightLine, rlBar.FlightUrno);
		}
		else if(pomViewer->IsAllFlightsGroup(imGroupno))
		{
			// the bars in each line in the "all flights" group, needs to be sorted
			// otherwise they overlap. Clicking a bar causes it to be deleted and recreated.
			// so its line needs to be resorted
			imCurrentBar = pomViewer->SortLineOfGroupAllFlights(imHighlightLine, rlBar.FlightUrno);
		}
		//Singapore
		else
		{
			imCurrentBar = pomViewer->SortLineOfGroupFlights(imGroupno,imHighlightLine, rlBar.FlightUrno);
		}

		// Update the display, also check if line height was changed
		if (GetLineHeight(imHighlightLine) == ilOldLineHeight)
		{
			RepaintGanttChart(imHighlightLine, TIMENULL, TIMENULL, TRUE);
		}
		else
		{
			//RepaintItemHeight(imHighlightLine);
			LONG lParam = MAKELONG(imHighlightLine, imGroupno);
			pomViewer->pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM, UD_UPDATELINEHEIGHT, lParam);
		}

		// Id 22-Sep-96
		// To make drag-and-drop operation works properly, we have to change the position
		// of the mouse cursor also, since the drag-and-drop will use the current position
		// as a basis for selecting a bar.
		prlBar = pomViewer->GetBar(imGroupno, imHighlightLine, imCurrentBar);
			// reload the bar again (it may be already moved to some other position)
		int ilNewOverlapLevel = prlBar->OverlapLevel;
		point.Offset(0, (ilNewOverlapLevel - ilOldOverlapLevel) * imOverlapHeight);
	}

    CListBox::OnLButtonDown(nFlags, point);

	// Update the cursor position to make sure that the cursor will always be over
	// the bar we just click on.
	CPoint olMousePosition = point;
	ClientToScreen(&olMousePosition);
	::SetCursorPos(olMousePosition.x, olMousePosition.y);
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	// Id 1-Oct-96
	// This will help us create a virtual double click on a double click after SetCursorPos().
	omLastClickedPosition = point;

	// Checking for dragging a job bar?
	// Notes: Here's the best place for checking for dragging from job bar.
	// We cannot simply regard OnLButtonDown() as the beginning of dragging,
	// since we defined OnLButtonDown() for bringing bar to front or to back.
	//
	// Id 25-Sep-96
	// All of the above are wrong. Try to move it back and you will see.
	// If we place this in OnMouseMove(), we can initiate the drag operation by
	// holding on the left mouse from some place which does not allow dragging,
	// then move the mouse over some bars, the drag operation will start when the
	// cursor is on some bar. This is weird and should not happened.
	if ((nFlags & MK_LBUTTON) &&
		imHighlightLine != -1 && imCurrentBar != -1)
	{
		DragFlightBarBegin(imHighlightLine, imCurrentBar);
		return;
	}
}

void GateGantt::OnLButtonUp(UINT nFlags, CPoint point) 
{
    if (umResizeMode != HTNOWHERE)  // in SetCapture()?
    {
		// This allow a fast way for terminating moving or resizing mode
        OnMovingOrResizing(point, nFlags & MK_LBUTTON);
        return;
    }

	CListBox::OnLButtonUp(nFlags, point);
}

void GateGantt::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
    // Check if the user starts moving/resizing mouse action
    if ((nFlags & MK_LBUTTON) && (nFlags & MK_CONTROL) &&
        BeginMovingOrResizing(point))
        return; // not call the default after SetCapture()

    // Pop-up detailed window
    if ((imHighlightLine != -1) && (point.x < imVerticalScaleWidth))	// vertical scale?
	{
		CString s = pomViewer->GetLineText(imGroupno, imHighlightLine);
		char clBuf[255]; strcpy(clBuf, s);
		BOOL blArrival=true, blDeparture=true;
		CWnd *polWnd = GetParent()->GetParent()->GetParent();
		new GateDetailWindow(polWnd,(const char *) clBuf, blArrival, blDeparture, omDisplayStart, omDisplayEnd);
    }
	if ((imHighlightLine != -1) && (imCurrentBar != -1) && (point.x >= imVerticalScaleWidth))	// bar?
    {
		GATE_BARDATA *prlBar = pomViewer->GetBar(imGroupno, imHighlightLine, imCurrentBar);
		if (prlBar->Type == BAR_FLIGHT)
		{
			long llArrUrno = (prlBar->FlightType == GATEBAR_DEPARTURE) ? 0L :  prlBar->FlightUrno;
			long llDepUrno = (prlBar->FlightType == GATEBAR_DEPARTURE) ? prlBar->FlightUrno :  prlBar->DepartureUrno;
			new FlightDetailWindow(this,llArrUrno,llDepUrno,prlBar->IsShadowBar,ALLOCUNITTYPE_GATE, NULL, PERSONNELDEMANDS, prlBar->StartTime);

			TIMEPACKET olTimePacket;
			olTimePacket.StartTime = omMarkTimeStart;
			olTimePacket.EndTime = omMarkTimeEnd;
			ogCCSDdx.DataChanged((void *)this, STAFFDIAGRAM_UPDATETIMEBAND, &olTimePacket);
		}
		else
			if (prlBar->Type == BAR_SPECIAL)
		{
			JOBDATA *prlJob = ogJobData.GetJobByUrno(prlBar->FlightUrno);
			if (prlJob != NULL)
			{
				new StaffDetailWindow(this,prlJob->Jour);
			}
		}
    }
}

void GateGantt::OnRButtonDown(UINT nFlags, CPoint point) 
{
    if ((nFlags & MK_LBUTTON) && (nFlags & MK_CONTROL) &&
        BeginMovingOrResizing(point))
        return; // not call the default after SetCapture()

	int itemID, ilBarno;
	if ((itemID = GetItemFromPoint(point)) == -1)
		return;	// not click in the Gantt window

	if (point.x < imVerticalScaleWidth - 2)	// in VerticalScale?
	{
		CMenu menu;
		menu.CreatePopupMenu();
		for (int i = menu.GetMenuItemCount(); i > 0; i--)
	        menu.RemoveMenu(i, MF_BYPOSITION);
	    //menu.AppendMenu(MF_STRING,11, "Bearbeiten");	// work on
	    menu.AppendMenu(MF_STRING,GG_EDITGATE_MENUITEM, GetString(IDS_STRING61352));	// work on

		ClientToScreen(&point);
		imContextItem = itemID;
        menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);
		return;
	}

	if ((ilBarno = GetBarnoFromPoint(itemID, point)) != -1)	// on a bar
	{
        GATE_BARDATA *prlBar = pomViewer->GetBar(imGroupno, itemID, ilBarno);
		bmContextBarSet = TRUE;	// Save bar data for handling after context menu closed
		rmContextBar = *prlBar;	// copy all data of this bar
		imContextItem = itemID;
		imContextBarNo = ilBarno;

		CMenu menu;
		menu.CreatePopupMenu();
		for (int i = menu.GetMenuItemCount(); i > 0; i--)
	        menu.RemoveMenu(i, MF_BYPOSITION);

		menu.AppendMenu(MF_STRING,GG_INFORMJOB_MENUITEM, GetString(IDS_INFORMJOB)); // Employee Informed
		menu.AppendMenu(MF_STRING,GG_CONFIRMJOB_MENUITEM, GetString(IDS_STRING61353));	// confirm job
		menu.AppendMenu(MF_STRING,GG_ENDJOB_MENUITEM, GetString(IDS_STRING61354));// Beenden
		menu.AppendMenu(MF_STRING,GG_EDITFLIGHT_MENUITEM, GetString(IDS_STRING61352));	// work on
		menu.AppendMenu(MF_STRING,GG_AVAILEMPS_MENUITEM, GetString(IDS_STRING61376));	// to find another free employee

		if (prlBar->Type == BAR_FLIGHT)
		{
			CString olFnum, olRotationFnum;
			bool blHasPlannedJobs = false, blHasStartedJobs = false, blAllEmpsInformed = true;
			FLIGHTDATA *prlFlight;
			if ((prlFlight = ogFlightData.GetFlightByUrno(prlBar->FlightUrno)) != NULL)
			{
				FLIGHTDATA *prlRotation = ogFlightData.GetFlightByUrno(rmContextBar.DepartureUrno);
				if(prlRotation != NULL)
				{
					olRotationFnum = prlRotation->Fnum;
				}

				olFnum = prlFlight->Fnum;

				CCSPtrArray<JOBDATA>  olJobs;
				bool blGetJobsForRotation = (prlRotation != NULL && prlBar->FlightType == PSTBAR_TURNAROUND);
				ogJobData.GetJobsByFlur(olJobs,prlBar->FlightUrno, FALSE, "",ALLOCUNITTYPE_GATE,PERSONNELDEMANDS,blGetJobsForRotation,prlBar->ReturnFlightType);
//				if (prlRotation != NULL && prlBar->FlightType == GATEBAR_TURNAROUND)
//				{
//					ogJobData.GetJobsByFlur(olJobs,prlRotation->Urno,FALSE,"",ALLOCUNITTYPE_GATE); // don't like JOBFM
//				}
				int ilNumJobs = olJobs.GetSize();
				for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
				{
					JOBDATA *prlJob = &olJobs[ilJ];
					if(*prlJob->Stat == 'P')
					{
						blHasPlannedJobs = true;
					}
					else if(*prlJob->Stat == 'C')
					{
						blHasStartedJobs = true;
					}
					if(!prlJob->Infm)
					{
						blAllEmpsInformed = false;
					}
				}

				CString olAloc = ALLOCUNITTYPE_GATE;
				CString olAlid = ""; // don't get the gate because one half of a turnaround may have different gate to the other half and son not all the demands are loaded
				CCSPtrArray <DEMANDDATA> olDemands;
				ogDataSet.GetOpenDemandsForFlight(prlFlight, prlRotation, olDemands, TIMENULL, TIMENULL, olAlid, olAloc, PERSONNELDEMANDS, rmContextBar.ReturnFlightType);
				if(olDemands.GetSize() <= 0)
				{
					// no open demands for available emps 
					menu.EnableMenuItem(GG_AVAILEMPS_MENUITEM, MF_BYCOMMAND || MF_GRAYED);
				}
			}

			if(!blHasPlannedJobs)
			{
				menu.EnableMenuItem(GG_INFORMJOB_MENUITEM,MF_BYCOMMAND || MF_GRAYED);
				menu.EnableMenuItem(GG_CONFIRMJOB_MENUITEM,MF_BYCOMMAND || MF_GRAYED);
			}
			else if(blAllEmpsInformed)
			{
				menu.CheckMenuItem(GG_INFORMJOB_MENUITEM, MF_CHECKED);
			}

			if(!blHasStartedJobs)
			{
				menu.EnableMenuItem(GG_ENDJOB_MENUITEM,MF_BYCOMMAND || MF_GRAYED);
			}

			omAcceptConflictsMap.RemoveAll();
			omAcceptDepConflictsMap.RemoveAll();
			omAcceptJobConflictsMap.RemoveAll();
			omConflictToJobMap.RemoveAll();

			int ilNumUnacceptedConflicts = 0;
			int ilConflNum = GG_ACCEPT_ALL_CONFLICTS + 1;
			CString olText;
			CCSPtrArray <CONFLICTDATA> olConflicts;
			ogConflicts.GetFlightConflicts(olConflicts,prlBar->FlightUrno,ALLOCUNITTYPE_GATE,PERSONNELDEMANDS,prlBar->ReturnFlightType);
			int ilNumConflicts = olConflicts.GetSize();
			if(ilNumConflicts > 0)
			{
				for(int ilConfl = 0; ilConfl < ilNumConflicts; ilConfl++)
				{
					//menu.AppendMenu(MF_STRING,25, "Konflikt akzeptieren");	// accept conflict
					if(prlBar->DepartureUrno > 0 && !olFnum.IsEmpty())
					{
						olText.Format("%s: %s %s",GetString(IDS_STRING61357),olFnum,ogConflicts.GetConflictTextByType(olConflicts[ilConfl].Type));
					}
					else
					{
						olText.Format("%s: %s",GetString(IDS_STRING61357),ogConflicts.GetConflictTextByType(olConflicts[ilConfl].Type));
					}
					olText += CString(" ") + ogConflicts.GetAdditionalConflictText(olConflicts[ilConfl].Type,prlFlight,NULL,ALLOCUNITTYPE_GATE);
					if(olConflicts[ilConfl].Confirmed)
					{
						menu.AppendMenu(MF_STRING|MF_GRAYED, ilConflNum, olText);
					}
					else
					{
						menu.AppendMenu(MF_STRING, ilConflNum, olText);
						ilNumUnacceptedConflicts++;
					}
					omAcceptConflictsMap.SetAt((void *) ilConflNum, &olConflicts[ilConfl]);
					ilConflNum++;
				}
				olConflicts.RemoveAll();
			}

			CCSPtrArray <JOBDATA> olJobs;
			JOBDATA *prlJob = NULL;
			CString olEmp;
			int ilNumJobs = 0;

			ogJobData.GetJobsByFlur(olJobs,prlBar->FlightUrno,FALSE,"",ALLOCUNITTYPE_GATE,PERSONNELDEMANDS,false,prlBar->ReturnFlightType);
			ilNumJobs = olJobs.GetSize();
			for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
			{
				prlJob = &olJobs[ilJob];
				ogConflicts.GetJobConflicts(olConflicts, prlJob->Urno);
				int ilNumJobConflicts = olConflicts.GetSize();
				for(int ilConfl = 0; ilConfl < ilNumJobConflicts; ilConfl++)
				{
					if(olConflicts[ilConfl].Type == CFI_SHIFTNOTSTARTED)
					{
						EMPDATA *prlEmp = ogEmpData.GetEmpByUrno(prlJob->Ustf);
						if(prlEmp != NULL)
						{
							olEmp = ogEmpData.GetEmpName(prlEmp);
						}
						olText.Format("%s: %s %s  %s  %s-%s",GetString(IDS_STRING61357),olFnum,ogConflicts.GetConflictTextByType(olConflicts[ilConfl].Type),olEmp,prlJob->Acfr.Format("%H:%M"),prlJob->Acto.Format("%H:%M"));
						if(olConflicts[ilConfl].Confirmed)
						{
							menu.AppendMenu(MF_STRING|MF_GRAYED, ilConflNum, olText);
						}
						else
						{
							menu.AppendMenu(MF_STRING, ilConflNum, olText);
							ilNumUnacceptedConflicts++;
							omAcceptJobConflictsMap.SetAt((void *) ilConflNum, &olConflicts[ilConfl]);
							omConflictToJobMap.SetAt((void *) ilConflNum, prlJob);
						}
						ilConflNum++;
//						break;
					}
					//Singapore
					if(ogBasicData.IsPaxServiceT2() == true && olConflicts[ilConfl].Type == CFI_JOBFROMT3TOT2EMPL)
					{
						EMPDATA *prlEmp = ogEmpData.GetEmpByUrno(prlJob->Ustf);
						if(prlEmp != NULL)
						{
							olEmp = ogEmpData.GetEmpName(prlEmp);
						}
						olText.Format("%s: %s %s  %s  %s-%s",GetString(IDS_STRING61357),olFnum,ogConflicts.GetConflictTextByType(olConflicts[ilConfl].Type),olEmp,prlJob->Acfr.Format("%H:%M"),prlJob->Acto.Format("%H:%M"));
						if(olConflicts[ilConfl].Confirmed)
						{
							menu.AppendMenu(MF_STRING|MF_GRAYED, ilConflNum, olText);
						}
						else
						{
							menu.AppendMenu(MF_STRING, ilConflNum, olText);
							ilNumUnacceptedConflicts++;
							omAcceptJobConflictsMap.SetAt((void *) ilConflNum, &olConflicts[ilConfl]);
							omConflictToJobMap.SetAt((void *) ilConflNum, prlJob);
						}
						ilConflNum++;
					}
					//Singapore
					if(ogBasicData.IsPaxServiceT2() == true && olConflicts[ilConfl].Type == CFI_JOBFROMT2TOT3EMPL)
					{
						EMPDATA *prlEmp = ogEmpData.GetEmpByUrno(prlJob->Ustf);
						if(prlEmp != NULL)
						{
							olEmp = ogEmpData.GetEmpName(prlEmp);
						}
						olText.Format("%s: %s %s  %s  %s-%s",GetString(IDS_STRING61357),olFnum,ogConflicts.GetConflictTextByType(olConflicts[ilConfl].Type),olEmp,prlJob->Acfr.Format("%H:%M"),prlJob->Acto.Format("%H:%M"));
						if(olConflicts[ilConfl].Confirmed)
						{
							menu.AppendMenu(MF_STRING|MF_GRAYED, ilConflNum, olText);
						}
						else
						{
							menu.AppendMenu(MF_STRING, ilConflNum, olText);
							ilNumUnacceptedConflicts++;
							omAcceptJobConflictsMap.SetAt((void *) ilConflNum, &olConflicts[ilConfl]);
							omConflictToJobMap.SetAt((void *) ilConflNum, prlJob);
						}
						ilConflNum++;
					}
				}
			}

			if(prlBar->DepartureUrno > 0)
			{
				// this is a turaround bar
				ogConflicts.GetFlightConflicts(olConflicts,prlBar->DepartureUrno,ALLOCUNITTYPE_GATE,PERSONNELDEMANDS,prlBar->DepReturnFlightType);
				ilNumConflicts = olConflicts.GetSize();
				if(ilNumConflicts > 0)
				{
					for(int ilConfl = 0; ilConfl < ilNumConflicts; ilConfl++)
					{
						//menu.AppendMenu(MF_STRING,25, "Konflikt akzeptieren");	// accept conflict
						olText.Format("%s: %s %s",GetString(IDS_STRING61357),olRotationFnum,ogConflicts.GetConflictTextByType(olConflicts[ilConfl].Type));
						olText += CString(" ") + ogConflicts.GetAdditionalConflictText(olConflicts[ilConfl].Type,prlFlight,NULL,ALLOCUNITTYPE_GATE);
						if(olConflicts[ilConfl].Confirmed)
						{
							menu.AppendMenu(MF_STRING|MF_GRAYED, ilConflNum, olText);
						}
						else
						{
							menu.AppendMenu(MF_STRING, ilConflNum, olText);
							ilNumUnacceptedConflicts++;
						}
						omAcceptDepConflictsMap.SetAt((void *) ilConflNum, &olConflicts[ilConfl]);
						ilConflNum++;
					}
					olConflicts.RemoveAll();
				}

				olJobs.RemoveAll();
				ogJobData.GetJobsByFlur(olJobs,prlBar->DepartureUrno,FALSE,"",ALLOCUNITTYPE_GATE,PERSONNELDEMANDS, false, prlBar->DepReturnFlightType);
				int ilNumJobs = olJobs.GetSize();
				for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
				{
					prlJob = &olJobs[ilJob];
					ogConflicts.GetJobConflicts(olConflicts, prlJob->Urno);
					int ilNumJobConflicts = olConflicts.GetSize();
					for(int ilConfl = 0; ilConfl < ilNumJobConflicts; ilConfl++)
					{
						if(olConflicts[ilConfl].Type == CFI_SHIFTNOTSTARTED)
						{
							EMPDATA *prlEmp = ogEmpData.GetEmpByUrno(prlJob->Ustf);
							if(prlEmp != NULL)
							{
								olEmp = ogEmpData.GetEmpName(prlEmp);
							}
							olText.Format("%s: %s %s  %s  %s-%s",GetString(IDS_STRING61357),olRotationFnum,ogConflicts.GetConflictTextByType(olConflicts[ilConfl].Type),olEmp,prlJob->Acfr.Format("%H:%M"),prlJob->Acto.Format("%H:%M"));
							if(olConflicts[ilConfl].Confirmed)
							{
								menu.AppendMenu(MF_STRING|MF_GRAYED, ilConflNum, olText);
							}
							else
							{
								menu.AppendMenu(MF_STRING, ilConflNum, olText);
								ilNumUnacceptedConflicts++;
								omAcceptJobConflictsMap.SetAt((void *) ilConflNum, &olConflicts[ilConfl]);
								omConflictToJobMap.SetAt((void *) ilConflNum, prlJob);
							}
							ilConflNum++;
//							break;
						}
						//Singapore
						if(ogBasicData.IsPaxServiceT2() == true && olConflicts[ilConfl].Type == CFI_JOBFROMT3TOT2EMPL)
						{
							EMPDATA *prlEmp = ogEmpData.GetEmpByUrno(prlJob->Ustf);
							if(prlEmp != NULL)
							{
								olEmp = ogEmpData.GetEmpName(prlEmp);
							}
							olText.Format("%s: %s %s  %s  %s-%s",GetString(IDS_STRING61357),olRotationFnum,ogConflicts.GetConflictTextByType(olConflicts[ilConfl].Type),olEmp,prlJob->Acfr.Format("%H:%M"),prlJob->Acto.Format("%H:%M"));
							if(olConflicts[ilConfl].Confirmed)
							{
								menu.AppendMenu(MF_STRING|MF_GRAYED, ilConflNum, olText);
							}
							else
							{
								menu.AppendMenu(MF_STRING, ilConflNum, olText);
								ilNumUnacceptedConflicts++;
								omAcceptJobConflictsMap.SetAt((void *) ilConflNum, &olConflicts[ilConfl]);
								omConflictToJobMap.SetAt((void *) ilConflNum, prlJob);
							}
							ilConflNum++;
						}
						//Singapore
						if(ogBasicData.IsPaxServiceT2() == true && olConflicts[ilConfl].Type == CFI_JOBFROMT2TOT3EMPL)
						{
							EMPDATA *prlEmp = ogEmpData.GetEmpByUrno(prlJob->Ustf);
							if(prlEmp != NULL)
							{
								olEmp = ogEmpData.GetEmpName(prlEmp);
							}
							olText.Format("%s: %s %s  %s  %s-%s",GetString(IDS_STRING61357),olRotationFnum,ogConflicts.GetConflictTextByType(olConflicts[ilConfl].Type),olEmp,prlJob->Acfr.Format("%H:%M"),prlJob->Acto.Format("%H:%M"));
							if(olConflicts[ilConfl].Confirmed)
							{
								menu.AppendMenu(MF_STRING|MF_GRAYED, ilConflNum, olText);
							}
							else
							{
								menu.AppendMenu(MF_STRING, ilConflNum, olText);
								ilNumUnacceptedConflicts++;
								omAcceptJobConflictsMap.SetAt((void *) ilConflNum, &olConflicts[ilConfl]);
								omConflictToJobMap.SetAt((void *) ilConflNum, prlJob);
							}
							ilConflNum++;
						}
					}
				}
			}

			if(ilNumUnacceptedConflicts > 1)
			{
				menu.AppendMenu(MF_STRING, GG_ACCEPT_ALL_CONFLICTS, GetString(IDS_STRING61378));
			}

			if(ogBasicData.DisplayDebugInfo())
			{
				menu.AppendMenu(MF_STRING, GG_CHECKCONFLICT_MENUITEM, "Check Conflict");
			}
		}
		ClientToScreen(&point);
        menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);
		return;
	}
	
	CWnd::OnRButtonDown(nFlags, point);
}

void GateGantt::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	GetParent()->SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
}

void GateGantt::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	GetParent()->SendMessage(WM_KEYUP, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
}

LONG GateGantt::OnUserKeyDown(UINT wParam, LONG lParam)
{
	int nIndex = GetTopIndex();

	switch (wParam) {
	case VK_UP:
		SetTopIndex(nIndex - 1);
		break;
	case VK_DOWN:
		SetTopIndex(nIndex + 1);
		break;
	}

	return 0L;
}

LONG GateGantt::OnUserKeyUp(UINT wParam, LONG lParam)
{
	return 0L;
}

/////////////////////////////////////////////////////////////////////////////
// GateGantt context menus helper functions

void GateGantt::OnMenuGateWorkOn()
{
	if (imContextItem > -1)
	{
		CString olAlid = pomViewer->GetLineText(imGroupno,imContextItem);

		GateDetailWindow *polGateDW = new GateDetailWindow(this,(const char *) olAlid,TRUE, TRUE, 
											omDisplayStart, omDisplayEnd);
	}
}


void GateGantt::GetTimeOfLongestUnresolvedDemand(GATE_BARDATA &rrmContextBar,CTime &ropStartTime,CTime &ropEndTime)
{
		// we are interested only in the time for the longest unresolved demand
		ropStartTime = rrmContextBar.EndTime;
		ropEndTime = rrmContextBar.StartTime;
		FLIGHTDATA *prlFlight;
		if ((prlFlight = ogFlightData.GetFlightByUrno(rrmContextBar.FlightUrno)) == NULL)
			return;	// error: no such flight
		CCSPtrArray<DEMANDDATA> olDemands;

		// We will loop this twice (for arrival-departure pair) if it's a turnaround
		long n = (ogFlightData.IsArrival(prlFlight) && ogFlightData.IsTurnaround(prlFlight))? 2: 1;
		for (long lpFlur = prlFlight->Urno, i = 0; i < n; lpFlur = prlFlight->Rkey, i++)
		{
			// quick fix by Brian because Rkey is no longer the URNO of the other half of the rotation !
			if(i == 1)
			{
				lpFlur = 0L;
				FLIGHTDATA *prlRotation = ogFlightData.GetRotationFlight(prlFlight);
				if(prlRotation != NULL)
				{
					lpFlur = prlRotation->Urno;
				}
			}
			ogDemandData.GetDemandsByFlur(olDemands, lpFlur, "",ALLOCUNITTYPE_GATE,PERSONNELDEMANDS);
			for(int ilD = 0; ilD < olDemands.GetSize(); ilD++)
			{
				DEMANDDATA *prlDemand = &olDemands[ilD];
				// Check if this demand has no job associated with yet
				if(!ogJodData.DemandHasJobs(prlDemand->Urno))
				{
					if (prlDemand->Debe < ropStartTime)
						ropStartTime = prlDemand->Debe;
					if (prlDemand->Deen > ropEndTime)
						ropEndTime = prlDemand->Deen;
				}
			}
		}

}
void GateGantt::OnMenuFreeEmployees()
{
	if (bmContextBarSet && rmContextBar.Type == BAR_FLIGHT)
	{
		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(rmContextBar.FlightUrno);
		if(prlFlight == NULL)
		{
			CString olErr;
			olErr.Format("Flight not found for URNO <%d>",rmContextBar.FlightUrno);
			MessageBox(olErr,"Internal Error!",MB_ICONERROR);
		}
		else
		{
			bool blFirstAttempt = true;
			bool blMultiAssign = true;
			while(blMultiAssign)
			{
				blMultiAssign = false;

				CString olAloc = ALLOCUNITTYPE_GATE;
				CString olAlid = ""; // don't get the position because one half of a turnaround may have different pos to the other half and son not all the demands are loaded

				// get the open demands for this flight
				FLIGHTDATA *prlRotation = ogFlightData.GetFlightByUrno(rmContextBar.DepartureUrno);
				CCSPtrArray <DEMANDDATA> olDemands;
				ogDataSet.GetOpenDemandsForFlight(prlFlight, prlRotation, olDemands, TIMENULL, TIMENULL, olAlid, olAloc, PERSONNELDEMANDS, rmContextBar.ReturnFlightType);
				int ilNumDemands = olDemands.GetSize();
				if(ilNumDemands <= 0)
				{
					// no open demands
					MessageBox(GetString(IDS_NOOPENDEMANDS), GetString(IDS_AVAILEMPSCAPTION), MB_ICONINFORMATION);
				}
				else
				{
					int ilNumEmps = 0;
					CAvailableEmpsDlg olAvailableEmpsDlg;
					olAvailableEmpsDlg.SetAllocUnit(olAloc, olAlid);
					olAvailableEmpsDlg.SetCaptionObject(prlFlight->Fnum);
					for(int ilD = 0; ilD < ilNumDemands; ilD++)
					{
						ilNumEmps += olAvailableEmpsDlg.GetAvailableEmployeesForDemand(&olDemands[ilD], true);
						olAvailableEmpsDlg.AddDemand(&olDemands[ilD]);
					}
					if(ilNumEmps <= 0)
					{
						// no employees available for the demands
						MessageBox(GetString(IDS_STRING61266), GetString(IDS_AVAILEMPSCAPTION), MB_ICONEXCLAMATION);
					}
					else
					{
						if(olAvailableEmpsDlg.DoModal() == IDOK && olAvailableEmpsDlg.omSelectedPoolJobUrnos.GetSize() > 0)
						{
							CCSPtrArray <DEMANDDATA> olDemands;
							olDemands.Add(olAvailableEmpsDlg.prmSelectedDemand);
							long llFlightUrno = ogDemandData.GetFlightUrno(olAvailableEmpsDlg.prmSelectedDemand);
							ogDataSet.CreateJobFlightFromDuty(this, olAvailableEmpsDlg.omSelectedPoolJobUrnos,
																llFlightUrno, olAloc, olAlid, olAvailableEmpsDlg.bmTeamAlloc, 
																&olDemands);

							blMultiAssign = ogBasicData.bmAvailEmpsMultiAssign;
						}
					}
				}
				blFirstAttempt = false;
			}
		}
	}
}

void GateGantt::OnMenuFlightConfirm()
{
	if (bmContextBarSet)
	{
		FLIGHTDATA *prlFlight;
		if ((prlFlight = ogFlightData.GetFlightByUrno(rmContextBar.FlightUrno)) != NULL)
		{
			CCSPtrArray<JOBDATA>  olJobs;
			bool bpForWholeRotation = (rmContextBar.FlightType == GATEBAR_TURNAROUND) ? true : false;
			ogJobData.GetJobsByFlur(olJobs,rmContextBar.FlightUrno, FALSE, "",ALLOCUNITTYPE_GATE, PERSONNELDEMANDS, bpForWholeRotation, rmContextBar.ReturnFlightType);

			int ilNumJobs = olJobs.GetSize();
			for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
			{
				JOBDATA *prlJob = &olJobs[ilJ];
				if(*prlJob->Stat == 'P')
				{
					ogDataSet.ConfirmJob(this,prlJob);
				}
			}

			ogConflicts.CheckConflicts(*prlFlight);
			ogCCSDdx.DataChanged((void *)this, FLIGHT_CHANGE, (void *)prlFlight);
		}
	}
	bmContextBarSet = FALSE;
}

void GateGantt::OnMenuFlightInform()
{
	if (bmContextBarSet)
	{
		FLIGHTDATA *prlFlight;
		if ((prlFlight = ogFlightData.GetFlightByUrno(rmContextBar.FlightUrno)) != NULL)
		{
			CCSPtrArray<JOBDATA>  olJobs;
			bool bpForWholeRotation = (rmContextBar.FlightType == GATEBAR_TURNAROUND) ? true : false;
			ogJobData.GetJobsByFlur(olJobs,rmContextBar.FlightUrno, FALSE, "",ALLOCUNITTYPE_GATE, PERSONNELDEMANDS, bpForWholeRotation, rmContextBar.ReturnFlightType);

			bool blAllEmpsInformed = true;
			int ilNumJobs = olJobs.GetSize(), ilJ;
			for(ilJ = 0; ilJ < ilNumJobs; ilJ++)
			{
				JOBDATA *prlJob = &olJobs[ilJ];
				if(!prlJob->Infm)
				{
					blAllEmpsInformed = false;
					break;
				}
			}
			for(ilJ = 0; ilJ < ilNumJobs; ilJ++)
			{
				JOBDATA *prlJob = &olJobs[ilJ];
				if(*prlJob->Stat == 'P')
				{
					prlJob->Infm = blAllEmpsInformed;
					ogDataSet.EmpInformedOfJob(prlJob);
				}
			}

			ogConflicts.CheckConflicts(*prlFlight);
			ogCCSDdx.DataChanged((void *)this, FLIGHT_CHANGE, (void *)prlFlight);
		}
	}
	bmContextBarSet = FALSE;
}

void GateGantt::OnMenuFlightEnd()
{
	if (bmContextBarSet)
	{
		FLIGHTDATA *prlFlight;
		if ((prlFlight = ogFlightData.GetFlightByUrno(rmContextBar.FlightUrno)) != NULL)
		{
			CCSPtrArray<JOBDATA>  olJobs;
			bool bpForWholeRotation = (rmContextBar.FlightType == GATEBAR_TURNAROUND) ? true : false;
			ogJobData.GetJobsByFlur(olJobs,rmContextBar.FlightUrno, FALSE, "",ALLOCUNITTYPE_GATE, PERSONNELDEMANDS, bpForWholeRotation, rmContextBar.ReturnFlightType);

			int ilNumJobs = olJobs.GetSize();
			for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
			{
				JOBDATA *prlJob = &olJobs[ilJ];
				if(*prlJob->Stat == 'C')
				{
					ogDataSet.FinishJob(prlJob);
				}
			}

			ogConflicts.CheckConflicts(*prlFlight);
			ogCCSDdx.DataChanged((void *)this, FLIGHT_CHANGE, (void *)prlFlight);
		}
	}
	bmContextBarSet = FALSE;
}

void GateGantt::OnMenuCheckConflict()
{
	if (bmContextBarSet)
	{
		if (rmContextBar.Type == BAR_FLIGHT)
		{
			FLIGHTDATA *prlFlight;
			if ((prlFlight = ogFlightData.GetFlightByUrno(rmContextBar.FlightUrno)) != NULL)
			{
				ogConflicts.CheckConflicts(*prlFlight,FALSE,FALSE,FALSE);
				int ilColorIndex = ogConflicts.GetFlightConflictColor(prlFlight, ALLOCUNITTYPE_GATE, PERSONNELDEMANDS, rmContextBar.ReturnFlightType);
			}
		}
	}
}


void GateGantt::OnMenuFlightWorkOn()
{
   	if (bmContextBarSet)
	{
		if (rmContextBar.Type == BAR_FLIGHT)
		{
			long llArrUrno = (rmContextBar.FlightType == PSTBAR_DEPARTURE) ? 0L :  rmContextBar.FlightUrno;
			long llDepUrno = (rmContextBar.FlightType == PSTBAR_DEPARTURE) ? rmContextBar.FlightUrno :  rmContextBar.DepartureUrno;
			new FlightDetailWindow(this,llArrUrno,llDepUrno,rmContextBar.IsShadowBar,ALLOCUNITTYPE_GATE, NULL, PERSONNELDEMANDS, rmContextBar.StartTime);
		}
		else
		{
			if (rmContextBar.Type == BAR_SPECIAL)
			{
				JOBDATA *prlJob = ogJobData.GetJobByUrno(rmContextBar.FlightUrno);
				if (prlJob != NULL)
				{
					new StaffDetailWindow(this,prlJob->Jour);
				}
			}
		}
	}

	bmContextBarSet = FALSE;
}


void GateGantt::OnMenuFlightAcceptConflict(UINT ipId)
{
	if (bmContextBarSet)
	{
		JOBDATA *prlJob = NULL;
		CONFLICTDATA *prlConflict;
		if(ipId == GG_ACCEPT_ALL_CONFLICTS)
		{
			// accept all conflicts
			ogConflicts.AcceptFlightConflicts(rmContextBar.FlightUrno, ALLOCUNITTYPE_GATE, rmContextBar.ReturnFlightType);
			if(rmContextBar.DepartureUrno > 0)
			{
				ogConflicts.AcceptFlightConflicts(rmContextBar.DepartureUrno, ALLOCUNITTYPE_GATE, rmContextBar.DepReturnFlightType);
			}
			long ilId;
			for(POSITION rlPos = omAcceptJobConflictsMap.GetStartPosition(); rlPos != NULL; )
			{
				omAcceptJobConflictsMap.GetNextAssoc(rlPos, (void *&)ilId, (void *&)prlConflict);
				if(omConflictToJobMap.Lookup((void *) ilId, (void *&) prlJob))
				{
					ogConflicts.AcceptOneJobConflict(prlJob->Urno, prlConflict->Type);
				}
			}
		}
		else
		{
			// accept a single conflict
			if(omAcceptConflictsMap.Lookup((void *) ipId, (void *&) prlConflict))
			{
				ogConflicts.AcceptOneFlightConflict(rmContextBar.FlightUrno, prlConflict->Type, ALLOCUNITTYPE_GATE, rmContextBar.ReturnFlightType);
			}
			else if(omAcceptDepConflictsMap.Lookup((void *) ipId, (void *&) prlConflict))
			{
				ogConflicts.AcceptOneFlightConflict(rmContextBar.DepartureUrno, prlConflict->Type, ALLOCUNITTYPE_GATE, rmContextBar.DepReturnFlightType);
			}
			else if(omAcceptJobConflictsMap.Lookup((void *) ipId, (void *&) prlConflict))
			{
				if(omConflictToJobMap.Lookup((void *) ipId, (void *&) prlJob))
				{
					ogConflicts.AcceptOneJobConflict(prlJob->Urno, prlConflict->Type);
				}
			}
		}
	}
	bmContextBarSet = FALSE;
}



static int CompareJobStartTime(const JOBDATA **e1, const JOBDATA **e2)
{
	return (int)((**e1).Acfr.GetTime() - (**e2).Acfr.GetTime());
}

static int CompareDemandStartTime(const DEMANDDATA **e1, const DEMANDDATA **e2)
{
	return (int)((**e1).Debe.GetTime() - (**e2).Debe.GetTime());
}

//void GateGantt::OnMenuShadowFollow()
//{
//	CCSPtrArray<JOBDATA> olJobs;
//	CCSPtrArray<DEMANDDATA> olDemands;
//
//	int ilLc;
//	int ilJobCount;
//	int ilDemandCount;
//
//    SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
//
//	if (bmContextBarSet)
//	{
//		FLIGHTDATA *prlFlight;
//		prlFlight = ogFlightData.GetFlightByUrno(rmContextBar.FlightUrno);
//		if (prlFlight != NULL)
//		{
//			ogJobData.GetJobsByFlur(olJobs,rmContextBar.FlightUrno,TRUE, ogFlightData.GetGate(prlFlight),ALLOCUNITTYPE_GATE);
//			// we are interested only in demands at the new gate
//			ogDemandData.GetDemandsByFlur(olDemands,rmContextBar.FlightUrno,ogFlightData.GetGate(prlFlight),ALLOCUNITTYPE_GATE,PERSONNELDEMANDS);
//			// we are intereseted only in jobs at the old gate
//			ilJobCount = olJobs.GetSize();
//			for (ilLc = ilJobCount-1; ilLc >= 0; ilLc--)
//			{
//				CString olTestAlid = CString(olJobs[ilLc].Alid);
//// ALIO
////				if (strcmp(olJobs[ilLc].Alid,prlFlight->Alio) != 0)
////				{
////					olJobs.RemoveAt(ilLc);
////				}
//// END ALIO
//			}
//			olJobs.Sort(CompareJobStartTime);
//			olDemands.Sort(CompareDemandStartTime);
//
//			ilDemandCount = olDemands.GetSize();
//			ilJobCount = olJobs.GetSize();
//			for (ilLc = ilJobCount-1; ilLc >= 0; ilLc--)
//			{
//				JOBDATA rlJob = olJobs[ilLc];
//				ogDataSet.DeleteJob(this, olJobs[ilLc].Urno);
//				if (ilLc < ilDemandCount)
//				{
//					ogDataSet.CreateJobFlightForDemandFromDuty(this,rlJob.Jour,
//						olDemands[ilLc].Urno,rlJob.Acfr,rlJob.Acto,FALSE);
//				}
//				else
//				{
//					ogDataSet.CreateJobFlightWithoutDemandFromDuty(this,rmContextBar.FlightUrno,
//						rlJob.Jour,rlJob.Acfr,rlJob.Acto,ALLOCUNITTYPE_GATE,ogFlightData.GetGate(prlFlight),FALSE);
//				}
//			}
//// ALIO
////			strcpy(prlFlight->Alio,prlFlight->Alid);
//// END ALIO
//			ogFlightData.SaveGate(prlFlight);
//			ogConflicts.CheckConflicts(*prlFlight);
//			ogCCSDdx.DataChanged((void *)this, FLIGHT_CHANGE, (void *)prlFlight);
//		}
//	}
//	bmContextBarSet = FALSE;
//    SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
//}


//void GateGantt::OnMenuShadowDelete()
//{
//	CCSPtrArray<JOBDATA> olJobs;
//	int ilLc;
//	int ilJobCount;
//
//	if (bmContextBarSet)
//	{
//		FLIGHTDATA *prlFlight;
//		prlFlight = ogFlightData.GetFlightByUrno(rmContextBar.FlightUrno);
//		if (prlFlight != NULL)
//		{
//			ogJobData.GetJobsByFlur(olJobs,rmContextBar.FlightUrno,TRUE,ogFlightData.GetGate(prlFlight),ALLOCUNITTYPE_GATE);
//			ilJobCount = olJobs.GetSize();
//			for (ilLc = ilJobCount-1; ilLc >= 0; ilLc--)
//			{
//// ALIO
////				if (strcmp(olJobs[ilLc].Alid,prlFlight->Alio) == 0)
////					ogDataSet.DeleteJob(this, olJobs[ilLc].Urno);
//// END ALIO
//			}
//// ALIO
////			strcpy(prlFlight->Alio,prlFlight->Alid);
//// END ALIO
//			ogFlightData.SaveGate(prlFlight);
//			ogConflicts.CheckConflicts(*prlFlight);
//			ogCCSDdx.DataChanged((void *)this, FLIGHT_CHANGE, (void *)prlFlight);
//		}
//		prlFlight = ogFlightData.GetFlightByUrno(rmContextBar.DepartureUrno);
//		if (prlFlight != NULL)
//		{
//			olJobs.RemoveAll();
//			ogJobData.GetJobsByFlur(olJobs,rmContextBar.DepartureUrno,TRUE,ogFlightData.GetGate(prlFlight),ALLOCUNITTYPE_GATE);
//			ilJobCount = olJobs.GetSize();
//			for (ilLc = ilJobCount-1; ilLc >= 0; ilLc--)
//			{
//// ALIO
////				if (strcmp(olJobs[ilLc].Alid,prlFlight->Alio) == 0)
////					ogDataSet.DeleteJob(this, olJobs[ilLc].Urno);
//// END ALIO
//			}
//// ALIO
////			strcpy(prlFlight->Alio,prlFlight->Alid);
//// END ALIO
//			ogConflicts.CheckConflicts(*prlFlight);
//			ogFlightData.SaveGate(prlFlight);
//			ogCCSDdx.DataChanged((void *)this, FLIGHT_CHANGE, (void *)prlFlight);
//		}
//	}
//	bmContextBarSet = FALSE;
//}


//void GateGantt::OnMenuShadowBarDelete()
//{
//	if (bmContextBarSet)
//	{
//		FLIGHTDATA *prlFlight;
//		prlFlight = ogFlightData.GetFlightByUrno(rmContextBar.FlightUrno);
//		if (prlFlight != NULL)
//		{
//// ALIO
////			strcpy(prlFlight->Alio,prlFlight->Alid);
//// END ALIO
//			ogFlightData.SaveGate(prlFlight);
//			ogCCSDdx.DataChanged((void *)this, FLIGHT_CHANGE, (void *)prlFlight);
//		}
//		prlFlight = ogFlightData.GetFlightByUrno(rmContextBar.DepartureUrno);
//		if (prlFlight != NULL)
//		{
//// ALIO
////			strcpy(prlFlight->Alio,prlFlight->Alid);
//// END ALIO
//			ogFlightData.SaveGate(prlFlight);
//			ogCCSDdx.DataChanged((void *)this, FLIGHT_CHANGE, (void *)prlFlight);
//		}
//	}
//	bmContextBarSet = FALSE;
//
//}




/////////////////////////////////////////////////////////////////////////////
// GateGantt message handlers helper functions

// Update every necessary GUI elements for each mouse/timer event
void GateGantt::UpdateGanttChart(CPoint point, BOOL bpIsControlKeyDown)
{
    int itemID = GetItemFromPoint(point);

    // Check if we have to change the highlight line in VerticalScale
	CPoint ptScreen = point;
	ClientToScreen(&ptScreen);
	if (WindowFromPoint(ptScreen) == this)	// moving inside GanttChart?
    {
        if (!bmIsMouseInWindow) // first time?
        {
            bmIsMouseInWindow = TRUE;
            pogButtonList->RegisterTimer(this);    // install the timer
        }

        if (itemID != imHighlightLine)  // move to new bar?
            RepaintVerticalScale(itemID, FALSE);
        if (IsBetween(point.x, 0, imVerticalScaleWidth-1))
            UpdateBarStatus(itemID, -1);    // moving on vertical scale, not a bar
        else
        {
            int ilBarno;
            if (!bpIsControlKeyDown)
                ilBarno = GetBarnoFromPoint(itemID, point);
            else
                ilBarno = GetBarnoFromPoint(itemID, point, imBorderPreLeft, imBorderPreRight);
            UpdateBarStatus(itemID, ilBarno);
        }
    }
    else if (bmIsMouseInWindow) // moving out of GanttChart? (first time only)
    {
        bmIsMouseInWindow = FALSE;
        RepaintVerticalScale(imHighlightLine, FALSE);
        imHighlightLine = -1;       // no more selected line
        UpdateBarStatus(-1, -1);    // no more status if mouse is outside of GanttChart
    }

    // Check if we have to change the icon (when user want to move/resize a bar)
	if (GetCapture() != NULL)
		;	// don't set cursor on drag-and-drop
    else if (bmIsControlKeyDown && !bpIsControlKeyDown)              // no more Ctrl-key?
        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
    else if (!bmIsMouseInWindow || !bpIsControlKeyDown || imCurrentBar == -1)
        ;   // do noting
    else if (HitTest(itemID, imCurrentBar, point) == HTCAPTION) // body?
        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEALL));	//hag20000613
/*
    else                                                        // left/right border?
        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEWE));
*/

    // Update control key state
    bmIsControlKeyDown = bpIsControlKeyDown;
}

// Update the frame window's status bar
void GateGantt::UpdateBarStatus(int ipLineno, int ipBarno)
{
    CString s;

    if (ipLineno == imHighlightLine && ipBarno == imCurrentBar)	// user still be on the old bar?
		return;

    if (ipLineno == -1) // there is no more bar status?
        s = "";
    else if (ipBarno == -1) // user move outside the bar?
        s = "";
	else
        s = pomViewer->GetBar(imGroupno, ipLineno, ipBarno)->StatusText;

	// display status bar
    if (pomStatusBar != NULL)
        pomStatusBar->SetPaneText(0, (LPCSTR)s);

	// display top scale indicator
	if (pomTimeScale != NULL)
	{
		if (ipLineno == -1 ||	// mouse moved out of every lines?
			ipBarno == -1 && imCurrentBar != -1)	// mouse just leave a bar?
			pomTimeScale->RemoveAllTopScaleIndicator();
		else if (ipBarno != imCurrentBar)
		{
			pomTimeScale->RemoveAllTopScaleIndicator();
			int ilIndicatorCount = pomViewer->GetIndicatorCount(imGroupno,
				ipLineno, ipBarno);
			for (int ilIndicatorno = 0; ilIndicatorno < ilIndicatorCount; ilIndicatorno++)
			{
				GATE_INDICATORDATA *prlIndicator = pomViewer->GetIndicator(imGroupno,
					ipLineno, ipBarno, ilIndicatorno);
				pomTimeScale->AddTopScaleIndicator(prlIndicator->StartTime,
					prlIndicator->EndTime, prlIndicator->Color);
			}
			pomTimeScale->DisplayTopScaleIndicator();
		}
	}

    // remember the bar and update the status bar (if exist)
    imCurrentBar = ipBarno;
}

// Start moving/resizing
BOOL GateGantt::BeginMovingOrResizing(CPoint point)
{
    int itemID = GetItemFromPoint(point);
    int ilBarno = GetBarnoFromPoint(itemID, point, imBorderPreLeft, imBorderPreRight);
    UpdateBarStatus(itemID, ilBarno);

	umResizeMode = HTNOWHERE;
    if (ilBarno != -1)
        umResizeMode = HitTest(itemID, ilBarno, point);

    if (umResizeMode != HTNOWHERE)
    {
        SetCapture();
        if (umResizeMode == HTCAPTION)
            SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEALL));		//hag20000613
        else
            SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEWE));

        // Calculate size of current bar
        GATE_BARDATA *prlBar = pomViewer->GetBar(imGroupno, itemID, ilBarno);
        CRect rcItem;
        GetItemRect(itemID, &rcItem);
        int left = (int)GetX(prlBar->StartTime);
        int right = (int)GetX(prlBar->EndTime);
        int top = rcItem.top + imGutterHeight + (prlBar->OverlapLevel * imOverlapHeight);
        int bottom = top + imBarHeight;

        omPointResize = point;
        omRectResize = CRect(left, top, right, bottom);
        CClientDC dc(this);
        DrawFocusRect(&dc, &omRectResize);	// first time focus rectangle
    }

    return (umResizeMode != HTNOWHERE);
}

// Update the resizing/moving rectangle
BOOL GateGantt::OnMovingOrResizing(CPoint point, BOOL bpIsLButtonDown)
{
    CClientDC dc(this);

    if (bpIsLButtonDown)    // still resizing/moving?
    {
        DrawFocusRect(&dc, &omRectResize);	// delete previous focus rectangle
        switch (umResizeMode)
        {
        case HTCAPTION: // moving mode
            omRectResize.OffsetRect(point.x - omPointResize.x, 0);
            break;
        case HTLEFT:    // resize left border
            omRectResize.left = min(point.x, omRectResize.right);
            break;
        case HTRIGHT:   // resize right border
            omRectResize.right = max(point.x, omRectResize.left);
            break;
        }
        omPointResize = point;
    }
    else
    {
        umResizeMode = HTNOWHERE;   // no more moving/resizing
        ReleaseCapture();
        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
    }

    DrawFocusRect(&dc, &omRectResize);
    return (umResizeMode != HTNOWHERE);
}

// return the item ID of the list box based on the given "point"
int GateGantt::GetItemFromPoint(CPoint point)
{
    CRect rcItem;
    for (int itemID = GetTopIndex(); itemID < GetCount(); itemID++)
    {
        GetItemRect(itemID, &rcItem);
        if (rcItem.PtInRect(point))
            return itemID;
    }
    return -1;
}

// Return the bar number of the list box based on the given point
int GateGantt::GetBarnoFromPoint(int ipLineno, CPoint point, int ipPreLeft, int ipPreRight)
{
    if (ipLineno == -1) // this checking was added after some bugs found
        return -1;

    // Calculate possible levels for vertical position
    CRect rcItem;
    GetItemRect(ipLineno, &rcItem);
    point.y -= rcItem.top + imGutterHeight;
    int level1 = (point.y < imBarHeight)? 0: ((point.y - imBarHeight) / imOverlapHeight) + 1;
    int level2 = (point.y < 0)? -1: point.y / imOverlapHeight;

    // Calculate possible time for horizontal position
    CTime time1 = GetCTime(point.x-ipPreLeft);
    CTime time2 = GetCTime(point.x+ipPreRight);

    return pomViewer->GetBarnoFromTime(imGroupno, ipLineno, time1, time2, level1, level2);
}

// Return the hit test area code (determine the location of the cursor).
// Possible codes are HTLEFT, HTRIGHT, HTNOWHERE (out-of-bar), HTCAPTION (bar body)
// 
UINT GateGantt::HitTest(int ipLineno, int ipBarno, CPoint point)
{
	return HTNOWHERE;

    if (ipLineno == -1 || ipBarno == -1)    // this checking was added after some bugs found
        return HTNOWHERE;

    // Calculate possible time for horizontal position
    CTime time1 = GetCTime(point.x-imBorderPreLeft);
    CTime time2 = GetCTime(point.x+imBorderPreRight);

    // Check against each possible case of hit test areas.
    // Since most people like to drag things to the right, we check right border before left.
    // Be careful, if the bar is very small, the user may not be able to get HTCAPTION or HTLEFT.
    //
    GATE_BARDATA *prlBar = pomViewer->GetBar(imGroupno, ipLineno, ipBarno);
    if (IsBetween(prlBar->EndTime, time1, time2))
        return HTRIGHT;
    else if (IsBetween(prlBar->StartTime, time1, time2))
        return HTLEFT;
    else if (IsOverlapped(time1, time2, prlBar->StartTime, prlBar->EndTime))
        return HTCAPTION;

    return HTNOWHERE;
}


/////////////////////////////////////////////////////////////////////////////
// GateGantt -- drag-and-drop functionalities
//
// The user may drag from:
//	- Vertical Scale for doing a gate job assignment action (create flight jobs).
//	- the Flight Bar for create normal flight jobs.
//
// and the user will have opportunities to:
//	-- drop from StaffTable (single or multiple staff shift) onto the vertical scale.
//		We will ask the user for the period of time and create many flight jobs
//		for this employees for demands in that Gate.
//	-- drop from StaffTable (single or multiple staff shift) onto FlightBar.
//		We will create normal flight jobs for those staffs.
//	-- drop from DutyBar onto the vertical scale (Gate).
//		We will ask the user for the period of time and create many flight jobs
//		for this employees for demands in that Gate.
//	-- drop from DutyBar onto FlightBar.
//		We will create a normal flight job for that staff.
//
void GateGantt::DragGateBegin(int ipLineno)
{
	m_DragDropSource.CreateDWordData(DIT_GATE, 1);
	m_DragDropSource.AddString(pomViewer->GetLineText(imGroupno, ipLineno));
	m_DragDropSource.BeginDrag();
}

void GateGantt::DragFlightBarBegin(int ipLineno, int ipBarno)
{
	GATE_BARDATA *prlBar = pomViewer->GetBar(imGroupno, ipLineno, ipBarno);
	if(prlBar != NULL)
	{
		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prlBar->FlightUrno);
		if (prlFlight != NULL)
		{
			m_DragDropSource.CreateDWordData(DIT_FLIGHTBAR, 4);
			m_DragDropSource.AddDWord(prlBar->FlightUrno);
			m_DragDropSource.AddDWord(prlBar->DepartureUrno);
			m_DragDropSource.AddString(ALLOCUNITTYPE_GATE);
			m_DragDropSource.AddString(ogFlightData.GetGate(prlFlight));
			m_DragDropSource.BeginDrag();
		}
	}
}

LONG GateGantt::OnDragOver(UINT i, LONG l)
{
	CPoint point;
    ::GetCursorPos(&point);
    ScreenToClient(&point);    
    UpdateGanttChart(point, FALSE);

    int ilClass = m_DragDropTarget.GetDataClass(); 
	if(ilClass != DIT_DUTYBAR && ilClass != DIT_FMDETAIL)
		return -1L;	// cannot interpret this object

	int itemID;
	if(imGroupno == -1)
		return -1L;
	if ((itemID = GetItemFromPoint(point)) == -1)
		return -1L;	// cannot find a line of this diagram

	CCSPtrArray <JOBDATA> olPoolJobs;
	int ilDataCount = m_DragDropTarget.GetDataCount() - 1;
	if(ilClass == DIT_DUTYBAR)
	{
		JOBDATA *prlPoolJob = NULL;
		for(int ilC = 0; ilC < ilDataCount; ilC++)
		{
			if((prlPoolJob = ogJobData.GetJobByUrno(m_DragDropTarget.GetDataDWord(ilC))) != NULL)
			{
				olPoolJobs.Add(prlPoolJob);
			}
		}
	}

	CString olGate = pomViewer->GetLineText(imGroupno, itemID);
	
	if(point.x < imVerticalScaleWidth - 2)
	{
		if(ilClass != DIT_DUTYBAR)
		{
			return -1L;
		}

		// check if the pool/emp can be dropped on this position
		CString olGroupText = pomViewer->GetGroupText(imGroupno);
		if(olGroupText == GetString(IDS_STRING61345) || olGroupText == GetString(IDS_ALL_FLIGHTS))
		{
			return -1L;
		}

		CMapPtrToPtr olUstfMap;
		int ilNumPoolJobs = olPoolJobs.GetSize();
		for(int ilPJ = 0; ilPJ < ilNumPoolJobs; ilPJ++)
		{
			if(!ogBasicData.IsInPool(ALLOCUNITTYPE_GATE,olGate,olPoolJobs[ilPJ].Alid))
				return -1L; // source pool is not in the list of restrictions for this position

			olUstfMap.SetAt((void *) olPoolJobs[ilPJ].Ustf, NULL);
		}
		if(olUstfMap.GetCount() > 1)
			return -1L; // can only drop a single emp onto the vertical scale

		return 0L;	// drop on Pst
	}

	int ilBarno = GetBarnoFromPoint(itemID, point);
	if(ilBarno == -1)
		return -1L;	// not on the bar (on the background of chart)

	GATE_BARDATA *prlBar = pomViewer->GetBar(imGroupno, itemID, ilBarno);	
	if (prlBar == NULL)
		return -1L; // must drop onto a bar

	FLIGHTDATA *prlFlight1 = ogFlightData.GetFlightByUrno(prlBar->FlightUrno);
	FLIGHTDATA *prlFlight2 = ogFlightData.GetFlightByUrno(prlBar->DepartureUrno);
	if(prlFlight1 == NULL && prlFlight2 == NULL)
		return -1L;

	CString olGate1, olGate2;
	if(prlFlight1 != NULL)
		olGate1 = ogFlightData.GetGate(prlFlight1);
	if(prlFlight2 != NULL)
		olGate2 = ogFlightData.GetGate(prlFlight2);

	CString olGroupText = pomViewer->GetGroupText(imGroupno);
	if(olGroupText != GetString(IDS_STRING61345)) // Without Gates
	{
		CMapPtrToPtr olUstfMap;
		int ilNumPoolJobs = olPoolJobs.GetSize();
		for(int ilPJ = 0; ilPJ < ilNumPoolJobs; ilPJ++)
		{
			if(prlFlight1 != NULL && prlFlight2 != NULL)
			{
				if(!ogBasicData.IsInPool(ALLOCUNITTYPE_GATE,olGate1,olPoolJobs[ilPJ].Alid) && 
					!ogBasicData.IsInPool(ALLOCUNITTYPE_GATE,olGate2,olPoolJobs[ilPJ].Alid))
					return -1L; // source pool is not in the list of restrictions for this position
			}
			else if(prlFlight1 != NULL)
			{
				if(!ogBasicData.IsInPool(ALLOCUNITTYPE_GATE,olGate1,olPoolJobs[ilPJ].Alid))
					return -1L; // source pool is not in the list of restrictions for this position
			}
		}
	}

	return 0L;	// the objects are welcome
}

LONG GateGantt::OnDrop(UINT i, LONG l)
{
    int ilClass = m_DragDropTarget.GetDataClass(); 
	if ((ilClass != DIT_DUTYBAR) && (ilClass != DIT_FMDETAIL))
		return -1L;	// cannot interpret this object

	CPoint point;
    ::GetCursorPos(&point);
    ScreenToClient(&point);    

	int itemID, ilBarno;
	if ((itemID = GetItemFromPoint(point)) == -1)
		return -1L;	// cannot find a line of this diagram
	if (point.x < imVerticalScaleWidth - 2)
	{
		if (ilClass != DIT_DUTYBAR)
			return -1L;	// cannot interpret this object

		// drop on Gate
		GATE_LINEDATA *prlLine = pomViewer->GetLine(imGroupno, itemID);
		if (prlLine != NULL)
		{
			CStringArray olEmpList;
			CTime olBegin = TIMENULL, olEnd = TIMENULL;
			JOBDATA *prlJob;
			CDWordArray olPoolJobUrnos;
			int ilDataCount = m_DragDropTarget.GetDataCount() - 1;
			for(int ilC = 0; ilC < ilDataCount; ilC++)
			{
				if((prlJob = ogJobData.GetJobByUrno(m_DragDropTarget.GetDataDWord(ilC))) != NULL)
				{
					olPoolJobUrnos.Add(prlJob->Urno);
					if(olBegin == TIMENULL || prlJob->Acfr < olBegin)
					{
						olBegin = prlJob->Acfr;
					}
					if(olEnd == TIMENULL || prlJob->Acto > olEnd)
					{
						olEnd = prlJob->Acto;
					}
					EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prlJob->Peno);
					if (prlEmp != NULL) 
					{
						olEmpList.Add(ogEmpData.GetEmpName(prlEmp));
					}
				}
			}
			if(olPoolJobUrnos.GetSize() > 0)
			{
				bool blTeamAlloc = m_DragDropTarget.GetDataDWord(ilDataCount) == 1 ? true : false; // assigning a whole team ?
				CString olGate = prlLine->Text;

				CString olEmpName = olEmpList.GetSize() > 0 ? olEmpList[0] : "";
				DGateJob olGateJobDialog(this,ALLOCUNITTYPE_GATE,olGate,olEmpName,olBegin);
				int ilDuration;
				bgModal++;
				if (olGateJobDialog.DoModal(olBegin,ilDuration) == IDOK)
				{
					olBegin = olGateJobDialog.m_Hour;
					ilDuration = olGateJobDialog.m_Duration;
					ogDataSet.CreateNormalJobs(this, JOBGATE, olPoolJobUrnos, ALLOCUNITTYPE_GATE, olGate, olBegin,olBegin+(ilDuration*60), false);
				}
				bgModal--;
			}
		}
	}
	else
	{
		if ((ilBarno = GetBarnoFromPoint(itemID, point)) == -1)
			return -1L;	// not on the bar (on the background of chart)

		GATE_BARDATA *prlBar = pomViewer->GetBar(imGroupno, itemID, ilBarno);	
		if (prlBar == NULL)
			return -1L; 
		if (prlBar->IsShadowBar)
			return -1L; // no drop on shadow bar
		if (prlBar->Type == BAR_SPECIAL)
			return -1L;
		

		if (ilClass == DIT_DUTYBAR)
		{
			//ogDataSet.CreateJobFlightFromDuty(this,olDutyUrnos,prlBar->FlightUrno, ALLOCUNITTYPE_GATE,ogFlightData.GetGate(prlBar->FlightUrno));
			CDWordArray olPoolJobUrnos;
			int ilDataCount = m_DragDropTarget.GetDataCount() - 1;
			if(ilDataCount > 0)
			{
				// an emp can have more than one pool job, for each emp we need to get only
				// the pool job that is closest to the point where the duty was dropped
				CMapPtrToPtr olUstfList;
				CTime olDropTime = GetCTime(point.x);
				JOBDATA *prlTmpJob, *prlJob;

				CCSPtrArray <JOBDATA> olPoolJobs;
				for(int ilPoolJob = 0; ilPoolJob < ilDataCount; ilPoolJob++)
				{
					prlJob = ogJobData.GetJobByUrno(m_DragDropTarget.GetDataDWord(ilPoolJob));
					if(prlJob != NULL)
					{
						if(olUstfList.Lookup((void *) prlJob->Ustf, (void *&) prlTmpJob))
						{
							// this job is closer to the drop point than the previous
							if(!IsBetween(olDropTime, prlTmpJob->Acfr, prlTmpJob->Acto) &&
								IsBetween(olDropTime, prlJob->Acfr, prlJob->Acto))
							{
								olUstfList.SetAt((void *) prlJob->Ustf, (void *) prlJob);
							}
						}
						else
						{
							olUstfList.SetAt((void *) prlJob->Ustf, (void *) prlJob);
						}
					}
				}

				// recreate the pool job urnos without duplication of pool jobs per employee
				olPoolJobUrnos.RemoveAll();
				for(POSITION rlPos = olUstfList.GetStartPosition(); rlPos != NULL; )
				{
					long llTmpUstf;
					olUstfList.GetNextAssoc(rlPos, (void *&) llTmpUstf, (void *&)prlJob);
					olPoolJobUrnos.Add(prlJob->Urno);
				}
				ilDataCount = olPoolJobUrnos.GetSize();
				bool blTeamAlloc = m_DragDropTarget.GetDataDWord(ilDataCount) == 1 ? true : false; // assigning a whole team ?
				if(prlBar->DepartureUrno > 0)
				{
					ogDataSet.CreateJobFlightFromDuty(this, olPoolJobUrnos, prlBar->FlightUrno, ALLOCUNITTYPE_GATE, 
						ogFlightData.GetGate(prlBar->FlightUrno), blTeamAlloc, NULL, TIMENULL, TIMENULL, true, 
						prlBar->DepartureUrno, false, false, 0L, prlBar->ReturnFlightType);
				}
				else
				{
					ogDataSet.CreateJobFlightFromDuty(this, olPoolJobUrnos, prlBar->FlightUrno, ALLOCUNITTYPE_GATE, 
						ogFlightData.GetGate(prlBar->FlightUrno), blTeamAlloc, NULL, TIMENULL, TIMENULL, true, 
						0L, false, false, 0L, prlBar->ReturnFlightType);
				}
			}
		}
		else
		{
			long llDutyJobUrno = m_DragDropTarget.GetDataDWord(0);
			ogDataSet.CreateSpecialFlightManagerJob(this,llDutyJobUrno,prlBar->FlightUrno);
		}
	}

    return 0L;
}

void GateGantt::DrawDottedLines(CDC *pDC, int itemID, const CRect &rcItem)
{
    CBitmap Bitmap;
	CTime olTime, olTmpTime;

	// select dotted pen
	CPen SepPen(PS_DOT, 1, BLACK);
	COLORREF olOldBkColor = pDC->SetBkColor(SILVER);
	CPen *pOldPen = pDC->SelectObject(&SepPen);
	
	// dotted horizontal lines
	CRect olRect(rcItem);
	int ilHeight = 0;
	CRect olTmpRect = olRect;
	{
		olRect.bottom = olRect.top + GetLineHeight(itemID);
        pDC->MoveTo(olRect.left + imVerticalScaleWidth + imVerticalScaleIndent, olRect.bottom - 1);
        pDC->LineTo(olRect.right, olRect.bottom - 1);
		olRect.top += GetLineHeight(itemID);
	}    
                     
	// vertical lines
//	int ilX;
//	olRect = olTmpRect;
//	CPen BlackPen(PS_DOT, 1, GRAY);
//	pDC->SetBkColor(GRAY);
//	pDC->SelectObject(&BlackPen);
//	olTime = omDisplayStart;
//
//	// calculate correct X-coordinates for first line
//	olTime += CTimeSpan(0, 0, 30, 0);
//	ilX = pomTimeScale->GetXFromTime(olTime) + imVerticalScaleWidth + imVerticalScaleIndent;
//
//	// draw lines
//	static int Offset = 0;
//	for(int i = 0; i < 10; i++)
//	{
//		olTime += CTimeSpan(0, 1, 0, 0);
//		//if (i == 5)
//		{
//			ilX = pomTimeScale->GetXFromTime(olTime) + imVerticalScaleWidth + imVerticalScaleIndent;
//			pDC->MoveTo(ilX, olRect.top);
//			pDC->LineTo(ilX, olRect.bottom);
//		}
//	}

	pDC->SelectObject(pOldPen);	
	pDC->SetBkColor(olOldBkColor);
}
