// PrintFlightSelection.cpp : implementation file
//

#include <stdafx.h>
#include <CPrintPageDialog.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPrintPageDialog dialog


CPrintPageDialog::CPrintPageDialog(BOOL bPrintSetupOnly,
		// TRUE for Print Setup, FALSE for Print Dialog
		DWORD dwFlags,BOOL bpEnablePageSelection/* = FALSE*/,CWnd* pParentWnd): CPrintDialog(bPrintSetupOnly,dwFlags,pParentWnd),bmEnablePageSelection(bpEnablePageSelection)
{	

	bmPageNoValid = TRUE;
	imPageBtnChecked = 0;
}


void CPrintPageDialog::DoDataExchange(CDataExchange* pDX)
{

	/***
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPrmDialog)
	DDX_Radio(pDX, 1058, imPageBtnChecked);
	DDX_Text(pDX, 1152, omStrFromPage);
	DDX_Text(pDX, 1153, omStrToPage);
	//}}AFX_DATA_MAP
****/

	/**
	DDX_Check(pDX,1058,imPageBtnChecked);
	DDX_Text(pDX, 1152,omStrFromPage);
	DDX_Text(pDX, 1153,omStrToPage);
	CDialog::DoDataExchange(pDX);	
	**/
}


BEGIN_MESSAGE_MAP(CPrintPageDialog, CPrintDialog)
	//{{AFX_MSG_MAP(CPrintPageDialog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrintPageDialog message handlers

BOOL CPrintPageDialog::OnInitDialog() 
{
	CPrintDialog::OnInitDialog();
	if(bmEnablePageSelection == TRUE)
	{
		(CWnd*)GetDlgItem(1058)->EnableWindow(TRUE);
		(CWnd*)GetDlgItem(1089)->EnableWindow(TRUE);
		(CWnd*)GetDlgItem(1152)->EnableWindow(TRUE);
		(CWnd*)GetDlgItem(1090)->EnableWindow(TRUE);
		(CWnd*)GetDlgItem(1153)->EnableWindow(TRUE);
		m_pd.nMinPage = 1 ;
		m_pd.nMaxPage = 0xffff ;
	}
    m_pd.hInstance = AfxGetInstanceHandle() ;
    m_pd.lpPrintTemplateName = MAKEINTRESOURCE(IDD_PRINTPAGE_DLG) ;
    m_pd.Flags |= PD_ENABLEPRINTTEMPLATE ;
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CPrintPageDialog::OnOK() 
{
	UpdateData(TRUE);
	
	BOOL isPageNoValid = TRUE;	
	int ilFromPage = 0,ilToPage = 0;

	ilFromPage = atoi(omStrFromPage);
	ilToPage = atoi(omStrToPage);
	
	bmPageNoValid = (ilFromPage > 0) & (ilToPage > 0) & (ilToPage >= ilFromPage);

	if(IsPageSelected() && !bmPageNoValid)
	{
		AfxMessageBox("Invalid page no selected");
		return;
	}

	CPrintDialog::OnOK();
}

BOOL CPrintPageDialog::IsPageSelected() const
{
	return imPageBtnChecked == 1 ? TRUE : FALSE;
}

BOOL CPrintPageDialog::IsPageNoValid() const
{
	return bmPageNoValid;
}
