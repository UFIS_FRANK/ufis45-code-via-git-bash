// TeamDelegationDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <TeamDelegationDlg.h>
#include <BasicData.h>
#include <CedaPolData.h>
#include <CedaWgpData.h>
#include <CedaTplData.h>
#include <FunctionCodeSelectionDlg.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTeamDelegationDlg dialog


CTeamDelegationDlg::CTeamDelegationDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTeamDelegationDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTeamDelegationDlg)
	m_ToTitleStatic = _T("");
	m_ToDate = NULL;
	m_ToTime = NULL;
	m_FromDate = NULL;
	m_FromTime = NULL;
	//}}AFX_DATA_INIT

	omFrom		= TIMENULL;
	omTo		= TIMENULL;
	omMinFrom	= TIMENULL;
	omMaxTo		= TIMENULL;
}


void CTeamDelegationDlg::DoDataExchange(CDataExchange* pDX)
{
	// Extended data exchange -- for member variables with Value type
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		m_FromDate	= omFrom;
		m_FromTime	= omFrom;
		m_ToDate	= omTo;
		m_ToTime	= omTo;
	}

	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTeamDelegationDlg)
	DDX_Control(pDX, IDC_TEMPLATE, m_TemplateCombo);
	DDX_Control(pDX, IDC_TEAM_COMBO, m_TeamCombo);
	DDX_Control(pDX, IDC_FROM_CAPTION, m_FromTitleStatic);
	DDX_Control(pDX, IDC_FUNCTIONS, m_FunctionsButton);
	DDX_Control(pDX, IDC_POOL_TITLE, m_PoolTitleStatic);
	DDX_Control(pDX, IDC_POOL_COMBO, m_PoolCombo);
	DDX_Control(pDX, IDC_EMP_LIST, m_EmpList);
	DDX_Text(pDX, IDC_TO_CAPTION, m_ToTitleStatic);
	DDX_CCSddmmyy(pDX, IDC_FROM_DATE, m_FromDate);
	DDX_CCSTime(pDX, IDC_FROM_TIME, m_FromTime);
	DDX_CCSddmmyy(pDX, IDC_TO_DATE, m_ToDate);
	DDX_CCSTime(pDX, IDC_TO_TIME, m_ToTime);
	//}}AFX_DATA_MAP

	// Extended data validation
	if(pDX->m_bSaveAndValidate == TRUE)
	{
		if(bmCheckTimes)
		{
			// Convert the date and time back
			omFrom = CTime(m_FromDate.GetYear(), m_FromDate.GetMonth(),
				m_FromDate.GetDay(), m_FromTime.GetHour(), m_FromTime.GetMinute(),
				m_FromTime.GetSecond());
			omTo = CTime(m_ToDate.GetYear(), m_ToDate.GetMonth(),
				m_ToDate.GetDay(),	m_ToTime.GetHour(), m_ToTime.GetMinute(),
				m_ToTime.GetSecond());
			if (omTo <= omFrom)	// don't allow end time greater than start time
			{
				// "Ung�ltige Zeit"
				MessageBox(GetString(IDS_STRING61213), NULL, MB_ICONEXCLAMATION);
				pDX->PrepareEditCtrl(IDC_FROM_TIME);
				pDX->Fail();
			}
			else if (omFrom < omMinFrom)
			{
				if (m_EmpList.GetCount() == 1)
				{
					// "Der angegebene Zeitraum liegt au�erhalb der Schicht von %s bis %s"
					CString olMsg;
					olMsg.Format(GetString(IDS_STRING62515),omMinFrom.Format("%H:%M"),omMaxTo.Format("%H:%M"));
					MessageBox(olMsg, NULL, MB_ICONEXCLAMATION);
				}
				else
				{
					// "Der angegebene Zeitraum liegt auerhalb der Schicht eines oder mehrerer Mitarbeiter von %s bis %s"
					CString olMsg;
					olMsg.Format(GetString(IDS_STRING62516),omMinFrom.Format("%H:%M"),omMaxTo.Format("%H:%M"));
					MessageBox(olMsg, NULL, MB_ICONEXCLAMATION);
				}

				pDX->PrepareEditCtrl(IDC_FROM_TIME);
				pDX->Fail();
			}
			else if (omTo > omMaxTo)
			{
				if (m_EmpList.GetCount() == 1)
				{
					// "Der angegebene Zeitraum liegt au�erhalb der Schicht von %s bis %s"
					CString olMsg;
					olMsg.Format(GetString(IDS_STRING62515),omMinFrom.Format("%H:%M"),omMaxTo.Format("%H:%M"));
					MessageBox(olMsg, NULL, MB_ICONEXCLAMATION);
				}
				else
				{
					// "Der angegebene Zeitraum liegt auerhalb der Schicht eines oder mehrerer Mitarbeiter von %s bis %s"
					CString olMsg;
					olMsg.Format(GetString(IDS_STRING62516),omMinFrom.Format("%H:%M"),omMaxTo.Format("%H:%M"));
					MessageBox(olMsg, NULL, MB_ICONEXCLAMATION);
				}
				pDX->PrepareEditCtrl(IDC_TO_TIME);
				pDX->Fail();
			}
		}
	}

}


BEGIN_MESSAGE_MAP(CTeamDelegationDlg, CDialog)
	//{{AFX_MSG_MAP(CTeamDelegationDlg)
	ON_BN_CLICKED(IDC_FUNCTIONS, OnFunctions)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTeamDelegationDlg message handlers

BOOL CTeamDelegationDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();


	CWnd *polWnd = GetDlgItem(IDC_POOL_TITLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_POOLTITLE));
	}
	polWnd = GetDlgItem(IDC_TEAM_TITLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_TEAMTITLE));
	}
	polWnd = GetDlgItem(IDC_FROM_CAPTION); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_FROMCAPTION));
	}
	polWnd = GetDlgItem(IDC_TO_CAPTION); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_TOCAPTION));
	}
	polWnd = GetDlgItem(IDC_FUNCTIONS); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_FUNCTIONS));
	}
	SetWindowText(GetString(IDS_TEAMDELEGATIONDLG));

	if(ogBasicData.bmDelegatingFlight)
	{
		GetDlgItem(IDC_TEMPLATE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_TEMPLATETITLE)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_FROM_TIME)->EnableWindow(FALSE);
		GetDlgItem(IDC_FROM_DATE)->EnableWindow(FALSE);
		GetDlgItem(IDC_TO_TIME)->EnableWindow(FALSE);
		GetDlgItem(IDC_TO_DATE)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_TEMPLATE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_TEMPLATETITLE)->ShowWindow(SW_HIDE);
	}

	SetEmpList();

	CStringArray olPoolNames;
	ogPolData.GetPoolNames(olPoolNames);
	int ilNumPools = olPoolNames.GetSize();
	for(int ilPool = 0; ilPool < ilNumPools; ilPool++)
	{
		// when Gruppendispo not available, cannot assign emp to his own pool
		if(ogBasicData.bmDisplayTeams || olPoolNames[ilPool] != omPool || ogBasicData.bmDelegatingFlight)
		{
			m_PoolCombo.AddString(olPoolNames[ilPool]);
		}
	}
	m_PoolCombo.SelectString(-1,omPool);
	
	CStringArray olTeamNames;
	ogWgpData.GetAllTeams(olTeamNames);
	int ilNumTeams = olTeamNames.GetSize();
	for(int ilTeam = 0; ilTeam < ilNumTeams; ilTeam++)
	{
		m_TeamCombo.AddString(olTeamNames[ilTeam]);
	}
	m_TeamCombo.SelectString(-1,omTeam);


	if(ogBasicData.bmDelegatingFlight)
	{
		for(int ilC = 0; ilC < ogTplData.omData.GetSize(); ilC++)
		{
			TPLDATA *polTplData = &ogTplData.omData[ilC];
			if (!polTplData)
				continue;
			if (polTplData->Tpst[0] == '0')
				continue;

			int ind = m_TemplateCombo.FindStringExact(-1,polTplData->Tnam);
			if (ind < 0)
			{
				ind = m_TemplateCombo.AddString(polTplData->Tnam);
				if(ind >= 0)
					m_TemplateCombo.SetItemData(ind,polTplData->Urno);
			}
		}
		if(omTemplate.IsEmpty())
			m_TemplateCombo.SetCurSel(0);
		else
			m_TemplateCombo.SelectString(-1,omTemplate);
	}

	if (omMinFrom == TIMENULL)
		omMinFrom = omFrom;

	if (omMaxTo == TIMENULL)
		omMaxTo = omTo;

	bmCheckTimes = true;
	if(omFrom == TIMENULL)
		bmCheckTimes = false;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CTeamDelegationDlg::OnOK() 
{
	int ilSel = m_PoolCombo.GetCurSel();
	if(ilSel != CB_ERR)
	{
		m_PoolCombo.GetLBText(ilSel,omPool);
	}

	ilSel = m_TeamCombo.GetCurSel();
	if(ilSel != CB_ERR)
	{
		m_TeamCombo.GetLBText(ilSel,omTeam);
	}

	ilSel = m_TemplateCombo.GetCurSel();
	if(ilSel != CB_ERR)
	{
		m_TemplateCombo.GetLBText(ilSel,omTemplate);
		lmUtpl = (long) m_TemplateCombo.GetItemData(ilSel);
	}

	CDialog::OnOK();
}

void CTeamDelegationDlg::OnFunctions() 
{
	int ilSelCount = m_EmpList.GetSelCount();
	if(ilSelCount <= 0)
	{
		MessageBox(GetString(IDS_PLEASESELECTLINE),"",MB_ICONSTOP);
	}
	else
	{
		CFunctionCodeSelectionDlg olFunctionCodeSelectionDlg;
		if(olFunctionCodeSelectionDlg.DoModal() == IDOK)
		{
			int *pilSelEmps = new int[ilSelCount];
			m_EmpList.GetSelItems(ilSelCount, pilSelEmps);
			for(int ilSelEmp = 0; ilSelEmp < ilSelCount; ilSelEmp++)
			{
				int ilEmpIdx = m_EmpList.GetItemData(pilSelEmps[ilSelEmp]);
				omFunctionList[ilEmpIdx] = olFunctionCodeSelectionDlg.omSelectedFunctionCode;
			}

			SetEmpList();

			delete [] pilSelEmps;
		}
	}
}

void CTeamDelegationDlg::SetEmpList(void)
{
	CMapStringToPtr olEmpsAlreadyAdded;
	m_EmpList.ResetContent();
	CString olEmpString;
	int ilNumEmps = omEmpList.GetSize();
	for(int ilEmp = 0; ilEmp < ilNumEmps; ilEmp++)
	{
		void *pvlDummy;
		if(!olEmpsAlreadyAdded.Lookup(omEmpList[ilEmp], (void *&) pvlDummy))
		{
			// don't add the emp more than once if they have >1 pool jobs
			olEmpsAlreadyAdded.SetAt(omEmpList[ilEmp], NULL);

			olEmpString.Format("%s      %s",omEmpList[ilEmp],omFunctionList[ilEmp]);
			int ilIndex = m_EmpList.AddString(olEmpString);
			m_EmpList.SetItemData(ilIndex,ilEmp);
		}
	}
}
