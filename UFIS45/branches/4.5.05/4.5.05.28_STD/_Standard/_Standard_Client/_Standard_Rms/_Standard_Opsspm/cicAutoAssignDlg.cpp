// cicAutoAssignDlg.cpp : implementation file
//

#include <stdafx.h>
#include <resource.h>
#include <cicAutoAssignDlg.h>
#include <CedaDemandData.h>
#include <CedaCccData.h>
#include <AllocData.h>
#include <WaitAssignDlg.h>
#include <CedaFlightData.h>
#include <CedaRudData.h>
#include <BasicData.h>
#include <DataSet.h>
#include <CciViewer.h>
#include <CicDemandTable.h>

#if	0
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
#endif

static int ByTime(const DEMANDDATA **pppDem1, const DEMANDDATA **pppDem2)
{
	return (int)((**pppDem1).Debe.GetTime() - (**pppDem2).Debe.GetTime());
}

static int ByTime2(const CCI_BKBARDATA **pppDem1, const CCI_BKBARDATA **pppDem2)
{
	return (int)((**pppDem1).StartTime.GetTime() - (**pppDem2).StartTime.GetTime());
}

// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsWithIn(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))

#define	_DEBUG_EX
#undef	_DEBUG_EX

/////////////////////////////////////////////////////////////////////////////
// CicAutoAssignDlg dialog


CicAutoAssignDlg::CicAutoAssignDlg(CciDiagramViewer *popViewer,CWnd* pParent /*=NULL*/)
	: CDialog(CicAutoAssignDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CicAutoAssignDlg)
	m_AssignWithoutLink = FALSE;
	m_UseCommonCheckin = TRUE;
	m_UseFIDCheckin = TRUE;
	m_UseDedicatedCheckin = TRUE;
	m_UseAllocatedOnly = FALSE;
	m_FromDay = _T("");
	m_FromTime = _T("");
	m_ToDay = _T("");
	m_ToTime = _T("");
	m_FlightDest = _T("");
	m_FlightNumber = _T("");
	m_FlightSuffix = _T("");
	m_CurrentSelection = 1;
	m_FlightAirline = _T("");
	m_ParametersRadioButton = 3;
	//}}AFX_DATA_INIT

	if (ogCfgData.rmUserSetup.OPTIONS.CCA != USERSETUPDATA::NONE)
		m_UseAllocatedOnly = TRUE;

	bmUseBestFit = true;

	pomViewer		= popViewer;
	ASSERT(pomViewer);

	omAssignStart	= pomViewer->GetStartTime();
	omAssignEnd		= pomViewer->GetEndTime();

	m_FromDay		= omAssignStart.Format("%d.%m.%Y");
	m_FromTime		= omAssignStart.Format("%H:%M");
	m_ToTime		= omAssignEnd.Format("%H:%M");
	m_ToDay			= omAssignEnd.Format("%d.%m.%Y");
	
}

void CicAutoAssignDlg::EnableSelection(BOOL bpEnable)
{
	CWnd *pWnd = GetDlgItem(IDC_FROMDAY);
	if (pWnd)
		pWnd->EnableWindow(bpEnable);

	pWnd = GetDlgItem(IDC_FROMTIME);
	if (pWnd)
		pWnd->EnableWindow(bpEnable);

	pWnd = GetDlgItem(IDC_TODAY);
	if (pWnd)
		pWnd->EnableWindow(bpEnable);

	pWnd = GetDlgItem(IDC_TOTIME);
	if (pWnd)
		pWnd->EnableWindow(bpEnable);

	pWnd = GetDlgItem(IDC_CHECK_COMMON_CHECKIN);
	if (pWnd)
		pWnd->EnableWindow(bpEnable);

	pWnd = GetDlgItem(IDC_CHECK_FID);
	if (pWnd)
		pWnd->EnableWindow(bpEnable);

	pWnd = GetDlgItem(IDC_CHECK_DEDICATED_CHECKIN);
	if (pWnd)
		pWnd->EnableWindow(bpEnable);

	pWnd = GetDlgItem(IDC_FLIGHTNUMBER);
	if (pWnd)
		pWnd->EnableWindow(bpEnable);

	pWnd = GetDlgItem(IDC_FLIGHTAIRLINE);
	if (pWnd)
		pWnd->EnableWindow(bpEnable);

	pWnd = GetDlgItem(IDC_FLIGHTSUFFIX);
	if (pWnd)
		pWnd->EnableWindow(bpEnable);

	pWnd = GetDlgItem(IDC_FLIGHTDEST);
	if (pWnd)
		pWnd->EnableWindow(bpEnable);

	
}

void CicAutoAssignDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CicAutoAssignDlg)
	DDX_Control(pDX, IDC_LIST_COUNTER_GROUPS_TO, m_CounterGroupsToList);
	DDX_Control(pDX, IDC_LIST_COUNTER_GROUPS_FROM, m_CounterGroupsFromList);
	DDX_Control(pDX, IDC_LIST_COUNTER_CLASSES_TO, m_CounterClassesToList);
	DDX_Control(pDX, IDC_LIST_COUNTER_CLASSES_FROM, m_CounterClassesFromList);
	DDX_Check(pDX, IDC_CHECK_ASSIGN_DEMANDS_WITHOUT_LINK, m_AssignWithoutLink);
	DDX_Check(pDX, IDC_CHECK_COMMON_CHECKIN, m_UseCommonCheckin);
	DDX_Check(pDX, IDC_CHECK_FID, m_UseFIDCheckin);
	DDX_Check(pDX, IDC_CHECK_DEDICATED_CHECKIN, m_UseDedicatedCheckin);
	DDX_Check(pDX, IDC_CHECK_USE_ALLOCATED_ONLY, m_UseAllocatedOnly);
	DDX_Text(pDX, IDC_FROMDAY, m_FromDay);
	DDX_Text(pDX, IDC_FROMTIME, m_FromTime);
	DDX_Text(pDX, IDC_TODAY, m_ToDay);
	DDX_Text(pDX, IDC_TOTIME, m_ToTime);
	DDX_Text(pDX, IDC_FLIGHTDEST, m_FlightDest);
	DDX_Text(pDX, IDC_FLIGHTNUMBER, m_FlightNumber);
	DDX_Text(pDX, IDC_FLIGHTSUFFIX, m_FlightSuffix);
	DDX_Radio(pDX,IDC_RADIO_USE_SELECTION,m_CurrentSelection);
	DDX_Text(pDX, IDC_FLIGHTAIRLINE, m_FlightAirline);
	DDX_Radio(pDX, IDC_RULEYES_FIPSYES, m_ParametersRadioButton);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CicAutoAssignDlg, CDialog)
	//{{AFX_MSG_MAP(CicAutoAssignDlg)
	ON_BN_CLICKED(IDC_RADIO_USE_ALL, OnRadioUseAll)
	ON_BN_CLICKED(IDC_RADIO_USE_SELECTION, OnRadioUseSelection)
	ON_BN_CLICKED(IDC_BUTTON_COUNTER_CLASSES_ADD, OnButtonCounterClassesAdd)
	ON_BN_CLICKED(IDC_BUTTON_COUNTER_CLASSES_REMOVE, OnButtonCounterClassesRemove)
	ON_BN_CLICKED(IDC_BUTTON_COUNTER_GROUP_ADD, OnButtonCounterGroupAdd)
	ON_BN_CLICKED(IDC_BUTTON_COUNTER_GROUP_REMOVE, OnButtonCounterGroupRemove)
	ON_BN_CLICKED(IDC_BUTTON_DELETE_ALL_ASSIGNS, OnButtonDeleteAllAssigns)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CicAutoAssignDlg message handlers

BOOL CicAutoAssignDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	SetWindowText(GetString(IDS_STRING61990));

	CWnd *polWnd = GetDlgItem(IDC_RADIO_USE_SELECTION);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61980));
	}

	polWnd = GetDlgItem(IDC_RADIO_USE_ALL);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61981));
	}

	polWnd = GetDlgItem(IDC_TIMEFRAME);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61935));
	}
	
	polWnd = GetDlgItem(IDC_TIMEFROM);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61936));
	}

	polWnd = GetDlgItem(IDC_TIMETO);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61937));
	}
	

	polWnd = GetDlgItem(IDC_FLIGHTS);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61939));
	}

	polWnd = GetDlgItem(IDC_FLIGHTNR);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61940));
	}

	polWnd = GetDlgItem(IDC_DESTTEXT);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61944));
	}

	polWnd = GetDlgItem(IDC_STATIC_TYPES);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61982));
	}

	polWnd = GetDlgItem(IDC_CHECK_COMMON_CHECKIN);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61983));
	}
	polWnd = GetDlgItem(IDC_CHECK_FID);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_CHECKINALLOC_FID));
	}

	polWnd = GetDlgItem(IDC_CHECK_DEDICATED_CHECKIN);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61984));
	}

	polWnd = GetDlgItem(IDC_STATIC_COUNTER_GROUPS);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61985));
	}

	polWnd = GetDlgItem(IDC_STATIC_COUNTER_CLASSES);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61986));
	}

	polWnd = GetDlgItem(IDC_STATIC_PARAMETERS);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61987));
	}

	polWnd = GetDlgItem(IDC_CHECK_ASSIGN_DEMANDS_WITHOUT_LINK);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61988));
	}

	polWnd = GetDlgItem(IDC_CHECK_USE_ALLOCATED_ONLY);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61989));
	}

	polWnd = GetDlgItem(IDOK);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_ASSIGNBUTTON));
		CButton *polBtn = (CButton *)polWnd;
		if (polBtn)
		{
			UINT ulStyle = polBtn->GetButtonStyle();
			if (!(ulStyle & BS_DEFPUSHBUTTON))
				polBtn->SetButtonStyle(ulStyle);  
		}
	}

	polWnd = GetDlgItem(IDCANCEL);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_CANCELBUTTON));
	}

	if (ogCfgData.rmUserSetup.OPTIONS.CCA == USERSETUPDATA::NONE)
	{
		polWnd = GetDlgItem(IDC_CHECK_USE_ALLOCATED_ONLY);
		if (polWnd)
			polWnd->EnableWindow(FALSE);
	}

	CStringArray olCicGroups;		
	pomViewer->GetGroupNames(olCicGroups);
	CString olGroupWithoutCounter = GetString(IDS_STRING62514);
	for (int ilC = 0; ilC < olCicGroups.GetSize(); ilC++)
	{
		if (olCicGroups[ilC] != olGroupWithoutCounter)
			int ind = m_CounterGroupsToList.AddString(olCicGroups[ilC]);
	}
	
	CStringArray olCicClasses;		
	ogCccData.GetClassNameList(olCicClasses);
	for (ilC = 0; ilC < olCicClasses.GetSize(); ilC++)
	{
		int ind = m_CounterClassesToList.AddString(olCicClasses[ilC]);
	}
	m_CounterClassesToList.AddString(GetString(IDS_UNDEFINED));

	UpdateData(FALSE);

	if (omSelectedDemandUrnos.GetSize())
	{
		EnableSelection(FALSE);
		m_CurrentSelection = 0;
		UpdateData(FALSE);
	}
	else
	{
		CWnd *pWnd = GetDlgItem(IDC_RADIO_USE_SELECTION);
		if (pWnd)
			pWnd->EnableWindow(FALSE);

		EnableSelection(TRUE);
	}

	if (ogCfgData.rmUserSetup.OPTIONS.CCA == USERSETUPDATA::NONE)
	{
		CWnd *polWnd = GetDlgItem(IDC_CHECK_USE_ALLOCATED_ONLY);
		if (polWnd)
			polWnd->EnableWindow(FALSE);
	}

#ifndef	_DEBUG_EX
	polWnd = GetDlgItem(IDC_BUTTON_DELETE_ALL_ASSIGNS);
	if (polWnd)
		polWnd->ShowWindow(SW_HIDE);
#endif
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CicAutoAssignDlg::OnOK() 
{
	// TODO: Add extra validation here
	if (!UpdateData(TRUE))
		return;

	CTime olDate = TIMENULL;
	CTime olTime = TIMENULL;

	UpdateData(TRUE);

	switch(m_ParametersRadioButton)
	{
	case 0:
		m_AssignWithoutLink = FALSE;
		m_UseAllocatedOnly = TRUE;
		break;
	case 1:
		m_AssignWithoutLink = TRUE;
		m_UseAllocatedOnly = TRUE;
		break;
	case 2:
		m_AssignWithoutLink = FALSE;
		m_UseAllocatedOnly = FALSE;
		break;
	case 3:
	default:
		m_AssignWithoutLink = TRUE;
		m_UseAllocatedOnly = FALSE;
		break;
	}

	if(m_FromDay != "" && m_FromTime != "")
	{
		olDate = DateStringToDate(m_FromDay);
		if(olDate == TIMENULL)
		{
			CString olText, olAdd;
			olText = GetString(IDS_STRING61213);
			olAdd = GetString(IDS_STRING61269);
			MessageBox(olText, olAdd, MB_OK);
			return;
		}
		
		olTime = HourStringToDate(m_FromTime, olDate);
		if(olTime == TIMENULL)
		{
			CString olText, olAdd;
			olText = GetString(IDS_STRING61213);
			olAdd = GetString(IDS_STRING61269);
			MessageBox(olText, olAdd, MB_OK);
			return;
		}
	}
	omAssignStart = olTime;
	if(m_ToDay != "" && m_ToTime != "")
	{
		olDate = DateStringToDate(m_ToDay);
		if(olDate == TIMENULL)
		{
			CString olText, olAdd;
			olText = GetString(IDS_STRING61213);
			olAdd = GetString(IDS_STRING61269);
			MessageBox(olText, olAdd, MB_OK);
			return;
		}
		
		olTime = HourStringToDate(m_ToTime, olDate);
		if(olTime == TIMENULL)
		{
			CString olText, olAdd;
			olText = GetString(IDS_STRING61213);
			olAdd = GetString(IDS_STRING61269);
			MessageBox(olText, olAdd, MB_OK);
			return ;
		}
	}
	omAssignEnd = olTime;

	CTime olTimeframeStart = ogBasicData.GetTimeframeStart() < pomViewer->GetStartTime() ? ogBasicData.GetTimeframeStart() : pomViewer->GetStartTime();
	CTime olTimeframeEnd   = ogBasicData.GetTimeframeEnd() > pomViewer->GetEndTime() ? ogBasicData.GetTimeframeEnd() : pomViewer->GetEndTime();

	if(omAssignEnd > olTimeframeEnd || omAssignStart < olTimeframeStart)
	{
		CString olText, olAdd;
		olText = GetString(IDS_STRING61213);
		olAdd = GetString(IDS_STRING61269);
		MessageBox(olText, olAdd, MB_OK);
		return;
	}

//	WaitAssignDlg olWaitDlg(this,"Please wait!");			// show the waitdlg
//	olWaitDlg.DoModal();
	AfxGetApp()->DoWaitCursor(1);

	CMapPtrToPtr olClassMap;
	for (int i = 0; i < m_CounterClassesToList.GetCount(); i++)
	{
		CString olGroupName;
		m_CounterClassesToList.GetText(i,olGroupName);
		if (olGroupName == GetString(IDS_UNDEFINED))
		{
			olClassMap.SetAt((void *)-1,NULL);
		}
		else
		{
			CCCDATA *polData = ogCccData.GetCccByName(olGroupName);
			if (polData)
				olClassMap.SetAt((void *)polData->Urno,NULL);
		}
	}

	CDWordArray olGroupCounters;
	CMapPtrToPtr olGroupMap;
	for (i = 0; i < m_CounterGroupsToList.GetCount(); i++)
	{
		CCSPtrArray<CICDATA> olCicList;
		CString olGroupName;
		m_CounterGroupsToList.GetText(i,olGroupName);
		pomViewer->GetCountersByGroup(olGroupName,olCicList);
		for (int j = 0; j < olCicList.GetSize();j++)
		{
			olGroupMap.SetAt((void *)olCicList[j].Urno,NULL);
			olGroupCounters.Add(olCicList[j].Urno);
		}
	}
	

	// create demand list and filter by dialogue		
	CCSPtrArray<DEMANDDATA> olDemandList;
	Filter(olClassMap,olGroupMap,olDemandList);

	// this is one single undo step
	if (olDemandList.GetSize())
	{
		ogUndoManager.NewAction();

		int ilNotAssigned = AssignDemands(olDemandList,olGroupCounters,olGroupMap);
	}

	AfxGetApp()->DoWaitCursor(-1);
	CDialog::OnOK();
}

void CicAutoAssignDlg::OnRadioUseAll() 
{
	// TODO: Add your control notification handler code here
	EnableSelection(TRUE);			
}

void CicAutoAssignDlg::OnRadioUseSelection() 
{
	// TODO: Add your control notification handler code here
	EnableSelection(FALSE);			
	
}

bool CicAutoAssignDlg::IsPassFilter(const CMapPtrToPtr& ropClassMap,const CMapPtrToPtr& ropGroupMap,DEMANDDATA *prpDem)
{
	if (!prpDem)
		return false;

	bool blDemIsOk = true;

	// check counter group and class
	if (blDemIsOk)
	{
		void *polValue;
		long llClassUrno = ogBasicData.GetCounterClassUrno(prpDem);
		if (llClassUrno)
		{
			blDemIsOk = ropClassMap.Lookup((void *)llClassUrno,polValue) == TRUE;
		}
		else
		{
			blDemIsOk = ropClassMap.Lookup((void *)-1,polValue) == TRUE;
		}
	}

	if (blDemIsOk)
	{
		CDWordArray olCounters;
		bool blCounterGroupMatch = false;
		if (ogBasicData.GetCounters(prpDem,olCounters))
		{
			void *polValue;
			bool blCounterMatch = false;
			for (int i = 0; i < olCounters.GetSize(); i++)
			{
				if (ropGroupMap.Lookup((void *)olCounters[i],polValue))
				{
					blCounterGroupMatch = true;
				}

				if (blCounterGroupMatch)
					break;
			}

			blDemIsOk = blCounterGroupMatch;
		}
		else // no link to location
		{
			blDemIsOk = m_AssignWithoutLink == TRUE; 
		}
	
	}

	// allocated counters only
	if (blDemIsOk)
	{
		
	}
	return blDemIsOk;
}

bool CicAutoAssignDlg::IsPassFilter(DEMANDDATA *prpDem) // the left side
{
	if (!prpDem)
		return false;

	bool blDemIsOk = true;

	if(blDemIsOk)
	{
		if (!IsOverlapped(omAssignStart,omAssignEnd,prpDem->Debe,prpDem->Deen))
		{
			blDemIsOk = false;
		}
	}

	if (blDemIsOk && prpDem->Dety[0] == CCI_DEMAND)
	{
		if (!m_UseCommonCheckin)
			blDemIsOk = false;
	}

	if (blDemIsOk && (prpDem->Dety[0] == ' ' || prpDem->Dety[0] == FID_DEMAND))
	{
		if (!m_UseFIDCheckin)
			blDemIsOk = false;
	}

	if (blDemIsOk && ogDemandData.IsFlightDependingDemand(prpDem->Dety))
	{
		if (!m_UseDedicatedCheckin)
			blDemIsOk = false;
	}

	if (blDemIsOk && ogDemandData.IsFlightDependingDemand(prpDem->Dety))
	{
		FLIGHTDATA * prlFlight = NULL;
		
		blDemIsOk = (IsPassFilter(ogFlightData.GetFlightByUrno(prpDem->Ouro))
			|| IsPassFilter(ogFlightData.GetFlightByUrno(prpDem->Ouri)));
	}

	return blDemIsOk;
}

bool CicAutoAssignDlg::IsPassFilter(FLIGHTDATA *prpFlight)
{
	if (!prpFlight)
		return false;

	bool blDest = true;
	bool blAlc = true;
	bool blFltn = true;
	bool blFlts = true;
	CString olTmp;
	
	if(!m_FlightDest.IsEmpty())
	{
		if(strcmp(prpFlight->Adid, "D") == 0)
		{
			olTmp = prpFlight->Des3;
			blDest = (m_FlightDest == olTmp)? true : false;
		}
		else
		{
			blDest = false;
		}
	}

	if(!m_FlightNumber.IsEmpty())
	{
		olTmp = prpFlight->Fltn;
		blFltn = (m_FlightNumber == olTmp)?true:false;
	}

	if(!m_FlightAirline.IsEmpty())
	{
		olTmp = prpFlight->Alc2;
		blAlc = (m_FlightAirline == olTmp)?true:false;
		if(!blAlc)
		{
			olTmp = prpFlight->Alc3;
			blAlc = (m_FlightAirline == olTmp)?true:false;
		}
	}

	if(!m_FlightSuffix.IsEmpty())
	{
		olTmp = prpFlight->Flns;
		blFlts = (m_FlightSuffix == olTmp)?true:false;
	}

	return (blDest && blAlc && blFltn && blFlts);

}

void CicAutoAssignDlg::OnButtonCounterGroupAdd() 
{
	// TODO: Add your control notification handler code here
	CString olText;

	// Move selected items from left list box to right list box
	for (int ilLc = m_CounterGroupsFromList.GetCount()-1; 
		ilLc >= 0 ; ilLc--)
	{
		if (!m_CounterGroupsFromList.GetSel(ilLc))	// unselected item?
			continue;
		m_CounterGroupsFromList.GetText(ilLc, olText);	// load string to "olText"
		
		m_CounterGroupsToList.AddString(olText);	// move string from left to right box
		m_CounterGroupsFromList.DeleteString(ilLc);
	}
	
}

void CicAutoAssignDlg::OnButtonCounterGroupRemove() 
{
	// TODO: Add your control notification handler code here
	CString olText;

	for (int ilLc = m_CounterGroupsToList.GetCount()-1; 
		ilLc >= 0 ; ilLc--)
	{
		if (!m_CounterGroupsToList.GetSel(ilLc))	// unselected item?
			continue;
		m_CounterGroupsToList.GetText(ilLc, olText);	// load string to "olText"
		
		m_CounterGroupsFromList.AddString(olText);	// move string from left to right box
		m_CounterGroupsToList.DeleteString(ilLc);
	}
	
}


void CicAutoAssignDlg::OnButtonCounterClassesAdd() 
{
	// TODO: Add your control notification handler code here
	CString olText;

	// Move selected items from left list box to right list box
	for (int ilLc = m_CounterClassesFromList.GetCount()-1; 
		ilLc >= 0 ; ilLc--)
	{
		if (!m_CounterClassesFromList.GetSel(ilLc))	// unselected item?
			continue;
		m_CounterClassesFromList.GetText(ilLc, olText);	// load string to "olText"
		
		m_CounterClassesToList.AddString(olText);	// move string from left to right box
		m_CounterClassesFromList.DeleteString(ilLc);
	}
	
}

void CicAutoAssignDlg::OnButtonCounterClassesRemove() 
{
	// TODO: Add your control notification handler code here
	CString olText;

	for (int ilLc = m_CounterClassesToList.GetCount()-1; 
		ilLc >= 0 ; ilLc--)
	{
		if (!m_CounterClassesToList.GetSel(ilLc))	// unselected item?
			continue;
		m_CounterClassesToList.GetText(ilLc, olText);	// load string to "olText"
		
		m_CounterClassesFromList.AddString(olText);	// move string from left to right box
		m_CounterClassesToList.DeleteString(ilLc);
	}
	
}

void CicAutoAssignDlg::Filter(const CMapPtrToPtr& ropClassMap,const CMapPtrToPtr& ropGroupMap,CCSPtrArray<DEMANDDATA>& ropDemandList)
{
	CCSPtrArray<DEMANDDATA> olDemandList;

	// use selection only
	if (m_CurrentSelection == 0)
	{
		for (int i = 0; i < omSelectedDemandUrnos.GetSize(); i++)
		{
			DEMANDDATA *prlDem = ogDemandData.GetDemandByUrno(omSelectedDemandUrnos[i]);
			olDemandList.Add(prlDem);
		}
	}
	else
	{
		ogDemandData.GetDemandsByAlid(olDemandList,"",ALLOCUNITTYPE_CIC,PERSONNELDEMANDS);
	}

	for (int i = 0; i < olDemandList.GetSize(); i++)
	{
		DEMANDDATA *prlDem = &olDemandList[i];
		// ignore already allocated demands
		if (strlen(prlDem->Alid) > 0)
			continue;

		// right side filter will always be evaluated
		if (IsPassFilter(ropClassMap,ropGroupMap,prlDem))
		{
			// must we evaluate left side filter ?
			if (m_CurrentSelection == 0)
			{
				ropDemandList.Add(prlDem);
			}
			else
			{
				if (IsPassFilter(prlDem))
				{
					ropDemandList.Add(prlDem);
				}
			}
		}
	}

}

void CicAutoAssignDlg::FilterByLocation(CCSPtrArray<DEMANDDATA>& ropDemandList,CCSPtrArray<DEMANDDATA>& ropDemandWithLocationList,CCSPtrArray<DEMANDDATA>& ropDemandWithoutLocationList)
{
	// check whether we must check for location or not 
	if (!m_AssignWithoutLink)
	{
		ropDemandWithLocationList = ropDemandList;
		return;
	}

	for (int i = 0; i < ropDemandList.GetSize(); i++)
	{
		DEMANDDATA *prlDem = &ropDemandList[i];

		CDWordArray olCounters;
		if (ogBasicData.GetCounters(prlDem,olCounters))
		{
			ropDemandWithLocationList.Add(prlDem);
		}
		else
		{
			ropDemandWithoutLocationList.Add(prlDem);
		}
	}
}

void CicAutoAssignDlg::SetData(CTime opAssignStart,CTime opAssignEnd,CDWordArray& ropSelectedDemandUrnos)
{
	omAssignStart			= opAssignStart;
	omAssignEnd				= opAssignEnd; 
	omSelectedDemandUrnos.Append(ropSelectedDemandUrnos);

	m_FromDay = opAssignStart.Format("%d.%m.%Y");
	m_FromTime= opAssignStart.Format("%H:%M");
	m_ToTime  = opAssignEnd.Format("%H:%M");
	m_ToDay   = opAssignEnd.Format("%d.%m.%Y");
}

int CicAutoAssignDlg::AssignDemands(CCSPtrArray<DEMANDDATA>& ropDemandList,CDWordArray& ropGroupCounters,const CMapPtrToPtr& ropGroupMap)
{
	int ilNotAssigned = 0;
	if (ogCfgData.rmUserSetup.OPTIONS.CCA != USERSETUPDATA::NONE)
	{
		ilNotAssigned = AssignCcaDemands(ropDemandList,ropGroupCounters,ropGroupMap);
		if (!m_UseAllocatedOnly)
		{
			ilNotAssigned = AssignNonCcaDemands(ropDemandList,ropGroupCounters,ropGroupMap);
		}

	}
	else
	{
		ilNotAssigned = AssignNonCcaDemands(ropDemandList,ropGroupCounters,ropGroupMap);
	}

	return ilNotAssigned;
}

int CicAutoAssignDlg::AssignCcaDemands(CCSPtrArray<DEMANDDATA>& ropDemandList,CDWordArray& ropGroupCounters,const CMapPtrToPtr& ropGroupMap)
{

	int ilNotAssigned = 0;

	// at first, sort by start time
	ropDemandList.Sort(ByTime);

	// but start with the latest one
	for (int i = ropDemandList.GetSize() - 1; i >= 0;i--)
	{
		DEMANDDATA *prlDem = &ropDemandList[i];
		CDWordArray olCounters;
		ogBasicData.GetCounters(prlDem,olCounters);
		if (!olCounters.GetSize())
			olCounters.Append(ropGroupCounters);
		else
		{
			// intersect between rule counters and group counters
			void *polValue;
			for (int k = olCounters.GetSize()-1; k >= 0; k--)
			{
				if (!ropGroupMap.Lookup((void *)olCounters[k],polValue))
					olCounters.RemoveAt(k);
			}
		}
		
		CCSPtrArray<CICDATA> olSortedCounters;
		GetSortedCicList(olCounters,olSortedCounters);

		BOOL blAssigned = FALSE;
		CCI_LINEDATA *prlLine = FindBestCcaFit(prlDem,olSortedCounters);
		if (prlLine)
		{
			if (AssignDemandToLocation(prlDem,prlLine->CicUrno))			
			{
				blAssigned = TRUE;
			}
		}
		if (!blAssigned)
			++ilNotAssigned;
		else
			ropDemandList.RemoveAt(i);			
	}

	return ilNotAssigned;
}

int CicAutoAssignDlg::AssignNonCcaDemands(CCSPtrArray<DEMANDDATA>& ropDemandList,CDWordArray& ropGroupCounters,const CMapPtrToPtr& ropGroupMap)
{

	int ilNotAssigned = 0;

	// at first, sort by start time
	ropDemandList.Sort(ByTime);

	// but start with the latest one
	for (int i = ropDemandList.GetSize() - 1; i >= 0;i--)
	{
		DEMANDDATA *prlDem = &ropDemandList[i];
		CDWordArray olCounters;
		ogBasicData.GetCounters(prlDem,olCounters);
		if (!olCounters.GetSize())
			olCounters.Append(ropGroupCounters);
		else
		{
			// intersect between rule counters and group counters
			void *polValue;
			for (int k = olCounters.GetSize()-1; k >= 0; k--)
			{
				if (!ropGroupMap.Lookup((void *)olCounters[k],polValue))
					olCounters.RemoveAt(k);
			}
		}
		
		CCSPtrArray<CICDATA> olSortedCounters;
		GetSortedCicList(olCounters,olSortedCounters);

		BOOL blAssigned = FALSE;
		if (!bmUseBestFit)
		{
			for (int j = 0; j < olSortedCounters.GetSize(); j++)
			{
				if (AssignDemandToLocation(prlDem,olSortedCounters[j].Urno))			
				{
					blAssigned = TRUE;
					break;
				}
			}
		}
		else
		{
			CCI_LINEDATA *prlLine = FindBestFit(prlDem,olSortedCounters);
			if (prlLine)
			{
				if (AssignDemandToLocation(prlDem,prlLine->CicUrno))			
				{
					blAssigned = TRUE;
				}
			}
		}
		if (!blAssigned)
			++ilNotAssigned;
	}

	return ilNotAssigned;
}

BOOL CicAutoAssignDlg::AssignDemandToLocation(DEMANDDATA *prlDem,long lpCounterUrno)
{
#ifdef	_DEBUG_EX
	FILE *fp = fopen(CCSLog::GetTmpPath("\\AutoAssign.log"),"at+");
	ASSERT(fp);
	fprintf(fp,"Assigning best fit for %s - %s\n",prlDem->Debe.Format("%d/%H:%M:%S"),prlDem->Deen.Format("%d/%H:%M:%S"));
#endif

	CICDATA *prlCic = ogCicData.GetCicByUrno(lpCounterUrno);
	if (!prlCic)
		return FALSE;

	int ilGroupno,ilLineno,ilSideno;
	if (!pomViewer->FindDesk(prlCic->Cnam,ilGroupno,ilLineno,ilSideno))
	{
#ifdef	_DEBUG_EX
		fclose(fp);
#endif
		return FALSE;
	}

	CCI_LINEDATA *prlLine = pomViewer->GetLine(ilGroupno,ilLineno,ilSideno);
	if (!prlLine)
	{
#ifdef	_DEBUG_EX
		fclose(fp);
#endif
		return FALSE;
	}

	BOOL blIsBlocked = FALSE;
	for (int i = 0; i < prlLine->BlkBkBars.GetSize(); i++)
	{
		CCI_BKBARDATA *prlBkBar = &prlLine->BlkBkBars[i];
		if (IsReallyOverlapped(prlDem->Debe,prlDem->Deen,prlBkBar->StartTime,prlBkBar->EndTime))
		{
#ifdef	_DEBUG_EX
			fprintf(fp,"NOT assigned area for %s - %s\n",prlBkBar->StartTime.Format("%d/%H:%M:%S"),prlBkBar->EndTime.Format("%d/%H:%M:%S"));
#endif
			blIsBlocked = TRUE;
			break;
		}
	}

	if (blIsBlocked)
	{
#ifdef	_DEBUG_EX
		fclose(fp);
#endif
		return FALSE;
	}

	for (i = 0; i < prlLine->DemBkBars.GetSize(); i++)
	{
		CCI_BKBARDATA *prlBkBar = &prlLine->DemBkBars[i];
		if (IsReallyOverlapped(prlDem->Debe,prlDem->Deen,prlBkBar->StartTime,prlBkBar->EndTime))
		{
#ifdef	_DEBUG_EX
			fprintf(fp,"NOT assigned area for %s - %s\n",prlBkBar->StartTime.Format("%d/%H:%M:%S"),prlBkBar->EndTime.Format("%d/%H:%M:%S"));
#endif
			blIsBlocked = TRUE;
			break;
		}
	}
	
	if (blIsBlocked)
	{
#ifdef	_DEBUG_EX
		fclose(fp);
#endif
		return FALSE;
	}

#ifdef	_DEBUG_EX
	fclose(fp);
#endif

	UndoReassignDemandAlid *prlUndo = new UndoReassignDemandAlid(prlDem,prlCic->Cnam);

	if (!ogDemandData.AssignAlid(prlDem->Urno,prlCic->Cnam,true))
	{
		delete prlUndo;
		return FALSE;
	}

	if (ogCfgData.rmUserSetup.OPTIONS.CCA == USERSETUPDATA::READWRITE)
	{
		CCSPtrArray<CCADATA> olCcaList;
		if (ogCcaData.CreateCcaRecords(prlDem,olCcaList))
		{
			for (int i = 0; i < olCcaList.GetSize(); i++)
			{
				prlUndo->AddNewCcaRecord(&olCcaList[i]);
			}
		}
	}

	ogUndoManager.NewStep("",prlUndo);
	return TRUE;
}

void CicAutoAssignDlg::GetSortedCicList(CDWordArray& ropCicUrnoList,CCSPtrArray<CICDATA>& ropCicList)
{
	for (int i = 0; i < ropCicUrnoList.GetSize(); i++)
	{
		CICDATA *prlCic = ogCicData.GetCicByUrno(ropCicUrnoList[i]);
		if (prlCic)
			ropCicList.Add(prlCic);
	}

	pomViewer->SortCounters(ropCicList);
}

CCI_LINEDATA *CicAutoAssignDlg::FindBestFit(DEMANDDATA *prpDem,CCSPtrArray<CICDATA>& ropCicList)
{
	CCI_LINEDATA *prlBestFit = NULL;
	CTimeSpan BestGap;

#ifdef	_DEBUG_EX
	FILE *fp = fopen(CCSLog::GetTmpPath("\\AutoAssign.log"),"at+");
	ASSERT(fp);
	fprintf(fp,"-----------------------------------------------------------------------------\n");
	fprintf(fp,"Searching best fit for %ld from %s - %s\n",prpDem->Urno,prpDem->Debe.Format("%d/%H:%M:%S"),prpDem->Deen.Format("%d/%H:%M:%S"));
#endif

	for (int i = 0; i < ropCicList.GetSize(); i++)
	{
		CCSPtrArray<CCI_BKBARDATA> olBlkList;

		int ilGroupno,ilLineno,ilSideno;
		if (!pomViewer->FindDesk(ropCicList[i].Cnam,ilGroupno,ilLineno,ilSideno))
			continue;

		CCI_LINEDATA *prlLine = pomViewer->GetLine(ilGroupno,ilLineno,ilSideno);
		if (!prlLine)
			continue;

#ifdef	_DEBUG_EX
		fprintf(fp,"Searching line %d with name = %s\n",i,ropCicList[i].Cnam);
#endif
		olBlkList.Append(prlLine->BlkBkBars);
		olBlkList.Append(prlLine->DemBkBars);

		olBlkList.Sort(ByTime2);

		if (olBlkList.GetSize() == 0)	// not used 
		{
			CTimeSpan CurrentGap = ogBasicData.GetTimeframeEnd() - ogBasicData.GetTimeframeStart();
#ifdef	_DEBUG_EX
			fprintf(fp,"current line with name = %s, current fit = %ld minutes\n",prlLine->Text,CurrentGap.GetTotalMinutes());
#endif
			if (!prlBestFit)
			{
				prlBestFit = prlLine;
				BestGap = CurrentGap;
			}
			else if (CurrentGap < BestGap)
			{
				prlBestFit = prlLine;
				BestGap = CurrentGap;
			}
		}
		else
		{
			CTime maxUsedTime;
			for (int j = 0; j < olBlkList.GetSize(); j++)
			{
				CCI_BKBARDATA *prlBkBar = &olBlkList[j];
#ifdef	_DEBUG_EX
				fprintf(fp,"Blocked area [%d] for %s - %s\n",j,prlBkBar->StartTime.Format("%d/%H:%M:%S"),prlBkBar->EndTime.Format("%d/%H:%M:%S"));
#endif
				if (j == 0)
				{
					maxUsedTime = olBlkList[j].EndTime;
					if (prpDem->Deen <= olBlkList[j].StartTime)
					{
						CTimeSpan CurrentGap = olBlkList[j].StartTime - ogBasicData.GetTimeframeStart();
#ifdef	_DEBUG_EX
						fprintf(fp,"current line with name = %s, current fit = %ld minutes\n",prlLine->Text,CurrentGap.GetTotalMinutes());
#endif
						if (!prlBestFit)
						{
							prlBestFit = prlLine;
							BestGap = CurrentGap;
						}
						else if (CurrentGap < BestGap)
						{
							prlBestFit = prlLine;
							BestGap = CurrentGap;
						}
					}
				}
				else if (!IsOverlapped(olBlkList[j-1].StartTime,olBlkList[j-1].EndTime,olBlkList[j].StartTime,olBlkList[j].EndTime))
				{
					if (olBlkList[j].EndTime > maxUsedTime)
						maxUsedTime = olBlkList[j].EndTime;

					if (prpDem->Deen <= olBlkList[j].StartTime)
					{
						if (prpDem->Debe >= olBlkList[j-1].EndTime)
						{
							CTimeSpan CurrentGap = olBlkList[j].StartTime - olBlkList[j-1].EndTime;
#ifdef	_DEBUG_EX
							fprintf(fp,"current line with name = %s, current fit = %ld minutes\n",prlLine->Text,CurrentGap.GetTotalMinutes());
#endif
							if (!prlBestFit)
							{
								prlBestFit = prlLine;
								BestGap = CurrentGap;
							}
							else if (CurrentGap < BestGap)
							{
								prlBestFit = prlLine;
								BestGap = CurrentGap;
							}
						}
					}
				}
				else if (olBlkList[j].EndTime > maxUsedTime)
				{
					maxUsedTime = olBlkList[j].EndTime;
				}

				if (j == olBlkList.GetSize() - 1)
				{
					if (prpDem->Debe >= maxUsedTime)
					{
						CTimeSpan CurrentGap = ogBasicData.GetTimeframeEnd() - maxUsedTime;
#ifdef	_DEBUG_EX
						fprintf(fp,"current line with name = %s, current fit = %ld minutes\n",prlLine->Text,CurrentGap.GetTotalMinutes());
#endif
						if (!prlBestFit)
						{
							prlBestFit = prlLine;
							BestGap = CurrentGap;
						}
						else if (CurrentGap < BestGap)
						{
							prlBestFit = prlLine;
							BestGap = CurrentGap;
						}
					}
				}

			}
		}
	}

#ifdef	_DEBUG_EX
	if (prlBestFit)
	{
		fprintf(fp,"Found line with name = %s, best fit = %ld minutes\n",prlBestFit->Text,BestGap.GetTotalMinutes());
	}
	else
	{
		fprintf(fp,"No gap found\n");
	}

	fclose(fp);
#endif
	return prlBestFit;
}


CCI_LINEDATA *CicAutoAssignDlg::FindBestCcaFit(DEMANDDATA *prpDem,CCSPtrArray<CICDATA>& ropCicList)
{
	CCI_LINEDATA*	prlBestFit = NULL;
	int				BestGap;

#ifdef	_DEBUG_EX
	FILE *fp = fopen(CCSLog::GetTmpPath("\\AutoAssign.log"),"at+");
	ASSERT(fp);
	fprintf(fp,"-----------------------------------------------------------------------------\n");
	fprintf(fp,"Searching best cca fit for %ld from %s - %s\n",prpDem->Urno,prpDem->Debe.Format("%d/%H:%M:%S"),prpDem->Deen.Format("%d/%H:%M:%S"));
#endif

	for (int i = 0; i < ropCicList.GetSize(); i++)
	{
		CCSPtrArray<CCI_BKBARDATA> olBlkList;
		CCSPtrArray<CCI_BKBARDATA> olCcaList;

		int ilGroupno,ilLineno,ilSideno;
		if (!pomViewer->FindDesk(ropCicList[i].Cnam,ilGroupno,ilLineno,ilSideno))
			continue;

		CCI_LINEDATA *prlLine = pomViewer->GetLine(ilGroupno,ilLineno,ilSideno);
		if (!prlLine)
			continue;

#ifdef	_DEBUG_EX
		fprintf(fp,"Searching line %d with name = %s\n",i,ropCicList[i].Cnam);
#endif
		for (int ilCca = 0; ilCca < prlLine->CcaBkBars.GetSize(); ilCca ++)
		{
			if (!ogDemandData.IsFlightDependingDemand(prpDem->Dety))
			{
				if (prlLine->CcaBkBars[ilCca].Type == CCI_BKBARDATA::COMMON)
				{
					olCcaList.Add(&prlLine->CcaBkBars[ilCca]);
#ifdef	_DEBUG_EX
					fprintf(fp,"CCATAB record from %s - %s added\n",prlLine->CcaBkBars[ilCca].StartTime.Format("%d/%H:%M:%S"),prlLine->CcaBkBars[ilCca].EndTime.Format("%d/%H:%M:%S"));
#endif
				}
			}
			else
			{
				if (prlLine->CcaBkBars[ilCca].Type == CCI_BKBARDATA::DEDICATED)
				{
					long ilUaft = ogDemandData.GetFlightUrno(prpDem);
					//if (prlLine->CcaBkBars[ilCca].Uaft == ilUaft && (prlLine->CcaBkBars[ilCca].Urue == 0 || prlLine->CcaBkBars[ilCca].Urue == prpDem->Urue))
					if(prlLine->CcaBkBars[ilCca].Uaft == ilUaft)
					{
						olCcaList.Add(&prlLine->CcaBkBars[ilCca]);
#ifdef	_DEBUG_EX
						fprintf(fp,"CCATAB record from %s - %s added\n",prlLine->CcaBkBars[ilCca].StartTime.Format("%d/%H:%M:%S"),prlLine->CcaBkBars[ilCca].EndTime.Format("%d/%H:%M:%S"));
#endif	
					}
				}
			}
		}

		olBlkList.Append(prlLine->BlkBkBars);
		olBlkList.Append(prlLine->DemBkBars);

		olCcaList.Sort(ByTime2);
		olBlkList.Sort(ByTime2);


		CTime olStart = prpDem->Debe;
		CTime olEnd   = prpDem->Deen;

		if (ogDemandData.IsFlightDependingDemand(prpDem))	// dedicated only
		{
			RUDDATA *prlRud = ogRudData.GetRudByUrno(prpDem->Urud);
			if (prlRud)
			{
				olStart += CTimeSpan(0L,0L,0L,prlRud->Ttgt);
				olEnd   -= CTimeSpan(0L,0L,0L,prlRud->Ttgf);

			}
		}

		for (ilCca = 0; ilCca < olCcaList.GetSize(); ilCca ++)
		{
			bool blMatch = false;
			if (prlLine->CcaBkBars[ilCca].Type == CCI_BKBARDATA::DEDICATED)
				blMatch = IsReallyOverlapped(olStart,olEnd,olCcaList[ilCca].StartTime,olCcaList[ilCca].EndTime);
			else
				blMatch = IsWithIn(olStart,olEnd,olCcaList[ilCca].StartTime,olCcaList[ilCca].EndTime);
			if (blMatch)
			{
				bool blBlocked = false;
				for (int ilBlk = 0; ilBlk < olBlkList.GetSize(); ilBlk++)
				{
					if (IsReallyOverlapped(olStart,olEnd,olBlkList[ilBlk].StartTime,olBlkList[ilBlk].EndTime))	
					{
						blBlocked = true;
						break;
					}
				}

				if (!blBlocked)
				{
					int CurrentGap = abs((olCcaList[ilCca].EndTime - olEnd).GetTotalMinutes()) + abs((olCcaList[ilCca].StartTime - olStart).GetTotalMinutes());

					if (!prlBestFit)
					{
						prlBestFit = prlLine;
						BestGap = CurrentGap;
					}
					else if (CurrentGap < BestGap)
					{
						prlBestFit = prlLine;
						BestGap = CurrentGap;
					}
				}
			}
		}
	}
#ifdef	_DEBUG_EX
	if (prlBestFit)
	{
		fprintf(fp,"Found line with name = %s, best fit = %ld minutes\n",prlBestFit->Text,BestGap);
	}
	else
	{
		fprintf(fp,"No gap found\n");
	}

	fclose(fp);
#endif
	return prlBestFit;
}


void CicAutoAssignDlg::OnButtonDeleteAllAssigns() 
{
	// TODO: Add your control notification handler code here
//	WaitAssignDlg olWaitDlg(this,"Please wait!");			// show the waitdlg
//	olWaitDlg.DoModal();
	AfxGetApp()->DoWaitCursor(1);

	CCSPtrArray<DEMANDDATA> olDemandList;
	ogDemandData.GetDemandsByAlid(olDemandList,"",ALLOCUNITTYPE_CIC,PERSONNELDEMANDS);
	for (int i = 0; i < olDemandList.GetSize(); i++)
	{
		DEMANDDATA *prlDem = &olDemandList[i];
		// ignore already deallocated demands
		if (strlen(prlDem->Alid) <= 0)
			continue;

		ogDemandData.AssignAlid(prlDem->Urno,"",true);
	}
	
	AfxGetApp()->DoWaitCursor(-1);
}
