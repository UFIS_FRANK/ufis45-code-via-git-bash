// Class for Airline Types
#ifndef _CEDAAPTDATA_H_
#define _CEDAAPTDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct AptDataStruct
{
	long	Urno;		// Unique Record Number
	char	Apc3[4];	// 3 letter airport code
	char	Apc4[5];	// 4 letter airport code

	AptDataStruct(void)
	{
		Urno = 0L;
		strcpy(Apc3,"");
		strcpy(Apc4,"");
	}
};

typedef struct AptDataStruct APTDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaAptData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <APTDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapStringToPtr omApc3Map;
	CMapStringToPtr omApc4Map;
	CString GetTableName(void);

// Operations
public:
	CedaAptData();
	~CedaAptData();

	CCSReturnCode ReadAptData();
	APTDATA* GetAptByUrno(long lpUrno);

	APTDATA* GetAptByApc3(const char *pcpApc3);
	APTDATA* GetAptByApc4(const char *pcpApc4);
	CString FormatAptString(const char *pcpApc3, const char *pcpApc4);
	CString FormatAptString(APTDATA *prpApt);
	int GetApc3Apc4Strings(CStringArray &ropApc3Apc4Strings);

private:
	void AddAptInternal(APTDATA &rrpApt);
	void ClearAll();
};


extern CedaAptData ogAptData;
#endif _CEDAAPTDATA_H_
