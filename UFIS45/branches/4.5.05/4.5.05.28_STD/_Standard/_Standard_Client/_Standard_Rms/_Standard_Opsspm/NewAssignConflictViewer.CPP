// FlightDV.cpp : implementation file
//  

#include <stdafx.h>
#include <CCSGlobl.h>
#include <resource.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CCSDragDropCtrl.h>
#include <CedaJobData.h>
#include <CedaFlightData.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <gantt.h>
#include <gbar.h>
#include <CedaFlightData.h>
#include <CedaDemandData.h>
#include <conflict.h>
#include <ccsddx.h>
#include <cviewer.h>
#include <NewAssignConflictViewer.h>
#include <CedaJodData.h>
#include <BasicData.h>
#include <AllocData.h>
#include <DataSet.h>
#include <CedaRudData.h>
#include <CedaRueData.h>
#include <CedaAloData.h>


#ifdef _DEBUG
#undef THIS_FILE
#define new DEBUG_NEW
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))

// Local function prototype
static void NewAssignConflictCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);


NewAssignConflictViewer::NewAssignConflictViewer()
{
	pomAttachWnd = NULL;
	bmTurnAroundFlight = FALSE;
	pomAF = NULL;
	pomDF = NULL;

	pomDemands = NULL;
	pomJobs = NULL;
	pomNoDemandJobs = NULL;

	pomInboundBrush = new CBrush(PGREEN); 
	pomOutboundBrush = new CBrush(PBLUE); 
	pomTurnaroundBrush = new CBrush(PYELLOW); 
	pomQualityBrush = new CBrush(PPINK);
	pomResBrush = new CBrush(PRED);

//	ogCCSDdx.Register(this, FLIGHT_CHANGE,CString("FLIGHTDETAIL_VIEWER"), CString("Flight Change"), NewAssignConflictCf);
//	ogCCSDdx.Register(this, JOB_CHANGE, CString("FLIGHTDETAIL_VIEWER"),CString("Job Changed"), NewAssignConflictCf);	// for what-if changes

	bmSingleJobAndDemand = false;
	prmSelectedDemand = NULL;
}

void NewAssignConflictViewer::RemoveAll()
{
	ASSIGNCONFLICT_LINEDATA *prlFL;
	for (int i = 0; i < omFlightLines.GetSize(); i++)
	{
		prlFL = (ASSIGNCONFLICT_LINEDATA *)omFlightLines[i];
		for (int j = 0; j < prlFL->Bars.GetSize(); j++)
		{
			ASSIGNCONFLICT_BARDATA *prlBar = (ASSIGNCONFLICT_BARDATA *)prlFL->Bars[j];
			delete prlBar;
		}
		prlFL->Bars.RemoveAll();
		for (j = 0; j < prlFL->BkBars.GetSize(); j++)
		{
			ASSIGNCONFLICT_BARDATA *prlBkBar = (ASSIGNCONFLICT_BARDATA *)prlFL->BkBars[j];
			delete prlBkBar;
		}
		prlFL->BkBars.RemoveAll();
		delete prlFL;
	}
	omFlightLines.RemoveAll();
}

NewAssignConflictViewer::~NewAssignConflictViewer()
{
	ogCCSDdx.UnRegister(this, NOTUSED);
	
	ASSIGNCONFLICT_LINEDATA *prlFL;
	for (int i = 0; i < omFlightLines.GetSize(); i++)
	{
		prlFL = (ASSIGNCONFLICT_LINEDATA *) omFlightLines.GetAt(i);
		for (int j = 0; j < prlFL->Bars.GetSize(); j++)
		{
			delete (ASSIGNCONFLICT_BARDATA *) (prlFL->Bars.GetAt(j));
		}
		prlFL->Bars.RemoveAll();
		for (j = 0; j < prlFL->BkBars.GetSize(); j++)
		{
			delete (ASSIGNCONFLICT_BARDATA *) (prlFL->BkBars.GetAt(j));
		}
		prlFL->BkBars.RemoveAll();
		delete prlFL;
	}
	omFlightLines.RemoveAll();

	delete pomInboundBrush; 
	delete pomOutboundBrush; 
	delete pomTurnaroundBrush; 
	delete pomQualityBrush;
	delete pomResBrush;
}

void NewAssignConflictViewer::Attach(CWnd *popAttachWnd)
{
	pomAttachWnd = popAttachWnd;
}

static int CompareJobStartTime(const JOBDATA **e1, const JOBDATA **e2)
{
	return (*e1)->Acfr.GetTime() - (*e2)->Acto.GetTime();
}

void NewAssignConflictViewer::Init(void)
{
	RemoveAll();

	int ilRed = 4, ilGreen = 8;

	// if only one bar is dispayed then the status text is always displayed
	// job assigned to a demand counts as a single bar.
	int ilSeperateBarCount = 0;

	ASSIGNCONFLICT_LINEDATA *prlFL;
	ASSIGNCONFLICT_BARDATA *prlBar;

	for (int i = 0; i < pomDemands->GetSize(); i++)
	{
		prlFL = new ASSIGNCONFLICT_LINEDATA;
		//char clBuf[64]; sprintf(clBuf, "Flight Line [%d]", i);
		char clBuf[500]; sprintf(clBuf, GetString(IDS_STRING61536), i);
		prlFL->Text = clBuf;
		prlFL->MaxOverlapLevel = 0;

		DEMANDDATA olDemand = pomDemands->GetAt(i);


		if(olDemand.Ulnk != 0L)
		{
			// create a background bar for the group
			prlBar = new ASSIGNCONFLICT_BARDATA;
			prlBar->FlightUrno = ogDemandData.GetFlightUrno(&olDemand);
			prlBar->Urno = olDemand.Urno;
			prlBar->Text = CString("");
			prlBar->StatusText.Format("Group <%ld>",olDemand.Ulnk);
			ogDemandData.GetOverallTeamTimeForDemand(&olDemand, prlBar->StartTime, prlBar->EndTime);
			prlBar->StartTime -= CTimeSpan(0,0,5,0); 
			prlBar->EndTime += CTimeSpan(0,0,5,0);
			prlBar->FrameType = FRAMENONE;
			prlBar->MarkerType = MARKFULL;
			prlBar->MarkerBrush = omWGBkBars.GetWorkGroupBkColour(olDemand.Ulnk);
			prlBar->OverlapLevel = 0;	// zero for the top-most level, each level lowered by 4 pixels
			prlBar->IsTeamBar = true;
			prlFL->BkBars.Add(prlBar);
		}

		// Pichate CreateBar
		prlBar = new ASSIGNCONFLICT_BARDATA;
		prlBar->FlightUrno = ogDemandData.GetFlightUrno(&olDemand);
		prlBar->Urno = olDemand.Urno;
		if (strcmp(olDemand.Obty,"FID") == 0)
		{
			prlBar->Text = ogBasicData.GetDemandBarText(&olDemand);
		}
		else if (olDemand.Dety[0] == CCI_DEMAND)
		{
			prlBar->Text = ogBasicData.GetDemandBarText(&olDemand);
		}
		else
		{
			prlBar->Text = ogFlightData.GetFlightNum(prlBar->FlightUrno) + CString(" - ") + ogBasicData.GetDemandBarText(&olDemand);
		}

		CString olStatusText;
		olStatusText.Format("%s - %s   %s %s - %s",olDemand.Debe.Format("%H%M/%d"),olDemand.Deen.Format("%H%M/%d"),GetDemandTypeText(olDemand),prlBar->Text,GetRuleEventText(olDemand));

		CString olDemandInfo;
		if(ogDemandData.RuleIndependant(&olDemand))
		{
			ogBasicData.GetDemandInfoAzatab(&olDemand, olDemandInfo);
		}
		else
		{
			ogBasicData.GetDemandInfo(&olDemand, olDemandInfo, false);
		}

		prlBar->StatusText = olStatusText + CString("\n") + olDemandInfo;
		prlBar->StartTime = olDemand.Debe;
		prlBar->EndTime = olDemand.Deen;

		prlBar->FrameType = FRAMERECT;
		if(bmSelectDemand && prmSelectedDemand != NULL && prmSelectedDemand->Urno == olDemand.Urno)
		{
			prlBar->FrameColor = YELLOW;
		}
		prlBar->MarkerType = MARKFULL;

		prlBar->MarkerBrush = pomInboundBrush;

		if(!strcmp(olDemand.Aloc,ALLOCUNITTYPE_GATE))
			prlBar->MarkerBrush = ogBrushs[FIRSTCONFLICTCOLOR+(CFI_GAT_NOTCOVERED*2)];
		else if(!strcmp(olDemand.Aloc,ALLOCUNITTYPE_CIC))
			prlBar->MarkerBrush = ogBrushs[FIRSTCONFLICTCOLOR+(CFI_CIC_NOTCOVERED*2)];
		else if(!strcmp(olDemand.Aloc,ALLOCUNITTYPE_PST))
			prlBar->MarkerBrush = ogBrushs[FIRSTCONFLICTCOLOR+(CFI_PST_NOTCOVERED*2)];
		else if(!strcmp(olDemand.Aloc,ALLOCUNITTYPE_REGN))
			prlBar->MarkerBrush = ogBrushs[FIRSTCONFLICTCOLOR+(CFI_REGN_NOTCOVERED*2)];

		if(*olDemand.Dety == '0')
			prlBar->MarkerBrush = pomTurnaroundBrush;
		else if(*olDemand.Dety == '1')
			prlBar->MarkerBrush = pomInboundBrush;
		else if(*olDemand.Dety == '2')
			prlBar->MarkerBrush = pomOutboundBrush;
		else if(*olDemand.Dety == '4')
			prlBar->MarkerBrush = pomResBrush;
		else if(*olDemand.Dety == '5')
			prlBar->MarkerBrush = pomQualityBrush;
		else
		{
			if(olDemand.Ouri == 0L)
				prlBar->MarkerBrush = pomOutboundBrush;
			else if(olDemand.Ouro == 0L)
				prlBar->MarkerBrush = pomInboundBrush;
			else
				prlBar->MarkerBrush = pomTurnaroundBrush;
		}

		prlBar->OverlapLevel = 0;	// zero for the top-most level, each level lowered by 4 pixels
		//prlBar->Indicators.RemoveAll();

		prlFL->BkBars.Add(prlBar);
		omFixedText = prlBar->StatusText;
		ilSeperateBarCount++;

		CCSPtrArray<JOBDATA> olJobs;
		GetJobsByDemand(olJobs, (*pomDemands)[i].Urno);
		olJobs.Sort(CompareJobStartTime);
		int ilNumJobs = olJobs.GetSize();
		if(ilNumJobs > 1)
		{
			ilSeperateBarCount++; // don't display fixed text if the demand has more than one job
		}
		for (int j = 0; j < ilNumJobs; j++)
		{
			JOBDATA olJob = olJobs[j];

			// Pichate CreateBar
			prlBar = new ASSIGNCONFLICT_BARDATA;
			olJob.Flur = ogDemandData.GetFlightUrno(&olDemand);
			prlBar->FlightUrno = olJob.Flur;
			prlBar->Urno = olJob.Urno;

			CString olJobInfo;
			CString olMissingMsg;
			if(ogRudData.DemandIsOfType(olDemand.Urud, PERSONNELDEMANDS))
			{
				EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(olJob.Peno);
				JOBDATA *prlPoolJob = ogJobData.GetJobByUrno(olJob.Jour);
				SHIFTDATA *prlShift = NULL;
				CString olShiftText, olEmpText;
				if (prlPoolJob != NULL)
				{
					if((prlShift = ogShiftData.GetShiftByUrno(prlPoolJob->Shur)) != NULL)
					{
						olShiftText.Format("%s,%s",prlShift->Egrp,prlShift->Sfca);
					}
					else
					{
						olShiftText.Format(" Shift Not Found (POOLJOB.SHUR <%s> in POOLJOB.URNO)",prlPoolJob->Shur,prlPoolJob->Urno);
					}
					ogBasicData.GetJobInfo(prlPoolJob, olJobInfo);
				}
				else
				{
					olShiftText.Format(" Pool Job Not Found (JOB.JOUR <%d> in JOB.URNO <%d>)",olJob.Jour,olJob.Urno);
				}

				olEmpText = ogEmpData.GetEmpName(prlEmp);

				prlBar->Text.Format("%s %s,%s",ogDataSet.JobBarText(&olJob),olShiftText,olEmpText);

				CStringArray olMissingPermitsAndFunctions;
				if(!ogBasicData.CheckFunctionsAndPermits(prlPoolJob,&olDemand,olMissingPermitsAndFunctions))
				{
					int ilNumConflicts = olMissingPermitsAndFunctions.GetSize();
					for(int ilConflict = 0; ilConflict < ilNumConflicts; ilConflict++)
					{
						olMissingMsg += olMissingPermitsAndFunctions[ilConflict] + CString("\n");
					}
				}
			}
			else if(ogRudData.DemandIsOfType(olDemand.Urud, EQUIPMENTDEMANDS))
			{
				EQUDATA *prlEqu = ogEquData.GetEquByUrno(olJob.Uequ);
				if(prlEqu != NULL)
				{
					prlBar->Text = prlEqu->Enam;
					olJobInfo = prlEqu->Enam;
					if(!ogBasicData.CheckDemandForCorrectEquipment(&olDemand, prlEqu))
					{
						olMissingMsg = GetString(IDS_NOTCORRECTEQUIPMENT) + CString("\n");
						CCSPtrArray <EQUDATA> olEquList;
						ogBasicData.GetEquipmentForDemand(&olDemand, olEquList);
						int ilNumEqu = olEquList.GetSize();
						olMissingMsg += GetString(IDS_REQUIREDEQUIPMENT);
						for(int ilEqu = 0; ilEqu < ilNumEqu; ilEqu++)
						{
							EQUDATA *prlEqu = &olEquList[ilEqu];
							if(ilEqu == 0)
							{
								olMissingMsg += CString(": ");
							}
							else
							{
								olMissingMsg += CString("/");
							}
							olMissingMsg += prlEqu->Enam;
						}
						olMissingMsg += CString("\n");
					}
				}
				else
				{
					olJobInfo.Format(" Equipment not found: JOB.UEQU <%d> JOB.URNO <%d>",olJob.Uequ,olJob.Urno);
				}
			}

			CString olConflicts = ogConflicts.CheckConflicts(olJob, TRUE,FALSE,FALSE,TRUE);
			bool blHasConflicts = false;
			if(!olMissingMsg.IsEmpty() || !olConflicts.IsEmpty())
			{
				blHasConflicts = true;
			}
			else
			{
				olConflicts = GetString(IDS_NOCONFLICTS);
			}

			prlBar->StatusText.Format("%s - %s %s\n%s\n\n%s%s",
				olJob.Acfr.Format("%H%M/%d"),olJob.Acto.Format("%H%M/%d"),prlBar->Text,olJobInfo,olMissingMsg,olConflicts);

			prlBar->StartTime = olJob.Acfr;
			prlBar->EndTime = olJob.Acto;
			prlBar->MarkerType = (olJob.Stat[0] == 'P')? MARKLEFT: MARKFULL;
			prlBar->FrameType = FRAMERECT;
			//prlBar->MarkerBrush = ogBrushs[olJob.ColorIndex];
			prlBar->MarkerBrush = ogBrushs[blHasConflicts ? ilRed : ilGreen];
			prlBar->OverlapLevel = 0;			// zero for the top-most level, each level lowered by 4 pixels
			//prlBar->Indicators.RemoveAll();

			prlFL->Bars.Add(prlBar);
			omFixedText = prlBar->StatusText;
		}

		omFlightLines.Add(prlFL);
	}

	for (int k = 0; k < pomNoDemandJobs->GetSize(); k++, i++)
	{
		prlFL = new ASSIGNCONFLICT_LINEDATA;
		CString olText; 
		olText.Format("Flight Line [%d]", i);
		prlFL->Text = olText;
		prlFL->MaxOverlapLevel = 0;

		JOBDATA olJob = pomNoDemandJobs->GetAt(k);

		// Pichate CreateBar
		prlBar = new ASSIGNCONFLICT_BARDATA;
		prlBar->FlightUrno = ogBasicData.GetFlightUrnoForJob(&olJob);
		prlBar->Urno = olJob.Urno;

		CString olJobInfo;
		if(ogBasicData.JobIsOfType(olJob.Urno, PERSONNELDEMANDS))
		{
			EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(olJob.Peno);
			JOBDATA *prlPoolJob = ogJobData.GetJobByUrno(olJob.Jour);
			SHIFTDATA *prlShift = NULL;
			if (prlPoolJob != NULL)
			{
				prlShift = ogShiftData.GetShiftByUrno(prlPoolJob->Shur);
			}
			if ((prlEmp != NULL) && (prlShift != NULL))
			{
				olText.Format("%s %s,%s,%s",olJob.Alid,prlShift->Egrp,prlShift->Sfca,ogEmpData.GetEmpName(prlEmp));
			}
			if(prlPoolJob != NULL)
			{
				ogBasicData.GetJobInfo(prlPoolJob, olJobInfo);
			}
		}
		else if(ogBasicData.JobIsOfType(olJob.Urno, EQUIPMENTDEMANDS))
		{
			EQUDATA *prlEqu = ogEquData.GetEquByUrno(olJob.Uequ);
			if(prlEqu != NULL)
			{
				olText = prlEqu->Enam;
				olJobInfo = prlEqu->Enam;
			}
			else
			{
				olJobInfo.Format(" Equipment not found: JOB.UEQU <%d> JOB.URNO <%s>",olJob.Uequ,olJob.Urno);
			}
		}
		prlBar->Text = olText;
		CString olConflicts = ogConflicts.CheckConflicts(olJob, TRUE,FALSE,FALSE,TRUE);

		prlBar->StatusText.Format("%s - %s %s\n%s\n\n%s\n%s",olJob.Acfr.Format("%H%M/%d"),olJob.Acto.Format("%H%M/%d"),prlBar->Text,olJobInfo,GetString(IDS_UNASSIGNEDJOB),olConflicts);
		prlBar->StartTime = olJob.Acfr;
		prlBar->EndTime = olJob.Acto;

		prlBar->MarkerType = (olJob.Stat[0] == 'P')? MARKLEFT: MARKFULL;
		prlBar->FrameType = FRAMERECT;
		//prlBar->MarkerBrush = ogBrushs[olJob.ColorIndex];
		prlBar->MarkerBrush = ogBrushs[ilRed];

		prlBar->OverlapLevel = 0;			// zero for the top-most level, each level lowered by 4 pixels
		//prlBar->Indicators.RemoveAll();

		prlFL->Bars.Add(prlBar);
		omFixedText = prlBar->StatusText;
		ilSeperateBarCount++;
		omFlightLines.Add(prlFL);
	}
	for (k = omFlightLines.GetSize();k < 9; k++)
	{
		prlFL = new ASSIGNCONFLICT_LINEDATA;
		prlFL->Text = "";
		prlFL->MaxOverlapLevel = 0;
		omFlightLines.Add(prlFL);
	}

	// if only one bar is dispayed then the status text is always displayed
	// job assigned to a demand counts as a single bar.
	if(ilSeperateBarCount != 1)
	{
		omFixedText.Empty();
	}
}

CString NewAssignConflictViewer::GetRuleEventText(DEMANDDATA &ropDemand)
{
	CString olRueText;

	if(*ropDemand.Dety == '0' || *ropDemand.Dety == '1' || *ropDemand.Dety == '2')
	{
		RUDDATA *prlRud = ogRudData.GetRudByUrno(ropDemand.Urud);
		if(prlRud != NULL)
		{
			RUEDATA *prlRue = ogRueData.GetRueByUrno(prlRud->Urue);
			if(prlRue != NULL)
			{
				olRueText.Format("%s (%s)",prlRue->Rusn,prlRue->Runa);
			}
			else
			{
				olRueText.Format("Rule Event Not Found RUE.URNO <%ld> DEM.URNO <%ld>",prlRud->Urue,ropDemand.Urno);
			}
		}
		else
		{
			olRueText.Format("Rule Demand Not Found RUD.URNO <%ld> DEM.URNO <%ld>",ropDemand.Urud,ropDemand.Urno);
		}
	}
	return olRueText;
}


CString NewAssignConflictViewer::GetDemandTypeText(DEMANDDATA &ropDemand)
{
	CString olDemandTypeText;
	if(*ropDemand.Dety == '0')
	{
		olDemandTypeText = GetString(IDS_TURNAROUNDDEMAND); // Rotazione
	}
	else if(*ropDemand.Dety == '1')
	{
		olDemandTypeText = GetString(IDS_INBOUNDDEMAND); // Arrivo
	}
	else if(*ropDemand.Dety == '2')
	{
		olDemandTypeText = GetString(IDS_OUTBOUNDDEMAND); // Partenza
	}
	else if(*ropDemand.Dety == '3')
	{
		olDemandTypeText = GetString(IDS_P05DEMAND); // P05
	}
	else if(*ropDemand.Dety == '4')
	{
		olDemandTypeText = GetString(IDS_P05RESDEMAND); // P05Res
	}
	else if(*ropDemand.Dety == '5')
	{
		olDemandTypeText = GetString(IDS_CASDEMAND); // Casp
	}

	return olDemandTypeText;
}

int NewAssignConflictViewer::GetLineCount(int ipGroupno)
{
	return omFlightLines.GetSize();
}

ASSIGNCONFLICT_LINEDATA *NewAssignConflictViewer::GetLine(int ipGroupno, int ipLineno)
{
	ASSIGNCONFLICT_LINEDATA *prlFL;
	prlFL = (ASSIGNCONFLICT_LINEDATA *) (omFlightLines.GetAt(ipLineno));
	return prlFL;
}

CString NewAssignConflictViewer::GetLineText(int ipGroupno, int ipLineno)
{
	static char clBuf[255];
	sprintf(clBuf, "[%d:%d]", ipGroupno, ipLineno);
	
	return CString(clBuf);
}

int NewAssignConflictViewer::CreateBar(int ipGroupno, int ipLineno, ASSIGNCONFLICT_BARDATA *prpBar, BOOL bpFrontBar)
{
	ASSIGNCONFLICT_LINEDATA *prlFL;
	prlFL = (ASSIGNCONFLICT_LINEDATA *) (omFlightLines.GetAt(ipLineno));
	ASSIGNCONFLICT_BARDATA *prlBar;
    
	int ilBarCount = prlFL->Bars.GetSize();

    // Search for the position which we want to insert this new bar
    for (int ilBarno = ilBarCount; ilBarno > 0; ilBarno--)
        if (prpBar->StartTime >= GetBar(ipGroupno,ipLineno,ilBarno-1)->StartTime)
            break;  // should be inserted after Bars[ilBarno-1]

	// Pichate CreateBar
	prlBar = new ASSIGNCONFLICT_BARDATA;
	prlBar->FlightUrno = prpBar->FlightUrno;
	prlBar->Urno = prpBar->Urno;
	prlBar->Text = prpBar->Text;
	prlBar->StatusText = prpBar->StatusText;
	prlBar->StartTime = prpBar->StartTime;
	prlBar->EndTime = prpBar->EndTime;
	prlBar->FrameType = prpBar->FrameType;
	prlBar->MarkerType = prpBar->MarkerType;
	prlBar->MarkerBrush = prpBar->MarkerBrush;
	prlBar->OverlapLevel = prpBar->OverlapLevel;			// zero for the top-most level, each level lowered by 4 pixels
	//prlBar->Indicators = prpBar->Indicators;				// CPtrArray
	
	prlFL->Bars.InsertAt(ilBarno, prlBar);

	return ilBarno;
}

void NewAssignConflictViewer::DeleteBar(int ipGroupno, int ipLineno, int ipBarno)
{
	ASSIGNCONFLICT_LINEDATA *prlFL;
	prlFL = (ASSIGNCONFLICT_LINEDATA *)(omFlightLines.GetAt(ipLineno));

	ASSIGNCONFLICT_BARDATA *prlBar;
	prlBar = (ASSIGNCONFLICT_BARDATA *)(prlFL->Bars.GetAt(ipBarno));
	delete prlBar;

	prlFL->Bars.RemoveAt(ipBarno);
}

int NewAssignConflictViewer::GetBarCount(int ipGroupno, int ipLineno)
{
	return ((ASSIGNCONFLICT_LINEDATA *) (omFlightLines.GetAt(ipLineno))) -> Bars.GetSize();
}

ASSIGNCONFLICT_BARDATA *NewAssignConflictViewer::GetBar(int ipGroupno, int ipLineno, int ipBarno)
{
	ASSIGNCONFLICT_LINEDATA *prlFL;
	prlFL = (ASSIGNCONFLICT_LINEDATA *)(omFlightLines.GetAt(ipLineno));

	ASSIGNCONFLICT_BARDATA *prlBar;
	prlBar = (ASSIGNCONFLICT_BARDATA *)(prlFL->Bars.GetAt(ipBarno));
	
	return prlBar;
}

int NewAssignConflictViewer::GetBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2, int ipOverlapLevel1, int ipOverlapLevel2)
{
	ASSIGNCONFLICT_LINEDATA *prlFL;
	prlFL = (ASSIGNCONFLICT_LINEDATA *)(omFlightLines.GetAt(ipLineno));
	ASSIGNCONFLICT_BARDATA *prlBar;
	int ilUB = prlFL->Bars.GetUpperBound();
	for (int i = ilUB; i >= 0; i--)
	{
		prlBar = (ASSIGNCONFLICT_BARDATA *)(prlFL->Bars.GetAt(i));
//		if (IsOverlapped(prlBar->StartTime, prlBar->EndTime, opTime1, opTime2) == TRUE)
        if (opTime1 <= prlBar->EndTime && prlBar->StartTime <= opTime2 &&
            ipOverlapLevel1 <= prlBar->OverlapLevel && prlBar->OverlapLevel <= ipOverlapLevel2)
			break;
	}

	return i;
}

int NewAssignConflictViewer::GetBkBarCount(int ipGroupno, int ipLineno)
{
	return ((ASSIGNCONFLICT_LINEDATA *) (omFlightLines.GetAt(ipLineno))) -> BkBars.GetSize();
}

ASSIGNCONFLICT_BARDATA *NewAssignConflictViewer::GetBkBar(int ipGroupno, int ipLineno, int ipBarno)
{
	ASSIGNCONFLICT_BARDATA *prlBar = NULL;

	if (ipGroupno >= 0 && ipLineno >= 0 && ipBarno >= 0 && ipLineno < omFlightLines.GetSize())
	{
		ASSIGNCONFLICT_LINEDATA *prlFL = (ASSIGNCONFLICT_LINEDATA *) omFlightLines.GetAt(ipLineno);

		if(prlFL != NULL && ipBarno < prlFL->BkBars.GetSize())
		{
			prlBar = (ASSIGNCONFLICT_BARDATA *) prlFL->BkBars.GetAt(ipBarno);
		}
	}
	
	return prlBar;
}

int NewAssignConflictViewer::GetBkBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2, int ipOverlapLevel1, int ipOverlapLevel2)
{
	ASSIGNCONFLICT_LINEDATA *prlFL;
	prlFL = (ASSIGNCONFLICT_LINEDATA *)(omFlightLines.GetAt(ipLineno));
	ASSIGNCONFLICT_BARDATA *prlBar;
	int ilUB = prlFL->BkBars.GetUpperBound();
	for (int i = ilUB; i >= 0; i--)
	{
		prlBar = (ASSIGNCONFLICT_BARDATA *)(prlFL->BkBars.GetAt(i));
		if (IsOverlapped(prlBar->StartTime, prlBar->EndTime, opTime1, opTime2) == TRUE)
			break;
	}

	return i;
}

CString NewAssignConflictViewer::GetEmpBarTextFromTime(int ipLineno, CTime opTime1, CTime opTime2)
{
	CString olText;
	bool blFound = false;
	ASSIGNCONFLICT_LINEDATA *prlFL;
	prlFL = (ASSIGNCONFLICT_LINEDATA *)(omFlightLines.GetAt(ipLineno));
	STAFFDETAIL_BARDATA *prlBar = NULL;
	int ilUB = prlFL->PoolJobBar.Bars.GetUpperBound();
	for (int i = ilUB; i >= 0; i--)
	{
		prlBar = &prlFL->PoolJobBar.Bars[i];
		if (IsOverlapped(prlBar->StartTime, prlBar->EndTime, opTime1, opTime2) == TRUE)
		{
			blFound = true;
			olText = prlBar->Text;
			if(olText.IsEmpty())
				olText = prlBar->StatusText;
			break;
		}
	}

	if(!blFound && IsOverlapped(prlFL->PoolJobBar.StartTime, prlFL->PoolJobBar.EndTime, opTime1, opTime2) == TRUE)
	{
		olText = prlFL->PoolJobBar.Text;
	}

	return olText;
}

ASSIGNCONFLICT_BARDATA *NewAssignConflictViewer::GetBkBarByUrno(long lpUrno)
{
	ASSIGNCONFLICT_BARDATA *prlBkBar = NULL;

	int ilNumLines = omFlightLines.GetSize();
	for(int ilLine = 0; ilLine < ilNumLines; ilLine++)
	{
		ASSIGNCONFLICT_LINEDATA *prlLine = (ASSIGNCONFLICT_LINEDATA *)(omFlightLines.GetAt(ilLine));
		int ilNumBkBars = prlLine->BkBars.GetSize();
		for(int ilBkBar = 0; ilBkBar < ilNumBkBars; ilBkBar++)
		{
			ASSIGNCONFLICT_BARDATA *prlTmpBkBar = (ASSIGNCONFLICT_BARDATA *)(prlLine->BkBars.GetAt(ilBkBar));
			if(prlTmpBkBar->Urno == lpUrno)
			{
				prlBkBar = prlTmpBkBar;
			}
		}
	}

	return prlBkBar;
}

bool NewAssignConflictViewer::FindBkBarByUrno(long lpUrno, int &ripLineno, int &ripBarno)
{
	ripLineno = ripBarno = -1;

	int ilNumLines = omFlightLines.GetSize();
	for(int ilLine = 0; ilLine < ilNumLines; ilLine++)
	{
		ASSIGNCONFLICT_LINEDATA *prlLine = (ASSIGNCONFLICT_LINEDATA *)(omFlightLines.GetAt(ilLine));
		int ilNumBkBars = prlLine->BkBars.GetSize();
		for(int ilBkBar = 0; ilBkBar < ilNumBkBars; ilBkBar++)
		{
			ASSIGNCONFLICT_BARDATA *prlTmpBkBar = (ASSIGNCONFLICT_BARDATA *)(prlLine->BkBars.GetAt(ilBkBar));
			if(prlTmpBkBar->Urno == lpUrno)
			{
				ripLineno = ilLine;
				ripBarno = ilBkBar;
				return true;
			}
		}
	}

	return false;
}

bool NewAssignConflictViewer::FindBreakJobLine(int &ripLineno)
{
	ripLineno = -1;

	int ilNumLines = omFlightLines.GetSize();
	for(int ilLine = 0; ilLine < ilNumLines; ilLine++)
	{
		ASSIGNCONFLICT_LINEDATA *prlLine = (ASSIGNCONFLICT_LINEDATA *)(omFlightLines.GetAt(ilLine));
		if(prlLine->IsBreakJobLine)
		{
			ripLineno = ilLine;
			return true;
		}
	}

	return false;
}
	
int NewAssignConflictViewer::CreateIndicator(int ipGroupno, int ipLineno, int ipBarno, ASSIGNCONFLICT_INDICATORDATA *prpIndicator)
{
	return 0;
}

int NewAssignConflictViewer::GetIndicatorCount(int ipGroupno, int ipLineno, int ipBarno)
{
	return 0;
}

ASSIGNCONFLICT_INDICATORDATA *NewAssignConflictViewer::GetIndicator(int ipGroupno, int ipLineno, int ipBarno, int ipIndicatorno)
{
	return NULL;
}

static void NewAssignConflictCf(void *popInstance, int ipDDXType,
	void *vpDataPointer, CString &ropInstanceName)
{
    NewAssignConflictViewer *polViewer = (NewAssignConflictViewer *)popInstance;

	if (ipDDXType == FLIGHT_CHANGE)
		polViewer->ProcessFlightChange((FLIGHTDATA *)vpDataPointer);
	if (ipDDXType == JOB_CHANGE)
		polViewer->ProcessJobChange((JOBDATA *)vpDataPointer);
}

void NewAssignConflictViewer::ProcessFlightChange(FLIGHTDATA *prpFlight)
{
	if (!::IsWindow(pomAttachWnd->GetSafeHwnd()))
		return;

//	((NewAssignConflictWindow *)pomAttachWnd)->Initialize(prpFlight->Urno,FALSE);
}

void NewAssignConflictViewer::ProcessJobChange(JOBDATA *prpJob)
{
	if (!::IsWindow(pomAttachWnd->GetSafeHwnd()))
		return;

//	((NewAssignConflictWindow *)pomAttachWnd)->Initialize(prpJob->Flur,FALSE);
}

void NewAssignConflictViewer::GetJobsByDemand(CCSPtrArray<JOBDATA> &ropJobs, long lpDemandUrno)
{
	int ilNumJobs = pomJobs->GetSize();
	if(ogJodData.bmUseJodtab)
	{
		int ilNumJods = pomJods->GetSize();
		for(int ilJod = 0; ilJod < ilNumJods; ilJod++)
		{
			JODDATA *prlJod = &(*pomJods)[ilJod];
			if(prlJod->Udem == lpDemandUrno)
			{
				for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
				{
					JOBDATA *prlJob = &(*pomJobs)[ilJob];
					if(prlJod->Ujob == prlJob->Urno)
					{
						ropJobs.Add(prlJob);
						break;
					}
				}
			}
		}
	}
	else
	{
		for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
		{
			JOBDATA *prlJob = &(*pomJobs)[ilJob];
			if(prlJob->Udem == lpDemandUrno)
			{
				ropJobs.Add(prlJob);
			}
		}
	}
}

JOBDATA *NewAssignConflictViewer::GetJobByUrno(long lpJobUrno)
{
	JOBDATA *prlJob = NULL;
	int ilNumJobs = pomJobs->GetSize(), ilJob;
	for(ilJob = 0; ilJob < ilNumJobs; ilJob++)
	{
		JOBDATA *prlTmpJob = &(*pomJobs)[ilJob];
		if(prlTmpJob->Urno == lpJobUrno)
		{
			prlJob = prlTmpJob;
			break;
		}
	}
	if(prlJob == NULL)
	{
		ilNumJobs = pomNoDemandJobs->GetSize();
		for(ilJob = 0; ilJob < ilNumJobs; ilJob++)
		{
			JOBDATA *prlTmpJob = &(*pomNoDemandJobs)[ilJob];
			if(prlTmpJob->Urno == lpJobUrno)
			{
				prlJob = prlTmpJob;
				break;
			}
		}
	}

	return prlJob;
}

DEMANDDATA *NewAssignConflictViewer::GetDemandByUrno(long lpDemandUrno)
{
	DEMANDDATA *prlDemand = NULL;
	int ilNumDemands = pomDemands->GetSize(), ilDemand;
	for(ilDemand = 0; ilDemand < ilNumDemands; ilDemand++)
	{
		DEMANDDATA *prlTmpDemand = &(*pomDemands)[ilDemand];
		if(prlTmpDemand->Urno == lpDemandUrno)
		{
			prlDemand = prlTmpDemand;
			break;
		}
	}

	return prlDemand;
}

void NewAssignConflictViewer::GetJodsByJob(CCSPtrArray <JODDATA> &ropJods,long lpJobUrno)
{
	int ilNumJods = pomJods->GetSize();
	for(int ilJod = 0; ilJod < ilNumJods; ilJod++)
	{
		JODDATA *prlJod = &(*pomJods)[ilJod];
		if(prlJod->Ujob == lpJobUrno)
		{
			ropJods.Add(prlJod);
		}
	}
}

void NewAssignConflictViewer::DeleteJodsByJobUrno(long lpJobUrno)
{
	JOBDATA *prlJobToChange = GetJobByUrno(lpJobUrno);
	if(prlJobToChange != NULL)
	{
		if(ogJodData.bmUseJodtab)
		{
			int ilNumJods = pomJods->GetSize();
			for(int ilJod = (ilNumJods-1); ilJod >= 0; ilJod--)
			{
				JODDATA *prlJod = &(*pomJods)[ilJod];
				if(prlJod->Ujob == lpJobUrno)
				{
					pomJods->DeleteAt(ilJod);
					int ilNumJobs = pomJobs->GetSize();
					for(int ilJob = (ilNumJobs-1); ilJob >= 0; ilJob--)
					{
						JOBDATA *prlJob = &(*pomJobs)[ilJob];
						if(prlJob->Urno == lpJobUrno)
						{
							// job is now unassigned
							pomNoDemandJobs->Add(prlJob);
							pomJobs->RemoveAt(ilJob);
						}
					}
				}
			}
		}
		else
		{
			prlJobToChange->Udem = 0L;
		}
	}
}

void NewAssignConflictViewer::AssignJobToDemand(long lpJobUrno, long lpDemUrno)
{
	JOBDATA *prlJobToChange = GetJobByUrno(lpJobUrno);
	DEMANDDATA *prlDem = GetDemandByUrno(lpDemUrno);

	if(prlJobToChange != NULL && prlDem != NULL)
	{
		int ilRc = IDYES;
		
		CStringArray olConflicts;
		bool blConflict = false;
		if(ogRudData.DemandIsOfType(prlDem->Urud, PERSONNELDEMANDS))
		{
			JOBDATA *prlPoolJob = ogJobData.GetJobByUrno(prlJobToChange->Jour);
			if(prlPoolJob != NULL && !ogBasicData.CheckFunctionsAndPermits(prlPoolJob,prlDem,olConflicts))
			{
				blConflict = true;
			}
		}
		else if(ogRudData.DemandIsOfType(prlDem->Urud, EQUIPMENTDEMANDS))
		{
			EQUDATA *prlEqu = ogEquData.GetEquByUrno(prlJobToChange->Uequ);
			if(prlEqu != NULL && !ogBasicData.CheckDemandForCorrectEquipment(prlDem, prlEqu))
			{
				olConflicts.Add(GetString(IDS_NOTCORRECTEQUIPMENT));
				blConflict = true;
			}
		}

		if(blConflict)
		{
			// "Error assigning the job to the demand!"
			CString olCaption = GetString(IDS_ASSIGNERROR1);
			CString olMessage;

			int ilNumConflicts = olConflicts.GetSize();
			for(int ilConflict = 0; ilConflict < ilNumConflicts; ilConflict++)
			{
				olMessage += olConflicts[ilConflict] + CString("\n");
			}

			// "Do you want to assign it anyway?"
			olMessage += CString("\n") + GetString(IDS_ASSIGNERROR2);

			if(ogBasicData.bmDisplayAssignConflictMessage)
			{
				if(pomAttachWnd != NULL)
				{
					ilRc = pomAttachWnd->MessageBox(olMessage,olCaption,MB_ICONWARNING|MB_YESNO);
				}
				else
				{
					ilRc = MessageBox(NULL,olMessage,olCaption,MB_ICONWARNING|MB_YESNO);
				}
			}
		}

		if(ilRc == IDYES)
		{
			bool blUpdated = false;
			int ilNumJods = pomJods->GetSize();
			if(ogJodData.bmUseJodtab)
			{
				for(int ilJod = 0; ilJod < ilNumJods; ilJod++)
				{
					// job will be reassigned from one demand to another
					JODDATA *prlJod = &(*pomJods)[ilJod];
					if(prlJod->Ujob == lpJobUrno)
					{
						prlJod->Udem = lpDemUrno;
						blUpdated = true;
					}
				}
			}
			else
			{
				if(prlJobToChange->Udem != 0L)
					blUpdated= true;
			}
			prlJobToChange->Udem = prlDem->Urno;

			if(!blUpdated)
			{
				// job was previously unassigned so assign it to a demand
				if(ogJodData.bmUseJodtab)
				{
					JODDATA rlJod;
					rlJod.Udem = lpDemUrno;
					rlJod.Ujob = lpJobUrno;
					pomJods->NewAt(ilNumJods,rlJod);
				}
				int ilNumJobs = pomNoDemandJobs->GetSize();
				for(int ilJob = (ilNumJobs-1); ilJob >= 0; ilJob--)
				{
					JOBDATA *prlJob = &(*pomNoDemandJobs)[ilJob];
					if(prlJob->Urno == lpJobUrno)
					{
						pomJobs->Add(prlJob);
						pomNoDemandJobs->RemoveAt(ilJob);
						blUpdated = true;
					}
				}
			}

			if(blUpdated)
			{
				prlJobToChange->Acfr = prlDem->Debe;
				prlJobToChange->Acto = prlDem->Deen;
				prlJobToChange->Flur = ogDemandData.GetFlightUrno(prlDem); // e.g. may have dragged from outbound and dropped on inbound demand
				strcpy(prlJobToChange->Alid, prlDem->Alid);
				strcpy(prlJobToChange->Aloc,prlDem->Aloc);
				prlJobToChange->Ualo = ogAloData.GetAloUrnoByName(prlJobToChange->Aloc);
				prlJobToChange->Uaid = ogBasicData.GetUaidForJob(prlJobToChange->Alid,prlJobToChange->Ualo);
				strcpy(prlJobToChange->Dety, prlDem->Dety);
				prlJobToChange->Utpl = ogDataSet.GetUtplByDemand(prlDem);
			}
		}
	}
}


DEMANDDATA *NewAssignConflictViewer::SelectBkBar(int ipGroupno, int ipLineno, int ipBarno)
{
	bool blSelected = false;
	if(bmSelectDemand)
	{
		ASSIGNCONFLICT_BARDATA *prlBar = NULL;
		if(prmSelectedDemand != NULL && (prlBar = GetBkBarByUrno(prmSelectedDemand->Urno)) != NULL)
		{
			prlBar->FrameColor = BLACK;
		}
		
		ASSIGNCONFLICT_BARDATA *prlBkBar = GetBkBar(ipGroupno, ipLineno, ipBarno);
		if(prlBkBar != NULL)
		{
			DEMANDDATA *prlDem = ogDemandData.GetDemandByUrno(prlBkBar->Urno);
			if(prlDem != NULL && (prmSelectedDemand == NULL || prlDem->Urno != prmSelectedDemand->Urno) && 
				(prlBar = GetBkBarByUrno(prlDem->Urno)) != NULL)
			{
				prmSelectedDemand = prlDem;
				prlBar->FrameColor = YELLOW;
				blSelected = true;
			}
		}
	}

	if(!blSelected)
	{
		prmSelectedDemand = NULL;
	}

	return prmSelectedDemand;
}

void NewAssignConflictViewer::CanSelectDemands(DEMANDDATA *prpDefaultDemand)
{
	prmSelectedDemand = prpDefaultDemand;
	bmSelectDemand = true;
}


// insert break bar(s) above the selected demand
int NewAssignConflictViewer::InsertBreakJobLine(long lpSelectedDemandUrno, CCSPtrArray <JOBDATA> &ropOverlappingBreakJobs)
{
	int ilLine = -1, ilBar = -1;
	if(FindBkBarByUrno(lpSelectedDemandUrno, ilLine, ilBar))
	{
		ASSIGNCONFLICT_LINEDATA *prlFL = new ASSIGNCONFLICT_LINEDATA;
		prlFL->Text = "";
		prlFL->MaxOverlapLevel = 0;
		prlFL->IsBreakJobLine = true;

		int ilNumBJ = ropOverlappingBreakJobs.GetSize();
		for(int ilBJ = 0; ilBJ < ilNumBJ; ilBJ++)
		{
			JOBDATA *prlBJ = &ropOverlappingBreakJobs[ilBJ];
			ASSIGNCONFLICT_BARDATA *prlBar = new ASSIGNCONFLICT_BARDATA;
			prlBar->StartTime = prlBJ->Acfr;
			prlBar->EndTime = prlBJ->Acto;
			prlBar->Urno = prlBJ->Urno;
			prlBar->TextColor = ogColors[ogConflicts.GetJobConflictColor(prlBJ)];
			prlBar->MarkerBrush = ogBrushs[21]; // break bitmap whose colour depends on prlBar->PauseColour
			prlBar->FrameType = FRAMERECT;
			prlBar->FrameColor = BLACK;
			prlBar->MarkerType = (prlBJ->Stat[0] == 'P')? MARKLEFT: MARKFULL;
			prlFL->BkBars.Add(prlBar);
		}

		omFlightLines.InsertAt(ilLine,prlFL);
	}

	return ilLine;
}

void NewAssignConflictViewer::InsertShiftLines(long lpSelectedDemandUrno, long lpShiftUrno, CUIntArray &ropNewLines)
{
	int ilLine = -1, ilBar = -1;
	if(FindBkBarByUrno(lpSelectedDemandUrno, ilLine, ilBar))
	{
		CTime olEndTime = omEndTime - CTimeSpan(0,0,5,0); // 5 mins for the vertical scroll bar

		CCSPtrArray<JOBDATA> olPoolJobs;
		ogJobData.GetJobsByShur(olPoolJobs,lpShiftUrno,TRUE);
		olPoolJobs.Sort(CompareJobStartTime);
		for (int i = 0; i < olPoolJobs.GetSize(); i++)
		{
			JOBDATA *prlPJ = &olPoolJobs[i];

			CCSPtrArray<JOBDATA> olJobs;
			ogJobData.GetJobsByPoolJob(olJobs,prlPJ->Urno);
			olJobs.Sort(CompareJobStartTime);

			ASSIGNCONFLICT_LINEDATA *prlShiftLine = new ASSIGNCONFLICT_LINEDATA;
			prlShiftLine->Text = "";
			prlShiftLine->MaxOverlapLevel = 1;
			prlShiftLine->IsPoolJobLine = true;

			prlShiftLine->PoolJobBar.Text.Format("%s  %s-%s", prlPJ->Alid, prlPJ->Acfr.Format("%H%M/%d"), prlPJ->Acto.Format("%H%M/%d"));
			prlShiftLine->PoolJobBar.MaxOverlapLevel = 10;
			prlShiftLine->PoolJobBar.StartTime = prlPJ->Acfr;
			prlShiftLine->PoolJobBar.EndTime = prlPJ->Acto;
			prlShiftLine->PoolJobBar.PoolJobUrno = prlPJ->Urno;
			prlShiftLine->PoolJobBar.OfflineBar = (!bgOnline && (ogJobData.JobChangedOffline(prlPJ) || ogJodData.JodChangedOffline(prlPJ->Urno)));

			omFlightLines.InsertAt(ilLine,prlShiftLine);
			ropNewLines.Add(ilLine);
	
			for (int j = 0; j < olJobs.GetSize(); j++)
			{
				JOBDATA *prlJob = &olJobs[j];

				// avoid unexplainable thick shift bar due to overlapping jobs outside the timeframe
				if(!IsOverlapped(omStartTime, olEndTime, prlJob->Acfr, prlJob->Acto))
					continue;

				// Pichate CreateBar
				STAFFDETAIL_BARDATA *prlBar = new STAFFDETAIL_BARDATA;
				prlBar->StaffUrno = ogBasicData.GetFlightUrnoForJob(prlJob);
				prlBar->Urno = prlJob->Urno;

				CString olAcfr = prlJob->Acfr.Format("%H%M/%d");
				CString olActo = prlJob->Acto.Format("%H%M/%d");
				if(!strcmp(prlJob->Jtco, JOBBREAK))
				{
					prlBar->Text = "";
					prlBar->StatusText.Format("%s %s-%s",	ogDataSet.JobBarText(prlJob), olAcfr, olActo);
					prlBar->IsPause = true;
					prlBar->PauseColour = ogColors[ogConflicts.GetJobConflictColor(prlJob)];
					prlBar->MarkerBrush = ogBrushs[21];
				}
				else
				{
					prlBar->Text.Format("%s %s-%s",	ogDataSet.JobBarText(prlJob), olAcfr, olActo);
					prlBar->StatusText = prlBar->Text;
					prlBar->MarkerBrush = ogBrushs[ogConflicts.GetJobConflictColor(prlJob)];
				}

				prlBar->FrameType = FRAMERECT;
				prlBar->FrameColor = (!strcmp(prlJob->Jtco, JOBBREAK) && *prlJob->Ignr == '1') ? WHITE : BLACK;

				prlBar->StartTime = prlJob->Acfr;
				prlBar->EndTime = prlJob->Acto;
				prlBar->MarkerType = (prlJob->Stat[0] == 'P')? MARKLEFT: MARKFULL;
				prlBar->OfflineBar = !bgOnline && (ogJobData.JobChangedOffline(prlJob) || ogJodData.JodChangedOffline(prlJob->Urno));

				if(prlJob->Stat[0] == 'P' && prlJob->Infm)
				{
					prlBar->Infm = true;
				}
				else
				{
					prlBar->Infm = false;
				}


				CUIntArray olBarnoList;
				GetOverlappedBarsFromTime(0, ilLine, prlBar->StartTime, prlBar->EndTime, olBarnoList);
				int ilListCount = olBarnoList.GetSize();
				for (int ilLc = 0; ilLc < ilListCount; ilLc++)
					prlShiftLine->MaxOverlapLevel = max(prlShiftLine->MaxOverlapLevel, ++prlShiftLine->PoolJobBar.Bars[olBarnoList[ilLc]].OverlapLevel);

				CCSPtrArray <STAFFDETAIL_BARDATA> olOverlappingBars;
				int ilNumOverlappingBars = GetOverlappingBars(&prlShiftLine->PoolJobBar, prlBar->StartTime, prlBar->EndTime, olOverlappingBars);
				for(int ilBar = 0; ilBar < ilNumOverlappingBars; ilBar++)
				{
					STAFFDETAIL_BARDATA *prlBar = &olOverlappingBars[ilBar];
					if(prlBar->OverlapLevel == prlBar->OverlapLevel)
					{
						prlBar->OverlapLevel += 4;
					}

					if(prlBar->OverlapLevel > prlShiftLine->PoolJobBar.MaxOverlapLevel)
					{
						prlShiftLine->PoolJobBar.MaxOverlapLevel = prlBar->OverlapLevel;
					}
				}
				prlShiftLine->PoolJobBar.Bars.Add(prlBar);
			}
		}
	}
}

void NewAssignConflictViewer::GetOverlappedBarsFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList)
{
    ASSIGNCONFLICT_LINEDATA *prlLine = GetLine(ipGroupno, ipLineno);
    int ilBarCount = prlLine->PoolJobBar.Bars.GetSize();

	// Prepare the empty array of the involvement flag for each bar
	CCSPtrArray<BOOL> IsInvolvedBar;
	for (int i = 0; i < ilBarCount; i++)
		IsInvolvedBar.NewAt(i, FALSE);

	// We will loop again and again until there is no more bar in this overlapping found.
	// The overlapping group is at first determine by the given "opTime1" and "opTime2".
	// But if the bar we find is exceed this period of time, we will extend searching
	// to both left side and right side in the next round.
	//
	int nBarsFoundThisRound = -1;
	while (nBarsFoundThisRound != 0)
	{
		nBarsFoundThisRound = 0;
		for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
		{
			CTime olStartTime = prlLine->PoolJobBar.Bars[ilBarno].StartTime;
			CTime olEndTime = prlLine->PoolJobBar.Bars[ilBarno].EndTime;
			BOOL blIsOverlapped = IsReallyOverlapped(olStartTime, olEndTime, opTime1, opTime2);
			BOOL blIsWithIn = IsWithIn(olStartTime, olEndTime, opTime1, opTime2);
				// this IsWithIn() fix the real thin bar bug
			BOOL blIsInvolved = (blIsOverlapped || blIsWithIn);

			if (!IsInvolvedBar[ilBarno] && blIsInvolved)	// some more new involved bars?
			{
				IsInvolvedBar[ilBarno] = TRUE;
				nBarsFoundThisRound++;
				opTime1 = min(opTime1, prlLine->PoolJobBar.Bars[ilBarno].StartTime);
				opTime2 = max(opTime2, prlLine->PoolJobBar.Bars[ilBarno].EndTime);
			}
		}
	}

	// Create the list of involved bars, then store them to "ropBarnoList"
	ropBarnoList.RemoveAll();
	for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
	{
		if (IsInvolvedBar[ilBarno])
		{
			ropBarnoList.Add(ilBarno);
		}
	}
	IsInvolvedBar.DeleteAll();
}

int NewAssignConflictViewer::GetOverlappingBars(STAFFDETAIL_LINEDATA *prpLine, CTime opStartTime, CTime opEndTime, CCSPtrArray <STAFFDETAIL_BARDATA> &ropOverlappingBars)
{
	if(prpLine != NULL)
	{
		int ilNumBars = prpLine->Bars.GetSize();
		for(int ilBar = 0; ilBar < ilNumBars; ilBar++)
		{
			STAFFDETAIL_BARDATA *prlBar = &prpLine->Bars[ilBar];
			if(IsReallyOverlapped(opStartTime, opEndTime, prlBar->StartTime, prlBar->EndTime))
			{
				ropOverlappingBars.Add(prlBar);
			}
		}
	}

	return ropOverlappingBars.GetSize();
}

// remove break bar(s)
int NewAssignConflictViewer::RemoveBreakJobLine()
{
	int ilLine = -1;
	if(FindBreakJobLine(ilLine))
	{
		ASSIGNCONFLICT_LINEDATA *prlFL = (ASSIGNCONFLICT_LINEDATA *)omFlightLines[ilLine];
		int ilNumBJ = prlFL->BkBars.GetSize();
		for(int ilBJ = 0; ilBJ < ilNumBJ; ilBJ++)
		{
			ASSIGNCONFLICT_BARDATA *prlBkBar = (ASSIGNCONFLICT_BARDATA *)prlFL->BkBars[ilBJ];
			delete prlBkBar;
		}
		prlFL->BkBars.RemoveAll();
		delete prlFL;
		omFlightLines.RemoveAt(ilLine);
	}

	return ilLine;
}

// remove shift bar(s)
void NewAssignConflictViewer::RemoveShiftLines(CUIntArray &ropLines)
{
	int ilNumLines = omFlightLines.GetSize();
	for(int ilLine = (ilNumLines-1); ilLine >= 0; ilLine--)
	{
		ASSIGNCONFLICT_LINEDATA *prlLine = (ASSIGNCONFLICT_LINEDATA *)omFlightLines[ilLine];
		if(prlLine->IsPoolJobLine)
		{
			prlLine->PoolJobBar.Bars.DeleteAll();
			prlLine->PoolJobBar.BkBars.DeleteAll();
			prlLine->PoolJobBar.FastLinkBkBars.DeleteAll();
			delete prlLine;
			omFlightLines.RemoveAt(ilLine);
			ropLines.Add(ilLine);
		}
	}
}

void NewAssignConflictViewer::SetDisplayTimeframe(CTime opStartTime, CTime opEndTime)
{
	omStartTime = opStartTime;
	omEndTime = opEndTime;
}
