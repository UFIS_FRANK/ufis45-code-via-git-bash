// FlightScheduleViewer.h
//
#ifndef __CFVIEWER_H__
#define __CFVIEWER_H__

#include <CCSPtrArray.h>
#include <conflict.h>
#include <cviewer.h>
#include <table.h>

enum enumConflictTableType{CFLTABLE_CONFLICT,CFLTABLE_ATTENTION};

struct CONFLICTTABLE_LINEDATA
{
	int		Type;
	int		Weight;
	CString NameOfObject;
	CString Alid;
	CTime	TimeOfConflict;
	CString Description;
	long Urno;        // Urno of the the job causing the conflict
	int	TypeOfObject; // Type of Object causing the conflict (FLIGHT or JOB)
	int ReturnFlightType; // ID_NOT_RETURNFLIGHT/ID_ARR_RETURNFLIGHT/ID_DEP_RETURNFLIGHT

	CONFLICTTABLE_LINEDATA(void)
	{
		ReturnFlightType = ID_NOT_RETURNFLIGHT;
	}
};

class ConflictTableViewer: public CViewer
{
// Constructions
public:
    ConflictTableViewer();
    ~ConflictTableViewer();

    void Attach(CTable *popAttachWnd);
    void ChangeViewTo(const char *pcpViewName);
	void SetTableType(int ipTableType);
//	void PrintView();
	CTime omStartTime,omEndTime;

	BOOL IsPassFilter(CONFLICTENTRY  *prpConflictEntry, int ipCflIndex);
	void ProcessConflictDelete(CONFLICTID *prpConflictId);
	void ProcessConflictChange(CONFLICTID *prpConflictId);
	int FindConflictLine(long lpUrno, int ipType);
	int Lines() const {return omLines.GetSize();} //Singapore

// Internal data processing routines
private:
    void PrepareFilter();
	void PrepareSorter();

    void MakeLines();
	void MakeLine(CONFLICTENTRY  *prpConflictEntry, int ipCflIndex);
	BOOL IsPassFilter(long lpUrno,int ipType, int ipTypeOfObject, const char *pcpAloc,const char *pcpAlid);
	int CreateLine(CONFLICTTABLE_LINEDATA *prpLine);
	int CompareConflicts(CONFLICTTABLE_LINEDATA *prpLine1,CONFLICTTABLE_LINEDATA *prpLine2);


	void DeleteAll();
	void DeleteLine(int ipLineno);

private:
	static void ConflictTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
	void UpdateDisplay();
	CString Format(CONFLICTTABLE_LINEDATA *prpLine);
	const char *AlocFromAllocGroupType(const char *pcpAllocGroupType);
	static const char*	omAllocGroupTypes[];


// Attributes used for filtering condition
private:
    int omGroupBy;              // enumerated value -- define in "cfviewer.cpp" (use first sorting)
    CWordArray omSortOrder;     // array of enumerated value -- defined in "cfviewer.cpp"
	bool bmSortTimeAscending;
	CMapStringToPtr omCMapForTypes;		// Conflict type
	CMapWordToPtr omCMapForTypeOfObject;		// object type (flight,job)
	CMapStringToPtr omCMapForAloc;
	CMapStringToPtr	omCMapForAlids;
	bool			bmUseAllAlocs;
	CMapStringToPtr	bmUseAllAlids;
	CMapStringToPtr	omCMapForAlcd;
	CMapStringToPtr	omCMapForAgents;
	BOOL			bmUseAllAgents;
    CString omDate;
	BOOL bmTableType;
	CMapPtrToPtr omAttentionMap;

	CMapStringToPtr	omCMapForPools;
	bool			bmUseAllPools;
	CMapStringToPtr	omCMapForFunctions;
	bool			bmUseAllFunctions;

// Attributes
private:
    CTable *pomTable;
    CCSPtrArray<CONFLICTTABLE_LINEDATA> omLines;

// Methods which handle changes (from Data Distributor)
public:
};

#endif //__CFVIEWER_H__
