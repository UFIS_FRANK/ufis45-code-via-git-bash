// CedaBsdData.h - Shift Code Stammdaten
#ifndef _CEDABSDDATA_H_
#define _CEDABSDDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

 // Data fields from table BSDTAB - Allocation Types
struct BsdDataStruct 
{
	long	Urno;		// Unique record number
	char	Bsdc[9];	// Shift Code
	char	Fctc[6];	// Function Code

	BsdDataStruct(void)
	{
		Urno = 0L;
		strcpy(Bsdc,"");
		strcpy(Fctc,"");
	}
};
typedef struct BsdDataStruct BSDDATA;

/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaBsdData: public CCSCedaData
{

public:
    CCSPtrArray<BSDDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapStringToPtr omCodeMap;
	CString GetTableName(void);

public:
    CedaBsdData();
    ~CedaBsdData();
	void ClearAll();
	BOOL ReadBsdData();
	BOOL AddBsdInternal(BSDDATA *prpBsd);
	BSDDATA *GetBsdByUrno(long lpUrno);
	BSDDATA *GetBsdByCode(char *pcpCode);
};

extern CedaBsdData ogBsdData;
#endif
