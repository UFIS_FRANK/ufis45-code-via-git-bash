// CedaJtyData.cpp - Class for job types
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaJtyData.h>
#include <BasicData.h>

CedaJtyData::CedaJtyData()
{                  
    BEGIN_CEDARECINFO(JTYDATA, JtyDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Name,"NAME")
		FIELD_CHAR_TRIM(Dscr,"DSCR")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(JtyDataRecInfo)/sizeof(JtyDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&JtyDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"JTYTAB");
    pcmFieldList = "URNO,NAME,DSCR";
}
 
CedaJtyData::~CedaJtyData()
{
	TRACE("CedaJtyData::~CedaJtyData called\n");
	ClearAll();
}

void CedaJtyData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
}

CCSReturnCode CedaJtyData::ReadJtyData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];
    strcpy(pclWhere,"");
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaJtyData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		JTYDATA rlJtyData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlJtyData)) == RCSuccess)
			{
				AddJtyInternal(rlJtyData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaJtyData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(JTYDATA), pclWhere);
    return ilRc;
}


void CedaJtyData::AddJtyInternal(JTYDATA &rrpJty)
{
	JTYDATA *prlJty = new JTYDATA;
	*prlJty = rrpJty;
	omData.Add(prlJty);
	omUrnoMap.SetAt((void *)prlJty->Urno,prlJty);
}


JTYDATA *CedaJtyData::GetJtyByUrno(long lpUrno)
{
	JTYDATA *prlJty = NULL;
	omUrnoMap.Lookup((void *)lpUrno,(void *& )prlJty);
	return prlJty;
}

CString CedaJtyData::GetJtyNameByUrno(long lpUrno)
{
	CString olJtyName;
	JTYDATA *prlJty = GetJtyByUrno(lpUrno);
	if(prlJty != NULL)
	{
		olJtyName = prlJty->Name;
	}
	return olJtyName;
}

long CedaJtyData::GetJtyUrnoByName(const char *pcpName)
{
	long llUrno = 0L;
	int ilNumJtys = omData.GetSize();
	for(int ilJty = 0; llUrno == 0L && ilJty < ilNumJtys; ilJty++)
	{
		if(!strcmp(omData[ilJty].Name,pcpName))
		{
			llUrno = omData[ilJty].Urno;
		}
	}
	return llUrno;
}

CString CedaJtyData::GetTableName(void)
{
	return CString(pcmTableName);
}