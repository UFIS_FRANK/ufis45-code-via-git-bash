#if !defined(AFX_STAFFTABLEFILTERPAGE_H__2E68A4D3_DDEE_11D7_8272_00010215BFE5__INCLUDED_)
#define AFX_STAFFTABLEFILTERPAGE_H__2E68A4D3_DDEE_11D7_8272_00010215BFE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StaffTableFilterPage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CStaffTableFilterPage dialog

class CStaffTableFilterPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CStaffTableFilterPage)
// Construction
public:
	CStaffTableFilterPage();

// Dialog Data
	//{{AFX_DATA(CStaffTableFilterPage)
	enum { IDD = IDD_STAFFTABLE_FILTER_PAGE };
	CButton	m_ShiftBeginCtrl;
	CButton	m_ShiftEndCtrl;
	CButton	m_ShiftOverlapCtrl;
	CButton	m_HideAbsentEmpsCtrl;
	CButton	m_HideFidEmpsCtrl;
	CButton	m_HideStandbyEmpsCtrl;
	BOOL	m_HideAbsentEmps;
	BOOL	m_HideFidEmps;
	BOOL	m_HideStandbyEmps;
	int		m_ShiftTime;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStaffTableFilterPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CStaffTableFilterPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STAFFTABLEFILTERPAGE_H__2E68A4D3_DDEE_11D7_8272_00010215BFE5__INCLUDED_)
