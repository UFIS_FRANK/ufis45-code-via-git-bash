// SearchResults.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <SearchResults.h>
#include <CCSGLOBL.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSearchResults dialog


CSearchResults::CSearchResults(CWnd* pParent /*=NULL*/)
	: CDialog(CSearchResults::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSearchResults)
	//}}AFX_DATA_INIT
	prmMatch = NULL;
}

CSearchResults::~CSearchResults()
{
    omData.RemoveAll();
}

void CSearchResults::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchResults)
	DDX_Control(pDX, IDC_SEARCHRESULTSLIST, m_SearchResultListCtrl);
	DDX_Control(pDX, IDC_TITLEFIELD, m_TitleFieldCtrl);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSearchResults, CDialog)
	//{{AFX_MSG_MAP(CSearchResults)
	ON_LBN_DBLCLK(IDC_SEARCHRESULTSLIST, OnDblclkSearchresultslist)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSearchResults message handlers

BOOL CSearchResults::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	CString olCaption;
	olCaption.Format("%s \"%s\"",GetString(IDS_SEARCHRESULTSCAPTION),omSearchString);
	SetWindowText(olCaption);

	m_TitleFieldCtrl.SetWindowText(GetString(IDS_SEARCHRESULTSMSG));

	int ilLine;
	for(int ilC = 0; ilC < omData.GetSize(); ilC++)
	{
		MATCHEDDATA *prlMatch = &omData[ilC];
		if((ilLine = m_SearchResultListCtrl.AddString(prlMatch->Text)) != LB_ERR)
		{
			m_SearchResultListCtrl.SetItemData(ilLine,(DWORD) prlMatch);
		}
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSearchResults::OnOK() 
{
	int ilLine;
	if((ilLine = m_SearchResultListCtrl.GetCurSel()) == LB_ERR)
	{
		CString olCaption;
		olCaption.Format("%s \"%s\"",GetString(IDS_SEARCHRESULTSCAPTION),omSearchString);
		MessageBox(GetString(IDS_SEARCHRESULTSERR),olCaption,MB_ICONERROR);
	}
	else
	{
		prmMatch = (MATCHEDDATA *) m_SearchResultListCtrl.GetItemData(ilLine);
		CDialog::OnOK();
	}
}

void CSearchResults::OnDblclkSearchresultslist() 
{
	OnOK();
}

void CSearchResults::Add(MATCHEDDATA *prpMatch)
{
	omData.Add(prpMatch);
}
