// PrmJobWithoutDemDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <PrmJobWithoutDemDlg.h>
#include <CCSTime.h>
#include <BasicData.h>
#include <CedaTplData.h>
#include <AllocData.h>
#include <CedaSerData.h>
#include <CedaDpxData.h>
#include <CedaWroData.h>
#include <CedaEmpData.h>
#include <DataSet.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PrmJobWithoutDemDlg dialog


PrmJobWithoutDemDlg::PrmJobWithoutDemDlg(CWnd* pParent /*=NULL*/)
	: CDialog(PrmJobWithoutDemDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(PrmJobWithoutDemDlg)
	m_FromDate = NULL;
	m_FromTime = NULL;
	m_ToDate = NULL;
	m_ToTime = NULL;

	//}}AFX_DATA_INIT
	bmDisableAlocationUnit = false;
	bmDisableTemplate = false;
	lmUtpl = 0L;
}

void PrmJobWithoutDemDlg::InitDefaultValues(long lpStfUrno, long lpPooljobUrno, CTime opFrom, CTime opTo, CString opAloc /*= ""*/, long lpUtpl /*= 0L*/)
{
	omFrom = opFrom;
	omTo = opTo;
	omAloc = opAloc;
	lmUtpl = lpUtpl;
	lmStfUrno = lpStfUrno;
	lmPooljobUrno = lpPooljobUrno;
}

void PrmJobWithoutDemDlg::DisableAlocationUnit()
{
	bmDisableAlocationUnit = true;
}

void PrmJobWithoutDemDlg::DisableTemplate()
{
	bmDisableTemplate = true;
}

void PrmJobWithoutDemDlg::DoDataExchange(CDataExchange* pDX)
{
	if(pDX->m_bSaveAndValidate == FALSE)
	{
	}

	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PrmJobWithoutDemDlg)
	//}}AFX_DATA_MAP

	if(pDX->m_bSaveAndValidate == TRUE)
	{
	}
}


BEGIN_MESSAGE_MAP(PrmJobWithoutDemDlg, CDialog)
	//{{AFX_MSG_MAP(PrmJobWithoutDemDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PrmJobWithoutDemDlg message handlers

BOOL PrmJobWithoutDemDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CWnd *polWnd = NULL;
	m_ShowAllPdas = false;
	

	EMPDATA *prlStaff = ogEmpData.GetEmpByUrno(lmStfUrno);
	if (prlStaff != NULL)
	{
		char clTemp[256];
		sprintf(clTemp,"Assign PDA to: %s, %s",prlStaff->Finm,prlStaff->Lanm);
		SetWindowText(clTemp);
	}

	polWnd = GetDlgItem(IDC_PDAIDTEXT);
	if(polWnd != NULL)
	{
		//polWnd->SetWindowText(GetString(IDC_PDAIDTEXT));
		polWnd->SetWindowText("Show All PDA");
	}

	CComboBox *polPdaIds = (CComboBox *) GetDlgItem(IDC_PDAID);
	if(polPdaIds != NULL)
	{
		polPdaIds->ResetContent();

		int ilPdaCount = ogPdaData.omData.GetSize();
		PDADATA *blpPdaData;
		int ilIndex;

		for (int ilLc = 0; ilLc < ilPdaCount; ilLc++) 
		{
			blpPdaData = &ogPdaData.omData[ilLc];
			if (m_ShowAllPdas || blpPdaData->Ustf == 0L)
			{
				ilIndex = polPdaIds->AddString(blpPdaData->Pdid);
				polPdaIds->SetItemData(ilIndex,blpPdaData->Urno);
			}
		}
	}

	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void PrmJobWithoutDemDlg::OnOK() 
{
	CComboBox *polPdaIds = (CComboBox *) GetDlgItem(IDC_TEMPLATE);
	if(polPdaIds != NULL)
	{
		int ilIndex = polPdaIds->GetCurSel();
		if(ilIndex != CB_ERR)
		{
			lmUstf = (long) polPdaIds->GetItemData(ilIndex);
		}
	}
	CDialog::OnOK();
}
