// StaffSortDlg.h : header file
//
#ifndef _CSTAFFSORTDIALOG_H_
#define _CSTAFFSORTDIALOG_H_

/////////////////////////////////////////////////////////////////////////////
// CStaffSortDialog dialog

class CStaffSortDialog : public CDialog
{
// Construction
public:
    CStaffSortDialog(CWnd* pParent = NULL);    // standard constructor

// Dialog Data
    //{{AFX_DATA(CStaffSortDialog)
    enum { IDD = IDD_STAFF_SORT };
    int     m_SortCriteria2;
    int     m_SortCriteria0;
    int     m_SortCriteria1;
    //}}AFX_DATA

// Implementation
protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

    // Generated message map functions
    //{{AFX_MSG(CStaffSortDialog)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};
#endif // _CSTAFFSORTDIALOG_H_