// MReqTable.h : header file
//
#ifndef _MREQTABLE_H_
#define _MREQTABLE_H_

/////////////////////////////////////////////////////////////////////////////
// MReqTable dialog

#include <MReqTblV.H>

struct MReqDataStruct {
    long    Urno;           // Unique Record Number 
    char    Vald[10];           // Date for which this request is valid (date part only)
	char    Rfem[32];        // Request for Employee
	int     Wght;            // Weight
    char    Crat[10];         // Creation date for this request
	char    Crby[32];        // Created by Employee
	char    Cont[802];       // Text of Request
};
typedef struct MReqDataStruct MREQDATA;

class MReqTable : public CDialog
{
// Construction
public:
	MReqTable(CString opTableType,CWnd* pParent = NULL);   // standard constructor
	~MReqTable();
	void UpdateView();

	int m_nDialogBarHeight;
	RequestTableViewer omViewer;

private :
    CTable *pomTable; // visual object, the table content
	CString omTableType;
	int omContextItem;
	BOOL bmIsViewOpen;

private:
	void UpdateDisplay();
	void UpdateComboBox();

// Dialog Data
	//{{AFX_DATA(MReqTable)
	enum { IDD = IDD_MREQTABLE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(MReqTable)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(MReqTable)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	virtual void OnCancel();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg LONG OnTableLButtonDblClk(UINT wParam, LONG lParam);
	afx_msg LONG OnTableRButtonDown(UINT wParam, LONG lParam);
	afx_msg void OnInsert();
	afx_msg void OnWorkon();
	afx_msg void OnView();
	afx_msg void OnMenuWorkon();
	afx_msg void OnMenuDelete();
	afx_msg void OnCloseupViewcombo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
#endif // _MREQTABLE_H_