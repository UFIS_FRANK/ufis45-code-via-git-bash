// Class for Acres
#ifndef _CEDAACRDATA_H_
#define _CEDAACRDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct AcrDataStruct
{
	long	Urno;		// Unique Record Number
	char	Regn[13];	// Registration
	char    Apui[2];    // Auxiliary in Operation (aircraft starter required)
	CTime	Vafr;		// Valid from
	CTime	Vato;		// Valid to
	char	Act3[4];	// Aircraft type 3 letter code

	AcrDataStruct(void)
	{
		Urno = 0L;
		strcpy(Regn,"");
		strcpy(Apui,"");
		Vafr = TIMENULL;
		Vato = TIMENULL;
		strcpy(Act3,"");
	}
};

typedef struct AcrDataStruct ACRDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaAcrData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <ACRDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapStringToPtr omNameMap;
	CString GetTableName(void);

// Operations
public:
	CedaAcrData();
	~CedaAcrData();

	CCSReturnCode ReadAcrData();
	ACRDATA* GetAcrByUrno(long lpUrno);
	ACRDATA* GetAcrByName(const char *pcpName);
	long GetAcrUrnoByName(const char *pcpName);
	void GetAcrNames(CStringArray &ropAcrNames);

private:
	void AddAcrInternal(ACRDATA &rrpAcr);
	void ClearAll();
};


extern CedaAcrData ogAcrData;
#endif _CEDAACRDATA_H_
