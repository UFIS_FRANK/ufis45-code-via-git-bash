// SearchDlg.h : header search dialog
// Brian Chandler March 2002

#if !defined(AFX_SEARCHDLG_H__C4AB50C3_31C4_11D6_8138_00010215BFE5__INCLUDED_)
#define AFX_SEARCHDLG_H__C4AB50C3_31C4_11D6_8138_00010215BFE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SearchDlg.h : header file
//
#include <CCSPtrArray.h>
#include <CCSGlobl.h>

/////////////////////////////////////////////////////////////////////////////
// CSearchDlg dialog
#define SEARCH_FIELD_CHOICE_SEPERATOR 0x02	// used to seperate strings where there is a choice of what is being searched for
											// eg. entering an aircraft code checks both ALC2 and ALC3 so SearchStrings[x] contains
											// ALC2+SEARCH_FIELD_CHOICE_SEPERATOR+ALC3

struct SearchDataStruct
{
	CStringArray	SearchStrings;	// eg gate, position
	CString			DisplayString;	// string to display in IDC_RESULTS_LIST, if empty then SearchStrings are displayed
	long			UserData;		// user data eg. URNO of the record
	CTime			Date1;			// first date for which the data are valid
	CTime			Date2;			// second date for which the data are valid (Date1&2 can act as from to)
};
typedef struct SearchDataStruct SEARCHDATA;

struct SearchTypeStruct
{
	CString			What;			// eg. "GATE" or "POSITION"
	CStringArray	Fields;			// Field (titles) for the search fields
	CUIntArray		FieldLens;		// lengths (chars) of the search fields
	CStringArray	InitialValues;	// the fields will be initialized to these values
	CStringArray	WhereList;		// Contents of IDC_WHERE_COMBO: where to search eg "Gate Gantt", "Flight Detail Dialog"
	CCSPtrArray <SEARCHDATA> Data;	// Contents of IDC_RESULTS_LIST (all possible values)
};
typedef struct SearchTypeStruct SEARCHTYPE;


class CSearchDlg : public CDialog
{
// Construction
public:
	CSearchDlg(CWnd* pParent = NULL);   // standard constructor
	~CSearchDlg();

// Dialog Data
	//{{AFX_DATA(CSearchDlg)
	enum { IDD = IDD_SEARCHDIALOG };
	CButton	m_OkButtonCtrl;
	CButton	m_CancelButtonCtrl;
	CEdit	m_Edit7;
	CEdit	m_Edit6;
	CEdit	m_Edit5;
	CEdit	m_Edit4;
	CEdit	m_Edit3;
	CEdit	m_Edit2;
	CEdit	m_Edit1;
	CStatic	m_Title7;
	CStatic	m_Title6;
	CStatic	m_Title5;
	CStatic	m_Title4;
	CStatic	m_Title3;
	CStatic	m_Title2;
	CStatic	m_Title1;
	CComboBox	m_WhereCombo;
	CComboBox	m_WhenCombo;
	CComboBox	m_WhatCombo;
	CListBox	m_ResultsListCtrl;
	CString	m_WhenTitle;
	CString	m_WhatTitle;
	CString	m_WhereTitle;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSearchDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSearchDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnSelchangeWhatCombo();
	afx_msg void OnSelchangeWhereCombo();
	afx_msg void OnSelchangeWhenCombo();
	afx_msg void OnChangeEdit1();
	afx_msg void OnChangeEdit2();
	afx_msg void OnChangeEdit3();
	afx_msg void OnChangeEdit4();
	afx_msg void OnChangeEdit5();
	afx_msg void OnChangeEdit6();
	afx_msg void OnChangeEdit7();
	afx_msg void OnDblclkResultslist();
	afx_msg void OnSelchangeResultslist();
	afx_msg void OnSelcancelResultslist();
	afx_msg void OnMove(int x, int y); //PRF 8712
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	void UpdateResultsList(void);
	CCSPtrArray <SEARCHTYPE> omSearchTypes; // eg Gate, position etc

	void HandleWhatChange(SEARCHTYPE *prpSearchType);
	void DisplayWhatList();
	void DisplayWhereList(const CStringArray &ropWhereList);
	void DisplayWhenList();
	void DisplaySearchFields(CStringArray &ropFields, CUIntArray &ropFieldLens, CStringArray &ropInitialValues);

	int SelectWhat(const CString &ropWhat);
	int SelectWhere(const CString &ropWhere);
	int SelectWhen(CTime opWhen);

	SEARCHTYPE *CSearchDlg::GetSearchType(CString opWhat);
	SEARCHTYPE *CSearchDlg::AddSearchType(CString opWhat);

	CString omSelectedWhere;
	CString omSelectedWhat;
	CTime omSelectedWhen;
	long lmSelectedUserData;
	SEARCHTYPE *prmSelectedSearchType;
	void GetAllEditValues(CStringArray &ropEditValues);

	void SetOkButtonState(void);
	bool IsSameDay(CTime &ropDate1, CTime &ropDate2);
	CString omSearchFieldChoiceSeperator;
	void SaveOldValues(void);

	CStringArray olOldEditValues;
	CRect omWindowRect; //PRF 8712

public:
	// one or more fields to enter search text
	void AddSearchField(CString opWhat, CString opField, int ipFieldLen, CString opInitialValue = "");

	// where to search eg "Gate Gantt", "Flight Detail Dialog"
	void AddWhere(CString opWhat, CString opWhere);

	// the data to be searched
	void AddData(CString opWhat, CString opSearchString, CString opDisplayString = "", long lpUserData = 0L, CTime opDate1 = TIMENULL, CTime opDate2 = TIMENULL); 
	void AddData(CString opWhat, CStringArray &ropSearchStrings, CString opDisplayString = "", long lpUserData = 0L, CTime opDate1 = TIMENULL, CTime opDate2 = TIMENULL); 

	// some search fields have a choice eg. entering airline code searches both ALC2 and ALC3
	void AddSearchFieldChoice(CString &ropSearchString, CString opNewString);

	// return the selected data
	void GetSelectedData(CString &ropWhat, CString &ropWhere, CTime &ropWhen, long &rlpUserData , 
						 CStringArray &ropSearchStrings, CString &ropDisplayString, CTime &ropDate1, CTime &ropDate2);

};

// The following describes how the old data are stored:-
//
//	omWhatMap		polSingleMap
//	===============================================
//	"GLOBAL"	->	"WHAT"		-> "Flights"		// Currently selected WHAT value
//
//	"Flights"	->	"WHERE"		-> "Flight Detail Dialog"
//	"Flights"	->	"WHEN"		-> "20021004080000"
//	"Flights"	->	"Code"		-> "LH"
//	"Flights"	->	"Number"	-> "297"
//	"Flights"	->	"Suffix"	-> ""
//
//	"Gates"		->	"WHERE"		-> "Gate Gantt"
//	"Gates"		->	"WHEN"		-> "20021004080000"
//	"Gates"		->	"Gate"		-> "A04"
//
// The above old data are stored in VCDTAB for each PKNO as follows:-
//
//	CTYP			CKEY			TEXT
//	===============================================
//	"SearchDlg"		"GLOBAL"		"WHAT=Flights"
//	"SearchDlg"		"Flights"		"WHERE=Flight Detail Dialog|WHEN=20021004080000|Code=LH|Number=297|Suffix="
//	"SearchDlg"		"Gates"			"WHERE=Gate Gantt|WHEN=20021004080000|Gate=A04"

#define SEARCHDLG_KEY		"SearchDlg"
#define SEARCHDLG_SEPERATOR	'|'

class COldSearchValues
{
public:
	COldSearchValues();
	~COldSearchValues();

	void SetOldSearchValue(CString opWhat, CString opField, CString opOldValue);
	CString GetOldSearchValue(CString opWhat, CString opField);

	void SaveOldSearchValuesToDb(void);
	void LoadOldSearchValuesFromDb(void);

private:
	CMapStringToPtr omWhatMap;
	void DeleteOldSearchValues(void);
	void GetFieldAndValue(CString &ropFieldAndValue, CString &ropField, CString &ropValue);
};

extern COldSearchValues ogOldSearchValues;
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SEARCHDLG_H__C4AB50C3_31C4_11D6_8138_00010215BFE5__INCLUDED_)
