// Settings.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <Settings.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSettings

CSettings::CSettings()
{
}

CSettings::~CSettings()
{
	omSettings.RemoveAll();
}


void CSettings::AddSetting(CString opKey, CString opValue)
{
	omSettings.SetAt(opKey, opValue);
}

void CSettings::AddSetting(CString opKey, long lpValue)
{
	omValue.Format("%ld", lpValue);
	AddSetting(opKey, omValue);
}

void CSettings::AddSetting(CString opKey, int ipValue)
{
	omValue.Format("%d", ipValue);
	AddSetting(opKey, omValue);
}

void CSettings::AddSetting(CString opKey, bool bpValue)
{
	omValue.Format("%d", (bpValue) ? 1 : 0);
	AddSetting(opKey, omValue);
}

CString CSettings::GetSetting(CString opKey)
{
	omValue.Empty();
	omSettings.Lookup(opKey, omValue);
	return omValue;
}

long CSettings::GetLongSetting(CString opKey)
{
	return atol(GetSetting(opKey));
}

int CSettings::GetIntSetting(CString opKey)
{
	return atoi(GetSetting(opKey));
}

bool CSettings::GetBoolSetting(CString opKey)
{
	return (GetSetting(opKey) == CString("1")) ? true : false;
}
