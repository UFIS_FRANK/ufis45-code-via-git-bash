// MainFrm.cpp : implementation of the CMainFrame class
//

#include <stdafx.h>
#include <OpssPm.h>
#include <CCSGlobl.h>
#include <CCSCedaCom.h>
#include <CCSCedaData.h>
#include <DInfo.h>
#include <LoginDlg.h>
#include <ctl3d.h>
#include <cxbutton.h>
#include <CCSDragDropCtrl.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <CedaEmpData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaFlightData.h>
#include <CedaShiftData.h>
#include <tscale.h>
#include <StaffViewer.h>
#include <StaffGantt.h>
#include <StaffDiagram.h>
#include <StaffChart.h>
#include <GateDiagram.h>
#include <GateGantt.h>
#include <GateChart.h>
#include <RegnDiagram.h>
#include <RegnGantt.h>
#include <RegnChart.h>
#include <CciDiagram.h>
#include <CciGantt.h>
#include <CciChart.h>
#include <conflict.h>
#include <CciDeskDlg.h>
#include <CedaAloData.h>
#include <CedaJtyData.h>
#include <PrePlanTable.h>
#include <ccitable.h>
#include <gatetable.h>
#include <BasicData.h>
#include <AllocData.h>

#include <FlightPlan.h>
#include <StaffTable.h>
#include <StartDate.h>
#include <ConflictConfigTable.h>
#include <ufis.h>
#include <ConflictTable.h>
#include <CedaTplData.h>

#include <SearchDlg.h>
#include <DlgSettings.h>
#include <PrivList.h>
#include <ButtonList.h>
#include <MainFrm.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CCSCedaCom ogCommHandler;

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_SIZE() //PRF 8712
	ON_WM_MOVE() //PRF 8712
	ON_WM_DESTROY() //PRF 8712
	//}}AFX_MSG_MAP
	// Global help commands
	ON_COMMAND(ID_HELP_FINDER, CMDIFrameWnd::OnHelpFinder)
	ON_COMMAND(ID_HELP, CMDIFrameWnd::OnHelp)
	ON_COMMAND(ID_CONTEXT_HELP, CMDIFrameWnd::OnContextHelp)
	ON_COMMAND(ID_DEFAULT_HELP, CMDIFrameWnd::OnHelpFinder)
	ON_COMMAND(ID_CHART_CCI, OnChartCci)
	ON_COMMAND(ID_CHART_GATES, OnChartGate)
	ON_COMMAND(ID_CHART_REGN, OnChartRegn)
	ON_COMMAND(ID_CHART_STAFF, OnChartStaff)
	ON_COMMAND(ID_LIST_ATTENTION, OnListAttention)
	ON_COMMAND(ID_LIST_CCI, OnListCci)
	ON_COMMAND(ID_LIST_CONFLICT, OnListConflict)
	ON_COMMAND(ID_LIST_FLIGHT, OnListFlight)
	ON_COMMAND(ID_LIST_GATE, OnListGate)
	ON_COMMAND(ID_FLIGHTJOBS, OnFlightJobs)
	ON_COMMAND(ID_LIST_PREPLAN, OnListPreplan)
	ON_COMMAND(ID_LIST_STAFF, OnListStaff)
	ON_COMMAND(ID_LIST_DEMANDS, OnListDemands)
	ON_COMMAND(ID_CHART_POS, OnChartPos)
	ON_COMMAND(ID_CHART_EQU, OnChartEqu)
	ON_COMMAND(ID_LIST_CLOSEALLSCHEDULES, OnListCloseAllSchedules)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(ID_HELPMENUITEM, OnHelpMenuItem)
	ON_COMMAND(ID_FILE_PRINT,OnFilePrint)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	
}

CMainFrame::~CMainFrame()
{
	delete pogButtonList;
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	/*
	if (!m_wndToolBar.CreateEx(this) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}
	

	if (!m_wndDlgBar.Create(this, IDD_BUTTONLIST, 
		CBRS_ALIGN_TOP, AFX_IDW_DIALOGBAR))
	{
		TRACE0("Failed to create dialogbar\n");
		return -1;		// fail to create
	}
	*/

	//CButtonListDialog dlg;
	pogButtonList = new CButtonListDialog(this);


	if (!m_wndReBar.Create(this) ||
		!m_wndReBar.AddBar(pogButtonList))
	{
		TRACE0("Failed to create rebar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Remove this if you don't want tool tips
	m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY);

	TranslateMenu();

	m_bAutoMenuEnable = FALSE;
	CMenu *pMenu = GetMenu();
	ASSERT(pMenu != NULL);
	if(!ogBasicData.bmDisplayGates)
	{
		pMenu->EnableMenuItem(ID_CHART_GATES,MF_GRAYED);
		pMenu->EnableMenuItem(ID_LIST_GATE,MF_GRAYED);
	}
	if(!ogBasicData.bmDisplayFlightJobsTable)
	{
		pMenu->EnableMenuItem(ID_FLIGHTJOBS,MF_GRAYED);
	}
	if(!ogBasicData.bmDisplayCheckins)
	{
		pMenu->EnableMenuItem(ID_CHART_CCI,MF_GRAYED);
		pMenu->EnableMenuItem(ID_LIST_CCI,MF_GRAYED);
	}
	if(!ogBasicData.bmDisplayRegistrations)
	{
		pMenu->EnableMenuItem(ID_CHART_REGN,MF_GRAYED);
	}
	if(!ogBasicData.bmDisplayPrePlanning)
	{
		pMenu->EnableMenuItem(ID_LIST_PREPLAN,MF_GRAYED);
	}
	if(!ogBasicData.bmDisplayDemands)
	{
		pMenu->EnableMenuItem(ID_LIST_DEMANDS,MF_GRAYED);
	}
	if(!ogBasicData.bmDisplayPositions)
	{
		pMenu->EnableMenuItem(ID_CHART_POS,MF_GRAYED);
	}
	if(!ogBasicData.bmDisplayEquipment)
	{
		pMenu->EnableMenuItem(ID_CHART_EQU,MF_GRAYED);
	}

	SetWindowTitle();

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

void CMainFrame::OnAppAbout()
{
	DInfo olInfoDlg;
	olInfoDlg.pomInfoList = &ogBasicData.omInfo;
	CString olText;
	olText.Format("%s / %s",ogUsername,ogCommHandler.pcmReqId);
	olInfoDlg.m_User = olText;
	olText.Format("%s / %s / %s",ogCommHandler.pcmHostName,ogCommHandler.pcmHostType,pcgHomeAirport);
	olInfoDlg.m_Server = olText;
	olInfoDlg.DoModal();
}

void CMainFrame::OnChartCci()
{
	if (pogButtonList)
		pogButtonList->OnClickMenu(ID_CHART_CCI);
}

void CMainFrame::OnChartGate()
{
	if (pogButtonList)
		pogButtonList->OnClickMenu(ID_CHART_GATES);
}

void CMainFrame::OnChartStaff()
{
	if (pogButtonList)
		pogButtonList->OnClickMenu(ID_CHART_STAFF);
}

void CMainFrame::OnChartRegn()
{
	if (pogButtonList)
		pogButtonList->OnClickMenu(ID_CHART_REGN);
}

void CMainFrame::OnListGate()
{
	if (pogButtonList)
		pogButtonList->OnClickMenu(ID_LIST_GATE);
}

void CMainFrame::OnFlightJobs()
{
	if (pogButtonList)
		pogButtonList->OnClickMenu(ID_FLIGHTJOBS);
}

void CMainFrame::OnListCci()
{ 
	if (pogButtonList)
		pogButtonList->OnClickMenu(ID_LIST_CCI);
}

void CMainFrame::OnListStaff()
{
	if (pogButtonList)
		pogButtonList->OnClickMenu(ID_LIST_STAFF);
}

void CMainFrame::OnListPreplan()
{
	if (pogButtonList)
		pogButtonList->OnClickMenu(ID_LIST_PREPLAN);
}

void CMainFrame::OnListFlight()
{
	if (pogButtonList)
		pogButtonList->OnClickMenu(ID_LIST_FLIGHT);
}

void CMainFrame::OnListConflict()
{
	if (pogButtonList)
		pogButtonList->OnClickMenu(ID_LIST_CONFLICT);
}

void CMainFrame::OnListAttention()
{
	if (pogButtonList)
		pogButtonList->OnClickMenu(ID_LIST_ATTENTION);
}

void CMainFrame::OnListDemands()
{
	if (pogButtonList)
		pogButtonList->OnClickMenu(ID_LIST_DEMANDS);
}

void CMainFrame::OnChartPos()
{
	if (pogButtonList)
		pogButtonList->OnClickMenu(ID_CHART_POS);
}

void CMainFrame::OnChartEqu()
{
	if (pogButtonList)
		pogButtonList->OnClickMenu(ID_CHART_EQU);
}

void CMainFrame::OnListCloseAllSchedules()
{
	if (pogButtonList)
		pogButtonList->OnClickMenu(ID_LIST_CLOSEALLSCHEDULES);
}


void CMainFrame::OnHelpMenuItem()
{
	AfxGetApp()->WinHelp(0,HELP_CONTENTS);
}

bool CMainFrame::TranslateMenu ()
{
	HMENU			hMenu, hSubMenu;
	BOOL			ok=TRUE;
	CMenu *polMenu = GetMenu ();

	if ( !polMenu || !polMenu->m_hMenu )
		return false;

	hMenu = polMenu->m_hMenu;
	//  Submenu "File"
	hSubMenu = GetSubMenu ( hMenu, 0 );
	if ( hSubMenu )
	{
		ok &= ModifyMenu( hMenu, 0, MF_STRING|MF_BYPOSITION, (UINT)hSubMenu,
						 GetString(IDS_STRING33113) );
		ok &= ModifyMenu( hSubMenu, ID_APP_EXIT, MF_STRING|MF_BYCOMMAND, 
						  ID_APP_EXIT, GetString(IDS_STRING33114) );
	}
	else 
		ok = false;


	//  Submenu "Charts"
	hSubMenu = GetSubMenu ( hMenu, 1 );
	if ( hSubMenu )
	{
		ok &= ModifyMenu( hMenu, 1, MF_STRING|MF_BYPOSITION, (UINT)hSubMenu,GetString(IDS_STRING33115) );

		ok &= SetMenu(hSubMenu, "BUTTONLIST IDC_STAFFDIAGRAM", GetString(IDS_STRING33116), ID_CHART_STAFF);
		ok &= SetMenu(hSubMenu, "BUTTONLIST IDC_GATEDIAGRAM", GetString(IDS_STRING33117), ID_CHART_GATES,ogBasicData.bmDisplayGates);
		ok &= SetMenu(hSubMenu, "BUTTONLIST IDC_CCIDIAGRAM", GetString(IDS_STRING33118), ID_CHART_CCI,ogBasicData.bmDisplayCheckins);
		ok &= SetMenu(hSubMenu, "BUTTONLIST IDC_REGNDIAGRAM", GetString(IDS_STRING33119), ID_CHART_REGN,ogBasicData.bmDisplayRegistrations);
		ok &= SetMenu(hSubMenu, "BUTTONLIST IDC_POSITIONS", GetString(IDS_STRING33120), ID_CHART_POS,ogBasicData.bmDisplayPositions);
		ok &= SetMenu(hSubMenu, "BUTTONLIST IDC_EQUIPMENTTABLE", GetString(IDS_EQUCHARTMENU), ID_CHART_EQU,ogBasicData.bmDisplayEquipment);
	}
	else 
		ok = false;

	//  Submenu "Lists"
	hSubMenu = GetSubMenu ( hMenu, 2 );
	if ( hSubMenu )
	{
		ok &= ModifyMenu( hMenu, 2, MF_STRING|MF_BYPOSITION, (UINT)hSubMenu, GetString(IDS_STRING33121) );

		ok &= SetMenu(hSubMenu, "BUTTONLIST IDC_STAFFTABLE", GetString(IDS_STRING33116), ID_LIST_STAFF);
		ok &= SetMenu(hSubMenu, "BUTTONLIST IDC_GATEBELEGUNG", GetString(IDS_STRING33117), ID_LIST_GATE,ogBasicData.bmDisplayGates);
		ok &= SetMenu(hSubMenu, "BUTTONLIST IDC_CCIBELEGUNG", GetString(IDS_STRING33118), ID_LIST_CCI,ogBasicData.bmDisplayCheckins);
		ok &= SetMenu(hSubMenu, "BUTTONLIST IDC_FLIGHTPLAN", GetString(IDS_STRING33122), ID_LIST_FLIGHT);
		ok &= SetMenu(hSubMenu, "BUTTONLIST IDC_FLIGHTJOBSTABLE", GetString(IDS_FLIGHTJOBSTABLE), ID_FLIGHTJOBS);
		ok &= SetMenu(hSubMenu, "BUTTONLIST IDC_PREPLANING", GetString(IDS_STRING33123), ID_LIST_PREPLAN,ogBasicData.bmDisplayPrePlanning);
		ok &= SetMenu(hSubMenu, "BUTTONLIST IDC_ATTENTION", GetString(IDS_STRING33124), ID_LIST_ATTENTION);
		ok &= SetMenu(hSubMenu, "BUTTONLIST IDC_CONFLICT", GetString(IDS_STRING33125), ID_LIST_CONFLICT);
		ok &= SetMenu(hSubMenu, "BUTTONLIST IDC_UNRESOLVED_DEMANDS", GetString(IDS_STRING61907), ID_LIST_DEMANDS, ogBasicData.bmDisplayDemands);
		ok &= SetMenu(hSubMenu, "BUTTONLIST IDC_FLIGHTPLAN", GetString(IDS_STRING33126), ID_LIST_CLOSEALLSCHEDULES);
	}
	else 
		ok = false;

	//  Submenu "View"
	hSubMenu = GetSubMenu ( hMenu, 3 );
	if ( hSubMenu )
	{
		ok &= ModifyMenu( hMenu, 3, MF_STRING|MF_BYPOSITION, (UINT)hSubMenu, GetString(IDS_STRING33127) );
		ok &= ModifyMenu( hSubMenu, ID_VIEW_TOOLBAR, MF_STRING|MF_BYCOMMAND, ID_VIEW_TOOLBAR, GetString(IDS_STRING33128) );
		ok &= ModifyMenu( hSubMenu, ID_VIEW_STATUS_BAR, MF_STRING|MF_BYCOMMAND, ID_VIEW_STATUS_BAR, GetString(IDS_STRING33129) );
	}
	else 
		ok = false;

	//  Submenu "Help"
	hSubMenu = GetSubMenu ( hMenu, 4 );
	if ( hSubMenu )
	{
		ok &= ModifyMenu( hMenu, 4, MF_STRING|MF_BYPOSITION, (UINT)hSubMenu, "&?");
		ok &= ModifyMenu( hSubMenu, ID_HELPMENUITEM, MF_STRING|MF_BYCOMMAND, ID_HELPMENUITEM, GetString(IDS_STRING33130));
		ok &= ModifyMenu( hSubMenu, ID_APP_ABOUT, MF_STRING|MF_BYCOMMAND, ID_APP_ABOUT, GetString(IDS_STRING33131));
	}
	else 
		ok = false;


	return (ok!=FALSE);
}

BOOL CMainFrame::SetMenu(HMENU opMenu, const char *pcpKey, const char *pcpText, UINT ipMenuId, bool bpEnabledInCedaIni /*=true*/)
{
	BOOL blRc = TRUE;
	if(opMenu)
	{
		char clStat = ogPrivList.GetStat(pcpKey);
		if(clStat == '-')
		{
			blRc = RemoveMenu(opMenu, ipMenuId, MF_BYCOMMAND);
		}
		else
		{
			UINT ilFlag = (clStat == '1' && bpEnabledInCedaIni) ? MF_STRING|MF_BYCOMMAND : MF_STRING|MF_BYCOMMAND|MF_GRAYED;
			blRc = ModifyMenu(opMenu, ipMenuId, ilFlag, ipMenuId, pcpText);
		}
	}

	return blRc;
}

void CMainFrame::OnClose()
{
	if (pogButtonList)
	{
		if (pogButtonList->ExitCommitted())
		{
			ogOldSearchValues.SaveOldSearchValuesToDb();
			ogDlgSettings.SaveSettingsToDb();
			ogBasicData.DumpRereadStatistics();
			CMDIFrameWnd::OnClose();
		} 
	}
}

void CMainFrame::SetWindowTitle()
{
	CString olAppTitle;
	olAppTitle.Format("OPSS-PM  %s  ", ogBasicData.GetTimeframeString());

	void *pvlDummy;
	CMapStringToPtr olTemplatesSoFar; // template names are duplicated (for Flight dependendent and indie)
	CDWordArray olTplUrnos;
	ogBasicData.GetTemplateFilters(olTplUrnos);
	int ilNumTpls = olTplUrnos.GetSize();
	for(int ilTpl = 0; ilTpl < ilNumTpls; ilTpl++)
	{
		TPLDATA *prlTpl = ogTplData.GetTplByUrno(olTplUrnos[ilTpl]);
		if(prlTpl != NULL && !olTemplatesSoFar.Lookup(prlTpl->Tnam,(void *&) pvlDummy))
		{
			if(ilTpl != 0)
			{
				olAppTitle += CString("/");
			}
			olAppTitle += prlTpl->Tnam;
			olTemplatesSoFar.SetAt(prlTpl->Tnam,NULL);
		}
	}

	SetWindowText(olAppTitle);
}

void CMainFrame::OnFilePrint()
{
	if(pogPreviewPrintWnd != NULL)
	{
		pogPreviewPrintWnd->SendMessage(WM_PREVIEW_PRINT,NULL,NULL);
	}
}

//PRF 8712
void CMainFrame::OnSize(UINT nType, int cx, int cy)
{
	CMDIFrameWnd::OnSize(nType,cx,cy);
	
	if(nType != SIZE_MINIMIZED)
	{
		GetWindowRect(&omWindowRect);
	}

	if(nType == SIZE_MAXIMIZED || nType == SIZE_RESTORED)
	{
		if(pogButtonList->m_wndStaffDiagram != NULL)
		{
			if(pogButtonList->m_wndStaffDiagram->IsIconic() == TRUE)
			{
				pogButtonList->m_wndStaffDiagram->ShowWindow(SW_MINIMIZE);
			}
			else if(pogButtonList->m_wndStaffDiagram->IsZoomed() == TRUE)
			{
				pogButtonList->m_wndStaffDiagram->ShowWindow(SW_MAXIMIZE);
			}
		}
		
		if(pogButtonList->m_wndGateDiagram != NULL)
		{
			if(pogButtonList->m_wndGateDiagram->IsIconic() == TRUE)
			{
				pogButtonList->m_wndGateDiagram->ShowWindow(SW_MINIMIZE);
			}
			else if(pogButtonList->m_wndGateDiagram->IsZoomed() == TRUE)
			{
				pogButtonList->m_wndGateDiagram->ShowWindow(SW_MAXIMIZE);
			}
		}
		
		if(pogButtonList->m_wndCciDiagram != NULL)
		{
			if(pogButtonList->m_wndCciDiagram->IsIconic() == TRUE)
			{
				pogButtonList->m_wndCciDiagram->ShowWindow(SW_MINIMIZE);
			}
			else if(pogButtonList->m_wndCciDiagram->IsZoomed() == TRUE)
			{
				pogButtonList->m_wndCciDiagram->ShowWindow(SW_MAXIMIZE);
			}
		}
		
		if(pogButtonList->m_wndRegnDiagram != NULL)
		{
			if(pogButtonList->m_wndRegnDiagram->IsIconic() == TRUE)
			{
				pogButtonList->m_wndRegnDiagram->ShowWindow(SW_MINIMIZE);
			}
			else if(pogButtonList->m_wndRegnDiagram->IsZoomed() == TRUE)
			{
				pogButtonList->m_wndRegnDiagram->ShowWindow(SW_MAXIMIZE);
			}
		}
		
		if(pogButtonList->m_wndEquipmentDiagram != NULL)
		{
			if(pogButtonList->m_wndEquipmentDiagram->IsIconic() == TRUE)
			{
				pogButtonList->m_wndEquipmentDiagram->ShowWindow(SW_MINIMIZE);
			}
			else if(pogButtonList->m_wndEquipmentDiagram->IsZoomed() == TRUE)
			{
				pogButtonList->m_wndEquipmentDiagram->ShowWindow(SW_MAXIMIZE);
			}
		}
		
		if(pogButtonList->m_wndPstDiagram != NULL)
		{
			if(pogButtonList->m_wndPstDiagram->IsIconic() == TRUE)
			{
				pogButtonList->m_wndPstDiagram->ShowWindow(SW_MINIMIZE);
			}
			else if(pogButtonList->m_wndPstDiagram->IsZoomed() == TRUE)
			{
				pogButtonList->m_wndPstDiagram->ShowWindow(SW_MAXIMIZE);
			}
		}
		
		if(pogButtonList->m_wndStaffTable != NULL)
		{
			if(pogButtonList->m_wndStaffTable->IsIconic() == TRUE)
			{
				pogButtonList->m_wndStaffTable->ShowWindow(SW_MINIMIZE);
			}
			else if(pogButtonList->m_wndStaffTable->IsZoomed() == TRUE)
			{
				pogButtonList->m_wndStaffTable->ShowWindow(SW_MAXIMIZE);
			}
		}
		
		if(pogButtonList->m_wndGateTable != NULL)
		{
			if(pogButtonList->m_wndGateTable->IsIconic() == TRUE)
			{
				pogButtonList->m_wndGateTable->ShowWindow(SW_MINIMIZE);
			}
			else if(pogButtonList->m_wndGateTable->IsZoomed() == TRUE)
			{
				pogButtonList->m_wndGateTable->ShowWindow(SW_MAXIMIZE);
			}
		}
		
		if(pogButtonList->m_wndCCITable != NULL)
		{
			if(pogButtonList->m_wndCCITable->IsIconic() == TRUE)
			{
				pogButtonList->m_wndCCITable->ShowWindow(SW_MINIMIZE);
			}
			else if(pogButtonList->m_wndCCITable->IsZoomed() == TRUE)
			{
				pogButtonList->m_wndCCITable->ShowWindow(SW_MAXIMIZE);
			}
		}
		
		if(pogButtonList->omFlightPlanArray.GetSize() > 0)
		{
			FlightPlan* polFlightPlan = NULL;
			for(int i= 0; i < pogButtonList->omFlightPlanArray.GetSize(); i++)
			{
				polFlightPlan = (FlightPlan*)pogButtonList->omFlightPlanArray.GetAt(i);				
				if(polFlightPlan != NULL && polFlightPlan->IsIconic() == TRUE)
				{
					polFlightPlan->ShowWindow(SW_MINIMIZE);
				}
				else if(polFlightPlan->IsZoomed() == TRUE)
				{
					polFlightPlan->ShowWindow(SW_MAXIMIZE);
				}
				polFlightPlan = NULL;
			}
		}
		
		if(pogButtonList->m_wndDemandTable != NULL)
		{
			if(pogButtonList->m_wndDemandTable->IsIconic() == TRUE)
			{
				pogButtonList->m_wndDemandTable->ShowWindow(SW_MINIMIZE);
			}
			else if(pogButtonList->m_wndDemandTable->IsZoomed() == TRUE)
			{
				pogButtonList->m_wndDemandTable->ShowWindow(SW_MAXIMIZE);
			}
		}
		
		if(pogButtonList->m_wndFlIndDemandTable != NULL)
		{
			if(pogButtonList->m_wndFlIndDemandTable->IsIconic() == TRUE)
			{
				pogButtonList->m_wndFlIndDemandTable->ShowWindow(SW_MINIMIZE);
			}
			else if(pogButtonList->m_wndFlIndDemandTable->IsZoomed() == TRUE)
			{
				pogButtonList->m_wndFlIndDemandTable->ShowWindow(SW_MAXIMIZE);
			}
		}
		
		if(pogButtonList->m_wndFlightJobsTable != NULL)
		{
			if(pogButtonList->m_wndFlightJobsTable->IsIconic() == TRUE)
			{
				pogButtonList->m_wndFlightJobsTable->ShowWindow(SW_MINIMIZE);
			}
			else if(pogButtonList->m_wndFlightJobsTable->IsZoomed() == TRUE)
			{
				pogButtonList->m_wndFlightJobsTable->ShowWindow(SW_MAXIMIZE);
			}
		}

		if(pogButtonList->m_wndPrePlanTable != NULL)
		{
			if(pogButtonList->m_wndPrePlanTable->IsIconic() == TRUE)
			{
				pogButtonList->m_wndPrePlanTable->ShowWindow(SW_MINIMIZE);
			}
			else if(pogButtonList->m_wndPrePlanTable->IsZoomed() == TRUE)
			{
				pogButtonList->m_wndPrePlanTable->ShowWindow(SW_MAXIMIZE);
			}
		}
		
		if(pogButtonList->m_wndConflictTable != NULL)
		{
			if(pogButtonList->m_wndConflictTable->IsIconic() == TRUE)
			{
				pogButtonList->m_wndConflictTable->ShowWindow(SW_MINIMIZE);
			}
			else if(pogButtonList->m_wndConflictTable->IsZoomed() == TRUE)
			{
				pogButtonList->m_wndConflictTable->ShowWindow(SW_MAXIMIZE);
			}
		}
		
		if(pogButtonList->m_wndAttentionTable != NULL)
		{
			if(pogButtonList->m_wndAttentionTable->IsIconic() == TRUE)
			{
				pogButtonList->m_wndAttentionTable->ShowWindow(SW_MINIMIZE);
			}
			else if(pogButtonList->m_wndAttentionTable->IsZoomed() == TRUE)
			{
				pogButtonList->m_wndAttentionTable->ShowWindow(SW_MAXIMIZE);
			}
		}
	}
}

//PRF 8712
void CMainFrame::OnMove(int x, int y)
{
	CMDIFrameWnd::OnMove(x,y);
	if(this->IsIconic() == FALSE)
	{
		GetWindowRect(&omWindowRect);
	}
}

//PRF 8712
void CMainFrame::OnDestroy()
{
	ogBasicData.WriteDialogToReg(this,COpssPmApp::OPSSPM_DIALOG_WINDOWPOS_REG_KEY,omWindowRect,this->IsIconic());
	CMDIFrameWnd::OnDestroy();
}