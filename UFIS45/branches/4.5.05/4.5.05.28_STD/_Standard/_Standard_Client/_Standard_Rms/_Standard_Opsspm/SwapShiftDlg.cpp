// SwapShiftDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BasicData.h>
#include <opsspm.h>
#include <SwapShiftDlg.h>
#include <CedaRnkData.h>
#include <CedaWgpData.h>
#include <PrivList.h>
#include <SelectShiftDlg.h>
#include <CedaDrgData.h>
#include <DataSet.h>
#include <CedaJobData.h>
#include <CedaDrdData.h>
#include <CedaDrsData.h>
#include <CedaDraData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSwapShiftDlg dialog


CSwapShiftDlg::CSwapShiftDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSwapShiftDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSwapShiftDlg)
	m_FunctionTitle = _T("");
	m_NameTitle = _T("");
	m_NewShiftTitle = _T("");
	m_OldShiftTitle = _T("");
	m_PknoTitle = _T("");
	m_Sub1Sub2Title = _T("");
	m_WorkGroupTitle = _T("");
	m_Name1 = _T("");
	m_Name2 = _T("");
	m_NewShift1 = _T("");
	m_NewShift2 = _T("");
	m_OldShift1 = _T("");
	m_OldShift2 = _T("");
	m_Pkno1 = _T("");
	m_Pkno2 = _T("");
	m_DeleteJobs = FALSE;
	//}}AFX_DATA_INIT

	prmOriginalShift1 = NULL;
	prmOriginalShift2 = NULL;
	bmInitialized = false;
}


void CSwapShiftDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSwapShiftDlg)
	DDX_Control(pDX, IDOK, m_OkCtrl);
	DDX_Control(pDX, IDC_PKNO2, m_Pkno2Ctrl);
	DDX_Control(pDX, IDC_PKNO1, m_Pkno1Ctrl);
	DDX_Control(pDX, IDC_WORKGROUP2, m_WorkGroup2Ctrl);
	DDX_Control(pDX, IDC_WORKGROUP1, m_WorkGroup1Ctrl);
	DDX_Control(pDX, IDC_SUB1SUB22, m_Sub1Sub22Ctrl);
	DDX_Control(pDX, IDC_SUB1SUB21, m_Sub1Sub21Ctrl);
	DDX_Control(pDX, IDC_FUNCTION2, m_Function2Ctrl);
	DDX_Control(pDX, IDC_FUNCTION1, m_Function1Ctrl);
	DDX_Text(pDX, IDC_FUNCTIONTITLE, m_FunctionTitle);
	DDX_Text(pDX, IDC_NAMETITLE, m_NameTitle);
	DDX_Text(pDX, IDC_NEWSHIFTTITLE, m_NewShiftTitle);
	DDX_Text(pDX, IDC_OLDSHIFTTITLE, m_OldShiftTitle);
	DDX_Text(pDX, IDC_PKNOTITLE, m_PknoTitle);
	DDX_Text(pDX, IDC_SUB1SUB2TITLE, m_Sub1Sub2Title);
	DDX_Text(pDX, IDC_WORKGROUPTITLE, m_WorkGroupTitle);
	DDX_Text(pDX, IDC_NAME1, m_Name1);
	DDX_Text(pDX, IDC_NAME2, m_Name2);
	DDX_Text(pDX, IDC_NEWSHIFT1, m_NewShift1);
	DDX_Text(pDX, IDC_NEWSHIFT2, m_NewShift2);
	DDX_Text(pDX, IDC_OLDSHIFT1, m_OldShift1);
	DDX_Text(pDX, IDC_OLDSHIFT2, m_OldShift2);
	DDX_Text(pDX, IDC_PKNO1, m_Pkno1);
	DDX_Text(pDX, IDC_PKNO2, m_Pkno2);
	DDX_Check(pDX, IDC_DELETEJOBS, m_DeleteJobs);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSwapShiftDlg, CDialog)
	//{{AFX_MSG_MAP(CSwapShiftDlg)
	ON_BN_CLICKED(IDC_SELEMP1, OnSelemp1)
	ON_BN_CLICKED(IDC_SELEMP2, OnSelemp2)
	ON_EN_CHANGE(IDC_PKNO1, OnChangePkno1)
	ON_EN_CHANGE(IDC_PKNO2, OnChangePkno2)
	ON_CBN_SELCHANGE(IDC_FUNCTION1, OnSelChangeFunction1)
	ON_CBN_SELCHANGE(IDC_FUNCTION2, OnSelChangeFunction2)
	ON_CBN_SELCHANGE(IDC_WORKGROUP1, OnSelChangeWorkgroup1)
	ON_CBN_SELCHANGE(IDC_WORKGROUP2, OnSelChangeWorkgroup2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSwapShiftDlg message handlers

void CSwapShiftDlg::SetSday(CString opSday)
{
	omSday = opSday;
}

void CSwapShiftDlg::SetShifts(SHIFTDATA *prpShift1, SHIFTDATA *prpShift2)
{
	omFunctionMap.RemoveAll();
	omWorkgroupMap.RemoveAll();
	omFunction1.Empty();
	omFunction2.Empty();
	omWorkgroup1.Empty();
	omWorkgroup2.Empty();

	if(prpShift1 != NULL)
	{
		DRGDATA *prlDrg = NULL;
		prmOriginalShift1 = prpShift1;
		prmOriginalShift2 = prpShift2;
		omShift1 = *prpShift1;

		if(strlen(prpShift1->Fctc) > 0)
		{
			omFunctionMap.SetAt(prpShift1->Fctc, (void *) NULL);
			omFunction1 = prpShift1->Fctc;
		}
		if(strlen(prpShift1->Egrp) > 0)
		{
			omWorkgroupMap.SetAt(prpShift1->Egrp, (void *) NULL);
			omWorkgroup1 = prpShift1->Egrp;
		}
		if((prlDrg = ogDrgData.GetDrgByShift(prpShift1)) != NULL)
		{
			if(strlen(prlDrg->Fctc) > 0)
			{
				omFunctionMap.SetAt(prlDrg->Fctc, (void *) NULL);
				omFunction1 = prlDrg->Fctc;
			}
			if(strlen(prlDrg->Wgpc) > 0)
			{
				omWorkgroupMap.SetAt(prlDrg->Wgpc, (void *) NULL);
				omWorkgroup1 = prlDrg->Wgpc;
			}
		}
		
		if(prpShift2 != NULL)
		{
			omShift2 = *prpShift2;

			strcpy(omShift1.Sfco,prpShift1->Sfca);
			strcpy(omShift1.Sfca,prpShift2->Sfca);
//			strcpy(omShift1.Fctc,prpShift2->Fctc);
//			strcpy(omShift1.Egrp,prpShift2->Egrp);

			strcpy(omShift2.Sfco,prpShift2->Sfca);
			strcpy(omShift2.Sfca,prpShift1->Sfca);
//			strcpy(omShift2.Fctc,prpShift1->Fctc);
//			strcpy(omShift2.Egrp,prpShift1->Egrp);

			if(strlen(prpShift2->Fctc) > 0)
			{
				omFunctionMap.SetAt(prpShift2->Fctc, (void *) NULL);
				omFunction2 = prpShift2->Fctc;
			}
			if(strlen(prpShift2->Egrp) > 0)
			{
				omWorkgroupMap.SetAt(prpShift2->Egrp, (void *) NULL);
				omWorkgroup2 = prpShift2->Egrp;
			}
			if((prlDrg = ogDrgData.GetDrgByShift(prpShift2)) != NULL)
			{
				if(strlen(prlDrg->Fctc) > 0)
				{
					omFunctionMap.SetAt(prlDrg->Fctc, (void *) NULL);
					omFunction2 = prlDrg->Fctc;
				}
				if(strlen(prlDrg->Wgpc) > 0)
				{
					omWorkgroupMap.SetAt(prlDrg->Wgpc, (void *) NULL);
					omWorkgroup2 = prlDrg->Wgpc;
				}
			}
		}
		else
		{
			omFunction2 = omFunction1;
			omWorkgroup2 = omWorkgroup1;
		}

		InitializeShift1Fields();
		InitializeShift2Fields();
	}
}

void CSwapShiftDlg::InitializeShift1Fields()
{
	if(prmOriginalShift1 != NULL && bmInitialized)
	{
		m_Pkno1 = omShift1.Peno;
		m_Name1 = ogEmpData.GetEmpName(omShift1.Stfu);
		m_OldShift1 = omShift1.Sfco;
		m_NewShift1 = omShift1.Sfca;

		m_Function1Ctrl.ResetContent();
		CString olFunction; void *pvlDummy;
		for(POSITION rlPos = omFunctionMap.GetStartPosition(); rlPos != NULL; )
		{
			omFunctionMap.GetNextAssoc(rlPos, olFunction, (void *&)pvlDummy);
			m_Function1Ctrl.AddString(olFunction);
		}
		SetComboBoxSelection(m_Function1Ctrl, omFunction2);
//		for(int ilF = 0; ilF < omFunctions.GetSize(); ilF++)
//			m_Function1Ctrl.AddString(omFunctions[ilF]);
//		m_Function1Ctrl.AddString(prmOriginalShift1->Fctc);
//		if(prlDrg != NULL && strcmp(prlDrg->Fctc,prmOriginalShift1->Fctc))
//			m_Function1Ctrl.AddString(prlDrg->Fctc);
//
//		if(prmOriginalShift2 != NULL && strcmp(prmOriginalShift2->Fctc,prmOriginalShift1->Fctc))
//			m_Function1Ctrl.AddString(prmOriginalShift2->Fctc);
//		if(prmOriginalShift2 != NULL)
//			SetComboBoxSelection(m_Function1Ctrl, prmOriginalShift2->Fctc);
//		else
//			SetComboBoxSelection(m_Function1Ctrl, omShift1.Fctc);

		m_WorkGroup1Ctrl.ResetContent();
		CString olWorkgroup;;
		for(rlPos = omWorkgroupMap.GetStartPosition(); rlPos != NULL; )
		{
			omWorkgroupMap.GetNextAssoc(rlPos, olWorkgroup, (void *&)pvlDummy);
			m_WorkGroup1Ctrl.AddString(olWorkgroup);
		}
		SetComboBoxSelection(m_WorkGroup1Ctrl, omWorkgroup2);
//		for(int ilW = 0; ilW < omTeams.GetSize(); ilW++)
//			m_WorkGroup1Ctrl.AddString(omTeams[ilW]);
//		m_WorkGroup1Ctrl.AddString(prmOriginalShift1->Egrp);
//		if(prlDrg != NULL && strcmp(prlDrg->Wgpc,prmOriginalShift1->Egrp))
//			m_Function1Ctrl.AddString(prlDrg->Wgpc);
//
//		if(prmOriginalShift2 != NULL && strcmp(prmOriginalShift2->Egrp,prmOriginalShift1->Egrp))
//			m_WorkGroup1Ctrl.AddString(prmOriginalShift2->Egrp);
//		if(prmOriginalShift2 != NULL)
//			SetComboBoxSelection(m_WorkGroup1Ctrl, prmOriginalShift2->Egrp);
//		else
//			SetComboBoxSelection(m_WorkGroup1Ctrl, omShift1.Egrp);

		InitSub1Sub2ComboBox(m_Sub1Sub21Ctrl, &omShift1);

		UpdateData(FALSE);
		SetSub1Sub2FieldsIfThereAreChanges();
	}
}

void CSwapShiftDlg::InitializeShift2Fields()
{
	if(prmOriginalShift2 != NULL && bmInitialized)
	{
		DRGDATA *prlDrg = ogDrgData.GetDrgByShift(prmOriginalShift2);

		m_Pkno2 = omShift2.Peno;
		m_Name2 = ogEmpData.GetEmpName(omShift2.Stfu);
		m_OldShift2 = omShift2.Sfco;
		m_NewShift2 = omShift2.Sfca;

		m_Function2Ctrl.ResetContent();
		CString olFunction; void *pvlDummy;
		for(POSITION rlPos = omFunctionMap.GetStartPosition(); rlPos != NULL; )
		{
			omFunctionMap.GetNextAssoc(rlPos, olFunction, (void *&)pvlDummy);
			m_Function2Ctrl.AddString(olFunction);
		}
		SetComboBoxSelection(m_Function2Ctrl, omFunction1);
//		for(int ilF = 0; ilF < omFunctions.GetSize(); ilF++)
//			m_Function2Ctrl.AddString(omFunctions[ilF]);
//		m_Function2Ctrl.AddString(prmOriginalShift2->Fctc);
//		if(prlDrg != NULL && strcmp(prlDrg->Fctc,prmOriginalShift2->Fctc))
//			m_Function2Ctrl.AddString(prlDrg->Fctc);
//
//		if(prmOriginalShift1 != NULL && strcmp(prmOriginalShift2->Fctc,prmOriginalShift1->Fctc))
//			m_Function2Ctrl.AddString(prmOriginalShift1->Fctc);
//		if(prmOriginalShift1 != NULL)
//			SetComboBoxSelection(m_Function2Ctrl, prmOriginalShift1->Fctc);
//		else
//			SetComboBoxSelection(m_Function2Ctrl, omShift2.Fctc);

		m_WorkGroup2Ctrl.ResetContent();
		CString olWorkgroup;;
		for(rlPos = omWorkgroupMap.GetStartPosition(); rlPos != NULL; )
		{
			omWorkgroupMap.GetNextAssoc(rlPos, olWorkgroup, (void *&)pvlDummy);
			m_WorkGroup2Ctrl.AddString(olWorkgroup);
		}
		SetComboBoxSelection(m_WorkGroup2Ctrl, omWorkgroup1);
//		for(int ilW = 0; ilW < omTeams.GetSize(); ilW++)
//			m_WorkGroup2Ctrl.AddString(omTeams[ilW]);
//		m_WorkGroup2Ctrl.AddString(prmOriginalShift1->Egrp);
//		if(prlDrg != NULL && strcmp(prlDrg->Wgpc,prmOriginalShift2->Egrp))
//			m_Function2Ctrl.AddString(prlDrg->Wgpc);
//
//		if(prmOriginalShift2 != NULL && strcmp(prmOriginalShift2->Egrp,prmOriginalShift1->Egrp))
//			m_WorkGroup2Ctrl.AddString(prmOriginalShift2->Egrp);
//		if(prmOriginalShift1 != NULL)
//			SetComboBoxSelection(m_WorkGroup2Ctrl, prmOriginalShift1->Egrp);
//		else
//			SetComboBoxSelection(m_WorkGroup2Ctrl, omShift2.Egrp);

		InitSub1Sub2ComboBox(m_Sub1Sub22Ctrl, &omShift2);

		UpdateData(FALSE);
		SetSub1Sub2FieldsIfThereAreChanges();
	}
}

void CSwapShiftDlg::InitSub1Sub2ComboBox(CComboBox &ropSub1Sub2Ctrl, SHIFTDATA *prpShift)
{
	ogBasicData.SetWindowStat("GENERAL SUB1SUB2", GetDlgItem(IDC_SUB1SUB2));
	CWnd *polWnd = GetDlgItem(IDC_SUB1SUB2TITLE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_SUB1SUB2));
		ogBasicData.SetWindowStat("GENERAL SUB1SUB2", polWnd);
	}
	ogBasicData.SetWindowStat("GENERAL SUB1SUB2", (CWnd *) &m_Sub1Sub21Ctrl);
	ogBasicData.SetWindowStat("GENERAL SUB1SUB2", (CWnd *) &m_Sub1Sub22Ctrl);
	
	if(ogPrivList.GetStat("GENERAL SUB1SUB2") != '-')
	{
		// initializing the combo
		CDC *pdc = ropSub1Sub2Ctrl.GetDC();
		CFont olFont;
		CFont *pOldFont	= pdc->SelectObject(&olFont);
		CSize olSizeSub1;
		CSize olSizeSub2;
		long ilWidth;

		CString olInsertSub1 = GetString(IDS_EMPDLG_SUB1);
		CString olInsertSub2 = GetString(IDS_EMPDLG_SUB2);

		ropSub1Sub2Ctrl.ResetContent();
		ropSub1Sub2Ctrl.InsertString(-1, olInsertSub1);
		ropSub1Sub2Ctrl.InsertString(-1, olInsertSub2);

		olSizeSub1 = pdc->GetTextExtent(olInsertSub1);
		olSizeSub2 = pdc->GetTextExtent(olInsertSub2);
		ilWidth = max(olSizeSub1.cx, olSizeSub2.cx) - 40;
		ropSub1Sub2Ctrl.SetDroppedWidth((UINT)ilWidth);
		pdc->SelectObject(&olFont);

		//setting the initial selection
		if(!strcmp(prpShift->Drs2, "2"))
			ropSub1Sub2Ctrl.SetCurSel(1);
		else
			ropSub1Sub2Ctrl.SetCurSel(0);
	}
}


BOOL CSwapShiftDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	SetWindowText(GetString(IDS_SWAPSHIFTSDLG_TITLE));
	m_PknoTitle = GetString(IDS_SWAPSHIFTSDLG_PKNOTITLE);
	m_NameTitle = GetString(IDS_SWAPSHIFTSDLG_NAMETITLE);
	m_OldShiftTitle = GetString(IDS_SWAPSHIFTSDLG_OLDSHIFTTITLE);
	m_NewShiftTitle = GetString(IDS_SWAPSHIFTSDLG_NEWSHIFTTITLE);
	m_Sub1Sub2Title = GetString(IDS_SWAPSHIFTSDLG_SUB1SUB2TITLE);
	m_WorkGroupTitle = GetString(IDS_SWAPSHIFTSDLG_WORKGROUPTITLE);
	m_FunctionTitle = GetString(IDS_SWAPSHIFTSDLG_FUNCTIONTITLE);

//	ogRnkData.GetAllRanks(omFunctions);
//	omFunctions.Add(""); // empty function code

//	ogWgpData.GetAllTeams(omTeams);

	bmInitialized = true;
	
	InitializeShift1Fields();
	InitializeShift2Fields();

	UpdateData(FALSE);

	return TRUE;
}

void CSwapShiftDlg::OnSelemp1() 
{
	CSelectShiftDlg olSelectShiftDlg;
	olSelectShiftDlg.SetSday(omSday);
	olSelectShiftDlg.pomShifts = &omShifts;
	if(olSelectShiftDlg.DoModal() == IDOK && olSelectShiftDlg.prmSelectedShift != NULL)
		SetShifts(olSelectShiftDlg.prmSelectedShift, prmOriginalShift2);
}

void CSwapShiftDlg::OnSelemp2() 
{
	CSelectShiftDlg olSelectShiftDlg;
	olSelectShiftDlg.SetSday(omSday);
	olSelectShiftDlg.pomShifts = &omShifts;
	if(olSelectShiftDlg.DoModal() == IDOK && olSelectShiftDlg.prmSelectedShift != NULL)
		SetShifts(prmOriginalShift1, olSelectShiftDlg.prmSelectedShift);
}

void CSwapShiftDlg::OnChangePkno1() 
{
	CString olPkno;
	m_Pkno1Ctrl.GetWindowText(olPkno);
	EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(olPkno);
	if(prlEmp != NULL && (prmOriginalShift1 == NULL || prlEmp->Urno != prmOriginalShift1->Stfu))
	{
		SHIFTDATA *prlShift = ogShiftData.GetShiftByUstfAndSdayAndRosl(prlEmp->Urno, omSday, MITARBEITER_TAGESLISTE, "1");
		if(prlShift != NULL)
		{
			SetShifts(prlShift, prmOriginalShift2);
		}
	}
}

void CSwapShiftDlg::OnChangePkno2() 
{
	CString olPkno;
	m_Pkno2Ctrl.GetWindowText(olPkno);
	EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(olPkno);
	if(prlEmp != NULL && (prmOriginalShift2 == NULL || prlEmp->Urno != prmOriginalShift2->Stfu))
	{
		SHIFTDATA *prlShift = ogShiftData.GetShiftByUstfAndSdayAndRosl(prlEmp->Urno, omSday, MITARBEITER_TAGESLISTE, "1");
		if(prlShift != NULL)
		{
			SetShifts(prmOriginalShift1, prlShift);
		}
	}
}

void CSwapShiftDlg::OnOK() 
{
	if(UpdateData(TRUE))
	{
		if(prmOriginalShift1 != NULL && prmOriginalShift2 != NULL && prmOriginalShift1->Stfu != prmOriginalShift2->Stfu)
		{
			CString olFunction1 = GetComboBoxSelection(m_Function1Ctrl);
			if(olFunction1.IsEmpty())
			{
				MessageBox(GetString(IDS_ERREMPDLGFUNC), GetString(IDS_SWAPSHIFTSDLG_TITLE), MB_ICONERROR);
				m_Function1Ctrl.SetFocus();
				return;
			}

			CString olFunction2 = GetComboBoxSelection(m_Function2Ctrl);
			if(olFunction2.IsEmpty())
			{
				MessageBox(GetString(IDS_ERREMPDLGFUNC), GetString(IDS_SWAPSHIFTSDLG_TITLE), MB_ICONERROR);
				m_Function2Ctrl.SetFocus();
				return;
			}

			// cannot swap shifts with deviation, temp absence or shadow shift
			if(!CheckForShadowShift(prmOriginalShift1, prmOriginalShift2))
				return;
			if(!CheckForDeviationOrAbsence(prmOriginalShift1, prmOriginalShift2))
				return;

			if(m_DeleteJobs)
			{
				CCSPtrArray <JOBDATA> olJobs;
				JOBDATA *prlJob = NULL;

				ogJobData.GetJobsByShur(olJobs, prmOriginalShift1->Urno, FALSE, true);
				for(int ilJ = (olJobs.GetSize()-1); ilJ >= 0; ilJ--)
				{
					prlJob = &olJobs[ilJ];
					if (!strcmp(prlJob->Jtco,JOBPOOL) || !strcmp(prlJob->Jtco,JOBFMGATEAREA) || !strcmp(prlJob->Jtco,JOBFMCCIAREA))
						olJobs.RemoveAt(ilJ);
				}
				if(olJobs.GetSize() > 0)
				{
					ogDataSet.DeleteJob(this, olJobs);
				}

				ogJobData.GetJobsByShur(olJobs, prmOriginalShift2->Urno, FALSE, true);
				for(ilJ = (olJobs.GetSize()-1); ilJ >= 0; ilJ--)
				{
					prlJob = &olJobs[ilJ];
					if (!strcmp(prlJob->Jtco,JOBPOOL) || !strcmp(prlJob->Jtco,JOBFMGATEAREA) || !strcmp(prlJob->Jtco,JOBFMCCIAREA))
						olJobs.RemoveAt(ilJ);
				}
				if(olJobs.GetSize() > 0)
				{
					ogDataSet.DeleteJob(this, olJobs);
				}
			}

			CString olSub1, olWorkGroup1 = GetComboBoxSelection(m_WorkGroup1Ctrl);
			CString olSub1Sub21 = GetComboBoxSelection(m_Sub1Sub21Ctrl);
			if(olSub1Sub21 == GetString(IDS_EMPDLG_SUB1))
				olSub1 = "1";
			else if(olSub1Sub21 == GetString(IDS_EMPDLG_SUB2))
				olSub1 = "2";
			else
				olSub1 = "";

			CString olSub2, olWorkGroup2 = GetComboBoxSelection(m_WorkGroup2Ctrl);
			CString olSub1Sub22 = GetComboBoxSelection(m_Sub1Sub21Ctrl);
			if(olSub1Sub22 == GetString(IDS_EMPDLG_SUB1))
				olSub2 = "1";
			else if(olSub1Sub22 == GetString(IDS_EMPDLG_SUB2))
				olSub2 = "2";
			else
				olSub2 = "";

			bool blRc = true;

			long llShift1Urno = prmOriginalShift1->Urno, llShift2Urno = prmOriginalShift2->Urno;

			if((blRc = ogShiftData.SwapShifts(prmOriginalShift1, olFunction1, olWorkGroup1, olSub1, prmOriginalShift2, olFunction2, olWorkGroup2, olSub2)) != true)
			{
				// "Error on update staff data"
				MessageBox(GetString(IDS_STRING61497) + ogShiftData.LastError());
			}

//			SHIFTDATA *prlUpdatedShift1 = ogShiftData.GetShiftByUrno(llShift1Urno);
//			SHIFTDATA *prlUpdatedShift2 = ogShiftData.GetShiftByUrno(llShift2Urno);
//
//			if(blRc)
//			{
//				ogJobData.StartTransaction();
//				CCSPtrArray<JOBDATA> olJobs;
//				if(prlUpdatedShift1 != NULL)
//				{
//					ogJobData.GetJobsByShur(olJobs, prlUpdatedShift1->Urno, FALSE, true);
//					for(int ilJ = 0; ilJ < olJobs.GetSize(); ilJ++)
//					{
//						JOBDATA *prlJob = &olJobs[ilJ];
//						if(prlJob->Ustf != prlUpdatedShift1->Stfu)
//						{
//							JOBDATA rlOldJob = *prlJob;
//							prlJob->Ustf = prlUpdatedShift1->Stfu;
//							strcpy(prlJob->Peno,ogEmpData.GetPenoByUrno(prlUpdatedShift1->Stfu));
//							if((blRc = ogJobData.ChangeJobData(prlJob, &rlOldJob, "SwapShift")) != true)
//							{
//								MessageBox("Error updating JOBTAB " + ogShiftData.LastError());
//								break;
//							}
//						}
//					}
//				}
//				if(prlUpdatedShift2 != NULL)
//				{
//					ogJobData.GetJobsByShur(olJobs, prlUpdatedShift2->Urno, FALSE, true);
//					for(int ilJ = 0; ilJ < olJobs.GetSize(); ilJ++)
//					{
//						JOBDATA *prlJob = &olJobs[ilJ];
//						if(prlJob->Ustf != prlUpdatedShift2->Stfu)
//						{
//							JOBDATA rlOldJob = *prlJob;
//							prlJob->Ustf = prlUpdatedShift2->Stfu;
//							strcpy(prlJob->Peno,ogEmpData.GetPenoByUrno(prlUpdatedShift2->Stfu));
//							if((blRc = ogJobData.ChangeJobData(prlJob, &rlOldJob, "SwapShift")) != true)
//							{
//								MessageBox("Error updating JOBTAB " + ogShiftData.LastError());
//								break;
//							}
//						}
//					}
//				}
//				ogJobData.CommitTransaction();
//			}
		}

		CDialog::OnOK();
	}
}

// cannot swap shifts with deviation, temp absence or shadow shift
bool CSwapShiftDlg::CheckForShadowShift(SHIFTDATA *prpShift1, SHIFTDATA *prpShift2)
{
	CString olErr, olErr2;
	bool blOk = true;
	if(ogDrsData.GetDrsByDrru(prpShift1->Urno) != NULL)
	{
		olErr2.Format("%s: %s\n", ogEmpData.GetEmpName(prpShift1->Stfu), GetString(IDS_SWAPSHIFT_SHADOW));
		olErr += olErr2;
	}
	if(ogDrsData.GetDrsByDrru(prpShift2->Urno) != NULL)
	{
		olErr2.Format("%s: %s\n", ogEmpData.GetEmpName(prpShift2->Stfu), GetString(IDS_SWAPSHIFT_SHADOW));
		olErr += olErr2;
	}
	if(!olErr.IsEmpty())
	{
		MessageBox(olErr, GetString(IDS_SWAPSHIFTSDLG_TITLE), MB_ICONSTOP);
		blOk = false;
	}

	return blOk;
}

// cannot swap shifts with deviation, temp absence or shadow shift
bool CSwapShiftDlg::CheckForDeviationOrAbsence(SHIFTDATA *prpShift1, SHIFTDATA *prpShift2)
{
	bool blOk = true;

	CString olErr, olErr2;
	int i;
	CCSPtrArray <JOBDATA> olJobs;
	CCSPtrArray <DRDDATA> olDrds;
	DRDDATA *prlDrd = NULL;
	bool blErr = false;

	ogJobData.GetJobsByShur(olJobs, prpShift1->Urno, TRUE);
	blErr = false;
	for(i = 0; i < olJobs.GetSize(); i++)
	{
		if((prlDrd = ogDrdData.GetDrdByUrno(olJobs[i].Udrd)) != NULL)
		{
			olDrds.Add(prlDrd);
			blErr = true;
		}
	}
	if(blErr)
	{
		olErr2.Format("%s: %s\n", ogEmpData.GetEmpName(prpShift1->Stfu), GetString(IDS_SWAPSHIFT_DEVIATION));
		olErr += olErr2;
	}

	ogJobData.GetJobsByShur(olJobs, prpShift2->Urno, TRUE);
	blErr = false;
	for(i = 0; i < olJobs.GetSize(); i++)
	{
		if((prlDrd = ogDrdData.GetDrdByUrno(olJobs[i].Udrd)) != NULL)
		{
			olDrds.Add(prlDrd);
			blErr = true;
		}
	}
	if(blErr)
	{
		olErr2.Format("%s: %s\n", ogEmpData.GetEmpName(prpShift2->Stfu), GetString(IDS_SWAPSHIFT_DEVIATION));
		olErr += olErr2;
	}

	CCSPtrArray <JOBDATA> olTempAbsences;
	CCSPtrArray <JOBDATA> olAllTempAbsences;

	ogJobData.GetJobsByPkno(olTempAbsences, prpShift1->Peno, JOBTEMPABSENCE);
	blErr = false;
	for(i = 0; i < olTempAbsences.GetSize(); i++)
	{
		if(IsOverlapped(olTempAbsences[i].Acfr, olTempAbsences[i].Acto, prpShift1->Avfa, prpShift1->Avta))
		{
			olAllTempAbsences.Add(&olTempAbsences[i]);
			blErr = true;
		}
	}
	if(blErr)
	{
		olErr2.Format("%s: %s\n", ogEmpData.GetEmpName(prpShift1->Stfu), GetString(IDS_SWAPSHIFT_TEMPABS));
		olErr += olErr2;
	}

	ogJobData.GetJobsByPkno(olTempAbsences, prpShift2->Peno, JOBTEMPABSENCE);
	blErr = false;
	for(i = 0; i < olTempAbsences.GetSize(); i++)
	{
		if(IsOverlapped(olTempAbsences[i].Acfr, olTempAbsences[i].Acto, prpShift2->Avfa, prpShift2->Avta))
		{
			olAllTempAbsences.Add(&olTempAbsences[i]);
			blErr = true;
		}
	}
	if(blErr)
	{
		olErr2.Format("%s: %s\n", ogEmpData.GetEmpName(prpShift2->Stfu), GetString(IDS_SWAPSHIFT_TEMPABS));
		olErr += olErr2;
	}

	if(!olErr.IsEmpty())
	{
		if(MessageBox(olErr, GetString(IDS_SWAPSHIFTSDLG_TITLE), MB_ICONEXCLAMATION|MB_OKCANCEL) == IDOK)
		{
			for(i = 0; i < olDrds.GetSize(); i++)
			{
				ogDrdData.DeleteDrdRecord(olDrds[i].Urno);
			}
			for(i = 0; i < olAllTempAbsences.GetSize(); i++)
			{
				JOBDATA *prlJob = &olAllTempAbsences[i];
				if(ogDraData.DeleteDraRecord(prlJob->Shur))
					ogJobData.DeleteJob(prlJob->Urno);
			}
		}
		else
		{
			blOk = false;
		}
	}

	return blOk;
}


CString CSwapShiftDlg::GetComboBoxSelection(CComboBox &ropComboBox)
{
	CString olSelection;
	int ilIdx = ropComboBox.GetCurSel();
	if(ilIdx != CB_ERR)
	{
		ropComboBox.GetLBText(ilIdx, olSelection);
	}

	return olSelection;
}

void CSwapShiftDlg::SetComboBoxSelection(CComboBox &ropComboBox, CString olSelection)
{
	ropComboBox.SelectString(-1, olSelection);
}

void CSwapShiftDlg::SetSub1Sub2FieldsIfThereAreChanges()
{
	BOOL blActivateOkButton = FALSE;
	if(UpdateData(TRUE))
	{
		if(prmOriginalShift1 != NULL)
		{
			if(strcmp(m_NewShift1,prmOriginalShift1->Sfca) || 
				strcmp(GetComboBoxSelection(m_Function1Ctrl),prmOriginalShift1->Fctc) || 
				strcmp(GetComboBoxSelection(m_WorkGroup1Ctrl),prmOriginalShift1->Egrp))
			{
				m_Sub1Sub21Ctrl.SetCurSel(1);
				blActivateOkButton = TRUE;
			}
			else
			{
				//setting the initial selection
				if(!strcmp(prmOriginalShift1->Drs2, "2"))
					m_Sub1Sub21Ctrl.SetCurSel(1);
				else
					m_Sub1Sub21Ctrl.SetCurSel(0);
			}
		}
		if(prmOriginalShift2 != NULL)
		{
			if(strcmp(m_NewShift2,prmOriginalShift2->Sfca) || 
				strcmp(GetComboBoxSelection(m_Function2Ctrl),prmOriginalShift2->Fctc) || 
				strcmp(GetComboBoxSelection(m_WorkGroup2Ctrl),prmOriginalShift2->Egrp))
			{
				m_Sub1Sub22Ctrl.SetCurSel(1);
				blActivateOkButton = TRUE;
			}
			else
			{
				//setting the initial selection
				if(!strcmp(prmOriginalShift2->Drs2, "2"))
					m_Sub1Sub22Ctrl.SetCurSel(1);
				else
					m_Sub1Sub22Ctrl.SetCurSel(0);
			}
		}
		if(prmOriginalShift1 == NULL || prmOriginalShift2 == NULL)
			blActivateOkButton = FALSE;
	}
	m_OkCtrl.EnableWindow(blActivateOkButton);
}

void CSwapShiftDlg::OnSelChangeFunction1() 
{
	SetSub1Sub2FieldsIfThereAreChanges();
}

void CSwapShiftDlg::OnSelChangeFunction2() 
{
	SetSub1Sub2FieldsIfThereAreChanges();
}

void CSwapShiftDlg::OnSelChangeWorkgroup1() 
{
	SetSub1Sub2FieldsIfThereAreChanges();
}

void CSwapShiftDlg::OnSelChangeWorkgroup2() 
{
	SetSub1Sub2FieldsIfThereAreChanges();
}
