#include <stdafx.h>
#include <CCSGlobl.h>
#include <AutoAssignAzViewer.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

AutoAssignAzViewer::AutoAssignAzViewer()
{
	int i;

	SetViewerKey("StaffDia");
	omStaffViewName = GetViewName();
	if(omStaffViewName.IsEmpty() == TRUE)
	{
		omStaffViewName = ogCfgData.rmUserSetup.STCV;
	}
    SelectView(omStaffViewName);

    CStringArray olFilterValues;
	GetFilter("Pool", olFilterValues);
	omPoolMap.RemoveAll();
	int ilNumPools = olFilterValues.GetSize();
	bmUseAllPools = false;
	for(i = 0; i < ilNumPools; i++)
	{
		omPoolMap[olFilterValues[i]] = NULL;
	}


	SetViewerKey("RegnDia");
//	omRegnAreaViewName = GetViewName();
//	if(omRegnAreaViewName.IsEmpty() == TRUE)
	{
		omRegnAreaViewName = ogCfgData.rmUserSetup.RECV;
	}
    SelectView(omRegnAreaViewName);
	omRegnAreaMap.RemoveAll();
	GetFilter("RegnArea", olFilterValues);
	int ilNumRegnAreas = olFilterValues.GetSize();
	for(i = 0; i < ilNumRegnAreas; i++)
	{
		omRegnAreaMap[olFilterValues[i]] = NULL;
	}

}

AutoAssignAzViewer::~AutoAssignAzViewer()
{
}

