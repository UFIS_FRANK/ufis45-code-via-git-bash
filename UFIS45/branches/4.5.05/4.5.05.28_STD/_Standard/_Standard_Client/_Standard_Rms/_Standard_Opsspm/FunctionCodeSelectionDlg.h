#if !defined(AFX_FUNCTIONCODESELECTIONDLG_H__8D438333_BB99_11D4_8020_00010215BFE5__INCLUDED_)
#define AFX_FUNCTIONCODESELECTIONDLG_H__8D438333_BB99_11D4_8020_00010215BFE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FunctionCodeSelectionDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFunctionCodeSelectionDlg dialog

class CFunctionCodeSelectionDlg : public CDialog
{
// Construction
public:
	CFunctionCodeSelectionDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CFunctionCodeSelectionDlg)
	enum { IDD = IDD_SEL_FUNCTION_DLG };
	CListBox	m_FunctionList;
	//}}AFX_DATA

	CString omSelectedFunctionCode;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFunctionCodeSelectionDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFunctionCodeSelectionDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnDblclkFunctionlist();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FUNCTIONCODESELECTIONDLG_H__8D438333_BB99_11D4_8020_00010215BFE5__INCLUDED_)
