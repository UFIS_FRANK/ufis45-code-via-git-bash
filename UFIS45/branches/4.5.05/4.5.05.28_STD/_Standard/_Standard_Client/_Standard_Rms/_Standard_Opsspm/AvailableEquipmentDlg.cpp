// AvailableEquipmentDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <AvailableEquipmentDlg.h>
#include <BasicData.h>
#include <CedaValData.h>
#include <CedaEqaData.h>

#define IsTotallyInside(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))

const CString CTYP_AVAILABLE_EQUIPMENTDLG = "AVAILABLE_EQUIPMENT_DLG";
const CString CKEY_FILTER_SETTINGS        = "FILTER-SETTINGS";
const CString TEXT_SHOW_ONLY_DEFINED_EQU  = "SHOW_DEFINED_EQU";
const CString TEXT_VIEW_NAME              = "VIEW_NAME";
const char*   TEXT_SEPARATOR              = "#";
const char*   DATA_SEPARATOR              = "=";

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAvailableEquipmentDlg dialog
static int CompareDemandStartTime(const DEMANDDATA **pppDemand1, const DEMANDDATA **pppDemand2);

static int CompareDemandStartTime(const DEMANDDATA **pppDemand1, const DEMANDDATA **pppDemand2)
{
	int ilRc = (int)((**pppDemand1).Debe.GetTime() - (**pppDemand2).Debe.GetTime());
	if (ilRc == 0)
		ilRc = (int)((**pppDemand1).Urno - (**pppDemand2).Urno);
	return ilRc;
}

static void AvailableEquipmentDlgCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);

static void AvailableEquipmentDlgCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
    CAvailableEquipmentDlg *polDlg = (CAvailableEquipmentDlg *)popInstance;

	if(ipDDXType == DEMANDGANTT_SELECT)
	{
		polDlg->ProcessSelectDemand((DEMANDDATA *)vpDataPointer);
	}
}


static int CompareEqAttributeName(const EQADATA **pppEqa1, const EQADATA **pppEqa2);

static int CompareEqAttributeName(const EQADATA **pppEqa1, const EQADATA **pppEqa2)
{
	return (int)(strcmp((*pppEqa1)->Name,(*pppEqa2)->Name));
}

// See ogBasicData.GetAssignmentSuitabiltyWeighting() for a description of the weight
//static int CompareWeight(const AVAILABLE_EQU_LINEDATA **pppLine1, const AVAILABLE_EQU_LINEDATA **pppLine2);
//static int CompareWeight(const AVAILABLE_EQU_LINEDATA **pppLine1, const AVAILABLE_EQU_LINEDATA **pppLine2)
//{
//	return (int) (*pppLine1)->Weight - (*pppLine2)->Weight;
//}

void CAvailableEquipmentDlg::ProcessSelectDemand(DEMANDDATA *prpDemand)
{
	if(prpDemand != NULL && ogDemandData.GetDemandByUrno(prpDemand->Urno) != NULL)
	{
		AfxGetApp()->DoWaitCursor(1);
		if(SelectDemand(prpDemand))
		{
			UpdateView();
		}
		AfxGetApp()->DoWaitCursor(-1);
	}
}


CAvailableEquipmentDlg::CAvailableEquipmentDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAvailableEquipmentDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAvailableEquipmentDlg)
	m_DisplayOnlyEquWithCorrectFunctionsAndPermits = FALSE;
	//}}AFX_DATA_INIT

	pomEquViewer = new AvailableEquipmentViewer(&omEquipment);
	pomEquTable = new CTable;
	pomEquTable->SetSelectMode(0);

	// display the same equipment that are displayed on the StaffGantt
	pomEquViewer->SetViewerKey("EquipmentDiagram");

	ogCCSDdx.Register(this, DEMANDGANTT_SELECT,CString("AVAILABLE_EQU_DLG"), CString("Select Demand"), AvailableEquipmentDlgCf);
	prmSelectedDemand = NULL;
	pomCfgData = NULL;
}

CAvailableEquipmentDlg::~CAvailableEquipmentDlg()
{
	delete pomEquTable;
	delete pomEquViewer;
	omEquipment.DeleteAll();
	ogCCSDdx.UnRegister(this, NOTUSED);
}


void CAvailableEquipmentDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAvailableEquipmentDlg)
	DDX_Control(pDX, IDC_TEAM_ALLOC, m_TeamAllocCtrl);
	DDX_Control(pDX, IDC_FILTERFUNCPERMITS, m_DisplayOnlyEquWithCorrectFunctionsAndPermitsCtrl);
	DDX_Control(pDX, IDC_STATUS, m_StatusCtrl);
	DDX_Check(pDX, IDC_FILTERFUNCPERMITS, m_DisplayOnlyEquWithCorrectFunctionsAndPermits);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAvailableEquipmentDlg, CDialog)
	//{{AFX_MSG_MAP(CAvailableEquipmentDlg)
    ON_CBN_SELCHANGE(IDC_VIEW, UpdateView)
	ON_WM_CREATE()
	ON_BN_CLICKED(IDC_FILTERFUNCPERMITS, OnFilterfuncpermits)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAvailableEquipmentDlg message handlers

BOOL CAvailableEquipmentDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);	

//	if(ogBasicData.bmShowMatchingEmpsByDefault)
//	{
		//m_DisplayOnlyEquWithCorrectFunctionsAndPermits = TRUE;
        pomCfgData = ogCfgData.GetCfgByCtypAndCkey(CTYP_AVAILABLE_EQUIPMENTDLG, CKEY_FILTER_SETTINGS);
		if(pomCfgData == NULL)
		{
			m_DisplayOnlyEquWithCorrectFunctionsAndPermits = TRUE;
		}
		else
		{			
			char *token;
			char chData[100];
			CString strCfgTextData = pomCfgData->Text;
			token = strtok(strCfgTextData.GetBuffer(0),TEXT_SEPARATOR);
			while(token != NULL)
			{				
				char* chFind = NULL;
				if((chFind = strstr(token,TEXT_SHOW_ONLY_DEFINED_EQU)) != NULL)
				{
					if((chFind = strstr(token,DATA_SEPARATOR)) != NULL)
					{
						strcpy(chData,chFind + 1);
						if(atoi(chData) == 1)
						{
							m_DisplayOnlyEquWithCorrectFunctionsAndPermits = TRUE;
						}
						else
						{
							m_DisplayOnlyEquWithCorrectFunctionsAndPermits = FALSE;
						}
					}
				}
				else if((chFind = strstr(token,TEXT_VIEW_NAME)) != NULL)
				{
					if((chFind = strstr(token,DATA_SEPARATOR)) != NULL)
					{
						strcpy(chData,chFind + 1);
						omLastSavedViewName = chData;
					}
				}
				token = strtok(NULL, TEXT_SEPARATOR);
			}			
		}
		UpdateData(FALSE);
//	}

	// select the earliest demand by default
	if(omDemands.GetSize() > 0)
	{
		omDemands.Sort(CompareDemandStartTime);
		SelectDemand(&omDemands[0]);
	}

	m_DisplayOnlyEquWithCorrectFunctionsAndPermitsCtrl.SetWindowText(GetString(IDS_AVEQ_MATCHINGONLY));
	m_TeamAllocCtrl.SetWindowText(GetString(IDS_AVEMPS_TEAMALLOC));

	CWnd *polWnd = GetDlgItem(IDC_IGNOREBREAKS); 
	if(polWnd != NULL)
	{
		polWnd->ShowWindow(SW_HIDE);
	}
	if((polWnd = GetDlgItem(IDC_IGNOREOVERLAP)) != NULL)
	{
		polWnd->ShowWindow(SW_HIDE);
	}
	if((polWnd = GetDlgItem(IDC_IGNOREOUTSIDESHIFT)) != NULL)
	{
		polWnd->ShowWindow(SW_HIDE);
	}

	// set the demands Gantt
	InitDemandsGantt();

	// set list of equipment
	InitEquipmentTable();

	// set the list of views
	InitSelectViewComboBox();
	
	UpdateView(); // update the table of available equipment

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CAvailableEquipmentDlg::InitSelectViewComboBox(void)
{
	UpdateComboBox();
	ogBasicData.SetWindowStat("AVAILABLE_EQU_DLG IDC_VIEW",GetDlgItem(IDC_VIEW));
}

void CAvailableEquipmentDlg::InitEquipmentTable(void)
{
    CRect olDlgRect, olStatusBarRect, olNewRect;
    GetClientRect(&olDlgRect);
    olDlgRect.InflateRect(1, 1);
	olNewRect = olDlgRect;
	m_StatusCtrl.GetWindowRect(&olStatusBarRect);
	ScreenToClient(&olStatusBarRect);
	olNewRect.top = olStatusBarRect.bottom + 4;
	olNewRect.bottom += olStatusBarRect.bottom + 4;
    pomEquTable->SetTableData(this, olNewRect.left, olNewRect.right, olNewRect.top, olNewRect.bottom);
	pomEquViewer->Attach(pomEquTable);
}

void CAvailableEquipmentDlg::InitDemandsGantt(void)
{
	omDemandsViewer.prmSelectedDemand = prmSelectedDemand;

	CTime olStartTime = TIMENULL, olEndTime = TIMENULL;
	int ilCount = omDemands.GetSize(), ilLc;
	for(ilLc = 0; ilLc < ilCount; ilLc++)
	{
		if(olStartTime == TIMENULL || olStartTime > omDemands[ilLc].Debe)
		{
			olStartTime = omDemands[ilLc].Debe;
		}
		if(olEndTime == TIMENULL || olEndTime < omDemands[ilLc].Deen)
		{
			olEndTime = omDemands[ilLc].Deen;
		}
	}

	omTSStartTime = olStartTime - CTimeSpan(0, 1, 0, 0);
	omTSDuration = (olEndTime + CTimeSpan(0, 1, 0, 0)) - omTSStartTime;
    omTSInterval = CTimeSpan(0, 0, 10, 0);

	omDemandsViewer.RemoveAll();
	omDemandsViewer.SetDemandsPtr(&omDemands);
	omDemandsViewer.SetJobsPtr(&omJobs);
	omDemandsViewer.SetJodsPtr(&omJods);
	omDemandsViewer.SetNoDemandJobsPtr(&omNoDemandJobs);
	omDemandsViewer.CanSelectDemands(prmSelectedDemand);
	omDemandsViewer.Init();

	if (::IsWindow(omDemandsGantt.GetSafeHwnd()))
	{
	    for (int ilLineno = 0; ilLineno < omDemandsViewer.omFlightLines.GetSize(); ilLineno++)
		{
			omDemandsGantt.AddString("");
			omDemandsGantt.SetItemHeight(ilLineno, omDemandsGantt.GetLineHeight(ilLineno));
		}
		omDemandsGantt.InvalidateRect(NULL);
	}

    CRect olDlgRect, olStatusBarRect, olNewRect;
    GetClientRect(&olDlgRect);
    olDlgRect.InflateRect(1, 1);
	olNewRect = olDlgRect;
	m_StatusCtrl.GetWindowRect(&olStatusBarRect);
	ScreenToClient(&olStatusBarRect);
	olNewRect.bottom = olStatusBarRect.top - 4;
	olNewRect.top += 70;

    omTimeScale.EnableDisplayCurrentTime(FALSE);
    omTimeScale.Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE,
						CRect(olNewRect.left, olNewRect.top, olNewRect.right, olNewRect.top + 34),
						this, 0, NULL);
    omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);

    omDemandsGantt.SetTimeScale(&omTimeScale);
    omDemandsGantt.SetViewer(&omDemandsViewer, 0 /* GroupNo */);
    omDemandsGantt.SetStatusBar(&m_StatusCtrl);
    omDemandsGantt.SetDisplayWindow(omTSStartTime, omTSStartTime + omTimeScale.GetDisplayDuration());
    omDemandsGantt.SetVerticalScaleWidth(-2);
    omDemandsGantt.SetFonts(&ogMSSansSerif_Regular_8, &ogSmallFonts_Regular_6);
	omDemandsGantt.SetGanttChartColors(NAVY, GRAY, YELLOW, GRAY);
    omDemandsGantt.Create(0, CRect(olNewRect.left, olNewRect.top + 34, olNewRect.right, olNewRect.bottom), this);
	omDemandsGantt.SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);

	ASSIGNCONFLICT_BARDATA *prlBkBar = omDemandsViewer.GetBkBarByUrno(prmSelectedDemand->Urno);
	if(prlBkBar != NULL)
	{
		omDemandsGantt.SetStatusText(prlBkBar->StatusText);
	}
	omDemandsViewer.Attach(this);
}

LONG CAvailableEquipmentDlg::OnTableLButtonDblclk(UINT ipItem, LONG lpLParam)
{
	CListBox *prlListBox = pomEquTable->GetCTableListBox();
	if(prlListBox != NULL)
	{
		int ilSelLine = prlListBox->GetCurSel ();
		if(ilSelLine >= 0)
		{
			OnOK();
		}
	}
	return 0L;
}

void CAvailableEquipmentDlg::OnOK() 
{
	CString olText;
	UpdateData(TRUE);
	omSelectedEquUrnos.RemoveAll();

	if(prmSelectedDemand != NULL)
	{
		CListBox *prlListBox = pomEquTable->GetCTableListBox();
		if(prlListBox != NULL)
		{
			int ilSelLine = prlListBox->GetCurSel ();
			if(ilSelLine >= 0)
			{
				AVAILABLE_EQU_LINEDATA *prlLine = (AVAILABLE_EQU_LINEDATA *) pomEquTable->GetTextLineData(ilSelLine);
				if(prlLine != NULL)
				{
					omSelectedEquUrnos.Add(prlLine->Urno);
					bmTeamAlloc = false;
					CString strCfgTextData;
					strCfgTextData.Format("%s%s%d%s%s%s%s",TEXT_SHOW_ONLY_DEFINED_EQU,DATA_SEPARATOR,m_DisplayOnlyEquWithCorrectFunctionsAndPermits == TRUE ? 1 : 0,TEXT_SEPARATOR,TEXT_VIEW_NAME,DATA_SEPARATOR,pomEquViewer->GetViewName());
					if(pomCfgData == NULL)
					{
						ogCfgData.CreateCfg(CTYP_AVAILABLE_EQUIPMENTDLG,CKEY_FILTER_SETTINGS,strCfgTextData);
					}
					else
					{
						strcpy(pomCfgData->Text,strCfgTextData.GetBuffer(0));
						ogCfgData.DeleteCfg(pomCfgData->Urno);
						ogCfgData.CreateCfg(CTYP_AVAILABLE_EQUIPMENTDLG,CKEY_FILTER_SETTINGS,strCfgTextData);
					}
					CDialog::OnOK();
				}
				else
				{
					MessageBox(GetString(IDS_AVEQ_SELLINEERR),GetString(IDS_AVEQ_CAPTION),MB_ICONERROR);
				}
			}
			else
			{
				MessageBox(GetString(IDS_AVEQ_SELLINEERR),GetString(IDS_AVEQ_CAPTION),MB_ICONERROR);
			}
		}
		else
		{
			MessageBox("The equipment list box is NULL","Internal Error!",MB_ICONERROR);
		}
	}
	else
	{
		MessageBox(GetString(IDS_AVEQ_SELDEMERR),GetString(IDS_AVEQ_CAPTION),MB_ICONERROR);
	}
}

void CAvailableEquipmentDlg::AddDemand(DEMANDDATA *prpDemand)
{
	if(prpDemand != NULL)
	{
		omDemands.Add(prpDemand);
	}
}

void CAvailableEquipmentDlg::SetAllocUnit(const char *pcpAllocUnitType, const char *pcpAllocUnit)
{
	omAllocUnitType = pcpAllocUnitType;
	omAllocUnit = pcpAllocUnit;
}

// e.g. Registration name or flight name or single demand
void CAvailableEquipmentDlg::SetCaptionObject(CString opCaptionObject)
{
	omCaptionObject = opCaptionObject;
}

bool CAvailableEquipmentDlg::SelectDemand(DEMANDDATA *prpDemand)
{
	bool blChanged = false;
	if(prmSelectedDemand != prpDemand)
	{
		prmSelectedDemand = prpDemand;
		omDemandsViewer.prmSelectedDemand = prmSelectedDemand;
		GetAvailableEquipmentForDemand(prmSelectedDemand);
		blChanged = true;
	}

	return blChanged;
}

// if bpOnlyTestHowManyAvailable is true then returns how many pieces of equipment are available but doesn't display them
int CAvailableEquipmentDlg::GetAvailableEquipmentForDemand(DEMANDDATA *prpDemand, bool bpOnlyTestHowManyAvailable /* = false */)
{
	int ilNumEquAvailable = 0;
	omEquipment.DeleteAll();

	if(prpDemand != NULL)
	{
		CTime olStartTime = prpDemand->Debe;
		CTime olEndTime = prpDemand->Deen;

		//CMapPrtToPtr olShiftsAlreadyAdded; // required if for some reason the emp has more than one pool job for a shift
		//void *pvlDummy;

		// select all equipment
		int ilNumEqu = ogEquData.omData.GetSize();

		for (int ilE = 0; ilE < ilNumEqu; ilE++)
		{
			// ignore pool jobs which is out of the range of time of the given bar
			EQUDATA *prlEqu = &ogEquData.omData[ilE];

//			// if the pool restrictions doesn't contain this alloc unit
//			if(!omAllocUnit.IsEmpty() && !ogBasicData.IsInPool(omAllocUnitType,omAllocUnit,prlPoolJob->Alid))
//				continue;

//			if (!IsTotallyInside(olStartTime, olEndTime, prlPoolJob->Acfr, prlPoolJob->Acto))
//				continue;

			// check for equipment not available
			bool blEquNotAvailable = false;
			CCSPtrArray <BLKDATA> olBlocked;
			if(ogBlkData.GetBlocked("EQU", prlEqu->Urno, olBlocked) > 0)
			{
				int ilNumBlocked = olBlocked.GetSize();
				for (int ilC = 0; !blEquNotAvailable && ilC < ilNumBlocked; ilC ++)
				{
					BLKDATA *prlBlk = &olBlocked[ilC];

					CCSPtrArray<BLKBLOCKEDTIMES> olBlockedTimes;
					if (ogBlkData.GetBlockedTimes(prlBlk,ogBasicData.GetTimeframeStart(),ogBasicData.GetTimeframeEnd(),olBlockedTimes) > 0)
					{
						for(int ilC = 0; !blEquNotAvailable && ilC < olBlockedTimes.GetSize(); ilC++)
						{
							if(IsOverlapped(olStartTime, olEndTime, olBlockedTimes[ilC].Tifr,olBlockedTimes[ilC].Tito))
							{
								blEquNotAvailable = true;
							}
						}
						olBlockedTimes.DeleteAll();
					}
				}
			}
			if(!blEquNotAvailable)
			{
				CCSPtrArray<BLKBLOCKEDTIMES> olBlockedTimes;
				if(ogValData.GetBlockedTimes(prlEqu->Urno, ogBasicData.GetTimeframeStart(), ogBasicData.GetTimeframeEnd(), olBlockedTimes) > 0)
				{
					for(int ilC = 0; ilC < olBlockedTimes.GetSize(); ilC++)
					{
						if(IsOverlapped(olStartTime, olEndTime, olBlockedTimes[ilC].Tifr,olBlockedTimes[ilC].Tito))
						{
							blEquNotAvailable = true;
							break;
						}
					}
					olBlockedTimes.DeleteAll();
				}
			}

			if(blEquNotAvailable)
				continue;

			// check if existing jobs for the equipment overlap the demand
			CCSPtrArray <JOBDATA> olEquJobs;
			ogJobData.GetJobsByUequ(olEquJobs, prlEqu->Urno);
			int ilNumJobs = olEquJobs.GetSize();
			for(int ilJ = 0; !blEquNotAvailable && ilJ < ilNumJobs; ilJ++)
			{
				JOBDATA *prlJob = &olEquJobs[ilJ];
				if(strcmp(prlJob->Jtco,JOBEQUIPMENTFASTLINK) && IsOverlapped(olStartTime, olEndTime, prlJob->Acfr, prlJob->Acto))
				{
					blEquNotAvailable = true;
				}
			}
			if(blEquNotAvailable)
				continue;

			if(!bpOnlyTestHowManyAvailable)
			{
				AddEquipment(prlEqu);
			}
			ilNumEquAvailable++;
		}
	}


	//omEquipment.Sort(CompareWeight);

	return ilNumEquAvailable;
}

void CAvailableEquipmentDlg::AddEquipment(EQUDATA *prpEqu)
{
	// Third step -- if we reach this point we will add him to the array
	AVAILABLE_EQU_LINEDATA rlEquLine;

	rlEquLine.IsCorrectEquForDemand = ogBasicData.CheckDemandForCorrectEquipment(prmSelectedDemand, prpEqu);

	if(!m_DisplayOnlyEquWithCorrectFunctionsAndPermits || rlEquLine.IsCorrectEquForDemand)
	{
		rlEquLine.Urno = prpEqu->Urno;
		strcpy(rlEquLine.Enam, prpEqu->Enam);
		strcpy(rlEquLine.Etyp, prpEqu->Etyp);
		strcpy(rlEquLine.Gcde, prpEqu->Gcde);


		CCSPtrArray <FASTLINK> olFastLinks;
		ogJobData.GetEquipmentFastLinkJobsByUequAndTime(olFastLinks, prpEqu->Urno, prmSelectedDemand->Debe, prmSelectedDemand->Deen);
		int ilNumFastLinks = olFastLinks.GetSize();
		for(int ilFL = 0; ilFL < ilNumFastLinks; ilFL++)
		{
			CString olEmp;
			int ilNumJobs = olFastLinks[ilFL].Data.GetSize(); // ilNumJobs will be 2 if a pair of fast links is required

			if(ilNumJobs > 1)
			{
				rlEquLine.FastLinkEmps += "(";
			}
			for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
			{
				JOBDATA *prlFastLink = &olFastLinks[ilFL].Data[ilJ];
				EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prlFastLink->Peno);
				if(prlEmp != NULL)
				{
					CTime olStartJob = (prlFastLink->Acfr > prmSelectedDemand->Debe) ? prlFastLink->Acfr: prmSelectedDemand->Debe;
					CTime olEndJob = (prlFastLink->Acto < prmSelectedDemand->Deen) ? prlFastLink->Acto : prmSelectedDemand->Deen;
					if(!olEmp.IsEmpty())
					{
						olEmp += "+";
					}
					CString olEmp2;
					olEmp2.Format("%s %s-%s", ogEmpData.GetEmpName(prlEmp), olStartJob.Format("%H%M"), olEndJob.Format("%H%M"));
					olEmp += olEmp2;
				}
			}
			rlEquLine.FastLinkEmps += olEmp;
			if(ilNumJobs > 1)
			{
				rlEquLine.FastLinkEmps += ") ";
			}
		}

		//Attribute for equipment
		CCSPtrArray <EQADATA> olEqaList;
		ogEqaData.GetEqasByUequ(prpEqu->Urno, olEqaList);
		olEqaList.Sort(CompareEqAttributeName);

		int ilNumEqaList = olEqaList.GetSize();
		CString oleqa,olTemp;
		for(int ilAL = 0; ilAL < ilNumEqaList; ilAL++)
		{
			EQADATA eqa = olEqaList[ilAL];
			
			olTemp.Format("%s=%s;",eqa.Name, eqa.Valu);
			oleqa += olTemp;
		}
		//strcpy(rlEquLine.EquipAttribute,oleqa);
		 rlEquLine.EquipAttribute = oleqa;

		omEquipment.New(rlEquLine);
	}
}

// set the list of views
void CAvailableEquipmentDlg::UpdateComboBox()
{
	// check if the view box is enabled and displayed depending on the status in BDPSSEC
	CComboBox *polCB = (CComboBox *) GetDlgItem(IDC_VIEW);
	if(polCB != NULL && !ogBasicData.IsHidden("AVAILABLE_EQU_DLG IDC_VIEW"))
	{
		polCB->ResetContent();
		CStringArray olStrArr;
		pomEquViewer->GetViews(olStrArr);
		for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
		{
			polCB->AddString(olStrArr[ilIndex]);
		}
		CString olViewName = omLastSavedViewName;
		if (olViewName.IsEmpty())
		{
			olViewName = pomEquViewer->GetViewName();
				if (olViewName.IsEmpty() == TRUE)
			{
				olViewName = ogCfgData.rmUserSetup.EQCV;
			}
		}

		ilIndex = polCB->FindString(-1,olViewName);
			
		if (ilIndex != CB_ERR)
		{
			polCB->SetCurSel(ilIndex);
		}
	}
}

// view selected (or initializing)
void CAvailableEquipmentDlg::UpdateView() 
{
	char pclViewName[200];
	strcpy(pclViewName,GetString(IDS_STRINGDEFAULT));

	// check if the view box is enabled and displayed depending on the status in BDPSSEC
    CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_VIEW);
	if(polCB != NULL && !ogBasicData.IsHidden("AVAILABLE_EQU_DLG IDC_VIEW"))
	{
		polCB->GetLBText(polCB->GetCurSel(), pclViewName);
	}

	AfxGetApp()->DoWaitCursor(1);
	pomEquViewer->UpdateView(pclViewName);

	CString olCaption;
	// "%s: %d Employees Available." 
	olCaption.Format("%s: %d %s",omCaptionObject,pomEquViewer->GetLineCount(),GetString(IDS_AVEQ_CAPTION));
	SetWindowText(olCaption);
	AfxGetApp()->DoWaitCursor(-1);
}

int CAvailableEquipmentDlg::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	

	return 0;
}

void CAvailableEquipmentDlg::OnFilterfuncpermits() 
{
	UpdateData(TRUE);
	AfxGetApp()->DoWaitCursor(1);
	GetAvailableEquipmentForDemand(prmSelectedDemand);
	UpdateView();
	AfxGetApp()->DoWaitCursor(-1);
}

