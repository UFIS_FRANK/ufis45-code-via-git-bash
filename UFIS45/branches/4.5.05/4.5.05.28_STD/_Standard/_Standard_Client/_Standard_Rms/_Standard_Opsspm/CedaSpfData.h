//
// CedaSpfData.h - links emps (STFTAB) to their functions
//
#ifndef _CEDASPFDATA_H_
#define _CEDASPFDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>
#include <CCSDefines.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct SpfDataStruct
{
	long	Urno;
	long	Surn;		// Emp URNO (STFTAB.URNO)
	char	Code[21];	// Function Code
	CTime	Vpfr;		// Valid From
	CTime	Vpto;		// Valid To
	char	Prio[3];	// Priority for multiple functions where 1 = highest >1 lower
	int		Priority;	// atoi(Prio)
};

typedef struct SpfDataStruct SPFDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaSpfData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <SPFDATA> omData;
	CMapPtrToPtr omSurnMap;
	CString GetTableName(void);

// Operations
public:
	CedaSpfData();
	~CedaSpfData();

	bool ReadSpfData();
	void AddRecord(long lpUrno, long lpSurn, char *pcpCode, CTime opVpfr, CTime opVpto, char *pcpPrio);
	SPFDATA* GetSpfByUrno(long lpUrno);
	CString GetFunctionBySurn(long lpUrud, CTime opTime=TIMENULL);
	int GetFunctionsBySurn(long lpSurn, CStringArray &ropFunctions, CTime opTime=TIMENULL);
	void GetSpfsBySurn(long lpSurn, CCSPtrArray <SPFDATA> &ropSpfs, CTime opTime=TIMENULL, bool bpReset=true);

private:
	void AddSpfInternal(SPFDATA &rrpSpf);
	void ClearAll();
};


extern CedaSpfData ogSpfData;
#endif _CEDASPFDATA_H_
