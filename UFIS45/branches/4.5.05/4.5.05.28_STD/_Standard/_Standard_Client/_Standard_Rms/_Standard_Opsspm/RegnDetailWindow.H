// RegnDetailWindow.h : header file
//

#ifndef _RegnDW_
#define _RegnDW_

#include <cviewer.h>
#include <RegnDetailViewer.h>


/////////////////////////////////////////////////////////////////////////////
// RegnDetailWindow dialog

class RegnDetailWindow : public CDialog
{
// Construction
public:
	RegnDetailWindow(CWnd* pParent,const char *pcpAlid, BOOL bpArrival, BOOL bpDeparture,
		CTime opStartTime, CTime opEndTime);

// Dialog Data
	//{{AFX_DATA(RegnDetailWindow)
	enum { IDD = IDD_REGN_DETAIL };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(RegnDetailWindow)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnCancel();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(RegnDetailWindow)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	static void RegnDetailWindowCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
protected:
	char cmAlid[255];
	char *pcmAlid;
	BOOL bmArrival;
	BOOL bmDeparture;
	CTime omStartTime;
	CTime omEndTime;

	int m_nDialogBarHeight;
	CTable omTable;
	RegnDetailViewer omViewer;
	static RegnDetailWindow *omCurrent;
};

#endif // _RegnDW_