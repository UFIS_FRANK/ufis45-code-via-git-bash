// NewParaSet.cpp : implementation file
//

#include <stdafx.h>
#include <OpssPm.h>
#include <NewParaSet.h>
#include <Ccsglobl.h>

/*
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
*/

/////////////////////////////////////////////////////////////////////////////
// CNewParaSet dialog


CNewParaSet::CNewParaSet(CWnd* pParent, char *pcpNewParaSet)
	: CDialog(CNewParaSet::IDD, pParent)
{
	//{{AFX_DATA_INIT(CNewParaSet)
	//}}AFX_DATA_INIT

	pcmNewParaSet = pcpNewParaSet;
}


void CNewParaSet::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNewParaSet)
	DDX_Control(pDX, IDC_NEW_PARASET, m_ParaSet);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CNewParaSet, CDialog)
	//{{AFX_MSG_MAP(CNewParaSet)
	ON_EN_KILLFOCUS(IDC_NEW_PARASET, OnKillfocusNewParaset)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNewParaSet message handlers


void CNewParaSet::OnKillfocusNewParaset() 
{
/*
	char pclTmpParaSet[82];
	UpdateData(TRUE);
	m_ParaSet.GetWindowText(pcmNewParaSet, sizeof(pcmNewParaSet));
*/

	GetDlgItemText(IDC_NEW_PARASET, pcmNewParaSet, 80);
}

BOOL CNewParaSet::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	SetWindowText(GetString(IDS_STRING33003));
	CWnd *polWnd = GetDlgItem(IDC_PARAMNAME); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32923));
	}
	polWnd = GetDlgItem(IDOK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61697));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61698));
	}
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
