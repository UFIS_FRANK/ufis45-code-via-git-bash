#ifndef __REGNDV_H__
#define __REGNDV_H__

#include <CCSPtrArray.h>
#include <CedaShiftData.h>
#include <cviewer.h>
#include <table.h>
#include <CedaJobData.h>
#include <CedaFlightData.h>

struct REGNDETAIL_ASSIGNMENT  // each assignment record
{
    CString Name;   // assigned to gate or flight
	long Urno;      // flight job Urno 
    CTime Acfr;     // assigned from
    CTime Acto;     // assigned until
};

struct REGNDETAIL_LINEDATA
{
	long	FlightUrno;
	CString Fnum;	// Flight number
	CString Rou1;	// Origin/Destination
	CString Iofl;	// I/O flag
	CTime   FlightTime;	// Stoa/Stod depends on I/O flag

    CCSPtrArray<REGNDETAIL_ASSIGNMENT> Assignment;
};

/////////////////////////////////////////////////////////////////////////////
// RegnDetailViewer

class RegnDetailViewer: public CViewer
{
// Constructions
public:
    RegnDetailViewer();
    ~RegnDetailViewer();

    void Attach(CTable *popTable,CTime opStartTime,CTime opEndTime,CString opAlid);
    void ChangeView();

// Internal data processing routines
private:
	void PrepareGrouping();
    void PrepareFilter();
	void PrepareSorter();
	BOOL IsPassFilter(CTime opAcfr);

    void MakeLines();
	void MakeLine(FLIGHTDATA *prpFlight);
	void MakeAssignments(long lpFlightUrno,REGNDETAIL_LINEDATA &rrpFlightLine);
	int CreateAssignment(REGNDETAIL_LINEDATA &rrlStaff, REGNDETAIL_ASSIGNMENT *prpAssignment);

	BOOL FindFlight(long lpFlightUrno, int &rilLineno);

// Operations
public:
	void DeleteAll();
	int CreateLine(REGNDETAIL_LINEDATA *prpStaff);
	void DeleteLine(int ipLineno);

// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(REGNDETAIL_LINEDATA *prpLine);
	void ProcessFlightChange(FLIGHTDATA *prpFlight);

// Attributes used for filtering condition
private:
    CString omDate;
    CTime omStartTime;
	CTime omEndTime;

// Attributes
private:
    CTable *pomTable;
    CCSPtrArray<REGNDETAIL_LINEDATA> omLines;
	CString omAlid;
// Methods which handle changes (from Data Distributor)
public:
};

#endif //__REGNDV_H__
