#if !defined(AFX_REGNDIAGRAM_H__E70E8871_A7F5_11D3_AC65_0000B45FD487__INCLUDED_)
#define AFX_REGNDIAGRAM_H__E70E8871_A7F5_11D3_AC65_0000B45FD487__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RegnDiagram.h : header file
//

#include <clientwn.h>
#include <tscale.h>
#include <RegnViewer.h>

/////////////////////////////////////////////////////////////////////////////
// RegnDiagram frame

class RegnDiagram : public CFrameWnd
{
	DECLARE_DYNCREATE(RegnDiagram)
public:
	RegnDiagram();           // protected constructor used by dynamic creation
	RegnDiagram(BOOL bpPrePlanMode,CTime opPrePlanTime);    
// Attributes
public:

private:
	BOOL bmIsViewOpen;
	CRect omWindowRect; //PRF 8712

// Operations
public:
	CTime omTimeBandStartTime, omTimeBandEndTime;
	void SetTimeBand(CTime opStartTime, CTime opEndTime);
	void UpdateTimeBand();
    void PositionChild();
    void SetTSStartTime(CTime opTSStartTime);
	void UpdateComboBox();
	void ChangeViewTo(const char *pcpViewName);
	void OnUpdatePrevNext(void);
	void PrePlanMode(BOOL bpToSet,CTime opPrePlanTime);
	void SetCaptionText(void);
	BOOL DestroyWindow() ;
	CTime GetTsStartTime();
    CTimeSpan GetTsDuration(void);
    void OnFirstChart();
    void OnLastChart();
	BOOL bmNoUpdatesNow;

private:
	CListBox *GetBottomMostGantt();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(RegnDiagram)
	//}}AFX_VIRTUAL

public:
	void SetRegnAreaButtonColor(int ipGroupno);
	void SetAllRegnAreaButtonsColor(void);

// Implementation
protected:
	virtual ~RegnDiagram();

	// Generated message map functions
	//{{AFX_MSG(RegnDiagram)
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnClose();
    afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
    afx_msg void OnPaint();
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnMove(int x, int y); //PRF 8712
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void OnNextChart();
    afx_msg void OnPrevChart();
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg LONG OnPositionChild(WPARAM wParam, LPARAM lParam);
    afx_msg LONG OnUpdateDiagram(WPARAM wParam, LPARAM lParam);
    afx_msg void OnArrival();
    afx_msg void OnDeparture();
    afx_msg void OnAnsicht();
	afx_msg void OnViewSelChange();
	afx_msg void OnCloseupView();
    afx_msg void OnMabstab();
    afx_msg void OnAssign();
    afx_msg void OnThirdPartyFilter();
    afx_msg void OnZeit();
	afx_msg void OnPrint();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnUpdateUIZeit(CCmdUI *pCmdUI);
	afx_msg void OnUpdateUIMabstab(CCmdUI *pCmdUI);
	afx_msg void OnUpdateUIPrint(CCmdUI *pCmdUI);
	afx_msg void OnUpdateUIAssign(CCmdUI *pCmdUI);
	afx_msg void OnUpdateUIThirdParty(CCmdUI *pCmdUI);
	afx_msg void OnUpdateUIAnsicht(CCmdUI *pCmdUI);
	afx_msg void OnUpdateUIView(CCmdUI *pCmdUI);
    afx_msg LONG OnSelectDiagram(WPARAM wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
protected:
    CDialogBar omDialogBar;
    C2StateButton omArrival;
    C2StateButton omDeparture;
    C3DStatic omTime;
    C3DStatic omDate;
    C3DStatic omTSDate;
    
    CTimeScale omTimeScale;
    CBitmapButton omBB1, omBB2;
    
    CClientWnd omClientWnd;
    CStatusBar omStatusBar;
    
    CTime omStartTime;
    CTimeSpan omDuration;
    CTime omTSStartTime;
    CTimeSpan omTSDuration;
    CTimeSpan omTSInterval;
    
    CPtrArray omPtrArray;
    int imFirstVisibleChart;
    int imStartTimeScalePos;
    
    CTime omPrePlanTime;
	BOOL omPrePlanMode;
	CString omCaptionText;

	long CalcTotalMinutes();

    RegnDiagramViewer omViewer;

// Redisplay all methods for DDX call back function
public:
	void RedisplayAll();
	void HandleGlobalDateUpdate(CTime opDate);

private:
    static CPoint omMaxTrackSize;
    static CPoint omMinTrackSize;
    static COLORREF lmBkColor;
    static COLORREF lmTextColor;
    static COLORREF lmHilightColor;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REGNDIAGRAM_H__E70E8871_A7F5_11D3_AC65_0000B45FD487__INCLUDED_)
