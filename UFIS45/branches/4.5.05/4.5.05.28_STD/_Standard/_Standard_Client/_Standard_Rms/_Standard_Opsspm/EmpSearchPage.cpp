// EmpSearchPage.cpp : implementation file
//

#include <stdafx.h>
#include <OpssPm.h>
#include <CCSTime.h>
#include <FilterData.h>
#include <EmpSearchPage.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// EmpSearchPage property page

IMPLEMENT_DYNCREATE(EmpSearchPage, CPropertyPage)

int EmpSearchPage::imLastState = -1;

EmpSearchPage::EmpSearchPage() : CPropertyPage(EmpSearchPage::IDD)
{
	//{{AFX_DATA_INIT(EmpSearchPage)
	m_Name = _T("");
	m_Rank = _T("");
	m_Shift = _T("");
	m_SingleEmployee = 1;
	//}}AFX_DATA_INIT

//	m_Date = ogBasicData.GetTime();
	omTitle = GetString(IDS_STRING32861);
	m_psp.pszTitle = omTitle;
	m_psp.dwFlags |= PSP_USETITLE;

	m_SingleEmployee = EmployeeDetailDisplayAsDefault();

}

EmpSearchPage::~EmpSearchPage()
{
}

int EmpSearchPage::EmployeeDetailDisplayAsDefault() const
{
	if (imLastState == -1)
	{
		char pclConfigPath[512];
		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));
		char pclDetailDisplay[512];
		GetPrivateProfileString(pcgAppName, "STAFFDETAILDISPLAY", "YES",pclDetailDisplay, sizeof pclDetailDisplay, pclConfigPath);
		if (stricmp(pclDetailDisplay,"YES") != 0)
			imLastState = 0;
		else
			imLastState = 1;
	}

	return imLastState;
}

void EmpSearchPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(EmpSearchPage)
//	DDX_CCSddmmyy(pDX, IDC_DATE, m_Date);
	DDX_Control(pDX, IDC_DATE, m_Date);
	DDX_Text(pDX, IDC_NAME, m_Name);
	DDX_Text(pDX, IDC_RANK, m_Rank);
	DDX_Text(pDX, IDC_SHIFT, m_Shift);
	DDX_Radio(pDX,IDC_GANTT_SELECT,m_SingleEmployee);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(EmpSearchPage, CPropertyPage)
	//{{AFX_MSG_MAP(EmpSearchPage)
	ON_BN_CLICKED(IDC_GANTT_SELECT, OnRadioGanttSelect)
	ON_BN_CLICKED(IDC_DETAIL_DISPLAY, OnRadioDetailDisplay)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// EmpSearchPage message handlers

BOOL EmpSearchPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	//SetWindowText(GetString(IDS_STRING32861));
	CWnd *polWnd = GetDlgItem(IDC_EMPNAME); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32923));
	}
	polWnd = GetDlgItem(IDC_EMPSHIFT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32924));
	}
	polWnd = GetDlgItem(IDC_EMPGRADE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32925));
	}
	polWnd = GetDlgItem(IDC_EMPDATE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32855));
	}

	polWnd = GetDlgItem(IDC_FLIGHT_GROUP);
	if (polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61913));
	}
	polWnd = GetDlgItem(IDC_GANTT_SELECT);
	if (polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61905));
	}
	polWnd = GetDlgItem(IDC_DETAIL_DISPLAY);
	if (polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61906));
	}

	// TODO: Add extra initialization here

	m_Name = CString(ogSearchFilter.rlEmpFilterData.Name);
	m_Rank = CString(ogSearchFilter.rlEmpFilterData.Rank);
	m_Shift = CString(ogSearchFilter.rlEmpFilterData.Shift);

	CStringArray olTimeframeList;
	ogBasicData.GetTimeframeList(olTimeframeList);
	int ilNumDays = olTimeframeList.GetSize();
	for(int ilDay = 0; ilDay < ilNumDays; ilDay++)
	{
		m_Date.AddString(olTimeframeList[ilDay]);
	}


	int ilDisplayDateOffset = ogBasicData.GetDisplayDateOffsetByDate(ogSearchFilter.rlEmpFilterData.Date);
	if(ilDisplayDateOffset >= 0 && ilDisplayDateOffset < ilNumDays)
	{
		m_Date.SetCurSel(ilDisplayDateOffset);
	}
	else
	{
		if(ilNumDays > 0)
		{
			m_Date.SetCurSel(ogBasicData.GetDisplayDateOffset());
		}
	}
	

	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void EmpSearchPage::OnRadioGanttSelect()
{
	UpdateData(TRUE);
	imLastState = m_SingleEmployee;
}

void EmpSearchPage::OnRadioDetailDisplay()
{
	UpdateData(TRUE);
	imLastState = m_SingleEmployee;
}