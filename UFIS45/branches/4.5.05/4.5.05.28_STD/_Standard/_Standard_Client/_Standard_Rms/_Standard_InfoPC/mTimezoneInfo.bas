Attribute VB_Name = "MTimezoneInfo"
Option Explicit

Public Type SYSTEMTIME
    wYear As Integer
    wMonth As Integer
    wDayOfWeek As Integer
    wDay As Integer
    wHour As Integer
    wMinute As Integer
    wSecond As Integer
    wMilliseconds As Integer
End Type

Public Type TIME_ZONE_INFORMATION
    Bias As Long
    StandardName(63) As Byte  ' <-- zero based array so there are actually 64 bytes in the array
    StandardDate As SYSTEMTIME
    StandardBias As Long
    DaylightName(63) As Byte  ' <-- zero based array so there are actually 64 bytes in the array
    DaylightDate As SYSTEMTIME
    DaylightBias As Long
End Type

Public Const TIME_ZONE_ID_INVALID As Long = &HFFFFFFFF
Public Const TIME_ZONE_ID_UNKNOWN As Long = 0&
Public Const TIME_ZONE_ID_STANDARD As Long = 1&
Public Const TIME_ZONE_ID_DAYLIGHT As Long = 2&

Public Declare Function GetTimeZoneInformation Lib "kernel32" (lpTimeZoneInformation As TIME_ZONE_INFORMATION) As Long
Public Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (lpDest As Any, lpSource As Any, ByVal cBytes&)

Public Function GetUTCOffsetHours() As Single

    Dim tzi As TIME_ZONE_INFORMATION, sCurTimeName$, sMsg$

    ' call the function to get the timezone info.
    GetTimeZoneInformation tzi
    GetUTCOffsetHours = (tzi.Bias / 60) * (-1)

End Function


'Private Function TranslateDay(ByVal nDayOfWeek&, ByVal nDay&) As String
'  ' this function is used if the date formate is of the Day-In-Month type
'
'  Dim sReturn$
'
'  sReturn = "The "
'
'  Select Case nDay
'    Case 1: sReturn = sReturn & "First "
'    Case 2: sReturn = sReturn & "Second "
'    Case 3: sReturn = sReturn & "Third "
'    Case 4: sReturn = sReturn & "Fourth "
'    Case 5: sReturn = sReturn & "Last "
'  End Select
'
'  Select Case nDayOfWeek
'    Case 0: sReturn = sReturn & "Sunday"
'    Case 1: sReturn = sReturn & "Monday"
'    Case 2: sReturn = sReturn & "Tuesday"
'    Case 3: sReturn = sReturn & "Wednesday"
'    Case 4: sReturn = sReturn & "Thursday"
'    Case 5: sReturn = sReturn & "Friday"
'    Case 6: sReturn = sReturn & "Saturday"
'  End Select
'
'  TranslateDay = sReturn & " In"
'
'End Function

'Private Function GetMonth(ByVal nMonth&) As String
'  ' returns the name of the month
'
'  Select Case nMonth
'    Case 1: GetMonth = "January"
'    Case 2: GetMonth = "February"
'    Case 3: GetMonth = "March"
'    Case 4: GetMonth = "April"
'    Case 5: GetMonth = "May"
'    Case 6: GetMonth = "June"
'    Case 7: GetMonth = "July"
'    Case 8: GetMonth = "August"
'    Case 9: GetMonth = "September"
'    Case 10: GetMonth = "October"
'    Case 11: GetMonth = "November"
'    Case 12: GetMonth = "December"
'  End Select
'End Function

'Public Function StripNulls(ByVal sText As String) As String
'' strips any nulls from the end of a string
'  Dim nPosition&
'
'  StripNulls = sText
'
'  nPosition = InStr(sText, vbNullChar)
'  If nPosition Then StripNulls = Left$(sText, nPosition - 1)
'  If Len(sText) Then If Left$(sText, 1) = vbNullChar Then StripNulls = ""
'End Function
