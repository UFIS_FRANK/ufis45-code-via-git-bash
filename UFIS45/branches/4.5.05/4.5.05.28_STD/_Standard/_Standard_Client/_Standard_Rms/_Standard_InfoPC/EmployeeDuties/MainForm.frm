VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{A2F31E92-C74F-11D3-A251-00500437F607}#1.0#0"; "UfisCom.ocx"
Begin VB.Form MainForm 
   BorderStyle     =   0  'None
   Caption         =   "Tab and UFIS-COM Test"
   ClientHeight    =   2955
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   2955
   LinkTopic       =   "Form1"
   ScaleHeight     =   197
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   197
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.Timer tmrMinutes 
      Interval        =   60000
      Left            =   240
      Top             =   1860
   End
   Begin TABLib.TAB TAB1 
      Height          =   1635
      Left            =   60
      TabIndex        =   0
      Top             =   120
      Width           =   1695
      _Version        =   65536
      _ExtentX        =   2990
      _ExtentY        =   2884
      _StockProps     =   64
   End
   Begin UFISCOMLib.UfisCom Ufis 
      Left            =   1140
      Top             =   1890
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   661
      _StockProps     =   0
   End
End
Attribute VB_Name = "MainForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Application Defines
'Private Const DEFAULT_CEDA_INI = "C:\UFIS\SYSTEM\CEDA.INI"

Public ConfigFileName As String
Public blIsConnected As Boolean
Public isread As Boolean
Public omDisplayMode As String
Public currScrollPos As Integer
Public US As UFisServer
Public COLUMN_PAIRS As Integer
Public COLUMN_WIDTH As Integer
Public TAB_LINES As Long
Public LINEHEIGHT As Integer
Public TAB_FONT As String
Public TAB_FONT_SIZE As Integer
Public MINUS_X_HOURS As Integer
Public PLUS_X_HOURS As Integer
Public COLOR1 As Long
Public COLOR2 As Long
Public JOBTYPES As String
Public SERVER As String
Public FUNCTIONS As String
Public HOPO As String
Public BC_HIGHLIGHT_SECONDS As Integer
Public COLUMN_PAIR_SEPARATOR As Integer
Public LOGO As String

Public RELOAD_MINUTES As Long
Private lmReloadMinutes As Long


Private Sub InitValues()
    Dim isResult As String
    Dim isConfigString As String
    
    If DEFAULT_CEDA_INI = "" Then GetUfisDir
    'ConfigFileName = "C:\Ufis\System\EmployeeList.cfg"
    ConfigFileName = UFIS_SYSTEM & "\EmployeeList.cfg"
    COLUMN_PAIRS = 4
    COLUMN_WIDTH = 82
    LINEHEIGHT = 30
    TAB_LINES = 28
    TAB_FONT = "Courier New"
    TAB_FONT_SIZE = 22
    MINUS_X_HOURS = 2
    PLUS_X_HOURS = 10
    COLOR1 = 12582912
    COLOR2 = 16711680
    RELOAD_MINUTES = 15
    BC_HIGHLIGHT_SECONDS = 10
    COLUMN_PAIR_SEPARATOR = 5

    isConfigString = GetFileContent(ConfigFileName)
'FUNCTIONS
    GetKeyItem isResult, isConfigString, "{=FUNCTIONS=}", "{=\=}"
    If isResult <> "" Then
        FUNCTIONS = isResult
    End If
'SERVER and HOPO
    GetKeyItem isResult, isConfigString, "{=SERVER=}", "{=\=}"
    If isResult <> "" Then
        SERVER = isResult
    End If
    GetKeyItem isResult, isConfigString, "{=HOPO=}", "{=\=}"
    If isResult <> "" Then
        HOPO = isResult
    End If
'Other configs
    GetKeyItem isResult, isConfigString, "{=JOBTYPES=}", "{=\=}"
    If isResult <> "" Then
        JOBTYPES = isResult
    End If
    GetKeyItem isResult, isConfigString, "{=COLUMN_PAIRS=}", "{=\=}"
    If isResult <> "" Then
        COLUMN_PAIRS = CInt(isResult)
    End If
    GetKeyItem isResult, isConfigString, "{=COLUMN_WIDTH=}", "{=\=}"
    If isResult <> "" Then
        COLUMN_WIDTH = CInt(isResult)
    End If
    GetKeyItem isResult, isConfigString, "{=TAB_LINES=}", "{=\=}"
    If isResult <> "" Then
        TAB_LINES = CLng(isResult)
    End If
    GetKeyItem isResult, isConfigString, "{=LINEHEIGHT=}", "{=\=}"
    If isResult <> "" Then
        LINEHEIGHT = CInt(isResult)
    End If
    GetKeyItem isResult, isConfigString, "{=TAB_FONT=}", "{=\=}"
    If isResult <> "" Then
        TAB_FONT = isResult
    End If
    GetKeyItem isResult, isConfigString, "{=TAB_FONT_SIZE=}", "{=\=}"
    If isResult <> "" Then
        TAB_FONT_SIZE = CInt(isResult)
    End If
    GetKeyItem isResult, isConfigString, "{=MINUS_X_HOURS=}", "{=\=}"
    If isResult <> "" Then
        MINUS_X_HOURS = CInt(isResult)
    End If
    GetKeyItem isResult, isConfigString, "{=PLUS_X_HOURS=}", "{=\=}"
    If isResult <> "" Then
        PLUS_X_HOURS = CInt(isResult)
    End If
    GetKeyItem isResult, isConfigString, "{=COLOR1=}", "{=\=}"
    If isResult <> "" Then
        COLOR1 = CLng(isResult)
    End If
    GetKeyItem isResult, isConfigString, "{=COLOR2=}", "{=\=}"
    If isResult <> "" Then
        COLOR2 = CLng(isResult)
    End If
    If (GetKeyItem(isResult, isConfigString, "{=RELOAD_MINUTES=}", "{=\=}") <> False) Then
        If isResult <> "" Then
            RELOAD_MINUTES = CLng(isResult)
        End If
    End If
    If (GetKeyItem(isResult, isConfigString, "{=BC_HIGHLIGHT_SECONDS=}", "{=\=}") <> False) Then
        If isResult <> "" Then
            BC_HIGHLIGHT_SECONDS = CInt(isResult)
        End If
    End If
    If (GetKeyItem(isResult, isConfigString, "{=COLUMN_PAIR_SEPARATOR=}", "{=\=}") <> False) Then
        If isResult <> "" Then
            COLUMN_PAIR_SEPARATOR = CInt(isResult)
        End If
    End If
    If (GetKeyItem(isResult, isConfigString, "{=LOGO=}", "{=\=}") <> False) Then
        If isResult <> "" Then
            LOGO = isResult
        End If
    End If


    save_COLUMN_PAIRS = COLUMN_PAIRS
    save_COLUMN_WIDTH = COLUMN_WIDTH
    save_TAB_LINES = TAB_LINES
    save_LINEHEIGHT = LINEHEIGHT
    save_TAB_FONT = TAB_FONT
    save_TAB_FONT_SIZE = TAB_FONT_SIZE
    save_MINUS_X_HOURS = MINUS_X_HOURS
    save_PLUS_X_HOURS = PLUS_X_HOURS
    save_COLOR1 = COLOR1
    save_COLOR2 = COLOR2
    save_BC_HIGHLIGHT_SECONDS = BC_HIGHLIGHT_SECONDS
    save_RELOAD_MINUTES = RELOAD_MINUTES
    save_FUNCTIONS = FUNCTIONS
    save_COLUMN_PAIR_SEPARATOR = COLUMN_PAIR_SEPARATOR
    save_LOGO = LOGO

    MainForm.Font.Name = TAB_FONT
    MainForm.Font.Size = TAB_FONT_SIZE
    MainForm.Font.Bold = True
End Sub


Private Sub Form_Load()
    Dim i As Integer
    Dim j As Integer
    Dim sHeader As String
    Dim sAlignment As String
    Dim sHeaderLenStr As String
    Dim isLineValues As String
    Dim strCellValue As String
    Dim A As Integer
    Dim llLineCounter As Long
    Dim datStart As Date

    InitValues 'call to initialize the variables
    TAB1.ResetContent
    TAB1.LINEHEIGHT = LINEHEIGHT
    TAB1.FontName = TAB_FONT
    TAB1.HeaderFontSize = TAB_FONT_SIZE
    TAB1.FontSize = TAB_FONT_SIZE
    TAB1.SetTabFontBold True
    TAB1.ShowVertScroller False
    omDisplayMode = "Arrival"

    Load frmTables
    'frmTables.Show vbModeless
    frmTables.LoadAllTables

    TAB1.CreateCellObj "COLOR1", COLOR1, vbYellow, TAB_FONT_SIZE, False, _
                       False, True, 1, TAB_FONT
    TAB1.CreateCellObj "COLOR2", COLOR2, vbYellow, TAB_FONT_SIZE, False, _
                       False, True, 1, TAB_FONT
    TAB1.CreateCellObj "HIGHLIGHT", &HFF00FF, vbYellow, TAB_FONT_SIZE, False, _
                       False, True, 1, TAB_FONT
    TAB1.CreateDecorationObject "PAIR_SEPARATOR", "R", CStr(COLUMN_PAIR_SEPARATOR), TAB1.GridLineColor

    currScrollPos = 0
    Set US = New UFisServer
    US.InitBroadcasts

    For i = 0 To COLUMN_PAIRS - 1
        If i = 0 Then
            sHeader = sHeader & LoadResString(118) & "," & LoadResString(119)
            sHeaderLenStr = sHeaderLenStr & CStr(COLUMN_WIDTH) & "," & CStr(COLUMN_WIDTH)
            sAlignment = sAlignment & "C,C"
        Else
            sHeader = sHeader & "," & LoadResString(118) & "," & LoadResString(119)
            sHeaderLenStr = sHeaderLenStr & "," & CStr(COLUMN_WIDTH) & "," & CStr(COLUMN_WIDTH)
            sAlignment = sAlignment & ",C,C"
        End If
    Next i

    TAB1.HeaderString = sHeader
    TAB1.ColumnAlignmentString = sAlignment
    TAB1.HeaderAlignmentString = sAlignment

    For i = 0 To TAB_LINES - 1
        isLineValues = ""
        For j = 0 To COLUMN_PAIRS - 1
            If j = 0 Then
                isLineValues = isLineValues + ","
            Else
                isLineValues = isLineValues + ",,"
            End If
        Next j
        TAB1.InsertTextLine isLineValues, True
        A = i Mod 2
        If A = 0 Then
          TAB1.SetLineColor i, vbYellow, COLOR1
        Else
          TAB1.SetLineColor i, vbYellow, COLOR2
        End If

        ' setting the column pair separator
        For j = 0 To COLUMN_PAIRS - 1
            TAB1.SetDecorationObject i, j * 2 + 1, "PAIR_SEPARATOR"
        Next j
    Next i
    RebuildScreen

    'starting the timer to reload
    tmrMinutes.Interval = 60000
    tmrMinutes.Enabled = True
    lmReloadMinutes = RELOAD_MINUTES
End Sub

Private Sub Form_Resize()
    On Error Resume Next
    Dim X As Integer
    Dim Y As Integer
    Dim j As Integer
    Dim ilWidth As Integer
    Dim ilColLen As Integer
    Dim strHeaderLen As String

    X = MainForm.Width
    Y = MainForm.Height
    TAB1.Move 0, 0, X, Y

    ilWidth = Screen.Width * (1 / Screen.TwipsPerPixelX)
    ilColLen = CInt((ilWidth / COLUMN_PAIRS) / 2)

    For j = 0 To COLUMN_PAIRS - 1
        If j = 0 Then
            strHeaderLen = CStr(ilColLen) & "," & CStr(ilColLen)
        Else
            strHeaderLen = strHeaderLen & "," & CStr(ilColLen) & "," & CStr(ilColLen)
        End If
    Next j
    TAB1.HeaderLengthString = strHeaderLen
End Sub

Private Sub Form_Terminate()
    Ufis.CleanupCom
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Ufis.CleanupCom
End Sub

Private Sub TAB1_HitKeyOnLine(ByVal Key As Integer, ByVal LineNo As Long)
    If Key = 13 Then 'Return Key was pressed
        End
    End If
    If Key = 65 Then ' "A" was pressed
        frmSetup.Show vbModal
    End If
    If Key = 66 Then ' "B" was pressed
        'frmTest.Show vbModal
    End If
    If Key = 80 Then
        'TAB1.PrintTab Tab1.P, "", CStr(Now)
    End If
End Sub

'------------------------------------------------
' Write the current value setting to config file
'------------------------------------------------
Public Sub SaveConfigFile()
    Dim strLine As String
    Dim buffer As String
    Dim arrJobTypes() As String
    Dim strJobTypes As String
    Dim i As Integer
    Dim count As Integer
    
    arrJobTypes = Split(JOBTYPES, ",")
    count = UBound(arrJobTypes)
    For i = 0 To count
        If i < count Then
            strJobTypes = strJobTypes + arrJobTypes(i) + ","
        Else
            strJobTypes = strJobTypes + arrJobTypes(i) + ""
        End If
    Next i
    If ConfigFileName <> "" Then
        Open ConfigFileName For Output As #1   ' Open file for input.
        'Open filename For Binary Access Write As #1   ' Open file for input.
        strLine = "{=SERVER=}" + SERVER + "{=\=}"
        Print #1, strLine
        strLine = "{=HOPO=}" + HOPO + "{=\=}"
        Print #1, strLine
        strLine = "{=COLUMN_PAIRS=}" + CStr(save_COLUMN_PAIRS) + "{=\=}"
        Print #1, strLine
        strLine = "{=COLUMN_WIDTH=}" + CStr(save_COLUMN_WIDTH) + "{=\=}"
        Print #1, strLine
        strLine = "{=TAB_LINES=}" + CStr(save_TAB_LINES) + "{=\=}"
        Print #1, strLine
        strLine = "{=LINEHEIGHT=}" + CStr(save_LINEHEIGHT) + "{=\=}"
        Print #1, strLine
        strLine = "{=TAB_FONT=}" + save_TAB_FONT + "{=\=}"
        Print #1, strLine
        strLine = "{=TAB_FONT_SIZE=}" + CStr(save_TAB_FONT_SIZE) + "{=\=}"
        Print #1, strLine
        strLine = "{=MINUS_X_HOURS=}" + CStr(save_MINUS_X_HOURS) + "{=\=}"
        Print #1, strLine
        strLine = "{=PLUS_X_HOURS=}" + CStr(save_PLUS_X_HOURS) + "{=\=}"
        Print #1, strLine
        strLine = "{=COLOR1=}" + CStr(save_COLOR1) + "{=\=}"
        Print #1, strLine
        strLine = "{=COLOR2=}" + CStr(save_COLOR2) + "{=\=}"
        Print #1, strLine
        strLine = "{=RELOAD_MINUTES=}" + CStr(save_RELOAD_MINUTES) + "{=\=}"
        Print #1, strLine
        strLine = "{=BC_HIGHLIGHT_SECONDS=}" + CStr(save_BC_HIGHLIGHT_SECONDS) + "{=\=}"
        Print #1, strLine
        strLine = "{=COLUMN_PAIR_SEPARATOR=}" + CStr(save_COLUMN_PAIR_SEPARATOR) + "{=\=}"
        Print #1, strLine
        strLine = "{=LOGO=}" + CStr(save_LOGO) + "{=\=}"
        Print #1, strLine
        strLine = "{=JOBTYPES=}" + strJobTypes + "{=\=}"
        Print #1, strLine
        strLine = "{=FUNCTIONS=}" + save_FUNCTIONS + "{=\=}"
        Print #1, strLine
        Close #1
    End If
End Sub
'--------------------------------------------------------------
' Rebuilds the main screen out of the frmTables.tabResult
' Alternate the back color of the lines to produce a better
' readable screen in the style of FIDS
'--------------------------------------------------------------
Public Function RebuildScreen()
    Dim isLineValues As String
    Dim strCellValue As String
    Dim llLineCounter As Long
    Dim datStart As Date
    Dim i As Integer
    Dim j As Integer

    llLineCounter = 0

    For i = 0 To COLUMN_PAIRS - 1
        For j = 0 To TAB_LINES - 1
            strCellValue = frmTables.tabResult.GetColumnValue(llLineCounter, 1)
            If strCellValue <> "" Then
                datStart = CedaFullDateToVb(strCellValue)
                strCellValue = Format(datStart + (sgUTCOffsetHours / 24), "hh:mm")
            End If
            TAB1.SetColumnValue j, (i * 2), strCellValue
            strCellValue = frmTables.tabResult.GetColumnValue(llLineCounter, 2)
            TAB1.SetColumnValue j, ((i * 2) + 1), strCellValue
            llLineCounter = llLineCounter + 1
        Next j
    Next i
    TAB1.RedrawTab
End Function

'---------------------------------------------------------
' Find the line in frmTables.tabResult to calculate the
' position of the cells to be highlighted in the Main TAB
' Set the Cell Property and the highlight timer values
' Search the line number and calc the cell y and x coords.
'---------------------------------------------------------
Public Function HighlightCells(cpUrno As String)
    Dim strLineNo As String
    Dim llLineNo As Long
    Dim llDestLineNo As Long
    Dim llDestColNo As Long

    strLineNo = frmTables.tabResult.GetLinesByColumnValue(0, cpUrno, 1)
    If strLineNo <> "" Then
        llLineNo = strLineNo
        llDestLineNo = (llLineNo Mod TAB_LINES) ' - 1
        llDestColNo = (llLineNo \ TAB_LINES) * 2
        TAB1.SetCellProperty llDestLineNo, llDestColNo, "HIGHLIGHT"
        TAB1.SetCellProperty llDestLineNo, llDestColNo + 1, "HIGHLIGHT"
        frmTables.tabResult.TimerSetValue llLineNo, BC_HIGHLIGHT_SECONDS
        frmTables.tabResult.SetLineStatusValue llLineNo, 1
        TAB1.RedrawTab
    End If
End Function

'---------------------------------------------------------
' recolor the lines after sorting
'---------------------------------------------------------
Public Function ManageColors()
    Dim strLineNo As String
    Dim llLineNo As Long
    Dim llDestLineNo As Long
    Dim llDestColNo As Long
    Dim i As Integer

    'reset all colors of the display-TAB (TAB1)
    For i = 0 To TAB1.GetLineCount - 1
        TAB1.ResetCellProperties i
    Next i

    'getting all line-numbers of the lines to be colored
    strLineNo = frmTables.tabResult.GetLinesByStatusValue(1, 0)

    'set all colors of the display-TAB (TAB1)
    For i = 1 To ItemCount(strLineNo, ",")
        llLineNo = CLng(GetItem(strLineNo, i, ","))
        llDestLineNo = (llLineNo Mod TAB_LINES) ' - 1
        llDestColNo = (llLineNo \ TAB_LINES) * 2
        TAB1.SetCellProperty llDestLineNo, llDestColNo, "HIGHLIGHT"
        TAB1.SetCellProperty llDestLineNo, llDestColNo + 1, "HIGHLIGHT"
    Next i

    'now redraw, so that the colors will take effect
    TAB1.RedrawTab
End Function

Private Sub tmrMinutes_Timer()
    lmReloadMinutes = lmReloadMinutes - 1
    If lmReloadMinutes = 0 Then
        'stop the timer
        tmrMinutes.Enabled = False

        'reload the data
        frmTables.LoadAllTables
        RebuildScreen

        'restart the timer
        lmReloadMinutes = RELOAD_MINUTES
        tmrMinutes.Enabled = True
    End If
End Sub
