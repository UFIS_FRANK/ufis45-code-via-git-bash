// ValuesGrid.cpp : implementation file
//

#include <stdafx.h>

#include <ccsglobl.h>
#include <ProfileEditor.h>
#include <ValuesGrid.h>
#include <ProfileEditorDoc.h>
#include <ProfileChart.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CValuesGrid

CValuesGrid::CValuesGrid()
{
	bmEditMode = false;
}

CValuesGrid::~CValuesGrid()
{
}


BEGIN_MESSAGE_MAP(CValuesGrid, CGridControl)
	//{{AFX_MSG_MAP(CValuesGrid)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CValuesGrid message handlers
BOOL CValuesGrid::Open ( CProfileChart *popChart, UINT idc )
{
	CRect rect;

	umID = idc;
	pomWnd = popChart;
	
	CWnd *polWnd;
	polWnd = popChart->GetDlgItem ( idc );
	polWnd->ShowWindow ( SW_HIDE );

    polWnd->GetWindowRect(&rect);
	popChart->ScreenToClient ( &rect );

	DWORD lWndStyle = WS_CHILD | WS_VISIBLE;
		
	lWndStyle |= WS_DLGFRAME | WS_VSCROLL | WS_HSCROLL;
    if (Create(lWndStyle, rect, popChart, 1) == 0)
    {
        return FALSE;
    }
    MoveWindow(&rect, TRUE);
	Initialize();

	SetParent ( pomWnd );

	GetParam()->EnableUndo(FALSE);
	LockUpdate(TRUE);
	SetRowCount(1);
	SetColCount(3);
	SetValueRange ( CGXRange(0,1), GetString(IDS_START) );
	SetValueRange ( CGXRange(0,2), GetString(IDS_END) );
	SetValueRange ( CGXRange(0,3), GetString(IDS_VALUE_PCNT) );
	SetEditControls ();
	SetStyleRange ( CGXRange().SetTable(), CGXStyle().SetReadOnly (!bmEditMode) );

	LockUpdate(FALSE);
	GetParam()->EnableUndo(TRUE);
	GetParam()->EnableTrackRowHeight(FALSE);
	popChart->InvalidateRect ( &rect );
	popChart->RedrawWindow();
	return TRUE;
}

void CValuesGrid::DisplayData ( CChartData *popData, bool bpEditMode )
{
	if ( !popData )
		return ;
	bmEditMode = bpEditMode;
	GetParam()->SetLockReadOnly(FALSE);

	SetStyleRange ( CGXRange().SetTable(), CGXStyle().SetReadOnly (TRUE) );
	EnableAutoGrow ( bpEditMode );
	TransferCurrentCell ();
	ResetValues ();

	int		i, ilCount; 
	int		ilStart, ilEnd;
	int		ilStep = bpEditMode ? 0 : popData->imDspStep;
	float	flValue;
	ROWCOL  ilRow = 0;

	ilCount = bpEditMode ? popData->omInputData.GetSize() :
						   popData->omDspData.GetSize();

	SetRowCount ( bpEditMode ? ilCount+1 : ilCount );
	SetEditControls ();

	for ( i=0; i<ilCount; i++ )
	{
		if ( bpEditMode && (popData->omInputData[i].status == DATA_DELETED ) )
			continue;
		ilRow ++;
		if ( bpEditMode  )
		{
			ilStart = popData->omInputData[i].start;
			ilEnd = popData->omInputData[i].end;
			flValue = popData->omInputData[i].val;
			SetItemDataPtr(ilRow, 1, &(popData->omInputData[i]) );
			SetItemDataPtr(ilRow, 2, (void*)DATA_UNCHANGED );
				//  In der 2. Spalte wird Status (DATA_UNCHANGED, DATA_NEW, 
				//	DATA_CHANGED, DATA_DELETED) gespeichert
			if ( !popData->omInputData[i].urno.IsEmpty() )
				SetStyleRange ( CGXRange(ilRow,1,ilRow,3), 
								CGXStyle().SetInterior(SILVER) );
		}
		else
		{
			ilStart = popData->omDspData[i].start;
			ilEnd = ilStart+ilStep;
			flValue = popData->omDspData[i].val;
		}
		SetValueRange ( CGXRange(ilRow, 1), (LONG)ilStart );
		SetValueRange ( CGXRange(ilRow, 2), (LONG)ilEnd );
		SetValueRange ( CGXRange(ilRow, 3), flValue  );
	}
	if ( bpEditMode  )
	{
		//  Weniger als omInputData.GetSize() Datens�tze dargestellt, weil
		//  Datens�tze mit status == DATA_DELETED vorhanden
		if ( ilCount>(int)ilRow )	
			RemoveRows ( ilRow+1, ilCount );
		SetDefaultValues ( ilRow, 1 );
		
		SetItemDataPtr( GetRowCount(), 1, 0 );
		SetItemDataPtr( GetRowCount(), 2, (void*)DATA_UNCHANGED );
	}
	SortTable ( 0, 1);
	SetStyleRange ( CGXRange().SetTable(), CGXStyle().SetReadOnly (!bpEditMode) );
	GetParam()->SetLockReadOnly(TRUE);
}

void CValuesGrid::SetEditControls ()
{
	ROWCOL ilRows = GetRowCount();
	ROWCOL ilCols = GetColCount();

	if ( ( ilRows < 1 ) || ( ilCols < 3 ) )
		return;
	SetStyleRange( CGXRange(1,1,ilRows,2), 
				   CGXStyle().SetControl(GX_IDS_CTRL_EDIT).SetValueType(GX_VT_NUMERIC)
							 .SetFormat(GX_FMT_FIXED).SetPlaces(0).SetValue("") );
	SetStyleRange( CGXRange(1,3,ilRows,3), 
				   CGXStyle().SetControl(GX_IDS_CTRL_EDIT).SetValueType(GX_VT_NUMERIC)
							 .SetFormat(GX_FMT_FIXED).SetPlaces(3).SetValue("")
 				 		     .SetUserAttribute(GX_IDS_UA_VALID_MIN, "0")
							 .SetUserAttribute(GX_IDS_UA_VALID_MAX, "100") );
	SetStyleRange ( CGXRange(1,1,ilRows,3), 
								CGXStyle().SetInterior(WHITE).SetTextColor(BLACK) );
}


BOOL CValuesGrid::OnEndEditing( ROWCOL nRow, ROWCOL nCol) 
{
	BOOL		blRet = CGXGridWnd::OnEndEditing( nRow, nCol) ;
	CString		olNewText;
	CGXControl	* polControl;

	DoAutoGrow ( nRow, nCol) ;

	if ( DeleteEmptyRows ( nRow, nCol ) )
	{
		bmIsDirty = true;
		if ( (nCol==1) || (nCol==3) )
			SetDefaultValues (nRow,3);
		return blRet;
	}	
	if ( ( polControl = GetControl( nRow, nCol) ) &&
		 polControl->GetModify () )
	{
		bmIsDirty = true;
		if ( (long)GetItemDataPtr(nRow, 2) == DATA_UNCHANGED )
			SetItemDataPtr ( nRow, 2, (void*)DATA_CHANGED );
	}
	if ( (nCol==1) || (nCol==3) )
		SetDefaultValues (nRow,nCol);
	return blRet;
}

void CValuesGrid::SetDefaultValues ( ROWCOL nRow, ROWCOL nCol )
{
	int ilRows = GetRowCount ();
	float flSum=0.0, flValue;
	CString olValue;

	if ( !bmEditMode )
		return;
	for ( int i = 1; i<ilRows; i++ )
	{
		olValue = GetValueRowCol ( i, 3 );
		if ( sscanf ( olValue, "%f", &flValue ) > 0 )
		{
			flSum += flValue;
		}
	}
	COLORREF ilColor = flSum>100.0005 ? RGB(255,0,0) : RGB(0,0,0);
	SetStyleRange ( CGXRange(ilRows,3), CGXStyle().SetTextColor(ilColor) );
	flValue = MyRound ( 100-flSum, 3 );
	SetValueRange ( CGXRange(ilRows,3), flValue );
	if ( nCol == 1 )
	{
		olValue = GetValueRowCol ( nRow, nCol );	
		if ( !olValue.IsEmpty() && ( sscanf ( olValue, "%f", &flValue ) > 0 ) )
		{
			olValue = GetValueRowCol ( nRow, 2 );	
			if ( olValue.IsEmpty() && pomWnd )
				SetValueRange ( CGXRange ( nRow,2), 
								flValue+((CProfileChart*)pomWnd)->m_Intervall );
		}
	}
}

bool CValuesGrid::SaveData ( CChartData *popData )
{
	ValInPeriodExt	rlInputValues, *popOldValues;
	long			llStatus ;
	CString			olValue;
	bool			blChanged;
	int				i, ilRows = GetRowCount ();

	for ( i=0; i<popData->omInputData.GetSize(); i++ )
		popData->omInputData[i].newstatus = DATA_DELETED;

	rlInputValues.status = rlInputValues.newstatus = DATA_NEW;
	rlInputValues.urno = "";
	for ( i = 1; i<ilRows; i++ )
	{
		llStatus = (long)GetItemDataPtr(i,2);
		popOldValues = (ValInPeriodExt*)GetItemDataPtr(i,1);
		if ( llStatus == DATA_UNCHANGED )
		{
			if ( popOldValues )		//  unge�nderte Daten --> Status bleibt
				popOldValues->newstatus = DATA_UNCHANGED;	
			continue;
		}
		olValue = GetValueRowCol ( i, 1 );
		if ( olValue.IsEmpty()  || 
			 (sscanf ( olValue, "%d", &(rlInputValues.start) ) <= 0) )
			continue;
		olValue = GetValueRowCol ( i, 2 );
		if ( olValue.IsEmpty()  || 
			 (sscanf ( olValue, "%d", &(rlInputValues.end) ) <= 0) )
			continue;
		olValue = GetValueRowCol ( i, 3 );
		if ( olValue.IsEmpty()  || 
			 (sscanf ( olValue, "%f", &(rlInputValues.val) ) <= 0) )
			continue;
		//  alle 3 Felder sind gef�llt
		if ( ( llStatus == DATA_NEW ) || !popOldValues )
		{	//  neue Daten --> rlInputValues.status = rlInputValues.newstatus = DATA_NEW;
			int idx = popData->omInputData.New ( rlInputValues );
			SetItemDataPtr ( i,1,&(popData->omInputData[idx]) );
		}
		else //  Werte "angeblich" ge�ndert
		{			
			blChanged = ( popOldValues->start != rlInputValues.start ) ||
						( popOldValues->end != rlInputValues.end ) ||
						( fabs(popOldValues->val - rlInputValues.val ) > 0.001 ) ;
			if ( blChanged )
			{
				popOldValues->start = rlInputValues.start;
				popOldValues->end = rlInputValues.end;
				popOldValues->val = rlInputValues.val;
				popOldValues->newstatus = DATA_CHANGED;
			}
		}
		//  Status f�r diese Zeile zur�cksetzen
		SetItemDataPtr(i,2, (void*)DATA_UNCHANGED );  //
	}
	for  ( i=0; i<popData->omInputData.GetSize(); i++ )
		if ( popData->omInputData[i].newstatus == DATA_DELETED )
			popData->omInputData[i].status = DATA_DELETED ;
		else	//  Status bisher DATA_UNCHANGED, nach �nderung DATA_CHANGED
			if ( (popData->omInputData[i].newstatus==DATA_CHANGED) &&
				 (popData->omInputData[i].status==DATA_UNCHANGED) )
				popData->omInputData[i].status = DATA_CHANGED;
	
	SetDirtyFlag ( false );
	bool blRet = popData->CheckInput ();
	blRet &= popData->CalcRawData ();
	blRet &= popData->CalcDspData ();
	popData->SetModified (VTP_CHANGED);
	return blRet;
}

void CValuesGrid::DoAutoGrow ( ROWCOL nRow, ROWCOL nCol) 
{
	CGridControl::DoAutoGrow ( nRow, nCol );	
	ROWCOL ilRowCount = GetRowCount ();
	//  wurde wirklich eine Zeile eingef�gt
	if ( ilRowCount > nRow )			//  damit die von oben kopierten 
	{									//  ItemDataPtr �berschrieben werden
		SetItemDataPtr ( ilRowCount, 1, 0 );
		SetItemDataPtr ( ilRowCount, 2, (void*)DATA_NEW );
		SetStyleRange ( CGXRange(ilRowCount-1,3), CGXStyle().SetTextColor(BLACK) );
	}
}

float MyRound ( float fpVal, UINT ipPlaces )
{
	double flFakt = pow ( 10.0, ipPlaces );
	double flTemp = fpVal * flFakt ;
	if ( flTemp > 0 )
		flTemp += 0.5;
	else
		flTemp -= 0.5;
	flTemp = (float)(int)flTemp ;
	flTemp /= flFakt;
	return (float)flTemp;
}