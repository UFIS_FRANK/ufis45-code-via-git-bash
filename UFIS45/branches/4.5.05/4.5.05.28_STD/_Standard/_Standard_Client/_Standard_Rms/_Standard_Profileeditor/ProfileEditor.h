// ProfileEditor.h : main header file for the PROFILEEDITOR application
//

#if !defined(AFX_PROFILEEDITOR_H__697CCAF4_B2F4_11D3_93B8_00001C033B5D__INCLUDED_)
#define AFX_PROFILEEDITOR_H__697CCAF4_B2F4_11D3_93B8_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include <resource.h>       // main symbols

class CProfileEditorDoc;

/////////////////////////////////////////////////////////////////////////////
// CProfileEditorApp:
// See ProfileEditor.cpp for the implementation of this class
//

class CProfileEditorApp : public CWinApp
{
public:
	CProfileEditorApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProfileEditorApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual CDocument* OpenDocumentFile(LPCTSTR lpszFileName);
	virtual void WinHelp(DWORD dwData, UINT nCmd = HELP_CONTEXT);
	//}}AFX_VIRTUAL

	bool LoadData ();
	void MakeChoiceLists();
	CDocument* FindDocument(LPCTSTR lpszDocTitle );
	CDocTemplate* FindTemplate(LPCTSTR lpszDocName );
	bool IsDocumentPointer (void *pData );
	void OnChangedColors ();

//  Data members
	char pcmConfigPath[_MAX_PATH+1];
	
// Implementation
	//{{AFX_MSG(CProfileEditorApp)
	afx_msg void OnAppAbout();
	afx_msg void OnFileOpen();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	CStatic	m_ClassLibVersion;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	public:
	virtual int DoModal(CProfileEditorDoc*popDoc=0);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
	CProfileEditorDoc *pomAktDocument;

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnAboutProfile();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


extern CProfileEditorApp theApp;

/////////////////////////////////////////////////////////////////////////////

bool ParseReftFieldEntry ( CString &ropReftEntry, CString &ropResTable, 
						   CString &ropResCodeField ) ;
void SetWhereValidString ( CString opTableName, CString &ropWhere );
bool GetNameOfTSRRecord ( CString &ropUrno, CString &ropName );
bool FillNamesArray ( CString &ropUrno, CStringArray &ropNames );
bool GetStringForTSRTextEntry ( CString opEntry, CString &ropString );
void IniGlobalIndices ();

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROFILEEDITOR_H__697CCAF4_B2F4_11D3_93B8_00001C033B5D__INCLUDED_)
