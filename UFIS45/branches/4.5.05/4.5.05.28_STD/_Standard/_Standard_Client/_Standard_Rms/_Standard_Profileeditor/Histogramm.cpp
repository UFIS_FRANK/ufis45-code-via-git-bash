// Histogramm.cpp : implementation file
//

#include <stdafx.h>
#include <math.h>

#include <ccsglobl.h>
#include <ProfileEditor.h>
#include <Histogramm.h>
#include <ProfileChart.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define SCALE_CALC_FACTORS 7
float fgScaleCalcFactors[SCALE_CALC_FACTORS] = 
				{ 1.0, 1.25, 2.0, 2.5, 4.0, 5.0, 10.0 };
int igScaleFactorsNks[SCALE_CALC_FACTORS] = 
				 { 0,  2,    0,   1,   0,   0,   1 };

/////////////////////////////////////////////////////////////////////////////
// CHistogramm

CHistogramm::CHistogramm()
{
	fmUnitsX = 1.0;
	fmUnitsY = 5.0;
	pomParent = 0;
	imColumnWidth = 5;
	omRightBottom.x = 15;
	omRightBottom.y = 0;

//	ilBkColor = SILVER;
//	ilRegularColColor = NAVY;
//	ilMarkedColColor = RED;

	fmScaleStepY = 5;
	imNksY = 1;
	imFirstXVal = -60*24;	 
	imLastXVal=0;
}

CHistogramm::~CHistogramm()
{
	omColumns.DeleteAll();
}


BEGIN_MESSAGE_MAP(CHistogramm, CWnd)
	//{{AFX_MSG_MAP(CHistogramm)
	ON_WM_DESTROY()
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_HSCROLL()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CHistogramm message handlers

void CHistogramm::OnDestroy() 
{
	CWnd::OnDestroy();
	
	// TODO: Add your message handler code here
	
}

BOOL CHistogramm::Open ( CWnd	 *popChart, UINT idc )
{
	BOOL blRet;
	CWnd *polWnd;
	if ( !popChart )
		return FALSE;
	pomParent = popChart;
	
	polWnd = popChart->GetDlgItem ( idc );
	if ( !(polWnd = popChart->GetDlgItem ( idc ) ) )
		return FALSE;
	polWnd->ShowWindow ( SW_HIDE );

    polWnd->GetWindowRect(&omClientArea);
	omUpdateArea = omClientArea;					//	omUpdateArea: komplettes Histogramm
	popChart->ScreenToClient ( &omClientArea );		//  incl. Skalen u. Scrollbar

	DWORD lWndStyle = WS_CHILD | WS_VISIBLE | WS_HSCROLL  ;
	blRet = Create(0, "", lWndStyle, omClientArea, popChart, 1 );
	GetClientRect ( &omClientArea );	//	omClientArea: Histogramm incl. Skalen ohne Scrollbar
	omViewPort = omClientArea;			//	omViewPort: nur Breich der Balken des Histogramms
	omViewPort.bottom -=20;				//              ohne  Skalen u. Scrollbar

	ScreenToClient ( &omUpdateArea );
	return blRet;
}

void CHistogramm::DisplayData ( CChartData *popData, bool bpNeu/*=true*/ )
{
	if ( !popData )
		return ;
	CHistoColumn *polColumn;
	omColumns.DeleteAll();
	
	CalcScaleFaktor ( popData );

	IniScrollBar( popData, bpNeu ) ;

	for ( int i=0; i<popData->omDspData.GetSize(); i++ )
	{
		polColumn = new CHistoColumn ( this, popData->omDspData[i] );
		
		polColumn->CalcWorldRect();
		polColumn->CalcDisplayRect();
		omColumns.Add(polColumn);
	}
	RedrawWindow(&omClientArea);
}


BOOL CHistogramm::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);
}

BOOL CHistogramm::OnEraseBkgnd(CDC* pDC) 
{
	// TODO: Add your message handler code here and/or call default
//	InvalidateRect(&omUpdateArea);
//	RedrawWindow(&omUpdateArea);
	return CWnd::OnEraseBkgnd(pDC);
}

void CHistogramm::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here
	CFont olFont, *polOldFont=0;

	if ( olFont.CreatePointFont (90,"Arial") )
		polOldFont = (CFont*)dc.SelectObject ( &olFont );
	dc.FillSolidRect ( omUpdateArea, igBkColor );
	DrawYAxis (&dc);
	DrawXAxis (&dc);
	for ( int i=0; i<omColumns.GetSize(); i++ )
		omColumns[i].Draw(&dc);
//	ilRegularColColor = NAVY;
//	ilMarkedColColor = RGB(128,128,255);
	if ( polOldFont )
	{
		dc.SelectObject (polOldFont);
		olFont.DeleteObject ();
	}

}

/*
void CHistogramm::InvalidateWindow()
{
	CRect olRect;
	GetWindowRect ( &olRect );
	ScreenToClient ( &olRect );
	InvalidateRect (&olRect,false);
}*/

void CHistogramm::CalcScaleFaktor ( CChartData *popData )
{
	float flMax=(float)0.1;
	int ilPixForMaxVal = omViewPort.Height()*9/10;
	for ( int i=0; i<popData->omDspData.GetSize(); i++ )
	{
		flMax = max ( popData->omDspData[i].val, flMax );
	}
	fmUnitsY = (float)ilPixForMaxVal / flMax;	
	fmScaleStepY = FindScaleStep ( flMax, 4, imNksY );
}

void CHistogramm:: DrawYAxis (CDC* pDC)
{
	int		i=0;
	int     ilYPos;
	float   flValue;
	int     ilTextOffSet, ilTextCxMax=0;
	CString olOut;
	CString olForm;
	olForm.Format ( "%%.%df %%%%", imNksY );
	ilTextOffSet = pDC->GetTextExtent ( "1.0").cy / 2;
	do 
	{
		flValue = ++i * fmScaleStepY;
		ilYPos = omViewPort.bottom -(int)(flValue*fmUnitsY) - omRightBottom.y;
		if ( ilYPos-ilTextOffSet < omViewPort.top )
			break;
		pDC->MoveTo ( omClientArea.left, ilYPos );
		pDC->LineTo ( omClientArea.right, ilYPos );
		olOut.Format ( olForm, flValue );
		pDC->TextOut ( omClientArea.left, ilYPos-ilTextOffSet, olOut ) ;
		ilTextCxMax = max ( ilTextCxMax, pDC->GetTextExtent ( olOut ).cx );
	}while (true); 
	omViewPort.left = omClientArea.left + ilTextCxMax +1 ;
}

//  FindScaleStep :
//	Suche optimale Achseneinteilung f�r einen Werte-Bereich 
//  IN: fpRange: Werte-Bereich 
//		ipSteps: gew�nschte Anzahl von Unterteilungen (circa-Wert)
float FindScaleStep ( float fpRange, int ipSteps, int &ipNks )
{
	if ( ipSteps < 1 )
		return fpRange;
	float flStepWidth = (float)fabs(fpRange)/ipSteps;
	return FindScaleStep4Value ( flStepWidth, 1.0, ipNks );
}

float FindScaleStep4Value ( float fpValue, float fpMuliplyBy, int &ipNks )
{
	int		ilIdxMin=0;
	float	flMinDist=10, flDist, flRet;

	fpValue  = (float)fabs(fpValue);
	//  Abbruchkriterium
	if ( (fpValue <= fgScaleCalcFactors[SCALE_CALC_FACTORS-1]) &&
		 (fpValue >= fgScaleCalcFactors[0]) )
	{
		for ( int i=0; i<SCALE_CALC_FACTORS; i++ )
		{
			flDist = (float)fabs(fpValue-fgScaleCalcFactors[i]);
			if ( flDist < flMinDist )
			{
				ilIdxMin = i;
				flMinDist = flDist;
			}
		}
		flRet = fgScaleCalcFactors[ilIdxMin];
		flRet *= fpMuliplyBy;
		ipNks = igScaleFactorsNks[ilIdxMin] - Rnd((float)log10(fpMuliplyBy));
		ipNks = max ( 0, ipNks );
		return flRet;
	}
	if ( fpValue < fgScaleCalcFactors[0] )
		 return FindScaleStep4Value ( fpValue*10, fpMuliplyBy/10, ipNks );
	else
		 return FindScaleStep4Value ( fpValue/10, fpMuliplyBy*10, ipNks );
}

int Rnd ( float fpVal )
{
	if ( fpVal > 0 )
		fpVal += 0.5;
	else
		fpVal -= 0.5;
	return int(fpVal);
}

float Rnd ( float fpVal, int nks )
{
	float fpMult = (float)pow(10.0, nks );
	fpVal *= fpMult;
	if ( fpVal > 0 )
		fpVal += 0.5;
	else
		fpVal -= 0.5;
	fpVal = (float)(int)fpVal;
	fpVal /= fpMult;
	return fpVal;
}

void CHistogramm::DrawXAxis (CDC* pDC)
{
	int yPos=omViewPort.bottom, xPos;
	CString olOut;
	int ilTextWidth, ilLastX=omViewPort.right+100;
	int ilMinute = 0, ilRightX;

	pDC->MoveTo ( omViewPort.left, omViewPort.bottom );
	pDC->LineTo ( omViewPort.right, omViewPort.bottom );
	do
	{
		xPos = GetXClient(ilMinute);
		if (xPos < omViewPort.left ) 
			break;
		pDC->MoveTo ( xPos, yPos );
		pDC->LineTo ( xPos, yPos+5 );
		if ( !ilMinute )
			olOut = "STD";
		else
			olOut.Format ( "%d", ilMinute );
		ilTextWidth = pDC->GetTextExtent ( olOut ).cx;
		if ( xPos+ilTextWidth/2<ilLastX)
		{
			pDC->MoveTo ( xPos+1, yPos );
			pDC->LineTo ( xPos+1, yPos+5 );
			pDC->MoveTo ( xPos, yPos );
			pDC->LineTo ( xPos, yPos+5 );

			ilLastX = xPos-ilTextWidth/2;
			pDC->TextOut ( ilLastX, yPos+5, olOut ); 
		}
		if ( ilMinute == 0 )	//  Position, an der "STD" aufh�rt, merken
			ilRightX = xPos+ilTextWidth/2;
		ilMinute -= imColumnWidth;
		
	}while(true);
	ilMinute = 0;
	for ( ilMinute = imColumnWidth; ilMinute<=imLastXVal; 
		  ilMinute+=imColumnWidth )
	{
		xPos = GetXClient(ilMinute);
		if (xPos > omViewPort.right ) 
			break;

		pDC->MoveTo ( xPos, yPos );
		pDC->LineTo ( xPos, yPos+5 );
		olOut.Format ( "%d", ilMinute );
		ilTextWidth = pDC->GetTextExtent ( olOut ).cx;
		if ( (xPos-ilTextWidth/2>ilRightX) &&
			 (xPos+ilTextWidth/2<=omViewPort.right) )
		{
			pDC->MoveTo ( xPos+1, yPos );
			pDC->LineTo ( xPos+1, yPos+5 );
			pDC->MoveTo ( xPos, yPos );
			pDC->LineTo ( xPos, yPos+5 );

			xPos -= ilTextWidth/2;
			pDC->TextOut ( xPos, yPos+5, olOut ); 
			ilRightX = xPos+ilTextWidth;
		}

	}
}

int CHistogramm::GetXClient ( int ipMinute )
{
	int ilXPos = (int)((float)ipMinute*fmUnitsX);
	ilXPos -=  (int)((float)omRightBottom.x*fmUnitsX);
	ilXPos += omViewPort.BottomRight().x;
	return ilXPos;
}

int CHistogramm::GetYClient ( float fpPercent )
{
	int ilYPos = -(int)(fpPercent*fmUnitsY);
	ilYPos -= omRightBottom.y;
	ilYPos += omViewPort.BottomRight().y;
	return ilYPos;
}
	

void CHistogramm::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// TODO: Add your message handler code here and/or call default
	int ilMinPos, ilMaxPos, ilIncr, ilLastPos;
	GetScrollRange( SB_HORZ, &ilMinPos, &ilMaxPos );
	ilIncr = (ilMaxPos - ilMinPos ) / 20;
	int ilActPos = ilLastPos = GetScrollPos( SB_HORZ );

	switch (nSBCode)
	{
		case SB_LEFT:			//  Scroll to far left.
			ilActPos = ilLastPos; 
			break;

		case SB_ENDSCROLL:		//  End scroll.
			break;

		case SB_LINELEFT:		// Scroll left. 
			ilActPos -= ilIncr;
			break;

		case SB_LINERIGHT:		//  Scroll right.
			ilActPos += ilIncr;
			break;

		case SB_PAGELEFT:		//  Scroll one page left.
			ilActPos -= 5*ilIncr;
			break;

		case SB_PAGERIGHT:		//  Scroll one page right.
			ilActPos += 5*ilIncr;
			break;

		case SB_RIGHT:			//  Scroll to far right.
			ilActPos = ilLastPos ;
			break;

		case SB_THUMBPOSITION:	//  Scroll to absolute position.
			ilActPos = nPos ;
			break;

		case SB_THUMBTRACK:		//   Drag scroll box to specified position. 
			ilActPos = nPos ;
			break;

	}
	ilActPos = min ( ilActPos, ilMaxPos );
	ilActPos = max ( ilActPos, ilMinPos );
	SetScrollPos( SB_HORZ, ilActPos );

	CWnd::OnHScroll(nSBCode, nPos, pScrollBar);
	if ( ilLastPos != ilActPos )
	{
		omRightBottom.x = imLastXVal + ilActPos; 
		for ( int i=0; i<omColumns.GetSize(); i++ )
			omColumns[i].CalcDisplayRect();
		//InvalidateRect ( &omViewPort );		
		RedrawWindow(&omClientArea);
	}
}


void CHistogramm::IniScrollBar( CChartData *popData, bool bpReset/*=false*/ ) 
{
	int ilSize, ilActPos =0;
	int ilViewPortSize=0;	//  in Minuten
	int ilValueRange=0;	//  in Minuten
	SCROLLINFO rlInfo; 
	rlInfo.cbSize = sizeof (SCROLLINFO);
	
	ilSize = popData->omDspData.GetSize();
	imFirstXVal = 0;
	imLastXVal = 0;
	for ( int i=0; i<ilSize; i++ )
	{
		imFirstXVal = min ( imFirstXVal, popData->omDspData[i].start );
		imLastXVal = max ( imLastXVal, popData->omDspData[i].start + popData->imDspStep );
	}
	imLastXVal += 15;
	imFirstXVal -= 15;

	ilViewPortSize = (int)((float)omViewPort.Width()/fmUnitsX);
	ilValueRange = imLastXVal-imFirstXVal;
	if ( bpReset )
		omRightBottom.x = imLastXVal;
	else
		ilActPos = omRightBottom.x - imLastXVal; 
		

	if ( ( ilValueRange > ilViewPortSize ) || ( omRightBottom.x < imLastXVal ) )
	{	//  Zeitspanne, f�r die Daten da sind, pa�t nicht ins Fenster
		EnableScrollBar( SB_HORZ, ESB_ENABLE_BOTH );
		SetScrollRange( SB_HORZ, -(ilValueRange-ilViewPortSize), 0, FALSE );

		SetScrollPos( SB_HORZ, ilActPos );
	}
	else
		EnableScrollBar( SB_HORZ, ESB_DISABLE_BOTH );

}


