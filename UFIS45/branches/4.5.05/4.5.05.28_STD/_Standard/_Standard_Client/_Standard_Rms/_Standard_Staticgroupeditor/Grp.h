// Grp.h : main header file for the GRP application
//

#if !defined(AFX_CCSPRJ_H__FC3BA6F7_1AED_11D1_82C3_0080AD1DC701__INCLUDED_)
#define AFX_CCSPRJ_H__FC3BA6F7_1AED_11D1_82C3_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
#include "GroupsDialog.h"	// Added by ClassView


class CHelperWnd;

/////////////////////////////////////////////////////////////////////////////
// CGrpApp:
// See Grp.cpp for the implementation of this class
//

class CGrpApp : public CWinApp
{
public:
	CGrpApp();
	~CGrpApp();

    void InitialLoad(CWnd *pParent);

	virtual BOOL ProcessMessageFilter(int code, LPMSG lpMsg);
	HWND m_hwndDialog;

private:
	int ExtractItemList(CString opSubString, CStringArray *popStrArray, char cpTrenner);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGrpApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CGrpApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CGroupsDialog dlg;
	CHelperWnd *polHelperWindow;
public:
	BOOL bIntern;		//*** CALLED FROM ANOTHER APP ***
};

extern CGrpApp theApp;

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CCSPRJ_H__FC3BA6F7_1AED_11D1_82C3_0080AD1DC701__INCLUDED_)
