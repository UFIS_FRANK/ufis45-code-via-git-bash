/////////////////////////////////////////////////////////////////////////////////////
// AboutBox - Implementation File

#include <stdafx.h>
#include <VersionInfo.h>
#include <AboutBox.h>
#include <ccsglobl.h>
#include <LibGlobl.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//-------------------------------------------------------------------------------------------------------------------
//					construction
//-------------------------------------------------------------------------------------------------------------------
CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT

}

//-------------------------------------------------------------------------------------------------------------------
//					data exchange, message map
//-------------------------------------------------------------------------------------------------------------------
void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	DDX_Control(pDX, IDC_CLASSLIBVERSION, m_ClassLibVersion);
	DDX_Control(pDX, IDC_VERSION, m_Version);
	//}}AFX_DATA_MAP
}
//-------------------------------------------------------------------------------------------------------------------

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



//-------------------------------------------------------------------------------------------------------------------
//					message handlers
//-------------------------------------------------------------------------------------------------------------------
BOOL CAboutDlg::OnInitDialog() 
{
	CCS_TRY

	CDialog::OnInitDialog();
	SetStaticTexts();

	CCS_CATCH_ALL
	return TRUE;
}



//-------------------------------------------------------------------------------------------------------------------
//					helper functions
//-------------------------------------------------------------------------------------------------------------------
void CAboutDlg::SetStaticTexts()
{
	CWnd *pWnd = NULL;

	// caption
	SetWindowLangText ( this, IDS_ABOUT_TITLE );

	// watch out: The date in the line below will only be renewed when _this_ file is compiled
	CString olVersion, olForm;
	VersionInfo olInfo;
	
	VersionInfo::GetVersionInfo(NULL,olInfo);
	olForm.LoadString(129);
	olVersion.Format ( olForm, olInfo.omFileVersion);
	olVersion += CString(__DATE__);  

	m_Version.SetWindowText(olVersion);	
	pWnd = GetDlgItem(IDC_COPYRIGHT);
	pWnd->SetWindowText(GetString(1485/*"COPYRIGHT_MSG"*/));
	
	// buttons
	pWnd = GetDlgItem(IDOK);
	pWnd->SetWindowText(GetString(1398/*"OK"*/));

	m_ClassLibVersion.SetWindowText(CString(pcgClassLibVersion));
	
}
//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
