// CCSErr.cpp: implementation of the CCSErr class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <poolalloc.h>
#include <CCSErr.h>
#include <ostream.h>


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif




const CCSReturnCode RCSuccess( 0, "Success");
const CCSReturnCode RCFailure(-1, "Failure");


//-------------------------------------------------------------------------------------------------
//					Construction
//-------------------------------------------------------------------------------------------------
CCSReturnCode::CCSReturnCode(int ipCode, const char *pcpName)
 : imCode(ipCode), pcmName(pcpName)
{
}



//-------------------------------------------------------------------------------------------------
//					Implementation
//-------------------------------------------------------------------------------------------------
CCSReturnCode::operator int() const
{
  return imCode;
}
//-------------------------------------------------------------------------------------------------

CCSReturnCode& CCSReturnCode::operator=(const CCSReturnCode& ropOther)
{
  imCode =  ropOther.imCode;
  pcmName = ropOther.pcmName;
  return *this;
}
//-------------------------------------------------------------------------------------------------

BOOL CCSReturnCode::operator==(const CCSReturnCode& ropOther) const
{
  return imCode == ropOther.imCode;
}
//-------------------------------------------------------------------------------------------------

ostream& operator<<(ostream& ropOs, const CCSReturnCode& ropRc)
{
  if (ropRc.pcmName)
    ropOs << "[" << ropRc.pcmName << "]";
  else
    ropOs << ropRc.imCode;
  return ropOs;
}
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------