// RespTabViewer.h: interface for the RespTabViewer class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RESPTABVIEWER_H__BCEAC384_0B8F_11D4_A712_00010204AA63__INCLUDED_)
#define AFX_RESPTABVIEWER_H__BCEAC384_0B8F_11D4_A712_00010204AA63__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include <RespTabViewer.h>
#include <CedaBasicData.h>
#include <ccsptrarray.h>
//#include "cviewer.h"
#include <table.h>
#include <RecordSet.h>


struct RESPTABLE_LINEDATA
{
	CString AloText;	// ALO.ALOD
	CString AloUrno;
	CString MemberText;
	CString SgmReft;
	CString SgmUval;
	CString SgmUrno;
};



class RespTabViewer/*:public CViewer*/
{
public:
	RespTabViewer();
	~RespTabViewer();


// Implementation
	void Attach(Table *popAttachWnd);
    void ChangeViewTo(CString opSgrUrno);
    
	void MakeLines();
	void MakeLine(RecordSet *popSgm);
	int CreateLine(RESPTABLE_LINEDATA *prpLine);

	BOOL FindLine(CString opUrno, int &ripLineno);
	RESPTABLE_LINEDATA *GetLine(int ipLineno);

	int GetLineCount();

	void ClearAll();
	
	void AddLine(CString opAloText, CString opAloUrno, CString opReft, CString opGrpMem, CString opUval);
	void DeleteLine(int ipLineno);
	void UpdateDisplay();

	CString Format(RESPTABLE_LINEDATA *prpLine);
	int CompareLine(RESPTABLE_LINEDATA *prpLine1, RESPTABLE_LINEDATA *prpLine2);
	
	void ProcessAloChange(RecordSet *pAlo);
	void ProcessAloDelete(RecordSet *pAlo);

// attributes
	CCSPtrArray<RESPTABLE_LINEDATA> omLines;
	Table *pomTable;
	

// Brauche ich die ???
	CTime omStartTime;
	CTime omEndTime;
	CString omDay;

	CString omCurrSgr;

};

#endif // !defined(AFX_RESPTABVIEWER_H__BCEAC384_0B8F_11D4_A712_00010204AA63__INCLUDED_)
