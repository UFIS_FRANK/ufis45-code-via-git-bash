// basicdat.cpp CBasicData class for providing general used methods

#include <stdafx.h>
#include <BasicData.h>
#include <CedaBasicData.h>
#include <RecordSet.h>



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



bool DateStringToDBString(CString opDateString, CString &opDBString)
{
	if (opDateString.GetLength() == 8)
	{
		CString opDBString = opDateString.Right(2);
		if (atoi(opDBString) < 90)
		{
			opDBString = CString("19") + opDBString;
		}
		else
		{
			opDBString = CString("20") + opDBString;
		}
	}
	else if (opDateString.GetLength() == 10)
	{
		CString opDBString = opDateString.Right(4);
	}
	else
	{
		return false;
	}

	opDBString += opDateString.Mid(4, 2);
	opDBString += opDateString.Left(2);

	return true;
}

CString DBStringToDate(CString opDBString)
{
	CString olTmp, olDate;
	olTmp = opDBString.Left(8);
	
	olDate.Format("%s.%s.%s", olTmp.Right(2), olTmp.Mid(4,2), olTmp.Left(4));

	return olDate;
}

// String mit Zeichen bis zur L�nge ipLength auff�llen
int FillRightByChar( CString &opStr, char cpSign, int ipLength )
{
	// hhb 7/98, return : Anzahl neue Zeichen
	int ipAnhang = ipLength - opStr.GetLength() ;
	
	if( ipAnhang > 0 )
	{
		opStr += CString( cpSign, ipAnhang ) ; 
		return( ipAnhang ) ;
	}
	else
		return( 0 ) ;

}



// Linken Anteil von String besorgen   
int CutLeft(CString & ropLeft, CString & ropLeftover, const char * OneOfSep)
{
	// liefert alle Zeichen von ropLeft bis 1. Auftreten von einem Zeichen aus "OneOfSep"
	// in ropLeft zur�ck, den Rest von ropLeft in ropLeftover
	// der Separator wird entfernt

	int ilSepPos = ropLeft.FindOneOf( OneOfSep ) ;	

	if( ilSepPos < 0 ) 
	{
		// kein Separator => alles in ropLeft !
		ropLeftover.Empty() ;
	}
	else
	{
		ropLeftover = ropLeft.Right( ropLeft.GetLength() - ilSepPos - 1 ) ;
		ropLeft = ropLeft.Left( ilSepPos ) ;
	}
	return( ropLeftover.GetLength() ) ;

}


int CutLeft(CString & ropLeft, CString & ropLeftover, int ipNo)
{
	// liefert "ipNo" Zeichen von ropLeft in "ropLeft" zur�ck, 
	// den Rest von ropLeft in "ropLeftover"

	ropLeftover = ropLeft.Right( ropLeft.GetLength() - ipNo ) ;
	ropLeft = ropLeft.Left( ipNo ) ;
	return( ropLeftover.GetLength() ) ;
}


// ......... String( dd.mm.yyyy-dd.mm.yyyy ) in Kurzform aufbereiten
void ShortRangeString( CString &ropStr )
{
	CString olFrom = ropStr.Left(10) ; 
	CString olTo = ropStr.Mid(11,10) ;

	if( ropStr.GetLength() == cimFullRangeLen )
	{
	
		if( olFrom == olTo ) ropStr = olFrom ;	// Eintagsflieger (dd.mm.yy)
		else
		{
			if( olFrom.Right(4) == olTo.Right(4) ) // gleiches Jahr (dd.mm.-dd.mm.yy)
			{
				ropStr = olFrom.Left(6)+"-"+olTo.Left(6)+olFrom.Right(4) ;
			}
			else	//  (dd.mm.yy-dd.mm.yy)
			{
				char pslShortJahr[4+1] ;
				strcpy( pslShortJahr, olFrom.Right(4) ) ;
				int ilShortJahr = atoi(pslShortJahr) % 100 ;
				itoa(ilShortJahr, pslShortJahr, 10 ) ;
				ropStr = olFrom.Left(6)+pslShortJahr+CString("-") ;
				strcpy( pslShortJahr, olTo.Right(4) ) ;
				ilShortJahr = atoi(pslShortJahr) % 100 ;
				itoa(ilShortJahr, pslShortJahr, 10 ) ;
				ropStr += olTo.Left(6)+pslShortJahr ;
			}
		}
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


bool GetListItemByField(CString &opDataList, CString &opFieldList, CString &opField, CString &opData)
{
	CStringArray olFields;
	CStringArray olData;
	int ilFields = ExtractItemList(opFieldList, &olFields);
	int ilData   = ExtractItemList(opDataList, &olData);

	for( int i = 0; i < ilFields; i++)
	{
		if(olFields[i] == opField)
		{
			if(ilData > i)
				opData = olData[i];
				return true; 
		}
	}
	return false;
}

bool GetListItemByField(CString &opDataList, CString &opFieldList, CString &opField, CTime &opData)
{
	CStringArray olFields;
	CStringArray olData;
	int ilFields = ExtractItemList(opFieldList, &olFields);
	int ilData   = ExtractItemList(opDataList, &olData);

	for( int i = 0; i < ilFields; i++)
	{
		if(olFields[i] == opField)
		{
			if(ilData > i)
				opData = DBStringToDateTime(olData[i]);
				return true; 
		}
	}
	return false;
}



void TraceFieldAndData(CString &opFieldList,CString &opListOfData, CString opMessage)
{

	CStringArray olFields;
	CStringArray olData;
	int ilFields = ExtractItemList(opFieldList, &olFields);
	int ilData   = ExtractItemList(opListOfData, &olData);

	TRACE("\n---------------------------------------------------------------------------------------------------------------------");
	TRACE("\n-- %s --- Fields:<%d>   Data:<%d>", opMessage, ilFields, ilData);


	if(ilFields == ilData)
	{
		for( int i = 0; i < ilData; i++)
		{
			TRACE("\n<%s> <%d> <%s>", olFields[i], olData[i].GetLength(), olData[i]);
		}
	}


	TRACE("\n---------------------------------------------------------------------------------------------------------------------");
}


int GetItemCount(CString olList, char cpTrenner  )
{
	CStringArray olStrArray;
	int ilItemCount = ExtractItemList(olList,&olStrArray,cpTrenner);

	return ilItemCount;
}


int ExtractItemList(CString opSubString, CStringArray *popStrArray, char cpTrenner)
{

	CString olText;

	popStrArray->RemoveAll();

	BOOL blEnd = FALSE;
	
	if(!opSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		while(blEnd == FALSE)
		{
			pos = opSubString.Find(cpTrenner);
			if(pos == -1)
			{
				blEnd = TRUE;
				olText = opSubString;
			}
			else
			{
				olText = opSubString.Mid(0, opSubString.Find(cpTrenner));
				opSubString = opSubString.Mid(opSubString.Find(cpTrenner)+1, opSubString.GetLength( )-opSubString.Find(cpTrenner)+1);
			}
			popStrArray->Add(olText);
		}
	}
	return	popStrArray->GetSize();

}




int SplitItemList(CString opString, CStringArray *popStrArray, int ipMaxItem, char cpTrenner)
{

	CString olSubStr;

	popStrArray->RemoveAll();
	
	CStringArray olStrArray;

	int ilCount = ExtractItemList(opString, &olStrArray);
	int ilSubCount = 0;

	for(int i = 0; i < ilCount; i++)
	{
		if(ilSubCount >= ipMaxItem)
		{
			if(!olSubStr.IsEmpty())
				olSubStr = olSubStr.Right(olSubStr.GetLength() - 1);
			popStrArray->Add(olSubStr);
			ilSubCount = 0;
			olSubStr = "";
		}
		ilSubCount++;
		olSubStr = olSubStr + cpTrenner + olStrArray[i];
	}
	if(!olSubStr.IsEmpty())
	{
		olSubStr = olSubStr.Right(olSubStr.GetLength() - 1);
		popStrArray->Add(olSubStr);
	}
	
	return	popStrArray->GetSize();
}






CString GetListItem(CString &opList, int ipPos, bool bpCut, char cpTrenner )
{
	CStringArray olStrArray;
	int ilAnz = ExtractItemList(opList, &olStrArray, cpTrenner);
	CString olReturn;

	if(ipPos == -1)
		ipPos = ilAnz;

	if((ipPos <= ilAnz) && (ipPos > 0))
		olReturn = olStrArray[ipPos - 1];
	if(bpCut)
	{
		opList = "";
		for(int ilLc = 0; ilLc < ilAnz; ilLc++)
		{
			if(ilLc != (ipPos - 1))
			{
				opList = opList + olStrArray[ilLc] + cpTrenner;
			}
		}
	}
	if(bpCut)
		opList = opList.Left(opList.GetLength() - 1);
	return olReturn;
}



CString DeleteListItem(CString &opList, CString olItem)
{
	CStringArray olStrArray;
	int ilAnz = ExtractItemList(opList, &olStrArray);
	opList = "";
	for(int ilLc = 0; ilLc < ilAnz; ilLc++)
	{
			if(olStrArray[ilLc] != olItem)
				opList = opList + olStrArray[ilLc] + ",";
	}
	opList = opList.Left(opList.GetLength() - 1);
	return opList;
}


CBasicData::CBasicData(void)
{
	char pclTmpText[512];
	char pclConfigPath[512];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	imNextOrder = 4711;
	*pcmCedaCmd = '\0';


	omDiaStartTime = CTime::GetCurrentTime();
	omDiaStartTime -= CTimeSpan(0, 1, 0, 0);
	omDiaEndTime = omDiaStartTime + CTimeSpan(0, 6, 0, 0);
	

	// now reading the ceda commands
	char pclComandBuf[24];
	omDefaultComands.Empty();
	omActualComands.Empty();
 
    GetPrivateProfileString(ogAppName, "EIOHDL", "LLF",
		pclTmpText, sizeof pclTmpText, pclConfigPath);
	sprintf(pclComandBuf," %3s ","LLF");
	omDefaultComands += pclComandBuf;
	sprintf(pclComandBuf," %3s ",pclTmpText);
	omActualComands += pclComandBuf;
}



CBasicData::~CBasicData(void)
{
	for (int i = ogRef.GetSize() - 1; i >= 0; i--)
	{
		ogRef[i].ValUrnoArr.DeleteAll();
	}

	ogRef.DeleteAll();
}


long CBasicData::GetNextUrno(void)
{
	long llUrno;

	//-- exception handling
	CCS_TRY
	
	if(omNewUrnos.GetSize() == 0)
	{
		GetManyUrnos(500, omNewUrnos);
	}

	llUrno = omNewUrnos[0];
	omNewUrnos.RemoveAt(0);

	//-- exception handling
	CCS_CATCH_ALL

	return llUrno;
}



bool CBasicData::GetManyUrnos(int ipAnz, CUIntArray &opUrnos)
{
	bool	blRc = false;

	//-- exception handling
	CCS_TRY
	
	char 	pclTmpDataBuf[10000];

	sprintf(pclTmpDataBuf, "%d", ipAnz);

	blRc = CedaAction("GMU", "", "", pclTmpDataBuf);

	if (blRc == true)
	{
		for ( int ilItemNo=1; (ilItemNo <= ipAnz) ; ilItemNo++ )
		{
			char pclTmpBuf[64];

			GetItem(ilItemNo, pclTmpDataBuf, pclTmpBuf);

			long llNewUrno = atol(pclTmpBuf);

			opUrnos.Add(llNewUrno);
		}
	}

	//-- exception handling
	CCS_CATCH_ALL

	return blRc;
}




int CBasicData::GetNextOrderNo()
{
	imNextOrder++;

	return (imNextOrder);
}


char *CBasicData::GetCedaCommand(CString opCmdType)
{ 
	//-- exception handling
	CCS_TRY

	int ilIndex;

	if ((ilIndex = omDefaultComands.Find(opCmdType)) != -1)
	{
		strcpy(pcmCedaComand,omActualComands.Mid(ilIndex,3));
	}
	else
	{
		strcpy(pcmCedaComand,opCmdType);
	}

	//-- exception handling
	CCS_CATCH_ALL

	return pcmCedaComand;
}


void CBasicData::GetDiagramStartTime(CTime &opStart, CTime &opEnd)
{
	opStart = omDiaStartTime;
	opEnd = omDiaEndTime;
	return;
}


void CBasicData::SetDiagramStartTime(CTime opDiagramStartTime, CTime opDiagramEndTime)
{
	omDiaStartTime = opDiagramStartTime;
	omDiaEndTime = opDiagramEndTime;
}


CString CBasicData::GetWorkstationName()
{
	return omWorkstationName;
}



void CBasicData::SetLocalDiff()
{
	//-- exception handling
	CCS_TRY

	int		ilTdi1;
	int		ilTdi2;
	int		ilMin;
	int		ilHour;
	CTime	olTich ;	
	CTime olCurr;
	RecordSet *prlRecord;	

	CCSPtrArray<RecordSet> olData;

	CString olWhere = CString("WHERE APC3 = '") + CString(pcgHome) + CString("'");

	if(ogBCD.ReadSpecial( "APT", "TICH,TDI1,TDI2", olWhere, olData))
	{
		if(olData.GetSize() > 0)
		{
			prlRecord = &olData[0];
		
			olTich = DBStringToDateTime((*prlRecord)[0]);
			omTich = olTich;
			ilTdi1 = atoi((*prlRecord)[1]);
			ilTdi2 = atoi((*prlRecord)[2]);
			
			ilHour = ilTdi1 / 60;
			ilMin  = ilTdi1 % 60;
			
			CTimeSpan	olTdi1(0,ilHour ,ilMin ,0);

			ilHour = ilTdi2 / 60;
			ilMin  = ilTdi2 % 60;
			
			CTimeSpan	olTdi2(0,ilHour ,ilMin ,0);

			olCurr = CTime::GetCurrentTime();

			if(olTich != TIMENULL) 
			{
				omLocalDiff1 = olTdi1;
				omLocalDiff2 = olTdi2;
			}
		}
	}
	olData.DeleteAll();

	//-- exception handling
	CCS_CATCH_ALL
}

// the same functions are in CCSCedaData !!!!
void CBasicData::LocalToUtc(CTime &opTime)
{
	//-- exception handling
	CCS_TRY

	if((opTime != TIMENULL) && (omTich != TIMENULL))
	{
		if(opTime < omTich)
		{
			opTime -= omLocalDiff1;
		}
		else
		{
			opTime -= omLocalDiff2;
		}
	}

	//-- exception handling
	CCS_CATCH_ALL
}

//--------------------------------------------------------------------------------------------------------------------
void CBasicData::UtcToLocal(CTime &opTime)
{
	//-- exception handling
	CCS_TRY

	CTime olTichUtc;
	olTichUtc = omTich - CTimeSpan(omLocalDiff2);



	if((opTime != TIMENULL) && (omTich != TIMENULL))
	{
		if(opTime < olTichUtc)
		{
			opTime += omLocalDiff1;
		}
		else
		{
			CString olFromDate;
			olFromDate.Format("%04d%02d%02d%02d%02d", omTich.GetYear(), omTich.GetMonth(), omTich.GetDay(), omTich.GetHour()-2, omTich.GetMinute());
			CString olToDate;
			olToDate.Format("%04d%02d%02d%02d%02d", omTich.GetYear(), omTich.GetMonth(), omTich.GetDay(), omTich.GetHour()-1, omTich.GetMinute());
			CString olOpDate = opTime.Format("%Y%m%d%H%M");
			if(olOpDate >= olFromDate && olOpDate <= olToDate)
			{
				opTime += omLocalDiff1;
			}
			else
			{
				opTime += omLocalDiff2;
			}
		}
	}

	//-- exception handling
	CCS_CATCH_ALL
}
//--------------------------------------------------------------------------------------------------------------------


void GetTimeSpanFromMinutes(const int ipMinutes, CTimeSpan &ropTimeSpan, bool &rbpNegative)
{
	if (ipMinutes >= 0)
	{
		rbpNegative = false;
	}
	else
	{
		rbpNegative = true;		
	}
	
	int ilTmp = abs(ipMinutes);
	int ilSeconds = 0;
	int ilMinutes = 0;
	int ilHours = 0;
	int ilDays = 0;
	
	if (ilTmp > 59)
	{
		ilMinutes = ilTmp % 60;
		ilHours = (int) ilTmp / 60;
		if (ilHours > 24)
		{
			ilDays = (int) ilHours % 24;
			ilHours = ilHours % 24;
		}
	}
	else
	{
		ilMinutes = ilTmp;
	}

	ropTimeSpan = CTimeSpan(ilDays, ilHours, ilMinutes, ilSeconds);
}
//--------------------------------------------------------------------------------------------------------------------

void GetTimeSpanFromSeconds(const int ipSeconds, CTimeSpan &ropTimeSpan, bool &rbpNegative)
{
	// save plus/minus flag
	if (ipSeconds >= 0)
	{
		rbpNegative = false;
	}
	else
	{
		rbpNegative = true;		
	}
	
	int ilTmp = abs(ipSeconds);
	int ilSeconds = ilTmp % 60;
	int ilMinutes = 0;
	int ilHours = 0;
	int ilDays = 0;
	
	// round up and convert to minutes
	ilTmp = (int) (ilTmp / 60);

	// get minutes, hours, days
	if (ilTmp > 59)
	{
		ilMinutes = ilTmp % 60;
		ilHours = (int) ilTmp / 60;
		if (ilHours > 24)
		{
			ilDays = (int) ilHours % 24;
			ilHours = ilHours % 24;
		}
	}
	else
	{
		ilMinutes = ilTmp;
	}
	
	// build return value
	ropTimeSpan = CTimeSpan(ilDays, ilHours, ilMinutes, ilSeconds);
}
//--------------------------------------------------------------------------------------------------------------------

bool CBasicData::MakeChoiceList(CCSPtrArray <RecordSet> *polArr, CString opObject, CString opField, CString &opChoiceList)
{
	opChoiceList = "";
	bool blFound = false;
	int ilCount = polArr->GetSize();
	int ilIdx = ogBCD.GetFieldIndex(opObject, opField);
	int ilUrnoIdx = ogBCD.GetFieldIndex(opObject, "URNO");
	CString olTmp1, olTmp2, olUrno;

	if (ilIdx != -1)
	{
		for (int i = 0; i < ilCount; i++)
		{
			opChoiceList += (*polArr)[i].Values[ilIdx];
			opChoiceList += CString("\n");
			
			// add entry to reference struct
			blFound = false;
			int ilRefCount = ogRef.GetSize();
			for (int j = 0; j < ilRefCount; j++)
			{
				if (ogRef[j].Table == opObject)
				{
					blFound = true;
					{
						VALUE_URNO rlValUrno;
						rlValUrno.Urno = (*polArr)[i].Values[ilUrnoIdx];
						rlValUrno.Value = (*polArr)[i].Values[ilIdx];
						rlValUrno.Field = opField;

						ogRef[j].ValUrnoArr.New(rlValUrno);
					}
				}
			}

			if (blFound == false)
			{
				VALUE_URNO rlValUrno;
				rlValUrno.Urno = (*polArr)[i].Values[ilUrnoIdx];
				rlValUrno.Value = (*polArr)[i].Values[ilIdx];
				
				REFTAB rlRefTab;
				rlRefTab.Table = opObject;
				rlRefTab.ValUrnoArr.New(rlValUrno);

				ogRef.New(rlRefTab);
			}
			else
			{
			
			}
		}

		// remove last "/n"
		opChoiceList = opChoiceList.Left(opChoiceList.GetLength() - 1);
		return true;
	}

	return false;
}
//--------------------------------------------------------------------------------------------------------------------

CString CBasicData::GetRefUrno(CString opTable, CString opValue)
{
	for (int i = 0; i < ogRef.GetSize(); i ++)
	{
		if (opTable == ogRef[i].Table)
		{
			for (int j = 0; j < ogRef[i].ValUrnoArr.GetSize(); j++)
			{
				if (opValue == ogRef[i].ValUrnoArr[j].Value)
				{
					return ogRef[i].ValUrnoArr[j].Urno;
				}
			}
		}
	}

	return "-1";
}
//--------------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------