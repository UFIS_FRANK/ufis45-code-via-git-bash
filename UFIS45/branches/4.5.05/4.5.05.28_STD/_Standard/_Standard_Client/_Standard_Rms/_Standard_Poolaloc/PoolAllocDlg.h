// PoolAllocDlg.h : header file
//

#if !defined(AFX_POOLALLOCDLG_H__2703BE57_A894_11D3_A6A9_0000C007916B__INCLUDED_)
#define AFX_POOLALLOCDLG_H__2703BE57_A894_11D3_A6A9_0000C007916B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000



#include <DialogGrid.h>
#include <RecordSet.h>
#include <RespTabViewer.h>
#include <Table.h>


class PoolAllocDlg : public CDialog
{
// Construction
public:
	PoolAllocDlg(CWnd* pParent = NULL);	// standard constructor
	~PoolAllocDlg();

// Implementation
	void SetStaticTexts();
	
	void InitializeTables();
	void ClearSgmTmp();
		
	void FillPoolGrid(CString opObject, CString opTitle);
	void FillGrpMemList(CString opAloUrno);
	void FillAloList();
	void FillRespTable();

	CString GetSelSgrUrno();
	CString GetSelectedAloUrno();
	void GetSelectedGrpMem(CString opReft, CString &opGrpMemUrno, CString &opGrpMemText);
	bool IsAlreadyAssigned(CString opUval, CString opSgrUrno);

	bool GetChoiceList(RecordSet *polAlo, CString &ropChoiceList);
	
	void DeletePoaEntries();	
	void SortPoolGrid();
	
	void MakeBarMenu(int itemID, CPoint point);
	void MakeRespMenu(int itemID, CPoint point) ;
	void FindObjectAndTitle(CString &opObject, CString &opTitle);
	
	bool IsItemAlreadyUsed(CGridFenster *popGrid, CString opText, int ipCol, int ipCurRow);
	
	
	LONG OnBcAdd(UINT /*wParam*/, LONG /*lParam*/); 
	
	// TESTFUNKTION: 
	//void DumpTmpObject(CString opObject);

	void SetTableHeader(int ipBtn);


// Attribute
HICON m_hIcon;

CGridFenster *pomActiveGrid;
CGridFenster *pomPoolGrid;

RespTabViewer *pomRespViewer;
Table *pomRespTable;

CString omSelectedPools;
CString omAloPoolUrno;
CString omPoaReft;

bool bmAutoAlloc;
bool bmRestrict;
bool bmInit;
bool bmSorting;
bool bmIsChanged;
bool bmRespChange;
bool bmReset;

int imCurrRow;
int imCurrCol;
int imCurPool;

int imSgmValuLength;





// Dialog Data
	//{{AFX_DATA(PoolAllocDlg)
	enum { IDD = IDD_POOLALLOC_DIALOG };
	int	imRadioBtn;
	CButton m_Opt_Default;
	CButton m_Opt_OrgUnit;
	CButton m_Opt_Func;
	CButton m_Opt_Quali;
	CButton m_Opt_Contr;
	CButton m_Opt_WkGrp;
	CButton m_Opt_PlGrp;
	CComboBox m_Cob_Pools;
	CListBox m_AloList;
	CListBox m_GrpMemList;
	int imNoRestric;
	CButton m_Btn_Add;
	//}}AFX_DATA
	

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(PoolAllocDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL



	// Generated message map functions
	//{{AFX_MSG(PoolAllocDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnDestroy();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnRadioButton();
	afx_msg void OnNoAutoAloc();
	afx_msg void OnNoRestrict();
	afx_msg void OnSelchangePools();
	afx_msg void OnDropdownPools();
	afx_msg void OnStartEditing(WPARAM wParam, LPARAM lParam);
	afx_msg void OnEndEditing(WPARAM wParam, LPARAM lParam);
	afx_msg void OnGridRClick(WPARAM wParam, LPARAM lParam);
	afx_msg void OnRespRBtnDown(WPARAM wParam, LPARAM lParam);
	afx_msg void OnDelete();
	afx_msg void OnOK();
	afx_msg void OnCancel();
	afx_msg void OnAddResp();
	afx_msg void OnSelChangeAlo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_POOLALLOCDLG_H__2703BE57_A894_11D3_A6A9_0000C007916B__INCLUDED_)
