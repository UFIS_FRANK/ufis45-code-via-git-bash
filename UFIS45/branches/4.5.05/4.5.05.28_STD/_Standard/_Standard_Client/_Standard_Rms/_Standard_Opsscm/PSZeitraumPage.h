#if !defined(AFX_PSZEITRAUMPAGE_H__1B055C51_8B1E_11D1_B444_0000B45A33F5__INCLUDED_)
#define AFX_PSZEITRAUMPAGE_H__1B055C51_8B1E_11D1_B444_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// PSZeitraumPage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// PSZeitraumPage dialog
#include <Ansicht.h>
#include <CCSPtrArray.h>
#include <CCSEdit.h>


class PSZeitraumPage : public CPropertyPage
{
		DECLARE_DYNCREATE(PSZeitraumPage)

// Construction
public:
	PSZeitraumPage();   // standard constructor

	void SetCalledFrom(CString opCalledFrom);
	CString omCalledFrom;

// Dialog Data
	//{{AFX_DATA(PSZeitraumPage)
	enum { IDD = IDD_PSZEEITRAUM_FLIGHT_PAGE };
	CButton	m_Check1;
	CButton	m_Check2;
	CButton	m_Check3;
	CButton	m_Check4;
	CButton	m_Check5;
	CButton	m_Check6;
	CButton	m_Check7;
	CButton	m_Check8;
	CButton	m_Rotation;
	CEdit	m_Edit1;   // TIFA From || TIFD From; Date
	CEdit	m_Edit2;   // TIFA From || TIFD From; Time
	CEdit	m_Edit3;   // TIFA To || TIFD To; Date
	CEdit	m_Edit4;   // TIFA To || TIFD To; Time
	CEdit	m_Edit5;   // SearchHours
	CEdit	m_Edit6;   // SearchDay
	CEdit	m_Edit7;   // DOOA DOOD
	CEdit	m_Edit8;   // RelHBefore
	CEdit	m_Edit9;   // RelHAfter
	CEdit	m_Edit10;  // LSTU; Date
	CEdit	m_Edit11;  // LSTU; Time
	CEdit	m_Edit12;  // FDAT; Date
	CEdit	m_Edit13;  // FDAT; Time
	CEdit	m_Edit14;  // ACL2
	CEdit	m_Edit15;  // FLTN
	CEdit	m_Edit16;  // FLNS
	CEdit	m_Edit17;  // REGN
	CEdit	m_Edit18;  // ACT3
	CEdit	m_Edit19;  // ORG3 || ORG4 || DES3 || DES4
	CEdit	m_Edit20;  // TIFA Flugzeit || TIFD Flugzeit; Date
	CEdit	m_Edit21;  // TIFA Flugzeit || TIFD Flugzeit; Time
	CEdit	m_Edit22;  // TTYP
	CEdit	m_Edit23;  // STEV
	//}}AFX_DATA

	CStringArray omValues;

	BOOL GetData();
	BOOL GetData(CStringArray &ropValues);
	void SetData();
	void SetData(CStringArray &ropValues);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(PSZeitraumPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(PSZeitraumPage)
		afx_msg void OnClickedAbflug();
		afx_msg void OnClickedAnkunft();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PSZEITRAUMPAGE_H__1B055C51_8B1E_11D1_B444_0000B45A33F5__INCLUDED_)
