#ifndef _CFLDATA_H_
#define _CFLDATA_H_

#include <CCSCedaData.h>
#include <CcaCedaFlightConfData.h>

void  ProcessDemandCf(void * popInstance, int ipDDXType, void *vpDataPointer, 
					  CString &ropInstanceName); // ?



/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct CFLDATA  
{
    long    Urno;
	char	Hopo[4];
	CTime	Time;
	char	Meno[5];
	char	Mety[3];
	char	Prio[3];
	char	Rtab[4];
	long	Rurn;
	char	Flst[256];  
	char	Nval[256];
	char	Oval[256];
	char	Akus[33];
	char	Akti[15];
	char	Akst[3];

	CFLDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
	}


};



/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaCflData: public CCSCedaData
{
// Attributes
public:

    CMapPtrToPtr omUrnoMap;       // Index at Urno
    CCSPtrArray<CFLDATA> omData;  // CFL-Data

	CcaCedaFlightConfData omFlightData;  // Flightdata of the loaded Cfl-Data

// Operations
public:
    CedaCflData();
    ~CedaCflData();

	void Register(void);

	bool ReadAllCfl(bool bpClearAll, bool bpDdx);	
	bool ReadCflOfFlights(const CString &opFlightUrnos, bool bpClearAll, bool bpDdx);
	void ReadFlightData(const CString &opFlightUrnos);
	bool InsertInternal(CFLDATA *prpDem, bool bpDdx = true);
	bool UpdateInternal(const CFLDATA *prpCfl, bool bpDdx);
	bool DeleteInternal(const CFLDATA *prpDem, bool bpDdx  = true);
	bool DeleteByUrno(long lpUrno);

	bool TestFlightOrigin(const CFLDATA *prpCfl) const;

	bool ClearAll(bool bpWithRegistration = true);

	void AddToKeyMap(CFLDATA *prpDem);
	void DelFromKeyMap(const CFLDATA *prpDem);

	CFLDATA *GetCflByUrno(long lpUrno) const;

	void ProcessDemandBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName); // ??
	
	bool ConfirmRecord(long lpUrno, const char *cpTime, const char *cpUser);
	void CflDataToStr(const CFLDATA &prpData, CString &opData) const;
	
	bool UpdateData(const CedaCflData &opDataSource, long lpUrno);


	bool bmInvalid;

private:
	bool bmAFTRead;


};

#endif
