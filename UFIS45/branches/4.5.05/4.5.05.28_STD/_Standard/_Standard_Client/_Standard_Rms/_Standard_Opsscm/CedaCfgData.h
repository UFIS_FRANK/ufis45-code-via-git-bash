#ifndef _CCFGD_H_
#define _CCFGD_H_

#include <CCSCedadata.h>
#include <CCSPtrArray.h>




#define MON_COUNT_STRING			"MONITORCOUNT"
#define MON_SEASONSCHEDULE_STRING	"SEASONSCHEDULE"
#define MON_DAILYSCHEDULE_STRING	"DAILYSCHEDULE"
#define MON_SEASONROTDLG_STRING	    "SEASONROTDLG"
#define MON_SEASONBATCH_STRING	    "SEASONBATCH"
#define MON_DAILYROTDLG_STRING	    "DAILYROTDLG"
#define MON_FLIGHTDIA_STRING	    "FLIGHTDIA"
#define MON_CCADIA_STRING			"CCADIA"





/////////////////////////////////////////////////////////////////////////////
// Record structure declaration




struct RAW_VIEWDATA
{
	char Ckey[40];
	char Name[100];
	char Type[40];
	char Page[40];
	char Values[2001];
	RAW_VIEWDATA()
	{ memset(this,'\0',sizeof(*this));}
};
struct VIEW_TEXTDATA
{
	CString Page;							// e.g. Rank, Pool, Shift
	CCSPtrArray <CString> omValues;					// e.g. Page=Shift: F1,F50,N1 etc.
};

struct VIEW_TYPEDATA
{
	CString Type;							// e.g. Filter, Group, Sort
	CCSPtrArray <VIEW_TEXTDATA> omTextData; // necessary only with filters
	CCSPtrArray<CString> omValues;					// values only relevant if Type != Filter
};
struct VIEW_VIEWNAMES
{
	CString ViewName;						// e.g. <Default>, Heute, Test etc.
	CCSPtrArray<VIEW_TYPEDATA> omTypeData;
};
struct VIEWDATA
{
	CString Ckey;							// e.g. Staffdia, CCI-Chart etc.
	CCSPtrArray<VIEW_VIEWNAMES> omNameData;
};
//struct CfgDataStruct {
struct CFGDATA {
    // Data fields from table CFGCKI for whatif-rows
    long    Urno;           // Unique Record Number of CFGCKI
    char    Appn[34];       // name of application
    char    Ctyp[34];       // Type of Row; in Whaif constant string "WHAT-IF"
	char    Ckey[34];		// Name of what-if row
    CTime	Vafr;           // Valid from
    CTime	Vato;           // Valid to
	char	Text[2001];		// Parameter string
	char	Pkno[34];		// Staff-/User-ID
    int	    IsChanged;		// Is changed flag

	CFGDATA(void) 
	{ memset(this,'\0',sizeof(*this));
	  strcpy(Appn,"OPSSCM");
	  //strcpy(Vafr, "19960101000000");
	  //strcpy(Vato, "20351231000000");
	  IsChanged=-1;}

};	

struct USERSETUPDATA
{
	CString MONS;	// Monitos:					1|2|3
	CString RESO;	// Resolution:				800x600|1024x768|1280y1024
	CString GACH;   // Gatechart monitor pos.:	L|M|R
	CString STCH;	// Staffchart monitor pos.:	L|M|R
	CString CCCH;	// CCI-Chart monitor pos.:	L|M|R
	CString FPTB;	// Flightplan monitor pos.:	L|M|R
	CString STTB;	// Stafftable monitor pos.:	L|M|R
	CString GATB;   // Gatetable monitor pos.:	L|M|R
	CString CCTB;   // CCI-Table monitor pos.:	L|M|R
	CString RQTB;   // Requests monitor pos.:	L|M|R
	CString PKTB;	// Peaktable monitor pos.:	L|M|R
	CString CVTB;	// Coverage monitor pos.:	L|M|R
	CString VPTB;	// Prplantable monitor pos.:L|M|R
	CString CFTB;	// Conflicts monitor pos.:	L|M|R
	CString ATTB;	// Info monitor pos.:		L|M|R
	CString INFO;	// Info monitor pos.:		L|M|R
	CString LHHS;	// LH-Host monitor pos.:	L|M|R
	CString TACK;   // Turnaroundbars			J|N
	CString FBCK;	// Flightbars without demands J|N
	CString SBCK;	// Shadowbars				J|N
	CString STCV;   // View: Staffchart
	CString GACV;   // View: Gatechart
	CString CCCV;   // View: CCI-Chart
	CString STLV;   // View: Stafftable
	CString FPLV;   // View: Flighttable
	CString GBLV;   // View: Gatetable
	CString CCBV;   // View: CCI-table
	CString RQSV;   // View: Requests
	CString PEAV;   // View: Peaktable
	CString VPLV;   // View: Preplantable
	CString CONV;   // View: Conflicts
	CString ATTV;   // View: "Achtung" (Attention)
	USERSETUPDATA(void)
	{
		MONS = "1";	RESO = "1024x768";
		GACH = "L";	STCH = "L";	CCCH = "L";	FPTB = "L";
		STTB = "L";	GATB = "L";	CCTB = "L";	RQTB = "L";
		PKTB = "L";	CVTB = "L";	VPTB = "L";	CFTB = "L";
		ATTB = "L";	INFO = "L";	LHHS = "L";	TACK = "J";
		FBCK = "J";	SBCK = "J";
		STCV = "<Default>";	GACV = "<Default>";
		CCCV = "<Default>";	STLV = "<Default>";
		FPLV = "<Default>";	GBLV = "<Default>";
		CCBV = "<Default>";	RQSV = "<Default>";
		PEAV = "<Default>";	VPLV = "<Default>";
		CONV = "<Default>";	ATTV = "<Default>";
	}
};

typedef struct CFGDATA SETUPDATA;
//typedef struct CfgDataStruct CFGDATA;

// the broadcast CallBack function, has to be outside the CedaCfgData class
void ProcessCfgCf(void *vpInstance,enum enumDDXTypes ipDDXType, void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaCfgData: public CCSCedaData
{
// Attributes
public:
    CCSPtrArray<CFGDATA> omData;
    CCSPtrArray<CFGDATA> omSetupData;
	CCSPtrArray<VIEWDATA> omViews;
    CMapStringToPtr omCkeyMap;
    CMapPtrToPtr    omUrnoMap;
    CMapPtrToPtr    omSetupUrnoMap;
	USERSETUPDATA   rmUserSetup;
	CFGDATA			rmMonitorSetup;
	CFGDATA			rmFontSetup;
	CFGDATA			rmPopsReportDailyAlcSetup;
// Operations
public:
    CedaCfgData();
    //@ManMemo: Destructor, Unregisters the CedaCfgData object from DataDistribution
	~CedaCfgData();

	
	char pcmListOfFields[2048];
	//@ManMemo: Create the Request for What-If
	bool CreateCfgRequest(const CFGDATA *prpCfgData);
    //@ManMemo: Read all Cfg from database at program start
	bool ReadCfgData();
	//@ManMemo: Delete a Cfg
	bool ChangeCfgData(CFGDATA *prpCfg);
	//@ManMemo: Delete the Cfg
	bool DeleteCfg(long lpUrno);
	//@ManMemo: Adds a Cfg to omData and to the Maps
	bool AddCfg(CFGDATA *prpCfg);
	//@ManMemo: Makes Database-Actions Insert/Update/Delete
	bool SaveCfg(CFGDATA *prpCfg);	
    //@ManMemo: Prepare the data, not used for the moment
	void PrepareCfgData(CFGDATA *prpCfg);
	//@ManMemo: Insert staff data (const CFGDATA *prpCfgData);    // used in PrePlanTable only

	int GetMonitorForWindow(CString opWindow);
	int GetMonitorCount();
	bool ReadMonitorSetup();


	bool ReadFontSetup();
	bool ReadPopsReportDailyAlcSetup();
	CString GetPopsReportDailyAlcSetup();


	BOOL InterpretSetupString(CString popSetupString, USERSETUPDATA *prpSetupData);
    //@ManMemo: Update cfg data 
    bool UpdateCfg(const CFGDATA *prpCfgData);    // used in PrePlanTable only
	void ProcessCfgBc(enum enumDDXTypes ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	bool InsertCfg(const CFGDATA *prpCfgData);
	long  GetUrnoById(char *pclWiid);
	BOOL  GetIdByUrno(long lpUrno,char *pcpWiid);

	CFGDATA  * GetCfgByUrno(long lpUrno);
	void SetCfgData();
	void MakeCurrentUser();
	void ClearAllViews();
	void PrepareViewData(CFGDATA *prpCfg);
	VIEWDATA * FindViewData(CFGDATA *prlCfg);
	VIEW_VIEWNAMES * FindViewNameData(RAW_VIEWDATA *prpRawData);
	VIEW_TYPEDATA * FindViewTypeData(RAW_VIEWDATA *prpRawData);
	VIEW_TEXTDATA * FindViewTextData(RAW_VIEWDATA *prpRawData);
	BOOL MakeRawViewData(CFGDATA *prpCfg, RAW_VIEWDATA * prpRawData);
	void MakeViewValues(char *pspValues, CCSPtrArray<CString> *popValues, char *pcpSepa = "|");
	void UpdateViewForDiagram(CString opDiagram, VIEWDATA &prpView,CString opViewName);
	void DeleteViewFromDiagram(CString opDiagram, CString olView);
	bool ReadConflicts(CStringArray &opLines);
	bool SaveConflictData(CFGDATA *prpCfg);
	
	bool GetFlightConfig();
	CString GetLkey();


private:
    BOOL CfgExist(long lpUrno);
    bool InsertCfgRecord(const CFGDATA *prpCfgData);
    bool UpdateCfgRecord(const CFGDATA *prpCfgData);


};

extern CedaCfgData ogCfgData;
#endif
