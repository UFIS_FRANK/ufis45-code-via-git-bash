// CedaDemandData.cpp - Read and write demand data 
//
//
// Written by:
// Damkerng Thammathakerngkit   May 5, 1996
//


#include <stdafx.h>
#include <ccsglobl.h>
#include <OpssCm.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <BasicData.h>
#include <CedaDemandData.h>
#include <CedaBasicData.h>
#include <CcaCedaFlightData.h>
#include <algorithm>
#include <UFIS.H>


static int CompareDebe(const DEMANDDATA **e1, const DEMANDDATA **e2)
{
	return (((**e1).Debe == (**e2).Debe) ? 0 : (((**e1).Debe > (**e2).Debe) ? 1 : -1));
}


static int CompareKKey(const DEMANDDATA **e1, const DEMANDDATA **e2)
{
	return (((**e1).KKey == (**e2).KKey) ? 0 : (((**e1).KKey > (**e2).KKey) ? 1 : -1));
}





void  ProcessDemandCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaDemandData::CedaDemandData()
{
    // Create an array of CEDARECINFO for DEMANDDATA
    BEGIN_CEDARECINFO(DEMANDDATA, DemandDataRecInfo)
        FIELD_LONG(Urno,"URNO")
        FIELD_LONG(Ouro,"OURO")
        FIELD_LONG(Urud,"Urud")
        FIELD_CHAR_TRIM(Alid,"ALID")
        FIELD_DATE(Debe,"DEBE")
        FIELD_DATE(Deen,"DEEN")
        FIELD_CHAR_TRIM(Dety,"DETY")
//        FIELD_CHAR_TRIM(Rety,"RETY")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(DemandDataRecInfo)/sizeof(DemandDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&DemandDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names


	strcpy(pcmTableName,"DEM");

//    pcmFieldList = "URNO,OURO,URUD,ALID,DEBE,DEEN,DETY,RETY";
    pcmFieldList = "URNO,OURO,URUD,ALID,DEBE,DEEN,DETY";


	omUrnoMap.InitHashTable( 2001 );
	
	omOuroMap.InitHashTable( 2001 );
	
	omData.SetSize(0,480);	
}


CedaDemandData::~CedaDemandData()
{
	TRACE("CedaDemandData::~CedaDemandData\n");
	ClearAll();
	ogDdx.UnRegister((void *)this,BC_DEMAND_CHANGE);
	ogDdx.UnRegister((void *)this,BC_DEMAND_NEW);
	ogDdx.UnRegister((void *)this,BC_DEMAND_DELETE);
	ogDdx.UnRegister((void *)this,CCA_FLIGHT_CHANGE);
	ogDdx.UnRegister((void *)this,BC_CFL_READY);
}


void CedaDemandData::Register(void)
{
	int inf = (int)BC_CFL_READY;
	ogDdx.Register((void *)this,BC_CFL_READY,       CString("BC_CFL_READY"), CString("Demands created"),ProcessDemandCf);	
	ogDdx.Register((void *)this,BC_DEMAND_CHANGE,	CString("DEMANDDATA"), CString("Cca-changed"),	ProcessDemandCf);
	ogDdx.Register((void *)this,BC_DEMAND_NEW,		CString("DEMANDDATA"), CString("Cca-new"),		ProcessDemandCf);
	ogDdx.Register((void *)this,BC_DEMAND_DELETE,	CString("DEMANDDATA"), CString("Cca-deleted"),	ProcessDemandCf);
	ogDdx.Register((void *)this,CCA_FLIGHT_CHANGE,	CString("CCAFLIGHTDATA"), CString("Cca-Flight-changed"),ProcessDemandCf);
}




bool CedaDemandData::ClearAll( bool bpWithRegistration)
{

	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
	omFlnus = "";

    omUrnoMap.RemoveAll();
    omData.DeleteAll();

	POSITION rlPos;
	CCSPtrArray<DEMANDDATA> *prlArray;


	for ( rlPos =  omOuroMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omOuroMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& )prlArray);
		prlArray->RemoveAll();
		delete prlArray;
	}
	omOuroMap.RemoveAll();


	omKKeyArray.RemoveAll();


	POSITION pos;
	void  *pVoid;
	for( pos = omKKeyMap.GetStartPosition(); pos != NULL; )
	{
		omKKeyMap.GetNextAssoc( pos, pVoid, (void *&)prlArray );
		prlArray->RemoveAll();
		delete prlArray;
	}
	omKKeyMap.RemoveAll();

    return true;
}



bool CedaDemandData::ReadAll(CString &opUrnos, CTime opFrom, CTime opTo, bool bpClearAll)
{
	char pclSel[7000];
	CString olSelection;
	CStringArray olUrnoLists;
	char pclSelection[1024] = "";

	if(bpClearAll)
	{
		ClearAll(false);
		omFlnus = opUrnos;
	}
	else
	{
		omFlnus += opUrnos;
	}

	for(int i = SplitItemList(opUrnos, &olUrnoLists, 100) - 1; i >= 0; i--)
	{

		olSelection = CString("WHERE OURO IN (");
		olSelection += olUrnoLists[i] + CString(") AND RETY = '001'");

		strcpy(pclSel, olSelection );

		Read(pclSel, false, false);

	}

	//read common demands
	sprintf(pclSel,"WHERE DEBE>'%s00' AND DEEN <'%s00' AND DETY = '6' AND RETY = '001'", opFrom.Format("%Y%m%d%H%M"), opTo.Format("%Y%m%d%H%M")); 
	Read(pclSel, false, false);



	/*
	DEMANDDATA *prlDemand;
	CString olRety;
	char buffer[64];

	for( i = omData.GetSize() - 1; i >= 0; i--)
	{
		prlDemand = &omData[i];
	
		ltoa(prlDemand->Urud, buffer, 10);

		olRety = ogBCD.GetField("RUD", "URNO", CString(buffer),"RETY");

		if(olRety != "001")
		{
			DelFromKeyMap(prlDemand);
			omData.DeleteAt(i);
		}

	}
	*/

	return true;
}





bool CedaDemandData::Read(char *pspWhere /*NULL*/, bool bpClearAll, bool bpDdx)
{
    // Select data from the database
	bool ilRc = true;

	if(bpClearAll)
	{
		ClearAll();
	}


	if(pspWhere == NULL)
	{	
		return false;
	}
	else
	{
		strcat(pspWhere, "AND ALOC = 'CIC' ");
		ilRc = CedaAction("RT", pspWhere);
	}

	if (ilRc != true)
	{
		if(!omLastErrorMessage.IsEmpty())
		{
			if(omLastErrorMessage.Find("ORA") != -1)
			{
				char pclMsg[2048]="";
				sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
				::MessageBox(NULL,pclMsg,"Datenbankfehler",MB_OK);
				return false;
			}
		}
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		DEMANDDATA *prlDem = new DEMANDDATA;
		if ((ilRc = GetFirstBufferRecord(prlDem)) == true)
		{
			if(UpdateInternal(prlDem, bpDdx))
			{
				delete prlDem;
			}
			else
			{
				InsertInternal(prlDem, bpDdx);
			}
		}
		else
		{
			delete prlDem;
		}
	}
    return true;

}




bool CedaDemandData::InsertInternal(DEMANDDATA *prpDem, bool bpDdx)
{
	DEMANDDATA *prlDem = GetDemandByUrno(prpDem->Urno);
	if(prlDem != NULL)
		DeleteInternal(prlDem);

	
	omData.Add(prpDem);//Update omData
	AddToKeyMap(prpDem);
	if(bpDdx)
		ogDdx.DataChanged((void *)this, DEMAND_CHANGE,(void *)&prpDem->Urno ); //Update Viewer
    return true;
}


bool CedaDemandData::UpdateInternal(DEMANDDATA *prpDem, bool bpDdx)
{
	DEMANDDATA *prlDem = GetDemandByUrno(prpDem->Urno);

	DelFromKeyMap(prlDem);

	if (prlDem != NULL)
	{
		*prlDem = *prpDem; //Update omData
		AddToKeyMap(prlDem);

		if(bpDdx)
			ogDdx.DataChanged((void *)this,DEMAND_CHANGE,(void *)&prlDem->Urno); //Update Viewer

		return true;
	}
    return false;
}


bool CedaDemandData::DeleteInternal(DEMANDDATA *prpDem, bool bpDdx )
{
	long llUrno = prpDem->Urno;

	DelFromKeyMap(prpDem);
	int ilCcaCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilCcaCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpDem->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
	if(bpDdx)
		ogDdx.DataChanged((void *)this,DEMAND_CHANGE,(void *)&llUrno); //Update Viewer
    return true;
}




void CedaDemandData::AddToKeyMap(DEMANDDATA *prpDem)
{
	CCSPtrArray<DEMANDDATA> *prlArray;

	if (prpDem != NULL )
	{
		omUrnoMap.SetAt((void *)prpDem->Urno,prpDem);

		if(prpDem->Ouro > 0)
		{
			if(omOuroMap.Lookup((void *)prpDem->Ouro,(void *& )prlArray) == TRUE)
			{
				for(int i = prlArray->GetSize() - 1; i >= 0; i--)
				{
					if(((*prlArray)[i]).Urno == prpDem->Urno)
					{
						prlArray->RemoveAt(i);
						break;
					}
				}
				prlArray->Add(prpDem);
			}
			else
			{
				prlArray = new CCSPtrArray<DEMANDDATA>;
				omOuroMap.SetAt((void *)prpDem->Ouro,prlArray);
				prlArray->Add(prpDem);;
			}
		}

	}

}



void CedaDemandData::DelFromKeyMap(DEMANDDATA *prpDem)
{
	CCSPtrArray<DEMANDDATA> *prlArray;

	if (prpDem != NULL )
	{
		omUrnoMap.RemoveKey((void *)prpDem->Urno);

		if(omOuroMap.Lookup((void *)prpDem->Ouro,(void *& )prlArray) == TRUE)
		{
			for(int i = prlArray->GetSize() - 1; i >= 0; i--)
			{
				if(((*prlArray)[i]).Urno == prpDem->Urno)
				{
					prlArray->RemoveAt(i);
					break;
				}
			}
			if(prlArray->GetSize() == 0)
			{
				omOuroMap.RemoveKey((void *)prpDem->Ouro);
				delete prlArray;
			}
		}

	}
}






DEMANDDATA  *CedaDemandData::GetDemandByUrno(long lpUrno)
{
	DEMANDDATA  *prlDemand;

	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlDemand) == TRUE)
	{
		return prlDemand;
	}
	return NULL;
}







bool  CedaDemandData::GetDemansByOuro(CCSPtrArray<DEMANDDATA> &ropDemandList, long lpOuro)
{

	CCSPtrArray<DEMANDDATA> *prlArray;
	DEMANDDATA  *prlDemand;

	if(omOuroMap.Lookup((void *)lpOuro,(void *& )prlArray) == TRUE)
	{
		for(int i = prlArray->GetSize() - 1; i >= 0; i--)
		{
			prlDemand = &(*prlArray)[i];
			ropDemandList.Add(prlDemand);
		}
	}
	
	ropDemandList.Sort(CompareKKey);
	if(ropDemandList.GetSize() > 0)
	{
		return true;
	}
	else
	{
		return false;
	}

}

bool  CedaDemandData::GetCommonDemands(CCSPtrArray<DEMANDDATA> &ropDemandList)
{
	DEMANDDATA  *prlDemand;

	for (int ilLc = 0; ilLc < omData.GetSize(); ilLc++)
	{
		prlDemand = &omData[ilLc];
		ASSERT(prlDemand);
		if (prlDemand)
		{
			if(prlDemand->Dety[0] == CCI_DEMAND && omData[ilLc].Ouro == 0)
				ropDemandList.Add(prlDemand);
		}
	}

	ropDemandList.Sort(CompareDebe);

	if(ropDemandList.GetSize() > 0)
		return true;
	else
		return false;
}








void  ProcessDemandCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{

		((CedaDemandData *)popInstance)->ProcessDemandBc(ipDDXType,vpDataPointer,ropInstanceName);
}


void  CedaDemandData::ProcessDemandBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{

	struct BcStruct *prlBcData;			
	CString olSelection;
	prlBcData = (struct BcStruct *) vpDataPointer;
	DEMANDDATA *prlDemand;

		if(ipDDXType == CCA_FLIGHT_CHANGE)
		{
			CCAFLIGHTDATA *prlFight = (	CCAFLIGHTDATA *)vpDataPointer;
			if (prlFight)
			{
				char buffer[65];
				ltoa(prlFight->Urno, buffer, 10);

				if(omFlnus.Find(buffer) >= 0)
					return;
				else
					omFlnus += CString(",'") + buffer + CString("'");

			}
		}


		if (ipDDXType == BC_CFL_READY)
		{
			char pclWks[200]="";
			CString olWks = prlBcData->ReqId;
			::GetWorkstationName(pclWks);
			CString olWks2 = pclWks;

			if(olWks == olWks2)
			{
				if (bgCcaDiaSeason)
					::MessageBox(NULL, GetString(IDS_CFL_READY_SEASON), GetString(IDS_WARNING), (MB_OK|MB_ICONINFORMATION));
				else
					::MessageBox(NULL, GetString(IDS_CFL_READY), GetString(IDS_WARNING), (MB_OK|MB_ICONINFORMATION));
				bgCreateDemIsActive = false;

			}
			
		}

		if(ipDDXType == BC_DEMAND_NEW)
		{
			prlDemand = new DEMANDDATA;
			GetRecordFromItemList(prlDemand,prlBcData->Fields,prlBcData->Data);

			CString olRety = "";
			GetListItemByField(CString(prlBcData->Data), CString(prlBcData->Fields), CString("RETY"), olRety);

			bool blpass = false;
			if(prlDemand->Dety[0] == CCI_DEMAND && prlDemand->Ouro == 0 && (strcmp(olRety, "001")) == 0)
				blpass = true;
			if(prlDemand->Dety[0] == OUTBOUND_DEMAND && prlDemand->Ouro != 0 && (strcmp(olRety, "001")) == 0)
				blpass = true;

			if(prlDemand->Urno > 0 && blpass)
			{
				if(IsPassFilter(prlDemand))
				{
					InsertInternal(prlDemand);
					return;
				}
			}
			delete prlDemand;
		}

		if((ipDDXType == BC_DEMAND_CHANGE) || (ipDDXType == BC_DEMAND_DELETE))
		{
			prlDemand = new DEMANDDATA;
			
			GetRecordFromItemList(prlDemand,prlBcData->Fields,prlBcData->Data);

			if(prlDemand->Urno == 0 )
				prlDemand->Urno = GetUrnoFromSelection(prlBcData->Selection);
				
			if(prlDemand->Urno > 0 )
			{
				DEMANDDATA *prlDemand2;

				bool blRead = false;
				if(omUrnoMap.Lookup((void *)prlDemand->Urno,(void *& )prlDemand2) == TRUE)
				{
					if(blRead = FillRecord((void *)prlDemand2, CString(prlBcData->Fields), CString(prlBcData->Data)) )
					{
						if (ipDDXType == BC_DEMAND_DELETE)
						{
							DIACCADATA *prlCca = ogCcaDiaFlightData.omCcaData.GetCcaByDemand(prlDemand->Urno);
							if (prlCca)
							{
								prlCca->Ghpu = 0;
								prlCca->Ghsu = 0;
							}

							DeleteInternal(prlDemand2, true);
						}
						else
							UpdateInternal(prlDemand2, true);
					}
				}
			}

			delete prlDemand;
			return;
		}
		
}



bool  CedaDemandData::IsPassFilter(DEMANDDATA *prlDemand)
{
	bool blRet = false;
//	bool blRet = true;
	char buffer[64];

	ltoa(prlDemand->Ouro, buffer, 10);

	if(omFlnus.Find(buffer) >= 0)
	{
		blRet = true;
	}

	return blRet;
}







void CedaDemandData::Kompress(CCSPtrArray<CCAFLIGHTDATA> &opFLights)
{
	bool bmNew = true;

	if(bmNew)
	{
		KompressFreq(opFLights);
		return;
	}

}





void CedaDemandData::KompressFreq(CCSPtrArray<CCAFLIGHTDATA> &opFLights)
{

	CCSPtrArray<DEMANDDATA> olDemands;
	CCSPtrArray<DEMANDDATA> *prlArray;
	CCSPtrArray<DEMANDDATA> *prlArray2;
	DEMANDDATA *prlDemand1;
	DEMANDDATA *prlDemand2;
	POSITION pos;
	void  *pVoid;

	CMapPtrToPtr olTmpMap;
	

	for(int i = opFLights.GetSize() - 1; i >= 0; i--)
	{
		GetDemansByOuro( olDemands, opFLights[i].Urno);
	}

	while( olDemands.GetSize() > 0 )
	{
		prlDemand1 = &olDemands[0];
		prlDemand1->KKey = ogBasicData.GetNextUrno();
		olDemands.RemoveAt(0);
		prlDemand1->FlightFreq = opFLights[0].Freq;

		//omKKeyArray.Add(prlDemand1->KKey);

		prlArray = new CCSPtrArray<DEMANDDATA>;
		prlArray->Add(prlDemand1);
		//omKKeyMap.SetAt((void *)prlDemand1->KKey , (void *&) prlArray);
		olTmpMap.SetAt((void *)prlDemand1->KKey , (void *&) prlArray);

		bool blTreffer;
		DEMANDDATA *prlDemand3;

		for(int j  = olDemands.GetSize() - 1; j >= 0 ; j--)
		{
			prlDemand2 = &olDemands[j];

			if( CompareCompressCca(prlDemand1, prlDemand2)   )
			{
				blTreffer = false;
				//if( omKKeyMap.Lookup((void *)prlDemand1->KKey, (void *&) prlArray) == TRUE)
				if( olTmpMap.Lookup((void *)prlDemand1->KKey, (void *&) prlArray) == TRUE)
				{
					for(int g = prlArray->GetSize() -1; g >= 0; g--)
					{
						prlDemand3 = &(*prlArray)[g];

						if(	(prlDemand2->Debe == prlDemand3->Debe) && (prlDemand2->Deen == prlDemand3->Deen)/* && (CString(prlDemand2->Ckic) == CString(prlDemand3->Ckic))*/) 
						{							
							blTreffer = true;
							break;
						}
					}
					if(!blTreffer)
					{
						prlDemand2->KKey = prlDemand1->KKey ;
						prlDemand2->FlightFreq = opFLights[0].Freq;
						olDemands.RemoveAt(j);
						prlArray->Add(prlDemand2);
					}
				}
			}
		}
	}



	////////////////////////////////////////////////////////
	// Freq	

	DEMANDDATA *prlDemandAct;
	DEMANDDATA *prlDemandPrev;
	DEMANDDATA *prlDemandNext;
	COleDateTimeSpan olDiff;
	int ilCount;

	for( pos = olTmpMap.GetStartPosition(); pos != NULL; )
	{
		olTmpMap.GetNextAssoc( pos, pVoid, (void *&)prlArray );
	
		prlArray->Sort(CompareDebe);

		ilCount = prlArray->GetSize();

		//TRACE("\n--------------- ");

		for(int i = 0;  i < ilCount; i++)
		{
			prlDemandAct = &(*prlArray)[i];

			if(i == 0)
				prlDemandPrev = NULL;
			else
				prlDemandPrev = &(*prlArray)[i-1];

			if(i < ilCount -1)
				prlDemandNext = &(*prlArray)[i+1];
			else
				prlDemandNext = NULL;

			
			if( prlDemandPrev == 0)
			{
				prlDemandAct->PrevC = 0;	
			}
			else
			{

				COleDateTime  olTag1( prlDemandAct->Debe.GetYear(), prlDemandAct->Debe.GetMonth(),
					prlDemandAct->Debe.GetDay(), prlDemandAct->Debe.GetHour(), 
					prlDemandAct->Debe.GetMinute(), prlDemandAct->Debe.GetSecond()) ;
				COleDateTime  olTag2( prlDemandPrev->Debe.GetYear(), prlDemandPrev->Debe.GetMonth(),
					prlDemandPrev->Debe.GetDay(), prlDemandPrev->Debe.GetHour(), 
					prlDemandPrev->Debe.GetMinute(), prlDemandPrev->Debe.GetSecond()) ;
				
				olDiff = olTag1 - olTag2 ;
				prlDemandAct->PrevC = olDiff.GetDays();	
			}

			if( prlDemandNext == 0)
			{
				prlDemandAct->NextC = 0;	
			}
			else
			{

				COleDateTime  olTag1( prlDemandAct->Debe.GetYear(), prlDemandAct->Debe.GetMonth(),
					prlDemandAct->Debe.GetDay(), prlDemandAct->Debe.GetHour(), 
					prlDemandAct->Debe.GetMinute(), prlDemandAct->Debe.GetSecond()) ;
				COleDateTime  olTag2( prlDemandNext->Debe.GetYear(), prlDemandNext->Debe.GetMonth(),
					prlDemandNext->Debe.GetDay(), prlDemandNext->Debe.GetHour(), 
					prlDemandNext->Debe.GetMinute(), prlDemandNext->Debe.GetSecond()) ;
				
				olDiff = olTag2 - olTag1;
				prlDemandAct->NextC = olDiff.GetDays();	
			}
			//TRACE("\n%s %s Prev: %d Next: %d Freq: %d ", prlDemandAct->Ckic, prlDemandAct->Debe.Format("%H:%M  %d.%m.%Y"), prlDemandAct->PrevC, prlDemandAct->NextC, prlDemandAct->Freq);  

		}
		//TRACE("\n--------------- ");

	}

	/*
	for( pos = olTmpMap.GetStartPosition(); pos != NULL; )
	{
		olTmpMap.GetNextAssoc( pos, pVoid, (void *&)prlArray );

		omKKeyMap.SetAt( (void *) pVoid , (void *&) prlArray);
		omKKeyArray.Add( (long)pVoid);


	}
	return;
	*/

	// The really kompressor
	int ilActPeri;
	char buffer[64];
	int ilLastFreq;
	bool blNew = false;
	DEMANDDATA	*prlDemand;

	for( pos = olTmpMap.GetStartPosition(); pos != NULL; )
	{
		olTmpMap.GetNextAssoc( pos, pVoid , (void *&)prlArray );
		
		ilCount = prlArray->GetSize();

		ilActPeri = 1;
		ilLastFreq = 0;		

		itoa(ilActPeri, buffer, 10);
		prlDemand = &(*prlArray)[0];

		prlArray2 = new CCSPtrArray<DEMANDDATA>;
		prlArray2->Add(prlDemand);
		

		prlDemand->KKey = ogBasicData.GetNextUrno();
		omKKeyArray.Add(prlDemand->KKey);

		omKKeyMap.SetAt( (void *)prlDemand->KKey , (void *&) prlArray2);


		prlDemandPrev = prlDemand;

		for(int i = 1;  i < ilCount; i++)
		{
			prlDemand = &(*prlArray)[i];

			if(prlDemand->PrevC > 7 )
			{
				if(((prlDemand->PrevC > prlDemand->NextC) && (prlDemand->NextC > 0)) || ((  ilLastFreq != prlDemand->PrevC )&& (ilLastFreq != 0)))
				{
					ilLastFreq = 0;
					ilActPeri++;
					blNew = true;
				}
				else
				{
					ilLastFreq = prlDemand->PrevC;
					prlDemand->Freq = ilLastFreq;
					prlDemandPrev->Freq = ilLastFreq;
				}
			}
			else
			{
				ilLastFreq = 7;
				prlDemand->Freq = ilLastFreq;
				prlDemandPrev->Freq = ilLastFreq;
			}
			
			if(!blNew)
			{
				if( omKKeyMap.Lookup( ( void *)prlDemandPrev->KKey, (void *&) prlArray2) == TRUE)
				{
					prlArray2->Add(prlDemand);
					prlArray2->Sort(CompareDebe);
					prlDemand->KKey = prlDemandPrev->KKey;

				}
			}
			else
			{
				prlArray2 = new CCSPtrArray<DEMANDDATA>;
				prlArray2->Add(prlDemand);

				prlDemand->KKey = ogBasicData.GetNextUrno();
				omKKeyArray.Add(prlDemand->KKey);
				omKKeyMap.SetAt( (void *) prlDemand->KKey , (void *&) prlArray2);
			}

			blNew = false;
			prlDemandPrev = prlDemand;

		}

	}






	// Clear tmpmap
	for( pos = olTmpMap.GetStartPosition(); pos != NULL; )
	{
		olTmpMap.GetNextAssoc( pos, pVoid, (void *&)prlArray );
		prlArray->RemoveAll();
		delete prlArray;
	}
	olTmpMap.RemoveAll();


}

bool CedaDemandData::CompareCompressCca(DEMANDDATA *prpDemand1, DEMANDDATA *prpDemand2)
{

	if( prpDemand1->Debe.Format("%H%M") != prpDemand2->Debe.Format("%H%M") ) return false;

	if( prpDemand1->Deen.Format("%H%M") != prpDemand2->Deen.Format("%H%M") ) return false;

	//////////////////////////////!!!!!!!!!!!!!!!!!!!!

	
	if( prpDemand1->Urud != prpDemand2->Urud ) return false;



	char buffer[64];

	ltoa(prpDemand1->Urud, buffer, 10);

	CString olRueUrno1 = ogBCD.GetField("RUD", "URNO", CString(buffer),"URUE");


	ltoa(prpDemand2->Urud, buffer, 10);

	CString olRueUrno2 = ogBCD.GetField("RUD", "URNO", CString(buffer),"URUE");


	if( olRueUrno2 != olRueUrno1 ) return false;



	return true;
}




bool CedaDemandData::GetDemandsByKKey(CCSPtrArray<DEMANDDATA> &ropDemandList, long lpKKey)
{
	bool blRet = false;

	CCSPtrArray<DEMANDDATA> *prlArray;

	DEMANDDATA *prlDemand;

	if( omKKeyMap.Lookup((void *)lpKKey, (void *&) prlArray) == TRUE)
	{
		blRet = true;
		
		for(int i = prlArray->GetSize() - 1; i >= 0; i--)
		{
			prlDemand = &((*prlArray)[i]);
			//TRACE("\n %s %ld %s %s ", prlCca->Ctyp, prlCca->Flnu, prlCca->Flno, prlCca->Ckbs.Format("%d.%d.%Y  %H:%M"));
			ropDemandList.Add(prlDemand);
		}

	}

	if(ropDemandList.GetSize() > 0)
	{
		ropDemandList.Sort(CompareDebe);
	}
	return blRet;

}


CString CedaDemandData::GetGroupNames( long opDemUrno)
{
	CString olRet;
	CString olGroupName;

	DEMANDDATA *prlDemand;
	RLODATA *prlRlo;

	CCSPtrArray<RLODATA> olArray;

	//char buffer[64];

	prlDemand = GetDemandByUrno(opDemUrno);

	if(prlDemand != NULL)
	{
		omRloData.GetRlosByUrud( olArray, prlDemand->Urud);

		for(int i = olArray.GetSize() -1 ; i >= 0; i--)
		{
			prlRlo = &olArray[i];

			olGroupName = "";

			olGroupName = ogBCD.GetField("SGR", "URNO", CString(prlRlo->Rloc), "GRPN");

			if(olGroupName.IsEmpty())
				olGroupName = ogBCD.GetField("CIC", "URNO", CString(prlRlo->Rloc), "CNAM");

			olRet += olGroupName + CString(" ");

		}

	}
	return olRet;

}


void CedaDemandData::GetDemandArrayByGroups(CCSPtrArray<DEMANDLIST> &ropDemandArrays, CCSPtrArray<DEMANDDATA> &ropDemands)
{
    CMapStringToPtr olTmpMap;
	POSITION pos;

	DEMANDDATA *prlDemand;
	CCSPtrArray<DEMANDDATA> *prlArray;

	CString olGoupNames;


	for(int i = ropDemands.GetSize() - 1; i >= 0 ; i--)
	{
		prlDemand = &ropDemands[i];

		olGoupNames = GetGroupNames( prlDemand->Urno);


		if( olTmpMap.Lookup(olGoupNames, (void *&) prlArray) == TRUE)
		{
			prlArray->Add(prlDemand);
		}
		else
		{
			prlArray = new CCSPtrArray<DEMANDDATA>;
			prlArray->Add(prlDemand);
			
			olTmpMap.SetAt(olGoupNames , (void *&) prlArray);
		}
	}

	CString olTmp;

	for( pos = olTmpMap.GetStartPosition(); pos != NULL; )
	{
		olTmpMap.GetNextAssoc( pos, olTmp , (void *&)prlArray );
		
		ropDemandArrays.Add(prlArray);
	}


}






void  CedaDemandData::GetCcas(DEMANDDATA *prlDemand, CStringArray &ropCcas)
{

	RLODATA *prlRlo;

	CCSPtrArray<RLODATA> olArray;
	CString olRloc;		
	CString olGtab;		

	CCSPtrArray<RecordSet> rpRecords;
	RecordSet *prpRecord;

	omRloData.GetRlosByUrud( olArray, prlDemand->Urud);

	for(int i = olArray.GetSize() -1 ; i >= 0; i--)
	{
		prlRlo = &olArray[i];
		
		olGtab = CString(prlRlo->Gtab);
		olRloc = CString(prlRlo->Rloc);
		/*
		olGtab = ogBCD.GetField("SGR", "URNO", CString(prlRlo->Rloc), "GTAB");
		olGtab.TrimLeft();
		olRloc = ogBCD.GetField("SGR", "URNO", CString(prlRlo->Rloc), "RLOC");
		*/
		if(olGtab.IsEmpty())
		{
			// Counter
			ropCcas.Add( ogBCD.GetField("CIC", "URNO", olRloc, "CNAM") );
		}
		else
		{
			//Group
			rpRecords.DeleteAll();
			ogBCD.GetRecords("SGM", "USGR", olRloc, &rpRecords);

			for(int j = rpRecords.GetSize() -1 ; j>= 0; j--)
			{
				prpRecord = &rpRecords[j];
				ropCcas.Add( ogBCD.GetField("CIC", "URNO", prpRecord->Values[ogBCD.GetFieldIndex("SGM","UVAL")] , "CNAM") );
			}
		}
	}

	std::sort(ropCcas.GetData(), ropCcas.GetData() +  ropCcas.GetSize());


}

//todo: its nearly the same as KompressFreq. If ok then join both.
void CedaDemandData::KompressCommonDemands()
{
	CCSPtrArray<DEMANDDATA> olDemands;
	CCSPtrArray<DEMANDDATA> *prlArray;
	CCSPtrArray<DEMANDDATA> *prlArray2;
	DEMANDDATA *prlDemand1;
	DEMANDDATA *prlDemand2;
	POSITION pos;
	void  *pVoid;

	CMapPtrToPtr olTmpMap;
	
	if (!GetCommonDemands(olDemands))
		return;

	while( olDemands.GetSize() > 0 )
	{
		prlDemand1 = &olDemands[0];
		prlDemand1->KKey = ogBasicData.GetNextUrno();
		olDemands.RemoveAt(0);

		if( olTmpMap.Lookup((void *)prlDemand1->KKey, (void *&) prlArray) == TRUE)
		{
			prlArray->Add(prlDemand1);
		}
		else
		{
			prlArray = new CCSPtrArray<DEMANDDATA>;
			prlArray->Add(prlDemand1);
			olTmpMap.SetAt((void *)prlDemand1->KKey , (void *&) prlArray);
		}

		for(int j  = olDemands.GetSize() - 1; j >= 0 ; j--)
		{
			prlDemand2 = &olDemands[j];

			if(CompareCompressCca(prlDemand1, prlDemand2))
			{
				prlDemand2->KKey = prlDemand1->KKey ;
				olDemands.RemoveAt(j);

				if( olTmpMap.Lookup((void *)prlDemand1->KKey, (void *&) prlArray) == TRUE)
				{
					prlArray->Add(prlDemand2);
				}
			}
		}
	}


	////////////////////////////////////////////////////////
	// Freq	

	DEMANDDATA *prlDemandAct;
	DEMANDDATA *prlDemandPrev;
	DEMANDDATA *prlDemandNext;
	COleDateTimeSpan olDiff;
	int ilCount;

	for( pos = olTmpMap.GetStartPosition(); pos != NULL; )
	{
		olTmpMap.GetNextAssoc( pos, pVoid, (void *&)prlArray );
	
		prlArray->Sort(CompareDebe);

		ilCount = prlArray->GetSize();

		//TRACE("\n--------------- ");

		for(int i = 0;  i < ilCount; i++)
		{
			prlDemandAct = &(*prlArray)[i];

			if(i == 0)
				prlDemandPrev = NULL;
			else
				prlDemandPrev = &(*prlArray)[i-1];

			if(i < ilCount -1)
				prlDemandNext = &(*prlArray)[i+1];
			else
				prlDemandNext = NULL;

			
			if( prlDemandPrev == 0)
			{
				prlDemandAct->PrevC = 0;	
			}
			else
			{

				COleDateTime  olTag1( prlDemandAct->Debe.GetYear(), prlDemandAct->Debe.GetMonth(),
					prlDemandAct->Debe.GetDay(), prlDemandAct->Debe.GetHour(), 
					prlDemandAct->Debe.GetMinute(), prlDemandAct->Debe.GetSecond()) ;
				COleDateTime  olTag2( prlDemandPrev->Debe.GetYear(), prlDemandPrev->Debe.GetMonth(),
					prlDemandPrev->Debe.GetDay(), prlDemandPrev->Debe.GetHour(), 
					prlDemandPrev->Debe.GetMinute(), prlDemandPrev->Debe.GetSecond()) ;
				
				olDiff = olTag1 - olTag2 ;
				prlDemandAct->PrevC = olDiff.GetDays();	
			}

			if( prlDemandNext == 0)
			{
				prlDemandAct->NextC = 0;	
			}
			else
			{

				COleDateTime  olTag1( prlDemandAct->Debe.GetYear(), prlDemandAct->Debe.GetMonth(),
					prlDemandAct->Debe.GetDay(), prlDemandAct->Debe.GetHour(), 
					prlDemandAct->Debe.GetMinute(), prlDemandAct->Debe.GetSecond()) ;
				COleDateTime  olTag2( prlDemandNext->Debe.GetYear(), prlDemandNext->Debe.GetMonth(),
					prlDemandNext->Debe.GetDay(), prlDemandNext->Debe.GetHour(), 
					prlDemandNext->Debe.GetMinute(), prlDemandNext->Debe.GetSecond()) ;
				
				olDiff = olTag2 - olTag1;
				prlDemandAct->NextC = olDiff.GetDays();	
			}
			//TRACE("\n%s %s Prev: %d Next: %d Freq: %d ", prlDemandAct->Ckic, prlDemandAct->Debe.Format("%H:%M  %d.%m.%Y"), prlDemandAct->PrevC, prlDemandAct->NextC, prlDemandAct->Freq);  

		}
		//TRACE("\n--------------- ");

	}


	// The really kompressor
	int ilActPeri;
	char buffer[64];
	int ilLastFreq;
	bool blNew = false;
	DEMANDDATA	*prlDemand;

	for( pos = olTmpMap.GetStartPosition(); pos != NULL; )
	{
		olTmpMap.GetNextAssoc( pos, pVoid , (void *&)prlArray );
		
		ilCount = prlArray->GetSize();

		ilActPeri = 1;
		ilLastFreq = 0;		

		itoa(ilActPeri, buffer, 10);
		prlDemand = &(*prlArray)[0];

		prlArray2 = new CCSPtrArray<DEMANDDATA>;
		prlArray2->Add(prlDemand);
		

		prlDemand->KKey = ogBasicData.GetNextUrno();
		omKKeyArray.Add(prlDemand->KKey);
		omKKeyMap.SetAt( (void *)prlDemand->KKey , (void *&) prlArray2);

		prlDemandPrev = prlDemand;
		for(int i = 1;  i < ilCount; i++)
		{
			prlDemand = &(*prlArray)[i];

			if(prlDemand->PrevC > 7 )
			{
				if(((prlDemand->PrevC > prlDemand->NextC) && (prlDemand->NextC > 0)) || ((  ilLastFreq != prlDemand->PrevC )&& (ilLastFreq != 0)))
				{
					ilLastFreq = 0;
					ilActPeri++;
					blNew = true;
				}
				else
				{
					ilLastFreq = prlDemand->PrevC;
					prlDemand->Freq = ilLastFreq;
					prlDemandPrev->Freq = ilLastFreq;
				}
			}
			else
			{
				ilLastFreq = 7;
				prlDemand->Freq = ilLastFreq;
				prlDemandPrev->Freq = ilLastFreq;
			}
			
			if(!blNew)
			{
				if( omKKeyMap.Lookup( ( void *)prlDemandPrev->KKey, (void *&) prlArray2) == TRUE)
				{
					prlArray2->Add(prlDemand);
					prlArray2->Sort(CompareDebe);
					prlDemand->KKey = prlDemandPrev->KKey;

				}
			}
			else
			{
				prlArray2 = new CCSPtrArray<DEMANDDATA>;
				prlArray2->Add(prlDemand);

				prlDemand->KKey = ogBasicData.GetNextUrno();
				omKKeyArray.Add(prlDemand->KKey);
				omKKeyMap.SetAt( (void *) prlDemand->KKey , (void *&) prlArray2);
			}

			blNew = false;
			prlDemandPrev = prlDemand;

		}

	}

	// Clear tmpmap
	for( pos = olTmpMap.GetStartPosition(); pos != NULL; )
	{
		olTmpMap.GetNextAssoc( pos, pVoid, (void *&)prlArray );
		prlArray->RemoveAll();
		delete prlArray;
	}
	olTmpMap.RemoveAll();
}

int	 CedaDemandData::GetFlightsFromCciDemand(DEMANDDATA *prpDemand,CDWordArray& ropFlights)
{
	if (!prpDemand || prpDemand->Dety[0] != CCI_DEMAND)
		return 0;

	char pclFields[128];
	strcpy(pclFields,"URUD,DEBE,DEEN");
	char pclData[2048];

	sprintf(pclData,"%ld,%s,%s",prpDemand->Urud,prpDemand->Debe.Format("%Y%m%d%H%M%S"),prpDemand->Deen.Format("%Y%m%d%H%M%S"));
	if (!CedaAction("CDF","DEM",pclFields,"","",pclData,"BUF1"))
	{
		return 0;
//		ogBasicData.LogCedaError("CCSBasicData::GetFlightsFromCciDemand Error",omLastErrorMessage,"CDF",pclFields,"DEM","");	
	}
	else
	{
		for (int i = 0; i < omDataBuf.GetSize(); i++)
		{
			CString olBuf = omDataBuf[i];
			char *pclToken = strtok(olBuf.GetBuffer(0),"|");
			while(pclToken)
			{
				long llUrno = 0L;
				if (sscanf(pclToken,"%ld",&llUrno) == 1 && llUrno != 0)
				{
					ropFlights.Add(llUrno);
				}
				pclToken = strtok(NULL,"|");
			}
		}
	}
	return ropFlights.GetSize();	
}

CString	 CedaDemandData::GetTextFromCciDemand(long lpDemandUrno)
{
	CString olText;

	DEMANDDATA *prlDemand = GetDemandByUrno(lpDemandUrno);

	if (!prlDemand )
		return CString(olText);
	if (prlDemand->Dety[0] != CCI_DEMAND)
		return CString(olText);

	CDWordArray olFlights;
	if (GetFlightsFromCciDemand(prlDemand, olFlights) > 0)
	{
		for (int ilLc = 0; ilLc < olFlights.GetSize(); ilLc++)
		{
			CCAFLIGHTDATA *prlFlight = ogCcaDiaFlightData.GetFlightByUrno(olFlights[ilLc]);
			if (prlFlight)
			{
				CString olAlc3 = CString(prlFlight->Alc3);

				if (!olAlc3.IsEmpty() && strstr(olText, olAlc3) == NULL)
				{
					olText += olAlc3 + CString(" ");
				}
			}
		}
	}
	olText.TrimLeft();

	return CString(olText);
}

CString	 CedaDemandData::GetTextFromCciDemand(CCSPtrArray<DEMANDDATA> &ropDemands)
{
	CString olText;
	DEMANDDATA *prlDemand;

	for( int j = 0; j < ropDemands.GetSize(); j++)
	{
		prlDemand = &ropDemands[j];

		if (prlDemand && prlDemand->Dety[0] == CCI_DEMAND)
		{

			CDWordArray olFlights;
			if (GetFlightsFromCciDemand(prlDemand, olFlights) > 0)
			{
				for (int ilLc = 0; ilLc < olFlights.GetSize(); ilLc++)
				{
					CCAFLIGHTDATA *prlFlight = ogCcaDiaFlightData.GetFlightByUrno(olFlights[ilLc]);
					if (prlFlight)
					{
						CString olAlc3 = CString(prlFlight->Alc3);

						if (!olAlc3.IsEmpty() && strstr(olText, olAlc3) == NULL)
						{
							olText += olAlc3 + CString(" ");
						}
					}
				}
			}
		}
	}

	olText.TrimLeft();
	return CString(olText);
}
