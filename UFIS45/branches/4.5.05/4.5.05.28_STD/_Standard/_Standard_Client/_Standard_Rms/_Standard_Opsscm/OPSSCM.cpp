// OPSSCM.cpp : Defines the class behaviors for the application.
//

#include <stdafx.h>
#include <OPSSCM.h>
#include <ufis.h>

#include <MainFrm.h>
#include <CedaBasicData.h>
#include <CCSGlobl.h>
#include <LoginDlg.h>
#include <PrivList.h>
#include <RegisterDlg.h>
#include <DataSet.h>
#include <Splash.h>
#include <AatLogin.h>
#include <CedaInitModuData.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


COLORREF CCSClientWnd::lmBkColor = lgBkColor;
COLORREF CCSClientWnd::lmTextColor = lgTextColor;
COLORREF CCSClientWnd::lmHilightColor = lgHilightColor;


static void InitSeasonFlightTableDefaultView()
{
	CViewer olViewer;
	CStringArray olPossibleFilters;
	CStringArray olGeoFilter;

	// Read the default value from the database in the server
	//TRACE("InitPrePlanTableDefaultView\n");

	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("DUMMY");
	olPossibleFilters.Add("GEOMETRIE");
	olViewer.SetViewerKey("CCADIA");
	CString olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	olViewer.SetFilter("GEOMETRIE", olGeoFilter);
	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view


}


/////////////////////////////////////////////////////////////////////////////
// COPSSCMApp

BEGIN_MESSAGE_MAP(COPSSCMApp, CWinApp)
	//{{AFX_MSG_MAP(COPSSCMApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COPSSCMApp construction

COPSSCMApp::COPSSCMApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

COPSSCMApp::~COPSSCMApp()
{

	DeleteBrushes();

}



/////////////////////////////////////////////////////////////////////////////
// The one and only COPSSCMApp object

COPSSCMApp theApp;

/////////////////////////////////////////////////////////////////////////////
// COPSSCMApp initialization

BOOL COPSSCMApp::InitInstance()
{
	// CG: The following block was added by the Splash Screen component.
	{
		/*
		CCommandLineInfo cmdInfo;
		ParseCommandLine(cmdInfo);

		CSplashWnd::EnableSplashScreen(cmdInfo.m_bShowSplash);

		CSplashWnd::ShowSplashScreen();
		*/

	}

	// Stingray Objective Grid
	GXInit(); 

	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}
	
	
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));




	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
	// CCS-Initialization
	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	CTime oltmptime = CTime::GetCurrentTime();
	CString oltmpstr = oltmptime.Format("Datum: %d.%m.%Y");
	of_catch.open("CritErr.txt", ios::app);
	of_catch << oltmpstr.GetBuffer(0) << endl;
	of_catch << "=================" << endl;



	/////////////////////////////////////////////////////////////////////////////
	// Standard CCS-Initialization
	/////////////////////////////////////////////////////////////////////////////

	
	ogLog.SetAppName(ogAppName);
	ogCommHandler.SetAppName(ogAppName);


	// Standard CCS-Fonts and Brushes initialization (see ccsglobl.h)
    InitFont();
	CreateBrushes();
	


	ogBcHandle.SetCloMessage(GetString(IDS_STRING1391));

	
    if (ogCommHandler.Initialize() != true)  // connection error?
    {
    	AfxMessageBox(CString("CedaCom:\n") + ogCommHandler.LastError());
   		ogCommHandler.CleanUpCom();	
		ExitProcess(0);
   		return FALSE;
	}
	




	::UfisDllAdmin("TRACE", "ON", "FATAL");


	// INIT Tablenames and Homeairport
	char pclConfigPath[256];
	char pclUser[256];
	char pclPassword[256];
	char pclDebug[256];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));


	char pclUseBcProxy[256];
	GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclUseBcProxy, sizeof pclUseBcProxy, pclConfigPath);

	//MWO/RRO 25.03.03
	if (stricmp(pclUseBcProxy,"BCPROXY") == 0) 
	{
		ogBcHandle.UseBcProxy();
		ogBcHandle.StopBc();
	}
	else if (stricmp(pclUseBcProxy,"BCCOM") == 0)
	{
		ogBcHandle.UseBcCom();
		ogBcHandle.StopBc();
	}


    GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "HAJ", pcgHome, sizeof pcgHome, pclConfigPath);
    GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "HAJ", pcgTableExt, sizeof pcgTableExt, pclConfigPath);

    GetPrivateProfileString("GLOBAL", "USER", "DEFAULT", pclUser, sizeof pclUser, pclConfigPath);
    GetPrivateProfileString("GLOBAL", "PASSWORD", "DEFAULT", pclPassword, sizeof pclPassword, pclConfigPath);
    GetPrivateProfileString("GLOBAL", "DEBUG", "DEFAULT", pclDebug, sizeof pclDebug, pclConfigPath);

	//ogBcHandle.SetHomeAirport(pcgHome);



	bgDebug = false;

	if(strcmp(pclDebug, "DEFAULT") != 0)
	{
		bgDebug = true;
		of_debug.open( pclDebug, ios::app);
	}




	strcpy(CCSCedaData::pcmTableExt, pcgTableExt);
	strcpy(CCSCedaData::pcmHomeAirport, pcgHome);
	strcpy(CCSCedaData::pcmApplName, ogAppName);
/*
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("SPR"), BC_FLIGHT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("ISF"), BC_FLIGHT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("IFR"), BC_FLIGHT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("UFR"), BC_FLIGHT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("DFR"), BC_FLIGHT_DELETE, false);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("JOF"), BC_FLIGHT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("UPS"), BC_FLIGHT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("UPJ"), BC_FLIGHT_CHANGE, false);
*/
//110102rkr

	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("SPR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("ISF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("IFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("UFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("DFR"), BC_FLIGHT_DELETE, true);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("JOF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("UPS"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("UPJ"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("IRT"), BC_FLIGHT_CHANGE, true);


	ogBcHandle.AddTableCommand(CString("SDB") + CString(pcgTableExt), CString("SPR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("SDB") + CString(pcgTableExt), CString("ISF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("SDB") + CString(pcgTableExt), CString("IFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("SDB") + CString(pcgTableExt), CString("UFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("SDB") + CString(pcgTableExt), CString("DFR"), BC_FLIGHT_DELETE, true);
	ogBcHandle.AddTableCommand(CString("SDB") + CString(pcgTableExt), CString("JOF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("SDB") + CString(pcgTableExt), CString("UPS"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("SDB") + CString(pcgTableExt), CString("UPJ"), BC_FLIGHT_CHANGE, true);

	ogBcHandle.AddTableCommand(CString("ARC") + CString(pcgTableExt), CString("SPR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("ARC") + CString(pcgTableExt), CString("ISF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("ARC") + CString(pcgTableExt), CString("IFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("ARC") + CString(pcgTableExt), CString("UFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("ARC") + CString(pcgTableExt), CString("DFR"), BC_FLIGHT_DELETE, true);
	ogBcHandle.AddTableCommand(CString("ARC") + CString(pcgTableExt), CString("JOF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("ARC") + CString(pcgTableExt), CString("UPS"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("ARC") + CString(pcgTableExt), CString("UPJ"), BC_FLIGHT_CHANGE, true);

/*	ogBcHandle.AddTableCommand(CString(""), CString("SPR"), BC_FLIGHT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString(""), CString("ISF"), BC_FLIGHT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString(""), CString("IFR"), BC_FLIGHT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString(""), CString("UFR"), BC_FLIGHT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString(""), CString("DFR"), BC_FLIGHT_DELETE, false);
	ogBcHandle.AddTableCommand(CString(""), CString("JOF"), BC_FLIGHT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString(""), CString("UPS"), BC_FLIGHT_CHANGE, false);
	ogBcHandle.AddTableCommand(CString(""), CString("UPJ"), BC_FLIGHT_CHANGE, false);
*/
//110102rkr
	ogBcHandle.AddTableCommand(CString(""), CString("SPR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString(""), CString("ISF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString(""), CString("IFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString(""), CString("UFR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString(""), CString("DFR"), BC_FLIGHT_DELETE, true);
	ogBcHandle.AddTableCommand(CString(""), CString("JOF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString(""), CString("UPS"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString(""), CString("UPJ"), BC_FLIGHT_CHANGE, true);


	ogBcHandle.AddTableCommand(CString("CCA") + CString(pcgTableExt), CString("IRT"), BC_CCA_NEW, false,ogAppName);
	ogBcHandle.AddTableCommand(CString("CCA") + CString(pcgTableExt), CString("DRT"), BC_CCA_DELETE, true);
	ogBcHandle.AddTableCommand(CString("CCA") + CString(pcgTableExt), CString("URT"), BC_CCA_CHANGE, true);

	ogBcHandle.AddTableCommand(CString("DEM") + CString(pcgTableExt), CString("IRT"), BC_DEMAND_NEW, true);
	ogBcHandle.AddTableCommand(CString("DEM") + CString(pcgTableExt), CString("DRT"), BC_DEMAND_DELETE, true);
	ogBcHandle.AddTableCommand(CString("DEM") + CString(pcgTableExt), CString("URT"), BC_DEMAND_CHANGE, true);

	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("CFL"), BC_CFL_READY,	 true);

#if	0	
	CLoginDialog olLoginDlg(pcgTableExt,ogAppName,NULL);

	if((strcmp(pclUser, "DEFAULT") != 0) && (strcmp(pclPassword, "DEFAULT") != 0))
	{
		strcpy(pcgUser,pclUser);
		strcpy(pcgPasswd,pclPassword);
		ogBasicData.omUserID = pclUser;
		ogCommHandler.SetUser(pclUser);

		strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);
		strcpy(CCSCedaData::pcmUser, pclUser);


		if(!ogPrivList.Login(pcgTableExt,pclUser,pclPassword,ogAppName))
			return FALSE;
	}
	else
	{
		if( olLoginDlg.DoModal() != IDCANCEL )
		{
			int ilStartApp = IDOK;
			if(ogPrivList.GetStat("InitModu") == '1')
			{
				RegisterDlg olRegisterDlg;
				ilStartApp = olRegisterDlg.DoModal();
			}
			if(ilStartApp != IDOK)
			{
				return FALSE;
			}

		}
		else
		{
			return FALSE;
		}
	}
#else
	CDialog olDummyDlg;	
	olDummyDlg.Create(IDD_ABOUTBOX,NULL);
	CAatLogin olLoginCtrl;
	olLoginCtrl.Create(NULL,WS_CHILD|WS_DISABLED,CRect(0,0,100,100),&olDummyDlg,4711);
	olLoginCtrl.SetInfoButtonVisible(TRUE);
	olLoginCtrl.SetApplicationName(ogAppName);
	olLoginCtrl.SetRegisterApplicationString(ogInitModuData.GetInitModuTxt());
	if((strcmp(pclUser, "DEFAULT") != 0) && (strcmp(pclPassword, "DEFAULT") != 0))
	{
		strcpy(pcgUser,pclUser);
		strcpy(pcgPasswd,pclPassword);
		ogBasicData.omUserID = pclUser;
		ogCommHandler.SetUser(pclUser);

		strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);
		strcpy(CCSCedaData::pcmUser, pclUser);


		if (olLoginCtrl.DoLoginSilentMode(pclUser,pclPassword) != "OK")
			return FALSE;
	}
	else
	{

		if (olLoginCtrl.ShowLoginDialog() != "OK")
		{
			olDummyDlg.DestroyWindow();
			return FALSE;
		}
	}

	strcpy(pcgUser,olLoginCtrl.GetUserName_());
	strcpy(pcgPasswd,olLoginCtrl.GetUserPassword());

	ogCommHandler.SetUser(olLoginCtrl.GetUserName_());
	strcpy(CCSCedaData::pcmUser, olLoginCtrl.GetUserName_());
	strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);

	ogPrivList.Add(olLoginCtrl.GetPrivilegList());

	olDummyDlg.DestroyWindow();

#endif

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));		


    InitialLoad(NULL);



	//Set Homeairport 4 letter code
	CString olApc4;
	ogBCD.GetField("APT","APC3", CString(pcgHome), "APC4", olApc4);
	strcpy(pcgHome4, olApc4);


	ogBasicData.SetLocalDiff();

	CCSCedaData::omLocalDiff1 = ogBasicData.GetLocalDiff1();
	CCSCedaData::omLocalDiff2 = ogBasicData.GetLocalDiff2();
	CCSCedaData::omTich = ogBasicData.GetTich() ;


	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));		





	// To create the main window, this code creates a new frame window
	// object and then sets it as the application's main window object.

	CMainFrame* pFrame = new CMainFrame;
	m_pMainWnd = pFrame;

	// create and load the frame with its resources

	pFrame->LoadFrame(IDR_MAINFRAME,
		WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE | WS_MAXIMIZE   , NULL,
		NULL);


	SetMenuPrivilege(ID_VIEW, "DESKTOP_CB_Daten_Laden");
	SetMenuPrivilege(ID_NOW, "DESKTOP_CB_Jetzt");
	SetMenuPrivilege(ID_DELETE, "DESKTOP_CB_L�schen");
	SetMenuPrivilege(ID_ALLOCATE, "DESKTOP_CB_Einteilen");
	SetMenuPrivilege(ID_WO_DEMAND, "DESKTOP_CB_Fl�ge_ohne_Bedarfe");
	SetMenuPrivilege(ID_DEMAND, "DESKTOP_CB_Bedarfe");
	SetMenuPrivilege(ID_FLIGHT_CONFLICTS, "DESKTOP_CB_Flug_Konflikte");
	SetMenuPrivilege(ID_CONFLICTS, "DESKTOP_CB_Konflikte");
	SetMenuPrivilege(ID_BDPSUIF, "DESKTOP_CB_Stammdaten");
	SetMenuPrivilege(ID_RULES, "DESKTOP_CB_Regelwerk");
	SetMenuPrivilege(ID_PRINT, "DESKTOP_CB_Drucken");
	SetMenuPrivilege(ID_APP_ABOUT, "DESKTOP_CB_Info");
	
	pFrame->DrawMenuBar();

	// The one and only window has been initialized, so show and update it.
	pFrame->ShowWindow(SW_SHOW);
	pFrame->UpdateWindow();

	return TRUE;
}



bool COPSSCMApp::SetMenuPrivilege(int ipCId, const CString &opPrivStr) const {

	char clPrivStat = ogPrivList.GetStat(opPrivStr);
	// Set menu privileges
	/*
	switch (ipCId)
	{
	case ID_VIEW: clPrivStat='-'; break;
	case ID_DEMAND: clPrivStat='-'; break;
	case ID_PRINT: clPrivStat='0'; break;
	}
	*/
	
	if (clPrivStat == '-')
	{
		CMenu *polMenu = m_pMainWnd->GetMenu();
		if (polMenu)
		{
			polMenu->RemoveMenu(ipCId, MF_BYCOMMAND);
		}
	}
	return true;
}





/////////////////////////////////////////////////////////////////////////////
// COPSSCMApp message handlers





/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
		// No message handlers
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void COPSSCMApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// COPSSCMApp message handlers



void COPSSCMApp::InitialLoad(CWnd *pParent)
{
	CCS_TRY

	//CTime olTime = CTime::GetCurrentTime();
	//TRACE("\n\nbeginn InitialLoad  %s", olTime.Format("%H:%M:%S"));

	pogInitialLoad = new CInitialLoadDlg(pParent); 

	int ilGant = 1;

	ogBCD.SetTableExtension(CString(pcgTableExt));
	ogCcaDiaFlightData.omCcaData.SetTableName(CString("CCA") + CString(pcgTableExt));


	//ogBCD.SetSystabErrorMsg( GetString(IDS_STRING1656), GetString(ST_FEHLER ) );




//////////////////////////////////////////////////////////////////////////////////////

	pogInitialLoad->SetRange(13);
	// Fluggesellschaften
	pogInitialLoad->SetProgress(ilGant);
	pogInitialLoad->SetMessage(GetString(IDS_STRING954));
	ogBCD.SetObject("ALT", "URNO,ALC2,ALC3,ALFN,TERM");
	ogBCD.SetObjectDesc("ALT", GetString(IDS_STRING955));
	ogBCD.SetTableHeader("ALT", "ALFN", GetString(IDS_STRING985));
	ogBCD.SetTableHeader("ALT", "ALC2", GetString(IDS_STRING987));
	ogBCD.SetTableHeader("ALT", "ALC3", GetString(IDS_STRING988));
	ogBCD.AddKeyMap("ALT", "ALC2");
	ogBCD.AddKeyMap("ALT", "ALC3");
	ogBCD.Read(CString("ALT"));
	pogInitialLoad->SetProgress(ilGant);
	//ogBCD.SetDdxType(CString("ALT"), "IRT", S_FLIGHT_INSERT);
	//ogBCD.SetDdxType(CString("ALT"), "DRT", S_FLIGHT_DELETE);
	//ogBCD.SetDdxType(CString("ALT"), "URT", S_FLIGHT_UPDATE);
	
	ogCfgData.ReadUfisCedaConfig();


	// Flugzeugtypen
	pogInitialLoad->SetMessage(GetString(IDS_STRING997));
	ogBCD.SetObject("ACT", "URNO,ACT3,ACT5,ACFN,SEAT");
	ogBCD.SetObjectDesc("ACT", GetString(IDS_STRING998));
	ogBCD.SetTableHeader("ACT", "ACFN", GetString(IDS_STRING985));
	ogBCD.SetTableHeader("ACT", "ACT3", GetString(IDS_STRING988));
	ogBCD.SetTableHeader("ACT", "ACT5", GetString(IDS_STRING991));
	ogBCD.AddKeyMap("ACT", "ACT5");
	ogBCD.AddKeyMap("ACT", "ACT3");
	ogBCD.Read(CString("ACT"));
	pogInitialLoad->SetProgress(ilGant);


	// Flugh�fen
	pogInitialLoad->SetMessage(GetString(IDS_STRING999));
	ogBCD.SetObject("APT", "URNO,APC3,APC4,APFN,TDI1,TDI2,TICH,APTT");
	ogBCD.SetObjectDesc("APT", GetString(IDS_STRING990));
	ogBCD.SetTableHeader("APT", "APFN", GetString(IDS_STRING985));
	ogBCD.SetTableHeader("APT", "APC3", GetString(IDS_STRING988));
	ogBCD.SetTableHeader("APT", "APC4", GetString(IDS_STRING989));
	ogBCD.AddKeyMap("APT", "APC3");
	ogBCD.AddKeyMap("APT", "APC4");
	ogBCD.Read(CString("APT"));
	pogInitialLoad->SetProgress(ilGant);


	// Checkin Schalter
	pogInitialLoad->SetMessage(GetString(IDS_STRING1011));
	ogBCD.SetObject("CIC", "URNO,TERM,CNAM,NAFR,NATO,HALL,CICR,VATO,VAFR,RGBL");
	ogBCD.SetObjectDesc("CIC", GetString(IDS_STRING1012));
	ogBCD.SetTableHeader("CIC", "TERM", GetString(IDS_STRING1001));
	ogBCD.SetTableHeader("CIC", "CNAM", GetString(IDS_STRING986));
	ogBCD.AddKeyMap("CIC", "CNAM");
	ogBCD.Read(CString("CIC"));

	ogBCD.SetDdxType("CIC", "IRT", BCD_CIC_CHANGE);
	ogBCD.SetDdxType("CIC", "DRT", BCD_CIC_CHANGE);
	ogBCD.SetDdxType("CIC", "URT", BCD_CIC_CHANGE);

	pogInitialLoad->SetProgress(ilGant);



	// Season
	pogInitialLoad->SetMessage(GetString(IDS_STRING1028));
	ogBCD.SetObject("SEA", "URNO,SEAS,VPFR,VPTO");
	ogBCD.SetObjectDesc("SEA", GetString(IDS_STRING1007));
	ogBCD.AddKeyMap("SEA", "SEAS");
	ogBCD.Read(CString("SEA"));
	pogInitialLoad->SetProgress(ilGant);


	//Checkin-Counter Blockierungen
	ogBCD.SetObject("BLK", "BURN,DAYS,NAFR,NATO,RESN,TABN,TYPE,TIFR,TITO,URNO");
	ogBCD.SetObjectDesc("BLK", "Blockade");
	ogBCD.Read("BLK");
	pogInitialLoad->SetProgress(ilGant);

	ogBCD.SetDdxType("BLK", "IRT", BLK_CHANGE);
	ogBCD.SetDdxType("BLK", "DRT", BLK_CHANGE);
	ogBCD.SetDdxType("BLK", "URT", BLK_CHANGE);
	pogInitialLoad->SetProgress(ilGant);
//########################

	// Gep�ckb�nder
	pogInitialLoad->SetMessage(GetString(IDS_STRING1000));
	ogBCD.SetObject("BLT", "URNO,TERM,BNAM,NAFR,NATO,MAXF,DEFD,BLTT");
	ogBCD.SetTableHeader("BLT", "TERM", GetString(IDS_STRING1001));
	ogBCD.SetTableHeader("BLT", "BNAM", GetString(IDS_STRING986));
	ogBCD.SetObjectDesc("BLT", GetString(IDS_STRING1010));
	ogBCD.AddKeyMap("BLT", "BNAM");

	ogBCD.SetDdxType("BLT", "IRT", BCD_BLT_CHANGE);
	ogBCD.SetDdxType("BLT", "DRT", BCD_BLT_CHANGE);
	ogBCD.SetDdxType("BLT", "URT", BCD_BLT_CHANGE);
	
	
	ogBCD.Read(CString("BLT"));
	pogInitialLoad->SetProgress(ilGant);


	// Ausg�nge
	pogInitialLoad->SetMessage(GetString(IDS_STRING1013));
	ogBCD.SetObject("EXT", "URNO,ENAM,TERM");
	ogBCD.SetObjectDesc("EXT", GetString(IDS_STRING1014));
	ogBCD.SetTableHeader("EXT", "TERM", GetString(IDS_STRING1001));
	ogBCD.SetTableHeader("EXT", "ENAM", GetString(IDS_STRING986));
	ogBCD.AddKeyMap("EXT", "ENAM");
	ogBCD.Read(CString("EXT"));
	pogInitialLoad->SetProgress(ilGant);

	// Fids Remarks
	pogInitialLoad->SetMessage(GetString(IDS_STRING1015));
	CString olFIDFields;
	olFIDFields.Format("URNO,%s,CODE", ogFIDRemarkField);
	ogBCD.SetObject("FID", olFIDFields);
	ogBCD.SetObjectDesc("FID", GetString(IDS_STRING1016));
	ogBCD.SetTableHeader("FID", ogFIDRemarkField, GetString(IDS_STRING985));
	ogBCD.SetTableHeader("FID", "CODE", GetString(IDS_STRING1017));
	ogBCD.AddKeyMap("FID", "CODE");
	ogBCD.Read(CString("FID"));

	
	ogBCD.SetObject("FID_CIC", "FID", olFIDFields);
	ogBCD.SetObjectDesc("FID_CIC", GetString(IDS_STRING1016));
	ogBCD.SetTableHeader("FID_CIC", ogFIDRemarkField, GetString(IDS_STRING985));
	ogBCD.SetTableHeader("FID_CIC", "CODE", GetString(IDS_STRING1017));
	ogBCD.AddKeyMap("FID_CIC", "CODE");
	ogBCD.Read(CString("FID_CIC"), "WHERE REMT = 'C'");
	
		
	pogInitialLoad->SetProgress(ilGant);



	// Gates
	pogInitialLoad->SetMessage(GetString(IDS_STRING1018));
	ogBCD.SetObject("GAT", "URNO,GNAM,TERM,NAFR,NATO,RGA1,RGA2,RBAB,RESB,BUSG,GATR,DEFD");
	ogBCD.SetObjectDesc("GAT", GetString(IDS_STRING1019));
	ogBCD.SetTableHeader("GAT", "TERM", GetString(IDS_STRING1001));
	ogBCD.SetTableHeader("GAT", "GNAM", GetString(IDS_STRING986));
	ogBCD.AddKeyMap("GAT", "GNAM");
	ogBCD.AddKeyMap("GAT", "RGA1");

	ogBCD.SetDdxType("GAT", "IRT", BCD_GAT_CHANGE);
	ogBCD.SetDdxType("GAT", "DRT", BCD_GAT_CHANGE);
	ogBCD.SetDdxType("GAT", "URT", BCD_GAT_CHANGE);


	ogBCD.Read(CString("GAT"));
	pogInitialLoad->SetProgress(ilGant);

	// Abfertigungsarten
	pogInitialLoad->SetMessage(GetString(IDS_STRING1020));
	ogBCD.SetObject("HTY", "URNO,HNAM,HTYP");
	ogBCD.SetTableHeader("HTY", "HTYP", GetString(IDS_STRING1009));
	ogBCD.SetTableHeader("HTY", "HNAM", GetString(IDS_STRING985));
	ogBCD.SetObjectDesc("HTY", GetString(IDS_STRING1021));
	ogBCD.AddKeyMap("HTY", "HTYP");
	ogBCD.Read(CString("HTY"));
	pogInitialLoad->SetProgress(ilGant);

	// Verkehrsarten
	pogInitialLoad->SetMessage(GetString(IDS_STRING1022));
	ogBCD.SetObject("NAT", "URNO,TNAM,TTYP,ALGA,ALPO");
	ogBCD.SetTableHeader("NAT", "TTYP", GetString(IDS_STRING1009));
	ogBCD.SetTableHeader("NAT", "TNAM", GetString(IDS_STRING985));
	ogBCD.SetObjectDesc("NAT", GetString(IDS_STRING1023));
	ogBCD.AddKeyMap("NAT", "TTYP");
	ogBCD.Read(CString("NAT"));

	ogBCD.SetDdxType("NAT", "IRT", BCD_NAT_CHANGE);
	ogBCD.SetDdxType("NAT", "DRT", BCD_NAT_CHANGE);
	ogBCD.SetDdxType("NAT", "URT", BCD_NAT_CHANGE);
	
	
	pogInitialLoad->SetProgress(ilGant);

	// Positionen
	pogInitialLoad->SetMessage(GetString(IDS_STRING1024));
	ogBCD.SetObject("PST", "URNO,PNAM,RESN,NAFR,NATO,CDAT,POSR,VATO,VAFR,TAXI,GPUS,DEFD,BRGS");
	ogBCD.SetObjectDesc("PST", GetString(IDS_STRING1025));
	ogBCD.SetTableHeader("PST", "PNAM", GetString(IDS_STRING986));
	ogBCD.AddKeyMap("PST", "PNAM");

	ogBCD.SetDdxType("PST", "IRT", BCD_PST_CHANGE);
	ogBCD.SetDdxType("PST", "DRT", BCD_PST_CHANGE);
	ogBCD.SetDdxType("PST", "URT", BCD_PST_CHANGE);


	ogBCD.Read(CString("PST"));
	pogInitialLoad->SetProgress(ilGant);

	// Servicetypen
	pogInitialLoad->SetMessage(GetString(IDS_STRING1006));
	ogBCD.SetObject("STY", "URNO,SNAM,STYP");
	ogBCD.SetObjectDesc("STY", GetString(IDS_STRING1008));
	ogBCD.SetTableHeader("STY", "SNAM", GetString(IDS_STRING986));
	ogBCD.SetTableHeader("STY", "STYP", GetString(IDS_STRING1009));
	ogBCD.AddKeyMap("STY", "STYP");
	ogBCD.Read(CString("STY"));
	pogInitialLoad->SetProgress(ilGant);


	// Warter�ume
	pogInitialLoad->SetMessage(GetString(IDS_STRING1003));
	ogBCD.SetObject("WRO", "URNO,WNAM,TERM,NAFR,NATO,GTE1,GTE2,DEFD,MAXF");
	ogBCD.SetObjectDesc("WRO", GetString(IDS_STRING1002));
	ogBCD.SetTableHeader("WRO", "WNAM", GetString(IDS_STRING986));
	ogBCD.SetTableHeader("WRO", "TERM", GetString(IDS_STRING1001));
	ogBCD.AddKeyMap("WRO", "WNAM");
	ogBCD.AddKeyMap("WRO", "GTE1");

	ogBCD.SetDdxType("WRO", "IRT", BCD_WRO_CHANGE);
	ogBCD.SetDdxType("WRO", "DRT", BCD_WRO_CHANGE);
	ogBCD.SetDdxType("WRO", "URT", BCD_WRO_CHANGE);
	
	
	ogBCD.Read(CString("WRO"));
	pogInitialLoad->SetProgress(ilGant);

//#########################
	// Flug-Konflikte
	TRACE("COPSSCMApp::InitialLoad: Read Flightconflicts\n");
	pogInitialLoad->SetMessage(GetString(IDS_STRING1689));

	CString olLkey = ogCfgData.GetLkey();
	CString olSel = CString("WHERE LKEY='") + olLkey + CString("'");

	ogBCD.SetObject("LBL", "TKEY,TEXT");
	ogBCD.AddKeyMap("LBL", "URNO");
	ogBCD.AddKeyMap("LBL", "TKEY");
	ogBCD.Read(CString("LBL"), olSel);

	ogCflData.ReadAllCfl(true, false);
	pogInitialLoad->SetProgress(ilGant);

	// Gruppenname
	pogInitialLoad->SetMessage(GetString(IDS_STRING1444));
	ogBCD.SetObject("SGR");
	ogBCD.Read(CString("SGR"));
	pogInitialLoad->SetProgress(ilGant);

	ogBCD.SetObject("SGM");
	ogBCD.Read(CString("SGM"));
	pogInitialLoad->SetProgress(ilGant);


	ogBCD.SetObject("RUD", "RETY,URNO,URUE");
	ogBCD.Read(CString("RUD"));
	pogInitialLoad->SetProgress(ilGant);

	ogBCD.SetObject("RUE");
	ogBCD.Read(CString("RUE"));
	pogInitialLoad->SetProgress(ilGant);



	ogCcaDiaFlightData.omDemandData.omRloData.ReadAll();

	/////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////
	CString olTable;


	/////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////



	// Konfigurationsdaten
	pogInitialLoad->SetMessage(GetString(IDS_STRING1004));
	//ogCfgData.GetFlightConfig();
	ogCfgData.ReadCfgData();
	ogCfgData.SetCfgData();
	//ogCfgData.ReadMonitorSetup();
	//ogCfgData.ReadFontSetup();
	
	pogInitialLoad->SetProgress(ilGant);

	// Ansichten
	pogInitialLoad->SetMessage(GetString(IDS_STRING1005));
	InitSeasonFlightTableDefaultView();
	pogInitialLoad->SetProgress(ilGant);




	if (pogInitialLoad != NULL)
	{
		pogInitialLoad->DestroyWindow();
		pogInitialLoad = NULL;
	}


	CCS_CATCH_ALL

	//olTime = CTime::GetCurrentTime();
	//TRACE("\n\nend    InitialLoad  %s", olTime.Format("%H:%M:%S")); 

}

BOOL COPSSCMApp::PreTranslateMessage(MSG* pMsg)
{
	// CG: The following lines were added by the Splash Screen component.
	if (CSplashWnd::PreTranslateAppMessage(pMsg))
		return TRUE;

	return CWinApp::PreTranslateMessage(pMsg);
}
