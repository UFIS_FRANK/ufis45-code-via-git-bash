#ifndef AFX_REPORTSEASONTABLEDLG_H__CDB257E5_8BF5_11D1_8127_0000B43C4B01__INCLUDED_
#define AFX_REPORTSEASONTABLEDLG_H__CDB257E5_8BF5_11D1_8127_0000B43C4B01__INCLUDED_

// ReportSeasonTableDlg.h : Header-Datei
//
//#include "CCSTable.h" 
#include <ReportSeasonTableViewer.h>


/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportSeasonTableDlg 

class CReportSeasonTableDlg : public CDialog
{
// Konstruktion
public:
	CReportSeasonTableDlg(CWnd *popParent, CcaDiagramViewer &ropViewer);
	~CReportSeasonTableDlg();

// Dialogfelddaten
	//{{AFX_DATA(CReportSeasonTableDlg)
	enum { IDD = IDD_REPORTTABLE };
	//CButton	m_CB_Beenden;
	CButton	m_RB_Printer;
	CButton	m_RB_File;
	//}}AFX_DATA
	CString omHeadline;	//Dialogbox-Überschrift


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CReportSeasonTableDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

public:

	int imTopSpace;
	
	ReportSeasonTableViewer omReportViewer;

	CGXGridWnd *pomReportGXGrid;

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CReportSeasonTableDlg)
	virtual BOOL OnInitDialog();
 	afx_msg void OnDrucken();
 	afx_msg void OnBeenden();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	const int imDlgBorder;
	CcaDiagramViewer &romViewer;

private:
 	bool bmIsCreated;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_REPORTSEASONTABLEDLG_H__CDB257E5_8BF5_11D1_8127_0000B43C4B01__INCLUDED_
