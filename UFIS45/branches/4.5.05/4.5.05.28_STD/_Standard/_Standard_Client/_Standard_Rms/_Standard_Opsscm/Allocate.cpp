
#include <StdAfx.h>
#include <CCSGlobl.h>
#include <Allocate.h>
#include <CedaDiaCcaData.h>
#include <CedaGrmData.h>


#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsWithIn(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))





Allocate::Allocate(int ipMode)
{
	//imMode = ipMode; //0 = Alle 1 = nur Offene
}


Allocate::~Allocate()
{

}




void Allocate::Run()
{
	CCSPtrArray<DEMANDDATA> rlDemands;
	CCSPtrArray<DEMANDDATA> *prlDemands;
	CCAFLIGHTDATA *prlFlight;

	bool blHasCcas;

	if(!bgCcaDiaSeason)
	{	
		for( int i = ogCcaDiaFlightData.omData.GetSize() -1 ; i >= 0; i--)
		{
			prlFlight = &ogCcaDiaFlightData.omData[i];
			blHasCcas = false;
			if(strcmp(prlFlight->Adid, "D") == 0)
			{
				rlDemands.RemoveAll();
				ogCcaDiaFlightData.omDemandData.GetDemansByOuro( rlDemands, prlFlight->Urno);

				for(int j = rlDemands.GetSize()-1; j >= 0; j--)
				{
					if(ogCcaDiaFlightData.omCcaData.ExistCca(rlDemands[j].Urno))
					{
						blHasCcas = true;
						break;
					}
				}

				if(!blHasCcas)
				{
					AllocateDemands(rlDemands, prlFlight);
				}
			}
		}
	}
	else
	{

		CCSPtrArray<CCAFLIGHTDATA> *prlArray;
		CCSPtrArray<DEMANDLIST> rlDemandArrays;
		POSITION pos;
		CString olTmp;

		for( pos = ogCcaDiaFlightData.omKompMap.GetStartPosition(); pos != NULL; )
		{
			ogCcaDiaFlightData.omKompMap.GetNextAssoc( pos, olTmp , (void *&)prlArray );

			blHasCcas = false;

			for(int k = 0; k < prlArray->GetSize() && !blHasCcas; k++)
			{
				prlFlight = &(*prlArray)[k];

				prlDemands = new CCSPtrArray<DEMANDDATA> ;

				rlDemandArrays.Add(prlDemands);

				ogCcaDiaFlightData.omDemandData.GetDemansByOuro( *prlDemands, prlFlight->Urno);

				for(int j = prlDemands->GetSize()-1; j >= 0; j--)
				{
					if(ogCcaDiaFlightData.omCcaData.ExistCca( (*prlDemands)[j].Urno))
					{
						blHasCcas = true;
						break;
					}
				}
			}

			if(!blHasCcas)
			{
				AllocateDemands(rlDemandArrays, *prlArray);
			}

			rlDemandArrays.DeleteAll();



		}

	}

}





void Allocate::AllocateDemands( CCSPtrArray<DEMANDLIST> &rlDemandArrays, CCSPtrArray<CCAFLIGHTDATA> &rlFlightArray)
{
	DEMANDDATA *prlDemand;
	DEMANDDATA *prlDemand2;
	CString olCcaList;
	CString olCnam;
	CStringArray olCcas;
	bool blTreffer = true;


	CCSPtrArray<DIACCADATA> olNewCcas;
	CCSPtrArray<DIACCADATA> olTmpCcas;


	CCSPtrArray<DEMANDDATA> *polDemands;
	CCSPtrArray<DEMANDDATA> *polDemands2;


	polDemands = &rlDemandArrays[0];


	CCAFLIGHTDATA *prlFlight = &rlFlightArray[0];
	CCAFLIGHTDATA *prlFlight2;



	for(int i = polDemands->GetSize() - 1; i >= 0 && blTreffer; i--)
	{
		prlDemand = &(*polDemands)[i];

		ogCcaDiaFlightData.omDemandData.GetCcas(prlDemand, olCcas);

		blTreffer = true;

		for(int j = olCcas.GetSize() -1; j >= 0; j--)
		{
			olCnam = olCcas[j];

			if(olCcaList.Find(olCnam) < 0)
			{

				for(int k = 0; k < rlDemandArrays.GetSize(); k++)
				{
					polDemands2 = &rlDemandArrays[k];
					prlDemand2 = &(*polDemands)[i];

					if(!ogCcaDiaFlightData.omCcaData.Check(olCnam, prlDemand2->Debe, prlDemand2->Deen))
					{
						blTreffer = false;
						break;
					}
				}


				if(blTreffer)
				{

					for(int k = 0; k < rlDemandArrays.GetSize(); k++)
					{
						polDemands2 = &rlDemandArrays[k];
						prlDemand2 = &(*polDemands2)[i];

						prlFlight2 = &rlFlightArray[k];

						DIACCADATA *prlCca = new DIACCADATA;

						prlCca->Cdat = CTime::GetCurrentTime();
						strcpy(prlCca->Usec, pcgUser);
						prlCca->Flnu = prlFlight2->Urno;
						prlCca->Urno = ogBasicData.GetNextUrno();
						strcpy(prlCca->Ctyp, " ");
						strcpy(prlCca->Ckic, olCnam);
						prlCca->Ckbs = prlDemand2->Debe;
						prlCca->Ckes = prlDemand2->Deen;
						prlCca->Ghpu = prlDemand2->Urno;
						prlCca->IsSelected = false;
						prlCca->IsChanged = DATA_NEW;

						strcpy(prlCca->Act3, prlFlight2->Act3);
						strcpy(prlCca->Flno, prlFlight2->Flno);

						olNewCcas.Add(prlCca);
					}
					olCcaList += CString(",") + olCnam;
					break;
				}
			}
		}
	}

	if(!blTreffer)
	{
		olNewCcas.DeleteAll();
	}
	else
	{
		ogCcaDiaFlightData.omCcaData.Release(&olNewCcas, true, false);
	}

}










void Allocate::AllocateDemands( CCSPtrArray<DEMANDDATA> &ropDemands, CCAFLIGHTDATA *prpFlight)
{
	DEMANDDATA *prlDemand;
	CString olCcaList;
	CString olCnam;
	CStringArray olCcas;
	bool blTreffer = true;


	CCSPtrArray<DIACCADATA> olNewCcas;

	for(int i = ropDemands.GetSize() - 1; i >= 0 && blTreffer; i--)
	{
		prlDemand = &ropDemands[i];

		ogCcaDiaFlightData.omDemandData.GetCcas(prlDemand, olCcas);

		blTreffer = false;

		for(int j = olCcas.GetSize() -1; j >= 0; j--)
		{
			olCnam = olCcas[j];

			if(olCcaList.Find(olCnam) < 0)
			{

				if(ogCcaDiaFlightData.omCcaData.Check(olCnam, prlDemand->Debe, prlDemand->Deen))
				{
					blTreffer = true;

					DIACCADATA *prlCca = new DIACCADATA;

					prlCca->Cdat = CTime::GetCurrentTime();
					strcpy(prlCca->Usec, pcgUser);
					prlCca->Flnu = prpFlight->Urno;
					prlCca->Urno = ogBasicData.GetNextUrno();
					strcpy(prlCca->Ctyp, " ");
					strcpy(prlCca->Ckic, olCnam);
					prlCca->Ckbs = prlDemand->Debe;
					prlCca->Ckes = prlDemand->Deen;
					prlCca->Ghpu = prlDemand->Urno;
					prlCca->IsSelected = false;
					prlCca->IsChanged = DATA_NEW;

					strcpy(prlCca->Act3, prpFlight->Act3);
					strcpy(prlCca->Flno, prpFlight->Flno);

					olNewCcas.Add(prlCca);

					olCcaList += CString(",") + olCnam;
					break;
				}
			}
		}
	}

	if(!blTreffer)
	{
		olNewCcas.DeleteAll();
	}
	else
	{
		ogCcaDiaFlightData.omCcaData.Release(&olNewCcas, true, false);
	}


}


