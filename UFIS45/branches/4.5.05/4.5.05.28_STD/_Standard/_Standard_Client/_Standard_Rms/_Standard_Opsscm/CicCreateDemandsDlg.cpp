// CicCreateDemandsDlg.cpp : implementation file
//

#include <stdafx.h>
#include <OPSSCM.h>
#include <CicCreateDemandsDlg.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CicCreateDemandsDlg dialog


CicCreateDemandsDlg::CicCreateDemandsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CicCreateDemandsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CicCreateDemandsDlg)
	m_SToTimeVal = _T("");
	m_SToDateVal = _T("");
	m_SFromDateVal = _T("");
	m_SFromTimeVal = _T("");
	m_LstuDateVal = _T("");
	m_FAirlineVal = _T("");
	m_FlightNrVal = _T("");
	m_FlightSuffixVal = _T("");
	m_Act3Val = _T("");
	m_RegnVal = _T("");
	m_AirpVal = _T("");
	m_Airp2Val = _T("");
	m_RemaVal = _T("");
	m_BetriebVal = FALSE;
	m_PlanungVal = FALSE;
	m_LstuTimeVal = _T("");
	//}}AFX_DATA_INIT
}

void CicCreateDemandsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CicCreateDemandsDlg)
	DDX_Text(pDX, IDC_STOTIME, m_SToTimeVal);
	DDV_MaxChars(pDX, m_SToTimeVal, 5);
	DDX_Text(pDX, IDC_STODATE, m_SToDateVal);
	DDV_MaxChars(pDX, m_SToDateVal, 10);
	DDX_Text(pDX, IDC_SFROMDATE, m_SFromDateVal);
	DDV_MaxChars(pDX, m_SFromDateVal, 10);
	DDX_Text(pDX, IDC_SFROMTIME, m_SFromTimeVal);
	DDV_MaxChars(pDX, m_SFromTimeVal, 5);
	DDX_Text(pDX, IDC_LSTUDATE, m_LstuDateVal);
	DDX_Text(pDX, IDC_FLIGHTAIRLINE, m_FAirlineVal);
	DDX_Text(pDX, IDC_FLIGHTNR, m_FlightNrVal);
	DDX_Text(pDX, IDC_FLIGHTSUFFIX, m_FlightSuffixVal);
	DDX_Text(pDX, IDC_ACT3, m_Act3Val);
	DDX_Text(pDX, IDC_REGN, m_RegnVal);
	DDX_Text(pDX, IDC_AIRP, m_AirpVal);
	DDX_Text(pDX, IDC_AIRP2, m_Airp2Val);
	DDX_Text(pDX, IDC_REMA, m_RemaVal);
	DDX_Check(pDX, IDC_BETRIEB, m_BetriebVal);
	DDX_Check(pDX, IDC_PLANUNG, m_PlanungVal);
	DDX_Text(pDX, IDC_LSTUTIME, m_LstuTimeVal);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CicCreateDemandsDlg, CDialog)
	//{{AFX_MSG_MAP(CicCreateDemandsDlg)
	ON_BN_CLICKED(ID_CREATE_DEMANDS_OK, OnCreateDemandsOk)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



bool CicCreateDemandsDlg::GetSearchText(CString &ropWhere) 
{
	CString olT1,olT2,olT3;
	CString olTmp; 
	CTime olDate = TIMENULL;
	CTime olTime = TIMENULL;
	m_SToTimeVal;
	m_SToDateVal;
	m_SFromDateVal;
	m_SFromTimeVal;

	ropWhere = "";
	CString olTmpFtypWhere;
	//TRACE("SFromDate=%s SFromTime=%s SToDate=%s SToTime=%s\n", m_SFromDateVal, m_SFromTimeVal, m_SToDateVal, m_SToTimeVal);
	if(m_SFromDateVal != "" && m_SFromTimeVal != "")
	{
		olDate = DateStringToDate(m_SFromDateVal);
		if(olDate != TIMENULL)
			olT1 = olDate.Format("%Y%m%d");
		else
		{
			CString olText, olAdd;
			olText.LoadString(IDS_NO_DATE_TIME);
			olAdd.LoadString(IDS_WARNING);
			MessageBox(olText, olAdd, MB_OK);
			return false;
		}
		if(olDate != TIMENULL)
		{
			olTime = HourStringToDate(m_SFromTimeVal, olDate);
			if(olTime != TIMENULL)
				olT1 = olDate.Format("%Y%m%d") + olTime.Format("%H%M%S");
			else
			{
				CString olText, olAdd;
				olText.LoadString(IDS_NO_DATE_TIME);
				olAdd.LoadString(IDS_WARNING);
				MessageBox(olText, olAdd, MB_OK);
				return false;
			}
		}
	}
	else
	{
		CString olText, olAdd;
		olText.LoadString(IDS_NO_DATE_TIME);
		olAdd.LoadString(IDS_WARNING);
		MessageBox(olText, olAdd, MB_OK);
		return false;
	}		
	if(m_SToDateVal != "" && m_SToTimeVal != "")
	{
		olDate = DateStringToDate(m_SToDateVal);
		if(olDate != TIMENULL)
			olT2 = olDate.Format("%Y%m%d");
		else
		{
			CString olText, olAdd;
			olText.LoadString(IDS_NO_DATE_TIME);
			olAdd.LoadString(IDS_WARNING);
			MessageBox(olText, olAdd, MB_OK);
			return false;
		}
		if(olDate != TIMENULL)
		{
			olTime = HourStringToDate(m_SToTimeVal, olDate);
			if(olTime != TIMENULL)
				olT2 = olDate.Format("%Y%m%d") + olTime.Format("%H%M%S");
			else
			{
				CString olText, olAdd;
				olText.LoadString(IDS_NO_DATE_TIME);
				olAdd.LoadString(IDS_WARNING);
				MessageBox(olText, olAdd, MB_OK);
				return false;
			}
		}
	}
	else
	{
		CString olText, olAdd;
		olText.LoadString(IDS_NO_DATE_TIME);
		olAdd.LoadString(IDS_WARNING);
		MessageBox(olText, olAdd, MB_OK);
		return false;
	}

	if(m_LstuDateVal != "" && m_LstuTimeVal != "")
	{
		olDate = DateStringToDate(m_LstuDateVal);
		if(olDate != TIMENULL)
			olT3 = olDate.Format("%Y%m%d");
		else
		{
			CString olText, olAdd;
			olText.LoadString(IDS_NO_DATE_TIME);
			olAdd.LoadString(IDS_WARNING);
			MessageBox(olText, olAdd, MB_OK);
			return false;
		}
		if(olDate != TIMENULL)
		{
			olTime = HourStringToDate(m_LstuTimeVal, olDate);
			if(olTime != TIMENULL)
				olT3 = olDate.Format("%Y%m%d") + olTime.Format("%H%M%S");
			else
			{
				CString olText, olAdd;
				olText.LoadString(IDS_NO_DATE_TIME);
				olAdd.LoadString(IDS_WARNING);
				MessageBox(olText, olAdd, MB_OK);
				return false;
			}
		}
	}
	else
	{
	  olT3 = "";
	}

					
	bool blOr = false;
	bool blAnd = false;

	CTime olTime1 = CTime::GetCurrentTime();
	CTime olTime2 = CTime::GetCurrentTime();
	//RST!
	olTime1 = DBStringToDateTime(olT1);
	olTime2 = DBStringToDateTime(olT2);
	ogBasicData.LocalToUtc(olTime1);
	ogBasicData.LocalToUtc(olTime2);

	ropWhere +=  olTime2.Format("TIFA <= '%Y%m%d%H%M00' AND ") + olTime1.Format("TIFD >= '%Y%m%d%H%M00'");
	blAnd = true;
	if(!olT3.IsEmpty())
	{
		CTime olTime3 = DBStringToDateTime(olT3);
		ogBasicData.LocalToUtc(olTime3);
		if(	blAnd )
			ropWhere += " AND ";
		blAnd = true;
		ropWhere +=  olTime3.Format(" LSTU >= '%Y%m%d%H%M00' ");
	}

	if(m_FAirlineVal.IsEmpty() == FALSE)
	{
		m_FAirlineVal.MakeUpper();
		if(	blAnd )
			ropWhere += " AND ";
		blAnd = true;
		olTmp = m_FAirlineVal;
		if(m_FAirlineVal.Find("*") > -1)
		{	
			olTmp.Replace("*","%");
			olTmp.Replace("?","_");
			ropWhere += "(ALC2 LIKE '" ;
			ropWhere += olTmp +"' OR ALC3 LIKE '" ;
			ropWhere +=  olTmp + "')";
		}
		else if(m_FAirlineVal.Find("?") > -1)
		{
			olTmp.Replace("?","_");
			if(olTmp.GetLength() > 2)
			{
				ropWhere += "ALC3 LIKE '" + olTmp +"'"; 
			}
			else
			{
				ropWhere += "ALC2 LIKE '" + olTmp +"'"; 
			}
		}
		else
		{
			if(olTmp.GetLength() > 2)
			{
				ropWhere += "ALC3 = '" + olTmp +"'"; 
			}
			else
			{
				ropWhere += "ALC2 = '" + olTmp +"'"; 
			}
		}
	}
	if(m_FlightNrVal.IsEmpty() == FALSE)
	{
		if(	blAnd )
			ropWhere += " AND ";
		blAnd = true;
		olTmp = m_FlightNrVal;
		if(olTmp.Find("*") > -1 || olTmp.Find("?") > -1)
		{	
			olTmp.Replace("*","%");
			olTmp.Replace("?","_");
			ropWhere += "FLTN LIKE '" + olTmp + CString("' ");
		}
		else
		{
			ropWhere += "FLTN = '" + olTmp + CString("' ");
		}

	}
	if(m_FlightSuffixVal.IsEmpty() == FALSE)
	{
		if(	blAnd )
			ropWhere += " AND ";
		blAnd = true;
		olTmp = m_FlightSuffixVal;
		if(olTmp.Find("*") > -1 || olTmp.Find("?") > -1)
		{	
			olTmp.Replace("*","%");
			olTmp.Replace("?","_");
			ropWhere += "FLNS LIKE '" + olTmp + CString("' ");
		}
		else
		{
			ropWhere += "FLNS = '" + olTmp + CString("' ");
		}

	}

	if(m_Act3Val.IsEmpty() == FALSE)
	{
		if(	blAnd )
			ropWhere += " AND ";
		blAnd = true;
		olTmp = m_Act3Val;
		if(olTmp.Find("*") > -1)
		{	
			olTmp.Replace("*","%");
			olTmp.Replace("?","_");
			ropWhere += "(ACT3 LIKE '" ;
			ropWhere += olTmp +"' OR ACT5 LIKE '" ;
			ropWhere +=  olTmp + "')";
		}
		else if(olTmp.Find("?") > -1)
		{
			olTmp.Replace("?","_");
			if(olTmp.GetLength() > 3)
			{
				ropWhere += "ACT5 LIKE '" + olTmp +"'"; 
			}
			else
			{
				ropWhere += "ACT3 LIKE '" + olTmp +"'"; 
			}
		}
		else
		{
			if(olTmp.GetLength() > 3)
			{
				ropWhere += "ACT5 = '" + olTmp +"'"; 
			}
			else
			{
				ropWhere += "ACT3 = '" + olTmp +"'"; 
			}
		}
	}
	if(m_RegnVal.IsEmpty() == FALSE)
	{
		if(	blAnd )
			ropWhere += " AND ";
		blAnd = true;
		olTmp = m_RegnVal;
		if(olTmp.Find("*") > -1 || olTmp.Find("?") > -1)
		{	
			olTmp.Replace("*","%");
			olTmp.Replace("?","_");
			ropWhere += "REGN LIKE '" + olTmp + CString("' ");
		}
		else
		{
			ropWhere += "REGN = '" + olTmp + CString("' ");
		}

	}
	
	if(m_AirpVal.IsEmpty() == FALSE) // Origin
	{
		if(	blAnd )
			ropWhere += " AND ";
		blAnd = true;
		olTmp = m_AirpVal;
		if(olTmp.Find("*") > -1)
		{	
			olTmp.Replace("*","%");
			olTmp.Replace("?","_");
			ropWhere += "(ORG3 LIKE '" ;
			ropWhere += olTmp +"' OR ORG4 LIKE '" ;
			ropWhere +=  olTmp + "')";
		}
		else if(olTmp.Find("?") > -1)
		{
			olTmp.Replace("?","_");
			if(olTmp.GetLength() > 3)
			{
				ropWhere += "ORG4 LIKE '" + olTmp +"'"; 
			}
			else
			{
				ropWhere += "ORG3 LIKE '" + olTmp +"'"; 
			}
		}
		else
		{
			if(olTmp.GetLength() > 3)
			{
				ropWhere += "ORG4 = '" + olTmp +"'"; 
			}
			else
			{
				ropWhere += "ORG3 = '" + olTmp +"'"; 
			}
		}
	}
	if(m_Airp2Val.IsEmpty() == FALSE) // Dest
	{
		if(	blAnd )
			ropWhere += " AND ";
		blAnd = true;
		olTmp = m_Airp2Val;
		if(olTmp.Find("*") > -1)
		{	
			olTmp.Replace("*","%");
			olTmp.Replace("?","_");
			ropWhere += "(DES3 LIKE '" ;
			ropWhere += olTmp +"' OR DES4 LIKE '" ;
			ropWhere +=  olTmp + "')";
		}
		else if(olTmp.Find("?") > -1)
		{
			olTmp.Replace("?","_");
			if(olTmp.GetLength() > 3)
			{
				ropWhere += "DES4 LIKE '" + olTmp +"'"; 
			}
			else
			{
				ropWhere += "DES3 LIKE '" + olTmp +"'"; 
			}
		}
		else
		{
			if(olTmp.GetLength() > 3)
			{
				ropWhere += "DES4 = '" + olTmp +"'"; 
			}
			else
			{
				ropWhere += "DES3 = '" + olTmp +"'"; 
			}
		}
	}
	if(m_RemaVal.IsEmpty() == FALSE)
	{
		if(	blAnd )
			ropWhere += " AND ";
		blAnd = true;
		olTmp = m_RemaVal;
		if(olTmp.Find("*") > -1 || olTmp.Find("?") > -1)
		{	
			olTmp.Replace("*","%");
			olTmp.Replace("?","_");
			ropWhere += "REM1 LIKE '" + olTmp + CString("' ");
		}
		else
		{
			ropWhere += "REM1 = '" + olTmp + CString("' ");
		}

	}
	
	olTmpFtypWhere = "";
	if(m_BetriebVal == TRUE)
	{	
		if(olTmpFtypWhere.IsEmpty())					
			olTmpFtypWhere += CString("FTYP IN ('O','R','D'");
		else
			olTmpFtypWhere += CString(",'O','R','D'");
	}
	if(m_PlanungVal == TRUE)
	{	
		if(olTmpFtypWhere.IsEmpty())					
			olTmpFtypWhere += CString("FTYP IN ('S'");
		else
			olTmpFtypWhere += CString(",'S'");
	}
	if(olTmpFtypWhere.IsEmpty())					
		olTmpFtypWhere += CString("FTYP IN ('X','N'");
	else
		olTmpFtypWhere += CString(",'X','N'");

	if(!ropWhere.IsEmpty())
	{
		CString olTmp;
		olTmp = CString("(") + ropWhere + CString(")");
		ropWhere = olTmp;
	}

	if(!olTmpFtypWhere.IsEmpty())
	{
		CString olTmp;
		olTmp = CString("(") + olTmpFtypWhere + CString("))");

		if(ropWhere.IsEmpty())
			ropWhere = olTmp;
		else
			ropWhere = ropWhere + CString(" AND ") + olTmp;
	}

	return true;
}



/////////////////////////////////////////////////////////////////////////////
// CicCreateDemandsDlg message handlers


void CicCreateDemandsDlg::OnCreateDemandsOk() 
{
	UpdateData(TRUE);
	if(!GetSearchText(omSearchText))
	{
		return;
	}

	omSearchText += "     [ROTATIONS]";
	
	OnOK();
}
