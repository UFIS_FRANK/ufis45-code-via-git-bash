// ChildView.h : interface of the CChildView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CHILDVIEW_H__1D5121CA_9846_11D3_97CB_00008630DDD6__INCLUDED_)
#define AFX_CHILDVIEW_H__1D5121CA_9846_11D3_97CB_00008630DDD6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include <CcaDiaViewer.h>
#include <CCSClientWnd.h>
#include <CCS3dStatic.h>
#include <CCSButtonCtrl.h>
#include <CCSTimeScale.h>
#include <CViewer.h>


/////////////////////////////////////////////////////////////////////////////
// CChildView window

class CChildView : public CWnd
{
// Construction
public:
	CChildView();



	void SetWndPos();
    void PositionChild();
    void SetTSStartTime(CTime opTSStartTime);
	void UpdateComboBox();
	void ChangeViewTo(const char *pcpViewName,bool RememberPositions = true);
	void OnUpdatePrevNext(void);
	void PrePlanMode(BOOL bpToSet,CTime opPrePlanTime);
	void ToggleOnline(void);
    void OnFirstChart();
    void OnLastChart();
	bool IsPassFlightFilter(CCAFLIGHTDATA *prpFlight);
	bool HasAllocatedCcas(CCSPtrArray<DIACCADATA> &ropCcaList);

	void ActivateTables();

// Attributes
public:

	CcaDiagramViewer omViewer;

	CString omOldWhere;
	void LoadFlights(char *pspView = NULL);

    CCS3DStatic omTime;
    CCS3DStatic omDate;
    CCS3DStatic omTSDate;

    CButton m_KlebeFkt;
    CCSTimeScale omTimeScale;
    CBitmapButton omBB1, omBB2;
    
    CCSClientWnd omClientWnd;
    CStatusBar omStatusBar;
    
    CTime omStartTime;
    CTimeSpan omDuration;
    CTime omTSStartTime;
    CTimeSpan omTSDuration;
    CTimeSpan omTSInterval;
    
    CPtrArray omPtrArray;
    int imFirstVisibleChart;
    int imStartTimeScalePos;
    
	CString omCaptionText;
	bool bmIsViewOpen;


	int imWhichMonitor;
	int imMonitorCount;


// Redisplay all methods for DDX call back function
public:
	BOOL bmNoUpdatesNow;

	void RedisplayAll();
	CListBox *GetBottomMostGantt();
// Time Band data, use the TIMENULL value for hiding the marker
public:
	void SetTimeBand(CTime opStartTime, CTime opEndTime);
	void UpdateTimeBand();
	void SetCaptionText(void);
	void SetMarkTime(CTime opStartTime, CTime opEndTime);
private:
	CTime omTimeBandStartTime;
	CTime omTimeBandEndTime;

private:
    CPoint omMaxTrackSize;
    CPoint omMinTrackSize;
    COLORREF lmBkColor;
    COLORREF lmTextColor;
    COLORREF lmHilightColor;










// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChildView)
	public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CChildView();

	// Generated message map functions
protected:
	//{{AFX_MSG(CChildView)
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHILDVIEW_H__1D5121CA_9846_11D3_97CB_00008630DDD6__INCLUDED_)
