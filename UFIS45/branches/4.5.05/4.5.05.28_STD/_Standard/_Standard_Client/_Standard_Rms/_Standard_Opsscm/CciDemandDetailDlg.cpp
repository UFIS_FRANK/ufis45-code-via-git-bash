// CciDemandDetailDlg.cpp : implementation file
//

#include <stdafx.h>
#include <CciDemandDetailDlg.h>
#include <CedaDemandData.h>
#include <CcaCedaFlightData.h>
#include <CedaRloData.h>
#include <BasicData.h>
#include <SeasonDlg.h>
#include <CedaBasicData.h>

/*
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
*/
/////////////////////////////////////////////////////////////////////////////
// CciDemandDetailDlg dialog
static void ProcessDemandCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);


//CciDemandDetailDlg::CciDemandDetailDlg(CWnd* pParent,long lpDemUrno,char* opCounterName)
CciDemandDetailDlg::CciDemandDetailDlg(CWnd* pParent,CUIntArray &lpDemUrno,char* opCounterName)
: CDialog()
{
	//{{AFX_DATA_INIT(CciDemandDetailDlg)
	m_RuleName = _T("");
	//}}AFX_DATA_INIT

	lmDemUrno.RemoveAll();
	lmDemUrno.Copy(lpDemUrno);

	omTable = new CCSTable;
	omCounter = opCounterName;

	Create(CciDemandDetailDlg::IDD,pParent);

	Register();
}


void CciDemandDetailDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CciDemandDetailDlg)
	DDX_Text(pDX, IDC_EDIT_RULENAME, m_RuleName);
	//}}AFX_DATA_MAP
}

void CciDemandDetailDlg::Register(void)
{
	ogDdx.Register((void *)this,DEMAND_CHANGE,	CString("DEMANDDATA"), CString("Cca-changed"),	ProcessDemandCf);
	ogDdx.Register((void *)this,CCA_FLIGHT_CHANGE,	CString("CCAFLIGHTDATA"), CString("Cca-Flight-changed"),ProcessDemandCf);
}

static void  ProcessDemandCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
    CciDemandDetailDlg *polDlg = (CciDemandDetailDlg *)popInstance;

	if (ipDDXType == DEMAND_CHANGE)
	{
        polDlg->ProcessDemandChange((long *)vpDataPointer);
	}

	if (ipDDXType == CCA_FLIGHT_CHANGE)
	{
        polDlg->ProcessFlightChange((CCAFLIGHTDATA *)vpDataPointer);
	}
}

void CciDemandDetailDlg::ProcessDemandChange(const long *plpDemUrno)
{

	CCS_TRY

	for (int ilDem = 0; ilDem < lmDemUrno.GetSize(); ilDem++)
	{
		if (lmDemUrno[ilDem] == *plpDemUrno)
		{
			UpdateDisplay();
			break;
		}
	}

	CCS_CATCH_ALL
}

void CciDemandDetailDlg::ProcessFlightChange(const CCAFLIGHTDATA *plpFlight)
{

	CCS_TRY

	DEMANDDATA *polDem = NULL;
	for (int ilDem = 0; ilDem < lmDemUrno.GetSize(); ilDem++)
	{
		if ( polDem = ogCcaDiaFlightData.omDemandData.GetDemandByUrno(lmDemUrno[ilDem]) )
		{
			CDWordArray olFlights;
			if (polDem->Dety[0] == CCI_DEMAND)
				ogCcaDiaFlightData.omDemandData.GetFlightsFromCciDemand(polDem,olFlights);
			else if (polDem->Dety[0] == OUTBOUND_DEMAND)
				olFlights.Add(polDem->Ouro);


			for (int ilLc = 0; ilLc < olFlights.GetSize(); ilLc++)
			{
				if ( plpFlight->Urno == olFlights[ilLc] )
				{
					UpdateDisplay();
					break;
				}
			}
		}
	}

	CCS_CATCH_ALL

}

void CciDemandDetailDlg::UpdateDisplay()
{

	omTable->ResetContent();
	DEMANDDATA *polDem = NULL;
	for (int ilDem = 0; ilDem < lmDemUrno.GetSize(); ilDem++)
	{
		if ( polDem = ogCcaDiaFlightData.omDemandData.GetDemandByUrno(lmDemUrno[ilDem]) )
		{
			CDWordArray olFlights;
			if (polDem->Dety[0] == CCI_DEMAND)
				ogCcaDiaFlightData.omDemandData.GetFlightsFromCciDemand(polDem,olFlights);
			else if (polDem->Dety[0] == OUTBOUND_DEMAND)
				olFlights.Add(polDem->Ouro);


			CCSPtrArray<TABLE_COLUMN> olColList;
			TABLE_COLUMN rlColumnData;

			rlColumnData.VerticalSeparator = SEPA_NONE;
			rlColumnData.SeparatorType = SEPA_NONE;
			rlColumnData.Font = &ogCourier_Regular_10;

			CCSEDIT_ATTRIB rlAttrib;
			rlAttrib.Style = ES_UPPERCASE;

			for (int ilLc = 0; ilLc < olFlights.GetSize(); ilLc++)
			{
				CCAFLIGHTDATA *prlFlight = ogCcaDiaFlightData.GetFlightByUrno(olFlights[ilLc]);
				if (prlFlight)
				{
					if(bgGatPosLocal) ogBasicData.UtcToLocal(prlFlight->Stod);

					// Flugnummer
					rlColumnData.Text = prlFlight->Flno;
					olColList.NewAt(olColList.GetSize(), rlColumnData);

					// Datum
					rlColumnData.Text = prlFlight->Stod.Format("%d.%m.%y");
					olColList.NewAt(olColList.GetSize(), rlColumnData);

					// Uhrzeit
					rlColumnData.Text = prlFlight->Stod.Format("%H:%M");
					olColList.NewAt(olColList.GetSize(), rlColumnData);

					// Aircraft
					CString olStr = CString(prlFlight->Act3) + "/" + CString(prlFlight->Act5);
					rlColumnData.Text = olStr;
					olColList.NewAt(olColList.GetSize(), rlColumnData);

					// Nature
					rlColumnData.Text = prlFlight->Ttyp;
					olColList.NewAt(olColList.GetSize(), rlColumnData);

					omTable->AddTextLine(olColList, (void*)prlFlight->Urno/*NULL*/);
					olColList.DeleteAll();

					if(bgGatPosLocal) ogBasicData.LocalToUtc(prlFlight->Stod);
				}
			}
		}
	}

	// Update the table content in the display
	if (polDem)
	{
		char buffer[64];
		ltoa(polDem->Urud, buffer, 10);

		CString olRueUrno = ogBCD.GetField("RUD", "URNO", CString(buffer),"URUE");
		CString olRuleName = ogBCD.GetField("RUE", "URNO", olRueUrno,"RUNA");

		m_RuleName = olRuleName;
		UpdateData(FALSE);
	}

	DrawHeader();
	omTable->DisplayTable();
}

CString CciDemandDetailDlg::Format(CCAFLIGHTDATA *prpFlight)
{
    CString olText;
	if (!prpFlight)
	    return olText;

	olText = prpFlight->Flno;

	return olText;
}


BEGIN_MESSAGE_MAP(CciDemandDetailDlg, CDialog)
	//{{AFX_MSG_MAP(CciDemandDetailDlg)
	ON_BN_CLICKED(IDC_SCHLIEBEN, OnSchlieben)
	ON_WM_CLOSE()
	ON_WM_DESTROY()
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
    ON_MESSAGE(WM_TABLE_RETURN_PRESSED, OnTableLButtonDblclk)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CciDemandDetailDlg message handlers

BOOL CciDemandDetailDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	InitTable();
	if (omCounter == "")
		SetWindowText(GetString(IDS_STRING_DEPLIST));
	else
		SetWindowText(GetString(IDS_STRING_DEPLIST) + ": " + omCounter);
	
	// TODO: Add extra initialization here
    UpdateDisplay();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CciDemandDetailDlg::OnSchlieben() 
{
	// TODO: Add your control notification handler code here
	this->DestroyWindow();	
}

void CciDemandDetailDlg::PostNcDestroy() 
{
	// TODO: Add your specialized code here and/or call the base class
	CDialog::PostNcDestroy();
	ogDdx.UnRegister((void *)this,DEMAND_CHANGE);
	ogDdx.UnRegister((void *)this,CCA_FLIGHT_CHANGE);
	delete omTable;
	delete this;
}

void CciDemandDetailDlg::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	this->DestroyWindow();	
}

void CciDemandDetailDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	
}

void CciDemandDetailDlg::InitTable()
{

	// TODO: Add your control notification handler code here
	CCS_TRY

	CRect olRectBorder;
	CWnd *pWnd = GetDlgItem(IDC_STATIC_FLIGHTS);
	ASSERT(pWnd);
	pWnd->GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

	omTable->SetHeaderSpacing(0);
    omTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	omTable->SetSelectMode(0);
	omTable->SetShowSelection(true);
	omTable->ResetContent();

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	
	TABLE_HEADER_COLUMN rlHeader;

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Font = &ogCourier_Regular_10;

	// Flugnummer
	rlHeader.Length = 10;
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// Datum
	rlHeader.Length = 10; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// Uhrzeit
	rlHeader.Length = 10; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// Aircraft
	rlHeader.Length = 10; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	// Nature
	rlHeader.Length = 10; 
	rlHeader.Text = CString("");
	omHeaderDataArray.New(rlHeader);
	
	omTable->SetHeaderFields(omHeaderDataArray);
	omTable->SetDefaultSeparator();
	omTable->SetTableEditable(false);
	omHeaderDataArray.DeleteAll();

	omTable->DisplayTable();

	CCS_CATCH_ALL
	
}


void CciDemandDetailDlg::DrawHeader()
{

	CCS_TRY

	int  ilTotalLines = 0;
	bool blNewLogicLine = false;

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;
	TABLE_HEADER_COLUMN *prlHeader[6];
	int ilFontFactor=9;

	// Flugnummer
	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 10*ilFontFactor;
	prlHeader[0]->Font = &ogCourier_Bold_10;
	prlHeader[0]->Text = GetString(IDS_STRING_FLNO);
	// Datum
	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 10*ilFontFactor;
	prlHeader[1]->Font = &ogCourier_Bold_10;
	prlHeader[1]->Text = GetString(IDS_STRING_DATE);
	// Uhrzeit
	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 10*ilFontFactor;
	prlHeader[2]->Font = &ogCourier_Bold_10;
	prlHeader[2]->Text = GetString(IDS_STRING_TIME);
	// Aircraft
	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 10*ilFontFactor;
	prlHeader[3]->Font = &ogCourier_Bold_10;
	prlHeader[3]->Text = GetString(IDS_STRING_ACT);
	// Nature
	prlHeader[4] = new TABLE_HEADER_COLUMN;
	prlHeader[4]->Alignment = COLALIGN_CENTER;
	prlHeader[4]->Length = 10*ilFontFactor;
	prlHeader[4]->Font = &ogCourier_Bold_10;
	prlHeader[4]->Text = GetString(IDS_STRING_NATURE);

	for(int ili = 0; ili < 5; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}

	omTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	omTable->SetDefaultSeparator();

	CCS_CATCH_ALL
	
}

LONG CciDemandDetailDlg::OnTableLButtonDblclk( UINT wParam, LPARAM lParam)
{
	CCSTABLENOTIFY *prlNotify = (CCSTABLENOTIFY*)lParam;

	UINT ipItem = wParam;
	long ilUrno = 0;
	ilUrno = (long)omTable->GetTextLineData(ipItem);

	CCAFLIGHTDATA *prlFlight = ogCcaDiaFlightData.GetFlightByUrno(ilUrno);
	if (prlFlight)
	{
		if (pogSeasonDlg)
			pogSeasonDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, DLG_CHANGE, bgGatPosLocal);
	}

	return 0L;

}
