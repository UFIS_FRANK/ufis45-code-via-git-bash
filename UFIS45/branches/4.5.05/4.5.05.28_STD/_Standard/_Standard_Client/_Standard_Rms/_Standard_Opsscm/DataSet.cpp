// dataset.cpp - Class for handle each user action
//
// Description:
// This module is separated from some parts of viewers in many diagrams and
// expected to contain the routines for removing dependencies among Ceda????Data
// classes.
//
// The concept is simple. If we want a new kind of job creation or any action
// that the user can perform on the system, we create a new method here, in this
// module. Then, if that action has some more consequents actions (assign a job
// to solve a demand will change the color of the flight, for example) we will
// provide that steps here, in this module too.
//
// Some actions may require the user to make an interactive decision. To aid
// such concept, the first parameter of every functions will be "popParentWindow",
// and every function here will return IDOK or IDYES on success, IDCANCEL or IDNO
// if the user cancel this operation. The difference between IDCANCEL and IDNO is
// the user may select IDCANCEL when he/she still want to resume some data entry
// in the previous dialogs if exist. But IDNO selection will totally cancel this
// operation, including the in-process dialogs if exist.
//
// Function in this module:
// (still incomplete and need keep on working).
//
//
// Written by:
// Damkerng Thammathakerngkit   July 6, 1996
//

#include <stdafx.h>

#include <DataSet.h>
#include <CCSDDX.h>
#include <CCSGlobl.h>
#include <BasicData.h>
#include <CedaBasicData.h>
#include <CedaDiaCcaData.h>







#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))



static int CompareRulesPrio(const PREMIS_ORDER **e1, const PREMIS_ORDER **e2)
{
	if((**e1).UserPrio == (**e2).UserPrio)
	{
		return (CString((**e1).Prio) == CString((**e2).Prio))? 0:
			(CString((**e1).Prio) > CString((**e2).Prio))? -1: 1;
	}
	else
	{
		return ( (**e1).UserPrio == (**e2).UserPrio)? 0:
			( (**e1).UserPrio > (**e2).UserPrio ) ? -1: 1;
	}

}


static int CompareCcaForCkbs(const DIACCADATA **e1, const DIACCADATA **e2)
{
	return ((**e1).Ckbs == (**e2).Ckbs)? 0: ((**e1).Ckbs < (**e2).Ckbs)? 1: -1;
}


DataSet::DataSet()
{
}

DataSet::~DataSet()
{
}























bool DataSet::DeAssignKKeyCca(long lpCcaKKey)
{

	CCSPtrArray<DIACCADATA> olCcaList;
	DIACCADATA *prlCca;
	//long llFlnu;
	bool blRet = false;

	DEMANDDATA *prlDemand;

	long llDemandKKey;

	if( ogCcaDiaFlightData.omCcaData.GetCcasByKKey(olCcaList, lpCcaKKey) )
	{
		for( int i = 0; i < olCcaList.GetSize(); i++)
		{
			prlCca = &olCcaList[i];
			prlCca->IsChanged = DATA_DELETED;
			
			prlDemand = ogCcaDiaFlightData.omDemandData.GetDemandByUrno(prlCca->Ghpu);
			
			if(prlDemand)
			{
			
				llDemandKKey = prlDemand->KKey;
			}
		}
		ogCcaDiaFlightData.omCcaData.Release(&olCcaList, true);
		
		ogDdx.DataChanged((void *)this, CCA_KKEY_CHANGE,(void *)&lpCcaKKey ); //Update Viewer
		ogDdx.DataChanged((void *)this, DEMAND_KKEY_CHANGE,(void *)&llDemandKKey ); //Update Viewer
	
		olCcaList.RemoveAll();
	}
	return blRet;
}



bool DataSet::DeAssignCca(long lpCcaUrno)
{
	bool blRet = false;
	DIACCADATA *prlCca = ogCcaDiaFlightData.omCcaData.GetCcaByUrno(lpCcaUrno);
	if(prlCca != NULL)
	{
		prlCca->IsChanged = DATA_DELETED;
		ogCcaDiaFlightData.omCcaData.Save(prlCca); //RST!
		ogCcaDiaFlightData.omCcaData.DeleteInternal(prlCca);
	}
	return blRet;
}


void DataSet::GetConflictBrushByCcaStatus(DIACCADATA *prpCca, CcaDIA_BARDATA *prlBar)
{
	prlBar->MarkerBrush = ogBrushs[IDX_AQUA];
	prlBar->TextColor = COLORREF(BLACK);

	if(prpCca->IsSelected == true)
	{
		prlBar->MarkerBrush = ogBrushs[5];
		prlBar->TextColor = COLORREF(WHITE);
	}
	else
	{
		if(CString(prpCca->Ctyp) == "N") // Not Available
		{
			if((CString(prpCca->Disp).Find("g�ltig von") >= 0) ||  (CString(prpCca->Disp).Find("g�ltig bis") >= 0))
			{
				prlBar->MarkerBrush = pogNotValidBrush;
			}
			else
			{
				prlBar->MarkerBrush = pogNotAvailBrush;
			}
		}

		if(CString(prpCca->Ctyp) == "C") // Common
		{
/*				if(prpCca->Ckea != TIMENULL)
				{
					prlBar->MarkerBrush = ogBrushs[IDX_GRAY];
					return;
				}

				if(prpCca->Ckba != TIMENULL)
				{
					prlBar->MarkerBrush = ogBrushs[IDX_LIME];
					return;
				}
*/
			prlBar->MarkerBrush = ogBrushs[IDX_TEAL];//prlBar->MarkerBrush;

			bool blConf = false;	
			
			if(prpCca->Stat[2] == 'J' && prpCca->Stat[3] == '0')//Overlapped
			{
				blConf = true;
				prlBar->MarkerBrush = ogBrushs[IDX_RED];
				return;
			}
			

			if(prpCca->Stat[0] == 'J') //Wrong Allocation
			{
				blConf = true;
				if(prpCca->Stat[1] == 'J') //Conflict Confirmed
				{
					prlBar->MarkerBrush = ogBrushs[IDX_FUCHSIA];
				}
				else //Not Confirmed
				{
					prlBar->MarkerBrush = ogBrushs[IDX_RED];
				}
				return;
			}
			else if(prpCca->Stat[2] == 'J')//Overlapped
			{
				blConf = true;
				prlBar->MarkerBrush = ogBrushs[IDX_RED];
			}
			else if(prpCca->Stat[4] == 'J')//FLNO changed
			{
				blConf = true;
				prlBar->MarkerBrush = ogBrushs[IDX_NAVY];
				prlBar->TextColor = COLORREF(WHITE);
			}
			else if(prpCca->Stat[5] == 'J')//Without demand
			{
				blConf = true;
				prlBar->MarkerBrush = ogBrushs[IDX_LIME];
				prlBar->TextColor = COLORREF(WHITE);
			}
			else if(prpCca->Stat[6] == 'J')//Rule changed
			{
				blConf = true;
				prlBar->MarkerBrush = ogBrushs[IDX_OLIVE];
				prlBar->TextColor = COLORREF(WHITE);
			}
			else if(prpCca->Stat[7] == 'J')//Act changed
			{
				blConf = true;
				prlBar->MarkerBrush = ogBrushs[IDX_MAROON];
				prlBar->TextColor = COLORREF(WHITE);
			}
			else if(prpCca->Stat[8] == 'J')//STD changed
			{
				blConf = true;
				prlBar->MarkerBrush = ogBrushs[IDX_PURPLE];
				prlBar->TextColor = COLORREF(WHITE);
			}

			if(prpCca->Stat[3] == 'J') //Conflict Confirmed
			{
				prlBar->MarkerBrush = ogBrushs[IDX_ORANGE];
			}

			return;
		}

		if(CString(prpCca->Ctyp) == "") 
		{
			bool blConf = false;	

			if(prpCca->Stat[2] == 'J' && prpCca->Stat[3] == '0')//Overlapped
			{
				blConf = true;
				prlBar->MarkerBrush = ogBrushs[IDX_RED];
				return;
			}
			

			if(prpCca->Stat[0] == 'J') //Wrong Allocation
			{
				blConf = true;
				if(prpCca->Stat[1] == 'J') //Conflict Confirmed
				{
					prlBar->MarkerBrush = ogBrushs[IDX_FUCHSIA];
				}
				else //Not Confirmed
				{
					prlBar->MarkerBrush = ogBrushs[IDX_RED];
				}
				return;
			}
			else if(prpCca->Stat[2] == 'J')//Overlapped
			{
				blConf = true;
				prlBar->MarkerBrush = ogBrushs[IDX_RED];
			}
			else if(prpCca->Stat[4] == 'J')//FLNO changed
			{
				blConf = true;
				prlBar->MarkerBrush = ogBrushs[IDX_NAVY];
				prlBar->TextColor = COLORREF(WHITE);
			}
			else if(prpCca->Stat[5] == 'J')//Without demand
			{
				blConf = true;
				prlBar->MarkerBrush = ogBrushs[IDX_LIME];
				prlBar->TextColor = COLORREF(WHITE);
			}
			else if(prpCca->Stat[6] == 'J')//Rule changed
			{
				blConf = true;
				prlBar->MarkerBrush = ogBrushs[IDX_OLIVE];
				prlBar->TextColor = COLORREF(WHITE);
			}
			else if(prpCca->Stat[7] == 'J')//Act changed
			{
				blConf = true;
				prlBar->MarkerBrush = ogBrushs[IDX_MAROON];
				prlBar->TextColor = COLORREF(WHITE);
			}
			else if(prpCca->Stat[8] == 'J')//STD changed
			{
				blConf = true;
				prlBar->MarkerBrush = ogBrushs[IDX_PURPLE];
				prlBar->TextColor = COLORREF(WHITE);
			}

			if(prpCca->Stat[3] == 'J') //Conflict Confirmed
			{
				prlBar->MarkerBrush = ogBrushs[IDX_ORANGE];
			}

		}
	}
}


int DataSet::GetFistCcaConflict(DIACCADATA *prpCca)
{
/*
	int ilConflIdx = -1;
	int ilLen = 10;
	int ilCurrIdx=0;
	bool blEnd = false;
	while(ilCurrIdx < ilLen && blEnd == false)
	{
		if(prpCca->Stat[ilCurrIdx] == 'J')
		{
			blEnd = true;
			ilConflIdx = ilCurrIdx;
		}
		ilCurrIdx = ilCurrIdx+2;
	}
	return ilConflIdx;
*/

	int ilConflIdx = -1;
	int ilLen = 10;
	int ilCurrIdx=2;
	bool blEnd = false;
	while(ilCurrIdx < ilLen && blEnd == false)
	{
		if(prpCca->Stat[ilCurrIdx] == 'J')
		{
			blEnd = true;
			ilConflIdx = ilCurrIdx;
		}
		if (ilCurrIdx == 2)
			ilCurrIdx = ilCurrIdx+2;
		else
			ilCurrIdx = ilCurrIdx+1;
	}
	return ilConflIdx;

/*
	int ilConflIdx = -1;
	int ilLen = 10;
	int ilCurrIdx=0;
	bool blEnd = false;
	while(ilCurrIdx < ilLen && blEnd == false)
	{
		if(prpCca->Stat[ilCurrIdx] == 'J')
		{
			blEnd = true;
			ilConflIdx = ilCurrIdx;
		}
		if (ilCurrIdx == 0 || ilCurrIdx == 2) 
			ilCurrIdx = ilCurrIdx+2;
		else
			ilCurrIdx = ilCurrIdx+1;
	}
	return ilConflIdx;
*/

}


bool DataSet::CheckKompCCaForOverlapping(CString opCkic)
{
	bool blChangesFound = false;
	CCSPtrArray<DIACCADATA> olCcaList;
	CCSPtrArray<DIACCADATA> olSaveCcaList;
	CCSPtrArray<DIACCADATA> olCcas;
	int ilCount;

	ogCcaDiaFlightData.omCcaData.GetCcasByCkic(olCcaList,opCkic);	
	ilCount = olCcas.GetSize();

	/*
	for( int i = 0; i < ilCount; i++)
	{	
		ogCcaDiaFlightData.omCcaData.GetCcasByKKey(olCcaList, olCcas[i].KKey);
	}
	*/


	ilCount = olCcaList.GetSize();
	
	olCcaList.Sort(CompareCcaForCkbs);

	for( int i = 0; i < ilCount; i++)
	{
		olSaveCcaList.New( olCcaList[i]);
		olCcaList[i].Stat[2] = '0';
	}

	DIACCADATA *prlActualCca;
	DIACCADATA *prlPreviousCca = NULL;
	
	
	for( i = 0; i < ilCount; i++)
	{
		prlActualCca = &olCcaList[i];

		for(int j = i + 1; j < ilCount; j++)
		{
			if(i != j)
			{
				prlPreviousCca = &olCcaList[j];

				if(prlPreviousCca->Urno != prlActualCca->Urno)
				{
					if(IsReallyOverlapped(prlPreviousCca->Ckbs,prlPreviousCca->Ckes, prlActualCca->Ckbs,prlActualCca->Ckes) || 
						IsWithIn(prlPreviousCca->Ckbs,prlPreviousCca->Ckes, prlActualCca->Ckbs,prlActualCca->Ckes) || 	
						IsWithIn(prlActualCca->Ckbs,prlActualCca->Ckes, prlPreviousCca->Ckbs,prlPreviousCca->Ckes))
					{
						// Overlapping found, set status flag
						prlPreviousCca->Stat[2] = 'J';
						prlActualCca->Stat[2] = 'J';
					}
				}
			}
		}
	}

	for( i = 0; i < ilCount; i++)
	{
		if(olCcaList[i].Stat[2] == 'J')
		{
		
			ogCcaDiaFlightData.omCcaData.GetCcasByKKey(olCcas, olCcaList[i].KKey);

			for(int j = olCcas.GetSize() - 1; j >= 0; j-- )
			{
				olCcas[j].Stat[2] = olCcaList[i].Stat[2];
				olCcas[j].Stat[3] = olCcaList[i].Stat[3];
			}
			olCcas.RemoveAll();
		}
	}


	for( i = 0; i < ilCount; i++)
	{
		if(olCcaList[i].Stat[2] != olSaveCcaList[i].Stat[2])
		{
			blChangesFound = true;
		}
		if(olCcaList[i].Stat[2] == '0' && olCcaList[i].Stat[3] != '0')
		{
			olCcaList[i].Stat[3] = '0';
			blChangesFound = true;
		}
		//olCcaList[i].Brush = GetConflictBrushByCcaStatus(&olCcaList[i]);

	}

	/*
	if(blChangesFound)
		TRACE("\n Treffer %s %d ", opCkic, ilCount);
	else
		TRACE("\n No tref %s %d ", opCkic, ilCount);
	*/

	olSaveCcaList.DeleteAll();
	olCcaList.RemoveAll();
	return blChangesFound;


}










bool DataSet::CheckCCaForOverlapping(CString opCkic)
{
	if(bgCcaDiaSeason && ogCcaDiaSingleDate == TIMENULL)
	{
		return CheckKompCCaForOverlapping(opCkic);
	}
	
	bool blChangesFound = false;
	CCSPtrArray<DIACCADATA> olCcaList;
	CCSPtrArray<DIACCADATA> olSaveCcaList;
	CCSPtrArray<DIACCADATA> olCcas;
	int ilCount;

	ogCcaDiaFlightData.omCcaData.GetCcasByCkic(olCcaList,opCkic);

	ilCount = olCcaList.GetSize();
	
	olCcaList.Sort(CompareCcaForCkbs);

	for( int i = 0; i < ilCount; i++)
	{
		olSaveCcaList.New( olCcaList[i]);
		olCcaList[i].Stat[2] = '0';
	}

	DIACCADATA *prlActualCca;
	DIACCADATA *prlPreviousCca = NULL;
	
	
	for( i = 0; i < ilCount; i++)
	{
		prlActualCca = &olCcaList[i];

		for(int j = i + 1; j < ilCount; j++)
		{
			if(i != j)
			{
				prlPreviousCca = &olCcaList[j];

				if(prlPreviousCca->Urno != prlActualCca->Urno)
				{

					if(IsReallyOverlapped(prlPreviousCca->Ckbs,prlPreviousCca->Ckes, prlActualCca->Ckbs,prlActualCca->Ckes) ||	
						IsWithIn(prlPreviousCca->Ckbs,prlPreviousCca->Ckes, prlActualCca->Ckbs,prlActualCca->Ckes) || 	
						IsWithIn(prlActualCca->Ckbs,prlActualCca->Ckes, prlPreviousCca->Ckbs,prlPreviousCca->Ckes))
					{
						// Overlapping found, set status flag
						prlPreviousCca->Stat[2] = 'J';
						prlActualCca->Stat[2] = 'J';
					}
				}
			}
		}
	}


	for( i = 0; i < ilCount; i++)
	{
		if(olCcaList[i].Stat[2] != olSaveCcaList[i].Stat[2])
		{
			blChangesFound = true;
		}
		if(olCcaList[i].Stat[2] == '0' && olCcaList[i].Stat[3] != '0')
		{
			olCcaList[i].Stat[3] = '0';
			blChangesFound = true;
		}
		//olCcaList[i].Brush = GetConflictBrushByCcaStatus(&olCcaList[i]);

	}

	olSaveCcaList.DeleteAll();
	olCcaList.RemoveAll();
	return blChangesFound;
}













CString DataSet::CreateFlno(CString opAlc, CString opFltn, CString opFlns)
{

	opAlc.TrimRight();	
	opFltn.TrimRight();	
	
	if(opAlc.IsEmpty() || opFltn.IsEmpty())
		return CString("");


	if(opAlc.GetLength() == 0)
		opAlc = "   ";

	if(opAlc.GetLength() == 1)
		opAlc += "  ";

	if(opAlc.GetLength() == 2)
		opAlc += " ";


	if(opFltn.GetLength() == 0)
		opFltn = "   ";

	if(opFltn.GetLength() == 1)
		opFltn = "00" + opFltn;

	if(opFltn.GetLength() == 2)
		opFltn = "0" + opFltn;

	if(opFltn.GetLength() == 3)
		opFltn += "  ";

	if(opFltn.GetLength() == 4)
		opFltn += " ";

	if(opFlns.GetLength() == 0)
		opFlns = " ";


	CString olFlno = opAlc + opFltn + opFlns;

	olFlno.TrimRight();

	if(olFlno.IsEmpty())
		return "";
	else
		return opAlc + opFltn + opFlns;

}



