#if !defined(AFX_SEASONDLG_H__B7342653_2F56_11D1_82E1_0080AD1DC701__INCLUDED_)
#define AFX_SEASONDLG_H__B7342653_2F56_11D1_82E1_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// FlightDlg.h : header file
//
// Modification History:
// 23-nov-00	rkr		HandlePostFlight() and CheckPostFlight() 
//						for handling postflights added;

#include <SeasonDlgCedaFlightData.h>
#include <CCSEdit.h>
#include <CCSButtonCtrl.h>
#include <CCSColorButton.h>
//#include "CedaCCAData.h"
#include <CedaDiaCCAData.h>
#include <CCSTable.h>
#include <ViaTableCtrl.h>
/////////////////////////////////////////////////////////////////////////////
// CSeasonDlg dialog


#define ID_OK    WM_USER + 17171


enum EDITLISTMODE
{
	EDITLIST_LAST,
	EDITLIST_FIRST,
	EDITLIST_UP,
	EDITLIST_DOWN
};


enum SEASONDLGMODUS
{
//	DLG_NEW,
//	DLG_COPY,
//	DLG_CHANGE,
	DLG_CHANGE_DIADATA
};


struct NEW_SEASONDLG_DATA
{
	CWnd *Parent;
	long CurrUrno;
};



class CSeasonDlg : public CDialog
{
// Construction
public:
	//CSeasonDlg(CWnd* pParent = NULL,  CCSTable *popFlightTable = NULL, SEASONDLGFLIGHTDATA *prpAFlight = NULL, SEASONDLGFLIGHTDATA *prpDFlight = NULL, int ipModus = DLG_NEW);   // standard constructor
	CSeasonDlg(CWnd* pParent = NULL);   // standard constructor
	~CSeasonDlg();

	void NewData(CWnd* pParent, long lpRkey, long lpUrno, int ipModus, bool bpLocalTime, CString opAdid = "U");
	void NewData(CWnd* pParent, const SEASONDLGFLIGHTDATA *prpDiaFlight, bool bpLocalTime, CString opAdid = 'U');

	CString omAdid;
	long lmCalledBy;
	long lmRkey;

	void FillFlightData();
	void FillGatPosFlightData();

// Dialog Data
	//{{AFX_DATA(CSeasonDlg)
	enum { IDD = IDD_SEASON };
	CCSButtonCtrl	m_CB_DCinsMal4;
	CCSEdit	m_CE_DBaz4;
	CCSEdit	m_CE_DBaz1;
	CComboBox	m_CB_DRemp;
	CComboBox	m_CB_ARemp;
	CCSEdit	m_CE_DDes4;
	CCSEdit	m_CE_AOrg4;
	CCSEdit	m_CE_DRem2;
	CCSEdit	m_CE_ARem2;
 	CStatic	m_CS_ACht3;
	CStatic	m_CS_DCht3;
	CCSEdit	m_CE_DCht3;
	CCSEdit	m_CE_ACht3;
	CStatic	m_CS_Nose;
	CCSEdit	m_CE_Nose;
	CCSButtonCtrl	m_CB_Ok;
	CCSButtonCtrl	m_CB_DDaly;
	CCSButtonCtrl	m_CB_DAlc3List2;
	CCSButtonCtrl	m_CB_DAlc3List3;
	CCSButtonCtrl	m_CB_Default;
	CCSButtonCtrl	m_CB_DStodCalc;
	CCSButtonCtrl	m_CB_AStoaCalc;
	CCSButtonCtrl	m_CB_AReset;
	CCSButtonCtrl	m_CB_DReset;
	CCSButtonCtrl	m_CB_DailyMask;
	CCSButtonCtrl	m_CB_NextFlight;
	CCSButtonCtrl	m_CB_PrevFlight;
	CCSButtonCtrl	m_CB_AShowJfno;
	CCSButtonCtrl	m_CB_ADaly;
	CCSButtonCtrl	m_CB_AAlc3List;
	CCSButtonCtrl	m_CB_AAlc3List3;
	CCSButtonCtrl	m_CB_AAlc3List2;
	CCSButtonCtrl	m_CB_DStatus;
	CCSButtonCtrl	m_CB_DShowJfno;
	CCSButtonCtrl	m_CB_DNoop;
	CCSButtonCtrl	m_CB_DCxx;
	CCSButtonCtrl	m_CB_AStatus;
	CCSButtonCtrl	m_CB_AStatus2;
	CCSButtonCtrl	m_CB_AStatus3;
	CCSButtonCtrl	m_CB_DStatus2;
	CCSButtonCtrl	m_CB_DStatus3;
	CCSButtonCtrl	m_CB_ANoop;
	CCSButtonCtrl	m_CB_ACxx;
	CCSButtonCtrl	m_CS_Ankunft;
	CCSButtonCtrl	m_CS_Abflug;
	CCSButtonCtrl	m_CB_AOrg3List;
	CCSButtonCtrl	m_CB_DDes3List;
	CCSButtonCtrl	m_CB_DAlc3List;
	CCSButtonCtrl	m_CB_Act35List;
	CCSButtonCtrl	m_CB_DTtypList;
	CCSButtonCtrl	m_CB_DStypList;
//	CCSButtonCtrl	m_CB_DHtypList;
	CCSButtonCtrl	m_CB_ATTypList;
	CCSButtonCtrl	m_CB_AStypList;
//	CCSButtonCtrl	m_CB_AHtypList;
	CCSButtonCtrl	m_CB_DWro1List;
	CCSButtonCtrl	m_CB_DPstdList;
	CCSButtonCtrl	m_CB_DGtd2List;
	CCSButtonCtrl	m_CB_DGtd1List;
	CCSButtonCtrl	m_CB_DCinsList4;
	CCSButtonCtrl	m_CB_DCinsList3;
	CCSButtonCtrl	m_CB_DCinsList2;
	CCSButtonCtrl	m_CB_DCinsList1;
	CCSEdit	m_CE_ATmb2;
	CCSButtonCtrl	m_CB_APstaList;
	CCSButtonCtrl	m_CB_AGta2List;
	CCSButtonCtrl	m_CB_AGta1List;
	CCSButtonCtrl	m_CB_AExt2List;
	CCSButtonCtrl	m_CB_AExt1List;
	CCSButtonCtrl	m_CB_ABlt2List;
	CCSButtonCtrl	m_CB_ABlt1List;
	CCSButtonCtrl	m_CB_Cancel;
	CCSButtonCtrl   m_CB_Agents;
	CStatic	m_CE_DJfnoBorder;
	CCSEdit	m_CE_DLastChange;
	CCSEdit	m_CE_DFluko;
	CCSEdit	m_CE_DCreated;
	CCSEdit	m_CE_DAlc3;
	CCSEdit	m_CE_ALastChange;
	CStatic	m_CE_AJfnoBorder;
	CCSEdit	m_CE_AFluko;
	CCSEdit	m_CE_ACreated;
	CCSEdit	m_CE_Seas;
	CCSEdit	m_CE_DTmct;
	CCSEdit	m_CE_DTmcf;
	CCSEdit	m_CE_DRem1;
	CCSEdit	m_CE_DGtd2;
	CCSEdit	m_CE_DGtd1;
	CCSEdit	m_CE_DDays;
	CCSEdit	m_CE_DCict;
	CCSEdit	m_CE_DCicf;
	CCSEdit	m_CE_AExt2;
	CCSEdit	m_CE_AExt1;
	CCSEdit	m_CE_ADays;
	CCSEdit	m_CE_ARem1;
	CCSEdit	m_CE_Act5;
	CCSEdit	m_CE_ABlt2;
	CCSEdit	m_CE_ABlt1;
	CCSEdit	m_CE_Act3;
	CCSEdit	m_CE_Ming;
//	CCSEdit	m_CE_DHtyp;
	CCSEdit	m_CE_DStev;
	CCSEdit	m_CE_DTtyp;
	CCSEdit	m_CE_DStyp;
	CCSEdit	m_CE_DFlns;
	CCSEdit	m_CE_DFltn;
	CCSEdit	m_CE_DFlti;
	CCSEdit	m_CE_DStod;
	CCSEdit	m_CE_DStoa;
	CCSEdit	m_CE_DEtdi;
	CCSEdit	m_CE_AEtai;
	CCSEdit	m_CE_DDes3;
	CCSEdit	m_CE_Pbis;
	CCSEdit	m_CE_Pvom;
	CCSEdit	m_CE_Freq;
	CCSEdit	m_CE_DOrg3;
	CCSEdit	m_CE_AGta2;
	CCSEdit	m_CE_AGta1;
	CCSEdit	m_CE_AStoa;
	CCSEdit	m_CE_AStod;
	CCSEdit	m_CE_AOrg3;
	CCSEdit	m_CE_AStev;
	CCSEdit	m_CE_AStyp;
//	CCSEdit	m_CE_AHtyp;
	CCSEdit	m_CE_ATtyp;
	CCSEdit	m_CE_AFlns;
	CCSEdit	m_CE_AFltn;
	CCSEdit	m_CE_AFlti;
	CCSEdit	m_CE_AAlc3;
	CCSEdit	m_CE_ADes3;
	CString	m_ADes3;
	CString	m_AEtoa;
	CString	m_AAlc3;
	CString	m_AFltn;
	CString	m_AFlns;
	CString	m_AFlti;
	CString	m_ATtyp;
//	CString	m_AHtyp;
	CString	m_AStyp;
	CString	m_AStev;
	CString	m_AOrg3;
	CString	m_AStod;
	CString	m_AStoa;
	CString	m_AGta1;
	CString	m_AGta2;
	CString	m_DOrg3;
	CString	m_Pvom;
	CString	m_Pbis;
	CString	m_ATage;
	CString	m_DDes3;
	CString	m_DTage;
	CString	m_DEtdi;
	CString	m_DStoa;
	CString	m_DStod;
	CString	m_DFltn;
	CString	m_DFlns;
	CString	m_DFlti;
	CString	m_DStyp;
	CString	m_DTtyp;
	CString	m_DStev;
	CString	m_DGta1;
	CString	m_DGta2;
//	CString	m_DHtyp;
	CString	m_AEtai;
	CString	m_Freq;
	CString	m_Ming;
	CString	m_Act3;
	CString	m_ABlt1;
	CString	m_ABlt2;
	CString	m_Act5;
	BOOL	m_ACxx;
	CString	m_ADays;
	CString	m_AExt1;
	CString	m_AExt2;
	BOOL	m_ANoop;
	CString	m_APstd2;
	CString	m_ARem1;
	int		m_AStatus;
	CString	m_DCicf;
	CString	m_DCict;
	BOOL	m_DCxx;
	CString	m_DDays;
	CString	m_DGtd1;
	CString	m_DGtd2;
	BOOL	m_DNoop;
	CString	m_DRem1;
	int		m_DStatus;
	CString	m_DTmcf;
	CString	m_DTmct;
	CString	m_Seas;
	CString	m_Created;
	CString	m_ACreated;
	CString	m_AFluko;
	CString	m_ALastChange;
	CString	m_DAlc3;
	CString	m_DCreated;
	CString	m_DFluko;
	CString	m_DLastChange;
	CString	m_AB1bs;
	CCSEdit	m_CE_AB1bs;
	CString	m_AB1es;
	CCSEdit	m_CE_AB1es;
	CString	m_AB2bs;
	CCSEdit	m_CE_AB2bs;
	CString	m_AB2es;
	CCSEdit	m_CE_AB2es;
	CString	m_AGa1b;
	CCSEdit	m_CE_AGa1b;
	CString	m_AGa1e;
	CCSEdit	m_CE_AGa1e;
	CString	m_AGa2b;
	CCSEdit	m_CE_AGa2b;
	CString	m_AGa2e;
	CCSEdit	m_CE_AGa2e;
	CString	m_APabs;
	CCSEdit	m_CE_APabs;
	CString	m_APaes;
	CCSEdit	m_CE_APaes;
	CString	m_APsta;
	CCSEdit	m_CE_APsta;
	CString	m_ATet1;
	CCSEdit	m_CE_ATet1;
	CString	m_ATet2;
	CCSEdit	m_CE_ATet2;
	CString	m_ATga1;
	CCSEdit	m_CE_ATga1;
	CString	m_ATga2;
	CCSEdit	m_CE_ATga2;
	CString	m_ATmb1;
	CCSEdit	m_CE_ATmb1;
	CString	m_ATmb2;
	CString	m_DGd1b;
	CCSEdit	m_CE_DGd1b;
	CString	m_DGd1e;
	CCSEdit	m_CE_DGd1e;
	CString	m_DGd2b;
	CCSEdit	m_CE_DGd2b;
	CString	m_DGd2e;
	CCSEdit	m_CE_DGd2e;
	CString	m_DPdbs;
	CCSEdit	m_CE_DPdbs;
	CString	m_DPdes;
	CCSEdit	m_CE_DPdes;
	CString	m_DTgd1;
	CCSEdit	m_CE_DTgd1;
	CString	m_DTgd2;
	CCSEdit	m_CE_DTgd2;
	CString	m_DTwr1;
	CCSEdit	m_CE_DTwr1;
	CString	m_DW1bs;
	CCSEdit	m_CE_DW1bs;
	CString	m_DW1es;
	CCSEdit	m_CE_DW1es;
	CString	m_DWro1;
	CCSEdit	m_CE_DWro1;
	CStatic	m_CE_DCinsBorder;
	CString	m_DPstd;
	CCSEdit	m_CE_DPstd;
	CString	m_Nose;
	CString	m_ACht3;
	CString	m_DCht3;
	CString	m_ARem2;
	CString	m_DRem2;
	CString	m_AOrg4;
	CString	m_DDes4;
	CString	m_DBaz1;
	CString	m_DBaz4;
	//}}AFX_DATA

protected:
	SEASONDLGFLIGHTDATA *prmAFlight;
	SEASONDLGFLIGHTDATA *prmDFlight;
	SEASONDLGFLIGHTDATA *prmAFlightSave;
	SEASONDLGFLIGHTDATA *prmDFlightSave;

	bool bmInit;	

	CWnd *pomParent;
	//CCSTable *pomFlightTable;
	int imModus;
	bool bmIsJfnoShown;
	int imAJfnoColumnPos;
	bool bmIsCheckInNew;
	bool bmDlgEnabled;
	bool bmChanged;
	bool bmLocalTime;

	CString	omAAlc2;
	CString omAAlc3;
	CString	omDAlc2;
	CString omDAlc3;

	CCSPtrArray<JFNODATA> omAJfno;
	CCSPtrArray<JFNODATA> omDJfno;
//	CCSPtrArray<CCADATA> omDCins;
//	CCSPtrArray<CCADATA> omOldDCins;
//	CCSPtrArray<CCADATA> omToSaveCca;
//	CCSPtrArray<CCADATA> omDCinsSave;

	CCSPtrArray<DIACCADATA> omDCins;
	CCSPtrArray<DIACCADATA> omOldDCins;
	CCSPtrArray<DIACCADATA> omToSaveCca;
	CCSPtrArray<DIACCADATA> omDCinsSave;

	bool bmAutoSetBaz1;
	bool bmAutoSetBaz4;
		
	// test
	CTime omOpenTime;
	void Settestdefault();
	// test

	void SetSecState();
	void SetWndPos();


	void InitDialog(bool bpArrival = true, bool bpDeparture = true);
	void InitTables();
	void ClearAll();
	bool CheckAll(CString &opGMess, CString &opAMess, CString &opDMess);
	bool CheckGatPos(bool bpAnkunft, bool bpAbflug, CString &opGMess, CString &opAMess, CString &opDMess);

	void AShowJfnoButton();
	void DShowJfnoButton();
	void DShowCinsTable();

	CCSTable *pomAJfnoTable;
	CCSTable *pomDJfnoTable;
	CCSTable *pomDCinsTable;

	ViaTableCtrl *pomAViaCtrl;
	ViaTableCtrl *pomDViaCtrl;

	CTime omARefDat;
	CTime omDRefDat;

	bool bmAnkunft;
	bool bmAbflug;

	bool bmIsAFfnoShown;
	bool bmIsDFfnoShown;

	char pcmDOrg4[5];
	char pcmADes4[5];


public:
	void ProcessFlightChange(SEASONDLGFLIGHTDATA *prpFlight);
	void ProcessFlightDelete(SEASONDLGFLIGHTDATA *prpFlight);
//	void ProcessCCA(const CCADATA *prpCca, int ipDDXType);
	void ProcessCCA(const DIACCADATA *prpCca, int ipDDXType);
	void ProcessRotationChange();
	void ProcessRotationChange(SEASONDLGFLIGHTDATA *prpFlight);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSeasonDlg)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

private:
//	CedaCcaData omCcaData;
	CedaDiaCcaData omCcaData;

	bool bmAFltiUserSet;
	bool bmDFltiUserSet;

	void EnableGatPos();
	void EnableArrival();
	void EnableGlobal();
	void EnableDeparture();

	//functions for handling postflights
	BOOL HandlePostFlight();
	BOOL CheckPostFlight();

	bool AddBltCheck();

	void ReadCcaData();

private:
//	static int CompareCcas(const CCADATA **e1, const CCADATA **e2);
	static int CompareCcas(const DIACCADATA **e1, const DIACCADATA **e2);
	void GetCcas();

		// checkin-counter has actual openingtime (-1L), otherwise (0L); ipLineNo = line from table
	LONG CcaHasNoOpentime(int ipLineNo);
	void ClearActualTimes();



// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSeasonDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnNextflight();
	afx_msg void OnPrevflight();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnAshowjfno();
	afx_msg void OnDshowjfno();
	afx_msg LONG OnEditKillfocus( UINT wParam, LPARAM lParam);
	afx_msg LONG OnEditChanged( UINT wParam, LPARAM lParam);
	afx_msg LONG OnTableIPEditKillfocus( UINT wParam, LPARAM lParam);
	afx_msg void OnDreset();
	afx_msg void OnDailyMask();
	afx_msg void OnDefault();
	afx_msg void OnAreset();
	afx_msg void OnAdaly();
	afx_msg void OnDdaly();
	afx_msg void OnAstatus();
	afx_msg void OnAcxx();
	afx_msg void OnAnoop();
	afx_msg void OnDcxx();
	afx_msg void OnDnoop();
	afx_msg void OnDstatus();
	afx_msg void OnAAlc3LIST();
	afx_msg void OnAAlc3LIST2();
	afx_msg void OnAAlc3LIST3();
	afx_msg void OnAblt1list();
	afx_msg void OnAblt2list();
	afx_msg void OnAct35list();
	afx_msg void OnAext1list();
	afx_msg void OnAext2list();
	afx_msg void OnAgta1list();
	afx_msg void OnAgta2list();
//	afx_msg void OnAhtyplist();
	afx_msg void OnAorg3list();
	afx_msg void OnApstalist();
	afx_msg void OnAstyplist();
	afx_msg void OnDalc3list();
	afx_msg void OnDalc3list2();
	afx_msg void OnDalc3list3();
	afx_msg void OnDcinslist1();
	afx_msg void OnDcinslist2();
	afx_msg void OnDcinslist3();
	afx_msg void OnDcinslist4();
	afx_msg void OnDdes3list();
	afx_msg void OnDgtd1list();
	afx_msg void OnDgtd2list();
//	afx_msg void OnDhtyplist();
	afx_msg void OnDpstdlist();
	afx_msg void OnDstyplist();
	afx_msg void OnDttyplist();
	afx_msg void OnDwro1list();
	afx_msg void OnAttyplist();
	afx_msg void OnDcinsmal4();
	afx_msg void OnSelchangeAremp();
	afx_msg void OnSelchangeDremp();
	afx_msg void OnEditRButtonDown(UINT wParam, LPARAM lParam);
	afx_msg void OnAgent(); 
	afx_msg LONG OnTableIPEdit( UINT wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SEASONDLG_H__B7342653_2F56_11D1_82E1_0080AD1DC701__INCLUDED_)
