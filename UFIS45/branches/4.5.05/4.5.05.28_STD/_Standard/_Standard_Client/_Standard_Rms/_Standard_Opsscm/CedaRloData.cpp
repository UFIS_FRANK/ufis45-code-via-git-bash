// CedaRloData.cpp - Read and write demand data 
//
//
// Written by:
// Damkerng Thammathakerngkit   May 5, 1996
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <OpssCm.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <BasicData.h>
#include <CedaRloData.h>






void  ProcessRloCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaRloData::CedaRloData()
{
    // Create an array of CEDARECINFO for RLODATA
    BEGIN_CEDARECINFO(RLODATA, DemandDataRecInfo)
        CCS_FIELD_LONG(Urno,"URNO", "",1)
        CCS_FIELD_LONG(Urud,"URUD", "",1)
        CCS_FIELD_CHAR_TRIM(Gtab,"GTAB", "",1)
        CCS_FIELD_CHAR_TRIM(Reft,"REFT", "",1)
        CCS_FIELD_CHAR_TRIM(Rloc,"RLOC", "",1)
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(DemandDataRecInfo)/sizeof(DemandDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&DemandDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names


	strcpy(pcmTableName,"RLO");

    pcmFieldList = "URNO,URUD,GTAB,REFT,RLOC";


	
}


CedaRloData::~CedaRloData()
{
	TRACE("CedaRloData::~CedaRloData\n");
	ClearAll();
	ogDdx.UnRegister((void *)this,BC_DEMAND_CHANGE);
}


void CedaRloData::Register(void)
{
	//ogDdx.Register((void *)this,BC_DEMAND_CHANGE,	CString("RLODATA"), CString("Cca-changed"),	ProcessRloCf);
	//ogDdx.Register((void *)this,BC_DEMAND_NEW,		CString("RLODATA"), CString("Cca-new"),		ProcessRloCf);
	//ogDdx.Register((void *)this,BC_DEMAND_DELETE,	CString("RLODATA"), CString("Cca-deleted"),	ProcessRloCf);
}




bool CedaRloData::ClearAll( bool bpWithRegistration)
{

	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
	omUruds = "";

    omUrnoMap.RemoveAll();
    omData.DeleteAll();

	POSITION rlPos;
	CCSPtrArray<RLODATA> *prlArray;


	for ( rlPos =  omUrudMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omUrudMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& )prlArray);
		prlArray->RemoveAll();
		delete prlArray;
	}
	omUrudMap.RemoveAll();

    return true;
}



bool CedaRloData::ReadAll()
{
	char pclSel[7000];

	ClearAll(false);

	strcpy(pclSel, " ");

	Read(pclSel, false, false);

	return true;
}


bool CedaRloData::Read(char *pspWhere /*NULL*/, bool bpClearAll, bool bpDdx)
{
    // Select data from the database
	bool ilRc = true;

	if(bpClearAll)
	{
		ClearAll();
	}


	if(pspWhere == NULL)
	{	
		return false;
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}

	if (ilRc != true)
	{
		if(!omLastErrorMessage.IsEmpty())
		{
			if(omLastErrorMessage.Find("ORA") != -1)
			{
				char pclMsg[2048]="";
				sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
				::MessageBox(NULL,pclMsg,"Datenbankfehler",MB_OK);
				return false;
			}
		}
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		RLODATA *prlRlo = new RLODATA;
		if ((ilRc = GetFirstBufferRecord(prlRlo)) == true)
		{
			if(UpdateInternal(prlRlo, bpDdx))
			{
				delete prlRlo;
			}
			else
			{
				InsertInternal(prlRlo, bpDdx);
			}
		}
		else
		{
			delete prlRlo;
		}
	}
    return true;

}




bool CedaRloData::InsertInternal(RLODATA *prpRlo, bool bpDdx)
{
	RLODATA *prlRlo = GetRloByUrno(prpRlo->Urno);
	if(prlRlo != NULL)
		DeleteInternal(prlRlo);

	
	omData.Add(prpRlo);//Update omData
	AddToKeyMap(prpRlo);
	//if(bpDdx)
		//ogDdx.DataChanged((void *)this, DEMAND_CHANGE,(void *)&prpRlo->Urno ); //Update Viewer
    return true;
}


bool CedaRloData::UpdateInternal(RLODATA *prpRlo, bool bpDdx)
{
	RLODATA *prlRlo = GetRloByUrno(prpRlo->Urno);

	DelFromKeyMap(prlRlo);

	if (prlRlo != NULL)
	{
		*prlRlo = *prpRlo; //Update omData
		AddToKeyMap(prlRlo);

		/////if(bpDdx)
			//ogDdx.DataChanged((void *)this,DEMAND_CHANGE,(void *)&prlRlo->Urno); //Update Viewer

		return true;
	}
    return false;
}


bool CedaRloData::DeleteInternal(RLODATA *prpRlo, bool bpDdx )
{
	long llUrno = prpRlo->Urno;

	DelFromKeyMap(prpRlo);
	int ilCcaCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilCcaCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpRlo->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
	//if(bpDdx)
		//ogDdx.DataChanged((void *)this,DEMAND_CHANGE,(void *)&llUrno); //Update Viewer
    return true;
}




void CedaRloData::AddToKeyMap(RLODATA *prpRlo)
{
	CCSPtrArray<RLODATA> *prlArray;

	if (prpRlo != NULL )
	{
		omUrnoMap.SetAt((void *)prpRlo->Urno,prpRlo);

		if(prpRlo->Urud > 0)
		{
			if(omUrudMap.Lookup((void *)prpRlo->Urud,(void *& )prlArray) == TRUE)
			{
				for(int i = prlArray->GetSize() - 1; i >= 0; i--)
				{
					if(((*prlArray)[i]).Urno == prpRlo->Urno)
					{
						prlArray->RemoveAt(i);
						break;
					}
				}
				prlArray->Add(prpRlo);
			}
			else
			{
				prlArray = new CCSPtrArray<RLODATA>;
				omUrudMap.SetAt((void *)prpRlo->Urud,prlArray);
				prlArray->Add(prpRlo);;
			}
		}

	}

}



void CedaRloData::DelFromKeyMap(RLODATA *prpRlo)
{
	CCSPtrArray<RLODATA> *prlArray;

	if (prpRlo != NULL )
	{
		omUrnoMap.RemoveKey((void *)prpRlo->Urno);

		if(omUrudMap.Lookup((void *)prpRlo->Urud,(void *& )prlArray) == TRUE)
		{
			for(int i = prlArray->GetSize() - 1; i >= 0; i--)
			{
				if(((*prlArray)[i]).Urno == prpRlo->Urno)
				{
					prlArray->RemoveAt(i);
					break;
				}
			}
			if(prlArray->GetSize() == 0)
			{
				omUrudMap.RemoveKey((void *)prpRlo->Urud);
				delete prlArray;
			}
		}

	}
}






RLODATA  *CedaRloData::GetRloByUrno(long lpUrno)
{
	RLODATA  *prlRlo;

	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlRlo) == TRUE)
	{
		return prlRlo;
	}
	return NULL;
}







bool  CedaRloData::GetRlosByUrud(CCSPtrArray<RLODATA> &ropRloList, long lpUrud)
{

	CCSPtrArray<RLODATA> *prlArray;
	RLODATA  *prlRlo;

	if(omUrudMap.Lookup((void *)lpUrud,(void *& )prlArray) == TRUE)
	{
		for(int i = prlArray->GetSize() - 1; i >= 0; i--)
		{
			prlRlo = &(*prlArray)[i];
			ropRloList.Add(prlRlo);
		}
	}
	
	if(ropRloList.GetSize() > 0)
	{
		return true;
	}
	else
	{
		return false;
	}

}








void  ProcessRloCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaRloData *)popInstance)->ProcessDemandBc(ipDDXType,vpDataPointer,ropInstanceName);
}


void  CedaRloData::ProcessDemandBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{

	struct BcStruct *prlBcData;			
	long llUrno;
	CString olSelection;
	prlBcData = (struct BcStruct *) vpDataPointer;
	RLODATA *prlRloand;


		if(ipDDXType == BC_DEMAND_NEW)
		{
			prlRloand = new RLODATA;
			GetRecordFromItemList(prlRloand,prlBcData->Fields,prlBcData->Data);
			if(prlRloand->Urno > 0)
			{
				//if(IsPassFilter(prlRloand))
				{
					InsertInternal(prlRloand);
					return;
				}
			}
			delete prlRloand;
		}

		if((ipDDXType == BC_DEMAND_CHANGE) || (ipDDXType == BC_DEMAND_DELETE))
		{
			prlRloand = new RLODATA;
			
			GetRecordFromItemList(prlRloand,prlBcData->Fields,prlBcData->Data);

			if(prlRloand->Urno == 0 )
			{
				olSelection = (CString)prlBcData->Selection;
				if (olSelection.Find('\'') != -1)
				{
					llUrno = GetUrnoFromSelection(prlBcData->Selection);
				}
				else
				{
					int ilFirst = olSelection.Find("=")+2;
					int ilLast  = olSelection.GetLength();
					llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
				}
				prlRloand->Urno = llUrno;
			}				
				
			if(prlRloand->Urno > 0 )
			{
				UpdateInternal(prlRloand, true);
			}
			delete prlRloand;
			return;

		}
	
		
}









