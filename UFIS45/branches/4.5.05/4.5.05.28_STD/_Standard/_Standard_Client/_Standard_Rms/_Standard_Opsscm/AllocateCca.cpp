
#include <StdAfx.h>
#include <CCSGlobl.h>
#include <AllocateCca.h>
#include <CedaDiaCcaData.h>
#include <CedaGrmData.h>


#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsWithIn(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))



static int CompareAllocInfo(const CIC_ALLOCATEINFO **e1, const CIC_ALLOCATEINFO **e2)
{
	return (strcmp((**e1).SortText, (**e2).SortText));
}

static int CompareCcaFlight(const FLT_CCA_FLIGHT **e1, const FLT_CCA_FLIGHT **e2)
{
	int ilRet;
	ilRet = ((**e1).OrdDigit == (**e2).OrdDigit)? 
						((**e1).MinDemandTime == (**e2).MinDemandTime)? 0: ((**e1).MinDemandTime < (**e2).MinDemandTime)? -1: 1 : 
	((**e1).OrdDigit < (**e2).OrdDigit)? -1: 1;

//	ilRet =  ((**e1).MinDemandTime == (**e2).MinDemandTime)? 0: ((**e1).MinDemandTime < (**e2).MinDemandTime)? -1: 1;
	return ilRet;
}

static int CompareCounterNo(const CIC_INFO **e1, const CIC_INFO **e2)
{
	return ((**e1).CounterNo == (**e2).CounterNo)? 0: ((**e1).CounterNo< (**e2).CounterNo)? -1: 1;
}




//-----------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
AllocateCca::AllocateCca(int ipMode)
{
	imMode = ipMode; //0 = Alle 1 = nur Offene
}

//-----------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
AllocateCca::~AllocateCca()
{
	ClearAll();
}

//-----------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
void AllocateCca::Allocate(CTime opFrom, CTime opTo)
{
	//ogCcaDiaFlightData.omCcaData
	int i = 0; 
	int j = 0;
	int k = 0;
	int ilCkiIdx = 0;
	CTime olTFrom = ogCcaDiaFlightData.omCcaData.omFrom;
	CTime olTTo = ogCcaDiaFlightData.omCcaData.omTo;
	ClearAll();
	InitAllocationData();


	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");
	TRACE("\n-----------------------------------------------");
	TRACE("\nBegin Allocate:  %s", CTime::GetCurrentTime().Format("%H:%M:%S"));


	if(!bgCcaDiaSeason)
	{	
		int ilCount = omFlightDem.GetSize();
		int ilCicCount = omCicData.GetSize();
		for(i = 0; i < ilCount; i++)
		{

			if( omFlightDem[i].Flno == "DE 4027")
			{
				int yxx= 0;
			}

			ilCkiIdx = 0;
			if(omFlightDem[i].IsAllocated == false)
			{
				FLT_CCA_FLIGHT rlCcaFlight = omFlightDem[i];
				if(rlCcaFlight.Type == "F")//Nur Dedicated f�r Fl�ge
				{
					bool blContinue = true;
					bool blCanAlloc = false;
					while(blContinue == true && blCanAlloc == false) 
					{
						if(ilCkiIdx+omFlightDem[i].NeedLines <= ilCicCount)
						{
							if(AreFollowingLinesValid(&omFlightDem[i]/*.Urno*/, ilCkiIdx, omFlightDem[i].NeedLines) == true)
							{
								//Calculate the possible rect
								CRect olPossibleRect;
								CString olF = olTFrom.Format("%d.%m.%Y-%H%M");
								CString olT = olTTo.Format("%d.%m.%Y-%H%M");
								CString olMin=omFlightDem[i].MinDemandTime.Format("%d.%m.%Y-%H%M");
								CString olMax=omFlightDem[i].MaxDemandTime.Format("%d.%m.%Y-%H%M");
								CTimeSpan olTDFrom = omFlightDem[i].MinDemandTime - olTFrom;
								CTimeSpan olTDTo   = omFlightDem[i].MaxDemandTime - olTFrom;
								olPossibleRect = CRect(olTDFrom.GetTotalMinutes(), 
													   ilCkiIdx*10, 
													   olTDTo.GetTotalMinutes(), 
													   ((ilCkiIdx+omFlightDem[i].NeedLines)*10)-1);
								omFlightDem[i].AllocRect = olPossibleRect;
								if(IsRectOverlapped(&omFlightDem[i]) == false)
								{
									blCanAlloc = true;
								}
							}// end if(AreFollowingLinesValid(long lpFlightUrno, int ipCkiIndex, int ilNeededLines) == true)
							if(blCanAlloc == false)
							{
								ilCkiIdx++;
							}
						}
						else
						{
							blContinue = false; //Dann sind wir durch
						}
					}//end while(blContinue == true && blCanAlloc == false) 
					if(blCanAlloc == true)
					{
						omFlightDem[i].IsAllocated = true;
						omFlightDem[i].StartLine = ilCkiIdx;
						omFlightDem[i].EndLine   = (ilCkiIdx+omFlightDem[i].NeedLines)-1;
						for(j = 0; j < omFlightDem[i].NeedLines; j++)
						{
							DIACCADATA *prlCca = ogCcaDiaFlightData.omCcaData.GetCcaByUrno(omFlightDem[i].DemandsUrnos[j]);
							if(prlCca != NULL)
							{
								int ilIdx = ilCkiIdx+j;
								if(ilIdx < ilCicCount)
								{
									prlCca->IsChanged = DATA_CHANGED;
									prlCca->Stat[0] = '0';
									prlCca->Stat[1] = '0';
									prlCca->Stat[2] = '0';
									prlCca->Stat[3] = '0';
									strcpy(prlCca->Ckic, omCicData[ilIdx].Cnam);
									strcpy(prlCca->Ckit, omCicData[ilIdx].Term);
									//RST ogDdx.DataChanged((void *)this,DIACCA_CHANGE,(void *)&prlCca->Urno); //Update Viewer
								}
							}
						}
					}//end if(blCanAlloc == true)
				}//end if(rlCcaFlight.Type == "F")//Nur Dedicated f�r Fl�ge
			}
		}//end for(i = 0; i < ilCount; i++)

	
	}
	else
	{
		// SEASON

		FLT_CCA_FLIGHT *prlCcaFlight;
		FLT_CCA_FLIGHT *prlCcaFlight2;

		int ilCount = omFlightDem.GetSize();
		int ilCicCount = omCicData.GetSize();
		int ilFirstLine = 0;
		int ilStartLine;
		bool blOk;
		CCSPtrArray<CCAFLIGHTDATA> *prlArray;
		POSITION pos;
		CString olTmp;
		bool blAllocate = false;
		




		for( pos = ogCcaDiaFlightData.omKompMap.GetStartPosition(); pos != NULL; )
		{
			ogCcaDiaFlightData.omKompMap.GetNextAssoc( pos, olTmp , (void *&)prlArray );


			ilStartLine = 0;
			ilFirstLine = 0;

			prlCcaFlight = NULL;


			if(omUrnoMap.Lookup((void *)((*prlArray)[0]).Urno,(void *& )prlCcaFlight) == TRUE)
			{
				if((prlCcaFlight->IsAllocated == false) && (prlCcaFlight->Type == "F"))
				{

					blOk = true;
					blAllocate = false;

					while( ilFirstLine != -1  && !blAllocate )
					{
						ilFirstLine = GetFirstValidLine( prlCcaFlight, ilStartLine);

						ilStartLine = ilFirstLine + 1;

						if(ilFirstLine >= 0)
						{
							blOk = true;

							for(k = 0; k < prlArray->GetSize(); k++)
							{
								if(omUrnoMap.Lookup((void *)((*prlArray)[k]).Urno,(void *& )prlCcaFlight2) == TRUE)
								{
									if((prlCcaFlight->IsAllocated == false) && (prlCcaFlight->Type == "F"))
									{
										if(ilFirstLine != GetFirstValidLine( prlCcaFlight2, ilFirstLine))
										{
											blOk = false;
											break;
										}
									}
								}
							}

						}
						if((ilFirstLine >= 0) && blOk)
						{
							blAllocate = true;

							for(k = 0; k < prlArray->GetSize(); k++)
							{
								if(omUrnoMap.Lookup((void *)((*prlArray)[k]).Urno,(void *& )prlCcaFlight2) == TRUE)
								{

									prlCcaFlight2->IsAllocated = true;
									prlCcaFlight2->StartLine = ilFirstLine;
									prlCcaFlight2->EndLine   = (ilFirstLine + prlCcaFlight2->NeedLines)-1;
									for(j = 0; j < prlCcaFlight2->NeedLines; j++)
									{
										DIACCADATA *prlCca = ogCcaDiaFlightData.omCcaData.GetCcaByUrno(prlCcaFlight2->DemandsUrnos[j]);
										if(prlCca != NULL)
										{
											int ilIdx = ilFirstLine + j;
											if(ilIdx < ilCicCount)
											{
												prlCca->IsChanged = DATA_CHANGED;
												prlCca->Stat[0] = '0';
												prlCca->Stat[1] = '0';
												prlCca->Stat[2] = '0';
												prlCca->Stat[3] = '0';
												strcpy(prlCca->Ckic, omCicData[ilIdx].Cnam);
												strcpy(prlCca->Ckit, omCicData[ilIdx].Term);
												//RST ogDdx.DataChanged((void *)this,DIACCA_CHANGE,(void *)&prlCca->Urno); //Update Viewer
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	TRACE("\nEnd Allocate:    %s", CTime::GetCurrentTime().Format("%H:%M:%S"));
	TRACE("\n-----------------------------------------------");


}


int AllocateCca::GetFirstValidLine( FLT_CCA_FLIGHT *prlCcaFlight, int ipStartLine )
{

	int ilCkiIdx = ipStartLine;
	CTime olTFrom = ogCcaDiaFlightData.omCcaData.omFrom;
	CTime olTTo = ogCcaDiaFlightData.omCcaData.omTo;
	bool blContinue = true;
	bool blCanAlloc = false;
	int ilCicCount = omCicData.GetSize();

	while(blContinue == true && blCanAlloc == false) 
	{
		if(ilCkiIdx + prlCcaFlight->NeedLines <= ilCicCount)
		{
			if(AreFollowingLinesValid(prlCcaFlight , ilCkiIdx, prlCcaFlight->NeedLines) == true)
			{
				//Calculate the possible rect
				CRect olPossibleRect;
				CString olF   = olTFrom.Format("%d.%m.%Y-%H%M");
				CString olT   = olTTo.Format("%d.%m.%Y-%H%M");
				CString olMin = prlCcaFlight->MinDemandTime.Format("%d.%m.%Y-%H%M");
				CString olMax = prlCcaFlight->MaxDemandTime.Format("%d.%m.%Y-%H%M");
				CTimeSpan olTDFrom = prlCcaFlight->MinDemandTime - olTFrom;
				CTimeSpan olTDTo   = prlCcaFlight->MaxDemandTime - olTFrom;
				olPossibleRect = CRect(olTDFrom.GetTotalMinutes(), 
									   ilCkiIdx*10, 
									   olTDTo.GetTotalMinutes(), 
									   ((ilCkiIdx+prlCcaFlight->NeedLines)*10)-1);
				prlCcaFlight->AllocRect = olPossibleRect;
				if(IsRectOverlapped(prlCcaFlight) == false)
				{
					blCanAlloc = true;
					return ilCkiIdx;
				}
					
			}// end if(AreFollowingLinesValid(long lpFlightUrno, int ipCkiIndex, int ilNeededLines) == true)
			if(blCanAlloc == false)
			{
				ilCkiIdx++;
			}
		}
		else
		{
			blContinue = false; //Dann sind wir durch
		}
	}//end while(blContinue == true && blCanAlloc == false) 
	return -1;
}







//-----------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
void AllocateCca::InitAllocationData()
{
	MakeCicList();
	GetFlightsAndDemands();
}

//-----------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
void AllocateCca::MakeCicList()
{
	ClearAll();
	int ilUrno = ogBCD.GetFieldIndex("CIC", "URNO");
	int ilTerm = ogBCD.GetFieldIndex("CIC", "TERM");
	int ilBnam = ogBCD.GetFieldIndex("CIC", "CNAM");

	ogBCD.SetSort("CIC", "CICR+,TERM+,CNAM+", true); //true ==> sort immediately
	int ilCount = ogBCD.GetDataCount("CIC");
	for(int i = 0; i < ilCount; i++)
	{
		CIC_ALLOCATEINFO rlAllocInf;
		RecordSet rlRec;
		ogBCD.GetRecord("CIC", i, rlRec);
		MakeCicEntry(&rlAllocInf, rlRec);
		omCicData.NewAt(0, rlAllocInf);

	}
	omCicData.Sort(CompareAllocInfo);
}

//-----------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
void AllocateCca::MakeCicEntry(CIC_ALLOCATEINFO *prpAllocInf, RecordSet &ropRec)
{
	int ilUrno = ogBCD.GetFieldIndex("CIC", "URNO");
	int ilTerm = ogBCD.GetFieldIndex("CIC", "TERM");
	int ilCnam = ogBCD.GetFieldIndex("CIC", "CNAM");
	int ilHall = ogBCD.GetFieldIndex("CIC", "HALL");
	int ilCicr = ogBCD.GetFieldIndex("CIC", "CICR");
	prpAllocInf->Urno = atoi(ropRec[ilUrno]);
	prpAllocInf->Cnam = ropRec[ilCnam];
	prpAllocInf->Term = ropRec[ilTerm];
	prpAllocInf->Cicr = ropRec[ilCicr];
	if(CString(ropRec[ilHall]).IsEmpty())
	{
		prpAllocInf->SortText += CString("T: ") + CString(ropRec[ilTerm]) + CString(" ") + CString(ropRec[ilCnam]);
	}
	else
	{
		prpAllocInf->SortText += CString("H: ") + CString(ropRec[ilHall]) + CString(" ") + CString(ropRec[ilCnam]);
	}
}

//-----------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
void AllocateCca::ClearAll()
{
	int ilCount = omCicData.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		omCicData[i].AllocTimes.DeleteAll();
	}
	omUrnoMap.RemoveAll();
	omCicData.DeleteAll();
	omFlightDem.DeleteAll();
}

//-----------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
void AllocateCca::GetFlightsAndDemands()
{
	//omFlightDem
	//ogCcaDiaFlightData.omCcaData
	CCAFLIGHTDATA *prlFlight;
	POSITION pos;
	void *pVoid;
	FLT_CCA_FLIGHT *prlCcaFlight;

	for( pos = ogCcaDiaFlightData.omUrnoMap.GetStartPosition(); pos != NULL; )
	{
		ogCcaDiaFlightData.omUrnoMap.GetNextAssoc( pos, pVoid , (void *&)prlFlight );
		if(strcmp(prlFlight->Adid, "D") == 0)
		{
			prlCcaFlight = new FLT_CCA_FLIGHT;
	
			prlCcaFlight->Urno = prlFlight->Urno;				//Flighturno

			prlCcaFlight->Flno = CString(prlFlight->Flno);			//For Debuggin, to see the Flight
			prlCcaFlight->Flno.TrimRight();
			prlCcaFlight->Type = CString("F");
			if(GetAndCalcDemands(prlCcaFlight, prlFlight->Urno) == true)
			{
				if(prlCcaFlight->DemandsUrnos.GetSize() > 0)
				{
					prlCcaFlight->OrdDigit = prlCcaFlight->CicInfo.GetSize() - prlCcaFlight->DemandsUrnos.GetSize();
					omFlightDem.Add(prlCcaFlight);
					omUrnoMap.SetAt((void *)prlCcaFlight->Urno, (void *&)prlCcaFlight);
				}
				else
					delete prlCcaFlight;
			}
			else
				delete prlCcaFlight;
		}
	}
//Get Common Checkin Counters
	DIACCADATA *prlCca;
	for( pos = ogCcaDiaFlightData.omCcaData.omUrnoMap.GetStartPosition(); pos != NULL; )
	{
		ogCcaDiaFlightData.omCcaData.omUrnoMap.GetNextAssoc( pos, pVoid , (void *&)prlCca );
		if(strcmp(prlCca->Ctyp, "C") == 0 || strcmp(prlCca->Ctyp, "N") == 0 )
		{
			if(strcmp(prlCca->Ckic, "") != 0 )
			{
				FLT_CCA_FLIGHT rlCcaFlight;
				rlCcaFlight.Urno = prlCca->Urno;				//Flighturno
				rlCcaFlight.Flno = CString("Common Cki");			//For Debuggin, to see the Flight
				rlCcaFlight.Flno.TrimRight();
				if(strcmp(prlCca->Ctyp, "N") == 0)
				{
					rlCcaFlight.Type = CString("N");
				}
				else
				{
					rlCcaFlight.Type = CString("C");
				}
				
				if(GetAndCalcCommonDemands(&rlCcaFlight, prlCca->Urno) == true)
				{
					if(rlCcaFlight.DemandsUrnos.GetSize() > 0)
					{
						rlCcaFlight.OrdDigit = 99999;
						omFlightDem.NewAt(0, rlCcaFlight);
					}
				}
			}
		}
		else
		{
			if(imMode == 1)
			{
				CTime olTFrom = ogCcaDiaFlightData.omCcaData.omFrom;
				CTime olTTo = ogCcaDiaFlightData.omCcaData.omTo;
				int ilC2 = omCicData.GetSize();
				bool blCicFound = false;
				int ilCicIdx = 0;
				for(int j = 0; ((j < ilC2) && (blCicFound == false)); j++)
				{
					if(omCicData[j].Cnam == CString(prlCca->Ckic))
					{
						ilCicIdx = j;
						blCicFound = true;
					}
				}
				if(blCicFound == true)
				{
					CTime olStart = TIMENULL, 
						  olEnd = TIMENULL;
					olStart = prlCca->Ckbs;
					olEnd = prlCca->Ckes;
					if(olStart != TIMENULL && olEnd != TIMENULL)
					{
						FLT_CCA_FLIGHT rlCcaFlight;
						rlCcaFlight.Urno = prlCca->Urno;				//Flighturno
						if(rlCcaFlight.MinDemandTime > olStart || rlCcaFlight.MinDemandTime == TIMENULL)
						{
							rlCcaFlight.MinDemandTime = olStart;
						}
						if(rlCcaFlight.MaxDemandTime < olEnd || rlCcaFlight.MaxDemandTime == TIMENULL)
						{
							rlCcaFlight.MaxDemandTime = olEnd;
						}
						if(strcmp(prlCca->Ckic, "") != 0)
						{
							rlCcaFlight.DemandsUrnos.NewAt(0, prlCca->Urno);
							CTimeSpan olTDFrom = rlCcaFlight.MinDemandTime - olTFrom;
							CTimeSpan olTDTo   = rlCcaFlight.MaxDemandTime - olTFrom;
							CRect olRect = CRect(olTDFrom.GetTotalMinutes(), 
												   ilCicIdx*10, 
												   olTDTo.GetTotalMinutes(), 
												   ((ilCicIdx+1 )*10)-1);
							rlCcaFlight.AllocRect = olRect;
							rlCcaFlight.IsAllocated = true;
							rlCcaFlight.OrdDigit = 99999;
							omFlightDem.NewAt(0, rlCcaFlight);
						}
					}
				}
			}
		}
	}
	omFlightDem.Sort(CompareCcaFlight);

}


//-----------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
bool AllocateCca::GetAndCalcDemands(FLT_CCA_FLIGHT *prpCcaFlight, long lpUrno)
{
	bool blRet = false;
	int i;
	CCSPtrArray<DIACCADATA> olCcaList;
	CTime olTFrom = ogCcaDiaFlightData.omCcaData.omFrom;
	CTime olTTo = ogCcaDiaFlightData.omCcaData.omTo;
	ogCcaDiaFlightData.omCcaData.GetCcasByFlnu(olCcaList, lpUrno);
	CString olCgru;

	int ilCount = olCcaList.GetSize();
	for(int k  = 0; k < ilCount; k++)
	{
		//Preparation of restricted checkin counter groups for this flight
		// If the list is empty ==> the allocation algorithm will not make a check
		//prpLineData->Remark = ogBCD.GetField("GHP", "URNO", olGhpUrno, "PRNA");
		if((strcmp(olCcaList[k].Ctyp, "E") != 0) && ( olCcaList[k].Stat[9] == '0'))
		{
			olCgru = CString(olCcaList[k].Cgru);
			CStringArray olStrArray;
			ExtractItemList(olCgru, &olStrArray, ';');
			int ilItemCount = olStrArray.GetSize();
			for(i = 0; i < ilItemCount; i++)
			{
				long llUrno = atol(olStrArray[i].GetBuffer(0));
				CIC_INFO rlCicInfo;
				CCSPtrArray<GRMDATA> olGrmList;
				ogGrmData.GetGrmDataByGrnUrno(olGrmList, llUrno);
				int ilGrmCount = olGrmList.GetSize();
				for(int j = 0; j < ilGrmCount; j++)
				{
					rlCicInfo.Urno = atol(olGrmList[j].Valu);
					CString olCkic;
					CString olTmpValu = olGrmList[j].Valu;
					ogBCD.GetField("CIC", "URNO", olGrmList[j].Valu, "CNAM", olCkic);
					if(!olCkic.IsEmpty())
					{
						rlCicInfo.CounterNo = atoi(olCkic.GetBuffer(0));
					}
					else
					{
						CString olText = CString("Counter Urno <") + olGrmList[j].Valu + CString("> not found!") ;
						MessageBox(NULL, olText, "Error!", MB_OK);
						rlCicInfo.CounterNo = 9999999;
					}
					prpCcaFlight->CicInfo.NewAt(0, rlCicInfo);
				}
				olGrmList.DeleteAll();
			}
			k = ilCount;
		}

	}
	prpCcaFlight->CicInfo.Sort(CompareCounterNo);
	for(i = 0; i < ilCount; i++)
	{
		if((strcmp(olCcaList[0].Ctyp, "E") != 0) && ( olCcaList[0].Stat[9] == '0'))
		{
			DIACCADATA rlCca = olCcaList[i];
			CTime olStart = TIMENULL, 
				  olEnd = TIMENULL;
			olStart = olCcaList[i].Ckbs;
			olEnd = olCcaList[i].Ckes;
			if(olStart != TIMENULL && olEnd != TIMENULL)
			{
				if(prpCcaFlight->MinDemandTime > olStart || prpCcaFlight->MinDemandTime == TIMENULL)
				{
					prpCcaFlight->MinDemandTime = olStart;
				}
				if(prpCcaFlight->MaxDemandTime < olEnd || prpCcaFlight->MaxDemandTime == TIMENULL)
				{
					prpCcaFlight->MaxDemandTime = olEnd;
				}
				if(!CString(rlCca.Ckic).IsEmpty() && imMode != 0)
					prpCcaFlight->IsAllocated = true;
				
				prpCcaFlight->NeedLines++;		//Amount of Demands
				prpCcaFlight->DemandsUrnos.NewAt(0, olCcaList[i].Urno); //All CCA-Demands for this Flight
				blRet = true;
			}
			//Update all Cca => Ckic = "" ==> not Allocated
			DIACCADATA *prlTmpCca = ogCcaDiaFlightData.omCcaData.GetCcaByUrno(olCcaList[i].Urno);
			if(prlTmpCca != NULL)
			{
				if(imMode == 0)
				{
					strcpy(prlTmpCca->Ckic, "");
					prlTmpCca->IsChanged = DATA_CHANGED;
					ogCcaDiaFlightData.omCcaData.UpdateInternal(prlTmpCca, false);
				}

			}
		}
	}
	CString olS = prpCcaFlight->MinDemandTime.Format("%H:%M");
	CString olE = prpCcaFlight->MaxDemandTime.Format("%H:%M");
	return blRet;
}

//-----------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
bool AllocateCca::GetAndCalcCommonDemands(FLT_CCA_FLIGHT *prpCcaFlight, long lpUrno)
{
	bool blRet = false;
	CTime olTFrom = ogCcaDiaFlightData.omCcaData.omFrom;
	CTime olTTo = ogCcaDiaFlightData.omCcaData.omTo;
	DIACCADATA *prlCca = ogCcaDiaFlightData.omCcaData.GetCcaByUrno(lpUrno);
	if(prlCca != NULL)
	{
		int ilCount = omCicData.GetSize();
		bool blCicFound = false;
		int ilCicIdx = 0;
		for(int i = 0; ((i < ilCount) && (blCicFound == false)); i++)
		{
			if(omCicData[i].Cnam == CString(prlCca->Ckic))
			{
				ilCicIdx = i;
				blCicFound = true;
			}
		}
		if(blCicFound == true)
		{
			CTime olStart = TIMENULL, 
				  olEnd = TIMENULL;
			olStart = prlCca->Ckbs;
			olEnd = prlCca->Ckes;
			if(olStart != TIMENULL && olEnd != TIMENULL)
			{
				if(prpCcaFlight->MinDemandTime > olStart || prpCcaFlight->MinDemandTime == TIMENULL)
				{
					prpCcaFlight->MinDemandTime = olStart;
				}
				if(prpCcaFlight->MaxDemandTime < olEnd || prpCcaFlight->MaxDemandTime == TIMENULL)
				{
					prpCcaFlight->MaxDemandTime = olEnd;
				}
				prpCcaFlight->NeedLines++;		//Amount of Demands
				prpCcaFlight->DemandsUrnos.NewAt(0, prlCca->Urno); //All CCA-Demands for this Flight

				CString olF = olTFrom.Format("%d.%m.%Y-%H%M");
				CString olT = olTTo.Format("%d.%m.%Y-%H%M");
				CString olMin=prpCcaFlight->MinDemandTime.Format("%d.%m.%Y-%H%M");
				CString olMax=prpCcaFlight->MaxDemandTime.Format("%d.%m.%Y-%H%M");
				CTimeSpan olTDFrom = prpCcaFlight->MinDemandTime - olTFrom;
				CTimeSpan olTDTo   = prpCcaFlight->MaxDemandTime - olTFrom;
				CRect olRect = CRect(olTDFrom.GetTotalMinutes(), 
									   ilCicIdx*10, 
									   olTDTo.GetTotalMinutes(), 
									   ((ilCicIdx+prpCcaFlight->NeedLines)*10)-1);
				prpCcaFlight->AllocRect = olRect;
				prpCcaFlight->IsAllocated = true;
				blRet = true;
			}//end if(olStart != TIMENULL && olEnd != TIMENULL)
		}//end if(blCicFound == true)
	}//end if(prlCca != NULL)
	return blRet;
}



bool AllocateCca::AreFollowingLinesValid(FLT_CCA_FLIGHT *prpCcaFlight, int ipCkiIndex, int ilNeededLines)
{
	bool blRet = true;
	bool blCkiFound = true;
//	int ilPreCounterNo = -1;
	CString olPreCicr;
	CString dbgFlno;

	dbgFlno = prpCcaFlight->Flno;
	dbgFlno.TrimRight();

	if(dbgFlno == "US 893")
	{
		int o = 0;
	}
	int ilCicUrnoCount = prpCcaFlight->CicInfo.GetSize();
	if(ilCicUrnoCount > 0)
	{
		//Es gibt Schaltervorgaben aus den Regeln f�r diesen Flug
		if((ipCkiIndex < 0) || (ipCkiIndex > omCicData.GetSize()) || ((ipCkiIndex+ilNeededLines) > omCicData.GetSize()))
		{
			blRet = false;
		}
		if(blRet == true)
		{
			olPreCicr = omCicData[ipCkiIndex].Cicr;
			for(int i = ipCkiIndex; ((i < (ipCkiIndex+ilNeededLines)) && (blRet == true)); i++)
			{
				CString olCnam = omCicData[i].Cnam;
				if(olCnam == "950")
				{
					int x = 0;
				}
				bool blFCic = false;
				if(olPreCicr != omCicData[i].Cicr)
				{
					blRet = false;
				}//end if(olPreCicr != omCicData[i].Cicr;)
				else
				{
					for(int j = 0; ((j < ilCicUrnoCount) && (blFCic == false)); j++)
					{
						if(omCicData[i].Urno == prpCcaFlight->CicInfo[j].Urno)
						{
							blFCic = true;
						}//end if(omCicData[i].Urno == prpCcaFlight->CicInfo[j].Urno)
					}// end for(int j = 0; ((j < ilCicUrnoCount) && (blFCic == false)); j++)
				}//end else of if(olPreCicr != omCicData[i].Cicr;)
				if(blFCic == false)
				{
					blRet = false;
				}
			}// end for(int i = ipCkiIndex; i < (ipCkiIndex+ilNeededLines); i++)
		}//end if(blRet == true)
	}//end if(prpCcaFlight->CicInfo.GetSize() > 0)
	else
	{
		//Keine Vorgabe f�r Checking-Schalter im Regelwerk ==> Pr+fe Konventionell auf Terminal oder Halle
		CCAFLIGHTDATA *prlFlight = ogCcaDiaFlightData.GetFlightByUrno(prpCcaFlight->Urno);
		if(prlFlight != NULL)
		{
			if((ipCkiIndex >= omCicData.GetSize()) || ((ipCkiIndex+ilNeededLines) >= omCicData.GetSize()))
			{
				blRet = false;
			}
			if(blRet == true)
			{
				for(int i = ipCkiIndex; i < (ipCkiIndex+ilNeededLines); i++)
				{
					if(!ogCcaDiaFlightData.CheckCkiAllocate(prlFlight,  omCicData[i].Cnam))
					{
						blRet = false;
						break;
					}
				}
			}
		}
		else
		{
			blRet = false;
		}
	}//end else von if(prpCcaFlight->CicInfo.GetSize() > 0)
	//ogCcaDiaFlightData.CheckCkiAllocate(prlFlight,  prlLine->Bnam)
	return blRet;
}

bool AllocateCca::IsRectOverlapped(FLT_CCA_FLIGHT *prpCcaFlight)
{
	bool blRet = false;
	for(int i = 0; i < omFlightDem.GetSize(); i++)
	{
		if(omFlightDem[i].Urno != prpCcaFlight->Urno)
		{
			if(omFlightDem[i].IsAllocated == true)
			{
				FLT_CCA_FLIGHT rlCcaFlight = omFlightDem[i];

				CRect olInterSectRect;
				CRect olAlocR = omFlightDem[i].AllocRect;
				CRect olFlR   = prpCcaFlight->AllocRect;
				BOOL blIRet = olInterSectRect.IntersectRect( omFlightDem[i].AllocRect, prpCcaFlight->AllocRect);
				if(blIRet == TRUE)
				{
					return true;
				}
			}
		}
	}
	return blRet;
}




