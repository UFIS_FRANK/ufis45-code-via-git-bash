// SerListView.cpp : implementation file
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <servicecatalog.h>
#include <basicdata.h>
#include <SerListView.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static int IntCompare(const void *pipItem1,const void *pipItem2);


/////////////////////////////////////////////////////////////////////////////
// CSerListView property page

IMPLEMENT_DYNCREATE(CSerListView, CPropertyPage)

CSerListView::CSerListView() : CPropertyPage(CSerListView::IDD)
{
	//{{AFX_DATA_INIT(CSerListView)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CSerListView::~CSerListView()
{
}

void CSerListView::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSerListView)
	DDX_Control(pDX, IDC_VRGCLST, m_VrgcLst);
	DDX_Control(pDX, IDC_FILTER, m_VrgcFilter);

	//}}AFX_DATA_MAP

	if (pDX->m_bSaveAndValidate == FALSE)
	{
		SetData(omValues);
	}
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		GetData(omValues);
	}
}


BEGIN_MESSAGE_MAP(CSerListView, CPropertyPage)
	//{{AFX_MSG_MAP(CSerListView)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_REMOVE, OnRemove)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSerListView message handlers

BOOL CSerListView::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSerListView::OnAdd() 
{
	// TODO: Add your control notification handler code here
	// This function moves SER lines from the left window of SERVIEW to the right one
	int ilMaxItems = m_VrgcLst.GetSelCount();
	int ilLen = 0;

	if(ilMaxItems>0)
	{
		int *pilItems = new int [ilMaxItems];
		m_VrgcLst.GetSelItems(ilMaxItems, pilItems);
		qsort((void *)pilItems, (size_t)ilMaxItems, sizeof(int), IntCompare);
		for(int ilIndex = 0, ilLen = 0; ilIndex < ilMaxItems; ilIndex++)
		{
			int ilTest = pilItems[ilIndex];
			long llItemUrno = m_VrgcLst.GetItemData(pilItems[ilIndex]);

			CString olUrno;
			olUrno.Format("%d", llItemUrno);
			CString olSnam = ogBCD.GetField("SER", "URNO", olUrno, "SNAM");

			if(olSnam != "")
			{
				int ilFilterIndex = m_VrgcFilter.AddString(olSnam);
				if(ilFilterIndex >= 0)
				{
					m_VrgcFilter.SetItemData(ilFilterIndex, llItemUrno);
				}
			}
		}
		for(ilIndex = ilMaxItems;--ilIndex>=0;)
		{
			m_VrgcLst.DeleteString(pilItems[ilIndex]);
		}
		delete pilItems;
	}

	
}

void CSerListView::OnRemove() 
{
	// TODO: Add your control notification handler code here
	// This function removes SERlines from the right window of SERVIEW and places them in the left box again
	int ilMaxItems = m_VrgcFilter.GetSelCount();
	if(ilMaxItems > 0)
	{
		int *pilItems = new int [ilMaxItems];
		m_VrgcFilter.GetSelItems(ilMaxItems, pilItems);
		qsort((void *)pilItems, (size_t)ilMaxItems, sizeof(int), IntCompare);
		for(int ilIndex = 0, ilLen = 0; ilIndex < ilMaxItems; ilIndex++)
		{
			int ilTest = pilItems[ilIndex];
			long llItemUrno = m_VrgcFilter.GetItemData(pilItems[ilIndex]);

			CString olUrno;
			olUrno.Format("%d", llItemUrno);
			CString olSnam = ogBCD.GetField("SER", "URNO", olUrno, "SNAM");

			if(olSnam != "")
			{
				int ilLstIndex = m_VrgcLst.AddString(olSnam);
				if(ilLstIndex >= 0)
				{
					m_VrgcLst.SetItemData(ilLstIndex, llItemUrno);
				}
			}
		}
		for (ilIndex = ilMaxItems - 1; ilIndex >= 0; ilIndex--)
		{
			m_VrgcFilter.DeleteString(pilItems[ilIndex]);
		}
		delete pilItems;
	}

	
}

void CSerListView::SetData(CStringArray &ropValues)
{
	//-- exception handling
	CCS_TRY

	int ilIndex = 0, ilCount, i ;
	int ilFlag;
	CString olUrno = "";
	CString olFilter = "";
	CString olStrg;
	CStringArray	olTmpStr;
	CStringList		olSelectedUrnos;
	m_VrgcLst.ResetContent();
	m_VrgcFilter.ResetContent();
	
	//  f�lle listbox der ausgew�hlten
	for ( i=0; i<ropValues.GetSize(); i++ )
	{
		olFilter = "";
		olStrg = ropValues[i];
		ilFlag = ropValues[i].Find('|');
		if(ilFlag != -1)
		{
			olFilter = ropValues[i].Mid(0,ilFlag);
		}

		if(olFilter.IsEmpty() != TRUE)
		{
			ilCount = ExtractItemList(olFilter, &olTmpStr, ' ');
			for(int ilLC = ilCount - 1; ilLC >= 0; ilLC--)
			{
				CString olFilterUrno = olTmpStr[ilLC];
				CString olSnam = ogBCD.GetField("SER", "URNO", olFilterUrno, "SNAM");
				
				if(olSnam != "")
				{
					ilIndex = m_VrgcFilter.AddString(olSnam);
					if(ilIndex >= 0)
					{
						m_VrgcFilter.SetItemData(ilIndex, atol(olFilterUrno));
						olSelectedUrnos.AddTail ( olFilterUrno );
					}
				}
			}
		}				
	}
	//  F�lle linke List-Box 
	ilCount = ogBCD.GetDataCount("SER");
	RecordSet olRecord;
	int ilSerUrnoIdx = ogBCD.GetFieldIndex(pcgTableName,"URNO" );	
	int ilSerSnamIdx = ogBCD.GetFieldIndex(pcgTableName,"SNAM" );

	for( i = 0; i < ilCount; i++)
	{
		ogBCD.GetRecord("SER", i, olRecord);
		olUrno = olRecord.Values[ilSerUrnoIdx] ;
		if ( olSelectedUrnos.Find( olUrno ) )
			continue;	//  Datensatz ist bereits in rechter Listbox
		ilIndex = m_VrgcLst.AddString(olRecord.Values[ilSerSnamIdx]);
		if(ilIndex >= 0)
		{
			m_VrgcLst.SetItemData(ilIndex, atol(olUrno));
		}
	}

	//-- exception handling
	CCS_CATCH_ALL
}



//-------------------------------------------------------------------------------------------------------
//					get methods
//-------------------------------------------------------------------------------------------------------
BOOL CSerListView::GetData(CStringArray &ropValues)
{
	BOOL ilRet = TRUE;

	//-- exception handling
	CCS_TRY

	CString olTmp, olUrnos="";
	ropValues.RemoveAll();
	ropValues.SetSize(3);
	int ilSize = m_VrgcLst.GetCount();
	long llUrno = 0L;
	
	ropValues.SetAt(0,"|");
	for(int i = 0; i < m_VrgcFilter.GetCount(); i++)
	{
		olTmp.Format(" %ld", m_VrgcFilter.GetItemData(i));
		olUrnos += olTmp;
	}
	olUrnos += "|";
	ropValues.SetAt(1, olUrnos);
	return ilRet;

	//-- exception handling
	CCS_CATCH_ALL

	return ilRet;
}

void CSerListView::SetCaption(const char *pcpCaption)
{
	strcpy(pcmCaption,pcpCaption);
	m_psp.pszTitle = pcmCaption;
	m_psp.dwFlags |= PSP_USETITLE;
}

//-------------------------------------------------------------------------------------------------------
//					implementation of static functions
//-------------------------------------------------------------------------------------------------------

static int IntCompare(const void *pipItem1,const void *pipItem2)
{
	int ilItem1 = *(int *)pipItem1;
	int ilItem2 = *(int *)pipItem2;
	
	if(ilItem1>ilItem2)
	{
		return 1;
	}
	else if(ilItem1<ilItem2)
	{
		return -1;
	}
	else
	{
		return 0;
	}
}
//-------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------
