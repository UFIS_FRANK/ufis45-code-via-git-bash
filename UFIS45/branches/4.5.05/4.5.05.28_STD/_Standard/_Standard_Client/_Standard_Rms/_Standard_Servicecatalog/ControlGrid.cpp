// ControlGrid.cpp: implementation of the CControlGrid class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <resource.h>
#include <Ccsglobl.h>
#include <basicdata.h>
#include <utilities.h>
#include <ServiceGrid.h>
#include <ServiceCatalogView.h>
#include <ServiceCatalogDoc.h>

#include <ControlGrid.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


int CompareStrings (const class CString **p1 ,const class CString **p2 )
{
	if ( *p1 )
		return (*p1)->Compare ( **p2 );
	else
		return 0;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CControlGrid::CControlGrid()
{
	bmSortEnabled = false;
}

CControlGrid::~CControlGrid()
{

}

CControlGrid::CControlGrid( CWnd *popParent, UINT nID, CString opTitle, 
						    UINT ipResourceType )
	:CTitleGridControl ( popParent, nID, 1, 1 )
{
	int ilLen = 0;
	CString olKey, olValue;

	IniResourcesList ( ipResourceType );

	POSITION pos = omResourcesList.GetStartPosition() ;

	while ( pos )
	{
		omResourcesList.GetNextAssoc ( pos, olKey, olValue ); 
		ilLen += olKey.GetLength() + 1;
	}
	//  �berschrift "Location typ" setzen
	if ( ipResourceType == LOCATIONS )
		SetValue ( 0, 1, GetString (IDS_LOCATION_TYP) );
	if ( ipResourceType == EQUIPMENT )
		SetValue ( 0, 1, GetString (IDS_NAME) );
	
	CreateComboBoxes ( ilLen+1 );
	SetTitle ( opTitle );
	bmSortEnabled = false;
}


bool CControlGrid::CreateComboBoxes ( int ipLenthOfChoice )
{
	char *pclChoiceList=0;
	CString olLocationTyp, olTableName; 
	POSITION pos;
	CCSPtrArray<CString> olItems;
	ROWCOL	ilRowCount = GetRowCount();

	pclChoiceList = new char[ipLenthOfChoice];
	pclChoiceList[0] = '\0';
	if ( !pclChoiceList )
	{
		FileErrMsg ( m_hWnd, IDS_ALLOC_FAILED, 
					 ">CControlGrid::CreateComboBoxes<", 0, 
					 MB_ICONEXCLAMATION|MB_OK );
		return false;
	}
	
	pos = omResourcesList.GetStartPosition ();
	//  for ( int i=STARTINDLOCATIONS; i<RESORUCETABLEANZ; i++ )
	while ( pos )
	{
		omResourcesList.GetNextAssoc( pos, olLocationTyp, olTableName );
		olItems.New ( olLocationTyp );
	}
	olItems.Sort(CompareStrings );

	for ( int i=0; i<olItems.GetSize (); i++ ) 
	{
		strcat ( pclChoiceList, olItems[i] );
		strcat ( pclChoiceList, "\n" );
	}
	olItems.DeleteAll ();

	SetStyleRange( CGXRange(2,1,ilRowCount,1), 
				   CGXStyle().SetControl(GX_IDS_CTRL_TEXTFIT).SetValue("")
				   .SetChoiceList(pclChoiceList) );		//GX_IDS_CTRL_CBS_DROPDOWNLIST    
	delete pclChoiceList;
	SetColWidth(0, 0, 20);
	SetColWidth(1, 1, 190);

	return true;
}

BOOL CControlGrid::OnEndEditing( ROWCOL nRow, ROWCOL nCol) 
{
	//  Benutzer hat ComboBox verlassen
	CString				olAssociatedTable, olWert;
	CVarServiceGrid		*polControlledGrid = 0;
	CServiceCatalogView *polView=0;

	BOOL ilRet = CTitleGridControl::OnEndEditing( nRow, nCol) ;
	polView = (CServiceCatalogView *)GetParent(); 
	
	if ( polView && ( nCol==1 ) && (nRow>=2) )
	{
		if ( umID == IDC_GEGTAB )
			polControlledGrid = polView->pomEQUGrid;
		if ( umID == IDC_LLGTAB )
			polControlledGrid = polView->pomLGSGrid;

		if ( polControlledGrid && DeleteEmptyRows ( nRow, nCol ) )
			polControlledGrid->RemoveOneRow ( nRow ) ;
		else
		{
			olWert = GetValueRowCol ( nRow, nCol );
			olAssociatedTable = omResourcesList[olWert];
			if ( polControlledGrid == polView->pomEQUGrid )
			{
				if ( olAssociatedTable.IsEmpty () )
					polControlledGrid->SetOneTable ( "", nRow, olAssociatedTable );
				else
					polControlledGrid->SetOneTable ( "EQU", nRow, olAssociatedTable );
			}
			else
				polControlledGrid->SetOneTable ( olAssociatedTable, nRow, "" );
		}
	}
	return ilRet;
}

void CControlGrid::ResetValues ()
{
	SetValueRange ( CGXRange(2,1,2,1), "" );
	ROWCOL ilRowCount = GetRowCount();
	if ( ilRowCount >= 3 )
		RemoveRows( 3, ilRowCount );
}


void CControlGrid::SelectResource ( CString &ropResource, int ipResNo,
									bool bpCallControlledGrid /*=true*/ ) 
{
	CString		olLocationTyp, olTableName;
	POSITION	pos;


	pos = omResourcesList.GetStartPosition ();
	while ( pos )
	{
		omResourcesList.GetNextAssoc( pos, olLocationTyp, olTableName );
		if ( olTableName == ropResource )
		{
			SetValue ( ipResNo+1, 1, olLocationTyp );
			break;
		}

	}
	if ( bpCallControlledGrid )
		OnEndEditing( ipResNo+2, 1 ) ;
}

bool CControlGrid::DeleteEmptyRows ( ROWCOL nRow, ROWCOL nCol ) 
{
	ROWCOL  ilRowCount = GetRowCount();
	//  Wenn nicht in der ersten Spalte gel�scht bzw. editiert worden ist
	//  gibt es keine Zeile zu l�schen
	if ( nCol != 1 )
		return false;
	if ( ( nRow>=2 ) && ( nRow<ilRowCount ) &&
		  GetValueRowCol ( nRow, nCol ).IsEmpty() )
	{
		//RemoveRows( nRow, nRow );
		//SetRowHeadersText ();
		RemoveOneRow( nRow );
		return true;
	}
	else 
		return false;
}

void CControlGrid::DoAutoGrow ( ROWCOL nRow, ROWCOL nCol)
{
	//  nur bei Eingabe in der ersten Spalte und letzter Zeile wird die
	//	Tabelle automatisch erweitert
	if ( nCol == 1 )		
	{
		CTitleGridControl::DoAutoGrow ( nRow, nCol );	
	}
}

void CControlGrid::IniResourcesList ( int ipResourceType )
{
	RESOURCETABLEINFO *polInfo;
	CString olLocationTyp;
	CServiceCatalogDoc *polDoc = 0;
	CString	olName, olUrno;

	if ( pomWnd )
		polDoc = ((CServiceCatalogView*)pomWnd )->GetDocument ();

	if ( ipResourceType == LOCATIONS )
	{
		if ( polDoc )
		{
			omResourcesList.RemoveAll ();
			for ( UINT i=STARTINDLOCATIONS; i<polDoc->imResTableAnz; i++ )
			{
				polInfo = &(polDoc->psmUsedResourceTables[i]);
				olLocationTyp = polInfo->resourcetyp;
				omResourcesList.SetAt ( olLocationTyp, CString(polInfo->table) );
			}
		}
	}
	if ( ipResourceType == EQUIPMENT )
	{
		int ilIdx=0;
		
		if ( DoesTableExist("EQT") )
		{
			RecordSet olRecord( ogBCD.GetFieldCount("EQT"));
			int ilEqtNameIdx = ogBCD.GetFieldIndex ( "EQT", "NAME" );
			int ilEqtUrnoIdx = ogBCD.GetFieldIndex ( "EQT", "URNO" );
		
			omResourcesList.RemoveAll ();
			if ( (ilEqtNameIdx>=0) && (ilEqtUrnoIdx>=0) )
				while  ( ogBCD.GetRecord("EQT", ilIdx, olRecord ) )
				{
					olName = olRecord.Values[ilEqtNameIdx];
					olUrno = olRecord.Values[ilEqtUrnoIdx];
					omResourcesList.SetAt ( olName, olUrno );
					ilIdx++;
					if ( polDoc )
						polDoc->SetEquChoiceList ( olUrno );
				}
			//ogBCD.RemoveObject ( "EQT" );
			if ( polDoc )
			{
				ogBCD.SetDdxType("EQT", "IRT", BC_EQT_IRT );
				ogBCD.SetDdxType("EQT", "URT", BC_EQT_URT );
				ogBCD.SetDdxType("EQT", "DRT", BC_EQT_DRT );
				polDoc->omBCTables.SetAtGrow ( BC_EQT_IRT, "EQT");
				polDoc->omBCTables.SetAtGrow ( BC_EQT_URT, "EQT");
				polDoc->omBCTables.SetAtGrow ( BC_EQT_DRT, "EQT");
				TRACE ( "DDX IRT <EQT>: %d\n", BC_EQT_IRT ); 
			}
		}
	}

}

void CControlGrid::ModifyChoiceLists ()
{
	CString olLocationTyp, olTableName, olChoiceList; 
	POSITION pos;
	CCSPtrArray<CString> olItems;
	ROWCOL	ilRowCount = GetRowCount();

	pos = omResourcesList.GetStartPosition ();
	//  for ( int i=STARTINDLOCATIONS; i<RESORUCETABLEANZ; i++ )
	while ( pos )
	{
		omResourcesList.GetNextAssoc( pos, olLocationTyp, olTableName );
		olItems.New ( olLocationTyp );
	}
	olItems.Sort(CompareStrings );

	for ( int i=0; i<olItems.GetSize (); i++ ) 
	{
		olChoiceList += olItems[i] ;
		olChoiceList += "\n";
	}
	olItems.DeleteAll ();

	SetStyleRange( CGXRange(2,1,ilRowCount,1), 
				   CGXStyle().SetControl(GX_IDS_CTRL_TEXTFIT).
				   SetChoiceList(pCHAR(olChoiceList) ) );		
}
