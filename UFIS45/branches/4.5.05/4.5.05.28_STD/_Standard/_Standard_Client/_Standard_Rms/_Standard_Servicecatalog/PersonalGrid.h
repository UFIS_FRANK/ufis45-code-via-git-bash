// PersonalGrid.h: interface for the CPersonalGrid class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PERSONALGRID_H__B0E500A4_6CD0_11D3_936F_00001C033B5D__INCLUDED_)
#define AFX_PERSONALGRID_H__B0E500A4_6CD0_11D3_936F_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <ServiceGrid.h>

struct PERSONALRECORD;
class CPersonellData;
class CServiceCatalogView;
class CServiceCatalogDoc;
class CQualificationsGrid;

class CPersonalGrid : public CServiceGrid  
{
public:
	CPersonalGrid ( CServiceCatalogView *popView, UINT nID, 
					char * pcpTableName, CServiceCatalogDoc *popDoc );
	virtual ~CPersonalGrid();
//  Implementation
	void SetExpanded ( bool bpExpanded=true ) { bmExpanded = bpExpanded;
												OnChangedMode(); };
	PERSONALRECORD*	GetAssocPtr ( ROWCOL ipRow );
	void ResetValues ();
protected:
	virtual void OnChangedMode ();
	BOOL OnRButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
protected:
	bool	bmExpanded;
	CPersonellData *pomData;
};


class CFunctionsGrid: public CPersonalGrid  
{
public:
	CFunctionsGrid(CServiceCatalogView *popView, UINT nID,
				   CServiceCatalogDoc *popDoc, 
				   CQualificationsGrid *popQualiGrid );
	virtual ~CFunctionsGrid();
//  implementation
	void UpdateDisplay ();
	BOOL OnLButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol,UINT nFlags, CPoint pt);
	bool DoppelteResourcen ();
	bool SortTable ( ROWCOL ipRowClicked, ROWCOL ipColClicked );
protected:
	void OnInitCurrentCell(ROWCOL nRow, ROWCOL nCol);
	void UpdateQualiGrid ();
	void OnChangedMode ();
	BOOL OnEndEditing( ROWCOL nRow, ROWCOL nCol) ;
	bool IsResourceAllReadySelected ( CString &ropResource, 
									  CString &ropTableName, ROWCOL ipDontTest, 
									  bool bpErrMsg=true );
//  Data members:
protected:
	CQualificationsGrid *pomQualiGrid;
	ROWCOL				imLastCurrentRow;
};


class CQualificationsGrid : public CPersonalGrid  
{
public:
	CQualificationsGrid(CServiceCatalogView *popView, UINT nID, 
						CServiceCatalogDoc *popDoc );
	virtual ~CQualificationsGrid();
//  Implementation 
	void UpdateDisplay ( PERSONALRECORD *prpSingleFunc=0 );
	bool DoppelteResourcen ();
protected:
	void OnChangedMode ();
	BOOL OnEndEditing( ROWCOL nRow, ROWCOL nCol) ;
	bool IsResourceAllReadySelected ( CString &ropResource, 
									  CString &ropTableName, ROWCOL ipDontTest, 
									  bool bpErrMsg=true );
	bool IsQualificationAssigned ( ROWCOL ipRow );
	BOOL OnRButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	
//  Data members:
protected:
	PERSONALRECORD *prmDisplayedFunc;

};

#endif // !defined(AFX_PERSONALGRID_H__B0E500A4_6CD0_11D3_936F_00001C033B5D__INCLUDED_)
