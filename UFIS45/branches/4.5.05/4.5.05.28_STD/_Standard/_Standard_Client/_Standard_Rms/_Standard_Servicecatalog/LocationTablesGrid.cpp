// LocationTablesGrid.cpp: implementation of the CLocationTablesGrid class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include <ccsglobl.h>
#include <ServiceCatalog.h>
#include <ServiceCatalogDoc.h>
#include <ServiceCatalogView.h>
#include <ServiceGrid.h>
#include <utilities.h>
#include <LocationTablesGrid.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLocationTablesGrid::CLocationTablesGrid()
{
	bmSortEnabled = false;
}

CLocationTablesGrid::CLocationTablesGrid( CWnd *popParent, UINT nID, CString opTitle )
	:CTitleGridControl ( popParent, nID, 1, 1 )
{
	int ilLen = 0;
	RESOURCETABLEINFO *polInfo;
	CString olLocationTyp;
	CServiceCatalogDoc *polDoc = 0;

	if ( popParent )
		polDoc = ((CServiceCatalogView*)popParent)->GetDocument ();
	if ( polDoc )
		for ( UINT i=STARTINDLOCATIONS; i<polDoc->imResTableAnz; i++ )
		{
			polInfo = &(polDoc->psmUsedResourceTables[i]);
			olLocationTyp = polInfo->resourcetyp;
			omLocationsList.SetAt ( olLocationTyp, CString(polInfo->table) );
			ilLen += olLocationTyp.GetLength() + 1;
		}
	//  �berschrift "Location typ" setzen
	SetValue ( 0, 1, GetString (IDS_LOCATION_TYP) );
	
	CreateComboBoxes ( ilLen+1 );
	SetTitle ( opTitle );
	bmSortEnabled = false;
}

CLocationTablesGrid::~CLocationTablesGrid()
{

}

bool CLocationTablesGrid::CreateComboBoxes ( int ipLenthOfChoice )
{
	char *pclChoiceList=0;
	CString olLocationTyp, olTableName; 
	POSITION pos;
	ROWCOL	ilRowCount = GetRowCount();

	pclChoiceList = new char[ipLenthOfChoice];
	pclChoiceList[0] = '\0';
	if ( !pclChoiceList )
	{
		FileErrMsg ( m_hWnd, IDS_ALLOC_FAILED, 
					 ">CServiceGrid::CreateComboBoxes<", 0, 
					 MB_ICONEXCLAMATION|MB_OK );
		return false;
	}
	
	pos = omLocationsList.GetStartPosition ();
	//  for ( int i=STARTINDLOCATIONS; i<RESORUCETABLEANZ; i++ )
	while ( pos )
	{
		omLocationsList.GetNextAssoc( pos, olLocationTyp, olTableName );
		strcat ( pclChoiceList, olLocationTyp );
		strcat ( pclChoiceList, "\n" );
	}
	SetStyleRange( CGXRange(2,1,ilRowCount,1), 
				   CGXStyle().SetControl(GX_IDS_CTRL_TEXTFIT).SetValue("")
				   .SetChoiceList(pclChoiceList) );		//GX_IDS_CTRL_CBS_DROPDOWNLIST    
	delete pclChoiceList;
	SetColWidth(0, 0, 20);
	SetColWidth(1, 1, 190);

	return true;
}

BOOL CLocationTablesGrid::OnEndEditing( ROWCOL nRow, ROWCOL nCol) 
{
	//  Benutzer hat ComboBox verlassen
	CString				olAssociatedTable, olWert;
	CServiceCatalogView *polView;

	BOOL ilRet = CTitleGridControl::OnEndEditing( nRow, nCol) ;
	if ( ( nCol==1 ) && (nRow>=2) )
	{
		polView = (CServiceCatalogView *)GetParent(); 
		if ( polView && DeleteEmptyRows ( nRow, nCol ) )
			polView->pomLGSGrid->RemoveOneRow ( nRow ) ;
		else
		{
			olWert = GetValueRowCol ( nRow, nCol );
			olAssociatedTable = omLocationsList[olWert];
			if ( polView )
				polView->pomLGSGrid->SetOneTable ( olAssociatedTable, nRow );
		}
	}
	return ilRet;
}

void CLocationTablesGrid::ResetValues ()
{
	SetValueRange ( CGXRange(2,1,2,1), "" );
	ROWCOL ilRowCount = GetRowCount();
	if ( ilRowCount >= 3 )
		RemoveRows( 3, ilRowCount );
}

/*
BOOL CLocationTablesGrid::InsertBottomRow ()
{
	CGXStyle olStyle;
	BOOL blRet = CGridControl::InsertBottomRow ();
	ROWCOL ilRowCount = GetRowCount ();
	ComposeStyleRowCol( ilRowCount-1, 1, &olStyle );
	SetStyleRange  ( CGXRange ( ilRowCount, 1), olStyle.SetValue("") );
	return blRet;
}
*/


void CLocationTablesGrid::SelectLocation ( CString &ropLocationTable, int ipResNo,
										   bool bpCallLocationGrid /*=true*/ ) 
{
	CString		olLocationTyp, olTableName;
	POSITION	pos;


	pos = omLocationsList.GetStartPosition ();
	while ( pos )
	{
		omLocationsList.GetNextAssoc( pos, olLocationTyp, olTableName );
		if ( olTableName == ropLocationTable )
		{
			SetValue ( ipResNo+1, 1, olLocationTyp );
			break;
		}

	}
	if ( bpCallLocationGrid )
		OnEndEditing( ipResNo+2, 1 ) ;
}

bool CLocationTablesGrid::DeleteEmptyRows ( ROWCOL nRow, ROWCOL nCol ) 
{
	ROWCOL  ilRowCount = GetRowCount();
	//  Wenn nicht in der ersten Spalte gel�scht bzw. editiert worden ist
	//  gibt es keine Zeile zu l�schen
	if ( nCol != 1 )
		return false;
	if ( ( nRow>=2 ) && ( nRow<ilRowCount ) &&
		  GetValueRowCol ( nRow, nCol ).IsEmpty() )
	{
		//RemoveRows( nRow, nRow );
		//SetRowHeadersText ();
		RemoveOneRow( nRow );
		return true;
	}
	else 
		return false;
}

void CLocationTablesGrid::DoAutoGrow ( ROWCOL nRow, ROWCOL nCol)
{
	//  nur bei Eingabe in der ersten Spalte und letzter Zeile wird die
	//	Tabelle automatisch erweitert
	if ( nCol == 1 )		
	{
		CTitleGridControl::DoAutoGrow ( nRow, nCol );	
	}
}

