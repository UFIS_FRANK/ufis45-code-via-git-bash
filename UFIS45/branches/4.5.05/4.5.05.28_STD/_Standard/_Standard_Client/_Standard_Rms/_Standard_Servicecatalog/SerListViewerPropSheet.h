#if !defined(AFX_SERLISTVIEWERPROPSHEET_H__E1749A52_8539_11D7_9777_18CA9C000000__INCLUDED_)
#define AFX_SERLISTVIEWERPROPSHEET_H__E1749A52_8539_11D7_9777_18CA9C000000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SerListViewerPropSheet.h : header file
//
#include <BasePropertySheet.h>
#include <SerListView.h>
#include <CViewer.h>

/////////////////////////////////////////////////////////////////////////////
// CSerListViewerPropSheet

class CSerListViewerPropSheet : public BasePropertySheet
{
	DECLARE_DYNAMIC(CSerListViewerPropSheet)

// Construction
public:
	CSerListViewerPropSheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, CViewer *popViewer=0, UINT iSelectPage = 0);

// Attributes
public:
	CSerListView	m_SerListView;
// Operations
public:
	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSerListViewerPropSheet)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSerListViewerPropSheet();

	// Generated message map functions
protected:
	//{{AFX_MSG(CSerListViewerPropSheet)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERLISTVIEWERPROPSHEET_H__E1749A52_8539_11D7_9777_18CA9C000000__INCLUDED_)
