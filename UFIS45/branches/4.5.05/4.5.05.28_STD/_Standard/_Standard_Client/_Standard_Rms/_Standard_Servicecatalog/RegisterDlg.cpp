// RegisterDlg.cpp : implementation file
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <ServiceCatalog.h>
#include <CedaInitModuData.h>
#include <RegisterDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// RegisterDlg dialog


RegisterDlg::RegisterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(RegisterDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(RegisterDlg)
	//}}AFX_DATA_INIT
}


void RegisterDlg::DoDataExchange(CDataExchange* pDX)
{
	//-- exception handling
//	CCS_TRY

	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RegisterDlg)
	DDX_Control(pDX, IDC_TIME, m_TIME);
	DDX_Control(pDX, IDC_TEXT, m_TEXT);
	DDX_Control(pDX, IDC_ABBRECHEN,		m_ABBRECHEN);
	DDX_Control(pDX, IDC_STARTEN,		m_STARTEN);
	DDX_Control(pDX, IDC_REGISTRIEREN,	m_RREGISTER);
	//}}AFX_DATA_MAP

	//-- exception handling
//	CCS_CATCH_ALL
}


BEGIN_MESSAGE_MAP(RegisterDlg, CDialog)
	//{{AFX_MSG_MAP(RegisterDlg)
	ON_BN_CLICKED(IDC_ABBRECHEN, OnAbbrechen)
	ON_BN_CLICKED(IDC_REGISTRIEREN, OnRegistrieren)
	ON_BN_CLICKED(IDC_STARTEN, OnStarten)
    ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// RegisterDlg message handlers
//---------------------------------------------------------------------------

BOOL RegisterDlg::OnInitDialog() 
{
	//-- exception handling
	CCS_TRY

	CDialog::OnInitDialog();
	CString olTmpText,olTmpTime;
	if(ogPrivList.GetSize() > 1)
	{
		olTmpText = CString(GetString(IDS_REGISTER_APPL))+
				 CString(GetString(IDS_REGISTER1))+
				 CString(GetString(IDS_REGISTER2));
		olTmpText.Replace ( "%s", GetString(AFX_IDS_APP_TITLE) );
		olTmpText.Replace ( '\n', ' ' );
		olTmpTime = GetString(IDS_REGISTER3);
		SetTimer(4711,1000,NULL);
		imTimer = 15;
	}
	if(ogPrivList.GetSize() == 1)
	{
		olTmpText = CString(GetString(IDS_SERVICE_NOT_REG)) +
				 CString(GetString(IDS_REGISTER_ALLG));

		m_STARTEN.EnableWindow(FALSE);	
	}
	m_TEXT.SetWindowText(olTmpText);
	m_TIME.SetWindowText(olTmpTime);
	m_ABBRECHEN.SetWindowText(GetString(IDS_CANCEL) );
	m_STARTEN.SetWindowText(GetString(IDS_STARTEN));
	m_RREGISTER.SetWindowText(GetString(IDS_REGISTER) );
	SetBdpsState();	//-- activate/deactivate elements according to state of BDPS-SEC 

	//-- exception handling
	CCS_CATCH_ALL

	return TRUE;  
}

//---------------------------------------------------------------------------

void RegisterDlg::OnAbbrechen() 
{
	KillTimer(4711);
	CDialog::OnCancel();
}

//---------------------------------------------------------------------------

void RegisterDlg::OnRegistrieren() 
{
	//-- exception handling
	CCS_TRY

	KillTimer(4711);
	m_TIME.SetWindowText(GetString(IDS_NOW_REGISTERING));
	m_TIME.InvalidateRect(NULL);
	m_TIME.UpdateWindow();
	AfxGetApp()->DoWaitCursor(1);
	m_ABBRECHEN.EnableWindow(FALSE);	
	m_STARTEN.EnableWindow(FALSE);	
	m_RREGISTER.EnableWindow(FALSE);
	ogInitModuData.SendInitModu();
	AfxGetApp()->DoWaitCursor(-1);
	CDialog::OnCancel();

	//-- exception handling
	CCS_CATCH_ALL
}

//---------------------------------------------------------------------------

void RegisterDlg::OnStarten() 
{
	KillTimer(4711);
	CDialog::OnOK();
}

//---------------------------------------------------------------------------

void RegisterDlg::OnTimer(UINT nIDEvent)
{
	//-- exception handling
	CCS_TRY

	CDialog::OnTimer(nIDEvent);
	CString olTmpTime;
	imTimer--;
	if(imTimer == 0)
	{
		KillTimer(4711);
		OnStarten();
	}
	else
	{
		olTmpTime.Format(GetString(IDS_REGISTER4),imTimer);
		m_TIME.SetWindowText(olTmpTime);
	}

	//-- exception handling
	CCS_CATCH_ALL
}
//---------------------------------------------------------------------------


void RegisterDlg::SetBdpsState()
{
	// Set activation state acording to BDPS-SEC

	//-- exception handling
	CCS_TRY

	CWnd *polWnd = NULL;
	CWnd *polWnd1 = NULL;

	polWnd = GetDlgItem(IDC_TEXT);
	if(polWnd != NULL)
	{
		switch(ogPrivList.GetStat("REGISTERDLG_TEXT"))
		{
		case '0':
			// Disable text
			polWnd->EnableWindow(FALSE);
			polWnd->ShowWindow(SW_SHOW);
			break;
		case '-':
			// Hide text
			polWnd->ShowWindow(SW_HIDE);
			break;
		case '1':
			// Activate text
			polWnd->EnableWindow(TRUE);
			polWnd->ShowWindow(SW_SHOW);
			break;
		default:
			break;
		}
	} 

	polWnd = GetDlgItem(IDC_TIME);
	if(polWnd != NULL)
	{
		switch(ogPrivList.GetStat("REGISTERDLG_TIME"))
		{
		case '0':
			// Disable time
			polWnd->EnableWindow(FALSE);
			polWnd->ShowWindow(SW_SHOW);
			break;
		case '-':
			// Hide time
			polWnd->ShowWindow(SW_HIDE);
			break;
		case '1':
			// Activate time
			polWnd->EnableWindow(TRUE);
			polWnd->ShowWindow(SW_SHOW);
			break;
		default:
			break;
		}
	} 

	polWnd = GetDlgItem(IDC_STARTEN);
	if(polWnd != NULL)
	{
		switch(ogPrivList.GetStat("REGISTERDLG_STARTEN"))
		{
		case '0':
			// Disable Start Button
			polWnd->EnableWindow(FALSE);
			polWnd->ShowWindow(SW_SHOW);
			break;
		case '-':
			// Hide Start Button
			polWnd->ShowWindow(SW_HIDE);
			break;
		case '1':
			// Activate Start Button
			polWnd->EnableWindow(TRUE);
			polWnd->ShowWindow(SW_SHOW);
			break;
		default:
			break;
		}
	} 

	polWnd = GetDlgItem(IDC_REGISTRIEREN);
	if(polWnd != NULL)
	{
		switch(ogPrivList.GetStat("REGISTERDLG_REGISTRIEREN"))
		{
		case '0':
			// Disable Register Button
			polWnd->EnableWindow(FALSE);
			polWnd->ShowWindow(SW_SHOW);
			break;
		case '-':
			// Hide Register Button
			polWnd->ShowWindow(SW_HIDE);
			break;
		case '1':
			// Activate Register Button
			polWnd->EnableWindow(TRUE);
			polWnd->ShowWindow(SW_SHOW);
			break;
		default:
			break;
		}
	} 

	//-- exception handling
	CCS_CATCH_ALL
}