// FidasColorButton_Included.h : header file
//

#ifndef FidasColorButton_Included
#define FidasColorButton_Included

/////////////////////////////////////////////////////////////////////////////
// CColorButton command target

class CFidasColorButton : public CButton
{   
	DECLARE_DYNCREATE(CFidasColorButton)
public:
	CFidasColorButton();			// protected constructor used by dynamic creation
// Attributes
public:
	void operator =(int color) { m_Color = color; }
	operator int() { return m_Color; }
private:
	int m_Color;
	
// Operations
public:
	BOOL SubclassDlgItem(UINT nID, CWnd* pParent);
    void DoColorDialog();
	void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
    
// Implementation
public:
	virtual ~CFidasColorButton();

protected:
	// Generated message map functions
	//{{AFX_MSG(CFidasColorButton)
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif

/////////////////////////////////////////////////////////////////////////////
