
/*
 * starter version FIDAS UFIS DB access class declarations
 * j heilig June 7 01
 *
 * Jun 22 01 released to test (jhe)
 *
 */

#ifndef CUfisDB_included
#define CUfisDB_included

/*
 * To encapsulate the database access details, the class CUfisDB is defined
 * to provide the required methods. An instance of it must be created and
 * passed to the document manager before starting the work with FIDAS
 * documents. 
 *
 * The class CPEDRec is defined to pass record data.
 */

#include <aatlib.h> 
#include <fstream> 

class CPEDRec
{
public:
	CPEDRec(const char *,const char *,const char *,const char *,const char *);
	CPEDRec(const CPEDRec&);
	CPEDRec& operator=(const CPEDRec&);
	CString csName, csDesc, csDef, csGroup, csLang;
};

/*
 * The constructor takes the parameters server, user and password.
 * These are sufficient to handle database connection configuration
 * in each case, irrespective of the underlying DB access layer
 * (e.g. ODBC, COM or test file). The GetColumns method returns a list
 * of table columns names for table.  GetPEDTable returns all records of
 * PEDTAB. UpdatePEDRec updates the record specfied by the csName and
 * csGroup members (which constitute the primary key of the table). The
 * same applies to the InsertPEDRec and DeletePEDRec methods.
 */

class CFieldInfo
{
public:
	CString	csName;				// the name of the table field
	CString	csTranslation;		// the translated field name
	CString	csType;				// type of field : 'DATE' || 'TRIM'
	int		ciLength;			// length of field
};

class CUfisDB
{
public:
	CUfisDB(const char *srv, const char *usr, const char *pwd);
	virtual	~	CUfisDB();
	virtual int GetColumns(const char *table, CStringVector &, CStringVector &) const;
	virtual int GetPEDTable(std::vector<CPEDRec> &dst);
	virtual int UpdatePEDRec(const CPEDRec &); 
	virtual int InsertPEDRec(const CPEDRec &); 
	virtual int DeletePEDRec(const CPEDRec &); 
	virtual int PEDTableProc(int sel=1);
	virtual CString InFilterSep(const char *);
	virtual CString OutFilterSep(const char *);
	virtual	int	GetFieldInfos(const char *table,std::vector<CFieldInfo>& dst);
protected:
	CUfisDB();
private:
	// DB access method specific parts
	CString PEDFile;
	ifstream *ifs;
	static const char *OurSepChars, *DBSepChars;
	std::vector<CPEDRec> PEDTable;
	int write_PEDTable();
	std::vector<CPEDRec>::iterator find_PEDRec(const CPEDRec &);
};


#endif // CUfisDB_included

