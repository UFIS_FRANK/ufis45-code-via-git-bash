// stdafx.cpp : source file that includes just the standard includes
//	UDba.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

#include <stdafx.h>

BOOL AFXAPI CFidasAssertFailedLine(LPCSTR lpszFileName, int nLine)
{
	TCHAR szMessage[_MAX_PATH*2];
	wsprintf(szMessage, _T("Assertion Failed: File %hs, Line %d\n"),lpszFileName, nLine);

	// display the assert
	int nCode = ::MessageBox(NULL, szMessage, _T("Assertion Failed!"),MB_TASKMODAL|MB_ICONHAND|MB_ABORTRETRYIGNORE|MB_SETFOREGROUND);

	if (nCode == IDIGNORE)
		return FALSE;   // ignore

	if (nCode == IDRETRY)
		return TRUE;    // will cause AfxDebugBreak

	AfxAbort();     // should not return (but otherwise AfxDebugBreak)

	return TRUE;
}


