/////////////////////////////////////////////////////////////////////////////
// CFidasTabView

#include <tab.h>

class CFidasTabView : public CCtrlView
{
	DECLARE_DYNCREATE(CFidasTabView)

// Construction
public:
	CFidasTabView();

// Attributes
public:
	CTAB& GetTabCtrl() const;

// Overridables
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);

protected:
	virtual BOOL OnChildNotify(UINT, WPARAM, LPARAM, LRESULT*);

public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	//{{AFX_MSG(CFidasTabView)
	afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnNcDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CTAB	m_TabCtrl;
};

