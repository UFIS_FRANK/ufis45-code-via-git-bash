#if !defined(AFX_FIDASDATETIMEFIELDDIALOG_H__5080960A_5E26_11D5_812B_00010215BFDE__INCLUDED_)
#define AFX_FIDASDATETIMEFIELDDIALOG_H__5080960A_5E26_11D5_812B_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FidasDateTimeFieldDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// FidasDateTimeFieldDialog dialog
class CFieldDesc;

class FidasDateTimeFieldDialog : public CDialog
{
// Construction
public:
	FidasDateTimeFieldDialog(CWnd* pParent = NULL);   // standard constructor
	void SetFieldDescription(CFieldDesc *prpFieldDesc);
	void SetFormat(BOOL bpShowTime,BOOL bpShowDate,BOOL bpShowExtendedDate);
private:
// Dialog Data
	//{{AFX_DATA(FidasDateTimeFieldDialog)
	enum { IDD = IDD_UFIS_TIMEDATE_NEW };
	CStatic	m_Static_AdditionalIndicator;
	CButton	m_Cancel_Button;
	CStatic	m_Static_DateHourOffset;
	CButton	m_Group_DateProperties;
	CStatic	m_Static_DayFormat;
	CStatic	m_Static_DowFormat;
	CStatic	m_Static_HourFormat;
	CStatic	m_Static_MinuteFormat;
	CStatic	m_Static_MonthFormat;
	CButton	m_Ok_Button;
	CComboBox	m_Combo_SecondFormat;
	CStatic	m_Static_SecondFormat;
	CStatic	m_Static_TimeHourOffset;
	CButton	m_Group_TimeProperties;
	CButton	m_Group_DateMask;
	CButton	m_Group_TimeMask;
	CStatic	m_Static_YearFormat;
	CStatic	m_Static_Separator;
	CButton	m_Group_DisplayOrder;
	CButton	m_Group_ExtendedProperties;
	CEdit	m_Edit_DateHourOffset;
	CSpinButtonCtrl	m_Spin_DateHourOffset;
	CComboBox	m_Combo_DayFormat;
	CComboBox	m_Combo_DowFormat;
	CComboBox	m_Combo_HourFormat;
	CComboBox	m_Combo_MinuteFormat;
	CComboBox	m_Combo_MonthFormat;
	CEdit	m_Edit_TimeHourOffset;
	CSpinButtonCtrl	m_Spin_TimeHourOffset;
	CComboBox	m_Combo_YearFormat;
	CEdit	m_Edit_DateMask;
	CEdit	m_Edit_TimeMask;
	CEdit	m_Edit_Separator;
	CComboBox	m_Combo_AdditionalIndicator;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FidasDateTimeFieldDialog)
	public:
	virtual void OnFinalRelease();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(FidasDateTimeFieldDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnUfisTimedatenewCancelButton();
	afx_msg void OnUfisTimedatenewOkButton();
	afx_msg void OnSelchangeUfisTimedatenewMonthformatCombo();
	afx_msg void OnSelchangeUfisTimedatenewDowformatCombo();
	afx_msg void OnDeltaposUfisTimedatenewDatehouroffsetSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeUfisTimedatenewMonthextendedformatEdit();
	afx_msg void OnChangeUfisTimedatenewDowextendedformatEdit();
	afx_msg void OnSelchangeUfisTimedatenewAdditionalindicatorCombo();
	afx_msg void OnSelchangeUfisTimedatenewYearformatCombo();
	afx_msg void OnSelchangeUfisTimedatenewDayformatCombo();
	afx_msg void OnChangeUfisTimedatenewDatehouroffsetEdit();
	afx_msg void OnSelchangeUfisTimedatenewHourformatCombo();
	afx_msg void OnSelchangeUfisTimedatenewMinuteformatCombo();
	afx_msg void OnSelchangeUfisTimedatenewSecondformatCombo();
	afx_msg void OnChangeUfisTimedatenewTimehouroffsetEdit();
	afx_msg void OnChangeUfisTimedatenewSeparatorEdit();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(FidasDateTimeFieldDialog)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
private: // helpers
	CString	MakeDateFormat() const;
	BOOL	ParseDateFormat(const CString& ropFormat);
	CString	MakeTimeFormat() const;
	BOOL	ParseTimeFormat(const CString& ropFormat);
	void	UpdateDateMask();
	void	UpdateTimeMask();

private: // implementation data
	CFieldDesc	*pomFieldDesc;
	BOOL		bmShowDate;
	BOOL		bmShowTime;
	BOOL		bmShowExtendedTime;
	BOOL		bmDateFirst;
	CString		omTimeDateSeparator;	
	CString		olGoodText;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIDASDATETIMEFIELDDIALOG_H__5080960A_5E26_11D5_812B_00010215BFDE__INCLUDED_)
