#if !defined(AFX_FIDASSTATICFIELDDIALOG_H__4FE89CEA_6154_11D5_812F_00010215BFDE__INCLUDED_)
#define AFX_FIDASSTATICFIELDDIALOG_H__4FE89CEA_6154_11D5_812F_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FidasStaticFieldDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFidasStaticFieldDialog dialog
class CFieldDesc;

class CFidasStaticFieldDialog : public CDialog
{
// Construction
public:
	CFidasStaticFieldDialog(CWnd* pParent = NULL);   // standard constructor
	void SetFieldDescription(CFieldDesc *prpFieldDesc);

private:
// Dialog Data
	//{{AFX_DATA(CFidasStaticFieldDialog)
	enum { IDD = IDD_UFIS_STATIC };
	CString	m_EditText;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFidasStaticFieldDialog)
	public:
	virtual void OnFinalRelease();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFidasStaticFieldDialog)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CFidasStaticFieldDialog)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
private:
	CFieldDesc	*pomFieldDesc;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIDASSTATICFIELDDIALOG_H__4FE89CEA_6154_11D5_812F_00010215BFDE__INCLUDED_)
