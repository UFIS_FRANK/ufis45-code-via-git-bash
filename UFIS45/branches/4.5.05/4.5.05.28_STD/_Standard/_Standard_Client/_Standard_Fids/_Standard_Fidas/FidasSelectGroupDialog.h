#if !defined(AFX_FIDASSELECTGROUPDIALOG_H__5080960D_5E26_11D5_812B_00010215BFDE__INCLUDED_)
#define AFX_FIDASSELECTGROUPDIALOG_H__5080960D_5E26_11D5_812B_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FidasSelectGroupDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFidasSelectGroupDialog dialog

#include <vector>
typedef std::vector<CString> CStringVector;

class CFidasSelectGroupDialog : public CDialog
{
// Construction
public:
	CFidasSelectGroupDialog(CWnd* pParent = NULL);   // standard constructor
	void SetGroupList(const CStringVector& ropGroupList);
	CString GetSelectedGroup() const;
private:
// Dialog Data
	//{{AFX_DATA(CFidasSelectGroupDialog)
	enum { IDD = IDD_SELECT_GROUP };
	CComboBox	m_ComboGroup;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFidasSelectGroupDialog)
	public:
	virtual void OnFinalRelease();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFidasSelectGroupDialog)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CFidasSelectGroupDialog)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
private:
	CStringVector	omGroupList;
	CString			omGroup;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIDASSELECTGROUPDIALOG_H__5080960D_5E26_11D5_812B_00010215BFDE__INCLUDED_)
