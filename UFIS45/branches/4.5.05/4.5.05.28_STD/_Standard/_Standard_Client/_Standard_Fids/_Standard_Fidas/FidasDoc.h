// FidasDoc.h : interface of the CFidasDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_FidasDOC_H__7B1DCEE1_54EB_11D5_811D_00010215BFDE__INCLUDED_)
#define AFX_FidasDOC_H__7B1DCEE1_54EB_11D5_811D_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <CUfisDB.h>
#include <CFDDoc.h>

class CFidasDoc : public CDocument
{
public:
	CFidasDoc(const CString& ropGroup);
protected: // create from serialization only
	CFidasDoc();
	DECLARE_DYNCREATE(CFidasDoc)

// Attributes
public:
	CString	GetGroup() const;
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFidasDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual void OnCloseDocument();
	protected:
	virtual BOOL SaveModified();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CFidasDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CFidasDoc)
	afx_msg void OnFileSave();
	afx_msg void OnFileSaveAs();
	afx_msg void OnProperties();
	afx_msg void OnInsertFielddef();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CFidasDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
private:
	friend class	CFidasView;
	CFieldDefDoc	omFieldDefDoc;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FidasDOC_H__7B1DCEE1_54EB_11D5_811D_00010215BFDE__INCLUDED_)
