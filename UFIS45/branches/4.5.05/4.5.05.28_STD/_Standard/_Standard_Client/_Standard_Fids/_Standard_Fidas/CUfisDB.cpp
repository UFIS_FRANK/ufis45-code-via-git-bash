/*
 * AAB/AAT UFIS Display Handler
 *
 * Implementation of CUfisDB database acces layer for local file db (test)
 * j. heilig June 17 2001
 *
 * Jun 22 01 released to test (jhe)
 *
 */
  
#include <CUfisDB.h>

const char *fldSep = "?";

static const char *tableAFT[] = 
	{ "FLNO","ETAI","STOA","ORIG","EXT1","REMP", "ORG3", "VIA3", NULL };
static const char *tableABC[] = 
	{ "FA","FB","FC","FD","FE","FF", "FG", "FH", NULL };

const char *CUfisDB::OurSepChars = "\042\047\054\012\015";
const char *CUfisDB::DBSepChars = "\260\261\262\264\263";

CUfisDB::CUfisDB()
: ifs(NULL)
{
}

CUfisDB::CUfisDB(const char *srv, const char *, const char *)
: PEDFile(srv), ifs(NULL)
{
	ifs = new ifstream(srv);
}

CUfisDB::~CUfisDB()
{
	delete ifs;
}

int CUfisDB::GetColumns(const char *table, CStringVector & vec, CStringVector &t) const
{
	const char **psrc;
	psrc = NULL;
	if (strcmp(table,"AFT") == 0)
		psrc = tableAFT;
	else if (strcmp(table,"ABC") == 0)
		psrc = tableABC;

	vec.empty();
	t.empty();
	if(psrc)
	{
		while(*psrc)
		{
			vec.push_back(CString(*psrc));
			vec.push_back(CString(*psrc));
			++psrc;
		}
		return 0;
	}
	return -1;
}

int	CUfisDB::GetFieldInfos(const char *table,std::vector<CFieldInfo>& dst)
{
	return -1;	// not implemented
}

int CUfisDB::GetPEDTable(std::vector<CPEDRec> &dst)
{
int ix, err;

	PEDTable.empty();
	for(ix=err=0; !ifs->fail() && !ifs->eof(); ++ix)
	{
		CTokenZ tzLine(*ifs,"\n","\n");

		if (tzLine.Length() == 0 || tzLine.String() == "#")
			continue;

		char *p = tzLine.DupZ();
		Assert(p && *p);
		istrstream instr(p);

		CTokenZ tzNm(instr,fldSep,NULL);
		CTokenZ tzGr(instr,fldSep,fldSep);
		CTokenZ tzDs(instr,fldSep,fldSep);
		CTokenZ tzDf(instr,fldSep,fldSep);
		CTokenZ tzLg(instr,"\n", fldSep);

		delete p;

		if(tzNm.Length()==0||tzGr.Length()==0||tzDf.Length()==0||tzLg.Length()==0)
		{
			err = -1;
			break;
		}
		CPEDRec rec(InFilterSep(tzNm), InFilterSep(tzDs), InFilterSep(tzDf),
                InFilterSep(tzGr), InFilterSep(tzLg));
		PEDTable.push_back(rec);
		dst.push_back(rec);
	}

	return err;
}

int CUfisDB::UpdatePEDRec(const CPEDRec &r)
{
std::vector<CPEDRec>::iterator it;
	it = find_PEDRec(r);
	if (it == PEDTable.end())
		return -1;
	*it = r;
	return write_PEDTable();
}

int CUfisDB::InsertPEDRec(const CPEDRec &r)
{
std::vector<CPEDRec>::iterator it;
	it = find_PEDRec(r);
	if (it != PEDTable.end())
		return -1;
	PEDTable.push_back(r);
	return write_PEDTable();
}

int CUfisDB::DeletePEDRec(const CPEDRec &r)
{
std::vector<CPEDRec>::iterator it;
	it = find_PEDRec(r);
	if (it == PEDTable.end())
		return -1;
	PEDTable.erase(it);
	return write_PEDTable();
}
    
// update column that the stored procs are triggered by
int CUfisDB::PEDTableProc(int)
{
	// tbi
	return 0;
}

// replace comma placeholder chars by commas
CString CUfisDB::InFilterSep(const char *pin)
{
int l, lr, ix;
char *pout, *pbase;
	l = pin == NULL ? 0 : strlen(pin);
	pbase = pout = new char [l+1];
	Assert(pout);
	lr = strlen(OurSepChars);
	Assert(lr=strlen(DBSepChars));

	while(*pin)
	{
		for(ix=0; ix<lr; ++ix)
			if (*pin == DBSepChars[ix])
				break;
		*pout++ = ix >= lr ? *pin : OurSepChars[ix];
		++pin;
	}
	*pout = '\0';

	CString sout(pbase);
	delete pbase;
	return sout;
}

// replace commas by placeholder chars
CString CUfisDB::OutFilterSep(const char *pin)
{
int l, lr, ix;
char *pout, *pbase;
	l = pin == NULL ? 0 : strlen(pin);
	pbase = pout = new char [l+1];
	Assert(pout);
	lr = strlen(OurSepChars);
	Assert(lr=strlen(DBSepChars));

	while(*pin)
	{
		for(ix=0; ix<lr; ++ix)
			if (*pin == OurSepChars[ix])
				break;
		*pout++ = ix >= lr ? *pin : DBSepChars[ix];
		++pin;
	}
	*pout = '\0';

	CString sout(pbase);
	delete pbase;
	return sout;
}

//===== CUfisDB privates

int CUfisDB::write_PEDTable()
{
	ofstream os(PEDFile);
	if(!os)
		return -1;

	std::vector<CPEDRec>::const_iterator it;
	for(it=PEDTable.begin(); it!=PEDTable.end() && os; ++it)
	{
		os << OutFilterSep(it->csName) << fldSep; 
		os << OutFilterSep(it->csGroup) << fldSep; 
		os << OutFilterSep(it->csDesc) << fldSep; 
		os << OutFilterSep(it->csDef) << fldSep; 
		os << OutFilterSep(it->csLang) << endl; 
	}
	return !os;
}

std::vector<CPEDRec>::iterator CUfisDB::find_PEDRec(const CPEDRec &r)
{
	std::vector<CPEDRec>::iterator it;
	for(it=PEDTable.begin(); it!=PEDTable.end(); ++it)
		if(it->csName == r.csName && it->csGroup == r.csGroup)
			break;
	return it;
}

//===== PEDRec

CPEDRec::CPEDRec(const char * szName, const char * szDesc, const char * szDef,
                 const char *szGrp, const char *szLang)
: csName(szName), csDesc(szDesc), csDef(szDef), csGroup(szGrp), csLang(szLang)
{
}

CPEDRec::CPEDRec(const CPEDRec &c)
: csName(c.csName), csDesc(c.csDesc), csDef(c.csDef), csGroup(c.csGroup), csLang(c.csLang)
{
}

CPEDRec &CPEDRec::operator = (const CPEDRec &a)
{
	csName = a.csName;
	csDesc = a.csDesc;
	csDef = a.csDef;
	csGroup = a.csGroup;
	csLang = a.csLang;
	return *this;
}
