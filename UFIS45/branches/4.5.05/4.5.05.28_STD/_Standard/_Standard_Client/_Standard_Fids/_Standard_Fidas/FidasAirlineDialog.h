#if !defined(AFX_FIDASAIRLINEDIALOG_H__C3385C74_5A35_11D5_8124_00010215BFDE__INCLUDED_)
#define AFX_FIDASAIRLINEDIALOG_H__C3385C74_5A35_11D5_8124_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FidasAirlineDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFidasAirlineDialog dialog

#include <vector>

class CFieldDesc;
class CFieldInfo;

typedef std::vector<CString> CStringVector;
typedef std::vector<CFieldInfo> CFieldInfoVector;

class CFidasAirlineDialog : public CDialog
{
// Construction
public:
	CFidasAirlineDialog(CWnd* pParent = NULL);   // standard constructor
	void SetFieldDescription(CFieldDesc *prpFieldDesc);

private:
// Dialog Data
	//{{AFX_DATA(CFidasAirlineDialog)
	enum { IDD = IDD_UFIS_AIRLINE };
	CComboBox	m_AirlineFields;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFidasAirlineDialog)
	public:
	virtual void OnFinalRelease();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFidasAirlineDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnUfisAirlineOkButton();
	afx_msg void OnUfisAirlineCancelButton();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CFidasAirlineDialog)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
private:
	CFieldDesc*			pomFieldDesc;
	CFieldInfoVector	omFieldInfos;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIDASAIRLINEDIALOG_H__C3385C74_5A35_11D5_8124_00010215BFDE__INCLUDED_)
