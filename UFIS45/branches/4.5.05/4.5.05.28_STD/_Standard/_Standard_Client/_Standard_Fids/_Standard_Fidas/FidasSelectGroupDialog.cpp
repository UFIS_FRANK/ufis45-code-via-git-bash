// FidasSelectGroupDialog.cpp : implementation file
//

#include <stdafx.h>
#include <fidas.h>
#include <FidasSelectGroupDialog.h>
#include <FidasUtilities.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFidasSelectGroupDialog dialog


CFidasSelectGroupDialog::CFidasSelectGroupDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CFidasSelectGroupDialog::IDD, pParent)
{
	EnableAutomation();

	//{{AFX_DATA_INIT(CFidasSelectGroupDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CFidasSelectGroupDialog::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CDialog::OnFinalRelease();
}

void CFidasSelectGroupDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFidasSelectGroupDialog)
	DDX_Control(pDX, IDC_COMBO_GROUP, m_ComboGroup);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFidasSelectGroupDialog, CDialog)
	//{{AFX_MSG_MAP(CFidasSelectGroupDialog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CFidasSelectGroupDialog, CDialog)
	//{{AFX_DISPATCH_MAP(CFidasSelectGroupDialog)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IFidasSelectGroupDialog to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {5080960C-5E26-11D5-812B-00010215BFDE}
static const IID IID_IFidasSelectGroupDialog =
{ 0x5080960c, 0x5e26, 0x11d5, { 0x81, 0x2b, 0x0, 0x1, 0x2, 0x15, 0xbf, 0xde } };

BEGIN_INTERFACE_MAP(CFidasSelectGroupDialog, CDialog)
	INTERFACE_PART(CFidasSelectGroupDialog, IID_IFidasSelectGroupDialog, Dispatch)
END_INTERFACE_MAP()

void CFidasSelectGroupDialog::SetGroupList(const CStringVector& ropGroupList)
{
	omGroupList.clear();
	omGroupList = ropGroupList;
}

CString CFidasSelectGroupDialog::GetSelectedGroup() const
{
	return omGroup;
}

/////////////////////////////////////////////////////////////////////////////
// CFidasSelectGroupDialog message handlers

BOOL CFidasSelectGroupDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	SetWindowText(CFidasUtilities::GetString(IDS_SELECT_GROUP));

	CWnd *pWnd = GetDlgItem(IDC_SELECT_GROUP_STATIC);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_SELECT_GROUP_STATIC));

	pWnd = GetDlgItem(IDOK);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_OK));
	
	pWnd = GetDlgItem(IDCANCEL);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_CANCEL));
	
	
	for (int i = 0; i < omGroupList.size(); i++)
	{
		m_ComboGroup.AddString(omGroupList[i]);
	}

	if (m_ComboGroup.GetCount() > 0)
	{
		m_ComboGroup.SetCurSel(0);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CFidasSelectGroupDialog::OnOK() 
{
	// TODO: Add extra validation here
	if (!UpdateData(TRUE))
		return;

	int ind = m_ComboGroup.GetCurSel();
	if (ind < 0)
		return;

	m_ComboGroup.GetWindowText(omGroup);
	
	CDialog::OnOK();
}
