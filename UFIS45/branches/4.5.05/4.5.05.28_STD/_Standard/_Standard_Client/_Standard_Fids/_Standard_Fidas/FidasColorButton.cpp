// colorbut.cpp : implementation file
//

#include <stdafx.h>

#include <FidasColorbutton.h>
//#include "colorsel.h"


#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFidasColorButton

IMPLEMENT_DYNCREATE(CFidasColorButton, CButton)

CFidasColorButton::CFidasColorButton()
{
	m_Color = 0;
}

CFidasColorButton::~CFidasColorButton()
{
}

BEGIN_MESSAGE_MAP(CFidasColorButton, CButton)
	//{{AFX_MSG_MAP(CFidasColorButton)
	ON_WM_DRAWITEM()
	ON_WM_LBUTTONUP()
	ON_WM_KEYUP()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CFidasColorButton operations

BOOL CFidasColorButton::SubclassDlgItem(UINT nID, CWnd* pParent)
{
	BOOL b = CButton::SubclassDlgItem(nID, pParent);
	SetButtonStyle(BS_OWNERDRAW);
	return b;
}

void CFidasColorButton::DoColorDialog()
{
/*
#if 0
	CColorDialog dlgColor(m_Color);
#else
	CColorSelectDlg dlgColor;
	dlgColor.SetColor(m_Color);
#endif
	if (dlgColor.DoModal() == IDOK)
	{
		m_Color = dlgColor.GetColor();
		Invalidate();
	}
*/
}

/////////////////////////////////////////////////////////////////////////////
// CFidasColorButton message handlers


void CFidasColorButton::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
		
	CPen lightPen;
	lightPen.CreatePen(PS_SOLID, 1, ::GetSysColor(COLOR_BTNHILIGHT));
	CPen darkPen;
	darkPen.CreatePen(PS_SOLID, 1, ::GetSysColor(COLOR_BTNSHADOW));
	CPen blackPen;
	blackPen.CreateStockObject(BLACK_PEN);
	CRect rect = lpDrawItemStruct->rcItem;
	if (m_Color == -1)
	{
		CBrush backBrush;
		backBrush.CreateSolidBrush(::GetSysColor(COLOR_BTNFACE));
		pDC->FillRect(&rect, &backBrush);
		COLORREF oldFore = pDC->SetTextColor(::GetSysColor(COLOR_BTNTEXT));
		COLORREF oldBack = pDC->SetBkColor(::GetSysColor(COLOR_BTNFACE));
		CString text = "None";
		CSize textSize = pDC->GetTextExtent(text);
		pDC->TextOut((rect.right+rect.left-textSize.cx)/2, (rect.top+rect.bottom-textSize.cy)/2, text);
		pDC->SetTextColor(oldFore);
		pDC->SetBkColor(oldBack);
	}
	else
	{
		CBrush brush;
		brush.CreateSolidBrush(COLORREF(m_Color));
		pDC->FillRect(&rect, &brush);
	}
	CPen *pOldPen;
	if (lpDrawItemStruct->itemState & ODS_SELECTED)
	{
		pOldPen = pDC->SelectObject(&blackPen);
		pDC->MoveTo(rect.right-1, rect.top);
		pDC->LineTo(rect.left, rect.top);
		pDC->LineTo(rect.left, rect.bottom-1);
		pDC->LineTo(rect.right-1, rect.bottom-1);
		pDC->LineTo(rect.right-1, rect.top);
		pDC->SelectObject(darkPen);
		pDC->MoveTo(rect.right-2, rect.top+1);
		pDC->LineTo(rect.left+1, rect.top+1);
		pDC->LineTo(rect.left+1, rect.bottom-1);
		pDC->SelectObject(&lightPen);
		pDC->MoveTo(rect.left+1, rect.bottom-2);
		pDC->LineTo(rect.right-2, rect.bottom-2);
		pDC->LineTo(rect.right-2, rect.top);
	}
	else
	{
		if (lpDrawItemStruct->itemState & ODS_FOCUS)
		{
			pOldPen = pDC->SelectObject(&blackPen);
			pDC->MoveTo(rect.right-1, rect.top);
			pDC->LineTo(rect.left, rect.top);
			pDC->LineTo(rect.left, rect.bottom);
			pDC->SelectObject(&lightPen);
			pDC->MoveTo(rect.right-1, rect.top+1);
			pDC->LineTo(rect.left+1, rect.top+1);
			pDC->LineTo(rect.left+1, rect.bottom+1);
		}
		else
		{
			pOldPen = pDC->SelectObject(&lightPen);
			pDC->MoveTo(rect.right-1, rect.top);
			pDC->LineTo(rect.left, rect.top);
			pDC->LineTo(rect.left, rect.bottom);
		}
		pDC->SelectObject(&darkPen);
		pDC->MoveTo(rect.left, rect.bottom-2);
		pDC->LineTo(rect.right-2, rect.bottom-2);
		pDC->LineTo(rect.right-2, rect.top-1);
		pDC->SelectObject(&blackPen);
		pDC->MoveTo(rect.right-1, rect.top);
		pDC->LineTo(rect.right-1, rect.bottom-1);
		pDC->LineTo(rect.left-1, rect.bottom-1);
	}
	pDC->SelectObject(&pOldPen);
}

void CFidasColorButton::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	CRect clientRect;
	GetClientRect(&clientRect);
	if (clientRect.PtInRect(point))
	{
		DoColorDialog();
	}
	CButton::OnLButtonUp(nFlags, point);
}

void CFidasColorButton::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: Add your message handler code here and/or call default
	
	if (nChar == VK_SPACE && nRepCnt == 1) 
	{
		DoColorDialog();
	}
	CButton::OnKeyUp(nChar, nRepCnt, nFlags);
}

