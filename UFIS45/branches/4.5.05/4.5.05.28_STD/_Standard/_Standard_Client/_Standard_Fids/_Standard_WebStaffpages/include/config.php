<?php

/*
// db-connection for CEDA-flight information data
$database="(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = dxb1)(PORT = 1521))) (CONNECT_DATA = (SID = UFIS) (SERVER = DEDICATED)))";
*/
$DatabaseType="oci8";
$dbusername="ceda";
$dbpassword="ceda";
$database="UFISLH_ORA";
$servername="UFISLH";

// db-connection for staff user admin tables
//$databaseSystem="(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = dxb1)(PORT = 1521))) (CONNECT_DATA = (SID = UFIS) (SERVER = DEDICATED)))";
$DatabaseTypeSystem="oci8";
$dbusernameSystem="HRA_SPA";
$dbpasswordSystem="HRA_SPA";
$databaseSystem="LOCALHOST";
$servernameSystem="LOCALHOST";

$gSQLMaxRows = 1000; // max no of rows to download
$gSQLBlockRows=25; // default max no of rows per table block

$currentpagetitle=" Page : ";
$pagecounttitle=" from "	;

//TemplatePath 
//$tmpl_path="/ceda/www/UfisInfo/staff_onsite_jim/templates/";

$tmpl_path="templates/";

// Main Template name
$maintmpl="";


// Page Title
$PageTitle="";
$OrgDes="";
$RowsPerPage=0;
$RefreshRate=60; // default for the page refresh

// ExtraQuery
$ExtraQuery="";


//20070221 GFO: WAW Project : Add $LangTR for the Second Language Support
$LangTR = "";

//20070221 GFO: WAW Project : Add APC3 as a Global variable for function FindTDI in functions.php 
$APC3 = "ATH";

// If we want to Explode Code Share Flights We must enable this 
// And our Query must contain a JFNO field
$ExlodeCodeShareFlights = false;

$mailfrom="webmaster";
$APPName="Web StaffPages";

$APPVersion="4.5.0.4";
$APPReleaseDate="19/10/2007";
?>