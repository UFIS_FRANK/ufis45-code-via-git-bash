VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsBroadcasts"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private WithEvents BcProxy As BcProxyLib.BcPrxy
Attribute BcProxy.VB_VarHelpID = -1
Private lmBC As Long

Private Sub BcProxy_OnBcReceive(ByVal ReqId As String, ByVal Dest1 As String, ByVal Dest2 As String, ByVal Cmd As String, ByVal Object As String, ByVal Seq As String, ByVal Tws As String, ByVal Twe As String, ByVal Selection As String, ByVal Fields As String, ByVal Data As String, ByVal BcNum As String)
    lmBC = lmBC + 1
    If Cmd = "CLO" Then
        MsgBox "The server had switched." & vbCrLf & "You can restart the application in one minute."
        End
    End If
    Select Case Object
        Case "AFTTAB":
            HandleBC_AFTTAB Cmd, Selection, Fields, Data, BcNum
        Case "FIDTAB":
            HandleBC_FIDTAB Cmd, Selection, Fields, Data, BcNum
        Case "FLDTAB":
            HandleBC_FLDTAB Cmd, Selection, Fields, Data, BcNum
        Case Else:
            MsgBox "Non-registered BC received!" & vbCrLf & _
                "ReqId: " & ReqId & vbCrLf & _
                "Dest1: " & Dest1 & vbCrLf & _
                "Dest2: " & Dest2 & vbCrLf & _
                "Cmd: " & Cmd & vbCrLf & _
                "Object: " & Object & vbCrLf & _
                "Seq: " & Seq & vbCrLf & _
                "Tws: " & Tws & vbCrLf & _
                "Twe: " & Twe & vbCrLf & _
                "Selection: " & Selection & vbCrLf & _
                "Fields: " & Fields & _
                "Data: " & Data & vbCrLf & _
                "BcNum: " & BcNum, vbCritical
    End Select
End Sub

Public Sub HandleBC_AFTTAB(ByVal Cmd As String, ByVal Selection As String, ByVal Fields As String, ByVal Data As String, ByVal BcNum As String)
    Dim strUrno As String
    Dim strItem As String
    Dim strLineNo As String
    Dim strInsert As String

    strUrno = Trim(GetFieldValue("URNO", Data, Fields))

    If strUrno <> "" Then

        If Trim(Cmd) = "UFR" Then

            strLineNo = UfisServer.DataBuffer(4).GetLinesByColumnValue(14, strUrno, 2)

            If strLineNo <> "" Then

                If IsNumeric(strLineNo) = True Then

                    ' -> update internal data-holding
                    If GetItemNo(Fields, "FLNO") <> -1 Then
                        strItem = Trim(GetFieldValue("FLNO", Data, Fields))
                        UfisServer.DataBuffer(4).SetColumnValue CLng(strLineNo), 0, strItem
                    End If

                    If GetItemNo(Fields, "DES3") <> -1 Then
                        strItem = Trim(GetFieldValue("DES3", Data, Fields))
                        UfisServer.DataBuffer(4).SetColumnValue CLng(strLineNo), 1, strItem
                    End If

                    If GetItemNo(Fields, "STOD") <> -1 Then
                        strItem = Trim(GetFieldValue("STOD", Data, Fields))
                        UfisServer.DataBuffer(4).SetColumnValue CLng(strLineNo), 2, strItem
                    End If

                    If GetItemNo(Fields, "ETDI") <> -1 Then
                        strItem = Trim(GetFieldValue("ETDI", Data, Fields))
                        UfisServer.DataBuffer(4).SetColumnValue CLng(strLineNo), 3, strItem
                    End If

                    If GetItemNo(Fields, "ETOD") <> -1 Then
                        strItem = Trim(GetFieldValue("ETOD", Data, Fields))
                        UfisServer.DataBuffer(4).SetColumnValue CLng(strLineNo), 4, strItem
                    End If

                    If GetItemNo(Fields, "GTD1") <> -1 Then
                        strItem = Trim(GetFieldValue("GTD1", Data, Fields))
                        UfisServer.DataBuffer(4).SetColumnValue CLng(strLineNo), 5, strItem
                    End If

                    If GetItemNo(Fields, "GTD2") <> -1 Then
                        strItem = Trim(GetFieldValue("GTD2", Data, Fields))
                        UfisServer.DataBuffer(4).SetColumnValue CLng(strLineNo), 6, strItem
                    End If

                    If GetItemNo(Fields, "GD1B") <> -1 Then
                        strItem = Trim(GetFieldValue("GD1B", Data, Fields))
                        UfisServer.DataBuffer(4).SetColumnValue CLng(strLineNo), 7, strItem
                    End If

                    If GetItemNo(Fields, "GD1E") <> -1 Then
                        strItem = Trim(GetFieldValue("GD1E", Data, Fields))
                        UfisServer.DataBuffer(4).SetColumnValue CLng(strLineNo), 8, strItem
                    End If

                    If GetItemNo(Fields, "GD1X") <> -1 Then
                        strItem = Trim(GetFieldValue("GD1X", Data, Fields))
                        UfisServer.DataBuffer(4).SetColumnValue CLng(strLineNo), 9, strItem
                    End If

                    If GetItemNo(Fields, "GD1Y") <> -1 Then
                        strItem = Trim(GetFieldValue("GD1Y", Data, Fields))
                        UfisServer.DataBuffer(4).SetColumnValue CLng(strLineNo), 10, strItem
                    End If

                    If GetItemNo(Fields, "GD2Y") <> -1 Then
                        strItem = Trim(GetFieldValue("GD2Y", Data, Fields))
                        UfisServer.DataBuffer(4).SetColumnValue CLng(strLineNo), 11, strItem
                    End If

                    If GetItemNo(Fields, "TIFD") <> -1 Then
                        strItem = Trim(GetFieldValue("TIFD", Data, Fields))
                        UfisServer.DataBuffer(4).SetColumnValue CLng(strLineNo), 12, strItem
                    End If

                    If GetItemNo(Fields, "REMP") <> -1 Then
                        strItem = Trim(GetFieldValue("REMP", Data, Fields))
                        UfisServer.DataBuffer(4).SetColumnValue CLng(strLineNo), 13, strItem
                    End If

                    If GetItemNo(Fields, "GD2X") <> -1 Then
                        strItem = Trim(GetFieldValue("GD2X", Data, Fields))
                        UfisServer.DataBuffer(4).SetColumnValue CLng(strLineNo), 15, strItem
                    End If

                    If GetItemNo(Fields, "FLTI") <> -1 Then
                        strItem = Trim(GetFieldValue("FLTI", Data, Fields))
                        UfisServer.DataBuffer(4).SetColumnValue CLng(strLineNo), 16, strItem
                    End If

                End If
            Else
                ' we have to look after the record, if we have to insert it into our data-holding
                Dim blLoad As Boolean
                blLoad = True

                ' check the correct gate
                Dim strGTD1 As String
                Dim strGTD2 As String
                strGTD1 = Trim(GetFieldValue("GTD1", Data, Fields))
                strGTD2 = Trim(GetFieldValue("GTD2", Data, Fields))
                If (Len(strGTD1) > 0 And strGTD1 <> UfisServer.Gate.Text) And _
                   (Len(strGTD2) > 0 And strGTD2 <> UfisServer.Gate.Text) Then
                    blLoad = False
                End If

                ' check the correct timeframe of departure
                Dim strTIFD As String
                strTIFD = Trim(GetFieldValue("TIFD", Data, Fields))
                If Len(strTIFD) > 0 Then
                    Dim tmpVpfr As String
                    Dim tmpVpto As String
                    tmpVpfr = GetTimeStampUTC(UfisServer.Offset1.Text)
                    tmpVpto = GetTimeStampUTC(CInt(UfisServer.Offset2.Text) + 60)
                    If strTIFD < tmpVpfr Or tmpVpto < strTIFD Then
                        blLoad = False
                    End If
                End If

                ' check the correct Allocation Gate times (they must be empty)
                Dim strFLTI As String
                Dim strGD1Y As String
                Dim strGD2Y As String
                strFLTI = Trim(GetFieldValue("FLTI", Data, Fields))
                If strFLTI = "M" And UfisServer.txtGateType.Text = "DOM" Then
                    strGD2Y = Trim(GetFieldValue("GD2Y", Data, Fields))
                Else
                    strGD1Y = Trim(GetFieldValue("GD1Y", Data, Fields))
                End If
                If Len(strGD1Y) > 0 Or Len(strGD2Y) > 0 Then
                    blLoad = False
                End If

                ' check the correct gate-type
                If Len(strFLTI) > 0 Then
                    If UfisServer.txtFldRtyp.Text = "INT" Then
                        ' for the type "INT" only "I" and "M" are allowed
                        If strFLTI = "D" Then
                            blLoad = False
                        End If
                    ElseIf UfisServer.txtFldRtyp.Text = "DOM" Then
                        ' for the type "DOM" only "D" and "M" are allowed
                        If strFLTI = "I" Then
                            blLoad = False
                        End If
                    End If
                End If

                ' if the record fits or if we don't know exactly if it fits, let's reload
                If blLoad = True Then 'it is still true...
                    Dim strFieldlist As String
                    Dim strSqlKey As String
                    Dim strData As String
                    strFieldlist = "FLNO,DES3,STOD,ETDI,ETOD,GTD1,GTD2,GD1B,GD1E,GD1X,GD1Y,GD2Y,TIFD,REMP,URNO,GD2X,FLTI"
                    strSqlKey = "WHERE URNO = '" + strUrno + "'"
                    If frmSplash.UfisCom1.CallServer("RT", "AFTTAB", strFieldlist, "", strSqlKey, "3600") = 0 Then
                        If frmSplash.UfisCom1.GetBufferCount > 0 Then
                            UfisServer.DataBuffer(4).InsertTextLine frmSplash.UfisCom1.GetBufferLine(0), True
                        End If
                    End If
                End If

            End If
        End If
    End If
End Sub

Public Sub HandleBC_FIDTAB(ByVal Cmd As String, ByVal Selection As String, ByVal Fields As String, ByVal Data As String, ByVal BcNum As String)
    Dim strUrno As String
    Dim strCODE As String
    Dim strBEMD As String
    Dim strBEME As String
    Dim strREMT As String
    Dim strBET3 As String
    Dim strBET4 As String
    Dim strLineNo As String

    strUrno = Trim(GetFieldValue("URNO", Data, Fields))

    If strUrno <> "" Then 'if there's no URNO, throw it away!
        strCODE = Trim(GetFieldValue("CODE", Data, Fields))
        strBEMD = Trim(GetFieldValue("BEMD", Data, Fields))
        strBEME = Trim(GetFieldValue("BEME", Data, Fields))
        strREMT = Trim(GetFieldValue("REMT", Data, Fields))
        strUrno = Trim(GetFieldValue("URNO", Data, Fields))
        strBET3 = Trim(GetFieldValue("BET3", Data, Fields))
        strBET4 = Trim(GetFieldValue("BET4", Data, Fields))

        If strREMT <> "G" Then
            Exit Sub
        End If

    ' --- case URT
        If Trim(Cmd) = "URT" Then

            strLineNo = UfisServer.DataBuffer(5).GetLinesByColumnValue(4, strUrno, 2)
            If Len(strLineNo) > 0 And IsNumeric(strLineNo) = True Then
                If strCODE = "" Then
                    strCODE = Trim(UfisServer.DataBuffer(5).GetColumnValue(CLng(strLineNo), 0))
                End If
                If strBEMD = "" Then
                    strBEMD = Trim(UfisServer.DataBuffer(5).GetColumnValue(CLng(strLineNo), 1))
                End If
                If strBEME = "" Then
                    strBEME = Trim(UfisServer.DataBuffer(5).GetColumnValue(CLng(strLineNo), 2))
                End If
                If strREMT = "" Then
                    strREMT = Trim(UfisServer.DataBuffer(5).GetColumnValue(CLng(strLineNo), 3))
                End If
                If strUrno = "" Then
                    strUrno = Trim(UfisServer.DataBuffer(5).GetColumnValue(CLng(strLineNo), 4))
                End If
                If strBET3 = "" Then
                    strBET3 = Trim(UfisServer.DataBuffer(5).GetColumnValue(CLng(strLineNo), 5))
                End If
                If strBET4 = "" Then
                    strBET4 = Trim(UfisServer.DataBuffer(5).GetColumnValue(CLng(strLineNo), 6))
                End If
            End If

            Dim strUpdate As String
            strUpdate = strCODE & "," & strBEMD & "," & strBEME & "," & strREMT & "," & strUrno & "," & strBET3 & "," & strBET4

            If Len(strLineNo) > 0 And IsNumeric(strLineNo) = True Then
                UfisServer.DataBuffer(5).UpdateTextLine strLineNo, strUpdate, False
            Else
                UfisServer.DataBuffer(5).InsertTextLine strUpdate, False
            End If

        End If

    ' --- case IRT
        If Trim(Cmd) = "IRT" Then
            If strCODE = "" Then
                Exit Sub 'we don't want to demand missing data from the server!
            Else
                Dim strInsert As String
                strInsert = strCODE & "," & strBEMD & "," & strBEME & "," & strREMT & "," & strUrno & "," & strBET3 & "," & strBET4
                UfisServer.DataBuffer(5).InsertTextLine strInsert, False
            End If
        End If

    ' --- case DRT
        If Trim(Cmd) = "DRT" Then
            strLineNo = UfisServer.DataBuffer(5).GetLinesByColumnValue(4, strUrno, 2)
            If strLineNo = "" Then
                Exit Sub
            ElseIf Len(strLineNo) > 0 And IsNumeric(strLineNo) = True Then
                UfisServer.DataBuffer(5).DeleteLine CLng(strLineNo)
            End If
        End If
    End If

    UfisServer.DataBuffer(5).Sort "0", True, True
    UfisServer.DataBuffer(5).RedrawTab
    Flights.FillRemarkCombo
End Sub

Public Sub HandleBC_FLDTAB(ByVal Cmd As String, ByVal Selection As String, ByVal Fields As String, ByVal Data As String, ByVal BcNum As String)
    Dim strUrno As String
    Dim strAurn As String
    Dim strRtyp As String
    Dim strRnam As String
    Dim strLineNo As String

    ' --- case IRT
    If Trim(Cmd) = "IRT" Then
        strUrno = Trim(GetFieldValue("URNO", Data, Fields))

        If strUrno <> "" Then 'if there's no URNO, throw it away!
            strAurn = Trim(GetFieldValue("AURN", Data, Fields))
            strRtyp = Trim(GetFieldValue("RTYP", Data, Fields))
            strRnam = Trim(GetFieldValue("RNAM", Data, Fields))

            If strRtyp <> UfisServer.txtFldRtyp.Text Or strRnam <> UfisServer.Gate.Text Then
                Exit Sub
            Else
                Dim strInsert As String
                strInsert = strRtyp & "," & strRnam & "," & strAurn & "," & strUrno
                UfisServer.DataBuffer(7).InsertTextLine strInsert, False
            End If
        End If
    End If

    ' --- case DRT
    If Trim(Cmd) = "DRT" Then
        strUrno = Trim(Selection)
        strLineNo = UfisServer.DataBuffer(7).GetLinesByColumnValue(3, strUrno, 2)
        If IsNumeric(strLineNo) = True And Len(strLineNo) > 0 Then
            UfisServer.DataBuffer(7).DeleteLine CLng(strLineNo)
        End If
    End If

End Sub

Public Sub InitBroadcasts()
    frmSplash.List1.AddItem ("Loading BcProxy ...")
    frmSplash.ProgressBar1.Value = frmSplash.ProgressBar1.Value + 1
    Set BcProxy = New BcPrxy
End Sub

Private Sub RegisterObject(Table As String, Command As String)
    BcProxy.RegisterObject Table, Command
End Sub

Private Sub UnregisterObject(Table As String, Command As String)
    On Error GoTo ErrHdl
    BcProxy.UnregisterObject Table, Command
    Exit Sub
ErrHdl:
    If lmBC = 0 Then
        lmBC = 1
        MsgBox "Someone killed BcProxy.exe...", vbInformation
    End If
End Sub

Public Sub RegisterBC()
    RegisterObject "AFTTAB", "UFR"

    RegisterObject "FLDTAB", "IRT"
    RegisterObject "FLDTAB", "DRT"

    RegisterObject "FIDTAB", "URT"
    RegisterObject "FIDTAB", "IRT"
    RegisterObject "FIDTAB", "DRT"
End Sub

Public Sub UnRegisterBC()
    UnregisterObject "AFTTAB", "UFR"

    UnregisterObject "FLDTAB", "IRT"
    UnregisterObject "FLDTAB", "DRT"

    UnregisterObject "FIDTAB", "URT"
    UnregisterObject "FIDTAB", "IRT"
    UnregisterObject "FIDTAB", "DRT"
End Sub
