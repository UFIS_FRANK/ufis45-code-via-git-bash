VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form Flights 
   Caption         =   "Gate Information"
   ClientHeight    =   6720
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11760
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   8.25
      Charset         =   161
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   FontTransparent =   0   'False
   Icon            =   "Flights.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6720
   ScaleWidth      =   11760
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer TimerReload 
      Interval        =   60000
      Left            =   5445
      Top             =   1350
   End
   Begin VB.Timer TimerRefresh 
      Interval        =   1000
      Left            =   5400
      Top             =   495
   End
   Begin VB.Frame FlightFrame 
      Height          =   2175
      Left            =   120
      TabIndex        =   15
      Top             =   480
      Width           =   4215
      Begin TABLib.TAB FlightList 
         Height          =   1635
         Left            =   270
         TabIndex        =   16
         Top             =   270
         Width           =   3675
         _Version        =   65536
         _ExtentX        =   6482
         _ExtentY        =   2884
         _StockProps     =   0
      End
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   13
      Top             =   6405
      Width           =   11760
      _ExtentX        =   20743
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   20241
         EndProperty
      EndProperty
   End
   Begin VB.CheckBox chkLoad 
      Caption         =   "Load"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   45
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Frame InputFrame 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3255
      Left            =   8070
      TabIndex        =   7
      Top             =   270
      Width           =   3285
      Begin VB.CommandButton Submit 
         Cancel          =   -1  'True
         Caption         =   "Submit"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1080
         MaskColor       =   &H8000000F&
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   2640
         Width           =   1095
      End
      Begin VB.ComboBox cboRemark 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         ItemData        =   "Flights.frx":030A
         Left            =   930
         List            =   "Flights.frx":030C
         TabIndex        =   5
         Top             =   1935
         Width           =   2235
      End
      Begin VB.TextBox GD1B 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   930
         TabIndex        =   1
         ToolTipText     =   "Time hh:mm"
         Top             =   1095
         Width           =   615
      End
      Begin VB.TextBox GD1E 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1620
         TabIndex        =   2
         ToolTipText     =   "Time hh:mm"
         Top             =   1095
         Width           =   615
      End
      Begin VB.TextBox GD1X 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   930
         TabIndex        =   3
         ToolTipText     =   "Time hh:mm"
         Top             =   1530
         Width           =   615
      End
      Begin VB.TextBox GD1Y 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1620
         TabIndex        =   4
         ToolTipText     =   "Time hh:mm"
         Top             =   1530
         Width           =   615
      End
      Begin VB.Label lblGateType 
         Alignment       =   2  'Center
         Caption         =   "(international)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   855
         TabIndex        =   18
         Top             =   495
         Width           =   1455
      End
      Begin VB.Label LblGate 
         Alignment       =   2  'Center
         Caption         =   "Gate"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   855
         TabIndex        =   17
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label Label6 
         Caption         =   "Remark"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   150
         TabIndex        =   14
         Top             =   2025
         Width           =   1095
      End
      Begin VB.Label Label4 
         Caption         =   "Close"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1650
         TabIndex        =   11
         Top             =   855
         Width           =   495
      End
      Begin VB.Label Label3 
         Caption         =   "Open"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1020
         TabIndex        =   10
         Top             =   855
         Width           =   495
      End
      Begin VB.Label Label2 
         Caption         =   "Actual"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   150
         TabIndex        =   9
         Top             =   1620
         Width           =   705
      End
      Begin VB.Label Label1 
         Caption         =   "Sched."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   165
         Left            =   150
         TabIndex        =   8
         Top             =   1200
         Width           =   705
      End
   End
   Begin VB.CheckBox chkExit 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   900
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   30
      Visible         =   0   'False
      Width           =   855
   End
End
Attribute VB_Name = "Flights"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim cboRemarkText As String

Public BC As clsBroadcasts

Private ilCountMinutes As Integer
Private blFirst As Boolean
Private strLastClickedUrno As String
Private imLastSelectedLine As Integer
Private Const cGrey = 12632256

Private bRemark_Change As Boolean

Private Sub cboRemark_Click()
    If cboRemarkText <> cboRemark.Text Then
        Submit.BackColor = vbRed
        cboRemarkText = Left$(cboRemark.Text, 3)
        bRemark_Change = True
    Else
        Submit.BackColor = vbButtonFace
        bRemark_Change = False
    End If
End Sub

Private Sub FlightList_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    SendLButtonClick LineNo, ColNo, True
End Sub

Private Sub FlightList_SendRButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    SendLButtonClick LineNo, ColNo, True
End Sub

'Private Sub FlightList_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
'    Dim llTextColor As Long
'    Dim llBackColor As Long
'    FlightList.GetLineColor LineNo, llTextColor, llBackColor
'    If llBackColor = cGrey Then
'        FlightList.SetCurrentSelection imLastSelectedLine
'    Else
'        If Selected = True Then
'            imLastSelectedLine = CInt(LineNo)
'        End If
'    End If
'
'End Sub

Private Sub Form_Load()
    ' register for BCs
    Set BC = New clsBroadcasts
    BC.InitBroadcasts
    BC.RegisterBC

    ilCountMinutes = 0
    blFirst = True
    bRemark_Change = False
    imLastSelectedLine = -1

    sgUTCOffsetMinutes = GetUTCOffset

    Load UfisServer

    InitGrids

    ' loading necessary data from the DB
    chkLoad_Click

    ' looking after enabling/disabling of controls
    HandlePrivileges

    LblGate.Caption = "Gate " & UfisServer.Gate.Text
    If UfisServer.txtGateType.Text = "INT" Then
        lblGateType.Caption = "(international)"
    ElseIf UfisServer.txtGateType.Text = "DOM" Then
        lblGateType.Caption = "(domestic)"
    End If

    If UfisServer.UTCorLocal.Text = False Then
        Flights.Caption = Flights.Caption + " (UTC Times)"
    Else
        Flights.Caption = Flights.Caption + " (Local Times)"
    End If
End Sub

Private Sub InitGrids()
    InitFlightList 21
    InitInputCover
    InitMainMask
End Sub

Private Sub InitFlightList(ipLines As Integer)
    Dim i As Integer
    Dim hdrLenLst As String

    hdrLenLst = ""
    'FLNO 0
    hdrLenLst = hdrLenLst & TextWidth("OAL1234 F") / Screen.TwipsPerPixelX + 30 & ","
    'DESC3 1
    hdrLenLst = hdrLenLst & TextWidth("FRA") / Screen.TwipsPerPixelX + 30 & ","
    'DATE 2
    hdrLenLst = hdrLenLst & TextWidth("00.00.00") / Screen.TwipsPerPixelX + 30 & ","
    'STOD 3
    hdrLenLst = hdrLenLst & TextWidth("00:00") / Screen.TwipsPerPixelX + 30 & ","
    'ETDI 4
    hdrLenLst = hdrLenLst & TextWidth("00:00") / Screen.TwipsPerPixelX + 30 & ","
    'ETOD 5
    hdrLenLst = hdrLenLst & TextWidth("00:00") / Screen.TwipsPerPixelX + 30 & ","
    'REMP 6
    hdrLenLst = hdrLenLst & TextWidth("WWWWW") / Screen.TwipsPerPixelX + 30 & ","
    'URNO 7
    hdrLenLst = hdrLenLst & "0,"
    'GateType
    hdrLenLst = hdrLenLst & TextWidth("W") / Screen.TwipsPerPixelX + 22 & ","

    FlightList.ResetContent
    FlightList.FontName = "Courier New"
    FlightList.SetTabFontBold True
    FlightList.HeaderFontSize = 18
    FlightList.FontSize = 18
    FlightList.LineHeight = 18
    FlightFrame.Top = 0
    FlightFrame.Left = 0
    FlightList.Top = FlightFrame.Top + 1 * Screen.TwipsPerPixelY
    FlightList.Left = FlightFrame.Left + 1 * Screen.TwipsPerPixelX
    FlightList.Height = ipLines * FlightList.LineHeight * Screen.TwipsPerPixelY
    FlightFrame.Height = FlightList.Height + 2 * Screen.TwipsPerPixelY
    FlightList.Width = 520 * Screen.TwipsPerPixelX '378 * Screen.TwipsPerPixelX
    FlightFrame.Width = FlightList.Width + 2 * Screen.TwipsPerPixelX
    FlightList.HeaderLengthString = hdrLenLst
    FlightList.HeaderString = "FlightNo,To,DATE,STD,ETD,ETOD,REMP,URNO,G"
    FlightList.HeaderAlignmentString = "C,C,C,C,C,C,C,C,C,C"
    FlightList.ColumnAlignmentString = "L,L,C,C,C,C,L,L,C,C"
    FlightList.Top = 0
    FlightList.Left = 0
    FlightList.ShowHorzScroller (True)
    FlightList.ShowVertScroller (True)

    FlightList.DateTimeSetColumn 2
    FlightList.DateTimeSetColumn 3
    FlightList.DateTimeSetColumn 4
    FlightList.DateTimeSetColumn 5
    FlightList.DateTimeSetInputFormatString 2, "YYYYMMDDhhmmss"
    FlightList.DateTimeSetInputFormatString 3, "YYYYMMDDhhmmss"
    FlightList.DateTimeSetInputFormatString 4, "YYYYMMDDhhmmss"
    FlightList.DateTimeSetInputFormatString 5, "YYYYMMDDhhmmss"
    FlightList.DateTimeSetOutputFormatString 2, "DD'.'MM'.'YY"
    FlightList.DateTimeSetOutputFormatString 3, "hh':'mm"
    FlightList.DateTimeSetOutputFormatString 4, "hh':'mm"
    FlightList.DateTimeSetOutputFormatString 5, "hh':'mm"
    If UfisServer.UTCorLocal = "TRUE" Then
        FlightList.DateTimeSetUTCOffsetMinutes 2, sgUTCOffsetMinutes
        FlightList.DateTimeSetUTCOffsetMinutes 3, sgUTCOffsetMinutes
        FlightList.DateTimeSetUTCOffsetMinutes 4, sgUTCOffsetMinutes
        FlightList.DateTimeSetUTCOffsetMinutes 5, sgUTCOffsetMinutes
    End If
End Sub

Private Sub InitInputCover()
    InputFrame.Left = FlightFrame.Left + FlightFrame.Width
    InputFrame.Top = 0 'CommonFrame.Top - 6 * Screen.TwipsPerPixelY
    InputFrame.ZOrder
End Sub

Private Sub InitMainMask()
    Flights.Height = FlightFrame.Top + FlightFrame.Height + StatusBar.Height + 29 * Screen.TwipsPerPixelY
    Flights.Width = FlightFrame.Width + InputFrame.Width + 32 * Screen.TwipsPerPixelX
End Sub

Private Sub chkLoad_Click()
    Dim pclFldLst As String
    Dim pclSqlKey As String
    Dim pclResult As String
    Dim pclDatLst As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim llCount As Long

'------------ Loading AFTTAB
    If blFirst = True Then
        frmSplash.List1.AddItem ("Loading AFTTAB ...")
        frmSplash.List1.Refresh
        frmSplash.ProgressBar1.Value = frmSplash.ProgressBar1.Value + 1
    End If

    UfisServer.DataBuffer(4).ResetContent

    pclFldLst = "FLNO,DES3,STOD,ETDI,ETOD,GTD1,GTD2,GD1B,GD1E,GD1X,GD1Y,GD2Y,TIFD,REMP,URNO,GD2X,FLTI"
    tmpVpfr = GetTimeStampUTC(UfisServer.Offset1.Text)
    tmpVpto = GetTimeStampUTC(CInt(UfisServer.Offset2.Text) + 60)
    pclSqlKey = "WHERE (TIFD BETWEEN '" & tmpVpfr & "' AND '" & tmpVpto & "')"
    pclSqlKey = pclSqlKey & " AND (GTD1='" & UfisServer.Gate.Text & "' OR GTD2='" & UfisServer.Gate.Text & "')"
    'pclSqlKey = pclSqlKey & " AND (GD1Y = ' ' AND GD2Y = ' ')"
    UfisServer.CallCeda pclResult, "RT", "AFTTAB", pclFldLst, pclDatLst, pclSqlKey, "TIFD,FLNO,REMP", 4, False, True

'------------ Loading FIDTAB (only at the very first time)
    If blFirst = True Then
        frmSplash.List1.AddItem (CStr(llCount + 1) & " records loaded.")
        frmSplash.List1.AddItem ("Loading FIDTAB ...")
        frmSplash.List1.Refresh
        frmSplash.ProgressBar1.Value = frmSplash.ProgressBar1.Value + 1
        UfisServer.DataBuffer(5).ResetContent
        pclFldLst = "CODE,BEMD,BEME,REMT,URNO,BET3,BET4"
        pclSqlKey = "WHERE REMT='G'"
        UfisServer.CallCeda pclResult, "RT", "FIDTAB", pclFldLst, pclDatLst, pclSqlKey, "CODE", 5, False, True
        FillRemarkCombo
    End If

'------------ Loading FLDTAB
    If blFirst = True Then
        frmSplash.List1.AddItem (CStr(llCount + 1) & " records loaded.")
        frmSplash.List1.AddItem ("Loading FLDTAB ...")
        frmSplash.List1.Refresh
        frmSplash.ProgressBar1.Value = frmSplash.ProgressBar1.Value + 1
    End If

    UfisServer.DataBuffer(7).ResetContent
    pclFldLst = "RTYP,RNAM,AURN,URNO"
    pclSqlKey = "WHERE RTYP='" & UfisServer.txtFldRtyp.Text & "' AND RNAM='" & UfisServer.Gate.Text & "'"
    UfisServer.CallCeda pclResult, "RT", "FLDTAB", pclFldLst, pclDatLst, pclSqlKey, "", 7, False, True
    UfisServer.DataBuffer(7).Sort "2", True, True

    If blFirst = True Then
        frmSplash.List1.AddItem (CStr(llCount + 1) & " records loaded.")
        blFirst = False
    End If

    chkLoad.Value = 0
End Sub

Private Sub HandlePrivileges()
    If frmSplash.AATLoginControl1.GetPrivileges("m_GD1B") <> "1" Then
        GD1B.Enabled = False
    End If

    If frmSplash.AATLoginControl1.GetPrivileges("m_GD1E") <> "1" Then
        GD1E.Enabled = False
    End If

    If frmSplash.AATLoginControl1.GetPrivileges("m_GD1X") <> "1" Then
        GD1X.Enabled = False
    End If

    If frmSplash.AATLoginControl1.GetPrivileges("m_GD1Y") <> "1" Then
        GD1Y.Enabled = False
    End If

    If frmSplash.AATLoginControl1.GetPrivileges("m_REMP") <> "1" Then
        cboRemark.Enabled = False
    End If

    If frmSplash.AATLoginControl1.GetPrivileges("m_Submit") <> "1" Then
        Submit.Enabled = False
    End If
End Sub

Private Sub FlightList_SendLButtonClick(ByVal Line As Long, ByVal Col As Long)
    SendLButtonClick Line, Col, True
End Sub

Private Sub SendLButtonClick(ByVal Line As Long, ByVal Col As Long, ByRef bFromTab As Boolean)
    Dim strUrno As String
    Dim strLineNo As String
    Dim strRemp As String
    Dim strGD1B As String
    Dim strGD1E As String
    Dim strGD1X As String
    Dim strGD1Y As String

    If Line = -1 Then
        strLastClickedUrno = ""
        Exit Sub
    End If

    Dim llTextColor As Long
    Dim llBackColor As Long
    FlightList.GetLineColor Line, llTextColor, llBackColor
    If llBackColor = cGrey Then
        If strLastClickedUrno = "" Then
            FlightList.SetCurrentSelection -1
            FlightList.ShowRowSelection = False
        Else
            strLineNo = FlightList.GetLinesByColumnValue(7, strLastClickedUrno, 2)
            If Len(strLineNo) > 0 And IsNumeric(strLineNo) = True Then
                FlightList.SetCurrentSelection CLng(strLineNo)
                FlightList.ShowRowSelection = True
            Else
                FlightList.SetCurrentSelection -1
                FlightList.ShowRowSelection = False
            End If
        End If
        FlightList.RedrawTab
        Me.Refresh
        Exit Sub
    End If

    FlightList.ShowRowSelection = True

    Screen.MousePointer = vbHourglass
    If Line >= 0 Then
        strUrno = FlightList.GetColumnValue(Line, 7)
        strLastClickedUrno = strUrno
        If strUrno <> "" Then
            strLineNo = UfisServer.DataBuffer(4).GetLinesByColumnValue(14, strUrno, 2)
            If IsNumeric(strLineNo) = True Then
                strRemp = Trim(UfisServer.DataBuffer(4).GetColumnValue(CLng(strLineNo), 13))
                strGD1B = GetValidTime(Trim(UfisServer.DataBuffer(4).GetColumnValue(CLng(strLineNo), 7)))
                strGD1E = GetValidTime(Trim(UfisServer.DataBuffer(4).GetColumnValue(CLng(strLineNo), 8)))
                strGD1X = GetValidTime(Trim(UfisServer.DataBuffer(4).GetColumnValue(CLng(strLineNo), 9)))
                strGD1Y = GetValidTime(Trim(UfisServer.DataBuffer(4).GetColumnValue(CLng(strLineNo), 10)))
            End If
        End If
    End If

    If (strGD1B = ":") Then
        strGD1B = ""
    End If

    If (strGD1E = ":") Then
        strGD1E = ""
    End If

    If (strGD1X = ":") Then
        strGD1X = ""
    End If

    If (strGD1Y = ":") Then
        strGD1Y = ""
    End If

    GD1B.Text = strGD1B
    GD1E.Text = strGD1E
    GD1X.Text = strGD1X
    GD1Y.Text = strGD1Y

    If bRemark_Change = False Then
        cboRemark.Text = strRemp
    End If

    If bFromTab = True Then
        bRemark_Change = False
        Submit.BackColor = vbButtonFace
    End If

    Screen.MousePointer = vbDefault
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Dim Msg, Response   ' Declare variables.
    Msg = LoadResString(1180)
    Response = MsgBox(Msg, vbQuestion + vbYesNo, "Closing Application")

    Select Case Response
        Case vbYes:
            Cancel = 0  'the MDI will be closed
        Case Else:
            Cancel = 1  'the MDI won't be closed
    End Select
End Sub

Private Sub Form_Resize()
    If Flights.ScaleHeight > StatusBar.Height + 2 * Screen.TwipsPerPixelY Then
        FlightList.Height = Flights.ScaleHeight - StatusBar.Height - 2 * Screen.TwipsPerPixelY
        FlightFrame.Height = FlightList.Height + 2 * Screen.TwipsPerPixelY
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Select Case Cancel
        Case 0:
            BC.UnRegisterBC
            Unload UfisServer
            Unload frmSplash

            If Me.WindowState <> vbMinimized Then
                SaveSetting App.Title, "Settings", "MainLeft", Me.Left
                SaveSetting App.Title, "Settings", "MainTop", Me.Top
                SaveSetting App.Title, "Settings", "MainWidth", Me.Width
                SaveSetting App.Title, "Settings", "MainHeight", Me.Height
            End If

        Case Else:
            'the MDI won't be closed!
    End Select
End Sub

Private Sub Submit_Click()
    Dim strNewRemp, strOldRemp, strLineValue, strDate As String
    Dim strUrno As String
    Dim strFieldlist As String
    Dim strData As String
    Dim strWhere As String
    Dim strNewGD1B, strNewGD1E, strNewGD1X, strNewGD1Y As String
    Dim strOldGD1B, strOldGD1E, strOldGD1X, strOldGD1Y As String
    Dim pclResult As String
    Dim RetVal As Integer
    Dim strLineNo As String

    Dim ilLine As Integer

    strNewRemp = Left(cboRemark.Text, 3)
    strNewGD1B = GD1B.Text
    strNewGD1E = GD1E.Text
    strNewGD1X = GD1X.Text
    strNewGD1Y = GD1Y.Text

    ilLine = FlightList.GetCurrentSelected

    If (ilLine = -1) Then
        Exit Sub
    End If

    strLineValue = FlightList.GetLineValues(ilLine)
    strOldRemp = FlightList.GetColumnValue(ilLine, 6)
    strDate = FlightList.GetColumnValue(ilLine, 2)
    strUrno = FlightList.GetColumnValue(ilLine, 7)
    strLineNo = UfisServer.DataBuffer(4).GetLinesByColumnValue(14, strUrno, 2)
    If strLineNo <> "" Then
        If IsNumeric(strLineNo) = True Then
            strOldGD1B = GetTimePart(Trim(UfisServer.DataBuffer(4).GetColumnValue(CLng(strLineNo), 7)))
            strOldGD1E = GetTimePart(Trim(UfisServer.DataBuffer(4).GetColumnValue(CLng(strLineNo), 8)))
            strOldGD1X = GetTimePart(Trim(UfisServer.DataBuffer(4).GetColumnValue(CLng(strLineNo), 9)))
            strOldGD1Y = GetTimePart(Trim(UfisServer.DataBuffer(4).GetColumnValue(CLng(strLineNo), 10)))
        End If
    End If

    If (strNewRemp = strOldRemp) And (strNewGD1B = strOldGD1B) And (strNewGD1E = strOldGD1E) And (strNewGD1X = strOldGD1X) And (strNewGD1Y = strOldGD1Y) Then
        Exit Sub
    End If

    strDate = VbDateStrgToCeda(strDate, "dd.mm.yy")
    strDate = "20" + strDate

    If (strNewGD1B = "") Then
        strNewGD1B = " "
    Else
        strNewGD1B = strDate + Left$(strNewGD1B, 2) + Right$(strNewGD1B, 2) + "00"
    End If

    If (strNewGD1E = "") Then
        strNewGD1E = " "
    Else
        strNewGD1E = strDate + Left$(strNewGD1E, 2) + Right$(strNewGD1E, 2) + "00"
    End If

    If (strNewGD1X = "") Then
        strNewGD1X = " "
    Else
        strNewGD1X = strDate + Left$(strNewGD1X, 2) + Right$(strNewGD1X, 2) + "00"
    End If

    If (strNewGD1Y = "") Then
        strNewGD1Y = " "
    Else
        strNewGD1Y = strDate + Left(strNewGD1Y, 2) + Right(strNewGD1Y, 2) + "00"
    End If

    strWhere = "WHERE URNO = " & strUrno
    'strWhere = "WHERE URNO = '" + strUrno + "'"
    strData = strNewRemp 'strNewGD1B + "," + strNewGD1E + "," + strNewGD1X + "," + strNewGD1Y + "," + strNewRemp
    strFieldlist = "REMP" '"GD1B,GD1E,GD1X,GD1Y,REMP"
    RetVal = frmSplash.UfisCom1.CallServer("UFR", "AFTTAB", strFieldlist, strData, strWhere, "360")

    Submit.BackColor = vbButtonFace
End Sub

Private Function GetUTCOffset() As Single
    Dim RetVal As Integer
    Dim pclFldLst As String
    Dim pclSqlKey As String
    Dim pclResult As String
    Dim pclDatLst As String
    Dim strTDI1 As String
    Dim strTDI2 As String
    Dim strTICH As String
    Dim tmpRec As String
    Dim Result As Single
    Dim strNow As String

    Result = 0

    UfisServer.DataBuffer(6).ResetContent
    pclFldLst = "TDI1,TDI2,TICH,APC3"
    pclSqlKey = "WHERE APC3 = '" + UfisServer.HOPO.Text + "'"
    RetVal = UfisServer.CallCeda(pclResult, "RT", "APTTAB", pclFldLst, pclDatLst, pclSqlKey, "APC3", 6, False, True)
    If (RetVal = 0) Then
        tmpRec = UfisServer.DataBuffer(6).GetLineValues(0)
        strTDI1 = GetItem(tmpRec, 1, ",")
        strTDI2 = GetItem(tmpRec, 2, ",")
        strTICH = GetItem(tmpRec, 3, ",")
        strNow = Format(Now, "yyyymmddhhmmss")
        If strNow > strTICH Then
            Result = CSng(strTDI2)
        Else
            Result = CSng(strTDI1)
        End If
    End If

    GetUTCOffset = Result
    Exit Function
End Function

Public Function GetValidTime(ByVal CedaDate As String) As String
    Dim strCedaDate As String
    Dim tmpTime 'as variant
    Dim UTCDate As Date

    If CedaDate <> "" Then
        strCedaDate = CedaDate
        If UfisServer.UTCorLocal = "FALSE" Then
            GetValidTime = GetTimePart(strCedaDate)
            Exit Function
        ElseIf UfisServer.UTCorLocal = "TRUE" Then
            UTCDate = CDate(CedaFullDateToVb(CedaDate))
            tmpTime = DateAdd("n", sgUTCOffsetMinutes, UTCDate)
            GetValidTime = Format(tmpTime, "hh:mm")
            Exit Function
        End If
    End If

    Exit Function
End Function

'check, if we have to delete records which are not in the timeframe any more
Private Sub TimerRefresh_Timer()
    Dim tmpVpfr As String
    Dim tmpVpto As String
    tmpVpfr = GetTimeStampUTC(UfisServer.Offset1.Text)
    tmpVpto = GetTimeStampUTC(UfisServer.Offset2.Text)

    Dim strLineNo As String
    Dim strUrno As String
    Dim l As Long
    Dim llLineCount As Long
    Dim strTIFD As String
    Dim strGTD1 As String
    Dim strGTD2 As String
    Dim strGate As String
    Dim strFLTI As String
    Dim strGD1Y As String
    Dim strGD2Y As String
    Dim strFLTIpossible As String

    strGate = UfisServer.Gate.Text

    llLineCount = UfisServer.DataBuffer(4).GetLineCount - 1
    For l = llLineCount To 0 Step -1
        strTIFD = Trim(UfisServer.DataBuffer(4).GetColumnValue(l, 12))
        strGTD1 = Trim(UfisServer.DataBuffer(4).GetColumnValue(l, 5))
        strGTD2 = Trim(UfisServer.DataBuffer(4).GetColumnValue(l, 6))
        strFLTI = Trim(UfisServer.DataBuffer(4).GetColumnValue(l, 16))
        strUrno = Trim(UfisServer.DataBuffer(4).GetColumnValue(l, 14))
        strLineNo = FlightList.GetLinesByColumnValue(7, strUrno, 2)
        If strFLTI = "M" And UfisServer.txtGateType.Text = "DOM" Then
            strGD1Y = ""
            strGD2Y = Trim(UfisServer.DataBuffer(4).GetColumnValue(l, 11))
        Else
            strGD1Y = Trim(UfisServer.DataBuffer(4).GetColumnValue(l, 10))
            strGD2Y = ""
        End If

        If (strTIFD < tmpVpfr Or tmpVpto < strTIFD) Or _
           (strGTD1 <> strGate And strGTD2 <> strGate) Or _
           (Len(strGD1Y) > 0 Or Len(strGD2Y) > 0) Then
            'record does not belong to timeframe, check to delete it
            If (Len(strLineNo) > 0) Then
                If (IsNumeric(strLineNo) = True) Then
                    FlightList.DeleteLine CLng(strLineNo)
                End If
            End If
        Else
            'record belongs to timeframe, check to add it or update
            Dim tmpRec As String
            Dim tmpLine As String

            'FLNO 0
            tmpLine = UfisServer.DataBuffer(4).GetColumnValue(l, 0) & ","
            'DESC3 1
            tmpLine = tmpLine & UfisServer.DataBuffer(4).GetColumnValue(l, 1) & ","
            'DATE 2
            tmpLine = tmpLine & UfisServer.DataBuffer(4).GetColumnValue(l, 2) & ","
            'STOD 3
            tmpLine = tmpLine & UfisServer.DataBuffer(4).GetColumnValue(l, 2) & ","
            'ETDI 4
            tmpLine = tmpLine & UfisServer.DataBuffer(4).GetColumnValue(l, 3) & ","
            'ETOD 5
            tmpLine = tmpLine & UfisServer.DataBuffer(4).GetColumnValue(l, 4) & ","
            'REMP 6
            tmpLine = tmpLine & UfisServer.DataBuffer(4).GetColumnValue(l, 13) & ","
            'URNO 7
            tmpLine = tmpLine & UfisServer.DataBuffer(4).GetColumnValue(l, 14) & ","
            'FLTI 8
            tmpLine = tmpLine & strFLTI & ","

            If Len(strLineNo) > 0 And IsNumeric(strLineNo) = True Then
                FlightList.UpdateTextLine CLng(strLineNo), tmpLine, False
            Else
                FlightList.InsertTextLine tmpLine, True
            End If
        End If
    Next l

    SortFlightList ' sorting the view

    If Len(strLastClickedUrno) > 0 Then
        strLineNo = FlightList.GetLinesByColumnValue(7, strLastClickedUrno, 2)
        If (Len(strLineNo) > 0) Then
            If (IsNumeric(strLineNo) = True) Then
                SendLButtonClick CLng(strLineNo), 0, False
                FlightList.SetCurrentSelection CLng(strLineNo)
            End If
        End If
    End If

    HandleColors ' coloring the view
    FlightList.RedrawTab
End Sub

Private Sub TimerReload_Timer()
    ilCountMinutes = ilCountMinutes + 1
    If ilCountMinutes = 60 Then
        ilCountMinutes = 0
        chkLoad_Click
    End If
End Sub

Private Sub SortFlightList()
    Dim strSortCols As String
    strSortCols = "2,3,0,6"
    Flights.FlightList.Sort strSortCols, True, True
    Flights.FlightList.RedrawTab
End Sub

Public Sub FillRemarkCombo()
    Dim llCount As Long
    Dim l As Long

    ' delete already existing entries
    Dim strOldText As String
    strOldText = cboRemark.Text
    cboRemark.Clear

    ' adding empty string
    cboRemark.AddItem ""

    ' adding remarks
    With UfisServer.DataBuffer(5)
        llCount = .GetLineCount - 1
        For l = 0 To llCount
            If UfisServer.RemarkText = "BEMD" Then
                cboRemark.AddItem .GetColumnValue(l, 0) & "  " & Trim(.GetColumnValue(l, 1))
            ElseIf UfisServer.RemarkText = "BEME" Then
                cboRemark.AddItem .GetColumnValue(l, 0) & "  " & Trim(.GetColumnValue(l, 2))
            ElseIf UfisServer.RemarkText = "BET3" Then
                cboRemark.AddItem .GetColumnValue(l, 0) & "  " & Trim(.GetColumnValue(l, 5))
            ElseIf UfisServer.RemarkText = "BET4" Then
                cboRemark.AddItem .GetColumnValue(l, 0) & "  " & Trim(.GetColumnValue(l, 6))
            End If
        Next
    End With

    cboRemark.Text = strOldText
End Sub

Private Sub HandleColors()
    Dim llCount As Long
    Dim l As Long
    Dim strAftUrno As String
    Dim strLineNo As String
    Dim strFLTI As String

    Dim strFLTIpossible As String
    If UfisServer.txtGateType.Text = "INT" Then
        strFLTIpossible = "IM"
    ElseIf UfisServer.txtGateType.Text = "DOM" Then
        strFLTIpossible = "DM"
    End If

    llCount = FlightList.GetLineCount - 1
    For l = 0 To llCount
        strFLTI = Trim(FlightList.GetColumnValue(l, 8))
        If InStr(1, strFLTIpossible, strFLTI, vbBinaryCompare) = 0 Then
            FlightList.SetLineColor l, vbBlack, cGrey
        Else
            strAftUrno = FlightList.GetColumnValue(l, 7)
            strLineNo = UfisServer.DataBuffer(7).GetLinesByColumnValue(2, strAftUrno, 2)
            If Len(strLineNo) > 0 Then
                FlightList.SetLineColor l, vbBlack, vbCyan
            Else
                FlightList.SetLineColor l, vbBlack, vbWhite
            End If
        End If
    Next l
End Sub
