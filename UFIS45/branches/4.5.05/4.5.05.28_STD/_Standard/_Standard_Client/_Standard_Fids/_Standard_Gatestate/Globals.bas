Attribute VB_Name = "Globals"
Option Explicit

' holding all employees
Public gcEmployee As New Collection

' holding all views
Public gcViews As New Collection

' name of the actual displayed view
'Public ogActualViewName As String

' information about the actual DAU
Public ogActualUser As String
Public ogActualUserURNO As String
Public ogActualUserPassword As String

' to initialize the detail window
Public ogActualEmployeeNameFirst As String
Public ogActualEmployeeNameSecond As String
Public ogActualEmployeeURNO As String
Public ogActualEmployeePENO As String

' to remember the split of the columns
Public ogMonthString As String
Public ogMainHeaderRanges As String

' lookup the letter attached to an absence code
Public gcAbsenceCodeLookup As New Collection

' hold all URNOs
Public ogURNO As New Collection

'hold the allowed maximum absent
Public gcMaxAbsent As New Collection

' get the command line arguments
Public ogCommandLine As String

' get the system metrics
Public ogSystemMetrics As New clsSystemMetrics

Public ogStartTime As Date

' looking up the absences not calculating hour-information
Public gcNoHrsCalculationAbsences As New Collection

'remember the UTC-offset
Public sgUTCOffsetMinutes As Single

