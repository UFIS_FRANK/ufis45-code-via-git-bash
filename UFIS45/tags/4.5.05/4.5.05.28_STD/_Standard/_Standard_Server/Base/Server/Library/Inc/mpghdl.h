#ifndef _DEF_mks_version_mpghdl_h
  #define _DEF_mks_version_mpghdl_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_mpghdl_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/mpghdl.h 1.2 2004/07/27 16:48:05SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The Master include file.                                              */
/*                                                                        */
/*  Program       :                                                       */
/*                                                                        */
/*  Revision date :                                                       */
/*                                                                        */
/*  Author    	  :                                                       */
/*                                                                        */
/*                                                                        */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */

/* defines for buffer sizes */
#define XXS_BUF 32
#define XS_BUF  128
#define S_BUF   512
#define M_BUF   1024
#define L_BUF   2048
#define XL_BUF  4096
#define XXL_BUF 8192
#define RES_BUF_SIZE 512000
