#ifndef _DEF_mks_version_tools_c
  #define _DEF_mks_version_tools_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_tools_c[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/tools.c 1.20 2008/10/01 16:55:00SGT mcu Exp  $";
#endif /* _DEF_mks_version */

/******************************************************************************
 *
 * History:
 * 20040406 JIM: Get_CEI_Cfg: read "Ceda Event Interface" configuration.           
 *
 * 20040820 JIM: iWriteConfigEntry: replace value only in named section 
 * 20040820 JIM: iWriteConfigEntry: add keyword to section , if missing
 * 20040820 JIM: iWriteConfigEntry: add section and keyword, if missing
 * 20050531 JIM: PRF 7443: do not move event in WMQ Environment 
 * 20050509 JIM: PRF 7374: GetDynamicQueueToMe with QUE_WMQ_RESP to get
 *               q>30000 to not close WMQ queue on QUE_PUT
 * 20060117 JIM: Specials for AIX also
 * 20070226 JIM: on AIX the pids are not continously increasing but random, so the
 * modulo may create same tag at 2 simultaniosly starting NETINs. Use complete PID
 ******************************************************************************
*/
 
#define STH_USE
#define STH_IS_USED_IN_LIB
#define iMAX_READ_BUF	2048

#define MAX_CT_SPOOL 5

#include <sys/types.h>
#include <sys/stat.h>



/* Define OWN_NAP, if the nap function is not supported by the unix */
#if defined(_SNI) || defined(_SOLARIS) || defined(_HPUX_SOURCE) || defined(_LINUX) || defined(_AIX)
#define OWN_NAP
#endif

#ifdef OWN_NAP
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <errno.h>
#include <time.h>
#include <sys/time.h>
#include <sys/param.h>
#endif


#include "tools.h"
#include "glbdef.h"
#include "initdef.h"
#include "netin.h"
#include "oracle.h"
#include "namsrv.h"
#include "debugrec.h"
#include "ct.h"

/* Unclear hack ... */
#ifdef _SOLARIS
int __ldmax=0x1000000;
#endif

#if !defined(_AIX)
#include <sys/fcntl.h>
#endif

#if defined(_SOLARIS)
#include <sys/procfs.h>
#endif


extern int iStrup(char *s);
extern char *sGet_word(char *s,short FldNum);
void time_stamp(char *s);
static void get_time(char *s);
static int StrnCaseCmp(char *,char *,int);
extern int errno;
extern int mod_id;
extern FILE *outp;
static short igSnoTabCursor=0;
static short igUrnoCursor;

static char pcgEvtSpool[] = "EVT_SPOOL";
static char pcgEvtDestName[64];
static char pcgEvtRecvName[64];
static char pcgEvtTwStart[64];
static char pcgEvtTwEnd[64];
static char pcgEvtFldSep[2];
static STR_DESC argSpoolField[MAX_CT_SPOOL+1];
static STR_DESC argSpoolData[MAX_CT_SPOOL+1];
static int igInitSpooler = FALSE;
static int ToolsInitEventSpooler(void);

/******************************************************************************
 *
 * Function: iGetConfigLine
 *
 * Parameters: char *file	the file name
 *             char *sect	the section name
 *             char *param	the keyword
 *             char *dest	the return value
 *
 * Returns: 	result of que() ?????
 *
 * Description: This function search for
 *              1. section name
 *              2. keyword
 *              and returns the whole line of the config file
 *
 *
 * Created by eth 24.10.2001 to solve problem with ftphdl password
 *
 *****************************************************************************/
int iGetConfigLine(char *file,char *sect,char *param,char *dest)
{
  FILE    *fp;
  char    sCfgin[iMAX_READ_BUF];
  char    StartSect[32]; /* Anfang der section */
  char    EndSect[32];   /* Ende der section */
  int     iValue;
  short   found;
 

  /* configfile oeffnen ... schon wieder ??? */
  if((fp = (FILE *)fopen(file,"r")) == (FILE *)NULL)
  {
    return E_FOPEN;
  } /* end if */
 
  /* iStrup(sect);*/ /* converts section-name to upper case letters */

  sprintf(StartSect, "[%s]", sect);
	iStrup(StartSect);

  found = FALSE;

  /* find the section in config file */ 
  while(fgets(sCfgin,iMAX_READ_BUF,fp))
  {
    iStrup(sCfgin);  /* converts a string to upper case letters */
    if(strncmp(sGet_word(sCfgin,1), StartSect, strlen(StartSect)) == 0 )
    {
      /* found the section */
      found = TRUE;
      break;
    } /* end if */
  } /* while */
 
  if ( found != TRUE )
  {
    /* section nicht gefunden ... und tschuess */
    fclose(fp);
    return E_SECTION;
  } /* end if */
 
  sprintf(EndSect, "[%s_END]", sect);
  found = FALSE;
 
  /* das keyword suchen */
  while( (fgets(sCfgin,iMAX_READ_BUF,fp)) && (found == FALSE))
  {
    if(strncmp(sGet_word(sCfgin,1), EndSect, strlen(EndSect)) == 0 )
    {
      /* keyword nicht gefunden ... und tschuess */
      break;
    } /* end if */

    if(strncmp(sGet_word(sCfgin,1), param, strlen(param)) == 0)
    {
      /* keyword gefunden ... und tschuess */
      found = TRUE;
      strcpy( dest, sCfgin);
      break;
    } /* end if */
  } /* end while */

  fclose(fp);
  return ( found == FALSE ) ? E_PARAM : OK;

} /* end of iGetConfigLine ... by eth 24.10.2001 */


int iGetConfigRow(char *file,char *sect,char *param,short type,char *dest)
{
	FILE	*fp;
	char	sCfgin[iMAX_READ_BUF];
	char	*pclPtr;
	char	LocalSect[32];
	int	i;
	int 	ilRC;
	int	iValue;
	short	found;
	
	/*Additional entry for multiline cfg entries*/ 
	if( type == CFG_MULTI)
	{
          /* Deactivated by BERNI. Uses Lex/Yak !!
	  ilRC = SearchPara(file,dest,sect,param,type);		
	  return ilRC;
          */
	  return RC_FAIL;
	}


	if ( (fp = (FILE *)fopen(file,"r")) == (FILE *)NULL) {
		return E_FOPEN;
	} /* end if */
	

	found = FALSE;
	/* iStrup(sect);*/
	/* iStrup(param); */

	sprintf(LocalSect,"[%s]",sect);
	iStrup(LocalSect);

	while (fgets(sCfgin,iMAX_READ_BUF,fp) ) {
		iStrup(sCfgin);  /* converts a string to upper case letters */
		if ( strncmp(sGet_word(sCfgin,1),LocalSect,strlen(LocalSect)) == 0 ) {
			found = TRUE;
        		break;
		} /* end if */
	} /* while */

	if ( found == FALSE ) {
		fclose(fp);
		return E_SECTION;
	} /* end if */

	sprintf(LocalSect,"[%s_END]",sect);
	found = FALSE;

	memset((void*)sCfgin, 0x00, iMAX_READ_BUF);
	while ((fgets(sCfgin,iMAX_READ_BUF,fp)) && (found == FALSE)   ) {
	/*	iStrup(sCfgin);  converts a string to upper case letters */
		if ( strncmp(sGet_word(sCfgin,1),LocalSect,strlen(LocalSect)) == 0 ) {
        		break;
		} /* end if */
		if ( strncmp(sGet_word(sCfgin,1),param,strlen(param)) == 0) {
			found = TRUE;
			switch ( type ) 
			{
				case CFG_STRING:
					pclPtr = sCfgin;

					/* delete all blanks at front */
					while (*pclPtr && isspace(*pclPtr))
						pclPtr++;

					for (i=0; i<2; i++)
					{
						/* delete entry number i */
						while (*pclPtr && !isspace(*pclPtr))
							pclPtr++;

						/* delete blanks after entry */
						while (*pclPtr && isspace(*pclPtr))
							pclPtr++;
					}

					/* now copy to calling routine... */
					i=0;
					while (*pclPtr && *pclPtr != 0x0D && *pclPtr != 0x0A)
				 	{
						dest[i++] = *pclPtr;
						pclPtr++;
					}
					dest[i] = '\0';

					/* delete all blanks at end of string */
					/*i = strlen(dest);*/
					while (i > 0 && dest[i-1] == ' '){
						dest[i-1] = '\0';
						i--;
					}
					break;	

				case CFG_INT:
					iValue =(int)atoi(sGet_word(sCfgin,3));
					memcpy(dest,&iValue,sizeof(int));
					break;	
			} /* end switch */
		} /* end if */
	} /* end while */

	fclose(fp);
	return ( found == FALSE ) ? E_PARAM : OK;	

} /* end of iGetConfigRow */

int iGetConfigEntry(char *file,char *sect,char *param,short type,char *dest)
{
	FILE	*fp;
	char	sCfgin[iMAX_READ_BUF];
	char	LocalSect[32];
	int	iValue;
	short	found;

	if ( (fp = (FILE *)fopen(file,"r")) == (FILE *)NULL) {
		return E_FOPEN;
	} /* end if */

	found = FALSE;
	/* iStrup(param); */

	sprintf(LocalSect,"[%s]",sect);

	iStrup(LocalSect);

	while (fgets(sCfgin,iMAX_READ_BUF,fp) ) {
		iStrup(sCfgin);  /* converts a string to upper case letters */
		if ( strncmp(sGet_word(sCfgin,1),LocalSect,strlen(LocalSect)) == 0 ) {
			found = TRUE;
        		break;
		} /* end if */
	} /* while */


	if ( found == FALSE ) {
		fclose(fp);
		return E_SECTION;
	} /* end if */

	sprintf(LocalSect,"[%s_END]",sect);
	iStrup(LocalSect);
	found = FALSE;

	while ((fgets(sCfgin,iMAX_READ_BUF,fp)) && (found == FALSE)   ) {
	/*	iStrup(sCfgin);  converts a string to upper case letters */
		if ( strncmp(sGet_word(sCfgin,1),LocalSect,strlen(LocalSect)) == 0 ) {
        		break;
		} /* end if */
		if ( strncmp(sGet_word(sCfgin,1),param,strlen(param)) == 0) {
			found = TRUE;
			switch ( type ) {
				case	CFG_STRING 	:
					strcpy(dest,sGet_word(sCfgin,3));
					break;	
				case	CFG_INT		:
        				iValue =(int)atoi(sGet_word(sCfgin,3));
					memcpy(dest,&iValue,sizeof(int));
					break;	
			} /* end switch */
		} /* end if */
	} /* end while */

	fclose(fp);
	return ( found == FALSE ) ? E_PARAM : OK;	

} /* end of iGetConfigEntry */


int iWriteConfigEntry(char *file,char *sect,char *param,short type,char *src)
{
	FILE	*fp,*fout;
	char	sCfgin[1024];
	char	LocalSect[32];
	char	line[1024];
	char	OutFile[1024];
	int	iValue;
	short	found;
	short	found2;
	int		ilRC = RC_SUCCESS;

	if ( (fp = (FILE *)fopen(file,"r")) == (FILE *)NULL) {
		return E_FOPEN;
	} /* end if */

	sprintf(OutFile,"%s/0x%.5ld.tmp",getenv("CFG_PATH"),getpid());

	if ( (fout = (FILE *)fopen(OutFile,"w")) == (FILE *)NULL) {
		fclose(fp);
		return E_FOPEN;
	} /* end if */

	found = FALSE;
	/* iStrup(sect); */
	/* iStrup(param); */

	sprintf(LocalSect,"[%s]",sect);


	while (fgets(sCfgin,132,fp) ) {
		/* iStrup(sCfgin);  converts a string to upper case letters */
		if ( strncmp(sGet_word(sCfgin,1),LocalSect,strlen(LocalSect)) == 0 ) {
			fprintf(fout,"%s",sCfgin);
			found = TRUE;
        		break;
		} /* end if */
		fprintf(fout,"%s",sCfgin);
	} /* while */


	if ( found == FALSE ) {
    /* 20040820 JIM: add section and keyword: */
			iValue = fprintf(fout,"%s\n",LocalSect);
			sprintf(line,"%s = %s",param,src);
			iValue = fprintf(fout,"%s\n",line);
    	sprintf(LocalSect,"[%s_END]",sect);
			iValue = fprintf(fout,"%s\n",LocalSect);
	} /* end if */
  else
  {
		sprintf(LocalSect,"[%s_END]",sect);
		found = FALSE;
    found2 = FALSE;

		while ((fgets(sCfgin,132,fp))    ) {
      /* 20040820 JIM: replace only in this section */
			if (( strncmp(sGet_word(sCfgin,1),param,strlen(param)) == 0) && 
          (found==FALSE)) {
				sprintf(line,"%s = %s",param,src);
				iValue = fprintf(fout,"%s\n",line);
				found2 = TRUE;
			} /* end if */
			else if ( strncmp(sGet_word(sCfgin,1),LocalSect,strlen(LocalSect)) == 0 ) {
        if (found2 == FALSE)
        {  /* 20040820 JIM: key word not found in section, add to section */
				   sprintf(line,"%s = %s",param,src);
				   iValue = fprintf(fout,"%s\n",line);
        }
				iValue = fprintf(fout,"%s",sCfgin);
        /* 20040820 JIM: mark section end, from now on copy only */
				found = TRUE;
			} 
      else {
				iValue = fprintf(fout,"%s",sCfgin);
			} /* end else */
		} /* end while */
  }

	fclose(fp);
	fclose(fout);

	errno = 0;
	ilRC = remove(file);
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"iWriteConfigEntry: remove<%s> returned <%d> <%d:%s>",file,ilRC,errno,strerror(errno));
	} else {
		errno = 0;
		ilRC = rename(OutFile,file);
		if(ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"iWriteConfigEntry: rename<%s,%s> returned <%d> <%d:%s>",OutFile,file,ilRC,errno,strerror(errno));
		}/* end of if */
	}/* end of if */

	return ( found == FALSE ) ? E_PARAM : OK;	

} /* end of iGetConfigEntry */


extern int iStrup(char *s)
{
	short	len;

	len = 0;
	while ( *s ) {
		len++;
		*s = toupper(*s);
		s++;
	} /* end while */

	return len;
}


extern int field_count(char *s)
{
  int	i=0,count=1;
  int level=0;

  /* New: to_char(abcd,'9999') is ONE field */

  while (*(s+i))
  {
    if ( *(s+i) == '(')
    {
      level++;
      i++;
      while (*(s+i) && level > 0)
      {
	if ( *(s+i) == '(')
	{
	  level++;
	} /* fi */

	if ( *(s+i) == ')')
	{
	  level--;
	} /* fi */
	i++;
      } /* while */
    } /* fi */

    if ( *(s+i))
    {
      if ( *(s+i) == ',') {
	count++;
      } /* end if */
      i++;
    } /* end if */
  } /* end while */
  
/*  fprintf(TRACE,"field_count: Got string:\n%s\nreturning %d\n",s,count); 
  
*/	
  return count;
}

extern char *sGet_item(char *s,short ItemNum)
{

	short	i;	/* field counter */
	static char	sItem[128];

	memset(sItem,0x00,128);
	i=0;

/*	dbg(TRACE,"FIELDS ARE %s ",s); */
	while ( *s && isspace(*s) ) { /* leave out blanks*/
		s++; i++;
	} /* end while*/


	for ( i=1 ; i < ItemNum ; i++ ) {
		while (*s && (*s!=',') ) {  /* leave out the current word*/
			s++;
		} /* end while*/

		while ( *s && (isspace(*s) || (*s==',')) ) { /* leave out blanks*/
			s++;
		} /* end while*/

	} /* end for*/

/*	dbg(TRACE,"FIELDS NOW ARE %s ",s); */

	i = 0;

	while (*s && (*s!=',') && !isspace(*s) ) {
		sItem[i] = *s;
		i++;
		s++;
	} /* end while*/
/*	dbg(TRACE,"returning %s ",sItem); */



	return sItem;
} /* end get_item returning a char pointer*/

/* added bch 21/03/97
	the following is identical to sGet_item()
	except that sGet_item() doesn't handle items with spaces

	example: myString = "itemOne,item Two, ,itemFour"

	sGet_item(myString,2)   returns  "item"
	sGet_item2(myString,2)	returns  "item Two"

	sGet_item(myString,3) 	returns  "itemFour"
	sGet_item2(myString,3)	returns  " "
*/
extern char *sGet_item2(char *s,short ItemNum)
{

	short	i;	/* field counter */
	static char	sItem[128];

	memset(sItem,0x00,128);
	i=0;

/*	dbg(TRACE,"FIELDS ARE %s ",s); */

	for ( i=1 ; i < ItemNum ; i++ ) {

		while (*s && *s!=',' && *s!=13 && *s!=10 ) {
			s++;  /* skip current word*/
		} /* end while*/

		if( *s == ',' )
			s++; /* move to first char after the current comma */

	} /* end for*/

/*	dbg(TRACE,"FIELDS NOW ARE %s ",s); */

	i = 0;
	/* copy the item found */
	while (*s && *s!=',' && *s!=13 && *s!=10 ) {
		sItem[i] = *s;
		i++;
		s++;
	} /* end while*/
/*	dbg(TRACE,"returning %s ",sItem); */


	return sItem;

} /* end sGet_item2() */

extern char *sGet_word(char *s,short FldNum)
{

	short	i;	/* field counter */
	static char	sWord[iMAX_READ_BUF];

	memset(sWord,0x00,iMAX_READ_BUF);
	i=0;

	while ( *s && isspace(*s) ) { /* leave out blanks*/
		s++; i++;
	} /* end while*/


	for ( i=1 ; i < FldNum ; i++ ) {
		while (*s && !isspace(*s) ) {  /* leave out the current word*/
			s++;
		} /* end while*/

		while ( *s && isspace(*s) ) { /* leave out blanks*/
			s++;
		} /* end while*/

	} /* end for*/

	i = 0;



	while (*s && !isspace(*s) ) {
		sWord[i] = *s;
		i++;
		s++;
	} /* end while*/



	return sWord;
} /* end get_word returning a char pointer*/

/******************************************************************************
 *
 * Function: tools_send_sql_rc
 *
 * Parameters: 	ipRouteID	address to send the command to
 *				pcpCommand	command type (ie. RT,DBT,AUA etc.)
 *				pcpTabName	name of table/s
 *				pcpSelection SQL 'WHERE' statement
 *				pcpFields	SQL field names
 *				pcpData		Data to send
 *				ipRetCode	return code of calling routine
 *
 * Returns: 	result of que() ?????
 *
 * Description: This function is identical to tools_send_sql()
 *				except that you can also set the return code
 *				passed to the receiving process. If the return
 *				code (ipRetCode) is not RC_SUCCESS then the
 *				data (pcpData) will be written to the BCHD->data
 *				field, which can then be used to identify the
 *				type of error.
 *
 * Created 23/04/96 by bch.
 *
 ********************************************************************/

int tools_send_sql_rc (	int ipRouteID,
						char *pcpCommand,
						char *pcpTabName,
						char *pcpSelection, 
						char *pcpFields,
						char *pcpData,
						int ipRetCode)
{
  int 		ilRC = RC_SUCCESS;
  int		ilLength=0;
  BC_HEAD 	*prlBchd;		/* Broadcast header		*/
  CMDBLK  	*prlCmdBlk;		/* Command Block 		*/
  char 		*pclDataBlk;		/* Data Block			*/
  char		*pclSelBlk;
  char		*pclFieldBlk;
  EVENT 	*prlOutEvent = NULL;


	if( ipRetCode != RC_SUCCESS )
		/* if there was an error then the data area gets copied to
			the area BCDH->data */
  		ilLength = strlen(pcpFields) + strlen(pcpData) + strlen(pcpData) +
  		strlen(pcpSelection) + sizeof(BC_HEAD)  + sizeof(CMDBLK) +
  		sizeof(EVENT) + 128; 
	else
  		ilLength = strlen(pcpFields) + strlen(pcpData) + strlen(pcpSelection) + 
    	sizeof(BC_HEAD)  + sizeof(CMDBLK) + sizeof(EVENT) + 128; 

	prlOutEvent = (EVENT *) malloc(ilLength);
	if (prlOutEvent == NULL)
	{
		ilRC= RC_FAIL;
		dbg(TRACE,"tools_send_sql: Malloc: %s", strerror(errno)); 
    
	}
	else
	{
		memset (((char *) prlOutEvent), 0, ilLength);
		prlOutEvent->data_offset = sizeof(EVENT);
		prlOutEvent->originator = mod_id; /* should B global */
		prlOutEvent->data_length =  ilLength;
		prlOutEvent->command = EVENT_DATA;
  
		prlBchd = (BC_HEAD *) (((char *)prlOutEvent) + sizeof(EVENT));

		if( ipRetCode != RC_SUCCESS )
		{
			/* if there was an error, then the data which contains
				the error description, gets copied to the BCHD->data
				area. This is because the dll uses this when the RC is
				non zero ! The command block will then point to the
				address after the BCHD address plus the length of the data */

			strcpy(prlBchd->data,pcpData);
			prlCmdBlk = (CMDBLK  *) ( ((char *)prlBchd->data) +
												strlen(pcpData) + 1);
		}
		else
		{
			prlCmdBlk= (CMDBLK  *) ((char *)prlBchd->data);
		}

		prlBchd->rc = ipRetCode;
		strcpy (prlCmdBlk->command, pcpCommand);
		pclSelBlk = prlCmdBlk->data;
		strcpy (pclSelBlk, pcpSelection);
		strcpy (prlCmdBlk->obj_name, pcpTabName);
		pclFieldBlk = (char *) pclSelBlk + strlen (pclSelBlk) + 1;
		strcpy (pclFieldBlk, pcpFields);
		pclDataBlk   = pclFieldBlk + strlen(pclFieldBlk) + 1;
		strcpy (pclDataBlk, pcpData);

		ilRC = que(QUE_PUT,ipRouteID,mod_id,PRIORITY_3,ilLength,
	    		 (char *)prlOutEvent);

		if (ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"tools_send_sql: rc=%d", ilRC); 	
		}
	}

	if (prlOutEvent != NULL)
	{
		free (prlOutEvent);
	}

	return ilRC;

} /* end tools_send_sql_rc */


/* ******************************************************************* */
/* The tools_send_sql routine						*/
/* Sends an  SQL Statement to the specified modid	 		*/
/* The result has to be analyzes after a que_get in the calling progranm*/
/* Parameters:								*/
/* route_id	route id 						*/
/* Pcmd		IRT, URT, DRT, RT or so					*/
/* Ptab_name	table name						*/
/* Psel		"WHERE a = 5"						*/
/* Pfields	"teno,ties,seem"					*/
/* Pdata	"1,FRA,42,5555,jajaja"					*/
/* rc		RC_SUCCESS, RC_FAIL					*/
/* ******************************************************************** */
int tools_send_sql (int route_id, char *Pcmd, char *Ptab_name, char *Psel, 
		    char *Pfields, char *Pdata)
{
  int rc = RC_SUCCESS;
  int	len=0;
  BC_HEAD *Pbc_head;		/* Broadcast header		*/
  CMDBLK  *Pcmdblk;		/* Command Block 		*/
  char 	*Pdatablk;		/* Data Block			*/
  char	*Pselection;
  char	*Pfie;
  EVENT *Pout_event = NULL;
  /* dbg(TRACE,"C<%s>, T<%s>, S<%s>, D<%s>",Pcmd,Ptab_name,Psel,Pdata); */
  len = strlen(Pfields) + strlen(Pdata) + strlen(Psel) + 
    sizeof(BC_HEAD)  + sizeof(CMDBLK) + sizeof(EVENT) + 128; 

  Pout_event = (EVENT *) malloc(len);
  if (Pout_event == NULL)
  {
    rc = RC_FAIL;
    dbg(TRACE,"tools_send_sql: Malloc: %s", strerror(errno)); 
    
  }
  else
  {
    memset (((char *) Pout_event), 0, len);
    Pout_event->data_offset = sizeof(EVENT);
    Pout_event->originator = mod_id; /* should B global */
    Pout_event->data_length =  len;
    Pout_event->command = EVENT_DATA;
  
    Pbc_head = (BC_HEAD *) (((char *)Pout_event) + sizeof(EVENT));
    Pcmdblk= (CMDBLK  *) ((char *)Pbc_head->data);
    Pbc_head->rc = RC_SUCCESS;
    strcpy (Pcmdblk->command, Pcmd);
    Pselection = Pcmdblk->data;
    strcpy (Pselection, Psel);
    strcpy (Pcmdblk->obj_name, Ptab_name);
    Pfie = (char *) Pselection + strlen (Pselection) + 1;
    strcpy (Pfie, Pfields);
    Pdatablk   = Pfie + strlen(Pfie) + 1;
    strcpy (Pdatablk, Pdata);

    rc = que(QUE_PUT,route_id,mod_id,PRIORITY_3,len,
	     (char *)Pout_event);

    if (rc != RC_SUCCESS)
    {
      dbg(TRACE,"tools_send_sql: rc=%d", rc); 	
    } /* fi */
  } /* fi */

  if (Pout_event != NULL)
  {
    free (Pout_event);
  } /* fi */

  return rc;
} /* tools_send_sql */

int tools_send_sql_prio (int route_id, char *Pcmd, char *Ptab_name, char *Psel, 
		    char *Pfields, char *Pdata,int ipPriority)
{
  int rc = RC_SUCCESS;
  int	len=0;
  BC_HEAD *Pbc_head;		/* Broadcast header		*/
  CMDBLK  *Pcmdblk;		/* Command Block 		*/
  char 	*Pdatablk;		/* Data Block			*/
  char	*Pselection;
  char	*Pfie;
  EVENT *Pout_event = NULL;
  /* dbg(TRACE,"C<%s>, T<%s>, S<%s>, D<%s>",Pcmd,Ptab_name,Psel,Pdata); */
  len = strlen(Pfields) + strlen(Pdata) + strlen(Psel) + 
    sizeof(BC_HEAD)  + sizeof(CMDBLK) + sizeof(EVENT) + 128; 

  Pout_event = (EVENT *) malloc(len);
  if (Pout_event == NULL)
  {
    rc = RC_FAIL;
    dbg(TRACE,"tools_send_sql: Malloc: %s", strerror(errno)); 
    
  }
  else
  {
    memset (((char *) Pout_event), 0, len);
    Pout_event->data_offset = sizeof(EVENT);
    Pout_event->originator = mod_id; /* should B global */
    Pout_event->data_length =  len;
    Pout_event->command = EVENT_DATA;
  
    Pbc_head = (BC_HEAD *) (((char *)Pout_event) + sizeof(EVENT));
    Pcmdblk= (CMDBLK  *) ((char *)Pbc_head->data);
    Pbc_head->rc = RC_SUCCESS;
    strcpy (Pcmdblk->command, Pcmd);
    Pselection = Pcmdblk->data;
    strcpy (Pselection, Psel);
    strcpy (Pcmdblk->obj_name, Ptab_name);
    Pfie = (char *) Pselection + strlen (Pselection) + 1;
    strcpy (Pfie, Pfields);
    Pdatablk   = Pfie + strlen(Pfie) + 1;
    strcpy (Pdatablk, Pdata);

    rc = que(QUE_PUT,route_id,mod_id,ipPriority,len,
	     (char *)Pout_event);

    if (rc != RC_SUCCESS)
    {
      dbg(TRACE,"tools_send_sql: rc=%d", rc); 	
    } /* fi */
  } /* fi */

  if (Pout_event != NULL)
  {
    free (Pout_event);
  } /* fi */

  return rc;
} /* tools_send_sql */



/* ******************************************************************** */
/* The tools_send_sql_flag routine					*/
/* Like tools_send_sql with a flag argument				*/
/* Sends an  SQL Statement to the specified modid	 		*/
/* The result has to be analyzes after a que_get in the calling progranm*/
/* Parameters:								*/
/* route_id	route id 						*/
/* Pcmd		IRT, URT, DRT, RT or so					*/
/* Ptab_name	table name						*/
/* Psel		"WHERE a = 5"						*/
/* Pfields	"teno,ties,seem"					*/
/* Pdata	"1,FRA,42,5555,jajaja"					*/
/* flag		NETOUT_NO_ACK or 0					*/
/* rc		RC_SUCCESS, RC_FAIL					*/
/* ******************************************************************** */
int tools_send_sql_flag(int route_id, char *Pcmd, char *Ptab_name, char *Psel, 
			char *Pfields, char *Pdata, int flag)
{
  int rc = RC_SUCCESS;
  int	len=0;
  BC_HEAD *Pbc_head;		/* Broadcast header		*/
  CMDBLK  *Pcmdblk;		/* Command Block 		*/
  char 	*Pdatablk;		/* Data Block			*/
  char	*Pselection;
  char	*Pfie;
  EVENT *Pout_event = NULL;
  /* dbg(TRACE,"C<%s>, T<%s>, S<%s>, D<%s>",Pcmd,Ptab_name,Psel,Pdata); */
  len = strlen(Pfields) + strlen(Pdata) + strlen(Psel) + 
    sizeof(BC_HEAD)  + sizeof(CMDBLK) + sizeof(EVENT) + 128; 

  Pout_event = (EVENT *) malloc(len);
  if (Pout_event == NULL)
  {
    rc = RC_FAIL;
    dbg(TRACE,"tools_send_sql_flag: Malloc: %s", strerror(errno)); 
    
  }
  else
  {
    memset (((char *) Pout_event), 0, len);
    Pout_event->data_offset = sizeof(EVENT);
    Pout_event->originator = mod_id; /* should B global */
    Pout_event->data_length =  len;
    Pout_event->command = EVENT_DATA;
  
    Pbc_head = (BC_HEAD *) (((char *)Pout_event) + sizeof(EVENT));
    Pcmdblk= (CMDBLK  *) ((char *)Pbc_head->data);
    if (flag == NETOUT_NO_ACK)
    {
      Pbc_head->rc = NETOUT_NO_ACK;
    }
    else
    {
      Pbc_head->rc = RC_SUCCESS;
    } /* fi */
    strcpy (Pcmdblk->command, Pcmd);
    Pselection = Pcmdblk->data;
    strcpy (Pselection, Psel);
    strcpy (Pcmdblk->obj_name, Ptab_name);
    Pfie = (char *) Pselection + strlen (Pselection) + 1;
    strcpy (Pfie, Pfields);
    Pdatablk   = Pfie + strlen(Pfie) + 1;
    strcpy (Pdatablk, Pdata);

    rc = que(QUE_PUT,route_id,mod_id,PRIORITY_3,len,
	     (char *)Pout_event);

    if (rc != RC_SUCCESS)
    {
      dbg(TRACE,"tools_send_sql_flag: rc=%d", rc); 	
    } /* fi */
  } /* fi */

  if (Pout_event != NULL)
  {
    free (Pout_event);
  } /* fi */

  return rc;
} /* tools_send_sql_flag */


/* ******************************************************************** */
/* The tools_send_info_flag routine					*/
/* Sends an Statement to the specified modid	 		*/
/* The result may be sent to the passed originator mod_id */
/* Parameters:								*/
/* route_id	route id 						*/
/* orig_id	route id of "originator" (or 0) 						*/
/* Pdst_name	name of destination or KeyWord	*/
/* Porg_name	name of originator or KeyWord	*/
/* Prcv_name	name of sender or KeyWord	*/
/* Pseq_id */
/* Pref_seq_id */
/* Ptw_start keyWord/counter for general use */
/* Ptw_end  keyWord for general use  */
/* Pcmd		IRT, URT, DRT, RT or so					*/
/* Ptab_name	table name						*/
/* Psel		"WHERE a = 5"						*/
/* Pfields	"TENO,TIES,SEEM"					*/
/* Pdata	"1,FRA,42,5555,jajaja"					*/
/* flag		NETOUT_NO_ACK or 0 or Priority				*/
/* rc		RC_SUCCESS, RC_FAIL					*/
/* *********************************** */
int tools_send_info_flag
   (int  route_id,     /* Queue ID of Receiver                               */
    int  orig_id,      /* EVENT.originator  : Set to mod_id if < 1           */
    char *Pdst_name,   /* BC_HEAD.dest_name : Used As User Name              */
    char *Porg_name,   /* BC_HEAD.orig_name : Used by BCHDL as Host Name     */
    char *Prcv_name,   /* BC_HEAD.recv_name : Used As Workstation Name       */
    char *Pseq_id,     /* BC_HEAD.seq_id    : Seems to be unused             */
    char *Pref_seq_id, /* BC_HEAD.ref_seq_id: Seems to be unused             */
    char *Ptw_start,   /* CMDBLK.tw_start   : Used by Flight as BC Counter   */
    char *Ptw_end,     /* CMDBLK.tw_end     : Used by Flight As StateStamp   */
    char *Pcmd,        /* CMDBLK.command    : Command KeyWord                */
    char *Ptab_name,   /* CMDBLK.obj_name   : Normally Table Name            */
    char *Psel,        /* CMDBLK.data       : Selection String in DataBlk    */
    char *Pfields,     /* CMDBLK.data       : Fieldlist String in DataBlk    */
    char *Pdata,       /* CMDBLK.data       : ListOfData Items in DataBlk    */
    int  flag)         /* BC_HEAD.rc        : RC_SUCCESS if != NETOUT_NO_ACK */
{
  int rc = RC_SUCCESS;
  int	len=0;
  int ilPrio = 0;
  BC_HEAD *Pbc_head;		/* Broadcast header		*/
  CMDBLK  *Pcmdblk;		/* Command Block 		*/
  char 	*Pdatablk;		/* Data Block			*/
  char	*Pselection;
  char	*Pfie;
  EVENT *Pout_event = NULL;

  if ((route_id > 0) && (route_id != 9999))
  {
    len = strlen(Pfields) + strlen(Pdata) + strlen(Psel) + 
      sizeof(BC_HEAD)  + sizeof(CMDBLK) + sizeof(EVENT) + 128; 

    Pout_event = (EVENT *) malloc(len);
    if (Pout_event == NULL)
    {
      rc = RC_FAIL;
      dbg(TRACE,"tools_send_sql_flag: Malloc: %s", strerror(errno)); 
    }
    else
    {
      memset (((char *) Pout_event), 0, len);
      Pout_event->data_offset = sizeof(EVENT);
      if (orig_id > 0)
      {
        Pout_event->originator = orig_id;
      } /* end if */
      else
      {
        Pout_event->originator = mod_id; /* should B global */
      } /* end else */
      Pout_event->data_length =  len;
      Pout_event->command = EVENT_DATA;
    
      Pbc_head = (BC_HEAD *) (((char *)Pout_event) + sizeof(EVENT));
      Pcmdblk= (CMDBLK  *) ((char *)Pbc_head->data);
  
      ilPrio = PRIORITY_3;
      if (flag == NETOUT_NO_ACK)
      {
        Pbc_head->rc = NETOUT_NO_ACK;
      }
      else
      {
        Pbc_head->rc = RC_SUCCESS;
        if ((flag >= PRIORITY_1) && (flag <= PRIORITY_5))
        {
          ilPrio = flag;
        } /* end if */
      } /* fi */

      strcpy (Pbc_head->dest_name, Pdst_name);
      strcpy (Pbc_head->orig_name, Porg_name);
      strcpy (Pbc_head->recv_name, Prcv_name);
      strcpy (Pbc_head->seq_id, Pseq_id);
      strcpy (Pbc_head->ref_seq_id, Pref_seq_id);
   
      strcpy (Pcmdblk->tw_start, Ptw_start);
      strcpy (Pcmdblk->tw_end, Ptw_end);
      strcpy (Pcmdblk->command, Pcmd);
      Pselection = Pcmdblk->data;
      strcpy (Pselection, Psel);
      strcpy (Pcmdblk->obj_name, Ptab_name);
      Pfie = (char *) Pselection + strlen (Pselection) + 1;
      strcpy (Pfie, Pfields);
      Pdatablk   = Pfie + strlen(Pfie) + 1;
      strcpy (Pdatablk, Pdata);


      rc = que(QUE_PUT,route_id,mod_id,ilPrio,len, (char *)Pout_event);
      if (rc != RC_SUCCESS)
      {
        dbg(TRACE,"tools_send_sql_flag: rc=%d", rc); 	
      } /* fi */
    } /* fi */

    if (Pout_event != NULL)
    {
      free (Pout_event);
    } /* fi */

  } /* end if */

  return rc;
} /* tools_send_info_flag */


/* ******************************************************************** */
/* The tools_send_err routine						*/
/* Sends an error message to the Q-ID. The mesage contains of the 	*/
/* bc_head struct with rc = ret						*/
/* rc		RC_SUCCESS, RC_FAIL					*/
/* ******************************************************************** */
int tools_send_err (int route_id, int ret)
{
  int rc = RC_SUCCESS;
  int	len=0;
  BC_HEAD *Pbc_head;		/* Broadcast header		*/
  EVENT *Pout_event = NULL;

  len =  sizeof(BC_HEAD)  + sizeof(EVENT) + 4; 
  Pout_event = (EVENT *) malloc(len);
  if (Pout_event == NULL)
  {
    rc = RC_FAIL;
    dbg(TRACE,"tools_send_err: Malloc: %s", strerror(errno)); 
    
  }
  else
  {
    memset (((char *) Pout_event), 0, len);
    Pout_event->data_offset = sizeof(EVENT);
    Pout_event->originator = mod_id; /* should B global */
    Pout_event->data_length =  (short) len;
    Pout_event->command = EVENT_DATA;
  
    Pbc_head = (BC_HEAD *) (((char *)Pout_event) + sizeof(EVENT));
    Pbc_head->rc = ret;

    rc = que(QUE_PUT,route_id,mod_id,PRIORITY_3,len,
	     (char *)Pout_event);

    if (rc != RC_SUCCESS)
    {
      dbg(TRACE,"tools_send_err: rc=%d", rc); 	
    } /* fi */
  } /* fi */

  return rc;
} /* tools_send_err */


#ifdef OWN_NAP
int nap(unsigned long time)
{
  int		nfds,readfds,writefds,exceptfds;
  struct timeval	Timer;
	
  nfds = readfds = writefds = exceptfds = 0;

  if (time == (unsigned long) 0) {
    return 0;
  } else if ((time > (unsigned long) 4000) || (time < (unsigned long) 0)) {
    errno = ERANGE;
    dbg(DEBUG,"nap time out of range (0 - 4000)");
    return(-1);
  } else { 

    Timer.tv_sec = time / (unsigned long) 1000;
    Timer.tv_usec = time % (unsigned long) 1000 * 1000;

    if (select(nfds,(fd_set *)&readfds,(fd_set *)&writefds,
	       (fd_set *)&exceptfds,&Timer) < 0) {
      dbg(DEBUG,"nap failed");
      return(-1);
    } /* end if */
  }
  /*dbg(DEBUG, "nap: sec=%d usec=%d ",Timer.tv_sec, Timer.tv_usec);*/

  return(0);
}
#endif




void time_stamp(char *s)
{
	char msg[512];
	char *work;
	
	strncpy(msg,s,460);
	work = msg + strlen(msg);
	get_time(work);
	dbg(DEBUG,"TS: %s",msg); 

	return;
}


static void get_time(char *s)
{
   struct timeb tp;
   long    sec,min,hour;
      
   ftime(&tp);
   min = tp.time / 60;
   sec = tp.time % 60;
   hour= min / 60;
   min = min % 60;
   sprintf(s," %2.2ld:%2.2ld:%d\n",min,sec,tp.millitm);
}

#ifdef NEVER_TUE_ES

#define QUE_TIMEOUT 		(120)
/* ******************************************************************** */
/* Following the implemenmtation of the BIG que get call. Within this   */
/* call you can receive more than 32K of data. The amount of data can be*/
/* received depends on the systems main memory                          */ 
/* ******************************************************************** */

int big_que_get(short fkt,int route,int mod_id,short prio ,int len, char **data)
{
	int	rc;
	
	dbg(TRACE,"Greetings from big_que_get"); 

	switch(fkt) {
		case	QUE_GETNW	:
			dbg(TRACE,"Dest Adr = 0x%x",*data); 
			
			rc = nw_read_que(mod_id,prio,data);
			break;
		case	QUE_GET		:
			rc = RC_FAIL;
			/* rc = read_que(mod_id,prio,data);*/
			break;
	} /* end switch */

	return rc;


} /* end of big_que_get */


static int nw_read_que(int mod_id,int prio,char **data)
{
	int	rc,t,n_times,total_len,llen;
	char	*rcv_base,*rcv_buf,*offs;
	EVENT	*ev,*tev;
	ITEM	*litem,*titem;

	dbg(TRACE,"Greeting from nw_read_que. I am %d",mod_id); 
	

	llen = QCP_MAX_LEN+sizeof(ITEM);
	litem = (ITEM *)malloc(llen);
	if ( litem == (ITEM *)NULL) {
		rc = RC_FAIL;
	} /* end if */

	t = QUE_TIMEOUT+1;
	do {
		nap (250);
		rc = que(QUE_GETNW,0,mod_id,prio,llen,(char *)litem);
  		t--;
	} while (t > 0 && rc == QUE_E_NOMSG);

	ev=(EVENT *)litem->text;

	/* dbg(TRACE,"After QUE GET(rc = %d) to satisfy request: after %d.%d sec", rc, (QUE_TIMEOUT - t - 1)/4, (t%4)*25 );  */
	
	if ( ev->command==SESS_DATA && rc!=QUE_E_NOMSG ) {	
						/* Read rest of Data */
		/* dbg(TRACE,"Continueing read ...");
		*/
		que(QUE_ACK,0,mod_id,0,0,0,NULL);

		n_times=1;
		titem  = (ITEM *)malloc(llen);  /* Temp Item */
		tev    = (EVENT *)titem->text;  /* Temp Event */

		memcpy(titem,litem,llen);
		total_len=tev->data_length;

		/* dbg(TRACE,"B.Wl rc = %d, cmd = %d ,sess = %d,eos = %d",
				rc,ev->command,SESS_DATA,END_O_SEQ);
		*/

		while ( tev->command==SESS_DATA && rc==RC_SUCCESS) {
			rc = que(QUE_GET,0,mod_id,prio,llen,(char *)titem);
			if ( rc == RC_SUCCESS ) {
				que(QUE_ACK,0,mod_id,0,0,0,NULL);

				n_times++;
				litem  = (ITEM *)realloc(litem,n_times*llen);
				ev     = (EVENT *)litem->text;
				offs   = (char *)ev+total_len;

				tev    = (EVENT *)titem->text;
				rcv_buf=(char *)tev + sizeof(EVENT);
				total_len += tev->data_length;

				memcpy(offs,rcv_buf,tev->data_length);
			} /* end if */
			/* dbg(TRACE,"In BIG WHILE(rc = %d) to satisfy request: after %d.%d sec", rc, (QUE_TIMEOUT - t - 1)/4, (t%4)*25 ); */
		} /* end while */
		if ( rc == RC_SUCCESS ) {
			/* dbg(TRACE,"Len = %d, offs = %d",ev->data_length,
							     ev->data_offset);
			*/
			ev->data_length = total_len - sizeof(EVENT);
			ev->data_offset = sizeof(EVENT);
			ev->command = EVENT_DATA;
			*data = (char *)litem;
		} /* end if */

		free(titem);

	} /* end if */
	else {
		if ( rc == RC_SUCCESS ) {
			que(QUE_ACK,0,mod_id,0,0,0,NULL);
			snap(ev,32,outp);
			*data = (char *)litem;
		} /* end if */
		else {
			free(litem);
			rc = RC_FAIL;
		} /* end else */
	} /* end else */

	return rc;

} /* end of nw_read_que */
#endif

/* ******************************************************************** */
/* ******************************************************************** */
/* THE FLT_* FUNCTIONS ARE MOVED FROM THE FLTCHECKER 			*/
/* ******************************************************************** */
/* ******************************************************************** */

/* ******************************************************************** */
/* The flt_get_nth_field routine					*/
/* *PPresult will point to the first byte of the nth field of Pstr.	*/
/* Counting starts with 1, fields are separated by ',', terms like the 	*/
/* following is one field: to_char(abc,5)				*/
/* The return value is RC_SUCCESS, if the field has been found, RC_FAIL */
/* else									*/
/* ******************************************************************** */

int flt_get_nth_field (char *Pstr, int n, char **PPresult)
{
  int rc = RC_SUCCESS;
  int i=0;
  int count=1;

  while (*(Pstr+i) && count < n){
    if ( *(Pstr+i) == '(')
    {
      while (*(Pstr+i) && *(Pstr+i) != ')')
	i++;
    }
    if ( *(Pstr+i))
    {
      if ( *(Pstr+i) == ',') {
	count++;
      } /* end if */
      i++;
    } /* end if */
  } /* end while */

  if (*(Pstr+i) || count == n)
  {
    *PPresult = Pstr+i;
  }
  else
  {
    rc = RC_FAIL;
  } /* fi */

/*dbg (TRACE, "flt_get_nth_field: Pstr=%.60sn=%d Result=%.60s",Pstr, n, 
	   *PPresult ); 
  */
  return rc;
} /* flt_get_nth_field */


/* ******************************************************************** */
/* The flt_cp_nth_field routine						*/
/* The nth field of Pstr will be copied to Presult.			*/
/* Counting starts with 1, fields are separated by ',', terms like the 	*/
/* following is one field: to_char(abc,5)				*/
/* The return value is RC_SUCCESS, if the field has been found, RC_FAIL */
/* else									*/
/* ******************************************************************** */

int flt_cp_nth_field (char *Pstr, int n, char *Presult)
{
  int rc = RC_SUCCESS;
  int i=0;
  int res_i=0;
  char *Ptmp=NULL;
  int p_level=0;

  rc = flt_get_nth_field (Pstr, n, &Ptmp);
  if (rc == RC_SUCCESS)
  {
    while (*(Ptmp+i) && (p_level > 0 || ( *(Ptmp+i) != ',' && 
			 *(Ptmp+i) != '\n') && *(Ptmp+i) != '\r') )
    {
      if ( *(Ptmp+i) == '(')
      {
	p_level++;
      }
      if ( *(Ptmp+i) == ')')
      {
	p_level--;
      }
      Presult[res_i] = *(Ptmp + i);
      res_i++;
      i++;
    } /* end while */
    Presult[res_i] = EOS;
  } /* end if */

/*  dbg (TRACE, "flt_cp_nth_field: Pstr=%.60sn=%d Result=%s",Pstr, n, 
	   Presult ); 
  */
  return rc;
} /* flt_cp_nth_field */


/* ******************************************************************** */
/* The flt_search_field routine						*/
/* Searches the string Pfield in Pstr and returns in *Pn the number of 	*/
/* the field, field counting starts with 1, fields are separated by ','	*/
/* The return value is RC_SUCCESS, if the field has been found, RC_FAIL */
/* else									*/
/* ******************************************************************** */

int flt_search_field (char *Pstr, char *Pfield, int *Pn)
{
  int rc = RC_SUCCESS;
  int i=0;
  int count=1;

  /* Uses strcasecmp for case insensitive comparision. */
  /* Is contained in socketlib (SCO) */
  while (*(Pstr+i) && StrnCaseCmp (Pstr+i, Pfield, strlen(Pfield)) != 0){
      if ( *(Pstr+i) == ',') {
	count++;
      } /* end if */
      i++;
  } /* end while */

  if (StrnCaseCmp(Pstr+i, Pfield, strlen(Pfield)) == 0)
  {
    *Pn = count;
  }
  else
  {
    rc = RC_FAIL;
  } /* fi */

/*  dbg (TRACE, "flt_search_field: Pstr=%.60sField=%s n=%d ",Pstr,  
	   Pfield,*Pn ); 
  */
  return rc;
} /* flt_search_field */

/* ******************************************************************** */
/* The flt_len_of_nth_field routine 					*/
/* The length of the nth field of Pstr will be returned.     		*/
/* Counting starts with 1, fields are separated by ',', te 	      	*/
/* The return value is RC_FAIL, if the field has not been found, 	*/
/* ******************************************************************** */

int flt_len_of_nth_field (char *Pstr, int n)
{
  int rc = RC_SUCCESS;
  int i=0;
  char  *Ptmp=NULL;

  rc = flt_get_nth_field (Pstr, n, &Ptmp);
  if (rc == RC_SUCCESS)
  {
    while (*(Ptmp+i) && *(Ptmp+i) != ',')
    {
      i++;
    } /* end while */
    rc = i;
  } /* end if */

  return rc;
} /* flt_len_of_nth_field */

/* ******************************************************************** */
/* The tool_fn_to_buf  routine						*/
/* Opens the given file, allocates a buffer, reads the file in the 	*/
/* buffer and returns a pointer in Pret					*/
/* ******************************************************************** */
int tool_fn_to_buf (char *Ppath, long *Psize, char **PPret)
{
  int 			rc = RC_SUCCESS;
  int				ilRead;
  FILE 			*fp=NULL;
  struct stat 	statbuf;
  char 			*Pbuffer=NULL;

  dbg(DEBUG,"<tool_fn_to_buf> --------- START ---------");

  rc = stat (Ppath, &statbuf);
  if (rc == -1)
  {
    dbg(TRACE,"<tool_fn_to_buf> stat for %s failed: %s", Ppath, strerror(errno));
	 ilRead = 0; 
    rc = RC_FAIL;
  } 

  if (rc == RC_SUCCESS)
  {
    *Psize = statbuf.st_size;
    Pbuffer = calloc ((size_t)1, (size_t)((*Psize)+1));
    if (Pbuffer == NULL)
    {
      dbg(TRACE,"<tool_fn_to_buf> calloc failed: %s:", strerror(errno));
     	ilRead = 0; 
      rc = RC_FAIL;
    } 
  }

  if (rc == RC_SUCCESS)
  {
    fp = fopen (Ppath, "r");
    if (fp == NULL)
    {
      dbg(TRACE,"<tool_fn_to_buf> open failed: %s:", strerror(errno));
		ilRead = 0;
      rc = RC_FAIL;
    } 
  }

  if (rc == RC_SUCCESS)
  {
    rc = (int)fread((void*)Pbuffer, (size_t)1, (size_t)(*Psize), fp);
	 ilRead = rc;
    if (rc < *Psize)
    {
      dbg(TRACE,"<tool_fn_to_buf> read %d bytes instead of %ld", rc, *Psize);
      rc = RC_FAIL;
    } 
    else
    {
      rc = RC_SUCCESS;
    } 
  }

  if (fp != NULL)
  {
    fclose (fp);
	 fp = NULL;
  }

  if (rc == RC_SUCCESS)
  {
    *PPret = Pbuffer;
  }
  else
  {
    dbg(TRACE,"<tool_fn_to_buf> read %d bytes & returns %d", ilRead, rc); 
    *PPret = NULL;
	 *Psize = 0;
	 dbg(DEBUG,"<tool_fn_to_buf> delete memory...");
    free((void*)Pbuffer); 
  }

  dbg(DEBUG,"<tool_fn_to_buf> --------- END ---------");
  return rc;
} /* tool_fn_to_buf */

/* ******************************************************************** */
/* The tool_search_eol  routine						*/
/* ******************************************************************** */

int tool_search_eol (char **PPbuf)
{
  while (**PPbuf != EOS && **PPbuf != '\n')
    (*PPbuf)++;
  if (**PPbuf != EOS)
    return RC_SUCCESS;
  else
    return RC_NOT_FOUND;
} /* tool_search_eol */

/* ******************************************************************** */
/* The tool_filter_num  routine						*/
/* ******************************************************************** */

int tool_filter_num (char *Pdata, int *Plen)
{
  int rc = RC_SUCCESS;
  int i=0, j=0;

  for (i=0, j=0; i < *Plen; i++)
  {
    if (isdigit (Pdata[i]))
    {
      Pdata[j] = Pdata[i];
      j++;
    } /* fi */
  } /* for */
  *Plen = j;
  
  return rc;
} /* tool_filter_num */

/* ******************************************************************** */
/* The tool_filter_lf  routine						*/
/* ******************************************************************** */
void tool_filter_lf (char *Pdata, int *Plen)
{
  int rc = RC_SUCCESS;
  int i=0, j=0;

  for (i=0, j=0; i < *Plen; i++)
  {
    if (Pdata[i] != 0x0a)
    {
      Pdata[j] = Pdata[i];
      j++;
    } /* fi */
  } /* for */
  *Plen = j;
} /* tool_filter_lf */


/* ******************************************************************** */
/* The ascii_snap  routine						*/
/* Prints the buffer as chars. ASCII control char are printed as words	*/
/* like <ESC>. Line wrapping appears before 80 chars per line		*/
/* ******************************************************************** */

void ascii_snap (char *Pdata, int len, FILE *outp)
{
  int i=0;
  int act_len=0;
  char *Pc=NULL;
  char buf[2];

  buf[1]=EOS;
  fprintf (outp,"\n");

  for (i=0, act_len=0; i < len; i++)
  {
    switch (Pdata[i])
    {
      case 0: Pc = "<NUL>"; break;
      case 1: Pc = "<SOH>"; break;
      case 2: Pc = "<STX>"; break;
      case 3: Pc = "<ETX>"; break;
      case 4: Pc = "<EOT>"; break;
      case 5: Pc = "<ENQ>"; break;
      case 6: Pc = "<ACK>"; break;
      case 7: Pc = "<BEL>"; break;
      case 8: Pc = "<BS>"; break;
      case 9: Pc = "<HT>"; break;
      case 10: Pc = "<LF>"; break;
      case 11: Pc = "<VT>"; break;
      case 12: Pc = "<FF>"; break;
      case 13: Pc = "<CR>"; break;
      case 14: Pc = "<SO>"; break;
      case 15: Pc = "<SI>"; break;
      case 16: Pc = "<DLE>"; break;
      case 17: Pc = "<DC1>"; break;
      case 18: Pc = "<DC2>"; break;
      case 19: Pc = "<DC3>"; break;
      case 20: Pc = "<DC4>"; break;
      case 21: Pc = "<NAK>"; break;
      case 22: Pc = "<SYN>"; break;
      case 23: Pc = "<ETB>"; break;
      case 24: Pc = "<CAN>"; break;
      case 25: Pc = "<EM>"; break;
      case 26: Pc = "<SUB>"; break;
      case 27: Pc = "<ESC>"; break;
      case 28: Pc = "<FS>"; break;
      case 29: Pc = "<GS>"; break;
      case 30: Pc = "<RSSOE>"; break;
      case 31: Pc = "<US>"; break;
      case 127: Pc = "<DEL>"; break;
      default: Pc=buf; buf[0]=Pdata[i];
    } /* switch */

    fprintf (outp,Pc);
    act_len += strlen (Pc);
    if (act_len > 70)
    {
      fprintf (outp,"\n");
      act_len=0;
    } /* fi */
  } /* for */

  fprintf (outp,"\n");
  
} /* ascii_snap */

/* ******************************************************************** */
/* The mts_snap  routine						*/
/* Prints the buffer as chars. MTS 2000 / UTS 20  escape sequences are 	*/
/* printed in text. Line wrapping appears before 80 chars per line	*/
/* ******************************************************************** */

void mts_snap (char *Pdata, int len, FILE *outp)
{
  int i=0;
  int act_len=0;
  char *Pc=NULL;
  char buf[80];

  buf[1]=EOS;
  dbg (TRACE,"\nUTS 20 SNAP:");

  for (i=0, act_len=0; i < len; i++)
  {
    if (Pdata[i] == 27)	/* ESC */
    {
      i++;
      if (i < len)
      {
	switch (Pdata[i])
	{
	  case 11: sprintf (buf,"<Cursor to Y=%d X=%d>\n",Pdata[i+1]-' ',
			    Pdata[i+2]-' ');
	           Pc=buf;
	           i += 4;
	           break;

	  case 'e': sprintf (buf,"<Cursor home>\n");
	           Pc=buf;
	           break;
	  case 'f': sprintf (buf,"<Scan up>\n");
	           Pc=buf;
	           break;
	  case 'g': sprintf (buf,"<Scan left>\n");
	           Pc=buf;
	           break;
	  case 'h': sprintf (buf,"<Scan right>\n");
	           Pc=buf;
	           break;
	  case 'i': sprintf (buf,"<Scan down>\n");
	           Pc=buf;
	           break;
	  case 'z': sprintf (buf,"<Backward tab>\n");
	           Pc=buf;
	           break;
	  case 'M': sprintf (buf,"<Erase Display>\n");
	           Pc=buf;
	           break;
	  case 'o': sprintf (buf,"<Control Page>\n");
	           Pc=buf;
	           break;
	  default: sprintf (buf,"<ESC>\n");
	           Pc=buf;
	           i--;
	           break;
	} /* switch */
      } /* fi len */	    
    } /* fi esc */	    
    else if (Pdata[i] == 31)	/* US */
    {
	sprintf (buf,"<FCC abs:Cursor to Y=%d X=%d M=%c (0x%x) N=%c (0x%x)>\n",
		 Pdata[i+1]-' ', Pdata[i+2]-' ',Pdata[i+3],Pdata[i+3],
		 Pdata[i+4],Pdata[i+4]);
	Pc=buf;
	i+=4;
    } /* fi */
    else if (Pdata[i] == 25)	/* EM */
    {
	sprintf (buf,"<FCC imm.: M=%c (0x%x) N=%c (0x%x)>\n",
		 Pdata[i+1],Pdata[i+1],Pdata[i+2],Pdata[i+2]);
	Pc=buf;
	i+=2;
    } /* fi */
    else
    {
     switch (Pdata[i])
     {
      case 0: Pc = "<NUL>"; break;
      case 1: Pc = "<SOH>"; break;
      case 2: Pc = "<STX>"; break;
      case 3: Pc = "<ETX>"; break;
      case 4: Pc = "<EOT>"; break;
      case 5: Pc = "<ENQ>"; break;
      case 6: Pc = "<ACK>"; break;
      case 7: Pc = "<BEL>"; break;
      case 8: Pc = "<BS>"; break;
      case 9: Pc = "<HT>"; break;
      case 10: Pc = "<LF>"; break;
      case 11: Pc = "<VT>"; break;
      case 12: Pc = "<FF>"; break;
      case 13: Pc = "<CR>"; break;
      case 14: Pc = "<SO>"; break;
      case 15: Pc = "<SI>"; break;
      case 16: Pc = "<DLE>"; break;
      case 17: Pc = "<DC1>"; break;
      case 18: Pc = "<DC2>"; break;
      case 19: Pc = "<DC3>"; break;
      case 20: Pc = "<DC4>"; break;
      case 21: Pc = "<NAK>"; break;
      case 22: Pc = "<SYN>"; break;
      case 23: Pc = "<ETB>"; break;
      case 24: Pc = "<CAN>"; break;
      case 25: Pc = "<EM>"; break;
      case 26: Pc = "<SUB>"; break;
      case 27: Pc = "<ESC>"; break;
      case 28: Pc = "<FS>"; break;
      case 29: Pc = "<GS>"; break;
      case 30: Pc = "<RSSOE>"; break;
      case 31: Pc = "<US>"; break;
      case 127: Pc = "<DEL>"; break;
      default: Pc=buf; buf[0]=Pdata[i];buf[1]=EOS;
     } /* switch */
    } /* fi */

    if ((int)(act_len + strlen(Pc)) > 79)
    {
      fprintf (outp,"\n");
      act_len= strlen (Pc);
    } /* fi */
    fprintf (outp,Pc);
    if (Pc[strlen(Pc) - 1] == '\n')
    {
      act_len=0;
    } /* fi */
  } /* for */

  fprintf (outp,"\n");
  
} /* mts_snap */


/* ******************************************************************** */
/* The sgs_get_record  routine						*/
/* Table:Define, recno: starting with 0, Pdest: Dest data		*/
/* ******************************************************************** */
int sgs_get_record (int table, int recno, char *Pdest)
{
  int rc = RC_SUCCESS;
  STHBCB bcb;
  unsigned long ret;		/* 	return code from 'STH' */

  /* Args for sth call */
  bcb.function = (STRECORD|STREAD);
  bcb.inputbuffer = NULL; 
  bcb.outputbuffer = Pdest;
  bcb.tabno = table;	
  /* Shared Mem tables start with 1, internal counting with 0 */
  bcb.recno = recno + 1; 

  ret = sth(&bcb, &gbladr);

  switch (ret)
  {
    case  STNORMAL: rc = RC_SUCCESS; break;
    case  STINVREC: rc = RC_NOT_FOUND; break;
    default: rc = RC_FAIL; break;
  } /* switch */

  if (rc == RC_FAIL)
  {
    dbg (DEBUG, "sgs_get_record: tab=%d rc =%d ret=%d",table,rc,ret); 
    
  } /* fi */

  return rc;
} /* sgs_get_record */


/* ******************************************************************** */
/* The sgs_get_no_of_record  routine					*/
/* Table:Define, Pdest: tmp buffer					*/
/* ******************************************************************** */
int sgs_get_no_of_record (int table, char *Pdest)
{
  int rc = RC_SUCCESS;
  int recno=0;

  for (recno=0; rc == RC_SUCCESS; recno++)
  {
    rc = sgs_get_record (table, recno, Pdest);
  } /* for */

  if (rc == RC_NOT_FOUND)
    return recno - 1;
  else
    return rc;
} /* sgs_get_no_of_record */


/* ******************************************************************** */
/* The sgs_write_record  routine					*/
/* Table:Define, recno: starting with 0, Pdest: Dest data		*/
/* ******************************************************************** */
int sgs_write_record (int table, int recno, char *Pdest)
{
  int rc = RC_SUCCESS;
  STHBCB bcb;		/* for sth call 		*/
  unsigned long ret;		/* 	return code from 'STH' */

  /* Args for sth call */
  bcb.function = (STRECORD|STWRITE);	/* write one full record */
  bcb.inputbuffer = Pdest; 
  bcb.outputbuffer = NULL;
  bcb.tabno = table;	
  bcb.recno = recno+1; /* sth uses indices from 1 to 100 */

  ret = sth(&bcb, &gbladr);
  if (ret != STNORMAL)
  {
    rc = RC_FAIL;
  } /* end else */

  if ( rc != RC_SUCCESS)
    dbg (TRACE, "sgs_write_record: ret =%d rc=%d",ret, rc); 

  return rc;
} /* sgs_write_record */

/* ******************************************************************** */
/* The nam_get_host  routine						*/
/* Returns the host for the type or RC_NOT_FOUND			*/
/* ******************************************************************** */
int nam_get_host (char *Ptype, char *Phost)
{
  int rc = RC_SUCCESS;
  int recno=0;
  char buf[20];
  int cnt=0;
  NAMSRV_INFO info;
  int ready=FALSE;

  rc = sgs_get_no_of_record (NMTAB, buf);

  if (rc > 0)
  {
    cnt = rc;
    rc = RC_SUCCESS;
  }
  else
  {
    rc = RC_FAIL;
  } /* fi */

  for (recno=0; ! ready && rc == RC_SUCCESS && recno < cnt; recno++)
  {	
    rc = sgs_get_record (NMTAB, recno, buf);
    if (rc == RC_SUCCESS)
    {
      strncpy (info.serv_type, buf, 8);
      info.serv_type[8] = EOS;
      strncpy (info.serv_host, buf+8, 8);
      info.serv_host[8] = EOS;
      
/*    dbg (TRACE,"nam_get_host: Got %s %s ", info.serv_type,info.serv_host);
          */
      if (strcmp (info.serv_type, Ptype) == 0)
      {
	ready = TRUE;
	if ( strcmp (info.serv_host, NAMSRV_EMPTY) != 0 )
	{
	  strcpy (Phost, info.serv_host);
	} 
	else
	{
	  rc = RC_NOT_FOUND;
	} /* fi */
      } /* fi */
    } /* fi */
  } /* for */

  if (recno >= cnt)
  {
    rc = RC_NOT_FOUND;
  } /* fi */
  
  if (rc != RC_SUCCESS)
  {
    dbg(TRACE,"nam_get_host: returned %d type=%s host=%s",rc,Ptype,Phost);
    
  } /* fi */

  return rc;
} /* nam_get_host */

/* ******************************************************************** */
/* The nam_get_recno  routine						*/
/* Returns the record no for the host or RC_NOT_FOUND			*/
/* ******************************************************************** */
int nam_get_recno (char *Phost)
{
  int rc = RC_SUCCESS;
  int recno=0;
  char buf[20];
  int cnt=0;
  NAMSRV_INFO info;
  int ready=FALSE;

  rc = sgs_get_no_of_record (NMTAB, buf);

  if (rc > 0)
  {
    cnt = rc;
    rc = RC_SUCCESS;
  }
  else
  {
    rc = RC_FAIL;
  } /* fi */

  for (recno=0; ! ready && rc == RC_SUCCESS && recno < cnt; recno++)
  {	
    rc = sgs_get_record (NMTAB, recno, buf);
    if (rc == RC_SUCCESS)
    {
      strncpy (info.serv_host, buf+8, 8);
      info.serv_host[8] = EOS;
      
      /*dbg (TRACE,"nam_get_recno: Look for %s Got %s",Phost, info.serv_host);
          */
      if (strcmp (info.serv_host, Phost) == 0)
      {
	ready = TRUE;
      } /* fi */
    } /* fi */
  } /* for */
  
  if (ready == TRUE)
  {
    rc = recno - 1 ;
  }
  else
  {
    rc = RC_NOT_FOUND;
    dbg (TRACE,"nam_get_recno: returned %d  host=%s",rc, Phost);
    
  } /* fi */

  return rc;
} /* nam_get_recno */

/* ******************************************************************** */
/* The null_end_spaces  routine						*/
/* ******************************************************************** */
void null_end_spaces (char *Pdata, int len)
{
  int rc = RC_SUCCESS;
  int i=0;

  for (i = len-1; i >= 0 && Pdata[i] == ' '; i--)
  {
    Pdata[i] = EOS;
  } /* for */

} /* null_end_spaces */

/* ******************************************************************** */
/* The null_end_char  routine						*/
/* ******************************************************************** */
void null_end_char (char *Pdata, int len, char c)
{
  int rc = RC_SUCCESS;
  int i=0;

  for (i = len-1; i >= 0 && Pdata[i] == c; i--)
  {
    Pdata[i] = EOS;
  } /* for */

} /* null_end_char */

/* ******************************************************************** */
/* The tool_filter_spaces  routine					*/
/* ******************************************************************** */
void tool_filter_spaces (char *Pdata)
{
  int rc = RC_SUCCESS;
  int i=0, j=0;

  for (i=0, j=0; Pdata[i] != EOS; i++)
  {
    if (! isspace (Pdata[i]))
    {
      Pdata[j] = Pdata[i];
      j++;
    } /* fi */
  } /* for */

  Pdata[j] = EOS;

} /* null_end_spaces */

/* ******************************************************************** */
/* The tool_search_exco_data routine					*/
/* ******************************************************************** */
int tool_search_exco_data (char *Proc, char *Pkey, char *Presult)
{
  int	rc = RC_SUCCESS;	/* Return code 			*/
  int recno=0;
  char buf[100];
  int cnt=0;
  int ready = FALSE;

  Presult[0] = EOS;

  rc = sgs_get_no_of_record (EXTAB, buf);

  if (rc > 0)
  {
    cnt = rc;
    rc = RC_SUCCESS;
  }
  else
  {
    rc = RC_FAIL;
  } /* fi */

  for (recno=0; ! ready && rc == RC_SUCCESS && recno < cnt; recno++)
  {	
    rc = sgs_get_record (EXTAB, recno, buf);
    if (rc == RC_SUCCESS)
    {
      if (strncmp (buf, Proc, strlen(Proc)) == 0 &&
	  strncmp (buf+4, Pkey, strlen(Pkey)) == 0)
      {
	ready = TRUE;
	strcpy (Presult, buf+12);
      } /* fi */
    } /* fi */
  } /* for */
  
  if (recno == cnt && ready == FALSE)
  {
    rc = RC_NOT_FOUND;
  } /* fi */

  /*dbg (DEBUG,
	   "tool_search_exco_data: rc=%d Proc=%s Key=%s \n\tResult=%s",
	   rc,Proc,Pkey, Presult);*/  
  return rc;
} /* tool_search_exco_data */


/* ******************************************************************** */
/* The tool_write_exco_data routine					*/
/* ******************************************************************** */
int tool_write_exco_data (char *Proc, char *Pkey, char *Pdata)
{
  int	rc = RC_SUCCESS;	/* Return code 			*/
  int recno=0;
  char buf[80];
  int cnt=0;
  int ready = FALSE;


  rc = sgs_get_no_of_record (EXTAB, buf);

  if (rc > 0)
  {
    cnt = rc;
    rc = RC_SUCCESS;
  }
  else
  {
    rc = RC_FAIL;
  } /* fi */

  for (recno=0; ! ready && rc == RC_SUCCESS && recno < cnt; recno++)
  {	
    rc = sgs_get_record (EXTAB, recno, buf);
    if (rc == RC_SUCCESS)
    {
      if (strncmp (buf, Proc, strlen(Proc)) == 0 &&
	  strncmp (buf+4, Pkey, strlen(Pkey)) == 0)
      {
	ready = TRUE;
	strncpy (buf+12, Pdata, 67);
	buf[12+67]=EOS;
	rc = sgs_write_record (EXTAB, recno, buf);
      } /* fi */
    } /* fi */
  } /* for */
  
  if (recno == cnt && ready == FALSE)
  {
    rc = RC_NOT_FOUND;
  } /* fi */

  dbg (DEBUG, "tool_write_exco_data: rc=%d Proc=%s Key=%s \n\tPdata=%s",
	   rc,Proc,Pkey, Pdata);  
  return rc;
} /* tool_write_exco_data */

/* ******************************************************************** */
/* The tool_get_q_id routine						*/
/* returns the QCP-ID for the given process name or RC_NOT_FOUND or	*/
/* RC_FAIL								*/
/* ******************************************************************** */
int tool_get_q_id (char *Pnam)
{
  int	rc = RC_SUCCESS;	/* Return code 			*/
  int recno=0;
  char buf[30];
  int cnt=0;
  int ready = FALSE;
  int i;

  rc = sgs_get_no_of_record (PNTAB, buf);

  if (rc > 0)
  {
    cnt = rc;
    rc = RC_SUCCESS;
  }
  else
  {
    rc = RC_FAIL;
  } /* fi */

  for (recno=0; ! ready && rc == RC_SUCCESS && recno < cnt; recno++)
  {	
    rc = sgs_get_record (PNTAB, recno, buf);
    if (rc == RC_SUCCESS)
    {
      if (strncmp (buf, Pnam, strlen(Pnam)) == 0)
      {
	ready = TRUE;
#if defined(_SNI) || defined(_HPUX_SOURCE) || defined(_SOLARIS) || defined(_AIX)
	rc = *((short *) (buf+8));
#else
	rc = *((int *) (buf+8));
#endif
      } /* fi */
    } /* fi */
  } /* for */
  
  if (rc != RC_SUCCESS)
  {
	 /*
    dbg (TRACE,"tool_get_q_id: returned %d Pnam=%s cnt=%d", rc,Pnam,cnt);
	 */
  }

  return rc;
} /* tool_get_q_id */

/* ******************************************************************** */
/* The tool_make_string routine						*/
/* Makes a PASCAL-like string						*/
/* ******************************************************************** */
void tool_make_string (char *Psrc, char *Pdest, unsigned short len)
{
  Pdest[0] = (char)(len & 0xff00) >> 8;
  Pdest[1] = (len & 0x00ff) ;
  memcpy (Pdest+2, Psrc, len);
} /* tool_make_string */

/* ******************************************************************** */
/* The tool_make_string2 routine					*/
/* Makes a PASCAL-like string - uses strcpy				*/
/* ******************************************************************** */
void tool_make_string2 (char *Psrc, char *Pdest, unsigned short len)
{
  Pdest[0] = (char )(len & 0xff00) >> 8;
  Pdest[1] = (len & 0x00ff) ;
  strcpy (Pdest+2, Psrc);
} /* tool_make_string2 */

/* ******************************************************************** */
/* The tool_swap_bytes routine						*/
/* Makes a SUN-like short						*/
/* ******************************************************************** */
unsigned short tool_swap_bytes (short val)
{
  unsigned short res=0;

  res = ((val & 0xff00) >> 8) | ((val & 0x00ff) << 8) ;
  return res;
} /* tool_make_string */


/* ******************************************************************** */
/* The rc_strerror routine						*/
/* ******************************************************************** */
void rc_strerror (int no, char **PPerr)
{
  switch (no)
  {
    case RC_SUCCESS: *PPerr = "SUCCESS"; break;
    case RC_COMM_FAIL: *PPerr = "COMM FAIL"; break;
    case RC_INIT_FAIL: *PPerr = "INIT FAIL"; break;
    case RC_CEDA_FAIL: *PPerr = "CEDA FAIL"; break;
    case RC_SHUTDOWN: *PPerr = "SHUTDOWN"; break;
    case RC_ALREADY_INIT: *PPerr = "ALREADY INIT"; break;
    case RC_NOT_FOUND: *PPerr = "NOT FOUND"; break;
    case RC_TIMEOUT: *PPerr = "TIMEOUT"; break;
    case RC_OVERFLOW: *PPerr = "OVERFLOW"; break;
    default: *PPerr = "Unknown error code"; break;
  }
} /*  rc_strerror */


/* ******************************************************************** */
/* The tool_get_field_data routine					*/
/* Get the data for the fieldname out of an field/data string pair	*/
/* ******************************************************************** */
int tool_get_field_data (char *Pfields, char *Pdata, char *Pfname, char *Pres)
{
  int	rc = RC_SUCCESS;	/* Return code 			*/
  int pos=0;

  rc = flt_search_field (Pfields, Pfname, &pos);
  if (rc == RC_SUCCESS)
  {
    rc = flt_cp_nth_field (Pdata, pos, Pres);
  } /* fi */

  dbg(DEBUG,"tool_get_field_data: returned %d Pfname=%s Pres=%s",
      rc, Pfname, Pres);
  return rc;
} /* tool_get_field_data */

/* ******************************************************************** */
/* Get a free dynamic queue                                             */
/* VBL 01.02.1996                                                       */
/* ******************************************************************** */
int	GetDynamicQueue(int *pipQueueId, char *pcpQueueName)
{
	int	 ilRC = RC_SUCCESS;
	int  ilCnt = 0;
	int  ilDummyId = 0;
	char rlstrQueueName[MAX_PRG_NAME+1];
	ITEM *prlitmResponseItem = NULL;
	
	if(pipQueueId == NULL)
	{
		return(RC_FAIL);
	}/* end of if */

	ilCnt = 0;
	do
	{
		ilRC = init_que();
		if(ilRC != RC_SUCCESS)
		{
			ilCnt++;
			sleep(1);
		}/* end of if */
	} while((ilRC != RC_SUCCESS) && (ilCnt < 5));

	if(ilRC == RC_SUCCESS)
	{
    /* 20070226 JIM: on AIX the pids are not continously increasing but random, so the
    * modulo may create same tag at 2 simultaniosly starting NETINs. Use complete PID
    * ilDummyId = ((getpid()%1000)+30000);
    */
    ilDummyId = getpid()+30000;

		memset(rlstrQueueName,0x00,MAX_PRG_NAME+1);

		ilCnt = strlen(pcpQueueName);

		if(ilCnt >= MAX_PRG_NAME)
		{
			memcpy(rlstrQueueName,pcpQueueName,MAX_PRG_NAME);
		} else {
			if(ilCnt >= 1)
			{
				memcpy(rlstrQueueName,pcpQueueName,ilCnt);
			} else {
				sprintf(rlstrQueueName,"C_C_S");
				ilCnt = strlen(rlstrQueueName);
			}/* end of if */
		}/* end of if */

		ilRC = que(QUE_CREATE,0,ilDummyId,0,ilCnt+1,rlstrQueueName);
		if(ilRC == RC_SUCCESS)
		{
			prlitmResponseItem = (ITEM *)malloc(sizeof(ITEM));
			if(prlitmResponseItem == NULL)
			{
				ilRC = RC_FAIL;
			} else {
				ilRC = que(QUE_RESP,0,ilDummyId,0,sizeof(ITEM),(char *)prlitmResponseItem);
				if(ilRC == RC_SUCCESS)
				{
					*pipQueueId = prlitmResponseItem->originator;
				}/* end of if */

				free(prlitmResponseItem);
			}/* end of if */
		}/* end of if */
	}/* end of if */

	return(ilRC);

}/* end of GetDynamicQueue */

/* ******************************************************************** */
/* Get a free dynamic queue for myself only                             */
/* JIM 20050609: in WMQ a QUE_PUT normally closes the queue. This has to*/
/*               be avoided, if a proc wants to use a queue as buffer   */
/* ******************************************************************** */
int	GetDynamicQueueToMe(int *pipQueueId, char *pcpQueueName)
{
	int	 ilRC = RC_SUCCESS;
	int  ilCnt = 0;
	int  ilDummyId = 0;
	char rlstrQueueName[MAX_PRG_NAME+1];
	ITEM *prlitmResponseItem = NULL;
	
	if(pipQueueId == NULL)
	{
		return(RC_FAIL);
	}/* end of if */

	ilCnt = 0;
	do
	{
		ilRC = init_que();
		if(ilRC != RC_SUCCESS)
		{
			ilCnt++;
			sleep(1);
		}/* end of if */
	} while((ilRC != RC_SUCCESS) && (ilCnt < 5));

	if(ilRC == RC_SUCCESS)
	{
    /* 20070226 JIM: on AIX the pids are not continously increasing but random, so the
    * modulo may create same tag at 2 simultaniosly starting NETINs. Use complete PID
    * tag = ((getpid()%1000)+30000);
    */
    ilDummyId = getpid()+30000;

		memset(rlstrQueueName,0x00,MAX_PRG_NAME+1);

		ilCnt = strlen(pcpQueueName);

		if(ilCnt >= MAX_PRG_NAME)
		{
			memcpy(rlstrQueueName,pcpQueueName,MAX_PRG_NAME);
		} else {
			if(ilCnt >= 1)
			{
				memcpy(rlstrQueueName,pcpQueueName,ilCnt);
			} else {
				sprintf(rlstrQueueName,"C_C_S");
				ilCnt = strlen(rlstrQueueName);
			}/* end of if */
		}/* end of if */

		ilRC = que(QUE_CREATE,0,ilDummyId,0,ilCnt+1,rlstrQueueName);
		if(ilRC == RC_SUCCESS)
		{
			prlitmResponseItem = (ITEM *)malloc(sizeof(ITEM));
			if(prlitmResponseItem == NULL)
			{
				ilRC = RC_FAIL;
			} else {
				ilRC = que(QUE_WMQ_RESP,0,ilDummyId,0,sizeof(ITEM),(char *)prlitmResponseItem);
				if(ilRC == RC_SUCCESS)
				{
					*pipQueueId = prlitmResponseItem->originator;
				}/* end of if */

				free(prlitmResponseItem);
			}/* end of if */
		}/* end of if */
	}/* end of if */

	return(ilRC);

}/* end of GetDynamicQueueToMe */


/* added bch 3/4/97 */
/********************************************************************/
/* return a count of the number of items in pcpString               */
/* eg.	"one,two,three" 	contains 3 items                        */
/* 		"one" 				contains 1 item                         */
/* 		"" 					contains 0 items                        */
/* 		"," 				contains 2 items                        */
/********************************************************************/
extern int itemCount(char *pcpString)
{
	char *pclSubString = strchr(pcpString,',');
	int	 ilItemCount = strlen(pcpString) ? 1 : 0;

	while( pclSubString != NULL )
	{
		ilItemCount++;
		pclSubString = strchr(pclSubString+1,',');

	} /* end while() */

	return ilItemCount;

} /* end itemCount() */


/* added bch 3/4/97 */
/***************************************************/
/* check if a field is NULL, if so set it to BLANK */
/***************************************************/
extern void removeNull(char *pcpField)
{
	if( pcpField == NULL || strlen(pcpField) <= 0 )
		strcpy(pcpField," ");

} /* end removeNull() */


/* added bch 3/4/97 */
/*****************************************************************************/
/* if the date format is yyyymmddhhmm add 00 to the end to represent seconds */
/*****************************************************************************/
extern void addSecs(char *pcpDate)
{
	if ( strlen(pcpDate) == 12 || pcpDate[12] == ' ' )
	{
		pcpDate[12] = '0';
		pcpDate[13] = '0';
	}

} /* end addSeconds() */

/***************************************************************************/
/*                                                                         */
/* Function: getItem()                                                     */
/*                                                                         */
/* Parameters: pcpFieldList - list of field names                          */
/*             pcpDataList - list of data                                  */
/*             pcpField - field to be returned                             */
/*                                                                         */
/* Return: pointer to pcpValue or NULL if field not found.                 */
/*                                                                         */
/* Example calls:                                                          */
/*                                                                         */
/*	getItem(clFieldList,clDataList,"USID",);                               */
/*                                                                         */
/*	ilUrno = atoi( getItem(clFieldList,clDataList,"URNO");                 */
/*                                                                         */
/* Where: clFieldList contains a comma seperated list of fields, eg.       */
/*			"URNO,USID,TYPE"                                               */
/*                                                                         */
/*		   clDataList contains a comma seperated list of data, eg.         */
/*			"2234345,Username,X"                                           */
/*                                                                         */
/***************************************************************************/
extern char *
getItem(char *pcpFieldList, char *pcpDataList, char *pcpField)
{
	char *pclSubStr = NULL;
	char *pclSubStr2 = NULL;
	int	 ilTmp;
	short slItemNum;
	int ilNotFound;

	ilTmp = strlen(pcpField);

	if( (ilTmp<=0) || (int)strlen(pcpFieldList)<ilTmp || (int)strlen(pcpDataList)<=0 )
		return (char *) NULL;


	/* find first the item num of the field being searched for */
	for( pclSubStr = pcpFieldList, slItemNum = 0, ilNotFound = 1;
		  pclSubStr != NULL  &&  ilNotFound; slItemNum++ )
	{
		ilNotFound = strncmp(pcpField,pclSubStr,ilTmp);
		if( ilNotFound && (pclSubStr = (char *) strchr(pclSubStr,',')) != NULL )
			pclSubStr++;
	}

	/* field name not found */
	if( ilNotFound )
		return (char *) NULL;

	return((char *) sGet_item2(pcpDataList,slItemNum));

} /* end getItem() */

/***************************************************************
 *
 * Function: new_tools_send_sql
 *
 * Parameters: 	IN: int ipRouteID	address to send the command to
 *				IN: BC_HEAD prpBchd     Broadcast header
 *				IN: prpCommand	command type (ie. RT,DBT,AUA etc.)
 *				IN: pcpFields	SQL field names
 *				IN: pcpData		Data to send
 *
 * Return: RC_SUCCESS, RC_FAIL.	
 *
 * Description: This function is identical to tools_send_sql_rc()
 *				except that you can also pass BC_HEAD information back
 *				to the receiving process.
 *
 * Created 24/07/97 by gsa.
 *
 ********************************************************************/

int new_tools_send_sql(int ipRouteID,
						BC_HEAD *prpBchd,
						CMDBLK *prpCmdblk,
						char *pcpFields,
						char *pcpData)
{
  int 		ilRC = RC_SUCCESS;
  static int ilTotalLen=0;
	int		ilLength=0;
  BC_HEAD 	*prlBchd;		/* Broadcast header		*/
  CMDBLK  	*prlCmdBlk;		/* Command Block 		*/
  char 		*pclDataBlk, pclErr[25];		/* Data Block			*/
  char		*pclSel;
  char		*pclField;
  static EVENT 	*prlOutEvent = NULL;

	dbg(DEBUG,"Entering new_tools_*. TotalLen<%d>   prlOutEvent<%ld>  DataLength<%i>",ilTotalLen,prlOutEvent,strlen(pcpData));	
	if( prpBchd->rc!= RC_SUCCESS )
        {
		/* if there was an error then the data area gets copied to
			the area BCDH->data */
  		ilLength = strlen(pcpFields) + strlen(pcpData) + strlen(pcpData) + strlen(prpCmdblk->data) + sizeof(BC_HEAD)  + sizeof(CMDBLK) + sizeof(EVENT) + 128; 
        } /* end if */
	else
        {
  		ilLength = strlen(pcpFields) + strlen(pcpData) + strlen(prpCmdblk->data) + sizeof(BC_HEAD)  + sizeof(CMDBLK) + sizeof(EVENT) + 128; 
        } /* end else */

	dbg(DEBUG,"new_tools_: ilLength<%d>",ilLength);
	if (prlOutEvent==NULL) {
		while(ilTotalLen<ilLength)
			ilTotalLen+=32*1024; /* Increase in steps of 32K */
		prlOutEvent = (EVENT *) malloc(ilTotalLen);
		if (prlOutEvent == NULL){
			ilRC= RC_FAIL;
			dbg(TRACE,"new_tools_send_sql: Malloc error %s",strerror(errno));
		} else {
			dbg(DEBUG,"new_tools_send_sql: Alloced prlOutEvent(size= %d)",ilTotalLen);
		}
	} else if (ilTotalLen<=ilLength){
		while(ilTotalLen<=ilLength)
			ilTotalLen+=32*1024; /* Increase in steps of 32K */
		prlOutEvent=(EVENT *) realloc(prlOutEvent,ilTotalLen);
		if(prlOutEvent==NULL){
			ilRC=RC_FAIL;
			dbg(TRACE,"new_tools_send_sql: Malloc: %s", strerror(errno)); 
		} else {
			dbg(DEBUG,"new_tools_send_sql: Realloced prlOutEvent<size %d>",ilTotalLen);    
		}
	} else {
		dbg(DEBUG,"new_tools_send_sql: Already alloced<size %d>",ilTotalLen);
	}
if(ilRC!=RC_FAIL) {
	memset (((char *) prlOutEvent),0, ilTotalLen);
	prlOutEvent->data_offset = sizeof(EVENT);
	prlOutEvent->originator = mod_id; /* should B global */
	prlOutEvent->data_length =  ilLength;
	prlOutEvent->command = EVENT_DATA;
 
	prlBchd = (BC_HEAD *) (((char *)prlOutEvent) + sizeof(EVENT));
	memcpy(prlBchd,prpBchd,sizeof(BC_HEAD));
	dbg(DEBUG,"new_tools: prlBchd->recv_name<%s>",prlBchd->recv_name);		

/* 20050531 JIM: PRF 7443: do not move event in WMQ Environment */
#ifndef WMQ
		if( prpBchd->rc!= RC_SUCCESS ) 
		{
			/* if there was an error, then the data which contains
				the error description, gets copied to the BCHD->data
				area. This is because the dll uses this when the RC is
				non zero ! The command block will then point to the
				address after the BCHD address plus the length of the data */
			dbg(DEBUG,"Copying data to BC Header<%s>",pcpData);
			strcpy(prlBchd->data,pcpData);
			prlCmdBlk = (CMDBLK *)(((char *)prlBchd->data)+strlen(pcpData)+1);
			dbg(DEBUG,"new_tools_: !RC_SUCCESS-> rc<%d> Cmd<%s>  Obj<%s>",prpBchd->rc,prpCmdblk->command,prpCmdblk->obj_name);	
		}
		else
		{
			prlCmdBlk= (CMDBLK  *) ((char *)prlBchd->data);
			dbg(DEBUG,"new_tools_: RC_SUCCESS-> rc<%d> Cmd<%s>  Obj<%s>",prpBchd->rc,prpCmdblk->command,prpCmdblk->obj_name);	
		}
#else  /* WMQ */
		prlCmdBlk= (CMDBLK  *) ((char *)prlBchd->data);
		if( prpBchd->rc!= RC_SUCCESS ) 
		{
		  prpBchd->rc = RC_WMQ_FAIL;
		  prlBchd->rc = RC_WMQ_FAIL;
		  dbg(TRACE,"new_tools_: rc<%d>! Cmd<%s>  Obj<%s>",prpBchd->rc,prpCmdblk->command,prpCmdblk->obj_name);	
		}
		else
		{
		  dbg(TRACE,"new_tools_: rc<%d> Cmd<%s>  Obj<%s>",prpBchd->rc,prpCmdblk->command,prpCmdblk->obj_name);	
		}
#endif /* WMQ */
		strcpy(prlCmdBlk->command,prpCmdblk->command);
		strcpy(prlCmdBlk->obj_name,prpCmdblk->obj_name);
		strcpy(prlCmdBlk->order,prpCmdblk->order);
		strcpy(prlCmdBlk->tw_start,prpCmdblk->tw_start);
		strcpy(prlCmdBlk->tw_end,prpCmdblk->tw_end);

		dbg(DEBUG,"new_tools_: rc<%d> Cmd<%s>  Obj<%s>",prlBchd->rc,prlCmdBlk->command,prlCmdBlk->obj_name);	
		pclSel = prlCmdBlk->data;
		strcpy (pclSel, prpCmdblk->data);
		strcpy (prlCmdBlk->obj_name,prpCmdblk->obj_name);
		pclField = (char *) pclSel + strlen (pclSel) + 1;
		strcpy (pclField, pcpFields);
		pclDataBlk   = pclField + strlen(pclField) + 1;
		strcpy (pclDataBlk, pcpData);
		dbg(DEBUG,"new_tools_send_sql: Sel<%s>  Field<%s>  Data Length<%d>",pclSel,pclField,strlen(pclDataBlk));
		dbg(DEBUG,"new_tools_send_sql: Len<%d>   Estimated<%d>",(strlen(pclSel)+strlen(pclField)+strlen(pclDataBlk)+sizeof(BC_HEAD)+sizeof(CMDBLK)+sizeof(EVENT)),ilLength);
		/* DebugPrintEvent(TRACE,prlOutEvent);	
		DebugPrintBchead(TRACE,prlBchd);	
		DebugPrintCmdblk(TRACE,prlCmdBlk);	*/
		ilRC = que(QUE_PUT,ipRouteID,mod_id,PRIORITY_3,ilLength,
	    		 (char *)prlOutEvent);
		dbg(DEBUG,"new_tools_send_sql: Returning rc=%d", ilRC); 	
	}
	return ilRC;
} /* end new_tools_send_sql */

/* ****************************************************************** 
* The StrnCaseCmp routine				
* Compare first n characters of two strings. Very much like strcmp
* however case-insensitive.
* the field, field counting starts with 1, fields are separated by ','	
* The return value is RC_SUCCESS, if the field has been found, RC_FAIL 
* else								
* ****************************************************************** */

int StrnCaseCmp (char *Pstr, char *Pfield, int Pn)
{
  int rc = 0;
  int i=0;
	rc=0;	
	for(i=0;i<Pn ;i++){
		if(toupper(Pstr[i])<toupper(Pfield[i])){
			return -1;
		} else if(toupper(Pstr[i])>toupper(Pfield[i])){
			return 1;	
		}
	}
	return rc;
} /* StrnCaseCmp */


/* ****************************************************************** 
* The string_to_key routine				
* ****************************************************************** */
int string_to_key (char *pcpStr, char *pcpKey)
{
  int i=0;
  int iLen=strlen(pcpStr);

  memset (pcpKey, 0, iLen+1);
  for (i=0; i < iLen; i++)
  {
    pcpKey[i] = (pcpStr[i] + i + 3) & 0x7f;
    if (pcpKey[i] < 0x20)
    {
      pcpKey[i] += 0x20;
    } /* fi */
  } /* for */

/*  dbg (DEBUG,"string_to_key: Str=%s Key=%s",pcpStr, pcpKey);*/
  return RC_SUCCESS;
} /* string_to_key */

/* ****************************************************************** 
 * ConfigureAction:  Send dynamic configuration to action
 * Parameters: 	IN: pcpTableName Complete table name including TABEND
 *				IN: pcpFields	 Field list (comma separated), 
 *							 	 if NULL all fields ("") will be set
 *				IN: pcpCommands	 command list (comma separated), 
 *							 	 if NULL "IRT,URT,DRT" will be set
 * Return: RC_SUCCESS, RC_FAIL.	
 * ****************************************************************** */
int ConfigureAction (char *pcpTableName, char *pcpFields, char *pcpCommands)
{
	return ChangeDynActionConfig (pcpTableName, pcpFields, pcpCommands, TRUE, TRUE );
}


int ChangeDynActionConfig (char *pcpTableName, char *pcpFields, char *pcpCommands, int bpAdd, int bpDel )
{
	int        		ilRc = RC_SUCCESS;    /* Return code */
	int        		ilRc2 = RC_SUCCESS;    
	BC_HEAD			*prlBchead = NULL;
	CMDBLK			*prlCmdblk = NULL;
	ACTIONConfig	rlAction;
 	char			*pclSelection = NULL;
	char			*pclFields = NULL;
	char			*pclData = NULL;
	static long		llActSize = 0;
	static int		ilActionId = -1;
	static EVENT	*pclEvent=0;

	if ( ilActionId == -1 )
	{
		ilActionId = tool_get_q_id ("action");
		if ( ilActionId >= 0 )
			dbg(DEBUG,"ChangeDynActionConfig: found mod_id <%d> for action", ilActionId );
	}
	if ( ilActionId <= 0 )
	{
		ilRc == RC_FAIL;
		dbg(TRACE,"ChangeDynActionConfig: Could not find mod_id for action <%d>", ilActionId );
	}
	
	if ( !pcpTableName )
	{
		dbg(TRACE,"ChangeDynActionConfig: Invalid parameter, TABLENAME is NULL" );
		ilRc = RC_FAIL;
	}

	if ( (ilRc == RC_SUCCESS) && !pclEvent )
	{
		llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK)
				   + sizeof(ACTIONConfig) + 666;
		pclEvent = malloc (llActSize);
		if(pclEvent == NULL)
		{
			dbg(TRACE,"ChangeDynActionConfig: malloc of event <%d> bytes failed", llActSize);
			ilRc = RC_FAIL;
		}
		else
			dbg(DEBUG,"ChangeDynActionConfig: allocated <%d> bytes for event", llActSize);
	}
	if(ilRc == RC_SUCCESS)
	{
		memset( pclEvent, 0, llActSize );	

		prlBchead     = (BC_HEAD *)((char *)pclEvent + sizeof(EVENT));
		
		memset( prlBchead, 0, sizeof(BC_HEAD) );	
		prlBchead->rc = RC_SUCCESS;					
		strcpy(prlBchead->dest_name, mod_name );	
		strcpy(prlBchead->recv_name, "ACTION" );	
		DebugPrintBchead(DEBUG,prlBchead) ;

   		prlCmdblk     = (CMDBLK *)((char *) prlBchead->data);
   		pclSelection  = prlCmdblk->data;
   		pclFields     = pclSelection + strlen(pclSelection) + 1;
   		pclData       = pclFields + strlen(pclFields) + 1;
   		strcpy (pclData, "DYN");

		memset(&rlAction,0,sizeof(ACTIONConfig));
   		/*prlAction     = (ACTIONConfig *)(pclData + strlen(pclData) + 1);*/
		
   		pclEvent->type        = USR_EVENT;
   		pclEvent->command     = EVENT_DATA;
   		pclEvent->originator  = mod_id;
   		pclEvent->retry_count = 0;
   		pclEvent->data_offset = sizeof(EVENT);
   		pclEvent->data_length = llActSize-sizeof(EVENT);
		DebugPrintEvent(DEBUG,pclEvent) ;

   		rlAction.iEmptyFieldHandling = iDELETE_ALL_BLANKS;
   		rlAction.iIgnoreEmptyFields = 0;
   		strcpy(rlAction.pcSndCmd, "");
   		sprintf(rlAction.pcSectionName,"%s_%s", mod_name,pcpTableName);
   		strcpy(rlAction.pcTableName, pcpTableName);
   		if ( pcpFields )
			strcpy ( rlAction.pcFields, pcpFields );
		else
	   		strcpy(rlAction.pcFields, "");

		if ( pcpCommands )
			strcpy(rlAction.pcSectionCommands, pcpCommands );
		else
	   		strcpy(rlAction.pcSectionCommands, "IRT,URT,DRT"); /* default */

   		rlAction.iModID = mod_id;

   		if ((ilRc == RC_SUCCESS) && bpDel )
   		{
   			rlAction.iADFlag = iDELETE_SECTION;

			/* 20030723 HAG/JIM: avoid alignment error by writing int to pclData */
			memcpy(pclData + strlen(pclData) + 1,&rlAction,sizeof(ACTIONConfig));

  	 		ilRc2 = que(QUE_PUT,ilActionId,mod_id,PRIORITY_2,llActSize, (char *)pclEvent); 
   			if(ilRc2 != RC_SUCCESS)
   				dbg(TRACE,"ChangeDynActionConfig: QUE_PUT failed <%d>",ilRc2);
   		}
	}

	if ((ilRc == RC_SUCCESS) && bpAdd )
	{
	    rlAction.iADFlag = iADD_SECTION ; 
	    DebugPrintACTIONConfig(DEBUG,&rlAction) ;
	
		/* 20030710 HAG/JIM: avoid enlignment error by writing int to pclData */
	    memcpy(pclData + strlen(pclData) + 1,&rlAction,sizeof(ACTIONConfig));
		ilRc2 = que(QUE_PUT,ilActionId,mod_id,PRIORITY_2,llActSize, (char *)pclEvent); 
		if(ilRc2 != RC_SUCCESS)
		{
			dbg(TRACE,"ChangeDynActionConfig: QUE_PUT failed <%d>",ilRc2);
		}
		else
		{
			dbg(DEBUG,"ChangeDynActionConfig: add dynamic action config OK");
		}
	}

	return ilRc;
}


/******************************************************************************/
/* function Get_CEI_Cfg: read "Ceda Event Interface" configuration.           */
/* The "Ceda Event Interface" is used for communication from UFIS to UFIS.    */
/* Each concerned process should configure the CEI in UfisToUfis.cfg by       */
/* one line, i.e. the process name.                                           */
/* Parameter: IN    pcpMyName:   Name of line to read                         */
/*            OUT   pcpDestBuff: line read                                    */
/*            IN    pcpDefault:  default, if no line found                    */
/* Return value:    RC_SUCCESS if file UfisToUfis.cfg found and line read     */
/*                  RC_FAIL else, even if default is set                      */
/* Example: Get_CEI_Cfg("flight",pclDest,"CLIENT,7835,7800")                  */
/*          this would look for a line "flight = " in section "[CEI]" and use */
/*          "CLIENT,7835,7800" if not succeded                                */
/******************************************************************************/
int Get_CEI_Cfg (char *pcpMyName, char *pcpDestBuff, char *pcpDefault)
{
  char *pclPtrPath;
  int ilRC = RC_SUCCESS;
  char pclActPath[128];
  char pclCEI_CfgFile[256];

  pcpDestBuff[0] = 0x00;
  /* Attach Config File */
  if ((pclPtrPath = getenv ("CFG_PATH")) == NULL)
  {
    strcpy (pclActPath, "/ceda/conf");
  }                           /* end if */
  else
  {
    strcpy (pclActPath, pclPtrPath);
  }                           /* end else */
  sprintf (pclCEI_CfgFile, "%s/UfisToUfis.cfg", pclActPath);

  ilRC = iGetConfigRow (pclCEI_CfgFile, "CEI", pcpMyName, CFG_STRING, pcpDestBuff);
  if (ilRC != RC_SUCCESS)
  {
    /* use default entry */
    strcpy (pcpDestBuff, pcpDefault);
  }                             /* end if */
  return ilRC;
}                               /* end Get_CEI_Cfg */


/******************************************************************************/
/*                                                                            */
/******************************************************************************/
int CheckMySizeDiff(long lpMaxSizeDiff)
{
  static long llMyInitSize = -1;
  int ilRC = RC_SUCCESS;
  long llMySize = 0;
  long llSizeDiff = 0;
  llMySize = GetMySize(-1);
  if (llMyInitSize < 0)
  {
    llMyInitSize = llMySize;
    dbg(TRACE,"===== MY INITIAL SIZE = %d K =====", llMyInitSize);
  }
  llSizeDiff = llMySize - llMyInitSize;
  if ((lpMaxSizeDiff > 0) && (llSizeDiff > lpMaxSizeDiff))
  {
    dbg(TRACE,"===== SIZE LIMIT (%dK) WARNING =====", lpMaxSizeDiff);
    dbg(TRACE,"===== SIZE CURRENT = %dK", llMySize);
    dbg(TRACE,"===== SIZE INITIAL = %dK", llMyInitSize);
    dbg(TRACE,"===== SIZE INCREASED BY %dK =====", llSizeDiff);
    ilRC = RC_FAIL;
  }
  return ilRC;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
long GetMySize(int ipUsePid)
{
  static int ilMyPid = -1;
  long llMySize = 1;
#if defined(_SOLARIS)
  int ilFd = -1;
  char pclFile[128];
  struct prpsinfo p;
  if (ipUsePid > 0)
  {
    sprintf(pclFile,"/proc/%d",ipUsePid);
  }
  else
  {
    if (ilMyPid < 0)
    {
      ilMyPid = getpid();
    }
    sprintf(pclFile,"/proc/%d",ilMyPid);
  }
  if ((ilFd = open(pclFile, O_RDONLY)) >= 0)
  {
    errno = 0;
    if (ioctl(ilFd, PIOCPSINFO, (void *) &p) >= 0)
    {
      llMySize = p.pr_size*8;
      /*
      printf("Size of this process is %ld KB, resident %ld KB\n\n",
      p.pr_size*8,p.pr_rssize*8);
      */
    }
    else
    {
      dbg(TRACE,"Can't read process information from file <%s>",pclFile);
      dbg(TRACE,"Errno %d %s",errno,strerror(errno));
    }
    close(ilFd);
  }
  else
  {
    dbg(TRACE,"Can't open process information from file <%s>",pclFile);
  }
#endif
  return llMySize;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
void ToolsSetSpoolerInfo(char *pcpUsr, char *pcpWks, char *pcpTws, char *pcpTwe)
{
  if (igInitSpooler == FALSE)
  {
    ToolsInitEventSpooler();
  }
  strcpy(pcgEvtDestName, pcpUsr);
  strcpy(pcgEvtRecvName, pcpWks);
  strcpy(pcgEvtTwStart, pcpTws);
  strcpy(pcgEvtTwEnd, pcpTwe);
  CT_ResetContent(pcgEvtSpool);
  return;
}
/******************************************************************************/
/*                                                                            */
/******************************************************************************/
int ToolsSpoolEventData(char *pcpRoute, char *pcpTbl, char *pcpCmd,
                        char *pcpUrnoList, char *pcpSel, char *pcpFld, 
                        char *pcpDat,char *pcpOldDat)
{
  int ilRC = RC_SUCCESS;
  int ilSelLen = 0;
  int ilSelPos = 0;
  int ilActLen = 0;
  int ilActPos = 0;
  int ilModPos = 0;
  int ilDatLen = 0;
  int ilDatPos = 0;

  if (igInitSpooler == FALSE)
  {
    ToolsInitEventSpooler();
  }

  if (pcpRoute[0] != '\0')
  {
    /*
    dbg(TRACE,"SPOOLING EVENT FOR DEST <%s>",pcpRoute);
    dbg(TRACE,"SPOOL TBL <%s>",pcpTbl);
    dbg(TRACE,"SPOOL CMD <%s>",pcpCmd);
    dbg(TRACE,"SPOOL SEL <%s>",pcpSel);
    dbg(TRACE,"SPOOL URN <%s>",pcpUrnoList);
    dbg(TRACE,"SPOOL USR <%s>",pcgEvtDestName);
    dbg(TRACE,"SPOOL WKS <%s>",pcgEvtRecvName);
    dbg(TRACE,"SPOOL TWS <%s>",pcgEvtTwStart);
    dbg(TRACE,"SPOOL TWE <%s>",pcgEvtTwEnd);
    dbg(TRACE,"SPOOL FLD <%s>",pcpFld);
    dbg(TRACE,"SPOOL NEW <%s>",pcpDat);
    dbg(TRACE,"SPOOL OLD <%s>",pcpOldDat);
    */

    ilSelLen = strlen(pcpUrnoList) + strlen(pcpSel) + 32;
    if (ilSelLen > argSpoolData[0].AllocatedSize)
    {
      argSpoolData[0].Value = (char *) realloc(argSpoolData[0].Value,ilSelLen);
      argSpoolData[0].AllocatedSize = ilSelLen;
    }
    ilDatLen = strlen(pcpDat) + strlen(pcpOldDat) + 32;
    if (ilDatLen > argSpoolData[1].AllocatedSize)
    {
      argSpoolData[1].Value = (char *) realloc(argSpoolData[1].Value, ilDatLen);
      argSpoolData[1].AllocatedSize = ilDatLen;
    }
    if ((argSpoolData[0].Value != NULL) && (argSpoolData[1].Value != NULL))
    {
      StrgPutStrg(argSpoolData[0].Value,&ilSelPos,pcpSel,0,-1,"\n");
      if (pcpUrnoList[0] != '\0')
      {
        StrgPutStrg(argSpoolData[0].Value,&ilSelPos,pcpUrnoList,0,-1,"\n");
      }
      ilSelPos--;
      argSpoolData[0].Value[ilSelPos] = 0x00;
      StrgPutStrg(argSpoolData[1].Value,&ilDatPos,pcpDat,0,-1,"\n");
      if (pcpOldDat[0] != '\0')
      {
        StrgPutStrg(argSpoolData[1].Value,&ilDatPos,pcpOldDat,0,-1,"\n");
      }
      ilDatPos--;
      argSpoolData[1].Value[ilDatPos] = 0x00;

      ilActLen = strlen(pcpRoute) + 16;
      ilActLen += strlen(pcpCmd) + 16;
      ilActLen += strlen(pcpTbl) + 16;
      ilActLen += ilSelPos + 16;
      ilActLen += strlen(pcpFld) + 16;
      ilActLen += ilDatPos + 16;
      ilActLen += 16;
      if (ilActLen > argSpoolData[2].AllocatedSize)
      {
        argSpoolData[2].Value = (char *) realloc(argSpoolData[2].Value,ilActLen);
        argSpoolData[2].AllocatedSize = ilActLen;
      }
      if (argSpoolData[2].Value != NULL)
      {
        ilSelPos--;
        ilDatPos--;
        ilActPos = 0;
        StrgPutStrg(argSpoolData[2].Value,&ilActPos,pcpRoute,0,-1,pcgEvtFldSep);
        StrgPutStrg(argSpoolData[2].Value,&ilActPos,pcpCmd,0,-1,pcgEvtFldSep);
        StrgPutStrg(argSpoolData[2].Value,&ilActPos,pcpTbl,0,-1,pcgEvtFldSep);
        StrgPutStrg(argSpoolData[2].Value,&ilActPos,argSpoolData[0].Value,0,ilSelPos,pcgEvtFldSep);
        StrgPutStrg(argSpoolData[2].Value,&ilActPos,pcpFld,0,-1,pcgEvtFldSep);
        StrgPutStrg(argSpoolData[2].Value,&ilActPos,argSpoolData[1].Value,0,ilDatPos,pcgEvtFldSep);
        argSpoolData[2].Value[ilActPos] = 0x00;
        CT_InsertTextLine(pcgEvtSpool,argSpoolData[2].Value);
      } /* end if */
      else
      {
        dbg(TRACE,"ERROR: ALLOC %d BYTES FOR EVENT LINE",ilActLen);
        argSpoolData[2].AllocatedSize = 0;
      } /* end else */
    } /* end if */
    else
    {
      dbg(TRACE,"ERROR: ALLOC %d+%d BYTES FOR EVENT SPOOLER",ilSelLen,ilDatLen);
      if (argSpoolData[0].Value == NULL)
      {
        argSpoolData[0].AllocatedSize = 0;
      }
      if (argSpoolData[1].Value == NULL)
      {
        argSpoolData[1].AllocatedSize = 0;
      }
    } /* end else */

  }

  return ilRC;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
static int ToolsInitEventSpooler(void)
{
  int ilRC = RC_SUCCESS;
  int i = 0;
  pcgEvtFldSep[0] = 15;
  pcgEvtFldSep[1] = 0x00;
  strcpy(pcgEvtDestName, "EventSpooler");
  strcpy(pcgEvtRecvName, "EventSpooler");
  strcpy(pcgEvtTwStart, "EventSpooler");
  strcpy(pcgEvtTwEnd, "EventSpooler");
  CT_CreateArray(pcgEvtSpool);
  CT_SetDataPoolAllocSize(pcgEvtSpool, 1000);
  CT_SetFieldSeparator(pcgEvtSpool, pcgEvtFldSep[0]);
  CT_ResetContent(pcgEvtSpool);
  for (i=0; i <= MAX_CT_SPOOL; i++)
  {
    CT_InitStringDescriptor(&argSpoolField[i]);
    CT_InitStringDescriptor(&argSpoolData[i]);
  }
  dbg(TRACE,"CT EVENT SPOOLER <%s> INITIALIZED",pcgEvtSpool);
  igInitSpooler = TRUE;
  return ilRC;
} /* end ToolsInitEventSpooler */

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
int ToolsReleaseEventSpooler(int ipSendEvent, int ipPrio)
{
  int ilRC = RC_SUCCESS;
  int ilOutQue = 0;
  int ilOutCnt = 0;
  int ilOutPrio = 0;
  long llBufferLines = 0;
  long llLineNo = 0;
  long llColNo = 0;
  char *pclQueBgn = NULL;
  char *pclQueEnd = NULL;
  char *pclPriBgn = NULL;
  char *pclPriEnd = NULL;
  if (ipSendEvent == TRUE)
  {
    llBufferLines =  CT_GetLineCount(pcgEvtSpool);
    if (llBufferLines > 0)
    {
      dbg(TRACE,"RELEASING %d LINES OF EVENT SPOOLER",llBufferLines);
      ilOutCnt = 0;
      for (llLineNo = 0; llLineNo < llBufferLines; llLineNo++)
      {
        for (llColNo=0; llColNo <= 5; llColNo++)
        {
          CT_GetColumnValue(pcgEvtSpool, llLineNo, llColNo, &argSpoolField[llColNo]);
          dbg(DEBUG,"<%s>",argSpoolField[llColNo].Value);
        }
        pclQueBgn = argSpoolField[0].Value;
        pclQueEnd = pclQueBgn;
        pclPriBgn = strstr(pclQueBgn, "|");
        if (pclPriBgn != NULL)
        {
          *pclPriBgn = '\0';
          pclPriBgn++;
        }
        while (pclQueEnd != NULL)
        {
          pclQueEnd = strstr(pclQueBgn, ",");
          if (pclQueEnd != NULL)
          {
            *pclQueEnd = '\0';
          }
          if (pclPriBgn != NULL)
          {
            pclPriEnd = strstr(pclPriBgn, ",");
            if (pclPriEnd != NULL)
            {
              *pclPriEnd = '\0';
            }
            ilOutPrio = atoi(pclPriBgn);
          }
          ilOutQue = atoi(pclQueBgn);
          if (ilOutQue > 0)
          {
            if (ilOutPrio <= 0)
            {
              ilOutPrio = ipPrio;
            }
            SendCedaEvent(ilOutQue,0,pcgEvtDestName,pcgEvtRecvName,pcgEvtTwStart,pcgEvtTwEnd,
                    argSpoolField[1].Value,argSpoolField[2].Value,
                    argSpoolField[3].Value,argSpoolField[4].Value,argSpoolField[5].Value,
                    "",ilOutPrio,RC_SUCCESS);
            ilOutCnt++;
          }
          if (pclQueEnd != NULL)
          {
            pclQueBgn = pclQueEnd + 1;
          }
          if (pclPriEnd != NULL)
          {
            pclPriBgn = pclPriEnd + 1;
          }
        }
      }
      dbg(TRACE,"RELEASED: %d EVENTS",ilOutCnt);
    }
  }
  CT_ResetContent(pcgEvtSpool);
  return ilRC;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
long ToolsListAddValue(LIST_DESC *rpListDesc, char *pcpValue, long lpLen)
{
  long llValLen = 0;
  long llCurLen = 0;
  long llNewLen = 0;
  long llMemLen = 0;
  long llAddLen = 0;
  llCurLen = rpListDesc->UsedSize;
  if (rpListDesc->StepSize > 0)
  {
    llValLen = lpLen;
    if (llValLen < 0)
    {
      llValLen = strlen(pcpValue);
    }
    llNewLen = llCurLen + llValLen + 1;
    llAddLen = strlen(rpListDesc->ValuePref) + strlen(rpListDesc->ValueSuff);
    llNewLen += llAddLen;
    llNewLen += strlen(rpListDesc->ValueSepa);
    if (llNewLen > rpListDesc->AllocSize)
    {
      llMemLen = rpListDesc->AllocSize + rpListDesc->StepSize;
      if (llNewLen > llMemLen)
      {
        llMemLen = llNewLen;
      }
      rpListDesc->ValueList = realloc(rpListDesc->ValueList, llMemLen);
      rpListDesc->AllocSize = llMemLen;
    }
    llValLen--;
    if (llAddLen == 0)
    {
      StrgPutStrg(rpListDesc->ValueList,&llCurLen,pcpValue,0,llValLen,rpListDesc->ValueSepa);
    }
    else
    {
      StrgPutStrg(rpListDesc->ValueList,&llCurLen,rpListDesc->ValuePref,0,-1,"\0");
      StrgPutStrg(rpListDesc->ValueList,&llCurLen,pcpValue,0,llValLen,rpListDesc->ValueSuff);
      StrgPutStrg(rpListDesc->ValueList,&llCurLen,rpListDesc->ValueSepa,0,-1,"\0");
    }
    rpListDesc->ValueList[llCurLen] = 0x00;
    rpListDesc->UsedSize = llCurLen;
    rpListDesc->ValueCount++;
  }
  return llCurLen; 
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
long ToolsListAddValuePlus(LIST_DESC *rpListDesc, 
              char *pcpValue, long lpLen, char *pcpSep, char *pcpValue2, long lpLen2)
{
  long llValLen = 0;
  long llValLen2 = 0;
  long llCurLen = 0;
  long llNewLen = 0;
  long llMemLen = 0;
  long llAddLen = 0;
  llCurLen = rpListDesc->UsedSize;
  if (rpListDesc->StepSize > 0)
  {
    llValLen = lpLen;
    if (llValLen < 0)
    {
      llValLen = strlen(pcpValue);
    }
    llValLen2 = lpLen2;
    if (llValLen2 < 0)
    {
      llValLen2 = strlen(pcpValue2);
    }
    llNewLen = llCurLen + llValLen + 1 + llValLen2 + 1 + strlen(pcpSep);
    llAddLen = strlen(rpListDesc->ValuePref) + strlen(rpListDesc->ValueSuff);
    llNewLen += llAddLen;
    llNewLen += strlen(rpListDesc->ValueSepa);
    if (llNewLen > rpListDesc->AllocSize)
    {
      llMemLen = rpListDesc->AllocSize + rpListDesc->StepSize;
      if (llNewLen > llMemLen)
      {
        llMemLen = llNewLen;
      }
      rpListDesc->ValueList = realloc(rpListDesc->ValueList, llMemLen);
      rpListDesc->AllocSize = llMemLen;
    }
    llValLen--;
    llValLen2--;
    if (llAddLen == 0)
    {
      StrgPutStrg(rpListDesc->ValueList,&llCurLen,pcpValue,0,llValLen,pcpSep);
      StrgPutStrg(rpListDesc->ValueList,&llCurLen,pcpValue2,0,llValLen2,rpListDesc->ValueSepa);
    }
    else
    {
      StrgPutStrg(rpListDesc->ValueList,&llCurLen,rpListDesc->ValuePref,0,-1,"\0");
      StrgPutStrg(rpListDesc->ValueList,&llCurLen,pcpValue,0,llValLen,rpListDesc->ValueSuff);
      StrgPutStrg(rpListDesc->ValueList,&llCurLen,rpListDesc->ValueSepa,0,-1,"\0");
    }
    rpListDesc->ValueList[llCurLen] = 0x00;
    rpListDesc->UsedSize = llCurLen;
    rpListDesc->ValueCount++;
  }
  return llCurLen; 
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
long ToolsListAddKeyItem(LIST_DESC *rpListDesc, 
                         char *pcpPrefix, char *pcpName, char *pcpSuffix, char *pcpValue, 
                         char *pcpEnd, long lpLen)
{
  long llCurLen = 0;
  long llValLen = 0;
  long llAddLen = 0;
  long llNewLen = 0;
  long llMemLen = 0;
  llCurLen = rpListDesc->UsedSize;
  if (rpListDesc->StepSize > 0)
  {
    llValLen = lpLen;
    if (llValLen < 0)
    {
      llValLen = strlen(pcpValue);
      llAddLen = (strlen(pcpPrefix) * 2);
      llAddLen += (strlen(pcpName) * 2);
      llAddLen += (strlen(pcpSuffix) * 2);
      llAddLen += strlen(pcpEnd);
    }
    llNewLen = llCurLen + llValLen + 1;
    llNewLen += llAddLen;
    llNewLen += strlen(rpListDesc->ValueSepa);
    if (llNewLen > rpListDesc->AllocSize)
    {
      llMemLen = rpListDesc->AllocSize + rpListDesc->StepSize;
      if (llNewLen > llMemLen)
      {
        llMemLen = llNewLen;
      }
      rpListDesc->ValueList = realloc(rpListDesc->ValueList, llMemLen);
      rpListDesc->AllocSize = llMemLen;
    }
    llValLen--;
    if (pcpEnd[0] != '\0')
    {
      StrgPutStrg(rpListDesc->ValueList,&llCurLen,pcpPrefix,0,-1,"\0");
      StrgPutStrg(rpListDesc->ValueList,&llCurLen,pcpName,0,-1,pcpSuffix);
      StrgPutStrg(rpListDesc->ValueList,&llCurLen,pcpValue,0,llValLen,pcpPrefix);
      StrgPutStrg(rpListDesc->ValueList,&llCurLen,pcpEnd,0,-1,pcpName);
      StrgPutStrg(rpListDesc->ValueList,&llCurLen,pcpSuffix,0,-1,rpListDesc->ValueSepa);
    }
    else
    {
      StrgPutStrg(rpListDesc->ValueList,&llCurLen,pcpPrefix,0,-1,"\0");
      StrgPutStrg(rpListDesc->ValueList,&llCurLen,pcpName,0,-1,pcpSuffix);
      StrgPutStrg(rpListDesc->ValueList,&llCurLen,pcpValue,0,llValLen,rpListDesc->ValueSepa);
    }
    rpListDesc->ValueList[llCurLen] = 0x00;
    rpListDesc->UsedSize = llCurLen;
  }
  return llCurLen; 
}

/******************************************************************************/
/******************************************************************************/
void ToolsListCreate(LIST_DESC **prpListDesc)
{
  if (prpListDesc != NULL)
  {
    if (*prpListDesc == NULL)
    {
      *prpListDesc = (LIST_DESC *)malloc(sizeof(LIST_DESC));
      ToolsListInit(*prpListDesc);
    }
  }
  return;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
void ToolsListInit(LIST_DESC *rpListDesc)
{
  strcpy(rpListDesc->ListName, "");
  strcpy(rpListDesc->ListCode, "");
  strcpy(rpListDesc->ListType, "");
  rpListDesc->ListHead = (char *)malloc(4);
  strcpy(rpListDesc->ListHead, "");
  rpListDesc->StepSize = 1024;
  rpListDesc->UsedSize = 0;
  rpListDesc->ValueCount = 0;
  strcpy(rpListDesc->ValuePref, "");
  strcpy(rpListDesc->ValueSuff, "");
  strcpy(rpListDesc->ValueSepa, "\n");
  rpListDesc->ValueList = (char *)malloc(4);
  strcpy(rpListDesc->ValueList, "");
  rpListDesc->AllocSize = 4;
  return;
}

void ToolsListSetStepSize(LIST_DESC *rpListDesc, long lpStepSize)
{
  rpListDesc->StepSize = lpStepSize;
  return;
}
/******************************************************************************/
/*                                                                            */
/******************************************************************************/
void ToolsListSetInfo(LIST_DESC *rpListDesc, char *pcpName, char *pcpCode,
                      char *pcpType, char *pcpHead, char *pcpPrefix, char *pcpSuffix,
                      char *pcpValueSepa, long lpStepSize)
{
  int ilLen = 0;
  strcpy(rpListDesc->ListName, pcpName);
  strcpy(rpListDesc->ListCode, pcpCode);
  strcpy(rpListDesc->ListType, pcpType);
  ilLen = strlen(pcpHead) + 1;
  rpListDesc->ListHead = realloc(rpListDesc->ListHead, ilLen);
  strcpy(rpListDesc->ListHead, pcpHead);
  strcpy(rpListDesc->ValuePref, pcpPrefix);
  strcpy(rpListDesc->ValueSuff, pcpSuffix);
  strcpy(rpListDesc->ValueSepa, pcpValueSepa);
  rpListDesc->StepSize = lpStepSize;
  return;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
void ToolsListFree(LIST_DESC *rpListDesc)
{
  if (rpListDesc->ListHead != NULL)
  {
    free(rpListDesc->ListHead);
  }
  rpListDesc->ListHead = NULL;
  rpListDesc->AllocSize = 0;
  rpListDesc->UsedSize = 0;
  rpListDesc->ValueCount = 0;
  if (rpListDesc->ValueList != NULL)
  {
    free(rpListDesc->ValueList);
  }
  rpListDesc->ValueList = NULL;
  return;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
void ToolsEventInfoInit(EVT_INFO *prpEvtInfo)
{
  strcpy(prpEvtInfo->OutQue,"");
  strcpy(prpEvtInfo->OutPri,"");
  strcpy(prpEvtInfo->EvtCmd,"");
  strcpy(prpEvtInfo->EvtTbl,"");
  strcpy(prpEvtInfo->EvtWks,"");
  strcpy(prpEvtInfo->EvtUsr,"");
  strcpy(prpEvtInfo->EvtTws,"");
  strcpy(prpEvtInfo->EvtTwe,"");
  CT_InitStringDescriptor(&(prpEvtInfo->EvtSel));
  CT_InitStringDescriptor(&(prpEvtInfo->EvtFld));
  CT_InitStringDescriptor(&(prpEvtInfo->EvtDat));
  return;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
void ToolsEventSetInfo(EVT_INFO *prpEvtInfo,
                       char *pcpQue, char *pcpPri, char *pcpCmd, char *pcpTbl,
                       char *pcpWks, char *pcpUsr, char *pcpTws, char *pcpTwe,
                       char *pcpSel, char *pcpFld, char *pcpDat)
{
  int ilLen = 0;
  STR_DESC *prlStrDesc;
  strcpy(prpEvtInfo->OutQue,pcpQue);
  strcpy(prpEvtInfo->OutPri,pcpPri);
  strcpy(prpEvtInfo->EvtCmd,pcpCmd);
  strcpy(prpEvtInfo->EvtTbl,pcpTbl);
  strcpy(prpEvtInfo->EvtWks,pcpWks);
  strcpy(prpEvtInfo->EvtUsr,pcpUsr);
  strcpy(prpEvtInfo->EvtTws,pcpTws);
  strcpy(prpEvtInfo->EvtTwe,pcpTwe);

  ilLen = strlen(pcpSel) + 1;
  prlStrDesc = &(prpEvtInfo->EvtSel);
  if (ilLen > prlStrDesc->AllocatedSize)
  {
    prlStrDesc->Value = (char *)realloc(prlStrDesc->Value,ilLen);
    prlStrDesc->AllocatedSize = ilLen;
  }
  strcpy(prlStrDesc->Value,pcpSel);
  
  ilLen = strlen(pcpFld) + 1;
  prlStrDesc = &(prpEvtInfo->EvtFld);
  if (ilLen > prlStrDesc->AllocatedSize)
  {
    prlStrDesc->Value = (char *)realloc(prlStrDesc->Value,ilLen);
    prlStrDesc->AllocatedSize = ilLen;
  }
  strcpy(prlStrDesc->Value,pcpFld);
  
  ilLen = strlen(pcpDat) + 1;
  prlStrDesc = &(prpEvtInfo->EvtDat);
  if (ilLen > prlStrDesc->AllocatedSize)
  {
    prlStrDesc->Value = (char *)realloc(prlStrDesc->Value,ilLen);
    prlStrDesc->AllocatedSize = ilLen;
  }
  strcpy(prlStrDesc->Value,pcpDat);
  
  return;
}





