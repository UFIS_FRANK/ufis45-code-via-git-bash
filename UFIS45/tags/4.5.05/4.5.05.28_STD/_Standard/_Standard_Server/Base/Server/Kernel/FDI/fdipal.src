#ifndef _DEF_mks_version_fdipal
  #define _DEF_mks_version_fdipal
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_fdipal[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/FDI/fdipal.src 1.10 2011/10/24 17:37:36SGT dka Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :  AKL                                                      */
/* Date           :                                                           */
/* Description    :  INCLUDE PART FROM FDIHDL                                 */
/*                   Request Information Message                              */
/*                                                                            */
/* Update history :                                                           */
/* 20111024 v.1.10   Allow fixed DPXTAB.PRID from configuration               */
/*                   in sendPALDPXRes().                                      */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* FDIPAL handle functions                                                    */
/* -----------------------                                                    */
/* static int HandlePAL(char *pcpData)                                        */
/* static int SendPALDPXRes()                                                 */

static int PALInterpretation(char *pcpData);

static int HandlePAL(char *pcpData)
{
  int ilRC;
  int ilLine;
  char pclFunc[]="HandlePAL:";
  char *pclData;
  char pclResult[100];
  char pclResult2[100];
  char *pclTmpPtr;
  T_TLXINFOCMD *prlInfoCmd;
  T_TLXRESULT rlTlxResult;
  int ilI;
  int ilCount;

  prlInfoCmd = &prgInfoCmd[0][0];
  pclData = pcpData;
  dbg(DEBUG,"%s Extract Flight Line",pclFunc); 
  /* Go back one line */
  ilI = rgTlxInfo.TxtStart - 2;
  while (ilI > 0 && pclData[ilI] != '\n')
  {
     ilI--;
  }
  if (ilI > 0)
  {
     ilI++;
     CopyLine(pclResult,&pclData[ilI]);
     MakeTokenList(pclResult2,pclResult," ",',');
     ilCount = GetNoOfElements(pclResult2,',');
     if (ilCount > 1)
     {
        rgTlxInfo.TxtStart = ilI + 4;
        while (pclData[rgTlxInfo.TxtStart] == ' ')
        {
           rgTlxInfo.TxtStart++;
        }
     }
  }
  ilLine = 0;
  ilRC = 0;
  /* i < 4 => flight line has to be within the following 4 lines   */
  /* and char. length > 1 */
  while (ilRC  < 2 && ilLine< 4)
  {
     /* Search for Flight line  */
     ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
     ilLine++;
  }
  if (ilRC > 0)
  {
     if (strcmp(pclResult,"=TEXT") == 0)
     {
        ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
        ilLine++;
        if (strcmp(pclResult,"COR") == 0)
        {
           ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
           ilLine++;
        }
        if (strcmp(pclResult,"QUOTE") == 0)
        {
           ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
           ilLine++;
        }
        if (strcmp(pclResult,"PAL") == 0 || strcmp(pclResult,"CAL") == 0)
        {
           ilRC = GetDataItem(pclResult,&(pclData[rgTlxInfo.TxtStart]),ilLine,'\n',"","  ");
           ilLine++;
        }
     }
     dbg(DEBUG,"%s Found Flight Line <%s>",pclFunc,pclResult); 
     ConvertFlightLine(pclResult);
     GetFlightDate(pclResult,1,2);
     if (strlen(pcgFltDate) == 0)
        GetFlightDate(pclResult,2,1);
     MakeTokenList(pclResult2,pclResult," ",',');
     MakeTokenList(pclResult,pclResult2,"/",',');
     MakeTokenList(pclResult2,pclResult,".",',');

     pclTmpPtr = pclResult2;
     /* Get Airline Code */
     ilRC = GetAlc(&pclTmpPtr,prgMasterTlx);
     /* Get Flight Number */
     ilRC = GetFltn(&pclTmpPtr,prgMasterTlx);
     /* Get Destination */
     GetDataItem(pclResult,pclResult2,3,',',"","\0\0");
     if (strlen(pclResult) == 3)
     {
        dbg(DEBUG,"%s Found Origin <%s>",pclFunc,pclResult); 
        memset(&rlTlxResult,0x00,TLXRESULT);
        strcpy(rlTlxResult.DValue,pclResult);
        strcpy(rlTlxResult.FValue,pclResult);
        strcpy(rlTlxResult.DName,"Orig");
        CCSArrayAddUnsort(prgMasterTlx,&rlTlxResult,TLXRESULT);
     }
     else
     {
        GetDataItem(pclResult,pclResult2,4,',',"","\0\0");
        if (strlen(pclResult) == 3)
        {
           dbg(DEBUG,"%s Found Origin <%s>",pclFunc,pclResult); 
           memset(&rlTlxResult,0x00,TLXRESULT);
           strcpy(rlTlxResult.DValue,pclResult);
           strcpy(rlTlxResult.FValue,pclResult);
           strcpy(rlTlxResult.DName,"Orig");
           CCSArrayAddUnsort(prgMasterTlx,&rlTlxResult,TLXRESULT);
        }
     }
  } 

  if (igPALInterpretation == TRUE)
     ilRC = PALInterpretation(pcpData);

  ilRC = SendTlxRes(FDI_PAL);

  return ilRC;
} /* end of HandlePAL  */
 

static int PALInterpretation(char *pcpData)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[]="PALInterpretation:";
  char pclSearch[128];
  char *pclTlxDat;
  int ilLine;
  char *pclTmpPtr;
  char *pclTmpPtr2;
  char pclLine[512];
  char pclNextLine[512];
  int ilNoPRMType;
  char pclPRMType[16];
  int ilI;
  int ilFound;
  int ilCount;
  char pclResult[128];
  char pclItem[128];
  char pclItem1[128];
  char pclItem2[128];
  char pclClass[16];
  char pclConFlight[128];
  char pclConFlightDate[128];
  char pclConFlightClass[128];
  char pclConFlightDest[128];
  char pclSaveFlightDate[16];
  T_TLXRESULT *prlTlxPtr = NULL;
  char clAdid;
  int ilValidLine;
  char pclFkt[4];
  char pclTlxTtyp[16];

  igCurPalValues = 0;
  igTrustTlxDate = TRUE;

  strcpy(pclTlxTtyp,"");
  prlTlxPtr = CCSArrayGetData(prgMasterTlx,"TlxTyp");
  if (prlTlxPtr != NULL )
     strcpy(pclTlxTtyp,prlTlxPtr->FValue);
  dbg(DEBUG,"%s Telex Type is <%s>",pclFunc,pclTlxTtyp);
  strcpy(pclItem,"XXX");
  prlTlxPtr = CCSArrayGetData(prgMasterTlx,"Orig");
  if (prlTlxPtr != NULL )
     strcpy(pclItem,prlTlxPtr->FValue);
  if (strcmp(pcgHomeAP,pclItem) != 0)
  {
     sprintf(pclSearch,"\n-%s",pcgHomeAP);
     pclTlxDat = strstr(pcpData,pclSearch);
     if (pclTlxDat == NULL)
     {
        dbg(TRACE,"%s Home Airport <%s> not found in telex message",pclFunc,pclSearch);
        return ilRC;
     }
     clAdid = 'A';
  }
  else
  {
     pclTlxDat = strstr(pcpData,"\n-");
     if (pclTlxDat == NULL)
        return ilRC;
     clAdid = 'D';
  }

  ilValidLine = TRUE;
  strcpy(pclFkt," ");
  ilNoPRMType = GetNoOfElements(pcgPRMKeyWords,',');
  pclTlxDat++;
  ilLine = 1;
  pclTmpPtr = GetLine(pclTlxDat,ilLine);
  while (pclTmpPtr != NULL)
  {
     strcpy(&prgPalValues[igCurPalValues].pclPalName[0]," ");
     strcpy(&prgPalValues[igCurPalValues].pclPalType[0]," ");
     strcpy(&prgPalValues[igCurPalValues].pclPalSeat[0]," ");
     strcpy(&prgPalValues[igCurPalValues].pclPalClass[0]," ");
     strcpy(&prgPalValues[igCurPalValues].pclPalConFlight[0]," ");
     strcpy(&prgPalValues[igCurPalValues].pclPalConFlightDate[0]," ");
     strcpy(&prgPalValues[igCurPalValues].pclPalConFlightDest[0]," ");
     strcpy(&prgPalValues[igCurPalValues].pclPalConFlightClass[0]," ");
     strcpy(&prgPalValues[igCurPalValues].pclPalConFlightAdid[0]," ");
     strcpy(&prgPalValues[igCurPalValues].pclPalFkt[0]," ");
     strcpy(&prgPalValues[igCurPalValues].pclPalTlxTtyp[0]," ");
     strcpy(&prgPalValues[igCurPalValues].pclPalConHdlAgt[0]," ");
     CopyLine(pclLine,pclTmpPtr);
     if (ilValidLine == TRUE)
     {
        pclTmpPtr = GetLine(pclTlxDat,ilLine+1);
        while (pclTmpPtr != NULL)
        {
           if (*pclTmpPtr != '-' && *pclTmpPtr != '1' && strncmp(pclTmpPtr,"END",3) != 0 &&
               strncmp(pclTmpPtr,"DEL",3) != 0 && strncmp(pclTmpPtr,"ADD",3) != 0 &&
               strncmp(pclTmpPtr,"CHG",3) != 0)
           {
              CopyLine(pclNextLine,pclTmpPtr);
              strcat(pclLine," ");
              strcat(pclLine,pclNextLine);
              ilLine++;
              pclTmpPtr = GetLine(pclTlxDat,ilLine+1);
           }
           else
              pclTmpPtr = NULL;
        }
        dbg(DEBUG,"%s Line = <%s>",pclFunc,pclLine);
        if (*pclLine == '-')
        {
           strncpy(pclClass,&pclLine[5],1);
           pclClass[1] = '\0';
           dbg(DEBUG,"%s Destination Info ==> Ignore Line",pclFunc);
        }
        else if (strncmp(pclLine,"DEL",3) == 0)
        {
           strcpy(pclFkt,"D");
           dbg(DEBUG,"%s Delete Info ==> Set Function to '%s'",pclFunc,pclFkt);
        }
        else if (strncmp(pclLine,"ADD",3) == 0)
        {
           strcpy(pclFkt,"I");
           dbg(DEBUG,"%s Insert Info ==> Set Function to '%s'",pclFunc,pclFkt);
        }
        else if (strncmp(pclLine,"CHG",3) == 0)
        {
           strcpy(pclFkt,"U");
           dbg(DEBUG,"%s Update Info ==> Set Function to '%s'",pclFunc,pclFkt);
        }
        else if (*pclLine != '1')
           dbg(DEBUG,"%s Invalid Line ==> Ignore Line",pclFunc);
        else
        {
           pclTmpPtr = strstr(pclLine,".R/");
           if (pclTmpPtr != NULL)
           {
              GetDataItem(pclItem,pclTmpPtr,1,' ',"","\0\0");
              ilFound = FALSE;
              for (ilI = 0; ilI < ilNoPRMType && ilFound == FALSE; ilI++)
              {
                 GetDataItem(pclPRMType,pcgPRMKeyWords,ilI+1,',',"","\0\0");
                 if (strcmp(&pclItem[3],pclPRMType) == 0)
                    ilFound = TRUE;
              }
              if (ilFound == FALSE)
                 dbg(DEBUG,"%s None of the Key Words <%s> found ==> Ignore Line",pclFunc,pcgPRMKeyWords);
              else
              {
                 strcpy(&prgPalValues[igCurPalValues].pclPalType[0],pclPRMType);
                 GetDataItem(pclItem,&pclLine[1],1,' ',"","\0\0");
                 strcpy(&prgPalValues[igCurPalValues].pclPalName[0],pclItem);
                 strcpy(&prgPalValues[igCurPalValues].pclPalClass[0],pclClass);
                 if (clAdid == 'A')
                    pclTmpPtr = strstr(pclLine,".O/");
                 else
                    pclTmpPtr = strstr(pclLine,".I/");
                 if (pclTmpPtr != NULL)
                 {
                    GetDataItem(pclItem,pclTmpPtr,1,' ',"","\0\0");
                    strcpy(pclConFlight,&pclItem[3]);
                    ilI = 3;
                    while (isdigit(pclConFlight[ilI]) && ilI < strlen(pclConFlight))
                       ilI++;
                    if (!isdigit(pclConFlight[ilI+1]))
                       ilI++;
                    strncpy(pclConFlightClass,&pclConFlight[ilI],1);
                    pclConFlightClass[1] = '\0';
                    if (strlen(pclConFlight) >= ilI+1)
                       strcpy(pclConFlightDate,&pclConFlight[ilI+1]);
                    else
                       strcpy(pclConFlightDate,"");
                    pclConFlight[ilI] = '\0';
                    if (strlen(pclConFlightDate) >= 2)
                       strcpy(pclConFlightDest,&pclConFlightDate[2]);
                    else
                       strcpy(pclConFlightDest," ");
                    pclConFlightDest[3] = '\0';
                    pclConFlightDate[2] = '\0';
                    if (strlen(pclConFlightDate) > 0)
                    {
                       strcpy(pclSaveFlightDate,pcgFltDate);
                       GetFlightDate(pclConFlightDate,1,1);
                       strcpy(&prgPalValues[igCurPalValues].pclPalConFlightDate[0],pcgFltDate);
                       strcpy(pcgFltDate,pclSaveFlightDate);
                    }
                    else
                       strcpy(&prgPalValues[igCurPalValues].pclPalConFlightDate[0],pcgFltDate);
                    ConvertFlightNumber(pclConFlight,&prgPalValues[igCurPalValues].pclPalConFlight[0]);
                    strcpy(&prgPalValues[igCurPalValues].pclPalConFlightDest[0],pclConFlightDest);
                    strcpy(&prgPalValues[igCurPalValues].pclPalConFlightClass[0],pclConFlightClass);
                    if (clAdid == 'A')
                       strcpy(&prgPalValues[igCurPalValues].pclPalConFlightAdid[0],"D");
                    else
                       strcpy(&prgPalValues[igCurPalValues].pclPalConFlightAdid[0],"A");
                 }
                 strcpy(&prgPalValues[igCurPalValues].pclPalFkt[0],pclFkt);
                 strcpy(&prgPalValues[igCurPalValues].pclPalTlxTtyp[0],pclTlxTtyp);
                 igCurPalValues++;
              }
           }
        }
     }
     ilLine++;
     pclTmpPtr = GetLine(pclTlxDat,ilLine);
     if (pclTmpPtr != NULL &&
         (strncmp(pclTmpPtr,"ENDPAL",6) == 0 || strncmp(pclTmpPtr,"ENDCAL",6) == 0 ||
          strncmp(pclTmpPtr,"ENDPART",7) == 0))
        pclTmpPtr = NULL;
     if (pclTmpPtr != NULL && clAdid == 'A' && *pclTmpPtr == '-')
     {
        if (strncmp(&pclTmpPtr[1],pcgHomeAP,3) == 0)
           ilValidLine = TRUE;
        else
           ilValidLine = FALSE;
     }
  }
  for (ilI = 0; ilI < igCurPalValues; ilI++)
  {
     dbg(DEBUG,"%s %d. ----------------------------------",pclFunc,ilI+1);
     dbg(DEBUG,"%s Name      = %s",pclFunc,&prgPalValues[ilI].pclPalName[0]);
     dbg(DEBUG,"%s Type      = %s",pclFunc,&prgPalValues[ilI].pclPalType[0]);
     dbg(DEBUG,"%s Class     = %s",pclFunc,&prgPalValues[ilI].pclPalClass[0]);
     dbg(DEBUG,"%s ConFlight = %s",pclFunc,&prgPalValues[ilI].pclPalConFlight[0]);
     dbg(DEBUG,"%s ConFDate  = %s",pclFunc,&prgPalValues[ilI].pclPalConFlightDate[0]);
     dbg(DEBUG,"%s ConFDest  = %s",pclFunc,&prgPalValues[ilI].pclPalConFlightDest[0]);
     dbg(DEBUG,"%s ConFClass = %s",pclFunc,&prgPalValues[ilI].pclPalConFlightClass[0]);
     dbg(DEBUG,"%s ConFAdid  = %s",pclFunc,&prgPalValues[ilI].pclPalConFlightAdid[0]);
     dbg(DEBUG,"%s TTyp      = %s",pclFunc,&prgPalValues[ilI].pclPalTlxTtyp[0]);
     dbg(DEBUG,"%s Func      = %s",pclFunc,&prgPalValues[ilI].pclPalFkt[0]);
  }
  dbg(DEBUG,"%s -------------------------------------",pclFunc,ilI+1);

  return ilRC;
} /* end of PALInterpretation  */


static int SendPALDPXRes()
{
  int ilRC = RC_SUCCESS;
  char pclFunc[]="SendPALDPXRes:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[1024];
  char pclSelection[1024];
  char pclDataBuf[1024];
  int ilI;
  char pclFieldList[1024];
  char pclValueList[1024];
  char pclCurTime[16];
  char pclDpxUrno[16];
  char pclDpxFlnu[16];
  char pclDpxTflu[16];
  char pclDpxFlnuX[16];
  char pclDpxTfluX[16];
  char pclUrnoList[1024];
  int ilNoEle;
  char pclCurUrno[16];
  int ilCount;
  char pclAltUrno[16];
  char pclFilt[16];
  char pclAlc[16];
  char pclTime[16];
  char pclAlmFields[128];
  char pclAlmData[1024];

  if (igPALInterpretation == FALSE)
     return ilRC;
  if (strlen(pcgAftUrno) <= 1)
     return ilRC;
  if (igCurPalValues == 0)
     return ilRC;

  /*
      v.1.10 - overwrite with configured fixed value if so defined.  
  */
  if (strcmp (pcgPalPridFixedValue, "undefined") != 0)
  {
    strcpy (pcgTlxTimeForPal,pcgPalPridFixedValue);
    dbg(TRACE,"%s Set DPXTAB.PRID to <%s>",pclFunc,pcgPalPridFixedValue);
  }

  strcpy(pclFilt," ");
  if (strlen(pcgHandlingTypeForPRM) > 0 && strlen(pcgAlc3ForPRM) > 0)
  {
     ilCount = 1;
     ilRC = syslibSearchDbData("ALTTAB","ALC3",pcgAlc3ForPRM,"URNO",pclAltUrno,&ilCount,"\n");
     TrimRight(pclAltUrno);
     sprintf(pclSqlBuf,"SELECT HSNA FROM HAITAB WHERE TASK = '%s' AND ALTU = %s",
             pcgHandlingTypeForPRM,pclAltUrno);
     dbg(DEBUG,"%s <%s>",pclFunc,pclSqlBuf);
     slCursor = 0;
     slFkt = START;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclFilt);
     close_my_cursor(&slCursor);
     if (ilRCdb == DB_SUCCESS)
        TrimRight(pclFilt);
  }

  for (ilI = 0; ilI < igCurPalValues; ilI++)
  {
     if (prgPalValues[ilI].pclPalConFlight[0] != ' ')
     {
        strcpy(&prgPalValues[ilI].pclPalConUrno[0]," ");
        if (prgPalValues[ilI].pclPalConFlightAdid[0] == 'A')
        {
           sprintf(pclSqlBuf,"SELECT URNO,STOA FROM AFTTAB ");
           sprintf(pclSelection,"WHERE FLNO = '%s' AND STOD LIKE '%s%%' AND ORG3 = '%s'",
                   &prgPalValues[ilI].pclPalConFlight[0],
                   &prgPalValues[ilI].pclPalConFlightDate[0],
                   &prgPalValues[ilI].pclPalConFlightDest[0]);
           strcat(pclSqlBuf,pclSelection);
           dbg(DEBUG,"%s <%s>",pclFunc,pclSqlBuf);
           slCursor = 0;
           slFkt = START;
           ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
           close_my_cursor(&slCursor);
           if (ilRCdb != DB_SUCCESS)
           {
              sprintf(pclSqlBuf,"SELECT URNO,STOA FROM AFTTAB ");
              sprintf(pclSelection,"WHERE FLNO = '%s' AND STOA LIKE '%s%%' AND ORG3 = '%s'",
                      &prgPalValues[ilI].pclPalConFlight[0],
                      &prgPalValues[ilI].pclPalConFlightDate[0],
                      &prgPalValues[ilI].pclPalConFlightDest[0]);
              strcat(pclSqlBuf,pclSelection);
           }
        }
        else
        {
           sprintf(pclSqlBuf,"SELECT URNO,STOD FROM AFTTAB ");
           sprintf(pclSelection,"WHERE FLNO = '%s' AND STOD LIKE '%s%%' AND DES3 = '%s'",
                   &prgPalValues[ilI].pclPalConFlight[0],
                   &prgPalValues[ilI].pclPalConFlightDate[0],
                   &prgPalValues[ilI].pclPalConFlightDest[0]);
           strcat(pclSqlBuf,pclSelection);
        }
        dbg(DEBUG,"%s <%s>",pclFunc,pclSqlBuf);
        slCursor = 0;
        slFkt = START;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        close_my_cursor(&slCursor);
        if (ilRCdb == DB_SUCCESS)
        {
           BuildItemBuffer(pclDataBuf,"",2,",");
           get_real_item(&prgPalValues[ilI].pclPalConUrno[0],pclDataBuf,1);
           TrimRight(&prgPalValues[ilI].pclPalConUrno[0]);
           get_real_item(&prgPalValues[ilI].pclPalConFlightStof[0],pclDataBuf,2);
           TrimRight(&prgPalValues[ilI].pclPalConFlightStof[0]);
           dbg(DEBUG,"%s Found Flight with URNO,STOF = <%s>",pclFunc,pclDataBuf);
           strcpy(pclAlc,&prgPalValues[ilI].pclPalConFlight[0]);
           if (strlen(pcgHandlingTypeForPRM) > 0)
           {
              ilCount = 1;
              if (pclAlc[2] == ' ')
              {
                 pclAlc[2] = '\0';
                 ilRC = syslibSearchDbData("ALTTAB","ALC2",pclAlc,"URNO",pclAltUrno,&ilCount,"\n");
              }
              else
              {
                 pclAlc[3] = '\0';
                 ilRC = syslibSearchDbData("ALTTAB","ALC3",pclAlc,"URNO",pclAltUrno,&ilCount,"\n");
              }
              TrimRight(pclAltUrno);
              sprintf(pclSqlBuf,"SELECT HSNA FROM HAITAB WHERE TASK = '%s' AND ALTU = %s",
                      pcgHandlingTypeForPRM,pclAltUrno);
              dbg(DEBUG,"%s <%s>",pclFunc,pclSqlBuf);
              slCursor = 0;
              slFkt = START;
              ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,&prgPalValues[ilI].pclPalConHdlAgt[0]);
              close_my_cursor(&slCursor);
              if (ilRCdb == DB_SUCCESS)
                 TrimRight(&prgPalValues[ilI].pclPalConHdlAgt[0]);
           }
        }
        else
           strcpy(&prgPalValues[ilI].pclPalConUrno[0],"0");
     }
     else
        strcpy(&prgPalValues[ilI].pclPalConUrno[0],"0");
  }

  strcpy(pclUrnoList,"");
  TimeToStr(pclCurTime,time(NULL));
  for (ilI = 0; ilI < igCurPalValues; ilI++)
  {
     if (*pcgAftAdid == 'D' && strcmp(&prgPalValues[ilI].pclPalConUrno[0],"0") != 0 &&
         igCreate2DPXRecords == FALSE)
     {
        strcpy(pclDpxFlnuX,&prgPalValues[ilI].pclPalConUrno[0]);
        strcpy(pclDpxTfluX,pcgAftUrno);
     }
     else
     {
        strcpy(pclDpxFlnuX,pcgAftUrno);
        strcpy(pclDpxTfluX,&prgPalValues[ilI].pclPalConUrno[0]);
     }
     if (strcmp(&prgPalValues[ilI].pclPalTlxTtyp[0],"PAL") == 0 ||
         prgPalValues[ilI].pclPalFkt[0] == 'I')
     {
        sprintf(pclSqlBuf,"SELECT URNO FROM DPXTAB ");
        sprintf(pclSelection,"WHERE NAME = '%s' AND (FLNU = %s OR TFLU = %s)",
                &prgPalValues[ilI].pclPalName[0],pcgAftUrno,pcgAftUrno);
        strcat(pclSqlBuf,pclSelection);
        dbg(DEBUG,"%s <%s>",pclFunc,pclSqlBuf);
        slCursor = 0;
        slFkt = START;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDpxUrno);
        close_my_cursor(&slCursor);
        if (ilRCdb != DB_SUCCESS)
        {
           if (strlen(pcgHandlingTypeForPRM) > 0)
           {
              strcpy(pclFieldList,"NAME,PRMT,FLNU,TFLU,TLXU,SEAT,ABSR,FDAT,SATI,ABDT,FILT,CDAT,USEC,PRID");
              sprintf(pclValueList,"%s,%s,%s,%s,%s,%s,PAL,%s,%s,%s,%s,%s,FDIHDL,%s",
                      &prgPalValues[ilI].pclPalName[0],
                      &prgPalValues[ilI].pclPalType[0],
                      pclDpxFlnuX,
                      pclDpxTfluX,
                      pcgNextTlxUrno,
                      &prgPalValues[ilI].pclPalSeat[0],
                      pcgFlightSchedTime,
                      pcgFlightSchedTime,
                      pclCurTime,
                      pclFilt,
                      pclCurTime,
                      pcgTlxTimeForPal);
           }
           else
           {
              strcpy(pclFieldList,"NAME,PRMT,FLNU,TFLU,TLXU,SEAT,ABSR,FDAT,SATI,ABDT,CDAT,USEC,PRID");
              sprintf(pclValueList,"%s,%s,%s,%s,%s,%s,PAL,%s,%s,%s,%s,FDIHDL,%s",
                      &prgPalValues[ilI].pclPalName[0],
                      &prgPalValues[ilI].pclPalType[0],
                      pclDpxFlnuX,
                      pclDpxTfluX,
                      pcgNextTlxUrno,
                      &prgPalValues[ilI].pclPalSeat[0],
                      pcgFlightSchedTime,
                      pcgFlightSchedTime,
                      pclCurTime,
                      pclCurTime,
                      pcgTlxTimeForPal);
           }
           ilRC = SendCedaEvent(7150,0,pcgAftDestName,pcgAftRecvName,
                                pcgAftTwStart,pcgAftTwEnd,
                                "IRT","DPXTAB","",
                                pclFieldList,pclValueList,"",3,NETOUT_NO_ACK);
           dbg(DEBUG,"%s FieldList <%s>",pclFunc,pclFieldList);
           dbg(DEBUG,"%s ValueList <%s>",pclFunc,pclValueList);
           dbg(DEBUG,"%s Sent <IRT> for <DPXTAB>. Returned <%d>",pclFunc,ilRC);
           if (strcmp(&prgPalValues[ilI].pclPalConUrno[0],"0") != 0)
           {
              if (igCreate2DPXRecords == TRUE)
              {
                 if (strlen(pcgHandlingTypeForPRM) > 0)
                    sprintf(pclValueList,"%s,%s,%s,%s,%s, ,PAL,%s,%s,%s,%s,%s,FDIHDL,%s",
                            &prgPalValues[ilI].pclPalName[0],
                            &prgPalValues[ilI].pclPalType[0],
                            &prgPalValues[ilI].pclPalConUrno[0],
                            pcgAftUrno,
                            pcgNextTlxUrno,
                            &prgPalValues[ilI].pclPalConFlightStof[0],
                            &prgPalValues[ilI].pclPalConFlightStof[0],
                            pclCurTime,
                            &prgPalValues[ilI].pclPalConHdlAgt[0],
                            pclCurTime,
                            pcgTlxTimeForPal);
                 else
                    sprintf(pclValueList,"%s,%s,%s,%s,%s, ,PAL,%s,%s,%s,%s,FDIHDL,%s",
                            &prgPalValues[ilI].pclPalName[0],
                            &prgPalValues[ilI].pclPalType[0],
                            &prgPalValues[ilI].pclPalConUrno[0],
                            pcgAftUrno,
                            pcgNextTlxUrno,
                            &prgPalValues[ilI].pclPalConFlightStof[0],
                            &prgPalValues[ilI].pclPalConFlightStof[0],
                            pclCurTime,
                            pclCurTime,
                            pcgTlxTimeForPal);
                 ilRC = SendCedaEvent(7150,0,pcgAftDestName,pcgAftRecvName,
                                      pcgAftTwStart,pcgAftTwEnd,
                                      "IRT","DPXTAB","",
                                      pclFieldList,pclValueList,"",3,NETOUT_NO_ACK);
                 dbg(DEBUG,"%s FieldList <%s>",pclFunc,pclFieldList);
                 dbg(DEBUG,"%s ValueList <%s>",pclFunc,pclValueList);
                 dbg(DEBUG,"%s Sent <IRT> for <DPXTAB>. Returned <%d>",pclFunc,ilRC);
              }
              if (strstr(pclUrnoList,&prgPalValues[ilI].pclPalConUrno[0]) == NULL)
              {
                 strcat(pclUrnoList,",");
                 strcat(pclUrnoList,&prgPalValues[ilI].pclPalConUrno[0]);
              }
           }
        }
     }
     else
     {
        sprintf(pclSqlBuf,"SELECT URNO,FLNU,TFLU FROM DPXTAB ");
        sprintf(pclSelection,"WHERE NAME = '%s' AND (FLNU = %s OR TFLU = %s)",
                &prgPalValues[ilI].pclPalName[0],pcgAftUrno,pcgAftUrno);
        strcat(pclSqlBuf,pclSelection);
        dbg(DEBUG,"%s <%s>",pclFunc,pclSqlBuf);
        slCursor = 0;
        slFkt = START;
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        while (ilRCdb == DB_SUCCESS)
        {
           BuildItemBuffer(pclDataBuf,"",3,",");
           get_real_item(pclDpxUrno,pclDataBuf,1);
           TrimRight(pclDpxUrno);
           get_real_item(pclDpxFlnu,pclDataBuf,2);
           TrimRight(pclDpxFlnu);
           get_real_item(pclDpxTflu,pclDataBuf,3);
           TrimRight(pclDpxTflu);
           if (prgPalValues[ilI].pclPalFkt[0] == 'D')
           {
              sprintf(pclSelection,"WHERE URNO = %s",pclDpxUrno);
              ilRC = SendCedaEvent(7150,0,pcgAftDestName,pcgAftRecvName,
                                   pcgAftTwStart,pcgAftTwEnd,
                                   "DRT","DPXTAB",pclSelection,
                                   "","","",3,NETOUT_NO_ACK);
              dbg(DEBUG,"%s Selection <%s>",pclFunc,pclSelection);
              dbg(DEBUG,"%s Sent <DRT> for <DPXTAB>. Returned <%d>",pclFunc,ilRC);
           }
           else if (prgPalValues[ilI].pclPalFkt[0] == 'U')
           {
              if (strcmp(pclDpxFlnuX,pclDpxFlnu) == 0)
              {
                 strcpy(pclFieldList,"PRMT,FLNU,TFLU,TLXU,LSTU,USEU,PRID");
                 sprintf(pclSelection,"WHERE URNO = %s",pclDpxUrno);
                 sprintf(pclValueList,"%s,%s,%s,%s,%s,FDIHDL,%s",
                         &prgPalValues[ilI].pclPalType[0],
                         pclDpxFlnuX,
                         pclDpxTfluX,
                         pcgNextTlxUrno,
                         pclCurTime,
                         pcgTlxTimeForPal);
                 ilRC = SendCedaEvent(7150,0,pcgAftDestName,pcgAftRecvName,
                                      pcgAftTwStart,pcgAftTwEnd,
                                      "URT","DPXTAB",pclSelection,
                                      pclFieldList,pclValueList,"",3,NETOUT_NO_ACK);
                 dbg(DEBUG,"%s Selection <%s>",pclFunc,pclSelection);
                 dbg(DEBUG,"%s FieldList <%s>",pclFunc,pclFieldList);
                 dbg(DEBUG,"%s ValueList <%s>",pclFunc,pclValueList);
                 dbg(DEBUG,"%s Sent <URT> for <DPXTAB>. Returned <%d>",pclFunc,ilRC);
                 if (igSendMsgToAlerterForPAL_PSM == TRUE)
                 {
                    TimeToStr(pclTime,time(NULL));
                    strcpy(pclAlmFields,"TXTL,URAL,CDAT,USEC");
                    sprintf(pclAlmData,pcgRalTextPal,
                            &prgPalValues[ilI].pclPalName[0],pcgFlightNumber,pcgFlightSchedTime);
                    strcat(pclAlmData,",");
                    strcat(pclAlmData,pcgRalUrnoPal);
                    strcat(pclAlmData,",");
                    strcat(pclAlmData,pclTime);
                    strcat(pclAlmData,",FDIHDL");
                    ilRC = FdiHandleSql("IBT","ALMTAB","",pclAlmFields,pclAlmData,
                                        pcgTwStart,pcgTwEndNew,FALSE,TRUE);
                 }
                 if (strcmp(pclDpxTflu,"0") == 0 && strcmp(&prgPalValues[ilI].pclPalConUrno[0],"0") != 0)
                 {
                    if (igCreate2DPXRecords == TRUE)
                    {
                       if (strlen(pcgHandlingTypeForPRM) > 0)
                       {
                          strcpy(pclFieldList,"NAME,PRMT,FLNU,TFLU,TLXU,SEAT,ABSR,FDAT,SATI,ABDT,FILT,CDAT,USEC,PRID");
                          sprintf(pclValueList,"%s,%s,%s,%s,%s, ,PAL,%s,%s,%s,%s,%s,FDIHDL,%s",
                                  &prgPalValues[ilI].pclPalName[0],
                                  &prgPalValues[ilI].pclPalType[0],
                                  &prgPalValues[ilI].pclPalConUrno[0],
                                  pcgAftUrno,
                                  pcgNextTlxUrno,
                                  &prgPalValues[ilI].pclPalConFlightStof[0],
                                  &prgPalValues[ilI].pclPalConFlightStof[0],
                                  pclCurTime,
                                  &prgPalValues[ilI].pclPalConHdlAgt[0],
                                  pclCurTime,
                                  pcgTlxTimeForPal);
                       }
                       else
                       {
                          strcpy(pclFieldList,"NAME,PRMT,FLNU,TFLU,TLXU,SEAT,ABSR,FDAT,SATI,ABDT,CDAT,USEC,PRID");
                          sprintf(pclValueList,"%s,%s,%s,%s,%s, ,PAL,%s,%s,%s,%s,FDIHDL,%s",
                                  &prgPalValues[ilI].pclPalName[0],
                                  &prgPalValues[ilI].pclPalType[0],
                                  &prgPalValues[ilI].pclPalConUrno[0],
                                  pcgAftUrno,
                                  pcgNextTlxUrno,
                                  &prgPalValues[ilI].pclPalConFlightStof[0],
                                  &prgPalValues[ilI].pclPalConFlightStof[0],
                                  pclCurTime,
                                  pclCurTime,
                                  pcgTlxTimeForPal);
                       }
                       ilRC = SendCedaEvent(7150,0,pcgAftDestName,pcgAftRecvName,
                                            pcgAftTwStart,pcgAftTwEnd,
                                            "IRT","DPXTAB","",
                                            pclFieldList,pclValueList,"",3,NETOUT_NO_ACK);
                       dbg(DEBUG,"%s FieldList <%s>",pclFunc,pclFieldList);
                       dbg(DEBUG,"%s ValueList <%s>",pclFunc,pclValueList);
                       dbg(DEBUG,"%s Sent <IRT> for <DPXTAB>. Returned <%d>",pclFunc,ilRC);
                    }
                    strcpy(pclDpxFlnu,&prgPalValues[ilI].pclPalConUrno[0]);
                 }
              }
/*
              else if (strcmp(&prgPalValues[ilI].pclPalConUrno[0],"0") == 0)
              {
                 sprintf(pclSelection,"WHERE URNO = %s",pclDpxUrno);
                 ilRC = SendCedaEvent(7150,0,pcgAftDestName,pcgAftRecvName,
                                      pcgAftTwStart,pcgAftTwEnd,
                                      "DRT","DPXTAB",pclSelection,
                                      "","","",3,NETOUT_NO_ACK);
                 dbg(DEBUG,"%s Selection <%s>",pclFunc,pclSelection);
                 dbg(DEBUG,"%s Sent <DRT> for <DPXTAB>. Returned <%d>",pclFunc,ilRC);
              }
*/
              else
              {
                 sprintf(pclSelection,"WHERE URNO = %s",pclDpxUrno);
                 if (strlen(pcgHandlingTypeForPRM) > 0)
                 {
                    strcpy(pclFieldList,"PRMT,FILT,FLNU,TFLU,TLXU,LSTU,USEU,PRID");
                    sprintf(pclValueList,"%s,%s,%s,%s,%s,%s,FDIHDL,%s",
                            &prgPalValues[ilI].pclPalType[0],
                            &prgPalValues[ilI].pclPalConHdlAgt[0],
                            pclDpxFlnuX,
                            pclDpxTfluX,
                            pcgNextTlxUrno,
                            pclCurTime,
                            pcgTlxTimeForPal);
                 }
                 else
                 {
                    strcpy(pclFieldList,"PRMT,FLNU,TFLU,TLXU,LSTU,USEU,PRID");
                    sprintf(pclValueList,"%s,%s,%s,%s,%s,FDIHDL,%s",
                            &prgPalValues[ilI].pclPalType[0],
                            pclDpxFlnuX,
                            pclDpxTfluX,
                            pcgNextTlxUrno,
                            pclCurTime,
                            pcgTlxTimeForPal);
                 }
                 ilRC = SendCedaEvent(7150,0,pcgAftDestName,pcgAftRecvName,
                                      pcgAftTwStart,pcgAftTwEnd,
                                      "URT","DPXTAB",pclSelection,
                                      pclFieldList,pclValueList,"",3,NETOUT_NO_ACK);
                 dbg(DEBUG,"%s Selection <%s>",pclFunc,pclSelection);
                 dbg(DEBUG,"%s FieldList <%s>",pclFunc,pclFieldList);
                 dbg(DEBUG,"%s ValueList <%s>",pclFunc,pclValueList);
                 dbg(DEBUG,"%s Sent <URT> for <DPXTAB>. Returned <%d>",pclFunc,ilRC);
                 if (igSendMsgToAlerterForPAL_PSM == TRUE)
                 {
                    TimeToStr(pclTime,time(NULL));
                    strcpy(pclAlmFields,"TXTL,URAL,CDAT,USEC");
                    sprintf(pclAlmData,pcgRalTextPal,
                            &prgPalValues[ilI].pclPalName[0],pcgFlightNumber,pcgFlightSchedTime);
                    strcat(pclAlmData,",");
                    strcat(pclAlmData,pcgRalUrnoPal);
                    strcat(pclAlmData,",");
                    strcat(pclAlmData,pclTime);
                    strcat(pclAlmData,",FDIHDL");
                    ilRC = FdiHandleSql("IBT","ALMTAB","",pclAlmFields,pclAlmData,
                                        pcgTwStart,pcgTwEndNew,FALSE,TRUE);
                 }
              }
           }
           if (strstr(pclUrnoList,pclDpxFlnu) == NULL && strcmp(pcgAftUrno,pclDpxFlnu) != 0)
           {
              strcat(pclUrnoList,",");
              strcat(pclUrnoList,pclDpxFlnu);
           }
           if (strstr(pclUrnoList,pclDpxTflu) == NULL && strcmp(pcgAftUrno,pclDpxTflu) != 0)
           {
              strcat(pclUrnoList,",");
              strcat(pclUrnoList,pclDpxTflu);
           }
           if (strstr(pclUrnoList,&prgPalValues[ilI].pclPalConUrno[0]) == NULL &&
               strcmp(pcgAftUrno,&prgPalValues[ilI].pclPalConUrno[0]) != 0)
           {
              strcat(pclUrnoList,",");
              strcat(pclUrnoList,&prgPalValues[ilI].pclPalConUrno[0]);
           }
           slFkt = NEXT;
           ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        }
        close_my_cursor(&slCursor);
     }
  }

  sprintf(pclSelection,"WHERE URNO = %s",pcgAftUrno);
  ilRC = SendCedaEvent(igAftReceiver,0,pcgAftDestName,pcgAftRecvName,
                       pcgAftTwStart,pcgAftTwEnd,
                       "UFR","AFTTAB",pclSelection,
                       "PRMC","Y","",3,NETOUT_NO_ACK);
  dbg(DEBUG,"%s Selection <%s>",pclFunc,pclSelection);
  dbg(DEBUG,"%s FieldList <PRMC>",pclFunc);
  dbg(DEBUG,"%s ValueList <Y>",pclFunc);
  dbg(DEBUG,"%s Sent <UFR>. Returned <%d>",pclFunc,ilRC);
  if (strlen(pclUrnoList) > 0)
  {
     ilNoEle = GetNoOfElements(pclUrnoList,',');
     for (ilI = 2; ilI <= ilNoEle; ilI++)
     {
        get_real_item(pclCurUrno,pclUrnoList,ilI);
        sprintf(pclSelection,"WHERE URNO = %s",pclCurUrno);
        ilRC = SendCedaEvent(igAftReceiver,0,pcgAftDestName,pcgAftRecvName,
                             pcgAftTwStart,pcgAftTwEnd,
                             "UFR","AFTTAB",pclSelection,
                             "PRMC","Y","",3,NETOUT_NO_ACK);
        dbg(DEBUG,"%s Selection <%s>",pclFunc,pclSelection);
        dbg(DEBUG,"%s FieldList <PRMC>",pclFunc);
        dbg(DEBUG,"%s ValueList <Y>",pclFunc);
        dbg(DEBUG,"%s Sent <UFR>. Returned <%d>",pclFunc,ilRC);
     }
  }

  if (strlen(pclUrnoList) > 0)
     sprintf(pclSelection,"WHERE URNO IN (%s%s)",pcgAftUrno,pclUrnoList);
  else
     sprintf(pclSelection,"WHERE URNO IN (%s)",pcgAftUrno);
  ilRC = SendCedaEvent(7540,0,pcgAftDestName,pcgAftRecvName,
                       pcgAftTwStart,pcgAftTwEnd,
                       "SFC","",pclSelection,
                       "","","",3,NETOUT_NO_ACK);
  dbg(DEBUG,"%s Selection <%s>",pclFunc,pclSelection);
  dbg(DEBUG,"%s Sent <SFC> to flicol. Returned <%d>",pclFunc,ilRC);

  return ilRC;
} /* end of SendPALDPXRes  */


