#ifndef _DEF_mks_version_uniif_h
  #define _DEF_mks_version_uniif_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_uniif_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/uniif.h 1.2 2004/07/27 17:04:48SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */
/*  The Master include file.                                              */
/*                                                                        */
/*  Program       : unicenter interface                                   */
/*                                                                        */
/*  Revision date :                                                       */
/*                                                                        */
/*  Author    	  : Volker Bliss                                          */
/*                                                                        */
/*                                                                        */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */

#ifndef _UNIIF_H
#define _UNIIF_H


#include <sys/types.h>
#include <time.h>



#define UNI_MSG1 0x14046701
#define UNI_MSG2 0x14046702
#define UNI_MSG3 0x14046703
#define UNI_MSG4 0x14046704

#define UNI_PROC_NAME	"uniif"
#define UNI_ROUTE		19901

#define UNI_CAUSE_LEN	255
#define UNI_MSG_LEN		255
#define UNI_REPLY_LEN	255



struct UnicenterIF {
	char	cCause[UNI_CAUSE_LEN+1];
	short	sModId;
	time_t	lTimeStamp;
	int		iState;
	int		iFlags;
	char	cMsg[UNI_MSG_LEN+1];
	char	cReply[UNI_REPLY_LEN+1];
};

typedef struct UnicenterIF UNIIF;


int SendMsgToUniIf(char *pcpMsg);

int SendToUniIf(char* pcpCause, short spModId, int ipState, int ipFlags, char* pcpMsg);

int SendStructToUniIf(UNIIF* prpUniIf);

int PrintUniIfVersion(int ipDebugLevel);

int PrintUniIf(int ipDebugLevel, UNIIF* prpUniif);



/***

int UniifSendLogonOk(...);
int UniifSendLogonFail(...);
int UniifSendLogoffOk(...);

int UniifSendProcTerminated(...);
int UniifSendProcRestarted(...);

***/

#endif

/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */
