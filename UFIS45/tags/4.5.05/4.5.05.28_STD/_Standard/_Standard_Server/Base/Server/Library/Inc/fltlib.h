#ifndef _DEF_mks_version_fltlib_h
  #define _DEF_mks_version_fltlib_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_fltlib_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/fltlib.h 1.1 2011/08/09 16:26:44SGT bst Exp bst(2011/08/09 16:42:24SGT) $";
#endif /* _DEF_mks_version */
#ifndef __FLTLIB_H__
#define __FLTLIB_H__

extern int FLT_InitFltLibEnvironment(void);
extern int FLT_TrimRecordData(char *pclTextBuff,char *pcpFldSep);
extern int FLT_SyncGorRotation(char *pcpSqlKey);
extern int FLT_GetGortabData(char *pcpBgnTime, char *pcpEndTime, char *pcpParam, int ipForWhat, STR_DESC *prpResult);

#endif
