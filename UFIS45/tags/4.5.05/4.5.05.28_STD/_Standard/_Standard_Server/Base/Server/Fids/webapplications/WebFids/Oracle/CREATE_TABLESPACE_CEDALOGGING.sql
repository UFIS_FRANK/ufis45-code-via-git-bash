
prompt prerequisite: a link in /ora/cedaloggin must point to free space
prompt create link: ln -s /u04 /ora/cedaloggin

CREATE TABLESPACE CEDALOGGING DATAFILE
        '/ora/webfids/oradata/UFIS/CEDALOGGIN.dbf' SIZE 1000M 
        autoextend on
default storage (
        initial         1M
        next            1M
        pctincrease     0
        minextents      1
        maxextents      unlimited
        );
