#ifndef _DEF_mks_version
#define _DEF_mks_version
#include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Wmq/wmqcdi.c 1.31 2009/06/15 12:38:10SGT cst Exp cst(2010/04/27 11:34:48SGT) $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history : HEB: added Alerter-Funktionality                          */
/*                : CST: 300608 Recovery for lost queue handle................*/
/*                : CST: 180708 Added function to detect unrecoverable errors */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

static char sccs_version[] ="@(#) UFIS45 (c) ABB AAT/I wmqcdi.c 45.3 / 04.06.2002 HEB";


/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */


#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "db_if.h"
#include <time.h>
#include <cedatime.h>
/* includes for WebsphereMQ  */
#include <cmqc.h>

#ifdef _HPUX_SOURCE
extern int daylight;
extern long timezone;
extern char *tzname[2];
#else
extern int _daylight;
extern long _timezone;
extern char *_tzname[2];
#endif

int debug_level = 0;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static EVENT *prgOutEvent  = NULL;

static char cgProcessName[20] = "\0";
static char  cgConfigFile[512] = "\0";
static char  cgHopo[8] = "\0";                         /* default home airport    */
static char  cgTabEnd[8] ="\0";                       /* default table extension */

static long lgEvtCnt = 0;

#define MAX_SEND_QUEUES = 20;

static char cgDbLogging[20] = "\0";
static char cgRecovery[10] = "\0";
static char cgUseAlerter[100] = "\0";
static char cgSelection[200] = "\0";
static char cgNameAlert[1024] = "\0";
static char cgContextAlert[2048] = "\0";
static char cgMQServer[100] = "\0";
static char cgQMgrReceive[50] = "\0";
static char cgQMgrSend[50] = "\0";
static char cgMqReceive[50] = "\0";
static char cgMqSend[1024] = "\0";
static char cgMqSendPrio[110] = "\0";
static int igMqSendPrio = 0;
static char cgCedaReceive[10] = "\0";
static char cgCedaReceiveCommand[10] = "\0";
static char cgCedaSend[10] = "\0";
static char cgCedaSendPrio[10] = "\0";
static char cgCedaSendCommand[10] = "\0";
static char cgSimulation[10] = "\0";
static char cgInterval[10] = "\0";
static char cgReconnectMq[10] = "\0";
static char cgCleanMqr[10] = "\0";
static char cgCleanMqs[10] = "\0";

static int igPutFailed = FALSE;
static int igPutFailedOld = FALSE;
static char cgFirstFailedUrno[20] = "\0";
static char cgFirstFailedData[4002] = "\0";
static char cgUseQName[10] = "\0";
static char igUseQName = FALSE;

static char cgWaitInterval[410] = "\0";
static int bgConnected = FALSE;
static long lgCount = -1;
static int igQueueCount = 0;
/* CST: 180708 */
static int igRetryCount = 0;


typedef struct
{
	MQHOBJ	HobjSend;
	char	cgQName[50];
	int		igQPrio;
	int		igQDynPrio;
	int		bgQConnected;
	int		igPutFailed;
	int		igPutFailedOld;
	char	cgFirstFailedData[20];
	char	cgFirstFailedUrno[20];
	char	cgUseAlerter[10];
	char	cgNameAlert[50];
	char	cgContextAlert[100];
	char	cgWaitInterval[20];
} _MyQueues;

_MyQueues rgMyQueues[20];



pid_t gChildPid;
/*Handles for Sending*/
MQHCONN  HconSend;                   /* connection handle             */
MQHOBJ   HobjSend;                   /* object handle                 */

/*Handles for Receiving */
MQHCONN  HconRecBrowse;              /* connection handle             */
MQHCONN  HconRecGet;                 /* connection handle             */
MQHOBJ   HobjRecBrowse;              /* object handle                 */
MQHOBJ   HobjRecGet;

/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int Init_Process();
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer, char *pcpDefault);


static int Reset(void);                       /* Reset program          */
static void Terminate(int ipSleep);            /* Terminate program      */
static void HandleSignal(int);                 /* Handles signals        */
static void HandleErr(int);                    /* Handles general errors */
static void HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/


static int HandleData(EVENT *prpEvent);       /* Handles event data     */
static int GetCommand(char *pcpCommand, int *pipCmd);

static int ReceiveFromMQ();
static int SendToCeda(char *pcpBuffer,char *pcpMyTime,long plpMyReason, char *pcpUrno);
static int ConnectMQ(char *pcpQManager, MQHCONN *Hcon);
static int TimeToStr(char *pcpTime,time_t lpTime);
static int CheckIfMessageInWork();
static int OpenSendMQ(int ipQueueNumber);
static int CreateLogFile();
void myinitialize(int argc,char *argv[]);
static int SendToMq(char *pcpData, int ipQueueNumber);
static int MarkToSend(char *pcpUrno, int ipQueueNumber);
static int SendFailedMessages(int ipQueueNumber);
void CleanDb();
static int DoRecovery(char *pcpBeginTime, int ipQueueNumber);
static int GetQueueNumber(char *clQueueName);
/* CST: 180708 */
static int IsRecoverable(MQLONG ilReasonCode);

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int	ilRC = RC_SUCCESS;			/* Return code			*/
    int	ilCnt = 0;
    int ilOldDebugLevel = 0;
	int ili = 0;

    gChildPid = fork();

	
	    

/*    INITIALIZE;	*/
    myinitialize(argc,argv);

    (void)SetSignals(HandleSignal);
    debug_level = DEBUG;

    if(gChildPid == -1)
    {
	dbg(TRACE,"%05d: Fork failed!",__LINE__);
    }

    strcpy(cgProcessName,argv[0]);

    dbg(TRACE,"MAIN: version <%s>",sccs_version);
    while (gChildPid == -1)
    {
	dbg(TRACE,"********* fork failed, waiting 60 Seconds **********");
    }
		
    if (gChildPid == 0)
    {
	dbg(TRACE,"******* I am the child *******");
    }
    else
    {
	dbg(TRACE,"******* I am the parent *******");
    }

    /* Attach to the MIKE queues */
    do
    {
	ilRC = init_que();
	if(ilRC != RC_SUCCESS)
	{
	    dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
	    sleep(6);
	    ilCnt++;
	}/* end of if */
    }while((ilCnt < 10) && (ilRC != RC_SUCCESS));

    if(ilRC != RC_SUCCESS)
    {
	dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
	sleep(60);
	exit(1);
    }else{
	dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

    do
    {
	ilRC = init_db();
	if (ilRC != RC_SUCCESS)
	{
	    check_ret(ilRC);
	    dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
	    sleep(6);
	    ilCnt++;
	} /* end of if */
    } while((ilCnt < 10) && (ilRC != RC_SUCCESS));

    if(ilRC != RC_SUCCESS)
    {
	dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
	sleep(60);
	exit(2);
    }else{
	dbg(TRACE,"MAIN: init_db() OK!");
    } /* end of if */


    sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
    dbg(TRACE,"Binary_File <%s>",cgConfigFile);
    ilRC = TransferFile(cgConfigFile);
    if(ilRC != RC_SUCCESS)
    {
	dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRC = TransferFile(cgConfigFile);
    if(ilRC != RC_SUCCESS)
    { 
	dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    ilRC = SendRemoteShutdown(mod_id);
    if(ilRC != RC_SUCCESS)
    {
	dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */


    dbg(TRACE,"=====================");
    dbg(TRACE,"MAIN: initializing OK");
    dbg(TRACE,"=====================");
	

    ilRC = Init_Process();

    if (gChildPid == 0)
    {
		if(strcmp(cgDbLogging,"ON") == 0)
		{
			CheckIfMessageInWork();
		}
	ReceiveFromMQ();
    }
    else
    {
		if((strcmp(cgDbLogging,"ON") == 0)&& (strcmp(cgRecovery,"ON") == 0))
		{
			for (ili = 0 ; ili < 20 ; ili++)
			{
				if(strlen(rgMyQueues[ili].cgQName) > 0)
				{
					SendFailedMessages(ili);
				}
			}
		}
    }
    for(;;)
    {
	/* the child should not listen to this CEDA-queue*/
	ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
	/* depending on the size of the received item  */
	/* a realloc could be made by the que function */
	/* so do never forget to set event pointer !!! */
	prgEvent = (EVENT *) prgItem->text;
				
	if( ilRC == RC_SUCCESS )
	{
	    /* Acknowledge the item */
	    ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
	    if( ilRC != RC_SUCCESS ) 
	    {
		/* handle que_ack error */
		HandleQueErr(ilRC);
	    } /* fi */

	    lgEvtCnt++;

	    switch( prgEvent->command )
	    {

		case	REMOTE_DB :
		    /* ctrl_sta is checked inside */
		    HandleRemoteDB(prgEvent);
		    break;
		case	SHUTDOWN	:
		    /* process shutdown - maybe from uutil */
		    Terminate(1);
		    break;
					
		case	RESET		:
		    ilRC = Reset();
		    break;
					
		case	EVENT_DATA	:

		    ilRC = HandleData(prgEvent);
		    if(ilRC != RC_SUCCESS)
		    {
			HandleErr(ilRC);
		    }/* end of if */
		    break;
				

		case	TRACE_ON :
		    dbg_handle_debug(prgEvent->command);
		    break;
		case	TRACE_OFF :
		    dbg_handle_debug(prgEvent->command);
		    break;
		default			:
		    dbg(TRACE,"MAIN: unknown event");
		    DebugPrintItem(TRACE,prgItem);
		    DebugPrintEvent(TRACE,prgEvent);
		    break;
	    } /* end switch */




	} else {
	    /* Handle queuing errors */
	    HandleQueErr(ilRC);
	} /* end else */
		
    } /* end for */
	
      
} /* end of MAIN */



static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer, char *pcpDefault)
{
    int ilRC = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRC = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
			   CFG_STRING,pcpCfgBuffer);
    if(ilRC != RC_SUCCESS)
    {
	dbg(TRACE,"Not found in %s: [%s] <%s>",cgConfigFile,clSection,clKeyword);
	dbg(TRACE,"use default-value: <%s>",pcpDefault);
	strcpy(pcpCfgBuffer,pcpDefault);
    } 
    else
    {
	dbg(DEBUG,"Config Entry [%s],<%s>:<%s> found in %s",
	    clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRC;
}


/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/

static int Init_Process()
{
    int  ilRC = RC_SUCCESS;			/* Return code */
    char clSection[64] = "\0";
    char clKeyword[64] = "\0";
    long pclAddFieldLens[12];
    char pclAddFields[256] = "\0";
    char pclSelection[1024] = "\0";
    short slCursor = 0;
    short slSqlFunc = 0;
    char  clBreak[24] = "\0";
    char clQueueName[30] = "\0";
	int ili = 0;
	char clTemp[100] = "\0";
	int blMoreQueues = TRUE;
    int ilPid = 0;

    debug_level = DEBUG;

    if(ilRC == RC_SUCCESS)
    {
	/* read HomeAirPort from SGS.TAB */
	ilRC = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
	if (ilRC != RC_SUCCESS)
	{
	    dbg(TRACE,"<Init_Process> EXTAB,SYS,HOMEAP not found in SGS.TAB");
	    Terminate(30);
	} else {
	    dbg(TRACE,"<Init_Process> home airport <%s>",cgHopo);
	}
    }

	
    if(ilRC == RC_SUCCESS)
    {

	ilRC = tool_search_exco_data("ALL","TABEND", cgTabEnd);
	if (ilRC != RC_SUCCESS)
	{
	    dbg(TRACE,"<Init_Process> EXTAB,ALL,TABEND not found in SGS.TAB");
	    Terminate(30);
	} else {
	    dbg(TRACE,"<Init_Process> table extension <%s>",cgTabEnd);
	}
    }

    if (ilRC == RC_SUCCESS)
	{
		ilRC = ReadConfigEntry("MAIN","DB_LOGGING",cgDbLogging,"ON");
        ilRC = ReadConfigEntry("MAIN","RECOVERY",cgRecovery,"ON");
		if(strcmp(cgDbLogging,"OFF") == 0)
		{
			strcpy(cgRecovery,"OFF");
			dbg(TRACE,"%05d: RECOVERY switched to <OFF> because DB-logging is <OFF>",__LINE__);
		}
		ilRC = ReadConfigEntry("MAIN","SELECTION",cgSelection,"");
		ilRC = ReadConfigEntry("MAIN","USE_QUEUENAME_FOR_INAM",cgUseQName,"NO");
		ilRC = ReadConfigEntry("MAIN","USE_ALERTER",cgUseAlerter,"YES");
		ilRC = ReadConfigEntry("MAIN","NAME_SEND_TO_ALERTER",cgNameAlert,"");
		ilRC = ReadConfigEntry("MAIN","CONTEXT_FOR_ALERT",cgContextAlert,"");
		ilRC = ReadConfigEntry("MAIN","WAIT_INTERVAL",cgWaitInterval,"60000");
		ilRC = ReadConfigEntry("QUEUE","MQSERVER",cgMQServer,"");
		ilRC = ReadConfigEntry("QUEUE","MQ_MGR_RECEIVE",cgQMgrReceive,"");
		ilRC = ReadConfigEntry("QUEUE","MQ_MGR_SEND",cgQMgrSend,"");
		ilRC = ReadConfigEntry("QUEUE","MQ_RECEIVE",cgMqReceive,"");
		ilRC = ReadConfigEntry("QUEUE","MQ_SEND",cgMqSend,"");
		ilRC = ReadConfigEntry("QUEUE","MQ_SEND_PRIO",cgMqSendPrio,"0");
		ilRC = ReadConfigEntry("QUEUE","CEDA_RECEIVE_COMMAND",cgCedaReceiveCommand,"");
		ilRC = ReadConfigEntry("QUEUE","CEDA_SEND",cgCedaSend,"");
		ilRC = ReadConfigEntry("QUEUE","CEDA_SEND_PRIO",cgCedaSendPrio,"5");
		ilRC = ReadConfigEntry("QUEUE","CEDA_SEND_COMMAND",cgCedaSendCommand,"");

		ilRC = ReadConfigEntry("RECONNECT","RECONNECT_MQ",cgReconnectMq,"10");

		ilRC = ReadConfigEntry("CLEANUP","CLEAN_MQR",cgCleanMqr,"30");
		ilRC = ReadConfigEntry("CLEANUP","CLEAN_MQS",cgCleanMqs,"30");

		if((strcmp(cgUseQName,"NO") == 0) && (strstr(cgMqSend,",") == NULL))
		{
			igUseQName = FALSE;
		}
		else
		{
			igUseQName = TRUE;
		}

		ili = 0;
		while(blMoreQueues == TRUE)
		{
			ilRC = get_item(ili+1,cgMqSend,rgMyQueues[ili].cgQName,0,",","\0","\0");
			if(ilRC == FALSE) blMoreQueues = FALSE;

			ilRC = get_item(ili+1,cgMqSendPrio,clTemp,0,",","\0","\0");
			if(ilRC == FALSE) blMoreQueues = FALSE;
			rgMyQueues[ili].igQPrio = atoi(clTemp);
			rgMyQueues[ili].igQDynPrio = -1;

			ilRC = get_item(ili+1,cgUseAlerter,rgMyQueues[ili].cgUseAlerter,0,",","\0","\0");
			if(ilRC == FALSE) blMoreQueues = FALSE;

			ilRC = get_item(ili+1,cgNameAlert,rgMyQueues[ili].cgNameAlert,0,",","\0","\0");
			if(ilRC == FALSE) blMoreQueues = FALSE;
			
			ilRC = get_item(ili+1,cgContextAlert,rgMyQueues[ili].cgContextAlert,0,",","\0","\0");
			if(ilRC == FALSE) blMoreQueues = FALSE;
			
			ilRC = get_item(ili+1,cgWaitInterval,rgMyQueues[ili].cgWaitInterval,0,",","\0","\0");
			if(ilRC == FALSE) blMoreQueues = FALSE;
			
			rgMyQueues[ili].bgQConnected = FALSE;
			rgMyQueues[ili].igPutFailed = FALSE;
			rgMyQueues[ili].igPutFailedOld = FALSE;
			rgMyQueues[ili].cgFirstFailedUrno[0] = '\0';
			rgMyQueues[ili].cgFirstFailedData[0] = '\0';
			ili++;
		}
		igQueueCount = ili -1;

		for(ili = 0 ; ili < igQueueCount ; ili++)
		{
			dbg(TRACE,"%05d: ......... QUEUE <%d> .......... found",__LINE__,ili);
			dbg(TRACE,"%05d: QName: <%s>",__LINE__,rgMyQueues[ili].cgQName);
			dbg(TRACE,"%05d: QPrio: <%d>",__LINE__,rgMyQueues[ili].igQPrio);
			dbg(TRACE,"%05d: QUseAlerter: <%s>",__LINE__,rgMyQueues[ili].cgUseAlerter);		
			dbg(TRACE,"%05d: QNameAlert: <%s>",__LINE__,rgMyQueues[ili].cgNameAlert);
			dbg(TRACE,"%05d: QContextAlert: <%s>",__LINE__,rgMyQueues[ili].cgContextAlert);
			dbg(TRACE,"%05d: QWaitInterval: <%s>",__LINE__,rgMyQueues[ili].cgWaitInterval);
			dbg(TRACE,"%05d: QConnected: <%d>",__LINE__,rgMyQueues[ili].bgQConnected);
			dbg(TRACE,"%05d: QFirstFailedUrno: <%s>",__LINE__,rgMyQueues[ili].cgFirstFailedUrno);
			dbg(TRACE,"%05d: QFirstFailedData: <%s>",__LINE__,rgMyQueues[ili].cgFirstFailedData);
		}
    }



	if( gChildPid != 0)
    {
		dbg(TRACE,"%05d: connect for sending to MQ",__LINE__);
		if((strlen(cgQMgrSend) > 0) && (bgConnected == FALSE))
		{
			ilRC = ConnectMQ(cgQMgrSend, &HconSend);

			for(ili = 0 ; ili < igQueueCount ; ili++)
			{
				ilRC = OpenSendMQ(ili);	
			}
		}
		else
		{
			dbg(TRACE,"%05d: Send-Qmanager not configured or already connected",__LINE__);
		}
    }


    dbg(TRACE," PID: <%d>",getpid());

    if (ilRC != RC_SUCCESS)
    {
		dbg(TRACE,"Init_Process failed");
    }
    else
    {
		dbg(DEBUG,"Init_Process successful");
    }



	

    debug_level = TRACE;
	
    return(ilRC);
	
} /* end of initialize */




/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int	ilRC = RC_SUCCESS;				/* Return code */
	
    dbg(TRACE,"Reset: now resetting");
	
    return ilRC;
	
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
    int ilRC = RC_SUCCESS;
	int ili = 0;

	MQLONG   C_options;              /* MQCLOSE options                 */
    MQLONG   CompCode;               /* completion code                 */
    MQLONG   Reason;                 /* reason code                     */

    C_options = MQCO_NONE;           /* no close options             */

    if(gChildPid == 0)   
    {
		MQCLOSE (HconRecBrowse, &HobjRecBrowse, C_options, &CompCode, &Reason);
		dbg(TRACE,"Terminate: Closed Browse Queue ended with Reasoncod: <%d>, CompletionCode: <%d>",Reason,CompCode);
		MQCLOSE (HconRecGet, &HobjRecGet, C_options, &CompCode, &Reason);
		dbg(TRACE,"Terminate: Closed Get Queue ended with Reasoncod: <%d>, CompletionCode: <%d>",Reason,CompCode);
		MQDISC(&HconRecBrowse,&CompCode,&Reason);
		dbg(TRACE,"Terminate: Closed Receive Browse connection ended with Reasoncod: <%d>, CompletionCode: <%d>",Reason,CompCode);
		MQDISC(&HconRecGet,&CompCode,&Reason);
		dbg(TRACE,"Terminate: Closed Receive Get connection ended with Reasoncod: <%d>, CompletionCode: <%d>",Reason,CompCode);

    }
    else
    {
		for(ili = 0 ; ili < igQueueCount ; ili++)
		{
			MQCLOSE (HconSend, &rgMyQueues[ili].HobjSend, C_options, &CompCode, &Reason);	
		}

		MQDISC(&HconSend,&CompCode,&Reason);
		dbg(TRACE,"Terminate: Closed Send connection ended with Reasoncod: <%d>, CompletionCode: <%d>",Reason,CompCode);
		dbg(TRACE,"Due to SIGTERM Kill Child now");
    }

    if(gChildPid == 0)
    {
    }
    else
    {
	kill(gChildPid,SIGTERM);
    }




    /* unset SIGCHLD ! DB-Child will terminate ! */

    dbg(TRACE,"Terminate: now DB logoff ...");
	
    logoff();

    dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


    dbg(TRACE,"Terminate: now leaving ...");

    fclose(outp);

    sleep(ipSleep < 1 ? 1 : ipSleep);
	
    exit(0);
	
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/

static void HandleSignal(int pipSig)
{
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

    switch(pipSig)
    {
	case SIGCHLD:

	    dbg(TRACE,"HandleSignal:SIGHCLD received, Terminate now");
	    Terminate(1);

	    break;
	case SIGTERM:
	    dbg(TRACE,"HandleSignal:SIGTERM received !");
	    if(gChildPid != 0)   /* then its a parent */
	    {
		dbg(TRACE,"Due to SIGTERM Kill Child now");
		kill(gChildPid,SIGTERM);
	    }
	    dbg(TRACE,"Due to SIGTERM Terminate now");
	    Terminate(1);
	    break;


	default	:
	    Terminate(1);
	    break;
    } /* end of switch */

    /*exit(1);*/
	
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
	    dbg(TRACE,"<%d> : unknown function",pipErr);
	    break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
	    dbg(TRACE,"<%d> : malloc failed",pipErr);
	    break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
	    dbg(TRACE,"<%d> : msgsnd failed",pipErr);
	    break;
	case	QUE_E_GET	:	/* Error using msgrcv */
	    dbg(TRACE,"%05d: <%d> : msgrcv failed",__LINE__,pipErr);
	    Terminate(1);
	    break;
	case	QUE_E_EXISTS	:
	    dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
	    break;
	case	QUE_E_NOFIND	:
	    dbg(TRACE,"<%d> : route not found ",pipErr);
	    break;
	case	QUE_E_ACKUNEX	:
	    dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
	    break;
	case	QUE_E_STATUS	:
	    dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
	    break;
	case	QUE_E_INACTIVE	:
	    dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
	    break;
	case	QUE_E_MISACK	:
	    dbg(TRACE,"<%d> : missing ack ",pipErr);
	    break;
	case	QUE_E_NOQUEUES	:
	    dbg(TRACE,"<%d> : queue does not exist",pipErr);
	    break;
	case	QUE_E_RESP	:	/* No response on CREATE */
	    dbg(TRACE,"<%d> : no response on create",pipErr);
	    break;
	case	QUE_E_FULL	:
	    dbg(TRACE,"<%d> : too many route destinations",pipErr);
	    break;
	case	QUE_E_NOMSG	:	/* No message on queue */
	    dbg(TRACE,"<%d> : no messages on queue",pipErr);
	    break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
	    dbg(TRACE,"<%d> : invalid originator=0",pipErr);
	    break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
	    dbg(TRACE,"<%d> : queues are not initialized",pipErr);
	    break;
	case	QUE_E_ITOBIG	:
	    dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
	    break;
	case	QUE_E_BUFSIZ	:
	    dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
	    break;
	default			:	/* Unknown queue error */
	    dbg(TRACE,"<%d> : unknown error",pipErr);
	    break;
    } /* end switch */
         
    return;

} /* end of HandleQueErr */
	



/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
    int	   ilRC           = RC_SUCCESS;			/* Return code */
    int      ilCmd          = 0;

    BC_HEAD *prlBchead       = NULL;
    CMDBLK  *prlCmdblk       = NULL;
    char    *pclSelection    = NULL;
    char    *pclFields       = NULL;
    char    *pclData         = NULL;
    char    *pclRow          = NULL;
    char 	clUrnoList[2400];
    char 	clTable[34];
    int		ilUpdPoolJob = TRUE;
	int		ilQueueNumber = 0;
	int		ili = 0;
	char	clQueueName[50] = "\0";
	long	llQueueNameLen = 0;
	char	*pclKeyPtr = NULL;
	char	clQDynPrio[10] = "\0";


    prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
    prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
    pclSelection = prlCmdblk->data;
    pclFields    = pclSelection + strlen(pclSelection) + 1;
    pclData      = pclFields + strlen(pclFields) + 1;


    strcpy(clTable,prlCmdblk->obj_name);

    dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);

    /****************************************/
    dbg(TRACE,"Command:   <%s>",prlCmdblk->command);
    dbg(TRACE,"tw_start:  <%s>",prlCmdblk->tw_start);
    dbg(TRACE,"tw_end:    <%s>",prlCmdblk->tw_end);
    dbg(TRACE,"originator <%d>",prpEvent->originator);
    dbg(TRACE,"selection  <%s>",pclSelection);
    dbg(TRACE,"fields     <%s>",pclFields);
    dbg(TRACE,"data       <%s>",pclData);
    /****************************************/

    dbg(TRACE,"%05d: prlCmdblk->command: <%s>, cgCedaReceiveCommand: <%s>",__LINE__,prlCmdblk->command,cgCedaReceiveCommand);
    if (strcmp(prlCmdblk->command,cgCedaReceiveCommand) == 0)
    {
		if(strlen(pclFields) == 0)
		{
			ilQueueNumber = 0; 
		}
		else
		{
			pclKeyPtr = CedaGetKeyItem(clQueueName,&llQueueNameLen,pclFields,"<QUEUE>","<\\QUEUE>", TRUE);
			if(strlen(clQueueName) > 0)
			{
				ilQueueNumber = GetQueueNumber(clQueueName);
				pclKeyPtr = CedaGetKeyItem(clQDynPrio,&llQueueNameLen,pclFields,"<QPRIO>","<\\QPRIO>", TRUE);
				if(strlen(clQDynPrio) > 0)
				{
					rgMyQueues[ilQueueNumber].igQDynPrio = atoi(clQDynPrio);
				}
				else
				{
					rgMyQueues[ilQueueNumber].igQDynPrio = -1;
				}
			}
			else
			{
				ilQueueNumber = 0;
			}
			dbg(TRACE,"%05d: QueueNumber: <%d>",__LINE__,ilQueueNumber);
		}
		if (ilQueueNumber < 0)
		{
			dbg(TRACE,"%05d: ilQueueNumber < 0 (%d). This can not be processed",__LINE__,ilQueueNumber);
		}
		else
		{
			if(strlen(pclData) >= 4000 && strcmp(cgDbLogging,"ON") == 0)
			{
				dbg(TRACE,"%05d: Data from CEDA >= 4000 Byte: actual length: <%d>. This can not be processed",__LINE__,strlen(pclData));
			}
			else
			{
				if(bgConnected == FALSE)
				{
					ilRC = ConnectMQ(cgQMgrSend, &HconSend);
					ilRC = OpenSendMQ(ilQueueNumber);
				}
				ilRC = SendToMq(pclData, ilQueueNumber);
			}
		}
    }

    if (strcmp(prlCmdblk->command,"SFM") == 0)
    {
		if((strcmp(cgDbLogging,"ON") == 0)&& (strcmp(cgRecovery,"ON") == 0))
		{
			for (ili = 0 ; ili < 20 ; ili++)
			{
				if(strlen(rgMyQueues[ili].cgQName) > 0)
				{
					SendFailedMessages(ili);
				}
			}
		}
    }

    if (strcmp(prlCmdblk->command,"CDB") == 0)
    {
		if(strcmp(cgDbLogging,"ON") == 0)
		{
			CleanDb();
		}
    }

    if (strcmp(prlCmdblk->command,"REC") == 0)
    {
		if(strcmp(cgDbLogging,"ON") == 0)
		{
			if(strlen(pclFields) == 0)
			{
				ilQueueNumber = 0; 
			}
			else
			{
				pclKeyPtr = CedaGetKeyItem(clQueueName,&llQueueNameLen,pclFields,"<QUEUE>","<\\QUEUE>", TRUE);
				ilQueueNumber = GetQueueNumber(clQueueName);
				ilRC = DoRecovery(pclSelection, ilQueueNumber);
			}

		}
    }

    if (strcmp(prlCmdblk->command,"28674") == 0)
    {
	Terminate(1);
    }

	

    dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);

    return(RC_SUCCESS);
	
} /* end of HandleData */



static int ConnectMQ(char *pcpQManager, MQHCONN *Hcon)
{
    static int ilRC = RC_SUCCESS;
 
    MQLONG   CompCode;               /* completion code               */
    MQLONG   CReason;                /* reason code for MQCONN        */

    static char clMyMQServer[100] = "\0";
    static char clNowTime[20] = "\0";
    static char clAlert[256] = "\0";

    /* SET ENVIRONMENT TO CONNECT TO THE RIGHT QMANAGER */
    sprintf(clMyMQServer,"MQSERVER=%s",cgMQServer);
    ilRC=putenv(clMyMQServer);
   
    /* CONNECT TO GIVEN QMANAGER */
    CompCode = MQCC_FAILED;
    
	MQCONN(pcpQManager,                  /* queue manager                  */
	       Hcon,                         /* connection handle              */
	       &CompCode,                    /* completion code                */
	       &CReason);                    /* reason code                    */

	/* report reason and stop if it failed     */
	if (CompCode == MQCC_FAILED)
	{
	    dbg(TRACE,"%05d: MQCONN ended with reason code <%d>, now waiting <%s> seconds and try to reconnect",__LINE__, CReason, cgReconnectMq);
		if(strcmp(cgUseAlerter,"YES") == 0)
		{
			sprintf(clAlert,"<%s>: MQCONN ended with reason code <%d> while connecting to QManager <%s>",cgContextAlert,CReason,pcpQManager);
			ilRC = AddAlert2(cgNameAlert," "," ","E",clAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
		}
		else
		{
			dbg(TRACE,"%05d: !!!!!!!!!! ERROR WHILE connecting to Qmanager !!!!!!!!!!!!!!!",__LINE__);
		}
		bgConnected = FALSE;	   
	}
	else
	{
		bgConnected = TRUE;
	    dbg(TRACE,"%05d: MQCONN connected with QManager <%s>",__LINE__, pcpQManager);
	    dbg(TRACE,"%05d: on Server: <%s>",__LINE__,cgMQServer); 
	}
    return ilRC;
}


static int ReceiveFromMQ()
{
    int ilRC = RC_SUCCESS;
    MQLONG   OpenBrowseOptions;              /* MQOPEN options                */
    MQHOBJ   QueueBrowseHobj;                   /* object handle                 */
    MQOD     QueueBrowseOd = {MQOD_DEFAULT};    /* Object Descriptor             */
    MQLONG   QueueBrowseOpenCode;               /* MQOPEN completion code        */
    MQLONG   QueueBrowseCompCode;               /* MQOPEN completion code        */
    MQLONG   QueueBrowseReason;                 /* reason code                   */
    MQGMO    QueueBrowseGmo = {MQGMO_DEFAULT};   /* get message options           */
    MQMD     ReceiveBrowseMd = {MQMD_DEFAULT};    /* Message Descriptor            */

    MQLONG   OpenOptions;              /* MQOPEN options                */
    MQHOBJ   QueueHobj;                   /* object handle                 */
    MQOD     QueueOd = {MQOD_DEFAULT};    /* Object Descriptor             */
    MQLONG   QueueOpenCode;               /* MQOPEN completion code        */
    MQLONG   QueueCompCode;               /* MQOPEN completion code        */
    MQLONG   QueueReason;                 /* reason code                   */
    MQGMO    QueueGmo = {MQGMO_DEFAULT};   /* get message options           */
    MQMD     ReceiveMd = {MQMD_DEFAULT};    /* Message Descriptor            */

    MQBYTE   RecBuffer[4000];            /* message buffer                */
    MQLONG   RecBuffLen;                 /* buffer length                 */
    MQLONG   MessLen;                /* message length received       */

    char clMsgId[30] = "\0";
    char clMyBuffer[4000] = "\0";
    char clMyTime[20] = "\0";
    short slFkt = 0;
    short slCursor = 0;
    char clData[100] = "\0";
    char clUrno[12] = "\0";
    char clNowTime[20] = "\0";
    char clSqlBuf[4500] ="\0";
    static char clAlert[256] = "\0";


/* CONNECT TO QMANAGER */
    if(strlen(cgQMgrReceive) == 0)
    {
	dbg(TRACE,"%05d: Receive-QManager not configured",__LINE__);
    }
    else
    {
	while(bgConnected == FALSE)
	{
		sleep(atoi(cgReconnectMq));
		ilRC = ConnectMQ(cgQMgrReceive, &HconRecBrowse);
		ilRC = ConnectMQ(cgQMgrReceive, &HconRecGet);

	}
    
    
/* OPEN QUEUE FOR BROWSING */
	OpenBrowseOptions = MQOO_INPUT_AS_Q_DEF    /* open queue for input      */
	    + MQOO_BROWSE            /* open for browsing */
	    + MQOO_FAIL_IF_QUIESCING;              /* but not if MQM stopping   */

	strcpy(QueueBrowseOd.ObjectName,cgMqReceive); 	     
    
	QueueBrowseReason = 1111; /* != MQRC_NONE */
	QueueBrowseOpenCode = MQCC_FAILED;
    
	while ((QueueBrowseReason != MQRC_NONE) || (QueueBrowseOpenCode == MQCC_FAILED))
	{
	    MQOPEN(HconRecBrowse,                      /* connection handle            */
		   &QueueBrowseOd,                /* object descriptor for queue  */
		   OpenBrowseOptions,                 /* open options                 */
		   &QueueBrowseHobj,                     /* object handle                */
		   &QueueBrowseOpenCode,                 /* completion code              */
		   &QueueBrowseReason);                  /* reason code                  */

	    if (QueueBrowseReason != MQRC_NONE)
	    {
		dbg(TRACE,"%05d: MQOPEN ended with reason code <%d>",__LINE__, QueueBrowseReason);
		if(strcmp(cgUseAlerter,"YES") == 0)
		{
			sprintf(clAlert,"<%s>: MQOPEN ended with reason code <%d> while connecting to Queue <%s>",cgContextAlert,QueueBrowseReason,cgMqReceive);
			ilRC = AddAlert2(cgNameAlert," "," ","E",clAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
		}
		else
		{
			dbg(TRACE,"%05d: !!!!!!!!!! ERROR WHILE opening queue !!!!!!!!!!!!!!!",__LINE__);
		}


		ilRC = RC_FAIL;
	    }

	    if (QueueBrowseOpenCode == MQCC_FAILED)
	    {
		dbg(TRACE,"%05d: unable to open receive-queue",__LINE__);
		ilRC = RC_FAIL;
	    }

	    if (ilRC == RC_FAIL)
	    {
		dbg(TRACE,"%05d: waiting <%s> seconds before try to reconnect",__LINE__,cgReconnectMq);
		sleep(atoi(cgReconnectMq));
	    }
	    else
	    {
		dbg(TRACE,"%05d: Queue <%s> opened for browsing",__LINE__,cgMqReceive);
	    }
	} /* end of while (QueueBrowseReason.......*/

/* OPEN QUEUE FOR Receiving with GET (and delete item on queue) */
	OpenOptions = MQOO_INPUT_AS_Q_DEF    /* open queue for input      */
	    +  MQOO_FAIL_IF_QUIESCING;              /* but not if MQM stopping   */

	strcpy(QueueOd.ObjectName,cgMqReceive); 	     
    
	QueueReason = 1111; /* != MQRC_NONE */
	QueueOpenCode = MQCC_FAILED;
    
	while ((QueueReason != MQRC_NONE) || (QueueOpenCode == MQCC_FAILED))
	{
	    MQOPEN(HconRecGet,                      /* connection handle            */
		   &QueueOd,                /* object descriptor for queue  */
		   OpenOptions,                 /* open options                 */
		   &QueueHobj,                     /* object handle                */
		   &QueueOpenCode,                 /* completion code              */
		   &QueueReason);                  /* reason code                  */

	    if (QueueReason != MQRC_NONE)
	    {
		dbg(TRACE,"%05d: MQOPEN ended with reason code <%d>",__LINE__, QueueReason);

		if(strcmp(cgUseAlerter,"YES") == 0 )
		{
			sprintf(clAlert,"<%s>: MQOPEN ended with reason code <%d> while connecting to Queue <%s>",cgContextAlert,QueueReason,cgMqReceive);
			ilRC = AddAlert2(cgNameAlert," "," ","E",clAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
		}
		else
		{
			dbg(TRACE,"%05d: !!!!!!!!!! ERROR WHILE opening queue !!!!!!!!!!!!!!!",__LINE__);
		}


		ilRC = RC_FAIL;
	    }

	    if (QueueOpenCode == MQCC_FAILED)
	    {
		dbg(TRACE,"%05d: unable to open receive-queue",__LINE__);
		ilRC = RC_FAIL;
	    }

	    if (ilRC == RC_FAIL)
	    {
		dbg(TRACE,"%05d: waiting <%s> seconds before try to reconnect",__LINE__,cgReconnectMq);
		sleep(atoi(cgReconnectMq));
	    }
	    else
	    {
		dbg(TRACE,"%05d: Queue <%s> opened for receive and delete item",__LINE__,cgMqReceive);
	    }
	} /* end of while (QueueReason.......*/


/* NOW LOOP FOR RECEIVING AND SENDING THE DATA FROM THE MQ-QUEUE */


	for(;;)
	{
/* NOW GET FIRST ITEM ON QUEUE WITH BROWSE, SO THE ITEM WILL NOT BE DELETED FROM THE QUEUE */

	    dbg(TRACE,"%05d: =================Wait for next message on Queue <%s>==============",__LINE__,cgMqReceive);
	    QueueBrowseGmo.Options = MQGMO_WAIT
		+ MQGMO_FAIL_IF_QUIESCING
		+ MQGMO_BROWSE_FIRST
		+ MQGMO_ACCEPT_TRUNCATED_MSG;
	    /*QueueBrowseGmo.WaitInterval = MQWI_UNLIMITED;*/       /* no limit for waiting     */

	    QueueBrowseGmo.WaitInterval = atol(cgWaitInterval);
	    dbg(TRACE,"%05d: MQGET-WaitInterval = <%d>",__LINE__, atol(cgWaitInterval));
	    if (atol(cgWaitInterval) == 0)
	    {
		dbg(TRACE,"%05d: MQGET-WaitInterval = 0 ----> set WaitInterval to 60000");
		QueueBrowseGmo.WaitInterval = 60000;
	    }
		if(atol(cgWaitInterval) == -1)
		{
			QueueBrowseGmo.WaitInterval = MQWI_UNLIMITED;
			dbg(TRACE,"%05d: MQGET-WaitInterval = MQWI_UNLIMITED",__LINE__);
		}

	    RecBuffLen = sizeof(RecBuffer) -1;
    
	    memcpy(ReceiveBrowseMd.MsgId, MQMI_NONE, sizeof(ReceiveBrowseMd.MsgId));
	    memcpy(ReceiveBrowseMd.CorrelId, MQCI_NONE, sizeof(ReceiveBrowseMd.CorrelId));
    
	    do
		{
	    do 
	    {
		MQGET(HconRecBrowse,                /* connection handle                 */
		      QueueBrowseHobj,                /* object handle                     */
		      &ReceiveBrowseMd,                 /* message descriptor                */
		      &QueueBrowseGmo,                /* get message options               */
		      RecBuffLen,              /* buffer length                     */
		      RecBuffer,              /* message buffer                    */
		      &MessLen,            /* message length                    */
		      &QueueBrowseCompCode,           /* completion code                   */
		      &QueueBrowseReason);            /* reason code                       */

		/* CST: 180708 */
		if (IsRecoverable(QueueBrowseReason) == FALSE)
		{
			Terminate(10);
		}
		if(QueueBrowseReason == 2033)
		{
		    dbg(TRACE,"%05d: MQGET ended with Reasoncode: <%d>",__LINE__,QueueBrowseReason);
			if(strcmp(cgUseAlerter,"YES") == 0 )
			{
				sprintf(clAlert,"<%s>: No message available ",cgContextAlert);
				ilRC = AddAlert2(cgNameAlert," "," ","E",clAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");		}
			else
			{
				dbg(TRACE,"%05d: !!!!!!!!!! ERROR WHILE RECEIVING MESSAGE !!!!!!!!!!!!!!!",__LINE__);
			}
		}

	    }while (QueueBrowseReason == 2033); /*timout-error*/

	    if (QueueBrowseReason == MQRC_NONE)
	    {
	  igRetryCount = 0;
		strcpy(clMyTime,ReceiveBrowseMd.PutDate);
		clMyTime[16] = '\0';
		RecBuffer[MessLen] = '\0';
		{
		/* Replace '\0' in the message by ' ' because the message will be handled
		   as string in further processing */
			 int ilLc;

			 for(ilLc = 0; ilLc < MessLen; ilLc++)
			 {
			 	if (RecBuffer[ilLc] == '\0')
				{
					RecBuffer[ilLc] = ' ';
				}
			}
		}
		dbg(TRACE,"%05d: MQGET (browse) received Message: <%s> Time: <%s>, Reason: <%d>",__LINE__,RecBuffer,clMyTime,QueueBrowseReason);

	    }
	    else
	    {
		dbg(TRACE,"%05d: MQGET ended with Reasoncode: <%d>",__LINE__,QueueBrowseReason);
		dbg(TRACE,"%05d: MQGET received Message: <%s> Time: <%s>",__LINE__,RecBuffer,clMyTime);
		if(strcmp(cgUseAlerter,"YES") == 0 )
		{
			sprintf(clAlert,"<%s>: MQGET ended with Reasoncode: <%d> ",cgContextAlert,QueueBrowseReason);
			ilRC = AddAlert2(cgNameAlert," "," ","E",clAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
		}
		else
		{
			dbg(TRACE,"%05d: !!!!!!!!!! ERROR WHILE RECEIVING MESSAGE !!!!!!!!!!!!!!!",__LINE__);
		}

		if(QueueBrowseReason != 2079)
		{
		    if(QueueBrowseReason == 2016)
		    {
			dbg(TRACE,"%05d: GetInhibited, waiting 120 seconds for retry",__LINE__);
			sleep(120);
		    }
		    else
		    {
			Terminate(1);
		    }
		}

	    }
		}
		while (QueueBrowseReason != MQRC_NONE && QueueBrowseReason != 2079);
	    sprintf(clMyBuffer,"%s",RecBuffer);

/* INSERT MESSAGE INTO MQRTAB */

		if(strcmp(cgDbLogging,"ON") == 0)
		{
			ilRC = GetNextValues(clUrno,1);
			TimeToStr(clNowTime,time(NULL));
			ConvertClientStringToDb(clMyBuffer);
		
			sprintf(clSqlBuf,"INSERT INTO MQRTAB (URNO,INAM,TPUT,DATA,COUN,STAT,REAS,LSTU) VALUES (%d,'%s','%s','%s',%d,'%s',%d,'%s')",
					atol(clUrno),mod_name,clMyTime,clMyBuffer,1,"RECEIVED",QueueBrowseReason,clNowTime);
			dbg(DEBUG,"%05d: clSqlBuf: <%s>",__LINE__,clSqlBuf);
			slFkt = START;
			slCursor = 0;
			ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clData);
			if (ilRC != RC_SUCCESS)
			{
				dbg(TRACE,"sql_if failed###################");
			}
			commit_work();
			close_my_cursor (&slCursor);
		    
			ConvertDbStringToClient(clMyBuffer);
		}

/* SEND DATA TO CEDA */
	    if((atoi(cgCedaSend) != 0) && (QueueBrowseReason != 2016))
	    {
			ilRC = SendToCeda(clMyBuffer,clMyTime,QueueBrowseReason,clUrno);
	    }

/* NOW GET FIRST ITEM ON QUEUE AND DELETE IT FROM THE QUEUE */
    
	    QueueGmo.Options = MQGMO_NO_WAIT
		+ MQGMO_FAIL_IF_QUIESCING
		+ MQGMO_ACCEPT_TRUNCATED_MSG;
    
	    RecBuffLen = sizeof(RecBuffer) -1;
    
	    memcpy(ReceiveMd.MsgId, MQMI_NONE, sizeof(ReceiveMd.MsgId));
	    memcpy(ReceiveMd.CorrelId, MQCI_NONE, sizeof(ReceiveMd.CorrelId));
		do
		{
	    MQGET(HconRecGet,                /* connection handle                 */
		  QueueHobj,                /* object handle                     */
		  &ReceiveMd,                 /* message descriptor                */
		  &QueueGmo,                /* get message options               */
		  RecBuffLen,              /* buffer length                     */
		  RecBuffer,              /* message buffer                    */
		  &MessLen,            /* message length                    */
		  &QueueCompCode,           /* completion code                   */
		  &QueueReason);            /* reason code                       */

		/* CST: 180708 */
		if (IsRecoverable(QueueReason) == FALSE)
		{
			Terminate(10);
		}

	    if (QueueReason == MQRC_NONE)
	    {
	  igRetryCount = 0;
		RecBuffer[MessLen] = '\0';
		dbg(DEBUG,"%05d: MQGET received and deleted Message: <%s>",__LINE__,RecBuffer);
		dbg(TRACE,"%05d: MQGET received and deleted Message from Queue",__LINE__);
	    }
	    else
	    {

		dbg(TRACE,"%05d: MQGET ended with Reasoncode: <%d>",__LINE__,QueueReason);
		dbg(TRACE,"%05d: MQGET received and deleted Message from MQ: <%s>",__LINE__,RecBuffer);
		if(strcmp(cgUseAlerter,"YES") == 0 )
		{
			sprintf(clAlert,"<%s>: MQGET ended with Reasoncode: <%d> ",cgContextAlert,QueueReason);
			ilRC = AddAlert2(cgNameAlert," "," ","E",clAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
		}
		else
		{
			dbg(TRACE,"%05d: !!!!!!!!!! ERROR WHILE RECEIVING MESSAGE !!!!!!!!!!!!!!!",__LINE__);
		}

		if(QueueReason != 2079)
		{
		    if(QueueReason == 2016)
		    {
			dbg(TRACE,"%05d: GetInhibited, waiting 120 seconds for retry",__LINE__);
			sleep(120);
		    }
		    else
		    {
				if(QueueReason != 2033)	
				{
					Terminate(1);
				}
		    }
		}
	    }
		}
		while (QueueReason != MQRC_NONE && QueueReason != 2079);
	} /* end of for(;;) */

    } /* end of if(strlen(Receive-Qmanager.....)) */
	 return ilRC;


}


static int SendToCeda(char *pcpBuffer,char *pcpMyTime,long plpMyReason, char *pcpUrno) 
{
    int ilRC = RC_SUCCESS;
    short slFkt = 0;
    short slCursor = 0;
    char clData[100] = "\0";
    char clUrno[12] = "\0";
    char clRecUrno[12] = "\0";
    int ilCount = 0;
    int ilAckRC = RC_FAIL;
    char clNowTime[20] = "\0";
    char clSqlBuf[4500] ="\0";
    char clTwStart[30] = "\0";
    char clTwEnd[30] = "\0";
    int i = 0;
    BC_HEAD *prlBchead       = NULL;
    CMDBLK  *prlCmdblk       = NULL;
    char    *pclSelection    = NULL;
    char    *pclFields       = NULL;
    char    *pclData         = NULL;
    char    *pclRow          = NULL;

    strcpy(clUrno,pcpUrno);
    TimeToStr(clNowTime,time(NULL));


    dbg(TRACE,"%05d: -----SendToCeda start -----",__LINE__);
    dbg(DEBUG,"%05d: pcpBuffer: <%s>, pcpMyTime: <%s>, plpMyReason: <%d>, pcpUrno: <%s>",__LINE__,pcpBuffer,pcpMyTime,plpMyReason,pcpUrno);


/* NOW SEND EVENT TO CEDA */

	sprintf(clTwStart,"%s,%s",mod_name,clUrno);
	sprintf(clTwEnd,"%s,%s,%s",cgHopo,cgTabEnd,mod_name);
	dbg(TRACE,"Now Send Event to <%d>",atoi(cgCedaSend));
	if (strlen(cgSelection) > 0)
	{
		ilRC = SendCedaEvent(atoi(cgCedaSend),0,mod_name,"TOOL",clTwStart,clTwEnd,cgCedaSendCommand,"",cgSelection,"",pcpBuffer,"",atoi(cgCedaSendPrio),RC_SUCCESS);
	}
	else
	{
		ilRC = SendCedaEvent(atoi(cgCedaSend),0,mod_name,"TOOL",clTwStart,clTwEnd,cgCedaSendCommand,"","","",clUrno,"",atoi(cgCedaSendPrio),RC_SUCCESS);
	}
	if (ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"%05d: command <%s> could not be sent to <%s>, MQRTAB-Urno: <%s>",__LINE__,cgCedaSendCommand,cgCedaSend,clUrno);

	}
	
	if(strcmp(cgDbLogging,"ON") == 0)
	{
		if(strlen(cgSelection) > 0)
		{
			sprintf(clSqlBuf,"UPDATE MQRTAB SET STAT='OK', LSTU='%s', COUN = 0 WHERE URNO=%d",clNowTime,atol(clUrno));
		}
		else
		{
			sprintf(clSqlBuf,"UPDATE MQRTAB SET STAT='IN WORK', LSTU='%s', COUN = 0 WHERE URNO=%d",clNowTime,atol(clUrno));
		}
	}



/* MRK MESSAGE IN DB  */
	if(strcmp(cgDbLogging,"ON") == 0)
	{
		TimeToStr(clNowTime,time(NULL));
		dbg(DEBUG,"%05d: Mark Message in DB clSqlBuf: <%s>",__LINE__,clSqlBuf);
		slFkt = START;
		slCursor = 0;
		ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clData);
		if (ilRC != RC_SUCCESS)
		{
		dbg(TRACE,"sql_if failed###################");
		}
		commit_work();
		close_my_cursor (&slCursor);
	}
    dbg(TRACE,"%05d: -----SendToCeda end -----",__LINE__);

    return ilRC;
}



static int TimeToStr(char *pcpTime,time_t lpTime)
{
    struct tm *_tm;
    char   _tmpc[6];

    /*_tm = (struct tm *)localtime(&lpTime); */
    _tm = (struct tm *)gmtime(&lpTime);
    /*      strftime(pcpTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",_tm); */


    sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
            _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
            _tm->tm_min,_tm->tm_sec);

    return RC_SUCCESS;

}     /* end of TimeToStr */


static int CheckIfMessageInWork()
{
    int ilRC = RC_SUCCESS;
    int ilRC2 = RC_FAIL;
    short slFkt = 0;
    short slCursor = 0;
    char clData[4500] = "\0";
    char clUrno[12] = "\0";
    char clRecUrno[12] = "\0";
    char clReas[8] = "\0";
    char clTput[18] = "\0";
    int ilCount = 1;
    int ilAckRC = RC_FAIL;
    char clNowTime[20] = "\0";
    char clSqlBuf[4500] ="\0";
    int ilLen = 0;


    dbg(TRACE,"-----CheckIfMessageInWork start -----");
    while (ilRC == RC_SUCCESS)
    {
	slFkt = START;
	slCursor = 0;

	sprintf(clSqlBuf,"SELECT URNO,DATA,REAS,TPUT FROM MQRTAB WHERE STAT ='RECEIVED' and INAM='%s'",mod_name);

	ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clData);
	
	commit_work();
	close_my_cursor (&slCursor);
	
	if(ilRC == RC_SUCCESS)
	{
	    BuildItemBuffer (clData, "", 4, ",");
	    strcpy(clSqlBuf,clData); /* clSqlBuf wird hier nur temporaer benutzt, hat nichts mit sql zu tun */
	    
	    ilLen = get_real_item (clUrno, clSqlBuf, 1);
	    ilLen = get_real_item (clData, clSqlBuf, -2);
	    ilLen = get_real_item (clReas, clSqlBuf, 3);
	    ilLen = get_real_item (clTput, clSqlBuf, 4);
	    ConvertDbStringToClient(clData);
	    dbg(TRACE,"%05d: Search for IN WORK return ilRC: <%d>, clUrno <%s>, clData: <%s>, clReas: <%s>, clTput: <%s>",__LINE__,ilRC,clUrno,clData,clReas,clTput);
	    ilRC2 = SendToCeda(clData,clTput,atol(clReas),clUrno);

	}
    }


    dbg(TRACE,"-----CheckIfMessageInWork end -----");
	
    return ilRC;
}




static int OpenSendMQ(int ipQueueNumber)
{
    int ilRC = RC_SUCCESS;
    MQOD od = {MQOD_DEFAULT};
    MQLONG   O_options;              
    MQLONG   QueueOpenCode;               /* completion code                 */
    MQLONG   QueueReason;                 /* reason code                     */
    static char clAlert[256] = "\0";

		if(bgConnected == FALSE)
		{
			ilRC = ConnectMQ(cgQMgrSend, &HconSend);
		}
    strcpy(od.ObjectName, rgMyQueues[ipQueueNumber].cgQName);
    
    O_options = MQOO_OUTPUT         /* open queue for output          */
	+ MQOO_FAIL_IF_QUIESCING; /* but not if MQM stopping    */
    QueueReason = 1111; /* != MQRC_NONE */
    QueueOpenCode = MQCC_FAILED;
    

	MQOPEN (HconSend, &od, O_options, &rgMyQueues[ipQueueNumber].HobjSend, &QueueOpenCode, &QueueReason);

	if (QueueReason != MQRC_NONE)
	{
	    dbg(TRACE,"%05d: MQOPEN ended with reason code <%d>",__LINE__, QueueReason);
	    
	    /* CST: 180708*/
			if (IsRecoverable(QueueReason) == FALSE )
			{
				  dbg(TRACE,"Connection broken, try to reconnect at next message");
			  	bgConnected = FALSE;
			}
		if(strcmp(cgUseAlerter,"YES") == 0 )
		{
			sprintf(clAlert,"<%s>: MQOPEN ended with reason code <%d> while connecting to Queue <%s>",cgContextAlert,QueueReason,rgMyQueues[ipQueueNumber].cgQName);
			ilRC = AddAlert2(cgNameAlert," "," ","E",clAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
		}
		else
		{
			dbg(TRACE,"%05d: !!!!!!!!!! ERROR WHILE Open Queue <%s> !!!!!!!!!!!!!!!",__LINE__,rgMyQueues[ipQueueNumber].cgQName);
		}


	    ilRC = RC_FAIL;
	}
	else
	{
		igRetryCount = 0;
	}

	if (QueueOpenCode == MQCC_FAILED)
	{
	    dbg(TRACE,"%05d: unable to open send-queue <%s>",__LINE__,rgMyQueues[ipQueueNumber].cgQName);
	    ilRC = RC_FAIL;
	}

	if (ilRC == RC_FAIL)
	{
	    /*dbg(TRACE,"%05d: waiting <%s> seconds before try to reconnect",__LINE__,cgReconnectMq);*/
	    /*sleep(atoi(cgReconnectMq));*/
	}
	else
	{
	    dbg(TRACE,"%05d: Queue <%s> opened for sending to MQ",__LINE__,rgMyQueues[ipQueueNumber].cgQName);
	}

    return ilRC;
}





void myinitialize(int argc,char *argv[])
{
    mod_name = argv[0];
    /* 20020822 JIM: if argv[0] contains path (i.e. started by ddd), skip path */
    if (strrchr(mod_name,'/')!= NULL)
    {
	mod_name= strrchr(mod_name,'/');
	mod_name++;
    }
    if(gChildPid == 0)
    {
        sprintf(__CedaLogFile,"%s/%sc%.5ld.log",getenv("DBG_PATH"),mod_name,getpid());
    }
    else
    {
        sprintf(__CedaLogFile,"%s/%sp%.5ld.log",getenv("DBG_PATH"),mod_name,getpid());
    }
    outp = fopen(__CedaLogFile,"w");
    if (argc > 1)
    {
	mod_id = atoi(argv[1]);
	ctrl_sta = atoi(argv[2]);
    } else {
	mod_id = 0;
	ctrl_sta = 1;
    }
    glbrc = init();
    if(glbrc)
    {
	fprintf(outp,"\n<%s><%ld>STHINIT failed: ret = %d\n",mod_name,getpid(),glbrc);
	exit(1);
    }
}



static int SendToMq(char *pcpData, int ipQueueNumber)
{
    int ilRC = RC_SUCCESS;
    char clMyTime[30] = "\0";
    char clNowTime[30] = "\0";
    short slFkt = 0;
    short slCursor = 0;
    char clUrno[20] = "\0";
    char clSqlBuf[4500] = "\0";
    char clData[4500] = "\0";
    char clAlert[256] = "\0";


    MQMD     md = {MQMD_DEFAULT};    /* Message Descriptor              */
    MQLONG   CompCode;               /* completion code                 */
    MQLONG   Reason;                 /* reason code                     */
    MQPMO    pmo = {MQPMO_DEFAULT};   /* put message options             */

    dbg(TRACE,"-----SendToMq start -----");

/* INSERT MESSAGE INTO MQSTAB */
	if(strcmp(cgDbLogging,"ON") == 0)
	{
		ilRC = GetNextValues(clUrno,1);


		if(rgMyQueues[ipQueueNumber].igPutFailed == TRUE)
		{
			if((strcmp(cgDbLogging,"ON") == 0) && (strcmp(cgRecovery,"ON") == 0))
			{
				ilRC = SendFailedMessages(ipQueueNumber);
			}
		}


		TimeToStr(clNowTime,time(NULL));
		ConvertClientStringToDb(pcpData);

		if(strcmp(cgRecovery,"ON") == 0)
		{
			if(igUseQName == FALSE)
			{
				sprintf(clSqlBuf,"INSERT INTO MQSTAB (URNO,INAM,DATA,STAT,LSTU) VALUES (%d,'%s','%s','%s','%s')",
						atol(clUrno),mod_name,pcpData,"TO SEND",clNowTime);
			}
			else
			{
				sprintf(clSqlBuf,"INSERT INTO MQSTAB (URNO,INAM,DATA,STAT,LSTU) VALUES (%d,'%s','%s','%s','%s')",
						atol(clUrno),rgMyQueues[ipQueueNumber].cgQName,pcpData,"TO SEND",clNowTime);
			}
		}
		else
		{
			if(igUseQName == FALSE)
			{
				sprintf(clSqlBuf,"INSERT INTO MQSTAB (URNO,INAM,DATA,STAT,LSTU) VALUES (%d,'%s','%s','%s','%s')",
						atol(clUrno),mod_name,pcpData,"FAILED",clNowTime);
			}
			else
			{
				sprintf(clSqlBuf,"INSERT INTO MQSTAB (URNO,INAM,DATA,STAT,LSTU) VALUES (%d,'%s','%s','%s','%s')",
						atol(clUrno),rgMyQueues[ipQueueNumber].cgQName,pcpData,"FAILED",clNowTime);
			}

		}

		ConvertDbStringToClient(pcpData);

		dbg(DEBUG,"%05d: clSqlBuf: <%s>",__LINE__,clSqlBuf);
		slFkt = START;
		slCursor = 0;
		ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clData);
		if (ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"sql_if failed###################");
		}
		commit_work();
		close_my_cursor (&slCursor);

	}

    if (rgMyQueues[ipQueueNumber].igPutFailed == FALSE)
    {
		/* PUT MESSAGE TO MQ QUEUE */

		memcpy(md.Format,           
			MQFMT_STRING, MQ_FORMAT_LENGTH);
		memcpy(md.MsgId, MQMI_NONE, sizeof(md.MsgId));
		if(rgMyQueues[ipQueueNumber].igQDynPrio == -1)
		{
			md.Priority = rgMyQueues[ipQueueNumber].igQPrio;
		}
		else
		{
			md.Priority = rgMyQueues[ipQueueNumber].igQDynPrio;
		}

		pmo.Options = MQPMO_NEW_MSG_ID
			+ MQPMO_FAIL_IF_QUIESCING;

		dbg(TRACE,"%05d: Send to Queue: <%s> pcpData <%s>, length <%d>",__LINE__,rgMyQueues[ipQueueNumber].cgQName,pcpData,strlen(pcpData));
		MQPUT(HconSend,                /* connection handle               */
			rgMyQueues[ipQueueNumber].HobjSend,          /* object handle                   */
			&md,                 /* message descriptor              */
			&pmo,                /* default options                 */
			strlen(pcpData),              /* buffer length                   */
			pcpData,              /* message buffer                  */
			&CompCode,           /* completion code                 */
			&Reason);            /* reason code                     */

		strcpy(clMyTime,md.PutDate);
		clMyTime[16] = '\0';
		/* report reason, if any */
		dbg(TRACE,"MQPUT ended with reason code <%d>, Time: <%s>", Reason, clMyTime);

		if(Reason == 2009)
		{
			Terminate(10);
		}
		if (Reason == MQRC_NONE)
		{
			if(strcmp(cgDbLogging,"ON") == 0)
			{
				sprintf(clSqlBuf,"UPDATE MQSTAB SET STAT='OK', LSTU='%s',TPUT = '%s' WHERE URNO=%d",clNowTime,clMyTime,atol(clUrno));
				dbg(DEBUG,"%05d: Mark Message  ",__LINE__);
				dbg(DEBUG,"%05d: clSqlBuf: <%s>",__LINE__,clSqlBuf);
				slFkt = START;
				slCursor = 0;
				ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clData);
				if (ilRC != RC_SUCCESS)
				{
				dbg(TRACE,"sql_if failed###################");
				}
				commit_work();
				close_my_cursor (&slCursor);
			}

		}
		else
		{
		if(strcmp(rgMyQueues[ipQueueNumber].cgUseAlerter,"YES") == 0 )
		{
			sprintf(clAlert,"<%s>: MQPUT ended with reason code <%d> ",rgMyQueues[ipQueueNumber].cgContextAlert,Reason);
			ilRC = AddAlert2(rgMyQueues[ipQueueNumber].cgNameAlert," "," ","E",clAlert,pcpData,TRUE,FALSE,"ALERT",rgMyQueues[ipQueueNumber].cgQName," "," ");
		}
		else
		{
			dbg(TRACE,"%05d: !!!!!!!!!! ERROR WHILE SENDING MESSAGE !!!!!!!!!!!!!!!",__LINE__);
		}

			
			if(strcmp(cgDbLogging,"ON") == 0)
			{

				if (strcmp(cgRecovery,"ON") == 0)
				{
					ilRC = MarkToSend(clUrno,ipQueueNumber);
					rgMyQueues[ipQueueNumber].igPutFailed = TRUE;
					rgMyQueues[ipQueueNumber].bgQConnected = FALSE;
					if(rgMyQueues[ipQueueNumber].igPutFailedOld == FALSE)
					{
						strcpy(rgMyQueues[ipQueueNumber].cgFirstFailedUrno,clUrno);
						strcpy(rgMyQueues[ipQueueNumber].cgFirstFailedData,pcpData);
					}
				}
			}
		}
	}/*geh�rt zu (rgMyQueues[ipQueueNumber].igPutFailed == FALSE)*/
	else
	{
		if (strcmp(cgRecovery,"ON") == 0)
		{
			MarkToSend(clUrno,ipQueueNumber);
		}
	}


    dbg(TRACE,"-----SendToMq end -----");

    return ilRC;
}




static int SendFailedMessages(int ipQueueNumber)
{
    int ilRC = RC_SUCCESS;
    int ilRC2 = RC_SUCCESS;
    short slFkt = 0;
    short slCursor = 0;
    short slFkt2 = 0;
    short slCursor2 = 0;
    char clSqlBuf[4500] = "\0";
    char clData[4500] = "\0";
    int ilLen = 0;
    char clUrno[20] ="\0";
    char clMyTime[30] ="\0";
    char clNowTime[20] ="\0";
    char clAlert[256] = "\0";


    MQMD     md = {MQMD_DEFAULT};    /* Message Descriptor              */
    MQLONG   CompCode;               /* completion code                 */
    MQLONG   Reason;                 /* reason code                     */
    MQPMO    pmo = {MQPMO_DEFAULT};  /* put message options             */

    dbg(TRACE,"-----SendFailedMessages start -----");

    slFkt = START;
    slCursor = 0;

	ilRC = OpenSendMQ(ipQueueNumber);

    while (ilRC == RC_SUCCESS)
    {
		if(strlen(rgMyQueues[ipQueueNumber].cgFirstFailedUrno) == 0)
		{
			if(igUseQName == FALSE)
			{
				sprintf(clSqlBuf,"SELECT URNO, DATA FROM MQSTAB WHERE STAT = 'TO SEND' AND INAM ='%s' ORDER BY COUN",mod_name);
			}
			else
			{
				sprintf(clSqlBuf,"SELECT URNO, DATA FROM MQSTAB WHERE STAT = 'TO SEND' AND INAM ='%s' ORDER BY COUN",rgMyQueues[ipQueueNumber].cgQName);
			}
			ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clData);
			if (ilRC != RC_SUCCESS)
			{
				dbg(TRACE,"%05d: sql_if returnd ilRC: <%d> probably no Data TO SEND found",__LINE__,ilRC);
				lgCount = 0;
			}
			slFkt = NEXT;
		
			BuildItemBuffer (clData, "", 2, ",");
			strcpy(clSqlBuf,clData); /* clSqlBuf wird hier nur temporaer benutzt, hat nichts mit sql zu tun */
		    
			ilLen = get_real_item (clUrno, clSqlBuf, 1);
			ilLen = get_real_item (clData, clSqlBuf, -2);
		}
		else
		{
			strcpy(clUrno,rgMyQueues[ipQueueNumber].cgFirstFailedUrno);
			strcpy(clData,rgMyQueues[ipQueueNumber].cgFirstFailedData);
		}
		if (ilRC == RC_SUCCESS)
		{
	    
		/* PUT MESSAGE TO MQ QUEUE */
		memcpy(md.Format, MQFMT_STRING, MQ_FORMAT_LENGTH);
	    memcpy(md.MsgId, MQMI_NONE, sizeof(md.MsgId));
		
		if(rgMyQueues[ipQueueNumber].igQDynPrio == -1)
		{
			md.Priority = rgMyQueues[ipQueueNumber].igQPrio;
		}
		else
		{
			md.Priority = rgMyQueues[ipQueueNumber].igQDynPrio;
		}

	    pmo.Options = MQPMO_NEW_MSG_ID
		+ MQPMO_FAIL_IF_QUIESCING;
	    ConvertDbStringToClient(clData);
	    dbg(TRACE,"%05d: Send to Queue: <%s> clData <%s>, length <%d>",__LINE__,rgMyQueues[ipQueueNumber].cgQName,clData,strlen(clData));
	    MQPUT(HconSend,                /* connection handle               */
		  rgMyQueues[ipQueueNumber].HobjSend,          /* object handle                   */
		  &md,                 /* message descriptor              */
		  &pmo,                /* default options                 */
		  strlen(clData),              /* buffer length                   */
		  clData,              /* message buffer                  */
		  &CompCode,           /* completion code                 */
		  &Reason);            /* reason code                     */

	    strcpy(clMyTime,md.PutDate);
	    clMyTime[16] = '\0';
	    /* report reason, if any */
	    dbg(TRACE,"%05d: MQPUT ended with reason code <%d>, Time: <%s>",__LINE__, Reason, clMyTime);

		if(Reason == 2009)
		{
			Terminate(10);
		}

	    if (Reason == MQRC_NONE)
	    {    
			TimeToStr(clNowTime,time(NULL));
			sprintf(clSqlBuf,"UPDATE MQSTAB SET COUN = 0, STAT='OK', LSTU='%s',TPUT = '%s' WHERE URNO=%s",clNowTime,clMyTime,clUrno);
			slFkt2 = START;
			slCursor2 = 0;
			ilRC2 = sql_if (slFkt2, &slCursor2, clSqlBuf, clData);
			if (ilRC2 != RC_SUCCESS)
			{
				dbg(TRACE,"%05d: clSqlBuf: <%s>",__LINE__,clSqlBuf);
				dbg(TRACE,"%05d: sql_if returnd ilRC: <%d>",__LINE__,ilRC2);
			}
			commit_work();
			close_my_cursor (&slCursor2);
			rgMyQueues[ipQueueNumber].igPutFailed = FALSE;
			rgMyQueues[ipQueueNumber].cgFirstFailedUrno[0] = '\0';
			rgMyQueues[ipQueueNumber].cgFirstFailedData[0] = '\0';
	    }
	    else
	    {
			if(strcmp(rgMyQueues[ipQueueNumber].cgUseAlerter,"YES") == 0 )
			{
				sprintf(clAlert,"<%s>: MQPUT ended with reason code <%d> ",rgMyQueues[ipQueueNumber].cgContextAlert,Reason);
				ilRC = AddAlert2(rgMyQueues[ipQueueNumber].cgNameAlert," "," ","E",clAlert,clData,TRUE,FALSE,"ALERT",rgMyQueues[ipQueueNumber].cgQName," "," ");
			}
			else
			{
				dbg(TRACE,"%05d: !!!!!!!!!! ERROR WHILE SENDING MESSAGE !!!!!!!!!!!!!!!",__LINE__);
			}

			ilRC = RC_FAIL;
			rgMyQueues[ipQueueNumber].igPutFailed = TRUE;

			if(rgMyQueues[ipQueueNumber].igPutFailed == FALSE)
			{
				strcpy(rgMyQueues[ipQueueNumber].cgFirstFailedUrno,clUrno);
				strcpy(rgMyQueues[ipQueueNumber].cgFirstFailedData,clData);
			}
	    }
	}
    }/*end of while(ilRC == RC_SUCCESS)*/
    close_my_cursor (&slCursor);

    dbg(TRACE,"-----SendFailedMessages end -----");

    return rgMyQueues[ipQueueNumber].igPutFailed;
} /*end of SendFailedMessages*/


static int MarkToSend(char *pcpUrno, int ipQueueNumber)
{
    int ilRC = RC_SUCCESS;
    char clNowTime[30] = "\0";
    short slFkt = 0;
    short slCursor = 0;
    char clSqlBuf[4500] = "\0";
    char clData[50] = "\0";

    dbg(TRACE,"-----MarkToSend start -----");
	if(lgCount == -1)
	{
		if(igUseQName == FALSE)
		{
			sprintf(clSqlBuf,"SELECT MAX(COUN) FROM MQSTAB WHERE INAM ='%s'",mod_name);
		}
		else
		{
			sprintf(clSqlBuf,"SELECT MAX(COUN) FROM MQSTAB WHERE INAM ='%s'",rgMyQueues[ipQueueNumber].cgQName);
		}
		slFkt = START;
		slCursor = 0;
		ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clData);
		if (ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"sql_if failed###################");
		}
		commit_work();
		close_my_cursor (&slCursor);
    
		lgCount = atol(clData);
	}
	lgCount++;
    TimeToStr(clNowTime,time(NULL));

    sprintf(clSqlBuf,"UPDATE MQSTAB SET COUN = %d, STAT = 'TO SEND', LSTU = '%s' WHERE URNO = %s",lgCount,clNowTime,pcpUrno);

    dbg(TRACE,"%05d: Mark Message clSqlBuf: <%s>",__LINE__,clSqlBuf);

    slFkt = START;
    slCursor = 0;
    ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clData);
    if (ilRC != RC_SUCCESS)
    {
	dbg(TRACE,"sql_if failed###################");
    }
    commit_work();
    close_my_cursor (&slCursor);
    dbg(TRACE,"-----MarkToSend end -----");

    return ilRC;
}


void CleanDb()
{
    int ilRC = RC_SUCCESS;
    short slFkt = 0;
    short slCursor = 0;
    char clSqlBuf[200] = "\0";
    char clData[50] = "\0";
    char clDelTime[30] = "\0";
    long llCleanMqr = 0;
    long llCleanMqs = 0;
	int ili = 0;

    dbg(TRACE,"-----CleanDb start-----");

/* NOW CLEAN UP MQRTAB*/

    TimeToStr(clDelTime,time(NULL));
    llCleanMqr = atol(cgCleanMqr);
    if (llCleanMqr > 30)
    {
	llCleanMqr = 30;
    }
    llCleanMqr = llCleanMqr*24*60*60*(-1);
    AddSecondsToCEDATime(clDelTime,llCleanMqr,1);
    dbg(TRACE,"Now Delete Records older than <%s> from MQRTAB for INAM = <%s>",clDelTime,mod_name);

    slFkt = START;
    slCursor = 0;
    
    sprintf(clSqlBuf,"DELETE FROM MQRTAB WHERE TPUT < '%s' AND INAM = '%s'",clDelTime,mod_name);
    ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clData);
    if (ilRC != RC_SUCCESS)
    {
	dbg(TRACE,"%05d: sql_if failed or nothing to delete in CleanDb",__LINE__);
    }
    commit_work();
    close_my_cursor (&slCursor);


/* NOW CLEAN UP MQSTAB */  

    TimeToStr(clDelTime,time(NULL));
    llCleanMqs = atol(cgCleanMqs);
    if (llCleanMqs > 30)
    {
	llCleanMqs = 30;
    }
    llCleanMqs = llCleanMqs*24*60*60*(-1);
    AddSecondsToCEDATime(clDelTime,llCleanMqs,1);
    


	if(igUseQName == TRUE)
	{
		for(ili = 0 ; ili < 20 ; ili++)
		{
			if(strlen(rgMyQueues[ili].cgQName) > 0)
			{
				dbg(TRACE,"Now Delete Records older than <%s> from MQSTAB for INAM = <%s>",clDelTime,rgMyQueues[ili].cgQName);
				slFkt = START;
				slCursor = 0;

				sprintf(clSqlBuf,"DELETE FROM MQSTAB WHERE TPUT < '%s' AND INAM = '%s'",clDelTime,rgMyQueues[ili].cgQName);
				ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clData);
				if (ilRC != RC_SUCCESS)
				{
					dbg(TRACE,"%05d: sql_if failed or nothing to delete in CleanDb",__LINE__);
				}
				commit_work();
				close_my_cursor (&slCursor);
			}
		}
	}
	else
	{
		dbg(TRACE,"Now Delete Records older than <%s> from MQSTAB for INAM = <%s>",clDelTime,mod_name);
		slFkt = START;
		slCursor = 0;

		sprintf(clSqlBuf,"DELETE FROM MQSTAB WHERE TPUT < '%s' AND INAM = '%s'",clDelTime,mod_name);
		ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clData);
		if (ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"%05d: sql_if failed or nothing to delete in CleanDb",__LINE__);
		}
		commit_work();
		close_my_cursor (&slCursor);
	}


    dbg(TRACE,"-----CleanDb end-----");


}


static int DoRecovery(char *pcpBeginTime, int ipQueueNumber)
{
    int ilRC = RC_SUCCESS;
    int ilRC2 = RC_SUCCESS;
    short slFkt = 0;
    short slCursor = 0;
    short slFkt2 = 0;
    short slCursor2 = 0;
    char clSqlBuf[4500] = "\0";
    char clData[4500] = "\0";
    int ilLen = 0;
    char clUrno[20] ="\0";
    char clMyTime[30] ="\0";
    char clNowTime[20] ="\0";

    MQMD     md = {MQMD_DEFAULT};    /* Message Descriptor              */
    MQLONG   CompCode;               /* completion code                 */
    MQLONG   Reason;                 /* reason code                     */
    MQPMO    pmo = {MQPMO_DEFAULT};  /* put message options             */

    dbg(TRACE,"-----DoRecovery start -----");

    slFkt = START;
    slCursor = 0;

    while (ilRC == RC_SUCCESS)
    {
	if(strlen(rgMyQueues[ipQueueNumber].cgFirstFailedUrno) == 0)
	{
	    sprintf(clSqlBuf,"SELECT URNO, DATA FROM MQSTAB WHERE INAM ='%s' AND TPUT > '%s' ORDER BY TPUT",mod_name,pcpBeginTime);
	    dbg(TRACE,"%05d: clSqlBuf: <%s>",__LINE__,clSqlBuf);
	    ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clData);
	    if (ilRC != RC_SUCCESS)
	    {
		dbg(TRACE,"%05d: sql_if returnd ilRC: <%d> probably no Data TO SEND found",__LINE__,ilRC);
	    }
	    slFkt = NEXT;
	
	    BuildItemBuffer (clData, "", 2, ",");
	    strcpy(clSqlBuf,clData); /* clSqlBuf wird hier nur temporaer benutzt, hat nichts mit sql zu tun */
	    
	    ilLen = get_real_item (clUrno, clSqlBuf, 1);
	    ilLen = get_real_item (clData, clSqlBuf, -2);
	}
	else
	{
	    strcpy(clUrno,cgFirstFailedUrno);
	    strcpy(clData,cgFirstFailedData);
	}
	if (ilRC == RC_SUCCESS)
	{
	    
/* PUT MESSAGE TO MQ QUEUE */
	    memcpy(md.Format,           
		   MQFMT_STRING, MQ_FORMAT_LENGTH);
	    memcpy(md.MsgId, MQMI_NONE, sizeof(md.MsgId));
		md.Priority = igMqSendPrio;

	    pmo.Options = MQPMO_NEW_MSG_ID
		+ MQPMO_FAIL_IF_QUIESCING;
	    ConvertDbStringToClient(clData);
	    dbg(TRACE,"%05d: Send to Queue: <%s> clData <%s>, length <%d>",__LINE__,cgMqSend,clData,strlen(clData));
	    if(((strcmp(mod_name,"tams")) == 0) && (clData[0] == 'R') && (clData[1] == 'Q'))
	    {
		dbg(TRACE,"%05d: dont send AdhocRequest in adhocrequest-mode",__LINE__);
	    }
	    else
	    {
		MQPUT(HconSend,                /* connection handle               */
		      HobjSend,          /* object handle                   */
		      &md,                 /* message descriptor              */
		      &pmo,                /* default options                 */
		      strlen(clData),              /* buffer length                   */
		      clData,              /* message buffer                  */
		      &CompCode,           /* completion code                 */
		      &Reason);            /* reason code                     */

		strcpy(clMyTime,md.PutDate);
		clMyTime[16] = '\0';
		/* report reason, if any */
		dbg(TRACE,"%05d: MQPUT ended with reason code <%d>, Time: <%s>",__LINE__, Reason, clMyTime);

		if(Reason == 2009)
		{
			Terminate(10);
		}
	    }

	}
    }/*end of while(ilRC == RC_SUCCESS)*/
    close_my_cursor (&slCursor);

    dbg(TRACE,"-----DoRecovery end -----");

    return 0;
} /*end of DoRecovery*/




static int GetQueueNumber(char *clQueueName)
{
	int ili = 0;
	int ilResult = -1;

	for(ili = 0 ; ili < 20 ; ili++)
	{
		if(strcmp(rgMyQueues[ili].cgQName,clQueueName) == 0)
		{
			ilResult = ili;
		}
	}
	return ilResult;
}

/* This function will determine if an error is recoverable or not */
/* MQ has a total of 409 reason codes, so the switch should be    */
/* expanded whenever reason codes are classified. The default     */
/* behavior is to treat the error condition as recoverable, but   */
/* a maximum of 10 retries will be allowed before the condition   */
/* is deemed fatal, and a new connect will be forced.             */

static int IsRecoverable(MQLONG ilReasonCode)
{
	int bRC = TRUE;
	
	switch (ilReasonCode)
	{
		case MQRC_CONNECTION_BROKEN :
		case MQRC_HANDLE_NOT_AVAILABLE :
			bRC = FALSE;
			break;
		case MQRC_NO_MSG_AVAILABLE :
		case MQRC_GET_INHIBITED :
			bRC = TRUE;
			igRetryCount = 0;
			break;
		default :
			bRC = TRUE;
			igRetryCount++;
			
			if (igRetryCount > 10)
			{
				bRC = FALSE;
			}
			break;
	}
	
	return bRC;
}
