#ifndef _DEF_mks_version_fltlib_c
  #define _DEF_mks_version_fltlib_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_fltlib_c[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/fltlib.c 1.1 2011/08/09 16:27:05SGT bst Exp  $";
#endif /* _DEF_mks_version_fltlib_c */

#include <stdlib.h>
#include <string.h>
#include <stdio.h> 
#include <malloc.h>

#include "ct.h"
#include "db_if.h"
#include "glbdef.h"
#include "fltlib.h"


#define CT_TRUE -1
#define CT_FALSE 0 

#define SORT_ASC        0
#define SORT_DESC       1

#define FOR_INIT		0

#define URNO_SIZE 10

#define MAX_CTFLT_VALUE 4

static STR_DESC argCtFlValue[MAX_CTFLT_VALUE+1];
static STR_DESC rgCtHtLine;
static STR_DESC rgCtFlLine;
static STR_DESC rgCtFlData;
static STR_DESC rgCtFlList;
static STR_DESC rgCtFlTemp;
static STR_DESC rgCtAftUrno;

static char pcgAftTabName[] = "AFTTAB";
static char pcgAftTabGrid[] = "AftTab";
static char pcgAftTabFldList[] = "URNO,RKEY,AURN,FTYP,ADID,FLNO,CSGN,STOA,STOD,REGN,ACT3,ACT5,PSTA,PSTD,USEC,ETAI,ETDI,ONBL,OFBL,TIFA,TIFD,TISA,TISD,PDBS,PDES";
static char pcgAftTabFldSize[] = "10  ,10  ,10  ,1   ,1   ,9   ,9   ,14  ,14  ,12  ,3   ,5   ,5   ,5   ,32  ,14  ,14  ,14  ,14  ,14  ,14  ,1   ,1   ,14  ,14  ";

static char pcgGorTabName[] = "GORTAB";
static char pcgGorTabGrid[] = "GorTab";
static char pcgGorTabFldList[] = "URNO,FLNU,AURN,DURN,RKEY,RSBT,GSQN,SQNO,RIFR,FTYP,ADID,OOOI,FRDS,SBLT,TIBT,TOBT,GRDT,FLNO,CSGN,REGN,ACT3,ACT5,RTAB,RURN,RBAS,RLNK,RTYP,RCOD,RNAM,RABS,RAES,RABE,RAEE,RABA,RAEA,TIFB,TIFE,STRN,SARR,SDEP,STTI,STTO,STAU,TISB,TISE,DLYA,DLYD,DLYO,OTIM";
static char pcgGorTabFldSize[] = "10  ,10  ,10  ,10  ,10  ,14  ,6   ,4   ,1   ,1   ,1   ,3   ,9   ,14  ,14  ,14  ,8   ,9   ,9   ,12  ,3   ,5   ,3   ,10  ,3   ,10  ,4   ,4   ,8   ,14  ,14  ,14  ,14  ,14  ,14  ,14  ,14  ,1   ,1   ,1   ,1   ,1   ,1   ,1   ,1   ,1   ,1   ,1   ,8   ";
static char pcgGorTabNulData[] = "0,0,0,0 ,0,,,,,,,,,,,,,,,,,,,0,,0,,,,,,,,,,,,,,,,,,,,,,,";

static char pcgAftGetArrList[] = "URNO,RKEY,AURN,FTYP,ADID,FLNO,CSGN,STOA,REGN,ACT3,ACT5";
static char pcgGorSetArrList[] = "FLNU,RKEY,AURN,FTYP,ADID,FLNO,CSGN,SBLT,REGN,ACT3,ACT5";
static char pcgAftGetDepList[] = "URNO,RKEY,AURN,FTYP,ADID,FLNO,CSGN,STOD,REGN,ACT3,ACT5";
static char pcgGorSetDepList[] = "FLNU,RKEY,AURN,FTYP,ADID,FLNO,CSGN,SBLT,REGN,ACT3,ACT5";

static char pcgFltEvtGrid[] = "FltEvt";
static char pcgFltEvtFldList[] = "SORT,LINE,AURN,DURN,RSBT,URNO,FTYP,ADID,OOOI,RTYP,STND";
static char pcgFltEvtFldSize[] = "60  ,4   ,10  ,10  ,14  ,10  ,1   ,1   ,3   ,3   ,5   ";
static char pcgFltEvtNulData[] = ",,,,,,,,,,";

/* FUNCTION PROTOTYPES */
static int GOR_InitMyTab(char *pcpCtTabName, char *pcpCtFldList, char *pcpCtFldSize);
static int GOR_SetMyTabUrnoPool(char *pcpCtTabName, char *pcpRangeName);
static int GOR_CheckGorTabRecs(int ipForWhat);
static int GOR_CreateFlightEventSorter(int ipForWhat);
static long GOR_CreateFltEvtRecord(int ipForWhat, long lpAftLine, char *pcpEvtAdid, char *pcpEvtRtyp, char *pcpEvtCode, char *pcpUseAurn);
static long GOR_GetMatchingLineNo(long lpAftLine, char *pcpAftAdid, 
                                  char *pclHitList, char *pclGetOooi, char *pclGetRtyp);
static long GOR_SetFlightRelation(char *pcpForAdid, char *pcpForFtyp, long lpAftLine, long lpGorLine);
static int GOR_CheckDbAction(char *pcpGridName, char *pcpTableName, int ipForWhat);
static int GOR_GetValueFromUrnoPool(char *pcpGridName, char *pcpNxtUrno);
static int GOR_CheckGridUrnoPool(char *pcpGridName);
static int GOR_FillGridUrnoPool(char *pcpGridName, int ipCount);
static int GOR_InsertGridRecord2DB(char *pcpGridName, long lpLineNo);
static int GOR_UpdateGridRecord2DB(char *pcpGridName, long lpLineNo);
static int GOR_DeleteGridRecord2DB(char *pcpGridName, long lpLineNo);
static void GOR_BuildSqlInsValStrg(char *pcpResult, char *pcpFldLst);
static void GOR_BuildSqlUpdValStrg(char *pcpResult, char *pcpFldLst);

static int GOR_GetNewDateTimeStrg(char *pcpResult, char *pcpDateTime, int ipMinuteOffset);
static int ShowEvtSorter(int ipForWhat);
static long GetNextAftLine(long lpBgnLine, char *pcpForType, char *pcpForOooi);

/* *************************************************** */
/* *************************************************** */
int FLT_InitFltLibEnvironment(void)
{
  int ilRc = RC_SUCCESS;
  dbg(TRACE,"PREPARING THE FLTLIB GRID ENVIRONMENT");
  dbg(TRACE,"-------------------------------------");
  GOR_InitMyTab("STR_DESC","","");
  GOR_InitMyTab(pcgAftTabGrid,pcgAftTabFldList,pcgAftTabFldSize);
  dbg(TRACE,"-------------------------------------");
  GOR_InitMyTab(pcgGorTabGrid,pcgGorTabFldList,pcgGorTabFldSize);
  GOR_SetMyTabUrnoPool(pcgGorTabGrid, pcgGorTabName);
  dbg(TRACE,"-------------------------------------");
  GOR_InitMyTab(pcgFltEvtGrid,pcgFltEvtFldList,pcgFltEvtFldSize);
  dbg(TRACE,"-------------------------------------");
  CT_SetTrimMethod(pcgAftTabGrid, "R");
  CT_SetTrimMethod(pcgGorTabGrid, "R");
  CT_SetTrimMethod(pcgFltEvtGrid, "R");
  CT_SetArrayDataTracking(pcgGorTabGrid, pcgGorTabName, 1, 1);

  return ilRc;
}

/* *************************************************** */
/* *************************************************** */
static int GOR_InitMyTab(char *pcpCtTabName, char *pcpCtFldList, char *pcpCtFldSize)
{
  int ilRC = RC_SUCCESS;
  int i = 0;

  if (strcmp(pcpCtTabName,"STR_DESC") != 0)
  {
    CT_CreateArray(pcpCtTabName);
    CT_SetFieldList(pcpCtTabName,pcpCtFldList);
    CT_SetLengthList(pcpCtTabName, pcpCtFldSize);
    CT_SetDataPoolAllocSize(pcpCtTabName, 1000);
    dbg(TRACE,"CT DATA GRID <%s> INITIALIZED",pcpCtTabName);
  }
  else
  {

    for (i=0;i<=MAX_CTFLT_VALUE;i++)
	{
      CT_InitStringDescriptor(&argCtFlValue[i]);
      argCtFlValue[i].Value = malloc(4096);
      argCtFlValue[i].AllocatedSize = 4096;
      argCtFlValue[i].Value[0] = 0x00;
	}

    CT_InitStringDescriptor(&rgCtHtLine);
    rgCtHtLine.Value = malloc(4096);
    rgCtHtLine.AllocatedSize = 4096;
    rgCtHtLine.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCtFlLine);
    rgCtFlLine.Value = malloc(4096);
    rgCtFlLine.AllocatedSize = 4096;
    rgCtFlLine.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCtFlData);
    rgCtFlData.Value = malloc(4096);
    rgCtFlData.AllocatedSize = 4096;
    rgCtFlData.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCtFlList);
    rgCtFlList.Value = malloc(9182);
    rgCtFlList.AllocatedSize = 9182;
    rgCtFlList.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCtFlTemp);
    rgCtFlTemp.Value = malloc(1024);
    rgCtFlTemp.AllocatedSize = 1024;
    rgCtFlTemp.Value[0] = 0x00;

    CT_InitStringDescriptor(&rgCtAftUrno);
    rgCtAftUrno.Value = malloc(16);
    rgCtAftUrno.AllocatedSize = 16;
    rgCtAftUrno.Value[0] = 0x00;

    dbg(TRACE,"STRING BUFFERS <%s> INITIALIZED",pcpCtTabName);
  }
  return ilRC;
} /* end of GOR_InitMyTab */

/* *************************************************** */
/* *************************************************** */
static int GOR_SetMyTabUrnoPool(char *pcpCtTabName, char *pcpTableName)
{
  int ilRc = RC_SUCCESS;
  int ilHandleUrnoRanges = 0;
  char pclNumTabKey[32] = "";
  ilHandleUrnoRanges = DB_GetUrnoKeyCode(pcpTableName,pclNumTabKey);
  dbg(TRACE,"URNO POOL: <%s> <%s> RANGE KEY CONFIGURED AS <%s>",pcpCtTabName,pcpTableName,pclNumTabKey);
  if (ilHandleUrnoRanges != TRUE)
  {
    dbg(TRACE,"URNO POOL: <%s> <%s> VALUES BY <GetNextValues> SNOTAB.ACNU",pcpCtTabName,pcpTableName);
  }
  else
  {
    dbg(TRACE,"URNO POOL: <%s> <%s> NUMBERS BY <GetNextNumbers> NUMTAB.KEYS",pcpCtTabName,pcpTableName);
  }
  CT_SetUrnoPoolMethod(pcpCtTabName, pcpTableName, ilHandleUrnoRanges, pclNumTabKey);
  return ilRc;
}

/* *************************************************** */
/* *************************************************** */
int FLT_SyncGorRotation(char *pcpAftSqlKey)
{
  int ilRc = RC_SUCCESS;
  char pclUseKey[4096];
  strcpy(pclUseKey,pcpAftSqlKey);
  FLT_LoadGridData(pcgAftTabGrid, pcgAftTabName, pclUseKey);
  CT_GetDistinctList(pcgAftTabGrid, "URNO", "", "", ",", &rgCtFlList);
  dbg(DEBUG,"AFT URNO LIST <%s>",rgCtFlList.Value);
  sprintf(pclUseKey,"WHERE FLNU IN (%s)",rgCtFlList.Value);
  FLT_LoadGridData(pcgGorTabGrid, pcgGorTabName, pclUseKey);
  GOR_CheckGorTabRecs(FOR_INIT);
  GOR_CheckDbAction(pcgGorTabGrid, pcgGorTabName, 0);
  return ilRc;
}

/* *************************************************** */
/* *************************************************** */
int FLT_LoadGridData(char *pcpGridName, char *pcpTblName, char *pcpSqlKey)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  char pclTblName[16] = "";
  char pclFldList[1024] = "";
  char pclSqlCond[1024] = "";
  char pclSqlData[1024] = "";
  char pclSqlBuff[1024] = "";
  long llCurTabLine = 0;
  long llMaxTabLine = 0;
  int ilFldCnt = 0;
  int ilLegCnt = 0;
  short slCursor = 0;
  short slFkt = 0;
  dbg(TRACE,"---------------------------------");
  dbg(TRACE,"LOADING <%s> RECORDS",pcpGridName);
  dbg(TRACE,"---------------------------------");
  CT_ResetContent(pcpGridName);
  CT_GetFieldList(pcpGridName, pclFldList);

  strcpy(pclTblName,pcpTblName);
  strcpy(pclSqlCond,pcpSqlKey);
  sprintf(pclSqlBuff,"SELECT %s FROM %s %s ",pclFldList,pclTblName,pclSqlCond);
  dbg(TRACE,"<%s>",pclSqlBuff);
  ilFldCnt = field_count(pclFldList);
  slCursor = 0;
  slFkt = START;
  ilGetRc = DB_SUCCESS;
  while (ilGetRc == DB_SUCCESS)
  {
    ilGetRc = sql_if(slFkt,&slCursor,pclSqlBuff,pclSqlData);
	if (ilGetRc == DB_SUCCESS)
    {
      BuildItemBuffer(pclSqlData,"",ilFldCnt,",");
      FLT_TrimRecordData(pclSqlData, ",");
      dbg(TRACE,"<%s>",pclSqlData);
      CT_InsertTextLine(pcpGridName,pclSqlData);
    }
    slFkt = NEXT;
  }
  close_my_cursor(&slCursor);
  dbg(TRACE,"-----------------------------------------------------");

  return ilRc;
}

/* *************************************************** */
/* *************************************************** */
int FLT_TrimRecordData(char *pclTextBuff,char *pcpFldSep)
{
  int ilCnt = 0;
  char *pclDest = NULL;
  char *pclSrc = NULL;
  char *pclLast = NULL; 
  /* last non blank byte position */
  /* but keep at least one blank */
  pclDest = pclTextBuff;
  pclSrc  = pclTextBuff;
  pclLast  = pclDest - 1;
  if (*pclSrc == ' ')
  {
    pclLast = pclTextBuff;
    pclSrc++;
    pclDest++;
  }
  while (*pclSrc != '\0')
  {
    if (*pclSrc == pcpFldSep[0])
    {
      pclDest = pclLast + 1;
      *pclDest = *pclSrc;
      pclSrc++;
      pclDest++;
      if (*pclSrc == ' ')
      {
        pclLast = pclDest;
      }
    }
    *pclDest = *pclSrc;
    if (*pclDest != ' ')
    {
      pclLast = pclDest;
    }
    pclDest++;
    pclSrc++;
  }
  pclDest = pclLast + 1;
  *pclDest = '\0';
  return ilCnt;
} /* end FLT_TrimRecordData */

/* *************************************************** */
/* *************************************************** */
static int GOR_CheckGorTabRecs(int ipForWhat)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  int ilCheckRtyp = TRUE;
  int ilUpdateGorLine = TRUE;
  int ilArrTypeIsFlt = FALSE;
  int ilArrTypeIsTow = FALSE;
  int ilDepTypeIsFlt = FALSE;
  int ilDepTypeIsTow = FALSE;
  int ilSetRecDelFlg = FALSE;
  char pclRefLine[16] = "";
  char pclCurRtyp[16] = "";

  char pclAftUrno[16] = "";
  char pclAftFtyp[16] = "";
  char pclAftAdid[16] = "";

  char pclAftArrUrno[16] = "";
  char pclAftArrFtyp[16] = "";
  char pclAftArrAdid[16] = "";
  char pclAftArrPsta[16] = "";
  char pclAftArrStoa[16] = "";
  char pclAftArrEtai[16] = "";
  char pclAftArrOnbl[16] = "";
  char pclAftArrTifa[16] = "";
  char pclAftArrTisa[16] = "";
  char pclAftArrUsec[64] = "";

  char pclAftDepUrno[16] = "";
  char pclAftDepFtyp[16] = "";
  char pclAftDepAdid[16] = "";
  char pclAftDepPstd[16] = "";
  char pclAftDepStod[16] = "";
  char pclAftDepEtdi[16] = "";
  char pclAftDepOfbl[16] = "";
  char pclAftDepTifd[16] = "";
  char pclAftDepTisd[16] = "";
  char pclAftDepUsec[64] = "";
  char pclAftDepPdbs[16] = "";
  char pclAftDepPdes[16] = "";


  char pclGorTabUrno[16] = ""; /*  */
  char pclGorTabFlnu[16] = ""; /*  */
  char pclGorTabAurn[16] = ""; /*  */
  char pclGorTabDurn[16] = ""; /*  */
  char pclGorTabRkey[16] = ""; /*  */
  char pclGorTabRsbt[16] = ""; /*  */
  char pclGorTabGsqn[16] = ""; /*  */
  char pclGorTabSqno[16] = ""; /*  */
  char pclGorTabFtyp[16] = ""; /*  */
  char pclGorTabAdid[16] = ""; /*  */
  char pclGorTabOooi[16] = ""; /*  */
  char pclGorTabFrds[16] = ""; /*  */
  char pclGorTabSblt[16] = ""; /*  */
  char pclGorTabTibt[16] = ""; /*  */
  char pclGorTabTobt[16] = ""; /*  */
  char pclGorTabGrdt[16] = ""; /*  */
  char pclGorTabFlno[16] = ""; /*  */
  char pclGorTabCsgn[16] = ""; /*  */
  char pclGorTabRegn[16] = ""; /*  */
  char pclGorTabAct3[16] = ""; /*  */
  char pclGorTabAct5[16] = ""; /*  */
  char pclGorTabRtyp[16] = ""; /*  */
  char pclGorTabRnam[16] = ""; /*  */
  char pclGorTabRabs[16] = ""; /* pclAftArrStoa */
  char pclGorTabRaes[16] = ""; /* pclAftDepStod */
  char pclGorTabRabe[16] = ""; /* pclAftArrEtai */
  char pclGorTabRaee[16] = ""; /* pclAftDepEtdi */
  char pclGorTabRaba[16] = ""; /* pclAftArrOnbl */
  char pclGorTabRaea[16] = ""; /* pclAftDepOfbl */
  char pclGorTabTifb[16] = ""; /* pclAftArrTifa */
  char pclGorTabTife[16] = ""; /* pclAftDepTifd */
  char pclGorTabStrn[16] = ""; /*  */
  char pclGorTabSarr[16] = ""; /*  */
  char pclGorTabSdep[16] = ""; /*  */
  char pclGorTabStti[16] = ""; /*  */
  char pclGorTabStto[16] = ""; /*  */
  char pclGorTabStau[16] = ""; /*  */
  char pclGorTabTisb[16] = ""; /* pclAftArrTisa */
  char pclGorTabTise[16] = ""; /* pclAftDepTisd */
  char pclGorTabDlya[16] = ""; /*  */
  char pclGorTabDlyd[16] = ""; /*  */
  char pclGorTabDlyo[16] = ""; /*  */
  char pclGorTabOtim[16] = ""; /*  */

  char pclCurAurn[16] = "";
  char pclCurDurn[16] = "";
  char pclPrvAurn[16] = "";
  char pclPrvDurn[16] = "";

  char pclCurTifa[16] = "";
  char pclCurTifd[16] = "";
  char pclPrvTifa[16] = "";
  char pclPrvTifd[16] = "";

  char pclArrFltUrno[16] = "";
  char pclArrFltFtyp[16] = "";
  char pclArrFltAdid[16] = "";
  char pclArrFltTifa[16] = "";

  char pclDepFltUrno[16] = "";
  char pclDepFltFtyp[16] = "";
  char pclDepFltAdid[16] = "";
  char pclDepFltTifd[16] = "";

  char pclEvtRsbt[16] = "";
  char pclEvtOooi[16] = "";
  char pclNxtOooi[16] = "";
  char pclEvtRtyp[16] = "";
  char pclNxtRtyp[16] = "";

  char pclTmpData[64] = "";

  long llMaxLine = 0;
  long llEvtLine = 0;
  long llNxtLine = 0;
  long llRefLine = 0;
  long llAftLine = 0;
  long llGorLine = 0;
  long llGorArrLine = 0;
  long llGorDepLine = 0;
  long llAftArrLine = 0;
  long llAftDepLine = 0;
  long llArrFltLine = 0;
  long llDepFltLine = 0;
  long llNxtDepLine = 0;
  long llUrnoCnt = 0;
  long llHitLines = 0;
  int ilGsqnVal = 0;
  int ilSqnoVal = 0;
  int ilOtimVal = 0;
  int ilTimeVal = 0;
  int ilTimeDif = 0;

  /* Step 1: Prepare Sort Keys and Sort them */
  GOR_CreateFlightEventSorter(0);

  /* Step 2: Now run through logically sorted AFT records */
  strcpy(pclPrvAurn,"----------");
  ilGsqnVal = 0;
  ilSqnoVal = 0;
  llArrFltLine = -1;
  llDepFltLine = -1;
  llEvtLine = -1;
  llMaxLine = CT_GetLineCount(pcgFltEvtGrid) - 1;
  while (llEvtLine < llMaxLine)
  {
    dbg(TRACE,"---------------------------------------------");
    llAftArrLine = -1;
    llAftDepLine = -1;
    llNxtLine = -1;
    llEvtLine++;
    if (llEvtLine <= llMaxLine)
    {
      /* GETTING THE ARRIVAL LINE */
      ilArrTypeIsFlt = FALSE;
      ilArrTypeIsTow = FALSE;
      ilDepTypeIsFlt = FALSE;
      ilDepTypeIsTow = FALSE;
      CT_GetMultiValues(pcgFltEvtGrid, llEvtLine, "LINE", pclRefLine);
      CT_GetMultiValues(pcgFltEvtGrid, llEvtLine, "RSBT", pclEvtRsbt);
      CT_GetMultiValues(pcgFltEvtGrid, llEvtLine, "OOOI", pclEvtOooi);
      CT_GetMultiValues(pcgFltEvtGrid, llEvtLine, "RTYP", pclEvtRtyp);
      CT_GetMultiValues(pcgFltEvtGrid, llEvtLine, "AURN", pclCurAurn);
      CT_GetMultiValues(pcgFltEvtGrid, llEvtLine, "DURN", pclCurDurn);
      if (strcmp(pclCurAurn,pclPrvAurn) != 0)
      {
        strcpy(pclPrvAurn,pclCurAurn);
        ilGsqnVal++;
        ilSqnoVal = 0;
      }
      llRefLine = atol(pclRefLine);
	  if (strcmp(pclEvtOooi,"ARR") == 0)
      {
        llAftArrLine = llRefLine;
        if (strcmp(pclEvtRtyp,"FLT") == 0)
        {
          dbg(TRACE,"LINE %d: REF AFT LINE=%d (OOOI=%s RTYP=%s) FLIGHT ARR",llEvtLine,llRefLine,pclEvtOooi,pclEvtRtyp);
          strcpy(pclGorTabOooi,"ARR");
          dbg(TRACE,"GOT REFERENCE TO ARR FLIGHT IN AFT DATA GRID LINE %d",llAftArrLine);
          CT_GetMultiValues(pcgAftTabGrid, llAftArrLine, "URNO", pclArrFltUrno);
          CT_GetMultiValues(pcgAftTabGrid, llAftArrLine, "FTYP", pclArrFltFtyp);
          CT_GetMultiValues(pcgAftTabGrid, llAftArrLine, "ADID", pclArrFltAdid);
          CT_GetMultiValues(pcgAftTabGrid, llAftArrLine, "TIFA", pclArrFltTifa);
          dbg(TRACE,"REF ARR DATA: URNO <%s> FTYP <%s> ADID <%s> TIFA <%s>",
                     pclArrFltUrno,pclArrFltFtyp,pclArrFltAdid,pclArrFltTifa);
          ilArrTypeIsFlt = TRUE;
          ilArrTypeIsTow = FALSE;
          llNxtDepLine = GetNextAftLine(llEvtLine, "FLT", "DEP");
          if (llNxtDepLine >= 0)
          {
            dbg(TRACE,"GOT REFERENCE TO DEP FLIGHT IN AFT DATA GRID LINE %d",llNxtDepLine);
            CT_GetMultiValues(pcgAftTabGrid, llNxtDepLine, "URNO", pclDepFltUrno);
            CT_GetMultiValues(pcgAftTabGrid, llNxtDepLine, "FTYP", pclDepFltFtyp);
            CT_GetMultiValues(pcgAftTabGrid, llNxtDepLine, "ADID", pclDepFltAdid);
            CT_GetMultiValues(pcgAftTabGrid, llNxtDepLine, "TIFD", pclDepFltTifd);
            dbg(TRACE,"REF DEP DATA: URNO <%s> FTYP <%s> ADID <%s> TIFD <%s>",
                       pclDepFltUrno,pclDepFltFtyp,pclDepFltAdid,pclDepFltTifd);
          }
          else
          {
            dbg(TRACE,"THIS ARR FLIGHT IS AN OPEN ARRIVAL LEG");
          }
        }
        else if (strcmp(pclEvtRtyp,"TOW") == 0)
        {
          dbg(TRACE,"LINE %d: REF AFT LINE=%d (OOOI=%s RTYP=%s) TOWING ARR",llEvtLine,llRefLine,pclEvtOooi,pclEvtRtyp);
          strcpy(pclGorTabOooi,"TOW");
          ilArrTypeIsFlt = FALSE;
          ilArrTypeIsTow = TRUE;
        }
        if (llEvtLine < llMaxLine)
        {
          llNxtLine = llEvtLine + 1;
        }
      }
	  else if (strcmp(pclEvtOooi,"DEP") == 0)
      {
        if (strcmp(pclEvtRtyp,"FLT") == 0)
        {
          llAftDepLine = llRefLine;
          dbg(TRACE,"LINE %d: REF AFT LINE=%d (OOOI=%s RTYP=%s) FLIGHT DEP",llEvtLine,llRefLine,pclEvtOooi,pclEvtRtyp);
          strcpy(pclGorTabOooi,"DEP");
          ilDepTypeIsFlt = TRUE;
          ilDepTypeIsTow = FALSE;
        }
        else if (strcmp(pclEvtRtyp,"TOW") == 0)
        {
          dbg(TRACE,"LINE %d: REF AFT LINE=%d (OOOI=%s RTYP=%s) TOWING DEP",llEvtLine,llRefLine,pclEvtOooi,pclEvtRtyp);
          strcpy(pclGorTabOooi,"TOW");
          ilDepTypeIsFlt = FALSE;
          ilDepTypeIsTow = TRUE;
        }
        llAftArrLine = -1;
        llNxtLine = -1;
      }

      llGorLine = -1;
      llAftLine = llRefLine;
      CT_GetMultiValues(pcgAftTabGrid, llAftLine, "URNO", pclAftUrno);
      CT_GetMultiValues(pcgAftTabGrid, llAftLine, "ADID", pclAftAdid);
      CT_GetMultiValues(pcgAftTabGrid, llAftLine, "FTYP", pclAftFtyp);
      dbg(DEBUG,"AFTTAB LINE %d: URNO <%s> FTYP <%s> ADID <%s>",llAftLine,pclAftUrno,pclAftFtyp,pclAftAdid);
      llHitLines = CT_GetLinesByColumnValue(pcgGorTabGrid, "FLNU", pclAftUrno, &rgCtHtLine, 0);
      dbg(DEBUG,"RELATED LINES IN GORTAB <%s> (FLNU=%s)",rgCtHtLine.Value,pclAftUrno);
      dbg(DEBUG,"FIRST SYNCHRONIZE THE STANDARD FIELDS");

      strcpy(pclCurRtyp,"POS");
      ilCheckRtyp = TRUE;
      if (ilCheckRtyp == TRUE);
      {
        switch (pclAftAdid[0])
        {
          case 'A':
            dbg(DEBUG,"GOR: SYNC PLAIN ARRIVAL FLIGHT (ADID='A'/FTYP='%s')",pclAftFtyp);
            llGorArrLine = GOR_GetMatchingLineNo(llAftLine, pclAftAdid, rgCtHtLine.Value, "ARR", pclCurRtyp);
            llGorArrLine = GOR_SetFlightRelation("A", pclAftFtyp, llAftLine, llGorArrLine);
            llGorLine = llGorArrLine;
          break;
          case 'B':
            if (strstr("TG",pclAftFtyp) != NULL)
            {
              dbg(DEBUG,"GOR: SYNC TOWING (ADID='B'/FTYP='%s')",pclAftFtyp);
              llGorArrLine = GOR_GetMatchingLineNo(llAftLine, pclAftAdid, rgCtHtLine.Value, "TOW", pclCurRtyp);
              llGorArrLine = GOR_SetFlightRelation("A", pclAftFtyp, llAftLine, llGorArrLine);
              llGorLine = llGorArrLine;
            }
            else
            {
              if (ilDepTypeIsFlt == TRUE)
              {
                dbg(DEBUG,"GOR: SYNC LOCAL FLIGHT (ADID='B'/FTYP='%s')",pclAftFtyp);
                dbg(DEBUG,"SYNC LOCAL DEPARTURE FLIGHT (ADID='D'/FTYP='%s')",pclAftFtyp);
                llGorDepLine = GOR_GetMatchingLineNo(llAftLine, pclAftAdid, rgCtHtLine.Value, "DEP", pclCurRtyp);
                llGorDepLine = GOR_SetFlightRelation("D", pclAftFtyp, llAftLine, llGorDepLine);
                llGorLine = llGorDepLine;
              }
              if (ilArrTypeIsFlt == TRUE)
              {
                dbg(DEBUG,"GOR: SYNC LOCAL ARRIVAL FLIGHT (ADID='A'/FTYP='%s')",pclAftFtyp);
                llGorArrLine = GOR_GetMatchingLineNo(llAftLine, pclAftAdid, rgCtHtLine.Value, "ARR", pclCurRtyp);
                llGorArrLine = GOR_SetFlightRelation("A", pclAftFtyp, llAftLine, llGorArrLine);
                llGorLine = llGorArrLine;
              }
            }
          break;
          case 'D':
            dbg(TRACE,"GOR: SYNC PLAIN DEPARTURE FLIGHT (ADID='D'/FTYP='%s')",pclAftFtyp);
            llGorDepLine = GOR_GetMatchingLineNo(llAftLine, pclAftAdid, rgCtHtLine.Value, "DEP", pclCurRtyp);
            llGorDepLine = GOR_SetFlightRelation("D", pclAftFtyp, llAftLine, llGorDepLine);
            llGorLine = llGorDepLine;
          break;
          default:
            dbg(TRACE,"GORTAB SYNC: UNDEFINED ADID VALUE <%s>",pclAftAdid);
          break;
        }
      }
    }
    dbg(DEBUG,"THE RELATED LINE IN GORTAB IS %d",llGorLine);
    dbg(DEBUG,"NOW GET THE 'END OF EVENT' REFERENCE");
    if ((llNxtLine > 0) && (llNxtLine <= llMaxLine))
    {
      /* GETTING THE DEPARTRURE LINE */
      CT_GetMultiValues(pcgFltEvtGrid, llNxtLine, "LINE", pclRefLine);
      CT_GetMultiValues(pcgFltEvtGrid, llNxtLine, "OOOI", pclNxtOooi);
      CT_GetMultiValues(pcgFltEvtGrid, llNxtLine, "RTYP", pclNxtRtyp);
      llRefLine = atol(pclRefLine);
	  if (strcmp(pclNxtOooi,"DEP") == 0)
      {
        llAftDepLine = llRefLine;
        if (strcmp(pclNxtRtyp,"FLT") == 0)
        {
          dbg(TRACE,"LINE %d: REF AFT LINE=%d (OOOI=%s RTYP=%s) FLIGHT DEP",llNxtLine,llRefLine,pclNxtOooi,pclNxtRtyp);
          ilDepTypeIsFlt = TRUE;
          ilDepTypeIsTow = FALSE;
        }
        else if (strcmp(pclNxtRtyp,"TOW") == 0)
        {
          dbg(TRACE,"LINE %d: REF AFT LINE=%d (OOOI=%s RTYP=%s) TOWING DEP",llNxtLine,llRefLine,pclNxtOooi,pclNxtRtyp);
          ilDepTypeIsFlt = FALSE;
          ilDepTypeIsTow = TRUE;
        }
        llEvtLine = llNxtLine;
      }
	  else if (strcmp(pclNxtOooi,"ARR") == 0)
      {
        dbg(TRACE,"MISSING DEP EVENT (NOK)");
        /* Get Same Line Again to start with ARR*/
        llEvtLine = llNxtLine - 1;
        llAftDepLine = -1;
      }
    }
    else
    {
      dbg(TRACE,"THAT'S NOT A PAIR OF EVENTS (THERE IS NO 'NEXT LINE')");
    }

    if (llAftArrLine >= 0)
    {
      dbg(DEBUG,"WE GOT AN ARRIVAL LINE");
      CT_GetMultiValues(pcgAftTabGrid, llAftArrLine, "PSTA", pclAftArrPsta);
      CT_GetMultiValues(pcgAftTabGrid, llAftArrLine, "STOA", pclAftArrStoa);
      CT_GetMultiValues(pcgAftTabGrid, llAftArrLine, "ETAI", pclAftArrEtai);
      CT_GetMultiValues(pcgAftTabGrid, llAftArrLine, "ONBL", pclAftArrOnbl);
      CT_GetMultiValues(pcgAftTabGrid, llAftArrLine, "TIFA", pclAftArrTifa);
      CT_GetMultiValues(pcgAftTabGrid, llAftArrLine, "TISA", pclAftArrTisa);
      CT_GetMultiValues(pcgAftTabGrid, llAftArrLine, "USEC", pclAftArrUsec);
      if (pclAftArrPsta[0] == '\0')
      {
        dbg(TRACE,"ATTENTION: NO ARR PARKING STAND ASSIGNED");
        strcpy(pclAftArrPsta, "---");
      }
      if (pclAftArrEtai[0] == '\0')
      {
        strcpy(pclAftArrEtai,pclAftArrStoa);
      }
    }
    else
    {
      dbg(DEBUG,"WE DID NOT GET AN ARRIVAL LINE");
      strcpy(pclAftArrPsta,"");
      strcpy(pclAftArrStoa,"");
      strcpy(pclAftArrEtai,"");
      strcpy(pclAftArrOnbl,"");
      strcpy(pclAftArrTifa,"");
      strcpy(pclAftArrTisa,"");
      strcpy(pclAftArrUsec,"");
    }
    if (llAftDepLine >= 0)
    {
      dbg(DEBUG,"WE GOT A DEPARTURE LINE");
      CT_GetMultiValues(pcgAftTabGrid, llAftDepLine, "PSTD", pclAftDepPstd);
      CT_GetMultiValues(pcgAftTabGrid, llAftDepLine, "STOD", pclAftDepStod);
      CT_GetMultiValues(pcgAftTabGrid, llAftDepLine, "ETDI", pclAftDepEtdi);
      CT_GetMultiValues(pcgAftTabGrid, llAftDepLine, "OFBL", pclAftDepOfbl);
      CT_GetMultiValues(pcgAftTabGrid, llAftDepLine, "TIFD", pclAftDepTifd);
      CT_GetMultiValues(pcgAftTabGrid, llAftDepLine, "TISD", pclAftDepTisd);
      CT_GetMultiValues(pcgAftTabGrid, llAftDepLine, "USEC", pclAftDepUsec);
      if (pclAftDepPstd[0] == '\0')
      {
        dbg(TRACE,"ATTENTION: NO DEP PARKING STAND ASSIGNED");
        strcpy(pclAftDepPstd, "---");
      }
      if (pclAftDepEtdi[0] == '\0')
      {
        strcpy(pclAftDepEtdi,pclAftDepStod);
      }
    }
    else
    {
      dbg(DEBUG,"WE DID NOT GET A DEPARTURE LINE");
      strcpy(pclAftDepPstd,"");
      strcpy(pclAftDepStod,"");
      strcpy(pclAftDepEtdi,"");
      strcpy(pclAftDepOfbl,"");
      strcpy(pclAftDepTifd,"");
      strcpy(pclAftDepTisd,"");
      strcpy(pclAftDepUsec,"");
    }

    ilUpdateGorLine = TRUE;

    if (ilArrTypeIsFlt == TRUE)
    {
      strcpy(pclCurTifa,pclAftArrTifa);
    }
    if ((llAftArrLine >= 0) && (llAftDepLine >= 0))
    {
      if ((ilArrTypeIsFlt == TRUE) && (ilDepTypeIsFlt == TRUE))
      {
        dbg(TRACE,"EVALUATE: PLAIN ARR/DEP FLIGHT TURNAROUND");
        dbg(TRACE,"POSITION: ARR FLT PSTA <%s> DEP FLT PSTD <%s>",pclAftArrPsta,pclAftDepPstd);
        strcpy(pclGorTabRnam, pclAftArrPsta);
        strcpy(pclGorTabRabs, pclAftArrStoa);
        strcpy(pclGorTabRaes, pclAftDepStod);
        strcpy(pclGorTabRabe, pclAftArrEtai);
        strcpy(pclGorTabRaee, pclAftDepEtdi);
        strcpy(pclGorTabRaba, pclAftArrOnbl);
        strcpy(pclGorTabRaea, pclAftDepOfbl);
        strcpy(pclGorTabTifb, pclAftArrTifa);
        strcpy(pclGorTabTife, pclAftDepTifd);
        strcpy(pclGorTabTisb, pclAftArrTisa);
        strcpy(pclGorTabTise, pclAftDepTisd);
      }
      else if ((ilArrTypeIsFlt == TRUE) && (ilDepTypeIsTow == TRUE))
      {
        dbg(TRACE,"EVALUATE: ARR FLIGHT EVENT WITH TOWING (TOW-OUT)");
        dbg(TRACE,"POSITION: ARR FLT PSTA <%s> TOW DEP PSTD <%s>",pclAftArrPsta,pclAftDepPstd);
        strcpy(pclGorTabRnam, pclAftArrPsta);
        strcpy(pclGorTabRabs, pclAftArrStoa);
        strcpy(pclGorTabRaes, pclAftDepStod);
        strcpy(pclGorTabRabe, pclAftArrEtai);
        strcpy(pclGorTabRaee, pclAftDepEtdi);
        strcpy(pclGorTabRaba, pclAftArrOnbl);
        strcpy(pclGorTabRaea, pclAftDepOfbl);
        strcpy(pclGorTabTifb, pclAftArrTifa);
        strcpy(pclGorTabTife, pclAftDepTifd);
        strcpy(pclGorTabTisb, pclAftArrTisa);
        strcpy(pclGorTabTise, pclAftDepTisd);
      }
      else if ((ilArrTypeIsTow == TRUE) && (ilDepTypeIsTow == TRUE))
      {
        dbg(TRACE,"EVALUATE: EVENT WITH TWO TOWINGS");
        dbg(TRACE,"POSITION: TOW ARR PSTA <%s> TOW DEP PSTD <%s>",pclAftArrPsta,pclAftDepPstd);
        if (strcmp(pclAftArrUsec,"AutoTowArr") == 0)
        {
          strcpy(pclGorTabOooi,"TWA");
        }
        else
        {
          strcpy(pclGorTabOooi,"TOW");
        }
        strcpy(pclGorTabRnam, pclAftArrPsta);
        strcpy(pclGorTabRabs, pclAftArrStoa);
        strcpy(pclGorTabRaes, pclAftDepStod);
        strcpy(pclGorTabRabe, pclAftArrEtai);
        strcpy(pclGorTabRaee, pclAftDepEtdi);
        strcpy(pclGorTabRaba, pclAftArrOnbl);
        strcpy(pclGorTabRaea, pclAftDepOfbl);
        strcpy(pclGorTabTifb, pclAftArrTifa);
        strcpy(pclGorTabTife, pclAftDepTifd);
        strcpy(pclGorTabTisb, pclAftArrTisa);
        strcpy(pclGorTabTise, pclAftDepTisd);
      }
      else if ((ilArrTypeIsTow == TRUE) && (ilDepTypeIsFlt == TRUE))
      {
        dbg(TRACE,"EVALUATE: EVENT WITH TOWING AND DEP FLIGHT (TOW-IN)");
        dbg(TRACE,"POSITION: TOW ARR PSTA <%s> DEP FLT PSTD <%s>",pclAftArrPsta,pclAftDepPstd);
        if (strcmp(pclAftArrUsec,"AutoTowDep") == 0)
        {
          strcpy(pclGorTabOooi,"TWD");
        }
        else
        {
          strcpy(pclGorTabOooi,"TOW");
        }
        strcpy(pclGorTabRnam, pclAftArrPsta);
        strcpy(pclGorTabRabs, pclAftArrStoa);
        strcpy(pclGorTabRaes, pclAftDepStod);
        strcpy(pclGorTabRabe, pclAftArrEtai);
        strcpy(pclGorTabRaee, pclAftDepEtdi);
        strcpy(pclGorTabRaba, pclAftArrOnbl);
        strcpy(pclGorTabRaea, pclAftDepOfbl);
        strcpy(pclGorTabTifb, pclAftArrTifa);
        strcpy(pclGorTabTife, pclAftDepTifd);
        strcpy(pclGorTabTisb, pclAftArrTisa);
        strcpy(pclGorTabTise, pclAftDepTisd);
      }
      else
      {
        dbg(TRACE,"ATTENTION: GOT AN UNEXPECTED EVENT COMBINATION");
        strcpy(pclGorTabRabs, pclAftArrStoa);
        strcpy(pclGorTabRaes, pclAftDepStod);
        strcpy(pclGorTabRabe, pclAftArrEtai);
        strcpy(pclGorTabRaee, pclAftDepEtdi);
        strcpy(pclGorTabRaba, pclAftArrOnbl);
        strcpy(pclGorTabRaea, pclAftDepOfbl);
        strcpy(pclGorTabTifb, pclAftArrTifa);
        strcpy(pclGorTabTife, pclAftDepTifd);
        strcpy(pclGorTabTisb, pclAftArrTisa);
        strcpy(pclGorTabTise, pclAftDepTisd);
      }
    }
    else if (llAftArrLine >= 0)
    {
      if (ilArrTypeIsFlt == TRUE)
      {
        dbg(TRACE,"EVALUATE: SINGLE ARRIVAL FLIGHT EVENT (OPEN LEG)");
      }
      if (ilDepTypeIsTow == TRUE)
      {
        dbg(TRACE,"EVALUATE: OPEN END TOWING EVENT (LONG TERM PARKING)");
      }
      strcpy(pclTmpData,"20201231235959");
      strcpy(pclGorTabRnam, pclAftArrPsta);
      strcpy(pclGorTabRabs, pclAftArrStoa);
      strcpy(pclGorTabRaes, pclTmpData);
      strcpy(pclGorTabRabe, pclAftArrEtai);
      strcpy(pclGorTabRaee, pclTmpData);
      strcpy(pclGorTabRaba, pclAftArrOnbl);
      strcpy(pclGorTabRaea, " ");
      strcpy(pclGorTabTifb, pclAftArrTifa);
      strcpy(pclGorTabTife, pclTmpData);
      strcpy(pclGorTabTisb, pclAftArrTisa);
      strcpy(pclGorTabTise, " ");
    }
    else if (llAftDepLine >= 0)
    {
      dbg(TRACE,"EVALUATE: SINGLE DEPRATURE FLIGHT EVENT");
      CT_GetMultiValues(pcgAftTabGrid, llAftDepLine, "PDBS", pclAftDepPdbs);
      CT_GetMultiValues(pcgAftTabGrid, llAftDepLine, "PDES", pclAftDepPdes);
      ilGetRc = DateDiffToMin(pclAftDepPdes, pclAftDepPdbs, &ilTimeDif);
      if (ilGetRc == RC_SUCCESS)
      {
        ilTimeDif += ilTimeDif;
      }
      else
      {
        ilTimeDif = 60;
      }
      dbg(TRACE,"PDES <%s> - PDBS <%s> = DIFF(*2) = %d",pclAftDepPdes,pclAftDepPdbs,ilTimeDif);

      strcpy(pclGorTabRnam, pclAftDepPstd);
      strcpy(pclGorTabRaes, pclAftDepStod);
      ilGetRc = GOR_GetNewDateTimeStrg(pclGorTabRabs, pclGorTabRaes, -ilTimeDif);
      strcpy(pclGorTabRaee, pclAftDepEtdi);
      ilGetRc = GOR_GetNewDateTimeStrg(pclGorTabRabe, pclGorTabRaee, -ilTimeDif);
      strcpy(pclGorTabRaba, " ");
      strcpy(pclGorTabRaea, pclAftDepOfbl);
      strcpy(pclGorTabTife, pclAftDepTifd);
      ilGetRc = GOR_GetNewDateTimeStrg(pclGorTabTifb, pclGorTabTife, -ilTimeDif);
      strcpy(pclGorTabTisb, " ");
      strcpy(pclGorTabTise, pclAftDepTisd);
    }
    else
    {
      dbg(TRACE,"ATTENTION: IT'S ALL WRONG SOMEHOW");
      ilUpdateGorLine = FALSE;
    }

    if (llGorLine >= 0)
    {
      if (ilUpdateGorLine == TRUE)
      {
        ilSqnoVal++;
        sprintf(pclGorTabGsqn,"%0.6d",ilGsqnVal);
        sprintf(pclGorTabSqno,"%0.4d",ilSqnoVal);
        strcpy(pclGorTabRsbt,pclEvtRsbt);
        dbg(TRACE,"UPD INFO: TIME <%s> GSQN/SQNO <%s> <%s> OOOI <%s>",pclGorTabRsbt,pclGorTabGsqn,pclGorTabSqno,pclGorTabOooi);

        if (llAftArrLine >= 0)
        {
          /* dbg(TRACE,"GOT REFERENCE TO ARR FLIGHT IN AFT DATA GRID LINE");        */
          /* CT_GetMultiValues(pcgAftTabGrid, llAftArrLine, "URNO", pclArrFltUrno); */
          /* CT_GetMultiValues(pcgAftTabGrid, llAftArrLine, "FTYP", pclArrFltFtyp); */
          /* CT_GetMultiValues(pcgAftTabGrid, llAftArrLine, "ADID", pclArrFltAdid); */
          /* CT_GetMultiValues(pcgAftTabGrid, llAftArrLine, "TIFA", pclArrFltTifa); */
          ilSetRecDelFlg = TRUE;
          if (strstr("OS",pclArrFltFtyp) == NULL)
          {
            dbg(TRACE,"ATTENTION: ARR FLIGHT FTYP <%s> IS NOT OPERATIONAL",pclArrFltFtyp);
            ilSetRecDelFlg = TRUE;
          }

          if (strcmp(pclGorTabTifb,pclArrFltTifa) < 0)
          {
            dbg(TRACE,"ATTENTION: GOR TIFB <%s> IS LESS THEN AFT TIFA <%s>",pclGorTabTifb,pclArrFltTifa);
          }
        }

        if (llNxtDepLine >= 0)
        {
          /* dbg(TRACE,"GOT REFERENCE TO DEP FLIGHT IN AFT DATA GRID LINE");        */
          /* CT_GetMultiValues(pcgAftTabGrid, llNxtDepLine, "URNO", pclDepFltUrno); */
          /* CT_GetMultiValues(pcgAftTabGrid, llNxtDepLine, "FTYP", pclDepFltFtyp); */
          /* CT_GetMultiValues(pcgAftTabGrid, llNxtDepLine, "ADID", pclDepFltAdid); */
          /* CT_GetMultiValues(pcgAftTabGrid, llNxtDepLine, "TIFD", pclDepFltTifd); */

        }

        ilGetRc = DateDiffToMin(pclGorTabTife, pclGorTabTifb, &ilOtimVal);
        sprintf(pclGorTabOtim,"%d",ilOtimVal);
        dbg(TRACE,"THE DURATION OF STAND USAGE ON <%s> IS <%s> MINUTES (OTIM)",pclGorTabRnam,pclGorTabOtim);

        strcpy(pclGorTabAurn,pclCurAurn);
        strcpy(pclGorTabDurn,pclCurDurn);
        dbg(TRACE,"UPD INFO: AURN/DURN = ARR URNO <%s> DEP URNO <%s>",pclGorTabAurn,pclGorTabDurn);


        dbg(TRACE,"UPD INFO: RABS/RAES = <%s> <%s>",pclGorTabRabs,pclGorTabRaes);
        dbg(TRACE,"UPD INFO: RABE/RAEE = <%s> <%s>",pclGorTabRabe,pclGorTabRaee);
        dbg(TRACE,"UPD INFO: RABA/RAEA = <%s> <%s>",pclGorTabRaba,pclGorTabRaea);
        dbg(TRACE,"UPD INFO: TIFB/TIFE = <%s> <%s>",pclGorTabTifb,pclGorTabTife);
        dbg(TRACE,"UPD INFO: TISB/TISE = <%s> <%s>",pclGorTabTisb,pclGorTabTise);

        if (pclGorTabRnam[0] == '\0')
        {
          dbg(TRACE,"ATTENTION: NO PARKING STAND ASSIGNED");
          strcpy(pclGorTabRnam, "---");
        }

        CT_SetMultiValues(pcgGorTabGrid, llGorLine, "RTYP", pclCurRtyp);
        CT_SetMultiValues(pcgGorTabGrid, llGorLine, "RNAM", pclGorTabRnam);

        CT_SetMultiValues(pcgGorTabGrid, llGorLine, "AURN", pclGorTabAurn);
        CT_SetMultiValues(pcgGorTabGrid, llGorLine, "DURN", pclGorTabDurn);

        CT_SetMultiValues(pcgGorTabGrid, llGorLine, "RSBT", pclGorTabRsbt);
        CT_SetMultiValues(pcgGorTabGrid, llGorLine, "GSQN", pclGorTabGsqn);
        CT_SetMultiValues(pcgGorTabGrid, llGorLine, "SQNO", pclGorTabSqno);
        CT_SetMultiValues(pcgGorTabGrid, llGorLine, "OOOI", pclGorTabOooi);

        CT_SetMultiValues(pcgGorTabGrid, llGorLine, "RABS", pclGorTabRabs);
        CT_SetMultiValues(pcgGorTabGrid, llGorLine, "RAES", pclGorTabRaes);
        CT_SetMultiValues(pcgGorTabGrid, llGorLine, "RABE", pclGorTabRabe);
        CT_SetMultiValues(pcgGorTabGrid, llGorLine, "RAEE", pclGorTabRaee);
        CT_SetMultiValues(pcgGorTabGrid, llGorLine, "RABA", pclGorTabRaba);
        CT_SetMultiValues(pcgGorTabGrid, llGorLine, "RAEA", pclGorTabRaea);
        CT_SetMultiValues(pcgGorTabGrid, llGorLine, "TIFB", pclGorTabTifb);
        CT_SetMultiValues(pcgGorTabGrid, llGorLine, "TIFE", pclGorTabTife);

        CT_SetMultiValues(pcgGorTabGrid, llGorLine, "TISB", pclGorTabTisb);
        CT_SetMultiValues(pcgGorTabGrid, llGorLine, "TISE", pclGorTabTise);
        CT_SetMultiValues(pcgGorTabGrid, llGorLine, "OTIM", pclGorTabOtim);

        if (pclGorTabRnam[0] == '\0')
        {
          dbg(TRACE,"ATTENTION: NO PARKING STAND ASSIGNED");
        }
      }
      CT_SetLineCheckedFlag(pcgGorTabGrid, llGorLine, 1);
    }

  }

  dbg(TRACE,"---------------------------------------------");
  return ilRc;
}

/* *************************************************** */
/* *************************************************** */
static long GetNextAftLine(long lpBgnLine, char *pcpForType, char *pcpForOooi)
{
  char pclEvtRtyp[16] = "";
  char pclEvtOooi[16] = "";
  char pclRefLine[16] = "";
  long llEvtLine = 0;
  long llAftLine = 0;
  long llMaxLine = 0;
  llAftLine = -1;
  llEvtLine = lpBgnLine;
  llMaxLine = CT_GetLineCount(pcgFltEvtGrid) - 1;
  while (llEvtLine < llMaxLine)
  {
    llEvtLine++;
    if (llEvtLine <= llMaxLine)
    {
      CT_GetMultiValues(pcgFltEvtGrid, llEvtLine, "OOOI", pclEvtOooi);
      CT_GetMultiValues(pcgFltEvtGrid, llEvtLine, "RTYP", pclEvtRtyp);
      if (strcmp(pclEvtRtyp,pcpForType) == 0)
      {
        if (strcmp(pclEvtOooi,pcpForOooi) == 0)
        {
          CT_GetMultiValues(pcgFltEvtGrid, llEvtLine, "LINE", pclRefLine);
          llAftLine = atol(pclRefLine);
        }
      }
    }
  }
  return llAftLine;
}

/* *************************************************** */
/* *************************************************** */
static int GOR_GetNewDateTimeStrg(char *pcpResult, char *pcpDateTime, int ipMinuteOffset)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  int ilDummy = 0;
  int ilMinVal = 0;
  ilGetRc = GetFullDay(pcpDateTime, &ilDummy, &ilMinVal, &ilDummy);
  ilMinVal += ipMinuteOffset;
  ilGetRc = FullTimeString(pcpResult, ilMinVal, "00");
  return ilRc;
}

/* *************************************************** */
/* *************************************************** */
static int GOR_CreateFlightEventSorter(int ipForWhat)
{
  int ilRc = RC_SUCCESS;
  int ilIsValid = TRUE;
  int ilGotArrTime = TRUE;
  int ilGotDepTime = TRUE;
  int ilGotUseTime = TRUE;
  int ilGotArrUrno = TRUE;
  int ilGotDepUrno = TRUE;
  int ilForWhat = 0;
  char pclAftAdid[8] = "";
  char pclAftFtyp[8] = "";
  char pclEvtRtyp[8] = "";
  char pclEvtOooi[8] = "";
  char pclEvtSort[64] = "";
  char pclNewSort[64] = "";
  char pclRefLine[16] = "";
  char pclUseAurn[16] = "";
  char pclUseDurn[16] = "";
  char pclUseTime[16] = "";
  char pclCurAurn[16] = "";
  char pclCurDurn[16] = "";
  char pclEvtCode[8] = "";
  char pclEvtAurn[16] = "";
  char pclEvtDurn[16] = "";
  long llMaxLine = 0;
  long llAftLine = 0;
  long llEvtLine = 0;
  ilForWhat = ipForWhat;
  if (ilForWhat == 0)
  {
    /* STEP 1: Create the basic sort order */
    /* Sorting by ARR/DEP events along RKEY */
    dbg(TRACE,"SORTER STEP 1: ORDER BY EVENTS AND TIME");
    CT_ResetContent(pcgFltEvtGrid);
    strcpy(pclUseAurn,"0");
    llMaxLine = CT_GetLineCount(pcgAftTabGrid) - 1;
    for (llAftLine=0;llAftLine<=llMaxLine;llAftLine++)
    {
      CT_GetMultiValues(pcgAftTabGrid, llAftLine, "FTYP", pclAftFtyp);
      ilIsValid = TRUE;
      if (strstr("XND",pclAftFtyp) != NULL)
      {
        /* ilIsValid = FALSE; */
        ilIsValid = TRUE;
      }
      if (ilIsValid == TRUE)
      {
        CT_GetMultiValues(pcgAftTabGrid, llAftLine, "ADID", pclAftAdid);
        switch (pclAftAdid[0])
        {
          case 'A':
            strcpy(pclEvtRtyp,"FLT");
            strcpy(pclEvtCode,"AFLT");
            CT_GetMultiValues(pcgAftTabGrid, llAftLine, "URNO", pclUseAurn);
            llEvtLine = GOR_CreateFltEvtRecord(0, llAftLine, "A", pclEvtRtyp, pclEvtCode, pclUseAurn);
          break;
          case 'B':
            if (strstr("TG",pclAftFtyp) != NULL)
            {
              strcpy(pclEvtRtyp,"TOW");
              strcpy(pclEvtCode,"BMOV");
              CT_GetMultiValues(pcgAftTabGrid, llAftLine, "AURN", pclUseAurn);
              llEvtLine = GOR_CreateFltEvtRecord(0, llAftLine, "A", pclEvtRtyp, pclEvtCode, pclUseAurn);
              strcpy(pclEvtCode,"BMOV");
              llEvtLine = GOR_CreateFltEvtRecord(0, llAftLine, "D", pclEvtRtyp, pclEvtCode, pclUseAurn);
            }
            else
            {
              strcpy(pclEvtRtyp,"FLT");
              strcpy(pclEvtCode,"AFLT");
              CT_GetMultiValues(pcgAftTabGrid, llAftLine, "URNO", pclUseAurn);
              llEvtLine = GOR_CreateFltEvtRecord(0, llAftLine, "A", pclEvtRtyp, pclEvtCode, pclUseAurn);
              CT_GetMultiValues(pcgAftTabGrid, llAftLine, "AURN", pclUseAurn);
              if (pclUseAurn[0] == '\0')
              {
                CT_GetMultiValues(pcgAftTabGrid, llAftLine, "URNO", pclUseAurn);
              }
              strcpy(pclEvtCode,"DFLT");
              llEvtLine = GOR_CreateFltEvtRecord(0, llAftLine, "D", pclEvtRtyp, pclEvtCode, pclUseAurn);
            }
          break;
          case 'D':
            strcpy(pclEvtRtyp,"FLT");
            strcpy(pclEvtCode,"DFLT");
            CT_GetMultiValues(pcgAftTabGrid, llAftLine, "AURN", pclUseAurn);
            if (pclUseAurn[0] == '\0')
            {
              CT_GetMultiValues(pcgAftTabGrid, llAftLine, "URNO", pclUseAurn);
            }
            llEvtLine = GOR_CreateFltEvtRecord(0, llAftLine, "D", pclEvtRtyp, pclEvtCode, pclUseAurn);
          break;
          default:
          break;
        }
      }
    }
    CT_SortByColumnNames(pcgFltEvtGrid, "SORT", SORT_ASC);
    ilForWhat = 99;
    ShowEvtSorter(ilForWhat);
    ilForWhat = 1;
  }
  if (ilForWhat == 1)
  {
    dbg(TRACE,"SORTER STEP 2: GROUP BY AURN REF (FLIGHT AND RETURN FLIGHTS)");
    ilGotArrTime = FALSE;
    ilGotDepTime = FALSE;
    ilGotArrUrno = FALSE;
    strcpy(pclUseTime,"00000000000000");
    llMaxLine = CT_GetLineCount(pcgFltEvtGrid) - 1;
    for (llEvtLine=0;llEvtLine<=llMaxLine;llEvtLine++)
    {
      ilGotUseTime = FALSE;
      CT_GetMultiValues(pcgFltEvtGrid, llEvtLine, "SORT", pclEvtSort);
      CT_GetMultiValues(pcgFltEvtGrid, llEvtLine, "RTYP", pclEvtRtyp);
      CT_GetMultiValues(pcgFltEvtGrid, llEvtLine, "OOOI", pclEvtOooi);
      CT_GetMultiValues(pcgFltEvtGrid, llEvtLine, "AURN", pclEvtAurn);
      if ((strcmp(pclEvtRtyp,"FLT") == 0) && (strcmp(pclEvtOooi,"ARR") == 0))
      {
        CT_GetMultiValues(pcgFltEvtGrid, llEvtLine, "LINE", pclRefLine);
        llAftLine = atol(pclRefLine);
        CT_GetMultiValues(pcgAftTabGrid, llAftLine, "STOA", pclUseTime);
        CT_GetMultiValues(pcgAftTabGrid, llAftLine, "URNO", pclCurAurn);
        ilGotArrTime = TRUE;
        ilGotDepTime = FALSE;
        ilGotUseTime = TRUE;
        ilGotArrUrno = TRUE;
      }
      if (ilGotArrTime == FALSE)
      {
        if ((strcmp(pclEvtRtyp,"FLT") == 0) && (strcmp(pclEvtOooi,"DEP") == 0))
        {
          CT_GetMultiValues(pcgFltEvtGrid, llEvtLine, "LINE", pclRefLine);
          llAftLine = atol(pclRefLine);
          CT_GetMultiValues(pcgAftTabGrid, llAftLine, "STOD", pclUseTime);
          ilGotArrTime = FALSE;
          ilGotDepTime = TRUE;
          ilGotUseTime = TRUE;
        }
      }
      if ((ilGotArrTime == FALSE) && (ilGotDepTime == FALSE))
      {
        if (ilGotUseTime == FALSE)
        {
          if ((strcmp(pclEvtRtyp,"TOW") == 0) && (strcmp(pclEvtOooi,"DEP") == 0))
          {
            CT_GetMultiValues(pcgFltEvtGrid, llEvtLine, "LINE", pclRefLine);
            llAftLine = atol(pclRefLine);
            CT_GetMultiValues(pcgAftTabGrid, llAftLine, "STOD", pclUseTime);
            ilGotUseTime = TRUE;
          }
        }
        if (ilGotUseTime == FALSE)
        {
          if ((strcmp(pclEvtRtyp,"TOW") == 0) && (strcmp(pclEvtOooi,"ARR") == 0))
          {
            CT_GetMultiValues(pcgFltEvtGrid, llEvtLine, "LINE", pclRefLine);
            llAftLine = atol(pclRefLine);
            CT_GetMultiValues(pcgAftTabGrid, llAftLine, "STOA", pclUseTime);
            ilGotUseTime = TRUE;
          }
        }
      }
      if (ilGotArrUrno == TRUE)
      {
        if (strcmp(pclCurAurn,pclEvtAurn) != 0)
        {
          dbg(TRACE,"ATTENTION: AURN MISMATCH FLT <%s> TOW <%s>",pclCurAurn,pclEvtAurn);
          strcpy(pclEvtAurn,pclCurAurn);
          CT_SetMultiValues(pcgFltEvtGrid, llEvtLine, "AURN", pclEvtAurn);
        }
      }
      sprintf(pclNewSort,"%s-%s",pclUseTime,pclEvtSort);
      CT_SetMultiValues(pcgFltEvtGrid, llEvtLine, "SORT", pclNewSort);
      CT_SetMultiValues(pcgFltEvtGrid, llEvtLine, "RSBT", pclUseTime);
    }
    dbg(TRACE,"SORTER STEP 3: SET THE REFERENCE TO DEPARTURE FLIGHTS");
    /* Run backwards to set the DURN value of all turnaround lines */
    strcpy(pclUseDurn,"0");
    llMaxLine = CT_GetLineCount(pcgFltEvtGrid) - 1;
    for (llEvtLine=llMaxLine;llEvtLine>=0;llEvtLine--)
    {
      CT_GetMultiValues(pcgFltEvtGrid, llEvtLine, "RTYP", pclEvtRtyp);
      CT_GetMultiValues(pcgFltEvtGrid, llEvtLine, "OOOI", pclEvtOooi);
      if ((strcmp(pclEvtRtyp,"FLT") == 0) && (strcmp(pclEvtOooi,"DEP") == 0))
      {
        CT_GetMultiValues(pcgFltEvtGrid, llEvtLine, "LINE", pclRefLine);
        llAftLine = atol(pclRefLine);
        CT_GetMultiValues(pcgAftTabGrid, llAftLine, "URNO", pclUseDurn);
      }
      CT_SetMultiValues(pcgFltEvtGrid, llEvtLine, "DURN", pclUseDurn);
    }
    CT_SortByColumnNames(pcgFltEvtGrid, "SORT", SORT_ASC);
    ilForWhat = 99;
    ShowEvtSorter(ilForWhat);
  }
  return ilRc;
}

/* *************************************************** */
static int ShowEvtSorter(int ipForWhat)
{
  int ilRc = RC_SUCCESS;
  long llMaxLine = 0;
  long llEvtLine = 0;
  if (ipForWhat == 99)
  {
    dbg(TRACE,"----------------------------------------------------------");
    dbg(TRACE,"FLIGHT EVENTS OF FETCHED ROTATION SORTED BY TIME/AURN/STOA/STOD");
    dbg(TRACE,"(FIELDS: %s)",pcgFltEvtFldList);
    dbg(TRACE,"----------------------------------------------------------");
    llMaxLine = CT_GetLineCount(pcgFltEvtGrid) - 1;
    for (llEvtLine=0;llEvtLine<=llMaxLine;llEvtLine++)
    {
      CT_GetLineValues(pcgFltEvtGrid, llEvtLine, &rgCtFlLine);
      dbg(TRACE,"<%s>",rgCtFlLine.Value);
    }
    dbg(TRACE,"---------------------------------------------");
  }
  return ilRc;
}

/* *************************************************** */
static long GOR_CreateFltEvtRecord(int ipForWhat, long lpAftLine, char *pcpEvtAdid, char *pcpEvtRtyp, char *pcpEvtCode, char *pcpUseAurn)
{
  int ilRc = RC_SUCCESS;
  char pclFltEvtFldBase[] = "SORT,LINE,OOOI,RTYP,STND";
  char pclFltEvtFldInfo[] = "AURN,URNO,FTYP,ADID";
  char pclAftTime[16] = "";
  char pclAftStnd[16] = "";
  char pclEvtSort[64] = "";
  char pclEvtData[512] = "";
  long llEvtLine = 0;
  CT_InsertTextLine(pcgFltEvtGrid,pcgFltEvtNulData);
  llEvtLine = CT_GetLineCount(pcgFltEvtGrid) - 1;
  switch (pcpEvtAdid[0])
  {
    case 'A':
      CT_GetMultiValues(pcgAftTabGrid, lpAftLine, "STOA", pclAftTime);
      CT_GetMultiValues(pcgAftTabGrid, lpAftLine, "PSTA", pclAftStnd);
      if (pclAftStnd[0] == '\0')
      {
        strcpy(pclAftStnd, "---");
      }
      sprintf(pclEvtSort,"%s_%s",pcpEvtCode,pclAftTime);
      sprintf(pclEvtData,"%s,%d,ARR,%s,%s",pclEvtSort,lpAftLine,pcpEvtRtyp,pclAftStnd);
      /*CT_SetMultiValues(pcgFltEvtGrid, llEvtLine, "RSBT", pclAftTime);*/
      CT_SetMultiValues(pcgFltEvtGrid, llEvtLine, pclFltEvtFldBase, pclEvtData);
    break;
    case 'D':
      CT_GetMultiValues(pcgAftTabGrid, lpAftLine, "STOD", pclAftTime);
      CT_GetMultiValues(pcgAftTabGrid, lpAftLine, "PSTD", pclAftStnd);
      if (pclAftStnd[0] == '\0')
      {
        strcpy(pclAftStnd, "---");
      }
      sprintf(pclEvtSort,"%s_%s",pcpEvtCode,pclAftTime);
      sprintf(pclEvtData,"%s,%d,DEP,%s,%s",pclEvtSort,lpAftLine,pcpEvtRtyp,pclAftStnd);
      /*CT_SetMultiValues(pcgFltEvtGrid, llEvtLine, "RSBT", pclAftTime);*/
      CT_SetMultiValues(pcgFltEvtGrid, llEvtLine, pclFltEvtFldBase, pclEvtData);
    break;
    default:
    break;
  }

  CT_GetMultiValues(pcgAftTabGrid, lpAftLine, pclFltEvtFldInfo, pclEvtData);
  CT_SetMultiValues(pcgFltEvtGrid, llEvtLine, pclFltEvtFldInfo, pclEvtData);
  CT_GetMultiValues(pcgFltEvtGrid, llEvtLine, "AURN", pclEvtData);
  if (strlen(pclEvtData) == 0)
  {
    strcpy(pclEvtData,pcpUseAurn);
  }
  CT_SetFieldValues(pcgFltEvtGrid, llEvtLine, "AURN", pclEvtData);  

  return llEvtLine;
}

/* *************************************************** */
/* The relation between AFT and GOR records usually is */
/* one to n, where "n" depends on the type of resource */
/* and the ARR/DEP/TOW status (FTYP) of the AFT record.*/
/* This funtion identifies the GOR record that matches */
/* the actual data of the given AFT record.            */
/* *************************************************** */
static long GOR_GetMatchingLineNo(long lpAftLine, char *pcpAftAdid, 
                                  char *pclHitList, char *pclGetOooi, char *pclGetRtyp)
{
  int ilLen = 0;
  long llCnt = 0;
  long llIdx = 0;
  long llAftLine = 0;
  long llCurLine = -1;
  long llGorLine = -1;
  long llLineNo = 0;
  char pclItemData[16] = "";
  char pclAftAdid[8] = "";
  char pclAftFtyp[8] = "";
  char pclGorRtyp[8] = "";
  char pclGorOooi[8] = "";
  llAftLine = lpAftLine;
  CT_GetMultiValues(pcgAftTabGrid, llAftLine, "ADID", pclAftAdid);

  llCnt = CT_CountPattern(pclHitList, ",");
  llIdx = 0;
  while ((llCnt >= 0) && (llGorLine < 0))
  {
    ilLen = CT_GetItemsFromTo(pclItemData, pclHitList, llIdx, llIdx, ",");    
    if (ilLen > 0)
    {
      llLineNo = atol(pclItemData);
      if (llLineNo >= 0)
      {
        llCurLine = llLineNo;
        CT_GetMultiValues(pcgGorTabGrid, llCurLine, "RTYP", pclGorRtyp);
        ilLen = FLT_TrimRecordData(pclGorRtyp, ",");
        if (ilLen < 3)
        {
          strcpy(pclGorRtyp,pclGetRtyp);
        }
        CT_GetMultiValues(pcgGorTabGrid, llCurLine, "OOOI", pclGorOooi);
        ilLen = FLT_TrimRecordData(pclGorOooi, ",");
        if (ilLen < 3)
        {
          strcpy(pclGorOooi,pclGetOooi);
        }
        
        if ((strcmp(pclGorOooi,pclGetOooi) == 0) &&
            (strcmp(pclGorRtyp,pclGetRtyp) == 0))
        {
          llGorLine = llCurLine;
          dbg(DEBUG,"FOUND GOR LINE %d WITH <%s> <%s>",llGorLine,pclGorOooi,pclGorRtyp);
        }
      }
    }
    llIdx++;
    llCnt--;
  }
  return llGorLine;
}

/* *************************************************** */
/* *************************************************** */
static long GOR_SetFlightRelation(char *pcpForAdid, char *pcpForFtyp, long lpAftLine, long lpGorLine)
{
  int ilRc = RC_SUCCESS;
  long llSrcLine = 0;
  long llDstLine = 0;
  char pclSyncData[1024] = "";
  char pclFrds[16] = "";
  char pclAurn[16] = "";
  llSrcLine = lpAftLine;
  llDstLine = lpGorLine;
  if (llDstLine < 0)
  {
    CT_InsertTextLine(pcgGorTabGrid,pcgGorTabNulData);
    llDstLine = CT_GetLineCount(pcgGorTabGrid) - 1;
    CT_SetOldData(pcgGorTabGrid, llDstLine, pcgGorTabNulData);
    CT_SetLineDbInsFlag(pcgGorTabGrid, llDstLine, 1);
  }
  switch (pcpForAdid[0])
  {
    case 'A':
      dbg(DEBUG,"AFT GET ARR <%s>",pcgAftGetArrList);
      dbg(DEBUG,"GOR SET ARR <%s>",pcgGorSetArrList);
      CT_GetMultiValues(pcgAftTabGrid, llSrcLine, pcgAftGetArrList, pclSyncData);
      CT_SetFieldValues(pcgGorTabGrid, llDstLine, pcgGorSetArrList, pclSyncData);
      dbg(DEBUG,"GOR DAT ARR <%s>",pclSyncData);
    break;
    case 'D':
      dbg(DEBUG,"AFT GET DEP <%s>",pcgAftGetDepList);
      dbg(DEBUG,"GOR SET DEP <%s>",pcgGorSetDepList);
      CT_GetMultiValues(pcgAftTabGrid, llSrcLine, pcgAftGetDepList, pclSyncData);
      CT_SetFieldValues(pcgGorTabGrid, llDstLine, pcgGorSetDepList, pclSyncData);
      dbg(DEBUG,"GOR DAT DEP <%s>",pclSyncData);
    break;
    default:
      dbg(TRACE,"GORTAB: UNDEFINED ADID VALUE <%s>",pcpForAdid);
    break;
  }
  CT_GetMultiValues(pcgGorTabGrid, llDstLine, "FLNO", pclFrds);
  if (strlen(pclFrds) == 0)
  {
    if ((pcpForFtyp[0] != 'T') && (pcpForFtyp[0] != 'G'))
    {
      CT_GetMultiValues(pcgGorTabGrid, llDstLine, "CSGN", pclFrds);
    }
  }
  CT_SetFieldValues(pcgGorTabGrid, llDstLine, "FRDS", pclFrds);  

  CT_GetMultiValues(pcgGorTabGrid, llDstLine, "AURN", pclAurn);
  if (strlen(pclAurn) == 0)
  {
    strcpy(pclAurn,"0");
  }
  CT_SetFieldValues(pcgGorTabGrid, llDstLine, "AURN", pclAurn);  
  
  return llDstLine;
}

/* *************************************************** */
/* *************************************************** */
static int GOR_CheckDbAction(char *pcpGridName, char *pcpTableName, int ipForWhat)
{
  int ilRc = RC_SUCCESS;
  long ilLineChecked = 0;
  char pclFld[4096] = "";
  char pclOld[4096] = "";
  char pclNew[4096] = "";
  char pclTableName[16] = "";
  char pclRangeName[16] = "";
  char pclCurUrno[16] = "";
  char pclNxtUrno[16] = "";
  long llMaxLine = 0;
  long llCurLine = 0;
  long llDbUpdFlag = 0;
  long llDbInsFlag = 0;
  long llDbDelFlag = 0;
  long llChgCount = 0;
  dbg(TRACE,"CHECK DB ACTIONS (INS/UPD/DEL) OF <%s> (%s)",pcpGridName,pcpTableName);

  llMaxLine = CT_GetLineCount(pcpGridName) - 1;
  for (llCurLine=0;llCurLine<=llMaxLine;llCurLine++)
  {
    CT_GetMultiValues(pcpGridName, llCurLine, "URNO", pclCurUrno);
    llDbInsFlag = CT_GetLineDbInsFlag(pcpGridName,llCurLine);
    llDbUpdFlag = CT_GetLineDbUpdFlag(pcpGridName,llCurLine);
    llDbDelFlag = CT_GetLineDbDelFlag(pcpGridName,llCurLine);
    ilLineChecked = CT_GetLineCheckedFlag(pcpGridName,llCurLine);
    dbg(TRACE,"LINE %d: INS=%d UPD=%d DEL=%d (CHECKED=%d)",llCurLine,llDbInsFlag,llDbUpdFlag,llDbDelFlag,ilLineChecked);
    if ((llDbDelFlag > 0) || (ilLineChecked == 0))
    {
      if (ilLineChecked == 0)
      {
		dbg(TRACE,"DELETION OF AN OBSOLETE GORTAB RECORD (URNO <%s>)",pclCurUrno);
      }
      if (llDbInsFlag == 0)
      {
        dbg(TRACE,"-------------------------------");
		dbg(TRACE,"DELETE GOR RECORD (URNO <%s>)",pclCurUrno);
        GOR_DeleteGridRecord2DB(pcpGridName, llCurLine);
      }
      else
      {
		dbg(TRACE,"WON'T DELETE OR INSERT A RECORD WITH 'INS/DEL' FLAGS COMBINATION");
      }
    }
    else if (llDbInsFlag > 0)
    {
      dbg(TRACE,"-------------------------------");
      dbg(TRACE,"INSERT GOR RECORD (URNO <%s>)",pclCurUrno);
      if (strcmp(pclCurUrno,"0") == 0)
      {
        dbg(TRACE,"GET ONE NEW URNO");
        GOR_GetValueFromUrnoPool(pcpGridName, pclNxtUrno);
        dbg(TRACE,"GOT NEW URNO <%s>",pclNxtUrno);
        CT_SetFieldValues(pcpGridName, llCurLine, "URNO", pclNxtUrno);  
        GOR_InsertGridRecord2DB(pcpGridName, llCurLine);
      }
    }
    else if (llDbUpdFlag > 0)
    {
      dbg(TRACE,"-------------------------------");
      dbg(TRACE,"UPDATE GOR RECORD (URNO <%s>)",pclCurUrno);
      GOR_UpdateGridRecord2DB(pcpGridName, llCurLine);
    }
  }
  dbg(TRACE,"-------------------------------");
  return ilRc;
}

/* *************************************************** */
/* *************************************************** */
static int GOR_GetValueFromUrnoPool(char *pcpGridName, char *pcpNxtUrno)
{
  int ilGetRc = 0;
  ilGetRc = GOR_CheckGridUrnoPool(pcpGridName);
  ilGetRc = CT_GetNextGridUrno(pcpGridName, pcpNxtUrno, TRUE);
  return ilGetRc;
}

/* *************************************************** */
/* *************************************************** */
static int GOR_CheckGridUrnoPool(char *pcpGridName)
{
  int ilCnt = 0;
  ilCnt = CT_GetGridUrnoCount(pcpGridName);
  dbg(TRACE,"URNO COUNT IS %d",ilCnt);
  if (ilCnt < 1)
  {
    dbg(TRACE,"MUST GET NEW URNO FROM NUMTAB");
    ilCnt = GOR_FillGridUrnoPool(pcpGridName, 1);
  }
  return ilCnt;
}

/* *************************************************** */
/* *************************************************** */
static int GOR_FillGridUrnoPool(char *pcpGridName, int ipCount)
{
  int ilRc = RC_SUCCESS;
  int ilHandleRanges = FALSE;
  int ilPoolIdx = 0;
  char pclTableName[16] = "";
  char pclNumTabKeys[16] = "";
  char pclNumTabAcnu[32] = "";
  ilHandleRanges = CT_GetUrnoPoolMethod(pcpGridName, pclTableName, pclNumTabKeys);
  dbg(TRACE,"FILL URNO POOL <%s> <%s> <%s> (NEED %d)",pcpGridName, pclTableName, pclNumTabKeys, ipCount);
  if (ilHandleRanges != TRUE)
  {
    dbg(TRACE,"GET %d %s URNO VALUES BY <GetNextValues>",ipCount,pclTableName);
    ilRc = GetNextValues (pclNumTabAcnu, ipCount);
    dbg(TRACE,"NEXT URNO (FIRST VALUE) FROM NUMTAB = <%s>", pclNumTabAcnu);
    ilPoolIdx = CT_SetUrnoPool(pclTableName,pclNumTabKeys, pclNumTabAcnu, ipCount);
    dbg(TRACE,"URNO POOL INDEX <%s><%s> = %d",pclTableName,pclNumTabKeys,ilPoolIdx);
  }
  else
  {
    /* Still to be implemented */
    /* See Flthdl.c */
  }
  return ilRc;
}

/* *************************************************** */
/* *************************************************** */
static int GOR_InsertGridRecord2DB(char *pcpGridName, long lpLineNo)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  char pclSqlTab[16] = "";
  char pclSqlFld[1024] = "";
  char pclSqlVal[1024] = "";
  char pclSqlBuf[1024] = "";
  short slCursor = 0;
  short slFkt = 0;

  CT_GetArrayTableName(pcpGridName, pclSqlTab);
  CT_GetFieldList(pcpGridName, pclSqlFld);
  CT_GetLineValues(pcpGridName, lpLineNo, &rgCtFlLine);
  dbg(TRACE,"NEW GRID RECORD\n<%s>",rgCtFlLine.Value);
  CT_CheckNullValues(rgCtFlLine.Value,rgCtFlData.Value);
  dbg(TRACE,"VALUES CHECKED\n<%s>",rgCtFlData.Value);

  GOR_BuildSqlInsValStrg(pclSqlVal,pclSqlFld);
  sprintf(pclSqlBuf,"INSERT INTO %s (%s) VALUES (%s)",pclSqlTab,pclSqlFld,pclSqlVal);

  dbg(TRACE,"<%s>",pclSqlBuf);

  slCursor = 0;
  slFkt = IGNORE;
  delton(rgCtFlData.Value);
  ilGetRc = sql_if(slFkt, &slCursor, pclSqlBuf, rgCtFlData.Value);
  if (ilGetRc == DB_SUCCESS)
  {
    commit_work ();
    dbg(TRACE,"RECORD INSERTED");
    dbg(TRACE,"===============");
  }
  else
  {
    dbg (TRACE, "ORA INSERT FAILED. ROLLBACK!");
    dbg (TRACE, "============================");
    rollback ();
  }
  close_my_cursor (&slCursor);

  return ilRc;
}

/* *************************************************** */
/* *************************************************** */
static int GOR_UpdateGridRecord2DB(char *pcpGridName, long lpLineNo)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  long llChgCount = 0;
  char pclUrno[16] = "";
  char pclFld[4096] = "";
  char pclOld[4096] = "";
  char pclNew[4096] = "";
  char pclSqlTab[16] = "";
  char pclSqlFld[1024] = "";
  char pclSqlVal[1024] = "";
  char pclSqlBuf[1024] = "";
  short slCursor = 0;
  short slFkt = 0;

  llChgCount = CT_GetChangedValues(pcpGridName, "", lpLineNo, pclFld, pclNew, pclOld);
  dbg(TRACE,"UPDATE 2DB LINE %d:",lpLineNo);
  dbg(TRACE,"FLD LIST: <%s>",pclFld);
  dbg(TRACE,"NEW DATA: <%s>",pclNew);
  dbg(TRACE,"OLD DATA: <%s>",pclOld);
  CT_CheckNullValues(pclNew,rgCtFlData.Value);
  dbg(TRACE,"VALUES CHECKED:\n<%s>",rgCtFlData.Value);

  CT_GetArrayTableName(pcpGridName, pclSqlTab);
  CT_GetMultiValues(pcpGridName, lpLineNo, "URNO", pclUrno);

  GOR_BuildSqlUpdValStrg(pclSqlVal,pclFld);
  sprintf(pclSqlBuf,"UPDATE %s SET %s WHERE URNO=%s",pclSqlTab,pclSqlVal,pclUrno);

  dbg(TRACE,"<%s>",pclSqlBuf);

  slCursor = 0;
  slFkt = IGNORE;
  delton(rgCtFlData.Value);
  ilGetRc = sql_if(slFkt, &slCursor, pclSqlBuf, rgCtFlData.Value);
  if (ilGetRc == DB_SUCCESS)
  {
    commit_work ();
    dbg(TRACE,"RECORD UPDATED");
    dbg(TRACE,"==============");
  }
  else
  {
    dbg (TRACE, "ORA UPDATE FAILED. ROLLBACK!");
    dbg (TRACE, "============================");
    rollback ();
  }
  close_my_cursor (&slCursor);

  return ilRc;
}

/* *************************************************** */
/* *************************************************** */
static int GOR_DeleteGridRecord2DB(char *pcpGridName, long lpLineNo)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  long llChgCount = 0;
  char pclUrno[16] = "";
  char pclFld[4096] = "";
  char pclOld[4096] = "";
  char pclNew[4096] = "";
  char pclSqlTab[16] = "";
  char pclSqlFld[1024] = "";
  char pclSqlVal[1024] = "";
  char pclSqlBuf[1024] = "";
  short slCursor = 0;
  short slFkt = 0;

  dbg(TRACE,"DELETE 2DB LINE %d:",lpLineNo);

  CT_GetArrayTableName(pcpGridName, pclSqlTab);
  CT_GetMultiValues(pcpGridName, lpLineNo, "URNO", pclUrno);

  sprintf(pclSqlBuf,"DELETE FROM %s WHERE URNO=%s",pclSqlTab,pclUrno);

  dbg(TRACE,"<%s>",pclSqlBuf);

  slCursor = 0;
  slFkt = IGNORE;
  ilGetRc = sql_if(slFkt, &slCursor, pclSqlBuf, pclNew);
  if (ilGetRc == DB_SUCCESS)
  {
    commit_work ();
    dbg(TRACE,"RECORD DELETED");
    dbg(TRACE,"==============");
  }
  else
  {
    dbg (TRACE, "ORA DELETION FAILED. (NO ROLLBACK!)");
    dbg (TRACE, "===================================");
    /*rollback ();*/
  }
  close_my_cursor (&slCursor);

  return ilRc;
}

/* *************************************************** */
/* *************************************************** */
static void GOR_BuildSqlInsValStrg(char *pcpResult, char *pcpFldLst)
{
  char pclFldNam[16] = "";
  char pclFldVal[32] = "";
  long llFldCnt = 0;
  long i = 0;
  pcpResult[0] = 0x00;
  llFldCnt = CT_CountPattern(pcpFldLst, ",");
  for (i=0;i<=llFldCnt;i++)
  {
    CT_GetItemsFromTo(pclFldNam, pcpFldLst, i, i, ",");    
    sprintf(pclFldVal,":V%s,",pclFldNam);
    strcat(pcpResult,pclFldVal);
  }
  i = strlen(pcpResult) - 1;
  pcpResult[i] = 0x00;
  return;
}

/* *************************************************** */
/* *************************************************** */
static void GOR_BuildSqlUpdValStrg(char *pcpResult, char *pcpFldLst)
{
  char pclFldNam[16] = "";
  char pclFldVal[32] = "";
  long llFldCnt = 0;
  long i = 0;
  pcpResult[0] = 0x00;
  llFldCnt = CT_CountPattern(pcpFldLst, ",");
  for (i=0;i<=llFldCnt;i++)
  {
    CT_GetItemsFromTo(pclFldNam, pcpFldLst, i, i, ",");    
    sprintf(pclFldVal,"%s=:V%s,",pclFldNam,pclFldNam);
    strcat(pcpResult,pclFldVal);
  }
  i = strlen(pcpResult) - 1;
  pcpResult[i] = 0x00;
  return;
}

/* *************************************************** */
/* *************************************************** */
int FLT_GetGortabData(char *pcpBgnTime, char *pcpEndTime, char *pcpParam, int ipForWhat, STR_DESC *prpResult)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  char pclBgn[16] = "";
  char pclEnd[16] = "";
  char pclAftRkey[16] = "";
  char pclTblName[16] = "";
  char pclFldList[1024] = "";
  char pclSqlCond[4096] = "";
  char pclSqlData[4096] = "";
  char pclSqlBuff[4096] = "";
  char pclTmpData[64] = "";
  long llMaxAftLine = 0;
  long llCurAftLine = 0;
  long llLineNo = 0;
  int ilGapOffBgn = 60;
  int ilGapOffEnd = 60;
  int ilValBgn = 0;
  int ilValEnd = 0;
  int ilValDif = 0;
  int ilValHours = 0;
  int ilDummy = 0;
  int ilFldCnt = 0;
  int ilCnt = 0;
  int ilLen = 0;
  int ilCntAft = 0;

  short slCursor = 0;
  short slFkt = 0;

  switch (ipForWhat)
  {
    case 0:
      dbg(TRACE,"------------------------------------------------");
      dbg(TRACE,"FETCH A LIST OF SELECTED OCCUPIED PARKING STANDS");
      dbg(TRACE,"------------------------------------------------");
      dbg(TRACE,"GET STANDS: <%s>",pcpParam);
      dbg(TRACE,"TIME FRAME: BEGIN <%s> END <%s>",pcpBgnTime,pcpEndTime);
      dbg(TRACE,"------------------------------------------------");
      strcpy(pclBgn,pcpBgnTime);
      strcpy(pclEnd,pcpEndTime);
      strcpy(pclTblName,"GORTAB");
      strcpy(pclFldList,"DISTINCT(RNAM)");
      sprintf(pclSqlCond,"(TIFE >= '%s' AND TIFB<='%s') AND RTYP='POS' AND RNAM IN (%s)",
                          pclBgn,pclEnd,pcpParam);
      sprintf(pclSqlBuff,"SELECT %s FROM %s WHERE %s",pclFldList,pclTblName,pclSqlCond);
      dbg(TRACE,"<%s>",pclSqlBuff);
      strcpy(prpResult->Value,"");
      dbg(DEBUG,"-----------------READ DB BGN-----------------");
      ilCnt = 0;
      slCursor = 0;
      slFkt = START;
      ilGetRc = RC_SUCCESS;
      while (ilGetRc == DB_SUCCESS)
      {
        ilGetRc = sql_if(slFkt,&slCursor,pclSqlBuff,pclSqlData);
        if (ilGetRc == DB_SUCCESS)
        {
          ilCnt++;
          CT_TrimRight(pclSqlData, ' ', -1);
          dbg(DEBUG,"%d: <%s>",ilCnt,pclSqlData);
          sprintf(pclTmpData,"'%s'",pclSqlData);
          strcat(prpResult->Value,pclTmpData);
          strcat(prpResult->Value,",");
        }
        slFkt = NEXT;
      }
      close_my_cursor(&slCursor);
      dbg(DEBUG,"-----------------READ DB END-----------------");
      ilLen = strlen(prpResult->Value);
      if (ilLen > 0)
      {
        ilLen--;
        prpResult->Value[ilLen] = 0x00;
      }
      dbg(DEBUG,"FETCHED %d ALLOCATION RECORDS FROM GORTAB",ilCnt);
    break;
    default:
    break;
  }

  return ilCnt;
}

