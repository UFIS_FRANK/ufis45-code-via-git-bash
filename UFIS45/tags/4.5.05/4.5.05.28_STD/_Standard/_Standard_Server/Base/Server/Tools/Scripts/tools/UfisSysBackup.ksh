if [ -f /ceda/etc/UfisEnv ] ; then
  . /ceda/etc/UfisEnv
fi

DATE=`date +%Y%m%d%H%M`
mkdir /ceda/Backup_${DATE} 
if [ -d /ceda/Backup_${DATE} ] ; then
   cd /ceda
   tar cvf - bin etc conf/*.cfg conf/*msg* conf/hsb.* conf/msg.* conf/sgs.tab \
       | ( cd /ceda/Backup_${DATE} ; tar xvf - )
fi
