#ifndef _DEF_mks_version_monutl_h
  #define _DEF_mks_version_monutl_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_monutl_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/monutl.h 1.2 2004/07/27 16:48:04SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The MonUtl include file.						  */
/*									  */
/*  Program	  : monutl.h   						  */
/*  Revision date :							  */
/*  Author    	  : JBE							  */
/*  									  */
/*  NOTE : This should be the only include file for your program          */
/* ********************************************************************** */

#ifndef __MONUTL_INC
#define __MONUTL_INC


int MonutlSendEvent (int ipRoutId, char *pcpCmd, char *pcpType, int ipArg1, int ipArg2, char *pcpArg3);

#endif

