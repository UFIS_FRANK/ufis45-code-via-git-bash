#ifndef _DEF_mks_version_chkval_h
  #define _DEF_mks_version_chkval_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_chkval_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/chkval.h 1.3 2004/07/27 16:47:02SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ******************************************************************** */
/*  The Master include file.                                            */
/*                                                                      */
/*  Program       :  chkval.c                                           */
/*                                                                      */
/*  Revision date :  11.11.1999                                         */ 
/*                                                                      */
/*  Author    	  :  GHe                                                */
/*                                                                      */
/*                                                                      */
/* ******************************************************************** */
/*                                                                      */
/* source-code-control-system version string                            */


/* be carefule with strftime or similar functions !!!                   */
/*                                                                      */                                                                         
                                                                                                                                                  
/***************************************************************************/ 
/* DESCRIPTION OF THE VALID FUNCTIONS :            ;-)                     */
/* ========================================================================*/
/*                                                                         */
/* InitValidityModule (int ipMode, char *pcpTable )                        */
/*                                                                         */
/*     where : - ipMode is the define for array or sql direct call         */
/*               .CHKVAL_ARRAY : use array function for DB table handle    */
/*               .CHKVAL_DB    : use DB (SQL) direct                       */
/*             - pcpTable the name for the used table                      */
/*                                                                         */
/*     return values are:  0 -- RC_SUCCESS        Init ok !                */
/*                        -1 -- RC_FAIL           -.-  nok                 */
/*                                                                         */
/*     indepence: the calling function use access for the DB               */
/*     attention: in case of CHKVAL_DB the function returns with an open   */
/*                SQL cursor and a defined global COMMON data range        */
/*                                                                         */
/*     intr. tables  : SYSTAB, VALTAB                                      */
/*     intr. sgs.tab : ---                                                 */ 
/***************************************************************************/ 
/*                                                                         */
/* UpdateValidityModule (EVENT *prpEvent )                                 */
/*                                                                         */
/*     where : - prpEvent is the first par. that the proc. rec. via queue  */
/*               If this par. has no information an error occured. Normaly */
/*               this EVENT includes an IRT, URT, DRT command, which       */
/*               access is for the VATTAB (eq. pcpTable name) table        */
/*                                                                         */
/*     rem:    for CHKVAL_DB mode no statement is important. The function  */
/*             returns in this case RC_SUCCESS                             */
/*                                                                         */ 
/*     return values are:  0 -- RC_SUCCESS  Init ok !  (always 4 CHKVAL_DB)*/  
/*                        -1 -- RC_FAIL           -.-  nok                 */
/*                                                                         */
/*     indepences: - the calling function use access for the DB            */
/*                 - prev. the InitValidate() must succesfully called      */ 
/*                                                                         */
/*     intr. tables  : SYSTAB, VALTAB                                      */
/*     intr. sgs.tab : ---                                                 */ 
/*                                                                         */
/***************************************************************************/ 
/*                                                                         */
/* CheckValidity (char *pcpAppl, char *pcpUrno, char *pcpDate,             */
/*                int *piplsValid)                                         */
/*                                                                         */
/*     where : - pcpAppl is a script from the calling function, which must */
/*               be equal with VALTAB.APPL. This parameter must exist and  */
/*               does'nt never changed!                                    */
/*             - pcpUrno is a Urno, which must be equal with VALTAB.URUE   */
/*               This parameter must exist and does'nt never changed!      */
/*             - pcpDate is a CEDA date, like this: YYYYMMDDhhmmss.        */
/*               This parameter must exist and does'nt never changed!      */
/*             - piplsValid is the return value for the check result       */
/*                . 0    - not successfully (no rec. found)  FALSE         */
/*                         CHKVAL_INVALID                                  */ 
/*                . 1    - successfully (found a rec. for this cases) TRUE */
/*                         CHKVAL_VALID                                    */
/*                                                                         */ 
/*     return values are:  0 -- RC_SUCCESS                                 */  
/*                        -1 -- RC_FAIL                                    */
/*                                                                         */
/*     indepences: - the calling function use access for the DB            */
/*                 - prev. the InitValidate() must succesfully called      */ 
/*                                                                         */
/*     intr. tables  : SYSTAB, VALTAB                                      */
/*     intr. sgs.tab : ---                                                 */ 
/*                                                                         */
/***************************************************************************/ 
/*                                                                         */
/* CleanupValidityModule ( void )                                          */
/*                                                                         */
/*     frees allocated memory and resets all status information            */
/*                                                                         */
/*     return values are:  0 -- RC_SUCCESS                                 */  
/*                        -1 -- RC_FAIL                                    */
/*                                                                         */
/*     indepences: - the calling function use access for the DB            */
/*                                                                         */
/*     attention: in case of CHKVAL_DB the function must close the open    */
/*                SQL cursor and frees the defined global COMMON data      */ 
/*                area range                                               */
/*                                                                         */
/*     intr. tables  : ---                                                 */
/*     intr. sgs.tab : ---                                                 */ 
/*                                                                         */
/***************************************************************************/



/***************************************************************************/
/* Additonal defines                                                       */
/***************************************************************************/

#ifndef CHKVAL
#define CHKVAL                1
#define CHKVAL_ARRAY          1
#define ARR_DEFAULT_RECNOS    ((long) 1000)
#define ARR_DEFAULT_INDIZES   4 
#define CHKVAL_DB             !CHKVAL_ARRAY
#define CHKVAL_INVALID        0
#define CHKVAL_VALID          1
#define DEFAULT_CGP_TABLE     "VALTAB"
#define ARR_VALID_NAME        "VALIDATE_ARR"
#define NOT_IN_USE           -1
#define SQL_LOADING_OK        RC_SUCCESS  
#define SQL_LOADING_NOK       RC_FAIL  
#define ARR_LOADING_OK        RC_SUCCESS  
#define ARR_LOADING_NOK       RC_FAIL  
#define DECL_MAX_BUFFER_SIZE  2048
#define SEL_DB_FIELDS  "APPL;CDAT;FREQ;HOPO;LSTU;TIMF;TIMT;URNO;URUE;USEC;USEU;VAFR;VATO;UVAL;TABN;"
                     /* C8   C14  C7   C3   C14  C4   C4   C10  C10  C32  C32  C14  C14   C10   C6 */

#endif



/***************************************************************************/
/* typedefs                                                                */
/***************************************************************************/
typedef struct   /* defined only for arrs [in moment] */  
{
 char   FieldName [32] ; 
 short  DefFieldLength ;
 short  FieldIndexNo ;   /* use 0 for no ndx, 1 for 1.st ndx, 2 for 2.nd ndx*/
 char   TabName [32] ;   /* if undef. -> use standard name */
 char   Str[1024] ;      /* received or send str. */
 short  StartPos ;       /* computed from fkt. with Fieldno value */ 
 short  Terminated ;     /* if str. terminated ==> 1 else 0 */  
} tFIELD_DEF ; 


/***************************************************************************/
/* External variables                                                      */
/***************************************************************************/


/***************************************************************************/
/* Global variables                                                        */
/***************************************************************************/


/***************************************************************************/
/* Function prototypes	                                                   */
/***************************************************************************/
int    InitValidityModule (int ipMode, char *pcpTable ) ;
int    UpdateValidityModule (EVENT *prpEvent )  ;
int    CheckValidity (char *pcpAppl, char *pcpUrno, char *pcpDate, int *piplsValid) ;
int    CleanupValidityModule ( void ) ;                       
int ChkDayFrequenz (char *pcpAct, char *pcpFrequenz) ;        








/*----------------------------------------------------------------*/
/* MORE INFORMATION  !!!                                          */
/*----------------------------------------------------------------*/

/* --- VALTAB (like : char *pcpTable) defines / 11.11.1999 */
/*                                                         */
/*    Name                        Null?      Type          */
/*   ---------------------------------------------------   */
/*   APPL                                    CHAR (8)      */
/*   CDAT                                    CHAR (14)     */
/*   FREQ       daily rep. (see below)       CHAR (7)      */
/*   HOPO                                    CHAR (3)      */
/*   LSTU                                    CHAR (14)     */
/*   TIMF       time from                    CHAR (4)      */
/*   TIMT       time to                      CHAR (4)      */
/*   URNO                                    CHAR (10)     */
/*   URUE                                    CHAR (10)     */
/*   USEC                                    CHAR (32)     */
/*   USEU                                    CHAR (32)     */
/*   VAFR       valid from                   CHAR (14)     */
/*   VATO       valid to                     CHAR (14)     */
/*   UVAL                                    CHAR (10)     */
/*   TABN                                    CHAR (6)      */
/* ---- END OF VALTAB -------------------------------------*/


/* FREQ : xxxxxxx    for the following dates: MTWTFSS (starts with monday)  */
/*                   for every day, we have one ascii date, like '0' or '1' */
/*                   in case of '1' the weekday is valid                    */
/* VAFR : date and time, like this: YYYYMMDDhhmmss                          */
/* VATO : see VAFR                                                          */






/* ******************************************************************** */
/* ******************************************************************** */
/* ******************************************************************** */
