#ifndef _DEF_mks_version_tools_h
  #define _DEF_mks_version_tools_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_tools_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/tools.h 1.12 2006/02/21 21:56:29SGT jim Exp  $";
#endif /* _DEF_mks_version */
#ifndef __TOOLS_INC
#define __TOOLS_INC

/*
20050509 JIM: PRF 7374: GetDynamicQueueToMe with QUE_WMQ_RESP to get
              q>30000 to not close WMQ queue on QUE_PUT
20060221 JIM: moved SwitchDebugFile and SwitchDebugFileExt from debugrec.c to 
              dbg.c, ignoring any parameters to avoids log file mess in i.e SQLHDL
*/
#include <stdio.h>
#include "quedef.h"
#include "uevent.h"
#include "ct.h"

#define		CFG_STRING	1
#define		CFG_INT		2
#define         CFG_MULTI       3
#ifndef	FALSE
#define		FALSE		-1
#endif
#ifndef TRUE	
#define 	TRUE		1
#endif
#ifndef	OK
#define		OK		0
#endif


#define		E_FOPEN		-1
#define		E_SECTION	-2
#define		E_PARAM		-3
#define		E_ERROR		-4

struct _ListDescriptor
{
  char  ListName[16]; /* identifier of the list content     */
  char  ListCode[16]; /* list relation (like AFTTAB)        */
  char  ListType[16]; /* list type (like URT)               */
  char *ListHead;     /* list header line (field list)      */
  long  StepSize;     /* number of bytes for malloc/realloc */
  long  AllocSize;    /* byte count from malloc or realloc  */
  long  UsedSize;     /* calculated value from strlen       */
  long  ValueCount;   /* number of entries (line count)     */
  char  ValuePref[4]; /* leading char of each value         */
  char  ValueSuff[4]; /* trailing char of each value        */
  char  ValueSepa[4]; /* separator between the values       */
  char *ValueList;    /* string with list of values (lines) */
};
typedef struct _ListDescriptor LIST_DESC;

struct _EventInfo
{
  char     OutQue[32];
  char     OutPri[32];
  char     EvtCmd[32];
  char     EvtTbl[32];
  char     EvtWks[32];
  char     EvtUsr[32];
  char     EvtTws[32];
  char     EvtTwe[32];
  STR_DESC EvtSel;
  STR_DESC EvtFld;
  STR_DESC EvtDat;
};
typedef struct _EventInfo EVT_INFO;

char *itoa(int i,char *a,int base);

/* Prototypes from dbg */
void dbg_handle_debug(int dbg_mod);
void dbg();
void SetDbgLimits (long lpMaxDbgLines, int ipMaxDbgFileNo);
extern void	SwitchDebugFile(int ipLevel,int *pipCounter);


/* Prototypes */
int tools_send_sql_rc (	int ipRouteID, char *pcpCommand, char *pcpTabName, char *pcpSelection, char *pcpFields, char *pcpData, int ipRetCode);

int tools_send_sql (int rout_id, char *Pcmd, char *Ptab_name, char *Psel, char *Pfields, char *Pdata);
int tools_send_sql_flag(int route_id, char *Pcmd, char *Ptab_name, char *Psel, char *Pfields, char *Pdata, int flag);
int tools_send_info_flag (int, int, char *, char *, char *, char *, char *, char *, char *, char *, char *, char *, char *, char *, int);

int flt_get_nth_field (char *Pstr, int n, char **PPresult);
int flt_cp_nth_field (char *Pstr, int n, char *Presult);
int flt_search_field (char *Pstr, char *Pfield, int *Pn);
int flt_len_of_nth_field (char *Pstr, int n);
int tool_fn_to_buf (char *Ppath, long *Psize, char **PPret);
int tool_search_eol (char **PPbuf);
int tool_filter_num (char *Pdata, int *Plen);
void ascii_snap (char *Pdata, int len, FILE *outp);
void mts_snap (char *Pdata, int len, FILE *outp);
int sgs_get_record (int table, int recno, char *Pdest);
int sgs_get_no_of_record (int table, char *Pdest);
int sgs_write_record (int table, int recno, char *Pdest);
int nam_get_host (char *Ptype, char *Phost);
void tool_filter_lf (char *Pdata, int *Plen);
int nam_get_recno (char *Phost);
void null_end_spaces (char *Pdata, int len);
void null_end_char (char *Pdata, int len, char c);
int tool_search_exco_data (char *Proc, char *Pkey, char *Presult);
int tool_get_q_id (char *Pnam);
void tool_make_string (char *Psrc, char *Pdest, unsigned short len);
unsigned short tool_swap_bytes (short val);
int tools_send_err (int route_id, int ret);
void tool_filter_spaces (char *Pdata);
void rc_strerror (int no, char **PPerr);
int tool_get_field_data (char *Pfields, char *Pdata, char *Pfname, char *Pres);
void tool_make_string2 (char *Psrc, char *Pdest, unsigned short len);
int GetDynamicQueue(int *, char*);
int GetDynamicQueueToMe(int *, char*);
int iGetConfigEntry(char *,char *,char *,short,char *);
int iGetConfigRow(char *,char *,char *,short,char *);
int iWriteConfigEntry(char *,char *,char *,short,char *);
int iStrup(char *s);
int tool_write_exco_data (char *Proc, char *Pkey, char *Pdata);
int new_tools_send_sql(int ipRouteID,BC_HEAD *prpBchd,CMDBLK *prpCmdblk,char *pcpFields,char *pcpData);
int new_tools_send_sql_prior(int ipRouteID,int ipPrior,BC_HEAD *prpBchd,CMDBLK *prpCmdblk,char *pcpFields,char *pcpData);
int field_count(char *);
char *sGet_item(char *,short);
int string_to_key (char *pcpStr, char *pcpKey);
int SearchPara(char *pcpFile,char *pcpResult,char *pcpSection,char *pcpParam,int lpType);
int ConfigureAction (char *pcpTableName, char *pcpFields, char *pcpCommands);
int ChangeDynActionConfig (char *pcpTableName, char *pcpFields, char *pcpCommands, int bpAdd, int bpDel );

long GetMySize(int ipUsePid);
int CheckMySizeDiff(long lpMaxSizeDiff);

void ToolsSetSpoolerInfo(char *pcpUsr, char *pcpWks, char *pcpTws, char *pcpTwe);
int ToolsSpoolEventData(char *pcpRoute, char *pcpTbl, char *pcpCmd,
                        char *pcpUrnoList, char *pcpSel, char *pcpFld, 
                        char *pcpDat,char *pcpOldDat);
int ToolsReleaseEventSpooler(int ipSendEvent, int ipPrio);


void ToolsListCreate(LIST_DESC **prpListDesc);
void ToolsListInit(LIST_DESC *rpListDesc);
void ToolsListSetStepSize(LIST_DESC *rpListDesc, long lpStepSize);
void ToolsListSetInfo(LIST_DESC *rpListDesc, char *pcpName, char *pcpCode, 
                      char *pcpType, char *pcpHead, char *pcpPrefix, char *pcpSuffix, 
                      char *pcpValueSepa, long lpStepSize);
long ToolsListAddValue(LIST_DESC *rpListDesc, char *pcpValue, long lpLen);
long ToolsListAddValuePlus(LIST_DESC *rpListDesc,char *pcpValue, long lpLen, char *pcpSep, char *pcpValue2, long lpLen2);
long ToolsListAddKeyItem(LIST_DESC *rpListDesc, 
                         char *pcpPrefix, char *pcpName, char *pcpSuffix, char *pcpValue, 
                         char *pcpEnd, long lpLen);
void ToolsListFree(LIST_DESC *rpListDesc);

void ToolsEventInfoInit(EVT_INFO *prpEvtInfo);
void ToolsEventSetInfo(EVT_INFO *prpEvtInfo,
                       char *pcpQue, char *pcpPri, char *pcpCmd, char *pcpTbl,
                       char *pcpWks, char *pcpUsr, char *pcpTws, char *pcpTwe,
                       char *pcpSel, char *pcpFld, char *pcpDat);
#endif

