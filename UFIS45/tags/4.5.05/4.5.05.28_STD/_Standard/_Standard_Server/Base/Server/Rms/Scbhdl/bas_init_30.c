/****************************************/
/* calculating holiday claim basic vaue */
/****************************************/
static int GetHolDays(int ipAge,const char *pcpScoCode)
{
	/* get holiday claim in dependancy of age and contrct from COHTAB */
	char	clKeyValue_ResultBuffer[30];
	char	clFAGE[4];
	char	clHOLD[4];
	int	ilHolDays = 0;
	int ilCohHandle = -1;
	int	ilBestFitAge = 0;
	int	ilFoundAge = 0;

	/* get the handle of the MFMTAB (Minimum Free of Month) */
	ilCohHandle = GetArrayIndex("COHTAB");
	if (ilCohHandle < 0)
	{
		dbg(TRACE,"GetHolDays(): ERROR - Array for COHTAB not found!");
		return 0;
	}

	/* the records are sorted by contract,age (COH.CTRC,COH.FAGE). Index is on COH.CTRC. */
	/* we have to find the best fitting age */
	strcpy(clKeyValue_ResultBuffer,pcpScoCode);
	if (FindFirstRecordByIndex(ilCohHandle, clKeyValue_ResultBuffer) > 0)
	{
		if (GetFieldValueFromMonoBlockedDataString("FAGE",sgRecordsets[ilCohHandle].cFieldList,clKeyValue_ResultBuffer,clFAGE) > 0)
		{
			ilFoundAge = atoi(clFAGE);
			if (ilFoundAge <= ipAge && ilFoundAge > ilBestFitAge)
			{
				ilBestFitAge = ilFoundAge;
				if (GetFieldValueFromMonoBlockedDataString("HOLD",sgRecordsets[ilCohHandle].cFieldList,clKeyValue_ResultBuffer,clHOLD) > 0)
				{
					ilHolDays = atoi(clHOLD);
				}
			}
		}
		dbg(DEBUG,"GetHolDays(): Employee age = <%d>",ipAge);
		dbg(DEBUG,"GetHolDays(): Initial value of <FAGE> = <%d>",ilFoundAge);
		dbg(DEBUG,"GetHolDays(): Initial value of <HOLD> = <%d>",ilHolDays);

		/* now we try to find the other records - maybe the age fits better */
		strcpy(clKeyValue_ResultBuffer,pcpScoCode);
		while (FindNextRecordByIndex(ilCohHandle, clKeyValue_ResultBuffer) > 0)
		{
			if (GetFieldValueFromMonoBlockedDataString("FAGE",sgRecordsets[ilCohHandle].cFieldList,clKeyValue_ResultBuffer,clFAGE) > 0)
			{
				ilFoundAge = atoi(clFAGE);
				if (ilFoundAge <= ipAge && ilFoundAge > ilBestFitAge)
				{
					ilBestFitAge = ilFoundAge;
					if (GetFieldValueFromMonoBlockedDataString("HOLD",sgRecordsets[ilCohHandle].cFieldList,clKeyValue_ResultBuffer,clHOLD) > 0)
					{
						ilHolDays = atoi(clHOLD);
					}
				}
			}
			dbg(DEBUG,"GetHolDays(): Next value of <FAGE> = <%d>",ilFoundAge);
			dbg(DEBUG,"GetHolDays(): Best fit of <FAGE> = <%d>",ilBestFitAge);
			dbg(DEBUG,"GetHolDays(): Best fit of <HOLD> = <%d>",ilHolDays);

			strcpy(clKeyValue_ResultBuffer,pcpScoCode);
		}
	}
	else
	{
		dbg(TRACE,"GetHolDays(): No COH-record for contract <%s> found!",pcpScoCode);
		return 0;
	}

	dbg(DEBUG,"GetHolDays(): Return value = <%d>",ilHolDays);
	return ilHolDays;
}

/**************************************/
/* calculating holiday hours of month */
/**************************************/
static double Calculate(const char *pcpStfu,const char *pcpSday,int ipAge)
{
	COTDATA	olCotData;
	SCODATA	olScoData;
	int ilHolBasicClaim = 0;	

	if (ipAge == 0)
	{
		dbg(TRACE,"Calculate Init 30: Age is 0 years!");
		return 0.0e0;
	}

	/* get contract */
	if (GetScoData(pcpStfu,pcpSday,&olScoData) == 0)
	{
		dbg(TRACE,"Calculate Init 30: No connection between employee and contract found");
		return 0.0e0;
	}

	if (fabs(olScoData.dmVereinbarteWochenstunden) < 1.0e-5)
	{
		dbg(DEBUG,"Calculate Init 30: Take agreed daily working minutes of contract.");
		if (GetCotDataByCtrc(olScoData.cmScoCode,&olCotData) == 0)
		{
			dbg(TRACE,"Calculate Init 30: Contract <%s> not found!",olScoData.cmScoCode);
			return 0.0e0;
		}
		else
		{
			olScoData.dmVereinbarteWochenstunden = olCotData.dmVereinbarteWochenstunden;
			dbg(DEBUG,"Calculate Init 30: Agreed daily working minutes of contract = <%lf>.",olCotData.dmVereinbarteWochenstunden);
		}
	}

	/* calculate basic holiday claim */
	ilHolBasicClaim = GetHolDays(ipAge,olScoData.cmScoCode);

	dbg(DEBUG,"Calculate Init 30: Contract: <%s>",olScoData.cmScoCode);
	dbg(DEBUG,"Calculate Init 30: Basic holiday claim: <%d>",ilHolBasicClaim);
	dbg(DEBUG,"Calculate Init 30: Minutes per day: <%lf>",olScoData.dmVereinbarteWochenstunden);

	/* holiday hours = holiday claim * (week minutes / 60) */
	return (olScoData.dmVereinbarteWochenstunden / 60) * ilHolBasicClaim;
}

/*****************************/
/* calculate age of employee */
/*****************************/
static int GetAge(int ipYear,STAFFDATA *popStaffData)
{
	char clBirthdayYear[6];

	strncpy(clBirthdayYear,popStaffData->cmGeburtstag,4);
	clBirthdayYear[4] = '\0';

	if (strlen(clBirthdayYear) == 0)
	{
		return 0;
	}
	else
	{
		return ipYear - atoi(clBirthdayYear);
	}
}

/**************************************************************************
 ****** init_30: Initial-Konto Nr. 30
 ****** ADO 19.10.00
 ****** Where-Clauses muessen das Format 'where ...' haben
 ****** ohne Apostroph!!! String Werte in Hochkommata einschliessen,
 ****** Zahlenwerte nicht.
 **************************************************************************/
static void bas_init_30()
{
	double dlHolDaysPerYear = 0.0e0;
	char clFrom[16];			
	char clYearFrom[6];
	char clMonFrom[4];
	char clDayFrom[4];

	char clTo[16];			
	char clYearTo[6];
	char clMonTo[4];
	char clDayTo[4];

	char clCalcDate[16];		
	char clCurrentEmpl[12];

	char clLocalYear[6];

	int ilCountEmpl;
	int ilCounter;

	int ilYearCount;
	int ilMonthCount;

	int ilAge;
	int ilDaysInYear;
	int ilDaysInMonth;
	double dlYelaValue = 0.0e0;
	double dlMonthValue = 0.0e0;

	STAFFDATA	olStaffData;

	dbg(DEBUG,"============================= INIT ACCOUNT INIT_30 START =============================");

	if (strlen(pcgData) == NULL)
	{
		dbg(TRACE,"Account Init 30: Stopped, because <pcgData> = undef!");
		return;
	}
	dbg(DEBUG,"Account Init 30: <pcgData> = <%s>",pcgData);

	/* checking the fields */
	if (strlen(pcgFields) == 0)
	{
		dbg(TRACE,"Account Init 30: Stopped, because <pcgFields> = undef!");
		return;
	}
	dbg(DEBUG, "Account Init 30: <pcgFields> = <%s>",pcgFields);

	/* checking the selection */
	if (strlen(pcgSelKey) == 0)
	{
		dbg(TRACE,"Account Init 30: Stopped, because <pcgSelKey> = undef!");
		return;
	}
	dbg(DEBUG, "Account Init 30: <pcgSelKey> = <%s>",pcgSelKey);

	/* get start date */
	if (GetFieldValueFromMonoBlockedDataString("FROM",pcgFields,pcgData,clFrom) <= 0)
	{
		dbg(TRACE,"Account Init 30: Stopped, because <clFrom> = undef!");
		return;
	}
	strncpy(clYearFrom,&clFrom[0],4);
	clYearFrom[4] = '\0';
	strcpy(clMonFrom,"01");
	strcpy(clDayFrom,"01");

	/* get end date  - only complete years are calculated */
	if (GetFieldValueFromMonoBlockedDataString("TO",pcgFields,pcgData,clTo) <= 0)
	{
		dbg(TRACE,"Account Init 30: Stopped, because <clTo> = undef!");
		return;
	}
	strncpy(clYearTo,&clTo[0],4);
	clYearTo[4] = '\0';
	strcpy(clMonTo,"12");
	strcpy(clDayTo,"31");

	dbg(DEBUG, "Account Init 30: Initializing from %s to %s.",clFrom,clTo);

	/* count number of employees */
	ilCountEmpl = CountElements(pcgSelKey);
	if (ilCountEmpl < 0)
	{
		dbg(TRACE,"Account Init 30: Stopped, because no employees to initialize!");
		return;
	}
	dbg(DEBUG,"Account Init 30: Number of employees = <%d>", ilCountEmpl);

	/* loop per employee */
	for (ilCounter = 0; ilCounter < ilCountEmpl; ilCounter++)
	{
		if (GetElement(pcgSelKey,ilCounter,clCurrentEmpl) <= 0)
		{
			dbg(TRACE,"Account Init 30: Stopped, because <clCurrentEmpl> = undef!");
			return;
		}

		/* get staff data containing e.g. PENO */
		GetStaffData(clCurrentEmpl,&olStaffData);

		/* Used in UpdateAccountXY! */
		WriteTemporaryField("Personalnummer",olStaffData.cmPersonalnummer);

		dbg(DEBUG,"Account Init 30: Start calculation for employee <%s>",clCurrentEmpl);

		/* loop through the years */
		for (ilYearCount = atoi(clYearFrom); ilYearCount <= atoi(clYearTo); ilYearCount++)
		{
			/* get days of the year */
			/* old: ilDaysInYear = GetDaysInYear(ilYearCount);*/
			if (LeapYear(ilYearCount))
			{
				ilDaysInYear = 366;
			}
			else
			{
				ilDaysInYear = 365;
			}

			/* get employee's age */
			ilAge = GetAge(ilYearCount,&olStaffData);

			dbg(DEBUG,"Account Init 30: Age of employee in year %d = %d years",ilYearCount,ilAge);
			dbg(DEBUG,"Account Init 30: Entry date of employee = %s",olStaffData.cmEintrittsdatum);
			dbg(DEBUG,"Account Init 30: Leaving date of employee = %s",olStaffData.cmAustrittsdatum);
			dbg(DEBUG,"Account Init 30: Days in year %d = %d",ilYearCount,ilDaysInYear);

			/* reset values */
			dlHolDaysPerYear = 0.0e0;

			/* loop per month */
			for (ilMonthCount = atoi(clMonFrom); ilMonthCount <= atoi(clMonTo); ilMonthCount++)
			{
				/* reset values */
				dlMonthValue = 0.0e0;

				/* build time-variable for calculation */
				sprintf(clCalcDate,"%4d%02d01",ilYearCount,ilMonthCount);
				dbg(DEBUG,"Account Init 30: Start calculation for date <%s>",clCalcDate);

				/* number of days per month */
				ilDaysInMonth = GetDaysInMonth(clCalcDate);
				dbg(DEBUG,"Account Init 30: Days in month = %d",ilDaysInMonth);

				/* check entry date */
				if (strlen(olStaffData.cmEintrittsdatum) > 0 && strcmp(olStaffData.cmEintrittsdatum,clCalcDate) > 0)
				{
					dbg(TRACE,"Account Init 30: Employee not employed at calculation date <%s> yet!",clCalcDate);
					continue;
				}

				/* check leaving date */
				if (strlen(olStaffData.cmAustrittsdatum) > 0 && strcmp(clCalcDate,olStaffData.cmAustrittsdatum) > 0)
				{
					dbg(TRACE,"Account Init 30: Employee not employed at calculation date <%s> any more!",clCalcDate);
					continue;
				}

				/* do the calculation */
				dlMonthValue = Calculate(clCurrentEmpl,clCalcDate,ilAge); 
				dlMonthValue = (dlMonthValue / ilDaysInYear) * ilDaysInMonth;
				dbg(DEBUG,"Account Init 30: Holiday hours per month = %8.2lf",dlMonthValue);

				/* add them*/
				dlHolDaysPerYear += dlMonthValue;
			}
			dbg(DEBUG,"Account Init 30: Result holiday hours = %8.2lf",dlHolDaysPerYear);

			/* update arrays (this and next year */
			sprintf(clLocalYear,"%4d",ilYearCount);
			UpdateInitialAcc("30",dlHolDaysPerYear,clLocalYear,clCurrentEmpl,&dlYelaValue);
			UpdateNextYearsValuesHandle("30",clLocalYear,clCurrentEmpl,dlHolDaysPerYear + dlYelaValue);
		}
	}
}
