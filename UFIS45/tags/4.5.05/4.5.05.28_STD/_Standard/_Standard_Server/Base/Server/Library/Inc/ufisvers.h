#ifndef UFIS_VERSION
   #define UFIS_VERSION "UFIS 4.5 (c) ABB AAT/I"
   #ifdef __GNUC__
     #define MARK_UNUSED __attribute__ ((unused))
   #else
     #define MARK_UNUSED
   #endif
#endif
