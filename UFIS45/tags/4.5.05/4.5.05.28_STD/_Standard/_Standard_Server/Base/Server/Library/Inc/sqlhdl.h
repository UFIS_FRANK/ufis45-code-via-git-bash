#ifndef _DEF_mks_version_sqlhdl_h
  #define _DEF_mks_version_sqlhdl_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_sqlhdl_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/sqlhdl.h 1.2 2004/07/27 16:48:28SGT jim Exp  $";
#endif /*_DEF_mks_version_sqlhdl_h*/

/* *******************************************
 * Library modulname:			sqlhdl header file
 * Author: 	Gordon Sawyerr	
 * Date:		16/06/97					
 * State:
 * Description: 
 * History: 
 * *********************************************/

#ifndef _SQLHDR
#define _SQLHDR


#include <sys/signal.h>

#include  "ugccsma.h"
#include  "glbdef.h"
#include  "quedef.h"
#include  "uevent.h"
#include  "msgno.h"
#include  "cli.h"
#include  "tools.h"
#include  "db_if.h"
#include  "send_tools.h"

/************** Tunable parameters **********************/
/* Realloc steps for result buffer, grows automatically */
#define I_RESULTBUFSTEP	(1024*1024*2) /* 2MB */
#define RESULTBUFSTEP	(1024*1024) /* 1MB */


#define DATABLK_SIZE (1024*1024) /* 1M */

/* Max ilLength of a row */
#define MAX_ROW_LEN 0x4000 /* 16KB */

#define ARRAY_SIZE  64

#define BD_PACKET "BDH"
#define MAX_NO_OF_BDTAB_ENTRIES 20

/* Max ilLength of a single field */
#define MAX_FIELD_LEN	0x0800 /* 2K */

/******************
External variables 
*******************/
extern char sql_buf[];

#endif
