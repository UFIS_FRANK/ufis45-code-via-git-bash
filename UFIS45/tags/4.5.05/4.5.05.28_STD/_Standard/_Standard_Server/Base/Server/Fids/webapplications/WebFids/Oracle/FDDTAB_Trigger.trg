create or replace trigger FDDTAB_Trigger
	after insert or
	update or
	delete on fddtab

	REFERENCING OLD AS OLD NEW AS NEW
  	for each row
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
--  TRIGGER           - FDDTAB_TRIGGER
--
--  Version           - 1.5.0
--
--  Written By        - Juergen Hammerschmid
--
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
--  Change Log
--
--  Name           Date               Version    Change
--  ----           ----------         -------    ------
--  Hammerschmid   31.03.2008         1.0        new
--  Hammerschmid   21.04.2008		  1.1		 now uses TR_LOG_SEQ and fixes bug when
--												 new package pck_webfids is installed that
--												 FDDTABTrigger will not work as designed.
--  Hammerschmid   15.05.2008		  1.2		 Synchronize processes
--  Hammerschmid   20.05.2008		  1.3        serialize jobs and kill overrunning
--  Hammerschmid   27.05.2008         1.4        Optimizations / remarks included in view
--  Hammerschmid   03.07.2008         1.5        Set time to current remark to local time,
--                                               additional fields are supported,
--                                               exception handling improved
--  Hammerschmid   07.08.2009         1.6        Bug in insert corrected
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
declare
  -- local variables here
	v_Seq_ID		NUMBER(12);
    v_TimeStart		DATE;
    v_GoAhead		BOOLEAN := FALSE;
	v_UTCOffset		APTTAB.tdis%TYPE;
    v_HOPO			APTTAB.HOPO%TYPE;
    v_Error			VARCHAR2(200) := '';
    v_Records		NUMBER;

	PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN
	-- check if cyclic job is running
    BEGIN
        SELECT SEQ_ID, P_START
        INTO v_Seq_ID, v_TimeStart
        FROM TR_LOG
        WHERE p_end IS NULL
        AND ERROR_FLAG <> 'Y'
        AND p_status <> 'UNKNOWN';
        IF (SYSDATE - v_TimeStart) > 30/86400 THEN
			UPDATE TR_LOG SET ERROR_FLAG = 'Y' WHERE seq_id = v_Seq_ID;
            UPDATE WEBFIDS_TABLE_TEMP SET Status = '';
            COMMIT;
        END IF;

    EXCEPTION
    	WHEN NO_DATA_FOUND THEN
        	v_GoAhead := TRUE;
    END;

    IF v_GoAhead = TRUE THEN
        BEGIN
        -- Get UTOOffset
            SELECT TDIS, HOPO
            INTO v_UTCOffset, v_HOPO
            FROM APTTAB
            WHERE APC3=HOPO;
        EXCEPTION
            WHEN OTHERS THEN
            	v_HOPO := '';
                v_UTCOffset := '0';
        END;

        SELECT TR_LOG_SEQ.Nextval
        INTO v_Seq_ID
        FROM dual;

        IF INSERTING THEN
        	BEGIN
                INSERT INTO TR_LOG VALUES (v_Seq_ID,'FDDTABURNO = ' || :NEW.URNO,sysdate,null,'INSERT','Y');
                UPDATE TR_LOG SET P_END = sysdate, ERROR_FLAG = 'F'
               	WHERE seq_id = v_Seq_ID;
            EXCEPTION
            	WHEN OTHERS THEN
                	v_Error := SUBSTR(SQLERRM, 1, 80);
                	UPDATE TR_LOG SET P_END = sysdate, ERROR_FLAG = 'N', PROCESS = PROCESS || ' INSERT  ' || v_Error
                	WHERE seq_id = v_Seq_ID;
            END;
        ELSIF UPDATING THEN
        	BEGIN
                INSERT INTO TR_LOG VALUES (v_Seq_ID,'FDDTABURNO = ' || :NEW.URNO ,sysdate,null,'UPDATE','N');
                UPDATE WEBFIDS_TABLE SET  ACTUAL_TIME = SYSDATE,
                                       AIRBORNE_TIME = fun_calculate_time(trim(:NEW.AIRB),'YYYYMMDDHH24MISS',v_UTCOffset,'MINUTE'),
                                       BELT1 = :NEW.BLT1,
                                       BELT2 = :NEW.BLT2,
                                       CHECK_IN_FROM = :NEW.CKIF,
                                       CHECK_IN_TO = :NEW.CKIT,
                                       ESTIMATED_TIME_OF_FLIGHT = fun_calculate_time(trim(:NEW.ETOF),'YYYYMMDDHH24MISS',v_UTCOffset,'MINUTE'),
                                       GATE_DEPARTURE_1 = :NEW.GTD1,
                                       GATE_DEPARTURE_2 = :NEW.GTD2,
--                                       LANDING_TIME = fun_calculate_time(trim(:NEW.LAND),'YYYYMMDDHH24MISS',v_UTCOffset,'MINUTE'),
--                                       OFFBLOCK_TIME = fun_calculate_time(trim(:NEW.OFBL),'YYYYMMDDHH24MISS',v_UTCOffset,'MINUTE'),
--                                       ONBLOCK_TIME = fun_calculate_time(trim(:NEW.ONBL),'YYYYMMDDHH24MISS',v_UTCOffset,'MINUTE'),
--                                       TIME_TO_CURRENT_REMARK = fun_calculate_time(trim(:NEW.RMTI),'YYYYMMDDHH24MISS',v_UTCOffset,'MINUTE'),
--                                       SCHEDULED_TIME = fun_calculate_time(trim(:NEW.STOF),'YYYYMMDDHH24MISS',v_UTCOffset,'MINUTE'),
--                                       TIFF = fun_calculate_time(trim(:NEW.TIFF),'YYYYMMDDHH24MISS',v_UTCOffset,'MINUTE'),
                                       LANDING_TIME = fun_calculate_time(trim(:NEW.LAND),'YYYYMMDDHH24MISS',PCK_WEBFIDS.FUN_GET_UTCOFFSET(:NEW.LAND,v_HOPO),'MINUTE'),
                                       OFFBLOCK_TIME = fun_calculate_time(trim(:NEW.OFBL),'YYYYMMDDHH24MISS',PCK_WEBFIDS.FUN_GET_UTCOFFSET(:NEW.OFBL,v_HOPO),'MINUTE'),
                                       ONBLOCK_TIME = fun_calculate_time(trim(:NEW.ONBL),'YYYYMMDDHH24MISS',PCK_WEBFIDS.FUN_GET_UTCOFFSET(:NEW.ONBL,v_HOPO),'MINUTE'),
                                       TIME_TO_CURRENT_REMARK = fun_calculate_time(trim(:NEW.RMTI),'YYYYMMDDHH24MISS',PCK_WEBFIDS.FUN_GET_UTCOFFSET(:NEW.RMTI,v_HOPO),'MINUTE'),
                                       SCHEDULED_TIME = fun_calculate_time(trim(:NEW.STOF),'YYYYMMDDHH24MISS',PCK_WEBFIDS.FUN_GET_UTCOFFSET(:NEW.STOF,v_HOPO),'MINUTE'),
                                       TIFF = fun_calculate_time(trim(:NEW.TIFF),'YYYYMMDDHH24MISS',PCK_WEBFIDS.FUN_GET_UTCOFFSET(:NEW.TIFF,v_HOPO),'MINUTE'),
                                       AIRPORTCODE = :NEW.APC3,
                                       -- AIRLINECODE = :NEW.ALC3,
                                       AIRPORT_VIA = :NEW.VIA3,
                                       -- AIRPORT_ORIGIN = :NEW.ORG3,
                                       -- AIRPORT_DESTINATION = :NEW.DES3,
                                       -- CHECKIN_REMARK = :NEW.CREC,
                                       -- BELT_REMARK = :NEW.DISP,
                                       CHECKIN_SUM_REMARK_STATUS = :NEW.REM2,
                                       FLIGHT_STATUS = :NEW.REMP
                WHERE FDDURNO = :NEW.URNO;
                v_Records := SQL%ROWCOUNT;
                IF v_Records = 0 THEN
                	RAISE NO_DATA_FOUND;
                ELSE
                	UPDATE TR_LOG SET P_END = sysdate, ERROR_FLAG = 'F'
                	WHERE seq_id = v_Seq_ID;
                END IF;
            EXCEPTION
            	WHEN NO_DATA_FOUND THEN
                	UPDATE TR_LOG SET P_END = sysdate, ERROR_FLAG = 'N', PROCESS = PROCESS || ' UPDATE '
                	WHERE seq_id = v_Seq_ID;
            	WHEN OTHERS THEN
                	v_Error := SUBSTR(SQLERRM, 1, 80);
                	UPDATE TR_LOG SET P_END = sysdate, ERROR_FLAG = 'N', PROCESS = PROCESS || ' UPDATE ' || v_Error
                	WHERE seq_id = v_Seq_ID;
            END;

        ELSIF DELETING THEN
        	BEGIN
                INSERT INTO TR_LOG VALUES (v_Seq_ID,'FDDTABURNO = ' || :OLD.URNO,sysdate,null,'DELETE','N');
                DELETE FROM WEBFIDS_TABLE
                WHERE FDDURNO = :OLD.URNO;
                v_Records := SQL%ROWCOUNT;
                IF v_Records = 0 THEN
                	RAISE NO_DATA_FOUND;
                ELSE
                	UPDATE TR_LOG SET P_END = sysdate, ERROR_FLAG = 'F'
                	WHERE seq_id = v_Seq_ID;
                END IF;
            EXCEPTION
            	WHEN NO_DATA_FOUND THEN
                	UPDATE TR_LOG SET P_END = sysdate, ERROR_FLAG = 'N', PROCESS = PROCESS || ' DELETE '
                	WHERE seq_id = v_Seq_ID;
            	WHEN OTHERS THEN
                	v_Error := SUBSTR(SQLERRM, 1, 80);
                	UPDATE TR_LOG SET P_END = sysdate, ERROR_FLAG = 'N', PROCESS = PROCESS || ' DELETE ' || v_Error
                	WHERE seq_id = v_Seq_ID;
            END;
        ELSE
            INSERT INTO TR_LOG VALUES (v_Seq_ID,'PRC_FIDS_GET_DATA_FOR_DISPLAY',sysdate,null,'UNKNOWN','N');
        END IF;
        COMMIT;
	END IF;
EXCEPTION
	WHEN OTHERS THEN
		ROLLBACK;
       	v_Error := SUBSTR(SQLERRM, 1, 80);
       	UPDATE TR_LOG SET P_END = sysdate, ERROR_FLAG = 'N', PROCESS = PROCESS || ' FDDTAB_TRIGGER ' || v_Error
       	WHERE seq_id = v_Seq_ID;
        COMMIT;
END FDDTAB_Trigger;
/
