#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/ftphdl.c 1.10 2006/05/17 22:04:37SGT jwe Exp  $";
#endif /* _DEF_mks_version */
/*****************************************************************************/
/*                                                                           */
/* ABB ACE/FC Program Skeleton                                               */
/*                                                                           */
/* Author         :                                                          */
/* Date           :                                                          */
/* Description    :                                                          */
/*                                                                           */
/* Update history : rkl 16.05.00 debug_level init from Configfile            */
/*                                                                           */
/*                : eth 12.01.2001 tide up some things                       */
/*                                                                           */
/*                : eth 06.02.01 added igRestartTimeout                      */
/*                                                                           */
/*                : eth 02.05.2001 added passout function                    */
/*                                                                           */
/*                : eth 08.08.01 added functionality to send multiple files  */
/*                                                                           */
/*                : JWE 18.09.03 bugfixed the rename_local_file function     */
/* 20040312 JIM: replaced dbg(...sccs_version) by dbg(...mks_version)        */
/* 20040312 JIM: removed sccs_version                                        */
/* 20040312 JIM: PRF 5860: AddDynamicConfig: not all settings (i.e.          */
/*               .pcOptSiteCmd) arecomming via queue, so init struct         */
/* 20040810 JIM: removed some unsused vars                                   */
/* 20050818 MCU: delete remote file before renaming if old file exist        */
/* 20060517 JWE: PRF 5398 - Corrected signal handling for MultipleFile-Sending*/
/*               now as well for UFIS 4.5 - FTPHDL                           */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/* be carefule with strftime or similar functions !!!                        */
/*                                                                           */
/*****************************************************************************/
/* This program is a MIKE main program */

#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
/* #include <sys/resource.h> */

#include "send.h"
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "ntisch.h"
#include "ftphdl.h"

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp       = NULL;*/
/* changed by eth 1.2.2001 */
int  debug_level = DEBUG;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM    *prgItem  = NULL;      /* The queue item pointer  */
static EVENT   *prgEvent = NULL;      /* The event pointer       */
static int       igInitOK = FALSE;
static char      cgConfigFile[512];
static int	 igTimschID = -1;
static char     pcgTABEnd[iMIN];
static char     pcgHomeAP[iMIN];
static char     pcgTwStart[64];
static char     pcgTwEnd[64];
static FTPMain   rgTM;
static int       igRestartTimeout; /* by eth 6.2.2001 */
static int 	igMultipleSend=FALSE;

/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int  InitFTPHdl();
static int  Reset(void);                       /* Reset program          */
static void Terminate(UINT);                   /* Terminate program      */
static void HandleSignal(int);                 /* Handles signals        */
static void HandleErr(int);                    /* Handles general errors */
static void HandleQueErr(int, int);                 /* Handles queuing errors */
static int  HandleData(void);                  /* Handles event data     */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/
static int  HandleFTP(int);                    /* hmmm */
static int  SendEventToTimsch(char *, int, char *);
static int  DeleteTimschEvent(int);
static int  GetCmdNo(char *);
static int  SendAnswer(char *, char *, char *, char *, FTPConfig *);
static int  SendToQue(int, int, BC_HEAD *, CMDBLK *, char *, char *, char *, FTPConfig *, size_t);
static int  SendCommand(char *, int, char *, char *);
static int  AddDynamicConfig(FTPConfig *);
static void ReplaceStdTokens(char *pcpCalledBy, char *pcpLine,char *pcpDynData,char *pcpDateFormat,char *pcpTimeFormat);

static int  passout(void);

#define	FTP_CONNECT_ERR_MSG "FTP: CONNECT TO REMOTE HOST FAILED! "
#define	FTP_LOGIN_ERR_MSG "FTP: LOGIN ON REMOTE HOST FAILED! "
#define	FTP_RECV_ERR_MSG "FTP: RECEIVE FROM REMOTE HOST FAILED! "
#define	FTP_SEND_ERR_MSG "FTP: SEND TO REMOTE HOST FAILED! "

#define DYNCONFIGNEW 1
#define DYNCONFIGOLD 2

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRC = RC_SUCCESS; /* Return code */
	int	ilCnt = 0;
	
	INITIALIZE;
	dbg(TRACE,"MAIN: started ... version <%s>",mks_version);

	/* get data from sgs.tab */
	if ((ilRC = tool_search_exco_data("ALL", "TABEND", pcgTABEnd)) != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: %05d cannot find entry <TABEND> in sgs.tab", __LINE__);
		dbg(TRACE,"MAIN: %05d using TABEND 'TAB'...", __LINE__);
		strcpy(pcgTABEnd, "TAB");
	}
	dbg(DEBUG,"<MAIN> %05d found TABEND: <%s>", __LINE__, pcgTABEnd);

	/* read HomeAirPort from SGS.TAB */
	if ((ilRC = tool_search_exco_data("SYS", "HOMEAP", pcgHomeAP)) != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: %05d cannot find entry <HOMEAP> in SGS.TAB", __LINE__);
		dbg(TRACE,"MAIN: %05d using HOMEAP 'HOM'...", __LINE__);
		strcpy(pcgHomeAP, "HOM");
	}
	dbg(DEBUG,"<MAIN> %05d found HOME-AIRPORT: <%s>", __LINE__, pcgHomeAP); 
	sprintf(pcgTwEnd,"%s,%s,%s",pcgTABEnd,pcgHomeAP,mod_name);

	/* FTP-Lib function needs it */
	SetSignals(HandleSignal);

	/* Attach to the MIKE queues */
	do
	{
		ilRC = init_que();
		if(ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}
	}while((ilCnt < 10) && (ilRC != RC_SUCCESS));

	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}
	else
	{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>", mod_id);
	}

	sprintf(cgConfigFile,"%s/ftphdl", getenv("BIN_PATH"));
	ilRC = TransferFile(cgConfigFile);
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	}

	sprintf(cgConfigFile,"%s/ftphdl.cfg", getenv("CFG_PATH")); 
	ilRC = TransferFile(cgConfigFile); 
	if(ilRC != RC_SUCCESS) 
	{ 
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile); 
	}

	ilRC = SendRemoteShutdown(mod_id);
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	} 

	if((ctrl_sta != HSB_STANDALONE) &&
	   (ctrl_sta != HSB_ACTIVE) &&
	   (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"MAIN: waiting for status switch ...");
		dbg(DEBUG,"MAIN: calling HandleQueues");
		HandleQueues();
	}

	if((ctrl_sta == HSB_STANDALONE) ||
	   (ctrl_sta == HSB_ACTIVE) ||
	   (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
		if(igInitOK == FALSE)
		{
			ilRC = InitFTPHdl();
			if(ilRC != RC_SUCCESS)
			{
				dbg(TRACE,"MAIN: InitFTPHdl: init failed!");
				dbg(TRACE,"MAIN: ... terminating ...");
				Terminate(igRestartTimeout);
				/* set to igRestartTimeout by eth */
			} 
		}
	} 
	else 
	{
		dbg(TRACE,"MAIN: HSB status not OK");
		dbg(TRACE,"MAIN: ... terminating ...");
		Terminate(igRestartTimeout);
		/* set to igRestartTimeout by eth */
	}

	dbg(TRACE,"MAIN: initializing OK");

	for(;;)
	{
		ilRC = que( QUE_GETBIG, 0, mod_id, PRIORITY_3, 0, (char *)&prgItem);

		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;
				
		if( ilRC == RC_SUCCESS )
		{
			ilRC = que( QUE_ACK, 0, mod_id, 0, 0, NULL);
			if( ilRC != RC_SUCCESS ) 
			{
				/* HandleQueErr(ilRC); */
				HandleQueErr( __LINE__, ilRC);
			} 
			
			switch( prgEvent->command )
			{
				case	HSB_STANDBY:
					ctrl_sta = prgEvent->command;
					HandleQueues();
					break;	

				case	HSB_COMING_UP:
					ctrl_sta = prgEvent->command;
					HandleQueues();
					break;	

				case	HSB_ACTIVE:
					ctrl_sta = prgEvent->command;
					break;	

				case	HSB_ACT_TO_SBY:
					ctrl_sta = prgEvent->command;
					HandleQueues();
					break;	

				case	HSB_DOWN:
					ctrl_sta = prgEvent->command;
					Terminate(0);
					break;	

				case	HSB_STANDALONE:
					ctrl_sta = prgEvent->command;
					break;	

				case	SHUTDOWN:
					Terminate(0);
					break;

				case	RESET:
					ilRC = Reset();
					break;

				case	EVENT_DATA:
					if((ctrl_sta == HSB_STANDALONE) ||
                                           (ctrl_sta == HSB_ACTIVE) ||
                                           (ctrl_sta == HSB_ACT_TO_SBY))
					{
						ilRC = HandleData();
						if(ilRC != RC_SUCCESS)
						{
							HandleErr(ilRC);
						}
					}
					else
					{
						dbg(TRACE,"MAIN: wrong HSB status <%d>",ctrl_sta);
						DebugPrintItem(TRACE,prgItem);
						DebugPrintEvent(TRACE,prgEvent);
					}
					break;
						
				case	TRACE_ON:
					dbg_handle_debug(prgEvent->command);
					break;

				case	TRACE_OFF:
					dbg_handle_debug(prgEvent->command);
					break;

				case	12345:
					dbg(DEBUG,"MAIN: calling passout");
                                        ilRC = passout();
					dbg(DEBUG,"MAIN: passout returns %d", ilRC);
					break;

				default:
					dbg(TRACE,"MAIN: unknown event");
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
					break;

			} /* end switch */
		}
		else 
		{
			/* HandleQueErr(ilRC); */
			HandleQueErr( __LINE__, ilRC);
		} 

	} /* end for(;;) */

	/* return RC_SUCCESS; */

} /* end of MAIN function */



/******************************************************************************/
/* The initialization routine                                                 */
/* tide up by eth 30.01.2001                                                  */
/******************************************************************************/
static int InitFTPHdl()
{
  int   i; /* counter */
  int	ilRC;
  int	ilCOS;
  int	ilSOS;
  int	ilCurCmd;
  int	ilTimeout;
  int	ilDebugLevel;
  long	llGetFileTimer;
  long	llReceiveTimer;
  char	clTransferType;

  char	*pclCfgPath = NULL; /* wegen getenv */
  char	pclCfgFile[iMIN_BUF_SIZE]; /* [iMIN_BUF_SIZE] */

  char	pclHostName[iMIN_BUF_SIZE]; /* [iMIN_BUF_SIZE] */
  char	pclUser[iMIN_BUF_SIZE]; /* [iMIN_BUF_SIZE] */
  char	pclPasswd[64];
  char	*pclS = NULL;
  char	pclTmpBuf[iMAX_BUF_SIZE]; /* [iMAX_BUF_SIZE] */

  char	pclAllCommands[2*iMAX_BUF_SIZE];

  dbg(DEBUG,"<InitFTPHdl> ---- START INIT ----");

  /*Get limits for file handles
  {
    int rc;
    struct rlimit rlim;
    rc = getrlimit(RLIMIT_NOFILE, &rlim);
    if (rc)
      perror("getrlimit");
    else
      dbg(TRACE,"rlim_cur=%ld\n",(long) rlim.rlim_cur);
  }
*/

  /* to inform the status-handler about startup time */
  if ((ilRC = SendStatus("FILEIO", "ftphdl", "START", 1, "", "", ""))
            != RC_SUCCESS)
  {
    dbg(TRACE,"<InitFTPHdl> %05d SendStatus returns: %d", __LINE__, ilRC);
    dbg(DEBUG,"<InitFTPHdl> ----- END -----");
    return RC_FAIL;
  }

  if ((pclCfgPath = getenv("CFG_PATH")) == NULL)
  {
    dbg(TRACE,"<InitFTPHdl> ERROR: missing environment CFG_PATH...");
    dbg(DEBUG,"<InitFTPHdl> ---- END ----");
    return RC_FAIL;
  }
  else
  {
    dbg(DEBUG,"<InitFTPHdl> CFG_PATH: <%s>", pclCfgPath);
  }

  strcpy(pclCfgFile, pclCfgPath);

  if (pclCfgFile[strlen(pclCfgFile)-1] != '/')
    strcat(pclCfgFile, "/");

  strcat(pclCfgFile, mod_name);
  strcat(pclCfgFile, ".cfg");
  dbg(DEBUG,"<InitFTPHdl> CFG-File: <%s>", pclCfgFile);

  /* mod-id of router... */
  if ((igTimschID = tool_get_q_id("ntisch")) == RC_NOT_FOUND ||
       igTimschID == RC_FAIL)
  {
    if (igTimschID == RC_FAIL)
      dbg(DEBUG,"InitFTPHdl: gTimschID = RC_FAIL");
    if (igTimschID == RC_NOT_FOUND)
      dbg(DEBUG,"InitFTPHdl: gTimschID = RC_NOT_FOUND");

    dbg(TRACE,"<InitFTPHdl> %05d tool_get_q_id(\"ntisch\") returns: %d",
      __LINE__, igTimschID);
    return RC_FAIL;
  }

/* -------------------------------------------------------------- */
/* debug_level -------------------------------------------------- */
/* -------------------------------------------------------------- */
  pclTmpBuf[0] = 0x00;
  if ((ilRC = iGetConfigEntry(pclCfgFile,
                              "MAIN",
                              "debug_level",
                              CFG_STRING,
                              pclTmpBuf))
            != RC_SUCCESS)
  {
    /* default */
    dbg(DEBUG,"<InitFTPHdl> no debug_level in section <MAIN>");
    dbg(DEBUG,"<InitFTPHdl> seting default debug_level");
    strcpy(pclTmpBuf, "TRACE");
  }

  /* which debug_level is set in the Configfile ? */
  StringUPR((UCHAR*)pclTmpBuf);
  if (!strcmp(pclTmpBuf,"DEBUG"))
  {
    debug_level = DEBUG;
    dbg(TRACE,"<InitFTPHdl> debug_level set to DEBUG");
  }
  else if (!strcmp(pclTmpBuf,"TRACE"))
       {
         debug_level = TRACE;
         dbg(TRACE,"<InitFTPHdl> debug_level set to TRACE");
       }
       else if (!strcmp(pclTmpBuf,"OFF"))
            {
              debug_level = 0;
              dbg(TRACE,"<InitFTPHdl> debug_level set to OFF");
            }
            else
            {
              debug_level = 0;
              dbg(TRACE,"<InitFTPHdl> unknown debug_level set to default OFF");
            }

/* -------------------------------------------------------------- */
/* answer_invalid_cmd ------------------------------------------- */
/* -------------------------------------------------------------- */
  pclTmpBuf[0] = 0x00;
  if ((ilRC = iGetConfigEntry(pclCfgFile,
                              "MAIN",
                              "answer_invalid_cmd",
                              CFG_STRING,
                              pclTmpBuf))
            != RC_SUCCESS)
  {
    /* default */
    dbg(DEBUG,"<InitFTPHdl> missing answer-invalid-cmd in section <MAIN>");
    strcpy(pclTmpBuf, "NO");
  }

  /* which OS? */
  StringUPR((UCHAR*)pclTmpBuf);

  if (!strcmp(pclTmpBuf,"NO"))
    rgTM.iAnswerInvalidCmd = 0;
  else if (!strcmp(pclTmpBuf,"YES"))
    rgTM.iAnswerInvalidCmd = 1;
  else
  {
    dbg(TRACE,"<InitFTPHdl> unknown answer-invalid-cmd entry in section <MAIN>");
    rgTM.iAnswerInvalidCmd = 0;
  }
  dbg(DEBUG,"<InitFTPHdl> answer invalid cmd: <%d>", rgTM.iAnswerInvalidCmd);

/* -------------------------------------------------------------- */
/* restart_timeout ---------------------------------------------- */
/* -------------------------------------------------------------- */
/* by eth 6.2.2001 */
  if ((ilRC = iGetConfigEntry(pclCfgFile,
                              "MAIN",
                              "restart_timeout",
                              CFG_INT,
                              (char*)&igRestartTimeout))
            != RC_SUCCESS)
  {
    dbg(TRACE,"<InitFTPHdl> missing restart_timeout in section <MAIN>");
    dbg(DEBUG,"<InitFTPHdl> set restart_timeout to default 60 sec.");
    igRestartTimeout = 60; /* sekunden */
  }
  else
  {
    dbg(DEBUG,"<InitFTPHdl> restart_timeout = %d",igRestartTimeout);
  }

  if (igRestartTimeout < 30 || igRestartTimeout > 300)
  {
    dbg(TRACE,"<InitFTPHdl> restart_timeout value wrong");
    dbg(DEBUG,"<InitFTPHdl> set restart_timeout to default 60 sec.");
    igRestartTimeout = 60; /* sekunden */
  } 
  else 
  {
    dbg(DEBUG,"<InitFTPHdl> restart_timeout value OK");
  }

/* -------------------------------------------------------------- */
/* commands ----------------------------------------------------- */
/* -------------------------------------------------------------- */
  memset((void*)pclAllCommands, 0x00, 2*iMAX_BUF_SIZE);
  if ((ilRC = iGetConfigEntry(pclCfgFile,
                              "MAIN",
                              "commands",
                              CFG_STRING,
                              pclAllCommands))
            != RC_SUCCESS)
  {
    dbg(TRACE,"<InitFTPHdl> missing commands in section MAIN...");
  }
  else
  {
    StringUPR((UCHAR*)pclAllCommands);
    if ((rgTM.iNoOfCmds = GetNoOfElements(pclAllCommands, cCOMMA)) <= 0)
    {
      dbg(TRACE,"<InitFTPHdl> no commands in section MAIN ...");
    }
    else
    {
      dbg(DEBUG,"<InitFTPHdl> found %d commands in MAIN section", rgTM.iNoOfCmds);

      /* get memory */
      if ((rgTM.prCmds = (FTPCmd*)malloc(rgTM.iNoOfCmds*sizeof(FTPCmd))) == NULL)
      {
        /* nix speicher, nix schaffe */
        dbg(TRACE,"<InitFTPHdl> ERROR: malloc failure...");
        dbg(DEBUG,"<InitFTPHdl> ---- END ----");
        return RC_FAIL;
      }

      /* read all commands and corresponding data */
      for (ilCurCmd = 0; ilCurCmd<rgTM.iNoOfCmds; ilCurCmd++)
      {
        strcpy(rgTM.prCmds[ilCurCmd].pcCmd, GetDataField(pclAllCommands, ilCurCmd, cCOMMA));
        dbg(DEBUG,"<InitFTPHdl> ---- START <%s> ----", rgTM.prCmds[ilCurCmd].pcCmd);

        /* set some flags */
       	rgTM.prCmds[ilCurCmd].iValid 		= iFTP_VALID;
       	rgTM.prCmds[ilCurCmd].iErrorCnt 	= 0;
       	rgTM.prCmds[ilCurCmd].tFirstEvent 	= -1;
       	rgTM.prCmds[ilCurCmd].tEventPeriod 	= -1;
       	rgTM.prCmds[ilCurCmd].iTimschNo 	= -1;
       	rgTM.prCmds[ilCurCmd].iIsDynamic	= FALSE;

/******************************************************************************/
        /* ----------------------------------------------------------- */
        /* Home Airport ---------------------------------------------- */
        /* ----------------------------------------------------------- */

       	/* read homeairport for this command */
       	rgTM.prCmds[ilCurCmd].pcHomeAirport[0] = 0x00;

       	(void)iGetConfigEntry(pclCfgFile,
                              rgTM.prCmds[ilCurCmd].pcCmd,
                              "home_airport",
                              CFG_STRING,
                              rgTM.prCmds[ilCurCmd].pcHomeAirport);
       	if (!strlen(rgTM.prCmds[ilCurCmd].pcHomeAirport))
       	{
       	  strcpy(rgTM.prCmds[ilCurCmd].pcHomeAirport, pcgHomeAP);
       	}
       	dbg(DEBUG,"<InitFTPHdl> %05d HomeAirport: <%s>",
         __LINE__, rgTM.prCmds[ilCurCmd].pcHomeAirport);

/******************************************************************************/
       	/* ----------------------------------------------------------- */
       	/* table_extension ------------------------------------------- */
       	/* ----------------------------------------------------------- */

       	/* read table extension for this command */
       	rgTM.prCmds[ilCurCmd].pcTableExtension[0] = 0x00;

       	(void)iGetConfigEntry(pclCfgFile,
                              rgTM.prCmds[ilCurCmd].pcCmd,
                              "table_extension",
                              CFG_STRING,
                              rgTM.prCmds[ilCurCmd].pcTableExtension);
       	if (!strlen(rgTM.prCmds[ilCurCmd].pcTableExtension))
        {
          strcpy(rgTM.prCmds[ilCurCmd].pcTableExtension, pcgTABEnd);
       	}

       	dbg(DEBUG,"<InitFTPHdl> %05d TableExtension: <%s>",
         __LINE__, rgTM.prCmds[ilCurCmd].pcTableExtension);

/******************************************************************************/
       	/* -------------------------------------------------------------- */
       	/* client_os ---------------------------------------------------- */
       	/* -------------------------------------------------------------- */
       	pclTmpBuf[0] = 0x00;
       	if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "client_os",
                                    CFG_STRING,
                                    pclTmpBuf))
                  != RC_SUCCESS)
       	{
          /* default */
          dbg(TRACE,"<InitFTPHdl> missing client OS in section <%s>",
                                      rgTM.prCmds[ilCurCmd].pcCmd);
       	  strcpy(pclTmpBuf, "UNIX");
       	}

       	/* which OS? */
       	StringUPR((UCHAR*)pclTmpBuf);
       	if (!strcmp(pclTmpBuf,"UNIX"))
          ilCOS = OS_UNIX;
       	else if (!strcmp(pclTmpBuf,"WIN"))
       	  ilCOS = OS_WIN;
       	else
       	{
       	  dbg(TRACE,"<InitFTPHdl> unknown client OS in section <%s>",
                                        rgTM.prCmds[ilCurCmd].pcCmd);
       	  ilCOS = OS_UNIX;
       	}
       	dbg(DEBUG,"<InitFTPHdl> client OS: <%s>", sOS(ilCOS));

/******************************************************************************/
        /* -------------------------------------------------------------- */
       	/* server_os ---------------------------------------------------- */
       	/* -------------------------------------------------------------- */
       	pclTmpBuf[0] = 0x00;
       	if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "server_os",
                                    CFG_STRING,
                                    pclTmpBuf))
                  != RC_SUCCESS)
       	{
       	  /* default */
       	  dbg(TRACE,"<InitFTPHdl> missing server OS in section <%s>",
                                        rgTM.prCmds[ilCurCmd].pcCmd);
       	  strcpy(pclTmpBuf, "UNIX");
       	}

       	/* which OS? */
       	StringUPR((UCHAR*)pclTmpBuf);
       	if (!strcmp(pclTmpBuf,"UNIX"))
       	  ilSOS = OS_UNIX;
       	else if (!strcmp(pclTmpBuf,"WIN"))
       	  ilSOS = OS_WIN;
       	else
       	{
       	  dbg(TRACE,"<InitFTPHdl> unknown server OS in section <%s>",
                                        rgTM.prCmds[ilCurCmd].pcCmd);
       	  ilSOS = OS_UNIX;
       	}
       	dbg(DEBUG,"<InitFTPHdl> server OS: <%s>", sOS(ilSOS));

/******************************************************************************/
       	/* -------------------------------------------------------------- */
       	/* debug_level -------------------------------------------------- */
       	/* -------------------------------------------------------------- */
       	pclTmpBuf[0] = 0x00;
       	if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "debug_level",
                                    CFG_STRING,
                                    pclTmpBuf))
                  != RC_SUCCESS)
       	{
       	  /* default */
          dbg(TRACE,"<InitFTPHdl> missing debug level in section <%s>",
                                          rgTM.prCmds[ilCurCmd].pcCmd);
       	  strcpy(pclTmpBuf, "TRACE");
       	}

       	/* what section type? */
       	StringUPR((UCHAR*)pclTmpBuf);
       	if (!strcmp(pclTmpBuf,"DEBUG"))
       	  ilDebugLevel = DEBUG;
       	else if (!strcmp(pclTmpBuf,"TRACE"))
       	  ilDebugLevel = TRACE;
       	else if (!strcmp(pclTmpBuf,"OFF"))
       	  ilDebugLevel = 0;
       	else
       	{
       	  dbg(TRACE,"<InitFTPHdl> unknown debug level in section <%s>",
                                          rgTM.prCmds[ilCurCmd].pcCmd);
       	  ilDebugLevel = TRACE;
       	}
       	dbg(DEBUG,"<InitFTPHdl> debug level: <%s>", sDBGLEVEL(ilDebugLevel));

/******************************************************************************/
       	/* -------------------------------------------------------------- */
       	/* invalid_time ------------------------------------------------- */
       	/* -------------------------------------------------------------- */
       	if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "invalid_time",
                                    CFG_INT,
                                    (char*)&rgTM.prCmds[ilCurCmd].iTimeInvalidOffset))
                  != RC_SUCCESS)
       	{
       	  /* default */
       	  dbg(TRACE,"<InitFTPHdl> missing invalid time in section <%s>",
                                           rgTM.prCmds[ilCurCmd].pcCmd);
       	  rgTM.prCmds[ilCurCmd].iTimeInvalidOffset = 0;
       	}
       	dbg(DEBUG,"<InitFTPHdl> invalid time: %d",
          rgTM.prCmds[ilCurCmd].iTimeInvalidOffset); 

/******************************************************************************/
       	/* -------------------------------------------------------------- */
       	/* try_again ---------------------------------------------------- */
       	/* -------------------------------------------------------------- */
       	if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "try_again",
                                    CFG_INT,
                                    (char*)&rgTM.prCmds[ilCurCmd].iTimeTryAgainOffset))
                  != RC_SUCCESS)
       	{
       	  /* default */
          dbg(TRACE,"<InitFTPHdl> missing try again in section <%s>",
                                        rgTM.prCmds[ilCurCmd].pcCmd);
          rgTM.prCmds[ilCurCmd].iTimeTryAgainOffset = 1;
       	}
       	dbg(DEBUG,"<InitFTPHdl> try again: %d", rgTM.prCmds[ilCurCmd].iTimeTryAgainOffset); 

/******************************************************************************/
       	/* -------------------------------------------------------------- */
       	/* Optional Parameter for SITE command before PUT-Command ------- */
       	/* -------------------------------------------------------------- */
       	/* pclTmpBuf[0] = 0x00; */
       	rgTM.prCmds[ilCurCmd].pcOptSiteCmd[0] = 0x00;

       	if ((ilRC = iGetConfigRow(pclCfgFile,
                                  rgTM.prCmds[ilCurCmd].pcCmd,
                                  "opt_site_cmd",
                                  CFG_STRING,
                                  rgTM.prCmds[ilCurCmd].pcOptSiteCmd))
                  != RC_SUCCESS)
       	{
       	  dbg(DEBUG,"<InitFTPHdl> NO optional Site Command: <%s>",
                              rgTM.prCmds[ilCurCmd].pcOptSiteCmd);
       	}
       	else
       	{
       	  dbg(DEBUG,"<InitFTPHdl> Optional Site to Send: <%s>",
                           rgTM.prCmds[ilCurCmd].pcOptSiteCmd);
        }

/******************************************************************************/
       	/* -------------------------------------------------------------- */
       	/* section_type ------------------------------------------------- */
       	/* -------------------------------------------------------------- */
       	pclTmpBuf[0] = 0x00;
       	if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "section_type",
                                    CFG_STRING,
                                    pclTmpBuf))
                  != RC_SUCCESS)
       	{
       	  /* default */
       	  dbg(TRACE,"<InitFTPHdl> missing section type in section <%s>",
                                           rgTM.prCmds[ilCurCmd].pcCmd);
       	  strcpy(pclTmpBuf, "SEND");
       	}

       	/* what section type? */
       	StringUPR((UCHAR*)pclTmpBuf);
       	if (!strcmp(pclTmpBuf,"SEND"))
       	  rgTM.prCmds[ilCurCmd].iSectionType = iSEND;
       	else if (!strcmp(pclTmpBuf,"RECEIVE"))
       	  rgTM.prCmds[ilCurCmd].iSectionType = iRECEIVE;
       	else
       	{
       	  dbg(TRACE,"<InitFTPHdl> unknown section type in section <%s>",
                                           rgTM.prCmds[ilCurCmd].pcCmd);
       	  rgTM.prCmds[ilCurCmd].iSectionType = iSEND;
       	}
       	dbg(DEBUG,"<InitFTPHdl> section type: <%s>",
          sTYPE(rgTM.prCmds[ilCurCmd].iSectionType));

/******************************************************************************/
       	/* -------------------------------------------------------------- */
       	/* -------------------------------------------------------------- */
       	/* -------------------------------------------------------------- */
       	if (rgTM.prCmds[ilCurCmd].iSectionType == iRECEIVE)
       	{
       	  /* ------------------------------------------------------------ */
       	  /* iRECEIVE --------------------------------------------------- */
          /* ------------------------------------------------------------ */
       	  pclTmpBuf[0] = 0x00;
       	  if ((ilRC = iGetConfigEntry(pclCfgFile,
                                      rgTM.prCmds[ilCurCmd].pcCmd,
                                      "delete_remote_file",
                                      CFG_STRING,
                                      pclTmpBuf))
                    != RC_SUCCESS)
       	  {
       	    /* default */
            dbg(TRACE,"<InitFTPHdl> missing delete remote file in section <%s>",
                                                   rgTM.prCmds[ilCurCmd].pcCmd);
       	    strcpy(pclTmpBuf, "NO");
       	  }

       	  /* what section type? */
       	  StringUPR((UCHAR*)pclTmpBuf);
          if (!strcmp(pclTmpBuf,"NO"))
       	    rgTM.prCmds[ilCurCmd].iDeleteRemoteSourceFile = 0;
       	  else if (!strcmp(pclTmpBuf,"YES"))
       	    rgTM.prCmds[ilCurCmd].iDeleteRemoteSourceFile = 1;
       	  else
       	  {
       	    dbg(TRACE,"<InitFTPHdl> unknown value for delete remote file in section <%s>",
                                                             rgTM.prCmds[ilCurCmd].pcCmd);
       	    rgTM.prCmds[ilCurCmd].iDeleteRemoteSourceFile = 0;
       	  }
       	  dbg(DEBUG,"<InitFTPHdl> delete remote file: %d",
           rgTM.prCmds[ilCurCmd].iDeleteRemoteSourceFile);

       	} /* end of receive */
       	else
       	{
       	  /* ----------------------------------------------------------- */
       	  /* iSEND ----------------------------------------------------- */
       	  /* ----------------------------------------------------------- */
       	  pclTmpBuf[0] = 0x00;
       	  if ((ilRC = iGetConfigEntry(pclCfgFile,
                                      rgTM.prCmds[ilCurCmd].pcCmd,
                                      "delete_local_file",
                                      CFG_STRING,
                                      pclTmpBuf))
                    != RC_SUCCESS)
       	  {
       	    /* default */
       	    dbg(TRACE,"<InitFTPHdl> missing delete local file in section <%s>",
                                                  rgTM.prCmds[ilCurCmd].pcCmd);
       	    strcpy(pclTmpBuf, "NO");
       	  }

       	  /* what section type? */
          StringUPR((UCHAR*)pclTmpBuf);
       	  if (!strcmp(pclTmpBuf,"NO"))
       	    rgTM.prCmds[ilCurCmd].iDeleteLocalSourceFile = 0;
       	  else if (!strcmp(pclTmpBuf,"YES"))
       	    rgTM.prCmds[ilCurCmd].iDeleteLocalSourceFile = 1;
       	  else
          {
       	    dbg(TRACE,"<InitFTPHdl> unknown value for delete local file in section <%s>",
                                                            rgTM.prCmds[ilCurCmd].pcCmd);
       	    rgTM.prCmds[ilCurCmd].iDeleteLocalSourceFile = 0;
       	  }
       	  dbg(DEBUG,"<InitFTPHdl> delete local file: %d",
           rgTM.prCmds[ilCurCmd].iDeleteLocalSourceFile);

       	} /* end of send */

/******************************************************************************/
       	/* -------------------------------------------------------------- */
       	/* retry_counter ------------------------------------------------ */
       	/* -------------------------------------------------------------- */
       	if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "retry_counter",
                                    CFG_INT,
                                    (char*)&rgTM.prCmds[ilCurCmd].iRetryCounter))
                  != RC_SUCCESS)
       	{
       	  /* default */
          dbg(TRACE,"<InitFTPHdl> missing retry counter in section <%s>",
                                    rgTM.prCmds[ilCurCmd].iRetryCounter);
       	  rgTM.prCmds[ilCurCmd].iRetryCounter = 3;
       	}
       	if (rgTM.prCmds[ilCurCmd].iRetryCounter < 0 || 
            rgTM.prCmds[ilCurCmd].iRetryCounter > 10)
       	{
       	  dbg(TRACE,"<InitFTPHdl> retry counter value error: %d",
                            rgTM.prCmds[ilCurCmd].iRetryCounter);
       	  rgTM.prCmds[ilCurCmd].iRetryCounter = 3;
       	}
       	dbg(DEBUG,"<InitFTPHdl> retry counter: %d", rgTM.prCmds[ilCurCmd].iRetryCounter);

/******************************************************************************/
       	/* -------------------------------------------------------------- */
       	/* transfer_type ------------------------------------------------ */
       	/* -------------------------------------------------------------- */
       	pclTmpBuf[0] = 0x00;
       	if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "transfer_type",
                                    CFG_STRING,
                                    pclTmpBuf))
                  != RC_SUCCESS)
       	{
       	  /* default */
       	  dbg(TRACE,"<InitFTPHdl> missing transfer type in section <%s>",
                                            rgTM.prCmds[ilCurCmd].pcCmd);
       	  strcpy(pclTmpBuf, "ASCII");
       	}

       	/* what section type? */
       	StringUPR((UCHAR*)pclTmpBuf);
       	if (!strcmp(pclTmpBuf,"ASCII"))
       	  clTransferType = CEDA_ASCII;
       	else if (!strcmp(pclTmpBuf,"BINARY"))
       	  clTransferType = CEDA_BINARY;
       	else if (!strcmp(pclTmpBuf,"EBCDIC"))
       	  clTransferType = CEDA_EBCDIC;
       	else if (!strcmp(pclTmpBuf,"BYTE"))
       	  clTransferType = CEDA_BYTE;
       	else
       	{
       	  dbg(TRACE,"<InitFTPHdl> unknown transfer type in section <%s>",
                                            rgTM.prCmds[ilCurCmd].pcCmd);
       	  clTransferType = CEDA_ASCII;
       	}
       	dbg(DEBUG,"<InitFTPHdl> transfer type: <%c>", clTransferType);

/******************************************************************************/
       	/* -------------------------------------------------------------- */
       	/* tcp_timeout -------------------------------------------------- */
       	/* -------------------------------------------------------------- */
       	if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "tcp_timeout",
                                    CFG_INT,
                                    (char*)&ilTimeout))
                  != RC_SUCCESS)
       	{
       	  /* default */
       	  dbg(TRACE,"<InitFTPHdl> missing tcp-timeout in section <%s>",
                                          rgTM.prCmds[ilCurCmd].pcCmd);
       	  ilTimeout = 1; /* second */
       	}

       	if (ilTimeout < 1 || ilTimeout > 10)
       	{
       	  dbg(TRACE,"<InitFTPHdl> tcp-timeout value error in section <%s>",
                                              rgTM.prCmds[ilCurCmd].pcCmd);
       	  ilTimeout = 1; /* second */
       	}
       	dbg(DEBUG,"<InitFTPHdl> tcp-timeout: %d", ilTimeout);

/******************************************************************************/
       	/* -------------------------------------------------------------- */
       	/* data_timeout ------------------------------------------------- */
       	/* -------------------------------------------------------------- */
       	if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "data_timeout",
                                    CFG_INT, (char*)&llGetFileTimer))
                  != RC_SUCCESS)
       	{
       	  /* default */
       	  dbg(TRACE,"<InitFTPHdl> missing data-timeout in section <%s>",
                                           rgTM.prCmds[ilCurCmd].pcCmd);
       	  llGetFileTimer = 1; /* seconds */
       	}

       	if (llGetFileTimer < 1 || llGetFileTimer > 10)
       	{
       	  dbg(TRACE,"<InitFTPHdl> data-timeout value error in section <%s>",
                                               rgTM.prCmds[ilCurCmd].pcCmd);
       	  llGetFileTimer = 1; /* seconds */
       	}
       	dbg(DEBUG,"<InitFTPHdl> data-timeout: %d", llGetFileTimer);

/******************************************************************************/
       	/* -------------------------------------------------------------- */
       	/* ctrl_timeout ------------------------------------------------- */
       	/* -------------------------------------------------------------- */
       	if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "ctrl_timeout",
                                    CFG_INT,
                                    (char*)&llReceiveTimer))
                  != RC_SUCCESS)
       	{
       	  /* default */
       	  dbg(TRACE,"<InitFTPHdl> missing ctrl-timeout in section <%s>",
                                           rgTM.prCmds[ilCurCmd].pcCmd);
       	  llReceiveTimer = 1; /* seconds */
       	}

       	if (llReceiveTimer < 1 || llReceiveTimer > 10)
       	{
       	  dbg(TRACE,"<InitFTPHdl> ctrl-timeout value error in section <%s>",
                                               rgTM.prCmds[ilCurCmd].pcCmd);
       	  llReceiveTimer = 1; /* seconds */
       	}
       	dbg(DEBUG,"<InitFTPHdl> ctrl-timeout: %d", llReceiveTimer);

/******************************************************************************/
       	/* -------------------------------------------------------------- */
       	/* ftp_user ----------------------------------------------------- */
       	/* -------------------------------------------------------------- */
       	pclUser[0] = 0x00;
       	if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "ftp_user",
                                    CFG_STRING,
                                    pclUser))
                  != RC_SUCCESS)
       	{
       	  /* cannot work without ftp user... */
       	  dbg(TRACE,"<InitFTPHdl> ERROR: missing ftp-user in section <%s>",
                                              rgTM.prCmds[ilCurCmd].pcCmd);
       	  dbg(DEBUG,"<InitFTPHdl> ---- END ----");
       	  return RC_FAIL;
       	}
       	dbg(DEBUG,"<InitFTPHdl> ftp-user: <%s>", pclUser);


/*******************************************************************************
 * 
 * ftp_password line
 * by eth 24.10.2001
 *
 * now use new function iGetConfigLine
 * to get full line from config file
 */
       	pclTmpBuf[0] = 0x00;
       	if ((ilRC = iGetConfigLine(pclCfgFile,
                                   rgTM.prCmds[ilCurCmd].pcCmd,
                                   "ftp_pass",
                                   pclTmpBuf))
                  != RC_SUCCESS)
       	{
       	  /* default */
       	  dbg(TRACE,"<InitFTPHdl> missing ftp_pass in section <%s>",
                                       rgTM.prCmds[ilCurCmd].pcCmd);
       	  return RC_FAIL;
       	}

        /* dbg(DEBUG,"<InitFTPHdl> read ftp_pass line:\n  <%s>", pclTmpBuf); */

  /* bis "=" suchen */
        i=0;
        /* while((pclTmpBuf[i] != '=') || (i<= strlen(pclTmpBuf))) */
        while(pclTmpBuf[i] != '=')
        {
          i++;
          if (i>15)
          {
       	    dbg(TRACE,"<InitFTPHdl> ERROR with ftp_pass in section <%s>",
                                            rgTM.prCmds[ilCurCmd].pcCmd);
       	    return RC_FAIL;
          }
        }

  /* "=" ueberspringen */
        i++;

  /* bis zum ersten zeichen das nicht blank oder tab ist suchen */
        while((pclTmpBuf[i] == ' ') || ( pclTmpBuf[i] == '\t'))
        {
          i++;
          if (i>20)
          {
       	    dbg(TRACE,"<InitFTPHdl> ERROR with ftp_pass in section <%s>",
                                            rgTM.prCmds[ilCurCmd].pcCmd);
       	    return RC_FAIL;
          }
        }

  /* den rest speichern */
        strcpy(pclPasswd,&pclTmpBuf[i]);

  /* das \n muss noch weg */
        i=0;
        while(pclPasswd[i] != '\n')
        {
          i++;
        }

  /* \n gegen \0 tauschen */
        pclPasswd[i] = '\0';

        dbg(DEBUG,"<InitFTPHdl> crypted ftp_pass word: <%s>", pclPasswd);

  /* encrypt password */
        pclS = pclPasswd;
        CDecode(&pclS);
        strcpy(pclPasswd, pclS);

        dbg(DEBUG,"<InitFTPHdl> encrypted ftp_pass: <%s>", pclPasswd);
        /* by eth 24.10.2001 */
       	dbg(DEBUG,"<InitFTPHdl> ftp-pass: <?>");

/******************************************************************************/
       	/* -------------------------------------------------------------- */
       	/* ftp_pass ----------------------------------------------------- */
       	/* -------------------------------------------------------------- */
#ifdef ETH
       	pclPasswd[0] = 0x00;
       	if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "ftp_pass",
                                    CFG_STRING,
                                    pclPasswd))
                  != RC_SUCCESS)
       	{
       	  /* cannot work without ftp password ... */
       	  dbg(TRACE,"<InitFTPHdl> ERROR: missing ftp-password in section <%s>",
                                              rgTM.prCmds[ilCurCmd].pcCmd);
       	  dbg(DEBUG,"<InitFTPHdl> ---- END ----");
       	  return RC_FAIL;
        }
       	else
       	{
       	  pclS = pclPasswd;
       	  CDecode(&pclS);
       	  strcpy(pclPasswd, pclS);
       	}
       	/* dbg(DEBUG,"<InitFTPHdl> ftp-pass: <%s>", pclPasswd); */
        /* by eth 3.2001 */
       	dbg(DEBUG,"<InitFTPHdl> ftp-pass: <?>");
#endif

/******************************************************************************/
       	/* -------------------------------------------------------------- */
       	/* remote_machine ----------------------------------------------- */
       	/* -------------------------------------------------------------- */
       	pclHostName[0] = 0x00;
       	if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "remote_machine",
                                    CFG_STRING,
                                    pclHostName))
                  != RC_SUCCESS)
       	{
       	  /* cannot work without ftp user... */
       	  dbg(TRACE,"<InitFTPHdl> ERROR: missing machine in section <%s>",
                                             rgTM.prCmds[ilCurCmd].pcCmd);
       	  dbg(DEBUG,"<InitFTPHdl> ---- END ----");
       	  return RC_FAIL;
       	}
       	dbg(DEBUG,"<InitFTPHdl> remote_machine: <%s>", pclHostName);

/******************************************************************************/
       	/* -------------------------------------------------------------- */
       	/* initialize --------------------------------------------------- */
       	/* -------------------------------------------------------------- */
       	/* initialize this */
       	if ((ilRC = CEDA_FTPInit(&rgTM.prCmds[ilCurCmd].rInfo,
                                 pclHostName,
       	                         pclUser,
       	                         pclPasswd,
       	                          clTransferType,
       	                          ilTimeout,
       	                          llGetFileTimer,
       	                          llReceiveTimer,
       	                          ilDebugLevel,
       	                          ilSOS,
                                  ilCOS))
                  != RC_SUCCESS)
        { /* by eth */
       	  dbg(DEBUG,"<InitFTPHdl> Problems with CEDA_FTPInit");
        }
        else
        {
       	  dbg(DEBUG,"<InitFTPHdl> CEDA_FTPInit OK");
        }

/******************************************************************************/
       	/* -------------------------------------------------------------- */
        /* remote_machine_path ------------------------------------------ */
       	/* -------------------------------------------------------------- */
       	rgTM.prCmds[ilCurCmd].pcRemoteFilePath[0] = 0x00;
       	if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "remote_machine_path",
                                    CFG_STRING,
                                    rgTM.prCmds[ilCurCmd].pcRemoteFilePath))
                  == RC_SUCCESS)
       	{
       	  dbg(DEBUG,"<InitFTPHdl> machine-path: <%s>",
              rgTM.prCmds[ilCurCmd].pcRemoteFilePath);
       	}

/******************************************************************************/
       	/* -------------------------------------------------------------- */
       	/* send_answer -------------------------------------------------- */
       	/* -------------------------------------------------------------- */
       	pclTmpBuf[0] = 0x00;
       	if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "send_answer",
                                    CFG_STRING,
                                    pclTmpBuf))
                  != RC_SUCCESS)
       	{
       	  /* default */
       	  dbg(TRACE,"<InitFTPHdl> missing send answer in section <%s>",
                                          rgTM.prCmds[ilCurCmd].pcCmd);
       	  strcpy(pclTmpBuf, "NO");
       	}

       	StringUPR((UCHAR*)pclTmpBuf);

       	if (!strcmp(pclTmpBuf,"NO"))
       	  rgTM.prCmds[ilCurCmd].iSendAnswer = 0;
       	else if (!strcmp(pclTmpBuf,"YES"))
       	  rgTM.prCmds[ilCurCmd].iSendAnswer = 1;
       	else
       	{
       	  dbg(TRACE,"<InitFTPHdl> unknown send answer entry in section <%s>",
                                                rgTM.prCmds[ilCurCmd].pcCmd);
       	  dbg(TRACE,"<InitFTPHdl> set send_answer to NO in section <%s>",
                                                rgTM.prCmds[ilCurCmd].pcCmd);
       	  rgTM.prCmds[ilCurCmd].iSendAnswer = 0;
       	}
       	dbg(DEBUG,"<InitFTPHdl> send answer: <%s>",
                rgTM.prCmds[ilCurCmd].iSendAnswer ? "YES" : "NO");

/******************************************************************************/
        /* -------------------------------------------------------------- */
       	/* structure_code ----------------------------------------------- */
       	/* -------------------------------------------------------------- */
       	pclTmpBuf[0] = 0x00;
       	if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "structure_code",
                                    CFG_STRING,
                                    pclTmpBuf))
                  != RC_SUCCESS)
       	{
       	  /* default */
       	  dbg(TRACE,"<InitFTPHdl> missing structure-code in section <%s>",
                                             rgTM.prCmds[ilCurCmd].pcCmd);
       	  strcpy(pclTmpBuf,"FILE");
       	}

       	StringUPR((UCHAR*)pclTmpBuf);
       	if (!strcmp(pclTmpBuf,"FILE"))
       	  rgTM.prCmds[ilCurCmd].cStructureCode = CEDA_FILE;
       	else if (!strcmp(pclTmpBuf,"RECORD"))
       	  rgTM.prCmds[ilCurCmd].cStructureCode = CEDA_RECORD;
       	else if (!strcmp(pclTmpBuf,"PAGE"))
       	  rgTM.prCmds[ilCurCmd].cStructureCode = CEDA_PAGE;
       	else
       	{
       	  dbg(TRACE,"<InitFTPHdl> unknown structure code in section <%s>",
                                             rgTM.prCmds[ilCurCmd].pcCmd);
       	  rgTM.prCmds[ilCurCmd].cStructureCode = CEDA_FILE;
       	}
       	dbg(DEBUG,"<InitFTPHdl> StructureCode: <%c>",
               rgTM.prCmds[ilCurCmd].cStructureCode);

/******************************************************************************/
       	/* -------------------------------------------------------------- */
       	/* transfer_mode ------------------------------------------------ */
       	/* -------------------------------------------------------------- */
       	pclTmpBuf[0] = 0x00;
       	if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "transfer_mode",
                                    CFG_STRING,
                                    pclTmpBuf))
                  != RC_SUCCESS)
       	{
       	  /* default */
       	  dbg(TRACE,"<InitFTPHdl> missing transfer-mode in section <%s>",
                                            rgTM.prCmds[ilCurCmd].pcCmd);
       	  strcpy(pclTmpBuf,"STREAM");
       	}
       	StringUPR((UCHAR*)pclTmpBuf);
       	if (!strcmp(pclTmpBuf,"STREAM"))
          rgTM.prCmds[ilCurCmd].cTransferMode = CEDA_STREAM;
       	else if (!strcmp(pclTmpBuf,"BLOCK"))
       	  rgTM.prCmds[ilCurCmd].cTransferMode = CEDA_BLOCK;
       	else if (!strcmp(pclTmpBuf,"COMPRESSED"))
       	  rgTM.prCmds[ilCurCmd].cTransferMode = CEDA_COMPRESSED;
       	else
       	{
       	  dbg(TRACE,"<InitFTPHdl> unknown transfer mode in section <%s>",
                                            rgTM.prCmds[ilCurCmd].pcCmd);
       	  rgTM.prCmds[ilCurCmd].cTransferMode = CEDA_STREAM;
	}
       	dbg(DEBUG,"<InitFTPHdl> TransferMode: <%c>",
               rgTM.prCmds[ilCurCmd].cTransferMode);

/******************************************************************************/
        /* -------------------------------------------------------------- */
       	/* store_type --------------------------------------------------- */
       	/* -------------------------------------------------------------- */
       	pclTmpBuf[0] = 0x00;
       	if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "store_type",
                                    CFG_STRING,
                                    pclTmpBuf))
                  != RC_SUCCESS)
       	{
       	  /* default */
       	  dbg(TRACE,"<InitFTPHdl> missing store-type in section <%s>",
                                         rgTM.prCmds[ilCurCmd].pcCmd);
       	  strcpy(pclTmpBuf,"CREATE");
        }
       	StringUPR((UCHAR*)pclTmpBuf);
       	if (!strcmp(pclTmpBuf,"CREATE"))
       	  rgTM.prCmds[ilCurCmd].iStoreType = CEDA_CREATE;
       	else if (!strcmp(pclTmpBuf,"APPEND"))
       	  rgTM.prCmds[ilCurCmd].iStoreType = CEDA_APPEND;
       	else
       	{
       	  dbg(TRACE,"<InitFTPHdl> unknown store type in section <%s>",
                                         rgTM.prCmds[ilCurCmd].pcCmd);
       	  rgTM.prCmds[ilCurCmd].iStoreType = CEDA_CREATE;
       	}
       	dbg(DEBUG,"<InitFTPHdl> StoreType: <%d>",
               rgTM.prCmds[ilCurCmd].iStoreType);

/******************************************************************************/
       	/* -------------------------------------------------------------- */
       	/* local_file_name ---------------------------------------------- */
       	/* -------------------------------------------------------------- */
       	if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "local_file_name",
                                    CFG_STRING,
                                    rgTM.prCmds[ilCurCmd].pcLocalFileName))
                  != RC_SUCCESS)
       	{
       	  dbg(TRACE,"<InitFTPHdl> ERROR: missing local file name in section <%s>",
                                                     rgTM.prCmds[ilCurCmd].pcCmd);
       	  dbg(DEBUG,"<InitFTPHdl> ---- END ----");
       	  return RC_FAIL;
       	}
       	dbg(DEBUG,"<InitFTPHdl> Local filename: <%s>",
               rgTM.prCmds[ilCurCmd].pcLocalFileName);

/******************************************************************************/
       	/* -------------------------------------------------------------- */
       	/* local_file_path ---------------------------------------------- */
       	/* -------------------------------------------------------------- */
       	if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "local_file_path",
                                    CFG_STRING,
                                    rgTM.prCmds[ilCurCmd].pcLocalFilePath))
                  != RC_SUCCESS)
       	{
       	  dbg(TRACE,"<InitFTPHdl> ERROR: missing local file path in section <%s>",
                                                     rgTM.prCmds[ilCurCmd].pcCmd);
       	  dbg(DEBUG,"<InitFTPHdl> ---- END ----");
       	  return RC_FAIL;
       	}
       	dbg(DEBUG,"<InitFTPHdl> Local filepath: <%s>",
               rgTM.prCmds[ilCurCmd].pcLocalFilePath);

/******************************************************************************/
       	/* -------------------------------------------------------------- */
       	/* remote_file_name --------------------------------------------- */
       	/* -------------------------------------------------------------- */
       	rgTM.prCmds[ilCurCmd].pcRemoteFileName[0] = 0x00;
       	if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "remote_file_name",
                                    CFG_STRING,
                                    rgTM.prCmds[ilCurCmd].pcRemoteFileName))
                  == RC_SUCCESS)
       	{
       	  dbg(DEBUG,"<InitFTPHdl> Remote filename: <%s>",
                 rgTM.prCmds[ilCurCmd].pcRemoteFileName);
       	}

/******************************************************************************/
       	/* -------------------------------------------------------------- */
       	/* rename_remote_file ------------------------------------------- */
       	/* -------------------------------------------------------------- */
       	rgTM.prCmds[ilCurCmd].pcRenameRemoteFile[0] = 0x00;
       	if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "rename_remote_file",
                                    CFG_STRING,
                                    rgTM.prCmds[ilCurCmd].pcRenameRemoteFile))
                  == RC_SUCCESS)
       	{
       	  dbg(DEBUG,"<InitFTPHdl> Rename Remote file: <%s>",
                  rgTM.prCmds[ilCurCmd].pcRenameRemoteFile);
       	}

/******************************************************************************/
       	/*rkl 2.9.99 ---------------------------------------------------- */
       	/* -------------------------------------------------------------- */
       	/* remote_append_extension -------------------------------------- */
       	/* -------------------------------------------------------------- */
        /* read all for renaming extension... */
        if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "remote_append_extension",
                                    CFG_STRING,
                                    rgTM.prCmds[ilCurCmd].pcRemoteFileAppendExtension))
                  == RC_SUCCESS)
        {
       	  rgTM.prCmds[ilCurCmd].iRemoteFileAppendExtension = 1;
        }
       	else
        {
          rgTM.prCmds[ilCurCmd].iRemoteFileAppendExtension = 0;
       	  memset((void*)rgTM.prCmds[ilCurCmd].pcRemoteFileAppendExtension,
                                                     0x00, iMIN_BUF_SIZE);
       	}

/******************************************************************************/
        /*rkl ----------------------------------------------------------- */
       	/* -------------------------------------------------------------- */
       	/* rename_local_file -------------------------------------------- */
       	/* -------------------------------------------------------------- */
       	rgTM.prCmds[ilCurCmd].pcRenameLocalFile[0] = 0x00;
       	if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "rename_local_file",
                                    CFG_STRING,
                                    rgTM.prCmds[ilCurCmd].pcRenameLocalFile))
                  == RC_SUCCESS)
       	{
       	  dbg(DEBUG,"<InitFTPHdl> Rename Local file: <%s>",
                  rgTM.prCmds[ilCurCmd].pcRenameLocalFile);
       	}
/******************************************************************************/
       	/* -------------------------------------------------------------- */
       	/* alert name for "Alerter" application                           */
       	/* -------------------------------------------------------------- */
       	rgTM.prCmds[ilCurCmd].pcAlertName[0] = 0x00;
       	if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "alert_name",
                                    CFG_STRING,
                                    rgTM.prCmds[ilCurCmd].pcAlertName))
                  == RC_SUCCESS)
       	{
       	  dbg(DEBUG,"<InitFTPHdl> Alert-Name: <%s>",
                  rgTM.prCmds[ilCurCmd].pcAlertName);
       	}

/******************************************************************************/
       	/* rkl 30.08.99 ------------------------------------------------- */
       	/* -------------------------------------------------------------- */
       	/* send_command ------------------------------------------------- */
       	/* -------------------------------------------------------------- */
       	if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "send_command",
                                    CFG_STRING,
                                    rgTM.prCmds[ilCurCmd].pcSendCommand))
                  != RC_SUCCESS)
       	{ 
          /* default */
       	  rgTM.prCmds[ilCurCmd].pcSendCommand[0] = 0x00;
       	}
       	StringUPR((UCHAR*)rgTM.prCmds[ilCurCmd].pcSendCommand);
       	dbg(DEBUG,"<InitFTPHdl> Send Command: <%s>",
               rgTM.prCmds[ilCurCmd].pcSendCommand);

/******************************************************************************/
       	/* -------------------------------------------------------------- */
       	/* mod_id ------------------------------------------------------- */
       	/* -------------------------------------------------------------- */
        /* Read MOD_ID for this command */
       	if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    rgTM.prCmds[ilCurCmd].pcCmd,
                                    "mod_id",
                                    CFG_INT,
                                    (char*)&rgTM.prCmds[ilCurCmd].iModID ))
                  != RC_SUCCESS)
       	{
       	  rgTM.prCmds[ilCurCmd].iModID = 0;
       	}
       	dbg(DEBUG,"<InitFTPHdl> mod_id: <%d>", rgTM.prCmds[ilCurCmd].iModID);

/******************************************************************************/
       	/* rkl-------------------------------------------------------------- */

       	dbg(DEBUG,"<InitFTPHdl> ---- END <%s> ----", rgTM.prCmds[ilCurCmd].pcCmd);

      } /* end for ... all commands and corresponding data ... */
    }
  } /* end of ... section MAIN */

  dbg(DEBUG,"<InitFTPHdl> ---- END INIT ----");

  /* everything looks great */
  igInitOK = TRUE;

  /* that's all */
  return RC_SUCCESS;

} /* end of InitFTPHdl */




/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
	int	ilRC;
	int	ilCurCmd;
	
	dbg(TRACE,"<Reset> now resetting");

	/* to inform the status-handler about end time */
	if ((ilRC = SendStatus("FILEIO", "ftphdl", "STOP", 1, "", "", "")) != RC_SUCCESS)
	{
		dbg(TRACE,"<Reset> %05d SendStatus returns: %d", __LINE__, ilRC);
		Terminate(0); /* set to 0 by eth 6.2.2001 */
	}

	/* delete all ntisch entries */
	for (ilCurCmd = 0; ilCurCmd<rgTM.iNoOfCmds; ilCurCmd++)
		if (rgTM.prCmds[ilCurCmd].iTimschNo > -1)
		{
			ilRC = DeleteTimschEvent(rgTM.prCmds[ilCurCmd].iTimschNo);
			dbg(DEBUG,"<Reset> DeleteTimschEvent with ID %d returns %d",
                                             rgTM.prCmds[ilCurCmd].iTimschNo, ilRC);
		}
			

	/* delete item memory */
	if (prgItem != NULL)
	{
		free(prgItem);
		prgItem = NULL;
	}

	/* delete structure memory */
	if (rgTM.prCmds != NULL)
	{
		free(rgTM.prCmds);
		rgTM.prCmds = NULL;
	}

	/* re-init the process... */
	igInitOK = FALSE;
	ilRC = InitFTPHdl();
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"<Reset> InitFTPHdl: init failed!");
		Terminate(igRestartTimeout);
                /* set to igRestartTimeout by eth 6.2.2001 */
	} 

	dbg(TRACE,"<Reset> finished");
	return RC_SUCCESS;

} /* end of Reset */




/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(UINT ipSleepTime)
{
	int		ilRC;
	int		ilCurCmd;

	/* delete all ntisch entries */
	for (ilCurCmd = 0; ilCurCmd < rgTM.iNoOfCmds; ilCurCmd++)
		if (rgTM.prCmds[ilCurCmd].iTimschNo > -1)
		{
			ilRC = DeleteTimschEvent(rgTM.prCmds[ilCurCmd].iTimschNo);
			dbg(DEBUG,"<Terminate> DeleteTimschEvent with ID %d returns %d",
                                                 rgTM.prCmds[ilCurCmd].iTimschNo, ilRC);
		}

	if (ipSleepTime > 0)
	{
		dbg(TRACE,"<Terminate> sleeping %d seconds", ipSleepTime);
		sleep(ipSleepTime);
	}

	/* to inform the status-handler about end time */
	if ((ilRC = SendStatus("FILEIO", "ftphdl", "STOP", 1, "", "", "")) != RC_SUCCESS)
	{
		dbg(TRACE,"<Terminate> %05d SendStatus returns: %d", __LINE__, ilRC);
	}

	dbg(TRACE,"Exit now ...");
	exit(0);

} /* end of Terminate */




/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
	/* dbg(TRACE,"HandleSignal: signal <%d> received",pipSig); */

	switch(pipSig)
	{
		case SIGPIPE:
			dbg(TRACE,"HandleSignal: signal SIGPIPE received");
			break;
		case SIGALRM:
			/* dbg(TRACE,"HandleSignal: signal SIGALRM received"); */
			break;
		case SIGCLD:
			if (igMultipleSend==FALSE)
			{
				dbg(TRACE,"HandleSignal: unexpected signal SIGCLD/SIGCHLD received! Exit now ...!");
				exit(0);
			}
			break;
		default	:
			dbg(TRACE,"HandleSignal: Exit now ...");
			exit(0);
			break;
	} 
} 


/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int ipErr)
{
	dbg(TRACE,"<HandleErr> calling with error-code: %d", ipErr);
	return;
}


/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr( int line, int pipErr)
{
	dbg(DEBUG,"<HandleQueErr> called in line %d",line);

	switch(pipErr) 
	{
		case	QUE_E_FUNC	:	/* Unknown function */
			dbg(TRACE,"<HandleQueErr> <%d> : unknown function",pipErr);
			break;
		case	QUE_E_MEMORY	:	/* Malloc reports no memory */
			dbg(TRACE,"<HandleQueErr> <%d> : malloc failed",pipErr);
			break;
		case	QUE_E_SEND	:	/* Error using msgsnd */
			dbg(TRACE,"<HandleQueErr> <%d> : msgsnd failed",pipErr);
			break;
		case	QUE_E_GET	:	/* Error using msgrcv */
			dbg(TRACE,"<HandleQueErr> <%d> : msgrcv failed",pipErr);
			break;
		case	QUE_E_EXISTS	:
			dbg(TRACE,"<HandleQueErr> <%d> : route/queue already exists ",pipErr);
			break;
		case	QUE_E_NOFIND	:
			dbg(TRACE,"<HandleQueErr> <%d> : route not found ",pipErr);
			break;
		case	QUE_E_ACKUNEX	:
			dbg(TRACE,"<HandleQueErr> <%d> : unexpected ack received ",pipErr);
			break;
		case	QUE_E_STATUS	:
			dbg(TRACE,"<HandleQueErr> <%d> : unknown queue status ",pipErr);
			break;
		case	QUE_E_INACTIVE	:
			dbg(TRACE,"<HandleQueErr> <%d> : queue is inaktive ",pipErr);
			break;
		case	QUE_E_MISACK	:
			dbg(TRACE,"<HandleQueErr> <%d> : missing ack ",pipErr);
			break;
		case	QUE_E_NOQUEUES	:
			dbg(TRACE,"<HandleQueErr> <%d> : queue does not exist",pipErr);
			break;
		case	QUE_E_RESP	:	/* No response on CREATE */
			dbg(TRACE,"<HandleQueErr> <%d> : no response on create",pipErr);
			break;
		case	QUE_E_FULL	:
			dbg(TRACE,"<HandleQueErr> <%d> : too many route destinations",pipErr);
			break;
		case	QUE_E_NOMSG	:	/* No message on queue */
			dbg(TRACE,"<HandleQueErr> <%d> : no messages on queue",pipErr);
			break;
		case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
			dbg(TRACE,"<HandleQueErr> <%d> : invalid originator=0",pipErr);
			break;
		case	QUE_E_NOINIT	:	/* Queues is not initialized*/
			dbg(TRACE,"<HandleQueErr> <%d> : queues are not initialized",pipErr);
			break;
		case	QUE_E_ITOBIG	:
			dbg(TRACE,"<HandleQueErr> <%d> : requestet itemsize to big ",pipErr);
			break;
		case	QUE_E_BUFSIZ	:
			dbg(TRACE,"<HandleQueErr> <%d> : receive buffer to small ",pipErr);
			break;
		default			:	/* Unknown queue error */
			dbg(TRACE,"<HandleQueErr> <%d> : unknown error",pipErr);
			break;
	}
         
	return;

} /* end of HandleQueErr */



/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRC = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	do /* while (ilBreakOut == FALSE) */
	{
		ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,0,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	

		if( ilRC == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRC != RC_SUCCESS ) 
			{
				dbg(DEBUG,"HandleQueues: QUE_ACK error ... calling HandleQueErr");
				/* HandleQueErr(ilRC); */
				HandleQueErr( __LINE__, ilRC);
			} 
		
			switch( prgEvent->command )
			{
				case	HSB_STANDBY	:
					ctrl_sta = prgEvent->command;
					break;	
		
				case	HSB_COMING_UP	:
					ctrl_sta = prgEvent->command;
					break;	
		
				case	HSB_ACTIVE	:
					ctrl_sta = prgEvent->command;
					ilBreakOut = TRUE;
					break;	

				case	HSB_ACT_TO_SBY	:
					ctrl_sta = prgEvent->command;
					break;	
		
				case	HSB_DOWN	:
					ctrl_sta = prgEvent->command;
					Terminate(0);
					break;	
		
				case	HSB_STANDALONE	:
					ctrl_sta = prgEvent->command;
					ilBreakOut = TRUE;
					break;	

				case	SHUTDOWN	:
					Terminate(0);
					break;
							
				case	RESET		:
					ilRC = Reset();
					break;
							
				case	EVENT_DATA	:
					dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
					break;
						
				case	TRACE_ON :
					dbg_handle_debug(prgEvent->command);
					break;
				case	TRACE_OFF :
					dbg_handle_debug(prgEvent->command);
					break;
				default			:
					dbg(TRACE,"HandleQueues: unknown event");
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
					break;
			}
		}
		else 
		{
			dbg(DEBUG,"HandleQueues: calling HandleQueErr");
			/* HandleQueErr(ilRC); */
			HandleQueErr( __LINE__, ilRC);
		} 

	} while (ilBreakOut == FALSE);

	if (igInitOK == FALSE)
	{
		dbg(DEBUG,"HandleQueues: igInitOK is FALSE");
		dbg(DEBUG,"HandleQueues: calling InitFTPHdl");
		ilRC = InitFTPHdl();
		if (ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"HandleQueues: InitFTPHdl failed!");
			Terminate(igRestartTimeout);
		}
		else
		  dbg(DEBUG,"HandleQueues: InitFTPHdl OK!");
	}

	return;

} /* end of HandleQueues */



/******************************************************************************/
/* The handle data routine                                                    */
/* tide up by eth 17.01.2001                                                  */
/******************************************************************************/

static int CreateDynamicConfigFromDatalist(FTPConfig *prpConfig,char *pcpData)
{
	int ilRc = RC_SUCCESS;
	char clTmpBuffer[iMAX_BUF_SIZE+1];

	dbg(DEBUG,"CreateDynamicConfigFromDatalist pcpData=<%s>",pcpData);
	get_real_item(clTmpBuffer,pcpData,2); /* iModID */
	prpConfig->iModID = atoi(clTmpBuffer);
	get_real_item(clTmpBuffer,pcpData,3); /* iFtpRC */
	prpConfig->iFtpRC = atoi(clTmpBuffer);
	get_real_item(prpConfig->pcCmd,pcpData,4); /* pcCmd */
	get_real_item(prpConfig->pcHostName,pcpData,5); /* pcHostName */
	get_real_item(prpConfig->pcUser,pcpData,6); /* pcUser */
	get_real_item(prpConfig->pcPasswd,pcpData,7); /* pcPasswd */
	get_real_item(clTmpBuffer,pcpData,8); /* cTransferType */
	prpConfig->cTransferType = *clTmpBuffer;
	get_real_item(clTmpBuffer,pcpData,9); /* iTimeout */
	prpConfig->iTimeout = atoi(clTmpBuffer);

	get_real_item(clTmpBuffer,pcpData,10); /* iGetFileTimer */
	prpConfig->lGetFileTimer = atoi(clTmpBuffer);
	get_real_item(clTmpBuffer,pcpData,11); /* iReceiveTimer */
	prpConfig->lReceiveTimer = atoi(clTmpBuffer);
	get_real_item(clTmpBuffer,pcpData,12); /* iDebugLevel */
	prpConfig->iDebugLevel = atoi(clTmpBuffer);
	get_real_item(clTmpBuffer,pcpData,13); /* iClientOS */
	prpConfig->iClientOS = atoi(clTmpBuffer);
	get_real_item(clTmpBuffer,pcpData,14); /* iServerOS */
	prpConfig->iServerOS = atoi(clTmpBuffer);
	get_real_item(clTmpBuffer,pcpData,15); /* iRetryCounter */
	prpConfig->iRetryCounter = atoi(clTmpBuffer);
	get_real_item(clTmpBuffer,pcpData,16); /* iDeleteRemoteSourceFile */
	prpConfig->iDeleteRemoteSourceFile = atoi(clTmpBuffer);
	get_real_item(clTmpBuffer,pcpData,17); /* iDeleteLocalSourceFile */
	prpConfig->iDeleteLocalSourceFile = atoi(clTmpBuffer);
	get_real_item(clTmpBuffer,pcpData,18); /* iSectionType */
	prpConfig->iSectionType = atoi(clTmpBuffer);
	get_real_item(clTmpBuffer,pcpData,19); /* iInvalidOffset */
	prpConfig->iInvalidOffset = atoi(clTmpBuffer);
	get_real_item(clTmpBuffer,pcpData,20); /* iTryAgainOffset */
	prpConfig->iTryAgainOffset = atoi(clTmpBuffer);
	get_real_item(prpConfig->pcHomeAirport,pcpData,21); /* pcHomeAirport */
	get_real_item(prpConfig->pcTableExtension,pcpData,22); /* pcTableExtension */
	get_real_item(prpConfig->pcLocalFilePath,pcpData,23); /* pcLocalFilePath */
	get_real_item(prpConfig->pcLocalFileName,pcpData,24); /* pcLocalFileName */
	get_real_item(prpConfig->pcRemoteFileName,pcpData,25); /* pcRemoteFileName */
	get_real_item(prpConfig->pcRemoteFilePath,pcpData,26); /* pcRemoteFilePath */
	get_real_item(prpConfig->pcRenameLocalFile,pcpData,27); /* pcRenameLocalFile */
	get_real_item(prpConfig->pcRenameRemoteFile,pcpData,28); /* pcRenameRemoteFile */
	get_real_item(clTmpBuffer,pcpData,29); /* iSendAnswer */
	prpConfig->iSendAnswer = atoi(clTmpBuffer);
	get_real_item(clTmpBuffer,pcpData,30); /* cStructureCode */
	prpConfig->cStructureCode = *clTmpBuffer;
	get_real_item(clTmpBuffer,pcpData,31); /* cTransferMode */
	prpConfig->cTransferMode = *clTmpBuffer;
	get_real_item(clTmpBuffer,pcpData,32); /* iStoreType */
	prpConfig->iStoreType = atoi(clTmpBuffer);
	prpConfig->data[0] = '\0';

	return ilRc;
}


static int HandleData()
{
  int		i;
  int		ilRC;
  int		ilSelCnt;
  int		ilFoundCmd;
  int		ilPeriod;
  int		ilValidFlg;
  float		flPeriod;
  time_t	t2ndEvent;

  BC_HEAD	*prlBCHead 	= NULL;
  CMDBLK	*prlCmdblk 	= NULL;
  char		*pclSelection 	= NULL;
  char		*pclFields 	= NULL;
  char		*pclData 	= NULL;
  char		*pclS		= NULL;
  FTPCmd	*prlCmds	= NULL;
  FTPConfig	*prlConfig	= NULL;
  FTPConfig	rlFTPConfig;
	int ilConfigType = 0;

  char		pclTmpSelection[2*iMAX_BUF_SIZE];
  char		pclAnswerString[2*iMAX_BUF_SIZE];


  if (debug_level == DEBUG)
    dbg(DEBUG,"HandleData: debug_level = DEBUG");
  if (debug_level == TRACE)
    dbg(TRACE,"HandleData: debug_level = TRACE");

  /* set BCHead pointer */
  prlBCHead = (BC_HEAD*)((char*)prgEvent+sizeof(EVENT));
  if (prlBCHead->rc == RC_FAIL)
  {
    /* this is an error */
    dbg(DEBUG,"<HandleData> BC_Head->rc: %d", prlBCHead->rc);
    dbg(DEBUG,"<HandleData> ERROR-MESSAGE: <%s>", prlBCHead->data);

    /* set local pointer */
    prlCmdblk 	= (CMDBLK*)(prlBCHead->data + strlen(prlBCHead->data) + 1);
    pclSelection = prlCmdblk->data;
    pclFields 	= pclSelection + strlen(pclSelection) + 1;
    pclData 	= pclFields + strlen(pclFields) + 1;
    *pclData	= 0x00;
  }
  else
  {
    /* this is not an error */
    dbg(TRACE,"<HandleData> prlBCHead OK");

    /* set local pointer */
    /* eth ... hier fehlt wohl was ... siehe oben */
    prlCmdblk    = (CMDBLK*)prlBCHead->data;
    pclSelection = prlCmdblk->data;
    pclFields    = pclSelection + strlen(pclSelection) + 1;
    pclData      = pclFields + strlen(pclFields) + 1;
    strcpy(pcgTwStart,prlCmdblk->tw_start);
  }

    /* is this a new (dynamic) configuration */
    if (!strcmp(pclData, "DYN"))
		{
			ilConfigType = DYNCONFIGOLD;
		}
    if (!strncmp(pclData, "NDC",3))
		{
			ilConfigType = DYNCONFIGNEW;
		}
    if (ilConfigType)
		{
			if (ilConfigType == DYNCONFIGNEW)
			{
			
				dbg(DEBUG,"<HandleData> found dynamic configuration... ");

        CreateDynamicConfigFromDatalist(&rlFTPConfig,pclData);
				prlConfig = &rlFTPConfig;
			}
			else
			{
				prlConfig = (FTPConfig*)(pclData + strlen(pclData) + 1);
			}
				if ((ilRC = AddDynamicConfig(prlConfig)) != RC_SUCCESS)
				{
					dbg(TRACE,"<HandleData> AddDynamicConfig returns: %d", ilRC);

					/* sender answer to sender */
					if (prlConfig->iSendAnswer)
					{
						dbg(TRACE,"<HandleData> sender answer to sender");

						sprintf(pclAnswerString, "FTP_INIT_ERROR,%s", pclSelection);
						prlConfig->iFtpRC = FTP_INIT_ERROR;
						(void)SendAnswer(prlCmdblk->command,
														 pclAnswerString, "UNK", "UNK",
														 prlConfig);
					}
					/* bye bye */
					dbg(DEBUG,"HandleData: problem with AddDynamicConfig");
					return RC_SUCCESS;
				}
    dbg(DEBUG,"HandleData: added dynamic configuration");
  } /* end DYN */
  else
  {
    dbg(DEBUG,"HandleData: no dynamic configuration");
  }

  dbg(DEBUG,"\n");
  dbg(DEBUG,"<HandleData> ---- START <%s> ----", prlCmdblk->command);

  /* save selection */
  pclTmpSelection[0] = 0x00;
  strcpy(pclTmpSelection, pclSelection);

  /* search command in configuration */
  if ((ilFoundCmd = GetCmdNo(prlCmdblk->command)) == -1)
  {
    dbg(TRACE,"<HandleData> command <%s> not found", prlCmdblk->command);
    dbg(TRACE,"<HandleData> originator is: <%d>", prgEvent->originator);

    /* sender answer to sender */
    if (rgTM.iAnswerInvalidCmd)
    {
      sprintf(pclAnswerString, "CMD_UNKNOWN,%s", pclTmpSelection);

      if (prlConfig != NULL) prlConfig->iFtpRC = FTP_CMD_UNKNOWN;	

      (void)SendAnswer(prlCmdblk->command,
                       pclAnswerString, "UNK", "UNK",
                       prlConfig);
    }
  }
  else /* found command in configuration */
  {
    /* first of all, set pointer */
    prlCmds = &rgTM.prCmds[ilFoundCmd];

    /* here we know command number and cmd-pointer is set */
    /* is there a filename?, a path, a valid-flag? */
    ilSelCnt = GetNoOfElements(pclSelection, ',');

    dbg(DEBUG,"<HandleData> found %d selection members", ilSelCnt);

    /* clear all temporary buffers */
    prlCmds->pcQUELocalFilePath[0] 	= 0x00;
    prlCmds->pcQUELocalFileName[0] 	= 0x00;
    prlCmds->pcQUERemoteFilePath[0] 	= 0x00;
    prlCmds->pcQUERemoteFileName[0] 	= 0x00;

    /* read all elements */
    for (i=0, ilValidFlg=FALSE; i<ilSelCnt; i++)
    {
      pclS = GetDataField(pclSelection, i, ',');

      if (!strncmp(pclS, "VALID", 5))
      {
        ilValidFlg = TRUE;
      }
      else if (!strncmp(pclS, "REMOTEPATH:", 11))
           {
             strcpy(prlCmds->pcQUERemoteFilePath, pclS+11);
             dbg(DEBUG,"<HandleData> found remote path: <%s>",
                                prlCmds->pcQUERemoteFilePath);
           }
           else if (!strncmp(pclS, "REMOTEFILE:", 11))
                {
                  strcpy(prlCmds->pcQUERemoteFileName, pclS+11);
                  dbg(DEBUG,"<HandleData> found remote file: <%s>",
                                     prlCmds->pcQUERemoteFileName);
                }
                else if (!strncmp(pclS, "LOCALPATH:", 10))
                     {
                       strcpy(prlCmds->pcQUELocalFilePath, pclS+10);
                       dbg(DEBUG,"<HandleData> found local path: <%s>",
                                          prlCmds->pcQUELocalFilePath);
                     }
                     else if (!strncmp(pclS, "LOCALFILE:", 10))
                          {
                            strcpy(prlCmds->pcQUELocalFileName, pclS+10);
                            dbg(DEBUG,"<HandleData> found local file: <%s>",
                                               prlCmds->pcQUELocalFileName);
                          }
                          else
                          {
                            dbg(TRACE,"HandleData: Selection-entry ignored");
                            dbg(DEBUG,"HandleData: pclS = %s", pclS);
                          }
    } /* end for ... */

    /* copy valid to selection (alone) */
    if (ilValidFlg) strcpy(pclSelection, "VALID");

    /* is this the first event, only normal (not dynamic) events ? */
    if (strcmp(pclSelection, "VALID"))
    {
      if (prlCmds->tEventPeriod == -1 || prlCmds->tFirstEvent == -1)
      {
        if (prlCmds->tFirstEvent == -1)
        {
          prlCmds->tFirstEvent = time(NULL);
          t2ndEvent = 0;
        }
        else
          t2ndEvent = time(NULL);

        if (t2ndEvent)
        {
          prlCmds->tEventPeriod = t2ndEvent - prlCmds->tFirstEvent;
          dbg(DEBUG,
              "<HandleData> Event period is %d seconds",
              prlCmds->tEventPeriod);
        }
      }
    } /* end VALID */

/* ------------------------------------------------------------------- */
/* ------------------------------------------------------------------- */
/* ------------------------------------------------------------------- */
    /* handle only valid sections */
    if (prlCmds->iValid == iFTP_PART_TIME_INVALID || 
        prlCmds->iValid == iFTP_TRY_AGAIN)
    {
      dbg(TRACE,"<HandleData> SECTION IS %s",sGET_STATE(prlCmds->iValid));
      if (!strcmp(pclSelection, "VALID"))
      {
        /* this is the start signal, received from ntisch */
        /* set flag */
        dbg(DEBUG,"<HandleData> set section %s to valid",
                            sGET_STATE(prlCmds->iValid));
        prlCmds->iValid = iFTP_VALID;

        /* here timsch event is deleted by ntisch itself */
        prlCmds->iTimschNo = -1;

        /* reset error counter... */
        prlCmds->iErrorCnt = 0;
      }
      else
      {
        if (prlCmds->iSendAnswer)
        {
          /* sender answer to sender */
          sprintf(pclAnswerString, "IGNORED,%s", pclTmpSelection);

          if (prlConfig != NULL) prlConfig->iFtpRC = FTP_CMD_IGNORED;	

          (void)SendAnswer(prlCmds->pcCmd,
                           pclAnswerString,
                           prlCmds->pcHomeAirport,
                           prlCmds->pcTableExtension,
                           prlConfig);
        }
      dbg(DEBUG,"<HandleData> ignore this event..");
      }
    }

/* ------------------------------------------------------------------- */
/* ------------------------------------------------------------------- */
/* ------------------------------------------------------------------- */
    if (prlCmds->iValid == iFTP_VALID)
    {
      /* call FTP now */ 
      if ((ilRC = HandleFTP(ilFoundCmd)) != RC_SUCCESS)
      {
        /* set flag */
        switch (ilRC)
        {

/* ------------------------------------------------------------------- */
          case iFTP_FOREVER_INVALID:
            dbg(TRACE,"<HandleData> SETTING SECTION FOREVER INVALID");
            prlCmds->iValid = iFTP_FOREVER_INVALID;

            /* send answer to sender */
            if (prlCmds->iSendAnswer)
            {
              /* sender answer to sender */
              sprintf(pclAnswerString, "FAIL,%s", pclTmpSelection);
              if (prlConfig != NULL) prlConfig->iFtpRC = FTP_FAIL;	

	     (void)SendAnswer(prlCmds->pcCmd,
                              pclAnswerString,
                              prlCmds->pcHomeAirport,
                              prlCmds->pcTableExtension,
                              prlConfig);
            }
            break;

/* ------------------------------------------------------------------- */
          case iFTP_PART_TIME_INVALID:
            dbg(TRACE,"<HandleData> SETTING SECTION PART TIME INVALID");
            /* now send event to timsch */
            prlCmds->iValid = iFTP_PART_TIME_INVALID;

            /* send answer to sender */
            if (prlCmds->iSendAnswer)
            {
              /* send answer to sender */
              sprintf(pclAnswerString, "RETRY,%s", pclTmpSelection);

              if (prlCmds->iSendAnswer) prlConfig->iFtpRC = FTP_RETRY;

              (void)SendAnswer(prlCmds->pcCmd,
                               pclAnswerString,
                               prlCmds->pcHomeAirport,
                               prlCmds->pcTableExtension,
                               prlConfig);
            }

            /* ntisch is programable, so do this */

            prlCmds->iTimschNo = SendEventToTimsch
              (prlCmds->pcCmd, prlCmds->iTimeInvalidOffset, "VALID");

            if (prlCmds->iTimschNo == RC_FAIL)
            {
              /* fatal error, break here */

              dbg(TRACE,"<HandleData> ERROR, ERROR, ERROR");
              dbg(TRACE,"<HandleData> NTISCH RETURNS: %d",ilRC);
              dbg(TRACE,"<HandleData> STOP WORKING NOW...");

              /* send answer to sender */
              if (prlCmds->iSendAnswer)
              {
                /* sender answer to sender */
                sprintf(pclAnswerString, "FAIL,%s", pclTmpSelection);

                if (prlConfig != NULL) prlConfig->iFtpRC = FTP_FAIL;	

                (void)SendAnswer(prlCmds->pcCmd,
                                 pclAnswerString,
                                 prlCmds->pcHomeAirport,
                                 prlCmds->pcTableExtension,
                                 prlConfig);
              }
              exit(0);
            }

            dbg(DEBUG,"<HandleData> TimschEventNo: %d", prlCmds->iTimschNo);
            break;

/* ------------------------------------------------------------------- */
          case iFTP_TRY_AGAIN:
            /* calculate next event */
            if (prlCmds->tEventPeriod > -1)
            {
              flPeriod = ((float)prlCmds->tEventPeriod/60.0)+0.5;
              ilPeriod = (int)flPeriod;
              dbg(DEBUG,"<HandleData> EventPeriod is %d minutes", ilPeriod);
            }
            else
              dbg(DEBUG,"<HandleData> Event priod is not calculated");

            if (prlCmds->iTimeTryAgainOffset < ilPeriod || 
                prlCmds->tEventPeriod == -1)
             {
               dbg(TRACE,"<HandleData> SETTING SECTION TO TRY AGAIN");

               /* set flag */
               prlCmds->iValid = iFTP_TRY_AGAIN;

               /* use ntisch again */
               if ((prlCmds->iTimschNo = SendEventToTimsch
                    (prlCmds->pcCmd, prlCmds->iTimeTryAgainOffset, "VALID")
                    ) == RC_FAIL)
               {
                 /* fatal error, break here */
                 dbg(TRACE,"<HandleData> ERROR, ERROR, ERROR");
                 dbg(TRACE,"<HandleData> NTISCH RETURNS: %d",ilRC);
                 dbg(TRACE,"<HandleData> STOP WORKING NOW...");

                 /* send answer to sender */
                 if (prlCmds->iSendAnswer)
                 {
                   /* sender answer to sender */
                   sprintf(pclAnswerString, "FAIL,%s", pclTmpSelection);

                   if (prlConfig != NULL)
		     prlConfig->iFtpRC = FTP_FAIL;	

                   (void)SendAnswer(prlCmds->pcCmd,
                                    pclAnswerString,
                                    prlCmds->pcHomeAirport,
                                    prlCmds->pcTableExtension,
                                    prlConfig);
                 }
                 exit(0);
               }
               dbg(DEBUG,"<HandleData> TimschEventNo: %d", prlCmds->iTimschNo);
             }
             else
             {
               /* normal period is smaller than configuration period */
               dbg(DEBUG,"<HandleData> FTP_TRY_AGAIN: use normal event");
             }

             /* send answer to sender */
             if (prlCmds->iSendAnswer)
             {
               /* sender answer to sender */
               sprintf(pclAnswerString, "RETRY,%s", pclTmpSelection);

               if (prlConfig != NULL) prlConfig->iFtpRC = FTP_RETRY;	

               (void)SendAnswer(prlCmds->pcCmd,
                                pclAnswerString,
                                prlCmds->pcHomeAirport,
                                prlCmds->pcTableExtension,
                                prlConfig);
             }
             break;

/* ------------------------------------------------------------------- */
          case RC_FAIL:
            /* error sending file to client */
            /* invalid time is null, so stop trying here */
            dbg(TRACE,"<HandleData> received RC_FAIL ... stop trying here");
            dbg(TRACE,"<HandleData> SETTING SECTION TO VALID");
            prlCmds->iValid = iFTP_VALID;

            /* send answer to sender */
            if (prlCmds->iSendAnswer)
            {
              /* sender answer to sender */
              sprintf(pclAnswerString, "FAIL,%s", pclTmpSelection);

              if (prlConfig != NULL) prlConfig->iFtpRC = FTP_FAIL;	

              (void)SendAnswer(prlCmds->pcCmd,
                               pclAnswerString,
                               prlCmds->pcHomeAirport,
                               prlCmds->pcTableExtension,
                               prlConfig);
            }
            break;

/* ------------------------------------------------------------------- */
          default:
            dbg(TRACE,"<HandleData> ERROR, ERROR, ERROR");
            dbg(TRACE,"<HandleData> UNEXPECTED RETURN CODE FROM HandleFTP: %d",ilRC);
            dbg(TRACE,"<HandleData> STOP WORKING NOW...");

            /* send answer to sender */
            if (prlCmds->iSendAnswer)
            {
              /* sender answer to sender */
              sprintf(pclAnswerString, "FAIL,%s", pclTmpSelection);

              if (prlConfig != NULL) prlConfig->iFtpRC = FTP_FAIL;	

              (void)SendAnswer(prlCmds->pcCmd,
                               pclAnswerString,
                               prlCmds->pcHomeAirport,
                               prlCmds->pcTableExtension,
                               prlConfig);
            }
            exit(0);
            break;

        } /* end switch */

/* ------------------------------------------------------------------- */

      }
      else /* ... RC_SUCCESS */
      {
        if(strlen(prlCmds->pcSendCommand))
        {
          ilRC = SendCommand(prlCmds->pcSendCommand,
                             prlCmds->iModID,
                             prlCmds->pcHomeAirport,
                             prlCmds->pcTableExtension);
          if(ilRC != RC_SUCCESS)
          {
            dbg(TRACE,"<HandleData> ERROR SendCommand returns <%d>",ilRC);
          }
        }

        /* send answer to sender */
        if (prlCmds->iSendAnswer)
        {
          /* sender answer to sender */
          if (ilRC == RC_SUCCESS)
          {
            sprintf(pclAnswerString, "RC_SUCCESS,%s", pclTmpSelection);
            if (prlConfig != NULL) prlConfig->iFtpRC = FTP_SUCCESS;	
          }
          else
          {
            sprintf(pclAnswerString, "FAIL,%s", pclTmpSelection);
            if (prlConfig != NULL) prlConfig->iFtpRC = FTP_FAIL;	
          }

          (void)SendAnswer(prlCmds->pcCmd,
                           pclAnswerString,
                           prlCmds->pcHomeAirport,
                           prlCmds->pcTableExtension,
                           prlConfig);
        }
      }
    } /* end ... iFTP_VALID */
    else if (prlCmds->iValid == iFTP_FOREVER_INVALID)
    {
      dbg(TRACE,"<HandleData> SECTION IS FOREVER INVALID");
    }


    /* in case of dynamic configuration, delete memory now... */
    if (prlCmds->iIsDynamic == TRUE)
    {
      dbg(DEBUG,
        "<HandleData> %05d delete dynamic configuration now...", __LINE__);

      --rgTM.iNoOfCmds;

      dbg(DEBUG, "<HandleData> %05d now %d commands in MAIN section",
                                           __LINE__, rgTM.iNoOfCmds);

      if ((rgTM.prCmds = (FTPCmd*)realloc(rgTM.prCmds,
                                          rgTM.iNoOfCmds*sizeof(FTPCmd))
           ) == NULL)
      {
        /* that's impossible... */
        dbg(TRACE,"<HandleData> %05d realloc failure", __LINE__);	
      }
    }
  } /* end of else ... found command in configuration */

  dbg(DEBUG,"<HandleData> ---- END <%s> ----", prlCmdblk->command);

  /* bye bye */
  return RC_SUCCESS;

} /* end of HandleData */
/* ------------------------------------------------------------------- */
/* ---  end if Handle Data ------------------------------------------- */
/* ------------------------------------------------------------------- */



/******************************************************************************/
/* The handle ftp routine                                                     */
/* tide up by eth 26.01.2001                                                  */
/* eth 08.02.01 added ilMgetFlag to decide single or multi file transfare     */
/******************************************************************************/
static int HandleFTP(int ipCmdNo)
{
  int      i;
  int      n;
	int		 ilRc = 0;
  int    ilFlg = 0;
  int    ilRenameSingleFile = TRUE;
  int    ilMgetFlag = 0; /* flag for get multiple files */
  int    ilMputFlag = 0; /* flag for put multiple files */
  int    ilRC[12]; /* need more than one returncode */
  FILE *TmpFilePointer; /* Tem File of put List */
  char	pclNameBuf[iMAX_BUF_SIZE]; /* filename with path */
  char	pclFileName[iMAX_BUF_SIZE]; /* die einzelnen filenames */
  char	pclFileNameForRename[iMAX_BUF_SIZE]; /* die einzelnen filenames */
  char  pclCmdStr[256]; /* system kommando string */
  char	pclTmpBuf1[256];
  char	pclTmpBuf2[256];
  char	pclLocalFile[iMAX_BUF_SIZE];
  char	pclRemoteFileName[2*iMIN_BUF_SIZE];
  char	pclRemoteFilePath[iMAX_BUF_SIZE];
  char	pclLocalFileName[2*iMIN_BUF_SIZE];
  char	pclLocalFilePath[iMAX_BUF_SIZE];
  char	pclPathAndFile[2*iMAX_BUF_SIZE];
  char	pclAlertInfo[iMAX_BUF_SIZE];
  char	pclAlertType[iMIN_BUF_SIZE];

	 *pclAlertType=0x00;
	 if (rgTM.prCmds[ipCmdNo].iSectionType == iSEND)
		strcpy(pclAlertType,"S");
	 if (rgTM.prCmds[ipCmdNo].iSectionType == iRECEIVE)
		strcpy(pclAlertType,"R");
	
/***********************************************************************/
  /* 1.st connect to remote system */
  /* if connect fails, try again */
  dbg(DEBUG,"<HandleFTP> calling CEDA_FTPConnect");
  ilRC[0] = CEDA_FTPConnect(&rgTM.prCmds[ipCmdNo].rInfo);
  dbg(DEBUG,"<HandleFTP> return value ilRC[0] = %d", ilRC[0]);

/***********************************************************************/
  /* 2.nd login */
  if (ilRC[0] == RC_SUCCESS)
  {
    /* if login fails, this is normally an init failure */
    dbg(DEBUG,"<HandleFTP> calling login");
    ilRC[1] = CEDA_FTPLogin(&rgTM.prCmds[ipCmdNo].rInfo);
    dbg(DEBUG,"<HandleFTP> return value ilRC[1] = %d", ilRC[1]);
  }
  else
	{
    ilRC[1] = ilRC[0]; 
		if (strlen(rgTM.prCmds[ipCmdNo].pcAlertName)>0)
		{
			sprintf(pclAlertInfo,"%s HOST:<%s>",FTP_CONNECT_ERR_MSG,rgTM.prCmds[ipCmdNo].rInfo.pcHostName);
			AddAlert(rgTM.prCmds[ipCmdNo].pcAlertName,pclAlertType," ","E",pclAlertInfo,TRUE,TRUE," "," "," ",pcgTwEnd);
		}
	}

/***********************************************************************/
  /* 3.rd representation type (ASCII,BINARAY,...) */
  if (ilRC[1] == RC_SUCCESS)
  {
    if (rgTM.prCmds[ipCmdNo].rInfo.cTransferType != CEDA_ASCII)
    {
      /* if failure -> try again */
      dbg(DEBUG,"<HandleFTP> calling set type");
      ilRC[2] = CEDA_FTPSetType(&rgTM.prCmds[ipCmdNo].rInfo,
                                 rgTM.prCmds[ipCmdNo].rInfo.cTransferType);
      dbg(DEBUG,"<HandleFTP> return value ilRC[2] = %d", ilRC[2]);
    }
    else
    ilRC[2] = ilRC[1];
  }
  else
	{
    ilRC[2] = ilRC[1];
		if (strlen(rgTM.prCmds[ipCmdNo].pcAlertName)>0)
		{
			sprintf(pclAlertInfo,"%s HOST:<%s> USER:<%s>"
				,FTP_LOGIN_ERR_MSG,rgTM.prCmds[ipCmdNo].rInfo.pcHostName,rgTM.prCmds[ipCmdNo].rInfo.pcUser);
			AddAlert(rgTM.prCmds[ipCmdNo].pcAlertName,pclAlertType," ","E",pclAlertInfo,TRUE,TRUE," "," "," ",pcgTwEnd);
		}
	}

/***********************************************************************/
  /* 4.th file structure (FILE,RECORD,PAGE) */
  if (ilRC[2] == RC_SUCCESS)
  {
    if (rgTM.prCmds[ipCmdNo].cStructureCode != CEDA_FILE)
    {
      /* if failure -> try again */
      dbg(DEBUG,"<HandleFTP> calling set file structure");
      ilRC[3] = CEDA_FTPSetFileStructure(&rgTM.prCmds[ipCmdNo].rInfo,
                                          rgTM.prCmds[ipCmdNo].cStructureCode);
      dbg(DEBUG,"<HandleFTP> return value ilRC[3] = %d", ilRC[3]);
    }
    else
      ilRC[3] = ilRC[2];
  }
  else
    ilRC[3] = ilRC[2];

/***********************************************************************/
  /* 5.th transfer mode (STREAM,BLOCK,COMPRESSED) */
  if (ilRC[3] == RC_SUCCESS)
  {
    if (rgTM.prCmds[ipCmdNo].cTransferMode != CEDA_STREAM)
    {
      /* if failure -> try again */
      dbg(DEBUG,"<HandleFTP> calling set transfer mode");
      ilRC[4] = CEDA_FTPSetTransferMode(&rgTM.prCmds[ipCmdNo].rInfo,
                                         rgTM.prCmds[ipCmdNo].cTransferMode);
      dbg(DEBUG,"<HandleFTP> return value ilRC[4] = %d", ilRC[4]);
    }
    else
      ilRC[4] = ilRC[3];
  }
  else
    ilRC[4] = ilRC[3];

/***********************************************************************/
  /* 6. change remote directory */
  if (ilRC[4] == RC_SUCCESS)
  {
    /* use path received from queue, if it exist */
    if (strlen(rgTM.prCmds[ipCmdNo].pcQUERemoteFilePath))
      strcpy(pclRemoteFilePath, rgTM.prCmds[ipCmdNo].pcQUERemoteFilePath);
    else
      strcpy(pclRemoteFilePath, rgTM.prCmds[ipCmdNo].pcRemoteFilePath);

    if (strlen(pclRemoteFilePath))
    {
      dbg(DEBUG,"<HandleFTP> calling cd");
      ilRC[5] = CEDA_FTPcd(&rgTM.prCmds[ipCmdNo].rInfo, pclRemoteFilePath);
      dbg(DEBUG,"<HandleFTP> return value ilRC[5] = %d", ilRC[5]);
    }
    else
      ilRC[5] = ilRC[4];
  }
  else
    ilRC[5] = ilRC[4];

/***********************************************************************/
  /* 7. send or receive a file */
  if (ilRC[5] == RC_SUCCESS)
  {
		/* reset filename for renaming */
		memset(pclFileNameForRename,0x00,iMAX_BUF_SIZE);
    /* check remote filename */
    if (strlen(rgTM.prCmds[ipCmdNo].pcQUERemoteFileName))
      strcpy(pclRemoteFileName, rgTM.prCmds[ipCmdNo].pcQUERemoteFileName);
    else
      strcpy(pclRemoteFileName, rgTM.prCmds[ipCmdNo].pcRemoteFileName);

    /* check local filename */
    if (strlen(rgTM.prCmds[ipCmdNo].pcQUELocalFileName))
      strcpy(pclLocalFileName, rgTM.prCmds[ipCmdNo].pcQUELocalFileName);
    else
      strcpy(pclLocalFileName, rgTM.prCmds[ipCmdNo].pcLocalFileName);

    /* check local path */
    if (strlen(rgTM.prCmds[ipCmdNo].pcQUELocalFilePath))
      strcpy(pclLocalFilePath, rgTM.prCmds[ipCmdNo].pcQUELocalFilePath);
    else
      strcpy(pclLocalFilePath, rgTM.prCmds[ipCmdNo].pcLocalFilePath);

    if (rgTM.prCmds[ipCmdNo].iSectionType == iSEND)
    { /* ---------- iSEND ---------- */

      /* put file(s) */
      dbg(DEBUG,"<HandleFTP> calling put file");
      ilMputFlag = 0; /* erst mal auf 0 setzen */

/* -------------------------------------------------------------- */
/* Baustelle  eth 06.08.01 new function send multiple files */
/* -------------------------------------------------------------- */
      if ( (strstr(pclLocalFileName,"*") != NULL) || 
           (strstr(pclLocalFileName,"?") != NULL) ||
           (strstr(pclLocalFileName,"%") != NULL) )
           /* '%' ist wildcard auf host swissport */
      { /* put multiple file */
        igMultipleSend=TRUE;        
        dbg(DEBUG,"<HandleFTP> found wildcard in local filename");
        ilMputFlag = 1; /* wir haben also multiple files */

/* path and file zusammenbasteln */
        /* char pclPathAndFile[2*iMAX_BUF_SIZE]; */
        sprintf(pclPathAndFile, "%s/%s", pclLocalFilePath, pclLocalFileName);

/* Liste der files erzeugen */
        sprintf(pclCmdStr, "ls -C1 %s > /tmp/ftp_put_list", pclPathAndFile);
        system(pclCmdStr);

/* File oeffnen */
        TmpFilePointer = fopen("/tmp/ftp_put_list", "r");

/* Liste zeilenweise lesen und abarbeiten */
        while( fgets( pclNameBuf, 100, TmpFilePointer) != NULL)
        {
/* leider kommt der pfad beim filename mit ... der muss weg */
/* String-Ende-Zeichen umwandeln ... ausser bei EOF */
          if (pclNameBuf[strlen(pclNameBuf)-1] == '\n')
            pclNameBuf[strlen(pclNameBuf)-1] = '\0';

/* letztes / im pclNameBuf finden */
          dbg(DEBUG,"<HandleFTP> length of %s is %d", pclNameBuf, strlen(pclFileName));
          n = strlen(pclNameBuf)-1;
          while( *(pclNameBuf+n) != '/')
          {
            /* dbg(DEBUG,"<HandleFTP> n = %d ", n); */
            n--;
            if (n<=0) break; /* zur Sicherheit */
          }
          n++;
          strcpy ( pclFileName, pclNameBuf+n);
          dbg(DEBUG,"<HandleFTP> try to send file: >%s<", pclFileName);
          dbg(DEBUG,"<HandleFTP> with the path   : >%s<", pclLocalFilePath);

/* send file */
          ilRC[6] = CEDA_FTPPutFile(&rgTM.prCmds[ipCmdNo].rInfo,
                                    pclLocalFilePath,
                                    pclFileName, 
                                    pclRemoteFileName,
                                    rgTM.prCmds[ipCmdNo].iStoreType,
                                    rgTM.prCmds[ipCmdNo].pcOptSiteCmd);
          dbg(DEBUG,"<HandleFTP> return value ilRC[6] = %d", ilRC[6]);

          if (ilRC[6] == RC_SUCCESS)
          {
            dbg(DEBUG,"<HandleFTP> send file: <%s> successfull", pclFileName);
            /* to inform the status-handler successful PutFile-Fct. call */
            (void)SendStatus("FILEIO", "ftphdl", "WRITE", 1, pclFileName, "", "");

/* local delete */
            if (rgTM.prCmds[ipCmdNo].iDeleteLocalSourceFile == 1)
            {
              dbg(DEBUG,"<HandleFTP> try to delete file: %s", pclFileName);
              /* delete local file */
              sprintf(pclCmdStr, "rm %s/%s", pclLocalFilePath, pclFileName);
              system(pclCmdStr);
            }
						else
						{
/* local rename (each file of a multiple file transfer)*/
							if (strlen(rgTM.prCmds[ipCmdNo].pcRenameLocalFile))
							{
									ilRenameSingleFile = FALSE;
									strcpy(pclFileNameForRename,rgTM.prCmds[ipCmdNo].pcRenameLocalFile);
									ReplaceStdTokens("<HandleFTP>",pclFileNameForRename,pclFileName,NULL,NULL);
									pclTmpBuf1[0] = pclTmpBuf2[0] = 0x00;

									sprintf(pclTmpBuf1, "%s/%s", pclLocalFilePath, pclFileName);

									sprintf(pclTmpBuf2, "%s/%s", pclLocalFilePath,pclFileNameForRename);

									dbg(DEBUG,"<HandleFTP> calling (multiple)-local rename, from <%s> to <%s>",pclTmpBuf1, pclTmpBuf2);

									errno = 0;
									ilRc = rename(pclTmpBuf1, pclTmpBuf2);
									if (ilRc != 0)
										dbg(TRACE,"<HandleFTP> rename()-error <%d>=<%s>!",errno,strerror(errno));
							}
						}
          }
          else
          {
            dbg(DEBUG," HandleFTP: ERROR while transfering file %s", pclFileName);
						if (strlen(rgTM.prCmds[ipCmdNo].pcAlertName)>0)
						{
							sprintf(pclAlertInfo,"%s HOST:<%s> FILE:<%s>",FTP_SEND_ERR_MSG,rgTM.prCmds[ipCmdNo].rInfo.pcHostName,pclFileName);
							AddAlert(rgTM.prCmds[ipCmdNo].pcAlertName,pclAlertType," ","E",pclAlertInfo,TRUE,TRUE," "," "," ",pcgTwEnd);
						}
            break;
          }
        } /* end while */

				/* Datei schliessen und FilePointer freigeben */
        dbg(DEBUG," HandleFTP: close temporary file");
        fclose(TmpFilePointer);

				/* Temporaere Datei loeschen */
        dbg(DEBUG," HandleFTP: delete temporary file");
        sprintf(pclCmdStr, "rm /tmp/ftp_put_list");
        system(pclCmdStr);
		igMultipleSend=FALSE;
      } /* end put multiple file */
      else
      { /* put one file */
        ilRC[6] = CEDA_FTPPutFile(&rgTM.prCmds[ipCmdNo].rInfo,
                                  pclLocalFilePath, 
                                  pclLocalFileName, 
                                  pclRemoteFileName, 
                                  rgTM.prCmds[ipCmdNo].iStoreType,
                                  rgTM.prCmds[ipCmdNo].pcOptSiteCmd);
        dbg(DEBUG,"<HandleFTP> return value ilRC[6] = %d", ilRC[6]);

        if (ilRC[6] == RC_SUCCESS)
        {
          /* to inform the status-handler successful PutFile-Fct. call */
          (void)SendStatus("FILEIO", "ftphdl", "WRITE", 1, pclLocalFileName, "", "");
        }
				else
				{
						if (strlen(rgTM.prCmds[ipCmdNo].pcAlertName)>0)
						{
							sprintf(pclAlertInfo,"%s HOST:<%s> FILE:<%s>",FTP_SEND_ERR_MSG,rgTM.prCmds[ipCmdNo].rInfo.pcHostName,pclLocalFileName);
							AddAlert(rgTM.prCmds[ipCmdNo].pcAlertName,pclAlertType," ","E",pclAlertInfo,TRUE,TRUE," "," "," ",pcgTwEnd);
						}
        }
      } /* end put one file */
    }
    else
    { /* ---------- iRECEIVE ---------- */

      /* get file(s) */
      dbg(TRACE,"<HandleFTP> RemoteFileName: <%s>",pclRemoteFileName);
      ilMgetFlag = 0; /* erst mal auf 0 setzen */

      if ( (strstr(pclRemoteFileName,"*") != NULL) || 
           (strstr(pclRemoteFileName,"?") != NULL) ||
           (strstr(pclRemoteFileName,"%") != NULL) ) /* '%' ist wildcard auf host swissport */
      { /* get multiple file */
        ilMgetFlag = 1; /* wir haben also multiple files */
        dbg(DEBUG," HandleFTP: found wildcard in remote filename");
        dbg(DEBUG," HandleFTP: calling CEDA_FTPMGetFile");
        ilRC[6] = CEDA_FTPMGetFile(&rgTM.prCmds[ipCmdNo].rInfo,
                                   pclRemoteFileName, 
                                   pclLocalFilePath, 
                                   rgTM.prCmds[ipCmdNo].iRemoteFileAppendExtension,
                                   rgTM.prCmds[ipCmdNo].pcRemoteFileAppendExtension,
                                   rgTM.prCmds[ipCmdNo].iDeleteRemoteSourceFile,
                                   rgTM.prCmds[ipCmdNo].iStoreType);
        dbg(DEBUG,"<HandleFTP> return value ilRC[6] = %d", ilRC[6]);

        if (ilRC[6] == RC_SUCCESS)
        {
          /* to inform the status-handler successful GetFile-Fct. call */
          (void)SendStatus("FILEIO", "ftphdl", "READ", 1, pclRemoteFileName, "", "");
        }
				else
				{
					if (strlen(rgTM.prCmds[ipCmdNo].pcAlertName)>0)
					{
						sprintf(pclAlertInfo,"%s HOST:<%s> FILE:<%s>",FTP_RECV_ERR_MSG,rgTM.prCmds[ipCmdNo].rInfo.pcHostName,pclRemoteFileName);
						AddAlert(rgTM.prCmds[ipCmdNo].pcAlertName,pclAlertType," ","E",pclAlertInfo,TRUE,TRUE," "," "," ",pcgTwEnd);
					}
				}
      } /* end get multiple file */
      else
      { /* get one file */
        dbg(DEBUG,"<HandleFTP> calling get file");
        ilRC[6] = CEDA_FTPGetFile(&rgTM.prCmds[ipCmdNo].rInfo,
                                  pclRemoteFileName, 
                                  pclLocalFilePath, 
                                  pclLocalFileName, 
                                  rgTM.prCmds[ipCmdNo].iStoreType);
        dbg(DEBUG,"<HandleFTP> return value ilRC[6] = %d", ilRC[6]);
        if (ilRC[6] == RC_SUCCESS)
        {
          /* to inform the status-handler successful GetFile-Fct. call */
          (void)SendStatus("FILEIO", "ftphdl", "READ", 1, pclRemoteFileName, "", "");
        }
				else
				{
					if (strlen(rgTM.prCmds[ipCmdNo].pcAlertName)>0)
					{
						sprintf(pclAlertInfo,"%s HOST:<%s> FILE:<%s>",FTP_RECV_ERR_MSG,rgTM.prCmds[ipCmdNo].rInfo.pcHostName,pclRemoteFileName);
						AddAlert(rgTM.prCmds[ipCmdNo].pcAlertName,pclAlertType," ","E",pclAlertInfo,TRUE,TRUE," "," "," ",pcgTwEnd);
					}
				}
      } /* end get one file */
    }
  }
  else /* ilRC[5] != RC_SUCCESS */
    ilRC[6] = ilRC[5];

/***********************************************************************/
  /* 8. rename remote file after sending it */	
  if (ilRC[6] == RC_SUCCESS)
  {
    if (strlen(rgTM.prCmds[ipCmdNo].pcRenameRemoteFile))
    {
      dbg(DEBUG,"<HandleFTP> calling remote rename");
      pclTmpBuf1[0] = pclTmpBuf2[0] = 0x00;
      sprintf(pclTmpBuf1, "%s/%s", pclRemoteFilePath,
                                   pclRemoteFileName);
      sprintf(pclTmpBuf2, "%s/%s", pclRemoteFilePath,
                                   rgTM.prCmds[ipCmdNo].pcRenameRemoteFile);
      ilRC[7] = CEDA_FTPRemoteRename(&rgTM.prCmds[ipCmdNo].rInfo,
                                     pclTmpBuf1, pclTmpBuf2);
      if (ilRC[7] != RC_SUCCESS)
			{
					int ilRc;
					dbg(TRACE,"<HandleFTP> renaming remote file failed ilRC[7]=%d try to delete old file <%s>",ilRC[7],pclTmpBuf2);
          ilRc = CEDA_FTPrm(&rgTM.prCmds[ipCmdNo].rInfo, pclTmpBuf2);
      		ilRC[7] = CEDA_FTPRemoteRename(&rgTM.prCmds[ipCmdNo].rInfo,
                                     pclTmpBuf1, pclTmpBuf2);
      		dbg(DEBUG,"<HandleFTP>  (rename remote file, second try) return value ilRC[7] = %d", ilRC[7]);
			}
			else
			{
      dbg(DEBUG,"<HandleFTP> return value ilRC[7] = %d", ilRC[7]);
			}
    }
    else
    {
      ilRC[7] = ilRC[6];
      dbg(DEBUG,"<HandleFTP> return value ilRC[7] = %d", ilRC[7]);
    }
  }
  else
    ilRC[7] = ilRC[6];

/***********************************************************************/
  /* 9. rename local file after sending it */	
  if (ilRC[7] == RC_SUCCESS && ilRenameSingleFile==TRUE)
  {
    if (strlen(rgTM.prCmds[ipCmdNo].pcRenameLocalFile))
    {
			strcpy(pclFileNameForRename,rgTM.prCmds[ipCmdNo].pcRenameLocalFile);
			ReplaceStdTokens("<HandleFTP>",pclFileNameForRename,pclLocalFileName,NULL,NULL);
      pclTmpBuf1[0] = pclTmpBuf2[0] = 0x00;
      sprintf(pclTmpBuf1, "%s/%s", pclLocalFilePath, pclLocalFileName);

      sprintf(pclTmpBuf2, "%s/%s", pclLocalFilePath,pclFileNameForRename);

      dbg(DEBUG,"<HandleFTP> calling (single)-local rename, from <%s> to <%s>",
                                              pclTmpBuf1, pclTmpBuf2);
			errno = 0;
      ilRc = rename(pclTmpBuf1, pclTmpBuf2);
			if (ilRc != 0)
				dbg(TRACE,"<HandleFTP> rename()-error <%d>=<%s>!",errno,strerror(errno));
      ilRC[8] = RC_SUCCESS;
      dbg(DEBUG,"<HandleFTP> return value ilRC[8] = %d", ilRC[8]);
    }
    else
    {
      ilRC[8] = ilRC[7];
      dbg(DEBUG,"<HandleFTP> return value ilRC[8] = %d", ilRC[8]);
    }
  }
  else
    ilRC[8] = ilRC[7];

/***********************************************************************/
  /* 10. must i delete remote or local source file */
  if (ilRC[8] == RC_SUCCESS)
  {
    if (rgTM.prCmds[ipCmdNo].iSectionType == iRECEIVE)
    {
      if (rgTM.prCmds[ipCmdNo].iDeleteRemoteSourceFile)
      {
        if (!ilMgetFlag)
        {
          dbg(DEBUG,"<HandleFTP> calling remote rm"); 
          ilRC[9] = CEDA_FTPrm(&rgTM.prCmds[ipCmdNo].rInfo, pclRemoteFileName);
          dbg(DEBUG,"<HandleFTP> return value ilRC[9] = %d", ilRC[9]);
        }
        else /* multiple files */
        {
          dbg(DEBUG,"<HandleFTP> remote files have been removed by CEDA_FTPMGetFile"); 
          ilRC[9] = RC_SUCCESS;
          dbg(DEBUG,"<HandleFTP> return value ilRC[9] = %d", ilRC[9]);
        }
      }
      else
      {
        ilRC[9] = ilRC[8];
        dbg(DEBUG,"<HandleFTP> return value ilRC[9] = %d", ilRC[9]);
      }
    }
    else /* iSEND */
    {
      if (rgTM.prCmds[ipCmdNo].iDeleteLocalSourceFile)
      {
        if (!ilMgetFlag)
        {
          /* remove one file on local machine */
          dbg(DEBUG,"<HandleFTP> calling local rm"); 

          /* produce filename */
          strcpy(pclLocalFile, pclLocalFilePath);
          if (pclLocalFile[strlen(pclLocalFile)-1] != '/')
          strcat(pclLocalFile, "/");
          strcat(pclLocalFile, pclLocalFileName);

          /* now remove file */
          dbg(DEBUG,"<HandleFTP> local filename is: <%s>", pclLocalFile);
          remove(pclLocalFile);
          ilRC[9] = ilRC[8];
          dbg(DEBUG,"<HandleFTP> return value ilRC[9] = %d", ilRC[9]);
        }
        else /* multiple files */
        {
          dbg(DEBUG,"<HandleFTP> local files have been removed before"); 
          ilRC[9] = RC_SUCCESS;
          dbg(DEBUG,"<HandleFTP> return value ilRC[9] = %d", ilRC[9]);
        }
      }
      else
      {
        ilRC[9] = ilRC[8];
        dbg(DEBUG,"<HandleFTP> return value ilRC[9] = %d", ilRC[9]);
      }
    }
  }
  else
    ilRC[9] = ilRC[8];

/***********************************************************************/
  /* must i write QUIT ftp ftp-server? */
  dbg(DEBUG,"<HandleFTP> must i write QUIT ftp ftp-server ?"); 
  for (i=0, ilFlg=RC_SUCCESS ; i<10 && ilFlg == RC_SUCCESS ; i++)
  {
    dbg(DEBUG,"<HandleFTP>  i=%d  ilFlg=%d  ilRC[%d]=%d", i, ilFlg, i, ilRC[i]);
    if (ilRC[i] != RC_SUCCESS)
    {
      ilFlg = ilRC[i];
    }
  }

  /* ilFlg = ilRC[9]; */

/***********************************************************************/
  /* 11. close connection */
  dbg(DEBUG,"<HandleFTP> calling close connection");
  dbg(DEBUG,"<HandleFTP> ilFlg = %d ", ilFlg);
  ilRC[10] = CEDA_FTPBye(&rgTM.prCmds[ipCmdNo].rInfo, ilFlg);
  dbg(DEBUG,"<HandleFTP> return value ilRC[10] = %d", ilRC[10]);

/***********************************************************************/
  /* bye bye */
  for (i=0; i<11; i++)
    if (ilRC[i] == iFTP_FOREVER_INVALID)
      return iFTP_FOREVER_INVALID;

  for (i=0; i<11; i++)
    if (ilRC[i] == iFTP_TRY_AGAIN)
    {
      /* increment error counter */
      if (rgTM.prCmds[ipCmdNo].iErrorCnt++ >=
          rgTM.prCmds[ipCmdNo].iRetryCounter)
      {
        rgTM.prCmds[ipCmdNo].iErrorCnt = 0;
        if (rgTM.prCmds[ipCmdNo].iTimeInvalidOffset > 0)
          return iFTP_PART_TIME_INVALID;
        else
          return RC_FAIL;
      }
      dbg(DEBUG,"<HandleFTP> ErrorCnt: %d", rgTM.prCmds[ipCmdNo].iErrorCnt);
      return iFTP_TRY_AGAIN;
    }

  /* reset error counter */
  rgTM.prCmds[ipCmdNo].iErrorCnt = 0;

  return RC_SUCCESS;

} /* end of HandleFTP */



/******************************************************************************/
/* the DeleteTimschEvent routine                                              */
/******************************************************************************/
static int DeleteTimschEvent(int ipTimId)
{ 
	return Timsch(DEL, ipTimId, mod_id, mod_id, NULL, 0, NULL); 
}

/******************************************************************************/
/* the SendEventToQue routine                                                 */
/******************************************************************************/
static int SendEventToTimsch(char *pcpCommand, int ipTime, char *pcpString)
{
	int			ilRC = RC_SUCCESS;
	int			ilLen;
	EVENT			*prlOutEvent 	= NULL;
	BC_HEAD		*prlOutBCHead 	= NULL;
	CMDBLK		*prlOutCmdblk	= NULL;
	PERIOD		rlPeriod;

	dbg(DEBUG,"<SendEventToTimsch> ----- START -----");

	/* calculate size of memory we need */
	ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + 
			  sizeof(CMDBLK) + strlen(pcpString) + 32;

	/* get memory for out event */
	if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
	{
		dbg(TRACE,"<SendEventToTimsch> %05d malloc failed, can't allocate %d bytes", __LINE__, ilLen);
		dbg(DEBUG,"<SendEventToTimsch> ----- END -----");
		return RC_FAIL;
	}

	/* clear buffer */
	memset((void*)prlOutEvent, 0x00, ilLen);

	/* set structure members */
	prlOutEvent->type 		 = SYS_EVENT;
	prlOutEvent->command 	 = EVENT_DATA;
	prlOutEvent->originator  = mod_id;
	prlOutEvent->retry_count = 0;
	prlOutEvent->data_offset = sizeof(EVENT);
	prlOutEvent->data_length = ilLen - sizeof(EVENT);

	/* BCHead members */
	prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
	prlOutBCHead->rc = RC_SUCCESS;
	strcpy(prlOutBCHead->dest_name, "FTPHDL");
	strcpy(prlOutBCHead->recv_name, "EXCO");

	/* CMDBLK members */
	prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
	strcpy(prlOutCmdblk->command, pcpCommand );

	/* my string */
	strcpy(prlOutCmdblk->data, pcpString);

	/* clear buffer */
	memset(&rlPeriod,0x00,sizeof(PERIOD));
	rlPeriod.day_o_week = -1;
	rlPeriod.hour_o_day = -1;
	rlPeriod.min_o_hour = ipTime;
	rlPeriod.n_times    = 1;
	rlPeriod.period     = iREL;

	/* now call timsch */
	if ((ilRC = Timsch(ADD, 0, mod_id, mod_id, &rlPeriod, ilLen, (char*)prlOutEvent)) <= 0)
	{
		ilRC = RC_FAIL;
	}

	/* delete memory */
	free((void*)prlOutEvent);

	dbg(DEBUG,"<SendEventToTimsch> ----- END -----");

	/* bye bye */
	return ilRC;
}

/******************************************************************************/
/* the GetCmdNo routine                                                       */
/******************************************************************************/
static int GetCmdNo(char *pcpCmd)
{
	int		ilFoundCmd;
	int		ilCurCmd;

	/* search command received by queue */
	for (ilCurCmd = 0, ilFoundCmd = -1; ilCurCmd<rgTM.iNoOfCmds && ilFoundCmd == -1; ilCurCmd++)
	{
		if (!strcmp(pcpCmd, rgTM.prCmds[ilCurCmd].pcCmd))
			ilFoundCmd = ilCurCmd;
	}
	return ilFoundCmd;
}

/******************************************************************************/
/* the SendAnswer routine                                                     */
/******************************************************************************/
static int SendAnswer(char *pcpCmd, char *pcpAnswer, 
		      char *pcpHomeAP, char *pcpTABEnd, FTPConfig *prpConfig)
{
	int	ilRC;
	int	ilModID;
	char	pclData[iMIN];
	BC_HEAD	rlBCHead;
	CMDBLK	rlCmdblk;

	/* sender answer to sender */
	if (prgEvent->originator != igTimschID)
	{
		dbg(DEBUG,"<SendAnswer> sending answer (%s)", pcpAnswer);
		/* clear buffer */
		memset((void*)&rlBCHead, 0x00, sizeof(BC_HEAD));
		rlBCHead.rc = NETOUT_NO_ACK;
		strncpy(rlBCHead.dest_name, "FTPHDL", 10);
		strncpy(rlBCHead.recv_name, "EXCO", 10);

		memset((void*)&rlCmdblk, 0x00, sizeof(CMDBLK));
		strncpy(rlCmdblk.command, pcpCmd, 6);
		sprintf(rlCmdblk.tw_start,"%s",pcgTwStart);
		dbg(DEBUG,"<SendAnswer> tw_start: <%s>", rlCmdblk.tw_start);
        sprintf(rlCmdblk.tw_end, "%s,%s,FTPHDL", pcpHomeAP, pcpTABEnd);
		dbg(DEBUG,"<SendAnswer> tw_end: <%s>", rlCmdblk.tw_end);

		if (prpConfig != NULL)
		{
			strcpy(pclData, "DYN");
			if (prpConfig->iModID != 0)
				ilModID = prpConfig->iModID;
			else
				ilModID = prgEvent->originator;
		}
		else
		{
			pclData[0] = 0x00;
			ilModID = prgEvent->originator;
		}

		if ((ilRC = SendToQue(ilModID, PRIORITY_3, &rlBCHead, &rlCmdblk, pcpAnswer, "", pclData, prpConfig, sizeof(FTPConfig))) != RC_SUCCESS)
		{
			dbg(TRACE,"<SendAnswer> SendToQue returns: %d", ilRC);
		}
	}
	else
	{
		dbg(DEBUG,"<SendAnswer> is ntisch, don't send answer (%s)", pcpAnswer);
		ilRC = RC_SUCCESS;
	}

	/* bye */
	return ilRC;
}

/******************************************************************************/
/* the SendToQue routine                                                      */
/******************************************************************************/
static int SendToQue(int ipModID, int ipPrio, BC_HEAD *prpBCHead, 
		     CMDBLK *prpCmdblk, char *pcpSelection, 
		     char *pcpFields, char *pcpData, 
		     FTPConfig *prpConfig, size_t ipConfigLen)
{
	int	 ilRC;
	int	 ilLen;
	EVENT	*prlOutEvent 	= NULL;
	BC_HEAD	*prlOutBCHead 	= NULL;
	CMDBLK	*prlOutCmdblk	= NULL;

	dbg(DEBUG,"<SendToQue> ----- START -----");

	/* calculate size of memory we need */
	if (prpConfig != NULL)
	{
		ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + ipConfigLen + 32; 
	}
	else
	{
		ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + 32; 
	}

	/* get memory for out event */
	if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
	{
		dbg(TRACE,"<SendToQue> %05d malloc failed, can't allocate %d bytes", __LINE__, ilLen);
		dbg(DEBUG,"<SendToQue> ----- END -----");
		return RC_FAIL;
	}

	/* clear buffer */
	memset((void*)prlOutEvent, 0x00, ilLen);

	/* set structure members */
	prlOutEvent->type 	 = SYS_EVENT;
	prlOutEvent->command 	 = EVENT_DATA;
	prlOutEvent->originator  = mod_id;
	prlOutEvent->retry_count = 0;
	prlOutEvent->data_offset = sizeof(EVENT);
	prlOutEvent->data_length = ilLen - sizeof(EVENT);

	/* BCHead members */
	prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
	memcpy(prlOutBCHead, prpBCHead, sizeof(BC_HEAD));

	/* CMDBLK members */
	prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
	memcpy(prlOutCmdblk, prpCmdblk, sizeof(CMDBLK));

	/* Selection */
	strcpy(prlOutCmdblk->data, pcpSelection);

	/* fields */
	strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1, pcpFields);

	/* data */
	strcpy(prlOutCmdblk->data+strlen(pcpSelection)+strlen(pcpFields)+2, pcpData);

	/* structure */
	if (prpConfig != NULL)
	{
		dbg(DEBUG,"<SendToQue> now moving Config-Structure...");
		DebugPrintFTPConfig(DEBUG, prpConfig);
		memcpy((void*)(prlOutCmdblk->data+strlen(pcpSelection)+strlen(pcpFields)+strlen(pcpData)+3), (void*)prpConfig, ipConfigLen);
	}

	/* send this message */
	dbg(DEBUG,"<SendToQue> sending to Mod-ID: %d", ipModID);
	if ((ilRC = que(QUE_PUT, ipModID, mod_id, ipPrio, ilLen, (char*)prlOutEvent)) != RC_SUCCESS)
	{
		/* delete memory */
		free((void*)prlOutEvent);

		dbg(TRACE,"<SendToQue> %05d QUE_PUT returns: %d", __LINE__, ilRC);
		dbg(DEBUG,"<SendToQue> ----- END -----");
		return RC_FAIL;
	}

	/* delete memory */
	free((void*)prlOutEvent);

	dbg(DEBUG,"<SendToQue> ----- END -----");

	/* bye bye */
	return RC_SUCCESS;
}

/******************************************************************************/
/* the SendCommand routine                                                    */
/******************************************************************************/
static int SendCommand(char *pcpCmd, int pcpModID, 
                       char *pcpHomeAP, char *pcpTABEnd)
{
  char   *pclFct = "SendCommand";
  int      ilRC;
  BC_HEAD  rlBCHead;
  CMDBLK   rlCmdblk;

  /* send answer to process */
  dbg(DEBUG,"<%s> sending CMD <%s> to ModID (%d)", pclFct, pcpCmd,pcpModID);
  /* clear buffer */
  memset((void*)&rlBCHead, 0x00, sizeof(BC_HEAD));
  rlBCHead.rc = NETOUT_NO_ACK;
  strncpy(rlBCHead.dest_name, "FTPHDL", 10);
  strncpy(rlBCHead.recv_name, "EXCO", 10);

  memset((void*)&rlCmdblk, 0x00, sizeof(CMDBLK));
  strncpy(rlCmdblk.command, pcpCmd, 6);
  sprintf(rlCmdblk.tw_end, "%s,%s,FTPHDL", pcpHomeAP, pcpTABEnd);
  dbg(DEBUG,"<%s> tw_end: <%s>", pclFct, rlCmdblk.tw_end);

  if ((ilRC = SendToQue(pcpModID, PRIORITY_3, &rlBCHead, &rlCmdblk,"","","", NULL,0)) != RC_SUCCESS)
  {
    dbg(TRACE,"<%s> SendToQue returns: %d", pclFct, ilRC);
  }

  /* bye */
  return ilRC;

} /* end of SendCommand */



/******************************************************************************/
/* the AddDynamicConfig routine                                               */
/******************************************************************************/
static int AddDynamicConfig(FTPConfig *prpCfg)
{
  int	ilRC;
  int	ilCurCmd;
  char   *pclS = NULL;
  char	pclPasswd[64];
	FTPConfig rlCfg;

  dbg(DEBUG,"<AddDynamicConfig> ----- START -----");

	/* copy the configuration struct to avoid alignment problems */
	memcpy(&rlCfg,prpCfg,sizeof(rlCfg));

  /* print received configuration structure... */
  DebugPrintFTPConfig(DEBUG, prpCfg);

  rgTM.iNoOfCmds++;
  dbg(DEBUG,"<AddDynamicConfig> %05d now %d commands in MAIN section",
      __LINE__,rgTM.iNoOfCmds);

  /* get memory */
  if ((rgTM.prCmds=(FTPCmd*)realloc(rgTM.prCmds, rgTM.iNoOfCmds*sizeof(FTPCmd)))==NULL)
  {
    /* nix speicher, nix schaffe */
    dbg(TRACE,"<AddDynamicConfig> %05d ERROR: malloc failure...", __LINE__);
    dbg(DEBUG,"<AddDynamicConfig> ---- END ----");
    return RC_FAIL;
  }

  /* set current command-number ... */
  ilCurCmd = rgTM.iNoOfCmds - 1;

  /* 20040312 JIM: not all settings (i.e. .pcOptSiteCmd) are comming via queue, 
     so init struct 
  */
  memset(&rgTM.prCmds[ilCurCmd],0x00,sizeof(FTPCmd)); 

  /* get 'real' passwd */
  /* dbg(DEBUG,"<AddDynamicConfig> Passwd encrypted 1: <%s>", prpCfg->pcPasswd); */
  /* by eth 3.2001 */
  dbg(DEBUG,"<AddDynamicConfig> Passwd encrypted 1: <???>");

  strcpy(pclPasswd, prpCfg->pcPasswd);
  /* dbg(DEBUG,"<AddDynamicConfig> Passwd encrypted 2: <%s>", pclPasswd); */
  /* by eth 3.2001 */
  dbg(DEBUG,"<AddDynamicConfig> Passwd encrypted 2: <???>");

  pclS = pclPasswd;
  CDecode(&pclS);
  strcpy(pclPasswd, pclS);
  /* dbg(DEBUG,"<AddDynamicConfig> 'real' Passwd is: <%s>", pclPasswd); */
  /* by eth 3.2001 */
  dbg(DEBUG,"<AddDynamicConfig> 'real' Passwd is: <?>");

  if ((ilRC = CEDA_FTPInit(&rgTM.prCmds[ilCurCmd].rInfo,
                            prpCfg->pcHostName,
                            prpCfg->pcUser,
                            pclPasswd,
                            prpCfg->cTransferType,
                            prpCfg->iTimeout,
                            prpCfg->lGetFileTimer,
                            prpCfg->lReceiveTimer,
                            prpCfg->iDebugLevel,
                            prpCfg->iServerOS, 
                            prpCfg->iClientOS)) != RC_SUCCESS)
  {
    /* delete memory here */
    dbg(TRACE,"<AddDynamicConfig> %05d CEDA_FTPInit returns NOT RC_SUCCESS: %d",
         __LINE__, ilRC);

    --rgTM.iNoOfCmds;
    dbg(DEBUG,"<AddDynamicConfig> %05d now %d commands in MAIN section",
         __LINE__, rgTM.iNoOfCmds);

    if ((rgTM.prCmds=(FTPCmd*)realloc(rgTM.prCmds, rgTM.iNoOfCmds*sizeof(FTPCmd)))==NULL)
    {
      /* that's impossible... */
      dbg(TRACE,"<AddDynamicConfig> %05d realloc failure", __LINE__);	
      dbg(DEBUG,"<AddDynamicConfig> ---- END ----");
    }
    return RC_FAIL;
  }

  /* set some flags */
  rgTM.prCmds[ilCurCmd].iValid       = iFTP_VALID;
  rgTM.prCmds[ilCurCmd].iErrorCnt    = 0;
  rgTM.prCmds[ilCurCmd].tFirstEvent  = -1;
  rgTM.prCmds[ilCurCmd].tEventPeriod = -1;
  rgTM.prCmds[ilCurCmd].iTimschNo    = -1;
  rgTM.prCmds[ilCurCmd].iIsDynamic   = TRUE;

  /* and all other members */
  rgTM.prCmds[ilCurCmd].iRetryCounter           = prpCfg->iRetryCounter;
  rgTM.prCmds[ilCurCmd].iSectionType            = prpCfg->iSectionType;
  rgTM.prCmds[ilCurCmd].iTimeTryAgainOffset     = prpCfg->iTryAgainOffset;
  rgTM.prCmds[ilCurCmd].iTimeInvalidOffset      = prpCfg->iInvalidOffset;
  rgTM.prCmds[ilCurCmd].iSendAnswer             = prpCfg->iSendAnswer;
  rgTM.prCmds[ilCurCmd].iStoreType              = prpCfg->iStoreType;
  rgTM.prCmds[ilCurCmd].cStructureCode          = prpCfg->cStructureCode;
  rgTM.prCmds[ilCurCmd].cTransferMode           = prpCfg->cTransferMode;
  rgTM.prCmds[ilCurCmd].iDeleteLocalSourceFile  = prpCfg->iDeleteLocalSourceFile;
  rgTM.prCmds[ilCurCmd].iDeleteRemoteSourceFile = prpCfg->iDeleteRemoteSourceFile;

  strcpy(rgTM.prCmds[ilCurCmd].pcTableExtension,   prpCfg->pcTableExtension);
  strcpy(rgTM.prCmds[ilCurCmd].pcHomeAirport,      prpCfg->pcHomeAirport);
  strcpy(rgTM.prCmds[ilCurCmd].pcRemoteFileName,   prpCfg->pcRemoteFileName);
  strcpy(rgTM.prCmds[ilCurCmd].pcRemoteFilePath,   prpCfg->pcRemoteFilePath);
  strcpy(rgTM.prCmds[ilCurCmd].pcLocalFileName,    prpCfg->pcLocalFileName);
  strcpy(rgTM.prCmds[ilCurCmd].pcLocalFilePath,    prpCfg->pcLocalFilePath);
  strcpy(rgTM.prCmds[ilCurCmd].pcRenameLocalFile,  prpCfg->pcRenameLocalFile);
  strcpy(rgTM.prCmds[ilCurCmd].pcRenameRemoteFile, prpCfg->pcRenameRemoteFile);
  strcpy(rgTM.prCmds[ilCurCmd].pcCmd,              prpCfg->pcCmd);

  dbg(DEBUG,"<AddDynamicConfig> ----- END -----");

  return RC_SUCCESS;

} /* end of  AddDynamicConfig */




/******************************************************************************/
/* password ausgabe per kommando */
/******************************************************************************/
static int passout(void)
{
  int  i; /* counter */
  int  ilRC = RC_SUCCESS;
  char *pclFct = "passout";

  char  *pclCfgPath = NULL;
  char  pclCfgFile[iMIN_BUF_SIZE];
 
  char  pclAllCommands[2*iMAX_BUF_SIZE];
  char  Command[8];
 
  int ilCmdCnt; /* Command counter "Anzahl" */
  int ilCurCmd; /* Command number "Hausnummer" */

  char  pclTmpBuf[iMAX_BUF_SIZE];
  char  pclUser[iMIN_BUF_SIZE];
  char *pclS = NULL;
  char  pclPasswd[64];
  char  pclHostName[iMIN_BUF_SIZE];



  dbg(DEBUG,"%s ---- START ----", pclFct);

  /* get the config path */
  if ((pclCfgPath = getenv("CFG_PATH")) == NULL)
  {
    dbg(DEBUG,"%s ERROR: missing environment CFG_PATH ...", pclFct);
    dbg(DEBUG,"%s ---- END ----\n\n", pclFct);
    return RC_FAIL;
  }
 
  strcpy(pclCfgFile, pclCfgPath);
 
  if (pclCfgFile[strlen(pclCfgFile)-1] != '/')
    strcat(pclCfgFile, "/");
 
  strcat(pclCfgFile, mod_name);
  strcat(pclCfgFile, ".cfg");
  dbg(DEBUG,"%s use cfg-File: %s", pclFct, pclCfgFile);


/* -------------------------------------------------------------- */
/* scan all commands -------------------------------------------- */
/* -------------------------------------------------------------- */
  memset((void*)pclAllCommands, 0x00, 2*iMAX_BUF_SIZE);
  if ((ilRC = iGetConfigEntry(pclCfgFile,
                              "MAIN",
                              "commands",
                              CFG_STRING,
                              pclAllCommands))
            != RC_SUCCESS)
  {
    dbg(DEBUG,"%s missing commands in section MAIN ...", pclFct);
    dbg(DEBUG,"%s ---- END ----\n\n", pclFct);
    return RC_FAIL;
  }
  else
  {
    StringUPR((UCHAR*)pclAllCommands);

    if ((ilCmdCnt = GetNoOfElements(pclAllCommands, cCOMMA)) <= 0)
    {
      dbg(DEBUG,"%s no commands defined ...", pclFct);
      dbg(DEBUG,"%s ---- END ----\n\n", pclFct);
      return RC_FAIL;
    }
    else
    {
      dbg(DEBUG,"%s found %d commands\n", pclFct, ilCmdCnt);
 
      /* read all commands and corresponding data */
      for (ilCurCmd = 0; ilCurCmd < ilCmdCnt ; ilCurCmd++)
      {
        strcpy(Command, GetDataField(pclAllCommands, ilCurCmd, cCOMMA));
        dbg(DEBUG,"%s Section : %s", pclFct, Command);

/******************************************************************************/
        /* -------------------------------------------------------------- */
        /* section_type ------------------------------------------------- */
        /* -------------------------------------------------------------- */
        pclTmpBuf[0] = 0x00;
        if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    Command,
                                    "section_type",
                                    CFG_STRING,
                                    pclTmpBuf))
                  != RC_SUCCESS)
        {
          /* default */
          dbg(DEBUG,"%s ERROR: missing section type in section %s", pclFct, Command);
          dbg(DEBUG,"%s ---- END ----\n\n", pclFct);
          return RC_FAIL;
        }
 
        StringUPR((UCHAR*)pclTmpBuf);
        if (!strcmp(pclTmpBuf,"SEND"))
          dbg(DEBUG,"%s type    :   SEND", pclFct);
        else if (!strcmp(pclTmpBuf,"RECEIVE"))
          dbg(DEBUG,"%s type    :   RECEIVE", pclFct);
        else
          dbg(DEBUG,"%s ERROR: unknown type in section: %s", pclFct, Command);
 
/******************************************************************************/
        /* -------------------------------------------------------------- */
        /* remote_machine ----------------------------------------------- */
        /* -------------------------------------------------------------- */
        pclHostName[0] = 0x00;
        if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    Command,
                                    "remote_machine",
                                    CFG_STRING,
                                    pclHostName))
                  != RC_SUCCESS)
        {
          /* cannot work without ftp user... */
          dbg(DEBUG,"%s ERROR: missing machine in section <%s>", pclFct, Command);
          dbg(DEBUG,"%s ---- END ----\n\n", pclFct);
          return RC_FAIL;
        }
        dbg(DEBUG,"%s machine :     %s", pclFct, pclHostName);

/******************************************************************************/
        /* -------------------------------------------------------------- */
        /* ftp_user ----------------------------------------------------- */
        /* -------------------------------------------------------------- */
        pclUser[0] = 0x00;
        if ((ilRC = iGetConfigEntry(pclCfgFile,
                                    Command,
                                    "ftp_user",
                                    CFG_STRING,
                                    pclUser))
                  != RC_SUCCESS)
        {
          /* cannot work without ftp user... */
          dbg(TRACE,"%s ERROR: missing ftp-user in section: %s", pclFct, Command);
          dbg(DEBUG,"%s ---- END ----\n\n", pclFct);
          return RC_FAIL;
        }
        dbg(DEBUG,"%s ftp-user:       %s", pclFct, pclUser);

/******************************************************************************/
/*******************************************************************************
 *
 * by eth 26.10.2001
 *
 * now use new function iGetConfigLine
 * to get full line from config file
 */
        pclTmpBuf[0] = 0x00;
        if ((ilRC = iGetConfigLine(pclCfgFile,
                                   rgTM.prCmds[ilCurCmd].pcCmd,
                                   "ftp_pass",
                                   pclTmpBuf))
                  != RC_SUCCESS)
        {
          /* default */
          dbg(TRACE,"<passout> missing ftp_pass in section <%s>",
                                       rgTM.prCmds[ilCurCmd].pcCmd);
          return RC_FAIL;
        }
 
        /* dbg(DEBUG,"<passout> read ftp_pass line:\n  <%s>", pclTmpBuf); */
 
  /* bis "=" suchen */
        i=0;
        /* while((pclTmpBuf[i] != '=') || (i<= strlen(pclTmpBuf))) */
        while(pclTmpBuf[i] != '=')
        { i++; }

  /* "=" ueberspringen */
        i++;
 
  /* bis zum ersten zeichen das nicht blank oder tab ist suchen */
        while((pclTmpBuf[i] == ' ') || ( pclTmpBuf[i] == '\t'))
        { i++; }
 
  /* den rest speichern */
        strcpy(pclPasswd,&pclTmpBuf[i]);
 
  /* das \n muss noch weg */
        i=0;
        while(pclPasswd[i] != '\n')
        { i++; }
 
  /* \n gegen \0 tauschen */
        pclPasswd[i] = '\0';
 
  /* encrypt password */
        pclS = pclPasswd;
        CDecode(&pclS);
        strcpy(pclPasswd, pclS);
 
        dbg(DEBUG,"%s password:         %s\n", pclFct, pclPasswd);
        /* by eth 26.10.2001 */

/* end ************************************************************************/

      } /* end for ... all commands and corresponding data ... */
    }
  } /* end of ... section MAIN */

  dbg(DEBUG,"%s ---- END ----", pclFct);

  return ilRC;

} /* end of passout */

/******************************************************************************/
/* The ReplaceStdTokens                                                      */
/* replaces configured keywords inside a filename,header,prefix,postfix or footer line*/
/* with dynamically calculated values like linecount,numbers,dates,times,etc.*/
/******************************************************************************/
static void ReplaceStdTokens(char *pcpCalledBy, char *pcpLine,char *pcpDynData,char *pcpDateFormat,char *pcpTimeFormat)
{
	int ilRc = RC_SUCCESS;
	int ilBreak = FALSE;
	char pclToken[iMAX_BUF_SIZE];
	char pclDynData[iMAX_BUF_SIZE];
	char pclTmpBuf[iMAX_BUF_SIZE];
	char pclTmpBuf2[iMAX_BUF_SIZE];

	do 
	{
		memset(pclDynData,0x00,iMAX_BUF_SIZE);
		memset(pclToken,0x00,iMAX_BUF_SIZE);
		memset(pclTmpBuf,0x00,iMAX_BUF_SIZE);
		memset(pclTmpBuf2,0x00,iMAX_BUF_SIZE);

		if (strstr(pcpLine,"#FILENAME")!=NULL && pcpDynData!=NULL)
		{
				strncpy(pclDynData,pcpDynData,iMAX_BUF_SIZE);
				strcpy(pclToken,"#FILENAME");
		}

	#if 0 /* for later possible usage */
		else if (strstr(pcpLine,"#DATE_LOC")!=NULL && pcpDateFormat!=NULL)
		{
			strcpy(pclToken,"#DATE_LOC");
			GetServerTimeStamp("LOC",1,0,pclTmpBuf);
			FormatDate(pclTmpBuf,pcpDateFormat,pclTmpBuf2);
			strcpy(pclDynData,pclTmpBuf2);
		}
		else if (strstr(pcpLine,"#TIME_LOC")!=NULL && pcpTimeFormat!=NULL)
		{
			strcpy(pclToken,"#TIME_LOC");
			GetServerTimeStamp("LOC",1,0,pclTmpBuf);
			FormatDate(pclTmpBuf,pcpTimeFormat,pclTmpBuf2);
			strcpy(pclDynData,pclTmpBuf2);
		}
		else if (strstr(pcpLine,"#DATE_UTC")!=NULL && pcpDateFormat!=NULL)
		{
			strcpy(pclToken,"#DATE_UTC");
			GetServerTimeStamp("UTC",1,0,pclTmpBuf);
			FormatDate(pclTmpBuf,pcpDateFormat,pclTmpBuf2);
			strcpy(pclDynData,pclTmpBuf2);
		}
		else if (strstr(pcpLine,"#TIME_UTC")!=NULL && pcpTimeFormat != NULL)
		{
			strcpy(pclToken,"#TIME_UTC");
			GetServerTimeStamp("UTC",1,0,pclTmpBuf);
			FormatDate(pclTmpBuf,pcpTimeFormat,pclTmpBuf2);
			strcpy(pclDynData,pclTmpBuf2);
		}
	#endif

		if (strlen(pclToken)>0 && strlen(pclDynData)>0)
		{
			dbg(DEBUG,"%s replace <%s> with <%s>",pcpCalledBy,pclToken,pclDynData);
			while((ilRc = SearchStringAndReplace(pcpLine,pclToken,pclDynData)) != RC_SUCCESS)
			{
				ilRc = RC_FAIL;
				ilBreak=TRUE;
			}
		}
		else
		{
			/*dbg(DEBUG,"%s either the token or the dynamic data are empty! Exit func.!",pcpCalledBy);*/
			ilBreak=TRUE;
		}
	}while (strlen(pclToken)>0 && ilBreak==FALSE);
}

/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/
