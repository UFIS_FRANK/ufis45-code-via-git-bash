#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/htmhdl.c 1.5 2011/01/14 19:12:07SGT gfo Exp  $";
#endif /* _DEF_mks_version */
 
/* ******************************************************************** */
/* CEDA MAIN program HTMLHDL						*/
/*									*/
/* Author		: Joern Weerts 					*/
/* Date			: 19/12/97					*/
/* Description		: reads data from the database, preconverts them to   */
/*                  to HTML-format, saves them into a file and sends    */
/*                  this file to a remote host.                         */
/*									*/
/* Update history   : jwe 19 December 1997                          	*/
/*                  created skeleton for htmlhdl.c                      */
/*									*/
/*                  jwe 15 January 1999                                 */
/*                  updated HTMHDL for TABEND compatibilty              */
/*									*/
/*                  mos	05 Jan 2000	                                */
/*                  updated HTMHDL for use of FTPHDL                    */
/*			changes in					*/
/*				htmhdl.h				*/
/*				htmhdl.cfg				*/
/*				global variables section		*/
/*				prototypes section			*/
/*				function ReadSectionsCfg		*/
/*				function TransmitFile			*/
/*				function SendEvent			*/
/*				function FreeDynCfgMem			*/
/*                   		                                        */
/*                  	                                                */
/*		     mos 18 Jan 2000					*/
/*		     implemented check-in data processing		*/
/*		     The values for the check in counters will be 	*/
/*		     evaluated according to a flights nature		*/
/*			11: MAIN & CSF flights common &ded. counters	*/
/*			12: domestic deps: counter 61-65		*/
/*			13: MAIN: see 11, CSF: ded.			*/
/*			14: MAIN: ded.,   CSF: common			*/
/*		     The fileif process is now configured to select	*/
/*		     the urno for each section. This value is ignored by*/
/*		     the htmhdl (it's used internally, though)		*/
/*			changes in					*/
/*				htmhdl.h				*/
/*				htmhdl.cfg				*/
/*				prototypes section			*/
/*				function ReadSectionsCfg		*/
/*				function CreateTableData		*/
/*				function CHECKINProcessData		*/
/*				function CHECKINProcessNature		*/
/*				function CHECKINProcessNature12		*/
/*				function CHECKINCalculateCounter	*/
/*				function FreeDynCfgMem			*/
/*                  	                                                */
/*		     mos 27 Jan 2000					*/
/*		     implemented additional row processing		*/
/*			changes in					*/
/*				htmhdl.h				*/
/*				htmhdl.cfg				*/
/*				prototypes section			*/
/*				function ReadSectionsCfg		*/
/*				function CreateTableData		*/
/*				function ROWCreate			*/
/*                  	                                                */
/*		     mos 05 Feb 2000					*/
/*		     implemented parking and checking page creation	*/
/*			changes in					*/
/*                  	         function TOOLSort                      */
/*                  	         function PARKINGProcessPage            */
/*                  	         function UtcToLocal                    */
/*                  	         function CHECKINProcessPage            */
/*                  	                                                */
/*                  	                                                */
/*		     mos 06 Apr 2000					*/
/*		     final changes and corrections for the WAW airport	*/
/*		       	changes in 					*/
/*				htmhdl.h				*/
/*				function FillPage			*/
/*				function PARKINGProcessPage		*/
/*									*/
/*                  	                                                */
/*		     mos 05 Oct 2000					*/
/*			added multihopo fct				*/
/*				init fails, if hopo is not specified 	*/
/*				in each section, param HOMEAP		*/
/*				all sqls use current hopo		*/
/*									*/
/*			mos Nov 2000					*/
/*				added configurable nature processing	*/
/*									*/
/*                      mos Apr 2001                                    */
/*                              ftp rename remote file + timeout	*/
/* 20020708: JIM QUE_GET -> QUE_GETBIG: no memset, no igItemLen, and    */
/*       prgItem as movable pointer due to realloc in recv_big_item()   */
/* 20020708: sccs_verion: date and time to compile time    */
/*                                                                      */
/* ******************************************************************** */
/**static char sccs_version[]="@(#) UFIS 4.4 (c) ABB AAT/I htmhdl.c 44.5 / "__DATE__" "__TIME__" / MOS"; **/
    
/***************************************/
/* This program is a CEDA main program */
/***************************************/

/* ******************************************************************** */
/* Local used defines							*/
/* ******************************************************************** */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#define CFG_STRING 1
#define CFG_DEL (UCHAR)';'
#define INT_TAB_DEL (UCHAR)','
#define FTP_CTRL_FILE "/ceda/debug/.htmhdl_ftp.ctl"
#define FTP_LOG_FILE "/htmhdl_ftp.log"
#define FREE(pointer) if((pointer) != NULL) {igFreed++;free((void *)(pointer));}
#define MALLOC(pointer,type,size) if(((pointer)=(type)malloc((size)+1)) == NULL) {dbg(TRACE,"MALLOC: Error <%s>. Terminating...",strerror(errno));terminate(RC_SHUTDOWN);}else{igAlloc++;}
/* ******************************************************************** */
/* The master header file	(all other includes are listed in htmhdl.h)   */
/* ******************************************************************** */
#include "htmhdl.h"

/* ******************************************************************** */
/* External variables							*/
/* ******************************************************************** */
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp = NULL;*/
int debug_level = TRACE;

/* ******************************************************************** */
/* External functions							*/
/* ******************************************************************** */
extern int 	init_que();		
extern int 	que(int, int, int, int, int, char*);
extern int 	get_ora_err(int,char *);
extern int 	get_item_no(char *,char *,short);
extern int 	snap(char*,long,FILE*);
extern int 	SearchStringAndReplace(char *,char *,char *);
extern int 	init_db(void);
extern int 	ResetDBCounter(void);
extern int 	logoff(void);
extern void	catch_all();		
extern void	dbg_handle_debug(int);
extern void 	HandleRemoteDB(EVENT *);
extern void 	CDecode(char **);
extern char 	*GetPartOfTimeStamp(char *,char *);
extern char 	*GetDataField(char *,UINT,char);

/* ******************************************************************** */
/* global variables                                           		*/
/* ******************************************************************** */
static int 	igAlloc = 0;
static int 	igFreed = 0;
static int 	igItemLen = 0;
static int 	igSections = 0;
static int 	igFieldCount = 0;
static ITEM 	*prgItem = NULL; 		/* The queue item pointer */
static EVENT 	*prgEvent = NULL; 		/* The event pointer */
static STATIC_CFG *prgStaCfg = NULL;
static DYNAMIC_CFG *prgDynCfg = NULL;
static char 	*pcgDebugPath = NULL;
static char 	pcgCfgFile[256];
static char 	pcgSections[MAX_BUFFER_LEN];
static char 	pcgReceivedCommand[20];
static char 	*pcgEolSign = NULL; 
static char 	pcgDefHomeAp[20]; 
static char 	pcgDefTabEnd[20]; 
static UCHAR 	ucgData_del; 
static char 	*pcgSectionPointer = NULL;
static FILE 	*pfgAsciiFile = NULL;
static FILE 	*pfgHtmlDestFile = NULL;
static FILE 	*pfgHtmlTable = NULL;
static LPLISTHEADER prgTableList = NULL;


/******* Changes by MOS 01 Nov 1999 for FTPHDL implementation *****/
static FTPConfig prgFtp;			/* struct for dynamic configuration of FTPHDL */
static char 	pcgProcessName[20];      	/* buffer for process name of htmhdl*/ 
static int 	igModID_Ftphdl  = 0;		/* MOD-ID of Router  */
/*******EO Changes *********/

/******* Changes by MOS 27 Jan 2000 for WAW Specials implementation *****/
static int	igROWCount 	= 1;		/* Actual no of rows; required to compare with no of fixed rows */
static char 	pfgROWPrevCommand[XS_BUFF];	/* last processed command*/ 
static int 	igROWPrevValue  = 1;		/* last value of belt, gate aso field  */
static char	pfgTOWINGFlno[10];
static TOWING_PAGE pfgPAGE;               /* struct containing the TOwing page */
static char	pcgCheckin[10]	= "MHC7";	/* command related functionaltites*/
static char	pcgDomCheckin[10]= "MHC13";	/* please check with your config file*/
static char	pcgExit[10]	= "MHC8";
static char	pcgParking[10]	= "MHC10";
static char	pcgTowing[10]	= "MHC11";
static char	pcgTowing2[10]	= "MHC20";
/*******EO Changes *********/


/* BOOL's for checking the cfg-file */
static BOOL 	bgIgnoreAll;
static BOOL 	bgResolv;
static BOOL 	bgData_del;
static BOOL 	bgFtp;
static BOOL 	bgAsciiFile;
static BOOL 	bgHtmlTable;
static BOOL 	bgHtmlDestFile;
static BOOL 	bgStore;
static BOOL 	bgHead;
static BOOL 	bgCreateImgFields;
static BOOL 	bgFormatDate;
static BOOL 	bgTableHeader;
static BOOL 	bgTableData;
static BOOL 	bgResolvViaDB = TRUE;
static int 	igUseHopo = FALSE;

/********************************************************************** */
/* prototypes 			                                        */
/* ******************************************************************** */
static int  	init_htmhdl();
static void 	CheckStaCfg(void);
static void 	CheckDynCfg(void);
static void 	handle_err(int);
static void 	handle_qerr(int);
static int  	handle_data(void);
static void 	handle_sig(int ipSig);
static void 	terminate(int ipSig);
static int 	Reset();
static void 	HandleQueues();
static int 	ReadSections();
static int 	ReadSectionCfg(char *pcpSection);
static int 	GetCfgEntry(char *pcpSection,char *pcpEntry,short spType,
											char **pcpDest);
static int 	GetSection(char *pcpSection);
static int 	ReadTables(char *pcpReReadTable);
static int 	ReadDataFromTable(char *pcpTable,char *pcpFields,char *pcpLen);
static void	DumpIntTables(void);
static int 	CreateDefEle(TAB_DEF *prpDefEle,char *pcpTable,char *pcpFields,
	char *pcpLen);
static void 	FreeDynCfgMem(void);
static void 	FreeStaCfgMem(void);
static void 	TrimRight(char *pcpBuffer);
static void 	ResolvCfgFields(char *pcpBuffer);
static void 	InternalSql(char **pcpResult,char *pcpReplacement,char *pcpTable,
												char *pcpReference, char *pcpData);
static void 	DeleteAllInternalTables();
static void 	DeleteInternalTable(char *pcpTable);
static int 	TransmitFileViaFtp(char *pcpRHost,char *pcpFtpUser,
																char *pcpFtpPass,FILE *pfpFile);
static int 	CreateFtpCtrlFile();


/******* Changes by MOS 01 Nov 1999 for FTPHDL implementation *****/
static int 	SendEvent(char *pcpCmd,char *pcpSelection,char *pcpFields,char *pcpData,
										char *pcpAddStruct,int ipAddLen,int ipModID,int ipPriority,
										char *pcpTable,char *pcpType,char *pcpFile);
/**************EO changes *************/


/* HTML functions */
static int 	MakeHtml();
static int 	CreateTable();
static int 	CreateTableHeader();
static int 	CreateTableData();
static void 	WriteHtmlLine(char *pcpBuffer,char *pcpContents,char *pcpSource);
static void 	TagHtmlLine(int ipType, char *pcpBuffer, int ipTag);
static int  	CheckHtmlLine(int ipType, char *pcpBuffer);
static int 	WriteDataToFile(char *pcpData); 
static int 	SetEnv();
static void	CloseEnv(char *pcpSection);
static char 	*CreateCellAttribute(char *pcpAttribPointer,char *pcpAttrib,
																	char *pcpHtmlLine); 
static char 	*CreateCellData(char *pcpAttribPointer,char *pcpAttrib,
																	char *pcpHtmlLine); 
static void 	WriteComment(char *pcpComment);
static int 	ConvertToTableData(char *pcpBuffer,char *pcpRowColor);
static void 	CheckDates(char *pcpBuffer);
static int 	RemoveEolSign(char *pcpBuffer);
static int 	SetEolSign();
static void 	GetFieldData(char* pcpFieldList,char *pcpField,char pcpFieldDel,
				char *pcpData,char pcpDataDel,char **pcpReturnBuffer
				,char **pcpDataPointer);
static int 	FormatDate(char *pcpOldDate,char *pcpFormat,char **pcpNewDate);
static void 	FormatImageFields(char *pcpBuffer);
static int 	CreateHtmlDestFile(void);

/******* Changes by MOS 18 Jan 2000 for Check in data processing *****/
static int 	CHECKINProcessData(char *pclLineBuffer_withurno,
				char *pcpBuffer, char *pcpUrno);		/* main function for checkin info processing */
static int 	CHECKINProcessNature(char *pclLineBuffer_withurno,
				char *pcpBuffer, CCATAB_DED *rgCCA_DED, 
				char *pcpUrno, char *pcpnature);			/* handles flights with nature 11 */
static int 	CHECKINGetCommonCheckIn(CCATAB_STRUCT *rgCCA, char *pcpUrno,char *pcpnature);	/* creates internal table with common check in data */
static int 	CHECKINCalculateCounter(char *pclLineBuffer_withurno,
			char *pcpBuffer, char *pcpUrno, int ilcommonstart[200], int ilcommonend[200], 
			CCATAB_DED *rgCCA_DED, char *pcpnature, int ilnoofflights);			/* calculates the correct counter sequence and replaces */
										/* the counter value in the line buffer */
static int 	TOOLSort(CCATAB_DED *rgCCA_DED);				/* just for sorting the countervalues */
/**************EO changes *************/

/******* Changes by MOS 27 Jan 2000 for add row processing *****/
static int 	ROWCreate(char *pclLineBuffer_withurno,char *pclUrno,		/* this function adds empty rows to the existing datarecords*/
		char *pclReadPointer,int *pilColor_1,int *pilColor_2,		/* it enables aslo creation of fix amount of records for each value (eg belt 1) */
		int *pilColor_Cnt);				/* the third functionality is the adding of code share flights to the existig records */
static int 	ROWFill(char *pclLineBuffer_withurno,char *pclUrno,		/* this function adds empty rows to the existing datarecords*/
		char *pclReadPointer,int *pilColor_1,int *pilColor_2,		/* it enables aslo creation of fix amount of records for each value (eg belt 1) */
		int *pilColor_Cnt);				/* the third functionality is the adding of code share flights to the existig records */
/**************EO changes *************/

/******* Changes by MOS 05 Feb 2000 for parking page processing *****/
static int 	PARKINGProcessPage(char *pclLineBuffer_withurno);
static int 	UtcToLocal(char *pcpTime);
static void 	ReplaceEstTime(char *pcpEstBuf,char *pcpBuffer);
static void 	HighlightUpdates(char *pcpBuffer, char *pcpUrno);
static time_t 	CEDADatetolong(char *pcpTime);
/**************EO changes *************/

/******* Changes by MOS 07 Mar 2000 for towing page processing *****/
static int 	TOWINGProcessPage(char *pcpBuffer);	/* Creates page where all afttab records with the same rkey will be merged into one record */
static int 	TOWINGProcessPage2(char *pcpBuffer);	/* Creates page with separate rows for each towing activity */
static int      FillPage();
/**************EO changes *************/

/******* Changes by MOS 07 Feb 2000 for parking page processing *****/
static int 	CHECKINProcessPage(char *pclLineBuffer_withurno, char *pcpBuffer, char *pcpUrno);
/**************EO changes *************/

/******* Changes by MOS 10 Feb 2000 for jfno field processing *****/
static int CSFProcess(char *pcpBuffer);
/**************EO changes *************/


/* ******************************************************************** */
/* ******************************************************************** */
/*									*/
/* The MAIN program							*/
/*									*/
/* ******************************************************************** */
/* ******************************************************************** */
MAIN
{
	int	ilRC;			/* Return code			*/
	int 	ilCnt = 0;	
	char 	pclSection[MAX_BUFFER_LEN];
  int ilLen = 0;

	INITIALIZE;		/* General initialization	*/
	dbg(TRACE,"MAIN: version <%s>",mks_version);
	
	
	/* copy process-name to global buffer */
	memset(pcgProcessName,0x00,sizeof(pcgProcessName));
	strcpy(pcgProcessName, argv[0]);                            


	igItemLen = MAX_EVENT_SIZE;	
	
  /* 20020708: QUE_GET -> QUE_GETBIG: get prgItem by realloc in recv_big_item()
	MALLOC(prgItem,ITEM*,igItemLen);
	prgEvent = (EVENT *) prgItem->text;
  */
	
	do
	{
		ilRC = init_que();
		if (ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}
	}while ((ilCnt < 10) && (ilRC != RC_SUCCESS));

	if (ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		terminate(RC_SHUTDOWN);
	}
	else
	{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}
	
	if ((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) &&
			(ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"MAIN: waiting for status switch");
		HandleQueues();
	}
	
	if ((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) ||
			(ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"----------------------");
		dbg(TRACE,"MAIN: initializing ...");
		ilRC = init_htmhdl();
		if (ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_htmhdl() failed.");
			sleep(60);
			terminate(RC_SHUTDOWN);
		}
	}
	else
	{
		terminate(RC_SHUTDOWN);
	}
	
	
	catch_all(handle_sig);		/* handles signal	*/
	
	if (ilRC == RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: initializing ...OK");
		dbg(TRACE,"------------------------");
		dbg(TRACE,"MAIN: waiting for queues ...");
		while(TRUE)
		{
			/* 20020708: QUE_GET -> QUE_GETBIG: no memset, no igItemLen, and 
         prgItem as movable pointer due to realloc in recv_big_item()
         
      memset(prgItem,0x00,igItemLen);
			ilRC = que(QUE_GET,0,mod_id,PRIORITY_3,igItemLen,(char *)prgItem);
      */
      ilLen= 0;
			ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,ilLen,(char *)&prgItem);
      
			if (ilRC == RC_SUCCESS)
			{
	      prgEvent = (EVENT *) prgItem->text;
				ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
				if ( ilRC != RC_SUCCESS )
				{
					/* handle que_ack error */
					handle_qerr(ilRC);
				} 
				
				switch (prgEvent->command)
				{
					case	REMOTE_DB	:
						HandleRemoteDB(prgEvent);
						break;
					case	SHUTDOWN	:
						terminate(SHUTDOWN);
						break;
					case	RESET		:
						ilRC = Reset();
						break;
					case	EVENT_DATA	:
						if(ctrl_sta == HSB_ACTIVE || 
							 ctrl_sta == HSB_ACT_TO_SBY ||
							 ctrl_sta == HSB_STANDALONE )
						{
							ilRC = handle_data();
							if (ilRC != RC_SUCCESS)
							{
								handle_err(ilRC);
							}
						}
						else
						{
							dbg(DEBUG,"MAIN: wrong system state");
						}
						break;
					case	HSB_DOWN	:
						ctrl_sta = prgEvent->command;
						terminate(RC_SHUTDOWN);
						break;
					case	HSB_STANDALONE	:
						ctrl_sta = prgEvent->command;
						ResetDBCounter();
						break;
					case	HSB_STANDBY	:
						ctrl_sta = prgEvent->command;
						HandleQueues();
						break;
					case	HSB_ACTIVE	:
						ctrl_sta = prgEvent->command;
						break;
					case	HSB_COMING_UP	:
						ctrl_sta = prgEvent->command;
						HandleQueues();
						break;
					case	HSB_ACT_TO_SBY	:
						ctrl_sta = prgEvent->command;
						HandleQueues();
						break;
					case TRACE_ON:  /* 7 */
						dbg_handle_debug(prgEvent->command);
						break;
					case TRACE_OFF: /* 8 */
						dbg_handle_debug(prgEvent->command);
						break;
					default:
						dbg(DEBUG,"unknown event (%d)",
						prgEvent->command); 
						break;
				} /* end switch */
				
				/* Handle error conditions */
				if (ilRC != RC_SUCCESS)
				{
					handle_err(ilRC);
				} 
			} 
			else
			{
				/* Handle queuing errors */
				handle_qerr(ilRC);
			} 
		}
	}
	exit(0);
} 

/* *********************************************************************/
/* The initialization routine																					 */
/* *********************************************************************/
static int init_htmhdl()
{
	int 	ilRC = RC_SUCCESS;
	char 	*pclCfgPath = NULL;
	
	MALLOC(prgDynCfg,DYNAMIC_CFG*,sizeof(DYNAMIC_CFG));
	MALLOC(prgStaCfg,STATIC_CFG*,sizeof(STATIC_CFG));
	prgTableList = ListInit(prgTableList,sizeof(TAB_DEF));
	memset(pfgTOWINGFlno,0x00,10);

	while (init_db())
	{
		sleep(5);		
	} 
	
	if ((pclCfgPath = getenv("CFG_PATH")) == NULL)
	{
		dbg(TRACE,"init_htmhdl: error reading CFG-path env variable.");
		ilRC = RC_FAIL;
	}
	else
	{
		*pcgCfgFile = '\0';
		sprintf(pcgCfgFile,"%s/%s.cfg",pclCfgPath,pcgProcessName);
		dbg(DEBUG,"init_htmhdl: cfg-file = <%s>",pcgCfgFile);
	}
	
	*pcgDefHomeAp = 0x00;
	ilRC = tool_search_exco_data("SYS","HOMEAP",pcgDefHomeAp);
	if (ilRC != RC_SUCCESS)
	{
	  dbg(TRACE,"init_htmhdl : No HOMEAP entry in sgs.tab: EXTAB! Please add!");
	  return RC_FAIL;
	}
	else
  {
    dbg(TRACE,"init_htmhdl : HOMEAP = <%s>",pcgDefHomeAp);
  }

	*pcgDefTabEnd = 0x00;
	ilRC = tool_search_exco_data("ALL","TABEND",pcgDefTabEnd);
	if (ilRC != RC_SUCCESS)
	{
	  dbg(TRACE,"init_htmhdl : No TABEND entry in sgs.tab: EXTAB! Please add!");
	  return RC_FAIL;
	}
	else
  {
    dbg(TRACE,"init_htmhdl : TABEND = <%s>",pcgDefTabEnd);
		if (strcmp(pcgDefTabEnd,"TAB") == 0)
		{
			igUseHopo = TRUE;
			dbg(TRACE,"Init_htmhdl : use HOPO-field!");
		}
	}

	if ((ilRC = ReadSections()) == RC_SUCCESS)
	{
		if ((ilRC = ReadTables(NULL)) != RC_SUCCESS)
		{
			dbg(DEBUG,"init_htmhdl: ReadTables failed!");
		}
		else
		{
			DumpIntTables();
		}
	}
	else
	{
		dbg(DEBUG,"init_htmhdl: ReadSections() failed!");
	}
	
	if ((pcgDebugPath = getenv("DBG_PATH")) == NULL)
	{
		dbg(TRACE,"init_htmhdl: error reading DBG-path env variable.");
		ilRC = RC_FAIL;
	}
	
	CheckStaCfg();
	
	return ilRC;
}

/* *********************************************************************/
/* The ReadSections() routine																					 */
/* *********************************************************************/
static int ReadSections()
{
	int 	ilRC = RC_SUCCESS;

	memset(pcgSections,0x00,MAX_BUFFER_LEN);
	memset(prgStaCfg,0x00,sizeof(STATIC_CFG));
	/* reading [MAIN] section from cfg-file */
	if ((ilRC=GetCfgEntry("MAIN","SECTIONS",CFG_STRING,&prgStaCfg->sections))
			== RC_SUCCESS)
	{
		strcpy(pcgSections,prgStaCfg->sections);
		pcgSectionPointer = pcgSections;
		igSections = GetNoOfElements(prgStaCfg->sections,CFG_DEL);
	}
	return ilRC;
}
/* *********************************************************************/
/* The ReadSectionCfg() routine																					 */
/* *********************************************************************/
static int ReadSectionCfg(char *pcpSection)
{
	int ilRC;
	char *pclhopobuffer;
	char pclbuffer[XS_BUFF];
	
	memset(prgDynCfg,0x00,sizeof(DYNAMIC_CFG));
	dbg(DEBUG,"---------------------------------------------");
	/************* [FILES] ***************/
	if ((ilRC=GetCfgEntry(pcpSection,"COMMAND",CFG_STRING,&prgDynCfg->command))
			!= RC_SUCCESS)
	{return RC_FAIL;}
	if (strcmp(prgDynCfg->command,pcgReceivedCommand) == 0)
	{
		if ((ilRC=GetCfgEntry(pcpSection,"HOMEAP",CFG_STRING,&pclhopobuffer))
				!= RC_SUCCESS)
		{
		    	dbg(TRACE,"init_htmhdl : NO HOPO FOUND IN SECTION  <%s>",pcpSection);
			return RC_FAIL;
		}
		strcpy(pcgDefHomeAp,pclhopobuffer);
		dbg(DEBUG,"init_htmhdl : HOMEAP = <%s>",pcgDefHomeAp);

		if ((ilRC=GetCfgEntry(pcpSection,"DATA_DELIMITER",CFG_STRING,&prgDynCfg->data_del))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"EOL_SIGN",CFG_STRING,&prgDynCfg->eol_sign))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"EOF_SIGN",CFG_STRING,&prgDynCfg->eof_sign))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"ASCII_PATH",CFG_STRING,&prgDynCfg->ascii_path))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"ASCII_FILE",CFG_STRING,&prgDynCfg->ascii_file))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"HTML_PATH",CFG_STRING,&prgDynCfg->html_path))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"HTML_TABLE",CFG_STRING,&prgDynCfg->html_table))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"HTML_SRC_FILES",CFG_STRING,&prgDynCfg->html_src_files))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"HTML_DEST_FILE",CFG_STRING,&prgDynCfg->html_dest_file))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		
		
		/************* [CONNECT] ***************/
		if ((ilRC=GetCfgEntry(pcpSection,"RHOST",CFG_STRING,&prgDynCfg->rhost))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"FTP_PATH",CFG_STRING,&prgDynCfg->ftp_path))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		
		/******* Changes by MOS 01 Nov 1999 for FTPHDL implementation *****/
		if ((ilRC=GetCfgEntry(pcpSection,"FTP_CLIENT_OS",CFG_STRING,&prgDynCfg->ftp_client_os))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		/********** EO changes*******************/

		if ((ilRC=GetCfgEntry(pcpSection,"FTP_FILE",CFG_STRING,&prgDynCfg->ftp_file))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"FTP_USER",CFG_STRING,&prgDynCfg->ftp_user))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"FTP_PASS",CFG_STRING,&prgDynCfg->ftp_pass))
				!= RC_SUCCESS)
		{return RC_FAIL;}

		if ((ilRC=GetCfgEntry(pcpSection,"FTP_TIMEOUT",CFG_STRING,&prgDynCfg->ftptimeout))
				!= RC_SUCCESS)
		{
			/* set default for this */
			MALLOC(prgDynCfg->ftptimeout,char*,XS_BUFF);
                        strcpy(prgDynCfg->ftptimeout, "1");
		}	

		if ((ilRC=GetCfgEntry(pcpSection,"FTP_DATATIMEOUT",CFG_STRING,&prgDynCfg->ftpfiletimer))
				!= RC_SUCCESS)
		{
			/* set default for this */
			MALLOC(prgDynCfg->ftpfiletimer,char*,XS_BUFF);
                        strcpy(prgDynCfg->ftpfiletimer, "1");
		}	

		if ((ilRC=GetCfgEntry(pcpSection,"FTP_MODE",CFG_STRING,&prgDynCfg->ftpmode))
				!= RC_SUCCESS)
		{
			/* set default for this */
			MALLOC(prgDynCfg->ftpmode,char*,XS_BUFF);
                        strcpy(prgDynCfg->ftpmode, "FTPHDL");
		}	

		if ((ilRC=GetCfgEntry(pcpSection,"FTP_RENAME_FILE",CFG_STRING,&prgDynCfg->pcFtpRenameDestFile))
				!= RC_SUCCESS)
		{
			/* set default for this */
			MALLOC(prgDynCfg->pcFtpRenameDestFile,char*,XS_BUFF);
                        strcpy(prgDynCfg->pcFtpRenameDestFile, " ");
		}	

		/************* [TABLELAYOUT] ***************/
		if ((ilRC=GetCfgEntry(pcpSection,"FIELDS",CFG_STRING,&prgDynCfg->fields))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"FIELD_HEAD",CFG_STRING,&prgDynCfg->field_head))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"IMG_PATH",CFG_STRING,&prgDynCfg->img_path))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"IMG_FIELDS",CFG_STRING,&prgDynCfg->img_fields))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"IMG_EXT",CFG_STRING,&prgDynCfg->img_ext))
				!= RC_SUCCESS)
		{return RC_FAIL;}
				
		/******* Changes by MOS 18 Jan 2000 for Check-In information handling *****/
		if ((ilRC=GetCfgEntry(pcpSection,"CHECKIN_GAP",CFG_STRING,&prgDynCfg->checkin_gap))
				!= RC_SUCCESS)
		{
			/* set default for this */
			MALLOC(prgDynCfg->checkin_gap,char*,XS_BUFF);
                        strcpy(prgDynCfg->checkin_gap, " ");
		}	
		/********** EO changes*******************/
		/******* Changes by MOS 06 Feb 2001 for Check-In information handling *****/
		if ((ilRC=GetCfgEntry(pcpSection,"CHECKIN_NATURE_DOM_STRING",CFG_STRING,&prgDynCfg->checkin_dom_string))
				!= RC_SUCCESS)
		{
			/* set default for this */
			MALLOC(prgDynCfg->checkin_dom_string,char*,XS_BUFF);
                        strcpy(prgDynCfg->checkin_dom_string, " ");
		}	
		memset(pclbuffer,0x00,XS_BUFF);
		sprintf(pclbuffer,"|%s|",prgDynCfg->checkin_dom_string);
		strcpy(prgDynCfg->checkin_dom_string,pclbuffer);

		if ((ilRC=GetCfgEntry(pcpSection,"CHECKIN_NATURE_DOM_START",CFG_STRING,&prgDynCfg->checkin_dom_start))
				!= RC_SUCCESS)
		{
			/* set default for this */
			MALLOC(prgDynCfg->checkin_dom_start,char*,XS_BUFF);
                        strcpy(prgDynCfg->checkin_dom_start, "60");
		}	
		/********** EO changes*******************/

		/******* Changes by MOS 27 Jan 2000 for additional row adding *****/
		if ((ilRC=GetCfgEntry(pcpSection,"ADD_ROWS",CFG_STRING,&prgDynCfg->addrows))
				!= RC_SUCCESS)
		{
			/* set default for this */
			MALLOC(prgDynCfg->addrows,char*,XS_BUFF);
                        strcpy(prgDynCfg->addrows, " ");
		}	
		if ((ilRC=GetCfgEntry(pcpSection,"FIXNOOFROWS",CFG_STRING,&prgDynCfg->fixrowamount))
				!= RC_SUCCESS)
		{
			/* set default for this */
			MALLOC(prgDynCfg->fixrowamount,char*,XS_BUFF);
                        strcpy(prgDynCfg->fixrowamount, " ");
		}	
			
		StringUPR((UCHAR *) prgDynCfg->addrows);

		if ((ilRC=GetCfgEntry(pcpSection,"JOIN_LEN",CFG_STRING,&prgDynCfg->jfno_len))
				!= RC_SUCCESS)
		{
			/* set default for this */
			MALLOC(prgDynCfg->jfno_len,char*,XS_BUFF);
                        strcpy(prgDynCfg->jfno_len, " ");
		}	
		
		if ((ilRC=GetCfgEntry(pcpSection,"JOIN_NR",CFG_STRING,&prgDynCfg->jfno_nr))
				!= RC_SUCCESS)
		{
			/* set default for this */
			MALLOC(prgDynCfg->jfno_nr,char*,XS_BUFF);
                        strcpy(prgDynCfg->jfno_nr, " ");
		}	
		
		if ((ilRC=GetCfgEntry(pcpSection,"FIXNOOFROWS_START",CFG_STRING,&prgDynCfg->fixrowstart))
				!= RC_SUCCESS)
		{
			/* set default for this */
			MALLOC(prgDynCfg->fixrowstart,char*,XS_BUFF);
                        strcpy(prgDynCfg->fixrowstart, " ");
		}	
		
		if ((ilRC=GetCfgEntry(pcpSection,"FIXNOOFROWS_END",CFG_STRING,&prgDynCfg->fixrowend))
				!= RC_SUCCESS)
		{
			/* set default for this */
			MALLOC(prgDynCfg->fixrowend,char*,XS_BUFF);
                        strcpy(prgDynCfg->fixrowend, " ");
		}	


                if ((ilRC=GetCfgEntry(pcpSection,"FIXNOOFROWS_FORMAT",CFG_STRING,&prgDynCfg->fixrowformat))
                                != RC_SUCCESS)
                {
                        /* set default for this */
                        MALLOC(prgDynCfg->fixrowformat,char*,XS_BUFF);
                        strcpy(prgDynCfg->fixrowformat, " ");
                }       

		
		if ((ilRC=GetCfgEntry(pcpSection,"PROCESS_CHECKIN",CFG_STRING,&prgDynCfg->processcheckin))
				!= RC_SUCCESS)
		{
			/* set default for this */
			MALLOC(prgDynCfg->processcheckin,char*,XS_BUFF);
                        strcpy(prgDynCfg->processcheckin, "NO");
		}	


		/********** EO changes*******************/

		/******* Changes by MOS 13 Sep 2000 for nature processing *****/
		if ((ilRC=GetCfgEntry(pcpSection,"CHECKIN_NATURE_ASSIGNMENT",CFG_STRING,&prgDynCfg->checkin_calc))
				!= RC_SUCCESS)
		{
			/* set default for this */
			MALLOC(prgDynCfg->checkin_calc,char*,XS_BUFF);
                        strcpy(prgDynCfg->checkin_calc, " ");
		}	

		if ((ilRC=GetCfgEntry(pcpSection,"CHECKIN_NATURE_USE_FLTI",CFG_STRING,&prgDynCfg->use_flti))
				!= RC_SUCCESS)
		{
			/* set default for this */
			MALLOC(prgDynCfg->use_flti,char*,XS_BUFF);
                        strcpy(prgDynCfg->use_flti, " ");
		}	

		/********** EO changes*******************/

		
		if ((ilRC=GetCfgEntry(pcpSection,"REPL_FIELDS",CFG_STRING,&prgDynCfg->repl_fields))
				!= RC_SUCCESS)
		{
			/* set default for this */
			MALLOC(prgDynCfg->repl_fields,char*,XS_BUFF);
                        strcpy(prgDynCfg->repl_fields, " ");
		}	
		if ((ilRC=GetCfgEntry(pcpSection,"REPLACEMENT",CFG_STRING,&prgDynCfg->replacement))
				!= RC_SUCCESS)
		{
			/* set default for this */
			MALLOC(prgDynCfg->replacement,char*,XS_BUFF);
                        strcpy(prgDynCfg->replacement, " ");
		}	
		if ((ilRC=GetCfgEntry(pcpSection,"REPL_TABLE",CFG_STRING,&prgDynCfg->repl_table))
				!= RC_SUCCESS)
		{
			/* set default for this */
			MALLOC(prgDynCfg->repl_table,char*,XS_BUFF);
                        strcpy(prgDynCfg->repl_table, " ");
		}	
		if ((ilRC=GetCfgEntry(pcpSection,"REFERENCE",CFG_STRING,&prgDynCfg->reference))
				!= RC_SUCCESS)
		{
			/* set default for this */
			MALLOC(prgDynCfg->reference,char*,XS_BUFF);
                        strcpy(prgDynCfg->reference, " ");
		}	
		if ((ilRC=GetCfgEntry(pcpSection,"NO_TXT",CFG_STRING,&prgDynCfg->no_txt))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"DATE_FIELDS",CFG_STRING,&prgDynCfg->date_fields))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"DATE_FORMAT",CFG_STRING,&prgDynCfg->date_format))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"TABLE_B",CFG_STRING,&prgDynCfg->table_b))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"TABLE_W",CFG_STRING,&prgDynCfg->table_w))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"TABLE_H",CFG_STRING,&prgDynCfg->table_h))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"TABLE_CS",CFG_STRING,&prgDynCfg->table_cs))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"TABLE_CP",CFG_STRING,&prgDynCfg->table_cp))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"TABLE_BGC",CFG_STRING,&prgDynCfg->table_bgc))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"TABLE_BC",CFG_STRING,&prgDynCfg->table_bc))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"TABLE_BCD",CFG_STRING,&prgDynCfg->table_bcd))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"TABLE_BCL",CFG_STRING,&prgDynCfg->table_bcl))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"TABLE_BACKG",CFG_STRING,&prgDynCfg->table_backg))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"TH_ALIGN",CFG_STRING,&prgDynCfg->th_align))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"TH_VALIGN",CFG_STRING,&prgDynCfg->th_valign))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"TH_WIDTH",CFG_STRING,&prgDynCfg->th_width))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"TH_HEIGHT",CFG_STRING,&prgDynCfg->th_height))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"TH_BGCOLOR",CFG_STRING,&prgDynCfg->th_bgc))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"TH_NOWRAP",CFG_STRING,&prgDynCfg->th_nowrap))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"TH_FONTFACE",CFG_STRING,&prgDynCfg->th_fontf))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"TH_FONTCOLOR",CFG_STRING,&prgDynCfg->th_fontc))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"TH_FONTSIZE",CFG_STRING,&prgDynCfg->th_fonts))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"TD_ALIGN",CFG_STRING,&prgDynCfg->td_align))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"TD_VALIGN",CFG_STRING,&prgDynCfg->td_valign))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"TD_HEIGHT",CFG_STRING,&prgDynCfg->td_height))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"TD_BGCOLOR1",CFG_STRING,&prgDynCfg->td_bgc1))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"TD_BGCOLOR2",CFG_STRING,&prgDynCfg->td_bgc2))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"TD_NOWRAP",CFG_STRING,&prgDynCfg->td_nowrap))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if ((ilRC=GetCfgEntry(pcpSection,"TD_FONTFACE",CFG_STRING,&prgDynCfg->td_fontf))
				!= RC_SUCCESS)
		{return RC_FAIL;}
		if((ilRC=GetCfgEntry(pcpSection,"TD_FONTCOLOR1",CFG_STRING,&prgDynCfg->td_fontc1))
				!= RC_SUCCESS)
		{return RC_FAIL;}

		/* HIGHLIGHT  SECTIONS */
		/*  UPDATES  */
		ilRC=GetCfgEntry(pcpSection,"HIGHLIGHT_UPDATES",CFG_STRING,&prgDynCfg->highlightupdate);
				
		if ((ilRC=GetCfgEntry(pcpSection,"HIGHLIGHT_DIFFERENCE",CFG_STRING,&prgDynCfg->highlightupdate_difference))
				!= RC_SUCCESS)
		{
			/* set default for this */
			MALLOC(prgDynCfg->highlightupdate_difference,char*,XS_BUFF);
                        strcpy(prgDynCfg->highlightupdate_difference, "0");
		}	

		/*  REMARK  */
		ilRC=GetCfgEntry(pcpSection,"HIGHLIGHT_REMARKS",CFG_STRING,&prgDynCfg->highlightremark);
		
		/*  TMO VLAUES  */
		ilRC=GetCfgEntry(pcpSection,"HIGHLIGHT_TMO",CFG_STRING,&prgDynCfg->highlighttmo);


		if ((ilRC=GetCfgEntry(pcpSection,"HIGHLIGHT_BGCOLOR1",CFG_STRING,&prgDynCfg->highlightupdate_bgcolor1))
				!= RC_SUCCESS)
		{
			/* set default for this */
			MALLOC(prgDynCfg->highlightupdate_bgcolor1,char*,XS_BUFF);
                        strcpy(prgDynCfg->highlightupdate_bgcolor1, prgDynCfg->td_bgc1);
		}	

		if ((ilRC=GetCfgEntry(pcpSection,"HIGHLIGHT_BGCOLOR2",CFG_STRING,&prgDynCfg->highlightupdate_bgcolor2))
				!= RC_SUCCESS)
		{
			/* set default for this */
			MALLOC(prgDynCfg->highlightupdate_bgcolor2,char*,XS_BUFF);
                        strcpy(prgDynCfg->highlightupdate_bgcolor2, prgDynCfg->td_bgc2);
		}	

		if ((ilRC=GetCfgEntry(pcpSection,"TD_FONTSIZE",CFG_STRING,&prgDynCfg->td_fonts))
				!= RC_SUCCESS)
		{return RC_FAIL;}

	}
	else
	{
		ilRC = RC_FAIL;
	}
	dbg(DEBUG,"---------------------------------------------");
	return ilRC;
}	

/* *********************************************************************/
/* The GetCfgEntry() routine   																				 */
/* *********************************************************************/
static int GetCfgEntry(char *pcpSection,char *pcpEntry,short spType,
												char **pcpDest)
{
	int 	ilRC = RC_SUCCESS;
	char 	pclCfgLineBuffer[MAX_BUFFER_LEN];
	
	memset(pclCfgLineBuffer,0x00,MAX_BUFFER_LEN);

	if ((ilRC=iGetConfigRow(pcgCfgFile,pcpSection,pcpEntry,spType,
													pclCfgLineBuffer)) != RC_SUCCESS)
	{
		dbg(DEBUG,"pcpSection<%s> pcpEntry<%s> spType<%d>",pcpSection,pcpEntry,spType);
		dbg(DEBUG,"GetCfgEntry: reading entry <%s> failed.",pcpEntry);
	}
	else
	{
		pclCfgLineBuffer[strlen(pclCfgLineBuffer)] = '\0';	
		
		MALLOC(*pcpDest,char*,strlen(pclCfgLineBuffer));

		strcpy(*pcpDest,pclCfgLineBuffer);
		dbg(DEBUG,"GetCfgEntry: %s = <%s>",pcpEntry,*pcpDest);
	}
	return ilRC;
}

/* ******************************************************************** */
/* The FreeStaCfgMem() routine	  	                                */
/* ******************************************************************** */
static void FreeStaCfgMem(void)
{
	FREE(prgStaCfg->sections);
	FREE(prgStaCfg->int_tables);
	FREE(prgStaCfg->int_fields);
	FREE(prgStaCfg->field_len);
}

/* ******************************************************************** */
/* The FreeDynCfgMem() routine	  	                                    */
/* ******************************************************************** */
static void FreeDynCfgMem(void)
{ 
	FREE(prgDynCfg->command);
	FREE(prgDynCfg->data_del);
	FREE(prgDynCfg->eol_sign);
	FREE(prgDynCfg->eof_sign);
	FREE(prgDynCfg->ascii_path);
	FREE(prgDynCfg->ascii_file);
	FREE(prgDynCfg->html_path);
	FREE(prgDynCfg->html_table);
	FREE(prgDynCfg->html_src_files);
	FREE(prgDynCfg->html_dest_file);
	FREE(prgDynCfg->rhost);
	FREE(prgDynCfg->ftp_path);
	FREE(prgDynCfg->ftp_client_os);
	FREE(prgDynCfg->ftp_file);
	FREE(prgDynCfg->ftp_user);
	FREE(prgDynCfg->ftp_pass);
	FREE(prgDynCfg->fields);
	FREE(prgDynCfg->field_head);
	FREE(prgDynCfg->img_path);
	FREE(prgDynCfg->img_fields);
	FREE(prgDynCfg->img_ext);
	
	FREE(prgDynCfg->checkin_gap);
	FREE(prgDynCfg->addrows);
	FREE(prgDynCfg->fixrowamount);
	FREE(prgDynCfg->jfno_len);
	FREE(prgDynCfg->jfno_nr);
	FREE(prgDynCfg->fixrowstart);
	FREE(prgDynCfg->fixrowend);
	FREE(prgDynCfg->fixrowformat);
	
	FREE(prgDynCfg->repl_fields);
	FREE(prgDynCfg->replacement);
	FREE(prgDynCfg->repl_table);
	FREE(prgDynCfg->reference);
	FREE(prgDynCfg->no_txt);
	FREE(prgDynCfg->date_fields);
	FREE(prgDynCfg->date_format);
	FREE(prgDynCfg->table_b);
	FREE(prgDynCfg->table_w);
	FREE(prgDynCfg->table_h);
	FREE(prgDynCfg->table_cs);
	FREE(prgDynCfg->table_cp);
	FREE(prgDynCfg->table_bgc);
	FREE(prgDynCfg->table_bc);
	FREE(prgDynCfg->table_bcd);
	FREE(prgDynCfg->table_bcl);
	FREE(prgDynCfg->table_backg);
	FREE(prgDynCfg->th_align);
	FREE(prgDynCfg->th_valign);
	FREE(prgDynCfg->th_width);
	FREE(prgDynCfg->th_height);
	FREE(prgDynCfg->th_bgc);
	FREE(prgDynCfg->th_nowrap);
	FREE(prgDynCfg->th_fontf);
	FREE(prgDynCfg->th_fontc);
	FREE(prgDynCfg->th_fonts);
	FREE(prgDynCfg->td_align);
	FREE(prgDynCfg->td_valign);
	FREE(prgDynCfg->td_height);
	FREE(prgDynCfg->td_bgc1);
	FREE(prgDynCfg->td_bgc2);
	FREE(prgDynCfg->td_nowrap);
	FREE(prgDynCfg->td_fontf);
	FREE(prgDynCfg->td_fontc1);
	FREE(prgDynCfg->td_fonts);
	
	FREE(pcgEolSign);
}

/* *********************************************************************/
/* The handle_sig routine     																				 */
/* *********************************************************************/
void handle_sig(ipSig) 
{
	int	ilRC = 0;		
	switch (ipSig)
	{
	case SIGHUP:
		dbg(TRACE,"handle_sig: Received Signal <%d>(SIGHUP).Reinitialize ..",ipSig);
		ilRC = ReadSections();
		break;
	case SIGTERM:
		dbg(TRACE,"handle_sig: Received Signal <%d>(SIGTERM).Terminating ..",ipSig);
		terminate(ipSig);
		break;
	case SIGALRM:
		dbg(TRACE,"handle_sig: Received Signal<%d>(SIGALRM)",ipSig);
		break;
	case SIGCLD:
		dbg(TRACE,"handle_sig: Received Signal<%d>(SIGCLD)",ipSig);
		break;
	default:
		dbg(TRACE,"handle_sig: Received Signal<%d>",ipSig);
		terminate(ipSig);
		break;
	} 
	exit(0);
} 

/**********************************************************************/
/* The terminate routine      																				 */
/**********************************************************************/
static void terminate(int ipSig)
{
	int 	ilRC;

	ilRC = logoff();  /* disconnect from oracle */
  	dbg(TRACE,"terminate: oracle     ... logged off.");
	DeleteAllInternalTables(); /* incl. FREE of all allocated memory */
	ListDestroy(prgTableList);
 	 dbg(TRACE,"terminate: int. tables... deleted.");
	FreeStaCfgMem();
	FREE(prgDynCfg);
	FREE(prgStaCfg);
	FREE(prgItem);
  	dbg(TRACE,"terminate: allocated  ... <%d> x",igAlloc);
  	dbg(TRACE,"terminate: freed      ... <%d> x",igFreed);
	dbg(TRACE,"terminate: now Leaving.............");
	exit(0);
} 

/**********************************************************************/
/* The Reset routine          																				*/
/**********************************************************************/
static int Reset(void)
{
	int 	ilRC;

	dbg(TRACE,"Reset: reinit htmhdl ...");
	DeleteAllInternalTables(); /* incl. FREE of all allocated memory */
  	dbg(TRACE,"Reset: int. tables   ... deleted.");
	FreeStaCfgMem();
  	igAlloc=0;
  	igFreed=0;
	
	if ((ilRC = ReadSections()) == RC_SUCCESS)
	{
  	dbg(TRACE,"Reset: sections      ... read.");
		if ((ilRC = ReadTables(NULL)) != RC_SUCCESS)
		{
			dbg(DEBUG,"Reset: ReadTables failed!");
		}
		else
		{
  		dbg(TRACE,"Reset: int. tables   ... read.");
		}
	}
	else
	{
		dbg(DEBUG,"Reset: ReadSections() failed!");
	}
	return ilRC;
} 

/* ******************************************************************** */
/* The handle_err routine			                                      		*/
/* ******************************************************************** */
static void handle_err(ipErr)				
 	int	 ipErr;
{  
	dbg(TRACE,"handle_err: received error (%d)",ipErr);
	return;
} 

/* ******************************************************************** */
/* The handle_qerr routine		                                    			*/
/* ******************************************************************** */
static void handle_qerr(int ipErr)
{
	switch(ipErr)
	{
		case	  QUE_E_FUNC	:		
			dbg(TRACE,"<%d>: unknown function.",ipErr);
			break;
		case	  QUE_E_MEMORY	:		
			dbg(TRACE,"<%d>: malloc failed.",ipErr);
			break;
		case	  QUE_E_SEND	:		
			dbg(TRACE,"<%d>: msgsnd failed.",ipErr);
			break;
		case	  QUE_E_GET	:		
			dbg(TRACE,"<%d>: msgrcv failed. ",ipErr);
			break;
		case	  QUE_E_EXISTS	:		
			dbg(TRACE,"<%d>: route/queue already exists.",ipErr);
			break;
		case	  QUE_E_NOFIND	:		
			dbg(TRACE,"<%d>: roue not found.",ipErr);
			break;
		case	  QUE_E_ACKUNEX	:		
			dbg(TRACE,"<%d>: unexpected ack received.",ipErr);
			break;
		case	  QUE_E_STATUS	:		
			dbg(TRACE,"<%d>: unknown queue status.",ipErr);
			break;
		case	  QUE_E_INACTIVE: 	
			dbg(TRACE,"<%d>: queue is inaktiv.",ipErr);
			break;
		case	  QUE_E_MISACK	:		
			dbg(TRACE,"<%d>: missing ack.",ipErr);
			break;
		case	  QUE_E_NOQUEUES:		
			dbg(TRACE,"<%d>: queue does not exists.",ipErr);
			break;
		case	  QUE_E_RESP	:		
			dbg(TRACE,"<%d>: no response on create.",ipErr);
			break;
		case	  QUE_E_FULL	:		
			dbg(TRACE,"<%d>: to many route destinations.",ipErr);
			break;
		case	  QUE_E_NOMSG	:		
			dbg(TRACE,"<%d>: no message on queue.",ipErr);
			break;
		case	  QUE_E_INVORG	:		
			dbg(TRACE,"<%d>: invalid originator=0.",ipErr);
			break;
		case	  QUE_E_NOINIT	:		
			dbg(TRACE,"<%d>: queues are not initialized.",ipErr);
			break;
		case	  QUE_E_ITOBIG	:		
			dbg(TRACE,"<%d>: requested itemsize to big.",ipErr);
			break;
		case	  QUE_E_BUFSIZ	:		
			dbg(TRACE,"<%d>: receive buffer to small.",ipErr);
			break;
		default			:		
			dbg(TRACE,"<%d>: unknown error.",ipErr);
			break;
	} 
	return;
} 

/* ******************************************************************** */
/* The handle data routine						                                  */
/* following commands are recognized:                                   */
/* ******************************************************************** */
static int handle_data(void)
{
  	int	ilRC = RC_SUCCESS;	/* Return code 			*/
  	char 	tmpBuf[16];
	char 	pclSection[MAX_BUFFER_LEN];
  	char 	pclTmpObj_name[35];
  
  	BC_HEAD *bchd = NULL;		/* Broadcast header		*/
  	CMDBLK  *cmdblk = NULL;	/* Command Block 		*/

  	bchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  	cmdblk= (CMDBLK  *) ((char *)bchd->data);

	memset(pcgReceivedCommand,0x00,20);
	strcpy(pcgReceivedCommand,cmdblk->command);
	
  	dbg(DEBUG,"");
  	dbg(DEBUG,"--- START ---");
  	dbg(DEBUG,"handle_data: Command<%s> from<%d> obj_name<%s>",cmdblk->command,
							prgEvent->originator,cmdblk->obj_name);

	/* HIER geht weiter mit auslesen und setzen von TABEND,HOMEAP und APPLIKATION aus */
	/* aus CMDBLK->tw_end f�r TABEND Erweiterung*/

	if (bgIgnoreAll == FALSE)
  	{
		pcgSectionPointer = pcgSections;
		while(GetSection(pclSection) == RC_SUCCESS)
		{
			if ((ilRC = ReadSectionCfg(pclSection)) == RC_SUCCESS)
			{
				CheckDynCfg();

				if ((ilRC = MakeHtml()) != RC_SUCCESS)
				{
					dbg(DEBUG,"handle_data: MakeHtml() <%s> failed!",pclSection);
					CloseEnv(pclSection);
				}
				else
				{
					CloseEnv(pclSection);
					if ((ilRC=CreateHtmlDestFile())
							== RC_SUCCESS)
					{
						if (bgFtp == TRUE)
						{
							if ((ilRC=TransmitFileViaFtp(prgDynCfg->rhost,prgDynCfg->ftp_user,
												prgDynCfg->ftp_pass,pfgHtmlDestFile)) != RC_SUCCESS)
							{
								dbg(TRACE,"handle_data: didn't TransmitFileViaFtp !");
							}
						}
					}
				}
				/* all cfg-memory freed only if all cfg-lines have been read.*/
				FreeDynCfgMem();
			}
			else
			{
				dbg(DEBUG,"handle_data: recv command <%s> != section command <%s>! "
					"skipping!",cmdblk->command,prgDynCfg->command);
				FREE(prgDynCfg->command);
				ilRC = RC_SUCCESS;
			}
		}
  	}
	else
	{
  	dbg(TRACE,"handle_data: htmhdl.cfg seems to be wrong. ignoring all commands.");
	
	}

	/* rereading internal tables because of broadcasts */
	if (strlen(cmdblk->obj_name) > 0)
	{
		strcpy(pclTmpObj_name,cmdblk->obj_name);
		/* check for internal tables */
		StringLWR((UCHAR*)pclTmpObj_name);
		if (strstr(prgStaCfg->int_tables,pclTmpObj_name) != NULL)
		{
			DeleteInternalTable(pclTmpObj_name);
			ilRC = ReadTables(pclTmpObj_name);	
		}
		else
		{
			StringUPR((UCHAR*)pclTmpObj_name);
			if (strstr(prgStaCfg->int_tables,pclTmpObj_name) != NULL)
			{
				DeleteInternalTable(pclTmpObj_name);
				ilRC = ReadTables(pclTmpObj_name);	
			}
		}
	}
  	dbg(DEBUG,"--- END ---");
  	dbg(DEBUG,"");
  	return ilRC;
} 

/* ******************************************************************** */
/* The HandleQueues routine  					                                  */
/* ******************************************************************** */
static void HandleQueues()
{
		int ilRC = RC_SUCCESS;
		int ilBreakOut = FALSE;
    int ilLen= 0;

		do	
		{
			/* 20020708: QUE_GET -> QUE_GETBIG: no memset, no igItemLen, and 
         prgItem as movable pointer due to realloc in recv_big_item()
         
			memset(prgItem,0x00,igItemLen);
			ilRC = que(QUE_GET,0,mod_id,PRIORITY_3,igItemLen,(char *)prgItem);
      */
      ilLen= 0;
			ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,ilLen,(char *)&prgItem);
			if (ilRC == RC_SUCCESS)
			{
	      prgEvent = (EVENT *) prgItem->text;
				ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
				if ( ilRC != RC_SUCCESS )
				{
					/* handle que_ack error */
					handle_qerr(ilRC);
				} 
				
				switch (prgEvent->command)
				{
					case	REMOTE_DB	:
						HandleRemoteDB(prgEvent);
						break;
					case	SHUTDOWN	:
						terminate(SHUTDOWN);
						break;
					case	RESET		:
						ilRC = Reset();
						break;
					case	EVENT_DATA	:
					dbg(TRACE,"HandleQueues: wrong HSB state <%d>",ctrl_sta);
						break;
					case	HSB_DOWN	:
						ctrl_sta = prgEvent->command;
						terminate(RC_SHUTDOWN);
						break;
					case	HSB_STANDALONE	:
						ctrl_sta = prgEvent->command;
						ResetDBCounter();
						ilBreakOut = TRUE;
						break;
					case	HSB_STANDBY	:
						ctrl_sta = prgEvent->command;
						break;
					case	HSB_ACTIVE	:
						ctrl_sta = prgEvent->command;
						ilBreakOut = TRUE;
						break;
					case	HSB_COMING_UP	:
						ctrl_sta = prgEvent->command;
						break;
					case	HSB_ACT_TO_SBY	:
						ctrl_sta = prgEvent->command;
						break;
					case TRACE_ON:  /* 7 */
						dbg_handle_debug(prgEvent->command);
						break;
					case TRACE_OFF: /* 8 */
						dbg_handle_debug(prgEvent->command);
						break;
					default:
						dbg(TRACE,"unknown event (%d)",
						prgEvent->command); 
						break;
				} /* end switch */
				
				/* Handle error conditions */
				if (ilRC != RC_SUCCESS)
				{
					handle_err(ilRC);
				} 
			} 
			else
			{
				/* Handle queuing errors */
				handle_qerr(ilRC);
			} 
		}while(ilBreakOut == FALSE);
}

/* ******************************************************************** */
/* The GetSection() routine						                                  */
/* ******************************************************************** */
static int GetSection(char *pcpSection)
{
	int ilRC = RC_SUCCESS;
	
	memset((char*)pcpSection,0x00,MAX_BUFFER_LEN);
	pcgSectionPointer = CopyNextField(pcgSectionPointer,CFG_DEL,pcpSection);
	if (pcpSection[0] == 0x00)
	{
		ilRC = RC_FAIL;
	}
	else
	{
		dbg(DEBUG,"GetSection : <%s>",pcpSection);
	}
	return ilRC;
}

/* ******************************************************************** */
/* The MakeHtml() routine	  					                                  */
/* ******************************************************************** */
static int MakeHtml()
{
	int 	ilRC = RC_SUCCESS;

	if ((ilRC = SetEnv()) == RC_SUCCESS)
	{
		if ((ilRC = CreateTable()) != RC_SUCCESS)
		{
			dbg(DEBUG,"MakeHtml: CreateTable() failed !");
		}
	}
	else
	{
		dbg(DEBUG,"MakeHtml: SetEnv() failed !");
	}
	return ilRC;
}

/* ******************************************************************** */
/* The CreateTable() routine	  			                                  */
/* ******************************************************************** */
static int CreateTable()
{
	int 	ilRC = RC_SUCCESS;
	int 	ilRC_th;
	int 	ilRC_td;
	char 	pclTableLayout[1024];
	
	*pclTableLayout = '\0';
		
	WriteComment("TABLELAYOUT");

	/* opening HTML-line and creating the table-layout */
	TagHtmlLine(TABLE,pclTableLayout,OPEN_TAG);
	WriteHtmlLine(pclTableLayout,"border=\"%s\"",prgDynCfg->table_b);
	WriteHtmlLine(pclTableLayout,"cellspacing=\"%s\"",prgDynCfg->table_cs);
	WriteHtmlLine(pclTableLayout,"cellpadding=\"%s\"",prgDynCfg->table_cp);
	WriteHtmlLine(pclTableLayout,"width=\"%s\"",prgDynCfg->table_w);
	WriteHtmlLine(pclTableLayout,"height=\"%s\"",prgDynCfg->table_h);
	WriteHtmlLine(pclTableLayout,"bgcolor=\"%s\"",prgDynCfg->table_bgc);
	WriteHtmlLine(pclTableLayout,"bordercolor=\"%s\"",prgDynCfg->table_bc);
	WriteHtmlLine(pclTableLayout,"bordercolorlight=\"%s\"",prgDynCfg->table_bcl);
	WriteHtmlLine(pclTableLayout,"bordercolordark=\"%s\"",prgDynCfg->table_bcd);
	WriteHtmlLine(pclTableLayout,"background=\"%s\"",prgDynCfg->table_backg);
	TagHtmlLine(TABLE,pclTableLayout,CLOSE_TAG);
			
	/* checking the line-syntax and writing it to a file if ok */
	if ((ilRC = CheckHtmlLine(TABLE,pclTableLayout)) == RC_SUCCESS)
	{
		if ((ilRC = WriteDataToFile(pclTableLayout)) != RC_SUCCESS)
		{
			dbg(DEBUG,"CreateTable: WriteDataToFile() failed!");
		}
		else
		{
			if (ilRC == RC_SUCCESS)
			{
				if ((ilRC_th = CreateTableHeader()) == RC_SUCCESS)
				{
					if ((ilRC_td = CreateTableData()) != RC_SUCCESS)
					{
						dbg(DEBUG,"CreateTable: No table data created.");
					}
				}
				else
				{
					dbg(DEBUG,"CreateTable: No table header created.");
				}
			}
			else
			{
				dbg(DEBUG,"CreateTable: No table-layout created.");
			}
		}
	}
	return ilRC;
}

/* ******************************************************************** */
/* The CreateTableHeader() routine	  	                                */
/* ******************************************************************** */
static int CreateTableHeader()
{
	int ilRC = RC_SUCCESS;
	int ilCnt = 0;
	char pclHeader[MAX_BUFFER_LEN];
	char pclCell[MAX_BUFFER_LEN];
	
	char *pclAlign = prgDynCfg->th_align; 
	char *pclValign = prgDynCfg->th_valign;
	char *pclWidth = prgDynCfg->th_width;
	char *pclHeight = prgDynCfg->th_height;
	char *pclBgc = prgDynCfg->th_bgc;
	char *pclNowrap = prgDynCfg->th_nowrap;
	char *pclFontf = prgDynCfg->th_fontf;
	char *pclFontc = prgDynCfg->th_fontc;
	char *pclFonts = prgDynCfg->th_fonts;
	char *pclField_Head = prgDynCfg->field_head;
	char *pclDummy = NULL;

	if (bgTableHeader == TRUE)
	{
		WriteComment("TABLEHEADER");

		*pclHeader = '\0';
		*pclCell = '\0';
		TagHtmlLine(TABLE_ROW,pclHeader,OPEN_TAG);
		TagHtmlLine(TABLE_ROW,pclHeader,CLOSE_TAG);
			
		/* opening HTML-line and creating the table-header-row */
		for (ilCnt = 0; ilCnt < igFieldCount; ilCnt ++)
		{
			TagHtmlLine(TABLE_HEAD,pclCell,OPEN_TAG);
			pclAlign = CreateCellAttribute(pclAlign,"align=\"%s\"",pclCell);
			pclValign = CreateCellAttribute(pclValign,"valign=\"%s\"",pclCell);
			pclWidth = CreateCellAttribute(pclWidth,"width=\"%s\"",pclCell);
			pclHeight = CreateCellAttribute(pclHeight,"height=\"%s\"",pclCell);
			pclBgc = CreateCellAttribute(pclBgc,"bgcolor=\"%s\"",pclCell);
			if ((strstr(pclNowrap,"YES") != NULL) || (strstr(pclNowrap,"yes") != NULL)) {
				pclNowrap = CreateCellAttribute(pclNowrap,"nowrap=\"%s\"",pclCell);
			}
			TagHtmlLine(TABLE_HEAD,pclCell,CLOSE_TAG);
			TagHtmlLine(FONT,pclCell,OPEN_TAG);
			pclFontf = CreateCellAttribute(pclFontf,"face=\"%s\"",pclCell);
			pclFontc = CreateCellAttribute(pclFontc,"color=\"%s\"",pclCell);
			pclFonts = CreateCellAttribute(pclFonts,"size=\"%s\"",pclCell);
			TagHtmlLine(FONT,pclCell,CLOSE_TAG);

			if (bgHead == TRUE)
			{
				pclField_Head = CreateCellAttribute(pclField_Head,"%s",pclCell);
			}
			else /* now the NO_TXT replacement is taken for the header-cell */
			{
				pclDummy = CreateCellAttribute(pclDummy,"%s",pclCell);
			}
			TagHtmlLine(FONT,pclCell,END_TAG);
			/* writing the cell layout AND text to the html-file */
			TagHtmlLine(TABLE_HEAD,pclCell,END_TAG);
		}	
		TagHtmlLine(TABLE_ROW,pclHeader,END_TAG);
	}
	else
	{
		ilRC = RC_FAIL;
	}
	return ilRC;
}

/* ******************************************************************** */
/* The CreateTableData() routine	  	        	        */
/*									*/
/*									*/
/*	CHANGE HISTORY							*/
/*		mos 18 Jan 2000 Check-In information processing		*/
/*			set linebufferpointer to first field		*/
/*				(field 0 is URNO)			*/
/*									*/
/*		mos up to Apr 6 several WAW implementations		*/
/*			calculate correct checkin information		*/
/*			create parking and towing page			*/
/*			insert CSF as new datarecord			*/
/*			insert empty datarecords			*/
/*		All implemetation just use the current linebuffer	*/
/*		and process the containing data				*/
/*		exemptions are the towing and parking pages, whose	*/
/*		linebuffers will be stored in a struct, sorted		*/
/*		and being written in ONE step to the file.		*/
/*		Unfortunately, the different functionalities are mostly	*/
/*		related to specific commands, so that they are hardcoded*/
/*									*/
/*									*/
/* ******************************************************************** */
static int CreateTableData()
{
	int 	ilRC 		= RC_SUCCESS;
	int 	ilItems 	= 0;
	int 	ilColor_1 	= 0;
	int 	ilColor_2 	= 1;
	int 	ilColor_Cnt 	= 0;
	int     ilRemark 	= 0;
	int	ilFieldPos 	= 0;
	char 	pclLineBuffer[MAX_BUFFER_LEN];
	char    pcldummybuffer[MAX_BUFFER_LEN];
	char    pclbgcolor1[MAX_BUFFER_LEN];
	char    pclbgcolor2[MAX_BUFFER_LEN];
	char 	*pclReadPointer = NULL;
	char 	*pclRowColor 	= NULL;
	
	/******* Changes by mos 18 Jan 00 	*******/
	char 	pclLineBuffer_withUrno[MAX_BUFFER_LEN];	/* datarecord with urno	*/
	char	pclUrno[12];				/* URNO from datarecord	*/
	int 	ilUrnoEndPosition = 0;			/* position of last urno character */
	/******* EO Changes			*******/
	
	
	
	if (bgTableData == TRUE)
	{
		memset(pclLineBuffer_withUrno,0x00,MAX_BUFFER_LEN);
		if ((pclReadPointer = fgets(pclLineBuffer_withUrno,MAX_BUFFER_LEN,pfgAsciiFile))==NULL)
		{
			dbg(DEBUG,"CreateTableData: No data found.");
			ilRC = RC_FAIL;
		}
		else
		{
			/******* Changes by mos 18 Jan 00 	*******/
			 /* reset buffer */
			memset(pclLineBuffer,0x00,MAX_BUFFER_LEN);
			memset(pclbgcolor1,0x00,MAX_BUFFER_LEN);
			memset(pclbgcolor2,0x00,MAX_BUFFER_LEN);
			memset(pclUrno,0x00,12);	

			 /* Extract Urno from valuelist */
			 /* get length of UNRNO Field and add 1 for data del*/
			 /* copy complete row from postion on into pcllinebuffer	*/
			CopyNextField2(&pclLineBuffer_withUrno[0],prgDynCfg->data_del,&pclUrno[0]);
			ilUrnoEndPosition = strlen(pclUrno)+1;
			strcpy(&pclLineBuffer[0],&pclLineBuffer_withUrno[ilUrnoEndPosition]);
			/******* EO Changes			*******/

			WriteComment("TABLEDATA");						
			
			/* Save the original row colors */
			strcpy(pclbgcolor1,prgDynCfg->td_bgc1);
			strcpy(pclbgcolor2,prgDynCfg->td_bgc2);
			
			
			while((pclReadPointer != NULL) && (strstr(pclReadPointer,prgDynCfg->eof_sign) == NULL) && ilRC == RC_SUCCESS)
			{
			
				if ((ilRC = RemoveEolSign(pclLineBuffer)) == RC_SUCCESS)
				{	
					/* if the line is not the EndOfFile line */
					if (strcmp(pclLineBuffer,prgDynCfg->eof_sign) != 0)
					{
						RemoveEolSign(pclLineBuffer_withUrno);
						
						/* Reset the row colors (necessary for update highlighting*/
						strcpy(prgDynCfg->td_bgc1,pclbgcolor1);
						strcpy(prgDynCfg->td_bgc2,pclbgcolor2);
						
						/* setting the row color */
						if (ilColor_Cnt == ilColor_1)
						{
							pclRowColor = prgDynCfg->td_bgc1;
							ilColor_1 = ilColor_1 + 2;
						}
						else
						{
							pclRowColor = prgDynCfg->td_bgc2;	
							ilColor_2 = ilColor_2 + 2;
						}
						
						/*** EOChanges ***/
	

						/* Check for last update time and change backgroundcolors if configured */
						HighlightUpdates(pclLineBuffer,pclUrno);

					
						/* REPLACE REMARK HH:MI with est time I*/
						/* get position from est time field in field list and extract value from linebuffer */
						memset(pcldummybuffer,0x00,MAX_BUFFER_LEN);
						for (ilFieldPos=1;ilFieldPos < igFieldCount;ilFieldPos++)
						{
							GetDataItem(pcldummybuffer, prgDynCfg->fields,ilFieldPos, CFG_DEL, "", " \0");
							StringUPR((UCHAR*)pcldummybuffer);
							if (strstr(pcldummybuffer, "ETOD") != NULL || 
								strstr(pcldummybuffer, "ETDI") != NULL || 
								strstr(pcldummybuffer, "ETOA") != NULL || 
								strstr(pcldummybuffer, "ETAI") != NULL)
							{
								break;
							}
						} /* for all fields */	
						memset(pcldummybuffer,0x00,MAX_BUFFER_LEN);
						GetDataItem(pcldummybuffer, pclLineBuffer,ilFieldPos, ucgData_del, "", " \0");
						
						ilRC= RC_SUCCESS;

                                                /*** changes by mos 28 Mai 2001 process towing page**/
                                                if ((ilRC = TOWINGProcessPage2(pclLineBuffer)) != RC_SUCCESS)
                                                {
                                                        dbg(DEBUG,"CreateTableData: Towing2 information processing failed.");
                                                }
                                                /*** EOChanges ***/
						
						if (bgFormatDate == TRUE)
						{
							/* Checking the dates and reformatting them if necessary */
							CheckDates(pclLineBuffer);
						}


						/* checking image-fields */
						if (bgCreateImgFields == TRUE)
						{
							FormatImageFields(pclLineBuffer);
						}
						if (bgResolv == TRUE)
						{
							ResolvCfgFields(pclLineBuffer);
						}

						/* REPLACE REMARK HH:MI with est time II*/
						ReplaceEstTime(pcldummybuffer,pclLineBuffer);
						
						
						/* copy buffer with formated dates and imagefields into buffer with urno */						
						strcpy(&pclLineBuffer_withUrno[ilUrnoEndPosition],&pclLineBuffer[0]);

                                                /*** changes by mos 03 Feb 2000 process towing page**/
                                                if ((ilRC = TOWINGProcessPage(pclLineBuffer)) != RC_SUCCESS)
                                                {
                                                        dbg(DEBUG,"CreateTableData: Towing information processing failed.");
                                                }
                                                /*** EOChanges ***/
						
						
						/*** changes by mos 07 Feb 2000 checkin page creation**/
						if ((ilRC = CHECKINProcessPage(pclLineBuffer_withUrno,pclLineBuffer, pclUrno)) != RC_SUCCESS)
						{
							dbg(DEBUG,"CreateTableData: Counter page creation failed.");
						}
						/*** EOChanges ***/
						
						/******* Changes by mos 10 Feb 00: jfno field formatting 	*******/
						if (ilRC = CSFProcess(pclLineBuffer)!= RC_SUCCESS)
						{
							dbg(DEBUG,"CreateTableData: CSF processing failed.");	
						}						
						/******* EO Changes			*******/

						
						/******* Changes by mos 25 Jan 00 multiple row processing	*******/
						if (strstr(prgDynCfg->command, pcgParking) == NULL) 
						{
							if ((ilRC = ROWFill(pclLineBuffer_withUrno,pclUrno,pclReadPointer,&ilColor_1,&ilColor_2,&ilColor_Cnt)) != RC_SUCCESS)
							{
								dbg(DEBUG,"CreateTableData: Additional row creation failed.");
							}
						}/* avoid if parking section is active */
						
						/*** EOChanges ***/
						/* resetting the row color if fix row processing is active*/
						if (strstr(prgDynCfg->addrows, prgDynCfg->command) != NULL)
						{
							if ((ilColor_Cnt + 1) == ilColor_1)
							{
								pclRowColor = prgDynCfg->td_bgc1;
							}
							else
							{
								pclRowColor = prgDynCfg->td_bgc2;	
							}
						}
						
						/*** changes by mos 03 Feb 2000 process parking page**/
					    	strcpy(&pclLineBuffer_withUrno[ilUrnoEndPosition],&pclLineBuffer[0]);
						if ((ilRC = PARKINGProcessPage(pclLineBuffer_withUrno)) != RC_SUCCESS)
						{
		     					dbg(DEBUG,"PARKINGFillPage: Parking information processing failed.");
						}
					 	strcpy(pclLineBuffer,&pclLineBuffer_withUrno[ilUrnoEndPosition]);
					 	/*** changes by mos 19 Jan 2000 format checkin fields**/
						if ((ilRC = CHECKINProcessData(pclLineBuffer_withUrno,pclLineBuffer, pclUrno)) != RC_SUCCESS)
						{
							dbg(DEBUG,"CreateTableData: Check in information processing failed.");
						}
	

						/* lines are skipped, if their Nr. of items doesn't match 	*/
						/* with the Nr. of fields or if the conversion to HTML fails 	*/
						/* rite, if TOWING or parking section is active, because 	*/
						/* this data has to be sorted first and then written		*/
						if ((strstr(prgDynCfg->command, pcgTowing) == NULL) && (strstr(prgDynCfg->command, pcgParking) == NULL)) 
						{
							if ((ilItems =
								GetNoOfElements(pclLineBuffer,ucgData_del)-1)
								== igFieldCount)
							{
								if ((ilRC = ConvertToTableData(pclLineBuffer,pclRowColor))
									!= RC_SUCCESS)
								{
									dbg(DEBUG,"CreateTableData: Convert failed! Skip line.");	
								}
								igROWCount++;
							}
							else
							{
								dbg(DEBUG,"CreateTableData: Nr.Items(%d) != Nr.Fields(%d)."
												" Skip line.",ilItems,igFieldCount);	
							}	
						} /* don't write, if TOWING or parking section is active*/
						
					  /******* Changes by mos 25 Jan 00 CSF processing	*******/
						if (strstr(prgDynCfg->command, pcgParking) == NULL) 
						{
							/* Reset the row colors (necessary for update highlighting*/
							strcpy(prgDynCfg->td_bgc1,pclbgcolor1);
							strcpy(prgDynCfg->td_bgc2,pclbgcolor2);

							if ((ilRC = ROWCreate(pclLineBuffer_withUrno,pclUrno,pclReadPointer,&ilColor_1,&ilColor_2,&ilColor_Cnt)) != RC_SUCCESS)
							{
								dbg(DEBUG,"CreateTableData: Additional row creation failed.");
							}	
						} /* if parking is not active */
						/*** EOChanges ***/

			
						 /* reset buffers*/
						memset(pclLineBuffer,0x00,MAX_BUFFER_LEN);
						memset(pclLineBuffer_withUrno,0x00,MAX_BUFFER_LEN);
						memset(pclUrno,0x00,12);
						
						pclReadPointer = fgets(pclLineBuffer_withUrno,MAX_BUFFER_LEN,pfgAsciiFile);
						
						
						 /* Extract Urno from valuelist */
						 /* get length of UNRNO Field and add 1 for data del*/
						 /* copy complete row from postion on into pcllinebuffer	*/
						CopyNextField2(&pclLineBuffer_withUrno[0],prgDynCfg->data_del,&pclUrno[0]);
						ilUrnoEndPosition = strlen(pclUrno)+1;
						strcpy(&pclLineBuffer[0],&pclLineBuffer_withUrno[ilUrnoEndPosition]);
						/******* EO Changes			*******/

					}
					else
					{
						pclReadPointer = NULL;
					}
				} /* if */
				ilColor_Cnt++;
			} /* while */
			
			/******* Changes by mos 10 Feb 00 	*******/
			/* create page with empty lines if configured, even if there's no data available */				
			/* setting the row color */				
			if (ilColor_Cnt == ilColor_1)
			{
				pclRowColor = prgDynCfg->td_bgc1;
				ilColor_1 = ilColor_1 + 2;
			}
			else
			{
				pclRowColor = prgDynCfg->td_bgc2;	
				ilColor_2 = ilColor_2 + 2;
			}
			if (strstr(prgDynCfg->command, pcgParking) == NULL) {
			if ((ilRC = ROWFill(pclLineBuffer_withUrno,pclUrno,pclReadPointer,&ilColor_1,&ilColor_2,&ilColor_Cnt)) != RC_SUCCESS)
			{
				dbg(DEBUG,"CreateTableData: Row creation failed.");
			}
			}
			/*** EOChanges ***/

			/***** changes by MOS for TOWING and parking page 	*/
			/* sort all data and write it in one step		*/
			if ((strstr(prgDynCfg->command, pcgTowing) !=NULL) || (strstr(prgDynCfg->command, pcgParking) !=NULL))
			{
			  FillPage();
			  pfgPAGE.iNoofRows=0;
			}
			/*** EOChanges ***/
			
		} /* if */
	}
	else
	{
		ilRC = RC_FAIL;
	}
	return ilRC;
}
/* ******************************************************************** */
/* The CHECKINProcessPage(char *pcpBuffer, char *pcpUrno) routine       */
/*									*/
/*	This process generates the correct CHECKIN information  	*/
/*	It looks for data records according to the counter type		*/
/*	if the type is 'C' the airline from alttab will be shown,	*/
/*	if not, the flight from afttab will be shown			*/
/*									*/
/* ******************************************************************** */
static int CHECKINProcessPage(char *pclLineBuffer_withurno, char *pcpBuffer, char *pcpUrno)
{
	int 	ilRC 		= RC_SUCCESS;
	int	ilCnt;
	int	ilPos		= 0;
	short 	slCursor 	= 0;		/* Cursor for SQL call		*/
	short 	slSqlFunc;			/* Type of SQl Call		*/
	char 	pclSqlBuf[200];			/* Buffer for SQl statements 	*/
	char 	pclDataArea[MAX_BUFFER_LEN];	/* Buffer for retrieved data	*/
	char 	pclOraErrorMsg[MAX_BUFFER_LEN]; /* Buffer for error message from SQl call */
	char 	pclhelpbuffer[MAX_BUFFER_LEN]; 
	char	pcldummy[100];
	char	pcldata_flight[20];		/* data from alt- or afttab according to type */
	char	pcldata_dest[10];		/* related data from afttab */
	char	pcldata_via[10];
	char	pcldata_std[30];
	char	pcldata_etd[30];
	char 	pclctyp[100];			/* counter tyoe from actual line buffer */
	char	pclckic[100];			/* counter name */
	char	pclflnu[100];
	char	pclckbs[100];			/* check-in begin */
	char	pclckes[100];			/* check-in end */
	char	pcldisp[100];			/* remarks */
	char	pclformat[100];			/* format string for sprintf call */
	BOOL	blerror = FALSE;		/* tag for oracle error processing */
	
	/** Is CHECKIN Section active?? If not leave function. 	**/	
	if ((strstr(prgDynCfg->command, pcgCheckin) == NULL) && (strstr(prgDynCfg->command, pcgDomCheckin) == NULL))
	{
		return ilRC;
	} /* if (strstr(prgDynCfg->command, pcgCheckin) */
	
	/* get position from ckif field in linebuffer list and get length of actual value*/
	memset(pcldummy,0x00,100);
	memset(pclformat,0x00,100);
			
	/* copy values from linebuffer */
	GetDataItem(pclckic, pcpBuffer, 1, ucgData_del, "", " \0");
	GetDataItem(pclflnu, pcpBuffer, 2, ucgData_del, "", " \0");
	GetDataItem(pclctyp, pcpBuffer, 3, ucgData_del, "", " \0");
	GetDataItem(pclckbs, pcpBuffer, 4, ucgData_del, "", " \0");
	GetDataItem(pclckes, pcpBuffer, 5, ucgData_del, "", " \0");
	GetDataItem(pcldisp, pcpBuffer, 9, ucgData_del, "", " \0");
	
	
	/** Select values from afttab for further processing		**/
	/* reset buffers */
	memset(pclSqlBuf,0x00,200);
	memset(pclDataArea,0x00,MAX_BUFFER_LEN);
	memset(pclOraErrorMsg,0x00,MAX_BUFFER_LEN);	
	memset(pcldata_dest,0x00,10);
	memset(pcldata_via,0x00,10);
	memset(pcldata_std,0x00,30);	
	memset(pcldata_etd,0x00,30);	
	memset(pcldata_flight,0x00,20);
			
	/* build sql statement for id of rotation flight */
	if (strstr(pclctyp, "C") == NULL)
	{
		sprintf(pclSqlBuf,"select flno,des3,via3,stod,etdi,ttyp,jfno from AFTTAB where hopo='%s' AND URNO='%s'",pcgDefHomeAp,pclflnu);
	}
	else
	{	
		sprintf(pclSqlBuf,"select alc2 from ALTTAB where hopo='%s' AND URNO='%s'",pcgDefHomeAp,pclflnu);
	} /* if (strstr(pclckic, "C") */

	/* set type of sql call */
	slSqlFunc = START;

	/* run sql */
	if ((ilRC = sql_if(slSqlFunc,&slCursor,pclSqlBuf,pclDataArea))!= RC_SUCCESS) 
	{
		if (ilRC == NOTFOUND)
		{
			ilRC = RC_SUCCESS;
			blerror == TRUE;
		}
		else
		{
			ilRC = RC_FAIL;
			get_ora_err(ilRC,pclOraErrorMsg);	
			dbg(TRACE,"CHECKINProcessPage: ORA-ERR <%s>  <%s>",pclOraErrorMsg, pclSqlBuf);
			blerror = TRUE;
		}
	} /* if ((ilRC = sql_if */


	/* close cursor */
	close_my_cursor(&slCursor);
		
	/* replace values in linebuffer with rotation flight values */
	memset(pclhelpbuffer,0x00,MAX_BUFFER_LEN);

	/* copy sql values */
	if ((strstr(pclctyp, "C") == NULL) && (strstr(pclctyp, "c") == NULL) && (blerror == FALSE))
	{
		GetDataItem(pcldummy, pclDataArea, 6, '\0', "", "::");
		/* if nature 13, then show CSF */
		if (strstr(pcldummy, "13") != NULL)
		{			
			GetDataItem(pcldata_flight, pclDataArea, 7, '\0', "", " \0");
			strcpy(prgDynCfg->fields,"ckic;jfno;ckbs;ckes;des3;via3;stod;etdi;disp");
		} 
		else
		{		
			GetDataItem(pcldata_flight, pclDataArea, 1, '\0', "", "  ");
			strcpy(prgDynCfg->fields,"ckic;flno;ckbs;ckes;des3;via3;stod;etdi;disp");
		}
		GetDataItem(pcldata_dest, pclDataArea, 2, '\0', "", " \0");
		GetDataItem(pcldata_via, pclDataArea, 3, '\0', "", " \0");
		GetDataItem(pcldata_std, pclDataArea, 4, '\0', "", " \0");
		GetDataItem(pcldata_etd, pclDataArea, 5, '\0', "", " \0");
	}
	else
	{	
		if (blerror == FALSE)
		{
			GetDataItem(pcldata_flight, pclDataArea, 1, "\0", "", " \0");
		}

	} /* if (strstr(pclckic, "C") */

	/* change utc to local time */
	if (strstr(pcldata_std,"20") != NULL)
        {
                UtcToLocal(pcldata_std);
        }
        if (strstr(pcldata_etd,"20") != NULL )
        {
                UtcToLocal(pcldata_etd);
        }
	
	
	if (blerror == TRUE)	
	{
		strcpy(pclformat,"%s|");
		strcat(pclformat,prgDynCfg->fixrowformat);
		strcat(pclformat,"|||||||||");
		sprintf(pclhelpbuffer,pclformat,pcpUrno,atoi(pclckic));

	} else {
		/* build new linebuffer */
		strcpy(pclformat,"%s|");
		if (strlen(prgDynCfg->fixrowformat)>1) {
			strcat(pclformat,prgDynCfg->fixrowformat);
		} else {
			strcat(pclformat,"%s");
		}
		strcat(pclformat,"|%s|%s|%s|%s|%s|%s|%s|%s|");
		
		if (strlen(prgDynCfg->fixrowformat)>1) {
			sprintf(pclhelpbuffer,pclformat,pcpUrno	,atoi(pclckic),pcldata_flight,pclckbs,pclckes,pcldata_dest,pcldata_via,pcldata_std,pcldata_etd,pcldisp);
		} else {
                        sprintf(pclhelpbuffer,pclformat,pcpUrno ,pclckic,pcldata_flight,pclckbs,pclckes,pcldata_dest,pcldata_via,pcldata_std,pcldata_etd,pcldisp);
		}


	} /* has oracle error occured? */

	/* format date fields */	
	if ((bgFormatDate == TRUE) && (blerror == FALSE))
	{
		/* Checking the dates and reformatting them if necessary */
		CheckDates(&pclhelpbuffer[strlen(pcpUrno) + 1]);
	}
	
	if (bgResolv == TRUE)						
	{       						
	    ResolvCfgFields(&pclhelpbuffer[strlen(pcpUrno) + 1]);						
	}
	
	dbg(DEBUG,"CHECKINProcessPage: pclhelpbuffer: %s",pclhelpbuffer);
	
	/* fill old linebuffers */
	strcpy(pcpBuffer,&pclhelpbuffer[strlen(pcpUrno) + 1]);
	strcpy(pclLineBuffer_withurno,pclhelpbuffer);

	return ilRC;
	
} /* CHECKINProcessPage */


/* ******************************************************************** */
/* The FillPage routine       						*/
/*									*/
/*	This process fills the in the struct pfgPAGE			*/
/*	stored values into the HTML file after having sorted them 	*/
/*									*/
/* ******************************************************************** */
static int FillPage()
{
	int 	ilRC 		= RC_SUCCESS;
	int	ilCnt;
	int     ilSort1         = 0;
	int     ilSort2         = 0;
	int	ilRemark	= 0;
	char    pclsortbuffer[MAX_BUFFER_LEN];
	int 	ilColor_1       = 0;		/* variables for html row color processing */
	int 	ilColor_2       = 1;
	int 	ilColor_Cnt     = 0;
	char 	*pclRowColor 	= NULL;		/* color of htmhl table row */
	char	pclurno[20];
	char    pcldummy1[MAX_BUFFER_LEN];
	char    pcldummybuffer[MAX_BUFFER_LEN];
	BOOL	blHANGA = FALSE;


	/* sort the values with time of arrival */ 
        for (ilSort1=0; ilSort1<pfgPAGE.iNoofRows-1; ilSort1++)
	{
  		   for (ilSort2=ilSort1+1; ilSort2<pfgPAGE.iNoofRows; ilSort2++)
		   { 
   			if(strcmp(pfgPAGE.prRow[ilSort1].pcpDate,pfgPAGE.prRow[ilSort2].pcpDate)>0)
   			{
   				strcpy(pclsortbuffer,pfgPAGE.prRow[ilSort1].pcpDate);
    				strcpy(pfgPAGE.prRow[ilSort1].pcpDate,pfgPAGE.prRow[ilSort2].pcpDate);
    				strcpy(pfgPAGE.prRow[ilSort2].pcpDate,pclsortbuffer);
   				strcpy(pclsortbuffer,pfgPAGE.prRow[ilSort1].pcpRows);
    				strcpy(pfgPAGE.prRow[ilSort1].pcpRows,pfgPAGE.prRow[ilSort2].pcpRows);
    				strcpy(pfgPAGE.prRow[ilSort2].pcpRows,pclsortbuffer);
   			} /*if */
		   } /* for*/
	} /* sort end */

        /* if parking is active */
        if (strstr(prgDynCfg->command, pcgParking) != NULL)
	{
		strcpy(pfgPAGE.prRow[pfgPAGE.iNoofRows].pcpRows,prgDynCfg->eof_sign);
		pfgPAGE.iNoofRows++;
	}

	/* format and write values in struct */
	for (ilCnt=0;ilCnt<pfgPAGE.iNoofRows;ilCnt++)
	{

	       /*do all formatting and write data to file */
	       /* setting the row color */
	       if (ilColor_Cnt == ilColor_1)
	       {
		   pclRowColor = prgDynCfg->td_bgc1;
		   ilColor_1 = ilColor_1 + 2;
	       }
	       else
	       {
		   pclRowColor = prgDynCfg->td_bgc2;
		   ilColor_2 = ilColor_2 + 2;
	       } /* setting the row color */


	       /* Write data*/
	       if (bgFormatDate == TRUE)
	       {
		   /* Checking the dates and reformatting them if necessary */
		   CheckDates(pfgPAGE.prRow[ilCnt].pcpRows);
	       }
	       dbg(DEBUG,"FillPage: : %s    at %d",pfgPAGE.prRow[ilCnt].pcpRows,ilCnt);
	       dbg(DEBUG,"FillPage: : %d    %d    %d",ilColor_1,ilColor_2,ilColor_Cnt);
	       dbg(DEBUG,"FillPage: : %s    at %d",pfgPAGE.prRow[ilCnt].pcpDate,ilCnt);

	       /* checking image-fields */
	       if (bgCreateImgFields == TRUE)
	       {
		   FormatImageFields(pfgPAGE.prRow[ilCnt].pcpRows);
	       }
	       
	       /* if parking is active */
	       if (strstr(prgDynCfg->command, pcgParking) != NULL)
	       {
	       		/* extract urno and save remaining string in buffer */
       			GetDataItem(pclurno, pfgPAGE.prRow[ilCnt].pcpRows,1, ucgData_del, "", " \0");
	       
	       		/*******multiple row processing	*******/
        		if ((ilRC = ROWFill(pfgPAGE.prRow[ilCnt].pcpRows,pclurno,pfgPAGE.prRow[ilCnt].pcpRows,&ilColor_1,&ilColor_2,&ilColor_Cnt)) != RC_SUCCESS)
			{
				dbg(DEBUG,"FillPage: Additional row creation failed.");
			}
       			strcpy(pcldummy1,pfgPAGE.prRow[ilCnt].pcpRows);
                        /*** EOChanges ***/
                        /* resetting the row color if fix row processing is active*/
                        if ((ilColor_Cnt + 1) == ilColor_1)
                        {
                               pclRowColor = prgDynCfg->td_bgc1;
                        }
                        else
                        {
                             pclRowColor = prgDynCfg->td_bgc2;
                        }
			
			/* is HANGA position in buffer */
			if (strstr(pcldummy1,"HANGA")!=NULL) {blHANGA=TRUE;}
			
			/* WAW-special: filter full REMARK text */
			/*eg: 'ODLOTY HH:MM' -> 'ODLOTY'	*/						
			if (strstr(pcldummy1,"HH:MM")!=NULL)
			{
                memset(pcldummybuffer,0x00,MAX_BUFFER_LEN); 
                ilRemark = strlen(pcldummy1)-11;  
				strncpy(pcldummybuffer,pcldummy1,ilRemark);
			 	strcat(pcldummybuffer,"|");
				strcpy(pcldummy1,pcldummybuffer);
			}
			
			if (strstr(pcldummy1,prgDynCfg->eof_sign)==NULL)
			{
		       		if ((ilRC = ConvertToTableData(&pcldummy1[strlen(pclurno)+1],pclRowColor))!= RC_SUCCESS)
	       			{
				   dbg(DEBUG,"FillPage: Creation of rows failed with value: %s.",pfgPAGE.prRow[ilCnt].pcpRows);	
	       			}
			} /*if not EOF */
		} else {
	       		if ((ilRC = ConvertToTableData(pfgPAGE.prRow[ilCnt].pcpRows,pclRowColor))!= RC_SUCCESS)
	       		{
			   dbg(DEBUG,"FillPage: Creation of rows failed with value: %s.",pfgPAGE.prRow[ilCnt].pcpRows);	
	       		}			
		}
	       ilColor_Cnt++;

	       /* reset memory */
	       memset(pfgPAGE.prRow[ilCnt].pcpRows,0x00,MAX_BUFFER_LEN);
	       memset(pfgPAGE.prRow[ilCnt].pcpDate,0x00,20);

	} /* write all values */

	/* write last Hanga line if no value is available*/
	if ((blHANGA==FALSE) && (strstr(prgDynCfg->command, pcgParking) != NULL) && (strstr(pcgDefHomeAp, "WAW") != NULL))
	{
		strcpy(pcldummy1,"HANGA|||||||||");
		if ((ilRC = ConvertToTableData(pcldummy1,pclRowColor))!= RC_SUCCESS)
                        {
                           dbg(DEBUG,"FillPage: Creation of rows failed with value: %s.",pfgPAGE.prRow[ilCnt].pcpRows);
                        }
	}
	return ilRC;

} /* FillPage */

/* ******************************************************************** */
/* The TOWINGProcessPage(char *pcpBuffer) routine       		*/
/*									*/
/*	This process generates the correct TOWING information  		*/
/*	It looks for rotation flights via the rkey field in the afttab	*/
/*	It also inserts the towing positions 2-4 if existent		*/
/*									*/
/* ******************************************************************** */
static int TOWINGProcessPage(char *pcpBuffer)
{
	int 	ilRC 		= RC_SUCCESS;
	int	ilCnt;
	int	ilCnt2;
	int     ilCnt3          = 0;
	short 	slCursor 	= 0;		/* Cursor for SQL call		*/
	short 	slSqlFunc;			/* Type of SQl Call		*/
	char 	pclSqlBuf[600];			/* Buffer for SQl statements 	*/
	char 	pclDataArea[MAX_BUFFER_LEN];	/* Buffer for retrieved data	*/
	char 	pclOraErrorMsg[MAX_BUFFER_LEN]; /* Buffer for error message from SQl call */
	char 	pclhelpbuffer[MAX_BUFFER_LEN]; 
	char	pclflno[20];
	char	pclrkey[20];
	char	pclpos2[20];
	char	pclpos3[20];
	char	pclpos4[20];
	char    pcldatedummy[20];               /* buffer for the arrival date to show*/
	char    pcldatedummy2[20];               /* buffer for the departure date to show*/
	int	ilNoofTowing	= 0;		/* amount of towing records */
	int	ilTowingstart	= 0;		/* pos of first towing record in structure */
	BOOL	blarrflight 	= FALSE;
	BOOL	bldepflight 	= FALSE;
	TOWING_STRUCT rlTOWING;			/* struct for twoing data */	


	/** Is TOWING Section active?? If not leave function. 	**/	
	if (strstr(prgDynCfg->command, pcgTowing) == NULL)
	{
		return ilRC;
	} /* if (strstr(prgDynCfg->command, pcgTowing) */


	/* reset buffers */
	memset(pclSqlBuf,0x00,600);
	memset(pclDataArea,0x00,MAX_BUFFER_LEN);
	memset(pclOraErrorMsg,0x00,MAX_BUFFER_LEN);	
	memset(pclhelpbuffer,0x00,MAX_BUFFER_LEN);	
	memset(pclrkey,0x00,20);
	memset(pclflno,0x00,20);
	memset(pclpos2,0x00,20);
	memset(pclpos3,0x00,20);
	memset(pclpos4,0x00,20);
		
	/* check for the last Towing flight */
	GetDataItem(pclflno, pcpBuffer,1, ucgData_del, "", " \0");
	if (strlen(pfgTOWINGFlno) <= 0)
	{
		strcpy(pfgTOWINGFlno,pclflno);
	} else {
		if (strstr(pfgTOWINGFlno,pclflno) == NULL) 
		{
			strcpy(pfgTOWINGFlno,pclflno);
		} else {

			dbg(DEBUG,"FLNO SIMILAR: %s  %s",pfgTOWINGFlno,pclflno);
		
			return ilRC;
		}
	} /* check flno */
	
	/****** get all values with the existing rkey and also the number of towing records */
	/* build sql statement for id of rotation flight */		
	/* SYSDATE function can remain because daylight time and normal time difference doesn't play a role in a 1day time
	frame*/
	GetDataItem(pclrkey, pcpBuffer,2, ucgData_del, "", " \0");
dbg(TRACE," RKEY = <%s> for buffer <%s>",pclrkey,pcpBuffer);						
	sprintf(pclSqlBuf,"select flno,act3,regn,stoa,stod,land,airb,psta,pstd,onbl,ofbl, \
			ftyp,etai,etdi from AFTTAB where hopo='%s' and ((rkey='%s' and ftyp='T' and  stod<TO_CHAR(SYSDATE+1,'YYYYMMDDHH24MISS')) \
 			or (rkey='%s' and adid='A' and ftyp='O') or \
 			(rkey='%s' and adid='D' and ftyp='O' and stod<TO_CHAR(SYSDATE+1,'YYYYMMDDHH24MISS'))) \
 			order by ftyp,flno,stoa",pcgDefHomeAp,pclrkey,pclrkey,pclrkey);
	
	/* set type of sql call */
	slSqlFunc = START;

	/* run sql */
	while ((ilRC = sql_if(slSqlFunc,&slCursor,pclSqlBuf,pclDataArea))== RC_SUCCESS) 
	{	
		/* write values in structure	*/
		GetDataItem(rlTOWING.prRow[ilNoofTowing].pcpflno, pclDataArea, 1, '\0', "", " \0");
		GetDataItem(rlTOWING.prRow[ilNoofTowing].pcpact3, pclDataArea, 2, '\0', "", " \0");
		GetDataItem(rlTOWING.prRow[ilNoofTowing].pcpregn, pclDataArea, 3, '\0', "", " \0");
		GetDataItem(rlTOWING.prRow[ilNoofTowing].pcpstoa, pclDataArea, 4, '\0', "", " \0");
		GetDataItem(rlTOWING.prRow[ilNoofTowing].pcpstod, pclDataArea, 5, '\0', "", " \0");
		GetDataItem(rlTOWING.prRow[ilNoofTowing].pcpland, pclDataArea, 6, '\0', "", " \0");
		GetDataItem(rlTOWING.prRow[ilNoofTowing].pcpairb, pclDataArea, 7, '\0', "", " \0");
		GetDataItem(rlTOWING.prRow[ilNoofTowing].pcppsta, pclDataArea, 8, '\0', "", " \0");
		GetDataItem(rlTOWING.prRow[ilNoofTowing].pcppstd, pclDataArea, 9, '\0', "", " \0");
		GetDataItem(rlTOWING.prRow[ilNoofTowing].pcponbl, pclDataArea, 10, '\0', "", " \0");
		GetDataItem(rlTOWING.prRow[ilNoofTowing].pcpofbl, pclDataArea, 11, '\0', "", " \0");
		GetDataItem(rlTOWING.prRow[ilNoofTowing].pcpftyp, pclDataArea, 12, '\0', "", " \0");
		GetDataItem(rlTOWING.prRow[ilNoofTowing].pcpetai, pclDataArea, 13, '\0', "", " \0");
		GetDataItem(rlTOWING.prRow[ilNoofTowing].pcpetdi, pclDataArea, 14, '\0', "", " \0");
		
		/* get position of first towing record in structure */
		if (strcmp(rlTOWING.prRow[ilNoofTowing].pcpftyp,"T") != 0) {ilTowingstart++;}

		/* set row flag */
		ilNoofTowing++;

		/* set type of sql call */
		slSqlFunc = NEXT;
	} /* while */

	if (ilRC == NOTFOUND)
	{
		  ilRC = RC_SUCCESS;
	}
	else
	{
		  ilRC = RC_FAIL;
		  get_ora_err(ilRC,pclOraErrorMsg);	
		  dbg(TRACE,"TOWINGProcessPagePage: ORA-ERR <%s>  <%s>",pclOraErrorMsg, pclSqlBuf);
		  /* close cursor */
		  close_my_cursor(&slCursor);
		  return ilRC;
	}
	
	/* close cursor */
	close_my_cursor(&slCursor);
	
	/* write arrival flight into buffer */
	for (ilCnt = 0; ilCnt <  ilNoofTowing; ilCnt++)
	{
		if ((strcmp(rlTOWING.prRow[ilCnt].pcpftyp,"O")==0) && (strlen(rlTOWING.prRow[ilCnt].pcppstd)==0))
		{
				/* change utc to local time */
				if (strstr(rlTOWING.prRow[ilTowingstart].pcpstod,"20") != NULL)
				{
					UtcToLocal(rlTOWING.prRow[ilTowingstart].pcpstod);
				}
				/* get the right date to show:  OBL,ATA,ETA,STA */
				if (strstr(rlTOWING.prRow[ilCnt].pcponbl,"20") != NULL)
				{
					strcpy(pcldatedummy,rlTOWING.prRow[ilCnt].pcponbl);

				} else if (strstr(rlTOWING.prRow[ilCnt].pcpland,"20") != NULL)
				{
					strcpy(pcldatedummy,rlTOWING.prRow[ilCnt].pcpland);

				} else if (strstr(rlTOWING.prRow[ilCnt].pcpetai,"20") != NULL)
				{
					strcpy(pcldatedummy,rlTOWING.prRow[ilCnt].pcpetai);

				} else {
					strcpy(pcldatedummy,rlTOWING.prRow[ilCnt].pcpstoa);
				}

				UtcToLocal(pcldatedummy);

			sprintf(pclhelpbuffer,"%s|%s|%s|%s|%s|%s",rlTOWING.prRow[ilCnt].pcpflno,rlTOWING.prRow[ilCnt].pcpact3, \
					rlTOWING.prRow[ilCnt].pcpregn, pcldatedummy, \
					rlTOWING.prRow[ilCnt].pcppsta,rlTOWING.prRow[ilTowingstart].pcpstod);		
			
			blarrflight = TRUE;
			break; 
		}
	} /* check for the arrival flight */
	if (blarrflight == FALSE)
	{
	    sprintf(pclhelpbuffer,"|||||");
	}



	/* check for departure flight */
	for (ilCnt = 0; ilCnt <  ilNoofTowing; ilCnt++)
	{
		if ((strcmp(rlTOWING.prRow[ilCnt].pcpftyp,"O")==0) && (strlen(rlTOWING.prRow[ilCnt].pcppsta)==0))
		{
			bldepflight =TRUE;			
			/*	dbg(TRACE,"\n\n DEPARTURE DETECTED at %d",ilCnt);*/
			break;
		}
	} /* check for the departure flight */

	
	if (bldepflight == TRUE)
	{
		 /* get the right date to show:  OBL,ATA,ETA,STA */
	      	 if (strstr(rlTOWING.prRow[ilCnt].pcpairb,"20") != NULL)
		{
			strcpy(pcldatedummy2,rlTOWING.prRow[ilCnt].pcpairb);

		} else if (strstr(rlTOWING.prRow[ilCnt].pcpofbl,"20") != NULL)
		{
			strcpy(pcldatedummy2,rlTOWING.prRow[ilCnt].pcpofbl);

		} else if (strstr(rlTOWING.prRow[ilCnt].pcpetdi,"20") != NULL)
		{
			strcpy(pcldatedummy2,rlTOWING.prRow[ilCnt].pcpetdi);

		} else {
			strcpy(pcldatedummy2,rlTOWING.prRow[ilCnt].pcpstod);
		}

	        UtcToLocal(pcldatedummy2);

		/* if only one towing flight, then insert just stod from this flight */
		if ((ilNoofTowing - ilTowingstart) == 1)			
		{
				if (strstr(rlTOWING.prRow[ilTowingstart].pcpstoa,"20") != NULL)
				{
					UtcToLocal(rlTOWING.prRow[ilTowingstart].pcpstoa);
				}

			sprintf(pclhelpbuffer,"%s||||%s|%s|%s|%s|",pclhelpbuffer,rlTOWING.prRow[ilTowingstart].pcpstoa,\
					rlTOWING.prRow[ilCnt].pcppstd,pcldatedummy2,rlTOWING.prRow[ilCnt].pcpflno);
		} else {
			for (ilCnt2 = ilTowingstart;ilCnt2 < (ilNoofTowing - 1);ilCnt2++)
			{
			        if (ilCnt3 > 2) {break;}
				strcat(pclhelpbuffer,"|");
				if (strlen(rlTOWING.prRow[ilCnt2].pcppsta) > 0)
				{
					strcat(pclhelpbuffer,rlTOWING.prRow[ilCnt2].pcppsta);
				}
				ilCnt3++;
			}
			if (ilCnt3 < 3) { strcat(pclhelpbuffer,"|");ilCnt3++;}
			if (ilCnt3 < 3) { strcat(pclhelpbuffer,"|");ilCnt3++;}
			if (ilCnt3 < 3) { strcat(pclhelpbuffer,"|");ilCnt3++;}
			if (ilCnt3 < 3) { strcat(pclhelpbuffer,"|");ilCnt3++;}

			if (strstr(rlTOWING.prRow[ilCnt2].pcpstoa,"20") != NULL)
			{
				UtcToLocal(rlTOWING.prRow[ilCnt2].pcpstoa);
			}

			sprintf(pclhelpbuffer,"%s|%s|%s|%s|%s|",pclhelpbuffer,rlTOWING.prRow[ilCnt2].pcpstoa,\
					rlTOWING.prRow[ilCnt].pcppstd,pcldatedummy2,rlTOWING.prRow[ilCnt].pcpflno);
				/*	dbg(TRACE,"\n\n DEPARTURE II BUFFER:    %s",pclhelpbuffer);			*/
		}
	} else {
		for (ilCnt2 = 1;ilCnt2 < ilNoofTowing;ilCnt2++)
		{
		        if (ilCnt3 > 2) {break;}
			strcat(pclhelpbuffer,"|");
			if (strlen(rlTOWING.prRow[ilCnt2].pcppsta) > 0)
			{
					strcat(pclhelpbuffer,rlTOWING.prRow[ilCnt2].pcppsta);
			}
			ilCnt3++;
		}
	        if (ilCnt3 < 3) { strcat(pclhelpbuffer,"|");ilCnt3++;}
	       	if (ilCnt3 < 3) { strcat(pclhelpbuffer,"|");ilCnt3++;}
	       	if (ilCnt3 < 3) { strcat(pclhelpbuffer,"|");ilCnt3++;}
	       	if (ilCnt3 < 3) { strcat(pclhelpbuffer,"|");ilCnt3++;}
	
		strcat(pclhelpbuffer,"|||||");
	}
	
        /* save buffer in page struct */
        strcpy(pfgPAGE.prRow[pfgPAGE.iNoofRows].pcpRows,pclhelpbuffer);
        strcpy(pfgPAGE.prRow[pfgPAGE.iNoofRows].pcpDate,pcldatedummy);        
        pfgPAGE.iNoofRows++;
		
	return ilRC;
} /* TOWINGProcessPagePage */


/* ******************************************************************** */
/* The TOWINGProcessPage(char *pcpBuffer) routine       		*/
/*									*/
/*	This process generates the correct TOWING information  		*/
/*	It looks for rotation flights via the rkey field in the afttab	*/
/*	Separate rows will show each towing activity			*/
/*									*/
/* ******************************************************************** */
static int TOWINGProcessPage2(char *pcpBuffer)
{
	int 	ilRC 		= RC_SUCCESS;
	int	ilCnt;
	short 	slCursor 	= 0;		/* Cursor for SQL call		*/
	short 	slSqlFunc;			/* Type of SQl Call		*/
	char 	pclSqlBuf[MAX_BUFFER_LEN];			/* Buffer for SQl statements 	*/
	char 	pclDataArea[MAX_BUFFER_LEN];	/* Buffer for retrieved data	*/
	char 	pclOraErrorMsg[MAX_BUFFER_LEN]; /* Buffer for error message from SQl call */
	char 	pclhelpbuffer[MAX_BUFFER_LEN]; 
	char	pclrkey[XS_BUFF];
	char	pclflno[XS_BUFF];
	char	pclstod[XS_BUFF];

	/** Is TOWING Section active?? If not leave function. 	**/	
	if (strstr(prgDynCfg->command, pcgTowing2) == NULL)
	{
		return ilRC;
	} /* if (strstr(prgDynCfg->command, pcgTowing) */


	/* reset buffers */
	memset(pclSqlBuf,0x00,MAX_BUFFER_LEN);
	memset(pclDataArea,0x00,MAX_BUFFER_LEN);
	memset(pclOraErrorMsg,0x00,MAX_BUFFER_LEN);	
	memset(pclhelpbuffer,0x00,MAX_BUFFER_LEN);	
	memset(pclrkey,0x00,XS_BUFF);
	
		
	/* retrieve the rkey from the buffer */
	GetDataItem(pclrkey, pcpBuffer,2, ucgData_del, "", " \0");

	/* Get values of arrival flight */	
	sprintf(pclSqlBuf,"select stoa from afttab where hopo='%s' and FTYP='O' and rkey='%s' and ADID='A'",pcgDefHomeAp,pclrkey);
	
	/* set type of sql call */
	slSqlFunc = START;

	/* run sql */
	if ((ilRC = sql_if(slSqlFunc,&slCursor,pclSqlBuf,pclDataArea))!= RC_SUCCESS) 
	{
		if (ilRC == NOTFOUND)
		{
			ilRC = RC_SUCCESS;
		}
		else
		{
			ilRC = RC_FAIL;
			get_ora_err(ilRC,pclOraErrorMsg);	
			dbg(TRACE,"TOWINGProcessPagePage2: ORA-ERR <%s>  <%s>",pclOraErrorMsg, pclSqlBuf);
		}
		/* close cursor */
		close_my_cursor(&slCursor);
		return ilRC;
	} /* if ((ilRC = sql_if */

	/* close cursor */
	close_my_cursor(&slCursor);

	/* copy value */
       	GetDataItem(pclhelpbuffer, pclDataArea, 1, '\0', "", " \0");
	
	/* Insert Value */
	if ((ilRC = SearchStringAndReplace(pcpBuffer,pclrkey,pclhelpbuffer))!= RC_SUCCESS)
	{
			dbg(DEBUG,"TOWINGProcessPagePage2: Search and replace rkey failed.");
	}



	/* Now retrieve FLNO and STOD from the departure flight */
	/* reset buffers */
	memset(pclSqlBuf,0x00,MAX_BUFFER_LEN);
	memset(pclDataArea,0x00,MAX_BUFFER_LEN);
	memset(pclOraErrorMsg,0x00,MAX_BUFFER_LEN);	
	memset(pclhelpbuffer,0x00,MAX_BUFFER_LEN);	
	memset(pclflno,0x00,XS_BUFF);	
	memset(pclstod,0x00,XS_BUFF);	

	/* Get values of arrival flight */	
	sprintf(pclSqlBuf,"select flno,stod from afttab where hopo='%s' and FTYP='O' and rkey='%s' and ADID='D'",pcgDefHomeAp,pclrkey);
	
	/* set type of sql call */
	slSqlFunc = START;

	/* run sql */
	if ((ilRC = sql_if(slSqlFunc,&slCursor,pclSqlBuf,pclDataArea))!= RC_SUCCESS) 
	{
		if (ilRC == NOTFOUND)
		{
			ilRC = RC_SUCCESS;
		}
		else
		{
			ilRC = RC_FAIL;
			get_ora_err(ilRC,pclOraErrorMsg);	
			dbg(TRACE,"TOWINGProcessPagePage2: ORA-ERR <%s>  <%s>",pclOraErrorMsg, pclSqlBuf);
		}
		/* close cursor */
		close_my_cursor(&slCursor);
		return ilRC;
	} else {
		/* copy value */
       		GetDataItem(pclflno, pclDataArea, 1, '\0', "", " \0");
       		GetDataItem(pclstod, pclDataArea, 2, '\0', "", " \0");
	
	}

	/* close cursor */
	close_my_cursor(&slCursor);


	/* Build buffer */	
	sprintf(pclhelpbuffer,"%s%s|%s|",pcpBuffer,pclflno,pclstod);

	/* Copy buffer */
	memset(pcpBuffer,0x00,MAX_BUFFER_LEN);
	sprintf(pcpBuffer,"%s",pclhelpbuffer);
		
	return ilRC;
} /* TOWINGProcessPagePage2 */

/* ******************************************************************** */
/* The PARKINGProcessPage(char *pcpBuffer, char *pcpUrno) routine       */
/*									*/
/*	This process generates the correct PARKING information  	*/
/*	It looks for rotation flights via the rkey field in the afttab	*/
/*	If this flight is not departed yet, towing activities are	*/
/*	analysed. If the aircraft has been towed, the last position	*/
/*	with onbl time will be shown as the aircraft's actual position	*/
/*									*/
/* ******************************************************************** */
static int PARKINGProcessPage(char *pclLineBuffer_withurno)
{
	int 	ilRC 		= RC_SUCCESS;
	int	ilCnt;
	int	ilPos		= 0;
	short 	slCursor 	= 0;		/* Cursor for SQL call		*/
	short 	slSqlFunc;			/* Type of SQl Call		*/
	char 	pclSqlBuf[200];			/* Buffer for SQl statements 	*/
	char 	pclDataArea[MAX_BUFFER_LEN];	/* Buffer for retrieved data	*/
	char 	pclOraErrorMsg[MAX_BUFFER_LEN]; /* Buffer for error message from SQl call */
	char 	pclhelpbuffer[MAX_BUFFER_LEN]; 
	char	pcldummy[MAX_BUFFER_LEN];
	char	pcldummy2[MAX_BUFFER_LEN];
	char    pclrkey[30];
	char	pcldata1[20];
	char	pcldata2[30];
	char	pcldata3[30];
	char	pcldata4[100];
	char	pcldatadep1[20];
	char	pcldatadep2[30];
	char	pcldatadep3[30];
	char	pcldatadep4[100];
	char    pclnewpos[30];
	char    pclpos[30];
	char    pcpUrno[30];
	char	pclonbl[20];
	BOOL    bldepflight = FALSE;	
	BOOL    bltowflight = FALSE;


	/** Is PARKING Section active?? If not leave function. 	**/	
	if (strstr(prgDynCfg->command, pcgParking) == NULL)
	{
		return ilRC;
	} /* if (strstr(prgDynCfg->command, pcgParking) */
	
	/** Select values from afttab for further processing		**/
	/* reset buffers */
	memset(pclSqlBuf,0x00,200);
	memset(pclDataArea,0x00,MAX_BUFFER_LEN);
	memset(pclOraErrorMsg,0x00,MAX_BUFFER_LEN);	
	memset(pcldata1,0x00,20);
	memset(pcldata2,0x00,30);
	memset(pclrkey,0x00,30);
	memset(pclnewpos,0x00,30);
	memset(pclpos,0x00,30);
	memset(pcldata3,0x00,30);
	memset(pcldata4,0x00,100);
	memset(pcpUrno,0x00,30);
	memset(pcldatadep1,0x00,20);
	memset(pcldatadep2,0x00,30);
	memset(pcldatadep3,0x00,30);
	memset(pcldatadep4,0x00,100);
	memset(pclonbl,0x00,20);
	
	/* get urno from linebuffer*/
	GetDataItem(pcpUrno, pclLineBuffer_withurno,1, ucgData_del, "", " \0");

	
	/* build sql statement for id of rotation flight */								
	sprintf(pclSqlBuf,"select rkey from AFTTAB where hopo='%s' and URNO='%s'",pcgDefHomeAp,pcpUrno);
	
	/* set type of sql call */
	slSqlFunc = START;

	/* run sql */
	if ((ilRC = sql_if(slSqlFunc,&slCursor,pclSqlBuf,pclDataArea))!= RC_SUCCESS) 
	{
		if (ilRC == NOTFOUND)
		{
			ilRC = RC_SUCCESS;
		}
		else
		{
			ilRC = RC_FAIL;
			get_ora_err(ilRC,pclOraErrorMsg);	
			dbg(TRACE,"PARKINGProcessPage: ORA-ERR <%s>  <%s>",pclOraErrorMsg, pclSqlBuf);
		}
		/* close cursor */
		close_my_cursor(&slCursor);
		return ilRC;
	} /* if ((ilRC = sql_if */

	/* close cursor */
	close_my_cursor(&slCursor);

	/* copy value */
       	GetDataItem(pclrkey, pclDataArea, 1, '\0', "", " \0");

	/* reset buffers */
	memset(pclSqlBuf,0x00,200);
	memset(pclOraErrorMsg,0x00,MAX_BUFFER_LEN);	
	memset(pclDataArea,0x00,MAX_BUFFER_LEN);
	
		
	/* build sql statement for rotation flight which is still onbl on new parking position */								
	sprintf(pclSqlBuf,"select flno,stod,etdi,remp,ofbl from AFTTAB where hopo='%s' and rkey='%s' and urno <> '%s' and ftyp='O'",pcgDefHomeAp,pclrkey,pcpUrno);

	
	/* set type of sql call */
	slSqlFunc 	= START;
	slCursor 	= 0;
	
	/* run sql */
	if ((ilRC = sql_if(slSqlFunc,&slCursor,pclSqlBuf,pclDataArea))!= RC_SUCCESS) 
	{
		if (ilRC == NOTFOUND)
		{

		}
		else
		{
			ilRC = RC_FAIL;
			get_ora_err(ilRC,pclOraErrorMsg);	
			dbg(TRACE,"PARKINGProcessPage: ORA-ERR <%s>  <%s>",pclOraErrorMsg, pclSqlBuf);
			return ilRC;
		}
	} /* if ((ilRC = sql_if */

	/* check for a flight */
	if (ilRC==RC_SUCCESS)
	  {
	    GetDataItem(pcldata1, pclDataArea, 2, '\0', "", "\0\0");
	    if (strstr(pcldata1,"20") != NULL) {bldepflight=TRUE;}
	    memset(pcldata1,0x00,20);
	  }

	/* close cursor */
	close_my_cursor(&slCursor);

	/* return if departure flight is ofbl*/
	if (bldepflight==TRUE)
	  {
		GetDataItem(pcldata1, pclDataArea, 5, '\0', "", "\0\0");
		if (strstr(pcldata1,"20") != NULL) {return ilRC;}
		memset(pcldata1,0x00,20);
	  }

	/* copy sql values */
	if ((ilRC == RC_SUCCESS) && (bldepflight==TRUE))
	{
		GetDataItem(pcldatadep1, pclDataArea, 1, '\0', "", " \0");
		GetDataItem(pcldatadep2, pclDataArea, 2, '\0', "", " \0");
		GetDataItem(pcldatadep3, pclDataArea, 3, '\0', "", " \0");
		GetDataItem(pcldatadep4, pclDataArea, 4, '\0', "", " \0");
	}

	/* GET TOWING FLIGHTS */
	  /* reset buffers */
	  memset(pclSqlBuf,0x00,200);
	  memset(pclOraErrorMsg,0x00,MAX_BUFFER_LEN);	
	  memset(pclDataArea,0x00,MAX_BUFFER_LEN);
	
		
	  /* build sql statement for towing activities  which is still onbl on new parking position */								
	  sprintf(pclSqlBuf,"select flno,stod,etdi,remp,psta,onbl from AFTTAB where hopo='%s' and rkey='%s' and psta<> ' ' and ftyp='T' order by stoa",pcgDefHomeAp,pclrkey);

	  /* set type of sql call */
	  slSqlFunc 	= START;
	  slCursor 	= 0;
	
	  /* run sql */
	  while ((ilRC = sql_if(slSqlFunc,&slCursor,pclSqlBuf,pclDataArea))== RC_SUCCESS) 
	    {
	    	/* is towing flight onbl on new position, then save values*/
	    	GetDataItem(pclonbl, pclDataArea, 6, '\0', "", " \0");
		if (strstr(pclonbl,"20") != NULL) {
			GetDataItem(pcldata1, pclDataArea, 1, '\0', "", " \0");
			GetDataItem(pcldata2, pclDataArea, 2, '\0', "", " \0");
			GetDataItem(pcldata3, pclDataArea, 3, '\0', "", " \0");
			GetDataItem(pcldata4, pclDataArea, 4, '\0', "", " \0");

			GetDataItem(pclnewpos, pclDataArea, 5, '\0', "", " \0");
			
			bltowflight=TRUE;
		}		
		memset(pclonbl,0x00,20);

		/* set type of sql call */
		slSqlFunc = NEXT;
	    } /* while */
	  /* close cursor */
	  close_my_cursor(&slCursor);

	  if (ilRC == NOTFOUND)
	    {
	    }
	  else
	    {
		ilRC = RC_FAIL;
		get_ora_err(ilRC,pclOraErrorMsg);	
		dbg(TRACE,"PARKINGProcessPage: ORA-ERR <%s>  <%s>",pclOraErrorMsg, pclSqlBuf);
		return ilRC;
	    }/*success?*/	

		
	/* replace values in linebuffer with rotation flight values */
	/* delete last 4 values from linebuffer */
	memset(pclhelpbuffer,0x00,MAX_BUFFER_LEN);
	memset(pcldummy,0x00,MAX_BUFFER_LEN);
	memset(pcldummy2,0x00,MAX_BUFFER_LEN);

	
	/* copy first six elements to dummy buffer */
	for (ilCnt=1;ilCnt<=6;ilCnt++)
	{
		GetDataItem(pcldummy, pclLineBuffer_withurno, ilCnt, ucgData_del, "", " \0");
		/* insert position for rotation flight values is at: */
		ilPos = ilPos + strlen(pcldummy) + 1;
	} /* for  */
	
	ilPos--;
	
	/* copy first five values to second dummy */
	strncpy(pcldummy2,pclLineBuffer_withurno,(size_t) ilPos);
	pcldummy2[ilPos] = 0x00;
	if (((bldepflight==TRUE) || (bltowflight==TRUE)) && (strlen(pclnewpos)>0))
	  {
		GetDataItem(pclpos, pcldummy2, 2, ucgData_del, "", " \0");
		/* replace */
		if ((ilRC = SearchStringAndReplace(pcldummy2,pclpos,pclnewpos))!= RC_SUCCESS)
		{
			dbg(DEBUG,"PARKINGProcessPage: Search and replace failed.");
		}
	  }/* if new position then replace*/
	
	
	/* change utc to local time */
	if (strstr(pcldata2,"20") != NULL)
	{
		UtcToLocal(pcldata2);
	}
	if (strstr(pcldata3,"20") != NULL)
	{
		UtcToLocal(pcldata3);
	}

       if (strstr(pcldatadep2,"20") != NULL)
        {
                UtcToLocal(pcldatadep2);
        }
        if (strstr(pcldatadep3,"20") != NULL)
        {
                UtcToLocal(pcldatadep3);
        }


	if (ilRC == NOTFOUND)
	{
		ilRC = RC_SUCCESS;
	}
			
			
	/* build new linebuffer */
		if (bldepflight==TRUE)
                {
                        sprintf(pclhelpbuffer,"%s|%s|%s|%s|%s|",pcldummy2,pcldatadep1,pcldatadep2,pcldatadep3,pcldatadep4);
                } else {
 	               sprintf(pclhelpbuffer,"%s|||||",pcldummy2);

                }
		
	if (bgFormatDate == TRUE)
       	{
		/* Checking the dates and reformatting them if necessary */
		CheckDates(&pclhelpbuffer[strlen(pcpUrno)+1]);
       	}

	/* checking image-fields */
	if (bgCreateImgFields == TRUE)
	{
		FormatImageFields(&pclhelpbuffer[strlen(pcpUrno)+1]);
	}
	if (bgResolv == TRUE)
	{
		ResolvCfgFields(&pclhelpbuffer[strlen(pcpUrno)+1]);
	}
	memset(pcldata1,0x00,20);
	GetDataItem(pcldata1,pclhelpbuffer ,2, ucgData_del, "", " \0");

        /* save buffer in page struct */
        strcpy(pfgPAGE.prRow[pfgPAGE.iNoofRows].pcpRows,pclhelpbuffer);
        strcpy(pfgPAGE.prRow[pfgPAGE.iNoofRows].pcpDate,pcldata1);        
        pfgPAGE.iNoofRows++;

	dbg(DEBUG,"PARKINGProcessPage: pclhelpbuffer: %s",pclhelpbuffer);

	return ilRC;
	
} /* PARKINGProcessPage */

/* ******************************************************************** */
/* The CHECKINProcessData(char *pcpBuffer, char *pcpUrno) routine       */
/*									*/
/*	This process generates the correct Counter information for a 	*/
/*	flight, according to its nature. The process also handles 	*/
/*	Code Share flights.						*/
/*	The intention for this process has been the fact, that		*/
/*	the afttab shows only dedicated counters. The common ones	*/
/*	and certain gaps in the counter					*/
/*	As result, the wrong counter value in the linebuffer will be 	*/
/*	replaced with the right one.					*/
/*	EG: old value: 12	new value: 13-25,64-67			*/
/*									*/
/* ******************************************************************** */
static int CHECKINProcessData(char *pclLineBuffer_withurno, char *pcpBuffer, char *pcpUrno)
{
	int 	ilRC 		= RC_SUCCESS;
	int	ilCnt		= 0;
	int	ilNature	= 0;
	short 	slCursor 	= 0;		/* Cursor for SQL call		*/
	short 	slSqlFunc;			/* Type of SQl Call		*/
	char 	pclSqlBuf[200];			/* Buffer for SQl statements 	*/
	char 	pclDataArea[MAX_BUFFER_LEN];	/* Buffer for retrieved data	*/
	char 	pclOraErrorMsg[MAX_BUFFER_LEN]; /* Buffer for error message from SQl call */
	CCATAB_DED 	rgCCA_DED;		/* struct containing the dedicated 	*/
						/* check in counter data		*/
						/* and the no of received records	*/
	
	/** Is field CKIF in fieldbuffer?? If not leave function. 	**/	
	if (strstr(prgDynCfg->processcheckin, "YES")==NULL)
	{
		return ilRC;
	} /* if (strstr(prgDynCfg->fields, "ckif") */

	/** Select values from afttab for further processing		**/
	/* reset buffers */
	memset(pclSqlBuf,0x00,200);
	memset(pclDataArea,0x00,MAX_BUFFER_LEN);
	memset(pclOraErrorMsg,0x00,MAX_BUFFER_LEN);	
	
	/* build sql statement for dedicated counters */								
	sprintf(pclSqlBuf,"select ckic from CCATAB where hopo='%s' and FLNU='%s' order by ckic",pcgDefHomeAp,pcpUrno);
	
	/* set type of sql call */
	slSqlFunc = START;

	while ((ilRC = sql_if(slSqlFunc,&slCursor,pclSqlBuf,pclDataArea))== RC_SUCCESS) 
	{
	  if (atoi(&pclDataArea[0])!=0)
	  {
		/* write values in structure	*/
		rgCCA_DED.ildedcounters[ilCnt] = atoi(&pclDataArea[0]);
		ilCnt++;
	  }
		/* set type of sql call */
		slSqlFunc = NEXT;		
	} /* while */
	rgCCA_DED.iNoofRows = ilCnt;

	/* close cursor */
	close_my_cursor(&slCursor);

	/* check for the right ilRC value */
	if (ilRC == NOTFOUND)
	{
		ilRC = RC_SUCCESS;
	}
	else
	{
		ilRC = RC_FAIL;
		get_ora_err(ilRC,pclOraErrorMsg);	
		dbg(TRACE,"CHECKINProcessData: no dedicated counters: ORA-ERR <%s>  <%s>",pclOraErrorMsg, pclSqlBuf);
		return ilRC;
	}
		
	/* reset buffers */
	memset(pclSqlBuf,0x00,200);
	memset(pclDataArea,0x00,MAX_BUFFER_LEN);
	memset(pclOraErrorMsg,0x00,MAX_BUFFER_LEN);	

	
		
	/* build sql statement for nature */								
	sprintf(pclSqlBuf,"select TTYP from AFTTAB where URNO='%s'",pcpUrno);
	
	/* set type of sql call */
	slSqlFunc 	= START;
	slCursor 	= 0;
	
	/* run sql */
	if ((ilRC = sql_if(slSqlFunc,&slCursor,pclSqlBuf,pclDataArea))!= RC_SUCCESS) 
	{
		if (ilRC == NOTFOUND)
		{
			ilRC = RC_SUCCESS;
		}
		else
		{
			ilRC = RC_FAIL;
			get_ora_err(ilRC,pclOraErrorMsg);	
			dbg(TRACE,"CHECKINProcessData: ORA-ERR <%s>  <%s>",pclOraErrorMsg, pclSqlBuf);
		}
		/* close cursor */
		close_my_cursor(&slCursor);
		return ilRC;
	} /* if ((ilRC = sql_if */
	
	/* close cursor */
	close_my_cursor(&slCursor);

	/** Calculate counter value according to nature*/	
	ilRC = CHECKINProcessNature(pclLineBuffer_withurno, pcpBuffer, &rgCCA_DED, pcpUrno,pclDataArea);
		
	
	return ilRC;

} /*** CHECKINProcessData ***/

/* ******************************************************************** */
/* The CHECKINGetCommonCheckIn(struct CCATAB_STRUCT *rgCCA, 		*/
/*						char *pcpUrno) routine	*/
/*									*/
/*	This process creates internal table with data as following:	*/
/*		ccatab.flnu						*/
/*		ccatab.ckic	counter name				*/
/*		alttab.alc2						*/
/*		alttab.alc3						*/
/*	The creation is done by a sql_if call with a JOIN statement	*/
/*									*/
/* ******************************************************************** */
static int CHECKINGetCommonCheckIn(CCATAB_STRUCT *rgCCA, char *pcpUrno,char *pcpnature)
{
  int 	ilRC 		= RC_SUCCESS;
  int	ilCurrentRow	= 0;		/* actual row 			*/
  short 	slCursor 	= 0;		/* Cursor for SQL call		*/
  short 	slSqlFunc;			/* Type of SQl Call		*/
  int	iljfno_len	= 0;		/* Length of CSF in JFNO fields	*/
  int	ilstartposdata	= 0;		/* start position for extracting values from sql databuffer */
  char 	pclSqlBuf[4*MAX_BUFFER_LEN];	/* Buffer for SQl statements 	*/
  char 	pclDataArea[MAX_BUFFER_LEN];	/* Buffer for retrieved data	*/
  char 	pclOraErrorMsg[MAX_BUFFER_LEN]; /* Buffer for error message from SQl call */
  char	pclActualTime[16];		/* actual UTC time		*/
  char    pcldummy[XS_BUFF];
  char    pcldummy2[XS_BUFF];
  char    pcljfnosql[S_BUFF];
  char    pcljfno[S_BUFF];
  char    pclCsf[XS_BUFF];
  char     pclAltUrnos[S_BUFF];
  /***************** Get actual Airline ********************/	
  /* reset buffers */
  memset(pclSqlBuf,0x00,4*MAX_BUFFER_LEN);
  memset(pclDataArea,0x00,MAX_BUFFER_LEN);
  memset(pclOraErrorMsg,0x00,MAX_BUFFER_LEN);	

	  
  /* select Airline and JFNO from flight */
  sprintf(pclSqlBuf,"select alc2,jfno from afttab where urno='%s'",pcpUrno);		
	
	
	
  /* set type of sql call */
  slSqlFunc = START;
	
  /* run sql */
  if ((ilRC = sql_if(slSqlFunc,&slCursor,&pclSqlBuf[0],pclDataArea))!= RC_SUCCESS) 
    {
      get_ora_err(ilRC,pclOraErrorMsg);	
      dbg(TRACE,"CHECKINGetCommonCheckIn: \nORA-ERR: <%s>  \nSQl statement: <%s>",pclOraErrorMsg, pclSqlBuf);
      close_my_cursor(&slCursor);
      ilRC = RC_FAIL;
      return ilRC;
    } /* if sql_if */
	
  /* close cursor */
  close_my_cursor(&slCursor);
	
	
  /***************** Get No of datarows ********************/	
  /* reset buffers */
  memset(pclSqlBuf,0x00,4*MAX_BUFFER_LEN);
  memset(pclOraErrorMsg,0x00,MAX_BUFFER_LEN);	
  memset(pclActualTime,0x00,16);	
  memset(pcldummy,0x00,XS_BUFF);	
  memset(pcldummy2,0x00,XS_BUFF);	
  memset(pcljfnosql,0x00,S_BUFF);
  memset(pcljfno,0x00,S_BUFF);
  memset(pclCsf,0x00,XS_BUFF);

  /* prepare value for airline */
  GetDataItem(pcldummy, pclDataArea, 1, '\0', "", "\0\0");
  strncpy(pcldummy2,pcldummy,2);
		
  sprintf(pcljfnosql,"(ALTTAB.ALC2 = '%s') ",pcldummy2);

  /* prepare values for jnfo */
  GetDataItem(pcldummy, pclDataArea, 2, '\0', "", "\0\0");
  /*Extract flights */
  if (strlen(pcldummy) > 1) {
	
    /* get  length of csf*/
    iljfno_len = atoi(prgDynCfg->jfno_len);

    /* fill sql buffer */
    strcat(pcljfnosql," OR ");
    /* split dataarea in separate flights and write them into linebuffer*/	
    while (ilstartposdata < strlen(pcldummy))
      {
	/* copy next flight suffix*/
	strncpy(pclCsf,&pcldummy[ilstartposdata],3);
	pclCsf[3] = 0x00;
	TrimRight(pclCsf);
	sprintf(pcljfno,"(ALTTAB.ALC2 = '%s' OR ALTTAB.ALC3 = '%s') OR ",pclCsf,pclCsf);
	strcat(pcljfnosql,pcljfno);
									
	ilstartposdata = ilstartposdata + iljfno_len;	
      }
    memset(pcldummy,0x00,XS_BUFF);
    strncpy(pcldummy,pcljfnosql,strlen(pcljfnosql)-4);
    memset(pcljfnosql,0x00,S_BUFF);
    sprintf(pcljfnosql,"%s",pcldummy);
  } /* CSF available? */
	
  sprintf(pclSqlBuf,"select urno from alttab where %s and hopo='%s'",pcljfnosql,pcgDefHomeAp);

  slSqlFunc 	= START;
  slCursor 	= 0;
  pclAltUrnos[0]='\0';
  while(sql_if(slSqlFunc,&slCursor,pclSqlBuf,pclDataArea)== RC_SUCCESS) 
    {
      if(pclAltUrnos[0]=='\0')
	strcpy(pclAltUrnos,pclDataArea);
      else
	sprintf(pclAltUrnos,"%s,%s",pclAltUrnos,pclDataArea);
      slSqlFunc 	=NEXT;
    }
  close_my_cursor(&slCursor);
  DeleteCharacterInString(pclAltUrnos,cBLANK);
  if(pclAltUrnos[0]=='\0')
    strcpy(pclAltUrnos,"0");
  dbg(TRACE,"pclAltUrnos -> %s ",pclAltUrnos);
  dbg(TRACE,"Start SQL");
	
  sprintf(pclSqlBuf,"select count (*) from ccatab where (CCATAB.CKES=' ' OR CCATAB.CKES>TO_CHAR(SYSDATE-1,'YYYYMMDDHH24MISS'))and flnu in (%s)  and ctyp='C' and hopo = '%s'and ckbs<TO_CHAR(SYSDATE+1,'YYYYMMDDHH24MISS') ",pclAltUrnos,pcgDefHomeAp);
	
  /* build sql statement*/
#if 0								
  sprintf(pclSqlBuf,"select count (*) from ccatab,alttab where  (%s) and alttab.urno=ccatab.flnu  and ccatab.ctyp='C' and ccatab.ckbs<TO_CHAR(SYSDATE+1,'YYYYMMDDHH24MISS') and (CCATAB.CKES=' ' OR CCATAB.CKES>TO_CHAR(SYSDATE-1,'YYYYMMDDHH24MISS'))",pcljfnosql,pcgDefHomeAp);
#endif
  dbg(TRACE,"END SQL <%s>",pclSqlBuf);
	
  memset(pclDataArea,0x00,MAX_BUFFER_LEN);
	
  /* set type of sql call */
  slSqlFunc 	= START;
  slCursor 	= 0;
	
  /* run sql */
  if ((ilRC = sql_if(slSqlFunc,&slCursor,&pclSqlBuf[0],pclDataArea))!= RC_SUCCESS) 
    {
      get_ora_err(ilRC,pclOraErrorMsg);	
      dbg(TRACE,"CHECKINGetCommonCheckIn: \nORA-ERR: <%s>  \nSQl statement: <%s>",pclOraErrorMsg, pclSqlBuf);
      close_my_cursor(&slCursor);
      ilRC = RC_FAIL;
      return ilRC;
    } /* if sql_if */
  dbg(TRACE,"End SQL");
  /* close cursor */
  close_my_cursor(&slCursor);

  /* Check if data is avaliable, quit if not */
  if ((rgCCA->iNoofRows = atoi(pclDataArea)) <= 0) 
    {
      dbg(DEBUG,"CHECKINGetCommonCheckIn: no common check in data available");
      return ilRC;
    } /* if  */
	
  /******************* Get datarows **********************/
  /* GET FLTI FIRST FOR THE WAW AIRPORT*/	
  memset(pclSqlBuf,0x00,4*MAX_BUFFER_LEN);
  if ((strcmp(prgDynCfg->use_flti,"YES")==0) && (strstr(pcgDefHomeAp, "WAW") != NULL)) {
    /* reset buffers */
    memset(pclDataArea,0x00,S_BUFF);
    memset(pclOraErrorMsg,0x00,MAX_BUFFER_LEN);
	
    /* build sql statement*/								
    sprintf(pclSqlBuf,"SELECT FLTI FROM AFT%s WHERE  URNO ='%s'",pcgDefTabEnd,pcpUrno);	
	
    /* set type of sql call */
    slSqlFunc 	= START;
    slCursor 	= 0;
	
    /* run sql */
    if ((ilRC = sql_if(slSqlFunc,&slCursor,pclSqlBuf,pclDataArea))!= RC_SUCCESS) 
      {
	if (ilRC == NOTFOUND)
	  {	
	    ilRC = RC_SUCCESS;
	  }
	else
	  {
	    ilRC = RC_FAIL;
	    get_ora_err(ilRC,pclOraErrorMsg);	
	    dbg(DEBUG,"ROWCreate: ORA-ERR <%s>  <%s>",pclOraErrorMsg, pclSqlBuf);
	  }
      } /* if ((ilRC = sql_if */
	
    /* close cursor */
    close_my_cursor(&slCursor);
	
    sprintf(pcldummy,"%s",pclDataArea);
    TrimRight(pcldummy);

    /* build sql statement*/
    /* IF FLTI=I then ckic<60, if FLTI=M, then all ckic, if FLTI=M then ckic>60 */
    if (strstr(pcldummy, "I") != NULL) {
      sprintf(pclSqlBuf,"select distinct ccatab.flnu, ccatab.ckic, alttab.alc2, alttab.alc3 from ccatab,alttab \
				where ((alttab.urno in (%s)  and ccatab.flnu in (%s) and alttab.urno=ccatab.flnu  and ccatab.hopo = '%s') \
                                and (ccatab.ctyp='C' and ccatab.ckic<'%s' and ccatab.ckic<>' ') and \
				((ccatab.ckbs<TO_CHAR(SYSDATE+1,'YYYYMMDDHH24MISS')) and ((CCATAB.CKES=' ') OR \
                                (CCATAB.CKES>TO_CHAR(SYSDATE-1,'YYYYMMDDHH24MISS'))))) \
				order by ccatab.ckic, ccatab.flnu",pclAltUrnos,pclAltUrnos,pcgDefHomeAp,prgDynCfg->checkin_dom_start);
    } else if (strstr(pcldummy, "D") != NULL) {
      sprintf(pclSqlBuf,"select distinct ccatab.flnu, ccatab.ckic, alttab.alc2, alttab.alc3 from ccatab,alttab \
				where ((alttab.urno in (%s) and ccatab.flnu in (%s) and alttab.urno=ccatab.flnu  and ccatab.hopo = '%s') \
                                and (ccatab.ctyp='C' and ccatab.ckic>'%s') and \
				((ccatab.ckbs<TO_CHAR(SYSDATE+1,'YYYYMMDDHH24MISS')) and ((CCATAB.CKES=' ') OR \
                                (CCATAB.CKES>TO_CHAR(SYSDATE-1,'YYYYMMDDHH24MISS'))))) \
				order by ccatab.ckic, ccatab.flnu",pclAltUrnos,pclAltUrnos,pcgDefHomeAp,prgDynCfg->checkin_dom_start);
    } else {
      sprintf(pclSqlBuf,"select distinct ccatab.flnu, ccatab.ckic, alttab.alc2, alttab.alc3 from ccatab,alttab \
				where ((alttab.urno in (%s) and ccatab.flnu in (%s) and alttab.urno=ccatab.flnu  and ccatab.hopo = '%s') \
                                and (ccatab.ctyp='C' and ccatab.ckic<>' ') and \
				((ccatab.ckbs<TO_CHAR(SYSDATE+1,'YYYYMMDDHH24MISS')) and ((CCATAB.CKES=' ') OR \
                                (CCATAB.CKES>TO_CHAR(SYSDATE-1,'YYYYMMDDHH24MISS'))))) \
				order by ccatab.ckic, ccatab.flnu",pclAltUrnos,pclAltUrnos,pcgDefHomeAp);
    }
  } else {
    /* FOR ALL OTHER AIRPORTS all ckic */
    sprintf(pclSqlBuf,"select distinct ccatab.flnu, ccatab.ckic, alttab.alc2, alttab.alc3 from ccatab,alttab \
			where ((alttab.urno=ccatab.flnu and (%s) and ccatab.hopo = '%s') \
                        and (ccatab.ctyp='C' and ccatab.ckic<>' ') and \
			((ccatab.ckbs<TO_CHAR(SYSDATE+1,'YYYYMMDDHH24MISS')) and ((CCATAB.CKES=' ') OR \
                        (CCATAB.CKES>TO_CHAR(SYSDATE-1,'YYYYMMDDHH24MISS')))) \
			order by ccatab.ckic, ccatab.flnu",pcljfnosql,pcgDefHomeAp);
  }


  /*** NOW GET THE CHECKINS */
  memset(pcldummy,0x00,XS_BUFF);	
  memset(pclDataArea,0x00,MAX_BUFFER_LEN);
  memset(pclOraErrorMsg,0x00,MAX_BUFFER_LEN);	
	
  /* set type of sql call */
  slSqlFunc 	= START;
  slCursor 	= 0;
	
  /* run sql */
  dbg(TRACE,"pclSqlBuf <%s>",pclSqlBuf);
  while ((ilRC = sql_if(slSqlFunc,&slCursor,pclSqlBuf,pclDataArea))== RC_SUCCESS) 
    {	
      /* write values in structure	*/
      BuildItemBuffer(pclDataArea,"FLNU,CKIC,ALC3,ALC2",0,",");
      GetDataItem(rgCCA->prRow[ilCurrentRow].pcflnu, pclDataArea, 1, ',', "", " \0");
      GetDataItem(rgCCA->prRow[ilCurrentRow].pcckic, pclDataArea, 2, ',', "", " \0");
      GetDataItem(rgCCA->prRow[ilCurrentRow].pcalc2, pclDataArea, 3, ',', "", " \0");
      GetDataItem(rgCCA->prRow[ilCurrentRow].pcalc3, pclDataArea, 4, ',', "", " \0");
      dbg(TRACE,"FLNU,CKIC,ALC3,ALC2 <%s>",pclDataArea);
      /* set row flag */
      ilCurrentRow++;
		
      /* set type of sql call */
      slSqlFunc = NEXT;
    } /* while */
	

  /* set no of valid datarows */
  rgCCA->iNoofRows = ilCurrentRow;

  /* close cursor */
  close_my_cursor(&slCursor);

  /* check for the right ilRC value */
  if (ilRC == NOTFOUND)
    {
      ilRC = RC_SUCCESS;
    }
  else
    {
      ilRC = RC_FAIL;
      get_ora_err(ilRC,pclOraErrorMsg);	
      dbg(TRACE,"CHECKINGetCommonCheckIn: ORA-ERR <%s>  <%s>",pclOraErrorMsg, pclSqlBuf);
    }
	
		
  return ilRC;

} /* CHECKINGetCommonCheckIn */

/* ******************************************************************** */
/* The CHECKINProcessNature(char *pcpBuffer,char *pcpDataArea) routine	*/
/*									*/
/*	This process handles flights with nature 11,13,14		*/
/*	MAIN & CSF use common & dedicated counters			*/
/*	as result, the evaluated value will replace the old one		*/
/*									*/
/*									*/
/* ******************************************************************** */
static int CHECKINProcessNature(char *pclLineBuffer_withurno, char *pcpBuffer, CCATAB_DED *rgCCA_DED, char *pcpUrno, char *pcpnature)
{
	int 		ilRC 		= RC_SUCCESS;
	int		ilmainCnt;
	int		ilCnt;
	int		ilCnt2;
	int             ilFieldPos;
	int		ilstartposition[200];	/* start of common counters		*/
	int		ildummyposition	= 0;	/* flag for common counters position	*/	
	int		ilendposition[200];	/* end of common counters		*/
	int		ildedstartposition= 0;	/* start of dedicated counters		*/
	int		ildedendposition= 0;	/* end of dedicated counters		*/
	int		ilstartposdata	= 0;		/* start position for extracting values from sql databuffer */
	short 		slCursor 	= 0;		/* Cursor for SQL call		*/
	short 		slSqlFunc;			/* Type of SQl Call		*/
	int		iljfno_len	= 0;		/* Length of CSF in JFNO fields	*/
	char            pcldummy[50];
	char 		pclSqlBuf[400];			/* Buffer for SQl statements 	*/
	char 		pclDataArea[MAX_BUFFER_LEN];	/* Buffer for retrieved data	*/
	char 		pclOraErrorMsg[MAX_BUFFER_LEN]; /* Buffer for error message from SQl call */
	char    	pclCsf[XS_BUFF];
	CCATAB_STRUCT 	rgCCA;			/* struct containing the common 	*/
						/* check in counter data		*/
						/* and the no of received records	*/

	/* Fill structure with common counter data  */
	if ((ilRC = CHECKINGetCommonCheckIn(&rgCCA, pcpUrno,pcpnature))!= RC_SUCCESS) 
	{
		dbg(DEBUG,"CHECKINProcessNature: CHECKINGetCommonCheckIn failed with %d",ilRC);
		return ilRC;
	} /* if CHECKINGetCommonCheckIn */

	
	/* get common checkin values for main flight and all code share flight */
	/* reset buffers */
	memset(pclSqlBuf,0x00,400);
	memset(pclDataArea,0x00,MAX_BUFFER_LEN);
	memset(pclOraErrorMsg,0x00,MAX_BUFFER_LEN);	
	memset(pcldummy,0x00,XS_BUFF);	
	memset(pclCsf,0x00,XS_BUFF);
	for (ilCnt = 0; ilCnt < 200; ilCnt ++) {
		ilstartposition[ilCnt]	= 0;	
		ilendposition[ilCnt]	= 0;
	}
	
	
	/* select Airline and JFNO from flight */
	sprintf(pclSqlBuf,"select alc2,jfno from afttab where urno='%s'",pcpUrno);		
			
	/* set type of sql call */
	slSqlFunc = START;
	
	/* run sql */
	if ((ilRC = sql_if(slSqlFunc,&slCursor,&pclSqlBuf[0],pclDataArea))!= RC_SUCCESS) 
	{
		get_ora_err(ilRC,pclOraErrorMsg);	
		dbg(TRACE,"CHECKINProcessNature: \nORA-ERR: <%s>  \nSQl statement: <%s>",pclOraErrorMsg, pclSqlBuf);
		close_my_cursor(&slCursor);
		ilRC = RC_FAIL;
		return ilRC;
	} /* if sql_if */
	
	/* close cursor */
	close_my_cursor(&slCursor);

	/* get  length of csf*/
	iljfno_len = atoi(prgDynCfg->jfno_len);

	/* prepare values for jnfo */
	GetDataItem(pcldummy, pclDataArea, 2, '\0', "", "\0\0");
	ilmainCnt	= 0;
	
	/* for all CSF, pcldummy is initially blank */
	while (ilstartposdata < strlen(pcldummy))
	{
		if (ilmainCnt==0) {
			/* prepare value for airline */
			GetDataItem(pclCsf, pclDataArea, 1, '\0', "", "\0\0");
			if (strlen(pcldummy)==1){
				ilstartposdata++;
			}

		} else {
			/*Extract flights */
			if (strlen(pcldummy) > 1) {
				/* copy flight suffix*/
				strncpy(pclCsf,&pcldummy[(ilmainCnt-1)*iljfno_len],3);
				pclCsf[3] = 0x00;
				TrimRight(pclCsf);
				ilstartposdata = ilstartposdata + iljfno_len;
			}
		} /* if (ilmainCnt==0) */



	
		/* if common checkin counter data available */
		if (rgCCA.iNoofRows > 0)
		{
			/*** Analyze data **/
			/* set start position */
			for (ilCnt = 0; ilCnt < rgCCA.iNoofRows; ilCnt ++) {
				if ((strcmp(rgCCA.prRow[ilCnt].pcalc2,pclCsf)==0) || (strcmp(rgCCA.prRow[ilCnt].pcalc3,pclCsf)==0)) {
					ilstartposition[ilmainCnt] = atoi(rgCCA.prRow[ilCnt].pcckic);
					ildummyposition = ilstartposition[ilmainCnt];
					break;
				}
			}
			/* search Structure for more values and set the actual endposition */
			for (ilCnt; ilCnt < rgCCA.iNoofRows; ilCnt ++)
			{
				for (ilCnt2 = ilCnt; ilCnt2 < rgCCA.iNoofRows; ilCnt2 ++) {
					if ((strcmp(rgCCA.prRow[ilCnt2].pcalc2,pclCsf)==0) || (strcmp(rgCCA.prRow[ilCnt2].pcalc3,pclCsf)==0)) {
						ilendposition[ilmainCnt] = atoi(rgCCA.prRow[ilCnt2].pcckic);
						break;
					}
				}
		
				/* is difference between the last end position and the actual > Checkin_gap then quit*/
				if ((ilendposition[ilmainCnt]-ildummyposition)>atoi(prgDynCfg->checkin_gap)) 
				{
					/* set endposition to last value*/ 
					ilendposition[ilmainCnt] = ildummyposition;
					break;
				}		
				ildummyposition = ilendposition[ilmainCnt];
			} /* for*/
		}
		else
		{
			ilstartposition[ilmainCnt] == 0;
			ilendposition[ilmainCnt] == ilstartposition[ilmainCnt];
		} /* if common checkin counter data available */

		ilmainCnt++;
		
	} /* for main and csf */
	
	
	ilmainCnt--;
	
	/* Calculate counter sequence  */
	if ((ilRC = CHECKINCalculateCounter(pclLineBuffer_withurno, pcpBuffer, pcpUrno, ilstartposition,ilendposition,rgCCA_DED, pcpnature, ilmainCnt))!= RC_SUCCESS) 
	{
		dbg(DEBUG,"CHECKINProcessNature: CHECKINCalculateCounter failed with %d",ilRC);
		return ilRC;
	} /* if CHECKINCalculateCounter */
	
	
	return ilRC;

} /* CHECKINProcessNature */


/* ******************************************************************** */
/* The TOOLSort(CCATAB_DED *rgCCA_DED)	function			*/ 
/*									*/
/*	This process sorts the struct with the counter values		*/
/*									*/
/* ******************************************************************** */
static int TOOLSort(CCATAB_DED *rgCCA_DED)
{
	int 	ilRC = RC_SUCCESS;
 	int  	ilCnt1, ilCnt2;                  
 	int  	ildummy;
 
 	for (ilCnt1=0; ilCnt1<rgCCA_DED->iNoofRows-1; ilCnt1++)
	{
  		for (ilCnt2=ilCnt1+1; ilCnt2<rgCCA_DED->iNoofRows; ilCnt2++)
		{ 
   			if(rgCCA_DED->ildedcounters[ilCnt1] > rgCCA_DED->ildedcounters[ilCnt2])
   			{
   				ildummy = rgCCA_DED->ildedcounters[ilCnt1];
    				rgCCA_DED->ildedcounters[ilCnt1] = rgCCA_DED->ildedcounters[ilCnt2];
    				rgCCA_DED->ildedcounters[ilCnt2] = ildummy;
   			} /*if */
		}
	}
	
	return ilRC;
} /* TOOLSort */

/* ******************************************************************** */
/* The CHECKINCalculateCounter(char *pcpBuffer,int commonstart,		*/ 
/*	int commonend, int dedstart, int dedend) routine		*/
/*									*/
/*	This process contains the algorithms for			*/
/*	the calculation of the correct counter sequence			*/
/*									*/
/* ******************************************************************** */
static int CHECKINCalculateCounter(char *pclLineBuffer_withurno,char *pcpBuffer,char *pcpUrno, int ilcommonstart[200], int ilcommonend[200], CCATAB_DED *rgCCA_DED, char *pcpnature, int ilnoofflights) 
{
	int 	ilRC 		= RC_SUCCESS;
	int	ilFieldlength	= 0;		/* length of ckif value */
	int	ilFieldPos;			/* position of ckif field */
	int	ilCnt;	
	int	ilmainCnt;		
	int	ilValuelength	= 0;		/* length of counterfiled value */		
	int	ildedstart 	= 0;		/* startposition of dedicated counters */
	int	ildedend 	= 0;		/* endposition of dedicated counters */
	int 	ilnoofdedcounters = 0;		/* number of dedicated numbers */
	int	ildedcounters[200];		/* buffer for dedicated counters */
	int	ilcomparelength	= 0;		/* flag for part of configurated nature,which will be compared with db nature */
	char 	pclDataArea[S_BUFF];		/* buffers for the jfno-afttab SQL call */
	char 	pclSqlBuf[S_BUFF];
	char 	pclOraErrorMsg[MAX_BUFFER_LEN];
	short 	slSqlFunc;
	short 	slCursor 	= 0;
	char	pclcomCounterbuffer[XS_BUFF];	/* buffer for formatted common checkin counter value */
	char	pcldedCounterbuffer[XS_BUFF];	/* buffer for formatted ded. checkin counter value */
	char	pclCounterbuffer[XS_BUFF];	/* buffer for formatted counter value */
	char	pclcsfCounterbuffer[XS_BUFF];	/* buffer for formatted counter value for main and code share*/
	char	pclcsfcomCounterbuffer[XS_BUFF];/* buffer for formatted common counter value for main and code share*/
	char	pcldummy[XS_BUFF];		
	char	pcldummy2[MAX_BUFFER_LEN];	/* buffer for memmove */		
	char	pcldummy3[XS_BUFF];		
	BOOL	blnature 			= FALSE;	/* tag for any found nature */
	char	pclmainflight[XS_BUFF];		
	char	pclcodeshareflight[XS_BUFF];		
	char	pclmaincounterstring[XS_BUFF];		
	char	pclcodesharecounterstring[XS_BUFF];		
	int	ilNaturecount	= 0;	
	CCATAB_DED 	rlbuffer;			


	/* reset buffers */
	memset(pclCounterbuffer,0x00,XS_BUFF);
	memset(pclcsfCounterbuffer,0x00,XS_BUFF);
	memset(pcldummy,0x00,XS_BUFF);
	memset(pcldummy2,0x00,MAX_BUFFER_LEN);
	memset(pclmainflight,0x00,XS_BUFF);
	memset(pclcodeshareflight,0x00,XS_BUFF);
	memset(pclmaincounterstring,0x00,XS_BUFF);
	memset(pclcodesharecounterstring,0x00,XS_BUFF);

	/* save number of dedicated counters */
	ilnoofdedcounters = rgCCA_DED->iNoofRows;

	ilRC = TOOLSort(rgCCA_DED);
	
	/* Save struct with dedicated counter */
	for (ilCnt=0;ilCnt<rgCCA_DED->iNoofRows;ilCnt++) {
		rlbuffer.ildedcounters[ilCnt] = rgCCA_DED->ildedcounters[ilCnt];
	}
	rlbuffer.iNoofRows = rgCCA_DED->iNoofRows;
	
	/********************************************************/	
	/* COMMON AND DED COUNTERS (MAIN AND CODE SHARE FLIGHTS)*/	
	/********************************************************/	
	for (ilmainCnt = 0; ilmainCnt<=ilnoofflights;ilmainCnt++) {
		
		memset(pcldummy,0x00,XS_BUFF);
		memset(pcldummy2,0x00,MAX_BUFFER_LEN);
				
		for (ilCnt = 0; ilCnt < rgCCA_DED->iNoofRows; ilCnt++)
		{
			ildedcounters[ilCnt] = rgCCA_DED->ildedcounters[ilCnt];
		} /* copy ded. counters */
	

		/******** Part 1: add common counter values to struct ********/
		/* only one common counter */
		if ((ilcommonstart[ilmainCnt]==ilcommonend[ilmainCnt]) && (ilcommonstart[ilmainCnt] != 0))
		{
			rgCCA_DED->ildedcounters[rgCCA_DED->iNoofRows] = ilcommonstart[ilmainCnt];
			rgCCA_DED->iNoofRows++;
		}	
	
		/* several common counters 	*/
		if ((ilcommonstart[ilmainCnt]!=ilcommonend[ilmainCnt]) && (ilcommonstart[ilmainCnt] != 0))
		{
			for (ilCnt = ilcommonstart[ilmainCnt]; ilCnt <= ilcommonend[ilmainCnt]; ilCnt++)
			{
				rgCCA_DED->ildedcounters[rgCCA_DED->iNoofRows] = ilCnt;
				rgCCA_DED->iNoofRows++;
			} /* for */
		} /* if more than one common counter */

	
		/******** Part 2: sort the struct ********/
		ilRC = TOOLSort(rgCCA_DED);
			for (ilCnt = 0; ilCnt < rgCCA_DED->iNoofRows; ilCnt++)
			{
				rgCCA_DED->ildedcounters[rgCCA_DED->iNoofRows] = ilCnt;

			} /* for */

		
		/******** Part 3: build the string from struct values ********/
		ildedstart 	= rgCCA_DED->ildedcounters[0];
		ildedend 	= ildedstart;
		for (ilCnt = 1; ilCnt <= rgCCA_DED->iNoofRows; ilCnt++)
		{

			if (((rgCCA_DED->ildedcounters[ilCnt] - ildedend) > 1) || (ilCnt == rgCCA_DED->iNoofRows))
			{
				if (ildedstart!=0) {
					if (ildedend == ildedstart)
					{
						/* copy start */
						
						sprintf(pcldummy,"%02d<BR>",ildedstart);
						if (ilmainCnt==0) {
							strcat(pclCounterbuffer,pcldummy);
						}
						strcat(pcldummy2,pcldummy);						
					}
					else
					{
						/*copy start-end */
						sprintf(pcldummy,"%02d-%02d<BR>",ildedstart,ildedend);
						if (ilmainCnt==0) {
							strcat(pclCounterbuffer,pcldummy);
						}
						strcat(pcldummy2,pcldummy);
					} /*(ildedend == 0)*/
				}
				dbg(DEBUG,"loop1 \nvalue:  %s  with pcldummy2 <%s> at pos %d\n",pcldummy,pcldummy2,ilCnt);
				ildedstart = rgCCA_DED->ildedcounters[ilCnt];
				ildedend = ildedstart;		
			}
			else
			{
				ildedend = rgCCA_DED->ildedcounters[ilCnt];
			}
			
			
		} /* for all arrayelements */
		
		if (ilmainCnt==0) {
			strcpy(pclcsfCounterbuffer,pcldummy2);
		} else {
			strcat(pclcsfCounterbuffer,"#");
			strcat(pclcsfCounterbuffer,pcldummy2);			
		}

		/* Reset struct with dedicated counter */
		for (ilCnt=0;ilCnt<rgCCA_DED->iNoofRows;ilCnt++) {
			rgCCA_DED->ildedcounters[ilCnt] = 0;
		}
		for (ilCnt=0;ilCnt<rlbuffer.iNoofRows;ilCnt++) {
			rgCCA_DED->ildedcounters[ilCnt] = rlbuffer.ildedcounters[ilCnt];
			rgCCA_DED->iNoofRows = rlbuffer.iNoofRows;
		}

	} /* for all flights */
	
	/************************************************/	
	/* COMMON COUNTERS (MAIN AND CODE SHARE FLIGHTS)*/	
	/************************************************/	
	memset(pclcomCounterbuffer,0x00,XS_BUFF);
	memset(pclcsfcomCounterbuffer,0x00,XS_BUFF);
	for (ilmainCnt = 0; ilmainCnt<=ilnoofflights;ilmainCnt++) {

		memset(pcldummy,0x00,XS_BUFF);
		if (ilcommonstart[ilmainCnt] != 0)
		{
			if (ilcommonstart[ilmainCnt] == ilcommonend[ilmainCnt]) 	
			{	
				if (ilmainCnt==0) {
					sprintf (pclcomCounterbuffer, "%02d", ilcommonstart[ilmainCnt]);
					sprintf (pclcsfcomCounterbuffer, "%02d", ilcommonstart[ilmainCnt]);
				} else {
					sprintf(pcldummy,"#%02d",ilcommonstart[ilmainCnt]);
					strcat(pclcsfcomCounterbuffer,pcldummy);
				}				
			}
			else 			
			{
				if (ilmainCnt==0) {
					sprintf(pclcomCounterbuffer,"%02d-%02d",ilcommonstart[ilmainCnt],ilcommonend[ilmainCnt]);
					sprintf (pclcsfcomCounterbuffer,"%02d-%02d",ilcommonstart[ilmainCnt],ilcommonend[ilmainCnt]);
				} else {
					sprintf(pcldummy,"#%02d-%02d",ilcommonstart[ilmainCnt],ilcommonend[ilmainCnt]);
					strcat(pclcsfcomCounterbuffer,pcldummy);
				}
			}
		} else {
			if (ilmainCnt==0) {
				strcat(pclcsfcomCounterbuffer," ");
			} else {
				strcat(pclcsfcomCounterbuffer,"# ");
			}
		} /* create common checkin counter string */
		
	} /* for all flights */
		
	
	/************************************************/			
	/* DEDICATED COUNTERS (ONLY MAIN FLIGHT)	*/	
	/************************************************/	
	memset(pcldedCounterbuffer,0x00,XS_BUFF);
	memset(pcldummy,0x00,XS_BUFF);
	if (ilnoofdedcounters > 0)
	{
		ildedstart 	= ildedcounters[0];
		ildedend 	= ildedstart;
		for (ilCnt = 1; ilCnt <= ilnoofdedcounters; ilCnt++)
		{
			if (ildedstart == 0){break;}
			if (((ildedcounters[ilCnt] - ildedend) > 1) || (ilCnt == ilnoofdedcounters))
			{
				if (ildedend == ildedstart)
				{
					/* copy start */
					sprintf(pcldedCounterbuffer,"%02d<BR>",ildedstart);
					strcat(pcldummy,pcldedCounterbuffer);
				}
				else
				{
					/*copy start-end */
					sprintf(pcldedCounterbuffer,"%02d-%02d<BR>",ildedstart,ildedend);
					strcat(pcldummy,pcldedCounterbuffer);
				} /*(ildedend == 0)*/
				ildedstart = ildedcounters[ilCnt];
				ildedend = ildedstart;
			}
			else
			{
				ildedend = ildedcounters[ilCnt];
			}
		} /* for all arrayelements */
		strcpy(pcldedCounterbuffer,pcldummy);
	} /* create dedicated checkin counter string */
	
	
	
	/******** Part 4: insert the string into the linebuffer ********/	
	/* get position from ckif field in field list and extract ckif value from linebuffer */
	for (ilFieldPos=1;ilFieldPos < igFieldCount;ilFieldPos++)
	{
		GetDataItem(pcldummy, prgDynCfg->fields,ilFieldPos, CFG_DEL, "", " \0");
		if (strstr(pcldummy, "ckif") != NULL || strstr(pcldummy, "CKIF") != NULL) {break;}
	} /* for all fields */
	
	/* get position from ckif field in linebuffer list and get length of actual value*/
	for (ilCnt=1;ilCnt < ilFieldPos;ilCnt++)
	{	
		GetDataItem(pcldummy, pcpBuffer,ilCnt, ucgData_del, "", " \0");
		ilFieldlength	= ilFieldlength + strlen(pcldummy) +1;
	} /* for all values */	
	GetDataItem(pcldummy, pcpBuffer,ilFieldPos, ucgData_del, "", " \0");
	ilValuelength 	= strlen(pcldummy);
	if (ilValuelength > 0)
	{
		memmove((void *) &pcpBuffer[ilFieldlength],&pcpBuffer[ilFieldlength + 
				ilValuelength],(size_t) 
				strlen(&pcpBuffer[ilFieldlength + ilValuelength])); 
		pcpBuffer[strlen(pcpBuffer) - ilValuelength] = 0x00;
		
		memmove((void *) &pclLineBuffer_withurno[strlen(pcpUrno) + 1 +ilFieldlength],
				&pclLineBuffer_withurno[strlen(pcpUrno) + 1 +ilFieldlength + ilValuelength],
				(size_t) strlen(&pclLineBuffer_withurno[strlen(pcpUrno) + 1 +ilFieldlength + ilValuelength])); 
		pclLineBuffer_withurno[strlen(pclLineBuffer_withurno) - ilValuelength] = 0x00;
		
	}



	/************************************************/			
	/* CONFIGURABLE NATURE PROCESSING		*/	
	/************************************************/	
	memset(pcldummy2,0x00,MAX_BUFFER_LEN);
	ilNaturecount = GetNoOfElements(prgDynCfg->checkin_calc,CFG_DEL);
	dbg(DEBUG,"\n\ncom <%s> ded <%s> comded <%s>",pclcomCounterbuffer,pcldedCounterbuffer,pclCounterbuffer);
	TrimRight(pcpnature);
	
	/* Now get the values of FLTI and add the nature to this value, if configured */
	/* EG interntional flight -> FLTI = I with nature 01002 -> nature I01002 */
	if (strcmp(prgDynCfg->use_flti,"YES")==0) {
		/* Prepare SQL statement */
		/* reset buffers */
		memset(pclSqlBuf,0x00,S_BUFF);
		memset(pclDataArea,0x00,S_BUFF);
		memset(pclOraErrorMsg,0x00,MAX_BUFFER_LEN);
	
		/* build sql statement*/								
		 sprintf(pclSqlBuf,"SELECT FLTI FROM AFT%s WHERE  URNO ='%s'",pcgDefTabEnd,pcpUrno);	
	
		/* set type of sql call */
		slSqlFunc 	= START;
	
		/* run sql */
		if ((ilRC = sql_if(slSqlFunc,&slCursor,pclSqlBuf,pclDataArea))!= RC_SUCCESS) 
		{
			if (ilRC == NOTFOUND)
			{	
				ilRC = RC_SUCCESS;
			}
			else
			{
				ilRC = RC_FAIL;
				get_ora_err(ilRC,pclOraErrorMsg);	
				dbg(DEBUG,"ROWCreate: ORA-ERR <%s>  <%s>",pclOraErrorMsg, pclSqlBuf);
			}
		} /* if ((ilRC = sql_if */
	
		/* close cursor */
		close_my_cursor(&slCursor);
	
		sprintf(pcldummy,"%s",pclDataArea);
		TrimRight(pcldummy);
		strcat(pcldummy,pcpnature);
		strcpy(pcpnature,pcldummy);
	} /* if FLTI is part of nature */
	
	
	for (ilCnt = 1; ilCnt <= ilNaturecount; ilCnt ++) {
	
		/* reset buffers*/
		memset(pcldummy,0x00,XS_BUFF);
		memset(pcldummy3,0x00,XS_BUFF);

		/* Extract nature of configuration entry */
		GetDataItem(pcldummy, prgDynCfg->checkin_calc,ilCnt, CFG_DEL, "", " \0");
		GetDataItem(pcldummy2, pcldummy,1, ',', "", " \0");
		
		/*Now check for * in entry */
		if (strchr(pcldummy2,'*')!=NULL) {
			strcpy (pcldummy3,pcldummy2);
			memset(pcldummy2,0x00,MAX_BUFFER_LEN);
			GetDataItem(pcldummy2, pcldummy3,1, '*', "", " \0");
		} 
		ilcomparelength = strlen(pcldummy2);
		
		if (pcpnature!=NULL) {
			/* found the nature?? */
			/* Find * */
			/* compare strings until position * */
			if (strncmp(pcldummy2,pcpnature,ilcomparelength)==0) {
					/* Get values for main and code share */
					GetDataItem(pclmainflight, pcldummy,2, ',', "", " \0");
					GetDataItem(pclcodeshareflight, pcldummy,3, ',', "", " \0");
					/*MAIN*/
					memset(pclmaincounterstring,0x00,XS_BUFF);
					memset(pclcodesharecounterstring,0x00,XS_BUFF);
					if ((strcmp(pclmainflight,"A")==0) || (strcmp(pclmainflight,"S")==0)) {
							sprintf(pclmaincounterstring,"%s",pclCounterbuffer);
							dbg(DEBUG,"\n\npclmaincounterstring <%s> with comded<%s>",pclmaincounterstring,pclCounterbuffer);
					} else if ((strcmp(pclmainflight,"C")==0) || (strcmp(pclmainflight,"P")==0)) {
							sprintf(pclmaincounterstring,"%s",pclcomCounterbuffer);
							dbg(DEBUG,"\n\npclmaincounterstring <%s> with com<%s>",pclmaincounterstring,pclcomCounterbuffer);
					} else if (strcmp(pclmainflight,"D")==0) {
							sprintf(pclmaincounterstring,"%s",pcldedCounterbuffer);
							dbg(DEBUG,"\n\npclmaincounterstring <%s> with ded<%s>",pclmaincounterstring,pcldedCounterbuffer);
					} else {
							sprintf(pclmaincounterstring,"%s",pclCounterbuffer);
					}
					/*CSF*/
					if (strcmp(pclcodeshareflight,"A")==0) {
							sprintf(pclcodesharecounterstring,"%s",pclcsfCounterbuffer);
					} else if (strcmp(pclcodeshareflight,"C")==0) {
							sprintf(pclcodesharecounterstring,"%s",pclcsfcomCounterbuffer);
					} else if (strcmp(pclcodeshareflight,"D")==0) {
							sprintf(pclcodesharecounterstring,"%s",pcldedCounterbuffer);
                                        } else if (strcmp(pclcodeshareflight,"S")==0) {
                                                        strcpy(pclcodesharecounterstring,pclmaincounterstring);
					} else if (strcmp(pclcodeshareflight,"P")==0) {
							sprintf(pclcodesharecounterstring,"%s",pclcomCounterbuffer);
					} else {
							sprintf(pclcodesharecounterstring,"%s",pclcsfCounterbuffer);
					}

			
					/* ASSIGN THE VALUES TO MAIN AND CODE SHARE FLIGHTS */
					/* replace in MAIN flight buffer with com&ded value*/
					if ((ilRC = InsertIntoString(pcpBuffer, ilFieldlength, pclmaincounterstring))!= RC_SUCCESS)
					{
							dbg(DEBUG,"CHECKINCalculateCounter: InsertIntoString for pcpBuffer failed.");
					}
					/*copy to buffer*/
		
					/* replace other buffer with com&ded value*/
					if ((ilRC = InsertIntoString(&pclLineBuffer_withurno[strlen(pcpUrno) + 1], ilFieldlength, pclcodesharecounterstring))!= RC_SUCCESS)
					{
							dbg(DEBUG,"CHECKINCalculateCounter: InsertIntoString for pclLineBuffer_withurno failed.");
					}
					blnature = TRUE;
					break;
			}
		}/*nature=null?*/		
	} /* for all natures */
	
	
	/* UNKNOWN NATURE ?? */
	if (blnature == FALSE) {
		/* Assign com & ded counters to both */
			if ((ilRC = InsertIntoString(pcpBuffer, ilFieldlength, pclCounterbuffer))!= RC_SUCCESS)
			{
					dbg(DEBUG,"CHECKINCalculateCounter: InsertIntoString MAIN failed.");
			}
			/*copy to buffer*/
		
			/* replace other buffer with com&ded value*/
			if ((ilRC = InsertIntoString(&pclLineBuffer_withurno[strlen(pcpUrno) + 1], ilFieldlength, pclcsfCounterbuffer))!= RC_SUCCESS)
			{
					dbg(DEBUG,"CHECKINCalculateCounter: InsertIntoString CSF failed.");
			}
	}

	return ilRC;
	
} /* CHECKINCalculateCounter */
/* ******************************************************************** */
/* The ROWFill(pclLineBuffer, pclUrno,.. routine	  	        */
/*									*/
/*	This procedure adds additional rows to the HTML page		*/
/*	The rows can be created from Code share flights or		*/
/*	from gate,belt,checkin aso data, which first value is 		*/
/*	a number. If there is a data gap, eg no data for belts 3-7	*/
/* 	then empty rows with the incremental number will be created.	*/
/*									*/
/* ******************************************************************** */
static int ROWFill(char *pclLineBuffer_withurno,char *pclUrno,char *pclReadPointer,
	int *pilColor_1,int *pilColor_2,int *pilColor_Cnt)
{
	int 	ilRC = RC_SUCCESS;
	int 	ilactualvalue 	= 0;		/* actual value of first field in line buffer */
	int 	ilfixrows 	= 0;		/* desired amount of fixed rows */
	int	ilCnt;
	int 	ilColor_1;			/* variables for html row color processing */
	int 	ilColor_2;
	int 	ilColor_Cnt;
	char 	*pclRowColor 	= NULL;		/* color of htmhl table row */
	char	pcldummylinebuffer[MAX_BUFFER_LEN];		/* buffer for empty row */
	

/*********** process additional rows for gate, belt, parking position etc **********/
if (strcmp(prgDynCfg->addrows, prgDynCfg->command) != 0)
{
	/* set actual command */
	strcpy(pfgROWPrevCommand,prgDynCfg->addrows);
	return ilRC;
} /* Row processing active */


/* if eof sign set last row value */ 
if (strstr(pclReadPointer,prgDynCfg->eof_sign) == NULL)
{
	/* get actual value from line buffer: take always the second value */		
	ilactualvalue = atoi(GetDataField(pclLineBuffer_withurno, 1, ucgData_del));		
	if ((strstr(GetDataField(pclLineBuffer_withurno, 1, ucgData_del),"H")!=NULL) && (strstr(pcgDefHomeAp,"WAW")!=NULL))
	{
		ilactualvalue = atoi(prgDynCfg->fixrowend) + 1;
	}
}
else
{
	ilactualvalue 	= atoi(prgDynCfg->fixrowend) + 1;	
} /* eof sign ??*/

/*add HANGA row if position section is active*/

/* New command?	If yes, then delete old flags */
if (strstr(prgDynCfg->addrows, pfgROWPrevCommand) == NULL)
{
	igROWCount	= 1;
	igROWPrevValue 	= 1;
} /*New command?*/

/* set row color variables */
ilColor_1 	= *pilColor_1;		
ilColor_2 	= *pilColor_2;		
ilColor_Cnt 	= *pilColor_Cnt;

/* set amount of fix rows */
/* process special values for the EXIT Page: exit 1 & 8  */
if (strstr(prgDynCfg->addrows, pcgExit) != NULL)
{
	if ((igROWPrevValue == 1) || (igROWPrevValue == 8))
	{
		ilfixrows = 3;
	} else {
		ilfixrows = atoi(prgDynCfg->fixrowamount);
	} 
} else {
	ilfixrows = atoi(prgDynCfg->fixrowamount);
} /* set amount of fix rows to show */

/* set first value if specified in config file */
if (igROWPrevValue < atoi(prgDynCfg->fixrowstart)) {igROWPrevValue = atoi(prgDynCfg->fixrowstart);}

/* fill rows from last value till the actual one */
/* omit already written rows */
/*if (ilactualvalue - igROWPrevValue > 0) {ilColor_Cnt++;}*/
while (ilactualvalue - igROWPrevValue > 0)
{
	/* build linebuffer with lastvalue as first entry */
	memset(pcldummylinebuffer,0x00,MAX_BUFFER_LEN);

	sprintf(pcldummylinebuffer,prgDynCfg->fixrowformat, igROWPrevValue);
	for (ilCnt=0;ilCnt < igFieldCount-1;ilCnt++)
	{
		strcat(pcldummylinebuffer,prgDynCfg->data_del);
	} /*for (ilCnt*/
			
	/* write data to file */		
	while (igROWCount <= ilfixrows) 
	{ilColor_Cnt++;	
		/* setting the row color */
		if (ilColor_Cnt == ilColor_1)
		{
			pclRowColor = prgDynCfg->td_bgc1;
			ilColor_1 = ilColor_1 + 2;
		}
		else
		{
			pclRowColor = prgDynCfg->td_bgc2;	
			ilColor_2 = ilColor_2 + 2;
		} /* setting the row color */

		/* Write data*/
		if ((ilRC = ConvertToTableData(pcldummylinebuffer,pclRowColor))!= RC_SUCCESS)
		{
			dbg(DEBUG,"ROWFill: Creation of additional rows failed with value: %s.",pcldummylinebuffer);	
		}
		ilfixrows--;

						
	} /* add empty rows */		

	/* reset rowcount for new belt,gate aso */
	/* go to next counter, belt aso */
	igROWCount = 1;			
	igROWPrevValue++;
	
	/* set amount of fix rows */
	/* process special values for the EXIT Page: exit 1 & 8  */
	if (strstr(prgDynCfg->addrows, pcgExit) != NULL)
	{
		if ((igROWPrevValue == 1) || (igROWPrevValue == 8))
		{
			ilfixrows = 3;
		} else {
			ilfixrows = atoi(prgDynCfg->fixrowamount);
		} 
	} else {
		ilfixrows = atoi(prgDynCfg->fixrowamount);
	} /* set amount of fix rows to show */

} /* while */

/* save last written value, special treatment for parking section */	
if (strstr(prgDynCfg->addrows, pcgParking) != NULL)
{
	igROWPrevValue = ilactualvalue+1;
} else {
	igROWPrevValue = ilactualvalue;
}
/* set actual command */
strcpy(pfgROWPrevCommand,prgDynCfg->addrows);

/* set row color variables for the calling function CreateTableData*/
*pilColor_1	=	ilColor_1;		
*pilColor_2	=	ilColor_2;		
*pilColor_Cnt	=	ilColor_Cnt;
		

return ilRC;


} /*ROWFill */

/* ******************************************************************** */
/* The ROWCreate(pclLineBuffer, pclUrno,.. routine	  	        */
/*									*/
/*	This procedure adds additional rows to the HTML page		*/
/*	containing the Code share flights				*/
/*									*/
/* ******************************************************************** */
static int ROWCreate(char *pclLineBuffer_withurno,char *pclUrno,char *pclReadPointer,
	int *pilColor_1,int *pilColor_2,int *pilColor_Cnt)
{
	int 	ilRC = RC_SUCCESS;
	int 	ilColor_1;			/* variables for html row color processing */
	int 	ilColor_2;
	int 	ilColor_Cnt;
	int	iljfno_len 	= 0;		/* length of one code share flight */
	int	ilFieldPos;			/* position of flno filed in linebuffer */
	int	ilstartposdata 	= 0;		/* start position for extracting values from sql databuffer */
	int	iljfnonr	= 0;		/* no of csf to show */
	int	ilcheckinpos	= 2;		/* position of first code share checkin entry in CKIF position of buffer */
	char 	*pclRowColor 	= NULL;		/* color of htmhl table row */
	char	pcldummylinebuffer[MAX_BUFFER_LEN];		/* buffer for empty row */
	
	char 	pclDataArea[S_BUFF];		/* buffers for the jfno-afttab SQL call */
	char 	pclDataAreadummy[S_BUFF];	
	char 	pclSqlBuf[S_BUFF];
	char 	pclOraErrorMsg[MAX_BUFFER_LEN];
	short 	slSqlFunc;
	short 	slCursor 	= 0;
	char 	pclCsf[XS_BUFF];		/* buffer for codeshareflights */
	char 	pclCheckin[XS_BUFF];		/* buffer for checkin counter of codeshareflights */
	char 	pclCheckinM[XS_BUFF];		/* buffer for new counter values*/
	char 	pclFLTI[XS_BUFF];		
	int	iljfnopos 	= 0;		/* position for string to copy in linebufferdummy */
	char 	pcllinebufferpos[XS_BUFF];	/* buffer for evaluating iljfnopos */
	char 	pclOldflightvalue[XS_BUFF];	/* buffer for main flight value */
	char 	pclOldcheckinvalue[XS_BUFF];	/* buffer for checkin list value */
	
/* set row color variables */
ilColor_1 	= *pilColor_1;		
ilColor_2 	= *pilColor_2;		
ilColor_Cnt 	= *pilColor_Cnt + 1;


/*********** process multiple datarecords for code share flights **********/
if ((strstr(prgDynCfg->addrows, "JFNO") != NULL) && (strstr(pclUrno,prgDynCfg->eof_sign) == NULL))
{
	/************** Select values from afttab for further processing **************/
	/* reset buffers */
	memset(pclSqlBuf,0x00,S_BUFF);
	memset(pclDataArea,0x00,S_BUFF);
	memset(pclDataAreadummy,0x00,S_BUFF);
	memset(pclOraErrorMsg,0x00,MAX_BUFFER_LEN);
		
	/* get conf file parameters for no of values to show */
	iljfnonr = atoi(prgDynCfg->jfno_nr);
	if (iljfnonr == 0 || strlen(prgDynCfg->jfno_nr) <= 0) {iljfnonr = 50;}
	
	/* build sql statement*/								
	 sprintf(pclSqlBuf,"SELECT JFNO,FLTI FROM AFT%s WHERE  URNO ='%s'",pcgDefTabEnd,pclUrno);	
	
	/* set type of sql call */
	slSqlFunc 	= START;
	
	/* run sql */
	if ((ilRC = sql_if(slSqlFunc,&slCursor,pclSqlBuf,pclDataArea))!= RC_SUCCESS) 
	{
		if (ilRC == NOTFOUND)
		{
			ilRC = RC_SUCCESS;
		}
		else
		{
			ilRC = RC_FAIL;
			get_ora_err(ilRC,pclOraErrorMsg);	
			dbg(DEBUG,"ROWCreate: ORA-ERR <%s>  <%s>",pclOraErrorMsg, pclSqlBuf);
		}
	} /* if ((ilRC = sql_if */
	
	/* close cursor */
	close_my_cursor(&slCursor);
		
	/* get  length of csf*/
	iljfno_len = atoi(prgDynCfg->jfno_len);
	
	/* extract string to copy from linebuffer */
	/* the first two elements from the linebuffer have to be ignored for the extraction*/
	/* get position from flno field in field list and extract flno value from linebuffer */
	memset(pclOldflightvalue,0x00,XS_BUFF);
	for (ilFieldPos=1;ilFieldPos < igFieldCount;ilFieldPos++)
	{
		GetDataItem(pclOldflightvalue, prgDynCfg->fields,ilFieldPos, CFG_DEL, "", " \0");
		if (strstr(pclOldflightvalue, "flno") != NULL || strstr(pclOldflightvalue, "FLNO") != NULL) {break;}
	} /* for all fields */		
	memset(pclOldflightvalue,0x00,XS_BUFF);
	GetDataItem(pclOldflightvalue, pclLineBuffer_withurno,ilFieldPos+1, ucgData_del, "", " \0");


	/* Now do the same for the checkin values in CKIF 	*/
	/* They are as following: eg 01<BR>07-10#22-26#01 	*/
	/* where pos 1 = main flight aso			*/
	memset(pclOldcheckinvalue,0x00,XS_BUFF);
	for (ilFieldPos=1;ilFieldPos < igFieldCount;ilFieldPos++)
	{
		GetDataItem(pclOldcheckinvalue, prgDynCfg->fields,ilFieldPos, CFG_DEL, "", " \0");
		if (strstr(pclOldcheckinvalue, "ckif") != NULL || strstr(pclOldcheckinvalue, "CKIF") != NULL) {break;}
	} /* for all fields */		
	memset(pclOldcheckinvalue,0x00,XS_BUFF);
	GetDataItem(pclOldcheckinvalue, pclLineBuffer_withurno,ilFieldPos+1, ucgData_del, "", "\0\0");
	
	
	/* WAW: Special: 	Now process flights with FLTI = M, where another Row with the 	*/
	/*			Domestic checkins has to be added.				*/

	/* build linebuffer with flight as first entry */
	memset(pcldummylinebuffer,0x00,MAX_BUFFER_LEN);
	strcpy(pcldummylinebuffer,&pclLineBuffer_withurno[strlen(pclUrno)+1]);


	/* Extract FLTI */
	memset(pclFLTI,0x00,XS_BUFF);
	if (strlen(pclDataArea) >= 1)  {
		GetDataItem(pclDataAreadummy, pclDataArea, 1, '\0', "", "\0");
		GetDataItem(pclFLTI, pclDataArea, 2, '\0', "", "\0");			
	} else {
		sprintf(pclFLTI,"%s"," ");
	}
	
	if ((strchr(pclFLTI,'M')!=NULL) && (strstr(pcgDefHomeAp,"WAW")!=NULL)) {
		
		/* Now replace the checkin value */
		if ((strchr(pclOldcheckinvalue,' ')==NULL) && (strlen(pclOldcheckinvalue)> 0)) {
			memset(pclCheckinM,0x00,XS_BUFF);
			sprintf(pclCheckinM,"|%s|",pclOldcheckinvalue);
			strcpy(pclOldcheckinvalue,pclCheckinM);
			
			if ((ilRC = SearchStringAndReplace(pcldummylinebuffer,pclOldcheckinvalue,prgDynCfg->checkin_dom_string))!= RC_SUCCESS)
			{
				dbg(DEBUG,"ROWCreate 1: Search and replace for checkin failed.");
			}
		}
		if (strchr(pclOldcheckinvalue,' ')!=NULL) {
			if ((ilRC = SearchStringAndReplace(pcldummylinebuffer,"| |",prgDynCfg->checkin_dom_string))!= RC_SUCCESS)
			{
				dbg(DEBUG,"ROWCreate 2: Search and replace for checkin failed.");
			}
		}

		/* setting the row color */
		if (ilColor_Cnt == ilColor_1)
		{
			pclRowColor = prgDynCfg->td_bgc1;
			ilColor_1 = ilColor_1 + 2;
		}
		else
		{
			pclRowColor = prgDynCfg->td_bgc2;	
			ilColor_2 = ilColor_2 + 2;
		} /* setting the row color */

		/* Write data*/
		if ((ilRC = ConvertToTableData(pcldummylinebuffer,pclRowColor))!= RC_SUCCESS)
		{
			dbg(DEBUG,"ROWCreate: Creation of additional rows failed.");	
		}
	
		/*set next row color */
		ilColor_Cnt++;	

		/* set row color variables for the calling function CreateTableData*/
		*pilColor_1	=	ilColor_1;		
		*pilColor_2	=	ilColor_2;		
		*pilColor_Cnt	=	ilColor_Cnt - 1;		


	} /* FLTI == M ?? */
	
	/*** quit, if no Code share flights available ***/
	if (strlen(pclDataArea) >= 1)  {
		memset(pclDataArea,0x00,S_BUFF);
		sprintf(pclDataArea,"%s",pclDataAreadummy);				
	} 

	if (strlen(pclDataArea) <= 1) {return ilRC;}
	
	/* split dataarea in separate flights and write them into linebuffer*/	
	while (ilstartposdata < strlen(pclDataArea))
	{
		/* reseet buffers*/
		memset(pclCsf,0x00,XS_BUFF);
		memset(pclCheckin,0x00,XS_BUFF);
		memset(pclCheckinM,0x00,XS_BUFF);
		
		/* copy next flight */
		strncpy(pclCsf,&pclDataArea[ilstartposdata],iljfno_len);
		pclCsf[iljfno_len] = 0x00;
				
		/* build linebuffer with flight as first entry */
		memset(pcldummylinebuffer,0x00,MAX_BUFFER_LEN);
		strcpy(pcldummylinebuffer,&pclLineBuffer_withurno[strlen(pclUrno)+1]);
				
		/* replace */
		if ((ilRC = SearchStringAndReplace(pcldummylinebuffer,pclOldflightvalue,pclCsf))!= RC_SUCCESS)
		{
			dbg(DEBUG,"ROWCreate: Search and replace for flno failed.");
		}
		
		/* extract the current checkin value */
		if (strchr(pclOldcheckinvalue,'#')!=NULL) {
			GetDataItem(pclCheckin, pclOldcheckinvalue,ilcheckinpos,'#', "", " \0");
		} else {
			/* GetDataItem(pclCheckin, pclOldcheckinvalue,1,'<', "", " \0"); */
			strcpy(pclCheckin,pclOldcheckinvalue);		
		}

		/* Now replace the checkin value */
		if (strchr(pclOldcheckinvalue,'#')!=NULL) {
			if (strncmp(pclCheckin,"|",1) < 0) {
				memset(pclCheckinM,0x00,XS_BUFF);
				sprintf(pclCheckinM,"|%s",pclCheckin);
				strcpy(pclCheckin,pclCheckinM);
			}
			if (strncmp(pclOldcheckinvalue,"|",1) < 0) {
				memset(pclCheckinM,0x00,XS_BUFF);
				sprintf(pclCheckinM,"|%s",pclOldcheckinvalue);
				strcpy(pclOldcheckinvalue,pclCheckinM);
			}

			if ((ilRC = SearchStringAndReplace(pcldummylinebuffer,pclOldcheckinvalue,pclCheckin))!= RC_SUCCESS)
			{
				dbg(DEBUG,"ROWCreate 3: Search and replace for checkin failed.");
			}
		}
		
		/* increase flags */
		ilstartposdata = ilstartposdata + iljfno_len;
		ilcheckinpos++;
		
		/* setting the row color */
		if (ilColor_Cnt == ilColor_1)
		{
			pclRowColor = prgDynCfg->td_bgc1;
			ilColor_1 = ilColor_1 + 2;
		}
		else
		{
			pclRowColor = prgDynCfg->td_bgc2;	
			ilColor_2 = ilColor_2 + 2;
		} /* setting the row color */

		/* Write data*/
		if ((ilRC = ConvertToTableData(pcldummylinebuffer,pclRowColor))!= RC_SUCCESS)
		{
			dbg(DEBUG,"ROWCreate: Creation of additional rows failed.");	
		}
	
		/*set next row color */
		ilColor_Cnt++;		
		
		
		/* WAW: Special: 	Now process flights with FLTI = M, where another Row with the 	*/
		/*			Domestic checkins has to be added.				*/
		if (strchr(pclFLTI,'M')!=NULL) {

			if (strncmp(pclCheckin,"|",1) < 0) {
				memset(pclCheckinM,0x00,XS_BUFF);
				sprintf(pclCheckinM,"|%s",pclCheckin);
				strcpy(pclCheckin,pclCheckinM);
			}

			if ((ilRC = SearchStringAndReplace(pcldummylinebuffer,pclCheckin,prgDynCfg->checkin_dom_string))!= RC_SUCCESS)
			{
				dbg(DEBUG,"ROWCreate: Search and replace for checkin failed.");
			}

			/* increase flags */
			ilstartposdata = ilstartposdata + iljfno_len;
			ilcheckinpos++;
		
			/* setting the row color */
			if (ilColor_Cnt == ilColor_1)
			{
				pclRowColor = prgDynCfg->td_bgc1;
				ilColor_1 = ilColor_1 + 2;
			}
			else
			{
				pclRowColor = prgDynCfg->td_bgc2;	
				ilColor_2 = ilColor_2 + 2;
			} /* setting the row color */

			/* Write data*/
			if ((ilRC = ConvertToTableData(pcldummylinebuffer,pclRowColor))!= RC_SUCCESS)
			{
				dbg(DEBUG,"ROWCreate: Creation of additional rows failed.");	
			}
	
			/*set next row color */
			ilColor_Cnt++;		
		} /* FLTI == M ?? */
		 
	} /* while */	
}

/* set row color variables for the calling function CreateTableData*/
*pilColor_1	=	ilColor_1;		
*pilColor_2	=	ilColor_2;		
*pilColor_Cnt	=	ilColor_Cnt - 1;		


return ilRC;

} /* function ROWCreate*/

/* ******************************************************************** */
/* The CSFProcess() routine						*/
/*									*/
/*	this process processes the jfno field information		*/
/*	input: 	..|LH 4711  LO 1234  KL 3456  HO 9999  |..		*/
/*	output:	..|LH 4711  <BR>LO 1234 				*/
/*									*/
/* ******************************************************************** */
static int CSFProcess(char *pcpBuffer)
{
	int 	ilRC = RC_SUCCESS;
	int	ilCnt		= 0;
	int 	ilCnt2		= 0;
	int 	ilCnt3		= 0;
	int	ilCurrentpos 	= 0;		/* POSITION IN REPLACEBUFFER */
	int	iljfnolength;			/* length of one jfno flight value */
	int	iljfnonr;			/* no of csf to show*/
	int	ilFieldlength	= 0;		/* length of jfno value */
	int	ilFieldPos;			/* position of jfno field */
	char	pclCSFbuffer[100];		/* buffer for raw csf string	*/
	char	pclCFSreplacestring[150];	/* buffer for string: eg LH 4711<br>LOT 4811	*/
	char	pcldummy[MAX_BUFFER_LEN];		

	
	
	/** Is field jfno in fieldbuffer?? If not leave function. 	**/	
	if (strstr(prgDynCfg->fields, "jfno") == NULL && strstr(prgDynCfg->fields, "JFNO") == NULL)
	{
		return ilRC;
	} /* if (strstr(pclTmpKeyWord, "jfno") */
	
	
	/* reset buffers */
	memset(pclCSFbuffer,0x00,100);
	memset(pclCFSreplacestring,0x00,150);	

		
	/* get position from jfno field in field list and extract jfno value from linebuffer */
	for (ilFieldPos=1;ilFieldPos < igFieldCount;ilFieldPos++)
	{
		GetDataItem(pcldummy, prgDynCfg->fields,ilFieldPos, CFG_DEL, "", " \0");
		if (strstr(pcldummy, "jfno") != NULL || strstr(pcldummy, "JFNO") != NULL) {break;}
	} /* for all fields */		
	ilFieldlength = GetDataItem(pclCSFbuffer, pcpBuffer,ilFieldPos, ucgData_del, "", " \0");
	
	
	/* get conf file parameters for no of values to show and length of jfno flight values */
	iljfnonr = atoi(prgDynCfg->jfno_nr);
	if (iljfnonr == 0 || strlen(prgDynCfg->jfno_nr) <= 0) {return ilRC;}
	iljfnolength = atoi(prgDynCfg->jfno_len);
	if (iljfnolength == 0 || strlen(prgDynCfg->jfno_len) <= 0) {return ilRC;}
		
		
	/* select first two elements and create string for replacement*/
	if (ilFieldlength > iljfnolength)
	{
	   while (iljfnonr>0)
	   {	
		for (ilCnt=0;ilCnt<iljfnolength;ilCnt++)
		{
				pclCFSreplacestring[ilCnt+ilCurrentpos] = pclCSFbuffer[(ilCnt2*iljfnolength)+ilCnt];
		} /* for */
		
    		/* trim off right spaces */
		for (ilCnt3 = (strlen(pclCFSreplacestring) - 1); ilCnt3 >= 0 && isspace(pclCFSreplacestring[ilCnt3]); ilCnt3--);
		ilCurrentpos = ++ilCnt3;
		
		/* add carriage return sign */
		if (iljfnonr>1) {strcpy(&pclCFSreplacestring[ilCnt3],"<BR>");}
		
		/* set current position */
		ilCurrentpos = ilCurrentpos + 4;
		ilCnt2++;			
		iljfnonr--;
	    } /* while*/
	}
	else
	{
		return ilRC;
	} /* ilFieldlength > 0 */
	
	
	/* replace */
	if ((ilRC = SearchStringAndReplace(pcpBuffer,pclCSFbuffer,pclCFSreplacestring))!= RC_SUCCESS)
	{
		dbg(DEBUG,"CSFProcess: Search and replace failed.");
	}
	
	dbg(DEBUG,"CSFProcess: pcpBuffer: %s",pcpBuffer);
	
	/* go home */
	return ilRC;
} /* CSFProcess */

/* ******************************************************************** */
/* The ConvertToTableData() routine	  	        	                        */
/* ******************************************************************** */
static int ConvertToTableData(char *pcpBuffer,char *pcpRowColor)
{
	int ilRC = RC_SUCCESS;
	
	int ilCnt = 0;
	char pclHeader[MAX_BUFFER_LEN];
	char pclCell[MAX_BUFFER_LEN];
	
	char *pclFields = prgDynCfg->fields; 
	char *pclAlign = prgDynCfg->td_align; 
	char *pclValign = prgDynCfg->td_valign;
	char *pclHeight = prgDynCfg->td_height;
	/* prgDynCfg->td_bgcolor = set by above parameter pcpRowColor */
	char *pclNowrap = prgDynCfg->td_nowrap;
	char *pclFontf = prgDynCfg->td_fontf;
	char *pclFontc = prgDynCfg->td_fontc1;
	char *pclFonts = prgDynCfg->td_fonts;
	char *pclData = pcpBuffer;
	
	memset(pclHeader,0x00,MAX_BUFFER_LEN);
	memset(pclCell,0x00,MAX_BUFFER_LEN);
	
	TagHtmlLine(TABLE_ROW,pclHeader,OPEN_TAG);
	TagHtmlLine(TABLE_ROW,pclHeader,CLOSE_TAG);
		
	/* opening HTML-line and creating the table-data-row */
	for (ilCnt = 0; ilCnt < igFieldCount; ilCnt ++)
	{
		TagHtmlLine(TABLE_DATA,pclCell,OPEN_TAG);
		pclAlign = CreateCellAttribute(pclAlign,"align=\"%s\"",pclCell);
		pclValign = CreateCellAttribute(pclValign,"valign=\"%s\"",pclCell);
		pclHeight = CreateCellAttribute(pclHeight,"height=\"%s\"",pclCell);
		pcpRowColor = CreateCellAttribute(pcpRowColor,"bgcolor=\"%s\"",pclCell);
                if ((strstr(pclNowrap,"YES") != NULL) || (strstr(pclNowrap,"yes") != NULL)) {
                        pclNowrap = CreateCellAttribute(pclNowrap,"nowrap=\"%s\"",pclCell);
                }
		TagHtmlLine(TABLE_DATA,pclCell,CLOSE_TAG);
		TagHtmlLine(FONT,pclCell,OPEN_TAG);
		pclFontf = CreateCellAttribute(pclFontf,"face=\"%s\"",pclCell);
		pclFontc = CreateCellAttribute(pclFontc,"color=\"%s\"",pclCell);
		pclFonts = CreateCellAttribute(pclFonts,"size=\"%s\"",pclCell);
		TagHtmlLine(FONT,pclCell,CLOSE_TAG);

		pclData = CreateCellData(pclData,"%s",pclCell);
		
		TagHtmlLine(FONT,pclCell,END_TAG);
		
		/* writing the cell layout AND text to the html-file */
		TagHtmlLine(TABLE_DATA,pclCell,END_TAG);
	}	
	TagHtmlLine(TABLE_ROW,pclHeader,END_TAG);
	return ilRC;
}

/* ******************************************************************** */
/* The WriteHtmlLine() routine	  	        	                        */
/* ******************************************************************** */
static void WriteHtmlLine(char *pcpBuffer,char *pcpContents,char *pcpSource)
{
	int 	ilLen = 0;
	char 	pclTmpBuffer[MAX_BUFFER_LEN];
	
	memset(pclTmpBuffer,0x00,MAX_BUFFER_LEN);

	if (strlen(pcpSource) > 0)
	{
		sprintf(pclTmpBuffer,pcpContents,pcpSource);
		strcat(pcpBuffer,pclTmpBuffer);
	}
}

/* ******************************************************************** */
/* The TagHtmlLine() routine	  	        	                        */
/* ******************************************************************** */
static void TagHtmlLine(int ipType, char *pcpBuffer, int ipTag)
{
	int ilRC;

	switch(ipType)
	{
		case TABLE:
				switch(ipTag)
				{
					case OPEN_TAG:
						strcat(pcpBuffer,"<table ");
						break;
					case CLOSE_TAG:
						strcat(pcpBuffer,">");
						break;
					case END_TAG:
						strcat(pcpBuffer,"</table>");
						break;
					default:
						dbg(DEBUG,"TagHtmlLine: unknown tag received.");	
						break;
				}
			break;
		case TABLE_ROW:
				switch(ipTag)
				{
					case OPEN_TAG:
						strcat(pcpBuffer,"<tr ");
						break;
					case CLOSE_TAG:
						strcat(pcpBuffer,">");
						if ((ilRC = WriteDataToFile(pcpBuffer)) != RC_SUCCESS)
						{
							dbg(DEBUG,"TagHtmlLine: WriteDataToFile() failed!");
						}
						memset(pcpBuffer,0x00,MAX_BUFFER_LEN);
						break;
					case END_TAG:
						strcat(pcpBuffer,"</tr>");
						if ((ilRC = WriteDataToFile(pcpBuffer)) != RC_SUCCESS)
						{
							dbg(DEBUG,"TagHtmlLine: WriteDataToFile() failed!");
						}
						memset(pcpBuffer,0x00,MAX_BUFFER_LEN);
						break;
					default:
						dbg(DEBUG,"TagHtmlLine: unknown tag received.");	
						break;
				}
			break;
		case TABLE_DATA:
				switch(ipTag)
				{
					case OPEN_TAG:
						strcat(pcpBuffer,"<td ");
						break;
					case CLOSE_TAG:
						strcat(pcpBuffer,">");
						break;
					case END_TAG:
						strcat(pcpBuffer,"</td>");
						if ((ilRC = WriteDataToFile(pcpBuffer)) != RC_SUCCESS)
						{
							dbg(DEBUG,"TagHtmlLine: WriteDataToFile() failed!");
						}
						memset(pcpBuffer,0x00,MAX_BUFFER_LEN);
						break;
					default:
						dbg(DEBUG,"TagHtmlLine: unknown tag received.");	
						break;
				}
			break;
		case TABLE_HEAD:
				switch(ipTag)
				{
					case OPEN_TAG:
						strcat(pcpBuffer,"<th ");
						break;
					case CLOSE_TAG:
						strcat(pcpBuffer,">");
						break;
					case END_TAG:
						strcat(pcpBuffer,"</th>");
						if ((ilRC = WriteDataToFile(pcpBuffer)) != RC_SUCCESS)
						{
							dbg(DEBUG,"TagHtmlLine: WriteDataToFile() failed!");
						}
						memset(pcpBuffer,0x00,MAX_BUFFER_LEN);
						break;
					default:
						dbg(DEBUG,"TagHtmlLine: unknown tag received.");	
						break;
				}
			break;
		case FONT:
				switch(ipTag)
				{
					case OPEN_TAG:
						strcat(pcpBuffer,"<font ");
						break;
					case CLOSE_TAG:
						strcat(pcpBuffer,">");
						break;
					case END_TAG:
						strcat(pcpBuffer,"</font>");
						break;
					default:
						dbg(DEBUG,"TagHtmlLine: unknown tag received.");	
						break;
				}
			break;
		case IMAGE:
				switch(ipTag)
				{
					case OPEN_TAG:
						strcat(pcpBuffer,"<img "); 
						break;
					case CLOSE_TAG:
						strcat(pcpBuffer,">");
						break;
					default:
						dbg(DEBUG,"TagHtmlLine: unknown tag received.");	
						break;
				}
			break;
		default:
			dbg(DEBUG,"TagHtmlLine: unknown type received.");	
			break;
	}
}

/* ******************************************************************** */
/* The CheckHtmlLine() routine	  	        	                        */
/* ******************************************************************** */
static int CheckHtmlLine(int ipType, char *pcpBuffer)
{
	int ilRC = RC_FAIL;
	
	switch(ipType)
	{
		case TABLE:
			if (strstr(pcpBuffer,"<table ") != NULL || strstr(pcpBuffer,"</table>")
					!= NULL)
			{
				ilRC = RC_SUCCESS;
			}
			break;
		case TABLE_ROW:
			if (strstr(pcpBuffer,"<tr") != NULL || strstr(pcpBuffer,"</tr>")
					!= NULL)
			{
				ilRC = RC_SUCCESS;
			}
			break;
		case TABLE_DATA:
			if (strstr(pcpBuffer,"<td") != NULL || strstr(pcpBuffer,"</td>")
					!= NULL)
			{
				ilRC = RC_SUCCESS;
			}
			break;
		case TABLE_HEAD:
			if (strstr(pcpBuffer,"<th") != NULL || strstr(pcpBuffer,"</th>")
					!= NULL)
			{
				ilRC = RC_SUCCESS;
			}
			break;
		case FONT:
			if (strstr(pcpBuffer,"<font") != NULL || strstr(pcpBuffer,"</font>")
					!= NULL)
			{
				ilRC = RC_SUCCESS;
			}
			break;
		default:
			dbg(DEBUG,"CheckHtmlLine: unknown type received.");	
			break;
	}
	return ilRC;
}

/* ******************************************************************** */
/* The SetEnv() routine	  	        	                        */
/* ******************************************************************** */
static int SetEnv()
{
	int 	ilRC = RC_SUCCESS;
	char 	pclFile[160];

	/* open the ASCII-file */
	*pclFile = '\0';
	if (bgAsciiFile == TRUE)
	{
		strcpy(pclFile,prgDynCfg->ascii_path);
		strcat(pclFile,prgDynCfg->ascii_file);
		dbg(DEBUG,"SetEnv: opening ascii-file (%s)",pclFile);
		if ((pfgAsciiFile = fopen(pclFile,"r")) == NULL)
		{
			ilRC =RC_FAIL;
		}
	}
	/* open the HTML-table file */
	*pclFile = '\0';
	if (bgHtmlTable == TRUE)
	{
		strcpy(pclFile,prgDynCfg->html_path);
		strcat(pclFile,prgDynCfg->html_table);
		dbg(DEBUG,"SetEnv: opening html-table file (%s)",pclFile);
		if ((pfgHtmlTable = fopen(pclFile,"w")) == NULL)
		{
			ilRC =RC_FAIL;
		}
	}

	/* open the HTML-dest file */
	*pclFile = '\0';
	if (bgHtmlDestFile == TRUE)
	{
		strcpy(pclFile,prgDynCfg->html_path);
		strcat(pclFile,prgDynCfg->html_dest_file);
		dbg(DEBUG,"SetEnv: setting html_dest_file (%s)",pclFile);
		pfgHtmlDestFile = (FILE *)pclFile;
	}
	/* setting the EndOfLine-Sign */
	if ((ilRC = SetEolSign()) != RC_SUCCESS)
	{
		ilRC = RC_FAIL;
	}
	
	if (bgData_del == TRUE)
	{
		ucgData_del = prgDynCfg->data_del[0];
	}
	else
	{
		ilRC = RC_FAIL;
	}
	return ilRC;
}

/* ******************************************************************** */
/* The CloseEnv() routine	  	        	                        */
/* ******************************************************************** */
static void CloseEnv(char *pcpSection)
{
	dbg(DEBUG,"CloseEnv: closing section <%s>",pcpSection);
	fclose(pfgAsciiFile);
	fflush(pfgHtmlTable);
	fclose(pfgHtmlTable);
}

/* ******************************************************************** */
/* The WriteDataToFile() routine       	                        */
/* ******************************************************************** */
static int WriteDataToFile(char *pcpData) 
{
	int ilRC = RC_SUCCESS;
	int ilBytes = 0;

	ilBytes = strlen(pcpData);
	/* writing data to html file */
	if ((ilBytes = fwrite(pcpData,ilBytes,1,pfgHtmlTable)) <= 0)
	{
		ilRC = RC_FAIL;
		dbg(DEBUG,"WriteDataToFile: fwrite() failed!");
	}
	else
	{
		if (fflush(pfgHtmlTable) != 0)
		{
			dbg(DEBUG,"WriteDataToFile: flushing data to file failed!");
		}
		fprintf(pfgHtmlTable,"\n");
	}
	return ilRC;	
}

/* ******************************************************************** */
/* The CreateCellAttribute() routine       	                        */
/* ******************************************************************** */
static char *CreateCellAttribute(char *pcpAttribPointer,char *pcpAttrib,
																	char *pcpHtmlLine) 
{
		char pclBuffer[MAX_BUFFER_LEN];
		memset(pclBuffer,0x00,MAX_BUFFER_LEN);

		pcpAttribPointer = CopyNextField(pcpAttribPointer,CFG_DEL,pclBuffer);
		if (!strlen(pclBuffer) > 0)
		{
			strcpy(pclBuffer,prgDynCfg->no_txt);
		}
		WriteHtmlLine(pcpHtmlLine,pcpAttrib,(char *)pclBuffer);
		return pcpAttribPointer;
}

/* ******************************************************************** */
/* The CreateCellData() routine       	                        */
/* ******************************************************************** */
static char *CreateCellData(char *pcpAttribPointer,char *pcpAttrib,char *pcpHtmlLine) 
{
	char pclBuffer[MAX_BUFFER_LEN];
	memset(pclBuffer,0x00,MAX_BUFFER_LEN);
	
	pcpAttribPointer =
			CopyNextField(pcpAttribPointer,ucgData_del,pclBuffer);
	if (!strlen(pclBuffer) > 0)
	{
		strcpy(pclBuffer,prgDynCfg->no_txt);
	}
	WriteHtmlLine(pcpHtmlLine,pcpAttrib,(char *)pclBuffer);
	return pcpAttribPointer;
}

/* ******************************************************************** */
/* The WriteComment(char *pcpComment) routine                          */
/* ******************************************************************** */
static void WriteComment(char *pcpComment)
{
	int 	ilRC = RC_SUCCESS;	
	char 	pclBegin[] = "<!-- ";
	char 	pclEnd[]	= " -->";
	char 	pclBuffer[128];

	*pclBuffer = '\0';

	strcpy(pclBuffer,pclBegin);
	if (strlen(pcpComment) > 126)
	{
		pcpComment[128] = '\0';
	}
	strcat(pclBuffer,pcpComment);
	strcat(pclBuffer,pclEnd);
	if ((ilRC = WriteDataToFile(pclBuffer)) != RC_SUCCESS)
	{
		dbg(DEBUG,"WriteComment: WriteDataToFile failed!");
	}
}

/* ******************************************************************** */
/* The SetEolSign() routine                          */
/* ******************************************************************** */
static int SetEolSign()
{
	int 	ilRC = RC_SUCCESS;
	int 	ilElements = 0;
	int 	ilCnt = 0;
	char 	*pclEolPointer = prgDynCfg->eol_sign;
	char 	pclBuffer[MAX_BUFFER_LEN];

	memset(pclBuffer,0x00,MAX_BUFFER_LEN);
	ilElements = GetNoOfElements(prgDynCfg->eol_sign,CFG_DEL);
	
	MALLOC(pcgEolSign,char*,strlen(prgDynCfg->eol_sign));
	memset(pcgEolSign,0x00,strlen(prgDynCfg->eol_sign));
	
	for (ilCnt = 1; ilCnt <= ilElements; ilCnt++)
	{
		pclEolPointer = CopyNextField(pclEolPointer,CFG_DEL,pclBuffer);
		if (strcmp(pclBuffer,"CR") == 0)
		{
			pcgEolSign[ilCnt-1] = 0x0D;
		}
		else
		{
			if (strcmp(pclBuffer,"LF") == 0)
			{
				pcgEolSign[ilCnt-1] = 0x0A;
			}
			else
			{
				dbg(DEBUG,"SetEolSign: unknown EOL_SIGN (%s) received.",pclBuffer);
				ilCnt = ilElements+1; /* leaving FOR */
				ilRC = RC_FAIL;
			}
		}
	}
	return ilRC;
}

/* ******************************************************************** */
/* The RemoveEolSign(char *pcpBuffer) routine                          */
/* ******************************************************************** */
static int RemoveEolSign(char *pcpBuffer)
{
	int ilRC = RC_SUCCESS;
	char *pclEolByte = NULL;
	
	if ((pclEolByte = strstr(pcpBuffer,pcgEolSign)) != NULL)
	{
		*pclEolByte = 0x00;
	}
	else
	{
		ilRC = RC_FAIL;
	}
	return ilRC;
}

/* ******************************************************************** */
/* The CheckDates(char *pcpBuffer)                           */
/* ******************************************************************** */
static void CheckDates(char *pcpBuffer)
{
	int ilRC = RC_SUCCESS;
	int ilNrOfFields = 0;
	int ilNrOfDateFields = 0;
	int ilNrOfDateFormats = 0;
	int ilFieldToCheck = 0;

	char *pclDatePointer = NULL;
	char *pclFieldPointer = NULL;
	char *pclDateFieldPointer = prgDynCfg->date_fields;
	char *pclDateFormatPointer = prgDynCfg->date_format;

	char pclField[MAX_BUFFER_LEN];
	char pclDateField[MAX_BUFFER_LEN];
	char pclCfgFormat[MAX_BUFFER_LEN];
	
	char *pclOldDate = NULL;
	char *pclNewDate = NULL;
	
	ilNrOfDateFields = GetNoOfElements(prgDynCfg->date_fields,CFG_DEL);
	ilNrOfDateFormats = GetNoOfElements(prgDynCfg->date_format,',');

	if (ilNrOfDateFields == ilNrOfDateFormats)
	{
		while(ilNrOfDateFields > 0)
		{
			memset(pclDateField,0x00,MAX_BUFFER_LEN);
			memset(pclCfgFormat,0x00,MAX_BUFFER_LEN);
			
			pclDateFieldPointer =
				CopyNextField(pclDateFieldPointer,CFG_DEL,pclDateField);
			
			pclDateFormatPointer =
				CopyNextField(pclDateFormatPointer,',',pclCfgFormat);
			
			ilFieldToCheck = 0;
			ilNrOfFields = igFieldCount;
			pclFieldPointer = prgDynCfg->fields;
			while(ilNrOfFields > 0)
			{
				memset(pclField,0x00,MAX_BUFFER_LEN-1);
				pclFieldPointer = CopyNextField(pclFieldPointer,CFG_DEL,pclField);	
				if (strcmp(pclDateField,pclField) == 0)
				{
					ilNrOfFields = 0;
					if ((pclDatePointer = GetDataField(pcpBuffer,
								(UINT)ilFieldToCheck,ucgData_del)) != NULL)
					{
						if (strlen((char *)pclDatePointer) > 0)
						{
							MALLOC(pclOldDate,char*,strlen((char *)pclDatePointer));
							memset(pclOldDate,0x00,strlen((char *)pclDatePointer));
							strcpy(pclOldDate,(char *)pclDatePointer);
							
							if ((ilRC = FormatDate((char *)pclDatePointer,pclCfgFormat,&pclNewDate)) 
												== RC_SUCCESS)
							{
								if ((ilRC = SearchStringAndReplace(pcpBuffer,pclOldDate,
														pclNewDate)) != RC_SUCCESS)
								{
									dbg(DEBUG,"CheckDates: Search and replace failed.");
								}
							}
							FREE(pclOldDate);
							FREE(pclNewDate);
						}	
					}
					else
					{
						dbg(DEBUG,"CheckDates: No date found in ASCII-string.");
					}
				}
				ilFieldToCheck++;
				ilNrOfFields--;
			}
			ilNrOfDateFields--;
		}
	}
	else
	{
		dbg(DEBUG,"CheckDates: Nr. of fields != Nr. of formats.");
	}
	
}

/* ******************************************************************** */
/* The FormatDate() routine                                         */
/* ******************************************************************** */
static int FormatDate(char *pcpOldDate,char *pcpFormat,char **pcpNewDate)
{
	int ilRC = RC_SUCCESS;
	int ilCfgChar = 0;
	int ilRightChar = 0;
	int ilDateChar = 0;
	
	char pclValidLetters[] = "YMDHIS";
	char *pclRightFormat = NULL;
	char *pclTmpDate = NULL;
	
	MALLOC(pclRightFormat,char*,strlen(pcpFormat));
	memset(pclRightFormat,0x00,strlen(pcpFormat));
	MALLOC(*pcpNewDate,char*,strlen(pcpFormat));

	/* adding '0' until the length of the date is 14byte */ 
	if (strlen(pcpOldDate) < 14)
	{
		while(strlen(pcpOldDate) < 14)
		{
			pcpOldDate[strlen(pcpOldDate)] = '0';
			pcpOldDate[strlen(pcpOldDate)+1] = 0x00;
		}
	}
	
	/* removing all unallowed letters from pcpFormat-string */
	while(ilCfgChar < (int)strlen(pcpFormat))
	{
		if (strchr(pclValidLetters,pcpFormat[ilCfgChar]) != NULL)
		{
			pclRightFormat[ilRightChar] = pcpFormat[ilCfgChar];
			ilRightChar++;
		}
		ilCfgChar++;
	}

	/* now formatting CEDA-time format from pcpOldDate to right format */	
	if ((pclTmpDate =
			GetPartOfTimeStamp(pcpOldDate,pclRightFormat)) != NULL)
	{
		/* now changing the layout like it is in the cfg-file */	
		ilCfgChar = 0;
		ilRightChar = 0;
		ilDateChar = 0;

		while(ilCfgChar < (int)strlen(pcpFormat))
		{
			if (strchr(pclValidLetters,pcpFormat[ilCfgChar]) != NULL)
			{
				(*pcpNewDate)[ilDateChar] = pclTmpDate[ilRightChar];
				ilRightChar++;
				ilDateChar++;
			}
			else
			{
				(*pcpNewDate)[ilDateChar] = pcpFormat[ilCfgChar];
				ilDateChar++;
			}
			ilCfgChar++;
		}
		(*pcpNewDate)[strlen(pcpFormat)] = 0x00;
	}
	else
	{
		ilRC = RC_FAIL;
	}
	FREE(pclRightFormat);

	return ilRC;	
}

/* ******************************************************************** */
/* The FormatImageFields(char *pcpBuffer) routine                       */
/* ******************************************************************** */
static void FormatImageFields(char *pcpBuffer)
{
	int ilRC;
	int ilCnt = 0;
	int ilNrOfImgFields = 0;

	char *pclImgField = prgDynCfg->img_fields;
	char *pclImgExt = prgDynCfg->img_ext;

	char *pclDataPointer = NULL;
	char *pclFieldDataBuffer = NULL;
	char pclNewDataBuffer[MAX_BUFFER_LEN];
	char pclFieldName[MAX_BUFFER_LEN];
	char pclExt[MAX_BUFFER_LEN];

	if ((ilNrOfImgFields = GetNoOfElements(prgDynCfg->img_fields,CFG_DEL)) > 0)
	{
		for (ilCnt = 0; ilCnt < ilNrOfImgFields; ilCnt++)
		{
			memset(pclFieldName,0x00,MAX_BUFFER_LEN);
			pclImgField = CopyNextField(pclImgField,CFG_DEL,pclFieldName);
			
			GetFieldData(prgDynCfg->fields,pclFieldName,CFG_DEL,pcpBuffer,ucgData_del,&pclFieldDataBuffer,&pclDataPointer);
			if (pclFieldDataBuffer == NULL)
			{
				dbg(DEBUG,"FormatImageFields: no data for formatting found.");
			}
			else
			{
				memset(pclExt,0x00,MAX_BUFFER_LEN);
				memset(pclNewDataBuffer,0x00,MAX_BUFFER_LEN);

				TagHtmlLine(IMAGE,pclNewDataBuffer,OPEN_TAG);
				strcat(pclNewDataBuffer,"src=\"");
				strcat(pclNewDataBuffer,prgDynCfg->img_path);
				strcat(pclNewDataBuffer,pclFieldDataBuffer);
				pclImgExt = CopyNextField(pclImgExt,CFG_DEL,pclExt);
				strcat(pclNewDataBuffer,pclExt);
				strcat(pclNewDataBuffer,"\"");
				TagHtmlLine(IMAGE,pclNewDataBuffer,CLOSE_TAG);

				if ((ilRC = SearchStringAndReplace(pcpBuffer,pclFieldDataBuffer,
										pclNewDataBuffer)) != RC_SUCCESS)
				{
					dbg(DEBUG,"FormatImageFields: Search and replace failed.");
				}
			}
		}
	}
}

/* ******************************************************************** */
/* The GetFieldData(char *pcpField,char *pcpData,char *pcpReturnBuffer) */
/*									*/
/*									*/
/* ******************************************************************** */
static void GetFieldData(char *pcpFieldList, char *pcpField,char pcpFieldDel,
												char *pcpData,char pcpDataDel,char **pcpReturnBuffer
												,char **pcpDataPointer)
{
	int ilCnt = 1;
	int ilNrOfData = 0;
	int ilNrOfFields = 0;
	char *pclFieldPointer = pcpFieldList;
	char *pclDataPointer	= pcpData;

	char pclFieldName[MAX_BUFFER_LEN];
	char pclData[MAX_BUFFER_LEN];
	char pclReturnBuffer[MAX_BUFFER_LEN];
	
	*pcpReturnBuffer = NULL;
	memset(pclReturnBuffer,0x00,MAX_BUFFER_LEN);
	ilNrOfData = GetNoOfElements(pcpData,pcpDataDel);
	
	ilNrOfFields = GetNoOfElements(pcpFieldList,pcpFieldDel);
			
	/* NECESSARY, because fileif adds a delimiter to the end of each data-line*/
	if (pcpFieldList == prgDynCfg->fields)
	{ilNrOfFields++;}
	
	if (ilNrOfData != ilNrOfFields)
	{
		dbg(DEBUG,"GetFieldData: Nr. of fields (%d) != Nr. of data (%d)."
							,ilNrOfData,ilNrOfFields);
	}
	else
	{
		if (strstr(pcpFieldList,pcpField) == NULL)
		{
			dbg(DEBUG,"GetFieldData: field not in field-list.");
		}
		else
		{
			for (ilCnt = 1; ilCnt <= ilNrOfFields; ilCnt++)
			{
				memset(pclFieldName,0x00,MAX_BUFFER_LEN);
				memset(pclData,0x00,MAX_BUFFER_LEN);
				pclFieldPointer = CopyNextField(pclFieldPointer,pcpFieldDel,pclFieldName);
				*pcpDataPointer = pclDataPointer;
				pclDataPointer  = CopyNextField(pclDataPointer,pcpDataDel,pclData);
				if (strcmp(pclFieldName,pcpField) == 0)
				{
					strcpy(pclReturnBuffer,pclData);
					*pcpReturnBuffer = pclReturnBuffer;
					ilCnt = ilNrOfFields+1;
				}
			}
		}
	}
}	

/* ******************************************************************** */
/* The ReadTables(char *pcpReReadTable)                			*/
/* ******************************************************************** */
static int ReadTables(char *pcpReReadTable)
{
	/***************************************************************/
	/* Elements of LISTHEADER:  LPLISTHEADER *First;*Last;*Current */
	/*                          int size;count                     */
	/* Elements of LISTELEMENT: lelement *Next;*Prev  void *Data   */
	/***************************************************************/
	int ilRC = RC_SUCCESS;
	int ilCnt = 0;
	int ilNrOfIntTables = 0;
	int ilNrOfIntFields = 0;
	int ilNrOfIntField_Len = 0;
	TAB_DEF *prlDefEle;
	char *pclTablePointer = NULL;
	char *pclFieldPointer = NULL;
	char *pclLenPointer = NULL;

	char pclTable[MAX_BUFFER_LEN];
	char pclFields[MAX_BUFFER_LEN];
	char pclField_len[MAX_BUFFER_LEN];
	
	if (pcpReReadTable == NULL)
	{
		if ((ilRC=GetCfgEntry("INTERNAL_TABLES","INT_TABLES",CFG_STRING,
													&prgStaCfg->int_tables)) == RC_SUCCESS)
		{pclTablePointer = prgStaCfg->int_tables;}
		if ((ilRC=GetCfgEntry("INTERNAL_TABLES","INT_FIELDS",CFG_STRING,
													&prgStaCfg->int_fields)) == RC_SUCCESS)
		{pclFieldPointer = prgStaCfg->int_fields;}
		if ((ilRC=GetCfgEntry("INTERNAL_TABLES","FIELD_LEN",CFG_STRING,
													&prgStaCfg->field_len)) == RC_SUCCESS)
		{pclLenPointer = prgStaCfg->field_len;}
	}
	else
	{	
		pclTablePointer = prgStaCfg->int_tables;
		pclFieldPointer = prgStaCfg->int_fields;	
		pclLenPointer = prgStaCfg->field_len;
	}

	if (pclTablePointer!=NULL && pclFieldPointer!=NULL && pclLenPointer!=NULL)
	{
		ilNrOfIntTables = GetNoOfElements(pclTablePointer,CFG_DEL);
		ilNrOfIntFields = GetNoOfElements(pclFieldPointer,CFG_DEL);
		ilNrOfIntField_Len = GetNoOfElements(pclLenPointer,CFG_DEL);

		if ((ilNrOfIntTables == ilNrOfIntFields) &&
				(ilNrOfIntTables == ilNrOfIntField_Len))
		{
			for (ilCnt=0;ilCnt<ilNrOfIntTables;ilCnt++)
			{ 
				memset(pclTable,0x00,MAX_BUFFER_LEN);
				memset(pclFields,0x00,MAX_BUFFER_LEN);
				memset(pclField_len,0x00,MAX_BUFFER_LEN);
				pclTablePointer = CopyNextField(pclTablePointer,CFG_DEL,pclTable);
				pclFieldPointer = CopyNextField(pclFieldPointer,CFG_DEL,pclFields);
				pclLenPointer = CopyNextField(pclLenPointer,CFG_DEL,pclField_len);
	
				if (pcpReReadTable == NULL)
				{
					ilRC = ReadDataFromTable(pclTable,pclFields,pclField_len);
				}
				else
				{
					if (strcmp(pclTable,pcpReReadTable) == 0)
					{
						ilRC = ReadDataFromTable(pcpReReadTable,pclFields,pclField_len);
						ilCnt = ilNrOfIntTables;
					}
				}
			}
		}
		else
		{
			prgTableList->First = NULL;
			ilRC = RC_FAIL;
			dbg(DEBUG,"ReadTables: Nr. of elements for INT_TABLES false."
								" Can't load tables.");
		}
	}
	else
	{
		ilRC = RC_FAIL;
		dbg(DEBUG,"ReadTables: Can't load tables. Please check cfg-file.");
	}
	return ilRC;
}

/* ******************************************************************** */
/* The CreateDefEle() routine															*/
/* ******************************************************************** */
static int CreateDefEle(TAB_DEF *prpDefEle,char *pcpTable,char *pcpFields,
												char *pcpLen)
{
	int ilRC = RC_SUCCESS;
	int ilDefEleSize = 0;
	int ilFields = 0;
	int ilCnt = 0;
	char *pclLenPointer;
	char pclLen[MAX_BUFFER_LEN];

	MALLOC(prpDefEle->tana,char*,strlen(pcpTable));
	MALLOC(prpDefEle->fields,char*,strlen(pcpFields));
	MALLOC(prpDefEle->len,char*,strlen(pcpLen));

	/* filling the TAB_DEF-structure of prpDefEle with informations */
	strcpy(prpDefEle->tana,pcpTable);
	strcpy(prpDefEle->fields,pcpFields);
	strcpy(prpDefEle->len,pcpLen);
	prpDefEle->data = NULL;
	prpDefEle->bytes = 0;
	
	/* calculating the size of bytes needed for the table-data */
	pclLenPointer = prpDefEle->len;
	ilFields = GetNoOfElements(pclLenPointer,INT_TAB_DEL);
	for (ilCnt = 0; ilCnt < ilFields;ilCnt++)
	{
		memset(pclLen,0x00,MAX_BUFFER_LEN);
		pclLenPointer = CopyNextField(pclLenPointer,INT_TAB_DEL,pclLen);
		prpDefEle->bytes = prpDefEle->bytes + atoi(pclLen);		
	}
	if (strstr(prgStaCfg->int_tables,prpDefEle->tana) == NULL)
	{
		ilRC = RC_FAIL;
	}
	return ilRC;
}

/* ******************************************************************** */
/* The ReadDataFromTable() 																									*/
/* ******************************************************************** */
static int ReadDataFromTable(char *pcpTable,char *pcpFields,char *pcpLen)
{
	int ilRC = RC_SUCCESS;
	short slSqlFunc;
	int ilFields = 0;
	int ilLen = 0;
	int ilCnt = 0;
	int ilCnt2 = 0;
	short slCursor = 0;
	char pclDataArea[MAX_BUFFER_LEN];
	char pclSqlBuf[MAX_BUFFER_LEN];
	char pclTmpBuffer[MAX_BUFFER_LEN];
	char pclLen[MAX_BUFFER_LEN];
	char pclOraErrorMsg[MAX_BUFFER_LEN];
	char *pclLenPointer = NULL;
	TAB_DEF rlNewDefEle;

	pclLenPointer = pcpLen;
	ilFields = GetNoOfElements(pclLenPointer,INT_TAB_DEL);
	memset(pclSqlBuf,0x00,MAX_BUFFER_LEN);
	memset(pclDataArea,0x00,MAX_BUFFER_LEN);

	/* TABEND changes to use default values from sgs.tab */
	 pcpTable[3] = 0x00; 
	 strcat(pcpTable,pcgDefTabEnd); 
	 sprintf(pclSqlBuf,"SELECT %s FROM %s WHERE HOPO = '%s'",pcpFields,pcpTable,pcgDefHomeAp);
	
	slSqlFunc = START;
	while ((ilRC = sql_if(slSqlFunc,&slCursor,pclSqlBuf,pclDataArea))
					== RC_SUCCESS) 
	{
		slSqlFunc = NEXT;
		pclLenPointer = pcpLen;
		memset(&rlNewDefEle,0x00,sizeof(TAB_DEF));

		if (CreateDefEle(&rlNewDefEle,pcpTable,pcpFields,pcpLen) == RC_SUCCESS)
		{
			MALLOC(rlNewDefEle.data,char*,(rlNewDefEle.bytes+ilFields));
			rlNewDefEle.data[0] = 0x00;
			for (ilCnt = 0;ilCnt<ilFields;ilCnt++)
			{
				memset(pclTmpBuffer,0x00,MAX_BUFFER_LEN);
				memset(pclLen,0x00,MAX_BUFFER_LEN);
				pclLenPointer = CopyNextField(pclLenPointer,INT_TAB_DEL,pclLen);
				ilLen = atoi(pclLen);
				get_fld(pclDataArea,(short)ilCnt,STR,ilLen,pclTmpBuffer);
				TrimRight(pclTmpBuffer);
				strcat(rlNewDefEle.data,pclTmpBuffer);
				strcat(rlNewDefEle.data,",");
			}
			rlNewDefEle.data[strlen(rlNewDefEle.data)-1] = 0x00;
			/* appending table-data to table-list */
			if (ListAppend(prgTableList,&rlNewDefEle) == NULL)
			{
				dbg(DEBUG,"ReadDataFromTable: ListAppend failed!");
				terminate(RC_SHUTDOWN);
			}
		}
		ilCnt2++;
	}
	close_my_cursor(&slCursor);

	if (ilRC == NOTFOUND)
	{
		ilRC = RC_SUCCESS;
	}
	else
	{
		ilRC = RC_FAIL;
		DeleteInternalTable(rlNewDefEle.tana);
		get_ora_err(ilRC,pclOraErrorMsg);	
		dbg(TRACE,"ReadDataFromTable: ORA-ERR <%s>",pclOraErrorMsg);
	}
	dbg(DEBUG,"ReadDataFromTable: <%s> <%d>-rows",pcpTable,ilCnt2);
	return ilRC;
}

/* ******************************************************************** */
/* The TrimRight() routine																						*/
/* ******************************************************************** */
static void TrimRight(char *pcpBuffer)
{
	while(pcpBuffer[strlen(pcpBuffer)-1] == ' ') 
	{pcpBuffer[strlen(pcpBuffer)-1] = 0x00;}
}

/* ******************************************************************** */
/* The DeleteAllInternalTables()         										*/
/* ******************************************************************** */
static void DeleteAllInternalTables()
{
	int ilCnt = 0;
	int ilNrOfIntTables = 0;
	char *pclInt_tablesPointer = prgStaCfg->int_tables;
	char pclInternalTable[MAX_BUFFER_LEN];

	ilNrOfIntTables = GetNoOfElements(pclInt_tablesPointer,CFG_DEL);
	for (ilCnt = 0; ilCnt < ilNrOfIntTables; ilCnt++)
	{
		memset(pclInternalTable,0x00,MAX_BUFFER_LEN);
		pclInt_tablesPointer =
			CopyNextField(pclInt_tablesPointer,CFG_DEL,pclInternalTable); 
		
		DeleteInternalTable(pclInternalTable);
	}
}
/* ******************************************************************** */
/* The DeleteInternalTable(char *pcpTable)         										*/
/* ******************************************************************** */
static void DeleteInternalTable(char *pcpTable)
{
	LPLISTELEMENT prlEle = NULL;
	TAB_DEF *prlTable = NULL;
	int ilCnt = 0;
	
	prlEle = ListFindLast(prgTableList);
	while(prlEle != NULL)
	{
		prlTable = (TAB_DEF *)prlEle->Data;
		if (strcmp(prlTable->tana,pcpTable) == 0)
		{
			FREE(prlTable->data);
			FREE(prlTable->len);
			FREE(prlTable->fields);
			FREE(prlTable->tana);
			/* deletes the current list-element from prgTableList */
			/* incl. Data-pointer & memory freeing                */
			prlEle = ListDelete(prgTableList);
			ilCnt++;
		}
		else
		{
			prlEle = ListFindPrevious(prgTableList);
		}
	}
	dbg(DEBUG,"DeleteInternalTable: <%s> rows<%d>",pcpTable,ilCnt);
}

/* ******************************************************************** */
/* The ResolvCfgFields() routine																						*/
/* ******************************************************************** */
static void ResolvCfgFields(char *pcpBuffer)
{
	int ilRC = 0;	
	int ilCnt = 0;	
	int ilFieldsToReplace = 0;	
	char *pclRepFieldsPointer = prgDynCfg->repl_fields;
	char *pclReplacementPointer = prgDynCfg->replacement;
	char *pclRepTablesPointer = prgDynCfg->repl_table;
	char *pclReferencePointer = prgDynCfg->reference;
	char *pclTmpBuffer = NULL;
	char *pclResult = NULL;
	char *pclDataPointer = NULL;
	char pclRepField[MAX_BUFFER_LEN];
	char pclReplacement[MAX_BUFFER_LEN];
	char pclRepTable[MAX_BUFFER_LEN];
	char pclReference[MAX_BUFFER_LEN];
	char pclOldData[MAX_BUFFER_LEN];

	char pclSqlBuf[MAX_BUFFER_LEN];
	char pclDataArea[MAX_BUFFER_LEN];
	short slSqlFunc = 0;
	short slCursor = 0;
	
	ilFieldsToReplace = GetNoOfElements(pclRepFieldsPointer,CFG_DEL);
	for (ilCnt=0;ilCnt<ilFieldsToReplace;ilCnt++)
	{
		pclResult = NULL;
		memset(pclRepField,0x00,MAX_BUFFER_LEN);
		memset(pclReplacement,0x00,MAX_BUFFER_LEN);
		memset(pclRepTable,0x00,MAX_BUFFER_LEN);
		memset(pclReference,0x00,MAX_BUFFER_LEN);
		memset(pclOldData,0x00,MAX_BUFFER_LEN);
			
		pclRepFieldsPointer=
			CopyNextField(pclRepFieldsPointer,CFG_DEL,pclRepField);  
		pclReplacementPointer=
			CopyNextField(pclReplacementPointer,CFG_DEL,pclReplacement);  
		pclRepTablesPointer=
			CopyNextField(pclRepTablesPointer,CFG_DEL,pclRepTable);  
		pclReferencePointer=
			CopyNextField(pclReferencePointer,CFG_DEL,pclReference); 

		GetFieldData(prgDynCfg->fields,pclRepField,CFG_DEL,pcpBuffer,
									ucgData_del,&pclTmpBuffer,&pclDataPointer);
		if (pclTmpBuffer == NULL) {return;}
		
		strcpy(pclOldData,pclTmpBuffer);

		/* now following the select on the database directly    */
		/* because fields are not available in systab           */
		/* SQL: select [pclReplacement] from [pclRepTable] where*/	
		/*      [pclReference] = [pclOldData] and HOPO = [pcgHopo];*/	

		if (bgResolvViaDB == TRUE)
		{
			sprintf(pclSqlBuf,"SELECT %s FROM %s WHERE %s = '%s' AND HOPO = '%s'"
					,pclReplacement,pclRepTable,pclReference,pclOldData,pcgDefHomeAp);

			slSqlFunc = START|REL_CURSOR;
			if ((ilRC = sql_if(slSqlFunc,&slCursor,pclSqlBuf,pclDataArea))
					== RC_SUCCESS) 
			{
				if (strlen(pclDataArea) > 0)
				{
					if ((ilRC=SearchStringAndReplace(pclDataPointer,pclOldData,pclDataArea))
								!= RC_SUCCESS)
					{
						dbg(DEBUG,"ResolvCfgFields: Search and replace failed.");
					}
				}
				else
				{
					dbg(DEBUG,"ResolvCfgFields: can't replace <%s>=<%s> with <%s>"
									,pclRepField,pclOldData,pclReplacement);
				}
			}
		}
		else
		{
		}

		#if 0 /* old resolving function */
		/* now following the select on the internal table list  */
		/* SQL: select [pclReplacement] from [pclRepTable] where*/	
		/*      [pclReference] = pclOldData;              */	
		if (strlen(pclOldData) > 0)
		{
			InternalSql
				(&pclResult,pclReplacement,pclRepTable,pclReference,pclOldData);
			if (pclResult != NULL)
			{
				/*if ((ilRC=SearchStringAndReplace(pcpBuffer,pclOldData,pclResult))*/
				if ((ilRC=SearchStringAndReplace(pclDataPointer,pclOldData,pclResult))
							!= RC_SUCCESS)
				{
					dbg(DEBUG,"ResolvCfgFields: Search and replace failed.");
				}
			}
			else
			{
				dbg(DEBUG,"ResolvCfgFields: can't replace <%s>=<%s> with <%s>"
								,pclRepField,pclOldData,pclReplacement);
			}
		}
		#endif
	}
}

/* ******************************************************************** */
/* The InternalSql() routine																						*/
/* ******************************************************************** */
static void InternalSql(char **pcpResult,char *pcpReplacement,char *pcpTable,
												char *pcpReference, char *pcpData)
{
	short 		slItemNo = 0;
	LPLISTELEMENT 	prlEle = NULL;
	TAB_DEF 	*prlTable = NULL;
	char 		*pclBuffer = NULL;
	char 		*pclDataPointer = NULL;
	
	prlEle = ListFindLast(prgTableList);
	while(prlEle != NULL && pcpData != NULL)
	{
		*pcpResult = NULL;
		prlTable = (TAB_DEF *)prlEle->Data;
		if (strcmp(prlTable->tana,pcpTable) == 0)
		{
			pclBuffer = NULL;
			
			GetFieldData(prlTable->fields,pcpReference,INT_TAB_DEL,
										prlTable->data,INT_TAB_DEL,&pclBuffer,&pclDataPointer);
			if(strcmp(pclBuffer,pcpData) == 0)
			{
				GetFieldData(prlTable->fields,pcpReplacement,INT_TAB_DEL,
											prlTable->data,INT_TAB_DEL,&pclBuffer,&pclDataPointer);
				*pcpResult = pclBuffer;
				/*dbg(DEBUG,"InternalSql: found <%s> as replacement for <%s>",*pcpResult
					,pcpData);*/
				prlEle = NULL;
			}
		}
		if (*pcpResult == NULL)
		{
			prlEle = ListFindPrevious(prgTableList);
		}
		else
		{
			prlEle = NULL;	
		}
	}
}

/* ******************************************************************** */
/* The TransmitFile() routine						*/
/*									*/
/* CHANGE HISTORY							*/
/*		11/01/99						*/
/*			FTPHDL CALL IMPLEMENTED				*/
/*			 if ftphdl is active, a structure will be	*/
/*			 filled with the data for the dynamic 		*/
/*			 configuration.					*/
/*			 if not, a ftp-system call will be invoked	*/
/*									*/
/* ******************************************************************** */
static int TransmitFileViaFtp(char *pcpRHost,char *pcpFtpUser,char *pcpFtpPass,FILE *pfpFile)
{
	int 	ilRC = RC_SUCCESS;
	char 	*pclPtr = NULL;
	char 	pclTmpBuffer[MAX_BUFFER_LEN]; 


	/******* Changes by MOS 01 Nov 1999 for FTPHDL implementation *****/
	/****************FTP System Call**************** */
	if ((igModID_Ftphdl = tool_get_q_id("ftphdl")) == RC_NOT_FOUND ||
			(igModID_Ftphdl == RC_FAIL) || (strcmp(prgDynCfg->ftpmode,"SYSTEM")== 0))	{
	/********EO changes **************/
		
		if ((ilRC = CreateFtpCtrlFile()) == RC_SUCCESS)
		{
			/* encode passwd */
			pclPtr = pcpFtpPass;
			CDecode(&pclPtr);
			strcpy(pcpFtpPass,pclPtr);


			memset(pclTmpBuffer,0x00,MAX_BUFFER_LEN);
			sprintf(pclTmpBuffer, "ftp -n %s < %s 2>&1 > %s%s",pcpRHost
				,FTP_CTRL_FILE,pcgDebugPath,FTP_LOG_FILE);
			dbg(DEBUG,"TransmitFileViaFtp: system call   : <%s>",pclTmpBuffer);
			ilRC = system(pclTmpBuffer);
			dbg(DEBUG,"TransmitFileViaFtp: system returns: <%d>",ilRC);
			/* remove control file */
			if ((ilRC = remove((const char*)FTP_CTRL_FILE)) != 0)
			{
				dbg(TRACE,"TransmitFileViaFtp: can't remove ctrl-file <%s> (%d)"
					,FTP_CTRL_FILE ,ilRC);
			}/*if ((ilRC = remove */
		}/* if ((ilRC = CreateFtpCtrlFile */
	} /* if ((ilRC = tool_get_q_id(ftphdl)  == 0)*/
	else
	{
	/******* Changes by MOS 01 Nov 1999 for FTPHDL implementation *****/
		/*****************************************************************************************/
		/* Filling of the FTPConfig-structure for the dynamic configuration of the ftphdl-process*/
		/*****************************************************************************************/
		memset(&prgFtp,0x00,sizeof(FTPConfig));

		prgFtp.iFtpRC 			= FTP_SUCCESS; /* Returncode of FTP-transmission */
		strcpy(prgFtp.pcCmd,"FTP");
		strcpy(prgFtp.pcHostName,pcpRHost);
		strcpy(prgFtp.pcUser,prgDynCfg->ftp_user);
		strcpy(prgFtp.pcPasswd,prgDynCfg->ftp_pass);
		prgFtp.cTransferType 		= CEDA_BINARY;
		prgFtp.iTimeout 		= atoi(prgDynCfg->ftptimeout); /* for tcp_waittimeout in seconds */
		prgFtp.lGetFileTimer 		= atol(prgDynCfg->ftpfiletimer); /* timeout (seconds) for receiving data */
		prgFtp.lReceiveTimer 		= atol(prgDynCfg->ftpfiletimer); /* timeout (seconds) for receiving data */
		prgFtp.iDebugLevel 		= TRACE; /* Debugging level of FTPHDL during operation */
		if (strcmp(prgDynCfg->ftp_client_os,"WIN") == 0)
		{
			prgFtp.iClientOS 	= OS_WIN; /* Operating system of client */
		}
		if (strcmp(prgDynCfg->ftp_client_os,"UNIX") == 0)
		{
			prgFtp.iClientOS 	= OS_UNIX; /* Operating system of client */
		}
		prgFtp.iServerOS 		= OS_UNIX; /* Operating system of server */
		prgFtp.iRetryCounter 		= 0; /* number of retries */
		prgFtp.iDeleteRemoteSourceFile 	= 0; /* yes=1 no=0 */
		prgFtp.iDeleteLocalSourceFile 	= 0; /* yes=1 no=0 */
		prgFtp.iSectionType 		= iSEND; /* iSEND or iRECEIVE */
		prgFtp.iInvalidOffset 		= 0; /* time (min.) the section will be set invalid, after unsuccessfull retry */
		prgFtp.iTryAgainOffset 		= 0; /* time between retries */
		strcpy(prgFtp.pcHomeAirport,pcgDefHomeAp); /* HomeAirport */
		strcpy(prgFtp.pcTableExtension,pcgDefTabEnd); /* Table extension */
		strcpy(prgFtp.pcLocalFilePath,prgDynCfg->html_path);
		if (strlen(prgDynCfg->pcFtpRenameDestFile)>1) {
			strcpy(prgFtp.pcRenameRemoteFile,prgDynCfg->pcFtpRenameDestFile);
		}
		/***exclude slash from filename****/
		strcpy(prgFtp.pcLocalFileName,&(prgDynCfg->ftp_file[1]));
		strcpy(prgFtp.pcRemoteFilePath,prgDynCfg->ftp_path);
		strcpy(prgFtp.pcRemoteFileName,&(prgDynCfg->html_dest_file[1]));
		prgFtp.iSendAnswer 		= 1; /* yes=1 no=0 */ 
		prgFtp.cStructureCode 		= CEDA_FILE;
		prgFtp.cTransferMode 		= CEDA_STREAM;
		prgFtp.iStoreType 		= CEDA_CREATE;
		prgFtp.data[0] 			= 0x00;

		if ((ilRC = SendEvent("FTP"," "," ","DYN",(char*)&prgFtp,
				sizeof(FTPConfig),igModID_Ftphdl, PRIORITY_3,"","",""))	!= RC_SUCCESS)
		{
			dbg(TRACE,"SFVF: SendEvent() to <%d> returns <%d>!",igModID_Ftphdl,ilRC);
		}

	/****************EO changes*****************/

	} /* if ((ilRC = tool_get_q_id(ftphdl)  == 8750)*/

	return ilRC;
}


/* **********************************************************************/
/* The SendEvent routine                                         	*/
/* prepares an internal CEDA-event                                  	*/
/*									*/
/* CHANGE HISTORY							*/
/*		implmented 11/01/99 for the ftphdl call			*/
/*									*/
/* **********************************************************************/
static int SendEvent(char *pcpCmd,char *pcpSelection,char *pcpFields, char *pcpData,
		char *pcpAddStruct, int ipAddLen,int ipModID,int ipPriority,
		char *pcpTable,char *pcpType,char *pcpFile)
{
	int     ilRC        	= RC_FAIL;
	int     ilLen        	= 0;
  	EVENT   *prlOutEvent    = NULL;
  	BC_HEAD *prlOutBCHead   = NULL;
  	CMDBLK  *prlOutCmdblk   = NULL;

	/* size-calculation for prlOutEvent */
	ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) +
					strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + ipAddLen + 10;

	/* memory for prlOutEvent */
	if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
	{
		dbg(TRACE,"SendEvent: cannot malloc <%d>-bytes for outgoing event!",ilLen);
		prlOutEvent = NULL;
	}
	else
	{
		/* clear whole outgoing event */
		memset((void*)prlOutEvent, 0x00, ilLen);

		/* set event structure... */
		prlOutEvent->type		= SYS_EVENT;
		prlOutEvent->command   		= EVENT_DATA;
		prlOutEvent->originator		= (short)mod_id;
		prlOutEvent->retry_count	= 0;
		prlOutEvent->data_offset	= sizeof(EVENT);
		prlOutEvent->data_length	= ilLen - sizeof(EVENT); 

		/* BC_HEAD-Structure... */
		prlOutBCHead 			= (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
		prlOutBCHead->rc 		= (short)RC_SUCCESS;
		strncpy(prlOutBCHead->dest_name,pcgProcessName,10);
		prlOutBCHead->dest_name[10] = 0x00;
		strncpy(prlOutBCHead->recv_name, "EXCO",10);
		prlOutBCHead->recv_name[10] = 0x00;
				
		/* Cmdblk-Structure... */
		prlOutCmdblk 			= (CMDBLK*)((char*)prlOutBCHead->data);
		strcpy(prlOutCmdblk->command,pcpCmd);
		strcpy(prlOutCmdblk->obj_name,pcpTable);
		strcat(prlOutCmdblk->obj_name,pcgDefTabEnd);
		
		/* setting tw_start entries */
		sprintf(prlOutCmdblk->tw_start,"%s,%s",pcpType,pcpFile);
		
		/* setting tw_end entries */
		sprintf(prlOutCmdblk->tw_end,"%s,%s,%s,TRUE",pcgDefHomeAp,pcgDefTabEnd,pcgProcessName);
		
		/* setting selection inside event */
		strcpy(prlOutCmdblk->data,pcpSelection);

		/* setting field-list inside event */
		strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1,pcpFields);

		/* setting data-list inside event */
		strcpy((prlOutCmdblk->data +
					(strlen(pcpSelection)+1) +
					(strlen(pcpFields)+1)),pcpData);

		if (pcpAddStruct != NULL)
		{
			memcpy((prlOutCmdblk->data +
						(strlen(pcpSelection)+1) +
						(strlen(pcpFields)+1)) + 
						(strlen(pcpData)+1),pcpAddStruct,ipAddLen);
		}

		/*DebugPrintEvent(DEBUG,prlOutEvent);*/ 
		/*snapit((char*)prlOutEvent,ilLen,outp);*/

		dbg(DEBUG,"SendEvent: <%d> --> <%d>",mod_id,ipModID);

		if ((ilRC = que(QUE_PUT,ipModID,mod_id,ipPriority,ilLen,(char*)prlOutEvent))
			!= RC_SUCCESS)
		{
			dbg(TRACE,"SendEvent: QUE_PUT to <%d> returns: <%d>",ipModID,ilRC);
		}
		/* free memory */
	  free((void*)prlOutEvent); 
	}
	return ilRC;
}

/* ******************************************************************** */
/* The CreateFtpCtrlFile() routine																						*/
/* ******************************************************************** */
static int CreateFtpCtrlFile()
{
	int 	ilRC = RC_SUCCESS;
	FILE 	*pfl = NULL;
        char    *pcldummy = NULL;
	
	if ((pfl = fopen(FTP_CTRL_FILE, "w")) == NULL)
	{
		dbg(DEBUG,"CreateFtpCtrlFile: can't open ctrl file!");
		ilRC = RC_FAIL;
	}
	else
	{
                /* encode passwd */
                pcldummy = prgDynCfg->ftp_pass;
                CDecode(&pcldummy);


		/* write user */
		fprintf(pfl, "user %s %s\n",prgDynCfg->ftp_user,pcldummy);
		/* change directory */
		fprintf(pfl, "cd %s\n",prgDynCfg->ftp_path);
		/* setting transmission mode to bin */
		fprintf(pfl,"bin\n");
		/* write it */
		fprintf(pfl, "put %s%s %s%s\n"
			,prgDynCfg->html_path,prgDynCfg->html_dest_file,prgDynCfg->ftp_path,prgDynCfg->ftp_file); 
		/* close & quit */
		fprintf(pfl,"close\n");
		fprintf(pfl,"quit\n");
		/* close file */
		fclose(pfl);
	}
	return ilRC;
}

/* ******************************************************************** */
/* The CheckStaCfg() routine																						*/
/* ******************************************************************** */
static void CheckStaCfg(void)
{
	bgIgnoreAll 	= FALSE;
	bgResolv 	= TRUE;

	if (strlen(prgStaCfg->sections) <= 0)
	{
		dbg(TRACE,"CheckCfg: no sections found. Ignore MHC-cmd!");
		bgIgnoreAll = TRUE;
	}

	if (strlen(prgStaCfg->int_tables) <= 0)
	{
		dbg(TRACE,"CheckCfg: no int_tables found. Ignore REPL_FIELDS!");
		bgResolv = FALSE;
	}
	else
	{
		if ((GetNoOfElements(prgStaCfg->int_tables,CFG_DEL) !=
				GetNoOfElements(prgStaCfg->int_fields,CFG_DEL)) ||
				(GetNoOfElements(prgStaCfg->int_tables,CFG_DEL) !=
					GetNoOfElements(prgStaCfg->field_len,CFG_DEL)))
		{
			dbg(TRACE,"CheckCfg: wrong INT_TABLES def. Ignore REPL_FIELDS!");
			bgResolv = FALSE;
		}
		else
		{
			if (GetNoOfElements(prgStaCfg->int_fields,INT_TAB_DEL) !=
					GetNoOfElements(prgStaCfg->field_len,INT_TAB_DEL))
			{
				dbg(TRACE,"CheckCfg: wrong INT_TABLES def. Ignore REPL_FIELDS!");
				bgResolv = FALSE;
			}
		}
	}
}

/* ******************************************************************** */
/* The CheckDynCfg() routine																						*/
/* ******************************************************************** */
static void CheckDynCfg(void)
{
	bgData_del 	= TRUE;
	bgFtp 		= TRUE;
	bgAsciiFile 	= TRUE;
	bgHtmlTable 	= TRUE;
	bgHtmlDestFile 	= TRUE;
	bgStore 	= TRUE;
	bgHead 		= TRUE;
	bgCreateImgFields = TRUE;
	bgFormatDate 	= TRUE;
	bgTableHeader 	= TRUE;
	bgTableData 	= TRUE;
	igFieldCount 	= GetNoOfElements(prgDynCfg->fields,CFG_DEL);
	
	if (strlen(prgDynCfg->data_del) <= 0 || strlen(prgDynCfg->data_del) > 1)
	{	
		dbg(TRACE,"CheckCfg: DATA_DELIMITER wrong. can't create html-file(s)!");
		bgData_del = FALSE;
	}

	if (strlen(prgDynCfg->rhost) <= 0 || strlen(prgDynCfg->ftp_path) <= 0 ||
			strlen(prgDynCfg->ftp_file) <= 0 ||strlen(prgDynCfg->ftp_user) <= 0 ||
			strlen(prgDynCfg->ftp_pass) <= 0 )
	{
		dbg(DEBUG,"CheckCfg: FTP-cfg. can't transmit html-file(s)!");
		bgFtp = FALSE;
	}

	if (strlen(prgDynCfg->ascii_path) <= 0 || strlen(prgDynCfg->ascii_file) <= 0)
	{
		dbg(TRACE,"CheckCfg: no ASCII_PATH or ASCII_FILE found!");
		bgAsciiFile = FALSE;
	}
	
	if (strlen(prgDynCfg->html_path) <= 0 || strlen(prgDynCfg->html_table)
			<= 0)
	{	
		dbg(TRACE,"CheckCfg: no HTML_PATH or HTML_TABLE found!");
		bgHtmlTable = FALSE;
	}
	if (strlen(prgDynCfg->html_path) <= 0 || strlen(prgDynCfg->html_dest_file)
			<= 0)
	{	
		dbg(TRACE,"CheckCfg: no HTML_PATH or HTML_DEST_FILE found!");
		bgHtmlDestFile = FALSE;
	}

	if (igFieldCount <= 0)
	{	
		dbg(TRACE,"CheckCfg: no FIELDS for converting found. Ignore MHC-cmd!");
		bgIgnoreAll = TRUE;
	}

	if (GetNoOfElements(prgDynCfg->field_head,CFG_DEL) != igFieldCount)
	{	
		dbg(TRACE,"CheckCfg: Nr. of FIELD_HEAD entries wrong. Ignore FIELD_HEAD text!");
		bgHead = FALSE;
	}
	
	if (GetNoOfElements(prgDynCfg->img_fields,CFG_DEL) !=
			GetNoOfElements(prgDynCfg->img_ext,CFG_DEL))
	{	
		dbg(TRACE,"CheckCfg: Nr. of IMG_FIELDS != Nr. of IMG_EXT. Ignore IMG_FIELDS!");
		bgCreateImgFields = FALSE;
	}
	
	if (GetNoOfElements(prgDynCfg->repl_fields,CFG_DEL) !=
			GetNoOfElements(prgDynCfg->replacement,CFG_DEL) ||
	    GetNoOfElements(prgDynCfg->repl_fields,CFG_DEL) !=
			GetNoOfElements(prgDynCfg->repl_table,CFG_DEL)  ||
	    GetNoOfElements(prgDynCfg->repl_fields,CFG_DEL) !=
			GetNoOfElements(prgDynCfg->reference,CFG_DEL))
	{	
		dbg(TRACE,"CheckCfg: Nr. of REPLACE-entries different. Ignore REPL_FIELDS!");
		bgResolv = FALSE;
	}

	if (GetNoOfElements(prgDynCfg->date_fields,CFG_DEL) !=
			GetNoOfElements(prgDynCfg->date_format,INT_TAB_DEL))
	{
		dbg(TRACE,"CheckCfg: Nr. of DATE-entries different. Ignore DATE_FIELDS!");
		bgFormatDate = FALSE;
	}
	
	if (GetNoOfElements(prgDynCfg->th_align,CFG_DEL) != igFieldCount ||
	    GetNoOfElements(prgDynCfg->th_valign,CFG_DEL) != igFieldCount ||
	    GetNoOfElements(prgDynCfg->th_width,CFG_DEL) != igFieldCount ||
	    GetNoOfElements(prgDynCfg->th_height,CFG_DEL) != igFieldCount ||
	    GetNoOfElements(prgDynCfg->th_bgc,CFG_DEL) != igFieldCount ||
	    GetNoOfElements(prgDynCfg->th_nowrap,CFG_DEL) != igFieldCount ||
	    GetNoOfElements(prgDynCfg->th_fontf,CFG_DEL) != igFieldCount ||
	    GetNoOfElements(prgDynCfg->th_fontc,CFG_DEL) != igFieldCount ||
	    GetNoOfElements(prgDynCfg->th_fonts,CFG_DEL) != igFieldCount)
	{	
		dbg(TRACE,"CheckCfg: Nr. of TH-entries different. can't create table-header!");
		bgTableHeader = FALSE;
	}
	
	if (GetNoOfElements(prgDynCfg->td_align,CFG_DEL) != igFieldCount ||
	    GetNoOfElements(prgDynCfg->td_valign,CFG_DEL) != igFieldCount ||
	    GetNoOfElements(prgDynCfg->td_height,CFG_DEL) != igFieldCount ||
	    GetNoOfElements(prgDynCfg->td_bgc1,CFG_DEL) != igFieldCount ||
	    GetNoOfElements(prgDynCfg->td_bgc2,CFG_DEL) != igFieldCount ||
	    GetNoOfElements(prgDynCfg->td_nowrap,CFG_DEL) != igFieldCount ||
	    GetNoOfElements(prgDynCfg->td_fontf,CFG_DEL) != igFieldCount ||
	    GetNoOfElements(prgDynCfg->td_fontc1,CFG_DEL) != igFieldCount ||
	    GetNoOfElements(prgDynCfg->td_fonts,CFG_DEL) != igFieldCount)
	{	
		dbg(TRACE,"CheckCfg: Nr. of TD-entries different. can't create table-data!");
		bgTableData = FALSE;
	}
}

/* ******************************************************************** */
/* The DumpIntTables() routine																						*/
/* ******************************************************************** */
static void DumpIntTables(void)
{
	LPLISTELEMENT 	prlEle 		= NULL;
	TAB_DEF 	*prlTable 	= NULL;
	char 		pclTable[MAX_BUFFER_LEN];
	char 		pclFields[MAX_BUFFER_LEN];

	memset(pclTable,0x00,MAX_BUFFER_LEN);
	memset(pclFields,0x00,MAX_BUFFER_LEN);
	prlEle = ListFindFirst(prgTableList);
	while(prlEle != NULL)
	{
		prlTable = (TAB_DEF*)prlEle->Data;
		if (strcmp(prlTable->tana,pclTable) != 0)
		{
			dbg(DEBUG,"");
			dbg(DEBUG,"TABLE : <%s>",prlTable->tana);
			dbg(DEBUG,"FIELDS: <%s>",prlTable->fields);
			strcpy(pclTable,prlTable->tana);
			strcpy(pclFields,prlTable->fields);
			dbg(DEBUG,"");
		}
		dbg(DEBUG,"data  : <%s>",prlTable->data);
		prlEle = ListFindNext(prgTableList);
	}
}

/* ******************************************************************** */
/* The CreateHtmlDestFile() routine																				*/
/* ******************************************************************** */
static int CreateHtmlDestFile(void)
{
	int ilRC = RC_SUCCESS;
	int ilCnt = 0;
	int ilFiles = 0;
	char pclTmpBuffer[MAX_BUFFER_LEN];
	char pclFileName[MAX_BUFFER_LEN];
	char *pclSrcFile = prgDynCfg->html_src_files;

	TrimRight(prgDynCfg->html_src_files);
	if(strlen(prgDynCfg->html_src_files) > 0)
	{
		ilFiles = GetNoOfElements(prgDynCfg->html_src_files,CFG_DEL);
		for (ilCnt = 0; ilCnt < ilFiles; ilCnt++)
		{
			memset(pclFileName,0x00,MAX_BUFFER_LEN);
			memset(pclTmpBuffer,0x00,MAX_BUFFER_LEN);
			pclSrcFile = CopyNextField(pclSrcFile,CFG_DEL,pclFileName);
			if (ilCnt == 0)
			{
				sprintf(pclTmpBuffer, "cat %s%s > %s%s",prgDynCfg->html_path,pclFileName
					,prgDynCfg->html_path,prgDynCfg->html_dest_file);
				ilRC = system(pclTmpBuffer);
			}
			else
			{
				sprintf(pclTmpBuffer, "cat %s%s >> %s%s",prgDynCfg->html_path,pclFileName
					,prgDynCfg->html_path,prgDynCfg->html_dest_file);
				ilRC = system(pclTmpBuffer);
			}
			dbg(DEBUG,"CreateHtmlDestFile: system call   : <%s>",pclTmpBuffer);
			if (ilRC != 0)
			{
				dbg(TRACE,"CreateHtmlDestFile: system returns: <%s>",strerror(errno));
			}
		}
	}
	else
	{
		sprintf(pclTmpBuffer, "cat %s%s > %s%s",prgDynCfg->html_path,prgDynCfg->html_table,prgDynCfg->html_path,prgDynCfg->html_dest_file);
		ilRC = system(pclTmpBuffer);
		dbg(DEBUG,"CreateHtmlDestFile: system call   : <%s>",pclTmpBuffer);
		if (ilRC != 0)
		{
			dbg(DEBUG,"CreateHtmlDestFile: system returns: <%s>",strerror(errno));
		}
	}
	return ilRC;
}

/* ******************************************************************** */
/* The UtcToLocal(char *pcpTime) routine 				*/
/*									*/
/*	This function replaces the given UTC value into the local time 	*/
/*									*/
/* ******************************************************************** */
static int UtcToLocal(char *pcpTime)
{
	int c;
		char year[5], month[3], day[3], hour[3], minute[3],second[3];
		struct tm TimeBuffer, *final_result;
		time_t time_result;

		/********** Extract the Year off CEDA timestamp **********/
		for(c=0; c<= 3; ++c)
		{
				year[c] = pcpTime[c];
		}
		year[4] = '\0';
		/********** Extract month, day, hour and minute off CEDA timestamp **********/
		for(c=0; c <= 1; ++c)
		{
			month[c]  = pcpTime[c + 4];
			day[c]    = pcpTime[c + 6];
			hour[c]   = pcpTime[c + 8];
			minute[c] = pcpTime[c + 10];
			second[c] = pcpTime[c + 12];
		}
		/********** Terminate the Buffer strings **********/
		month[2]  = '\0';
		day[2]    = '\0';
		hour[2]   = '\0';
		minute[2] = '\0';
		second[2] = '\0';


		/***** Fill a broken-down time structure incl. string to integer *****/
		TimeBuffer.tm_year  = atoi(year) - 1900;
		TimeBuffer.tm_mon   = atoi(month) - 1;
		TimeBuffer.tm_mday  = atoi(day);
		TimeBuffer.tm_hour  = atoi(hour);
		TimeBuffer.tm_min   = atoi(minute);
		TimeBuffer.tm_sec   = atoi(second);
		TimeBuffer.tm_isdst = 0;
		/***** Make secondbased timeformat and correct mktime *****/
		time_result = mktime(&TimeBuffer) - timezone;
		/***** Reconvert into broken-down time structure *****/
		final_result = localtime(&time_result);

		sprintf(pcpTime,"%d%.2d%.2d%.2d%.2d%.2d"
			,final_result->tm_year+1900
			,final_result->tm_mon+1
			,final_result->tm_mday
			,final_result->tm_hour
			,final_result->tm_min
			,final_result->tm_sec);

		return(0); /**** DONE WELL ****/
}

static void ReplaceEstTime(char *pcpEstBuf,char *pcpBuffer)
{
        char pclTmpEstTime[300];
        char pclTime[6];
        char *pclTmpPointer = NULL;
        char *pclTmpPointer2 = NULL;
		
		
		
        char *pclPointer  = strstr(pcpBuffer,"HH:MM");		
        char *pclPointer1 = strstr(pcpBuffer,"hh:mm");
        char *pclPointer2 = strstr(pcpBuffer,"HH:mm");
        char *pclPointer3 = strstr(pcpBuffer,"hh:MM");
		// Latest BDPSUIF convert the : to \277 
		char *pclPointer3a  = strstr(pcpBuffer,"HH\277MM");		

        char *pclPointer4 = strstr(pcpBuffer,"HHMM");
        char *pclPointer5 = strstr(pcpBuffer,"hhmm");
        char *pclPointer6 = strstr(pcpBuffer,"HHmm");
        char *pclPointer7 = strstr(pcpBuffer,"hhMM");

        if (pclPointer!= NULL)
        {
                pclTmpPointer = pclPointer;
        }
        else
        {
                if (pclPointer1!= NULL)
                {
                        pclTmpPointer = pclPointer1;
                }
                else
                {
                        if (pclPointer2!= NULL)
                        {
                                pclTmpPointer = pclPointer2;
                        }
                        else
                        {
                                if (pclPointer3!= NULL)
                                {
                                        pclTmpPointer = pclPointer3;
                                }
                                else
                                {
                                        if (pclPointer3a!= NULL)
										{
											pclTmpPointer = pclPointer3a;
										}
										else
										{
											pclTmpPointer = NULL;
										}		
                                }
                        }
                }
        }

        if (pclPointer4!= NULL)
        {
                pclTmpPointer2 = pclPointer4;
        }
        else
        {
                if (pclPointer5!= NULL)
                {
                        pclTmpPointer2 = pclPointer5;
                }
                else
                {
                        if (pclPointer6!= NULL)
                        {
                                pclTmpPointer2 = pclPointer6;
                        }
                        else
                        {
                                if (pclPointer7!= NULL)
                                {
                                        pclTmpPointer2 = pclPointer7;
                                }
                                else
                                {
                                        pclTmpPointer2 = NULL;
                                }
                        }
                }
        }

        if (pclTmpPointer != NULL)
        {
                if (strlen(pcpEstBuf)>0)
                {
                        memset(pclTmpEstTime,0x00,300);
                        strcpy(pclTmpEstTime,pcpEstBuf);
                        memset(pclTime,0x00,6);
                        strncpy(pclTime,(char*)&pclTmpEstTime[8],2);
                        strncat(pclTime,":",1);
                        strncat(pclTime,(char*)&pclTmpEstTime[10],2);
                        strncpy(pclTmpPointer,pclTime,5);
                }
                else
                {
                        strncpy(pclTmpPointer,"     ",5);
                }
        }

        if (pclTmpPointer2 != NULL)
        {
                if (strlen(pcpEstBuf)>0)
                {
                        memset(pclTmpEstTime,0x00,300);
                        strcpy(pclTmpEstTime,pcpEstBuf);
                        memset(pclTmpEstTime,0x00,300);
                        UtcToLocal(pclTmpEstTime);
                        memset(pclTime,0x00,6);
                        strncpy(pclTime,(char*)&pclTmpEstTime[8],2);
                        strncat(pclTime,(char*)&pclTmpEstTime[10],2);
                        strncpy(pclTmpPointer2,pclTime,4);
                }
                else
                {
                        strncpy(pclTmpPointer2,"     ",4);
                }
        }
}


static void HighlightUpdates(char *pcpBuffer, char *pcpUrno)
{
	int	ilRC		= RC_SUCCESS;
	int 	ilFieldPos;
	time_t	ildifference	= 0;
	short 	slCursor 	= 0;		/* Cursor for SQL call		*/
	short 	slSqlFunc;			/* Type of SQl Call		*/
	char 	pclSqlBuf[S_BUFF];			/* Buffer for SQl statements 	*/
	char 	pclDataArea[XS_BUFF];	/* Buffer for retrieved data	*/
	char 	pclOraErrorMsg[MAX_BUFFER_LEN]; /* Buffer for error message from SQl call */
	char    pclLstu[XS_BUFF];
	char    pclRemp[XS_BUFF];
	char    pclTmoa[XS_BUFF];
	time_t 	tactualtime		= NULL;
	struct  tm *prllstutime		= NULL;
	
	/** Is HighlightUpdates Section active?? If not leave function. 	**/	
	if ((prgDynCfg->highlightupdate == NULL) && (prgDynCfg->highlighttmo == NULL) && (prgDynCfg->highlightremark == NULL))
	{
		return;
	} 

	/* reset buffers */
	memset(pclSqlBuf,0x00,S_BUFF);
	memset(pclDataArea,0x00,XS_BUFF);
	memset(pclOraErrorMsg,0x00,MAX_BUFFER_LEN);	
	memset(pclLstu,0x00,XS_BUFF);
	memset(pclRemp,0x00,XS_BUFF);
	memset(pclTmoa,0x00,XS_BUFF);


	/** Select lstu from table for further processing		**/
	
	/* build sql statement for id of rotation flight */	
	if (strlen(prgDynCfg->highlightupdate)==3) {							
		sprintf(pclSqlBuf,"select LSTU from %sTAB where URNO='%s'",prgDynCfg->highlightupdate,pcpUrno);
	}
	/*dbg(TRACE,"HighlightUpdates: ORA-SQL <%s> ", pclSqlBuf);*/
	/* set type of sql call */
	slSqlFunc = START;

	/* run sql */
	if ((ilRC = sql_if(slSqlFunc,&slCursor,pclSqlBuf,pclDataArea))!= RC_SUCCESS) 
	{
		if (ilRC != NOTFOUND)
		{
			get_ora_err(ilRC,pclOraErrorMsg);	
			dbg(TRACE,"HighlightUpdates: ORA-ERR <%s>  <%s>",pclOraErrorMsg, pclSqlBuf);
		}
		/* close cursor */
		close_my_cursor(&slCursor);
		return;
	} else {
		/* copy value */
       		GetDataItem(pclLstu, pclDataArea, 1, '\0', "", " \0");
	}

	/* close cursor */
	close_my_cursor(&slCursor);

        /* reset buffers */
        memset(pclSqlBuf,0x00,S_BUFF);
        memset(pclDataArea,0x00,XS_BUFF);
        memset(pclOraErrorMsg,0x00,MAX_BUFFER_LEN);

        /** Select values from afttab or ccatabfor further processing           **/

        /* build sql statement for id of rotation flight */
        if (strcmp(prgDynCfg->highlightupdate,"AFT")==0) {                                                            
                sprintf(pclSqlBuf,"select REMP,TMOA from AFTTAB where URNO='%s'",pcpUrno);
        } else if (strcmp(prgDynCfg->highlightupdate,"CCA")==0) {
                sprintf(pclSqlBuf,"select DISP,LSTU from CCATAB where FLNU='%s'",pcpUrno);
        } else {
		sprintf(pclSqlBuf,"select LSTU from %sTAB where URNO='%s'",prgDynCfg->highlightupdate,pcpUrno);
	}

	/*dbg(TRACE,"HighlightUpdates: ORA-SQL <%s> ", pclSqlBuf);*/
        /* set type of sql call */
        slSqlFunc = START;

        /* run sql */
        if ((ilRC = sql_if(slSqlFunc,&slCursor,pclSqlBuf,pclDataArea))!= RC_SUCCESS)
        {
                if (ilRC != NOTFOUND)
                {
                        get_ora_err(ilRC,pclOraErrorMsg);
                        dbg(TRACE,"HighlightUpdates: ORA-ERR <%s>  <%s>",pclOraErrorMsg, pclSqlBuf);
                }
                /* close cursor */
                close_my_cursor(&slCursor);
                return;
        } else {
                /* copy value */
                GetDataItem(pclRemp, pclDataArea, 1, '\0', "", " \0");
                GetDataItem(pclTmoa, pclDataArea, 2, '\0', "", " \0");
        }

        /* close cursor */
        close_my_cursor(&slCursor);


	/******** HIGHLIGHT UPDATES *********/
	if (prgDynCfg->highlightupdate != NULL) {
		/* Now get actual time */
		tactualtime 	= time(0);
	
	
		/* get the difference in seconds between actual date and lstu */
		ildifference 	= tactualtime - CEDADatetolong(pclLstu);

		/* compare and change colors if difference of actualdate and lstu is smaller than configured */
		if ((atoi(prgDynCfg->highlightupdate_difference) - ildifference) > 0 ) {
			/* Now change the colors */
			strcpy(prgDynCfg->td_bgc1,prgDynCfg->highlightupdate_bgcolor1);
			strcpy(prgDynCfg->td_bgc2,prgDynCfg->highlightupdate_bgcolor2);	
			dbg(DEBUG,"\n\nHighlightUpdates: ildifference<%d> for lstu <%s> and conf <%d> anddiff <%d>",ildifference,pclLstu,atoi(prgDynCfg->highlightupdate_difference),(atoi(prgDynCfg->highlightupdate_difference) - ildifference));
		}
	} /* HIGHLIGHT UPDATES */

	/******** HIGHLIGHT TMOA *********/
	if (prgDynCfg->highlighttmo != NULL) {
		if (strlen(pclTmoa) > 10) {
			strcpy(prgDynCfg->td_bgc1,prgDynCfg->highlightupdate_bgcolor1);
			strcpy(prgDynCfg->td_bgc2,prgDynCfg->highlightupdate_bgcolor2);	
			dbg(DEBUG,"\n\nHighlightTMOA: pclTmoa <%s> ",pclTmoa);
		}
	} /* HIGHLIGHT TMOA */
	
	/******** HIGHLIGHT REMARK *********/
	if (prgDynCfg->highlightremark != NULL) {
		/* Is remark in field list ? */
		if ((strstr(prgDynCfg->fields,"remp") == NULL) \
			&& (strstr(prgDynCfg->fields, "REMP") == NULL) \
			&& (strstr(prgDynCfg->fields, "disp") == NULL) \
			&& (strstr(prgDynCfg->fields, "DISP") == NULL)) {
			return;
		}

		/* extract remark */
		memset(pclLstu,0x00,XS_BUFF);
		for (ilFieldPos=1;ilFieldPos < igFieldCount;ilFieldPos++)
		{	
			GetDataItem(pclLstu, prgDynCfg->fields,ilFieldPos, CFG_DEL, "", " \0");
			if (strstr(pclLstu, "remp") != NULL || strstr(pclLstu, "REMP") != NULL \
				|| strstr(pclLstu, "disp") != NULL || strstr(pclLstu, "DISP") != NULL) {break;}
		} /* for all fields */	

		memset(pclLstu,0x00,XS_BUFF);	
		GetDataItem(pclLstu, pcpBuffer,ilFieldPos, ucgData_del, "", " \0");
		TrimRight(pclRemp);

		if ((strstr(prgDynCfg->highlightremark,pclRemp) != NULL) && (strlen(pclRemp) > 0)) {
			strcpy(prgDynCfg->td_bgc1,prgDynCfg->highlightupdate_bgcolor1);
			strcpy(prgDynCfg->td_bgc2,prgDynCfg->highlightupdate_bgcolor2);	
			dbg(DEBUG,"\n\nHighlightREMARK: remark <%s> buffer <%s>",pclLstu,pcpBuffer);
		}
	} /* HIGHLIGHT REMARK */

	return;

}

static time_t CEDADatetolong(char *pcpTime)
{
	int c;
		char year[5], month[3], day[3], hour[3], minute[3],second[3];
		struct tm TimeBuffer, *final_result;
		time_t time_result;

		/********** Extract the Year off CEDA timestamp **********/
		for(c=0; c<= 3; ++c)
		{
				year[c] = pcpTime[c];
		}
		year[4] = '\0';
		/********** Extract month, day, hour and minute off CEDA timestamp **********/
		for(c=0; c <= 1; ++c)
		{
			month[c]  = pcpTime[c + 4];
			day[c]    = pcpTime[c + 6];
			hour[c]   = pcpTime[c + 8];
			minute[c] = pcpTime[c + 10];
			second[c] = pcpTime[c + 12];
		}
		/********** Terminate the Buffer strings **********/
		month[2]  = '\0';
		day[2]    = '\0';
		hour[2]   = '\0';
		minute[2] = '\0';
		second[2] = '\0';


		/***** Fill a broken-down time structure incl. string to integer *****/
		TimeBuffer.tm_year  = atoi(year) - 1900;
		TimeBuffer.tm_mon   = atoi(month) - 1;
		TimeBuffer.tm_mday  = atoi(day);
		TimeBuffer.tm_hour  = atoi(hour);
		TimeBuffer.tm_min   = atoi(minute);
		TimeBuffer.tm_sec   = atoi(second);
		TimeBuffer.tm_isdst = 0;
		/***** Make secondbased timeformat and correct mktime *****/
		time_result = mktime(&TimeBuffer) - timezone;

		return time_result; /**** DONE WELL ****/
}



/* ******************************************************************** */
/* ******************************************************************** */
/* ******************************************************************** */
/* 		EOF HTMHDL						*/
/* ******************************************************************** */
/* ******************************************************************** */
/* ******************************************************************** */
