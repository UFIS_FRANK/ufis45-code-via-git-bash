#ifndef _DEF_mks_version_stxhdl_h
  #define _DEF_mks_version_stxhdl_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_stxhdl_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/stxhdl.h 1.2 2004/07/27 17:04:32SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The Master include file.						  */
/*									  */
/*  Program	  :     						  */
/*  Revision date :							  */
/*  Author    	  :							  */
/*  									  */
/*  NOTE : This should be the only include file for your program          */
/* ********************************************************************** */
#ifndef _STXHDL_H
#define _STXHDL_H


#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>

#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "tools.h"
#include "helpful.h"
#include "debugrec.h"
#include "ctxhdl.h"
#include "db_if.h"
#include "new_catch.h"
#include "syslib.h"
/* PROD4.4_BEGIN 04.12.00/GKA/Removed HSB functionality */
/* #include "hsbsub.h"*/
/* PROD4.4_END 04.12.00/GKA/Removed HSB functionality */

extern char *strdup(const char*);

#define	iASCII							0
#define	iANSI								1
#define	iIS_DYNAMIC						0
#define	iIS_FIX							1
#define	iUSE_NONE_FIELDNAME			0
#define	iUSE_FIELDNAME					1

#define	iINITIAL							-1
#define	iBLOCK							1
#define	iDATA								2

#define	iTABLE_STEP						128
#define	iMEMORY_STEP					0x8FFFF	

#define	IsAscii(a) 			((a) == iASCII ? 1 : 0)
#define	IsAnsi(a)  			((a) == iANSI  ? 1 : 0)
#define	IsCR(a)				(strcmp((a),"CR") ? 0 : 1)	
#define	IsLF(a)				(strcmp((a),"LF") ? 0 : 1)	
#define	IsDynamic(a)		((a) == iIS_DYNAMIC ? 1 : 0)
#define	IsFix(a)				((a) == iIS_FIX ? 1 : 0)
#define	IsBlock(a,b,c,d)	(((a) == (b) && (c) == (d)) ? 1 : 0)

typedef struct	_stati
{
	UINT				iNoAllRows;
	UINT				iNoRowWithSHMFailure;
	UINT				iNoEmptyRows;
} Statistics;

typedef struct _syslib
{
	char				pcTabName[iMIN];	/* Name of table */
	char				pcTabField[iMIN]; /* field in table */	
	char				pcIField[iMIN];	/* internal field */
	char				pcResultField[iMIN]; /* result field */
} SysLib;

typedef struct _tsttabdef
{
	char				*pcAlignment; /* the alignment LEFT/RIGTH/ORIGINAL */
	int				  iNoSyslib;	/* how many syslib data entries */
	SysLib			*prSyslib;	/* for all syslibs... */
	char				*pcULOSign; /* format received data UPPER/LOWER/ORIGINAL */
	char				pcTableName[iMIN]; /* the table name */
	int				  iNoOfFields;	/* number of fields for this table */
	char				**pcFields;	/* the fields */
	int				  iNoOfFieldLength;	/* counter for field-length */
	short				*psFieldLength;	/* length of fields defined in database */
	int				  iNoOfDataLength;	/* number of next */
	UINT				*piDataLength; /* optionally, the data field length */
	int				  iNoOfDataPosition; /* position */
	UINT				*piStartDataPosition; /* position start:end */
	UINT				*piEndDataPosition; /* position start:end */
	char				pcSndCmd[iMIN]; /* command (cmdblk->command) */
	char				pcRecvName[iMIN]; /* command (BCHead->recv_name) */
	char				pcDestName[iMIN]; /* command (BCHead->dest_name) */
	int			  	iModID;	/* the mod_id we use... */
	UINT				iDeleteEndBlanks; /* blanks at end? */
	UINT				iDeleteStartBlanks; /* blanks at head... */
	char				**pcDeleteFieldStartCharacter; /* start character to delete */
	char				**pcDeleteFieldEndCharacter; /* end character to delete */ 
	char				**pcDeleteFieldCharacter; /* character to delete (all) */
  int         iNoOfWhereFields; /* number of where-clause-fields  */
  char        **pcWhereFields; /* the where-clause-fields  */
  int         iHopoInFieldlist;  /* Hopo in Fieldlist */
} TSTTabDef;

typedef struct _tstsection
{
	char				pcHomeAirport[iMIN]; /* the home airport */
	char				pcTableExtension[iMIN]; /* the table extension */
	int				iIgnoreRecordsShorterThan; /* length of records */
	int				iNoOfIgnoreStrings; /* number of strings to ignore */
	char				**pcIgnoreStrings; /* all (first) strings to ignore */
	int				iHeader;	/* how many header rows... */
	int				iTrailer; /* how may trailer rows... */
	Statistics		rlMyStat;	/* only for debug info... */
	int				iSULength; /* length of all fields... */
	UINT				iUseSystemKnz;	/* is there a system kennzeichen */
	char				pcSystemKnz[iMAX_BUF_SIZE]; /* put in selection... */
	char				pcCommand[iMIN];	/* recognized command */
	int				iNoOfEOLCharacters;	/* number of end of line characters */
	char				pcEndOfLine[iMIN];	/* end of line characters */
	int				iUseComment;	/* should i use comment? */
	char				pcCommentSign[iMIN]; /* sign for comment */
	char				pcDataSeparator[iMIN]; /* separator for dynamic files */
	UINT				iFileType;		/* fix or dynamic */
	UINT				iCharacterSet;		/* ANSI or ASCII */
	UINT				iFieldName;		/* with or without fieldnames */
	int				iNoOfBlk;	/* no. of blocks */
	char				pcSBlk[iMIN];	/* all start of block characters */
	char				pcEBlk[iMIN];	/* all end of block characters */
	int				iNoOfTables; /* number of tables for ths section */
	TSTTabDef		*prTabDef;	/* the tables */

} TSTSection;

typedef struct _tstmain
{
	int				  iNoOfSections; /* number of all sections */
	TSTSection	*prSection;	/* the sections */

} TSTMain;

#endif /* _STXHDL_H */


