#ifndef _DEF_mks_version_ctxhdl_h
  #define _DEF_mks_version_ctxhdl_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_ctxhdl_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/ctxhdl.h 1.2 2004/07/27 16:47:25SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ==================================================================== */
/*									*/
/* header of ctxhdl.c							*/
/*									*/
/* CTXHDL               :  CONFIGURABLE TEXT HANDLER                    */
/*									*/
/* Author		:  Bernhard Straesser				*/
/* Date			:  03/1995					*/
/* Description		:  Interpreter for formatted texts (telex)      */
/*									*/
/* Update history	:  designed 03/1995, coded in 03/1995           */
/*			                                             	*/
/*									*/
/* ==================================================================== */

#ifndef _CTXHDL_INC
#define _CTXHDL_INC



#define CTX_CMD  "CTX"


/* Text types to interpret */
#define CTX_TYPE_TELEX		1
#define CTX_TYPE_PDI		2
#define CTX_TYPE_NSY		3
#define CTX_TYPE_NCO		4
#define CTX_TYPE_RES_SCREEN	5
#define CTX_TYPE_VTX		6
#define CTX_TYPE_WDF		7
#define CTX_TYPE_FLUKO		8
#define CTX_TYPE_TA         9
#define CTX_TYPE_TD	       10
#define CTX_TYPE_FISOPS    11
#define CTX_TYPE_WLD       12
#define CTX_TYPE_WLP       13
#define CTX_TYPE_SEAUPD    14

/**** Result to - Codes ****/

#define CTX_RESPONSE_QUICK 64   /* To OR with CTX_RESULT_... */

/* Back to sender only */
#define CTX_RESULT_SENDER	1
/* Only in DB */
#define CTX_RESULT_DBONLY	2
/* Only send_error_msg */
#define CTX_RESULT_ERRMSG	3
/* DB or send_error_msg */
#define CTX_RESULT_DB_ERRMSG	4



struct _ctxblk {                /* ------------------------------------ */
        int  text_type;         /*                                      */
        int  result_to;         /*                                      */
        char data[1];           /*                                      */
};/* end of struct _ctxblk */
typedef struct _ctxblk CTXBLK;
#define CTXBLK_SIZE sizeof(CTXBLK)


#endif  /* end #ifndef _CTXHDL_H */

/* ********************************************************* */
/* ********************************************************* */
/* ********************************************************* */
/* ********************************************************* */
/* ********************************************************* */
/* ********************************************************* */
/* ********************************************************* */
                 
