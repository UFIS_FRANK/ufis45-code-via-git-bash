#ifndef _DEF_mks_version_uevent_h
  #define _DEF_mks_version_uevent_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_uevent_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/uevent.h 1.4 2006/01/17 18:26:52SGT jim Exp  $";
#endif /* _DEF_mks_version */
/****************************************************************************/
/****************************************************************************/

/* ******************************************************************** */
/* uevent.h   10.02.94							*/
/* This module contains the defines for the UGCCS internal events	*/
/*									*/
/* 20050313 JIM: added define EVT_FUNC(a).... */
/* ******************************************************************** */

#ifndef UEVENT_DEF			/* Avoid multiple inclusions */
#define UEVENT_DEF


#include "glbdef.h"
/* The layout of an internal event is as follows:

	+-------------------------------+
	|	   Event Type		|
	+-------------------------------+
	|	    Command		|
	+-------------------------------+
	|	   Originator		|
	+-------------------------------+
	|  System ID (when applicable)	|
	+-------------------------------+
	|	  Retry count		|
	+-------------------------------+
	|     Offset to event data	|
	+-------------------------------+
	|     Length of event data	|
	+-------------------------------+
	|				|
		   Event data
	|				|
	+-------------------------------+

*/

struct Ugccs_Event_Header {
	short	type;				/* The event type */
	short	command;			/* The internal command */
	short	originator;			/* The event originator ID */
	char	sys_ID[SYSID_SIZE];		/* The system ID */
	short	retry_count;			/* The retry count */
	short	data_offset;			/* Offset to event data */
	unsigned long	data_length;		/* Length of event data */
};

typedef struct Ugccs_Event_Header EVENT;

#define	EVENT_H_LEN	sizeof(EVENT)


/* General event type defines:

	USR_EVENT	: The event is a user event
	SYS_EVENT	: The event is an internal system event
*/

#define USR_EVENT	0			/* An UGCCS user event */
#define	SYS_EVENT	1			/* An UGCCS system event */

/* General event commands:

	RESET		: Requests a (re)initialization of the receiver
	SHUTDOWN	: Requests a termination of the receiver
	EVENT_DATA	: The event contains data.
	SESS_DATA	: The event data contains a session header
	END_O_SEQ	: End of a session sequence
	LOG_SWITCH	: Switch logfile command
	IP_EVENT	: Special inter process event
 	IP_SHUTDOWN	: Special inter process event
	SWITCH_READY    : Logfile switched	
	PORT_DATA	: Event with sysid & port  
	DEVICE_ENABLE   : manual device enable
 	DEVICE_DISABLE  : device disable
	IPG_END         : indicates that the screen paging is finished 
	OPEN_FILES	: open portfiles request 
	All system commands are offset by hex FF00
*/

#define SYS_OFFSET	0x7000			/* Offset for system cmds   */

#define	RESET		1 + SYS_OFFSET		/* Reset ( Initialize )	    */
#define	SHUTDOWN	2 + SYS_OFFSET		/* Shutdown command         */
#define EVENT_DATA	1			/* Normal event data        */
#define LOG_SWITCH	2			/* Switch logfile command   */
#define SESS_DATA	3			/* Session controlled event */
#define END_O_SEQ	4			/* End of sequence (sess.)  */
#define IP_EVENT	5			/* Initialization data      */
#define SWITCH_READY    6			/* logfile switched         */
#define TRACE_ON	7			/* Event with sysid & port  */
#define TRACE_OFF   	8			/* manual device enable     */
#define IPG_END		9                       /* end of scr ( icon pager) */
#define REMOTE_DB	10			/* remote db request	    */
						/* formaly OPEN_FILES */
#define DEVICE_DISABLE  11			/* manual device disable    */
#define IP_SHUTDOWN  	12			/* shutdown for all ports   */
#define OPEN_FILES	13			/* open portfiles request   */
#define BACKUP_DB	14
#define REMOTE_QUE	15
#define EVENT_LPB		19
#define HSB_DOWN        20                      /* local Sys. standby   */  
#define HSB_STANDALONE  21			/* local Sys. comming   */
#define HSB_COMING_UP  22                      /* local Sys. standalone */  
#define HSB_COMMING_UP  22                      /* local Sys. standalone */  
#define HSB_STANDBY     23                      /* local Sys. activ     */
#define HSB_ACTIVE      24                      /* local Sys. down      */
#define HSB_ACT_TO_SBY  25			/* Active to standby    */
/* whoever adds some EVENT_FUNCT defines has to add the appropiate string below */

#define         EVT_FUNC(a) \
   ((a) ==  (RESET         ) ? "RESET         " : \
   ((a) ==  (SHUTDOWN      ) ? "SHUTDOWN      " : \
   ((a) ==  (EVENT_DATA    ) ? "EVENT_DATA    " : \
   ((a) ==  (LOG_SWITCH    ) ? "LOG_SWITCH    " : \
   ((a) ==  (SESS_DATA     ) ? "SESS_DATA     " : \
   ((a) ==  (END_O_SEQ     ) ? "END_O_SEQ     " : \
   ((a) ==  (IP_EVENT      ) ? "IP_EVENT      " : \
   ((a) ==  (SWITCH_READY  ) ? "SWITCH_READY  " : \
   ((a) ==  (TRACE_ON      ) ? "TRACE_ON      " : \
   ((a) ==  (TRACE_OFF     ) ? "TRACE_OFF     " : \
   ((a) ==  (IPG_END       ) ? "IPG_END       " : \
   ((a) ==  (REMOTE_DB     ) ? "REMOTE_DB     " : \
   ((a) ==  (DEVICE_DISABLE) ? "DEVICE_DISABLE" : \
   ((a) ==  (IP_SHUTDOWN   ) ? "IP_SHUTDOWN   " : \
   ((a) ==  (OPEN_FILES    ) ? "OPEN_FILES    " : \
   ((a) ==  (BACKUP_DB     ) ? "BACKUP_DB     " : \
   ((a) ==  (REMOTE_QUE    ) ? "REMOTE_QUE    " : \
   ((a) ==  (EVENT_LPB     ) ? "EVENT_LPB     " : \
   ((a) ==  (HSB_DOWN      ) ? "HSB_DOWN      " : \
   ((a) ==  (HSB_STANDALONE) ? "HSB_STANDALONE" : \
   ((a) ==  (HSB_COMING_UP ) ? "HSB_COMING_UP " : \
   ((a) ==  (HSB_COMMING_UP) ? "HSB_COMMING_UP" : \
   ((a) ==  (HSB_STANDBY   ) ? "HSB_STANDBY   " : \
   ((a) ==  (HSB_ACTIVE    ) ? "HSB_ACTIVE    " : \
   ((a) ==  (HSB_ACT_TO_SBY) ? "HSB_ACT_TO_SBY" : \
   fcUnknownEvent(a) )))))))))))))))))))))))))
/* whoever adds some EVENT_FUNCT defines has to add closing bracket ')' */
/* fcUnknownEvent is defined in debugrec.c because we need string result */

   
   
   
   
/* QCP limit, the real limit at the moment	*/
#define QCP_MAX_LEN 	0x6000

/************************************************************************/


struct sess_header {
	int	session_no;
	int	destination;
};

typedef struct sess_header SESS_H;

struct _number_header {
	int	number;
};

typedef struct _number_header NO_HEADER;


#endif /* end of UEVENT.h  */
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
