
prompt prerequisite: a link in /ora/webfids must point to free space
prompt create link: ln -s /u04 /ora/webfids

CREATE TABLESPACE WEBFIDS DATAFILE
        '/ora/webfids/oradata/UFIS/WEBFIDS.dbf' SIZE 2000M 
        autoextend on
default storage (
        initial         1M
        next            1M
        pctincrease     0
        minextents      1
        maxextents      unlimited
        );
