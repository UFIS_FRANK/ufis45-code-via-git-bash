#ifndef _DEF_mks_version_router_h
  #define _DEF_mks_version_router_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_router_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/router.h 1.2 2004/07/27 16:48:22SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ***************************************************************** */
/*  The Master include file.						  */
/*									  */
/*  Program	  :     						  */
/*  Revision date :							  */
/*  Author    	  :							  */
/*  									  */
/*  NOTE : This should be the only include file for your program   */
/* ****************************************************************** */

#ifndef _ROUTER_H 
#define _ROUTER_H


#include <stdio.h>
#include <malloc.h>
#include<math.h>
#include<time.h>

#include "ugccsma.h"
#include "glbdef.h"
#include "msgno.h"
#include "quedef.h"
#include "uevent.h"
#include "send_tools.h"

#define NO_OF_PNS 1024 /* Max. no of unique processes */
#define MAX_REC_LEN 30 /* Max. rec length */
#define STR 7 /* Max length of individual fields in records */


/***********************************************************
* PN_QUE is a struct that contains information about queues
***********************************************************/
struct pn_que {
	int iModID;
	time_t lTime;
	struct pn_que *prNext;
}; 
typedef struct pn_que PN_QUE;

/***********************************************************
* QUE_LST is a struct used to create list of dynamic queue
* information.
***********************************************************/
typedef struct {
	struct pn_que *prQueue;
	int iNoOfItems;
} QUE_LST;

/**************************************************************
* PN_HEAD is a struct that define the process name, no. of 
* instances of this process and a pointer to a ring of mod_ids
***************************************************************/
typedef struct {
	char pcProcessName[10]; /* Process name */
	struct pn_que *prLastQueue;  /* pointer to last accessed queue */
	int iNoOfQueues;		/* Number of queues created for this process */
	struct pn_que *prQueue; /* Pointer to information about queues */
} PN_HEAD;



/* 
 *
 * Following the Check table  structure CHKTAB
 *
 */
typedef struct {
	char		table_name[8];
	char		cmd[6];
	char		receiver[7];  /* Process name, i.e 1st 6 characters of it */
} CHKTAB;

/* 
 *
 * Following the Check table  structure C2_CHKTAB
 *
 */
typedef struct {
	char		table_name[8];
	char		cmd[6];
	int		receiver;  /* Process name, i.e 1st 6 characters of it */
} C2_CHKTAB;

/* 
 *
 * Following the Command table  CMDTAB
 *
 */
typedef struct {
	char		command[6];
	char 		receiver[7]; /* Process name */ 
} CMDTAB;

/* 
 *
 * Following the Command table  C1_CMDTAB
 *
 */
typedef struct {
	char		command[6];
	int 		receiver; /* Process id */ 
} C1_CMDTAB;



#endif
