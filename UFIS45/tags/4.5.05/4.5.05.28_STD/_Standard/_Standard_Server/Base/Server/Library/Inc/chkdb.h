#ifndef _DEF_mks_version_chkdb_h
  #define _DEF_mks_version_chkdb_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_chkdb_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/chkdb.h 1.2 2004/07/27 16:46:59SGT jim Exp  $";
#endif /* _DEF_mks_version */
/*	**********************************************************************	*/
/*	The Master include file.											*/
/*																*/
/*	Program		:	chkdb										*/
/*	Revision date	:												*/
/*	Author		:	jmu											*/
/*																*/
/*	NOTE			:	This should be the only include file for your program	*/
/*	**********************************************************************	*/

#include	"chkdef.h"

#define	USER_FIELD_LIST1	"TABLE_NAME,TABLESPACE_NAME,CLUSTER_NAME,PCT_FREE,PCT_USED,INI_TRANS,MAX_TRANS,INITIAL_EXTENT,NEXT_EXTENT,MIN_EXTENTS,MAX_EXTENTS,PCT_INCREASE,BACKED_UP,NUM_ROWS,BLOCKS,EMPTY_BLOCKS,AVG_SPACE,"
#define	USER_FIELD_LIST2	"CHAIN_CNT,AVG_ROW_LEN,DEGREE,INSTANCES,CACHE"



