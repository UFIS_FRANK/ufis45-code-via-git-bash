#ifndef _DEF_mks_version_reldpl_h
  #define _DEF_mks_version_reldpl_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_reldpl_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/reldpl.h 1.2 2004/07/27 16:48:18SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The Master include file.                                              */
/*                                                                        */
/*  Program       :                                                       */
/*                                                                        */
/*  Revision date :                                                       */
/*                                                                        */
/*  Author    	  :                                                       */
/*                                                                        */
/*                                                                        */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */
