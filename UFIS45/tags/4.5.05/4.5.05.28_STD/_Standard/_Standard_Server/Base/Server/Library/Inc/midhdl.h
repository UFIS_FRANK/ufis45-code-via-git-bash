#ifndef _DEF_mks_version_midhdl_h
  #define _DEF_mks_version_midhdl_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_midhdl_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/midhdl.h 1.2 2004/07/27 16:48:03SGT jim Exp  $";
#endif /* _DEF_mks_version */
/*********************************************************/
/* MIDHDL (AEG-MIS LCD board controller handler )        */
/* header file                                           */
/*********************************************************/
#ifndef BOOL
#define BOOL int
#endif

/***************/
/* ALL DEFINES */
/***************/
#define XXS_BUFF  32
#define XS_BUFF  128
#define S_BUFF  512
#define M_BUFF  1024
#define L_BUFF  2048
#define XL_BUFF 4096

#ifndef MAX_BUFFER_LEN
	#define MAX_BUFFER_LEN 2048
#endif
#define CONNECT_TIMEOUT 2
#define READ_TIMEOUT 2

#define CFG_ALPHA 1200
#define CFG_NUM		1201
#define CFG_ALPHANUM	1202
#define CFG_IGNORE	1203
#define CFG_PRINT	1204



/************************/
/* CFG-File - STRUCTURE */
/************************/
typedef struct 
{
	/* section in .cfg file */    
	char *loc_area;
	int locarea;
	char *address;
	char *location;
	char *actions;
	char *act_table;
	char *act_field;
	int ilClearButton;
	char clKeyState[10][10];
}KEYPAD_INFO;

typedef struct
{
	char *debug_level;
	char *log;
	char *write_queue;
	char *mid_type;
	char *mid_udpport;
	char *mid_tcpport;
	char *mid_ip;
	char *pollrate;
	char *clr_policy;
	char *buttons_per_flight;
	char *button_offset;
	char *number_of_mids;
	char *keypads;
	LPLISTHEADER prlKeyPads;
}MID_INFO;

typedef struct
{
	unsigned short *id; 	/* has to be 0x0200 for answer to dZine mid-controller */
	unsigned short *count; 
	unsigned long  *clientaddr;
	unsigned long  *serveraddr;
	unsigned short *tcpport;
	unsigned short *time;
	unsigned short *free;
}MID_UDP_RESPONSE;
#define MID_UDP_RESPONSE_SIZE 18
#define HEX_CONNECT_RESPONSE  0x0200
