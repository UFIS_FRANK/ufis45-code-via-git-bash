<?php 
/*
V0.60 08 Nov 2000 (c) 2000 John Lim. All rights reserved.
  Released under Lesser GPL library license. See License.txt.
*/ 
  
// specific code for tohtml

// RecordSet to HTML
//------------------------------------------------------------
// Convert a recordset to a html table. Multiple tables are generated
// if the number of rows is > $gSQLBlockRows. This is because
// web browsers normally require the whole table to be downloaded
// before it can be rendered, so we break the output into several
// smaller faster rendering tables.
//
// $rs: the recordset
// $ztabhtml: the table tag attributes
// $zheaderarray: contains the replacement strings for the headers
//
// RETURNS: number of rows displayed

 
 /* 
 Break the results into pages , we use the $gSQLBlockRows
 as the maximum records per page we also use the $RecordCount var 
 in order to known what is the number of records tha the select statement returns
 */
 
function listpages(&$rs,$RecordCount,$ztabhtml="")
{

$s =''; $flds; $ncols; $i;$rows=0;$hdr;$docnt = false;
GLOBAL $gSQLMaxRows,$gSQLBlockRows,$DatabaseType,$listElement,$movenext,$db,$list_PagingMove,$lastpage,$prevrows;
/* var for the search form */
GLOBAL $allcategory,$todateplus,$airline,$flno,$city,$dadr,$stat,$PHP_SELF;
GLOBAL $fromdateMonth,$fromdateYear,$fromdateDay,$fromdateHour,$fromdateMin;
GLOBAL $todateMonth,$todateYear,$todateDay,$todateHour,$todateMin;
GLOBAL $ol,$masktitle,$type,$debug,$tdi,$tdiminus,$HeadingTitle;
GLOBAL $tpl,$currentpagetitle,$pagecounttitle,$TMPL_MENUID,$login,$FldArray,$login;

	if (! $ztabhtml) $ztabhtml = " BORDER=3 width=50%  bgcolor=\"#77B0E4\"  border=\"0\" cellspacing=\"0\" cellpadding=\"0\"' ";

		$cr=1;

		$pagecount=round($RecordCount/$gSQLBlockRows);

		if ($pagecount < $RecordCount/$gSQLBlockRows) { 
		 $pagecount = $pagecount +1;
		}

	switch ($list_PagingMove) {		
       case "Requery":
//            list.Requery
			 break;
        case "   <<   ":
			$rs->movefirst();			
			$cr=1;
			 break;
        case "   <    ":
			if ($prevrows<20) {
				$result=$movenext-$gSQLBlockRows-$prevrows;
			} else {
				$result=$movenext-($gSQLBlockRows*2);
			}
			
			$rs->move($result);
			$cr=($result)+$gSQLBlockRows;
			 break;
        case "    >   ":
			if ($movenext>=$RecordCount) { $movenext=0;}
			$rs->move($movenext);
			$cr=($movenext-1)+$gSQLBlockRows;
			 break;
        case "   >>   ":
				$result=$pagecount*$gSQLBlockRows;
				if ($result > $RecordCount) {
					$result=$result- $RecordCount;
					$result=$gSQLBlockRows-$result;
					$result=$RecordCount-$result;
				} else {
					$result= $RecordCount - $result;
					$result=$gSQLBlockRows-$result;
					$result=$RecordCount-$result;
				echo $result;
				}
				$rs->move($result);
				$cr=($result)+$gSQLBlockRows;
	 		 break;
    }


		if  ($rs->EOF && $RecordCount >0)  {
			$rs->movefirst();
			$cr=1;
		}

		$currentpage=round( $cr/ $gSQLBlockRows );
		if ($currentpage<=0) {$currentpage=1;}
		// echo "count:$RecordCount,BlockR=$gSQLBlockRows<br>";
		if ($RecordCount>$gSQLBlockRows) { 
			// Assign the values 
			$tpl->set_var( "RecordCount",$RecordCount );		
			$tpl->set_var( "HeadingTitle",$HeadingTitle );		
			$tpl->set_var( "currentpage",$currentpagetitle.$currentpage);		
			$tpl->set_var( "pagecount",$pagecounttitle.$pagecount );				
			$tpl->set_var( "FormType","submit" );				
		}  else {
			$tpl->set_var( "RecordCount",$RecordCount );		
			$tpl->set_var( "HeadingTitle",$HeadingTitle );		
			$tpl->set_var( "currentpage",$currentpagetitle.$currentpage);		
			$tpl->set_var( "pagecount",$pagecounttitle.$pagecount );				
			$tpl->set_var( "FormType","hidden" );				
			//$tpl->set_var( "RecordCount","" );		
			//$tpl->set_var( "HeadingTitle","" );		
			//$tpl->set_var( "currentpage","");		
			//$tpl->set_var( "pagecount","" );				
			//$tpl->set_var( "FormType","hidden" );				
		}

		// We Must change the "result" to the value from the table		
		// We include the columns 
		$tpl->set_var( "ztabhtml",$ztabhtml );			
		$TableHeader="";

		$tpl->set_var( "TableHeader",$login->FldHeader ($FldArray) );				
		
		if ($rs->EOF) {
		 		// We include the NoRows 
				$TableNoRows="";
		        $TableNoRows=$TableNoRows."<td colspan=\"20\" align=\"center\" valign=\"top\">	<I class=\"norows\">We are sorry but there are no results for you</I></td>";
				$tpl->set_var( "TableRows",$TableNoRows );		
				$tpl->set_var( "SearchValues","" );	
			 	$tpl->parse("Rows",true);
		 } 
 

		while (!$rs->EOF) {
	
	 		// We include the Rows 
			$TableRows="";		
			 
			$tpl->set_var( "TableRows",	$login->FldRows ($FldArray,$rs,$rows) );		
		 	$tpl->parse("Rows",true);

			$rows += 1;
				if ($rows >= $gSQLMaxRows) {
					$rows = "<p>Truncated at $gSQLMaxRows</p>";
				break;
				}
			$rs->MoveNext();
			// additional EOF check to prevent a widow header
	    if (( !$rs->EOF && $rows == $gSQLBlockRows)|| ($rs->EOF &&$pagecount>1) ){

		$tpl->set_var("CurrentRow",$rs->CurrentRow() );		
		$tpl->set_var( "Prevrows",$rows );		
		// Assign Search Values
		$SearchValues="";
	if ($allcategory) {
		$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"allcategory\" value=\"". $allcategory."\">\n";
	}
	if ($flno) {
		$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"flno\" value=\"". $flno."\">\n";
	}
	if ($dadr) {
		$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"dadr\" value=\"". $dadr."\">\n";
	}
	if ($stat) {
		$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"stat\" value=\"". $stat."\">\n";
	}
	if ($airline) {
		$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"airline\" value=\"". $airline."\">\n";
	}

	if ($city) {
		$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"city\" value=\"". $city."\">\n";
	}

	if ($fromdateMonth) {
		$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"fromdateMonth\" value=\"". $fromdateMonth."\">\n";
	}
	if ($fromdateYear) {
		$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"fromdateYear\" value=\"". $fromdateYear."\">\n";
	}
	if ($fromdateDay) {
		$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"fromdateDay\" value=\"". $fromdateDay."\">\n";
	}
if ($fromdateHour) {
		$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"fromdateHour\" value=\"". $fromdateHour."\">\n";
}
if ($fromdateMin) {
		$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"fromdateMin\" value=\"". $fromdateMin."\">\n";
}

	if ($todateMonth) {
		$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"todateMonth\" value=\"". $todateMonth."\">\n";
	}
	if ($todateYear) {
		$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"todateYear\" value=\"". $todateYear."\">\n";
	}
	if ($todateDay) {
		$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"todateDay\" value=\"". $todateDay."\">\n";
	}
if ($todateHour) {
		$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"todateHour\" value=\"". $todateHour."\">\n";
}
if ($todateMin) {
		$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"todateMin\" value=\"". $todateMin."\">\n";
}

if ($todateplus) {
		$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"todateplus\" value=\"". $todateplus."\">\n";
}
else
{
	$todateplus="";
}
if ($debug==true) {
		$SearchValues=	$SearchValues."<input type=\"hidden\" name=\"debug\" value=\"". $debug."\">\n";
}
		$SearchValues=	$SearchValues. $login->FldSrows ($FldArray);

		$tpl->set_var( "SearchValues",$SearchValues );
				return $rows;
			} //else{
	    } // while
	return $rows;
 }
?>
