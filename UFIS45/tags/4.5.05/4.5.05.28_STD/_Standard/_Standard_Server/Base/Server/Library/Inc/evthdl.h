#ifndef _DEF_mks_version_evthdl_h
  #define _DEF_mks_version_evthdl_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_evthdl_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/evthdl.h 1.2 2004/07/27 16:47:39SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The Master include file.                                              */
/*                                                                        */
/*  Program       :                                                       */
/*                                                                        */
/*  Revision date :                                                       */
/*                                                                        */
/*  Author    	  :                                                       */
/*                                                                        */
/*                                                                        */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */

struct t_config{

	FILE		*prFile;
	char		*pcFilename;/*read this from config and check if %d is in string!*/
	int			iCount;
	int			iRecords;
	int			iCloseCount;
};

typedef struct t_config CONF;

#define FIELDDELIMITER	'\x01'		/* SOH */
#define RECORDSTART		'\x02'		/* STX */
#define RECORDEND		'\x03'		/* ETX */
