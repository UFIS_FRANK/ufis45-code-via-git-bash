# $Id: Ufis/_Standard/_Standard_Server/Base/Server/Tools/Scripts/ora_datamodel.ksh 1.2 2003/12/08 22:25:03SGT jim Exp  $
#
# 20031208 JIM: using $CEDADBUSER/$CEDADBPW instead of fix settings
#
function doc
{
cat <<EOFDOC1

====== 
Title: 
====== 
 
Report CEDA datamodel 
 
 
========= 
Abstract: 
========= 
 
Report CEDA datamodel by displaying some column data from 
the ORACLE view 'user_tab_columns'. The column DATA_DEFAULT
can not be used in rpad() functions, so the column is
truncated by awk. The value of DATA_DEFAULT is enclosed in
'<...>' brackets.
 
 
============= 
Requirements: 
============= 
 
SELECT on 'user_tab_columns' 

============= 
Author: 
============= 
Jibbo Mueller

EOFDOC1
}

# ==============================================================

function data_model
{

sqlplus -s $CEDADBUSER/$CEDADBPW @<<EOSQL
SET PAGES 0
SET LINESIZE 1024
SELECT 
  RPAD(TABLE_NAME,6)||'#'||
  RPAD(COLUMN_NAME,4)||'#'||
  RPAD(DATA_TYPE,8)||'#'||
  RPAD(DATA_LENGTH,4)||'#'||
  NULLABLE||'#'
  ,DATA_DEFAULT,'#'||
  RPAD(DATA_PRECISION,2) 
FROM user_tab_columns  
WHERE TABLE_NAME LIKE '___TAB' 
ORDER BY TABLE_NAME,COLUMN_NAME;

EOSQL
}

function get_datamodel
{
   data_model | grep -v "SP2-0310: unable to open file" | grep -v "^$" | grep -v selected |\
      awk 'BEGIN {FS="#"} {printf "%s %s %s %s %s <%s...> %s\n",$1,$2,$3,$4,$5,substr($6,1,20),$7}'
}

# ==============================================================


case "$1" in
   "-?"|"-h"|"-help"|"--h"|"--help")
       doc
       echo  ==============================================================
       echo  ==============================================================
       exit
       ;;
   *)  get_datamodel
       ;;
esac

