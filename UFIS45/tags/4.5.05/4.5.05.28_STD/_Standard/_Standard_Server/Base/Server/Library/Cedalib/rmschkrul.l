%{
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef _DEF_mks_version_rmschkrul_l
  #define _DEF_mks_version_rmschkrul_l
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_rmschkrul_l[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/rmschkrul.l 1.2 2004/01/07 21:29:34SGT jim Exp  $";
#endif /* _DEF_mks_version_rmschkrul_l */

char *rmschkrulstring;

int rmschkrul_yyinput(char *buf, int);

#undef YY_INPUT
#define YY_INPUT(b, r, ms) (r = rmschkrul_yyinput(b, ms))

%}

BEREICH	([0-9a-zA-Z,'])

%%
[0-9]*[.][0-9]+		{sscanf(yytext, "%f", &yylval.real); return REAL;}
[0-9]+			{yylval.real = atof(yytext); return REAL;}
['][a-zA-Z0-9]*[']	{strcpy(yylval.string, yytext); return STRING;}
[a-zA-Z][a-zA-Z0-9]*    {strcpy(yylval.string, yytext); return STRING;}
[<][>]			return UNGLEICH;
[|][|]			return ODER;
[&][&]			return UND;
[><=()+-]		return yytext[0];
[ \t\n]+		;
.			;
%%


int rmschkrulwrap()
{
 /* got from source libyywrap.c */
 return 1;
}



int rmschkrul_yyinput(char *buf, int max_size)
{
 static int nlflag = 0;

 /* ignore max_size - always copy one character */
 if (*rmschkrulstring != 0x00)
 {
  *buf = *rmschkrulstring++;
   return 1;
 }
 else
 {
  if (nlflag == 0)
  {
   *buf = 0x00;
    nlflag = 1;
    return 1;
  }
  else
  {
   nlflag = 0 ;
   return 0 ;
  }
 }
}


