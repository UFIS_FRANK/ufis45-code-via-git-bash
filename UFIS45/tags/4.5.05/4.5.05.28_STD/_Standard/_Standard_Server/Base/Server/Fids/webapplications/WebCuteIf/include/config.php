<?php

/*
Note: Since PHP 5.1.0 (when the date/time functions were rewritten), every call to a date/time function will generate a E_NOTICE if the timezone isn't valid, and/or a E_STRICT message if using the system settings or the TZ environment variable. 
*/

date_default_timezone_set("UTC");

//----------------------- Connection Section ----------------------- 
//Define the Connection String
$DatabaseType="oci8";
$dbusername="ceda";
$dbpassword="ceda";
$database="LISUFIS.BKK1";
$servername="localhost";



$DatabaseTypeMysql="mysqlt";
$dbusernameMysql="root";
$dbpasswordMysql="";
$databaseMysql="cute_fids";
// Oracle Home variable
//putenv("ORACLE_HOME=/app/home/oracle_client/product/10.1.0.2");

//----------------------- Authorization   Section ----------------------- 
// AuthType Please uncoment what you want

//Authedication Type
//$AuthType="Mysql";
//$AuthType="Oracle";
//$AuthType="Variable"; // + File  , Set the Loginhopo
$AuthType="File";
//if AuthType== FILE || AuthType==Variable specifie the directory with the users,passwd file
$UserFilename="db/users.txt";

	
//THE HOPO  of the Airport
$hopo = "LIS";
$Loginhopo = "LIS"; // Must be provided from SITA

//----------------------- Logo   Section ----------------------- 
//Logo Selection 
$logoPath = "/logos/";
$logoRealPath = "/home/ceda/conf/fids/graphics";

$logoExt = "_S.GIF";
$logoSelection = true;
//Possible Values DB , File
$logoSelectionType="File";
$FLZTABuse = false;

//Not Used yet
$LeftLogoFile = "";
//----------------------- FreeText  Remarks Section ----------------------- 
//FreeText 
$FreeTextSelection = true;

//----------------------- Checkin Predifend  Remarks Section ----------------------- 
//Predifend Remarks 
$CheckinPredifendSelection = true;

//Display Both ogo and Remarks if above valus are true 
$DisplayBoth_Logo_Remark = true;



//-----------------------REMT FOR FIDTAB Section ----------------------------------
$PRemarksRemTYPGate='G';  //The REMT value for GATES inside the FIDTAB 
$PRemarksRemTYPCheckin='C';  //The REMT value for CHECKIN inside the FIDTAB

//-----------------------RTYP FOR FLDTAB Section ----------------------------------
$FLDRTYPGate ='GATE';
$FLDRTYPCheckin ='CHECK';



// The Flight will be viewable for x minutes 
// After The user has put the Actual End Time for the Gate
$FlightTimeDiffGates = 5;

//Gate Check Allocation in Minutes (From - To) 
$GateFrom = -60;
$GateTo = 120;

//Checkin (dedicated) Check Allocation in Minutes (From - To) 
$CheckinFrom = -30;
$CheckinTo = 720;

//Checkin (Common) Check Allocation in Minutes (From - To)
$CommonCheckinFrom = -1;   // ********* IN DAYS *************
$CommonCheckinTo = 30;

//Gates Remarks
$RemarkGateOpen ='GTO';
$RemarkGateBoarding ='BDO';
$RemarkGateFinalCall ='LCL';
$RemarkGateClose ='GTC';


//----------------------- Cput   Section ----------------------- 
// The RSH server For the cput Command
// Leave blank ($RSHIP="";) if the cput command is in the local server

//$RSHIP="ufislh";
$RSHIP="";
// The comand tha we want to use in order to execute the remote script
//$RSCMD="/usr/bin/rexec -l username -p passwd ";
$RSCMD="/usr/bin/rsh -n ";

//$CputPath="/ceda/etc/cput45";
//The Cput File
$CputPath="/ceda/etc/kshCput";

// Page Title
$PageTitle="CUTE INTERFACE INTERNATIONAL";

//----------------------- Messages ------------------------------

//$RedGoodByeMsg="PLEASE REMEMBER TO CLOSE THE CHECK-IN FLAP DOORS <br>THANK YOU !";
$RedGoodByeMsg="";

// Cput parameteres
//$wks="\'".$HostName."\'";
$wks="\'EXCO\'";
$cputuser="\'CUTEIF\'";   
$TW_START="\'0\'";
$TW_END="\'$hopo,TAB,CUTEIF\'";



$mailfrom="webmaster";
$APPName="Web CUTE-IF";

$APPVersion="4.5.0.6";
$APPReleaseDate="05/06/2009";




?>
