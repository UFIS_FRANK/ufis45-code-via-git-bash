#ifndef _DEF_mks_version_quedef_h
  #define _DEF_mks_version_quedef_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_quedef_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/quedef.h 1.10 2008/10/28 18:49:33SGT fei Exp  $";
#endif /* _DEF_mks_version */
/* ******************************************************************** */
/*	                    Q u	e d e f. h 				*/
/* This file contains the defines for the UGCCS QCP and for the UGCCS	*/
/* queuing primitive.							*/
/* The message type allocation for the UGCCS modules is also contained	*/
/* in this file.							*/
/*									*/
/* 25.10.1991								*/
/* 20040715: added QUE_SYSDBG:	toggle SYSQCP debug_level 0=> 7=> 8=> 7=> 8...*/
/*                 QUE_SYSTRCOFF:	set  SYSQCP debug_level to OFF              */
/* 20040823: added QUE_DEL_BY_NAME: Delete an existing queue by name needed by*/
/* 20041117 JIM:  added #define 	CMD_FUNC(a) for debugging                   */
/* 20041123 JIM:  added use of fcUnknownFunc in CMD_FUNC to dbg number        */
/* 20050509 JIM:  PRF 7374: added QUE_WMQ_RESP for WMQ Get response with      */
/*                q>30000 to not close WMQ queue on QUE_PUT                   */
/* 20051111 JIM: in linux only define msgbuf when no GNU is set               */
/* ******************************************************************** */

#ifndef QUEUE_DEF			/* Avoid multiple includes */
#define QUEUE_DEF


/* The queue mechanism in the UGCCS package is implemented using the standard
   UNIX V message queue facility. Extra control in provided by the QCP, ex.
   to handle queue full conditions ( using overflow buffers ).

   All processes queues their output on the same queue. A priority scheme
   is implemented by using message types on this queue. A total of four
   message types are used, corresponding to priority 1 to 4. The messages
   on this queue contains a route ID which allows the QCP to forward a
   message to more than one process. The routes can be configured dynamic-
   ly, thereby allowing for changes and extensions on a running system.

   The QCP forward the messages on more than queue, but common for these
   queues is that the receiving process is identified by the message type.
*/

/* We will do the include of the msg specific header files here */

#ifndef NBBY
#include <sys/types.h>
#endif
#ifndef IPC_ALLOC
#include <sys/ipc.h>
#include <sys/shm.h>
#endif
#ifndef PMSG
#include <sys/msg.h>
#endif

/* ******************************************************************** */
/*									*/
/* The following section contains the global definitions for the UGCCS	*/
/* queuing complex							*/
/*									*/
/* ******************************************************************** */

/* The queue item header */

/* 20051111 JIM: in GNU msgbuf already is defined in /usr/include/linux/msg.h
 *               and /usr/include/sys/msg.h :
 * #if defined _LINUX
 */
#if defined _LINUX && !defined _GNU_SOURCE
struct msgbuf
  {
    long int mtype;             /* type of received/sent message */
    char mtext[1];              /* text of the message */
  };
#endif

struct	Item_Header {
	struct msgbuf	msg_header;
	int		function;
	int		route;
	int		priority;
	int		msg_length;
	int		originator;
	int		block_nr;
	char		text[1];
};

typedef struct Item_Header ITEM;
#define ITEM_SIZE	sizeof(ITEM)

/* The Shared Memory header */

struct ShmHeader {
	int	ShmId;
	int	DataLength;
	struct	shmid_ds ShmIdDs;
};

typedef struct ShmHeader SHMADR;
#define	SHMADR_SIZE	sizeof(SHMADR);


/* Both defines are for Software-Control, used in systrt.c and qcp.c */

#define STRLEN	 50	/* Message length		*/
#define TSTR_LEN 8	/* Length of time-string	*/

/* The queuing functions
   These functions are used with the UGCCS queuing primitive defined in
   the file queprim.c
*/
#define Q_ITEM_DATA	100

#define LOW_FUN 1
#define HIG_FUN 255

#define	QUE_PUT			1		/* Adds an item to end of queue */
#define QUE_PUT_FRONT		2		/* Adds an item to front of queue */
#define QUE_GET			3		/* Gets an item from queue */
#define QUE_ACK			4		/* Acks an item ( is removed ) */
#define QUE_CREATE		5		/* Create a new queue */
#define QUE_DELETE		6		/* Delete an existing queue */
#define QUE_EMPTY		7		/* Empty a queue */
#define QUE_GO			8		/* Set queue on go */
#define QUE_HOLD		9		/* Set queue on hold */
#define QUE_WAKE_UP		10		/* Handshake from scheduler */
#define QUE_RESP		11		/* Get response on QUE_CREATE */
#define QUE_QOVER		12		/* Queue overview request */
#define QUE_QDISP		13		/* Queue display request */
#define QUE_ROVER		14		/* Route overview request */
#define QUE_RDISP		15		/* Route display request */
#define QUE_AROUTE		16		/* Add a route */
#define QUE_DROUTE		17		/* Delete a route */
#define QUE_ARENTRY		18		/* Add a route destination entry */
#define QUE_DRENTRY		19		/* Delete a route destination entry */
#define QUE_GETNW		20		/* Gets an item from que (no wait) */
#define QUE_DESTROY		21		/* Destroys a queue */
#define QUE_HSBCMD      	22		/* A SCM Command to QCP itself */
#define	QUE_CHGNAME		23		/* Changes the Name of a Que */
#define	QUE_SHM			24		/* Transfer via Shared Memory */
#define	QUE_O_QOVER		25		/* Get Item Count for one Queue */
#define	QUE_GETBIG		26		/* Get an item of unknown size */
								/* the last parameter of funktion que() */
								/* must be an address of an pointer and */
								/* must be freed manually after usage */
#define	QUE_GETBIGNW		27		/* Get an item of unknown size with nowait */
#define QUE_RESTART             28              /* restart complete mike - shutdown mike now and start mike 1 minute later */
#define QUE_NEXT		29		/* Ready to receive next item */
#define QUE_NEXTNW		30		/* Ready to receive, but will not wait */
#define QUE_SYSDBG		31		/* toggle SYSQCP debug_level 0=> 7=> 8=> 7=> 8...*/
#define QUE_SYSTRCOFF	32		/* set    SYSQCP debug_level to OFF   */
#define QUE_DEL_BY_NAME	33	/* Delete an existing queue by name */
#ifdef WMQ /* 20050509 JIM: */
  #define QUE_WMQ_RESP	34		/* WMQ Get response on QUE_CREATE with q>=30000 */
#else
  #define QUE_WMQ_RESP	11		/* Normal Get response on QUE_CREATE */
#endif
#define QUE_NAK			255		/* A QCP NAK */
#define QUE_SHUTDOWN		256		/* Shutdown (checked in items from attach queue) */
/* whoever adds some QUE_ defines has to add the appropiate string below */


#define 	CMD_FUNC(a) \
  ((a) ==  (QUE_PUT)         ? "QUE_PUT        " : \
  ((a) ==  (QUE_PUT_FRONT)   ? "QUE_PUT_FRONT  " : \
  ((a) ==  (QUE_GET)         ? "QUE_GET        " : \
  ((a) ==  (QUE_ACK)         ? "QUE_ACK        " : \
  ((a) ==  (QUE_CREATE)      ? "QUE_CREATE     " : \
  ((a) ==  (QUE_DELETE)      ? "QUE_DELETE     " : \
  ((a) ==  (QUE_EMPTY)       ? "QUE_EMPTY      " : \
  ((a) ==  (QUE_GO)          ? "QUE_GO         " : \
  ((a) ==  (QUE_HOLD)        ? "QUE_HOLD       " : \
  ((a) ==  (QUE_WAKE_UP)     ? "QUE_WAKE_UP    " : \
  ((a) ==  (QUE_RESP)        ? "QUE_RESP       " : \
  ((a) ==  (QUE_QOVER)       ? "QUE_QOVER      " : \
  ((a) ==  (QUE_QDISP)       ? "QUE_QDISP      " : \
  ((a) ==  (QUE_ROVER)       ? "QUE_ROVER      " : \
  ((a) ==  (QUE_RDISP)       ? "QUE_RDISP      " : \
  ((a) ==  (QUE_AROUTE)      ? "QUE_AROUTE     " : \
  ((a) ==  (QUE_DROUTE)      ? "QUE_DROUTE     " : \
  ((a) ==  (QUE_ARENTRY)     ? "QUE_ARENTRY    " : \
  ((a) ==  (QUE_DRENTRY)     ? "QUE_DRENTRY    " : \
  ((a) ==  (QUE_GETNW)       ? "QUE_GETNW      " : \
  ((a) ==  (QUE_DESTROY)     ? "QUE_DESTROY    " : \
  ((a) ==  (QUE_HSBCMD)      ? "QUE_HSBCMD     " : \
  ((a) ==  (QUE_CHGNAME)     ? "QUE_CHGNAME    " : \
  ((a) ==  (QUE_SHM)         ? "QUE_SHM        " : \
  ((a) ==  (QUE_O_QOVER)     ? "QUE_O_QOVER    " : \
  ((a) ==  (QUE_GETBIG)      ? "QUE_GETBIG     " : \
  ((a) ==  (QUE_GETBIGNW)    ? "QUE_GETBIGNW   " : \
  ((a) ==  (QUE_RESTART)     ? "QUE_RESTART    " : \
  ((a) ==  (QUE_NEXT)        ? "QUE_NEXT       " : \
  ((a) ==  (QUE_NEXTNW)      ? "QUE_NEXTNW     " : \
  ((a) ==  (QUE_SYSDBG)      ? "QUE_SYSDBG     " : \
  ((a) ==  (QUE_SYSTRCOFF)   ? "QUE_SYSTRCOFF  " : \
  ((a) ==  (QUE_DEL_BY_NAME) ? "QUE_DEL_BY_NAME" : \
  ((a) ==  (QUE_NAK)         ? "QUE_NAK        " : \
  ((a) ==  (QUE_WMQ_RESP)    ? "QUE_WMQ_RESP   " : \
  fcUnknownFunc(a) )))))))))))))))))))))))))))))))))))
/* whoever adds some QUE_ defines has to add closing bracket ')' */
/* fcUnknownFunc is defined in debugrec.c because we need string result */

/* The message priorities. These priorities are for input to the QCP */

#define	PRIORITY_1	1
#define	PRIORITY_2	2
#define	PRIORITY_3	3
#define	PRIORITY_4	4
#define	PRIORITY_5	5

/* Error returns from the queuing primitive */

#define QUE_E_FUNC	1		/* Unknown function */
#define QUE_E_MEMORY	2		/* Malloc reports no memory */
#define QUE_E_SEND	3		/* Error using msgsnd */
#define QUE_E_GET	4		/* Error using msgrcv */
#define QUE_E_EXISTS	5		/* Route/Queue exists */
#define QUE_E_NOFIND	6		/* Not found ( ex. route ) */
#define QUE_E_ACKUNEX	7		/* Unexpected ACK received */
#define QUE_E_STATUS	8		/* Unknown queue status */
#define QUE_E_INACTIVE	9		/* Queue is inactive */
#define QUE_E_MISACK	10		/* Missing ACK */
#define QUE_E_NOQUEUES	11		/* The QCP queues do not exist */
#define QUE_E_RESP	12		/* Negative response on CREATE */
#define QUE_E_FULL	13		/* Table full */
#define QUE_E_NOMSG	14		/* No message on queue */
#define QUE_E_INVORG	15		/* Module id by que call == 0 */
#define QUE_E_NOINIT	16		/* Queprim is not initialized */
#define QUE_E_ITOBIG	17		/* requested itemsize to big */
#define QUE_E_BUFSIZ	18		/* Receive buffer too small */
#define QUE_E_PRIORITY	19		/* unknown priority */
#define QUE_E_MSGGET	20		/* msgget returns an error */
#define QUE_E_MSGCTL	21		/* msgctl returns an error */
#define QUE_E_ADDTOQUE	22		/* add_to_que returns with an error */

/* ******************************************************************** */
/*									*/
/* The following section contains the route define for the modules.	*/
/*									*/
/* ******************************************************************** */
/* The Queue Control Program QCP */
#define QCP_PRIMARY     1

/* The system start program */
#define SYS_PRIMARY	2

/* The System Control program */
#define HSB_PRIMARY     3

/* used by dbtsvr */
#define	DBT_PRIMARY	4


/* The message handler */
#define MSG_PRIMARY	1000
#define MSGHDL_PRIMARY	1100

/* The status editor/expander */
#define STA_PRIMARY	1300

/* The time scheduler */
#define TIM_PRIMARY     2300

/* the time scheduler interface routine */
#define TIMIF		19001

/* the paging interface output queue id */
#define PAGEIF		19002

/* the menu interface output queue id */
#define MENUIF		19003

/* the gaib interface output queue id */
#define GRAIF		19004

/* route from systrt to stedit */
#define STEIF		19005

/* Work around !!!! Something 2 do !!!!!! */
#ifndef _LINUX
#define I_SIZE		(1024*16)		/* The QCP input buffer */
#else
#define I_SIZE          8192    /* The QCP input buffer see: <linux/msg.h>*/
#endif

/* Bits for get_que_items */
#define GQI_PRIO1	1
#define GQI_PRIO2	2
#define GQI_PRIO3	4
#define GQI_PRIO4	8
#define GQI_PRIO5	16
#define GQI_PRIO_ALL	31


#ifdef QUE_INTERNAL
/* ******************************************************************** */
/*									*/
/* The following section contains the internal definitions for the QCP	*/
/*									*/
/* ******************************************************************** */

/* The defines for the QCP input queue */

#define QCP_INPUT       (key_t) 44	/* The input queue ID */
/* Changed at 22/02/95 RHU */
#define BLOCK_SIZE	4096		/* The QCP input buffer */

/* The defines for the QCP output queues */

#define QCP_ATTACH	(key_t) 55	/* Queue for attach resp. */
#define QCP_GENERAL	(key_t) 66	/* General output queue */

#define UUTIL		1
#define OTHER		2

/* Defines for loadsharing */

#define QTYPE_NORMAL	0
#define QTYPE_PRIMARY	1
#define QTYPE_SECONDARY	2

/* Reserve the queue/route range for loadsharing (500-599) */

#define LOAD_SH_LOW	500
#define LOAD_SH_HIGH	LOAD_SH_LOW + 99

/* Define for the NEXT protocol (Deferred ACK handling */

#define PROTOCOL_ACK	0	/* Standard ACK protocol */
#define PROTOCOL_NEXT	1	/* NEXT protocol */

/* The Queue Entry */

struct Que_Entry {
	long			time_stamp;
	struct Que_Entry	*prev;
	struct Que_Entry	*next;
	int			len;
	char			data[1];
};

typedef struct Que_Entry Q_ENTRY;

struct Q_Items {
	int			nbr_items;
	Q_ENTRY			*first;
	Q_ENTRY			*last;
};
	
typedef struct Q_Items QITEMS;

/* The queue control block structure */

struct	Que_Control_Block {
	int			que_ID;
	char			name[MAX_PRG_NAME+1];	/* Changed at 22/02/95   */
	int			que_type;	/* Queue type (Loadsharing) */
	int			protocol;	/* Protocol to use ACK or NEXT */
	int			primary_ID;	/* ID of primary queue (Loadsharing) */
	int			status;
	int			retry_cnt;
	long			msg_count;
	long			timestamp;	/* Timestamp last xmit   */
	int			total;		/* total number of items */
	int			ack_count;	/* number of outstanding
						   ACKs			 */
 	short			last_prio;	/* last send priority	 */
	QITEMS			qitem[5];	/* ptr to all items	 */
};

typedef struct Que_Control_Block QCB;

/* The queue table */

struct Que_Table {
	int	nbr_queues;
	int	QTAB_size;
	int	QTAB_free;
	QCB	queue[1];
};

typedef struct Que_Table QTAB;

#define NBR_QUEUES 8		/* No. entries to expand QTAB with */

/* The retry structure */

#define MAX_Q_IDS 250

struct Que_Retry {
	int			nbr_entries;
	int			current;
	int			que_ID[MAX_Q_IDS];
};

typedef struct Que_Retry RETRY;

/* The Route structure */

struct Que_R_Entry {
	int			route_ID;
	int			nbr_entries;
	int			routine_ID[16];
};

typedef struct Que_R_Entry ROUTE;

/* The Route Control Block structure */

struct Route_Control_Block {
	int			nbr_entries;
	int			RCB_size;
	int			RCB_free;
	ROUTE			route[1];
};

typedef struct Route_Control_Block RCB;

#define NBR_ROUTES 8		/* No. of entries to expand RCB with */

/* The functions prototypes.. */

/* extern int		que(int, int, int, int ,int, void *); */

/* Queue control defines */

#define FRONT	0			/* Queue to front of queue */
#define BACK	1			/* Queue to back of queue */

#define INV_ORG	99			/* Originator = 0 */

#define GO	1			/* Queue on go */
#define HOLD	2			/* Queue on hold */
#define F_ACK	4			/* Outstanding ACK */
#define S_ACK	8			/* Outstanding "send"-ACK */
#define DATA	16			/* Outstanding DATA to be sent */
#define NEXTI	32			/* Ready to receive next item */


/* The following defines is used in connection with the QCP util program */

struct	Queue_Info {
	int	id;
	char	name[MAX_PRG_NAME+1];
	int	nbr_items[5];
	int	status;
	long	msg_count;
	int	total;
};

typedef struct Queue_Info QINFO;

struct	Queue_Info_Header {
	int	entries;
	QINFO	data[1];
};

typedef struct Queue_Info_Header QUE_INFO;

struct	Route_Info {
	int	id;
	int	entries;
	int	queues[16];
};

typedef struct Route_Info RINFO;

struct Route_Info_Header {
	int	entries;
	RINFO	data[1];
};

typedef struct Route_Info_Header ROU_INFO;

struct Queue_Send_Data {
	int	que_ID;
	int	prio;
	int	no_item;
	Q_ENTRY	*next;
};

typedef struct Queue_Send_Data QUE_SEND;

struct head {
	QCB	qcb;
	Q_ENTRY	*q_ent;
};

typedef struct head HEAD;

struct	minfos {
	int	len;
	int	prio;
	ITEM	item;
};

typedef	struct	minfos	MINFO;

#endif /* end of internal definitions */

/* Prototypes */

#ifndef WMQ
		int que(int func,int route,int module,int priority,int len,char *msg);
#endif
int init_que(void );
int get_que_items(long req_que, int bits);

#endif /* End of queuedef.h */

/****************************************************************************/
/****************************************************************************/
