#ifndef _DEF_mks_version_chkdb_c
  #define _DEF_mks_version_chkdb_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_chkdb_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Dblib/chkdb.c 1.2 2004/08/10 20:41:17SGT jim Exp  $";
#endif /* _DEF_mks_version */
#include	"chkdb.h"
/*	#include	"db_if.h"	*/

extern	int	debug_level;

int	CheckDataBase(FILE *pFpDbgFile)
{
	int		ilRC;
	int		ili;
	int		ilj;
	int		ilSystabCount;
	int		ilTableCount;
	short		ilFkt;
	char		*pclBuffer[MAX_BUFFER_LEN];
	char		pclErrorText[MAX_BUFFER_LEN];
	char		pSlTabNameBuf[MAX_TAB_COUNT][10];
	char		pclDataArea[100];
	char		pcpBuffer[MAX_BUFFER_LEN];
	char		pclFieldBuf[1000];
	char		pclSqlBuf[1000];
	char		pclFunBuf[100];
	short	slLocCurs;
	short	slActCurs;
	long		llDbVal;

/*	No1:	Try to logg on to DB	*/

	ilRC = init_db();
	if (ilRC != DB_SUCCESS) {
/*	If Error, Check DBUSER and PASSWORD	*/
		sprintf(pclErrorText,"\nError trying to Loggon to Datebase:\nNow Checking Environment-Variables CCSDBUSER and CCSDBPW again\n");
		PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
		ilRC = CheckEnvVariables("CEDADBUSER",FALSE,pclBuffer,NULL);
		memset(pcpBuffer,0x00,MAX_BUFFER_LEN);
		if (ilRC != FAILURE) {
			sprintf(pclErrorText,"\nCCSDBUSER set to '%s'\n",pclBuffer);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
		}	/*	end if	*/
		else {
			sprintf(pclErrorText,"\nEnvironment-Variable CCSDBUSER not set!\n");
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
		}   	/*	end else	*/
		ilRC = CheckEnvVariables("CEDADBPW",FALSE,pclBuffer,NULL);
		memset(pcpBuffer,0x00,MAX_BUFFER_LEN);
		if (ilRC != FAILURE) {
			sprintf(pclErrorText,"\nCCSDBPW set to '%s'\n",pclBuffer);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
		}	/*	end if	*/
		else {
			sprintf(pclErrorText,"\nEnvironment-Variable CCSDBPW not set!\n");
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
		}	/*	end else	*/
	}	/*	Error trying to Loggon to Database	*/
	else {
		sprintf(pclErrorText,"\nLoggon on to Datebase succeeded!\n");
		PrintToSpezifiedFile(pclErrorText,pFpDbgFile);

/*	Loggon succeeded, print DBUSER and PASSWORD	*/

		CheckEnvVariables("CEDADBUSER",FALSE,pclBuffer,NULL);
		sprintf(pclErrorText,"\nCCSDBUSER set to '%s'\n",pclBuffer);
		PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
		CheckEnvVariables("CEDADBPW",FALSE,pclBuffer,NULL);
		sprintf(pclErrorText,"\nCCSDBPW set to '%s'\n",pclBuffer);
		PrintToSpezifiedFile(pclErrorText,pFpDbgFile);

/*	Read Number of records in USER_TABLES	*/

/*		sprintf(pclFieldBuf,"%s%s",USER_FIELD_LIST1,USER_FIELD_LIST2);
		sprintf(pclSqlBuf,"SELECT %s from USER_TABLES;",pclFieldBuf);
		sprintf(pclErrorText,"S<%s>\n",pclSqlBuf);
		PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
		ilRC = sql_if(START|REL_CURSOR,&slLocCurs,pclSqlBuf,pclDataArea);
		check_ret(ilRC);
		commit_work();
		if ( ilRC == DB_SUCCESS ) {
			memset(pclFunBuf,0x00,100);
			get_fld(pclDataArea,FIELD_1,RAW,30,pclFunBuf);
			sprintf(pclErrorText,"TABLE_NAME is '%s'\n",pclBuffer);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			memset(pclFunBuf,0x00,100);
			get_fld(pclDataArea,FIELD_2,RAW,30,pclFunBuf);
			sprintf(pclErrorText,"TABLESPACE_NAME is '%s'\n",pclBuffer);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			memset(pclFunBuf,0x00,100);
			get_fld(pclDataArea,FIELD_3,RAW,30,pclFunBuf);
			sprintf(pclErrorText,"CLUSTER_NAME is '%s'\n",pclBuffer);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			memset(pclFunBuf,0x00,100);
			get_fld(pclDataArea,FIELD_4,LONG,IGNORE,(char *) &llDbVal);
			sprintf(pclErrorText,"PCT_FREE is '%ld'\n",llDbVal);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			memset(&llDbVal,0x00,sizeof(long));
			get_fld(pclDataArea,FIELD_5,LONG,IGNORE,(char *) &llDbVal);
			sprintf(pclErrorText,"PCT_USED is '%ld'\n",llDbVal);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			memset(&llDbVal,0x00,sizeof(long));
			get_fld(pclDataArea,FIELD_6,LONG,IGNORE,(char *) &llDbVal);
			sprintf(pclErrorText,"INI_TRANS is '%ld'\n",llDbVal);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			memset(&llDbVal,0x00,sizeof(long));
			get_fld(pclDataArea,FIELD_7,LONG,IGNORE,(char *) &llDbVal);
			sprintf(pclErrorText,"MAX_TRANS is '%ld'\n",llDbVal);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			memset(&llDbVal,0x00,sizeof(long));
			get_fld(pclDataArea,FIELD_8,LONG,IGNORE,(char *) &llDbVal);
			sprintf(pclErrorText,"INITIAL_EXTENT is '%ld'\n",llDbVal);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			memset(&llDbVal,0x00,sizeof(long));
			get_fld(pclDataArea,FIELD_9,LONG,IGNORE,(char *) &llDbVal);
			sprintf(pclErrorText,"NEXT_EXTENT is '%ld'\n",llDbVal);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			memset(&llDbVal,0x00,sizeof(long));
			get_fld(pclDataArea,FIELD_10,LONG,IGNORE,(char *) &llDbVal);
			sprintf(pclErrorText,"min_EXTENTS is '%ld'\n",llDbVal);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			memset(&llDbVal,0x00,sizeof(long));
			get_fld(pclDataArea,FIELD_11,LONG,IGNORE,(char *) &llDbVal);
			sprintf(pclErrorText,"MAX_EXTENTS is '%ld'\n",llDbVal);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			memset(&llDbVal,0x00,sizeof(long));
			get_fld(pclDataArea,FIELD_12,LONG,IGNORE,(char *) &llDbVal);
			sprintf(pclErrorText,"PCT_INCREASE is '%ld'\n",llDbVal);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			memset(&llDbVal,0x00,sizeof(long));
			get_fld(pclDataArea,FIELD_13,RAW,1,pclFunBuf);
			sprintf(pclErrorText,"BACKED_UP is '%s'\n",pclBuffer);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			memset(pclFunBuf,0x00,100);
			get_fld(pclDataArea,FIELD_14,LONG,IGNORE,(char *) &llDbVal);
			sprintf(pclErrorText,"NUM_ROWS is '%ld'\n",llDbVal);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			memset(&llDbVal,0x00,sizeof(long));
			get_fld(pclDataArea,FIELD_15,LONG,IGNORE,(char *) &llDbVal);
			sprintf(pclErrorText,"BLOCKS is '%ld'\n",llDbVal);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			memset(&llDbVal,0x00,sizeof(long));
			get_fld(pclDataArea,FIELD_16,LONG,IGNORE,(char *) &llDbVal);
			sprintf(pclErrorText,"EMPTY_BLOCKS is '%ld'\n",llDbVal);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			memset(&llDbVal,0x00,sizeof(long));
			get_fld(pclDataArea,FIELD_17,LONG,IGNORE,(char *) &llDbVal);
			sprintf(pclErrorText,"AVG_SPACE is '%ld'\n",llDbVal);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			memset(&llDbVal,0x00,sizeof(long));
			get_fld(pclDataArea,FIELD_18,LONG,IGNORE,(char *) &llDbVal);
			sprintf(pclErrorText,"CHAIN_COUNT is '%ld'\n",llDbVal);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			memset(&llDbVal,0x00,sizeof(long));
			get_fld(pclDataArea,FIELD_19,LONG,IGNORE,(char *) &llDbVal);
			sprintf(pclErrorText,"AVG_ROW_LEN is '%ld'\n",llDbVal);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			memset(&llDbVal,0x00,sizeof(long));
			get_fld(pclDataArea,FIELD_20,RAW,10,pclFunBuf);
			sprintf(pclErrorText,"INSTANCES is '%s'\n",pclBuffer);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			memset(pclFunBuf,0x00,100);
			get_fld(pclDataArea,FIELD_21,RAW,10,pclFunBuf);
			sprintf(pclErrorText,"CACHE is '%s'\n",pclBuffer);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			memset(pclFunBuf,0x00,100);
			get_fld(pclDataArea,FIELD_22,RAW,5,pclFunBuf);
			sprintf(pclErrorText,"TABLE_NAME is '%s'\n",pclBuffer);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			memset(pclFunBuf,0x00,100);
			close_my_cursor(&slLocCurs);
		}                                       
		else {
			sprintf(pclErrorText,"Reading the USER_TABLES FAILED\n");
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
		}
*/		
/*	Read Number of records in SYSTAB	*/

/*		sprintf(pclSqlBuf,"SELECT COUNT (*) from SYSTAB;");	*/
		sprintf(pclSqlBuf,"SELECT COUNT (*) from TABTAB;");
		ilRC = sql_if(START|REL_CURSOR,&slLocCurs,pclSqlBuf,pclDataArea);
		check_ret(ilRC);
		commit_work();
		if ( ilRC == DB_SUCCESS ) {                       
			get_fld(pclDataArea,FIELD_1,STR,IGNORE,pclFunBuf);
			ilSystabCount = atoi(pclFunBuf);
			sprintf(pclErrorText,"\nReading System-Table SYSTAB worked alright\nSYSTAB holds %d Entries\n",ilSystabCount);
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			if (ilSystabCount > 0) {

/*	Number of Entries in SYSTAB is bigger than 0	*/

				sprintf(pclErrorText,"\nNow checking SYSTAB's Entries\n");
				PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
/*				sprintf(pclSqlBuf,"SELECT tana from SYSTAB;");	*/
				sprintf(pclSqlBuf,"SELECT DISTINCT tana from TABTAB;");

/*	Get Entries of SYSTAB	*/

				slLocCurs = 0;
				for (ili = 0,ilFkt = START; (ili < ilSystabCount) && (ilRC == DB_SUCCESS); ili++) {
					memset(pclDataArea,0x00,100);
					ilRC = sql_if(ilFkt,&slLocCurs,pclSqlBuf,pclDataArea);
					check_ret(ilRC);
					if ( ilRC == DB_SUCCESS ) {
						get_fld(pclDataArea,FIELD_1,STR,IGNORE,pSlTabNameBuf[ili]);
if (debug_level == DEBUG) {
sprintf(pclErrorText,"\nB<%s> D<%s>\n",pSlTabNameBuf[ili],pclDataArea);
PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
}
					}	/*	end if	*/
					ilFkt = NEXT;
				}	/*	end for	*/
				close_my_cursor(&slLocCurs);
				commit_work();

/*	Print Entries of SYSTAB and Filling of Tables	*/

				slActCurs = 0;
				for (ilj = 0; (ilj < (ili - 1)) && (ilj < ilSystabCount); ilj++) {
					slActCurs = 0;
					memset(pclSqlBuf,0x00,100);
					sprintf(pclSqlBuf,"SELECT COUNT (*) from %.10s;",pSlTabNameBuf[ilj]);
					memset(pclDataArea,0x00,100);
if (debug_level == DEBUG) {
sprintf(pclErrorText,"\nS<%s> D<%s>\n",pclSqlBuf,pclDataArea);
PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
}
					ilRC = sql_if(START,&slActCurs,pclSqlBuf,pclDataArea);
					check_ret(ilRC);
					if ( ilRC == DB_SUCCESS ) {
						get_fld(pclDataArea,FIELD_1,STR,IGNORE,pclFunBuf);
						ilTableCount = atoi(pclFunBuf);
						sprintf(pclErrorText,"\nTable %s exists, holding %d Entries\n",pSlTabNameBuf[ilj],ilTableCount);
						PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
					}	/*	end if	*/
					else {
						sprintf(pclErrorText,"\nTable %s doesn't exist! (rc = %d) \n",pSlTabNameBuf[ilj],ilRC);
						PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
					}	/*	end else	*/
					close_my_cursor(&slActCurs);
				}	/*	end for	*/
				close_my_cursor(&slActCurs);
				commit_work();
			}/* end if */
			else {
				sprintf(pclErrorText,"\nBut SYSTAB is emty\n");
				PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
			}	/*	end else	*/
		}	/*	end if	*/
		else {
			sprintf(pclErrorText,"\nReading System-Table SYSTAB failed!\nCannot check other Tables\n");
			PrintToSpezifiedFile(pclErrorText,pFpDbgFile);
		}	/*	end else	*/
	}	/*	end else	*/
	logoff();
	return ilRC;
}	/*	end of CheckDataBase()	*/


