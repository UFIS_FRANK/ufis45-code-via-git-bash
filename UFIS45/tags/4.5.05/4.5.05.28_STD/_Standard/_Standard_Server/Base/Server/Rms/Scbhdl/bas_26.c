/*********************/
/* 26: working hours */
/*********************/
static void bas_26()
{
	char 	clLastCommand[4];
	char	clStaffUrno[12];
	char	clSday[16];
	char	clScoCode[10];
	char	clDrrBsduUrno[12];
	char	clDrrShiftCode[12];
	char	clDrrStartTime[16];
	char	clDrrEndTime[16];
	char	clDrrStatus[2];
	char	clDrrBreakTime[16];
	char	clNewCode1[2];
	char	clNewCode3[2];
	char	clNewCode4[2];
	double	dlDrrMin;
	char	clMaName[128];
	char	clVereinbarteWochenstunden[128];
	char	clOldDrrBreakTime[16];
	char	clOldDrrStartTime[16];
	char	clOldDrrEndTime[16];
	char	clOldDrrShiftCode[12];
	char	clOldDrrBsduUrno[12];
	char	clOldCode1[2];
	char	clOldCode3[2];
	char	clOldCode4[2];
	char	clOldDrrStatus[2];
	char	clYear[6];
	char	clMonth[4];
	char	clOldAccValue[128];
	double	dlOldValue;
	double	dlNewValue;
	double	dlResultValue;
	double	dlOTValue;
	double	dlNewAccValue;

	char	clIsTraining[2];
	char	clIsRegFree[2];
	char	clIsSleepDay[2];
	char	clIsBsd[2];

	dbg(DEBUG, "============================= ACCOUNT 26 START ===========================");

	/* checking the data */
	if (strlen(pcgData) == NULL)
	{
		dbg(TRACE,"Account 26: Stopped, because <pcgData> = undef!");
		return;
	}
	dbg(DEBUG,"Account 26: pcgData = <%s>",pcgData);

	/* checking the fields */
	if (strlen(pcgFields) == 0)
	{
		dbg(TRACE,"Account 26: Stopped, because <pcgFields> = undef!");
		return;
	}
	dbg(DEBUG, "Account 26: pcgFields = <%s>",pcgFields);


	/* checking the URNO of the employee */
	if (ReadTemporaryField("StfUrno",clStaffUrno) == 0)
	{
		dbg(TRACE,"Account 26: Stopped, because <clStaffUrno> = undef!");
		return;
	}

	/* checking the SDAY */
	if (ReadTemporaryField("SDAY",clSday) == 0)
	{
		dbg(TRACE,"Account 26: Stopped, because <clSday> = undef!");
		return;
	}

	/* BSDU of new DRR-record */
	if (ReadTemporaryField("NewBsdu",clDrrBsduUrno) == 0)
	{
		dbg(TRACE,"Account 26: <clDrrBsduUrno> = undef!");
		strcpy(clDrrBsduUrno,"0");
	}

	/* SCOD of new DRR-record */
	if (ReadTemporaryField("NewScod",clDrrShiftCode) < 0)
	{
		dbg(TRACE, "Account 26: <clDrrShiftCode> = undef!");
		clDrrShiftCode[0] = '\0';
	}

	/* start time */
	if (ReadTemporaryField("NewAvfr",clDrrStartTime) <= 0)
	{
		dbg(TRACE, "Account 26: <clDrrStartTime> = undef!");
		strcpy(clDrrStartTime,"0");
	}

	/* end time */
	if (ReadTemporaryField("NewAvto",clDrrEndTime) <= 0)
	{
		dbg(TRACE, "Account 26: <clDrrEndTime> = undef!");
		strcpy(clDrrEndTime,"0");
	}

	/* status */
	if (ReadTemporaryField("NewRoss",clDrrStatus) < 0)
	{
		dbg(TRACE, "Account 26: <clDrrStatus> = undef!");
		clDrrStatus[0] = '\0';
	}

	/* break duration */
	if (ReadTemporaryField("NewSblu",clDrrBreakTime) <= 0)
	{
		dbg(TRACE, "Account 26: <clDrrBreakTime> = undef!");
		strcpy(clDrrBreakTime,"0");
	}

	/* DRS values */
	GetFieldValueFromMonoBlockedDataString("DRS1",pcgFields,pcgData,clNewCode1);
	GetFieldValueFromMonoBlockedDataString("DRS3",pcgFields,pcgData,clNewCode3);
	GetFieldValueFromMonoBlockedDataString("DRS4",pcgFields,pcgData,clNewCode4);

	/* checking command (IRT,URT,DRT) */
	strncpy(clLastCommand,GetLastCommand(),3);
	clLastCommand[3] = '\0';
	dbg(DEBUG,"Account 26: Command (%s)!",clLastCommand);

	/* initializing variables */
	dlDrrMin	= 0;
	clMaName[0]	= '\0';
	clScoCode[0]	= '\0';

	/* prepare calculations */
	ReadTemporaryField("MaName",clMaName);
	ReadTemporaryField("ScoCode",clScoCode);
	ReadTemporaryField("VereinbarteWochenstunden",clVereinbarteWochenstunden);

	dbg(DEBUG, "Account 26: Name of employee <%s>",clMaName);
	dbg(DEBUG, "Account 26: Contract code <%s>",clScoCode);
	dbg(DEBUG, "Account 26: Agreed weekly working hours: (per day in min) <%s>",clVereinbarteWochenstunden);

	dlOldValue = 0;
	/* *** Nur wenn ein Update kommt den alten Wert berechnen	****/
	if (strcmp(clLastCommand,"URT") == 0)
	{
		/* checking old data */
		if (strlen(pcgOldData) == NULL)
		{
			dbg(TRACE,"Account 26: <pcgOldData> = undef!");
		}

		/* old start time */
		if (ReadTemporaryField("OldAvfr",clOldDrrStartTime) <= 0)
		{
			dbg(TRACE, "Account 26: <clOldDrrStartTime> = undef!");
			strcpy(clOldDrrStartTime,"0");
		}
	
		/* old end time */
		if (ReadTemporaryField("OldAvto",clOldDrrEndTime) <= 0)
		{
			dbg(TRACE, "Account 26: <clOldDrrEndTime> = undef!");
			strcpy(clOldDrrEndTime,"0");
		}
	
		/* old status */
		if (ReadTemporaryField("OldRoss",clOldDrrStatus) < 0)
		{
			dbg(TRACE, "Account 26: <clOldDrrStatus> = undef!");
			clOldDrrStatus[0] = '\0';
		}

		/* old break duration */
		if (ReadTemporaryField("OldSblu",clOldDrrBreakTime) <= 0)
		{
			strcpy(clOldDrrBreakTime,"0");
		}

		/* ****** SchichtCode des alten DRR Datensatzen		*****/
		if (ReadTemporaryField("OldScod",clOldDrrShiftCode) < 0)
		{
			dbg(TRACE, "Account 26: <OldDrrShiftCode> = undef!!!\n");
			strcpy(clOldDrrShiftCode,"0");
		}

		/* BSDU of old DRR-record */
		if (ReadTemporaryField("OldBsdu",clOldDrrBsduUrno) == 0)
		{
			dbg(TRACE,"Account 26: <clOldDrrBsduUrno> = undef!");
			strcpy(clOldDrrBsduUrno,"0");
		}

		/* old DRS values */
		GetFieldValueFromMonoBlockedDataString("DRS1",pcgFields,pcgOldData,clOldCode1);
		GetFieldValueFromMonoBlockedDataString("DRS3",pcgFields,pcgOldData,clOldCode3);
		GetFieldValueFromMonoBlockedDataString("DRS4",pcgFields,pcgOldData,clOldCode4);

		if (strcmp(clOldDrrBsduUrno,"0") != 0)
		{
			/* use only active DRR-records */
			if (strcmp(clOldDrrStatus,"A") == 0)
			{
				ReadTemporaryField("OldIsTraining",clIsTraining);
				ReadTemporaryField("OldIsRegFree",clIsRegFree);
				ReadTemporaryField("OldIsShiftChangeoverDay",clIsSleepDay);
				ReadTemporaryField("OldIsBsd",clIsBsd);

				if (GetValue26(clOldDrrBsduUrno,clOldDrrBreakTime,clOldDrrStartTime,clOldDrrEndTime,clOldDrrShiftCode,clSday,clVereinbarteWochenstunden,clScoCode,&dlDrrMin,clIsRegFree,clIsSleepDay,clIsTraining,clIsBsd) == 1) 
				{
					dlOldValue = dlDrrMin / 60;

					/* calculate OT */
					dlOTValue = GetDrsValue(clOldDrrBreakTime,clOldDrrStartTime,clOldDrrEndTime,clOldDrrBsduUrno,clOldCode1,clOldCode3,clOldCode4,clSday,clIsTraining);

					dbg(DEBUG, "Account 26: Old ShiftCode        <%s>" ,clOldDrrShiftCode);
					dbg(DEBUG, "Account 26: Old ShiftUrno        <%s>" ,clOldDrrBsduUrno);
					dbg(DEBUG, "Account 26: Old StartTime        <%s>" ,clOldDrrStartTime);
					dbg(DEBUG, "Account 26: Old EndTime          <%s>" ,clOldDrrEndTime);
					dbg(DEBUG, "Account 26: Old BreakTime        <%s>" ,clOldDrrBreakTime);
					dbg(DEBUG, "Account 26: Old DRS-values       <Drs1:%s - Drs3:%s - Drs4:%s>",clOldCode1,clOldCode3,clOldCode4);
					dbg(DEBUG, "Account 26: Old Value without OT <%lf>" , dlOldValue);
					dbg(DEBUG, "Account 26: Old OT �             <%lf>" , dlOTValue);

					dlOldValue = dlOldValue - dlOTValue;
				}
				dbg(DEBUG, "Account 26: Old value with OT    <%lf>" , dlOldValue);
			}
			else
			{
				dbg(TRACE, "Account 26: Wrong status of old DRR-record: <clOldDrrStatus> = <%s>" ,clOldDrrStatus);
			}
		}
	}

	/* calculate new value */
	dlNewValue = 0;

	/* use only active DRR-records */
	if (strcmp(clDrrStatus,"A") == 0 && strcmp(clDrrBsduUrno,"0") != 0)
	{
		ReadTemporaryField("NewIsTraining",clIsTraining);
		ReadTemporaryField("NewIsRegFree",clIsRegFree);
		ReadTemporaryField("NewIsShiftChangeoverDay",clIsSleepDay);
		ReadTemporaryField("NewIsBsd",clIsBsd);

		if (GetValue26(clDrrBsduUrno,clDrrBreakTime,clDrrStartTime,clDrrEndTime,clDrrShiftCode,clSday,clVereinbarteWochenstunden,clScoCode,&dlDrrMin,clIsRegFree,clIsSleepDay,clIsTraining,clIsBsd) == 1)
		{
			dlNewValue = dlDrrMin / 60;

			/* calculate OT */
			dlOTValue = GetDrsValue(clDrrBreakTime,clDrrStartTime,clDrrEndTime,clDrrBsduUrno,clNewCode1,clNewCode3,clNewCode4,clSday,clIsTraining);

			dbg(DEBUG, "Account 26: New ShiftCode        <%s>" , clDrrShiftCode);
			dbg(DEBUG, "Account 26: New ShiftUrno        <%s>" , clDrrBsduUrno);
			dbg(DEBUG, "Account 26: New BreakTime)       <%s>" , clDrrBreakTime);
			dbg(DEBUG, "Account 26: New StartTime)       <%s>" , clDrrStartTime);
			dbg(DEBUG, "Account 26: New EndTime)         <%s>" , clDrrEndTime);
			dbg(DEBUG, "Account 26: New DRS-values       <Drs1:%s - Drs3:%s - Drs4:%s>",clNewCode1,clNewCode3,clNewCode4);
			dbg(DEBUG, "Account 26: New Value without OT <%lf>" , dlNewValue);
			dbg(DEBUG, "Account 26: New OT               <%lf>" , dlOTValue);

			dlNewValue = dlNewValue - dlOTValue;
		}
		dbg(DEBUG, "Account 26: New value with OT    <%lf>" , dlNewValue);
	}
	else
	{
		dbg(TRACE, "Account 26: Wrong status of new DRR-record: <clDrrStatus> = <%s>" ,clDrrStatus);
	}

	/* calculate difference */
	if (strcmp(clLastCommand,"DRT") != 0)
	{
		dlResultValue = dlNewValue - dlOldValue;
	}
	else
	{
		dlResultValue = - dlNewValue; 
	}

	dbg(DEBUG, "Account 26: Result value = <%lf>" , dlResultValue);

	/* save record */
	clMonth[0] = clSday[4];
	clMonth[1] = clSday[5];
	clMonth[2] = '\0';

	strncpy(clYear,clSday,4);
	clYear[4] = '\0';

	GetAccValueHandle("26",clMonth,clYear,clStaffUrno,"CL",clOldAccValue);

	dbg(DEBUG, "Account 26: Old ACC value: <%s>", clOldAccValue);

	dlNewAccValue = atof(clOldAccValue) + dlResultValue;

	dbg(DEBUG, "Account 26: New ACC value: <%lf>", dlNewAccValue);

	if (fabs(dlResultValue) > 1.0e-5)
	{
		UpdateAccountHandle(dlNewAccValue,"26",clMonth,clYear,clStaffUrno,"SCBHDL");

		SetNextScript("/ceda/bas/51.bbf");
		dbg(DEBUG, "Account 26: next account to be processed: account 51");
	}
	else
	{
		dbg(TRACE, "Account 26: Value not changed: <%s>", clOldAccValue);
	}
}

