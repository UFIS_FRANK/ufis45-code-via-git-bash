#ifndef _DEF_mks_version_actlog_c
  #define _DEF_mks_version_actlog_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_actlog_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/actlog.c 1.2 2004/08/10 20:10:31SGT jim Exp  $";
#endif /* _DEF_mks_version */
static   char  sccs_actlog_lib[] = "@(#) UFIS 4.4 (c) ABB AAT/I actlog.c  44.1 / 00/01/06 11:19:20 / JMU";

/*	**********************************************************************	*/
/*																*/
/*	CCS libary routine WriteActionLog()								*/
/*																*/
/*	Author:			JM�											*/
/*	Date:			22.01.1998									*/
/*	Description:		Libary-routine to send Log-Information to loghdl and	*/
/*					to Action Schedular								*/
/*																*/
/*	Update history:												*/
/*																*/
/*	**********************************************************************	*/

#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>

/*	#include	<sys/types.h>	*/
#include	<time.h>
#include	<sys/timeb.h>

#include	"ugccsma.h"
#include	"msgno.h"
#include	"glbdef.h"
#include	"quedef.h"
#include	"uevent.h"
#include	"loghdl.h"
#include	"action_tools.h"

#define	MAX_FUNC_NO	7
#ifndef	LOG_PRIMARY
#define	LOG_PRIMARY	7700
#endif

#ifdef ODT5
extern	void dbg(int,char *, ...);
#endif
extern	int	WriteLogRecord(int,char *,int,char *,char *,char *,char *,char *,char *);

static	int				iGTransMode;	/*	Neu 19.01.1998 JM�	*/
static	struct _Actions	*pSlActionHead;
extern	int				mod_id;
extern	int				debug_level;

		int	WriteActionLog(int,char *,int,char *,char *,char *,char *,char *,char *,char *,short *,char *);
static	int	CheckProtocol(int ipParMode);
static	int	CheckWriteLog(short *pspRouList);


int	WriteActionLog(int ipMode,char *pcpCom,int ipRou,char *pcpLead,char *pcpUser,char *pcpTab,char *pcpOrno,char *pcpFields,char *pcpValues,char *pcpSel,short *pspRouList,char *pcpErrBuf)
{
	int		ilRC = RC_SUCCESS,
			ilRC1 = RC_SUCCESS,
			ilRC2 = RC_SUCCESS;
	struct _ActionData	SlActionData;

/*	Pr�fe, ob Protokoll eingehalten ist und aktualisiere Flag ggfs.	*/

	ilRC = CheckProtocol(ipMode);
	if (ilRC == -1) {
		return LOG_PROTOCOL;
	}	/*	end if	*/
	else {
		iGTransMode = ilRC;
	}	/*	end else	*/
	
/*	Pr�fe, ob Daten auch an loghdl zu senden sind	*/

	ilRC = CheckWriteLog(pspRouList);
dbg(DEBUG,"CheckWriteLog: Return with %d",ilRC);
	if (ilRC == RC_SUCCESS ) {

/*	Ist dies der Fall, rufe WriteLogRecord() auf	*/

dbg(DEBUG,"WriteActionLog: Now go to WriteLogRecord with data");
		ilRC1 = WriteLogRecord(ipMode,pcpCom,ipRou,pcpLead,pcpUser,pcpTab,pcpOrno,pcpFields,pcpValues);
dbg(DEBUG,"WriteActionLog: WriteLogRecord returns %d",ilRC1);
	}	/*	end if	*/                                                       
	else if ( (ipMode == TRANS_BEGIN) || (ipMode == TRANS_ROLLBACK) || (ipMode == TRANS_COMMIT) ) {
dbg(DEBUG,"WriteActionLog: Now go to WriteLogRecord with COMMIT or ROLLBACK or BEGIN");
		ilRC1 = WriteLogRecord(ipMode,pcpCom,ipRou,pcpLead,pcpUser,pcpTab,pcpOrno,pcpFields,pcpValues);
dbg(DEBUG,"WriteActionLog: WriteLogRecord returns %d",ilRC1);
	}	/*	end if	*/

/*	**********************************************************************	*/

	if ( (pspRouList[0] == 1) && (pspRouList[1] == 7700) && (ipMode == TRANS_MEMBER) ) {
		return (ilRC1 + ilRC2);
	}	/*	end if	*/
	else {
		switch (ipMode) {
			case INIT_TRANSACTION_HEAD	:
			case TRANS_BEGIN	:
				pSlActionHead = NULL;
				SlActionData.iOriginID = 0;
				memset(SlActionData.iRouteID,0x00,iMIN_BUF_SIZE);
				memset(SlActionData.pcCommand,0x00,6);
				memset(SlActionData.pcObjName,0x00,33);
				SlActionData.pcSelection = NULL;
				SlActionData.pcFields = NULL;
				SlActionData.pcData = NULL;
				break;
			case	TRANS_MEMBER	:
				if (pcpFields == NULL)
					SlActionData.pcFields = " ";
				else 
					SlActionData.pcFields = pcpFields;
				if (pcpValues == NULL)
					SlActionData.pcData = " ";
				else 
					SlActionData.pcData = pcpValues;
				SlActionData.iOriginID = ipRou;
				if ( ((pspRouList[0] + 1) * sizeof(short)) < iMIN_BUF_SIZE )
					memcpy(SlActionData.iRouteID,pspRouList,((pspRouList[0] + 1) * sizeof(short)));
				else
					memcpy(SlActionData.iRouteID,pspRouList,iMIN_BUF_SIZE);
				strncpy(SlActionData.pcCommand,pcpCom,6);
				strncpy(SlActionData.pcObjName,pcpTab,33);
				SlActionData.pcSelection = pcpSel;
				pSlActionHead = AddListItem(pSlActionHead,NULL,&SlActionData);
				if (pSlActionHead == NULL)
					ilRC2 = 10;
				SlActionData.iOriginID = 0;
				memset(SlActionData.iRouteID,0x00,iMIN_BUF_SIZE);
				memset(SlActionData.pcCommand,0x00,6);
				memset(SlActionData.pcObjName,0x00,33);
				SlActionData.pcSelection = NULL;
				SlActionData.pcFields = NULL;
				SlActionData.pcData = NULL;
				break;
			case	TRANS_COMMIT	:
				pSlActionHead = CommitActions(pSlActionHead);
				if (pSlActionHead != NULL) {
					ilRC2 = 10;
					pcpErrBuf = (char *) pSlActionHead;
				}	/*	end if	*/
				break;
			case	TRANS_ROLLBACK	:
				pSlActionHead = RollbackActions(pSlActionHead);
				if (pSlActionHead != NULL) {
					ilRC2 = 10;
					pcpErrBuf = (char *) pSlActionHead;
				}	/*	end if	*/
				break;
			default	:
				ilRC2 = 10;
				break;
		}	/*	end switch	*/
	}	/*	end else	*/

/*	Hierhin mu� die Verarbeitung von Action-Items						*/

/*	**********************************************************************	*/
			
/*	ilRC1 enth�lt Return-Code f�r WriteLogRecord(), ilRC2 den f�r Action-Items	*/

	return (ilRC1 + ilRC2);

}	/*	end of WriteLogRecord()	*/

static	int	CheckProtocol(int ipParMode)
{
	int		ilRC = -1;

dbg(DEBUG,"CheckProtocol: Inp-Mode = %d, Glob-Mode = %d",ipParMode,iGTransMode);
	switch (ipParMode) {
		case INIT_TRANSACTION_HEAD	:
			ilRC = TRANS_BEGIN;
			break;
		case TRANS_BEGIN	:
			if (iGTransMode == TRANS_BEGIN) 
				ilRC = TRANS_MEMBER;
			break;
		case TRANS_MEMBER	:
			if (iGTransMode == TRANS_MEMBER)
				ilRC = TRANS_MEMBER;
			break;
		case TRANS_COMMIT	:
		case TRANS_ROLLBACK	:
			if (iGTransMode == TRANS_MEMBER)
				ilRC = TRANS_BEGIN;
			break;
		case NO_TRANS	:
			if ( (iGTransMode == INIT_TRANSACTION_HEAD) || (iGTransMode == TRANS_BEGIN) )
				ilRC = TRANS_BEGIN;
			break;
		default	:
			break;
	}	/*	end if	*/
dbg(DEBUG,"CheckProtocol:returns %d",ilRC);
	return ilRC;
}	/*	end of CheckProtocol()	*/

static	int	CheckWriteLog(short *pspRouList)
{
	int		ilRC = RC_FAIL,
			ilFound = FALSE,
			ili;
	short	slVal;
	short	slVal1;

dbg(DEBUG,"CheckWriteLog: Checking ...");
	slVal = pspRouList[0];
	slVal1 = (short) *pspRouList;
dbg(DEBUG,"CheckWriteLog: Number = %d (%d/%d)",slVal,pspRouList[0],slVal1);
	for (ili = 1; ili <= slVal; ili++) {
dbg(DEBUG,"CheckWriteLog: Checking %d and %d (LOG_PRIMARY)",pspRouList[ili],LOG_PRIMARY);
		if (LOG_PRIMARY == pspRouList[ili]) {
			ilFound = TRUE;
			break;
		}	/*	end if	*/
	}	/*	end for	*/
	if (ilFound == TRUE) {
		if (ili < slVal) {
			pspRouList[ili] = pspRouList[slVal];
			pspRouList[slVal] = 0;
			pspRouList[0] = pspRouList[0] - 1;
		}	/*	end if	*/
		ilRC = RC_SUCCESS;
	}	/*	end if	*/
dbg(DEBUG,"CheckWriteLog: Checking returns %d",ilRC);
	return ilRC;
}	/*	end of CheckWriteLog()	*/

