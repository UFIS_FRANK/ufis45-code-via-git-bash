#ifndef _DEF_mks_version_ftptools_h
  #define _DEF_mks_version_ftptools_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_ftptools_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/ftptools.h 1.2 2004/07/27 16:47:46SGT jim Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* CEDA Program Skeleton                                                      */
/*                                                                            */
/* Author         : RBI                                                       */
/* Date           : 98/11/12                                                  */
/* Description    : FTP-Library-Functions header file                         */
/*						  Containing all necessary prototypes and structures        */
/*                                                                            */
/* Update history : 98/11/12 Start                                            */
/*                                                                            */
/******************************************************************************/

#ifndef __FTPTOOLS_H_
#define __FTPTOOLS_H_

/* This program is not a CEDA main program */

/* global includes */
#include <netdb.h>
#include <signal.h>
#include <unistd.h>

#if defined(_HPUX_SOURCE) || defined(_UNIXWARE) || defined(_SNI) || defined(_SOLARIS) || defined(_WINNT) || defined(_LINUX)
		#include <netinet/in.h>
#else
	/* everything for SCO Openserver */
	#include <sys/netinet/in.h>
#endif

/* defines */
#define CEDA_ASCII						'A'
#define CEDA_BINARY						'I'
#define CEDA_EBCDIC						'E'
#define CEDA_BYTE						'L'

#define CEDA_FILE						'F'
#define CEDA_RECORD						'R'
#define CEDA_PAGE						'P'

#define CEDA_STREAM						'S'
#define CEDA_BLOCK						'B'
#define CEDA_COMPRESSED					'C'

#define CEDA_APPEND						0
#define CEDA_CREATE						1

#define OS_UNIX							0
#define OS_WIN							1


/* each define represents a library function */
#define CEDA_CONNECT	    		0
#define CEDA_LOGIN_USER				1
#define CEDA_LOGIN_PASS				2
#define CEDA_SETTYPE				3
#define CEDA_BYE					4
#define CEDA_CD						5
#define CEDA_SET_FILE_STRUCTURE		6
#define CEDA_SET_TRANSFER_MODE		7
#define CEDA_RM						8
#define CEDA_RMDIR					9
#define CEDA_MKDIR					10
#define CEDA_GETFILE				11
#define CEDA_PUTFILE				12
#define CEDA_RENAME					13
#define CEDA_LS                     14
#define CEDA_NLST                   15
#define CEDA_SITECMD                16

/* returncodes */
#define	iFTP_VALID						0
#define	iFTP_FOREVER_INVALID			-100
#define	iFTP_PART_TIME_INVALID		-101
#define	iFTP_TRY_AGAIN					-102

/* stuctures */
typedef struct _CEDA_FTPPort
{
	unsigned short			sHigh;
	unsigned short			sLow;
} CEDA_FTPPort;

typedef struct _CEDA_FTPAdr
{
	unsigned long			lByte4;
	unsigned long			lByte3;
	unsigned long			lByte2;
	unsigned long			lByte1;
} CEDA_FTPAdr;

typedef struct _CEDA_FTPInfo
{
	int						iClientOS;
	int						iServerOS;
	int						iDebugLevel;
	char					pcCommand[iMAXIMUM];
	char					pcReply[iMAXIMUM];
	char					pcReplyCode[4];
	int						iReplyCode;
	char					pcUser[iMIN_BUF_SIZE];
	char					pcPasswd[iMIN_BUF_SIZE];
	char					pcHostName[iMIN_BUF_SIZE];
	char					cTransferType;
	int						iTimeout;
	long					lGetFileTimer;
	long					lReceiveTimer;
	int						iConnected;
	int						iDataSocket;
	int						iCtrlSocket;
	int						iInitOK;
	CEDA_FTPAdr				rRemoteAddress;
	CEDA_FTPPort			rRemotePort;
} CEDA_FTPInfo;

/* prototypes */
int CEDA_FTPInit(CEDA_FTPInfo *, char *, char *, char *, 
					  char, int, long, long, int, int, int);
int CEDA_FTPConnect(CEDA_FTPInfo *); /* initialize the connection */
int CEDA_FTPLogin(CEDA_FTPInfo *); /* performs a complete login at remote sys.*/
int CEDA_FTPSetType(CEDA_FTPInfo *, char); /* transfer type, e.g. ASCII, Bin. */
int CEDA_FTPSetFileStructure(CEDA_FTPInfo *, char);/* set file structure type */
int CEDA_FTPSetTransferMode(CEDA_FTPInfo *, char); /* set transfer mode */
int CEDA_FTPBye(CEDA_FTPInfo *, int); /* logoff */
int CEDA_FTPClose(CEDA_FTPInfo *, int); /* see above */
												/* receives a file from remote machine */
int CEDA_FTPGetFile (CEDA_FTPInfo *, char *, char *, char *, int); 
int CEDA_FTPMGetFile(CEDA_FTPInfo *, char *, char *, int , char *, int, int); 
												/* sends a file to remote machine */
int CEDA_FTPPutFile(CEDA_FTPInfo *, char *, char *, char *, int, char*); 
int CEDA_FTPRemoteRename(CEDA_FTPInfo *, char *, char *);

/* following all functions are comparable with 'normal' UNIX functions */
/* all functions work on the remote system */
int CEDA_FTPmkdir(CEDA_FTPInfo *, char *);
int CEDA_FTPrmdir(CEDA_FTPInfo *, char *);
int CEDA_FTPrm(CEDA_FTPInfo *, char *);
int CEDA_FTPcd(CEDA_FTPInfo *, char *);

#endif  /* __FTPTOOLS_H_ */


