#ifndef _DEF_mks_version_ruldiag_h
  #define _DEF_mks_version_ruldiag_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_ruldiag_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/ruldiag.h 1.3 2004/07/27 16:48:23SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The Master include file.                                              */
/*                                                                        */
/*  Program       :                                                       */
/*                                                                        */
/*  Revision date :                                                       */
/*                                                                        */
/*  Author    	  :                                                       */
/*                                                                        */
/*                                                                        */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */

#ifndef __RUL_DIAG
#define __RUL_DIAG


#define		DIAGTEMPLATE		1
/* Verification of template <%s> */
#define		DIAGNOVALIDRULE		2
/* No valid rules for this template found */
#define		DIAGNORUE			3
/* No duty requirements for this template and event %s found */
#define		DIAGRUE				4
/* Verification of duty requirement <%s> */
#define		DIAGRUENOTVALA		5
/* Rule not valid for arrival at <%s> */
#define		DIAGRUENOTVALD		6
/* Rule not valid for departure at <%s> */
#define		DIAGMAXTTOLONG		7
/* Maximal ground time <%ld min> longer than real ground time */
#define		DIAGMISTTOSHORT		8
/* Minimal split time <%ld min> shorter than real ground time */
/*#define		DIAGNOTUSED			9*/
/* */
#define		DIAGCONDTRUE		10
/* all conditions are true */
#define		DIAGCONDINVALID		11
/* Syntax error in condition */
#define		DIAGCONDFALSE		12
/* Condition <%s> not fulfilled */
#define		DIAGCONDUNEQUAL		13
/* <%s> unequal <%s> */
#define		DIAGTYPEFALSE		14
/* Event type <%s> and condition type <%s> don't fit*/
#define		DIAGNODYNRULE		15
/* Dynamic group <%s> not found */
#define		DIAGTOOMANYDYNR		16
/* Too many dynamic rules exist */
#define		DIAGNODATA		17
/* No date for <%s> found */
#define		DIAGNOTABNAME		18
/* No valid tablename in condition */
#define		DIAGNOFIELDNAME		19
/* No valid fieldname in condition */
#define		DIAGCONDINCORR		20
/* Condition <%s> is not correct */
#define		DIAGNOTINSGR		21
/* %s: <%s> is not in static group <%s> */
#define		DIAGINBUNEQALAOUTB	22
/*  %s: Values for inbound <%s> and outbound <%s> different or empty */
#define		DIAGINBFTYP			23
/* Status of arrival <%s> not configured for demand creation */
#define		DIAGOUTBFTYP		24
/*  Status of departure <%s> not configured for demand creation */

#endif

