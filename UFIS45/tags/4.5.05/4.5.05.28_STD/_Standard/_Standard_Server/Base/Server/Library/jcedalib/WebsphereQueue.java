package jcedalib; 

import com.ibm.mq.*;  
import java.io.*;

public class WebsphereQueue
{
  private static String mks_version = "@(#) UFIS_VERSION $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/jcedalib/WebsphereQueue.java 1.2 2005/09/13 21:00:59SGT heb Exp  $";

  private MQQueueManager qMgr;
  private MQQueue queue;
  private int openOptions;
	   			
	
      public WebsphereQueue(String hostname, String channel, String qManager, String qName, int port, String put_get) 
      throws Exception 
      {
            try 
            {
                  MQEnvironment.hostname = hostname;
                  MQEnvironment.channel  = channel; 
                  MQEnvironment.port = port;
                  MQEnvironment.properties.put(MQC.TRANSPORT_PROPERTY, MQC.TRANSPORT_MQSERIES);
                  qMgr = new MQQueueManager(qManager);
                  if (put_get.equals("PUT"))
                  {
                        openOptions =  MQC.MQOO_OUTPUT; 
                  }
                  else
                  {
                        openOptions = MQC.MQOO_INPUT_AS_Q_DEF ; 
                  }
                  queue = qMgr.accessQueue(qName, openOptions);    	
            }
            catch (Exception e)
            {
            
                  throw new Exception(e);
            }

      } // end WebsphereQueue
	
  protected void DisconnectQueue()
  throws Exception 
  {
    try
    {
      queue.close();
      qMgr.disconnect();
    }
    catch (Exception e)
    {
      throw new Exception("Exception in DisconnectQueue(): " + e);
    }
  } // end DisconnectQueue()
	   			
  public void sendMsg(String msg)
  throws Exception 
  {
    try 
    {
      MQMessage mess = new MQMessage();
      MQPutMessageOptions pmo = new MQPutMessageOptions();
      mess.writeString(msg);
      queue.put(mess, pmo);
    }
    catch (Exception ex) 
    {
      throw new Exception("Exception in sendMsg: " + ex);
    }
  } // end sendMsg
	

      public String receiveMsg()
      throws Exception 
      {
            String msg = new String();
            int i = 0;
            try 
            {
                  MQGetMessageOptions gmo = new MQGetMessageOptions();
                  gmo.options = MQC.MQGMO_WAIT | MQC.MQGMO_FAIL_IF_QUIESCING ;// | MQC.MQGMO_ACCEPT_TRUNCATED_MSG;
                  gmo.waitInterval = MQC.MQWI_UNLIMITED;
                  MQMessage mess = new MQMessage();
                  mess.clearMessage();
                  mess.correlationId = MQC.MQCI_NONE;
                  mess.messageId = MQC.MQMI_NONE;
                  queue.get(mess, gmo);
                  msg = mess.readString(mess.getMessageLength()); 
            }
            catch (Exception ex) 
            {
               throw new Exception("Exception" + ex);
            }
            return msg;
      } // end recvMsg
    
} // end Class WebsphereQueue
