/********************************************************************************************************/
/*	Endscript f�r die DRR Bearbeitung. Sammelt alle relevanten Infos									*/
/*	die dann f�r alle andere Scripte zur Verf�gung stehen												*/
/*	ADO 6.6.01																							*/
/********************************************************************************************************/	

void bas_exit_drr()
{
	int		ilAccHandle;
	char	clStaffUrno[12];
	char	clSday[16];
	char	clLocalYear[6];
	char	clIsRecalc[2];
	
	dbg(DEBUG, "============================= EXIT DRR SCRIPT START =============================\n");
	dbg(DEBUG, "============================= Revision 4.4.1.2.2 (compiled)======================\n");

	/****** erster Schritt: den DRR-Datensatz, der die Ausfuehrung	*****/
	/****** dieses Scripts veranlasst hat, analysieren				*****/
	if (strlen(pcgData) == NULL)
	{
		dbg(DEBUG,"<pcgData> = undef!!! Ausfuehrung des Scripts wird abgebrochen!!!\n");
		return;
	}

	dbg(DEBUG,"pcgData = <%s>\n",pcgData);

	if (strlen(pcgFields) == 0)
	{
		dbg(DEBUG,"<pcgFields> = undef!!! Ausfuehrung des Scripts wird abgebrochen!!!\n");
		return;
	}

	dbg(DEBUG, "pcgFields = <%s>\n",pcgFields);

	/****** die Urno des Mitarbeiters	******/
	if (GetFieldValueFromDataString("STFU",pcgFields,pcgData,clStaffUrno) <= 0)
	{
		dbg(DEBUG,"<varStaffUrno> = undef!!! Ausfuehrung des Scripts wird abgebrochen!!!\n");
		return;
	}

	/****** das Datum, fuer das das Konto ermittelt wird	*/
	if (GetFieldValueFromDataString("SDAY",pcgFields,pcgData,clSday) <= 0)
	{
		dbg(DEBUG,"varSday> = undef!!! Ausfuehrung des Scripts wird abgebrochen!!!\n");
		return;
	}

	ReadTemporaryField("IsRecalc",clIsRecalc);
	if (strcmp(clIsRecalc,"0") == 0)
	{
		/* write account changes to DB	*/
		ilAccHandle = GetArrayIndex("ACCHDL");
		if (ilAccHandle >= 0)
		{
			CloseRecordset(ilAccHandle,1);
		}
	
		/* decrement reference counter to staff accounts */
		strncpy(clLocalYear,clSday,4);
		clLocalYear[4] = '\0';
		StfToAccPrefetch(clStaffUrno,clLocalYear,FALSE);
		sprintf(clLocalYear,"%d",atoi(clLocalYear)+1);
		StfToAccPrefetch(clStaffUrno,clLocalYear,FALSE);
	}

}	
	
