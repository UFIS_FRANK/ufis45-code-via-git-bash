#ifndef _DEF_mks_version_queutil_h
  #define _DEF_mks_version_queutil_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_queutil_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/queutil.h 1.2 2004/07/27 16:48:17SGT jim Exp  $";
#endif /* _DEF_mks_version */
/****************************************************************************/
/****************************************************************************/
/* ******************************************************************** */
/*									*/
/* This file contains the defines for the QCP utility program.		*/
/*									*/
/* ******************************************************************** */

#ifndef __QUEUTIL_INC
#define __QUEUTIL_INC


/* The defines for the displays */
#ifdef _HPUX_SOURCE /* HP UX (11.11?) doesn't print anything with \r */
#define CR_LF           "\n" 
#else
#define CR_LF           "\n\r"
#endif

#define QO_HEADER	"QUEUEID\tNAME\tNO. ITEMS  WITH PRIORITY:\t     STATUS\tTOTAL NO. ITEMS"
#define QO_HEAD2	"\t    \tTOTAL\t    1    2    3    4    5"CR_LF
#define QO_INFO(data)   CR_LF"%5u\t%s%5u\t%5u%5u%5u%5u%5u\t%u\t%8lu",data.id,data.name,data.total,data.nbr_items[0],data.nbr_items[1],data.nbr_items[2],data.nbr_items[3],data.nbr_items[4],data.status,data.msg_count
#define QD_HEADERI	"TYPE\tFUNC\tROUTE\tPRIORITY\tLENGTH\tORIGINATOR"CR_LF
#define QD_ITEM(data)	"%lu\t%u\t%u\t%u\t\t%u\t%u",data.msg_header.mtype,data.function,data.route,data.priority,data.msg_length,data.originator
#define QD_HEADERE	"TYPE\tCOMMAND\t\tORIGINATOR\tSYS ID\t\tRETRY\tOFFSET\tLENGTH"CR_LF
#define QD_EVENT(data)	"%d\t%d\t\t%d\t\t%.8s\t%d\t%d\t%d"CR_LF,data->type,data->command,data->originator,data->sys_ID,data->retry_count,data->data_offset,data->data_length
#define RO_HEADER	"ROUTE ID\tQUEUES"
#define RO_INFO(data)	CR_LF"%u",data.id
#define RO_QUE(data,i)	"%u\t",data.queues[i]
#define RD_INFO(data)	CR_LF"%u",data->route_ID
#define RD_QUE(data,i)	"%u\t",data->routine_ID[i]
				
/* The prompts and errors */

#define M_SELECTION	"Enter selection :"
#define M_NOTSU		"Invalid selection, not SUPER USER..."
#define M_INVALID	"Invalid selection..."
#define M_MEMORY	"Memory allocation error..."
#define M_QUEUINGS	"Function failed, queuing error by send..."
#define M_QUEUINGR	"Function failed, queuing error by receive..."
#define M_ENTERQID1	"Enter queue ID ( 1 - 19999 ) : "
#define M_ENTERQID2	"Enter queue ID ( 1 - 29999 ) : "
#define M_ENTERNAME	"Enter associated name : "
#define M_ENTERPRIO	"Enter priority ( 1 - 5 ) : "
#define M_ENTERNOIT	"Enter number of items : "
#define M_QUEADDOK	"Queue was succesfully added..."
#define M_QUEADDFAIL	"Add of queue was unsuccessful..."
#define M_INVQUEID1	"Invalid queue ID, must be in range 1 - 19999 ..."
#define M_INVQUEID2	"Invalid queue ID, must be in range 1 - 29999 ..."
#define M_INVPRIO	"Invalid priority ( 1 - 5 )..."
#define M_INVNOIT	"Invalid number of items ( 1 - 188 )..."
#define M_QUEDELOK	"Queue delete request successfully processed..."
#define M_QUECLROK	"Queue clear request successfully processed..."
#define M_QUESTAOK	"Queue status change request successfully processed..."
#define M_NOTIMPL	"Function is not yet implemented..."
#define M_COMMAND	"Enter command : "
#define M_ENTERRID1	"Enter route ID ( 1 - 19999 ) : "
#define M_ENTERRID2	"Enter route ID ( 1 - 29999 ) : "
#define M_INVROUID1	"Invalid route ID, must be in range 1 - 19999 ..."
#define M_INVROUID2	"Invalid route ID, must be in range 1 - 29999 ..."
#define M_ROUADDOK	"Route was succesfully added..."
#define M_ROUDELOK	"Route delete request successfully processed..."
#define M_ROUEXISTS	"Route already exists..."
#define M_DESTINATION	"Enter destination ID : "
#define M_RCHANGE	"Route change request successfully processed..."
#define M_RCDELETE	"DESTINATION DELETION PROCESSING"
#define M_RCADD		"DESTINATION ADD PROCESSING"
#define M_COMMANDS	"Command succesfully sent..."
#define M_NOITEMS	"No Items on this queue..."
#define M_NOQUEUE	"Not a possible Queue ID..."
#define M_NOROUTE	"Not a possible ROUTE..."
#define M_NODEST	"Not a possible destination..."
#define M_DESTEXISTS	"Destination already exists..."
#define M_NOSPACE	"No more space for a new destination..."
#define M_NOSTATUS	"Status error..."

/* General purpose defines */

#define ADD		1
#define DELETE		2
#define FOUND		1
#define OK2		2
#define NOT_FOUND_D	-2

#endif

/****************************************************************************/
/****************************************************************************/
