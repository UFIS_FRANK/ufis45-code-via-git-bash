<?php 
/* 
V0.60 08 Nov 2000 (c) 2000 John Lim. All rights reserved.
  Released under Lesser GPL library license. See License.txt. 
  Set tabs to 8.
  
  Latest version is available at http://php.weblogs.com/
*/
 
if (!defined("_ADODB_GENERAL_LAYER")) {
	define("_ADODB_GENERAL_LAYER", 1 );
	GLOBAL $ADODB_vers, $ADODB_Database, $ADODB_RootPath;
	
	/* SET THE VALUE BELOW TO THE DIRECTORY WHERE THIS FILE RESIDES */
	//	$ADODB_RootPath = '/usr/local/apache/htdocs/adodb';	

	//==============================================================================================	
	// CHANGE NOTHING BELOW UNLESS YOU ARE CODING
	//==============================================================================================	

	$ADODB_Database = '';		
	$ADODB_vers = 'V0.60 08 Nov 2000 (c) 2000 John Lim. All rights reserved.';
	
	class ADODBFieldObject { // helper class for Fields
		var $name = '';
		var $max_length=0;
		var $type="";
	}

	// Connection object
	class ADODBConnection {
	/* PUBLIC VARS */
	var $dataProvider = 'native';
	
	var $databaseType = '';	 // RDBMS currently in use, eg. odbc, mysql, mssql
								
	var $database = '';	// Name of database to be used.	
	
	var $host = ''; //The hostname of the database server
							
	var $user = ''; // The username which is used to connect to the database server. 
	
	var $password = ''; // Password for the username -- currently not saved for security reasons

	var $debug = true; // if set to true will output sql statements

	//var $maxblobsize = 8000; // maximum size of blobs or large text fields -- some databases die otherwise like foxpro
	
	var $concat_operator = '+'; 	// default concat operator -- change to || for Oracle/Interbase	
//	var $fmtDate = "'Y-m-d'"; 	// used by DBDate() as the default date format used by the database
	var $fmtDate = "'d-m-Y'"; 	// used by DBDate() as the default date format used by the database
	var $fmtTimeStamp = "'Y-m-d, h:i:s A'"; // used by DBTimeStamp as the default timestamp fmt.
	var $true = '1'; 		// string that represents TRUE for a database
	var $false = '0'; 		// string that represents FALSE for a database
	var $replaceQuote = "\\'"; 	// string to use to replace quotes
        var $hasInsertID = false; 	// supports autoincrement ID?
        var $hasAffectedRows = false; 	// supports affected rows for update/delete?
        var $autoCommit = true; 
	/* PRIVATE VARS */
	
	//var $_rsList	= array();	// An array of result sets.  For results cleanup purposes. not needed in PHP4*/
	
	var $_connectionID	= -1;	// The returned link identifier whenever a successful database connection is made.	*/
		
	var $_errorMsg = '';		// A variable which was used to keep the returned last error message.  The value will
					//then returned by the errorMsg() function	
						
	var $_queryID = -1;		// This variable keeps the last created result link identifier.		*/
	
	var $_isPersistentConnection = false;	// A boolean variable to state whether its a persistent connection or normal connection.	*/
	
	var $_bindInputArray = false; // set to true if ADODBConnection.Execute() permits binding of array parameters.
	
	function ADODBConnection()			
	{
		die('Virtual Class -- cannot instantiate');
	}
	
	// returns TRUE or FALSE
	function Connect($argHostname = "", $argUsername = "", $argPassword = "", $argDatabaseName = "") {
		if ($argHostname != "") $this->host = $argHostname;
		if ($argUsername != "") $this->user = $argUsername;
		//if ($argPassword != "") $this->password = $argPassword;
		if ($argDatabaseName != "") $this->database = $argDatabaseName;		
		
	
		return $this->_connect($argHostname,$argUsername,$argPassword,$argDatabaseName);
		
	}	
	
	// return TRUE or FALSE		
	function PConnect($argHostname = "", $argUsername = "", $argPassword = "", $argDatabaseName = "")
	{
		if ($argHostname != "") $this->host = $argHostname;
		if ($argUsername != "") $this->user = $argUsername;
		//if ($argPassword != "") $this->password = $argPassword;
		if ($argDatabaseName != "") $this->database = $argDatabaseName;			
			
		if ( $this->_pconnect($argHostname, $argUsername, $argPassword, $argDatabaseName)) {
			$this->_isPersistentConnection = true;	
			return true;			
		}
		
		return false;
	}
	
	// should actually only prepare the sql statement and return a dummy recordset
	// prepare is left for legacy applications, but should not be used because
	// too many databases implement prepare differently.
	function Prepare($sql)
	{
		return $this->Execute($sql);
	}

        // returns the last inserted ID
        function Insert_ID()
        {
                if ($this->hasInsertID) return $this->_insertid();
                if ($this->debug) print '<p>Insert_ID error</p>';
                return false;
        }
        
        // returns # rows affected by UPDATE/DELETE
        function Affected_Rows()
        {
                if ($this->hasAffectedRows) {
                       $val = $this->_affectedrows();
                       return ($val < 0) ? false : $val;
                }
                        
                if ($this->debug) print '<p>Affected_Rows error</p>';
                return false;
        }
        
	// override this to return the last error message
	function ErrorMsg()
	{
		return '!! '.$this->databaseType.' does not support error messages currently';
	}
	
	// override this to return the last error number
	function ErrorNo()
	{
		return 0;
	}
	
	function &Execute($sql,$inputarr=false) {
		if (!$this->_bindInputArray && $inputarr) {
			$sqlarr = explode("?",$sql);
			$sql = "";
			for ($i=0, $zmax = sizeof($inputarr); $i < $zmax; $i++) {
				$sql .= $sqlarr[$i];
				$sql .= '\''.$inputarr[$i].'\'';
			}
			$sql .= $sqlarr[$i];
			if ($zmax+1 != sizeof($sqlarr))	 print "Input Array does not match ?: $sql";
			$inputarr = false;
		}
		if ($this->debug) {
			print "<hr>($this->databaseType): $sql<hr>";
			$this->_queryID = $this->_query($sql,$inputarr);
		} else 
			$this->_queryID =@$this->_query($sql,$inputarr);
		
		if ($this->_queryID === false) {
			return false;
		}


		$rsclass = "ADORecordSet_".$this->databaseType;

		$rs = new $rsclass(&$this->_queryID);

		//$this->_insertQuery(&$rs); PHP4 handles closing automatically
		$rs->sql = $sql;

		return $rs;
	}
	
 	
	/*	Returns: true on success, false on failure
		Close the database connection.	*/	
	function Close() 
	{
		/*if ($this->_rsList && sizeof($this->_rsList) > 0) {
			while(list($_key, $_resultID) = each($this->_rsList)) {
				$_resultID->Close();
			} 
		} PHP4 handles closing automatically */
		
		if ($this->_isPersistentConnection != true) $this->_close();
		else return true;
	
	}
	
	// returns true/false
	function BeginTrans()
	{
                return false;
	}
	// returns true/false. 
	// If database does not support transactions, always return true as data always commited.
	function CommitTrans()
	{
                return true;
	}
	// returns true/false
	// If database does not support transactions, rollbacks always fail, so return false.
	function RollbackTrans()
	{
                return false;
	}

        // returns array of databases -- not yet implemented
        function &MetaDatabases()
        {
		return false;
        }
        
        // returns array of tables for current database - not yet implemented
        function &MetaTables()
        {
		return false;
        }
        
        // returns columns of table -- should we use ADOFieldObject or PHPLib format?
        // - not yet implemented
        function &MetaColumns($table)
        {
		return false;
        }
        	
	//////////////////////////////////////////////// UTILITY FUNCTIONS
		 
	function Concat()
	{
		$first = true;
		$s = "";
		$arr = func_get_args();
		$concat = $this->concat_operator;
		foreach($arr as $a) {
			if ($first) {
				$s = (string) $a;
				$first = false;
			} else $s .= $concat.$a;
		}
		return $s;
	}
	
	// format and return date string in database date format
	function DBDate($d)
	{
		return date($this->fmtDate,$d);
	}
	
	// format and return date string in database timestamp format
	function DBTimeStamp($ts)
	{
		return date($this->fmtTimeStamp,$ts);
	}
	
	// quote string to be sent back to database
	function qstr($s,$nofixquotes=false)
	{
		if (!$nofixquotes) return  "'".str_replace("'",$this->replaceQuote,$s)."'";
		return "'".$s."'";
	}
	
	//function _insertQuery(&$query_id) {
		//$this->_rsList[] = &$query_id;
	//}	
	
	
	} // end class ADODB
	
	
	//==============================================================================================	
	class ADORecordSet {
	/*	public variables	*/
	var $dataProvider = "native";
	var $fields; // holds the current row data
	var $blobSize = 200; 	// any varchar/char field this size or greater is treated as a blob
				// in other words, we use a text area for editting.
	var $canSeek = false; 	// indicates that seek is supported
	var $sql; 		// sql text
	var $EOF = false;	/* Indicates that the current record position is after the last record in a Recordset object. */
	
	/*	private variables	*/
	var $_numOfRows = -1;	
	var $_numOfFields = -1;	
	var $_queryID = -1;	/* This variable keeps the result link identifier.	*/
	var $_currentRow = -1;	/* This variable keeps the current row in the Recordset.	*/
	var $_closed = false; 	/* has recordset been closed */
	

	function ADORecordSet($queryID) {
		$this->_queryID = $queryID;
		if ($queryID) @$this->_initrs();
		else {
			$this->_numOfRows = 0;
			$this->_numOfFields = 0;
		}
		if ($this->_numOfRows != 0 && $this->_numOfFields && $this->_currentRow == -1) {
			$this->_currentRow = 0;
			$this->EOF = ($this->_fetch() === false);
		} else 
			$this->EOF = true;
		
 		return $this->_queryID;
	}
	
	// generate a <SELECT> string from a recordset, and return the string
	function GetMenu($name,$defstr='',$blank1stItem=true,$multiple=false,$mult_size=4)
	{
		$hasvalue = false;

		if ($multiple) {
			$multiple = "multiple size=$mult_size";
			if (!strpos($name,'[]')) $name .= '[]';
		} else $multiple = "";
		
		
		$s = "<select name='$name' $multiple>";
		if ($blank1stItem) {
			$s .= "\n<option>";
		}
		if ($this->FieldCount() > 1) $hasvalue=true;
		while(!$this->EOF) {
			$zval = trim($this->fields[0]);
			if ($blank1stItem && $zval=="") {
				$this->MoveNext();
				continue;
			}
			if ($hasvalue) {
				$value = 'value="'.htmlspecialchars(trim($this->fields[1])).'"';
			}
			
			if (strcasecmp($zval,$defstr)==0) $s .= '<option selected $value>'.htmlspecialchars($zval);
			else $s .= "\n<option ".$value.'>'.htmlspecialchars($zval);
			$this->MoveNext();
		}
		
		return $s ."\n</select>\n";
	}
	
	// return an array indexed by the rows (0-based) from the recordset
	function &GetArray($nRows = -1) {
		$results = array();
		$cnt = 0;
		while (!$this->EOF && $nRows != $cnt) {
			$results[$cnt++] = $this->fields;
			$this->MoveNext();
		}
		
		return $results;
	}
	
	// return an associative array from the recordset
	function &GetAssoc($force_array = false) {
		$cols = $this->_numOfFields;
		if ($cols < 2) {
			return false;
		}
		$results = array();
		if ($cols > 2 || $force_array) {
			while (!$this->EOF) {
				$results[trim($this->fields[0])] = array_slice($this->fields, 1);
				$this->MoveNext();
			}
		} else {
			// return scalar values
			while (!$this->EOF) {
			// some bug in mssql PHP 4.02 -- doesn't handle references properly so we FORCE creating a new string
				$val = ''.$this->fields[1]; 
				$results[trim($this->fields[0])] = $val;
				$this->MoveNext();
			}
		}
		return $results; 
	}
	
	// return a timestamp formated as user desires
	function UserTimeStamp($v,$fmt='Y-m-d')
	{
	
		if (!ereg( "([0-9]{4})-([0-9]{1,2})-([0-9]{1,2}) ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})", 
			$v, $rr)) return $v;
		
		if ($rr[1] < 1970) return '- - ::';
		// h-m-s-MM-DD-YY
		return date($fmt,mktime($rr[4],$rr[5],$rr[6],$rr[2],$rr[3],$rr[1]));
		
	}
	
        // return a date formated as user desires
	function UserDate($v,$fmt='Y-m-d h:i:s')
	{
		if (!trim($v)) return '';
		if (!ereg( "([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})", 
			$v, $rr)) return $v;
			
		if ($rr[1] < 1970) return '- -';
		// h-m-s-MM-DD-YY
		return date($fmt,mktime(0,0,0,$rr[2],$rr[3],$rr[1]));
	
	}

	// returns true or false
	function MoveFirst() 
	{

		if ($this->_currentRow == 0) return true;
		return $this->Move(0);			
	}				

	// Returns: true on success, false on failure 	
	function MoveLast() 
	{
		if ($this->_numOfRows >= 0) return $this->Move($this->_numOfRows-1);
                while (!$this->EOF) $this->MoveNext();
		return true;
	}
	
	/* get value using assoc array*/
	function Fields($colname)
	{
		return $this->fields[$colname];
		
	}
	
	// clean up
	function Close() 
	{
		if (!$this->_closed) {
			$this->_closed = true;
			return $this->_close();		
		} else
			return true;
	}
	
	// Returns: true if there still rows available, or false if there are no more rows (EOF). 		
	function MoveNext($ignore_fields=false) 
	{
		if ($this->_numOfRows != 0) {		
			$this->_currentRow++;
			if ($this->_fetch($ignore_fields)) return true;
		}
		$this->EOF = true;
		return false;
	}	
	
	// scroll forward
	function Move($rowNumber = 0) 
	{
		if ($rowNumber == $this->_currentRow) return true;
                if ($rowNumber > $this->_numOfRows)
                        if ($this->_numOfRows != -1)
                                $rowNumber = $this->_numOfRows-1;
   
                   if ($this->canSeek) {
                        if ($this->_seek($rowNumber)) {
				$this->_currentRow = $rowNumber;
				if ($this->_fetch()) {
					$this->EOF = false;	
                                      //  $this->_currentRow += 1;			
					return true;
				}
			} else 
				return false;
                } else {
                        if ($rowNumber < $this->_currentRow) return false;
                        while (! $this->EOF && $this->_currentRow < $rowNumber) {
				$this->_currentRow++;
                                if (!$this->_fetch()) $this->EOF = true;
			}
                        if ($this->EOF) return false;
                        return true;
                }
		
		$this->fields = null;	
		$this->EOF = true;
		return false;
	}
	function RsTable() {
		return $this->_fetchall();
	}
		
	function RecordCount() {
		return $this->_numOfRows;
	}

	function CurrentRow() {
		return $this->_currentRow;
	}

	// synonym for CurrentRow -- for ADO compat
	function AbsolutePosition() {
		return $this->_currentRow;
	}
	
	function FieldCount() {
		return $this->_numOfFields;
	}   
	
	// returns the general type of the data: 
	//	C for character < 200 chars
	//	B for BLOB (>= 200 chars)
	// 	N for numeric
	//	D for date
	//	T for timestamp
	// 	L for logical/Boolean
	// this is used for formating 
	// - trick -- if return value is 'C' or less, then it is alphanumeric
	//
	// the data types here are a generic one that covers many databases
	function MetaType($t,$len=-1)
	{

		switch (strtoupper($t)) {
		case 'VARCHAR':
		case 'CHAR':
		case 'STRING':
		case 'C':
			if ($len <= $this->blobSize) return 'C';
		
		case 'TEXT':
		case 'BLOB':
		case 'M':
			return 'B';
			
		case 'DATE':
		case 'D':
			return 'D';
		
		
		case 'TIME':
		case 'TIMESTAMP':
		case 'DATETIME':
		case 'T':
			return 'T';
		
		case 'BOOLEAN': 
		case 'BIT':
			return 'L';
		
		default: return 'N';
		}
	}
	} // end class ADORecordSet
	
	//==============================================================================================	
	// HELPER FUNCTIONS
	//==============================================================================================			
	
        // old implementation pre 1.00
        function ADOLoadDB($dbType) { return ADOLoadCode($dbType);}
        
        // new 1.0 implementation
        function ADOLoadCode($dbType) {
	GLOBAL $ADODB_RootPath;
	GLOBAL $ADODB_Database;
		switch (strtolower($dbType)) {
			case 'odbc':	
			case 'mysql':
			case 'mssql':
			case 'vfp':
			case 'access':
			case 'odbc_mssql':
			default:
				$ADODB_Database = $dbType;
				include("$ADODB_RootPath/adodb-$dbType.inc.php");
//				include("adodb-$dbType.inc.php");
				break;
		}					
		return true;		
	}

	
	function &ADONewConnection($db="")
	{
	GLOBAL $ADODB_Database;
		if (!$db) $db = $ADODB_Database;
		
		$cls = "ADODB_".$db;
		return new $cls();
	}
	
} // define _ADOBDB_GENERAL_LAYER
?>
