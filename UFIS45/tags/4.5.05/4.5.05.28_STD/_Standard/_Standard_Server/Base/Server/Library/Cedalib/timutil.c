#ifndef _DEF_mks_version_timutil_c
  #define _DEF_mks_version_timutil_c
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_timutil_c[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Cedalib/timutil.c 1.2 2004/08/10 20:29:53SGT jim Exp  $";
#endif /* _DEF_mks_version */

#include <time.h>
#include <stdio.h>
#include "glbdef.h"
/**********************************************************************/
/* Following the function prototypes				      */
/**********************************************************************/
time_t strtotim(char *);
void   get_utc(char *);
void   StrAddTime(char *,char*,char *);
long   StrDiffTime(char *,char*);



/*
 *     struct  tm {
 *             int     tm_sec;    seconds after the minute -- [0, 59]  
 *             int     tm_min;    minutes after the hour -- [0, 59] 
 *             int     tm_hour;   hour since midnight -- [0, 23] 
 *             int     tm_mday;   day of the month -- [1, 31] 
 *             int     tm_mon;    months since January -- [0, 11] 
 *             int     tm_year;   years since 1900 
 *             int     tm_wday;   days since Sunday -- [0, 6] 
 *             int     tm_yday;   days since January 1 -- [0, 365] 
 *             int     tm_isdst;  flag for daylight savings time 
 *             long    tm_tzadj;  seconds from GMT (east < 0) 
 *             char    tm_name[LTZNMAX];   name of timezone 
 *     };
 */

time_t strtotim(char *dt)
{
	struct tm *_tm;
	time_t    now;
	char	  _tmpc[3];


	if (strlen(dt) != 10 ){
		return (time_t) 0;
	} /* end if */

	now = time(0L);
	_tm = (struct tm *)localtime(&now);

	_tm -> tm_sec = 0;
	strncpy(_tmpc,dt+8,2);
	_tm -> tm_min = atoi(_tmpc);
	strncpy(_tmpc,dt+6,2);
	_tm -> tm_hour = atoi(_tmpc);
	strncpy(_tmpc,dt+4,2);
	_tm -> tm_mday = atoi(_tmpc);
	strncpy(_tmpc,dt+2,2);
	_tm -> tm_mon = atoi(_tmpc)-1;
	strncpy(_tmpc,dt,2);
	_tm -> tm_year = atoi(_tmpc);
	_tm -> tm_wday = 0;
	_tm -> tm_yday = 0;

	now = mktime(_tm);

	return now;
	
}

void   get_utc(char *dest)
{
	struct tm *_tm;
	time_t    now;
	memset(dest,0x00,6);
	now = time(0L);
	strftime(dest,6,"%" "H%" "M",(struct tm *)gmtime(&now));
	return ;
}


void	StrAddTime(char *pcpBase,char *pcpOffs,char *pcpDest)
{

	struct tm *_tm;
	time_t ilTime;

	ilTime = strtotim(pcpBase);
	ilTime += (time_t)(atoi(pcpOffs)*60);
	_tm = localtime((time_t *)&ilTime);
	strftime(pcpDest,11,"%y%m%d%""H%""M",_tm);	


	return ;
} /* end of StrAddTime */
long	StrDiffTime(char *pcpT1,char *pcpT2)
{

	time_t ilT1;
	time_t ilT2;

	ilT1 = strtotim(pcpT1);
	ilT2 = strtotim(pcpT2);


	return ((long)(ilT1-ilT2));
} /* end of StrAddTime */
