#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h"  /* sets UFIS_VERSION, must be done defore mks_version */
  static char mks_version[]   MARK_UNUSED = "@(#) "UFIS_VERSION" Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/dirutil.h 1.0 2011/11/24 04:21:53SGT ble Exp  $";
#endif /* _DEF_mks_version */

/* ********************************************************************** */
/*  The Master include file for dirutil                                   */
/*                                                                        */
/*  Program       : DIRUTIL                                               */
/*                                                                        */
/*  Revision date :                                                       */
/*                                                                        */
/*  Author        :  Mei,Lee Bee Suan                                     */
/*                                                                        */
/*                                                                        */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */
/* ********************************************************************** */

#ifndef __DIRUTIL_INC
#define __DIRUTIL_INC


#define SORT_BY_FILENAME 8
/* ********************************************************************** */
/* ********************************************************************** */
#endif
