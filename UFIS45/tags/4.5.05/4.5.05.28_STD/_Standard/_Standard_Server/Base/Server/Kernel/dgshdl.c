#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Kernel/dgshdl.c 1.2 2007/07/17 15:13:37SGT akl Exp  $";
#endif /* _DEF_mks_version */

/*                                                                            */
/* UFIS AS DGSHDL.C                                                           */
/*                                                                            */
/* Author         : Andreas Kloidt                                            */
/* Date           : April 2007                                                */
/* Description    : Process to receive and send messages to SAFEGATE DGS      */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
static char sccs_dgshdl[]="%Z% UFIS 4.4 (c) ABB AAT/I %M% %I% / %E% %U% / AKL";
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE

#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <netdb.h>
#include <time.h>
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h" 
#include "tools.h"
#include "helpful.h"
#include "timdef.h"
#include "dgshdl.h"
 

#define XS_BUFF  128
#define S_BUFF   512
#define M_BUFF   1024
#define L_BUFF   2048
#define XL_BUFF  4096
#define XXL_BUFF 8192 



/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
FILE *outp       = NULL;
int  debug_level = TRACE;
/******************************************************************************/
/* External functions                                                         */
/******************************************************************************/
extern int SetSignals(void (*HandleSignal)(int));
extern int DebugPrintItem(int,ITEM *);
extern int DebugPrintEvent(int,EVENT *);
extern int init_db(void);
extern int  ResetDBCounter(void);
extern void HandleRemoteDB(EVENT*);
extern int  sql_if(short ,short* ,char* ,char* ); 
extern int close_my_cursor(short *cursor); 
extern void snap(char*,int,FILE*);
extern int GetDataItem(char *pcpResult, char *pcpInput, int ipNum, char cpDel,char *pcpDef,char *pcpTrim);
extern int BuildItemBuffer(char *pcpData, char *pcpFieldList, int ipNoOfFields, char *pcpSepChr);
extern int get_item(int,char*,char*,int,char*,char*,char*);
extern int get_item_no(char *s, char *f, short elem_len);
extern void  GetServerTimeStamp(char*,int,long,char*);
extern int AddSecondsToCEDATime(char *,long,int);
extern int get_real_item(char *, char *, int);
extern int GetNoOfElements(char *s, char c);
extern long nap(long);
extern int GetFullDay(char *pcpData, int *ipDay, int *ipMin, int *ipWkDay);
extern int syslibSearchDbData(char*,char*,char*,char*,char*,int*,char*);
extern int tcp_create_socket(int,char*);
extern int tcp_open_connection(int,char*,char*);
extern int SendCedaEvent(int,int,char*,char*,char*,char*,
                         char*,char*,char*,char*,char*,char*,int,int);

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;        /* The queue item pointer  */
static EVENT *prgEvent     = NULL;        /* The event pointer       */
static int   igItemLen     = 0;           /* length of incoming item */
static int   igInitOK      = FALSE;      /* Flag for init */

/*   static CFG   *prgCfg; */                     /* structure of the cfg-file */
static char  pcgHomeAp[XS_BUFF];      /* buffer for home airport */
static char  pcgTabEnd[XS_BUFF];      /* buffer for TABEND */
static char  pcgTwStart[XS_BUFF] = "";
static char  pcgTwEnd[XS_BUFF];
static char  pcgConfFile[S_BUFF];      /* buffer for config-file name */
static int   igUseHopo = FALSE;          /* flag for use of HOPO-field */
static int   igModID_Router  = 1200;      /* MOD-ID of Router  */

static char      pcgHostName[XS_BUFF];
static char      pcgHostIp[XS_BUFF];

static int    igQueCounter=0;

/*entry's from configfile*/
static char pcgConfigFile[512];
static char pcgCfgBuffer[512];
static EVENT *prgOutEvent = NULL;
static char pcgCurrentTime[32];
static int igDiffUtcToLocal;
static char pcgServerChars[110];
static char pcgClientChars[110];
static char pcgUfisConfigFile[512];

/* XML structure */
#define MAX_FILE_SIZE 512000
#define MAX_XML_LINES 2000
#define MAX_INTERFACES 50
typedef struct
{
  int ilLevel;
  char pclTag[16];
  char pclName[32];
  char pclType[8];
  char pclBasTab[32];
  char pclBasFld[32];
  char pclFlag[64];
  char pclMethod[MAX_INTERFACES][32];
  int ilNoMethod;
  char pclFieldA[MAX_INTERFACES][16];
  int ilNoFieldA;
  char pclFieldD[MAX_INTERFACES][16];
  int ilNoFieldD;
  int ilRcvFlag;
  int ilPrcFlag;
  char pclData[4000];
  char pclOldData[4000];
} _LINE;

typedef struct
{
  _LINE rlLine[MAX_XML_LINES];
} _XML;
_XML rgXml;
static int igCurXmlLines = 0;

typedef struct
{
  char pclIntfName[32];
  int ilIntfIdx;
} _INTERFACE;
_INTERFACE rgInterface[MAX_INTERFACES];
static int igCurInterface = 0;
/* End XML structure */
typedef struct
{
  char pclField[32];
  char pclType[8];
  char pclMethod[32];
  char pclData[1024];
} _XMLKEY;
_XMLKEY rgKeys[16];
static int igNoKeys;
typedef struct
{
  char pclField[32];
  char pclType[8];
  char pclMethod[32];
  char pclData[1024];
  char pclOldData[1024];
} _XMLDATA;
_XMLDATA rgData[1000];
static int igNoData;
static char pcgAdid[4];
static char pcgUrno[16];
static char pcgIntfName[32];
static int igIntfIdx;
static int igVdgsTimeDiff;
static int igTimeWindowArrBegin;
static int igTimeWindowArrEnd;
static int igTimeWindowDepBegin;
static int igTimeWindowDepEnd;
static int igMaxTimeDiffForUpdate;
static int igActualPeriod;
static char pcgNewUrnos[1000*16];
static int igLastUrno = 0;
static int igLastUrnoIdx = -1;
static int igSavInsCnt = 0;
static int igSavDelCnt = 0;
static int igSavRetCnt = 0;
static char pcgCDStart[64];
static char pcgCDEnd[64];
static char pcgMode[16] = "INPUT";
static int igAutoActType;
#ifndef _SOLARIS
static struct sockaddr_in my_client;
#else
static struct sockaddr my_client;
#endif
static char pcgLastMessage[32] = "20201231235959";

static char pcgSitaText[512000];

static BOOL bgSocketOpen = FALSE; /* global flag for connection state */
static BOOL bgAlarm = FALSE; /* global flag for time-out socket-read */
static time_t tgNextConnect = 0; /* time for trying a connection to DGS */
static time_t tgRecvTimeOut = 0; /* time until dgs waits for valid telegram */
static int igSock = 0; /* global tcp/ip socket for receive */
static int igSockCreate = 0; /* global tcp/ip socket for create */
static int igMsgCounter = 0; /* counter for message numbers */
static int igRecvTimeout = 0; /* number of secs to wait for a valif telegram */
static CFG prgCfg;
static char pcgDGS_Host[64]; /* buffer for the host that is connected */
static char pcgRecvBuffer[MAX_TELEGRAM_LEN]; /* global buffer for socket-read */
static char pcgSendBuffer[MAX_TELEGRAM_LEN]; /* global buffer for socket-write */
static int igHeadLen;
static FILE *pgReceiveLogFile = NULL; /* log file pointer for all received messages */
static FILE *pgSendLogFile = NULL; /* log file pointer for all sent messages */
static int igWaitForAck= 0; /* seconds for waiting for ack */
static int igMaxSends = 0; /* maximum number of sends for telegram */
static int igFtpTimeout = 0;
static int igKeepAlive = 0;
static char pcgNextKeepAlive[32] = "\0";
static char pcgTestMessage[MAX_TELEGRAM_LEN] = "\0";


static int    Init_dgshdl();
static int    Reset(void);                        /* Reset program          */
static void   Terminate(int);                     /* Terminate program      */
static void   HandleSignal(int);                  /* Handles signals        */
static void   HandleErr(int);                     /* Handles general errors */
static void   HandleQueErr(int);                  /* Handles queuing errors */
static int    HandleInternalData(void);           /* Handles event data     */
static void   HandleQueues(void);                 /* Waiting for Sts.-switch*/
/******************************************************************************/
/* Function prototypes by AKL                                                 */
/******************************************************************************/
/* Init-functions  */

static int GetQueues();
static int SendEvent(char *pcpCmd,int ipModID,int ipPriority,char *pcpTable,
                     char *pcpTwStart,char* pcpTwEnd,
		     char *pcpSelection,char *pcpFields,char *pcpData,
                     char *pcpAddStruct,int ipAddstructSize); 
static int GetConfig();
static int TimeToStr(char *pcpTime,time_t lpTime);
static int TriggerBchdlAction(char *pcpCmd, char *pcpTable, char *pcpSelection,
                              char *pcpFields, char *pcpData, int ipBchdl, int ipAction);
static long GetSecondsFromCEDATime(char *pcpDateTime);
static void TrimRight(char *pcpBuffer);
static void TrimLeftRight(char *pcpBuffer);
static void CheckCDATA(char *pcpBuffer);
static int MyAddSecondsToCEDATime(char *pcpTime, long lpValue, int ipP);
static char *GetLine(char *pcpLine, int ipLineNo);
static void CopyLine(char *pcpDest, char *pcpSource);
static int GetXMLSchema();
static int CheckXmlInput(char *pcpData);
static int HandleInput(char *pcpData, int ipBegin, int ipEnd);
static int GetXmlLineIdx(char *pcpTag, char *pcpName, char *pcpSection);
static int RemoveCharFromTag(char *pcpTag, char *pcpTmpTag);
static int CheckData(char *pcpData, int ipIdx);
static int CheckFlightNo(char *pcpFlight);
static int HandleUpdate(char *pcpFunc);
static int SearchFlight(char *pcpMethod, char *pcpTable);
static int GetKeys(char *pcpFkt);
static int GetData(char *pcpMethod, int ipIdx, char *pcpName);
static int ShowKeys();
static int ShowData();
static int ShowAll();
static int ConnectTCP(char *pcpHost, int ipTry);
static void CloseTCP();
static int poll_q_and_sock();
static int Receive_data(int ipSock, int ipAlarm, char *pcpCalledBy);
static int Send_data(int ipSock, char *pcpData, int ipLen);
static void snapit(void *pcpBuffer, long lpDataLen, FILE *pcpDbgFile);
static int GetCfgEntry(char *pcpFile,char *pcpSection,char *pcpEntry,short spType,char **pcpDest,
                       int ipValueType,char *pcpDefVal);
static int CheckValue(char *pcpEntry,char *pcpValue,int ipType);
static int SendKeepAliveMsg();
static int SendAckMsg();


/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
  int    ilRc = RC_SUCCESS;            /* Return code            */
  int    ilCnt = 0;
  int   ilItemFlag=TRUE; 
  time_t now = 0;
  INITIALIZE;            /* General initialization    */

  /* signal handling of SIGPIPE,SIGCHLD,SIGALRM,SIGTERM */
  SetSignals(HandleSignal);

  dbg(TRACE,"------------------------------------------");
  dbg(TRACE,"MAIN: version <%s>",sccs_dgshdl);

  /* Attach to the MIKE queues */
  do{
    ilRc = init_que();
    if(ilRc != RC_SUCCESS)
      {
	dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
	sleep(6);
	ilCnt++;
      }/* end of if */
  }while((ilCnt < 10) && (ilRc != RC_SUCCESS));
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
      sleep(60);
      exit(1);
    }else{
      dbg(TRACE,"MAIN: init_que() OK!");
      dbg(TRACE,"MAIN: mod_id   <%d>",mod_id);
      dbg(TRACE,"MAIN: mod_name <%s>",mod_name);
    }/* end of if */
  do
    {
      ilRc = init_db();
      if (ilRc != RC_SUCCESS)
	{
	  dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
	  sleep(6);
	  ilCnt++;
	} /* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
      sleep(60);
      exit(2);
    }else{
      dbg(TRACE,"MAIN: init_db()  OK!");
    } /* end of if */

  /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */
  *pcgConfFile = 0x00;
  sprintf(pcgConfFile,"%s/%s",getenv("BIN_PATH"),mod_name);
  ilRc = TransferFile(pcgConfFile);
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: TransferFile(%s) failed!",pcgConfFile);
    } /* end of if */
  dbg(TRACE,"MAIN: Binary-file = <%s>",pcgConfFile);
  ilRc = SendRemoteShutdown(mod_id);
  if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    }
  if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
      dbg(TRACE,"MAIN: waiting for status switch ...");
      HandleQueues();
      dbg(TRACE,"MAIN: now running ...");
    }/* end of if */
  if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
      dbg(TRACE,"MAIN: initializing ...");
      dbg(TRACE,"------------------------------------------");
      if(igInitOK == FALSE)
	{
	  ilRc = Init_dgshdl();
	  if(ilRc == RC_SUCCESS)
	    {
	      dbg(TRACE,"");
	      dbg(TRACE,"------------------------------------------");
	      dbg(TRACE,"MAIN: initializing OK");
	      igInitOK = TRUE;
	    } 
	}
    }else{
      Terminate(1);
    }
  dbg(TRACE,"------------------------------------------");
    
  if (igInitOK == TRUE)
  {
     now = time(NULL);
     while(TRUE)
     {
        if ((ilRc = poll_q_and_sock()) == RC_SUCCESS)
        {
        }
        now = time(NULL);
     } /* end while */
  }
  else
  {
     dbg(TRACE,"MAIN: Init_dgshdl() failed with <%d> Sleeping 30 sec.! Then terminating ...",ilRc);
     sleep(30);
  }
  exit(0);
  return 0;
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_dgshdl()
{
  int    ilRc = RC_SUCCESS;            /* Return code */

  GetQueues();
  /* reading default home-airport from sgs.tab */
  memset(pcgHomeAp,0x00,sizeof(pcgHomeAp));
  ilRc = tool_search_exco_data("SYS","HOMEAP",pcgHomeAp);
  if (ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"Init_dgshdl : No HOMEAP entry in sgs.tab: EXTAB! Please add!");
      return RC_FAIL;
    }
  else
    {
      dbg(TRACE,"Init_dgshdl : HOMEAP = <%s>",pcgHomeAp);
    }
  /* reading default table-extension from sgs.tab */
  memset(pcgTabEnd,0x00,sizeof(pcgTabEnd));
  ilRc = tool_search_exco_data("ALL","TABEND",pcgTabEnd);
  if (ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"Init_dgshdl : No TABEND entry in sgs.tab: EXTAB! Please add!");
      return RC_FAIL;
    }
  else
    {
      dbg(TRACE,"Init_dgshdl : TABEND = <%s>",pcgTabEnd);
      memset(pcgTwEnd,0x00,XS_BUFF);
      sprintf(pcgTwEnd,"%s,%s,%s",pcgHomeAp,pcgTabEnd,mod_name);
      dbg(TRACE,"Init_dgshdl : TW_END = <%s>",pcgTwEnd);
      
      if (strcmp(pcgTabEnd,"TAB") == 0)
	{
	  igUseHopo = TRUE;
	  dbg(TRACE,"Init_dgshdl: use HOPO-field!");
	}
    }

  ilRc = GetConfig();
  if (strlen(prgCfg.recv_log) > 0)
  {
     pgReceiveLogFile = fopen(prgCfg.recv_log,"w");
     if (!pgReceiveLogFile)
     {
        dbg(TRACE,"Init_bhsif : ReceiveLog: <%> not opened! fopen() returns <%s>.",
            prgCfg.recv_log,strerror(errno));
     }
  }
  if (strlen(prgCfg.send_log) > 0)
  {
     pgSendLogFile = fopen(prgCfg.send_log,"w");
     if (!pgSendLogFile)
     {
        dbg(TRACE,"Init_bhsif : SendLog: <%> not opened! fopen() returns <%s>.",
            prgCfg.send_log,strerror(errno));
     }
  }
  tgRecvTimeOut = time(0) + (time_t)(igRecvTimeout - READ_TIMEOUT);


  ilRc = TimeToStr(pcgCurrentTime,time(NULL));
  strcpy(pcgNextKeepAlive,pcgCurrentTime);

  igSavInsCnt = 0;
  igSavDelCnt = 0;
  igSavRetCnt = 0;

  return(ilRc);
} /* end of initialize */
/*********************************************************************
Function : GetQueues()
Paramter :
Return Code: RC_SUCCESS,RC_FAIL
Result:
Description: Gets all necessary queue-ID's for CEDA-internal
             communication!
*********************************************************************/
static int GetQueues()
{
  int ilRc = RC_FAIL;

  /* get mod-id of router */
  if ((igModID_Router = tool_get_q_id("router")) == RC_NOT_FOUND ||
      igModID_Router == RC_FAIL || igModID_Router == 0)
    {
      dbg(TRACE,"GetQueues   : tool_get_q_id(router) returns: <%d>",igModID_Router);
      ilRc = RC_FAIL;
    }else{
      dbg(TRACE,"GetQueues   : <router> mod_id <%d>",igModID_Router);
      ilRc = RC_SUCCESS;
    } 
  return ilRc;
}

static int CheckCfg(void)
{
  int ilRc = RC_SUCCESS;
  return ilRc;
}
/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{

  int    ilRc = RC_SUCCESS;    /* Return code */
    
  dbg(TRACE,"Reset: now reseting ...");

  return ilRc;
    
} /* end of Reset */
/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
  dbg(TRACE,"Terminate: now leaving ...");

  sleep(ipSleep);
  
  exit(0);
    
} /* end of Terminate */
/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
  switch(pipSig)
    {
    case SIGALRM:
      bgAlarm = TRUE;
      break;
    case SIGPIPE:
      if (igSock != 0 || igSockCreate != 0)
         CloseTCP();
      break;
    case SIGCHLD:
      break;
    case SIGTERM:
      Terminate(1);
      break;
    default    :
      Terminate(10);
      break;
    } /* end of switch */
} /* end of HandleSignal */
/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
  /*    int    ilRc = RC_SUCCESS; */
    return;
} /* end of HandleErr */
/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    int    ilRc = RC_SUCCESS;
    
    switch(pipErr) {
    case    QUE_E_FUNC    :    /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :    /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND    :    /* Error using msgsnd */
            dbg(TRACE,"<%d> : msgsnd failed",pipErr);
            break;
    case    QUE_E_GET    :    /* Error using msgrcv */
            if(pipErr != 4)        
             dbg(DEBUG,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX    : 
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :   unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE    :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES    :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP    :    /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL    :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG    :    /* No message on queue */
        /*dbg(TRACE,"<%d> : no messages on queue",pipErr);*/
        break;
    case    QUE_E_INVORG    :    /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :    /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    case    QUE_E_PRIORITY    :
        dbg(TRACE,"<%d> : wrong priority was send ",pipErr);
        break;
    default            :    /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */
/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
  int    ilRc = RC_SUCCESS;            /* Return code */
  int    ilBreakOut = FALSE;
    
  do{
    memset(prgItem,0x00,igItemLen);
    ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
    /* depending on the size of the received item  */
    /* a realloc could be made by the que function */
    /* so do never forget to set event pointer !!! */
    prgEvent = (EVENT *) prgItem->text;    
    if( ilRc == RC_SUCCESS )
      {
    /* Acknowledge the item */
    ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
    if( ilRc != RC_SUCCESS ) 
      {
        /* handle que_ack error */
        HandleQueErr(ilRc);
      } /* fi */
        
    switch( prgEvent->command )
      {
      case    HSB_STANDBY    :
        ctrl_sta = prgEvent->command;
        break;    
      case    HSB_COMING_UP    :
        ctrl_sta = prgEvent->command;
        break;    
      case    HSB_ACTIVE    :
        ctrl_sta = prgEvent->command;
        ilBreakOut = TRUE;
        break;    
      case    HSB_ACT_TO_SBY    :
        ctrl_sta = prgEvent->command;
        break;    
      case    HSB_DOWN    :
        /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
        ctrl_sta = prgEvent->command;
        Terminate(10);
        break;    
      case    HSB_STANDALONE    :
        ctrl_sta = prgEvent->command;
        ResetDBCounter();
        ilBreakOut = TRUE;
        break;    
      case    REMOTE_DB :
        /* ctrl_sta is checked inside */
        HandleRemoteDB(prgEvent);
        break;
      case    SHUTDOWN    :
        Terminate(1);
        break;
      case    RESET        :
        ilRc = Reset();
        break;
      case    EVENT_DATA    :
        dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
        DebugPrintItem(TRACE,prgItem);
        DebugPrintEvent(TRACE,prgEvent);
        break;
      case    TRACE_ON :
        dbg_handle_debug(prgEvent->command);
        break;
      case    TRACE_OFF :
        dbg_handle_debug(prgEvent->command);
        break;
      default            :
        dbg(TRACE,"HandleQueues: unknown event");
        DebugPrintItem(TRACE,prgItem);

        DebugPrintEvent(TRACE,prgEvent);

        break;
      } /* end switch */
      }else{
    /* Handle queuing errors */
    HandleQueErr(ilRc);
      } /* end else */
  } while (ilBreakOut == FALSE);
  if(igInitOK == FALSE)
    {
      ilRc = Init_dgshdl();
      if(ilRc == RC_SUCCESS)
    {
      dbg(TRACE,"HandleQueues: Init_dgshdl() OK!");
      igInitOK = TRUE;
    }else{ /* end of if */
      dbg(TRACE,"HandleQueues: Init_dgshdl() failed!");
      igInitOK = FALSE;
    } /* end of if */
    }/* end of if */
  /* OpenConnection(); */
} /* end of HandleQueues */
/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleInternalData()
{
  int  ilRC = RC_SUCCESS;      /* Return code */
  char *pclSelection = NULL;
  char *pclFields = NULL;
  char *pclData = NULL;
  char pclNewData[512000];
  char pclOldData[512000];
  char pclUrno[50];
  char *pclTmpPtr=NULL;
  BC_HEAD *bchd = NULL;          /* Broadcast header*/
  CMDBLK  *cmdblk = NULL; 
  char pclTmpInitFog[32];
  char pclRegn[32];
  char pclStartTime[32];
  int ilLen;

  bchd  = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  cmdblk= (CMDBLK  *) ((char *)bchd->data);

  ilRC = iGetConfigEntry(pcgConfigFile,"DGSHDL","TWS",CFG_STRING,pcgTwStart);
  if (ilRC != RC_SUCCESS)
     strcpy(pcgTwStart,cmdblk->tw_start);
  strcpy(pcgTwEnd,cmdblk->tw_end);

  /***********************************/
  /*    DebugPrintItem(DEBUG,prgItem);   */
  /*    DebugPrintEvent(DEBUG,prgEvent); */
  /***********************************/
  memset(pclOldData,0x00,L_BUFF);
  pclSelection = cmdblk->data;
  pclFields = (char *)pclSelection + strlen(pclSelection) + 1;
  pclData = (char *)pclFields + strlen(pclFields) + 1;

  strcpy(pclNewData,pclData);
  *pclUrno = '\0';
  pclTmpPtr = strstr(pclSelection,"\n");
  if (pclTmpPtr != NULL)
  {
     *pclTmpPtr = '\0';
     pclTmpPtr++;
     strcpy(pclUrno,pclTmpPtr);
  }

  ilRC = TimeToStr(pcgCurrentTime,time(NULL));

  dbg(DEBUG,"========================= START ===============================");
  if (strcmp(cmdblk->command,"DGSI") == 0)
  {
     dbg(DEBUG,"Command:   <%s>",cmdblk->command);
     dbg(DEBUG,"Selection: <%s><%s>",pclSelection,pclUrno);
     dbg(DEBUG,"Fields:    <%s>",pclFields);
     dbg(DEBUG,"Data:      \n<%s>",pclNewData);
     dbg(DEBUG,"TwStart:   <%s>",pcgTwStart);
     dbg(DEBUG,"TwEnd:     <%s>",pcgTwEnd);
     ilRC = CheckXmlInput(pclNewData);
     if (ilRC == RC_SUCCESS)
        ilRC = HandleInput(pclNewData,-1,-1);
  }
  dbg(DEBUG,"========================= END ================================");
  return ilRC;
} /* end of HandleInternalData */
/*********************************************************************
Function : SendEvent()
Paramter : IN: pcpCmd = command for cmdblk->command
           IN: ipModID = process-ID where the event is send to
       IN: ipPriority = priority for sending ( 1- 5, usuallay 3)
       IN: pcpTable = Name (3 letters) of cmdblk->obj_name (if
           necessary), will be expanded with "pcgTabEnd".
       IN: pcpTwStart = cmdblk->twstart
       IN: pcpTwEnd = cmdblk->twend (always HOMEAP,TABEND,processname)
       IN: pcpSelection = selection for event (cmdblk->data)
       IN: pcpFields = fieldlist (corresponding to pcpdata)
       IN: pcpData = datalist (comma separated, corresponding to 
                                   pcpFields)
       IN: pcpAddStruct = additional structure to be transmitted
       IN: ipAddStructSize = size of the additional structure
Return Code: RC_SUCCESS, RC_FAIL
Result:
Description: Sends an event to another CEDA-process using que(QUE_PUT).
             Sends the event in standard CEDA-format (BCHEAD,CMDBLK,
         selection,fieldlist,datalist) or sends a different
         data structure (special) at CMDBLK->data. !! Sends always
         only one type, standard OR special, with one event !!
*********************************************************************/
static int SendEvent(char *pcpCmd,int ipModID,int ipPriority,char *pcpTable,
             char *pcpTwStart, char *pcpTwEnd,
             char *pcpSelection,char *pcpFields,char *pcpData,
             char *pcpAddStruct,int ipAddStructSize)
{
  int     ilRc             = RC_FAIL;
  int     ilLen            = 0;
  EVENT   *prlOutEvent  = NULL;
  BC_HEAD *prlOutBCHead = NULL;
  CMDBLK  *prlOutCmdblk = NULL;

  if (pcpAddStruct == NULL)
    ipAddStructSize = 0;
  if (ipAddStructSize == 0)
    pcpAddStruct = NULL;

  /* size-calculation for prlOutEvent */
  ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 
    strlen(pcpSelection) + strlen(pcpFields) + strlen(pcpData) + 
    ipAddStructSize + 128;

  /* memory for prlOutEvent */
  if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
    {
      dbg(TRACE,"SendEvent: cannot malloc <%d>-bytes for outgoing event!",ilLen);
      prlOutEvent = NULL;
    }else{
      /* clear whole outgoing event */
      memset((void*)prlOutEvent, 0x00, ilLen);

      /* set event structure... */
      prlOutEvent->type         = SYS_EVENT;
      prlOutEvent->command    = EVENT_DATA;
      prlOutEvent->originator   = (short)mod_id;
      prlOutEvent->retry_count  = 0;
      prlOutEvent->data_offset  = sizeof(EVENT);
      prlOutEvent->data_length  = ilLen - sizeof(EVENT); 

      /* BC_HEAD-Structure... */
      prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
      /* prlOutBCHead->rc = (short)RC_SUCCESS;*/
      prlOutBCHead->rc = (short)NETOUT_NO_ACK;/*spaeter nur bei disconnect*/
      strncpy(prlOutBCHead->dest_name,mod_name,10);
      strncpy(prlOutBCHead->recv_name, "EXCO",10);
 
      /* Cmdblk-Structure... */
      prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data);
      strcpy(prlOutCmdblk->command,pcpCmd);
      if (pcpTable != NULL)
    {
      strcpy(prlOutCmdblk->obj_name,pcpTable);
      strcat(prlOutCmdblk->obj_name,pcgTabEnd);
    }
        
      /* setting tw_x entries */
      strncpy(prlOutCmdblk->tw_start,pcpTwStart,32);
      strncpy(prlOutCmdblk->tw_end,pcpTwEnd,32);
        
      /* means that no additional structure is used */
      /* STANDARD CEDA-ipcs between CEDA-processes */
      if (pcpAddStruct == NULL)
    {
      /* setting selection inside event */
      strcpy(prlOutCmdblk->data,pcpSelection);
      /* setting field-list inside event */
      strcpy(prlOutCmdblk->data+strlen(pcpSelection)+1,pcpFields);
      /* setting data-list inside event */
      strcpy((prlOutCmdblk->data + (strlen(pcpSelection)+1) + (strlen(pcpFields)+1)),pcpData);
    }else{
      /*an additional structure is used and will be copied to */
      /*cmdblk + sizeof(CMDBLK).!!! No STANDARD CEDA-ipcs is used !!!! */
      memcpy(prlOutCmdblk->data,(char*)pcpAddStruct,ipAddStructSize);    
    }

      /*DebugPrintEvent(DEBUG,prlOutEvent);*/ 
      /*snapit((char*)prlOutEvent,ilLen,outp);*/
      dbg(DEBUG,"SendEvent: sending event to mod_id <%d>",ipModID);

      if (ipModID != 0)
    {
      if ((ilRc = que(QUE_PUT,ipModID,mod_id,ipPriority,ilLen,(char*)prlOutEvent))
          != RC_SUCCESS)
        {
          dbg(TRACE,"SendEvent: QUE_PUT returns: <%d>", ilRc);
          Terminate(1);
        }
    }else{
      dbg(TRACE,"SendEvent: mod_id = <%d>! Can't send!",ipModID);
    }
      /* free memory */
      free((void*)prlOutEvent); 
    }
  return ilRc;
}

/*
	Get Config Entries
*/
static int GetConfig()
{
  int ilRC = RC_SUCCESS;
  int ilI;
  int ilJ;
  char pclTmpBuf[128];
  char pclDebugLevel[128];
  int ilLen;
  char pclBuffer[10];
  char pclClieBuffer[100];
  char pclServBuffer[100];

  sprintf(pcgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
  dbg(TRACE,"Config File is <%s>",pcgConfigFile);

  ilRC = iGetConfigEntry(pcgConfigFile,"DGSHDL","CDStart",CFG_STRING,pclTmpBuf);
  if (ilRC == RC_SUCCESS)
     strcpy(pcgCDStart,pclTmpBuf);
  else
     strcpy(pcgCDStart,"<![CDATA[");
  ilRC = iGetConfigEntry(pcgConfigFile,"DGSHDL","CDEnd",CFG_STRING,pclTmpBuf);
  if (ilRC == RC_SUCCESS)
     strcpy(pcgCDEnd,pclTmpBuf);
  else
     strcpy(pcgCDEnd,"]]>");

  ilRC = iGetConfigEntry(pcgConfigFile,"DGSHDL","TIME_WINDOW_ARR_BEGIN",CFG_STRING,pclTmpBuf);
  if (ilRC == RC_SUCCESS)
     igTimeWindowArrBegin = atoi(pclTmpBuf);
  else
     igTimeWindowArrBegin = 0;
  ilRC = iGetConfigEntry(pcgConfigFile,"DGSHDL","TIME_WINDOW_ARR_END",CFG_STRING,pclTmpBuf);
  if (ilRC == RC_SUCCESS)
     igTimeWindowArrEnd = atoi(pclTmpBuf);
  else
     igTimeWindowArrEnd = 0;
  ilRC = iGetConfigEntry(pcgConfigFile,"DGSHDL","TIME_WINDOW_DEP_BEGIN",CFG_STRING,pclTmpBuf);
  if (ilRC == RC_SUCCESS)
     igTimeWindowDepBegin = atoi(pclTmpBuf);
  else
     igTimeWindowDepBegin = 0;
  ilRC = iGetConfigEntry(pcgConfigFile,"DGSHDL","TIME_WINDOW_DEP_END",CFG_STRING,pclTmpBuf);
  if (ilRC == RC_SUCCESS)
     igTimeWindowDepEnd = atoi(pclTmpBuf);
  else
     igTimeWindowDepEnd = 0;

  ilRC = iGetConfigEntry(pcgConfigFile,"DGSHDL","MAX_TIME_DIFF_FOR_UPDATE",CFG_STRING,pclTmpBuf);
  if (ilRC == RC_SUCCESS)
     igMaxTimeDiffForUpdate = atoi(pclTmpBuf);
  else
     igMaxTimeDiffForUpdate = 0;

  ilRC = iGetConfigEntry(pcgConfigFile,"DGSHDL","DEBUG_LEVEL",CFG_STRING,pclDebugLevel);
  if (strcmp(pclDebugLevel,"DEBUG") == 0)
  {
     debug_level = DEBUG;
  }
  else
  {
     if (strcmp(pclDebugLevel,"TRACE") == 0)
     {
        debug_level = TRACE;
     }
     else
     {
        if (strcmp(pclDebugLevel,"NULL") == 0)
        {
           debug_level = 0;
        }
        else
        {
           debug_level = TRACE;
        }
     }
  }

  dbg(TRACE,"CDStart = %s",pcgCDStart);
  dbg(TRACE,"CDEnd   = %s",pcgCDEnd);
  dbg(TRACE,"TIME_WINDOW_ARR_BEGIN = %d",igTimeWindowArrBegin);
  dbg(TRACE,"TIME_WINDOW_ARR_END   = %d",igTimeWindowArrEnd);
  dbg(TRACE,"TIME_WINDOW_DEP_BEGIN = %d",igTimeWindowDepBegin);
  dbg(TRACE,"TIME_WINDOW_DEP_END   = %d",igTimeWindowDepEnd);
  dbg(TRACE,"MAX_TIME_DIFF_FOR_UPDATE   = %d",igMaxTimeDiffForUpdate);
  dbg(TRACE,"DEBUG_LEVEL = %s",pclDebugLevel);

  /* Get Client and Server chars */
  sprintf(pcgUfisConfigFile,"%s/ufis_ceda.cfg",getenv("CFG_PATH")); 
  ilRC = iGetConfigEntry(pcgUfisConfigFile,"CHAR_PATCH","CLIENT_CHARS",
                         CFG_STRING, pclClieBuffer);
  if (ilRC == RC_SUCCESS)
  {
     ilRC = iGetConfigEntry(pcgUfisConfigFile,"CHAR_PATCH","SERVER_CHARS",
                            CFG_STRING, pclServBuffer);
     if (ilRC == RC_SUCCESS)
     {
        ilI = GetNoOfElements(pclClieBuffer,',');
        if (ilI == GetNoOfElements(pclServBuffer,','))	
        {
           memset(pcgServerChars,0x00,100*sizeof(char));
           memset(pcgClientChars,0x00,100*sizeof(char));
           for (ilJ=0; ilJ < ilI; ilJ++) 
           {
              GetDataItem(pclBuffer,pclServBuffer,ilJ+1,',',"","\0\0");
              pcgServerChars[ilJ*2] = atoi(pclBuffer);	
              pcgServerChars[ilJ*2+1] = ',';
           }
           pcgServerChars[(ilJ-1)*2+1] = 0x00;
           for (ilJ=0; ilJ < ilI; ilJ++) 
           {
              GetDataItem(pclBuffer,pclClieBuffer,ilJ+1,',',"","\0\0");
              pcgClientChars[ilJ*2] = atoi(pclBuffer);	
              pcgClientChars[ilJ*2+1] = ',';
           }
           pcgClientChars[(ilJ-1)*2+1] = 0x00;
           dbg(DEBUG,"New Clientchars <%s> dec <%s>",pcgClientChars,pclClieBuffer);
           dbg(DEBUG,"New Serverchars <%s> dec <%s>",pcgServerChars,pclServBuffer);
           dbg(DEBUG,"Serverchars <%d> ",strlen(pcgServerChars));
           dbg(DEBUG,"%s  and count <%d> ",pclBuffer,strlen(pcgServerChars));
           dbg(DEBUG,"ilI <%d>",ilI);
        }
     }
     else
     {
        ilRC = RC_SUCCESS;
        dbg(DEBUG,"Use standard (old) serverchars");	
     }
  }
  else
  {
     dbg(DEBUG,"Use standard (old) serverchars");	
     ilRC = RC_SUCCESS;
  }
  if (ilRC != RC_SUCCESS)
  {
     strcpy(pcgClientChars,"\042\047\054\012\015");
     strcpy(pcgServerChars,"\260\261\262\263\263");
     ilRC = RC_SUCCESS;
  }

  dbg(TRACE,"Now read parameters for interface");
  if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","MODE",CFG_STRING,&prgCfg.mode,CFG_ALPHA,"REAL"))
                        != RC_SUCCESS)
     return RC_FAIL;

  if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","FN_ONBL",CFG_STRING,&prgCfg.fn_onbl,CFG_PRINT,"ONBE"))
                        != RC_SUCCESS)
     { }
  if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","FN_OFBL",CFG_STRING,&prgCfg.fn_ofbl,CFG_PRINT,"OFBE"))
                        != RC_SUCCESS)
     { }
  if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","DGS_HOST1",CFG_STRING,&prgCfg.dgs_host1,CFG_PRINT,""))
                        != RC_SUCCESS)
     return RC_FAIL;
  if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","DGS_HOST2",CFG_STRING,&prgCfg.dgs_host2,CFG_PRINT,""))
                        != RC_SUCCESS)
     return RC_FAIL;
  if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","SERVICE_PORT",CFG_STRING,&prgCfg.service_port,CFG_PRINT,"EXCO_DGS"))
                        != RC_SUCCESS)
     return RC_FAIL;
  if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","WAIT_FOR_ACK",CFG_STRING,&prgCfg.wait_for_ack,CFG_NUM,"2"))
                        != RC_SUCCESS)
     return RC_FAIL;
  else
     igWaitForAck = atoi(prgCfg.wait_for_ack);
  if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","RECV_TIMEOUT",CFG_STRING,&prgCfg.recv_timeout,CFG_NUM,"60"))
                        != RC_SUCCESS)
     return RC_FAIL;
  else
     igRecvTimeout = atoi(prgCfg.recv_timeout);
  if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","MAX_SENDS",CFG_STRING,&prgCfg.max_sends,CFG_NUM,"2"))
                        != RC_SUCCESS)
     return RC_FAIL;
  else
     igMaxSends = atoi(prgCfg.max_sends);
  if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","RECV_LOG",CFG_STRING,&prgCfg.recv_log,CFG_PRINT,"/ceda/debug/dgshdl_recv.log"))
                        != RC_SUCCESS)
     return RC_FAIL;
  if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","SEND_LOG",CFG_STRING,&prgCfg.send_log,CFG_PRINT,"/ceda/debug/dgshdl_send.log"))
                        != RC_SUCCESS)
     return RC_FAIL;
  if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","TRY_RECONNECT",CFG_STRING,&prgCfg.try_reconnect,CFG_NUM,"60"))
                        != RC_SUCCESS)
     return RC_FAIL;
  if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","KEEP_ALIVE",CFG_STRING,&prgCfg.keep_alive,CFG_NUM,"60"))
                        != RC_SUCCESS)
     return RC_FAIL;
  else
     igKeepAlive = atoi(prgCfg.keep_alive);
  if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","FTP_CLIENT_OS",CFG_STRING,&prgCfg.ftp_client_os,CFG_ALPHA,"UNIX"))
                        != RC_SUCCESS)
     return RC_FAIL;
  if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","FTP_USER",CFG_STRING,&prgCfg.ftp_user,CFG_ALPHANUM,"none"))
                        != RC_SUCCESS)
     return RC_FAIL;
  if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","FTP_PASS",CFG_STRING,&prgCfg.ftp_pass,CFG_IGNORE,"none"))
                        != RC_SUCCESS)
     return RC_FAIL;
  if ((ilRC=GetCfgEntry(pcgConfigFile,"MAIN","FTP_TIMEOUT",CFG_STRING,&prgCfg.ftp_timeout,CFG_NUM,"1"))
                        != RC_SUCCESS)
     igFtpTimeout=1;
  else
     igFtpTimeout=atoi(prgCfg.ftp_timeout);

  ilRC = GetXMLSchema();

  return RC_SUCCESS;
} /* Enf of GetConfig */

/******************************************************************************/
/* The TimeToStr routine                                                      */
/******************************************************************************/
static int TimeToStr(char *pcpTime,time_t lpTime) 
{                                                                              
  struct tm *_tm;
    
  _tm = (struct tm *)gmtime(&lpTime);

  sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
          _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
          _tm->tm_min,_tm->tm_sec);

  return RC_SUCCESS;  
                        
}     /* end of TimeToStr */


static int TriggerBchdlAction(char *pcpCmd, char *pcpTable, char *pcpSelection,
                              char *pcpFields, char *pcpData, int ipBchdl, int ipAction)
{
  int ilRC = RC_SUCCESS;

  if (ipBchdl == TRUE)
  {
     (void) tools_send_info_flag(1900,0,"dgshdl","","CEDA","","",pcgTwStart,pcgTwEnd,
                                 pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData,0);
     dbg(DEBUG,"Send Broadcast: <%s><%s><%s><%s><%s>",
         pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData);
  }
  if (ipAction == TRUE)
  {
     (void) tools_send_info_flag(7400,0,"dgshdl","","CEDA","","",pcgTwStart,pcgTwEnd,
                                 pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData,0);
     dbg(DEBUG,"Send to ACTION: <%s><%s><%s><%s><%s>",
         pcpCmd,pcpTable,pcpSelection,pcpFields,pcpData);
  }

  return ilRC;
} /* End of TriggerBchdlAction */


/*******************************************++++++++++*************************/
/*   GetSecondsFromCEDATime                                                   */
/*                                                                            */
/*   Serviceroutine, rechnet einen Datumseintrag im Cedaformat in Seconds um  */
/*   Input:  char *  Zeiger auf CEDA-Zeitpuffer                               */
/*   Output: long    Zeitwert                                                 */
/******************************************************************************/

static long GetSecondsFromCEDATime(char *pcpDateTime)
{
  long rc = 0;
  int year;
  char ch_help[5];
  struct tm *tstr1 = NULL, t1;
  struct tm *CurTime, t2;
  time_t llTime;
  time_t llUtcTime;
  time_t llLocalTime;

  CurTime = &t2;
  llTime = time(NULL);
  CurTime = (struct tm *)gmtime(&llTime);
  CurTime->tm_isdst = 0;
  llUtcTime = mktime(CurTime);
  CurTime = (struct tm *)localtime(&llTime);
  CurTime->tm_isdst = 0;
  llLocalTime = mktime(CurTime);
  igDiffUtcToLocal = llLocalTime - llUtcTime;
  /* dbg(TRACE,"UTC: <%ld> , Local: <%ld> , Diff: <%ld>",llUtcTime,llLocalTime,igDiffUtcToLocal); */

  memset(&t1,0x00,sizeof(struct tm));

  tstr1 = &t1;

  memset(ch_help,0x00,5);
  strncpy(ch_help,pcpDateTime,4);
  year = atoi(ch_help);
  tstr1->tm_year = year - 1900;

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[4],2);
  tstr1->tm_mon = atoi(ch_help) -1;

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[6],2);
  tstr1->tm_mday = atoi(ch_help);

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[8],2);
  tstr1->tm_hour = atoi(ch_help);

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[10],2);
  tstr1->tm_min = atoi(ch_help);

  memset(ch_help,0x00,5);
  strncpy(ch_help,&pcpDateTime[12],2);
  tstr1->tm_sec = atoi(ch_help);

  tstr1->tm_wday = 0;
  tstr1->tm_yday = 0;
  tstr1->tm_isdst = 0;
  rc = mktime(tstr1);

  rc = rc + igDiffUtcToLocal; /* add difference between local and utc */

  return(rc);
} /* end of GetSecondsFromCEDATime() */


static void TrimRight(char *pcpBuffer)
{
  char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];

  if (strlen(pcpBuffer) == 0)
  {
     strcpy(pcpBuffer, " ");
  }
  else
  {
     while (isspace(*pclBlank) && pclBlank != pcpBuffer)
     {
        *pclBlank = '\0';
        pclBlank--;
     }
  }
} /* End of TrimRight */


static void TrimLeftRight(char *pcpBuffer)
{
  char *pclBlank = &pcpBuffer[0];

  if (strlen(pcpBuffer) == 0)
  {
     strcpy(pcpBuffer, " ");
  }
  else
  {
     while (isspace(*pclBlank) && *pclBlank != '\0')
     {
        pclBlank++;
     }
     strcpy(pcpBuffer,pclBlank);
     TrimRight(pcpBuffer);
  }
} /* End of TrimLeftRight */


static void CheckCDATA(char *pcpBuffer)
{
  char pclTmpBuf[512000];
  char *pclTmpPtr1;
  char *pclTmpPtr2;

  memset(pclTmpBuf,0x00,512000);
  pclTmpPtr1 = strstr(pcpBuffer,pcgCDStart);
  if (pclTmpPtr1 != NULL)
  {
     if (pclTmpPtr1 != pcpBuffer)
     {
        strncpy(pclTmpBuf,pcpBuffer,pclTmpPtr1 - pcpBuffer);
     }
     pclTmpPtr1 += strlen(pcgCDStart);
     pclTmpPtr2 = strstr(pclTmpPtr1,pcgCDEnd);
     if (pclTmpPtr2 == NULL)
        strcat(pclTmpBuf,pclTmpPtr1);
     else
     {
        strncat(pclTmpBuf,pclTmpPtr1,pclTmpPtr2 - pclTmpPtr1);
        pclTmpPtr2 += strlen(pcgCDEnd);
        if (*pclTmpPtr2 != '\0')
           strcat(pclTmpBuf,pclTmpPtr2);
     }
  strcpy(pcpBuffer,pclTmpBuf);
  }
} /* End of CheckCDATA */


static int MyAddSecondsToCEDATime(char *pcpTime, long lpValue, int ipP)
{
  int ilRC = RC_SUCCESS;

  if (strlen(pcpTime) == 12)
     strcat(pcpTime,"00");
  ilRC = AddSecondsToCEDATime(pcpTime,lpValue,ipP);
  return ilRC;
} /* End of MyAddSecondsToCEDATime */


static char *GetLine(char *pcpLine, int ipLineNo)
{
  char *pclResult = NULL;
  char *pclP = pcpLine;
  int ilCurLine;

  ilCurLine = 1;
  while (*pclP != '\0' && ilCurLine < ipLineNo)
  {
     if (*pclP == '\n')
        ilCurLine++;
     pclP++;
  }
  if (*pclP != '\0')
     pclResult = pclP;

  return pclResult;
} /* End of GetLine */


static void CopyLine(char *pcpDest, char *pcpSource)
{
  char *pclP1 = pcpDest;
  char *pclP2 = pcpSource;

  while (*pclP2 != '\n' && *pclP2 != '\0')
  {
     *pclP1 = *pclP2;
     pclP1++;
     pclP2++;
  }
  *pclP1 = '\0';
  TrimRight(pcpDest);
} /* End of CopyLine */


static int GetXMLSchema()
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "GetXMLSchema:";
  char pclCfgFile[128];
  FILE *fp;
  int ilCurLine;
  char pclLine[2048];
  int ilNoEle;
  int ilI;
  int ilCurLevel;
  char pclTag[16];
  char pclPrevTag[16];
  char pclName[1024];
  char pclType[8];
  char pclBasTab[8];
  char pclBasFld[8];
  char pclFlag[64];
  int ilInfoFldFlag;
  char pclNameStack[100][1024];
  char pclItem1[32];
  char pclItem2[32];
  char pclItem3[32];
  int ilJ;
  int ilNoMethod;
  char pclTmpBuf[128];

  ilRC = iGetConfigEntry(pcgConfigFile,"DGSHDL","XML_SCHEMA_FILE",CFG_STRING,pclTmpBuf);
  if (ilRC == RC_SUCCESS)
     sprintf(pclCfgFile,"%s/%s",getenv("CFG_PATH"),pclTmpBuf);
  else
     sprintf(pclCfgFile,"%s/%s.csv",getenv("CFG_PATH"),mod_name);
  dbg(TRACE,"%s XML Config File is <%s>",pclFunc,pclCfgFile);
  if ((fp = (FILE *)fopen(pclCfgFile,"r")) == (FILE *)NULL)
  {
     dbg(TRACE,"%s XML Config File <%s> does not exist",pclFunc,pclCfgFile);
     return RC_FAIL;
  }

  igCurInterface = 0;
  memset((char *)&rgXml.rlLine[0].ilLevel,0x00,sizeof(_LINE)*MAX_XML_LINES);
  strcpy(pclPrevTag,"");
  ilInfoFldFlag = FALSE;
  igCurXmlLines = 0;
  ilCurLevel = 0;
  ilCurLine = 1;
  while (fgets(pclLine,2048,fp))
  {
     pclLine[strlen(pclLine)-1] = '\0';
     ilNoEle = GetNoOfElements(pclLine,';');
     dbg(DEBUG,"%s Current Line = (%d)<%s>",pclFunc,ilNoEle,pclLine);
     get_item(1,pclLine,pclTag,0,";","\0","\0");
     TrimRight(pclTag);
     get_item(2,pclLine,pclName,0,";","\0","\0");
     TrimRight(pclName);
     get_item(3,pclLine,pclType,0,";","\0","\0");
     TrimRight(pclType);
     get_item(4,pclLine,pclBasTab,0,";","\0","\0");
     TrimRight(pclBasTab);
     get_item(5,pclLine,pclBasFld,0,";","\0","\0");
     TrimRight(pclBasFld);
     get_item(6,pclLine,pclFlag,0,";","\0","\0");
     TrimRight(pclFlag);
     if (strcmp(pclTag,"TITLE") != 0)
     {
        if (strcmp(pclTag,"STR_BGN") == 0)
        {
           if (strcmp(pclPrevTag,"STR_END") != 0)
              ilCurLevel++;
           ilInfoFldFlag = FALSE;
           strcpy(&pclNameStack[ilCurLevel-1][0],pclName);
        }
        else
        {
           if (strcmp(pclTag,"STR_END") == 0)
           {
              ilCurLevel--;
              ilInfoFldFlag = FALSE;
              if (strcmp(pclName,&pclNameStack[ilCurLevel-1][0]) != 0)
              {
                 dbg(TRACE,"%s Wrong Name at STR_END <%s><%s> specified!!! Line = %d",
                     pclFunc,pclName,&pclNameStack[ilCurLevel-1][0],ilCurLine);
                 return ilRC;
              }
           }
           else
           {
              if (strcmp(pclTag,"HDR") == 0)
              {
                 if (ilInfoFldFlag == FALSE)
                 {
                    ilCurLevel++;
                    ilInfoFldFlag = TRUE;
                 }
              }
              else
              {
                 if (strcmp(pclTag,"KEY") == 0)
                 {
                    if (ilInfoFldFlag == FALSE)
                    {
                       ilCurLevel++;
                       ilInfoFldFlag = TRUE;
                    }
                 }
                 else
                 {
                    if (strcmp(pclTag,"DAT") == 0)
                    {
                       if (ilInfoFldFlag == FALSE)
                       {
                          ilCurLevel++;
                          ilInfoFldFlag = TRUE;
                       }
                    }
                    else
                    {
                       dbg(TRACE,"%s Wrong Tag <%s> specified!!!",pclFunc,pclTag);
                       return ilRC;
                    }
                 }
              }
           }
        }
        rgXml.rlLine[igCurXmlLines].ilLevel = ilCurLevel;
        strcpy(&rgXml.rlLine[igCurXmlLines].pclTag[0],pclTag);
        strcpy(&rgXml.rlLine[igCurXmlLines].pclName[0],pclName);
        strcpy(&rgXml.rlLine[igCurXmlLines].pclType[0],pclType);
        if (strcmp(pcgMode,"OUTPUT") == 0 && strcmp(pclTag,"STR_BGN") != 0 && strcmp(pclTag,"STR_END") != 0)
        {
           if (*pclBasTab == ' ')
              strcpy(&rgXml.rlLine[igCurXmlLines].pclBasTab[0],pclName);
           else
              strcpy(&rgXml.rlLine[igCurXmlLines].pclBasTab[0],pclBasTab);
           if (*pclBasFld == ' ')
              strcpy(&rgXml.rlLine[igCurXmlLines].pclBasFld[0],pclName);
           else
              strcpy(&rgXml.rlLine[igCurXmlLines].pclBasFld[0],pclBasFld);
        }
        else
        {
           strcpy(&rgXml.rlLine[igCurXmlLines].pclBasTab[0],pclBasTab);
           strcpy(&rgXml.rlLine[igCurXmlLines].pclBasFld[0],pclBasFld);
        }
        strcpy(&rgXml.rlLine[igCurXmlLines].pclFlag[0],pclFlag);
        ilNoEle = GetNoOfElements(pclLine,';');
        for (ilI = 7; ilI <= ilNoEle; ilI += 3)
        {
           get_item(ilI,pclLine,pclItem1,0,";","\0","\0");
           TrimRight(pclItem1);
           get_item(ilI+1,pclLine,pclItem2,0,";","\0","\0");
           TrimRight(pclItem2);
           get_item(ilI+2,pclLine,pclItem3,0,";","\0","\0");
           TrimRight(pclItem3);
           if (*pclItem1 != ' ')
           {
              if (strcmp(&rgXml.rlLine[igCurXmlLines].pclTag[0],"HDR") == 0 &&
                  strcmp(&rgXml.rlLine[igCurXmlLines].pclName[0],"MESSAGEORIGIN") == 0)
              {
                 strcpy(&rgInterface[igCurInterface].pclIntfName[0],pclItem1);
                 rgInterface[igCurInterface].ilIntfIdx = (ilI - 7) / 3;
                 igCurInterface++;
              }
              else
              {
                 strcpy(&rgXml.rlLine[igCurXmlLines].pclMethod[(ilI-7)/3][0],pclItem1);
                 rgXml.rlLine[igCurXmlLines].ilNoMethod++;
              }
           }
           if (*pclItem2 != ' ')
           {
              strcpy(&rgXml.rlLine[igCurXmlLines].pclFieldA[(ilI-7)/3][0],pclItem2);
              rgXml.rlLine[igCurXmlLines].ilNoFieldA++;
           }
           if (*pclItem3 != ' ')
           {
              strcpy(&rgXml.rlLine[igCurXmlLines].pclFieldD[(ilI-7)/3][0],pclItem3);
              rgXml.rlLine[igCurXmlLines].ilNoFieldD++;
           }
        }
        igCurXmlLines++;
        ilCurLine++;
        strcpy(pclPrevTag,pclTag);
     }
  }
  fclose(fp);
  if (strcmp(pcgMode,"INPUT") == 0)
  {
     strcpy(pclItem1,"");
     ilNoMethod = 0;
     for (ilI = 0; ilI < MAX_INTERFACES; ilI++)
     {
        for (ilJ = 0; ilJ < igCurXmlLines; ilJ++)
        {
           if (strlen(&rgXml.rlLine[ilJ].pclMethod[ilI][0]) > 0)
           {
              if (strcmp(&rgXml.rlLine[ilJ].pclTag[0],"HDR") == 0 &&
                  strcmp(&rgXml.rlLine[ilJ].pclName[0],"ACTIONTYPE") == 0)
              {
              }
              else
              {
                 strcpy(pclItem1,&rgXml.rlLine[ilJ].pclMethod[ilI][0]);
                 ilNoMethod = rgXml.rlLine[ilJ].ilNoMethod;
              }
           }
           else
           {
              strcpy(&rgXml.rlLine[ilJ].pclMethod[ilI][0],pclItem1);
              rgXml.rlLine[ilJ].ilNoMethod = ilNoMethod;
           }
        }
     }
  }
  for (ilI = 0; ilI < igCurXmlLines; ilI++)
  {
    dbg(DEBUG,"%s %d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,%d,%d",pclFunc,
        rgXml.rlLine[ilI].ilLevel,
        &rgXml.rlLine[ilI].pclTag[0],
        &rgXml.rlLine[ilI].pclName[0],
        &rgXml.rlLine[ilI].pclType[0],
        &rgXml.rlLine[ilI].pclBasTab[0],
        &rgXml.rlLine[ilI].pclBasFld[0],
        &rgXml.rlLine[ilI].pclFlag[0],
        &rgXml.rlLine[ilI].pclMethod[0][0],
        &rgXml.rlLine[ilI].pclFieldA[0][0],
        &rgXml.rlLine[ilI].pclFieldD[0][0],
        &rgXml.rlLine[ilI].pclMethod[1][0],
        &rgXml.rlLine[ilI].pclFieldA[1][0],
        &rgXml.rlLine[ilI].pclFieldD[1][0],
        rgXml.rlLine[ilI].ilNoMethod,
        rgXml.rlLine[ilI].ilNoFieldA,
        rgXml.rlLine[ilI].ilNoFieldD);
  }
  for (ilI = 0; ilI < igCurInterface; ilI++)
  {
    dbg(DEBUG,"%s Interface: %s,%d",pclFunc,
        &rgInterface[ilI].pclIntfName[0],
        rgInterface[ilI].ilIntfIdx);
  }

  return ilRC;
} /* End of GetXMLSchema */


static int CheckXmlInput(char *pcpData)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "CheckXmlInput:";
  char pclNewData[512000];
  int ilCurLine;
  char *pclTmpPtr;
  char *pclTmpPtr2;
  char *pclTmpPtr3;
  char pclLine[32000];
  char pclTag[1024];
  char pclTmpTag[1024];
  char pclData[4096];
  char *pclTmpBgn;
  char *pclTmpEnd;
  int ilIdx;
  char pclSearchItem[2048];
  char pclStack[50][1024];
  char pclStack2[50][1024];
  int ilStack;
  char pclSitaSections[1024];
  int ilSitaSecFound;
  int ilContentFound;
  int ilI;
  int ilNoEle;
  char pclTmpBuf[1024];
  char pclSecBgn[1024];
  char pclSecEnd[1024];

  ilRC = iGetConfigEntry(pcgConfigFile,"DGSHDL","SITA_SECTIONS",CFG_STRING,pclSitaSections);
  if (ilRC == RC_SUCCESS)
     ilNoEle = GetNoOfElements(pclSitaSections,',');
  else
     ilNoEle = 0;
  ilSitaSecFound = FALSE;
  ilContentFound = FALSE;
  memset(pclNewData,0x00,512000);
  strcpy(pcgSitaText,"");
  pclTmpPtr = pclNewData;
  pclTmpPtr2 = pcpData;
  while (*pclTmpPtr2 != '\0')
  {
     if (*pclTmpPtr2 != '\n')
     {
        *pclTmpPtr = *pclTmpPtr2;
        pclTmpPtr++;
     }
     else
     {
        CopyLine(pclLine,pclTmpPtr2+1);
        for (ilI = 1; ilI <= ilNoEle; ilI++)
        {
           get_real_item(pclTmpBuf,pclSitaSections,ilI);
           sprintf(pclSecBgn,"<%s>",pclTmpBuf);
           sprintf(pclSecEnd,"</%s>",pclTmpBuf);
           if (strstr(pclLine,pclSecBgn) != NULL)
              ilSitaSecFound = TRUE;
           else if (strstr(pclLine,pclSecEnd) != NULL)
              ilSitaSecFound = FALSE;
        }
        if (ilSitaSecFound == TRUE)
        {
           if (strstr(pclLine,"<CONTENT>") != NULL)
              ilContentFound = TRUE;
           else if (strstr(pclLine,"</CONTENT>") != NULL)
           {
              ilContentFound = FALSE;
              strcat(pcgSitaText,pclLine);
              strcat(pcgSitaText,"\n");
           }
           if (ilContentFound == TRUE)
           {
              strcat(pcgSitaText,pclLine);
              strcat(pcgSitaText,"\n");
              while (*pclTmpPtr != '>')
              {
                 *pclTmpPtr = '\0';
                 pclTmpPtr--;
              }
              pclTmpPtr++;
           }
        }
     }
     pclTmpPtr2++;
  }
  strcpy(pcpData,pclNewData);
/*dbg(TRACE,"ConfData = \n%s",pcpData);*/
  ilStack = 0;
  strcpy(pclNewData,"");
  ilCurLine = 1;
  pclTmpPtr = GetLine(pcpData,ilCurLine);
  while (pclTmpPtr != NULL)
  {
     CopyLine(pclLine,pclTmpPtr);
     TrimRight(pclLine);
     pclTmpBgn = pclLine;
     while (*pclTmpBgn != '\0')
     {
        while (*pclTmpBgn != '\0' && *pclTmpBgn != '<')
           pclTmpBgn++;
        pclTmpEnd = pclTmpBgn;
        while (*pclTmpEnd != '\0' && *pclTmpEnd != '>')
           pclTmpEnd++;
        if (*pclTmpEnd != '\0')
           pclTmpEnd++;
        memset(pclTag,0x00,1024);
        strncpy(pclTag,pclTmpBgn,pclTmpEnd-pclTmpBgn);
        strcat(pclNewData,pclTag);
        pclTmpBgn = pclTmpEnd;
        if (strstr(pclTag,"!--") != NULL || strstr(pclTag,"?xml") != NULL)
           strcat(pclNewData,"\n");
        else
        {
           ilRC = RemoveCharFromTag(pclTag,pclTmpTag);
           ilIdx = GetXmlLineIdx("STR_BGN",pclTmpTag,NULL);
           if (ilIdx >= 0)
           {
              strcat(pclNewData,"\n");
              if (pclTag[strlen(pclTag)-2] != '/')
              {
                 if (pclTag[1] != '/')
                 {
                    strcpy(&pclStack[ilStack][0],pclTmpTag);
                    strcpy(&pclStack2[ilStack][0],pclTag);
                    ilStack++;
                 }
                 else
                 {
                    if (ilStack == 0)
                    {
                       strcpy(&pclStack[ilStack][0],pclTmpTag);
                       strcpy(&pclStack2[ilStack][0],pclTag);
                       ilStack++;
                    }
                    else
                    {
                       if (strcmp(&pclStack[ilStack-1][0],pclTmpTag) != 0)
                       {
                          strcpy(&pclStack[ilStack][0],pclTmpTag);
                          strcpy(&pclStack2[ilStack][0],pclTag);
                          ilStack++;
                       }
                       else
                          ilStack--;
                    }
                 }
              }
           }
           else
           {
/*
              pclTmpEnd = pclTmpBgn;
              while (*pclTmpEnd != '\0' && *pclTmpEnd != '>')
                 pclTmpEnd++;
              if (*pclTmpEnd != '\0')
                 pclTmpEnd++;
              memset(pclData,0x00,4096);
              strncpy(pclData,pclTmpBgn,pclTmpEnd-pclTmpBgn);
              strcat(pclNewData,pclData);
              pclTmpBgn = pclTmpEnd;
              while (*pclTmpBgn != '\0' && *pclTmpBgn != '<')
                 pclTmpBgn++;
              pclTmpEnd = pclTmpBgn;
              while (*pclTmpEnd != '\0' && *pclTmpEnd != '>')
                 pclTmpEnd++;
              if (*pclTmpEnd != '\0')
                 pclTmpEnd++;
*/
              sprintf(pclSearchItem,"</%s>",pclTmpTag);
              pclTmpEnd = strstr(pclTmpBgn,pclSearchItem);
              if (pclTmpEnd != NULL)
              {
                 while (*pclTmpEnd != '\0' && *pclTmpEnd != '>')
                    pclTmpEnd++;
                 if (*pclTmpEnd != '\0')
                    pclTmpEnd++;
                 memset(pclTag,0x00,1024);
                 strncpy(pclTag,pclTmpBgn,pclTmpEnd-pclTmpBgn);
                 strcat(pclNewData,pclTag);
                 strcat(pclNewData,"\n");
                 pclTmpBgn = pclTmpEnd;
              }
              else
              {
                 if (pclTag[strlen(pclTag)-2] != '/')
                 {
                    dbg(TRACE,"%s Ending Tag <%s> not found",pclFunc,pclTmpTag);
                    return RC_FAIL;
                 }
                 else
                 {
                    strcat(pclNewData,pclSearchItem);
                    strcat(pclNewData,"\n");
                 }
              }
           }
        }
     }
     ilCurLine++;
     pclTmpPtr = GetLine(pcpData,ilCurLine);
  }
  if (ilStack > 0)
  {
     dbg(TRACE,"%s Structure Mismatch",pclFunc);
     for (ilIdx = 0; ilIdx < ilStack; ilIdx++)
        dbg(TRACE,"%s Stack %d %s",pclFunc,ilIdx,&pclStack2[ilIdx][0]);
     return RC_FAIL;
  }
/*dbg(TRACE,"NewData = \n%s",pclNewData);*/
  strcpy(pcpData,pclNewData);

  return ilRC;
} /* End of CheckXmlInput */


static int GetXmlLineIdx(char *pcpTag, char *pcpName, char *pcpSection)
{
  int ilRC = -1;
  char pclFunc[] = "GetXmlLineIdx:";
  int ilI;
  int ilJ;
  int ilFound;
  char pclFldNam[32];

  ilFound = FALSE;
  ilI = 0;
  if (pcpSection != NULL)
  {
     while (strcmp(&rgXml.rlLine[ilI].pclName[0],pcpSection) != 0 && ilI <igCurXmlLines)
        ilI++;
  }
  else
     ilI = -1;
  for (ilJ = ilI + 1; ilJ < igCurXmlLines && ilFound == FALSE; ilJ++)
  {
     if (*pcgAdid == 'D')
        strcpy(pclFldNam,&rgXml.rlLine[ilJ].pclBasFld[0]);
     else
        strcpy(pclFldNam,&rgXml.rlLine[ilJ].pclBasTab[0]);
     if (strlen(pclFldNam) == 0 || *pclFldNam == ' ')
        strcpy(pclFldNam,&rgXml.rlLine[ilJ].pclName[0]);
     if (strcmp(&rgXml.rlLine[ilJ].pclTag[0],pcpTag) == 0 && 
         (strcmp(pclFldNam,pcpName) == 0 || strcmp(&rgXml.rlLine[ilJ].pclType[0],pcpName) == 0))
     {
        ilRC = ilJ;
        ilFound = TRUE;
     }
     if (pcpSection != NULL)
     {
        if (strcmp(&rgXml.rlLine[ilJ].pclTag[0],"STR_END") == 0 && strcmp(&rgXml.rlLine[ilJ].pclName[0],pcpSection) == 0)
        {
           ilFound = TRUE;
        }
     }
  }

  return ilRC;
} /* End of GetXmlLineIdx */


static int RemoveCharFromTag(char *pcpTag, char *pcpNewTag)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "RemoveCharFromTag:";
  int ilI;
  char *pclTmpPtr;

  ilI = 0;
  pclTmpPtr = pcpTag;
  while (*pclTmpPtr != '\0')
  {
     if (*pclTmpPtr != '<' && *pclTmpPtr != '>' && *pclTmpPtr != '/')
     {
        pcpNewTag[ilI] = *pclTmpPtr;
        ilI++;
     }
     pclTmpPtr++;
  }
  pcpNewTag[ilI] = '\0';

  return ilRC;
} /* End of RemoveCharFromTag */


static int CheckData(char *pcpData, int ipIdx)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "CheckData:";
  int ilDay;
  int ilMin;
  int ilWkDay;
  char pclTmpBuf[1024];
  char pclTable[16];
  char pclField[16];
  int ilCount;

  if (strcmp(&rgXml.rlLine[ipIdx].pclType[0],"DATI") == 0)
  {
     if (*pcpData != ' ')
     {
        if (strlen(pcpData) < 9)
           ilRC = RC_FAIL;
        else
           ilRC = GetFullDay(pcpData,&ilDay,&ilMin,&ilWkDay);
     }
  }
  else
  {
     if (strcmp(&rgXml.rlLine[ipIdx].pclType[0],"DATE") == 0)
     {
        if (*pcpData != ' ')
        {
           sprintf(pclTmpBuf,"%s120000",pcpData);
           ilRC = GetFullDay(pclTmpBuf,&ilDay,&ilMin,&ilWkDay);
        }
     }
     else
     {
        if (strcmp(&rgXml.rlLine[ipIdx].pclType[0],"TIME") == 0 ||
            strcmp(&rgXml.rlLine[ipIdx].pclType[0],"TIMS") == 0)
        {
           if (*pcpData != ' ')
           {
              if (strcmp(&rgXml.rlLine[ipIdx].pclType[0],"TIMS") == 0 && strlen(pcpData) > 4)
                 ilRC = RC_FAIL;
              else
              {
                 sprintf(pclTmpBuf,"20050202%s",pcpData);
                 ilRC = GetFullDay(pclTmpBuf,&ilDay,&ilMin,&ilWkDay);
              }
           }
        }
        else
        {
           if (strcmp(&rgXml.rlLine[ipIdx].pclType[0],"FLNO") == 0)
           {
              ilRC = CheckFlightNo(pcpData);
              if (ilRC == RC_SUCCESS)
                 strcpy(&rgXml.rlLine[ipIdx].pclData[0],pcpData);
           }
           else
           {
              if (strcmp(&rgXml.rlLine[ipIdx].pclType[0],"BAS") == 0)
              {
                 if (*pcpData != ' ')
                 {
                    strcpy(pclTable,&rgXml.rlLine[ipIdx].pclBasTab[0]);
                    strcpy(pclField,&rgXml.rlLine[ipIdx].pclBasFld[0]);
                    ilCount = 1;
                    ilRC = syslibSearchDbData(pclTable,pclField,pcpData,pclField,pclTmpBuf,&ilCount,"\n");
                 }
              }
              else
              {
                 if (strcmp(&rgXml.rlLine[ipIdx].pclType[0],"VIAL") == 0)
                 {
                 }
                 else
                 {
                    if (strcmp(&rgXml.rlLine[ipIdx].pclType[0],"JFNO") == 0)
                    {
                    }
                    else
                    {
                    }
                 }
              }
           }
        }
     }
  }
  if (ilRC != RC_SUCCESS)
  {
     dbg(TRACE,"%s Wrong Data <%s> for <%s>",pclFunc,pcpData,&rgXml.rlLine[ipIdx].pclName[0]);
snap(pcpData,strlen(pcpData),outp);
  }

  return ilRC;
} /* End of CheckData */


static int CheckFlightNo(char *pcpFlight)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "CheckFlightNo:";
  char pclTmpBuf[100];
  char pclTmpNum[100];
  int ilTmpNum;
  char pclSuffix[4];
  int ilI;
  int ilCount;
  char pclAlc[16];
  char pclResult[16];

  if (*pcpFlight == '\0' || *pcpFlight == ' ')
     return RC_SUCCESS;

  memset(pclTmpBuf,0x00,100);
  *pclSuffix = '\0';
  strncpy(pclTmpBuf,pcpFlight,2);
  if (pcpFlight[2] == ' ')
  {
     pclTmpBuf[2] =  ' ';
     strcpy(pclTmpNum,&pcpFlight[3]);
  }
  else
  {
     if (!isdigit(pcpFlight[2]))
     {
        pclTmpBuf[2] = pcpFlight[2];
        strcpy(pclTmpNum,&pcpFlight[3]);
     }
     else
     {
        pclTmpBuf[2] =  ' ';
        strcpy(pclTmpNum,&pcpFlight[2]);
     }
  }
  if (!isdigit(pcpFlight[strlen(pcpFlight)-1]))
  {
     strcpy(pclSuffix,&pcpFlight[strlen(pcpFlight)-1]);
     /*pcpFlight[strlen(pcpFlight)-1] = '\0';*/
     pclTmpNum[strlen(pclTmpNum)-1] = '\0';
  }
  if (strlen(pclTmpNum) == 0)
  {
     dbg(TRACE,"%s Number Part is empty",pclFunc);
     return RC_FAIL;
  }
  TrimRight(pclTmpNum);
  for (ilI = 0; ilI < strlen(pclTmpNum); ilI++)
  {
     if (!isdigit(pclTmpNum[ilI]))
     {
        dbg(TRACE,"%s Number Part contains Char <%s>",pclFunc,pclTmpNum);
        return RC_FAIL;
     }
  }
  strcpy(pclAlc,pclTmpBuf);
  if (pclAlc[2] == ' ')
     pclAlc[2] = '\0';
  ilCount = 1;
  if (strlen(pclAlc) == 2)
     ilRC = syslibSearchDbData("ALTTAB","ALC2",pclAlc,"URNO",pclResult,&ilCount,"\n");
  else
     ilRC = syslibSearchDbData("ALTTAB","ALC3",pclAlc,"URNO",pclResult,&ilCount,"\n");
  if (ilRC != RC_SUCCESS)
  {
     dbg(TRACE,"%s Airline Code <%s> not found in ALTTAB",pclFunc,pclAlc);
     return ilRC;
  }
  ilTmpNum = atoi(pclTmpNum);
  if (ilTmpNum <= 9)
  {
     strcat(pclTmpBuf,"00");
  }
  else
  {
     if (ilTmpNum <= 99)
     {
        strcat(pclTmpBuf,"0");
     }
  }
  sprintf(pclTmpNum,"%d",ilTmpNum);
  strcat(pclTmpBuf,pclTmpNum);
  if (*pclSuffix != '\0')
  {
     if (strlen(pclTmpBuf) == 6)
        strcat(pclTmpBuf,"  ");
     else
        if (strlen(pclTmpBuf) == 7)
           strcat(pclTmpBuf," ");
     strcat(pclTmpBuf,pclSuffix);
  }
  strcpy(pcpFlight,pclTmpBuf);

  return ilRC;
} /* End of CheckFlightNo */


static int HandleInput(char *pcpData, int ipBegin, int ipEnd)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "HandleInput:";
  int ilI;
  int ilJ;
  int ilCurLine;
  char *pclTmpPtr;
  char pclLine[8000];
  char *pclLinePtr;
  char pclNewLine[8000];
  int ilIdx;
  int ilCurXMLLine;
  int ilFound;
  char pclData[4000];
  char pclAction[16];
  int ilWrongData;
  char pclTmpBuf[1024];
  int ilNoEle;
  int ilBegin;
  int ilEnd;
  int ilReturn = RC_SUCCESS;
  int ilActTypeIdx;
  char pclSearchItem[8000];
  char pclTagBgn[8000];
  char pclDat[8000];
  char pclTagEnd[8000];
  char *pclTmpPtr2;
  char pclSitaSections[1024];
  char pclTableSections[1024];
  char pclMultipleSections[1024];
  char pclMinTime[32];
  char pclMaxTime[32];
  char pclMinField[32];
  char pclMaxField[32];
  char pclName[1024];
  char *pclNamePtr;
  char pclOrigin[16];
  char pclSection[32] = "";
  char pclFileRequest[1024];

  strcpy(pcgAdid,"");
  strcpy(pcgUrno,"");
  strcpy(pcgIntfName,"");
  igNoKeys = 0;
  igNoData = 0;
  igIntfIdx = 0;
  ilWrongData = FALSE;
  igAutoActType = FALSE;
  for (ilI = 0; ilI < igCurXmlLines; ilI++)
  {
     rgXml.rlLine[ilI].ilRcvFlag = 0;
     rgXml.rlLine[ilI].ilPrcFlag = 0;
     strcpy(&rgXml.rlLine[ilI].pclData[0],"");
  }

  ilCurXMLLine = 0;
  ilCurLine = 1;
  pclTmpPtr = GetLine(pcpData,ilCurLine);
  while (pclTmpPtr != NULL)
  {
     memset(pclNewLine,0x00,8000);
     memset(pclDat,0x00,8000);
     strcpy(pclTagBgn,"");
     strcpy(pclTagEnd,"");
     ilIdx = 0;
     CopyLine(pclLine,pclTmpPtr);
     TrimRight(pclLine);
     pclLinePtr = pclLine;
     while (*pclLinePtr == ' ' && *pclLinePtr != '\0')
        pclLinePtr++;
     if (strlen(pclLinePtr) > 0)
     {
        if (*pclLinePtr == '<')
        {
           pclLinePtr++;
           while (*pclLinePtr != '>' && *pclLinePtr != '\0')
           {
              pclNewLine[ilIdx] = *pclLinePtr;
              pclLinePtr++;
              ilIdx++;
           }
           if (*pclLinePtr != '\0')
              pclLinePtr++;
           strcpy(pclTagBgn,pclNewLine);
           if (strlen(pclLinePtr) > 0)
           {
              sprintf(pclSearchItem,"</%s",pclNewLine);
              if (pclSearchItem[strlen(pclSearchItem)-1] == '/')
                 pclSearchItem[strlen(pclSearchItem)-1] = '\0';
              pclTmpPtr2 = strstr(pclLinePtr,pclSearchItem);
              if (pclTmpPtr2 != NULL)
              {
                 strncpy(pclDat,pclLinePtr,pclTmpPtr2-pclLinePtr);
                 pclTmpPtr2++;
                 strcpy(pclTagEnd,pclTmpPtr2);
                 if (pclTagEnd[strlen(pclTagEnd)-1] == '>')
                    pclTagEnd[strlen(pclTagEnd)-1] = '\0';
              }
              else
              {
                 strcpy(pclDat,pclLinePtr);
              }
           }
           if (strlen(pclTagEnd) > 0)
           {
              strcat(pclTagBgn,",");
              strcat(pclTagBgn,pclDat);
              strcat(pclTagBgn,",");
              strcat(pclTagBgn,pclTagEnd);
           }
           else if (strlen(pclDat) > 0)
           {
              strcat(pclTagBgn,",");
              strcat(pclTagBgn,pclDat);
           }
           strcpy(pclNewLine,pclTagBgn);
           get_item(1,pclNewLine,pclName,0,",","\0","\0");
           TrimRight(pclName);
           pclNamePtr = pclName;
           if (pclName[strlen(pclName)-1] != '/' && strstr(pclName,"!--") == NULL && strstr(pclName,"?xml") == NULL)
           { /* ignore line */
              if (*pclNamePtr == '/')
                 *pclNamePtr++;
              ilFound = FALSE;
              if (ipBegin >= 0)
              {
                 ilBegin = ipBegin;
                 ilEnd = ipEnd;
              }
              else
              {
                 ilBegin = ilCurXMLLine;
                 ilEnd = igCurXmlLines;
              }
              for (ilI = ilBegin; ilI < ilEnd && ilFound == FALSE; ilI++)
              {
                 if (strcmp(&rgXml.rlLine[ilI].pclName[0],pclNamePtr) == 0)
                 {
                    if (rgXml.rlLine[ilI].ilRcvFlag == 0)
                    {
                       rgXml.rlLine[ilI].ilRcvFlag = 1;
                       if (GetNoOfElements(pclNewLine,',') > 1)
                       {
                          get_item(2,pclNewLine,pclData,0,",","\0","\0");
                          TrimLeftRight(pclData);
                          CheckCDATA(pclData);
                          strcpy(&rgXml.rlLine[ilI].pclData[0],pclData);
                          ilRC = CheckData(pclData,ilI);
                          if (ilRC != RC_SUCCESS) 
                             ilWrongData = TRUE;
                       }
                       else
                       {
                          if (strcmp(&rgXml.rlLine[ilI].pclTag[0],"STR_BGN") == 0)
                          {
                             strcpy(pclSection,&rgXml.rlLine[ilI].pclName[0]);
                             ilCurXMLLine = ilI + 1;
                          }
                       }
                       ilFound = TRUE;
                    }
                    else
                    {
                       ilReturn = - 10;
                    }
                 }
              }
              if (ilFound == FALSE)
              {
                 dbg(TRACE,"%s Can't find Definition for Line %s in XML Schema! Section = <%s>",
                     pclFunc,pclLine,pclSection);
              }
           }
        }
     }
     ilCurLine++;
     pclTmpPtr = GetLine(pcpData,ilCurLine);
  }

  if (ilWrongData == TRUE)
     return RC_FAIL;

/*
  dbg(DEBUG,"%s Found Input:",pclFunc);
  for (ilI = 0; ilI < igCurXmlLines; ilI++)
  {
    if (rgXml.rlLine[ilI].ilRcvFlag == 1)
    {
       dbg(DEBUG,"%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,%d,%d,%s",
           rgXml.rlLine[ilI].ilLevel,
           &rgXml.rlLine[ilI].pclTag[0],
           &rgXml.rlLine[ilI].pclName[0],
           &rgXml.rlLine[ilI].pclType[0],
           &rgXml.rlLine[ilI].pclBasFld[0],
           &rgXml.rlLine[ilI].pclBasTab[0],
           &rgXml.rlLine[ilI].pclFlag[0],
           &rgXml.rlLine[ilI].pclMethod[0][0],
           &rgXml.rlLine[ilI].pclFieldA[0][0],
           &rgXml.rlLine[ilI].pclFieldD[0][0],
           &rgXml.rlLine[ilI].pclMethod[1][0],
           &rgXml.rlLine[ilI].pclFieldA[1][0],
           &rgXml.rlLine[ilI].pclFieldD[1][0],
           rgXml.rlLine[ilI].ilNoMethod,
           rgXml.rlLine[ilI].ilNoFieldA,
           rgXml.rlLine[ilI].ilNoFieldD,
           &rgXml.rlLine[ilI].pclData[0]);
     }
  }
*/

  ilActTypeIdx = GetXmlLineIdx("HDR","ACTIONTYPE",NULL);
  if (ilActTypeIdx >= 0)
  {
     if (rgXml.rlLine[ilActTypeIdx].ilRcvFlag == 0)
     {
        rgXml.rlLine[ilActTypeIdx].ilRcvFlag = 1;
        strcpy(&rgXml.rlLine[ilActTypeIdx].pclData[0],"U");
        igAutoActType = TRUE;
     }
  }

  ilFound = FALSE;
  for (ilI = 0; ilI < igCurXmlLines && ilFound == FALSE; ilI++)
  {
     if (strcmp(&rgXml.rlLine[ilI].pclTag[0],"HDR") == 0 && strcmp(&rgXml.rlLine[ilI].pclName[0],"MESSAGEORIGIN") == 0)
     {
        ilFound = TRUE;
        strcpy(pclOrigin,"STD");
        if (rgXml.rlLine[ilI].ilRcvFlag == 1)
        {
           strcpy(pclOrigin,&rgXml.rlLine[ilI].pclData[0]);
           strcpy(pcgIntfName,&rgXml.rlLine[ilI].pclData[0]);
        }
     }
  }
  ilFound = FALSE;
  for (ilI = 0; ilI < igCurInterface && ilFound == FALSE; ilI++)
  {
     if (strcmp(&rgInterface[ilI].pclIntfName[0],pclOrigin) == 0)
     {
        igIntfIdx = rgInterface[ilI].ilIntfIdx;
        ilFound = TRUE;
     }
  }
  if (ilFound == FALSE)
  {
     if (strcmp(pclOrigin,"STD") == 0)
     {
        igIntfIdx = 0;
     }
     else
     {
        strcpy(pclOrigin,"STD");
        ilFound = FALSE;
        for (ilI = 0; ilI < igCurInterface && ilFound == FALSE; ilI++)
        {
           if (strcmp(&rgInterface[ilI].pclIntfName[0],pclOrigin) == 0)
           {
              igIntfIdx = rgInterface[ilI].ilIntfIdx;
              ilFound = TRUE;
           }
        }
     }
  }
/*
  if (ilFound == TRUE)
     dbg(DEBUG,"Use Interface Index = %d for Interface = %s",igIntfIdx,&rgInterface[igIntfIdx].pclIntfName[0]);
  else
     dbg(DEBUG,"No Interface defined!!!");
*/

  if (igAutoActType == TRUE)
  {
     if (rgXml.rlLine[ilActTypeIdx].pclMethod[igIntfIdx][0] == 'M')
     {
        rgXml.rlLine[ilActTypeIdx].ilRcvFlag = 0;
        strcpy(&rgXml.rlLine[ilActTypeIdx].pclData[0],"");
        dbg(TRACE,"%s Header Field <ACTIONTYPE> is mandatory for this Interface",pclFunc);
        return RC_FAIL;
     }
  }
  if (igMaxTimeDiffForUpdate > 0)
  {  /* Check for abnormal Time Differences */
     strcpy(pclMinTime,"");
     strcpy(pclMaxTime,"");
     strcpy(pclMinField,"");
     strcpy(pclMaxField,"");
     for (ilI = 0; ilI < igCurXmlLines; ilI++)
     {
        if ((strcmp(&rgXml.rlLine[ilI].pclTag[0],"KEY") == 0 || strcmp(&rgXml.rlLine[ilI].pclTag[0],"DAT") == 0) &&
            strcmp(&rgXml.rlLine[ilI].pclType[0],"DATI") == 0 && rgXml.rlLine[ilI].ilRcvFlag == 1)
        {
           strcpy(pclTmpBuf,&rgXml.rlLine[ilI].pclData[0]);
           if (strlen(pclTmpBuf) == 12)
              strcat(pclTmpBuf,"00");
           if (strlen(pclTmpBuf) == 14)
           {
              if (*pclMinTime == '\0')
              {
                 strcpy(pclMinTime,pclTmpBuf);
                 strcpy(pclMaxTime,pclTmpBuf);
                 strcpy(pclMinField,&rgXml.rlLine[ilI].pclName[0]);
                 strcpy(pclMaxField,&rgXml.rlLine[ilI].pclName[0]);
              }
              else
              {
                 if (strcmp(pclTmpBuf,pclMinTime) < 0)
                 {
                    strcpy(pclMinTime,pclTmpBuf);
                    strcpy(pclMinField,&rgXml.rlLine[ilI].pclName[0]);
                 }
                 else
                 {
                    if (strcmp(pclTmpBuf,pclMaxTime) > 0)
                    {
                       strcpy(pclMaxTime,pclTmpBuf);
                       strcpy(pclMaxField,&rgXml.rlLine[ilI].pclName[0]);
                    }
                 }
              }
           }
        }
     }
     strcpy(pclTmpBuf,pclMinTime);
     ilRC = MyAddSecondsToCEDATime(pclTmpBuf,igMaxTimeDiffForUpdate*60*60,1);
     if (strcmp(pclTmpBuf,pclMaxTime) < 0)
     {
        dbg(TRACE,"%s Time Difference between Field <%s> and <%s> is more than %d hours",
            pclFunc,pclMinField,pclMaxField,igMaxTimeDiffForUpdate);
        dbg(TRACE,"%s Message is rejected",pclFunc);
        return RC_FAIL;
     }
  }
  ilFound = FALSE;
  for (ilI = 0; ilI < igCurXmlLines && ilFound == FALSE; ilI++)
  {
     if (strcmp(&rgXml.rlLine[ilI].pclTag[0],"HDR") == 0 && strcmp(&rgXml.rlLine[ilI].pclName[0],"ACTIONTYPE") == 0)
     {
        ilFound = TRUE;
        if (rgXml.rlLine[ilI].ilRcvFlag == 1)
        {
           strcpy(pclAction,&rgXml.rlLine[ilI].pclData[0]);
           if (strcmp(pclAction,"U") == 0 || strcmp(pclAction,"P") == 0)
           {
              ilRC = HandleUpdate("U");
           }
           else
           {
              if (strcmp(pclAction,"I") == 0)
              {
                 /*ilRC = HandleInsert();*/
              }
              else
              {
                 if (strcmp(pclAction,"D") == 0)
                 {
                    ilRC = HandleUpdate("D");
                 }
                 else
                 {
                    if (strcmp(pclAction,"A") == 0)
                    {
                       /*ilRC = HandleAck();*/
                    }
                    else
                    {
                       ilRC = RC_FAIL;
                       dbg(TRACE,"%s Wrong Action Type <%s> received!!!",pclFunc,pclAction);
                    }
                 }
              }
           }
        }
        else
        {
           ilRC = RC_FAIL;
           dbg(TRACE,"%s No Action Type received!!!",pclFunc);
        }
     }
  }
  if (ilFound == FALSE)
  {
     ilRC = RC_FAIL;
     dbg(TRACE,"%s No Action Type defined!!!",pclFunc);
  }

  return ilRC;
} /* End of HandleInput */


static int HandleUpdate(char *pcpFunc)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "HandleUpdate:";
  int ilI;
  int ilJ;
  int ilFound;
  int ilNoEle;
  char pclMethod[32];
  char pclName[1024];
  char pclTmpBuf[1024];
  char pclTable[16];
  char pclFieldList[1024];
  char pclDataList[8000];
  char pclDataBuf[8000];
  int ilItemNo;
  char pclFtyp[16];
  char pclLand[16];
  char pclStoa[16];

  ilRC = iGetConfigEntry(pcgConfigFile,"DGSHDL","FLIGHT",CFG_STRING,pclName);
  strcpy(pclMethod,"");
  ilFound = FALSE;
  for (ilI = 0; ilI < igCurXmlLines && ilFound == FALSE; ilI++)
  {
     if (strstr(pclName,&rgXml.rlLine[ilI].pclName[0]) != NULL)
     {
        strcpy(pclMethod,&rgXml.rlLine[ilI].pclMethod[igIntfIdx][0]);
        ilFound = TRUE;
     }
  }
  ilRC = GetKeys("U");
  if (ilRC != RC_SUCCESS)
     return ilRC;
  ilFound = FALSE;
  ilNoEle = GetNoOfElements(pclName,',');
  for (ilI = 1; ilI <= ilNoEle; ilI++)
  {
     get_real_item(pclTmpBuf,pclName,ilI);
     for (ilJ = 0; ilJ < igCurXmlLines && ilFound == FALSE; ilJ++)
     {
        if (strstr(pclTmpBuf,&rgXml.rlLine[ilJ].pclName[0]) != NULL && rgXml.rlLine[ilJ].ilRcvFlag == 1)
        {
           ilFound = TRUE;
        }
     }
  }
  if (igNoKeys > 0)
  {
     if (*pcpFunc == 'D')
     {
        ilFound = FALSE;
        for (ilI = 0; ilI < igNoKeys && ilFound == FALSE; ilI++)
        {
           if (strcmp(&rgKeys[ilI].pclField[0],"URNO") == 0 || strcmp(&rgKeys[ilI].pclType[0],"URNO") == 0)
           {
              strcpy(pcgUrno,&rgKeys[ilI].pclData[0]);
              ilFound == TRUE;
           }
        }
     }
     else
     {
        ilRC = SearchFlight(pclMethod,pclTable);
     }
  }
  if (*pcgUrno != '\0')
  {
     ilNoEle = GetNoOfElements(pclName,',');
     igNoData = 0;
     for (ilI = 1; ilI <= ilNoEle; ilI++)
     {
        get_real_item(pclTmpBuf,pclName,ilI);
        ilRC = GetData(pclMethod,igNoData,pclTmpBuf);
     }
     /*ilRC = ShowAll();*/
     strcpy(pclFieldList,"");
     strcpy(pclDataList,"");
     for (ilI = 0; ilI < igNoKeys; ilI++)
     {
        strcat(pclFieldList,&rgKeys[ilI].pclField[0]);
        strcat(pclFieldList,",");
        strcat(pclDataList,&rgKeys[ilI].pclData[0]);
        strcat(pclDataList,",");
     }
     for (ilI = 0; ilI < igNoData; ilI++)
     {
        strcat(pclFieldList,&rgData[ilI].pclField[0]);
        strcat(pclFieldList,",");
        strcat(pclDataList,&rgData[ilI].pclData[0]);
        strcat(pclDataList,",");
     }
     if (strlen(pclFieldList) > 0)
     {
        pclFieldList[strlen(pclFieldList)-1] = '\0';
        pclDataList[strlen(pclDataList)-1] = '\0';
     }
     ilItemNo = get_item_no(pclFieldList,"ADID",5) + 1;
     if (ilItemNo > 0)
     {
        get_real_item(pcgAdid,pclDataList,ilItemNo);
        TrimRight(pcgAdid);
        strcpy(pclDataBuf,"L1");
        if (*pcgAdid == 'A')
           strcat(pclDataBuf,"6 ");
        else if (*pcgAdid == 'D')
           strcat(pclDataBuf,"18");
        ilItemNo = get_item_no(pclFieldList,"FTYP",5) + 1;
        if (ilItemNo > 0)
           get_real_item(pclFtyp,pclDataList,ilItemNo);
        else
           strcpy(pclFtyp,"X");
        strcpy(pclLand,"D");
        if (*pcgAdid == 'A' && *pclFtyp != 'T')
        {
           ilItemNo = get_item_no(pclFieldList,"LAND",5) + 1;
           get_real_item(pclLand,pclDataList,ilItemNo);
           TrimRight(pclLand);
        }
        if (*pcgAdid == 'A')
           ilItemNo = get_item_no(pclFieldList,"PSTA",5) + 1;
        else if (*pcgAdid == 'D')
           ilItemNo = get_item_no(pclFieldList,"PSTD",5) + 1;
        if (ilItemNo > 0)
           get_real_item(pclTmpBuf,pclDataList,ilItemNo);
        else
           strcpy(pclTmpBuf," ");
        TrimRight(pclTmpBuf);
        if (*pclFtyp == 'X' || *pclLand == ' ')
           strcpy(pclTmpBuf," ");
        strcat(pclDataBuf,pclTmpBuf);
        ilI = strlen(pclDataBuf);
        while (ilI < 10)
        {
           strcat(pclDataBuf," ");
           ilI++;
        }
        ilItemNo = get_item_no(pclFieldList,"ACT5",5) + 1;
        if (ilItemNo > 0)
           get_real_item(pclTmpBuf,pclDataList,ilItemNo);
        else
           strcpy(pclTmpBuf," ");
        TrimRight(pclTmpBuf);
        strcat(pclDataBuf,pclTmpBuf);
        ilI = strlen(pclDataBuf);
        while (ilI < 16)
        {
           strcat(pclDataBuf," ");
           ilI++;
        }
        ilItemNo = get_item_no(pclFieldList,"FLNO",5) + 1;
        get_real_item(pclTmpBuf,pclDataList,ilItemNo);
        TrimRight(pclTmpBuf);
        strcat(pclDataBuf,pclTmpBuf);
        ilI = strlen(pclDataBuf);
        while (ilI < 36)
        {
           strcat(pclDataBuf," ");
           ilI++;
        }
        if (*pcgAdid == 'A')
           ilItemNo = get_item_no(pclFieldList,"STOA",5) + 1;
        else if (*pcgAdid == 'D')
           ilItemNo = get_item_no(pclFieldList,"STOD",5) + 1;
        get_real_item(pclTmpBuf,pclDataList,ilItemNo);
        strcpy(pclStoa,pclTmpBuf);
        if (strlen(pclTmpBuf) == 12)
           strcat(pclTmpBuf,"00");
        strcat(pclDataBuf,pclTmpBuf);
        if (*pcgAdid == 'A')
           ilItemNo = get_item_no(pclFieldList,"LAND",5) + 1;
        else if (*pcgAdid == 'D')
           ilItemNo = get_item_no(pclFieldList,"TIFD",5) + 1;
        if (ilItemNo > 0)
           get_real_item(pclTmpBuf,pclDataList,ilItemNo);
        else
           strcpy(pclTmpBuf,"");
        if (strlen(pclTmpBuf) == 12)
           strcat(pclTmpBuf,"00");
        else if (strlen(pclTmpBuf) == 0)
           strcat(pclTmpBuf,pclStoa);
        strcat(pclDataBuf,pclTmpBuf);
        ilItemNo = get_item_no(pclFieldList,"FTYP",5) + 1;
        if (ilItemNo > 0)
           get_real_item(pclTmpBuf,pclDataList,ilItemNo);
        else
           strcpy(pclFtyp,"X");
        TrimRight(pclTmpBuf);
        if (*pcgAdid == 'A')
        {
           if (*pclTmpBuf == 'T')
              strcat(pclDataBuf,"1");
           else
              strcat(pclDataBuf,"0");
        }
        else
           strcat(pclDataBuf," ");
        ilRC = Send_data(igSock,pclDataBuf,strlen(pclDataBuf)+1);
     }
  }

  return ilRC;
} /* End of HandleUpdate */


static int SearchFlight(char *pcpMethod, char *pcpTable)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SearchFlight:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[2048];
  char pclDataBuf[8000];
  int ilI;
  int ilFound;
  char pclUrno[16];
  char pclField[16];
  char pclFieldList[1024];
  char pclSelection[1024];
  char pclTmpBuf[1024];
  int ilNoItems;
  int ilHit;
  int ilItemNo;
  char pclAdid[8];
  char pclFtyp[8];
  char pclOrg3[8];
  char pclDes3[8];
  int ilDone;
  char pclTimeBegin[16];
  char pclTimeEnd[16];
  char pclAirlineCodes[1024];
  char pclAlc2[16];
  char pclAlc3[16];
  int ilNoEle;

  *pclUrno = '\0';
  strcpy(pclField,"URNO");
  ilFound = FALSE;
  for (ilI = 0; ilI < igNoKeys && ilFound == FALSE; ilI++)
  {
     if (strcmp(&rgKeys[ilI].pclField[0],"URNO") == 0 || strcmp(&rgKeys[ilI].pclType[0],"URNO") == 0)
     {
        strcpy(pclUrno,&rgKeys[ilI].pclData[0]);
        strcpy(pclField,&rgKeys[ilI].pclField[0]);
        strcpy(pcpMethod,&rgKeys[ilI].pclMethod[0]);
        ilFound == TRUE;
     }
  }
  if (*pclUrno != '\0')
  {
     sprintf(pclSelection,"WHERE %s = %s",pclField,pclUrno);
  }
  else
  {
     for (ilI = 0; ilI < igCurXmlLines; ilI++)
     {
        if (strcmp(&rgXml.rlLine[ilI].pclTag[0],"KEY") == 0 && rgXml.rlLine[ilI].ilRcvFlag == 0 &&
            (rgXml.rlLine[ilI].pclFlag[0] == 'M' || rgXml.rlLine[ilI].pclFlag[0] == 'C'))
        {
           if (strcmp(&rgXml.rlLine[ilI].pclName[0],"URNO") != 0 &&
               strcmp(&rgXml.rlLine[ilI].pclType[0],"URNO") != 0)
           {
              dbg(TRACE,"%s Key Field <%s> missing",pclFunc,&rgXml.rlLine[ilI].pclName[0]);
              return RC_FAIL;
           }
        }
     }
     strcpy(pcpMethod,&rgKeys[0].pclMethod[0]);
     strcpy(pclSelection,"WHERE ");
     for (ilI = 0; ilI < igNoKeys; ilI++)
     {
        ilDone = FALSE;
        if (strcmp(&rgKeys[ilI].pclType[0],"DATE") == 0)
        {
           sprintf(pclTmpBuf,"%s LIKE '%s%%'",&rgKeys[ilI].pclField[0],&rgKeys[ilI].pclData[0]);
           ilDone = TRUE;
        }
        else
        {
           if (strcmp(&rgKeys[ilI].pclType[0],"DATI") == 0)
           {
              if (*pcgAdid == 'A')
              {
                 if (igTimeWindowArrBegin != 0 || igTimeWindowArrEnd != 0)
                 {
                    strcpy(pclTimeBegin,&rgKeys[ilI].pclData[0]);
                    ilRC = MyAddSecondsToCEDATime(pclTimeBegin,igTimeWindowArrBegin*60,1);
                    strcpy(pclTimeEnd,&rgKeys[ilI].pclData[0]);
                    ilRC = MyAddSecondsToCEDATime(pclTimeEnd,igTimeWindowArrEnd*60,1);
                    sprintf(pclTmpBuf,"%s BETWEEN '%s' AND '%s'",&rgKeys[ilI].pclField[0],pclTimeBegin,pclTimeEnd);
                    ilDone = TRUE;
                 }
              }
              else
              {
                 if (*pcgAdid == 'D')
                 {
                    if (igTimeWindowArrBegin != 0 || igTimeWindowArrEnd != 0)
                    {
                       strcpy(pclTimeBegin,&rgKeys[ilI].pclData[0]);
                       ilRC = MyAddSecondsToCEDATime(pclTimeBegin,igTimeWindowDepBegin*60,1);
                       strcpy(pclTimeEnd,&rgKeys[ilI].pclData[0]);
                       ilRC = MyAddSecondsToCEDATime(pclTimeEnd,igTimeWindowDepEnd*60,1);
                       sprintf(pclTmpBuf,"%s BETWEEN '%s' AND '%s'",&rgKeys[ilI].pclField[0],pclTimeBegin,pclTimeEnd);
                       ilDone = TRUE;
                    }
                 }
              }
           }
           else
           {
              if (strcmp(&rgKeys[ilI].pclField[0],"ADID") == 0)
              {
                 if (*pcgAdid == 'D')
                 {
                    sprintf(pclTmpBuf,"ORG3 = '%s'",pcgHomeAp);
                    ilDone = TRUE;
                 }
                 else
                 {
                    sprintf(pclTmpBuf,"DES3 = '%s'",pcgHomeAp);
                    ilDone = TRUE;
                 }
              }
           }
        }
        if (ilDone == FALSE)
           sprintf(pclTmpBuf,"%s = '%s'",&rgKeys[ilI].pclField[0],&rgKeys[ilI].pclData[0]);
        if (strlen(pclSelection) > 6)
           strcat(pclSelection," AND ");
        strcat(pclSelection,pclTmpBuf);
     }
  }
  ilNoItems = 0;
  strcpy(pclTmpBuf,"");
  for (ilI = 0; ilI < igNoKeys; ilI++)
  {
     if (strcmp(&rgKeys[ilI].pclField[0],"STDT") != 0)
     {
        strcat(pclTmpBuf,&rgKeys[ilI].pclField[0]);
        strcat(pclTmpBuf,",");
        ilNoItems++;
     }
  }
  if (strlen(pclTmpBuf) > 0)
     pclTmpBuf[strlen(pclTmpBuf)-1] = '\0';
  if (strstr(pclTmpBuf,"URNO") == NULL)
  {
     sprintf(pclFieldList,"URNO,%s",pclTmpBuf);
     ilNoItems++;
  }
  else
     strcpy(pclFieldList,pclTmpBuf);
  ilRC = iGetConfigEntry(pcgConfigFile,pcpMethod,"table",CFG_STRING,pcpTable);
  if (ilRC != RC_SUCCESS)
     strcpy(pcpTable,"AFTTAB");
  if (*pcgAdid == '\0' && strstr(pclTmpBuf,"ADID") == NULL && strcmp(pcpTable,"AFTTAB") == 0)
  {
     strcat(pclFieldList,",ADID");
     ilNoItems++;
  }
  strcpy(pclAirlineCodes,"");
  sprintf(pclTmpBuf,"ALLOWED_AIRLINE_CODES_FOR_%s",pcgIntfName);
  ilRC = iGetConfigEntry(pcgConfigFile,"DGSHDL",pclTmpBuf,CFG_STRING,pclAirlineCodes);
  if (strlen(pclAirlineCodes) > 0)
  {
     if (strstr(pclFieldList,"ALC2") == NULL)
     {
        strcat(pclFieldList,",ALC2");
        ilNoItems++;
     }
     if (strstr(pclFieldList,"ALC3") == NULL)
     {
        strcat(pclFieldList,",ALC3");
        ilNoItems++;
     }
  }
  if (strstr(pclFieldList,"FTYP") == NULL)
  {
     strcat(pclFieldList,",FTYP");
     ilNoItems++;
  }
  if (strstr(pclFieldList,"ORG3") == NULL)
  {
     strcat(pclFieldList,",ORG3");
     ilNoItems++;
  }
  if (strstr(pclFieldList,"DES3") == NULL)
  {
     strcat(pclFieldList,",DES3");
     ilNoItems++;
  }
  ilRC = RC_SUCCESS;
  ilHit = 0;
  strcpy(pcgUrno,"");
  sprintf(pclSqlBuf,"SELECT %s FROM %s %s",pclFieldList,pcpTable,pclSelection);
  slCursor = 0;
  slFkt = START;
  dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
  ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  while (ilRCdb == DB_SUCCESS)
  {
     ilHit++;
     slFkt = NEXT;
     ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
  }
  close_my_cursor(&slCursor);
  if (ilHit == 0)
  {
     dbg(TRACE,"%s No Record Found: <%s>",pclFunc,pclSqlBuf);
     ilRC = RC_FAIL;
  }
  else
  {
     if (ilHit > 1)
     {
        dbg(TRACE,"%s Too Many (%d) Records Found: <%s>",pclFunc,ilHit,pclSqlBuf);
        ilRC = RC_FAIL;
     }
     else
     {
        BuildItemBuffer(pclDataBuf,"",ilNoItems,",");
        ilItemNo = get_item_no(pclFieldList,"URNO",5) + 1;
        get_real_item(pcgUrno,pclDataBuf,ilItemNo);
        dbg(DEBUG,"%s Found Record with URNO <%s>",pclFunc,pcgUrno);
        ilItemNo = get_item_no(pclFieldList,"ADID",5) + 1;
        get_real_item(pclAdid,pclDataBuf,ilItemNo);
        ilItemNo = get_item_no(pclFieldList,"FTYP",5) + 1;
        get_real_item(pclFtyp,pclDataBuf,ilItemNo);
        ilItemNo = get_item_no(pclFieldList,"ORG3",5) + 1;
        get_real_item(pclOrg3,pclDataBuf,ilItemNo);
        ilItemNo = get_item_no(pclFieldList,"DES3",5) + 1;
        get_real_item(pclDes3,pclDataBuf,ilItemNo);
        if (*pcgAdid == '\0')
        {
           strcpy(pcgAdid,pclAdid);
           dbg(DEBUG,"%s ADID = <%s> is now used",pclFunc,pcgAdid);
        }
        else
        {
           if (*pcgAdid != *pclAdid)
           {
              if (*pclFtyp != 'Z' && *pclFtyp != 'B' && *pclFtyp != 'T' && *pclFtyp != 'G')
              {
                 if (strcmp(pclOrg3,pclDes3) != 0)
                 {
                    dbg(TRACE,"%s ADID mismatch, Key: <%s> Rec; <%s>",pclFunc,pcgAdid,pclAdid);
                    *pcgUrno = '\0';
                    ilRC = RC_FAIL;
                 }
              }
           }
        }
        if (strlen(pclAirlineCodes) > 0)
        {
           ilItemNo = get_item_no(pclFieldList,"ALC2",5) + 1;
           get_real_item(pclAlc2,pclDataBuf,ilItemNo);
           ilItemNo = get_item_no(pclFieldList,"ALC3",5) + 1;
           get_real_item(pclAlc3,pclDataBuf,ilItemNo);
           TrimRight(pclAlc2);
           TrimRight(pclAlc3);
           ilFound = FALSE;
           ilNoEle = GetNoOfElements(pclAirlineCodes,',');
           for (ilI = 1; ilI <= ilNoEle && ilFound == FALSE; ilI++)
           {
              GetDataItem(pclTmpBuf,pclAirlineCodes,ilI,',',""," ");
              TrimRight(pclTmpBuf);
              if (strcmp(pclTmpBuf,pclAlc2) == 0 || strcmp(pclTmpBuf,pclAlc3) == 0)
                 ilFound = TRUE;
           }
           if (ilFound == FALSE)
           {
              dbg(TRACE,"%s Airline <%s>/<%s> not allowed for this Interface <%s>",pclFunc,pclAlc2,pclAlc3,pcgIntfName);
              *pcgUrno = '\0';
              ilRC = RC_FAIL;
           }
        }
     }
  }

  return ilRC;
} /* End of SearchFlight */


static int GetKeys(char *pcpFkt)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "GetKeys:";
  int ilI;
  int ilFound;
  int ilUrnoRcv;

  ilUrnoRcv = FALSE;
  ilFound = FALSE;
  *pcgAdid = '\0';
  igNoKeys = 0;
  for (ilI = 0; ilI < igCurXmlLines  && ilFound == FALSE; ilI++)
  {
     if (strcmp(&rgXml.rlLine[ilI].pclTag[0],"KEY") == 0 && rgXml.rlLine[ilI].ilRcvFlag == 1)
     {
        if (strcmp(&rgXml.rlLine[ilI].pclName[0],"URNO") == 0 ||
            strcmp(&rgXml.rlLine[ilI].pclType[0],"URNO") == 0)
        {
           ilUrnoRcv = TRUE;
        }
        if (strcmp(&rgXml.rlLine[ilI].pclName[0],"ADID") == 0)
        {
           strcpy(pcgAdid,&rgXml.rlLine[ilI].pclData[0]);
           ilFound = TRUE;
        }
        strcpy(&rgKeys[igNoKeys].pclField[0],&rgXml.rlLine[ilI].pclName[0]);
        strcpy(&rgKeys[igNoKeys].pclType[0],&rgXml.rlLine[ilI].pclType[0]);
        strcpy(&rgKeys[igNoKeys].pclMethod[0],&rgXml.rlLine[ilI].pclMethod[igIntfIdx][0]);
        strcpy(&rgKeys[igNoKeys].pclData[0],&rgXml.rlLine[ilI].pclData[0]);
        igNoKeys++;
        rgXml.rlLine[ilI].ilPrcFlag = 1;
     }
  }
  if (*pcgAdid != '\0')
  {
     if (*pcgAdid != 'A' && *pcgAdid != 'D' && *pcgAdid != 'B')
     {
        dbg(TRACE,"%s Invalid ADID <%s> received!!!",pclFunc,pcgAdid);
        ilRC = RC_FAIL;
     }
     igNoKeys = 0;
     for (ilI = 0; ilI < igCurXmlLines; ilI++)
     {
        if (strcmp(&rgXml.rlLine[ilI].pclTag[0],"KEY") == 0 && rgXml.rlLine[ilI].ilRcvFlag == 1)
        {
           strcpy(&rgKeys[igNoKeys].pclField[0],&rgXml.rlLine[ilI].pclName[0]);
           if (*pcgAdid == 'A' || *pcgAdid == 'B')
           {
              if (strlen(&rgXml.rlLine[ilI].pclFieldA[igIntfIdx][0]) > 0)
                 strcpy(&rgKeys[igNoKeys].pclField[0],&rgXml.rlLine[ilI].pclFieldA[igIntfIdx][0]);
              else
                 if (strlen(&rgXml.rlLine[ilI].pclFieldA[0][0]) > 0)
                    strcpy(&rgKeys[igNoKeys].pclField[0],&rgXml.rlLine[ilI].pclFieldA[0][0]);
           }
           if (*pcgAdid == 'D')
           {
              if (strlen(&rgXml.rlLine[ilI].pclFieldD[igIntfIdx][0]) > 0)
                 strcpy(&rgKeys[igNoKeys].pclField[0],&rgXml.rlLine[ilI].pclFieldD[igIntfIdx][0]);
              else
                 if (strlen(&rgXml.rlLine[ilI].pclFieldD[0][0]) > 0)
                    strcpy(&rgKeys[igNoKeys].pclField[0],&rgXml.rlLine[ilI].pclFieldD[0][0]);
           }
           strcpy(&rgKeys[igNoKeys].pclType[0],&rgXml.rlLine[ilI].pclType[0]);
           strcpy(&rgKeys[igNoKeys].pclMethod[0],&rgXml.rlLine[ilI].pclMethod[igIntfIdx][0]);
           strcpy(&rgKeys[igNoKeys].pclData[0],&rgXml.rlLine[ilI].pclData[0]);
           igNoKeys++;
        }
     }
  }
  else
  {
     if (ilUrnoRcv == FALSE)
     {
        if (strcmp(&rgInterface[igIntfIdx].pclIntfName[0],"ATC") != 0 && *pcpFkt != 'I')
        {
           dbg(TRACE,"%s No ADID and no URNO received!!!",pclFunc);
           /*ilRC = RC_FAIL;*/
        }
     }
  }
/*
  for (ilI = 0; ilI < igNoKeys; ilI++)
  {
     dbg(DEBUG,"%s Key: <%s> <%s> <%s> <%s>",pclFunc,
         &rgKeys[ilI].pclField[0],&rgKeys[ilI].pclType[0],
         &rgKeys[ilI].pclMethod[0],&rgKeys[ilI].pclData[0]);
  }
*/

  return ilRC;
} /* End of GetKeys */


static int ShowKeys()
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "ShowKeys:";
  int ilI;

  for (ilI = 0; ilI < igCurXmlLines; ilI++)
  {
     if (strcmp(&rgXml.rlLine[ilI].pclTag[0],"KEY") == 0 && rgXml.rlLine[ilI].ilRcvFlag == 1)
     {
        dbg(DEBUG,"%s Key: <%s> <%s> <%s> <%s>",pclFunc,
            &rgXml.rlLine[ilI].pclName[0],&rgXml.rlLine[ilI].pclType[0],
            &rgXml.rlLine[ilI].pclMethod[0],&rgXml.rlLine[ilI].pclData[0]);
     }
  }

  return ilRC;
} /* End of ShowKeys */


static int GetData(char *pcpMethod, int ipIdx, char *pcpName)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "GetData:";
  int ilI;
  int ilJ;

  igNoData = ipIdx;
  ilJ = 0;
  while (ilJ < igCurXmlLines && strcmp(&rgXml.rlLine[ilJ].pclName[0],pcpName) != 0)
     ilJ++;
  for (ilI = ilJ + 1; ilI < igCurXmlLines; ilI++)
  {
     if (strcmp(&rgXml.rlLine[ilI].pclTag[0],"DAT") == 0 && rgXml.rlLine[ilI].ilRcvFlag == 1 &&
         rgXml.rlLine[ilI].ilPrcFlag == 0  && strcmp(&rgXml.rlLine[ilI].pclType[0],"IGNORE") != 0)
     {
        strcpy(&rgData[igNoData].pclField[0],&rgXml.rlLine[ilI].pclName[0]);
        if (*pcgAdid == 'A' || *pcgAdid == '\0')
        {
           if (strlen(&rgXml.rlLine[ilI].pclFieldA[igIntfIdx][0]) > 0)
              strcpy(&rgData[igNoData].pclField[0],&rgXml.rlLine[ilI].pclFieldA[igIntfIdx][0]);
        }
        if (*pcgAdid == 'D')
        {
           if (strlen(&rgXml.rlLine[ilI].pclFieldD[igIntfIdx][0]) > 0)
              strcpy(&rgData[igNoData].pclField[0],&rgXml.rlLine[ilI].pclFieldD[igIntfIdx][0]);
        }
        strcpy(&rgData[igNoData].pclType[0],&rgXml.rlLine[ilI].pclType[0]);
        strcpy(&rgData[igNoData].pclMethod[0],&rgXml.rlLine[ilI].pclMethod[igIntfIdx][0]);
        strcpy(&rgData[igNoData].pclData[0],&rgXml.rlLine[ilI].pclData[0]);
        strcpy(&rgData[igNoData].pclOldData[0],"");
        if (strlen(pcpMethod) > 0)
        {
           if (strcmp(&rgData[igNoData].pclMethod[0],pcpMethod) == 0)
           {
              rgXml.rlLine[ilI].ilPrcFlag = 1;
              igNoData++;
           }
        }
        else
        {
           rgXml.rlLine[ilI].ilPrcFlag = 1;
           igNoData++;
        }
     }
     if (strcmp(&rgXml.rlLine[ilI].pclName[0],pcpName) == 0)
        ilI = igCurXmlLines;
  }
/*
  if (igNoData > ipIdx)
  {
     for (ilI = 0; ilI < igNoData; ilI++)
     {
        if (strcmp(&rgData[ilI].pclType[0],"URNO") == 0)
           strcpy(&rgData[ilI].pclField[0],"URNO");
        if (strcmp(&rgData[ilI].pclType[0],"FLNU") == 0)
           strcpy(&rgData[ilI].pclField[0],"FLNU");
        if (strcmp(&rgData[ilI].pclType[0],"RAID") == 0)
           strcpy(&rgData[ilI].pclField[0],"RAID");
        if (strcmp(&rgData[ilI].pclType[0],"ALCD") == 0)
           strcpy(&rgData[ilI].pclField[0],"ALCD");
        dbg(DEBUG,"%s Data: <%s> <%s> <%s> <%s>",pclFunc,
            &rgData[ilI].pclField[0],&rgData[ilI].pclType[0],
            &rgData[ilI].pclMethod[0],&rgData[ilI].pclData[0]);
     }
  }
*/

  return ilRC;
} /* End of GetData */


static int ShowData()
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "ShowData:";
  int ilI;

  for (ilI = 0; ilI < igCurXmlLines; ilI++)
  {
     if (strcmp(&rgXml.rlLine[ilI].pclTag[0],"DAT") == 0 && rgXml.rlLine[ilI].ilRcvFlag == 1)
     {
        dbg(DEBUG,"%s Data: <%s> <%s> <%s> <%s>",pclFunc,
            &rgXml.rlLine[ilI].pclName[0],&rgXml.rlLine[ilI].pclType[0],
            &rgXml.rlLine[ilI].pclMethod[0],&rgXml.rlLine[ilI].pclData[0]);
     }
  }

  return ilRC;
} /* End of ShowData */


static int ShowAll()
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "ShowAll:";
  int ilI;

  for (ilI = 0; ilI < igCurXmlLines; ilI++)
  {
     if (rgXml.rlLine[ilI].ilRcvFlag == 1)
     {
        dbg(DEBUG,"%s (%d) <%s> <%s> <%s>",pclFunc,
            rgXml.rlLine[ilI].ilLevel,&rgXml.rlLine[ilI].pclTag[0],
            &rgXml.rlLine[ilI].pclName[0],&rgXml.rlLine[ilI].pclData[0]);
     }
  }

  return ilRC;
} /* End of ShowAll */


/* ****************************************************************** */
/* The ConnectTCP routine                                             */
/* ****************************************************************** */
static int ConnectTCP (char *pcpHost,int ipTry)
{
  int ilRc = RC_FAIL;	/*Return code*/
  time_t tlNow = 0;
  int ilLen = 0;

  tlNow = time(NULL);
  if (*pcpHost != 0x00)
  {
     /* try to establish the connection */
     if ((bgSocketOpen == FALSE) && (tlNow > tgNextConnect))
     {
        dbg(DEBUG,"ConnectTCP: Try Nr.%d!",ipTry);
        igSockCreate = tcp_create_socket(SOCK_STREAM,prgCfg.service_port);
        if (igSockCreate < 0)
        {
           dbg(TRACE,"ConnectTCP: Create socket failed: %s.",strerror(errno));
           ilRc = RC_FAIL;
        }
        else
        {
           dbg(DEBUG,"ConnectTCP: Create socket <ID=%d> successfull.",igSockCreate);
           listen(igSockCreate,20);
           ilLen = sizeof(my_client);
           alarm(CONNECT_TIMEOUT);
           igSock = accept(igSockCreate,(struct sockaddr*)&my_client,&ilLen);
           alarm(0);
           if (igSock < 0)
           {
              if (errno == EINTR)
                 dbg(TRACE,"ConnectTCP: accept timeout: %s:",strerror(errno));
              else
                 dbg(TRACE,"ConnectTCP: accept failed: %s:",strerror(errno));

              /*shutdown(igSock,2);*/
              close(igSockCreate);
              bgSocketOpen = FALSE;
              ilRc = RC_FAIL;
              if (ipTry == 2)
              {
                 tlNow = time(NULL);
                 tgNextConnect = tlNow + atol(prgCfg.try_reconnect);
                 dbg(DEBUG,"ConnectTCP: next try in %ld sec.!",(tgNextConnect-tlNow));
              }
           }
           else
           {
              bgSocketOpen = TRUE;
              igMsgCounter = 0;
              ilRc = RC_SUCCESS;
              dbg(TRACE,"ConnectTCP: open connection to <%s> successfull.",pcpHost);
              tlNow = time(NULL);
              tgRecvTimeOut = tlNow + (time_t)(igRecvTimeout - READ_TIMEOUT);
              strcpy(pcgDGS_Host,pcpHost);
           }
        }
     }
  }
  else
  {
     dbg(TRACE,"ConnectTCP: wrong DGS_HOST <%s> entry!",pcpHost);
  }
  return ilRc;
} 


/* ******************************************************************** */
/* Following the poll_q_and_sock function				*/
/* Waits for input on the socket and polls the QCP for messages*/
/* ******************************************************************** */
static int poll_q_and_sock () 
{
  int ilRc;
  int ilRc_Connect = RC_FAIL;
  char pclTmpBuf[128];
  int ilI;

  do
  {
     nap(100); /* waiting because of NOT-waiting QUE_GETBIGNW */
     /*---------------------------*/
     /* now looking on ceda-queue */
     /*---------------------------*/
    ilRc = RC_NOT_FOUND;
    if (ilRc == RC_NOT_FOUND)
    {
       while ((ilRc = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,igItemLen,
                          (char *) &prgItem)) == RC_SUCCESS)
       {
          /* depending on the size of the received item  */
          /* a realloc could be made by the que function */
          /* so do never forget to set event pointer !!! */
          prgEvent = (EVENT *) prgItem->text;

          /* Acknowledge the item */
          ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
          if (ilRc != RC_SUCCESS) 
          {
             /* handle que_ack error */
             HandleQueErr(ilRc);
          }

          switch(prgEvent->command)
          {
             case HSB_STANDBY	:
                ctrl_sta = prgEvent->command;
                dbg(TRACE,"PQS: received HSB_STANDBY event!");
                HandleQueues();
                break;	
             case HSB_COMING_UP	:
                ctrl_sta = prgEvent->command;
                dbg(TRACE,"PQS: received HSB_COMING_UP event!");
                HandleQueues();
                break;	
             case HSB_ACTIVE	:
                ctrl_sta = prgEvent->command;
                dbg(TRACE,"PQS: received HSB_ACTIVE event!");
                break;	
             case HSB_ACT_TO_SBY	:
                ctrl_sta = prgEvent->command;
                dbg(TRACE,"PQS: received HSB_ACT_TO_SBY event!");
                HandleQueues();
                break;	
             case HSB_DOWN	:
                /* whole system shutdown - do not further use que(), */
                /* send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                dbg(TRACE,"PQS: received HSB_DOWN event!");
                Terminate(FALSE);
                break;	
             case HSB_STANDALONE	:
                ctrl_sta = prgEvent->command;
                dbg(TRACE,"PQS: received HSB_STANDALONE event!");
                break;	
             case REMOTE_DB :
                /* ctrl_sta is checked inside */
                /*HandleRemoteDB(prgEvent);*/
                break;
             case SHUTDOWN	:
                /* process shutdown - maybe from uutil */
                dbg(TRACE,"PQS: received SHUTDOWN event!");
                Terminate(FALSE);
                break;
             case RESET		:
                dbg(TRACE,"PQS: received RESET event!");
                ilRc = Reset();
                if (ilRc == RC_FAIL)
                   Terminate(FALSE);
                break;
             case EVENT_DATA	:
                if ((ctrl_sta == HSB_STANDALONE) ||
                    (ctrl_sta == HSB_ACTIVE) ||
                    (ctrl_sta == HSB_ACT_TO_SBY))
                {
                   if (bgSocketOpen == TRUE ||
                       strcmp(prgCfg.mode,"TEST") == 0)
                   {
                      ilRc = HandleInternalData();
                      if (ilRc!=RC_SUCCESS)
                      {
                         dbg(TRACE,"PQS: HandleInternalData failed <%d>",ilRc);
                         HandleErr(ilRc);
                      }
                   }
                   else
                   {
                      dbg(TRACE,"PQS: No DGS-connection! Ignoring event!");
                   }
                }
                else
                {
                   dbg(TRACE,"poll_q_and_sock: wrong HSB-status <%d>",ctrl_sta);
                   DebugPrintItem(TRACE,prgItem);
                   DebugPrintEvent(TRACE,prgEvent);
                }/* end of if */
                break;
             case TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
             case TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
             case 100: /* Simulate Keep Alive Message */
                TimeToStr(pcgCurrentTime,time(NULL));
                sprintf(pcgTestMessage,"3 DGS %s  I0",pcgCurrentTime);
                break;
             case 101: /* Simulate Ack Message */
                TimeToStr(pcgCurrentTime,time(NULL));
                sprintf(pcgTestMessage,"3 DGS %s  L0",pcgCurrentTime);
                break;
             case 102: /* Simulate OnBlock/OffBlock Message */
                TimeToStr(pcgCurrentTime,time(NULL));
                iGetConfigEntry(pcgConfigFile,"MAIN","TEST_MESSAGE",CFG_STRING,pclTmpBuf);
                sprintf(pcgTestMessage,"%dDGS %s  %s",strlen(pclTmpBuf)+1,pcgCurrentTime,pclTmpBuf);
                for (ilI = 0; ilI < strlen(pcgTestMessage); ilI++)
                   if (pcgTestMessage[ilI] == '_')
                      pcgTestMessage[ilI] = ' ';
                break;
             default			:
                dbg(TRACE,"MAIN: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
          } /* end switch */
       }/* end while */ 
       ilRc = RC_FAIL; /* proceed */
    }

    if (bgSocketOpen == TRUE)
    {
       ilRc = Receive_data(igSock,READ_TIMEOUT,"MAIN");
       if (ilRc != RC_FAIL)
          ilRc = SendKeepAliveMsg();
    }
    else
    {
       if (strcmp(prgCfg.mode,"TEST") == 0)
       {
          ilRc = Receive_data(igSock,READ_TIMEOUT,"MAIN");
          if (ilRc != RC_FAIL)
             ilRc = SendKeepAliveMsg();
       }
       ilRc = RC_FAIL;
    }

    /* now trying to reconnect if there was a failure */
    if (ilRc == RC_FAIL)
    {
       CloseTCP(); /* for cleaning up the connections */

       strcpy(pcgDGS_Host,prgCfg.dgs_host1);
       ilRc_Connect = ConnectTCP(prgCfg.dgs_host1,1);

    }
 } while (ilRc != RC_SUCCESS);
  return ilRc;
} 


/* ***************************************************************** */
/* The Receive_data routine                                          */
/* Rec data from DGS into global buffer (and into a file )           */
/* ***************************************************************** */
static int Receive_data(int ipSock,int ipTimeOut,char *pcpCalledBy)
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "Receive_data:";
  int ilRCdb = DB_SUCCESS;
  short slFkt;
  short slCursor;
  char pclSqlBuf[1024];
  char pclDataBuf[1024];
  char pclFieldList[1024];
  char pclSelection[1024];
  int ilNo;
  int ilByteCnt;
  char pclTmpBuf[128];
  int ilDataBytes;
  char pclMsgLen[16];
  char pclFlno[32];
  char pclStad[32];
  char pclOblt[32];
  char pclPos[32];
  char pclAct5[32];
  char pclRegn[32];
  char pclAftUrno[32];
  char pclAftAdid[32];
  char pclAftLand[32];
  char pclAftOnbl[32];
  char pclAftOfbl[32];
  char pclAftPsta[32];
  char pclAftPstd[32];
  char pclAftAct5[32];
  char pclAftRegn[32];
  char pclCurrentTime[32];

  memset(pcgRecvBuffer,0x00,sizeof(MAX_TELEGRAM_LEN));
  ilByteCnt = 0;

  ilNo = 0;
  bgAlarm = FALSE;
  memset(pclTmpBuf,0x00,sizeof(pclTmpBuf));
  errno = 0;
  alarm(ipTimeOut);
  ilNo = read(ipSock,&pclTmpBuf[0],6);
  alarm(0);
  if (strcmp(prgCfg.mode,"TEST") == 0)
  {
     if (strlen(pcgTestMessage) > 0)
     {
        ilNo = 6;
        strncpy(pclTmpBuf,pcgTestMessage,ilNo);
     }
     else
        return RC_NOT_FOUND;
  }
  if (ilNo <= 0 && bgAlarm == FALSE)
  {  /* <0 means failure at read if no alarm appeared, =0 means connection lost*/
     dbg(TRACE,"Receive_data: OS-ERROR read(%d) on socket <%d>",ilNo,ipSock);
     dbg(TRACE,"Receive_data: ERRNO (%d)=(%s)",errno,strerror(errno));
     return RC_FAIL;
  }
  if (ilNo < 0 && bgAlarm == TRUE)
  {  /* <0 means no data read inside READ_TIMEOUT time */
     TimeToStr(pclCurrentTime,time(NULL));
     if (strcmp(pclCurrentTime,pcgLastMessage) > 0)
     {
        dbg(TRACE,"Receive_data: Message missing from DGS, Now = <%s>, Last = <%s>",pclCurrentTime,pcgLastMessage);
        return RC_FAIL;
     }
     else
        return RC_NOT_FOUND;
  }
  dbg(DEBUG,"========================= START ===============================");
  if (strcmp(&pclTmpBuf[2],"DGS ") == 0)
  {  /* message from DGS */
     TimeToStr(pcgLastMessage,time(NULL));
     AddSecondsToCEDATime(pcgLastMessage,igKeepAlive,1);
     strncpy(pclMsgLen,pclTmpBuf,2);
     if (pclMsgLen[1] == ' ')
        pclMsgLen[1] = '\0';
     else
        pclMsgLen[2] = '\0';
     ilDataBytes = atoi(pclMsgLen);
     dbg(DEBUG,"%s Receiving %d bytes of data",pclFunc,ilDataBytes);
     strcpy(pcgRecvBuffer,pclTmpBuf);
     ilByteCnt = ilDataBytes + 16;
     errno = 0;
     alarm(ipTimeOut);
     ilNo = read(ipSock,&pcgRecvBuffer[6],ilByteCnt);
     alarm(0);
     if (strcmp(prgCfg.mode,"TEST") == 0)
     {
        if (strlen(pcgTestMessage) > 0)
        {
           ilNo = ilByteCnt;
           strncpy(&pcgRecvBuffer[6],&pcgTestMessage[6],ilNo);
           strcpy(pcgTestMessage,"");
        }
     }
     if (ilNo <= 0 && bgAlarm == FALSE)
     {  /* <0 means failure at read if no alarm appeared, =0 means connection lost*/
        dbg(TRACE,"Receive_data: OS-ERROR read(%d) on socket <%d>",ilNo,ipSock);
        dbg(TRACE,"Receive_data: ERRNO (%d)=(%s)",errno,strerror(errno));
        dbg(DEBUG,"========================= END ================================");
        return RC_FAIL;
     }
     dbg(DEBUG,"%s Complete Message: %d <%s>",pclFunc,ilNo,pcgRecvBuffer);
     if (pgReceiveLogFile)
     {
        fwrite(pcgRecvBuffer,sizeof(char),strlen(pcgRecvBuffer),pgReceiveLogFile);
        fwrite("\n",sizeof(char),1,pgReceiveLogFile);
        fflush(pgReceiveLogFile);
     }
     strncpy(pclTmpBuf,&pcgRecvBuffer[22],2);
     pclTmpBuf[2] = '\0';
     if (strcmp(pclTmpBuf,"I0") == 0)
     {
        dbg(DEBUG,"%s Keep Alive Message received",pclFunc);
     }
     else if (strcmp(pclTmpBuf,"L0") == 0)
     {
        dbg(DEBUG,"%s Acknowledge Message received",pclFunc);
     }
     else if (strcmp(pclTmpBuf,"G1") == 0 || strcmp(pclTmpBuf,"G2") == 0)
     {
        dbg(DEBUG,"%s OnBlock / OffBlock Message received",pclFunc);
        SendAckMsg();
        strncpy(pclPos,&pcgRecvBuffer[26],6);
        pclPos[6] = '\0';
        TrimRight(pclPos);
        strncpy(pclAct5,&pcgRecvBuffer[32],6);
        pclAct5[6] = '\0';
        TrimRight(pclAct5);
        strncpy(pclFlno,&pcgRecvBuffer[38],10);
        pclFlno[10] = '\0';
        TrimRight(pclFlno);
        CheckFlightNo(pclFlno);
        strncpy(pclRegn,&pcgRecvBuffer[48],10);
        pclRegn[10] = '\0';
        TrimRight(pclRegn);
        strncpy(pclStad,&pcgRecvBuffer[58],14);
        pclStad[14] = '\0';
        TrimRight(pclStad);
        strncpy(pclOblt,&pcgRecvBuffer[72],14);
        pclOblt[14] = '\0';
        TrimRight(pclOblt);
        dbg(TRACE,"%s Flight: <%s> <%s> <%s> <%s> <%s> <%s>",
            pclFunc,pclFlno,pclStad,pclOblt,pclPos,pclAct5,pclRegn);
        strcpy(pclFieldList,"URNO,ADID,LAND,ONBL,OFBL,PSTA,PSTD,ACT5,REGN");
        sprintf(pclSelection,
                "WHERE FLNO = '%s' AND ((STOA = '%s' AND DES3 = '%s') OR (STOD = '%s' AND ORG3 = '%s'))",
                pclFlno,pclStad,pcgHomeAp,pclStad,pcgHomeAp);
        sprintf(pclSqlBuf,"SELECT %s FROM AFTTAB %s",pclFieldList,pclSelection);
        slCursor = 0;
        slFkt = START;
        dbg(DEBUG,"%s SQL = <%s>",pclFunc,pclSqlBuf);
        ilRCdb = sql_if(slFkt,&slCursor,pclSqlBuf,pclDataBuf);
        close_my_cursor(&slCursor);
        if (ilRCdb == DB_SUCCESS)
        {
           BuildItemBuffer(pclDataBuf,"",9,",");
           get_real_item(pclAftUrno,pclDataBuf,1);
           TrimRight(pclAftUrno);
           get_real_item(pclAftAdid,pclDataBuf,2);
           TrimRight(pclAftAdid);
           get_real_item(pclAftLand,pclDataBuf,3);
           TrimRight(pclAftLand);
           get_real_item(pclAftOnbl,pclDataBuf,4);
           TrimRight(pclAftOnbl);
           get_real_item(pclAftOfbl,pclDataBuf,5);
           TrimRight(pclAftOfbl);
           get_real_item(pclAftPsta,pclDataBuf,6);
           TrimRight(pclAftPsta);
           get_real_item(pclAftPstd,pclDataBuf,7);
           TrimRight(pclAftPstd);
           get_real_item(pclAftAct5,pclDataBuf,8);
           TrimRight(pclAftAct5);
           get_real_item(pclAftRegn,pclDataBuf,9);
           TrimRight(pclAftRegn);
           dbg(DEBUG,"%s Flight Found <%s> <%s> <%s> <%s> <%s> <%s> <%s> <%s> <%s>",pclFunc,
               pclAftUrno,pclAftAdid,pclAftLand,pclAftOnbl,pclAftOfbl,pclAftPsta,pclAftPstd,
               pclAftAct5,pclAftRegn);
           sprintf(pclSelection,"WHERE URNO = %s",pclAftUrno);
           if (strcmp(pclTmpBuf,"G1") == 0)
              strcpy(pclFieldList,prgCfg.fn_onbl);
           else
              strcpy(pclFieldList,prgCfg.fn_ofbl);
           dbg(DEBUG,"%s Send UFR with:",pclFunc);
           dbg(DEBUG,"%s Selection: <%s>",pclFunc,pclSelection);
           dbg(DEBUG,"%s Fields:    <%s>",pclFunc,pclFieldList);
           dbg(DEBUG,"%s Data:      <%s>",pclFunc,pclOblt);
           ilRC = SendCedaEvent(7800,0,"DGS","CEDA",pcgTwStart,pcgTwEnd,
                                "UFR","AFTTAB",pclSelection,pclFieldList,pclOblt,"",3,NETOUT_NO_ACK);
        }
        else
        {
           dbg(DEBUG,"%s No Flight Found!",pclFunc);
        }
     }
     else
     {
        dbg(DEBUG,"%s Invalid Message received",pclFunc);
     }
  }
  else
  {  /* incorrect message */
     dbg(TRACE,"%s Incorrect Message Part 1: <%s>",pclFunc,pclTmpBuf);
     dbg(DEBUG,"========================= END ================================");
     return RC_FAIL;
  }
  dbg(DEBUG,"========================= END ================================");

  return ilRC;
}


/* ***************************************************************** */
/* The CloseTCP routine                                              */
/* ***************************************************************** */
static void CloseTCP()
{
  time_t tlNow = 0;
  *pcgDGS_Host = 0x00;

   if (igSock != 0 || igSockCreate != 0)
   {
      dbg(DEBUG,"CloseTCP: closing TCP/IP connection!");
      /*shutdown(igSock,2);*/
      if (igSock != 0)
         close(igSock);
      if (igSockCreate != 0)
         close(igSockCreate);
      tlNow = time(NULL);
      tgNextConnect = tlNow + atol(prgCfg.try_reconnect);
      dbg(TRACE,"CloseTCP: connection closed! Wait (%s) sec. before reconnect",prgCfg.try_reconnect);
      sleep(atol(prgCfg.try_reconnect));
      igSock = 0;
      igSockCreate = 0;
      bgSocketOpen = FALSE;
      /*Terminate(1);*/
   }
   igSock = 0;
   igSockCreate = 0;
   bgSocketOpen = FALSE;
   strcpy(pcgLastMessage,"20201231235959");

}


/* **************************************************************** */
/* The snapit routine                                               */
/* snaps data if the debug-level is set to DEBUG                    */
/* **************************************************************** */
static void snapit(void *pcpBuffer,long lpDataLen,FILE *pcpDbgFile)
{
  if (debug_level==DEBUG)
     snap((char*)pcpBuffer,(long)lpDataLen,(FILE*)pcpDbgFile);
}


/* ******************************************************************** */
/* The GetCfgEntry() routine                                            */
/* ******************************************************************** */
static int GetCfgEntry(char *pcpFile,char *pcpSection,char *pcpEntry,short spType,char **pcpDest,
                       int ipValueType,char *pcpDefVal)
{
  int ilRc = RC_SUCCESS;
  char pclCfgLineBuffer[L_BUFF];

  memset(pclCfgLineBuffer,0x00,L_BUFF);
  if ((ilRc=iGetConfigRow(pcpFile,pcpSection,pcpEntry,spType,pclCfgLineBuffer)) != RC_SUCCESS)
  {
     dbg(TRACE,"GetCfgEntry: reading entry <%s> failed.",pcpEntry);
     dbg(TRACE,"GetCfgEntry: EMPTY <%s>! Use default <%s>.",pcpEntry,pcpDefVal);
     strcpy(pclCfgLineBuffer,pcpDefVal);
  }
  else
  {
     if (strlen(pclCfgLineBuffer) < 1)
     {
        dbg(TRACE,"GetCfgEntry: EMPTY <%s>! Use default <%s>.",pcpEntry,pcpDefVal);
        strcpy(pclCfgLineBuffer,pcpDefVal);
     }
  }
  *pcpDest = malloc(strlen(pclCfgLineBuffer)+1);
  strcpy(*pcpDest,pclCfgLineBuffer);
  dbg(TRACE,"GetCfgEntry: %s = <%s>",pcpEntry,*pcpDest);
  if ((ilRc = CheckValue(pcpEntry,*pcpDest,ipValueType)) != RC_SUCCESS)
  {
     dbg(TRACE,"GetCfgEntry: please correct value <%s>!",pcpEntry);
  }
  return ilRc;
}


/* ******************************************************************** */
/* The CheckValue() routine
/* ******************************************************************** */
static int CheckValue(char *pcpEntry,char *pcpValue,int ipType)
{
  int ilRc = RC_SUCCESS;

  switch(ipType)
  {
     case CFG_NUM:
        while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
        {
           if (isdigit(*pcpValue)==0)
           {
              dbg(TRACE,"CheckValue : NOT A NUMBER! <%s>!",pcpEntry);
              ilRc = RC_FAIL;
           }
           pcpValue++;
        }
        break;
     case CFG_ALPHA:
        while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
        {
           if (isalpha(*pcpValue)==0)
           {
              dbg(TRACE,"CheckValue : <%c> INVALID in <%s>-entry!",*pcpValue,pcpEntry);
              ilRc = RC_FAIL;
           }
           pcpValue++;
        }
        break;
     case CFG_ALPHANUM:
        while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
        {
           if (isalnum(*pcpValue)==0)
           {
              dbg(TRACE,"CheckValue : <%c> INVALID in <%s>-entry!",*pcpValue,pcpEntry);
              ilRc = RC_FAIL;
           }
           pcpValue++;
        }
        break;
     case CFG_PRINT:
        while(*pcpValue != 0x00 && ilRc==RC_SUCCESS)
        {
           if (isprint(*pcpValue)==0)
           {
              dbg(TRACE,"CheckValue : <%c> INVALID in <%s>-entry!",*pcpValue,pcpEntry);
              ilRc = RC_FAIL;
           }
           pcpValue++;
           }
        break;
     default:
        break;
  }
  return ilRc;
}


/* **************************************************************** */
/* The Send_data routine                                            */
/* Sends telegrams to the DGS-system                                */
/* **************************************************************** */
static int Send_data(int ipSock,char *pcpData,int ipLen)
{
  int ilRc=RC_SUCCESS;
  int ilBytes = 0;
  char pclCurrentTime[32];

  /* ipSock = opened socket */
  /* ilBytes = number of bytes that have to be transmitted. */
  ilBytes = ipLen;

  /* Build header */
  if (ilBytes > 9)
     sprintf(pcgSendBuffer,"%d",ilBytes);
  else
     sprintf(pcgSendBuffer,"%d ",ilBytes);
  strcat(pcgSendBuffer,"DGS ");
  TimeToStr(pclCurrentTime,time(NULL));
  strcat(pcgSendBuffer,pclCurrentTime);
  strcat(pcgSendBuffer,"  ");
  ilBytes += strlen(pcgSendBuffer);
  strcat(pcgSendBuffer,pcpData);

  if (ipSock != 0)
  {
     errno = 0;
     alarm(WRITE_TIMEOUT);
     ilRc = write(ipSock,pcgSendBuffer,ilBytes);/*unix*/
     alarm(0);

     if (ilRc == -1)
     {
        if (bgAlarm == FALSE)
        {
           dbg(TRACE,"Send_data: Write failed: Socket <%d>! <%d>=<%s>",
               ipSock,errno,strerror(errno));
           ilRc = RC_FAIL;
        }
        else
        {
           bgAlarm = FALSE;
           ilRc = RC_FAIL;
        }
        CloseTCP();
     }
     else
     {
        dbg(DEBUG,"Send_data: wrote <%d> Bytes to socket Nr.<%d>.",ilRc,ipSock);
        snapit((char *)pcgSendBuffer,ilBytes,outp);
        fflush(outp);
        if (pgSendLogFile)
        {
           fwrite(pcgSendBuffer,sizeof(char),ilBytes,pgSendLogFile);
           fwrite("\n",sizeof(char),1,pgSendLogFile);
           fflush(pgSendLogFile);
        }
        ilRc = RC_SUCCESS;
     }
  }
  else
  {
     if (strcmp(prgCfg.mode,"TEST") == 0)
     {
        dbg(DEBUG,"Send_data: wrote <%d> Bytes to dummy-socket.",ilBytes);
        snapit((char *)pcgSendBuffer,ilBytes,outp);
        fflush(outp);
        if (pgSendLogFile)
        {
           fwrite(pcgSendBuffer,sizeof(char),ilBytes,pgSendLogFile);
           fwrite("\n",sizeof(char),1,pgSendLogFile);
           fflush(pgSendLogFile);
        }
        ilRc = RC_SUCCESS;
     }
     else
     {
        dbg(TRACE,"Send_data: No connection to <%s>! Can't send!",pcgDGS_Host);
        ilRc = RC_FAIL;
     }
  }
  TimeToStr(pclCurrentTime,time(NULL));
  AddSecondsToCEDATime(pclCurrentTime,igKeepAlive,1);
  strcpy(pcgNextKeepAlive,pclCurrentTime);
  return ilRc;
}


static int SendKeepAliveMsg()
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SendKeepAliveMsg:";
  char pclCurrentTime[32];
  char pclDataBuf[16];

  ilRC = TimeToStr(pclCurrentTime,time(NULL));
  if (strcmp(pcgNextKeepAlive,pclCurrentTime) <= 0)
  {
     dbg(DEBUG,"========================= START ===============================");
     dbg(DEBUG,"%s Send Keep Alive Msg",pclFunc);
     strcpy(pclDataBuf,"I2");
     ilRC = Send_data(igSock,pclDataBuf,strlen(pclDataBuf)+1);
     dbg(DEBUG,"========================= END ================================");
  }

  return ilRC;
} /* End of SendKeepAliveMsg */


static int SendAckMsg()
{
  int ilRC = RC_SUCCESS;
  char pclFunc[] = "SendAckMsg:";
  char pclDataBuf[16];

  dbg(DEBUG,"========================= START ===============================");
  dbg(DEBUG,"%s Send Ack Msg",pclFunc);
  strcpy(pclDataBuf,"G0");
  ilRC = Send_data(igSock,pclDataBuf,strlen(pclDataBuf)+1);
  dbg(DEBUG,"========================= END ================================");

  return ilRC;
} /* End of SendAckMsg */

/* AKL Last Version */

