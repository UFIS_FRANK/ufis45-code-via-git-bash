#ifndef _DEF_mks_version_tcpudp_h
  #define _DEF_mks_version_tcpudp_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_tcpudp_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/tcpudp.h 1.2 2004/07/27 17:04:38SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/* The Master include file.                                               */
/*                                                                        */
/* Program       :                                                        */
/* Revision date :                                                        */
/* Author        :                                                        */
/*                                                                        */
/* NOTE : This should be the only include file for your program           */
/* ********************************************************************** */

#ifndef __TCPUDP_INC
#define __TCPUDP_INC


#include <sys/types.h>
#include <sys/times.h>
#ifdef _UNIXWARE
#include <sys/time.h>
#endif
#ifndef _HPUX_SOURCE
#include <sys/select.h>
#endif
#include <sys/signal.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <malloc.h>

#include "glbdef.h"
#include "ugccsma.h"


int TcpAccept(int *pipSocket, char *pcpService, char *pcpHost);
int TcpConnect(int *pipSocket, char *pcpService, char *pcpHost);
int TcpRecv(int ipSocket, char **pcpBuf, int *pipBufLen);
int TcpSend(int ipSocket, char *pcpBuf, int ipBufLen);
int TcpSendKeepalive(int ipSocket);
int TcpSendAck(int ipSocket);
int TcpRecvAck(int ipSocket, int ipTimeout);
int TcpClose(int ipSocket);

#endif

