<?php
session_name("RMS");
session_start();
session_register("Session");

function IsNumeric($n)
{
  $p=trim($n);
  $l=strlen($p);

  for ($t=0; $t<$l; $t++)
  {
    $c=substr($p,$t,1);
    if ($c<'0' || $c>'9')
    { if ($c!='.') return 0; }
  }

  return 1;
}

function len($str) {
        return strlen($str);
}

function DayOfWeek ($str) {

$year = substr ($str, 0, 4); 
$month = substr ($str, 4,2); 
$day = substr ($str, 6, 2);         
$hour = substr ($str, 8, 2); 
$min = substr ($str, 10, 2); 
$sec = substr ($str, 12, 2); 
	

$date=mktime ($hour,$min,$sec,$month,$day,$year);    

$date_time_array  = getdate($date);
$hours =  $date_time_array["hours"];
$minutes =  $date_time_array["minutes"];
$seconds =  $date_time_array["seconds"];
$month =  $date_time_array["mon"];
$day =  $date_time_array["mday"];
$year =  $date_time_array["year"];

$wday =  $date_time_array["wday"];
$weekday =  $date_time_array["weekday"];



$RwdayEn="";
$RwdayGr="";

	switch ($wday) {
        case 0:
            $RwdayEn="Su";
			$RwdayGr="��";
            break;        
        case 1:
            $RwdayEn="Mon";
			$RwdayGr="��";
            break;        
        case 2:
            $RwdayEn="Tu";
			$RwdayGr="ձ�";
            break;        
        case 3:
            $RwdayEn="We";
			$RwdayGr="Յ�";
            break;        
        case 4:
            $RwdayEn="Th";
			$RwdayGr="υ�";
            break;        
        case 5:
            $RwdayEn="Fr";
			$RwdayGr="�߱";
            break;        
        case 6:
            $RwdayEn="Sa";
			$RwdayGr="�߂";
            break;        
		
	}    
	
	return $RwdayEn;
}

    
function DateAdd ($interval,  $number, $date) {

$date_time_array  = getdate($date);

$hours =  $date_time_array["hours"];
$minutes =  $date_time_array["minutes"];
$seconds =  $date_time_array["seconds"];
$month =  $date_time_array["mon"];
$day =  $date_time_array["mday"];
$year =  $date_time_array["year"];


    switch ($interval) {
    
        case "yyyy":
            $year +=$number;
            break;        
        case "q":
            $year +=($number*3);
            break;        
        case "m":
            $month +=$number;
			$day=1;
            break;        
        case "y":
        case "d":
        case "w":
             $day+=$number;
            break;        
        case "ww":
             $day+=($number*7);
            break;        
        case "h":
             $hours+=$number;
            break;        
        case "n":

		 $h1=$number/60;
//echo $hours."--".$h1."After";
	     $hours+=$h1;
//echo $hours."--".$h1."<br>";
//             $minutes+=$number;
            break;        
        case "s":
             $seconds+=$number;
            break;        

    }    
	//echo "$hours ,$minutes, $seconds,$month ,$day, $year<br>";
    $timestamp =  mktime($hours ,$minutes, $seconds,$month ,$day, $year);


//	$date_time_array1  = 	getdate($timestamp);

//$hours =  $date_time_array1["hours"];
//$minutes =  $date_time_array1["minutes"];


    return $timestamp;
}


Function DateDiff ($interval, $date1,$date2) {

    // get the number of seconds between the two dates
$timedifference =  $date2 - $date1;
    
    switch ($interval) {
        case "w":
            $retval  = bcdiv($timedifference ,604800);
            break;
        case "d":
            $retval  = bcdiv($timedifference,86400);
            break;
        case "h":
             $retval = bcdiv($timedifference,3600);
            break;        
        case "n":
            $retval  = bcdiv($timedifference,60);
            break;        
        case "s":
            $retval  = $timedifference;
            break;        

    }    
    return $retval;
    
}


Function FindTDI () {
/* Time Diff */
    Global $dbU,$fromdate,$TDI1,$TDI2,$TICH;
 $cmdTempCommandText = "SELECT TICH,TDI1,TDI2   from APTTAB  where APC3='ATH'";
 $Currenttimediff=&$dbU->Execute($cmdTempCommandText);
    if (! $Currenttimediff->EOF)  {
    
//	        putenv( "TZ=Europe/Athens" );
			$str=$Currenttimediff->fields("TICH");
			$year = substr ($str, 0, 4); 
		    $month = substr ($str, 4,2); 
			$day = substr ($str, 6, 2); 
			
            $tich= ChopString ("n", 0,  $year.$month.$day."010000") ;

//            $tich= ChopString ("n", 0,  "20021030040000") ;

	    	$hh=gmstrftime("%H",time());
            $mm=gmstrftime("%M",time());
            $ss=gmstrftime("%S",time());

            $Month=gmstrftime("%m",time());
            $Day=gmstrftime("%d",time());
            $Year=gmstrftime("%Y",time());

            //$tnow=mktime($hh,$mm,$ss,$Month,$Day,$Year);
 
						
	    $TDI1=$Currenttimediff->fields("TDI1");
	    $TDI2=$Currenttimediff->fields("TDI2");
	    $TICH=$tich;

        if ($fromdate>$tich) {
			$tdi=  $Currenttimediff->fields("TDI2");
	    } else {
			$tdi=  $Currenttimediff->fields("TDI1");
        }
            
		//echo $tdi;
        return $tdi;        
            
      }// If End
    if ($timediff) $timediff->Close();
}

function ChopString ($interval,  $number, $str) {

    if (len($str)>0) {
    $year = substr ($str, 0, 4); 
    $month = substr ($str, 4,2); 
    $day = substr ($str, 6, 2);         
    $hour = substr ($str, 8, 2); 
    $min = substr ($str, 10, 2); 
    $sec = substr ($str, 12, 2); 
	

    $date=mktime ($hour,$min,$sec,$month,$day,$year);            
	} else {$date="";}
        

    if ($number!="0" && len($str)>0) {
		return DateAdd ($interval,  $number, $date) ;
    } else {
        return $date;    
    }
    
}




function OrgUnitTbl ($DPT1) {
    Global $db,$Session,$tpl,$login;
    $tmp="";
    /* Start  Getting the information from the user */
//    $tmp="(stoa BETWEEN '$fromhour' AND '$tohour')";

    /* Create the query */
    $fields=" DPT1,DPTN ";
    $table=" ORGTAB ";


	
    $wherest=$login->OrgUnitWhereSt($Session["cano"],$Session["sorcode"]) ;
	
    $cmdTempCommandText = "SELECT ".$fields." from ". $table.$wherest;
	//echo  $Session["cano"];
	if ($Session["cano"]=="SU01") {
		$Selected="";
		if ("All"==$DPT1) {
				$Selected=" Selected";
		}
		$tpl->set_var("Selected", $Selected);
			
		$tpl->set_var("DPT1", "All");
		$tpl->set_var("DPTN", "All");
		$tpl->parse("OrgUnit", true);
	}
    $rs = &$db->Execute($cmdTempCommandText );
	if (!$rs->EOF)  {
		while (!$rs->EOF) {
			$Selected="";
			if (trim($rs->fields("DPT1"))==$DPT1) {
				$Selected=" Selected";
			}
			$tpl->set_var("Selected", $Selected);
			
			$tpl->set_var("DPT1", trim($rs->fields("DPT1")));
			$tpl->set_var("DPTN", trim($rs->fields("DPT1"))."--".trim($rs->fields("DPTN")));
			
			$tpl->parse("OrgUnit", true);
			$rs->MoveNext();
		}// While End

	}// If End
	if ($rs)$rs->Close();

}




function FunctionTbl ($FCTC,$DPTC) {
    Global $db,$Session,$tpl,$login;
    $tmp="";
    /*  Start  Getting the information from the user      */
	//  $tmp="(stoa BETWEEN '$fromhour' AND '$tohour')";

    /* Create the query */
    $fields=" FCTC,FCTN";
    $table=" PFCTAB ";


    $wherest=$login->FunctionWhereSt($Session["cano"],$DPTC,$Session["spfcode"]);

    $cmdTempCommandText = "SELECT ".$fields." from ". $table.$wherest;
	//echo $cmdTempCommandText;

    $rs = &$db->Execute($cmdTempCommandText );
	if ($Session["cano"]=="SU01") {
		$Selected="";
		if ("All"==$FCTC) {
				$Selected=" Selected";
		}
		$tpl->set_var("Selected", $Selected);
			
		$tpl->set_var("FCTC", "'All'");
		$tpl->set_var("FCTN", "All");
		$tpl->parse("Function", true);
	}
	if (!$rs->EOF)  {
		while (!$rs->EOF) {
			$Selected="";
			
			$rsFCTC = trim($rs->fields("FCTC"));
			
			if (strpos($FCTC,$rsFCTC) >0 ) {
			//if (trim($rs->fields("FCTC"))==$FCTC) {
				$Selected=" Selected";
			}
			$tpl->set_var("Selected", $Selected);
			
			$tpl->set_var("FCTC", "'". trim($rs->fields("FCTC")) ."'");
			$tpl->set_var("FCTN", trim($rs->fields("FCTC")) ."--".trim($rs->fields("FCTN")));
			
			$tpl->parse("Function", true);
			$rs->MoveNext();
		}// While End

		
	}// If End
	if ($rs)$rs->Close();

}

Function StaffNames($OrgUniCode,$FunctionCode,$Today,$DateFrom) {
	Global $db,$Session,$tpl,$StaffTblArray,$login;
	$tmp="";
	$FldArraytmp=Array();
	
    /* Get The functions / Org Units for the Staff */

    /* Create the query */

	// Changed on 11-08-2005 
	// Change to check valid from , to For Function and Organization Unit
	// spftab.vpfr spfvpfr,spftab.vpto spfvpto,sortab.vpfr sorvpfr,sortab.vpto sorvpto"
	$fields = " distinct stftab.urno,stftab.finm, stftab.lanm, stftab.peno, spftab.code, sortab.code,stftab.shnm,spftab.vpfr spfvpfr,spftab.vpto spfvpto,sortab.vpfr sorvpfr,sortab.vpto sorvpto";
    $table = " sortab, spftab, stftab";//,drrtab ";
	$wherest = " where ";
	$wherest = $wherest. $login->StaffNamesWhereSt($Session["cano"],$Session["stfurno"]);
    $wherest = $wherest."  ((spftab.surn = stftab.urno) AND (sortab.surn = stftab.urno) )and ";
	// (drrtab.stfu = stftab.urno) ) and ";
	$wherest = $wherest. "( ('".$Today."' between spftab.vpfr and  spftab.vpto) or ('".$DateFrom."' between spftab.vpfr and  spftab.vpto) or (spftab.vpfr<= '".$Today."' and  spftab.vpto=' '  )) and ";
	$wherest = $wherest.  "( ('".$Today."' between sortab.vpfr and  sortab.vpto) or ('".$DateFrom."' between sortab.vpfr and  sortab.vpto) or (sortab.vpfr<= '".$Today."' and  sortab.vpto=' '  ))  ";
	
	if ($FunctionCode !="All") {
		$wherest = $wherest. " and (spftab.code in (".$FunctionCode.")  )"; 
	}  else {
		if ($OrgUniCode !="All") {$wherest = $wherest. " and (spftab.code in (select FCTC from pfctab where dptc='".$OrgUniCode."' ) )"; }
	}
	
	if ($OrgUniCode !="All") {$wherest = $wherest. " and sortab.code='".$OrgUniCode."' "; }

	//Last Working Day (DODM ,Date of separation from company)
	$wherest = $wherest. " and (stftab.DODM>='".$Today."' or stftab.DODM=' ')"; 
	
	// (DOEM ,Date of joining the company)
	$wherest = $wherest. " and (stftab.DOEM<='".$Today."' )"; 

	//$wherest = $wherest. " order by SUBSTR(Shnm,4,length(Shnm))";
	$wherest = $wherest. " order by Shnm";
	
    $cmdTempCommandText = "SELECT ".$fields." from ". $table.$wherest;

	//echo $cmdTempCommandText;
    $rs = &$db->Execute($cmdTempCommandText);
	
	if (!$rs->EOF)  {
		$MySurnoSql="";
		
		while (!$rs->EOF) {
			$Finm=trim($rs->fields("finm"));
			$Lanm=trim($rs->fields("lanm"));
			$Peno=trim($rs->fields("peno"));
			$Shnm=trim($rs->fields("Shnm"));
			$Surno=trim($rs->fields("urno"));
			
			// Changed on 11-08-2005 
			// Change to check valid from , to For Function and Organization Unit
			$Spfvpfr=trim($rs->fields("spfvpfr"));
			$Spfvpto=trim($rs->fields("spfvpto"));

			$Sorvpfr=trim($rs->fields("sorvpfr"));
			$Sorvpto=trim($rs->fields("sorvpto"));
			
			$MySurnoSql= $MySurnoSql ."'".$Surno."',";
			
			$FldArraytmp=array("U".$Surno =>array($Surno,$Finm,$Lanm,$Shnm,$Peno,$Spfvpfr,$Spfvpto,$Sorvpfr,$Sorvpto));
			$StaffTblArray=array_merge ($StaffTblArray, $FldArraytmp);
			
			$rs->MoveNext();
		}// While End

		// Bug fix for the last comma
		$MySurnoSql= $MySurnoSql ." '0'";
		
	}// If End
	if ($rs)$rs->Close();
	
	return $MySurnoSql;
}

Function SPRTAB($SurnoSql,$DateFrom,$DateTo) {
	Global $db,$Session,$tpl,$SPRTblArray,$login;
	$DateFrom = date("Ymd200000",ChopString ("d",  -1, $DateFrom));
	//Add one more day until 9 o'clock in the morning of the next day
	$DateTo = date("Ymd090000",ChopString ("d",  1, $DateTo));
	$fields = " SUBSTR(sprtab.acti,1,8) SACTI,ACTI,FCOL,PKNO,ustf ";
	$table = " sprtab ";
	//$SurnoSql ='213101322';
	
	$wherest .= " where ". $login->SPRTABWhereSt($Session["cano"],$SurnoSql,$Session["stfurno"]) . " and ";

    
	
	//$wherest =$wherest ." (acti between '".substr($DateFrom, 0, 8)."000000' and '".substr($DateTo, 0, 8)."595959')  ";
	$wherest =$wherest ." (acti between '".$DateFrom."' and '".$DateTo."')  ";
	
    $cmdTempCommandText = "SELECT ".$fields." from ". $table.$wherest . " order by ustf,acti";
	//echo $cmdTempCommandText;
    $rs = &$db->Execute($cmdTempCommandText );
	if (!$rs->EOF)  {
		$i = 1;
		while (!$rs->EOF) {

				$SACTI = trim($rs->fields("SACTI"))."000000";
				$ACTI = trim($rs->fields("ACTI"));
				$FCOL = trim($rs->fields("FCOL"));
				$PKNO = trim($rs->fields("PKNO"));
				$ustf = trim($rs->fields("ustf"));
				//echo $ACTI ."--".$FCOL."--".$SACTI."<br>";
				//$FldArraytmp=array($Stfu."-".$Sday."-".$Drrn =>array($Scod,$Sday,$Drrn,$Bsdu,$Fctc));
				
				$SPRTblArray[$ustf."-".$SACTI][] = $ACTI;

				/*
				if (array_key_exists($ustf."-".$SACTI, $SPRTblArray)) {//exists 
				    $i = count($SPRTblArray[$ustf."-".$SACTI][$i] ;
					$SPRTblArray[$ustf."-".$SACTI][$i] = $ACTI;
				   
				} else {
				
				}
				*/
				
				/*
				//We Can Display The Time for the Shifts in Pairs 
				
				if (array_key_exists($ustf."-".$SACTI, $SPRTblArray)) {//exists 
					

				   //if ($FCOL==0) {
				    $i++;
					$SPRTblArray[$ustf."-".$SACTI][$i] = $ACTI;
					//echo $ustf."-".$SACTI."<br>";
					//echo $SPRTblArray[$ustf."-".$SACTI][2]."-2<br>";
				   
				} else {
					$prevSACTI=  date("Ymd000000",ChopString ("d",  -1, $SACTI));
					$timeACTI =  date("H",ChopString ("d",  0, $ACTI));
					//echo $SACTI ."--" .$prevSACTI ."--".$timeACTI."--".$i."--<br>";
				   if  ($SPRTblArray[$ustf."-".$prevSACTI][3]) {
				   		$prevtimeACTI =  date("H",ChopString ("d",  0, $SPRTblArray[$ustf."-".$prevSACTI][3])); 
				   } elseif ($SPRTblArray[$ustf."-".$prevSACTI][1] ) {
					   	$prevtimeACTI =  date("H",ChopString ("d",  0, $SPRTblArray[$ustf."-".$prevSACTI][1])); 
				   }
				   
				   if ( ($SPRTblArray[$ustf."-".$prevSACTI][3]  || $SPRTblArray[$ustf."-".$prevSACTI][1]) && ($timeACTI >= 05 &&  $timeACTI <= 07) && ($prevtimeACTI >= 21 && $prevtimeACTI <= 23)) {
				   	$SPRTblArray[$ustf."-".$prevSACTI][4] = $ACTI ;
					//echo $SACTI ."--" .$prevSACTI ."<br>";
				   } else {
   				   	$SPRTblArray[$ustf."-".$SACTI][1] = $ACTI ;
				   }
					$i = 1;	
					//echo $SPRTblArray[$ustf."-".$SACTI][1]."<br>";
					//echo $SPRTblArray[$ustf."-".$SACTI][1]."-1<br>";

				
				}
				*/

			$rs->MoveNext();
		}// While End

	}// If End
	if ($rs)$rs->Close();
	return 0;
}


Function DRRTab($SurnoSql,$DateFrom,$DateTo,$rosl,$FunctionCode,$Maxlstu=false) {
	Global $db,$Session,$tpl,$DRRTblArray,$DailyStatsArray,$DailyScodArray,$StaffTblArray,$login;
	$tmp="";
	$FldArraytmp=Array();
	$FldArraytmpStats=Array();
    /* Daily Roster Deviations */

    /* Create the query */
	if ($Maxlstu==true) {
		$fields = " max(lstu) maxlstu ";
	} else {
	    $fields = " scod,scoo,lstu,sday,drrn,stfu,ross,rosl,bsdu,fctc ";
	}
	
    $table = " drrtab ";

    $wherest = " where  stfu in (".$SurnoSql.") and ";
	$wherest =$wherest ." (sday between '".substr($DateFrom, 0, 8)."' and '".substr($DateTo, 0, 8)."') and ";
	$wherest =$wherest ." rosl='".$rosl."' ";

	
	if ($FunctionCode != "All") {$wherest .= $login->DRRTabWhereSt($Session["cano"],'',$FunctionCode);}
	
	
    $cmdTempCommandText = "SELECT ".$fields." from ". $table.$wherest . " order by sday,stfu,drrn ";
	$maxlstu=0;
    $rs = &$db->Execute($cmdTempCommandText );
	//echo $cmdTempCommandText;
	if (!$rs->EOF)  {
		$MySurnoSql="";
		
		while (!$rs->EOF) {

			if ($Maxlstu==true) {
				$maxlstu = trim($rs->fields("maxlstu"));
			} else {
				$Scod = trim($rs->fields("scod"));
				$OldScod = trim($rs->fields("scoo"));
				$Sday = trim($rs->fields("sday"))."000000";
				$lstu = trim($rs->fields("lstu"));

				$Drrn = trim($rs->fields("drrn"));
				$Stfu = trim($rs->fields("stfu"));
				$Bsdu = trim($rs->fields("bsdu"));
				$Fctc = trim($rs->fields("fctc"));

				//if ($Stfu='213093688') {
				//	echo $Stfu."-".$Sday."-".$Drrn;
				//}
				
				// Change on 18-07-2005 
				// From array_merge to array[$key][..]=$value; 
				// for better Performance
				
				
				if ($Session["cano"]=="BU01" || $Session["cano"]=="BU02") {
					$Spfvpfr = "200101010000";
					$Spfvpto = "";
				} else {
					$Spfvpfr = $StaffTblArray["U".$Stfu][5];
					$Spfvpto = $StaffTblArray["U".$Stfu][6];
				}
				
				//|| $Spfvpto==""
				if (  ($Sday >= $Spfvpfr  && $Sday <= $Spfvpto) || ($Sday >= $Spfvpfr  && $Spfvpto=="") ) {
				
					$FldArraytmp=array($Stfu."-".$Sday."-".$Drrn =>array($Scod,$Sday,$Drrn,$Bsdu,$Fctc));
					//$DRRTblArray=array_merge ($DRRTblArray, $FldArraytmp);
				
					//echo "drrtab  ".$Stfu."-".$Sday."<br>";
					$DRRTblArray[$Stfu."-".$Sday."-".$Drrn][0] = $Scod;
					$DRRTblArray[$Stfu."-".$Sday."-".$Drrn][1] = $Sday;
					$DRRTblArray[$Stfu."-".$Sday."-".$Drrn][2] = $Drrn;
					$DRRTblArray[$Stfu."-".$Sday."-".$Drrn][3] = $Bsdu;
					$DRRTblArray[$Stfu."-".$Sday."-".$Drrn][4] = $Fctc;
					$DRRTblArray[$Stfu."-".$Sday."-".$Drrn][5] = $OldScod;
					$DRRTblArray[$Stfu."-".$Sday."-".$Drrn][6] = $lstu;
				
					
				
					if (array_key_exists($Sday."-".$Scod, $DailyStatsArray)) {
					    $DstatsCount = $DailyStatsArray[$Sday."-".$Scod];
					    $DailyStatsArray[$Sday."-".$Scod]=$DstatsCount+1;
					} else {
						$FldArraytmpStats=array($Sday."-".$Scod =>1);
						$DailyStatsArray=array_merge ($DailyStatsArray, $FldArraytmpStats);
					}
					if (!array_key_exists($Scod, $DailyScodArray)) {
						$DailyScodArray=array_merge ($DailyScodArray, array($Scod=>array($Scod)));
					}

				}
			}
			$rs->MoveNext();
		}// While End

	}// If End
	if ($rs)$rs->Close();
	return $maxlstu;
}


Function DisplayStaff ($datefrom,$dateto) {
Global $db,$Session,$tpl,$StaffTblArray;
// Reset Array 
 reset($StaffTblArray);

 $tpl->set_var("StaffNames","");	
  
  
  for ( $i = 0; $i < count($StaffTblArray); $i++ ) {
      while ( $column = each($StaffTblArray) ) {   
		$data = $column['value'];
		
		$Surno=$data[0];
		$finm=$data[1];
		$lanm=$data[2];
		$Shnm=substr($data[3],3,strlen($data[3]));
		$Shnm=ereg_replace('�',',',$Shnm);
		$peno=$data[4];
		//HighLIght Current User
		$bgcolor="";
		if ($Shnm==$Session["shnm"]) {
			$bgcolor="beige";
		}
		$tpl->set_var("Staffbgcolor",$bgcolor);
		
		$tpl->set_var("Fname", $Shnm );
		//$tpl->set_var("Fname", $finm ." " .$lanm ." ". $peno);
		
		//$tpl->set_var("Lname", $lanm);
		//$tpl->set_var("Pcode", $peno);

		$tpl->parse("StaffNames", true);


         
      }// end while
   }// end for

   
}

Function DisplayDDr ($datefrom,$dateto,$MaxDaysLSTU,$ColorLSTU) {
Global $db,$Session,$tpl,$StaffTblArray,$DRRTblArray,$BSDTblArray,$ODATblArray,$DailyScodArray,$SPRTblArray;
// Reset Array 
 reset($StaffTblArray);

  
  
  
  for ( $i = 0; $i < count($StaffTblArray); $i++ ) {
      while ( $column = each($StaffTblArray) ) {   
		$data = $column['value'];
		
		$Surno=$data[0];
		$DateTmp=$datefrom;
		$tpl->set_var("DrrDates","");		
		
		while (($DateTmp <= $dateto) ) {
			
			reset($DRRTblArray);
			$DrrDisplayArr=Array();
			$SecDrrDisplayArr=Array();
			
			$DrrDisplayArr=$DRRTblArray[$Surno.'-'.$DateTmp.'-1'];
			$SecDrrDisplayArr=$DRRTblArray[$Surno.'-'.$DateTmp.'-2'];

			
			
			//echo $Surno.'-'.$DateTmp.'-1';
			//echo count($DrrDisplayArr);
			$Scod="";
			$SecScod="";
			
			$Scod=$DrrDisplayArr[0];
			$Scod_ArrKey=$DrrDisplayArr[0];

			$SecScod=$SecDrrDisplayArr[0];

			$BUrno=$DrrDisplayArr[3];
			$Fctc=$DrrDisplayArr[4];
			
			$SecBUrno=$SecDrrDisplayArr[3];
			$SecFctc=$SecDrrDisplayArr[4];
			
			//LastUpdateField
			$OldScod=$DrrDisplayArr[5];
			$lstu=$DrrDisplayArr[6];
			
			$SecOldScod=$SecDrrDisplayArr[5];
			$Seclstu=$SecDrrDisplayArr[6];
			
			
			if ( len($Scod)>0 ) {
				
				if ( len($SecScod)>0 ) {
					$Scod=$Scod."/".$SecScod;
				}
			} else {
				$Scod="&nbsp;";
				//.$DateTmp."--".$Surno;
			}

			$ScodColor=Array();
			$SecScodColor=Array();
			//echo count($ScodColor);
			$TdColor="#ffffff";
			$ToolTipScod="";
			
			if (count($BSDTblArray["BSD".$BUrno])>0) {
				$ScodColor=$BSDTblArray["BSD".$BUrno];
		
				if (len($ScodColor[2])>0) {
					$ToolTipScod=$ScodColor[2] . "(".$Fctc.", ".$ScodColor[3].")";
					//$DailyScodArray[$Scod][1]=	$Scod1." (".$ScodColor[3].")";
					//$DailyScodArray[$Scod][1] = $Scod1;
					$DailyScodArray[$Scod_ArrKey][1] = $ScodColor[3];
				}
				if ( len($SecScod)>0 ) {
					$SecScodColor=$BSDTblArray["BSD".$SecBUrno];
					if (len($ScodColor[2])>0) {
					 $ToolTipScod=$ToolTipScod."<br>".$SecScodColor[2] . "(".$SecFctc.", ".$SecScodColor[3].")";
					 //$DailyScodArray[$SecScod][1]=$SecScod." (".$SecScodColor[3].")";
					 //$DailyScodArray[$SecScod][1] = $SecScod;
					 $DailyScodArray[$SecScod][1] = $SecScodColor[3];
					}
				}
			} else {
				$ScodColor=$ODATblArray["OSD".$BUrno];
				
				if (len($ScodColor[2])>0) {
					$ToolTipScod=$ScodColor[2];
					//$DailyScodArray[$Scod1][1]=$Scod1.",".$ToolTipScod;
					unset($DailyScodArray[$Scod_ArrKey]) ;
				}
			}
			
			
			if (len($ScodColor[1])>0) {
				$TdColor="#".$ScodColor[1];
			}
			
			//TimeDiff
			$mklstu = ChopString ("",  "0", $lstu);
			$mkSeclstu = ChopString ("",  "0", $Seclstu);
			
			if ( DateDiff("d",$mklstu,time())<=$MaxDaysLSTU ||  DateDiff("d",$mkSeclstu,time())<=$MaxDaysLSTU) {
				if ( ($Scod != $OldScod) || ($SecScod != $SecOldScod) ) {
					$TdColor = $ColorLSTU;
				}
			}
			
			// ClockHit
//			echo $SPRTblArray[$Surno.'-'.$DateTmp][0] ."<br>".$SPRTblArray[$Surno.'-'.$DateTmp][1];
//			echo $SPRTblArray[$Surno.'-'.$DateTmp] ."<br>";
			if ($Scod!= "&nbsp;") {

			if (array_key_exists($Surno.'-'.$DateTmp, $SPRTblArray)) {
			 $tmptime="";
			 $tmptime1="";
			 $TdColor ="red";
			 $cArray = count($SPRTblArray[$Surno.'-'.$DateTmp]);
			 for ( $i = 0; $i < $cArray; $i++ ) {
			    $tmpcomma = ",";
			 	$tmptime1 = $SPRTblArray[$Surno.'-'.$DateTmp][$i];
				$tmptime1 = date(" H:i ",ChopString ("m",  0, $tmptime1));
				if ($i  == $cArray -1 )  { $tmpcomma = ""; } 
			 	$tmptime = $tmptime . $tmptime1 . $tmpcomma ;
				
			 }
			 
			 $ToolTipScod = $ToolTipScod ."<br>Clock Times  ($tmptime)";
			 }
			/*
				//echo "found<br>";
				$fromtime = "";
				$totime = "";
				$fromtime1 = "";
				$totime1 = "";
				echo count($SPRTblArray[$Surno.'-'.$DateTmp]) ."<br>";
				if ( $SPRTblArray[$Surno.'-'.$DateTmp][1] ) {
					$fromtime = $SPRTblArray[$Surno.'-'.$DateTmp][1];
					$fromtime = date(" H:i ",ChopString ("m",  0, $fromtime));
				}
				if ( $SPRTblArray[$Surno.'-'.$DateTmp][2] )  {
					
					$totime =  $SPRTblArray[$Surno.'-'.$DateTmp][2];
					$totime = date(" H:i ",ChopString ("m",  0, $totime));
					
				}
				if ( $SPRTblArray[$Surno.'-'.$DateTmp][3] ) {
					$fromtime1 = $SPRTblArray[$Surno.'-'.$DateTmp][3];
					$fromtime1 = date(" H:i ",ChopString ("m",  0, $fromtime1));
				}
				if ( $SPRTblArray[$Surno.'-'.$DateTmp][4] ) {
					$totime1 = $SPRTblArray[$Surno.'-'.$DateTmp][4];
					$totime1 = date(" H:i ",ChopString ("m",  0, $totime1));
				}
				
				if (len($totime1) > 0 && len($totime)==0) {
					$totime = $totime1;
					$totime1 = "";
				}
				
				$TdColor ="red";
				$ToolTipScod = $ToolTipScod ."<br> Employee Clock In ($fromtime - $totime) ";
				if (len($fromtime1)>0 || len($totime1)>0 ) {
					$ToolTipScod = $ToolTipScod ."<br> ($fromtime1 - $totime1)";
				}
			*/
			}
			

			

			
			if ($Scod!="&nbsp;" ) {
				$tpl->set_var("ToolTipScod",$ToolTipScod );
				$tpl->set_var("ScodColor",$TdColor );
				$tpl->set_var("Scod", $Scod);
			} else {
				$tpl->set_var("ToolTipScod","" );
				$tpl->set_var("ScodColor","#ffffff" );
				$tpl->set_var("Scod", $Scod);
			}
				$tpl->parse("DrrDates", true);
			
			$DateTmp=date("Ymd000000",ChopString ("w",  1, $DateTmp));
			
		}
		$tpl->parse("DrrDates0", true);
		
		

         
      }// end while
   }// end for

   
}


Function DisplayDailyStats ($datefrom,$dateto) {
Global $db,$Session,$tpl,$DailyStatsArray,$DailyScodArray,$SortedDailyScodArray;

	reset($SortedDailyScodArray);
 // for ( $i = 0; $i < count($DailyScodArray); $i++ ) {
	while ( $column = each($SortedDailyScodArray) ) {   	
	
		//$tpl->set_var("DailyScod","");		
		//$Scod=$DailyScodArray[$i];
		$data = $column['value'];
		$Scod=$data[0];
		
		$DateTmp=$datefrom;
		$tpl->set_var("DailyStats","");
		
		while (($DateTmp <= $dateto) ) {
			
			reset($DailyStatsArray);
			//$StatNumber=Array();
			$StatNumber=$DailyStatsArray[$DateTmp."-".$Scod];

			$tpl->set_var("StatNumber", $StatNumber);
			$tpl->parse("DailyStats", true);
			
			$DateTmp=date("Ymd000000",ChopString ("w",  1, $DateTmp));
			
    	 }// end while
		$tpl->parse("DrrStats0", true);
	}// end while
 //  }// end for

   
}

Function DisplayDailyScod () {
Global $db,$Session,$tpl,$DailyScodArray,$SortedDailyScodArray;
// Sort Array

$array_sort = new array_sort($DailyScodArray, "<[1]>");
if(!$array_sort->error["flag"]){ 
	$SortedDailyScodArray = $array_sort->get_sorted_array(); 
} else { 
	$SortedDailyScodArray = $DailyScodArray;
}
  
  //for ( $i = 0; $i < count($DailyScodArray); $i++ ) {
		while ( $column = each($SortedDailyScodArray) ) {   
			$data = $column['value'];
			if (strlen($data[1])>0) {
				$Scod=$data[0] ." (". $data[1] .")";
			} else {
				$Scod=$data[0];
			}

			//$tpl->set_var("DailyScod","");		
			//$Scod=key($DailyScodArray);
			$tpl->set_var("Scod", $Scod);
			$tpl->parse("DailyStatsScod", true);
		}
		
		//next($DailyScodArray);
  // }// end for

}

function DateFromDateTo ($datefrom,$dateto) {
	Global $db,$Session,$tpl;
	
	$DateTmp=$datefrom;
	
	if ($datefrom < $dateto) {
	$i=0;
		while (($DateTmp <= $dateto) ) {
		
			$DateName=DayOfWeek($DateTmp);
			$DatebgColor="#ffffff";
			if ($DateName=="Su" || $DateName=="Sa") {
				$DatebgColor="#FEB501";
			}
			
			
			$tpl->set_var("DatebgColor",$DatebgColor);
			$tpl->set_var("DateName",$DateName);
			$tpl->set_var("DateNum",date(" d.m ",ChopString ("w",  0, $DateTmp) ));
			$tpl->parse("Dates", true);
			$DateTmp=date("Ymd000000",ChopString ("w",  1, $DateTmp));
		
			if ($i==100) {break;}
			$i++;
		}
		
	}
	
}


// Legend Function 
function BSDTab () {
	
    Global $db,$Session,$tpl,$BSDTblArray;
    $tmp="";
	$FldArraytmp=Array();
    /* Start  Getting the information from the user */
//    $tmp="(stoa BETWEEN '$fromhour' AND '$tohour')";

    /* Create the query */
    $fields=" Urno,BSDC,BSDN,ESBG,LSEN,BKF1,BKT1,RGBC ";
    $table=" BSDTAB ";
    $wherest="";
    $cmdTempCommandText = "SELECT ".$fields." from ". $table.$wherest . " order by ESBG,LSEN";

//	echo $cmdTempCommandText ;
    $rs = &$db->Execute($cmdTempCommandText );
	if (!$rs->EOF)  {
		while (!$rs->EOF) {
			$BUrno=trim($rs->fields("Urno"));
			
			$BSDC=trim($rs->fields("BSDC"));
			$BSDN=trim($rs->fields("BSDN"));

			// Change Time Format 
			$ESBG=substr(trim($rs->fields("ESBG")),0,2).":".substr(trim($rs->fields("ESBG")),2,4);
			$LSEN=substr(trim($rs->fields("LSEN")),0,2).":".substr(trim($rs->fields("LSEN")),2,4);
			$BKF1=substr(trim($rs->fields("BKF1")),0,2).":".substr(trim($rs->fields("BKF1")),2,4);
			$BKT1=substr(trim($rs->fields("BKT1")),0,2).":".substr(trim($rs->fields("BKT1")),2,4);

			$RGBC=trim($rs->fields("RGBC"));
			$RGBC=ereg_replace(' ','0',$RGBC);
			
			$ToolTipName = $BSDN;
			$ToolTipTime = $ESBG." - ". $LSEN;
			
			$tpl->set_var("BSDC", $BSDC ." (".$BSDN.")");
			$tpl->set_var("Time", $ESBG ." - ".$LSEN );
			$tpl->set_var("BreakTime", $BKF1 ." - ".$BKT1 );
			
			$tpl->parse("BSD", true);

			$FldArraytmp=array("BSD".$BUrno =>array($BSDC,$RGBC,$ToolTipName,$ToolTipTime));
			$BSDTblArray=array_merge ($BSDTblArray, $FldArraytmp);
			
			$rs->MoveNext();
		}// While End

	}// If End
	if ($rs)$rs->Close();

}

function ODATab () {
    Global $db,$Session,$tpl,$ODATblArray;
    $tmp="";
	$FldArraytmp=Array();
    /* Start  Getting the information from the user */
//    $tmp="(stoa BETWEEN '$fromhour' AND '$tohour')";

    /* Create the query */
    $fields=" Urno,SDAC,SDAN,RGBC ";
    $table=" ODATAB ";
    $wherest="";
    $cmdTempCommandText = "SELECT ".$fields." from ". $table.$wherest . " order by SDAC";


    $rs = &$db->Execute($cmdTempCommandText );
	if (!$rs->EOF)  {
		while (!$rs->EOF) {
			$OUrno=trim($rs->fields("Urno"));
			
			$SDAC=trim($rs->fields("SDAC"));
			$SDAN=trim($rs->fields("SDAN"));

			$RGBC=$rs->fields("RGBC");
			// Replace Spaces with 0 
			$RGBC=ereg_replace(' ','0',$RGBC);
			
			//Reverse string !! 
			$RGBC=strrev($RGBC);
			//Put 0 at the end of the string 
			//if (count($RGBC)<7) {
			//	$RGBC=$RGBC.str_repeat("0", 6-count($RGBC));
			//}
			
			//if ($SDAC=="R") {$RGBC="FFFF00";}
			//if ($SDAC=="V") {$RGBC="80FFFF";}
			//if ($SDAC=="S") {$RGBC="FF04FF";}
						
			$tpl->set_var("SDAC", $SDAC);
			$tpl->set_var("SDAN", $SDAN );
			$tpl->parse("ODA", true);

			$FldArraytmp=array("OSD".$OUrno =>array($SDAC,$RGBC,$SDAN));
			$ODATblArray=array_merge ($ODATblArray, $FldArraytmp);
			
			$rs->MoveNext();
		}// While End

	}// If End
	if ($rs)$rs->Close();

}

Function DisplayMonthNames ($SelectedYear=0,$SelectedMonth=0) {
Global $tpl;
Global $FromDate,$ToDate;


if ($SelectedMonth==0) {$SelectedMonth=date("n",time()); }
if ($SelectedYear==0) {$SelectedYear=date("n",time()); }


//$Month_Number=date("m",DateAdd ("m",  0, time()));
//$Month_Number=$Month_Number+$ToDate;
//echo $Month_Number;
$Year_Number=date("Y",time());



for ($i = $FromDate;$i<=$ToDate; $i++) {
   if ($i > 32) {
       $i=1;
       break;
   }
   
//   echo date("m",time())."<br>";
   $Month_Name=date("F",DateAdd ("m",  $i, time()));
   $Month_Number=date("m",DateAdd ("m",  $i, time()));
   $Year_Number=date("Y",DateAdd ("m",  $i, time()));

   $tmp="";
   if ($SelectedMonth==$Month_Number && $SelectedYear==$Year_Number) { 
   	$tmp=" Selected";
   }
   $tpl->set_var("MonthId", $Year_Number . $Month_Number );
   $tpl->set_var("Selected", $tmp );
   $tpl->set_var("MonthName", $Month_Name."(".$Year_Number.")");
   $tpl->parse("Month", true);
   
}


}

function RosterLevelF ($SelectedRosteLevel) {
Global $tpl;
	for ($i = 1;$i<=3; $i++) {
		 $tmp="";
   		if ($SelectedRosteLevel==$i) { $tmp=" Selected";}
		$tpl->set_var("Selected", $tmp );
		$tpl->set_var("rosl",$i);
		$tpl->parse("RosterLevel", true);
	}
	$tpl->parse("NoRosterLevel", true);
}

function DisplayGRoup () {
Global $db,$GroupArray;

	$i=0;
	$GroupArray[$i]=trim("BU01");
	$i++;
	$GroupArray[$i]=trim("BU02");
	$i++;
	$GroupArray[$i]=trim("SU00");
	$i++;
	$GroupArray[$i]=trim("SU01");
	$i++;
	$GroupArray[$i]=trim("SV01");
	$i++;
	$GroupArray[$i]=trim("SV02");
	$i++;
	$GroupArray[$i]=trim("SV03");
	$i++;
	$GroupArray[$i]=trim("SV04");

}

function DisplayGRoupv1 () {
Global $db,$GroupArray;

/* Create the query */
    $fields = " distinct ugrp";
    $table = " stftab";//,drrtab ";
	$wherest = "  ";
	 $cmdTempCommandText = "SELECT ".$fields." from ". $table.$wherest;

	//echo $cmdTempCommandText;
    $rs = &$db->Execute($cmdTempCommandText);
	$i=0;
	if (!$rs->EOF)  {
		while (!$rs->EOF) {
			$GroupArray[$i]=trim($rs->fields("ugrp"));
			$i++;
			
			$rs->MoveNext();
		}// While End
	}
	// If End
	if ($rs)$rs->Close();

}

Function SearchStaff($finm,$lanm,$shnm) {
	Global $db,$Session,$tpl,$GroupArray;
	$tmp="";
	$FldArraytmp=Array();
	
    /* Get The functions / Org Units for the Staff */

    /* Create the query */
    $fields = " distinct stftab.urno,stftab.finm, stftab.lanm, stftab.peno, stftab.shnm,stftab.ugrp ";
    $table = " stftab";//,drrtab ";
	$wherest = " where ";
	
    //$wherest = $wherest."  (upper(stftab.lanm) like '%".preg_replace("/([a-z\xE0-\xFF])/e","chr(ord('\\1')-32)",$lanm)."%') AND (upper(stftab.finm) like '%".strtoupper($finm)."%')";
	$wherest = $wherest."   (upper(stftab.shnm) like '%".strtoupper($shnm)."%')";	
	
	$wherest = $wherest. " order by stftab.lanm,stftab.finm";
	
	
    $cmdTempCommandText = "SELECT ".$fields." from ". $table.$wherest;

	//echo $cmdTempCommandText;
    $rs = &$db->Execute($cmdTempCommandText);
	
	if (!$rs->EOF)  {
		
		
		while (!$rs->EOF) {
			$Finm=trim($rs->fields("finm"));
			$Lanm=trim($rs->fields("lanm"));
			$Peno=trim($rs->fields("peno"));
			$thisShnm=trim($rs->fields("shnm"));
			$ugrp=trim($rs->fields("ugrp"));
			
			$thisShnm=substr($thisShnm,3,strlen($thisShnm));
			$thisShnm=ereg_replace('�',',',$thisShnm);
			
			$stfurno=trim($rs->fields("urno"));
	
	

		  for ( $i = 0; $i < count($GroupArray); $i++ ) {
			$Selected = "";
			if ($GroupArray[$i]==$ugrp) {
				$Selected = " Selected";
			}
			$tpl->set_var("UGRPSelected",$Selected);
			$tpl->set_var("UGRPvalue",$GroupArray[$i]);
			$tpl->parse("UGRP", true);
		   }
	      if (count($GroupArray)==0) {
	  			$tpl->set_var("UGRPSelected","");
				$tpl->set_var("UGRPvalue","");
				$tpl->set_var("UGRP", "");
		  }
		
			$tpl->set_var("Name",$thisShnm);
			$tpl->set_var("GName",$Lanm . ",". $Finm);
			$tpl->set_var("qpeno",$Peno);
			$tpl->set_var("stfurno",$stfurno);
			$tpl->parse("StaffNames", true);
			$rs->MoveNext();
		}// While End

		
	} else {
		$tpl->set_var("FName","");
		$tpl->set_var("LName","");
		$tpl->set_var("StaffNames", "");
		$rs->MoveNext();
	}
	// If End
	if ($rs)$rs->Close();
	
	return "";
}



?>
