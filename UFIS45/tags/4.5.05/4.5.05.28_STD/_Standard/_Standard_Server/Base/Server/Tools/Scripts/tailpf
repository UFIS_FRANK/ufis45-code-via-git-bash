# @(#) $Id: Ufis/_Standard/_Standard_Server/Base/Server/Tools/Scripts/tailpf 1.8 2005/09/12 19:35:19SGT jim Exp  $
# 20031027 JIM: added processing for switching log files
# 20031118 JIM: quote $line to avoid evalutaing of "*" in 'echo $line'
# 20031216 JIM: corrected handling for wmqcdip and wmqcdic, where running process for both is wmqcdi
# 20040119 JIM: extended for processes, where childs with same name as parent write to parents log file
# 20040323 JIM: corrected for process numbers with more than one leading blank
# 20050912 JIM: PRF 7752: if decision by process name failes, show newest file
#               fitting first parameter

function logtail 
{
# the following '(' creates a subshell with a new pid
(
   echo $$
   # the pid of this subshell can be evaluated in AWK
   tail -f $1 | while [ "1" = "1" ] ; do 
      read line
      echo "$line"  
      case "$line" in
         "Renaming Old Debug File to "*)   
            # get the pid belonging to the piping tail:
            tailppid=`ps -ef | grep " $LOGNAME *[0-9][0-9]* *$$ " | awk '{print $2}'`
            tailpid=`ps -ef | grep " $LOGNAME *[0-9][0-9]* *${tailppid} .* tail -f" | awk '{print $2}'`
            echo $tailppid $tailpid
            kill -9 $tailpid
            break
	      ;;
      esac
   done
)
}

if [ ! -z "$1" ] ; then
   if [ "$1" = "sysmon" ] ; then
      # sysmon daoes not append pid to file name
      PRID=""
      LOGN=${DBG_PATH}/$1.log
   else
      # get pid of CEDA process by listing of processes and grep for a line
      # beginning with 'ceda', somewhere in the line the process name surrounded
      # by blanks and followed by the queue id number and status (21 or 20)  
      # The process name, the queue id and the status are the last three items
      # of the line. If process name is not surrounded by blank, try any char
      # foolowing the name.
      PRID=`ps -ef | grep "^[ \t]*ceda .* $1 [0-9]* 2[01][ \t]*$" | awk '{printf "%05d",$2}'`
      if [ -z "$PRID" ] ; then
         PNAME=`echo $1 | awk '{print substr($1,1,length($1)-1)}'`
         PRID=`ps -ef | grep "^[ \t]*ceda .* ${PNAME}.* [0-9]* 2[01][ \t]*$" | awk '{printf "%05d\n",$2}'`
         for PP in $PRID ; do 
            N=/ceda/debug/$1${PP}.log
            if [ -f $N ] ; then 
               LOGN=$N
            fi
         done
      else
         LOGN=${DBG_PATH}/$1$PRID.log
         if [ ! -f "$LOGN" ] ; then
	    # 20040119 JIM: process found, but no log file: may be more procs with same name and one 
	    # log file (i.e. cdrhdl)
            PRID=`ps -ef | grep "^[ \t]*ceda .* $1 [0-9]* 2[01][ \t]*$" | awk '{printf "%05d\n",$2}'`
            for PP in $PRID ; do 
               N=/ceda/debug/$1${PP}.log
               if [ -f $N ] ; then 
                  LOGN=$N
               fi
            done
	 fi
      fi
   fi
   # 20050912 JIM: PRF 7752
   # if all the above failed to get log file of running process, user may have
   # requested a file of a not running process or named other than process: 
   if [ ! -f "$LOGN" ] ; then
      LOGN=`ls -t /ceda/debug/$1* 2>/dev/null | head -1`
   fi
   if [ -f "$LOGN" ] ; then
     echo $LOGN
     while [ "1" = "1" ] ; do
        logtail $LOGN
        # perhaps we miss some dbg lines now, but anyway....
        cat <<EOF 
 ==============================================================

 #    #  ######  #    #          ######     #    #       ######
 ##   #  #       #    #          #          #    #       #
 # #  #  #####   #    #          #####      #    #       #####
 #  # #  #       # ## #          #          #    #       #
 #   ##  #       ##  ##          #          #    #       #
 #    #  ######  #    #          #          #    ######  ######

 ==============================================================

EOF
     done
   else
      if [ -z "$LOGN" ] ; then
        echo "file $1\* not found"
      else
        echo "file $LOGN not found"
      fi
      shift
   fi
fi
if [ -z "$1" ] ; then
   echo "Usage: tailpf <processname>"
   echo "<processname> should be complete name like in sgs.tab to avoid"
   echo "              confusion i.e. for sqlhdl and sqlhdl2"
   
fi
