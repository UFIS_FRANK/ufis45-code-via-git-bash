# @(#) $Id :$
# Script to get the version numbers of all UFIS binaries
#
# 20031027 JIM: added output for used libraries
# 20031027 JIM: tested for Sun-OS, Red Hat Linux, HP-UX
#

DisplayHelp()
{ 
   cat <<EOF
   `basename $0`: Script to get the version numbers of one UFIS binary
   Usage: 
   `basename $0` <name of binary>
EOF
}

THIS=`basename $0`
f="$1"
process=`basename "$1"`
option="$2"

ListStamp2()
{
  awk '
    BEGIN {
      lastfile=" "
      prjbase=""
    }
    {
      file=$3
      if (file != lastfile)
      {
        if (lastfile != " ")
        {
           printf "%-10s : %-16s %-66s %s \n", \
                  "'${process}'",lastfile,verinfo,compinfo
        }
        prjbase=""
        version=""
        date=""
        time=""
        checked_by=""
        sourcesize=""
        compdatepos=""
        compdate=""
        compiled_by=""
        compiled_on=""
        prj=""
        checked_out=""
        build=""
        verinfo="VERSION INFO NOT AVAILABLE"
        compinfo="(not to compile)"
        compinfo=sprintf("%-10s %-10s %10s","-","-","-")
      }
      if ($4 == "MKS-Id:")
      {
        UFIS_VERSION=$5" "$6
        version=$7
        prjbase=$8
        date=$9
        time=$10
        checked_by=$11
        checked_out=$12
        verinfo=sprintf("%-8s %-20s %-8s %-3s %-20s",\
                version,prjbase,UFIS_VERSION,checked_by,checked_out)
      }
      else if ($4 == "STAMP:")
      {
        sourcesize=$5
        compiled_by=$6
        compiled_on=substr($6,index($6,"@")+1)
        prj=$7
        compdate=$8" "$9" "$10" "$11
        build=$12
        if ( "'$option'" == "FOR_BUILD_NR" )
        {
           compinfo=sprintf("%-10s %-10s %10s",\
                            prj,compiled_on,sourcesize)
        }
        else
        {
           compinfo=sprintf("%-10s %-10s %10s %7s",\
                            prj,compiled_on,sourcesize,build)
        }
      }
      lastfile=file
    }
    END {
       # print last line
       if ( prjbase != "" )
       {
           printf "%-10s : %-16s %-66s %s \n", \
                  "'${process}'",lastfile,verinfo,compinfo
       }
    }'
}


ListStamp()
{ # This function reformats the stamps to sortable strings

  grep "@(#) " | awk '
    {
      a=substr($0,index($0,"@(#)"))
      level="SUB:"
      if (index(a,"$Id: ") > 0)
      {
        num=split(a,ID," ")
        lfile=ID[8]
        num=split(lfile,fileparts,"/")
        prjbase=fileparts[3]
        file=fileparts[num]
        version= ID[9]
        date=ID[10]
        time=ID[11]
        usr=ID[12]
        if (ID[14] != "$")
        {
          checked_out=ID[14]")"
        }
        else
        {
          checked_out="-"
        }
        UFIS_VERSION=ID[2]" "ID[3]
        if ("'${process}'.c" == file)
        {
	        level="MAIN:"
	      }
        printf "%-9s %-5s %-12s MKS-Id: %-10s %-10s %-10s %-10s %-10s %-10s %-16s\n",\
                "'${process}'",level,file,UFIS_VERSION,version,prjbase,date,time,usr,checked_out
      }
      else if (index(a,"FileStat: ") > 0)
      {
        num=split(a,ID," ")
        file=ID[3]
        sourcesize=ID[5]
        compdatepos=index(a,"compiled at:") + 13
        compdate=substr(a,compdatepos,20)
        BuildPos=index(a,"Build:") + 7
        if (BuildPos>7)
        {
           Build_No=substr(a,BuildPos)
        }
        else
        {
           Build_No=""
        }
        usr=ID[9]
        prj=ID[10]
        if ("'${process}'.c" == file)
        {
          level="MAIN:"
        }
        printf "%-9s %-5s %-12s STAMP:  %-10s %-10s %-10s %-10s %7s\n",\
                "'${process}'",level,file,sourcesize,usr,prj,compdate,Build_No
      }
    }'
}

ListStrings()
{
  # run and evaluate checksum
  eval `cksum $f | awk '{print "check="$1" ; size="$2}'`
  # now check and size are set
  
  # get all version strings of binaries and add process name 
  # and checksum to the line 
  # delete duplicate lines (i.e. header used in different libs)
  strings $f | ListStamp | sort -u | ListStamp2 #| grep " .*\.c " 
  # strings $f | ListStamp | sort -u >xy.lst
}

PrintDiffToOld()
{
  awk '
  BEGIN {
     diff_found=0
     BUILD=0
  }
  {
     if ($1 == "<")
     {
        if ($2 == "Build:") 
        {
           BUILD=$3
        }
        else if ($2 != "build-number")
        {
           print "Old: "substr($0,2)
           diff_found=1
        }
     }
     else if ($1 == ">")
     {
        print "New: "substr($0,2);
     }
  }
  END {
     if (diff_found == 1)
     {
        print "build-number incremented from",BUILD," to ",BUILD+1":"
        print "Build: ",BUILD+1
     }
  }'
}

DoTheBuildCheck()
{
  OUTDIR=${M}/BuildInfo
  OUTFILE=${M}/BuildInfo/${process}.BuildNr
  if [ ! -d ${OUTDIR} ] ; then
     mkdir ${OUTDIR}
  fi
  
  if [ -f "$f" ] ; then
     #echo Binary $f exists
     #echo "ListStrings $f >${OUTFILE}.tmp"
     ListStrings $f >${OUTFILE}.tmp
     if [ -s ${OUTFILE} ] ; then
       echo Build Nr ${OUTFILE} exists
       #echo "diff ${OUTFILE} ${OUTFILE}.tmp | PrintDiffToOld >${OUTFILE}.Note"
       diff ${OUTFILE} ${OUTFILE}.tmp | PrintDiffToOld >${OUTFILE}.Note
       NEW_BUILD=`grep "Build: " ${OUTFILE}.Note`
       if [ "$NEW_BUILD" = "" ] ; then
         echo rm ${OUTFILE}.tmp ${OUTFILE}.Note
       else
         echo $NEW_BUILD >> ${OUTFILE}.tmp
         mv ${OUTFILE}.tmp ${OUTFILE}
         mv ${OUTFILE}.Note ${OUTDIR}/${process}.BuildNote
       fi
     else
       echo "build-number initial set:" >>${OUTFILE}.tmp
       echo "Build: 1031" >>${OUTFILE}.tmp
       mv ${OUTFILE}.tmp ${OUTFILE}
     fi
  else
     if [ ! -f ${OUTFILE} ] ; then
        touch ${OUTFILE}
     fi
  fi
}

if [ "$f" = "" ] ; then
   DisplayHelp
else
   if [ ! -f "$f" -a ! -f "${process}.c" ] ; then
      DisplayHelp
   else
      if [ "${option}" = "FOR_BUILD_NR" ] ; then
         DoTheBuildCheck $f
      else
         if [ "${option}" = "-v" ] ; then
            ListStrings
         else
            ListStrings | awk 'NR==1 {print $0}'
         fi
      fi
   fi
fi

