/* ******************************************/
/* ****** Account 38: free Sundays in month */
/* ******************************************/
static void bas_38()
{
	char 	clLastCommand[4];
	char	clStaffUrno[12];
	char	clSday[16];
	char	clNewDrrStatus[2];
	char	clYearIndex[6];
	char	clMonthIndex[4];
	char	clOldAccValue[128];
	char	clOldDrrStatus[2];
	char	clNewIsRegFree[2];
	char	clOldIsRegFree[2];

	double	dlOldValue;
	double	dlNewValue;
	double	dlResultValue;
	double	dlNewAccValue;
	double	dlOldAccValue;

	dbg(DEBUG, "============================= ACCOUNT 38 START ===========================");

	/* checking the SDAY */
	if (ReadTemporaryField("SDAY",clSday) == 0)
	{
		dbg(TRACE,"Account 38: Stopped, because <clSday> = undef!");
		return;
	}

	/* checking if shift day is a sunday */
	if (GetDayOfWeek(clSday) != 6)
	{
		dbg(DEBUG,"Account 38: Stopped, because shift day <clSday> = %s is no Sunday.",clSday);
		return;
	}

	/* get last command (IRT,URT,DRT) */
	strncpy(clLastCommand,GetLastCommand(),3);
	clLastCommand[3] = '\0';
	dbg(DEBUG,"Account 38: Command (%s)",clLastCommand);

	/* checking the URNO of the employee */
	if (ReadTemporaryField("StfUrno",clStaffUrno) == 0)
	{
		dbg(TRACE,"Account 38: Stopped, because <clStaffUrno> = undef!");
		return;
	}

	/* checking if new shift is regular free */
	if (ReadTemporaryField("NewIsRegFree",clNewIsRegFree) == 0)
	{
		dbg(TRACE,"Account 38: Stopped, because <clNewIsRegFree> = undef!");
		return;
	}	

	/* Status of new record	*/
	if (ReadTemporaryField("NewRoss",clNewDrrStatus) == 0)
	{
		dbg(TRACE,"Account 38: Stopped, because <clNewDrrStatus> = undef!");
		return;
	}

	/* get old data */
	if (strcmp(clLastCommand,"URT") == 0) 
	{
		/* checking if old shift was regular free */
		if (ReadTemporaryField("OldIsRegFree",clOldIsRegFree) == 0)
		{
			clOldIsRegFree[0] = '\0';
		}

		/* Status of old record	*/
		if (ReadTemporaryField("OldRoss",clOldDrrStatus) == 0)
		{
			dbg(TRACE,"Account 38: <clOldDrrStatus> = undef!");
			clOldDrrStatus[0] = '\0';
		}
	}

	/* calculate old value */
	dlOldValue = 0;
	if (strcmp(clLastCommand,"URT") == 0)
	{
        	/* take only active DRR-records */
		if (strcmp(clOldDrrStatus,"A") == 0)
		{
			if (strcmp(clOldIsRegFree,"1") == 0)
			{
				dlOldValue = 1;
			}
			dbg(DEBUG,"Account 38: Old Value <dlOldValue> = <%lf>",dlOldValue);
		}
		else
		{
			dbg(TRACE,"Account 38: Wrong status of old DRR-record: <%s>" ,clOldDrrStatus);
		}
	}

	/* calculate new value */
	dlNewValue = 0;
	if (strcmp(clNewDrrStatus,"A") == 0)
	{
		if (strcmp(clNewIsRegFree,"1") == 0)
		{
			dlNewValue = 1;
		}
		dbg(DEBUG,"Account 38: New Value <dlNewValue> = <%lf>",dlNewValue);
	}
    	else
    	{
    		dbg(TRACE,"Account 38: Wrong status of new DRR-record: <%s>" ,clNewDrrStatus);
	}

	/* calculate difference */
	if (strcmp(clLastCommand,"DRT") != 0)
	{
		dlResultValue = dlNewValue - dlOldValue;
	}
	else
	{
		dlResultValue = - dlNewValue;
	}

	/* get ACC record */
	strncpy(clYearIndex,clSday,4);
	clYearIndex[4] = '\0';
	strncpy(clMonthIndex,&clSday[4],2);
	clMonthIndex[2] = '\0';

	GetAccValueHandle("38",clMonthIndex,clYearIndex,clStaffUrno,"CL",clOldAccValue);
	dbg(DEBUG,"Account 38: Old ACC value <clOldAccValue> = <%s>",clOldAccValue);

	if  (fabs(dlResultValue) < 1.0e-5)
	{
		dbg(DEBUG,"Account 38: Value not changed.");
		return;
	}

	dlOldAccValue = atof(clOldAccValue);
	dlNewAccValue = dlOldAccValue + dlResultValue;
	dbg(DEBUG,"Account 38: New ACC value <dlNewAccValue> = <%lf>",dlNewAccValue);

	/* save result */
	UpdateAccountHandle(dlNewAccValue,"38",clMonthIndex,clYearIndex,clStaffUrno,"SCBHDL");
}
