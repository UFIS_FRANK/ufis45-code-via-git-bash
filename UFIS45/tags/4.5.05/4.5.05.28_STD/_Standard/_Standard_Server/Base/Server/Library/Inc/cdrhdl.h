#ifndef _DEF_mks_version_cdrhdl_h
  #define _DEF_mks_version_cdrhdl_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_cdrhdl_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/cdrhdl.h 1.2 2004/07/27 16:46:55SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The Master include file.                                              */
/*                                                                        */
/*  Program       : bcrout  Client Interface             */
/*                                                                        */
/*  Revision date : 22.08.2001                                            */
/*                                                                        */
/*  Author    	  : bst                                                   */
/*                                                                        */
/*  History       : adapted version of ivsif (ZRH)               */
/*                                */
/*                                                                        */
/*                                                                        */
/*                                                                        */
/* ********************************************************************** */
/* ********************************************************************** */

/* length of queue name */
#define MSGSTRLEN 16

/* length of VRU message strings */
#define VRUMSGLEN (128*1024)


/* ********************************************************************** */
/* ********************************************************************** */
