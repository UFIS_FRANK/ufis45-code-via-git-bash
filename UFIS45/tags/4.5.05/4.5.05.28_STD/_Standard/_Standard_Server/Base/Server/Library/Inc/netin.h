#ifndef _DEF_mks_version_netin_h
  #define _DEF_mks_version_netin_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_netin_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/_Standard/_Standard_Server/Base/Server/Library/Inc/netin.h 1.2 2004/07/27 16:48:09SGT jim Exp  $";
#endif /* _DEF_mks_version */
/* ********************************************************************** */
/*  The Master include file.						  */
/*									  */
/*  Program	  :     						  */
/*  Revision date :							  */
/*  Author    	  :							  */
/*  									  */
/*  NOTE : This should be the only include file for your program          */
/* ********************************************************************** */

#ifndef __NETIN_INC
#define __NETIN_INC


#include <sys/types.h>
#include <sys/socket.h>
#include <sys/signal.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>

#ifdef _ODT5
#include <prototypes.h>
#endif

#include <malloc.h>
#include <string.h>

#include "glbdef.h"
#include "ugccsma.h"
#include "quedef.h"
#include "uevent.h"
#include "msgno.h"
#include "tools.h"
#include "helpful.h"
#include "hsbsub.h"
    

extern void (*signal(int, void(*)(int)))(int);
extern pid_t      getpid(void);


#define		NETREAD_SIZE		(35000)
#define		NET_SHUTDOWN		(0x1)
#define		NET_DATA		(0x2)
#define		PACKET_DATA		(-100)
#define		PACKET_START		(-200)
#define		PACKET_END		(-300)
#define		PACKET_SINGLE		(-400)
#define		PACKET_RESEND		(-500)
#ifdef PACKET_LEN /*changes made by jhi 01.09.2000*/
#undef PACKET_LEN
#endif
#define		PACKET_LEN              (1024)
#define		MAX_CHILDS		(64)

#define UFIS_SERVICE_NAME    "UFIS"
#define UFIS_OUT_NAME	     "UFIS_OUT"

typedef	struct {
		short	command;
		int	length;
		char	data[1];
}COMMIF;

#endif

