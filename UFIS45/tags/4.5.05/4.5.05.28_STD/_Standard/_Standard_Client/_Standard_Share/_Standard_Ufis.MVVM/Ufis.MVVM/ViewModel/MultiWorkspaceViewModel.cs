﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Data;

namespace Ufis.MVVM.ViewModel
{
    /// <summary>
    /// Represents the workspace that contains multiple child-workspaces 
    /// and multiple commands.
    /// This class is abstract.
    /// </summary>
    public abstract class MultiWorkspaceViewModel : WorkspaceViewModel
    {
        ReadOnlyCollection<CommandViewModel> _commands;
        ObservableCollection<WorkspaceViewModel> _workspaces;

        #region Commands

        /// <summary>
        /// Returns a read-only list of commands 
        /// that the UI can display and execute.
        /// </summary>
        public ReadOnlyCollection<CommandViewModel> Commands
        {
            get
            {
                if (_commands == null)
                {
                    List<CommandViewModel> commands = CreateCommands();
                    _commands = new ReadOnlyCollection<CommandViewModel>(commands);
                }
                return _commands;
            }
        }

        /// <summary>
        /// Returns the list of commands for this workspace.
        /// </summary>
        /// <returns></returns>
        protected virtual List<CommandViewModel> CreateCommands()
        {
            return new List<CommandViewModel>();
        }
        #endregion

        #region Workspaces

        /// <summary>
        /// Gets the collection of child-workspaces in this workspace.
        /// </summary>
        public ObservableCollection<WorkspaceViewModel> Workspaces {
            get {
                if(_workspaces == null) {
                    _workspaces = new ObservableCollection<WorkspaceViewModel>();
                    _workspaces.CollectionChanged += this.OnWorkspacesChanged;
                }
                return _workspaces;
            }
        }

        /// <summary>
        /// Fires when workspaces changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void OnWorkspacesChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if(e.NewItems != null && e.NewItems.Count != 0)
                foreach(WorkspaceViewModel workspace in e.NewItems)
                    workspace.RequestClose += this.OnWorkspaceRequestClose;

            if(e.OldItems != null && e.OldItems.Count != 0)
                foreach(WorkspaceViewModel workspace in e.OldItems)
                    workspace.RequestClose -= this.OnWorkspaceRequestClose;
        }

        /// <summary>
        /// Fires when child workspace is closed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void OnWorkspaceRequestClose(object sender, EventArgs e) {
            WorkspaceViewModel workspace = sender as WorkspaceViewModel;
            if(workspace is PanelWorkspaceViewModel ||workspace is MultiPanelWorkspaceViewModel) {
                workspace.IsClosed = true;
            }
            else {
                workspace.Dispose();
                this.Workspaces.Remove(workspace);
            }
        }

        /// <summary>
        /// Opens a child workspace.
        /// </summary>
        /// <param name="workspace">The child workspace to open.</param>
        protected virtual void OpenWorkspace(WorkspaceViewModel workspace)
        {
            if (Workspaces.Contains(workspace))
            {
                if (workspace.IsClosed)
                    workspace.IsClosed = !workspace.IsClosed;
            }
            else
            {
                this.Workspaces.Add(workspace);
            }
            this.SetActiveWorkspace(workspace);
        }

        /// <summary>
        /// Opens a child workspace if it has not been opened,
        /// or closes it if it is opened.
        /// </summary>
        /// <param name="workspace">The child workspace to open or close.</param>
        protected virtual void OpenOrCloseWorkspace(WorkspaceViewModel workspace)
        {
            if (Workspaces.Contains(workspace))
            {
                workspace.IsClosed = !workspace.IsClosed;
            }
            else
            {
                this.Workspaces.Add(workspace);
                this.SetActiveWorkspace(workspace);
            }
        }

        /// <summary>
        /// Sets the active chlid workspace.
        /// </summary>
        /// <param name="workspace">The child workspace to be set as an active one.</param>
        public void SetActiveWorkspace(WorkspaceViewModel workspace)
        {
            Debug.Assert(this.Workspaces.Contains(workspace));
            ICollectionView collectionView = CollectionViewSource.GetDefaultView(this.Workspaces);
            if (collectionView != null)
                collectionView.MoveCurrentTo(workspace);
        }
        #endregion // Private Helpers
    }
}
