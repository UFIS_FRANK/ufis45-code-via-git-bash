﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Xpf.Docking;

namespace Ufis.MVVM.ViewModel
{
    /// <summary>
    /// Represents the workspace that that can be render in 
    /// DevExpress Xpf Docking suite.
    /// This class is abstract.
    /// </summary>
    public abstract class MultiPanelWorkspaceViewModel : MultiWorkspaceViewModel
    {
        /// <summary>
        /// Gets the name of DevExpress docking layout group object which will host this workspace.
        /// </summary>
        abstract protected string TargetName { get; }

        /// <summary>
        /// Initializes the new MultiPanelWorkspaceViewModel object.
        /// </summary>
        public MultiPanelWorkspaceViewModel()
        {
            MVVMHelper.SetTargetName(this, TargetName);
        }

        /// <summary>
        /// Initializes the new MultiPanelWorkspaceViewModel object.
        /// </summary>
        /// <param name="targetName">The name of DevExpress docking layout group object which will host this workspace.</param>
        public MultiPanelWorkspaceViewModel(string targetName)
        {
            MVVMHelper.SetTargetName(this, targetName);
        }
    }
}
