// -----------------------------------------------------------------
// File name:  ufismult.C - The MULTI APPLICATION VERSION
//
// This is the main DLL source file.  It contains LibMain, the DLL's
// entry point and all other entry points.
// -----------------------------------------------------------------


#include <windows.h>
#include <winsock.h>
#include <stdio.h>
#include <time.h>

// Broadcast buffer
#define MAX_BC_BUF_ENTRIES  (128)
#define FIRSTBC				20000     
#define MAXBCNUM			30000
#define PACKET_LEN     			(1024)  
#define DEFAULT_BC_FN		"C:\\TMP\\BCDATA.DAT"   
#define BC_LOST_TAG			4711

#include <netutil.h>
#include <ufismult.h>


#define START_BCSRV
                                                       
// Defines                                             

// To make a DLL without name service usage, define it
#undef NO_NAME_SERVICE 
#define NO_NAME_SERVICE


// Data buffer size
#define DEFAULT_DATA_BUF_SIZE (0x8000L)

char ufis_ct[] = __TIMESTAMP__;					/* Compile time */
static HGLOBAL bc_handle = NULL; 				/* Handle for GetBc buffer			*/
static BC_HEAD  *Pbc_head_gl = NULL;			/* Pointer to GetBc buffer			*/
//static BC_FROM_DZINE  *Pbc_head_gl_dz = NULL;/* Pointer to GetBc buffer (dZine)	*/
static HGLOBAL bcb_handle = NULL; 				/* 		Handle 						*/ 
static int wt_prio = 0;							/* WriteTrace priority				*/
static int traceCount = 0;						// trace buffer line count
static int traceFileIndex = 0;					// trace file name index
static char pcmTmpPath[500];


//#define CONFIG_FILE_NAME "c:\\ufis\\rel_lux\\system\\ufisappl.cfg"
static char pcgConfigFileName[228] = "c:\\ufis\\system\\ceda.ini";

//#define CONFIG_FILE_NAME2 "c:\\ufis\\system\\ufisappl.cfg"

/* ******************************************************************** */
/* External variables							*/
/* ******************************************************************** */

HANDLE ghDLLInst = 0; 
char trace_buffer [9172] = "";  				/* The trace buffer */

FILE  *prgBcDataFile = NULL;				/* file pointer for BCs, stored on Harddisk */

//BC_STRUCT *prgBcBuf[MAXBCNUM+1];


// Static Variables  -- shared     
#pragma data_seg(".sdata")
HWND   gHidhWnd = 0;           				// handle to window of the hidden task
int    gcUsage =0;         				// number of times an application that uses

short psgLostBcs[MAXBCNUM+1] = {0};

char nam_service[10] = "";					/* Namsrv service name: CEDA, EXCO, CEDA2 ...*/
char pcgAddr[24] = "";
char pcgAddr2[24] = "";

//SERVENT *sp = NULL;							// Service by name

MULTI multi[MAX_NO_OF_APPL] = {0};			/* Multi application variables		*/
static char UFIS32rgGlobalPacket[PACKET_LEN+40] = {0};		/* GetBc buffer			*/
//static COMMIF  *prgUFIS32rgGlobalPacket = (COMMIF *) UFIS32rgGlobalPacket;
//static COMMIF  *Packet = NULL;				
//static char Pbc_buffer[(MAX_BC_BUF_ENTRIES+2) * PACKET_LEN] = {""};					/* Broadcast buffer					*/
//static BC_STRUCT Pbc_buffer[(MAX_BC_BUF_ENTRIES+2)];
static BC_STRUCT *prgFirstBc = NULL;
static BC_STRUCT *prgNextFreeBc = NULL;
static BC_STRUCT *prgLastUsedBc = NULL;
//static int bcb_nextfree = -1;					/* Place in Pbc_buffer to add next BC	*/                   
//static int bcb_first = 0;						/*		First entry					*/                   
//static int bcb_no = 0;							/*		Number of entries			*/                   
static int in_bcbuf=FALSE;						/* Sync for BcBuf */
//HWND pfgBcWindow = 0;

// DDC functionality
static int dZine = 0;
static char nam_host_name[100] = "";					// CEDA host name

#pragma data_seg()




/* ******************************************************************** */
/* Static function prototypes							*/
/* ******************************************************************** */

static int TbToBuf (BC_HEAD  *Pbc_head, char * req_id, char * dest1, 
			 char * dest2, char * cmd, 
	         char * object, char * seq, char * tws, char * twe, char * selection, 	/*  (Bsp: WHERE CARR = 'LH')	*/
	         char * fields, char * data );               
	         
static int BufToTb (BC_HEAD  *Pbc_head, char * req_id, char * dest1, 
			 char * dest2, char * cmd, 
	         char * object, char * seq,char * tws, char * twe, char * selection, 	/*  (Bsp: WHERE CARR = 'LH')	*/
	         char * fields, char * data, int fill_data);

static DWORD CalcBufLen (char * selection, char * fields, char * data);
static int SendBuffer (SOCKET socket, BC_HEAD  *Pbc_head, long buflen);
static int SendBcBuffer (SOCKET socket, BC_HEAD  *Pbc_head, long buflen, short bc_port_no);
static int RecBuffer (SOCKET socket, BC_HEAD  *  *Pbc_head, 
						DWORD *buflen , HGLOBAL *handle);
static int SendShutdown (SOCKET socket);
static int AdjustBcNum (int new_num);
static int BcBufGet (char * Pentry,int ipIndex);
//static void InitMultiStruct (MULTI *Pmulti, HTASK task);
static void InitMultiStruct (MULTI *Pmulti, DWORD task);
//static int SearchMultiStruct (HTASK htask);
static int SearchMultiStruct (DWORD htask);

static int SearchNewMultiStruct (void);
int  PASCAL   GetLostBcList(char **pcpLostBcList);
int  PASCAL CleanupComGo(int t_i);
int  PASCAL RegisterTask(char *);
void  PASCAL UnRegisterTask(void);
BOOL  PASCAL EnumWindowsCallback(HWND hWnd, LONG lParam);
void PASCAL ForceTrace(char *buf);
                                                     

// Network Constants                                                      
            
#define UFIS_SERVICE_NAME 		"UFIS"
#define SYMAP_SERVICE_NAME 		"SYMAP"
#define UFIS_BC_SERVICE_NAME 	"UFIS_BC"
#define UFIS_PORT_NO 			(3350)
#define UFIS_BC_PORT_NO			(3351)
#define SYMAP_PORT_NO 			(3450)

     
#define DATAPACKETLEN 			(1024-12)     

#define NETREAD_SIZE 			(0x8000)


// -----------------------------------------------------------------
//
// Function: LibMain
//
// Purpose : This is the DLL's entry point.  It is analogous to WinMain
//           for applications.
//
// Params  : hInstance   ==  The handle to the DLL's instance.
//           wDataSeg    ==  Basically it is a pointer to the DLL's
//                           data segment.
//           wHeapSize   ==  Size of the DLL's heap in bytes.
//           lpszCmdLine ==  The command line passed to the DLL
//                           by Windows.  This is rarely used.
//
// Returns : 1 indicating DLL initialization is successful.
//
// Comments: LibMain is called by Windows.  Do not call it in your
//           application!
// -----------------------------------------------------------------

int  PASCAL LibMain (HANDLE hInstance,
                        WORD   wDataSeg,
                        WORD   wHeapSize,
                        char *  lpszCmdLine)
   {
   ghDLLInst = hInstance;               
   
   strcpy ((char *)nam_service, (char *) NAMSRV_TYPE_CEDA); 

/*   if (wHeapSize != 0)   // If DLL data seg is MOVEABLE
      UnlockData (0);
*/
   return (1);
   }


int  PASCAL RegisterBcWindow(HWND hpBcWindow)
{
	int ilIndex;

	ilIndex = MakeNewMultiStruct();
	if (ilIndex < 0)
		return RC_NOT_FOUND;
			     
	multi[ilIndex].BcWindow = hpBcWindow;
	return RC_SUCCESS;
}

int  PASCAL UnRegisterBcWindow(HWND hpBcWindow)
{

	int ilIndex;

	ilIndex = MakeNewMultiStruct();
	if (ilIndex < 0)
		return RC_NOT_FOUND;
			     
	multi[ilIndex].BcWindow = 0;
	return RC_SUCCESS;
	
}
          
// -----------------------------------------------------------------
//
// Function: InitCom
//
// Purpose : Initialization of the UFIS DLL
//
// Params  : HostType:	Host Type for Name service
//			 CedaHost:	Name of CEDA host
//
// Returns : 0 Operation was successful. 
//			 < 0: Communication error
//			 > 0: Error code from ceda
//
// Comments: 
//           
// -----------------------------------------------------------------
int  PASCAL InitCom  (char * HostType, char * CedaHost)
{                 
	int rc = RC_SUCCESS;
	HTASK task=NULL;
	int t_i=-1;
	FILE *config = NULL;	// config file
	int i, len;
	char host1[100];
	char host2[100];
	char pclFileName[500];
//	char configbuf[1000];
//	int bufsiz;
//    int i;
         
	SOCKADDR_IN rlSAddr;
	int ilLen = sizeof(SOCKADDR_IN);


	wt_prio = WT_ERROR; // TEST only

	Yield();            
	t_i = MakeNewMultiStruct();
	if (t_i < 0)
		return RC_TO_MANY_APP;

	traceCount = 0;
			     
	multi[t_i].outpt = (FILE *) fopen (multi[t_i].TraceFileName,"w");
	if (multi[t_i].outpt == NULL)     
	{
		rc =  RC_INIT_FAIL;
		return rc;
    } /* end if */  

    if(getenv("UFISTMP") != NULL)
		strcpy(pcmTmpPath, getenv("UFISTMP"));
	else
		strcpy(pcmTmpPath, "C:\\TMP");
	sprintf(pclFileName, "%s\\BCDATA.DAT", pcmTmpPath);

	prgBcDataFile = (FILE *) fopen (pclFileName,"rb");

	if (multi[t_i].IsInit == TRUE)
		return RC_ALREADY_INIT;
		                   
			                   
	strcpy (multi[t_i].ceda_host_name, CedaHost); 
	 
	// If new Hosttype is given ...
	if (HostType != NULL && HostType[0] != '1' && 
		HostType[0] != ' ' && HostType[0] != EOS)
	{
		strncpy ((char *)nam_service, HostType, 9);
		nam_service[9] = EOS; 
	}
	
    if (rc == RC_SUCCESS)
    {
     	rc = InitWinsock ();
    }

#ifndef NO_NAME_SERVICE
    if (rc == RC_SUCCESS)
    {
		nam_host_name[0]=EOS;
		rc = GetCedaHost ((char *) nam_host_name);    
		sprintf(trace_buffer,"Name service returns %d host=%s",rc, nam_host_name); 
		WriteTrace(trace_buffer, WT_DEBUG);
	}
#else
	//nam_host_name[0]=EOS;
	strcpy((char *) nam_host_name,CedaHost);
	sprintf(trace_buffer,"Name service not used"); 
	WriteTrace(trace_buffer, WT_DEBUG);
	sprintf(trace_buffer,"ARG-HOST=%s",CedaHost); 
	WriteTrace(trace_buffer, WT_DEBUG);
#endif
	    
	multi[t_i].IsNonBlock = FALSE;

    if (rc == RC_SUCCESS || rc == RC_NOT_FOUND)
    { 
    	rc = CreateSocket( (SOCKET  *) &multi[t_i].gl_conn_socket, SOCK_STREAM, 
    					UFIS_SERVICE_NAME, FALSE );
    }
	sprintf(trace_buffer,"Nach CreateSocket (conn) %d",rc); 
	WriteTrace(trace_buffer, WT_DEBUG);
  
	sprintf(trace_buffer,"InitCom c Calling Process %d Process ID %d",
		GetCurrentProcess(),GetCurrentProcessId());
	WriteTrace(trace_buffer, WT_DEBUG);

// New for HSB by JBE

    if (rc == RC_SUCCESS)
    { 
    	host1[0] = '\0';
    	host2[0] = '\0';
		i = 0;

		while (nam_host_name[i] != '/' && nam_host_name[i] != '\0')
		{
			host1[i] = nam_host_name[i];
			i++;
		}
		host1[i] = '\0';
		if (nam_host_name[i] == '/')
		{
			i++;
			strcpy(host2,&nam_host_name[i]);
		}
		sprintf(trace_buffer,"InitComm: Host Name(s) = <%s> + <%s>",host1,host2); 
		WriteTrace(trace_buffer, WT_ERROR);
		len = strlen(nam_host_name);


		rc = OpenConnection (multi[t_i].gl_conn_socket, host1, UFIS_PORT_NO); 


		
		if (rc == RC_SUCCESS)
		{
			unsigned long llTmpAddr;

			strncpy(CedaHost,host1,len); 
			CedaHost[len] = '\0';
			strcpy(nam_host_name,CedaHost);

			rc = getsockname (multi[t_i].gl_conn_socket,(struct sockaddr *)&rlSAddr,&ilLen);
			if (rc != RC_SUCCESS)
			{
				sprintf(trace_buffer,"InitComm: getsockname failed with ec=%d",WSAGetLastError());
				WriteTrace(trace_buffer,WT_ERROR);
				return RC_INIT_FAIL;
			}

			llTmpAddr = ntohl(rlSAddr.sin_addr.s_addr);
			sprintf(pcgAddr,"%08x",llTmpAddr);

			sprintf(trace_buffer,"InitComm nach OpenConnection: rc=%d <%s> + <%d> <%s>",rc,
					inet_ntoa(rlSAddr.sin_addr),ntohs(rlSAddr.sin_port),pcgAddr); 
			WriteTrace(trace_buffer, WT_ERROR);
			#ifdef START_BCSRV
				rc = RegisterTask(inet_ntoa(rlSAddr.sin_addr));
				if (rc != RC_SUCCESS)
					return RC_INIT_FAIL;
			#endif
		}
		else
		{
			if (strlen(host2) > 0)
			{
				rc = OpenConnection (multi[t_i].gl_conn_socket, host2, UFIS_PORT_NO); 
				if (rc == RC_SUCCESS)
				{
					unsigned long llTmpAddr;

					strncpy(CedaHost,host2,len); 
					CedaHost[len] = '\0';
					strcpy(nam_host_name,CedaHost);

					getsockname (multi[t_i].gl_conn_socket,(struct sockaddr *)&rlSAddr,&ilLen);

					llTmpAddr = ntohl(rlSAddr.sin_addr.s_addr);
					sprintf(pcgAddr,"%08x",llTmpAddr);


					sprintf(trace_buffer,"InitComm nach OpenConnection: rc=%d <%s> + <%d>",rc,
							inet_ntoa(rlSAddr.sin_addr),ntohs(rlSAddr.sin_port)); 
					WriteTrace(trace_buffer, WT_ERROR);
					#ifdef START_BCSRV
						rc = RegisterTask(inet_ntoa(rlSAddr.sin_addr));
						if (rc != RC_SUCCESS)
							return RC_INIT_FAIL;
					#endif
				}
			}
		}
    }
	sprintf(trace_buffer,"Nach OpenConnection %d , HostName = <%s> (used for BC-filtering",rc,nam_host_name); 
	WriteTrace(trace_buffer, WT_DEBUG);

	if (rc == RC_SUCCESS)
		multi[t_i].IsInit = TRUE;

    //	Check DDC functionality
    
    dZine = 0; // dZine not used in UFIS32.DLL

	return rc;
}  /* InitCom */            


          
// -----------------------------------------------------------------
//
// Function: InitComDDC
//
// Purpose : Initialization of the UFIS DLL for DDCs
//
// Params  : IpAddr:	IP address of own host
//			 CedaHost:	Name of CEDA host
//			 bind:		= 0 don't bind socket
//						= 1 bind socket
//
// Returns : 0 Operation was successful. 
//			 < 0: Communication error
//			 > 0: Error code from ceda
//
// Comments: 
//           
// -----------------------------------------------------------------
int  PASCAL InitComDDC  (char * IpAddr, char * CedaHost, int bind)
{                 
	int rc = RC_SUCCESS;
	HTASK task=NULL;
	short broadcast_bool = 1;	/* Arg for setsockopt */
	SOCKET socket;
	int t_i=-1;
	SOCKERR serr = 0;

	Yield();
	t_i = MakeNewMultiStruct();
	if (t_i < 0)
		return RC_NOT_FOUND;
			                   
	multi[t_i].IsNonBlock = FALSE;

    if (rc == RC_SUCCESS && bind == 1)
    { 
    	rc = CreateSocketBind( (SOCKET  *) &multi[t_i].gl_conn_DDC_socket, SOCK_STREAM, 
    					UFIS_SERVICE_NAME, TRUE, UFIS_PORT_NO );
    }
    if (rc == RC_SUCCESS && bind == 0)
    { 
    	rc = CreateSocket( (SOCKET  *) &multi[t_i].gl_conn_DDC_socket, SOCK_STREAM, 
    					UFIS_SERVICE_NAME, TRUE );
    }
	sprintf(trace_buffer,"Nach CreateSocket (conn) %d",rc); 
	WriteTrace(trace_buffer, WT_DEBUG);

    if (rc == RC_SUCCESS && bind == 1)
    {
    	socket = multi[t_i].gl_conn_DDC_socket; 
		sprintf(trace_buffer,"Vor WSAASyncSelect %d , %d , %d , %d , %ld",
		(SOCKET) socket,(HWND) gHidhWnd,(unsigned int) WM_USER,(unsigned int) GVC_SYNC,
		(long) (FD_READ|FD_ACCEPT)); 
		WriteTrace(trace_buffer, WT_DEBUG);
    	rc = WSAAsyncSelect( (SOCKET) socket, (HWND) gHidhWnd, (unsigned int) GVC_SYNC,
    						 (long) (FD_READ|FD_ACCEPT) );
    	if (rc != 0)
    	{
    	serr = WSAGetLastError();
		sprintf(trace_buffer,"Error WSAASyncSelect %s",SockerrToString(serr)); 
		WriteTrace(trace_buffer, WT_DEBUG);
		rc = RC_INIT_FAIL;
    	}
		sprintf(trace_buffer,"Nach WSAAsyncSelect %d",rc); 
		WriteTrace(trace_buffer, WT_DEBUG);
    }

    if (rc == RC_SUCCESS)
    { 
    	rc = CreateSocket( (SOCKET  *) &multi[t_i].gl_bc_socket, SOCK_DGRAM, 
    					UFIS_BC_SERVICE_NAME, TRUE );
    }
	sprintf(trace_buffer,"Nach CreateSocket (bc) %d",rc); 
	WriteTrace(trace_buffer, WT_DEBUG);

    if (rc == RC_SUCCESS)
    {
    	socket = multi[t_i].gl_bc_socket; 
    	rc = setsockopt( socket, SOL_SOCKET, SO_BROADCAST, (char *) &broadcast_bool, 
    					 sizeof (broadcast_bool) );
    }
	sprintf(trace_buffer,"Nach SetSockOpt (SO_BROADCAST) %d",rc); 
	WriteTrace(trace_buffer, WT_DEBUG);
    
	if (rc == RC_SUCCESS)
		multi[t_i].IsInit = TRUE;

	return rc;
}  /* InitComDDC */            



// -----------------------------------------------------------------
//
// Function: CleanupCom (Frontend)
//
// Purpose : Cleanup of the UFIS DLL
//
// Params  : none
//
// Returns : 0 Operation was successful. 
//			 < 0: Communication error
//			 > 0: Error code from ceda
//
// Comments: 
//           
// -----------------------------------------------------------------
int  PASCAL CleanupCom  (void)
{
	int t_i;
            
	t_i = SearchMultiStruct(GetCurrentProcessId());
	if (t_i == RC_NOT_FOUND)
		return RC_NOT_FOUND;
	
	return CleanupComGo(t_i);
} /* end CleanupCom */

// -----------------------------------------------------------------
//
// Function: CleanupComGo
//
// Purpose : Cleanup of the UFIS DLL
//
// Params  : none
//
// Returns : 0 Operation was successful. 
//			 < 0: Communication error
//			 > 0: Error code from ceda
//
// Comments: 
//           
// -----------------------------------------------------------------
int  PASCAL CleanupComGo  (int t_i)
{
	int rc = RC_SUCCESS;
	int i=0;
            
	Yield();            
	if (t_i < 0)
		return RC_NOT_FOUND;
	
#ifdef START_BCSRV	
	UnRegisterTask();
#endif
	multi[t_i].BcWindow = 0;

	(void) SendShutdown (multi[t_i].gl_conn_socket);
	(void) ResetSocket (multi[t_i].gl_conn_socket);
    multi[t_i].gl_conn_socket = INVALID_SOCKET;
	(void) ResetSocket (multi[t_i].gl_conn_DDC_socket);
    multi[t_i].gl_conn_DDC_socket = INVALID_SOCKET;
	(void) ResetSocket (multi[t_i].gl_bc_socket);
    multi[t_i].gl_bc_socket = INVALID_SOCKET;
	for (i=0; i < NO_OF_CONNECTIONS; i++)
	{
		if (multi[t_i].gl_conn_gvc[i] != INVALID_SOCKET)
		{
			(void) SendShutdown (multi[t_i].gl_conn_gvc[i]);
			(void) ResetSocket (multi[t_i].gl_conn_gvc[i]);
	    	multi[t_i].gl_conn_gvc[i] = INVALID_SOCKET;
		}
	} /* for */
	sprintf(trace_buffer,"Nach ResetSocket "); 
	WriteTrace(trace_buffer, WT_DEBUG);

	rc = CleanupWinsock();

	if (multi[t_i].cc_handle != NULL)
	{
		DoFree (multi[t_i].cc_handle);
		multi[t_i].cc_handle = NULL;
	}

	for (i=0; i < NO_OF_DATA_BUFFERS; i++)
	{
		if (multi[t_i].buf_handle[i] != NULL)
			GlobalUnlock (multi[t_i].buf_handle[i]);	
	} /* for */
	
	if (multi[t_i].outpt != NULL)
	{
		fclose (multi[t_i].outpt); 
		multi[t_i].outpt = NULL;
	}
	multi[t_i].IsInit = FALSE;
	multi[t_i].used = FALSE;
	
	return rc;
} /* end CleanupComGo */


// -----------------------------------------------------------------
//
// Function: CleanupComGvc
//
// Purpose : Shutdown GVC Socket
//
// Params  : GVC Index
//
// Returns : 0 Operation was successful. 
//			 < 0: Communication error
//			 > 0: Error code from ceda
//
// Comments: 
//           
// -----------------------------------------------------------------
int  PASCAL CleanupComGvc  (int gvcInd)
{
	int rc = RC_SUCCESS;
	int t_i=-1;
	int i=0;

	Yield();
	sprintf(trace_buffer,"Enter CleanupComGvc "); 
	WriteTrace(trace_buffer, WT_DEBUG);

	t_i = MakeNewMultiStruct();
	if (t_i < 0)
		return RC_NOT_FOUND;

	if (multi[t_i].gl_conn_gvc[gvcInd] != INVALID_SOCKET)
	{
		(void) SendShutdown (multi[t_i].gl_conn_gvc[gvcInd]);
		(void) ResetSocket (multi[t_i].gl_conn_gvc[gvcInd]);
	    multi[t_i].gl_conn_gvc[gvcInd] = INVALID_SOCKET;
	}
		
	sprintf(trace_buffer,"Nach ResetSocket GVC "); 
	WriteTrace(trace_buffer, WT_DEBUG);
	
	return rc;
} /* end CleanupComGvc */


                 
// -----------------------------------------------------------------
//
// Function: UfisDllAdmin
//
// Purpose : Configure the UFIS DLL
//
// Params  : Command string, 2 args
//
// Returns : 0 Operation was successful. 
//			 < 0:  error
//
// Comments: Toolbook interface to set debug params and others. 
//			 All args are strings.     
//
//			 Supported commands are:
//	COMMAND		ARG1			ARG2	 		Purpose 
//
//	TRACE		ON | OFF		DEBUG | ERROR | FATAL 	Switch Trace on / off
//												and sets the priority
//	TRACEFN		<file name>						Set the trace file name 
//  BROADCAST	ON | OFF						Opens or closes the 
//												broadcast socket 
//	BUFSIZE		DATA			<size in bytes>	Sets the buffer size for
//												the data buffer. If more data
//												is received, CallCeda will
//												copy only <size> bytes and
//												return RC_DATA_CUT 
//  
//           
// -----------------------------------------------------------------
int  PASCAL UfisDllAdmin  (char * Pcommand, char * Parg1, char * Parg2)
{
	int rc = RC_SUCCESS;
	int t_i=-1;

	Yield();
	t_i = MakeNewMultiStruct();
	if (t_i < 0)
		return RC_TO_MANY_APP;
			                   

//TRACE ******************************************************************
	if (strcmp (Pcommand, (char *) "TRACE") == 0)
	{
		if (strcmp (Parg1, (char *) "ON") == 0)
		{    
		 	multi[t_i].do_trace = TRUE;
			if (strcmp (Parg2, (char *) "DEBUG") == 0)
			{    
				wt_prio = WT_DEBUG;
			}
			else if (strcmp (Parg2, (char *) "ERROR") == 0)
			{
				wt_prio = WT_ERROR;
			}
			else if (strcmp (Parg2, (char *) "FATAL") == 0)
			{    
				wt_prio = WT_FATAL_ERROR;
			}		 	
	 	}
	 	else
		{    
		 	multi[t_i].do_trace = FALSE;
	 	} /* end if arg1 */
	}  /* enn if TRACE */

//TRACEFN ****************************************************************
	else if (strcmp (Pcommand, (char *) "TRACEFN") == 0)
	{
		strncpy ((char *)multi[t_i].TraceFileName, Parg1, __min (TRACE_FN_LEN, 
					strlen(Parg1) +1));
		//	check for special trace file name with extension ".0"
		traceFileIndex = strlen(Parg1) - 2;
		if (strncmp(&multi[t_i].TraceFileName[traceFileIndex],".0",2) != 0)
		{
			traceFileIndex = 0;
		}
		else
		{
			traceFileIndex++;
		}
	}  /* end if TRACEFN */

//TRACE ******************************************************************
	else if (strcmp (Pcommand, (char *) "BUFSIZE") == 0)
	{
		if (strcmp (Parg1, (char *) "DATA") == 0)
		{     
			multi[t_i].data_buf_size = atol(Parg2);
			if (multi[t_i].data_buf_size <= 0)
			{
				multi[t_i].data_buf_size = DEFAULT_DATA_BUF_SIZE;
				rc = RC_FAIL;
			} /* end if */
		}
		else
		{
			rc = RC_FAIL;
		} /* end if DATA */
	} /* end if BUFSIZE */

//ERROR ******************************************************************
	else
	{
        rc = RC_FAIL;
	}
              
	sprintf(trace_buffer,"UfisDllAdmin:Args: %Fs %Fs %Fs  rc= %d",
		Pcommand, Parg1, Parg2,rc); 
	WriteTrace(trace_buffer, WT_DEBUG);     
	
	return rc;
} /* end UfisDllAdmin */              

// -----------------------------------------------------------------
//
// Function: GetTraceBuf
//
// Purpose : Interface to the trace buffer
//
// Params  : none
//
// Returns : Pointer to global trace buffer
//
// Comments: 
//           
// -----------------------------------------------------------------
char *GetTraceBuf  (void)  
{
	return trace_buffer;
}


// -----------------------------------------------------------------
//
// Function: CallCeda
//
// Purpose : This is the DLL's external interface for connection oriented
//           communication with ceda.
//
// Params  : see below
//			 Data Destination (String):
//				RETURN:	Received Data is returned in Arguments
//				BUF1:	Received Data is put in DLL-Buffer 1
//				BUF2:	Received Data is put in DLL-Buffer 2
//				BUF3:	Received Data is put in DLL-Buffer 3
//				BUF4:	Received Data is put in DLL-Buffer 4
//
// Returns : 0 Operation was successful. 
//			 < 0: Communication error
//			 > 0: Error code from ceda
//
// Comments: 
//           
// -----------------------------------------------------------------
  
int  PASCAL CallCeda (char * req_id,		/*  Wird von Toolbook vergeben  */
	         char * dest1,     	/*  Im Regelfall CEDA			*/
	         char * dest2,     	/*  Ergebniss Empfaenger		*/
	         char * cmd,       	/*  Kommando (RT,....)         	*/
	         char * object,   	/*  Object fuer Kommando (bei RT: TableName)*/
	         char * seq,       	/*  Sortierkriterium			*/
	         char * tws,       	/*  Time Window start   		*/
	         char * twe,       	/*  Time Window end				*/
	         char * selection, 	/*  (Bsp: WHERE CARR = 'LH')	*/
	         char * fields,    	/*  (Bsp: AC2,AC3,CARR  )		*/
	         char * data,      	/*  ( )                			*/
	         char * data_dest )	/*  Data Destination			*/
{
	int rc = RC_SUCCESS; 
	DWORD buflen = 0;
	BC_HEAD  *Pbc_head = NULL;
	SOCKET socket;
	short fertig = FALSE;
	short GotBc = FALSE;
	int cnt = 0;
	int t_i=-1;
	char *pclLostBcList = NULL;
	long llStart,llEnd,llUpload,llDownload;
	double dlDuration;

//	char pclSelection[24];

	Yield();
	t_i = MakeNewMultiStruct();
	if (t_i < 0)
		return RC_NOT_FOUND;
			                   
		                      
//	sprintf(trace_buffer,"CallCeda: bcb_nextfree = %d ", bcb_nextfree);
//	WriteTrace(trace_buffer, WT_DEBUG);

	/* Test, if InitComm has been called */		                      
	if (multi[t_i].IsInit == FALSE)
	{
		return RC_INIT_FAIL;
	}

	/* Test, if CallCeda is already entered */		                      
	if (multi[t_i].InCallCeda == TRUE)
	{
		sprintf(trace_buffer, "CallCeda: BUSY!  2nd call rejected with rc = %d ",
						RC_FAIL); 
		WriteTrace(trace_buffer, WT_ERROR);
		return RC_BUSY;
	}

	/* Test, if GetBc is entered */		                      
	if (multi[t_i].InGetBc == TRUE)
	{
		sprintf(trace_buffer, "CallCeda: BUSY!  already in GetBc"); 
		WriteTrace(trace_buffer, WT_ERROR);
		return RC_BUSY;
	}
	
	llStart = clock();
	multi[t_i].InCallCeda = TRUE;
			/**************                        
	sprintf(trace_buffer,
	"CallCeda:Arguments \n%Fs\n%Fs\n%Fs\n%Fs\n%Fs\n%Fs\n%Fs\n%Fs\n%Fs\n%Fs\n%Fs\n"
	"-----------------------------------------",
	req_id, dest1, dest2, cmd, object, seq, tws, twe, selection, fields, data);
	WriteTrace(trace_buffer, WT_DEBUG);
*****************/

	if (multi[t_i].cc_handle != NULL)
	{
		DoFree (multi[t_i].cc_handle);
		multi[t_i].cc_handle = NULL;
	}
		         
	if(strcmp(cmd,"RBS") == 0)
	{
		// reread broadcast, first we fill data with lost BC list
		GetLostBcList(&pclLostBcList);

		{
			char clLost[258];

			strncpy(clLost,pclLostBcList,256);
			clLost[256] = '\0';
	sprintf(trace_buffer,"RBS LostBcList <%s> ",clLost); 
	WriteTrace(trace_buffer, WT_ERROR);
		}

		if (*pclLostBcList != '\0')
		{
		//	multi[t_i].InCallCeda = FALSE;
		//	return RC_SUCCESS;
			data = pclLostBcList;
		}
		else
		{
			multi[t_i].InCallCeda = FALSE;
			return RC_SUCCESS;
		}
		strcpy(pcgAddr2,pcgAddr);
		selection = pcgAddr2;
		// fill selection with own IP-Address
	}
	
	buflen = CalcBufLen (selection, fields, data);
	llUpload = buflen;
	Pbc_head = (BC_HEAD  *) DoMalloc ( (long) buflen, &multi[t_i].cc_handle);
	if (Pbc_head == NULL)
		rc = RC_FAIL;

    if (rc == RC_SUCCESS)
    {    
    	(void) TbToBuf ((BC_HEAD  *) Pbc_head, req_id, dest1, dest2, cmd, object, seq, tws,  
    				twe,  selection, fields, data );
	}
 
	socket = multi[t_i].gl_conn_socket;

    if (rc == RC_SUCCESS)
    { 
    	rc = SendBuffer (socket, (BC_HEAD  *) Pbc_head, (long)buflen);
	}
	sprintf(trace_buffer,"Nach SendBuffer %d",rc); 
	WriteTrace(trace_buffer, WT_DEBUG);
	
	DoFree (multi[t_i].cc_handle);
	multi[t_i].cc_handle = NULL;

    
  	if (rc == RC_SUCCESS)
   	{ 
    	rc = RecBuffer (socket, &Pbc_head, &buflen, &multi[t_i].cc_handle);
	}
	sprintf(trace_buffer,"Nach RecBuffer %d",rc); 
	WriteTrace(trace_buffer, WT_DEBUG);
//	snap ((char *)Pbc_head, (int)buflen); 
	                
    if (rc == RC_SUCCESS)
    {    
		data[0] = EOS;

		if (strcmp (data_dest, "RETURN") == 0)
		{
         	rc = BufToTb (Pbc_head, req_id, dest1, dest2, cmd, object, seq, tws,
    						twe,  selection, fields, data, TRUE );
			llDownload = strlen(data);
    	}
    	else
    	{
         	rc = BufToTb (Pbc_head, req_id, dest1, dest2, cmd, object, seq, tws,
    						twe,  selection, fields, data, FALSE );
    		if (rc == RC_SUCCESS)
    		{
    			rc = StoreBuffer ((BC_HEAD  *) Pbc_head, buflen, data_dest, t_i);
				llDownload = buflen;
    		} /* fi */
    	} /* fi */
		llEnd = clock();
		dlDuration = llEnd - llStart;
		//
		sprintf(trace_buffer,
		"CallCeda: <%s> <%s> Dauer: %2.2f Upload %ld Download %ld",
			cmd,object,dlDuration/1000,llUpload,llDownload);
		WriteTrace(trace_buffer, WT_DEBUG);
	} /* fi */            
	if(pclLostBcList != NULL)
	{
		free(pclLostBcList);
	}
	multi[t_i].InCallCeda = FALSE;

	return rc;
}  /* end CallCeda */


// -----------------------------------------------------------------
//
// Function: SendBc
//
// Purpose : This is the DLL's external interface for sending a
//           broadcast message.
//
// Params  : see below
//			 Data Destination (String):
//				RETURN:	Received Data is returned in Arguments
//				BUF1:	Received Data is put in DLL-Buffer 1
//				BUF2:	Received Data is put in DLL-Buffer 2
//				BUF3:	Received Data is put in DLL-Buffer 3
//				BUF4:	Received Data is put in DLL-Buffer 4
//
// Returns : 0 Operation was successful. 
//			 < 0: Communication error
//			 > 0: Error code from ceda
//
// Comments: 
//           
// -----------------------------------------------------------------
  
int  PASCAL SendBc (char * req_id,		/*  Wird von Toolbook vergeben  */
	         char * dest1,     	/*  Im Regelfall CEDA			*/
	         char * dest2,     	/*  Ergebniss Empfaenger		*/
	         char * cmd,       	/*  Kommando (RT,....)         	*/
	         char * object,   	/*  Object fuer Kommando (bei RT: TableName)*/
	         char * seq,       	/*  Sortierkriterium			*/
	         char * tws,       	/*  Time Window start   		*/
	         char * twe,       	/*  Time Window end				*/
	         char * selection, 	/*  (Bsp: WHERE CARR = 'LH')	*/
	         char * fields,    	/*  (Bsp: AC2,AC3,CARR  )		*/
	         char * data,      	/*  ( )                			*/
	         char * data_dest )	/*  Data Destination			*/
{
	int rc = RC_SUCCESS; 
	DWORD buflen = 0;
	BC_HEAD  *Pbc_head = NULL;
	//BC_TO_DZINE  *Pbc_head_dz = NULL;
	//BC_TO_DZINE_TIME  *Pbc_head_dz_t = NULL;
	SOCKET socket;
	int t_i=-1;
	int maxlpcnt=3;
	time_t CurrentTime=0;
	time_t PrevTime=0;

	Yield();
	t_i = MakeNewMultiStruct();
	if (t_i < 0)
		return RC_NOT_FOUND;


	/* Test, if InitComm has been called */		                      
	if (multi[t_i].IsInit == FALSE)
	{
		return RC_INIT_FAIL;
	}

	sprintf(trace_buffer,
	"SendBc:Arguments \n%Fs\n%Fs\n%Fs\n%Fs\n%Fs\n%Fs\n%Fs\n%Fs\n%Fs\n%Fs\n%Fs\n"
	"-----------------------------------------",
	req_id, dest1, dest2, cmd, object, seq, tws, twe, selection, fields, data);
	WriteTrace(trace_buffer, WT_DEBUG);

	if (multi[t_i].cc_handle != NULL)
	{
		DoFree (multi[t_i].cc_handle);
		multi[t_i].cc_handle = NULL;
	}
		                        
	buflen = CalcBufLen (selection, fields, data);          

	Pbc_head = (BC_HEAD  *) DoMalloc ( (long) buflen, &multi[t_i].cc_handle);
	if (Pbc_head == NULL)
		rc = RC_FAIL;

    if (rc == RC_SUCCESS)
    {
    	{    
    		(void) TbToBuf ((BC_HEAD  *) Pbc_head, req_id, dest1, dest2, cmd, object, seq, tws,  
    						twe,  selection, fields, data );
    	}
	}

	socket = multi[t_i].gl_bc_socket;

    if (rc == RC_SUCCESS)
    { 
    	rc = SendBcBuffer (socket, (BC_HEAD  *) Pbc_head, (long)buflen, UFIS_BC_PORT_NO);
	}

	sprintf(trace_buffer,"Nach SendBcBuffer %d",rc);
	WriteTrace(trace_buffer,WT_DEBUG);
		

	DoFree (multi[t_i].cc_handle);
	multi[t_i].cc_handle = NULL;

	return rc;
}  /* end SendBc */

                 

                 
// -----------------------------------------------------------------
//
// Function: GetBc - Get Broadcast
//
// Purpose : This is the DLL's external interface for receiving broadcasts 
//           from ceda.
//
// Params  : Like CallCeda, see below
//
// Returns : 0 Operation was successful. 
//			 < 0: Communication error
//			 > 0: Error code from ceda
//
// Comments: 
//           
// -----------------------------------------------------------------
  
int  PASCAL GetBc (char * req_id,		/*  Wird von Toolbook vergeben  */
	         char * dest1,     	/*  Im Regelfall CEDA			*/
	         char * dest2,     	/*  Ergebniss Empfaenger		*/
	         char * cmd,       	/*  Kommando (RT,....)         	*/
	         char * object,   	/*  Object fuer Kommando (bei RT: TableName)*/
	         char * seq,       	/*  Sortierkriterium			*/
	         char * tws,       	/*  Time Window start   		*/
	         char * twe,       	/*  Time Window end				*/
	         char * selection, 	/*  (Bsp: WHERE CARR = 'LH')	*/
	         char * fields,    	/*  (Bsp: AC2,AC3,CARR  )		*/
	         char * data,      	/*  ( )                			*/
	         char * bc_num)		/*  received broadcast number	*/
{
	int rc = RC_SUCCESS; 
	int len = PACKET_LEN;
	int t_i=-1;
    char buf[8];
  //  char tmpbuf[100];


    
	Yield();

//	sprintf(trace_buffer,"GetBC: Start bcb_first = %d  bcb_no = %d  ",
//				bcb_first, bcb_no);
//	WriteTrace(trace_buffer, WT_DEBUG);
	
	t_i = MakeNewMultiStruct();
	if (t_i < 0)
		return RC_NOT_FOUND;
			                   
		                      
	/* Test, if InitComm has been called */		                      
	if (multi[t_i].IsInit == FALSE)
	{
		return RC_INIT_FAIL;
	}
    
    /* no parallel work */
    if (multi[t_i].InCallCeda)
	{
		return RC_BUSY;
	}

	/* Test, if GetBc is already entered */		                      
	if (multi[t_i].InGetBc == TRUE)
	{
		sprintf(trace_buffer, "GetBc: BUSY!  already in GetBc"); 
		WriteTrace(trace_buffer, WT_ERROR);
		return RC_BUSY;
	}
	multi[t_i].InGetBc = TRUE;
	    
//	sprintf(trace_buffer,"GetBC:start bcb_first = %d  bcb_no = %d",bcb_first, bcb_no);
//	WriteTrace(trace_buffer, WT_DEBUG);
/*	sprintf(trace_buffer,
	"GetBc:Arguments \n%Fs\n%Fs\n%Fs\n%Fs\n%Fs\n%Fs\n%Fs\n%Fs\n%Fs\n%Fs\n%Fs\n"
	"-----------------------------------------",
	req_id, dest1, dest2, cmd, object, seq, tws, twe, selection, fields, data);
	WriteTrace(trace_buffer, WT_DEBUG);
*/        

    /* Is there a packet in the buffer ? */
    if (rc == RC_SUCCESS)
		rc = BcBufGet (UFIS32rgGlobalPacket,t_i);
	
	/**
	 sprintf(trace_buffer,"GetBC: Packet found bcb_nextfree = %d  Commif_cmd = %d",
			bcb_nextfree,  UFIS32rgGlobalPacket.command);
	WriteTrace(trace_buffer, WT_DEBUG);
	**/
	if (ntohs(((COMMIF *) UFIS32rgGlobalPacket)->command) == PACKET_DATA || ntohs(((COMMIF *) UFIS32rgGlobalPacket)->command) == PACKET_START ||
		ntohs(((COMMIF *) UFIS32rgGlobalPacket)->command) == PACKET_END)
	{
		((COMMIF *) UFIS32rgGlobalPacket)->command = ntohs(((COMMIF *) UFIS32rgGlobalPacket)->command);
		((COMMIF *) UFIS32rgGlobalPacket)->length = ntohl(((COMMIF *) UFIS32rgGlobalPacket)->length);
	}

    if (rc == RC_SUCCESS)
    { 
		len = (int) ((COMMIF *) UFIS32rgGlobalPacket)->length;
    	Pbc_head_gl = (BC_HEAD  *) ((COMMIF *) UFIS32rgGlobalPacket)->data;                         

    	rc = BufToTb (Pbc_head_gl, req_id, dest1, dest2, cmd, object, seq, tws,
    				twe,  selection, fields, data, TRUE );
    	sprintf (buf , "%d",(int) Pbc_head_gl->bc_num);
	   	strcpy (bc_num, (char *) buf); 
//				sprintf(trace_buffer,"BC_NUM = %d",(int) Pbc_head_gl->bc_num);
//				WriteTrace(trace_buffer, WT_DEBUG);
		if (rc == RC_SUCCESS && Pbc_head_gl->rc == RC_EMPTY_MSG)
		{
			rc = RC_NOT_FOUND;
		}                     
		
    }
                          
	multi[t_i].InGetBc = FALSE;
	return rc;                          
} /* GetBc */


// -----------------------------------------------------------------
//
// Function: CalcBufLen
//
// Purpose : Calculates the length of the BC_HEAD buffer
//
// Params  : see below
//
// Returns : > 0 Operation was successful. 
//			 < 0:  error
//
// Comments: 
//           
// -----------------------------------------------------------------

static DWORD CalcBufLen (char * selection, char * fields, char * data)
{               
	DWORD cmd_size, data_size, tot_size;
	
    cmd_size = sizeof (CMDBLK) - 1 + strlen (selection) +
    					strlen (fields);
	
    data_size = strlen (data) ;

    tot_size = sizeof (BC_HEAD) - 1 + cmd_size + data_size + 4;

	sprintf(trace_buffer,"CalcBufLen: %d",tot_size); 
	WriteTrace(trace_buffer, WT_DEBUG);                           
	
	return tot_size;
}




// -----------------------------------------------------------------
//
// Function: TbToBuf
//
// Purpose : Converts the arguments got from Toolbock to UFIS network 
//           buffer format
//
// Params  : see below
//
// Returns : 0 Operation was successful.  ALWAYS !
//			 < 0:  error
//
// Comments: 
//           
// -----------------------------------------------------------------

static int TbToBuf (BC_HEAD  *Pbc_head,  /* Result buffer 		*/
			 char * req_id,		/*  Wird von Toolbook vergeben  */
	         char * dest1,     	/*  Im Regelfall CEDA			*/
	         char * dest2,     	/*  Ergebniss Empfaenger		*/
	         char * cmd,       	/*  Kommando (RT,....)         	*/
	         char * object,   	/*  Object fuer Kommando (bei RT: TableName)*/
	         char * seq,       	/*  Sortierkriterium			*/
	         char * tws,       	/*  Time Window start   		*/
	         char * twe,       	/*  Time Window end				*/
	         char * selection, 	/*  (Bsp: WHERE CARR = 'LH')	*/
	         char * fields,    	/*  (Bsp: AC2,AC3,CARR  )		*/
	         char * data )      	/*  ( )                			*/
{
	int rc = RC_SUCCESS;         
	CMDBLK  *cmdblk;
	char * Pact = NULL;
          
    Pbc_head->rc = RC_SUCCESS;
	strncpy ((char *) Pbc_head->dest_name, dest1, __min (strlen(dest1) +1, 10)); 
	Pbc_head->orig_name[0] = EOS;   	
	strncpy ((char *) Pbc_head->recv_name, dest2, __min (strlen(dest2) +1,10));    	
    
    Pbc_head->bc_num = 0;  /* Init */
	strncpy ((char *) Pbc_head->seq_id, req_id, __min (strlen(req_id) +1,10));    	
    Pbc_head->act_buf = 0; /* Init */
	Pbc_head->ref_seq_id[0] = EOS;   	
                  
	cmdblk = (CMDBLK  *) &(Pbc_head->data);
	
	strncpy ((char *) &(cmdblk->command), cmd, __min (strlen(cmd) +1, 6));
	strncpy ((char *) &(cmdblk->obj_name), object, __min (strlen(object) +1, 33));
	strncpy ((char *) &(cmdblk->order), seq, __min (strlen(seq) +1, 2));
	strncpy ((char *) &(cmdblk->tw_start), tws, __min (strlen(tws) +1, 33));
	strncpy ((char *) &(cmdblk->tw_end), twe, __min (strlen(twe) +1, 33)); 
	                                      
    Pbc_head->cmd_size = sizeof (CMDBLK) - 1 + strlen (selection) +
    					strlen (fields) ;
	
    Pbc_head->data_size = strlen (data) ;

    Pbc_head->tot_size = sizeof (BC_HEAD) - 1 +
    					Pbc_head->cmd_size  + Pbc_head->data_size;

	Pact = (char *) &(Pbc_head->data) + sizeof (CMDBLK) - 1;				

	strcpy (Pact,	selection);
	Pact += strlen (selection) + 1;
	
	strcpy (Pact,	fields);
	Pact +=	strlen (fields) + 1;

	strcpy (Pact,	data);
        	
	return rc;
}  /* end TbToBuf */
    
    
// -----------------------------------------------------------------
//
// Function: BufToTb
//
// Purpose : Converts UFIS network buffer format to the arguments got 
//			 from Toolbock  
//
// Params  : see below
//
// Returns : 0 Operation was successful. 
//			 < 0:  error
//
// Comments: 
//           
// -----------------------------------------------------------------

static int BufToTb (BC_HEAD  *Pbc_head,  /* Network buffer 		*/
			 char * req_id,		/*    */
	         char * dest1,     	/*  			*/
	         char * dest2,     	/*  Ergebniss Empfaenger		*/
	         char * cmd,       	/*  Kommando (RT,....)         	*/
	         char * object,   	/*  Object fuer Kommando (bei RT: TableName)*/
	         char * seq,       	/*  Sortierkriterium			*/
	         char * tws,       	/*  Time Window start   		*/
	         char * twe,       	/*  Time Window end				*/
	         char * selection, 	/*  (Bsp: WHERE CARR = 'LH')	*/
	         char * fields,    	/*  (Bsp: AC2,AC3,CARR  )		*/
	         char * data,       	/*  ( )                			*/
	         int fill_data )	/*  TRUE or FALSE				*/
{
	int rc = RC_SUCCESS;            
	CMDBLK  *cmdblk;
	char  *Pact = NULL;
	int t_i=-1;
    char  * cur_adr=NULL;
    int count=0;
    MSGBLK  *Pmsgblk=NULL;
    char tmpbuf[256]; 
	char * Pfno = NULL;
	char * Prno = NULL;     
	char * Pfld = NULL;
	char * Pdat = NULL;    
    
	t_i = MakeNewMultiStruct();
	if (t_i < 0)
		return RC_NOT_FOUND;
			    
                                                 
//sprintf(trace_buffer,"BufToTb:Return Code from CEDA: %d", Pbc_head->rc); 
//WriteTrace(trace_buffer, WT_DEBUG);
                                                 
	strncpy ( dest1, (char *) Pbc_head->dest_name, 
			__min (strlen(Pbc_head->dest_name) +1, 10)); 
/*	Pbc_head->orig_name ???   	*/
	strncpy ( dest2, (char *) Pbc_head->recv_name, 
			__min (strlen(Pbc_head->recv_name) +1, 10));    	
    
	strncpy (req_id, (char *) Pbc_head->seq_id, 
			__min (strlen(Pbc_head->seq_id) +1, 10));    	
/*    Pbc_head->act_buf = 0; ?? */
/*	Pbc_head->ref_seq_id[0] = EOS; ?? */   	
                  
	if (Pbc_head->rc != RC_SUCCESS && Pbc_head->rc != RC_EMPTY_MSG && 
		Pbc_head->rc != RC_DATA_CUT)
	{
		snap((char *)Pbc_head,128);
		/* CEDA sends error: data contains error text */
	 	rc = RC_CEDA_FAIL;
//		sprintf(trace_buffer,"BufToTb:CEDA sends error %d", Pbc_head->rc); 
//		WriteTrace(trace_buffer, WT_ERROR);
		if (Pbc_head->rc == RC_CEDA_FAIL)  /* MSGBLK included */
		{
			Pmsgblk = (MSGBLK  *) &(Pbc_head->data);
			Pfno = (char *) Pmsgblk->data;
			Prno = Pfno + strlen (Pfno) + 1;     
			Pfld = Prno + strlen (Prno) + 1;
			Pdat = Pfld + strlen (Pfld) + 1;
/*			sprintf(trace_buffer,
				"BufToTb:CEDA sends CEDAFAIL1; fno=%Fs\n rno=%Fs \nfld=%Fs\n dat=%Fs\n", 
					Pfno, Prno, Pfld, Pdat); 
			WriteTrace(trace_buffer, WT_DEBUG);
*/
			sprintf (tmpbuf, "%ld,%Fs,%Fs", Pmsgblk->prio, Pfno, Prno); 
			strcpy (selection, (char *) tmpbuf);
			strcpy (fields, Pfld);      

			// Debug Version:
			sprintf (tmpbuf, "%ld,%Fs", Pmsgblk->msg_no, Pdat); 
			strcpy (data, (char *) tmpbuf);
			
			/* rc = make_msg (Pmsgblk->msg_no, Pdat, data); */			
			/***
			sprintf(trace_buffer,"BufToTb:CEDA sends CEDAFAIL; Sel=%Fs fields=%Fs data=%Fs\n", 
					selection, fields, data); 
//			WriteTrace(trace_buffer, WT_DEBUG);
*****************/
		}
		else
		{                       			/* Old String included */
			strncpy (data, (char *) Pbc_head->data, 
				__min (strlen(Pbc_head->data) +1, (size_t) multi[t_i].data_buf_size) );
		} /* fi */
	}
	else
	{            
	  if (Pbc_head->rc == RC_EMPTY_MSG)
	  {    
		sprintf(trace_buffer,"BufToTb:CEDA sends Empty Message %d", Pbc_head->rc); 
//		WriteTrace(trace_buffer, WT_ERROR);
	   	;  // nothing
	  }
	  else
	  {
		cmdblk = (CMDBLK  *) &(Pbc_head->data);
	
		strncpy ( cmd, (char *) &(cmdblk->command), 6);
		strncpy ( object, (char *) &(cmdblk->obj_name), 33);
		strncpy ( seq, (char *) &(cmdblk->order), 2);
		strncpy ( tws, (char *) &(cmdblk->tw_start), 33);
		strncpy ( twe, (char *) &(cmdblk->tw_end), 33); 
               

		Pact = (char  *) &(cmdblk->data);				
		strcpy (selection, Pact);
		if (strlen(selection) > 950)
		{
			sprintf(trace_buffer,"*****Selection(first 950 chars):\n%.950\nPact = %d",selection, 
					(int) ((char *)Pact - (char *)Pbc_head));  
		}
		else
		{
			sprintf(trace_buffer,"*****Selection:\n%Fs\nPact = %d",selection, 
				(int) ((char *)Pact - (char *)Pbc_head)); 
		}
//		WriteTrace(trace_buffer, WT_DEBUG);
	
		Pact += strlen (selection) + 1;
		strcpy (fields, Pact);
		if (strlen(fields) > 950)
		{
			sprintf(trace_buffer,"*****Fields(first 950 chars):\n%.950\nPact = %d",fields,
					(int) ((char *)Pact - (char *)Pbc_head)); 
		}
		else
		{
			sprintf(trace_buffer,"*****Fields:\n%Fs\nPact = %d",fields,
					(int) ((char *)Pact - (char *)Pbc_head)); 
		}
//		WriteTrace(trace_buffer, WT_DEBUG);
	    
	    if (fill_data)
	    {
			Pact +=	strlen (fields) + 1;
			if (strlen (Pact) >  multi[t_i].data_buf_size)
			{
				strncpy (data, Pact, (size_t) multi[t_i].data_buf_size);
				rc = RC_DATA_CUT;
			}
			else
			{
				strcpy (data, Pact);
			} /*end if buffer size */ 



				for (count=0,cur_adr = Pact; *cur_adr; cur_adr++)
				{
					if (*cur_adr == '\n')
						count++;
				}
//				sprintf(trace_buffer,"BufToTb:Lines: %d)",count);
//				WriteTrace(trace_buffer, WT_DEBUG);



		//snap (Pact,16);
      	} /* end if fill_data */
	  } /* end if empty msg */
	} /* end if rc fails */         
	
	if (rc == RC_SUCCESS && Pbc_head->rc == RC_DATA_CUT)
	{
		rc = RC_DATA_CUT;
	} /* fi */		

    if (fill_data)
    {
		if (strlen(data) > 950)
		{
			sprintf(trace_buffer,"*****Data(first 950 chars):\n %.950Fs \nPact = %d",data,
					(int) ((char *)Pact - (char *)Pbc_head));
		}
		else
		{ 
			sprintf(trace_buffer,"*****Data:\n%Fs\nPact = %d",data,
					(int) ((char *)Pact - (char *)Pbc_head)); 
		}
//		WriteTrace(trace_buffer, WT_DEBUG);
	} /* fi fill_data */
	
	return rc;
}  /* end BufToTb */
                                             

// -----------------------------------------------------------------
//
// Function: SendBuffer
//
// Purpose : Sends the BC_HEAD buffer on the socket
//
// Params  : see below
//
// Returns : 0 Operation was successful. 
//			 < 0:  error
//
// Comments: 
//           
// -----------------------------------------------------------------
static int SendBuffer (SOCKET socket, BC_HEAD  *Pbc_head, long buflen)  
{             
	int rc = RC_SUCCESS;
	long	remain;
	int	send_len;
	COMMIF	 *ldata;
	COMMIF	ack;
	char   * offs;
	HGLOBAL handle;                       
	SOCKERR serr = 0;
	int ilSockErrCnt = 0;

	ldata 	= (COMMIF  *) DoMalloc((long) (DATAPACKETLEN + sizeof(COMMIF)), &handle);
	offs	= (char  *) Pbc_head;
	remain	= buflen;                             
	
	do {
		if ( remain > DATAPACKETLEN ) {
			ldata->command = htons((u_short)PACKET_DATA);
			ldata->length = htonl((long) DATAPACKETLEN);
			memcpy((char *) &(ldata->data),offs,DATAPACKETLEN);
			remain -= DATAPACKETLEN;
			offs+=DATAPACKETLEN;
		} /* end if */
		else {
			ldata->length = htonl(remain);
			ldata->command = htons((u_short)PACKET_END);
			memcpy((char *) &(ldata->data),offs,(int)remain);
			remain = 0;
		} /* end else */                   
		send_len = (int) (htonl(ldata->length) + sizeof(COMMIF));
		sprintf(trace_buffer,"SendBuffer: Vor send: len=%d ldata_len=%ld", 
				send_len,htonl(ldata->length) );
		WriteTrace(trace_buffer, WT_DEBUG);    
//		rc = send (socket, (char *)ldata, PACKET_LEN, 0);
		rc = send (socket, (char *)ldata, send_len, 0);
	    Yield();
		if ( rc == SOCKET_ERROR ) {
			serr = WSAGetLastError();
			sprintf(trace_buffer,"SendBuffer: send: %s", SockerrToString( serr ) );
			WriteTrace(trace_buffer, WT_DEBUG);  
			ilSockErrCnt++;
			rc = RC_COMM_FAIL;
		} /* end if */
		else {
			do {
				rc = recv(socket,(char *) &ack,sizeof(COMMIF), 0);
				Yield();
				sprintf(trace_buffer,"(%d) received ACKn",rc); 
				WriteTrace(trace_buffer, WT_DEBUG);
				if ( rc == SOCKET_ERROR) {
					serr = WSAGetLastError();
					sprintf(trace_buffer,"SendBuffer: recv: %s", SockerrToString( serr ) );
					WriteTrace(trace_buffer, WT_ERROR);    
					rc = RC_COMM_FAIL;
					ilSockErrCnt++;
				} /* end if */
			} while ( rc == 0 && rc != RC_COMM_FAIL );
		} /* end else */
	} while (remain >0 && rc != RC_COMM_FAIL);
	
	DoFree(handle);

    if(ilSockErrCnt > 0)
	{
	    char tmpbuf[256]; 
		sprintf(tmpbuf, "A critical physical connection error occured <error code %d>.\nPlease call your System Administrator", serr);
		MessageBox(NULL, tmpbuf, "Critcal Connection Error", MB_OK|MB_ICONEXCLAMATION|MB_SYSTEMMODAL);
	}
    if (rc != RC_COMM_FAIL)
    	rc = RC_SUCCESS;
 	return rc;
} /* end SendBuffer */
                                             

// -----------------------------------------------------------------
//
// Function: SendBcBuffer
//
// Purpose : Sends the BC_HEAD buffer on the broadcast socket
//
// Params  : see below
//
// Returns : 0 Operation was successful. 
//			 < 0:  error
//
// Comments: 
//           
// -----------------------------------------------------------------
static int SendBcBuffer (SOCKET socket, BC_HEAD  *Pbc_head, long buflen, short bc_port_no)  
{             
	int rc = RC_SUCCESS;
	long	remain;
	long	packet_size;
//	long	send_len;
	COMMIF	 *ldata;
	char   * offs;
	HGLOBAL handle;                       
	SOCKERR serr = 0;
	SOCKADDR_IN dummy;
	time_t CurrentTime1=0;
	time_t PrevTime1=0;
	int maxlpcnt=0;
	int i;
	
	ldata 	= (COMMIF  *) DoMalloc((long) (DATAPACKETLEN + sizeof(COMMIF)), &handle);
	offs	= (char  *) Pbc_head;
	remain	= buflen;                             
	
	do {
		memset ((char *) &dummy, 0, sizeof(SOCKADDR_IN));
		dummy.sin_addr.s_addr = (u_long) INADDR_BROADCAST;
		dummy.sin_family = AF_INET;
		dummy.sin_port = htons(bc_port_no);
		if ( remain > DATAPACKETLEN ) {
			ldata->command = htons((u_short)PACKET_DATA);
			ldata->length = htonl((int) DATAPACKETLEN);
			memcpy((char *) &(ldata->data),offs,DATAPACKETLEN);
			remain -= DATAPACKETLEN;
			packet_size = DATAPACKETLEN + sizeof(COMMIF);
			offs+=DATAPACKETLEN;
		} /* end if */
		else 
		{
			if (strncmp(offs,"CI",2) == 0 || strncmp(offs,"Time",4) == 0)
			{
				memcpy((char *) ldata,offs,(int)remain);
				packet_size = remain;
			}
			else
			{
				ldata->length = htonl(remain);
				ldata->command = htons((u_short)PACKET_END);
				memcpy((char *) &(ldata->data),offs,(int)remain);
				packet_size = remain + sizeof(COMMIF);
			}
			remain = 0;
		} /* end else */
//		snap((char *) ldata,32);                   
		rc = sendto (socket, (char *)ldata, (int)packet_size, 0, (SOCKADDR  *) &dummy,
					 sizeof (SOCKADDR_IN));
		Yield();
		sprintf(trace_buffer,"SendBcBuffer: done: %d , len: %ld", socket, packet_size);
		WriteTrace(trace_buffer, WT_DEBUG);    
		if ( rc == SOCKET_ERROR ) 
		{
			serr = WSAGetLastError();
			if (serr == WSAEWOULDBLOCK)
			{
				i = 0;
				time(&PrevTime1);
				rc = RC_MISSING_ANSWER;
				while (rc == RC_MISSING_ANSWER && i < maxlpcnt)
				{
					if (PrevTime1 != time(&CurrentTime1))
					{
						PrevTime1 = CurrentTime1;
						i++;
						Yield();	// let bcserv look for broadcasts
					}
					rc = sendto (socket, (char *)ldata, (int)packet_size, 0,
								 (SOCKADDR  *) &dummy, sizeof (SOCKADDR_IN));
					if ( rc == SOCKET_ERROR ) 
					{
						serr = WSAGetLastError();
						sprintf(trace_buffer,"SendBcBuffer: send (%d): %s", i, 
								SockerrToString( serr ) );
						WriteTrace(trace_buffer, WT_DEBUG);    
						rc = RC_MISSING_ANSWER;
					}
				}
				if (rc == RC_MISSING_ANSWER)
				{
					rc = RC_COMM_FAIL;
				}
			}
			else
			{
				sprintf(trace_buffer,"SendBcBuffer: send: %s", SockerrToString( serr ) );
				WriteTrace(trace_buffer, WT_DEBUG);    
				rc = RC_COMM_FAIL;
			} /* end if */
		} /* end if */
	} while (remain >0 && rc != RC_COMM_FAIL);
	
	DoFree(handle);
    
    if (rc != RC_COMM_FAIL)
    	rc = RC_SUCCESS;
 	return rc;
} /* end SendBcBuffer */


// -----------------------------------------------------------------
//
// Function: SendShutdown
//
// Purpose : Sends a SHUTDOWN command on the socket
//
// Params  : see below
//
// Returns : 0 Operation was successful. 
//			 < 0:  error
//
// Comments: 
//           
// -----------------------------------------------------------------
static int SendShutdown (SOCKET socket)                                             
{             
	int rc = RC_SUCCESS;   
	COMMIF	packet;
	/*COMMIF	ack;*/
	SOCKERR serr = 0;

	packet.command = htons(NET_SHUTDOWN);   
	packet.length = htonl(0);
	rc = send (socket, (char *)&packet, sizeof(COMMIF), 0);
    if ( rc == SOCKET_ERROR ) {
		serr = WSAGetLastError();
		sprintf(trace_buffer,"SendShutdown: send: %s", SockerrToString( serr ) );
		WriteTrace(trace_buffer, WT_ERROR);    
		rc = RC_COMM_FAIL; 
	}

    return rc;
} /* end SendShutdown */



// -----------------------------------------------------------------
//
// Function: RecBuffer
//
// Purpose : Receives BC_HEAD buffer on the (connection) socket
//
// Params  : Socket, address of a BC_HEAD pointer, pointer to len,
//			 pointer to a handle
//
// Returns : 0 Operation was successful. 
//			 < 0:  error
//
// Comments: After returning Pbc_head points to a new allokated 
//			 buffer, buflen contains the length and handle the
//			 GlobalAlloc handle of the buffer
//           
// -----------------------------------------------------------------
static int RecBuffer (SOCKET socket, BC_HEAD  *  *Pbc_head, 
						DWORD *buflen, HGLOBAL *handle)                                             
{             
	int	rc = RC_SUCCESS;			/* Return code			*/
	SOCKERR serr=0;

	COMMIF	 *packet;			/* transport data */
	COMMIF	 *pdata;				/* transport data */
	char * cur_pdata=NULL;			/* transport data pointer */
	COMMIF	ack;
	char  * cur_adr;      
	HGLOBAL packet_hndl, data_hndl;
	DWORD packet_len=0;
	DWORD tmp = 0;
    short ready = FALSE;
    int count=0;
    int cur_len=0;
    int p_len=0;
    

	packet = (COMMIF  *) DoMalloc((long)NETREAD_SIZE, &packet_hndl);
	pdata  = (COMMIF  *) DoMalloc((long)PACKET_LEN, &data_hndl);
                                
	if (packet == NULL || pdata == NULL)
	{
		sprintf(trace_buffer,"RecBuffer: Malloc failed");
		WriteTrace(trace_buffer, WT_ERROR);                          
		rc = RC_INIT_FAIL;	
	}
	packet_len = NETREAD_SIZE;                     
	
	if (rc == RC_SUCCESS)
	{           
		cur_adr = (char  *)(packet->data) ;
		//_fmemset (packet->data, 0x00, NETREAD_SIZE);
		packet ->length = 0;
		ack.command = (short)htons((u_short)PACKET_DATA);
		ack.length = htonl(sizeof(COMMIF));
		while (! ready) {       
			cur_pdata = (char *) pdata;
            cur_len = 0;
            p_len = PACKET_LEN;
			do
			{       
				Yield();
				if ((rc = recv(socket, (char *) cur_pdata, PACKET_LEN - cur_len, 0)) == SOCKET_ERROR ) 
				{
					serr = WSAGetLastError();
					sprintf(trace_buffer,"RecBuffer: recv: %s", SockerrToString( serr ) );
					WriteTrace(trace_buffer, WT_ERROR);
					if (serr == WSAEWOULDBLOCK) /* non blocking mode ??*/
						rc = RC_NOT_FOUND;
					else    
						rc = RC_COMM_FAIL;
					DoFree(packet_hndl); 
					ready = TRUE;
					rc = RC_FAIL;
				} 
				else
				{           
					if (rc == 0)
					{
						// MCU 96.10.11 we should return RC_FAIL if the connection has been closed
						sprintf(trace_buffer,"RecBuffer: recv: %d  connection closed by host",rc);
						rc = RC_FAIL;
					}
					else
					{
						cur_len += rc;
						cur_pdata += rc;
						if (cur_len >= sizeof (COMMIF) )
						{                      /* How many bytes will come ? */
							p_len = (int) ntohl(pdata->length) + sizeof(COMMIF);
						} /* fi */
						/*
						sprintf(trace_buffer,"RecBuffer: recv: %d bytes len=%d plen=%d", 
							rc, cur_len, p_len );
						WriteTrace(trace_buffer, WT_DEBUG);
						*/
						rc = RC_SUCCESS;
					}
				} /* fi */
			} while (rc == RC_SUCCESS && cur_len < PACKET_LEN && cur_len < p_len);		
			
//			snap ((char *) pdata, 72);
                      
            if (rc == RC_SUCCESS)
            {
            	Yield();
				rc = send (socket, (char *) &ack, sizeof(COMMIF), 0);
				pdata->command = ntohs(pdata->command);
				pdata->length =  ntohl(pdata->length);

//				sprintf(trace_buffer,"RecBuffer: cmd=%d len=%ld",pdata->command, pdata->length);
//				WriteTrace(trace_buffer, WT_DEBUG); 
            
				if (rc >= 0)
					rc = RC_SUCCESS; 
			} /* fi */
			                        
			if ( rc == RC_SUCCESS && pdata->command == NET_SHUTDOWN ) 
			{	
				DoFree(packet_hndl); 
				ready = TRUE;
				rc = RC_SHUTDOWN;
				continue;
			} /* end if */
			
			/*
			sprintf(trace_buffer,"RecBuffer:Test auf oRealloc packet->length=%d pdata->length=%d packet_len=%d",
				packet->length,pdata->length,packet_len);
			WriteTrace(trace_buffer, WT_DEBUG);
			*/	
			if ( rc == RC_SUCCESS && (packet->length + pdata->length + 8) > packet_len) 
			{

				tmp = ((char  *) cur_adr - (char  *) packet); 
				packet_len += NETREAD_SIZE;
				packet = (COMMIF  *)  DoRealloc (packet_len, &packet_hndl); 
				if (packet == NULL)
				{
					DoFree(packet_hndl); 
					ready = TRUE;
					rc = RC_FAIL;
					continue;
				} /* fi */ 
				cur_adr = (char  *) packet + tmp;
/*
				sprintf(trace_buffer,"RecBuffer:NACH DoRealloc tmp=%ld,packet_len=%d  cur=%p packet=%p",
					tmp,packet_len,cur_adr,packet);
				WriteTrace(trace_buffer, WT_DEBUG);
			
				TestOnly = cur_adr - 8;
				sprintf(trace_buffer,"RecBuffer:Nach DoRealloc %02x %02x %02x %02x %02x %02x %02x %02x ",
					TestOnly[0],TestOnly[1],TestOnly[2],TestOnly[3],
					TestOnly[4],TestOnly[5],TestOnly[6],TestOnly[7]
					);
				WriteTrace(trace_buffer, WT_DEBUG);
*/
			
			}  /* fi */
			
            if (rc == RC_SUCCESS)
            {
				CCSMemCpy (cur_adr, (char  *) pdata->data, (DWORD) pdata->length);
				packet->length += pdata->length;
				if ( pdata->command == PACKET_DATA ) 
				{
		//			sprintf(trace_buffer,"RecBuffer:PDATA(Len:%ld)",packet->length);
		//			WriteTrace(trace_buffer, WT_DEBUG);
					cur_adr += pdata->length;
		//			sprintf(trace_buffer,"RecBuffer:CurAadr(ADR:%p), length=%ld,pdata->length=%d",
		//					cur_adr,cur_adr-(char *)packet,pdata->length);
		//			WriteTrace(trace_buffer, WT_DEBUG);
				} /* end if */
				if ( pdata->command == PACKET_END) 
				{
					unsigned int ByteCount;

					sprintf(trace_buffer,"RecBuffer:PDATA_END(Len: %ld)",packet->length);
					WriteTrace(trace_buffer, WT_DEBUG);
					//snap (packet->data,packet->length);
				 
					*Pbc_head = (BC_HEAD  *) packet->data;
				
//					for (count=0,cur_adr = (char  *)packet->data; *cur_adr; cur_adr++)
					for (count=0,ByteCount=0,cur_adr = (char  *)packet->data; ByteCount < packet->length; 
								cur_adr++,ByteCount++)
					{
						if (*cur_adr == '\n')
							count++;
						/*
						if (*cur_adr == '\0')
						{
							sprintf(trace_buffer,"Null Byte found at %d",ByteCount);
							WriteTrace(trace_buffer, WT_DEBUG);
						}
						*/
					}
					sprintf(trace_buffer,"RecBuffer:Lines: %d)",count);
					WriteTrace(trace_buffer, WT_DEBUG);
				
					*handle = packet_hndl;
					*buflen	= (DWORD) packet->length;
					ready = TRUE;
					/* SendData(0,NULL,packet); not used here */
				} /* end if */
			} /* end if */
		} /* end while */
		
		DoFree(data_hndl); 
	} /* end if hndl == NULL */
	
	
	
	return rc;
}  /* RecBuffer end */

// -----------------------------------------------------------------
//
// Function: DoMalloc
//
// Purpose : Simple malloc, the handle has to be used for DoFree !
//
// Params  : see below
//
// Returns : Pointer: 	Operation was successful. 
//			 NULL:  	error
//
// Comments: 
//           
// -----------------------------------------------------------------

char * DoMalloc (long buflen, HGLOBAL *Phandle )
{                      
	int rc = RC_SUCCESS;
	char * Pbuf = NULL;

	*Phandle = GlobalAlloc (GHND, (DWORD) buflen);
	if (*Phandle == NULL)
	{
		sprintf(trace_buffer,"DoMalloc:GlobalAlloc failed"); WriteTrace(trace_buffer, WT_ERROR);
	 	rc = RC_INIT_FAIL;
	}
    
    if (rc == RC_SUCCESS)
    {
        Pbuf = (char *) GlobalLock (*Phandle);
		if (Pbuf == NULL)
		{
			sprintf(trace_buffer,"DoMalloc:GlobalLock failed"); WriteTrace(trace_buffer, WT_ERROR);
	 		rc = RC_INIT_FAIL;
		}
    }           
    
    if (rc == RC_SUCCESS)
    {
		sprintf(trace_buffer,"DoMalloc:%ld Bytes done handle = %d",buflen,*Phandle);
		WriteTrace(trace_buffer, WT_DEBUG);
    	return Pbuf;                                               
    }
    else
    {
    	return NULL;
    }  /* end if */
}   /* end DoMalloc */
                          
                          
// -----------------------------------------------------------------
//
// Function: DoRealloc
//
// Purpose : Simple Realloc, the handle has to be used for DoFree !
//
// Params  : New len
//
// Returns : Pointer: 	Operation was successful. 
//			 NULL:  	error
//
// Comments: 
//           
// -----------------------------------------------------------------

char * DoRealloc (DWORD buflen, HGLOBAL *Phandle )
{                      
	int rc = RC_SUCCESS;
	char * Pbuf = NULL;

	GlobalUnlock (*Phandle);
	*Phandle = GlobalReAlloc (*Phandle, (DWORD) buflen, GMEM_ZEROINIT);
	if (*Phandle == NULL)
	{
		sprintf(trace_buffer,"DoRealloc:GlobalAlloc failed"); WriteTrace(trace_buffer, WT_ERROR);
	 	rc = RC_INIT_FAIL;
	}
    
    if (rc == RC_SUCCESS)
    {
        Pbuf = (char *) GlobalLock (*Phandle);
		if (Pbuf == NULL)
		{
			sprintf(trace_buffer,"DoRealloc:GlobalLock failed"); WriteTrace(trace_buffer, WT_ERROR);
	 		rc = RC_INIT_FAIL;
		}
    }           
    
    if (rc == RC_SUCCESS)
    {
		sprintf(trace_buffer,"DoRealloc:%ld Bytes done handle = %d",buflen,*Phandle);
		WriteTrace(trace_buffer, WT_DEBUG);
    	return Pbuf;                                               
    }
    else
    {
    	return NULL;
    }  /* end if */
}   /* end DoRealloc */


// -----------------------------------------------------------------
//
// Function: DoFree
//
// Purpose : Simple free, the handle comes from a DoMalloc call
//
// Params  : see below
//
// Returns : none
//
// Comments: 
//           
// -----------------------------------------------------------------

void DoFree ( HGLOBAL handle)
{
	GlobalUnlock (handle);
	GlobalFree (handle);
	sprintf(trace_buffer,"DoFree:done handle = %d", handle); WriteTrace(trace_buffer, WT_DEBUG);
}


// -----------------------------------------------------------------
//
// Function: snap
//
// Purpose : Hex Dump tool
//
// Params  : Buffer, no of bytes
//
// Returns : none
//
// Comments: 
//           
// -----------------------------------------------------------------
void snap (char * buf, int buflen)
{
 	int i; 
 	char * Pact=buf;
	int t_i=-1;

	t_i = MakeNewMultiStruct();
	if (t_i < 0)
		return ;
			    
 	
    if (multi[t_i].do_trace && multi[t_i].outpt != NULL)
    {
   	  for (i=0; i<buflen; i++)
 	  {
 		if (buflen - i >= 16)
 		{
 			sprintf (trace_buffer, 
 			"%02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x",
            (unsigned char) *(Pact+0),(unsigned char) *(Pact+1),
            (unsigned char) *(Pact+2),(unsigned char) *(Pact+3),
            (unsigned char) *(Pact+4),(unsigned char) *(Pact+5),
            (unsigned char) *(Pact+6),(unsigned char) *(Pact+7),
            (unsigned char) *(Pact+8),(unsigned char) *(Pact+9),
            (unsigned char) *(Pact+10),(unsigned char) *(Pact+11),
            (unsigned char) *(Pact+12),(unsigned char) *(Pact+13),
            (unsigned char) *(Pact+14),(unsigned char) *(Pact+15));
			WriteTrace(trace_buffer, WT_DEBUG);
			trace_buffer[0] = EOS;
            Pact += 16;
            i += 15;
        }
        else
        {
 			sprintf (trace_buffer + strlen (trace_buffer), "%02x ", (unsigned char) *(Pact+0));
 			Pact++;         	
        } 
      }
	  WriteTrace(trace_buffer, WT_DEBUG);
	}
} /* end snap */

void NewSnap (char * buf, int buflen,FILE *prpOut)
{
 	int i; 
 	char * Pact=buf;
	int t_i=-1;

	t_i = MakeNewMultiStruct();
	if (t_i < 0)
		return ;
			    
 	
    if (multi[t_i].do_trace && multi[t_i].outpt != NULL)
    {
   	  for (i=0; i<buflen; i++)
 	  {
 		if (buflen - i >= 16)
 		{
 			sprintf (trace_buffer, 
 			"%02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x",
            (unsigned char) *(Pact+0),(unsigned char) *(Pact+1),
            (unsigned char) *(Pact+2),(unsigned char) *(Pact+3),
            (unsigned char) *(Pact+4),(unsigned char) *(Pact+5),
            (unsigned char) *(Pact+6),(unsigned char) *(Pact+7),
            (unsigned char) *(Pact+8),(unsigned char) *(Pact+9),
            (unsigned char) *(Pact+10),(unsigned char) *(Pact+11),
            (unsigned char) *(Pact+12),(unsigned char) *(Pact+13),
            (unsigned char) *(Pact+14),(unsigned char) *(Pact+15));
			//fprintf(prpOut,"%s\n",trace_buffer);
			trace_buffer[0] = EOS;
            Pact += 16;
            i += 15;
        }
        else
        {
 			sprintf (trace_buffer + strlen (trace_buffer), "%02x ", (unsigned char) *(Pact+0));
 			Pact++;         	
        } 
      }
	//fprintf(prpOut,"%s\n",trace_buffer);
	fflush(prpOut);
	}
} /* end snap */

// -----------------------------------------------------------------
//
// Function: WriteTrace
//
// Purpose : Write the buffer to the trace file, if trace is enabled
//
// Params  : buffer
//
// Returns : none
//                                     
// Comments: 
//           
// -----------------------------------------------------------------

void WriteTrace ( char * buf, int prio)
{                          
/*MWO: PRF because the logfile size > 1GB */
	int t_i=-1;
	char timebuf[20];
	char filext[6];
	int filextnum;
	
	t_i = MakeNewMultiStruct();
	if (t_i < 0)
		return ;
	
	if (multi[t_i].outpt != NULL && prio >= wt_prio)

    /* if (multi[t_i].do_trace && multi[t_i].outpt != NULL && prio >= wt_prio) */
    {
		_strdate (timebuf);   
		timebuf[8] = ' ';
		_strtime (timebuf + 9);
		fprintf (multi[t_i].outpt, "%s: %s\n", timebuf, buf);
		fflush (multi[t_i].outpt);              
		if (traceFileIndex > 0)
		{
			traceCount++;
		}
		if (traceCount > 500)
		{
			fclose (multi[t_i].outpt);	
			multi[t_i].outpt = NULL;
			strcpy(filext,&multi[t_i].TraceFileName[traceFileIndex]);
			filextnum = atoi(filext) + 1;
			if (filextnum > 999)
			{
				filextnum = 0;
			}
			sprintf(&multi[t_i].TraceFileName[traceFileIndex],"%d",filextnum);
			multi[t_i].outpt = (FILE *) fopen (multi[t_i].TraceFileName,"w");
			traceCount = 0;
		}
	}
/* END MWO */
}

void PASCAL ForceTrace ( char * buf)
{
	FILE	*dbg_fd;
	
	dbg_fd = fopen("C:\\Tmp\\Ufisdll.log","a");
	fprintf (dbg_fd, "%s\n", buf);
	fflush (dbg_fd);
	fclose(dbg_fd);
}              


static int DistributeBc(BC_STRUCT *prpBcStruct)
{
#ifdef NONO
	MULTI *prlMulti;
	char *prlDest;
	int ilRequiredSpace;
	int ilFreeSpace;
	BC_STRUCT *prlTmpBc;
	bool blPostSuccess;

		int ilLc;
				
		for (ilLc = 0; ilLc < MAX_NO_OF_APPL; ilLc++)
		{
			if (multi[ilLc].used == TRUE)
			{
				prlMulti = &multi[ilLc];
				if (prlMulti->BcLast == NULL)
				{
					// this ist the first BC which we receive for this application
					prlDest = prlMulti->PendingBCs;
				}
				else
				{
					prlDest = (char *) prlMulti->BcLast->NextBc;
				}
				if (prlMulti->BcNext == NULL)
				{
					prlMulti->BcNext = (BC_STRUCT *)prlDest;
				}
				ilRequiredSpace = sizeof(BC_STRUCT) + prpBcStruct->BcHead.tot_size + 4;

				if ((void *)prlDest < (void *)prlMulti->BcNext)
				{
					// reusing space for already retrieved BCs
					ilFreeSpace = (char *) prlMulti->BcNext - (char *) prlDest;
				}
				else
				{
					ilFreeSpace = (char *) prlMulti->PendingBCs[MAX_BC_BUF_LEN] - (char *) prlDest;
				}
				if (ilFreeSpace > ilRequiredSpace)
				{
					prlTmpBc = (BC_STRUCT *)prlDest;
					memcpy(prlDest,prpBcStruct,sizeof(prpBcStruct));
					prlDest += sizeof(prpBcStruct);
					prlTmpBc->BcData = prlDest;
					memcpy(prlTmpBc->BcData,prpBcStruct->BcData,prpBcStruct->BcHead.tot_size);
					prlDest += prpBcStruct->BcHead.tot_size;
					prlTmpBc->NextBc = (BC_STRUCT *)prlDest;
					prlMulti->BcLast = prlTmpBc;
				}
				if (multi[ilLc].BcWindow != 0)
				{
					if (IsWindow(multi[ilLc].BcWindow))
					{
						PostMessage(multi[ilLc].BcWindow,WM_USER+16 /* WM_BCADD */,0,0L);
					}
					else {
						
						/* The window does not exist, remove from the multi array */
	sprintf(trace_buffer,"DistributeBC: Dead connection %d, Usage count %d",ilLc,gcUsage); 
	ForceTrace(trace_buffer);
						multi[ilLc].used = FALSE;
						UnRegisterTask();
					}
					
				}
			}
		}
	//	return rc+bcb_no;
#endif
}


BC_STRUCT *CreateBcEntry(BC_HEAD *prpBcHead)
{
	BC_STRUCT *prlBc = calloc(1,sizeof(BC_STRUCT));
	if (prlBc != NULL)
	{
		prlBc->BcData = calloc(prpBcHead->tot_size,sizeof(char));
		if (prlBc->BcData == NULL)
		{
			free(prlBc);
			prlBc = NULL;
		}
	}
	return prlBc;

}


// -----------------------------------------------------------------
//
// Function: BcBufAdd
//
// Purpose : Adds an entry to the broadcast buffer
//
// Params  : entry
//
// Returns : RC_SUCCESS
//			 RC_FAIL		- Malloc failed
//			 RC_NOT_FOUND 	- Buffer full
//
// Comments: 
//           
// -----------------------------------------------------------------

int  PASCAL BcBufAdd (short spBcNum)
{
	int rc=RC_SUCCESS;
	int ilLc;

	Yield();
	if (in_bcbuf == TRUE)
		return RC_DATA_CUT;

	in_bcbuf = TRUE;
	
	for (ilLc = 0; ilLc < MAX_NO_OF_APPL; ilLc++)
	{
		if (multi[ilLc].used == TRUE)
		{
			multi[ilLc].ActBcNum = spBcNum;
			if (multi[ilLc].BcFirst <= 0)
			{
				multi[ilLc].BcFirst = spBcNum-1;
			}
			multi[ilLc].BcLast = spBcNum;
			if (multi[ilLc].BcWindow != 0)
			{
				
				if (IsWindow(multi[ilLc].BcWindow))
					{
						PostMessage(multi[ilLc].BcWindow,WM_USER+16 /* WM_BCADD */,spBcNum,0L);
						sprintf(trace_buffer,"BcBufAdd: ApplNo: %d, BcFirst = %d,  BcLast = %d",
						ilLc,multi[ilLc].BcFirst,multi[ilLc].BcLast);
						WriteTrace(trace_buffer, WT_DEBUG);
					}
					else
					{
						
						/* The window does not exist, remove from the multi array */
	sprintf(trace_buffer,"BcBufAdd: Dead connection %d, Usage count %d",ilLc,gcUsage); 
	ForceTrace(trace_buffer);
						multi[ilLc].used = FALSE;
						UnRegisterTask();
					}
			}
		}
	} 
    in_bcbuf = FALSE;
	return rc;
} /* end BcBufAdd */                        

int  PASCAL AddToBcLostList(short spBcNum,BOOL bpAdd)
{
	int rc=RC_SUCCESS;
	short slLost_tag = 0;

	if (bpAdd)
	{
		slLost_tag = BC_LOST_TAG;
	}

	if(spBcNum >= 0 && spBcNum <= MAXBCNUM)
	{
		psgLostBcs[spBcNum] = slLost_tag;
	}

	return RC_SUCCESS;
} /* end BcBufAdd */                        

int  PASCAL GetLostBcCount()
{
	int ilLc;
	int ilLostBcCount = 0;
	int ilCorruptCount = 0;
	BOOL blCorrupt_Array = FALSE;

	for(ilLc = 0; ilLc <= MAXBCNUM; ilLc++)
	{
		if (psgLostBcs[ilLc] == BC_LOST_TAG)
		{
			ilLostBcCount++;
		}
		else if (psgLostBcs[ilLc] != 0)
		{
			psgLostBcs[ilLc] = 0;
			ilCorruptCount++;
			blCorrupt_Array = TRUE;
		}
	}

	if (blCorrupt_Array)
	{
		sprintf(trace_buffer,"Corrupt BC Lost array, count =%d",ilCorruptCount);
		WriteTrace(trace_buffer, WT_ERROR);   
	}
	
	return ilLostBcCount;
} /* end GetLostBcList */                        


int  PASCAL GetLostBcList(char **pcpLostBcList)
{
	int rc=RC_SUCCESS;
	int ilLc,ilLastChar;
	int ilLostBcCount = 0;
	char *pclLostBcList = NULL;
	char pclBcNumBuf[24];

	for(ilLc = 0; ilLc <= MAXBCNUM; ilLc++)
	{
		if (psgLostBcs[ilLc] == BC_LOST_TAG)
		{
			ilLostBcCount++;
		}
	}
	pclLostBcList = realloc(*pcpLostBcList,(ilLostBcCount+100)*6);
	*pclLostBcList = '\0';
	if (pclLostBcList != NULL)
	{
		for(ilLc = 0; ilLc <= MAXBCNUM; ilLc++)
		{
			if (psgLostBcs[ilLc] == BC_LOST_TAG)
			{
				sprintf(pclBcNumBuf,"%d,",ilLc);
				strcat(pclLostBcList,pclBcNumBuf);
			}
		}
		ilLastChar = strlen(pclLostBcList)-1;
		if(ilLastChar >= 0 && pclLostBcList[ilLastChar] == ',')
		{
			pclLostBcList[ilLastChar] = '\0';
		}
		
		*pcpLostBcList = pclLostBcList;
	}
	
	return rc;
} /* end GetLostBcList */                        


// -----------------------------------------------------------------
//
// Function: BcBufGet
//
// Purpose : Gets an entry out of the broadcast buffer
//
// Params  : Pentry
//
// Returns : RC_SUCCESS
//			 RC_NOT_FOUND 	- Buffer empty
//
// Comments: 
//           
// -----------------------------------------------------------------

static int BcBufGet (char * Pentry,int ipMultiIndex)
{
	int rc=RC_SUCCESS;
	char pclFileName[500];

	sprintf(pclFileName, "%s\\BCDATA.DAT", pcmTmpPath);
	
	if (in_bcbuf == TRUE)
		return RC_NOT_FOUND;

    if (/*(Pbc_buffer == NULL) || */
		(multi[ipMultiIndex].BcFirst == multi[ipMultiIndex].BcLast) ||
		(multi[ipMultiIndex].BcLast == -1))
    {
    	rc = RC_NOT_FOUND;
    } /* end if */
    
    if (rc == RC_SUCCESS)
    {  
		multi[ipMultiIndex].BcFirst = (multi[ipMultiIndex].BcFirst + 1) %  MAXBCNUM;
    //	memcpy(Pentry, Pbc_buffer + (multi[ipMultiIndex].BcFirst * PACKET_LEN), PACKET_LEN);
	//  now read BC from file, not from internal buffer
		if (!prgBcDataFile)
		{
			prgBcDataFile = (FILE *) fopen (pclFileName,"rb");
		}	

		if (prgBcDataFile)
		{
			fseek(prgBcDataFile,FIRSTBC+(multi[ipMultiIndex].BcFirst*PACKET_LEN),SEEK_SET);
			fread(Pentry,PACKET_LEN,1,prgBcDataFile);
		}
		else
		{
	    	rc = RC_NOT_FOUND;
		}
	//	sprintf(trace_buffer,"BcBufGet: rc = %d bcb_first = %d  bcb_no = %d",rc, bcb_first, bcb_no);
	//	WriteTrace(trace_buffer, WT_DEBUG);
    } /* end if */
	return rc;
} /* end BcBufGet */


#ifdef START_BCSRV
// -----------------------------------------------------------------
//
// EnumWindowsCallback()
//
// Purpose:  Call back function for EnumWindows(), looks for the window
//           of the hidden application.
//
// Parameters: HWND - Window to check
//             LONG - Instance handle of the hidden application
//
// Return Value:  1 indicating DLL initialization is successful.
//
// Comments:   LibMain is called by Windows.  Do not call it in your
//             application!
// -----------------------------------------------------------------

BOOL  PASCAL EnumWindowsCallback(HWND hWnd, LONG lParam)
{
	long ilCharCount;
	char pclClassName[24];

	/*
	ilTmpLong = GetWindowLong(hWnd,GWL_HINSTANCE);
    if((WORD)lParam == GetWindowLong(hWnd,GWL_HINSTANCE))
      {
      gHidhWnd = hWnd;
      return FALSE;
      }
	  */
	Yield();
	ilCharCount = GetClassName(hWnd,pclClassName,sizeof(pclClassName)-1);
	if (GetClassName(hWnd,pclClassName,sizeof(pclClassName)-1) > 0)
	{
		if (strcmp(pclClassName,"BCSERVCLASS") == 0)
		{
 		  gHidhWnd = hWnd;
		  return FALSE;
		}
	}

   return TRUE;
}

// -----------------------------------------------------------------
//
// RegisterTask ()
// Purpose:  This is called when a new task links to the DLL
//
// Parameters: NONE
//
// Returns: NONE
//
// -----------------------------------------------------------------

int  PASCAL RegisterTask(char *pcpAddr)
{           
   char buf[50];
   int rc=RC_SUCCESS;
   	
   // increment the usage variable.  If this is the first time, then
   // start the hidden app 
   if(!(gcUsage++))
   {
      FARPROC EWProc;          // EnumWindows callback procedure
	  STARTUPINFO rlStartUpInfo;
	  PROCESS_INFORMATION rlProcessInformation;
	  BOOL blRc;

	rlStartUpInfo.cb = sizeof(rlStartUpInfo); 
  	rlStartUpInfo.lpReserved = NULL;
	rlStartUpInfo.lpDesktop = NULL;
  	rlStartUpInfo.lpTitle = NULL;
	rlStartUpInfo.dwX = 0;
  	rlStartUpInfo.dwY = 0;
	rlStartUpInfo.dwXSize = 0;
	rlStartUpInfo.dwYSize = 0;
	rlStartUpInfo.dwXCountChars = 0;
  	rlStartUpInfo.dwYCountChars = 0;
	rlStartUpInfo.dwFillAttribute = 0;
  	rlStartUpInfo.dwFlags = STARTF_USESHOWWINDOW;
	rlStartUpInfo.wShowWindow = SW_SHOW;
  	rlStartUpInfo.cbReserved2 = 0;
	rlStartUpInfo.lpReserved2 = NULL;
  	rlStartUpInfo.hStdInput = 0;
	rlStartUpInfo.hStdOutput = 0;
  	rlStartUpInfo.hStdError = 0;

      // Initialize the DLL here

      // start up the hidden application BCSERV
      sprintf (buf, "bcserv32.exe %s",pcpAddr);
     // hHidInst = WinExec((LPCSTR) buf,SW_HIDE);
	  
		blRc =	CreateProcess(NULL,buf,
					NULL,NULL,FALSE,
					DETACHED_PROCESS,
					NULL,NULL,&rlStartUpInfo,
					&rlProcessInformation);

      if (blRc == FALSE)
      {
		DWORD llError;
		
		llError = GetLastError();

		sprintf(trace_buffer,"RegisterTask:Start of bcserv failed reason=%ld",llError);
		WriteTrace(trace_buffer, WT_ERROR);   
		rc = RC_FAIL;
	  } /* fi */     
      else
      {
      	// Find the hWnd associated with this instance
		Sleep(1000);
      //	EWProc = MakeProcInstance((FARPROC)EnumWindowsCallback,ghDLLInst);
      	//EnumWindows(EWProc,(LONG)hHidInst);
		//EnumWindows(EWProc,(LONG)rlProcessInformation.hProcess);
		EnumWindows(EnumWindowsCallback,(LONG)rlProcessInformation.hProcess); 
      	FreeProcInstance(EWProc);
      }

   }         
   
   return rc;
}  /* RegisterTask */


// -----------------------------------------------------------------
//
// UnRegisterTask ()
// Purpose:  This is called when a task shuts down
//
// Parameters: NONE
//
// Returns: NONE
//
// -----------------------------------------------------------------

void  PASCAL UnRegisterTask()
{
   // Decrement usage, if last task is shutting down, then
   // shut down the hidden app.
   if((--gcUsage) < 1)
   {
      	// Kill the hidden app
      	(void) SendMessage(gHidhWnd,(unsigned int)UFIS_QUIT,(WPARAM)NULL,(LPARAM)NULL);
 
 		// Free buffers
		/* No dynamic buffers in 32-bit DLL
 		if (bc_handle != NULL)
		{
			DoFree (bc_handle);
			bc_handle = NULL;
		}
		*/
   } /* fi */
}  /* UnRegisterTask */

#endif


// -----------------------------------------------------------------
//
// Function: InitMultiStruct
//
// Purpose : Inits the given MULTI struct
//
// Params  : Pmulti
//
// Returns : void
//
// Comments: 
//           
// -----------------------------------------------------------------

static void InitMultiStruct (MULTI *Pmulti, DWORD task)
{                   
	int i=0;
	
	Pmulti->used = TRUE;
	Pmulti->task = task;
	Pmulti->gl_conn_socket = INVALID_SOCKET;
	Pmulti->gl_conn_DDC_socket = INVALID_SOCKET;
	Pmulti->gl_bc_socket = INVALID_SOCKET;
	for (i=0; i < NO_OF_CONNECTIONS; i++)
	{
		Pmulti->gl_conn_gvc[i] = INVALID_SOCKET;
	} /* for */
	Pmulti->InCallCeda = FALSE;
	Pmulti->InGetBc = FALSE;
	Pmulti->ActBcNum = 0;
	Pmulti->FirstBcNum = 0;
	Pmulti->BcFirst = -1;
//	Pmulti->BcLast = -1;
	Pmulti->BcWindow = 0;
	Pmulti->IsInit = FALSE;
	strcpy ((char *) (Pmulti->TraceFileName), (char *) DEFAULT_TRACE_FN);
	Pmulti->do_trace = FALSE;
	Pmulti->ceda_host_name[0] = EOS;
	Pmulti->cc_handle = NULL;
	Pmulti->IsNonBlock = TRUE;
	Pmulti->data_buf_size = DEFAULT_DATA_BUF_SIZE;
	for (i=0; i < NO_OF_DATA_BUFFERS; i++)
	{
		Pmulti->buf_handle[i] = NULL;
		Pmulti->buf_len[i] = 0; 
	} /* for */
} /* InitMultiStruct */      


  
// -----------------------------------------------------------------
//
// Function: SearchMultiStruct
//
// Purpose : Searches the given task handle in the global multi array
//
// Params  : Task handle
//
// Returns : RC_NOT_FOUND or the array index
//
// Comments: 
//           
// -----------------------------------------------------------------

static int SearchMultiStruct (DWORD htask)
{   
	int rc = RC_NOT_FOUND;
	int i = 0;
	int ready = FALSE;
	
	for (i=0; i < MAX_NO_OF_APPL && ! ready; i++)
	{
		if (multi[i].used == TRUE && multi[i].task == htask)
			ready = TRUE;
	} /* for */
			
	if (ready == TRUE)
		rc = i-1;

	return rc;
} /* SearchMultiStruct */              

// -----------------------------------------------------------------
//
// Function: SearchNewMultiStruct
//
// Purpose : Searches a free entry in the global multi array
//
// Params  : void
//
// Returns : RC_NOT_FOUND or the array index
//
// Comments: 
//           
// -----------------------------------------------------------------

static int SearchNewMultiStruct (void)
{   
	int rc = RC_NOT_FOUND;
	int i = 0;
	
	for (i=0; i < MAX_NO_OF_APPL && multi[i].used == TRUE; i++)
		;	
	if (i < MAX_NO_OF_APPL)
		rc = i;

	return rc;
} /* SearchNewMultiStruct */


// -----------------------------------------------------------------
//
// Function: MakeNewMultiStruct
//
// Purpose : Searches the actual task handle in the global multi array
//			 If it's not found, a new entry will be created
//			 If there is no free slot in the array, NOT FOUND is returned
//
// Params  : void
//
// Returns : RC_NOT_FOUND or the array index
//
// Comments: 
//           
// -----------------------------------------------------------------

int MakeNewMultiStruct (void)
{   
	int rc = RC_NOT_FOUND;
	int i = 0;
    HTASK task=NULL;
    DWORD taskId;
 
  //  task = GetCurrentProcess();
	taskId = GetCurrentProcessId();
	rc = SearchMultiStruct(taskId);
	if (rc == RC_NOT_FOUND)    
	{
		rc = SearchNewMultiStruct();
		if (rc != RC_NOT_FOUND)    
        	InitMultiStruct (&(multi[rc]), GetCurrentProcessId());
    }
    
	return rc;
} /* MakeNewMultiStruct */


// -----------------------------------------------------------------
//
// Function: make_msg
//
// Purpose : Converts Message number and data String to full Message
//
// Params  : Number, Data, Result string
//
// Returns : RC_SUCCESS, RC_NOT_FOUND, RC_FAIL
//
// Comments: 
//           
// -----------------------------------------------------------------

int make_msg  (long msg_no, char * Pdat, char * Pres)
{
	int rc = RC_SUCCESS;             
              
    // Something to do          
              
	return rc;
} /* make_msg */

// -----------------------------------------------------------------
//
// Function: CCSMemCpy
//
// Purpose : Copies data for  buffers
//
// Params  : like memcpy
//
// Returns : void
//
// Comments: When moving bytes in the same buffer (overlapping), the
//			 "copy direction" (first byte first or last byte first)
//			 is selected automatically !!
//           
// -----------------------------------------------------------------

void CCSMemCpy  (char  *Pdest, char  *Psrc, DWORD cnt)
{            
	DWORD i=0;
	              
//	sprintf(trace_buffer,"CCSMemCpy:cnt=%ld dest=%p src=%p",cnt, Pdest, Psrc);
//	WriteTrace(trace_buffer, WT_DEBUG);   

	if (Pdest > Psrc)
	{   
		Pdest += (cnt - 1);
		Psrc  += (cnt - 1);
		for (i=0; i < cnt; i++, Pdest--, Psrc--)
			*Pdest = *Psrc;
	}
	else
	{
		for (i=0; i < cnt; i++, Pdest++, Psrc++)
			*Pdest = *Psrc;
	}
} /* CCSMemCpy */


// -----------------------------------------------------------------
//
// Function: CCSStrLen
//
// Purpose : Returns length of  buffers
//
// Params  : like strcpy
//
// Returns : void
//
//           
// -----------------------------------------------------------------

DWORD CCSStrLen  (char  *Pbuf)
{            
	DWORD i=0;
	              
	while (Pbuf[i] != EOS)
		i++;
		
//	sprintf(trace_buffer,"CCSStrLen:ret=%ld",i);
//	WriteTrace(trace_buffer, WT_DEBUG);   
    
    return i;
} /* CCSMemCpy */


//------------------------------------------------------------------
//
//	get actual number of broadcast buffers
//
//------------------------------------------------------------------

int getnobcs (int *first, int *no)
{
	*first = 0;
//	*no = bcb_nextfree;
	
	return 0;
}


// -----------------------------------------------------------------
//
// Function: do_filter
//
// Purpose : Filters received packets
//
// Params  : Packet
//
// Returns : RC_SUCCESS		OK, save the packet
//			 RC_NOT_FOUND	Ignore
//
// Comments: 
//           
// -----------------------------------------------------------------
//int PASCAL d (char * Pdata)
int FAR PASCAL do_filter(LPSTR Pdata,char *pcpTrace)
{
   	int rc=RC_SUCCESS;
   	COMMIF  *Packet = NULL;				
   	BC_HEAD  *Pbc_head = NULL;		
	char pclOrigName[100]="";
	char pclHostName[100]="";

//	if (prpOut != NULL)
//		NewSnap ((LPSTR)Pdata, 32,prpOut);

	Packet = (COMMIF  *) Pdata;
	Pbc_head = (BC_HEAD  *) Packet->data;
	strcpy(pclOrigName, Pbc_head->orig_name);
	strcpy(pclHostName, (char *) nam_host_name);


	strupr(pclHostName); 
	strupr(pclOrigName);

	if (pcpTrace != NULL)
	{
		char pclTmp[124];

		sprintf(pcpTrace,"do_filter: BCHEAD->orig_name <%s> nam_host_name <%s>\n",Pbc_head->orig_name,(char *)nam_host_name);
		sprintf(pclTmp,"do_filter: HostName <%s> OrigName<%s>",pclHostName,pclOrigName);
		strcat(pcpTrace,pclTmp);
	}

	if (strcmp (pclOrigName, pclHostName) == 0) 
	{
    	rc = RC_SUCCESS;
    }
    else
    {
    	rc = RC_NOT_FOUND;
		strcpy(Pbc_head->orig_name,nam_host_name);
   }

    return rc;
} /* do_filter */


//------------------------------------------------------------------
//
//	display text
//
//------------------------------------------------------------------

int  PASCAL SendText (char *text)
{
	sprintf(trace_buffer,"%s",text);
	WriteTrace(trace_buffer, WT_DEBUG);   
	
	return 0;
}


#ifdef START_BCSRV
// -----------------------------------------------------------------
//
// StartBCSERV ()
// Purpose:  This is called to start BCSERV again
//
// Parameters: NONE
//
// Returns: NONE
//
// -----------------------------------------------------------------

int  PASCAL StartBCSERV(void)
{           
   char buf[50];
   int rc=RC_SUCCESS;
   	
      unsigned int hHidInst;         // Instance handle of hidden app
      FARPROC EWProc;          // EnumWindows callback procedure

      // Initialize the DLL here

      // start up the hidden application BCSERV
      sprintf (buf, "bcserv32.exe 42");
      hHidInst = WinExec((LPCSTR) buf,SW_HIDE);
      if (hHidInst < 32)
      {
		sprintf(trace_buffer,"StartBCSERV: Start of bcserv failed rc=%d",hHidInst);
		WriteTrace(trace_buffer, WT_ERROR);   
		rc = RC_FAIL;
	  } /* fi */     
      else
      {
      	// Find the hWnd associated with this instance

      //	EWProc = MakeProcInstance((FARPROC)EnumWindowsCallback,ghDLLInst);
      	EnumWindows(EnumWindowsCallback,(LONG)hHidInst);
      	FreeProcInstance(EWProc);
		sprintf(trace_buffer,"StartBCSERV: Start of bcserv done");
		WriteTrace(trace_buffer, WT_DEBUG);   
      }
   
   return rc;
}  /* StartBCSERV */


// -----------------------------------------------------------------
//
// StopBCSERV ()
// Purpose:  This is called to stop BCSERV
//
// Parameters: NONE
//
// Returns: NONE
//
// -----------------------------------------------------------------

int  PASCAL StopBCSERV(void)
{
   int rc=RC_SUCCESS;
   	
      	// Kill the hidden app
      	(void) SendMessage(gHidhWnd,(unsigned int)UFIS_QUIT,(WPARAM)NULL,(LPARAM)NULL);
		sprintf(trace_buffer,"StopBCSERV: Stop of bcserv done");
		WriteTrace(trace_buffer, WT_DEBUG);   
 
 		// Free buffers
		/*
 		if (bc_handle != NULL)
		{
			DoFree (bc_handle);
			bc_handle = NULL;
		}
		*/
	
	return rc;
}  /* Stop BCSERV */

#endif

int PASCAL GetActualBcNum (void)
{
	int t_i;

	t_i = MakeNewMultiStruct();
	if (t_i < 0)
		return RC_NOT_FOUND;
			                   
		                      
	/* Test, if InitComm has been called */		                      
	if (multi[t_i].IsInit == FALSE)
	{
		return RC_INIT_FAIL;
	}


	return multi[t_i].ActBcNum;

}

int PASCAL GetFirstBcNum (void)
{
	int t_i;

	t_i = MakeNewMultiStruct();
	if (t_i < 0)
		return RC_NOT_FOUND;
			                   
		                      
	/* Test, if InitComm has been called */		                      
	if (multi[t_i].IsInit == FALSE)
	{
		return RC_INIT_FAIL;
	}


	return multi[t_i].ActBcNum;
}

