

#include <winsock.h>


#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>


//
//  Private constants.
//
#define DESIRED_WINSOCK_VERSION  0x0101  // we'd like winsock ver 1.1...
#define MINIMUM_WINSOCK_VERSION  0x0001  // ...but we'll take ver 1.0

#ifndef RC_SUCCESS
#define RC_SUCCESS 0 
#endif
#ifndef RC_FAIL
#define RC_FAIL    		(-1)
#endif
#ifndef RC_COMM_FAIL
#define RC_EMPTY_MSG	(1)
#define RC_COMM_FAIL    (-2)
#define RC_INIT_FAIL    (-3)
#define RC_CEDA_FAIL    (-4)
#define RC_SHUTDOWN     (-5)
#define RC_ALREADY_INIT	(-6)
#define RC_NOT_FOUND	(-7)
#define RC_DATA_CUT		(-8)
#define RC_TIMEOUT		(-9)
#define RC_ALREADY_CONNECTED	(-10)
#define RC_MISSING_ANSWER		(-11)
#define RC_BUSY			(-31)
#define RC_TO_MANY_APP  (-32)
#endif
                      
                      
#define EOS '\0'

//
//  Some useful types missing from 16-bit Windows.
//

#ifndef WIN32

typedef char            CHAR;
typedef int             INT;
typedef unsigned long   ULONG;
typedef ULONG         * PULONG;
typedef ULONG         * LPULONG;

#endif  // !WIN32


//
//  Socket-specific types.
//

typedef INT             SOCKERR;        // A socket error code.
typedef WORD            PORT;           // A socket port number.
typedef WORD            SOCKEVENT;      // An asynchronous socket event.



// COMMIF Struct

#define		NET_SHUTDOWN	(0x1)
#define		NET_DATA		(0x2)
#define		PACKET_DATA		(-100)
#define		PACKET_START	(-200)
#define		PACKET_END		(-300)

/* Nmae server commands */
#define NAMSRV_REQ_CMD		"NREQ"
#define NAMSRV_INFO_CMD		"NINF"
#define NAMSRV_SHUT_CMD		"SHUT"

// Searched type
#define NAMSRV_TYPE_CEDA	"CEDA"


/* 
 *
 * Following the Broadcast Header  BC_HEAD 
 *
 */
typedef struct {
	char		dest_name[10];		// user login name
	char		orig_name[10];		// filter (BC) or timeout for netin
	char		recv_name[10];		// WKS-Name
	short		bc_num;				// serial number of broadcast 1-30000 
	char		seq_id[10];			// WKS-Name (obsolete)
	short		tot_buf;			// Count BC-Parts
	short		act_buf;			// actual BC PartNo
	char		ref_seq_id[10];		// not used
	short		rc;					// return code, mostly useless
	short		tot_size;			// total bytes of BC (data only)
	short		cmd_size;			// bytes of this part
	short		data_size;			// not used
	char		data[1];			// data, cmdblk
}BC_HEAD;

/* 
 *
 * Following the Broadcast Buffer from dZine Controller
 *
 */
typedef struct {
	char				ident[2];
	unsigned short		bc_num;
	unsigned char		address[4];
	unsigned char		time[6];
	char				reserved[16];
}BC_FROM_DZINE;

/* 
 *
 * Following the Broadcast Buffer to dZine Controller (for connection)
 *
 */
typedef struct {
	char				ident[2];
	unsigned char		cont_address[4];
	unsigned char		ddc_address[4];
	unsigned short		tcpport;
	unsigned short		changenetworkpart;
	char				reserved[16];
}BC_TO_DZINE;

/* 
 *
 * Following the Broadcast Buffer to dZine Controller (for time sychronization)
 *
 */
typedef struct {
	char				ident[4];
	unsigned char		dztime[6];
	char				reserved[16];
}BC_TO_DZINE_TIME;

/* 
 *
 * Following the Command Block  CMDBLK
 *
 */
typedef struct {
	char		command[6];			// router command
	char		obj_name[33];		// object name (table name)
	char		order[2];			// not used
	char		tw_start[33];		// used for application data
	char		tw_end[33];			// hopo,tabend,modul name,modul version
 	char		data[1];			// data (selection,fields,data)
}CMDBLK;
          
typedef	struct {
		short	command;			//
		DWORD	length;				// 
		char	data[1];			//
}COMMIF;


//
//  Utility functions.
//


LPSTR SockerrToString( SOCKERR serr );

SOCKERR ResetSocket( SOCKET sock );

SOCKERR CreateSocket( SOCKET  * psock,
                      int          type,
					  LPSTR		   service,
					  short		   non_blocking );
SOCKERR CreateSocketBind( SOCKET  * psock,
                      	  int          type,
					  	  LPSTR		   service,
					  	  short		   non_blocking,
					  	  short		   port_no );
SOCKERR ListenSocket( SOCKET 	   socket,
                      int          backlog );
SOCKERR AcceptSocket( SOCKET  * psock, 
					  SOCKET 	   socket );

int InitWinsock  (void);
int OpenConnection (SOCKET socket, LPSTR hostname, short port_no);
int CleanupWinsock (void);
int ForwardData( SOCKET sock, LPSTR hostname, short port_no, LPSTR data, 
						int length);
int ReceiveData( SOCKET sock, LPSTR packet, int  *length);
int SetNonBlocking (SOCKET socket, short non_blocking);
void bchead_hton  (BC_HEAD  *Pbc_head);
void bchead_ntoh  (BC_HEAD  *Pbc_head);
int  pascal GetWorkstationName (LPSTR ws_name);
int  pascal GetRealWorkstationName (LPSTR ws_name);
int GetCedaHost (LPSTR Phostname)  ;

