// AddEntryDlg.cpp : implementation file
//
//
// AddEntry is part of the CoCo project
//
// AddEntry is a little dialog class that receives user input (application name, country code,
//	a textstring, a text ID and a string ID) to be processed by the CoCo tool.
//
//	Version 1.0 on 04.11.1999 by WES
//
//


#include <stdafx.h>
#include <CoCo3.h>
#include <AddEntryDlg.h>
#include <CedaBasicData.h>
#include <CCSGlobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// AddEntryDlg dialog


AddEntryDlg::AddEntryDlg(CWnd* pParent /*=NULL*/)
	: CDialog(AddEntryDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(AddEntryDlg)
	m_NewTextID = _T("");
	m_NewStringID = _T("");
	m_NewString = _T("");
	m_NewApplication = _T("");
	m_NewCoCo = _T("");
	m_NewStatus = -1;
	//}}AFX_DATA_INIT
}


void AddEntryDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(AddEntryDlg)
	DDX_Control(pDX, IDC_COMBO4, m_EditApp);
	DDX_Control(pDX, IDC_COMBO6, m_EditStatus);
	DDX_Control(pDX, IDC_COMBO5, m_EditCoCo);
	DDX_Control(pDX, IDC_EDIT7, m_EditStrID);
	DDX_Control(pDX, IDC_EDIT6, m_EditText);
	DDX_Control(pDX, IDC_EDIT5, m_EditString);
	DDX_Text(pDX, IDC_EDIT6, m_NewTextID);
	DDX_Text(pDX, IDC_EDIT7, m_NewStringID);
	DDX_Text(pDX, IDC_EDIT5, m_NewString);
	DDX_CBString(pDX, IDC_COMBO4, m_NewApplication);
	DDX_CBString(pDX, IDC_COMBO5, m_NewCoCo);
	DDX_CBIndex(pDX, IDC_COMBO6, m_NewStatus);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(AddEntryDlg, CDialog)
	//{{AFX_MSG_MAP(AddEntryDlg)
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

bool editentry;

/////////////////////////////////////////////////////////////////////////////
// AddEntryDlg message handlers

void AddEntryDlg::OnOK() 
{
	int dataValid = 1;

	UpdateData(TRUE);
	// check some fields
	if (m_NewApplication == "") 
	{
		dataValid=0; 
		MessageBox("Please specify an application name.", "validation test", MB_OK | MB_ICONEXCLAMATION); 
	}

	if ((m_NewCoCo == "") && dataValid)
	{
		dataValid=0; 
		MessageBox("Please specify an application language.", "validation test", MB_OK | MB_ICONEXCLAMATION); 
	}

	if ( dataValid && (m_NewStringID == "") && (m_NewTextID == "") )
	{
		dataValid=0; 
		MessageBox("Please specify either a stringID or a textID.", "validation test", MB_OK | MB_ICONEXCLAMATION); 
	}
	//  Check constraints of TXT-Table
	if ( dataValid )
	{
		if ( editentry )
			dataValid = IsChangedRecordAllowed ( m_NewApplication, m_NewCoCo, 
										m_NewStringID, m_NewTextID, globUrno );
		else
			dataValid = IsNewRecordAllowed ( m_NewApplication, m_NewCoCo, 
											 m_NewStringID, m_NewTextID );
	}

	// trivial checks are done, so test for duplicate entries
	//int k = IDNO;
	if (dataValid && !editentry)
	{
		if ( theApp.UseExistingDuplicateString ( this, m_NewString, m_NewCoCo, 
												 m_NewApplication ) )
		{
			OnCancel();
			return;
		}
		/*
		// find out if string already exists
		int i=globStrList.GetSize()-1;
		while ((i>0) && (globStrList[i] != m_NewString)) { i--; }				
		if (i>0) 
		{
			// find position of duplicate string unro
			CString myurno=globUrnoList[i];
			CString newurno;
			i=myglobGrid->GetRowCount();
			while ((i>0) && (strcmp((newurno=myglobGrid->GetValueRowCol(i,1)),myurno) != 0)) i--;
			
			if (i>=0) 
			{
				dataValid=0;
				myglobGrid->SetSelection(0, i,1,i,7);
				myglobGrid->SelectRange(CGXRange(i,1,i,7),true,true);				// SelectRange does not scroll the grid so scroll to this position manually
				myglobGrid->ScrollCellInView(i, i);						// make it visible
				// get APPL
				CString myappl=myglobGrid->GetValueRowCol(i,2);
				if (strcmp(myappl,"GENERAL") == 0)
				{
					MessageBox("Another entry with the same contents already exists. Please use this one instead.", "information", MB_OK | MB_ICONINFORMATION);
				}
				else 
				{
					k=MessageBox("Another entry with the same contents already exists. Do you want to use the already existing entry (recommended)?", "please confirm:", MB_YESNOCANCEL | MB_DEFBUTTON1 | MB_ICONQUESTION);
					if (k == IDCANCEL) 
					{ 
						OnCancel(); 
					}
					else if (k == IDYES)			// only set field APPL of the already existing record to "GENERAL"
					{
						ogBCD.SetField("TXT", "URNO", newurno, "APPL", "GENERAL",AutoCommitChanges);
						myglobGrid->SetValueRange(CGXRange(i,2),"GENERAL");
						isModified=true;
						OnCancel();
					}
				}

			}
		}*/
	}
	if (dataValid) 
		CDialog::OnOK();
}

void AddEntryDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	/* taken from main dialog - wes */
	CString s;
	int i;


	if (globAppList.GetSize() >0 ) 
	{
		m_EditApp.EnableWindow(true);
		for (int t=0; t<(globAppList.GetSize()); t++)		// add all entries to the selectable application list
		{
			m_EditApp.AddString(globAppList.GetAt(t));
		}
		i=m_EditApp.FindStringExact(0,globfilterapp); m_EditApp.SetCurSel(i);
	}
	else
	{
		m_EditApp.EnableWindow(false);
		m_EditApp.AddString(globfilterapp);
		m_EditApp.SetCurSel(0);
	}

	if (globCoCoList.GetSize() > 0) 
	{
		m_EditCoCo.EnableWindow(true);	
		for (int t=0; t<(globCoCoList.GetSize()); t++)		// add all entries to the selectable coco list
		{
			m_EditCoCo.AddString(globCoCoList.GetAt(t));
		}
		i=m_EditCoCo.FindStringExact(0,globfiltercoco); m_EditCoCo.SetCurSel(i);
	}
	else
	{
		m_EditCoCo.EnableWindow(false);
		m_EditCoCo.AddString(globfiltercoco);
		m_EditCoCo.SetCurSel(0);
	}

	if (globStatus == "NEW") m_EditStatus.SetCurSel(0); 
	else m_EditStatus.SetCurSel(1);			// new entries have default state of "TRANSLATED"
/**/

	if (globString == "") { SetWindowText("add entry"); editentry=false; }
	else { SetWindowText("edit entry"); editentry=true; }
	m_EditStatus.SetWindowText(globStatus);
	m_EditString.SetWindowText(globString);
	m_EditStrID.SetWindowText(globStringID);
	m_EditText.SetWindowText(globTextID);

	m_EditApp.LimitText(8);
	m_EditCoCo.LimitText(3);
	m_EditStatus.LimitText(10);
	m_EditStrID.LimitText(10);
	m_EditString.LimitText(400);
	m_EditText.LimitText(32);
}
