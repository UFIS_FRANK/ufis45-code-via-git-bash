// CompTxtGrid.cpp: implementation of the CCompTxtGrid class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include <RecordSet.h>
#include <ccsGlobl.h>
#include <CompareDlg.h>
#include <CoCo3.h>
#include <CompTxtGrid.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCompTxtGrid::CCompTxtGrid()
{

}

CCompTxtGrid::~CCompTxtGrid()
{

}

BOOL  CCompTxtGrid::OnEndEditing(ROWCOL nRow, ROWCOL nCol)
{
	CString		olUrno, olNewText, olOldText;
	BOOL		blModified = FALSE;
	ROWCOL		ilUrnoCol, ilStatCol;
	RecordSet	olRecord(ogBCD.GetFieldCount("TXT"));
	
	if ( ( (nCol == 5) || (nCol == 8) ) && (nRow>0))		// only string-column is editable
	{
		CGXControl *polControl = GetControl(nRow, nCol);
		if ( polControl )
			blModified = polControl->GetModify();
		ilUrnoCol = nCol - 2;
		ilStatCol = nCol - 1;;

		if ( blModified )
		{	//  ComparGrid l�uft nicht im virtuellen Modus
	
			// get urno from col 3 resp. 6 and new text string from col 5 and 8
			olUrno = GetValueRowCol(nRow,ilUrnoCol);
			olNewText = GetValueRowCol(nRow,nCol);
			Grid2Internal(olNewText); 
	
			if ( !olUrno.IsEmpty() )
				ogBCD.GetField("TXT", "URNO",olUrno, "STRG", olOldText);
	
			if ( olOldText != olNewText )
			{
				if ( olUrno.IsEmpty() )    //neuer Text
				{	
					// convert to replace some special characters
					olRecord.Values[POS_STID] = GetValueRowCol( nRow, 2 );
					olRecord.Values[POS_TXID] = GetValueRowCol( nRow, 1 );
					olRecord.Values[POS_APPL] = GetActualAppl ();
					olRecord.Values[POS_COCO] = GetActualCoco (nCol);
					olRecord.Values[POS_STRN] = olNewText; 
					olUrno.Format("%lu",ogBCD.GetNextUrno() );
					olRecord.Values[POS_URNO] = olUrno;
					olRecord.Values[POS_STAT] = "TRANSLATED"; 
					if ( !ogBCD.InsertRecord("TXT",olRecord, AutoCommitChanges) )
					{
						ogLog.Trace("DEBUG", "[LanguageTool::AddRecord] Failed appending a record into TXTTAB.");
						SetValueRange ( CGXRange(nRow, ilStatCol, nRow, nCol), "" );
					}
					else
					{
						//globStrList.Add(olNewText);			// keep track on strings
						globUrnoList.Add(olUrno);				// and also remember urnos
						SetValueRange ( CGXRange(nRow, ilStatCol), "TRANSLATED" );
						SetValueRange ( CGXRange(nRow, ilUrnoCol), olUrno );
					}
				}
				else   //  ge�nderter Text
				{
					// update record by urno: new text and status is TRANSLATED
					ogBCD.SetField("TXT", "URNO",olUrno, "STRG", olNewText, 
								   AutoCommitChanges);
					ogBCD.SetField("TXT", "URNO",olUrno, "STAT", "TRANSLATED",
								   AutoCommitChanges);
					if ( theApp.TextChanged ( GetActualCoco (nCol), olOldText, olNewText ) > 0 )
					{
						CCompareDlg *polDlg;
						if ( pomParent && 
							 (pomParent->GetRuntimeClass() == RUNTIME_CLASS(CCompareDlg) ) )
						{
							polDlg = (CCompareDlg*)pomParent;
							polDlg->FillGrid ();
						}
					}
					SetValueRange ( CGXRange ( nRow, ilStatCol ), "TRANSLATED" );
					isModified=true;
				}
				if ( pomParent )
					pomParent->SendMessage (GX_ROWCOL_CHANGED, nRow, nCol );
			}
		}
	}
	return TRUE;
}

BOOL CCompTxtGrid::OnStartEditing(ROWCOL nRow, ROWCOL nCol)
{
	if ( (nCol == 5) || (nCol == 8) )		// only string-column is editable
	{
		return TRUE;
	}
	else return FALSE;
}

BOOL CCompTxtGrid::OnLButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	return CGXGridWnd::OnLButtonDblClkRowCol(nRow, nCol, nFlags, pt);
}

CString CCompTxtGrid::GetActualAppl ()
{
	CString olRet;
	CCompareDlg *polDlg;
	if ( pomParent && (pomParent->GetRuntimeClass() == RUNTIME_CLASS(CCompareDlg) ) )
	{
		polDlg = (CCompareDlg*)pomParent;
		if ( polDlg->imSelApplIdx >= 0 )
			polDlg->omApplCb.GetLBText ( polDlg->imSelApplIdx, olRet );
	}
	return olRet;
}

CString CCompTxtGrid::GetActualCoco ( ROWCOL ipColumn )
{
	CString olRet;
	CCompareDlg *polDlg;
	
	if ( pomParent && (pomParent->GetRuntimeClass() == RUNTIME_CLASS(CCompareDlg) ) )
	{
		polDlg = (CCompareDlg*)pomParent;
		if ( ipColumn >= 6 )	//  rechte Sprache
		{
			if ( polDlg->imSelRightCoco >= 0 )
				polDlg->omRightCocoCb.GetLBText ( polDlg->imSelRightCoco, olRet );
		}
		else
		{
			if ( polDlg->imSelLeftCoco >= 0 )
				polDlg->omLeftCocoCb.GetLBText ( polDlg->imSelLeftCoco, olRet );
		}
	}
	return olRet;
}