#if !defined(AFX_CLONEAPPDLG_H__441B9494_91D2_11D3_8FB2_0050DA1CAD13__INCLUDED_)
#define AFX_CLONEAPPDLG_H__441B9494_91D2_11D3_8FB2_0050DA1CAD13__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CloneAppDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCloneAppDlg dialog

class CCloneAppDlg : public CDialog
{
// Construction
public:
	CCloneAppDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCloneAppDlg)
	enum { IDD = IDD_CLONE_DLG };
	CListBox	omOldLangLB;
	CString	m_CloneCoCo;
	BOOL	bmNewLanguage;
	BOOL	bmOldLanguages;
	//}}AFX_DATA
	CStringArray olNewCocos;
	CString	m_OriginCoCo;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCloneAppDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCloneAppDlg)
	afx_msg void OnCheckBox() ;
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLONEAPPDLG_H__441B9494_91D2_11D3_8FB2_0050DA1CAD13__INCLUDED_)
