// InputGrid.cpp: implementation of the CInputGrid class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>


#include <gxgridtab.h>
#include <CoCo3.h>
#include <InputGrid.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CInputGrid::CInputGrid()
{
	bmSortAscend = true;
	imLastSortCol = 0;
	bmSortEnabled = true;
	pomParent = 0;
	imLastFilledRow = 0;
	bmNowPasting = false;
	EnableGridToolTips(TRUE);
	m_nClipboardFlags =  GX_DNDTEXT |  GX_DNDNOAPPENDCOLS | GX_DNDCOMPOSE ;
}

CInputGrid::~CInputGrid()
{

}

CInputGrid::CInputGrid(CWnd *popParent)
{
	bmSortAscend = true;
	imLastSortCol = 0;
	bmSortEnabled = true;
	pomParent = popParent;
	imLastFilledRow = 0;
	bmNowPasting = false;
	EnableGridToolTips(TRUE);
	m_nClipboardFlags =  GX_DNDTEXT |  GX_DNDNOAPPENDCOLS | GX_DNDCOMPOSE ;
}

BEGIN_MESSAGE_MAP(CInputGrid, CGXGridWnd)
	//{{AFX_MSG_MAP(CInputGrid)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		ON_WM_LBUTTONUP()				// wes
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


void CInputGrid::SetParent(CWnd *popParent)
{
	pomParent = popParent;
}

void CInputGrid::OnLButtonUp(UINT nFlags, CPoint point)
{
	CGXGridWnd::OnLButtonUp(nFlags, point);

    ROWCOL  Row;
	ROWCOL  Col;

	if ( !bmSortEnabled )
		return;
	//--- Betroffene Zelle ermitten
    if ( HitTest ( point, &Row,  &Col, NULL ) != GX_HEADERHIT )
		return;

	//--- Check, ob ung�ltige Spaltennummer
	if ( ( Col > GetColCount( ) ) || ( Col== 0 ) )
		return;
	SortTable ( Col );
}


void CInputGrid::SortTable ( ROWCOL ipCol )
{
	CGXSortInfoArray sortInfo;

	DWORD llStart = GetTickCount();
	sortInfo.SetSize(1);

	if ( ipCol != imLastSortCol )
		bmSortAscend = true;
	else
		bmSortAscend = !bmSortAscend;
	
	sortInfo[0].nRC = imLastSortCol = ipCol;
	sortInfo[0].sortType = CGXSortInfo::autodetect;
	sortInfo[0].sortOrder = bmSortAscend ? CGXSortInfo::ascending : 
										   CGXSortInfo::descending;
		
	//SortRows(CGXRange().SetTable(),sortInfo);
	if ( imLastFilledRow > 1 )
		SortRows(CGXRange().SetRows(1,imLastFilledRow),sortInfo);
	DWORD llEnd = GetTickCount();
	llEnd -= llStart;
	CString olText;
	double dlSec = (double)llEnd  / 1000.0;
	olText.Format ( "Funktion dauerte %.3lf sec", dlSec );
	TRACE ( "%s   %s\n", "<GxGridTab::OnLButtonUp>", olText );   

	pomParent->SendMessage(GX_GRID_SORTED,0,0);
}


BOOL CInputGrid::OnValidateCell(ROWCOL nRow, ROWCOL nCol)
{
	BOOL blRet = CGXGridWnd::OnValidateCell(nRow, nCol);
	if ( blRet && (nRow>imLastFilledRow) )
		imLastFilledRow = nRow;
	return blRet;
}

BOOL CInputGrid::SetStyleRange( const CGXRange& range, const CGXStyle* pStyle,
								 GXModifyType mt, int nType /*= 0*/,
								 const CObArray* pCellsArray /*= NULL*/,
								 UINT flags /*= GX_UPDATENOW*/,
								 GXCmdType ctCmd /*= gxDo*/)
{
	if ( bmNowPasting )
		imLastFilledRow = max ( imLastFilledRow, range.bottom );
	return CGXGridWnd::SetStyleRange( range, pStyle, mt, nType, pCellsArray,
									  flags, ctCmd );
}

BOOL CInputGrid::Paste()
{
	BOOL blRet;
	bmNowPasting = true;
	blRet = CGXGridWnd::Paste ();

	bmNowPasting = false;
	return blRet;
}


BOOL CInputGrid::DeleteRow ( ROWCOL ipRow )
{
	if ( ipRow <= imLastFilledRow )
		imLastFilledRow--;
	return RemoveRows ( ipRow, ipRow );
}