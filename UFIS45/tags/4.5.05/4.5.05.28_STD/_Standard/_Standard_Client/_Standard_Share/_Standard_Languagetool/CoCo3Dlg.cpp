// CoCo3Dlg.cpp : implementation file
//
//
// CoCo3Dlg is part of the CoCo project
//
// CoCo3Dlg is a the main dialog class (as this application is dialog based). It provides the user
//	interface for all functions and also a grid to display database entries. Please see the functions
//	headers �for further information.
//	
//
//	Version 1.0 on 04.11.1999 by WES
//
//

#include <stdafx.h>
#include <CoCo3.h>
#include <CoCo3Dlg.h>
#include <CedaBasicData.h>
#include <AddEntryDlg.h>
#include <CCSGlobl.h>
#include <CloneAppDlg.h>
#include <CoCoViewer.h>
#include <ImportResourceDlg.h>
#include <CompareDlg.h>
#include <gxall.h>

#include <AddIntoGrid.h>
//#include <ccsparam.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// some global variable
//
//CCSParam ogCCSParam;

IMPLEMENT_DYNCREATE(CCoCo3Dlg, CDialog)


/////////////////////////////////////////////////////////////////////////////
// CCoCo3Dlg dialog

CCoCo3Dlg::CCoCo3Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCoCo3Dlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCoCo3Dlg)
	m_AutoTranslate = FALSE;
	m_WithConfirmation = FALSE;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	blBCSinceLastLoad = false;
}

void CCoCo3Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCoCo3Dlg)
	DDX_Control(pDX, IDC_WANT_CONFIRMATION_CHK, m_ConfirmationBtn);
	DDX_Control(pDX, IDC_BUTTON4, m_ImportRCButton);
	DDX_Control(pDX, IDC_BUTTON7, m_SaveButton);
	DDX_Control(pDX, IDC_COMBO4, m_CoCos);
	DDX_Control(pDX, IDC_COMBO3, m_Applications);
	DDX_Control(pDX, IDC_CHECK1, m_ShowGeneral);
	DDX_Check(pDX, IDC_AUTO_TRANSLATE_CHK, m_AutoTranslate);
	DDX_Check(pDX, IDC_WANT_CONFIRMATION_CHK, m_WithConfirmation);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CCoCo3Dlg, CDialog)
	//{{AFX_MSG_MAP(CCoCo3Dlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_WM_SIZE()
	ON_CBN_SELCHANGE(IDC_COMBO1, OnSelchangeCombo1)
	ON_CBN_SELCHANGE(IDC_COMBO2, OnSelchangeCombo2)
	ON_WM_KEYDOWN()
	ON_BN_CLICKED(IDC_BUTTON5, OnButton5)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	ON_BN_CLICKED(IDC_BUTTON3, OnButton3)
	ON_WM_LBUTTONDOWN()
	ON_BN_CLICKED(IDC_BUTTON6, OnButton6)
	ON_BN_CLICKED(IDC_CHECK1, OnShowGeneral)
	ON_BN_CLICKED(IDC_BUTTON40, OnButton40)
	ON_CBN_SELCHANGE(IDC_COMBO3, OnSelchangeCombo3)
	ON_CBN_SELCHANGE(IDC_COMBO4, OnSelchangeCombo4)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BUTTON4, OnButton4)
	ON_BN_CLICKED(IDC_BUTTON7, OnButton7)
	ON_BN_CLICKED(IDC_ADD_IN_GRID, OnAddInGrid)
	ON_BN_CLICKED(IDC_WANT_CONFIRMATION_CHK, OnWantConfirmationChk)
	ON_BN_CLICKED(IDC_AUTO_TRANSLATE_CHK, OnAutoTranslateChk)
	//}}AFX_MSG_MAP
	ON_MESSAGE(GX_ROWCOL_CHANGED, OnGridEndEditing)
	ON_MESSAGE(WM_BCADD,OnBcAdd)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCoCo3Dlg message handlers


//
// OnInitDialog resizes the grid to the maximum. It also sets the first row of the grid to act
//	as grids header. The two comboboxes (application name, language) are filled with default values.
//
//
BOOL CCoCo3Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	if (!showRCButton) m_ImportRCButton.ShowWindow(SW_HIDE);
	
	// TODO: Add extra initialization here
	omCoCoGrid.SetParent(this);
	omCoCoGrid.SubclassDlgItem(IDC_GRID1, this);
	omCoCoGrid.Initialize();
	omCoCoGrid.GetParam()->EnableUndo(FALSE);

	myglobGrid=&omCoCoGrid;				// used to track duplicate entries
	pogTxtGrid = &omCoCoGrid;	

	// resize Grid
	RECT myrect;

	GetClientRect(&myrect);
	myrect.top=75;
	omCoCoGrid.MoveWindow(&myrect,TRUE); 

	// set grids data
	omCoCoGrid.SetColCount(7);
	omCoCoGrid.SetValueRange(CGXRange(0, 0), "Nr");	
	omCoCoGrid.SetValueRange(CGXRange(0, 1), "Urno");		
	omCoCoGrid.SetValueRange(CGXRange(0, 2), "App");		
	omCoCoGrid.SetValueRange(CGXRange(0, 3), "CoCo");		
	omCoCoGrid.SetValueRange(CGXRange(0, 4), "Status");		
	omCoCoGrid.SetValueRange(CGXRange(0, 5), "String");		
	omCoCoGrid.SetValueRange(CGXRange(0, 6), "TextID");		
	omCoCoGrid.SetValueRange(CGXRange(0, 7), "StringID");	

	for (int t=0; t<8; t++)	
		omCoCoGrid.SetColWidth(t, t, StdColWidthArray[t]);

	int fcnt=ogBCD.GetDataCount("TXT");
	omCoCoGrid.SetRowCount(fcnt);

	// hide urno if not specified in ceda.ini
	if (showUrnoCol == 0) omCoCoGrid.HideCols(1,1,TRUE);

	m_Applications.Clear();
	m_Applications.AddString(AllApplicationStr);

	m_CoCos.Clear();
	m_CoCos.AddString(AllLanguagesStr);

	// get field index for all visible fields - this is VERY important because of the different installations

	POS_URNO=ogBCD.GetFieldIndex("TXT","URNO");
	POS_APPL=ogBCD.GetFieldIndex("TXT","APPL");
	POS_COCO=ogBCD.GetFieldIndex("TXT","COCO");
	POS_STAT=ogBCD.GetFieldIndex("TXT","STAT");
	POS_STRN=ogBCD.GetFieldIndex("TXT","STRG");
	POS_TXID=ogBCD.GetFieldIndex("TXT","TXID");
	POS_STID=ogBCD.GetFieldIndex("TXT","STID");

	
	IniStringGrid ();
	UpdateDisplay();
	omCoCoGrid.GetParam()->EnableUndo(TRUE);

	omCoCoGrid.SetFocus();	// focus grid on startup

	ShowWindow(SW_SHOWMAXIMIZED);			// maximize on startup
	isModified=false;

	TestModifiedState();

	OnWantConfirmationChk();

	char pclConfigPath[142];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));


	char pclUseBcProxy[256];
	GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclUseBcProxy, sizeof pclUseBcProxy, pclConfigPath);

	//MWO/RRO 25.03.03
	if (stricmp(pclUseBcProxy,"BCPROXY") == 0 || stricmp(pclUseBcProxy,"BCCOM") == 0)
	{
		//MWO/RRO
		//ogCommHandler.RegisterBcWindow(this);
		ogBcHandle.StartBc(); 
		//END MWO/RRO
	}
	else
	{
		ogCommHandler.RegisterBcWindow(this);
	}

	ogDdx.Register(this, TXT_NEW,	 CString("CCoCo3Dlg"), CString("TXT New"),    MainDDXCallback);
	ogDdx.Register(this, TXT_CHANGE, CString("CCoCo3Dlg"), CString("TXT Change"), MainDDXCallback);
	ogDdx.Register(this, TXT_DELETE, CString("CCoCo3Dlg"), CString("TXT Delete"), MainDDXCallback);

	return TRUE;  // return TRUE  unless you set the focus to a control
}


//
// show status of modifications in caption and "save"-buttons color
//
//
void CCoCo3Dlg::TestModifiedState()
{
	if (isModified) 
	{
		SetWindowText(defaultTitle+" (modified)");
		m_SaveButton.SetWindowText("save!");
	}
	else 
	{
		SetWindowText(defaultTitle);
		m_SaveButton.SetWindowText("(save)");
	}
}


// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCoCo3Dlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCoCo3Dlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}


//
// add an entry to the current list
//
//
void CCoCo3Dlg::OnButton1()		// Add entry
{
	AddEntryDlg	MyDlg;
	CString s;

	int i=m_Applications.GetCurSel(); 	// set global variables
	if (i>=0) 
		m_Applications.GetLBText(i,globfilterapp);
	else 
		globfilterapp="";
	i= m_CoCos.GetCurSel(); 
	if (i>=0) 
		m_CoCos.GetLBText(i,globfiltercoco);
	else 
		globfiltercoco="";

	globStatus="";
	globString="";
	globTextID="";
	globStringID="";

	if ( MyDlg.DoModal() == IDOK )
	{
		CString olStid, olStrg, olTxid, olAppl, olCoco, olStat, olUrno;

		omCoCoGrid.GetParam()->EnableUndo(FALSE);

		int i=omCoCoGrid.GetRowCount()+1;		// append one field
		omCoCoGrid.SetRowCount(i);

		olStid = MyDlg.m_NewStringID; 
		Grid2Internal(olStid); 
		
		olStrg = MyDlg.m_NewString; 
		Grid2Internal(olStrg); 
		MakeCedaString(olStrg); 
		
		olAppl = MyDlg.m_NewApplication; 
		Grid2Internal(olAppl); 

		olTxid = MyDlg.m_NewTextID; 
		Grid2Internal(olTxid); 
		
		olStat = ( !MyDlg.m_NewStatus )  ? "NEW" : "TRANSLATED"; 
		MakeCedaString(olStat); 
		
		olCoco = MyDlg.m_NewCoCo; 
		Grid2Internal(olCoco); 
		
		if ( theApp.AddTxtRecord( olAppl, olCoco, olStat, olStrg, olTxid, 
								  olStid, olUrno ) )
		{
			ogLog.Trace("DEBUG", "[LanguageTool::AddRecord] Failed appending a record into TXTTAB.");
			omCoCoGrid.SetRowCount(i-1);	// remove last entry on grid
		}
		omCoCoGrid.GetParam()->EnableUndo(TRUE);
		UpdateDisplay();			// optimized update display - only on grid
		
	}
	TestModifiedState();
}

//
// resize the grid to maximum
//
//
void CCoCo3Dlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	if (omCoCoGrid.m_hWnd)			// resize grid of already existing
	{
		RECT myrect;

		omCoCoGrid.GetParam()->EnableUndo(FALSE);
	
		GetClientRect(&myrect);
		myrect.right=cx;
		myrect.top=75;
		omCoCoGrid.MoveWindow(&myrect,TRUE); 
		// resize grids colum for string text
		int n=0;
		int t;
		for (t=0; t<8; t++)
		{
			if ((!omCoCoGrid.IsColHidden(t)) && (t != 5)) n+=omCoCoGrid.GetColWidth(t);
		}
		omCoCoGrid.GetClientRect(&myrect);
		int newsize=(myrect.right-myrect.left)-n;
		if (newsize<50) newsize=50;
		omCoCoGrid.SetColWidth(5,5,newsize);
		omCoCoGrid.GetParam()->EnableUndo(TRUE);
	
	}
}


//
// UpdateDisplay clears and recreates the content of the main grid. It also updates the comboboxes
//	for application name and language. A standard sorting criteria (application name, ascending) is
//	applied.
//
//
void CCoCo3Dlg::UpdateDisplay()
{
	DWORD llStart = GetTickCount();
	
	int filterapp  = m_Applications.GetCurSel();
	int filtercoco = m_CoCos.GetCurSel();
	char *pclCocoFilter=0, *pclApplFilter=0;
	
	CString filterAppStr, filterCoCoStr, olAppl, olCoco;
	int dofilterapp, dofiltercoco, dogeneralall;

	if (filterapp > -1) 
	{ 
		m_Applications.GetLBText(filterapp,filterAppStr); 
		dofilterapp = (strcmp(filterAppStr,AllApplicationStr) != 0); 
	}
	else dofilterapp = false;

	if (filtercoco > -1) 
	{ 
		m_CoCos.GetLBText(filtercoco,filterCoCoStr); 
		dofiltercoco = (strcmp(filterCoCoStr,AllLanguagesStr) != 0);
	}
	else 
		dofiltercoco = false;

	dogeneralall = m_ShowGeneral.GetCheck();
	
	pclCocoFilter = dofiltercoco ? filterCoCoStr.GetBuffer(0) : 0;
	pclApplFilter = dofilterapp ? filterAppStr.GetBuffer(0) : 0; 
	omCoCoGrid.LoadData ( pclCocoFilter, pclApplFilter, dogeneralall==TRUE );
	if ( blBCSinceLastLoad )
		SynchrCocoApplList();
	omCoCoGrid.Redraw ();

	TestModifiedState();

	DWORD llEnd = GetTickCount();
	llEnd -= llStart;
	CString olText;
	double dlSec = (double)llEnd  / 1000.0;
	olText.Format ( "Funktion dauerte %.3lf sec", dlSec );
	TRACE ( "%s   %s\n", "<CCoCo3Dlg::UpdateDisplay>", olText );   
	//MessageBox ( olText, "<CCoCo3Dlg::UpdateDisplay>" );

	omCoCoGrid.SelectRange ( CGXRange().SetTable(), FALSE );
}

//
// update grid when another application is selected
//
//
void CCoCo3Dlg::OnSelchangeCombo1() 
{
	UpdateDisplay();
}

//
// update grid if another language is selected
//
//
void CCoCo3Dlg::OnSelchangeCombo2() 
{
	UpdateDisplay();
}

void CCoCo3Dlg::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	
	CDialog::OnKeyDown(nChar, nRepCnt, nFlags);
}


//
// Activate searching function inside Grid
//
//
void CCoCo3Dlg::OnButton5()		//  search
{
	omCoCoGrid.OnShowFindReplaceDialog(TRUE);
}


//
// Clone an existing language to be used with different coco in the same application.
//	Cloning means to duplicate entries and reset its values for county code (=selected in the
//	dialog box) and status (="NEW"). Urno is automatically added when inserting.
//
//
void CCoCo3Dlg::OnButton2()		//  clone app.
{
	CCloneAppDlg	MyDlg;

	CString			AppName,OldCoCo;
	CString			olTxid, olStid, olText, olMsg;
	CStringArray	olFailed;

	int filterapp  = m_Applications.GetCurSel();
	int filtercoco = m_CoCos.GetCurSel();
	if (filterapp > 0) 
	{
		if (filtercoco > 0)
		{
			m_Applications.GetLBText(filterapp,AppName);
			m_CoCos.GetLBText(filtercoco,OldCoCo);
			
			omCoCoGrid.GetParam()->EnableUndo(FALSE);

			MyDlg.m_OriginCoCo= OldCoCo;	// zu kopierende Sprache in Dlg setzen
			if (MyDlg.DoModal() == IDOK)
			{
				for ( int i=0; i<MyDlg.olNewCocos.GetSize(); i++ )
				{
					CString olNewCoco = MyDlg.olNewCocos[i];
					if ( OldCoCo !=olNewCoco )
					{
						int cnt=ogBCD.GetFieldCount("TXT");
						RecordSet olRecord(cnt);//, olNewRecord(cnt);
						
						CString newurno;

						int fcnt=ogBCD.GetDataCount("TXT");

						int rcnt = 1;
						for (int t=1; t<=fcnt;t++)
						{
							ogBCD.GetRecord("TXT", t-1, olRecord);

							if ( !strcmp(olRecord.Values[POS_APPL],AppName) && 
								 !strcmp(olRecord.Values[POS_COCO],OldCoCo) )
							{
								int i=omCoCoGrid.GetRowCount()+1;		// append one field
								omCoCoGrid.SetRowCount(i);

								newurno.Empty();
								if ( theApp.AddTxtRecord ( olRecord.Values[POS_APPL], olNewCoco, 
																  CString("NEW"), olRecord.Values[POS_STRN], 
																  olRecord.Values[POS_TXID], 
																  olRecord.Values[POS_STID], 
																  newurno, false ) )
								{
									omCoCoGrid.SetRowCount(i-1);	

									olTxid = ( olRecord.Values[POS_TXID].IsEmpty () ) ? "<empty>" 
																					  : olRecord.Values[POS_TXID];
									olStid = ( olRecord.Values[POS_STID].IsEmpty () ) ? "<empty>" 
																					  : olRecord.Values[POS_STID];
									olText.Format ( " COCO=%s TXID=%s STID=%s ",
													olNewCoco, olTxid, olStid );
									olFailed.Add ( olText );
								}
								else
									isModified=true;
							}
						}
					}
					else 
						MessageBox("Can not clone same languages - please select a different language before trying to clone this application.", 
									"sorry ...", MB_OK | MB_ICONEXCLAMATION); 
				}	//  end of for-loop
				if ( olFailed.GetSize() > 0 )
				{
					olMsg = "Insert of following records failed:\n";
					for ( i=0; i<olFailed.GetSize(); i++ )
					{
						if ( i<10 )
							olMsg += olFailed[i] + "\n";
						else
						{
							olMsg += "...";
							break;
						}
					}
					MessageBox ( olMsg, "Clone Application" );
				}
				if ( isModified )
				{
					UpdateDisplay(); // changes aren't done on the grid anly longer - so new update needed
					if ( AutoCommitChanges )
						ogBCD.Save ("TXT" );
				}
			}
			omCoCoGrid.GetParam()->EnableUndo(TRUE);
		}
		else 
			MessageBox("Please select a language before trying to clone this application.",
						"sorry ...", MB_OK | MB_ICONEXCLAMATION); 
	}
	else 
		MessageBox("Please select an application before trying to clone its language.", 
					"sorry ...", MB_OK | MB_ICONEXCLAMATION); 
	TestModifiedState();
}

//
// Delete selected rows. To maintain the correct ros number, the for-loop starts at the end of the list.
//
//
void CCoCo3Dlg::OnButton3()		//  delete entry
{
	CRowColArray MyRows;
	int selcnt,i;
	CString MyUrno;

	selcnt=omCoCoGrid.GetSelectedRows(MyRows, TRUE, TRUE);	
	if (selcnt > 0)
	{
		omCoCoGrid.LockUpdate(TRUE);
		ogBCD.Save("TXT");
		if ( MessageBox("Do you really want to delete the selected rows.", "please confirm:", MB_YESNO | MB_ICONQUESTION) == IDYES)
		{
			isModified=false;
			for (int t=(selcnt-1); t>=0; t--) 
			{
				MyUrno=omCoCoGrid.GetValueRowCol(MyRows[t],1);			// get urno from grid
				//if ( !ogBCD.DeleteRecord("TXT","URNO",MyUrno, true ) )
				if ( !ogBCD.DeleteRecord("TXT","URNO",MyUrno ) )
				{
					//ogLog.Trace("DEBUG", "[LanguageTool::DeleteEntries] Failed deleting a record from TXTTAB.");
					isModified = true;
				}
				else
				{
					omCoCoGrid.omGridData.DeleteAt ( MyRows[t]-1 );
					i=globUrnoList.GetSize()-1;
					while ((i>=0) && (MyUrno != globUrnoList[i])) i--;
					if (i>=0)
					{
						//if ( i< globStrList.GetSize() )
						//	globStrList.RemoveAt(i);						// keep track on strings
						globUrnoList.RemoveAt(i);						// and also remember urnos
					}
				}
			}
			if ( !ogBCD.Save ( "TXT" ) )
				isModified = true;
			if ( isModified )		//  entweder L�schen oder Speichern ging schief
				ogLog.Trace("DEBUG", "[LanguageTool::DeleteEntries] Failed deleting one or more records from TXTTAB.");
			//UpdateDisplay(); // not needed anymore !
			omCoCoGrid.LockUpdate(FALSE);
			omCoCoGrid.Redraw ();
		}
		else
			omCoCoGrid.LockUpdate(FALSE);
	}
	else
		MessageBox(	"Can not delete because no row is selected.", "sorry ...",
					MB_OK | MB_ICONEXCLAMATION );
	TestModifiedState();
}

void CCoCo3Dlg::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CDialog::OnLButtonDown(nFlags, point);
}


//
// Compare country code for an application: this is done inside the dialog. Please look inside
//	the docu of CCoCoViewer for more information.
//
//
void CCoCo3Dlg::OnButton6()		//  compare
{
	// set global variables for the dialog
	CCompareDlg olCompareDlg(this);

	int filterapp  = m_Applications.GetCurSel();
	if (filterapp > 0) 
		m_Applications.GetLBText(filterapp, refApp); 
	else
		refApp.Empty();
	int filtercoco  = m_CoCos.GetCurSel();
	if (filtercoco > 0) 
		m_CoCos.GetLBText(filtercoco, refCoCo); 
	else
		refCoCo.Empty();

	olCompareDlg.DoModal();
	if ( olCompareDlg.imDataStatus == DATA_NEW )
		UpdateDisplay();
	else
		if ( olCompareDlg.imDataStatus == DATA_CHANGED )
			omCoCoGrid.Redraw ();
	isModified = ( olCompareDlg.imDataStatus != DATA_UNCHANGED );

/*	hag 20000224
	int filterapp  = m_Applications.GetCurSel();
	if (filterapp > 0) 
	{ 
		m_Applications.GetLBText(filterapp, refApp); 

		int filtercoco  = m_CoCos.GetCurSel();
		if (filtercoco > 0) { m_CoCos.GetLBText(filtercoco, refCoCo); }
		else { refCoCo="US"; }

		CCoCoViewer	MyDlg;

		CString s = "reference language is "+refCoCo;
		MyDlg.m_RefCoCo=s;
		MyDlg.DoModal();

		
		UpdateDisplay();
	}
	else { MessageBox("Please select an application before trying to compare its languages.", "sorry ...", MB_OK | MB_ICONEXCLAMATION); }
	*/
	TestModifiedState();
}



void CCoCo3Dlg::OnShowGeneral() 
{
	UpdateDisplay();	
}

//
// clone selection
//
//
void CCoCo3Dlg::OnButton40() //  clone selection
{
	CCloneAppDlg	MyDlg;

	CString AppName,OldCoCo;
	CRowColArray MyRows;
	int selcnt;
	int filterapp  = m_Applications.GetCurSel();
	int i, t;
	UINT ilRowAnz;
	bool blSameCoco=true;

	selcnt=omCoCoGrid.GetSelectedRows(MyRows, TRUE, TRUE);	
	ilRowAnz = omCoCoGrid.GetRowCount();
	for ( i=selcnt-1; i>=0; i-- )
		if ( (MyRows[i] < 1) || (MyRows[i]>ilRowAnz) )
			MyRows.RemoveAt( i );
	selcnt = MyRows.GetSize ();
	if ( selcnt <= 0 )
		return;		//  keine Zeilen selektiert
	if (filterapp > 0) 
	{
		
		OldCoCo = omCoCoGrid.GetValueRowCol(MyRows[0],3);
		for ( i=1; blSameCoco&&(i<MyRows.GetSize()); i++ )
			if ( OldCoCo != omCoCoGrid.GetValueRowCol(MyRows[i],3) )
				blSameCoco = false;
						
		if (blSameCoco )
		{
			CString MyUrno, olMsg, olText;
			CStringArray olFailed;
	
			m_Applications.GetLBText(filterapp,AppName);
			omCoCoGrid.GetParam()->EnableUndo(FALSE);

			MyDlg.m_OriginCoCo= OldCoCo;	// zu kopierende Sprache in Dlg setzen
			if (MyDlg.DoModal() == IDOK)
			{
				for ( i=0; i<MyDlg.olNewCocos.GetSize(); i++ )
				{
					CString olNewCoco = MyDlg.olNewCocos[i];
					if ( OldCoCo != olNewCoco )
					{
						CString newurno, olStid, olTxid, olAppl, olStrg;

						for (t=(selcnt-1); t>=0; t--) 
						{

							olStid = omCoCoGrid.GetValueRowCol(MyRows[t],7);
							olTxid = omCoCoGrid.GetValueRowCol(MyRows[t],6); 
							i=omCoCoGrid.GetRowCount()+1;		// append one field (one by one :)
							omCoCoGrid.SetRowCount(i);
							olAppl = omCoCoGrid.GetValueRowCol(MyRows[t],2);
							olStrg = omCoCoGrid.GetValueRowCol(MyRows[t],5);
							newurno.Empty();
							if ( theApp.AddTxtRecord ( olAppl, olNewCoco, CString("NEW"), 
													   olStrg, olTxid, olStid, newurno, false ) )
							{	
								omCoCoGrid.SetRowCount(i-1);	

								if ( olTxid.IsEmpty () )
									olTxid = "<empty>";
								if ( olStid.IsEmpty () )
									olStid = "<empty>";
								olText.Format ( " COCO=%s TXID=%s STID=%s ",
												olNewCoco, olTxid, olStid );
								olFailed.Add ( olText );
							}
							else
								isModified=true;

						}
					}
					else 
						MessageBox("Can not clone same languages - please select a different language before trying to clone this application.", "sorry ...", MB_OK | MB_ICONEXCLAMATION); 
				}	//  end of for-loop
				if ( olFailed.GetSize() > 0 )
				{
					olMsg = "Insert of following records failed:\n";
					for ( i=0; i<olFailed.GetSize(); i++ )
					{
						if ( i<10 )
							olMsg += olFailed[i] + "\n";
						else
						{
							olMsg += "...";
							break;
						}
					}
					MessageBox ( olMsg, "Clone Selection" );
				}
				if ( isModified )
					UpdateDisplay();		
			}
			omCoCoGrid.GetParam()->EnableUndo(TRUE);
		}
		else 
			MessageBox("Please select strings only from one language before trying to clone then.", "sorry ...", MB_OK | MB_ICONEXCLAMATION);
	}
	else 
		MessageBox("Please select an application before trying to clone its language.", "sorry ...", MB_OK | MB_ICONEXCLAMATION); 
	TestModifiedState();
}

void CCoCo3Dlg::OnSelchangeCombo3() 
{
	UpdateDisplay();
}

void CCoCo3Dlg::OnSelchangeCombo4() 
{
	UpdateDisplay();
}

void CCoCo3Dlg::OnClose() 
{
	int k;
	if (isModified && ((k=MessageBox("Do you want to save all changes to the database?", "please confirm:", MB_YESNOCANCEL | MB_DEFBUTTON1 | MB_ICONQUESTION)) == IDYES))
	{
		ogBCD.Save("TXT");
	}
	if (k != IDCANCEL) 
	{
		ogDdx.UnRegisterAll();
		CDialog::OnClose();		// do not close on cancel !!!!
	}
}

void CCoCo3Dlg::OnDofindgrid() 
{
	OnButton5();	
}

//
// import a projects resource files
//
//
void CCoCo3Dlg::OnButton4()		//import RC
{
	CImportResourceDlg	MyDlg;

	CString AppName,CoCo,s;

	int i=m_Applications.GetCurSel(); 	// set global variables
	if (i>=0) m_Applications.GetLBText(i,globfilterapp);
	else globfilterapp="";
	i= m_CoCos.GetCurSel(); 
	if (i>=0) m_CoCos.GetLBText(i,globfiltercoco);
	else globfiltercoco="";

/*	globAppList.RemoveAll();
	for (int t=1; t<m_Applications.GetCount(); t++)		// add all entries to the global selectable application list
	{
		m_Applications.GetLBText(t,s);
		globAppList.Add(s);
	}
	globCoCoList.RemoveAll();
	for (t=1; t<m_CoCos.GetCount(); t++)		// add all entries to the global selectable language list
	{
		m_CoCos.GetLBText(t,s);
		globCoCoList.Add(s);
	}*/

	MyDlg.DoModal();

	if (isModified) UpdateDisplay();
	TestModifiedState();
}

//
// Write records (modifications) in CedaBasicData back to database
//
//
void CCoCo3Dlg::OnButton7()		//save
{
	if (MessageBox("Do you want to save all changes to the database?", "please confirm:", MB_YESNO | MB_DEFBUTTON1 | MB_ICONQUESTION) == IDYES)
	{
		ogBCD.Save("TXT");
		isModified=false;
		TestModifiedState();
	}
}

void CCoCo3Dlg::IniStringGrid ()
{
	CString olAppl, olCoco, olUrno, olStrg;
	DWORD llStart = GetTickCount();

	int cnt=ogBCD.GetFieldCount("TXT");
	RecordSet olRecord(cnt);
	int fcnt=ogBCD.GetDataCount("TXT");

	omCoCoGrid.SetRowCount(fcnt);

	//globStrList.RemoveAll();
	globUrnoList.RemoveAll();
	globAppList.RemoveAll();
	globCoCoList.RemoveAll();
	
	for (int t=1; t<=fcnt;t++)
	{
		ogBCD.GetRecord("TXT", t-1, olRecord);

		olUrno = olRecord.Values[POS_URNO];
		olAppl = olRecord.Values[POS_APPL];
		olStrg = olRecord.Values[POS_STRN]; 
		olCoco = olRecord.Values[POS_COCO]; 

		MakeClientString(olStrg); 
		Internal2Grid(olStrg); 

		//globStrList.Add(olStrg);				// keep track on strings
		globUrnoList.Add(olUrno);				// and also remember urnos

		AddApplIfNew ( olAppl );
		AddCocoIfNew ( olCoco );
	}

	TestModifiedState();

	DWORD llEnd = GetTickCount();
	llEnd -= llStart;
	CString olText;
	double dlSec = (double)llEnd  / 1000.0;
	olText.Format ( "Funktion dauerte %.3lf sec", dlSec );
}

void CCoCo3Dlg::OnGridEndEditing( WPARAM nRow, LPARAM nCol )
{
	TestModifiedState();
}

void CCoCo3Dlg::OnAddInGrid() 
{
	// TODO: Add your control notification handler code here
	CAddIntoGrid olDlg(this);
	if ( olDlg.DoModal () == IDOK )
		UpdateDisplay();
	
}

void CCoCo3Dlg::AddApplIfNew ( LPCSTR pcpAppl )
{
	if ( !pcpAppl || (strlen(pcpAppl)<=0) )
		return;
	if ( m_Applications.FindStringExact(0,pcpAppl) == CB_ERR )
	{
		m_Applications.AddString(pcpAppl);
		globAppList.Add(pcpAppl) ;
	}
}

void CCoCo3Dlg::AddCocoIfNew ( LPCSTR pcpCoco )
{
	if ( !pcpCoco || (strlen(pcpCoco)<=0) )
		return;
	if ( m_CoCos.FindStringExact(0,pcpCoco) == CB_ERR )
	{
		m_CoCos.AddString(pcpCoco); 
		globCoCoList.Add(pcpCoco);
	}
}

void CCoCo3Dlg::OnWantConfirmationChk() 
{
	// TODO: Add your control notification handler code here
	UpdateData ();
	if ( m_AutoTranslate )
		igAutoTranslationMode = m_WithConfirmation ? AUTO_WITH_CONFIRM : AUTO_NO_CONFIRM;
	else
		igAutoTranslationMode = NO_AUTO_TRANS;
}

void CCoCo3Dlg::OnAutoTranslateChk() 
{
	// TODO: Add your control notification handler code here
	UpdateData ();
	m_ConfirmationBtn.EnableWindow (m_AutoTranslate);
	if ( m_AutoTranslate )
		igAutoTranslationMode = m_WithConfirmation ? AUTO_WITH_CONFIRM : AUTO_NO_CONFIRM;
	else
		igAutoTranslationMode = NO_AUTO_TRANS;
}

LONG CCoCo3Dlg::OnBcAdd(UINT wParam, LONG lParam)
{
	ogBcHandle.GetBc(wParam);
	blBCSinceLastLoad = true;
	return TRUE;
} 

void CCoCo3Dlg::SynchrCocoApplList()
{
	DWORD llStart = GetTickCount();
	for ( int i =0; i<ogBCD.GetDataCount("TXT"); i++ )
	{
		AddApplIfNew ( ogBCD.GetField("TXT", i, "APPL" ) );
		AddCocoIfNew ( ogBCD.GetField("TXT", i, "COCO" ) );
	}	
	blBCSinceLastLoad = false;
	DWORD llEnd = GetTickCount();
	llEnd -= llStart;
	CString olText;
	double dlSec = (double)llEnd  / 1000.0;
	olText.Format ( "Funktion dauerte %.3lf sec", dlSec );
}


void CCoCo3Dlg::OnTXTBC ( RecordSet *popRec, int ipDDXType )
{
	CString	olUrno;
	RecordSet *oldptr, *polRec;
	if ( !popRec )
		return;
	olUrno = popRec->Values[POS_URNO];
	for ( int i=0; i<omCoCoGrid.omGridData.GetSize(); i++ )
	{
		if ( omCoCoGrid.omGridData[i].urno == olUrno )
		{
			oldptr = omCoCoGrid.omGridData[i].ptr;
			if ( ipDDXType == TXT_DELETE )
				omCoCoGrid.omGridData[i].ptr = 0;
			else
			{
				polRec = GetAdressOfTxtRec ( olUrno );
				omCoCoGrid.omGridData[i].ptr = polRec; 
			}
		}
	}
}

//---------------------------------------------------------------------------
//			implementation of static and global functions
//---------------------------------------------------------------------------
static void MainDDXCallback(void *popInstance, int ipDDXType,
							void *vpDataPointer, CString &ropInstanceName)
{
	RecordSet *polRec;
	CCoCo3Dlg *polMainDlg = (CCoCo3Dlg*)theApp.m_pMainWnd;

	switch (ipDDXType)
	{
		case TXT_NEW:
		case TXT_CHANGE:
		case TXT_DELETE:
			polRec = ( RecordSet *)vpDataPointer;	
			if ( polRec && polMainDlg )
				polMainDlg->OnTXTBC ( polRec, ipDDXType );
	}
}
