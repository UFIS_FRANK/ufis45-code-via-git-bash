# Microsoft Developer Studio Project File - Name="CoCo3" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=CoCo3 - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "CoCo3.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "CoCo3.mak" CFG="CoCo3 - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "CoCo3 - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "CoCo3 - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "CoCo3 - Win32 Release"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "C:\Ufis_Bin\Release"
# PROP Intermediate_Dir "C:\Ufis_Intermediate\Languagetool\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I ".\\" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 C:\Ufis_Bin\Release\Ufis32.lib c:\Ufis_Bin\ClassLib\Release\CCSClass.lib Wsock32.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "CoCo3 - Win32 Debug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "C:\Ufis_Bin\Debug"
# PROP Intermediate_Dir "C:\Ufis_Intermediate\Languagetool\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x407 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 C:\Ufis_Bin\Debug\Ufis32.lib c:\Ufis_Bin\ClassLib\Debug\CCSClass.lib Wsock32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "CoCo3 - Win32 Release"
# Name "CoCo3 - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\_Standard_Wrapper\aatlogin.cpp
# End Source File
# Begin Source File

SOURCE=.\AddEntryDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\AddIntoGrid.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSGlobl.cpp
# End Source File
# Begin Source File

SOURCE=.\CloneAppDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CoCo3.cpp
# End Source File
# Begin Source File

SOURCE=.\CoCo3.rc
# End Source File
# Begin Source File

SOURCE=.\CoCo3Dlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CoCoViewer.cpp
# End Source File
# Begin Source File

SOURCE=.\CompareDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CompTxtGrid.cpp
# End Source File
# Begin Source File

SOURCE=.\GxGridTab.cpp
# End Source File
# Begin Source File

SOURCE=.\ImportResourceDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\InputGrid.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\TxtGrid.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\_Standard_Wrapper\aatlogin.h
# End Source File
# Begin Source File

SOURCE=.\AddEntryDlg.h
# End Source File
# Begin Source File

SOURCE=.\AddIntoGrid.h
# End Source File
# Begin Source File

SOURCE=.\CCSGLOBL.H
# End Source File
# Begin Source File

SOURCE=.\CloneAppDlg.h
# End Source File
# Begin Source File

SOURCE=.\CoCo3.h
# End Source File
# Begin Source File

SOURCE=.\CoCo3Dlg.h
# End Source File
# Begin Source File

SOURCE=.\CoCoViewer.h
# End Source File
# Begin Source File

SOURCE=.\CompareDlg.h
# End Source File
# Begin Source File

SOURCE=.\CompTxtGrid.h
# End Source File
# Begin Source File

SOURCE=.\GxGridTab.h
# End Source File
# Begin Source File

SOURCE=.\ImportResourceDlg.h
# End Source File
# Begin Source File

SOURCE=.\InputGrid.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\TxtGrid.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\CoCo3.ico
# End Source File
# Begin Source File

SOURCE=.\res\CoCo3.rc2
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon2.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon3.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon4.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon5.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon6.ico
# End Source File
# End Group
# Begin Group "Help Files"

# PROP Default_Filter "cnt;rtf"
# Begin Source File

SOURCE=.\Docu\manual.html
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
