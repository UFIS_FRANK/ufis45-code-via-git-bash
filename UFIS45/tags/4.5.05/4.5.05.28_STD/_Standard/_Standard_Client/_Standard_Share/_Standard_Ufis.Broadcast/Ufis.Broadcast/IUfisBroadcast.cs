﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ufis.Broadcast
{
    /// <summary>
    /// Interface for Ufis Broadcast.
    /// </summary>
    public interface IUfisBroadcast: IDisposable
    {
        /// <summary>
        /// Event handler when broadcast is received.
        /// </summary>
        event EventHandler<BroadcastEventArgs> BroadcastReceived;
    }
}
