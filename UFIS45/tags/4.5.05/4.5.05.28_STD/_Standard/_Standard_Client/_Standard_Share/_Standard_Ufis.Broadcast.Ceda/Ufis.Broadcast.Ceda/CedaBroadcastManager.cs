﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Ufis.Broadcast;

namespace Ufis.Broadcast.Ceda
{
    /// <summary>
	/// The CedaBroadcastManager class provides a black box from where external 
	/// data changes are automatically incorporated into the application.
	/// The UFIS suite provides the BCProxy, BCComClient (ActiveX), BCComServer and MQSeries 
	/// in the future as potontial	Broadcast servers. To keep it transparent from 
	/// where the broadcasts are received. This class manages the connection to the external
	/// ATL servers and distributes the broadcasts into the <see cref="Ufis.Data"/> instances
	/// to decide what shall happen with the data change message.
	/// </summary>
    public class CedaBroadcastManager : IUfisBroadcast
    {
        /// <summary>
        /// Broadcast event types.
        /// </summary>
		private enum EventTypes
		{
			None	= 0,
			BcProxy = 1,
			BcCom	= 2,
			BcMq	= 4
		};

		#region implementation
        private readonly BcProxyLib.BcPrxy _bcProxy;				    // BcProxy object reference
        private readonly BCCOMSERVERLib.BcComAtl _bcCom;		        // BcCom object reference
        private readonly EventTypes _eventTypes = EventTypes.BcProxy;	// for compatibility reasons
        private readonly StringBuilder _bcData = new StringBuilder();
        private bool _disposed;                                         // Track whether Dispose has been called.
		#endregion
        /// <summary>
        /// Event handler when broadcast is received.
        /// </summary>
        public event EventHandler<BroadcastEventArgs> BroadcastReceived;

        protected virtual void OnBroadcastReceived(BroadcastEventArgs e)
        {
            EventHandler<BroadcastEventArgs> handler = BroadcastReceived;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Broadcast.Ceda.CedaBroadcastManager class.
        /// </summary>
        public CedaBroadcastManager()
            : this(null)
        {

        }

        /// <summary>
        /// Initializes the new instance of Ufis.Broadcast.Ceda.CedaBroadcastManager class.
        /// </summary>
        /// <param name="bcEventTypes">The broadcast event type.</param>
        public CedaBroadcastManager(string bcEventTypes)
		{					
			if (string.IsNullOrEmpty(bcEventTypes))
			{
				bcEventTypes = "BCPROXY";
				_eventTypes |= EventTypes.BcProxy;
			}
			else
			{
				_eventTypes = EventTypes.None;
			}

			if (bcEventTypes.IndexOf("BCMQ") >= 0)
			{
				_eventTypes |= EventTypes.BcMq;
			}

			if (bcEventTypes.IndexOf("BCCOM") >= 0)
			{
                using (Process process = Process.GetCurrentProcess())
                {
                    _eventTypes |= EventTypes.BcCom;
                    _bcCom = new BCCOMSERVERLib.BcComAtl();
                    _bcCom.SetPID(process.MainModule.ModuleName, process.Id.ToString());
                }
                _bcCom.OnBc += bcCom_OnBc;
			}

			if (bcEventTypes.IndexOf("BCPROXY") >= 0)
			{
				_eventTypes |= EventTypes.BcProxy;
                _bcProxy = new BcProxyLib.BcPrxy();
                _bcProxy.OnBcReceive += bcProxy_OnBcReceive;
			}

			if (bcEventTypes.IndexOf("ALL") >= 0)
			{
				_eventTypes |= EventTypes.BcProxy;
				_bcProxy = new BcProxyLib.BcPrxy();
                _bcProxy.OnBcReceive += bcProxy_OnBcReceive;

                using (Process process = Process.GetCurrentProcess())
                {
                    _eventTypes |= EventTypes.BcCom;
                    _bcCom = new BCCOMSERVERLib.BcComAtl();
                    _bcCom.SetPID(process.MainModule.ModuleName, process.Id.ToString());
                }
                _bcCom.OnBc += bcCom_OnBc;
			}			 
		}

        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected virtual void Dispose( bool disposing )
		{
            // Check to see if Dispose has already been called.
            if (!_disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    if (IsEventEnabled(EventTypes.BcCom))
                    {
                        _bcCom.OnBc -= bcCom_OnBc;
                        _bcCom.StopBroadcasting();
                    }

                    if (IsEventEnabled(EventTypes.BcProxy))
                    {
                        if (_bcProxy != null)
                        {
                            _bcProxy.OnBcReceive -= bcProxy_OnBcReceive;
                        }
                    }

                    // Note disposing has been done.
                    _disposed = true;
                }
			}
		}

		/// <summary>
		/// Registers a table + command + indicator if own broadcast and the application
		/// at the broadcast ATL server for filtering purposes of broadcasts.
		/// <list type="bullet">
		///		<listheader>Commands</listheader>
		///		<item>IRT</item>
		///		<item>URT</item>
		///		<item>DRT</item>
		///		<item>JOF</item>
		///		<item> .... please inform yourself about the UFIS router commands.</item>
		/// </list>
		/// </summary>
		/// <param name="table">The application registers for this database table broadcasts</param>
		/// <param name="cmd"> and for this command</param>
		/// <param name="ownBc">it incorporates own initiated broadcasts</param>
		/// <param name="appl">The application to register</param>
		/// <example>
		/// Function example:
		/// <code>
		/// [C#]
		/// RegisterObject("AFT", "DRF", true, "FIPS");
		/// </code>
		/// In this case the Table "AFTTAB" with the command "DRF" is incorporated
		/// even if the broadcast was initiated by this application and the registered
		/// application for this case is <B>FIPS</B>.
		/// </example>
		public void RegisterObject(string table,string cmd,int ownBc,string appl)
		{
			if (IsEventEnabled(EventTypes.BcProxy))
			{
				_bcProxy.RegisterObject(table,cmd,ownBc,appl);
			}

			if (IsEventEnabled(EventTypes.BcCom))
			{
				_bcCom.SetFilter(table,cmd,ownBc,appl);
			}
		}

		/// <summary>
		/// Unregisters a combination of database table and a router command from the
		/// external ATL server, which is responsible to provide broadcasts.
		/// </summary>
		/// <param name="table">The database table.</param>
		/// <param name="cmd">The router command.</param>
		public void UnregisterObject(string table,string cmd)
		{
			if (IsEventEnabled(EventTypes.BcProxy))
			{
				_bcProxy.UnregisterObject(table,cmd);
			}
		}

		/// <summary>
		/// Sets a filter range as criteria for broadcasts. An application might be interested
		/// in data for a certain time frame, let's assume from 01.01.2005 to 02.01.2005. 
		/// The application will be interested in data changes for the fields:
		/// <list type="bullet">
		///		<item>STOA for Scheduled arrival</item>
		///		<item>ETAI for estimated arrival</item>
		///		<item>ONBL for onblock</item>
		/// </list>
		/// The application will be interested in field changes explicitly for the time frame between
		/// 01.01.2005 and 02.01.2004. For this purpose the "fromValue" and "toValue" fields 
		/// must be set in the CEDA date and time format, e.g. "20050101000000" and "20050102000000"
		/// </summary>
		/// <param name="table">The database table, which contains the fields.</param>
		/// <param name="fromField">The field "From" as start of the range.</param>
		/// <param name="toField">The field "To" as end of the range.</param>
		/// <param name="fromValue">The "From" value.</param>
		/// <param name="toValue">The "To value"</param>
		/// <example>
		///		<code>
		///		[C#]
		///		SetFilterRange("AFT", "ONBL", "ONBL", "20050101000000", "20050102000000");
		///		SetFilterRange("AFT", "ETAI", "ETAI", "20050101000000", "20050102000000");
		///		SetFilterRange("AFT", "STOA", "STOA", "20050101000000", "20050102000000");
		///		</code>
		/// All the fields will be combined by an "AND" statement and if all criteria fit the
		/// broadcast data, the broadcast will be taken into account. Otherwise the external
		/// ATL broadcast server will filter out the braodcast. 
		/// </example>
		public void SetFilterRange(string table,string fromField,string toField,string fromValue,string toValue)
		{
			if (IsEventEnabled(EventTypes.BcProxy))
			{
				_bcProxy.SetFilterRange(table,fromField,toField,fromValue,toValue);
			}
		}

		/// <summary>
		/// The BcProxy provides a functionality for internal broadcast spooling. If the
		/// spooling is set, the application will not get any broadcast until the spooling
		/// is switched off. After switching off, all queued broadcasts are sent immediately
		/// to the application.
		/// </summary>
		public void SetSpoolOn()
		{
			if (IsEventEnabled(EventTypes.BcProxy))
			{
				_bcProxy.SetSpoolOn();
			}

			if (IsEventEnabled(EventTypes.BcCom))
			{
				_bcCom.StopBroadcasting();
			}
		}

		/// <summary>
		/// The BcProxy provides a functionality for internal broadcast spooling. If the
		/// spooling is set, the application will not get any broadcast until the spooling
		/// is switched off. After switching off, all queued broadcasts are sent immediately
		/// to the application.
		/// </summary>
		public void SetSpoolOff()
		{
			if (IsEventEnabled(EventTypes.BcProxy))
			{
				_bcProxy.SetSpoolOff();
			}

			if (IsEventEnabled(EventTypes.BcCom))
			{
				_bcCom.StartBroadcasting();
				_bcCom.TransmitFilter();
			}
		}


		private bool IsEventEnabled(EventTypes type)
		{
			return ((_eventTypes & type) == type);
		}

		private void bcProxy_OnBcReceive(string ReqId, string Dest1, string Dest2, string Cmd, string Object, string Seq, string Tws, string Twe, string Selection, string Fields, string Data, string BcNum)
		{
            OnBroadcastReceived(new BroadcastEventArgs()
            {
                BroadcastMessage = new CedaBroadcastMessage()
                {
                    RequestId = ReqId,
                    Destination1 = Dest1,
                    Destination2 = Dest2,
                    Command = Cmd,
                    Object = Object,
                    Sequence = Seq,
                    Tws = Tws,
                    Twe = Twe,
                    Selection = Selection,
                    Fields = Fields,
                    Data = Data,
                    BroadcastNumber = BcNum,
                    Attachment = string.Empty,
                    Additional = string.Empty
                }
            });
		}

		private void bcCom_OnBc(int size, int currPackage, int totalPackages, string strData)
		{
			if (currPackage == 1)
			{
				_bcData.Length = 0;
				_bcData.Append(strData);
			}
			else
			{
				_bcData.Append(strData);
			}

			if (currPackage == totalPackages)
			{
				string ReqId, Dest1, Dest2, Cmd, Object, Seq, Tws, Twe, Selection, Fields, Data, BcNum,Attach;
				ReqId = GetKeyItem(_bcData.ToString(), "{=USR=}", "{="); 
				Dest1 = GetKeyItem(_bcData.ToString(), "{=USR=}", "{="); 
				Dest2 = GetKeyItem(_bcData.ToString(), "{=WKS=}", "{="); 
				if (Dest2.Length > 0)
				{
					ReqId = Dest2;
				}

				Cmd = GetKeyItem(_bcData.ToString(), "{=CMD=}", "{="); 
				Object = GetKeyItem(_bcData.ToString(), "{=TBL=}", "{="); 
				Seq = GetKeyItem(_bcData.ToString(), "{=XXX=}", "{="); 
				Tws = GetKeyItem(_bcData.ToString(), "{=TWS=}", "{="); 
				Twe = GetKeyItem(_bcData.ToString(), "{=TWE=}", "{="); 
				Selection = GetKeyItem(_bcData.ToString(), "{=WHE=}", "{="); 
				Fields = GetKeyItem(_bcData.ToString(), "{=FLD=}", "{="); 
				Data = GetKeyItem(_bcData.ToString(), "{=DAT=}", "{="); 
				BcNum = GetKeyItem(_bcData.ToString(), "{=BCNUM=}", "{="); 
				Attach = GetKeyItem(_bcData.ToString(), "{=ATTACH=}", "{=");

                OnBroadcastReceived(new BroadcastEventArgs()
                {
                    BroadcastMessage = new CedaBroadcastMessage()
                    {
                        RequestId = ReqId,
                        Destination1 = Dest1,
                        Destination2 = Dest2,
                        Command = Cmd,
                        Object = Object,
                        Sequence = Seq,
                        Tws = Tws,
                        Twe = Twe,
                        Selection = Selection,
                        Fields = Fields,
                        Data = Data,
                        BroadcastNumber = BcNum,
                        Attachment = Attach,
                        Additional = string.Empty
                    }
                });
			}
		}

        /// <summary>
        /// This method extracts the value of a key item from the string <B>pcpTextBuff</B>.
        /// You can consider this method to be a poor XML function :-)
        /// </summary>
        /// <param name="textBuff">The string, which contains some key items
        /// in a special syntax.</param>
        /// <param name="keyword">The Key word to be searched for.</param>
        /// <param name="itemEnd">The end identifier.</param>
        /// <returns>A string containing the value of the key item or empty.</returns>
        /// <example>
        /// The following example demonstrate the usage of <B>GetKeyItem</B>:
        /// <code>
        /// [C#]
        /// using Ufis.Utils;
        /// 
        /// class myClass
        /// {
        ///		string myName	= "";
        ///		string myStreet	= "";
        ///		string myCity   = "";
        ///		
        ///		// Lets assume that the string "theAdressData" contains the following:
        ///		// "&lt;NAME&gt;John Doe,&lt;STRET&gt;Elmstreet,&lt;CITY&gt;Denver"
        ///		public myClass(string theAdressData)
        ///		{
        ///			myName	 = UT.GetKeyItem(theAdressData, "&lt;NAME&gt;", "&gt;")
        ///			myStreet = UT.GetKeyItem(theAdressData, "&lt;STREET&gt;", "&gt;")
        ///			myCity	 = UT.GetKeyItem(theAdressData, "&lt;CITY&gt;", "&gt;")
        ///			// the result is myName="John Doe", myStreet="Elmstreet", myCity="Denver"
        ///		}
        /// }
        /// </code>
        /// </example>
        /// <remarks>none.</remarks>
        public static string GetKeyItem(string textBuff, string keyword, string itemEnd)
        {
            int intDataBegin = -1;
            int intDataEnd = -1;
            intDataBegin = textBuff.IndexOf(keyword);
            /* Search the keyword */
            if (intDataBegin >= 0)
            {
                /* Did we find it? Yes. */
                intDataBegin += keyword.Length;
                /* Skip behind the keyword */
                intDataEnd = textBuff.IndexOf(itemEnd, intDataBegin);
                /* Search end of data */
                if (intDataEnd == -1)
                {
                    /* End not found? */
                    return textBuff.Substring(intDataBegin);
                    /* Take the whole string */
                } /* end if */
                else
                {
                    int intDataSize = intDataEnd - intDataBegin;
                    return textBuff.Substring(intDataBegin, intDataSize);
                }
            }
            else
            {
                return string.Empty;
            }
        }
	}
}

