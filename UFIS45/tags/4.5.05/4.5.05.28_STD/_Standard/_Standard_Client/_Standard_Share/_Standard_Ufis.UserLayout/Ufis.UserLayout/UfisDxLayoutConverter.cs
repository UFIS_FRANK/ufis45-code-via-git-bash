﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Xml.Linq;
using System.Xml;

namespace Ufis.UserLayout
{
    /// <summary>
    /// Provides the functionality to convert layout stored in UFIS database 
    /// into DevExpress xml layout and vise versa.
    /// </summary>
    public class UfisDxLayoutConverter
    {
        /// <summary>
        /// Converts grid's layout stored in UFIS database into DevExpress grid's layout.
        /// </summary>
        /// <param name="ufisGridLayout">Grid's layout stored in UFIS database.</param>
        /// <returns></returns>
        public static Stream UfisToDxGridLayout(string ufisGridLayout)
        {
            Stream dxGridLayoutStream = null;

            try
            {
                XDocument ufisDoc = XDocument.Parse(ufisGridLayout);

                XDocument dxDoc = new XDocument();

                XElement elHeader = new XElement("XtraSerializer",
                    new XAttribute("version", "1.0"),
                    new XAttribute("application", "GridControl"));

                XElement elLayoutVersion = new XElement("property",
                    new XAttribute("name", "#LayoutVersion"),
                    new XAttribute("isnull", "true"));

                XElement elGridControl = new XElement("property",
                    new XAttribute("name", "$GridControl"),
                    new XAttribute("iskey", "true"),
                    new XAttribute("value", "GridControl"));

                XElement elColumns = new XElement("property",
                    new XAttribute("name", "Columns"),
                    new XAttribute("iskey", "true"));

                IEnumerable<XElement> elElements = (from c in ufisDoc.Descendants("Col")
                                                      select c);

                int i = 1;
                foreach (XElement elElement in elElements)
                {
                    string strFieldName = GetAttribute(elElement, "Field");
                    if (!string.IsNullOrEmpty(strFieldName))
                    {
                        string strName = GetAttribute(elElement, "Name", strFieldName);

                        XElement elItem = new XElement("property",
                            new XAttribute("name", string.Format("Item{0}", i++)),
                            new XAttribute("isnull", "true"),
                            new XAttribute("iskey", "true"));

                        XElement elItemProp = new XElement("property", strFieldName);
                        elItemProp.Add(new XAttribute("name", "FieldName"));
                        elItem.Add(elItemProp);

                        elItemProp = new XElement("property", strName);
                        elItemProp.Add(new XAttribute("name", "Name"));
                        elItem.Add(elItemProp);

                        string strVisibleIndex = GetAttribute(elElement, "Index");
                        if (!string.IsNullOrEmpty(strVisibleIndex))
                        {
                            elItemProp = new XElement("property", strVisibleIndex);
                            elItemProp.Add(new XAttribute("name", "VisibleIndex"));
                            elItem.Add(elItemProp);
                        }

                        string strVisible = GetAttribute(elElement, "Visible");
                        if (!string.IsNullOrEmpty(strVisible))
                        {
                            strVisible = (strVisible == "0" ? "false" : "true");

                            elItemProp = new XElement("property", strVisible);
                            elItemProp.Add(new XAttribute("name", "Visible"));
                            elItem.Add(elItemProp);
                        }

                        string strHeader = GetAttribute(elElement, "Header", strName);
                        elItemProp = new XElement("property", strHeader);
                        elItemProp.Add(new XAttribute("name", "Header"));
                        elItemProp.Add(new XAttribute("type", "System.String"));
                        elItem.Add(elItemProp);
                    
                        string strActualWidth = GetAttribute(elElement, "Width", "NaN");
                        elItemProp = new XElement("property", strActualWidth);
                        elItemProp.Add(new XAttribute("name", "ActualWidth"));
                        elItem.Add(elItemProp);

                        elColumns.Add(elItem);
                    }
                }

                elColumns.Add(new XAttribute("value", --i));
                elGridControl.Add(elColumns);

                elElements = (from c in ufisDoc.Descendants("Group")
                                    select c);
                XElement elGroup = null;
                if (elElements.Count() > 0) elGroup = elElements.First();
                string strElValue = (elGroup == null ? null : GetAttribute(elGroup, "Count"));
                int intGroupCount = (string.IsNullOrEmpty(strElValue) ? 0 : int.Parse(strElValue));
                XElement elFooter = new XElement("property", intGroupCount);
                elFooter.Add(new XAttribute("name", "GroupCount"));
                elGridControl.Add(elFooter);

                elFooter = new XElement("property", 
                    new XAttribute("name", "TotalSummary"),
                    new XAttribute("iskey", true),
                    new XAttribute("value", 0));
                elGridControl.Add(elFooter);

                elFooter = new XElement("property",
                    new XAttribute("name", "GroupSummary"),
                    new XAttribute("iskey", true),
                    new XAttribute("value", 0));
                elGridControl.Add(elFooter);


                elFooter = new XElement("property",
                    new XAttribute("name", "SortInfo"),
                    new XAttribute("iskey", true));

                elElements = (from c in ufisDoc.Descendants("Sort")
                                select c);                
                i = 1;
                foreach (XElement xElement in elElements)
                {
                    string strFieldName = GetAttribute(xElement, "Field");
                    if (!string.IsNullOrEmpty(strFieldName))
                    {
                        string strOrder = GetAttribute(xElement, "Order", "1");
                        strOrder = (strOrder == "0" ? "Descending" : "Ascending");

                        XElement elItem = new XElement("property",
                            new XAttribute("name", string.Format("Item{0}", i++)),
                            new XAttribute("isnull", "true"),
                            new XAttribute("iskey", "true"));

                        XElement elItemProp = new XElement("property", strOrder);
                        elItemProp.Add(new XAttribute("name", "SortOrder"));
                        elItem.Add(elItemProp);

                        elItemProp = new XElement("property", strFieldName);
                        elItemProp.Add(new XAttribute("name", "FieldName"));
                        elItem.Add(elItemProp);

                        elFooter.Add(elItem);
                    }
                }

                elFooter.Add(new XAttribute("value", --i));
                elGridControl.Add(elFooter);

                elFooter = new XElement("property", false);
                elFooter.Add(new XAttribute("name", "ShowLoadingPanel"));
                elGridControl.Add(elFooter);

                elFooter = new XElement("property",
                    new XAttribute("name", "MRUFilters"),
                    new XAttribute("iskey", true),
                    new XAttribute("value", 0));
                elGridControl.Add(elFooter);

                elElements = (from c in ufisDoc.Descendants("Filter")
                                select c);
                XElement elFilter = null;
                if (elElements.Count() > 0) elFilter = elElements.First();
                if (elFilter != null)
                {
                    strElValue = GetAttribute(elFilter, "String");
                    if (!string.IsNullOrEmpty(strElValue))
                    {
                        elFooter = new XElement("property", strElValue);
                        elFooter.Add(new XAttribute("name", "FilterString"));
                        elGridControl.Add(elFooter);

                        elFooter = new XElement("property", true);
                        elFooter.Add(new XAttribute("name", "IsFilterEnabled"));
                        elGridControl.Add(elFooter);
                    }
                }

                elFooter = new XElement("property",
                    new XAttribute("name", "GroupSummarySortInfo"),
                    new XAttribute("iskey", true),
                    new XAttribute("value", 0));
                elGridControl.Add(elFooter);

                elHeader.Add(elLayoutVersion);
                elHeader.Add(elGridControl);

                dxDoc.Add(elHeader);

                dxGridLayoutStream = XDocToStream(dxDoc);
            }
            catch
            {
                dxGridLayoutStream = null;
            }

            return dxGridLayoutStream;
        }

        /// <summary>
        /// Converts DevExpress grid's layout into UFIS database grid's layout.
        /// </summary>
        /// <param name="dxGridLayout">DevExpress grid's layout.</param>
        /// <returns></returns>
        public static string DxToUfisGridLayout(Stream dxGridLayout)
        {
            string strUfisGridLayout = null;

            try
            {
                dxGridLayout.Position = 0;
                XDocument dxDoc = XDocument.Load(dxGridLayout);

                XDocument ufisDoc = new XDocument();                
            
                XElement elGrid = new XElement("Grid");

                IEnumerable<XElement> elElements = (from c in dxDoc.Descendants("property")
                                                      where (string)c.Attribute("name") == "Columns"
                                                      select c);

                foreach (XElement elElement in elElements)
                {
                    IEnumerable<XElement> elItems = (from item in elElement.Descendants("property")
                                                        where ((string)item.Attribute("name")).StartsWith("Item")
                                                        select item);
                    foreach (XElement elItem in elItems)
                    {
                        XElement elCol = new XElement("Col");

                        IEnumerable<XElement> elProps = elItem.Descendants("property");
                        string strFieldName = null;
                        string strName = null;
                        foreach (XElement elProp in elProps)
                        {                            
                            string strPropName = GetAttribute(elProp, "name");
                            string strValue = elProp.Value;

                            switch (strPropName)
                            {
                                case "FieldName":
                                    strPropName = "Field";
                                    strFieldName = strValue;
                                    break;
                                case "Name":
                                    strName = strValue;
                                    if (strValue == strFieldName) strValue = null;
                                    break;
                                case "VisibleIndex":
                                    strPropName = "Index";
                                    break;
                                case "Visible":
                                    strValue = (bool.Parse(strValue) ? null : "0");
                                    break;
                                case "Header":
                                    if (strValue == strName) strValue = null;
                                    break;
                                case "Width":
                                    strValue = null;
                                    break;
                                case "ActualWidth": 
                                    strPropName = "Width";                                    
                                    if (strValue == "NaN") strValue = null;
                                    break;
                            }

                            if (strValue != null)
                            {
                                XAttribute atProp = new XAttribute(strPropName, strValue);
                                elCol.Add(atProp);
                            }
                        }
                        elGrid.Add(elCol);
                    }
                }

                elElements = (from c in dxDoc.Descendants("property")
                              where (string)c.Attribute("name") == "GroupCount"
                              select c);
                XElement elGroup = null;
                if (elElements.Count() > 0) elGroup = elElements.First();
                string strElValue = (elGroup == null ? null : elGroup.Value);
                int intGroupCount = (string.IsNullOrEmpty(strElValue) ? 0 : int.Parse(strElValue));
                if (intGroupCount > 0)
                {
                    XElement elFooter = new XElement("Group",
                        new XAttribute("Count", intGroupCount));
                    elGrid.Add(elFooter);
                }

                elElements = (from c in dxDoc.Descendants("property")
                              where (string)c.Attribute("name") == "SortInfo"
                              select c);
                foreach (XElement elElement in elElements)
                {
                    IEnumerable<XElement> elItems = (from item in elElement.Descendants("property")
                                                     where ((string)item.Attribute("name")).StartsWith("Item")
                                                     select item);

                    foreach (XElement elItem in elItems)
                    {
                        XElement elSort = new XElement("Sort");

                        IEnumerable<XElement> elProps = elItem.Descendants("property");
                        foreach (XElement elProp in elProps)
                        {
                            string strPropName = GetAttribute(elProp, "name");
                            string strValue = elProp.Value;

                            switch (strPropName)
                            {
                                case "FieldName":
                                    strPropName = "Field";
                                    break;
                                case "SortOrder":
                                    strPropName = "Order";
                                    strValue = (strValue == "Descending" ? "0" : null);
                                    break;
                            }

                            if (strValue != null)
                            {
                                XAttribute atProp = new XAttribute(strPropName, strValue);
                                elSort.Add(atProp);
                            }
                        }
                        elGrid.Add(elSort);
                    }
                }

                elElements = (from c in dxDoc.Descendants("property")
                              where (string)c.Attribute("name") == "FilterString"
                              select c);
                XElement elFilter = null;
                if (elElements.Count() > 0) elFilter = elElements.Last();
                strElValue = (elFilter == null ? null : elFilter.Value);
                if (!string.IsNullOrEmpty(strElValue))
                {
                    XElement elFooter = new XElement("Filter",
                        new XAttribute("String", strElValue));
                    elGrid.Add(elFooter);
                }
                
                ufisDoc.Add(elGrid);

                strUfisGridLayout = ufisDoc.ToString(SaveOptions.DisableFormatting);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                strUfisGridLayout = null;
            }

            return strUfisGridLayout;
        }

        /// <summary>
        /// Converts XDocument into stream.
        /// </summary>
        /// <param name="doc">XDocument object.</param>
        /// <returns></returns>
        private static Stream XDocToStream(XDocument doc)
        {
            MemoryStream stream = new MemoryStream();
            XmlWriterSettings xws = new XmlWriterSettings() { OmitXmlDeclaration = true, Indent = true };

            using (XmlWriter xw = XmlWriter.Create(stream, xws))
            {
                doc.WriteTo(xw);
            }

            if (stream != null) stream.Position = 0;
            return stream;
        }

        /// <summary>
        /// Get XElement's attribute value.
        /// </summary>
        /// <param name="el">XElement object.</param>
        /// <param name="name">Atrribute name.</param>
        /// <returns>Attribute value, or null if no match attribute name found.</returns>
        private static string GetAttribute(XElement el, string name)
        {
            return GetAttribute(el, name, null);
        }

        /// <summary>
        /// Get XElement's attribute value.
        /// </summary>
        /// <param name="el">XElement object.</param>
        /// <param name="name">Atrribute name.</param>
        /// <param name="defValue">Default value to return if no match attribute name found.</param>
        /// <returns>Attribute value, or defValue if no match attribute name found.</returns>
        private static string GetAttribute(XElement el, string name, string defValue)
        {
            XAttribute xAttr = el.Attribute(name);
            if (xAttr == null)
                return defValue;
            else
                return xAttr.Value;
        }
    }
}
