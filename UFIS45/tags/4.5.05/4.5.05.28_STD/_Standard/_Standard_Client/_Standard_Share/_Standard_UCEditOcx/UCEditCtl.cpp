// UCEditCtl.cpp : Implementation of the CUCEditCtrl ActiveX Control class.
#define UNICODE
#include "stdafx.h"
#include "UCEdit.h"
#include "UCEditCtl.h"
#include "UCEditPpg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CUCEditCtrl, COleControl)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CUCEditCtrl, COleControl)
	//{{AFX_MSG_MAP(CUCEditCtrl)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
	ON_MESSAGE(OCM_COMMAND, OnOcmCommand)
	ON_OLEVERB(AFX_IDS_VERB_EDIT, OnEdit)
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Dispatch map

BEGIN_DISPATCH_MAP(CUCEditCtrl, COleControl)
	//{{AFX_DISPATCH_MAP(CUCEditCtrl)
	DISP_FUNCTION(CUCEditCtrl, "RetrieveUCText", RetrieveUCText, VT_UNKNOWN, VTS_NONE)
	DISP_FUNCTION(CUCEditCtrl, "AssignUCText", AssignUCText, VT_EMPTY, VTS_UNKNOWN)
	DISP_FUNCTION(CUCEditCtrl, "SetUCText", SetUCText, VT_EMPTY, VTS_UNKNOWN VTS_I2)
	DISP_FUNCTION(CUCEditCtrl, "GetUCText", GetUCText, VT_I2, VTS_UNKNOWN)
	DISP_STOCKPROP_READYSTATE()
	//}}AFX_DISPATCH_MAP
	DISP_FUNCTION_ID(CUCEditCtrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
END_DISPATCH_MAP()


/////////////////////////////////////////////////////////////////////////////
// Event map

BEGIN_EVENT_MAP(CUCEditCtrl, COleControl)
	//{{AFX_EVENT_MAP(CUCEditCtrl)
	// NOTE - ClassWizard will add and remove event map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	EVENT_STOCK_READYSTATECHANGE()
	//}}AFX_EVENT_MAP
END_EVENT_MAP()


/////////////////////////////////////////////////////////////////////////////
// Property pages

// TODO: Add more property pages as needed.  Remember to increase the count!
BEGIN_PROPPAGEIDS(CUCEditCtrl, 1)
	PROPPAGEID(CUCEditPropPage::guid)
END_PROPPAGEIDS(CUCEditCtrl)


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CUCEditCtrl, "UCEDIT.UCEditCtrl.1",
	0xaadde249, 0x609a, 0x46b9, 0xb0, 0x8d, 0x88, 0xb2, 0x24, 0xfd, 0x45, 0xf3)


/////////////////////////////////////////////////////////////////////////////
// Type library ID and version

IMPLEMENT_OLETYPELIB(CUCEditCtrl, _tlid, _wVerMajor, _wVerMinor)


/////////////////////////////////////////////////////////////////////////////
// Interface IDs

const IID BASED_CODE IID_DUCEdit =
		{ 0x24078785, 0x1d87, 0x4cf6, { 0x8a, 0x58, 0x36, 0x97, 0xc4, 0xfe, 0x32, 0x6a } };
const IID BASED_CODE IID_DUCEditEvents =
		{ 0xdafa3a40, 0x63cc, 0x43e3, { 0xa5, 0xc0, 0x66, 0xf7, 0x1e, 0xf1, 0x52, 0x2e } };


/////////////////////////////////////////////////////////////////////////////
// Control type information

static const DWORD BASED_CODE _dwUCEditOleMisc =
	OLEMISC_SIMPLEFRAME |
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CUCEditCtrl, IDS_UCEDIT, _dwUCEditOleMisc)


/////////////////////////////////////////////////////////////////////////////
// CUCEditCtrl::CUCEditCtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CUCEditCtrl

BOOL CUCEditCtrl::CUCEditCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Verify that your control follows apartment-model threading rules.
	// Refer to MFC TechNote 64 for more information.
	// If your control does not conform to the apartment-model rules, then
	// you must modify the code below, changing the 6th parameter from
	// afxRegInsertable | afxRegApartmentThreading to afxRegInsertable.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_UCEDIT,
			IDB_UCEDIT,
			afxRegInsertable | afxRegApartmentThreading,
			_dwUCEditOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}


/////////////////////////////////////////////////////////////////////////////
// CUCEditCtrl::CUCEditCtrl - Constructor

CUCEditCtrl::CUCEditCtrl()
{
	InitializeIIDs(&IID_DUCEdit, &IID_DUCEditEvents);

	EnableSimpleFrame();

	m_lReadyState = READYSTATE_LOADING;
	// TODO: Call InternalSetReadyState when the readystate changes.

	// TODO: Initialize your control's instance data here.
	plByteArr = NULL;
	slText="";
	BYTE byteArr[6]={14,31,14,36,14,42};
	//AssignUCText( (LPUNKNOWN) byteArr );
	SetUCText( (LPUNKNOWN) byteArr , 6);
}


/////////////////////////////////////////////////////////////////////////////
// CUCEditCtrl::~CUCEditCtrl - Destructor

CUCEditCtrl::~CUCEditCtrl()
{
	// TODO: Cleanup your control's instance data here.
}


/////////////////////////////////////////////////////////////////////////////
// CUCEditCtrl::OnDraw - Drawing function

void CUCEditCtrl::OnDraw(
			CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	//DoSuperclassPaint(pdc, rcBounds);
	pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(WHITE_BRUSH)));
	COleControl::SetAppearance(1);
	COleControl::SetBorderStyle(1);
	COleControl::DoSuperclassPaint(pdc, rcBounds);
}


/////////////////////////////////////////////////////////////////////////////
// CUCEditCtrl::DoPropExchange - Persistence support

void CUCEditCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: Call PX_ functions for each persistent custom property.

}


/////////////////////////////////////////////////////////////////////////////
// CUCEditCtrl::OnResetState - Reset control to default state

void CUCEditCtrl::OnResetState()
{
	COleControl::OnResetState();  // Resets defaults found in DoPropExchange

	// TODO: Reset any other control state here.
}


/////////////////////////////////////////////////////////////////////////////
// CUCEditCtrl::AboutBox - Display an "About" box to the user

void CUCEditCtrl::AboutBox()
{
	CDialog dlgAbout(IDD_ABOUTBOX_UCEDIT);
	dlgAbout.DoModal();
}


/////////////////////////////////////////////////////////////////////////////
// CUCEditCtrl::PreCreateWindow - Modify parameters for CreateWindowEx

BOOL CUCEditCtrl::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.lpszClass = _T("EDIT");
	
	return COleControl::PreCreateWindow(cs);
}


/////////////////////////////////////////////////////////////////////////////
// CUCEditCtrl::IsSubclassedControl - This is a subclassed control

BOOL CUCEditCtrl::IsSubclassedControl()
{
	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CUCEditCtrl::OnOcmCommand - Handle command messages

LRESULT CUCEditCtrl::OnOcmCommand(WPARAM wParam, LPARAM lParam)
{
#ifdef _WIN32
	WORD wNotifyCode = HIWORD(wParam);
#else
	WORD wNotifyCode = HIWORD(lParam);
#endif

	// TODO: Switch on wNotifyCode here.

	return 0;
}


/////////////////////////////////////////////////////////////////////////////
// CUCEditCtrl message handlers

void CUCEditCtrl::DoDataExchange(CDataExchange* pDX) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	COleControl::DoDataExchange(pDX);
}


BOOL CUCEditCtrl::OnGetPredefinedValue(DISPID dispid, DWORD dwCookie, VARIANT* lpvarOut) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return COleControl::OnGetPredefinedValue(dispid, dwCookie, lpvarOut);
}

LPUNKNOWN CUCEditCtrl::RetrieveUCText() 
{
	// TODO: Add your dispatch handler code here

	plByteArr = NULL;
	//CString slText = COleControl::GetText();
	CString slText = COleControl::InternalGetText();
	//MessageBox("text:" + slText );
	if (slText.IsEmpty())
	{
		
	}
	else
	{
		int cnt = slText.GetLength();
		//MessageBox( (CString) cnt );
		plByteArr=NULL;
		plByteArr = new BYTE[cnt*2];
		int curCnt = 0;
		for (int i=0; i<cnt; i++)
		{
			TCHAR ch = slText.GetAt(i);
			BYTE lowByte = ch && 0x00FF;
			BYTE hiByte = (ch && 0xFF00) >> 8;
			plByteArr[curCnt++] = hiByte;
			plByteArr[curCnt++] = lowByte;
			MessageBox((CString) ch);
		}
	}
	MessageBox(_T("byte") );
	return (LPUNKNOWN) plByteArr;
}

void CUCEditCtrl::AssignUCText(LPUNKNOWN pupByteArr) 
{
	// TODO: Add your dispatch handler code here
	//COleControl::SetText((unsigned short*)pupByteArr);
	//return;
	slText.Empty();

	if (pupByteArr!=NULL){
		BYTE* plByteArr = (BYTE*) pupByteArr;
		int cnt = sizeof(pupByteArr)/sizeof(BYTE);
		//st = new wchar_t[cnt/2];
		if (cnt<1)
		{
			slText.Empty();
		}
		else
		{
			if ((cnt%2)!=0) cnt--;
			int curCnt=0;
			
			for (int i=0; i<cnt; i=i+2)
			{
				int hiByte = plByteArr[i];
				int lowByte = plByteArr[i+1];
				TCHAR ch = (hiByte << 8) + lowByte;
				//st[curCnt] = ch;
				
				slText.Insert(curCnt, ch);
				curCnt++;
				//MessageBox(_T("Cnt"));
				
			}
		}
	}
	//BSTR b;
	//LPSTR p = (LPSTR) slText;
	//::MultiByteToWideChar( CP_ACP,
	//	0,
	//	p,
	//	0,
	//	b,
	//	0);
	//USES_CONVERSION
	//COleControl::SetText( slText.AllocSysString() );
	//COleControl::SetText((unsigned short*) T2W(slText) );
	//COleControl::SetText( b);
	//COleControl::SetText( st );
	//COleControl::Refresh();
	//COleControl::OnTextChanged();
	//COleControl::OnFocus();
	//COleControl::SetText( slText);
	COleControl::OnSetText(0, (LPARAM)slText.GetBuffer(10));
	//COleControl::InvalidateControl();
	//COleControl::Refresh();
	//COleControl::FireClick();
	//COleControl::PreModalDialog();
	//COleControl::SetFocus();
	//COleControl::InternalSetReadyState(READYSTATE_COMPLETE);
}

void CUCEditCtrl::SetUCText(LPUNKNOWN pupByteArr, short spSize) 
{
	// TODO: Add your dispatch handler code here
	slText.Empty();

	if (pupByteArr!=NULL){
		BYTE* plByteArr = (BYTE*) pupByteArr;
		if (spSize<0) spSize = 0;
		int cnt = spSize/sizeof(BYTE);
		if (cnt<1)
		{
			slText.Empty();
		}
		else
		{
			if ((cnt%2)!=0) cnt--;
			int curCnt=0;
			
			for (int i=0; i<cnt; i=i+2)
			{
				int hiByte = plByteArr[i];
				int lowByte = plByteArr[i+1];
				TCHAR ch = (hiByte << 8) + lowByte;
				
				slText.Insert(curCnt, ch);
				curCnt++;
			}
		}
	}
	COleControl::OnSetText(0, (LPARAM)slText.GetBuffer(10));

}



short CUCEditCtrl::GetUCText(LPUNKNOWN pupByteArr) 
{
	// TODO: Add your dispatch handler code here
	short size = 0;
	plByteArr = NULL;
	//CString slText = COleControl::GetText();
	CString slText = COleControl::InternalGetText();
	if (slText.IsEmpty())
	{
		
	}
	else
	{
		int cnt = slText.GetLength();
		plByteArr=NULL;
		plByteArr = (BYTE*)pupByteArr;
		size = cnt*2;
		//plByteArr = new BYTE[size];
		int curCnt = 0;
		for (int i=0; i<cnt; i++)
		{
			TCHAR ch = slText.GetAt(i);
			BYTE lowByte = ch & 0x00FF;
			BYTE hiByte = (ch & 0xFF00) >> 8;
			plByteArr[curCnt++] = hiByte;
			plByteArr[curCnt++] = lowByte;
		}
	}
	pupByteArr = (LPUNKNOWN) plByteArr;
	return size;
}
