#if !defined(AFX_UFISCOMCTL_H__A2F31EA3_C74F_11D3_A251_00500437F607__INCLUDED_)
#define AFX_UFISCOMCTL_H__A2F31EA3_C74F_11D3_A251_00500437F607__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// UfisComCtl.h : Declaration of the CUfisComCtrl ActiveX Control class.

/////////////////////////////////////////////////////////////////////////////
// CUfisComCtrl : See UfisComCtl.cpp for implementation.


#include <UfisSocket.h>
#include <URecSet.h>
#include <Adodc.h>

#ifndef RC_SUCCESS
#define RC_SUCCESS 0 
#endif
#ifndef RC_FAIL
#define RC_FAIL    		(-1)
#endif
#ifndef RC_COMM_FAIL
#define RC_EMPTY_MSG	(1)
#define RC_COMM_FAIL    (-2)
#define RC_INIT_FAIL    (-3)
#define RC_CEDA_FAIL    (-4)
#define RC_SHUTDOWN     (-5)
#define RC_ALREADY_INIT	(-6)
#define RC_NOT_FOUND	(-7)
#define RC_DATA_CUT		(-8)
#define RC_TIMEOUT		(-9)
#define RC_ALREADY_CONNECTED	(-10)
#define RC_MISSING_ANSWER		(-11)
#define RC_BUSY			(-31)
#define RC_TO_MANY_APP  (-32)
#endif


#define DATAPACKETLEN 			(1024-12)     

#define NETREAD_SIZE 			(0x8000)
#define PACKET_LEN     			(1024)  


#define		NET_SHUTDOWN	(0x1)
#define		NET_DATA		(0x2)
#define		PACKET_DATA		(-100)
#define		PACKET_START	(-200)
#define		PACKET_END		(-300)

typedef struct {
	long	msg_no;
	long	prio;
	char	data[1]; // 4 Strings: Flight_no, rot_no, ref_fld, msg_data
} MSGBLK;


typedef struct {
	char		dest_name[10];		// user login name
	char		orig_name[10];		// filter (BC) or timeout for netin
	char		recv_name[10];		// WKS-Name
	short		bc_num;				// serial number of broadcast 1-30000 
	char		seq_id[10];			// WKS-Name (obsolete)
	short		tot_buf;			// Count BC-Parts
	short		act_buf;			// actual BC PartNo
	char		ref_seq_id[10];		// not used
	short		rc;					// return code, mostly useless
	short		tot_size;			// total bytes of BC (data only)
	short		cmd_size;			// bytes of this part
	short		data_size;			// not used
	char		data[1];			// data, cmdblk
}BC_HEAD;

typedef struct {
	char		command[6];			// router command
	char		obj_name[33];		// object name (table name)
	char		order[2];			// not used
	char		tw_start[33];		// used for application data
	char		tw_end[33];			// hopo,tabend,modul name,modul version
 	char		data[1];			// data (selection,fields,data)
}CMDBLK;
          
typedef	struct {
		short	command;			//
		DWORD	length;				// 
		char	data[1];			//
}COMMIF;

struct BcStruct
{
	BOOL	IsValid;
	time_t	BcReceived;
	BC_HEAD	BcHead;
	CMDBLK	CmdBlk;
	char    *BcData;
	struct BcStruct *NextBc;
};



class CUfisComCtrl : public COleControl
{
	DECLARE_DYNCREATE(CUfisComCtrl)

// Constructor
public:
	CUfisComCtrl();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUfisComCtrl)
	public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	//}}AFX_VIRTUAL

	UfisSocket* pomCedaSock;
	URecSet omRS; 
	CDatabase omDB;
	CAdodc *pomAdoCtrl;

	// For ODBC Connects
	HENV	hEnv;
	HDBC	hDbConn;
	HSTMT	hStmt;

	bool	isOdbcInit;
	CString omXUser;
// Implementation
protected:
	~CUfisComCtrl();

	DECLARE_OLECREATE_EX(CUfisComCtrl)    // Class factory and guid
	DECLARE_OLETYPELIB(CUfisComCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CUfisComCtrl)     // Property page IDs
	DECLARE_OLECTLTYPE(CUfisComCtrl)		// Type name and misc status


	int CallCeda(char * req_id,		/*  Wird von Toolbook vergeben  */
				char * dest1,     	/*  Im Regelfall CEDA			*/
				char * dest2,     	/*  Ergebniss Empfaenger		*/
				char * cmd,       	/*  Kommando (RT,....)         	*/
				char * object,   	/*  Object fuer Kommando (bei RT: TableName)*/
				char * seq,       	/*  Sortierkriterium			*/
				char * tws,       	/*  Time Window start   		*/
				char * twe,       	/*  Time Window end				*/
				char * selection, 	/*  (Bsp: WHERE CARR = 'LH')	*/
				char * fields,    	/*  (Bsp: AC2,AC3,CARR  )		*/
				char * data,
				char * timeout);      	/*  ( )                			*/
	
	DWORD CalcBufLen (char * selection, char * fields, char * data);

	int CallRecordSet(CString opAction, CString opObject, CString opFields, CString opWhere, CString opData);

	int ReceiveUfisData(BC_HEAD  **Pbc_head, DWORD *buflen);

	bool MakeRow(char *pclDataBuf, short spCommand, int ipLength,BOOL bpFirstLine);
	bool CUfisComCtrl::AddToList(CString& olList,char *pcpActData,int *pipLen);

	CStringArray omDataArray;

	CStringArray omCurrFields;

	bool	bmUseIpAddress;
	bool	bmIsFromCallServer;
	bool	bmStayConnected;
	bool	bmIsConnected;

	bool InitAdo();

	void	GetVersionInfo();

// Message maps
	//{{AFX_MSG(CUfisComCtrl)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMove(int x, int y);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Dispatch maps
	//{{AFX_DISPATCH(CUfisComCtrl)
	CString omServerName;
	afx_msg void OnServerNameChanged();
	CString omAccessMethod;
	afx_msg void OnAccessMethodChanged();
	CString omWorkStation;
	afx_msg void OnWorkStationChanged();
	CString omUserName;
	afx_msg void OnUserNameChanged();
	CString omTableExt;
	afx_msg void OnTableExtChanged();
	CString omHomeAirport;
	afx_msg void OnHomeAirportChanged();
	CString m_reqid;
	afx_msg void OnReqidChanged();
	CString m_dest1;
	afx_msg void OnDest1Changed();
	CString m_dest2;
	afx_msg void OnDest2Changed();
	CString m_cmd;
	afx_msg void OnCmdChanged();
	CString m_object;
	afx_msg void OnObjectChanged();
	CString m_seq;
	afx_msg void OnSeqChanged();
	CString m_tws;
	afx_msg void OnTwsChanged();
	CString m_twe;
	afx_msg void OnTweChanged();
	CString m_origName;
	afx_msg void OnOrigNameChanged();
	CString m_selection;
	afx_msg void OnSelectionChanged();
	CString m_fields;
	afx_msg void OnFieldsChanged();
	CString m_version;
	afx_msg void OnVersionChanged();
	CString m_module;
	afx_msg void OnModuleChanged();
	CString m_lastErrorMessage;
	afx_msg void OnLastErrorMessageChanged();
	CString m_buildDate;
	afx_msg void OnBuildDateChanged();
	afx_msg BSTR GetXUser();
	afx_msg void SetXUser(LPCTSTR lpszNewValue);
	afx_msg BOOL GetStayConnected();
	afx_msg void SetStayConnected(BOOL bNewValue);
	afx_msg BOOL InitCom(LPCTSTR ServerName, LPCTSTR AccessMethod);
	afx_msg BOOL CleanupCom();
	afx_msg BSTR GetWorkstationName();
	afx_msg BOOL SetCedaPerameters(LPCTSTR UserName, LPCTSTR HomeAirport, LPCTSTR TableExt);
	afx_msg BSTR Ufis(LPCTSTR Command, LPCTSTR Object, LPCTSTR Fields, LPCTSTR Where, LPCTSTR Data);
	afx_msg long GetBufferCount();
	afx_msg BSTR GetBufferLine(long BufferIndex);
	afx_msg BSTR GetDataBuffer(BOOL WithClear);
	afx_msg void ClearDataBuffer();
	afx_msg short CallServer(LPCTSTR Command, LPCTSTR Object, LPCTSTR Fields,  LPCTSTR Data, LPCTSTR Where, LPCTSTR Timout);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()

	afx_msg void AboutBox();

// Event maps
	//{{AFX_EVENT(CUfisComCtrl)
	//}}AFX_EVENT
	DECLARE_EVENT_MAP()

// Dispatch and event IDs
public:
	enum {
	//{{AFX_DISP_ID(CUfisComCtrl)
	dispidServerName = 1L,
	dispidAccessMethod = 2L,
	dispidWorkStation = 3L,
	dispidUserName = 4L,
	dispidTableExt = 5L,
	dispidHomeAirport = 6L,
	dispidXUser = 22L,
	dispidReqid = 7L,
	dispidDest1 = 8L,
	dispidDest2 = 9L,
	dispidCmd = 10L,
	dispidObject = 11L,
	dispidSeq = 12L,
	dispidTws = 13L,
	dispidTwe = 14L,
	dispidOrigName = 15L,
	dispidSelection = 16L,
	dispidFields = 17L,
	dispidVersion = 18L,
	dispidModule = 19L,
	dispidLastErrorMessage = 20L,
	dispidBuildDate = 21L,
	dispidStayConnected = 23L,
	dispidInitCom = 24L,
	dispidCleanupCom = 25L,
	dispidGetWorkstationName = 26L,
	dispidSetCedaPerameters = 27L,
	dispidUfis = 28L,
	dispidGetBufferCount = 29L,
	dispidGetBufferLine = 30L,
	dispidGetDataBuffer = 31L,
	dispidClearDataBuffer = 32L,
	dispidCallServer = 33L,
	//}}AFX_DISP_ID
	};
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UFISCOMCTL_H__A2F31EA3_C74F_11D3_A251_00500437F607__INCLUDED)
