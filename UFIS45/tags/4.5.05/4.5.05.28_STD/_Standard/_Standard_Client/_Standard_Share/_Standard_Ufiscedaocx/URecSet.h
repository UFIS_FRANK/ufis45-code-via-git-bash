#if !defined(AFX_URECSET_H__09CA068E_C8F3_11D3_A254_00500437F607__INCLUDED_)
#define AFX_URECSET_H__09CA068E_C8F3_11D3_A254_00500437F607__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// URecSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// URecSet recordset

class URecSet : public CRecordset
{
public:
	URecSet(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(URecSet)

// Field/Param Data
	//{{AFX_FIELD(URecSet, CRecordset)
	CString	m_ADDI;
	CString	m_CONT;
	CString	m_DATR;
	CString	m_DEFA;
	CString	m_DEPE;
	CString	m_FELE;
	CString	m_FINA;
	CString	m_FITY;
	CString	m_FMTS;
	CString	m_GCFL;
	CString	m_GDSR;
	CString	m_GLFL;
	CString	m_GSTY;
	CString	m_GTYP;
	CString	m_GUID;
	CString	m_HOPO;
	CString	m_INDX;
	CString	m_KONT;
	CString	m_LABL;
	CString	m_LOGD;
	CString	m_MSGT;
	CString	m_ORIE;
	CString	m_PROJ;
	CString	m_REFE;
	CString	m_REQF;
	CString	m_SORT;
	CString	m_STAT;
	CString	m_SYST;
	CString	m_TANA;
	CString	m_TATY;
	CString	m_TYPE;
	CString	m_URNO;
	CString	m_CLSR;
	CString	m_DEFV;
	CString	m_DSCR;
	CString	m_FTAN;
	CString	m_HEDR;
	CString	m_TOLT;
	CString	m_TTYP;
	CString	m_TZON;
	//}}AFX_FIELD


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(URecSet)
	public:
	virtual CString GetDefaultConnect();    // Default connection string
	virtual CString GetDefaultSQL();    // Default SQL for Recordset
	virtual void DoFieldExchange(CFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_URECSET_H__09CA068E_C8F3_11D3_A254_00500437F607__INCLUDED_)
