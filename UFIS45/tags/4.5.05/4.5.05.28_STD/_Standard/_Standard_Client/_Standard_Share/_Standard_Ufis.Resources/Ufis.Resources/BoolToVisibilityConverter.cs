using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace Ufis.Resources
{
    /// <summary>
    /// Converts true to visible, false to hidden or collapse depending on the parameter.
    /// If no parameter defined or invalid parameter defined, false value will be converted to hidden.
    /// </summary>
    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class BoolToVisibilityConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            bool bVisible = (bool)value;
            string strFalseValue = ( parameter == null ? string.Empty : parameter.ToString() );

            if (bVisible)
                return Visibility.Visible;
            else if (strFalseValue.ToUpper() == "COLLAPSED")
                return Visibility.Collapsed;
            else
                return Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility visibility = (Visibility)value;

            return (visibility == Visibility.Visible);
        }

        #endregion
    }
}
