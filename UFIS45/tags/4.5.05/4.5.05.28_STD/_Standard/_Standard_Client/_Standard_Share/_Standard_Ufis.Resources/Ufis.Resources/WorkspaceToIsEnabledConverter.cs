using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using Ufis.MVVM.ViewModel;

namespace Ufis.Resources
{
    /// <summary>
    /// Converts workspace to IsEnabled
    /// </summary>
    [ValueConversion(typeof(WorkspaceViewModel), typeof(bool))]
    public class WorkspaceToIsEnabledConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            WorkspaceViewModel workspace = (WorkspaceViewModel)value;

            return (workspace != null);
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
