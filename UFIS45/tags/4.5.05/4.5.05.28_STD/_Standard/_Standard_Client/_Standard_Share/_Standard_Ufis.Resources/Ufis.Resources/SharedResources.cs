﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace Ufis.Resources
{
    public class SharedResources
    {
        public static ComponentResourceKey ShellBackground
        {
            get { return new ComponentResourceKey(typeof(SharedResources), "ShellBackground"); }
        }
        public static ComponentResourceKey AiportSolutionsStyle
        {
            get { return new ComponentResourceKey(typeof(SharedResources), "AiportSolutionsStyle"); }
        }
        public static ComponentResourceKey UFISLogoTemplate
        {
            get { return new ComponentResourceKey(typeof(SharedResources), "UFISLogoTemplate"); }
        }
        public static ComponentResourceKey MessageInfoTemplate
        {
            get { return new ComponentResourceKey(typeof(SharedResources), "MessageInfoTemplate"); }
        }
        public static ComponentResourceKey ShellButtonTextStyle
        {
            get { return new ComponentResourceKey(typeof(SharedResources), "ShellButtonTextStyle"); }
        }
        public static ComponentResourceKey MandatoryFieldBackground
        {
            get { return new ComponentResourceKey(typeof(SharedResources), "MandatoryFieldBackground"); }
        }
        public static ComponentResourceKey ReadOnlyFieldBackground
        {
            get { return new ComponentResourceKey(typeof(SharedResources), "ReadOnlyFieldBackground"); }
        }
        public static ComponentResourceKey SortIndexInColumnHeaderTemplate
        {
            get { return new ComponentResourceKey(typeof(SharedResources), "SortIndexInColumnHeaderTemplate"); }
        }
    }
}
