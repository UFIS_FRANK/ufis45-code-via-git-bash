using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;

namespace Ufis.Resources
{
    /// <summary>
    /// Converts multiple boolean values to single bool value with AND operator
    /// </summary>
    public class MultiBoolWithAndOperatorConverter : IMultiValueConverter
    {
        #region IValueConverter Members

        public object Convert(object[] values, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            bool bReturn = true;

            try
            {
                foreach (object objValue in values)
                {
                    bReturn = (bool)objValue;
                    if (!bReturn) break;
                }
            }
            catch
            {
                bReturn = false;
            }

            return bReturn;
        }

        public object[] ConvertBack(object value, Type[] targetTypes,
            object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
