// BcCCmdTarget.cpp: implementation of the BcCCmdTarget class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <BcCCmdTarget.h>
#include <VersionInfo.h>
#include <process.h>

static const IID IID_BcPrxyEvents = {0x68e25ff2, 0x6790, 0x11d4, {0x90, 0x3b, 0x0, 0x01, 0x02, 0x04, 0xaa, 0x51}};
static const IID DIID__IBcComAtlEvents = {0x8D66CD3E,0x38E2,0x4631,{0xB2,0x1F,0x42,0xEA,0xCB,0x28,0xAC,0x50}};

CString IDP_OLE_INIT_FAILED = "OLE initialization failed.  Make sure that the OLE libraries are the correct version.";
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

BcCCmdTarget::BcCCmdTarget(CCSBcHandle *prpBcHandle)
{
	EnableAutomation();

	this->m_bUseBcCom	= FALSE;
	this->m_bUseBcProxy = FALSE;
	this->m_bTransmitFilter = FALSE;

	char pclConfigPath[256];
	char pclTmpBuf[256];
	
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));


    ::GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV", pclTmpBuf, sizeof pclTmpBuf, pclConfigPath);
	CString olTmpBuf(pclTmpBuf);
	olTmpBuf.MakeUpper();

	if (olTmpBuf.Find("BCCOM") >= 0)
	{
		this->m_bUseBcCom = TRUE;
	}

	if (olTmpBuf.Find("BCPROXY") >= 0)
	{
		this->m_bUseBcProxy = TRUE;
	}


	prmBcHandle = prpBcHandle;

	CLSID clsid;
	HRESULT res;

	pomBcPrxyEvents = NULL;
	pDispBcPrxyEvents = NULL;
	pIUnknown_Auto = NULL;
	this->m_dwCookie = 0;

	if (this->m_bUseBcProxy)
	{
		// receiving the class-ID of BcProxy
		res = CLSIDFromProgID(L"BcProxy.BcPrxy.1",&clsid);
		if (res != S_OK)
		{
			AfxMessageBox (_T("couldn�t get clsid of BcProxy !"));
			return;
		}

		// *** creating dispatch for events
		pomBcPrxyEvents = new _IBcPrxyEvents;

		if (!pomBcPrxyEvents->CreateDispatch (clsid))
		{
			AfxMessageBox (_T("unable to create BcPrxyEvents instance !"));
			return;
		}

		pDispBcPrxyEvents = pomBcPrxyEvents->m_lpDispatch ;
		pDispBcPrxyEvents->AddRef();

		// ^^^^^ pointer to Disp. Interface of BcPrxyEvents

		//connect server to automation class
		pIUnknown_Auto = this->GetIDispatch(FALSE);
		int erg = AfxConnectionAdvise(pDispBcPrxyEvents,IID_BcPrxyEvents,pIUnknown_Auto,TRUE,&m_dwCookie);

		pomBcPrxyInstance = (IBcPrxy*)pomBcPrxyEvents;
		// ^^^^^ pointer to Disp. Interface of BcPrxyInstance
		
		CCSCedaData::smBcProxy = pomBcPrxyInstance;
	}

	pDispAtlComEvents = NULL;
	this->m_dwAtlComCookie = 0;

	if (this->m_bUseBcCom)
	{
		// receiving the class-ID of BcProxy
		res = CLSIDFromProgID(L"BcComServer.BcComAtl.1",&clsid);
		if (res != S_OK)
		{
			AfxMessageBox (_T("couldn�t get clsid of BCComServer !"));
			return;
		}

		// *** creating dispatch for events
		if (!omAtlComEvents.CreateDispatch (clsid))
		{
			AfxMessageBox (_T("unable to create BcComServerEvents instance !"));
			return;
		}

		pDispAtlComEvents = omAtlComEvents.m_lpDispatch ;
		pDispAtlComEvents->AddRef();

		// ^^^^^ pointer to Disp. Interface of BcPrxyEvents

		//connect server to automation class
		pIUnknown_Auto = this->GetIDispatch(TRUE);
		if (AfxConnectionAdvise(pDispAtlComEvents,DIID__IBcComAtlEvents,pIUnknown_Auto,TRUE,&m_dwAtlComCookie))
		{
			pomAtlComInstance = (IBcComAtl*)&omAtlComEvents;
			// ^^^^^ pointer to Disp. Interface of BcPrxyInstance

			CString olAppl = "Dummy";
			VersionInfo olVersionInfo;
			BOOL blResult = VersionInfo::GetVersionInfo(AfxGetInstanceHandle(),olVersionInfo);
			if (blResult)
			{
				olAppl = olVersionInfo.omProductName;
			}

			CString olPID;
			olPID.Format("%d", _getpid());
			pomAtlComInstance->SetPID(olAppl,olPID);
		}
	}
}

BcCCmdTarget::~BcCCmdTarget()
{
	ExitApp();
}

void BcCCmdTarget::ExitApp()
{
	if (m_dwCookie != 0)
	{
		AfxConnectionUnadvise(pDispBcPrxyEvents,IID_BcPrxyEvents,pIUnknown_Auto,TRUE,m_dwCookie);
		pDispBcPrxyEvents->Release();
		m_dwCookie = 0;
	}

	if (pomBcPrxyEvents != NULL && pomBcPrxyEvents->m_lpDispatch != NULL)
	{
		pomBcPrxyEvents->ReleaseDispatch ();
		delete pomBcPrxyEvents;
		pomBcPrxyEvents = NULL;
	}

	if (m_dwAtlComCookie != 0)
	{
		AfxConnectionUnadvise(pDispAtlComEvents,DIID__IBcComAtlEvents,pIUnknown_Auto,TRUE,m_dwAtlComCookie);
		m_dwAtlComCookie = 0;
		pDispAtlComEvents->Release();
	}

	if (omAtlComEvents.m_lpDispatch != NULL)
	{
		omAtlComEvents.ReleaseDispatch ();
	}

}


BEGIN_DISPATCH_MAP(BcCCmdTarget, CCmdTarget)
	//{{AFX_DISPATCH_MAP(BcCCmdTarget)
		DISP_FUNCTION_ID(BcCCmdTarget,"OnBcReceive",1,OnBroadcast, VT_BOOL, VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR)
		DISP_FUNCTION_ID(BcCCmdTarget,"OnLostBc"	,1,OnLostBc		, VT_I4, VTS_I4 )
		DISP_FUNCTION_ID(BcCCmdTarget,"OnLostBcEx",2,OnLostBcEx	, VT_I4, VTS_BSTR)
		DISP_FUNCTION_ID(BcCCmdTarget,"OnBc"		,3,OnBc			, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_BSTR)
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

BEGIN_INTERFACE_MAP(BcCCmdTarget, CCmdTarget)
	INTERFACE_PART(BcCCmdTarget, IID_BcPrxyEvents, Dispatch)
	INTERFACE_PART(BcCCmdTarget, DIID__IBcComAtlEvents, Dispatch)
END_INTERFACE_MAP()

BEGIN_MESSAGE_MAP(BcCCmdTarget, CCmdTarget)
	//{{AFX_MSG_MAP(BcCCmdTarget)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


//******************************************************
// Date: 24.03.2003
// Author:	MWO/RRO
// Get the broadcasts
//******************************************************
BOOL BcCCmdTarget::OnBroadcast(LPCTSTR ReqId, 
							 LPCTSTR Dest1, 
							 LPCTSTR Dest2, 
							 LPCTSTR Cmd, 
							 LPCTSTR Object,
							 LPCTSTR Seq, 
							 LPCTSTR Tws, 
							 LPCTSTR Twe, 
							 LPCTSTR Selection, 
							 LPCTSTR Fields, 
							 LPCTSTR Data, 
							 LPCTSTR BcNum)
{
	BcStruct rlBcData;
	strcpy (rlBcData.ReqId,ReqId); 
	strcpy (rlBcData.Dest1,Dest1); 
	strcpy (rlBcData.Dest2,Dest2); 
	strcpy (rlBcData.Cmd,Cmd); 
	strcpy (rlBcData.Object,Object);
	strcpy (rlBcData.Seq,Seq);
	strcpy (rlBcData.Tws,Tws);
	strcpy (rlBcData.Twe,Twe);
	strcpy (rlBcData.Selection,Selection);
	strcpy (rlBcData.Fields,Fields);
	strcpy (rlBcData.Data,Data);
	strcpy (rlBcData.BcNum,BcNum);

	long llNum;
	llNum = atol((char*)BcNum);
	if(prmBcHandle->TrafficLightCallBack != NULL)
	{
		prmBcHandle->TrafficLightCallBack(llNum, 0);
	}
	prmBcHandle->DistributeBc(rlBcData);
	if(prmBcHandle->TrafficLightCallBack != NULL)
	{
		prmBcHandle->TrafficLightCallBack(llNum, 1);
	}
	
	return TRUE;
}

void BcCCmdTarget::SetSpoolOn()
{
	if (this->m_bUseBcProxy)
	{
		pomBcPrxyInstance->SetSpoolOn();
	}

	if (this->m_bUseBcCom)
	{
		pomAtlComInstance->StopBroadcasting();
	}
}

void BcCCmdTarget::SetSpoolOff()
{
	if (this->m_bUseBcProxy)
	{
		pomBcPrxyInstance->SetSpoolOff();
	}

	if (this->m_bUseBcCom)
	{
		pomAtlComInstance->StartBroadcasting();

		if (this->m_bTransmitFilter)
		{
			this->m_bTransmitFilter = FALSE;
			pomAtlComInstance->TransmitFilter();
		}

	}
}

void BcCCmdTarget::AddTableCommand(CString opTable, CString opCommand,bool bpOwnBc,CString opAppl)
{
	if (this->m_bUseBcProxy)
	{
		pomBcPrxyInstance->RegisterObject(opTable, opCommand,bpOwnBc,opAppl);
	}

	if (this->m_bUseBcCom)
	{
		this->m_bTransmitFilter = TRUE;
		pomAtlComInstance->SetFilter(opTable,opCommand,bpOwnBc,opAppl);
	}
}

void BcCCmdTarget::RemoveTableCommand(CString opTable, CString opCommand)
{
	if (this->m_bUseBcProxy)
	{
		pomBcPrxyInstance->UnregisterObject(opTable, opCommand);
	}
}
void BcCCmdTarget::SetFilterRange(CString spTable, CString spFromField, CString spToField, CString spFromValue, CString spToValue)
{
	if (this->m_bUseBcProxy)
	{
		this->m_bTransmitFilter = TRUE;
		pomBcPrxyInstance->SetFilterRange(spTable, spFromField, spToField, spFromValue, spToValue);
	}
}

long BcCCmdTarget::OnLostBc(long ilErr)
{
	CString olMsg;
	olMsg.Format ("CEDA Socket error : %d occured",ilErr);
	AfxMessageBox(olMsg,MB_OK|MB_ICONSTOP);
	return S_OK;
}

long BcCCmdTarget::OnLostBcEx(LPCTSTR strException)
{
	CString olMsg;
	olMsg.Format ("Exception : %s occured",strException);
	AfxMessageBox(olMsg,MB_OK|MB_ICONSTOP);
	return S_OK;
}

long BcCCmdTarget::OnBc(long size,long currPackage,long totalPackages,LPCTSTR strData)
{
	if (currPackage == 1)
	{
		omData = strData;
	}
	else
	{
		omData += strData;
	}

	if (currPackage == totalPackages)
	{
		char pclResult[25000];
		char *pclDataBegin;
		long llSize;

		CString ReqId,Dest1,Dest2,Cmd,Object,Seq,Tws,Twe,Selection,Fields,Data,BcNum,Attachment;

		pclDataBegin = FastCedaConnection::GetKeyItem(pclResult,&llSize,omData.GetBuffer(0),"{=USR=}","{=",true);
		if (pclDataBegin != NULL)
		{
			ReqId = pclResult; 
		}

		pclDataBegin = FastCedaConnection::GetKeyItem(pclResult,&llSize,omData.GetBuffer(0),"{=USR=}","{=",true);
		if (pclDataBegin != NULL)
		{
			Dest1 = pclResult; 
		}

		pclDataBegin = FastCedaConnection::GetKeyItem(pclResult,&llSize,omData.GetBuffer(0),"{=WKS=}","{=",true);
		if (pclDataBegin != NULL)
		{
			Dest2 = pclResult; 
			ReqId = pclResult; 
		}

		pclDataBegin = FastCedaConnection::GetKeyItem(pclResult,&llSize,omData.GetBuffer(0),"{=CMD=}","{=",true);
		if (pclDataBegin != NULL)
		{
			Cmd = pclResult; 
		}

		pclDataBegin = FastCedaConnection::GetKeyItem(pclResult,&llSize,omData.GetBuffer(0),"{=TBL=}","{=",true);
		if (pclDataBegin != NULL)
		{
			Object = pclResult;
		}

		pclDataBegin = FastCedaConnection::GetKeyItem(pclResult,&llSize,omData.GetBuffer(0),"{=XXX=}","{=",true);
		if (pclDataBegin != NULL)
		{
			Seq = pclResult;
		}

		pclDataBegin = FastCedaConnection::GetKeyItem(pclResult,&llSize,omData.GetBuffer(0),"{=TWS=}","{=",true);
		if (pclDataBegin != NULL)
		{
			Tws = pclResult;
		}

		pclDataBegin = FastCedaConnection::GetKeyItem(pclResult,&llSize,omData.GetBuffer(0),"{=TWE=}","{=",true);
		if (pclDataBegin != NULL)
		{
			Twe = pclResult;
		}

		pclDataBegin = FastCedaConnection::GetKeyItem(pclResult,&llSize,omData.GetBuffer(0),"{=WHE=}","{=",true);
		if (pclDataBegin != NULL)
		{
			Selection = pclResult;
		}

		pclDataBegin = FastCedaConnection::GetKeyItem(pclResult,&llSize,omData.GetBuffer(0),"{=FLD=}","{=",true);
		if (pclDataBegin != NULL)
		{
			Fields = pclResult;
		}

		pclDataBegin = FastCedaConnection::GetKeyItem(pclResult,&llSize,omData.GetBuffer(0),"{=DAT=}","{=",false);
		if (pclDataBegin != NULL)
		{
			char tmp = pclDataBegin[llSize];
			pclDataBegin[llSize] = '\0';
			Data = pclDataBegin;
			pclDataBegin[llSize] = tmp;
		}

		long llNum = -1;
		pclDataBegin = FastCedaConnection::GetKeyItem(pclResult,&llSize,omData.GetBuffer(0),"{=BCNUM=}","{=",true);
		if (pclDataBegin != NULL)
		{
			BcNum = pclResult;
			llNum = atol(pclResult);
		}

		pclDataBegin = FastCedaConnection::GetKeyItem(pclResult,&llSize,omData.GetBuffer(0),"{=ATTACH=}","{=\\ATTACH=}",false);
		if (pclDataBegin != NULL)
		{
			char tmp = pclDataBegin[llSize];
			pclDataBegin[llSize] = '\0';
			Attachment = pclDataBegin;
			pclDataBegin[llSize] = tmp;

		}

		if(prmBcHandle->TrafficLightCallBack != NULL)
		{
			prmBcHandle->TrafficLightCallBack(llNum, 0);
		}

		prmBcHandle->ProcessBroadcast(ReqId,Dest1,Dest2,Cmd,Object,Seq,Tws,Twe,Selection,Fields,Data,BcNum,Attachment,"");

		if(prmBcHandle->TrafficLightCallBack != NULL)
		{
			prmBcHandle->TrafficLightCallBack(llNum, 1);
		}
	}

	return S_OK;
}

