// RecordSet.cpp: implementation of the RecordSet class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <RecordSet.h>
#include <CCSCedaData.h>
//#include "BasicData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif





void ExtractItemListTest(CString &opSubString, CStringArray *popStrArray, char cpTrenner = ',')
{
	CString olText;
	bool blEnd = false;
	int ilPos = 0;	
	CCSCedaData olCedaData;

	if(!opSubString.IsEmpty())
	{
		int pos;

		while(blEnd == false)
		{
			pos = opSubString.Find(cpTrenner);
			if(pos == -1)
			{
				blEnd = true;
				olText = opSubString;
			}
			else
			{
				olText = opSubString.Left(pos);
				opSubString = opSubString.Mid(pos + 1, opSubString.GetLength( )- pos + 1);
			}
			olText.TrimRight();
			olCedaData.MakeClientString(olText);	/* hag20020411 */

			//check arraysize!!!
			if (ilPos < popStrArray->GetSize())
				popStrArray->SetAt(ilPos,olText);
			else
			{
				ASSERT(FALSE);
				return;
			}
			ilPos++;
		}
	}
}




//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

RecordSet::RecordSet(int ipCount)
{
	pSortValues = NULL;
	Values.SetSize(ipCount);
	Values.FreeExtra();
	Status = 0;
	TmpUse = 0;
	UseBy = 0;
}

RecordSet::~RecordSet()
{
	if(pSortValues != NULL)
		delete pSortValues;
}



RecordSet::RecordSet(CString &opDataList, int ipCount)
{
	TmpUse = 0;
	UseBy = 0;
	pSortValues = NULL;
	Values.SetSize(ipCount);
	ExtractItemListTest(opDataList, &Values);
	Status = 0;
}



RecordSet::RecordSet(CStringArray &opData, int ipCount)
{
	CCSCedaData olCedaData;

	TmpUse = 0;
	UseBy = 0;
	pSortValues = NULL;
	Status = 0;
	Values.SetSize(ipCount);
	for(int i = 0; (i < opData.GetSize()) && (i < ipCount); i++)
	{
		Values[i] = opData[i];
		olCedaData.MakeClientString(Values[i]);	/* hag20020411 */
	}
}


CString& RecordSet::operator[] ( int Index )
{
	static CString olStr("");
	if((Index < 0) || ( Index >= Values.GetSize()))
	{
		return olStr;
	}
	else
	{
		return Values[Index];
	}
}






RecordSet::RecordSet(const RecordSet& s)
{
	TmpUse = 0;
	UseBy = 0;
	pSortValues = NULL;
	Status = 0;
	for(int i = 0; i < s.Values.GetSize(); i++)
	{
		Values.Add(s.Values[i]);
	}
}


const RecordSet& RecordSet::operator= ( const RecordSet& s)
{
	if(this == &s)
	{
		return *this;
	}
	this->Status = 0;

	pSortValues = NULL;
	Values.RemoveAll();

	for(int i = 0; i < s.Values.GetSize(); i++)
	{
		Values.Add(s.Values[i]);
	}
	return *this;

}