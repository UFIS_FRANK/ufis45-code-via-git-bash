// CCSBasic.h: Schnittstelle f�r die Klasse CCSBasic.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CCSBASIC_H__7343CD11_B26E_11D1_A3D8_0000B45A33F5__INCLUDED_)
#define AFX_CCSBASIC_H__7343CD11_B26E_11D1_A3D8_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CCSBasic  
{
public:
	CCSBasic();
	virtual ~CCSBasic();

	CMapStringToString omReplaceMap;

	CString GetSortValue(CString opValue);


};

#endif // !defined(AFX_CCSBASIC_H__7343CD11_B26E_11D1_A3D8_0000B45A33F5__INCLUDED_)
