#ifndef _CCSTABLE_H_
#define _CCSTABLE_H_

#include <afxwin.h>
#include <CCSDefines.h>

#include <ccsdragdropctrl.h>
#include <CCSEdit.h>
#include <CCSButtonCtrl.h>
#include <ccsptrarray.h>

//#include "ccsobj.h"

// Constants for CCSTable::SetTextLineSeparator()
#define ST_NORMAL	0
#define ST_THICK	1


typedef struct _CCSTABLENOTIFY
{
	CWnd *SourceTable;
	int	 Line;
	int  Column;
	void *Data;
	CPoint Point;
}CCSTABLENOTIFY;


typedef struct _CCSIPEDITNOTIFY
{
	CWnd *SourceTable;
	CCSEdit *SourceControl;
	int	 Line;
	int  Column;
	CString	Text;
	bool	Status;	//Return value
	bool	UserStatus;	//Is Return value valid?
	bool	IsChanged;
	bool    IsEscapeKeyPressed;
}CCSIPEDITNOTIFY;


//@Man:
//@Memo: Display data as a list of textlines
//@See: MFC's CWnd, MFC's CListBox, CCSObject
/*Doc
  The class CCSTable displays table (lists) of textlines. The apperance of such a CCSTable
  field had already shown in the PEPPER prototype. The CCSTable class contains these
  following graphical elements:

  \begin{itemize}
  \item Background (colour and margins are configurable)
  \item Title
  \item Data fields in the title
  \item TextLines, automatic height depending on the text font
  \item Line separator (currently a dotted line)
  \item 3D-look column separator (ridges), placement is calculated automatically based
        on the size of the fields in the title or via format array in dialog units.
        Proportional fonts are possible.
  \item Vertical scroll bar will be displayed when the number of lines is larger than
        the viewing area.
  \item Horizontal scroll bar will be displayed when the number of columns is larger
        than the viewing area.
  \item The surrounding window can call the SetPosition(..) member function with appropriate
        parameters to adjust the table window to the new size and new position.
  \end{itemize}    

  In this first implementation, the CCSTable will handle the mouse events the same way as
  the standard Windows list box does (Click, Shift+Click and Ctrl+Click).
*/

/*@ManDoc:
	provides the types of a cell 
*/	
enum egTableColumnType{COLUMN_INT, COLUMN_LONG, COLUMN_FLOAT, COLUMN_DOUBLE, COLUMN_STRING, 
					 COLUMN_DATE, COLUMN_HOUR, COLUMN_bool, COLUMN_BITMAP, COLUMN_EDIT};

enum egFrames{COL_NOFRAME, COL_FRAMEMEDIUM, COL_FRAMETHIN, COL_FRAMETHICK, COL_FRAME_LIKE_VERTICAL};

enum egSpecialControlTypes{CTL_NORMAL, CTL_BUTTON, CTL_RADIOBUTTON, CTL_CHECKBUTTON, CTL_COMBOBOX};
/*@ManDoc:
	privides the alignment definition //Format
*/
enum egTableColumnAlign{COLALIGN_LEFT, COLALIGN_RIGHT, COLALIGN_CENTER};

enum egLineSeparators
{
	SEPA_NONE, 
	SEPA_NORMAL, 
	SEPA_THICK, 
	SEPA_LIKEVERTICAL, 
	SEPA_DASHDOT,
	SEPA_DASH,
	SEPA_DOT
};


struct TABLE_MENU
{
	int Line;
	int Column;
	CCSPtrArray<MENUITEM> Items;
};


struct TABLE_COLUMN
{
	int			Lineno;
	int			Columnno;
	int			Alignment;
	int			Length;
	int			FrameLeft;
	int			FrameRight;
	int			FrameTop;
	int			FrameBottom;
	int			Type;
	CString		Format;
	//CCSEdit//////////////////////	
	CCSEDIT_ATTRIB EditAttrib;
	bool		IPStatus;
/*
	int			RangeFrom;				// Only used for KT_INT
	int			RangeTo;
	double		dRangeFrom;
	double		dRangeTo;
	int			TextLimit;
	int			TextMinLenght;
	int			TextMaxLenght;
	int			FloatIntPart;
	int			FloatFloatPart;
	COLORREF	ErrColor;
*/
	////////////////////////////////	
	int			SeparatorType;			//Seperator of the column
	int			VerticalSeparator;
	int			HorizontalSeparator;
	int			SecialControlType;      // CTL_COMBOBOX, CTL_BUTTON etc.
	CWnd		*SpecialControl;		// Controlobject CButton, CComboBox etc.
	COLORREF	BkColor;
	COLORREF	TextColor;
	CRect		rect;
	CFont		*Font;
	CString		Text;
	CEdit		*polEdit;
	bool		blIsEditable;
	CString		ComboEntries;
	bool		blToolTip;
	TABLE_COLUMN()
	{
		Lineno = -1; Columnno = -1; Alignment = COLALIGN_LEFT; Length = 0;
		FrameLeft = COL_NOFRAME; FrameTop = COL_NOFRAME; FrameRight = COL_NOFRAME; 
		FrameBottom = COL_NOFRAME; Type = COLUMN_STRING; SeparatorType = SEPA_NONE;
		Format = ""; BkColor = RGB(255,255,255); TextColor = RGB(0,0,0);
		VerticalSeparator = SEPA_LIKEVERTICAL;
		HorizontalSeparator = SEPA_LIKEVERTICAL;
		blIsEditable = true;
		rect = CRect(0,0,0,0);
		SecialControlType = CTL_NORMAL;
		SpecialControl = (CWnd*)NULL;
		blToolTip = false;
		IPStatus = true;
/*	    CDC dc;
		dc.CreateCompatibleDC(NULL);

		LOGFONT logFont;
		memset(&logFont, 0, sizeof(LOGFONT));

		logFont.lfHeight = - MulDiv(10, dc.GetDeviceCaps(LOGPIXELSY), 72);
		logFont.lfWeight = FW_NORMAL;
		logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
		lstrcpy(logFont.lfFaceName, "Courier New");
		Font->CreateFontIndirect(&logFont);
*/		Font = NULL;
		Text = "";
		polEdit = NULL;
	}
};

struct TABLE_HEADER_COLUMN
{
	int Alignment;
	int Length;
	int AnzChar;
	int FrameLeft;
	int FrameRight;
	int FrameTop;
	int FrameBottom;
	int Type;
	int SeparatorType;
	CString Format;
	COLORREF BkColor;
	COLORREF TextColor;
	CFont *Font;
	CString Text;
	CString String;
	TABLE_HEADER_COLUMN()
	{
		Alignment = COLALIGN_LEFT; Length = 100; FrameLeft = COL_NOFRAME; 
		FrameTop = COL_NOFRAME; FrameRight = COL_NOFRAME; 
		FrameBottom = COL_NOFRAME; Type = COLUMN_STRING; Format = ""; 
		BkColor = ::GetSysColor(COLOR_BTNFACE); TextColor = RGB(0,0,0);
		SeparatorType = SEPA_LIKEVERTICAL;

	 /*   CDC dc;
		dc.CreateCompatibleDC(NULL);
		LOGFONT logFont;
		memset(&logFont, 0, sizeof(LOGFONT));

		logFont.lfHeight = - MulDiv(10, dc.GetDeviceCaps(LOGPIXELSY), 72);
		logFont.lfWeight = FW_NORMAL;
		logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
		lstrcpy(logFont.lfFaceName, "Courier New");
		Font->CreateFontIndirect(&logFont); 
	*/	Font = NULL;
		Text = "";
		AnzChar = 0;
	}
};

struct TABLE_LINE
{
	int Lineno;
	CCSPtrArray <TABLE_COLUMN> Columns;
};

/////////////////////////////////////////////////////////////////////////////
// CCSTableListBox

// In order to implement the CCSTable correctly, we need CCSTableListBox to be subclassed
// from the MFC's CListBox.
//
// The first reason is we need to handle mouse events. For example, when the user
// double-click in the table content, CCSTableListBox will detect the WM_LBUTTONDBLCLK
// message. It will generate a message WM_TABLETEXTSELECT to the surrounding window,
// in turn.
//
// The second reason is we need to update the table content correctly. This need
// CCSTableListBox as a separated class since, we need to override the OnPaint() member
// functions to handle the WM_PAINT message. When the CCSTableListBox, which is an
// owner-draw control receive this message, it need to redraw the entire table content
// as an empty table. By this requirement, I need to declare the CCSTableListBox as
//"friend" of the CCSTable.
//
class CCSTable;
class CCSHeaderTable;

class CCSTableListBox: public CListBox
{
public:
    // Specify the CCSTable object which this list box associated with.
    // Since CCSTableListBox is a separated object from CCSTable, it need this data member.
	CCSTableListBox(){pomTableWindow = NULL;pomToolTip=NULL;pomToolTipStaic=NULL;}
    CCSTable *pomTableWindow;
	 CToolTipCtrl *pomToolTip;
	 CStatic *pomToolTipStaic;
	
	 
	// For remember the clip-box which detected in OnEraseBkgnd and used in OnPaint
	CRect omClipBox;
	// Return the item ID of the list box based on the given "point"
	int GetItemFromPoint(CPoint point);

	// Update the member "imCurrentColumn" of the parent CCSTable
	void DetectCurrentColumn(CPoint point);


	//get coordinates of first visible column
	bool GetVisibleLeftTopColumn(int &ripX, int &ripY);

	//get coordinate of last visible column
	bool GetVisibleRightBottomColumn(int &ripX, int &ripY);

	// returns the column specified by a point
	int GetColumnFromPoint(CPoint opPoint);

	// retruns the index of the first visible Item of the listbox
	int GetFirstVisibleItem();
	// return the index of the last visible Item of the listbox
	int GetLastVisibleItem();
	void OnMouseMove(UINT nFlags, CPoint point);




protected:
    //{{AFX_MSG(CCSTableListBox)
    afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg LONG OnEditRButtonDown( UINT wParam, LPARAM lParam);
	afx_msg LONG OnEditLButtonDblClk( UINT wParam, LPARAM lParam);
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
    afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnKillfocus();
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void SelectionHasBeenChanged();
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};

class CCSTable: public CWnd
{
// Public members
public:
    /*@ManDoc:
      Creates an empty CCSTable object.
    */
    CCSTable::CCSTable();

    /*@ManDoc:
      Creates CCSTable object. Initializes data members with data passed as parameters.
      It will not create or display the window table, this should be dont by the
      DisplayTable(..) method only. The calling function is responsible for passing valid
      data. See also: CCSTable::SetTableData().
    */
	CCSTable::CCSTable(
        CWnd *popParentWindow,
        int ipXStart, int ipXEnd, int ipYStart, int ipYEnd,
        COLORREF lpTextColor = ::GetSysColor(COLOR_WINDOWTEXT),
        COLORREF lpTextBkColor = ::GetSysColor(COLOR_WINDOW),
        COLORREF lpHighlightColor = ::GetSysColor(COLOR_HIGHLIGHTTEXT),
        COLORREF lpHighlightBkColor = ::GetSysColor(COLOR_HIGHLIGHT),
        CFont *popTextFont = NULL,
        CFont *popHeaderFont = NULL
    );

    ~CCSTable();

	CCSDragDropCtrl m_DragDropSource;
	CCSDragDropCtrl m_DragDropTarget;

	bool bmMultiDrag;
	bool bmEnableHeaderDragDrop; 

	// There is an new method to store Linedata not using the format
	// SetFormatList/SetHeaderFields so we need another structure for
	// storing data
	CCSPtrArray <TABLE_LINE> omLines;
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;
	CCSPtrArray <CString> omTableFieldArray;  

	bool IsMethodWithPointerArray;
	//CInplaceEdit *pomInplaceEdit;
	CCSEdit *pomIPEdit;
	bool IsInplaceEditMode;


	int imNextControlID;
    /*@ManDoc:
      Set data members to data passed as parameters. The calling function is responsible
      for passing valid data. By now, this member function always return {\bf RCSuccess}.

      {\bf ATTENTION!}
      Please notice that the passed parameters have changed from the specification of
      {\bf ..\BKK0410A\TABLE.DOC}. The HANDLE hmParentWindow was changed to {\bf (CWnd *)}
      for MFC compatibility. Window positioning parameters changed from {\bf long}
      to {\bf int} because finally they must converted to {\bf CRect} which providing
      constructor only for type {\bf int}. Actually, I feel that we should just pass the
      {\bf CRect} directly to this method. Text, both normal and selected (highlighted),
      colors and font parameters were changed. (Note that the header bar and the vertical
      separators or ridges use the system color of button shading.) Furthermore, many
      parameters also provide default value.
    */
    bool SetTableData(
        CWnd *popParentWindow,
        int ipXStart, int ipXEnd, int ipYStart, int ipYEnd,
        COLORREF lpTextColor = ::GetSysColor(COLOR_WINDOWTEXT),
        COLORREF lpTextBkColor = ::GetSysColor(COLOR_WINDOW),
        COLORREF lpHighlightColor = ::GetSysColor(COLOR_HIGHLIGHTTEXT),
        COLORREF lpHighlightBkColor = ::GetSysColor(COLOR_HIGHLIGHT),
        CFont *popTextFont = NULL,
        CFont *popHeaderFont = NULL
    );
    
    /*@ManDoc:
      Displays the table. Calling this method is a must after you have change window
      parameters, move/resize window, or update data in the table. Except for
      SetPosition(..), AddTextLine(..), ChangeTextLine(..), and DeleteTextLine(..),
      other provided public methods should call this method after finish update table.
      The DisplayTable(..) will create the window if it is not already created.
      This method returns {\bf RCSuccess} on success, or {\bf RCFailure} if it cannot
      create windows for a CCSTable instance.
    */
	// nBar => SB_BOTH, SB_HORZ oder SB_VERT
    bool DisplayTable(UINT nBar = SB_BOTH);

    /*@ManDoc:
      Sets the position data members to the passed parameters if the CCSTable window is
      currently displayed. Also update the display. This method always returns
      {\bf RCSuccess}.
    */
    bool SetPosition(int ipXStart, int ipXEnd, int ipYStart, int ipYEnd);


	CCSPtrArray<TABLE_MENU> omMenuItems;


	void AddMenuItem(int ipLine, int ipColumn, UINT ipFlags, UINT ipID, CString opText);

	void SetEditMenu(int ipLine, int ipColumn, CCSEdit *pomEdit);


    //@ManMemo: Returns the height of table
	 int GetTableHeight();
    //@ManMemo: Returns the associate listbox of the this table.
    CListBox *GetCTableListBox();
	 //@ManMemo: Set selection (highlight) of a line
	int SelectLine(int ipLineno, BOOL bpSelect = TRUE);
	//@ManMemo: Returns the line which the point (logical related to listbox) is on.
	int GetLinenoFromPoint(CPoint opPoint);

	//@ManMemo: Performs column detection and return current column.
	int GetColumnnoFromPoint(CPoint opPoint);

    /*@ManDoc:
      Fill the {\bf omFieldNames} array with the text data passed in {\bf opFieldNames}.
      {\bf opFieldNames} passes the text of the field names in a comma-separated list.
      The first item in this list will be copied to {\bf omFieldNames[0]}. These field names
      will be used in {\bf GetFieldValue(..)}. This method always returns {\bf RCSuccess}.
    */
    bool SetFieldNames(CString opFieldNames);

    /*@ManDoc:
      Fill the {\bf omHeaderFields} array with the text data passed in {\bf opHeaderFields}.
      {\bf opHeaderFields} passes the text of the table header fields in a vertbar-separated
      list. The first item in this list will be copied to {\bf omHeaderFields[0]}. This
      method always returns {\bf RCSuccess}.
    */
    bool SetHeaderFields(CCSPtrArray <TABLE_HEADER_COLUMN> opHeaderDataArray);

	/* For rebuilding the Table Fields	*/
	bool SetTableFields(CCSPtrArray <CString> omTableFieldArray);  

	// TVO
	TABLE_HEADER_COLUMN *GetHeader(int ipColumnNo);


	/*@ManDoc:
      Fill the {\bf omStructuredLineData} with the data passed in prpTextCOLUMN.
    */
	bool SetFieldList(TABLE_COLUMN *prpTextColumn);
        //@ManMemo: Remove items from the table content.
    bool ResetContent();

    //@ManMemo: Returns zero-based index of the item that has the focus rectangle.
    int GetCurrentLine();


	 //@ManMemo: Returns the index of current selected line
	int GetCurSel();

    /*@ManDoc:
      Formats the passed {\bf opText} and add it to the internal {\bf omData} member.
      Update the display if necessary. The parameter "pvpData" is a pointer to the
      corresponding of the formatted {\bf opText}. This is required for passing the
      {\bf pvpData} back to the parent window when user press a double-click and generate
      a WM_TABLETEXTSELECT message. Returns {\bf RCSuccess} or {\bf RCFailure}.

      {\bf ATTENTION!}
      The {\bf opText} changed to be a vertical bar separated list in stead of comma list.
    */
    bool AddTextLine(CCSPtrArray <TABLE_COLUMN> &opLine, void *pvpData);

    /*@ManDoc:
      Formats the passed {\bf opText} and insert it to the internal {\bf omData} member.
      Unlike the {\bf AddTextLine} member function, {\bf InsertTextLine} will insert the
      given {\bf opText} to be at the line number given in {\bf ipLineNo}. If {\bf ipLineNo}
      parameter is -1, the string is added to the end of the list. Update the display if
      necessary. The parameter "pvpData" is a pointer to the corresponding of the formatted
      {\bf opText}. This is required for passing the {\bf pvpData} back to the parent window
      when user press a double-click and generate a WM_TABLETEXTSELECT message. Returns
      {\bf RCSuccess} or {\bf RCFailure}.

      {\bf ATTENTION!}
      The {\bf opText} changed to be a vertical bar separated list in stead of comma list.
    */
    bool InsertTextLine(int ipLineNo, CCSPtrArray <TABLE_COLUMN> &popLine, void *pvpData);

    /*@ManDoc:
      If {\bf ipLineNo} contains a valid line number, formats the passed {\bf opText} and
      replace the line in {\bf omData} member. ({\bf ipLineNo} -1 indicates current line.)
      The parameter "pvpData" is a pointer to the corresponding of the formatted
      {\bf opText}. This is required for passing the {\bf pvpData} back to the parent window
      when user press a double-click and generate a WM_TABLETEXTSELECT message.
      Update display if necessary. Returns {\bf RCSuccess} or {\bf RCFailure}.

      {\bf ATTENTION!}
      The {\bf opText} changed to be a vertical bar separated list in stead of comma list.
    */
    bool ChangeTextLine(int ipLineNo, CCSPtrArray <TABLE_COLUMN> *popLine, void *pvpData);

    /*@ManDoc:
      If {\bf ipLineNo} contains a valid line number, delete the line from {\bf omData}
      member. ({\bf ipLineNo} -1 indicates current line.) Update display if necessary.
      Returns {\bf RCSuccess} or {\bf RCFailure}.
    */
    bool DeleteTextLine(int ipLineNo);

    /*@ManDoc:
      If {\bf ipLineNo} contains a valid line number, change color of the line.
      ({\bf ipLineNo} -1 indicates current line.) Update display if necessary.
      Returns {\bf RCSuccess} or {\bf RCFailure}.
    */
    bool SetTextLineColor(int ipLineNo, COLORREF lpTextColor, COLORREF lpTextBkColor);

    /*@ManDoc:
      If {\bf ipLineNo} contains a valid line number, change the line separator of the
	  line. The separator may be TLS_NORMAL or TLS_THICK. ({\bf ipLineNo} -1 indicates
	  current line.) Update display if necessary. Returns {\bf RCSuccess} or {\bf RCFailure}.
    */
	bool SetTextLineSeparator(int ipLineNo, int ipSeparatorType);

    /*@ManDoc:
      If {\bf ipLineNo} contains a valid line number, change the font of the line.
	  Update display if necessary. Returns {\bf RCSuccess} or {\bf RCFailure}.
    */
	bool SetTextLineFont(int ipLineNo, CFont *popFont);

    /*@ManDoc:
      If {\bf ipLineNo} contains a valid line number, change the current drag status of
      the given line ({\bf ipLineNo} -1 indicates current line.) The user can Only the lines with
	  dragging enabled will be dragg
      Returns {\bf RCSuccess} or {\bf RCFailure}.
    */
    bool SetTextLineDragEnable(int ipLineNo, bool bpEnable);
	bool SetTextColumnFont(int ipLineNo, int ipColumnNo, CFont *popFont);
	bool SetTextColumnFont(int ipColumnNo, CFont *popFont);
	bool SetTextColumnColor(int ipLineNo, int ipColumnNo, COLORREF lpTextColor, COLORREF lpTextBkColor);
	bool SetTextColumnColor(int ipColumnNo, COLORREF lpTextColor, COLORREF lpTextBkColor);
	bool SetColumnEditable(int ipColumnno, bool bpEdit = true);
	bool SetColumnEditable(int ipLineno, int ipColumnno, bool bpEdit = true);

	bool SetCellType(int ipLineno, int ipColumnno,  CCSEDIT_ATTRIB rpAttrib);

	bool SetColumnType(int ipColumnno,  CCSEDIT_ATTRIB rpAttrib);

	bool SetLineColumnType(int ipColumnFrom, int ipLineFrom,  int ipColumnTo, int ipLineTo, CCSEDIT_ATTRIB rpAttrib);

	bool GetLineColumnBkColor(int ipLine,  int ipColumn, COLORREF &lpColor);

	void UnsetDefaultSeparator(){bmDefaultSeparator = false;};
	void SetDefaultSeparator(){bmDefaultSeparator = true;};
    /*@ManDoc:
      If {\bf ipLineNo} contains a valid line number, get the text from the line in
      {\bf omData} member ({\bf ipLineNo} -1 indicates current line), copy it to {\bf opText}.
      Returns {\bf RCSuccess} or {\bf RCFailure}.

      {\bf ATTENTION!}
      The specification expected this function formats line back to internal format
      (trim characer fields, format date and time fields to Ceda format). However, this
      feature was removed since it's not make sense since the destination is CString.
      Moreover, CCSTable does not know anything about the data record structure. But the
      caller do.
    */
    bool GetTextLineValue(int ipLineNo, CString &opText);
	bool GetTextLineColumns(CCSPtrArray <TABLE_COLUMN> *popLine, int ipLineno);

    /*@ManDoc:
      If {\bf ipLineNo} contains a valid line number, return a void pointer which previously
      stored together with text line when AddTextLine(), InsertTextLine(), or ChangeTextLine().
      Otherwise, return NULL if there is no current line or window is not created yet.
    */
    void *GetTextLineData(int ipLineNo);

	//@ManMemo: returns current drag enable status of the specified text line
	BOOL GetTextLineDragEnable(int ipLineNo);

    /*@ManDoc:
      If {\bf ipLineNo} contains a valid line number and {\bf ipFieldNo} a valid field
      number, get the text of the requested field from the line in {\bf omData} member
      ({\bf ipLineNo} -1 indicates current line), copy it to {\bf opText}. Returns
      {\bf RCSuccess} or {\bf RCFailure}.
      
      {\bf ATTENTION!}
      The specification expected this function formats line back to internal format
      (trim characer fields, format date and time fields to Ceda format). However, this
      feature was removed since it's not make sense since the destination is CString.
      Moreover, CCSTable does not know anything about the data record structure. But the
      caller do.
    */
    bool GetTextFieldValue(int ipLineNo, int ipFieldNo, CString &opText);

	bool SetTextFieldValue(int ipLineNo, int ipFieldNo, const CString &ropText);

	int GetLinesCount();

    /*@ManDoc:
      If {\bf ipLineNo} contains a valid line number and {\bf opFieldName} a valid field
      name, get the text of the requested field (calculated field number from
      {\bf omFieldNames}) from the line in {\bf omData} member. Returns {\bf RCSuccess}
      or {\bf RCFailure}.
    */
    bool GetTextFieldValue(int ipLineNo, CString opFieldName, CString &opText);

    /*@ManDoc:
      If {\bf ipFieldNo} contains a valid field number, search through the table for the
      first line containing {\bf opText}. Returns line number (base-0) containing search
      string ({\bf opText}), or -1 if not found. On success, remember {\bf imNextLineToSearch}
      and the text {\bf opText} in {\bf omLastSeachText}. On further calls to
      SearchTextLine(..) with the same text in {\bf opText} start at this next line.
    */
    int SearchTextLine(int ipFieldNo, CString opText);

	void SetSelectMode(int ipSelectMode);
	
	void SetStyle(int ipStyle);

	/*
	This mothod sets the state whether a selection should be shown. I meen the blue
	highlighting line. The method should be used when we want to hide the feeling
	of a listbox
	*/
	void SetShowSelection(bool bpSelectMode = true);
	//New methods after concept change
	TABLE_COLUMN * FindColumn(int ipLineno, int ipColumnno);
	bool AddLine(CCSPtrArray <TABLE_LINE> *popLine);
	bool AddColumnToHeader(TABLE_HEADER_COLUMN *prpHeaderData);
	bool AddColumn(CCSPtrArray <TABLE_COLUMN> &opColumns);


	bool SetHeader(CCSPtrArray <TABLE_HEADER_COLUMN> *prpHeaderLine);
	void SetHeaderSpacing(int ipPixel);

	bool AddColumnToLine(TABLE_COLUMN *prpColumnData);
	bool AddColumnToLine(int ipLineno, TABLE_COLUMN *prpColumnData);
	bool ChangeColumnData(int ipLineno, int ipColumnno, TABLE_COLUMN *prpColumnData);
	bool ChangeColumnData(int ipColumnno, TABLE_COLUMN *prpColumnData);
	bool ChangeColumnColor(int ipColumnno, COLORREF rlBkColor, COLORREF rlTextColor);
	bool ChangeColumnColor(int ipLineno, int ipColumnno, COLORREF rlBkColor, COLORREF rlTextColor);
	bool ChangeColumnType(int ipLineno, int ipColumnno, int Type);
	bool ChangeColumnType(int ipColumnno, int Type);
	bool DeleteLineData(int ipLineno);
	bool DeleteHeaderData();
	void SetLeftDockingRange(int ipNumOfColumns); // Docking only valid for the n leftmost columns
	void SetRightDockingRange(int ipNumOfColumns);

	// Methods for Creating special control, like Combobox, Button, Radio/CheckButton
	// in a cell
	bool MakeSpecialControl(int ilX, int ilY, int ilCtlType);
	int GetNextControlId();

	// Methods for using this tableclass in a dialog as a control
	// this modus provides a new behavior for more information ask RST
	void SetMiniTable(bool bpModus = true);
	void KillFocus();


	void SetIPEditModus(bool bmWithoutALT) {bmIPEwithoutALT = bmWithoutALT;};

	// Methods for using an inplace editline
	bool MakeInplaceEdit(int ilItemID, int ipColumnno = -1);

	bool GetCellStatus(int ipLineno, int ipColumnno);

	void SetIPValue(int ipLineno, int ipColumnno, CString olValue, COLORREF opColor = RGB(0,128,0), bool bpIPStatus = true);

	void EndIPEditByLostFocus(bool bpSet);

	bool MoveIPEdit();


	void EndInplaceEditing();
	void CheckScrolling();
	void SetTableEditable(bool bpSet)
	    {bmEditable = bpSet;}


	
	// For Selecting a Specified Cell in the CCSTable.
	void SelectCell(int iLineNo,int iCellNo,CString strData);  
	void EnableHeaderDragDrop(bool bpEnableHeaderDragDrop = true){bmEnableHeaderDragDrop = bpEnableHeaderDragDrop;} 

// Protected members
protected:
    // This variable indicates that the member function SetTableData(..) has already
    // been called or not. It will be set to false on the default constructor, and changed
    // to true on success SetTableData(..). The member function DisplayTable(..) cannot
    // create the window until "bmInited" is set to true.
    //
    bool bmInited;
	bool bmEditable;
	bool bmEndIPEditByLostFocus;

	bool bmIPEwithoutALT;

	 CToolTipCtrl *pomToolTip;
	bool blIsForDockingOnly;
    // these protected data members are corresponding to parameters in SetTableData()
    CWnd *pomParentWindow;
	//CCSDockingWindow *pomLeftScale;
	int imLeftScaleWidth;
    int imXStart, imXEnd;
    int imYStart, imYEnd;
    COLORREF lmTextColor;
    COLORREF lmTextBkColor;
    COLORREF lmHighlightColor;
    COLORREF lmHighlightBkColor;
    CFont *pomTextFont;
    CFont *pomHeaderFont;
	int    imLeftTopMostItem;
	int    imRightBottomMostItem;
	int		imHeaderSpacing;

    // These three variables will be changed by SetHeaderFields(), SetFieldNames(), and
    // SetFormatList(). Actually, we may implement to keep the comma-separated strings
    // which passed into these member functions directly. However, for sake of some
    // efficiency, they are split into a form of array, which we could access any elements
    // directly and more efficiently.
    //
    CStringArray omHeaderFields;    // array of column headers
    CStringArray omFieldNames;      // array of column (field) names
    CStringArray omFormatList;      // array of column format specifiers
	CStringArray omTypeList;		// array of column type specifiers (text or color)
    // This array represents the whole data of the table.
    // Each record is kept in the vertbar-separated format, and will be extracted on the fly.
    //
    // ATTENTION!
    // Type of this member changed from the speicification (CPtrArray) which was defined
    // as an array of structs, because just handling string is simpler, use less resource,
    // and the specification say something unclear about the raw data struct with unknown
    // format or details of content.
    //
    CPtrArray omDataPtr;		// associated data pointer for each text line
	CDWordArray omColor;		// associated color for each text line
	CDWordArray omBkColor;		// associated background color for each text line
	CWordArray omDragEnabled;	// associated DragEnable boolean flag for each text line
	CWordArray omSeparatorType;	// associated separator type for each text line
	CPtrArray omFontPtr;		// associated font for each text line

	int imLeftDockingColumns;		// first columns are docking
	int imRightDockingColumns;
								// default should be 0 ==> no docking
    // internal members used for searching routine -- SearchTextLine(..)
    int imNextLineToSearch;
    CString omLastSearchText;

    // private data for pixel calculation -- mostly used in CalculateWidgets()
    int imTextFontHeight;
    int imHeaderFontHeight;
    CDWordArray omLBorder;       // array of column's left-border X position
    CDWordArray omRBorder;       // array of column's right-border X position
    int imTableContentOffset;
	 int imTableYOffset;
    int imHorizontalExtent;     // list box width (in pixel)
    CRect omHeaderBarRect;
	 CRect omLeftDockingRect;
	 CRect omRightDockingRect;
	 //CWnd *pomParentWindow;
	 //CWnd  *pomLeftScale;
	int imCurrentEditPosX;
	int imCurrentEditPosY;
	int omHeaderBarHeight;

	int imCurrentEditLine;
	int imCurrentEditColumn;

	bool bmMiniTable;
	int  imUserStyle;

	CFont omMSSansSerif_Regular_8;
	CFont omCourier_Regular_10;

    void CalculateWidgets();

    // The scrollable list box represents the table content:
   // class CCSTableListBox;
    friend CCSTableListBox;       // needed since CCSTableListBox calls method DrawItem()
	friend CCSHeaderTable;
	//friend CCSDockingWindow;
	//friend CCSDockingHeader;
public:
	CCSTableListBox *pomListBox;
	CCSHeaderTable *pomListHeader;
	int imItemHeight;
	bool bmDefaultSeparator;

    // helper drawing object -- created in constructor, destroyed in destructor.
    CPen FacePen, ShadowPen, WhitePen, BlackPen, DashDotPen, DotPen, DashPen;
    CBrush FaceBrush, SeparatorBrush;

    // Message handling:
    // CCSTable is implemented as an owner-drawn listbox within a child window.
    // These member functions will handle essential messages to allow CCSTable work correctly.
    //
    // Please understand that, following to OOP concept and MFC style, we should
    // implement MeasureItem() and DrawItem() in the owner-draw list box itself instead of
    // in a CCSTable which is its parent. However, since MeasureItem() and DrawItem() need
    // to access too many of CCSTable protected members, I moved to use OnMeasureItem() and
    // OnDrawItem() of CCSTable instead. On the other hand, if we want to follow OOP concept
    // and MFC style, we should have a new class, CCSTableContent derived from CListBox, for
    // the table content and handling its own MeasureItem() and DrawItem(). Then, CCSTable
    // will have a CCSTableContent as a member, and only draw its header bar.
    //
    //{{AFX_MSG(CCSTable)
    afx_msg void OnPaint();
    afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
    afx_msg void OnDrawItem(int NIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
    afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
    afx_msg LONG OnMoveInplaceLeft(UINT, LONG); 
    afx_msg LONG OnMoveInplaceRight(UINT, LONG); 
    afx_msg LONG OnMoveInplaceUp(UINT, LONG); 
    afx_msg LONG OnMoveInplaceDown(UINT, LONG); 
    afx_msg LONG OnEndInplaceEditing(UINT, LONG); 
    afx_msg LONG OnInplaceReturn(UINT, LONG); 
	afx_msg int OnToolHitTest( CPoint point, TOOLINFO* pTI );
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg LONG OnEditKillfocus( UINT wParam, LPARAM lParam);
	afx_msg LONG OnEditChanged( UINT wParam, LPARAM lParam);
	afx_msg void OnMenuSelect( UINT nID );
	afx_msg LRESULT OnMenuSelect2( UINT wParam, LPARAM lParam );
	afx_msg LONG OnTableRButtonDown( UINT wParam, LPARAM lParam);
	virtual BOOL PreTranslateMessage( MSG* pMsg );
	afx_msg BOOL OnToolTipNotify( UINT id, NMHDR * pNMHDR, LRESULT * pResult );
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()

    // helper functions for drawing header bar and table
    void DrawHeaderBar();
	 void DrawLeftScale();
	 void DrawRightScale();
    void DrawItem(CDC &dc, UINT itemID, const CRect &rectClip,
        BOOL bpIsToggleFocusOnly, BOOL bpIsSelected, BOOL bpIsFocus);

public:
	int tempFlag;    
	int imCurrentColumn;
	int imSelectMode;
	bool blShowSelection;
};

//CCSHeaderTable class is for handling the drag and drop operation between header columns.
class CCSHeaderTable : public CWnd
{
public:
	CCSHeaderTable() : CWnd()
	{
		m_intSourceColumn = -1;
		m_intDestColumn = -1;
	}	
	
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnPaint();
	afx_msg void OnMouseMove(UINT nFlags,CPoint point);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);	
	afx_msg LONG OnDrop(UINT wParam, LONG lParam);	
	afx_msg LONG OnDragOver(WPARAM wParam, LPARAM lParam);	
	afx_msg LONG OnDragBegin(UINT wParam, LONG lParam);

	CCSTable* pomTableWindow;
	int  DetectCurrentColumn(CPoint point);
	void ResetColumnValues()
	{		
		m_intDestColumn = -1;
		m_intSourceColumn = -1;
	}

private:
	CCSDragDropCtrl omDragDropTarget;
	int m_intSourceColumn;
	int m_intDestColumn;

	DECLARE_MESSAGE_MAP()
};

#endif
