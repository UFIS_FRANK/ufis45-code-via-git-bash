// CedaObject.h: interface for the CedaObject class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CEDAOBJECT_H__28E63D62_971E_11D1_8371_0080AD1DC701__INCLUDED_)
#define AFX_CEDAOBJECT_H__28E63D62_971E_11D1_8371_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <RecordSet.h>
#include <FieldSet.h>
#include <CCSPtrArray.h>
#include <CCSBcHandle.h>
#include <CCSBasic.h>
#include <UfisOdbc.h>
#include <AatHelp.h>


typedef int (*CALLBACKFUNC)(struct BcStruct *vpDataPointer);

class CedaSysTabData;

class CedaObject  
{
public:
	CedaObject(CString opAlias = "", CString opObject = "", CString opTableExtension = "", CCSBasic *popBasic = NULL);
	virtual ~CedaObject();


	CedaObject(const CedaObject& s);

	void			SetBroadcastCallback(CALLBACKFUNC pfpFunction);
	CALLBACKFUNC	GetBroadcastCallback();


	CString GetTableName();

	bool bmIsLogicObject;

	void SetObjectDesc(CString opDesc) { omObjectDesc = opDesc; };	

	CString GetObjectDesc() { return omObjectDesc; };	

	const CedaObject& operator= ( const CedaObject& s);

	void ClearAll();

	void ClearTmpData();

	bool Init(CCSPtrArray<FieldSet> opFieldSet);

	bool Init(CString opFieldList, UfisOdbc *popOdbc = NULL);

	CString  GetTableHeader(CString opFields, CString opTrenner);

	void  SetTableHeader(CString opField, CString opHeader);
	static bool IsFieldAvailable(CString opTable, CString opField);
	static int  GetMaxFieldLength(CString opTable,CString opField);

	CString GetFieldList();
	//MWO 21.05.1999
	CString GetDBFieldList();
	//END MWO

	//MWO 13.03.2000
	void GetFieldSetInfo(CStringArray &ropDBTypes, CStringArray &ropFields);

	CString MakeODBCString(CString opData);

	CString GetDBFieldType(CString opField);
	//END MWO

	// MNE 27.01.00
	CString GetDBDataList(RecordSet *prpRecord);
	// END MNE


	//if the field is not valid ==> function returns -1
	bool GetFieldIndex(CString &opField, int &ilIndex);

	int GetMaxFieldLength(CString opField);

	CString GetFieldType(CString opField);

	void TraceData();

	bool AddKeyMap(CString opField);

	CMapStringToPtr * GetMapPointer(CString opField);

	void AddToKeyMap(RecordSet *prlRecordSet);

	CString GetFields(int ipIndex, CString opFields, CString opTrenner, int ipFlags);
	
	bool GetFields(CString opRefField, CString opRefValue, CString opField1, CString opField2, CString &opReturn1, CString &opReturn2);

//if the field is not valid ==> function returns "NULL"
	CString GetField(int ipIndex, CString opField);

	void GetRecords(CString opRefField, CString opRefValue, CCSPtrArray<RecordSet> *prpRecords);


	CString  GetValueList(CString opRefField, CString opRefValue, CString opField, CString opTrenner = ",");

	//MWO 23.12.98
	bool GetAllValuesForField(CString opField, CStringArray &ropValues);

	//END MWO

	//MWO 21.05.1999
	//Must be commaseparated and in capital letters and per field length of 4 characters
	bool SetLogicalFields(CCSPtrArray<FieldSet> opFieldSet);
	//END MWO

	void GetAllFields(CString opFields, CString opTrenner, int ipFlag, CStringArray &olData);

	int ShowAllFields(CString opFields, CListBox *polList, CString opSelString, bool bpTrimRight = false, bool bpAddBlankLine  = false);

	CString GetField(CString opRefField, CString opRefValue, CString opField);

	bool GetField(CString opRefField, CString opRefValue, CString opField, CString &opReturn);

	bool GetFieldVal(CString opRefField, CString opRefValue, CString opField, CString &opReturn, CString opValDate = "");

	bool GetRecord(CString opRefField, CString opRefValue, RecordSet &rpRecord);

	bool GetRecord(int ilIndex, RecordSet &rpRecord);

	int GetAllRecords(CCSPtrArray <RecordSet> &ropRecords);

	bool IsDataLoad(){return bmIsDataRead;};

	bool IsReadAll(){return bmIsReadAll;};

	int GetFieldCount(){return FieldCount;};
	//MWO 21.05.199
	int GetDBFieldCount(){return FieldCount - imLogicalFieldCount;};
	//END MWO

	void SetReadAll(bool bpSet){bmIsReadAll = bpSet;};

	
	bool UpdateInternal(CString &opUrno, CString opFieldList, CString opDataList, RecordSet *prpRecord = NULL);
	
	bool InsertInternal(CString opFieldList, CString opDataList, RecordSet *prpRecord = NULL);
	
	bool DeleteInternal(CString &opUrno, RecordSet *prpRecord = NULL);

	bool GetField(CString opRefField1, CString opRefField2, CString opRefValue, CString &opReturn1, CString &opReturn2, CString opValData = "");

	bool GetFieldBetween(CString opRefFieldFrom, CString opRefFieldTo, CString opRefValue, CString opField, CString &opReturn);

	CString GetField2(CString opRefField1, CString opRefField2, CString opRefValue, CString opField);

	CString GetFieldExt(CString opRefField1, CString opRefField2, CString opRefValue1, CString opRefValue2, CString opField);

	int GetRecordsExt(CString opRefField1, CString opRefField2, CString opRefValue1, CString opRefValue2, CCSPtrArray<RecordSet> *prpRecords);

	bool SharedMem(){return bmFieldsInSM;};

	int GetDataCount(){return Data.GetSize();};

	void AddRecord(CString &opDataList);

	void AddRecord(CCSPtrArray<RecordSet> *popBuffer, CString &opDataList);

	bool GetWhere(CString &opRefField, CString &opRefValue, CString &opWhere);

	bool SetRecord(CString opRefField, CString opRefValue, CStringArray &opRecord);

	bool SetField(CString opRefField, CString opRefValue, CString opField, CString opValue);

	bool InsertRecord(RecordSet &rpRecord, CString opUrno);

	bool DeleteRecord(CString opRefField, CString opRefValue);

	CMapPtrToPtr *GetSaveRecords(){return &omChangedMap;};

	bool RemoveRecord(CString opRefField, CString opRefValue);

	bool RemoveRecord(RecordSet *prpRecord);

	void SetSort(CString opSortList, bool bpDo, bool bpNumeric = false);

	void Sort();

	// BC

	CCSPtrArray<BcStruct> &GetBcBuffer(){return BcBuffer;};

	void SetBcMode(int ilBufferSize = 0) { BcBufferSize = ilBufferSize; };

	bool BufferBc(BcStruct *prlBC);

	void SetDdxType(CString opCmd, int ipDdxType);

	bool GetDdxType(CString opCmd, int &ipDdxType);

	void SetSentBcBufferOverflow(int ipDdxType);

	bool SentBcBufferOverflow(int &ipDdxType);


	//

	//MWO 21.05.1999
	CString GetInternalDBFieldList();
	//END MWO
	CString GetInternalFieldList();

	CString GetInternalDataList(RecordSet *prpRecord);

	//MWO 21.05.1999
	CString GetInternalDBDataList(RecordSet *prpRecord, CString opAccessMethod = CString(""));
	//END MWO

	bool IsFieldAvailabe(CString &opField);


	CCSPtrArray<RecordSet>  *GetDataBuffer(){ return &Data; };

	void CreateKeyMap(int ipStartPos = 0);

	void SetSelection(CString opWhere) { omWhere = opWhere; };

	CString GetSelection() { return omWhere; };
	

//protected:

	void DelFromKeyMap( RecordSet *prlRecordSet);
	void UpdateRecord(RecordSet *prlRecordSet, CString opFieldList, CString opDataList);

	/*hag20030422: static */CStringArray smOrder;
	static int CompareRecord(const RecordSet **e1, const RecordSet **e2);
	
	static CCSBasic *pomBasic;

	/*hag20030422: static */CUIntArray imIndex;
	static CUIntArray *pimIndex;		//hag20030422
	static CStringArray *psmOrder;		//hag20030422

	bool bmFieldsInSM;
	CString Object;
	CString Alias;

	//MWO 21.05.99
	int imLogicalFieldCount;
	//END MWO
	CCSPtrArray<RecordSet> Data;
	CCSPtrArray<FieldSet> Desc;
	
	CCSPtrArray<BcStruct> BcBuffer;

	int BcBufferSize;

	CString omWhere;

	bool bmNumericSort;	
	static bool *pbmNumericSort;	
	
	CMapStringToPtr DdxMap;

	CMapStringToString HeaderMap;

	int FieldCount;
	// Maps Fieldname to FieldIndex
	CMapStringToPtr omIndexMap;

	//MWO 23.12.98
	CMapStringToPtr omFieldSetMap;
	//END MWO
	bool bmIsDataRead;
	bool bmIsReadAll;
	CMapStringToPtr omKeyMaps;
	CMapPtrToPtr omChangedMap;

	CString omObjectDesc;
	CString omTableExtension;

	CALLBACKFUNC	pomBcFunc;
	
	static char SystabErrorMsg[256];
	static char SystabErrorCaptionMsg[64];

private:
	static 	CedaSysTabData *pomSystab;
	friend void AatHelp::ExitApp();
public:
	// to do: read this information form systab
	bool bmCdat;
	bool bmLstu;
	bool bmUsec;
	bool bmUseu;

};

#endif // !defined(AFX_CEDAOBJECT_H__28E63D62_971E_11D1_8371_0080AD1DC701__INCLUDED_)
