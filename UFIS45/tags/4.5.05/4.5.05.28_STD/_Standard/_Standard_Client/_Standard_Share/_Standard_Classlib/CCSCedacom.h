#ifndef _CCSCEDACOM_H_
#define _CCSCEDACOM_H_

#include <ccslog.h>


//@Man:
//@Memo: Provide methods for communication with CEDA server.
//@See: CCSCedaData, UFIS.DLL
/*@Doc:
  This class has been implemented for communication with the CEDA server 
  by calls to {\bf UFIS.DLL}.
  There is (more correctly, should be) only one instance of this class.

  Notes: CCSCedaCom will do housekeeping service for making sure that the communication
  will be initialized only once, eventhough you may have many instances of CedaCom.
*/
class CCSCedaCom: public CObject
{
// Attributes
public:
    //@ManMemo: Last returned code from UFIS.DLL
    //@See: UFIS.DLL, CedaCom::LastError()
    /*@Doc:
      The {\bf imLastReturnCode} data member is a public variable containing the
      last returned code from ::InitCom(), ::CleanupCom(), and ::CallCeda().
    */
    static int imLastReturnCode;
    //@ManMemo: Display only flights which have demands

    //@ManMemo: Meaning of #imLastReturnCode#
    /*@Doc:
	  The following returncodes are defined:
	  \begin{itemize}
      \item  {\bf RC_SUCCESS	  =  0}    Everthing OK
      \item  {\bf RC_FAIL		  = -1}    General serious error
      \item  {\bf RC_COMM_FAIL	  = -2}    WINSOCK.DLL error
      \item  {\bf RC_INIT_FAIL	  = -3}    Open the log file
      \item  {\bf RC_CEDA_FAIL	  = -4}    CEDA reports an error
      \item  {\bf RC_SHUTDOWN	  = -5}    CEDA reports shutdown
      \item  {\bf RC_ALREADY_INIT = -6}    InitComm called up for a second line
      \item  {\bf RC_NOT_FOUND	  = -7}    Data not found (More than 7 applications are running)
      \item  {\bf RC_DATA_CUT	  = -8}    Data incomplete
      \item  {\bf RC_TIMEOUT	  = -9}    CEDA reports a time-out
	  \end{itemize}
	*/
    enum {
        RC_SUCCESS = 0,         // Everthing OK
        RC_FAIL = -1,           // General serious error
        RC_COMM_FAIL = -2,      // WINSOCK.DLL error
        RC_INIT_FAIL = -3,      // Open the log file
        RC_CEDA_FAIL = -4,      // CEDA reports an error
        RC_SHUTDOWN = -5,       // CEDA reports shutdown
        RC_ALREADY_INIT = -6,   // InitComm called up for a second line
        RC_NOT_FOUND = -7,      // Data not found (More than 7 applications are running)
        RC_DATA_CUT = -8,       // Data incomplete
        RC_TIMEOUT = -9         // CEDA reports a time-out
    };

// Operations
public:
    //@ManMemo: Default Constructor
    //@See: UFIS.DLL, CedaCom::InitCom
    /*@Doc:
      Create CedaCom object to initialize the communication with the CEDA server
      with default {\bf HostName} and {\bf HostType} from the configuration file.
    */
    CCSCedaCom(CCSLog *popLog = NULL, const char *pcpAppName = NULL);

    //@ManMemo: Constructor with specific HostName and HostType
    //@See: UFIS.DLL, CedaCom::InitCom
    /*@Doc:
      You could pass NULL on {\bf pcpHostName} and {\bf pcpHostType} to use the default
      settings in the configuration file.
    */
    CCSCedaCom(const char *pcpHostName, const char *pcpHostType, CCSLog *popLog = NULL, const char *pcpAppName = NULL);


    //@ManMemo: Default destructor
    /*@Doc:
      Close communication with CEDA server if there is no more CedaCom instance.
    */
    ~CCSCedaCom();

	void SetAppName(CString opName);


    //@ManMemo: Closes connection to ceda server
    //@See: UFIS.DLL, CedaCom::InitCom
    /*@Doc:
		If a connection to ceda server exist, this connection will be closed.
    */
    bool CleanUpCom();
	
	void SetUser(const char *pcpUser);

    //@ManMemo: Explicit initialization of the communication with the CEDA server
    /*@Doc:
      Close existing connection to CEDA server and establish new connection to the
      {\bm HostName/HostType} passed as parameter. If {\bm HostName/HostType} is NULL,
      read the appropriate parameter from the configuration file (in the section
      {\bf [Host]}). The configuration file must be specified in the OS environment
      named {\bf CEDA}. If there is no {\bf CEDA}, CedaCom will use {\bf C:UFIS\SYSTEM\CEDA.INI}
      as the default.

      On success, establish new connection by calling {\bf InitCom(HostName, HostType)}
      in {\bf UFIS.DLL} and return {\bf RCSuccess}. On failure, return {\bf RCFailure}.
    */
    bool Initialize(const char *pcpHostName = NULL, const char *pcpHostType = NULL);
	bool RegisterBcWindow(CWnd *BcWindow);
	bool UnRegisterBcWindow(CWnd *BcWindow);

    //@ManMemo: Meaning of the last returncode from UFIS.DLL
    //@See: UFIS.DLL, CedaCom::imLastReturnCode
    /*@Doc:
      Return the meaning of the last returned code from UFIS.DLL. This method uses the value
      kept in {\bf imLastReturnCode} data member, then returns a CString which is the
      interpreted meaning of the {\bf imLastReturnCode}.
    */
    CString LastError(void);
public:
	char pcmReqId[24];
	char pcmUser[33];
    //@ManMemo: are we connected to server now?
     static bool bmIsConnected;
    //@ManMemo: Name of the actual host
     char pcmHostName[128];
     char pcmRealHostName[128];

	 //@ManMemo: Type of the actual host
     char pcmHostType[12];
     char pcmRealHostType[12];
    //@ManMemo: full path name of the configuration file
     char pcmConfigPath[256];
    //@ManMemo: return code of last ceda action

	 //MWO: 14.07.2004
	 bool bmUseNetin;
// Internal attributes
private:
    static int imCount;             // number of instances created
	CCSLog *pomLog;
	char pcmAppName[128];
};      


#endif
