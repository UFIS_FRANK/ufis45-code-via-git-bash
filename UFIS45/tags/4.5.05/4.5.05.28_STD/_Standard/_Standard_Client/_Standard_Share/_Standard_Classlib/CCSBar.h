// CCSBar.h

#ifndef _CCSBAR_H_
#define _CCSBAR_H_

#include <afxwin.h>
#include <ccsptrarray.h>
#include <ccsbar.h>

// Frame types
enum { FRAMENONE, FRAMERECT, FRAMEBACKGROUND, THICKFRAMERECT };

enum { BD_CCSBAR, BD_TEXT, BD_CIRCLE, BD_SUBBAR,
       BD_LEFT,BD_RIGHT,BD_CENTER,BD_LEFTOFBAR,BD_RIGHTOFBAR,
       BD_TOP, BD_BOTTOM, BD_ABOVE, BD_BELOW, BD_REGION, BD_LINE,
	   BD_FRAME
};
enum { MARKNONE, MARKLEFT, MARKRIGHT, MARKFULL };

enum { TRIANGLE_NONE , TRIANGLE_RIGHT, TRIANGLE_LEFT, TRIANGLE_BOTH };

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

//@Man:  BARDECO
//@Memo: BARDECO
//@See:  GBAR
/*@Doc:
	The following struct-items are defined:
	\begin{itemize}
	\item  #int#		type;		{\bf Decorationtype}
	\item  #CString#	Value;		{\bf String to be positioned in the CRect}
	\item  #CRect#		Rect;		{\bf possibility of drawing an additional rect in bar. May be used to paint a line as specialized rect.}
	\item  #COLORREF#   Color;
	\item  #CFont#      *DecoFont;
	\end{itemize}
*/
struct CCSBarDecoStruct
{
	int			Type;					// Decorationtype
	int			HorizontalAlignment;
	int			VerticalAlignment;
	//char		Text[128];		// String to be positioned in the CRect
	//CString Text;
	const char *Text;
	CRect		Rect;			// possibility of drawing an additional rect in bar
										// may be used to paint a line as specialized rect
	COLORREF	BkColor;
	COLORREF	TextColor;
	int			FrameType;
	int			FramePixel;
	int			MarkerType;
	CBrush		*MarkerBrush;
	CBrush		*FrameBrush;
	CFont		*TextFont;
	CPen		*pPen;
	CRgn		*Region;
	POINT		PointFrom;
	POINT		PointTo;

	struct CCSBarDecoStruct(void)
	{
		memset(this,'\0',sizeof(*this));
		}
};
typedef struct CCSBarDecoStruct CCSBARDECO;

// Marker types

/////////////////////////////////////////////////////////////////////////////
// CTestViewer window
 

/////////////////////////////////////////////////////////////////////////////
// Class declaration of CCSGanttBar

//@Man:
//@Memo: Base class
//@See:  CCSPTARR
/*@Doc:
  Base class.
*/
class CCSGanttBar
{
// Construction
public:
    //@ManMemo: Default constructor
    CCSGanttBar();
    //@ManMemo: More explicit constructor
    CCSGanttBar(CDC *popDC,CCSPtrArray<CCSBARDECO> &ropDeco);


// Operations
public:
    //@ManMemo: Paint
    void Paint(CDC *popDC,CCSPtrArray<CCSBARDECO> &ropDeco);
private:
	void CCSGanttBar::PaintBar(CDC *popDC,CCSBARDECO *prpDeco);
	void CCSGanttBar::DrawText(CDC *popDC,CCSBARDECO *prpDeco);
	void CCSGanttBar::DrawFrame(CDC *popDC,CCSBARDECO *prpDeco);
	void CCSGanttBar::PaintDeco(CDC *popDC,CCSBARDECO *prpDeco);

};

#endif
