//---------------------------------------------------------------------------------------------------------------
// FILE:	
//
// Class:			CCSTime
//
// Derived from:	CTime
//
// Provides:		additional functions for working with time/hour/strings
//
// Author:			MWO
//
// Date:			23.04.1997
//---------------------------------------------------------------------------------------------------------------
#ifndef __CCSTIME__
#define __CCSTIME__

#include <afxwin.h>

static int g_dayInmonth[] =
{ 
    31, 28, 31, 30, 31, 30, 
    31, 31, 30, 31, 30, 31
};

BOOL CheckDateValid( CString str ); 
BOOL CheckMonthValid( CString str ); 
BOOL CheckYearValid( CString str );
BOOL CheckDDMMYYValid( CString &str ); 
BOOL CheckOleDDMMYYValid( CString &str );
BOOL CheckDDMMValid( CString &str ); 
BOOL CheckHHMMValid( CString &str );
void WINAPI DDX_CCSDate( CDataExchange *pDX, int nIDC, CTime& tm ); 
void WINAPI DDX_CCSMonth( CDataExchange *pDX, int nIDC, CTime& tm ); 
void WINAPI DDX_CCSYear( CDataExchange *pDX, int nIDC, CTime& tm );
void WINAPI DDX_CCSddmmyy( CDataExchange *pDX, int nIDC, CTime& tm );
void WINAPI DDX_CCSTime( CDataExchange *pDX, int nIDC, CTime& tm );
void WINAPI DDX_SSSNumeric( CDataExchange *pDX, int nIDC, CString& str );
void WINAPI DDX_CCSddmm( CDataExchange *pDX, int nIDC, CTime& tm );
void AFXAPI DDX_CCSFString(CDataExchange* pDX, int nIDC, CString& value);


bool CheckUtcPast(CTime &opTime, int ipDays = 2);

bool DaysinPeriod(CTime olMinDate, CTime olMaxDate, CString &olDays);

bool GetDayOfWeek(CTime &opDate, char *pcpDest);
int GetDayOfWeek(CTime &opDate);


CString CTimeToDBString(CTime &opDateTime,CTime &opDate);
CTime DBStringToDateTime(CString &opString);

COleDateTime OleDateStringToDate(CString &opString);
CTime DateStringToDate(CString &opString);
CTime HourStringToDate(CString &opString, CTime olTime,bool blSet = true);
CTime HourStringToDate(CString &opString,bool blSet = true);
CTime DateHourStringToDate(CString &opDateString, CString &opHourString);
CString DateToHourDivString(CTime &opDateTime,CTime &opDate);

void UtcToLocal(CTime &opUtc);
void LocalToUtc(CTime &opLocal);

int GetOffset(CString &opString);

#endif //__CCSTIME__
