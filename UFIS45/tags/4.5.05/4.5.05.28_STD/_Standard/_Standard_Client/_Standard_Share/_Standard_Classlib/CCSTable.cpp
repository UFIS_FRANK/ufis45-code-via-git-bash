// table.cpp - Displays data as a list of textlines
// 
// Notes: I decided to derive the class CTable from the standard MFC CWnd.
// This window will draw the upper of its client-area as table header bar, and
// its lower will be the standard MFC CListBox.
//
// Most routines in this class will assume that the table window is exist if
// the derived data member "m_hWnd" is not NULL.
//
//
// Written by:
// Damkerng Thammathakerngkit   April 16, 1996
//
// Redesigned: MWO 10/96 
// 
// MWO:
// Modifikation History:
// MWO 18.02.1997:		Attributes for no vertical ridges
//						Atrributes for vertical ridges
//						Atrributes for no standard ridges
//
// MWO: 17.03.1997		SetColumnEditable
//		 				SetShowSelection
// MWO: 18.03.1997		Starting Implementation of inplace editing
//
// MWO: 26.03.1997		Try to implement docking columns in a seperate Window
//
// MWO: 22.04.1997		SetColumnType: Means TextLimit, (Cell)Type and Format(String)
//						This information is used by inplace-edit-field for checking input
//
// ARE  13.04.2000      DisplayTable() um DisplayTable(UINT nBar = SB_BOTH) erweitert
//                      ermoglicht das initiale ein- und ausschalten der ScrollBar
//                      nBar >> SB_BOTH, SB_HORZ oder SB_VERT
//
// TVO: 13.11.2000		'GetHeader' added	
//
// RKR: 19.04.2001		::MakeInplaceEdit(); check of bmIPEwithoutALT-flag.
//						PRF 1352 LIS
//
//						::PreTranslateMessage(MSG* pMsg) added.
//						sends message if return-key is pressed.
//						PRF 1358 PRODUCT4.4



#include <stdlib.h>
#include <CCSDefines.h>
#include <ccsptrarray.h>
#include <ccsdragdropctrl.h>
#include <CCSTable.h>
//#include "ccsobj.h"
//#include "cedadata.h"



#define BLACK   RGB(  0,   0,   0)
#define MAROON  RGB(128,   0,   0)          // dark red
#define GREEN   RGB(  0, 128,   0)          // dark green
#define OLIVE   RGB(128, 128,   0)          // dark yellow
#define NAVY    RGB(  0,   0, 128)          // dark blue
#define PURPLE  RGB(128,   0, 128)          // dark magenta
#define TEAL    RGB(  0, 128, 128)          // dark cyan
#define GRAY    RGB(128, 128, 128)          // dark gray
//#define SILVER  RGB(192, 192, 192)          // light gray
#define SILVER  ::GetSysColor(COLOR_BTNFACE)          // light gray
#define RED     RGB(255,   0,   0)
#define ORANGE  RGB(255, 132,   0)
#define LIME    RGB(  0, 255,   0)          // green
#define YELLOW  RGB(255, 255,   0)
#define BLUE    RGB(  0,   0, 255)
#define FUCHSIA RGB(255,   0, 255)          // magenta
#define AQUA    RGB(  0, 255, 255)          // cyan
#define WHITE   RGB(255, 255, 255)


// default font (in stock object) used when no font specified
#define DEFAULT_FONT    omCourier_Regular_10//ANSI_FIXED_FONT

// spacing between the font and the dotted-line
#define EXTRASPACE	2
bool IsEscapePressed;

BEGIN_MESSAGE_MAP(CCSTableListBox, CListBox)
    //{{AFX_MSG_MAP(CCSTableListBox)
	ON_WM_KEYDOWN()
    ON_WM_LBUTTONDBLCLK()
    ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
    ON_WM_PAINT()
	ON_WM_ERASEBKGND()
    ON_WM_MOUSEMOVE()
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	ON_CONTROL_REFLECT(LBN_SELCHANGE, SelectionHasBeenChanged)
	ON_CONTROL_REFLECT(LBN_KILLFOCUS, OnKillfocus)
	ON_WM_KILLFOCUS()
	ON_MESSAGE(WM_EDIT_RBUTTONDOWN, OnEditRButtonDown)
	ON_MESSAGE(WM_EDIT_LBUTTONDBLCLK, OnEditLButtonDblClk)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

	//ON_COMMAND(LBN_SELCHANGE, SelectionHasBeenChanged)


void CCSTableListBox::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CListBox::OnKeyDown(nChar, nRepCnt, nFlags);
	pomTableWindow->pomParentWindow->SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
}



void CCSTableListBox::OnKillfocus() 
{
	//pomTableWindow->KillFocus();
}

void CCSTableListBox::OnKillFocus(CWnd* pNewWnd) 
{
	//pomTableWindow->KillFocus();
	
	CListBox::OnKillFocus(pNewWnd);
}




void CCSTableListBox::SelectionHasBeenChanged()
{
	pomTableWindow->pomParentWindow->SendMessage(WM_TABLE_SELCHANGE, 0, 0);
	return ;
}

// GetItemFromPoint: Return the item ID of the list box based on the given "point"
int CCSTableListBox::GetItemFromPoint(CPoint point)
{
    CRect rcItem;

	BOOL blResult;
	/*UINT nItem = */
	ItemFromPoint(point,blResult);
	for (int itemID = GetTopIndex(); itemID < GetCount(); itemID++)
    {
        GetItemRect(itemID, &rcItem);
        if (rcItem.PtInRect(point))
            return itemID;
    }
    return -1;
}

// returns the column specified by a point
int CCSTableListBox::GetColumnFromPoint(CPoint opPoint)
{
	int x = opPoint.x + abs(pomTableWindow->imTableContentOffset);
	for (int ilLc = 0; ilLc < pomTableWindow->omLBorder.GetSize(); ilLc++)
	{
		long x1, x2;
		x1 = pomTableWindow->omLBorder[ilLc];
		x2 = pomTableWindow->omRBorder[ilLc];
		if ((int)pomTableWindow->omLBorder[ilLc] < x && x < (int)pomTableWindow->omRBorder[ilLc])
		{
			return ilLc;
		}
	}
	return -1;
}

// DetectCurrentColumn: Update the member "imCurrentColumn" of the parent CCSTable
void CCSTableListBox::OnMouseMove(UINT nFlags, CPoint point)
{
	int ilX = GetColumnFromPoint(point);
	int ilY = GetItemFromPoint(point);

	if(!(nFlags & MK_LBUTTON))
	{
		CListBox::OnMouseMove(nFlags,point);
		return;
	}

	if(ilX != -1 && ilY != -1)
	{
		if(ilY < pomTableWindow->omLines.GetSize())
		{
			if(ilX < pomTableWindow->omLines[ilY].Columns.GetSize())
			{

				CCSTABLENOTIFY olNotify;
				olNotify.SourceTable = pomTableWindow;
				olNotify.Line = ilY;
				olNotify.Column = ilX;
				CWnd *pWnd = pomTableWindow->pomParentWindow;	// surrounding window

				
				//For grouping the selected columns while the control key is down.
				if ((nFlags & MK_LBUTTON) && (nFlags & MK_CONTROL))
				{
					pWnd->SendMessage(WM_TABLE_CTRL_GROUP,ilY,(LPARAM)&olNotify);
					return;
				}
				

				if (pomTableWindow->imSelectMode & (LBS_MULTIPLESEL | LBS_EXTENDEDSEL))	// multiple selection?
				{
					if (ilY != LB_ERR && GetSel(ilY))
					{												// user start dragging?
						pWnd->SendMessage(WM_TABLE_DRAGBEGIN, ilY, (LPARAM)&olNotify);
						return;	// don't call the default action of CListBox if some dragging performed
					}
					else
					{
						//SetSel(-1, false);
					}
				}
				else
				{
					if (ilY != LB_ERR)
					{
						//TVO SetCurSel(ilY);
						pWnd->SendMessage(WM_TABLE_DRAGBEGIN, ilY, (LPARAM)&olNotify);
						return;	// don't call the default action of CListBox if some dragging performed
					}
				}
			}
		}
	}
	CListBox::OnMouseMove(nFlags,point);

}

void CCSTableListBox::DetectCurrentColumn(CPoint point)
{
	int x = point.x + abs(pomTableWindow->imTableContentOffset);
	for (int ilLc = 0; ilLc < pomTableWindow->omLBorder.GetSize(); ilLc++)
	{
		long x1, x2;
		x1 = pomTableWindow->omLBorder[ilLc];
		x2 = pomTableWindow->omRBorder[ilLc];
		if ((int)pomTableWindow->omLBorder[ilLc] < x && x < (int)pomTableWindow->omRBorder[ilLc])
		{
			pomTableWindow->imCurrentColumn = ilLc;
			return;
		}
	}

	// the user click on outside
	pomTableWindow->imCurrentColumn = -1;
}

//get coordinates of first visible column
bool CCSTableListBox::GetVisibleLeftTopColumn(int &ripX, int &ripY)
{
	CRect olRect;
	
	GetClientRect(&olRect);
	CPoint olTopLoc = CPoint(olRect.left+1, olRect.top+1);
	ripX = GetColumnFromPoint(olTopLoc);
	ripY = GetItemFromPoint(olTopLoc);
	if(ripX != -1 && ripY != -1)
	{
		return true;
	}
	else
	{
		return false;
	}
}

int CCSTableListBox::GetFirstVisibleItem()
{
	return GetTopIndex();
}
int CCSTableListBox::GetLastVisibleItem()
{
	CRect olRect;
	int ilLastPoint;
	int ilCount=0;
	int ilItemHeight;
	GetClientRect(&olRect);
	ilLastPoint = olRect.bottom;
	while(olRect.top < olRect.bottom)
	{
		ilItemHeight = GetItemHeight(ilCount+GetTopIndex());
		olRect.top = olRect.top + ilItemHeight;
		ilCount++;
	}
	return (ilCount+GetTopIndex());
}
//get coordinate of last visible column
bool CCSTableListBox::GetVisibleRightBottomColumn(int &ripX, int &ripY)
{
	CRect olRect;
	CPoint olOrigPoint;
	GetClientRect(&olRect);
	CPoint olBottomLoc = CPoint(olRect.right-1, olRect.bottom-1);
	olOrigPoint = olBottomLoc;
	ripY = GetItemFromPoint(olBottomLoc);
	while(ripY == -1 && olBottomLoc.y >= 1)
	{
		olBottomLoc.y = olBottomLoc.y - pomTableWindow->imTextFontHeight+1;
		ripY = GetItemFromPoint(olBottomLoc);
	}
	if(ripY == -1)
	{
		return false;
	}
	olBottomLoc = olOrigPoint;
	ripX = GetColumnFromPoint(olBottomLoc);
	while(ripX == -1 && olBottomLoc.x > 1 )
	{
		olBottomLoc.x = olBottomLoc.x - 3;
		ripX = GetColumnFromPoint(olBottomLoc);
	}
	if(ripX == -1)
	{
		return false;
	}
	return true;
}


// OnLButtonDblClk: Notify the parent window of CCSTable "There's a double-click down here".
void CCSTableListBox::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	nFlags;
    //Double click action should be avoided, while the control key is down.
	if(nFlags & MK_CONTROL)
		return;
	

	if(pomTableWindow->omLines.GetSize() == 0)
	{
		return;
	}

	CCSTABLENOTIFY olNotify;
	DetectCurrentColumn(point);

    int itemID = GetCaretIndex();
	CWnd *pWnd = pomTableWindow->pomParentWindow;	// surrounding window

	if(itemID > pomTableWindow->omLines.GetSize())
	{
		return;
	}
	olNotify.SourceTable = pomTableWindow;
	olNotify.Line = itemID;
	olNotify.Column = pomTableWindow->imCurrentColumn;
	if(pomTableWindow->imCurrentColumn >= 0 && pomTableWindow->omLines[itemID].Columns.GetSize() > pomTableWindow->imCurrentColumn)
	{
		olNotify.Data = (void*)&(pomTableWindow->omLines[itemID].Columns[pomTableWindow->imCurrentColumn]);
	}
	olNotify.Point = point;

    pWnd->SendMessage(WM_TABLE_LBUTTONDBLCLK, itemID, (LPARAM)&olNotify);
}

// OnLButtonDblClk: Notify the parent window of CCSTable "There's a dragging/click down here".
void CCSTableListBox::OnLButtonDown(UINT nFlags, CPoint point)
{
	DetectCurrentColumn(point);
	
	CWnd *pWnd = pomTableWindow->pomParentWindow;	// surrounding window

	int itemID = GetItemFromPoint(point);

	bool blIPEdit = false;

	/* RST!!! this was inserted for 4.6 but through this multi-select is not more possible - needs to be checked later
	//No column should be selected while the <Shift> key down.
	if(nFlags & MK_SHIFT)
		return;
	//For grouping, columns should be selected while <Ctrl> key down.
	if(nFlags & MK_CONTROL)
	{
		if(pomTableWindow->imCurrentColumn < 0 || itemID == -1 ||pomTableWindow->omLines.GetSize() == 0 
		   || pomTableWindow->omLines[itemID].Columns.GetSize() <= pomTableWindow->imCurrentColumn)
		{			
			return;
		}
		
		CCSTABLENOTIFY olNotify;		
		olNotify.SourceTable = pomTableWindow;
		olNotify.Line = itemID;
		olNotify.Column = pomTableWindow->imCurrentColumn;
		if(pomTableWindow->imCurrentColumn >= 0 && pomTableWindow->omLines[itemID].Columns.GetSize() > pomTableWindow->imCurrentColumn)
		{
			olNotify.Data = (void*)&(pomTableWindow->omLines[itemID].Columns[pomTableWindow->imCurrentColumn]);
		}
		olNotify.Point = point;
		pWnd->SendMessage(WM_TABLE_CTRL_DOWN, itemID, (LPARAM)&olNotify);
		return;
	}
	*/

	//unsigned short ilRet = GetKeyState(VK_MENU);
	if((GetKeyState(VK_MENU) & 0xff80) || pomTableWindow->bmMiniTable || pomTableWindow->bmIPEwithoutALT) // is <ALT>-Key pressed ?
	{
		
		pomTableWindow->EndInplaceEditing();
		blIPEdit = pomTableWindow->MakeInplaceEdit(itemID);
	}



	if(!blIPEdit)
	{
		if(pomTableWindow->omLines.GetSize() == 0)
		{
			return;
		}

		CCSTABLENOTIFY olNotify;
		DetectCurrentColumn(point);
		itemID = GetItemFromPoint(point);

		if(itemID > pomTableWindow->omLines.GetSize() || itemID < 0)
		{
			return;
		}
		olNotify.SourceTable = pomTableWindow;
		olNotify.Line = itemID;
		olNotify.Column = pomTableWindow->imCurrentColumn;
		if(pomTableWindow->imCurrentColumn >= 0 && pomTableWindow->omLines[itemID].Columns.GetSize() > pomTableWindow->imCurrentColumn)
		{
			olNotify.Data = (void*)&(pomTableWindow->omLines[itemID].Columns[pomTableWindow->imCurrentColumn]);
		}
		olNotify.Point = point;


		if(pomTableWindow->bmMultiDrag)
		{
			if(pomTableWindow->imSelectMode & (LBS_MULTIPLESEL | LBS_EXTENDEDSEL))	// multiple selection?
			{
				if (itemID != LB_ERR && pomTableWindow->omDragEnabled[itemID] && GetSel(itemID))
				{												// user start dragging?
					pWnd->SendMessage(WM_TABLE_DRAGBEGIN, itemID, (LPARAM)&olNotify);
					return;	// don't call the default action of CListBox if some dragging performed
				}
			}
			else
			{
				if (itemID != LB_ERR && pomTableWindow->omDragEnabled[itemID])
				{
					SetCurSel(itemID);
					pWnd->SendMessage(WM_TABLE_DRAGBEGIN, itemID, (LPARAM)&olNotify);
					return;	// don't call the default action of CListBox if some dragging performed
				}
			}
		}

		CListBox::OnLButtonDown(nFlags, point);

		// copy all column-info into CCSTABLENOTIFY

		pWnd->SendMessage(WM_TABLE_LBUTTONDOWN, itemID, (LPARAM)&olNotify);
		//pWnd->SendMessage(WM_TABLE_LBUTTONDOWN, itemID, (LPARAM)this);
	}
}



void CCSTableListBox::OnRButtonDown(UINT nFlags, CPoint point)
{
	nFlags;
	if(pomTableWindow->omLines.GetSize() == 0)
	{
		return;
	}

	int itemID;
	CCSTABLENOTIFY olNotify;
	DetectCurrentColumn(point);
	itemID = GetItemFromPoint(point);
	CWnd *pWnd = pomTableWindow->pomParentWindow;	// surrounding window

	if(itemID > pomTableWindow->omLines.GetSize() || itemID < 0)
	{
		return;
	}
	olNotify.SourceTable = pomTableWindow;
	olNotify.Line = itemID;
	olNotify.Column = pomTableWindow->imCurrentColumn;
	if(pomTableWindow->imCurrentColumn >= 0 && pomTableWindow->omLines[itemID].Columns.GetSize() > pomTableWindow->imCurrentColumn)
	{
		olNotify.Data = (void*)&(pomTableWindow->omLines[itemID].Columns[pomTableWindow->imCurrentColumn]);
	}
	olNotify.Point = point;

   pWnd->SendMessage(WM_TABLE_RBUTTONDOWN, itemID, (LPARAM)&olNotify);

   pomTableWindow->SendMessage(WM_TABLE_RBUTTONDOWN, itemID, (LPARAM)&olNotify);


}


LONG CCSTableListBox::OnEditLButtonDblClk( UINT wParam, LPARAM lParam)
{
	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY *)lParam;

	CPoint point = prlNotify->Point;

	ScreenToClient(&point);

	OnLButtonDblClk(0,point);

	return 0L;

}


LONG CCSTableListBox::OnEditRButtonDown( UINT wParam, LPARAM lParam)
{
	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY *)lParam;

	CPoint point = prlNotify->Point;

	ScreenToClient(&point);

	if(pomTableWindow->omLines.GetSize() == 0)
	{
		return 0L;
	}

	int itemID;
	CCSTABLENOTIFY olNotify;
	DetectCurrentColumn(point);
	itemID = GetItemFromPoint(point);
	CWnd *pWnd = pomTableWindow->pomParentWindow;	// surrounding window

	if(itemID > pomTableWindow->omLines.GetSize() || itemID < 0)
	{
		return 0L;
	}
	olNotify.SourceTable = pomTableWindow;
	olNotify.Line = itemID;
	olNotify.Column = pomTableWindow->GetColumnnoFromPoint(point);
	pomTableWindow->imCurrentColumn = olNotify.Column;

	if(pomTableWindow->imCurrentColumn >= 0 && pomTableWindow->omLines[itemID].Columns.GetSize() > pomTableWindow->imCurrentColumn)
	{
		olNotify.Data = (void*)&(pomTableWindow->omLines[itemID].Columns[pomTableWindow->imCurrentColumn]);
	}
	olNotify.Point = point;

   pWnd->SendMessage(WM_TABLE_RBUTTONDOWN, itemID, (LPARAM)&olNotify);

   pomTableWindow->SendMessage(WM_TABLE_RBUTTONDOWN, itemID, (LPARAM)&olNotify);

	return 0L;
}





LONG CCSTableListBox::OnDragOver(UINT wParam, LONG lParam)
{
	long llRc;

	CPoint olPoint;
    ::GetCursorPos(&olPoint);
	DetectCurrentColumn(olPoint);

	CWnd *pWnd = pomTableWindow->pomParentWindow;	// surrounding window
    llRc = pWnd->SendMessage(WM_DRAGOVER, wParam, lParam);
	return llRc;
}

LONG CCSTableListBox::OnDrop(UINT wParam, LONG lParam)
{
	long llRc;

	CPoint olPoint;
    ::GetCursorPos(&olPoint);
	DetectCurrentColumn(olPoint);

	CWnd *pWnd = pomTableWindow->pomParentWindow;	// surrounding window
    llRc = pWnd->SendMessage(WM_DROP, wParam, lParam);
	return llRc;
}

// CCSTableListBox::OnEraseBkgnd: Remember the clip-box for the paint routine
BOOL CCSTableListBox::OnEraseBkgnd(CDC* pDC) 
{
	pDC->GetClipBox(omClipBox);
	return TRUE;
}

// CCSTableListBox::OnPaint Redraw the entire table content if it's empty.
void CCSTableListBox::OnPaint()
{

    CListBox::OnPaint();
    if (GetCount() == 0)	// table is empty?
	{
        // reset table content beginning offset (in pixels)
        pomTableWindow->imTableContentOffset = 0;
		  pomTableWindow->imTableYOffset;

        // calculate size and position of each line
        CRect rect;
        GetClientRect(&rect);
        int bottom = rect.bottom;

        // draw the empty entire table content line-by-line
        CClientDC dc(this);
		int height = pomTableWindow->imTextFontHeight + EXTRASPACE;
        while (rect.top <= bottom)
        {
            rect.bottom = min(rect.top + height, bottom);
            pomTableWindow->DrawItem(dc, 0, rect, false, false, false);
            rect.top += height;
        }
	}
	// Want to know that if the painting region is intersect with one of the string.
	// This will fix bug which table not update the lower region when there is no data there.
	else if (GetCount() > 0)
	{
		CRect rcItem;
		GetItemRect(GetCount()-1, rcItem);
		if (rcItem.bottom < omClipBox.top)	// need some fixing?
		{
			InvalidateRect(NULL);
		}
    }
};


void CCSTableListBox::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// TODO: Add your message handler code here and/or call default
 
	CListBox::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CCSTableListBox::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// TODO: Add your message handler code here and/or call default
 
	CRect olRect;
	
	GetClientRect(&olRect);

	CListBox::OnVScroll(nSBCode, nPos, pScrollBar);
	pomTableWindow->DrawLeftScale();
	pomTableWindow->DrawRightScale();
	//pomTableWindow->MoveIPEdit();
}

/////////////////////////////////////////////////////////////////////////////
// CCSTable

BEGIN_MESSAGE_MAP(CCSTable, CWnd)
    //{{AFX_MSG_MAP(CCSTable)
    ON_WM_PAINT()
    ON_WM_MEASUREITEM()
    ON_WM_DRAWITEM()
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
    ON_MESSAGE(WM_EDIT_MOVE_IP_RIGHT, OnMoveInplaceRight)  
    ON_MESSAGE(WM_EDIT_MOVE_IP_LEFT, OnMoveInplaceLeft)  
    ON_MESSAGE(WM_EDIT_MOVE_IP_UP , OnMoveInplaceUp)  
    ON_MESSAGE(WM_EDIT_MOVE_IP_DOWN, OnMoveInplaceDown)  
    ON_MESSAGE(WM_EDIT_IP_RETURN, OnInplaceReturn)  
    ON_MESSAGE(WM_EDIT_IP_END, OnEndInplaceEditing)  
	 ON_NOTIFY_EX( TTN_NEEDTEXT, 0, OnToolTipNotify)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnEditKillfocus)
	ON_MESSAGE(WM_EDIT_CHANGED, OnEditChanged)
	ON_MESSAGE(WM_EDIT_MENU_SELECT, OnMenuSelect2)
	ON_MESSAGE(WM_TABLE_RBUTTONDOWN, OnTableRButtonDown)
	ON_COMMAND_RANGE( 100, 200, OnMenuSelect )	
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CCSTable::SetSelectMode(int ipSelectMode)
{
	imSelectMode = ipSelectMode;
}

// create an empty CCSTable object
CCSTable::CCSTable()
{
    pomListBox = new CCSTableListBox;
    
	pomListHeader = new CCSHeaderTable;
	bmEnableHeaderDragDrop = false;
	
    
	imLeftScaleWidth = 0;
	//pomLeftScale = NULL;
    // indicates no initialization yet
    bmInited = false;
	blShowSelection = true;
	bmDefaultSeparator = true;
	IsInplaceEditMode = false;
	imLeftDockingColumns = 0; // Initial no docking
	imRightDockingColumns = 0;
	imSelectMode = LBS_MULTIPLESEL | LBS_EXTENDEDSEL;

	blIsForDockingOnly = false;
	bmEditable = false;
    // initialize data for searching routine
    imNextLineToSearch = 0;
    omLastSearchText = "";
	imHeaderSpacing = 5;
	//pomInplaceEdit = NULL;
	pomIPEdit = NULL;
	imCurrentEditLine = -1;
	imCurrentEditColumn = -1;

	imCurrentEditPosX = -1; 
	imCurrentEditPosY = -1;
	imLeftTopMostItem = -1;
	imRightBottomMostItem= -1;
	imNextControlID = WM_USER+900;
    // prepare helper drawing objects
    FacePen.CreatePen(PS_SOLID, 1, ::GetSysColor(COLOR_BTNFACE));
    ShadowPen.CreatePen(PS_SOLID, 1, ::GetSysColor(COLOR_BTNSHADOW));
    WhitePen.CreatePen(PS_SOLID, 1, ::GetSysColor(COLOR_WINDOW));
    BlackPen.CreatePen(PS_SOLID, 1, ::GetSysColor(COLOR_WINDOWFRAME));
	DashDotPen.CreatePen(PS_DASHDOT, 1, COLORREF(BLACK));
	DotPen.CreatePen(PS_DOT, 1, COLORREF(BLACK)); 
	DashPen.CreatePen(PS_DASH, 1, COLORREF(BLACK));

    FaceBrush.CreateSolidBrush(::GetSysColor(COLOR_BTNFACE));


    CDC dc;
    dc.CreateCompatibleDC(NULL);

    LOGFONT lolFont;
    memset(&lolFont, 0, sizeof(LOGFONT));

	lolFont.lfCharSet= DEFAULT_CHARSET;
    lolFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    lolFont.lfWeight = FW_NORMAL;
    lolFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(lolFont.lfFaceName, "Courier New");
    omMSSansSerif_Regular_8.CreateFontIndirect(&lolFont);

	lolFont.lfCharSet= DEFAULT_CHARSET;
    lolFont.lfHeight = - MulDiv(10, dc.GetDeviceCaps(LOGPIXELSY), 72);
    lolFont.lfWeight = FW_NORMAL;
    lolFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(lolFont.lfFaceName, "Courier New");
    omCourier_Regular_10.CreateFontIndirect(&lolFont);

	//RST!
	bmMiniTable = false;
	imUserStyle	 = 0;
	pomToolTip = NULL;
	 pomListBox->pomToolTip = NULL;
	 pomListBox->pomToolTipStaic = NULL;
	 EnableToolTips(false);
    // prepare the horizontal separator brush pattern
    CBitmap Bitmap;
    static WORD separator[] = { 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa };

    Bitmap.CreateBitmap(8, 8, 1, 1, separator);
    SeparatorBrush.CreatePatternBrush(&Bitmap);
    Bitmap.DeleteObject();

	bmEndIPEditByLostFocus = true;

	bmIPEwithoutALT = false;

	bmMultiDrag = false;

	imCurrentColumn = 0;
}



// create a CCSTable object and initialize data members
CCSTable::CCSTable(CWnd *popParentWindow,
    int ipXStart, int ipXEnd, int ipYStart, int ipYEnd,
    COLORREF lpTextColor, COLORREF lpTextBkColor,
    COLORREF lpHighlightColor, COLORREF lpHighlightBkColor,
    CFont *popTextFont, CFont *popHeaderFont)
{
    CCSTable();

	//pomLeftScale = NULL;
	imLeftScaleWidth = 0;
	pomParentWindow = popParentWindow;
    SetTableData(popParentWindow,
        ipXStart, ipXEnd, ipYStart, ipYEnd,
        lpTextColor, lpTextBkColor,
        lpHighlightColor, lpHighlightBkColor,
        popTextFont, popHeaderFont);
}
 
 // destructor for a CCSTable object
CCSTable::~CCSTable()
{
	int ilCount;
 	if(pomListBox != NULL)
	{
    delete pomListBox;
		pomListBox = NULL;
	}

	if(pomListHeader != NULL)
	{
		delete pomListHeader;
		pomListHeader = NULL;
	}

	omCourier_Regular_10.DeleteObject();
	omMSSansSerif_Regular_8.DeleteObject();

    // destroy helper drawing objects of this instances
    FacePen.DeleteObject();
    ShadowPen.DeleteObject();
    WhitePen.DeleteObject();
    BlackPen.DeleteObject();
    FaceBrush.DeleteObject();
    SeparatorBrush.DeleteObject();
	ilCount = omLines.GetSize();
	while(omLines.GetSize() > 0)
	{
		while(omLines[0].Columns.GetSize() > 0)
		{
			//if(omLines[0].Columns[0].EditAttrib != NULL)
			//	delete omLines[0].Columns[0].EditAttrib;
			omLines[0].Columns.DeleteAt(0);
		}
		omLines.DeleteAt(0);
	}
	omHeaderDataArray.DeleteAll();
	omTableFieldArray.DeleteAll(); 
	if(pomIPEdit != NULL)
	{
		pomIPEdit->DestroyWindow();
		delete pomIPEdit;
		pomIPEdit = NULL;
	}


	//if(pomLeftScale != NULL)
	//{
	//	delete pomLeftScale;
	//}
	for (ilCount = 0; ilCount < omMenuItems.GetSize(); ilCount++)
	{
		omMenuItems[ilCount].Items.DeleteAll();
	}
	omMenuItems.DeleteAll();

	OnDestroy();
}

// change parameters of the created CCSTable instance
bool CCSTable::SetTableData(CWnd *popParentWindow,
    int ipXStart, int ipXEnd, int ipYStart, int ipYEnd,
    COLORREF lpTextColor, COLORREF lpTextBkColor,
    COLORREF lpHighlightColor, COLORREF lpHighlightBkColor,
    CFont *popTextFont, CFont *popHeaderFont)
{
    // destroy the window if parent window changed
    if (popParentWindow != pomParentWindow)
        DestroyWindow();    // this works even if m_hWnd == NULL.

    // initialize all data members
    pomParentWindow = popParentWindow;
    imXStart = min(ipXStart, ipXEnd);   // ensure positive height and width
    imXEnd = max(ipXStart, ipXEnd);
    imYStart = min(ipYStart, ipYEnd);
    imYEnd = max(ipYStart, ipYEnd);
    lmTextColor = lpTextColor;
    lmTextBkColor = lpTextBkColor;
    lmHighlightColor = lpHighlightColor;
    lmHighlightBkColor = lpHighlightBkColor;
    pomTextFont = popTextFont;
    pomHeaderFont = popHeaderFont;

    // indicate that window information is initialized
    bmInited = true;

    // update object positions.
    // Warning: CalculateWidgets() have to be done before SetPosition() in case if header
    // font size changed.
    //
    CalculateWidgets();
    SetPosition(imXStart, imXEnd, imYStart, imYEnd);
    if (m_hWnd != NULL)
    {
		pomListBox->SetItemHeight(0, imTextFontHeight + EXTRASPACE);
        pomListBox->SetHorizontalExtent(imHorizontalExtent);
		imItemHeight = imTextFontHeight + EXTRASPACE;
		//pomListBox->SetScrollRange(SB_HORZ,0,imHorizontalExtent); 
    }

    ////RC(true);
    return true;
}


//RST!
void CCSTable::SetMiniTable(bool bpModus)
{
	bmMiniTable = bpModus;
	bmIPEwithoutALT = true;
}

int CCSTable::GetLinesCount()
{
	return omLines.GetSize();
}


void CCSTable::SetStyle(int ipStyle)
{ 
	imUserStyle = ipStyle;
}


void CCSTable::EndIPEditByLostFocus(bool bpSet)
{
	bmEndIPEditByLostFocus = bpSet;
}

//RST END


int CCSTable::GetTableHeight()
{
	CRect olRect;
	GetWindowRect(&olRect);
	return (olRect.bottom - olRect.top);
}

// display the table (also create window if necessary)
// ScrollBar => SB_BOTH, SB_HORZ oder SB_VERT
bool CCSTable::DisplayTable(UINT nBar /*= SB_BOTH*/)
{
    if (!bmInited)  // uninitialized?
    {
        //RC(false);
        return false;
    }

	CalculateWidgets();
    if (m_hWnd == NULL) // window was not created yet?
    {
        // create the table window
        //DWORD lWndStyle = WS_CHILD | WS_BORDER | WS_VISIBLE;
		CalculateWidgets();
		DWORD lWndStyle = WS_CHILD | WS_VISIBLE;
		 lWndStyle |= imUserStyle;
        CRect rectWnd(imXStart, imYStart, imXEnd/*-imXStart+1*/, imYEnd/*-imYStart+1*/);

		int ilRightOffset = 0;

		//Create the window if the table has docking columns
		if(imLeftDockingColumns > 0)
		{
			//int ilRight = 0;
			int ilC = omLines.GetSize();
			if(ilC > 0)
			{
				int ilC2 = omLines[0].Columns.GetSize();
				if(ilC2 >= imLeftDockingColumns)
				{
					imLeftScaleWidth = omRBorder[imLeftDockingColumns-1]+5;
					//MWO
			        CRect rectWnd(imXStart/*+imLeftScaleWidth*/, imYStart, imXEnd/*-imXStart+1*/, imYEnd/*-imYStart+1*/);
					if (Create(NULL, NULL, lWndStyle, rectWnd, pomParentWindow, 1) == 0)
					{
						//RC(false);
						return false;
					}
				}
			}
		}
		else
		{
	        CRect rectWnd(imXStart, imYStart, imXEnd/*-imXStart+1*/, imYEnd/*-imYStart+1*/);
			if (Create(NULL, NULL, lWndStyle, rectWnd, pomParentWindow, 1) == 0)
			{
				//RC(false);
				return false;
			}
		}

      // create header bar (CRect only, not real window) for OnPaint routine
      GetClientRect(&omHeaderBarRect);
	if(!bmMiniTable)
		omHeaderBarRect.bottom = omHeaderBarRect.top + imHeaderFontHeight + 7;
	else
	{
		omHeaderBarRect.bottom = 0;
		omHeaderBarRect.top = 0;
	}



      imTableContentOffset = 0;
		imTableYOffset = 0;
		omLeftDockingRect;
		omRightDockingRect;


      // let the list box know which window it must send a message to
      pomListBox->pomTableWindow = this;

	  pomListHeader->pomTableWindow = this;
	  

      // create an owner-drawn list box control

		if(imLeftDockingColumns > 0)
		{
			GetClientRect(&omLeftDockingRect);
			omLeftDockingRect.right = 0;
			omLeftDockingRect.left = 0;
			omLeftDockingRect.right = omRBorder[imLeftDockingColumns-1];
		}
		if(imRightDockingColumns > 0)
		{
			int ilC2 = omRBorder.GetSize();
			ilC2--;
			GetClientRect(&omRightDockingRect);
			omRightDockingRect.right = 0;
			omRightDockingRect.left = 0;
			omRightDockingRect.left = omRBorder[ilC2-imRightDockingColumns];
			omRightDockingRect.right = omRBorder[ilC2];
			ilRightOffset = omRightDockingRect.right - omRightDockingRect.left;
		}
      
		CRect rect;
		DWORD lListBoxStyle;
		if(nBar == SB_HORZ)
		{
		  lListBoxStyle = WS_HSCROLL | WS_VISIBLE /*| WS_BORDER*/
			  | imSelectMode | LBS_NOTIFY | LBS_NOINTEGRALHEIGHT | /*LBS_OWNERDRAWVARIABLE*/LBS_OWNERDRAWFIXED;
		}
		else if (nBar == SB_VERT)
		{
		  lListBoxStyle = WS_VSCROLL | WS_VISIBLE /*| WS_BORDER*/
			  | imSelectMode | LBS_NOTIFY | LBS_NOINTEGRALHEIGHT | /*LBS_OWNERDRAWVARIABLE*/LBS_OWNERDRAWFIXED;
		}
		else //nBar == SB_BOTH
		{
		  lListBoxStyle = WS_HSCROLL | WS_VSCROLL | WS_VISIBLE /*| WS_BORDER*/
			  | imSelectMode | LBS_NOTIFY | LBS_NOINTEGRALHEIGHT | /*LBS_OWNERDRAWVARIABLE*/LBS_OWNERDRAWFIXED;
		}
      GetClientRect(&rect);
		omRightDockingRect.right = rect.right;
		omRightDockingRect.left = rect.right - ilRightOffset;
		//RST!rect.OffsetRect(-1, -1);
		rect.left += imLeftScaleWidth;
		rect.right -= ilRightOffset;
      rect.top += imHeaderFontHeight + 7;

      if (pomListBox->Create(lListBoxStyle, rect, this, 1) == 0)
      {
          //RC(false);
          return false;
      }

	  
	  //Create the window for enabling the drag and drop of header columns.
      //The window is only for drag and drop operation and is in the background of header bar.
	  if(omHeaderDataArray.GetSize() > 0)
	  {
		  CRect olHeaderRect;
		  GetClientRect(&olHeaderRect);
		  olHeaderRect.bottom = imHeaderFontHeight +7;	 
		  olHeaderRect.left += imLeftScaleWidth;	  	  
		  olHeaderRect.right = omHeaderDataArray[omHeaderDataArray.GetSize() - 1].FrameRight + imTableContentOffset;	 	  

		  if(pomListHeader->Create(NULL,NULL,WS_ICONIC,olHeaderRect, this, GetNextControlId()) == FALSE)
		  {
			return false;
		  }
		  pomListHeader->InvalidateRect(&olHeaderRect);	  	  
		  pomListHeader->RedrawWindow();
		  pomListHeader->ShowWindow(SW_SHOWNOACTIVATE);
	  }
	  

      // Register Target for Dragdrop.
      //pomListBox->RegisterDropTarget();

      // Fix Windows 3.x's bug on list box co-ordinate calculation.
      // Windows 3.x will inflate window position given when created by -1,-1.
      // However, this problem could be fixed by artfully moving window just one time.
      //
		//rect.bottom--, rect.right--;
        pomListBox->MoveWindow(&rect, false);

		//Count the number of lines to be inserted
/*		int ilC1 = omLines.GetSize();
		int ilCountLines = 0;
			//for(int j = 0; j < omLines[ili].Columns.GetSize(); j++)
		for (int ili = 0; ili < ilC1; ili++)
				ilCountLines++;

        // insert data from the omData into the owner-drawn list box control
        for (int i = ilCountLines; i > 0; i--)
            pomListBox->AddString("");
*/
		int ilC1 = omLines.GetSize();
		for(int i = 0; i < ilC1; i++)
		{
            pomListBox->AddString("");
		}
        // set up the list box horizontal horizontal controls
        pomListBox->SetHorizontalExtent(imHorizontalExtent);
  		//pomListBox->SetScrollRange(SB_HORZ,0,imHorizontalExtent); 
	}


    //Resizing the window according to the size of the header bar.
	if(omHeaderDataArray.GetSize() > 0)
	{
		CRect olHeaderRect;
		GetClientRect(&olHeaderRect);
		olHeaderRect.bottom = imHeaderFontHeight +7;    
		olHeaderRect.left += imLeftScaleWidth;    
		olHeaderRect.right = omHeaderDataArray[omHeaderDataArray.GetSize() - 1].FrameRight + imTableContentOffset;//omRBorder[omRBorder.GetSize() - 1] + imTableContentOffset + 1
 
		if(pomListHeader != NULL && pomListHeader->m_hWnd != NULL)
		{
			pomListHeader->MoveWindow(&olHeaderRect);
  			pomListHeader->InvalidateRect(&olHeaderRect);	  	  
			pomListHeader->RedrawWindow();
			pomListHeader->ShowWindow(SW_SHOWNOACTIVATE);
		}
	}
	


// what we need is to know all detail-rects of all cells
	int ilC1 = omLines.GetSize();
	TABLE_COLUMN *prlColumn;
	for(int i = 0; i < ilC1; i++)
	{
		int ilC2 = omLines[i].Columns.GetSize();
		for(int j = 0; j < ilC2; j++)
		{
			prlColumn = &omLines[i].Columns[j];
			if(i == 0)
			{
				omLines[i].Columns[j].rect.top = 0;
				omLines[i].Columns[j].rect.bottom = imTextFontHeight+1;
			}
			else
			{
				omLines[i].Columns[j].rect.top    = (imTextFontHeight+2)*i;
				omLines[i].Columns[j].rect.bottom = ((imTextFontHeight+2)*i)+imTextFontHeight+1;
			}
			omLines[i].Columns[j].rect.left   = omLBorder[j] + 1;
			omLines[i].Columns[j].rect.right  = omRBorder[j] - 1;
		}
	}
	for(i = 0; i < ilC1; i++)
	{
		int ilC2 = omLines[i].Columns.GetSize();
		for(int j = 0; j < ilC2; j++)
		{
			if(omLines[i].Columns[j].SecialControlType != CTL_NORMAL)
			{
				if(omLines[i].Columns[j].SpecialControl != (CWnd*)NULL)
				{
					delete omLines[i].Columns[j].SpecialControl;
					omLines[i].Columns[j].SpecialControl = (CWnd*)NULL;
				}
			}
			switch(omLines[i].Columns[j].SecialControlType)
			{
			case CTL_BUTTON: 
				CCSTableButton *polButton;
				polButton = new CCSTableButton(i, j);
				//(CTableButton*)(omLines[i].Columns[j].SpecialControl)
				polButton->Create(omLines[i].Columns[j].Text, BS_OWNERDRAW /*| WS_CHILD*/ | WS_VISIBLE, 
																omLines[i].Columns[j].rect, pomListBox, GetNextControlId());
				
			    polButton->SetFont(/*&ogMSSansSerif_Bold_8*/&omMSSansSerif_Regular_8, false);
				if(omLines[i].Columns[j].BkColor == WHITE)
				{
					polButton->SetColors(/*COLORREF*/ ::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
				}
				else
				{
					polButton->SetColors(omLines[i].Columns[j].BkColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
				}
				omLines[i].Columns[j].SpecialControl = (CWnd*)polButton;
				break;
			case CTL_RADIOBUTTON: 
				break;
			case CTL_CHECKBUTTON: 
				CButton *polCheckButton;
				polCheckButton = new CButton();  //CButton
				//(CTableButton*)(omLines[i].Columns[j].SpecialControl)
				//polCheckButton->Create( LPCTSTR lpszCaption, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID );
  

				polCheckButton->Create(omLines[i].Columns[j].Text, WS_OVERLAPPED|WS_CHILD | WS_VISIBLE | BS_AUTOCHECKBOX, 
																omLines[i].Columns[j].rect, pomListBox, GetNextControlId());
				
				omLines[i].Columns[j].SpecialControl = (CWnd*)polCheckButton;
				break;
			case CTL_COMBOBOX:
				{
					CComboBox *polCombo;
					int ilHeight;
					polCombo = new CComboBox();
					CRect olRectCombo;
					olRectCombo = omLines[i].Columns[j].rect;
					ilHeight = omLines[i].Columns[j].rect.Height()*5;
					olRectCombo.bottom = omLines[i].Columns[j].rect.bottom + ilHeight;
					polCombo->Create( WS_CHILD | WS_VISIBLE | CBS_DROPDOWN, olRectCombo, pomListBox, GetNextControlId() );
					//for(int ilLc = 0; ilLc < omLines[i].Columns[j].ComboEntries.GetSize(); ilLc++)
					//{
					//	polCombo->AddString(omLines[i].Columns[j].ComboEntries[ilLc]);
					//}
					CString olText;
					CString olSubString = omLines[i].Columns[j].ComboEntries;
					bool blEnd = false;
					if(!olSubString.IsEmpty())
					{
						int pos;
						//int olPos = 0;
						while(blEnd == false)
						{
							pos = olSubString.Find('|');
							if(pos == -1)
							{
								blEnd = true;
								olText = olSubString;
							}
							else
							{
								olText = olSubString.Mid(0, olSubString.Find('|'));
								olSubString = olSubString.Mid(olSubString.Find('|')+1, olSubString.GetLength( )-olSubString.Find('|')+1);
							}
							polCombo->AddString(olText);
						}
					}
					omLines[i].Columns[j].SpecialControl = (CWnd*)polCombo;
				}
				break;
			case CTL_NORMAL:
				break;
			default:
				break;
			}
		}
	}
    // force itself to refresh the window
    InvalidateRect(NULL, false);

	if(pomIPEdit != NULL)
		pomIPEdit->SetWindowPos(&wndTop, 0,0,0,0, SWP_NOSIZE | SWP_NOMOVE | SWP_SHOWWINDOW);

    //RC(true);
    return true;
}

int CCSTable::GetNextControlId()
{
	imNextControlID++;
	return imNextControlID;
}
// reposition the table window
bool CCSTable::SetPosition(int ipXStart, int ipXEnd, int ipYStart, int ipYEnd)
{
    bool blRepaint = true;

    if (abs(ipXEnd-ipXStart+1) == imXEnd/*-imXStart+1*/ &&
        abs(ipYEnd-ipYStart+1) == imYEnd/*-imYStart+1*/)
        blRepaint = false;

    imXStart = min(ipXStart, ipXEnd);   // ensure positive height and width
    imXEnd = max(ipXStart, ipXEnd);
    imYStart = min(ipYStart, ipYEnd);
    imYEnd = max(ipYStart, ipYEnd);


    // recalculate object positions
    if (blRepaint)
        CalculateWidgets();

    // reposition the window, including the owner-drawn list box controls
    if (m_hWnd != NULL)
    {
        CRect rect;
        MoveWindow(imXStart/*+imLeftScaleWidth*/, imYStart, imXEnd-(imXStart/*+imLeftScaleWidth*/)+1, 
					imYEnd-imYStart+1, blRepaint);
        GetClientRect(&rect);
		  rect.OffsetRect(-1, -1);
        rect.top += imHeaderFontHeight + 7;
		  rect.right -= (omRightDockingRect.right-omRightDockingRect.left);
		  if(omLeftDockingRect.right > 0)
		  {
				rect.left += omLeftDockingRect.right+5;
		  }
        pomListBox->MoveWindow(&rect, blRepaint);
		//DisplayTable();
    }
	
    //RC(true);
    return true;
}

CListBox *CCSTable::GetCTableListBox()
{
	return pomListBox;
}


int CCSTable::SelectLine(int ipLineno, BOOL bpSelect)
{
	//MWO + THO: War vorher totaler Unsinn!! Jetzt ist's OK
	int ilRet = false;

	if((imSelectMode & LBS_MULTIPLESEL) || (imSelectMode & LBS_EXTENDEDSEL))
	{
		if( (ipLineno >= 0) || (bpSelect == FALSE))
		{
			int *pilItems = NULL;
			int ilAnz = pomListBox->GetSelCount();
			if(ilAnz > 0)
			{
				pilItems = new int[ilAnz];
			}
			if(pilItems != NULL)
			{
				ilAnz = pomListBox->GetSelItems(ilAnz, pilItems);
				for(int i = 0; i < ilAnz; i++)
				{
					ilRet = pomListBox->SetSel(pilItems[i] , FALSE);
				}
				if(bpSelect == TRUE)
				{
					ilRet = pomListBox->SetSel(ipLineno);
				}
				delete [] pilItems;
			}
		}
	}
	else
	{
		if((bpSelect == FALSE) || (ipLineno < 0))
			ilRet = pomListBox->SetCurSel(-1);
		else
			ilRet = pomListBox->SetCurSel(ipLineno);
	}

	pomParentWindow->SendMessage(WM_TABLE_SELCHANGE, (LONG)ipLineno, 0);
	return ilRet;
}


int CCSTable::GetLinenoFromPoint(CPoint opPoint)
{
	//pomListBox->ScreenToClient(&opPoint);    
	return pomListBox->GetItemFromPoint(opPoint);
}


int CCSTable::GetColumnnoFromPoint(CPoint opPoint)
{
	//pomListBox->ScreenToClient(&opPoint);    
	pomListBox->DetectCurrentColumn(opPoint);
	return imCurrentColumn;
}

bool CCSTable::SetFieldNames(CString opFieldNames)
{
    omFieldNames.RemoveAll();

    int i = 0;
    while (i < opFieldNames.GetLength())
    {
        for (int n = i; n < opFieldNames.GetLength() && opFieldNames[n] != ','; n++)
            ;
        omFieldNames.Add(opFieldNames.Mid(i, n-i));
        i = n + 1;
    }

    //RC(true);
    return true;
}

///////////////////////////////////////////////////////////////////
// MWO: Adds one column to the header
bool CCSTable::AddColumnToHeader(TABLE_HEADER_COLUMN *prpHeaderData)
{
	TABLE_HEADER_COLUMN rlHColumn;
	rlHColumn.Alignment = prpHeaderData->Alignment;
	rlHColumn.Length = prpHeaderData->Length;
	rlHColumn.FrameLeft = prpHeaderData->FrameLeft;
	rlHColumn.FrameRight = prpHeaderData->FrameRight;
	rlHColumn.FrameTop = prpHeaderData->FrameTop;
	rlHColumn.FrameBottom = prpHeaderData->FrameBottom;
	rlHColumn.Type = prpHeaderData->Type;
	rlHColumn.Format = prpHeaderData->Format;
	rlHColumn.BkColor = prpHeaderData->BkColor;
	rlHColumn.TextColor = prpHeaderData->TextColor;
	rlHColumn.Font = prpHeaderData->Font; //CFont
	rlHColumn.Text = prpHeaderData->Text;
	
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHColumn);
    CalculateWidgets();
    return true;

}
///////////////////////////////////////////////////////////////////
// MWO: adds the column to the line
bool CCSTable::AddColumnToLine(int ipLineno, TABLE_COLUMN *prpColumnData)
{
	TABLE_COLUMN rlColumn;
	rlColumn.Lineno = ipLineno;
	rlColumn.Alignment = prpColumnData->Alignment;
	rlColumn.Length = prpColumnData->Length;
	rlColumn.FrameLeft = prpColumnData->FrameLeft;
	rlColumn.FrameRight = prpColumnData->FrameRight;
	rlColumn.FrameTop = prpColumnData->FrameTop;
	rlColumn.FrameBottom = prpColumnData->FrameBottom;
	rlColumn.Format = prpColumnData->Format;
	rlColumn.BkColor = prpColumnData->BkColor;
	rlColumn.TextColor = prpColumnData->TextColor;
	rlColumn.Font = prpColumnData->Font;
	rlColumn.Text = prpColumnData->Text;
	rlColumn.polEdit = prpColumnData->polEdit;
	rlColumn.SecialControlType = prpColumnData->SecialControlType;      // CTL_COMBOBOX, CTL_BUTTON etc.
	rlColumn.SpecialControl    = prpColumnData->SpecialControl;         // Controlobject CButton, CComboBox etc.
	int ilCount = omLines.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		if(omLines[i].Lineno == ipLineno)
		{
			int ilCC = omLines[i].Columns.GetSize();
			rlColumn.Columnno = ilCC+1;
			omLines[i].Columns.NewAt(ilCC, rlColumn);
		}
	}
    return true;
}


bool CCSTable::AddColumn(CCSPtrArray <TABLE_COLUMN> &opColumns)
{
	TABLE_COLUMN rlColumn;
	
	int ilCount = omLines.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		int ilCC = omLines[i].Columns.GetSize();
		
		rlColumn = opColumns[i];

		rlColumn.Columnno = ilCC+1;
		rlColumn.Lineno = i;
		
		omLines[i].Columns.NewAt(ilCC, rlColumn);
	}
    return true;
}





///////////////////////////////////////////////////////////////////
// MWO: Adds an new line to the table
bool CCSTable::AddLine(CCSPtrArray <TABLE_LINE> *popLine)
{
	//omLines.NewAt(omLines.GetSize(), opLine);

	omLines.Add(popLine);
    return true;
}

///////////////////////////////////////////////////////////////////
// MWO: Changes one column in the specified line
bool CCSTable::ChangeColumnData(int ipLineno, int ipColumnno, TABLE_COLUMN *prpColumnData)
{
	TABLE_COLUMN *prlColumn;
	prlColumn = FindColumn(ipLineno, ipColumnno);
	if(prlColumn != NULL)
	{
		if((pomIPEdit != NULL) && (ipLineno == imCurrentEditPosY) && (ipColumnno == imCurrentEditPosX))
		{
			pomIPEdit->SetWindowText(prpColumnData->Text);
			pomIPEdit->SetSel(0,-1);
		}

		prlColumn->Lineno  = prpColumnData->Lineno;
		prlColumn->Columnno  = prpColumnData->Columnno;
		prlColumn->Alignment  = prpColumnData->Alignment;
		prlColumn->Length  = prpColumnData->Length;
		prlColumn->FrameLeft  = prpColumnData->FrameLeft;
		prlColumn->FrameRight  = prpColumnData->FrameRight;
		prlColumn->FrameTop  = prpColumnData->FrameTop;
		prlColumn->FrameBottom  = prpColumnData->FrameBottom;
		prlColumn->Type  = prpColumnData->Type;
		prlColumn->SeparatorType		  = prpColumnData->SeparatorType;
		prlColumn->VerticalSeparator  = prpColumnData->VerticalSeparator;
		prlColumn->HorizontalSeparator  = prpColumnData->HorizontalSeparator;
		prlColumn->Format  = prpColumnData->Format;
		prlColumn->BkColor  = prpColumnData->BkColor;
		prlColumn->TextColor  = prpColumnData->TextColor;
		prlColumn->Font  = prpColumnData->Font;
		prlColumn->Text  = prpColumnData->Text;
		prlColumn->polEdit  = prpColumnData->polEdit;
		prlColumn->SecialControlType = prpColumnData->SecialControlType;      // CTL_COMBOBOX, CTL_BUTTON etc.
		prlColumn->SpecialControl    = prpColumnData->SpecialControl;         // Controlobject CButton, CComboBox etc.

	}
   if (m_hWnd != NULL)     // list box is exist?
   {
		CRect rcItem;
		if (pomListBox->GetItemRect(ipLineno, rcItem) == LB_ERR 
			// ||  pomListBox->SetCaretIndex(ipLineNo) == LB_ERR
			)
       {
           //RC(false);
           return false;
       }
		pomListBox->InvalidateRect(rcItem);
   }
   return true;
}

///////////////////////////////////////////////////////////////////
// MWO: Changes the specified Column in all lines
bool CCSTable::ChangeColumnData(int ipColumnno, TABLE_COLUMN *prpColumnData)
{
	int ilCount = omLines.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		ChangeColumnData(i, ipColumnno, prpColumnData);
		if (m_hWnd != NULL)     // list box is exist?
		 {
			CRect rcItem;
			if (pomListBox->GetItemRect(i, rcItem) == LB_ERR 
				// ||  pomListBox->SetCaretIndex(ipLineNo) == LB_ERR
				)
			  {
					//RC(false);
					return false;
			  }
			pomListBox->InvalidateRect(rcItem);
		 }
	}
    return true;
}

///////////////////////////////////////////////////////////////////
// MWO:
bool CCSTable::ChangeColumnColor(int ipColumnno, COLORREF rlBkColor, COLORREF rlTextColor)
{
	int ilCount = omLines.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		if(ipColumnno <= omLines[i].Columns.GetSize()-1)
		{
			omLines[i].Columns[ipColumnno].BkColor   = rlBkColor;
			omLines[i].Columns[ipColumnno].TextColor = rlTextColor;
			if (m_hWnd != NULL)     // list box is exist?
			{
				CRect rcItem;
				if (pomListBox->GetItemRect(i, rcItem) == LB_ERR 
					// ||  pomListBox->SetCaretIndex(ipLineNo) == LB_ERR
					)
				  {
						//RC(false);
						return false;
				  }
				pomListBox->InvalidateRect(rcItem);
			}
		}
	}
    return true;
}

///////////////////////////////////////////////////////////////////
// MWO:
bool CCSTable::ChangeColumnColor(int ipLineno, int ipColumnno, COLORREF rlBkColor, COLORREF rlTextColor)
{
	 if(ipLineno <= omLines.GetSize()-1)
	 {
		 if(ipColumnno <= omLines[ipLineno].Columns.GetSize())
		 {
			omLines[ipLineno].Columns[ipColumnno].BkColor   = rlBkColor;
			omLines[ipLineno].Columns[ipColumnno].TextColor = rlTextColor;
			if (m_hWnd != NULL)     // list box is exist?
			{
				CRect rcItem;
				if (pomListBox->GetItemRect(ipLineno, rcItem) == LB_ERR 
					// ||  pomListBox->SetCaretIndex(ipLineNo) == LB_ERR
					)
				  {
						//RC(false);
						return false;
				  }
				pomListBox->InvalidateRect(rcItem);
			}
		 }
	 }
    return true;
}


bool CCSTable::GetLineColumnBkColor(int ipLineno,  int ipColumnno, COLORREF &lpColor)
{
	 if(ipLineno <= omLines.GetSize()-1)
	 {
		 if(ipColumnno <= omLines[ipLineno].Columns.GetSize())
		 {
			if (m_hWnd != NULL)     // list box is exist?
			{
				lpColor = omLines[ipLineno].Columns[ipColumnno].BkColor;
				//omLines[ipLineno].Columns[ipColumnno].TextColor = rlTextColor;
				return true;
			}
		 }
	 }
    return false;
}






///////////////////////////////////////////////////////////////////
// MWO:
bool CCSTable::ChangeColumnType(int ipLineno, int ipColumnno, int Type)
{
	ipLineno;
	ipColumnno;
	Type;
    return true;
}

///////////////////////////////////////////////////////////////////
// MWO:
bool CCSTable::ChangeColumnType(int ipColumnno, int Type)
{
	ipColumnno;
	Type;
    return true;
}

///////////////////////////////////////////////////////////////////
// MWO: Deletes a line in the table
// Don't know whether it would be better to use change the old method "DeleteTextLine()"
bool CCSTable::DeleteLineData(int ipLineno)
{
    if (ipLineno == -1 && m_hWnd != NULL)   // use default line number?
        ipLineno = pomListBox->GetCaretIndex();

    if (!(0 <= ipLineno && ipLineno < omLines.GetSize()))    // valid line number?
    {
        //RC(false);
        return false;
    }
	int ilCount = omLines[ipLineno].Columns.GetSize();
	while(ilCount-- > 0)
	{
		omLines[ipLineno].Columns.DeleteAt(ilCount);
	}
	omLines.DeleteAt(ipLineno);
	//ATTENTION: we have to notify the embedded Listbox to delete the line

    if (m_hWnd != NULL)     // list box is exist?
    {
        if (pomListBox->DeleteString(ipLineno) == LB_ERR)
        {
            //RC(false);
            return false;
        }
    }
    return true;
}

///////////////////////////////////////////////////////////////////
// MWO: Deletes the header array used for setting an new header after
bool CCSTable::DeleteHeaderData()
{
	int ilCount = omHeaderDataArray.GetSize();
	while (ilCount-- > 0)
	{
		omHeaderDataArray.DeleteAt(ilCount);
	}
    return true;
}

///////////////////////////////////////////////////////////////////
// MWO: Searches the specified column in the specified line and
// returns a pointer to the column
TABLE_COLUMN * CCSTable::FindColumn(int ipLineno, int ipColumnno)
{
	return &omLines[ipLineno].Columns[ipColumnno];

/*	int ilC1 = omLines.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if(omLines[i].Lineno == ipLineno)
		{
			int ilC2 = omLines[i].Columns.GetSize();
			for(int j=0; j<ilC2; j++)
			{
				if(omLines[i].Columns[j].Columnno == ipColumnno)
				{
					return &omLines[i].Columns[j];
				}
			}
		}
	}
*/
    return NULL;
}

bool CCSTable::SetHeaderFields(CCSPtrArray <TABLE_HEADER_COLUMN> opHeaderDataArray)
{

    CDC dc;
	//int ilSize = 0;

    TEXTMETRIC tm;
    
	dc.CreateCompatibleDC(NULL);

	CString olChar = "x";
	CString olTmp;

	CFont *pOldFont = NULL;
	CSize olSize;

	omHeaderDataArray.DeleteAll();

	pOldFont= dc.SelectObject(&omCourier_Regular_10);
	int ilCount = opHeaderDataArray.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		TABLE_HEADER_COLUMN *prlColumn = new TABLE_HEADER_COLUMN;
		prlColumn->Alignment = opHeaderDataArray[i].Alignment;
		prlColumn->Length = opHeaderDataArray[i].Length;
		prlColumn->String = opHeaderDataArray[i].String;
		prlColumn->AnzChar = opHeaderDataArray[i].AnzChar;
		prlColumn->FrameLeft = opHeaderDataArray[i].FrameLeft;
		prlColumn->FrameRight = opHeaderDataArray[i].FrameRight;
		prlColumn->FrameTop = opHeaderDataArray[i].FrameTop;
		prlColumn->FrameBottom = opHeaderDataArray[i].FrameBottom;
		prlColumn->SeparatorType = opHeaderDataArray[i].SeparatorType;
		prlColumn->Type = opHeaderDataArray[i].Type;
		prlColumn->Format = opHeaderDataArray[i].Format;
		prlColumn->BkColor = opHeaderDataArray[i].BkColor;
		prlColumn->TextColor = opHeaderDataArray[i].TextColor;
		prlColumn->Font = opHeaderDataArray[i].Font;
		prlColumn->Text = opHeaderDataArray[i].Text;
		omHeaderDataArray.Add(prlColumn);


		if(!prlColumn->String.IsEmpty() )
		{

			if ((dc.SelectObject(prlColumn->Font)) == NULL)   
				dc.SelectObject(&omCourier_Regular_10);


			olSize = dc.GetOutputTextExtent(prlColumn->String + olChar);

			dc.GetTextMetrics(&tm);

			prlColumn->Length = olSize.cx;

		}

	}
	dc.SelectObject(pOldFont);
    dc.DeleteDC();

    return true;
}



//To synchronize the mapping between the header name and field name while the position of headers is changed.
bool  CCSTable::SetTableFields(CCSPtrArray <CString> opTableFieldArray)
{
	omTableFieldArray.DeleteAll();
	int ilCount = opTableFieldArray.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		omTableFieldArray.NewAt(i,opTableFieldArray[i]);
	}
	 
	return true;
}


// TVO
TABLE_HEADER_COLUMN *CCSTable::GetHeader(int ipColumnNo)
{
	int ilCount = omHeaderDataArray.GetSize();
	for(int i = 0; i < ilCount && i < ipColumnNo; i++);

	if (i >= ilCount)
		return NULL;
	return &omHeaderDataArray[i];
}

// TVO END


bool CCSTable::ResetContent()
{

	if(pomIPEdit != NULL)
	{
		pomIPEdit->DestroyWindow();
		delete pomIPEdit;
		pomIPEdit = NULL;
	}

    for(int i = omMenuItems.GetSize()-1; i >= 0;i--)
	{
		omMenuItems[i].Items.DeleteAll();
	}
	omMenuItems.DeleteAll();
	
	omDataPtr.RemoveAll();
	omColor.RemoveAll();
	omBkColor.RemoveAll();
	omDragEnabled.RemoveAll();
    omSeparatorType.RemoveAll();
	omFontPtr.RemoveAll();

	int ilC1 = omLines.GetSize();
	while(ilC1-- > 0)
	{
		int ilC2 = omLines[ilC1].Columns.GetSize();
		while(ilC2-- > 0)
		{
			//delete omLines[ilC1].Columns[ilC2].EditAttrib;
			omLines[ilC1].Columns.DeleteAt(ilC2);
		}
		omLines.DeleteAt(ilC1);
	}
    if (m_hWnd != NULL)     // list box is exist?
        pomListBox->ResetContent();

    //RC(true);
    return true;
}

int CCSTable::GetCurSel()
{
	return pomListBox->GetCurSel();
}

int CCSTable::GetCurrentLine()
{
    return (m_hWnd != NULL)? pomListBox->GetCaretIndex() : -1 ;
}

bool CCSTable::SetHeader(CCSPtrArray <TABLE_HEADER_COLUMN> *prpHeaderLine)
{
	int ilCount = prpHeaderLine->GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), prpHeaderLine->GetAt(i));	
	}
	
    return true;
}


bool CCSTable::GetTextLineColumns(CCSPtrArray <TABLE_COLUMN> *popLine, int ipLineno)
{
	int ilCount = omLines.GetSize();
	if(ilCount < ipLineno)
	{
		return false;
	}
	int ilCols = omLines[ipLineno].Columns.GetSize();
	for(int i = 0; i < ilCols; i++)
	{
		popLine->Add(&omLines[ipLineno].Columns[i]);
	}
	return true;
}






bool CCSTable::SetTextLineColor(int ipLineNo, COLORREF lpTextColor, COLORREF lpTextBkColor)
{
    if (ipLineNo == -1 && m_hWnd != NULL)   // use default line number?
        ipLineNo = pomListBox->GetCaretIndex();

    if (!(0 <= ipLineNo && ipLineNo < omLines.GetSize()))    // valid line number?
    {
        //RC(false);
        return false;
    }

    // not implemented yet, waiting to be sure about the color
	if (lpTextColor != -1L)
		omColor[ipLineNo] = lpTextColor;
	if (lpTextBkColor != -1L)
		omBkColor[ipLineNo] = lpTextBkColor;
	int ilCount = omLines[ipLineNo].Columns.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		omLines[ipLineNo].Columns[i].BkColor = lpTextBkColor;
		omLines[ipLineNo].Columns[i].TextColor = lpTextColor;
	}

    //RC(true);
    InvalidateRect(NULL, false);
	//DisplayTable();
    return true;
}
////////////////////////////////////////////////////////////////////////////////
// MWO:
// Sets the backround- and the textcolor for the specified column in one line
bool CCSTable::SetTextColumnColor(int ipLineNo, int ipColumnNo, COLORREF lpTextColor, COLORREF lpTextBkColor)
{
    if (ipLineNo == -1 && m_hWnd != NULL)   // use default line number?
        ipLineNo = pomListBox->GetCaretIndex();

    if (!(0 <= ipLineNo && ipLineNo < omLines.GetSize()))    // valid line number?
    {
        //RC(false);
        return false;
    }
	if((ipColumnNo < 0) || (ipColumnNo > omLines[ipLineNo].Columns.GetSize()))
	{
        return false;
	}
	int ilCount = omLines[ipLineNo].Columns.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		if(i == ipColumnNo)
		{
			omLines[ipLineNo].Columns[i].BkColor = lpTextBkColor;
			omLines[ipLineNo].Columns[i].TextColor = lpTextColor;
		}
	}

    InvalidateRect(NULL, false);
    return true;
}

////////////////////////////////////////////////////////////////////////////////
// MWO:
// Sets the backround- and the textcolor for the specified column in all lines
bool CCSTable::SetTextColumnColor(int ipColumnNo, COLORREF lpTextColor, COLORREF lpTextBkColor)
{
	int ilCount = omLines.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		int ilC2 = omLines[i].Columns.GetSize();
		for(int j = 0; j < ilC2; j++)
		{
			if(j == ipColumnNo)
			{
				omLines[i].Columns[j].BkColor = lpTextBkColor;
				omLines[i].Columns[j].TextColor = lpTextColor;
			}
		}
	}
    InvalidateRect(NULL, false);
    return true;
}



//---------------------------------------------------------------------------
// MWO: Changes the attributes of just one cell
bool CCSTable::SetCellType(int ipLineno, int ipColumnno,  CCSEDIT_ATTRIB rpAttrib)
{
    if (!(0 <= ipLineno && ipLineno < omLines.GetSize()))    // valid line number?
    {
        //RC(false);
        return false;
    }
	if((ipColumnno < 0) || (ipColumnno > omLines[ipLineno].Columns.GetSize()))
	{
        return false;
	}
	omLines[ipLineno].Columns[ipColumnno].blIsEditable = true;
	omLines[ipLineno].Columns[ipColumnno].EditAttrib = rpAttrib;
   return true;
}
//---------------------------------------------------------------------------
// MWO: Changes the attributes for every line of specified column
bool CCSTable::SetColumnType(int ipColumnno,  CCSEDIT_ATTRIB rpAttrib)
{
	int ilCount = omLines.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		int ilC2 = omLines[i].Columns.GetSize();
		for(int j = 0; j < ilC2; j++)
		{
			if(j == ipColumnno)
			{
				omLines[i].Columns[j].blIsEditable = true;
				omLines[i].Columns[j].EditAttrib = rpAttrib;
			}
		}
	}
    return true;
}

//---------------------------------------------------------------------------
// MWO: Changes the attributes for the specified region
// 
bool CCSTable::SetLineColumnType(int ipColumnFrom, int ipLineFrom, int ipColumnTo, int ipLineTo, CCSEDIT_ATTRIB rpAttrib)
{
	int ilCount = omLines.GetSize();
	for(int i = ipLineFrom; (i < ilCount && i <= ipLineTo); i++)
	{
		int ilC2 = omLines[i].Columns.GetSize();
		for(int j = ipColumnFrom; (j < ilC2 && j <= ipColumnTo); j++)
		{
				omLines[i].Columns[j].blIsEditable = true;
				omLines[i].Columns[j].EditAttrib = rpAttrib;
		}
	}
    return true;
}


bool CCSTable::SetTextLineSeparator(int ipLineNo, int ipSeparatorType)
{
    if (ipLineNo == -1 && m_hWnd != NULL)   // use default line number?
        ipLineNo = pomListBox->GetCaretIndex();

    if (!(0 <= ipLineNo && ipLineNo < omLines.GetSize()))    // valid line number?
    {
        //RC(false);
        return false;
    }
	omSeparatorType[ipLineNo] = (unsigned short)ipSeparatorType;
	int ilCount = omLines[ipLineNo].Columns.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		omLines[ipLineNo].Columns[i].SeparatorType = ipSeparatorType;
	}

    //RC(true);
    InvalidateRect(NULL, false);
    return true;
}

bool CCSTable::SetTextLineFont(int ipLineNo, CFont *popFont)
{
    if (!(0 <= ipLineNo && ipLineNo < omLines.GetSize()))    // valid line number?
    {
        //RC(false);
        return false;
    }

	omFontPtr[ipLineNo] = popFont; // Set the default font for the line
								   // if the columns have a different fonttype it can be
								   // changed explicit for the column

	int ilCount = omLines[ipLineNo].Columns.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		omLines[ipLineNo].Columns[i].Font = popFont;
	}

    //RC(true);
	CalculateWidgets();
    InvalidateRect(NULL, false);
    return true;
}

//Sets the font for one column in the specified line
bool CCSTable::SetTextColumnFont(int ipLineNo, int ipColumnNo, CFont *popFont)
{
    if (ipLineNo == -1 && m_hWnd != NULL)   // use default line number?
        ipLineNo = pomListBox->GetCaretIndex();


	// Ckeck for valid line an for valid column in line
	if((ipLineNo < 0) || (ipLineNo >= omLines.GetSize()))
	{
			return false;
	}
	if((ipColumnNo < 0) || (ipColumnNo >= omLines[ipLineNo].Columns.GetSize()))
	{
			return false;
	}

	omLines[ipLineNo].Columns[ipColumnNo].Font = popFont;
	CalculateWidgets();
    //InvalidateRect(NULL, false);
	DisplayTable();

    //RC(true);
    return true;
}

//Sets the Font for the specified column in all lines
bool CCSTable::SetTextColumnFont(int ipColumnNo, CFont *popFont)
{
	int ilC1 = omLines.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if(ipColumnNo < omLines[i].Columns.GetSize())
		{
			omLines[i].Columns[ipColumnNo].Font = popFont;
		}
	}
	CalculateWidgets();
    //InvalidateRect(NULL, false);
	DisplayTable();

    return true;
}

bool CCSTable::SetTextLineDragEnable(int ipLineNo, bool bpEnable)
{
    if (ipLineNo == -1 && m_hWnd != NULL)   // use default line number?
        ipLineNo = pomListBox->GetCaretIndex();

	omDragEnabled[ipLineNo] = bpEnable;

	//RC(true);
	return true;
}

bool CCSTable::GetTextLineValue(int ipLineNo, CString &opText)
{
    if (ipLineNo == -1 && m_hWnd != NULL)   // use default line number?
        ipLineNo = pomListBox->GetCaretIndex();

    if (!(0 <= ipLineNo && ipLineNo < omLines.GetSize()))    // valid line number?
    {
        //RC(false);
        return false;
    }

	int ilCount = omLines[ipLineNo].Columns.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		opText += omLines[ipLineNo].Columns[i].Text;
	}

    //RC(true);
    return true;
}

void *CCSTable::GetTextLineData(int ipLineNo)
{
    if (ipLineNo == -1 && m_hWnd != NULL)   // use default line number?
        ipLineNo = pomListBox->GetCaretIndex();

    if (!(0 <= ipLineNo && ipLineNo < omLines.GetSize()))    // valid line number?
        return NULL;

    return omDataPtr[ipLineNo];
}

BOOL CCSTable::GetTextLineDragEnable(int ipLineNo)
{
    if (ipLineNo == -1 && m_hWnd != NULL)   // use default line number?
        ipLineNo = pomListBox->GetCaretIndex();

    if (!(0 <= ipLineNo && ipLineNo < omLines.GetSize()))    // valid line number?
        return false;

	return omDragEnabled[ipLineNo];
}

bool CCSTable::GetTextFieldValue(int ipLineNo, int ipFieldNo, CString &opText)
{
	opText = "";

    if (!(0 <= ipLineNo && ipLineNo < omLines.GetSize()))    // valid line number?
        return false;

    if (!(0 <= ipFieldNo && ipFieldNo < omLines[ipLineNo].Columns.GetSize()))    // valid column number?
        return false;
	
	opText = omLines[ipLineNo].Columns[ipFieldNo].Text;

	return true;

	//RST!
	/*
    CString s;

    // get the entire line, and checking for errors
    if (GetTextLineValue(ipLineNo, s) == false)
    {
        //RC(false);
        return false;
    }

    // skip over the leading fields
    int n = s.GetLength();
    for (int i = 0, fieldno = ipFieldNo; i < n && fieldno > 0; i++)
        if (s[i] == '|')
            fieldno--;
    if (fieldno != 0)
    {
        //RC(false);
        return false;
    }

    // extract text field value
    for (int pos = i; pos < n && s[pos] != '|'; pos++)
            ;
    opText = s.Mid(i, pos-i);

    //RC(true);
    return true;
	*/
}

bool CCSTable::GetTextFieldValue(int ipLineNo, CString opFieldName, CString &opText)
{
    // searching for the given "omFieldName"
    for (int i = 0; i < omFieldNames.GetSize(); i++)
        if (opFieldName == omFieldNames[i])
            return GetTextFieldValue(ipLineNo, i, opText);

    //RC(false);
    return false;
}

bool CCSTable::SetTextFieldValue(int ipLineNo, int ipFieldNo, const CString &ropText)
{
 
    if (!(0 <= ipLineNo && ipLineNo < omLines.GetSize()))    // valid line number?
        return false;

    if (!(0 <= ipFieldNo && ipFieldNo < omLines[ipLineNo].Columns.GetSize()))    // valid column number?
        return false;
	
	omLines[ipLineNo].Columns[ipFieldNo].Text = ropText;

	return true;
}


int CCSTable::SearchTextLine(int ipFieldNo, CString opText)
{
    if (opText != omLastSearchText) // new search string?
    {
        imNextLineToSearch = 0;
        omLastSearchText = opText;  // remember text for next searching
    }

    // scanning the "omData" line-by-line
    for (int n = omLines.GetSize(); imNextLineToSearch < n; imNextLineToSearch++)
    {
        CString field;

        if (GetTextFieldValue(imNextLineToSearch, ipFieldNo, field) == false)   // error?
            return -1;
        if (field == opText)    // found?
            return imNextLineToSearch++;
    }

    return -1;  // not found
}


void CCSTable::SetHeaderSpacing(int ipPixel)
{
	imHeaderSpacing = ipPixel;
}

/////////////////////////////////////////////////////////////////////////////
// CalculateWidgets:
//
// This private member function will recompute the correct font height, font width,
// and everything that need to display the table.
//
void CCSTable::CalculateWidgets()
{
    CDC dc;
    TEXTMETRIC tm;
    int ilTextFontWidth=0;
	int ilMaxHeightInLine = 0;
	imTextFontHeight = 0;
	imHeaderFontHeight = 0;

    if (!bmInited)
        return;     // cannot calculate anything if not initialized yet

    dc.CreateCompatibleDC(NULL);

	CFont *pOldFont = dc.SelectObject(&omCourier_Regular_10);
    // calculate the text font height and width
	int ilTextColumns = omLines.GetSize();
	if(ilTextColumns == 0)
	{
		imTextFontHeight = 10;
	}
	else
	{
		for(int k = 0; k < ilTextColumns; k++)
		{
			int ilColCount = omLines[k].Columns.GetSize();
			for(int l = 0; l < ilColCount; l++)
			{
				int ilTmpFontWidth;
				int ilTmpFontHeight;

				if (dc.SelectObject(omLines[k].Columns[l].Font) == NULL)  
					dc.SelectObject(&omCourier_Regular_10);
				dc.GetTextMetrics(&tm);
				ilTmpFontWidth = tm.tmAveCharWidth;
				ilTmpFontHeight = tm.tmHeight + tm.tmExternalLeading;
				if(ilTextFontWidth < ilTmpFontWidth)
				{
					ilTextFontWidth = ilTmpFontWidth;
				}
				if(imTextFontHeight < ilTmpFontHeight)
				{
					imTextFontHeight = ilTmpFontHeight;
				}
				if(ilMaxHeightInLine < ilTmpFontHeight)
				{
					ilMaxHeightInLine = ilTmpFontHeight;
				}
				if (m_hWnd != NULL) // window was not created yet?
				{
					pomListBox->SetItemHeight(k, imTextFontHeight + EXTRASPACE);
				}
				imItemHeight = imTextFontHeight + EXTRASPACE;
			}
			ilMaxHeightInLine = 0;
		}
	}
    // calculate the header font height and width
	int ilHeaderColumns = omHeaderDataArray.GetSize();
	for(int ili = 0; ili < ilHeaderColumns; ili++)
	{
		int ilTmpHeaderFontHeight;
		if (dc.SelectObject(&omHeaderDataArray[ili].Font) == NULL) // it works with both NULL and invalid font
			dc.SelectObject(&omCourier_Regular_10);
		dc.GetTextMetrics(&tm);
		ilTmpHeaderFontHeight = tm.tmHeight + tm.tmExternalLeading;
		// the greatest Font is the winner
		if(ilTmpHeaderFontHeight > imHeaderFontHeight)
		{
			imHeaderFontHeight = ilTmpHeaderFontHeight;
		}
	}

	if(bmMiniTable)
		imHeaderFontHeight = -5;


    // Calculate column width and positions.
    // Notes: expression "pos += width+5" means lframe, rframe, white, gray, and shadow
    // vertical bars (ridges separating columns).
    //
    omLBorder.RemoveAll();
    omRBorder.RemoveAll();
	//find the greates count of entries in all lines
	ilTextColumns = omLines.GetSize();
	int ilMaxEntryCount = 0;
	for(ili = 0; ili < ilTextColumns; ili++)
	{
		int ilC2 = omLines[ili].Columns.GetSize();
		ilMaxEntryCount = (ilC2 > ilMaxEntryCount)? ilC2 : ilMaxEntryCount;
	}

    int n = max(omHeaderDataArray.GetSize(), ilMaxEntryCount);
	int ilDockingOffset = 0;
	int ilRightDockingOffset = 0;

    for (int i = 0, pos = -1, width = 0; i < n; i++, pos += width+5)
    {
        int w1 = 0; // width calculated from the header bar

        if (i < omHeaderDataArray.GetSize())
		{
            w1 = dc.GetOutputTextExtent(omHeaderDataArray[i].Text, omHeaderDataArray[i].Text.GetLength()).cx;
			int ilX = omHeaderDataArray[i].Length;
			if(ilX > w1)
				w1 = ilX;
		}
        width = w1 + imHeaderSpacing;  


		//if we have docking columns we must set the right and left 
		//borders as their origin positions
		//for all none docking columns we must substract the offset
		//of cummulated leght of docking columns
		if(i >= n-imRightDockingColumns)
		{
			ilRightDockingOffset += width+5;
		}
		if(i < imLeftDockingColumns)
		{
			omLBorder.Add(pos);
			omRBorder.Add(pos + width+ 1);
			ilDockingOffset += width+5;
		}
		else
		{
			omLBorder.Add(pos - ilDockingOffset);
			omRBorder.Add(pos + width + 1 - ilDockingOffset);
		}
		
        //To store the values of dimensions of individual header columns.
        //These values are used to create the window for drag and drop operation.
		if(omHeaderDataArray.GetSize() > 0)
		{
			omHeaderDataArray[i].FrameBottom = imYStart + imHeaderFontHeight + 3;
			omHeaderDataArray[i].FrameLeft =  omLBorder[i] + 1;;
			omHeaderDataArray[i].FrameRight = omRBorder[i] + 1;;
			omHeaderDataArray[i].FrameTop = imYStart;
		}
    }

	dc.SelectObject(pOldFont);
    dc.DeleteDC();

    // determine width of the list box (for hiding/displaying horizontal scroll bar)
    imHorizontalExtent = pos - 4 - ilDockingOffset - ilRightDockingOffset; //perhaps shortening the extent with lenght of dockingcolumns
    if (m_hWnd != NULL)
	{
        pomListBox->SetHorizontalExtent(imHorizontalExtent);
	}

    // Checking for the last column (must provide a filler column on the right).
    // We use the client-area width to allow calculation while the window was not created yet.
    //
    if (pos < (imXEnd - imXStart - 1))
    {
        omLBorder.Add(pos);
        omRBorder.Add(imXEnd - imXStart);
    }
	//remember the origin rect of headerbar;
	omHeaderBarHeight = imHeaderFontHeight + 7;

}


/////////////////////////////////////////////////////////////////////////////
// Message Handlings

// OnPaint: redraw the header and the table content
afx_msg void CCSTable::OnPaint()
{
    // update header bar if necessary
    CRect rect, rectTest;
    GetUpdateRect(&rect);
    if (rectTest.IntersectRect(&rect, &omHeaderBarRect))    // "rect" overlaps the header bar?
	{
        DrawHeaderBar();
	}
	  DrawLeftScale();
	  DrawRightScale();
    ValidateRect(NULL);

    // update the table content (which is an owner-drawn list box)
    MapWindowPoints(pomListBox, &rect);
    pomListBox->InvalidateRect(&rect, true);
}

// OnMeasureItem: set up the height of each item in list box
afx_msg void CCSTable::OnMeasureItem(int, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	lpMeasureItemStruct->itemHeight = imTextFontHeight + EXTRASPACE;
}

// OnDrawItem: draw each item of the owner-drawn list box
afx_msg void CCSTable::OnDrawItem(int, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
    CDC dc;
    CRect rect;
    BOOL blToggleFocusOnly;


    dc.Attach(lpDrawItemStruct->hDC);

    // change the visible position if necessary (redraw the header bar too)
    if (-dc.GetWindowOrg().x != imTableContentOffset)
    {
        imTableContentOffset = -dc.GetWindowOrg().x;
        DrawHeaderBar();
		  //DrawLeftScale();
    }
/*	 if(-dc.GetWindowOrg().y != imTableYOffset)
	 {
		 imTableYOffset = -dc.GetWindowOrg().y;
		 DrawLeftScale();
		 DrawRightScale();
	 }
*/
    // draw the given item 
    blToggleFocusOnly = !(lpDrawItemStruct->itemAction & (ODA_DRAWENTIRE | ODA_SELECT));
    DrawItem(dc, lpDrawItemStruct->itemID,
        lpDrawItemStruct->rcItem,
        blToggleFocusOnly,
        lpDrawItemStruct->itemState & ODS_SELECTED,
        lpDrawItemStruct->itemState & ODS_FOCUS);

    // fill blank items at the end of the table, if necessary
    if ((lpDrawItemStruct->itemID == (UINT)pomListBox->GetCount() - 1) &&   // last line?
        (lpDrawItemStruct->itemAction & ODA_DRAWENTIRE))
    {
        int bottom, height;
        int id = lpDrawItemStruct->itemID;

        // calculate size and position of each line
        pomListBox->GetClientRect(&rect);
        bottom = rect.bottom;
        rect = lpDrawItemStruct->rcItem;
        height = rect.bottom - rect.top;

        // draw lower empty part of table content line-by-line
        for (rect.top += height; rect.top <= bottom; rect.top += height)
        {
            rect.bottom = min(rect.top + height, bottom);
            DrawItem(dc, ++id, rect, false, false, false);
        }
    }

    dc.Detach();
}


/////////////////////////////////////////////////////////////////////////////
// Drawing Routines

// DrawHeaderBar: draw the header bar of the table
void CCSTable::DrawHeaderBar()
{
	//RST!
	if(bmMiniTable)
		return;
	//RST END

	CFont *polOldFont = 0;

    CDC *pDC;
	CPen *polOldPen = 0;
    CRect rect, rectTextClip;

    // prepare device context and font
    pDC = GetDC();
    pDC->SetBkMode(TRANSPARENT);
    pDC->SetTextColor(::GetSysColor(COLOR_WINDOWTEXT));

	polOldFont = pDC->SelectObject(pomHeaderFont);
    if (polOldFont == NULL)   // it works with both NULL and invalid font
	{
        polOldFont = pDC->SelectObject(&omCourier_Regular_10);
	}
		
    // paint background of the header bar

    GetClientRect(&rect);

    rect.bottom = rect.top + imHeaderFontHeight + 7;
    pDC->FillRect(&rect, &FaceBrush);
    // draw header of each field, one-by-one
	int ilCount = omLBorder.GetSize();

	polOldPen = pDC->SelectObject(&ShadowPen);
    for (int i = imLeftDockingColumns/*0*/; i < ilCount; i++)
    {
        // draw recessed-style container box

		int ilC2 = omHeaderDataArray.GetSize();
        if (i < ilC2)
		{
			pDC->SelectObject(&ShadowPen);
			pDC->MoveTo(imTableContentOffset + omLBorder[i] - 1 , rect.bottom - 1);
			pDC->LineTo(imTableContentOffset + omLBorder[i] - 1, rect.top + 1);
			pDC->LineTo(imTableContentOffset + omRBorder[i] + 1, rect.top + 1);
			pDC->SelectObject(&WhitePen);
			pDC->LineTo(imTableContentOffset + omRBorder[i] + 1, rect.bottom - 3);
			pDC->LineTo(imTableContentOffset + omLBorder[i] - 1, rect.bottom - 3);

			if(omHeaderDataArray[i].SeparatorType == SEPA_LIKEVERTICAL)
			{
				// draw the border line between the header bar and the table content
				pDC->SelectObject(&BlackPen);
				pDC->MoveTo(imTableContentOffset + omLBorder[i], rect.bottom - 1);
				pDC->LineTo(imTableContentOffset + omRBorder[i] + 1, rect.bottom - 1);
			}
			// else do not draw any separator
		}

        // draw header text in the given clip-region
		ilC2 = omHeaderDataArray.GetSize();
        if (i < ilC2)
        {
			UINT ilFormat;
            const char *s = omHeaderDataArray[i].Text;
            int n = omHeaderDataArray[i].Text.GetLength();
			//int n = omHeaderDataArray[i].Length;
            rectTextClip.left = imTableContentOffset + omLBorder[i] + 1;
            rectTextClip.top = rect.top + 2;
            rectTextClip.right = imTableContentOffset + omRBorder[i] + 1;
            rectTextClip.bottom = rect.bottom - 4;

			if( omHeaderDataArray[i].Alignment == COLALIGN_LEFT)
				ilFormat = DT_LEFT;
			else if (omHeaderDataArray[i].Alignment == COLALIGN_RIGHT)
				ilFormat = DT_RIGHT;
			else 
				ilFormat = DT_CENTER;

			CBrush *polBrush;
			polBrush = new CBrush;
			polBrush->CreateSolidBrush(omHeaderDataArray[i].BkColor);
			pDC->FillRect(&rectTextClip, polBrush);
			pDC->SetTextColor(omHeaderDataArray[i].TextColor);

			if ((pDC->SelectObject(omHeaderDataArray[i].Font)) == NULL)   // it works with both NULL and invalid font
			{
				if (pDC->SelectObject(pomHeaderFont) == NULL)
				{
					pDC->SelectObject(&omCourier_Regular_10);
				}
			}

            pDC->DrawText(s, n, &rectTextClip, /*DT_TOP*/DT_BOTTOM | ilFormat);
			delete polBrush;
        }
    }
	pDC->SelectObject(polOldFont);
	pDC->SelectObject(polOldPen);
    ReleaseDC(pDC);
}

void CCSTable::DrawLeftScale()
{
   CDC *pDC;
   CRect rect, rectTextClip;
   int ilYFrom, ilYTo;
   int ilX;

   CFont  *pOldFont  = NULL;
   CPen   *pOldPen   = NULL;
    // prepare device context and font
   pDC = GetDC();
   pDC->SetBkMode(TRANSPARENT);
   pDC->SetTextColor(::GetSysColor(COLOR_WINDOWTEXT));

   pOldFont = pDC->SelectObject(pomHeaderFont);
   if (pOldFont == NULL)   // it works with both NULL and invalid font
   {
       pOldFont = pDC->SelectObject(&omCourier_Regular_10);
   }
		
   // paint background of the header bar
	
   GetClientRect(&rect);

   //omLeftDockingRect.top = omLeftDockingRect.top + imHeaderFontHeight + 7;
	int ilOff = imHeaderFontHeight + 7;

	rect.top = omLeftDockingRect.top + ilOff;
	rect.bottom = rect.top  + imTextFontHeight;
	pDC->FillRect(&rect, &FaceBrush);
	pomListBox->GetVisibleLeftTopColumn(ilX, ilYFrom);
	pomListBox->GetVisibleRightBottomColumn(ilX, ilYTo);
	pOldPen = pDC->SelectObject(&ShadowPen);
	for(int i = ilYFrom; i < ilYTo+1; i++)
	{
		for(int j = 0; j < imLeftDockingColumns; j++)
		{

			pDC->SelectObject(&ShadowPen);
			pDC->MoveTo(omLBorder[j] - 1 , rect.bottom - 1);
			if(i == 0)
			{
				pDC->LineTo(omLBorder[j] - 1, rect.top-1);
				pDC->LineTo(omRBorder[j] + 1, rect.top-1);
			}
			else
			{
				pDC->LineTo(omLBorder[j] - 1, rect.top + 1);
				pDC->LineTo(omRBorder[j] + 1, rect.top + 1);
			}
			pDC->SelectObject(&WhitePen);
			pDC->LineTo(omRBorder[j] + 1, rect.bottom - 1);
			pDC->LineTo(omLBorder[j] - 1, rect.bottom - 1);

			if(omHeaderDataArray[j].SeparatorType == SEPA_LIKEVERTICAL)
			{
				// draw the border line between the header bar and the table content
				pDC->SelectObject(&BlackPen);
				pDC->MoveTo(omLBorder[j], rect.bottom - 1);
				pDC->LineTo(omRBorder[j] + 1, rect.bottom - 1);
			}
			// else do not draw any separator
			if(i < omLines.GetSize())
			{
				if(j < omLines[i].Columns.GetSize())
				{
					UINT ilFormat;
						const char *s = omLines[i].Columns[j].Text;
						int n = omLines[i].Columns[j].Text.GetLength();
					//int n = omHeaderDataArray[i].Length;
					if(i == 0)
					{
						rectTextClip.left = omLBorder[j] + 1;
						rectTextClip.top = rect.top;
						rectTextClip.right = omRBorder[j] + 1;
						rectTextClip.bottom = rect.bottom - 2;
					}
					else
					{
						rectTextClip.left = omLBorder[j] + 1;
						rectTextClip.top = rect.top + 2;
						rectTextClip.right = omRBorder[j] + 1;
						rectTextClip.bottom = rect.bottom - 2;
					}
					if( omLines[i].Columns[j].Alignment == COLALIGN_LEFT)
						ilFormat = DT_LEFT;
					else if (omLines[i].Columns[j].Alignment == COLALIGN_RIGHT)
						ilFormat = DT_RIGHT;
					else 
						ilFormat = DT_CENTER;

					CBrush *polBrush;
					polBrush = new CBrush;
					polBrush->CreateSolidBrush(COLORREF(SILVER)/*omLines[i].Columns[j].BkColor*/);
					pDC->FillRect(&rectTextClip, polBrush);
					pDC->SetTextColor(omLines[i].Columns[j].TextColor);

					if (pDC->SelectObject(omLines[i].Columns[j].Font) == NULL)   // it works with both NULL and invalid font
					{
						if (pDC->SelectObject(pomHeaderFont) == NULL)
						{
							pDC->SelectObject(&omCourier_Regular_10);
						}
					}

					pDC->DrawText(s, n, &rectTextClip, /*DT_TOP*/DT_BOTTOM | ilFormat);
					delete polBrush;
				}
			}
		}

		
/*-----*/
		rect.top = rect.bottom;
	   rect.bottom = rect.top + imTextFontHeight + 2;

	}
	pDC->SelectObject(pOldFont);
	pDC->SelectObject(pOldPen);
	ReleaseDC(pDC);
}


void CCSTable::DrawRightScale()
{
	CDC *pDC;
	CRect rect, rectTextClip;
	CFont  *pOldFont  = NULL;
	CPen   *pOldPen   = NULL;
	int ilYFrom, ilYTo;
	int ilX;

    // prepare device context and font
	pDC = GetDC();
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(::GetSysColor(COLOR_WINDOWTEXT));
	pOldFont = pDC->SelectObject(pomHeaderFont);
	if( pOldFont == NULL)   // it works with both NULL and invalid font
	{
	   pOldFont = pDC->SelectObject(&omCourier_Regular_10);
	}
		
   // paint background of the header bar
	

	//we have to calculate the absolute borders for right most docking columns
	CDWordArray olLBorder;
	CDWordArray olRBorder;
	int ilCount = omRBorder.GetSize();
	int ilAktOffset = 3;
	for(int l = ilCount-imRightDockingColumns; l < ilCount; l++)
	{
		olLBorder.Add(omRightDockingRect.left + ilAktOffset);
		olRBorder.Add(omRightDockingRect.left + ilAktOffset+ omRBorder[l]-omLBorder[l] - 4);
//		olLBorder.Add(omRightDockingRect.left + ilAktOffset);
//		olRBorder.Add(omRightDockingRect.left + ilAktOffset + omHeaderDataArray[l].Length);
		ilAktOffset = omRBorder[l]-omLBorder[l]+3;//omHeaderDataArray[l].Length;

	}
	int ilOff = imHeaderFontHeight + 7;

	rect.top = omRightDockingRect.top + ilOff;
   rect.bottom = rect.top  + imTextFontHeight;
	CRect olTmpRect = omRightDockingRect;
	olTmpRect.top += ilOff;
   pDC->FillRect(&olTmpRect/*rect*/, &FaceBrush);
	pomListBox->GetVisibleLeftTopColumn(ilX, ilYFrom);
	pomListBox->GetVisibleRightBottomColumn(ilX, ilYTo);
	pOldPen = pDC->SelectObject(&ShadowPen);
	for(int i = ilYFrom; i < ilYTo+1; i++)
	{
		int ilCount = omRBorder.GetSize();
		for(int j = ilCount-imRightDockingColumns; j < ilCount; j++)
		{
			int ilIndex = j - (ilCount-imRightDockingColumns);
			//int f = olLBorder[ilIndex];
			//int t = olRBorder[ilIndex];
			pDC->SelectObject(&ShadowPen);
			pDC->MoveTo(olLBorder[ilIndex] - 1 , rect.bottom - 1);
			if(i == 0)
			{
				pDC->LineTo(olLBorder[ilIndex] - 1, rect.top-1);
				pDC->LineTo(olRBorder[ilIndex] + 1, rect.top-1);
			}
			else
			{
				pDC->LineTo(olLBorder[ilIndex] - 1, rect.top + 1);
				pDC->LineTo(olRBorder[ilIndex] + 1, rect.top + 1);
			}
			pDC->SelectObject(&WhitePen);
			pDC->LineTo(olRBorder[ilIndex] + 1, rect.bottom - 1);
			pDC->LineTo(olLBorder[ilIndex] - 1, rect.bottom - 1);

			if(omHeaderDataArray[j].SeparatorType == SEPA_LIKEVERTICAL)
			{
				// draw the border line between the header bar and the table content
				pDC->SelectObject(&BlackPen);
				pDC->MoveTo(olLBorder[ilIndex], rect.bottom - 1);
				pDC->LineTo(olRBorder[ilIndex] + 1, rect.bottom - 1);
			}
			// else do not draw any separator
			if(i < omLines.GetSize())
			{
				if(j < omLines[i].Columns.GetSize())
				{
					UINT ilFormat;
						const char *s = omLines[i].Columns[j].Text;
						int n = omLines[i].Columns[j].Text.GetLength();
					//int n = omHeaderDataArray[i].Length;
					if(i == 0)
					{
						rectTextClip.left = olLBorder[ilIndex] + 1;
						rectTextClip.top = rect.top;
						rectTextClip.right = olRBorder[ilIndex] + 1;
						rectTextClip.bottom = rect.bottom - 2;
					}
					else
					{
						rectTextClip.left = olLBorder[ilIndex] + 1;
						rectTextClip.top = rect.top + 2;
						rectTextClip.right = olRBorder[ilIndex] + 1;
						rectTextClip.bottom = rect.bottom - 2;
					}
					if( omLines[i].Columns[j].Alignment == COLALIGN_LEFT)
						ilFormat = DT_LEFT;
					else if (omLines[i].Columns[j].Alignment == COLALIGN_RIGHT)
						ilFormat = DT_RIGHT;
					else 
						ilFormat = DT_CENTER;

					CBrush *polBrush;
					polBrush = new CBrush;
					polBrush->CreateSolidBrush(COLORREF(SILVER)/*omLines[i].Columns[j].BkColor*/);
					pDC->FillRect(&rectTextClip, polBrush);
					pDC->SetTextColor(omLines[i].Columns[j].TextColor);

					if (pDC->SelectObject(omLines[i].Columns[j].Font) == NULL)   // it works with both NULL and invalid font
					{
						if (pDC->SelectObject(pomHeaderFont) == NULL)
						{
							pDC->SelectObject(&omCourier_Regular_10);
						}
					}

					pDC->DrawText(s, n, &rectTextClip, /*DT_TOP*/DT_BOTTOM | ilFormat);
					delete polBrush;
				}
			}
		}

		
/*-----*/
		rect.top = rect.bottom;
	   rect.bottom = rect.top + imTextFontHeight + 2;

	}
	while(olLBorder.GetSize() > 0)
	{
		olLBorder.RemoveAt(0);
	}
	while(olRBorder.GetSize() > 0)
	{
		olRBorder.RemoveAt(0);
	}
	pDC->SelectObject(pOldFont);
	pDC->SelectObject(pOldPen);
	ReleaseDC(pDC);
}

// DrawItem: draw an item in the table content area
void CCSTable::DrawItem(CDC &dc, UINT itemID, const CRect &rectClip,
    BOOL bpIsToggleFocusOnly, BOOL bpIsSelected, BOOL bpIsFocus)
{
    CString s;                  // comma-list represent record to be displayed
    //int n;                      // length of string "s"
    //int left;            // position to extract field from the comma-list

    CBrush TextBkBrush;         // helper brush (derived from lmTextBkColor)
    CBrush HighlightBkBrush;    // helper brush (derived from lmHighlightBkColor)
    CPen *pOldPen;
    CBrush *pOldBrush;
    CFont *pOldFont;
    int OldBkMode;
    COLORREF OldBkColor;
    COLORREF OldTextColor;
	bool blIsItemEmpty = false;
	bool blDockingmode = false;
	CRect ilOrigRectClip;
	//bool isDockingColumn = false;
	ilOrigRectClip = rectClip;
	if(imLeftDockingColumns > 0)
	{
		blDockingmode = true;
	}
    // prepare helper drawing objects
	bool blIsValidItemID = (itemID < (UINT)omLines.GetSize());
	COLORREF llTextColor = (blIsValidItemID)? omColor[itemID]: lmTextColor;
	COLORREF llTextBkColor = (blIsValidItemID)? omBkColor[itemID]: lmTextBkColor;
	bool blIsThickSeparateLine = blIsValidItemID && (omSeparatorType[itemID] == SEPA_THICK);
	CFont *polFont = (blIsValidItemID)? (CFont *)omFontPtr[itemID]: NULL;
    //TextBkBrush.CreateSolidBrush(llTextBkColor);
    HighlightBkBrush.CreateSolidBrush(lmHighlightBkColor);

    // change selected objects in current device context
    pOldPen = dc.SelectObject(&BlackPen);
    SeparatorBrush.UnrealizeObject();       // reset brush origin (for consistent pattern)
    dc.SetBrushOrg(dc.GetWindowOrg().x % 8, 0);
    pOldBrush = dc.SelectObject(&SeparatorBrush);
    if ((pOldFont = dc.SelectObject(polFont)) == NULL)  // it works with both NULL and invalid font
        pOldFont = (CFont *)dc.SelectObject(&omCourier_Regular_10);
    OldBkMode = dc.SetBkMode(TRANSPARENT);
	BOOL blIsDraggable = (blIsValidItemID)? omDragEnabled[itemID]: FALSE;
    OldBkColor = dc.SetBkColor(bpIsSelected && blIsDraggable? lmHighlightBkColor: llTextBkColor);
    OldTextColor = dc.SetTextColor(bpIsSelected && blIsDraggable? lmHighlightColor: llTextColor);

	CFont *polOldFont = NULL;
    // prepare data for scanning

    // paint each field of the given item



	if((int)itemID >= omLines.GetSize())
	{
		s = "";
	}
    int i;
	int ilInitCount = 0;
	if(blDockingmode == true)
	{
		ilInitCount = ilInitCount + imLeftDockingColumns;
	}
	int ilCount1 = omLBorder.GetSize() - imRightDockingColumns;
	int ilLines = omLines.GetSize();
    for (i = imLeftDockingColumns; i < ilCount1; i++)
    {
		TABLE_COLUMN *prlColumn = 0;
		TABLE_COLUMN *prlUpperColumn = NULL;
        CRect rect, rectTextClip;
		int olType = COLUMN_STRING;
		if(itemID < (UINT)ilLines)
		{
			if(i < omLines[itemID].Columns.GetSize())
			{
	
				olType = (i < omLines[itemID].Columns.GetSize()-imRightDockingColumns)? omLines[itemID].Columns[i].Type: COLUMN_STRING;
				if(itemID < (UINT)omLines.GetSize())
				{
					if(omLines[itemID].Columns.GetSize() > 0)
					{
						prlColumn = &omLines[itemID].Columns[i];
					}
				}
				prlUpperColumn = ((itemID == 0) ||(i > omLines[itemID-1].Columns.GetSize()-imRightDockingColumns))? NULL : &omLines[itemID-1].Columns[i]; 
			}
			else
			{
				blIsItemEmpty = true;
			}
		}
		else
		{
			blIsItemEmpty = true;
		}
			

        // do nothing if it is unnecessary to redraw this column CPtrArray
        rect = ilOrigRectClip;
        rect.left = omLBorder[i] - 1;
        rect.right = omRBorder[i] + 3;
		if (rect.left > rect.right)
			rect.left = 1;
        if (!rect.IntersectRect(&rect, &ilOrigRectClip))  // this column is out off drawing rectangle?
            continue;

        // drawing interior of the given field
        rect.left = omLBorder[i] + 1;
        rect.top = ilOrigRectClip.top;
        rect.right = omRBorder[i] - 1;
        rect.bottom = ilOrigRectClip.bottom - 1;

		//GetWindowRect

		if(prlColumn != NULL && blIsItemEmpty == false)
		{
			prlColumn->rect = rect;
		}

        if (!bpIsToggleFocusOnly)
        {
			if (blIsItemEmpty == false)
			{
				CBrush BkBrush;
				CBrush TextBrush;
				// Draw the item background (using helper brushes).
				// The function FillRect() just won't fill the most right pixels.
				// So, I fixed it here before and after FillRect().
				rect.right+=3; rect.left-=3;rect.bottom++; 
				if(prlUpperColumn != NULL)
				{
					if(prlUpperColumn->SeparatorType == SEPA_LIKEVERTICAL)
					{
						rect.top++;
					}
				}

				if(prlColumn->BkColor == llTextBkColor)
				{
					BkBrush.CreateSolidBrush(llTextBkColor);
				}
				else
				{
					BkBrush.CreateSolidBrush(prlColumn->BkColor);
				}

				if(bpIsSelected==TRUE)
				{
					if(blShowSelection == true)
					{
						dc.FillRect(&rect, &HighlightBkBrush);
					}
					else
					{
						dc.FillRect(&rect, &BkBrush);
					}

				}
				else
				{
					dc.FillRect(&rect, &BkBrush);
				}

				// Old moethode to decide selected mode or not selected mode
				
				rect.right-=3; rect.left+=3;rect.bottom--; 
				if(prlUpperColumn != NULL)
				{
					if(prlUpperColumn->SeparatorType == SEPA_LIKEVERTICAL)
					{
						rect.top--;
					}
				}


				COLORREF oldColor;
				// draw text (content of the table) in the given clip-region
				UINT ilFormat;
				if(bpIsSelected == TRUE){
					if(blShowSelection == true){
						oldColor = dc.SetTextColor(COLORREF(WHITE));	
					}
					else{
						oldColor = dc.SetTextColor(prlColumn->TextColor);	
					}
				}
				else{
					oldColor = dc.SetTextColor(prlColumn->TextColor);	
				}
				rectTextClip = rect;
				rectTextClip.left++;	// for lefting one pixel before the text CString
				rectTextClip.right--;
				switch(prlColumn->Alignment)
				{
				case COLALIGN_LEFT:
					ilFormat =  DT_LEFT;
					break;
				case COLALIGN_RIGHT:
					ilFormat = DT_RIGHT;
					break;
				case COLALIGN_CENTER:
					ilFormat = DT_CENTER;
					break;
				default:
					ilFormat = DT_LEFT;
				}
				if ((polOldFont = dc.SelectObject(prlColumn->Font)) == NULL)   // it works with both NULL and invalid font
				{
					if ((polOldFont = dc.SelectObject(pomHeaderFont)) == NULL)
					{
						polOldFont = dc.SelectObject(&omCourier_Regular_10);
					}
				}
				
				int ilRealTextHeight = 0;
				CRect olTmpRect;
				//provide the formatting of the column
				switch(prlColumn->Type)
				{
				case COLUMN_STRING:
					ilRealTextHeight = dc.DrawText(prlColumn->Text, prlColumn->Text.GetLength(), &olTmpRect, DT_CALCRECT);
					rectTextClip.top = rectTextClip.bottom - ilRealTextHeight - 2;
					dc.DrawText(prlColumn->Text, prlColumn->Text.GetLength(), &rectTextClip, ilFormat);
					break;
				case COLUMN_INT:
				case COLUMN_LONG:
					ilFormat = DT_RIGHT;
					ilRealTextHeight = dc.DrawText(prlColumn->Text, prlColumn->Text.GetLength(), &olTmpRect, DT_CALCRECT);
					rectTextClip.top = rectTextClip.bottom - ilRealTextHeight - 2;
					dc.DrawText(prlColumn->Text, prlColumn->Text.GetLength(), &rectTextClip, ilFormat);
					break;
				case COLUMN_FLOAT:
					ilFormat = DT_RIGHT;
					ilRealTextHeight = dc.DrawText(prlColumn->Text, prlColumn->Text.GetLength(), &olTmpRect, DT_CALCRECT);
					rectTextClip.top = rectTextClip.bottom - ilRealTextHeight - 2;
					dc.DrawText(prlColumn->Text, prlColumn->Text.GetLength(), &rectTextClip, ilFormat);
					break;
				case COLUMN_DOUBLE:
					ilFormat = DT_RIGHT;
					ilRealTextHeight = dc.DrawText(prlColumn->Text, prlColumn->Text.GetLength(), &olTmpRect, DT_CALCRECT);
					rectTextClip.top = rectTextClip.bottom - ilRealTextHeight - 2;
					dc.DrawText(prlColumn->Text, prlColumn->Text.GetLength(), &rectTextClip, ilFormat);
					break;
				case COLUMN_DATE:
					ilRealTextHeight = dc.DrawText(prlColumn->Text, prlColumn->Text.GetLength(), &olTmpRect, DT_CALCRECT);
					rectTextClip.top = rectTextClip.bottom - ilRealTextHeight - 2;
					dc.DrawText(prlColumn->Text, prlColumn->Text.GetLength(), &rectTextClip, ilFormat);
					break;
				case COLUMN_HOUR:
					ilRealTextHeight = dc.DrawText(prlColumn->Text, prlColumn->Text.GetLength(), &olTmpRect, DT_CALCRECT);
					rectTextClip.top = rectTextClip.bottom - ilRealTextHeight - 2;
					dc.DrawText(prlColumn->Text, prlColumn->Text.GetLength(), &rectTextClip, ilFormat);
					break;
				case COLUMN_bool:
				case COLUMN_BITMAP:
				case COLUMN_EDIT:
				default:
					ilRealTextHeight = dc.DrawText(prlColumn->Text, prlColumn->Text.GetLength(), &olTmpRect, DT_CALCRECT);
					rectTextClip.top = rectTextClip.bottom - ilRealTextHeight - 2;
					dc.DrawText(prlColumn->Text, prlColumn->Text.GetLength(), &rectTextClip, ilFormat);
				}

				dc.SetTextColor(oldColor);	
				dc.SetBkColor(OldBkColor);
				dc. SelectObject(polOldFont);
				rectTextClip.right++;
				// draw the separator line between items
				switch(prlColumn->VerticalSeparator)
				{
					case SEPA_NONE:
						// don nothing
						break;
					case SEPA_LIKEVERTICAL:
						dc.SelectObject(&FacePen);
						dc.MoveTo(omRBorder[i]-1, ilOrigRectClip.top);
						dc.LineTo(omRBorder[i]-1, ilOrigRectClip.bottom);
						dc.SelectObject(&ShadowPen);
						dc.MoveTo(omRBorder[i], ilOrigRectClip.top);
						dc.LineTo(omRBorder[i], ilOrigRectClip.bottom);
						dc.SelectObject(&BlackPen);
						dc.MoveTo(omRBorder[i]+1, ilOrigRectClip.top);
						dc.LineTo(omRBorder[i]+1, ilOrigRectClip.bottom);
						break;
					case SEPA_THICK:
						dc.SelectObject(&BlackPen);
						dc.MoveTo(omRBorder[i], ilOrigRectClip.top);
						dc.LineTo(omRBorder[i], ilOrigRectClip.bottom);
						dc.MoveTo(omRBorder[i]+1, ilOrigRectClip.top);
						dc.LineTo(omRBorder[i]+1, ilOrigRectClip.bottom);
					case SEPA_NORMAL:
						dc.SelectObject(&BlackPen);
						dc.MoveTo(omRBorder[i]+1, ilOrigRectClip.top);
						dc.LineTo(omRBorder[i]+1, ilOrigRectClip.bottom);
						break;
					case SEPA_DASHDOT:
						dc.SelectObject(&DashDotPen);
						dc.MoveTo(omRBorder[i]+1, ilOrigRectClip.top);
						dc.LineTo(omRBorder[i]+1, ilOrigRectClip.bottom);
						break;
					case SEPA_DASH:
						dc.SelectObject(&DashPen);
						dc.MoveTo(omRBorder[i]+1, ilOrigRectClip.top);
						dc.LineTo(omRBorder[i]+1, ilOrigRectClip.bottom);
						break;
					case SEPA_DOT:
						dc.SelectObject(&DotPen);
						dc.MoveTo(omRBorder[i]+1, ilOrigRectClip.top);
						dc.LineTo(omRBorder[i]+1, ilOrigRectClip.bottom);
						break;
				}//end switch(prlColumn->VerticalSeparator)
				switch(prlColumn->SeparatorType)
				{
				case SEPA_NORMAL:
					if(prlColumn->VerticalSeparator == SEPA_LIKEVERTICAL)
					{
						dc.SelectObject(&BlackPen);
						dc.MoveTo(omLBorder[i]-1, ilOrigRectClip.bottom-1);
						dc.LineTo(omRBorder[i]-1, ilOrigRectClip.bottom-1);
					}
					else
					{
						dc.SelectObject(&BlackPen);
						dc.MoveTo(omLBorder[i]-3, ilOrigRectClip.bottom-1);
						dc.LineTo(omRBorder[i]+3, ilOrigRectClip.bottom-1);
					}
					break;
				case SEPA_DASHDOT:
					if(prlColumn->VerticalSeparator == SEPA_LIKEVERTICAL)
					{
						dc.SelectObject(&DashDotPen);
						dc.MoveTo(omLBorder[i]-1, ilOrigRectClip.bottom-1);
						dc.LineTo(omRBorder[i]-1, ilOrigRectClip.bottom-1);
					}
					else
					{
						dc.SelectObject(&DashDotPen);
						dc.MoveTo(omLBorder[i]-3, ilOrigRectClip.bottom-1);
						dc.LineTo(omRBorder[i]+3, ilOrigRectClip.bottom-1);
					}
					break;
				case SEPA_DOT:
					if(prlColumn->VerticalSeparator == SEPA_LIKEVERTICAL)
					{
						dc.SelectObject(&DotPen);
						dc.MoveTo(omLBorder[i]-1, ilOrigRectClip.bottom-1);
						dc.LineTo(omRBorder[i]-1, ilOrigRectClip.bottom-1);
					}
					else
					{
						dc.SelectObject(&DotPen);
						dc.MoveTo(omLBorder[i]-3, ilOrigRectClip.bottom-1);
						dc.LineTo(omRBorder[i]+3, ilOrigRectClip.bottom-1);
					}
					break;
				case SEPA_DASH:
					if(prlColumn->VerticalSeparator == SEPA_LIKEVERTICAL)
					{
						dc.SelectObject(&DashPen);
						dc.MoveTo(omLBorder[i]-1, ilOrigRectClip.bottom-1);
						dc.LineTo(omRBorder[i]-1, ilOrigRectClip.bottom-1);
					}
					else
					{
						dc.SelectObject(&DashPen);
						dc.MoveTo(omLBorder[i]-3, ilOrigRectClip.bottom-1);
						dc.LineTo(omRBorder[i]+3, ilOrigRectClip.bottom-1);
					}
					break;
				case SEPA_THICK:
					if(prlColumn->VerticalSeparator == SEPA_LIKEVERTICAL)
					{
						dc.SelectObject(&BlackPen);
						dc.MoveTo(omLBorder[i]-1, ilOrigRectClip.bottom-1);
						dc.LineTo(omRBorder[i]-1, ilOrigRectClip.bottom-1);
						dc.MoveTo(omLBorder[i]-1, ilOrigRectClip.bottom);
						dc.LineTo(omRBorder[i]-1, ilOrigRectClip.bottom);
					}
					else
					{
						dc.SelectObject(&BlackPen);
						dc.MoveTo(omLBorder[i]-3, ilOrigRectClip.bottom-1);
						dc.LineTo(omRBorder[i]+3, ilOrigRectClip.bottom-1);
						dc.MoveTo(omLBorder[i]-3, ilOrigRectClip.bottom);
						dc.LineTo(omRBorder[i]+3, ilOrigRectClip.bottom);
					}
					break;
				case SEPA_LIKEVERTICAL:
					if(prlColumn->VerticalSeparator == SEPA_LIKEVERTICAL)
					{
						dc.SelectObject(&FacePen);
						dc.MoveTo(omLBorder[i]-1, ilOrigRectClip.bottom-2);
						dc.LineTo(omRBorder[i]-1, ilOrigRectClip.bottom-2);
						dc.SelectObject(&ShadowPen);
						dc.MoveTo(omLBorder[i]-1, ilOrigRectClip.bottom-1);
						dc.LineTo(omRBorder[i]-1, ilOrigRectClip.bottom-1);
						dc.SelectObject(&BlackPen);
						dc.MoveTo(omLBorder[i]-1, ilOrigRectClip.bottom);
						dc.LineTo(omRBorder[i]-1, ilOrigRectClip.bottom);
					}
					else
					{
						dc.SelectObject(&FacePen);
						dc.MoveTo(omLBorder[i]-3, ilOrigRectClip.bottom-2);
						dc.LineTo(omRBorder[i]+3, ilOrigRectClip.bottom-2);
						dc.SelectObject(&ShadowPen);
						dc.MoveTo(omLBorder[i]-3, ilOrigRectClip.bottom-1);
						dc.LineTo(omRBorder[i]+3, ilOrigRectClip.bottom-1);
						dc.SelectObject(&BlackPen);
						dc.MoveTo(omLBorder[i]-3, ilOrigRectClip.bottom);
						dc.LineTo(omRBorder[i]+3, ilOrigRectClip.bottom);
					}
					break;
				}//End switch(prlColumn->SeparatorType)
				
			}
			else //
			{
				CBrush BkBrush;
				CBrush TextBrush;
				// Draw the item background (using helper brushes).
				// The function FillRect() just won't fill the most right pixels.
				// So, I fixed it here before and after FillRect().

				rect.right++, rect.bottom++;
				BkBrush.CreateSolidBrush(WHITE);
				dc.FillRect(&rect, &BkBrush);
				rect.right--, rect.bottom--;


				if(bmDefaultSeparator == true)
				{
					DWORD llRop = (blIsThickSeparateLine)? BLACKNESS: PATCOPY;
					if (rect.bottom == rect.top + imTextFontHeight + EXTRASPACE - 1)
					{
						COLORREF oldColor = dc.SetTextColor(BLACK);	// make sure that it will be black dotted
						dc.PatBlt(rect.left, rect.bottom, rect.right - rect.left + 1, 1, llRop);
						if (blIsThickSeparateLine)
						{
							dc.PatBlt(rect.left, rect.bottom-1, rect.right - rect.left + 1, 1, llRop);
						}
						dc.SetTextColor(oldColor);
						dc.SetBkColor(OldBkColor);
					}
				}
				// no draw text for color fields
			}
            // draw the ridge (vertical separator)
			if(bmDefaultSeparator == true)
			{
				dc.SelectObject(&ShadowPen);
				dc.MoveTo(omLBorder[i] - 1, ilOrigRectClip.top);
				dc.LineTo(omLBorder[i] - 1, ilOrigRectClip.bottom);
				dc.SelectObject(&BlackPen);
				dc.MoveTo(omLBorder[i], ilOrigRectClip.top);
				dc.LineTo(omLBorder[i], ilOrigRectClip.bottom);
				dc.MoveTo(omRBorder[i], ilOrigRectClip.top);
				dc.LineTo(omRBorder[i], ilOrigRectClip.bottom);
				dc.SelectObject(&WhitePen);
				dc.MoveTo(omRBorder[i] + 1, ilOrigRectClip.top);
				dc.LineTo(omRBorder[i] + 1, ilOrigRectClip.bottom);
				dc.SelectObject(&FacePen);
				dc.MoveTo(omRBorder[i] + 2, ilOrigRectClip.top);
				dc.LineTo(omRBorder[i] + 2, ilOrigRectClip.bottom);
				
			}
			DWORD llRop = PATCOPY;
			if (rect.bottom == rect.top + imTextFontHeight + EXTRASPACE - 1)
			{
				dc.PatBlt(rect.left, rect.bottom, rect.right - rect.left + 1, 1, llRop);
				if (blIsThickSeparateLine)
				{
					dc.PatBlt(rect.left, rect.bottom-1, rect.right - rect.left + 1, 1, llRop);
				}
			}

			if(prlColumn != NULL && blIsItemEmpty == false)
			{
				if(prlColumn->SpecialControl != (CWnd*)NULL)
				{
					prlColumn->SpecialControl->ShowWindow(SW_SHOW);
					
				}
			}
        }

        // need to redraw focus rectangle?
        if ((bpIsFocus | bpIsToggleFocusOnly))// && (blShowSelection == true))
            dc.DrawFocusRect(&rect);
    }//End for (i = 0; i < omLBorder.GetSize(); i++)

    // delete helper drawing objects
    TextBkBrush.DeleteObject();
    HighlightBkBrush.DeleteObject();

    // restore the old device context (necessary for owner-drawn controls in MFC)
    dc.SelectObject(pOldPen);
    dc.SelectObject(pOldBrush);
    dc.SelectObject(pOldFont);
    dc.SetBkMode(OldBkMode);
    dc.SetBkColor(OldBkColor);
    dc.SetTextColor(OldTextColor);
}

LONG CCSTable::OnDragOver(UINT wParam, LONG lParam)
{
	long llRc;

 	CWnd *pWnd = pomParentWindow;	// surrounding window
    llRc = pWnd->SendMessage(WM_DRAGOVER, wParam, lParam);
	return llRc;
}

LONG CCSTable::OnDrop(UINT wParam, LONG lParam)
{
	long llRc;

 	CWnd *pWnd = pomParentWindow;	// surrounding window
    llRc = pWnd->SendMessage(WM_DROP, wParam, lParam);
	return llRc;
}

void CCSTable::OnMouseMove(UINT nFlags, CPoint point)
{
	nFlags;
	int ilX = pomListBox->GetColumnFromPoint(point);
	int ilY = pomListBox->GetItemFromPoint(point);
	if(ilX != -1 && ilY != -1)
	{
		if(ilY < omLines.GetSize())
		{
			if(ilX < omLines[ilY].Columns.GetSize())
			{
				if(omLines[ilY].Columns[ilX].blToolTip == true )
				{
/*					 if(pomToolTip != NULL)
					 {
						 delete pomToolTip;
					 }
				 	 pomToolTip = new CToolTipCtrl();
					 pomToolTip->Create( this, TTS_ALWAYSTIP);
					 TOOLINFO rlToolInfo;
					 strcpy(rlToolInfo.lpszText, omLines[ilY].Columns[ilX].Text); 
					 CRect olRect;
					 pomListBox->GetItemRect(ilY,&olRect);
					 rlToolInfo.rect = olRect;
					 pomToolTip->SetToolInfo(&rlToolInfo);
				    pomToolTip->SetDelayTime( 2);
*/
				}
			}
		}
	}
}

BOOL CCSTable::OnToolTipNotify( UINT id, NMHDR * pNMHDR, LRESULT * pResult )
{
	id;
	pResult;
    TOOLTIPTEXT *pTTT = (TOOLTIPTEXT *)pNMHDR;
    UINT nID =pNMHDR->idFrom;
    if (pTTT->uFlags & TTF_IDISHWND)
    {
        // idFrom is actually the HWND of the tool
        nID = ::GetDlgCtrlID((HWND)nID);
        if(nID)
        {
            strcpy(pTTT->lpszText, "HALLO ALEX,\n\nich m�chte dich nur an deine W�nsche erinnern!!!\n\n REINER ");// = MAKEINTRESOURCE(nID);
            //pTTT->lpszText = MAKEINTRESOURCE(nID);
            pTTT->hinst = AfxGetResourceHandle();
            return TRUE;
        }
    }

    return FALSE;
}

int CCSTable::OnToolHitTest( CPoint point, TOOLINFO* pTI )
{
	pTI->hwnd = m_hWnd;//   Handle to a window
	pTI->uId = (UINT)GetSafeHwnd();//(UINT)hWndChild   Handle to a child window
	pTI->uFlags |= TTF_IDISHWND;//   Handle of the tool
	int ilX = pomListBox->GetColumnFromPoint(point);
	int ilY = pomListBox->GetItemFromPoint(point);
	if(ilX != -1 && ilY != -1)
	{
		if(ilY < omLines.GetSize())
		{
			if(ilX < omLines[ilY].Columns.GetSize())
			{
				if(omLines[ilY].Columns[ilX].blToolTip == true )
				{

					sprintf(pTI->lpszText, "%s", omLines[ilY].Columns[ilX].Text);
					return 1;
				}
			}
		}
	}
	return -1;
	//pTI->lpszText = LPSTR_TEXTCALLBACK   Pointer to the string that is to be displayed in the specified window 
}

//-------------------------------------------------------------
// set the rage for the leftmost docking columns
// e.g. if ipNumOfColumns == 3 ==> the first 3 columns are
// docking
void CCSTable::SetLeftDockingRange(int ipNumOfColumns) 
{
	imLeftDockingColumns = ipNumOfColumns;
}

void CCSTable::SetRightDockingRange(int ipNumOfColumns) 
{
	imRightDockingColumns = ipNumOfColumns;
}

void CCSTable::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// TODO: Add your message handler code here and/or call default
	
	CWnd::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CCSTable::SetShowSelection(bool bpSelectMode)
{
	blShowSelection = bpSelectMode;
}



//******************************************************************************
// MWO: Implementation of inplace-edit functionality
// Date: 19.03.1997
//******************************************************************************
bool CCSTable::SetColumnEditable(int ipColumnNo, bool bpEdit /*true*/)
{
	int ilC1 = omLines.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if((ipColumnNo < 0) || (ipColumnNo > omLines[i].Columns.GetSize()))
		{
			//return false;
			// do nothing ==> bcause it is possible, that the number of columns per line
			// is not equal for every line
		}
		else
		{
			omLines[i].Columns[ipColumnNo].blIsEditable = bpEdit;
		}
	}
	return true;
}

bool CCSTable::SetColumnEditable(int ipLineNo, int ipColumnNo, bool bpEdit /*true*/)
{
    if (ipLineNo == -1 && m_hWnd != NULL)   // use default line number?
        ipLineNo = pomListBox->GetCaretIndex();

    if (!(0 <= ipLineNo && ipLineNo < omLines.GetSize()))    // valid line number?
    {
        //RC(false);
        return false;
    }
	if((ipColumnNo < 0) || (ipColumnNo > omLines[ipLineNo].Columns.GetSize()))
	{
        //return false;
		// do nothing ==> bcause it is possible, that the number of columns per line
		// is not equal for every line
	}
	else
	{
		int ilCount = omLines[ipLineNo].Columns.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			if(i == ipColumnNo)
			{
				omLines[ipLineNo].Columns[i].blIsEditable = bpEdit;
			}
		}
	}

    InvalidateRect(NULL, false);
    return true;
}

bool MakeSpecialControl(int ilX, int ilY, int ilCtlType)
{
	ilY;
	ilX;
	switch(ilCtlType)
	{
	case CTL_BUTTON: 
		break;
	case CTL_RADIOBUTTON: 
		break;
	case CTL_CHECKBUTTON: 
		break;
	case CTL_COMBOBOX:
		break;
	case CTL_NORMAL: 
		break;
	default:
		break;
	}
	return true;
}


void CCSTable::CheckScrolling()
{
	int ilX=0, ilY=0;
	if(pomListBox->GetVisibleLeftTopColumn(ilX, ilY) == true)
	{
		CScrollBar* polScrollH = pomListBox->GetScrollBarCtrl(SB_HORZ);
		CScrollBar* polScrollV = pomListBox->GetScrollBarCtrl(SB_VERT);
		if(ilY < imCurrentEditPosY)
		{
			//scroll up
			if(polScrollV != NULL)
			{
				LPARAM lp = MAKELPARAM(SB_PAGEUP, 0);
				polScrollV->SendMessage(WM_VSCROLL, lp, 0);
			}
		}
		if(ilY > imCurrentEditPosY)
		{
			//scroll down
			if(polScrollV != NULL)
			{
				LPARAM lp = MAKELPARAM(SB_PAGEDOWN, 0);
				polScrollV->SendMessage(WM_VSCROLL, lp, 0);
			}
		}
		if(ilX < imCurrentEditPosX)
		{
			//scroll left
			if(polScrollH != NULL)
			{
				LPARAM lp = MAKELPARAM(SB_PAGELEFT, 0);
				polScrollV->SendMessage(WM_HSCROLL, lp, 0);
			}
		}
		if(ilX > imCurrentEditPosX)
		{
			//scroll right
			if(polScrollH != NULL)
			{
				LPARAM lp = MAKELPARAM(SB_PAGERIGHT, 0);
				polScrollV->SendMessage(WM_HSCROLL, lp, 0);
			}
		}
	}
	if(pomListBox->GetVisibleRightBottomColumn(ilX, ilY) == true)
	{
		SCROLLINFO olScrollInfo;
		int ilScrollPos = -1;
		int ilPageDelta = -1;
		CScrollBar* polScrollH = pomListBox->GetScrollBarCtrl(SB_HORZ);
		CScrollBar* polScrollV = pomListBox->GetScrollBarCtrl(SB_VERT);
		if(pomListBox->GetScrollInfo(SB_VERT, &olScrollInfo, SIF_ALL ) == TRUE)
		{
			ilPageDelta = olScrollInfo.nPage;
			ilScrollPos = olScrollInfo.nPos;
		}
		if(ilY < imCurrentEditPosY)
		{
			//scroll up
			//pomListBox->SetScrollPos(SB_VERT, (ilScrollPos-1));//-ilPageDelta-1) );
			//pomListBox->ScrollWindow(0, ilScrollPos/*-ilPageDelta*/-1);
			//pomListBox->UpdateWindow( );
			pomListBox->SendMessage(WM_VSCROLL, MAKELPARAM(SB_LINEUP, 0));
			UpdateWindow( );
			if(polScrollV != NULL)
			{
				LPARAM lp = MAKELPARAM(SB_PAGEUP, 0);
				polScrollV->SendMessage(WM_VSCROLL, lp, 0);

			}
		}
		if(ilY > imCurrentEditPosY)
		{
			//scroll down
			//pomListBox->SetScrollPos(SB_VERT, (ilScrollPos+1));//ilPageDelta-1) );
			//pomListBox->ScrollWindow(0, ilScrollPos+1/*ilPageDelta-1*/);
			//pomListBox->UpdateWindow( );
			pomListBox->SendMessage(WM_VSCROLL, MAKELPARAM(SB_LINEDOWN, 0));
			UpdateWindow( );
			if(polScrollV != NULL)
			{
				LPARAM lp = MAKELPARAM(SB_PAGEDOWN, 0);
				polScrollV->SendMessage(WM_VSCROLL, lp, 0);
			}
		}
		if(ilX < imCurrentEditPosX)
		{
			//scroll left
			//pomListBox->SetScrollPos(SB_HORZ, (ilScrollPos-ilPageDelta-1) );
			pomListBox->SendMessage(WM_HSCROLL, MAKELPARAM(SB_LINELEFT, 0));
			UpdateWindow( );
			if(polScrollH != NULL)
			{
				LPARAM lp = MAKELPARAM(SB_PAGELEFT, 0);
				polScrollV->SendMessage(WM_HSCROLL, lp, 0);
			}
		}
		if(ilX > imCurrentEditPosX)
		{
			//scroll right
			pomListBox->SendMessage(WM_HSCROLL, MAKELPARAM(SB_LINERIGHT, 0));
			UpdateWindow( );
			if(polScrollH != NULL)
			{
				LPARAM lp = MAKELPARAM(SB_PAGERIGHT, 0);
				polScrollV->SendMessage(WM_HSCROLL, lp, 0);
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////



bool CCSTable::MakeInplaceEdit(int ilItemID, int ipColumnno)
{
//rkr19042001 check of bmIPEwithoutALT-flag
	if (!bmIPEwithoutALT && !(GetKeyState(VK_MENU) & 0xff80))
		return false;

	if (bmIPEwithoutALT && (GetKeyState(VK_MENU) & 0xff80))
		return false;
//rkr19042001

	//char pclText[100]="";
	CString olEditText;


	if(ipColumnno >= 0)
		imCurrentColumn = ipColumnno;


	//imCurrentColumn = imCurrentColumn + imLeftDockingColumns;
	if(bmEditable == false)
		return false;
	if(imCurrentColumn < imLeftDockingColumns)
	{
		imCurrentColumn = imCurrentColumn + imLeftDockingColumns;
	}
	if(ilItemID < 0 || (ilItemID > (omLines.GetSize() - 1)))
		return false;
	if((omLines.GetSize() == 0) || (omLines[ilItemID].Columns.GetSize() == 0))
		return false;
		
	if(imCurrentColumn < 0 || (imCurrentColumn > (omLines[ilItemID].Columns.GetSize() - 1)))
		return false;


	if(!omLines[ilItemID].Columns[imCurrentColumn].blIsEditable)
		return false;

	
	CRect olRect = omLines[ilItemID].Columns[imCurrentColumn].rect;
	olEditText = omLines[ilItemID].Columns[imCurrentColumn].Text;
	
	if(pomIPEdit != NULL)
	{
		pomIPEdit->DestroyWindow();
		delete pomIPEdit;
		pomIPEdit = NULL;
	}
	imCurrentEditPosY = ilItemID; 
	imCurrentEditPosX = imCurrentColumn;// + imLeftDockingColumns;


	CCSTABLENOTIFY olNotify;

	olNotify.SourceTable = this;
	olNotify.Line = ilItemID;
	olNotify.Column = imCurrentEditPosX;
	if(imCurrentColumn >= 0 && omLines[ilItemID].Columns.GetSize() > imCurrentColumn)
	{
		olNotify.Data = (void*)&(omLines[ilItemID].Columns[imCurrentColumn]);
	}




	if(!bmMiniTable)
		SelectLine(imCurrentEditPosY);//RST

	olRect.left = olRect.left + imTableContentOffset;
	olRect.right = olRect.right + imTableContentOffset;
	CRect olItemRect;
	pomListBox->GetItemRect(ilItemID, &olItemRect);
	olRect.top = olItemRect.top;
	olRect.bottom = olItemRect.bottom;

	//olRect.InflateRect(5,4,5,2);



	pomIPEdit = new CCSEdit(olRect, pomListBox, this, omLines[ilItemID].Columns[imCurrentColumn].Font, true, &omLines[ilItemID].Columns[imCurrentColumn].EditAttrib); 
	pomIPEdit->SetWindowText(olEditText);
	pomIPEdit->SetSel(0,-1);
	IsInplaceEditMode = true;
	SetEditMenu(ilItemID, imCurrentColumn, pomIPEdit);

	pomParentWindow->SendMessage(WM_TABLE_IPEDIT, ilItemID, (LPARAM)&olNotify);
	
	return true;
}




LONG CCSTable::OnMoveInplaceRight(UINT wParam, LONG lParam)
{
	int ilCount;
	ilCount = omLines[imCurrentEditPosY].Columns.GetSize();
	if(imCurrentEditPosX >= 0 && imCurrentEditPosY >= 0)
	{
		if(imCurrentEditPosX+1 >= omLines[imCurrentEditPosY].Columns.GetSize())
		{
			if(imCurrentEditPosY+1 < omLines.GetSize()/*-1*/)
			{
				if(omLines[imCurrentEditPosY+1].Columns.GetSize() > 0)
				{
					CString olEditText;
					CRect olRect = omLines[imCurrentEditPosY+1].Columns[0+imLeftDockingColumns].rect;
					olEditText = omLines[imCurrentEditPosY+1].Columns[0+imLeftDockingColumns].Text;
					if(pomIPEdit != NULL)
					{
						if(bmMiniTable || pomIPEdit->GetStatus())
						{
							CString olTmp;
							CString olOldText;
							olOldText = omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].Text;
							pomIPEdit->GetWindowText(olTmp);
							omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].Text = olTmp;
							omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].IPStatus = pomIPEdit->GetStatus();
							omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].EditAttrib.IsChanged = pomIPEdit->IsChanged();
							if(olOldText != olTmp)
							{
								pomParentWindow->SendMessage(WM_TBL_INLINE_UPDATE, imCurrentEditPosY, (LPARAM)imCurrentEditPosX);
								omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].TextColor = pomIPEdit->GetTextColor();
							}
						}
					}
					//omLines[ilItemID].Column[imCurrentColumn].rect
					if(pomIPEdit != NULL)
					{
						pomIPEdit->DestroyWindow();
						delete pomIPEdit;
						pomIPEdit = NULL;
					}
					imCurrentEditPosY++; 
					imCurrentEditPosX = imLeftDockingColumns;//0;
					if(!bmMiniTable)
						SelectLine(imCurrentEditPosY);//RST
					CRect rect1 = omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].rect;
					int ilLastLine = pomListBox->GetLastVisibleItem();
					if(imCurrentEditPosY+1 > ilLastLine)
					{
						pomListBox->SendMessage(WM_VSCROLL, MAKELPARAM(SB_LINEDOWN, 0));
					}
					pomListBox->SendMessage(WM_HSCROLL, MAKELPARAM(SB_TOP, 0));


					if (omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].blIsEditable == false)
					{
						OnMoveInplaceRight(0, 0);
					}
					else
					{
						CRect olTmpRect;
						pomListBox->GetItemRect(imCurrentEditPosY, &olTmpRect);
						olRect.bottom = olTmpRect.bottom;
						olRect.top = olTmpRect.top;
						olRect.left = olRect.left + imTableContentOffset;
						olRect.right = olRect.right + imTableContentOffset;
						pomIPEdit = new CCSEdit(olRect, pomListBox, this, omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].Font, true, &omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].EditAttrib); 
						pomIPEdit->SetWindowText(olEditText);
						pomIPEdit->SetSel(0,-1);
						SetEditMenu(imCurrentEditPosY, imCurrentEditPosX, pomIPEdit);

						// Handle the Tab button down message. This handling is required in order to implement inline editing
						pomParentWindow->SendMessage(WM_EDIT_TAB_BUTTONDOWN, imCurrentEditPosY, (LPARAM)imCurrentEditPosX);  
						//CheckScrolling();
					}
				}
			}
		}
		else if(imCurrentEditPosX+1 < omLines[imCurrentEditPosY].Columns.GetSize())
		{
			CString olEditText;
			CRect olRect = omLines[imCurrentEditPosY].Columns[imCurrentEditPosX+1].rect;
			olEditText = omLines[imCurrentEditPosY].Columns[imCurrentEditPosX+1].Text;
			if(pomIPEdit != NULL)
			{
				if(bmMiniTable || pomIPEdit->GetStatus())
				{
					CString olTmp;
					CString olOldText;
					olOldText = omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].Text;
					pomIPEdit->GetWindowText(olTmp);
					omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].Text = olTmp;
					omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].IPStatus = pomIPEdit->GetStatus();
					omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].EditAttrib.IsChanged = pomIPEdit->IsChanged();
					if(olOldText != olTmp)
					{
						pomParentWindow->SendMessage(WM_TBL_INLINE_UPDATE, imCurrentEditPosY, (LPARAM)imCurrentEditPosX);
						omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].TextColor = pomIPEdit->GetTextColor();
					}
				}
			}
			//omLines[ilItemID].Column[imCurrentColumn].rect
			if(pomIPEdit != NULL)
			{
				pomIPEdit->DestroyWindow();
				delete pomIPEdit;
				pomIPEdit = NULL;
			}
			imCurrentEditPosX++;
			if(!bmMiniTable)
				SelectLine(imCurrentEditPosY);//RST
			CRect olTmpRect;
			pomListBox->GetClientRect(&olTmpRect);
			if(olRect.left + imTableContentOffset >= olTmpRect.right)
			{
				pomListBox->SendMessage(WM_HSCROLL, MAKELPARAM(SB_PAGERIGHT, 0));
				pomListBox->UpdateWindow();
				olRect = omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].rect;
			}
			olRect = omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].rect;
			pomListBox->GetItemRect(imCurrentEditPosY, &olTmpRect);
			olRect.left = olRect.left + imTableContentOffset;
			olRect.right = olRect.right + imTableContentOffset;
			olRect.bottom = olTmpRect.bottom;
			olRect.top = olTmpRect.top;

			if (omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].blIsEditable == false)
			{
				OnMoveInplaceRight(0, 0);
			}
			else
			{
				pomIPEdit = new CCSEdit(olRect, pomListBox, this, omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].Font, true, &omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].EditAttrib); 
				pomIPEdit->SetWindowText(olEditText);
				pomIPEdit->SetSel(0,-1);
				SetEditMenu(imCurrentEditPosY, imCurrentEditPosX, pomIPEdit);
				// Handle the Tab button down message. This handling is required in order to implement inline editing
				pomParentWindow->SendMessage(WM_EDIT_TAB_BUTTONDOWN, imCurrentEditPosY, (LPARAM)imCurrentEditPosX);  

				//CheckScrolling();
			}
		}
	}
	return 0L;
}

LONG CCSTable::OnMoveInplaceLeft(UINT wParam, LONG lParam)
{
	int ilCount;
	ilCount = omLines[imCurrentEditPosY].Columns.GetSize();
	if(imCurrentEditPosY == imLeftDockingColumns && imCurrentEditPosX == 0)
		return false;
	if(imCurrentEditPosX == imLeftDockingColumns && imCurrentEditPosY > 0)
	{
		int ilC2 = omLines[imCurrentEditPosY-1].Columns.GetSize();
		if(ilC2 > 0)
		{
			CString olEditText;
			if(pomIPEdit != NULL)
			{
				if(bmMiniTable || pomIPEdit->GetStatus())
				{
					CString olTmp;
					CString olOldText;
					olOldText = omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].Text;
					pomIPEdit->GetWindowText(olTmp);
					omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].Text = olTmp;
					omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].IPStatus = pomIPEdit->GetStatus();
					omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].EditAttrib.IsChanged = pomIPEdit->IsChanged();
					if(olOldText != olTmp)
					{
						pomParentWindow->SendMessage(WM_TBL_INLINE_UPDATE, imCurrentEditPosY, (LPARAM)imCurrentEditPosX);
						omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].TextColor = pomIPEdit->GetTextColor();
					}
				}
			}
			imCurrentEditPosX = ilC2-1;
			imCurrentEditPosY--;
			CRect olRect = omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].rect;
			olEditText = omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].Text;
			CRect rect1 = omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].rect;
			int ilFirstLine = pomListBox->GetFirstVisibleItem();
			if(imCurrentEditPosY < ilFirstLine)
			{
				pomListBox->SendMessage(WM_VSCROLL, MAKELPARAM(SB_LINEUP, 0));
				//pomListBox->UpdateWindow();
			}
			pomListBox->SendMessage(WM_HSCROLL, MAKELPARAM(SB_BOTTOM, 0));
			if(!bmMiniTable)
				SelectLine(imCurrentEditPosY);//RST
			CRect olTmpRect;
			pomListBox->GetItemRect(imCurrentEditPosY, &olTmpRect);
			olRect.bottom = olTmpRect.bottom;
			olRect.top = olTmpRect.top;
			olRect.left = olRect.left + imTableContentOffset;
			olRect.right = olRect.right + imTableContentOffset;
			//omLines[ilItemID].Column[imCurrentColumn].rect
			if(pomIPEdit != NULL)
			{
				pomIPEdit->DestroyWindow();
				delete pomIPEdit;
				pomIPEdit = NULL;
			}
			if (omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].blIsEditable == false)
			{
				OnMoveInplaceLeft(0, 0);
			}
			else
			{
				pomIPEdit = new CCSEdit(olRect, pomListBox, this, omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].Font, true, &omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].EditAttrib); 
				pomIPEdit->SetWindowText(olEditText);
				pomIPEdit->SetSel(0,-1);
				SetEditMenu(imCurrentEditPosY, imCurrentEditPosX, pomIPEdit);
				// Handle the Tab button down message. This handling is required in order to implement inline editing
				pomParentWindow->SendMessage(WM_EDIT_SHIFTTAB_BUTTONDOWN, imCurrentEditPosY, (LPARAM)imCurrentEditPosX);  
				//CheckScrolling();
			}
		}
	}
	else if(imCurrentEditPosX > imLeftDockingColumns)
	{
		CString olEditText;
		CRect olRect = omLines[imCurrentEditPosY].Columns[imCurrentEditPosX-1].rect;
		olEditText = omLines[imCurrentEditPosY].Columns[imCurrentEditPosX-1].Text;
		//omLines[ilItemID].Column[imCurrentColumn].rect
		if(pomIPEdit != NULL)
		{
			if(bmMiniTable || pomIPEdit->GetStatus())
			{
				CString olTmp;
				CString olOldText;
				olOldText = omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].Text;
				omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].IPStatus = pomIPEdit->GetStatus();
				omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].TextColor = pomIPEdit->GetTextColor();
				omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].EditAttrib.IsChanged = pomIPEdit->IsChanged();
				pomIPEdit->GetWindowText(olTmp);
				omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].Text = olTmp;
				if(olOldText != olTmp)
				{
					pomParentWindow->SendMessage(WM_TBL_INLINE_UPDATE, imCurrentEditPosY, (LPARAM)imCurrentEditPosX);
					omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].TextColor = pomIPEdit->GetTextColor();
				}
			}
		}
		if(pomIPEdit != NULL)
		{
			pomIPEdit->DestroyWindow();
			delete pomIPEdit;
			pomIPEdit = NULL;
		}
		imCurrentEditPosX--;
		if(!bmMiniTable)
			SelectLine(imCurrentEditPosY);//RST
		CRect olTmpRect;
		pomListBox->GetClientRect(&olTmpRect);
		if(olRect.right + imTableContentOffset <= olTmpRect.left)
		{
			pomListBox->SendMessage(WM_HSCROLL, MAKELPARAM(SB_PAGELEFT, 0));
			pomListBox->UpdateWindow();
			olRect = omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].rect;
		}
		olRect = omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].rect;
		pomListBox->GetItemRect(imCurrentEditPosY, &olTmpRect);
		olRect.left = olRect.left + imTableContentOffset;
		olRect.right = olRect.right + imTableContentOffset;
		olRect.bottom = olTmpRect.bottom;
		olRect.top = olTmpRect.top;
//		olRect.left = olRect.left + imTableContentOffset;
//		olRect.right = olRect.right + imTableContentOffset;

		if (omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].blIsEditable == false)
		{
			OnMoveInplaceLeft(0, 0);
		}
		else
		{
			pomIPEdit = new CCSEdit(olRect, pomListBox, this, omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].Font, true, &omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].EditAttrib); 
			pomIPEdit->SetWindowText(olEditText);
			pomIPEdit->SetSel(0,-1);
			SetEditMenu(imCurrentEditPosY, imCurrentEditPosX, pomIPEdit);
			// Handle the Tab button down message. This handling is required in order to implement inline editing
			pomParentWindow->SendMessage(WM_EDIT_SHIFTTAB_BUTTONDOWN, imCurrentEditPosY, (LPARAM)imCurrentEditPosX);  

			//CheckScrolling();
		}
	}
	return 0L;
}

LONG CCSTable::OnMoveInplaceUp(UINT wParam, LONG lParam)
{
	if(imCurrentEditPosY <= 0)
		return false;

	int ilCount = omLines[imCurrentEditPosY-1].Columns.GetSize()-1;
	if(imCurrentEditPosX <= ilCount )
	{
		CString olEditText;
		if(pomIPEdit != NULL)
		{
			if(bmMiniTable || pomIPEdit->GetStatus())
			{
				CString olTmp;
				CString olOldText;
				olOldText = omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].Text;
				pomIPEdit->GetWindowText(olTmp);
				omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].Text = olTmp;
				omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].IPStatus = pomIPEdit->GetStatus();
				omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].EditAttrib.IsChanged = pomIPEdit->IsChanged();
				if(olOldText != olTmp)
				{
					pomParentWindow->SendMessage(WM_TBL_INLINE_UPDATE, imCurrentEditPosY, (LPARAM)imCurrentEditPosX);
					omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].TextColor = pomIPEdit->GetTextColor();
				}
			}
		}
		imCurrentEditPosY--;

		CRect rect1 = omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].rect;
		int ilFirstLine = pomListBox->GetFirstVisibleItem();
		if(imCurrentEditPosY < ilFirstLine)
		{
			pomListBox->SendMessage(WM_VSCROLL, MAKELPARAM(SB_LINEUP, 0));
		}
		if(!bmMiniTable)
			SelectLine(imCurrentEditPosY);//RST
		CRect olRect = omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].rect;
		olEditText = omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].Text;
		//omLines[ilItemID].Column[imCurrentColumn].rect
		if(pomIPEdit != NULL)
		{
			pomIPEdit->DestroyWindow();
			delete pomIPEdit;
			pomIPEdit = NULL;
		}
		CRect olTmpRect;
		pomListBox->GetItemRect(imCurrentEditPosY, &olTmpRect);
		olRect.bottom = olTmpRect.bottom;
		olRect.top = olTmpRect.top;
		olRect.left = olRect.left + imTableContentOffset;
		olRect.right = olRect.right + imTableContentOffset;

		pomIPEdit = new CCSEdit(olRect, pomListBox, this, omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].Font, true, &omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].EditAttrib); 
		pomIPEdit->SetWindowText(olEditText);
		pomIPEdit->SetSel(0,-1);
		SetEditMenu(imCurrentEditPosY, imCurrentEditPosX, pomIPEdit);

		//pomListBox->UpdateWindow();
		//UpdateWindow( );

		//CheckScrolling();
	}
	return 0L;
}



bool CCSTable::GetCellStatus(int ipLineNo, int ipColumnNo)
{
    if (!(0 <= ipLineNo && ipLineNo < omLines.GetSize()))    // valid line number?
        return false;

    if (!(0 <= ipColumnNo && ipColumnNo < omLines[ipLineNo].Columns.GetSize()))    // valid column number?
        return false;
	
	return omLines[ipLineNo].Columns[ipColumnNo].IPStatus;
}




LONG CCSTable::OnMoveInplaceDown(UINT wParam, LONG lParam)
{
	if(imCurrentEditPosY >= omLines.GetSize()-1)
	{
		pomListBox->SendMessage(WM_VSCROLL, MAKELPARAM(SB_BOTTOM, 0));
		return false;
	}

	int ilFirstLine, ilLastLine;
	int ilCount = omLines[imCurrentEditPosY+1].Columns.GetSize()-1;
	if(imCurrentEditPosX <= ilCount )
	{
		CString olEditText;
		if(pomIPEdit != NULL)
		{
			if(bmMiniTable || pomIPEdit->GetStatus())
			{
				CString olTmp;
				CString olOldText;
				olOldText = omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].Text;
				pomIPEdit->GetWindowText(olTmp);
				omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].Text = olTmp;
				omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].IPStatus = pomIPEdit->GetStatus();
				omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].EditAttrib.IsChanged = pomIPEdit->IsChanged();
				if(olOldText != olTmp)
				{
					pomParentWindow->SendMessage(WM_TBL_INLINE_UPDATE, imCurrentEditPosY, (LPARAM)imCurrentEditPosX);
					omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].TextColor = pomIPEdit->GetTextColor();
				}
			}
		}

		imCurrentEditPosY++;
		CRect rect1 = omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].rect;
		ilFirstLine = pomListBox->GetFirstVisibleItem();
		ilLastLine = pomListBox->GetLastVisibleItem();
		if(imCurrentEditPosY+1 > ilLastLine)
		{
			pomListBox->SendMessage(WM_VSCROLL, MAKELPARAM(SB_LINEDOWN, 0));
			//pomListBox->UpdateWindow();
		}
		//if(imCurrentEditPosY == ilLastLine)
		if(!bmMiniTable)
			SelectLine(imCurrentEditPosY);//RST
		CRect olRect = omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].rect;
		olEditText = omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].Text;
		//omLines[ilItemID].Column[imCurrentColumn].rect
		if(pomIPEdit != NULL)
		{
			pomIPEdit->DestroyWindow();
			delete pomIPEdit;
			pomIPEdit = NULL;
		}

		CRect olTmpRect;
		pomListBox->GetItemRect(imCurrentEditPosY, &olTmpRect);
		olRect.bottom = olTmpRect.bottom;
		olRect.top = olTmpRect.top;
		olRect.left = olRect.left + imTableContentOffset;
		olRect.right = olRect.right + imTableContentOffset;

		pomIPEdit = new CCSEdit(olRect, pomListBox, this, omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].Font, true, &omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].EditAttrib); 
		
		pomIPEdit->SetWindowText(olEditText);
		pomIPEdit->SetSel(0,-1);
		SetEditMenu(imCurrentEditPosY, imCurrentEditPosX, pomIPEdit);

		//pomListBox->UpdateWindow();
		//UpdateWindow( );
		// prepare perhalps scrolling
		//CheckScrolling();
	}
	return 0L;
}

LONG CCSTable::OnEndInplaceEditing(UINT wParam, LONG lParam)
{
	EndInplaceEditing();
	return 0L;
}


LONG CCSTable::OnInplaceReturn(UINT wParam, LONG lParam)
{
	EndInplaceEditing();
	
	if(bmMiniTable)
	{
		if(pomParentWindow != NULL)
			pomParentWindow->SendMessage(WM_KEYDOWN,13, 28);
	}

	return 0L;
}


void CCSTable::EndInplaceEditing() 
{
	if(pomIPEdit != NULL)
	{
		if(bmMiniTable || pomIPEdit->GetStatus())
		{
			CString olTmp;
			CString olOldText;
			if(imCurrentEditPosY >= 0 &&  imCurrentEditPosX >= 0)
			{
				olOldText = omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].Text;
				pomIPEdit->GetWindowText(olTmp);
				omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].Text = olTmp;
				omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].IPStatus = pomIPEdit->GetStatus();
				omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].EditAttrib.IsChanged = pomIPEdit->IsChanged();
				if(olOldText != olTmp)
				{
					pomParentWindow->SendMessage(WM_TBL_INLINE_UPDATE, imCurrentEditPosY, (LPARAM)imCurrentEditPosX);
					omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].TextColor = pomIPEdit->GetTextColor();
				}
			}
		}
		pomIPEdit->DestroyWindow();
		delete pomIPEdit;
		pomIPEdit = NULL;
	}
	IsInplaceEditMode = false;
}

LONG CCSTable::OnEditKillfocus( UINT wParam, LPARAM lParam)
{

	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*)lParam;
	CCSIPEDITNOTIFY *prlIPNotify = new CCSIPEDITNOTIFY;

	prlIPNotify->SourceTable = this;
	prlIPNotify->SourceControl = prlNotify->SourceControl;
	prlIPNotify->Text = prlNotify->Text;
	prlIPNotify->Status = prlNotify->Status;
	prlIPNotify->IsChanged = prlNotify->IsChanged;
	prlIPNotify->UserStatus = prlNotify->UserStatus;
	prlIPNotify->Line = imCurrentEditPosY;
	prlIPNotify->Column = imCurrentEditPosX;
	prlIPNotify->IsEscapeKeyPressed =  IsEscapePressed;
	pomParentWindow->SendMessage(WM_TABLE_IPEDIT_KILLFOCUS, wParam, (LPARAM)prlIPNotify);
	prlNotify->Status = prlIPNotify->Status;
	prlNotify->UserStatus = prlIPNotify->UserStatus;
	prlNotify->Text = prlIPNotify->Text;

	delete prlIPNotify;
	return 0L;
}


LONG CCSTable::OnEditChanged( UINT wParam, LPARAM lParam)
{
	pomParentWindow->SendMessage(WM_EDIT_CHANGED, wParam, lParam);
	return 0L;
}


void CCSTable::KillFocus()
{
	if(bmMiniTable)
		EndInplaceEditing();
}



void CCSTable::SetIPValue(int ipLineno, int ipColumnno, CString olValue, COLORREF opColor, bool bpIPStatus)
{
	 if(ipLineno <= omLines.GetSize()-1)
	 {
		 if(ipColumnno <= omLines[ipLineno].Columns.GetSize())
		 {
			omLines[ipLineno].Columns[ipColumnno].TextColor = opColor;
			omLines[ipLineno].Columns[ipColumnno].Text		= olValue;
			omLines[ipLineno].Columns[ipColumnno].IPStatus	= bpIPStatus;
			omLines[ipLineno].Columns[ipColumnno].EditAttrib.IsChanged = true;			
			if (m_hWnd != NULL)     // list box is exist?
			{
				CRect rcItem;
				if (pomListBox->GetItemRect(ipLineno, rcItem) == LB_ERR 
					// ||  pomListBox->SetCaretIndex(ipLineNo) == LB_ERR
					)
				  {
						//RC(false);
						return;
				  }
				if(pomParentWindow != NULL)
					pomParentWindow->SendMessage(WM_EDIT_CHANGED, 0, 0);
				pomListBox->InvalidateRect(rcItem);
			}
		 }
	 }
}





////////////////////////////////////////////////////////////////////////////////////////////////
// RST 


bool CCSTable::DeleteTextLine(int ipLineNo)
{
	
	if(	ipLineNo == imCurrentEditPosY)
		EndInplaceEditing();


	if(	ipLineNo < imCurrentEditPosY)
		imCurrentEditPosY--;



    if (ipLineNo == -1 && m_hWnd != NULL)   // use default line number?
        ipLineNo = pomListBox->GetCaretIndex();

    if (!(0 <= ipLineNo && ipLineNo < omLines.GetSize()))    // valid line number?
    {
        //RC(false);
        return false;
    }

    //omData.RemoveAt(ipLineNo);
	int ilCount = omLines[ipLineNo].Columns.GetSize();
	while(ilCount-- > 0)
	{
		omLines[ipLineNo].Columns.DeleteAt(ilCount);
	}
	omLines.DeleteAt(ipLineNo);

	omDataPtr.RemoveAt(ipLineNo);
	omColor.RemoveAt(ipLineNo);
	omBkColor.RemoveAt(ipLineNo);
	omDragEnabled.RemoveAt(ipLineNo);
	omSeparatorType.RemoveAt(ipLineNo);
	omFontPtr.RemoveAt(ipLineNo);
    if (m_hWnd != NULL)     // list box is exist?
    {
        if (pomListBox->DeleteString(ipLineNo) == LB_ERR)
        {
            //RC(false);
            return false;
        }
    }

    //RC(true);
    InvalidateRect(NULL, false);

	MoveIPEdit();

    return true;
}


bool CCSTable::AddTextLine(CCSPtrArray <TABLE_COLUMN> &opLine, void *pvpData)
{

	TABLE_LINE rlLine;

    omDataPtr.Add(pvpData);
	omColor.Add(lmTextColor);
	omBkColor.Add(lmTextBkColor);
	omDragEnabled.Add(true);
	omSeparatorType.Add(ST_NORMAL);
	omFontPtr.Add(pomTextFont);
	int ilCount = opLine.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		TABLE_COLUMN rlCol = opLine[i];
		rlLine.Columns.NewAt(rlLine.Columns.GetSize(), opLine[i]);
	}
	rlLine.Lineno = omLines.GetSize();//-1;
	omLines.NewAt(omLines.GetSize(), rlLine);
    if (m_hWnd != NULL)     // list box is exist?
    {
        if (pomListBox->AddString("") == LB_ERR)
        {
            //RC(false);
            return false;
        }
    }

	MoveIPEdit();
                   
    //RC(true);
    return true;
}


bool CCSTable::ChangeTextLine(int ipLineNo, CCSPtrArray <TABLE_COLUMN> *popLine, void *pvpData)
{

    if (ipLineNo == -1 && m_hWnd != NULL)   // use default line number?
        ipLineNo = pomListBox->GetCaretIndex();

    if (!(0 <= ipLineNo && ipLineNo < omLines.GetSize()))    // valid line number?
    {
        //RC(false);
        return false;
    }


	if((pomIPEdit != NULL) && (ipLineNo == imCurrentEditPosY))
	{
		pomIPEdit->SetWindowText((*popLine)[imCurrentEditPosX].Text);
		pomIPEdit->SetSel(0,-1);
	}


    //omData.SetAt(ipLineNo, opText);
	TABLE_LINE *prlLine = new TABLE_LINE;
	TABLE_LINE *prlLineOld = &omLines[ipLineNo];
	int ilCount = popLine->GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		TABLE_COLUMN rlCol = (*popLine)[i];
		prlLine->Columns.NewAt(prlLine->Columns.GetSize(), rlCol);

		prlLine->Columns[i].EditAttrib = prlLineOld->Columns[i].EditAttrib;
		prlLine->Columns[i].EditAttrib.IsChanged = false;

	}
	//rlLine.Lineno = omLines.GetSize();//-1;
	omLines.SetAt(ipLineNo, prlLine);
	//rlLine.Clo
	prlLineOld->Columns.DeleteAll();
	
	delete prlLineOld;

	//omLines[ipLineNo].Columns.SetAt(0, popLine);
    omDataPtr.SetAt(ipLineNo, pvpData);
    if (m_hWnd != NULL)     // list box is exist?
    {
		CRect rcItem;
		if (pomListBox->GetItemRect(ipLineNo, rcItem) == LB_ERR 
			// ||  pomListBox->SetCaretIndex(ipLineNo) == LB_ERR
			)
        {
            //RC(false);
            return false;
        }
		pomListBox->InvalidateRect(rcItem);
    }

	if(pomIPEdit != NULL)
		pomIPEdit->SetWindowPos(&wndTop, 0,0,0,0, SWP_NOSIZE | SWP_NOMOVE | SWP_SHOWWINDOW);

    //RC(true);
    return true;
}


bool CCSTable::InsertTextLine(int ipLineNo, CCSPtrArray <TABLE_COLUMN> &popLine, void *pvpData)
{
    if ((ipLineNo == -1) || (ipLineNo >= omLines.GetSize()))    // add string to the end of the list?
        return AddTextLine(popLine,pvpData);


	if(	ipLineNo <= imCurrentEditPosY)
		imCurrentEditPosY++;


//    omData.InsertAt(ipLineNo, opText);
    omDataPtr.InsertAt(ipLineNo, pvpData);
	omColor.InsertAt(ipLineNo, lmTextColor);
	omBkColor.InsertAt(ipLineNo, lmTextBkColor);
	omDragEnabled.InsertAt(ipLineNo, true);
	omSeparatorType.InsertAt(ipLineNo, (WORD)ST_NORMAL);
	omFontPtr.InsertAt(ipLineNo, pomTextFont);
	//omLines.InsertAt(ipLineNo, &popLine);

	TABLE_LINE rlLine;

	int ilCount = popLine.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		TABLE_COLUMN rlCol = popLine[i];
		rlLine.Columns.NewAt(i, popLine[i]);
	}
	//rlLine.Lineno = omLines.GetSize();//-1;
	ilCount = omLines.GetSize();
	for(i = ipLineNo; i < ilCount; i++)
	{
		omLines[i].Lineno = i;
	}
	omLines.NewAt(ipLineNo, rlLine);
    if (m_hWnd != NULL)     // list box is exist?
    {
		if (pomListBox->InsertString(ipLineNo, "") == LB_ERR)
		{
			//RC(false);
			return false;
		}
    }
	MoveIPEdit();

    //RC(true);
    return true;
}




bool CCSTable::MoveIPEdit()
{
	if(pomIPEdit == NULL)
		return false;

	if(!((imCurrentEditPosY <=  pomListBox->GetLastVisibleItem()) && (imCurrentEditPosY >=  pomListBox->GetFirstVisibleItem())))
	{
		pomListBox->SetTopIndex(imCurrentEditPosY);
	}

	
	CRect olRect = omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].rect;

	olRect.left = olRect.left + imTableContentOffset;
	olRect.right = olRect.right + imTableContentOffset;
	CRect olItemRect;
	pomListBox->GetItemRect(imCurrentEditPosY, &olItemRect);
	olRect.top = olItemRect.top;
	olRect.bottom = olItemRect.bottom;

	pomIPEdit->MoveWindow(olRect);
	return true;
}


LRESULT CCSTable::OnMenuSelect2( UINT wParam, LPARAM lParam )
{

	OnMenuSelect(wParam);
	return 0L;

}


void CCSTable::OnMenuSelect( UINT nID )
{
	CCSTABLENOTIFY olNotify;

	olNotify.SourceTable = this;
	olNotify.Line = imCurrentEditPosY;
	olNotify.Column = imCurrentEditPosX;
	//if(imCurrentColumn >= 0 && omLines[ilItemID].Columns.GetSize() > imCurrentColumn)
	{
		olNotify.Data = (void*)&(omLines[imCurrentEditPosY].Columns[imCurrentEditPosX]);
	}

	EndInplaceEditing();
	
	pomParentWindow->SendMessage(WM_TABLE_MENU_SELECT, nID, (LPARAM)&olNotify);

	return;
}




void CCSTable::AddMenuItem(int ipLine, int ipColumn, UINT ipFlags, UINT ipID, CString opText)
{
	MENUITEM *prlItem = NULL;
	TABLE_MENU *prlTableMenu = NULL;
	int ilCount = omMenuItems.GetSize();


	for(int i = 0; i < ilCount; i++)
	{
		if((omMenuItems[i].Line == ipLine) && (omMenuItems[i].Column == ipColumn))
		{
			prlTableMenu = &omMenuItems[i];
			break;
		}
	}

	if(prlTableMenu == NULL)
	{
		prlTableMenu = new TABLE_MENU;
		prlTableMenu->Line = ipLine;
		prlTableMenu->Column = ipColumn;
		omMenuItems.Add(prlTableMenu);
	}
	
	prlItem = new MENUITEM;

	prlTableMenu->Items.Add(prlItem);

	prlItem->ID = ipID;
	prlItem->Text = opText;
	prlItem->Flags = ipFlags;


}



void CCSTable::SetEditMenu(int ipLine, int ipColumn, CCSEdit *pomEdit)
{
	MENUITEM *prlItem = NULL;
	TABLE_MENU *prlTableMenu = NULL;
	int ilCount = omMenuItems.GetSize();
	
	for(int i = 0; i < ilCount; i++)
	{
		if((omMenuItems[i].Line == ipLine) || (omMenuItems[i].Column == ipColumn))
		{

			prlTableMenu = &omMenuItems[i];
			
			for(int j = 0; j < prlTableMenu->Items.GetSize(); j++)
			{
				prlItem = &prlTableMenu->Items[j];
				pomEdit->AddMenuItem(prlItem->Flags, prlItem->ID, prlItem->Text);
			}
		}
	}
}


LONG CCSTable::OnTableRButtonDown( UINT wParam, LPARAM lParam)
{
	bool blFound = false;
 
	CMenu menu;
	menu.CreatePopupMenu();
	for (int i = menu.GetMenuItemCount(); i > 0; i--)
		 menu.RemoveMenu(i, MF_BYPOSITION);


	CCSTABLENOTIFY *prlNotify = (CCSTABLENOTIFY*)lParam;
	
	CPoint point = prlNotify->Point;


	imCurrentEditPosY = prlNotify->Line ;
	imCurrentEditPosX = prlNotify->Column;

	MENUITEM *prlItem = NULL;
	TABLE_MENU *prlTableMenu = NULL;
	int ilCount = omMenuItems.GetSize();
	
	for( i = 0; i < ilCount; i++)
	{
		if((omMenuItems[i].Line == prlNotify->Line) || (omMenuItems[i].Column == prlNotify->Column))
		{

			if(omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].blIsEditable)
			{
				prlTableMenu = &omMenuItems[i];
				
				for(int j = 0; j < prlTableMenu->Items.GetSize(); j++)
				{
					prlItem = &prlTableMenu->Items[j];
					menu.AppendMenu(prlItem->Flags, prlItem->ID , prlItem->Text);	
					blFound = true;
				}
			}
		}
	}

	if(blFound)
	{
		//CPoint olPoint(LOWORD(lParam),HIWORD(lParam));
		ClientToScreen(&point);
		menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);
	}

	return 0L;

}

//send message if return-key is pressed
BOOL CCSTable::PreTranslateMessage(MSG* pMsg)
{
	if (!pMsg)
		return FALSE;

	if (IsInplaceEditMode)
		return FALSE;

	if( pMsg->message == WM_KEYDOWN )
	{
		if(/*pMsg->wParam == VK_ESCAPE || */pMsg->wParam == VK_RETURN)
		{
			int itemID = pomListBox->GetCaretIndex();

			if(itemID < 0 || itemID > omLines.GetSize())
				return FALSE;

			CCSTABLENOTIFY olNotify;
			olNotify.SourceTable = this;
			olNotify.Line = itemID;
			olNotify.Column = -1;
			olNotify.Data = (void*) NULL;

			if (pomParentWindow)
				pomParentWindow->SendMessage(WM_TABLE_RETURN_PRESSED, itemID, (LPARAM)&olNotify);

			return TRUE; // DO NOT process further
		}
	}

	return FALSE;
}


// For Selecting a Specified Cell in the CCSTable.
// Selects the Specified Cell and assgins the strData to that Cell.
void CCSTable::SelectCell(int iLineNo, int iCellNo, CString strData)
{
	imCurrentEditPosY = iLineNo;
	imCurrentEditPosX = iCellNo;

	CRect olRect = omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].rect;
	olRect.left = olRect.left + imTableContentOffset;
	olRect.right = olRect.right + imTableContentOffset;
	CRect olItemRect;
	pomListBox->GetItemRect(imCurrentEditPosY, &olItemRect);
	olRect.top = olItemRect.top;
	olRect.bottom = olItemRect.bottom;

	SelectLine(iLineNo); // Select Line
	pomIPEdit = new CCSEdit(olRect, pomListBox, this, omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].Font, true, &omLines[imCurrentEditPosY].Columns[imCurrentEditPosX].EditAttrib); 
	pomIPEdit->SetWindowText(strData);
	pomIPEdit->SetSel(0,-1);
	SetEditMenu(imCurrentEditPosY, imCurrentEditPosX, pomIPEdit);

}


BEGIN_MESSAGE_MAP(CCSHeaderTable,CWnd)
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_MOUSEMOVE()	
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()	
	ON_MESSAGE(WM_TABLE_DRAGBEGIN,OnDragBegin)
	ON_MESSAGE(WM_DROP,OnDrop)	
	ON_MESSAGE(WM_DRAGOVER, OnDragOver)
END_MESSAGE_MAP()

void CCSHeaderTable::OnLButtonDown(UINT nFlags, CPoint point)
{	
	int intCurrentColumn = DetectCurrentColumn(point);
	if(intCurrentColumn == -1)
		return;
	m_intSourceColumn = intCurrentColumn;

	CPoint olPoint;
	::GetCursorPos(&olPoint);
}

void CCSHeaderTable::OnLButtonUp(UINT nFlags, CPoint point)
{	
	int intCurrentColumn;
	if((intCurrentColumn = DetectCurrentColumn(point)) == -1)
		return;
	m_intDestColumn = intCurrentColumn;

	CPoint olPoint;
	::GetCursorPos(&olPoint);
	ScreenToClient(&olPoint);		
	if((m_intSourceColumn == m_intDestColumn) && (m_intSourceColumn != -1))
	{		
		pomTableWindow->pomParentWindow->SendMessage(WM_TABLE_SORT);
	}
	ResetColumnValues();
}

void CCSHeaderTable::OnPaint()
{
	InvalidateRect(NULL);
	CWnd::OnPaint();
}

void CCSHeaderTable::OnMouseMove(UINT nFlags, CPoint point)
{
	//Drag a column only in the event of mouse move and Source Column.
	if((MK_LBUTTON == nFlags) && (pomTableWindow->bmEnableHeaderDragDrop == true) && (m_intSourceColumn != -1))
	{
		SendMessage(WM_TABLE_DRAGBEGIN);
	}

	CWnd::OnMouseMove(nFlags,point);
	return;
}

int CCSHeaderTable::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CWnd::OnCreate(lpCreateStruct);
	CRect olRect;
	olRect.bottom = lpCreateStruct->cy;
	olRect.top = lpCreateStruct->y;
	olRect.left = lpCreateStruct->x;
	olRect.right = lpCreateStruct->cx;
	MoveWindow(&olRect);
	omDragDropTarget.RegisterTarget(this,this);
	return 0;
}

//Getting the current column from the point of LButtonDown.
int CCSHeaderTable::DetectCurrentColumn(CPoint point)
{
	int x = point.x + abs(pomTableWindow->imTableContentOffset);	
	for (int ilLc = 0; ilLc < pomTableWindow->omLBorder.GetSize(); ilLc++)
	{
		long x1, x2;
		x1 = pomTableWindow->omLBorder[ilLc];
		x2 = pomTableWindow->omRBorder[ilLc];
		if ((int)pomTableWindow->omLBorder[ilLc] < x && x < (int)pomTableWindow->omRBorder[ilLc])
		{			
			return pomTableWindow->imCurrentColumn = ilLc;
		}
	}	
	return -1;
}

LONG CCSHeaderTable::OnDragBegin(UINT wParam, LONG lParam)
{    
	omDragDropTarget.CreateDWordData(CCSTBL_HEADER_COLUMN, 1);	
	omDragDropTarget.AddDWord(m_intSourceColumn);
	omDragDropTarget.BeginDrag();
	return 0L;
}

LONG CCSHeaderTable::OnDragOver(WPARAM wParam, LPARAM lParam)
{
	if(omDragDropTarget.GetDataClass() != CCSTBL_HEADER_COLUMN)
	{
		return -1L;
	}
	return 0L;
} 

//This function is called when a selected header column is dragged and dropped over other/same column.
LONG CCSHeaderTable::OnDrop(UINT wParam, LPARAM lParam)
{	
	// remove the focus of pomIPEdit
	if(pomTableWindow->pomIPEdit != NULL)
	{
		pomTableWindow->pomIPEdit->DestroyWindow();
		delete pomTableWindow->pomIPEdit;
		pomTableWindow->pomIPEdit = NULL;
	}

	//The below line is to ensure that the dropped object is of type TABLE_HEADER_COLUMN.
    //Or else no operation will happen. 
	if(omDragDropTarget.GetDataClass() != CCSTBL_HEADER_COLUMN)
	{
		ResetColumnValues();
		return 0L;
	}

	int i = m_intSourceColumn;

	CPoint olPoint;
	::GetCursorPos(&olPoint);
	ScreenToClient(&olPoint);
	m_intSourceColumn = omDragDropTarget.GetDataDWord(0);
	m_intDestColumn  = DetectCurrentColumn(olPoint);
	if((m_intDestColumn == -1 || m_intSourceColumn == -1) || (m_intSourceColumn == m_intDestColumn))
	{
		ResetColumnValues();
		return 0L;
	}

	TABLE_HEADER_COLUMN olSourceHeaderColumn = pomTableWindow->omHeaderDataArray[m_intSourceColumn];
	TABLE_HEADER_COLUMN olDestHeaderColumn = pomTableWindow->omHeaderDataArray[m_intDestColumn];
	TABLE_HEADER_COLUMN olNextHeaderColumn;
	
	CString olSourceField = pomTableWindow->omTableFieldArray[m_intSourceColumn];
	CString olDestField = pomTableWindow->omTableFieldArray[m_intDestColumn];
	CString olNextField;


	TABLE_COLUMN olSourceColumn;
	TABLE_COLUMN olDestColumn;
	TABLE_COLUMN olNextColumn;

	if(m_intSourceColumn > m_intDestColumn)
	{
		pomTableWindow->omHeaderDataArray[m_intDestColumn] = olSourceHeaderColumn;
		pomTableWindow->omTableFieldArray[m_intDestColumn] = olSourceField;

		for(int j = m_intDestColumn+1;j<=m_intSourceColumn;j++)
		{
			olNextHeaderColumn = pomTableWindow->omHeaderDataArray[j];
			pomTableWindow->omHeaderDataArray[j] = olDestHeaderColumn;
			olDestHeaderColumn = olNextHeaderColumn;

			olNextField = pomTableWindow->omTableFieldArray[j];
			pomTableWindow->omTableFieldArray[j] = olDestField;
			olDestField = olNextField;
		}	

		for(int k =0;k<pomTableWindow->omLines.GetSize();k++)
		{
			olSourceColumn = pomTableWindow->omLines[k].Columns[m_intSourceColumn];
			olDestColumn = pomTableWindow->omLines[k].Columns[m_intDestColumn];
			
			pomTableWindow->omLines[k].Columns[m_intDestColumn] = olSourceColumn;
			for(int l = m_intDestColumn+1;l<=m_intSourceColumn;l++)
			{
				olNextColumn = pomTableWindow->omLines[k].Columns[l];
				pomTableWindow->omLines[k].Columns[l]= olDestColumn;
				olDestColumn = olNextColumn;
			}
		}	
	}
	else
	{
		//Header Changing		
		for(int j = m_intSourceColumn+1;j<=m_intDestColumn;j++)
		{
			olNextHeaderColumn = pomTableWindow->omHeaderDataArray[j];
			pomTableWindow->omHeaderDataArray[j-1] = olNextHeaderColumn;
			olDestHeaderColumn = olNextHeaderColumn;

			olNextField = pomTableWindow->omTableFieldArray[j];
			pomTableWindow->omTableFieldArray[j-1] = olNextField;
			olDestField = olNextField;
		}

		pomTableWindow->omHeaderDataArray[m_intDestColumn] = olSourceHeaderColumn;
		pomTableWindow->omTableFieldArray[m_intDestColumn] = olSourceField;

		///Data Changing 
		for(int k =0;k<pomTableWindow->omLines.GetSize();k++)
		{
			olSourceColumn = pomTableWindow->omLines[k].Columns[m_intSourceColumn];
			olDestColumn = pomTableWindow->omLines[k].Columns[m_intDestColumn];
			
			for(int l = m_intSourceColumn+1;l<=m_intDestColumn;l++)
			{
				olNextColumn = pomTableWindow->omLines[k].Columns[l];
				pomTableWindow->omLines[k].Columns[l-1]= olNextColumn;
				olDestColumn = olNextColumn;
			}
			
			pomTableWindow->omLines[k].Columns[m_intDestColumn] = olSourceColumn;
		}
	}

	pomTableWindow->DisplayTable();
	ResetColumnValues();
	return 0L;
}  

