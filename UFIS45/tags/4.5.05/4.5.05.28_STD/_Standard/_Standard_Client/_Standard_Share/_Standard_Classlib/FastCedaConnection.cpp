// FastCedaConnection.cpp : implementation file
//

#include "stdafx.h"
#include "FastCedaConnection.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FastCedaConnection


int FastCedaConnection::ExtractTextLineFast(CStringArray &opItems, char *pcpLineText, char *pcpSepa)
{
	int ilSepaLen = strlen(pcpSepa);
	int ilCount = 0;
	char *currPtr;
	currPtr = strstr(pcpLineText, pcpSepa);
	while (currPtr != NULL)
	{
		*currPtr = '\0';
		opItems.Add(pcpLineText);
		ilCount++;
		pcpLineText = currPtr + ilSepaLen;
		currPtr		= strstr(pcpLineText, pcpSepa);
	}
	if(strcmp(pcpLineText, "") != 0)
	{
		opItems.Add(pcpLineText);
		ilCount++;
	}

	return ilCount;
}

FastCedaConnection::FastCedaConnection()
{
	pomSocket = NULL;
	bmIsConnected = false;
	pcmDataBuffer = NULL;
	omSepa = CString("\n");
	imSendTimeout = 240;
	imReceiveTimeout = 240;

	pomCommHandler = NULL;

	CString olConfigFileName;
	if (getenv("CEDA") == NULL)
	{
		olConfigFileName = "C:\\UFIS\\SYSTEM\\CEDA.INI";
	}
	else
	{
		olConfigFileName = getenv("CEDA");
	}

	lmPacketSize = ::GetPrivateProfileInt("GLOBAL","PACKETSIZE",0,olConfigFileName);

	pfmCallback		 = NULL;
	pomControl		 = NULL;
	canKeepAlive = false;
}

void FastCedaConnection::AttachComHandler(CCSCedaCom *popCommHandler)
{
	pomCommHandler = popCommHandler;
}

FastCedaConnection::~FastCedaConnection()
{
	ClearDataBuffer();
	if(pomSocket != NULL)
	{
		delete pomSocket;
		bmIsConnected = false;
	}
}

//---------------------------------------------------------------
//connects to the server in case of empty string 
// the socket connects to the server specified in omServer
// returns 0 = SUCCESS or -1 = FAILURE
//---------------------------------------------------------------
int FastCedaConnection::ConnectToCeda(CString opConnectionstring)
{
	int blRet = 0;
	char *pclTmpPtr;
	char OrigServer[512]="",
		 Svr1[512]="",
		 Svr2[512]="";
	bool blHas2Servers = false;

	strcpy(OrigServer, opConnectionstring);
	pclTmpPtr = strchr(OrigServer, '/');
	if(pclTmpPtr != NULL)
	{
		*pclTmpPtr = '\0';
		strcpy(Svr1, OrigServer);
		pclTmpPtr++;
		strcpy(Svr2, pclTmpPtr);
		blHas2Servers = true;
	}
	else
	{
		strcpy(Svr1, opConnectionstring);
	}



	//pomSocket = new CSocket();
	pomSocket = new CBlockingSocket();

	if (!pomSocket->Create())
	{
		int ilErr = pomSocket->GetLastError();
		CString olTxt;
		olTxt.Format("Creation failed: Error Number ==> %d",ilErr);
		delete pomSocket;
		pomSocket = NULL;
		bmIsConnected = false;
		AfxMessageBox(olTxt);
		blRet = -1;
		return blRet;
	}

	if(blHas2Servers == true)
	{
		bool blConnected = false;
		int ilRetry = 0;
		while(blConnected == false && ilRetry < 10)
		{
			if (!pomSocket->Connect(pomSocket->GetHostByName( Svr1, atoi(omPort.GetBuffer(0)))))
			{
				if (!pomSocket->Connect(pomSocket->GetHostByName( Svr2, atoi(omPort.GetBuffer(0)))))
				{
				//  No Server Found
					ilRetry++;
					Sleep(300);
				}
				else
				{
					ilRetry = 11;
					blConnected = true;
				}
			}
		}
		if(blConnected == false)
		{
			delete pomSocket; 
			pomSocket = NULL;
			blRet = -1;
			CString olMsg;
			olMsg.Format("Connection to Server failed!\nError-code=<%d>\nData will be inconsistent, do you want to shut down the application?",WSAGetLastError());
			if(AfxMessageBox(olMsg, MB_YESNO) == IDYES)
			{
				if(pomCommHandler != NULL)
				{
					pomCommHandler->CleanUpCom();
				}
				exit(0);
			}
			//AfxMessageBox(olMsg);
		}
	}
	else
	{
		bool blConnected = false;
		int ilRetry = 0;
		while(blConnected == false && ilRetry < 10)
		{
			if (!pomSocket->Connect(pomSocket->GetHostByName( Svr1, atoi(omPort.GetBuffer(0)))))
			{
				ilRetry++;
				Sleep(300);
			}
			else
			{
				ilRetry = 11;
				blConnected = true;
			}
		}
		if(blConnected == false)
		{
			delete pomSocket;
			pomSocket = NULL;
			blRet = -1;
			CString olMsg;
			olMsg.Format("Connection to Server failed!\nError-code=<%d>\nData will be inconsistent, do you want to shut down the application?",WSAGetLastError());
			if(AfxMessageBox(olMsg, MB_YESNO) == IDYES)
			{
				if(pomCommHandler != NULL)
				{
					pomCommHandler->CleanUpCom();
				}
				exit(0);
			}
			//AfxMessageBox(olMsg);
		}
	}
	if(blRet == 0)
	{
		bmIsConnected = true;
	}
	else
	{
		bmIsConnected = false;
	}

	return blRet;
}

//---------------------------------------------------------------
// Close the established connection
//---------------------------------------------------------------
void FastCedaConnection::ResetConnection()
{
	if(pomSocket != NULL)
	{
		pomSocket->Cleanup();
		delete pomSocket;
		pomSocket = NULL;
		bmIsConnected = false;
	}
}

//---------------------------------------------------------------
//clears omData and pcmDataBuffer
//---------------------------------------------------------------
void FastCedaConnection::ClearDataBuffer()
{
	if(pcmDataBuffer != NULL)
	{
		free(pcmDataBuffer);
		pcmDataBuffer = NULL;
	}
	omData.RemoveAll();
}

//---------------------------------------------------------------
//return 0 for SUCCESS of -1 for Error
//---------------------------------------------------------------
int FastCedaConnection::CedaAction()
{
	ClearDataBuffer();

	if (this->SendData())
	{
		if (this->ReceiveData())
		{
			return 0;
		}
		else
		{
			return (this->bmError) ? -1 : 0;
		}
	}
	else
	{
		return (this->bmError) ? -1 : 0;
	}
}


//---------------------------------------------------------------
// in case of bmError = true the error text will be returned
//---------------------------------------------------------------
CString FastCedaConnection::GetError()
{
	CString olRetString = omErrorText;
	return olRetString;
}


//---------------------------------------------------------------
// returns the amount of records to enable the user to iterate 
// through the databuffer an retrieve line by line
//---------------------------------------------------------------
long FastCedaConnection::GetDataCount()
{
	long blRet = omData.GetSize();
	return blRet;
}


//---------------------------------------------------------------
// returns the record at the specified index
//---------------------------------------------------------------
CString FastCedaConnection::GetDataRecord(long ipIdx)
{
	CString olRetString;
	if (ipIdx < omData.GetSize())
	{
		olRetString = omData[ipIdx];
	}

	return olRetString;
}


//---------------------------------------------------------------
//Returns the whole data buffer with "\n" separator
//---------------------------------------------------------------
CString FastCedaConnection::GetDataBuffer()
{
	CString olRetString;

	return olRetString;
}

/* 

============================================================================ 
Function GetKeyItem 

---------------------------------------------------------------------------- 
Strip data from a textbuffer using a keyword and an indicator of the key's 
'end of data'. 

Purpose and mainly use: find a text anchor by any pattern and determine the 
end of data 
by another pattern. This function will be used by the Full Document 
Interpreter (fdihdl) 
and by the Ceda Data Request Handler (cdrhdl) as well. It will be 
incorporated into the 
new 'accelerated communication' layer of the (rostering) client 
applications also. 

---------------------------------------------------------------------------- 
There are 4 IN parameters: 
pcpTextBuff: The text stream that has to be analysed 
pcpKeyCode: The keyword (pattern of anchor) of the text that is wanted 
pcpItemEnd: The pattern that defines the end of the wanted text 
bpCopyData: A flag that indicates if the identified data will be copied 
into the resultbuffer. 
This feature enables the caller to get the pointer to the data 
and the size of data 
regardless of the size of the given resultbuffer. 

---------------------------------------------------------------------------- 
And 2 OUT parameters: 
pcpResultBuff: will contain the extracted data stream (if bpCopyData == 
true) 
pcpResultSize: always contains the size of the identified data. 

---------------------------------------------------------------------------- 
Return Value: The (char) pointer to the identified item data in the text 
or NULL. 

============================================================================ 

Normal usage: 

---------------------------------------------------------------------------- 
Let's assume that we got a text stream (pclTextBuff) like: 

"{=CMD=}RTA{=HOP=}ATH{=MODUL=}{=DATA=}Here is a lot of data\nwith several 
rows ...."; 

and we want to extract the 'HOPO' section: We use the keyword {=HOP=} and 
as indicator for 
EndOfData we use "{=" from the beginning of the next keyword. Finally we 
decide to get the 
data back into the 'pclResult' buffer and set the flag CopyData to true. 

pclDataBegin = GetKeyItem(pclResult, &llSize, pclTextBuff, "{=HOP=}", "{=", 
true); 
if (pclDataBegin != NULL) 
{ 
printf("{=HOP=} :\t<%s>\n",pclResult); 
} 

Now we receive the data in pclResult and llSize contains the length. 
Additionally the returned value in pclDataBegin points to the original 
position of the 
data inside the text stream of pclTextBuff for any purposes. 

---------------------------------------------------------------------------- 
We have different possibilities to determine what we found: 
Case 1: 
CopyData set to true ... 
... and pclDataBegin != NULL: The keyword exists 
... and llSize > 0 : pclResult contains 
valid data 
... (or llSize == 0: The keyword section 
is empty, pclResult is set to '\0') 
CopyData set to true ... 
... and pclDataBegin == NULL: The keyword does not exist 
... then llSize = 0 : No data available 
... and 
pclResult is set to '\0' 
Case 2: 
CopyData set to false ... The result buffer 
remains unchanged 
... and pclDataBegin != NULL: The keyword exists 
... and llSize > 0 : pclDataBegin points 
to valid data 
... (or llSize == 0: The keyword section 
is empty) 
CopyData set to false ... 
... and pclDataBegin == NULL: The keyword does not exist 
... then llSize == 0 and it makes no sense to proceed. 

------------------------------------------------------------------------------------------------- 
Note: (Special Usage) 
When you expect a very big data stream of several Mb you surely won't copy 
it into your local 
pclResult buffer. In such cases it is always the best way to set the 
CopyFlag to 'false': You 
will get the DataPointer and DataLength and you proceed working directly on 
the received data. 

================================================================================================= 
*/ 

char* FastCedaConnection::GetKeyItem(char *pcpResultBuff, long *plpResultSize, 
				 char *pcpTextBuff, char *pcpKeyWord, 
				 char *pcpItemEnd, bool bpCopyData) 
{ 
	long llDataSize = 0L; 
	char *pclDataBegin = NULL; 
	char *pclDataEnd = NULL; 
	pclDataBegin = strstr(pcpTextBuff, pcpKeyWord); 
	/* Search the keyword */ 
	if (pclDataBegin != NULL) 
	{ 
		/* Did we find it? Yes. */ 
		pclDataBegin += strlen(pcpKeyWord); 
		/* Skip behind the keyword */ 
		pclDataEnd = strstr(pclDataBegin, pcpItemEnd); 
		/* Search end of data */ 
		if (pclDataEnd == NULL) 
		{ 
			/* End not found? */ 
			pclDataEnd = pclDataBegin + strlen(pclDataBegin); 
			/* Take the whole string */ 
		} /* end if */ 
		llDataSize = pclDataEnd - pclDataBegin; 
		/* Now calculate the length */ 
		if (bpCopyData == true) 
		{ 
			/* Shall we copy? */ 
			strncpy(pcpResultBuff, pclDataBegin, llDataSize); 
			/* Yes, strip out the data */ 
		} /* end if */ 
	} /* end if */ 
	if (bpCopyData == true) 
	{ 
		/* Allowed to set EOS? */ 
		pcpResultBuff[llDataSize] = 0x00; 
		/* Yes, terminate string */ 
	} /* end if */ 
	*plpResultSize = llDataSize; 
	/* Pass the length back */ 
	return pclDataBegin; 
	/* Return the data's begin */ 
} /* end GetKeyItem */ 



bool FastCedaConnection::SendData()
{
	CString olCommStr;
	CString olTmpStr;
	CString olTotPart;
	CString olT2;
	CString strTimeout;
	long llBytes = 0;

	this->bmError = false;

	CTime olStartTime, olEndTime;
	strTimeout.Format("%ld", imReceiveTimeout);


	olTotPart = "{=TOT=}";
	olTmpStr += "{=CMD=}" + this->omCommand;
	olTmpStr += "{=IDN=}" + this->omIdentifier;
	olTmpStr += "{=TBL=}" + this->omTable;
	olTmpStr += "{=EXT=}" + this->omTabext;
	olTmpStr += "{=HOPO=}" + this->omHopo;
	olTmpStr += "{=FLD=}" + this->omFields;
	olTmpStr += "{=WHE=}" + this->omWhere;
	olTmpStr += "{=USR=}" + this->omUser;
	olTmpStr += "{=WKS=}" + this->omWks;
	olTmpStr += "{=SEPA=}" + this->omSepa;
	olTmpStr += "{=APP=}" + this->omAppl;
	olTmpStr += "{=TIM=}" + strTimeout;
	olTmpStr += "{=TWE=}" + this->omTwe;
	olTmpStr += "{=TWS=}" + this->omTws;
	olTmpStr += "{=ORAHINT=}" + this->omOraHint;

	if(!this->omDataString.IsEmpty())
	{
		olTmpStr += "{=DAT=}" + this->omDataString;
	}

	// use packaging ?
	if (this->lmPacketSize > 0)
	{
		CString olPacketSize;
		olPacketSize.Format("%ld",this->lmPacketSize);
		olTmpStr += "{=PACK=}" + olPacketSize;
	}

	// simulate error ?
	if (this->omSimErr.GetLength() > 0)
	{
		olTmpStr += "{=SIMERR=}" + this->omSimErr;
	}


	return SendStringToCeda(olTmpStr);
}


bool FastCedaConnection::ReceiveData()
{
	bool blOK		   = true;	
	bool blCompleted   = false;
	bool blFirstPacket = true;

	CString	olMsg;
	int ilErr = 0;

	if (pomSocket == NULL)
		return false;

	while(blOK && !blCompleted)
	{
		blOK = ReceivePacketData(blFirstPacket,blCompleted);
		blFirstPacket = false;
	}

	if (blOK)
	{
		omDataString = CString(pcmDataBuffer);
		ExtractTextLineFast(omData, omDataString.GetBuffer(0), omSepa.GetBuffer(0));
		if(canKeepAlive == false)
		{
			SendStringToCeda(CString("{=ACK=}"));
			ResetConnection();
		}
		else
		{
			SendStringToCeda(CString("{=ACK=}KEEP"));
		}

	}
	else
	{
		ResetConnection();
	}
	//MWO: Due to {=ACK=}KEEP do not disconnect
	//ResetConnection();

	return blOK;

}

bool FastCedaConnection::ReceivePacketData(bool bpFirstPacket,bool& bpCompleted)
{
	bool blRet = true;
	int	ilErr = 0;
	CString olMsg;

	char pclBuf[25001];
	char *pclReturnBuffer;
	char pclRetFirst[100];
	char myTmpStr[20]="\0";
	char pclKeyWord[100];
	char pclResult[25000];
	char *pclDataBegin;
	char *pclCurrPtr = NULL;
	char *pclTotBufBegin;
	char *pclTotalDataBuffer = NULL;
	int  ilBytes;
	long llSize;
	long llTransferBytes = 0;
	long llCurrentBytes;
	
	int  revnow=0;


	memset((void*)pclBuf, 0x00, 1000);
	memset((void*)pclRetFirst, 0x00, 100);

	try
	{
		int myTotalNeedReV = 16;
		int myTotalRevNow = 0;
		int needRev=0;
		int count=0;
		while (myTotalRevNow < myTotalNeedReV && count<=3)
		{
			count++;
			memset((void*)myTmpStr, 0x00, 20);
			needRev = myTotalNeedReV - myTotalRevNow;
			revnow = pomSocket->Receive(myTmpStr, needRev, imReceiveTimeout);
			if (revnow == 0)
			{
				//olMsg.Format("Socket closed");
				ResetConnection();
				//AfxMessageBox(olMsg);
				return false;
			}
			myTotalRevNow += revnow;

			if (pclRetFirst[0] == NULL)
			{
				sprintf(pclRetFirst,"%s",myTmpStr);
			}
			else
			{
				sprintf(pclRetFirst,"%s%s",pclRetFirst,myTmpStr);
				//olMsg.Format("read <%d> times,get data <%s>", count,pclRetFirst);
				//AfxMessageBox(olMsg);
			}
		}
	}
	catch(const char* e)
	{
		ilErr = pomSocket->GetLastError();
		if(ilErr != 0)
		{
			olMsg.Format("CEDA-Socket Error: %d", ilErr);
			ResetConnection();
			AfxMessageBox(olMsg);
			return false;
		}
		else
		{
			ResetConnection();
			AfxMessageBox(e);
			return false;
		}
	}

	if(strcmp(pclRetFirst, "") == 0)
		return false;

	strcpy(pclKeyWord,"{=TOT=}"); 
	if((pclDataBegin = GetKeyItem(pclResult, &llSize, pclRetFirst, pclKeyWord, "{=", true)) == NULL)
	{
		//olMsg.Format("%s - Keyword not found in <%s>", pclKeyWord,pclRetFirst);
		//AfxMessageBox(olMsg);
		return false;
	}

	long llTotal = atol(pclResult);
	llTotal -= 16;
	if (llTotal < 0) //take care that it is not negative
	{
		llTotal = 0;
	}

	pclReturnBuffer = (char*)malloc((size_t)llTotal+1);
	memset((void*)pclReturnBuffer, 0x00, llTotal+1);
	if (pclTotalDataBuffer == NULL)
	{
		pclTotalDataBuffer = (char*)malloc((size_t)llTotal+1);
	}

	memset((void*)pclTotalDataBuffer, 0x00, llTotal+1);

	try
	{
		ilBytes = pomSocket->Receive(pclTotalDataBuffer, llTotal, imReceiveTimeout);
		if(strcmp(pclTotalDataBuffer, "") == 0)
			return false;
	}
	catch(const char* e)
	{
		ilErr = pomSocket->GetLastError();
		if (ilErr != 0)
		{
			olMsg.Format("CEDA-Socket Error: %d", ilErr);
			ResetConnection();
			AfxMessageBox(olMsg);
			return false;
		}
		else
		{
			ResetConnection();
			AfxMessageBox(e);
			return false;
		}
	}

	pclTotBufBegin = pclTotalDataBuffer;
	pclTotalDataBuffer = pclTotalDataBuffer + ilBytes;
	llCurrentBytes = strlen(pclReturnBuffer);
	llTransferBytes += ilBytes;
	struct linger rmLinger;
	rmLinger.l_onoff = 0;
	rmLinger.l_linger = 2;
	while (llTransferBytes < llTotal)
	{
		try
		{
			ilBytes = pomSocket->Receive(pclTotalDataBuffer, (llTotal-llTransferBytes), imReceiveTimeout);
			ilErr = pomSocket->GetLastError();
			if(ilErr != 0)
			{
				olMsg.Format("CEDA-Socket Error: %d", ilErr);
				ResetConnection();
				AfxMessageBox(olMsg);
				return false;
			}
			pclTotalDataBuffer = pclTotalDataBuffer + ilBytes;
			llTransferBytes += ilBytes;
		}
		catch(const char* e)
		{
			ilErr = pomSocket->GetLastError();
			if(ilErr != 0)
			{
				olMsg.Format("CEDA-Socket Error: %d", ilErr);
				ResetConnection();
				AfxMessageBox(olMsg);
				return false;
			}
			else
			{
				ResetConnection();
				AfxMessageBox(e);
				return false;
			}
		}
	}
	pclTotalDataBuffer = pclTotBufBegin;

//----------------------------
// Extract the incomming data
	strcpy(pclKeyWord,"{=CMD=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, pclTotalDataBuffer, pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		omCommand = CString(pclResult);
	} 
	strcpy(pclKeyWord,"{=IDN=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, pclTotalDataBuffer, pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		omIdentifier = CString(pclResult);
	} 
	strcpy(pclKeyWord,"{=TBL=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, pclTotalDataBuffer, pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		omTable = CString(pclResult);
	} 
	strcpy(pclKeyWord,"{=EXT=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, pclTotalDataBuffer, pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		omTabext = CString(pclResult);
	} 

	strcpy(pclKeyWord,"{=TWS=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, pclTotalDataBuffer, pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		omTws = CString(pclResult);
	} 
	strcpy(pclKeyWord,"{=TWE=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, pclTotalDataBuffer, pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		omTwe = CString(pclResult);
	} 



	strcpy(pclKeyWord,"{=HOPO=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, pclTotalDataBuffer, pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		omHopo = CString(pclResult);
	} 
	strcpy(pclKeyWord,"{=FLD=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, pclTotalDataBuffer, pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		omFields = CString(pclResult);
	} 
	strcpy(pclKeyWord,"{=WHE=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, pclTotalDataBuffer, pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		omWhere = CString(pclResult);
	} 
	strcpy(pclKeyWord,"{=USR=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, pclTotalDataBuffer, pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		omUser = CString(pclResult);
	} 
	strcpy(pclKeyWord,"{=WKS=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, pclTotalDataBuffer, pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		omWks  = CString(pclResult);
	} 
	strcpy(pclKeyWord,"{=APP=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, pclTotalDataBuffer, pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		omAppl  = CString(pclResult);
	} 

	strcpy(pclKeyWord,"{=SEPA=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, pclTotalDataBuffer, pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		omSepa  = CString(pclResult);
	} 

	strcpy(pclKeyWord,"{=ERR=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, pclTotalDataBuffer, pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		omErrorText = CString(pclResult);
		this->bmError = false;
		if(omErrorText.GetLength() > 0)
		{
			this->bmError = true;
			blRet = false;
		}
	} 

	strcpy(pclKeyWord,"{=PACK=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, pclTotalDataBuffer, pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		lmCurrentPacketSize  = atol(pclResult);
		if (pfmCallback != NULL)
		{
			this->pfmCallback(pomControl,lmCurrentPacketSize);
		}

		if (this->lmPacketSize > 0)
		{
			canKeepAlive = true;
		}
	}
	else
	{
		lmCurrentPacketSize  = 0;
	}

	int ilDataLen = 0;
	if (bmError == false)
	{
		strcpy(pclKeyWord,"{=DAT=}"); 
		pclDataBegin = GetKeyItem(pclResult, &llSize, pclTotalDataBuffer, pclKeyWord, "{=", false); 
		if (pclDataBegin != NULL) 
		{ 
			if (bpFirstPacket)
			{
				ClearDataBuffer();
				ilDataLen = strlen(pclDataBegin);
				pcmDataBuffer = (char*)malloc((size_t)ilDataLen+1);
				memset(pcmDataBuffer, 0x00, ilDataLen+1);
				memcpy(pcmDataBuffer, pclDataBegin, (size_t)ilDataLen);
				omAppl  = CString(pclResult);
			}
			else
			{
				int ilOldDataLen = strlen(pcmDataBuffer);
				ilDataLen = strlen(pclDataBegin);
				pcmDataBuffer = (char *)realloc(pcmDataBuffer,ilOldDataLen + ilDataLen + omSepa.GetLength() + 1);
				memset(&pcmDataBuffer[ilOldDataLen], 0x00, ilDataLen + omSepa.GetLength() + 1);
				memcpy(&pcmDataBuffer[ilOldDataLen], omSepa,omSepa.GetLength());
				memcpy(&pcmDataBuffer[ilOldDataLen + omSepa.GetLength()], pclDataBegin, (size_t)ilDataLen);
			}

			if (this->lmPacketSize <= 0)
			{
				bpCompleted = true;
			}
			else if (lmCurrentPacketSize < this->lmPacketSize)
			{
				bpCompleted = true;
			}
			else
			{
				bpCompleted = false;
			}

		}
		else
		{
			if (this->lmPacketSize <= 0)
			{
				bpCompleted = true;
			}
			else if (lmCurrentPacketSize < this->lmPacketSize)
			{
				bpCompleted = true;
			}
			else
			{
				bpCompleted = false;
			}
		}
	}
	else
	{
		ClearDataBuffer();
	}

// End data extractions
//----------------------------
	free(pclReturnBuffer);
	free(pclTotalDataBuffer);
	pclTotalDataBuffer = NULL;

	return blRet;
}

bool FastCedaConnection::CloseConnection()
{
	bool blRet = true;
	if(bmIsConnected == true)
	{
		blRet = SendStringToCeda(CString("{=CMD=}CLOSE"));
		ResetConnection();
	}
	return blRet;
}

bool FastCedaConnection::SendStringToCeda(CString opString)
{
	bool blRet = true;
	bool blSend = false;
	long llBytes = 0;
	CString olCommStr = "";
	CString olTotPart = "{=TOT=}";
	CString olT2;
	CString msg;
	int ilCnt = 0;

	if(bmIsConnected == false && pomCommHandler != NULL)
	{
		ConnectToCeda(omServer);
	}


	if(pomSocket != NULL && bmIsConnected == true)
	{
		llBytes = (long)opString.GetLength()+16;
		olT2.Format("% 9ld", llBytes);
		olCommStr = olTotPart + olT2 + opString;
		while((blSend == false) && (ilCnt < 2))
		{
			try
			{
				int nc = pomSocket->Send(olCommStr.GetBuffer(0), olCommStr.GetLength(), this->imSendTimeout);
				blSend = true;
				blRet = true;
			}
			catch(const char* e)
			{
				WSASetLastError(0);
				ResetConnection();
				blRet = false;
				msg = CString(e);
				if(ConnectToCeda(omServer) == -1)
				{
					blRet = false;
					ilCnt=3;
				}
			} 
			ilCnt++;
		}
		if(blRet == false)
		{
			AfxMessageBox(msg);
		}
	}
	return blRet;
}









