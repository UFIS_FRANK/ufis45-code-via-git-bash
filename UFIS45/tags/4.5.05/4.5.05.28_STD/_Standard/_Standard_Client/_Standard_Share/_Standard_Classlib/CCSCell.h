#ifndef AFX_CCSCELL_H__0286A342_A838_11D1_A3CE_0000B45A33F5__INCLUDED_
#define AFX_CCSCELL_H__0286A342_A838_11D1_A3CE_0000B45A33F5__INCLUDED_

// CCSCell.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Fenster CCSCell 







class CCSCell : public CWnd
{
// Konstruktion
public:
	CCSCell();

// Attribute
public:

// Operationen
public:

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CCSCell)
	//}}AFX_VIRTUAL

// Implementierung
public:
	virtual ~CCSCell();

	CCSCell(const CCSCell& s);
	const CCSCell& operator= ( const CCSCell& s);

	CBrush omBrush;

	COLORREF omBKColor;
	COLORREF omTextColor;
	COLORREF omSelBKColor;
	COLORREF omSelTextColor ;
	COLORREF omDisabledBKColor;

	bool bmDisabled;
	bool bmSelected;
	bool bmMultiSelected;
	bool bmIsMultiSelected;
	bool bmToggleSel;
	bool bmMouseMove;
	int imLine;
	int imColumn;
	CString omName;
	CString omText;
	CFont *pomFont;
	CFont omDefaultFont;

	void SetName(CString opName){ omName = opName;};

	void SetText(CString opText);

	void SetFont(CString opText);

	void SetTextColor(COLORREF opColor){ omTextColor = opColor;};

	COLORREF SetSelBKColor(COLORREF opColor);
	COLORREF SetBKColor(COLORREF opColor);
	COLORREF SetDisabledBKColor(COLORREF opColor);

	CString GetName(){ return omName;};

	bool IsSelect(){ return bmSelected;};

	void SetLine(int ipLine){imLine = ipLine;};
	
	void SetColumn(int ipColumn){imColumn = ipColumn;};
	
	void ReleaseMultiSel();

	void SetMultiSel(bool bpSet);

	void Select(bool bpSet = true);

	void Disable(bool bpSet = true);

	bool IsDisabled();

	//functios for a focusdrawing on an cell (used to mark the actual day etc.)
	//negative values will set a offset with zero and mark the cell.
	//possitive values draw a focus inside the cell, if the focus-height will be less than
	//two pixel no focus will be set. 
	bool SetFocusRect(int ipOffset);
	bool HasFocusRect(int &ipOffset);

private:
	//members for a focusdrawing on an cell (used to mark the actual day etc.)
	bool bmHasFocusRect;
	int imFocusRectOffset;

	// Generierte Nachrichtenzuordnungsfunktionen
protected:
	//{{AFX_MSG(CCSCell)
	afx_msg void OnPaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
    afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};



struct CCSCELLNOTIFY
{
	CCSCell *SourceControl;
	CString	Name;
	int	 Line;	
	int	 Column;
	CPoint Point;
	CString Text;
	CCSCELLNOTIFY()
	{
		SourceControl = NULL;
		Line = 0;
		Column = 0;
	}
};





/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_CCSCELL_H__0286A342_A838_11D1_A3CE_0000B45A33F5__INCLUDED_
 