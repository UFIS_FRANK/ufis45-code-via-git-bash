// FlightData.cpp: implementation of the FlightData class.
//
//////////////////////////////////////////////////////////////////////

#include <FlightData.h>
#include <CedaSysTabData.h>
#include <CCSBasicFunc.h>
#include <CCSDefines.h>




#include <malloc.h>


CUIntArray FlightData::smIndex;



// Local function prototype
static void ProcessFlightCf(void *popInstance, int ipDDXType,	void *vpDataPointer, CString &ropInstanceName);



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

FlightData::FlightData(CCSDdx *popDdx)
{
	pomDdx = popDdx;
	FieldCount = 0;
}


FlightData::~FlightData()
{
	Data.DeleteAll();
	Desc.DeleteAll();
	Modul.DeleteAll();

	omUrnoMap.RemoveAll();

	CCSPtrArray<RecordSet> *prlRkeys; 
	POSITION pos;
	CString olTmp;
	for( pos = omRkeyMap.GetStartPosition(); pos != NULL; )
	{
		omRkeyMap.GetNextAssoc( pos, olTmp , (void *&)prlRkeys );
		prlRkeys->RemoveAll();
		delete prlRkeys;
	}
	omRkeyMap.RemoveAll();
}



void FlightData::Init(CString opTableExtension)
{
	omTableExtension = opTableExtension;

	CedaSysTabData olSystab;	

	olSystab.SetTableExtension(omTableExtension);

	char pclSelection[256];
	sprintf(pclSelection, "WHERE TANA='AFT'");


	if(!olSystab.Read(pclSelection, true, "FINA,TYPE,FELE,FITY,SYST"))
	{
		MessageBox(NULL,"Fehler beim lesen der Systab!", "Fehler", MB_OK);
		return ;
	}

	FieldSet  *prlFieldSet;

	for(int i = olSystab.omData.GetSize() - 1; i >= 0; i--)
	{
			prlFieldSet = new FieldSet;
			prlFieldSet->Field = CString(olSystab.omData[i].Fina);
			prlFieldSet->Type = CString(olSystab.omData[i].Type);
			prlFieldSet->DBType = CString(olSystab.omData[i].Fity);
			prlFieldSet->Length = olSystab.omData[i].Fele;

			Desc.Add(prlFieldSet);
			omIndexMap.SetAt((LPCSTR)prlFieldSet->Field, (void *)FieldCount);
			FieldCount++;

	}

}



void FlightData::AddModul(DWORD dwModul, CString opFields)
{
	ModulData *prlModul = new ModulData;

	Modul.Add(prlModul);
	
	prlModul->Modul = dwModul;

	prlModul->Fields = opFields;

	omModulMap.SetAt((void*)dwModul,  (void*) prlModul);

}


void FlightData::Register(void)
{
	pomDdx->Register( (void *)this, BC_NFLIGHT_CHANGE, CString("FLIGHTDATA"), CString("Flight changed"), ProcessFlightCf);
	pomDdx->Register( (void *)this, BC_NFLIGHT_DELETE, CString("FLIGHTDATA"), CString("Flight delete "), ProcessFlightCf);
}

void FlightData::UnRegister(void)
{
	pomDdx->UnRegister(this,NOTUSED);
}




ModulData *FlightData::GetModul(DWORD dwModul)
{
	ModulData *prlModul = NULL;

	omModulMap.Lookup((void*)dwModul,  (void*&) prlModul);

	return prlModul;
}

void FlightData::RemoveModul(DWORD dwModul)
{



}


int FlightData::GetFieldIndex(CString opField)
{
	int ilRet = -1;	
	omIndexMap.Lookup(opField, (void *&)ilRet);
	return ilRet;
}



void FlightData::Clear(DWORD dwModul)
{
	RecordSet *prlFlight;

	for(int i = Data.GetSize() - 1; i >= 0; i--)
	{
		prlFlight = &Data[i];

		if(prlFlight->UseBy & dwModul)
		{
			prlFlight->UseBy = prlFlight->UseBy & ~dwModul;

			if(prlFlight->UseBy == 0)
			{
				DelFromKeyMap(prlFlight);
				Data.DeleteAt(i);
			}
		}
	}

	_heapmin( );
}


int FlightData::Read(DWORD dwModul, CString opSelection, bool bpClearAll)
{
	ModulData *prlModul = GetModul(dwModul);

	if(prlModul == NULL)
		return -1;

	Clear(dwModul);
	
	prlModul->Selection = opSelection;

	char pclSelection[1024] = "";
	char pclTable[64];
	char pclFieldList[2048];
	char pclData[256] = "";
	char pclCmd[16] = "GFR";
	CString olDataList;
	
	strcpy(pclSelection, opSelection);
	strcpy(pclTable, "AFT");
	strcat(pclTable, omTableExtension);


	strcpy(pclFieldList, prlModul->Fields );
	

	CStringArray *polDataBuf = GetDataBuff();

	polDataBuf->RemoveAll();

/*
typedef struct _MEMORYSTATUS { // mst 
    DWORD dwLength;        // sizeof(MEMORYSTATUS) 
    DWORD dwMemoryLoad;    // percent of memory in use 
    DWORD dwTotalPhys;     // bytes of physical memory 
    DWORD dwAvailPhys;     // free physical memory bytes 
    DWORD dwTotalPageFile; // bytes of paging file 
    DWORD dwAvailPageFile; // free bytes of paging file 
    DWORD dwTotalVirtual;  // user bytes of address space 
    DWORD dwAvailVirtual;  // free user bytes 
} MEMORYSTATUS, *LPMEMORYSTATUS; 

*/


	MEMORYSTATUS rlMemAfter;
	MEMORYSTATUS rlMemBefore;


	rlMemBefore.dwLength = sizeof(rlMemBefore);
	rlMemAfter.dwLength = sizeof(rlMemAfter);

	GlobalMemoryStatus(&rlMemBefore);

	/*
	TRACE("\nbefore------------------------------------------------------");

	TRACE("\ndwMemoryLoad   %d  percent of memory in use", rlMem.dwMemoryLoad);
	TRACE("\ndwTotalPhys     %d  bytes of physical memory", rlMem.dwTotalPhys);
	TRACE("\ndwAvailPhys     %d  free physical memory bytes", rlMem.dwAvailPhys);
	TRACE("\ndwTotalPageFile %d  bytes of paging file", rlMem.dwTotalPageFile);
	TRACE("\ndwAvailPageFile %d  free bytes of paging file", rlMem.dwAvailPageFile);
	TRACE("\ndwTotalVirtual  %d  user bytes of address space", rlMem.dwTotalVirtual);
	TRACE("\ndwAvailVirtual  %d  free user bytes", rlMem.dwAvailVirtual);
	
	TRACE("\nbefore------------------------------------------------------");
	*/


	bool blRet = CedaAction(pclCmd, pclTable, pclFieldList, pclSelection, "", pclData, "BUF1", false, 0);


	int ilCount = polDataBuf->GetSize();
	
	TRACE("\n Count: %d", ilCount );


	//TRACE("\nBegin %s", );

	RecordSet *p = new RecordSet[ilCount];

	int i= 0;
	while(polDataBuf->GetSize() > 0)
	{
		//RecordSet *prpFlight = new RecordSet(FieldCount);
		FillRecord(&p[i], (*polDataBuf)[0], prlModul->Fields);
		AddFlightInternal(&p[i], dwModul);
		polDataBuf->RemoveAt(0);
		i++;
	}


	GlobalMemoryStatus(&rlMemAfter);

	/*
	TRACE("\nafter------------------------------------------------------");

	TRACE("\ndwMemoryLoad   %d  percent of memory in use", rlMem.dwMemoryLoad);
	TRACE("\ndwTotalPhys     %d  bytes of physical memory", rlMem.dwTotalPhys);
	TRACE("\ndwAvailPhys     %d  free physical memory bytes", rlMem.dwAvailPhys);
	TRACE("\ndwTotalPageFile %d  bytes of paging file", rlMem.dwTotalPageFile);
	TRACE("\ndwAvailPageFile %d  free bytes of paging file", rlMem.dwAvailPageFile);
	TRACE("\ndwTotalVirtual  %d  user bytes of address space", rlMem.dwTotalVirtual);
	TRACE("\ndwAvailVirtual  %d  free user bytes", rlMem.dwAvailVirtual);
	
	TRACE("\nafter------------------------------------------------------");
	*/


	TRACE("\nafter------------------------------------------------------");

	TRACE("\ndwMemoryLoad    %10d  %10d  %10d    percent of memory in use",    rlMemBefore.dwMemoryLoad,    rlMemAfter.dwMemoryLoad    , rlMemAfter.dwMemoryLoad    - rlMemBefore.dwMemoryLoad  );
	TRACE("\ndwTotalPhys     %10d  %10d  %10d    bytes of physical memory",    rlMemBefore.dwTotalPhys,     rlMemAfter.dwTotalPhys     , rlMemAfter.dwTotalPhys     - rlMemBefore.dwTotalPhys     ); 
	TRACE("\ndwAvailPhys     %10d  %10d  %10d    free physical memory bytes",  rlMemBefore.dwAvailPhys,     rlMemAfter.dwAvailPhys     , rlMemAfter.dwAvailPhys     - rlMemBefore.dwAvailPhys    );
	TRACE("\ndwTotalPageFile %10d  %10d  %10d    bytes of paging file",        rlMemBefore.dwTotalPageFile, rlMemAfter.dwTotalPageFile , rlMemAfter.dwTotalPageFile - rlMemBefore.dwTotalPageFile);
	TRACE("\ndwAvailPageFile %10d  %10d  %10d    free bytes of paging file",   rlMemBefore.dwAvailPageFile, rlMemAfter.dwAvailPageFile , rlMemAfter.dwAvailPageFile - rlMemBefore.dwAvailPageFile);
	TRACE("\ndwTotalVirtual  %10d  %10d  %10d    user bytes of address space", rlMemBefore.dwTotalVirtual,  rlMemAfter.dwTotalVirtual  , rlMemAfter.dwTotalVirtual  - rlMemBefore.dwTotalVirtual);
	TRACE("\ndwAvailVirtual  %10d  %10d  %10d    free user bytes",             rlMemBefore.dwAvailVirtual,  rlMemAfter.dwAvailVirtual  , rlMemAfter.dwAvailVirtual  - rlMemBefore.dwAvailVirtual);
	
	TRACE("\nafter------------------------------------------------------");




	return ilCount;
}


bool FlightData::FillRecord(RecordSet *prpRecord , CString &opData, CString &opFields)
{

	CStringArray olData;
	CStringArray olFields;

	int ilCount = ExtractItemList(opData, &olData);

	ExtractItemList(opFields, &olFields);

	for(int i = 0; i < ilCount; i++)
	{
		prpRecord->Values[GetFieldIndex(olFields[i])] = olData[i];
	}

	return true;
}



bool FlightData::AddFlightInternal(RecordSet *prpRecord, DWORD dwModul )
{
	prpRecord->UseBy = prpRecord->UseBy | dwModul;

	Data.Add(prpRecord);
	AddToKeyMap(prpRecord);
	return true;
}


void FlightData::DelFromKeyMap(RecordSet *prpRecord )
{

	CCSPtrArray<RecordSet> *prlRkeys; 
	RecordSet *prlRecord; 

	if (prpRecord != NULL )
	{
		omUrnoMap.RemoveKey((*prpRecord)[GetFieldIndex("URNO")]);

		if(omRkeyMap.Lookup((*prpRecord)[GetFieldIndex("RKEY")],(void *& )prlRkeys) == TRUE)
		{
			for(int i = prlRkeys->GetSize() - 1; i >= 0; i--)
			{
				for(int i = prlRkeys->GetSize() - 1; i >= 0; i--)
				{
					prlRecord = &(*prlRkeys)[i];

					if( (*prlRecord)[GetFieldIndex("URNO")] == (*prpRecord)[GetFieldIndex("URNO")])
					{
						prlRkeys->RemoveAt(i);
						break;
					}
				}
			}
			if(prlRkeys->GetSize() == 0)
			{
				omRkeyMap.RemoveKey((*prpRecord)[GetFieldIndex("RKEY")]);
				delete prlRkeys;
			}
		}
	}
}


bool FlightData::AddToKeyMap(RecordSet *prpRecord )
{
	CCSPtrArray<RecordSet> *prlRkeys; 
	RecordSet *prlRecord; 


	smIndex.RemoveAll();

	smIndex.Add(GetFieldIndex("ADID"));
	smIndex.Add(GetFieldIndex("TIFA"));

	if (prpRecord != NULL )
	{
		omUrnoMap.SetAt((*prpRecord)[GetFieldIndex("URNO")], prpRecord);


		if(omRkeyMap.Lookup((*prpRecord)[GetFieldIndex("RKEY")],(void *& )prlRkeys) == TRUE)
		{
			for(int i = prlRkeys->GetSize() - 1; i >= 0; i--)
			{
				prlRecord = &(*prlRkeys)[i];

				if( (*prlRecord)[GetFieldIndex("URNO")] == (*prpRecord)[GetFieldIndex("URNO")])
				{
					prlRkeys->RemoveAt(i);
					break;
				}
			}
			prlRkeys->Add(prpRecord);
			prlRkeys->Sort(CompareRecord);
		}
		else
		{
			prlRkeys = new CCSPtrArray<RecordSet>;
			
			omRkeyMap.SetAt((*prpRecord)[GetFieldIndex("RKEY")], prlRkeys);
			prlRkeys->Add(prpRecord);
			prlRkeys->Sort(CompareRecord);
		}
	}
    return true;
}




const RecordSet *FlightData::GetFlightByUrno(CString opUrno)
{
	RecordSet *prlFlight;
	if (omUrnoMap.Lookup(opUrno,(void *& )prlFlight) == TRUE)
		return prlFlight;
	else
		return NULL;
}




int FlightData::CompareRecord(const RecordSet **e1, const RecordSet **e2)
{
	int	ilCompareResult = 0;
	int ilCount = smIndex.GetSize();


	//CString olTest = (**e1).Values[1];

	for (int i = 0; i < ilCount; i++)
	{
		ilCompareResult = ( (**e1).Values[smIndex[i]] == (**e2).Values[smIndex[i]])? 0: ((**e1).Values[smIndex[i]] > (**e2).Values[smIndex[i]])? 1: -1;

		if (ilCompareResult != 0)
			return ilCompareResult;
	}
	return 0;
}



RecordSet *FlightData::GetArrival(CString opRkey)
{
	CCSPtrArray<RecordSet> *prlRkeys; 
	RecordSet *prlRecord; 

	if(omRkeyMap.Lookup( opRkey,(void *& )prlRkeys) == TRUE)
	{
		if(prlRkeys->GetSize() > 0)
		{
			prlRecord = &(*prlRkeys)[0];

			if(prlRecord->Values[GetFieldIndex("ADID")] == "A" )
				return prlRecord;
		}
	}
	return NULL;
}


RecordSet *FlightData::GetDeparture(CString opRkey)
{
	CCSPtrArray<RecordSet> *prlRkeys; 
	RecordSet *prlRecord; 

	if(omRkeyMap.Lookup( opRkey,(void *& )prlRkeys) == TRUE)
	{
		if(prlRkeys->GetSize() > 0)
		{
			prlRecord = &(*prlRkeys)[prlRkeys->GetSize() - 1];

			if(prlRecord->Values[GetFieldIndex("ADID")] == "D")
				return prlRecord;
		}
	}
	return NULL;
}



void  ProcessFlightCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
		case BC_NFLIGHT_CHANGE :
		case BC_NFLIGHT_DELETE :
			((FlightData *)popInstance)->ProcessFlightBc(ipDDXType,vpDataPointer,ropInstanceName);
			break;
	}
}


void  FlightData::ProcessFlightBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
/*
	long llRkeyNew1 = 0;
	long llRkeyNew2 = 0;
	long llRkeyOld1 = 0;
	long llRkeyOld2 = 0;

	CStringArray olStrArray;
	RKEYLIST *prlRkey;
	SEASONFLIGHTDATA *prlFlight;
	bool blIgnoreView = false;

	BcStruct *prlBcStruct = (BcStruct *) vpDataPointer;

	if(prlBcStruct == NULL)
		return;

	if(atol(prlBcStruct->Tws) == IDM_IMPORT)
		return;


	if (ipDDXType == BC_FLIGHT_CHANGE)
	{

		if(strcmp(prlBcStruct->Cmd, "UFR") == 0)
		{
			ProcessFlightUFR( prlBcStruct );
			return;
		}

		if(strcmp(prlBcStruct->Cmd, "IFR") == 0)
		{
			ProcessFlightIFR( prlBcStruct );
			return;
		}
		
		if(strcmp(prlBcStruct->Cmd, "UPS") == 0)
		{
			ProcessFlightUFR( prlBcStruct );
			return;
		}


		if(strcmp(prlBcStruct->Cmd, "UPJ") == 0)
		{
			ProcessFlightUFR( prlBcStruct );
			return;
		}

		/////////////////////////////////////////////////////////////////////////////
		// BC_Split
		if(strcmp(prlBcStruct->Cmd, "SPR") == 0)
		{
			ExtractItemList(CString(prlBcStruct->Data), &olStrArray);
			
			if(olStrArray.GetSize() == 2)
			{
				llRkeyOld1 = atol(olStrArray[0]);
				llRkeyNew1 = atol(olStrArray[1]);
			
				if(omRkeyMap.Lookup((void *)llRkeyOld1,(void *& )prlRkey) == TRUE)
				{
					ogDdx.DataChanged((void *)this, S_FLIGHT_DELETE, (void *)prlRkey );
					ogDdx.DataChanged((void *)this, S_FLIGHT_INSERT, (void *)prlRkey );
				}
				if(omRkeyMap.Lookup((void *)llRkeyNew1,(void *& )prlRkey) == TRUE)
				{
					ogDdx.DataChanged((void *)this, S_FLIGHT_INSERT, (void *)prlRkey );
				}
			}
			return;
		}
		
		/////////////////////////////////////////////////////////////////////////////
		// BC_Join
		if(strcmp(prlBcStruct->Cmd, "JOF") == 0)
		{
			ExtractItemList(CString(prlBcStruct->Data), &olStrArray);

			if(olStrArray.GetSize() == 3)
			{
				llRkeyOld1 = atol(olStrArray[0]);
				llRkeyOld2 = atol(olStrArray[1]);
				llRkeyNew1 = atol(olStrArray[2]);
				
				if(omRkeyMap.Lookup((void *)llRkeyOld1,(void *& )prlRkey) == TRUE)
				{
					ogDdx.DataChanged((void *)this, S_FLIGHT_DELETE, (void *)prlRkey );
					ogDdx.DataChanged((void *)this, S_FLIGHT_INSERT, (void *)prlRkey );
				}
				if(omRkeyMap.Lookup((void *)llRkeyOld2,(void *& )prlRkey) == TRUE)
				{
					ogDdx.DataChanged((void *)this, S_FLIGHT_DELETE, (void *)prlRkey );
					ogDdx.DataChanged((void *)this, S_FLIGHT_INSERT, (void *)prlRkey );
				}
				if(omRkeyMap.Lookup((void *)llRkeyNew1,(void *& )prlRkey) == TRUE)
				{
					ogDdx.DataChanged((void *)this, S_FLIGHT_DELETE, (void *)prlRkey );
					ogDdx.DataChanged((void *)this, S_FLIGHT_INSERT, (void *)prlRkey );
				}
			}
			return;
		}
	}

	/////////////////////////////////////////////////////////////////////////////
	// BC_Delete
	if (ipDDXType == BC_FLIGHT_DELETE)
	{
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));		
		ExtractItemList(CString(prlBcStruct->Data), &olStrArray);

		for(int ilLc = olStrArray.GetSize() - 1; ilLc >= 0; ilLc--)
		{
			if(omUrnoMap.Lookup((void *)atol(olStrArray[ilLc]),(void *& )prlFlight) == TRUE)
			{
				if(omRkeyMap.Lookup((void *)prlFlight->Rkey,(void *& )prlRkey) == TRUE)
				{
					if(prlRkey->Rotation.GetSize() > 1)
					{
						ogDdx.DataChanged((void *)this, S_FLIGHT_DELETE, (void *)prlRkey );
						DeleteFlightInternal(atol(olStrArray[ilLc]),true);
						ogDdx.DataChanged((void *)this, S_FLIGHT_INSERT, (void *)prlRkey );
					}
					else
					{
						if(prlRkey->Rotation.GetSize() == 1)
						{
							ogDdx.DataChanged((void *)this, S_FLIGHT_DELETE, (void *)prlRkey );
							DeleteFlightInternal(atol(olStrArray[ilLc]), true);
						}
					}
				}
			}
		}
	}


*/





}

