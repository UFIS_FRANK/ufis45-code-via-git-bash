#if !defined(AFX_COLORCHECK_H__A897D857_09A3_11D6_8CAD_00500439B545__INCLUDED_)
#define AFX_COLORCHECK_H__A897D857_09A3_11D6_8CAD_00500439B545__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ColorCheck.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CColorCheck window

class AatColorCheckBox : public CButton
{
DECLARE_DYNAMIC(AatColorCheckBox)

// Construction
public:

	AatColorCheckBox();
	virtual ~AatColorCheckBox();
	
//	BOOL Attach(const UINT nID, CWnd* pParent);	
	virtual void SetCheck(int nCheck);
	virtual int GetCheck();
	COLORREF SetBkColor(COLORREF color);
	COLORREF SetArrowColor(COLORREF newColor);
	COLORREF SetMyTextColor(COLORREF txtColor);
// Implementation
private:
	BOOL checkFlag;
	UINT oldAction;
	UINT oldState;
	BOOL drawFocus;
	COLORREF newColor, newArrowColor, newTextColor;

	// Generated message map functions
protected:
	void DrawCheckCaption(CDC *pDC, CRect R, const char *Buf, COLORREF TextColor);
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItem);	//override the standard function (no WM_DRAWITEM !!!)
	COLORREF GetDisabledColor() { return disabled; }
	//{{AFX_MSG(AatColorCheckBox)
	afx_msg BOOL OnButtonClick();		
	//}}AFX_MSG
private:
	COLORREF disabled;

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COLORCHECK_H__A897D857_09A3_11D6_8CAD_00500439B545__INCLUDED_)
