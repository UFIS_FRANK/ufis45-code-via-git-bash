
#include <stdafx.h>
#include <ccslog.h>

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <time.h>
#include <sys/timeb.h>

#include <iostream.h>
#include <fstream.h>



static const char *TimeFormat(const char *pcpFormat)
{
  static char pclRes[64];
	/**************
  time_t llTime         = time(0L);
  struct tm *prlSysTime = ::localtime(&llTime);
  ::strftime(pclRes, 32, pcpFormat, prlSysTime);
	***************/
 // return pclRes;
  time_t llTime         = time(0L);
  struct tm *prlSysTime = ::localtime(&llTime);
	sprintf(pclRes,"%02d:%02d.%02d",prlSysTime->tm_hour,prlSysTime->tm_min,prlSysTime->tm_sec);
	return pclRes;
}

// get time stamp in yyyymmddHHMMSS format
// 
CString TimeStamp()
{
  return CString(TimeFormat("%Y%m%d%H%M%S"));
}

// get time stamp in HH:MM:SS[:mmm] format
//
CString TimeOfDayStamp(bool bpMillisecs)
{
  CString olStr(TimeFormat("%X"));

  if (bpMillisecs)
  {
    char pclTmp[8];
    struct timeb rlSysTimeb;
    ::ftime(&rlSysTimeb);
    ::sprintf(pclTmp, ":%3.3d", rlSysTimeb.millitm);
    olStr += pclTmp;
  }
  return olStr;
}

static void msg2stream(ostream& os, const CString& prefix,
		       const char *fmt, va_list va)
{
  char str[1024]; // unsafe!
  ::vsprintf(str, fmt, va);
  os << prefix << str << endl;
  os.flush();
}

// class CCSLog
//

ostream * const CCSLog::pomDefaultOStream = &cerr;
//ostream * const CCSLog::pomDefaultOStream = 0;


void CCSLog::SetAppName(CString opName)
{
	strcpy(pcmAppName, opName);
}



CCSLog::CCSLog(const char *pcpConfigFile, const char *pcpAppName )
 : pomOStream(pomDefaultOStream)
{
  // DebugFilter(None, "ccslog"); // debug class

  // set defaults: filter everything
  DebugFilter(All);
  TraceFilter(true);

	if(pcpAppName != NULL)
  		strcpy(pcmAppName, pcpAppName);
	
	char pclConfigPath[142];
	char pclLogConfig[142];

    // retrieve the configuration path from OS environment
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

    //BCH8064 sprintf(pclLogConfig,"C:\\TMP\\%s.Log",pcmAppName);
    sprintf(pclLogConfig,"%s\\%s.Log", GetTmpPath(), pcmAppName);


  if (pcpConfigFile)
    Load(pcpConfigFile);
  else
    Load(pclLogConfig);

    Trace("START1","Test Start %d",1);
    Trace("START2","Test Start %d",2);

    Debug("START1",CCSLog::Most,(const char *)"Test Debug Most Start %d",1);
    Debug("START2",CCSLog::Some,(const char *)"Test Debug Some Start %d",2);
    Debug("START3",CCSLog::None,(const char *)"Test Debug None Start %d",3);
    Error("START1","Test Error Start %d",1);

  //InitLogging();

  Debug("*", Some, *this); // view configuration
}

// gets the path of the c:\tmp directory which can be specified in the TMP or TEMP environment variable
CString CCSLog::GetTmpPath(void)
{
	char *pclTmpPath = NULL;
	CString olTmpPath = "C:\\TMP";
    if((pclTmpPath = getenv("UFISTMP")) != NULL)
	{
		olTmpPath = CString(pclTmpPath);
	}
//    else if((pclTmpPath = getenv("TMP")) != NULL)
//	{
//		olTmpPath = CString(pclTmpPath);
//	}
//	else if((pclTmpPath = getenv("TEMP")) != NULL)
//	{
//		olTmpPath = CString(pclTmpPath);
//	}

	return olTmpPath;
}

// gets the path of the c:\tmp directory which can be specified in the TMP or TEMP environment variable
// adds additional text specified by the user to the end of the path
CString CCSLog::GetTmpPath(const char *pcpAdditionalText)
{
	CString olTmpPath = GetTmpPath() + CString(pcpAdditionalText);
	return olTmpPath;
}

// gets the path of the c:\ufis\system directory which can be specified in the CEDA environment variable
CString CCSLog::GetUfisSystemPath(void)
{
	char *pclUfisSystemPath = NULL;
	CString olUfisSystemPath = "C:\\UFIS\\SYSTEM";
    if((pclUfisSystemPath = getenv("UFISSYSTEM")) != NULL)
	{
		olUfisSystemPath = CString(pclUfisSystemPath);
	}
    else if((pclUfisSystemPath = getenv("CEDA")) != NULL)
	{
		if((pclUfisSystemPath = getenv("CEDA")) != NULL)
		{
			olUfisSystemPath = CString(pclUfisSystemPath);
		}
		olUfisSystemPath.MakeUpper();
		olUfisSystemPath.TrimRight("\\CEDA.INI");
	}
	return olUfisSystemPath;
}

// gets the path of the c:\tmp directory which can be specified in the CEDA environment variable
// adds additional text specified by the user to the end of the path
CString CCSLog::GetUfisSystemPath(const char *pcpAdditionalText)
{
	CString olUfisSystemPath = GetUfisSystemPath() + CString(pcpAdditionalText);
	return olUfisSystemPath;
}

CCSLog::~CCSLog()
{
	
  if (pomOStream)
  {
    Debug("ccslog", All, "CCSLog::~CCSLog(): log ends here.");
    if (pomOStream != pomDefaultOStream)
      delete pomOStream;
  }
}

void CCSLog::Debug(const char *pcpKey, Level epLevel,
		   const CObject& ropObj) const
{
  if (!pomOStream || epLevel <= DebugFilter(pcpKey))
    return;

  Debug(pcpKey, epLevel, "Object dump for key `%s' follows...", pcpKey);
  *pomOStream << pcpKey << ".CCSObject::omRc = " << "RST(21.08.1997): CCSObject not used in new projects!!!"  << endl;

  //RST! pomOStream << pcpLabel << ".CCSObject::omRc = " << RC() << endl;
  //RST! ropObj.Dump(*pomOStream, pcpKey);
}

void CCSLog::Debug(const char *pcpKey, Level epLevel,
		   const char *pcpFormat ...) const
{
  if (!pomOStream || epLevel <= DebugFilter(pcpKey))
    return;

  va_list va;
  va_start(va, pcpFormat);
  ::msg2stream(*pomOStream, "Debug[" + TimeOfDayStamp() + "] ", pcpFormat, va);
  va_end(va);
}

void CCSLog::Snap(const char *pcpKey, Level epLevel,
		   const void *pvpAddr, int ipBytes, int ipRowLen) const
{
  if (!pomOStream || epLevel <= DebugFilter(pcpKey))
    return;

  Debug(pcpKey, epLevel, "Snap dump for key `%s' follows...", pcpKey);

  const unsigned char *pclAddr     = (const unsigned char *)pvpAddr;
  const unsigned char *clHexDigits = (unsigned char *)"0123456789abcdef";

  char clHexAddr[16];

  for (int ilI = 0; ipBytes > 0;
       ilI++, pclAddr += ipRowLen, ipBytes -= ipRowLen)
  {
    ::sprintf(clHexAddr, "%-x: ", (unsigned long)pclAddr);
    *pomOStream << clHexAddr;

    int ilRowLen = ipBytes < ipRowLen ? ipBytes : ipRowLen;
    int ilJ;

    for (ilJ = 0; ilJ < ilRowLen; ilJ++)
      *pomOStream << ' ' << clHexDigits[pclAddr[ilJ] / 16]
                         << clHexDigits[pclAddr[ilJ] % 16];
    for (; ilJ < ipRowLen; ilJ++)
      *pomOStream << "   ";

    *pomOStream << "  ";
    for (ilJ = 0; ilJ < ilRowLen; ilJ++)
      if (pclAddr[ilJ] >= ' ')
	*pomOStream << pclAddr[ilJ];
      else
	*pomOStream << '.';
    for (; ilJ < ipRowLen; ilJ++)
      *pomOStream << " ";
    
    *pomOStream << endl;
  }
}

void CCSLog::Trace(const char *pcpKey, const char *pcpFormat ...) const
{
  if (!pomOStream || TraceFilter(pcpKey))
    return;

  va_list va;
  va_start(va, pcpFormat);
  ::msg2stream(*pomOStream, "Trace[" + TimeOfDayStamp() + "] ", pcpFormat, va);
  va_end(va);
}

void CCSLog::Error(const char *pcpFormat ...) const
{
  if (!pomOStream)
    return;

  va_list va;
  va_start(va, pcpFormat);
  ::msg2stream(*pomOStream, "Error[" + TimeOfDayStamp() + "] ", pcpFormat, va);
  va_end(va);
}

void CCSLog::Dump(ostream& ropOs, const char *pcpLabel) const
{
  // dump base classes
  //RST! CCSObject::Dump(ropOs, pcpLabel);

  ropOs << pcpLabel << ".CCSLog::omTable =" << endl;
  for (unsigned ilI = 0; ilI < omTable.Size(); ilI++)
  {
    ropOs.width(2);
    ropOs << ilI+1 << ". key: ";
    ropOs.width(16);
    ropOs              << omTable[ilI].Key()   << ", " << "filters: "
          << "trace "  << omTable[ilI].Trace() << ", "
          << "debug "  << (int)omTable[ilI].Debug() << endl;
  }
}

int CCSLog::GetIndex(const char *pcpKey) const
{
  unsigned ilI = 0;
  while (ilI < omTable.Size() && omTable[ilI].Key() != pcpKey)
    ++ ilI;

  if (ilI == omTable.Size())
    return -1;
  return ilI;
}

int CCSLog::InsertKey(const char *pcpKey)
{
  Trace("ccslog", "CCSLog::InsertKey(%s)", pcpKey);

  omTable[omTable.Size()].Key() = pcpKey;

  return omTable.Size()-1;
}

void CCSLog::TraceFilter(bool bmTrace, const char *pcpKey)
{
  Trace("ccslog", "CCSLog::TraceFilter(%d, %s)", bmTrace, pcpKey);

  int ilIndex = GetIndex(pcpKey);
  if (ilIndex < 0)
    ilIndex = InsertKey(pcpKey);
  if (ilIndex < 0)
     return;
  omTable[ilIndex].Trace() = bmTrace;
}

bool CCSLog::TraceFilter(const char *pcpKey) const
{
  int ilIndex = GetIndex(pcpKey);
  if (ilIndex < 0)
     return true;
  return omTable[ilIndex].Trace();
}

void CCSLog::DebugFilter(Level epLevel, const char *pcpKey)
{
  Trace("ccslog", "CCSLog::DebugFilter(%d, %s)", epLevel, pcpKey);

  int ilIndex = GetIndex(pcpKey);
  if (ilIndex < 0)
    ilIndex = InsertKey(pcpKey);
  if (ilIndex < 0)
     return;
  omTable[ilIndex].Debug() = epLevel;
}

CCSLog::Level CCSLog::DebugFilter(const char *pcpKey) const
{
  int ilIndex = GetIndex(pcpKey);
  if (ilIndex < 0)
    return All;
  return omTable[ilIndex].Debug();
}

void CCSLog::Load(const char *pcpConfigFile)
{
  // Level elDbg = None;
  Level elDbg = All; // debug

  Trace("ccslog", "CCSLog::Load(%s)\n", pcpConfigFile);

  ifstream olConfig(pcpConfigFile,ios::nocreate|ios::in);  

  char clLine[256];
  int ilRow = 0;

  ostream *polNewOStream = 0;

  bool blError = false;

  while (olConfig.good() && !olConfig.eof()) // read and parse clLine
  {
    int ilCol;

    do
    {
      olConfig.getline(clLine, 256);
      if (!olConfig.good())
	break;
      ilCol = 0;
      ++ ilRow;

      while (clLine[ilCol] == ' ' || clLine[ilCol] == '\t') // skip white
	++ ilCol;
    } while (clLine[ilCol] == '#' || clLine[ilCol] == '\0');

    // Debug("ccslog", elDbg, "CCSLog::Load(..): got line: `%s'", clLine);

    if (olConfig.bad())
    {
      Error("CCSLog::Load(..): can't read %s, row %d", pcpConfigFile, ilRow);
      blError = true;
      break;
    }
    if (olConfig.eof())
    {
      Debug("ccslog", elDbg, "CCSLog::Load(..): end of file reached");
      break;
    }

    char *pclKey = &clLine[ilCol];

    char *pclDot = ::strchr(&clLine[ilCol], '.');
    if (!pclDot)
    {
      Error("CCSLog::Load(..): missing '.', row %d, col %d", ilRow, ilCol);
      blError = true;
      break;
    }
    *pclDot = '\0';
    // Debug("ccslog", elDbg, "CCSLog::Load(..): got key `%s'", pclKey);

    ilCol += ::strlen(pclKey) + 1; // first char after '.'

    // go to end of token, replace first char with 0
    char *pclP = &clLine[ilCol];
    while (isalpha(*pclP))
      ++ pclP;
    char clSave = *pclP;
    *pclP = 0;

    // get one of 'Debug', 'Trace', 'Output':
    int ilToken = 0;

    if (stricmp(&clLine[ilCol],"trace") == 0)
    {
      ilToken = 1;
      ilCol  += 5;
    }
    else if (stricmp(&clLine[ilCol],"debug") == 0)
    {
      ilToken = 2;
      ilCol  += 5;
    }
    else if (stricmp(&clLine[ilCol],"output") == 0)
    {
      ilToken = 3;
      ilCol  += 6;
    }
    else
    {
      Error("bad token, row %d, col %d", ilRow, ilCol);
      blError = true;
      break;
    }

    // restore end marker
    *pclP = clSave;

    // Debug("ccslog", elDbg, "CCSLog::Load(..): got token %d", ilToken);

    while (clLine[ilCol] == ' ' || clLine[ilCol] == '\t') // skip white
      ++ ilCol;

    if (clLine[ilCol] != ':' && clLine[ilCol] != '=')
    {
      Error("':' expected, row %d, col %d", ilRow, ilCol);
      blError = true;
      break;
    }
    ++ ilCol;

    while (clLine[ilCol] == ' ' || clLine[ilCol] == '\t') // skip white
      ++ ilCol;

    char *pclValue = pclP = &clLine[ilCol];
    while (*pclP && *pclP != ' ' && *pclP != '\t')
      ++ pclP;
    *pclP = 0;
    // Debug(elDbg, elDbg, "CCSLog::Load(..): got value `%s'", pclValue);
    
    switch(ilToken)
    {
      case 1: // Trace
      {
	if (stricmp(pclValue,"false") == 0)
	  TraceFilter(false, pclKey);
	else if (stricmp(pclValue,"true") == 0)
	  TraceFilter(true, pclKey);
	else
	{
	  Error("CCSLog::Load(..): bad value, row %d, col %d", ilRow, ilCol);
	  blError = true;
	}
      }
      break;
      case 2: // Debug
      {
	if      (stricmp(pclValue,"all") == 0)
	  DebugFilter(All,  pclKey);
	else if (stricmp(pclValue,"most") == 0)
	  DebugFilter(Most, pclKey);
	else if (stricmp(pclValue,"some") == 0)
	  DebugFilter(Some, pclKey);
	else if (stricmp(pclValue,"none") == 0)
	  DebugFilter(None, pclKey);
	else
	{
	  Error("CCSLog::Load(..): bad value, row %d, col %d", ilRow, ilCol);
	  blError = true;
	}
      }
      break;
      case 3: // Output
      {
	if (::strcmp(pclKey, "*"))
	{
	  Error("CCSLog::Load(..): \
separating output not yet supported, row %d: `%s'", ilRow, clLine);
	  blError = true;
	  break;
	}
	if (pomOStream != pomDefaultOStream)
	{
	  Error("CCSLog::Load(..): \
multiple `*.Output: ...', row %d: `%s'", ilRow, clLine);
	  blError = true;
	  break;
	}

	polNewOStream = new ofstream(pclValue);
	if (!polNewOStream || !polNewOStream->good())
	{
	  if (polNewOStream)
	  {
	    delete polNewOStream;
	    polNewOStream = 0;
	  }
	  Error("CCSLog::Load(..): can't open `%s'", pclValue);
	  blError = true;
	  break;
	}
	Debug("ccslog", elDbg, "CCSLog::Load(..): logging to `%s'", pclValue);
      }
      break;
    } // end of switch(token)
    if (blError)
      break;
  } // end of while

  if (polNewOStream)
  {
    Debug("ccslog", All, "CCSLog::Load(..): log ends here.");
    if (pomOStream != pomDefaultOStream)
      delete pomOStream;
    pomOStream = polNewOStream;
  }

  //RST! RC(blError ? RCFailure : RCSuccess);
}


