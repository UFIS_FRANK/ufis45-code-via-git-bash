// NameConfigurationDlg.h: Schnittstelle f�r die Klasse NameConfigurationDlg.
//
//////////////////////////////////////////////////////////////////////

#include "resource.h"

#if !defined(AFX_NAMECONFIGURATIONDLG_H__89D1729B_30D8_4BEA_90F7_F0F653F8D235__INCLUDED_)
#define AFX_NAMECONFIGURATIONDLG_H__89D1729B_30D8_4BEA_90F7_F0F653F8D235__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class NameConfigurationDlg : public CDialog  
{
public:
	void SetDatabaseFieldNames(CStringArray opArr);
	CString GetConfigStringSeparator();
	void SetConfigStringSeparator(CString opSeparator);
	//
	// Delivers the formatted string depending on the set configuration-string.
	// (Set the config-string using "SetConfigString(CString opConfigString)")
	//
	CString GetNameString(char* cpFirstName, char* cpLastName, char* cpNickName, char* cpInitial, char* cpPeno);

	// Allows the calling environment to set the maximum name length, names
	// longer than this length will be truncated - do not call this if the
	// name length is unlimited.
	//
	void SetMaxNameLength(int ipMaxNameLen);

	//
	// Get the (new) config-string after the user closed the dialog with "OK".
	// If the user closed with "Cancel", the old one will be kept
	//
	CString GetConfigString();

	//
	// set the configuration-string if you just want to get a formatted string
	// without showing the window
	//
	void SetConfigString(CString opConfigString);

	// The strings in the CStringArray must be ordered:
	// 1) IDD_NAME_CONFIGURATION - dialog title
	// 2) IDC_S_FINM - first name
	// 3) IDC_S_LANM - last name
	// 4) IDC_S_SHNM - shortname/nickname
	// 5) IDC_S_PERC - initials
	// 6) IDC_S_PENO - personnal no.
	// 7) IDC_S_FIELD_NAME - "field:"
	// 8) IDC_S_NO_CHARS - "No. of chars:"
	// 9) IDC_S_SUFFIX - "Suffix:"
	//10) IDC_S_PREVIEW - "Preview:"
	NameConfigurationDlg(CStringArray* prpStrings, CString opConfigString, CWnd* pParent = NULL);
	virtual ~NameConfigurationDlg();

	//{{AFX_DATA(NameConfigurationDlg)
	enum { IDD = IDD_NAME_CONFIGURATION };
	CComboBox	m_Combo5;
	CComboBox	m_Combo4;
	CComboBox	m_Combo3;
	CComboBox	m_Combo2;
	CComboBox	m_Combo1;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(NameConfigurationDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung
	//}}AFX_VIRTUAL

protected:
	// Generierte Message-Map-Funktionen
	//{{AFX_MSG(NameConfigurationDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnSelchangeCombo1();
	afx_msg void OnSelchangeCombo2();
	afx_msg void OnSelchangeCombo3();
	afx_msg void OnSelchangeCombo4();
	afx_msg void OnSelchangeCombo5();
	afx_msg void OnChangeFinm();
	afx_msg void OnChangeLanm();
	afx_msg void OnChangePerc();
	afx_msg void OnChangeShnm();
	afx_msg void OnChangeEPeno();
	afx_msg void OnChangeEChar1();
	afx_msg void OnChangeEChar2();
	afx_msg void OnChangeEChar3();
	afx_msg void OnChangeEChar4();
	afx_msg void OnChangeEChar5();
	afx_msg void OnChangeESuffix1();
	afx_msg void OnChangeESuffix2();
	afx_msg void OnChangeESuffix3();
	afx_msg void OnChangeESuffix4();
	afx_msg void OnChangeESuffix5();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CString omConfigSeparator;
	void InitConfigString(CString opConfigString);
	void BuildConfigString();
	void InitLabels(CStringArray *ropStrings);
	CStringArray omStringArr;
	CStringArray omFieldArray;
	CStringArray omConfigStringArr;
	void SetComboSelection(CComboBox* prpCombo, CString opFieldName);
	CString GetFormatedString(char* pcpText, char* pcpLength, char* pcpSuffix);
	bool CheckInput();
	void InitCombos();
	CString omConfigString;
	void RebuildPreview();
	CString olFINM;
	CString olLANM;
	CString olPERC;
	CString olSHNM;
	CString olPENO;
	int imMaxNameLen;

};

//{{AFX_INSERT_LOCATION}}

#endif // !defined(AFX_NAMECONFIGURATIONDLG_H__89D1729B_30D8_4BEA_90F7_F0F653F8D235__INCLUDED_)
