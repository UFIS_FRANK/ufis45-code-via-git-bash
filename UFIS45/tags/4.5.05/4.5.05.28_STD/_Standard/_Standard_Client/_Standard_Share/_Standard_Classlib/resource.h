//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by ClassLib.rc
//
#define IDD_NAME_CONFIGURATION          7000
#define IDC_S_FINM_NAMECONFIG           7001
#define IDC_S_LANM_NAMECONFIG           7002
#define IDC_E_LANM_NAMECONFIG           7003
#define IDC_E_FINM_NAMECONFIG           7004
#define IDC_S_FIELD_NAME_NAMECONFIG     7005
#define IDC_C_SEL1_NAMECONFIG           7006
#define IDC_E_CHAR1_NAMECONFIG          7007
#define IDC_S_PERC_NAMECONFIG           7008
#define IDC_S_SHNM_NAMECONFIG           7009
#define IDC_E_SHNM_NAMECONFIG           7010
#define IDC_E_PERC_NAMECONFIG           7011
#define IDC_S_NO_CHARS_NAMECONFIG       7012
#define IDC_S_SUFFIX_NAMECONFIG         7013
#define IDC_C_SEL2_NAMECONFIG           7014
#define IDC_C_SEL3_NAMECONFIG           7015
#define IDC_C_SEL4_NAMECONFIG           7016
#define IDC_C_SEL5_NAMECONFIG           7017
#define IDC_E_PENO_NAMECONFIG           7018
#define IDC_E_SUFFIX1_NAMECONFIG        7019
#define IDC_E_CHAR2_NAMECONFIG          7020
#define IDC_E_SUFFIX2_NAMECONFIG        7021
#define IDC_E_CHAR3_NAMECONFIG          7022
#define IDC_E_SUFFIX3_NAMECONFIG        7023
#define IDC_E_CHAR4_NAMECONFIG          7024
#define IDC_E_SUFFIX4_NAMECONFIG        7025
#define IDC_S_PREVIEW_NAMECONFIG        7026
#define IDC_E_PREVIEW_NAMECONFIG        7027
#define IDC_E_CHAR5_NAMECONFIG          7028
#define IDC_E_SUFFIX5_NAMECONFIG        7029
#define IDC_S_PENO_NAMECONFIG           7030
#define IDCANCEL_NAMECONFIG             7031

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        703
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         7032
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
