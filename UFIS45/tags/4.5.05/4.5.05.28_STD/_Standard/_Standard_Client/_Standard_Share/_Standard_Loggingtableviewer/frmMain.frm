VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmMain 
   Caption         =   "Viewer for logging events ..."
   ClientHeight    =   4845
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10260
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4845
   ScaleWidth      =   10260
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtAFTFIELD 
      Height          =   315
      Left            =   4020
      TabIndex        =   4
      Top             =   4500
      Width           =   1875
   End
   Begin TABLib.TAB tabEvents 
      Height          =   4155
      Left            =   0
      TabIndex        =   3
      Top             =   360
      Width           =   10215
      _Version        =   65536
      _ExtentX        =   18018
      _ExtentY        =   7329
      _StockProps     =   0
   End
   Begin VB.CommandButton cmdLoad 
      Caption         =   "&Load Data"
      Height          =   315
      Left            =   7020
      TabIndex        =   2
      Top             =   4500
      Width           =   1155
   End
   Begin VB.TextBox txtAFTURNO 
      Height          =   315
      Left            =   1140
      TabIndex        =   0
      Top             =   4500
      Width           =   1875
   End
   Begin VB.Label lblRecords 
      BackColor       =   &H80000008&
      Caption         =   "Records:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FF00&
      Height          =   255
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Visible         =   0   'False
      Width           =   10635
   End
   Begin VB.Label lblAftField 
      Caption         =   "AFT Field:"
      Height          =   195
      Left            =   3060
      TabIndex        =   5
      Top             =   4560
      Width           =   855
   End
   Begin VB.Label lblUrno 
      Caption         =   "AFT Urno:"
      Height          =   195
      Left            =   180
      TabIndex        =   1
      Top             =   4560
      Width           =   855
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdLoad_Click()
    Dim cnt As Long
    Dim i As Long
    Dim ilItem As Long
    Dim strTmp As String
    Dim strLines As String
    Dim strFields As String
    Dim strValues As String
    Dim strData As String
    Dim myD As Date
    Dim myTab As TABLib.TAB
    Dim strFldList As String
    
    frmHiddenData.LoadForAft (txtAFTURNO)
    
    tabEvents.ResetContent
    strLines = frmHiddenData.tabData(1).GetLinesByColumnValue(1, strAFTFIELD, 0)
    If Len(strLines) > 0 Then
        ilItem = GetRealItem(strLines, 0, ",")
        strTmp = "Field: " + strAFTFIELD + "=" + frmHiddenData.tabData(1).GetFieldValue(ilItem, "ADDI")
    End If
    tabEvents.HeaderString = "Urno,Table,Cmd,User,Time," + strTmp 'Info"
    tabEvents.HeaderLengthString = "70,50,40,50,90,400"
    tabEvents.FontName = "Arial"
    tabEvents.HeaderFontSize = 14
    
    tabEvents.ShowHorzScroller True
    tabEvents.EnableHeaderSizing True
    Set myTab = frmHiddenData.tabData(2)
    cnt = myTab.GetLineCount
    For i = 0 To cnt - 1
        strData = myTab.GetFieldValue(i, "URNO") + ","
        strData = strData + myTab.GetFieldValue(i, "STAB") + ","
        strData = strData + myTab.GetFieldValue(i, "SFKT") + ","
        strData = strData + myTab.GetFieldValue(i, "USEC") + ","
        myD = CedaFullDateToVb(myTab.GetFieldValue(i, "TIME"))
        strData = strData + Format(myD, "DD.MM.YY-HH:MM") + ","
        
        strFldList = myTab.GetFieldValue(i, "FLST")
        strFldList = Replace(strFldList, Chr(30), ",")
        ilItem = GetItemNo(strFldList, strAFTFIELD) - 1
        If ilItem > -1 Then
            strValues = myTab.GetFieldValue(i, "FVAL")
            strValues = Replace(strValues, Chr(30), ",")
            strTmp = GetRealItem(strValues, ilItem, ",")
            strData = strData + strTmp
            tabEvents.InsertTextLine strData, False
        End If
    Next i
    tabEvents.Refresh
End Sub

Private Sub Form_Load()
    Dim ilLeft As Integer
    Dim ilTop As Integer
    Dim blStandaloneOrDebug As Boolean
    Dim strCommand As String
    Dim ilCnt As Long
    
    strServer = GetIniEntry("", "LOGTABVIEWER", "GLOBAL", "HOSTNAME", "")
    strHOPO = GetIniEntry("", "LOGTABVIEWER", "GLOBAL", "HOMEAIRPORT", "")
    strAFTFIELDS = GetIniEntry("", "LOGTABVIEWER", "GLOBAL", "AFTFIELDS", "")
    
    tabEvents.ResetContent
    tabEvents.HeaderString = "Urno,Table,Cmd,User,Time,Change Info" 'Info"
    tabEvents.HeaderLengthString = "70,50,40,50,90,400"
    tabEvents.FontName = "Arial"
    tabEvents.HeaderFontSize = 14
    ' look after the command (afttab.urno) we got by the app we are started from
    ' The format for the commandline parameter can be as follows:
    ' DBG   TAB    URNO     FIELD to show
    ' DEBUG,AFTTAB,92403912,CHGI
    ' TAB    URNO     FIELD to show
    ' AFTTAB,92403912,CHGI
    ' <nothing>
    strCommand = Command()
    ilCnt = ItemCount(strCommand, ",")
    If strCommand = "" Then
        strUseAppFor = "COMMON"
        bgDebug = False
    Else
        If InStr(strCommand, "DEBUG") = 0 Then
            bgDebug = False
        Else
            bgDebug = True
        End If
        If InStr(strCommand, "AFTTAB") = 0 Then
            strUseAppFor = "COMMON"
        Else
            strUseAppFor = "AFTTAB"
        End If
        If ilCnt = 4 Then
            txtAFTURNO.Text = GetRealItem(strCommand, 2, ",")
            strAFTFIELD = GetRealItem(strCommand, 3, ",")
            txtAFTFIELD = strAFTFIELD
        End If
        If ilCnt = 3 Then
            txtAFTURNO.Text = GetRealItem(strCommand, 1, ",")
            strAFTFIELD = GetRealItem(strCommand, 2, ",")
        End If
    End If
    ilLeft = GetSetting(AppName:="LogTabViewerMain", _
                        section:="Startup", _
                        Key:="frmMainLeft", _
                        Default:=-1)
    ilTop = GetSetting(AppName:="LogTabViewerMain", _
                        section:="Startup", _
                        Key:="frmMainTop", _
                        Default:=-1)
    If ilTop = -1 Or ilLeft = -1 Then
        Me.Move 0, 0
    Else
        Me.Move ilLeft, ilTop
    End If
    
    
    ' if we are in "runtime", start loading the data immediately
    If bgDebug = True Then
        frmHiddenData.Show
    End If
    frmEventDetails.Show
    
    If strUseAppFor = "COMMON" Then
        txtAFTURNO.Visible = False
        txtAFTFIELD.Visible = False
        lblAftField.Visible = False
        cmdLoad.Visible = False
        lblUrno.Visible = False
        frmLoggingTables.Show
        frmLoadDefinition.Show
    Else
        cmdLoad_Click
        txtAFTURNO.Visible = True
        cmdLoad.Visible = True
        lblUrno.Visible = True
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim ilLeft As Integer
    Dim ilTop As Integer

    ilLeft = Me.left
    ilTop = Me.top
    SaveSetting "LogTabViewerMain", _
                "Startup", _
                "frmMainLeft", _
                ilLeft
    SaveSetting "LogTabViewerMain", _
                "Startup", _
                "frmMainTop", ilTop

    Unload frmEventDetails
    Unload frmHiddenData
    Unload frmLoggingTables
    Unload frmLoadDefinition
    End
End Sub

Private Sub tabEvents_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim strUrno As String
    If LineNo >= 0 Then
        strUrno = tabEvents.GetColumnValue(LineNo, 0)
        frmEventDetails.SetData strUrno
    End If
End Sub

Private Sub txtAFTFIELD_Change()
    strAFTFIELD = txtAFTFIELD
End Sub
'====================================================
' Prepares the tab from the logging table information
'====================================================
Public Sub SetGridFromLogTable()
    Dim cnt As Long
    Dim i As Long
    Dim ilItem As Long
    Dim strTmp As String
    Dim strLines As String
    Dim strFields As String
    Dim strValues As String
    Dim strData As String
    Dim myD As Date
    Dim myTab As TABLib.TAB
    Dim strFldList As String
    Dim strFilterField As String
    Dim curr As Long
    Dim strBuffer As String
    Dim strUser As String
    Dim strCommand As String
    
    strUser = frmLoadDefinition.cbUsers.Text
    strCommand = frmLoadDefinition.cbType.Text
    lblRecords.Visible = True
    strFilterField = frmLoadDefinition.cbField.Text
    If strFilterField = "**ALL FIELDS**" Then
        strFilterField = ""
    End If
    tabEvents.ResetContent
    If strFilterField <> "" Then
        strLines = frmHiddenData.tabData(1).GetLinesByColumnValue(1, strAFTFIELD, 0)
        If Len(strLines) > 0 Then
            ilItem = GetRealItem(strLines, 0, ",")
            strTmp = "Field: " + strAFTFIELD + "=" + frmHiddenData.tabData(1).GetFieldValue(ilItem, "ADDI")
        End If
    End If
    If strFilterField <> "" Then
        tabEvents.HeaderString = "Urno,Table,Cmd,User,Time," + strTmp
    Else
        tabEvents.HeaderString = "Urno,Table,Cmd,User,Time,Change Info"
    End If
    tabEvents.HeaderLengthString = "70,50,40,50,90,400"
    tabEvents.FontName = "Arial"
    tabEvents.HeaderFontSize = 14
    
    tabEvents.ShowHorzScroller True
    tabEvents.EnableHeaderSizing True
    Set myTab = frmHiddenData.tabData(2)
    cnt = myTab.GetLineCount
    curr = 0
    For i = 0 To cnt - 1
        strData = myTab.GetFieldValue(i, "URNO") + ","
        strData = strData + myTab.GetFieldValue(i, "STAB") + ","
        strData = strData + myTab.GetFieldValue(i, "SFKT") + ","
        If (strUser = "**ALL USERS**" Or strUser = myTab.GetFieldValue(i, "USEC")) And _
            (strCommand = "**ALL COMMANDS**" Or strCommand = myTab.GetFieldValue(i, "SFKT")) Then
            strData = strData + myTab.GetFieldValue(i, "USEC") + ","
            myD = CedaFullDateToVb(myTab.GetFieldValue(i, "TIME"))
            strData = strData + Format(myD, "DD.MM.YY-HH:MM") + ","
            
            strFldList = myTab.GetFieldValue(i, "FLST")
            strFldList = Replace(strFldList, Chr(30), ",")
            If strFilterField <> "" Then
                ilItem = GetItemNo(strFldList, strFilterField) - 1
                If ilItem > -1 Then
                    strValues = myTab.GetFieldValue(i, "FVAL")
                    strValues = Replace(strValues, Chr(30), ",")
                    strTmp = GetRealItem(strValues, ilItem, ",")
                    strData = strData + strTmp
                    strBuffer = strBuffer + strData + vbLf
                    curr = curr + 1
                    If curr > 1000 Then
                        If strBuffer <> "" Then
                            tabEvents.InsertBuffer strBuffer, vbLf
                            strBuffer = ""
                            curr = 0
                        End If
                    End If
                End If
            Else
                strValues = myTab.GetFieldValue(i, "FVAL")
                strValues = Replace(strValues, Chr(30), ";")
                strData = strData + strValues
                tabEvents.InsertTextLine strData, False
                curr = curr + 1
                If curr > 1000 Then
                    tabEvents.InsertBuffer strBuffer, vbLf
                    strBuffer = ""
                    curr = 0
                End If
            End If
        End If
        If i Mod 500 = 0 Then
            lblRecords = "Records: Scanned " & i & " Inserted: " & tabEvents.GetLineCount
            lblRecords.Refresh
            DoEvents
        End If
    Next i
    tabEvents.InsertBuffer strBuffer, vbLf
    tabEvents.Refresh
    lblRecords = "Records: " & tabEvents.GetLineCount & " prepared"
    lblRecords.Refresh
End Sub
