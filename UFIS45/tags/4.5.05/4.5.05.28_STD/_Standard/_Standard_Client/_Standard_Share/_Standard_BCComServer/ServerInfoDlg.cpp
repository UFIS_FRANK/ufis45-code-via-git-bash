// ServerInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "bccomserver.h"
#include "ServerInfoDlg.h"
#include "MonitorDlg.h"
#include "BcComAtl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CServerInfoDlg dialog


CServerInfoDlg::CServerInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CServerInfoDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CServerInfoDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	char pclConfigPath[256];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	imTcpPort	   = ::GetPrivateProfileInt("GLOBAL", "BCTCPPORT",3358, pclConfigPath);
					 ::GetPrivateProfileString("GLOBAL", "HOSTNAME", "", pcmHostName, sizeof pcmHostName, pclConfigPath);

}


void CServerInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CServerInfoDlg)
	DDX_Control(pDX, IDC_LIST1, m_InfoList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CServerInfoDlg, CDialog)
	//{{AFX_MSG_MAP(CServerInfoDlg)
	ON_BN_CLICKED(IDC_BUTTON_STOPTHREAD, OnButtonStopthread)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CServerInfoDlg message handlers

BOOL CServerInfoDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_InfoList.InsertColumn(0,"Queue  Id",LVCFMT_CENTER,70);
	m_InfoList.InsertColumn(1,"Thread Id",LVCFMT_CENTER,70);
	m_InfoList.InsertColumn(2,"Socket Id",LVCFMT_CENTER,70);
	m_InfoList.InsertColumn(3,"Client IP",LVCFMT_CENTER,70);
	m_InfoList.InsertColumn(4,"Client IP",LVCFMT_CENTER,70);
	m_InfoList.InsertColumn(5,"Items sent",LVCFMT_CENTER,70);
	m_InfoList.InsertColumn(6,"Items total",LVCFMT_CENTER,70);


	CStringArray olConnInfo;
	if (CBcComAtl::GetServerInfo(this->pcmHostName,this->imTcpPort,olConnInfo))
	{
		for (int i = 0; i < olConnInfo.GetSize(); i++)
		{
			CStringArray olSingleInfo;
			ExtractItemList(olConnInfo[i],&olSingleInfo,',');

			int iItem = 0;
			for (int j = 0; j < olSingleInfo.GetSize(); j++)
			{
				LVITEM olItem;

				if (j == 0)
				{
					olItem.mask		= LVIF_TEXT;
					olItem.iItem	= iItem;
					olItem.iSubItem = j;
					olItem.pszText  = olSingleInfo[j].GetBuffer(0);
					olItem.cchTextMax = olSingleInfo[j].GetLength();
					iItem = m_InfoList.InsertItem(&olItem);
				}
				else
				{
					olItem.mask		= LVIF_TEXT;
					olItem.iItem	= iItem;
					olItem.iSubItem = j;
					olItem.pszText  = olSingleInfo[j].GetBuffer(0);
					olItem.cchTextMax = olSingleInfo[j].GetLength();
					m_InfoList.SetItem(&olItem);
				}
			}
		}

	}
	else
	{
		MessageBox("No server information available");
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

int CServerInfoDlg::ExtractItemList(CString opSubString, CStringArray *popStrArray, char cpTrenner)
{
	int ilSepaLen = 1;//
	int ilCount = 0;
	char *currPtr;
	char *ptrOrig;
	char *pclTextLine;
	char pclSepa[100]="";
	sprintf(pclSepa, "%c", cpTrenner);
	pclTextLine = (char*)malloc((size_t)opSubString.GetLength()+1);
	memset((void*)pclTextLine, 0x00, opSubString.GetLength()+1);
	strcpy(pclTextLine, opSubString.GetBuffer(0));
	ptrOrig = pclTextLine;
	currPtr = strstr(pclTextLine , pclSepa);
	while (currPtr != NULL)
	{
		*currPtr = '\0';
		popStrArray->Add(pclTextLine );
		ilCount++;
		pclTextLine  = currPtr + ilSepaLen;
		currPtr		= strstr(pclTextLine , pclSepa);
	}
	if(strcmp(pclTextLine , "") != 0)
	{
		popStrArray->Add(pclTextLine );
		ilCount++;
	}
	free(ptrOrig);
	return ilCount;
}

void CServerInfoDlg::OnButtonStopthread() 
{
	// TODO: Add your control notification handler code here

	POSITION pos = this->m_InfoList.GetFirstSelectedItemPosition();
	if (pos == NULL)
		return;

	while (pos)
	{
		int nItem = this->m_InfoList.GetNextSelectedItem(pos);
		CString olQueueId = this->m_InfoList.GetItemText(nItem,0);
		if (olQueueId.GetLength() > 0)
		{
			CBcComAtl::StopConnection(this->pcmHostName,this->imTcpPort,olQueueId);
		}
	}
}
