// CCSGlobl.h: interface for the CCSGlobl class.
//
//////////////////////////////////////////////////////////////////////

// modified by MNE, March 1999:

// REMARK:	Most of the structs used anywhere in the program can be found in this file.
//			There's one major exception: All structs that have to do with views
//			are contained in CedaCfgData. 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *	BCProxy application
 *
 *  
 *
 *	ABB Airport Technologies AAT/I 1999
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *		Change history
 *		--------------
 *
 *		01/08/00	CLA/MWO
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */



#if !defined(AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_)
#define AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_

#ifndef __CCSGLOBL_H__
#define __CCSGLOBL_H__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <CCSDefines.h>
#include <ccsdragdropctrl.h>
#include <CCSTime.h>
#include <CCSPtrArray.h>
//#include "DataSet.h"
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include <CCSLog.h>
#include <CCSCedacom.h>
#include <CCSBcHandle.h>
#include <Ufis.h>
#include <CCSCedaData.h>
#include <BcPrxy.h>
#include <map>

//----------------------------------
//		forward declarations
//----------------------------------
class CCSBcHandle;
class CCSCedaCom;
class CCSDdx;
class CCSLog;
class CCSBcHandle;
class CedaBasicData;
class CInitialLoadDlg;
// actually the things contained in the local BasicData class should be part of CCSBasic (at least most of them)
class CCSBasic;		// (lib)
class CBasicData;	// (local)

class CBcPrxy;

extern bool bgApplicationHidden;

/*
 * Broadcast handling
 * cla:
 */
#define WM_BCADD						(WM_USER + 16)
#define CM_GANTT_MENU_EDIT				(WM_USER + 19)					
#define CM_GANTT_MENU_ACTIVATE			(WM_USER + 20)
#define CM_GANTT_MENU_OPERATIVE 		(WM_USER + 21)
#define CM_GANTT_MENU_PARTIALDEM		(WM_USER + 22)
#define CM_GANTT_MENU_CALCULATEDDEM		(WM_USER + 23)
#define CM_GANTT_MENU_DELETEBAR			(WM_USER + 24)
#define CM_GANTT_MENU_CONNECTBAR		(WM_USER + 25)
#define CM_GANTT_MENU_REMOVECON			(WM_USER + 26)
#define CM_INDEP_MENU_DELETEBAR			(WM_USER + 40)

struct PROXY_INSTANCES
{
	CBcPrxy *pProxy;
	CMapStringToPtr mapTabCmd;
};
extern CCSPtrArray<PROXY_INSTANCES> ogConnections;

typedef struct DDXTable
{
	int Ddx_Bc_Type[3];		// BC_XXX_NEW, BC_XXX_CHANGE, BC_XXX_DELETE
	int Ddx_Type[3];		// XXX_NEW, XXX_CHANGE, XXX_DELETE
	CString Table;			// table name

} DDX_TABLE;

//----------------------------------
//			global functions
//----------------------------------
CString GetResString(UINT ID);
bool GetString (CString opRefField, CString opIDS, CString &ropText);

void InitFont();
void DeleteBrushes();
void CreateBrushes();
CString GetDBString(CString ID);
bool SetDlgItemLangText ( CWnd *popWnd, UINT ilIDC, CString opRefField, 
						  CString opIDS );
bool SetWindowLangText ( CWnd *popWnd, CString opRefField, CString opIDS );
bool EnableDlgItem ( CWnd *popWnd, UINT ipIDC, BOOL bpEnable, BOOL bpShow );
BOOL MoveDlgItem ( CWnd *popWnd, UINT ipIDC, int x, int y );
BOOL ResizeDlgItem ( CWnd *popWnd, UINT ipIDC, int cx, int cy );


//--- enums

//--- class objects
extern CInitialLoadDlg *pogInitialLoad;
extern CCSBcHandle ogBcHandle;
extern CCSCedaCom ogCommHandler;  // The one and only CedaCom object
extern CCSDdx ogDdx;
extern CCSLog ogLog;
extern CedaBasicData ogBCD;
extern CCSBasic ogCCSBasic;
extern CBasicData ogBasicData;

extern CString ogAppName;
extern CString ogCallingApp;
extern char pcgUser[33];
extern char pcgPasswd[33];
extern char pcgConfigPath[142];
extern char pcgHome[4];
extern char pcgTableExt[10];

extern bool bgAdaptScale;
extern bool bgDebug;
extern bool bgNoScroll;
extern bool bgOnline;
extern bool bgIsButtonListMovable;
extern bool bgIsServicesOpen;
extern bool bgSeasonLocal;
extern bool bgUseResourceStrings;
extern BOOL bgIsInitialized;

extern int igDaysToRead;

//extern CBcPrxy *pogPrxy;
//extern CCSPtrArray<CBcPrxy> omProxyConnections;

struct CLIENT_FILTER
{
	CBcPrxy* pClient;
	CString		strTable;
	CString		strFromField;
	CString		strToField;
	CString		strFromValue;
	CString		strToValue;

};
extern CCSPtrArray<CLIENT_FILTER> ogClientFilterArray;

//----------------------------------
//			error handling
//----------------------------------
extern ofstream of_catch;

#define  CCS_TRY try{

#define  CCS_CATCH_ALL }\
						catch(...)\
						{\
						    char pclExcText[512]="";\
						    char pclText[1024]="";\
						    sprintf(pclText, "FILE:%s, Line: %s",\
							                 __FILE__, __LINE__);\
						    strcat(pclText, "LINE");\
						    sprintf(pclExcText, "FILE:%s, Line: %s", __FILE__, __LINE__);\
						    of_catch << pclExcText << endl;\
							if(::MessageBox(NULL, pclText, "Continue?", (MB_YESNO)) == IDNO)\
							{\
						       ExitProcess(0);\
							}\
						}




#endif //__CCSGLOBL_H__
#endif // !defined(AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_)
