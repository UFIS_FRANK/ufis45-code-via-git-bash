// BcProxy.h : main header file for the BCPROXY application
//

#if !defined(AFX_BCPROXY_H__68E25FE4_6790_11D4_903B_00010204AA51__INCLUDED_)
#define AFX_BCPROXY_H__68E25FE4_6790_11D4_903B_00010204AA51__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include <resource.h>		// main symbols
#include <BcProxy_i.h>

/////////////////////////////////////////////////////////////////////////////
// CBcProxyApp:
// See BcProxy.cpp for the implementation of this class
//

class CBcProxyApp : public CWinApp
{
public:
	CBcProxyApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBcProxyApp)
	public:
	virtual BOOL InitInstance();
		virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CBcProxyApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	BOOL m_bATLInited;
private:
	BOOL InitATL();
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BCPROXY_H__68E25FE4_6790_11D4_903B_00010204AA51__INCLUDED_)
