#if !defined(AFX_AATEDIT5_H__C386524B_FE62_11D4_90BC_00010204AA51__INCLUDED_)
#define AFX_AATEDIT5_H__C386524B_FE62_11D4_90BC_00010204AA51__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// aatedit5.h : main header file for AATEDIT5.DLL

#if !defined( __AFXCTL_H__ )
	#error include 'afxctl.h' before including this file
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CAatedit5App : See aatedit5.cpp for implementation.

class CAatedit5App : public COleControlModule
{
public:
	BOOL InitInstance();
	int ExitInstance();
};

extern const GUID CDECL _tlid;
extern const WORD _wVerMajor;
extern const WORD _wVerMinor;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AATEDIT5_H__C386524B_FE62_11D4_90BC_00010204AA51__INCLUDED)
