VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmChoiceList 
   Caption         =   "Choice List ..."
   ClientHeight    =   3510
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   3150
   Icon            =   "frmChoiceList.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   3510
   ScaleWidth      =   3150
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   1148
      TabIndex        =   1
      Top             =   3060
      Width           =   855
   End
   Begin TABLib.TAB tabList 
      Height          =   2835
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   3015
      _Version        =   65536
      _ExtentX        =   5318
      _ExtentY        =   5001
      _StockProps     =   0
   End
End
Attribute VB_Name = "frmChoiceList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public strFieldList As String
Public strHeaderList As String
Public imResultIndex As Integer
Public strHeaderLenList As String
Public strResultValue As String
Public dataTabIndex As Integer ' index of tabData from frmHiddenData
Public imSortIdx As Integer


Private Sub cmdOK_Click()
    Dim strLine As String
    Dim idx As Long
    
    idx = tabList.GetCurrentSelected
    If idx > -1 Then
        strResultValue = tabList.GetColumnValue(idx, imResultIndex)
    End If
    Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyEscape Then
        Unload Me
    End If

End Sub

Private Sub Form_Load()
    Dim i As Long
    Dim j As Long
    Dim currVal As String
    Dim idxSrc As Long
    Dim strNewVal As String
    
    tabList.ResetContent
    tabList.HeaderString = strHeaderList
    tabList.HeaderLengthString = strHeaderLenList
    strResultValue = ""
    For i = 0 To frmHiddenData.tabData(dataTabIndex).GetLineCount - 1
        For j = 0 To ItemCount(strFieldList, ",") - 1
            currVal = GetRealItem(strFieldList, CInt(j), ",")
            idxSrc = GetRealItemNo(frmHiddenData.tabData(dataTabIndex).HeaderString, currVal)
            strNewVal = strNewVal + frmHiddenData.tabData(dataTabIndex).GetColumnValue(i, idxSrc) + ","
        Next j
        tabList.InsertTextLine strNewVal, False
        strNewVal = ""
    Next i
    tabList.RedrawTab
    tabList.Sort imSortIdx, True, False
End Sub

Private Sub tabList_HitKeyOnLine(ByVal Key As Integer, ByVal LineNo As Long)
    If Key = vbKeyEscape Then
        Unload Me
    End If

End Sub

Private Sub tabList_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    cmdOK_Click
End Sub
