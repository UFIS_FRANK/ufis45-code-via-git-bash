﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Entities;
using Ufis.Broadcast;

namespace Ufis.Data
{
    /// <summary>
    /// Represents an abstract class of observable collection of entities.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class EntityCollectionBase<T> : ObservableCollectionEx<T>
    {
        private readonly IList<T> _removedItems;
        private readonly EntityDataContextBase _dataContext;

        /// <summary>
        /// Gets or sets whether to save removed items.
        /// </summary>
        public bool SaveRemovedItems { get; set; }

        /// <summary>
        /// Gets or sets the column list.
        /// </summary>
        public string ColumnList { get; set; }

        /// <summary>
        /// Gets the EntityDataContext of this instance.
        /// </summary>
        public EntityDataContextBase DataContext
        {
            get { return _dataContext; }
        }

        protected override void ClearItems()
        {
            if (SaveRemovedItems)
            {                
                foreach (T item in base.Items)
                {
                    object objItem = (object)item;
                    if (objItem is Entity)
                    {
                        Entity entity = (Entity)objItem;
                        entity.SetEntityState(Entity.EntityState.Deleted);

                        _removedItems.Add(item);
                    }
                }
            }

            base.ClearItems();
        }

        protected override void RemoveItem(int index)
        {
            if (SaveRemovedItems)
            {
                T item = base.Items[index];
                object objItem = (object)item;
                if (objItem is Entity)
                {
                    Entity entity = (Entity)objItem;
                    entity.SetEntityState(Entity.EntityState.Deleted);

                    _removedItems.Add(item);
                }                
            }

            base.RemoveItem(index);
        }

        /// <summary>
        /// Occurs when broadcast is received.
        /// </summary>
        public event EventHandler<BroadcastEventArgs> BroadcastReceived;

        protected virtual void OnBroadcastReceived(BroadcastEventArgs e)
        {
            EventHandler<BroadcastEventArgs> handler = BroadcastReceived;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Occurs after broadcast is processed or handled.
        /// </summary>
        public event EventHandler<BroadcastEventArgs> BroadcastHandled;

        protected virtual void OnBroadcastHandled(BroadcastEventArgs e)
        {
            EventHandler<BroadcastEventArgs> handler = BroadcastHandled;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Raise the broadcast received event.
        /// </summary>
        /// <param name="e">The broadcast event arguments.</param>
        public void RaiseBroadcastReceived(BroadcastEventArgs e)
        {
            OnBroadcastReceived(e);
            if (!e.Handled)
            {
                HandleBroadcast(e);
            }
            OnBroadcastHandled(e);
        }

        /// <summary>
        /// Handle the broadcast. Child class must override this method.
        /// </summary>
        /// <param name="e">BroadcastEventArgs</param>
        protected abstract void HandleBroadcast(BroadcastEventArgs e);

        /// <summary>
        /// Clears the list of removed items.
        /// </summary>
        public void ClearRemovedItems()
        {
            _removedItems.Clear();
        }

        /// <summary>
        /// Gets the list of removed items.
        /// </summary>
        /// <returns></returns>
        public List<T> GetRemovedItems()
        {
            return _removedItems.ToList();
        }


        /// <summary>
        /// Adds or refreshes collection in this list of entities with the
        /// the Ufis.Data.DataRow object.
        /// </summary>
        /// <param name="row">A Ufis.Data.DataRow that contains the record.</param>
        public void AddCollection(DataRow row)
        {
            DataRowCollection rows = new DataRowCollection();
            rows.AddRow(row);

            AddCollection(rows);
        }

        /// <summary>
        /// Adds or refreshes collection in this list of entities with the records from
        /// the Ufis.Data.DataRowCollection object.
        /// </summary>
        /// <param name="rows">A Ufis.Data.DataRowCollection that contains the records.</param>  
        public void AddCollection(DataRowCollection rows)
        {
            DataContext.FillEntityCollection<T>(this, rows);
        }

        /// <summary>
        /// Save all changes in this instance of Ufis.Data.EntityCollectionBase to the data source.
        /// </summary>
        public void Save()
        {
            DataContext.SaveEntityCollection<T>(this);
        }

        /// <summary>
        /// Save changes of one item in this instance of Ufis.Data.EntityCollectionBase to the data source.
        /// </summary>
        public void Save(T item)
        {
            Entity entity = (Entity)((object)item);
            DataContext.SaveEntity(entity, ColumnList);
        }

        /// <summary>
        /// Closes this instance of Ufis.Data.EntityCollectionBase class.
        /// </summary>
        public void Close()
        {
            DataContext.CloseEntityCollection<T>(this);
        }

        /// <summary>
        /// Initializes a new instance of Ufis.Data.EntityCollectionBase class.
        /// </summary>
        /// <param name="dataContext">The Ufis.Data.EntityDataContextBase that owns this instance of Ufis.Data.EntityCollectionBase class.</param>
        public EntityCollectionBase(EntityDataContextBase dataContext)
        {
            _removedItems = new List<T>();
            _dataContext = dataContext;            
        }

        /// <summary>
        /// Initializes a new instance of Ufis.Data.EntityCollectionBase class.
        /// </summary>
        /// <param name="list">The list form which the elements are copied.</param>
        /// <param name="dataContext">The Ufis.Data.EntityDataContextBase that owns this instance of Ufis.Data.EntityCollectionBase class.</param>
        public EntityCollectionBase(List<T> list, EntityDataContextBase dataContext)
            : base(list)
        {
            _removedItems = new List<T>();
            _dataContext = dataContext;   
        }

        /// <summary>
        /// Initializes a new instance of Ufis.Data.EntityCollectionBase class.
        /// </summary>
        /// <param name="collection">The collection form which the elements are copied.</param>
        /// <param name="dataContext">The Ufis.Data.EntityDataContextBase that owns this instance of Ufis.Data.EntityCollectionBase class.</param>
        public EntityCollectionBase(IEnumerable<T> collection, EntityDataContextBase dataContext)
            : base(collection)
        {
            _removedItems = new List<T>();
            _dataContext = dataContext;   
        }
         
    }
}
