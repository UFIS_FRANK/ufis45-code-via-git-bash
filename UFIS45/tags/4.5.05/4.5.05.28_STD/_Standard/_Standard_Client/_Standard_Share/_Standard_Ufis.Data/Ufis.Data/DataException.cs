﻿using System;

namespace Ufis.Data
{
    public class DataException : Exception
    {
        /// <summary>
        /// Gets a collection of one or more Ufis.Data.DataError that give detailed explanation about the exceptions.
        /// </summary>
        public DataErrorCollection Errors { get; private set; }

        /// <summary>
        /// Gets the first error message that describes current exception.
        /// </summary>
        public new string Message
        {
            get 
            {
                string strMessage = base.Message;
                if (this.Errors.Count > 0)
                {
                    strMessage = this.Errors[0].Message;
                }
                return strMessage;
            }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.DataException class.
        /// </summary>
        public DataException() 
        {
            this.Errors = null;
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.DataException class.
        /// </summary>
        /// <param name="errors">A collection of one or more Ufis.Data.DataError that give detailed explanation about the exceptions.</param>
        public DataException(DataErrorCollection errors)
        {
            this.Errors = errors;
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.DataException class.
        /// </summary>
        /// <param name="message">An error message.</param>
        public DataException(string message) : this(message, null) { }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.DataException class.
        /// </summary>
        /// <param name="message">An array of error message.</param>
        public DataException(string[] messages) : this(messages, null) { }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.DataException class.
        /// </summary>
        /// <param name="message">An error message.</param>
        /// <param name="exception">A System.Exception instance that caused this error.</param>
        public DataException(string message, Exception exception)
        {
            DataErrorCollection errors = new DataErrorCollection();
            errors.Add(new DataError(message, exception));
            this.Errors = errors;
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.DataException class.
        /// </summary>
        /// <param name="message">An array of error message.</param>
        /// <param name="exception">An array of System.Exception instance that caused this error.</param>
        public DataException(string[] messages, Exception[] exceptions)
        {
            DataErrorCollection errors = new DataErrorCollection();
            int i = 0;
            foreach (string strMessage in messages)
            {
                Exception ex = null;
                if (exceptions != null) ex = exceptions[i++];
                errors.Add(new DataError(strMessage, ex));
            }
            this.Errors = errors;
        }
    }
}
