﻿using System;

namespace Ufis.Data
{
    public enum ConnectionState
    {
        Closed,
        Open,
        Connecting
    }
}
