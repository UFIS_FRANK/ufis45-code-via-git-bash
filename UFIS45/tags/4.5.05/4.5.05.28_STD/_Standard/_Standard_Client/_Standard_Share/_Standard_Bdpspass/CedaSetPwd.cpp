//
// CedaSetPwd.cpp
//
//

#include <stdafx.h>
#include <CCSCedadata.h>
#include <CedaSetPwd.h>

//
// CedaSetPwd()
//
// constructor
//
CedaSetPwd::CedaSetPwd()
{


} // end CedaSetPwd()


//
// CedaSetPwd()
//
// destructor
//
CedaSetPwd::~CedaSetPwd(void)
{


} // end destructor()



//
// SetPwd()
//
//
bool CedaSetPwd::Update(const char *pcpHomeAirport, const char *pcpUsid, const char *pcpOldPass, const char *pcpNewPass)
{
	bool blRc = true;

	char pclData[524];
	char pclFields[524];
	char pclHomeAirport[50];
	char pclWhere[20];


	sprintf(pclData,"%s,%s,%s",(LPCTSTR) pcpUsid, (LPCTSTR) pcpOldPass, (LPCTSTR) pcpNewPass);
	strcpy(pclFields,"USID,OLDP,NEWP");
	strcpy(pclHomeAirport,pcpHomeAirport);
	strcat(pclHomeAirport, " ");
	strcpy(pclWhere,"NEWPWD");

	// Get Privileges
	blRc = CedaAction("SEC",pclHomeAirport,pclFields,pclWhere,"",pclData);

	if( ! blRc )
		omErrorMessage = omLastErrorMessage;

	return blRc;


} // end Update()


