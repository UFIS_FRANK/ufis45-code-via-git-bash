﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
using Ufis.Data;

namespace _Standard_Bdpsuif_Addon.Ctrl
{
    public class CtrlBDValidity
    {       
        public class BDDataSource
        {
            public class Table
            {
                [FlagsAttribute]
                public enum TableTypeEnum
                {
                    Unspecified = 0,
                    MasterTable = 1,
                    DetailTable = 2,
                    SchedulerTable = 4,
                    HelperTable = 8
                }

                public class Column
                {
                    public enum ColumnDataTypeEnum
                    {
                        String,
                        Int,
                        Double,
                        DateTime,
                        ValidFrom,
                        ValidTo,
                        CreatedBy,
                        ModifiedBy,
                        CreationDate,
                        ModificationDate,
                        DeletionFlag
                    }

                    public enum ColumnDefaultValueTypeEnum
                    {
                        Text,
                        Expression
                    }

                    public string Name { get; set; }                    
                    public ColumnDataTypeEnum DataType { get; set; }
                    public bool Required { get; set; }
                    public object DefaultValue { get; set; }
                    public ColumnDefaultValueTypeEnum DefaultValueType { get; set; }
                    public bool Unique { get; set; }
                    public int Length { get; set; }

                    public static ColumnDataTypeEnum GetColumnDataType(string dataType)
                    {
                        switch (dataType.ToUpper())
                        {
                            case "INT":
                            case "INT32":
                                return ColumnDataTypeEnum.Int;
                            case "DBL":
                            case "DOUBLE":
                            case "FLOAT":
                                return ColumnDataTypeEnum.Double;
                            case "DATE":
                            case "DATETIME":
                                return ColumnDataTypeEnum.DateTime;
                            case "VAFR":
                            case "VALIDFROM":
                                return ColumnDataTypeEnum.ValidFrom;
                            case "VATO":
                            case "VALIDTO":
                                return ColumnDataTypeEnum.ValidTo;
                            case "CDAT":
                            case "CREATIONDATE":
                                return ColumnDataTypeEnum.CreationDate;
                            case "USEC":
                            case "CREATEDBY":
                                return ColumnDataTypeEnum.CreatedBy;
                            case "LSTU":
                            case "MODIFICATIONDATE":
                                return ColumnDataTypeEnum.ModificationDate;
                            case "USEU":
                            case "MODIFIEDBY":
                                return ColumnDataTypeEnum.ModifiedBy;
                            case "DELFLAG":
                            case "DELETIONFLAG":
                                return ColumnDataTypeEnum.DeletionFlag;
                            default:
                                return ColumnDataTypeEnum.String;
                        }
                    }

                    public static ColumnDefaultValueTypeEnum GetColumnDefaultValueType(string defaultValueType)
                    {
                        switch (defaultValueType.ToUpper())
                        {
                            case "EXP":
                            case "EXPRESSION":
                                return ColumnDefaultValueTypeEnum.Expression;
                            default:
                                return ColumnDefaultValueTypeEnum.Text;
                        }
                    }

                    public Column(string name)
                    {
                        this.Name = name;
                        this.Required = false;
                        this.DataType = ColumnDataTypeEnum.String;
                        this.DefaultValue = null;
                        this.DefaultValueType = ColumnDefaultValueTypeEnum.Text;
                        this.Unique = false;
                    }

                    public Column() : this(string.Empty) { }
                }

                public class ColumnCollection : List<Column> 
                {
                    private Dictionary<string, Column> columns = null;

                    public bool TryGetColumnByName(string columnName, out Column column)
                    {
                        return TryGetColumnByName(columnName, false, out column);
                    }

                    public bool TryGetColumnByName(string columnName, bool recalculate, out Column column)
                    {
                        if (recalculate || columns == null)
                        {
                            if (columns == null)
                                columns = new Dictionary<string, Column>();
                            foreach (Column col in this)
                            {
                                columns.Add(col.Name, col);
                            }
                        }
                        if (columns == null)
                        {
                            column = null;
                            return false;
                        }
                        else
                        {
                            return columns.TryGetValue(columnName, out column);
                        }
                    }
                }

                public string TableName { get; set; }
                public string TableAlias { get; set; }
                public string Filter { get; set; }
                public string Sort { get; set; }
                public TableTypeEnum Type { get; set; }
                public ColumnCollection Columns { get; set; }

                public Table()
                {
                    this.TableName = string.Empty;
                    this.TableAlias = string.Empty;
                    this.Filter = string.Empty;
                    this.Sort = string.Empty;
                    this.Type = TableTypeEnum.Unspecified;
                    this.Columns = new ColumnCollection();                    
                }

                public static TableTypeEnum GetTableType(string tableType)
                {
                    switch (tableType.ToUpper())
                    {
                        case "MASTER":
                        case "MASTERTABLE":
                            return TableTypeEnum.MasterTable;
                        case "DETAIL":
                        case "DETAILTABLE":
                            return TableTypeEnum.DetailTable;
                        case "SCHEDULER":
                        case "SCHEDULERTABLE":
                            return TableTypeEnum.SchedulerTable;
                        default:
                            return TableTypeEnum.HelperTable;
                    }
                }
            }

            public class ColumnMap
            {
                public enum ColumnMapValueTypeEnum
                {
                    Text,
                    DetailColumnName,
                    Expression
                }

                public string SchedulerColumnName { get; set; }
                public string Value { get; set; }
                public ColumnMapValueTypeEnum ValueType { get; set; }
                public char Separator { get; set; }
                public bool IsKeyColumn { get; set; }

                public ColumnMap(string schedulerColumnName, string value, 
                    ColumnMapValueTypeEnum valueType, char separator, bool isKeyColumn)
                {
                    this.SchedulerColumnName = schedulerColumnName;
                    this.Value = value;
                    this.ValueType = valueType;
                    this.Separator = separator;
                    this.IsKeyColumn = isKeyColumn;
                }

                public ColumnMap(string schedulerColumnName, string value,
                    ColumnMapValueTypeEnum valueType, char separator)
                    : this(schedulerColumnName, value, valueType, separator, false) { }

                public ColumnMap(string schedulerColumnName, string value, 
                    ColumnMapValueTypeEnum valueType)
                    : this(schedulerColumnName, value, valueType, ';', false) { }

                public ColumnMap(string schedulerColumnName, string value, bool isKeyColumn)
                    : this(schedulerColumnName, value,
                            ColumnMapValueTypeEnum.DetailColumnName, ';', isKeyColumn) { }

                public ColumnMap(string schedulerColumnName, string value)
                    : this(schedulerColumnName, value, 
                            ColumnMapValueTypeEnum.DetailColumnName, ';', false) { }

                public ColumnMap()
                    : this(string.Empty, string.Empty) { }

                public static ColumnMapValueTypeEnum GetColumnMapValueType(string columnMapValueType)
                {
                    switch (columnMapValueType.ToUpper())
                    {
                        case "TEXT":
                            return ColumnMapValueTypeEnum.Text;
                        case "EXP":
                        case "EXPRESSION":
                            return ColumnMapValueTypeEnum.Expression;
                        default:
                            return ColumnMapValueTypeEnum.DetailColumnName;
                    }
                }
            }

            public class ColumnMapCollection: List<ColumnMap> 
            {
                public ColumnMap GetKeyColumnMap()
                {
                    foreach (ColumnMap column in this)
                    {
                        if (column.IsKeyColumn)
                        {
                            return column;
                        }
                    }
                    return null;
                }                
            }

            public class TableCollection: List<Table>
            {
                public bool TryGetTable(string tableName, out Table table)
                {
                    foreach (Table tbl in this)
                    {
                        if (tbl.TableName == tableName)
                        {
                            table = tbl;
                            return true;
                        }
                    }
                    table = null;
                    return false;
                }

                public Table MasterTable
                {
                    get 
                    {
                        foreach (Table table in this)
                        {
                            if ((table.Type & Table.TableTypeEnum.MasterTable) == Table.TableTypeEnum.MasterTable)
                            {
                                return table;
                            }
                        }
                    
                        return null; 
                    }
                }

                public Table DetailTable
                {
                    get
                    {
                        foreach (Table table in this)
                        {
                            if ((table.Type & Table.TableTypeEnum.DetailTable) == Table.TableTypeEnum.DetailTable)
                            {
                                return table;
                            }
                        }

                        return null;
                    }
                }

                public Table SchedulerTable
                {
                    get
                    {
                        foreach (Table table in this)
                        {
                            if ((table.Type & Table.TableTypeEnum.SchedulerTable) == Table.TableTypeEnum.SchedulerTable)
                            {
                                return table;
                            }
                        }

                        return null;
                    }
                }
                
                public TableCollection HelperTables
                {
                    get
                    {
                        TableCollection helperTables = new TableCollection();

                        foreach (Table table in this)
                        {
                            if ((table.Type & Table.TableTypeEnum.HelperTable) == Table.TableTypeEnum.HelperTable)
                            {
                                helperTables.Add(table);
                            }
                        }

                        return helperTables;
                    }
                }
            }

            public TableCollection Tables { get; set; }
            public ColumnMapCollection ColumnMaps { get; set; }

            public BDDataSource()
            {
                this.Tables = new TableCollection();
                this.ColumnMaps = new ColumnMapCollection();
            }
        }

        public class BDLayout
        {
            public class Section
            {
                public class SectionItem
                {
                    public enum SectionItemValueTypeEnum
                    {
                        Text,
                        BoundField,
                        Expression
                    }

                    public SectionItemValueTypeEnum ValueType { get; set; }
                    public string Value { get; set; }
                    public string DataSource { get; set; }
                    public string FontName { get; set; }
                    public float FontSize { get; set; }
                    public bool FontBold { get; set; }
                    public bool FontItalic { get; set; }
                    public bool FontUnderline { get; set; }
                    public BorderStyle BorderStyle { get; set; }

                    public SectionItem(string value)
                    {
                        this.Value = value;
                        this.ValueType = SectionItemValueTypeEnum.Text;
                        this.DataSource = string.Empty;
                        this.FontName = "Microsoft Sans Serif";
                        this.FontSize = 8.25F;
                        this.FontBold = false;
                        this.FontItalic = false;
                        this.FontUnderline = false;
                        this.BorderStyle = BorderStyle.None;
                    }

                    public SectionItem(): this(string.Empty)
                    { }

                    public static SectionItemValueTypeEnum GetSectionItemValueType(string sectionItemValueType)
                    {
                        switch (sectionItemValueType.ToUpper())
                        {
                            case "FIELD":
                            case "BOUNDFIELD":
                                return SectionItemValueTypeEnum.BoundField;
                            case "EXP":
                            case "EXPRESSION":
                                return SectionItemValueTypeEnum.Expression;
                            default:
                                return SectionItemValueTypeEnum.Text;
                        }
                    }

                    public static BorderStyle GetSectionItemBorderStyle(string sectionItemBorderStyle)
                    {
                        switch (sectionItemBorderStyle.ToUpper())
                        {
                            case "FIXED3D":
                                return BorderStyle.Fixed3D;
                            case "FIXEDSINGLE":
                                return BorderStyle.FixedSingle;
                            default:
                                return BorderStyle.None;
                        }
                    }
                }

                public List<SectionItem> Items { get; set; }

                public Section()
                {
                    this.Items = new List<SectionItem>();
                }
            }

            public class BodySection
            {
                public class ViewColumn
                {
                    public enum ViewColumnTypeEnum
                    {
                        Text,
                        List,
                        Checkbox,
                        Button,
                        Image
                    }

                    public enum ViewColumnSourceTypeEnum
                    {
                        ValueList,
                        UfisTable
                    }

                    public class ViewColumnValueChangedRule
                    {
                        public string ColumnName { get; set; }
                        public string Expression { get; set; }

                        public ViewColumnValueChangedRule(string columnName, string expression)
                        {
                            this.ColumnName = columnName;
                            this.Expression = expression;
                        }

                        public ViewColumnValueChangedRule() : this(string.Empty, string.Empty) { }
                    }
                                        
                    public string Caption { get; set; }
                    public string BoundField { get; set; }
                    public ViewColumnTypeEnum ColumnType { get; set; }
                    public string DisplayFormat { get; set; }
                    public string[] InputFormats { get; set; }
                    public int Width { get; set; }
                    public bool ReadOnly { get; set; }
                    public bool Visible { get; set; }
                    public string ValueMember { get; set; }
                    public string DisplayMember { get; set; }
                    public ViewColumnSourceTypeEnum SourceType { get; set; }
                    public string DataSource { get; set; }
                    public DataGridViewContentAlignment HeaderAlignment { get; set; }
                    public DataGridViewContentAlignment ContentAlignment { get; set; }
                    public string EditableRule { get; set; }
                    public ViewColumnValueChangedRule ValueChangedRule { get; set; }
                    
                    public ViewColumn()
                    {
                        this.BoundField = string.Empty;
                        this.Caption = string.Empty;
                        this.ColumnType = ViewColumnTypeEnum.Text;
                        this.DisplayFormat = string.Empty;
                        this.InputFormats = null;
                        this.Width = 100;
                        this.ReadOnly = false;
                        this.Visible = true;
                        this.ValueMember = string.Empty;
                        this.DisplayMember = string.Empty;
                        this.SourceType = ViewColumnSourceTypeEnum.ValueList;
                        this.DataSource = string.Empty;
                        this.HeaderAlignment = DataGridViewContentAlignment.MiddleLeft;
                        this.ContentAlignment = DataGridViewContentAlignment.MiddleLeft;
                        this.EditableRule = string.Empty; 
                        this.ValueChangedRule = null;
                    }

                    public static ViewColumnTypeEnum GetViewColumnType(string viewColumnType)
                    {
                        switch (viewColumnType.ToUpper())
                        {
                            case "LIST":
                                return ViewColumnTypeEnum.List;
                            case "CHECKBOX":
                                return ViewColumnTypeEnum.Checkbox;
                            case "BUTTON":
                                return ViewColumnTypeEnum.Button;
                            case "IMAGE":
                                return ViewColumnTypeEnum.Image;
                            default:
                                return ViewColumnTypeEnum.Text;
                        }
                    }

                    public static ViewColumnSourceTypeEnum GetViewColumnSourceType(string viewColumnSourceType)
                    {
                        switch (viewColumnSourceType.ToUpper())
                        {   
                            case "TABLE":
                            case "UFISTABLE":
                                return ViewColumnSourceTypeEnum.UfisTable;
                            default:
                                return ViewColumnSourceTypeEnum.ValueList;
                        }
                    }

                    public static DataGridViewContentAlignment GetViewColumnAlignment(string viewColumnAlignment)
                    {
                        switch (viewColumnAlignment.ToUpper())
                        {
                            case "BOTTOMCENTER":
                                return DataGridViewContentAlignment.BottomCenter;
                            case "BOTTOMLEFT":
                                return DataGridViewContentAlignment.BottomLeft;
                            case "BOTTOMRIGHT":
                                return DataGridViewContentAlignment.BottomRight;
                            case "MIDDLECENTER": 
                                return DataGridViewContentAlignment.MiddleCenter;
                            case "MIDDLELEFT":
                                return DataGridViewContentAlignment.MiddleLeft;
                            case "MIDDLERIGHT":
                                return DataGridViewContentAlignment.MiddleRight;
                            case "TOPCENTER":
                                return DataGridViewContentAlignment.TopCenter;
                            case "TOPLEFT":
                                return DataGridViewContentAlignment.TopLeft;
                            case "TOPRIGHT":
                                return DataGridViewContentAlignment.TopRight;
                            default:
                                return DataGridViewContentAlignment.NotSet;
                        }
                    }
                }

                public class ViewColumnCollection: List<ViewColumn> 
                {
                    public ViewColumn FindByName(string viewColumnName)
                    {
                        foreach (ViewColumn column in this)
                        {
                            if (column.BoundField == viewColumnName)
                            {
                                return column;
                            }
                        }

                        return null;
                    }
                }

                public class PairViewColumn
                {
                    public class Column
                    {
                        public string ViewColumnName { get; set; }
                        public string FieldName { get; set; }

                        public Column(string viewColumnName, string fieldName)
                        {
                            this.ViewColumnName = viewColumnName;
                            this.FieldName = fieldName;
                        }

                        public Column() : this(string.Empty, string.Empty) { }
                    }

                    public class ColumnCollection: List<Column> {}

                    public string DataSource { get; set; }
                    public ColumnCollection Columns { get; set; }

                    public PairViewColumn()
                    {
                        this.DataSource = string.Empty;
                        this.Columns = new ColumnCollection();
                    }
                }

                public class PairViewColumnCollection : List<PairViewColumn> 
                {
                    public PairViewColumn FindByName(string viewColumnName)
                    {
                        foreach (PairViewColumn pairColumn in this)
                        {
                            foreach (PairViewColumn.Column column in pairColumn.Columns)
                            {
                                if (column.ViewColumnName == viewColumnName)
                                {
                                    return pairColumn;
                                }
                            }
                        }

                        return null;
                    }
                }                

                public bool ShowDeleteButton { get; set; }
                public ViewColumnCollection Columns { get; set; }
                public PairViewColumnCollection PairColumns { get; set; }

                public BodySection()
                {
                    this.ShowDeleteButton = false;
                    this.Columns = new ViewColumnCollection();
                    this.PairColumns = new PairViewColumnCollection();
                }
            }

            public string Title { get; set; }
            public Section Header { get; set; }
            public BodySection Body { get; set; }
            public Section Footer { get; set; }

            public BDLayout(string title)
            {
                this.Title = title;
                this.Header = new Section();
                this.Body = new BodySection();
                this.Footer = new Section();
            }

            public BDLayout() : this("") { }
        }

        public BDDataSource DataSource { get; set; }
        public BDLayout Layout { get; set; }

        public CtrlBDValidity()
        {
            this.DataSource = new BDDataSource();
            this.Layout = new BDLayout();
        }
    }
}
