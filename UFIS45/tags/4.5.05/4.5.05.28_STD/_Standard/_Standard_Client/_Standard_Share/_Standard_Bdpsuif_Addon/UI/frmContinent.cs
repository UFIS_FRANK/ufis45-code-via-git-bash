﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Ufis.Data;
using Ufis.Utils;

using _Standard_Bdpsuif_Addon.Ctrl;
using _Standard_Bdpsuif_Addon.Helper;
using _Standard_Bdpsuif_Addon.UserControl;

namespace _Standard_Bdpsuif_Addon.UI
{
    public partial class frmContinent : Form
    {
        public enum Modes { CONTINENT_NEW, CONTINENT_UPDATE };

        public const string CNTTAB_DB_FIELDS =
        "URNO,CODE,DESP,CDAT,USEC,LSTU,USEU,VAFR,VATO,HOPO,STAT";
        public const string CNTTAB_FIELD_LENGTHS =
           "14,14,200,14,32,14,32,14,14,3,1";
        public const string CNTTAB_LOGICAL_FIELDS =
           "URNO,CODE,DESP,CDAT,USEC,LSTU,USEU,VAFR,VATO,HOPO,STAT";
        //"URNO,Code,Description,CDAT,USEC,LSTU,USEU,VAFR,VATO,HOPO";

        //private IniFile myIni = null;
        private IDatabase myDB = null;
        public ITable ctnTab = null;
        private _Standard_Bdpsuif_Addon.UI.frmContinent.Modes curMode = _Standard_Bdpsuif_Addon.UI.frmContinent.Modes.CONTINENT_UPDATE;
        string _urno = "";
        
       /* public frmContinent()
        {
            InitializeComponent();
        }
        */
        public frmContinent(string urno, _Standard_Bdpsuif_Addon.UI.frmContinent.Modes mode)
        {
            InitializeComponent();
            myDB = CtrlData.GetInstance().MyDB;
            curMode = mode;
            _urno = urno;
            //Load all the Continent records
            LoadContinents();
            if (curMode == Modes.CONTINENT_UPDATE)
            {
                LoadAContinent(urno);
            }
            else
            {
                PrepareBlankForm();
            }
        }
        void LoadContinents()
        {
            myDB.Unbind("CNT_SEC");
            ctnTab = myDB.Bind("CNT_SEC", "CNT", CNTTAB_LOGICAL_FIELDS, CNTTAB_FIELD_LENGTHS, CNTTAB_DB_FIELDS);
            ctnTab.Load("WHERE STAT<> 'D' ORDER BY URNO");
            ctnTab.CreateIndex("URNO", "URNO");
        }

        void PrepareBlankForm()
        {
            txtCreatedBy.Text = Program.UserName;
            txtCreatedDate.Text = String.Format("{0:dd.MM.yyyy}", DateTime.Now);
            txtCreatedTime.Text = String.Format("{0:HH:mm}", DateTime.Now);
        }
        void LoadAContinent(String urno)
        {
            myDB.Unbind("TMP");
            ctnTab = myDB.Bind("TMP", "CNT", CNTTAB_LOGICAL_FIELDS, CNTTAB_FIELD_LENGTHS, CNTTAB_DB_FIELDS);
            ctnTab.Load("WHERE URNO=" + urno);

            int cnt = ctnTab.Count;
            for (int i = 0; i < cnt; i++)
            {
                //URNO,CODE,DESP,CDAT,USEC,LSTU,USEU,VAFR,VATO,HOPO
                IRow row = ctnTab[i];
                //string urno = ctnTab[i]["URNO"];
                string strCode = ctnTab[i]["CODE"];
                string strDesp = ctnTab[i]["DESP"];
                string strCDAT = ctnTab[i]["CDAT"];
                string strUsec = ctnTab[i]["USEC"];
                string strUseu = ctnTab[i]["USEU"];
                string strLstu = ctnTab[i]["LSTU"];
                string strHopo = ctnTab[i]["HOPO"];

                txtCode.Text = strCode;
                txtDesc.Text = strDesp;
                
                txtCreatedBy.Text = strUsec;
                DateTime dtTmp=new DateTime();
                if (strCDAT.Length > 0 && IsValidUfisTimeString(strCDAT, out dtTmp))
                {
                    txtCreatedDate.Text = String.Format("{0:dd.MM.yyyy}", UT.UtcToLocal(dtTmp));
                    txtCreatedTime.Text = String.Format("{0:HH:mm}", UT.UtcToLocal(dtTmp));
                }

                txtChangedBy.Text = strUseu;

                if (strLstu.Length> 0 && IsValidUfisTimeString(strLstu, out dtTmp))
                {
                    txtChangedDate.Text = String.Format("{0:dd.MM.yyyy}", UT.UtcToLocal(dtTmp));
                    txtChangedTime.Text = String.Format("{0:HH:mm}", UT.UtcToLocal(dtTmp));
                }
            }
        }
        public bool IsValidUfisTimeString(string st, out DateTime dt)
        {
            bool valid = false;
            dt = new DateTime(1, 1, 1);
            if (st == "") { valid = true; }
            else if (st.Length == 14)
            {
                try
                {
                    if (Convert.ToInt64(st).ToString() == st)
                    {
                        dt = new DateTime(Convert.ToInt16(st.Substring(0, 4)),
                            Convert.ToInt16(st.Substring(4, 2)),
                            Convert.ToInt16(st.Substring(6, 2)),
                            Convert.ToInt16(st.Substring(8, 2)),
                            Convert.ToInt16(st.Substring(10, 2)),
                            Convert.ToInt16(st.Substring(12, 2)));
                        valid = true;
                    }
                }
                catch (Exception)
                {
                    //throw;
                }
            }
            return valid;
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (curMode == Modes.CONTINENT_UPDATE)
            {
                UpdateAContinent(_urno);
            }
            else
            {
                SaveAContinent();
            }
        }

        public void SaveAContinent()
        {
            IRow row = ctnTab.CreateEmptyRow();
            string strCode = txtCode.Text.Trim();
            if (IsValid(""))
            {
                row["URNO"] = myDB.GetNextUrno();
                row["CODE"] = strCode;
                row["DESP"] = txtDesc.Text.Trim();
                row["CDAT"] = UT.DateTimeToCeda(UT.LocalToUtc(DateTime.Now));
                row["USEC"] = Program.UserName;
                row["HOPO"] = UT.Hopo;
                row["STAT"] = "I";
                row.Status = State.Created;
                ctnTab.Add(row);
                ctnTab.Save(row);
                
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                //MessageBox.Show("Duplicate Code found!");
            }
        }
        private bool IsValid(string urno)
        {
            bool isOK = true;
            string strCode = txtCode.Text.Trim();
            if (string.IsNullOrEmpty(strCode))
            {
                isOK = false;
                MessageBox.Show("Code should not be blank!");
                return isOK;
            }
            
            int cnt = ctnTab.Count;
            for (int i = 0; i < cnt; i++)
            {
                IRow row = ctnTab[i];
                string strRowCode = ctnTab[i]["CODE"].Trim();
                if (!string.IsNullOrEmpty(urno))
                {
                    string strUrno = ctnTab[i]["URNO"].Trim();
                    if (strUrno == urno)
                    {
                        continue;
                    }
                }

                if (strRowCode == strCode)
                {
                    isOK = false;
                    MessageBox.Show("Duplicate Code found!");
                    return isOK;
                }
            }
            string strDesc = txtDesc.Text.Trim();
            if (string.IsNullOrEmpty(strDesc))
            {
                isOK = false;
                MessageBox.Show("Identifier should not be blank!");
                return isOK;
            }

            return isOK;
        }
        
        void UpdateAContinent(string urno)
        {
            IRow row=null;
            for (int i = 0; i < ctnTab.Count; i++)
            {
                row = ctnTab[i];
                string rowUro = row["URNO"];
                if (rowUro == urno)
                {
                    break;
                }
            }
            if (row != null)
            {
                if (IsValid(urno))
                {
                    row["URNO"] = urno;
                    row["CODE"] = txtCode.Text.Trim();
                    row["DESP"] = txtDesc.Text.Trim();
                    row["LSTU"] = UT.DateTimeToCeda(UT.LocalToUtc(DateTime.Now));
                    row["USEU"] = Program.UserName;
                    row["HOPO"] = UT.Hopo;
                    row["STAT"] = "U";
                    row.Status = State.Modified;
                    ctnTab.Save(row);
                }
            }
            this.DialogResult = DialogResult.OK;
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

     
    }
}
