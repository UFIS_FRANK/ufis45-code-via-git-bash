// -----------------------------------------------------------------
// File name:  UFIS.H
//
// This header file contains the function prototypes for functions
// exported by the DLL named DLLSKEL.
//
// -----------------------------------------------------------------
                                                      
// Includes
                                                      
#include <sys/types.h> 
#include <stdio.h>
#include <STRING.H>
#include <memory.h> 


// Types

              
/*
 *
 * Following the FileTransfer Header .....FT_HEAD
 *
 *
 */                        
typedef struct {
	short		command;
	char		param[128];
	char		dest_param[128];
	char		data[1];
}FT_HEAD;
 

// Global Variable

extern HANDLE ghDLLInst;      // Global to store DLL instance handle
                              

// Defines
// For FileTransfer
                               
#define		RC_ERROR		0xff                           
#define		WRITE_FILE		0x1
#define		GET_FILE		0x2
#define		RESULT			0x3
#define		PASSWD			0x4                                      
#define		RESET_JOB		0x5                                      


// WriteTrace options
#define WT_DEBUG		1
#define WT_ERROR		2                              
#define WT_FATAL_ERROR	3                              


// Function Prototypes

int FAR PASCAL InitCom  (LPSTR IpAddr, LPSTR CedaHost);
int FAR PASCAL CleanupCom  (void);
int FAR PASCAL CallCeda (LPSTR req_id,LPSTR dest1, LPSTR dest2, LPSTR cmd, LPSTR object,
	          LPSTR seq, LPSTR tws, LPSTR twe, LPSTR selection, LPSTR fields,
	          LPSTR data ) ;
int FAR PASCAL GetBc (LPSTR req_id,LPSTR dest1, LPSTR dest2, LPSTR cmd, LPSTR object,
	          LPSTR seq, LPSTR tws, LPSTR twe, LPSTR selection, LPSTR fields,
	          LPSTR data/*, int FAR *bc_num*/ ) ;
void WriteTrace ( LPSTR buf, int prio);
int FAR PASCAL UfisDllAdmin  (LPSTR Pcommand, LPSTR Parg1, LPSTR Parg2);
char *GetTraceBuf  (void);  


