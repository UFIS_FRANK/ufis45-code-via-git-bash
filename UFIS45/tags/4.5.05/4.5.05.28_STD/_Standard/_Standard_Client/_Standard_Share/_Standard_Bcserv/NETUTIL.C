/*******************************************************************

    netutil.c

    This file contains routines of general utility.

********************************************************************/

#include <time.h>

#include <netutil.h>
#include <ufis.h>
                   

// External Variables
                   
extern FILE *outpt;
extern char ufis_ct[40];

// Defines

#define UFIS_PORT_NO 3350 
#define SYMAP_PORT_NO 3450 
//#define UFIS_BC_PORT_NO 3351 
#define UFIS_BC_PORT_NO 3352


#define PACKET_LEN 1024
                       
// Buffer for own internet address                       
static char MyIpAddr[20];

static unsigned short UfisBcPortNo = UFIS_BC_PORT_NO;

// DDC functionality
static int dZine;
//#define CONFIG_FILE_NAME "c:\\ufis\\rel_lux\\system\\ufisappl.cfg"
//#define CONFIG_FILE_NAME2 "c:\\ufis\\system\\ufisappl.cfg"
static char pcgConfigFileName[228] = "c:\\ufis\\system\\ceda.ini";

// Pointer to external trace buffer
static char *trace_buf = NULL;

/* Define GETSERVBYNAME_WORKS, if it realy works */
#undef GETSERVBYNAME_WORKS

/* Define GETHOSTBYNAME_WORKS, if it realy works. IT DOES (not really MCU 19.02.98) */
// #define GETHOSTBYNAME_WORKS

/* Define USE_BCHEAD_NETWORK_BYTE_ORDER, if BC_HEAD should be converted */
#undef USE_BCHEAD_NETWORK_BYTE_ORDER



/*******************************************************************

    NAME:       SockerrToString

    SYNOPSIS:   Maps a socket error (like WSAEINTR) to a displayable
                form (like "Interrupted system call").

    ENTRY:      serr - The error to map.

    RETURNS:    LPSTR - The displayable form of the error.  Will be
                    "Unknown" for unknown errors.

********************************************************************/
LPSTR SockerrToString( SOCKERR serr )
{
	static char errbuf[32];
	
    switch( serr )
    {
    case WSAENAMETOOLONG :
        return "Name too long";

    case WSANOTINITIALISED :
        return "Not initialized";

    case WSASYSNOTREADY :
        return "System not ready";

    case WSAVERNOTSUPPORTED :
        return "Version is not supported";

    case WSAESHUTDOWN :
        return "Can't send after socket shutdown";

    case WSAEINTR :
        return "Interrupted system call";

    case WSAHOST_NOT_FOUND :
        return "Host not found";

    case WSATRY_AGAIN :
        return "Try again";

    case WSANO_RECOVERY :
        return "Non-recoverable error";

    case WSANO_DATA :
        return "No data record available";

    case WSAEBADF :
        return "Bad file number";

    case WSAEWOULDBLOCK :
        return "Operation would block";

    case WSAEINPROGRESS :
        return "Operation now in progress";

    case WSAEALREADY :
        return "Operation already in progress";

    case WSAEFAULT :
        return "Bad address";

    case WSAEDESTADDRREQ :
        return "Destination address required";

    case WSAEMSGSIZE :
        return "Message too long";

    case WSAEPFNOSUPPORT :
        return "Protocol family not supported";

    case WSAENOTEMPTY :
        return "Directory not empty";

    case WSAEPROCLIM :
        return "EPROCLIM returned";

    case WSAEUSERS :
        return "EUSERS returned";

    case WSAEDQUOT :
        return "Disk quota exceeded";

    case WSAESTALE :
        return "ESTALE returned";

    case WSAEINVAL :
        return "Invalid argument";

    case WSAEMFILE :
        return "Too many open files";

    case WSAEACCES :
        return "Access denied";

    case WSAELOOP :
        return "Too many levels of symbolic links";

    case WSAEREMOTE :
        return "The object is remote";

    case WSAENOTSOCK :
        return "Socket operation on non-socket";

    case WSAEADDRNOTAVAIL :
        return "Can't assign requested address";

    case WSAEADDRINUSE :
        return "Address already in use";

    case WSAEAFNOSUPPORT :
        return "Address family not supported by protocol family";

    case WSAESOCKTNOSUPPORT :
        return "Socket type not supported";

    case WSAEPROTONOSUPPORT :
        return "Protocol not supported";

    case WSAENOBUFS :
        return "No buffer space is supported";

    case WSAETIMEDOUT :
        return "Connection timed out";

    case WSAEISCONN :
        return "Socket is already connected";

    case WSAENOTCONN :
        return "Socket is not connected";

    case WSAENOPROTOOPT :
        return "Bad protocol option";

    case WSAECONNRESET :
        return "Connection reset by peer";

    case WSAECONNABORTED :
        return "Software caused connection abort";

    case WSAENETDOWN :
        return "Network is down";

    case WSAENETRESET :
        return "Network was reset";

    case WSAECONNREFUSED :
        return "Connection refused";

    case WSAEHOSTDOWN :
        return "Host is down";

    case WSAEHOSTUNREACH :
        return "Host is unreachable";

    case WSAEPROTOTYPE :
        return "Protocol is wrong type for socket";

    case WSAEOPNOTSUPP :
        return "Operation not supported on socket";

    case WSAENETUNREACH :
        return "ICMP network unreachable";

    case WSAETOOMANYREFS :
        return "Too many references";

    case 0 :
        return "No error";

    default :
    	sprintf (errbuf, "Unknown error code %d", serr);
        return errbuf;
    }
}


/*******************************************************************

    NAME:       ResetSocket

    SYNOPSIS:   Performs a "hard" close on the given socket.

    ENTRY:      sock - The socket to close.

    RETURNS:    SOCKERR - RC_SUCCESS if successful, RC_FAIL if not.

********************************************************************/
SOCKERR ResetSocket( SOCKET sock )
{                
	int rc = RC_SUCCESS;
    LINGER linger;
    SOCKERR serr=0;
    
    if( sock == INVALID_SOCKET )
    {
        //
        //  Ignore invalid sockets.
        //

        return RC_FAIL;
    }

    //
    //  Enable linger with a timeout of zero.  This will
    //  force the hard close when we call closesocket().
    //
    //  We ignore the error return from setsockopt.  If it
    //  fails, we'll just try to close the socket anyway.
    //

    linger.l_onoff  = TRUE;
    linger.l_linger = 0;

    setsockopt( sock,
                SOL_SOCKET,
                SO_LINGER,
                (CHAR FAR *)&linger,
                sizeof(linger) );

    //
    //  Close the socket.
    //

    rc = closesocket( sock );
    if (rc < 0)
	{
		serr = WSAGetLastError();
		sprintf(trace_buf,"ResetSocket: %s\n",SockerrToString( serr ) );
		WriteTrace(trace_buf, WT_DEBUG);
    	rc = RC_FAIL;
    }
    	
    return rc;

}   // ResetSocket

                           
/*******************************************************************

    NAME:       InitWinsock

    SYNOPSIS:   Init of the Windows Socket Library

    ENTRY:      address - The IP address for the socket.

    RETURNS:    SOCKERR - RC_SUCCESS if successful, RC_FAIL if not.

********************************************************************/

int InitWinsock  (LPSTR IpAddr)
{                      
	SOCKERR serr=0;
    WSADATA wsadata;
	char timebuf[20];	
	FILE *config = NULL;	// config file
    
	/* Date and Time to trace file */	              
	_strdate (timebuf);   
	timebuf[8] = ' ';
	_strtime (timebuf + 9);
	
	if (outpt != NULL)
	{
		fprintf(outpt, "UFIS-DLL:\nInitWinsock started %s\n", timebuf);
		fprintf(outpt, "Compile time ufis.c: %s  netutil.c: %s\n", ufis_ct, __TIMESTAMP__);
    }
    
	trace_buf = GetTraceBuf();
	                 
	                 
	// Save my internet address
	
    strcpy (MyIpAddr, IpAddr);
    
    //	Check DDC functionality
    
    dZine = 0;
    
    //
    //  Initialize the sockets library.
    //

    serr = WSAStartup( DESIRED_WINSOCK_VERSION, &wsadata );

    if( serr != 0 )
    {
		if (outpt != NULL) 
		{
			fprintf(outpt,
				"UFIS-DLL:\nInitWinsock: Cannot initialize socket library, error %d: %s",
                serr, SockerrToString( serr ) );
			fflush(outpt);
		}
        return RC_COMM_FAIL;
    }

    if( wsadata.wVersion < MINIMUM_WINSOCK_VERSION )
    {
		if (outpt != NULL) 
		{
	        fprintf(outpt,
				"InitWinsock:Windows Sockets version %02X.%02X, I need at least %02X.%02X",
                LOBYTE(wsadata.wVersion),
                HIBYTE(wsadata.wVersion),
                LOBYTE(MINIMUM_WINSOCK_VERSION),
                HIBYTE(MINIMUM_WINSOCK_VERSION) );
			fflush(outpt);
		}

        return RC_COMM_FAIL;
    }  
	if (outpt != NULL) 
	{
	    fprintf(outpt,
			"InitWinsock:Windows Sockets version %02X.%02X\n",
                LOBYTE(wsadata.wVersion),
                HIBYTE(wsadata.wVersion),
                LOBYTE(MINIMUM_WINSOCK_VERSION),
                HIBYTE(MINIMUM_WINSOCK_VERSION) );
		fflush(outpt);
    	fprintf(outpt,
			"Description: \n%s\nStatus:\n%s\n", wsadata.szDescription, 
				wsadata.szSystemStatus);
		fflush(outpt);
    	fprintf(outpt,
			"MaxSockets: %d MaxUdpDatagram Size = %d Bytes\n", wsadata.iMaxSockets, 
				wsadata.iMaxUdpDg);
		fflush(outpt);
    	fprintf(outpt, 
    	"--------------------------------------------------------------------\n");
		fflush(outpt); 
    } 
        
    return RC_SUCCESS;
}
                           
/*******************************************************************

    NAME:       CreateSocket

    SYNOPSIS:   Creates a data socket for the specified address & port.
                                                                          
    ENTRY:      psock - Will receive the new socket ID if successful.

                type - The socket type (SOCK_DGRAM or SOCK_STREAM).

                service - The IP service name. 
                
                non_blocking - Bool, if socket should be non_blocking or blocking

    RETURNS:    SOCKERR - RC_SUCCESS if successful, RC_FAIL if not.

********************************************************************/
SOCKERR CreateSocket( SOCKET FAR * psock,
                      int          type,
					  LPSTR		   service,
					  short		   non_blocking )
{
    int rc=RC_SUCCESS;
    SOCKET  sNew;
    SOCKERR serr;
    SOCKADDR_IN sockAddr;
    int broadcast_bool=1;
	char buf[132];    
//	int ilLc;
      

#ifdef GETHOSTBYNAME_WORKS                   
    LPHOSTENT lpHost;
#endif
#ifdef GETSERVBYNAME_WORKS
    LPSERVENT lpServEnt;
#endif
    //
    //  Create the socket.
    //

    sNew = socket( AF_INET, type, 0 );
    serr = ( sNew == INVALID_SOCKET)  ? WSAGetLastError() : 0;
    
    if( serr == 0 )
    {
#ifdef GETHOSTBYNAME_WORKS                   
		rc = GetWorkstationName ((LPSTR) buf);
    	if (rc == RC_SUCCESS)
    	{
	    	lpHost = gethostbyname (buf);
    	} /* fi */
    	if (rc != RC_SUCCESS || lpHost != NULL)
    	{
	        //
    	    //  Bind an address to the socket.
        	//

			memcpy((char FAR *)&sockAddr.sin_addr,
				  	(char FAR *) *(lpHost->h_addr_list),	lpHost->h_length);

/*****************
			sprintf(trace_buf,
			"CreateSocket:Debug gethostbyname: name=%Fs sin_addr = 0x%lx len=%d\n",
				lpHost->h_name, 
				(u_long) (lpHost->h_addr), lpHost->h_length );
				*********************/
			sprintf(trace_buf,
			"CreateSocket:Debug gethostbyname: Name=%Fs Adress-Length=%d\n",
				lpHost->h_name, lpHost->h_length);
				WriteTrace(trace_buf, WT_ERROR);
			for(ilLc = 0; lpHost->h_addr_list[ilLc] != NULL; ilLc++)
			{
				sprintf(trace_buf,"CreateSocket: Adress No %d: %s",ilLc,
				 inet_ntoa(*((struct in_addr *)lpHost->h_addr_list[ilLc])));
				WriteTrace(trace_buf, WT_ERROR);
			}
#else
		if (1)  /* gethostbyname does not work */
		{	
            sockAddr.sin_addr.s_addr = inet_addr((LPSTR) MyIpAddr);
			sprintf(trace_buf,
			"CreateSocket:Debug: net addr = 0x%lx <%s> \n",
				sockAddr.sin_addr.s_addr,MyIpAddr);
			WriteTrace(trace_buf, WT_DEBUG);
#endif        			
        	sockAddr.sin_family  = AF_INET;
        	memset (sockAddr.sin_zero, '\0', sizeof (sockAddr.sin_zero)); 

/************************ TEST *******************************/
			// Thommy's Adresse
 // 	            sockAddr.sin_addr.s_addr = inet_addr((LPSTR) "192.9.200.142");
		
#ifdef GETSERVBYNAME_WORKS
/*  Does not work ?!  */
        	lpServEnt = getservbyname (service, NULL); 
        	if (lpServEnt != NULL)
        	{
	        	sockAddr.sin_port        = lpServEnt->s_port;
#else                                                
#ifdef USED_FOR_SYMAP
        	if (1)
        	{       
	        		sockAddr.sin_port        = htons (SYMAP_PORT_NO);
#else
        	if (1)
        	{       
        		if (non_blocking == TRUE)
	        		sockAddr.sin_port        = htons (UfisBcPortNo);
				else
	        		sockAddr.sin_port        = htons (UfisBcPortNo);
#endif	        		
#endif	
    	
        		if( bind( sNew, (LPSOCKADDR)&sockAddr, sizeof(SOCKADDR_IN)) 
        				!= 0 )
   		    	{
					serr = WSAGetLastError();
					sprintf(trace_buf,"CreateSocket:bind: %Fs\n", 
						SockerrToString( serr ));
					WriteTrace(trace_buf, WT_ERROR);    
				}
				else
				{    
					serr = SetNonBlocking (sNew, non_blocking);
					if (serr == 0 && non_blocking == TRUE)
					{
						if ( setsockopt (sNew, SOL_SOCKET, SO_BROADCAST, 
									(char FAR *) &broadcast_bool, 
		   							sizeof (broadcast_bool)) != 0)
		   				{
							serr = WSAGetLastError();
							sprintf(trace_buf,"CreateSocket:setsockopt: %Fs\n", 
								SockerrToString( serr ));
							WriteTrace(trace_buf, WT_ERROR); 
						}
						else
							serr = 0;
					}  /* end if */ 

				} /* end if bind */
    		}
			else
			{
				serr = WSAGetLastError();
				sprintf(trace_buf,"CreateSocket:getservbyname: %Fs \n", 
					SockerrToString( serr ) );
				WriteTrace(trace_buf, WT_ERROR);
			}   /* end if getservbyname */
        }  /* end if gethostbyname */
		else
		{
			serr = WSAGetLastError();
			sprintf(trace_buf,"CreateSocket:Gethostbyname: %Fs  %s\n", 
				SockerrToString( serr ), buf );
			WriteTrace(trace_buf, WT_ERROR);
		}
    }  
	else
	{
		serr = WSAGetLastError();
		sprintf(trace_buf,"CreateSocket:socket: %Fs \n", 
			SockerrToString( serr ) );
		WriteTrace(trace_buf, WT_ERROR);
    }  /* end if socket */
    
	sprintf(trace_buf,"CreateSocket:parameter: sin_port = %d \n", ntohs(sockAddr.sin_port));
	WriteTrace(trace_buf, WT_ERROR);

    if( serr != 0 )
    {
        ResetSocket( sNew );
        sNew = INVALID_SOCKET;
	    return RC_COMM_FAIL;
    }
    else
    { 
    	*psock = sNew;
		sprintf(trace_buf,"CreateSocket:%s blocking socket %d created\n", 
			non_blocking == TRUE ? "non" : "", sNew);
		WriteTrace(trace_buf, WT_DEBUG);
    	return RC_SUCCESS;
    }
    
}   // CreateSocket

/* ******************************************************************** */
/* Following the GetWorkstationName function							*/
/* Returns the Host name of the Workstation configured as local host		*/
/* ******************************************************************** */
int far pascal GetWorkstationName (LPSTR ws_name)
{
	int rc = RC_SUCCESS;
	SOCKERR serr;                      

	if ( rc = gethostname( ws_name,130) == SOCKET_ERROR ) {
		serr = WSAGetLastError();
		sprintf(trace_buf,"GetWorkstationName:gethostname: %Fs\n", 
			SockerrToString( serr ));
		WriteTrace(trace_buf, WT_ERROR);
		return RC_FAIL;
	} /* end if */

	return rc;
} /* GetWorkstationName */	          



/*******************************************************************

    NAME:       OpenConnection

    SYNOPSIS:   Opens a connection to the specified host & port on the
    			specified socket.


    RETURNS:    RC_SUCCESS if successful, RC_FAIL if not.

********************************************************************/
int OpenConnection (SOCKET socket, LPSTR hostname, short remote_port_no)
{
		int rc=RC_FAIL;
/*		HOSTENT FAR *hp; */
		LPHOSTENT hp; 
		SOCKADDR_IN	dummy;
		SOCKERR serr=0;
#ifdef GETSERVBYNAME_WORKS
    	LPSERVENT sp;
#endif

		if ( ( hp = gethostbyname( hostname)) == NULL ) {
			serr = WSAGetLastError();
			sprintf(trace_buf,"OpenConnection:Gethostbyname: %Fs  %Fs\n", 
				SockerrToString( serr ), hostname );
			WriteTrace(trace_buf, WT_ERROR);
			return RC_FAIL;
		} /* end if */

		memcpy((char FAR *)&dummy.sin_addr,
				*hp->h_addr_list,
				 hp->h_length);
				 
#ifdef GETSERVBYNAME_WORKS
		/*    Doesn't work  ... */
		sprintf(trace_buf,"NETIF:got host by name\n"); WriteTrace(trace_buf, WT_DEBUG);
		if ( ( sp = getservbyname((char FAR *) "UFIS",NULL) ) == NULL ) {
			serr = WSAGetLastError();
			sprintf(trace_buf,"OpenConnection: Getservbyname: %s\n", SockerrToString( serr ) );
			WriteTrace(trace_buf, WT_ERROR);
			return RC_FAIL;
		} 
//		sprintf(trace_buf,"adr hp = 0x%lx sp = 0x%lx\n",hp,sp); WriteTrace(trace_buf, WT_DEBUG);
		
		dummy.sin_port   = sp->s_port;
		sprintf(trace_buf,"OpenConnection:got service UFIS\n"); WriteTrace(trace_buf, WT_DEBUG);

#else
		dummy.sin_port   = htons (remote_port_no);
#endif

		dummy.sin_family = AF_INET;
		
		if ((rc = connect(socket,(SOCKADDR FAR *) &dummy,sizeof(dummy))) <0) 
		{
			sprintf(trace_buf,"OpenConnection: ConnectFailed: \n");
			WriteTrace(trace_buf, WT_ERROR);    
			
			serr = WSAGetLastError();
			sprintf(trace_buf,"OpenConnection: Connect: %s\n", SockerrToString( serr ) );
			WriteTrace(trace_buf, WT_ERROR);    
			return RC_FAIL;
		} /* end if */

		sprintf(trace_buf,"OpenConnection:got connection\n"); WriteTrace(trace_buf, WT_DEBUG);
	
 		return RC_SUCCESS;                                         
}  /* OpenConnection */                                         



/*******************************************************************

    NAME:       CleanupWinsock

    SYNOPSIS:   Ends the Socket communication


    RETURNS:    RC_SUCCESS if successful, RC_FAIL if not.

********************************************************************/
int CleanupWinsock (void)
{
	SOCKERR serr=0;
	char timebuf[20];	
	 
    serr = WSACleanup ();  
    if (serr != 0)
    {
		serr = WSAGetLastError();
		sprintf(trace_buf,"Cleanup: %s\n",SockerrToString( serr ) );
		WriteTrace(trace_buf, WT_DEBUG);
		return RC_FAIL;	
	}
	else
	{	
		/* Date and Time to trace file */	              
		_strdate (timebuf);   
		timebuf[8] = ' ';
		_strtime (timebuf + 9);
		fprintf(outpt, "UFIS-DLL: CleanupWinsock: Ends %s\n", timebuf);

		return RC_SUCCESS;	
	}
}


#ifdef FUTUR

/* ******************************************************************** */
/* Following the ForwardData function					*/
/* ******************************************************************** */
int ForwardData( SOCKET sock, LPSTR hostname, short port_no, LPSTR data, 
						int length)
{
	int	rc;
	LPSTR errstr;
	SOCKERR sockerr;
    SOCKADDR_IN sockAddr;         
	HOSTENT FAR *hp;


	if ( ( hp = gethostbyname( hostname)) == NULL ) {
		sockerr = WSAGetLastError();
		sprintf(trace_buf,"ForwardData:Gethostbyname: %Fs  %Fs\n", 
			SockerrToString( sockerr ), hostname );
		WriteTrace(trace_buf, WT_ERROR);
		return RC_FAIL;
	} /* end if */
			
	sprintf(trace_buf,
	"ForwardData:Debug: name=%Fs sin_addr = 0x%lx len=%d\n",
		hp->h_name, 
		(u_long) (hp->h_addr), hp->h_length );
	WriteTrace(trace_buf, WT_DEBUG);

	memcpy((char FAR *)&sockAddr.sin_addr,
			  *hp->h_addr_list,
			  hp->h_length);
    sockAddr.sin_family      = AF_INET;
    sockAddr.sin_port        = htons (port_no);
    
	rc =RC_FAIL;
	
	rc = sendto(sock,data,length,0, (SOCKADDR FAR *) &sockAddr, 
				sizeof (SOCKADDR_IN));
	if ( rc <0 ) {
		sockerr = WSAGetLastError();
		errstr = SockerrToString (sockerr);
	  	sprintf(trace_buf,"ForwardData: sendto: socket = 0x%x: %s\n", sock,
	  		errstr);
		WriteTrace(trace_buf, WT_ERROR);
	  	sprintf(trace_buf,"ForwardData: rc = %d : str=%s \n"
		  ,rc,strerror(errno));
		WriteTrace(trace_buf, WT_ERROR);
	} /* end if */
	else {
		sprintf(trace_buf,"ForwardData: %d bytes send \n",rc);
		WriteTrace(trace_buf, WT_DEBUG);
	} /* end else */
	
	if (rc < 0 )
		return RC_FAIL;
	else
		return RC_SUCCESS;
} /* end of forward */

#endif /* FURUR */

  
/* ******************************************************************** */
/* Following the ReceiveData function									*/ 
/*																		*/ 
/* Gets the length of the buffer packet in *length and returns the 		*/
/* number of bytes got there. If the rc is RC_SUCCESS and the returned	*/
/* length is 0, no data has been arrived.								*/
/* if the rc is RC_SHUTDOWN, the connection has been closed by the other*/
/* host.																*/
/* ******************************************************************** */ 

int ReceiveData( SOCKET sock, LPSTR packet, int FAR *length)
{
	int	rc = RC_SUCCESS; 
	int cnt=0;
	int r_len=0;
	SOCKERR serr;                      
	SOCKADDR_IN remote_addr;
	int remote_len=sizeof(SOCKADDR_IN);
	LPSTR cur_packet=packet;
	COMMIF FAR *Packet;
	
	for (r_len=0; rc == RC_SUCCESS && r_len < PACKET_LEN; r_len += cnt, cur_packet += cnt)
	{	
		*length = PACKET_LEN - r_len;
  		cnt = recvfrom (sock, cur_packet, *length, 0, 
						  (struct sockaddr FAR *) &remote_addr, 
						  (int FAR *) &remote_len);
		if (cnt == SOCKET_ERROR)
		{
			serr = WSAGetLastError();
			sprintf(trace_buf,"ReceiveData: Recvfrom: %s  socket=%d",SockerrToString( serr ),sock );
			WriteTrace(trace_buf, WT_ERROR);	
			if (serr == WSAEWOULDBLOCK)
			{
				rc = RC_NOT_FOUND;
			}
			else
			{
				rc = RC_FAIL ;
			} /* end if */
		}
		else
		{   
			if (cnt == 0)
			{
				rc = RC_NOT_FOUND;
				sprintf(trace_buf,
					"ReceiveData: 0 Bytes received from ip 0x%lx remote_len=%d", 
					remote_addr.sin_addr.s_addr, remote_len);
				WriteTrace(trace_buf, WT_ERROR);
			}
			else
			{
				sprintf(trace_buf,
					"ReceiveData: received  %d bytes from ip 0x%lx",
			 		cnt, remote_addr.sin_addr.s_addr); 
				WriteTrace(trace_buf, WT_DEBUG);
			} /* fi */
		} /* fi */                 
		if (rc == RC_SUCCESS)
		{
			if (dZine == 1)
			{
				r_len += cnt;
				break;
			}
			else
			{
				Packet = (COMMIF FAR *) cur_packet;
				if (Packet->command == 0x5a64 || Packet->command == 0x4943 ||
					Packet->command == 0x6954)		// message from or to dZine controller
				{									// ignore message
					sprintf(trace_buf,"ReceiveData: Ignore received data from or to dZine controller");
					WriteTrace(trace_buf, WT_DEBUG);
					cnt = 0;
					r_len += cnt;
					rc = RC_NOT_FOUND;
					break;
				}
			}
		}
    } /* for */
    
    if (rc == RC_SUCCESS)
    {
    	*length = r_len;
    } 
	else
	{
		*length = 0;
    } /* fi */
    
	return rc;
}  /* ReceiveData */

// -----------------------------------------------------------------
//
// Function: SetBlocking
//
// Purpose : Sets a socket to blocking or non blocking mode
//
// Params  : socket, bool non blocking
//
// Returns : RC_SUCCESS
//			 RC_NOT_FOUND 	- Buffer empty
//
// Comments: 
//           
// -----------------------------------------------------------------

int SetNonBlocking (SOCKET socket, short non_blocking)
{
	int rc=RC_SUCCESS;
    u_long block;
    SOCKERR serr=0;

	if (non_blocking == TRUE)
		block = 1;
	else
		block = 0;

    if( ioctlsocket( socket, FIONBIO, (u_long FAR *) &block) != 0 )
   	{
		serr = WSAGetLastError();
		sprintf(trace_buf,"SetNonBlocking:ioctlsocket: %Fs\n", 
			SockerrToString( serr ));
		WriteTrace(trace_buf, WT_ERROR);    
	}/* end if ioctlsocket */  
    
	sprintf(trace_buf, "SetNonBlocking: non_blocking = %d  rc = %d socket = %d", 
		non_blocking, rc, socket);
	WriteTrace(trace_buf, WT_DEBUG);
	
    return rc;
}  /* SetBlocking */

/* ******************************************************************** */
/* Following the bchead_hton function									*/
/* Converts from network to host order 									*/
/* ******************************************************************** */
void bchead_hton  (BC_HEAD FAR *Pbc_head)
{
#ifdef USE_BCHEAD_NETWORK_BYTE_ORDER

  Pbc_head->bc_num = htons (Pbc_head->bc_num);
  Pbc_head->tot_buf = htons (Pbc_head->tot_buf);
  Pbc_head->act_buf = htons (Pbc_head->act_buf);
  Pbc_head->rc = htons (Pbc_head->rc);
  Pbc_head->tot_size = htons (Pbc_head->tot_size);
  Pbc_head->cmd_size = htons (Pbc_head->cmd_size);
  Pbc_head->data_size = htons (Pbc_head->data_size);

#endif
}  /* bchead_hton */


/* ******************************************************************** */
/* Following the bchead_hton function									*/
/* Converts from network to host order 									*/
/* ******************************************************************** */
void bchead_ntoh  (BC_HEAD FAR *Pbc_head)
{
#ifdef USE_BCHEAD_NETWORK_BYTE_ORDER

  Pbc_head->bc_num = ntohs (Pbc_head->bc_num);
  Pbc_head->tot_buf = ntohs (Pbc_head->tot_buf);
  Pbc_head->act_buf = ntohs (Pbc_head->act_buf);
  Pbc_head->rc = ntohs (Pbc_head->rc);
  Pbc_head->tot_size = ntohs (Pbc_head->tot_size);
  Pbc_head->cmd_size = ntohs (Pbc_head->cmd_size);
  Pbc_head->data_size = ntohs (Pbc_head->data_size);

#endif
}  /* bchead_hton */
	