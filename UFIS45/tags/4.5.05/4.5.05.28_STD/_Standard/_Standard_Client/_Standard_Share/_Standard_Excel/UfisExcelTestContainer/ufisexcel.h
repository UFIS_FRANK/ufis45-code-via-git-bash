#if !defined(AFX_UFISEXCEL_H__CB49C1C0_0996_45BB_872B_E88F2CD3ABF4__INCLUDED_)
#define AFX_UFISEXCEL_H__CB49C1C0_0996_45BB_872B_E88F2CD3ABF4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Machine generated IDispatch wrapper class(es) created by Microsoft Visual C++

// NOTE: Do not modify the contents of this file.  If this class is regenerated by
//  Microsoft Visual C++, your modifications will be overwritten.

/////////////////////////////////////////////////////////////////////////////
// CUfisExcel wrapper class

class CUfisExcel : public CWnd
{
protected:
	DECLARE_DYNCREATE(CUfisExcel)
public:
	CLSID const& GetClsid()
	{
		static CLSID const clsid
			= { 0xd8a28baa, 0x524f, 0x4098, { 0x95, 0x58, 0xcd, 0xcc, 0x53, 0xc4, 0x83, 0x4 } };
		return clsid;
	}
	virtual BOOL Create(LPCTSTR lpszClassName,
		LPCTSTR lpszWindowName, DWORD dwStyle,
		const RECT& rect,
		CWnd* pParentWnd, UINT nID,
		CCreateContext* pContext = NULL)
	{ return CreateControl(GetClsid(), lpszWindowName, dwStyle, rect, pParentWnd, nID); }

    BOOL Create(LPCTSTR lpszWindowName, DWORD dwStyle,
		const RECT& rect, CWnd* pParentWnd, UINT nID,
		CFile* pPersist = NULL, BOOL bStorage = FALSE,
		BSTR bstrLicKey = NULL)
	{ return CreateControl(GetClsid(), lpszWindowName, dwStyle, rect, pParentWnd, nID,
		pPersist, bStorage, bstrLicKey); }

// Attributes
public:

// Operations
public:
	BOOL GenerateFromCSV(LPCTSTR FileName);
	BOOL GenerateFromXSL(LPCTSTR FileName);
	BOOL GenerateFromTabCtrl(LPDISPATCH ITabCtrl);
	BOOL GenerateFromTab(long HTabCtrl);
	void AboutBox();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UFISEXCEL_H__CB49C1C0_0996_45BB_872B_E88F2CD3ABF4__INCLUDED_)
