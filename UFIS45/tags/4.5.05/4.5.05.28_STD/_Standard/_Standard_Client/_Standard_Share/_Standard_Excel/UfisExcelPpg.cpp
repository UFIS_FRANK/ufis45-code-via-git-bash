// UfisExcelPpg.cpp : Implementation of the CUfisExcelPropPage property page class.

#include "stdafx.h"
#include "UfisExcel.h"
#include "UfisExcelPpg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CUfisExcelPropPage, COlePropertyPage)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CUfisExcelPropPage, COlePropertyPage)
	//{{AFX_MSG_MAP(CUfisExcelPropPage)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CUfisExcelPropPage, "UFISEXCEL.UfisExcelPropPage.1",
	0x107bffe0, 0x8521, 0x4b55, 0x85, 0x37, 0x64, 0x96, 0xa9, 0xfa, 0x3b, 0x73)


/////////////////////////////////////////////////////////////////////////////
// CUfisExcelPropPage::CUfisExcelPropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CUfisExcelPropPage

BOOL CUfisExcelPropPage::CUfisExcelPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_UFISEXCEL_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CUfisExcelPropPage::CUfisExcelPropPage - Constructor

CUfisExcelPropPage::CUfisExcelPropPage() :
	COlePropertyPage(IDD, IDS_UFISEXCEL_PPG_CAPTION)
{
	//{{AFX_DATA_INIT(CUfisExcelPropPage)
	// NOTE: ClassWizard will add member initialization here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_INIT
}


/////////////////////////////////////////////////////////////////////////////
// CUfisExcelPropPage::DoDataExchange - Moves data between page and properties

void CUfisExcelPropPage::DoDataExchange(CDataExchange* pDX)
{
	//{{AFX_DATA_MAP(CUfisExcelPropPage)
	// NOTE: ClassWizard will add DDP, DDX, and DDV calls here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_MAP
	DDP_PostProcessing(pDX);
}


/////////////////////////////////////////////////////////////////////////////
// CUfisExcelPropPage message handlers
