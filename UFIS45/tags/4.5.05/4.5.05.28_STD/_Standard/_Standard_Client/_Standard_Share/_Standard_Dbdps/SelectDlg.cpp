// SelectDlg.cpp : implementation file
//

#include <stdafx.h>
#include <DBDPS.h>
#include <SelectDlg.h>
#include <DBDPSDoc.h>
#include <DBDPSView.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelectDlg dialog


CSelectDlg::CSelectDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelectDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

void CSelectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	DDX_Control(pDX, IDC_LIST_TABELLEN, m_ctlList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelectDlg, CDialog)
	//{{AFX_MSG_MAP(CSelectDlg)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_TABELLEN, OnDblclkListTabellen)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectDlg message handlers and functions

//-----------------------------------------------------------------------
// OnInitDialog
//	
//	The function is called during initializing of the dialog
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 30.06.1999 | Martin Danihel    | Added security functionality
//-----------------------------------------------------------------------
BOOL CSelectDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CString myTableName;
	CString *myLongTableName;
	CString myStatus;	// security status
	int j = 0;

	for(int i = 0; i < gTables.GetSize(); i++)
	{
		myTableName = gTables.GetAt(i);

		myStatus = GetSecurityStatus(myTableName + gTableExt, _T("FILE_OPEN"));
		if(myStatus == SEC_STAT_ACT)	// add table to the list of available tables
		{
			if (!gLongTableNames.Lookup(myTableName + gTableExt, myLongTableName))	// if a long name exists set this
			{
				AfxMessageBox("CSelectDlg::OnInitDialog(). Long table name not found.", MB_OK + MB_ICONSTOP);
				return FALSE;
			}
			m_ctlList.InsertItem(j, *myLongTableName);
			j++;
		}
	}

	return TRUE;
}

//-----------------------------------------------------------------------
// OnOK
//	
//	The function sets the member variable m_myChoose with the selected
// list item
//-----------------------------------------------------------------------
void CSelectDlg::OnOK() 
{
	UINT myMask = LVIS_SELECTED;
	BOOL myFound = FALSE;
	m_myChoose.Empty();

	for (int i = 0; i < m_ctlList.GetItemCount(); i++)
	{
		if (m_ctlList.GetItemState(i, myMask))
		{
			CString myTableName;
			CString *myLongTableName;
			CString mySelectLongTableName = m_ctlList.GetItemText(i, 0);
			for(int j = 0; j < gTables.GetSize(); j++)
			{
				myTableName = gTables.GetAt(j);
				if (!gLongTableNames.Lookup(myTableName + gTableExt, myLongTableName))	// if a long name exists set this
				{
					AfxMessageBox("OnOK(). Long table name not found.", MB_OK + MB_ICONSTOP);
					return;
				}
				if(*myLongTableName == mySelectLongTableName)
				{
					m_myChoose = myTableName;
					myFound = TRUE;
				}
				if(myFound) break;
			}
		}
		if(myFound) break;
	}
	
	CDialog::OnOK();
}

//-----------------------------------------------------------------------
// OnDblclkListTabellen
//	
//	The function is equivalent to function OnOK
//-----------------------------------------------------------------------
void CSelectDlg::OnDblclkListTabellen(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnOK();
}

