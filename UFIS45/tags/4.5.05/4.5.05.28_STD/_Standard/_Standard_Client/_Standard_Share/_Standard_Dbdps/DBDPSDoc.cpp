// DBDPSDoc.cpp : implementation of the CDBDPSDoc class

#include <afxdisp.h>
#include <gxall.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <TCHAR.h>
#include <wchar.h>
#include <winnls.h>
#include <winbase.h>
#include <stdafx.h>
#include <DBDPS.h>
#include <DBDPSDoc.h>
#include <DBDPSView.h>
#include <Globals.h>
#include <LoginDlg.h>
#include <RegisterDlg.h>

#ifdef ABBCCS_RELEASE
#include <UfisCEDA.h>
#else 
#include <UfisSWH.h>
#endif

#ifdef ABBCCS_RELEASE
#include <CCSBcHandle.h>
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDBDPSDoc

IMPLEMENT_DYNCREATE(CDBDPSDoc, CDocument)

BEGIN_MESSAGE_MAP(CDBDPSDoc, CDocument)
	//{{AFX_MSG_MAP(CDBDPSDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CDBDPSDoc construction/destruction

CDBDPSDoc::CDBDPSDoc()
{
	if (!GetLocaleSettings())		// load some locales
	{	
		AfxMessageBox("GetLocaleSettings() - CRASH.", MB_OK + MB_ICONSTOP);
		exit(0);
	}
	
	CLoginDlg loginDlg;

	bool bLoginOK=FALSE;

	if(loginDlg.DoModal() == IDOK)
	{
		//*** 15.11.99 SHA ***
		CreateLogFile(gSystabLogFile);

		if (!LoadSysInfo(gTableExt))	// load SYSTAB
		{
			CString olMessage;
			olMessage.Format("An error ocurred while reading the SYSTAB. Check the file <%s> for details.",gSystabLogFile);
			//AfxMessageBox("LoadSysInfo() - CRASH.", MB_OK + MB_ICONSTOP);
			AfxMessageBox(olMessage, MB_OK + MB_ICONSTOP);
			//*** 15.11.99 SHA ***
			CloseLogFile();

			GlobalFree(gDataPtr);
			GlobalFree(gSecPtr);
			exit(0);
		}

		//*** 16.11.99 SHA ***
		//*** PREPARE PATH FOR USER-SETTINGS ***
		if (gSettingsPath!="#")
			gSettingsPath = gSettingsPath + "DBDPS_" + gUserName +".INI";


		GlobalFree(gDataPtr);
		//*** 15.11.99 SHA ***
		CloseLogFile();

		if (!LoadSecurityInfo())
		{
			AfxMessageBox("LoadSecurityInfo() - CRASH.", MB_OK + MB_ICONSTOP);
			GlobalFree(gSecPtr);
			exit(0);
		}

		SetDefaultPriority();

		LPTSTR DataPtr = gSecPtr;
		TCHAR Value[MAX_VALUE_LENGTH];
		LPTSTR ValuePtr = Value;
		CString ObjectId;
		CString ControlId;
		CString Status;
		int myRowCount = gSecRowCount;
		int myColumnCount = 3;
		int position;

		for(int i = 0; i < myRowCount; i++)
		{
			for(int j = 0; j < myColumnCount; j++)
			{
				if(!GetToken(DataPtr, ValuePtr) && 
						(i != myRowCount - 1) && (j != myColumnCount - 1))
				{
					AfxMessageBox("Login(). CallCeda returned invalid data buffer.", MB_OK + MB_ICONSTOP);
					GlobalFree(gSecPtr);
					exit(0);
				}

				if(!_tcscmp(Value, DB_NULL_VALUE))
					*Value = 0;

				CString myTempValue(ValuePtr);
				ConvertStringToClient(myTempValue);
				_tcscpy(ValuePtr, myTempValue);

				if(j == 0 && (!_tcscmp(Value, _T("[PRV]"))))
					break;	// breaks to the next row from buffer

				// copy values
				switch(j)
				{
					case 0:
	//					ObjectId = Value;
	//					ObjectId.TrimLeft();
	//					ObjectId.TrimRight();
						break;
					case 1:
						ControlId = Value;
						ConvertStringToClient(ControlId);
						ControlId.TrimLeft();
						ControlId.TrimRight();
						break;
					case 2:
						Status = Value;
						Status.TrimLeft();
						Status.TrimRight();

						if (!_tcscmp(ControlId, _T("InitModu")))
						{
							if(!_tcscmp(Status, SEC_STAT_ACT))
								gRegisteringApp = TRUE;
						}
						else
						{
							position = ControlId.Find(SECURITY_SEPARATOR);
							if(position == -1)
								AfxMessageBox(_T("Login(). No security separator found."), MB_OK + MB_ICONSTOP);
							else
							{
								ObjectId = ControlId.Left(position);
								ObjectId.TrimLeft();
								ObjectId.TrimRight();
								ControlId = ControlId.Right(ControlId.GetLength() - 
									position - 1);
								ControlId.TrimLeft();
								ControlId.TrimRight();

								if((!_tcscmp(ObjectId, _T("InitModu"))) &&
									(!_tcscmp(ControlId, _T("InitModu"))) &&
									(!_tcscmp(Status, SEC_STAT_ACT)))
									gRegisteringApp = TRUE;
								else
									SetSecurityStatus(ObjectId, ControlId, Status);
							}
						}
				}
			}
		}

		GlobalFree(gSecPtr);

		if(gRegisteringApp)
		{
			CRegisterDlg registerDlg;
			switch(registerDlg.DoModal())
			{
				case IDYES :
				{
					AfxGetApp()->DoWaitCursor(1);
					if(!RegisterApp())
					{
						AfxGetApp()->DoWaitCursor(-1);
						AfxMessageBox("RegisterApp() - CRASH.", MB_OK + MB_ICONSTOP);
					}
					else
						AfxGetApp()->DoWaitCursor(-1);
					exit(0);
				}
				case IDCANCEL :
					exit(0);
			}
		}
	}
	else
		exit(0);
	
	gURNONumBufIndex = MAX_URNOS;		// that means no free URNO is available

#ifdef ABBCCS_RELEASE
	gDdx.Register((void *) this, DBDPS_BC_NEW, CString("DBDPSDATA"), CString("DBDPSDATA-NEW"),
		ProcessBroadcasts);
	gDdx.Register((void *) this, DBDPS_BC_CHANGE, CString("DBDPSDATA"), CString("DBDPSDATA-UPDATE"),
		ProcessBroadcasts);
	gDdx.Register((void *) this, DBDPS_BC_DELETE, CString("DBDPSDATA"), CString("DBDPSDATA-DELETE"),
		ProcessBroadcasts);


	//*** 20.10.99 SHA ***
	gDdx.Register((void *)this,BC_PST_CHANGE,CString("PSTDATA"), CString("PST-changed")
		,ProcessBroadcasts);


#endif

// MAIN security info
	gCanMainInsertRow = GetSecurityStatus(_T("MAIN"), _T("INSERT_ROW")) == CString(SEC_STAT_ACT);
	gCanMainUpdateRow = GetSecurityStatus(_T("MAIN"), _T("UPDATE_ROW")) == CString(SEC_STAT_ACT);
	gCanMainDeleteRow = GetSecurityStatus(_T("MAIN"), _T("DELETE_ROW")) == CString(SEC_STAT_ACT);

	if (!LoadSecFieldInfo())
	{
		AfxMessageBox("LoadSecFieldInfo() - CRASH.", MB_OK + MB_ICONSTOP);
		exit(0);
	}
}

CDBDPSDoc::~CDBDPSDoc()
{
	ClearDataStructure();
	ClearRecordset(&mChangingRecord);
	ClearSysInfo();
	ClearSecurityInfo();
}

BOOL CDBDPSDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;
	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)
	SetTitle("No table");
	return TRUE;
}

//-----------------------------------------------------------------------
// FillDataStructure()
//	
//	The function loads the particular table data and fills the gGRTable
//-----------------------------------------------------------------------
bool CDBDPSDoc::FillDataStructure
	(
		LPCTSTR pTableName,		// the table name
		LPCTSTR pExtension,		// the table extension
		LPCTSTR pWhere				// the where clause
	)
{
//h - start
	CWaitCursor myWait;
//h - end

	POSITION pos = GetFirstViewPosition();		
	if (pos != NULL)
	{
		CView* pView = GetNextView(pos);
		ASSERT(pView->IsKindOf(RUNTIME_CLASS(CDBDPSView)));
		if (((CDBDPSView*)pView)->GetCurrentCellControl())	// if there is already existing table
																// then repair a possible bad cursor position
			((CDBDPSView*)pView)->SetCurrentCell(1, 1, GX_NOSETFOCUS | GX_INVALIDATE);
		
		ClearDataStructure();		// clear data
		SetTitle("No table");

		((CDBDPSView*)pView)->OnQueryChange();			// update the grid view
	}

	if (!gTablesDesc.Lookup(pTableName, gCurrentFieldsDesc)) // look for the table description
	{
		CString OutputStr;
		CString SysTable(SYSTAB + CString(pExtension));
		OutputStr.Format(_T("Table %s is not descripted in the %s."), 
								pTableName,
								SysTable);
		AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);

		return false;
	}

	CString SelectFields(SYS_FIELD[F_URNO]);		// start to fill it with "URNO"
	CString Table(CString(pTableName) + pExtension);	// add extension

	for (int i = 0; i < gCurrentFieldsDesc->GetSize(); i++)		// build the select clause
	{	
		SelectFields += DELIMITER_CHAR;
		SelectFields += gCurrentFieldsDesc->GetAt(i)->mFieldName;
	}

	int ret = DBDPSCallCeda(UFIS_SELECT, Table, NULL,
							 pWhere, SelectFields, NULL);
	
	if (ret == RC_NOT_FOUND)
	{
		gCanTableUpdateFields = true;
		bool myInvisibleAllFields = true;
		for (int i = 0; i < gCurrentFieldsDesc->GetSize(); i++)
		{
			CFieldInfo *myFieldInfo = gCurrentFieldsDesc->GetAt(i);
			switch (myFieldInfo->mLGType)
			{
				case LG_CDAT:
				case LG_LSTU:
				case LG_CUSR:
				case LG_USER:
					continue;
					break;
				default:
					if (myFieldInfo->mReadOnly || !myFieldInfo->mVisible)
						gCanTableUpdateFields = false;
					if (myFieldInfo->mVisible)
						myInvisibleAllFields = false;	// at least one field is visible
					break;
			}
		}

		if (!gCanTableUpdateFields)
		{
			CString OutputStr;
			OutputStr.Format(_T("No available rows in %s."), (LPCTSTR)Table);
			AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);

			return false;
		}
		
		if (myInvisibleAllFields)
		{
			CString OutputStr;
			OutputStr.Format(_T("No visible columns in %s."), (LPCTSTR)Table);
			AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);

			return false;
		}
		
		gTableName = pTableName;
		CString *myLongTableName;
		if (!gLongTableNames.Lookup(gTableName + gTableExt, myLongTableName)) 
			myLongTableName = &gTableName;
		SetTitle(*myLongTableName);

		gWhereClause = pWhere;
		gTableLoad = true;		

#ifdef ABBCCS_RELEASE
		gBcHandle.AddTableCommand(gTableName + gTableExt, UFIS_INSERT, DBDPS_BC_NEW);
		gBcHandle.AddTableCommand(gTableName + gTableExt, UFIS_UPDATE, DBDPS_BC_CHANGE);
		gBcHandle.AddTableCommand(gTableName + gTableExt, UFIS_DELETE, DBDPS_BC_DELETE);
#endif

		// TABLE security info
		gCanTableInsertRow = GetSecurityStatus(pTableName + gTableExt, _T("INSERT_ROW")) == CString(SEC_STAT_ACT);
		gCanTableUpdateRow = GetSecurityStatus(pTableName + gTableExt, _T("UPDATE_ROW")) == CString(SEC_STAT_ACT);
		gCanTableDeleteRow = GetSecurityStatus(pTableName + gTableExt, _T("DELETE_ROW")) == CString(SEC_STAT_ACT);

		return true;	//empty query
	}

	if (ret)
	{
		CString OutputStr;
		OutputStr.Format(_T("Error on reading %s. ") + gErrorMessage + POINT_CHAR, (LPCTSTR)Table);
		AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);

		return false;
	}

	HGLOBAL myHandle;
	GetBufferHandle(UFIS_BUF, &myHandle);
	LPTSTR DataPtr = (LPTSTR)GlobalLock(myHandle);		// data from the SYSTAB
	
	TCHAR Value[MAX_VALUE_LENGTH];
	LPTSTR ValuePtr = Value;			

	int nCols = gCurrentFieldsDesc->GetSize();	
	int nRows = GetNumberOfLines(UFIS_BUF);

	int CurrentURNO;
	int row;
	for(row = 0; row < nRows; row++)
	{
		CGRRecordset *myRecordset = new CGRRecordset;
		for(int col = -1; col < nCols; col++)		// starting from -1 because of URNO
		{
			ValuePtr = Value;		
			if (!GetToken(DataPtr, ValuePtr) && col != nCols - 1 && row != nRows - 1)	// GetToken finished earlier
			{
				AfxMessageBox("FillDataStructure(). CallCeda returned invalid data buffer.", MB_OK + MB_ICONSTOP);
				ClearRecordset(myRecordset);
				delete myRecordset;
				ClearDataStructure();

				return false;
			}

			if (!_tcscmp(Value, DB_NULL_VALUE))	// Null value?
				*Value = 0;

			if (col == -1)		// it is URNO
			{
				CurrentURNO = _ttoi(Value);
				if (CurrentURNO)
				{
					int *myIndex = new int;
					int *myUrno = new int;
					*myUrno = CurrentURNO;				// urno
					*myIndex = mIndexMap.GetSize();	// order 
					mIndexMap.Add(myIndex);
					gUrnoMap.Add(myUrno);
				}
			}
			else
			{
				CFieldInfo *myFieldInfo = gCurrentFieldsDesc->GetAt(col);
				CGRCell *myGRCell = new CGRCell;
				TransDBToCell(myGRCell, myFieldInfo, Value); 
				myRecordset->Add(myGRCell);				// add the cell to recordset
			}
		}
		if (CurrentURNO) 
			gGRTable.Add(myRecordset);
		else			
		{									// ignore record with invalid URNO
			ClearRecordset(myRecordset);
			delete myRecordset;
		}
	}

	gCanTableUpdateFields = true;
	bool myInvisibleAllFields = true;
	for (i = 0; i < gCurrentFieldsDesc->GetSize(); i++)
	{
		CFieldInfo *myFieldInfo = gCurrentFieldsDesc->GetAt(i);
		switch (myFieldInfo->mLGType)
		{
			case LG_CDAT:
			case LG_LSTU:
			case LG_CUSR:
			case LG_USER:
				continue;
				break;
			default:
				if (myFieldInfo->mReadOnly || !myFieldInfo->mVisible)
					gCanTableUpdateFields = false;
				if (myFieldInfo->mVisible)
					myInvisibleAllFields = false;	// at least one field is visible
				break;
		}
	}
	if (myInvisibleAllFields)
	{
		CString OutputStr;
		OutputStr.Format(_T("No visible column in %s."), (LPCTSTR)Table);
		AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);

		return false;
	}

	// set the collection of the table names
	gTableName = pTableName;
	CString *myLongTableName;
	if (!gLongTableNames.Lookup(gTableName + gTableExt, myLongTableName)) 
		myLongTableName = &gTableName;
	SetTitle(*myLongTableName);

	gWhereClause = pWhere;	// set the current where clause
	gTableLoad = true;		

#ifdef ABBCCS_RELEASE
	gBcHandle.AddTableCommand(gTableName + gTableExt, UFIS_INSERT, DBDPS_BC_NEW);
	gBcHandle.AddTableCommand(gTableName + gTableExt, UFIS_UPDATE, DBDPS_BC_CHANGE);
	gBcHandle.AddTableCommand(gTableName + gTableExt, UFIS_DELETE, DBDPS_BC_DELETE);
#endif

	// TABLE security info
	gCanTableInsertRow = GetSecurityStatus(pTableName + gTableExt, _T("INSERT_ROW")) == CString(SEC_STAT_ACT);
	gCanTableUpdateRow = GetSecurityStatus(pTableName + gTableExt, _T("UPDATE_ROW")) == CString(SEC_STAT_ACT);
	gCanTableDeleteRow = GetSecurityStatus(pTableName + gTableExt, _T("DELETE_ROW")) == CString(SEC_STAT_ACT);

	return true;
}

//-----------------------------------------------------------------------
// DoSort()
//	
//	The function performs the sort
//-----------------------------------------------------------------------
void CDBDPSDoc::DoSort()
{
	if (gSortedFields.GetSize() == 0) return;

	int *myIndexMap = new int[mIndexMap.GetSize()];		// use my own copy 
	int i;															// of mIndexMap
	for (i = 0; i < mIndexMap.GetSize(); i++)
		myIndexMap[i] = *mIndexMap.GetAt(i);

	qsort((void *)myIndexMap, (size_t)mIndexMap.GetSize(), sizeof(int), GridCompare);

	for (i = 0; i < mIndexMap.GetSize(); i++)
		*mIndexMap.GetAt(i) = myIndexMap[i];			// copy it back

	delete myIndexMap;										//	and free the memory
}

//-----------------------------------------------------------------------
// ClearDataStructure()
//	
//	The function clears the gGRTable and deletes the corresponding objects
//-----------------------------------------------------------------------
void CDBDPSDoc::ClearDataStructure()
{
	if(gTableLoad)
	{
#ifdef ABBCCS_RELEASE
		gBcHandle.RemoveTableCommand(gTableName + gTableExt, UFIS_INSERT, DBDPS_BC_NEW);
		gBcHandle.RemoveTableCommand(gTableName + gTableExt, UFIS_UPDATE, DBDPS_BC_CHANGE);
		gBcHandle.RemoveTableCommand(gTableName + gTableExt, UFIS_DELETE, DBDPS_BC_DELETE);
#endif
	}

	gTableLoad = false;

	gTableName.Empty();
	gWhereClause.Empty();
	gSortClause.Empty();

	ClearIndexMap();			// at first clears the index map
	ClearSortedFields();		// clears the sorted fields array
	if (gCurrentFieldsDesc)
		ClearChangingRecord();	// clears the temporary recordset

	ROWCOL nRows = gGRTable.GetSize();
	for (ROWCOL row = 0; row < nRows; row++)
	{
		ClearRecordset(gGRTable[row]);
		delete gGRTable[row];
	}
	gGRTable.RemoveAll();
}

//-----------------------------------------------------------------------
// ClearRecordset()
//	
//	The function clears the recordset
//-----------------------------------------------------------------------
void CDBDPSDoc::ClearRecordset(CGRRecordset *pGRRecordset)
{
	ROWCOL nCols = pGRRecordset->GetSize();
	for (ROWCOL col = 0; col < nCols; col++) 
		delete pGRRecordset->GetAt(col);			// delete the cell 
	pGRRecordset->RemoveAll();						// remove all the items
}

//-----------------------------------------------------------------------
// ClearIndexMap()
//	
//	The function clears the mIndexMap and gUrnoMap structures
//-----------------------------------------------------------------------
void CDBDPSDoc::ClearIndexMap()
{
	int nRows = mIndexMap.GetSize();
	for (int row = 0; row < nRows; row++) 
		delete mIndexMap.GetAt(row);				// delete the index
	mIndexMap.RemoveAll();							// remove all the items

	nRows = gUrnoMap.GetSize();
	for (row = 0; row < nRows; row++) 
		delete gUrnoMap.GetAt(row);				// delete the urno
	gUrnoMap.RemoveAll();							// remove all the items
}

//-----------------------------------------------------------------------
// ClearChangingRecord()
//	
//	The function clears the mChangingRecord and sets the default data to it
//-----------------------------------------------------------------------
void CDBDPSDoc::ClearChangingRecord()
{
	ClearRecordset(&mChangingRecord);
	for (int i = 0; i < gCurrentFieldsDesc->GetSize(); i++)
	{
		CGRCell *myGRCell = new CGRCell;
		myGRCell->mCellState = CELL_STATE_NOT_USED;
		CFieldInfo *myFieldInfo = gCurrentFieldsDesc->GetAt(i);

		ReplaceCellByDefault(myGRCell, myFieldInfo, false);
		mChangingRecord.Add(myGRCell);
	}
}

//-----------------------------------------------------------------------
// ClearSysInfo()
//	
//	The function clears all the objects containg the system information 
//-----------------------------------------------------------------------
void CDBDPSDoc::ClearSysInfo()
{
	POSITION pos;    
	CString myKey;
	CFieldsDesc *myFD;
	for (pos = gTablesDesc.GetStartPosition(); pos != NULL;)
	{
		gTablesDesc.GetNextAssoc(pos, myKey, myFD);

		int nCols = myFD->GetSize();
		for (int col = 0; col < nCols; col++) 
		delete myFD->GetAt(col);				// delete the item definition
		myFD->RemoveAll();						// remove all the items
		delete myFD;								// delete the field info collection
	}
	gTablesDesc.RemoveAll();				

	gTables.RemoveAll();				// also the table names
	
	CString *myString;
	for (pos = gLongTableNames.GetStartPosition(); pos != NULL;)
	{
		gLongTableNames.GetNextAssoc(pos, myKey, myString);
		delete myString;
	}
	gLongTableNames.RemoveAll();	// and the long table names
}

//-----------------------------------------------------------------------
// UpdateRecord()
//	
//	The function updates the edited record in gGRTable by the mChangingRecord 
//-----------------------------------------------------------------------
bool CDBDPSDoc::UpdateRecord
	(
		ROWCOL pRow
	)
{
	if (!gStyleBuffer.mNowUndoUpdate)
		gStyleBuffer.ClearUndo();
	gStyleBuffer.mNowUndoUpdate = false;

	// give me the real GRTable Index and URNO
	int RealRow = *mIndexMap.GetAt(pRow);
	int RealUrno = *gUrnoMap.GetAt(RealRow);
	
	// now update the record in the database
	int i;

	// effective but not appropriate code (1 - old)
	// in future one can switch between them since they are marked
	// CString UpdateFields;
	// CString FieldValues;		

	// the new code (1 - new)
	CString UpdateFields(SYS_FIELD[F_URNO]);
	CString FieldValues;		
	FieldValues.Format("%ld", RealUrno);

	for (i = 0; i < gCurrentFieldsDesc->GetSize(); i++)		// fill out the select clause
	{	
		CGRCell *myGRCell = mChangingRecord.GetAt(i);
		CFieldInfo *myFieldInfo = gCurrentFieldsDesc->GetAt(i);

/*		(2 - old no new)
		if (!(myGRCell->mCellState & CELL_UPDATED) &&
				myFieldInfo->mLGType != LG_LSTU &&
				myFieldInfo->mLGType != LG_USER) continue;
*/
		if (myFieldInfo->mLGType == LG_LSTU)
		{
			myGRCell->mCellState |= CELL_UPDATED;
			CurrentDateTimeToCell(myGRCell->mStrValue);
		}

		if (myFieldInfo->mLGType == LG_USER)
			myGRCell->mCellState |= CELL_UPDATED;

		if (!UpdateFields.IsEmpty()) UpdateFields += DELIMITER_CHAR;
		UpdateFields += gCurrentFieldsDesc->GetAt(i)->mFieldName;

		if (!FieldValues.IsEmpty()) FieldValues += DELIMITER_CHAR;

		CString myDBValue;
		if (myGRCell->mCellState & CELL_UPDATED)
			TransCellToDB(i, myGRCell->mStrValue, myDBValue);
		else
		{
			TransCellToDB(i, gGRTable.GetAt(RealRow)->GetAt(i)->mStrValue, myDBValue);
			myGRCell->mStrValue = gGRTable.GetAt(RealRow)->GetAt(i)->mStrValue;
			myGRCell->mNumValue = gGRTable.GetAt(RealRow)->GetAt(i)->mNumValue;
			myGRCell->mCellState |= CELL_UPDATED;
		}

		FieldValues += myDBValue;
	}
	if (FieldValues.IsEmpty()) return false;

	// now update
	CString WhereClause;
	WhereClause.Format(WHERE_STRING _T("%s = %ld"), SYS_FIELD[F_URNO], RealUrno);
	if (UpdateRecordDB(gTableName + gTableExt, UpdateFields, FieldValues, WhereClause)) return false;

	// if the update action was succesfull add the record also to the GRTable
	// and to the corresponding index map cell
	for (i = 0; i < mChangingRecord.GetSize(); i++)
	{
		CGRCell *myInputGRCell = mChangingRecord.GetAt(i);
		if (!(myInputGRCell->mCellState & CELL_UPDATED)) continue;
		CGRCell *myOutputGRCell = gGRTable.GetAt(RealRow)->GetAt(i);
		*myOutputGRCell = *myInputGRCell;
		myOutputGRCell->mCellState = CELL_STATE_NOT_USED;
	}

	return true;
}

//-----------------------------------------------------------------------
// InsertRecord()
//	
//	The function inserts the mChangingRecord into gGRTable 
//-----------------------------------------------------------------------
bool CDBDPSDoc::InsertRecord() 
{
	CString NewURNO;
	if (!GetNewURNO(NewURNO)) return false;		// create the new URNO

	// now insert into the database
	int i;
	CString NullValue(DB_NULL_VALUE);
	CString InsertFields(SYS_FIELD[F_URNO]);
	CString FieldValues(NewURNO);
	for (i = 0; i < gCurrentFieldsDesc->GetSize(); i++)		// fill out the select clause
	{	
		CFieldInfo *myFieldInfo = gCurrentFieldsDesc->GetAt(i);
		CGRCell *myGRCell = mChangingRecord.GetAt(i);
		if (myFieldInfo->mLGType == LG_LSTU || myFieldInfo->mLGType == LG_CDAT)
			CurrentDateTimeToCell(myGRCell->mStrValue);

		CString myDBValue;
		TransCellToDB(i, myGRCell->mStrValue, myDBValue);

		InsertFields += DELIMITER_CHAR + myFieldInfo->mFieldName;
		FieldValues += DELIMITER_CHAR + myDBValue;
	}
	if (InsertRecordDB(gTableName + gTableExt, InsertFields, FieldValues)) return false;

	// if the insert action was succesfull add the record also to the GRTable
	// and to the corresponding index map cell
	CGRRecordset *myRecordset = new CGRRecordset;
	for (i = 0; i < gCurrentFieldsDesc->GetSize(); i++) 
		myRecordset->Add(new CGRCell);
	gGRTable.Add(myRecordset);

	for (i = 0; i < mChangingRecord.GetSize(); i++)
	{
		CGRCell *myInputGRCell = mChangingRecord.GetAt(i);
		CGRCell *myOutputGRCell = myRecordset->GetAt(i);
		*myOutputGRCell = *myInputGRCell;
		myOutputGRCell->mCellState = CELL_STATE_NOT_USED;
	}

	int *myIndex = new int;
	int *myUrno = new int;
	*myIndex = mIndexMap.GetSize();
	*myUrno = _ttoi(NewURNO);
	mIndexMap.Add(myIndex);
	gUrnoMap.Add(myUrno);

	return true;
}

//-----------------------------------------------------------------------
// DeleteRecord()
//	
//	The function deletes the selected record
//-----------------------------------------------------------------------
bool CDBDPSDoc::DeleteRecord
	(
		ROWCOL pRow
	)
{
	gStyleBuffer.ClearUndo();

	// give me the real GRTable Index and URNO
	int RealRow = *mIndexMap.GetAt(pRow);
	CGRRecordset *myRecordset = gGRTable.GetAt(RealRow);
	int RealUrno = *gUrnoMap.GetAt(RealRow);
	
	// now delete from the database
	CString WhereClause;
	WhereClause.Format(WHERE_STRING _T("%s = %ld"), SYS_FIELD[F_URNO], RealUrno);
	if (DeleteRecordDB(gTableName + gTableExt, WhereClause)) return false;

	// if the delete action was succesfull delete also the recordset 
	// and the corresponding index map cell
	ClearRecordset(myRecordset);
	gGRTable.RemoveAt(RealRow);
	delete myRecordset;

	delete mIndexMap.GetAt(pRow);
	mIndexMap.RemoveAt(pRow);
	delete gUrnoMap.GetAt(RealRow);
	gUrnoMap.RemoveAt(RealRow);

	for (int i = 0; i < mIndexMap.GetSize(); i++)
		if (*mIndexMap.GetAt(i) > RealRow) 
			(*mIndexMap.GetAt(i))--;
	
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// CDBDPSDoc serialization

void CDBDPSDoc::Serialize(CArchive& ar)
{
}


/////////////////////////////////////////////////////////////////////////////
// CDBDPSDoc diagnostics

#ifdef _DEBUG
void CDBDPSDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CDBDPSDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

//-----------------------------------------------------------------------
// UpdateRecordDB()
//	
//	The function calls the CallCeda for update
//-----------------------------------------------------------------------
int CDBDPSDoc::UpdateRecordDB
	(
		LPCTSTR pTableName, 
		LPCTSTR pTableFields,
		LPCTSTR pFieldsValues, 
		LPCTSTR pWhereClause
	)
{
	CString Fields(pTableFields);
	CString Values(pFieldsValues);

	//*** 02.12.1999 SHA ***
	//*** CHECK FOR UNIQUE-KEY ***
	//CMapStringToString	olValues;
	CString				KeyFields="";
	CString				KeyNames = "" ;
	CString				ValidFrom ="";
	CString				ValidTo ="";
	bool				bCheckValid = FALSE;
	CStringArray		olKeys;
	CString				olFields;
	CString				olValues;
	bool				obSaveOk=true;

	ogTableKeys.Lookup(pTableName,KeyNames);
	//olValues.RemoveAll();

	if (gConfirmUpdate)
		if (AfxMessageBox("Apply changes to database ?",MB_ICONQUESTION|MB_YESNO)==IDYES)	
			obSaveOk = true;
		else
			obSaveOk = false;


	if (obSaveOk)
	{
		int ret;
		if (KeyNames=="")
		{
			//*** NO KEYS -> STORE WITHOUT CHECKING ***
			ret = DBDPSCallCeda(UFIS_UPDATE, pTableName, NULL,
								pWhereClause, pTableFields, pFieldsValues);
		}
		else
		{
			//*** KEYS -> CHECK CONTENT ***
			ConvertToArray(KeyNames,olKeys,"-",bCheckValid);
			//if (bCheckValid==true)
			//{
				CString olTable = pTableName;
				olTable = olTable.Left(3);

				olFields = pTableFields;
				olValues = pFieldsValues;
				ValidFrom = GetValue(olFields,olValues,",","VAFR");
				ValidTo = GetValue(olFields,olValues,",","VATO");

				CString olDmy="(";
				//SHA20000301
				for (int i=0;i<olKeys.GetSize();i++)
				{
					olDmy = olDmy + olKeys[i] + "='" + GetValue(olFields,olValues,",",olKeys[i]) +"' ";
					if (i+1<olKeys.GetSize())
						olDmy=olDmy + "AND ";
					KeyFields =KeyFields + 	olKeys[i];
					if (i<olKeys.GetSize()-1) 
						KeyFields =KeyFields + ",";	
		
				}
				olDmy = olDmy + ")";

				CString olDmyValid="((VAFR>'" + GetValue(olFields,olValues,",","VAFR") +"' AND VAFR<'" 
					+ GetValue(olFields,olValues,",","VATO") + "')" + " OR (VATO>'"
					+ GetValue(olFields,olValues,",","VAFR") + "' AND VATO<'"
					+ GetValue(olFields,olValues,",","VATO") +"'))";
	
				//olDmyValid="(VATO>'"
				//+ GetValue(olFields,olValues,",","VAFR") + "' AND VATO<'"
				//+ GetValue(olFields,olValues,",","VATO") +"')";

				//olDmyValid="((VAFR>'" + GetValue(olFields,olValues,",","VAFR") +"' AND VAFR<'" 
				//	+ GetValue(olFields,olValues,",","VATO") + "')";

				CString clWhere="WHERE ";
				if (!bCheckValid)
					clWhere = clWhere + "(" + olDmy + ")";
				else
					clWhere = clWhere + "(" + olDmy + " AND "+olDmyValid +")";

				ogBCD.SetTableExtension(gTableExt);
				ogBCD.SetObject(olTable);
				ogBCD.Read(olTable,clWhere);

				int ilDataCount=ogBCD.GetDataCount(olTable); //number of rows

				if (ilDataCount>0)
				{
							//for(int i = 0; i < ilDataCount; i++)
							//{
							//		RecordSet olRecord;
							//		BOOL blRead=ogBCD.GetRecord("CHT", i,  olRecord);
							//		CString clRow=olRecord.Values[ogBCD.GetFieldIndex("CHT","CHTC")];
							//}

					CString olMessage;
					olMessage = "Double Key-Field (" + KeyFields + ") values. Record cannot be updated. Press <ESC>";

					AfxMessageBox( olMessage,MB_OK + MB_ICONSTOP);	

					return 1;
				}
				else
				{
					//*** OK, SAVE DATA ***
					ret = DBDPSCallCeda(UFIS_UPDATE, pTableName, NULL,
								pWhereClause, pTableFields, pFieldsValues);

				}
		}

		//*** 31.08.99 SHA ***
		if (ret)	
		{
			if (gErrorMessage.Find(",1,Error")!=-1)
			{
				AfxMessageBox("Double Key-Field values. Record cannot be inserted.",
							MB_OK + MB_ICONSTOP);
				//return 0;
			}
			else

				AfxMessageBox("UpdateRecordDB(). Error on CallCeda. " + gErrorMessage + POINT_CHAR, 
							MB_OK + MB_ICONSTOP);
		}

		return ret;
	}
	else
		return -1;

}

//-----------------------------------------------------------------------
// InsertRecordDB()
//	
//	The function calls the CallCeda for insert
//-----------------------------------------------------------------------
int CDBDPSDoc::InsertRecordDB
	(
		LPCTSTR pTableName, 
		LPCTSTR pTableFields,
		LPCTSTR pFieldsValue
	)
{
	//*++++++++++++++++++++
	
	
	CString Fields(pTableFields);
	CString Values(pFieldsValue);

	//*** 02.12.1999 SHA ***
	//*** CHECK FOR UNIQUE-KEY ***
	//CMapStringToString	olValues;
	CString				KeyFields="";
	CString				KeyNames = "" ;
	CString				ValidFrom ="";
	CString				ValidTo ="";
	bool				bCheckValid = FALSE;
	CStringArray		olKeys;
	CString				olFields;
	CString				olValues;
	bool				obSaveOk=true;

	ogTableKeys.Lookup(pTableName,KeyNames);
	//olValues.RemoveAll();

	if (gConfirmUpdate)
		if (AfxMessageBox("Apply changes to database ?",MB_ICONQUESTION|MB_YESNO)==IDYES)	
			obSaveOk = true;
		else
			obSaveOk = false;


	if (obSaveOk)
	{
		int ret;
		if (KeyNames=="")
		{
			//*** NO KEYS -> STORE WITHOUT CHECKING ***
			ret = DBDPSCallCeda(UFIS_INSERT, pTableName, NULL,
								NULL, pTableFields, pFieldsValue);

		}
		else
		{
			//*** KEYS -> CHECK CONTENT ***
			ConvertToArray(KeyNames,olKeys,"-",bCheckValid);
				CString olTable = pTableName;
				olTable = olTable.Left(3);

				olFields = pTableFields;
				olValues = pFieldsValue;
				ValidFrom = GetValue(olFields,olValues,",","VAFR");
				ValidTo = GetValue(olFields,olValues,",","VATO");

				CString olDmy="(";
				for (int i=0;i<olKeys.GetSize();i++)
				{
					olDmy = olDmy + olKeys[i] + "='" + GetValue(olFields,olValues,",",olKeys[i]) +"' ";
					if (i+1<olKeys.GetSize())
						olDmy=olDmy + "AND ";
					KeyFields =KeyFields + 	olKeys[i];
					if (i<olKeys.GetSize()-1) 
						KeyFields =KeyFields + ",";	
		
				}
				olDmy = olDmy + ")";

				CString olDmyValid="((VAFR>'" + GetValue(olFields,olValues,",","VAFR") +"' AND VAFR<'" 
					+ GetValue(olFields,olValues,",","VATO") + "')" + " OR (VATO>'"
					+ GetValue(olFields,olValues,",","VAFR") + "' AND VATO<'"
					+ GetValue(olFields,olValues,",","VATO") +"'))";

				CString clWhere="WHERE ";
				if (!bCheckValid)
					clWhere = clWhere + "(" + olDmy + ")";
				else
					clWhere = clWhere + "(" + olDmy + " AND "+olDmyValid +")";

				ogBCD.SetTableExtension(gTableExt);
				ogBCD.SetObject(olTable);
				ogBCD.Read(olTable,clWhere);

				int ilDataCount=ogBCD.GetDataCount(olTable); //number of rows

				if (ilDataCount>0)
				{

					CString olMessage;
					olMessage = "Double Key-Field (" + KeyFields + ") values. Record cannot be inserted. Press <ESC>";

					AfxMessageBox( olMessage,MB_OK + MB_ICONSTOP);	

					return 1;
				}
				else
				{
					//*** OK, SAVE DATA ***
					ret = DBDPSCallCeda(UFIS_INSERT, pTableName, NULL,
							NULL, pTableFields, pFieldsValue);

				}
		}

	
	
	

	
	
	
	//++++++++++++++++++++++++
	//int ret = DBDPSCallCeda(UFIS_INSERT, pTableName, NULL,
	//						NULL, pTableFields, pFieldsValue);
	//*** 31.08.99 SHA ***

	if (ret)	
	{
		if (gErrorMessage.Find("00001")!=-1)
		{
			AfxMessageBox("Double Key-Field values. Record cannot be inserted.",
						MB_OK + MB_ICONSTOP);
			//return 0;	
		}
		else
			AfxMessageBox("InsertRecordDB(). Error on CallCeda. " + gErrorMessage + POINT_CHAR,
						MB_OK + MB_ICONSTOP);
	}
	

	return ret;
	}
	else 
		return -1;
}

//-----------------------------------------------------------------------
// DeleteRecordDB()
//	
//	The function calls the CallCeda for delete
//-----------------------------------------------------------------------
int CDBDPSDoc::DeleteRecordDB
	(
		LPCTSTR pTableName, 
		LPCTSTR pWhereClause
	)
{
	bool obSaveOk;

	if (gConfirmUpdate)
	if (AfxMessageBox("Delete the selected recordset ?",MB_ICONQUESTION|MB_YESNO)==IDYES)	
		obSaveOk = true;
	else
		obSaveOk = false;


	if (obSaveOk)
	{
		int ret = DBDPSCallCeda(UFIS_DELETE, pTableName, NULL,
								pWhereClause, NULL, NULL);
		if (ret)	
			AfxMessageBox("DeleteRecordDB(). Error on CallCeda. " + gErrorMessage + POINT_CHAR,
							MB_OK + MB_ICONSTOP);
		return ret;
	}
	else
		return -1;
	
}

//-----------------------------------------------------------------------
// InsertRecordBc()
//	
//	The function handles the insert broadcast event. On action returns 0 
//	otherwise nonzero.
//-----------------------------------------------------------------------
int CDBDPSDoc::InsertRecordBc
	(
		LPCTSTR pTableFields,
		LPCTSTR pFieldsValue
	)
{
	if (!gTableLoad) return RC_FAIL;
/*
	POSITION pos = GetFirstViewPosition();
	if (pos)
	{
		CView* pView = GetNextView(pos);
		ASSERT(pView->IsKindOf(RUNTIME_CLASS(CDBDPSView)));
		if (((CDBDPSView*)pView)->GetBrowseParam()->m_nEditMode != GRID_NO_EDIT_MODE)
			return RC_FAIL;	// grid is still in edit mode !!!
	}
*/
	ClearChangingRecord();
	
	TCHAR myValue[MAX_VALUE_LENGTH];
	TCHAR myField[FINA_LENGTH + 1];

	LPTSTR myValuePtr = myValue;		
	LPTSTR myFieldPtr = myField;		
	LPCTSTR myFieldsValuePtr = pFieldsValue;		
	LPCTSTR myTableFieldsPtr = pTableFields;		

	int nFields = gCurrentFieldsDesc->GetSize();
	CString myUrno;
	for (int i = -1; i < nFields; i++)	// i = -1 because of the URNO appearance
	{
		if (!GetToken(myTableFieldsPtr, myFieldPtr) && i != nFields - 1)	// GetToken finished earlier
			return RC_FAIL;		
		if (!GetToken(myFieldsValuePtr, myValuePtr) && i != nFields - 1)	// GetToken finished earlier
			return RC_FAIL;		
		if (!_tcsicmp(myFieldPtr, SYS_FIELD[F_URNO]))
			myUrno = myValuePtr;
		else 
		{	
			if (!_tcsicmp(myValuePtr, DB_NULL_VALUE)) 
				*myValuePtr = 0;

			int myFieldCol;
			for (myFieldCol = 0; myFieldCol < nFields; myFieldCol++)
				if (!gCurrentFieldsDesc->GetAt(myFieldCol)->mFieldName.CompareNoCase(myFieldPtr))
					break;
			if (myFieldCol >= nFields) return RC_FAIL;
			TransDBToCell(mChangingRecord.GetAt(myFieldCol), 
								gCurrentFieldsDesc->GetAt(myFieldCol), 
								myValuePtr);
		}
	}
	if (myUrno.IsEmpty()) return RC_FAIL;
	
	CString myWhereClause(gWhereClause);
	if (myWhereClause.IsEmpty())
		myWhereClause = myWhereClause + WHERE_STRING + SYS_FIELD[F_URNO] + EQ_CHAR + myUrno;
	else
		myWhereClause = myWhereClause + AND_STRING + SYS_FIELD[F_URNO] + EQ_CHAR + myUrno;

	int ret = DBDPSCallCeda(UFIS_SELECT, gTableName + gTableExt, NULL,
							myWhereClause, SYS_FIELD[F_URNO], NULL);
	if (ret) return ret;

	// if the select action was succesfull add the record also to the GRTable
	// and to the corresponding index map cell
	CGRRecordset *myRecordset = new CGRRecordset;
	for (i = 0; i < nFields; i++) myRecordset->Add(new CGRCell);
	gGRTable.Add(myRecordset);

	for (i = 0; i < mChangingRecord.GetSize(); i++)
	{
		CGRCell *myInputGRCell = mChangingRecord.GetAt(i);
		CGRCell *myOutputGRCell = myRecordset->GetAt(i);
		*myOutputGRCell = *myInputGRCell;
		myOutputGRCell->mCellState = CELL_STATE_NOT_USED;
	}

	int *myIndex = new int;			// also add the new index and URNO
	int *myUrnoNumber = new int;
	*myIndex = mIndexMap.GetSize();
	*myUrnoNumber = _ttoi(myUrno);
	mIndexMap.Add(myIndex);
	gUrnoMap.Add(myUrnoNumber);

	return 0;
}

//-----------------------------------------------------------------------
// UpdateRecordBc() 
//	
//	The function handles the update broadcast event. On action returns 0 
//	otherwise nonzero.
//-----------------------------------------------------------------------
int CDBDPSDoc::UpdateRecordBc
	(
		LPCTSTR pTableFields,
		LPCTSTR pFieldsValue,
		LPCTSTR pWhereClause
	)
{
	gStyleBuffer.ClearUndo();

	if (!gTableLoad) return RC_FAIL;
/*
	POSITION pos = GetFirstViewPosition();
	if (pos)
	{
		CView* pView = GetNextView(pos);
		ASSERT(pView->IsKindOf(RUNTIME_CLASS(CDBDPSView)));
		if (((CDBDPSView*)pView)->GetBrowseParam()->m_nEditMode != GRID_NO_EDIT_MODE)
			return RC_FAIL;	// grid is still in edit mode !!!
	}
*/	
	CString myWhereClause(pWhereClause);
	int myPosition = myWhereClause.Find(EQ_CHAR);		// find the '='
	for (myPosition++; pWhereClause[myPosition] == BLANK_CHAR && pWhereClause[myPosition]; myPosition++);
	if (!pWhereClause[myPosition]) return RC_FAIL;

	CString myUrno(pWhereClause + myPosition);
	int myUrnoNumber = _ttoi(pWhereClause + myPosition);
	int myRow;
	for (myRow = 0; myRow < gUrnoMap.GetSize(); myRow++)
		if (*gUrnoMap.GetAt(myRow) == myUrnoNumber) break;

	if (myRow >= gUrnoMap.GetSize()) // did not find
	{	
		CString SelectFields(SYS_FIELD[F_URNO]);
		CString myWhereClause(gWhereClause);
		if (myWhereClause.IsEmpty())
			myWhereClause = myWhereClause + WHERE_STRING + SYS_FIELD[F_URNO] + EQ_CHAR + myUrno;
		else
			myWhereClause = myWhereClause + AND_STRING + SYS_FIELD[F_URNO] + EQ_CHAR + myUrno;

		for (int i = 0; i < gCurrentFieldsDesc->GetSize(); i++)		// fill out the select clause
		{	
			SelectFields += DELIMITER_CHAR;
			SelectFields += gCurrentFieldsDesc->GetAt(i)->mFieldName;
		}
		int ret = DBDPSCallCeda(UFIS_SELECT, gTableName + gTableExt, NULL,
								 myWhereClause, SelectFields, NULL);
		if (ret) return ret;

		HGLOBAL myHandle;
		GetBufferHandle(UFIS_BUF, &myHandle);
		LPTSTR DataPtr = (LPTSTR)GlobalLock(myHandle);		// data from the SYSTAB
		
		TCHAR Value[MAX_VALUE_LENGTH];
		LPTSTR ValuePtr = Value;
		int nCols = gCurrentFieldsDesc->GetSize();

		CGRRecordset *myRecordset = new CGRRecordset;
		for (int col = -1; col < nCols; col++)		// starting from -1 because of URNO
		{
			ValuePtr = Value;		
			if (!GetToken(DataPtr, ValuePtr) && col != nCols - 1)	// GetToken finished earlier
			{
				ClearRecordset(myRecordset);
				delete myRecordset;
				return RC_FAIL;
			}
			
			if (!_tcscmp(Value, DB_NULL_VALUE))	// Null value?
				*Value = 0;

			if (col == -1) continue;		// it is URNO

			CFieldInfo *myFieldInfo = gCurrentFieldsDesc->GetAt(col);
			CGRCell *myGRCell = new CGRCell;
			TransDBToCell(myGRCell, myFieldInfo, Value); 
			myRecordset->Add(myGRCell);				// add the cell to recordset
		}
		gGRTable.Add(myRecordset);
		int *myIndex = new int;
		int *myUrno = new int;
		*myUrno = myUrnoNumber;				// urno
		*myIndex = mIndexMap.GetSize();	// order 
		mIndexMap.Add(myIndex);
		gUrnoMap.Add(myUrno);
	}
	else						// did find => update data structure
	{
		TCHAR myValue[MAX_VALUE_LENGTH];
		TCHAR myField[FINA_LENGTH + 1];

		int nFields = gCurrentFieldsDesc->GetSize();
		bool OneMoreLoop = true;
		
		LPTSTR myValuePtr = myValue;		
		LPTSTR myFieldPtr = myField;		
		LPCTSTR myFieldsValuePtr = pFieldsValue;		
		LPCTSTR myTableFieldsPtr = pTableFields;		
		for (int myCol = 0; OneMoreLoop; myCol++)
		{
			if (!GetToken(myTableFieldsPtr, myFieldPtr))	
				OneMoreLoop = false;		
			if (!GetToken(myFieldsValuePtr, myValuePtr))	
				OneMoreLoop = false;		

			if (!_tcsicmp(myFieldPtr, SYS_FIELD[F_URNO])) continue; // just ignore the URNO

			int myFieldCol;
			for (myFieldCol = 0; myFieldCol < nFields; myFieldCol++)
				if (!gCurrentFieldsDesc->GetAt(myFieldCol)->mFieldName.CompareNoCase(myFieldPtr))
					break;
			if (myFieldCol >= nFields) return RC_FAIL;
			
			CGRRecordset *myRecordset = gGRTable.GetAt(myRow);
			if (!_tcsicmp(myValuePtr, DB_NULL_VALUE)) 
				*myValuePtr = 0;
			TransDBToCell(myRecordset->GetAt(myFieldCol), 
								gCurrentFieldsDesc->GetAt(myFieldCol), 
								myValuePtr);
		}
	}
	return 0;
}		

//-----------------------------------------------------------------------
// DeleteRecordBc() 
//	
//	The function handles the delete broadcast event. On action returns 0 
//	otherwise nonzero.
//-----------------------------------------------------------------------
int CDBDPSDoc::DeleteRecordBc
	(
		LPCTSTR pWhereClause
	)
{
	gStyleBuffer.ClearUndo();

	if (!gTableLoad) return RC_FAIL;
/*
	POSITION pos = GetFirstViewPosition();
	if (pos)
	{
		CView* pView = GetNextView(pos);
		ASSERT(pView->IsKindOf(RUNTIME_CLASS(CDBDPSView)));
		if (((CDBDPSView*)pView)->GetBrowseParam()->m_nEditMode != GRID_NO_EDIT_MODE)
			return RC_FAIL;	// grid is still in edit mode !!!
	}
*/	
	CString myWhereClause(pWhereClause);
	int myPosition = myWhereClause.Find(EQ_CHAR);		// find the '='
	for (myPosition++; pWhereClause[myPosition] == BLANK_CHAR && pWhereClause[myPosition]; myPosition++);
	if (!pWhereClause[myPosition]) return RC_FAIL;

	CString myUrno(pWhereClause + myPosition);
	int myUrnoNumber = _ttoi(pWhereClause + myPosition);
	int myRow;
	for (myRow = 0; myRow < gUrnoMap.GetSize(); myRow++)
		if (*gUrnoMap.GetAt(myRow) == myUrnoNumber) break;

	if (myRow >= gUrnoMap.GetSize()) return RC_NOT_FOUND;	// did not find the record

	CGRRecordset *myRecordset = gGRTable.GetAt(myRow);
	ClearRecordset(myRecordset);
	gGRTable.RemoveAt(myRow);
	delete myRecordset;

	int myMapRow;
	for (myMapRow = 0; myMapRow < mIndexMap.GetSize(); myMapRow++)
		if (*mIndexMap.GetAt(myMapRow) == myRow) break;

	delete mIndexMap.GetAt(myMapRow);
	mIndexMap.RemoveAt(myMapRow);
	delete gUrnoMap.GetAt(myRow);
	gUrnoMap.RemoveAt(myRow);

	for (int i = 0; i < mIndexMap.GetSize(); i++)
		if (*mIndexMap.GetAt(i) > myRow) 
			(*mIndexMap.GetAt(i))--;

	return 0;
}		

//-----------------------------------------------------------------------
// LoadSysInfo()
//	
//	The function loads the SYSTAB data and fills the gTablesDesc and gGRTable
//-----------------------------------------------------------------------
bool CDBDPSDoc::LoadSysInfo(LPCTSTR pExtension)		// the table extension 
{
	CString SysTable(SYSTAB + CString(pExtension));
	CString AptTable(APTTAB + CString(pExtension));
	CString TabTable(TABTAB + CString(pExtension));

	CString Tables;
	Tables = SysTable;

	bool olSystabError=FALSE;		

#ifdef ABBCCS_RELEASE
	// only for Oracle

	// Tables = SysTable + DELIMITER_CHAR + AptTable;	// with outer join
#else
	// only for Access 
	
	// with join
	// Tables += " SYSHAJ LEFT JOIN APTHAJ ON SYSHAJ.TZON = APTHAJ.APC3 ";
#endif

	CString WhereClause(WHERE_STRING);
	// WhereClause += SysTable + POINT_CHAR + SYS_FIELD[F_TATY]; // with prefix
	WhereClause += SYS_FIELD[F_TATY];		// without prefix
	WhereClause += EQ_CHAR;
	WhereClause += DBDPS_STRING;

#ifdef ABBCCS_RELEASE
	// only for Oracle 

/*		// with outer join
	WhereClause += AND_STRING;

	WhereClause += SysTable + POINT_CHAR + SYS_FIELD[F_TZON];
	WhereClause += EQ_CHAR;
	WhereClause += AptTable + POINT_CHAR + SYS_FIELD[F_APC3] + OUTER_JOIN_STRING;

	WhereClause += AND_STRING;

	WhereClause += AptTable + POINT_CHAR + SYS_FIELD[F_APC3] + OUTER_JOIN_STRING;
	WhereClause += NEQ_STRING;
	WhereClause += _T("\' \'");
*/
#endif

	CString SelectFields;			// build the select clause
	for (int i = 0; i < NUM_SYSFIELDS; i++) 
	{
		if (!SelectFields.IsEmpty()) SelectFields += DELIMITER_CHAR;
		// SelectFields += SysTable + POINT_CHAR + SYS_FIELD[i]; // with prefix
		SelectFields += SYS_FIELD[i];		// without prefix
/*
		switch (i)
		{
			case F_APC3:
			case F_TICH:
			case F_TDI1:
			case F_TDI2:
				SelectFields += AptTable + POINT_CHAR;
				break;
			default:
				SelectFields += SysTable + POINT_CHAR;
				break;
		}
		SelectFields += SYS_FIELD[i];
*/
	}
	CString SortClause;
/*	// with prefix
	SortClause = SysTable + POINT_CHAR + SYS_FIELD[F_SORT]; 
	SortClause += DELIMITER_CHAR + SysTable + POINT_CHAR + SYS_FIELD[F_URNO];
*/
	// without prefix
	SortClause = SYS_FIELD[F_SORT]; 
	SortClause = SortClause + DELIMITER_CHAR + SYS_FIELD[F_URNO];

#ifdef ABBCCS_RELEASE
	// remove the two lines when necessary !!!!	
	WhereClause += CString(" ORDER BY ") + SortClause;//"WHERE TATY='DBDPS' ORDER BY SORT,URNO";
	SortClause.Empty();	
#endif

	int ret = DBDPSCallCeda(UFIS_SELECT, Tables, SortClause,
							 WhereClause, SelectFields, NULL);

	if (ret)
	{
		CString OutputStr;
		OutputStr.Format(_T("Error on reading %s. ") + gErrorMessage + POINT_CHAR, 
						(LPCTSTR)Tables);
		AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);
		//*** 15.11.99 SHA ***
		WriteLogFile(OutputStr);
		//*** STOPPING OK, HARD ERROR ***
		return false;
	}

	HGLOBAL myHandle;
	GetBufferHandle(UFIS_BUF, &myHandle);
	LPTSTR tempDataPtr = (LPTSTR)GlobalLock(myHandle);		// data from the SYSTAB
	int myBufLen = _tcslen(tempDataPtr);

	gDataPtr = (TCHAR*)GlobalAlloc(GMEM_FIXED, (myBufLen + 1) * sizeof(TCHAR));
	if (!gDataPtr)
	{
		AfxMessageBox(_T("LoadSysInfo(). Error on memory allocation. "), MB_OK + MB_ICONSTOP);
		//*** 15.11.99 SHA ***
		WriteLogFile("LoadSysInfo(). Error on memory allocation.");
		//*** STOPPING OK, HARD ERROR ***
		return false;
	}
	_tcscpy(gDataPtr, tempDataPtr);

	TCHAR Value[MAX_VALUE_LENGTH + 1];
	LPTSTR ValuePtr = Value;

	ROWCOL nCols = NUM_SYSFIELDS;
	ROWCOL nRows = GetNumberOfLines(UFIS_BUF);

	CString CurrentURNO;
	for(ROWCOL row = 1; row <= nRows; row++)
	{
		bool CurrentURNOIsSet = false;
		CFieldsDesc *myFsD;
		CFieldInfo *myFI = new CFieldInfo;
		CString myLongTableName;
		CString myShortTableName;
//		int myTDif, TimeDif, TimeDif1, TimeDif2;	// time zone
		for(ROWCOL col = 0; col < nCols; col++)
		{
			ValuePtr = Value;		
			bool FIWasAdded = false;
			if (!GetToken(gDataPtr, ValuePtr) && col != nCols - 1 && row != nRows)	// GetToken finished earlier
			{
				AfxMessageBox("LoadSysInfo(). CallCeda returned invalid data buffer.", MB_OK + MB_ICONSTOP);
				if (!FIWasAdded) delete myFI;
				//*** 15.11.99 SHA ***
				WriteLogFile("LoadSysInfo(). CallCeda returned invalid data buffer.");
				//*** STOPPING OK, HARD ERROR ***
				return false;
			}

			if (!_tcscmp(Value, DB_NULL_VALUE))	// Null value?
				*Value = 0;

			CString myTempValue(ValuePtr);		// conversion
			ConvertStringToClient(myTempValue);
			_tcscpy(ValuePtr, myTempValue);

			switch (col)		// for every SYSTAB field there is the case statement
			{
			case F_URNO:
				if (!CheckAndTrim(SysTable,		// makes trim as necessary and checks the value
										SYS_FIELD[col],	// according to the log. type settings
										SYS_FIELD[F_URNO], 
										CurrentURNO, 
										CurrentURNOIsSet, 
										ValuePtr, 
										SYS_FIELD_LENGTH[col]))
				{
					//if (!FIWasAdded) delete myFI;
					//*** 15.11.99 SHA ***
					//return false;
					CString OutputStr;
					OutputStr.Format("Table <%s> Field <%s>",myShortTableName,myFI->mFieldName);
					WriteLogFile(OutputStr);
					olSystabError = true;
					break;
				}
				CurrentURNO = ValuePtr;
				CurrentURNOIsSet = true;
				break;
			case F_TANA:
				if (!CheckAndTrim(SysTable, 
										SYS_FIELD[col], 
										SYS_FIELD[F_URNO], 
										CurrentURNO, 
										CurrentURNOIsSet, 
										ValuePtr, 
										SYS_FIELD_LENGTH[col], true, false))
				{
					//if (!FIWasAdded) delete myFI;
					//*** 15.11.99 SHA ***
					//return false;
					CString OutputStr;
					OutputStr.Format("Table <%s> Field <%s>",myShortTableName,myFI->mFieldName);
					WriteLogFile(OutputStr);
					olSystabError = true;
					break;
				}
				if (!gTablesDesc.Lookup(ValuePtr, myFsD))		// if there is not yet the entry
				{															// add the new one
					myFsD = new CFieldsDesc;
					gTablesDesc.SetAt(ValuePtr, myFsD);
					gTables.Add(ValuePtr);
				}
				myFsD->Add(myFI);
				FIWasAdded = true;
				break;
			case F_FTAN: 
				if (!CheckAndTrim(SysTable, 
										SYS_FIELD[col], 
										SYS_FIELD[F_URNO], 
										CurrentURNO, 
										CurrentURNOIsSet, 
										ValuePtr, 
										SYS_FIELD_LENGTH[col], true, false))
				{
					//if (!FIWasAdded) delete myFI;
					//*** 15.11.99 SHA ***
					//return false;
					CString OutputStr;
					OutputStr.Format("Table <%s> Field <%s>",myShortTableName,myFI->mFieldName);
					WriteLogFile(OutputStr);
					olSystabError = true;
					break;
				}
				myShortTableName = ValuePtr;
				break;
			case F_FINA:
				if (!CheckAndTrim(SysTable, 
										SYS_FIELD[col], 
										SYS_FIELD[F_URNO], 
										CurrentURNO, 
										CurrentURNOIsSet, 
										ValuePtr, 
										SYS_FIELD_LENGTH[col], true, false))
				{
					//if (!FIWasAdded) delete myFI;
					//*** 15.11.99 SHA ***
					//return false;
					CString OutputStr;
					OutputStr.Format("Table <%s> Field <%s>",myShortTableName,myFI->mFieldName);
					WriteLogFile(OutputStr);
					olSystabError = true;
					break;
				}
				myFI->mFieldName = ValuePtr;
				break;
			case F_FITY:
				if (!CheckAndTrim(SysTable, 
										SYS_FIELD[col], 
										SYS_FIELD[F_URNO], 
										CurrentURNO, 
										CurrentURNOIsSet, 
										ValuePtr, 
										SYS_FIELD_LENGTH[col]))
				{
					//if (!FIWasAdded) delete myFI;
					//*** 15.11.99 SHA ***
					//return false;
					CString OutputStr;
					OutputStr.Format("Table <%s> Field <%s>",myShortTableName,myFI->mFieldName);
					WriteLogFile(OutputStr);
					olSystabError = true;
					break;
				}
				if ((myFI->mDBType = DBTypeToEnum(ValuePtr)) == DB_EL__)	
				{					// the value is out of the set of allowed values
					CString OutputStr;
					CString WhichURNO;
					if (CurrentURNOIsSet) WhichURNO = _T("current");
					else WhichURNO = _T("previous");
					OutputStr.Format(_T("Invalid %s field in %s. The %s %s = %s."), 
											SYS_FIELD[col], 
											SysTable,
											(LPCTSTR)WhichURNO,
											SYS_FIELD[F_URNO],
											CurrentURNO);
					//*** 15.11.99 SHA ***
					//*** CHECK ALL THE FIELDS AND COLLECT ALL THE ERRORS ***
					//*** REMARKED: AfxMessageBox() ***
					//*** REMARKED: return false ***

					//AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);
					//*** 15.11.99 SHA ***
					WriteLogFile(OutputStr);
					//*** STOPPING AFTER COMPLETE CHECKING ***

					//if (!FIWasAdded) delete myFI;
					//*** 15.11.99 SHA ***
					//return false;
					OutputStr.Format("Table <%s> Field <%s>",myShortTableName,myFI->mFieldName);
					WriteLogFile(OutputStr);
					olSystabError = true;
					break;
				}
				break;
			case F_TATY:
				break;
			case F_FELE:
				if (!CheckAndTrim(SysTable, 
										SYS_FIELD[col], 
										SYS_FIELD[F_URNO], 
										CurrentURNO, 
										CurrentURNOIsSet, 
										ValuePtr, 
										SYS_FIELD_LENGTH[col]))
				{
					//if (!FIWasAdded) delete myFI;
					//*** 15.11.99 SHA ***
					//return false;
					CString OutputStr;
					OutputStr.Format("Table <%s> Field <%s>",myShortTableName,myFI->mFieldName);
					WriteLogFile(OutputStr);
					olSystabError = true;
					break;
				}
				myFI->mCellWidth = myFI->mDBLength = _ttoi(ValuePtr);
				if (myFI->mDBLength <= 0 || myFI->mDBLength > MAX_VALUE_LENGTH)
				{			// the value is out of the range of allowed values
					CString OutputStr;
					CString WhichURNO;
					if (CurrentURNOIsSet) WhichURNO = _T("current");
					else WhichURNO = _T("previous");
					OutputStr.Format(_T("Invalid %s field in %s. The %s %s = %s."), 
											SYS_FIELD[col], 
											SysTable,
											(LPCTSTR)WhichURNO,
											SYS_FIELD[F_URNO],
											CurrentURNO);
					//*** 15.11.99 SHA ***
					//AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);
					WriteLogFile(OutputStr);
					//if (!FIWasAdded) delete myFI;
					//*** 15.11.99 SHA ***
					//return false;
					OutputStr.Format("Table <%s> Field <%s>",myShortTableName,myFI->mFieldName);
					WriteLogFile(OutputStr);
					olSystabError = true;
					break;
				}
				break;
			case F_ADDI:
				if (!CheckAndTrim(SysTable, 
										SYS_FIELD[col], 
										SYS_FIELD[F_URNO], 
										CurrentURNO, 
										CurrentURNOIsSet, 
										ValuePtr, 
										SYS_FIELD_LENGTH[col]))
				{
					//if (!FIWasAdded) delete myFI;
					//*** 15.11.99 SHA ***
					//return false;
					CString OutputStr;
					OutputStr.Format("Table <%s> Field <%s>",myShortTableName,myFI->mFieldName);
					WriteLogFile(OutputStr);
					olSystabError = true;
					break;
				}
				myFI->mLongFieldName = ValuePtr;
				break;
			case F_TYPE:
				if (!CheckAndTrim(SysTable, 
										SYS_FIELD[col], 
										SYS_FIELD[F_URNO], 
										CurrentURNO, 
										CurrentURNOIsSet, 
										ValuePtr, 
										SYS_FIELD_LENGTH[col], true, false))
				{
					//if (!FIWasAdded) delete myFI;
					//*** 15.11.99 SHA ***
					//return false;
					CString OutputStr;
					OutputStr.Format("Table <%s> Field <%s>",myShortTableName,myFI->mFieldName);
					WriteLogFile(OutputStr);
					olSystabError = true;
					break;
				}
				if ((myFI->mLGType = LGTypeToEnum(ValuePtr)) == LG_EL__)
				{					// the value is out of the set of allowed values
					CString OutputStr;
					CString WhichURNO;
					if (CurrentURNOIsSet) WhichURNO = _T("current");
					else WhichURNO = _T("previous");
					OutputStr.Format(_T("Invalid %s field in %s. The %s %s = %s."), 
											SYS_FIELD[col], 
											SysTable,
											(LPCTSTR)WhichURNO,
											SYS_FIELD[F_URNO],
											CurrentURNO);
					//*** 15.11.99 SHA ***
					//AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);
					WriteLogFile(OutputStr);
					//if (!FIWasAdded) delete myFI;
					//*** 15.11.99 SHA ***
					//return false;
					OutputStr.Format("Table <%s> Field <%s>",myShortTableName,myFI->mFieldName);
					WriteLogFile(OutputStr);
					olSystabError = true;
					break;
				}
				break;
			case F_TTYP:
				if (!CheckAndTrim(SysTable, 
										SYS_FIELD[col], 
										SYS_FIELD[F_URNO], 
										CurrentURNO, 
										CurrentURNOIsSet, 
										ValuePtr, 
										SYS_FIELD_LENGTH[col], true, false))
				{
					//if (!FIWasAdded) delete myFI;
					//*** 15.11.99 SHA ***
					//return false;
					CString OutputStr;
					OutputStr.Format("Table <%s> Field <%s>",myShortTableName,myFI->mFieldName);
					WriteLogFile(OutputStr);
					olSystabError = true;
					break;
				}
				if ((myFI->mTimeType = TimeTypeToEnum(ValuePtr)) == TIME_EL__)
				{			// the value is out of the set of allowed values
					CString OutputStr;
					CString WhichURNO;
					if (CurrentURNOIsSet) WhichURNO = _T("current");
					else WhichURNO = _T("previous");
					OutputStr.Format(_T("Invalid %s field in %s. The %s %s = %s."), 
											SYS_FIELD[col], 
											SysTable,
											(LPCTSTR)WhichURNO,
											SYS_FIELD[F_URNO],
											CurrentURNO);
					//*** 15.11.99 SHA ***
					//AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);
					WriteLogFile(OutputStr);
					//if (!FIWasAdded) delete myFI;
					//*** 15.11.99 SHA ***
					//return false;
					OutputStr.Format("Table <%s> Field <%s>",myShortTableName,myFI->mFieldName);
					WriteLogFile(OutputStr);
					olSystabError = true;
					break;
				}
				break;
/*			// time zone
			case F_TZON:	
			case F_APC3:
				break;
			case F_TICH:
				{
					if (!CheckAndTrim(SysTable, 
											SYS_FIELD[col], 
											SYS_FIELD[F_URNO], 
											CurrentURNO, 
											CurrentURNOIsSet, 
											ValuePtr, 
											SYS_FIELD_LENGTH[col], false))
					{
						if (!FIWasAdded) delete myFI;
						return false;
					}
					COleDateTime dt = COleDateTime::GetCurrentTime();
					CString myTime;
					myTime.Format(_T("%04d%02d%02d%02d%02d00"),
										dt.GetYear(),
										dt.GetMonth(),
										dt.GetDay(),
										dt.GetHour(),
										dt.GetMinute(), 0);
					if (myTime <= ValuePtr) TimeDif = 1;	// either the TDI1
					else TimeDif = 2;								// or TDI2
				}
				break;
			case F_TDI1:
			case F_TDI2:
				if (!CheckAndTrim(SysTable, 
										SYS_FIELD[col], 
										SYS_FIELD[F_URNO], 
										CurrentURNO, 
										CurrentURNOIsSet, 
										ValuePtr, 
										SYS_FIELD_LENGTH[col], false))
				{
					if (!FIWasAdded) delete myFI;
					return false;
				}
				if (col == F_TDI1)
				{
					TimeDif1 = _ttoi(ValuePtr);	// if the value does not exist
					myTDif = TimeDif1;				// the output is 0
				}
				else 
				{
					TimeDif2 = _ttoi(ValuePtr);	// if the value does not exist
					myTDif = TimeDif2;				// the output is 0
				}
				if (myTDif < MIN_TIME_DIF || myTDif > MAX_TIME_DIF)
				{
					CString OutputStr;
					CString WhichURNO;
					if (CurrentURNOIsSet) WhichURNO = _T("current");
					else WhichURNO = _T("previous");
					OutputStr.Format(_T("Invalid %s field in %s. The %s %s = %s."), 
											SYS_FIELD[col], 
											SysTable,
											(LPCTSTR)WhichURNO,
											SYS_FIELD[F_URNO],
											CurrentURNO);
					AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);
					if (!FIWasAdded) delete myFI;
					return false;
				}
				break;
*/
			case F_REFE:
				if (!CheckAndTrim(SysTable, 
										SYS_FIELD[col], 
										SYS_FIELD[F_URNO], 
										CurrentURNO, 
										CurrentURNOIsSet, 
										ValuePtr, 
										SYS_FIELD_LENGTH[col], false))
				{
					//if (!FIWasAdded) delete myFI;
					//*** 15.11.99 SHA ***
					//return false;
					CString OutputStr;
					OutputStr.Format("Table <%s> Field <%s>",myShortTableName,myFI->mFieldName);
					WriteLogFile(OutputStr);
					olSystabError = true;
					break;
				}
				if (*ValuePtr)
				{
					myFI->mComboTabField = ValuePtr;
					myFI->mComboTabField = myFI->mComboTabField.Left(SYS_FIELD_LENGTH[F_TANA]) // first 3 chars
												+ pExtension														// then extension
												+ myFI->mComboTabField.Right(SYS_FIELD_LENGTH[F_FINA] + 1);	// then the rest if any
				}
				break;
			case F_CLSR:
				if (!CheckAndTrim(SysTable, 
										SYS_FIELD[col], 
										SYS_FIELD[F_URNO], 
										CurrentURNO, 
										CurrentURNOIsSet, 
										ValuePtr, 
										SYS_FIELD_LENGTH[col], true, false))
				{
					//if (!FIWasAdded) delete myFI;
					//*** 15.11.99 SHA ***
					//return false;
					CString OutputStr;
					OutputStr.Format("Table <%s> Field <%s>",myShortTableName,myFI->mFieldName);
					WriteLogFile(OutputStr);
					olSystabError = true;
					break;
				}
				if (*ValuePtr == DB_TRUE_VALUE)
					myFI->mUseCollate = true;
				else if (*ValuePtr == DB_FALSE_VALUE)
					myFI->mUseCollate = false;
				else 
				{		// the value is out of the set of allowed values
					CString OutputStr;
					CString WhichURNO;
					if (CurrentURNOIsSet) WhichURNO = _T("current");
					else WhichURNO = _T("previous");
					OutputStr.Format(_T("Invalid %s field in %s. The %s %s = %s."), 
											SYS_FIELD[col], 
											SysTable,
											(LPCTSTR)WhichURNO,
											SYS_FIELD[F_URNO],
											CurrentURNO);
					//*** 15.11.99 SHA ***
					//AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);
					WriteLogFile(OutputStr);
					//if (!FIWasAdded) delete myFI;
					//*** 15.11.99 SHA ***
					//return false;
					OutputStr.Format("Table <%s> Field <%s>",myShortTableName,myFI->mFieldName);
					WriteLogFile(OutputStr);
					olSystabError = true;
					break;
				}
				break;
			case F_LABL:
				if (!CheckAndTrim(SysTable, 
										SYS_FIELD[col], 
										SYS_FIELD[F_URNO], 
										CurrentURNO, 
										CurrentURNOIsSet, 
										ValuePtr, 
										SYS_FIELD_LENGTH[col]))
				{
					//if (!FIWasAdded) delete myFI;
					//*** 15.11.99 SHA ***
					//return false;
					CString OutputStr;
					OutputStr.Format("Table <%s> Field <%s>",myShortTableName,myFI->mFieldName);
					WriteLogFile(OutputStr);
					olSystabError = true;
					break;
				}
				myFI->mLongHeader = ValuePtr;
				break;
			case F_REQF:
				if (!CheckAndTrim(SysTable, 
										SYS_FIELD[col], 
										SYS_FIELD[F_URNO], 
										CurrentURNO, 
										CurrentURNOIsSet, 
										ValuePtr, 
										SYS_FIELD_LENGTH[col], true, false))
				{
					//if (!FIWasAdded) delete myFI;
					//*** 15.11.99 SHA ***
					//return false;
					CString OutputStr;
					OutputStr.Format("Table <%s> Field <%s>",myShortTableName,myFI->mFieldName);
					WriteLogFile(OutputStr);
					olSystabError = true;
					break;
				}
				if (*ValuePtr == DB_TRUE_VALUE)
					myFI->mRequired = true;
				else if (*ValuePtr == DB_FALSE_VALUE)
					myFI->mRequired = false;
				else			// the value is out of the set of allowed values
				{			
					CString OutputStr;
					CString WhichURNO;
					if (CurrentURNOIsSet) WhichURNO = _T("current");
					else WhichURNO = _T("previous");
					OutputStr.Format(_T("Invalid %s field in %s. The %s %s = %s."), 
											SYS_FIELD[col], 
											SysTable,
											(LPCTSTR)WhichURNO,
											SYS_FIELD[F_URNO],
											CurrentURNO);
					//*** 15.11.99 SHA ***
					//AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);
					WriteLogFile(OutputStr);
					//if (!FIWasAdded) delete myFI;
					//*** 15.11.99 SHA ***
					//return false;
					OutputStr.Format("Table <%s> Field <%s>",myShortTableName,myFI->mFieldName);
					WriteLogFile(OutputStr);
					olSystabError = true;
					break;
				}
				break;
			case F_TOLT:
				if (!CheckAndTrim(SysTable, 
										SYS_FIELD[col], 
										SYS_FIELD[F_URNO], 
										CurrentURNO, 
										CurrentURNOIsSet, 
										ValuePtr, 
										SYS_FIELD_LENGTH[col], false)) 
				{
					//if (!FIWasAdded) delete myFI;
					//*** 15.11.99 SHA ***
					//return false;
					CString OutputStr;
					OutputStr.Format("Table <%s> Field <%s>",myShortTableName,myFI->mFieldName);
					WriteLogFile(OutputStr);
					olSystabError = true;
					break;
				}
				myFI->mToolTip = ValuePtr;
				break;
			case F_SORT: 
				break;
			case F_DEFV: 
				if (!CheckAndTrim(SysTable, 
										SYS_FIELD[col], 
										SYS_FIELD[F_URNO], 
										CurrentURNO, 
										CurrentURNOIsSet, 
										ValuePtr, 
										SYS_FIELD_LENGTH[col], false))
				{
					//if (!FIWasAdded) delete myFI;
					//*** 15.11.99 SHA ***
					//return false;
					CString OutputStr;
					OutputStr.Format("Table <%s> Field <%s>",myShortTableName,myFI->mFieldName);
					WriteLogFile(OutputStr);
					olSystabError = true;
					break;
				}
				myFI->mDefaultStrValue = ValuePtr;
				break;
			default:
				AfxMessageBox("LoadSysInfo(). Invalid SYSTAB field.", MB_OK + MB_ICONSTOP);
				return false;
				break;
			}
		}
		myFI->mIsString = true;		// basic settings
		myFI->mSortable = true;
		myFI->mReadOnly = false;
		myFI->mVisible = true;
		
		switch (myFI->mLGType)
		{
		case LG_USER:
		case LG_CUSR:
			myFI->mRequired = true; 
			myFI->mReadOnly = true;
			break;
		case LG_DATE:
			myFI->mDBLength = DATE_TIME_LENGTH;
			myFI->mCellWidth = DATE_WIDTH;		// the width of the grid cell
			break;
		case LG_TIME:
		case LG_DTIM: 
			myFI->mDBLength = DATE_TIME_LENGTH;
			if (myFI->mLGType == LG_TIME)
				myFI->mCellWidth = TIME_WIDTH;
			else 
				myFI->mCellWidth = DATE_TIME_WIDTH;

			switch (myFI->mTimeType)
			{
				case TIME_UTC:
					myFI->mTimeZoneDif = 0;
					break;
				case TIME_LOCAL:
					{
						TIME_ZONE_INFORMATION myTZI;
						int ret = GetTimeZoneInformation(&myTZI);
						if (ret == 0xFFFFFFFF)		// error
							myFI->mTimeZoneDif = 0;
						else										// UTC - bias = local time
							myFI->mTimeZoneDif = myTZI.Bias + myTZI.DaylightBias;
					}
					break;
/*				// time zone
				case TIME_ZONE:
					if (TimeDif == 1)
						myFI->mTimeZoneDif = -TimeDif1;	// UTC - (-TDI1) = zone time
					else
						myFI->mTimeZoneDif = -TimeDif2;	// UTC - (-TDI2) = zone time
					break;
*/
					//*** 18.11.1999 SHA ***
					//*** DEFAULT EINGEBAUT; SONST UNSINNIGE WERTE IN DTIM !!!! ***
				default:
					myFI->mTimeZoneDif = 0;
					break;

			}
			break;
		case LG_WDAY:
			myFI->mDBLength = MAX_WDAY_LENGTH;
			myFI->mCellWidth = WDAY_WIDTH;
			myFI->mSortable = false;
			break;
		case LG_URNO:
			myFsD->RemoveAt(myFsD->GetUpperBound());	// urno is not to be displayed
			delete myFI;	
			continue; 
			break;
		case LG_REAL:
		case LG_CURR:
		case LG_LONG:
		case LG_NMBR:
			if (myFI->mDBLength > MAX_NUMBER_LENGTH)
				myFI->mCellWidth = myFI->mDBLength = MAX_NUMBER_LENGTH;
			myFI->mIsString = false; 
			break;
		case LG_BOOL:
//h - start
		case LG_BOL1:
//h - end
		case LG_BOL2:
			myFI->mDBLength = BOOL_LENGTH;
			myFI->mCellWidth = BOOL_WIDTH;
			myFI->mSortable = false;
			break;
		case LG_LSTU:
		case LG_CDAT:
			myFI->mDBLength = DATE_TIME_LENGTH;
			myFI->mCellWidth = DATE_TIME_WIDTH;
			myFI->mRequired = true;
			myFI->mReadOnly = true;
			{
				TIME_ZONE_INFORMATION myTZI;
				int ret = GetTimeZoneInformation(&myTZI);
				if (ret == 0xFFFFFFFF)		// error
					myFI->mTimeZoneDif = 0;
				else										// UTC - bias = local time
					myFI->mTimeZoneDif = myTZI.Bias + myTZI.DaylightBias;
			}
			break;
		case LG_FKEY:
		case LG_FURN:
		case LG_FREL:
			myFsD->RemoveAt(myFsD->GetUpperBound()); // not to be displayed
			delete myFI;	
			continue; 
			break;
		case LG_SLST:
		case LG_MLST:
			if (myFI->mComboTabField.IsEmpty())		// there is no reference to the 
			{							// source table containing the items description
				CString OutputStr;
				CString WhichURNO;
				if (CurrentURNOIsSet) WhichURNO = _T("current");
				else WhichURNO = _T("previous");
				OutputStr.Format(_T("Invalid %s field in %s. The value cannot be Null. The %s %s = %s."), 
										SYS_FIELD[F_REFE], 
										SysTable,
										(LPCTSTR)WhichURNO,
										SYS_FIELD[F_URNO],
										CurrentURNO);
				AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);
				return false;
			}
			if (myFI->mLGType == LG_MLST)
			{
				myFI->mComboTabField = myFI->mComboTabField.Left(SYS_FIELD_LENGTH[F_TANA] + EXT_LENGTH);
				myFI->mSortable = false;
			}
			break;
		default:
			break;
		}

		bool myDefaultValueProblem;
		if (!SetDefaultValue(myFI, myDefaultValueProblem))
		{
			if (myDefaultValueProblem) 	
			{												
				CString OutputStr;
				CString WhichURNO;
				if (CurrentURNOIsSet) WhichURNO = _T("current");
				else WhichURNO = _T("previous");
				CString CannotBeNull;
				if (myFI->mRequired && myFI->mDefaultStrValue.IsEmpty())
					CannotBeNull = _T("The value cannot be Null.");
				OutputStr.Format(_T("Invalid %s field in %s. %s The %s %s = %s."), 
										SYS_FIELD[F_DEFV], 
										SysTable,
										CannotBeNull,
										(LPCTSTR)WhichURNO,
										SYS_FIELD[F_URNO],
										CurrentURNO);
				AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);
			}
			return false;
		}

		CString *myLTN;
		if (!gLongTableNames.Lookup(myShortTableName, myLTN))
		{		// if there is no long name supply it by the short one
			myLTN = new CString(myShortTableName);
			gLongTableNames.SetAt(myShortTableName, myLTN);
		}
	} 

	if (!LoadTabInfo())
		return false;

//h - start
	for (int myStop = 0; myStop < gTables.GetSize(); myStop++)
	{
		CString *myStopString;
		if (!gLongTableNames.Lookup(gTables[myStop] + gTableExt, myStopString))
			return false;
		for (int myStart = 0; myStart < myStop; myStart++)
		{
			CString *myCompareString;
			if (!gLongTableNames.Lookup(gTables[myStart] + gTableExt, myCompareString))
				return false;
			if (*myStopString <= *myCompareString)
			{
				CString myTempString = gTables[myStop];
				gTables.RemoveAt(myStop);
				gTables.InsertAt(myStart, myTempString); // item exchange
				break;
			}
		}
	}
//h - end			

	//*** 15.11.99 SHA ***
	//return true;
	return !olSystabError;
}

//-----------------------------------------------------------------------
// LoadTabInfo()
// 
// The function reads the data from the TABTAB table
//-----------------------------------------------------------------------
bool CDBDPSDoc::LoadTabInfo() 
{
	CString TabTable(TABTAB + CString(gTableExt));
	CString Tables(TabTable);
	CString SelectFields;			// build the select clause

	for (int i = 0; i < NUM_TABFIELDS; i++) 
	{
		if (!SelectFields.IsEmpty()) SelectFields += DELIMITER_CHAR;
		// SelectFields += TabTable + POINT_CHAR + TAB_FIELD[i]; // with prefix
		SelectFields += TAB_FIELD[i];		// without prefix
	}

	int ret = DBDPSCallCeda(UFIS_SELECT, Tables, NULL,
							 NULL, SelectFields, NULL);

	if (ret)
	{
		CString OutputStr;
		OutputStr.Format(_T("Error on reading %s. ") + gErrorMessage + POINT_CHAR, 
						(LPCTSTR)Tables);
		AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);
		return false;
	}

	HGLOBAL myHandle;
	GetBufferHandle(UFIS_BUF, &myHandle);
	LPTSTR DataPtr = (LPTSTR)GlobalLock(myHandle);		// data from the TABTAB
	
	TCHAR Value[MAX_VALUE_LENGTH + 1];
	LPTSTR ValuePtr = Value;

	ROWCOL nCols = NUM_TABFIELDS;
	ROWCOL nRows = GetNumberOfLines(UFIS_BUF);

	//*** 02.12.1999 SHA ***
	//*** CLEAR KEY-MAP ***
	ogTableKeys.RemoveAll();

	for(ROWCOL row = 1; row <= nRows; row++)
	{		
		CString CurrentURNO;
		bool CurrentURNOIsSet = false;
		CString myLongTableName;
		CString myShortTableName;
		for(ROWCOL col = 0; col < nCols; col++)
		{
			ValuePtr = Value;		
			if (!GetToken(DataPtr, ValuePtr) && col != nCols - 1 && row != nRows)	// GetToken finished earlier
			{
				AfxMessageBox("LoadTabInfo(). CallCeda returned invalid data buffer.", MB_OK + MB_ICONSTOP);
				return false;
			}

			if (!_tcscmp(Value, DB_NULL_VALUE))	// Null value?
				*Value = 0;

			CString myTempValue(ValuePtr);		// conversion
			ConvertStringToClient(myTempValue);
			_tcscpy(ValuePtr, myTempValue);

			switch (col)		// for every SYSTAB field there is the case statement
			{
				case F_URNO:
					if (!CheckAndTrim(TabTable,		// makes trim as necessary and checks the value
											TAB_FIELD[col],	// according to the log. type settings
											TAB_FIELD[TAB_URNO], 
											CurrentURNO, 
											CurrentURNOIsSet, 
											ValuePtr, 
											TAB_FIELD_LENGTH[col]))
						return false;
					CurrentURNO = ValuePtr;
					CurrentURNOIsSet = true;
					break;
				case TAB_LTNA:
					if (!CheckAndTrim(TabTable,		// makes trim as necessary and checks the value
											TAB_FIELD[col],	// according to the log. type settings
											TAB_FIELD[TAB_URNO], 
											CurrentURNO, 
											CurrentURNOIsSet, 
											ValuePtr, 
											TAB_FIELD_LENGTH[col], true, false))
						return false;
					myShortTableName = ValuePtr;
					break;
				case TAB_LNAM:
					if (!CheckAndTrim(TabTable,		// makes trim as necessary and checks the value
											TAB_FIELD[col],	// according to the log. type settings
											TAB_FIELD[TAB_URNO], 
											CurrentURNO, 
											CurrentURNOIsSet, 
											ValuePtr, 
											TAB_FIELD_LENGTH[col]))
						return false;
					myLongTableName = ValuePtr;
					break;
				//*** 02.12.1999 SHA ***
				//*** THE KEYS ARE STORED IN THE DVIW-COL ***
				case TAB_DVIW:
					afxDump << "ValuePtr:" << ValuePtr <<"\n";
					ogTableKeys.SetAt(myShortTableName,ValuePtr);

					break;
			default:
				AfxMessageBox("LoadTabInfo(). Invalid TABTAB field.", MB_OK + MB_ICONSTOP);
				return false;
				break;
			}
		}

		CString *myLTN;
		if (!gLongTableNames.Lookup(myShortTableName, myLTN))
			continue;
		if (myLongTableName.IsEmpty()) 
			continue;
		*myLTN = myLongTableName;
	}

	return true;
}

//-----------------------------------------------------------------------
// LoadSecFieldInfo()
// 
// The function reads the security information for particular fields
//-----------------------------------------------------------------------
bool CDBDPSDoc::LoadSecFieldInfo() 
{
	for (int i = 0; i < gTables.GetSize(); i++)
	{
		CFieldsDesc *myFieldsDesc;
		CString my3LcTableName = gTables.GetAt(i);
		if (!gTablesDesc.Lookup(my3LcTableName, myFieldsDesc))	
		{
			AfxMessageBox("LoadSecFieldInfo(). Table name not found.", MB_OK + MB_ICONSTOP);
			return false;
		}
		for (int j = 0; j < myFieldsDesc->GetSize(); j++)
		{
			CFieldInfo *myFieldInfo = myFieldsDesc->GetAt(j);
			if (!gCanMainUpdateRow) 
				myFieldInfo->mReadOnly = true;
			if (GetSecurityStatus(my3LcTableName + gTableExt, _T("UPDATE_ROW")) != CString(SEC_STAT_ACT))
				myFieldInfo->mReadOnly = true;
			if (GetSecurityStatus(my3LcTableName + gTableExt, myFieldInfo->mFieldName) != CString(SEC_STAT_ACT))
				myFieldInfo->mReadOnly = true;
			if (GetSecurityStatus(my3LcTableName + gTableExt, myFieldInfo->mFieldName) == CString(SEC_STAT_INV))
				myFieldInfo->mVisible = false;
		}
	}
	return true;
}

//-----------------------------------------------------------------------
// CheckAndTrim()
// 
// The function checks and trims the string value according to DB length 
// and outputs no message
//-----------------------------------------------------------------------
bool CDBDPSDoc::CheckAndTrim
	(
		LPTSTR &pValue,		// the string value to be checked and trimmed
		int pLength,			// the required length
		bool pRequired,		// true if the pValue cannot be null
		bool pCanBeShorter	// true if the pValue can be shorter than pLength
	)
{
	return CheckAndTrim(NULL, NULL, NULL, NULL, 
						false, pValue,	pLength, pRequired, pCanBeShorter, true);
}

//-----------------------------------------------------------------------
// CheckAndTrim()
// 
// The function checks and trims the string value according to DB length
//-----------------------------------------------------------------------
bool CDBDPSDoc::CheckAndTrim
	(
		LPCTSTR TableName,	// the read table
		LPCTSTR FieldName,	// the read field
		LPCTSTR URNOName,		// the name of URNO field
		LPCTSTR URNOValue,	// the value of URNO
		bool URNOIsCurrent,	// true if the URNO is current, false if previous
		LPTSTR &pValue,		// the string value to be checked and trimmed
		int pLength,			// the required length
		bool pRequired,		// true if the pValue is mandatory
		bool pCanBeShorter,	// true if the pValue can be shorter than pLength
		bool pNoMessage		// true if no error message will be outputed
	)
{
	if (pRequired && !*pValue) 
	{
		if (pNoMessage) 
			return false;
		CString OutputStr;
		CString WhichURNO;
		if (URNOIsCurrent) WhichURNO = _T("current");
		else WhichURNO = _T("previous");
		OutputStr.Format(_T("Invalid %s field in %s. %s The %s %s = %s."), 
								FieldName,
								TableName,
								_T("The value cannot be NULL."), 
								(LPCTSTR)WhichURNO,
								URNOName,
								URNOValue);
		//*** 15.11.99 SHA ***
		//AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);
		WriteLogFile(OutputStr);

		return false;
	}
	if (!*pValue) 
		return true;

	LPTSTR StrBegin = pValue;
	int Count;

	// trim left 
	for (Count = 0;	
		  *StrBegin == BLANK_CHAR &&	//	still blank
		  Count < MAX_VALUE_LENGTH;	//	less than MAX_VALUE_LENGTH
		  StrBegin++, Count++);			// increment
	
	if (Count >= MAX_VALUE_LENGTH && *StrBegin) 
	{
		if (pNoMessage) 
			return false;
		CString OutputStr;
		CString WhichURNO;
		if (URNOIsCurrent) WhichURNO = _T("current");
		else WhichURNO = _T("previous");
		OutputStr.Format(_T("Invalid %s field in %s. %s The %s %s = %s."), 
								FieldName,
								TableName,
								_T("The value is too long."), 
								(LPCTSTR)WhichURNO,
								URNOName,
								URNOValue);
		//*** 15.11.99 SHA ***
		//AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);
		WriteLogFile(OutputStr);
		return false;
	}

	// go to the end of the string
	LPTSTR StrEnd; 
	for (StrEnd = StrBegin;
		  *StrEnd &&						// not yet the end
		  Count < MAX_VALUE_LENGTH;	//	less than MAX_VALUE_LENGTH
		  StrEnd++, Count++);			// increment

	if (*StrEnd)		// still not the end
	{
		if (pNoMessage) 
			return false;
		CString OutputStr;
		CString WhichURNO;
		if (URNOIsCurrent) WhichURNO = _T("current");
		else WhichURNO = _T("previous");
		OutputStr.Format(_T("Invalid %s field in %s. %s The %s %s = %s."), 
								FieldName,
								TableName,
								_T("The value is too long."), 
								(LPCTSTR)WhichURNO,
								URNOName,
								URNOValue);
		//*** 15.11.99 SHA ***
		//AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);
		WriteLogFile(OutputStr);
		return false;
	}
	
	if (StrBegin == StrEnd && !pRequired)
	{
		pValue = StrBegin;	// possibly the new begin of the pValue
		return true;
	}
	// trim right
	for (--StrEnd; StrBegin != StrEnd && *StrEnd == BLANK_CHAR; StrEnd--, Count--);
	if ((StrEnd - StrBegin + 1) > pLength)
	{
		if (pNoMessage) 
			return false;
		CString OutputStr;
		CString WhichURNO;
		if (URNOIsCurrent) WhichURNO = _T("current");
		else WhichURNO = _T("previous");
		OutputStr.Format(_T("Invalid %s field in %s. %s The %s %s = %s."), 
								FieldName,
								TableName,
								_T("The value is too long."), 
								(LPCTSTR)WhichURNO,
								URNOName,
								URNOValue);
		//*** 15.11.99 SHA ***
		//AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);
		WriteLogFile(OutputStr);
		return false;
	}
	if (!pCanBeShorter && (StrEnd - StrBegin + 1) < pLength)
	{
		if (pNoMessage) 
			return false;
		CString OutputStr;
		CString WhichURNO;
		if (URNOIsCurrent) WhichURNO = _T("current");
		else WhichURNO = _T("previous");
		OutputStr.Format(_T("Invalid %s field in %s. %s The %s %s = %s."), 
								FieldName,
								TableName,
								_T("The value is too short."), 
								(LPCTSTR)WhichURNO,
								URNOName,
								URNOValue);
		//*** 15.11.99 SHA ***
		//AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);
		WriteLogFile(OutputStr);
		return false;
	}
	pValue = StrBegin;	// possibly the new begin of the pValue
	*++StrEnd = 0;			// possibly the new end of the pValue
	return true;
}

//-----------------------------------------------------------------------
// DBTypeToEnum()
//	
//	The function converts the string to the corresponding EDBType value
//-----------------------------------------------------------------------
EDBType CDBDPSDoc::DBTypeToEnum(const CString &pDBType)
{
	EDBType i;
	for (i = EDBType(0); i < NUM_DBTYPES; i = EDBType(i + 1))
		if (pDBType == DATABASE_TYPE[i]) return i;
	return DB_EL__;	// bad value
}

//-----------------------------------------------------------------------
// BoolTypeToEnum()
//	
//	The function converts the string to the corresponding EBoolType value
//-----------------------------------------------------------------------
EBoolType CDBDPSDoc::BoolTypeToEnum(const CString &pBoolType)
{
	EBoolType i;
	for (i = EBoolType(0); i < NUM_BOOLTYPES; i = EBoolType(i + 1))
		if (pBoolType == BOOLEAN_TYPE[i]) return i;
	return BOOL_EL__;	// bad value
}

//h - start
//-----------------------------------------------------------------------
// Bol1TypeToEnum()
//	
//	The function converts the string to the corresponding EBol1Type value
//-----------------------------------------------------------------------
EBol1Type CDBDPSDoc::Bol1TypeToEnum(const CString &pBol1Type)
{
	EBol1Type i;
	for (i = EBol1Type(0); i < NUM_BOL1TYPES; i = EBol1Type(i + 1))
		if (pBol1Type == BOL1_TYPE[i]) return i;
	return BOL1_EL__;	// bad value
}
//h - end

//*** 18.11.1999 SHA ***
EBol2Type CDBDPSDoc::Bol2TypeToEnum(const CString &pBol2Type)
{
	EBol2Type i;
	for (i = EBol2Type(0); i < NUM_BOL2TYPES; i = EBol2Type(i + 1))
		if (pBol2Type == BOL2_TYPE[i]) return i;
	return BOL2_EL__;	// bad value
}


//-----------------------------------------------------------------------
// LGTypeToEnum()
//	
//	The function converts the string to the corresponding ELGType value
//-----------------------------------------------------------------------
ELGType CDBDPSDoc::LGTypeToEnum(const CString &pLGType)
{
	for (ELGType i = ELGType(0); i < NUM_LGTYPES; i = ELGType(i + 1))
		if (pLGType == LOGICAL_TYPE[i]) return i;
	return LG_EL__;	// bad value
}

//-----------------------------------------------------------------------
// TimeTypeToEnum()
//	
//	The function converts the string to the corresponding ETimeType value
//-----------------------------------------------------------------------
ETimeType CDBDPSDoc::TimeTypeToEnum(const CString &pTimeType)
{
	for (ETimeType i = ETimeType(0); i < NUM_TIMETYPES; i = ETimeType(i + 1))
		if (pTimeType == TIME_TYPE[i]) return i;
	return TIME_EL__;	// bad value
}

//-----------------------------------------------------------------------
// TransDBToCell() 
//	
//	The function sets the value of the particular GRCell from database
//-----------------------------------------------------------------------
bool CDBDPSDoc::TransDBToCell 
	(
		CGRCell *pGRCell,
		CFieldInfo *pFieldInfo,
		LPTSTR pDBValue
	)
{
	CString myTempValue(pDBValue);				// db to client conversion
	ConvertStringToClient(myTempValue);
	_tcscpy(pDBValue, myTempValue);

	pGRCell->mCellState = CELL_STATE_NOT_USED;		// clear the mCellState

	bool ValueCanBeShorter = true;
	bool BothSidesTrim = true;
	switch (pFieldInfo->mLGType)
	{
	case LG_CHAR:
		BothSidesTrim = false;
		break;
	case LG_LSTU:
	case LG_CDAT:
	case LG_DTIM: 
	case LG_DATE: 
	case LG_TIME: 
		ValueCanBeShorter = false;
		break;
	default:
		break;
	}
	
	LPTSTR myDBValue = pDBValue;
	if (!CheckAndTrim(myDBValue, 
							pFieldInfo->mDBLength,
							pFieldInfo->mRequired,
							ValueCanBeShorter))
	{		// set to the default value
		ReplaceCellByDefault(pGRCell, pFieldInfo, true);
		return false;
	}
	if (!BothSidesTrim) 
		myDBValue = pDBValue;
	
	if (!*myDBValue)		// empty (but regular) value
	{	
		EmptyDBToCell(pFieldInfo, pGRCell);
		return true;
	}

	switch (pFieldInfo->mLGType)			// necessary conversions for every LGType
	{								// there is the case statement otherwise the pure assignment
	case LG_TIME:								
		if (!TimeDBToCell(myDBValue, 
								pGRCell->mStrValue, 
								pFieldInfo->mTimeZoneDif))
	{		// set to the default value
		ReplaceCellByDefault(pGRCell, pFieldInfo, true);
		return false;
	}
		break;
	case LG_DATE:
		if (!DateDBToCell(myDBValue, pGRCell->mStrValue))
		{		// set to the default value
			ReplaceCellByDefault(pGRCell, pFieldInfo, true);
			return false;
		}
		break;
	case LG_LSTU:
	case LG_CDAT:
	case LG_DTIM: 
		if (!DateTimeDBToCell(myDBValue, 
									pGRCell->mStrValue, 
									pFieldInfo->mTimeZoneDif))
		{		// set to the default value
			ReplaceCellByDefault(pGRCell, pFieldInfo, true);
			return false;
		}
		break;
	case LG_WDAY:
		if (!WeekDBToCell(myDBValue, pGRCell->mStrValue))
		{		// set to the default value
			ReplaceCellByDefault(pGRCell, pFieldInfo, true);
			return false;
		}
		break;
	case LG_MLST:
		if (!MlstDBToCell(myDBValue, pGRCell->mStrValue, pFieldInfo->mMultichoiceAtt))
		{		// set to the default value
			ReplaceCellByDefault(pGRCell, pFieldInfo, true);
			return false;
		}
		break;
	case LG_REAL:
	case LG_CURR:
		if (!NumberDBToCell(myDBValue, pGRCell->mStrValue, pGRCell->mNumValue, NUM_CON_REAL_NUMS))
		{		// set to the default value
			ReplaceCellByDefault(pGRCell, pFieldInfo, true);
			return false;
		}
		break;
	case LG_LONG:
	case LG_NMBR:
		if (!NumberDBToCell(myDBValue, pGRCell->mStrValue, pGRCell->mNumValue, NUM_CON_LONG_NUMS))
		{		// set to the default value
			ReplaceCellByDefault(pGRCell, pFieldInfo, true);
			return false;
		}
		break;
	case LG_BOOL: 
		if (!BoolDBToCell(myDBValue, pGRCell->mStrValue))
		{		// set to the default value
			ReplaceCellByDefault(pGRCell, pFieldInfo, true);
			return false;
		}
		break;
//h - start
	case LG_BOL1: 
		if (!Bol1DBToCell(myDBValue, pGRCell->mStrValue))
		{		// set to the default value
			ReplaceCellByDefault(pGRCell, pFieldInfo, true);
			return false;
		}
		break;
//h - end
	//*** 18.11.1999 SHA ***
	case LG_BOL2: 
		if (!Bol2DBToCell(myDBValue, pGRCell->mStrValue))
		{		// set to the default value
			ReplaceCellByDefault(pGRCell, pFieldInfo, true);
			return false;
		}
		break;

	case LG_SLST:
		if (!ComboDBToCell(myDBValue, pGRCell->mStrValue, pFieldInfo))
		{		// set to the default value
			ReplaceCellByDefault(pGRCell, pFieldInfo, true);
			return false;
		}
		break;
	default:			// other types will be taken as are
		pGRCell->mStrValue = myDBValue;				// set the value
		break;
	}			

	return true;
}


//-----------------------------------------------------------------------
// TimeDBToCell()
//	
//	The function converts the DB TIME format to the cell format
//-----------------------------------------------------------------------
bool CDBDPSDoc::TimeDBToCell
	(
		LPCTSTR pDBValue, 
		CString &pCellValue,
		int pTimeZoneDif
	)
{
	if (!_tcsicmp(pDBValue, CURRENT_DATE_TIME))
	{
		CurrentDateTimeToCell(pCellValue);
		pCellValue.Right(STR_TIME_END - STR_TIME_BEGIN);
	}
	else
	{
		int myHour, myMin;
		_stscanf(pDBValue + STR_TIME_BEGIN, _T("%2d%2d%*2d"), &myHour, &myMin);
		if (!CheckDateTime(1900, 1, 1, myHour, myMin)) 
			return false;

		COleDateTime dt(1900, 1, 1, myHour, myMin, 0);
		dt = dt - COleDateTimeSpan(0, 0, pTimeZoneDif, 0);

		pCellValue.Format(_T("%02d%02d"), dt.GetHour(), dt.GetMinute());
	}

	return true;
}

//-----------------------------------------------------------------------
// DateTimeDBToCell()
//	
//	The function converts the DB TIME format to the cell format
//-----------------------------------------------------------------------
bool CDBDPSDoc::DateTimeDBToCell
	(
		LPCTSTR pDBValue, 
		CString &pCellValue,
		int pTimeZoneDif
	)
{
	if (!_tcsicmp(pDBValue, CURRENT_DATE_TIME))
		CurrentDateTimeToCell(pCellValue);
	else
	{
		int myYear, myMonth, myDay, myHour, myMin;
		_stscanf(pDBValue, _T("%4d%2d%2d%2d%2d%*2d%"), 
					&myYear, &myMonth, &myDay, &myHour, &myMin);
		if (!CheckDateTime(myYear, myMonth, myDay, myHour, myMin)) return false;

		COleDateTime dt(myYear, myMonth, myDay, myHour, myMin, 0);
		dt = dt - COleDateTimeSpan(0, 0, pTimeZoneDif, 0);

		pCellValue.Format(_T("%04d%02d%02d%02d%02d"), 
							dt.GetYear(), dt.GetMonth(), dt.GetDay(),
							dt.GetHour(), dt.GetMinute());
	}

	return true;
}

//-----------------------------------------------------------------------
// CurrentDateTimeToCell()
//	
//	The function puts the current date and time to the cell format
//-----------------------------------------------------------------------
bool CDBDPSDoc::CurrentDateTimeToCell(CString &pCellValue)
{
	COleDateTime dt = COleDateTime::GetCurrentTime();
	pCellValue.Format(_T("%04d%02d%02d%02d%02d"),
						dt.GetYear(),
						dt.GetMonth(),
						dt.GetDay(),
						dt.GetHour(),
						dt.GetMinute());

	return true;
}

//-----------------------------------------------------------------------
// DateDBToCell()
//	
//	The function converts the DB DATE format to the cell format
//-----------------------------------------------------------------------
bool CDBDPSDoc::DateDBToCell
	(
		LPCTSTR pDBValue, 
		CString &pCellValue
	)
{
	if (!_tcsicmp(pDBValue, CURRENT_DATE_TIME))
	{
		CurrentDateTimeToCell(pCellValue);
		pCellValue.Left(STR_DATE_END);
	}
	else
	{
		int myYear, myMonth, myDay;
		_stscanf(pDBValue, _T("%4d%2d%2d%*2d%*2d%*2d%"), 
					&myYear, &myMonth, &myDay);
		if (!CheckDateTime(myYear, myMonth, myDay, 0, 0)) 
			return false;
		pCellValue = CString(pDBValue + STR_DATE_BEGIN, 
									STR_DATE_END - STR_DATE_BEGIN);
	}

	return true;
}

//-----------------------------------------------------------------------
// WeekDBToCell()
//	
//	The function converts the DB WDAY format to the cell format
//-----------------------------------------------------------------------
bool CDBDPSDoc::WeekDBToCell
	(
		LPCTSTR pDBValue, 
		CString &pCellValue
	)
{
	CString myWeekDays(EMPTY_WEEK);			
	while (*pDBValue)
	{
		int windex = *pDBValue - _T('0');		// convert char to index
		if (windex < 1 || windex > NUM_WEEKDAYS) 
			return false;
		myWeekDays.SetAt(windex - 1, *pDBValue);
		pDBValue++;
	}
	pCellValue = myWeekDays;				// set the value

	return true;
}

//-----------------------------------------------------------------------
// WeekCellToDB
//	
//	The function converts the cell WDAY format to the DB format
//-----------------------------------------------------------------------
void CDBDPSDoc::WeekCellToDB
	(
		LPCTSTR pCellValue,
		CString &pDBValue
	)
{
	TCHAR myDBValue[NUM_WEEKDAYS + 1];
	LPTSTR myDBValuePtr = myDBValue;
	while (*pCellValue)
	{
		TCHAR day;
		if (!_istdigit(day = *pCellValue++)) continue;
		*myDBValuePtr++ = day;
	}
	*myDBValuePtr = 0;
	pDBValue = myDBValue;
}

//-----------------------------------------------------------------------
// MlstDBToCell()
//	
//	The function converts the DB MLST format to the cell format
//-----------------------------------------------------------------------
bool CDBDPSDoc::MlstDBToCell
	(
		LPCTSTR pDBValue, 
		CString &pCellValue,
		CWDayCheckListComboBox::Attributes *pAttr
	)
{
	CString myCodeValue(_T('0'), pAttr->Items);
	int myNumber = 0;

	for (LPCTSTR myChar = pDBValue; true; myChar++)
	{
		if (*myChar == BLANK_CHAR)		// blank is allowed
			continue;
		if (!*myChar || *myChar == MLST_DELIMITER)	// create the number
		{
			if (!myNumber) 
				break;
			if (myNumber > pAttr->Items)
				return false;
			myCodeValue.SetAt(myNumber - 1, _T('1'));
			myNumber = 0;
			if (*myChar)
				continue;
			else 
				break;
		}
		if (_istdigit(*myChar))		// digit
			myNumber = myNumber * 10 + *myChar - _T('0');
		else 
			return false;		// bad character
	}

	CString myMlstString;
	for (int i = 0; i < pAttr->Items; i++)
	{
		CString myItem;
		if (myCodeValue.GetAt(i) == _T('1'))
			myItem = pAttr->Checked[i];
		else 
			myItem = pAttr->Unchecked[i];

		if (pAttr->SmartSeparator && !myItem.IsEmpty() && !myMlstString.IsEmpty()) 
			myMlstString += pAttr->Separator;
		else if (!pAttr->SmartSeparator && i != 0) 
			myMlstString += pAttr->Separator;

		myMlstString += myItem;
	}
	pCellValue = myMlstString;
	
	return true;
}

//-----------------------------------------------------------------------
// MlstCellToDB
//	
//	The function converts the cell MLST format to the DB format
//-----------------------------------------------------------------------
void CDBDPSDoc::MlstCellToDB
	(
		LPCTSTR pCellValue,
		CString &pDBValue,
		CWDayCheckListComboBox::Attributes *pAttr
	)
{
	CString myCodeValue = pCellValue;
	gWdayControl->EncodeValue(pAttr, myCodeValue);
	CString myDBValue;
	for (int i = 0; i < pAttr->Items; i++)
		if (myCodeValue.GetAt(i) == _T('1'))
		{
			if (!myDBValue.IsEmpty())
				myDBValue += MLST_DELIMITER;
			TCHAR myString[10];
			myDBValue += _itot(i + 1, myString, 10);
		}

	pDBValue = myDBValue;
}

//-----------------------------------------------------------------------
// NumberDBToCell()
//	
//	The function converts the DB LONG or REAL format to the cell format
//-----------------------------------------------------------------------
bool CDBDPSDoc::NumberDBToCell
	(
		LPCTSTR pDBValue,				// input
		CString &pCellStrValue,		// str output (if valid)
		double &pCellNumValue,		// num output (if valid)
		int pAfterPoint				// number of digits after point
	)
{
	CString cstr = pDBValue;
	LPTSTR  str  = cstr.GetBuffer(0);
					 
	LPTCH	 ch  = str;			// ptr to actual char from the "str" to be processed
	double num = 0.;			// the nummber retrieved from the "str"
	double exp = 1.;			// exponent ( using only after processing chars after "." )

	int Blanks1		= 0;		// leading blanks
	int Blanks2		= 0;		// blanks after signature
	int Nulls		= 0;		// leading nulls ("0")
	int Sign			= 0;		// sign (+/-)
	int Nums1		= 0;		// nummbers before "."
	int Nums2		= 0;		// nummbers after "."

	// leading blanks
	while (*ch == NUM_CON_BLANK1 || *ch == NUM_CON_BLANK2)
	{ 
		ch++; 
		Blanks1++; 
	}

	// sign
	if (*ch == NUM_CON_PLUS)
	{ 
		ch++; 
		Sign = 1; 
	}
	else if (*ch == NUM_CON_MINUS)
	{ 
		ch++; 
		Sign = 2; 
	}

	// blanks after sign
	while (*ch == NUM_CON_BLANK1 || *ch == NUM_CON_BLANK2)
	{ 
		ch++; 
		Blanks2++; 
	}

	// leading "0" nulls
	while (*ch == NUM_CON_NULL_NUMERAL)
	{ 
		ch++; 
		Nulls++; 
	}

	// the numbers before the decimal symbol
	while (*ch >= NUM_CON_MIN_NUMERAL && *ch <= NUM_CON_MAX_NUMERAL)
	{ 
		num = num * 10 + (int)(*ch - NUM_CON_MIN_NUMERAL);
		ch++; 
		Nums1++; 
	}

	// is there a decimal symbol?
	if (*ch == gDecimalSymbol || *ch == NUM_CON_POINT || *ch == NUM_CON_COMMA)
	{
		if (pAfterPoint>0) 
			ch++;
		else
			return false;
	}

	// the numbers after "."
	while (*ch >= NUM_CON_MIN_NUMERAL && *ch <= NUM_CON_MAX_NUMERAL)
	{ 
		exp /= 10;
		num += exp * (int)(*ch - NUM_CON_MIN_NUMERAL);

		ch++; 
		Nums2++;
	}

	// ending blanks
	while (*ch == NUM_CON_BLANK1 || *ch == NUM_CON_BLANK2)	ch++; 

	// if no char processed ___ its invalid
	if (ch == str) 
		return false;

	// if not the whole string processed ___ its invalid
	if (*ch != NUM_CON_END_OF_STRING) 
		return false;

	// if no "0"nulls && no numbers before "." and no nums. after "." ___ its invalid
	if (Nulls == 0 && Nums1 == 0 && Nums2 == 0) 
		return false;

	// testing if there follows the valid number of
	//		literals after "."
	// -->
	if (Nums2 > pAfterPoint) 
		return false;
	// <--

	// pos = where do the number_chars begins
	// pos1 = where should I copy them to

	int pos = (Sign == 0) ? Blanks1 : Blanks1 + Blanks2 + 1;
	int pos1 = 0;
	bool valid = false;

	if (Sign == 2 && (Nums1 != 0 || Nums2 != 0))
		// the first char is "-", shift pos1 to the next char
		str[pos1++] = NUM_CON_MINUS;

	// ignore leading "0"nulls
	pos += Nulls;

	if (Nums1 != 0)
	{
		CopyString(str+pos1, str+pos, Nums1);
		pos += Nums1;
		pos1 += Nums1;
		valid = true;
	}

	if (Nums2 != 0)
	{
		CopyString(str+pos1, str+pos, Nums2+1);
		pos += Nums2 + 1;
		pos1 += Nums2 + 1;
		valid = true;
	}

	// if no number before"." and no after, BUT some "0"nulls then :
	if (!valid && Nulls != 0) 
	{
		str[pos1++] = NUM_CON_NULL_NUMERAL;
		valid = true;
	}

	// here the "valid" must be always true
	if (valid) 
	{
		str[pos1] = NUM_CON_END_OF_STRING;
		pCellNumValue = num;
		if (Sign == 2) pCellNumValue *= -1;
	}

	cstr.ReleaseBuffer();

	if (valid)
		pCellStrValue = cstr;

	return valid;
}

//-----------------------------------------------------------------------
// CopyString																	
//	
//	The function copies string (at most bts bytes). Serves for NumberDBToCell()
//-----------------------------------------------------------------------
void CDBDPSDoc::CopyString
	(	
		LPTSTR dest,	// destination string
		LPTSTR src,		// source string
		int bts			// copy at most bts bytes
	)					
{
	LPTCH ptrDest	= dest;
	LPTCH ptrSrc	= src;
	
	while (bts > 0)
	{
		if (*ptrSrc == 0) break;
		*ptrDest = *ptrSrc;
		ptrSrc++;
		ptrDest++;
		bts--;
	}
}

//-----------------------------------------------------------------------
// NumberGRToCell()
//	
//	The function converts the GR real or long format to the cell format
//-----------------------------------------------------------------------
bool CDBDPSDoc::NumberGRToCell
	(
		const CString &pGRValue, 
		CString &pCellStrValue,
		double &pCellNumValue,
		int pAfterPoint
	)
{
	// pCellStrValue = pGRValue;
	// pCellNumValue = _gxttof(pCellStrValue);
	// return true;
	return NumberDBToCell(pGRValue, pCellStrValue, pCellNumValue, pAfterPoint);
}

//-----------------------------------------------------------------------
// TimeGRToCell()
//	
//	The function converts the GR time format to the cell format
//-----------------------------------------------------------------------
bool CDBDPSDoc::TimeGRToCell
	(
		const CString &pGRValue, 
		CString &pCellValue
	)
{
	COleDateTime dt;
	dt.ParseDateTime(pGRValue);

	if (pGRValue.Find(_T(":")) == -1) return false;
	pCellValue.Format(_T("%02u%02u"), dt.GetHour(), dt.GetMinute());
	return true;
}

//-----------------------------------------------------------------------
// DateTimeGRToCell()
//	
//	The function converts the GR time format to the cell format
//-----------------------------------------------------------------------
bool CDBDPSDoc::DateTimeGRToCell
	(
		const CString &pGRValue, 
		CString &pCellValue
	)
{
	COleDateTime dt;
	dt.ParseDateTime(pGRValue);

	pCellValue.Format(_T("%04d%02d%02d%02d%02d"), 
					dt.GetYear(), dt.GetMonth(), dt.GetDay(),
					dt.GetHour(), dt.GetMinute());
	return true;
}

//-----------------------------------------------------------------------
// DateGRToCell()
//	
//	The function converts the GR date format to the cell format
//-----------------------------------------------------------------------
bool CDBDPSDoc::DateGRToCell
	(
		const CString &pGRValue, 
		CString &pCellValue
	)
{
	COleDateTime dt;
	dt.ParseDateTime(pGRValue);
	
	if (pGRValue.Find(_T(".")) == -1) return false;
	pCellValue.Format(_T("%04u%02u%02u"), dt.GetYear(), dt.GetMonth(), dt.GetDay());
	return true;
}

//-----------------------------------------------------------------------
// TimeCellToGR()
//	
//	The function converts the cell time format to the grid format
//-----------------------------------------------------------------------
bool CDBDPSDoc::TimeCellToGR
	(
		const CString &pCellValue, 
		CString &pGRValue
	)
{
	pGRValue = pCellValue.Left(2) + COLON_CHAR + pCellValue.Mid(2,2);
	return true;
}

//-----------------------------------------------------------------------
// DateTimeCellToGR()
//	
//	The function converts the cell time format to the grid format
//-----------------------------------------------------------------------
bool CDBDPSDoc::DateTimeCellToGR
	(
		const CString &pCellValue, 
		CString &pGRValue
	)
{
	pGRValue = pCellValue.Mid(6,2) + POINT_CHAR + 
					pCellValue.Mid(4,2) + POINT_CHAR + 
					pCellValue.Left(4) + BLANK_CHAR + 
					pCellValue.Mid(8,2) + COLON_CHAR +
					pCellValue.Mid(10,2);

	return true;
}

//-----------------------------------------------------------------------
// TimeCellToDB()
//	
//	The function converts the cell time format to the DB format
//-----------------------------------------------------------------------
void CDBDPSDoc::TimeCellToDB
	(
		const CString &pCellValue, 
		CString &pDBValue,
		int pTimeZoneDif
	)
{
	int myHour, myMin;
	_stscanf(pCellValue, _T("%2d%2d"), &myHour, &myMin);
	COleDateTime dt(1900, 1, 1, myHour, myMin, 0);
	dt = dt + COleDateTimeSpan(0, 0, pTimeZoneDif, 0);

	pDBValue.Format(_T("00000000%02d%02d00"), dt.GetHour(), dt.GetMinute());
}

//-----------------------------------------------------------------------
// DateTimeCellToDB()
//	
//	The function converts the cell time format to the database format
//-----------------------------------------------------------------------
void CDBDPSDoc::DateTimeCellToDB
	(
		const CString &pCellValue, 
		CString &pDBValue,
		int pTimeZoneDif
	)
{
	int myYear, myMonth, myDay, myHour, myMin;
	_stscanf(pCellValue, _T("%4d%2d%2d%2d%2d%2d"), 
				&myYear, &myMonth, &myDay, &myHour, &myMin);
	COleDateTime dt(myYear, myMonth, myDay, myHour, myMin, 0);
	dt = dt + COleDateTimeSpan(0, 0, pTimeZoneDif, 0);

	pDBValue.Format(_T("%04d%02d%02d%02d%02d00"), 
						dt.GetYear(), dt.GetMonth(), dt.GetDay(),
						dt.GetHour(), dt.GetMinute());
}

//-----------------------------------------------------------------------
// DateCellToDB()
//	
//	The function converts the cell date format to the database format
//-----------------------------------------------------------------------
void CDBDPSDoc::DateCellToDB
	(
		const CString &pCellValue, 
		CString &pDBValue
	)
{
	pDBValue = pCellValue + _T("000000");
}

//-----------------------------------------------------------------------
// DateCellToGR()
//	
//	The function converts the cell date format to the grid format
//-----------------------------------------------------------------------
bool CDBDPSDoc::DateCellToGR
	(
		const CString &pCellValue, 
		CString &pGRValue
	)
{
	pGRValue = pCellValue.Mid(6,2) + POINT_CHAR + 
		pCellValue.Mid(4,2) + POINT_CHAR + pCellValue.Left(4);
	return true;
}

//-----------------------------------------------------------------------
// ComboDBToCell()
//	
//	The function converts the cell SLST format to the grid format
//-----------------------------------------------------------------------
bool CDBDPSDoc::ComboDBToCell
	(
		LPCTSTR pDBValue, 
		CString &pCellValue,
		CFieldInfo *pFieldInfo
	)
{
	int pos; 
	if ((pos = pFieldInfo->mChoiceList.Find(pDBValue)) == -1)
		return false;
	if (pos > 0 && pFieldInfo->mChoiceList.GetAt(pos - 1) != LF_CHAR) 
		return false;
	if (pFieldInfo->mChoiceList.GetAt(pos + _tcslen(pDBValue)) != LF_CHAR) 
		return false;
	pCellValue = pDBValue;				// set the value

	return true;
}

//-----------------------------------------------------------------------
// BoolDBToCell()
//	
//	The function converts the cell BOOL format to the grid format
//-----------------------------------------------------------------------
bool CDBDPSDoc::BoolDBToCell
	(
		LPCTSTR pDBValue, 
		CString &pCellValue
	)
{
	if (BoolTypeToEnum(pDBValue) == BOOL_EL__) return false;
	pCellValue = pDBValue;				// set the value

	return true;
}

//h - start
//-----------------------------------------------------------------------
// Bol1DBToCell()
//	
//	The function converts the cell BOL1 format to the grid format
//-----------------------------------------------------------------------
bool CDBDPSDoc::Bol1DBToCell
	(
		LPCTSTR pDBValue, 
		CString &pCellValue
	)
{
	if (Bol1TypeToEnum(pDBValue) == BOL1_EL__) return false;
	pCellValue = pDBValue;				// set the value

	return true;
}
//h - end

//*** 18.11.1999 SHA ***
bool CDBDPSDoc::Bol2DBToCell
	(
		LPCTSTR pDBValue, 
		CString &pCellValue
	)
{
	if (Bol2TypeToEnum(pDBValue) == BOL2_EL__) return false;
	pCellValue = pDBValue;				// set the value

	return true;
}

//-----------------------------------------------------------------------
// SetStyleColor()
//	
//	The function converts the cell date format to the grid format
//-----------------------------------------------------------------------
void CDBDPSDoc::SetStyleColor
	(
		CGXStyle &pStyle,
		ECLType pColor
	)
{
	switch (pColor) {
	case CL_YELLOW:
			pStyle.ChangeStyle(CGXStyle()
						.SetInterior(RGB(255,255,64))		// yellow background
						.SetTextColor(RGB(0,0,0)),				// black textcolor
						gxOverride);				
			break;
	case CL_GREEN:
			pStyle.ChangeStyle(CGXStyle()
						.SetInterior(RGB(255,255,255))		// white background
						.SetTextColor(RGB(0,150,0)),		// green textcolor
						gxOverride);				
			break;
	case CL_RED:
			pStyle.ChangeStyle(CGXStyle()
						.SetInterior(RGB(255,64,64))		// red background
						.SetTextColor(RGB(0,0,0)),			// black textcolor
						gxOverride);				
			break;
	case CL_LIGHT_RED:
			pStyle.ChangeStyle(CGXStyle()
						.SetInterior(RGB(255,145,162))		// light red background
						.SetTextColor(RGB(0,0,0)),				// black textcolor
						gxOverride);				
			break;
	case CL_STANDART:
			pStyle.ChangeStyle(CGXStyle()
						.SetInterior (RGB(255,255,255))		// white background
						.SetTextColor(RGB(0,0,0)),				// black textcolor
						gxOverride);				
			break;
	}
}

//-----------------------------------------------------------------------
// GetGRCellValue()
//	
//	The function puts the value of the particular GRCell to the pStyle
//-----------------------------------------------------------------------
void CDBDPSDoc::GetGRCellValue
	(	
		ROWCOL pRow,
		ROWCOL pCol,
		CGXStyle &pStyle
	)
{
	CFieldInfo *myFieldInfo = gCurrentFieldsDesc->GetAt(pCol);
	if ((int)pRow > gGRTable.GetUpperBound())
	{	// this is the place the grid shows the new (virtual) record 
			// so it has to fill it by the default values
		CGRCell myGRCell;
		ReplaceCellByDefault(&myGRCell, myFieldInfo, false);
		TransCellToGR(myFieldInfo, &myGRCell, pStyle);
		return;
	}

	int RealRow = *mIndexMap.GetAt(pRow);	// real GRTable Index
	CGRCell *myGRCell = gGRTable.GetAt(RealRow)->GetAt(pCol);
	TransCellToGR(myFieldInfo, myGRCell, pStyle);
}

//-----------------------------------------------------------------------
// ValidGRValue()
//	
//	The function checks the value of the particular GRCell from the grid
//-----------------------------------------------------------------------
bool CDBDPSDoc::ValidGRValue
	(	
		ROWCOL pCol,
		const CString &pGRValue,
		CString &pNewValue
	)
{
	double myNumber;
	return TransGRToCell(pCol, pGRValue, pNewValue, myNumber);
}

//-----------------------------------------------------------------------
// CheckValueSetColor()
//	
//	The function checks the value of the particular GRCell from the grid and sets
//	the corresponding color
//-----------------------------------------------------------------------
void CDBDPSDoc::CheckValueSetColor
	(	
		ROWCOL pCol,
		const CString &pGRValue,
		CGXStyle &pStyle
	)
{
	CString myValue;
	if (ValidGRValue(pCol, pGRValue, myValue))
		SetStyleColor(pStyle, CL_GREEN);
	else 
		SetStyleColor(pStyle, CL_RED);
}

//-----------------------------------------------------------------------
// CheckValueSetValue()
//	
//	The function checks the value of the particular GRCell from the grid and sets
//	the corresponding value
//-----------------------------------------------------------------------
bool CDBDPSDoc::CheckValueSetValue
	(	
		ROWCOL pCol,
		const CString &pGRValue,
		CGXStyle &pStyle
	)
{
	CString myCellValue;
	double myNumValue;

	if (!TransGRToCell(pCol, pGRValue, myCellValue, myNumValue))
		return false;

	if (!myCellValue.IsEmpty() && !gCurrentFieldsDesc->GetAt(pCol)->mIsString) 
		pStyle.SetValue(myNumValue);
	return true;
}

//-----------------------------------------------------------------------
// TransGRToCell()
//	
//	The function transforms the grid value to the cell value
//-----------------------------------------------------------------------
bool CDBDPSDoc::TransGRToCell
	(	
		ROWCOL pCol,
		const CString &pGRValue,
		CString &pCellStrValue,
		double &pCellNumValue
	)
{
	CFieldInfo *myFieldInfo = gCurrentFieldsDesc->GetAt(pCol);
	if (myFieldInfo->mRequired)	// the case the value is required and empty
	{				
		if (myFieldInfo->mLGType == LG_MLST)
		{
			CString Code = pGRValue;
			gWdayControl->EncodeValue(myFieldInfo->mMultichoiceAtt, Code);
			if (Code.Find(_T('1')) == -1) // did not find anything checked
				return false;
		}
		else if (myFieldInfo->mLGType == LG_WDAY && pGRValue == EMPTY_WEEK)
			return false;
		else if (pGRValue.IsEmpty()) 
			return false;
	}
	if (pGRValue.IsEmpty())
	{
		pCellStrValue.Empty();
		pCellNumValue = -DBL_MAX;
		return true;
	}

	CString olDmy;
	switch (myFieldInfo->mLGType)			// necessary conversions
	{			
		
		case LG_UPPC:
			olDmy = pGRValue;
			olDmy.MakeUpper();
			pCellStrValue = olDmy;
			break;
		case LG_TRIM:
		case LG_USER:
		case LG_CUSR:
			pCellStrValue = pGRValue;
			pCellStrValue.TrimLeft();
			pCellStrValue.TrimRight();
			break;
		case LG_CHAR:
			pCellStrValue = pGRValue;
			pCellStrValue.TrimRight();
			break;
		case LG_TIME:
			return TimeGRToCell(pGRValue, pCellStrValue);	// convert 
			break;
		case LG_DATE:
			return DateGRToCell(pGRValue, pCellStrValue);	// convert 
			break;
		case LG_LSTU:
		case LG_CDAT:
		case LG_DTIM: 
			return DateTimeGRToCell(pGRValue, pCellStrValue);	// convert 
			break;
		case LG_REAL:
		case LG_CURR: 
			return NumberGRToCell(pGRValue, pCellStrValue, pCellNumValue, NUM_CON_REAL_NUMS);
			break;
		case LG_LONG:
		case LG_NMBR: 
			return NumberGRToCell(pGRValue, pCellStrValue, pCellNumValue, NUM_CON_LONG_NUMS);
		default:			
			pCellStrValue = pGRValue;
			break;
	}			

	return true;
}

//-----------------------------------------------------------------------
// TransCellToGR()
//	
//	The function transforms the cell value to the grid value
//-----------------------------------------------------------------------
void CDBDPSDoc::TransCellToGR
	(	
		CFieldInfo *pFieldInfo,
		CGRCell *pGRCell,
		CGXStyle &pStyle
	)
{
	if (pGRCell->mStrValue.IsEmpty())
		pStyle.SetValue(pGRCell->mStrValue);
	else
	{
		CString myGRValue;
		switch (pFieldInfo->mLGType)
		{
			case LG_TIME:
				TimeCellToGR(pGRCell->mStrValue, myGRValue);	// convert 
				pStyle.SetValue(myGRValue);
				break;
			case LG_DATE:
				DateCellToGR(pGRCell->mStrValue, myGRValue);	// convert 
				pStyle.SetValue(myGRValue);
				break;
			case LG_LSTU:
			case LG_CDAT:
			case LG_DTIM: 
				DateTimeCellToGR(pGRCell->mStrValue, myGRValue);	// convert 
				pStyle.SetValue(myGRValue);
				break;
			case LG_REAL: 
			case LG_CURR: 
			case LG_LONG: 
			case LG_NMBR: 
				pStyle.SetValue(pGRCell->mNumValue);
				break;
			default:
				pStyle.SetValue(pGRCell->mStrValue);
				break;
		}
	}
}

//-----------------------------------------------------------------------
// TransCellToDB
//	
//	The function transforms the cell value to the database value
//-----------------------------------------------------------------------
void CDBDPSDoc::TransCellToDB
	(	
		ROWCOL pCol,
		const CString &pCellValue,
		CString &pDBValue
	)
{
	if (pCellValue.IsEmpty()) 
		pDBValue = DB_NULL_VALUE;	// the representation of the null value
	else 
	{
		CFieldInfo *myFieldInfo = gCurrentFieldsDesc->GetAt(pCol);
		switch (myFieldInfo->mLGType)
		{
			case LG_TIME:
				TimeCellToDB(pCellValue, pDBValue, myFieldInfo->mTimeZoneDif);		// convert 
				break;
			case LG_DATE:
				DateCellToDB(pCellValue, pDBValue);	// convert 
				break;
			case LG_LSTU:
			case LG_CDAT:
			case LG_DTIM: 
				DateTimeCellToDB(pCellValue, pDBValue, myFieldInfo->mTimeZoneDif);	// convert 
				break;
			case LG_WDAY:
				WeekCellToDB(pCellValue, pDBValue);	// convert 
				break;
			case LG_MLST:
				MlstCellToDB(pCellValue, pDBValue, myFieldInfo->mMultichoiceAtt);
				break;
			default:
				pDBValue = pCellValue;
				break;
		}
	}

	if (pDBValue.IsEmpty()) 
		pDBValue = DB_NULL_VALUE;	// the representation of the null value
	ConvertStringToServer(pDBValue);
	//pDBValue = _T("'") + pDBValue + _T("'");	
}

//-----------------------------------------------------------------------
// GetLocaleSettings
//	
//	The loads all the necessary regional settings data
//-----------------------------------------------------------------------
bool CDBDPSDoc::GetLocaleSettings()
{
	TCHAR Decimal[10];
	if (!GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_SDECIMAL, Decimal, sizeof(Decimal)))
		return false;
	gDecimalSymbol = *Decimal;

	return true;
}

//-----------------------------------------------------------------------
// ReplaceCellByDefault()
//	
//	The function replaces the cell value by the default.
//-----------------------------------------------------------------------
void CDBDPSDoc::ReplaceCellByDefault
	(
		CGRCell *pGRCell,
		CFieldInfo *pFieldInfo,
		bool pInvalid
	)
{	
		if (pInvalid)
			pGRCell->mCellState |= CELL_DB_INVALID;
		switch (pFieldInfo->mLGType)
		{
			case LG_REAL:
			case LG_CURR:
			case LG_LONG:
			case LG_NMBR:
				pGRCell->mStrValue = pFieldInfo->mDefaultStrValue;
				pGRCell->mNumValue = pFieldInfo->mDefaultNumValue;
				break;
			case LG_LSTU:
			case LG_CDAT:
				CurrentDateTimeToCell(pGRCell->mStrValue);
				break;
			default: 
				pGRCell->mStrValue = pFieldInfo->mDefaultStrValue;
				break;
		}
}

//-----------------------------------------------------------------------
// EmptyDBToCell()
//	
//	The function converts the empty DB value to the cell value
//-----------------------------------------------------------------------
void CDBDPSDoc::EmptyDBToCell
	(
		CFieldInfo *pFieldInfo,
		CGRCell *pGRCell
	)
{	
		switch (pFieldInfo->mLGType)
		{
			case LG_WDAY:
				WeekDBToCell(_T(""), pGRCell->mStrValue);
				break;
			case LG_REAL:
			case LG_CURR:
			case LG_LONG:
			case LG_NMBR:
				pGRCell->mStrValue.Empty();
				pGRCell->mNumValue = -DBL_MAX;
				break;
			case LG_MLST:
				MlstDBToCell(_T(""), pGRCell->mStrValue, pFieldInfo->mMultichoiceAtt);
				break;
			default: 
				pGRCell->mStrValue.Empty();
				break;
		}
}

//-----------------------------------------------------------------------
// SetDefaultValue()
//	
//	The function sets the default values for all fields.
//	Moreover the function loads the choice list for the MLST and SLST fields from the database.
//-----------------------------------------------------------------------
bool CDBDPSDoc::SetDefaultValue
	(
		CFieldInfo *pFieldInfo, 
		bool &pDefaultValueProblem
	)
{	
	CString myCellDefaultStrValue;
	double myCellDefaultNumValue;
	pDefaultValueProblem = true;

	switch (pFieldInfo->mLGType)	// choice lists
	{
		case LG_MLST:
			if (!GetMultichoiceAtt(pFieldInfo->mComboTabField, 
					pFieldInfo->mMultichoiceAtt)) 
			{
				pDefaultValueProblem = false;
				return false;
			}
			break;
		case LG_SLST:
			if (!GetChoiceList(pFieldInfo)) 
			{
				pDefaultValueProblem = false;
				return false;
			}
			break;
	}
	
	if (pFieldInfo->mRequired && pFieldInfo->mDefaultStrValue.IsEmpty()) // the default value is
		switch (pFieldInfo->mLGType)
		{
			case LG_USER:
			case LG_CUSR:
			case LG_LSTU:
			case LG_CDAT:	
				break;		// just ignore the default value
			default:
				return false;
		}
	if (pFieldInfo->mDefaultStrValue.IsEmpty()) 
		switch (pFieldInfo->mLGType)
		{
			case LG_USER:
			case LG_CUSR:
				myCellDefaultStrValue = gUserName;
				break;
			case LG_REAL:
			case LG_CURR:
			case LG_LONG:
			case LG_NMBR:
				myCellDefaultNumValue = -DBL_MAX;
				break;
			default:
				break;
		}
	else					// only if the default value is not empty
		switch (pFieldInfo->mLGType)
		{
			case LG_USER:
			case LG_CUSR:
				myCellDefaultStrValue = gUserName;
				break;
			case LG_DATE:
				if (!DateDBToCell(pFieldInfo->mDefaultStrValue, 
										myCellDefaultStrValue))
					return false;
				break;
			case LG_TIME:								
				if (!TimeDBToCell(pFieldInfo->mDefaultStrValue, 
										myCellDefaultStrValue, 
										pFieldInfo->mTimeZoneDif))
					return false;
				break;
			case LG_LSTU:
			case LG_CDAT:
				break;		// just ignore the default value
			case LG_DTIM:
				if (!DateTimeDBToCell(pFieldInfo->mDefaultStrValue, 
											myCellDefaultStrValue, 
											pFieldInfo->mTimeZoneDif))
					return false;
				break;
			case LG_WDAY:
				if (!WeekDBToCell(pFieldInfo->mDefaultStrValue, 
										myCellDefaultStrValue))
					return false;
				break;
			case LG_REAL:
			case LG_CURR:
				if (!NumberDBToCell(pFieldInfo->mDefaultStrValue, 
											myCellDefaultStrValue, 
											myCellDefaultNumValue, 
											NUM_CON_REAL_NUMS))
					return false;
				break;
			case LG_LONG:
			case LG_NMBR:
				if (!NumberDBToCell(pFieldInfo->mDefaultStrValue, 
											myCellDefaultStrValue, 
											myCellDefaultNumValue, 
											NUM_CON_LONG_NUMS))
					return false;
				break;
			case LG_BOOL: 
				if (!BoolDBToCell(pFieldInfo->mDefaultStrValue, 
										myCellDefaultStrValue))
					return false;
				break;
//h - start
			case LG_BOL1: 
				if (!Bol1DBToCell(pFieldInfo->mDefaultStrValue, 
										myCellDefaultStrValue))
					return false;
				break;
//h - end
			//*** 18.11.1999 SHA ***
			case LG_BOL2: 
				if (!Bol2DBToCell(pFieldInfo->mDefaultStrValue, 
										myCellDefaultStrValue))
					return false;
				break;

			case LG_SLST:
				if (!ComboDBToCell(pFieldInfo->mDefaultStrValue, 
											myCellDefaultStrValue, 
											pFieldInfo))
					return false;
				break;
			case LG_MLST:
				if (!MlstDBToCell(pFieldInfo->mDefaultStrValue, 
										myCellDefaultStrValue, 
										pFieldInfo->mMultichoiceAtt))
					return false;
				break;
			default:			// other types will be taken as are
				myCellDefaultStrValue = pFieldInfo->mDefaultStrValue;
				break;
		}

	pFieldInfo->mDefaultStrValue = myCellDefaultStrValue;
	if (!pFieldInfo->mIsString)
		pFieldInfo->mDefaultNumValue = myCellDefaultNumValue;

	return true;
}

//-----------------------------------------------------------------------
// GetChoiceList()
//	
//	The function loads the choice list for the particular field from the database
//-----------------------------------------------------------------------
bool CDBDPSDoc::GetChoiceList(CFieldInfo *pFieldInfo)
{
	CString Tables(pFieldInfo->mComboTabField.Left(SYS_FIELD_LENGTH[F_TANA] + EXT_LENGTH));
	CString ComboField(pFieldInfo->mComboTabField.Right(FINA_LENGTH));

	int ret = DBDPSCallCeda(UFIS_SELECT, Tables, NULL,
							 NULL, ComboField, NULL);
	if (ret)
	{
		CString OutputStr;
		OutputStr.Format(_T("GetChoiceList(). Error on reading %s. ") + gErrorMessage + POINT_CHAR, 
						(LPCTSTR)Tables);
		AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);
		return false;
	}

	HGLOBAL myHandle;
	GetBufferHandle(UFIS_BUF, &myHandle);
	LPTSTR DataPtr = (LPTSTR)GlobalLock(myHandle);		// data from the SYSTAB
	
	TCHAR Value[MAX_VALUE_LENGTH + 1];
	LPTSTR ValuePtr = Value;

	int nRows = GetNumberOfLines(UFIS_BUF);
	
	CTypedPtrArray<CPtrArray, CString*> myStrings;	
	for(int row = 0; row < nRows; row++)
	{
		ValuePtr = Value;		
		if (!GetToken(DataPtr, ValuePtr) && row != nRows - 1)	// GetToken finished earlier
		{
			AfxMessageBox("GetChoiceList(). CallCeda returned invalid data buffer.", MB_OK + MB_ICONSTOP);
			return false;
		}
		if (!_tcscmp(ValuePtr, DB_NULL_VALUE)) 
			continue;		// ignore Null value

		CString myTempValue(ValuePtr);		// conversion
		ConvertStringToClient(myTempValue);
		_tcscpy(ValuePtr, myTempValue);

		CString *myStr = new CString(ValuePtr);
		myStr->TrimLeft();
		myStr->TrimRight();
		if (myStr->IsEmpty())
			continue;

		if (pFieldInfo->mUseCollate)
		{			// to make an order using Collate sorting
			bool InsertIt = true;
			for (int i = 0; i < myStrings.GetSize(); i++)	
			{
				if (myStr->Collate(*myStrings.GetAt(i)) == 0)	// the same item - not to insert twice
				{
					InsertIt = false;
					break;
				}
				if (myStr->Collate(*myStrings.GetAt(i)) < 0) 
					break;
			}
			if (InsertIt)
				myStrings.InsertAt(i, myStr);
			else 
				delete myStr;
		}
		else
		{			// to make an order without Collate
			bool InsertIt = true;
			for (int i = 0; i < myStrings.GetSize(); i++)	
			{
				if (*myStr == *myStrings.GetAt(i)) // the same item - not to insert twice
				{
					InsertIt = false;
					break;
				}
				if (*myStr < *myStrings.GetAt(i)) 
					break;
			}
			if (InsertIt)
				myStrings.InsertAt(i, myStr);
			else 
				delete myStr;
		}
	}

	CString myChoiceList;
	for (int i = 0; i < myStrings.GetSize(); i++)
	{
		myChoiceList += *myStrings.GetAt(i) + LF_CHAR;	// build the choice list
		delete myStrings.GetAt(i);
	}
	if (!pFieldInfo->mRequired)
		myChoiceList = LF_CHAR + myChoiceList;	// add the empty string

	myStrings.RemoveAll();		// delete the temporary collection
	pFieldInfo->mChoiceList = myChoiceList;

	return true;
}

//-----------------------------------------------------------------------
// GetMultichoiceAtt()
//	
//	The function loads the multi choice attribute object for CheckListComboBox
//-----------------------------------------------------------------------
bool CDBDPSDoc::GetMultichoiceAtt
	(
		const CString &pComboTabField,
		CWDayCheckListComboBox::Attributes *&pMultichoiceAtt
	)
{
	int ret = DBDPSCallCeda(UFIS_SELECT, pComboTabField, SYS_FIELD[F_URNO],
							 NULL, MC_FIELDS, NULL);
	if (ret)
	{
		CString OutputStr;
		OutputStr.Format(_T("GetMultichoiceAtt(). Error on reading %s. ") + gErrorMessage + POINT_CHAR,
						(LPCTSTR)pComboTabField);
		AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);
		return false;
	}

	HGLOBAL myHandle;
	GetBufferHandle(UFIS_BUF, &myHandle);
	LPTSTR DataPtr = (LPTSTR)GlobalLock(myHandle);
	
	int nRows = GetNumberOfLines(UFIS_BUF);
	// real instances for the particular CheckListComboBox attributes
	CStringArray *myName = new CStringArray;		
	CStringArray *myChecked = new CStringArray;
	CStringArray *myUnchecked = new CStringArray;
	CUIntArray *myVirtKey = new CUIntArray;
	CString *mySeparator = new CString(SEPARATOR_CHAR);
	CString *myDefaultCode = new CString;
	
	myName->SetSize(nRows);
	myChecked->SetSize(nRows);
	myUnchecked->SetSize(nRows);
	myVirtKey->SetSize(nRows);

	TCHAR Value[MAX_VALUE_LENGTH + 1];
	LPTSTR ValuePtr = Value;

	for(int row = 0; row < nRows; row++)
	{
		for (int col = 0; col < NUMBER_OF_MCFIELDS; col++)
		{
			ValuePtr = Value;		
			if (!GetToken(DataPtr, ValuePtr) && row != nRows - 1)	// GetToken finished earlier
			{
				AfxMessageBox("GetMultichoiceAtt(). CallCeda returned invalid data buffer.", MB_OK + MB_ICONSTOP);
				return false;
			}
			if (!_tcscmp(ValuePtr, DB_NULL_VALUE)) 
				*ValuePtr = 0;	

			CString myTempValue(ValuePtr);		// conversion
			ConvertStringToClient(myTempValue);
			_tcscpy(ValuePtr, myTempValue);

			switch (col)		// fill the attributes by the obtained data
			{
				case 0:
					myName->ElementAt(row) = ValuePtr;
				break;
				case 1:
					myChecked->ElementAt(row) = ValuePtr;
				break;
				case 2:
					myUnchecked->ElementAt(row) = ValuePtr;
				break;
				case 3:
					if (*ValuePtr) ValuePtr[1] = 0;
					myVirtKey->ElementAt(row) = ValuePtr[0];
				break;
			}
		}
	}
	// create the object and assign it to the corresponding FiledInfo::mMultichoiceAtt
	pMultichoiceAtt = new CWDayCheckListComboBox::Attributes
	(
		nRows,
		*myName,
		*mySeparator,
		false,
		*myChecked,
		*myUnchecked,
		*myVirtKey,
		*myDefaultCode
	);
	return true;
}

//-----------------------------------------------------------------------
// ProcessBc
//	
//	The function processes broadcasts
//-----------------------------------------------------------------------
void CDBDPSDoc::ProcessBc
(
	int pDdxType,
	void * pDataPointer,
	CString &pInstanceName
)
{	
	afxDump << "!";
#ifdef ABBCCS_RELEASE
	struct BcStruct *myBcStruct;
	myBcStruct = (struct BcStruct *) pDataPointer;

	switch (pDdxType)
	{
		case DBDPS_BC_NEW:
			InsertRecordBc(myBcStruct->Fields, myBcStruct->Data);
			break;
		case DBDPS_BC_CHANGE:
			UpdateRecordBc(myBcStruct->Fields, myBcStruct->Data, myBcStruct->Selection);
			break;
		case DBDPS_BC_DELETE:
			DeleteRecordBc(myBcStruct->Selection);
			break;
		default:
			ASSERT(0);
			break;
	}
	POSITION pos = GetFirstViewPosition();
	if (pos)
	{
		CView* pView = GetNextView(pos);
		ASSERT(pView->IsKindOf(RUNTIME_CLASS(CDBDPSView)));
		((CDBDPSView*)pView)->Redraw();
	}
#endif
}

//-----------------------------------------------------------------------
// CheckDateTime()
//	
//	The function checks the validity of the date and/or time
//-----------------------------------------------------------------------
bool CDBDPSDoc::CheckDateTime
	(
		int pYear, 
		int pMonth, 
		int pDay, 
		int pHour, 
		int pMinute
	)
{
		if (pYear > MAX_YEAR || pYear < MIN_YEAR) 
			return false;
		if (pMonth > MAX_MONTH || pMonth < MIN_MONTH) 
			return false;
		if (pDay < MIN_DAY) 
			return false;
		switch (pMonth)
		{
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				if (pDay > MAX_DAY31) 
					return false;
				break;
			case 4:
			case 6:
			case 9:
			case 11:
				if (pDay > MAX_DAY30) 
					return false;
				break;
			case 2:
				if (!(pYear % 4) && (pYear % 100) || !(pYear % 400))
				{
					if (pDay > MAX_DAY29) 
						return false;
				}
				else if (pDay > MAX_DAY28) 
					return false;
				break;
			default:
				return false;
		}

	return true;
}

//-----------------------------------------------------------------------
// AddSecInfo()
//	
//	The function adds the object containing the security information into the
// security objects collection
//-----------------------------------------------------------------------
void CDBDPSDoc::AddSecInfo
	(
		CSecurityObjectCollection *pObjectCollection,
		CString pLabel,
		CString pType,
		CString pStatus,
		CString pControlId
	)
{
	CSecurityObject *myObject = new CSecurityObject;
	myObject->mLabel = pLabel;
	myObject->mType = pType;
	myObject->mStatus = pStatus;
	pObjectCollection->SetAt(pControlId, myObject);
}

//-----------------------------------------------------------------------
// LoadSecurityInfo()
//	
//	The function loads the objects containing the security information 
//-----------------------------------------------------------------------
//*** english ***
/*BOOL CDBDPSDoc::LoadSecurityInfo()
{
	CSecurityObjectCollection *myCollection = new CSecurityObjectCollection;

	// Object MAIN
	AddSecInfo(myCollection, _T("Open table"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("FILE_OPEN"));
	AddSecInfo(myCollection, _T("Export table"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("FILE_EXPORT"));
	AddSecInfo(myCollection, _T("Print table"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("FILE_PRINT"));
	AddSecInfo(myCollection, _T("Print preview of table"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("FILE_PRINT_PREVIEW"));
	AddSecInfo(myCollection, _T("Page settings"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("FILE_PAGE_SETUP"));
	AddSecInfo(myCollection, _T("Cut"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("EDIT_CUT"));
	AddSecInfo(myCollection, _T("Copy"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("EDIT_COPY"));
	AddSecInfo(myCollection, _T("Insert"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("EDIT_PASTE"));
	AddSecInfo(myCollection, _T("Insert row"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("EDIT_PASTE_ROW"));
	AddSecInfo(myCollection, _T("Insert region"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("EDIT_PASTE_REGION"));
	AddSecInfo(myCollection, _T("Insert row"), 
					SEC_TYPE_MI, SEC_STAT_ACT, _T("INSERT_ROW"));
	AddSecInfo(myCollection, _T("Change row"), 
					SEC_TYPE_MI, SEC_STAT_ACT, _T("UPDATE_ROW"));
	AddSecInfo(myCollection, _T("Delete row"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("DELETE_ROW"));
	AddSecInfo(myCollection, _T("Call filter and sort dialog"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("VIEW_FILTER_SORT_DLG"));
	AddSecInfo(myCollection, _T("Refresh sorting"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("VIEW_SORT_UPDATE"));
	gSecurityMap.SetAt(_T("MAIN"), myCollection);

	myCollection = new CSecurityObjectCollection;

	// Object VIEW_SORT_DLG
	AddSecInfo(myCollection, _T("Specify filter"),
					SEC_TYPE_EF, SEC_STAT_ACT, _T("SPECIFY_FILTER"));
	AddSecInfo(myCollection, _T("Specify sorting"),
					SEC_TYPE_LC, SEC_STAT_ACT, _T("SPECIFY_SORT"));
//	gSecurityMap.SetAt(_T("VIEW_SORT_DLG"), myCollection);
	gSecurityMap.SetAt(_T("VIEW_SORT_DLG"), myCollection);

	// Object LTNA from TABTAB
	CString myTableName;
	CString *myPLongTableName;
	CFieldsDesc *myFieldsDesc;
	for (int i = 0; i < gTables.GetSize(); i++)
	{
		myCollection = new CSecurityObjectCollection;
		myTableName = gTables.GetAt(i);
		if (!gLongTableNames.Lookup(myTableName + gTableExt, myPLongTableName))	// if a long name exists set this
		{
			AfxMessageBox("LoadSecurityInfo(). Long table name not found.", MB_OK + MB_ICONSTOP);
			return FALSE;
		}
		CString myLongTableName = CString(*myPLongTableName);

		AddSecInfo(myCollection, _T("Open table ") + myLongTableName + _T(" "),
						SEC_TYPE_MI, SEC_STAT_ACT, _T("FILE_OPEN"));
		AddSecInfo(myCollection, _T("Export table ") + myLongTableName + _T(" "),
							SEC_TYPE_MI, SEC_STAT_ACT, _T("FILE_EXPORT"));
		AddSecInfo(myCollection, _T("Print table ") + myLongTableName + _T(" "),
						SEC_TYPE_MI, SEC_STAT_ACT, _T("FILE_PRINT"));
		AddSecInfo(myCollection, _T("Insert row into table ") + myLongTableName + _T(" "),
						SEC_TYPE_MI, SEC_STAT_ACT, _T("INSERT_ROW"));
		AddSecInfo(myCollection, _T("Change row in table ") + myLongTableName + _T(" "),
						SEC_TYPE_MI, SEC_STAT_ACT, _T("UPDATE_ROW"));
		AddSecInfo(myCollection, _T("Delete row from table ") + myLongTableName + _T(" "),
						SEC_TYPE_MI, SEC_STAT_ACT, _T("DELETE_ROW"));
		AddSecInfo(myCollection, _T("Filter and sort dialog for table ") + myLongTableName,
						SEC_TYPE_MI, SEC_STAT_ACT, _T("VIEW_FILTER_SORT_DLG"));
		AddSecInfo(myCollection, _T("Refresh sorting for table ") + myLongTableName,
						SEC_TYPE_MI, SEC_STAT_ACT, _T("VIEW_SORT_UPDATE"));
		AddSecInfo(myCollection, _T("Specify filter for table ") + myLongTableName,
						SEC_TYPE_EF, SEC_STAT_ACT, _T("SPECIFY_FILTER"));
		AddSecInfo(myCollection, _T("Specify sorting for table ") + myLongTableName,
						SEC_TYPE_LC, SEC_STAT_ACT, _T("SPECIFY_SORT"));

		if (!gTablesDesc.Lookup(myTableName, myFieldsDesc)) // look for the table description
		{
			CString OutputStr;
			CString SysTable(SYSTAB + gTableExt);
			OutputStr.Format(_T("Table %s is not descripted in the %s."), 
									myTableName,
									SysTable);
			AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);
			return FALSE;
		} 
		for (int i = 0; i < myFieldsDesc->GetSize(); i++)	// get the fields names
			AddSecInfo(myCollection, _T("Access at field ") + myFieldsDesc->GetAt(i)->mLongHeader + _T(" in table ") + myLongTableName,
					SEC_TYPE_TC, SEC_STAT_ACT, myFieldsDesc->GetAt(i)->mFieldName);
		gSecurityMap.SetAt(myTableName + gTableExt, myCollection);
	}

	return TRUE;
}

/*
//*** GERMAN ***
BOOL CDBDPSDoc::LoadSecurityInfo()
{
	CSecurityObjectCollection *myCollection = new CSecurityObjectCollection;

	// Object MAIN
	AddSecInfo(myCollection, _T("Tabelle �ffnen"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("FILE_OPEN"));
	AddSecInfo(myCollection, _T("Tabelle exportieren"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("FILE_EXPORT"));
	AddSecInfo(myCollection, _T("Tabelle drucken"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("FILE_PRINT"));
	AddSecInfo(myCollection, _T("Tabelle Druckvorschau"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("FILE_PRINT_PREVIEW"));
	AddSecInfo(myCollection, _T("Seite einrichten"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("FILE_PAGE_SETUP"));
	AddSecInfo(myCollection, _T("Ausschneiden"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("EDIT_CUT"));
	AddSecInfo(myCollection, _T("Kopieren"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("EDIT_COPY"));
	AddSecInfo(myCollection, _T("Einf�gen"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("EDIT_PASTE"));
	AddSecInfo(myCollection, _T("Zeile Einf�gen"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("EDIT_PASTE_ROW"));
	AddSecInfo(myCollection, _T("Region Einf�gen"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("EDIT_PASTE_REGION"));
	AddSecInfo(myCollection, _T("Zeile einf�gen"), 
					SEC_TYPE_MI, SEC_STAT_ACT, _T("INSERT_ROW"));
	AddSecInfo(myCollection, _T("Zeile �ndern"), 
					SEC_TYPE_MI, SEC_STAT_ACT, _T("UPDATE_ROW"));
	AddSecInfo(myCollection, _T("Zeile l�schen"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("DELETE_ROW"));
	AddSecInfo(myCollection, _T("Aufrufen Anzeige-Einschr�nken-Dialog"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("VIEW_FILTER_SORT_DLG"));
	AddSecInfo(myCollection, _T("Aktualisieren Sortierung"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("VIEW_SORT_UPDATE"));
	gSecurityMap.SetAt(_T("MAIN"), myCollection);

	myCollection = new CSecurityObjectCollection;

	// Object VIEW_SORT_DLG
	AddSecInfo(myCollection, _T("Einschr�nkungen f�r Anzeige spezifizieren"),
					SEC_TYPE_EF, SEC_STAT_ACT, _T("SPECIFY_FILTER"));
	AddSecInfo(myCollection, _T("Sortierkriterien f�r Anzeige spezifizieren"),
					SEC_TYPE_LC, SEC_STAT_ACT, _T("SPECIFY_SORT"));
//	gSecurityMap.SetAt(_T("VIEW_SORT_DLG"), myCollection);
	gSecurityMap.SetAt(_T("VIEW_SORT_DLG"), myCollection);

	// Object LTNA from TABTAB
	CString myTableName;
	CString *myPLongTableName;
	CFieldsDesc *myFieldsDesc;
	for (int i = 0; i < gTables.GetSize(); i++)
	{
		myCollection = new CSecurityObjectCollection;
		myTableName = gTables.GetAt(i);
		if (!gLongTableNames.Lookup(myTableName + gTableExt, myPLongTableName))	// if a long name exists set this
		{
			AfxMessageBox("LoadSecurityInfo(). Long table name not found.", MB_OK + MB_ICONSTOP);
			return FALSE;
		}
		CString myLongTableName = CString(*myPLongTableName);

		AddSecInfo(myCollection, _T("Tabelle ") + myLongTableName + _T(" �ffnen"),
						SEC_TYPE_MI, SEC_STAT_ACT, _T("FILE_OPEN"));
		AddSecInfo(myCollection, _T("Tabelle ") + myLongTableName + _T(" exportieren"),
							SEC_TYPE_MI, SEC_STAT_ACT, _T("FILE_EXPORT"));
		AddSecInfo(myCollection, _T("Tabelle ") + myLongTableName + _T(" drucken"),
						SEC_TYPE_MI, SEC_STAT_ACT, _T("FILE_PRINT"));
		AddSecInfo(myCollection, _T("Zeile in die Tabelle ") + myLongTableName + _T(" einf�gen"),
						SEC_TYPE_MI, SEC_STAT_ACT, _T("INSERT_ROW"));
		AddSecInfo(myCollection, _T("Zeile in der Tabelle ") + myLongTableName + _T(" �ndern"),
						SEC_TYPE_MI, SEC_STAT_ACT, _T("UPDATE_ROW"));
		AddSecInfo(myCollection, _T("Zeile aus der Tabelle ") + myLongTableName + _T(" l�schen"),
						SEC_TYPE_MI, SEC_STAT_ACT, _T("DELETE_ROW"));
		AddSecInfo(myCollection, _T("Aufrufen Anzeige-Einschr�nken-Dialog f�r Tabelle ") + myLongTableName,
						SEC_TYPE_MI, SEC_STAT_ACT, _T("VIEW_FILTER_SORT_DLG"));
		AddSecInfo(myCollection, _T("Aktualisieren Sortierung f�r Tabelle ") + myLongTableName,
						SEC_TYPE_MI, SEC_STAT_ACT, _T("VIEW_SORT_UPDATE"));
		AddSecInfo(myCollection, _T("Einschr�nkungen f�r Anzeige spezifizieren f�r Tabelle ") + myLongTableName,
						SEC_TYPE_EF, SEC_STAT_ACT, _T("SPECIFY_FILTER"));
		AddSecInfo(myCollection, _T("Sortierkriterien f�r Anzeige spezifizieren f�r Tabelle ") + myLongTableName,
						SEC_TYPE_LC, SEC_STAT_ACT, _T("SPECIFY_SORT"));

		if (!gTablesDesc.Lookup(myTableName, myFieldsDesc)) // look for the table description
		{
			CString OutputStr;
			CString SysTable(SYSTAB + gTableExt);
			OutputStr.Format(_T("Table %s is not descripted in the %s."), 
									myTableName,
									SysTable);
			AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);
			return FALSE;
		} 
		for (int i = 0; i < myFieldsDesc->GetSize(); i++)	// get the fields names
			AddSecInfo(myCollection, _T("Zugriff auf Field ") + myFieldsDesc->GetAt(i)->mLongHeader + _T(" der Tabelle ") + myLongTableName,
					SEC_TYPE_TC, SEC_STAT_ACT, myFieldsDesc->GetAt(i)->mFieldName);
		gSecurityMap.SetAt(myTableName + gTableExt, myCollection);
	}

	return TRUE;
}
*/

//*** ITALIAN ***
BOOL CDBDPSDoc::LoadSecurityInfo()
{
	CSecurityObjectCollection *myCollection = new CSecurityObjectCollection;

	// Object MAIN
	AddSecInfo(myCollection, _T("Apri"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("FILE_OPEN"));
	AddSecInfo(myCollection, _T("Esporta"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("FILE_EXPORT"));
	AddSecInfo(myCollection, _T("Stampa"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("FILE_PRINT"));
	AddSecInfo(myCollection, _T("Anteprima di stampa"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("FILE_PRINT_PREVIEW"));
	AddSecInfo(myCollection, _T("Imposta stampante"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("FILE_PAGE_SETUP"));
	AddSecInfo(myCollection, _T("Taglia"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("EDIT_CUT"));
	AddSecInfo(myCollection, _T("Copia"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("EDIT_COPY"));
	AddSecInfo(myCollection, _T("Incolla"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("EDIT_PASTE"));
	AddSecInfo(myCollection, _T("Incolla riga"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("EDIT_PASTE_ROW"));
	AddSecInfo(myCollection, _T("Incolla regione"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("EDIT_PASTE_REGION"));
	AddSecInfo(myCollection, _T("Inserisci riga"), 
					SEC_TYPE_MI, SEC_STAT_ACT, _T("INSERT_ROW"));
	AddSecInfo(myCollection, _T("Aggiorna riga"), 
					SEC_TYPE_MI, SEC_STAT_ACT, _T("UPDATE_ROW"));
	AddSecInfo(myCollection, _T("Cancella riga"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("DELETE_ROW"));
	AddSecInfo(myCollection, _T("Maschera Filtro/Ordinamento"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("VIEW_FILTER_SORT_DLG"));
	AddSecInfo(myCollection, _T("Aggiorna �l'ordinamento dei record"),
					SEC_TYPE_MI, SEC_STAT_ACT, _T("VIEW_SORT_UPDATE"));
	gSecurityMap.SetAt(_T("MAIN"), myCollection);

	myCollection = new CSecurityObjectCollection;

	// Object VIEW_SORT_DLG
	AddSecInfo(myCollection, _T("Imposta un filtro"),
					SEC_TYPE_EF, SEC_STAT_ACT, _T("SPECIFY_FILTER"));
	AddSecInfo(myCollection, _T("Imposta un ordinamento"),
					SEC_TYPE_LC, SEC_STAT_ACT, _T("SPECIFY_SORT"));
//	gSecurityMap.SetAt(_T("VIEW_SORT_DLG"), myCollection);
	gSecurityMap.SetAt(_T("VIEW_SORT_DLG"), myCollection);

	// Object LTNA from TABTAB
	CString myTableName;
	CString *myPLongTableName;
	CFieldsDesc *myFieldsDesc;
	for (int i = 0; i < gTables.GetSize(); i++)
	{
		myCollection = new CSecurityObjectCollection;
		myTableName = gTables.GetAt(i);
		if (!gLongTableNames.Lookup(myTableName + gTableExt, myPLongTableName))	// if a long name exists set this
		{
			AfxMessageBox("LoadSecurityInfo(). Long table name not found.", MB_OK + MB_ICONSTOP);
			return FALSE;
		}
		CString myLongTableName = CString(*myPLongTableName);

		AddSecInfo(myCollection, _T("Apri Tabella ") + myLongTableName ,
						SEC_TYPE_MI, SEC_STAT_ACT, _T("FILE_OPEN"));
		AddSecInfo(myCollection, _T("Esporta Tabella ") + myLongTableName ,
							SEC_TYPE_MI, SEC_STAT_ACT, _T("FILE_EXPORT"));
		AddSecInfo(myCollection,  _T("Stampa Tabella ") + myLongTableName,
						SEC_TYPE_MI, SEC_STAT_ACT, _T("FILE_PRINT"));
		AddSecInfo(myCollection, _T("Inserisci riga nella tabella ") + myLongTableName ,
						SEC_TYPE_MI, SEC_STAT_ACT, _T("INSERT_ROW"));
		AddSecInfo(myCollection, _T("Aggiorna riga nella tabella ")+ myLongTableName,
						SEC_TYPE_MI, SEC_STAT_ACT, _T("UPDATE_ROW"));
		AddSecInfo(myCollection, _T("Cancella riga dalla tabella ") + myLongTableName,
						SEC_TYPE_MI, SEC_STAT_ACT, _T("DELETE_ROW"));
		AddSecInfo(myCollection, _T("Apri la maschera di Filtro/Ordinamento per la tabella ") + myLongTableName,
						SEC_TYPE_MI, SEC_STAT_ACT, _T("VIEW_FILTER_SORT_DLG"));
		AddSecInfo(myCollection, _T("Aggiorna l'ordinamento dei record nella tabella ") + myLongTableName,
						SEC_TYPE_MI, SEC_STAT_ACT, _T("VIEW_SORT_UPDATE"));
		AddSecInfo(myCollection, _T("Imposta un filtro per la tabella ") + myLongTableName,
						SEC_TYPE_EF, SEC_STAT_ACT, _T("SPECIFY_FILTER"));
		AddSecInfo(myCollection, _T("Imposta un ordinamento per la tabella ") + myLongTableName,
						SEC_TYPE_LC, SEC_STAT_ACT, _T("SPECIFY_SORT"));

		if (!gTablesDesc.Lookup(myTableName, myFieldsDesc)) // look for the table description
		{
			CString OutputStr;
			CString SysTable(SYSTAB + gTableExt);
			OutputStr.Format(_T("Table %s is not descripted in the %s."), 
									myTableName,
									SysTable);
			AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);
			return FALSE;
		} 
		for (int i = 0; i < myFieldsDesc->GetSize(); i++)	// get the fields names
			AddSecInfo(myCollection, _T("Campo ") + myFieldsDesc->GetAt(i)->mLongHeader + _T(" della tabella ") +myLongTableName,
					SEC_TYPE_TC, SEC_STAT_ACT, myFieldsDesc->GetAt(i)->mFieldName);
		gSecurityMap.SetAt(myTableName + gTableExt, myCollection);
	}

	return TRUE;
}

//-----------------------------------------------------------------------
// ClearSecurityInfo()
//	
//	The function clears the objects containing the security information 
//-----------------------------------------------------------------------
void CDBDPSDoc::ClearSecurityInfo()
{
	POSITION pos;
	CString myKey;
	CSecurityObjectCollection *mySOC;
	POSITION pos2;
	CString myKey2;
	CSecurityObject *mySO;

	for (pos = gSecurityMap.GetStartPosition(); pos != NULL;)
	{
		gSecurityMap.GetNextAssoc(pos, myKey, mySOC);

		for (pos2 = mySOC->GetStartPosition(); pos2 != NULL;)
		{
			mySOC->GetNextAssoc(pos2, myKey2, mySO);
			delete mySO;
		}
		mySOC->RemoveAll();
		delete mySOC;
	}
	gSecurityMap.RemoveAll();				
}

//-----------------------------------------------------------------------
// RegisterApp()
//	
//	The function makes the registration of the application
//-----------------------------------------------------------------------
BOOL CDBDPSDoc::RegisterApp()
{
	POSITION pos;
	CString myKey;
	CSecurityObjectCollection *mySOC;
	POSITION pos2;
	CString myKey2;
	CSecurityObject *mySO;
	CString myRegisterText;

	for (pos = gSecurityMap.GetStartPosition(); pos != NULL;)
	{
		gSecurityMap.GetNextAssoc(pos, myKey, mySOC);

		for (pos2 = mySOC->GetStartPosition(); pos2 != NULL;)
		{
			mySOC->GetNextAssoc(pos2, myKey2, mySO);

			CString tmpObjektId = myKey;
			ConvertStringToServer(tmpObjektId);

			CString tmpControlId = myKey2;
			ConvertStringToServer(tmpControlId);

			CString tmpLabel = mySO->mLabel;
			ConvertStringToServer(tmpLabel);

			//*** 19.08.99 SHA ***
			//*** Problem: FIELD IN FTKTAB TOO SHORT -> CUT ! ***
			if (tmpLabel.GetLength()> 80)
			{
				//afxDump << "tmpLabel:" << tmpLabel <<"\n";
				tmpLabel = tmpLabel.Left(80);
			}

			CString tmpType = mySO->mType;
			ConvertStringToServer(tmpType);

			myRegisterText += tmpObjektId + DELIMITER_CHAR +
//									myKey2 + DELIMITER_CHAR +
									tmpObjektId + SECURITY_SEPARATOR + tmpControlId + DELIMITER_CHAR +
									tmpLabel + DELIMITER_CHAR +
//									_T("TEST") + DELIMITER_CHAR +
									tmpType + DELIMITER_CHAR +
									_T("1") + DELIMITER_CHAR;
		}
	}
	if(!myRegisterText.IsEmpty())	// cut last ','
		myRegisterText = myRegisterText.Left(myRegisterText.GetLength() - ((CString) DELIMITER_CHAR).GetLength());
	myRegisterText = CString(APP_NAME) + ",InitModu,InitModu,Initialize (InitModu),B,-," + myRegisterText;
	if(!RegisterModules(myRegisterText))
		return FALSE;

	return TRUE;
}

//-----------------------------------------------------------------------
// RegisterModules()
//	
//	The function makes the registration of the application modules
//-----------------------------------------------------------------------
BOOL CDBDPSDoc::RegisterModules
	(
		LPCTSTR pRegisterText
	)
{
	int ret = DBDPSCallCeda
					(
						UFIS_REG,
						gTableExt,
						NULL,
						NULL,
						_T("APPL,SUBD,FUNC,FUAL,TYPE,STAT"),
						pRegisterText
					);
	if (ret)
	{
		AfxMessageBox("RegisterModules(). Error on CallCeda. " + gErrorMessage + POINT_CHAR,
						MB_OK + MB_ICONSTOP);
		return FALSE;
	}

	return TRUE;
}

//-----------------------------------------------------------------------
// SetDefaultPriority
//	
//	The function sets default priority of the security modules
//-----------------------------------------------------------------------
void CDBDPSDoc::SetDefaultPriority()
{
	POSITION pos;
	CString myKey;
	CSecurityObjectCollection *mySOC;
	POSITION pos2;
	CString myKey2;
	CSecurityObject *mySO;

	for (pos = gSecurityMap.GetStartPosition(); pos != NULL;)
	{
		gSecurityMap.GetNextAssoc(pos, myKey, mySOC);

		for (pos2 = mySOC->GetStartPosition(); pos2 != NULL;)
		{
			mySOC->GetNextAssoc(pos2, myKey2, mySO);
			mySO->mStatus = SEC_STAT_DEF;
		}
	}
} 

//-----------------------------------------------------------------------
// SetSecurityStatus
//	
//	The function sets default priority of the security modules
//-----------------------------------------------------------------------
void CDBDPSDoc::SetSecurityStatus
	(
		LPCTSTR pObjectId,
		LPCTSTR pControlId,
		LPCTSTR pStatus
	)
{
	CSecurityObjectCollection *mySOC;
	CSecurityObject *mySO;

	if(gSecurityMap.Lookup(pObjectId, mySOC))
		if(mySOC->Lookup(pControlId, mySO))
			mySO->mStatus = pStatus;
}




//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//*** 15.11.99 SHA ***

bool CDBDPSDoc::CreateLogFile(CString olFile)
{
	
	COleDateTime dt = COleDateTime::GetCurrentTime();
	CString olTime;
	olTime.Format(_T("%04d-%02d-%02d (%02d:%02d)"),
						dt.GetYear(),
						dt.GetMonth(),
						dt.GetDay(),
						dt.GetHour(),
						dt.GetMinute(), 0);

	outFileOK = FALSE;

	if (outFile.Open((olFile),CFile::modeCreate|CFile::modeWrite))
	{
		outFile.WriteString("-------------------------------\n");
		outFile.WriteString("DBDPS - CHECK OF SYSTAB ENTRIES\n");
		outFile.WriteString("-------------------------------\n");
		outFile.WriteString("Started at: " + olTime + "\n");
		outFile.WriteString("-------------------------------\n");		
		outFileOK = TRUE;
	}

	return TRUE;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
bool CDBDPSDoc::CloseLogFile()
{
	
	if (outFileOK)
	{
		COleDateTime dt = COleDateTime::GetCurrentTime();
		CString olTime;
		olTime.Format(_T("%04d-%02d-%02d (%02d:%02d)"),
							dt.GetYear(),
							dt.GetMonth(),
							dt.GetDay(),
							dt.GetHour(),
							dt.GetMinute(), 0);

			outFile.WriteString("-------------------------------\n");
			outFile.WriteString("Stopped at: " + olTime + "\n");		
			outFile.WriteString("-------------------------------\n");
			outFile.Close();
	}

	return TRUE;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
bool CDBDPSDoc::WriteLogFile(CString olText)
{
	if (outFileOK)
	{
			outFile.WriteString(olText + "\n");		
	}

	return TRUE;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//*** 02.12.1999 SHA ***
CString CDBDPSDoc::GetTokenFromString(CString Delimiter, long TokenNo, CString &FromString)
{
	CString olBack="";

	return olBack;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//*** 02.12.1999 SHA ***
void CDBDPSDoc::ConvertToArray(CString &olIn, 
							   CStringArray &olOut, 
							   CString Delimiter, 
							   bool &Valid)
{
	//*** 02.12.1999 SHA ***
	//*** CONVERT DELIMITED STRING TO ARRAY ***

	int ilPos1,ilPos2;
	CString olDmy;

	ilPos2 = olIn.Find(Delimiter,0);
	ilPos1=0;

	if (ilPos2==-1)
	{
		olOut.RemoveAll();
	}
	else
	{
		while (ilPos2!=-1)
		{
			if (ilPos1==0)
				ilPos1=-1;
			CString olDmy=olIn.Mid(ilPos1+1,(ilPos2-ilPos1-1));
			ilPos1 = ilPos2;
			ilPos2 = olIn.Find(Delimiter,ilPos2+1);
			
			if (olDmy!="VAFR" && olDmy!="VATO")
				olOut.Add(olDmy);
			else
				Valid=true;
			
		}
	}
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//*** 02.12.1999 SHA ***
CString CDBDPSDoc::GetValue(CString &Fields, CString &Values, CString Delimiter, CString FieldName)
{
	CString back="";
	int ilPos1,ilPos2,ilPosValue=0,ilFind=0;
	CString olDmy;

	ilPos2 = Fields.Find(Delimiter,0);
	ilPos1=-1;

	while (ilPos2!=-1)
	{
		CString olDmy=Fields.Mid(ilPos1+1,(ilPos2-ilPos1-1));
		ilPos1 = ilPos2;
		ilPos2 = Fields.Find(Delimiter,ilPos2+1);
		
		if (olDmy!=FieldName)
		{
			ilPosValue++;
		}
		else
		{
			ilFind=ilPosValue;
			break;
		}
		
	}

	ilPosValue=0;
	ilPos2 = Values.Find(Delimiter,0);
	ilPos1=-1;
	
	while (ilPos2!=-1)
	{
		CString olDmy=Values.Mid(ilPos1+1,(ilPos2-ilPos1-1));
		ilPos1 = ilPos2;
		ilPos2 = Values.Find(Delimiter,ilPos2+1);
		
		ilPosValue++;
		if (ilPosValue==ilFind+1)
		{
			back = olDmy;
			break;
		}
		
	}


	return back;
}
