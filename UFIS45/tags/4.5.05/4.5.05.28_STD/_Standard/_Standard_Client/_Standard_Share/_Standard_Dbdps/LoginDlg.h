#if !defined(AFX_LOGINDLG_H__73F7E3C3_249C_11D3_A2E7_00105A663F96__INCLUDED_)
#define AFX_LOGINDLG_H__73F7E3C3_249C_11D3_A2E7_00105A663F96__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// LoginDlg.h : header file
//

//*** 17.11.99 SHA ***
#include <CCSEdit.h>

/////////////////////////////////////////////////////////////////////////////
// CLoginDlg dialog

class CLoginDlg : public CDialog
{
// Construction
public:
	CLoginDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLoginDlg)
	enum { IDD = IDD_DIALOG_LOGIN };
	CString	m_EditUserName;
	CString	m_EditPassword;
	//}}AFX_DATA

	//*** 17.11.99 SHA ***
	CCSEdit	m_UsernameCtrl;
	CCSEdit	m_PasswordCtrl;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLoginDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
private:
	int m_LoginCount;
	int Login
		(
			LPCTSTR pUserName,
			LPCTSTR pPassword
		);

protected:

	// Generated message map functions
	//{{AFX_MSG(CLoginDlg)
	afx_msg void OnMyOk();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOGINDLG_H__73F7E3C3_249C_11D3_A2E7_00105A663F96__INCLUDED_)
