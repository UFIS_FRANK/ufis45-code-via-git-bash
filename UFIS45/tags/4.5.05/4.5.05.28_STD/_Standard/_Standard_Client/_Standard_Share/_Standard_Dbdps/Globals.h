//***********************************************************************
// Globals.h
//***********************************************************************
#ifndef __GLOBALS_H__
#define __GLOBALS_H__
#pragma once

#define ABBCCS_RELEASE 

#include <afxtempl.h>
#include <afxcoll.h>
#include <afxwin.h>
#include <float.h>
#include <WDayChk.h>
#include <CopPast.h>

#ifdef ABBCCS_RELEASE
#include <CCSLog.h>
#include <CCSDdx.h>
#include <CCSCedaCom.h>
#include <CCSBcHandle.h>
#endif

//*** 02.12.1999 SHA ***
#include <CedaBasicData.h>

#define VERSION_DATE		"04/10/2000"
#define VERSION_NO			"1.2.3.6"

// CallCeda return values
#define	RC_SUCCESS			0     // Everything OK
#define	RC_FAIL				-1    // General serious error
#define	RC_COMM_FAIL		-2	   // WINSOCK.DLL error
#define	RC_INIT_FAIL		-3    // Open the log file
#define	RC_CEDA_FAIL		-4    // CEDA reports an error
#define	RC_SHUTDOWN			-5    // CEDA reports shutdown
#define	RC_ALREADY_INIT	-6		// InitComm called up for a second line
#define	RC_NOT_FOUND		-7    // Data not found (More than 7 applications are running)
#define	RC_DATA_CUT			-8    // Data incomplete
#define	RC_TIMEOUT			-9    // CEDA reports a time-out

// database and UFIS chars
#define OUTER_JOIN_STRING	_T(" (+) ")	// the Oracle join operator
#define EQ_CHAR				_T('=')	// the equality operator
#define NEQ_STRING			_T("<>")	// the non equality operator
#define COLON_CHAR			_T(':')	// the colon
#define POINT_CHAR			_T('.')	// the point
#define DELIMITER_CHAR		_T(',')	// the delimiter
#define MLST_DELIMITER		_T(':')	// the MLST delimiter
#define APOSTROPHE_CHAR		_T('\'')	// the apostrophe
#define BLANK_CHAR			_T(' ')	// the blank
#define BLANK_STRING			_T(" ")	// the blank string
#define AND_STRING			_T(" AND ")	// the AND string
#define OR_STRING				_T(" OR ")	// the OR string
#define LBR_STRING			_T("(")	// the "(" string
#define RBR_STRING			_T(")")	// the ")" string
#define WHERE_STRING			_T("WHERE ")	// the "WHERE" keyword

//h
#ifdef ABBCCS_RELEASE
#define WILD_ALL_CHAR		_T("%")		// wild character for Oracle
#else
#define WILD_ALL_CHAR		_T("*")		// wild character for Access
#endif
//h

#define DBDPS_STRING			_T("'DBDPS'")

#define LF_CHAR				_T('\n')	// the newline character
#define CR_CHAR				_T('\r')	// the carriage return  character
#define DB_TRUE_VALUE		_T('Y')	// simply the 'Y' in the database
#define DB_FALSE_VALUE		_T('N')	// simply the 'N' in the database
#define DB_NO_DATA_FOUND	_T("no data found")	// no data found message

#define DB_NULL_VALUE		_T(" ")		// the Null representation for UFIS
//#define DB_NULL_VALUE		_T("Null")	// the Null representation for UFIS

#define MAX_COLUMNS			1000			// the max. number of columns

#define DATE_TIME_WIDTH		15			// the particular width constant for the date time control
#define TIME_WIDTH			7			// the particular width constant for the time control
#define BOOL_WIDTH			3			// the particular width constant for the time control
#define DATE_WIDTH			13			// the particular width constant for the date control
#define WDAY_WIDTH			9			// the particular width constant for the week day control
#define WIDTH_CONST			9			// the width constant is to determine the column width
#define MAX_COL_WIDTH		200		// the max width for the column
#define MIN_COL_WIDTH		30			// the min width for the column

#define MAX_NUMBER_LENGTH	13			// the max length of the number value
#define DATE_TIME_LENGTH	14			// the length of the date and time value
#define MAX_WDAY_LENGTH		7			// the length of the wday value
#define BOOL_LENGTH			1			// the length of the bool value

#define MAX_VALUE_LENGTH	2000		// the max length of the field value
#define URNO_LENGTH			10			// the max DB length of the URNO field
#define TANA_LENGTH			3			// the DB length of the TANA field
#define EXT_LENGTH			3			// the length of the table extension
#define FINA_LENGTH			4			// the DB length of the FINA field

#define MAX_URNOS				500			// the max number of flushed URNOs
#define MAX_URNOS_STRING	_T("500")	// the string representing the MAX_URNOS

// date and time 
#define MIN_TIME_DIF			0				// the min value for time zone difference 
#define MAX_TIME_DIF			(24*60 - 1)	// the max value for time zone difference
#define STR_TIME_BEGIN		8			// begin position for the time string
#define STR_TIME_END			12			// end position for the time string
#define STR_DATE_BEGIN		0			// begin position for the date string
#define STR_DATE_END			8			// end position for the date string
#define CURRENT_DATE_TIME	_T("CURRENT")	// current date and/or time (special default value)

#define MAX_DATE				_T("31.12.2399")
#define MIN_DATE				_T("01.01.1400")
#define MAX_YEAR				2399
#define MIN_YEAR				1400		
#define MAX_MONTH				12
#define MIN_MONTH				1
#define MAX_DAY31				31	
#define MAX_DAY30				30	
#define MAX_DAY29				29	
#define MAX_DAY28				28	
#define MIN_DAY				1
#define MAX_HOUR				23	
#define MIN_HOUR				0
#define MAX_MINUTE			59
#define MIN_MINUTE			0

// weekday
#define NUM_WEEKDAYS			7			// the number of week days
#define EMPTY_WEEK			_T("-------")	// the default value of WDAY type
#define EMPTY_DAY				_T("-")		// the empty day value 

//CheckListComboBox
#define SEPARATOR_CHAR		_T(',')		// the separator
#define MC_FIELDS				_T("NAME,CHCK,UNCH,VKEY") // CheckListComboBox fields
#define NUMBER_OF_MCFIELDS	4				// the number of CheckListComboBox fields in the table

// number conversion
#define	NUM_CON_BLANK1				_T(' ')
#define	NUM_CON_BLANK2				_T('\t')
#define	NUM_CON_NULL_NUMERAL		_T('0')
#define	NUM_CON_MIN_NUMERAL		_T('0')
#define	NUM_CON_MAX_NUMERAL		_T('9')
#define	NUM_CON_PLUS				_T('+')
#define	NUM_CON_MINUS				_T('-')
#define	NUM_CON_POINT				_T('.')
#define	NUM_CON_COMMA				_T(',')
#define	NUM_CON_END_OF_STRING	0
#define	NUM_CON_REAL_NUMS			2
#define	NUM_CON_LONG_NUMS			0

// UFIS commands
#define UFIS_INSERT			_T("IRT")		// insert command
#define UFIS_SELECT			_T("RTA")		// select command
#define UFIS_UPDATE			_T("URT")		// update command
#define UFIS_DELETE			_T("DRT")		// delete command
#define UFIS_BUF				_T("BUF1")		// the Ufis buffer 
#define UFIS_GMU				_T("GMU")		// the GMU command
#define UFIS_REG				_T("SMI")		// the SMI command
#define UFIS_GPR				_T("GPR")		// the GPR command

#define SYSTAB					_T("SYS")	// the SYSTAB table name without extension
#define APTTAB					_T("APT")	// the APTTAB table name without extension
#define TABTAB					_T("TAB")	// the TABTAB table name without extension

// constants for mCellState
#define CELL_STATE_NOT_USED	0	// the state is not used
#define CELL_GR_INVALID		0x1	// the value coming from grid is invalid
#define CELL_DB_INVALID		0x2	// the value coming from database is invalid
#define CELL_UPDATED			0x8	//	the cell was currently updated

enum ESysField			// the list of SYSTAB field indexes
{
	F_URNO,
	F_TANA,					
	F_FTAN,					
	F_FINA,
	F_FITY,
	F_TATY,
	F_FELE,
	F_ADDI,
	F_TYPE,
	F_TTYP,
//	F_TZON,	// systab, but not currently used 
//	F_APC3,	// apttab
//	F_TICH,	// apttab
//	F_TDI1,	// apttab
//	F_TDI2,	// apttab
	F_REFE,
	F_CLSR,
	F_LABL,
	F_REQF,
	F_TOLT,
	F_SORT, 
	F_DEFV, 
	F_EL__		// the ending label determines the number of elements
};
#define NUM_SYSFIELDS	((int)F_EL__)		// the number of SYSTAB fields

enum ETabField			// the list of TABTAB field indexes
{
	TAB_URNO,					
	TAB_LTNA,					
	TAB_LNAM,
	//*** 02.12.1999 SHA ***
	TAB_DVIW,
	TAB_EL__		// the ending label determines the number of elements
};
#define NUM_TABFIELDS	((int)TAB_EL__)		// the number of TABTAB fields

// descriptive
enum EBoolType			// the boolean type of the field
{
	BOOL_TRUE,			// the TRUE value
	BOOL_FALSE,			// the FALSE value
	BOOL_NULL,			// the NULL value
	BOOL_EL__			// the ending label
};
#define NUM_BOOLTYPES	((int)BOOL_EL__)		// the number of boolean types
//h - start
enum EBol1Type			// the bol1 type of the field
{
	BOL1_TRUE,			// the TRUE value
	BOL1_FALSE,			// the FALSE value
	BOL1_NULL,			// the NULL value
	BOL1_EL__			// the ending label
};
#define NUM_BOL1TYPES	((int)BOL1_EL__)		// the number of bol1 types
//h - end
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//*** 17.11.99 SHA ***
enum EBol2Type			// the bol2 type of the field
{
	BOL2_TRUE,			// the TRUE value
	BOL2_FALSE,			// the FALSE value
	BOL2_NULL,			// the NULL value
	BOL2_EL__			// the ending label
};
#define NUM_BOL2TYPES	((int)BOL2_EL__)		// the number of bol1 types
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

enum EDBType			// the database type of the field
{
	DB_CHAR,				// the CHAR type
	DB_VARCHAR2,		// the VARCHAR2 type
	DB_NUMBER,			// the NUMBER type
	DB_EL__				// the ending label
};
#define NUM_DBTYPES	((int)DB_EL__)		// the number of database types

enum ELGType			// the logical type of the field
{
	LG_TRIM,				// @ the "String" type (both sides trimmed)
    LG_UPPC,				//*** 31.08.99 SHA ***
							//*** CONVERT TO UPPERCASE ***
	LG_USER,				// like TRIM but read only
	LG_CUSR,				// like TRIM but read only

	LG_CHAR,				// @ the "String" type (right side trimmed)

	LG_DATE,				// @ the "Date" type
	LG_TIME,				// @ the "Time" type				
	
	LG_WDAY,				// @ the "Week day" type
	
	LG_REAL,				// @ the "Currency and Floating point" type
	LG_CURR,				// like REAL
	
	LG_LONG,				// @ the "Number" type
	LG_NMBR,				// like LONG

	LG_BOOL,				// @ "Y" or "N" - boolean
//h - start
	LG_BOL1,				// @ "1" or "0" - boolean
//h - end
//*** 18.11.1999 SHA ***
	LG_BOL2,				// @ "X" or " " - boolean

	LG_URNO,				// @ one only the number as DB type

	LG_FKEY,				// like LONG but this time ignored
	LG_FURN,				// like LONG but this time ignored
	LG_FREL,				// like LONG but this time ignored

	LG_LSTU,				// @ the complete date and time but read only 
	LG_CDAT,				// @ the complete date and time but read only 
	LG_DTIM,				// @ the complete date and time 

	LG_SLST,				// @ the ComboBox consisting of the TRIM strings
	LG_MLST,				// @ the CheckListComboBox consisting of the TRIM strings

	LG_EL__				// ending label
};
#define NUM_LGTYPES	((int)LG_EL__)		// the number of logical types

enum ETimeType			// the time type
{
	TIME_UTC,			// the UTC time
	TIME_LOCAL,			// the local client time (the time zone offset needs to be added)
	// TIME_ZONE,			// the time of the determined zone (the time zone offset needs to be added)
	TIME_NOT_USED,		// not used
	TIME_EL__			// ending label
};
#define NUM_TIMETYPES	((int)TIME_EL__)		// the number of logical types

// style colors
enum ECLType			// the color type
{
	CL_RED,				// red
	CL_LIGHT_RED,		// light red
	CL_GREEN,			// green
	CL_YELLOW,			// yellow
	CL_STANDART,		// typical
	CL_EL__				// ending label
};

typedef CTypedPtrArray<CPtrArray, int*> CIndexMap;	// the index map
typedef CTypedPtrArray<CPtrArray, int*> CUrnoMap;	// the URNO map
// end data

// sort
enum ESortInfo			// the information how to perform the sort
{
	STRCMP_A,			// string compare ascending
	STRCMP_D,			// string compare descending
	INDEX_A,				// index	compare ascending
	INDEX_D				// index	compare descending
};

class CSortFieldInfo		// the information of the field to be sorted
{
public:
	int mFieldIndex;		// the index to the array of fields
	ESortInfo mSortInfo;	//	the information how to sort the field
};

// the dynamic array denoting the selected fields to be sorted
typedef CTypedPtrArray<CPtrArray, CSortFieldInfo*> CFieldsToBeSorted;	// selected fields
// end sort

// descriptive
class CFieldInfo			// the object describing the corresponding field
{
public:
	CString mFieldName;		// the name of the table field
	CString mLongFieldName;	//	the long name (description) for the field
	CString mLongHeader;		// the long name of the field for the grid header
	CString mToolTip;			// the tooltip for the field

	int mDBLength;				// the database length of the field
	int mCellWidth;			// the cell width of the field

	EDBType mDBType;			// the database type of the field
	ELGType mLGType;			// the logical type of the field
	
	ETimeType mTimeType;		// the time type
	int mTimeZoneDif;			// the time zone difference in minutes (depending on the mTimeType)
							// all translations are based on the following formula:
							// UTC = local time + mTimeZoneDif

	CString mDefaultStrValue;	// the string default value
	double mDefaultNumValue;	// the numeric default value
	bool mReadOnly;			// true if the field is read only
	bool mVisible;				// true if the field is visible
	bool mRequired;			// true if the field is mandatory
	bool mUseCollate;			// true if needs to use the Collate() for sorting
	bool mSortable;			// true if the field can be sorted
	bool mIsString;			// true if the field is a string

	CString mComboTabField;	// TABEXT.FLD (TABEXT) reference to obtain the combo  
									// (CheckListCombo) texts out 
									// of the table TABEXT and the field FLD (fields NAME, CHCK, UNCH, VKEY)
									// (if mLGType == SLST (MLST))
	CString mChoiceList;		// the choice list for the combobox control
	CWDayCheckListComboBox::Attributes *mMultichoiceAtt;	// the multi choice attribute list for the CheckListComboBox control
public:
	CFieldInfo() {mMultichoiceAtt = 0;}
	~CFieldInfo();
};	

class CGRCell			// the grid cell
{
public:
	CString mStrValue;	// the string value of the grid cell
	double mNumValue;		// the number value of the grid cell

	int mCellState;		// the state of the cell
	// double mSortIndex;		// the order of the pre-sorted field
};

typedef CTypedPtrArray<CPtrArray, CGRCell*> CGRRecordset;	// the grid recordset
typedef CTypedPtrArray<CPtrArray, CGRRecordset*> CGRTable;	// the grid table

typedef CTypedPtrArray<CPtrArray, CFieldInfo*> CFieldsDesc;	// the fields descriptive collection
typedef CTypedPtrMap<CMapStringToPtr, CString, CFieldsDesc*> CTablesDesc;	// the tables descriptive collection
// end descriptive

typedef CTypedPtrMap<CMapStringToPtr, CString, CString*> CTableNames;	// the long table names collection
//-----------------------------------------------------------------------
// globals
//-----------------------------------------------------------------------



//*** 02.12.1999 SHA ***
extern CMapStringToString ogTableKeys;
extern char pcgHome[4];
extern CedaBasicData ogBCD;

// aliases for inappropriate characters
extern const CString CLIENT_CHARS;
extern const CString SERVER_CHARS;

extern LPCTSTR SYS_FIELD[];		// the list of SYSTAB field names
extern const int SYS_FIELD_LENGTH[];		// the list of SYSTAB field lengths

extern LPCTSTR TAB_FIELD[];					// the list of TABTAB field names
extern const int TAB_FIELD_LENGTH[];		// the list of TABTAB field lengths

extern LPCTSTR BOOLEAN_TYPE[];				// the list of boolean values 
//h - start
extern LPCTSTR BOL1_TYPE[];					// the list of bol1 values 
//h - end

//*** 18.11.1999 SHA ***
extern LPCTSTR BOL2_TYPE[];					// the list of bol2 values 

extern LPCTSTR DATABASE_TYPE[]; 
extern LPCTSTR LOGICAL_TYPE[];				// the names for the logical types

// database values for ETimeType
extern LPCTSTR TIME_TYPE[];

// globals for application menu
#define MAX_OF_MENU_ITEMS_VIEWS	20	// maximum ID_VIEW_MAX - ID_VIEW_MIN (in rc file)

// globals for grid
#define GRID_NO_EDIT_MODE	0	// grid is in valid state

// globals for dialog Filter - Sort
#define MAX_LEN_FILTER	800	// max length of the text for filter
#define MAX_LEN_SORT		1100	// max length of the text for sort (max: 2000 - MAX_LEN_FILTER)
#define MAX_LEN_VIEW_NAME	30	// max length of the view name (VCD table)
#define LEN_FIELD_NAME	4		// length of the field name from database
#define FILTER_SORT_SEPARATOR	_T("\t")	// separator of filter and sort clause in the VCD table
#define SECURITY_SEPARATOR	_T("\t")	// separator of security informations

//globals for the main application
extern CTablesDesc gTablesDesc;				// descriptive structure
extern CStringArray gTables;					// the table name collection
extern CTableNames gLongTableNames;			// the long table name collection
extern CGRTable gGRTable;						// the internal data structure
extern CFieldsToBeSorted gSortedFields;	// fields to be sorted
extern CFieldsDesc *gCurrentFieldsDesc;	// the description fields of the current table 
extern int gURNONumBufIndex;					// the index to the URNO array
extern int gURNONumBuf[MAX_URNOS];			// the array for the MAX_URNOS URNOs
extern CUrnoMap gUrnoMap;						// the URNO map 
extern TCHAR gDecimalSymbol;					// the decimal symbol

extern CWDayCheckListComboBox *gWdayControl;		// Wday control pointer providing its methods

extern TCHAR gWorkstationName[];
extern CString gErrorMessage;
extern CCopPast gStyleBuffer;					// Style buffer

#define APP_NAME	_T("DBDPS")				// default application name
//#define INI_FILE	_T("C:\\Ufis\\System\\Ufis.ini")	// default application name
#define INI_FILE	_T("C:\\Ufis\\System\\Ceda.ini")	// default application name

#ifdef ABBCCS_RELEASE
extern CCSLog gLog;
extern CCSDdx gDdx;
extern CCSCedaCom gComHandle;
extern CCSBcHandle gBcHandle;
#endif

#define DBDPS_BC_NEW 1
#define DBDPS_BC_CHANGE 2
#define DBDPS_BC_DELETE 3
//*** 20.10.99 SHA ***
#define BC_PST_CHANGE 4


//*** 15.11.99 SHA ***
extern CString gSystabLogFile;
//*** 16.11.99 SHA ***
extern CString gSettingsPath;
//*** 09.12.1999 SHA ***
extern bool	   gConfirmUpdate;
//*** 20.01.2000 SHA ***
extern CString gStandardFont;		//*** STANDARF FONT NAME ***

extern CString gParameterPassword;		//*** Password via parameter ***
extern CString gParameterUser;			//*** user via parameter ***

extern CString gHomeairport;	// homeairport
extern LPTSTR gDataPtr;			// Data pointer
extern LPTSTR gSecPtr;			// security data pointer
extern int gSecRowCount;		// number of security rows
extern CString gTableExt;			// tables extension of the application
extern CString gTableName;			// current table 
extern CString gLongTableName;	// current long table name 
extern CString gWhereClause;		// current where clause
extern CString gSortClause;		// current sort clause 

#define VCDTAB	_T("VCD")	// the VCDTAB table name without extension

#define DEF_VIEW_URNO	_T("-1")	// urno of the 'default' view
#define DEF_VIEW_NAME	_T("<Default>")	// name of the 'default' view

#define LEN_RETURN_VALUE	40	// length of value returned by the GetPrivateProfileString
#define DEF_STR	_T("UNKNOWN")	// default value for GetPrivateProfileString

extern CString gHostName;	// DB server
extern CString gHostType;	// DB server

extern CString gUserName;	// default user name

extern bool gTableLoad;	// is table load flag

typedef CTypedPtrArray<CPtrArray, CFieldInfo*> CFieldsDesc;	// the fields descriptive collection

class CSavedViews	// views
{
	CTypedPtrArray<CPtrArray, CString*>	m_nUrnoText;
	CTypedPtrArray<CPtrArray, CString*>	m_nViewName;
	CTypedPtrArray<CPtrArray, CString*>	m_nTableName;
	CTypedPtrArray<CPtrArray, CString*>	m_nFilterText;
	CTypedPtrArray<CPtrArray, CString*>	m_nSortText;

public:
	void Add(CString &UText, CString &VName, CString &TName, CString &FText, CString &SText)
	{
		CString *myUText = new CString(UText);
		m_nUrnoText.Add(myUText);
		CString *myVName = new CString(VName);
		m_nViewName.Add(myVName);
		CString *myTName = new CString(TName);
		m_nTableName.Add(myTName);
		CString *myFText = new CString(FText);
		m_nFilterText.Add(myFText);
		CString *mySText = new CString(SText);
		m_nSortText.Add(mySText);
	}
	void GetAt(int idx, CString &UText, CString &VName, CString &TName, CString &FText, CString &SText)
	{
		UText = *m_nUrnoText.GetAt(idx);
		VName = *m_nViewName.GetAt(idx);
		TName = *m_nTableName.GetAt(idx);
		FText = *m_nFilterText.GetAt(idx);
		SText = *m_nSortText.GetAt(idx);
	}
	void SetAt(int idx, CString &UText, CString &VName, CString &TName, CString &FText, CString &SText)
	{
/*
		m_nUrnoText.ElementAt(idx) = UText;
		m_nViewName.ElementAt(idx) = VName;
		m_nTableName.ElementAt(idx) = TName;
		m_nFilterText.ElementAt(idx) = FText;
		m_nSortText.ElementAt(idx) = SText;
*/
		*m_nUrnoText.GetAt(idx) = UText;
		*m_nViewName.GetAt(idx) = VName;
		*m_nTableName.GetAt(idx) = TName;
		*m_nFilterText.GetAt(idx) = FText;
		*m_nSortText.GetAt(idx) = SText;
	}
	void RemoveAt(int idx)
	{
/*
		delete &m_nUrnoText.ElementAt(idx);
		delete &m_nViewName.ElementAt(idx);
		delete &m_nTableName.ElementAt(idx);
		delete &m_nFilterText.ElementAt(idx);
		delete &m_nSortText.ElementAt(idx);
*/
		delete m_nUrnoText.GetAt(idx);
		delete m_nViewName.GetAt(idx);
		delete m_nTableName.GetAt(idx);
		delete m_nFilterText.GetAt(idx);
		delete m_nSortText.GetAt(idx);
		
		m_nUrnoText.RemoveAt(idx);
		m_nViewName.RemoveAt(idx);
		m_nTableName.RemoveAt(idx);
		m_nFilterText.RemoveAt(idx);
		m_nSortText.RemoveAt(idx);
	}
	void RemoveAll()
	{
		for (int idx = 0; idx < m_nUrnoText.GetSize(); idx++)
		{
			delete m_nUrnoText.GetAt(idx);
			delete m_nViewName.GetAt(idx);
			delete m_nTableName.GetAt(idx);
			delete m_nFilterText.GetAt(idx);
			delete m_nSortText.GetAt(idx);
/*
			delete &(m_nUrnoText.ElementAt(idx));
			delete &(m_nViewName.ElementAt(idx));
			delete &(m_nTableName.ElementAt(idx));
			delete &(m_nFilterText.ElementAt(idx));
			delete &(m_nSortText.ElementAt(idx));
*/
		}

		m_nUrnoText.RemoveAll();
		m_nViewName.RemoveAll();
		m_nTableName.RemoveAll();
		m_nFilterText.RemoveAll();
		m_nSortText.RemoveAll();
	}
	int GetSize() const {return m_nUrnoText.GetSize();}
};

//-----------------------------------------------------------------------
//	Prototypes
//-----------------------------------------------------------------------
bool GetToken
	(
		LPCTSTR &FromBuffer,
		LPTSTR ToBuffer
	);
void ConvertStringToServer(CString &pText);
void ConvertStringToClient(CString &pText);
void SpecialConversion(CString &pText);
int GridCompare(const void *arg1, const void *arg2);
bool GetNewURNO(CString &NewURNO);
bool GetManyURNO(LPTSTR URNOStrBuf);
void ClearSortedFields();
BOOL LoadSavedViews(CString pTableName, CSavedViews* pSavedViews, BOOL pDefault);
BOOL ConvertFieldsToSortIndex(CString &pTableName, const CString &pSortFields);

int DBDPSCallCeda(LPCTSTR pUfisCommand,
				  LPCTSTR pTableName,
				  LPCTSTR pSortClause,
				  LPCTSTR pWhereClause,
				  LPCTSTR pFields,
				  LPCTSTR pValues);

void ProcessBroadcasts
	(
		void * pInstance,
		int pDdxType,
		void * pDataPointer,
		CString &pInstanceName
	);

LPCTSTR GetSecurityStatus
	(
		LPCTSTR pObjectId,
		LPCTSTR pControlId
	);

// security
class CSecurityObject			// the object describing the security object
{
public:
	CString mLabel;	// the label of the security object
	CString mType;		// the control type of this object
	CString mStatus;	//	the security status for this object
};

typedef CTypedPtrMap<CMapStringToPtr, CString, CSecurityObject*> CSecurityObjectCollection;	// the security labels collection
typedef CTypedPtrMap<CMapStringToPtr, CString, CSecurityObjectCollection*> CSecurityPtrMap;	// the security labels collection
extern CSecurityPtrMap gSecurityMap;

extern bool gCanMainInsertRow;
extern bool gCanTableInsertRow;
extern bool gCanMainUpdateRow;
extern bool gCanTableUpdateRow;
extern bool gCanMainDeleteRow;
extern bool gCanTableDeleteRow;
extern bool gCanTableUpdateFields;
extern BOOL gRegisteringApp;

#define SEC_STAT_ACT			_T("1")	// security status 'activated'
#define SEC_STAT_DEACT		_T("0")	// security status 'deactivated'
#define SEC_STAT_INV			_T("-")	// security status 'invisible'
#define SEC_STAT_DEF			SEC_STAT_INV	// default security status

#define SEC_TYPE_MI _T("I")	// security controltype 'menu item'
#define SEC_TYPE_EF _T("E")	// security controltype 'edit field'
#define SEC_TYPE_LC _T("A")	// security controltype 'list control'
#define SEC_TYPE_TC _T("N")	// security controltype 'table column'

#define MAX_LOGIN	3	// maximum count of login attempt

#endif //__GLOBALS_H__