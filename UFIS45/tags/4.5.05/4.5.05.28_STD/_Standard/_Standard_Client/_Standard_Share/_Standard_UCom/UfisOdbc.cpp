#include <stdafx.h>
#include ".\UfisOdbc.h"


extern int ExtractTextLineFast(CStringArray &opItems, char *pcpLineText, char *pcpSepa);

UfisOdbc::UfisOdbc()
{
	isOdbcInit = false;
}

UfisOdbc::UfisOdbc(CString opConnectionString)
{
	omConnectionString = opConnectionString;
	isOdbcInit = false;
}

UfisOdbc::~UfisOdbc()
{
}

int UfisOdbc::CallDB(CString opAction, 
					 CString opTable, 
					 CString opFields, 
					 CString opWhere, 
					 CString opValues)
{
	CDatabase olDB;
	int sqlRet = 0;
	char	pclValue[2000]="";
//	SDWORD	OutputDataLen;
	unsigned char pclConnStrOut[256];
	int ilFieldCount=0;
	int ilValueCount=0;
	char  sDataArray[255][2000];
	char pclDataBuffer[25600];

	char pclSqlState[6]="";
	long  ilNativeError;
	SQLCHAR pclErrorMsg[SQL_MAX_MESSAGE_LENGTH];
	short ilErrorLen;
	SDWORD dwDataLen[255];
	CStringArray olFields;
	CStringArray olValues;
	CString strFields = opFields;
	CString strValues = opValues;
	CString olTable = opTable;
	CString olWhere = opWhere;
	ilFieldCount = ExtractTextLineFast(olFields, opFields.GetBuffer(0), ",");//ExtractItemList(opFields, &olFields, ',');
	ilValueCount = ExtractTextLineFast(olValues, opValues.GetBuffer(0), ",");//ExtractItemList(opValues, &olValues, ',');
	omData.RemoveAll();
	omLastErrorMessage="";

	pclErrorMsg[0] = '\0';
	if(isOdbcInit == false)
	{
		sqlRet = ::SQLAllocEnv(&hEnv);
		if(sqlRet == SQL_SUCCESS)
		{
			sqlRet = ::SQLAllocConnect(hEnv, &hDbConn);
			if(sqlRet == SQL_SUCCESS)
			{
				//sqlRet = ::SQLDriverConnect(hDbConn,0,(unsigned char*)"DSN=Peking-ODBC;UID=ceda;PWD=ceda", SQL_NTS, pclConnStrOut, 256, NULL, SQL_DRIVER_NOPROMPT);
				sqlRet = ::SQLDriverConnect(hDbConn,0,(unsigned char*)omConnectionString.GetBuffer(0), SQL_NTS, pclConnStrOut, 256, NULL, SQL_DRIVER_NOPROMPT);
				if(sqlRet == SQL_SUCCESS)
				{
					isOdbcInit = true;				
				}
				else
				{
					AfxMessageBox("Could not connect ODBC-Database!!");
					return -1;
				}
			}
		}
	} 

	CString olSql;
	if(opAction == "Select")
	{
		if(::SQLAllocStmt(hDbConn, &hStmt) == SQL_SUCCESS)
		{

			CString olSql = opAction + " " + strFields + " FROM " + olTable + " " + olWhere;
			if(::SQLExecDirect(hStmt, (unsigned char*)olSql.GetBuffer(0), SQL_NTS) == SQL_SUCCESS) //ilFieldCount
			{
			   //SQLNumResultCols(hStmt, &nCols);
			   for(int ilC =0; ilC < ilFieldCount; ilC++)
				  SQLBindCol(hStmt, (UWORD)(ilC+1), SQL_C_CHAR, (unsigned char*)sDataArray[ilC], 2000, &dwDataLen[ilC]);

				for(sqlRet = ::SQLFetch(hStmt); sqlRet == SQL_SUCCESS; sqlRet = ::SQLFetch(hStmt))
				{
				
					pclDataBuffer[0]='\0';
					for(int i=0;  i<ilFieldCount; i++) 
					{
					 // check if the column is a null value?
						CString olValue;
						olValue = CString((dwDataLen[i]==SQL_NULL_DATA)?"":sDataArray[i]);
						olValue.TrimRight();
						MakeClientString( olValue );
						strcat(pclDataBuffer, olValue.GetBuffer(0));
						int dwText = strlen(pclDataBuffer);
						pclDataBuffer[dwText++] = ',';
						pclDataBuffer[dwText] = '\0';
					}
					if (*pclDataBuffer)
						pclDataBuffer[strlen(pclDataBuffer)-1]='\0';
					else
						break;
					omData.Add(pclDataBuffer);
					pclDataBuffer[0]='\0';

				}
				omLastErrorMessage="";
			}
			else
			{
				::SQLGetDiagRec(SQL_HANDLE_STMT, hStmt, 1, (unsigned char*)pclSqlState, &ilNativeError, 
								pclErrorMsg, sizeof(pclErrorMsg), &ilErrorLen);
				omLastErrorMessage = CString(pclErrorMsg);
			}
			::SQLFreeStmt(hStmt, SQL_DROP);
		}

	}
	if(opAction == "Update" )
	{
		if(ilFieldCount != ilValueCount)
		{
			AfxMessageBox("Fieldlist not the same amount as Valuelist");
			return -1;
		}
		if(::SQLAllocStmt(hDbConn, &hStmt) == SQL_SUCCESS)
		{
			CString olSql = opAction + " " + opTable + " Set ";//opFields + " FROM " + "CEDA." + opTable + " " + opWhere;
			CString olPart;
			for(int i = 0; i < olValues.GetSize() && i < olFields.GetSize(); i++)
			{
				if(!olValues[i].IsEmpty() && !olFields[i].IsEmpty())
				{
					if((i+1) == olValues.GetSize())
					{
						olPart += olFields[i] + CString("=") + olValues[i] + CString(" ");
					}
					else
					{
						olPart += olFields[i] + CString("=") + olValues[i] + CString(",");
					}
				}
			}
			olSql += olPart + opWhere;
			if(::SQLExecDirect(hStmt, (unsigned char*)olSql.GetBuffer(0), SQL_NTS) != SQL_SUCCESS) //ilFieldCount
			{
				AfxMessageBox("Update could not be performed ==> Error occured!!");
			}
			else
			{
				::SQLGetDiagRec(SQL_HANDLE_STMT, hStmt, 1, (unsigned char*)pclSqlState, &ilNativeError, 
								pclErrorMsg, sizeof(pclErrorMsg), &ilErrorLen);
				omLastErrorMessage = CString(pclErrorMsg);
			}
			::SQLFreeStmt(hStmt, SQL_DROP);
		}
	}
	if(opAction == "Insert")
	{
		if(ilFieldCount != ilValueCount)
		{
			AfxMessageBox("Fieldlist not the same amount as Valuelist");
			return -1;
		}
		if(::SQLAllocStmt(hDbConn, &hStmt) == SQL_SUCCESS)
		{
			CString olSql = opAction + " INTO " + opTable + CString("(") + opFields +  CString(")") + " VALUES (" + opValues + CString(")");
			if(::SQLExecDirect(hStmt, (unsigned char*)olSql.GetBuffer(0), SQL_NTS) != SQL_SUCCESS) //ilFieldCount
			{
				AfxMessageBox("Insert could not be performed ==> Error occured!!");
			}
			else
			{
				::SQLGetDiagRec(SQL_HANDLE_STMT, hStmt, 1, (unsigned char*)pclSqlState, &ilNativeError, 
								pclErrorMsg, sizeof(pclErrorMsg), &ilErrorLen);
				omLastErrorMessage = CString(pclErrorMsg);
			}
			::SQLFreeStmt(hStmt, SQL_DROP);
		}
	}
	if(opAction == "Delete")
	{
		if(opWhere.IsEmpty())
		{
			AfxMessageBox("Delete without WHERE is not permitted!!");
			return -1;
		}
		CString olSql = opAction + " FROM " + opTable + CString(" ") + opWhere;
		if(::SQLExecDirect(hStmt, (unsigned char*)olSql.GetBuffer(0), SQL_NTS) != SQL_SUCCESS) //ilFieldCount
		{
			::SQLGetDiagRec(SQL_HANDLE_STMT, hStmt, 1, (unsigned char*)pclSqlState, &ilNativeError, 
							pclErrorMsg, sizeof(pclErrorMsg), &ilErrorLen);
			omLastErrorMessage = CString(pclErrorMsg);
//			AfxMessageBox("Delete could not be performed ==> Error occured!!");
		}
		::SQLFreeStmt(hStmt, SQL_DROP);

	}
	return 0;
}

bool UfisOdbc::GetManyUrnos(int ipCount, CUIntArray &opUrnos)
{
	bool blRet = true;
	int ilUrno,ilCurr;
	opUrnos.RemoveAll();
	if(CallDB("Select", "NUMTAB", "ACNU", "WHERE KEYS='SNOTAB'", "") == 0)
	{
		ilUrno = atoi(omData[0].GetBuffer(0));
		ilCurr = ilUrno;
		for(int i = ilUrno; i < (ilUrno+ipCount); i++)
		{
			opUrnos.Add(ilCurr++);
		}
	}
	CString olCurr;
	olCurr.Format("%d", ilCurr);
	if(CallDB("Update", "NUMTAB", "ACNU", "WHERE KEYS='SNOTAB'", olCurr) != 0)
	{
		AfxMessageBox("DB Problem occured at GetManyUrnos");
		blRet = false;
	}
	return blRet;
}

int UfisOdbc::GetLineCount()
{
	return omData.GetSize();
}

//return then number of values
int UfisOdbc::GetLine(int ipLineNo, CStringArray &ropValues)
{
	int ilValueCount=0;
	if(omData.GetSize() > ipLineNo)
	{
		ilValueCount = ExtractTextLineFast(ropValues, omData[ipLineNo].GetBuffer(0), ",");//ExtractItemList(omData[ipLineNo], &ropValues, ',');
//		for(int i = 0; i < ilValueCount; i++)
//		{
//			ropValues[i].TrimRight();
//		}
	}
	return ilValueCount;
}

void UfisOdbc::MakeClientString(CString &ropText)
{
	CString olClientChars("\042\047");  //\54\012\015\072");  // dezimal: 34,39,44,10,13,58

	olClientChars += CString(",") + CString("\012\015\072");
						    //  {   }   [   ]   |   ^   ? (  )  ;  @
	olClientChars += CString("\173\175\133\135\174\136\77\50\51\73\100");

	CString    olServerChars("\260\261"); //\375\264\263\277");  // dezimal:  176,177,253,180,179,191

	olServerChars += CString("�") + CString("\264\263\277");
							//  {   }   [   ]   |   ^   ?   (   )   ;   @
	olServerChars += CString("\223\224\225\226\227\230\231\233\234\235\236");

	for (int ilLc = 0; ilLc < ropText.GetLength(); ilLc++)
	{
		int ilIndex;
		if ((ilIndex = olServerChars.Find(ropText.GetAt(ilLc))) > -1)
		{
			ropText.SetAt(ilLc,olClientChars[ilIndex]);
		}
	}
}
