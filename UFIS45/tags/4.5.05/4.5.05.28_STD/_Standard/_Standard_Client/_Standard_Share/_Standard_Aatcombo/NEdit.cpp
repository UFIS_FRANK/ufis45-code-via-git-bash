/************************************
  REVISION LOG ENTRY
  Revision By: Mihai Filimon
  Revised on 12/11/98 3:15:27 PM
  Comments: NEdit.cpp : implementation file
 ************************************/

#include "stdafx.h"
#include "NCombo.h"
#include "NEdit.h"
#include "NWindow.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNEdit

#define defaultOpenKey VK_F4
#define defaultForwardKey VK_F3

CNEdit::CNEdit():m_bkBrush(RGB(255,255,255))
{
}

CNEdit::~CNEdit()
{
}


BEGIN_MESSAGE_MAP(CNEdit, CEdit)
	//{{AFX_MSG_MAP(CNEdit)
	ON_WM_KEYDOWN()
	ON_WM_KILLFOCUS()
	ON_WM_CREATE()
	ON_WM_CTLCOLOR_REFLECT()
	ON_WM_SETFOCUS()
	ON_CONTROL_REFLECT(EN_CHANGE, OnChange)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNEdit message handlers

// Function name	: CNEdit::GetColumn
// Description	    : The ID of control is in fact the column number
// Return type		: const int 
const int CNEdit::GetColumn()
{
	return GetDlgCtrlID(); 
}

// Function name	: CNEdit::SetColumn
// Description	    : Change the ID to be index of column
// Return type		: void 
// Argument         : int nColumn
void CNEdit::SetColumn(int nColumn)
{
	SetDlgCtrlID(nColumn);
}


// Function name	: CNEdit::OnKeyDown
// Description	    : 
// Return type		: void 
// Argument         : UINT nChar
// Argument         : UINT nRepCnt
// Argument         : UINT nFlags
void CNEdit::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	if (CNListCtrl* pNListCtrl = GetListCtrl())
		switch (nChar)
		{
			case defaultOpenKey:
			{
				pNListCtrl->Toggle();
				break;
			}
			case defaultForwardKey:
			{
				if (GetHeader()->IsUserReason())
				{
					CString sText; GetWindowText(sText);
					pNListCtrl->OnEditChange(GetColumn(), sText, GetAsyncKeyState(VK_SHIFT) < 0 ? -1 : 1);
				}
				break;
			}
			case VK_UP:
			case VK_DOWN:
			case VK_NEXT:
			case VK_PRIOR:
			{
				pNListCtrl->SendMessage(WM_KEYDOWN, nChar, 0);
				return;
			}
			case VK_TAB:
			{
				if (pNListCtrl->IsMouseReleased())
					GetHeader()->Next(this, (!(GetAsyncKeyState(VK_SHIFT) < 0) ));
				break;
			}
			case VK_ESCAPE:
			case VK_RETURN:
			{
				if (pNListCtrl->IsOpen())
					if ( nChar == VK_RETURN )
						pNListCtrl->Close(FALSE);
					else
						if ( nChar == VK_ESCAPE )
						{
							pNListCtrl->Restore();
							pNListCtrl->Close();
						}
				break;
			}
		}

	CEdit::OnKeyDown(nChar, nRepCnt, nFlags);
}

// Function name	: CNEdit::GetParentNWindow
// Description	    : Return the parent of header
// Return type		: CNWindow* 
CNWindow* CNEdit::GetParentNWindow()
{
	return GetHeader()->m_pParent;
}

// Function name	: CNEdit::GetHeader
// Description	    : Return the parent header of this
// Return type		: CNHeaderCtrl* 
CNHeaderCtrl* CNEdit::GetHeader()
{
	return (CNHeaderCtrl*)GetParent();
}

// Function name	: CNEdit::OnKillFocus
// Description	    : 
// Return type		: void 
// Argument         : CWnd* pNewWnd
void CNEdit::OnKillFocus(CWnd* pNewWnd) 
{
	CEdit::OnKillFocus(pNewWnd);
	
	if (CNListCtrl* pNListCtrl = GetListCtrl())
		pNListCtrl->OnKillFocus(pNewWnd);

	OnEndEdit();
}

// Function name	: CNEdit::GetListCtrl
// Description	    : 
// Return type		: CNListCtrl* 
CNListCtrl* CNEdit::GetListCtrl()
{
	CNListCtrl* pNListCtrl = GetParentNWindow()->GetListCtrl();
	if (pNListCtrl)
		if (::IsWindow((pNListCtrl->m_hWnd)))
			return pNListCtrl;
	return NULL;
}

// Function name	: CNEdit::OnCreate
// Description	    : 
// Return type		: int 
// Argument         : LPCREATESTRUCT lpCreateStruct
int CNEdit::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CEdit::OnCreate(lpCreateStruct) == -1)
		return -1;

	CloneFont(GetParentNWindow()->GetListCtrl()->GetFont(), &m_font);
	SetFont(&m_font);
	SetMargins(4,4);

	m_tool.Create(this, TTS_ALWAYSTIP);
	m_tool.AddTool(this, GetParentNWindow()->GetColumnName(GetColumn()));
	m_tool.Activate(TRUE);

	return 0;
}

// Function name	: CNEdit::CtlColor
// Description	    : 
// Return type		: HBRUSH 
// Argument         : CDC* pDC
// Argument         : UINT nCtlColor
HBRUSH CNEdit::CtlColor(CDC* pDC, UINT nCtlColor) 
{
	//uhi: Hier Farben des Edit Felds setzten
	pDC->SetTextColor( (this == GetFocus()) ? RGB(0, 0, 255) : RGB(0,0,0));
	return (HBRUSH)m_bkBrush;
}

// Function name	: CNEdit::PreCreateWindow
// Description	    : 
// Return type		: BOOL 
// Argument         : CREATESTRUCT& cs
BOOL CNEdit::PreCreateWindow(CREATESTRUCT& cs) 
{
	cs.style |= ES_AUTOHSCROLL;
	
	return CEdit::PreCreateWindow(cs);
}

// Function name	: CNEdit::OnSetFocus
// Description	    : 
// Return type		: void 
// Argument         : CWnd* pOldWnd
void CNEdit::OnSetFocus(CWnd* pOldWnd) 
{
	CEdit::OnSetFocus(pOldWnd);

	if (CNListCtrl* pNListCtrl = GetListCtrl())
	{
		CRect rInter, rChild, rParent; GetParent()->GetWindowRect(rParent); GetWindowRect(rChild);
		BOOL bNeed = TRUE;
		if (rInter.IntersectRect(rParent, rChild)) bNeed = !(rInter == rChild);
		if (bNeed)
		{
			int cx = -16 + GetHeader()->GetTotalWidth2(this, pNListCtrl->GetHScrollMax());
			// Only if list is droped. Why? Because the list do not scroll if this is hide
			if (pNListCtrl->IsOpen())
			{
				pNListCtrl->Scroll(CSize(-pNListCtrl->GetHScroll() + cx,pNListCtrl->GetVScroll()));
				pNListCtrl->Invalidate(FALSE);
				GetHeader()->ScrollToPos(pNListCtrl->GetHScroll());
			}
		}
		OnStartEdit();
	}
}

// Function name	: CNEdit::OnStartEdit
// Description	    : Put a border arround this
// Return type		: void 
void CNEdit::OnStartEdit()
{
	ModifyStyle(0, WS_BORDER, SWP_FRAMECHANGED);
	GetParentNWindow()->SetColumnKey(GetColumn());
}

// Function name	: CNEdit::OnEndEdit
// Description	    : 
// Return type		: void 
void CNEdit::OnEndEdit()
{
	ModifyStyle(WS_BORDER,0, SWP_DRAWFRAME | SWP_FRAMECHANGED);
}

// Function name	: CNEdit::CanBeFocused
// Description	    : return TRUE or FALSE if this edit can be focused
// Return type		: BOOL 
BOOL CNEdit::CanBeFocused()
{
	return (IsWindowVisible() && IsWindowEnabled() && GetListCtrl()->GetColumnWidth(GetColumn()));
}

// Function name	: CNEdit::OnChange
// Description	    : Something is change in this edit. 
// We will notify the parent what's happen
// Return type		: void 
void CNEdit::OnChange() 
{
	if (GetHeader()->IsUserReason())
	{
		CString sText; GetWindowText(sText);
		GetListCtrl()->OnEditChange(GetColumn(), sText);
	}
}

// Function name	: CNEdit::WindowProc
// Description	    : 
// Return type		: LRESULT 
// Argument         : UINT message
// Argument         : WPARAM wParam
// Argument         : LPARAM lParam
LRESULT CNEdit::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	if (::IsWindow(m_tool.m_hWnd))
	{
		MSG msg = {m_hWnd, message, wParam, lParam};
		m_tool.RelayEvent(&msg);
	}
	
	return CEdit::WindowProc(message, wParam, lParam);
}
