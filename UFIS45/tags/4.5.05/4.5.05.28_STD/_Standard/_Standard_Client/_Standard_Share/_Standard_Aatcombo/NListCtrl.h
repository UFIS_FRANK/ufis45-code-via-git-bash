/************************************
  REVISION LOG ENTRY
  Revision By: Mihai Filimon
  Revised on 12/11/98 3:19:11 PM
  Comments: NListCtrl.h : header file
 ************************************/


#if !defined(AFX_NLISTCTRL_H__DE20179B_9000_11D2_8726_0040055C08D9__INCLUDED_)
#define AFX_NLISTCTRL_H__DE20179B_9000_11D2_8726_0040055C08D9__INCLUDED_

#include "NHeaderCtrl.h"	// Added by ClassView

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CNComboCtrl;

/////////////////////////////////////////////////////////////////////////////
// CNListCtrl window

class CNWindow;

class CNListCtrl : public CListCtrl
{
	friend class CNWindow;
	friend class CNEdit;
// Construction
public:
	CNListCtrl();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNListCtrl)
	protected:
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual short FindItemData(long nItemData);
	virtual short FindItemText(short nColumn, LPCTSTR lpszColumnText);
	virtual CNComboCtrl* GetControl();
	virtual void Restore();
	virtual int GetItemHeight();
	virtual void SetColumnWidth2(int iColumn, int nNewValue);
	virtual void OnEditChange(int nColumn, LPCTSTR lpszColumnText, int nType = 0);
	virtual const int GetSelect();
	virtual void SetSelect(int nItem, BOOL bNotify = FALSE);
	virtual BOOL IsMouseReleased();
	virtual BOOL IsClose();
	virtual BOOL IsOpen();
	virtual int GetHScrollMax();
	virtual int GetHScroll();
	virtual int GetVScroll();
	virtual void Toggle();
	virtual void Open();
	virtual void SetParent(CNWindow* pParent);
	virtual ~CNListCtrl();

	// Generated message map functions
protected:
	virtual void NotifyChangeColumnContent(int nColumn, LPCTSTR sColumnContent, int nType);
	BOOL m_bLastChanging;
	long m_nLastNotifyItem;
	virtual void NotifyControlChange(BOOL bChanging, long nItem);
	int m_nSaveSelectItem;
	CFont m_font;
	virtual BOOL IsMouseCaptured();
	CPoint m_ptnCapture;
	virtual int IsSizingColumn(CPoint point);
	BOOL m_bSizing;
	int m_nSelected;
	int m_nLastSelected;
	int m_nReleaseMouse;
	int m_nHScroll;
	UINT m_nHitTest;
	virtual void Close(BOOL bCancel = TRUE);
	virtual int OnInit();
	//{{AFX_MSG(CNListCtrl)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnWindowPosChanging(WINDOWPOS FAR* lpwndpos);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
	CNWindow* m_pParent;
private:
	int m_iResizeColumn;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NLISTCTRL_H__DE20179B_9000_11D2_8726_0040055C08D9__INCLUDED_)
