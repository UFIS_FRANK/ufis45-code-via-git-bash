/************************************
  REVISION LOG ENTRY
  Revision By: Mihai Filimon
  Revised on 12/11/98 3:18:59 PM
  Comments: NEdit.h : header file
 ************************************/


#if !defined(AFX_NEDIT_H__9B904A96_90C7_11D2_8728_0040055C08D9__INCLUDED_)
#define AFX_NEDIT_H__9B904A96_90C7_11D2_8728_0040055C08D9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CNEdit window

class CNWindow;
class CNListCtrl;
class CNHeaderCtrl;


class CNEdit : public CEdit
{
// Construction
public:
	CNEdit();

// Attributes
public:
	CToolTipCtrl m_tool;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNEdit)
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual void SetColumn(int nColumn);
	virtual const int GetColumn();
	virtual BOOL CanBeFocused();
	virtual ~CNEdit();

	// Generated message map functions
protected:
	CFont m_font;
	virtual CNHeaderCtrl* GetHeader();
	virtual void OnEndEdit();
	virtual void OnStartEdit();
	CBrush m_bkBrush;
	virtual CNListCtrl* GetListCtrl();
	virtual CNWindow* GetParentNWindow();
	//{{AFX_MSG(CNEdit)
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnChange();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NEDIT_H__9B904A96_90C7_11D2_8728_0040055C08D9__INCLUDED_)
