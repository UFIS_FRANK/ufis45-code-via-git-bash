; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CAboutDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "ncombo.h"
LastPage=0
CDK=Y

ClassCount=7
Class1=CAboutDlg
Class2=CNComboCtrl
Class3=CNComboPropPage
Class4=CNEdit
Class5=CNHeaderCtrl
Class6=CNListCtrl
Class7=CNWindow

ResourceCount=2
Resource1=IDD_ABOUTBOX_NCOMBO (English (U.S.))
Resource2=IDD_PROPPAGE_NCOMBO (English (U.S.))

[CLS:CAboutDlg]
Type=0
BaseClass=CDialog
HeaderFile=AboutDlg.h
ImplementationFile=AboutDlg.cpp
LastObject=CAboutDlg

[CLS:CNComboCtrl]
Type=0
BaseClass=COleControl
HeaderFile=NComboCtl.h
ImplementationFile=NComboCtl.cpp

[CLS:CNComboPropPage]
Type=0
BaseClass=COlePropertyPage
HeaderFile=NComboPpg.h
ImplementationFile=NComboPpg.cpp

[CLS:CNEdit]
Type=0
BaseClass=CEdit
HeaderFile=NEdit.h
ImplementationFile=NEdit.cpp

[CLS:CNHeaderCtrl]
Type=0
BaseClass=CWnd
HeaderFile=NHeaderCtrl.h
ImplementationFile=NHeaderCtrl.cpp

[CLS:CNListCtrl]
Type=0
BaseClass=CListCtrl
HeaderFile=NListCtrl.h
ImplementationFile=NListCtrl.cpp

[CLS:CNWindow]
Type=0
BaseClass=CWnd
HeaderFile=NWindow.h
ImplementationFile=NWindow.cpp

[DLG:IDD_ABOUTBOX_NCOMBO]
Type=1
Class=CAboutDlg

[DLG:IDD_PROPPAGE_NCOMBO]
Type=1
Class=CNComboPropPage

[DLG:IDD_ABOUTBOX_NCOMBO (English (U.S.))]
Type=1
Class=?
ControlCount=9
Control1=IDC_STATIC,static,1342308352
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342308352
Control5=IDC_STATIC,static,1342308352
Control6=IDC_STATIC,static,1342308352
Control7=IDC_STATIC,static,1342308352
Control8=IDC_STATIC,static,1342308352
Control9=IDC_STATIC,static,1342177294

[DLG:IDD_PROPPAGE_NCOMBO (English (U.S.))]
Type=1
Class=?
ControlCount=0

