/************************************
  REVISION LOG ENTRY
  Revision By: Mihai Filimon
  Revised on 12/16/98 10:40:22 AM
  Comments: SmartInvalidate.cpp: implementation of the CSmartInvalidate class.
 ************************************/

#include "stdafx.h"
#include "NCombo.h"
#include "SmartInvalidate.h"
#include "gdi.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

// Function name	: CSmartInvalidate::CSmartInvalidate
// Description	    : default constructor
// Return type		: 
CSmartInvalidate::CSmartInvalidate()
{
	Reset();
}

// Function name	: CSmartInvalidate::~CSmartInvalidate
// Description	    : virtual destructor
// Return type		: 
CSmartInvalidate::~CSmartInvalidate()
{
	Reset();
}

// Function name	: CSmartInvalidate::Reset
// Description	    : 
// Return type		: void 
void CSmartInvalidate::Reset()
{
	for (int i =0; i < m_arWindows.GetSize(); i++)
		delete m_arWindows[i];
	m_arWindows.RemoveAll();
}

// Function name	: CSmartInvalidate::Preview
// Description	    : 
// Return type		: CBitmap* 
// Argument         : CWnd *pWnd
// Argument         : COLORREF colorRGB
CBitmap* CSmartInvalidate::Preview(CWnd *pWnd, COLORREF colorBkGnd)
{
	CBitmap *pBitmap = NULL, bitmapNonClient;
	CWnd *pDrawWnd = pWnd;
	ASSERT (pWnd);
	if (pDrawWnd && ::IsWindow(pDrawWnd->m_hWnd))
	{
		CRect rectWindow; pDrawWnd->GetWindowRect(rectWindow);
		CRect rectClient; pDrawWnd->GetClientRect(rectClient);
		CPoint p(0,0);
		pDrawWnd->ClientToScreen(&p);
		rectClient.OffsetRect(p.x - rectWindow.left, p.y - rectWindow.top);
		CGdiStack stack;
		CDC* pDC = pDrawWnd->GetDC();
		CDC dcMemSourceNonClient, dcMemDestination;
		if (dcMemSourceNonClient.CreateCompatibleDC(pDC))
			if (dcMemDestination.CreateCompatibleDC(pDC))
				if (pBitmap = new CBitmap())
					if (pBitmap->CreateCompatibleBitmap(pDC, rectWindow.Width(), rectWindow.Height()))
						if (bitmapNonClient.CreateCompatibleBitmap(pDC, rectWindow.Width(), rectWindow.Height()))
						{
							CBrush brBkGnd(colorBkGnd);
							stack.Push(&dcMemSourceNonClient, &bitmapNonClient);
							stack.Push(&dcMemSourceNonClient, &brBkGnd);
							dcMemSourceNonClient.PatBlt(0,0,rectWindow.Width(), rectWindow.Height(), PATCOPY);
							stack.Push(&dcMemDestination, pBitmap);
							if (pWnd)
								pDrawWnd->Print(&dcMemSourceNonClient, PRF_NONCLIENT);
							dcMemDestination.BitBlt(0,0,rectWindow.Width(), rectWindow.Height(), &dcMemSourceNonClient, 0,0, SRCCOPY);
							if (pWnd)
							{
								CPoint pLT(0,0);
								pDrawWnd->ClientToScreen(&pLT);
								dcMemDestination.SetViewportOrg(pLT.x - rectWindow.left, pLT.y - rectWindow.top);
								pDrawWnd->Print(&dcMemDestination, PRF_CHILDREN | PRF_CLIENT | PRF_ERASEBKGND);
							}
							stack.Pop();
							stack.Pop();
							stack.Pop();
							brBkGnd.DeleteObject();
							bitmapNonClient.DeleteObject();
							dcMemSourceNonClient.DeleteDC();
							dcMemDestination.DeleteDC();
						}
		pDrawWnd->ReleaseDC(pDC);
	}
	return pBitmap;
}

// Function name	: CSmartInvalidate::Add
// Description	    : Add a new window for smart invalidate
// Return type		: void 
// Argument         : CWnd *pWindow
void CSmartInvalidate::Add(CWnd *pWindow)
{
	// Add a new window into vector, ! After add the flag WS_VISIBLE is reset!
	m_arWindows.Add(new _WTStruct(pWindow));
}

// Function name	: CSmartInvalidate::Update
// Description	    : 
// Return type		: void 
void CSmartInvalidate::Update()
{
	int nWindows = m_arWindows.GetSize(), i = NULL;
	// Prepare the preview, dc...
	for (i = 0; i < nWindows; i++)
	{
		m_arWindows[i]->OnUpdate();
		m_arWindows[i]->Prepare();
	}
	for (i = 0; i < nWindows; i++)
		m_arWindows[i]->Draw();
	for (i = 0; i < nWindows; i++)
		m_arWindows[i]->Restore();
	Reset();
}

