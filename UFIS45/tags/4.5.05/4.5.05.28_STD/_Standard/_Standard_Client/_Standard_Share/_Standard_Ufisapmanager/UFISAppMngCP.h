#ifndef _UFISAPPMNGCP_H_
#define _UFISAPPMNGCP_H_

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 *	UFISAppMang:	UFIS Client Application Manager Project
 *
 *	Proxy Communicator for Client Applications
 *	
 *	ABB Airport Technologies GmbH 2000
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *		Author			Date			Comment
 *	------------------------------------------------------------------
 *
 *		cla	AAT/ID		02/09/2000		Initial version
 *		cla AAT/ID		17/03/2001		Multithreaded version
 *										to avoid deadlocks
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

 /*
  * Be careful here, store a backup of this file in SCM system!
  * The App wizard will recreate this stuff! So you will loose all the changes
  * Remark: To understand what is going on here, see either Don Box -> COM ...
  * or Guy Eddon, Henry Eddon -> Inside DCOM
  */
 /*
  * The ThreadDataStruct ist just for passing the parameters
  * to the thread
  */
struct ThreadDataStruct
{
	IStream *pomStream;
	CComVariant omRes;
	CComVariant omVars[2];
};

template <class T>
class CProxy_IUFISAmEvents : public IConnectionPointImpl<T, &DIID__IUFISAmEvents, CComDynamicUnkArray>
{
	//Warning this class may be recreated by the wizard.

public:
	HRESULT Fire_ForwardData(LONG Orig, BSTR Data)
	{
		CComVariant varResult;
		T* pT = static_cast<T*>(this);
		int nConnectionIndex;
		CComVariant* pvars = new CComVariant[2];
		int nConnections = m_vec.GetSize();

		for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
		{
			pT->Lock();
			CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
			pT->Unlock();
			IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
			if (pDispatch != NULL)
			{
				
				/*
				 * Things getting interesting here
				 * the original code is also running except the call to 'Invoke', of course
				 */
				// ---
				DWORD olThreadId;
				ThreadDataStruct *olThreadData = new ThreadDataStruct;

				CoMarshalInterThreadInterfaceInStream(IID_IDispatch,pDispatch,&(olThreadData->pomStream));
				// ---

				VariantClear(&varResult);
				
				pvars[1] = Orig;
				pvars[0] = Data;

				// ---
				// Pass the function parameter to thread storage

				olThreadData->omVars[1] = Orig;
				olThreadData->omVars[0] = Data;
				/*
				 * Now spawn the new thread
				 */
				CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)ThreadRoutine0,olThreadData,0,&olThreadId);
				// ---
				
				DISPPARAMS disp = { pvars, NULL, 2, 0 };
				
				// Is done in the thread
				//pDispatch->Invoke(0x1, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, &varResult, NULL, NULL);
			}
		}
		delete[] pvars;
		return varResult.scode;
	
	}
	HRESULT Fire_SetAppTag(LONG * AppID)
	{
		CComVariant varResult;
		T* pT = static_cast<T*>(this);
		int nConnectionIndex;
		CComVariant* pvars = new CComVariant[1];
		int nConnections = m_vec.GetSize();
		
		for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
		{
			pT->Lock();
			CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
			pT->Unlock();
			IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
			if (pDispatch != NULL)
			{
				/*
				 * Things getting interesting here
				 * the original code is also running except the call to 'Invoke', of course
				 */
				// ---
				DWORD olThreadId;
				ThreadDataStruct *olThreadData = new ThreadDataStruct;

				CoMarshalInterThreadInterfaceInStream(IID_IDispatch,pDispatch,&(olThreadData->pomStream));
				// ---

				VariantClear(&varResult);
				pvars[0] = AppID;
				// ---
				// Pass the function parameter to thread storage

				olThreadData->omVars[0] = AppID;
				/*
				 * Now spawn the new thread
				 */
				CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)ThreadRoutine1,olThreadData,0,&olThreadId);
				// ---

				DISPPARAMS disp = { pvars, NULL, 1, 0 };
				// Is done in the thread
				//pDispatch->Invoke(0x2, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, &varResult, NULL, NULL);
			}
		}
		delete[] pvars;
		return varResult.scode;
	
	}

	// ==================================================================
	// 
	// FUNCTION :  static void __stdcall ThreadRoutine0()
	// 
	// * Description :
	// 
	// 
	// * Author : [cla AAT/ID], Created : [17.03.01 11:11:35]
	// 
	// * Returns : [void] -
	// 
	// * Function parameters : 
	// [*pThreadData] - Thread storage
	// 
	// ==================================================================
	static void __stdcall ThreadRoutine0(ThreadDataStruct *pThreadData)
	{

		/*
		 * Apartmentthreaded is ok, for most of the sinks
		 * (I hope ... :-))
		 */

		CoInitializeEx(NULL,COINIT_APARTMENTTHREADED);
		
		IDispatch *polDispatch;
		CoGetInterfaceAndReleaseStream(pThreadData->pomStream,IID_IDispatch,(void**)&polDispatch);
		VariantClear(&(pThreadData->omRes));
		DISPPARAMS disp = {(pThreadData->omVars), NULL, 2, 0 };
		polDispatch->Invoke(0x1, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, &(pThreadData->omRes), NULL, NULL);
	
		CoUninitialize();
		delete pThreadData;
		// and exit the thread ...
		// no idea what else to do?
	}

	// ==================================================================
	// 
	// FUNCTION :  static void __stdcall ThreadRoutine1()
	// 
	// * Description :
	// 
	// 
	// * Author : [cla AAT/ID], Created : [17.03.01 11:11:35]
	// 
	// * Returns : [void] -
	// 
	// * Function parameters : 
	// [*pThreadData] - Thread storage
	// 
	// ==================================================================
	static void __stdcall ThreadRoutine1(ThreadDataStruct *pThreadData)
	{

		/*
		 * Apartmentthreaded is ok, for most of the sinks
		 * (I hope ... :-))
		 */

		CoInitializeEx(NULL,COINIT_APARTMENTTHREADED);
		
		IDispatch *polDispatch;
		CoGetInterfaceAndReleaseStream(pThreadData->pomStream,IID_IDispatch,(void**)&polDispatch);
		VariantClear(&(pThreadData->omRes));
		DISPPARAMS disp = {(pThreadData->omVars), NULL, 1, 0 };
		polDispatch->Invoke(0x2, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, &(pThreadData->omRes), NULL, NULL);
	
		CoUninitialize();
		delete pThreadData;
		// and exit the thread ...
		// no idea what else to do?
	}

};
#endif