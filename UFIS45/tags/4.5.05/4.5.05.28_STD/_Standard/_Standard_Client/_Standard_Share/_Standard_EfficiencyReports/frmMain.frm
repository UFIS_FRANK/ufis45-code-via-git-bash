VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{A2F31E92-C74F-11D3-A251-00500437F607}#1.0#0"; "UfisCom.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.ocx"
Object = "{7BDE1363-DF9A-4D96-A2A0-4E85E0191F0F}#1.0#0"; "AatLogin.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmMain 
   Caption         =   "Efficiency Reports"
   ClientHeight    =   9315
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   12975
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   9315
   ScaleWidth      =   12975
   StartUpPosition =   3  'Windows Default
   Tag             =   "s"
   Visible         =   0   'False
   Begin VB.CommandButton cmdJobWODemand 
      Caption         =   "Create Flight Jobs Without Demand"
      Height          =   435
      Left            =   6240
      TabIndex        =   36
      Top             =   7200
      Width           =   2655
   End
   Begin VB.ComboBox cmdTemplates 
      Height          =   315
      Left            =   4920
      TabIndex        =   33
      Top             =   1680
      Width           =   2175
   End
   Begin VB.CommandButton cmdReportOthers 
      Caption         =   "Create Other Allocation Report"
      Height          =   435
      Left            =   6480
      TabIndex        =   30
      Top             =   7200
      Width           =   2535
   End
   Begin VB.CommandButton cmdFunction 
      Caption         =   "Create Demand Comparision Based on Functions"
      Height          =   495
      Left            =   4680
      TabIndex        =   26
      Top             =   7560
      Width           =   2295
   End
   Begin VB.CommandButton cmdCheckin 
      Caption         =   "Create CCI Demands"
      Height          =   495
      Left            =   4680
      TabIndex        =   25
      Top             =   6720
      Width           =   2295
   End
   Begin VB.CommandButton cmdDependent 
      Caption         =   "Create Flight Dependent"
      Height          =   495
      Left            =   2160
      TabIndex        =   24
      Top             =   7560
      Width           =   2295
   End
   Begin VB.CommandButton cmdIndependent 
      Caption         =   "Create Flight Independent"
      Height          =   495
      Left            =   2160
      TabIndex        =   23
      Top             =   6720
      Width           =   2295
   End
   Begin MSComDlg.CommonDialog dlgSave 
      Left            =   360
      Top             =   5880
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "Exit"
      Height          =   435
      Left            =   9360
      TabIndex        =   9
      Top             =   7200
      Width           =   1935
   End
   Begin VB.CommandButton cmdReport 
      Caption         =   "Create Workload Report"
      Height          =   435
      Left            =   6840
      TabIndex        =   8
      Top             =   7200
      Width           =   2295
   End
   Begin VB.Frame pools 
      Caption         =   "Pools"
      Height          =   5175
      Left            =   4680
      TabIndex        =   7
      Top             =   1200
      Width           =   7695
      Begin VB.CommandButton cmdBack 
         Caption         =   "<<"
         Height          =   375
         Left            =   3240
         TabIndex        =   17
         Top             =   2760
         Width           =   1095
      End
      Begin VB.CommandButton cmdFront 
         Caption         =   ">>"
         Height          =   375
         Left            =   3240
         TabIndex        =   16
         Top             =   2160
         Width           =   1095
      End
      Begin VB.ListBox toList 
         Height          =   4155
         Left            =   4560
         TabIndex        =   15
         Top             =   720
         Width           =   2775
      End
      Begin VB.ListBox fromList 
         Height          =   4155
         Left            =   240
         TabIndex        =   14
         Top             =   720
         Width           =   2775
      End
      Begin VB.Frame Frame3 
         Height          =   3255
         Left            =   240
         TabIndex        =   34
         Top             =   1200
         Width           =   2655
      End
      Begin TABLib.TAB tab_DEMTABPLAN 
         Height          =   615
         Left            =   840
         TabIndex        =   21
         Tag             =   "{=TABLE=}SHDEMTAB{=FIELDS=}/*+ INDEX(SHDEMTAB SHDEMTAB_DEBE_DETY) */ DEBE,DEEN,DETY,FLGS,OURI,OURO,RETY,UREF,URUD,UTPL"
         Top             =   2400
         Width           =   375
         _Version        =   65536
         _ExtentX        =   661
         _ExtentY        =   1085
         _StockProps     =   64
      End
      Begin TABLib.TAB tab_DEMTABPLANTEMP 
         Height          =   615
         Left            =   360
         TabIndex        =   27
         Top             =   1680
         Width           =   375
         _Version        =   65536
         _ExtentX        =   661
         _ExtentY        =   1085
         _StockProps     =   64
      End
      Begin TABLib.TAB tab_DEMTABSHOW 
         Height          =   615
         Left            =   840
         TabIndex        =   28
         Top             =   1680
         Width           =   375
         _Version        =   65536
         _ExtentX        =   661
         _ExtentY        =   1085
         _StockProps     =   64
      End
      Begin TABLib.TAB tab_JOBTABPOOL 
         Height          =   615
         Left            =   1320
         TabIndex        =   18
         Tag             =   "{=TABLE=}JOBTAB{=FIELDS=}/*+ INDEX(JOBTAB JOBTAB_ACTO) */ UDSR,URNO,ACFR,ACTO"
         Top             =   1680
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   64
      End
      Begin TABLib.TAB tab_DEMTABTEMP 
         Height          =   615
         Left            =   1320
         TabIndex        =   22
         Top             =   2400
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   64
      End
      Begin TABLib.TAB tab_DEMTABLIVE 
         Height          =   615
         Left            =   360
         TabIndex        =   20
         Tag             =   "{=TABLE=}DEMTAB{=FIELDS=}/*+ INDEX(DEMTAB DEMTAB_DEBE_DETY) */ DEBE,DEEN,DETY,FLGS,OURI,OURO,RETY,UREF,URUD,UTPL,URNO"
         Top             =   2400
         Width           =   375
         _Version        =   65536
         _ExtentX        =   661
         _ExtentY        =   1085
         _StockProps     =   64
      End
      Begin TABLib.TAB tab_JOBTABTEMP 
         Height          =   495
         Left            =   360
         TabIndex        =   19
         Tag             =   "{=TABLE=}JOBTAB{=FIELDS=}/*+ INDEX(JOBTAB JOBTAB_ACTO) */ URNO,ACFR,ACTO,JOUR,TEXT,FCCO,UAFT,DETY"
         Top             =   3120
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   873
         _StockProps     =   64
      End
      Begin TABLib.TAB tab_JOBTABDEM 
         Height          =   495
         Left            =   1320
         TabIndex        =   29
         Tag             =   "{=TABLE=}JOBTAB{=FIELDS=}/*+ INDEX(JOBTAB JOBTAB_ACTO) */ URNO,ACFR,ACTO,UDEM"
         Top             =   3120
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   873
         _StockProps     =   64
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Time Frame"
      Height          =   1455
      Left            =   1200
      TabIndex        =   3
      Top             =   4320
      Width           =   3015
      Begin VB.TextBox toTime 
         Height          =   285
         Left            =   2040
         MaxLength       =   5
         TabIndex        =   13
         Top             =   960
         Width           =   615
      End
      Begin VB.TextBox toDate 
         Height          =   285
         Left            =   840
         TabIndex        =   12
         Top             =   960
         Width           =   1095
      End
      Begin VB.TextBox fromTime 
         Height          =   285
         Left            =   2040
         MaxLength       =   5
         TabIndex        =   11
         Top             =   600
         Width           =   615
      End
      Begin VB.TextBox fromDate 
         Height          =   285
         Left            =   840
         TabIndex        =   10
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label2 
         Caption         =   "To"
         Height          =   375
         Left            =   240
         TabIndex        =   6
         Top             =   960
         Width           =   495
      End
      Begin VB.Label Label1 
         Caption         =   "From"
         Height          =   375
         Left            =   240
         TabIndex        =   5
         Top             =   600
         Width           =   495
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Reports"
      Height          =   2775
      Left            =   1200
      TabIndex        =   2
      Top             =   1200
      Width           =   3015
      Begin VB.OptionButton FlightJobWODemand 
         Caption         =   "Flight Jobs Without Demand"
         Height          =   375
         Left            =   480
         TabIndex        =   35
         Top             =   2160
         Width           =   2415
      End
      Begin VB.OptionButton Others 
         Caption         =   "Other Allocation Jobs"
         Height          =   495
         Left            =   480
         TabIndex        =   32
         Top             =   1560
         Width           =   1815
      End
      Begin VB.OptionButton work 
         Caption         =   "Workload Report"
         Height          =   495
         Left            =   480
         TabIndex        =   31
         Top             =   480
         Width           =   1815
      End
      Begin VB.OptionButton demandCompare 
         Caption         =   "Demand Comparision"
         Height          =   375
         Left            =   480
         TabIndex        =   4
         Top             =   1080
         Width           =   1935
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   300
      Left            =   0
      TabIndex        =   0
      Top             =   9015
      Width           =   12975
      _ExtentX        =   22886
      _ExtentY        =   529
      SimpleText      =   "Ready."
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   17251
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            TextSave        =   "2/15/2007"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   2
            TextSave        =   "5:53 PM"
         EndProperty
      EndProperty
   End
   Begin UFISCOMLib.UfisCom UfisCom1 
      Left            =   120
      Top             =   120
      _Version        =   65536
      _ExtentX        =   1111
      _ExtentY        =   1164
      _StockProps     =   0
   End
   Begin AATLOGINLib.AatLogin AATLoginControl1 
      Height          =   30
      Left            =   3480
      TabIndex        =   1
      Top             =   1560
      Width           =   30
      _Version        =   65536
      _ExtentX        =   53
      _ExtentY        =   53
      _StockProps     =   0
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'#######################################################################
'# Main form
'# WKSADMIN TOOL
'#  based on HASConfigTool Sources
'#
'#  adapted by SHA 20060324
'#
'#######################################################################


Option Explicit

Private gSaved As Boolean
Private gNewRecord As Boolean
Private gOrgWKST As String
Private gOrgDEPT As String
Private gOrgREMA As String
Private gOrgLOCT As String
Private gOrgTADR As String
Private gOrgURNO As String
Private gFilter As String
Private gExcelFile As String

'#######################################################################
'###
'#######################################################################

Public Sub RefreshView()

    Dim tmp As String
    Dim strWhere As String
    Dim i As Long
    Dim l As Long
    Dim d1 As Date
    Dim d2 As Date
    
    InitTabs
    
    gUTCOffset = frmData.GetUTCOffset
    'MsgBox gUTCOffset
    
    work.Value = True
    pools.Visible = True
    pools.Caption = "Organizational Units"
    
    
    tmp = Format(Now, ("dd.mm.yyyy" & " \/ hh:mm"))
    fromDate = Mid(tmp, 1, InStr(tmp, "/") - 2)
    'fromTime = Mid(tmp, InStr(tmp, "/") + 2)
    fromTime = "00:00"
    
    'Preparing date, according to current date&time & current date&time+1
    tmp = Format(DateAdd("d", 1, Now), ("dd.mm.yyyy" & " \/ hh:mm"))
    toDate = Mid(tmp, 1, InStr(tmp, "/") - 2)
    'toTime = Mid(tmp, InStr(tmp, "/") + 2)
    toTime = "23:59"
    
    'Get the required templates info from TPLTAB,
    strWhere = "where tpst = '1'"
    frmData.LoadData frmData.tab_TPLTAB, strWhere
    
    'Get the required pools info from POLTAB
    strWhere = ""
    'frmData.LoadData frmData.tab_POLTAB, strWhere
    frmData.LoadData frmData.tab_ORGTAB, strWhere
    frmData.LoadData frmData.tab_POATAB, strWhere
    
    'Get the required Rules information from RUDTAB & RUETAB .. these will be useful in demand comparision & to create reports based on rules
    frmData.LoadData frmData.tab_RUDTAB, strWhere
    frmData.LoadData frmData.tab_RUETAB, strWhere
    
    'Get the required Rules information from RUDTAB & RUETAB .. these will be useful in demand comparision & to create reports based on functions
    frmData.LoadData frmData.tab_RPFTAB, strWhere
    frmData.LoadData frmData.tab_PFCTAB, strWhere
    
    fromList.Clear
    toList.Clear
    
    'Prepare the pools list as default.
    'i = frmData.tab_POLTAB.GetLineCount
    'For l = 0 To i - 1 Step 1
    '    fromList.AddItem frmData.tab_POLTAB.GetColumnValues(l, 2)
    'Next l
    
    'pools.Caption = "Organizational Units"
    i = frmData.tab_ORGTAB.GetLineCount
    
    For l = 0 To i - 1 Step 1
        fromList.AddItem frmData.tab_ORGTAB.GetColumnValues(l, 1)
    Next l
    
    cmdIndependent.Visible = False
    cmdDependent.Visible = False
    cmdCheckin.Visible = False
    cmdFunction.Visible = False
    
    
End Sub

Private Sub cmdBack_Click()
    fromList.AddItem toList.Text
    toList.RemoveItem toList.ListIndex
End Sub

Private Sub cmdCheckin_Click()
    prepareFlightCheckin
End Sub

Private Sub cmdDependent_Click()
    prepareFlightDependent
End Sub

Private Sub cmdExit_Click()
End
    Dim ilRet
    ilRet = MsgBox("Do you want to exit ?", vbYesNo, "EXIT")
    If ilRet = vbYes Then
        Unload frmData
        End
    End If

End Sub

Private Sub cmdFront_Click()
    toList.AddItem fromList.Text
    fromList.RemoveItem fromList.ListIndex
End Sub

Private Sub cmdFunction_Click()
    prepareFunctionReport
End Sub

Private Sub cmdIndependent_Click()
    prepareFlightIndependentReport
End Sub

Private Sub cmdJobWODemand_Click()
    prepareFlightJobsWithoutDemand
End Sub

Private Sub cmdReport_Click()
    prepareWorkloadReport
End Sub

Private Sub cmdReportOthers_Click()
    prepareOtherAllocationWorkReport2
End Sub




Private Sub FlightJobWODemand_Click()
    Dim i As Long
    Dim l As Long
    Dim tplList As String
    
    tplList = ""
    fromList.Clear
    toList.Clear
    
    pools.Caption = "Templates"
    i = frmData.tab_TPLTAB.GetLineCount
    cmdTemplates.Visible = True
    For l = 0 To i - 1 Step 1
        If InStr(1, tplList, frmData.tab_TPLTAB.GetColumnValues(l, 1)) = 0 Then
            cmdTemplates.AddItem frmData.tab_TPLTAB.GetColumnValues(l, 1)
            tplList = tplList & frmData.tab_TPLTAB.GetColumnValues(l, 1) & ","
       End If
    Next l
    
    cmdIndependent.Visible = False
    cmdDependent.Visible = False
    cmdCheckin.Visible = False
    cmdFunction.Visible = False
    cmdReport.Visible = False
    fromList.Visible = False
    cmdFront.Visible = False
    cmdBack.Visible = False
    toList.Visible = False
    cmdReportOthers.Visible = False
    cmdJobWODemand.Visible = True
End Sub

'#######################################################################
'###
'#######################################################################

Private Sub Form_Unload(Cancel As Integer)
    Unload frmData
End Sub


Private Sub optOthers_Click()
    Dim i As Long
    Dim l As Long
    Dim tplList As String
    
    tplList = ""
    fromList.Clear
    toList.Clear
    
    pools.Caption = "Templates"
    i = frmData.tab_TPLTAB.GetLineCount
    
    For l = 0 To i - 1 Step 1
        If InStr(1, tplList, frmData.tab_TPLTAB.GetColumnValues(l, 1)) = 0 Then
            fromList.AddItem frmData.tab_TPLTAB.GetColumnValues(l, 1)
            tplList = tplList & frmData.tab_TPLTAB.GetColumnValues(l, 1) & ","
        End If
    Next l
    
    cmdReport.Visible = False
    cmdIndependent.Visible = False
    cmdDependent.Visible = False
    cmdCheckin.Visible = False
    cmdFunction.Visible = False
    cmdReportOthers.Visible = True
    
End Sub



Sub InitTabs()

    frmData.InitTabGeneral frmData.tab_POLTAB
    frmData.InitTabForCedaConnection frmData.tab_POLTAB
    frmData.tab_POLTAB.AutoSizeColumns

    frmData.InitTabGeneral frmData.tab_TPLTAB
    frmData.InitTabForCedaConnection frmData.tab_TPLTAB
    frmData.tab_TPLTAB.AutoSizeColumns
    
    frmData.InitTabGeneral frmData.tab_ORGTAB
    frmData.InitTabForCedaConnection frmData.tab_ORGTAB
    frmData.tab_ORGTAB.AutoSizeColumns
    
    frmData.InitTabGeneral frmData.tab_POATAB
    frmData.InitTabForCedaConnection frmData.tab_POATAB
    frmData.tab_POATAB.AutoSizeColumns
    
    frmData.InitTabGeneral frmData.tab_CCCTAB
    frmData.InitTabForCedaConnection frmData.tab_CCCTAB
    frmData.tab_CCCTAB.AutoSizeColumns
    
    frmData.InitTabGeneral frmData.tab_UCDTAB
    frmData.InitTabForCedaConnection frmData.tab_UCDTAB
    frmData.tab_UCDTAB.AutoSizeColumns
    
    frmData.InitTabGeneral frmData.tab_UCDTABPLAN
    frmData.InitTabForCedaConnection frmData.tab_UCDTABPLAN
    frmData.tab_UCDTABPLAN.AutoSizeColumns
    
    frmData.InitTabGeneral frmData.tab_SDITAB
    frmData.InitTabForCedaConnection frmData.tab_SDITAB
    frmData.tab_SDITAB.AutoSizeColumns
    
    frmData.InitTabGeneral frmData.tab_SDITABPLAN
    frmData.InitTabForCedaConnection frmData.tab_SDITABPLAN
    frmData.tab_SDITABPLAN.AutoSizeColumns
    
    frmData.InitTabGeneral frmData.tab_RUDTAB
    frmData.InitTabForCedaConnection frmData.tab_RUDTAB
    frmData.tab_RUDTAB.AutoSizeColumns

    frmData.InitTabGeneral frmData.tab_RUETAB
    frmData.InitTabForCedaConnection frmData.tab_RUETAB
    frmData.tab_RUETAB.AutoSizeColumns
    
    frmData.InitTabGeneral frmData.tab_RPFTAB
    frmData.InitTabForCedaConnection frmData.tab_RPFTAB
    frmData.tab_RPFTAB.AutoSizeColumns
    
    frmData.InitTabGeneral frmData.tab_PFCTAB
    frmData.InitTabForCedaConnection frmData.tab_PFCTAB
    frmData.tab_PFCTAB.AutoSizeColumns
    
    frmData.InitTabGeneral frmData.tab_AFTTAB
    frmData.InitTabForCedaConnection frmData.tab_AFTTAB
    frmData.tab_AFTTAB.AutoSizeColumns
    
    frmData.InitTabGeneral frmData.tab_AFTTABPLAN
    frmData.InitTabForCedaConnection frmData.tab_AFTTABPLAN
    frmData.tab_AFTTABPLAN.AutoSizeColumns

    frmData.InitTabGeneral tab_JOBTABPOOL
    frmData.InitTabForCedaConnection tab_JOBTABPOOL
    tab_JOBTABPOOL.AutoSizeColumns
    
    frmData.InitTabGeneral tab_JOBTABTEMP
    frmData.InitTabForCedaConnection tab_JOBTABTEMP
    tab_JOBTABTEMP.AutoSizeColumns
    
    frmData.InitTabGeneral tab_JOBTABDEM
    frmData.InitTabForCedaConnection tab_JOBTABDEM
    tab_JOBTABDEM.AutoSizeColumns
    
    frmData.InitTabGeneral tab_DEMTABLIVE
    frmData.InitTabForCedaConnection tab_DEMTABLIVE
    tab_DEMTABLIVE.AutoSizeColumns
    
    frmData.InitTabGeneral tab_DEMTABPLAN
    frmData.InitTabForCedaConnection tab_DEMTABPLAN
    tab_DEMTABPLAN.AutoSizeColumns
    
    frmData.InitTabGeneral frmData.tab_JOBTAB
    frmData.InitTabForCedaConnection frmData.tab_JOBTAB
    frmData.tab_JOBTAB.AutoSizeColumns
    
End Sub

'This Function written to prepare Workload Report & export to Excel
Sub prepareWorkloadReport()
    Dim i As Long
    Dim l As Long
    Dim llLineCount As Long
    Dim orgCount As Long
    Dim fromDt As String
    Dim toDt As String
    Dim orgName As String
    Dim orgUrno As String
    Dim jobStrWhere As String
    Dim urnoPoolJobs As String
    Dim strkey As String
    Dim poolJobList As String
    Dim delHrs As Long
    Dim brkHrs As Long
    Dim brkHrsTemp As Long
    Dim tmpHrs As Long
    Dim realHrs As Long
    Dim availHrs As Long
    Dim poolHrs As Long
    Dim numShifts As Long
    Dim prevUdsr As String
    Dim nextUdsr As String
    Dim ujtyVal As String
    Dim realCount As Long
    Dim utilVal As Double
    Dim FileNumber
    Dim poolUrno As String
    Dim tempPoolUrno As String
    Dim poolUrnos As String
    Dim poolCount As Integer
    Dim j As Integer

    Dim fromDt1 As String
    Dim toDt1 As String
    Dim toDt2 As String
    Dim xlapp
    Dim xlbook
    Dim xlsheet
    Dim xlRow As Integer
    
    'Get the fromDate & time, as well as toDate & time to be used in preparing query.
    'fromDt = Mid(fromDate, 7, 4) + Mid(fromDate, 4, 2) + Mid(fromDate, 1, 2) + Mid(fromTime, 1, 2) + Mid(fromTime, 4, 2) + "00"
    'toDt = Mid(toDate, 7, 4) + Mid(toDate, 4, 2) + Mid(toDate, 1, 2) + Mid(toTime, 1, 2) + Mid(toTime, 4, 2) + "00"

    ' Get the Input Date and Time Changed by KKH
    fromDate = Trim(Replace(fromDate, ".", ""))
    toDate = Trim(Replace(toDate, ".", ""))
    
    fromDt = Mid(fromDate, 5, 4) + Mid(fromDate, 3, 2) + Mid(fromDate, 1, 2) + Mid(fromTime, 1, 2) + Mid(fromTime, 4, 2) + "00"
    toDt = Mid(toDate, 5, 4) + Mid(toDate, 3, 2) + Mid(toDate, 1, 2) + Mid(toTime, 1, 2) + Mid(toTime, 4, 2) + "00"
    
    fromDt1 = Format(DateAdd("h", -gUTCOffset, CedaFullDateToVb(fromDt)), "yyyymmddhhmmss")
    toDt1 = Format(DateAdd("h", -gUTCOffset, CedaFullDateToVb(toDt)), "yyyymmddhhmmss")
    toDt2 = Format(DateAdd("d", 1, CedaFullDateToVb(toDt)), "yyyymmddhhmmss")
    
    'First get all the selected Employee pools
    'For each pool, need to create a separate line in the Workload report.
    orgCount = toList.ListCount
    
    'Added by KKH on 15/02/2007
    'OpenErr1 is invoked if user cancels from Open Dialog
    On Error GoTo OpenErr1
    
    If orgCount > 0 Then
        dlgSave.DialogTitle = "Save as Excel"
        dlgSave.Filter = "Excel (CSV)|*.csv"
        dlgSave.FileName = "Work Load Report"
        dlgSave.ShowSave

        Set xlapp = CreateObject("excel.application") 'Starts the Excel Session
        Set xlbook = xlapp.Workbooks.Add 'Add a Workbook
        Set xlsheet = xlbook.Worksheets.Item(1) 'Select a Sheet
        xlapp.Application.Visible = False
        
        If dlgSave.FileName <> "" Then
            
            'Prepare Heading of the report
            xlRow = 2
            'xlapp.Application.Cells(xlRow, 1).ColumnWidth = 100
            xlapp.Application.Cells(xlRow, 4).Font.Bold = True
            xlapp.Application.Cells(xlRow, 4).Value = "Work Load Report for time period from " & fromDate & " " & fromTime & " To " & toDate & " " & toTime
            xlapp.Application.Cells(xlRow, 7).Font.Bold = True
            xlapp.Application.Cells(xlRow, 7).Value = "Report Genearation Time, Start: " & Now
            xlapp.Application.Cells(xlRow, 4).HorizontalAlignment = xlCenter
            xlRow = xlRow + 2

            xlapp.Application.Cells(xlRow, 1).ColumnWidth = 20
            xlapp.Application.Cells(xlRow, 2).ColumnWidth = 15
            xlapp.Application.Cells(xlRow, 3).ColumnWidth = 11
            xlapp.Application.Cells(xlRow, 4).ColumnWidth = 22
            xlapp.Application.Cells(xlRow, 5).ColumnWidth = 18
            xlapp.Application.Cells(xlRow, 6).ColumnWidth = 23
            xlapp.Application.Cells(xlRow, 1).Font.Bold = True
            xlapp.Application.Cells(xlRow, 2).Font.Bold = True
            xlapp.Application.Cells(xlRow, 3).Font.Bold = True
            xlapp.Application.Cells(xlRow, 4).Font.Bold = True
            xlapp.Application.Cells(xlRow, 5).Font.Bold = True
            xlapp.Application.Cells(xlRow, 6).Font.Bold = True
            xlapp.Application.Cells(xlRow, 1).Value = "Org Unit Name"
            xlapp.Application.Cells(xlRow, 2).Value = "Number of Shifts"
            xlapp.Application.Cells(xlRow, 3).Value = "Actual Hours"
            xlapp.Application.Cells(xlRow, 4).Value = "Sum Of Hours Available"
            xlapp.Application.Cells(xlRow, 5).Value = "Sum of Hours Break"
            xlapp.Application.Cells(xlRow, 6).Value = "Utilization of Deployment"
            xlRow = xlRow + 1
           
            MousePointer = vbHourglass
            
            For l = 0 To orgCount - 1
                poolHrs = 0
                delHrs = 0
                brkHrs = 0
                tmpHrs = 0
                realHrs = 0
                availHrs = 0
                numShifts = 0
                prevUdsr = ""
                nextUdsr = ""
                poolJobList = ""
                poolUrnos = ""
                brkHrsTemp = 0
                
                'Get the org unit name from the ListBox
                orgName = toList.List(l)
                
                'Get the URNO of poolname from tab_POLTAB
                'poolUrno = frmData.tab_POLTAB.GetLinesByColumnValue(2, poolName, 0)
                'poolUrno = frmData.tab_POLTAB.GetColumnValues(poolUrno, 0)
                
                orgUrno = frmData.tab_ORGTAB.GetLinesByColumnValue(1, orgName, 0)
                orgUrno = frmData.tab_ORGTAB.GetColumnValues(orgUrno, 0)
                
                'MsgBox orgUrno
                
                poolUrno = frmData.tab_POATAB.GetLinesByColumnValue(1, orgUrno, 0)
                
                'MsgBox (poolUrno)
                poolCount = ItemCount(poolUrno, ",")
                
                'One or more pools might be assigned to each Orgunit. Need to check for those.
                If poolCount > 1 Then
                    For j = 1 To poolCount - 1 Step 1
                        tempPoolUrno = GetItem(poolUrno, j, ",")
                        tempPoolUrno = frmData.tab_POATAB.GetColumnValues(tempPoolUrno, 2)
                       
                        If IsNumeric(tempPoolUrno) Then
                            poolUrnos = poolUrnos + "'" + tempPoolUrno + "'"
                        End If
                        
                        If j < poolCount - 1 Then
                            poolUrnos = poolUrnos & ","
                        End If
                    Next j
                Else
                    'If there is only one pool available for org unit.
                    If IsNumeric(poolUrno) Then
                        tempPoolUrno = frmData.tab_POATAB.GetColumnValues(poolUrno, 2)
                        If IsNumeric(tempPoolUrno) Then
                            poolUrnos = "'" + tempPoolUrno + "'"
                        End If
                    End If
                End If
               
                
                'MsgBox poolUrnos
                'MousePointer = vbDefault
                'Exit Sub
                
                'MsgBox "Start :" & Now
                
                'Get all the Jobs from JOBTAB & Prepare a list for selected time frame
                'jobStrWhere = "where UAID='" + poolUrno + "' and UJTY='2007' and ACTO > '" + fromDt1 + "' and ACFR < '" + toDt1 + "' order by UDSR"
                'jobStrWhere = "where UAID='" + poolUrno + "' and UJTY='2007' and ACTO between '" + fromDt1 + "' and '" + toDt2 + "' and ACFR < '" + toDt1 + "' order by UDSR"
                jobStrWhere = "where UAID IN (" + poolUrnos + ") and UJTY='2007' and ACTO between '" + fromDt1 + "' and '" + toDt2 + "' and ACFR < '" + toDt1 + "' order by UDSR"
                'MsgBox jobStrWhere
                tab_JOBTABPOOL.ResetContent
                tab_JOBTABPOOL.Refresh
                frmData.LoadData tab_JOBTABPOOL, jobStrWhere
                
                'Once get all the jobs, prepare poolJobList
                'Prepare poolJobs & Number of Shifts
                llLineCount = tab_JOBTABPOOL.GetLineCount
                For i = 0 To llLineCount - 1 Step 1
                    'Prepare poolJoblist
                    strkey = tab_JOBTABPOOL.GetColumnValues(i, 1)
                    'poolJobList = poolJobList & "'" & strkey & "'"
                    poolJobList = poolJobList & strkey
                    If i < llLineCount - 1 Then
                        poolJobList = poolJobList & ","
                    End If
                    
                    nextUdsr = tab_JOBTABPOOL.GetColumnValues(i, 0)
                    If prevUdsr <> nextUdsr Then numShifts = numShifts + 1
                    prevUdsr = nextUdsr
                    
                    'Prepare the poolJobHours
                    poolHrs = poolHrs + DateDiff("n", CedaFullDateToVb(tab_JOBTABPOOL.GetColumnValues(i, 2)), CedaFullDateToVb(tab_JOBTABPOOL.GetColumnValues(i, 3)))
                Next i
                
                'MsgBox "poolHrs First : " & poolHrs
            
                'To retrieve breaks
                'jobStrWhere = "where JOUR in (" & poolJobList & ") and UJTY ='2011' and ACTO > '" + fromDt + "' and ACFR < '" + toDt + "' "
                jobStrWhere = "where UJTY ='2011' and ACTO between '" + fromDt1 + "' and '" + toDt2 + "' and ACFR < '" + toDt1 + "' "
                tab_JOBTABTEMP.ResetContent
                tab_JOBTABTEMP.Refresh
                frmData.LoadData tab_JOBTABTEMP, jobStrWhere
                
                'Calculate break hours
                llLineCount = tab_JOBTABTEMP.GetLineCount
                For i = 0 To llLineCount - 1 Step 1
                    If InStr(1, poolJobList, tab_JOBTABTEMP.GetColumnValues(i, 3)) > 0 Then
                        brkHrsTemp = DateDiff("n", CedaFullDateToVb(tab_JOBTABTEMP.GetColumnValues(i, 1)), CedaFullDateToVb(tab_JOBTABTEMP.GetColumnValues(i, 2)))
                        'brkHrs = brkHrs + DateDiff("h", CedaFullDateToVb(tab_JOBTABTEMP.GetColumnValues(i, 1)), CedaFullDateToVb(tab_JOBTABTEMP.GetColumnValues(i, 2)))
                        If (brkHrsTemp <= 120) Then
                            brkHrs = brkHrs + brkHrsTemp
                        Else
                            poolHrs = poolHrs - brkHrsTemp
                        End If
                    End If
                Next i
                
                'MsgBox "poolHrs First : " & poolHrs
                'MsgBox " Breakhrs : " & brkHrs
                
                'To retrieve delegations
                'jobStrWhere = "where JOUR in (" & poolJobList & ") and UJTY ='2011' and ACTO > '" + fromDt + "' and ACFR < '" + toDt + "' "
                jobStrWhere = "where UJTY in ('2008','2016') and ACTO between '" + fromDt1 + "' and '" + toDt2 + "' and ACFR < '" + toDt1 + "' "
                tab_JOBTABTEMP.ResetContent
                tab_JOBTABTEMP.Refresh
                frmData.LoadData tab_JOBTABTEMP, jobStrWhere
                
                'Calculate break hours
                llLineCount = tab_JOBTABTEMP.GetLineCount
                For i = 0 To llLineCount - 1 Step 1
                    If InStr(1, poolJobList, tab_JOBTABTEMP.GetColumnValues(i, 3)) > 0 Then
                        delHrs = delHrs + DateDiff("n", CedaFullDateToVb(tab_JOBTABTEMP.GetColumnValues(i, 1)), CedaFullDateToVb(tab_JOBTABTEMP.GetColumnValues(i, 2)))
                    End If
                Next i
                
                'To retrieve temporary absences
                'jobStrWhere = "where JOUR in (" & poolJobList & ") and UJTY ='2011' and ACTO > '" + fromDt + "' and ACFR < '" + toDt + "' "
                jobStrWhere = "where UJTY = '2015'  and ACTO between '" + fromDt1 + "' and '" + toDt2 + "' and ACFR < '" + toDt1 + "' "
                tab_JOBTABTEMP.ResetContent
                tab_JOBTABTEMP.Refresh
                frmData.LoadData tab_JOBTABTEMP, jobStrWhere
                
                'Calculate break hours
                llLineCount = tab_JOBTABTEMP.GetLineCount
                For i = 0 To llLineCount - 1 Step 1
                    If InStr(1, poolJobList, tab_JOBTABTEMP.GetColumnValues(i, 3)) > 0 Then
                        tmpHrs = tmpHrs + DateDiff("n", CedaFullDateToVb(tab_JOBTABTEMP.GetColumnValues(i, 1)), CedaFullDateToVb(tab_JOBTABTEMP.GetColumnValues(i, 2)))
                    End If
                Next i
                
                'To retrieve real jobs
                'jobStrWhere = "where JOUR in (" & poolJobList & ") and UJTY not in ('2007','2008','2016','2011','2015','2019') and ACTO > '" + fromDt + "' and ACFR < '" + toDt + "' "
                jobStrWhere = "where UJTY not in ('2007','2008','2016','2011','2015','2019') and ACTO between '" + fromDt1 + "' and '" + toDt2 + "' and ACFR < '" + toDt1 + "' "
                tab_JOBTABTEMP.ResetContent
                tab_JOBTABTEMP.Refresh
                frmData.LoadData tab_JOBTABTEMP, jobStrWhere
                
                'Calculate real jobs hours
                llLineCount = tab_JOBTABTEMP.GetLineCount
                For i = 0 To llLineCount - 1 Step 1
                    If InStr(1, poolJobList, tab_JOBTABTEMP.GetColumnValues(i, 3)) > 0 Then
                        realHrs = realHrs + DateDiff("n", CedaFullDateToVb(tab_JOBTABTEMP.GetColumnValues(i, 1)), CedaFullDateToVb(tab_JOBTABTEMP.GetColumnValues(i, 2)))
                    End If
                Next i
                
                availHrs = poolHrs - delHrs - tmpHrs - brkHrs
                If realHrs <> 0 And availHrs <> 0 Then utilVal = (realHrs / availHrs) * 100
                
                'MsgBox "End :" & Now
            
                
                xlapp.Application.Cells(xlRow, 1).Value = orgName
                xlapp.Application.Cells(xlRow, 2).Value = numShifts
                xlapp.Application.Cells(xlRow, 3).Value = Int(realHrs / 60)
                xlapp.Application.Cells(xlRow, 4).Value = Int(availHrs / 60)
                xlapp.Application.Cells(xlRow, 5).Value = Int(brkHrs / 60)
                xlapp.Application.Cells(xlRow, 6).Value = Int(utilVal) & "%"
                'xlapp.Application.Cells(xlRow, 7).Value = utilVal
                xlRow = xlRow + 1

            Next l

            xlRow = xlRow + 1
            xlapp.Application.Cells(xlRow, 7).Value = "Report Genearation Time, End: " & Now
            xlapp.Application.Cells(xlRow, 7).Font.Bold = True
    
            MousePointer = vbDefault
            xlbook.SaveAs dlgSave.FileName
            xlbook.Close
            xlapp.Quit
            
            MsgBox "" & dlgSave.FileName & " created successfully"
        Else
            MsgBox "Please Enter filename to export"
        End If
    Else
        MsgBox "Please select poolname to prepare workload report"
    End If
OpenErr1:
    Exit Sub
End Sub

'This Function written to prepare Other Allocation Work Report & export to Excel
Sub prepareOtherAllocationWorkReport2()
    Dim i As Long
    Dim l As Long
    Dim k As Integer
    Dim j As Integer
    Dim llLineCount As Long
    Dim poolCount As Long
    Dim fromDt As String
    Dim toDt As String
    Dim toDt2 As String
    Dim templateName As String
    Dim templateUrno As String
    Dim templateLine As String
    Dim templateUrnoTemp As String
    Dim demStrWhere As String
    Dim urnoTemplateJobs As String
    Dim strkey As String
    Dim templateDemList As String
    Dim templateCount As Long
    Dim fltIndUrno As String
    Dim fltDepUrno As String
    Dim templateFlag As Boolean
    Dim demLiveCount As Long
    Dim demPlanCount As Long
    Dim demUrudList As String
    Dim fltIndList As String
    Dim fltIndCount As Long
    Dim tempLine As String
    Dim tempLinedem As String
    Dim FileNumber
    Dim ruleNameLen As Long
    
    Dim xlapp
    Dim xlbook
    Dim xlsheet
    Dim xlRow As Integer
    Dim realHrs As Long
    
    'KKH - 31/01/2007
    Dim textStr As String
    Dim textFCCO As String
    Dim jobStrWhere As String
    Dim templateLineCount As Long
    
    templateName = cmdTemplates.List(cmdTemplates.ListIndex)
    templateUrnoTemp = frmData.tab_TPLTAB.GetLinesByColumnValue(1, templateName, 0)
    
    If templateName = "" Then
        MsgBox "Please select a template"
        cmdTemplates.SetFocus
        MousePointer = vbDefault
        Exit Sub
    End If
    
       
    'Added by KKH on 15/02/2007
    'OpenErr1 is invoked if user cancels from Open Dialog
    On Error GoTo OpenErr1

    dlgSave.DialogTitle = "Save as Excel"
    'dlgSave.Filter = "Text | *.txt"
    dlgSave.Filter = "Excel (CSV)|*.csv"
    dlgSave.FileName = "Other Allocation Jobs Report"
    dlgSave.ShowSave
    
    Set xlapp = CreateObject("excel.application") 'Starts the Excel Session
    Set xlbook = xlapp.Workbooks.Add 'Add a Workbook
    Set xlsheet = xlbook.Worksheets.Item(1) 'Select a Sheet
    xlapp.Application.Visible = False
        
    
    If dlgSave.FileName <> "" Then
    
        MousePointer = vbHourglass
        
        'Get the fromDate & time, as well as toDate & time to be used in preparing query.
        'fromDt = Mid(fromDate, 7, 4) + Mid(fromDate, 4, 2) + Mid(fromDate, 1, 2) + Mid(fromTime, 1, 2) + Mid(fromTime, 4, 2) + "00"
        'toDt = Mid(toDate, 7, 4) + Mid(toDate, 4, 2) + Mid(toDate, 1, 2) + Mid(toTime, 1, 2) + Mid(toTime, 4, 2) + "00"
        
        ' Get the Input Date and Time Changed by KKH
        fromDate = Trim(Replace(fromDate, ".", ""))
        toDate = Trim(Replace(toDate, ".", ""))
        
        fromDt = Mid(fromDate, 5, 4) + Mid(fromDate, 3, 2) + Mid(fromDate, 1, 2) + Mid(fromTime, 1, 2) + Mid(fromTime, 4, 2) + "00"
        toDt = Mid(toDate, 5, 4) + Mid(toDate, 3, 2) + Mid(toDate, 1, 2) + Mid(toTime, 1, 2) + Mid(toTime, 4, 2) + "00"
        
'        fromDt = Format(DateAdd("h", -8, CedaFullDateToVb(fromDt)), "yyyymmddhhmmss")
'        toDt = Format(DateAdd("h", -8, CedaFullDateToVb(toDt)), "yyyymmddhhmmss")
'        toDt2 = Format(DateAdd("d", 1, CedaFullDateToVb(toDt)), "yyyymmddhhmmss")
        
        fromDt = Format(DateAdd("h", -gUTCOffset, CedaFullDateToVb(fromDt)), "yyyymmddhhmmss")
        toDt = Format(DateAdd("h", -gUTCOffset, CedaFullDateToVb(toDt)), "yyyymmddhhmmss")
        toDt2 = Format(DateAdd("d", 1, CedaFullDateToVb(toDt)), "yyyymmddhhmmss")

        'Prepare Heading of the report
        xlRow = 1
        'xlapp.Application.Cells(xlRow, 1).ColumnWidth = 100
        xlapp.Application.Cells(xlRow, 3).Font.Bold = True
        xlapp.Application.Cells(xlRow, 3).Value = "Other Allocation Work"
        xlapp.Application.Cells(xlRow, 5).Font.Bold = True
        xlapp.Application.Cells(xlRow, 5).Value = "Report Genearation Time, Start: " & Now
        xlapp.Application.Cells(xlRow, 3).HorizontalAlignment = xlCenter
        
        xlRow = xlRow + 2
        xlapp.Application.Cells(xlRow, 3).Font.Bold = True
        xlapp.Application.Cells(xlRow, 3).Value = "Other Allocation Work Report for Loaded Timeframe  from " & fromDate & "/" & fromTime & " To " & toDate & "/" & toTime & "     Template: " & templateName
        xlapp.Application.Cells(xlRow, 3).HorizontalAlignment = xlCenter
        xlRow = xlRow + 2
        
        xlapp.Application.Cells(xlRow, 1).ColumnWidth = 20
        xlapp.Application.Cells(xlRow, 2).ColumnWidth = 30
        xlapp.Application.Cells(xlRow, 3).ColumnWidth = 25
        xlapp.Application.Cells(xlRow, 4).ColumnWidth = 25
        xlapp.Application.Cells(xlRow, 5).ColumnWidth = 18
        xlapp.Application.Cells(xlRow, 1).Font.Bold = True
        xlapp.Application.Cells(xlRow, 2).Font.Bold = True
        xlapp.Application.Cells(xlRow, 3).Font.Bold = True
        xlapp.Application.Cells(xlRow, 4).Font.Bold = True
        xlapp.Application.Cells(xlRow, 5).Font.Bold = True
        xlapp.Application.Cells(xlRow, 1).Value = "Template Name"
        xlapp.Application.Cells(xlRow, 2).Value = "Job Name"
        xlapp.Application.Cells(xlRow, 3).Value = "Actual Job Duration(Hrs)"
        xlapp.Application.Cells(xlRow, 4).Value = "Actual Job Duration(Min)"
        xlapp.Application.Cells(xlRow, 5).Value = "Function Code"
        xlRow = xlRow + 1
    
        MousePointer = vbHourglass
        'Get the template selected
        templateName = cmdTemplates.List(cmdTemplates.ListIndex)
        templateUrnoTemp = frmData.tab_TPLTAB.GetLinesByColumnValue(1, templateName, 0)

        'For every Template, there will be flight dependent & independent demands will be there. Need to check
        If ItemCount(templateUrnoTemp, ",") > 1 Then
            templateLineCount = ItemCount(templateUrnoTemp, ",")
            
            For k = 1 To templateLineCount Step 1
                templateLine = GetItem(templateUrnoTemp, k, ",")
                If frmData.tab_TPLTAB.GetColumnValues(templateLine, 2) = "RULE_AFT" Then
                    fltDepUrno = frmData.tab_TPLTAB.GetColumnValues(templateLine, 0)
                    templateFlag = True
                End If
            Next k
        Else
            If frmData.tab_TPLTAB.GetColumnValues(templateUrnoTemp, 2) = "RULE_AFT" Then
                fltDepUrno = frmData.tab_TPLTAB.GetColumnValues(templateUrnoTemp, 0)
                templateFlag = True
            End If
        End If
          
        If templateFlag Then
      
'          FileNumber = FreeFile   ' Get unused file number.
'          Open dlgSave.FileName For Append As #FileNumber    ' Create file name.
'          Print #FileNumber, "Demands for Loaded Timeframe  from " & fromDate & "/" & fromTime & " To " & toDate & "/" & toTime & "     Template: " & templateName
'          Print #FileNumber, "-------------------------------------------------------------------------------------------------------------"
'          Close #FileNumber
        
          'To retrieve real jobs
          'jobStrWhere = "where JOUR in (" & poolJobList & ") and UJTY not in ('2007','2008','2016','2011','2015','2019') and ACTO > '" + fromDt + "' and ACFR < '" + toDt + "' "
          jobStrWhere = "where UJTY not in ('2007','2008','2016','2011','2015','2019') and UTPL = '" + fltDepUrno + "' and UDEM = '0' and UAFT = '0' and ACTO between '" + fromDt + "' and '" + toDt2 + "' and ACFR < '" + toDt + "' order by FCCO, TEXT "
          Debug.Print jobStrWhere
          tab_JOBTABTEMP.ResetContent
          tab_JOBTABTEMP.Refresh
          frmData.LoadData tab_JOBTABTEMP, jobStrWhere
          Debug.Print jobStrWhere
          
          
          
          llLineCount = tab_JOBTABTEMP.GetLineCount
              
          For i = 0 To llLineCount - 1 Step 1
              textStr = tab_JOBTABTEMP.GetColumnValues(i, 4)
              textFCCO = tab_JOBTABTEMP.GetColumnValues(i, 5)
              realHrs = 0
              'If InStr(1, poolJobList, tab_JOBTABTEMP.GetColumnValues(i, 3)) > 0 Then
                  realHrs = DateDiff("n", CedaFullDateToVb(tab_JOBTABTEMP.GetColumnValues(i, 1)), CedaFullDateToVb(tab_JOBTABTEMP.GetColumnValues(i, 2)))
              'End If
              
              'tab_DEMTABSHOW.InsertTextLine orgName & "," & textStr & "," & Int(realHrs / 60) & "Hrs:" & (realHrs Mod 60) & "Min, True"
              xlapp.Application.Cells(xlRow, 1).Value = templateName
              xlapp.Application.Cells(xlRow, 2).Value = CleanString(textStr, FOR_CLIENT, True)
              'xlapp.Application.Cells(xlRow, 3).Value = Int(realHrs / 60) & "Hrs:" & (realHrs Mod 60) & "Min"
              xlapp.Application.Cells(xlRow, 3).Value = Int(realHrs / 60)
              xlapp.Application.Cells(xlRow, 4).Value = Int(realHrs Mod 60)
              xlapp.Application.Cells(xlRow, 5).Value = CleanString(textFCCO, FOR_CLIENT, True)
              xlRow = xlRow + 1
          Next i
        
            MousePointer = vbDefault
            
            xlRow = xlRow + 1
            xlapp.Application.Cells(xlRow, 5).Value = "Report Generation Time, End: " & Now
            xlapp.Application.Cells(xlRow, 5).Font.Bold = True
    
            MousePointer = vbDefault
            xlbook.SaveAs dlgSave.FileName
            xlbook.Close
            xlapp.Quit
            
            MsgBox "" & dlgSave.FileName & " created successfully"
        End If
    Else
        MsgBox "Please Enter filename to export"
    End If
OpenErr1:
    Exit Sub
End Sub
'This Function written to prepare Flight Jobs Without Demand Report & export to Excel
Sub prepareFlightJobsWithoutDemand()
    Dim i As Long
    Dim l As Long
    Dim k As Integer
    Dim j As Integer
    Dim k1 As Integer
    Dim llLineCount As Long
    Dim poolCount As Long
   
    Dim fromDt As String
    Dim toDt As String
    Dim fromDt1 As String
    Dim toDt1 As String
    Dim toDt2 As String
    Dim fromDt3 As String
    Dim toDt3 As String
    Dim fromDt2 As String
    
    Dim templateName As String
    Dim templateUrno As String
    Dim templateLine As String
    Dim templateUrnoTemp As String
    Dim aftStrWhere As String
    Dim urnoTemplateJobs As String
    Dim strkey As String
    Dim templateDemList As String
    Dim templateCount As Long
    Dim fltIndUrno As String
    Dim fltDepUrno As String
    Dim templateFlag As Boolean
    Dim demLiveCount As Long
    Dim demPlanCount As Long
    Dim demUrudList As String
    Dim fltIndList As String
    Dim fltIndCount As Long
    Dim tempLine As String
    Dim demShowVal As String
    Dim FileNumber
    Dim demShowCount As Long
    Dim jobUAFT As String
    Dim fltUrnoList As String
    
    Dim inBoundStr As String
    Dim outBoundStr As String
    Dim fltAcType As String
    Dim planFltAcType As String
    Dim fltLine As String
    Dim fltLine1 As String
    Dim fltAdid As String
    Dim fltStoa As String
    Dim fltStod As String
    Dim fltEtai As String
    Dim fltEtdi As String
    Dim fltCount As Long
    
    Dim xlapp
    Dim xlbook
    Dim xlsheet
    Dim xlRow As Integer
    Dim realHrs As Long
    Dim jobRow As String
    
    'KKH - 31/01/2007
    Dim textStr As String
    Dim textFCCO As String
    Dim jobStrWhere As String
    Dim templateLineCount As Long
    Dim tempDETY As Integer
    
    Dim jobCount As Long
    Dim jobDuration As Long
    Dim jobListInbound As String
    Dim p As Integer
    Dim aftRkey As String
    
    templateName = cmdTemplates.List(cmdTemplates.ListIndex)
    templateUrnoTemp = frmData.tab_TPLTAB.GetLinesByColumnValue(1, templateName, 0)
    
    If templateName = "" Then
        MsgBox "Please select a template"
        cmdTemplates.SetFocus
        MousePointer = vbDefault
        Exit Sub
    End If
      
    'Added by KKH on 15/02/2007
    'OpenErr1 is invoked if user cancels from Open Dialog
    On Error GoTo OpenErr1
    
    'if fromTime =
    dlgSave.DialogTitle = "Save as Excel"
    'dlgSave.Filter = "Text | *.txt"
    dlgSave.Filter = "Excel (CSV)|*.csv"
    dlgSave.FileName = "Flight Jobs Without Demand Report"
    dlgSave.ShowSave
    
    Set xlapp = CreateObject("excel.application") 'Starts the Excel Session
    Set xlbook = xlapp.Workbooks.Add 'Add a Workbook
    Set xlsheet = xlbook.Worksheets.Item(1) 'Select a Sheet
    xlapp.Application.Visible = False
        
    
    If dlgSave.FileName <> "" Then
    
        MousePointer = vbHourglass
        
        ' Get the Input Date and Time Changed by KKH
        fromDate = Trim(Replace(fromDate, ".", ""))
        toDate = Trim(Replace(toDate, ".", ""))
        
        fromDt = Mid(fromDate, 5, 4) + Mid(fromDate, 3, 2) + Mid(fromDate, 1, 2) + Mid(fromTime, 1, 2) + Mid(fromTime, 4, 2) + "00"
        toDt = Mid(toDate, 5, 4) + Mid(toDate, 3, 2) + Mid(toDate, 1, 2) + Mid(toTime, 1, 2) + Mid(toTime, 4, 2) + "00"
                
        fromDt1 = Format(DateAdd("d", -5, CedaFullDateToVb(fromDt)), "yyyymmddhhmmss")
        toDt1 = Format(DateAdd("d", 5, CedaFullDateToVb(toDt)), "yyyymmddhhmmss")

        fromDt3 = Format(DateAdd("d", -1, CedaFullDateToVb(fromDt)), "yyyymmddhhmmss")
        toDt3 = Format(DateAdd("d", 1, CedaFullDateToVb(toDt)), "yyyymmddhhmmss")
        
        fromDt = Format(DateAdd("h", -gUTCOffset, CedaFullDateToVb(fromDt)), "yyyymmddhhmmss")
        toDt = Format(DateAdd("h", -gUTCOffset, CedaFullDateToVb(toDt)), "yyyymmddhhmmss")
        toDt2 = Format(DateAdd("d", 1, CedaFullDateToVb(toDt)), "yyyymmddhhmmss")
        
        'Prepare Heading of the report
'        xlRow = 2
'        xlapp.Application.Cells(xlRow, 2).Font.Bold = True
'        xlapp.Application.Cells(xlRow, 2).Value = "Flight Jobs Without Demand Report for time period from " & fromDate & " " & fromTime & " To " & toDate & " " & toTime
'        xlapp.Application.Cells(xlRow, 12).Font.Bold = True
'        xlapp.Application.Cells(xlRow, 12).Value = "Report Generation Time, Start: " & Now
'        xlapp.Application.Cells(xlRow, 2).HorizontalAlignment = xlRight


        'Prepare Heading of the report
        xlRow = 1
        xlapp.Application.Cells(xlRow, 4).ColumnWidth = 100
        xlapp.Application.Cells(xlRow, 4).Font.Bold = True
        xlapp.Application.Cells(xlRow, 4).Value = "Flight Jobs Without Demands"
        xlapp.Application.Cells(xlRow, 8).Font.Bold = True
        xlapp.Application.Cells(xlRow, 8).Value = "Report Genearation Time, Start: " & Now
        
        xlRow = xlRow + 2
        xlapp.Application.Cells(xlRow, 5).Font.Bold = True
        xlapp.Application.Cells(xlRow, 5).Value = "Flight Jobs Without Demands for Loaded Timeframe  from " & fromDate & "/" & fromTime & " To " & toDate & "/" & toTime & "     Template: " & templateName
        xlapp.Application.Cells(xlRow, 5).HorizontalAlignment = xlCenter
        xlRow = xlRow + 2
        
        xlapp.Application.Cells(xlRow, 1).ColumnWidth = 15
        xlapp.Application.Cells(xlRow, 2).ColumnWidth = 15
        xlapp.Application.Cells(xlRow, 3).ColumnWidth = 15
        xlapp.Application.Cells(xlRow, 4).ColumnWidth = 15
        xlapp.Application.Cells(xlRow, 5).ColumnWidth = 15
        xlapp.Application.Cells(xlRow, 6).ColumnWidth = 15
        xlapp.Application.Cells(xlRow, 7).ColumnWidth = 15
        xlapp.Application.Cells(xlRow, 8).ColumnWidth = 15
        xlapp.Application.Cells(xlRow, 9).ColumnWidth = 15
        xlapp.Application.Cells(xlRow, 10).ColumnWidth = 25
        xlapp.Application.Cells(xlRow, 11).ColumnWidth = 25
        xlapp.Application.Cells(xlRow, 1).Font.Bold = True
        xlapp.Application.Cells(xlRow, 2).Font.Bold = True
        xlapp.Application.Cells(xlRow, 3).Font.Bold = True
        xlapp.Application.Cells(xlRow, 4).Font.Bold = True
        xlapp.Application.Cells(xlRow, 5).Font.Bold = True
        xlapp.Application.Cells(xlRow, 6).Font.Bold = True
        xlapp.Application.Cells(xlRow, 7).Font.Bold = True
        xlapp.Application.Cells(xlRow, 8).Font.Bold = True
        xlapp.Application.Cells(xlRow, 9).Font.Bold = True
        xlapp.Application.Cells(xlRow, 10).Font.Bold = True
        xlapp.Application.Cells(xlRow, 11).Font.Bold = True
        xlapp.Application.Cells(xlRow, 1).Value = "Arrival"
        xlapp.Application.Cells(xlRow, 2).Value = "Departure"
        xlapp.Application.Cells(xlRow, 3).Value = "Actual A/C"
        xlapp.Application.Cells(xlRow, 4).Value = "Planned A/C"
        xlapp.Application.Cells(xlRow, 5).Value = "STOA"
        xlapp.Application.Cells(xlRow, 6).Value = "STOD"
        xlapp.Application.Cells(xlRow, 7).Value = "ETA"
        xlapp.Application.Cells(xlRow, 8).Value = "ETD"
        xlapp.Application.Cells(xlRow, 9).Value = "Actual Jobs"
        xlapp.Application.Cells(xlRow, 10).Value = "Actual Jobs Duration(Hrs)"
        xlapp.Application.Cells(xlRow, 11).Value = "Actual Jobs Duration(Min)"

        xlRow = xlRow + 1
    
        MousePointer = vbHourglass
        
        aftStrWhere = "where STOD between '" + fromDt1 + "' and '" + toDt1 + "' or STOA between '" + fromDt1 + "' and '" + toDt1 + "'"
        frmData.tab_AFTTAB.ResetContent
        frmData.tab_AFTTAB.Refresh
        frmData.LoadData frmData.tab_AFTTAB, aftStrWhere
            
        'Get the template selected
        templateName = cmdTemplates.List(cmdTemplates.ListIndex)
        templateUrnoTemp = frmData.tab_TPLTAB.GetLinesByColumnValue(1, templateName, 0)

        'For every Template, there will be flight dependent & independent demands will be there. Need to check
        If ItemCount(templateUrnoTemp, ",") > 1 Then
            templateLineCount = ItemCount(templateUrnoTemp, ",")
            
            For k = 1 To templateLineCount Step 1
                templateLine = GetItem(templateUrnoTemp, k, ",")
                If frmData.tab_TPLTAB.GetColumnValues(templateLine, 2) = "RULE_AFT" Then
                    fltDepUrno = frmData.tab_TPLTAB.GetColumnValues(templateLine, 0)
                    templateFlag = True
                End If
            Next k
        Else
            If frmData.tab_TPLTAB.GetColumnValues(templateUrnoTemp, 2) = "RULE_AFT" Then
                fltDepUrno = frmData.tab_TPLTAB.GetColumnValues(templateUrnoTemp, 0)
                templateFlag = True
            End If
        End If
          
        If templateFlag Then

            'To retrieve real jobs
            jobStrWhere = "where UJTY not in ('2007','2008','2016','2011','2015','2019') and UTPL = '" + fltDepUrno + "' and UDEM = '0' and UAFT <> '0' and DETY in ('0','1','2') and ACTO between '" + fromDt + "' and '" + toDt2 + "' and ACFR < '" + toDt + "'"
            tab_JOBTABTEMP.ResetContent
            tab_JOBTABTEMP.Refresh
            frmData.LoadData tab_JOBTABTEMP, jobStrWhere
            Debug.Print jobStrWhere
                    
            llLineCount = tab_JOBTABTEMP.GetLineCount
    
            tab_DEMTABSHOW.ResetContent
            tab_DEMTABSHOW.Refresh
                    
            
            For i = 0 To llLineCount - 1 Step 1
                  
                tempDETY = tab_JOBTABTEMP.GetColumnValues(i, 7)
                
                inBoundStr = ""
                outBoundStr = ""
                fltAcType = ""
                jobCount = 0
                jobDuration = 0
                jobListInbound = 0
                aftRkey = ""
                
                'If its inbound job
                If tempDETY = "1" Then
                    jobUAFT = tab_JOBTABTEMP.GetColumnValues(i, 6)
        
                    If InStr(1, fltUrnoList, jobUAFT) = 0 Then
                        fltUrnoList = fltUrnoList & jobUAFT & ","
            
                        'Get the information for inbound flight
                        fltLine = frmData.tab_AFTTAB.GetLinesByColumnValue(0, jobUAFT, 0)
                        If fltLine <> "" Then
                            fltAdid = frmData.tab_AFTTAB.GetColumnValue(fltLine, 2)
                            fltAcType = frmData.tab_AFTTAB.GetColumnValue(fltLine, 10)
                            fltStoa = frmData.tab_AFTTAB.GetColumnValue(fltLine, 3)
                            fltEtai = frmData.tab_AFTTAB.GetColumnValue(fltLine, 11)
                            inBoundStr = frmData.tab_AFTTAB.GetColumnValue(fltLine, 1)
                            outBoundStr = ""
                            fltStod = ""
                            fltEtdi = ""
        
                             'Get the planned AC type from PLANNED AFTTAB
                            fltLine1 = frmData.tab_AFTTABPLAN.GetLinesByColumnValue(0, jobUAFT, 0)
                            If fltLine1 <> "" Then
                                planFltAcType = frmData.tab_AFTTABPLAN.GetColumnValue(jobUAFT, 10)
                            Else
                                planFltAcType = ""
                            End If
                        End If
                     End If
                         
                     If inBoundStr <> "" Then
                        'Get all the inbound demands for the flight
                        jobListInbound = tab_JOBTABTEMP.GetLinesByColumnValue(6, jobUAFT, 0)
                        jobCount = ItemCount(jobListInbound, ",")
    
                        For p = 0 To jobCount - 1 Step 1
                            jobRow = GetItem(jobListInbound, p, ",")
                            'If Mid(tab_JOBTABTEMP.GetColumnValue(jobRow, 3), 2, 1) <> "1" Then
                                jobDuration = jobDuration + DateDiff("n", CedaFullDateToVb(tab_JOBTABTEMP.GetColumnValues(p, 1)), CedaFullDateToVb(tab_JOBTABTEMP.GetColumnValues(p, 2)))
                            'End If
                        Next p
                        tab_DEMTABSHOW.InsertTextLine inBoundStr & "," & outBoundStr & "," & fltAcType & "," & planFltAcType & "," & fltStoa & "," & fltStod & "," & fltEtai & "," & fltEtdi & "," & jobCount & "," & Int(jobDuration / 60) & "," & Int(jobDuration Mod 60), True
                     End If
                 End If
                 
                 'If its outbound job
                 If tempDETY = "2" Then
                    jobUAFT = tab_JOBTABTEMP.GetColumnValues(i, 6)
        
                    If InStr(1, fltUrnoList, jobUAFT) = 0 Then
                        fltUrnoList = fltUrnoList & jobUAFT & ","
            
                        'Get the information for inbound flight
                        fltLine = frmData.tab_AFTTAB.GetLinesByColumnValue(0, jobUAFT, 0)
                        If fltLine <> "" Then
                            fltAdid = frmData.tab_AFTTAB.GetColumnValue(fltLine, 2)
                            fltAcType = frmData.tab_AFTTAB.GetColumnValue(fltLine, 10)
                            fltStoa = ""
                            fltEtai = ""
                            inBoundStr = ""
                            outBoundStr = frmData.tab_AFTTAB.GetColumnValue(fltLine, 1)
                            fltStod = frmData.tab_AFTTAB.GetColumnValue(fltLine, 4)
                            fltEtdi = frmData.tab_AFTTAB.GetColumnValue(fltLine, 12)
        
                             'Get the planned AC type from PLANNED AFTTAB
                            fltLine1 = frmData.tab_AFTTABPLAN.GetLinesByColumnValue(0, jobUAFT, 0)
                            If fltLine1 <> "" Then
                                planFltAcType = frmData.tab_AFTTABPLAN.GetColumnValue(jobUAFT, 10)
                            Else
                                planFltAcType = ""
                            End If
                        End If
                     End If
                         
                     If outBoundStr <> "" Then
                        'Get all the outbound demands for the flight
                        Debug.Print tab_JOBTABTEMP.GetLinesByColumnValue(6, jobUAFT, 1)
                        Debug.Print tab_JOBTABTEMP.GetLinesByColumnValues(6, jobUAFT, 0)
                        
                        Debug.Print tab_JOBTABTEMP.GetLinesByColumnValues(0, jobUAFT, 0)
                        Debug.Print tab_JOBTABTEMP.GetLinesByColumnValues(1, jobUAFT, 0)
                        Debug.Print tab_JOBTABTEMP.GetLinesByColumnValues(2, jobUAFT, 0)
                        Debug.Print tab_JOBTABTEMP.GetLinesByColumnValues(3, jobUAFT, 0)
                        Debug.Print tab_JOBTABTEMP.GetLinesByColumnValues(4, jobUAFT, 0)
                        Debug.Print tab_JOBTABTEMP.GetLinesByColumnValues(5, jobUAFT, 0)
                        Debug.Print tab_JOBTABTEMP.GetLinesByColumnValues(6, jobUAFT, 0)
                        Debug.Print tab_JOBTABTEMP.GetLinesByColumnValues(7, jobUAFT, 0)
                        Debug.Print tab_JOBTABTEMP.GetLinesByColumnValues(8, jobUAFT, 0)
                        
                        Debug.Print tab_JOBTABTEMP.GetLineCount
                        Debug.Print jobUAFT
                        jobListInbound = tab_JOBTABTEMP.GetLinesByColumnValue(6, jobUAFT, 1)
                        jobCount = ItemCount(jobListInbound, ",")
    
                        For p = 0 To jobCount - 1 Step 1
                            jobRow = GetItem(jobListInbound, p, ",")
                            'If Mid(tab_JOBTABTEMP.GetColumnValue(jobRow, 3), 2, 1) <> "1" Then
                                Debug.Print tab_JOBTABTEMP.GetColumnValues(p, 1)
                                Debug.Print tab_JOBTABTEMP.GetColumnValues(p, 2)
                                jobDuration = jobDuration + DateDiff("n", CedaFullDateToVb(tab_JOBTABTEMP.GetColumnValues(p, 1)), CedaFullDateToVb(tab_JOBTABTEMP.GetColumnValues(p, 2)))
                            'End If
                            
                        Next p
                        tab_DEMTABSHOW.InsertTextLine inBoundStr & "," & outBoundStr & "," & fltAcType & "," & planFltAcType & "," & fltStoa & "," & fltStod & "," & fltEtai & "," & fltEtdi & "," & jobCount & "," & Int(jobDuration / 60) & "," & Int(jobDuration Mod 60), True
                     End If
                 End If
                 
                 '----- pause here ----
                 'If its turn around demand
                If tempDETY = "0" Then
                    jobUAFT = tab_JOBTABTEMP.GetColumnValues(i, 6)

                    If InStr(1, fltUrnoList, jobUAFT) = 0 Then
                        'Get the information for outbound flight
                        fltLine = frmData.tab_AFTTAB.GetLinesByColumnValue(0, jobUAFT, 0)
                        If fltLine <> "" Then
                            fltAdid = frmData.tab_AFTTAB.GetColumnValue(fltLine, 2)
                            fltAcType = frmData.tab_AFTTAB.GetColumnValue(fltLine, 10)
                            fltStod = frmData.tab_AFTTAB.GetColumnValue(fltLine, 4)
                            fltEtdi = frmData.tab_AFTTAB.GetColumnValue(fltLine, 12)
                            outBoundStr = frmData.tab_AFTTAB.GetColumnValue(fltLine, 1)
                            aftRkey = frmData.tab_AFTTAB.GetColumnValue(fltLine, 13)
                        
                        Else
                            outBoundStr = ""
                            fltAcType = ""
                            fltStod = ""
                            fltEtdi = ""
                            fltAcType = ""
                        End If
    
                        'Get the information for inbound flight
                        fltLine = frmData.tab_AFTTAB.GetLinesByColumnValue(0, aftRkey, 0)
                        If fltLine <> "" Then
                            fltStoa = frmData.tab_AFTTAB.GetColumnValue(fltLine, 3)
                            fltEtai = frmData.tab_AFTTAB.GetColumnValue(fltLine, 11)
                            inBoundStr = frmData.tab_AFTTAB.GetColumnValuedd(fltLine, 1)
                        Else
                            fltStoa = ""
                            fltEtai = ""
                            inBoundStr = ""
                        End If
    
                        'Get the planned AC type from PLANNED AFTTAB
                        fltLine1 = frmData.tab_AFTTABPLAN.GetLinesByColumnValue(0, jobUAFT, 0)
                        If fltLine1 <> "" Then
                            planFltAcType = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine1, 10)
                        Else
                            planFltAcType = ""
                        End If
                    End If
               
    
                    'If flight is available, then only need to show the demands count, otherwise leave it.
                    If inBoundStr <> "" And outBoundStr <> "" Then
                        'Get all the inbound demands for the flight
                        jobListInbound = tab_JOBTABTEMP.GetLinesByColumnValue(6, jobUAFT, 0)
                        jobCount = ItemCount(jobListInbound, ",")

                        For p = 0 To jobCount - 1 Step 1
                            jobRow = GetItem(jobListInbound, p, ",")
                            'If Mid(tab_JOBTABTEMP.GetColumnValue(jobRow, 3), 2, 1) <> "1" Then
                                jobDuration = jobDuration + DateDiff("n", CedaFullDateToVb(tab_JOBTABTEMP.GetColumnValues(p, 1)), CedaFullDateToVb(tab_JOBTABTEMP.GetColumnValues(p, 2)))
                            'End If
                        Next p
                        tab_DEMTABSHOW.InsertTextLine inBoundStr & "," & outBoundStr & "," & fltAcType & "," & planFltAcType & "," & fltStoa & "," & fltStod & "," & fltEtai & "," & fltEtdi & "," & jobCount & "," & Int(jobDuration / 60) & "," & (jobDuration Mod 60), True
                    End If
                End If
                
                'tab_DEMTABSHOW.InsertTextLine inBoundStr & "," & outBoundStr & ", ," & planFltAcType & "," & fltStoa & "," & fltStod & "," & fltEtai & "," & fltEtdi & ",0,0Hrs:0Min,0,0,0Hrs:0Min," & demPlanRowCount & "," & Int(demPlanDemandDuration / 60) & "Hrs:" & (demPlanDemandDuration Mod 60) & "Min," & ruleNameStr & "," & planRuleNameStr, True
            Next i
'            'tab_DEMTABSHOW.InsertTextLine orgName & "," & textStr & "," & Int(realHrs / 60) & "Hrs:" & (realHrs Mod 60) & "Min, True"
'            xlapp.Application.Cells(xlRow, 1).Value = templateName
'            xlapp.Application.Cells(xlRow, 2).Value = CleanString(textStr, FOR_CLIENT, True)
'            'xlapp.Application.Cells(xlRow, 3).Value = Int(realHrs / 60)
'            Debug.Print xlapp.Application.Cells(xlRow, 2).Value
'            xlapp.Application.Cells(xlRow, 3).Value = Int(realHrs / 60) & "Hrs:" & (realHrs Mod 60) & "Min"
'            xlapp.Application.Cells(xlRow, 4).Value = CleanString(textFCCO, FOR_CLIENT, True)

               ' Prepare script here to display the rows in tab_DEMTABSHOW to excel sheet
               tab_DEMTABSHOW.Sort 4, True, False
               tab_DEMTABSHOW.Refresh
               demShowCount = tab_DEMTABSHOW.GetLineCount
    
               For k = 0 To demShowCount - 1 Step 1
                   For k1 = 0 To 10 Step 1
                       demShowVal = tab_DEMTABSHOW.GetColumnValues(k, k1)
                       xlapp.Application.Cells(xlRow, k1 + 1).HorizontalAlignment = xlLeft
                       If (k1 = 4 Or k1 = 5 Or k1 = 6 Or k1 = 7) And demShowVal <> "" Then
                           xlapp.Application.Cells(xlRow, k1 + 1).Value = CedaFullDateToVb(Format(DateAdd("h", gUTCOffset, CedaFullDateToVb(demShowVal)), "yyyymmddhhmmss"))
                           'xlapp.Application.Cells(xlRow, k1 + 1).Value = demShowVal
                       Else
                           xlapp.Application.Cells(xlRow, k1 + 1).Value = demShowVal
                       End If
                   Next k1
                   xlRow = xlRow + 1
               Next k
               
            xlRow = xlRow + 1
            MousePointer = vbDefault
                        
            xlRow = xlRow + 1
            xlapp.Application.Cells(xlRow, 8).Value = "Report Generation Time, End: " & Now
            xlapp.Application.Cells(xlRow, 8).Font.Bold = True
    
            MousePointer = vbDefault
            xlbook.SaveAs dlgSave.FileName
            xlbook.Close
            xlapp.Quit
            MsgBox "" & dlgSave.FileName & " created successfully"
        End If
    Else
        MsgBox "Please Enter filename to export"
    End If
OpenErr1:
    Exit Sub
End Sub
Sub prepareDemandCompareReport()
    Dim i As Long
    Dim l As Long
    Dim k As Integer
    Dim j As Integer
    Dim llLineCount As Long
    Dim poolCount As Long
    Dim fromDt As String
    Dim toDt As String
    Dim templateName As String
    Dim templateUrno As String
    Dim templateLine As String
    Dim templateUrnoTemp As String
    Dim demStrWhere As String
    Dim urnoTemplateJobs As String
    Dim strkey As String
    Dim templateDemList As String
    Dim templateCount As Long
    Dim fltIndUrno As String
    Dim fltDepUrno As String
    Dim templateFlag As Boolean
    Dim demLiveCount As Long
    Dim demPlanCount As Long
    Dim demUrudList As String
    Dim fltIndList As String
    Dim fltIndCount As Long
    Dim tempLine As String
    Dim tempLinedem As String
    Dim tempUrud As String
    Dim tempUrue As String
    Dim urudCount As Long
    Dim ruleName As String
    Dim ruleUrno As String
    Dim ruleUrnoList As String
    Dim ruleCount As String
    Dim demRows As String
    Dim demCount As Long
    Dim demRowCount As Long
    Dim demandDuration As Long
    Dim sumDemRowCount As Long
    Dim sumDemandDuration As Long
    Dim FileNumber
    Dim ruleNameLen As Long
    
    
    dlgSave.DialogTitle = "Save as Excel"
    dlgSave.Filter = "Text | *.txt"
    dlgSave.FileName = "Demand Comparison Report"
    dlgSave.ShowSave
    
    If dlgSave.FileName <> "" Then
    
        MousePointer = vbHourglass
        
        'Get the fromDate & time, as well as toDate & time to be used in preparing query.
        'fromDt = Mid(fromDate, 7, 4) + Mid(fromDate, 4, 2) + Mid(fromDate, 1, 2) + Mid(fromTime, 1, 2) + Mid(fromTime, 4, 2) + "00"
        'toDt = Mid(toDate, 7, 4) + Mid(toDate, 4, 2) + Mid(toDate, 1, 2) + Mid(toTime, 1, 2) + Mid(toTime, 4, 2) + "00"
        
        ' Get the Input Date and Time Changed by KKH
        fromDate = Trim(Replace(fromDate, ".", ""))
        toDate = Trim(Replace(toDate, ".", ""))
        
        fromDt = Mid(fromDate, 5, 4) + Mid(fromDate, 3, 2) + Mid(fromDate, 1, 2) + Mid(fromTime, 1, 2) + Mid(fromTime, 4, 2) + "00"
        toDt = Mid(toDate, 5, 4) + Mid(toDate, 3, 2) + Mid(toDate, 1, 2) + Mid(toTime, 1, 2) + Mid(toTime, 4, 2) + "00"
        
        fromDt = Format(DateAdd("h", -8, CedaFullDateToVb(fromDt)), "yyyymmddhhmmss")
        toDt = Format(DateAdd("h", -8, CedaFullDateToVb(toDt)), "yyyymmddhhmmss")
        
        
        'First get all the selected Templates
        'For each Template, need to calculate flight dependent, flight independent & CCI demands
        templateCount = toList.ListCount
        
        For l = 0 To templateCount - 1
            templateName = toList.List(l)
            
            FileNumber = FreeFile   ' Get unused file number.
            Open dlgSave.FileName For Append As #FileNumber    ' Create file name.
            Print #FileNumber, "Demands for Loaded Timeframe  from " & fromDate & "/" & fromTime & " To " & toDate & "/" & toTime & "     Template: " & templateName
            Print #FileNumber, "-------------------------------------------------------------------------------------------------------------"
            Close #FileNumber
            
            'Get the URNO of template names from tab_TPLTAB
            templateUrnoTemp = frmData.tab_TPLTAB.GetLinesByColumnValue(1, templateName, 0)
            
            'For every Template, there will be flight dependent & independent demands will be there. Need to check
            If ItemCount(templateUrnoTemp, ",") > 1 Then
                templateLine = GetItem(templateUrnoTemp, 1, ",")
                If frmData.tab_TPLTAB.GetColumnValues(templateLine, 2) = "RULE_AFT" Then
                    fltDepUrno = frmData.tab_TPLTAB.GetColumnValues(templateLine, 0)
                    fltIndUrno = frmData.tab_TPLTAB.GetColumnValues(GetItem(templateUrnoTemp, 2, ","), 0)
                Else
                    fltIndUrno = frmData.tab_TPLTAB.GetColumnValues(templateLine, 0)
                    fltDepUrno = frmData.tab_TPLTAB.GetColumnValues(GetItem(templateUrnoTemp, 2, ","), 0)
                End If
                templateFlag = True
            Else
                'If there is only one row available for template.
                fltDepUrno = frmData.tab_TPLTAB.GetColumnValues(templateUrnoTemp, 0)
                templateFlag = False
            End If
            
            'Prepare demStrWhere to make a query on DEMTAB
            If templateFlag Then
                'demStrWhere = "where UTPL in ('" & fltDepUrno & "','" & fltIndUrno & "')  and DEEN > '" + fromDt + "' and DEBE < '" + toDt + "' "
                demStrWhere = "where UTPL in ('" & fltDepUrno & "','" & fltIndUrno & "')  and DEEN > '" + fromDt + "' and DEBE between '" + fromDt + "' and '" + toDt + "' "
            Else
                'demStrWhere = "where UTPL in ('" & fltDepUrno & "')  and DEEN > '" + fromDt + "' and DEBE < '" + toDt + "' "
                demStrWhere = "where UTPL in ('" & fltDepUrno & "')  and DEEN > '" + fromDt + "' and DEBE between '" + fromDt + "' and '" + toDt + "' "
            End If
            
            tab_DEMTABLIVE.ResetContent
            tab_DEMTABLIVE.Refresh
            frmData.LoadData tab_DEMTABLIVE, demStrWhere
            demLiveCount = tab_DEMTABLIVE.GetLineCount
            
            MsgBox demLiveCount
            'MsgBox ItemCount(tab_DEMTABLIVE.GetLinesByColumnValue(2, "6", 0), ",")
            
            
            '   /*   Below script is to prepare Flight Independent demands   */
            'prepareFlightIndependentReport fltIndUrno
            
            '   /*   Below script is to prepare demand comparision based on Functions   */
            'prepareFunctionReport
            
            '   /*   Below script is to Flight dependent demands */
            'prepareFlightDependent fltDepUrno
            
            
        Next l
        
        MousePointer = vbDefault
    Else
        MsgBox "Please Enter filename to export"
    End If
        
End Sub

'This function prepares the demand comparision based on the FUNCTION NAMES
'More explantion need to be given
Sub prepareFunctionReport()

    Dim FileNumber
    Dim fktCodeLine As String
    Dim fktNameLine As String
    Dim actualDemandsLine As String
    Dim plannedDemandsLine As String
    Dim actualDemandDurationLine As String
    Dim plannedDemandDurationLine As String
    Dim sumDemandDuration As Long
    Dim sumDemRowCount As Long
    Dim i As Long
    Dim j As Integer
    Dim k As Integer
    Dim l As Integer
    Dim m As Integer
    Dim numDemands As Long
    Dim tempUrud As String
    Dim fktCode As String
    Dim fktName As String
    Dim fktList As String
    Dim tempFcco As String
    Dim fktCount As Long
    Dim demRows As String
    Dim demCount As Long
    Dim demRowCount As Long
    Dim demandDuration As Long
    Dim tempLinedem As Long
    Dim tempLine As String
    Dim fltIndUrno As String
    Dim fltDepUrno As String
    Dim templateName As String
    Dim templateLineCount As Long
    Dim templateUrnoTemp  As String
    Dim templateLine As String
    Dim templateFlag As Boolean
    Dim demStrWhere As String
    Dim templateCount As Long
    Dim fromDt As String
    Dim toDt As String
    Dim fromDt1 As String
    Dim demPlanCount As Long
    Dim demPlanRowCount As Long
    Dim demPlanDemandDuration As Long
    Dim sumPlanDemandDuration As Long
    Dim sumPlanDemRowCount As Long
    Dim fktListPlan As String

    Dim xlapp
    Dim xlbook
    Dim xlsheet
    Dim xlRow As Integer
    Dim x1RowStart As Integer
    Dim x1ColStart As Integer

    Dim jobStrWhere As String
    Dim jobDemCount As Long
    Dim jobDemDuration As Long
    Dim jobDemUsed As Long
    Dim sumJobDemCount As Long
    Dim sumJobDemDuration As Long
    Dim tempDemUrno As String
    Dim jobList As String
    Dim jobListCount As Integer
    Dim jobRow As String
    Dim fromDt3 As String
    Dim toDt3 As String
    
    templateCount = toList.ListCount

    'Added by KKH on 15/02/2007
    'OpenErr1 is invoked if user cancels from Open Dialog
    On Error GoTo OpenErr1
    
    If templateCount > 0 Then
        dlgSave.DialogTitle = "Save as Excel"
        dlgSave.Filter = "Excel (CSV)|*.csv"
        dlgSave.FileName = "Flight Function Comparision Demands Report"
        dlgSave.ShowSave

        Set xlapp = CreateObject("excel.application") 'Starts the Excel Session
        Set xlbook = xlapp.Workbooks.Add 'Add a Workbook
        Set xlsheet = xlbook.Worksheets.Item(1) 'Select a Sheet
        xlapp.Application.Visible = False
            
        If dlgSave.FileName <> "" Then
            
            MousePointer = vbHourglass

            'Prepare Heading of the report
            xlRow = 1
            xlapp.Application.Cells(xlRow, 1).ColumnWidth = 100
            xlapp.Application.Cells(xlRow, 1).Font.Bold = True
            xlapp.Application.Cells(xlRow, 1).Value = "Actual Vs Planned Report Based on Functions: Loaded Timeframe  from " & fromDate & "/" & fromTime & " To " & toDate & "/" & toTime
            xlRow = xlRow + 1
            xlapp.Application.Cells(xlRow, 4).Font.Bold = True
            xlapp.Application.Cells(xlRow, 4).Value = "Report Genearation Time, Start: " & Now
            xlRow = xlRow + 2
            
    
            'Get the fromDate & time, as well as toDate & time to be used in preparing query.
            'fromDt = Mid(fromDate, 7, 4) + Mid(fromDate, 4, 2) + Mid(fromDate, 1, 2) + Mid(fromTime, 1, 2) + Mid(fromTime, 4, 2) + "00"
            'toDt = Mid(toDate, 7, 4) + Mid(toDate, 4, 2) + Mid(toDate, 1, 2) + Mid(toTime, 1, 2) + Mid(toTime, 4, 2) + "00"

            ' Get the Input Date and Time Changed by KKH
            fromDate = Trim(Replace(fromDate, ".", ""))
            toDate = Trim(Replace(toDate, ".", ""))
            
            fromDt = Mid(fromDate, 5, 4) + Mid(fromDate, 3, 2) + Mid(fromDate, 1, 2) + Mid(fromTime, 1, 2) + Mid(fromTime, 4, 2) + "00"
            toDt = Mid(toDate, 5, 4) + Mid(toDate, 3, 2) + Mid(toDate, 1, 2) + Mid(toTime, 1, 2) + Mid(toTime, 4, 2) + "00"
           
            fromDt3 = Format(DateAdd("d", -1, CedaFullDateToVb(fromDt)), "yyyymmddhhmmss")
            toDt3 = Format(DateAdd("d", 1, CedaFullDateToVb(toDt)), "yyyymmddhhmmss")
            
            fromDt = Format(DateAdd("h", -gUTCOffset, CedaFullDateToVb(fromDt)), "yyyymmddhhmmss")
            toDt = Format(DateAdd("h", -gUTCOffset, CedaFullDateToVb(toDt)), "yyyymmddhhmmss")
            fromDt1 = Format(DateAdd("d", -1, CedaFullDateToVb(fromDt)), "yyyymmddhhmmss")
            
            
            'First get all the selected Templates
            'For each Template, need to calculate flight dependent, flight independent & CCI demands
            
            
            For l = 0 To templateCount - 1 Step 1
                templateName = toList.List(l)
                templateFlag = False
                
                'Get the URNO of template names from tab_TPLTAB
                templateUrnoTemp = frmData.tab_TPLTAB.GetLinesByColumnValue(1, templateName, 0)
                
                'For every Template, there will be flight dependent & independent demands will be there. Need to check
                If ItemCount(templateUrnoTemp, ",") > 1 Then
                    templateLineCount = ItemCount(templateUrnoTemp, ",")
                    
                    For k = 1 To templateLineCount Step 1
                        templateLine = GetItem(templateUrnoTemp, k, ",")
                        If frmData.tab_TPLTAB.GetColumnValues(templateLine, 2) = "RULE_FIR" Then
                            fltIndUrno = frmData.tab_TPLTAB.GetColumnValues(templateLine, 0)
                            templateFlag = True
                        End If
                        If frmData.tab_TPLTAB.GetColumnValues(templateLine, 2) = "RULE_AFT" Then
                            fltDepUrno = frmData.tab_TPLTAB.GetColumnValues(templateLine, 0)
                            templateFlag = True
                        End If
                    Next k
                Else
                    If frmData.tab_TPLTAB.GetColumnValues(templateUrnoTemp, 2) = "RULE_FIR" Then
                        fltIndUrno = frmData.tab_TPLTAB.GetColumnValues(templateUrnoTemp, 0)
                        templateFlag = True
                    End If
                    If frmData.tab_TPLTAB.GetColumnValues(templateLine, 2) = "RULE_AFT" Then
                        fltDepUrno = frmData.tab_TPLTAB.GetColumnValues(templateUrnoTemp, 0)
                        templateFlag = True
                    End If
                End If
                    
                If templateFlag Then

                    'Prepare Excel settings properly
                    xlapp.Application.Cells(xlRow, 1).ColumnWidth = 100
                    xlapp.Application.Cells(xlRow, 1).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 1).Value = "Flight Demands Based on Function, for Loaded Timeframe  from " & fromDate & "/" & fromTime & " To " & toDate & "/" & toTime & "     Template: " & templateName
                    'xlapp.Application.Cells(xlRow, 1).HorizontalAlignment = xlMiddle
                    xlRow = xlRow + 2

                    x1RowStart = xlRow
                    x1ColStart = 2
            
                    xlapp.Application.Cells(xlRow, 1).ColumnWidth = 25
                    xlapp.Application.Cells(xlRow, 1).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 1).Value = "Function Code"
                    xlRow = xlRow + 1
            
                    xlapp.Application.Cells(xlRow, 1).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 1).Value = "Function Name"
                    xlRow = xlRow + 1

                    xlapp.Application.Cells(xlRow, 1).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 1).Value = "Actual Jobs"
                    xlRow = xlRow + 1

                    xlapp.Application.Cells(xlRow, 1).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 1).Value = "Actual Jobs Duration"
                    xlRow = xlRow + 1

                    xlapp.Application.Cells(xlRow, 1).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 1).Value = "Actual Demands Used"
                    xlRow = xlRow + 1
            
                    xlapp.Application.Cells(xlRow, 1).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 1).Value = "Actual Demands"
                    xlRow = xlRow + 1

                    xlapp.Application.Cells(xlRow, 1).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 1).Value = "Planned Demands"
                    xlRow = xlRow + 1

                    xlapp.Application.Cells(xlRow, 1).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 1).Value = "Actual Demands Duration"
                    xlRow = xlRow + 1
            
                    xlapp.Application.Cells(xlRow, 1).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 1).Value = "Planned Demands Duration"
                    xlRow = xlRow + 1
         
                    If fltIndUrno <> "" Then
                        'demStrWhere = "where UTPL = '" & fltIndUrno & "'  and DEEN > '" + fromDt + "' and DEBE between '" + fromDt1 + "' and '" + toDt + "' "
                        demStrWhere = "where UTPL = '" & fltIndUrno & "' and RETY = '100' and DEEN > '" + fromDt + "' and DEBE between '" + fromDt1 + "' and '" + toDt + "' "
                        jobStrWhere = "where UTPL = '" & fltIndUrno & "' and ACTO between '" + fromDt3 + "' and '" + toDt3 + "' and ACFR < '" + toDt3 + "' "
                    End If

                    If fltDepUrno <> "" Then
                        'demStrWhere = "where UTPL = '" & fltDepUrno & "'  and DEEN > '" + fromDt + "' and DEBE between '" + fromDt1 + "' and '" + toDt + "' "
                        demStrWhere = "where UTPL = '" & fltDepUrno & "' and RETY = '100' and DEEN > '" + fromDt + "' and DEBE between '" + fromDt1 + "' and '" + toDt + "' "
                        jobStrWhere = "where UTPL = '" & fltDepUrno & "' and ACTO between '" + fromDt3 + "' and '" + toDt3 + "' and ACFR < '" + toDt3 + "' "
                    End If

                    If fltIndUrno <> "" And fltDepUrno <> "" Then
                        'demStrWhere = "where UTPL in ('" & fltIndUrno & "','" & fltDepUrno & "') and DEEN > '" + fromDt + "' and DEBE between '" + fromDt1 + "' and '" + toDt + "' "
                        demStrWhere = "where UTPL in ('" & fltIndUrno & "','" & fltDepUrno & "') and RETY = '100' and DEEN > '" + fromDt + "' and DEBE between '" + fromDt1 + "' and '" + toDt + "' "
                        jobStrWhere = "where UTPL in ('" & fltIndUrno & "','" & fltDepUrno & "')  and ACTO between '" + fromDt3 + "' and '" + toDt3 + "' and ACFR < '" + toDt3 + "' "
                    End If
                        
                    'Get the Flight Independent Demands from DEMTAB
                    tab_DEMTABLIVE.ResetContent
                    tab_DEMTABLIVE.Refresh
                    frmData.LoadData tab_DEMTABLIVE, demStrWhere
                    numDemands = tab_DEMTABLIVE.GetLineCount

                    'Get the Flight Independent Demands from DEMTAB
                    tab_DEMTABPLAN.ResetContent
                    tab_DEMTABPLAN.Refresh
                    frmData.LoadData tab_DEMTABPLAN, demStrWhere
                    demPlanCount = tab_DEMTABPLAN.GetLineCount

                    tab_JOBTABDEM.ResetContent
                    tab_JOBTABDEM.Refresh
                    frmData.LoadData tab_JOBTABDEM, jobStrWhere
                    'MsgBox tab_JOBTABDEM.GetLineCount

                    tab_DEMTABTEMP.ResetContent
                    tab_DEMTABTEMP.Refresh
                    sumDemandDuration = 0
                    sumDemRowCount = 0

                    tab_DEMTABPLANTEMP.ResetContent
                    tab_DEMTABPLANTEMP.Refresh
                    sumPlanDemandDuration = 0
                    sumPlanDemRowCount = 0
                    fktList = ""
                    fktListPlan = ""

                    'sumJobDemCount = 0
                    'sumJobDemDuration = 0
                    'sumJobDemUsed = 0


                    'Prepare distinct functions list based on URUD of RPFTAB - DEMTABLIVE
                    For j = 0 To numDemands - 1 Step 1
                        If Mid(tab_DEMTABLIVE.GetColumnValue(j, 3), 2, 1) <> "1" Then
                            tempUrud = tab_DEMTABLIVE.GetColumnValues(j, 8)
                            
                            'Get the rowvalue of tempUrud from RPFTAB & get the FCCO value to check in PFCTAB
                            If IsNumeric(frmData.tab_RPFTAB.GetLinesByColumnValue(0, tempUrud, 0)) Then
                                tempLine = frmData.tab_RPFTAB.GetLinesByColumnValue(0, tempUrud, 0)
                                tempFcco = Trim(frmData.tab_RPFTAB.GetColumnValue(tempLine, 1))
                                tab_DEMTABTEMP.InsertTextLine tab_DEMTABLIVE.GetColumnValues(j, "0,1,2,3,4,5,6,7,8,9") & "," & tempFcco, True
                                
                                If InStr(1, fktList, tempFcco) = 0 Then fktList = fktList & tempFcco & ","
                            Else
                                'MsgBox "Cant find RPFTAB entry ... URUD: " & tempUrud
                            End If
                        End If
                    Next j

                    
                    'Prepare distinct functions list based on URUD of RPFTAB - DEMTABPLAN
                    For j = 0 To demPlanCount - 1 Step 1
                        If Mid(tab_DEMTABPLAN.GetColumnValue(j, 3), 2, 1) <> "1" Then
                            tempUrud = tab_DEMTABPLAN.GetColumnValues(j, 8)
                            
                            'Get the rowvalue of tempUrud from RPFTAB & get the FCCO value to check in PFCTAB
                            If IsNumeric(frmData.tab_RPFTAB.GetLinesByColumnValue(0, tempUrud, 0)) Then
                                tempLine = frmData.tab_RPFTAB.GetLinesByColumnValue(0, tempUrud, 0)
                                tempFcco = Trim(frmData.tab_RPFTAB.GetColumnValue(tempLine, 1))
                                tab_DEMTABPLANTEMP.InsertTextLine tab_DEMTABPLAN.GetColumnValues(j, "0,1,2,3,4,5,6,7,8,9") & "," & tempFcco, True
                                
                                If InStr(1, fktListPlan, tempFcco) = 0 Then fktListPlan = fktListPlan & tempFcco & ","
                            Else
                                'MsgBox "Cant find RPFTAB entry ... URUD: " & tempUrud
                            End If
                        End If
                    Next
                    
                    fktCount = ItemCount(fktList, ",")
                    
                    For j = 1 To fktCount - 1 Step 1
                        tempFcco = GetItem(fktList, j, ",")
                        
                        'Get the function name & prepare the function code & function name strings
                        If IsNumeric(frmData.tab_PFCTAB.GetLinesByColumnValue(0, tempFcco, 0)) Then
                        
                            tempLine = frmData.tab_PFCTAB.GetLinesByColumnValue(0, tempFcco, 0)
                            fktName = frmData.tab_PFCTAB.GetColumnValues(tempLine, 1)

                            xlapp.Application.Cells(x1RowStart, x1ColStart).ColumnWidth = Len(Trim(fktName))
                            xlapp.Application.Cells(x1RowStart, x1ColStart).Value = tempFcco
                            xlapp.Application.Cells(x1RowStart + 1, x1ColStart).Value = Trim(fktName)
                            
                            'To get all the rows for the  ruleUrno
                            demRows = tab_DEMTABTEMP.GetLinesByColumnValue(10, tempFcco, 0)
                            demCount = ItemCount(demRows, ",")
                            demRowCount = 0
                            demandDuration = 0
                            jobDemCount = 0
                            jobDemDuration = 0
                            jobDemUsed = 0
                            
                            If demCount > 0 Then
                                For k = 1 To demCount Step 1
                                    'Prepare the demands duration
                                    tempLinedem = GetItem(demRows, k, ",")
                                    demRowCount = demRowCount + 1
                                    demandDuration = demandDuration + DateDiff("n", CedaFullDateToVb(tab_DEMTABTEMP.GetColumnValues(tempLinedem, 0)), CedaFullDateToVb(tab_DEMTABTEMP.GetColumnValues(tempLinedem, 1)))

                                    'To calculate actual jobs, need to check JOBTAB & get the details from JOBTAB.
                                    'Getting the urno for each demand & calculate the actual job times
                                    'KKH 1)
                                    tempDemUrno = tab_DEMTABLIVE.GetColumnValues(tempLinedem, 10)
                                    jobList = tab_JOBTABDEM.GetLinesByColumnValue(3, tempDemUrno, 0)
                                    jobListCount = ItemCount(jobList, ",")

                                    'if jobListCount is not zero, then calculate the actual job
                                    If jobListCount > 0 Then
                                        jobDemCount = jobDemCount + jobListCount
                                        jobDemUsed = jobDemUsed + 1
                                        'if more than one person worked for the same demand, need to calculate
                                        For m = 1 To jobListCount Step 1
                                            jobRow = GetItem(jobList, m, ",")
                                            jobDemDuration = jobDemDuration + DateDiff("n", CedaFullDateToVb(tab_JOBTABDEM.GetColumnValues(jobRow, 1)), CedaFullDateToVb(tab_JOBTABDEM.GetColumnValues(jobRow, 2)))
                                        Next m
                                    End If
                                    'End - calculation of actual jobs.
 
                                Next k
                            End If

                            'To get all the rows for the  tempFcco - DEMTABPLANTEMP
                            demRows = tab_DEMTABPLANTEMP.GetLinesByColumnValue(10, tempFcco, 0)
                            demCount = ItemCount(demRows, ",")
                            demPlanRowCount = 0
                            demPlanDemandDuration = 0

                            If demCount > 0 Then
                                For k = 1 To demCount Step 1
                                    'Prepare the demands duration
                                    tempLinedem = GetItem(demRows, k, ",")
                                    demPlanRowCount = demPlanRowCount + 1
                                    demPlanDemandDuration = demPlanDemandDuration + DateDiff("n", CedaFullDateToVb(tab_DEMTABPLANTEMP.GetColumnValues(tempLinedem, 0)), CedaFullDateToVb(tab_DEMTABPLANTEMP.GetColumnValues(tempLinedem, 1)))
                                Next k
                            End If
                            
                            xlapp.Application.Cells(x1RowStart + 2, x1ColStart).Value = jobDemCount
                            xlapp.Application.Cells(x1RowStart + 2, x1ColStart).HorizontalAlignment = xlLeft
                            xlapp.Application.Cells(x1RowStart + 3, x1ColStart).Value = Int(jobDemDuration / 60) & " Hrs : " & (jobDemDuration Mod 60) & " Min"
                            xlapp.Application.Cells(x1RowStart + 3, x1ColStart).HorizontalAlignment = xlLeft
                            xlapp.Application.Cells(x1RowStart + 4, x1ColStart).Value = jobDemUsed
                            xlapp.Application.Cells(x1RowStart + 4, x1ColStart).HorizontalAlignment = xlLeft
                            xlapp.Application.Cells(x1RowStart + 5, x1ColStart).Value = demRowCount
                            xlapp.Application.Cells(x1RowStart + 5, x1ColStart).HorizontalAlignment = xlLeft
                            xlapp.Application.Cells(x1RowStart + 6, x1ColStart).Value = demPlanRowCount
                            xlapp.Application.Cells(x1RowStart + 6, x1ColStart).HorizontalAlignment = xlLeft
                            xlapp.Application.Cells(x1RowStart + 7, x1ColStart).HorizontalAlignment = xlLeft
                            xlapp.Application.Cells(x1RowStart + 7, x1ColStart).Value = Int(demandDuration / 60) & " Hrs : " & (demandDuration Mod 60) & " Min"
                            xlapp.Application.Cells(x1RowStart + 8, x1ColStart).HorizontalAlignment = xlLeft
                            xlapp.Application.Cells(x1RowStart + 8, x1ColStart).Value = Int(demPlanDemandDuration / 60) & " Hrs : " & (demPlanDemandDuration Mod 60) & " Min"
                            
                            x1ColStart = x1ColStart + 1
                        Else
                            'MsgBox "No record available for FCCO: " & tempFcco & "Return Value is: " & frmData.tab_PFCTAB.GetLinesByColumnValue(0, tempFcco, 0)
                        End If
                    Next j


                    'If more functions available in planned demands when compared to actual demands, then
                    fktCount = ItemCount(fktListPlan, ",")
                    For j = 1 To fktCount - 1 Step 1
                        tempFcco = GetItem(fktListPlan, j, ",")

                        If InStr(1, fktList, tempFcco) = 0 Then
                            If IsNumeric(frmData.tab_PFCTAB.GetLinesByColumnValue(0, tempFcco, 0)) Then
                        
                                tempLine = frmData.tab_PFCTAB.GetLinesByColumnValue(0, tempFcco, 0)
                                fktName = frmData.tab_PFCTAB.GetColumnValues(tempLine, 1)
                                
                                xlapp.Application.Cells(x1RowStart, x1ColStart).ColumnWidth = Len(Trim(fktName))
                                xlapp.Application.Cells(x1RowStart, x1ColStart).Value = tempFcco
                                xlapp.Application.Cells(x1RowStart + 1, x1ColStart).Value = Trim(fktName)

                                'To get all the rows for the  tempFcco - DEMTABPLANTEMP
                                demRows = tab_DEMTABPLANTEMP.GetLinesByColumnValue(10, tempFcco, 0)
                                demCount = ItemCount(demRows, ",")
                                demPlanRowCount = 0
                                demPlanDemandDuration = 0

                                If demCount > 0 Then
                                    For k = 1 To demCount Step 1
                                        'Prepare the demands duration
                                        tempLinedem = GetItem(demRows, k, ",")
                                        demPlanRowCount = demPlanRowCount + 1
                                        demPlanDemandDuration = demPlanDemandDuration + DateDiff("n", CedaFullDateToVb(tab_DEMTABPLANTEMP.GetColumnValues(tempLinedem, 0)), CedaFullDateToVb(tab_DEMTABPLANTEMP.GetColumnValues(tempLinedem, 1)))
                                    Next k
                                End If
                            
                                xlapp.Application.Cells(x1RowStart + 2, x1ColStart).Value = 0
                                xlapp.Application.Cells(x1RowStart + 2, x1ColStart).HorizontalAlignment = xlLeft
                                xlapp.Application.Cells(x1RowStart + 3, x1ColStart).Value = 0
                                xlapp.Application.Cells(x1RowStart + 3, x1ColStart).HorizontalAlignment = xlLeft
                                xlapp.Application.Cells(x1RowStart + 4, x1ColStart).Value = 0
                                xlapp.Application.Cells(x1RowStart + 4, x1ColStart).HorizontalAlignment = xlLeft
                                xlapp.Application.Cells(x1RowStart + 5, x1ColStart).Value = 0
                                xlapp.Application.Cells(x1RowStart + 5, x1ColStart).HorizontalAlignment = xlLeft
                                xlapp.Application.Cells(x1RowStart + 6, x1ColStart).Value = demPlanRowCount
                                xlapp.Application.Cells(x1RowStart + 6, x1ColStart).HorizontalAlignment = xlLeft
                                xlapp.Application.Cells(x1RowStart + 7, x1ColStart).Value = 0
                                xlapp.Application.Cells(x1RowStart + 7, x1ColStart).HorizontalAlignment = xlLeft
                                xlapp.Application.Cells(x1RowStart + 8, x1ColStart).HorizontalAlignment = xlLeft
                                xlapp.Application.Cells(x1RowStart + 8, x1ColStart).Value = Int(demPlanDemandDuration / 60) & " Hrs : " & (demPlanDemandDuration Mod 60) & " Min"
                                xlapp.Application.Cells(x1RowStart + 5, x1ColStart).HorizontalAlignment = xlLeft
                                x1ColStart = x1ColStart + 1
                            End If
                        End If
                    Next j
                    
                Else 'Template Flag
                    xlapp.Application.Cells(xlRow, 1).ColumnWidth = 100
                    xlapp.Application.Cells(xlRow, 1).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 1).Value = " , Function Demands wont be available for this template :" & templateName
                    xlRow = xlRow + 1
                End If
                xlRow = xlRow + 2
             Next l
            
             xlRow = xlRow + 1
             xlapp.Application.Cells(xlRow, 4).Value = "Report Genearation Time, End: " & Now
             xlapp.Application.Cells(xlRow, 4).Font.Bold = True

             xlbook.SaveAs dlgSave.FileName
             xlbook.Close
             xlapp.Quit
            
             MsgBox "" & dlgSave.FileName & " created successfully"
        Else
             MsgBox "Please Enter Filename to upload Flight Independent Report"
        End If
    Else
         MsgBox "Please Select the templates"
    End If
OpenErr1:
    Exit Sub
End Sub
'This function prepares Demand comparision reports based on Flight Independent Rules
Sub prepareFlightIndependentReport()
    
    Dim FileNumber
    Dim i As Long
    Dim k As Integer
    Dim j As Integer
    Dim l As Integer
    Dim m As Integer
    Dim urnoTemplateJobs As String
    Dim strkey As String
    Dim fltIndList As String
    Dim fltIndCount As Long
    Dim tempLine As String
    Dim tempLinedem As String
    Dim tempUrud As String
    Dim tempUrue As String
    Dim urudCount As Long
    Dim ruleName As String
    Dim ruleUrno As String
    Dim ruleUrnoList As String
    Dim ruleCount As String
    Dim demRows As String
    Dim demCount As Long
    Dim demRowCount As Long
    Dim demandDuration As Long
    Dim sumDemRowCount As Long
    Dim sumDemandDuration As Long
    Dim ruleNameLen As Long
    Dim templateCount As Long
    Dim fromDt As String
    Dim toDt As String
    Dim templateName As String
    Dim templateLineCount As Long
    Dim templateUrnoTemp  As String
    Dim templateLine As String
    Dim fltIndUrno As String
    Dim templateFlag As Boolean
    Dim demStrWhere As String
    Dim demLiveCount As Long
    Dim strWhere As String
    Dim fromDt1 As String
    Dim demPlanCount As Long
    Dim demPlanRowCount As Long
    Dim demPlanDemandDuration As Long
    Dim sumPlanDemandDuration As Long
    Dim sumPlanDemRowCount As Long
    Dim ruleUrnoListPlan As String

    Dim xlapp
    Dim xlbook
    Dim xlsheet
    Dim xlRow As Integer

    Dim jobStrWhere As String
    Dim jobDemCount As Long
    Dim jobDemDuration As Long
    Dim jobDemUsed As Long
    Dim sumJobDemCount As Long
    Dim sumJobDemDuration As Long
    Dim sumJobDemUsed As Long
    Dim tempDemUrno As String
    Dim jobList As String
    Dim jobListCount As Integer
    Dim jobRow As String
    Dim fromDt3 As String
    Dim toDt3 As String
    
    'Get the selected templates
    templateCount = toList.ListCount
    
    'Added by KKH on 15/02/2007
    'OpenErr1 is invoked if user cancels from Open Dialog
    On Error GoTo OpenErr1
    
    If templateCount > 0 Then
    
        dlgSave.DialogTitle = "Save as Excel"
        dlgSave.Filter = "Excel (CSV)|*.csv"
        dlgSave.FileName = "Flight Independent Demands Report"
        dlgSave.ShowSave

        Set xlapp = CreateObject("excel.application") 'Starts the Excel Session
        Set xlbook = xlapp.Workbooks.Add 'Add a Workbook
        Set xlsheet = xlbook.Worksheets.Item(1) 'Select a Sheet
        xlapp.Application.Visible = False
        
'        If dlgSave.CancelError = False Then
'            Exit Sub
'        End If

        If dlgSave.FileName <> "" Then
           
            'Prepare Heading of the report
            xlRow = 1
            xlapp.Application.Cells(xlRow, 4).Font.Bold = True
            xlapp.Application.Cells(xlRow, 4).Value = "Flight Independent Demands"
            xlapp.Application.Cells(xlRow, 7).Font.Bold = True
            xlapp.Application.Cells(xlRow, 7).Value = "Report Genearation Time, Start: " & Now
            xlRow = xlRow + 2
            
            MousePointer = vbHourglass
            
            'Get the fromDate & time, as well as toDate & time to be used in preparing query.
            'fromDt = Mid(fromDate, 7, 4) + Mid(fromDate, 4, 2) + Mid(fromDate, 1, 2) + Mid(fromTime, 1, 2) + Mid(fromTime, 4, 2) + "00"
            'toDt = Mid(toDate, 7, 4) + Mid(toDate, 4, 2) + Mid(toDate, 1, 2) + Mid(toTime, 1, 2) + Mid(toTime, 4, 2) + "00"

            ' Get the Input Date and Time Changed by KKH
            fromDate = Trim(Replace(fromDate, ".", ""))
            toDate = Trim(Replace(toDate, ".", ""))
            
            fromDt = Mid(fromDate, 5, 4) + Mid(fromDate, 3, 2) + Mid(fromDate, 1, 2) + Mid(fromTime, 1, 2) + Mid(fromTime, 4, 2) + "00"
            toDt = Mid(toDate, 5, 4) + Mid(toDate, 3, 2) + Mid(toDate, 1, 2) + Mid(toTime, 1, 2) + Mid(toTime, 4, 2) + "00"
            
            fromDt3 = Format(DateAdd("d", -1, CedaFullDateToVb(fromDt)), "yyyymmddhhmmss")
            toDt3 = Format(DateAdd("d", 1, CedaFullDateToVb(toDt)), "yyyymmddhhmmss")
            
            fromDt = Format(DateAdd("h", -8, CedaFullDateToVb(fromDt)), "yyyymmddhhmmss")
            toDt = Format(DateAdd("h", -8, CedaFullDateToVb(toDt)), "yyyymmddhhmmss")
            fromDt1 = Format(DateAdd("d", -1, CedaFullDateToVb(fromDt)), "yyyymmddhhmmss")
            
            
            'First get all the selected Templates
            'For each Template, need to calculate flight dependent, flight independent & CCI demands
            
            For l = 0 To templateCount - 1 Step 1
                templateName = toList.List(l)
                templateFlag = False

                'Prepare Excel settings properly
                'xlapp.Application.Cells(xlRow, 1).ColumnWidth = 100
                xlapp.Application.Cells(xlRow, 3).Font.Bold = True
                xlapp.Application.Cells(xlRow, 3).Value = "Flight Independent Demands for Loaded Timeframe  from " & fromDate & "/" & fromTime & " To " & toDate & "/" & toTime & "     Template: " & templateName
                xlRow = xlRow + 2

                'Changed by KKH on 13/02/2007
                xlapp.Application.Cells(xlRow, 1).ColumnWidth = 15
                xlapp.Application.Cells(xlRow, 2).ColumnWidth = 15
                xlapp.Application.Cells(xlRow, 3).ColumnWidth = 25
                xlapp.Application.Cells(xlRow, 4).ColumnWidth = 25
                xlapp.Application.Cells(xlRow, 5).ColumnWidth = 25
                xlapp.Application.Cells(xlRow, 6).ColumnWidth = 15
                xlapp.Application.Cells(xlRow, 7).ColumnWidth = 25
                xlapp.Application.Cells(xlRow, 8).ColumnWidth = 17
                xlapp.Application.Cells(xlRow, 9).ColumnWidth = 27
                xlapp.Application.Cells(xlRow, 1).Font.Bold = True
                xlapp.Application.Cells(xlRow, 2).Font.Bold = True
                xlapp.Application.Cells(xlRow, 3).Font.Bold = True
                xlapp.Application.Cells(xlRow, 4).Font.Bold = True
                xlapp.Application.Cells(xlRow, 5).Font.Bold = True
                xlapp.Application.Cells(xlRow, 6).Font.Bold = True
                xlapp.Application.Cells(xlRow, 7).Font.Bold = True
                xlapp.Application.Cells(xlRow, 8).Font.Bold = True
                xlapp.Application.Cells(xlRow, 9).Font.Bold = True
                xlapp.Application.Cells(xlRow, 1).Value = "Rule Name"
                xlapp.Application.Cells(xlRow, 2).Value = "Actual Jobs"
                xlapp.Application.Cells(xlRow, 3).Value = "Actual Jobs Duration(Hrs)"
                xlapp.Application.Cells(xlRow, 4).Value = "Actual Jobs Duration(Min)"
                xlapp.Application.Cells(xlRow, 5).Value = "Actual Demands Used"
                xlapp.Application.Cells(xlRow, 6).Value = "Actual Demands"
                xlapp.Application.Cells(xlRow, 7).Value = "Actual Demand Duration"
                xlapp.Application.Cells(xlRow, 8).Value = "Planned Demands"
                xlapp.Application.Cells(xlRow, 9).Value = "Planned Demands Duration"
                xlRow = xlRow + 1
            
                'Get the URNO of template names from tab_TPLTAB
                templateUrnoTemp = frmData.tab_TPLTAB.GetLinesByColumnValue(1, templateName, 0)
                
                'For every Template, there will be flight dependent & independent demands will be there. Need to check
                If ItemCount(templateUrnoTemp, ",") > 1 Then
                    templateLineCount = ItemCount(templateUrnoTemp, ",")
                    
                    For k = 1 To templateLineCount Step 1
                        templateLine = GetItem(templateUrnoTemp, k, ",")
                        If frmData.tab_TPLTAB.GetColumnValues(templateLine, 2) = "RULE_FIR" Then
                            fltIndUrno = frmData.tab_TPLTAB.GetColumnValues(templateLine, 0)
                            templateFlag = True
                        End If
                    Next k
                Else
                    If frmData.tab_TPLTAB.GetColumnValues(templateUrnoTemp, 2) = "RULE_FIR" Then
                        fltIndUrno = frmData.tab_TPLTAB.GetColumnValues(templateUrnoTemp, 0)
                        templateFlag = True
                    End If
                End If
                    
                If templateFlag Then
                
                    'Get only the Rules information for the specific template
                    strWhere = "where UTPL = '" & fltIndUrno & "'"
                    frmData.tab_RUDTAB.ResetContent
                    frmData.tab_RUDTAB.Refresh
                    frmData.LoadData frmData.tab_RUDTAB, strWhere
                    
                    'demStrWhere = "where UTPL = '" & fltIndUrno & "'  and DEEN > '" + fromDt + "' and DEBE between '" + fromDt1 + "' and '" + toDt + "' "
                    'Selecting flight dependent demands, by avoiding CCI demands. Only demands involved personnel should be picked up for demand comparision. RETY = '100'
                    demStrWhere = "where UTPL = '" & fltIndUrno & "'  and RETY = '100' and DEEN > '" + fromDt + "' and DEBE between '" + fromDt1 + "' and '" + toDt + "' "
                    
                    'Get the Flight Independent Demands from DEMTAB
                    tab_DEMTABLIVE.ResetContent
                    tab_DEMTABLIVE.Refresh
                    frmData.LoadData tab_DEMTABLIVE, demStrWhere
                    demLiveCount = tab_DEMTABLIVE.GetLineCount

                    'Get the Flight Independent Demands from DEMTAB
                    tab_DEMTABPLAN.ResetContent
                    tab_DEMTABPLAN.Refresh
                    frmData.LoadData tab_DEMTABPLAN, demStrWhere
                    demPlanCount = tab_DEMTABPLAN.GetLineCount

                    jobStrWhere = "where UTPL = '" & fltIndUrno & "' and ACTO between '" + fromDt3 + "' and '" + toDt3 + "' and ACFR < '" + toDt3 + "' "
                    Debug.Print jobStrWhere
                    tab_JOBTABDEM.ResetContent
                    tab_JOBTABDEM.Refresh
                    frmData.LoadData tab_JOBTABDEM, jobStrWhere
                    'MsgBox tab_JOBTABDEM.GetLineCount
                    
                    tab_DEMTABTEMP.ResetContent
                    tab_DEMTABTEMP.Refresh
                    sumDemandDuration = 0
                    sumDemRowCount = 0
                    ruleUrnoList = ""
                    ruleUrnoListPlan = ""

                    tab_DEMTABPLANTEMP.ResetContent
                    tab_DEMTABPLANTEMP.Refresh
                    sumPlanDemandDuration = 0
                    sumPlanDemRowCount = 0
                    sumJobDemCount = 0
                    sumJobDemDuration = 0
                    sumJobDemUsed = 0
                   
                    'Prepare distinct URUD, so that to get Rule names & prepare list of demands per rule name - DEMTABLIVE
                    For j = 0 To demLiveCount - 1 Step 1
                        'tempLinedem = GetItem(fltIndList, j, ",")
                        
                        'Prepare DEMTABTEMP, provided demand is valid
                        If Mid(tab_DEMTABLIVE.GetColumnValue(j, 3), 2, 1) <> "1" Then
                            tempUrud = tab_DEMTABLIVE.GetColumnValues(j, 8)
                            
                            'Get the rowvalue of tempUrud from RUDTAB & get the URUE value to check in RUETAB
                            tempLine = frmData.tab_RUDTAB.GetLinesByColumnValue(0, tempUrud, 0)
                            tempUrue = frmData.tab_RUDTAB.GetColumnValues(tempLine, 1)
                            
                            'Get the rowvalue of tempUrue from RUETAB & get the Recordname from RUETAB
                            tempLine = frmData.tab_RUETAB.GetLinesByColumnValue(0, tempUrue, 0)
                            ruleUrno = frmData.tab_RUETAB.GetColumnValues(tempLine, 0)
                            
                            'Add DEMTAB row to a temporary table with URNO of RULE..
                            'tab_DEMTABTEMP.InsertTextLine tab_DEMTABLIVE.GetColumnValues(tempLine, "0,1,2,3,4,5,6,7,8,9") & "," & ruleUrno, True
                            tab_DEMTABTEMP.InsertTextLine tab_DEMTABLIVE.GetColumnValues(j, "0,1,2,3,4,5,6,7,8,9") & "," & ruleUrno, True
                            
                            If InStr(1, ruleUrnoList, ruleUrno) = 0 Then ruleUrnoList = ruleUrnoList & ruleUrno & ","
                        End If
                    Next j
                    

                    'Prepare distinct URUD, so that to get Rule names & prepare list of demands per rule name - DEMTABPLAN
                    For j = 0 To demPlanCount - 1 Step 1
                        'tempLinedem = GetItem(fltIndList, j, ",")
                        
                        'Prepare DEMTABTEMP, provided demand is valid
                        If Mid(tab_DEMTABPLAN.GetColumnValue(j, 3), 2, 1) <> "1" Then
                            tempUrud = tab_DEMTABPLAN.GetColumnValues(j, 8)
                            
                            'Get the rowvalue of tempUrud from RUDTAB & get the URUE value to check in RUETAB
                            tempLine = frmData.tab_RUDTAB.GetLinesByColumnValue(0, tempUrud, 0)
                            tempUrue = frmData.tab_RUDTAB.GetColumnValues(tempLine, 1)
                            
                            'Get the rowvalue of tempUrue from RUETAB & get the Recordname from RUETAB
                            tempLine = frmData.tab_RUETAB.GetLinesByColumnValue(0, tempUrue, 0)
                            ruleUrno = frmData.tab_RUETAB.GetColumnValues(tempLine, 0)
                            
                            'Add DEMTAB row to a temporary table with URNO of RULE..
                            'tab_DEMTABTEMP.InsertTextLine tab_DEMTABLIVE.GetColumnValues(tempLine, "0,1,2,3,4,5,6,7,8,9") & "," & ruleUrno, True
                            tab_DEMTABPLANTEMP.InsertTextLine tab_DEMTABPLAN.GetColumnValues(j, "0,1,2,3,4,5,6,7,8,9") & "," & ruleUrno, True

                            If InStr(1, ruleUrnoListPlan, ruleUrno) = 0 Then ruleUrnoListPlan = ruleUrnoListPlan & ruleUrno & ","
                            
                        End If
                    Next j
                    
               
                    ruleCount = ItemCount(ruleUrnoList, ",")
        
                    For j = 1 To ruleCount - 1 Step 1
                        ruleUrno = GetItem(ruleUrnoList, j, ",")
                        
                        'Get the rule name
                        tempLine = frmData.tab_RUETAB.GetLinesByColumnValue(0, ruleUrno, 0)
                        ruleName = frmData.tab_RUETAB.GetColumnValues(tempLine, 1)
                        
                        'To get all the rows for the  ruleUrno - DEMTABLIVE
                        demRows = tab_DEMTABTEMP.GetLinesByColumnValue(10, ruleUrno, 0)
                        demCount = ItemCount(demRows, ",")
                        demRowCount = 0
                        demandDuration = 0
                        jobDemCount = 0
                        jobDemDuration = 0
                        jobDemUsed = 0
                        
                        If demCount > 0 Then
                            For k = 1 To demCount Step 1
                                'Prepare the demands duration
                                tempLinedem = GetItem(demRows, k, ",")
                                'prepare number of hours & demand count .. provided if demand is valid
                               
                                demRowCount = demRowCount + 1
                                demandDuration = demandDuration + DateDiff("n", CedaFullDateToVb(tab_DEMTABTEMP.GetColumnValues(tempLinedem, 0)), CedaFullDateToVb(tab_DEMTABTEMP.GetColumnValues(tempLinedem, 1)))

                                'To calculate actual jobs, need to check JOBTAB & get the details from JOBTAB.
                                'Getting the urno for each demand & calculate the actual job times
                                'KKH 2)
                                tempDemUrno = tab_DEMTABLIVE.GetColumnValues(tempLinedem, 10)
                                jobList = tab_JOBTABDEM.GetLinesByColumnValue(3, tempDemUrno, 0)
                                jobListCount = ItemCount(jobList, ",")

                                'if jobListCount is not zero, then calculate the actual job
                                If jobListCount > 0 Then
                                    jobDemCount = jobDemCount + jobListCount
                                    jobDemUsed = jobDemUsed + 1
                                    'if more than one person worked for the same demand, need to calculate
                                    For m = 1 To jobListCount Step 1
                                        jobRow = GetItem(jobList, m, ",")
                                        jobDemDuration = jobDemDuration + DateDiff("n", CedaFullDateToVb(tab_JOBTABDEM.GetColumnValues(jobRow, 1)), CedaFullDateToVb(tab_JOBTABDEM.GetColumnValues(jobRow, 2)))
                                    Next m
                                End If
                                'End - calculation of actual jobs.
                               
                            Next k
                            
                            sumDemRowCount = sumDemRowCount + demRowCount
                            sumDemandDuration = sumDemandDuration + demandDuration
                            sumJobDemCount = sumJobDemCount + jobDemCount
                            sumJobDemDuration = sumJobDemDuration + jobDemDuration
                            sumJobDemUsed = sumJobDemUsed + jobDemUsed
                        
                        End If

                        'To get all the rows for the  ruleUrno - DEMTABPLAN
                        demRows = tab_DEMTABPLANTEMP.GetLinesByColumnValue(10, ruleUrno, 0)
                        demCount = ItemCount(demRows, ",")
                        demPlanRowCount = 0
                        demPlanDemandDuration = 0
                        
                        If demCount > 0 Then
                            For k = 1 To demCount Step 1
                                'Prepare the demands duration
                                tempLinedem = GetItem(demRows, k, ",")
                                'prepare number of hours & demand count .. provided if demand is valid
                               
                                demPlanRowCount = demPlanRowCount + 1
                                demPlanDemandDuration = demPlanDemandDuration + DateDiff("n", CedaFullDateToVb(tab_DEMTABPLANTEMP.GetColumnValues(tempLinedem, 0)), CedaFullDateToVb(tab_DEMTABPLANTEMP.GetColumnValues(tempLinedem, 1)))
                               
                            Next k
                            
                            sumPlanDemRowCount = sumPlanDemRowCount + demPlanRowCount
                            sumPlanDemandDuration = sumPlanDemandDuration + demPlanDemandDuration
                        End If
                     
                        'Print #FileNumber, "," & Trim(ruleName) & "," & demRowCount & "," & demandDuration & "," & demPlanRowCount & "," & demPlanDemandDuration

                        'KKH - Changed on 26/01/2007, special character mapping
                        'xlapp.Application.Cells(xlRow, 1).Value = Trim(ruleName)
                        xlapp.Application.Cells(xlRow, 1).Value = CleanString(ruleName, FOR_CLIENT, True)
                        'Changed by KKH on 13/02/2007
                        xlapp.Application.Cells(xlRow, 2).Value = jobDemCount
                        xlapp.Application.Cells(xlRow, 2).HorizontalAlignment = xlLeft
                        'xlapp.Application.Cells(xlRow, 3).Value = Int(jobDemDuration / 60) & " Hrs :" & (jobDemDuration Mod 60) & " Min"
                        xlapp.Application.Cells(xlRow, 3).Value = Int(jobDemDuration / 60)
                        xlapp.Application.Cells(xlRow, 3).HorizontalAlignment = xlLeft
                        xlapp.Application.Cells(xlRow, 4).Value = Int(jobDemDuration Mod 60)
                        xlapp.Application.Cells(xlRow, 4).HorizontalAlignment = xlLeft
                        
                        xlapp.Application.Cells(xlRow, 5).Value = jobDemUsed
                        xlapp.Application.Cells(xlRow, 5).HorizontalAlignment = xlLeft
                        xlapp.Application.Cells(xlRow, 6).Value = demRowCount
                        xlapp.Application.Cells(xlRow, 6).HorizontalAlignment = xlLeft
                        xlapp.Application.Cells(xlRow, 7).Value = Int(demandDuration / 60) & " Hrs :" & (demandDuration Mod 60) & " Min"
                        xlapp.Application.Cells(xlRow, 7).HorizontalAlignment = xlLeft
                        xlapp.Application.Cells(xlRow, 8).Value = demPlanRowCount
                        xlapp.Application.Cells(xlRow, 8).HorizontalAlignment = xlLeft
                        xlapp.Application.Cells(xlRow, 9).Value = Int(demPlanDemandDuration / 60) & " Hrs :" & (demPlanDemandDuration Mod 60) & " Min"
                        xlapp.Application.Cells(xlRow, 9).HorizontalAlignment = xlLeft
                        xlRow = xlRow + 1
              
                    Next j


                    'If any Rules which are not available in the demandslive rule list, then need to execute them.
                    ruleCount = ItemCount(ruleUrnoListPlan, ",")
        
                    For j = 1 To ruleCount - 1 Step 1
                        ruleUrno = GetItem(ruleUrnoListPlan, j, ",")

                        If InStr(1, ruleUrnoList, ruleUrno) = 0 Then
                            tempLine = frmData.tab_RUETAB.GetLinesByColumnValue(0, ruleUrno, 0)
                            ruleName = frmData.tab_RUETAB.GetColumnValues(tempLine, 1)

                            'To get all the rows for the  ruleUrno - DEMTABPLANTEMP
                            demRows = tab_DEMTABPLANTEMP.GetLinesByColumnValue(10, ruleUrno, 0)
                            demCount = ItemCount(demRows, ",")
                            demPlanRowCount = 0
                            demPlanDemandDuration = 0
                            
                            If demCount > 0 Then
                                For k = 1 To demCount Step 1
                                    'Prepare the demands duration
                                    tempLinedem = GetItem(demRows, k, ",")
                                    'prepare number of hours & demand count .. provided if demand is valid
                                   
                                    demPlanRowCount = demPlanRowCount + 1
                                    demPlanDemandDuration = demPlanDemandDuration + DateDiff("n", CedaFullDateToVb(tab_DEMTABPLANTEMP.GetColumnValues(tempLinedem, 0)), CedaFullDateToVb(tab_DEMTABPLANTEMP.GetColumnValues(tempLinedem, 1)))
                                   
                                Next k
                                
                                sumPlanDemRowCount = sumPlanDemRowCount + demPlanRowCount
                                sumPlanDemandDuration = sumPlanDemandDuration + demPlanDemandDuration
                            End If
                        
                       
                        'KKH - Changed on 26/01/2007, special character mapping
                        'xlapp.Application.Cells(xlRow, 1).Value = Trim(ruleName)
                        xlapp.Application.Cells(xlRow, 1).Value = CleanString(ruleName, FOR_CLIENT, True)
                        xlapp.Application.Cells(xlRow, 2).Value = 0
                        xlapp.Application.Cells(xlRow, 2).HorizontalAlignment = xlLeft
                        'Changed by KKH on 13/02/2007
                        xlapp.Application.Cells(xlRow, 3).Value = 0
                        xlapp.Application.Cells(xlRow, 3).HorizontalAlignment = xlLeft
                        xlapp.Application.Cells(xlRow, 4).Value = 0
                        xlapp.Application.Cells(xlRow, 4).HorizontalAlignment = xlLeft
                        
                        xlapp.Application.Cells(xlRow, 5).Value = 0
                        xlapp.Application.Cells(xlRow, 5).HorizontalAlignment = xlLeft
                        xlapp.Application.Cells(xlRow, 6).Value = 0
                        xlapp.Application.Cells(xlRow, 6).HorizontalAlignment = xlLeft
                        xlapp.Application.Cells(xlRow, 7).Value = 0
                        xlapp.Application.Cells(xlRow, 7).HorizontalAlignment = xlLeft
                        xlapp.Application.Cells(xlRow, 8).HorizontalAlignment = xlLeft
                        xlapp.Application.Cells(xlRow, 8).Value = demPlanRowCount
                        xlapp.Application.Cells(xlRow, 9).HorizontalAlignment = xlLeft
                        xlapp.Application.Cells(xlRow, 9).Value = Int(demPlanDemandDuration / 60) & " Hrs :" & (demPlanDemandDuration Mod 60) & " Min"
                        xlRow = xlRow + 1
      
                        End If
                    Next j

                    xlRow = xlRow + 1
                    'Changed by KKH on 13/02/2007
                    xlapp.Application.Cells(xlRow, 1).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 2).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 3).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 4).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 5).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 6).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 7).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 8).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 9).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 1).Value = "Sum :"
                    xlapp.Application.Cells(xlRow, 2).Value = sumJobDemCount
                    xlapp.Application.Cells(xlRow, 2).HorizontalAlignment = xlLeft
                    'xlapp.Application.Cells(xlRow, 3).Value = Int(sumJobDemDuration / 60) & " Hrs :" & (sumJobDemDuration Mod 60) & " Min"
                    xlapp.Application.Cells(xlRow, 3).Value = "Actual Jobs Duration :"
                    xlapp.Application.Cells(xlRow, 3).HorizontalAlignment = xlLeft
                    xlapp.Application.Cells(xlRow, 4).Value = Int(sumJobDemDuration / 60) & " Hrs :" & (sumJobDemDuration Mod 60) & " Min"
                    xlapp.Application.Cells(xlRow, 4).HorizontalAlignment = xlLeft
                    
                    xlapp.Application.Cells(xlRow, 5).Value = sumJobDemUsed
                    xlapp.Application.Cells(xlRow, 5).HorizontalAlignment = xlLeft
                    xlapp.Application.Cells(xlRow, 6).Value = sumDemRowCount
                    xlapp.Application.Cells(xlRow, 6).HorizontalAlignment = xlLeft
                    xlapp.Application.Cells(xlRow, 7).Value = Int(sumDemandDuration / 60) & " Hrs :" & (sumDemandDuration Mod 60) & " Min"
                    xlapp.Application.Cells(xlRow, 7).HorizontalAlignment = xlLeft
                    xlapp.Application.Cells(xlRow, 8).Value = sumPlanDemRowCount
                    xlapp.Application.Cells(xlRow, 8).HorizontalAlignment = xlLeft
                    xlapp.Application.Cells(xlRow, 9).Value = Int(sumPlanDemandDuration / 60) & " Hrs :" & (sumPlanDemandDuration Mod 60) & " Min"
                    xlapp.Application.Cells(xlRow, 9).HorizontalAlignment = xlLeft
                    xlRow = xlRow + 2
   
                Else 'Template Flag
                    'xlapp.Application.Cells(xlRow, 1).ColumnWidth = 100
                    xlapp.Application.Cells(xlRow, 1).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 1).Value = " Flight Independent Demands wont be available for this template :" & templateName
                    xlRow = xlRow + 1
                End If
            Next l
            
            xlRow = xlRow + 1
            xlapp.Application.Cells(xlRow, 7).Value = "Report Genearation Time, End: " & Now
            xlapp.Application.Cells(xlRow, 7).Font.Bold = True

            xlbook.SaveAs dlgSave.FileName
            xlbook.Close
            xlapp.Quit
            MousePointer = vbDefault
            
            MsgBox "" & dlgSave.FileName & " created successfully"
        Else 'dlgSave if
            MsgBox "Please Enter Filename to upload Flight Independent Report"
        End If
    Else
        MsgBox "Please Select the templates"
    End If
OpenErr1:
    Exit Sub

End Sub

'This function prepares flight dependent demands, Inbound, outbound & turn around. Except CCI demands
Sub prepareFlightDependent()
    Dim FileNumber
    Dim i As Long
    Dim k As Integer
    Dim j As Long
    Dim l As Long
    Dim m As Integer
    Dim urnoTemplateJobs As String
    Dim strkey As String
    Dim fltDepList As String
    Dim fltDepCount As Long
    Dim tempLine As String
    Dim tempLinedem As String
    Dim tempUrud As String
    Dim tempUrue As String
    Dim urudCount As Long
    Dim ruleName As String
    Dim ruleUrno As String
    Dim fltUrnoList As String
    Dim ruleCount As String
    Dim demRows As String
    Dim demCount As Long
    Dim demRowCount As Long
    Dim demandDuration As Long
    Dim sumDemRowCount As Long
    Dim sumDemandDuration As Long
    Dim ruleNameLen As Long
    Dim aftStrWhere As String
    Dim fromDt As String
    Dim toDt As String
    Dim fromDt1 As String
    Dim toDt1 As String
    Dim fromDt3 As String
    Dim toDt3 As String
    Dim fromDt2 As String
    Dim demOuri As String
    Dim demOuro As String
    Dim tempfltUrno As String
    Dim ruleNameStr As String
    Dim tempRuleUrno As String
    Dim inBoundStr As String
    Dim outBoundStr As String
    Dim fltAcType As String
    Dim fltLine As String
    Dim fltAdid As String
    Dim fltStoa As String
    Dim fltStod As String
    Dim fltEtai As String
    Dim fltEtdi As String
    Dim fltCount As Long
    Dim templateName As String
    Dim templateLineCount As Long
    Dim templateUrnoTemp  As String
    Dim templateLine As String
    Dim fltDepUrno As String
    Dim templateFlag As Boolean
    Dim demStrWhere As String
    Dim demLiveCount As Long
    Dim turnAroundFlag As Boolean
    Dim templateCount As Long
    Dim demRowListInbound As String
    Dim demRowListInboundCount As Long
    Dim demRowListOutbound As String
    Dim demRowListOutboundCount As Long
    Dim strWhere As String
    Dim tempDETY As String
    Dim demRowsList As String
    Dim demRow As String
    Dim tempTime As Integer
    Dim sumPlanDemandDuration As Long
    Dim sumPlanDemRowCount As Long
    Dim demPlanDemandDuration As Long
    Dim demPlanRowCount As Long
    Dim planFltAcType As String
    Dim demPlanCount As Long
    Dim fltLine1 As String
    'Added by KKH on 13/02/2006
    Dim sumActualJob As Long
    Dim sumActualJobDuration As Long
    
    Dim xlapp
    Dim xlbook
    Dim xlsheet
    Dim xlRow As Integer
    Dim demShowCount As Integer
    Dim k1 As Integer
    Dim demShowVal As String
    Dim planRuleNameStr As String

    Dim jobStrWhere As String
    Dim jobDemCount As Long
    Dim jobDemDuration As Long
    Dim jobDemUsed As Long
    Dim sumJobDemCount As Long
    Dim sumJobDemDuration As Long
    Dim tempDemUrno As String
    Dim jobList As String
    Dim jobListCount As Integer
    Dim jobRow As String

    'KKH 30/01/2007
'    Dim jobCount As Long
'    Dim jobDuration As Long
'    Dim p As Integer
    
    
    'Get the selected templates
    templateCount = toList.ListCount
    
    'Added by KKH on 15/02/2007
    'OpenErr1 is invoked if user cancels from Open Dialog
    On Error GoTo OpenErr1
    
    If templateCount > 0 Then
        dlgSave.DialogTitle = "Save as Excel"
        dlgSave.Filter = "Excel (CSV)|*.csv"
        dlgSave.FileName = "Flight Dependent Demands Report"
        dlgSave.ShowSave

        Set xlapp = CreateObject("excel.application") 'Starts the Excel Session
        Set xlbook = xlapp.Workbooks.Add 'Add a Workbook
        Set xlsheet = xlbook.Worksheets.Item(1) 'Select a Sheet
        xlapp.Application.Visible = False
        
        If dlgSave.FileName <> "" Then
            
            'Prepare Heading of the report
            xlRow = 1
            xlapp.Application.Cells(xlRow, 6).ColumnWidth = 100
            xlapp.Application.Cells(xlRow, 6).Font.Bold = True
            xlapp.Application.Cells(xlRow, 6).Value = "Flight Dependent Demands"
            xlapp.Application.Cells(xlRow, 11).Font.Bold = True
            xlapp.Application.Cells(xlRow, 11).Value = "Report Genearation Time, Start: " & Now
            
            'xlapp.Application.Cells(xlRow, 1).HorizontalAlignment = xlright
            xlRow = xlRow + 2

            MousePointer = vbHourglass
            
            'Get the fromDate & time, as well as toDate & time to be used in preparing query.
            'fromDt = Mid(fromDate, 7, 4) + Mid(fromDate, 4, 2) + Mid(fromDate, 1, 2) + Mid(fromTime, 1, 2) + Mid(fromTime, 4, 2) + "00"
            'toDt = Mid(toDate, 7, 4) + Mid(toDate, 4, 2) + Mid(toDate, 1, 2) + Mid(toTime, 1, 2) + Mid(toTime, 4, 2) + "00"

            ' Get the Input Date and Time Changed by KKH
            fromDate = Trim(Replace(fromDate, ".", ""))
            toDate = Trim(Replace(toDate, ".", ""))
            
            fromDt = Mid(fromDate, 5, 4) + Mid(fromDate, 3, 2) + Mid(fromDate, 1, 2) + Mid(fromTime, 1, 2) + Mid(fromTime, 4, 2) + "00"
            toDt = Mid(toDate, 5, 4) + Mid(toDate, 3, 2) + Mid(toDate, 1, 2) + Mid(toTime, 1, 2) + Mid(toTime, 4, 2) + "00"
           
            fromDt1 = Format(DateAdd("d", -5, CedaFullDateToVb(fromDt)), "yyyymmddhhmmss")
            toDt1 = Format(DateAdd("d", 5, CedaFullDateToVb(toDt)), "yyyymmddhhmmss")

            fromDt3 = Format(DateAdd("d", -1, CedaFullDateToVb(fromDt)), "yyyymmddhhmmss")
            toDt3 = Format(DateAdd("d", 1, CedaFullDateToVb(toDt)), "yyyymmddhhmmss")
            
            fromDt = Format(DateAdd("h", -gUTCOffset, CedaFullDateToVb(fromDt)), "yyyymmddhhmmss")
            toDt = Format(DateAdd("h", -gUTCOffset, CedaFullDateToVb(toDt)), "yyyymmddhhmmss")
            fromDt2 = Format(DateAdd("d", -1, CedaFullDateToVb(fromDt)), "yyyymmddhhmmss")
            
            aftStrWhere = "where STOD between '" + fromDt1 + "' and '" + toDt1 + "' or STOA between '" + fromDt1 + "' and '" + toDt1 + "'"
            'aftStrWhere = "where (STOD between '" + fromDt + "' and '" + toDt + "') or (STOA between '" + fromDt + "' and '" + toDt + "')"
            frmData.tab_AFTTAB.ResetContent
            frmData.tab_AFTTAB.Refresh
            frmData.LoadData frmData.tab_AFTTAB, aftStrWhere
            
            'Get the planned AFTTAB as well
            frmData.tab_AFTTABPLAN.ResetContent
            frmData.tab_AFTTABPLAN.Refresh
            frmData.LoadData frmData.tab_AFTTABPLAN, aftStrWhere
            
            'First get all the selected Templates
            'For each Template, need to calculate flight dependent, flight independent & CCI demands
    
            For l = 0 To templateCount - 1 Step 1
                templateName = toList.List(l)
                templateFlag = False
                
                'Prepare Excel settings properly
                xlapp.Application.Cells(xlRow, 6).Font.Bold = True
                xlapp.Application.Cells(xlRow, 6).Value = "Flight Dependent Demands for Loaded Timeframe  from " & fromDate & "/" & fromTime & " To " & toDate & "/" & toTime & "     Template: " & templateName
                xlapp.Application.Cells(xlRow, 6).HorizontalAlignment = xlCenter
                xlRow = xlRow + 2


                xlapp.Application.Cells(xlRow, 1).ColumnWidth = 15
                xlapp.Application.Cells(xlRow, 2).ColumnWidth = 15
                xlapp.Application.Cells(xlRow, 3).ColumnWidth = 15
                xlapp.Application.Cells(xlRow, 4).ColumnWidth = 15
                xlapp.Application.Cells(xlRow, 5).ColumnWidth = 15
                xlapp.Application.Cells(xlRow, 6).ColumnWidth = 15
                xlapp.Application.Cells(xlRow, 7).ColumnWidth = 20
                xlapp.Application.Cells(xlRow, 8).ColumnWidth = 15
                xlapp.Application.Cells(xlRow, 9).ColumnWidth = 15
                xlapp.Application.Cells(xlRow, 10).ColumnWidth = 25
                'Changed by KKH on 13/02/2007
                xlapp.Application.Cells(xlRow, 11).ColumnWidth = 25
                xlapp.Application.Cells(xlRow, 12).ColumnWidth = 15
                xlapp.Application.Cells(xlRow, 13).ColumnWidth = 15
                xlapp.Application.Cells(xlRow, 14).ColumnWidth = 25
                xlapp.Application.Cells(xlRow, 15).ColumnWidth = 15
                xlapp.Application.Cells(xlRow, 16).ColumnWidth = 25
                xlapp.Application.Cells(xlRow, 17).ColumnWidth = 30
                xlapp.Application.Cells(xlRow, 18).ColumnWidth = 30
                
                xlapp.Application.Cells(xlRow, 1).Font.Bold = True
                xlapp.Application.Cells(xlRow, 2).Font.Bold = True
                xlapp.Application.Cells(xlRow, 3).Font.Bold = True
                xlapp.Application.Cells(xlRow, 4).Font.Bold = True
                xlapp.Application.Cells(xlRow, 5).Font.Bold = True
                xlapp.Application.Cells(xlRow, 6).Font.Bold = True
                xlapp.Application.Cells(xlRow, 7).Font.Bold = True
                xlapp.Application.Cells(xlRow, 8).Font.Bold = True
                xlapp.Application.Cells(xlRow, 9).Font.Bold = True
                xlapp.Application.Cells(xlRow, 10).Font.Bold = True
                xlapp.Application.Cells(xlRow, 11).Font.Bold = True
                xlapp.Application.Cells(xlRow, 12).Font.Bold = True
                xlapp.Application.Cells(xlRow, 13).Font.Bold = True
                xlapp.Application.Cells(xlRow, 14).Font.Bold = True
                xlapp.Application.Cells(xlRow, 15).Font.Bold = True
                xlapp.Application.Cells(xlRow, 16).Font.Bold = True
                xlapp.Application.Cells(xlRow, 17).Font.Bold = True
                xlapp.Application.Cells(xlRow, 18).Font.Bold = True
                
                xlapp.Application.Cells(xlRow, 1).Value = "Arrival"
                xlapp.Application.Cells(xlRow, 2).Value = "Departure"
                xlapp.Application.Cells(xlRow, 3).Value = "Actual A/C"
                xlapp.Application.Cells(xlRow, 4).Value = "Planned A/C"
                xlapp.Application.Cells(xlRow, 5).Value = "STOA"
                xlapp.Application.Cells(xlRow, 6).Value = "STOD"
                xlapp.Application.Cells(xlRow, 7).Value = "ETA"
                xlapp.Application.Cells(xlRow, 8).Value = "ETD"
                xlapp.Application.Cells(xlRow, 9).Value = "Actual Jobs"
                xlapp.Application.Cells(xlRow, 10).Value = "Actual Jobs Duration(Hrs)"
                xlapp.Application.Cells(xlRow, 11).Value = "Actual Jobs Duration(Min)"
                xlapp.Application.Cells(xlRow, 12).Value = "Actual Demands Used"
                xlapp.Application.Cells(xlRow, 13).Value = "Actual Demands Available"
                xlapp.Application.Cells(xlRow, 14).Value = "Actual Demands Duration"
                xlapp.Application.Cells(xlRow, 15).Value = "Planned Demands"
                xlapp.Application.Cells(xlRow, 16).Value = "Planned Demands Duration"
                xlapp.Application.Cells(xlRow, 17).Value = "Rules Actual"
                xlapp.Application.Cells(xlRow, 18).Value = "Rules Planned"
                xlRow = xlRow + 1

                'Get the URNO of template names from tab_TPLTAB
                templateUrnoTemp = frmData.tab_TPLTAB.GetLinesByColumnValue(1, templateName, 0)
                
                'For every Template, there will be flight dependent & independent demands will be there. Need to check
                If ItemCount(templateUrnoTemp, ",") > 1 Then
                    templateLineCount = ItemCount(templateUrnoTemp, ",")
                    
                    For k = 1 To templateLineCount Step 1
                        templateLine = GetItem(templateUrnoTemp, k, ",")
                        If frmData.tab_TPLTAB.GetColumnValues(templateLine, 2) = "RULE_AFT" Then
                            fltDepUrno = frmData.tab_TPLTAB.GetColumnValues(templateLine, 0)
                            templateFlag = True
                        End If
                    Next k
                Else
                    If frmData.tab_TPLTAB.GetColumnValues(templateUrnoTemp, 2) = "RULE_AFT" Then
                        fltDepUrno = frmData.tab_TPLTAB.GetColumnValues(templateUrnoTemp, 0)
                        templateFlag = True
                    End If
                End If
                    
                If templateFlag Then
                    
                    'Get only the Rules information for the specific template
                    strWhere = "where UTPL = '" & fltDepUrno & "'"
                    frmData.tab_RUDTAB.ResetContent
                    frmData.tab_RUDTAB.Refresh
                    frmData.LoadData frmData.tab_RUDTAB, strWhere
                    

                    'demStrWhere = "where UTPL = '" & fltDepUrno & "'  and DEEN > '" + fromDt + "' and DEBE between '" + fromDt + "' and '" + toDt + "' "
                    'Selecting flight dependent demands, by avoiding CCI demands. Only demands involved personnel should be picked up for demand comparision. RETY = '100'
                    demStrWhere = "where UTPL = '" & fltDepUrno & "'  and RETY = '100' and DEEN > '" + fromDt + "' and DEBE between '" + fromDt2 + "' and '" + toDt + "' and DETY in ('0','1','2') "
                    
                    'Get the Flight Independent Demands from DEMTAB
                    tab_DEMTABLIVE.ResetContent
                    tab_DEMTABLIVE.Refresh
                    frmData.LoadData tab_DEMTABLIVE, demStrWhere
                    demLiveCount = tab_DEMTABLIVE.GetLineCount


                    'Get the Flight Dependent Demands from DEMTABPLAN
                    tab_DEMTABPLAN.ResetContent
                    tab_DEMTABPLAN.Refresh
                    frmData.LoadData tab_DEMTABPLAN, demStrWhere
                    demPlanCount = tab_DEMTABPLAN.GetLineCount

                    jobStrWhere = "where UTPL = '" & fltDepUrno & "' and ACTO between '" + fromDt3 + "' and '" + toDt3 + "' and ACFR < '" + toDt3 + "' "
                    tab_JOBTABDEM.ResetContent
                    tab_JOBTABDEM.Refresh
                    frmData.LoadData tab_JOBTABDEM, jobStrWhere
                    
                    'Debug.Print jobStrWhere
                    'MsgBox tab_JOBTABDEM.GetLineCount


                    tab_DEMTABSHOW.ResetContent
                    tab_DEMTABSHOW.Refresh
                    
                    'MsgBox demLiveCount
                    sumDemandDuration = 0
                    sumDemRowCount = 0
                    fltCount = 0

                    sumPlanDemandDuration = 0
                    sumPlanDemRowCount = 0
                    sumJobDemCount = 0
                    sumJobDemDuration = 0
   
                     'Prepare distinct URUD, so that to get Rule names & prepare list of demands per rule name
                     For j = 0 To demLiveCount - 1 Step 1
                         
                         demRowCount = 0
                         demandDuration = 0
                         ruleNameStr = ""
                         inBoundStr = ""
                         outBoundStr = ""
                         fltAcType = ""
                         planRuleNameStr = ""
                         jobDemCount = 0
                         jobDemDuration = 0
                         jobDemUsed = 0
                         
                         ' KKH - 30/01/2007
'                         jobCount = 0
'                         jobDuration = 0
                         

                        
                         'Check whether demand is valid or not
                         If Mid(tab_DEMTABLIVE.GetColumnValue(j, 3), 2, 1) <> "1" Then
                             tempDETY = tab_DEMTABLIVE.GetColumnValues(j, 2)

                             'If its turn around demand
                             If tempDETY = "0" Then
                                 demOuri = tab_DEMTABLIVE.GetColumnValues(j, 4)
                                 demOuro = tab_DEMTABLIVE.GetColumnValues(j, 5)

                                 If InStr(1, fltUrnoList, demOuri) = 0 And InStr(1, fltUrnoList, demOuri) = 0 Then
                                
                                        fltUrnoList = fltUrnoList & demOuri & ","
                                        fltUrnoList = fltUrnoList & demOuro & ","
                                        
                                        'Get the information for inbound flight
                                        fltLine = frmData.tab_AFTTAB.GetLinesByColumnValue(0, demOuri, 0)
                                        If fltLine <> "" Then
                                            fltAdid = frmData.tab_AFTTAB.GetColumnValue(fltLine, 2)
                                            fltAcType = frmData.tab_AFTTAB.GetColumnValue(fltLine, 10)
                                            fltStoa = frmData.tab_AFTTAB.GetColumnValue(fltLine, 3)
                                            fltEtai = frmData.tab_AFTTAB.GetColumnValue(fltLine, 11)
                                            inBoundStr = frmData.tab_AFTTAB.GetColumnValue(fltLine, 1)
                                        Else
                                            inBoundStr = ""
                                            fltAcType = ""
                                            fltStoa = ""
                                            fltEtai = ""
                                            fltAcType = ""
                                        End If
                                    
                                        'Get the information for outbound flight
                                        fltLine = frmData.tab_AFTTAB.GetLinesByColumnValue(0, demOuro, 0)
                                        If fltLine <> "" Then
                                            fltStod = frmData.tab_AFTTAB.GetColumnValue(fltLine, 4)
                                            fltEtdi = frmData.tab_AFTTAB.GetColumnValue(fltLine, 12)
                                            outBoundStr = frmData.tab_AFTTAB.GetColumnValue(fltLine, 1)
                                        Else
                                            fltStod = ""
                                            fltEtdi = ""
                                            outBoundStr = ""
                                        End If


                                        'Get the planned AC type from PLANNED AFTTAB
                                        fltLine1 = frmData.tab_AFTTABPLAN.GetLinesByColumnValue(0, demOuri, 0)
                                        If fltLine1 <> "" Then
                                            planFltAcType = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine1, 10)
                                        Else
                                            planFltAcType = ""
                                        End If
                        
                                        'If flight is available, then only need to show the demands count, otherwise leave it.
                                        If inBoundStr <> "" And outBoundStr <> "" Then
                                            'Get all the inbound demands for the flight
                                            demRowListInbound = tab_DEMTABLIVE.GetLinesByColumnValue(4, demOuri, 0)
                                            demRowListInboundCount = ItemCount(demRowListInbound, ",")
                                            demRowCount = 0

                                            For k = 1 To demRowListInboundCount Step 1
                                                demRow = GetItem(demRowListInbound, k, ",")
                                                If Mid(tab_DEMTABLIVE.GetColumnValue(demRow, 3), 2, 1) <> "1" Then
                                                    tempUrud = tab_DEMTABLIVE.GetColumnValues(demRow, 8)

                                                    'Get the rowvalue of tempUrud from RUDTAB & get the URUE value to check in RUETAB
                                                    tempLine = frmData.tab_RUDTAB.GetLinesByColumnValue(0, tempUrud, 0)
                                                    tempUrue = frmData.tab_RUDTAB.GetColumnValues(tempLine, 1)
                                                     
                                                    'Get the rowvalue of tempUrue from RUETAB & get the Recordname from RUETAB
                                                    tempLine = frmData.tab_RUETAB.GetLinesByColumnValue(0, tempUrue, 0)
                                                    ruleName = frmData.tab_RUETAB.GetColumnValues(tempLine, 2)
                                                    If InStr(1, ruleNameStr, ruleName) = 0 Then ruleNameStr = ruleNameStr & ruleName & ";"

                                                    tempTime = DateDiff("n", CedaFullDateToVb(tab_DEMTABLIVE.GetColumnValues(demRow, 0)), CedaFullDateToVb(tab_DEMTABLIVE.GetColumnValues(demRow, 1)))
                                                    demRowCount = demRowCount + 1
                                                    
                                                    'demandDuration = demandDuration + DateDiff("h", CedaFullDateToVb(tab_DEMTABLIVE.GetColumnValues(demRow, 0)), CedaFullDateToVb(tab_DEMTABLIVE.GetColumnValues(demRow, 1)))
                                                    demandDuration = demandDuration + DateDiff("n", CedaFullDateToVb(tab_DEMTABLIVE.GetColumnValues(demRow, 0)), CedaFullDateToVb(tab_DEMTABLIVE.GetColumnValues(demRow, 1)))

                                                    'To calculate actual jobs, need to check JOBTAB & get the details from JOBTAB.
                                                    'Getting the urno for each demand & calculate the actual job times
                                                    'KKH 3)
                                                    Debug.Print demandDuration
                                                    tempDemUrno = tab_DEMTABLIVE.GetColumnValues(demRow, 10)
                                                    jobList = tab_JOBTABDEM.GetLinesByColumnValue(3, tempDemUrno, 0)
                                                    Debug.Print jobList
                                                    
                                                    jobListCount = ItemCount(jobList, ",")

                                                    'if jobListCount is not zero, then calculate the actual job
                                                    If jobListCount > 0 Then
                                                        jobDemCount = jobDemCount + jobListCount
                                                        jobDemUsed = jobDemUsed + 1
                                                        'if more than one person worked for the same demand, need to calculate
                                                        For m = 1 To jobListCount Step 1
                                                            jobRow = GetItem(jobList, m, ",")
                                                            jobDemDuration = jobDemDuration + DateDiff("n", CedaFullDateToVb(tab_JOBTABDEM.GetColumnValues(jobRow, 1)), CedaFullDateToVb(tab_JOBTABDEM.GetColumnValues(jobRow, 2)))
                                                        Next m
                                                    End If
                                                    'End - calculation of actual jobs.
                                                
                                                End If
                                            Next k

                                            sumDemRowCount = sumDemRowCount + demRowCount
                                            sumDemandDuration = sumDemandDuration + demandDuration
                                            sumJobDemCount = sumJobDemCount + jobDemCount
                                            sumJobDemDuration = sumJobDemDuration + jobDemDuration
                                            
                                            'Added by KKH on 13/002/2006
                                            'sumActualJob = sumActualJob +

                                            'Get the demands for the flight from planned demands
                                            demRowListInbound = tab_DEMTABPLAN.GetLinesByColumnValue(4, demOuri, 0)
                                            demRowListInboundCount = ItemCount(demRowListInbound, ",")
                                            demPlanRowCount = 0
                                            demPlanDemandDuration = 0

                                            For k = 1 To demRowListInboundCount Step 1
                                                demRow = GetItem(demRowListInbound, k, ",")
                                                If Mid(tab_DEMTABPLAN.GetColumnValue(demRow, 3), 2, 1) <> "1" Then
                                                    tempUrud = tab_DEMTABPLAN.GetColumnValues(demRow, 8)

                                                    'Get the rowvalue of tempUrud from RUDTAB & get the URUE value to check in RUETAB
                                                    tempLine = frmData.tab_RUDTAB.GetLinesByColumnValue(0, tempUrud, 0)
                                                    tempUrue = frmData.tab_RUDTAB.GetColumnValues(tempLine, 1)
                                                     
                                                    'Get the rowvalue of tempUrue from RUETAB & get the Recordname from RUETAB
                                                    tempLine = frmData.tab_RUETAB.GetLinesByColumnValue(0, tempUrue, 0)
                                                    ruleName = frmData.tab_RUETAB.GetColumnValues(tempLine, 2)
                                                    If InStr(1, planRuleNameStr, ruleName) = 0 Then planRuleNameStr = planRuleNameStr & ruleName & ";"

                                                    demPlanRowCount = demPlanRowCount + 1
                                                    demPlanDemandDuration = demPlanDemandDuration + DateDiff("n", CedaFullDateToVb(tab_DEMTABPLAN.GetColumnValues(demRow, 0)), CedaFullDateToVb(tab_DEMTABPLAN.GetColumnValues(demRow, 1)))
                                                End If
                                            Next k

                                            sumPlanDemRowCount = sumPlanDemRowCount + demPlanRowCount
                                            sumPlanDemandDuration = sumPlanDemandDuration + demPlanDemandDuration

                                            ' KKH - 30/01/2007
                                            ' Get job that without demand JOBTAB.UDEM = 0
'                                            jobStrWhere = "where UDEM = 0 and UAFT = '" & demOuri & "' or UDEM = 0 and UAFT = '" & demOuro & "'"
'
'                                            frmData.tab_JOBTAB.ResetContent
'                                            frmData.tab_JOBTAB.Refresh
'                                            frmData.LoadData frmData.tab_JOBTAB, jobStrWhere
'
'                                            jobCount = 0
'                                            jobDuration = 0
'                                            jobCount = frmData.tab_JOBTAB.GetLineCount
'
'                                            If jobCount > 0 Then
'                                                For p = 0 To jobCount - 1 Step 1
'                                                    jobDuration = jobDuration + DateDiff("n", CedaFullDateToVb(frmData.tab_JOBTAB.GetColumnValues(p, 2)), CedaFullDateToVb(frmData.tab_JOBTAB.GetColumnValues(p, 3)))
'                                               Next p
'                                            End If
'                                            ' To sum up the job with demand and job w/o demand
'                                            jobCount = frmData.tab_JOBTAB.GetLineCount + jobDemCount
'                                            jobDuration = jobDuration + jobDemDuration
'
'                                            tab_DEMTABSHOW.InsertTextLine inBoundStr & "," & outBoundStr & "," & fltAcType & "," & planFltAcType & "," & fltStoa & "," & fltStod & "," & fltEtai & "," & fltEtdi & "," & jobCount & "," & Int(jobDuration / 60) & "Hrs:" & (jobDuration Mod 60) & "Min" & "," & jobDemUsed & "," & demRowCount & "," & Int(demandDuration / 60) & "Hrs:" & (demandDuration Mod 60) & "Min," & demPlanRowCount & "," & Int(demPlanDemandDuration / 60) & "Hrs:" & (demPlanDemandDuration Mod 60) & "Min," & ruleNameStr & "," & planRuleNameStr, True
                                            'Changed by KKH to separate the Actual Jobs Duration to Hrs and Min
                                            'tab_DEMTABSHOW.InsertTextLine inBoundStr & "," & outBoundStr & "," & fltAcType & "," & planFltAcType & "," & fltStoa & "," & fltStod & "," & fltEtai & "," & fltEtdi & "," & jobDemCount & "," & Int(jobDemDuration / 60) & "Hrs:" & (jobDemDuration Mod 60) & "Min" & "," & jobDemUsed & "," & demRowCount & "," & Int(demandDuration / 60) & "Hrs:" & (demandDuration Mod 60) & "Min," & demPlanRowCount & "," & Int(demPlanDemandDuration / 60) & "Hrs:" & (demPlanDemandDuration Mod 60) & "Min," & ruleNameStr & "," & planRuleNameStr, True
                                            tab_DEMTABSHOW.InsertTextLine inBoundStr & "," & outBoundStr & "," & fltAcType & "," & planFltAcType & "," & fltStoa & "," & fltStod & "," & fltEtai & "," & fltEtdi & "," & jobDemCount & "," & Int(jobDemDuration / 60) & "," & Int(jobDemDuration Mod 60) & "," & jobDemUsed & "," & demRowCount & "," & Int(demandDuration / 60) & "Hrs:" & (demandDuration Mod 60) & "Min," & demPlanRowCount & "," & Int(demPlanDemandDuration / 60) & "Hrs:" & (demPlanDemandDuration Mod 60) & "Min," & ruleNameStr & "," & planRuleNameStr, True
                                            'Print #FileNumber, "," & inBoundStr & "," & outBoundStr & "," & fltAcType & "," & planFltAcType & "," & fltStoa & "," & fltStod & "," & fltEtai & "," & fltEtdi & "," & demRowCount & "," & Int(demandDuration / 60) & ":" & (demandDuration Mod 60) & "," & demPlanRowCount & "," & Int(demPlanDemandDuration / 60) & ":" & (demPlanDemandDuration Mod 60) & "," & ruleNameStr
                                        End If 'End of flight check

                                End If 'End of if loop for turn around demand
                            End If 'End of if loop for fltUrnoList check
                            
                            'If the demand is for Inbound flight
                            If tempDETY = "1" Then
                                demOuri = tab_DEMTABLIVE.GetColumnValues(j, 4)

                                If InStr(1, fltUrnoList, demOuri) = 0 Then
                                     
                                    fltUrnoList = fltUrnoList & demOuri & ","
                               
                                    'Get the information for inbound flight
                                    fltLine = frmData.tab_AFTTAB.GetLinesByColumnValue(0, demOuri, 0)
                                    If fltLine <> "" Then
                                        fltAdid = frmData.tab_AFTTAB.GetColumnValue(fltLine, 2)
                                        fltAcType = frmData.tab_AFTTAB.GetColumnValue(fltLine, 10)
                                        fltStoa = frmData.tab_AFTTAB.GetColumnValue(fltLine, 3)
                                        fltEtai = frmData.tab_AFTTAB.GetColumnValue(fltLine, 11)
                                        inBoundStr = frmData.tab_AFTTAB.GetColumnValue(fltLine, 1)


                                        'Get the planned AC type from PLANNED AFTTAB
                                        fltLine1 = frmData.tab_AFTTABPLAN.GetLinesByColumnValue(0, demOuri, 0)
                                        If fltLine1 <> "" Then
                                            planFltAcType = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine1, 10)
                                        Else
                                            planFltAcType = ""
                                        End If
                                   
                                            
                                        'If any inbound demands are there for the flight, which are not available in turn around demand
                                        demRowListInbound = tab_DEMTABLIVE.GetLinesByColumnValue(4, demOuri, 0)
                                        demRowListInboundCount = ItemCount(demRowListInbound, ",")
                                        demRowCount = 0

                                        For k = 1 To demRowListInboundCount Step 1
                                            demRow = GetItem(demRowListInbound, k, ",")
                                            If Mid(tab_DEMTABLIVE.GetColumnValue(demRow, 3), 2, 1) <> "1" Then
                                                tempUrud = tab_DEMTABLIVE.GetColumnValues(demRow, 8)
                                                demOuro = tab_DEMTABLIVE.GetColumnValues(demRow, 5)

                                                'Get the rowvalue of tempUrud from RUDTAB & get the URUE value to check in RUETAB
                                                tempLine = frmData.tab_RUDTAB.GetLinesByColumnValue(0, tempUrud, 0)
                                                tempUrue = frmData.tab_RUDTAB.GetColumnValues(tempLine, 1)
                                                 
                                                'Get the rowvalue of tempUrue from RUETAB & get the Recordname from RUETAB
                                                tempLine = frmData.tab_RUETAB.GetLinesByColumnValue(0, tempUrue, 0)
                                                ruleName = frmData.tab_RUETAB.GetColumnValues(tempLine, 2)
                                                If InStr(1, ruleNameStr, ruleName) = 0 Then ruleNameStr = ruleNameStr & ruleName & ";"

                                                demRowCount = demRowCount + 1
                                                
                                                'demandDuration = demandDuration + DateDiff("h", CedaFullDateToVb(tab_DEMTABLIVE.GetColumnValues(demRow, 0)), CedaFullDateToVb(tab_DEMTABLIVE.GetColumnValues(demRow, 1)))
                                                demandDuration = demandDuration + DateDiff("n", CedaFullDateToVb(tab_DEMTABLIVE.GetColumnValues(demRow, 0)), CedaFullDateToVb(tab_DEMTABLIVE.GetColumnValues(demRow, 1)))

                                                'To calculate actual jobs, need to check JOBTAB & get the details from JOBTAB.
                                                'Getting the urno for each demand & calculate the actual job times
                                                'KKH 4)
                                                tempDemUrno = tab_DEMTABLIVE.GetColumnValues(demRow, 10)
                                                jobList = tab_JOBTABDEM.GetLinesByColumnValue(3, tempDemUrno, 0)
                                                jobListCount = ItemCount(jobList, ",")

                                                'if jobListCount is not zero, then calculate the actual job
                                                If jobListCount > 0 Then
                                                    jobDemCount = jobDemCount + jobListCount
                                                    jobDemUsed = jobDemUsed + 1
                                                    'if more than one person worked for the same demand, need to calculate
                                                    For m = 1 To jobListCount Step 1
                                                        jobRow = GetItem(jobList, m, ",")
                                                        jobDemDuration = jobDemDuration + DateDiff("n", CedaFullDateToVb(tab_JOBTABDEM.GetColumnValues(jobRow, 1)), CedaFullDateToVb(tab_JOBTABDEM.GetColumnValues(jobRow, 2)))
                                                    Next m
                                                End If
                                                'End - calculation of actual jobs.
                                            End If
                                        Next k

                                        sumDemRowCount = sumDemRowCount + demRowCount
                                        sumDemandDuration = sumDemandDuration + demandDuration
                                        sumJobDemCount = sumJobDemCount + jobDemCount
                                        sumJobDemDuration = sumJobDemDuration + jobDemDuration

                                        If demOuro <> "" Then
                                            fltUrnoList = fltUrnoList & demOuro & ","
                                            fltLine = frmData.tab_AFTTAB.GetLinesByColumnValue(0, demOuro, 0)

                                            If fltLine <> "" Then
                                                fltStod = frmData.tab_AFTTAB.GetColumnValue(fltLine, 4)
                                                fltEtdi = frmData.tab_AFTTAB.GetColumnValue(fltLine, 12)
                                                outBoundStr = frmData.tab_AFTTAB.GetColumnValue(fltLine, 1)
                                            Else
                                                fltStod = ""
                                                fltEtdi = ""
                                                outBoundStr = ""
                                            End If
                                        End If


                                        'Get the demands for the flight from planned demands
                                        demRowListInbound = tab_DEMTABPLAN.GetLinesByColumnValue(4, demOuri, 0)
                                        demRowListInboundCount = ItemCount(demRowListInbound, ",")
                                        demPlanRowCount = 0
                                        demPlanDemandDuration = 0

                                        For k = 1 To demRowListInboundCount Step 1
                                            demRow = GetItem(demRowListInbound, k, ",")
                                            If Mid(tab_DEMTABPLAN.GetColumnValue(demRow, 3), 2, 1) <> "1" Then
                                                tempUrud = tab_DEMTABPLAN.GetColumnValues(demRow, 8)

                                                'Get the rowvalue of tempUrud from RUDTAB & get the URUE value to check in RUETAB
                                                tempLine = frmData.tab_RUDTAB.GetLinesByColumnValue(0, tempUrud, 0)
                                                tempUrue = frmData.tab_RUDTAB.GetColumnValues(tempLine, 1)
                                                 
                                                'Get the rowvalue of tempUrue from RUETAB & get the Recordname from RUETAB
                                                tempLine = frmData.tab_RUETAB.GetLinesByColumnValue(0, tempUrue, 0)
                                                ruleName = frmData.tab_RUETAB.GetColumnValues(tempLine, 2)
                                                If InStr(1, planRuleNameStr, ruleName) = 0 Then planRuleNameStr = planRuleNameStr & ruleName & ";"

                                                demPlanRowCount = demPlanRowCount + 1

                                                demPlanDemandDuration = demPlanDemandDuration + DateDiff("n", CedaFullDateToVb(tab_DEMTABPLAN.GetColumnValues(demRow, 0)), CedaFullDateToVb(tab_DEMTABPLAN.GetColumnValues(demRow, 1)))
                                            End If
                                        Next k

                                        sumPlanDemRowCount = sumPlanDemRowCount + demPlanRowCount
                                        sumPlanDemandDuration = sumPlanDemandDuration + demPlanDemandDuration
                                        
                                        ' KKH - 30/01/2007
                                        ' Get job that without demand JOBTAB.UDEM = 0
'                                        jobStrWhere = "where UDEM = 0 and UAFT = '" & demOuri & "'"
'
'                                        frmData.tab_JOBTAB.ResetContent
'                                        frmData.tab_JOBTAB.Refresh
'                                        frmData.LoadData frmData.tab_JOBTAB, jobStrWhere
'
'                                        jobCount = 0
'                                        jobDuration = 0
'                                        jobCount = frmData.tab_JOBTAB.GetLineCount
'
'                                        If jobCount > 0 Then
'                                            For p = 0 To jobCount - 1 Step 1
'                                                jobDuration = jobDuration + DateDiff("n", CedaFullDateToVb(frmData.tab_JOBTAB.GetColumnValues(p, 2)), CedaFullDateToVb(frmData.tab_JOBTAB.GetColumnValues(p, 3)))
'                                           Next p
'                                        End If
'                                        ' To sum up the job with demand and job w/o demand
'                                        jobCount = frmData.tab_JOBTAB.GetLineCount + jobDemCount
'                                        jobDuration = jobDuration + jobDemDuration
'
'                                        tab_DEMTABSHOW.InsertTextLine inBoundStr & "," & outBoundStr & "," & fltAcType & "," & planFltAcType & "," & fltStoa & "," & fltStod & "," & fltEtai & "," & fltEtdi & "," & jobCount & "," & Int(jobDuration / 60) & "Hrs:" & (jobDuration Mod 60) & "Min" & "," & jobDemUsed & "," & demRowCount & "," & Int(demandDuration / 60) & "Hrs:" & (demandDuration Mod 60) & "Min," & demPlanRowCount & "," & Int(demPlanDemandDuration / 60) & "Hrs:" & (demPlanDemandDuration Mod 60) & "Min," & ruleNameStr & "," & planRuleNameStr, True
                                        'Changed by KKH to separate the Actual Jobs Duration to Hrs and Min
                                        'tab_DEMTABSHOW.InsertTextLine inBoundStr & "," & outBoundStr & "," & fltAcType & "," & planFltAcType & "," & fltStoa & "," & fltStod & "," & fltEtai & "," & fltEtdi & "," & jobDemCount & "," & Int(jobDemDuration / 60) & "Hrs:" & (jobDemDuration Mod 60) & "Min" & "," & jobDemUsed & "," & demRowCount & "," & Int(demandDuration / 60) & "Hrs:" & (demandDuration Mod 60) & "Min," & demPlanRowCount & "," & Int(demPlanDemandDuration / 60) & "Hrs:" & (demPlanDemandDuration Mod 60) & "Min," & ruleNameStr & "," & planRuleNameStr, True
                                        tab_DEMTABSHOW.InsertTextLine inBoundStr & "," & outBoundStr & "," & fltAcType & "," & planFltAcType & "," & fltStoa & "," & fltStod & "," & fltEtai & "," & fltEtdi & "," & jobDemCount & "," & Int(jobDemDuration / 60) & "," & Int(jobDemDuration Mod 60) & "," & jobDemUsed & "," & demRowCount & "," & Int(demandDuration / 60) & "Hrs:" & (demandDuration Mod 60) & "Min," & demPlanRowCount & "," & Int(demPlanDemandDuration / 60) & "Hrs:" & (demPlanDemandDuration Mod 60) & "Min," & ruleNameStr & "," & planRuleNameStr, True
                                        'Print #FileNumber, "," & inBoundStr & "," & outBoundStr & "," & fltAcType & "," & planFltAcType & "," & fltStoa & "," & fltStod & "," & fltEtai & "," & fltEtdi & "," & demRowCount & "," & Int(demandDuration / 60) & ":" & (demandDuration Mod 60) & "," & demPlanRowCount & "," & Int(demPlanDemandDuration / 60) & ":" & (demPlanDemandDuration Mod 60) & "," & ruleNameStr

                                    End If 'Flight check
                                End If 'End of if loop for fltUrnoList
                            End If 'End of if loop for Inbound Flight

                            
                            'If the demand is for Inbound flight
                            If tempDETY = "2" Then
                                demOuro = tab_DEMTABLIVE.GetColumnValues(j, 5)

                                If InStr(1, fltUrnoList, demOuro) = 0 Then
                                 
                                    fltUrnoList = fltUrnoList & demOuro & ","
                                    
                                    'Get the information for outbound flight
                                    fltLine = frmData.tab_AFTTAB.GetLinesByColumnValue(0, demOuro, 0)
                                    If fltLine <> "" Then
                                        fltStod = frmData.tab_AFTTAB.GetColumnValue(fltLine, 4)
                                        fltEtdi = frmData.tab_AFTTAB.GetColumnValue(fltLine, 12)
                                        outBoundStr = frmData.tab_AFTTAB.GetColumnValue(fltLine, 1)
                                        fltAdid = frmData.tab_AFTTAB.GetColumnValue(fltLine, 2)
                                        fltAcType = frmData.tab_AFTTAB.GetColumnValue(fltLine, 10)

                                        'Get the planned AC type from PLANNED AFTTAB
                                        fltLine1 = frmData.tab_AFTTABPLAN.GetLinesByColumnValue(0, demOuro, 0)
                                        If fltLine1 <> "" Then
                                            planFltAcType = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine1, 10)
                                        Else
                                            planFltAcType = ""
                                        End If
                                   
                                    
                                        'If any outbound demands are there for the flight, which are not available in turn around demand
                                        demRowListOutbound = tab_DEMTABLIVE.GetLinesByColumnValue(5, demOuro, 0)
                                        demRowListOutboundCount = ItemCount(demRowListOutbound, ",")
                                        demRowCount = 0

                                        For k = 1 To demRowListOutboundCount Step 1
                                            demRow = GetItem(demRowListOutbound, k, ",")
                                            If Mid(tab_DEMTABLIVE.GetColumnValue(demRow, 3), 2, 1) <> "1" Then
                                                tempUrud = tab_DEMTABLIVE.GetColumnValues(demRow, 8)
                                                demOuri = tab_DEMTABLIVE.GetColumnValues(demRow, 4)

                                                'Get the rowvalue of tempUrud from RUDTAB & get the URUE value to check in RUETAB
                                                tempLine = frmData.tab_RUDTAB.GetLinesByColumnValue(0, tempUrud, 0)
                                                tempUrue = frmData.tab_RUDTAB.GetColumnValues(tempLine, 1)
                                                 
                                                'Get the rowvalue of tempUrue from RUETAB & get the Recordname from RUETAB
                                                tempLine = frmData.tab_RUETAB.GetLinesByColumnValue(0, tempUrue, 0)
                                                ruleName = frmData.tab_RUETAB.GetColumnValues(tempLine, 2)
                                                If InStr(1, ruleNameStr, ruleName) = 0 Then ruleNameStr = ruleNameStr & ruleName & ";"

                                                demRowCount = demRowCount + 1
                                                
                                                'demandDuration = demandDuration + DateDiff("h", CedaFullDateToVb(tab_DEMTABLIVE.GetColumnValues(demRow, 0)), CedaFullDateToVb(tab_DEMTABLIVE.GetColumnValues(demRow, 1)))
                                                demandDuration = demandDuration + DateDiff("n", CedaFullDateToVb(tab_DEMTABLIVE.GetColumnValues(demRow, 0)), CedaFullDateToVb(tab_DEMTABLIVE.GetColumnValues(demRow, 1)))

                                                'To calculate actual jobs, need to check JOBTAB & get the details from JOBTAB.
                                                'Getting the urno for each demand & calculate the actual job times
                                                'KKh 5)
                                                tempDemUrno = tab_DEMTABLIVE.GetColumnValues(demRow, 10)
                                                jobList = tab_JOBTABDEM.GetLinesByColumnValue(3, tempDemUrno, 0)
                                                jobListCount = ItemCount(jobList, ",")

                                                'if jobListCount is not zero, then calculate the actual job
                                                If jobListCount > 0 Then
                                                    jobDemCount = jobDemCount + jobListCount
                                                    jobDemUsed = jobDemUsed + 1
                                                    'if more than one person worked for the same demand, need to calculate
                                                    For m = 1 To jobListCount Step 1
                                                        jobRow = GetItem(jobList, m, ",")
                                                        jobDemDuration = jobDemDuration + DateDiff("n", CedaFullDateToVb(tab_JOBTABDEM.GetColumnValues(jobRow, 1)), CedaFullDateToVb(tab_JOBTABDEM.GetColumnValues(jobRow, 2)))
                                                    Next m
                                                End If
                                                'End - calculation of actual jobs.
                                            End If
                                        Next k

                                        sumDemRowCount = sumDemRowCount + demRowCount
                                        sumDemandDuration = sumDemandDuration + demandDuration
                                        sumJobDemCount = sumJobDemCount + jobDemCount
                                        sumJobDemDuration = sumJobDemDuration + jobDemDuration


                                        If demOuri <> "" Then
                                            fltUrnoList = fltUrnoList & demOuri & ","
                                            fltLine = frmData.tab_AFTTAB.GetLinesByColumnValue(0, demOuri, 0)

                                            If fltLine <> "" Then
                                                fltStoa = frmData.tab_AFTTAB.GetColumnValue(fltLine, 3)
                                                fltEtai = frmData.tab_AFTTAB.GetColumnValue(fltLine, 11)
                                                inBoundStr = frmData.tab_AFTTAB.GetColumnValue(fltLine, 1)
                                            Else
                                                fltStoa = ""
                                                fltEtai = ""
                                                inBoundStr = ""
                                            End If
                                        End If

                                        'Get the demands for the flight from planned demands
                                        demRowListOutbound = tab_DEMTABPLAN.GetLinesByColumnValue(5, demOuri, 0)
                                        demRowListOutboundCount = ItemCount(demRowListInbound, ",")
                                        demPlanRowCount = 0
                                        demPlanDemandDuration = 0

                                        For k = 1 To demRowListOutboundCount Step 1
                                            demRow = GetItem(demRowListInbound, k, ",")
                                            If Mid(tab_DEMTABPLAN.GetColumnValue(demRow, 3), 2, 1) <> "1" Then
                                                tempUrud = tab_DEMTABPLAN.GetColumnValues(demRow, 8)

                                                'Get the rowvalue of tempUrud from RUDTAB & get the URUE value to check in RUETAB
                                                tempLine = frmData.tab_RUDTAB.GetLinesByColumnValue(0, tempUrud, 0)
                                                tempUrue = frmData.tab_RUDTAB.GetColumnValues(tempLine, 1)
                                                 
                                                'Get the rowvalue of tempUrue from RUETAB & get the Recordname from RUETAB
                                                tempLine = frmData.tab_RUETAB.GetLinesByColumnValue(0, tempUrue, 0)
                                                ruleName = frmData.tab_RUETAB.GetColumnValues(tempLine, 2)
                                                If InStr(1, planRuleNameStr, ruleName) = 0 Then planRuleNameStr = planRuleNameStr & ruleName & ";"

                                                demPlanRowCount = demPlanRowCount + 1

                                                demPlanDemandDuration = demPlanDemandDuration + DateDiff("n", CedaFullDateToVb(tab_DEMTABPLAN.GetColumnValues(demRow, 0)), CedaFullDateToVb(tab_DEMTABPLAN.GetColumnValues(demRow, 1)))
                                            End If
                                        Next k

                                        sumPlanDemRowCount = sumPlanDemRowCount + demPlanRowCount
                                        sumPlanDemandDuration = sumPlanDemandDuration + demPlanDemandDuration

                                        ' KKH - 30/01/2007
                                        ' Get job that without demand JOBTAB.UDEM = 0
'                                        jobStrWhere = "where UDEM = 0 and UAFT = '" & demOuro & "'"
'
'                                        frmData.tab_JOBTAB.ResetContent
'                                        frmData.tab_JOBTAB.Refresh
'                                        frmData.LoadData frmData.tab_JOBTAB, jobStrWhere
'
'                                        jobCount = 0
'                                        jobDuration = 0
'                                        jobCount = frmData.tab_JOBTAB.GetLineCount
'
'                                        Debug.Print jobStrWhere
'
'                                        If jobCount > 0 Then
'                                            For p = 0 To jobCount - 1 Step 1
'                                                jobDuration = jobDuration + DateDiff("n", CedaFullDateToVb(frmData.tab_JOBTAB.GetColumnValues(p, 2)), CedaFullDateToVb(frmData.tab_JOBTAB.GetColumnValues(p, 3)))
'                                           Next p
'                                        End If
'                                        ' To sum up the job with demand and job w/o demand
'                                        jobCount = frmData.tab_JOBTAB.GetLineCount + jobDemCount
'                                        jobDuration = jobDuration + jobDemDuration
'                                        tab_DEMTABSHOW.InsertTextLine inBoundStr & "," & outBoundStr & "," & fltAcType & "," & planFltAcType & "," & fltStoa & "," & fltStod & "," & fltEtai & "," & fltEtdi & "," & jobCount & "," & Int(jobDuration / 60) & "Hrs:" & (jobDuration Mod 60) & "Min" & "," & jobDemUsed & "," & demRowCount & "," & Int(demandDuration / 60) & "Hrs:" & (demandDuration Mod 60) & "Min," & demPlanRowCount & "," & Int(demPlanDemandDuration / 60) & "Hrs:" & (demPlanDemandDuration Mod 60) & "Min," & ruleNameStr & "," & planRuleNameStr, True
                                        'Changed by KKH to separate the Actual Jobs Duration to Hrs and Min
                                        'tab_DEMTABSHOW.InsertTextLine inBoundStr & "," & outBoundStr & "," & fltAcType & "," & planFltAcType & "," & fltStoa & "," & fltStod & "," & fltEtai & "," & fltEtdi & "," & jobDemCount & "," & Int(jobDemDuration / 60) & "Hrs:" & (jobDemDuration Mod 60) & "Min" & "," & jobDemUsed & "," & demRowCount & "," & Int(demandDuration / 60) & "Hrs:" & (demandDuration Mod 60) & "Min," & demPlanRowCount & "," & Int(demPlanDemandDuration / 60) & "Hrs:" & (demPlanDemandDuration Mod 60) & "Min," & ruleNameStr & "," & planRuleNameStr, True
                                        tab_DEMTABSHOW.InsertTextLine inBoundStr & "," & outBoundStr & "," & fltAcType & "," & planFltAcType & "," & fltStoa & "," & fltStod & "," & fltEtai & "," & fltEtdi & "," & jobDemCount & "," & Int(jobDemDuration / 60) & "," & Int(jobDemDuration Mod 60) & "," & jobDemUsed & "," & demRowCount & "," & Int(demandDuration / 60) & "Hrs:" & (demandDuration Mod 60) & "Min," & demPlanRowCount & "," & Int(demPlanDemandDuration / 60) & "Hrs:" & (demPlanDemandDuration Mod 60) & "Min," & ruleNameStr & "," & planRuleNameStr, True
                                        'Print #FileNumber, "," & inBoundStr & "," & outBoundStr & "," & fltAcType & "," & planFltAcType & "," & fltStoa & "," & fltStod & "," & fltEtai & "," & fltEtdi & "," & demRowCount & "," & Int(demandDuration / 60) & ":" & (demandDuration Mod 60) & "," & demPlanRowCount & "," & Int(demPlanDemandDuration / 60) & ":" & (demPlanDemandDuration Mod 60) & "," & ruleNameStr
                                        
                                    End If 'Flight check
                                End If 'End of if loop for fltUrnoList
                            End If 'End of if loop for outbound demand
          
                         End If ' End of if loop for whether demand is a valid demand or not ... Till here DEMTABLIVE demands

                    Next j

                    'If more flights available in planned Demands then .. need to write script to cover those as well
                    'This is according to my understanding .. Thanooj.
                    'If its not required then remove below lines starting from ************** to ***************** .... Thanooj
                    '*********************************************************************************** Thanooj
                    
                     For j = 0 To demPlanCount - 1 Step 1
                         
                         demRowCount = 0
                         demandDuration = 0
                         ruleNameStr = ""
                         inBoundStr = ""
                         outBoundStr = ""
                         fltAcType = ""
                         planRuleNameStr = ""
                        
                         'Check whether demand is valid or not
                         If Mid(tab_DEMTABPLAN.GetColumnValue(j, 3), 2, 1) <> "1" Then
                             tempDETY = tab_DEMTABPLAN.GetColumnValues(j, 2)

                             'If its turn around demand
                             If tempDETY = "0" Then
                                 demOuri = tab_DEMTABPLAN.GetColumnValues(j, 4)
                                 demOuro = tab_DEMTABPLAN.GetColumnValues(j, 5)

                                 If InStr(1, fltUrnoList, demOuri) = 0 And InStr(1, fltUrnoList, demOuri) = 0 Then
                                
                                        fltUrnoList = fltUrnoList & demOuri & ","
                                        fltUrnoList = fltUrnoList & demOuro & ","
                                        
                                        'Get the information for inbound flight
                                        fltLine = frmData.tab_AFTTABPLAN.GetLinesByColumnValue(0, demOuri, 0)
                                        If fltLine <> "" Then
                                            fltAdid = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine, 2)
                                            planFltAcType = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine, 10)
                                            fltStoa = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine, 3)
                                            fltEtai = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine, 11)
                                            inBoundStr = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine, 1)
                                        Else
                                            inBoundStr = ""
                                            fltAcType = ""
                                            fltStoa = ""
                                            fltEtai = ""
                                            fltAcType = ""
                                        End If
                                    
                                        'Get the information for outbound flight
                                        fltLine = frmData.tab_AFTTABPLAN.GetLinesByColumnValue(0, demOuro, 0)
                                        If fltLine <> "" Then
                                            fltStod = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine, 4)
                                            fltEtdi = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine, 12)
                                            outBoundStr = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine, 1)
                                        Else
                                            fltStod = ""
                                            fltEtdi = ""
                                            outBoundStr = ""
                                        End If
                        
                                        'If flight is available, then only need to show the demands count, otherwise leave it.
                                        If inBoundStr <> "" And outBoundStr <> "" Then
                                            'Get the demands for the flight from planned demands
                                            demRowListInbound = tab_DEMTABPLAN.GetLinesByColumnValue(4, demOuri, 0)
                                            demRowListInboundCount = ItemCount(demRowListInbound, ",")
                                            demPlanRowCount = 0
                                            demPlanDemandDuration = 0

                                            For k = 1 To demRowListInboundCount Step 1
                                                demRow = GetItem(demRowListInbound, k, ",")
                                                If Mid(tab_DEMTABPLAN.GetColumnValue(demRow, 3), 2, 1) <> "1" Then
                                                    tempUrud = tab_DEMTABPLAN.GetColumnValues(demRow, 8)

                                                    'Get the rowvalue of tempUrud from RUDTAB & get the URUE value to check in RUETAB
                                                    tempLine = frmData.tab_RUDTAB.GetLinesByColumnValue(0, tempUrud, 0)
                                                    tempUrue = frmData.tab_RUDTAB.GetColumnValues(tempLine, 1)
                                                     
                                                    'Get the rowvalue of tempUrue from RUETAB & get the Recordname from RUETAB
                                                    tempLine = frmData.tab_RUETAB.GetLinesByColumnValue(0, tempUrue, 0)
                                                    ruleName = frmData.tab_RUETAB.GetColumnValues(tempLine, 2)
                                                    If InStr(1, planRuleNameStr, ruleName) = 0 Then planRuleNameStr = planRuleNameStr & ruleName & ";"

                                                    demPlanRowCount = demPlanRowCount + 1
                                                    demPlanDemandDuration = demPlanDemandDuration + DateDiff("n", CedaFullDateToVb(tab_DEMTABPLAN.GetColumnValues(demRow, 0)), CedaFullDateToVb(tab_DEMTABPLAN.GetColumnValues(demRow, 1)))
                                                End If
                                            Next k

                                            sumPlanDemRowCount = sumPlanDemRowCount + demPlanRowCount
                                            sumPlanDemandDuration = sumPlanDemandDuration + demPlanDemandDuration
                                            'Changed by KKH to separate the Actual Jobs Duration to Hrs and Min
                                            'tab_DEMTABSHOW.InsertTextLine inBoundStr & "," & outBoundStr & ", ," & planFltAcType & "," & fltStoa & "," & fltStod & "," & fltEtai & "," & fltEtdi & ",0,0Hrs:0Min,0,0,0Hrs:0Min," & demPlanRowCount & "," & Int(demPlanDemandDuration / 60) & "Hrs:" & (demPlanDemandDuration Mod 60) & "Min," & ruleNameStr & "," & planRuleNameStr, True
                                            tab_DEMTABSHOW.InsertTextLine inBoundStr & "," & outBoundStr & ", ," & planFltAcType & "," & fltStoa & "," & fltStod & "," & fltEtai & "," & fltEtdi & ",0,0,0,0,0,0Hrs:0Min," & demPlanRowCount & "," & Int(demPlanDemandDuration / 60) & "Hrs:" & (demPlanDemandDuration Mod 60) & "Min," & ruleNameStr & "," & planRuleNameStr, True
                                            'Print #FileNumber, "," & inBoundStr & "," & outBoundStr & "," & fltAcType & "," & planFltAcType & "," & fltStoa & "," & fltStod & "," & fltEtai & "," & fltEtdi & "," & demRowCount & "," & Int(demandDuration / 60) & ":" & (demandDuration Mod 60) & "," & demPlanRowCount & "," & Int(demPlanDemandDuration / 60) & ":" & (demPlanDemandDuration Mod 60) & "," & ruleNameStr
                                        End If 'End of flight check

                                End If 'End of if loop for turn around demand
                            End If 'End of if loop for fltUrnoList check
                            
                            'If the demand is for Inbound flight
                            If tempDETY = "1" Then
                                demOuri = tab_DEMTABPLAN.GetColumnValues(j, 4)

                                If InStr(1, fltUrnoList, demOuri) = 0 Then
                                     
                                    fltUrnoList = fltUrnoList & demOuri & ","
                               
                                    'Get the information for inbound flight
                                    fltLine = frmData.tab_AFTTABPLAN.GetLinesByColumnValue(0, demOuri, 0)
                                    If fltLine <> "" Then
                                        fltAdid = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine, 2)
                                        planFltAcType = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine, 10)
                                        fltStoa = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine, 3)
                                        fltEtai = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine, 11)
                                        inBoundStr = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine, 1)


                                        'Get the demands for the flight from planned demands
                                        demRowListInbound = tab_DEMTABPLAN.GetLinesByColumnValue(4, demOuri, 0)
                                        demRowListInboundCount = ItemCount(demRowListInbound, ",")
                                        demPlanRowCount = 0
                                        demPlanDemandDuration = 0

                                        For k = 1 To demRowListInboundCount Step 1
                                            demRow = GetItem(demRowListInbound, k, ",")
                                            If Mid(tab_DEMTABPLAN.GetColumnValue(demRow, 3), 2, 1) <> "1" Then
                                                tempUrud = tab_DEMTABPLAN.GetColumnValues(demRow, 8)
                                                demOuro = tab_DEMTABPLAN.GetColumnValues(demRow, 5)

                                                'Get the rowvalue of tempUrud from RUDTAB & get the URUE value to check in RUETAB
                                                tempLine = frmData.tab_RUDTAB.GetLinesByColumnValue(0, tempUrud, 0)
                                                tempUrue = frmData.tab_RUDTAB.GetColumnValues(tempLine, 1)
                                                 
                                                'Get the rowvalue of tempUrue from RUETAB & get the Recordname from RUETAB
                                                tempLine = frmData.tab_RUETAB.GetLinesByColumnValue(0, tempUrue, 0)
                                                ruleName = frmData.tab_RUETAB.GetColumnValues(tempLine, 2)
                                                If InStr(1, planRuleNameStr, ruleName) = 0 Then planRuleNameStr = planRuleNameStr & ruleName & ";"

                                                demPlanRowCount = demPlanRowCount + 1

                                                demPlanDemandDuration = demPlanDemandDuration + DateDiff("n", CedaFullDateToVb(tab_DEMTABPLAN.GetColumnValues(demRow, 0)), CedaFullDateToVb(tab_DEMTABPLAN.GetColumnValues(demRow, 1)))
                                            End If
                                        Next k

                                         If demOuro <> "" Then
                                            fltUrnoList = fltUrnoList & demOuro & ","
                                            fltLine = frmData.tab_AFTTAB.GetLinesByColumnValue(0, demOuro, 0)

                                            If fltLine <> "" Then
                                                fltStod = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine, 4)
                                                fltEtdi = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine, 12)
                                                outBoundStr = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine, 1)
                                            Else
                                                fltStod = ""
                                                fltEtdi = ""
                                                outBoundStr = ""
                                            End If
                                        End If

                                        sumPlanDemRowCount = sumPlanDemRowCount + demPlanRowCount
                                        sumPlanDemandDuration = sumPlanDemandDuration + demPlanDemandDuration
                                        'Changed by KKH to separate the Actual Jobs Duration to Hrs and Min
                                        'tab_DEMTABSHOW.InsertTextLine inBoundStr & "," & outBoundStr & ", ," & planFltAcType & "," & fltStoa & "," & fltStod & "," & fltEtai & "," & fltEtdi & ",0,0Hrs:0Min,0,0,0Hrs:0Min," & demPlanRowCount & "," & Int(demPlanDemandDuration / 60) & "Hrs:" & (demPlanDemandDuration Mod 60) & "Min," & ruleNameStr & "," & planRuleNameStr, True
                                        tab_DEMTABSHOW.InsertTextLine inBoundStr & "," & outBoundStr & ", ," & planFltAcType & "," & fltStoa & "," & fltStod & "," & fltEtai & "," & fltEtdi & ",0,0,0,0,0,0Hrs:0Min," & demPlanRowCount & "," & Int(demPlanDemandDuration / 60) & "Hrs:" & (demPlanDemandDuration Mod 60) & "Min," & ruleNameStr & "," & planRuleNameStr, True
                                        'Print #FileNumber, "," & inBoundStr & "," & outBoundStr & "," & fltAcType & "," & planFltAcType & "," & fltStoa & "," & fltStod & "," & fltEtai & "," & fltEtdi & "," & demRowCount & "," & Int(demandDuration / 60) & ":" & (demandDuration Mod 60) & "," & demPlanRowCount & "," & Int(demPlanDemandDuration / 60) & ":" & (demPlanDemandDuration Mod 60) & "," & ruleNameStr

                                    End If 'Flight check
                                End If 'End of if loop for fltUrnoList
                            End If 'End of if loop for Inbound Flight

                            
                            'If the demand is for Inbound flight
                            If tempDETY = "2" Then
                                demOuro = tab_DEMTABPLAN.GetColumnValues(j, 5)

                                If InStr(1, fltUrnoList, demOuro) = 0 Then
                                 
                                    fltUrnoList = fltUrnoList & demOuro & ","
                                    
                                    'Get the information for outbound flight
                                    fltLine = frmData.tab_AFTTABPLAN.GetLinesByColumnValue(0, demOuro, 0)
                                    If fltLine <> "" Then
                                        fltStod = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine, 4)
                                        fltEtdi = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine, 12)
                                        outBoundStr = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine, 1)
                                        fltAdid = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine, 2)
                                        planFltAcType = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine, 10)

                                        'Get the demands for the flight from planned demands
                                        demRowListOutbound = tab_DEMTABPLAN.GetLinesByColumnValue(5, demOuri, 0)
                                        demRowListOutboundCount = ItemCount(demRowListInbound, ",")
                                        demPlanRowCount = 0
                                        demPlanDemandDuration = 0

                                        For k = 1 To demRowListOutboundCount Step 1
                                            demRow = GetItem(demRowListInbound, k, ",")
                                            If Mid(tab_DEMTABPLAN.GetColumnValue(demRow, 3), 2, 1) <> "1" Then
                                                tempUrud = tab_DEMTABPLAN.GetColumnValues(demRow, 8)
                                                demOuri = tab_DEMTABPLAN.GetColumnValues(demRow, 4)


                                                'Get the rowvalue of tempUrud from RUDTAB & get the URUE value to check in RUETAB
                                                tempLine = frmData.tab_RUDTAB.GetLinesByColumnValue(0, tempUrud, 0)
                                                tempUrue = frmData.tab_RUDTAB.GetColumnValues(tempLine, 1)
                                                 
                                                'Get the rowvalue of tempUrue from RUETAB & get the Recordname from RUETAB
                                                tempLine = frmData.tab_RUETAB.GetLinesByColumnValue(0, tempUrue, 0)
                                                ruleName = frmData.tab_RUETAB.GetColumnValues(tempLine, 2)
                                                If InStr(1, planRuleNameStr, ruleName) = 0 Then planRuleNameStr = planRuleNameStr & ruleName & ";"

                                                demPlanRowCount = demPlanRowCount + 1

                                                demPlanDemandDuration = demPlanDemandDuration + DateDiff("n", CedaFullDateToVb(tab_DEMTABPLAN.GetColumnValues(demRow, 0)), CedaFullDateToVb(tab_DEMTABPLAN.GetColumnValues(demRow, 1)))
                                            End If
                                        Next k

                                        If demOuri <> "" Then
                                            fltUrnoList = fltUrnoList & demOuri & ","
                                            fltLine = frmData.tab_AFTTABPLAN.GetLinesByColumnValue(0, demOuri, 0)

                                            If fltLine <> "" Then
                                                fltStoa = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine, 3)
                                                fltEtai = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine, 11)
                                                inBoundStr = frmData.tab_AFTTABPLAN.GetColumnValue(fltLine, 1)
                                            Else
                                                fltStoa = ""
                                                fltEtai = ""
                                                inBoundStr = ""
                                            End If
                                        End If

                                        sumPlanDemRowCount = sumPlanDemRowCount + demPlanRowCount
                                        sumPlanDemandDuration = sumPlanDemandDuration + demPlanDemandDuration
                                        'Changed by KKH to separate the Actual Jobs Duration to Hrs and Min
                                        'tab_DEMTABSHOW.InsertTextLine inBoundStr & "," & outBoundStr & ", ," & planFltAcType & "," & fltStoa & "," & fltStod & "," & fltEtai & "," & fltEtdi & ",0,0Hrs:0Min,0,0,0Hrs:0Min," & demPlanRowCount & "," & Int(demPlanDemandDuration / 60) & "Hrs:" & (demPlanDemandDuration Mod 60) & "Min," & ruleNameStr & "," & planRuleNameStr, True
                                        tab_DEMTABSHOW.InsertTextLine inBoundStr & "," & outBoundStr & ", ," & planFltAcType & "," & fltStoa & "," & fltStod & "," & fltEtai & "," & fltEtdi & ",0,0,0,0,0,0Hrs:0Min," & demPlanRowCount & "," & Int(demPlanDemandDuration / 60) & "Hrs:" & (demPlanDemandDuration Mod 60) & "Min," & ruleNameStr & "," & planRuleNameStr, True
                                        'Print #FileNumber, "," & inBoundStr & "," & outBoundStr & "," & fltAcType & "," & planFltAcType & "," & fltStoa & "," & fltStod & "," & fltEtai & "," & fltEtdi & "," & demRowCount & "," & Int(demandDuration / 60) & ":" & (demandDuration Mod 60) & "," & demPlanRowCount & "," & Int(demPlanDemandDuration / 60) & ":" & (demPlanDemandDuration Mod 60) & "," & ruleNameStr
                                        
                                    End If 'Flight check
                                End If 'End of if loop for fltUrnoList
                            End If 'End of if loop for outbound demand
          
                         End If ' End of if loop for whether demand is a valid demand or not ... Till here DEMTABLIVE demands

                    Next j
                    '*********************************************************************************** Thanooj

                    ' Prepare script here to display the rows in tab_DEMTABSHOW to excel sheet
                    tab_DEMTABSHOW.Sort 4, True, False
                    tab_DEMTABSHOW.Refresh
                    demShowCount = tab_DEMTABSHOW.GetLineCount

                    For k = 0 To demShowCount - 1 Step 1
                        For k1 = 0 To 16 Step 1
                            demShowVal = tab_DEMTABSHOW.GetColumnValues(k, k1)
                            xlapp.Application.Cells(xlRow, k1 + 1).HorizontalAlignment = xlLeft
                            If (k1 = 4 Or k1 = 5 Or k1 = 6 Or k1 = 7) And demShowVal <> "" Then
                                xlapp.Application.Cells(xlRow, k1 + 1).Value = CedaFullDateToVb(Format(DateAdd("h", gUTCOffset, CedaFullDateToVb(demShowVal)), "yyyymmddhhmmss"))
                                'xlapp.Application.Cells(xlRow, k1 + 1).Value = demShowVal
                            Else
                                xlapp.Application.Cells(xlRow, k1 + 1).Value = demShowVal
                            End If
                        Next k1
                        xlRow = xlRow + 1
                    Next k
         
                    xlRow = xlRow + 1
                    
                    xlapp.Application.Cells(xlRow, 1).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 3).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 4).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 5).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 6).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 7).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 8).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 9).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 10).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 11).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 12).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 13).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 14).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 15).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 16).Font.Bold = True
                    
                    xlapp.Application.Cells(xlRow, 1).Value = "Sum:"
                    xlapp.Application.Cells(xlRow, 3).Value = "Flights: "
                    xlapp.Application.Cells(xlRow, 4).Value = ItemCount(fltUrnoList, ",") - 1
                    xlapp.Application.Cells(xlRow, 4).HorizontalAlignment = xlLeft
                    'Changed by KKH on 13/02/2007 show the Total Actual Jobs and Total Actual Jobs Duration
                    xlapp.Application.Cells(xlRow, 5).Value = "Actual Jobs:"
                    xlapp.Application.Cells(xlRow, 5).HorizontalAlignment = xlLeft
                    
                    xlapp.Application.Cells(xlRow, 6).Value = sumJobDemCount
                    xlapp.Application.Cells(xlRow, 6).HorizontalAlignment = xlLeft
                    
                    xlapp.Application.Cells(xlRow, 7).Value = "Actual Jobs Duration:"
                    xlapp.Application.Cells(xlRow, 7).HorizontalAlignment = xlLeft
                    
                    xlapp.Application.Cells(xlRow, 8).Value = Int(sumJobDemDuration / 60) & "Hrs:" & (sumJobDemDuration Mod 60) & "Min"
                    xlapp.Application.Cells(xlRow, 8).HorizontalAlignment = xlLeft
                    
                    xlapp.Application.Cells(xlRow, 9).Value = "Actual Demands:"
                    xlapp.Application.Cells(xlRow, 10).Value = sumDemRowCount
                    xlapp.Application.Cells(xlRow, 10).HorizontalAlignment = xlLeft
                    xlapp.Application.Cells(xlRow, 11).Value = "Actual Demands Duration:"
                    xlapp.Application.Cells(xlRow, 11).ColumnWidth = 25
                    xlapp.Application.Cells(xlRow, 12).Value = Int(sumDemandDuration / 60) & "Hrs:" & (sumDemandDuration Mod 60) & "Min"
                    xlapp.Application.Cells(xlRow, 12).HorizontalAlignment = xlLeft
                    xlapp.Application.Cells(xlRow, 13).Value = "Planned Demands:"
                    xlapp.Application.Cells(xlRow, 14).Value = sumPlanDemRowCount
                    xlapp.Application.Cells(xlRow, 14).HorizontalAlignment = xlLeft
                    xlapp.Application.Cells(xlRow, 15).Value = "Planned Demands Duration:"
                    xlapp.Application.Cells(xlRow, 15).ColumnWidth = 25
                    xlapp.Application.Cells(xlRow, 16).Value = Int(sumPlanDemandDuration / 60) & "Hrs:" & (sumPlanDemandDuration Mod 60) & "Min"
                    xlapp.Application.Cells(xlRow, 16).HorizontalAlignment = xlLeft
                    xlRow = xlRow + 2

                Else 'Template Flag
                    xlapp.Application.Cells(xlRow, 3).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 3).Value = "Flight Independent Demands wont be available for this template :" & templateName
                    xlRow = xlRow + 1
                End If
            Next l
            
            xlRow = xlRow + 1
            xlapp.Application.Cells(xlRow, 11).Value = "Report Genearation Time, End: " & Now
            xlapp.Application.Cells(xlRow, 11).Font.Bold = True
            
            xlbook.SaveAs dlgSave.FileName
            xlbook.Close
            xlapp.Quit
            MousePointer = vbDefault
            
            MsgBox "" & dlgSave.FileName & " created successfully"
        Else 'dlgSave if
            MsgBox "Please Enter Filename to upload Flight Independent Report"
        End If
    Else
        MsgBox "Please Select the templates"
    End If
    
OpenErr1:
    Exit Sub
End Sub
'Prepare Check-in demand comparision
Sub prepareFlightCheckin()

    Dim templateName As String
    Dim templateLineCount As Long
    Dim templateUrnoTemp  As String
    Dim templateLine As String
    Dim fltDepUrno As String
    Dim templateFlag As Boolean
    Dim demStrWhere As String
    Dim demLiveCount As Long
    Dim turnAroundFlag As Boolean
    Dim templateCount As Long
    Dim demRowListInbound As String
    Dim demRowListInboundCount As Long
    Dim demRowListOutbound As String
    Dim demRowListOutboundCount As Long
    Dim strWhere As String
    Dim tempDETY As String
    Dim demRowsList As String
    Dim demRow As String
    Dim tempTime As Integer
    Dim sumPlanDemandDuration As Long
    Dim sumPlanDemRowCount As Long
    Dim demPlanDemandDuration As Long
    Dim demPlanRowCount As Long
    Dim planFltAcType As String
    Dim demPlanCount As Long
    Dim fltLine1 As String
    
    Dim FileNumber
    Dim fromDt As String
    Dim toDt As String
    Dim fromDt1 As String
    Dim fromDt2 As String
    Dim toDt1 As String
    Dim aftStrWhere As String
    Dim l As Integer
    Dim k As Integer
    Dim k1 As Long
    Dim j As Integer
    Dim m As Integer
    Dim strkey As String
    Dim fltDepList As String
    Dim fltDepCount As Long
    Dim tempLine As String
    Dim tempLinedem As String
    Dim tempUrud As String
    Dim tempUrue As String
    Dim urudCount As Long
    Dim ruleName As String
    Dim ruleUrno As String
    Dim fltUrnoList As String
    Dim ruleCount As String
    Dim demRows As String
    Dim demCount As Long
    Dim demRowCount As Long
    Dim demandDuration As Long
    Dim sumDemRowCount As Long
    Dim sumDemandDuration As Long
    Dim ruleUrnoList As String
    Dim ruleUrnoListPlan As String
    Dim urudList As String
    Dim urudPlanList As String
    Dim urudVal As String
    Dim uaftVal As String
    Dim uaftList As String
    Dim ucdRows As String
    Dim ucdCount As String
    Dim fltStr As String
    Dim aftRow As String
    Dim sdiRow As String
    Dim flnoVal As String
    Dim sdiData As String
    Dim paxVal As Integer
    Dim paxValPlan As Integer


    Dim xlapp
    Dim xlbook
    Dim xlsheet
    Dim xlRow As Integer

    Dim jobStrWhere As String
    Dim jobDemCount As Long
    Dim jobDemDuration As Long
    Dim jobDemUsed As Long
    Dim sumJobDemCount As Long
    Dim sumJobDemDuration As Long
    Dim sumJobDemUsed As Long
    Dim tempDemUrno As String
    Dim jobList As String
    Dim jobListCount As Integer
    Dim jobRow As String
    Dim fromDt3 As String
    Dim toDt3 As String
    
    'Get the selected templates
    templateCount = toList.ListCount
    
    'Added by KKH on 15/02/2007
    'OpenErr1 is invoked if user cancels from Open Dialog
    On Error GoTo OpenErr1
    
    If templateCount > 0 Then
        
        'Prepare file settings
        dlgSave.DialogTitle = "Save as Excel"
        dlgSave.Filter = "Excel (CSV)|*.csv"
        dlgSave.FileName = "Flight Check-In Demands Report"
        dlgSave.ShowSave

        Set xlapp = CreateObject("excel.application") 'Starts the Excel Session
        Set xlbook = xlapp.Workbooks.Add 'Add a Workbook
        Set xlsheet = xlbook.Worksheets.Item(1) 'Select a Sheet
        xlapp.Application.Visible = False
        
        If dlgSave.FileName <> "" Then

            MousePointer = vbHourglass
            
            'Get the fromDate & time, as well as toDate & time to be used in preparing query.
            'fromDt = Mid(fromDate, 7, 4) + Mid(fromDate, 4, 2) + Mid(fromDate, 1, 2) + Mid(fromTime, 1, 2) + Mid(fromTime, 4, 2) + "00"
            'toDt = Mid(toDate, 7, 4) + Mid(toDate, 4, 2) + Mid(toDate, 1, 2) + Mid(toTime, 1, 2) + Mid(toTime, 4, 2) + "00"
            
            ' Get the Input Date and Time Changed by KKH
            fromDate = Trim(Replace(fromDate, ".", ""))
            toDate = Trim(Replace(toDate, ".", ""))
            
            fromDt = Mid(fromDate, 5, 4) + Mid(fromDate, 3, 2) + Mid(fromDate, 1, 2) + Mid(fromTime, 1, 2) + Mid(fromTime, 4, 2) + "00"
            toDt = Mid(toDate, 5, 4) + Mid(toDate, 3, 2) + Mid(toDate, 1, 2) + Mid(toTime, 1, 2) + Mid(toTime, 4, 2) + "00"
            
            fromDt1 = Format(DateAdd("d", -5, CedaFullDateToVb(fromDt)), "yyyymmddhhmmss")
            toDt1 = Format(DateAdd("d", 5, CedaFullDateToVb(toDt)), "yyyymmddhhmmss")

            fromDt3 = Format(DateAdd("d", -1, CedaFullDateToVb(fromDt)), "yyyymmddhhmmss")
            toDt3 = Format(DateAdd("d", 1, CedaFullDateToVb(toDt)), "yyyymmddhhmmss")
            
            fromDt = Format(DateAdd("h", -gUTCOffset, CedaFullDateToVb(fromDt)), "yyyymmddhhmmss")
            toDt = Format(DateAdd("h", -gUTCOffset, CedaFullDateToVb(toDt)), "yyyymmddhhmmss")
            fromDt2 = Format(DateAdd("d", -1, CedaFullDateToVb(fromDt)), "yyyymmddhhmmss")
            
            aftStrWhere = "where STOD between '" + fromDt1 + "' and '" + toDt1 + "' or STOA between '" + fromDt1 + "' and '" + toDt1 + "'"
            frmData.tab_AFTTAB.ResetContent
            frmData.tab_AFTTAB.Refresh
            frmData.LoadData frmData.tab_AFTTAB, aftStrWhere
                         
            'Get the planned AFTTAB as well
            frmData.tab_AFTTABPLAN.ResetContent
            frmData.tab_AFTTABPLAN.Refresh
            frmData.LoadData frmData.tab_AFTTABPLAN, aftStrWhere

            'Prepare Heading of the report
            xlRow = 1
            'xlapp.Application.Cells(xlRow, 1).ColumnWidth = 100
            xlapp.Application.Cells(xlRow, 4).Font.Bold = True
            xlapp.Application.Cells(xlRow, 4).Value = "Flight Check-In Demands"
            'xlapp.Application.Cells(xlRow, 1).HorizontalAlignment = xlright
            xlRow = xlRow + 2

            
            'First get all the selected Templates
            'For each Template, need to calculate flight dependent, flight independent & CCI demands
    
            For l = 0 To templateCount - 1 Step 1
                templateName = toList.List(l)
                templateFlag = False
                
                'Prepare Excel settings properly
                'xlapp.Application.Cells(xlRow, 1).ColumnWidth = 100
                xlapp.Application.Cells(xlRow, 2).Font.Bold = True
                xlapp.Application.Cells(xlRow, 2).Value = "Flight CCI Demands for Loaded Timeframe  from " & fromDate & "/" & fromTime & " To " & toDate & "/" & toTime & "     Template: " & templateName
                xlRow = xlRow + 1
                xlapp.Application.Cells(xlRow, 8).Font.Bold = True
                xlapp.Application.Cells(xlRow, 8).Value = "Report Genearation Time, Start: " & Now
                xlRow = xlRow + 2

                'Changed by KKH on 13/02/2007
                xlapp.Application.Cells(xlRow, 1).ColumnWidth = 15
                xlapp.Application.Cells(xlRow, 2).ColumnWidth = 15
                xlapp.Application.Cells(xlRow, 3).ColumnWidth = 15
                xlapp.Application.Cells(xlRow, 4).ColumnWidth = 23
                xlapp.Application.Cells(xlRow, 5).ColumnWidth = 23
                xlapp.Application.Cells(xlRow, 6).ColumnWidth = 15
                xlapp.Application.Cells(xlRow, 7).ColumnWidth = 15
                xlapp.Application.Cells(xlRow, 8).ColumnWidth = 23
                xlapp.Application.Cells(xlRow, 9).ColumnWidth = 17
                xlapp.Application.Cells(xlRow, 10).ColumnWidth = 25
                xlapp.Application.Cells(xlRow, 11).ColumnWidth = 15
                xlapp.Application.Cells(xlRow, 12).ColumnWidth = 15
                xlapp.Application.Cells(xlRow, 13).ColumnWidth = 15
                xlapp.Application.Cells(xlRow, 1).Font.Bold = True
                xlapp.Application.Cells(xlRow, 2).Font.Bold = True
                xlapp.Application.Cells(xlRow, 3).Font.Bold = True
                xlapp.Application.Cells(xlRow, 4).Font.Bold = True
                xlapp.Application.Cells(xlRow, 5).Font.Bold = True
                xlapp.Application.Cells(xlRow, 6).Font.Bold = True
                xlapp.Application.Cells(xlRow, 7).Font.Bold = True
                xlapp.Application.Cells(xlRow, 8).Font.Bold = True
                xlapp.Application.Cells(xlRow, 9).Font.Bold = True
                xlapp.Application.Cells(xlRow, 10).Font.Bold = True
                xlapp.Application.Cells(xlRow, 11).Font.Bold = True
                xlapp.Application.Cells(xlRow, 12).Font.Bold = True
                xlapp.Application.Cells(xlRow, 13).Font.Bold = True
                xlapp.Application.Cells(xlRow, 1).Value = "Rule Name"
                xlapp.Application.Cells(xlRow, 2).Value = "Class"
                xlapp.Application.Cells(xlRow, 3).Value = "Actual Jobs"
                xlapp.Application.Cells(xlRow, 4).Value = "Actual Jobs Duration(Hrs)"
                xlapp.Application.Cells(xlRow, 5).Value = "Actual Jobs Duration(Min)"
                xlapp.Application.Cells(xlRow, 6).Value = "Actual Demands Used"
                xlapp.Application.Cells(xlRow, 7).Value = "Actual Demands"
                xlapp.Application.Cells(xlRow, 8).Value = "Actual Demand Duration"
                xlapp.Application.Cells(xlRow, 9).Value = "Planned Demands"
                xlapp.Application.Cells(xlRow, 10).Value = "Planned Demands Duration"
                xlapp.Application.Cells(xlRow, 11).Value = "Flight Number"
                xlapp.Application.Cells(xlRow, 12).Value = "Pax Load Actual"
                xlapp.Application.Cells(xlRow, 13).Value = "Pax Load Planned"
                xlRow = xlRow + 1

                'Get the URNO of template names from tab_TPLTAB
                templateUrnoTemp = frmData.tab_TPLTAB.GetLinesByColumnValue(1, templateName, 0)
                
                'For every Template, there will be flight dependent & independent demands will be there. Need to check
                If ItemCount(templateUrnoTemp, ",") > 1 Then
                    templateLineCount = ItemCount(templateUrnoTemp, ",")
                    
                    For k = 1 To templateLineCount Step 1
                        templateLine = GetItem(templateUrnoTemp, k, ",")
                        If frmData.tab_TPLTAB.GetColumnValues(templateLine, 2) = "RULE_AFT" Then
                            fltDepUrno = frmData.tab_TPLTAB.GetColumnValues(templateLine, 0)
                            templateFlag = True
                        End If
                    Next k
                Else
                    If frmData.tab_TPLTAB.GetColumnValues(templateUrnoTemp, 2) = "RULE_AFT" Then
                        fltDepUrno = frmData.tab_TPLTAB.GetColumnValues(templateUrnoTemp, 0)
                        templateFlag = True
                    End If
                End If
                    
                If templateFlag Then
                    
                    'Get only the Rules information for the specific template
                    strWhere = "where UTPL = '" & fltDepUrno & "'"
                    frmData.tab_RUDTAB.ResetContent
                    frmData.tab_RUDTAB.Refresh
                    frmData.LoadData frmData.tab_RUDTAB, strWhere

                    'Get all UCDTAB Entries - LIVE
                    strWhere = ""
                    frmData.tab_UCDTAB.ResetContent
                    frmData.tab_UCDTAB.Refresh
                    frmData.LoadData frmData.tab_UCDTAB, strWhere
    
                    'Get all UCDTAB Entries - LIVE
                    strWhere = ""
                    frmData.tab_UCDTABPLAN.ResetContent
                    frmData.tab_UCDTABPLAN.Refresh
                    frmData.LoadData frmData.tab_UCDTABPLAN, strWhere
     
                    'Get all SDITAB Entries - LIVE
                    strWhere = "where PENA > '" + fromDt + "' and PBEA between '" + fromDt2 + "' and '" + toDt + "' "
                    frmData.tab_SDITAB.ResetContent
                    frmData.tab_SDITAB.Refresh
                    frmData.LoadData frmData.tab_SDITAB, strWhere
    
                    'Get all SDITAB Entries - PLAN
                    frmData.tab_SDITABPLAN.ResetContent
                    frmData.tab_SDITABPLAN.Refresh
                    frmData.LoadData frmData.tab_SDITABPLAN, strWhere
 
                    'Need to test below query, so that only flight dependent demands can be retrieved, by avoiding CCI demands
                    'demStrWhere = "where UTPL = '" & fltDepUrno & "'  and DEEN > '" + fromDt + "' and DEBE between '" + fromDt2 + "' and '" + toDt + "' and DETY = '6' "
                    demStrWhere = "where UTPL = '" & fltDepUrno & "' and RETY = '100' and DEEN > '" + fromDt + "' and DEBE between '" + fromDt2 + "' and '" + toDt + "' and DETY = '6' "
                    
                    'Get the Flight Independent Demands from DEMTAB
                    tab_DEMTABLIVE.ResetContent
                    tab_DEMTABLIVE.Refresh
                    frmData.LoadData tab_DEMTABLIVE, demStrWhere
                    demLiveCount = tab_DEMTABLIVE.GetLineCount

                    'Get the Flight Dependent Demands from DEMTABPLAN
                    tab_DEMTABPLAN.ResetContent
                    tab_DEMTABPLAN.Refresh
                    frmData.LoadData tab_DEMTABPLAN, demStrWhere
                    demPlanCount = tab_DEMTABPLAN.GetLineCount

                    jobStrWhere = "where UTPL = '" & fltDepUrno & "' and ACTO between '" + fromDt3 + "' and '" + toDt3 + "' and ACFR < '" + toDt3 + "' "
                    tab_JOBTABDEM.ResetContent
                    tab_JOBTABDEM.Refresh
                    frmData.LoadData tab_JOBTABDEM, jobStrWhere
                    'MsgBox tab_JOBTABDEM.GetLineCount
                  
                    tab_DEMTABTEMP.ResetContent
                    tab_DEMTABTEMP.Refresh
                    sumDemandDuration = 0
                    sumDemRowCount = 0
                    ruleUrnoList = ""
                    ruleUrnoListPlan = ""

                    tab_DEMTABPLANTEMP.ResetContent
                    tab_DEMTABPLANTEMP.Refresh
                    sumPlanDemandDuration = 0
                    sumPlanDemRowCount = 0

                    sumJobDemCount = 0
                    sumJobDemDuration = 0
                    sumJobDemUsed = 0


                    'Prepare distinct URUD, so that to get Rule names & prepare list of demands per rule name - DEMTABLIVE
                    For j = 0 To demLiveCount - 1 Step 1
                        'tempLinedem = GetItem(fltIndList, j, ",")
                        
                        'Prepare DEMTABTEMP, provided demand is valid
                        If Mid(tab_DEMTABLIVE.GetColumnValue(j, 3), 2, 1) <> "1" Then
                            tempUrud = tab_DEMTABLIVE.GetColumnValues(j, 8)
                            
                            'Get the rowvalue of tempUrud from RUDTAB & get the URUE value to check in RUETAB
                            tempLine = frmData.tab_RUDTAB.GetLinesByColumnValue(0, tempUrud, 0)
                            tempUrue = frmData.tab_RUDTAB.GetColumnValues(tempLine, 1)
                            
                            'Get the rowvalue of tempUrue from RUETAB & get the Recordname from RUETAB
                            tempLine = frmData.tab_RUETAB.GetLinesByColumnValue(0, tempUrue, 0)
                            ruleUrno = frmData.tab_RUETAB.GetColumnValues(tempLine, 0)
                            
                            'Add DEMTAB row to a temporary table with URNO of RULE..
                            tab_DEMTABTEMP.InsertTextLine tab_DEMTABLIVE.GetColumnValues(j, "0,1,2,3,4,5,6,7,8,9") & "," & ruleUrno, True
                            
                            If InStr(1, ruleUrnoList, ruleUrno) = 0 Then ruleUrnoList = ruleUrnoList & ruleUrno & ","
                            If InStr(1, urudList, tempUrud) = 0 Then urudList = urudList & tempUrud & ","
                        End If
                    Next j
  
                    'Prepare distinct URUD, so that to get Rule names & prepare list of demands per rule name - DEMTABPLAN
                    For j = 0 To demPlanCount - 1 Step 1
                        
                        'Prepare DEMTABTEMP, provided demand is valid
                        If Mid(tab_DEMTABPLAN.GetColumnValue(j, 3), 2, 1) <> "1" Then
                            tempUrud = tab_DEMTABPLAN.GetColumnValues(j, 8)
                            
                            'Get the rowvalue of tempUrud from RUDTAB & get the URUE value to check in RUETAB
                            tempLine = frmData.tab_RUDTAB.GetLinesByColumnValue(0, tempUrud, 0)
                            tempUrue = frmData.tab_RUDTAB.GetColumnValues(tempLine, 1)
                            
                            'Get the rowvalue of tempUrue from RUETAB & get the Recordname from RUETAB
                            tempLine = frmData.tab_RUETAB.GetLinesByColumnValue(0, tempUrue, 0)
                            ruleUrno = frmData.tab_RUETAB.GetColumnValues(tempLine, 0)
                            
                            'Add DEMTAB row to a temporary table with URNO of RULE..
                            tab_DEMTABPLANTEMP.InsertTextLine tab_DEMTABPLAN.GetColumnValues(j, "0,1,2,3,4,5,6,7,8,9") & "," & ruleUrno, True

                            If InStr(1, ruleUrnoListPlan, ruleUrno) = 0 Then ruleUrnoListPlan = ruleUrnoListPlan & ruleUrno & ","
                            If InStr(1, urudPlanList, tempUrud) = 0 Then urudPlanList = urudPlanList & tempUrud & ","
                            
                        End If
                    Next j

                    ruleCount = ItemCount(ruleUrnoList, ",")
        
                    For j = 1 To ruleCount - 1 Step 1
                        ruleUrno = GetItem(ruleUrnoList, j, ",")
                        urudVal = GetItem(urudList, j, ",")
                        
                        'Get the rule name
                        tempLine = frmData.tab_RUETAB.GetLinesByColumnValue(0, ruleUrno, 0)
                        ruleName = frmData.tab_RUETAB.GetColumnValues(tempLine, 1)
                        
                        'To get all the rows for the  ruleUrno - DEMTABLIVE
                        demRows = tab_DEMTABTEMP.GetLinesByColumnValue(10, ruleUrno, 0)
                        demCount = ItemCount(demRows, ",")
                        demRowCount = 0
                        demandDuration = 0

                        jobDemCount = 0
                        jobDemDuration = 0
                        jobDemUsed = 0
                        
                        If demCount > 0 Then
                            For k = 1 To demCount Step 1
                                'Prepare the demands duration
                                tempLinedem = GetItem(demRows, k, ",")
                                'prepare number of hours & demand count .. provided if demand is valid
                               
                                demRowCount = demRowCount + 1
                                demandDuration = demandDuration + DateDiff("n", CedaFullDateToVb(tab_DEMTABTEMP.GetColumnValues(tempLinedem, 0)), CedaFullDateToVb(tab_DEMTABTEMP.GetColumnValues(tempLinedem, 1)))

                                'To calculate actual jobs, need to check JOBTAB & get the details from JOBTAB.
                                'Getting the urno for each demand & calculate the actual job times
                                'KKH 6)
                                tempDemUrno = tab_DEMTABLIVE.GetColumnValues(tempLinedem, 10)
                                jobList = tab_JOBTABDEM.GetLinesByColumnValue(3, tempDemUrno, 0)
                                jobListCount = ItemCount(jobList, ",")

                                'if jobListCount is not zero, then calculate the actual job
                                If jobListCount > 0 Then
                                    jobDemCount = jobDemCount + jobListCount
                                    jobDemUsed = jobDemUsed + 1
                                    'if more than one person worked for the same demand, need to calculate
                                    For m = 1 To jobListCount Step 1
                                        jobRow = GetItem(jobList, m, ",")
                                        jobDemDuration = jobDemDuration + DateDiff("n", CedaFullDateToVb(tab_JOBTABDEM.GetColumnValues(jobRow, 1)), CedaFullDateToVb(tab_JOBTABDEM.GetColumnValues(jobRow, 2)))
                                    Next m
                                End If
                                'End - calculation of actual jobs.
                                
                            Next k
                            
                            sumDemRowCount = sumDemRowCount + demRowCount
                            sumDemandDuration = sumDemandDuration + demandDuration
                            sumJobDemCount = sumJobDemCount + jobDemCount
                            sumJobDemDuration = sumJobDemDuration + jobDemDuration
                            sumJobDemUsed = sumJobDemUsed + jobDemUsed

                        End If

                        'To get all the rows for the  ruleUrno - DEMTABPLAN
                        demRows = tab_DEMTABPLANTEMP.GetLinesByColumnValue(10, ruleUrno, 0)
                        demCount = ItemCount(demRows, ",")
                        demPlanRowCount = 0
                        demPlanDemandDuration = 0
                        
                        If demCount > 0 Then
                            For k = 1 To demCount Step 1
                                'Prepare the demands duration
                                tempLinedem = GetItem(demRows, k, ",")
                                'prepare number of hours & demand count .. provided if demand is valid
                               
                                demPlanRowCount = demPlanRowCount + 1
                                demPlanDemandDuration = demPlanDemandDuration + DateDiff("n", CedaFullDateToVb(tab_DEMTABPLANTEMP.GetColumnValues(tempLinedem, 0)), CedaFullDateToVb(tab_DEMTABPLANTEMP.GetColumnValues(tempLinedem, 1)))
                               
                            Next k
                            
                            sumPlanDemRowCount = sumPlanDemRowCount + demPlanRowCount
                            sumPlanDemandDuration = sumPlanDemandDuration + demPlanDemandDuration
                        End If
                        
                        'KKH - Changed on 26/01/2007, special character mapping
                        'xlapp.Application.Cells(xlRow, 1).Value = Trim(ruleName)
                        xlapp.Application.Cells(xlRow, 1).Value = CleanString(ruleName, FOR_CLIENT, True)
                        xlapp.Application.Cells(xlRow, 2).Value = " "
                        xlapp.Application.Cells(xlRow, 3).Value = jobDemCount
                        'Changed by KKH on 13/02/2007
                        'xlapp.Application.Cells(xlRow, 4).Value = Int(jobDemDuration / 60) & " Hrs :" & (jobDemDuration Mod 60) & " Min"
                        xlapp.Application.Cells(xlRow, 4).Value = Int(jobDemDuration / 60)
                        xlapp.Application.Cells(xlRow, 5).Value = Int(jobDemDuration Mod 60)
                        xlapp.Application.Cells(xlRow, 6).Value = jobDemUsed
                        xlapp.Application.Cells(xlRow, 7).Value = demRowCount
                        xlapp.Application.Cells(xlRow, 8).Value = Int(demandDuration / 60) & " Hrs :" & (demandDuration Mod 60) & " Min"
                        xlapp.Application.Cells(xlRow, 9).Value = demPlanRowCount
                        xlapp.Application.Cells(xlRow, 10).Value = Int(demPlanDemandDuration / 60) & " Hrs :" & (demPlanDemandDuration Mod 60) & " Min"
                       
                        'Need to incorporate the code to write Flights Information
                        'Get all the records from UCDTAB based on URUD

                        ucdRows = frmData.tab_UCDTAB.GetLinesByColumnValue(1, urudVal, 0)
                        ucdCount = ItemCount(ucdRows, ",")

                        If ucdCount > 0 Then
                            For k1 = 1 To ucdCount - 1 Step 1
                                tempLinedem = GetRealItem(ucdRows, k1, ",")
                                uaftVal = frmData.tab_UCDTAB.GetColumnValues(tempLinedem, 2)
                                If IsNumeric(frmData.tab_SDITAB.GetLinesByColumnValue(1, uaftVal, 0)) Then
                                    
                                    aftRow = frmData.tab_AFTTAB.GetLinesByColumnValue(0, uaftVal, 0)
                                    sdiRow = frmData.tab_SDITAB.GetLinesByColumnValue(1, uaftVal, 0)
                                    
                                    If (IsNumeric(aftRow) And IsNumeric(sdiRow)) Then

                                        flnoVal = frmData.tab_AFTTAB.GetColumnValues(aftRow, 1)
                                        sdiData = frmData.tab_SDITAB.GetColumnValues(sdiRow, 5)
                                        paxVal = calculatePaxData(sdiData)

                                        aftRow = frmData.tab_AFTTABPLAN.GetLinesByColumnValue(0, uaftVal, 0)
                                        sdiRow = frmData.tab_SDITABPLAN.GetLinesByColumnValue(1, uaftVal, 0)

                                        If (IsNumeric(aftRow) And IsNumeric(sdiRow)) Then
                                            sdiData = frmData.tab_SDITABPLAN.GetColumnValues(sdiRow, 5)
                                            paxValPlan = calculatePaxData(sdiData)
                                        Else
                                            paxValPlan = 0
                                        End If

                                        fltStr = fltStr & uaftVal & ","
                                        
                                        'Changed by KKH on 13/02/2007
'                                        xlapp.Application.Cells(xlRow, 7).Value = flnoVal
'                                        xlapp.Application.Cells(xlRow, 8).Value = paxVal
'                                        xlapp.Application.Cells(xlRow, 9).Value = paxValPlan
'comment by KKH on 14/02/2007
'                                        xlapp.Application.Cells(xlRow, 11).Value = flnoVal
'                                        xlapp.Application.Cells(xlRow, 12).Value = paxVal
'                                        xlapp.Application.Cells(xlRow, 13).Value = paxValPlan

                                        xlRow = xlRow + 1

                                    End If

                                End If
                            Next k1
                        End If

                        'If any more flights available in planned, need to show them as well .. with LIVE pax data as zero
                        ucdRows = frmData.tab_UCDTABPLAN.GetLinesByColumnValue(1, urudVal, 0)
                        ucdCount = ItemCount(ucdRows, ",")

                        If ucdCount > 0 Then
                            For k1 = 1 To ucdCount - 1 Step 1
                                tempLinedem = GetRealItem(ucdRows, k1, ",")
                                uaftVal = frmData.tab_UCDTABPLAN.GetColumnValues(tempLinedem, 2)

                                If InStr(1, fltStr, uaftVal) = 0 Then
                                    If IsNumeric(frmData.tab_SDITABPLAN.GetLinesByColumnValue(1, uaftVal, 0)) Then
                                        
                                        aftRow = frmData.tab_AFTTABPLAN.GetLinesByColumnValue(0, uaftVal, 0)
                                        sdiRow = frmData.tab_SDITABPLAN.GetLinesByColumnValue(1, uaftVal, 0)
                                        
                                        If IsNumeric(aftRow) And IsNumeric(sdiRow) Then

                                            flnoVal = frmData.tab_AFTTABPLAN.GetColumnValues(aftRow, 1)
                                            sdiData = frmData.tab_SDITABPLAN.GetColumnValues(sdiRow, 5)
                                            paxValPlan = calculatePaxData(sdiData)

                                            fltStr = fltStr & uaftVal & ","
                                            'Changed by KKH on 13/02/2007
'                                            xlapp.Application.Cells(xlRow, 7).Value = flnoVal
'                                            xlapp.Application.Cells(xlRow, 8).Value = 0
'                                            xlapp.Application.Cells(xlRow, 9).Value = paxValPlan
'comment by KKH on 14/02/2007
'                                            xlapp.Application.Cells(xlRow, 11).Value = flnoVal
'                                            xlapp.Application.Cells(xlRow, 12).Value = 0
'                                            xlapp.Application.Cells(xlRow, 13).Value = paxValPlan
                                            xlRow = xlRow + 1

                                        End If
                                    End If
                                End If
                            Next k1
                        End If
                        xlRow = xlRow + 1

                    Next j

                    'If any Rules which are not available in the demandslive rule list, then need to execute them.
                    ruleCount = ItemCount(ruleUrnoListPlan, ",")
        
                    For j = 1 To ruleCount - 1 Step 1
                        ruleUrno = GetItem(ruleUrnoListPlan, j, ",")
                        urudVal = GetItem(urudList, j, ",")

                        If InStr(1, ruleUrnoList, ruleUrno) = 0 Then
                            tempLine = frmData.tab_RUETAB.GetLinesByColumnValue(0, ruleUrno, 0)
                            ruleName = frmData.tab_RUETAB.GetColumnValues(tempLine, 1)

                            'To get all the rows for the  ruleUrno - DEMTABPLANTEMP
                            demRows = tab_DEMTABPLANTEMP.GetLinesByColumnValue(10, ruleUrno, 0)
                            demCount = ItemCount(demRows, ",")
                            demPlanRowCount = 0
                            demPlanDemandDuration = 0
                            
                            If demCount > 0 Then
                                For k = 1 To demCount Step 1
                                    'Prepare the demands duration
                                    tempLinedem = GetItem(demRows, k, ",")
                                    'prepare number of hours & demand count .. provided if demand is valid
                                   
                                    demPlanRowCount = demPlanRowCount + 1
                                    demPlanDemandDuration = demPlanDemandDuration + DateDiff("n", CedaFullDateToVb(tab_DEMTABPLANTEMP.GetColumnValues(tempLinedem, 0)), CedaFullDateToVb(tab_DEMTABPLANTEMP.GetColumnValues(tempLinedem, 1)))
                                   
                                Next k
                                
                                sumPlanDemRowCount = sumPlanDemRowCount + demPlanRowCount
                                sumPlanDemandDuration = sumPlanDemandDuration + demPlanDemandDuration
                            End If

                        'KKH - Changed on 26/01/2007, special character mapping
                        'xlapp.Application.Cells(xlRow, 1).Value = Trim(ruleName)
                        xlapp.Application.Cells(xlRow, 1).Value = CleanString(ruleName, FOR_CLIENT, True)
                        xlapp.Application.Cells(xlRow, 2).Value = " "
                        xlapp.Application.Cells(xlRow, 3).Value = 0
                        'Changed by KKH on 13/02/2007
                        xlapp.Application.Cells(xlRow, 4).Value = 0
                        xlapp.Application.Cells(xlRow, 5).Value = 0
                        xlapp.Application.Cells(xlRow, 6).Value = 0
                        xlapp.Application.Cells(xlRow, 7).Value = 0
                        xlapp.Application.Cells(xlRow, 8).Value = 0
                        xlapp.Application.Cells(xlRow, 9).Value = demPlanRowCount
                        xlapp.Application.Cells(xlRow, 10).Value = Int(demPlanDemandDuration / 60) & " Hrs :" & (demPlanDemandDuration Mod 60) & " Min"
                        
                        
                        'Need to incorporate the code to write Flights Information
                        'Get all the records from UCDTAB based on URUD

                        ucdRows = frmData.tab_UCDTABPLAN.GetLinesByColumnValue(1, urudVal, 0)
                        ucdCount = ItemCount(ucdRows, ",")

                        If ucdCount > 0 Then
                            For k1 = 1 To ucdCount - 1 Step 1
                                tempLinedem = GetRealItem(ucdRows, k1, ",")
                                uaftVal = frmData.tab_UCDTABPLAN.GetColumnValues(tempLinedem, 2)

                                If InStr(1, fltStr, uaftVal) = 0 Then
                                    If IsNumeric(frmData.tab_SDITABPLAN.GetLinesByColumnValue(1, uaftVal, 0)) Then
                                        
                                        aftRow = frmData.tab_AFTTABPLAN.GetLinesByColumnValue(0, uaftVal, 0)
                                        sdiRow = frmData.tab_SDITABPLAN.GetLinesByColumnValue(1, uaftVal, 0)
                                        
                                        If IsNumeric(aftRow) And IsNumeric(sdiRow) Then

                                            flnoVal = frmData.tab_AFTTABPLAN.GetColumnValues(aftRow, 1)
                                            sdiData = frmData.tab_SDITABPLAN.GetColumnValues(sdiRow, 5)
                                            paxValPlan = calculatePaxData(sdiData)

                                            fltStr = fltStr & uaftVal & ","
                                            'Changed by KKH on 13/02/2007
'                                            xlapp.Application.Cells(xlRow, 7).Value = flnoVal
'                                            xlapp.Application.Cells(xlRow, 8).Value = 0
'                                            xlapp.Application.Cells(xlRow, 9).Value = paxValPlan
'comment by KKH on 14/02/2007
'                                            xlapp.Application.Cells(xlRow, 11).Value = flnoVal
'                                            xlapp.Application.Cells(xlRow, 12).Value = 0
'                                            xlapp.Application.Cells(xlRow, 13).Value = paxValPlan
                                            xlRow = xlRow + 1

                                        End If
                                    End If
                                End If
                            Next k1
                        End If

                        End If
                        xlRow = xlRow + 1
                    Next j

                    xlapp.Application.Cells(xlRow, 1).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 2).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 3).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 4).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 5).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 6).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 7).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 8).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 9).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 10).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 1).Value = "Sum:"
                    xlapp.Application.Cells(xlRow, 2).Value = " "
                    xlapp.Application.Cells(xlRow, 3).Value = sumJobDemCount
                    'Changed by KKH on 13/02/2007
                    'xlapp.Application.Cells(xlRow, 4).Value = Int(sumJobDemDuration / 60) & "Hrs : " & (sumJobDemDuration Mod 60) & " Min"
                    xlapp.Application.Cells(xlRow, 4).Value = "Actual Jobs Duration :"
                    xlapp.Application.Cells(xlRow, 5).Value = Int(sumJobDemDuration / 60) & "Hrs : " & (sumJobDemDuration Mod 60) & " Min"
                    xlapp.Application.Cells(xlRow, 6).Value = sumJobDemUsed
                    xlapp.Application.Cells(xlRow, 7).Value = sumDemRowCount
                    xlapp.Application.Cells(xlRow, 8).Value = Int(sumDemandDuration / 60) & "Hrs : " & (sumDemandDuration Mod 60) & " Min"
                    xlapp.Application.Cells(xlRow, 9).Value = sumPlanDemRowCount
                    xlapp.Application.Cells(xlRow, 10).Value = Int(sumPlanDemandDuration / 60) & "Hrs : " & (sumPlanDemandDuration Mod 60) & " Min"
                    xlRow = xlRow + 2

                Else 'Template Flag
                    xlapp.Application.Cells(xlRow, 2).Font.Bold = True
                    xlapp.Application.Cells(xlRow, 2).Value = "Flight CCI Demands wont be available for this template :" & templateName
                    xlRow = xlRow + 1
                End If
            Next l
            
            xlRow = xlRow + 1
            xlapp.Application.Cells(xlRow, 8).Value = "Report Genearation Time, End: " & Now
            xlapp.Application.Cells(xlRow, 8).Font.Bold = True

            'Close #FileNumber
            xlbook.SaveAs dlgSave.FileName
            xlbook.Close
            xlapp.Quit
            MousePointer = vbDefault
            
            MsgBox "" & dlgSave.FileName & " created successfully"
        Else 'dlgSave if
            MsgBox "Please Enter Filename to upload Flight CCI Report"
        End If
    Else
        MsgBox "Please Select the templates"
    End If
OpenErr1:
    Exit Sub
End Sub

Function calculatePaxData(sdiData As String) As Integer
    Dim paxVal As Integer
    Dim i As Integer
    Dim count As Integer
    Dim val As String

    paxVal = 0
    count = ItemCount(sdiData, "|")
    For i = 1 To count - 1 Step 1
        val = GetItem(sdiData, i, "|")
        If val <> "" Then paxVal = paxVal + Int(Trim(val))
    Next i

    calculatePaxData = paxVal
End Function

Private Sub Others_Click()
    Dim i As Long
    Dim l As Long
    Dim tplList As String
    
    tplList = ""
    fromList.Clear
    toList.Clear
    
    pools.Caption = "Templates"
    i = frmData.tab_TPLTAB.GetLineCount
    cmdTemplates.Visible = True
    For l = 0 To i - 1 Step 1
        If InStr(1, tplList, frmData.tab_TPLTAB.GetColumnValues(l, 1)) = 0 Then
            cmdTemplates.AddItem frmData.tab_TPLTAB.GetColumnValues(l, 1)
            tplList = tplList & frmData.tab_TPLTAB.GetColumnValues(l, 1) & ","
       End If
    Next l
    
    cmdIndependent.Visible = False
    cmdDependent.Visible = False
    cmdCheckin.Visible = False
    cmdFunction.Visible = False
    cmdReport.Visible = False
    fromList.Visible = False
    cmdFront.Visible = False
    cmdBack.Visible = False
    toList.Visible = False
    cmdJobWODemand.Visible = False
    cmdReportOthers.Visible = True
End Sub

Private Sub work_Click()
    Dim i As Long
    Dim l As Long
    
    fromList.Clear
    toList.Clear
    
    'pools.Caption = "Pools"
    'i = frmData.tab_POLTAB.GetLineCount
    
    'For l = 0 To i - 1 Step 1
    '    fromList.AddItem frmData.tab_POLTAB.GetColumnValues(l, 2)
    'Next l
    
    pools.Caption = "Organizational Units"
    i = frmData.tab_ORGTAB.GetLineCount
    
    For l = 0 To i - 1 Step 1
        fromList.AddItem frmData.tab_ORGTAB.GetColumnValues(l, 1)
    Next l
    
    cmdTemplates.Visible = False
    cmdIndependent.Visible = False
    cmdDependent.Visible = False
    cmdCheckin.Visible = False
    cmdFunction.Visible = False
    cmdReport.Visible = True
    
    fromList.Visible = True
    cmdFront.Visible = True
    cmdBack.Visible = True
    toList.Visible = True
    cmdJobWODemand.Visible = False
    cmdReportOthers.Visible = False
End Sub

Private Sub demandCompare_Click()
    Dim i As Long
    Dim l As Long
    Dim tplList As String
    
    tplList = ""
    fromList.Clear
    toList.Clear
    
    pools.Caption = "Templates"
    i = frmData.tab_TPLTAB.GetLineCount
    
    For l = 0 To i - 1 Step 1
        If InStr(1, tplList, frmData.tab_TPLTAB.GetColumnValues(l, 1)) = 0 Then
            fromList.AddItem frmData.tab_TPLTAB.GetColumnValues(l, 1)
            tplList = tplList & frmData.tab_TPLTAB.GetColumnValues(l, 1) & ","
        End If
    Next l
    cmdTemplates.Visible = False
    cmdReport.Visible = False
    cmdIndependent.Visible = True
    cmdDependent.Visible = True
    cmdCheckin.Visible = True
    cmdFunction.Visible = True
    
    fromList.Visible = True
    cmdFront.Visible = True
    cmdBack.Visible = True
    toList.Visible = True
    cmdJobWODemand.Visible = False
    cmdReportOthers.Visible = False
    
End Sub
