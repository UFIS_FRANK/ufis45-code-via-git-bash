// HeaderDlg.cpp : implementation file
//

#include "stdafx.h"
#include "TAB.h"
#include "HeaderDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// HeaderDlg dialog


HeaderDlg::HeaderDlg(CWnd* pParent /*=NULL*/)
	: CDialog(HeaderDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(HeaderDlg)
	m_T4 = _T("");
	m_T5 = _T("");
	m_T6 = _T("");
	m_T1 = _T("");
	m_T2 = _T("");
	m_T3 = _T("");
	//}}AFX_DATA_INIT
}


void HeaderDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(HeaderDlg)
	DDX_Text(pDX, IDC_EDIT4, m_T4);
	DDX_Text(pDX, IDC_EDIT5, m_T5);
	DDX_Text(pDX, IDC_EDIT6, m_T6);
	DDX_Text(pDX, IDC_H1, m_T1);
	DDX_Text(pDX, IDC_H2, m_T2);
	DDX_Text(pDX, IDC_H3, m_T3);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(HeaderDlg, CDialog)
	//{{AFX_MSG_MAP(HeaderDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// HeaderDlg message handlers

void HeaderDlg::OnOK() 
{
	if(UpdateData() == TRUE)	
	{
		CDialog::OnOK();
	}
}
