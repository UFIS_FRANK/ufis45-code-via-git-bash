#if !defined(AFX_TABCTL_H__64E8E393_05E2_11D2_9B1D_9B0630BC8F12__INCLUDED_)
#define AFX_TABCTL_H__64E8E393_05E2_11D2_9B1D_9B0630BC8F12__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

// This program uses regex++,
// Copyright Dr John Maddock 1998-9 all rights reserved.
//#include <cregex.h>
#include <afxtempl.h>
#include "UInlineEdit.h"
#include "CCSPtrArray.h"
#include "Dib.h"
#include "FastCedaConnection.h"
#include "TabCombo.h"

/////////////////////////////////////////////////////////////////////////////
// CTABCtrl : See TABCtl.cpp for implementation.

enum
{
	SEP_NONE,
	SEP_NORMAL,
	SEP_3D,
};

enum
{
	EDIT_MOVE_UP,
	EDIT_MOVE_DOWN,
	EDIT_MOVE_LEFT,
	EDIT_MOVE_RIGHT,
	EDIT_MOVE_START,
	EDIT_MOVE_END,
	EDIT_MOVE_PGDOWN,
	EDIT_MOVE_PGUP,
	EDIT_MOVE_BOTTOM,
	EDIT_MOVE_TOP,
	EDIT_MOVE_CRLF,
};

enum
{
	POST_ENTER_RIGHT,
	POST_ENTER_DOWN,
	POST_ENTER_NOWHERE,
	POST_ENTER_TAB
};

struct _StringDescriptor
{
        char* Value;                    /* string with values */
        long  AllocatedSize;    /* byte count from malloc or realloc */
        long  UsedLen;                  /* calulated value from strlen */
};
typedef struct _StringDescriptor STR_DESC;

class CELL_PROP : public CObject
{
public:
	CString		ObjID;
	CFont		CellFont;
	long		TextColor;
	long		BackColor;
};

class CELL_BOOL_PROP : public CObject
{
public:
	CString ObjID;
	CString TrueValue,
			FalseValue;
	int		DrawMode;		//Flat, 3D, Route
};

class CELL_BITMAP_PROP : public CObject
{
public:
	CString		ObjID;
	CString		Path;
	CDib		Bitmap;
};


struct LINE_DEF
{
	long			StatusValue;
	long			FontColor;
	long			BackColor;
	CString			SortString;
	CString			Values;
	CString			Tag;
	CMapPtrToPtr	mapCellProp;
	CMapPtrToPtr	mapCellBitmapProp;
	CMapPtrToPtr	mapDecoObjects;
	short			TimeValue;

	LINE_DEF(void)
	{
		FontColor	= 0;
		BackColor	= 16777215;
		StatusValue	= 0;
		TimeValue	= 0;
	}
};

class DECORATION_OBJECT : public CObject
{
public:
	CString			ID;
	CStringArray	LocationList;
	CStringArray	PixelList;
	CStringArray	ColorList;
};

class CURSOR_DECORATION_OBJECT : public DECORATION_OBJECT
{
public:
	CStringArray	LogicalFields;
};

//One index item, with value and line number
struct INDEX_ITEM
{
	long					LineNo; // Linenumber which is returned by search fuctionality
	CString					Value;  // The value, sorted for this value
};

class INDEX_OBJECT : public CObject
{
public:
	INDEX_OBJECT()
	{
		IndexName=CString("");
		Column = 0;
	}
	CString					IndexName;	// Name of this index object
	long					Column;		// Column of the tab
	CCSPtrArray<INDEX_ITEM> Items;		// Items
};

class CTABCtrl : public COleControl
{
	DECLARE_DYNCREATE(CTABCtrl)

// Constructor
public:
	CTABCtrl();
	CCSPtrArray<LINE_DEF>			omLines;
	CCSPtrArray<CELL_PROP>			omCellProps;
	CCSPtrArray<CELL_BITMAP_PROP>	omCellBitmapProps;
	CCSPtrArray<CELL_BOOL_PROP>		omCellBoolProps;
	CCSPtrArray<DECORATION_OBJECT>  omDecoObjects;
	CCSPtrArray<COMBO_OBJECT_PROP>  omComboObjects;
	CCSPtrArray<INDEX_OBJECT>		omIndexes;
	CMapPtrToPtr					mapColumnProp;
	CMapPtrToPtr					mapColumnBoolProp;
	CMapPtrToPtr					mapDecoObjects;
	CMapPtrToPtr					mapComboObjects;

	CURSOR_DECORATION_OBJECT		omCursorDeco;
	CStringArray					omHeader;
	CStringArray					omMonths;
	//CStringArray omDateFormat;
	CUIntArray						omHeaderLen;
	CUIntArray						omSelectedColumns;
	CUIntArray						omBoolPropertyReadOnly;
	CString							omFieldSeparator;

	CUIntArray						omUniqueColumns;
	CMapStringToPtr					mapUnique;
	CMapStringToPtr					mapLogicalFields;
	CCSPtrArray<long>				omLogicalFieldCols;
	// To uses the internal buffer for GetLinesByXXX results
	long							lmNextResultIdx;
	CUIntArray						omResultLines;
	bool							bmUseResultLines;
	// END To uses the internal buffer for GetLinesByXXX results
//------
// Mainheader definitions
	CUIntArray   omMainHeaderRanges;
	CStringArray omMainHeaderValues;
	CUIntArray   omMainHeaderColors;
	CUIntArray   omColumnSeparatorWidths;
	CFont		 omMainHeaderFont;
	BOOL	     bmNoHeader;
//------
	CFont	omHeaderFont;
	long	omHeaderTextColor;
	long	omHeaderBackColor;
	CFont omTabFont;
	CScrollBar *pomHScroll;
	CScrollBar *pomVScroll;
	short imLineHeight;
	int imLogicalWidth;
	int imSelectedLine;

	bool bmAllSelected;
	bool isInitialized;

	BOOL bmShowHScroller;
	BOOL bmShowVScroller;
	BOOL bmVScrollerAutoHidden;

	BOOL bmEnableHeaderSelection;
	BOOL bmEnableHeaderSizing;
	BOOL bmTabFontBold;

	BOOL bmScrollLocked;
	//MWO: 13.08.2002 store the last sort order
	CUIntArray omLastSortOrder;
	bool bmTabIsSorted;
	//MWO: END

// Inplace Edit Properties and Functions
	BOOL bmEnableInlineEdit;
	BOOL bmEditMoveInternal;
	UInlineEdit* pomEdit;
	int imEditCol,
		imEditLine,
		imOldEditCol,
		imOldEditLine;
	CString olTempText,	//stores the window-text if the edit is out of the visible area
			olOldValue;	//stores the old cell value when beginning inplace-editing

	int imResizeOffset;			// Header resizing offset = tolerance area
	int imDirectionPixels;		// Sizing to left or right ==> 1 or -1
	int imCurrentResizeColumn;	// when is in resizing ==> we need the specif. col
	int imResizeColMinX;
	bool isInColumnResizing;	// Indicator, where TAB is in resizemode for a column

	CStatic *pomSizeMarker;		//Static will be moved to represent the sizeposition
	long imComboMinWidth;
	long lmCurrMouseOverHeaderColumn;
	long lmOldMouseOverHeaderColumn;
	bool bmDrawHeaderOnly;
//---------------------------------------
// CEDA Connection variables
	FastCedaConnection omFastCeda;

// Overrides 
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTABCtrl)
	public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	virtual BOOL OnSetObjectRects(LPCRECT lpRectPos, LPCRECT lpRectClip);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void OnInactiveMouseMove(LPCRECT lprcBounds, long x, long y, DWORD dwKeyState);
	//}}AFX_VIRTUAL

// Implementation
protected:
	~CTABCtrl();

	DECLARE_OLECREATE_EX(CTABCtrl)		// Class factory and guid
	DECLARE_OLETYPELIB(CTABCtrl)		// GetTypeInfo
	DECLARE_PROPPAGEIDS(CTABCtrl)		// Property page IDs
	DECLARE_OLECTLTYPE(CTABCtrl)		// Type name and misc status

	int CalcHorzScrollRange();
	bool IsInColumnResizeArea(int ipX, int &ripColumn);

	int imCountTimeValues;
// Message maps
	//{{AFX_MSG(CTABCtrl)
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnTabCBSelCHange(UINT wParam, LONG lParam);
	afx_msg void OnEditKillFocus(UINT wParam, LONG lParam);
	afx_msg void OnEditEscape(UINT wParam, LONG lParam);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	BOOL bTracking; 
// Dispatch maps
	//{{AFX_DISPATCH(CTABCtrl)
	afx_msg void OnMouseLeave(); 

	long m_columns;
	afx_msg void OnColumnsChanged();
	long m_lines;
	afx_msg void OnLinesChanged();
	CString m_headerString;
	afx_msg void OnHeaderStringChanged();
	CString m_headerLengthString;
	afx_msg void OnHeaderLengthStringChanged();
	short m_lineHeight;
	afx_msg void OnLineHeightChanged();
	short m_fontSize;
	afx_msg void OnFontSizeChanged();
	short m_HeaderFontSize;
	afx_msg void OnHeaderFontSizeChanged();
	CString m_fontName;
	afx_msg void OnFontNameChanged();
	long m_leftTextOffset;
	afx_msg void OnLeftTextOffsetChanged();
	BOOL m_vScrollMaster;
	afx_msg void OnVScrollMasterChanged();
	short m_postEnterBehavior;
	afx_msg void OnPostEnterBehaviorChanged();
	BOOL m_hScrollMaster;
	afx_msg void OnHScrollMasterChanged();
	CString m_version;
	afx_msg void OnVersionChanged();
	BOOL m_inplaceEditUpperCase;
	afx_msg void OnInplaceEditUpperCaseChanged();
	long m_gridLineColor;
	afx_msg void OnGridLineColorChanged();
	long m_emptyAreaBackColor;
	afx_msg void OnEmptyAreaBackColorChanged();
	CString m_columnWidthString;
	afx_msg void OnColumnWidthStringChanged();
	CString m_columnAlignmentString;
	afx_msg void OnColumnAlignmentStringChanged();
	long m_selectBackColor;
	afx_msg void OnSelectBackColorChanged();
	long m_selectTextColor;
	afx_msg void OnSelectTextColorChanged();
	long m_emptyAreaRightColor;
	afx_msg void OnEmptyAreaRightColorChanged();
	long m_selectColumnBackColor;
	afx_msg void OnSelectColumnBackColorChanged();
	long m_selectColumnTextColor;
	afx_msg void OnSelectColumnTextColorChanged();
	long m_displayTextColor;
	afx_msg void OnDisplayTextColorChanged();
	long m_displayBackColor;
	afx_msg void OnDisplayBackColorChanged();
	BOOL m_mainHeader;
	afx_msg void OnMainHeaderChanged();
	BOOL m_mainHeaderOnly;
	afx_msg void OnMainHeaderOnlyChanged();
	BOOL m_showRowSelection;
	afx_msg void OnShowRowSelectionChanged();
	CString m_setLocaleLanguageString;
	afx_msg void OnSetLocaleLanguageStringChanged();
	CString m_CedacurrentApplication;
	afx_msg void OnCedaCurrentApplicationChanged();
	CString m_Cedaport;
	afx_msg void OnCedaPortChanged();
	CString m_Cedahopo;
	afx_msg void OnCedaHopoChanged();
	CString m_CedaserverName;
	afx_msg void OnCedaServerNameChanged();
	CString m_Cedatabext;
	afx_msg void OnCedaTabextChanged();
	CString m_Cedauser;
	afx_msg void OnCedaUserChanged();
	CString m_Cedaworkstation;
	afx_msg void OnCedaWorkstationChanged();
	short m_CedasendTimeout;
	afx_msg void OnCedaSendTimeoutChanged();
	short m_CedareceiveTimeout;
	afx_msg void OnCedaReceiveTimeoutChanged();
	CString m_cedaRecordSeparator;
	afx_msg void OnCedaRecordSeparatorChanged();
	CString m_cedaIdentifier;
	afx_msg void OnCedaIdentifierChanged();
	CString m_headerAlignmentString;
	afx_msg void OnHeaderAlignmentStringChanged();
	BOOL m_autoSizeByHeader;
	afx_msg void OnAutoSizeByHeaderChanged();
	BOOL m_inplaceEditSendKeyEvents;
	afx_msg void OnInplaceEditSendKeyEventsChanged();
	CString m_logicalFieldList;
	afx_msg void OnLogicalFieldListChanged();
	CString m_cedaFieldSeparator;
	afx_msg void OnCedaFieldSeparatorChanged();
	CString m_myTag;
	afx_msg void OnMyTagChanged();
	CString m_myName;
	afx_msg void OnMyNameChanged();
	CString m_buildDate;
	afx_msg void OnBuildDateChanged();
	BOOL m_defaultCursor;
	afx_msg void OnDefaultCursorChanged();
	BOOL m_lifeStyle;
	afx_msg void OnLifeStyleChanged();
	BOOL m_cursorLifeStyle;
	afx_msg void OnCursorLifeStyleChanged();
	BOOL m_columnSelectionLifeStyle;
	afx_msg void OnColumnSelectionLifeStyleChanged();
	BOOL m_sortOrderASC;
	afx_msg void OnSortOrderASCChanged();
	long m_currentSortColumn;
	afx_msg void OnCurrentSortColumnChanged();
	afx_msg BSTR GetNoFocusColumns();
	afx_msg void SetNoFocusColumns(LPCTSTR lpszNewValue);
	afx_msg long GetComboMinWidth();
	afx_msg void SetComboMinWidth(long nNewValue);
	afx_msg void SetLineCount(long ipCount);
	afx_msg void SetColumnCount(long ipCount);
	afx_msg void SetHeaderText(LPCTSTR HeaderText);
	afx_msg void ResetContent();
	afx_msg void InsertTextLine(LPCTSTR LineText, BOOL bpRedraw);
	afx_msg long GetLineCount();
	afx_msg void SetLineColor(long LineNo, long TextColor, long BackColor);
	afx_msg void InsertTextLineAt(long LineNo, LPCTSTR LineText, BOOL bpRedraw);
	afx_msg short DeleteLine(long LineNo);
	afx_msg long GetCurrentSelected();
	afx_msg BSTR GetLineValues(long LineNo);
	afx_msg BSTR GetLinesByTextColor(long Color);
	afx_msg BSTR GetLinesByBackColor(long Color);
	afx_msg void Init();
	afx_msg BSTR GetColumnValue(long LineNo, long ColNo);
	afx_msg void SetColumnValue(long LineNo, long ColNo, LPCTSTR Value);
	afx_msg BSTR GetLinesByColor(long BackColor, long TextColor);
	afx_msg void UpdateTextLine(long LineNo, LPCTSTR Values, BOOL Redraw);
	afx_msg void RedrawTab();
	afx_msg void SetCurrentSelection(long LineNo);
	afx_msg void SetFieldSeparator(LPCTSTR NewSeparator);
	afx_msg void LockScroll(BOOL Locked);
	afx_msg BSTR GetLinesByColumnValue(long ColNo, LPCTSTR Value, short CompareMethod);
	afx_msg void GetLineColor(long LineNo, long FAR* txtColor, long FAR* bkColor);
	afx_msg BSTR GetSelectedColumns();
	afx_msg void EnableHeaderSelection(BOOL enable);
	afx_msg void EnableInlineEdit(BOOL IE_Enable);
	afx_msg void EnableInlineMoveInternal(BOOL IE_Move);
	afx_msg long InsertBuffer(LPCTSTR StrBuffer, LPCTSTR LineSeparator);
	afx_msg void ShowHorzScroller(BOOL Show);
	afx_msg void EnableHeaderSizing(BOOL enable);
	afx_msg void SetTabFontBold(BOOL bold);
	afx_msg void ColSelectionRemoveAll();
	afx_msg void ColSelectionRemove(long ColNo);
	afx_msg void ColSelectionAdd(long ColNo);
	afx_msg void OnVScrollTo(long LineNo);
	afx_msg long GetColumnCount();
	afx_msg BSTR GetHeaderText();
	afx_msg void ShowVertScroller(BOOL Show);
	afx_msg void OnHScrollTo(long ColNo);
	afx_msg void SetRegExFormatString(LPCTSTR FormatString);
	afx_msg BOOL CheckRegExMatchCell(long LineNo, long ColNo);
	afx_msg BSTR CheckRegExMatchLine(long LineNo, BOOL TrueFalse);
	afx_msg void SetInplaceEdit(long LineNo, long ColNo, BOOL bAssumeValue);
	afx_msg long GetVScrollPos();
	afx_msg long GetHScrollPos();
	afx_msg void Sort(LPCTSTR ColNo, BOOL bAscending, BOOL bCaseSensitive);
	afx_msg BSTR SelectDistinct(LPCTSTR Cols, LPCTSTR StrPrefix, LPCTSTR StrAppendix, LPCTSTR StrDelimiter, BOOL Distinct);
	afx_msg long GetLineStatusValue(long LineNo);
	afx_msg BSTR GetLinesByStatusValue(long Value, short CompareMethod);
	afx_msg void SetLineStatusValue(long LineNo, long Value);
	afx_msg void SetMainHeaderValues(LPCTSTR Ranges, LPCTSTR Values, LPCTSTR Colors);
	afx_msg BSTR GetMainHeaderRanges();
	afx_msg BSTR GetMainHeaderValues();
	afx_msg BSTR GetMainHeaderColors();
	afx_msg BSTR GetNextLineByColumnValue(long ColNo, LPCTSTR Value, short CompareMethod);
	afx_msg void ResetCellProperty(long LineNo, long ColNo);
	afx_msg BOOL SetCellProperty(long LineNo, long ColNo, LPCTSTR ObjID);
	afx_msg void CreateCellObj(LPCTSTR ObjID, long BackColor, long TextColor, short TextSize, BOOL bItalic, BOOL bUnderline, BOOL bBold, short CharSet, LPCTSTR FontName);
	afx_msg BSTR GetLinesByColumnValues(long ColNo, LPCTSTR Values, short CompareMethod);
	afx_msg void CreateCellBitmpapObj(LPCTSTR ObjID, LPCTSTR BitmapFile);
	afx_msg BOOL SetCellBitmapProperty(long LineNo, long ColNo, LPCTSTR ObjID);
	afx_msg void ResetCellBitmapProperty(long LineNo, long ColNo);
	afx_msg BSTR GetBitmapFileName(long LineNo, long ColNo);
	afx_msg void ResetCellProperties(long LineNo);
	afx_msg void ResetBitmapProperties(long LineNo);
	afx_msg BSTR GetLinesByMultipleColumnValue(LPCTSTR Columns, LPCTSTR Values, short CompareMethod);
	afx_msg BOOL SetColumnProperty(long ColNo, LPCTSTR ObjID);
	afx_msg void ResetColumnProperty(long ColNo);
	afx_msg BOOL SetHeaderFont(short TextSize, BOOL bItalic, BOOL bUnderline, BOOL bBold, short CharSet, LPCTSTR FontName);
	afx_msg BOOL SetHeaderColors(long BackColor, long FontColor);
	afx_msg BOOL SetMainHeaderFont(short TextSize, BOOL bItalic, BOOL bUnderline, BOOL bBold, short CharSet, LPCTSTR FontName);
	afx_msg void TimerSetValue(long LineNo, short Value);
	afx_msg void TimerCheck();
	afx_msg void PrintTabShowDialog(LPCTSTR sDocTitle, LPCTSTR sDate);
	afx_msg void PrintTab(long lhDC, LPCTSTR sDocTitle, LPCTSTR sDate);
	afx_msg void PrintSetPageHeaderHeight(long lHeight);
	afx_msg long PrintGetPageHeaderHeight();
	afx_msg void PrintSetPageFooterHeight(long lHeight);
	afx_msg long PrintGetPageFooterHeight();
	afx_msg void PrintSetDatePosition(LPCTSTR sLocation, LPCTSTR sAlignment);
	afx_msg void PrintGetDatePosition(BSTR FAR* psLocation, BSTR FAR* psAlignment);
	afx_msg void PrintSetTitlePosition(LPCTSTR sLocation, LPCTSTR sAlignment);
	afx_msg void PrintGetTitlePosition(BSTR FAR* psLocation, BSTR FAR* psAlignment);
	afx_msg void PrintSetPageEnumeration(LPCTSTR sLoaction, LPCTSTR sAlignment, long lStyle);
	afx_msg void PrintGetPageEnumeration(BSTR FAR* psLocation, BSTR FAR* psAlignment, long FAR* plStyle);
	afx_msg void PrintSetLogo(LPCTSTR sLocation, LPCTSTR sAlignment, LPCTSTR sPath);
	afx_msg void PrintGetLogo(BSTR FAR* psLocation, BSTR FAR* psAlignment, BSTR FAR* psPath);
	afx_msg void PrintSetSeparatorLineTop(long lDistance, long lWidth);
	afx_msg void PrintGetSeparatorLineTop(long FAR* plDistance, long FAR* plWidth);
	afx_msg void PrintSetSeparatorLineBottom(long lDistance, long lWidth);
	afx_msg void PrintGetSeparatorLineBottom(long FAR* plDistance, long FAR* plWidth);
	afx_msg void PrintSetFitToPage(BOOL bFitToPage);
	afx_msg BOOL PrintGetFitToPage();
	afx_msg void PrintSetColOrderString(LPCTSTR sColString);
	afx_msg BSTR PrintGetColOrderString();
	afx_msg void PrintSetMargins(long lTop, long lBottom, long lLeft, long lRight);
	afx_msg void PrintGetMargins(long FAR* plTop, long FAR* plBottom, long FAR* plLeft, long FAR* plRight);
	afx_msg BOOL PrintSetPageTitleFont(short TextSize, BOOL bItalic, BOOL bUnderline, BOOL bBold, short CharSet, LPCTSTR FontName);
	afx_msg void PrintRepeatStatus(short sRepeatStatus);
	afx_msg BSTR GetBuffer(long lFromLine, long lToLine, LPCTSTR LineSeparator);
	afx_msg BOOL CedaAction(LPCTSTR Cmd, LPCTSTR DBTable, LPCTSTR DBFields, LPCTSTR DBData, LPCTSTR DBWhere);
	afx_msg BSTR GetLastCedaError();
	afx_msg BOOL WriteToFile(LPCTSTR FileName, BOOL WithBasicInfo);
	afx_msg BOOL ReadFromFile(LPCTSTR FileName);
	afx_msg void DateTimeSetColumn(long ColNo);
	afx_msg void DateTimeResetColumn(long ColNo);
	afx_msg void DateTimeSetInputFormatString(long ColNo, LPCTSTR FormatString);
	afx_msg BSTR DateTimeGetInputFormatString(long ColNo);
	afx_msg void DateTimeSetOutputFormatString(long ColNo, LPCTSTR FormatString);
	afx_msg BSTR DateTimeGetOutputFormatString(long ColNo);
	afx_msg void DateTimeSetUTCOffsetMinutes(long ColNo, long OffsetMinutes);
	afx_msg long DateTimeGetUTCOffsetMinutes(long ColNo);
	afx_msg void DateTimeSetMonthNames(LPCTSTR MonthNames);
	afx_msg BSTR DateTimeGetMonthNames();
	afx_msg BOOL WriteToFileWithProperties(LPCTSTR FileName);
	afx_msg BOOL ReadFromFileWithProperties(LPCTSTR FileName);
	afx_msg void SetLineTag(long LineNo, LPCTSTR Values);
	afx_msg BSTR GetLineTag(long LineNo);
	afx_msg BSTR GetLineTagKeyItem(long LineNo, LPCTSTR KeyItem, LPCTSTR StartSeparator, LPCTSTR EndSeparator);
	afx_msg void AutoSizeColumns();
	afx_msg BOOL SetColumnBoolProperty(long ColNo, LPCTSTR TrueValue, LPCTSTR FalseValue);
	afx_msg void ResetColumnBoolProperty(long ColNo);
	afx_msg void CreateDecorationObject(LPCTSTR ID, LPCTSTR LocationList, LPCTSTR PixelList, LPCTSTR ColorList);
	afx_msg void SetDecorationObject(long LineNo, long ColNo, LPCTSTR ID);
	afx_msg void ResetCellDecoration(long LineNo, long ColNo);
	afx_msg void ResetLineDecorations(long LineNo);
	afx_msg void ComboAddTextLines(LPCTSTR ComboID, LPCTSTR Buffer, LPCTSTR LineSeparator);
	afx_msg void ComboSetGridStyle(LPCTSTR ComboID, short PixelWidth, long GridColor);
	afx_msg void ComboDeleteLine(LPCTSTR ComboID, long LineNo);
	afx_msg void ComboSetLineValues(LPCTSTR ComboID, long LineNo, LPCTSTR Values);
	afx_msg void ComboSetResultColumn(LPCTSTR ComboID, long ColNo);
	afx_msg void SetComboColumn(LPCTSTR ComboID, long ColNo);
	afx_msg void ComboSetLineColors(LPCTSTR ComboID, long LineNo, long TextColor, long BackColor);
	afx_msg void ComboSetColumnLengthString(LPCTSTR ComboID, LPCTSTR LenString);
	afx_msg void CreateComboObject(LPCTSTR ComboID, long TabColumn, long ComboColumns, LPCTSTR Style, long ListboxWidth);
	afx_msg void ComboResetContent(LPCTSTR ComboID);
	afx_msg void ComboResetObject(LPCTSTR ComboID);
	afx_msg BOOL CedaFreeCommand(LPCTSTR strCommand);
	afx_msg void SetBoolPropertyReadOnly(long ColNo, BOOL SetResetValue);
	afx_msg BSTR GetColumnValues(long LineNo, LPCTSTR ColumnList);
	afx_msg void ComboShow(LPCTSTR ComboID, BOOL Show);
	afx_msg BSTR GetFieldSeparator();
	afx_msg void ResetConfiguration();
	afx_msg void IndexCreate(LPCTSTR Name, long ColNo);
	afx_msg void IndexDestroy(LPCTSTR IndexName);
	afx_msg BSTR GetLinesByIndexValue(LPCTSTR IndexName, LPCTSTR SearchValue, long CompareMethod);
	afx_msg long IndexGetDataLineNo(LPCTSTR IndexName, long IndexLine);
	afx_msg BSTR GetFieldValue(long LineNo, LPCTSTR FieldName);
	afx_msg void SetUniqueFields(LPCTSTR FieldList);
	afx_msg void SetInternalLineBuffer(BOOL blOn);
	afx_msg long GetNextResultLine();
	afx_msg void DateTimeSetColumnFormat(long ColNo, LPCTSTR InputFormat, LPCTSTR OutputFormat);
	afx_msg void SetFieldValues(long LineNo, LPCTSTR FieldList, LPCTSTR ValueList);
	afx_msg BSTR GetFieldValues(long LineNo, LPCTSTR FieldList);
	afx_msg BSTR GetBufferByFieldList(long lFromLine, long lToLine, LPCTSTR FieldList, LPCTSTR LineSeparator);
	afx_msg void ReorgIndexes();
	afx_msg void CursorDecoration(LPCTSTR LogicalFields, LPCTSTR LocationList, LPCTSTR PixelList, LPCTSTR ColorList);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()

	afx_msg void AboutBox();


// Event maps
	//{{AFX_EVENT(CTABCtrl)
	void FireSendLButtonClick(long LineNo, long ColNo)
		{FireEvent(eventidSendLButtonClick,EVENT_PARAM(VTS_I4  VTS_I4), LineNo, ColNo);}
	void FireSendLButtonDblClick(long LineNo, long ColNo)
		{FireEvent(eventidSendLButtonDblClick,EVENT_PARAM(VTS_I4  VTS_I4), LineNo, ColNo);}
	void FireSendRButtonClick(long LineNo, long ColNo)
		{FireEvent(eventidSendRButtonClick,EVENT_PARAM(VTS_I4  VTS_I4), LineNo, ColNo);}
	void FireSendMouseMove(long LineNo, long ColNo, LPCTSTR Flags)
		{FireEvent(eventidSendMouseMove,EVENT_PARAM(VTS_I4  VTS_I4  VTS_BSTR), LineNo, ColNo, Flags);}
	void FireColumnSelectionChanged(long ColNo, BOOL Selected)
		{FireEvent(eventidColumnSelectionChanged,EVENT_PARAM(VTS_I4  VTS_BOOL), ColNo, Selected);}
	void FireOnVScroll(long LineNo)
		{FireEvent(eventidOnVScroll,EVENT_PARAM(VTS_I4), LineNo);}
	void FireInplaceEditCell(long LineNo, long ColNo, LPCTSTR NewValue, LPCTSTR OldValue)
		{FireEvent(eventidInplaceEditCell,EVENT_PARAM(VTS_I4  VTS_I4  VTS_BSTR  VTS_BSTR), LineNo, ColNo, NewValue, OldValue);}
	void FireOnHScroll(long ColNo)
		{FireEvent(eventidOnHScroll,EVENT_PARAM(VTS_I4), ColNo);}
	void FireRowSelectionChanged(long LineNo, BOOL Selected)
		{FireEvent(eventidRowSelectionChanged,EVENT_PARAM(VTS_I4  VTS_BOOL), LineNo, Selected);}
	void FireHitKeyOnLine(short Key, long LineNo)
		{FireEvent(eventidHitKeyOnLine,EVENT_PARAM(VTS_I2  VTS_I4), Key, LineNo);}
	void FireTimerExpired(long LineNo, long LineStatus)
		{FireEvent(eventidTimerExpired,EVENT_PARAM(VTS_I4  VTS_I4), LineNo, LineStatus);}
	void FirePrintFinishedTab()
		{FireEvent(eventidPrintFinishedTab,EVENT_PARAM(VTS_NONE));}
	void FireComboSelChanged(long LineNo, long ColNo, LPCTSTR OldValue, LPCTSTR NewValues)
		{FireEvent(eventidComboSelChanged,EVENT_PARAM(VTS_I4  VTS_I4  VTS_BSTR  VTS_BSTR), LineNo, ColNo, OldValue, NewValues);}
	void FireEditPositionChanged(long LineNo, long ColNo, LPCTSTR Value)
		{FireEvent(eventidEditPositionChanged,EVENT_PARAM(VTS_I4  VTS_I4  VTS_BSTR), LineNo, ColNo, Value);}
	void FireCloseInplaceEdit(long LineNo, long ColNo)
		{FireEvent(eventidCloseInplaceEdit,EVENT_PARAM(VTS_I4  VTS_I4), LineNo, ColNo);}
	void FireBoolPropertyChanged(long LineNo, long ColNo, BOOL NewValue)
		{FireEvent(eventidBoolPropertyChanged,EVENT_PARAM(VTS_I4  VTS_I4  VTS_BOOL), LineNo, ColNo, NewValue);}
	void FirePackageReceived(LONG lpPackage)
		{FireEvent(eventidPackageReceived, EVENT_PARAM(VTS_I4), lpPackage);}
	//}}AFX_EVENT
	DECLARE_EVENT_MAP()

// Dispatch and event IDs
public:
	CFont omPageTitleFont;
	CString MakeTimeString(CString olStr, long ColNo);
	void SendHitKeyOnLine(short Key, long LineNo);
	CString GetColumnValueInternal(long LineNo, long ColNo); 
	short smBlindOffset;
	BOOL TestColumnFocus(int ilColNo);
	CUIntArray omNoFocusColumns;
	//CString omFormatString;
	COleDateTime MakeTimeFromString(CString olTimeString, CString olFormatString);
	CString omColumnDataType;
	CString omRegExFormatString;
	void HandleSelectionScrolling();
	int CalcHScrollRange();
	void HandleEditScrolling();
	void MoveEditSelection(BOOL blForward);
	void MoveEdit(short olDirection, BOOL blAssumeValue);
	void MoveComboboxes(long ipLineNo);
	CRect GetCellRect(long llLine, long llColumn);
	int CountHeaderLines();
	void SetVScrollRange();
	int  GetVisibleLines();
	//MWO: 13.08.2002
	CString SearchBinaryByColumnValue(long ColNo, LPCTSTR Value, short CompareMethod) ;
	//MWO: END
	void ShowComboObject(long ColNo, COMBO_OBJECT_PROP *popCbProps);
	void DrawDecoObject(CDC *pdc, DECORATION_OBJECT *polDecoObj, CRect &olHRect);
	COMBO_OBJECT_PROP *GetComboPropByID(CString id);
	INDEX_OBJECT *GetIndexObjByName(CString IndexName);
	CString SearchInIndexBinary(CString opIndexName, LPCTSTR Value, short CompareMethod);
	bool CheckForUnique(char* pcpLineValues);
	CString GetLinesByMultipleColumnValueInternal(LPCTSTR Columns, LPCTSTR Values, short CompareMethod);

	bool IsDataTimeColumn(long ColNo);
	long GetIndexFromLogicalField(CString opField);
	void DrawSortMarker(CDC *pdc, CRect &olHRect);

	void DrawLifeStyle(CDC *pdc, CRect opRect, int MyColor, int ipMode, BOOL DrawDown, int ipPercent);
	CString	CheckFilePath(const CString& ropFilePath);
	enum {
		eventidPackageReceived = 17L,		dispidCedaErrorSimulation = 219,		dispidCedaPacketSize = 218,		dispidSqlAccessMethod = 217,		dispidUCom = 216,		dispidComboOwnerDraw = 215,	//{{AFX_DISP_ID(CTABCtrl)
	dispidColumns = 1L,
	dispidLines = 2L,
	dispidHeaderString = 3L,
	dispidHeaderLengthString = 4L,
	dispidLineHeight = 5L,
	dispidFontSize = 6L,
	dispidHeaderFontSize = 7L,
	dispidFontName = 8L,
	dispidLeftTextOffset = 9L,
	dispidVScrollMaster = 10L,
	dispidPostEnterBehavior = 11L,
	dispidHScrollMaster = 12L,
	dispidNoFocusColumns = 55L,
	dispidVersion = 13L,
	dispidInplaceEditUpperCase = 14L,
	dispidGridLineColor = 15L,
	dispidEmptyAreaBackColor = 16L,
	dispidColumnWidthString = 17L,
	dispidColumnAlignmentString = 18L,
	dispidSelectBackColor = 19L,
	dispidSelectTextColor = 20L,
	dispidEmptyAreaRightColor = 21L,
	dispidSelectColumnBackColor = 22L,
	dispidSelectColumnTextColor = 23L,
	dispidDisplayTextColor = 24L,
	dispidDisplayBackColor = 25L,
	dispidMainHeader = 26L,
	dispidMainHeaderOnly = 27L,
	dispidShowRowSelection = 28L,
	dispidSetLocaleLanguageString = 29L,
	dispidCedaCurrentApplication = 30L,
	dispidCedaPort = 31L,
	dispidCedaHopo = 32L,
	dispidCedaServerName = 33L,
	dispidCedaTabext = 34L,
	dispidCedaUser = 35L,
	dispidCedaWorkstation = 36L,
	dispidCedaSendTimeout = 37L,
	dispidCedaReceiveTimeout = 38L,
	dispidCedaRecordSeparator = 39L,
	dispidCedaIdentifier = 40L,
	dispidHeaderAlignmentString = 41L,
	dispidAutoSizeByHeader = 42L,
	dispidInplaceEditSendKeyEvents = 43L,
	dispidComboMinWidth = 56L,
	dispidLogicalFieldList = 44L,
	dispidCedaFieldSeparator = 45L,
	dispidMyTag = 46L,
	dispidMyName = 47L,
	dispidBuildDate = 48L,
	dispidDefaultCursor = 49L,
	dispidLifeStyle = 50L,
	dispidCursorLifeStyle = 51L,
	dispidColumnSelectionLifeStyle = 52L,
	dispidSortOrderASC = 53L,
	dispidCurrentSortColumn = 54L,
	dispidSetLineCount = 57L,
	dispidSetColumnCount = 58L,
	dispidSetHeaderText = 59L,
	dispidResetContent = 60L,
	dispidInsertTextLine = 61L,
	dispidGetLineCount = 62L,
	dispidSetLineColor = 63L,
	dispidInsertTextLineAt = 64L,
	dispidDeleteLine = 65L,
	dispidGetCurrentSelected = 66L,
	dispidGetLineValues = 67L,
	dispidGetLinesByTextColor = 68L,
	dispidGetLinesByBackColor = 69L,
	dispidInit = 70L,
	dispidGetColumnValue = 71L,
	dispidSetColumnValue = 72L,
	dispidGetLinesByColor = 73L,
	dispidUpdateTextLine = 74L,
	dispidRedrawTab = 75L,
	dispidSetCurrentSelection = 76L,
	dispidSetFieldSeparator = 77L,
	dispidLockScroll = 78L,
	dispidGetLinesByColumnValue = 79L,
	dispidGetLineColor = 80L,
	dispidGetSelectedColumns = 81L,
	dispidEnableHeaderSelection = 82L,
	dispidEnableInlineEdit = 83L,
	dispidEnableInlineMoveInternal = 84L,
	dispidInsertBuffer = 85L,
	dispidShowHorzScroller = 86L,
	dispidEnableHeaderSizing = 87L,
	dispidSetTabFontBold = 88L,
	dispidColSelectionRemoveAll = 89L,
	dispidColSelectionRemove = 90L,
	dispidColSelectionAdd = 91L,
	dispidOnVScrollTo = 92L,
	dispidGetColumnCount = 93L,
	dispidGetHeaderText = 94L,
	dispidShowVertScroller = 95L,
	dispidOnHScrollTo = 96L,
	dispidSetRegExFormatString = 97L,
	dispidCheckRegExMatchCell = 98L,
	dispidCheckRegExMatchLine = 99L,
	dispidSetInplaceEdit = 100L,
	dispidGetVScrollPos = 101L,
	dispidGetHScrollPos = 102L,
	dispidSort = 103L,
	dispidSelectDistinct = 104L,
	dispidGetLineStatusValue = 105L,
	dispidGetLinesByStatusValue = 106L,
	dispidSetLineStatusValue = 107L,
	dispidSetMainHeaderValues = 108L,
	dispidGetMainHeaderRanges = 109L,
	dispidGetMainHeaderValues = 110L,
	dispidGetMainHeaderColors = 111L,
	dispidGetNextLineByColumnValue = 112L,
	dispidResetCellProperty = 113L,
	dispidSetCellProperty = 114L,
	dispidCreateCellObj = 115L,
	dispidGetLinesByColumnValues = 116L,
	dispidCreateCellBitmpapObj = 117L,
	dispidSetCellBitmapProperty = 118L,
	dispidResetCellBitmapProperty = 119L,
	dispidGetBitmapFileName = 120L,
	dispidResetCellProperties = 121L,
	dispidResetBitmapProperties = 122L,
	dispidGetLinesByMultipleColumnValue = 123L,
	dispidSetColumnProperty = 124L,
	dispidResetColumnProperty = 125L,
	dispidSetHeaderFont = 126L,
	dispidSetHeaderColors = 127L,
	dispidSetMainHeaderFont = 128L,
	dispidTimerSetValue = 129L,
	dispidTimerCheck = 130L,
	dispidPrintTabShowDialog = 131L,
	dispidPrintTab = 132L,
	dispidPrintSetPageHeaderHeight = 133L,
	dispidPrintGetPageHeaderHeight = 134L,
	dispidPrintSetPageFooterHeight = 135L,
	dispidPrintGetPageFooterHeight = 136L,
	dispidPrintSetDatePosition = 137L,
	dispidPrintGetDatePosition = 138L,
	dispidPrintSetTitlePosition = 139L,
	dispidPrintGetTitlePosition = 140L,
	dispidPrintSetPageEnumeration = 141L,
	dispidPrintGetPageEnumeration = 142L,
	dispidPrintSetLogo = 143L,
	dispidPrintGetLogo = 144L,
	dispidPrintSetSeparatorLineTop = 145L,
	dispidPrintGetSeparatorLineTop = 146L,
	dispidPrintSetSeparatorLineBottom = 147L,
	dispidPrintGetSeparatorLineBottom = 148L,
	dispidPrintSetFitToPage = 149L,
	dispidPrintGetFitToPage = 150L,
	dispidPrintSetColOrderString = 151L,
	dispidPrintGetColOrderString = 152L,
	dispidPrintSetMargins = 153L,
	dispidPrintGetMargins = 154L,
	dispidPrintSetPageTitleFont = 155L,
	dispidPrintRepeatStatus = 156L,
	dispidGetBuffer = 157L,
	dispidCedaAction = 158L,
	dispidGetLastCedaError = 159L,
	dispidWriteToFile = 160L,
	dispidReadFromFile = 161L,
	dispidDateTimeSetColumn = 162L,
	dispidDateTimeResetColumn = 163L,
	dispidDateTimeSetInputFormatString = 164L,
	dispidDateTimeGetInputFormatString = 165L,
	dispidDateTimeSetOutputFormatString = 166L,
	dispidDateTimeGetOutputFormatString = 167L,
	dispidDateTimeSetUTCOffsetMinutes = 168L,
	dispidDateTimeGetUTCOffsetMinutes = 169L,
	dispidDateTimeSetMonthNames = 170L,
	dispidDateTimeGetMonthNames = 171L,
	dispidWriteToFileWithProperties = 172L,
	dispidReadFromFileWithProperties = 173L,
	dispidSetLineTag = 174L,
	dispidGetLineTag = 175L,
	dispidGetLineTagKeyItem = 176L,
	dispidAutoSizeColumns = 177L,
	dispidSetColumnBoolProperty = 178L,
	dispidResetColumnBoolProperty = 179L,
	dispidCreateDecorationObject = 180L,
	dispidSetDecorationObject = 181L,
	dispidResetCellDecoration = 182L,
	dispidResetLineDecorations = 183L,
	dispidComboAddTextLines = 184L,
	dispidComboSetGridStyle = 185L,
	dispidComboDeleteLine = 186L,
	dispidComboSetLineValues = 187L,
	dispidComboSetResultColumn = 188L,
	dispidSetComboColumn = 189L,
	dispidComboSetLineColors = 190L,
	dispidComboSetColumnLengthString = 191L,
	dispidCreateComboObject = 192L,
	dispidComboResetContent = 193L,
	dispidComboResetObject = 194L,
	dispidCedaFreeCommand = 195L,
	dispidSetBoolPropertyReadOnly = 196L,
	dispidGetColumnValues = 197L,
	dispidComboShow = 198L,
	dispidGetFieldSeparator = 199L,
	dispidResetConfiguration = 200L,
	dispidIndexCreate = 201L,
	dispidIndexDestroy = 202L,
	dispidGetLinesByIndexValue = 203L,
	dispidIndexGetDataLineNo = 204L,
	dispidGetFieldValue = 205L,
	dispidSetUniqueFields = 206L,
	dispidSetInternalLineBuffer = 207L,
	dispidGetNextResultLine = 208L,
	dispidDateTimeSetColumnFormat = 209L,
	dispidSetFieldValues = 210L,
	dispidGetFieldValues = 211L,
	dispidGetBufferByFieldList = 212L,
	dispidReorgIndexes = 213L,
	dispidCursorDecoration = 214L,
	eventidSendLButtonClick = 1L,
	eventidSendLButtonDblClick = 2L,
	eventidSendRButtonClick = 3L,
	eventidSendMouseMove = 4L,
	eventidColumnSelectionChanged = 5L,
	eventidOnVScroll = 6L,
	eventidInplaceEditCell = 7L,
	eventidOnHScroll = 8L,
	eventidRowSelectionChanged = 9L,
	eventidHitKeyOnLine = 10L,
	eventidTimerExpired = 11L,
	eventidPrintFinishedTab = 12L,
	eventidComboSelChanged = 13L,
	eventidEditPositionChanged = 14L,
	eventidCloseInplaceEdit = 15L,
	eventidBoolPropertyChanged = 16L,
	//}}AFX_DISP_ID
	};
private:
	CString MakeStringFromTime (COleDateTime olTime, char *pcpOutMask);
	CString m_PrintColOrderString;
	long m_PrintMarginRight;
	long m_PrintMarginLeft;
	long m_PrintMarginBottom;
	long m_PrintMarginTop;
	BOOL m_PrintFitToPage;
	long m_PrintSeparatorLineBottomWidth;
	long m_PrintSeparatorLineBottomDistance;
	long m_PrintSeparatorLineTopWidth;
	long m_PrintSeparatorLineTopDistance;
	long m_PrintLogoAlignment;
	long m_PrintLogoLocation;
	CString m_PrintLogoPath;
	long m_PrintPageEnumerationStyle;
	long m_PrintPageEnumerationAlignment;
	long m_PrintPageEnumerationLocation;
	long m_PrintTitleAlignment;
	long m_PrintTitleLocation;
	long m_PrintDateAlignment;
	long m_PrintDateLocation;
	long m_PrintPageFooterHeight;
	long m_PrintPageHeaderHeight;
	short smRepeatStatus;

	CUIntArray				omDateTimeColumns;
	CCSPtrArray<long>		omDateTimeUTCOffset;
	CCSPtrArray<CString>	omDateTimeInputFormatString;
	CCSPtrArray<CString>	omDateTimeOutputFormatString;
	IDispatch*				pomUCom;

private:
	void OnComboOwnerDrawChanged(void);
	BOOL m_ComboOwnerDraw;
	void GetVersionInfo();	
protected:
	IDispatch* GetUCom(void);
	void SetUCom(IDispatch* pVal);
	BSTR GetSqlAccessMethod(void);
	void SetSqlAccessMethod(LPCTSTR newVal);
	LONG GetCedaPacketSize(void);
	void SetCedaPacketSize(LONG newVal);
	BSTR GetCedaErrorSimulation(void);
	void SetCedaErrorSimulation(LPCTSTR newVal);

	static void OnReceivePackage(CCmdTarget *popCtrl,long lpPackage);

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TABCTL_H__64E8E393_05E2_11D2_9B1D_9B0630BC8F12__INCLUDED)

extern CTABCtrl *pomMe;
