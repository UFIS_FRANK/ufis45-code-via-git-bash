// TAB.odl : type library source for ActiveX Control project.

// This file will be processed by the Make Type Library (mktyplib) tool to
// produce the type library (TAB.tlb) that will become a resource in
// TAB.ocx.

#include <olectl.h>
#include <idispids.h>

[ uuid(64E8E382-05E2-11D2-9B1D-9B0630BC8F12), version(1.0),
  helpfile("TAB.hlp"),
  helpstring("TAB ActiveX Control module"),
  control ]
library TABLib
{ 
	importlib(STDOLE_TLB);
	importlib(STDTYPE_TLB);

	//  Primary dispatch interface for CTABCtrl

	[ uuid(64E8E383-05E2-11D2-9B1D-9B0630BC8F12),
	  helpstring("Dispatch interface for TAB Control"), hidden ]
	dispinterface _DTAB
	{
		properties:
			// NOTE - ClassWizard will maintain property information here.
			//    Use extreme caution when editing this section.
			//{{AFX_ODL_PROP(CTABCtrl)
			[id(1)] long Columns;
			[id(2)] long Lines;
			[id(3)] BSTR HeaderString;
			[id(4)] BSTR HeaderLengthString;
			[id(5)] short LineHeight;
			[id(6)] short FontSize;
			[id(7)] short HeaderFontSize;
			[id(8)] BSTR FontName;
			[id(9)] long LeftTextOffset;
			[id(10)] boolean VScrollMaster;
			[id(11)] short PostEnterBehavior;
			[id(12)] boolean HScrollMaster;
			[id(55)] BSTR NoFocusColumns;
			[id(13)] BSTR Version;
			[id(14)] boolean InplaceEditUpperCase;
			[id(15)] long GridLineColor;
			[id(16)] long EmptyAreaBackColor;
			[id(17)] BSTR ColumnWidthString;
			[id(18)] BSTR ColumnAlignmentString;
			[id(19)] long SelectBackColor;
			[id(20)] long SelectTextColor;
			[id(21)] long EmptyAreaRightColor;
			[id(22)] long SelectColumnBackColor;
			[id(23)] long SelectColumnTextColor;
			[id(24)] long DisplayTextColor;
			[id(25)] long DisplayBackColor;
			[id(26)] boolean MainHeader;
			[id(27)] boolean MainHeaderOnly;
			[id(28)] boolean ShowRowSelection;
			[id(29)] BSTR SetLocaleLanguageString;
			[id(30)] BSTR CedaCurrentApplication;
			[id(31)] BSTR CedaPort;
			[id(32)] BSTR CedaHopo;
			[id(33)] BSTR CedaServerName;
			[id(34)] BSTR CedaTabext;
			[id(35)] BSTR CedaUser;
			[id(36)] BSTR CedaWorkstation;
			[id(37)] short CedaSendTimeout;
			[id(38)] short CedaReceiveTimeout;
			[id(39)] BSTR CedaRecordSeparator;
			[id(40)] BSTR CedaIdentifier;
			[id(41)] BSTR HeaderAlignmentString;
			[id(DISPID_HWND)] OLE_HANDLE hWnd;
			[id(42)] boolean AutoSizeByHeader;
			[id(43)] boolean InplaceEditSendKeyEvents;
			[id(56)] long ComboMinWidth;
			[id(44)] BSTR LogicalFieldList;
			[id(45)] BSTR CedaFieldSeparator;
			[id(46)] BSTR myTag;
			[id(47)] BSTR myName;
			[id(DISPID_ENABLED), bindable, requestedit] boolean Enabled;
			[id(48)] BSTR BuildDate;
			[id(49)] boolean DefaultCursor;
			[id(50)] boolean LifeStyle;
			[id(51)] boolean CursorLifeStyle;
			[id(52)] boolean ColumnSelectionLifeStyle;
			[id(53)] boolean SortOrderASC;
			[id(54)] long CurrentSortColumn;
			[id(215)] boolean ComboOwnerDraw;
			[id(216), helpstring("property UCom")] IDispatch*UCom;
			[id(217), helpstring("property SqlAccessMethod")] BSTR SqlAccessMethod;
			[id(218), helpstring("property CedaPacketSize")] LONG CedaPacketSize;
			[id(219), helpstring("property CedaErrorSimulation")] BSTR CedaErrorSimulation;
			//}}AFX_ODL_PROP

		methods:
			// NOTE - ClassWizard will maintain method information here.
			//    Use extreme caution when editing this section.
			//{{AFX_ODL_METHOD(CTABCtrl)
			[id(57)] void SetLineCount(long ipCount);
			[id(58)] void SetColumnCount(long ipCount);
			[id(59), helpstring("Sets the header text comm separated")] void SetHeaderText(BSTR HeaderText);
			[id(60), helpstring("Clears the grid")] void ResetContent();
			[id(61), helpstring("Appends a line at the end")] void InsertTextLine(BSTR LineText, boolean bpRedraw);
			[id(62), helpstring("Returns the total amount of lines")] long GetLineCount();
			[id(63), helpstring("Sets text and backcolor LineNo, use like vbBlack")] void SetLineColor(long LineNo, long TextColor, long BackColor);
			[id(64), helpstring("Insert a text line at LineNo, redraw=true forces a refresh")] void InsertTextLineAt(long LineNo, BSTR LineText, boolean bpRedraw);
			[id(65), helpstring("Deletes the line LineNo")] short DeleteLine(long LineNo);
			[id(66)] long GetCurrentSelected();
			[id(67), helpstring("Linevalues at index, index -1 ==> Headerstring")] BSTR GetLineValues(long LineNo);
			[id(68)] BSTR GetLinesByTextColor(long Color);
			[id(69)] BSTR GetLinesByBackColor(long Color);
			[id(70)] void Init();
			[id(71)] BSTR GetColumnValue(long LineNo, long ColNo);
			[id(72)] void SetColumnValue(long LineNo, long ColNo, BSTR Value);
			[id(73)] BSTR GetLinesByColor(long BackColor, long TextColor);
			[id(74)] void UpdateTextLine(long LineNo, BSTR Values, boolean Redraw);
			[id(75)] void RedrawTab();
			[id(76)] void SetCurrentSelection(long LineNo);
			[id(77)] void SetFieldSeparator(BSTR NewSeparator);
			[id(78)] void LockScroll(boolean Locked);
			[id(79)] BSTR GetLinesByColumnValue(long ColNo, BSTR Value, short CompareMethod);
			[id(80)] void GetLineColor(long LineNo, long* txtColor, long* bkColor);
			[id(81)] BSTR GetSelectedColumns();
			[id(82)] void EnableHeaderSelection(boolean bEnable);
			[id(83)] void EnableInlineEdit(boolean IE_Enable);
			[id(84)] void EnableInlineMoveInternal(boolean IE_Move);
			[id(85)] long InsertBuffer(BSTR StrBuffer, BSTR LineSeparator);
			[id(86)] void ShowHorzScroller(boolean Show);
			[id(87)] void EnableHeaderSizing(boolean bEnable);
			[id(88)] void SetTabFontBold(boolean bold);
			[id(89)] void ColSelectionRemoveAll();
			[id(90)] void ColSelectionRemove(long ColNo);
			[id(91)] void ColSelectionAdd(long ColNo);
			[id(92)] void OnVScrollTo(long LineNo);
			[id(93)] long GetColumnCount();
			[id(94)] BSTR GetHeaderText();
			[id(95)] void ShowVertScroller(boolean Show);
			[id(96)] void OnHScrollTo(long ColNo);
			[id(97)] void SetRegExFormatString(BSTR FormatString);
			[id(98)] boolean CheckRegExMatchCell(long LineNo, long ColNo);
			[id(99)] BSTR CheckRegExMatchLine(long LineNo, boolean TrueFalse);
			[id(100)] void SetInplaceEdit(long LineNo, long ColNo, boolean bAssumeValue);
			[id(101)] long GetVScrollPos();
			[id(102)] long GetHScrollPos();
			[id(103)] void Sort(BSTR ColNo, boolean bAscending, boolean bCaseSensitive);
			[id(104)] BSTR SelectDistinct(BSTR Cols, BSTR StrPrefix, BSTR StrAppendix, BSTR StrDelimiter, boolean Distinct);
			[id(105)] long GetLineStatusValue(long LineNo);
			[id(106)] BSTR GetLinesByStatusValue(long Value, short CompareMethod);
			[id(107)] void SetLineStatusValue(long LineNo, long Value);
			[id(108)] void SetMainHeaderValues(BSTR Ranges, BSTR Values, BSTR Colors);
			[id(109)] BSTR GetMainHeaderRanges();
			[id(110)] BSTR GetMainHeaderValues();
			[id(111)] BSTR GetMainHeaderColors();
			[id(112)] BSTR GetNextLineByColumnValue(long ColNo, BSTR Value, short CompareMethod);
			[id(113)] void ResetCellProperty(long LineNo, long ColNo);
			[id(114)] boolean SetCellProperty(long LineNo, long ColNo, BSTR ObjID);
			[id(115)] void CreateCellObj(BSTR ObjID, long BackColor, long TextColor, short TextSize, boolean bItalic, boolean bUnderline, boolean bBold, short CharSet, BSTR FontName);
			[id(116)] BSTR GetLinesByColumnValues(long ColNo, BSTR Values, short CompareMethod);
			[id(117)] void CreateCellBitmpapObj(BSTR ObjID, BSTR BitmapFile);
			[id(118)] boolean SetCellBitmapProperty(long LineNo, long ColNo, BSTR ObjID);
			[id(119)] void ResetCellBitmapProperty(long LineNo, long ColNo);
			[id(120)] BSTR GetBitmapFileName(long LineNo, long ColNo);
			[id(121)] void ResetCellProperties(long LineNo);
			[id(122)] void ResetBitmapProperties(long LineNo);
			[id(123)] BSTR GetLinesByMultipleColumnValue(BSTR Columns, BSTR Values, short CompareMethod);
			[id(124)] boolean SetColumnProperty(long ColNo, BSTR ObjID);
			[id(125)] void ResetColumnProperty(long ColNo);
			[id(126)] boolean SetHeaderFont(short TextSize, boolean bItalic, boolean bUnderline, boolean bBold, short CharSet, BSTR FontName);
			[id(127)] boolean SetHeaderColors(long BackColor, long FontColor);
			[id(128)] boolean SetMainHeaderFont(short TextSize, boolean bItalic, boolean bUnderline, boolean bBold, short CharSet, BSTR FontName);
			[id(129)] void TimerSetValue(long LineNo, short Value);
			[id(130)] void TimerCheck();
			[id(131)] void PrintTabShowDialog(BSTR sDocTitle, BSTR sDate);
			[id(132)] void PrintTab(long lhDC, BSTR sDocTitle, BSTR sDate);
			[id(133)] void PrintSetPageHeaderHeight(long lHeight);
			[id(134)] long PrintGetPageHeaderHeight();
			[id(135)] void PrintSetPageFooterHeight(long lHeight);
			[id(136)] long PrintGetPageFooterHeight();
			[id(137)] void PrintSetDatePosition(BSTR sLocation, BSTR sAlignment);
			[id(138)] void PrintGetDatePosition(BSTR* psLocation, BSTR* psAlignment);
			[id(139)] void PrintSetTitlePosition(BSTR sLocation, BSTR sAlignment);
			[id(140)] void PrintGetTitlePosition(BSTR* psLocation, BSTR* psAlignment);
			[id(141)] void PrintSetPageEnumeration(BSTR sLoaction, BSTR sAlignment, long lStyle);
			[id(142)] void PrintGetPageEnumeration(BSTR* psLocation, BSTR* psAlignment, long* plStyle);
			[id(143)] void PrintSetLogo(BSTR sLocation, BSTR sAlignment, BSTR sPath);
			[id(144)] void PrintGetLogo(BSTR* psLocation, BSTR* psAlignment, BSTR* psPath);
			[id(145)] void PrintSetSeparatorLineTop(long lDistance, long lWidth);
			[id(146)] void PrintGetSeparatorLineTop(long* plDistance, long* plWidth);
			[id(147)] void PrintSetSeparatorLineBottom(long lDistance, long lWidth);
			[id(148)] void PrintGetSeparatorLineBottom(long* plDistance, long* plWidth);
			[id(149)] void PrintSetFitToPage(boolean bFitToPage);
			[id(150)] boolean PrintGetFitToPage();
			[id(151)] void PrintSetColOrderString(BSTR sColString);
			[id(152)] BSTR PrintGetColOrderString();
			[id(153)] void PrintSetMargins(long lTop, long lBottom, long lLeft, long lRight);
			[id(154)] void PrintGetMargins(long* plTop, long* plBottom, long* plLeft, long* plRight);
			[id(155)] boolean PrintSetPageTitleFont(short TextSize, boolean bItalic, boolean bUnderline, boolean bBold, short CharSet, BSTR FontName);
			[id(156)] void PrintRepeatStatus(short sRepeatStatus);
			[id(157)] BSTR GetBuffer(long lFromLine, long lToLine, BSTR LineSeparator);
			[id(158)] boolean CedaAction(BSTR Cmd, BSTR DBTable, BSTR DBFields, BSTR DBData, BSTR DBWhere);
			[id(159)] BSTR GetLastCedaError();
			[id(160)] boolean WriteToFile(BSTR FileName, boolean WithBasicInfo);
			[id(161)] boolean ReadFromFile(BSTR FileName);
			[id(162)] void DateTimeSetColumn(long ColNo);
			[id(163)] void DateTimeResetColumn(long ColNo);
			[id(164)] void DateTimeSetInputFormatString(long ColNo, BSTR FormatString);
			[id(165)] BSTR DateTimeGetInputFormatString(long ColNo);
			[id(166)] void DateTimeSetOutputFormatString(long ColNo, BSTR FormatString);
			[id(167)] BSTR DateTimeGetOutputFormatString(long ColNo);
			[id(168)] void DateTimeSetUTCOffsetMinutes(long ColNo, long OffsetMinutes);
			[id(169)] long DateTimeGetUTCOffsetMinutes(long ColNo);
			[id(170)] void DateTimeSetMonthNames(BSTR MonthNames);
			[id(171)] BSTR DateTimeGetMonthNames();
			[id(172)] boolean WriteToFileWithProperties(BSTR FileName);
			[id(173)] boolean ReadFromFileWithProperties(BSTR FileName);
			[id(174)] void SetLineTag(long LineNo, BSTR Values);
			[id(175)] BSTR GetLineTag(long LineNo);
			[id(176)] BSTR GetLineTagKeyItem(long LineNo, BSTR KeyItem, BSTR StartSeparator, BSTR EndSeparator);
			[id(177)] void AutoSizeColumns();
			[id(178)] boolean SetColumnBoolProperty(long ColNo, BSTR TrueValue, BSTR FalseValue);
			[id(179)] void ResetColumnBoolProperty(long ColNo);
			[id(180)] void CreateDecorationObject(BSTR ID, BSTR LocationList, BSTR PixelList, BSTR ColorList);
			[id(181)] void SetDecorationObject(long LineNo, long ColNo, BSTR ID);
			[id(182)] void ResetCellDecoration(long LineNo, long ColNo);
			[id(183)] void ResetLineDecorations(long LineNo);
			[id(184)] void ComboAddTextLines(BSTR ComboID, BSTR Buffer, BSTR LineSeparator);
			[id(185), helpstring("style: CHOICE -> Not edit field, EDIT -> with edit field")] void ComboSetGridStyle(BSTR ComboID, short PixelWidth, long GridColor);
			[id(186)] void ComboDeleteLine(BSTR ComboID, long LineNo);
			[id(187)] void ComboSetLineValues(BSTR ComboID, long LineNo, BSTR Values);
			[id(188)] void ComboSetResultColumn(BSTR ComboID, long ColNo);
			[id(189)] void SetComboColumn(BSTR ComboID, long ColNo);
			[id(190)] void ComboSetLineColors(BSTR ComboID, long LineNo, long TextColor, long BackColor);
			[id(191)] void ComboSetColumnLengthString(BSTR ComboID, BSTR LenString);
			[id(192)] void CreateComboObject(BSTR ComboID, long TabColumn, long ComboColumns, BSTR Style, long ListboxWidth);
			[id(193)] void ComboResetContent(BSTR ComboID);
			[id(194)] void ComboResetObject(BSTR ComboID);
			[id(195)] boolean CedaFreeCommand(BSTR strCommand);
			[id(196)] void SetBoolPropertyReadOnly(long ColNo, boolean SetResetValue);
			[id(197)] BSTR GetColumnValues(long LineNo, BSTR ColumnList);
			[id(198)] void ComboShow(BSTR ComboID, boolean Show);
			[id(199)] BSTR GetFieldSeparator();
			[id(200)] void ResetConfiguration();
			[id(201)] void IndexCreate(BSTR Name, long ColNo);
			[id(202)] void IndexDestroy(BSTR IndexName);
			[id(203)] BSTR GetLinesByIndexValue(BSTR IndexName, BSTR SearchValue, long CompareMethod);
			[id(204)] long IndexGetDataLineNo(BSTR IndexName, long IndexLine);
			[id(DISPID_REFRESH)] void Refresh();
			[id(205)] BSTR GetFieldValue(long LineNo, BSTR FieldName);
			[id(206)] void SetUniqueFields(BSTR FieldList);
			[id(207)] void SetInternalLineBuffer(boolean blOn);
			[id(208)] long GetNextResultLine();
			[id(209)] void DateTimeSetColumnFormat(long ColNo, BSTR InputFormat, BSTR OutputFormat);
			[id(210)] void SetFieldValues(long LineNo, BSTR FieldList, BSTR ValueList);
			[id(211)] BSTR GetFieldValues(long LineNo, BSTR FieldList);
			[id(212)] BSTR GetBufferByFieldList(long lFromLine, long lToLine, BSTR FieldList, BSTR LineSeparator);
			[id(213)] void ReorgIndexes();
			[id(214)] void CursorDecoration(BSTR LogicalFields, BSTR LocationList, BSTR PixelList, BSTR ColorList);
			//}}AFX_ODL_METHOD

			[id(DISPID_ABOUTBOX)] void AboutBox();
	};

	//  Event dispatch interface for CTABCtrl

	[ uuid(64E8E384-05E2-11D2-9B1D-9B0630BC8F12),
	  helpstring("Event interface for TAB Control") ]
	dispinterface _DTABEvents
	{
		properties:
			//  Event interface has no properties

		methods:
			// NOTE - ClassWizard will maintain event information here.
			//    Use extreme caution when editing this section.
			//{{AFX_ODL_EVENT(CTABCtrl)
			[id(1)] void SendLButtonClick(long LineNo, long ColNo);
			[id(2)] void SendLButtonDblClick(long LineNo, long ColNo);
			[id(3)] void SendRButtonClick(long LineNo, long ColNo);
			[id(4)] void SendMouseMove(long LineNo, long ColNo, BSTR Flags);
			[id(5)] void ColumnSelectionChanged(long ColNo, boolean Selected);
			[id(6)] void OnVScroll(long LineNo);
			[id(7)] void InplaceEditCell(long LineNo, long ColNo, BSTR NewValue, BSTR OldValue);
			[id(8)] void OnHScroll(long ColNo);
			[id(9)] void RowSelectionChanged(long LineNo, boolean Selected);
			[id(10)] void HitKeyOnLine(short Key, long LineNo);
			[id(11)] void TimerExpired(long LineNo, long LineStatus);
			[id(12)] void PrintFinishedTab();
			[id(13)] void ComboSelChanged(long LineNo, long ColNo, BSTR OldValue, BSTR NewValues);
			[id(14)] void EditPositionChanged(long LineNo, long ColNo, BSTR Value);
			[id(15)] void CloseInplaceEdit(long LineNo, long ColNo);
			[id(16)] void BoolPropertyChanged(long LineNo, long ColNo, boolean NewValue);
			[id(17)] void PackageReceived(long lpPackage);
			//}}AFX_ODL_EVENT
	};

	//  Class information for CTABCtrl

	[ uuid(64E8E385-05E2-11D2-9B1D-9B0630BC8F12),
	  helpstring("TAB Control"), control ]
	coclass TAB
	{
		[default] dispinterface _DTAB;
		[default, source] dispinterface _DTABEvents;
	};


	//{{AFX_APPEND_ODL}}
	//}}AFX_APPEND_ODL}}
};
