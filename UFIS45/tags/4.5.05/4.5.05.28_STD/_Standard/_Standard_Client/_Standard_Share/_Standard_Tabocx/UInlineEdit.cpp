// UInlineEdit.cpp : implementation file
//

#include "stdafx.h"
#include "TAB.h"
#include "UInlineEdit.h"
#include "TABCtl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// UInlineEdit

UInlineEdit::UInlineEdit()
{
	bmEscapeKey = FALSE;
}

UInlineEdit::UInlineEdit(CWnd *popParent, long lpLine, long lpCol)
{
	pomParent	= popParent;
	lmLine      = lpLine;
	lmCol		= lpCol;
	bmEscapeKey = FALSE;
}

UInlineEdit::~UInlineEdit()
{
}

BEGIN_MESSAGE_MAP(UInlineEdit, CEdit)
	//{{AFX_MSG_MAP(UInlineEdit)
	ON_WM_KILLFOCUS()
	ON_WM_KEYDOWN()
	ON_WM_SETFOCUS()
	ON_WM_CHAR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// UInlineEdit message handlers

void UInlineEdit::OnKillFocus(CWnd* pNewWnd) 
{
	pomParent->SendMessage(WM_IP_EDIT_KILLFOCUS, 0, 0);

}

BOOL UInlineEdit::PreTranslateMessage(MSG* pMsg) 
{
	char *pcl;
	pcl = omColumnSepa.GetBuffer(0);
	if(pMsg->wParam == VK_ESCAPE)
	{
		PostMessage(WM_CLOSE, 0, 0);
		pomParent->SendMessage(WM_IP_EDIT_ESC, 0, 0);
		return bmEscapeKey = TRUE;
	}	

	return CEdit::PreTranslateMessage(pMsg);
}

void UInlineEdit::PostNcDestroy() 
{
	this->DestroyWindow();
	CEdit::PostNcDestroy();
}

void UInlineEdit::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CEdit::OnKeyDown(nChar, nRepCnt, nFlags);
}

void UInlineEdit::OnSetFocus(CWnd* pOldWnd) 
{
	CEdit::OnSetFocus(pOldWnd);

	int ilLen;
	CString olText;
	GetWindowText(olText);
	ilLen = olText.GetLength();
	SetSel (0, ilLen);
}

void UInlineEdit::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	char *pcl = omColumnSepa.GetBuffer(0);
	
	if (nChar != (unsigned	)*pcl)
	{
		CEdit::OnChar(nChar, nRepCnt, nFlags);
	}
}
