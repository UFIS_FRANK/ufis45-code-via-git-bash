#if !defined(AFX_UINLINEEDIT_H__F3778D01_6F68_11D4_A2EE_00500437F607__INCLUDED_)
#define AFX_UINLINEEDIT_H__F3778D01_6F68_11D4_A2EE_00500437F607__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UInlineEdit.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// UInlineEdit window
#define WM_EDIT_MOVE_IP_LEFT		(WM_USER + 1001)
#define WM_EDIT_MOVE_IP_RIGHT		(WM_USER + 1002)
#define WM_EDIT_MOVE_IP_UP			(WM_USER + 1003)
#define WM_EDIT_MOVE_IP_DOWN		(WM_USER + 1004)
#define WM_EDIT_IP_END				(WM_USER + 1005)
#define WM_EDIT_KEYPRESSED			(WM_USER + 1006)
#define EDITCLASSMSG				(WM_USER + 1007)
#define WM_IP_EDIT_KILLFOCUS		(WM_USER + 1008)
#define WM_IP_EDIT_ESC				(WM_USER + 1009)



class UInlineEdit : public CEdit
{
// Construction
public:
	UInlineEdit();
	UInlineEdit(CWnd *popParent, long lpLine, long lmCol);

	CString omColumnSepa;
	long lmLine, lmCol;
	CWnd *pomParent;
// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(UInlineEdit)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
public:
	BOOL bmEscapeKey;
	virtual ~UInlineEdit();

	// Generated message map functions
protected:
	//{{AFX_MSG(UInlineEdit)
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UINLINEEDIT_H__F3778D01_6F68_11D4_A2EE_00500437F607__INCLUDED_)
