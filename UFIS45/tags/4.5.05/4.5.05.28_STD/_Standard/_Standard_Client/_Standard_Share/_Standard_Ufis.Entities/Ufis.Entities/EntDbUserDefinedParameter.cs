﻿using System;

namespace Ufis.Entities
{
    [EntityAttribute(SerializedName="PARTAB")]
    public class EntDbUserDefinedParameter : BaseEntity
    {
        private string _applicationName;
        private string _description;
        private string _identifier;
        private string _type;
        private string _telexIdentifier;
        private string _dataTypeIdentifier;
        private string _value;

        [EntityAttribute(SerializedName = "APPL", MaxLength=8)]
        public string ApplicationName
        {
            get { return _applicationName; }
            set
            {
                if (_applicationName != value)
                {
                    _applicationName = value;
                    OnPropertyChanged("ApplicationName");
                }
            }
        }

        [EntityAttribute(SerializedName = "NAME", MaxLength = 100)]
        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (_description != value)
                {
                    _description = value;
                    OnPropertyChanged("Description");
                }
            }
        }

        [EntityAttribute(SerializedName = "PAID", MaxLength = 16)]
        public string Identifier
        {
            get { return _identifier; }
            set
            {
                if (_identifier != value)
                {
                    _identifier = value;
                    OnPropertyChanged("Identifier");
                }
            }
        }

        [EntityAttribute(SerializedName = "PTYP", MaxLength = 16)]
        public string Type
        {
            get { return _type; }
            set
            {
                if (_type != value)
                {
                    _type = value;
                    OnPropertyChanged("Type");
                }
            }
        }

        [EntityAttribute(SerializedName = "TXID", MaxLength = 32)]
        public string TelexIdentifier
        {
            get { return _telexIdentifier; }
            set
            {
                if (_telexIdentifier != value)
                {
                    _telexIdentifier = value;
                    OnPropertyChanged("TelexIdentifier");
                }
            }
        }

        [EntityAttribute(SerializedName = "TYPE", MaxLength = 4)]
        public string DataTypeIdentifier
        {
            get { return _dataTypeIdentifier; }
            set
            {
                if (_dataTypeIdentifier != value)
                {
                    _dataTypeIdentifier = value;
                    OnPropertyChanged("DataTypeIdentifier");
                }
            }
        }

        [EntityAttribute(SerializedName = "VALU", MaxLength = 64)]
        public string Value
        {
            get { return _value; }
            set
            {
                if (_value != value)
                {
                    _value = value;
                    OnPropertyChanged("Value");
                }
            }
        }
    }
}
