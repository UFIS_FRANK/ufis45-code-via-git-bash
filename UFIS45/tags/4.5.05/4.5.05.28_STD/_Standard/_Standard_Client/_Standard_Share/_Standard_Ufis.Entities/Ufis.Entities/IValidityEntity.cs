﻿using System;

namespace Ufis.Entities
{
    public interface IValidityEntity
    {
        DateTime? ValidFrom { get; set; }
        DateTime? ValidTo { get; set; }
    }
}
