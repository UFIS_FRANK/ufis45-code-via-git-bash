﻿using System;

namespace Ufis.Entities
{
    [Entity(SerializedName = "FIMTAB")]
    public class EntDbFlightIdMap : ValidityBaseEntity
    {
        private string _dataSourceName;
        private string _arrivalDepartureId;
        private string _interfaceCallSign;
        private string _callSign;

        [Entity(SerializedName = "DSSN", MaxLength = 3, IsMandatory = true)]
        public string DataSourceName
        {
            get { return _dataSourceName; }
            set
            {
                if (_dataSourceName != value)
                {
                    _dataSourceName = value;
                    OnPropertyChanged("DataSourceName");
                }
            }
        }

        [Entity(SerializedName = "ADID", MaxLength = 1)]
        public string ArrivalDepartureId
        {
            get { return _arrivalDepartureId; }
            set
            {
                if (_arrivalDepartureId != value)
                {
                    _arrivalDepartureId = value;
                    OnPropertyChanged("ArrivalDepartureId");
                }
            }
        }

        [Entity(SerializedName = "ICSG", MaxLength = 8, IsMandatory = true)]
        public string InterfaceCallSign
        {
            get { return _interfaceCallSign; }
            set
            {
                if (_interfaceCallSign != value)
                {
                    _interfaceCallSign = value;
                    OnPropertyChanged("InterfaceCallSign");
                }
            }
        }

        [Entity(SerializedName = "CSGN", MaxLength = 8, IsMandatory = true)]
        public string CallSign
        {
            get { return _callSign; }
            set
            {
                if (_callSign != value)
                {
                    _callSign = value;
                    OnPropertyChanged("CallSign");
                }
            }
        }
    }
}
