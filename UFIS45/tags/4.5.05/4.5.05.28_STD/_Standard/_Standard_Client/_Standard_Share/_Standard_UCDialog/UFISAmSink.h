#if !defined(AFX_UFISAMSINK_H__32F920CF_8306_11D4_9046_00010204AA51__INCLUDED_)
#define AFX_UFISAMSINK_H__32F920CF_8306_11D4_9046_00010204AA51__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 *	UFISTestClnt:	UFIS Application Manager Project
 *
 *	Example Application for C++ Dialog based clients
 *	
 *	ABB Airport Technologies GmbH 2000
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *		Author			Date			Comment
 *	------------------------------------------------------------------
 *
 *		cla AAT/IR		04/09/2000		Initial version
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
// UFISAmSink.h : header file
//

/*
 * Import Interface
 */ 
#import "c:\ufis\system\UFISAppMng.tlb" no_namespace named_guids

#include "ConnectionAdvisor.h"
class CUCDialogDlg;

/*----------------------------------------------------------------------------*/

const IID IID_IUFISAmEventSink = __uuidof(_IUFISAmEvents);

/*----------------------------------------------------------------------------*/

/////////////////////////////////////////////////////////////////////////////
// CUFISAmSink command target

class CUFISAmSink : public CCmdTarget
{
	DECLARE_DYNCREATE(CUFISAmSink)

	CUFISAmSink();           // protected constructor used by dynamic creation
	virtual ~CUFISAmSink();

	BOOL Advise(IUnknown* pSource, REFIID iid);
	BOOL Unadvise(REFIID iid);
	void SetLauncher(CUCDialogDlg* pUFISAmauncher);

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUFISAmSink)
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CUFISAmSink)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CUFISAmSink)
	afx_msg void OnForwardData(long,LPCSTR);
	afx_msg void OnSetAppTag(long*);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
private:
	CConnectionAdvisor m_UFISAmEventsAdvisor;
	CUCDialogDlg* m_pUFISAmLauncher;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UFISAMSINK_H__32F920CF_8306_11D4_9046_00010204AA51__INCLUDED_)
