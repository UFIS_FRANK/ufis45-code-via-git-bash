// UtilUC.cpp: implementation of the UtilUC class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UCDialog.h"
#include "UtilUC.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

UtilUC::UtilUC()
{

}

UtilUC::~UtilUC()
{

}

bool UtilUC::ConvByteArrToHexSt( BYTE* pbpByteArr, short len, char* pspSt )
{
	bool isOk = true;
	//pspSt = new char[len*2];
	pspSt[0] = '\0';
	for (int i=0; i<len; i++)
	{
		char* tmpSt= new char[3];
		isOk = ConvByteToHexSt( pbpByteArr[i] , tmpSt );
		if (isOk)
		{
			pspSt = strcat( pspSt, tmpSt);
		}
		else
		{
			break;
		}
	}
	return isOk;
}


bool UtilUC::ConvHexStToByteArr( char* pspSt, BYTE* pbpByteArr, short& byteCnt )
{
	bool isOk = true;
	int len = 0;
	len = (strlen( pspSt )/sizeof(char));
	if ((len%2)!=0) len--;
	if (len<0) len = 0;
	byteCnt = 0;
	for (int i=0; i<len; i= i+2)
	{
		char tmpSt[2];
		tmpSt[0] = pspSt[i];
		tmpSt[1] = pspSt[i+1];
		tmpSt[2] = '\0';
		BYTE num = 0;
		isOk = ConvHexStToByte( tmpSt, num );
		if (isOk)
		{
			pbpByteArr[byteCnt++] = num;
		}
		else
		{
			break;
		}
	}
	if (!isOk) byteCnt = 0;
	return isOk;
}



bool UtilUC::ConvByteToHexSt( BYTE bpByte, char* pcpSt )
{
	bool isOk = true;
	char lowCh,hiCh;

	isOk = ConvIntToHexChar( ((int) (bpByte & 0x0F)), lowCh );//Lower 4 bits
	isOk = ConvIntToHexChar( ((int) (bpByte & 0xF0) >> 4), hiCh );//Higher 4 bits
	if (isOk)
	{
		char ch1 = hiCh;
		pcpSt[0] = hiCh;
		pcpSt[1] = lowCh;
		pcpSt[2] = '\0';
	}else{
		pcpSt[0] = '\0';
	}
	return isOk;
}

bool UtilUC::ConvHexStToByte( char* st, BYTE& bpByte )
{//Convert 2 Hex Digits in String to Byte
	bpByte = 0;
	bool isOk = true;

	int lowNum,hiNum;
    if (strlen(st)!=2) isOk = false; //expecting 2 chars only

	if (isOk)
	{
		isOk = ConvHexCharToInt(st[0], hiNum);//convert higher 4 bit
		isOk = ConvHexCharToInt(st[1], lowNum); //Convert lower 4 bit
		if (isOk)
		{
			bpByte = (hiNum << 4 ) + lowNum;
		}
	}
	return isOk;
}

bool UtilUC::ConvHexCharToInt( char cpCh, int& num )
{
	bool isOk = true;
	num = 0;
    switch( cpCh )
	{
		case '0' : num = 0;	break;
		case '1' : num = 1; break;
		case '2' : num = 2; break;
		case '3' : num = 3; break;
		case '4' : num = 4; break;
		case '5' : num = 5; break;
		case '6' : num = 6; break;
		case '7' : num = 7; break;
		case '8' : num = 8; break;
		case '9' : num = 9; break;
		case 'A' : num = 10; break;
		case 'a' : num = 10; break;
		case 'B' : num = 11; break;
		case 'b' : num = 11; break;
		case 'C' : num = 12; break;
		case 'c' : num = 12; break;
		case 'D' : num = 13; break;
		case 'd' : num = 13; break;
		case 'E' : num = 14; break;
		case 'e' : num = 14; break;
		case 'F' : num = 15; break;
		case 'f' : num = 15; break;
		default  : isOk = false;
	}
	return isOk;
}


bool UtilUC::ConvIntToHexChar( int num, char& ch )
{
	bool isOk = true;
    switch( num )
	{
		case 0  : ch = '0';	break;
		case 1  : ch = '1'; break;
		case 2  : ch = '2'; break;
		case 3  : ch = '3'; break;
		case 4  : ch = '4'; break;
		case 5  : ch = '5'; break;
		case 6  : ch = '6'; break;
		case 7  : ch = '7'; break;
		case 8  : ch = '8'; break;
		case 9  : ch = '9'; break;
		case 10 : ch = 'A'; break;
		case 11 : ch = 'B'; break;
		case 12 : ch = 'C'; break;
		case 13 : ch = 'D'; break;
		case 14 : ch = 'E'; break;
		case 15 : ch = 'F'; break;
		default : isOk = false;
	}
	return isOk;
}


bool UtilUC::ConvOctCharToInt( char cpCh , int& num)
{
	bool isOk = true;
	num = 0;
    switch( cpCh )
	{
		case '0' : num = 0;	break;
		case '1' : num = 1; break;
		case '2' : num = 2; break;
		case '3' : num = 3; break;
		case '4' : num = 4; break;
		case '5' : num = 5; break;
		case '6' : num = 6; break;
		case '7' : num = 7; break;
		default  : isOk = false;
	}
	return isOk;
}

bool UtilUC::ConvIntToOctChar( int num, char& ch )
{
	bool isOk = true;
    switch( num )
	{
		case 0  : ch = '0';	break;
		case 1  : ch = '1'; break;
		case 2  : ch = '2'; break;
		case 3  : ch = '3'; break;
		case 4  : ch = '4'; break;
		case 5  : ch = '5'; break;
		case 6  : ch = '6'; break;
		case 7  : ch = '7'; break;
		default : isOk = false;
	}
	return isOk;
}



bool UtilUC::ConvByteArrToOctSt( BYTE* pbpByteArr, short len, char* pspSt )
{
	bool isOk = true;
	pspSt[0] = '\0';
	for (int i=0; i<len; i++)
	{
		char* tmpSt= new char[3];
		isOk = ConvByteToOctSt( pbpByteArr[i] , tmpSt );
		if (isOk)
		{
			pspSt = strcat( pspSt, tmpSt);
		}
		else
		{
			break;
		}
	}
	return isOk;
}


bool UtilUC::ConvOctStToByteArr( char* pspSt, BYTE* pbpByteArr, short& byteCnt )
{
	bool isOk = true;
	int len = 0;
	len = (strlen( pspSt )/sizeof(char));
	int res = len%3;
	len = len - res;
	if (len<0) len = 0;
	byteCnt = 0;
	for (int i=0; i<len; i= i+3)
	{
		char tmpSt[3];
		tmpSt[0] = pspSt[i];
		tmpSt[1] = pspSt[i+1];
		tmpSt[2] = pspSt[i+2];
		tmpSt[3] = '\0';
		BYTE num = 0;
		isOk = ConvOctStToByte( tmpSt, num );
		if (isOk)
		{
			pbpByteArr[byteCnt++] = num;
		}
		else
		{
			break;
		}
	}
	if (!isOk) byteCnt = 0;
	return isOk;
}



bool UtilUC::ConvByteToOctSt( BYTE bpByte, char* pcpSt )
{
	bool isOk = true;
	char lowCh,midCh,hiCh;

	isOk = ConvIntToOctChar( ((int) (bpByte & 07)), lowCh );//Lower 3 bits
	isOk = ConvIntToOctChar( ((int) ((bpByte>>3) & 07)), midCh );//Middle 3 bits
	isOk = ConvIntToOctChar( ((int) ((bpByte>>6) & 07 )), hiCh );//Higher 2 bits
	if (isOk)
	{
		pcpSt[0] = hiCh;
		pcpSt[1] = midCh;
		pcpSt[2] = lowCh;
		pcpSt[3] = '\0';
	}else{
		pcpSt[0] = '\0';
	}
	return isOk;
}

bool UtilUC::ConvOctStToByte( char* st, BYTE& bpByte )
{//Convert 3 Oct Digits in String to Byte
	bpByte = 0;
	bool isOk = true;

	int lowNum,midNum,hiNum;
    if (strlen(st)!=3) isOk = false; //expecting 3 chars only

	if (isOk)
	{
		isOk = ConvOctCharToInt(st[0], hiNum);//convert higher 2 bits
		isOk = ConvOctCharToInt(st[1], midNum); //Convert middle 3 bits
		isOk = ConvOctCharToInt(st[2], lowNum); //Convert lower 3 bits
		if (isOk)
		{
			bpByte = (hiNum << 6 ) + (midNum<<3) + lowNum;
		}
	}
	return isOk;
}


