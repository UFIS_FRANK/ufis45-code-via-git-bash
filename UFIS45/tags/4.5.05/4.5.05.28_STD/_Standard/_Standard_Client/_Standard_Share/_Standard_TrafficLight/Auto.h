#if !defined(AFX_AUTO_H__6B582D92_9241_11D5_9969_0000865098D4__INCLUDED_)
#define AFX_AUTO_H__6B582D92_9241_11D5_9969_0000865098D4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Auto.h : header file
//



/////////////////////////////////////////////////////////////////////////////
// Auto command target

class Auto : public CCmdTarget
{
	DECLARE_DYNCREATE(Auto)

	Auto();           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Auto)
	public:
	virtual void OnFinalRelease();
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~Auto();

	// Generated message map functions
	//{{AFX_MSG(Auto)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(Auto)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AUTO_H__6B582D92_9241_11D5_9969_0000865098D4__INCLUDED_)
