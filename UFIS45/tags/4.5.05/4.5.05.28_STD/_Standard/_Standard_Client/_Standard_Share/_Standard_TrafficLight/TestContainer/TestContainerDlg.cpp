// lb_containerDlg.cpp : implementation file
//

#include <stdafx.h>

#include <TestContainer.h>
#include <TestContainerDlg.h>
#include <DlgProxy.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define IDC_AMPTESTCTRL1 815 // ID of traffic light control in Dlg

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLb_containerDlg dialog

IMPLEMENT_DYNAMIC(CLb_containerDlg, CDialog);

CLb_containerDlg::CLb_containerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLb_containerDlg::IDD, pParent)
{
	
	AfxEnableControlContainer ();
	//{{AFX_DATA_INIT(CLb_containerDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_pAutoProxy = NULL;
	m_pAmpel = NULL;
	pDispUfisCom = NULL;
	pDispBcPrxyEvents = NULL;
	m_iMaxTime = 0;
	something_wrong = false;
	m_hWnd = GetSafeHwnd();
}

CLb_containerDlg::~CLb_containerDlg()
{
	// If there is an automation proxy for this dialog, set
	//  its back pointer to this dialog to NULL, so it knows
	//  the dialog has been deleted.
	if (m_pAutoProxy != NULL)
		m_pAutoProxy->m_pDialog = NULL;
	

}

void CLb_containerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLb_containerDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CLb_containerDlg, CDialog)
	//{{AFX_MSG_MAP(CLb_containerDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_TLC, OnTlc)
	ON_BN_CLICKED(IDC_PING, OnPing)
	ON_BN_CLICKED(IDC_RED, OnRed)
	ON_BN_CLICKED(IDC_RELEASE, OnRelease)
	ON_BN_CLICKED(IDC_YELLOW, OnYellow)
	ON_BN_CLICKED(IDC_GREEN, OnGreen)
	ON_BN_CLICKED(IDC_PROP, OnProp)
	ON_BN_CLICKED(IDC_ABOUT, OnAbout)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLb_containerDlg message handlers

BOOL CLb_containerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	
	//--------------------------------------------------------------------------------
	// instanciate BcProxy
	
	if (!StartBc())
	{
		MessageBox(_T("couldn�t create BcProxy !"));
		something_wrong = true;
	}
	// Start database connection
	if (!StartUfisCom())
	{
		MessageBox(_T("couldn�t connect to database !"));
		something_wrong = true;
	}

	
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CLb_containerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CLb_containerDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CLb_containerDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

// Automation servers should not exit when a user closes the UI
//  if a controller still holds on to one of its objects.  These
//  message handlers make sure that if the proxy is still in use,
//  then the UI is hidden but the dialog remains around if it
//  is dismissed.

void CLb_containerDlg::OnClose() 
{
	if (CanExit())
		CDialog::OnClose();
}

void CLb_containerDlg::OnOK() 
{
	if (CanExit())
		CDialog::OnOK();
}

void CLb_containerDlg::OnCancel() 
{
	if (CanExit())
		CDialog::OnCancel();
}

BOOL CLb_containerDlg::CanExit()
{
	// If the proxy object is still around, then the automation
	//  controller is still holding on to this application.  Leave
	//  the dialog around, but hide its UI.
	// release traffic light control
	if (m_pAmpel != NULL)
	{
		m_pAmpel->Detach ();
	}
	if (m_pAutoProxy != NULL)
	{
		ShowWindow(SW_HIDE);
		return FALSE;
	}
	if (omBcPrxyEvents)
	{
		omBcPrxyEvents.ReleaseDispatch ();
		delete omBcPrxyEvents;
	}
	if (pUfisCom != NULL)
	{
		pUfisCom->Detach ();
		pUfisCom = NULL;
	}
	return TRUE;
}

bool CLb_containerDlg::StartBc()
{
	CLSID clsid;
	HRESULT res;

	res = CLSIDFromProgID(L"BcProxy.BcPrxy.1",&clsid);
	if (res != S_OK)
	{
		MessageBox (_T("couldn�t get clsid of BcProxy !"));
		return false;
	}
	if (!omBcPrxyEvents.CreateDispatch (clsid))
	{
		MessageBox (_T("unable to create BcPrxy instance !"));
		return false;
	}
	pDispBcPrxyEvents = omBcPrxyEvents.m_lpDispatch ;
	// ^^^^^ pointer to Disp. Interface of BcPrxyEvents
	

	return true;
}

bool CLb_containerDlg::StartUfisCom()
{
	CString strHopo,strTableExt,strServer,strConnectType;
	int res;
	res = 0;
	strHopo = "SIN";
	strServer = "SIN1";
	strTableExt = "TAB";
	strConnectType = "CEDA";

	pUfisCom = NULL;
	pUfisCom = new CUfisCom();
	pUfisCom->Create (NULL,WS_CHILD,CRect(0,0,0,0),this,1430,NULL,FALSE);
	pUfisCom->EnableAutomation ();
	IUnknown* lpIUnknown = pUfisCom->GetControlUnknown ();
	lpIUnknown->QueryInterface (IID_IDispatch,(void**)&pDispUfisCom);
	if (pUfisCom == NULL)
	{
		MessageBox(_T("no UfisCom created !"));
	}

	
	pUfisCom->SetCedaPerameters ("TLC",strHopo,strTableExt);
	res = pUfisCom->InitCom (strServer,strConnectType);
	if (res == 0)
	{
		MessageBox(_T("Con : Connection to CEDA failed !"));
	}
	return true;
}

void CLb_containerDlg::OnTlc() 
{
	if (!something_wrong)
	{
		// CLSID of Ampel Control
		CLSID clsid = {0xA2F31E92, 0xC74F, 0x11D3, {0xa2, 0x51, 0x0, 0x50, 0x04, 0x37, 0xF6, 0x07}};
		long res;
		m_pAmpel = new CAmp_test();
		//m_pAmpel->CreateControl (clsid,NULL,WS_CHILD,CRect (0,0,32,97),this,815,NULL,FALSE,NULL);
		m_pAmpel->Create(NULL,WS_CHILD|WS_VISIBLE,CRect (10,10,42,107),this,IDC_AMPTESTCTRL1,NULL,FALSE);

		if (m_pAmpel == NULL)
		{
			MessageBox (_T("Con : Failed to instanciate TLC-Control !"));
		}
		m_pAmpel->SetDispBc (pDispBcPrxyEvents);
		m_pAmpel->SetDispatch (pDispUfisCom);
		res = m_pAmpel->Init ();
	}
	else // error while creating BcPrxy or UfisCom
	{
		MessageBox(_T("can�t start traffic light control !"));
	}

}

BEGIN_EVENTSINK_MAP(CLb_containerDlg, CDialog)
    //{{AFX_EVENTSINK_MAP(CLb_containerDlg)
	 ON_EVENT(CLb_containerDlg, IDC_AMPTESTCTRL1, 1 /* time */, OntimeAmp, VTS_I4)
	//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()

void CLb_containerDlg::OntimeAmp(long ltime) 
{
	SetDlgItemInt(IDC_TIME,ltime);
	if (ltime > m_iMaxTime)
	{
		m_iMaxTime = ltime;
		SetDlgItemInt(IDC_MAX_TIME,m_iMaxTime);
	}
	UpdateData(false); // show delaytime
}

void CLb_containerDlg::OnPing() 
{
	//pUfisCom->CleanupCom ();
	//pUfisCom->SetCedaPerameters ("Dlg","ZRH","TAB");
	//int ret = pUfisCom->InitCom ("nizza","CEDA");
	//if (ret == 0)
	//{
	//	MessageBox(_T("connection failed !"));
	//}
	int ret = pUfisCom->CallServer("SBC","SBCTAB","", "","", "360");
	if (ret != 0)
	{
		MessageBox(_T("Call to server failed !"));
	}

	
	
}

void CLb_containerDlg::OnRed() 
{
	m_pAmpel->SetColor(3);
	
}

void CLb_containerDlg::OnRelease() 
{
	m_pAmpel->SetColor(0);
	
}

void CLb_containerDlg::OnYellow() 
{
	m_pAmpel->SetColor(2);	
}

void CLb_containerDlg::OnGreen() 
{
	m_pAmpel->SetColor(1);	
}

void CLb_containerDlg::OnProp() 
{
	LPUNKNOWN pIUnknown = NULL;
	pIUnknown = m_pAmpel->GetControlUnknown();
	HRESULT res;
	LPOLEOBJECT pIOleObject = NULL;
	res = pIUnknown->QueryInterface(IID_IOleObject,(void**)&pIOleObject);
	if (res != S_OK)
	{
		MessageBox(_T("no connection to IOleObject"));
	}
	else
	{
		IOleClientSite *pActiveSite;
		pIOleObject->GetClientSite (&pActiveSite);
		pIOleObject->DoVerb (OLEIVERB_PROPERTIES,NULL,pActiveSite,0,m_hWnd,CRect(0,0,5,5));
		// the CRect seems to have no effect for the Properties OLEVERB      ^^^^^^^^^^^
		pActiveSite->Release ();
		pActiveSite = NULL;
		pIOleObject->Release ();
		pIOleObject = NULL;
	}
	
	
}

void CLb_containerDlg::OnAbout() 
{
	if (m_pAmpel != NULL)
	{
		m_pAmpel->AboutBox();
	}
}
