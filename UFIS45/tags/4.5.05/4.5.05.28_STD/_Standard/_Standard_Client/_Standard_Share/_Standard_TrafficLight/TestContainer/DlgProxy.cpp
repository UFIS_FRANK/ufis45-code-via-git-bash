// DlgProxy.cpp : implementation file
//

#include <stdafx.h>
#include <TestContainer.h>
#include <DlgProxy.h>
#include <TestContainerDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLb_containerDlgAutoProxy

IMPLEMENT_DYNCREATE(CLb_containerDlgAutoProxy, CCmdTarget)

CLb_containerDlgAutoProxy::CLb_containerDlgAutoProxy()
{
	EnableAutomation();
	
	// To keep the application running as long as an automation 
	//	object is active, the constructor calls AfxOleLockApp.
	AfxOleLockApp();

	// Get access to the dialog through the application's
	//  main window pointer.  Set the proxy's internal pointer
	//  to point to the dialog, and set the dialog's back pointer to
	//  this proxy.
	ASSERT (AfxGetApp()->m_pMainWnd != NULL);
	ASSERT_VALID (AfxGetApp()->m_pMainWnd);
	ASSERT_KINDOF(CLb_containerDlg, AfxGetApp()->m_pMainWnd);
	m_pDialog = (CLb_containerDlg*) AfxGetApp()->m_pMainWnd;
	m_pDialog->m_pAutoProxy = this;
}

CLb_containerDlgAutoProxy::~CLb_containerDlgAutoProxy()
{
	// To terminate the application when all objects created with
	// 	with automation, the destructor calls AfxOleUnlockApp.
	//  Among other things, this will destroy the main dialog
	if (m_pDialog != NULL)
		m_pDialog->m_pAutoProxy = NULL;
	AfxOleUnlockApp();
}

void CLb_containerDlgAutoProxy::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CCmdTarget::OnFinalRelease();
}

BEGIN_MESSAGE_MAP(CLb_containerDlgAutoProxy, CCmdTarget)
	//{{AFX_MSG_MAP(CLb_containerDlgAutoProxy)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CLb_containerDlgAutoProxy, CCmdTarget)
	//{{AFX_DISPATCH_MAP(CLb_containerDlgAutoProxy)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_ILb_container to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {8942EEDB-9796-11D5-9970-0000865098D4}
static const IID IID_ILb_container =
{ 0x8942eedb, 0x9796, 0x11d5, { 0x99, 0x70, 0x0, 0x0, 0x86, 0x50, 0x98, 0xd4 } };

BEGIN_INTERFACE_MAP(CLb_containerDlgAutoProxy, CCmdTarget)
	INTERFACE_PART(CLb_containerDlgAutoProxy, IID_ILb_container, Dispatch)
END_INTERFACE_MAP()

// The IMPLEMENT_OLECREATE2 macro is defined in StdAfx.h of this project
// {8942EED9-9796-11D5-9970-0000865098D4}
IMPLEMENT_OLECREATE2(CLb_containerDlgAutoProxy, "Lb_container.Application", 0x8942eed9, 0x9796, 0x11d5, 0x99, 0x70, 0x0, 0x0, 0x86, 0x50, 0x98, 0xd4)

/////////////////////////////////////////////////////////////////////////////
// CLb_containerDlgAutoProxy message handlers
