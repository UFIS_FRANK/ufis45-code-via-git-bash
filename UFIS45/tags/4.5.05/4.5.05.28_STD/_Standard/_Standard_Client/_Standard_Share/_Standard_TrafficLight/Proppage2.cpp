// Proppage2.cpp : implementation file
//

#include <stdafx.h>
#include <TrafficLight.h>
#include <Proppage2.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CProppage2 dialog

IMPLEMENT_DYNCREATE(CProppage2, COlePropertyPage)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CProppage2, COlePropertyPage)
	//{{AFX_MSG_MAP(CProppage2)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

// {AFBD4FA5-AC03-11D5-BFF8-0004769155FB}
IMPLEMENT_OLECREATE_EX(CProppage2, "amp_test.CProppage2",
	0xafbd4fa5, 0xac03, 0x11d5, 0xbf, 0xf8, 0x0, 0x4, 0x76, 0x91, 0x55, 0xfb)


/////////////////////////////////////////////////////////////////////////////
// CProppage2::CProppage2Factory::UpdateRegistry -
// Adds or removes system registry entries for CProppage2

BOOL CProppage2::CProppage2Factory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Define string resource for page type; replace '0' below with ID.

	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_AMP_PPG_GRAPHICS);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CProppage2::CProppage2 - Constructor

// TODO: Define string resource for page caption; replace '0' below with ID.

CProppage2::CProppage2() :
	COlePropertyPage(IDD, IDS_AMP_PPG_GRAPHICS)
{
	//{{AFX_DATA_INIT(CProppage2)
	m_iRadio1 = -1;
	//}}AFX_DATA_INIT
}


/////////////////////////////////////////////////////////////////////////////
// CProppage2::DoDataExchange - Moves data between page and properties

void CProppage2::DoDataExchange(CDataExchange* pDX)
{
	// NOTE: ClassWizard will add DDP, DDX, and DDV calls here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//{{AFX_DATA_MAP(CProppage2)
	DDP_Radio(pDX, IDC_RADIO1, m_iRadio1, _T("Radio") );
	DDX_Radio(pDX, IDC_RADIO1, m_iRadio1);
	//}}AFX_DATA_MAP
	DDP_PostProcessing(pDX);
}


/////////////////////////////////////////////////////////////////////////////
// CProppage2 message handlers
