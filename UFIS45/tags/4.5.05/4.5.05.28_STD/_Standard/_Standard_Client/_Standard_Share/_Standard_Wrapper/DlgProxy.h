// DlgProxy.h : header file
//

#if !defined(AFX_DLGPROXY_H__701253C1_8BE8_43CA_9A3E_807FE9DCFCAE__INCLUDED_)
#define AFX_DLGPROXY_H__701253C1_8BE8_43CA_9A3E_807FE9DCFCAE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class C_Standard_WrapperDlg;

/////////////////////////////////////////////////////////////////////////////
// C_Standard_WrapperDlgAutoProxy command target

class C_Standard_WrapperDlgAutoProxy : public CCmdTarget
{
	DECLARE_DYNCREATE(C_Standard_WrapperDlgAutoProxy)

	C_Standard_WrapperDlgAutoProxy();           // protected constructor used by dynamic creation

// Attributes
public:
	C_Standard_WrapperDlg* m_pDialog;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(C_Standard_WrapperDlgAutoProxy)
	public:
	virtual void OnFinalRelease();
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~C_Standard_WrapperDlgAutoProxy();

	// Generated message map functions
	//{{AFX_MSG(C_Standard_WrapperDlgAutoProxy)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
	DECLARE_OLECREATE(C_Standard_WrapperDlgAutoProxy)

	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(C_Standard_WrapperDlgAutoProxy)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPROXY_H__701253C1_8BE8_43CA_9A3E_807FE9DCFCAE__INCLUDED_)
