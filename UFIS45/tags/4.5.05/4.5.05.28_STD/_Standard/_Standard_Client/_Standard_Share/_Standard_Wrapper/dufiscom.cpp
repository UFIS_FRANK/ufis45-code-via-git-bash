// Machine generated IDispatch wrapper class(es) created with ClassWizard

#include "stdafx.h"
#include "dufiscom.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// _DUfisCom properties

CString _DUfisCom::GetServerName()
{
	CString result;
	GetProperty(0x1, VT_BSTR, (void*)&result);
	return result;
}

void _DUfisCom::SetServerName(LPCTSTR propVal)
{
	SetProperty(0x1, VT_BSTR, propVal);
}

CString _DUfisCom::GetAccessMethod()
{
	CString result;
	GetProperty(0x2, VT_BSTR, (void*)&result);
	return result;
}

void _DUfisCom::SetAccessMethod(LPCTSTR propVal)
{
	SetProperty(0x2, VT_BSTR, propVal);
}

CString _DUfisCom::GetWorkStation()
{
	CString result;
	GetProperty(0x3, VT_BSTR, (void*)&result);
	return result;
}

void _DUfisCom::SetWorkStation(LPCTSTR propVal)
{
	SetProperty(0x3, VT_BSTR, propVal);
}

CString _DUfisCom::GetUserName()
{
	CString result;
	GetProperty(0x4, VT_BSTR, (void*)&result);
	return result;
}

void _DUfisCom::SetUserName(LPCTSTR propVal)
{
	SetProperty(0x4, VT_BSTR, propVal);
}

CString _DUfisCom::GetTableExt()
{
	CString result;
	GetProperty(0x5, VT_BSTR, (void*)&result);
	return result;
}

void _DUfisCom::SetTableExt(LPCTSTR propVal)
{
	SetProperty(0x5, VT_BSTR, propVal);
}

CString _DUfisCom::GetHomeAirport()
{
	CString result;
	GetProperty(0x6, VT_BSTR, (void*)&result);
	return result;
}

void _DUfisCom::SetHomeAirport(LPCTSTR propVal)
{
	SetProperty(0x6, VT_BSTR, propVal);
}

CString _DUfisCom::GetXUser()
{
	CString result;
	GetProperty(0x16, VT_BSTR, (void*)&result);
	return result;
}

void _DUfisCom::SetXUser(LPCTSTR propVal)
{
	SetProperty(0x16, VT_BSTR, propVal);
}

CString _DUfisCom::GetReqid()
{
	CString result;
	GetProperty(0x7, VT_BSTR, (void*)&result);
	return result;
}

void _DUfisCom::SetReqid(LPCTSTR propVal)
{
	SetProperty(0x7, VT_BSTR, propVal);
}

CString _DUfisCom::GetDest1()
{
	CString result;
	GetProperty(0x8, VT_BSTR, (void*)&result);
	return result;
}

void _DUfisCom::SetDest1(LPCTSTR propVal)
{
	SetProperty(0x8, VT_BSTR, propVal);
}

CString _DUfisCom::GetDest2()
{
	CString result;
	GetProperty(0x9, VT_BSTR, (void*)&result);
	return result;
}

void _DUfisCom::SetDest2(LPCTSTR propVal)
{
	SetProperty(0x9, VT_BSTR, propVal);
}

CString _DUfisCom::GetCmd()
{
	CString result;
	GetProperty(0xa, VT_BSTR, (void*)&result);
	return result;
}

void _DUfisCom::SetCmd(LPCTSTR propVal)
{
	SetProperty(0xa, VT_BSTR, propVal);
}

CString _DUfisCom::GetObject()
{
	CString result;
	GetProperty(0xb, VT_BSTR, (void*)&result);
	return result;
}

void _DUfisCom::SetObject(LPCTSTR propVal)
{
	SetProperty(0xb, VT_BSTR, propVal);
}

CString _DUfisCom::GetSeq()
{
	CString result;
	GetProperty(0xc, VT_BSTR, (void*)&result);
	return result;
}

void _DUfisCom::SetSeq(LPCTSTR propVal)
{
	SetProperty(0xc, VT_BSTR, propVal);
}

CString _DUfisCom::GetTws()
{
	CString result;
	GetProperty(0xd, VT_BSTR, (void*)&result);
	return result;
}

void _DUfisCom::SetTws(LPCTSTR propVal)
{
	SetProperty(0xd, VT_BSTR, propVal);
}

CString _DUfisCom::GetTwe()
{
	CString result;
	GetProperty(0xe, VT_BSTR, (void*)&result);
	return result;
}

void _DUfisCom::SetTwe(LPCTSTR propVal)
{
	SetProperty(0xe, VT_BSTR, propVal);
}

CString _DUfisCom::GetOrigName()
{
	CString result;
	GetProperty(0xf, VT_BSTR, (void*)&result);
	return result;
}

void _DUfisCom::SetOrigName(LPCTSTR propVal)
{
	SetProperty(0xf, VT_BSTR, propVal);
}

CString _DUfisCom::GetSelection()
{
	CString result;
	GetProperty(0x10, VT_BSTR, (void*)&result);
	return result;
}

void _DUfisCom::SetSelection(LPCTSTR propVal)
{
	SetProperty(0x10, VT_BSTR, propVal);
}

CString _DUfisCom::GetFields()
{
	CString result;
	GetProperty(0x11, VT_BSTR, (void*)&result);
	return result;
}

void _DUfisCom::SetFields(LPCTSTR propVal)
{
	SetProperty(0x11, VT_BSTR, propVal);
}

CString _DUfisCom::GetVersion()
{
	CString result;
	GetProperty(0x12, VT_BSTR, (void*)&result);
	return result;
}

void _DUfisCom::SetVersion(LPCTSTR propVal)
{
	SetProperty(0x12, VT_BSTR, propVal);
}

CString _DUfisCom::GetModule()
{
	CString result;
	GetProperty(0x13, VT_BSTR, (void*)&result);
	return result;
}

void _DUfisCom::SetModule(LPCTSTR propVal)
{
	SetProperty(0x13, VT_BSTR, propVal);
}

CString _DUfisCom::GetLastErrorMessage()
{
	CString result;
	GetProperty(0x14, VT_BSTR, (void*)&result);
	return result;
}

void _DUfisCom::SetLastErrorMessage(LPCTSTR propVal)
{
	SetProperty(0x14, VT_BSTR, propVal);
}

CString _DUfisCom::GetBuildDate()
{
	CString result;
	GetProperty(0x15, VT_BSTR, (void*)&result);
	return result;
}

void _DUfisCom::SetBuildDate(LPCTSTR propVal)
{
	SetProperty(0x15, VT_BSTR, propVal);
}

BOOL _DUfisCom::GetStayConnected()
{
	BOOL result;
	GetProperty(0x17, VT_BOOL, (void*)&result);
	return result;
}

void _DUfisCom::SetStayConnected(BOOL propVal)
{
	SetProperty(0x17, VT_BOOL, propVal);
}

/////////////////////////////////////////////////////////////////////////////
// _DUfisCom operations

BOOL _DUfisCom::InitCom(LPCTSTR ServerName, LPCTSTR AccessMethod)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x18, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		ServerName, AccessMethod);
	return result;
}

BOOL _DUfisCom::CleanupCom()
{
	BOOL result;
	InvokeHelper(0x19, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
	return result;
}

CString _DUfisCom::GetWorkstationName()
{
	CString result;
	InvokeHelper(0x1a, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL _DUfisCom::SetCedaPerameters(LPCTSTR UserName, LPCTSTR HomeAirport, LPCTSTR TableExt)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR;
	InvokeHelper(0x1b, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		UserName, HomeAirport, TableExt);
	return result;
}

CString _DUfisCom::Ufis(LPCTSTR Command, LPCTSTR Object, LPCTSTR Fields, LPCTSTR Where, LPCTSTR Data)
{
	CString result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR;
	InvokeHelper(0x1c, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		Command, Object, Fields, Where, Data);
	return result;
}

long _DUfisCom::GetBufferCount()
{
	long result;
	InvokeHelper(0x1d, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

CString _DUfisCom::GetBufferLine(long BufferIndex)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1e, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		BufferIndex);
	return result;
}

CString _DUfisCom::GetDataBuffer(long WithClear)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1f, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		WithClear);
	return result;
}

void _DUfisCom::ClearDataBuffer()
{
	InvokeHelper(0x20, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

short _DUfisCom::CallServer(LPCTSTR Command, LPCTSTR Object, LPCTSTR Fields, LPCTSTR Data, LPCTSTR Where, LPCTSTR Timout)
{
	short result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR;
	InvokeHelper(0x21, DISPATCH_METHOD, VT_I2, (void*)&result, parms,
		Command, Object, Fields, Data, Where, Timout);
	return result;
}

void _DUfisCom::AboutBox()
{
	InvokeHelper(0xfffffdd8, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// _DUfisComEvents properties

/////////////////////////////////////////////////////////////////////////////
// _DUfisComEvents operations
