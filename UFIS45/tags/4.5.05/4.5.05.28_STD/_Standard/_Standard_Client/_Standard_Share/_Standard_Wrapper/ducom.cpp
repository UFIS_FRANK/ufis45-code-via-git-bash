// Machine generated IDispatch wrapper class(es) created with ClassWizard

#include "stdafx.h"
#include "ducom.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// _DUCom properties

CString _DUCom::GetApplicationName()
{
	CString result;
	GetProperty(0x5, VT_BSTR, (void*)&result);
	return result;
}

void _DUCom::SetApplicationName(LPCTSTR propVal)
{
	SetProperty(0x5, VT_BSTR, propVal);
}

CString _DUCom::GetPort()
{
	CString result;
	GetProperty(0x6, VT_BSTR, (void*)&result);
	return result;
}

void _DUCom::SetPort(LPCTSTR propVal)
{
	SetProperty(0x6, VT_BSTR, propVal);
}

CString _DUCom::GetHopo()
{
	CString result;
	GetProperty(0x7, VT_BSTR, (void*)&result);
	return result;
}

void _DUCom::SetHopo(LPCTSTR propVal)
{
	SetProperty(0x7, VT_BSTR, propVal);
}

CString _DUCom::GetServerName()
{
	CString result;
	GetProperty(0x8, VT_BSTR, (void*)&result);
	return result;
}

void _DUCom::SetServerName(LPCTSTR propVal)
{
	SetProperty(0x8, VT_BSTR, propVal);
}

CString _DUCom::GetTableExtension()
{
	CString result;
	GetProperty(0x9, VT_BSTR, (void*)&result);
	return result;
}

void _DUCom::SetTableExtension(LPCTSTR propVal)
{
	SetProperty(0x9, VT_BSTR, propVal);
}

CString _DUCom::GetUserName()
{
	CString result;
	GetProperty(0xa, VT_BSTR, (void*)&result);
	return result;
}

void _DUCom::SetUserName(LPCTSTR propVal)
{
	SetProperty(0xa, VT_BSTR, propVal);
}

CString _DUCom::GetWorkstationName()
{
	CString result;
	GetProperty(0xb, VT_BSTR, (void*)&result);
	return result;
}

void _DUCom::SetWorkstationName(LPCTSTR propVal)
{
	SetProperty(0xb, VT_BSTR, propVal);
}

CString _DUCom::GetSendTimeoutSeconds()
{
	CString result;
	GetProperty(0xc, VT_BSTR, (void*)&result);
	return result;
}

void _DUCom::SetSendTimeoutSeconds(LPCTSTR propVal)
{
	SetProperty(0xc, VT_BSTR, propVal);
}

CString _DUCom::GetReceiveTimeoutSeconds()
{
	CString result;
	GetProperty(0xd, VT_BSTR, (void*)&result);
	return result;
}

void _DUCom::SetReceiveTimeoutSeconds(LPCTSTR propVal)
{
	SetProperty(0xd, VT_BSTR, propVal);
}

CString _DUCom::GetRecordSeparator()
{
	CString result;
	GetProperty(0xe, VT_BSTR, (void*)&result);
	return result;
}

void _DUCom::SetRecordSeparator(LPCTSTR propVal)
{
	SetProperty(0xe, VT_BSTR, propVal);
}

CString _DUCom::GetCedaIdentifier()
{
	CString result;
	GetProperty(0xf, VT_BSTR, (void*)&result);
	return result;
}

void _DUCom::SetCedaIdentifier(LPCTSTR propVal)
{
	SetProperty(0xf, VT_BSTR, propVal);
}

CString _DUCom::GetVersion()
{
	CString result;
	GetProperty(0x1, VT_BSTR, (void*)&result);
	return result;
}

void _DUCom::SetVersion(LPCTSTR propVal)
{
	SetProperty(0x1, VT_BSTR, propVal);
}

CString _DUCom::GetBuildDate()
{
	CString result;
	GetProperty(0x2, VT_BSTR, (void*)&result);
	return result;
}

void _DUCom::SetBuildDate(LPCTSTR propVal)
{
	SetProperty(0x2, VT_BSTR, propVal);
}

CString _DUCom::GetSQLAccessMethod()
{
	CString result;
	GetProperty(0x3, VT_BSTR, (void*)&result);
	return result;
}

void _DUCom::SetSQLAccessMethod(LPCTSTR propVal)
{
	SetProperty(0x3, VT_BSTR, propVal);
}

CString _DUCom::GetSQLConnectionString()
{
	CString result;
	GetProperty(0x4, VT_BSTR, (void*)&result);
	return result;
}

void _DUCom::SetSQLConnectionString(LPCTSTR propVal)
{
	SetProperty(0x4, VT_BSTR, propVal);
}

long _DUCom::GetPacketSize()
{
	long result;
	GetProperty(0x10, VT_I4, (void*)&result);
	return result;
}

void _DUCom::SetPacketSize(long propVal)
{
	SetProperty(0x10, VT_I4, propVal);
}

CString _DUCom::GetErrorSimulation()
{
	CString result;
	GetProperty(0x11, VT_BSTR, (void*)&result);
	return result;
}

void _DUCom::SetErrorSimulation(LPCTSTR propVal)
{
	SetProperty(0x11, VT_BSTR, propVal);
}

/////////////////////////////////////////////////////////////////////////////
// _DUCom operations

CString _DUCom::GetLastError()
{
	CString result;
	InvokeHelper(0x12, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
	return result;
}

BOOL _DUCom::CedaAction(LPCTSTR Command, LPCTSTR DBTable, LPCTSTR DBFields, LPCTSTR DBData, LPCTSTR DBWhere)
{
	BOOL result;
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR;
	InvokeHelper(0x13, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
		Command, DBTable, DBFields, DBData, DBWhere);
	return result;
}

long _DUCom::GetBufferCount()
{
	long result;
	InvokeHelper(0x14, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
	return result;
}

CString _DUCom::GetRecord(long RecordNo)
{
	CString result;
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x15, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
		RecordNo);
	return result;
}


/////////////////////////////////////////////////////////////////////////////
// _DUComEvents properties

/////////////////////////////////////////////////////////////////////////////
// _DUComEvents operations

void _DUComEvents::PackageReceived(long ipPackage)
{
	static BYTE parms[] =
		VTS_I4;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 ipPackage);
}
