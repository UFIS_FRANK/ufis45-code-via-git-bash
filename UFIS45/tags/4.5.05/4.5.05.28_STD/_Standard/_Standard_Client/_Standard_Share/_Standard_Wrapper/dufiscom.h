// Machine generated IDispatch wrapper class(es) created with ClassWizard
/////////////////////////////////////////////////////////////////////////////
// _DUfisCom wrapper class

class _DUfisCom : public COleDispatchDriver
{
public:
	_DUfisCom() {}		// Calls COleDispatchDriver default constructor
	_DUfisCom(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	_DUfisCom(const _DUfisCom& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:
	CString GetServerName();
	void SetServerName(LPCTSTR);
	CString GetAccessMethod();
	void SetAccessMethod(LPCTSTR);
	CString GetWorkStation();
	void SetWorkStation(LPCTSTR);
	CString GetUserName();
	void SetUserName(LPCTSTR);
	CString GetTableExt();
	void SetTableExt(LPCTSTR);
	CString GetHomeAirport();
	void SetHomeAirport(LPCTSTR);
	CString GetXUser();
	void SetXUser(LPCTSTR);
	CString GetReqid();
	void SetReqid(LPCTSTR);
	CString GetDest1();
	void SetDest1(LPCTSTR);
	CString GetDest2();
	void SetDest2(LPCTSTR);
	CString GetCmd();
	void SetCmd(LPCTSTR);
	CString GetObject();
	void SetObject(LPCTSTR);
	CString GetSeq();
	void SetSeq(LPCTSTR);
	CString GetTws();
	void SetTws(LPCTSTR);
	CString GetTwe();
	void SetTwe(LPCTSTR);
	CString GetOrigName();
	void SetOrigName(LPCTSTR);
	CString GetSelection();
	void SetSelection(LPCTSTR);
	CString GetFields();
	void SetFields(LPCTSTR);
	CString GetVersion();
	void SetVersion(LPCTSTR);
	CString GetModule();
	void SetModule(LPCTSTR);
	CString GetLastErrorMessage();
	void SetLastErrorMessage(LPCTSTR);
	CString GetBuildDate();
	void SetBuildDate(LPCTSTR);
	BOOL GetStayConnected();
	void SetStayConnected(BOOL);

// Operations
public:
	BOOL InitCom(LPCTSTR ServerName, LPCTSTR AccessMethod);
	BOOL CleanupCom();
	CString GetWorkstationName();
	BOOL SetCedaPerameters(LPCTSTR UserName, LPCTSTR HomeAirport, LPCTSTR TableExt);
	CString Ufis(LPCTSTR Command, LPCTSTR Object, LPCTSTR Fields, LPCTSTR Where, LPCTSTR Data);
	long GetBufferCount();
	CString GetBufferLine(long BufferIndex);
	CString GetDataBuffer(long WithClear);
	void ClearDataBuffer();
	short CallServer(LPCTSTR Command, LPCTSTR Object, LPCTSTR Fields, LPCTSTR Data, LPCTSTR Where, LPCTSTR Timout);
	void AboutBox();
};
/////////////////////////////////////////////////////////////////////////////
// _DUfisComEvents wrapper class

class _DUfisComEvents : public COleDispatchDriver
{
public:
	_DUfisComEvents() {}		// Calls COleDispatchDriver default constructor
	_DUfisComEvents(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	_DUfisComEvents(const _DUfisComEvents& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
};
