; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=C_Standard_WrapperDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "_Standard_Wrapper.h"
ODLFile=_Standard_Wrapper.odl

ClassCount=4
Class1=C_Standard_WrapperApp
Class2=C_Standard_WrapperDlg
Class3=CAboutDlg
Class4=C_Standard_WrapperDlgAutoProxy

ResourceCount=5
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD__STANDARD_WRAPPER_DIALOG
Resource4=IDD_ABOUTBOX (English (U.S.))
Resource5=IDD__STANDARD_WRAPPER_DIALOG (English (U.S.))

[CLS:C_Standard_WrapperApp]
Type=0
HeaderFile=_Standard_Wrapper.h
ImplementationFile=_Standard_Wrapper.cpp
Filter=N

[CLS:C_Standard_WrapperDlg]
Type=0
HeaderFile=_Standard_WrapperDlg.h
ImplementationFile=_Standard_WrapperDlg.cpp
Filter=D
LastObject=IDC_AATLOGINCTRL1
BaseClass=CDialog
VirtualFilter=dWC

[CLS:CAboutDlg]
Type=0
HeaderFile=_Standard_WrapperDlg.h
ImplementationFile=_Standard_WrapperDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889
Class=CAboutDlg

[CLS:C_Standard_WrapperDlgAutoProxy]
Type=0
HeaderFile=DlgProxy.h
ImplementationFile=DlgProxy.cpp
BaseClass=CCmdTarget
Filter=N

[DLG:IDD__STANDARD_WRAPPER_DIALOG]
Type=1
ControlCount=3
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Class=C_Standard_WrapperDlg

[DLG:IDD__STANDARD_WRAPPER_DIALOG (English (U.S.))]
Type=1
Class=C_Standard_WrapperDlg
ControlCount=9
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_AATLOGINCTRL1,{17D7E209-2DEC-48BC-83E2-3E9F43B90D59},1342242816
Control4=IDC_BCCOMCLIENTCTRL1,{EA6DE32C-D8C1-474D-96DD-90ADDFA68767},1342242816
Control5=IDC_TABCTRL1,{64E8E385-05E2-11D2-9B1D-9B0630BC8F12},1342242816
Control6=IDC_UCOMCTRL1,{F9478336-76BA-11D6-8067-0001022205E4},1342242816
Control7=IDC_UGANTTCTRL1,{6782E138-3223-11D4-996A-0000863DE95C},1342242816
Control8=IDC_UFISCOMCTRL1,{A2F31E95-C74F-11D3-A251-00500437F607},1342242816
Control9=IDC_AMPTESTCTRL1,{59BEC158-923C-11D5-9969-0000865098D4},1342242816

[DLG:IDD_ABOUTBOX (English (U.S.))]
Type=1
Class=?
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

