// Machine generated IDispatch wrapper class(es) created with ClassWizard

#include "stdafx.h"
#include "bcproxy.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// _IBcPrxyEvents properties

/////////////////////////////////////////////////////////////////////////////
// _IBcPrxyEvents operations


/////////////////////////////////////////////////////////////////////////////
// IBcPrxy properties

/////////////////////////////////////////////////////////////////////////////
// IBcPrxy operations

void IBcPrxy::RegisterObject(LPCTSTR table, LPCTSTR command, long ownBc, LPCTSTR appl)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_I4 VTS_BSTR;
	InvokeHelper(0x1, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 table, command, ownBc, appl);
}

void IBcPrxy::UnregisterObject(LPCTSTR table, LPCTSTR command)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR;
	InvokeHelper(0x2, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 table, command);
}

CString IBcPrxy::GetVersion()
{
	CString result;
	InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}

void IBcPrxy::SetSpoolOn()
{
	InvokeHelper(0x4, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IBcPrxy::SetSpoolOff()
{
	InvokeHelper(0x5, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void IBcPrxy::GetNextBufferdBC(BSTR* pReqId, BSTR* pDest1, BSTR* pDest2, BSTR* pCmd, BSTR* pObject, BSTR* pSeq, BSTR* pTws, BSTR* pTwe, BSTR* pSelection, BSTR* pFields, BSTR* pData, BSTR* pBcNum)
{
	static BYTE parms[] =
		VTS_PBSTR VTS_PBSTR VTS_PBSTR VTS_PBSTR VTS_PBSTR VTS_PBSTR VTS_PBSTR VTS_PBSTR VTS_PBSTR VTS_PBSTR VTS_PBSTR VTS_PBSTR;
	InvokeHelper(0x6, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 pReqId, pDest1, pDest2, pCmd, pObject, pSeq, pTws, pTwe, pSelection, pFields, pData, pBcNum);
}

void IBcPrxy::SetFilterRange(LPCTSTR table, LPCTSTR sFromField, LPCTSTR sToField, LPCTSTR sFromValue, LPCTSTR sToValue)
{
	static BYTE parms[] =
		VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR;
	InvokeHelper(0x7, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
		 table, sFromField, sToField, sFromValue, sToValue);
}

CString IBcPrxy::GetBuildDate()
{
	CString result;
	InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
	return result;
}
