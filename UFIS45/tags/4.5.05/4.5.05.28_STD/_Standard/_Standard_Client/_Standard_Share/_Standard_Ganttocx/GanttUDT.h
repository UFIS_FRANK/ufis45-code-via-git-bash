#ifndef __UdtForOlePassing_H__
#define __UdtForOlePassing_H__

	struct _BAR_RECORD
	{
		BSTR		 Key;				//the key of the bar
		BSTR		 LineKey;			//the key of the line
		long		 LineNo;			//the lineNo
		DATE		 Begin,				//begin of the bar
					 End;				//end of the bar
		BSTR		 BarText,			//text of the bar
					 LeftOutsideText,	//text left of the bar
					 RightOutsideText;	//text right of the bar
		int			 Shape;				//shape of the bar (0=NORMAL, 1=RAISED, 2=INSET, 3=BMP)
		long		 BackColor,			//backcolor of the bar
					 LeftBackColor,		//only if SplitColor==TRUE!
					 RightBackColor,	//only if SplitColor==TRUE!
					 leftTriangleColor, //Color of left triagle if set otherwise -1
					 rightTriangleColor;//Color of left triagle if set otherwise -1
		long		 TextColor,			//textcolor of the bar's text
					 LeftTextColor,		//textcolor of the left bar's text
					 RightTextColor;	//textcolor of the right bar's text
		int			 OverlapLevel;		//overlap-level if there are at least two bars overlapping
		BOOL		 SplitColor;		//flag to see if the bar should be colored in 2 colors
		BOOL		 WithTextOutside;	//flag to see if the bar has Outside text
		BOOL		 WithArrowFrom,		//flag to see if the bar is involved in an "arrow-relation" with another bar
					 WithArrowTo;
		BSTR		 MainbarKey;		//The key of the main bar, if this is a sub bar
		BOOL		 isSubbar;
	} BAR_RECORD;
#endif