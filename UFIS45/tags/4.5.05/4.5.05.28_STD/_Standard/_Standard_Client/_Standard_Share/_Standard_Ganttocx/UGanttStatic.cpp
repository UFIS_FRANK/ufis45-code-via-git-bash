// UGanttStatic.cpp : implementation file
//

#include <stdafx.h>
#include <UGantt.h>
#include <UGanttStatic.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUGanttStatic

CUGanttStatic::CUGanttStatic()
{
	// We start with a nice Background color:
	omBkColor = COLOR_WINDOW;
}

CUGanttStatic::~CUGanttStatic()
{
}


BEGIN_MESSAGE_MAP(CUGanttStatic, CStatic)
	//{{AFX_MSG_MAP(CUGanttStatic)
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUGanttStatic message handlers



BOOL CUGanttStatic::OnEraseBkgnd(CDC* pDC) 
{
	// Paint the background with our own color:
	CRect olRect;
	GetClientRect(&olRect);

	MyDraw(pDC, olRect);

	//CBrush olBrush(omBkColor);

	
	//pDC->FillRect(&olRect, &olBrush);

	return 0;
	//	return CWnd::OnEraseBkgnd(pDC);
}

void CUGanttStatic::MyDraw(CDC* pdc, CRect opRect)
{
	//CRect olRect;
	CBrush olBrush(omBkColor);

	//GetClientRect(&olRect);
	//LONG dx = olRect.right - olRect.left;
	//olRect.left = lLeft;
	//olRect.right = lLeft + dx;
	pdc->FillRect(&opRect, &olBrush);
	//OnEraseBkgnd( pdc );
	//pdc->BitBlt( omCurrentTabRect.left, omCurrentTabRect.top, omCurrentTabRect.right, omCurrentTabRect.bottom, &myMemDC, omCurrentTabRect.left, omCurrentTabRect.top, SRCCOPY);
}

