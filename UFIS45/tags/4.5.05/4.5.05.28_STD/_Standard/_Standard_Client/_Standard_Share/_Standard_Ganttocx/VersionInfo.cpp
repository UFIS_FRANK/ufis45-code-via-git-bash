// VersionInfo.cpp : implementation file
//

#include <stdafx.h>
#include <VersionInfo.h>

#pragma comment(lib,"Version.lib") 

bool VersionInfo::GetVersionInfo(HMODULE hModule,VersionInfo& ropVersionInfo)
{
	DWORD ulHandle;

	char olFilename[256];
	if (GetModuleFileName(hModule,olFilename,sizeof(olFilename)) == 0)
		return false;

	DWORD ulSize = ::GetFileVersionInfoSize(olFilename,&ulHandle);
	if (!ulSize)
		return false;

	void *polData = malloc(ulSize);
	if (!::GetFileVersionInfo(olFilename,ulHandle,ulSize,polData))
		return false;

	BYTE *olVar = NULL;
	
	unsigned int ulLen = ulSize;
	if (!::VerQueryValue(polData,"\\VarFileInfo\\Translation",(void **)&olVar,&ulLen))
	{
		free (polData);
		return false;
	}

	LONG hexnumber = olVar[2] + olVar[3] * 0x100l + olVar[0] * 0x10000l + olVar[1] * 0x1000000l;

	CString olLanguage;
	olLanguage.Format("%08x",hexnumber);

	CString olBuffer = "\\StringFileInfo\\" + olLanguage + "\\";
	char *polBuffer;
	char olFormat[512];
	strcpy(olFormat,olBuffer + "Comments");
	if (::VerQueryValue(polData,olFormat,(void **)&polBuffer,&ulLen))
	{
		ropVersionInfo.omComments = polBuffer;
	}

	strcpy(olFormat,olBuffer + "CompanyName");
	if (::VerQueryValue(polData,olFormat,(void **)&polBuffer,&ulLen))
	{
		ropVersionInfo.omCompanyName = polBuffer;
	}

	strcpy(olFormat,olBuffer + "FileDescription");
	if (::VerQueryValue(polData,olFormat,(void **)&polBuffer,&ulLen))
	{
		ropVersionInfo.omFileDescription = polBuffer;
	}

	strcpy(olFormat,olBuffer + "FileVersion");
	if (::VerQueryValue(polData,olFormat,(void **)&polBuffer,&ulLen))
	{
		ropVersionInfo.omFileVersion = polBuffer;
	}

	strcpy(olFormat,olBuffer + "InternalName");
	if (::VerQueryValue(polData,olFormat,(void **)&polBuffer,&ulLen))
	{
		ropVersionInfo.omInternalName = polBuffer;
	}

	strcpy(olFormat,olBuffer + "LegalCopyright");
	if (::VerQueryValue(polData,olFormat,(void **)&polBuffer,&ulLen))
	{
		ropVersionInfo.omLegalCopyright = polBuffer;
	}

	strcpy(olFormat,olBuffer + "LegalTrademarks");
	if (::VerQueryValue(polData,olFormat,(void **)&polBuffer,&ulLen))
	{
		ropVersionInfo.omLegalTrademarks = polBuffer;
	}

	strcpy(olFormat,olBuffer + "OriginalFilename");
	if (::VerQueryValue(polData,olFormat,(void **)&polBuffer,&ulLen))
	{
		ropVersionInfo.omOriginalFilename = polBuffer;
	}

	strcpy(olFormat,olBuffer + "PrivateBuild");
	if (::VerQueryValue(polData,olFormat,(void **)&polBuffer,&ulLen))
	{
		ropVersionInfo.omPrivateBuild = polBuffer;
	}

	strcpy(olFormat,olBuffer + "ProductName");
	if (::VerQueryValue(polData,olFormat,(void **)&polBuffer,&ulLen))
	{
		ropVersionInfo.omProductName = polBuffer;
	}

	strcpy(olFormat,olBuffer + "ProductVersion");
	if (::VerQueryValue(polData,olFormat,(void **)&polBuffer,&ulLen))
	{
		ropVersionInfo.omProductVersion = polBuffer;
	}

	strcpy(olFormat,olBuffer + "SpecialBuild");
	if (::VerQueryValue(polData,olFormat,(void **)&polBuffer,&ulLen))
	{
		ropVersionInfo.omSpecialBuild = polBuffer;
	}

	free (polData);

	return true;
}