#if !defined(AFX_UGANTTCTL_H__6782E146_3223_11D4_996A_0000863DE95C__INCLUDED_)
#define AFX_UGANTTCTL_H__6782E146_3223_11D4_996A_0000863DE95C__INCLUDED_

#define CF_GANTT	18
#define	WM_REFRESH_LINE		(WM_USER + 1000)

#define MIN_DATE (-657434L)
#define MAX_DATE (2958465L)

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include <CCSTimeScale.h>
#include <ForwardTarget.h>
#include <UGanttStatic.h>
#include <UGanttArrowData.h>
#include <GanttUDT.h>
#include <UTimeFrame.h>
#include <LifeStyleObject.h>
// UGanttCtl.h : Declaration of the CUGanttCtrl ActiveX Control class.

/////////////////////////////////////////////////////////////////////////////
// CUGanttCtrl : See UGanttCtl.cpp for implementation.

enum
{
	NORMAL = 0,
	RAISED = 1,
	INSET = 2,
	BMP = 3
};

enum
{
  SCROLL_NOSCROLL = 0,
  SCROLL_UP = 1,
  SCROLL_DOWN = 2,
  SCROLL_LEFT = 3,
  SCROLL_RIGHT = 4
};

class TabGroupColumn : public CObject 
{
public:
	long ColNo;
	long AlternateColor;
};


class DECORATION_OBJECT : public CObject
{
public:
	CString			ID;				// <ID> for normal Deco
	DECORATION_OBJECT()
	{
		TriangleLeftColor	= 0;
		TriangleRightColor	= 0;
		AsFull				= true;
		AsTriangle			= false;
	}
	CStringArray	LocationList;
	CStringArray	PixelList;
	CStringArray	ColorList;
	long			TriangleLeftColor;
	long			TriangleRightColor;
	bool			AsFull;		// True = full triagle, False = half size triangel (height/width)
									// default: true
	bool			AsTriangle;		// True if triangle or false if normal deco object (default: false)
};

class BarData 
{
public:
	BarData()
	{
		Shape			= NORMAL;
		SplitColor		= FALSE;
		BackColor		= COLORREF(RGB(128,128,255));			// Blue
		TextColor		= COLORREF(RGB(  0,  0,  0));			// Black
		LeftTextColor	= COLORREF(RGB(  0,  0,  0));			// Black
		RightTextColor	= COLORREF(RGB(  0,  0,  0));			// Black
		WithArrowFrom	= FALSE;
		WithArrowTo		= FALSE;
		isSubbar		= false;
	}
	~BarData()
	{
		omSubBars.DeleteAll();
	}
	const BarData &operator = (const BarData &opO) 
	{
		this->Key				= opO.Key;
		this->LineKey			= opO.LineKey;
		this->LineNo			= opO.LineNo;
		this->Begin				= opO.Begin;
		this->End				= opO.End;
		this->BarText			= opO.BarText;
		this->LeftOutsideText	= opO.LeftOutsideText;
		this->RightOutsideText	= opO.RightOutsideText;
		this->LeftTextColor		= opO.LeftTextColor;
		this->RightTextColor	= opO.RightTextColor;
		this->Shape				= opO.Shape;
		this->BackColor			= opO.BackColor;
		this->SplitColor		= opO.SplitColor;
		this->LeftBackColor		= opO.LeftBackColor;
		this->RightBackColor	= opO.RightBackColor;
		this->TextColor			= opO.TextColor;
		this->OverlapLevel		= opO.OverlapLevel;
		this->WithArrowFrom		= opO.WithArrowFrom;
		this->WithArrowTo		= opO.WithArrowTo;
		this->isSubbar			= opO.isSubbar;
		for (int i = 0; i < opO.omSubBars.GetSize(); i++)
		{
			BarData *polSubBar = new BarData();
			*polSubBar = opO.omSubBars[i];
			this->omSubBars.Add (polSubBar);
		}
		return *this;
	}

	CString		 Key;				//the key of the bar
	CString		 LineKey;			//the key of the line
	long		 LineNo;			//the lineNo
	COleDateTime Begin,				//begin of the bar
				 End;				//end of the bar
	CString		 BarText,			//text of the bar
				 LeftOutsideText,	//text left of the bar
				 RightOutsideText;	//text right of the bar
	int			 Shape;				//shape of the bar (0=NORMAL, 1=RAISED, 2=INSET, 3=BMP)
	COLORREF	 BackColor,			//backcolor of the bar
				 LeftBackColor,		//only if SplitColor==TRUE!
				 RightBackColor;	//only if SplitColor==TRUE!
	COLORREF	 TextColor,			//textcolor of the bar's text
				 LeftTextColor,		//textcolor of the left bar's text
				 RightTextColor;	//textcolor of the right bar's text
	int			 OverlapLevel;		//overlap-level if there are at least two bars overlapping
	BOOL		 SplitColor;		//flag to see if the bar should be colored in 2 colors
	BOOL		 WithTextOutside;	//flag to see if the bar has Outside text
	BOOL		 WithArrowFrom,		//flag to see if the bar is involved in an "arrow-relation" with another bar
				 WithArrowTo;

	//MWO: 19.08.2002
	CCSPtrArray<BarData> omSubBars; //To draw subbars, which are related to a main bar
	CString		 MainbarKey;		//The key of the main bar, if this is a sub bar
	bool		 isSubbar;
	//END sMWO: 19.08.2002
};


class LineData
{
public:

	LineData()
	{
		TabTextColor				= COLORREF(RGB(0,0,0));
		TabBackColor				= ::GetSysColor(COLOR_BTNFACE);//COLORREF(RGB(192,192,192));
		LineSeparator				= FALSE;
		LineSeparatorHeight			= 0;
		LineSeparatorHeightComplete = 0;
		LineSeparatorBegin			= 1;
	}

	CString			SortString;

	~LineData()
	{
		omBackBars.DeleteAll();
		omBars.DeleteAll();
	}

	CString					Key;
	CString					TabValues;
	CCSPtrArray<BarData>	omBackBars;
	CCSPtrArray<BarData>	omBars;
	COLORREF				TabTextColor;
	COLORREF				TabBackColor;
	int						MaxOverlapLevel;
	BOOL					LineSeparator;
	short					LineSeparatorHeight,
							LineSeparatorHeightComplete,
							LineSeparatorBegin;
	COLORREF				LineSeparatorColor;
	CPen					LineSeparatorPen;
};


class CUGanttCtrl : public COleControl
{
	DECLARE_DYNCREATE(CUGanttCtrl)

private:
	bool m_SelfIsDragging;
	bool m_CanAcceptDrop;

// Where should the timer event scroll to ?
// Needed while dragging, moving and sizing
  int m_AutoScrollDirection;   // Possible values: SCROLL_NOSCROLL, SCROLL_UP
                               //                  SCROLL_DOWN, SCROLL_LEFT, SCROLL_RIGHT

// Internal scroll time counter (decreased every 1/10 second)                                     
// Needed while dragging, moving and sizing
  int m_nScrollTime;

// Remebers Old positions of bars (back bars) while moving or sizing:
  DATE m_dMoveBarOldBegin;
  DATE m_dMoveBarOldEnd;
  DATE m_dMoveBkBarOldBegin;
  DATE m_dMoveBkBarOldEnd;
  CCSPtrArray<COleDateTime>	m_MoveSubBarsOldBegin;
  CCSPtrArray<COleDateTime>	m_MoveSubBarsOldEnd;

  struct _BAR_RECORD omOleBar;
  void CopyBarToOleBar(struct _BAR_RECORD *pOleBar, BarData *pBar);
  void CopyOleBarTo(BarData *pBarstruct, _BAR_RECORD *pOleBar);
  void InitOleBar(struct _BAR_RECORD *pOleBar);

	//MWO: 13.08.2002 store the last sort order
	CUIntArray omLastSortOrder;

// Stores the actual cursor, so it can be restored, when windows changed it
  HCURSOR m_nCursor;

// Checks, if the Time Marker is in the GanttArea:
  BOOL TimeMarkerVisible(CUGanttStatic *popTimeMarker);
  BOOL SaveScreenToFile(LPCTSTR fileName, COleDateTime dtFrom);
  long GetGanttHeight();
  long GetNextVScrollPos( int curStartPos, long height);
  void DrawTimeMarker(CDC *pDc, bool bSaveOption);
  void OnMyDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid, bool bSaveOption);
// Constructor
public:
	CUGanttCtrl();

//drag & drop stuff
	CForwardingTarget<CUGanttCtrl> m_DropTarget;

	//MWO: 25.06.03
	struct _LIFE_STYLE omLifeStyle;
	void DrawLifeStyle(CDC *pdc, CRect opRect, int MyColor, int ipMode, BOOL DrawDown, int ipPercent);
	void DrawBodyBackGround(CBitmap &romBitmap);
	//END MWO: 25.06.03
	
	DROPEFFECT OnDragEnter(CWnd* pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);
	DROPEFFECT OnDragOver(CWnd* pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);
	BOOL OnDrop(CWnd* pWnd, COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point);
	void OnDragLeave(CWnd* pWnd);
    // Drag/Drop related fields
	int m_nDragDelay;       // time delay (in msec) before drag should start
	int m_nDragMinDist;     // min. distance (radius) before drag should start

// Control Properties
	short	BarOverlapOffset,
			BarNumberExpandLine;
	long	ResizeAreaWidth,
			ResizeMinimalWidth,
			SplitterPosition,
			TabColumns,
			TabHighlightColor,
			TabLines,
			TimeScaleHeaderHeight;

	COleDataSource* omDataSource;

	COleDateTime TimeFrameFrom;
	COleDateTime TimeFrameTo;
	COleDateTime omStartTime;	//Current Start time for Timescale
	COleDateTime omCurrentTime;

	CString TabBodyFontName;
	CString TabHeaderFontName;
	CString TabHeaderLengthString;
	CString TabHeaderString;

	short	TabLineHeight;
	short	TabFontSize;
	short	TabHeaderFontSize;


	BOOL With2ndScroller,
		 WithHorizontalScroller,
		 WithTimeScale;

	int imTimeScaleDuration;
	int imStartIdx;  //Start Index for Vertical Line

//handling of the bars
	BOOL	bmFoundBar;			//is the mouse on a bar?
	BOOL	bmFoundBkBar;		//is the mouse on a backgroundbar?
	CString	omBarMode,			//can have the values MOVE, SIZE, NONE
			omCurrBarKey;		//the key of the current bar
	int		imBarOffsetLeft,	//from the click-point to the left of the bar
			imBarOffsetRight,	//from the click-point to the right of the bar
			imCurrBarLine,		//the current bar-line in omLines (is also the line in the Gantt)
			imCurrBarPos,		//the current bar-pos in omLines[imCurrBarLine].omBars)
			imMaxLeft,			//the max left point of resizing a bar
			imMaxRight,			//the max right point of resizing a bar
			imCurrYTop,			//the y-point of the top of the current line
			imRefreshY;			//the y-point of the top of the line which should be refreshed
	CRect	omCurrBarRect;		//the rect of the current bar - it's important for moving and sizing
								//a bar, because you move the bitmap of only the current bar
	BarData*		pomBar;

//handling of the arrows
	BOOL		bmWithArrows;
	CMapStringToOb	mapBarRect; //store the rects of the involved bars in the map for fast redrawing
	CCSPtrArray<CUGanttArrowData> omArrows;
	short		smArrowWidth,
				smArrowHead;
	COLORREF	omArrowColorNormal,
				omArrowColorSelected;

// Normal members

	CCSPtrArray<LineData> omLines;
	//MWO: 19.08.2002
	CCSPtrArray<DECORATION_OBJECT>  omDecoObjects;
	CMapStringToPtr					mapBarDecoObjects;
	CMapStringToPtr					mapBarTriangleObjects;
	CMapStringToPtr					mapBkBarDecoObjects;
	CMapStringToPtr					mapBars;
	CMapStringToPtr					mapSubBars;
	CMapStringToPtr					mapBkBars;
//MWO: 11.02.2003
	CUIntArray						omGroupCols;
	CUIntArray						omGroupAlternateColor;

	DECORATION_OBJECT* GetDecorationObject(LPCTSTR ID);
	//END MWO: 19.08.2002

  CCSPtrArray<CUGanttStatic> omTimeMarkers;

	//CStringArray omTabLines;
	CStringArray omTabHeader;
	CUIntArray   omTabHeaderLen;
	CUIntArray   omTabTextColors;
	CUIntArray   omTabBackColors;
	CUIntArray	 omTabLineHeight;
	int imTabLogicalWidth;				//Width == sum(all column sizes)
	int imTabCurrentWidth;				//Depends on the splitter position
	CFont omTabHeaderFont;
	CFont omTabFont;

//	CCSPtrArray<GanttLineData> omGanttLines;
	CScrollBar *pomHScroll;
	CScrollBar *pomTabVScroll;
	CScrollBar *pomVScroll;
	int  imTabVScrollWidth;
	int  imTab2ndVScrollWidth;
	int  imTabHScrollHeight;
	int  imTabSelectedLine;
	bool bmAllSelected;
	int  imCurrentHighlightLine;

	bool bmFirstDrawing;		//just for drawing first time different

//Area positions
	CRect omCurrentTabRect,
		  omTotalTabRect,
		  omTimeScaleRect,
		  omGanttRect,
		  omSplitterRect,
		  omTolerantRect;

//members for optimizing the drawing
	CBitmap		bmpGantt;

	COLORREF	omBackgroundColor;

	CString		omDrawHint;
	BOOL		bmDrawALL,
				bmDrawTAB,
				bmDrawGANTT,
				bmDrawSPLITTER,
				bmDrawTIMESCALE,
				bmDrawGLINE;
//TimeScale methods
	void DrawTimeScale(CDC* pdc);
	void DrawTimeScaleMark( CDC *pdc );
	COleDateTime GetTimeFromX(int ipX);
	int GetXFromTime(COleDateTime opTime);
	CCSTimeScale *pomTimeScale;
//Table methods
	void DrawTab(CDC *pdc);
//Splitter methods / members
	void DrawSplitter(CDC *pdc);
	int imCurrSplitterOffset;
	bool isSplitterUpdating;
//Gantt Methods
	void DrawGanttBmp();
	void DrawGanttBmp(CDC *pdc, bool DrawGanttBmp);
	//Draws one line in the gantt area, if ipCurrY == -1 => the function is
	//forced to calculate the y-coord itself otherwise use the parameter
	//remind the click!

//Helper methods

	void Handle2ndScroller();// Creates or removes the 2nd scrollbar
	void CalcAreaRects();

  BOOL  IntSetCursor(HCURSOR hpCursor);
//Sets and remembers the cursor, use only this to change the cursor
//Also checks if omBarMode is "SIZE" or "MOVE"

////////////
	void OnTabMouseMove(UINT nFlags, CPoint point);

//////////////
	void MakeSampleData();
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUGanttCtrl)
	public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	//}}AFX_VIRTUAL

// Implementation
protected:
	~CUGanttCtrl();

	DECLARE_OLECREATE_EX(CUGanttCtrl)    // Class factory and guid
	DECLARE_OLETYPELIB(CUGanttCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CUGanttCtrl)     // Property page IDs
	DECLARE_OLECTLTYPE(CUGanttCtrl)		 // Type name and misc status

	// drag/drop stuff
	COleDataObject  m_DataObject;
	COleDropSource	m_DropSource;
	DROPEFFECT		m_prevDropEffect;
	CWnd*           m_callMsgWnd;
	//COleDropTarget  m_DropTarget;

	// current selection - store the node and the last drawn rect
	BarData* m_pSelectedBar;
	static BarData* NEAR m_pDragBar;

// Message maps
	//{{AFX_MSG(CUGanttCtrl)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Dispatch maps
	//{{AFX_DISPATCH(CUGanttCtrl)
	BOOL m_enableDragDropBar;
	afx_msg void OnEnableDragDropBarChanged();
	BOOL m_enableDragDropBkBar;
	afx_msg void OnEnableDragDropBkBarChanged();
	BOOL m_enableDragDrop;
	afx_msg void OnEnableDragDropChanged();
	BOOL m_autoScroll;
	afx_msg void OnAutoScrollChanged();
	long m_nScrollDelay;
	afx_msg void OnScrollDelayChanged();
	long m_nScrollInset;
	afx_msg void OnScrollInsetChanged();
	long m_nScrollInterval;
	afx_msg void OnScrollIntervalChanged();
	CString m_barFontName;
	afx_msg void OnBarFontNameChanged();
	long m_barFontSize;
	afx_msg void OnBarFontSizeChanged();
	long m_tabBodyFontSize;
	afx_msg void OnTabBodyFontSizeChanged();
	BOOL m_tabHeaderFontBold;
	afx_msg void OnTabHeaderFontBoldChanged();
	BOOL m_tabBodyFontBold;
	afx_msg void OnTabBodyFontBoldChanged();
	CString m_tabFontName;
	afx_msg void OnTabFontNameChanged();
	BOOL m_barFontBold;
	afx_msg void OnBarFontBoldChanged();
	long m_subbarStackCount;
	afx_msg void OnSubbarStackCountChanged();
	short m_uTCOffsetInHours;
	afx_msg void OnUTCOffsetInHoursChanged();
	BOOL m_autoMoveSubBarsWithBar;
	afx_msg void OnAutoMoveSubBarsWithBarChanged();
	BOOL m_overlappingRightBarTop;
	afx_msg void OnOverlappingRightBarTopChanged();
	BOOL m_autoSizeLineHeightToFit;
	afx_msg void OnAutoSizeLineHeightToFitChanged();
	BOOL m_lifeStyle;
	afx_msg void OnLifeStyleChanged();
	CString m_version;
	afx_msg void OnVersionChanged();
	CString m_buildDate;
	afx_msg void OnBuildDateChanged();
	BOOL m_splitterMovable;
	afx_msg void OnSplitterMovableChanged();
	afx_msg long GetTabColumns();
	afx_msg void SetTabColumns(long nNewValue);
	afx_msg long GetTabLines();
	afx_msg void SetTabLines(long nNewValue);
	afx_msg BSTR GetTabHeaderString();
	afx_msg void SetTabHeaderString(LPCTSTR lpszNewValue);
	afx_msg BSTR GetTabHeaderLengthString();
	afx_msg void SetTabHeaderLengthString(LPCTSTR lpszNewValue);
	afx_msg short GetTabLineHeight();
	afx_msg void SetTabLineHeight(short nNewValue);
	afx_msg short GetTabFontSize();
	afx_msg void SetTabFontSize(short nNewValue);
	afx_msg short GetTabHeaderFontSize();
	afx_msg void SetTabHeaderFontSize(short nNewValue);
	afx_msg BSTR GetTabHeaderFontName();
	afx_msg void SetTabHeaderFontName(LPCTSTR lpszNewValue);
	afx_msg BSTR GetTabBodyFontName();
	afx_msg void SetTabBodyFontName(LPCTSTR lpszNewValue);
	afx_msg DATE GetTimeFrameFrom();
	afx_msg void SetTimeFrameFrom(DATE newValue);
	afx_msg DATE GetTimeFrameTo();
	afx_msg void SetTimeFrameTo(DATE newValue);
	afx_msg long GetTimeScaleHeaderHeight();
	afx_msg void SetTimeScaleHeaderHeight(long nNewValue);
	afx_msg long GetSplitterPosition();
	afx_msg void SetSplitterPosition(long nNewValue);
	afx_msg BOOL GetWith2ndScroller();
	afx_msg void SetWith2ndScroller(BOOL bNewValue);
	afx_msg long GetTimeScaleDuration();
	afx_msg void SetTimeScaleDuration(long nNewValue);
	afx_msg long GetTabHighlightColor();
	afx_msg void SetTabHighlightColor(long nNewValue);
	afx_msg long GetResizeAreaWidth();
	afx_msg void SetResizeAreaWidth(long nNewValue);
	afx_msg long GetResizeMinimalWidth();
	afx_msg void SetResizeMinimalWidth(long nNewValue);
	afx_msg short GetBarOverlapOffset();
	afx_msg void SetBarOverlapOffset(short nNewValue);
	afx_msg short GetBarNumberExpandLine();
	afx_msg void SetBarNumberExpandLine(short nNewValue);
	afx_msg BOOL GetWithHorizontalScroller();
	afx_msg void SetWithHorizontalScroller(BOOL bNewValue);
	afx_msg short GetArrowWidth();
	afx_msg void SetArrowWidth(short nNewValue);
	afx_msg short GetArrowHead();
	afx_msg void SetArrowHead(short nNewValue);
	afx_msg long GetArrowColorNormal();
	afx_msg void SetArrowColorNormal(long nNewValue);
	afx_msg long GetArrowColorSelected();
	afx_msg void SetArrowColorSelected(long nNewValue);
	afx_msg void TabSetHeaderText(LPCTSTR HeaderText);
	afx_msg void ResetContent();
	afx_msg void MakeNewLine(long LineNo, LPCTSTR TabValues);
	afx_msg void TabSetLineColors(long LineNo, long TextColor, long BackColor);
	afx_msg void GanttSetBackColor(long BackColor);
	afx_msg void DeleteLine(long LineNo);
	afx_msg long TabGetCurrentSelected();
	afx_msg BSTR TabGetLinesByTextColor(long Color);
	afx_msg BSTR TabGetLinesByBackColor(long Color);
	afx_msg void TabUpdateValues(long LineNo, LPCTSTR Values);
	afx_msg void AppendLine(LPCTSTR Values);
	afx_msg BOOL AddBkBarToLine(long LineNo, LPCTSTR BkBarKey, DATE StartTime, DATE EndTime, long BarColor, LPCTSTR BarText);
	afx_msg void RefreshArea(LPCTSTR Hint);
	afx_msg BOOL AddBarToLine(long LineNo, LPCTSTR Key, DATE StartTime, DATE EndTime, LPCTSTR BarText, long Shape, long BackColor, long TextColor, BOOL SplitColor, long LeftColor, long RightColor, LPCTSTR LeftOutsideText, LPCTSTR RightOutsideText, long LeftTextColor, long RightTextColor);
	afx_msg BOOL DeleteBarByKey(LPCTSTR Key);
	afx_msg BOOL DeleteBkBarByKey(LPCTSTR Key);
	afx_msg long GetBkBarLineByKey(LPCTSTR olKey);
	afx_msg long GetBkBarPosInLine(LPCTSTR olKey, long llLineNo);
	afx_msg BSTR GetBkBarsByLine(long llLineNo);
	afx_msg BSTR GetBarsByLine(long llLineNo);
	afx_msg BSTR GetBarsByBkBar(LPCTSTR olBkBarKey);
	afx_msg void SetBarColor(LPCTSTR Key, long Color);
	afx_msg void SetBarDate(LPCTSTR Key, DATE Begin, DATE Ende, BOOL HandleOverlap);
	afx_msg void SetBkBarColor(LPCTSTR Key, long Color);
	afx_msg void SetBkBarDate(LPCTSTR Key, DATE Begin, DATE Ende);
	afx_msg void SetBarTextColor(LPCTSTR Key, long Color);
	afx_msg void SetBkBarTextColor(LPCTSTR Key, long Color);
	afx_msg void RefreshGanttLine(long LineNo);
	afx_msg void ScrollTo(DATE FromDate);
	afx_msg BOOL AddLineAt(long LineNo, LPCTSTR KeyValue, LPCTSTR TabString);
	afx_msg void SetLineSeparator(long LineNo, short LSHeight, long LSColor, short LSPen, short TotalHeight, short LSBegin);
	afx_msg void DeleteLineSeparator(long LineNo);
	afx_msg void SetStart2Start(LPCTSTR BarKeySrc, LPCTSTR BarKeyDest);
	afx_msg void SetStart2Finish(LPCTSTR BarKeySrc, LPCTSTR BarKeyDest);
	afx_msg void SetFinish2Start(LPCTSTR BarKeySrc, LPCTSTR BarKeyDest);
	afx_msg void SetFinish2Finish(LPCTSTR BarKeySrc, LPCTSTR BarKeyDest);
	afx_msg void DeleteX2X(LPCTSTR BarKeySrc, LPCTSTR BarKeyDest);
	afx_msg void SetBarLeftOutsideText(LPCTSTR Key, LPCTSTR Text);
	afx_msg void SetBarRightOutsideText(LPCTSTR Key, LPCTSTR Text);
	afx_msg void SetBarLeftOutsideTextColor(LPCTSTR Key, long Color);
	afx_msg void SetBarRightOutsideTextColor(LPCTSTR Key, long Color);
	afx_msg void SetBarSplitColor(LPCTSTR Key, long LeftColor, long RightColor);
	afx_msg void DeleteSplitColor(LPCTSTR Key);
	afx_msg void SetBarStyle(LPCTSTR Key, short BarStyle);
	afx_msg BOOL SetTimeMarker(DATE Time, long Color);
	afx_msg BOOL DeleteTimeMarker(DATE Time);
	afx_msg long GetTimeMarker(DATE Time);
	afx_msg void DeleteAllTimeMarkers();
	afx_msg void DecorationObjectCreate(LPCTSTR ID, LPCTSTR LocationList, LPCTSTR PixelList, LPCTSTR ColorList);
	afx_msg void DecorationObjectSetToBar(LPCTSTR ID, LPCTSTR BarKey);
	afx_msg void DecorationObjectSetToBkBar(LPCTSTR ID, LPCTSTR BkBarKey);
	afx_msg void DecorationObjectResetBar(LPCTSTR ID, LPCTSTR BarKey);
	afx_msg void DecorationObjectResetBkBar(LPCTSTR ID, LPCTSTR BkBarKey);
	afx_msg BSTR DecorationObjectsGetByBarKey(LPCTSTR BarKey);
	afx_msg BSTR DecorationObjectsGetByBkBarKey(LPCTSTR BkBarKey);
	afx_msg void AddSubBar(LPCTSTR MainBarKey, LPCTSTR Key, DATE StartTime, DATE EndTime, LPCTSTR BarText, long Shape, long BackColor, long TextColor, long SplitColor, long LeftColor, long RightColor, LPCTSTR LeftOutsideText, LPCTSTR RightOutsideText, long LeftTextColor, long RightTextColor);
	afx_msg void RemoveSubBar(LPCTSTR MainKey, LPCTSTR Key);
	afx_msg long GetLineNoByKey(LPCTSTR Key);
	afx_msg void TabSetGroupColumn(short ColNo, long AlternateColor);
	afx_msg void TabResetAllGroups();
	afx_msg void TabResetGroupsColumn(short ColNo);
	afx_msg void SortTab(LPCTSTR ColNo, BOOL bAscending, BOOL bCaseSensitive);
	afx_msg BSTR TabGetColumnValue(long LineNo, long ColNo);
	afx_msg BSTR TabGetLineKey(long LineNo);
	afx_msg BSTR TabGetLineValues(long LineNo);
	afx_msg void SetBarTriangles(LPCTSTR BarKey, LPCTSTR TriangleID, long LeftColor, long RightColor, BOOL FullTriangle);
	afx_msg DATE GetBarStartTime(LPCTSTR BarKey);
	afx_msg DATE GetBarEndTime(LPCTSTR BarKey);
	afx_msg DATE GetBkBarStartTime(LPCTSTR BarKey);
	afx_msg DATE GetBkBarEndTime(LPCTSTR BarKey);
	afx_msg VARIANT GetBarRecord(LPCTSTR BarKey);
	afx_msg VARIANT GetBarRecordsByLine(long LineNo);
	afx_msg VARIANT GetGaps(long LineNo);
	afx_msg VARIANT GetGapsForTimeframe(long LineNo, DATE FromDateTime, DATE ToDateTime);
	afx_msg long GetTotalBarCount();
	afx_msg void AddBarRecord(const VARIANT FAR& pBar);
	afx_msg void DeleteAllBars(BOOL withBackgroundBars);
	afx_msg long GetLineNoByBarKey(LPCTSTR Key);
	afx_msg BSTR GetGapsForTimeFrame2(long LineNo, DATE FromDateTime, DATE ToDateTime);
	afx_msg void ScrollToLine(long LineNo);
	afx_msg BOOL SaveToFile(LPCTSTR fileName);	
	afx_msg BOOL SaveToFileWithTime(LPCTSTR fileName, DATE fromDateTime, int toMinutes, int overlapMinutes);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()

	afx_msg void AboutBox();

// Event maps
	//{{AFX_EVENT(CUGanttCtrl)
	void FireActualLineNo(long ActualLineNo)
		{FireEvent(eventidActualLineNo,EVENT_PARAM(VTS_I4), ActualLineNo);}
	void FireTimeFromX(DATE Time)
		{FireEvent(eventidTimeFromX,EVENT_PARAM(VTS_DATE), Time);}
	void FireChangeBarDate(LPCTSTR Key, DATE Begin, DATE End)
		{FireEvent(eventidChangeBarDate,EVENT_PARAM(VTS_BSTR  VTS_DATE  VTS_DATE), Key, Begin, End);}
	void FireChangeBkBarDate(LPCTSTR Key, DATE Begin, DATE End)
		{FireEvent(eventidChangeBkBarDate,EVENT_PARAM(VTS_BSTR  VTS_DATE  VTS_DATE), Key, Begin, End);}
	void FireActualTimescaleDate(DATE TimescaleDate)
		{FireEvent(eventidActualTimescaleDate,EVENT_PARAM(VTS_DATE), TimescaleDate);}
	void FireOnDragEnter(LPCTSTR Key)
		{FireEvent(eventidOnDragEnter,EVENT_PARAM(VTS_BSTR), Key);}
	void FireOnDragLeave(LPCTSTR Key)
		{FireEvent(eventidOnDragLeave,EVENT_PARAM(VTS_BSTR), Key);}
	void FireOnDragOver(LPCTSTR Key, long LineNo)
		{FireEvent(eventidOnDragOver,EVENT_PARAM(VTS_BSTR  VTS_I4), Key, LineNo);}
	void FireOnDrop(LPCTSTR Key, long LineNo)
		{FireEvent(eventidOnDrop,EVENT_PARAM(VTS_BSTR  VTS_I4), Key, LineNo);}
	void FireEndOfSizeOrMove(LPCTSTR Key, long LineNo)
		{FireEvent(eventidEndOfSizeOrMove,EVENT_PARAM(VTS_BSTR  VTS_I4), Key, LineNo);}
	void FireOnLButtonDblClkBar(LPCTSTR Key, short nFlags)
		{FireEvent(eventidOnLButtonDblClkBar,EVENT_PARAM(VTS_BSTR  VTS_I2), Key, nFlags);}
	void FireOnLButtonDblClkBkBar(LPCTSTR Key, short nFlags)
		{FireEvent(eventidOnLButtonDblClkBkBar,EVENT_PARAM(VTS_BSTR  VTS_I2), Key, nFlags);}
	void FireOnLButtonDownBar(LPCTSTR Key, short nFlags)
		{FireEvent(eventidOnLButtonDownBar,EVENT_PARAM(VTS_BSTR  VTS_I2), Key, nFlags);}
	void FireOnLButtonDownBkBar(LPCTSTR Key, short nFlags)
		{FireEvent(eventidOnLButtonDownBkBar,EVENT_PARAM(VTS_BSTR  VTS_I2), Key, nFlags);}
	void FireOnRButtonDownBar(LPCTSTR Key, short nFlags)
		{FireEvent(eventidOnRButtonDownBar,EVENT_PARAM(VTS_BSTR  VTS_I2), Key, nFlags);}
	void FireOnRButtonDownBkBar(LPCTSTR Key, short nFlags)
		{FireEvent(eventidOnRButtonDownBkBar,EVENT_PARAM(VTS_BSTR  VTS_I2), Key, nFlags);}
	void FireOnRButtonDownBkGantt()
		{FireEvent(eventidOnRButtonDownBkGantt,EVENT_PARAM(VTS_NONE));}
	void FireOnLButtonDownBkGantt()
		{FireEvent(eventidOnLButtonDownBkGantt,EVENT_PARAM(VTS_NONE));}
	void FireOnLButtonDblClkBkGantt()
		{FireEvent(eventidOnLButtonDblClkBkGantt,EVENT_PARAM(VTS_NONE));}
	void FireOnLButtonDownTab(long Row, short Column, short nFlags)
		{FireEvent(eventidOnLButtonDownTab,EVENT_PARAM(VTS_I4  VTS_I2  VTS_I2), Row, Column, nFlags);}
	void FireOnLButtonDblClkTab(long Row, short Column, short nFlags)
		{FireEvent(eventidOnLButtonDblClkTab,EVENT_PARAM(VTS_I4  VTS_I2  VTS_I2), Row, Column, nFlags);}
	void FireOnRButtonDownTab(long Row, short Column, short nFlags)
		{FireEvent(eventidOnRButtonDownTab,EVENT_PARAM(VTS_I4  VTS_I2  VTS_I2), Row, Column, nFlags);}
	void FireOnDebug(LPCTSTR Output)
		{FireEvent(eventidOnDebug,EVENT_PARAM(VTS_BSTR), Output);}
	void FireOnLButtonDblClkPercentBar(LPCTSTR Key, short Percent, short nFlags)
		{FireEvent(eventidOnLButtonDblClkPercentBar,EVENT_PARAM(VTS_BSTR  VTS_I2  VTS_I2), Key, Percent, nFlags);}
	void FireOnLButtonDblClkPercentBkBar(LPCTSTR Key, short Percent, short nFlags)
		{FireEvent(eventidOnLButtonDblClkPercentBkBar,EVENT_PARAM(VTS_BSTR  VTS_I2  VTS_I2), Key, Percent, nFlags);}
	void FireOnMouseMoveBar(LPCTSTR Key)
		{FireEvent(eventidOnMouseMoveBar,EVENT_PARAM(VTS_BSTR), Key);}
	void FireOnMouseMoveBkBar(LPCTSTR Key)
		{FireEvent(eventidOnMouseMoveBkBar,EVENT_PARAM(VTS_BSTR), Key);}
	void FireOnLButtonDownSubBar(LPCTSTR MainBarKey, LPCTSTR SubBarKey, short nFlags)
		{FireEvent(eventidOnLButtonDownSubBar,EVENT_PARAM(VTS_BSTR  VTS_BSTR  VTS_I2), MainBarKey, SubBarKey, nFlags);}
	void FireOnLButtonDblClkSubBar(LPCTSTR MainBarKey, LPCTSTR SubBarKey, short nFlags)
		{FireEvent(eventidOnLButtonDblClkSubBar,EVENT_PARAM(VTS_BSTR  VTS_BSTR  VTS_I2), MainBarKey, SubBarKey, nFlags);}
	void FireOnRButtonDownSubBar(LPCTSTR MainBarKey, LPCTSTR SubBarKey, short nFlags)
		{FireEvent(eventidOnRButtonDownSubBar,EVENT_PARAM(VTS_BSTR  VTS_BSTR  VTS_I2), MainBarKey, SubBarKey, nFlags);}
	void FireOnHScroll(long ScrollPos)
		{FireEvent(eventidOnHScroll,EVENT_PARAM(VTS_I4), ScrollPos);}
	//}}AFX_EVENT
	DECLARE_EVENT_MAP()

// Dispatch and event IDs
public:
	void OnMouseHover(UINT wParam, UINT lParam);
	void OnRButtonDownTab(UINT nFlags, CPoint point);
	void OnLButtonDblClkTab(UINT nFlags, CPoint point);
	void OnLButtonDownTab(UINT nFlags, CPoint point);
	void OnRButtonDownGantt(UINT nFlags, CPoint point);
	void DrawGanttLineBmp();
	void UpdateAllRects();
	void UpdateSingleRect(CString olKey);
	void AddBarRect(CString BarKey);
	void DrawArrows(CBitmap &romBitmap);
	void HandleHorizontalScroller();
	CRect GetBarRectByKey(CString omKey);
	void DrawBkBarsOfLine(CBitmap &romBitmap, long lpLineNo, int ipCurrY);
	void DrawBarsOfLine(CBitmap &romBitmap, long lpLineNo, int ipCurrY);
	//MWO: 19.08.2002
	void DrawDecoObject(CDC *pdc, DECORATION_OBJECT *polDecoObj, CRect &olHRect);

	int GetTopOfLine(long LineNo);
	void OnLButtonDblClkGantt(UINT nFlags, CPoint point);
	int imMaxVisibleLines;
	void HandleLineHight (long lpLineNo);
	void HandleOverlapLevelOfVisibleLines();
	void HandleOverlapLevelOfLine (long lpLineNo);
	void HandleOverlapLevelOfBar (CString olKey, BOOL blTop);
	void HandleOverlapLevelOfLineAndTime(long lpLineNo, COleDateTime opTimeStart, COleDateTime opTimeEnd);
	int GetMaxOverlapLevel(long lpLineNo);
	void GetOverlappedBarsFromTime (long lpLineNo, COleDateTime opTime1, COleDateTime opTime2, CUIntArray &romBarNoList);
	bool DoBarsOverlap(BarData *ropBar1, BarData *ropBar2);
	void CalcTopOfLine(long lpLineNo);
	int GetNumberOfVisibleLines();
	void HandleCursorGantt(CPoint point, UINT nFlags, int imLeft, int imRight);
	void HighlightLine(CPoint point);
	void OnMouseMoveGantt(UINT nFlags, CPoint point);
	void OnLButtonDownGantt(UINT nFlags, CPoint point);
	long GetBarPosInLine(LPCTSTR opKey, long lpLineNo);
	long GetBarLineByKey(LPCTSTR opKey);
	COleDateTime GetCurrentTime();
	//MWO: 19.08.2002
	BarData* GetBarByKey(LPCTSTR opKey);
	BarData* GetBkBarByKey(LPCTSTR opKey);
	CString GetBarByMousePosition(CPoint point);
	CString GetBkBarByMousePosition(CPoint point);
	BarData* GetSubBarByMousePosition(CPoint point);

	void	GetVersionInfo();

	enum {
	//{{AFX_DISP_ID(CUGanttCtrl)
	dispidEnableDragDropBar = 1L,
	dispidEnableDragDropBkBar = 2L,
	dispidEnableDragDrop = 3L,
	dispidAutoScroll = 4L,
	dispidScrollDelay = 5L,
	dispidScrollInset = 6L,
	dispidScrollInterval = 7L,
	dispidBarFontName = 8L,
	dispidBarFontSize = 9L,
	dispidTabBodyFontSize = 10L,
	dispidTabHeaderFontBold = 11L,
	dispidTabBodyFontBold = 12L,
	dispidTabFontName = 13L,
	dispidBarFontBold = 14L,
	dispidSubbarStackCount = 15L,
	dispidUTCOffsetInHours = 16L,
	dispidAutoMoveSubBarsWithBar = 17L,
	dispidOverlappingRightBarTop = 18L,
	dispidTabColumns = 24L,
	dispidTabLines = 25L,
	dispidTabHeaderString = 26L,
	dispidTabHeaderLengthString = 27L,
	dispidTabLineHeight = 28L,
	dispidTabFontSize = 29L,
	dispidTabHeaderFontSize = 30L,
	dispidTabHeaderFontName = 31L,
	dispidTabBodyFontName = 32L,
	dispidTimeFrameFrom = 33L,
	dispidTimeFrameTo = 34L,
	dispidTimeScaleHeaderHeight = 35L,
	dispidSplitterPosition = 36L,
	dispidWith2ndScroller = 37L,
	dispidTimeScaleDuration = 38L,
	dispidTabHighlightColor = 39L,
	dispidResizeAreaWidth = 40L,
	dispidResizeMinimalWidth = 41L,
	dispidBarOverlapOffset = 42L,
	dispidBarNumberExpandLine = 43L,
	dispidWithHorizontalScroller = 44L,
	dispidArrowWidth = 45L,
	dispidArrowHead = 46L,
	dispidArrowColorNormal = 47L,
	dispidArrowColorSelected = 48L,
	dispidAutoSizeLineHeightToFit = 19L,
	dispidLifeStyle = 20L,
	dispidVersion = 21L,
	dispidBuildDate = 22L,
	dispidSplitterMovable = 23L,
	dispidTabSetHeaderText = 49L,
	dispidResetContent = 50L,
	dispidMakeNewLine = 51L,
	dispidTabSetLineColors = 52L,
	dispidGanttSetBackColor = 53L,
	dispidDeleteLine = 54L,
	dispidTabGetCurrentSelected = 55L,
	dispidTabGetLinesByTextColor = 56L,
	dispidTabGetLinesByBackColor = 57L,
	dispidTabUpdateValues = 58L,
	dispidAppendLine = 59L,
	dispidAddBkBarToLine = 60L,
	dispidRefreshArea = 61L,
	dispidAddBarToLine = 62L,
	dispidDeleteBarByKey = 63L,
	dispidDeleteBkBarByKey = 64L,
	dispidGetBkBarLineByKey = 65L,
	dispidGetBkBarPosInLine = 66L,
	dispidGetBkBarsByLine = 67L,
	dispidGetBarsByLine = 68L,
	dispidGetBarsByBkBar = 69L,
	dispidSetBarColor = 70L,
	dispidSetBarDate = 71L,
	dispidSetBkBarColor = 72L,
	dispidSetBkBarDate = 73L,
	dispidSetBarTextColor = 74L,
	dispidSetBkBarTextColor = 75L,
	dispidRefreshGanttLine = 76L,
	dispidScrollTo = 77L,
	dispidAddLineAt = 78L,
	dispidSetLineSeparator = 79L,
	dispidDeleteLineSeparator = 80L,
	dispidSetStart2Start = 81L,
	dispidSetStart2Finish = 82L,
	dispidSetFinish2Start = 83L,
	dispidSetFinish2Finish = 84L,
	dispidDeleteX2X = 85L,
	dispidSetBarLeftOutsideText = 86L,
	dispidSetBarRightOutsideText = 87L,
	dispidSetBarLeftOutsideTextColor = 88L,
	dispidSetBarRightOutsideTextColor = 89L,
	dispidSetBarSplitColor = 90L,
	dispidDeleteSplitColor = 91L,
	dispidSetBarStyle = 92L,
	dispidSetTimeMarker = 93L,
	dispidDeleteTimeMarker = 94L,
	dispidGetTimeMarker = 95L,
	dispidDeleteAllTimeMarkers = 96L,
	dispidDecorationObjectCreate = 97L,
	dispidDecorationObjectSetToBar = 98L,
	dispidDecorationObjectSetToBkBar = 99L,
	dispidDecorationObjectResetBar = 100L,
	dispidDecorationObjectResetBkBar = 101L,
	dispidDecorationObjectsGetByBarKey = 102L,
	dispidDecorationObjectsGetByBkBarKey = 103L,
	dispidAddSubBar = 104L,
	dispidRemoveSubBar = 105L,
	dispidGetLineNoByKey = 106L,
	dispidTabSetGroupColumn = 107L,
	dispidTabResetAllGroups = 108L,
	dispidTabResetGroupsColumn = 109L,
	dispidSortTab = 110L,
	dispidTabGetColumnValue = 111L,
	dispidTabGetLineKey = 112L,
	dispidTabGetLineValues = 113L,
	dispidSetBarTriangles = 114L,
	dispidGetBarStartTime = 115L,
	dispidGetBarEndTime = 116L,
	dispidGetBkBarStartTime = 117L,
	dispidGetBkBarEndTime = 118L,
	dispidGetBarRecord = 119L,
	dispidGetBarRecordsByLine = 120L,
	dispidGetGaps = 121L,
	dispidGetGapsForTimeframe = 122L,
	dispidGetTotalBarCount = 123L,
	dispidAddBarRecord = 124L,
	dispidDeleteAllBars = 125L,
	dispidGetLineNoByBarKey = 126L,
	dispidGetGapsForTimeFrame2 = 127L,
	dispidScrollToLine = 128L,
	dispidSaveToFile = 129L,
	dispidSaveToFileWithTime = 130L,
	eventidActualLineNo = 1L,
	eventidTimeFromX = 2L,
	eventidChangeBarDate = 3L,
	eventidChangeBkBarDate = 4L,
	eventidActualTimescaleDate = 5L,
	eventidOnDragEnter = 6L,
	eventidOnDragLeave = 7L,
	eventidOnDragOver = 8L,
	eventidOnDrop = 9L,
	eventidEndOfSizeOrMove = 10L,
	eventidOnLButtonDblClkBar = 11L,
	eventidOnLButtonDblClkBkBar = 12L,
	eventidOnLButtonDownBar = 13L,
	eventidOnLButtonDownBkBar = 14L,
	eventidOnRButtonDownBar = 15L,
	eventidOnRButtonDownBkBar = 16L,
	eventidOnRButtonDownBkGantt = 17L,
	eventidOnLButtonDownBkGantt = 18L,
	eventidOnLButtonDblClkBkGantt = 19L,
	eventidOnLButtonDownTab = 20L,
	eventidOnLButtonDblClkTab = 21L,
	eventidOnRButtonDownTab = 22L,
	eventidOnDebug = 23L,
	eventidOnLButtonDblClkPercentBar = 24L,
	eventidOnLButtonDblClkPercentBkBar = 25L,
	eventidOnMouseMoveBar = 26L,
	eventidOnMouseMoveBkBar = 27L,
	eventidOnLButtonDownSubBar = 28L,
	eventidOnLButtonDblClkSubBar = 29L,
	eventidOnRButtonDownSubBar = 30L,
	eventidOnHScroll = 31L,
	//}}AFX_DISP_ID
	};
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UGANTTCTL_H__6782E146_3223_11D4_996A_0000863DE95C__INCLUDED)
