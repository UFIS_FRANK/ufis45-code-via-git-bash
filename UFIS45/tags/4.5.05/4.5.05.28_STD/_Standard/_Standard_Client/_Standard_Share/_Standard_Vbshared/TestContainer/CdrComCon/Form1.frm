VERSION 5.00
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form Form1 
   Caption         =   "Ceda BC COM Connector"
   ClientHeight    =   7770
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   11610
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7770
   ScaleWidth      =   11610
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtTblExt 
      Height          =   285
      Left            =   120
      TabIndex        =   49
      Top             =   4380
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox txtHopo 
      Height          =   285
      Left            =   120
      TabIndex        =   48
      Top             =   4080
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.TextBox Text2 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2625
      Left            =   8220
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   47
      Top             =   2040
      Visible         =   0   'False
      Width           =   3135
   End
   Begin VB.TextBox txtVpto 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   10380
      Locked          =   -1  'True
      TabIndex        =   46
      Top             =   390
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.TextBox txtVpfr 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   9360
      Locked          =   -1  'True
      TabIndex        =   45
      Top             =   390
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.TextBox txtBgnOff 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   7770
      TabIndex        =   44
      Text            =   "0"
      Top             =   360
      Width           =   555
   End
   Begin VB.TextBox txtEndOff 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   8370
      TabIndex        =   43
      Text            =   "0"
      Top             =   360
      Width           =   555
   End
   Begin VB.CheckBox chkRead 
      Caption         =   "Read Data"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6540
      Style           =   1  'Graphical
      TabIndex        =   42
      Top             =   390
      Width           =   1185
   End
   Begin VB.Timer Timer2 
      Enabled         =   0   'False
      Interval        =   50
      Left            =   120
      Top             =   5760
   End
   Begin VB.CheckBox chkOld 
      Caption         =   "Old Style"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5310
      Style           =   1  'Graphical
      TabIndex        =   41
      Top             =   390
      Width           =   1185
   End
   Begin VB.TextBox txtPack 
      Alignment       =   1  'Right Justify
      Height          =   285
      Left            =   10380
      TabIndex        =   38
      Top             =   720
      Width           =   975
   End
   Begin VB.CheckBox chkClose 
      Caption         =   "Close All"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   9000
      Style           =   1  'Graphical
      TabIndex        =   37
      Top             =   60
      Width           =   1185
   End
   Begin VB.TextBox txtStart 
      Height          =   285
      Left            =   11040
      TabIndex        =   36
      Text            =   "20"
      Top             =   60
      Width           =   315
   End
   Begin VB.CheckBox chkStart 
      Caption         =   "Run"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   10470
      Style           =   1  'Graphical
      TabIndex        =   35
      Top             =   60
      Width           =   585
   End
   Begin VB.CheckBox chkLock 
      Caption         =   "Lock"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   34
      Top             =   4740
      Width           =   585
   End
   Begin VB.CheckBox chkDisconnect 
      Caption         =   "Disconnect"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   7770
      Style           =   1  'Graphical
      TabIndex        =   33
      Top             =   60
      Width           =   1185
   End
   Begin VB.CheckBox chkFilter 
      Caption         =   "Send Filter"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6540
      Style           =   1  'Graphical
      TabIndex        =   32
      Top             =   60
      Width           =   1185
   End
   Begin VB.CheckBox chkConnect 
      Caption         =   "Connect"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5310
      Style           =   1  'Graphical
      TabIndex        =   31
      Top             =   60
      Width           =   1185
   End
   Begin VB.TextBox txtData 
      Height          =   285
      Left            =   2400
      TabIndex        =   29
      Top             =   1710
      Width           =   8955
   End
   Begin VB.TextBox Text1 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2625
      Left            =   690
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   28
      Top             =   2040
      Width           =   10665
   End
   Begin VB.TextBox txtFilter 
      Height          =   285
      Left            =   690
      TabIndex        =   27
      Top             =   390
      Width           =   4455
   End
   Begin MSWinsockLib.Winsock WS1 
      Index           =   1
      Left            =   120
      Top             =   2640
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin VB.TextBox txtQueue 
      Height          =   285
      Left            =   4185
      TabIndex        =   25
      Top             =   60
      Width           =   975
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Left            =   120
      Top             =   5310
   End
   Begin VB.TextBox txtPort 
      Height          =   285
      Left            =   2400
      TabIndex        =   23
      Text            =   "3358"
      Top             =   60
      Width           =   975
   End
   Begin VB.TextBox txtTws 
      Height          =   285
      Left            =   5880
      TabIndex        =   21
      Top             =   720
      Width           =   1485
   End
   Begin VB.TextBox txtTwe 
      Height          =   285
      Left            =   8100
      TabIndex        =   19
      Top             =   720
      Width           =   1455
   End
   Begin VB.TextBox txtWks 
      Height          =   285
      Left            =   690
      TabIndex        =   10
      Top             =   1710
      Width           =   975
   End
   Begin VB.TextBox txtUsr 
      Height          =   285
      Left            =   690
      TabIndex        =   9
      Top             =   1380
      Width           =   975
   End
   Begin VB.TextBox txtWhe 
      Height          =   285
      Left            =   2400
      TabIndex        =   8
      Top             =   1380
      Width           =   8955
   End
   Begin VB.TextBox txtFld 
      Height          =   285
      Left            =   2400
      TabIndex        =   7
      Top             =   1050
      Width           =   8955
   End
   Begin VB.TextBox txtBcNum 
      Height          =   285
      Left            =   690
      TabIndex        =   6
      Top             =   1050
      Width           =   975
   End
   Begin VB.TextBox txtTbl 
      Height          =   285
      Left            =   4170
      TabIndex        =   5
      Top             =   720
      Width           =   975
   End
   Begin VB.TextBox txtCmd 
      Height          =   285
      Left            =   2400
      TabIndex        =   4
      Top             =   720
      Width           =   975
   End
   Begin VB.TextBox txtTot 
      Height          =   285
      Left            =   690
      TabIndex        =   3
      Top             =   720
      Width           =   975
   End
   Begin VB.TextBox txtServer 
      Height          =   285
      Left            =   690
      TabIndex        =   1
      Text            =   "sin1"
      Top             =   60
      Width           =   975
   End
   Begin TABLib.TAB TAB1 
      Height          =   2715
      Left            =   690
      TabIndex        =   0
      Top             =   4740
      Width           =   10665
      _Version        =   65536
      _ExtentX        =   18812
      _ExtentY        =   4789
      _StockProps     =   64
   End
   Begin MSWinsockLib.Winsock WS1 
      Index           =   0
      Left            =   120
      Top             =   2160
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin MSWinsockLib.Winsock WS1 
      Index           =   2
      Left            =   120
      Top             =   3120
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin MSWinsockLib.Winsock WS1 
      Index           =   3
      Left            =   120
      Top             =   3600
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin VB.Label lblFilter 
      Caption         =   "Filter:"
      Height          =   285
      Left            =   60
      TabIndex        =   40
      Top             =   420
      Width           =   585
   End
   Begin VB.Label Label6 
      Caption         =   "PACK:"
      Height          =   255
      Left            =   9750
      TabIndex        =   39
      Top             =   750
      Width           =   585
   End
   Begin VB.Label Label4 
      Caption         =   "DATA:"
      Height          =   255
      Left            =   1770
      TabIndex        =   30
      Top             =   1740
      Width           =   585
   End
   Begin VB.Label Label16 
      Caption         =   "Queue:"
      Height          =   255
      Left            =   3540
      TabIndex        =   26
      Top             =   90
      Width           =   555
   End
   Begin VB.Label Label15 
      Caption         =   "Port:"
      Height          =   255
      Left            =   1770
      TabIndex        =   24
      Top             =   90
      Width           =   585
   End
   Begin VB.Label Label14 
      Caption         =   "TWS:"
      Height          =   255
      Left            =   5310
      TabIndex        =   22
      Top             =   750
      Width           =   585
   End
   Begin VB.Label Label12 
      Caption         =   "TWE:"
      Height          =   255
      Left            =   7530
      TabIndex        =   20
      Top             =   750
      Width           =   585
   End
   Begin VB.Label Label11 
      Caption         =   "WKS:"
      Height          =   255
      Left            =   60
      TabIndex        =   18
      Top             =   1740
      Width           =   585
   End
   Begin VB.Label Label10 
      Caption         =   "USR:"
      Height          =   255
      Left            =   60
      TabIndex        =   17
      Top             =   1410
      Width           =   585
   End
   Begin VB.Label Label9 
      Caption         =   "WHE:"
      Height          =   255
      Left            =   1770
      TabIndex        =   16
      Top             =   1410
      Width           =   585
   End
   Begin VB.Label Label8 
      Caption         =   "FLD:"
      Height          =   255
      Left            =   1770
      TabIndex        =   15
      Top             =   1080
      Width           =   585
   End
   Begin VB.Label Label7 
      Caption         =   "BCNUM"
      Height          =   255
      Left            =   60
      TabIndex        =   14
      Top             =   1080
      Width           =   585
   End
   Begin VB.Label Label5 
      Caption         =   "TBL:"
      Height          =   255
      Left            =   3540
      TabIndex        =   13
      Top             =   750
      Width           =   585
   End
   Begin VB.Label Label3 
      Caption         =   "CMD:"
      Height          =   255
      Left            =   1770
      TabIndex        =   12
      Top             =   750
      Width           =   585
   End
   Begin VB.Label Label2 
      Caption         =   "TOT:"
      Height          =   255
      Left            =   60
      TabIndex        =   11
      Top             =   750
      Width           =   585
   End
   Begin VB.Label Label1 
      Caption         =   "Server:"
      Height          =   255
      Left            =   60
      TabIndex        =   2
      Top             =   90
      Width           =   585
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim TotalBuffer As String
Dim CdrBuffer As String
Dim LastError As String
Dim MyApplQueue As String
Dim CdrTransAction As Integer
Dim LastBcNum As Long
Dim CloseAllSent As Boolean
Dim OutMsg As String

Private Sub chkClose_Click()
    Dim TcpAdr As String
    Dim TcpPort As String
    If chkClose.Value = 1 Then
        chkClose.BackColor = LightGreen
        If WS1(2).State <> sckClosed Then WS1(2).Close
        If MyApplQueue <> "" Then
            CdrTransAction = 2
            TcpAdr = txtServer.Text
            TcpPort = "3357"
            WS1(2).RemoteHost = TcpAdr
            WS1(2).RemotePort = TcpPort
            WS1(2).Connect TcpAdr, TcpPort
        End If
        chkClose.Value = 0
    Else
        chkClose.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkConnect_Click()
    If chkConnect.Value = 1 Then
        chkConnect.BackColor = LightGreen
        chkDisconnect.Value = 0
        Command2_Click
        chkFilter.Enabled = True
        chkDisconnect.Enabled = True
        chkClose.Enabled = True
        'chkRead.Enabled = True
        chkOld.Enabled = True
        chkConnect.Caption = "Connected"
    Else
        chkDisconnect.Value = 1
        chkConnect.Caption = "Connect"
        chkFilter.Enabled = False
        chkDisconnect.Enabled = False
        chkClose.Enabled = False
        chkRead.Enabled = False
        chkOld.Enabled = False
        chkConnect.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkDisconnect_Click()
    Dim TcpAdr As String
    Dim TcpPort As String
    If chkDisconnect.Value = 1 Then
        chkDisconnect.BackColor = LightGreen
        chkConnect.Value = 0
        If WS1(1).State <> sckClosed Then WS1(1).Close
        If MyApplQueue <> "" Then
            CdrTransAction = 0
            TcpAdr = txtServer.Text
            TcpPort = txtPort.Text
            WS1(1).RemoteHost = TcpAdr
            WS1(1).RemotePort = TcpPort
            WS1(1).Connect TcpAdr, TcpPort
        End If
        chkDisconnect.Value = 0
    Else
        chkDisconnect.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkFilter_Click()
    If chkFilter.Value = 1 Then
        chkFilter.BackColor = LightGreen
        Command1_Click
        chkFilter.Value = 0
    Else
        chkFilter.BackColor = vbButtonFace
    End If
End Sub

Private Sub Command2_Click()
    Dim TcpAdr As String
    Dim TcpPort As String
    If WS1(0).State <> sckClosed Then WS1(0).Close
    txtQueue.Text = ""
    TotalBuffer = ""
    CdrTransAction = -1
    TcpAdr = txtServer.Text
    TcpPort = txtPort.Text
    WS1(0).RemoteHost = TcpAdr
    WS1(0).RemotePort = TcpPort
    WS1(0).Connect TcpAdr, TcpPort
End Sub

Private Sub Command1_Click()
    Dim TcpAdr As String
    Dim TcpPort As String
    If WS1(1).State <> sckClosed Then WS1(1).Close
    If MyApplQueue <> "" Then
        CdrTransAction = 1
        TcpAdr = txtServer.Text
        TcpPort = txtPort.Text
        WS1(1).RemoteHost = TcpAdr
        WS1(1).RemotePort = TcpPort
        WS1(1).Connect TcpAdr, TcpPort
    End If
End Sub

Private Sub chkLock_Click()
    If chkLock.Value = 1 Then
        chkLock.BackColor = LightGreen
        TAB1.LockScroll True
    Else
        TAB1.LockScroll False
        chkLock.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkOld_Click()
    If chkOld.Value = 1 Then
        chkOld.BackColor = LightGreen
        Text2.Visible = True
        Text1.Width = 7485
    Else
        Text1.Width = 10665
        Text2.Visible = False
        chkOld.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkRead_Click()
    Dim TcpAdr As String
    Dim TcpPort As String
    If chkRead.Value = 1 Then
        chkRead.BackColor = vbYellow
        If WS1(3).State <> sckClosed Then WS1(3).Close
        If MyApplQueue <> "" Then
            InitFromTo
            InitCedaFields "AFT"
            BuildOutMsg
            CdrBuffer = ""
            Text2.Text = ""
            CdrTransAction = 9
            Me.Caption = "Ceda BC COM Connector <READING: " & chkRead.Caption & ">"
            TcpAdr = txtServer.Text
            TcpPort = "3357"
            WS1(3).RemoteHost = TcpAdr
            WS1(3).RemotePort = TcpPort
            WS1(3).Connect TcpAdr, TcpPort
        End If
    Else
        chkRead.Caption = "Read Data"
        Me.Caption = "Ceda BC COM Connector"
        chkRead.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkStart_Click()
    Dim i As Integer
    Dim cnt As Integer
    Dim tmpArgs As String
    Dim tmpStyle As String
    Dim tmpFilter As String
    Dim myAppl As String
    If chkStart.Value = 1 Then
        tmpStyle = "{=STYLE=}" & CStr(chkOld.Value) & "," & txtBgnOff.Text & "," & txtEndOff.Text
        tmpFilter = "{=FILTER=}" & txtFilter.Text
        tmpArgs = tmpStyle & tmpFilter
        cnt = Val(txtStart.Text)
        If InStr(Command, "CHILD") = 0 Then cnt = cnt - 1
        For i = 1 To cnt
            myAppl = App.Path & "\" & App.EXEName & ".exe CHILD " & tmpArgs
            Shell myAppl, vbNormalFocus
            chkStart.Value = 0
        Next
        'chkStart.Visible = False
        'txtStart.Visible = False
    End If
End Sub

Private Sub Form_Activate()
    Static IsActive As Boolean
    Dim tmpStyle As String
    Dim tmpData As String
    If Not IsActive Then
        chkConnect.Value = 1
        If InStr(Command, "CHILD") > 0 Then
            GetKeyItem tmpStyle, Command, "{=STYLE=}", "{="
            tmpData = GetItem(tmpStyle, 1, ",")
            chkOld.Value = Val(tmpData)
            tmpData = GetItem(tmpStyle, 2, ",")
            If tmpData = "" Then tmpData = "0"
            txtBgnOff.Text = tmpData
            tmpData = GetItem(tmpStyle, 3, ",")
            If tmpData = "" Then tmpData = "0"
            txtEndOff.Text = tmpData
        End If
        IsActive = True
    End If
End Sub

Private Sub Form_Load()
    txtHopo.Text = GetIniEntry("", "GLOBAL", "", "HOMEAIRPORT", "???")
    txtTblExt.Text = GetIniEntry("", "GLOBAL", "", "TABLEEXTENSION", "TAB")
    txtServer.Text = GetIniEntry("", "GLOBAL", "", "HOSTNAME", "")
    
    chkFilter.Enabled = False
    chkDisconnect.Enabled = False
    TAB1.ResetContent
    TAB1.HeaderString = "C,Received BroadCast KeyItem List"
    TAB1.HeaderLengthString = "100,1000"
    TAB1.SetColumnBoolProperty 0, "1", "0"
    TAB1.SetFieldSeparator Chr(15)
    TAB1.SetHeaderFont 17, False, False, False, 0, "Courier New"
    TAB1.AutoSizeByHeader = True
    TAB1.AutoSizeColumns
    LastBcNum = -1
End Sub

Private Sub Form_Resize()
    Dim newSize As Long
    newSize = Me.ScaleHeight - TAB1.Top - 240
    If newSize > 600 Then TAB1.Height = newSize
End Sub

Private Sub lblFilter_DblClick()
    Dim tmpFilter As String
    tmpFilter = "DRRTAB;DRATAB;SPRTAB;SWGTAB;RUETAB;RPQTAB;UPDDEM;CCATAB;RUDTAB;BLKTAB;RELDEM;SBCXXX;UPDJOB;JODTAB;DLGTAB;VALTAB;AFLEND;AFTTAB;RELJOB;LOATAB;JOBTAB;REQTAB;PFCTAB;SGRTAB;DEMTAB;PERTAB;DELTAB;DRGTAB;REMTAB;EQUTAB;RELDRR;RPFTAB;SGMTAB;DRDTAB;EQATAB;PAXTAB;DRSTAB"
    txtFilter.Text = tmpFilter
End Sub

Private Sub TAB1_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim tmpData As String
    tmpData = TAB1.GetColumnValue(LineNo, 1)
    ShowBcData tmpData
End Sub

Private Sub Timer1_Timer()
    Dim tmpTag As String
    Timer1.Enabled = False
    tmpTag = Timer1.Tag
    Select Case tmpTag
        Case ""
            chkConnect.Value = 0
            Timer1.Tag = "DOWN"
        Case "DOWN"
            End
        Case Else
    End Select
End Sub

Private Sub Timer2_Timer()
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim ChkLine As Long
    Dim tmpChk As String
    Dim tmpMsg As String
    Dim tmpTable As String
    Dim BreakOut As Boolean
    Timer2.Enabled = False
    If chkRead.Value = 0 Then
        MaxLine = TAB1.GetLineCount - 1
        ChkLine = -1
        BreakOut = False
        CurLine = MaxLine
        While (CurLine >= 0) And (BreakOut = False)
            tmpChk = TAB1.GetColumnValue(CurLine, 0)
            If tmpChk = "0" Then
                tmpMsg = TAB1.GetColumnValue(CurLine, 1)
                GetKeyItem tmpTable, tmpMsg, "{=TBL=}", "{="
                If tmpTable = "" Then tmpTable = "......"
                If InStr("TIMERS,UPDDEM,RELDEM,UPDJOB,RELJOB,RELDRR", tmpTable) > 0 Then
                    TAB1.SetLineColor CurLine, vbBlack, vbYellow
                    Timer2.Tag = "WAIT," & tmpTable
                    chkRead.Caption = tmpTable
                    chkRead.Refresh
                    chkRead.Value = 1
                    BreakOut = True
                End If
                TAB1.SetColumnValue CurLine, 0, "1"
            End If
            CurLine = CurLine - 1
        Wend
    End If
    TAB1.Refresh
End Sub

Private Sub txtBgnOff_Change()
    InitFromTo
End Sub

Private Sub txtEndOff_Change()
    InitFromTo
End Sub

Private Sub WS1_Close(Index As Integer)
    If WS1(Index).State <> sckClosed Then WS1(Index).Close
    If Index = 0 Then MyApplQueue = ""
End Sub

Private Sub WS1_Connect(Index As Integer)
    Dim tmpTxt As String
    Select Case Index
        Case 0
            TotalBuffer = ""
            MyApplQueue = ""
            LastBcNum = -1
            SendDataToBcOut Index, "BCOUT", ""
        Case 1
            If MyApplQueue <> "" Then
                Select Case CdrTransAction
                    Case 0
                        SendDataToBcOut Index, "BCOFF", ""
                        If Timer1.Tag = "DOWN" Then
                            Timer1.Enabled = True
                        End If
                    Case 1
                        tmpTxt = txtFilter.Text
                        tmpTxt = Replace(tmpTxt, ",", ";", 1, -1, vbBinaryCompare)
                        tmpTxt = Replace(tmpTxt, " ", "", 1, -1, vbBinaryCompare)
                        tmpTxt = UCase(tmpTxt)
                        SendDataToBcOut Index, "BCKEY", tmpTxt
                        txtFilter.Text = tmpTxt
                        If tmpTxt <> "" Then
                            lblFilter.FontBold = True
                        Else
                            lblFilter.FontBold = False
                        End If
                    Case 2
                    Case Else
                End Select
            End If
        Case 2
            If MyApplQueue <> "" Then
                Select Case CdrTransAction
                    Case 0
                    Case 1
                    Case 2
                        SendDataToBcOut Index, "SBC", "{=TBL=}SYSTEM{=FLD=}BCCOM TEST CLIENT{=WHE=}CLOSEALL"
                        CloseAllSent = True
                    Case Else
                End Select
            End If
        Case 3
            Select Case CdrTransAction
                Case 9
                    WS1(3).SendData OutMsg
                Case Else
            End Select
        
        Case Else
    End Select
End Sub

Private Sub WS1_DataArrival(Index As Integer, ByVal bytesTotal As Long)
    Dim GotMessage As Boolean
    Dim Data As String
    Dim Cmd As String
    Dim tmpData As String
    Dim datalen As Integer
    Dim TotalLen As Long
    Dim CurBcNum As Long
    Dim ModId As String
    Dim Tot As String

    WS1(Index).GetData Data, vbString
    If Index = 0 Then
        TotalBuffer = TotalBuffer & Data
        GotMessage = True
        While GotMessage
            GotMessage = False
            If Len(TotalBuffer) >= 16 Then
                GetKeyItem Tot, TotalBuffer, "{=TOT=}", "{="
                TotalLen = Val(Tot)
                If Len(TotalBuffer) >= TotalLen Then
                    GotMessage = True
                    Data = Left(TotalBuffer, TotalLen)
                    TotalBuffer = Mid(TotalBuffer, TotalLen + 1)
        
                    GetKeyItem ModId, Data, "{=QUE=}", "{="
                    If ModId <> "" Then
                        MyApplQueue = ModId
                        txtQueue.Text = ModId
                    End If
                    
                    If chkOld.Value = 1 Then
                        TAB1.InsertTextLineAt 0, "0" & Chr(15) & Data, False
                    Else
                        TAB1.InsertTextLineAt 0, "1" & Chr(15) & Data, False
                    End If
                    TAB1.DeleteLine 500
                    TAB1.AutoSizeColumns
                    TAB1.Refresh
                    If (chkOld.Value = 1) And (Timer2.Tag = "") Then Timer2.Enabled = True
                    
                    If chkLock.Value = 0 Then ShowBcData (Data)
                    
                    GetKeyItem Cmd, Data, "{=CMD=}", "{="
                    If Cmd = "CLO" Then
                        Data = "{=ACK=}ACK"
                        WS1(Index).SendData Data
                    End If
                    If Cmd = "BCOUT" Then
                        GetKeyItem tmpData, Command, "{=FILTER=}", "{="
                        txtFilter.Text = tmpData
                        If tmpData <> "" Then chkFilter.Value = 1
                    End If
                    
                    GetKeyItem tmpData, Data, "{=TBL=}", "{="
                    If tmpData = "SYSTEM" Then
                        GetKeyItem tmpData, Data, "{=WHE=}", "{="
                        If tmpData = "CLOSEALL" Then
                            Timer1.Interval = 1000
                            Timer1.Enabled = True
                        End If
                    End If
                End If
            End If
        Wend
    End If
    
    If Index = 2 Then
        If CloseAllSent Then
            Data = "{=ACK=}"
            WS1(2).SendData Data
        End If
    End If
    
    If Index = 3 Then
        CdrBuffer = CdrBuffer & Data
        GotMessage = False
        If Len(CdrBuffer) >= 16 Then
            GetKeyItem Tot, CdrBuffer, "{=TOT=}", "{="
            TotalLen = Val(Tot)
            If Len(CdrBuffer) >= TotalLen Then
                GotMessage = True
                Data = Left(CdrBuffer, TotalLen)
                CdrBuffer = Mid(CdrBuffer, TotalLen + 1)
            End If
        End If
        If GotMessage Then
            GetKeyItem tmpData, Data, "{=DAT=}", "{="
            Text2.Text = Replace(tmpData, vbLf, vbNewLine, 1, -1, vbBinaryCompare)
            Data = "{=ACK=}"
            WS1(3).SendData Data
            chkRead.Value = 0
            If chkOld.Value = 1 Then
                Timer2.Tag = ""
                Timer2.Enabled = True
            End If
            CdrBuffer = ""
        End If
    End If
End Sub

Private Function GetKeyItem(ByRef rResult, ByRef rString, ByVal vStartSep, ByVal vEndSep) As Boolean
    Dim llPosStart As Long
    Dim llPosEnd As Long
    GetKeyItem = False
    rResult = ""
    llPosStart = InStr(1, rString, vStartSep, vbTextCompare)
    If llPosStart > 0 Then
        GetKeyItem = True
        llPosStart = llPosStart + Len(vStartSep)
        llPosEnd = InStr(llPosStart, rString, vEndSep, vbTextCompare)
        If llPosEnd > 0 Then
            rResult = Mid(rString, llPosStart, (llPosEnd - llPosStart))
        Else
            rResult = Right(rString, Len(rString) - llPosStart + 1)
        End If
    End If
End Function
                    
Private Sub WS1_Error(Index As Integer, ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
    Dim tmpMsg As String
    tmpMsg = "WS" & CStr(Index) & " ERR:" & CStr(Number) & vbNewLine & Description
    Text1.Text = tmpMsg
    Select Case Index
        Case 1
            If Timer1.Tag = "DOWN" Then chkDisconnect.Value = 1
        Case Else
    End Select
End Sub

Private Sub SendDataToBcOut(Index As Integer, BcCmd As String, Data As String)
    Dim tmpStr As String
    Dim tmpLen As Long
    tmpStr = Space(16)
    tmpStr = tmpStr + "{=CMD=}" + BcCmd
    If MyApplQueue <> "" Then tmpStr = tmpStr + "{=QUE=}" + MyApplQueue
    If Data <> "" Then tmpStr = tmpStr + "{=DAT=}" + Data
    tmpLen = Len(tmpStr)
    Mid(tmpStr, 1, 16) = Left("{=TOT=}" & CStr(tmpLen) & "                  ", 16)
    WS1(Index).SendData tmpStr
End Sub

Private Function GetHexTcpIp() As String
    Dim MyTcpIpAdr As String
    Dim HexTcpAdr As String
    Dim tmpdat As String
    Dim tmpVal As Integer
    Dim ItmNbr As Integer
    
    MyTcpIpAdr = WS1(0).LocalIP
    HexTcpAdr = ""
    For ItmNbr = 1 To 4
        tmpVal = Val(GetItem(MyTcpIpAdr, ItmNbr, "."))
        tmpdat = Hex(tmpVal)
        HexTcpAdr = HexTcpAdr + Right("00" + tmpdat, 2)
    Next
    GetHexTcpIp = HexTcpAdr
End Function

Private Sub ShowBcData(Data As String)
    Dim tmpVal As String
    Dim tmpOff As Integer
    
    GetKeyItem tmpVal, Data, "{=TOT=}", "{="
    txtTot.Text = tmpVal
    GetKeyItem tmpVal, Data, "{=PACK=}", "{="
    txtPack.Text = tmpVal
    GetKeyItem tmpVal, Data, "{=CMD=}", "{="
    txtCmd.Text = tmpVal
    GetKeyItem tmpVal, Data, "{=TBL=}", "{="
    txtTbl.Text = tmpVal
    GetKeyItem tmpVal, Data, "{=FLD=}", "{="
    txtFld.Text = tmpVal
    GetKeyItem tmpVal, Data, "{=WHE=}", "{="
    txtWhe.Text = tmpVal
    GetKeyItem tmpVal, Data, "{=USR=}", "{="
    txtUsr.Text = tmpVal
    GetKeyItem tmpVal, Data, "{=WKS=}", "{="
    txtWks.Text = tmpVal
    GetKeyItem tmpVal, Data, "{=TWS=}", "{="
    txtTws.Text = tmpVal
    GetKeyItem tmpVal, Data, "{=TWE=}", "{="
    txtTwe.Text = tmpVal
    GetKeyItem tmpVal, Data, "{=BCNUM=}", "{="
    txtBcNum.Text = tmpVal
    GetKeyItem tmpVal, Data, "{=DAT=}", "{="
    Text1.Text = Replace(tmpVal, vbLf, vbNewLine, 1, -1, vbBinaryCompare)
    tmpOff = InStr(tmpVal, "{=ATTACH=}")
    If tmpOff > 0 Then tmpVal = Left(tmpVal, tmpOff - 1)
    txtData.Text = tmpVal
    
End Sub

Private Sub InitFromTo()
    Dim FrameVal
    Dim hours As Integer
    hours = Abs(Val(txtBgnOff.Text))
    FrameVal = DateAdd("h", -hours, Now)
    txtVpfr.Text = Format(FrameVal, "yyyymmddhh")
    hours = Abs(Val(txtEndOff.Text))
    FrameVal = DateAdd("h", hours, Now)
    txtVpto.Text = Format(FrameVal, "yyyymmddhh")
End Sub

Private Sub InitCedaFields(ForWhat As String)
    Dim tmpTxt As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    txtWks.Text = GetHexTcpIp()
    txtUsr.Text = "CdrTest"
    txtTws.Text = CStr(App.hInstance)
    txtTwe.Text = txtHopo.Text & "," & txtTblExt.Text & ",CdrAlive"
    Select Case ForWhat
        Case "AFT"
            txtTbl.Text = "AFTTAB"
            txtCmd.Text = "GFR"
            txtFld.Text = UCase("Urno,Fdat,Flno,Alc2,Alc3,Fltn,Flns,Adid,Org3,Des3,Des4,Via3,Vian,Vial,Gta1,Gta2,Gtd1,Gtd2,Regn,Act3,Nose,Stod,Stoa,Etdi,Etai,Tifd,Tifa,Land,Slot,Airb,Onbl,Onbe,Ofbl,Tmoa,Tisd,Tisa,Styp,Dcd1,Dtd1,Dcd2,Dtd2,Nxti,Iskd,Ftyp,Psta,Pstd,Paxt,Pax2,Pax3,Lstu,Rkey,Stev,Skey,Jfno,Jcnt,Act5,Ttyp,Paxt,Cgot,Bagn,Mail,Blt1,Baz1,Ddlf,Bagw,Hapx,Hara")
            tmpTxt = "((ADID = 'A' AND TIFA BETWEEN 'VPFR' AND 'VPTO') OR (ADID = 'D' AND TIFD BETWEEN 'VPFR' AND 'VPTO'))"
            tmpVpfr = txtVpfr & "0000"
            tmpVpto = txtVpto & "5959"
            tmpTxt = Replace(tmpTxt, "VPFR", tmpVpfr, 1, -1, vbBinaryCompare)
            tmpTxt = Replace(tmpTxt, "VPTO", tmpVpto, 1, -1, vbBinaryCompare)
            txtWhe.Text = tmpTxt
            'txtPack.Text = "1000"
        Case Else
    End Select
End Sub

Private Sub BuildOutMsg()
    Dim tmpCdrBuf As String
    Dim tmpLen As Integer
    Dim tmpTot As String
    tmpCdrBuf = ""
    tmpCdrBuf = tmpCdrBuf & "{=CMD=}" & txtCmd.Text
    tmpCdrBuf = tmpCdrBuf & "{=WKS=}" & txtWks.Text
    tmpCdrBuf = tmpCdrBuf & "{=USR=}" & txtUsr.Text
    'tmpCdrBuf = tmpCdrBuf & "{=PACK=}" & txtPack.Text
    tmpCdrBuf = tmpCdrBuf & "{=TBL=}" & txtTbl.Text
    tmpCdrBuf = tmpCdrBuf & "{=FLD=}" & txtFld.Text
    tmpCdrBuf = tmpCdrBuf & "{=WHE=}WHERE " & txtWhe.Text
    tmpLen = Len(tmpCdrBuf) + 16
    tmpTot = "{=TOT=}" & Right("000000000" & CStr(tmpLen), 9)
    OutMsg = tmpTot & tmpCdrBuf
    txtPack.Tag = txtPack.Text
End Sub


