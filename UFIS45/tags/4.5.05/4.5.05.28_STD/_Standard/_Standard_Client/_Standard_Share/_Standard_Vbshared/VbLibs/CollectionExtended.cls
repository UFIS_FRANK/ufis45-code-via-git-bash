VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CollectionExtended"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Dim oCollection As Collection

Private Sub Class_Initialize()
    Set oCollection = New Collection
End Sub

Private Sub Class_Terminate()
    Set oCollection = Nothing
End Sub

Public Sub Add(ByVal Item, Optional ByVal Key, Optional ByVal Before, Optional ByVal After)
    oCollection.Add Item, Key, Before, After
End Sub

Public Function Count() As Long
    Count = oCollection.Count
End Function

Public Function Exists(ByVal Index) As Boolean
    Dim oColl
    
    On Error GoTo ErrHandle1
    
    If oCollection.Count = 0 Then
        Exists = False
    Else
        oColl = oCollection.Item(Index)
        
        If IsEmpty(oColl) Then
            On Error GoTo ErrHandle2
            Set oColl = oCollection.Item(Index)
        End If
        
        Exists = True
    End If
    
    Exit Function
    
ErrHandle1:
    Resume Next
    
ErrHandle2:
    Exists = False
End Function

Public Function Item(ByVal Index)
    On Error GoTo ErrHandle1

    If IsObject(oCollection.Item(Index)) Then
        Set Item = oCollection.Item(Index)
    Else
        Item = oCollection.Item(Index)
    End If
    
    Exit Function
    
ErrHandle1:
    Resume Next
    
ErrHandle2:
End Function

Public Sub Remove(ByVal Index)
    oCollection.Remove Index
End Sub

Public Sub RemoveAll()
    Do While oCollection.Count > 0
        oCollection.Remove 1
    Loop
End Sub
