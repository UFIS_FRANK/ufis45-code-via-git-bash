VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "UFISLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private m_UserName As String
Private m_ApplicationName As String
Private m_VersionString As String
Private m_InfoCaption As String
Private m_InfoButtonVisible As Boolean
Private m_InfoUfisVersion As String
Private m_InfoAppVersion As String
Private m_InfoCopyright As String
Private m_InfoAAT As String
Private m_UserNameLCase As Boolean
Private m_LoginAttempts  As Integer
Private m_RegisterApplicationString As String

Private m_oAatLogin As Object
Private m_oUfisCom As Object

Public Property Get UserName() As String
    UserName = m_UserName
End Property

Public Property Let ApplicationName(ByVal sApplicationName As String)
    m_ApplicationName = sApplicationName
End Property

Public Property Get ApplicationName() As String
    ApplicationName = m_ApplicationName
End Property

Public Property Let VersionString(ByVal sVersionString As String)
    m_VersionString = sVersionString
End Property

Public Property Get VersionString() As String
    VersionString = m_VersionString
End Property

Public Property Let InfoCaption(ByVal sInfoCaption As String)
    m_InfoCaption = sInfoCaption
End Property

Public Property Get InfoCaption() As String
    InfoCaption = m_InfoCaption
End Property

Public Property Let InfoButtonVisible(ByVal bInfoButtonVisible As Boolean)
    m_InfoButtonVisible = bInfoButtonVisible
End Property

Public Property Get InfoButtonVisible() As Boolean
    InfoButtonVisible = m_InfoButtonVisible
End Property

Public Property Let InfoUfisVersion(ByVal sInfoUfisVersion As String)
    m_InfoUfisVersion = sInfoUfisVersion
End Property

Public Property Get InfoUfisVersion() As String
    InfoUfisVersion = m_InfoUfisVersion
End Property

Public Property Let InfoAppVersion(ByVal sInfoAppVersion As String)
    m_InfoAppVersion = sInfoAppVersion
End Property

Public Property Get InfoAppVersion() As String
    InfoAppVersion = m_InfoAppVersion
End Property

Public Property Let InfoCopyright(ByVal sInfoCopyright As String)
    m_InfoCopyright = sInfoCopyright
End Property

Public Property Get InfoCopyright() As String
    InfoCopyright = m_InfoCopyright
End Property

Public Property Let InfoAAT(ByVal sInfoAAT As String)
    m_InfoAAT = sInfoAAT
End Property

Public Property Get InfoAAT() As String
    InfoAAT = m_InfoAAT
End Property

Public Property Let UserNameLCase(ByVal bUserNameLCase As Boolean)
    m_UserNameLCase = bUserNameLCase
End Property

Public Property Get UserNameLCase() As Boolean
    UserNameLCase = m_UserNameLCase
End Property

Public Property Let LoginAttempts(ByVal iLoginAttempts As Integer)
    m_LoginAttempts = iLoginAttempts
End Property

Public Property Get LoginAttempts() As Integer
    LoginAttempts = m_LoginAttempts
End Property

Public Property Let RegisterApplicationString(ByVal sRegisterApplicationString As String)
    m_RegisterApplicationString = sRegisterApplicationString
End Property

Public Property Get RegisterApplicationString() As String
    RegisterApplicationString = m_RegisterApplicationString
End Property

Public Function ShowLoginDialog() As String
    Dim strRetLogin As String
    
    ' giving the login control an UfisCom to do the login
    Set m_oAatLogin.UfisComCtrl = m_oUfisCom

    ' setting some information to be displayed on the login control
    m_oAatLogin.ApplicationName = m_ApplicationName
    m_oAatLogin.VersionString = m_VersionString
    m_oAatLogin.InfoCaption = m_InfoCaption
    m_oAatLogin.InfoButtonVisible = m_InfoButtonVisible
    m_oAatLogin.InfoUfisVersion = m_InfoUfisVersion
    m_oAatLogin.InfoAppVersion = m_InfoAppVersion
    m_oAatLogin.InfoCopyright = m_InfoCopyright
    m_oAatLogin.InfoAAT = m_InfoAAT
    m_oAatLogin.UserNameLCase = m_UserNameLCase
    m_oAatLogin.LoginAttempts = m_LoginAttempts
    m_oAatLogin.RegisterApplicationString = m_RegisterApplicationString
    
    strRetLogin = m_oAatLogin.ShowLoginDialog

    ' close connection to server
    m_oUfisCom.CleanupCom

    'look after the login result
    If strRetLogin = "CANCEL" Or strRetLogin = "ERROR" Then
        m_UserName = ""
    Else
        m_UserName = m_oAatLogin.GetUserName()
    End If
    
    ShowLoginDialog = m_UserName
End Function

Private Sub Class_Initialize()
    Set m_oAatLogin = CreateObject("AATLOGIN.AatLoginCtrl.1")
    Set m_oUfisCom = CreateObject("UFISCOM.UfisComCtrl.1")
End Sub

Private Sub Class_Terminate()
    Set m_oAatLogin = Nothing
    Set m_oUfisCom = Nothing
End Sub
