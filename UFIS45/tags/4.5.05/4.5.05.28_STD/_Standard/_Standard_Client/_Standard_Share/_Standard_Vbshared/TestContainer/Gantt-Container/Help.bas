Attribute VB_Name = "Help"
Option Explicit

Declare Function WinHelp Lib "user32" Alias "WinHelpA" (ByVal hwnd As Long, _
                                                        ByVal lpHelpFile As String, _
                                                        ByVal wCommand As Long, _
                                                        ByVal dwData As Long) As Long

Public Const HELP_CONTEXT = &H1
Public Const HELP_QUIT = &H2
Public Const HELP_INDEX = &H3
Public Const HELP_HELPONHELP = &H4
Public Const HELP_SETINDEX = &H5
Public Const HELP_KEY = &H101
Public Const HELP_MULTIKEY = &H201
Public Const HELP_FILE = "GanttHelp.hlp"

Public Sub DisplayHelp_On_Index(X As Form)
    Dim Foo As Long
    Dim HelpFilePath As String

    On Local Error GoTo Error_Handler

    HelpFilePath = App.Path & "\" & HELP_FILE
    Foo = WinHelp(X.hwnd, HelpFilePath, HELP_INDEX, CLng(0))

Error_End:
    Exit Sub

Error_Handler:
    MsgBox Err.Description
    Resume Error_End
End Sub
