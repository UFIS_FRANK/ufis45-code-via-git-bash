// stdafx.cpp : source file that includes just the standard includes
//  stdafx.pch will be the pre-compiled header
//  stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

CString LoadStg(UINT nID)
{
	CString olResult;
	olResult.LoadString(nID);
	int nLength = olResult.Find("*INTXT*",0);
	if (nLength >= 0)
		olResult = olResult.Left(nLength);

	return olResult;
}


