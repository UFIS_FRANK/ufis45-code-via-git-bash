#if !defined(AFX_PASSWORDDLG_H__8256755A_A522_4958_8947_92A947092EAA__INCLUDED_)
#define AFX_PASSWORDDLG_H__8256755A_A522_4958_8947_92A947092EAA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PasswordDlg.h : header file
//
#include <CCSEdit.h>
#include "AatStatic.h"



/////////////////////////////////////////////////////////////////////////////
// CPasswordDlg dialog
class CAatLoginCtrl;

class CPasswordDlg : public CDialog
{
// Construction
public:
	CPasswordDlg(CWnd* pParent = NULL);   // standard constructor
	CPasswordDlg(CWnd* pParent,CAatLoginCtrl *popLoginCtrl);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CPasswordDlg)
	enum { IDD = IDD_BDPSPASS_DIALOG };
		// NOTE: the ClassWizard will add data members here
	CCSEdit	m_NewPass1;
	CCSEdit	m_NewPass2;
	CCSEdit	m_OldPass;
	CCSEdit	m_Usid;
	AatStatic	m_LblUsid;
	AatStatic	m_LblOldPass;
	AatStatic	m_LblNewPass1;
	AatStatic	m_LblNewPass2;

	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPasswordDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CPasswordDlg)
		// NOTE: the ClassWizard will add member functions here
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	void SetText(int ipId, int ipStringId);
	BOOL ConnectToCeda();

	CString GetString(UINT nID);

private:
	CAatLoginCtrl	*pomLoginCtrl;
	BOOL			bmExtendedSecurity;
	UINT			imMinPasswordLength;

	int igPasswordLength;
	int igMandCaptitalLetter;
	int igMandSmallLetter;
	int igMandNumbers;
	int igNumConsecutive ;



};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PASSWORDDLG_H__8256755A_A522_4958_8947_92A947092EAA__INCLUDED_)
