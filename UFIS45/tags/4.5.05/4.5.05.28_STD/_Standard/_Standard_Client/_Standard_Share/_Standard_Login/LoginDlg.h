#if !defined(AFX_LOGINDLG_H__D6DA1133_1506_4304_AE9D_4E1DC8198E1B__INCLUDED_)
#define AFX_LOGINDLG_H__D6DA1133_1506_4304_AE9D_4E1DC8198E1B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LoginDlg.h : header file
//
#include <CCSEdit.h>
#include "AatStatic.h"
/////////////////////////////////////////////////////////////////////////////
// CLoginDlg dialog
class CAatLoginCtrl;

class CLoginDlg : public CDialog
{
// Construction
public:
	CLoginDlg(CWnd* pParent = NULL);   // standard constructor
	CLoginDlg(CWnd* pParent,CAatLoginCtrl *popLoginCtrl);   // standard constructor
	CString DoSilentLogin(const CString& ropUserName,const CString& ropPassword);

// Dialog Data
	//{{AFX_DATA(CLoginDlg)
	enum { IDD = IDD_LOGIN };
	CCSEdit		m_TxtUserName;
	CCSEdit		m_TxtPassword;
	AatStatic	m_LblUserName;
	AatStatic	m_LblPassword;
	AatStatic	m_LblVersion;
	CButton		m_BtnOk;
	CButton		m_BtnCancel;
	CButton		m_BtnAbout;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLoginDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLoginDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnAbout();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	BOOL	ConnectToCeda();
	CString	GetSetting(const CString& ropAppName,const CString& ropSection,const CString& ropKey,const CString& ropDefaultValue);
	BOOL	SaveSetting(const CString& ropAppName,const CString& ropSection,const CString& ropKey,const CString& ropValue);
	CString GetErrorMessageFromCode(const CString& ropErrCode);

	int GetDaysBeforeDeactivation();

private:
	CAatLoginCtrl	*pomLoginCtrl;
	BOOL			bmExtendedSecurity;
	UINT			imMinPasswordLength;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOGINDLG_H__D6DA1133_1506_4304_AE9D_4E1DC8198E1B__INCLUDED_)
