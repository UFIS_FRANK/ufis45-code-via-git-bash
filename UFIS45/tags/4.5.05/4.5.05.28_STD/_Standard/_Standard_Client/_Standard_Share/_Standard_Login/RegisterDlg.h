#if !defined(AFX_REGISTERDLG_H__42E6A991_8C5C_4CE4_8536_D72FA6DE5713__INCLUDED_)
#define AFX_REGISTERDLG_H__42E6A991_8C5C_4CE4_8536_D72FA6DE5713__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RegisterDlg.h : header file
//
#include "AatStatic.h"

/////////////////////////////////////////////////////////////////////////////
// CRegisterDlg dialog
class CAatLoginCtrl;

class CRegisterDlg : public CDialog
{
// Construction
public:
	CRegisterDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CRegisterDlg)
	enum { IDD = IDD_REGISTERDLG };
		// NOTE: the ClassWizard will add data members here
	CEdit		m_TxtTime;
	AatStatic	m_TxtText;
	CButton	m_BtnAbort;
	CButton	m_BtnStart;
	CButton	m_BtnRegister;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRegisterDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CRegisterDlg)
		// NOTE: the ClassWizard will add member functions here
	afx_msg void OnAbort();
	afx_msg void OnRegister();
	afx_msg void OnStart();
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CAatLoginCtrl	*pomLoginCtrl;
	int				imTimer;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REGISTERDLG_H__42E6A991_8C5C_4CE4_8536_D72FA6DE5713__INCLUDED_)
