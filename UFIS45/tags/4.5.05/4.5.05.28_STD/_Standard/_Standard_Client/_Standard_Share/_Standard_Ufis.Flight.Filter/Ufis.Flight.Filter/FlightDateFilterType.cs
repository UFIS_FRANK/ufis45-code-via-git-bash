﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ufis.Flight.Filter
{
    /// <summary>
    /// Enumerations for type of flight date filter.
    /// </summary>
    public enum FlightDateFilterType
    {        
        AbsoluteDate,
        RelativeDate
    }
}
