// MappBCProxyEvents.cpp : implementation file
//

#include "stdafx.h"
#include "BCComClient.h"
#include "MappBCProxyEvents.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static const IID IID_BcPrxyEvents = {0x68e25ff2, 0x6790, 0x11d4, {0x90, 0x3b, 0x0, 0x01, 0x02, 0x04, 0xaa, 0x51}};
//CString IDP_OLE_INIT_FAILED = "OLE initialization failed.  Make sure that the OLE libraries are the correct version.";
/////////////////////////////////////////////////////////////////////////////
// MappBCProxyEvents

IMPLEMENT_DYNCREATE(MappBCProxyEvents, CCmdTarget)

MappBCProxyEvents::MappBCProxyEvents()
{
}

MappBCProxyEvents::MappBCProxyEvents(CBCComClientCtrl *popCtrl) : CCmdTarget()
{
	EnableAutomation();
	pomCtrl = popCtrl;
	EnableAutomation();


	CLSID clsid;
	HRESULT res;

	pDispBcPrxyEvents = NULL;
	pIUnknown_Auto = NULL;
	this->m_dwCookie = 0;

	// receiving the class-ID of BcProxy
	res = CLSIDFromProgID(L"BcProxy.BcPrxy.1",&clsid);
	if (res != S_OK)
	{
		AfxMessageBox (_T("couldn�t get clsid of BcProxy !"));
		return;
	}

	// *** creating dispatch for events
	if (!omBcPrxyEvents.CreateDispatch (clsid))
	{
		AfxMessageBox (_T("unable to create BcPrxyEvents instance !"));
		return;
	}

	pDispBcPrxyEvents = omBcPrxyEvents.m_lpDispatch ;
	pDispBcPrxyEvents->AddRef();

	// ^^^^^ pointer to Disp. Interface of BcPrxyEvents

	//connect server to automation class
	pIUnknown_Auto = this->GetIDispatch(FALSE);
	int erg = AfxConnectionAdvise(pDispBcPrxyEvents,IID_BcPrxyEvents,pIUnknown_Auto,TRUE,&m_dwCookie);

	pomBcPrxyInstance = (IBcPrxy*)&omBcPrxyEvents;
	// ^^^^^ pointer to Disp. Interface of BcPrxyInstance
}

MappBCProxyEvents::~MappBCProxyEvents()
{
}

BOOL MappBCProxyEvents::OnBroadcast (LPCTSTR ReqId, 
							 LPCTSTR Dest1, 
							 LPCTSTR Dest2, 
							 LPCTSTR Cmd, 
							 LPCTSTR Object,
							 LPCTSTR Seq, 
							 LPCTSTR Tws, 
							 LPCTSTR Twe, 
							 LPCTSTR Selection, 
							 LPCTSTR Fields, 
							 LPCTSTR Data, 
							 LPCTSTR BcNum)
{
	return TRUE;
}

BEGIN_DISPATCH_MAP(MappBCProxyEvents, CCmdTarget)
	//{{AFX_DISPATCH_MAP(Auto_test)
		DISP_FUNCTION_ID(MappBCProxyEvents,"OnBcReceive",1,OnBroadcast, VT_BOOL, VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR 
						VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR)  
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()



BEGIN_INTERFACE_MAP(MappBCProxyEvents, CCmdTarget)
	INTERFACE_PART(MappBCProxyEvents, IID_BcPrxyEvents, Dispatch)
END_INTERFACE_MAP()



BEGIN_MESSAGE_MAP(MappBCProxyEvents, CCmdTarget)
	//{{AFX_MSG_MAP(MappBCProxyEvents)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// MappBCProxyEvents message handlers

void MappBCProxyEvents::OnFinalRelease() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CCmdTarget::OnFinalRelease();
}

void MappBCProxyEvents::RegisterObject(LPCTSTR strTable, LPCTSTR strCommand, BOOL bOwnBC, LPCTSTR application)
{
	pomBcPrxyInstance->RegisterObject(strTable, strCommand, bOwnBC, application); 
}

void MappBCProxyEvents::Disconnect()
{
	if (m_dwCookie != 0)
	{
		AfxConnectionUnadvise(pDispBcPrxyEvents,IID_BcPrxyEvents,pIUnknown_Auto,TRUE,m_dwCookie);
		pDispBcPrxyEvents->Release();
	}

	if (omBcPrxyEvents)
	{
		omBcPrxyEvents.ReleaseDispatch ();
	}
}
