// CedaBlkData.h

#ifndef __CEDABLKDATA__
#define __CEDABLKDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------

struct BLKDATA 
{
	long 	 Urno;
	CTime 	 Nafr;
	CTime 	 Nato;
	char 	 Days[9];
	char 	 Type[3];
	char 	 Tabn[5];
	char 	 Resn[42];
	long 	 Burn;
	char 	 Tifr[6];
	char 	 Tito[6];
	long	 Ibit;		// Index bitmap for blocked times

	int		 IsChanged;	// Check whether Data has Changed f�r Relaese

	BLKDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged	=	DATA_UNCHANGED;
		Nafr		=	TIMENULL;
		Nato		=	TIMENULL;
		Urno		=	0;
		Burn		=	0;
	}

}; // end BlkDataStrukt

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaBlkData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<BLKDATA> omData;

	char pcmListOfFields[2048];

// Operations
public:
    CedaBlkData();
	~CedaBlkData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(BLKDATA *prpBlk);
	bool InsertInternal(BLKDATA *prpBlk);
	bool Update(BLKDATA *prpBlk);
	bool UpdateInternal(BLKDATA *prpBlk);
	bool Delete(long lpUrno);
	bool DeleteInternal(BLKDATA *prpBlk);
	bool ReadSpecialData(CCSPtrArray<BLKDATA> *popBlk,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(BLKDATA *prpBlk);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	BLKDATA  *GetBlkByUrno(long lpUrno);


	// Private methods
private:
    void PrepareBlkData(BLKDATA *prpBlkData);

};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDABLKDATA__
