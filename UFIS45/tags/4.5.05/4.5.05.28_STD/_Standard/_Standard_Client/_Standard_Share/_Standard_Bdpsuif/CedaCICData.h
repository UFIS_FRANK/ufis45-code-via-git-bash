// CedaCICData.h

#ifndef __CEDACICDATA__
#define __CEDACICDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

struct CICDATA 
{
	CTime	 Cdat;		// Erstellungsdatum
	char 	 Cnam[7]; 	// Check-In Schalter Name
	char 	 Edpe[27]; 	// EDV-Ausr�stung
	CTime	 Lstu; 		// Datum letzte �nderung
//	CTime	 Nafr;		// Nicht verf�gbar vom
//	CTime	 Nato;		// Nicht verf�gbar bis
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Tele[12]; 	// Telefonnummer
	//*** 15.12.1999 SHA ***
	char 	 Tel2[12]; 	// Telefonnummer
	long 	 Urno;		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	CTime	 Vafr;		// G�ltig von
	CTime	 Vato;		// G�ltig bis
	char 	 Rgbl[7]; 	// Verkn�pfter Warteraum
//	char 	 Resn[42]; 	// Grund f�r die Sperrung
	char 	 Term[3]; 	// Terminal
	char 	 Hall[7]; 	// Halle
	//*** 15.12.1999 SHA ***
	//*** 5->12 ***
	char 	 Cicr[12]; 	// Reihe/Insel
	char 	 Home[4]; 	// Reihe/Insel
	char 	 Cicl[4]; 	// Reihe/Insel
	char 	 Cbaz[4]; 	// Reihe/Insel
	long	 Ibit;		// Index bitmap for blocked times
	char	 Catr[8];	// Additional attributes
	char	 Cseq[6];	// Sequence number

	//*** 17.11.2011 WNY ***
	char 	 Apis[2]; 	// APIS
	char 	 Wesc[2]; 	// Weighing scale
	char 	 Oubl[4]; 	// No. of outbound belts
	char 	 Inbl[4]; 	// No. of inbound belts

	//DataCreated by this class
	int      IsChanged;

	CICDATA(void)
	{
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
//		Nafr=-1;
//		Nato=-1;
		Vafr=-1;
		Vato=-1;
	}

}; // end CICDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaCICData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<CICDATA> omData;

// Operations
public:
    CedaCICData();
	~CedaCICData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(void);

    bool Read(char *pcpWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<CICDATA> *popCic,char *pspWhere,char *pspFieldList,bool ipSYS = true);
	bool InsertCIC(CICDATA *prpCIC,BOOL bpSendDdx = TRUE);
	bool InsertCICInternal(CICDATA *prpCIC);
	bool UpdateCIC(CICDATA *prpCIC,BOOL bpSendDdx = TRUE);
	bool UpdateCICInternal(CICDATA *prpCIC);
	bool DeleteCIC(long lpUrno);
	bool DeleteCICInternal(CICDATA *prpCIC);
	CICDATA  *GetCICByUrno(long lpUrno);
	bool SaveCIC(CICDATA *prpCIC);
	char pcmCICFieldList[2048];
	void ProcessCICBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	bool CompareCIC(CString &pcpListOfData, CString &pcpFieldList, void *pvpSaveDataStruct, void *pvpChangedDataStruct);
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;} //Prf: 8795
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;} //Prf: 8795
	// Private methods
private:
    void PrepareCICData(CICDATA *prpCICData);
	bool MakeCedaData(CCSPtrArray<CEDARECINFO> *pomRecInfo,CString &pcpListOfData, CString &pcpFieldList, void *pvpSaveDataStruct, void *pvpChangedDataStruct);
	bool MakeCedaData(CString &pcpListOfData, CString &pcpFieldList, void *pvpSaveDataStruct, void *pvpChangedDataStruct);
	bool MakeCedaData(CCSPtrArray<CEDARECINFO> *pomRecInfo,CString &pcpListOfData, void *pvpDataStruct);

	CString omWhere; //Prf: 8795
	bool ValidateCICBcData(const long& lrpUnro); //Prf: 8795
};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDACICDATA__
