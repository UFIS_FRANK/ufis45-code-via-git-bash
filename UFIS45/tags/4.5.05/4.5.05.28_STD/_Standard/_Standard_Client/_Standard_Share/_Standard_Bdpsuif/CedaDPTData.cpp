// CedaDPTData.cpp
 
#include <stdafx.h>
#include <CedaDPTData.h>
#include <resource.h>
#include <CedaBasicData.h>


// Local function prototype
static void ProcessDPTCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaDPTData::CedaDPTData() : CCSCedaData(&ogCommHandler)
{
    // Create an array of CEDARECINFO for DPTDATA
	BEGIN_CEDARECINFO(DPTDATA,DPTDATARecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Hopo,"HOPO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_LONG      (Rurn,"RURN")
		FIELD_CHAR_TRIM (Defl,"DEFL")
		FIELD_CHAR_TRIM (Doop,"DOOP")
		FIELD_CHAR_TRIM (Dbgn,"DBGN")
		FIELD_CHAR_TRIM (Dend,"DEND")
		FIELD_CHAR_TRIM (Dpid,"DPID")
		FIELD_CHAR_TRIM (Dffd,"DFFD")
		FIELD_CHAR_TRIM (Dfco,"DFCO")
		FIELD_CHAR_TRIM (Dter,"DTER")
		FIELD_CHAR_TRIM (Dare,"DARE")
		FIELD_CHAR_TRIM (Algc,"ALGC")
	END_CEDARECINFO //(DPTDATA)


	// Copy the record structure
	for (int i=0; i< sizeof(DPTDATARecInfo)/sizeof(DPTDATARecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&DPTDATARecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"DPT");	
	strcpy(pcmDPTFieldList,"URNO,HOPO,USEC,USEU,CDAT,LSTU,RURN,DEFL,DOOP,DBGN,DEND,DPID,DFFD,DFCO,DTER,DARE,ALGC");
	pcmFieldList = pcmDPTFieldList;
	omData.SetSize(0,1000);
}

//---------------------------------------------------------------------------------------------------

void CedaDPTData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("URNO"); ropDesription.Add(LoadStg(IDS_STRING346)); ropType.Add("String");
	ropFields.Add("HOPO"); ropDesription.Add(LoadStg(IDS_STRING711)); ropType.Add("String");
	ropFields.Add("USEC"); ropDesription.Add(LoadStg(IDS_STRING347)); ropType.Add("String");
	ropFields.Add("USEU"); ropDesription.Add(LoadStg(IDS_STRING348)); ropType.Add("String");
	ropFields.Add("CDAT"); ropDesription.Add(LoadStg(IDS_STRING343)); ropType.Add("Date");
	ropFields.Add("LSTU"); ropDesription.Add(LoadStg(IDS_STRING344)); ropType.Add("Date");
	ropFields.Add("RURN"); ropDesription.Add(LoadStg(IDS_STRING1130)); ropType.Add("String");
	ropFields.Add("DEFL"); ropDesription.Add(LoadStg(IDS_STRING1131)); ropType.Add("String");
	ropFields.Add("DOOP"); ropDesription.Add(LoadStg(IDS_STRING1132)); ropType.Add("String");
	ropFields.Add("DBGN"); ropDesription.Add(LoadStg(IDS_STRING1133)); ropType.Add("String");
	ropFields.Add("DEND"); ropDesription.Add(LoadStg(IDS_STRING1134)); ropType.Add("String");
	ropFields.Add("DPID"); ropDesription.Add(LoadStg(IDS_STRING1135)); ropType.Add("String");
	ropFields.Add("DFFD"); ropDesription.Add(LoadStg(IDS_STRING1136)); ropType.Add("String");
	ropFields.Add("DFCO"); ropDesription.Add(LoadStg(IDS_STRING1137)); ropType.Add("String");
	ropFields.Add("DTER"); ropDesription.Add(LoadStg(IDS_STRING1138)); ropType.Add("String");
	ropFields.Add("DARE"); ropDesription.Add(LoadStg(IDS_STRING1139)); ropType.Add("String");
	ropFields.Add("ALGC"); ropDesription.Add(LoadStg(IDS_STRING1140)); ropType.Add("String");
}

//-----------------------------------------------------------------------------------------------

void CedaDPTData::Register(void)
{
	ogDdx.Register((void *)this,BC_DPT_CHANGE,CString("DPTDATA"), CString("DPT-changed"),ProcessDPTCf);
	ogDdx.Register((void *)this,BC_DPT_DELETE,CString("DPTDATA"), CString("DPT-deleted"),ProcessDPTCf);	
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaDPTData::~CedaDPTData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaDPTData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaDPTData::Read(char *pcpWhere)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pcpWhere != NULL)
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pcpWhere);
	else
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		DPTDATA *prpDPT = new DPTDATA;
		if ((ilRc = GetFirstBufferRecord(prpDPT)) == true)
		{
			prpDPT->IsChanged = DATA_UNCHANGED;
			omData.Add(prpDPT);//Update omData
			omUrnoMap.SetAt((void *)prpDPT->Urno,prpDPT);
		}
		else
		{
			delete prpDPT;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaDPTData::InsertDPT(DPTDATA *prpDPT,BOOL bpSendDdx)
{
	prpDPT->IsChanged = DATA_NEW;
	if (SaveDPT(prpDPT) == false) 
		return false; //Update Database
	InsertDPTInternal(prpDPT);
    return true;
}

//--INSERT-INTERNAL-------------------------------------------------------------------------------------------

bool CedaDPTData::InsertDPTInternal(DPTDATA *prpDPT)
{
	//PrepareDPTDATA(prpDPT);
	ogDdx.DataChanged((void *)this, DPT_CHANGE,(void *)prpDPT ); //Update Viewer
	omData.Add(prpDPT);//Update omData
	omUrnoMap.SetAt((void *)prpDPT->Urno,prpDPT);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaDPTData::DeleteDPT(long lpUrno)
{
	DPTDATA *prlDPT = GetDPTByUrno(lpUrno);
	if (prlDPT != NULL)
	{
		prlDPT->IsChanged = DATA_DELETED;
		if(SaveDPT(prlDPT) == false) return false; //Update Database
		DeleteDPTInternal(prlDPT);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaDPTData::DeleteDPTInternal(DPTDATA *prpDPT)
{
	ogDdx.DataChanged((void *)this,DPT_DELETE,(void *)prpDPT); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpDPT->Urno);
	int ilDPTCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilDPTCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpDPT->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaDPTData::PrepareDPTData(DPTDATA *prpDPT)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaDPTData::UpdateDPT(DPTDATA *prpDPT,BOOL bpSendDdx)
{
	if (GetDPTByUrno(prpDPT->Urno) != NULL)
	{
		if (prpDPT->IsChanged == DATA_UNCHANGED)
		{
			prpDPT->IsChanged = DATA_CHANGED;
		}
		if(SaveDPT(prpDPT) == false) return false; //Update Database
		UpdateDPTInternal(prpDPT);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaDPTData::UpdateDPTInternal(DPTDATA *prpDPT)
{
	DPTDATA *prlDPT = GetDPTByUrno(prpDPT->Urno);
	if (prlDPT != NULL)
	{
		*prlDPT = *prpDPT; //Update omData
		ogDdx.DataChanged((void *)this,DPT_CHANGE,(void *)prlDPT); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

DPTDATA *CedaDPTData::GetDPTByUrno(long lpUrno)
{
	DPTDATA  *prlDPT;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlDPT) == TRUE)
	{
		return prlDPT;
	}
	return NULL;
	
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaDPTData::ReadSpecialData(CCSPtrArray<DPTDATA> *popDPTArray,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","DPT",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","DPT",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popDPTArray != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			DPTDATA *prpDPT = new DPTDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpDPT,CString(pclFieldList))) == true)
			{
				popDPTArray->Add(prpDPT);
			}
			else
			{
				delete prpDPT;
			}
		}
		if(popDPTArray->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaDPTData::SaveDPT(DPTDATA *prpDPT)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[2048];

	if (prpDPT->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpDPT->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpDPT);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpDPT->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpDPT->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpDPT);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpDPT->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpDPT->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}

    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessDPTCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_DPT_CHANGE :
	case BC_DPT_DELETE :
		((CedaDPTData *)popInstance)->ProcessDPTBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaDPTData::ProcessDPTBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlDPTData = (struct BcStruct *) vpDataPointer;
	long llUrno = GetUrnoFromSelection(prlDPTData->Selection);

	DPTDATA *prlDPT = GetDPTByUrno(llUrno);
	if(ipDDXType == BC_DPT_CHANGE)
	{
		if(prlDPT != NULL)
		{
			GetRecordFromItemList(prlDPT,prlDPTData->Fields,prlDPTData->Data);
			if(ValidateDPTBcData(prlDPT->Urno))
			{
				UpdateDPTInternal(prlDPT);
			}
			else
			{
				DeleteDPTInternal(prlDPT);
			}
		}
		else
		{
			prlDPT = new DPTDATA;
			GetRecordFromItemList(prlDPT,prlDPTData->Fields,prlDPTData->Data);
			if(ValidateDPTBcData(prlDPT->Urno))
			{
				InsertDPTInternal(prlDPT);
			}
			else
			{
				delete prlDPT;
			}
		}
	}
	if(ipDDXType == BC_DPT_DELETE)
	{
		if (prlDPT != NULL)
		{
			DeleteDPTInternal(prlDPT);
		}
	}
}

//--ValidateDPTBcData--------------------------------------------------------------------------------------

bool CedaDPTData::ValidateDPTBcData(const long& lrpUrno)
{
	bool blValidateDPTBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<DPTDATA> olDPTs;
		if(!ReadSpecialData(&olDPTs,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateDPTBcData = false;
		}
	}
	return blValidateDPTBcData;
}
//---------------------------------------------------------------------------------------------------------
