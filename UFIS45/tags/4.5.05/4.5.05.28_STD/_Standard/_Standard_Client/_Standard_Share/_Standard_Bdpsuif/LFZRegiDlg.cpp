// LFZRegiDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <LFZRegiDlg.h>
#include <CedaACTData.h>
#include <PrivList.h>
#include <AwBasicDataDlg.h>
#include <CedaBasicData.h>
#include <Util.h>
#include <CCSGlobl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// LFZRegiDlg dialog


LFZRegiDlg::LFZRegiDlg(ACRDATA *popACR,CWnd* pParent /*=NULL*/,bool isnew) : CDialog(LFZRegiDlg::IDD, pParent)
{
	pomACR = popACR;
	ombNew = isnew;

	if (ogBasicData.UseUniqueActPair())
	{
		ogBCD.SetObject("ACT","URNO,ACT3,ACT5,ACFN,ACTI");
		ogBCD.SetObjectDesc("ACT",LoadStg(IDS_STRING176));
		ogBCD.SetTableHeader("ACT","ACT3","A/C");
		ogBCD.SetTableHeader("ACT","ACT5","ICAO");
		ogBCD.SetTableHeader("ACT","ACFN",LoadStg(IDS_STRING229));
		ogBCD.AddKeyMap("ACT","ACT3");
		ogBCD.AddKeyMap("ACT","ACT5");
		ogBCD.Read("ACT");
	}

	//{{AFX_DATA_INIT(LFZRegiDlg)
	//}}AFX_DATA_INIT
}


void LFZRegiDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(LFZRegiDlg)
	DDX_Control(pDX, IDC_BLACK_LIST, m_BLACK_LIST);
	DDX_Control(pDX, IDC_MING, m_MING);
	DDX_Control(pDX, IDC_REGI, m_REGI);
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_APUI,   m_APUI);
	DDX_Control(pDX, IDC_MAIN,   m_MAIN);
	DDX_Control(pDX, IDC_ACT3,   m_ACT3);
	DDX_Control(pDX, IDC_ANNX,   m_ANNX);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_DEBI,   m_DEBI);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_MOWE,   m_MOWE);
	DDX_Control(pDX, IDC_MTOW,   m_MTOW);
	DDX_Control(pDX, IDC_NOGA,   m_NOGA);
	DDX_Control(pDX, IDC_NOSE,   m_NOSE);
	DDX_Control(pDX, IDC_NOTO,   m_NOTO);
	DDX_Control(pDX, IDC_OWNE,   m_OWNE);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_REGN,   m_REGN);
	DDX_Control(pDX, IDC_REMA,   m_REMA);
	DDX_Control(pDX, IDC_SELC,   m_SELC);
	DDX_Control(pDX, IDC_ICOA,   m_ACT5);
	DDX_Control(pDX, IDC_VAFR_D2, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T1, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D1, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T1, m_VATOT);
	DDX_Control(pDX, IDC_ACTI,	 m_ACTI);
	DDX_Control(pDX, IDC_ACRH,	 m_ACRH);
	DDX_Control(pDX, IDC_ACRL,	 m_ACRL);
	DDX_Control(pDX, IDC_ACRW,	 m_ACRW);
	DDX_Control(pDX, IDC_CTRY,	 m_CTRY);
	DDX_Control(pDX, IDC_UFIS, m_UFIS);
	DDX_Control(pDX, IDC_MTOW_DETAIL, m_MTOW_DETAIL);
	DDX_Control(pDX, IDC_CODE_DETAIL, m_CODE_DETAIL);
	DDX_Control(pDX, IDC_HOM2, m_HOM2);
	DDX_Control(pDX, IDC_NACO, m_NACO);
	DDX_Control(pDX, IDC_OADR, m_OADR);
	DDX_Control(pDX, IDC_TYPU, m_TYPU);
	DDX_Control(pDX, IDC_CRW1, m_CRW1);
	DDX_Control(pDX, IDC_CRW2, m_CRW2);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(LFZRegiDlg, CDialog)
	//{{AFX_MSG_MAP(LFZRegiDlg)
    ON_MESSAGE(WM_EDIT_KILLFOCUS, OnKillfocus)
    ON_MESSAGE(WM_EDIT_CHANGED,   OnSelChange)
	ON_BN_CLICKED(IDC_EXTENDED, OnExtended)
	ON_BN_CLICKED(IDC_BSELCODE,   OnBSelCode)
	ON_BN_CLICKED(IDC_MTOW_DETAIL,   OnBMtow)
	ON_BN_CLICKED(IDC_CODE_DETAIL,   OnBCode)
	ON_BN_CLICKED(IDC_BLACK_LIST, OnBlackList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// LFZRegiDlg message handlers
//----------------------------------------------------------------------------------------

BOOL LFZRegiDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING182) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);

	char clStat = ogPrivList.GetStat("LFZREGISTRATIONDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomACR->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomACR->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomACR->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomACR->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomACR->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomACR->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_REGN.SetTypeToString("X(12)",12,1);
	m_REGN.SetTextErrColor(RED);
	m_REGN.SetInitText(pomACR->Regn);
	m_REGN.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_REGN"));
	dlgExt.m_RegnExt = pomACR->Regn;
	//------------------------------------
	m_REGI.SetTypeToString("X(12)",12,1);
	//m_REGI.SetBKColor(YELLOW);
	m_REGI.SetTextErrColor(RED);
	m_REGI.SetInitText(pomACR->Regi);
	m_REGI.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_REGN"));
	//------------------------------------
	if (ogBasicData.UseUniqueActPair())
	{
		m_ACT3.SetFormat("X(3)");
		m_ACT3.SetTextLimit(0,3,true);
		m_ACT3.SetBKColor(YELLOW);  
		m_ACT3.SetTextErrColor(RED);
		m_ACT3.SetInitText(pomACR->Act3);
		m_ACT3.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_ACT3"));
		//------------------------------------
		m_ACT5.SetFormat("X(5)");
		m_ACT5.SetTextLimit(0,5,true);
		m_ACT5.SetBKColor(YELLOW);  
		m_ACT5.SetTextErrColor(RED);
		m_ACT5.SetInitText(pomACR->Act5);
		m_ACT5.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_ACT5"));

		CWnd *polWnd = this->GetDlgItem(IDC_BSELCODE);
		if (polWnd != NULL)
		{
			polWnd->EnableWindow(TRUE);
			polWnd->ShowWindow(SW_SHOW);
		}
	}
	else
	{
		m_ACT3.SetFormat("X(3)");
		m_ACT3.SetTextLimit(0,3,true);
		m_ACT3.SetTextErrColor(RED);
		m_ACT3.SetInitText(pomACR->Act3);
		m_ACT3.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_ACT3"));
		//------------------------------------
		m_ACT5.SetFormat("X(5)");
		m_ACT5.SetTextLimit(0,5,true);
		m_ACT5.SetBKColor(YELLOW);  
		m_ACT5.SetTextErrColor(RED);
		m_ACT5.SetInitText(pomACR->Act5);
		m_ACT5.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_ACT5"));
	}

	//------------------------------------
	m_ACTI.SetBKColor(SILVER);
	m_ACTI.SetInitText(pomACR->Acti);
	m_ACTI.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_ACTI"));
	//------------------------------------
	m_OWNE.SetTypeToString("X(10)",12,0);
	m_OWNE.SetTextErrColor(RED);
	m_OWNE.SetInitText(pomACR->Owne);
	m_OWNE.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_OWNE"));
	//------------------------------------
	
	//GFO Change to fit the 4.5
	//m_SELC.SetFormat("x|#x|#x|#x|#x|#");
	m_SELC.SetFormat("X(5)");
	m_SELC.SetTextLimit(0,5);
	m_SELC.SetTextErrColor(RED);
	m_SELC.SetInitText(pomACR->Selc);
	m_SELC.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_SELC"));
	//------------------------------------
//~~~~~ 10.08.99 SHA : 
//	m_LADPD.SetTypeToDate();
	//m_LADPD.SetTextErrColor(RED);
//	m_LADPD.SetInitText(pomACR->Ladp.Format("%d.%m.%Y"));
	dlgExt.m_Ladp_d = (pomACR->Ladp.Format("%d.%m.%Y"));
//	m_LADPD.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_LADP"));
//	// - - - - - - - - - - - - - - - - - -
//	m_LADPT.SetTypeToTime();
//	m_LADPT.SetTextErrColor(RED);
//	m_LADPT.SetInitText(pomACR->Ladp.Format("%H:%M"));
	dlgExt.m_Ladp_t = (pomACR->Ladp.Format("%H:%M"));
//	m_LADPT.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_LADP"));
	//------------------------------------
//~~~~~ 10.08.99 SHA : 
	m_MTOW.SetTypeToDouble(7, 2, 0.01,9999999.99);
//	m_MTOW.SetBKColor(YELLOW);  
	m_MTOW.SetTextErrColor(RED);
	m_MTOW.SetInitText(pomACR->Mtow);
	m_MTOW.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_MTOW"));
	//------------------------------------
	m_MOWE.SetTypeToDouble(7, 2, 0,9999999.99);
	m_MOWE.SetTextErrColor(RED);
	m_MOWE.SetInitText(pomACR->Mowe);
	m_MOWE.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_MOWE"));
	//------------------------------------
	m_NOSE.SetTypeToInt(0,999);
	m_NOSE.SetTextErrColor(RED);
	m_NOSE.SetInitText(pomACR->Nose);
	m_NOSE.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_NOSE"));
	//------------------------------------
	m_NOGA.SetTypeToInt(0,99);
	m_NOGA.SetTextErrColor(RED);
	m_NOGA.SetInitText(pomACR->Noga);
	m_NOGA.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_NOGA"));
	//------------------------------------
	m_NOTO.SetTypeToInt(0,99);
	m_NOTO.SetTextErrColor(RED);
	m_NOTO.SetInitText(pomACR->Noto);
	m_NOTO.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_NOTO"));
	//------------------------------------
	m_DEBI.SetTypeToString("X(10)",10,0);
	m_DEBI.SetTextErrColor(RED);
	m_DEBI.SetInitText(pomACR->Debi);
	m_DEBI.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_DEBI"));
	//------------------------------------
		
	m_HOM2.SetTypeToString("X(3)",3,0);
	m_HOM2.SetTextErrColor(RED);
	m_HOM2.SetInitText(pomACR->Home);
	m_HOM2.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_HOM2"));
	//------------------------------------
	/**/
	m_NACO.SetTypeToString("X(3)",3,0);
	m_NACO.SetTextErrColor(RED);
	m_NACO.SetInitText(pomACR->Naco);
	m_NACO.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_NACO"));
	//------------------------------------
	m_OADR.SetTypeToString("X(256)",256,0);
	m_OADR.SetTextErrColor(RED);
	m_OADR.SetInitText(pomACR->Oadr);
	m_OADR.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_OADR"));
	//------------------------------------
	m_TYPU.SetTypeToString("X(3)",3,0);
	m_TYPU.SetTextErrColor(RED);
	m_TYPU.SetInitText(pomACR->Typu);
	m_TYPU.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_TYPU"));
	//------------------------------------
	m_CRW1.SetTypeToString("X(2)",2,0);
	m_CRW1.SetTextErrColor(RED);
	m_CRW1.SetInitText(pomACR->Crw1);
	m_CRW1.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_CRW1"));
	//------------------------------------
	m_CRW2.SetTypeToString("X(2)",2,0);
	m_CRW2.SetTextErrColor(RED);
	m_CRW2.SetInitText(pomACR->Crw2);
	m_CRW2.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_CRW2"));
	//------------------------------------
	/*
*/	m_ANNX.SetTypeToString("X(2)",2,1);
	//m_ANNX.SetBKColor(YELLOW);  //Commented for 458
	m_ANNX.SetTextErrColor(RED);
	m_ANNX.SetInitText(pomACR->Annx);
	m_ANNX.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_ANNX"));
	//------------------------------------
/*	m_ENTY.SetBKColor(SILVER);
	m_ENTY.SetInitText(pomACR->Enty);
	m_ENTY.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_ENTY"));
*/	//------------------------------------
	dlgExt.m_Enty = pomACR->Enty;
	dlgExt.m_Gall = pomACR->Gall;
/*	m_GALL.SetTypeToString("X(32)",32,0);
	m_GALL.SetTextErrColor(RED);
	m_GALL.SetInitText(pomACR->Gall);
	m_GALL.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_GALL"));
*/	//------------------------------------
	dlgExt.m_Frst = pomACR->Frst;
/*	m_FRST.SetTypeToString("X(3)",3,0);
	m_FRST.SetTextErrColor(RED);
	m_FRST.SetInitText(pomACR->Frst);
	m_FRST.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_FRST"));
	//------------------------------------
	*/
	dlgExt.m_Rest = pomACR->Rest;
	/*
	m_REST.SetTypeToString("X(3)",3,0);
	m_REST.SetTextErrColor(RED);
	m_REST.SetInitText(pomACR->Rest);
	m_REST.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_REST"));
*/	//------------------------------------
	dlgExt.m_Efis= pomACR->Efis;
/*	m_EFIS.SetTypeToString("X(3)",3,0);
	m_EFIS.SetTextErrColor(RED);
	m_EFIS.SetInitText(pomACR->Efis);
	m_EFIS.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_EFIS"));
	//------------------------------------
	*/
	dlgExt.m_Enna = pomACR->Enna;
	/*
	m_ENNA.SetTypeToString("X(10)",10,0);
	m_ENNA.SetTextErrColor(RED);
	m_ENNA.SetInitText(pomACR->Enna);
	m_ENNA.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_ENNA"));
*/	//------------------------------------
	m_REMA.SetTypeToString("X(40)",40,0);
	m_REMA.SetInitText(pomACR->Rema);
	m_REMA.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_REMA"));
	//------------------------------------
	if (pomACR->Apui[0] == 'X') m_APUI.SetCheck(1);
	clStat = ogPrivList.GetStat("LFZREGISTRATIONDLG.m_APUI");
	SetWndStatAll(clStat,m_APUI);
	//------------------------------------
	if (pomACR->Main[0] == 'X') m_MAIN.SetCheck(1);
	clStat = ogPrivList.GetStat("LFZREGISTRATIONDLG.m_MAIN");
	SetWndStatAll(clStat,m_MAIN);
	//------------------------------------
	if (pomACR->Ufis[0] == '1') m_UFIS.SetCheck(1);
	clStat = ogPrivList.GetStat("LFZREGISTRATIONDLG.m_UFIS");
	SetWndStatAll(clStat,m_UFIS);
	//------------------------------------
	
	m_VAFRD.SetTypeToDate();
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetInitText(pomACR->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	
	m_VAFRT.SetTypeToTime();
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetInitText(pomACR->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomACR->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomACR->Vato.Format("%H:%M"));
	m_VATOT.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_VATO"));
	//------------------------------------
	m_ACRH.SetTypeToDouble(3,2,000.01,999.99);
	m_ACRH.SetTypeToString("X(6)",10,0);
	m_ACRH.SetTextErrColor(RED);
	m_ACRH.SetInitText(pomACR->Ache);
	m_ACRH.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_ACRH"));
	//------------------------------------
	m_ACRL.SetTypeToDouble(3,2,000.01,999.99);
	m_ACRL.SetTypeToString("X(6)",10,0);
	m_ACRL.SetTextErrColor(RED);
	m_ACRL.SetInitText(pomACR->Acle);
	m_ACRL.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_ACRL"));
	//------------------------------------
	m_ACRW.SetTypeToDouble(3,2,000.01,999.99);
	m_ACRW.SetTypeToString("X(6)",10,0);
	m_ACRW.SetTextErrColor(RED);
	m_ACRW.SetInitText(pomACR->Acws);
	m_ACRW.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_ACRW"));
	//------------------------------------
//*** 04.10.99 SHA ***
/*#ifdef CLIENT_SHA
	m_MING.SetTypeToString("#(3)",10,0);
	m_MING.SetTextErrColor(RED);
	m_MING.SetInitText(pomACR->Ming);
	//------------------------------------
#endif*/

	//OKKKK
	dlgExt.m_Conf = pomACR->Conf;
/*	m_CONF.SetTypeToString("X(16)",10,0);
	m_CONF.SetTextErrColor(RED);
	m_CONF.SetInitText(pomACR->Conf);
	m_CONF.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_CONF"));
	//------------------------------------
	m_CONT.SetTypeToString("X(1)",10,0);
	m_CONT.SetTextErrColor(RED);
	m_CONT.SetInitText(pomACR->Cont);
	m_CONT.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_CONT"));
*/	//------------------------------------
	dlgExt.m_Cont = pomACR->Cont;

	m_CTRY.SetTypeToString("X(10)",10,0);
	m_CTRY.SetTextErrColor(RED);
	m_CTRY.SetInitText(pomACR->Ctry);
	m_CTRY.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_CTRY"));
	//------------------------------------
	
/*	m_DELI.SetTypeToString("X(4)",10,0);
	m_DELI.SetTextErrColor(RED);
	m_DELI.SetInitText(pomACR->Deli);
	m_DELI.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_DELI"));
	
*/
	dlgExt.m_Deli = pomACR->Deli;
	//------------------------------------
	dlgExt.m_Exrg = pomACR->Exrg;
/*	m_EXRG.SetTypeToString("X(11)",10,0);
	m_EXRG.SetTextErrColor(RED);
	m_EXRG.SetInitText(pomACR->Exrg);
	m_EXRG.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_EXRG"));
	//------------------------------------
	*/
	dlgExt.m_Fuse = pomACR->Fuse;
	/*
	m_FUSE.SetTypeToString("X(5)",10,0);
	m_FUSE.SetTextErrColor(RED);
	m_FUSE.SetInitText(pomACR->Fuse);
	m_FUSE.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_FUSE"));
	//------------------------------------
	*/
	dlgExt.m_Iata = pomACR->Iata;
	/*
	m_IATA.SetTypeToString("X(4)",10,0);
	m_IATA.SetTextErrColor(RED);
	m_IATA.SetInitText(pomACR->Iata);
	m_IATA.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_IATA"));
	//------------------------------------
	m_ICAO.SetTypeToString("X(4)",10,0);
	m_ICAO.SetTextErrColor(RED);
	m_ICAO.SetInitText(pomACR->Icao);
	m_ICAO.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_ICAO"));
*/	//------------------------------------
	dlgExt.m_Icao = pomACR->Icao;
	
	dlgExt.m_Lcod = pomACR->Lcod;
/*	m_LCOD.SetTypeToString("X(4)",10,0);
	m_LCOD.SetTextErrColor(RED);
	m_LCOD.SetInitText(pomACR->Lcod);
	m_LCOD.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_LCOD"));
	
	//------------------------------------
	*/
	dlgExt.m_Nois = pomACR->Nois;
	/*
	m_NOIS.SetTypeToString("X(1)",10,0);
	m_NOIS.SetTextErrColor(RED);
	m_NOIS.SetInitText(pomACR->Nois);
	m_NOIS.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_NOIS"));
	//------------------------------------
	m_ODAT.SetTypeToString("X(4)",10,0);
	m_ODAT.SetTextErrColor(RED);
	m_ODAT.SetInitText(pomACR->Odat);
	m_ODAT.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_ODAT"));
	//------------------------------------
	*/
	dlgExt.m_Odat = pomACR->Odat;
	dlgExt.m_Powd = pomACR->Powd;
	/*
	m_POWD.SetTypeToString("X(21)",10,0);
	m_POWD.SetTextErrColor(RED);
	m_POWD.SetInitText(pomACR->Powd);
	m_POWD.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_POWD"));
*/	//------------------------------------
	dlgExt.m_Scod = pomACR->Scod;
/*	m_SCOD.SetTypeToString("X(10)",10,0);
	m_SCOD.SetTextErrColor(RED);
	m_SCOD.SetInitText(pomACR->Scod);
	m_SCOD.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_SCOD"));
*/	//------------------------------------
/*	m_SERI.SetTypeToString("X(16)",10,0);
	m_SERI.SetTextErrColor(RED);
	m_SERI.SetInitText(pomACR->Seri);
	m_SERI.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_SERI"));

*/	
	dlgExt.m_Seri = pomACR->Seri;
	//------------------------------------
	dlgExt.m_Stxt = pomACR->Text;
/*	m_STXT.SetTypeToString("X(20)",10,0);
	m_STXT.SetTextErrColor(RED);
	m_STXT.SetInitText(pomACR->Text);
	m_STXT.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_STXT"));
*/	//------------------------------------
//	m_TYPE.SetTypeToString("X(42)",10,0);
//	m_TYPE.SetTextErrColor(RED);
//	m_TYPE.SetInitText(pomACR->Type);
//	m_TYPE.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_TYPE"));
	dlgExt.m_Type = pomACR->Type;
	//------------------------------------
	dlgExt.m_Year = pomACR->Year;
/*	m_YEAR.SetTypeToString("X(4)",10,0);
	m_YEAR.SetTextErrColor(RED);
	m_YEAR.SetInitText(pomACR->Year);
	m_YEAR.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_YEAR"));*/
	//------------------------------------
	dlgExt.m_Lsdf = pomACR->Lsdf;
/*	m_LSDF.SetTypeToString("X(20)",10,0);
	m_LSDF.SetTextErrColor(RED);
	m_LSDF.SetInitText(pomACR->Lsdf);
	m_LSDF.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_LSDF"));
	//------------------------------------
	if (pomACR->Cvtd[0] == 'X') m_CVTD.SetCheck(1);
	clStat = ogPrivList.GetStat("LFZREGISTRATIONDLG.m_CVTD");
	SetWndStatAll(clStat,m_CVTD);
	//------------------------------------
	*/
	if (pomACR->Lsdb[0] == 'X') 
		dlgExt.m_Lsdb = TRUE ;
	if (pomACR->Lsfb[0] == 'X') 
		dlgExt.m_Lsfb = TRUE ;
	if (pomACR->Lstb[0] == 'X') 
		dlgExt.m_Lstb = TRUE ;
	if (pomACR->Oope[0] == 'X') 
		dlgExt.m_Oope = TRUE ;
	if (pomACR->Opbf[0] == 'X') 
		dlgExt.m_Opbf = TRUE ;
	if (pomACR->Opwb[0] == 'X') 
		dlgExt.m_Opwb = TRUE ;
	if (pomACR->Opbb[0] == 'X') 
		dlgExt.m_Opbb = TRUE ;
	if (pomACR->Oord[0] == 'X') 
		dlgExt.m_Oord = TRUE ;
	if (pomACR->Oopt[0] == 'X') 
		dlgExt.m_Oopt = TRUE ;
	if (pomACR->Wobo[0] == 'X') 
		dlgExt.m_Wobo = TRUE ;
	if (pomACR->Wfub[0] == 'X') 
		dlgExt.m_Wfub = TRUE ;
	if (pomACR->Regb[0] == 'X') 
		dlgExt.m_Regb= TRUE ;
	if (pomACR->Cvtd[0] == 'X') 
		dlgExt.m_Cvtd= TRUE ;
	if (pomACR->Strd[0] == 'X') 
		dlgExt.m_Strd = TRUE ;


	/*
	if (pomACR->Lsdb[0] == 'X') m_LSDB.SetCheck(1);
	clStat = ogPrivList.GetStat("LFZREGISTRATIONDLG.m_LSDB");
	SetWndStatAll(clStat,m_LSDB);
	//------------------------------------
	if (pomACR->Lsfb[0] == 'X') m_LSFB.SetCheck(1);
	clStat = ogPrivList.GetStat("LFZREGISTRATIONDLG.m_LSFB");
	SetWndStatAll(clStat,m_LSFB);
	//------------------------------------
	if (pomACR->Lstb[0] == 'X') m_LSTB.SetCheck(1);
	clStat = ogPrivList.GetStat("LFZREGISTRATIONDLG.m_LSTB");
	SetWndStatAll(clStat,m_LSTB);
	//------------------------------------
	if (pomACR->Oope[0] == 'X') m_OOPE.SetCheck(1);
	clStat = ogPrivList.GetStat("LFZREGISTRATIONDLG.m_OOPE");
	SetWndStatAll(clStat,m_OOPE);
	//------------------------------------
	if (pomACR->Oopt[0] == 'X') m_OOPT.SetCheck(1);
	clStat = ogPrivList.GetStat("LFZREGISTRATIONDLG.m_OOPT");
	SetWndStatAll(clStat,m_OOPT);
	//------------------------------------
	if (pomACR->Oord[0] == 'X') m_OORD.SetCheck(1);
	clStat = ogPrivList.GetStat("LFZREGISTRATIONDLG.m_OORD");
	SetWndStatAll(clStat,m_OORD);
	//------------------------------------
	if (pomACR->Opbb[0] == 'X') m_OPBB.SetCheck(1);
	clStat = ogPrivList.GetStat("LFZREGISTRATIONDLG.m_OPBB");
	SetWndStatAll(clStat,m_OPBB);
	//------------------------------------
	if (pomACR->Opbf[0] == 'X') m_OPBF.SetCheck(1);
	clStat = ogPrivList.GetStat("LFZREGISTRATIONDLG.m_OPBF");
	SetWndStatAll(clStat,m_OPBF);
	//------------------------------------
	if (pomACR->Opwb[0] == 'X') m_OPWB.SetCheck(1);
	clStat = ogPrivList.GetStat("LFZREGISTRATIONDLG.m_OPWB");
	SetWndStatAll(clStat,m_OPWB);
	//------------------------------------
	if (pomACR->Regb[0] == 'X') m_REGB.SetCheck(1);
	clStat = ogPrivList.GetStat("LFZREGISTRATIONDLG.m_REGB");
	SetWndStatAll(clStat,m_REGB);
	//------------------------------------
	if (pomACR->Strd[0] == 'X') m_STRD.SetCheck(1);
	clStat = ogPrivList.GetStat("LFZREGISTRATIONDLG.m_STRD");
	SetWndStatAll(clStat,m_STRD);
	//------------------------------------
	if (pomACR->Wfub[0] == 'X') m_WFUB.SetCheck(1);
	clStat = ogPrivList.GetStat("LFZREGISTRATIONDLG.m_WFUB");
	SetWndStatAll(clStat,m_WFUB);
	//------------------------------------
	if (pomACR->Wobo[0] == 'X') m_WOBO.SetCheck(1);
	clStat = ogPrivList.GetStat("LFZREGISTRATIONDLG.m_WOBO");
	SetWndStatAll(clStat,m_WOBO);
	//------------------------------------
*//*	m_CNAM.SetTypeToString("X(40)",10,0);
	m_CNAM.SetTextErrColor(RED);
	m_CNAM.SetInitText(pomACR->Cnam);
	m_CNAM.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_CNAM"));*/
	dlgExt.m_Cnam = pomACR->Cnam;
	//------------------------------------
	dlgExt.m_Pnum = pomACR->Pnum;
/*	m_PNUM.SetTypeToString("X(10)",10,0);
	m_PNUM.SetTextErrColor(RED);
	m_PNUM.SetInitText(pomACR->Pnum);
	m_PNUM.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_PNUM"));
*/	//------------------------------------

	//Aircraft Registration Screen Enhancement			
	BOOL regEnh = IsPrivateProfileOn("REG_ENH", "NO" );

	if(regEnh)
	{
		if(strlen(pomACR->Regn) != 0)//Disable not to check whether if this is new registration
		{
			//m_IATA.SetReadOnly(true);
			//m_ICAO.SetReadOnly(true);
			//m_ACTI.SetReadOnly(true);
			//m_MTWO_TON = "Test";
			m_ACT3.SetReadOnly(true);
			m_ACT5.SetReadOnly(true);

			char clStat = ogPrivList.GetStat("LFZREGISTRATIONDLG.m_CODE_DETAIL");
			
			m_CODE_DETAIL.ShowWindow(SW_SHOW);
			m_CODE_DETAIL.SetWindowText(LoadStg(IDS_STRING32823));
			
			clStat = ogPrivList.GetStat("LFZREGISTRATIONDLG.m_MTOW_DETAIL");
			
			m_MTOW_DETAIL.ShowWindow(SW_SHOW);
			m_MTOW_DETAIL.SetWindowText(LoadStg(IDS_STRING32824));

			m_MOWE.SetReadOnly(true);
		}
		
		omNtsFormula = GetFormula();
			((CWnd*)GetDlgItem(IDC_FORMULA))->SetWindowText(omNtsFormula);

		m_MTOW.SetReadOnly(true);
		
		((CWnd*)GetDlgItem(IDC_MTOW_TON))->SetWindowText(LoadStg(IDS_STRING32825));
		((CWnd*)GetDlgItem(IDC_MOW_KG))->SetWindowText(LoadStg(IDS_STRING32826));
	}

	//------------------------------------
	m_BLACK_LIST.SetSecState(ogPrivList.GetStat("LFZREGISTRATIONDLG.m_BLACK_LIST"));

	return TRUE;  
}

//----------------------------------------------------------------------------------------

void LFZRegiDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	CString olTemp;
	int ilFind_P,ilFind_K;
	bool ilStatus = true;
	if(m_REGN.GetStatus() == false)
	{
		ilStatus = false;
		if(m_REGN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING182) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING182) + ogNotFormat;
		}
	}
	if(m_ACT3.GetStatus() == false)
	{
		if(m_ACT3.GetWindowTextLength() != 0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING240) + ogNotFormat;
		}
	}
	if(m_ACT5.GetStatus() == false)
	{
		if(m_ACT5.GetWindowTextLength() != 0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING241) + ogNotFormat;
		}
	}
	

	/*
	char clRegWhere[100];
	Bool blIsNewReg = false;
	CString olReg;
	CCSPtrArray<ACRDATA> olAcrReg;

	m_REGN.GetWindowText(olReg);
	sprintf(clRegWhere,"WHERE REGN='%s'",olReg);

	//Temporarily
	if(ogACRData.ReadSpecialData(&olAcrReg,clRegWhere,"URNO,REGN",false) == true)
	{
		blIsNewReg = true;
	}
	*/
	//if(!blIsNewReg)
	{
		if(m_ACT3.GetWindowTextLength() == 0 && m_ACT5.GetWindowTextLength() == 0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING52);
		}
	}

	if(m_OWNE.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING53) + ogNotFormat;
	}
	if(m_SELC.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING54) + ogNotFormat;
	}
//~~~~~ 10.08.99 SHA : 
/*	if(m_LADPD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING55) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_LADPT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING55) + LoadStg(IDS_STRING233) + ogNotFormat;
	}
*/

/*	if(m_MTOW.GetStatus() == false)
	{
		ilStatus = false;
		if(m_MTOW.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING56) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING56) + ogNotFormat;
		}
	}
*/	if(m_MOWE.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING57) + ogNotFormat;
	}
	if(m_NOSE.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING58) + ogNotFormat;
	}
	if(m_NOGA.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING59) + ogNotFormat;
	}
	if(m_NOTO.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING60) + ogNotFormat;
	}
/*	if(m_ENNA.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING51) + ogNotFormat;
	}

*/	
	//Modified the checking by Christine for UFIS-458
	/*
	if(m_ANNX.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING48) + ogNotFormat;
		
	}
	*/
	if(m_DEBI.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING62) + ogNotFormat;
	}
//~~~~~ 10.08.99 SHA : 
/*	if(m_GALL.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING58) + ogNotFormat;
	}
	if(m_FRST.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING58) + ogNotFormat;
	}
	if(m_REST.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING58) + ogNotFormat;
	}
	if(m_EFIS.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING58) + ogNotFormat;
	}
*/	if(m_REMA.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING238) + ogNotFormat;
	}
	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////////////
	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
	//	olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		
		if(!olVafrd.IsEmpty())
		{
			CString olStr;
			olStr = olVafrd.Right(4);
			int i = atoi(olStr);
			if(i <= 1970 || i >= 2037)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING32795);
			}
			else
				olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);

		}
		//olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(!olVatod.IsEmpty())
		{
			CString olStr;
			olStr = olVatod.Right(4);
			int i = atoi(olStr);
			if(i <= 1970 || i >= 2037)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING32796);
			}
			else
				olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
			if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr))// || olTmpTimeFr == -1))
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING236);
			}

		}
		/*if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr))// || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}*/
	}

//~~~~~ 10.08.99 SHA : 
/*	if((m_LADPD.GetWindowTextLength() != 0 && m_LADPT.GetWindowTextLength() == 0) || (m_LADPD.GetWindowTextLength() == 0 && m_LADPT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING63);
	}
*/
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olAct3,olAct5,olRegn;
		char clWhere[100];

		CCSPtrArray<ACTDATA> olActCPA;

		if (ogBasicData.UseUniqueActPair())
		{
			if(m_ACT3.GetWindowTextLength() != 0)
			{
				m_ACT3.GetWindowText(olAct3);

				if (m_ACT5.GetWindowTextLength() != 0)
				{
					m_ACT5.GetWindowText(olAct5);
				}
				else
				{
					olAct5 = " ";
				}
			}
			else	// only ACT5 available
			{
				m_ACT5.GetWindowText(olAct5);
				olAct3 = " ";
			}

			sprintf(clWhere,"WHERE ACT3='%s' AND ACT5='%s'",olAct3,olAct5);
			if (ogACTData.ReadSpecialData(&olActCPA,clWhere,"ACT3,ACT5,ACTI,ENTY",false) == true && olActCPA.GetSize() == 1)
			{
				strcpy(pomACR->Act3,olActCPA[0].Act3);
				if (strlen(pomACR->Act3) == 0)
					strcpy(pomACR->Act3," ");
				strcpy(pomACR->Act5,olActCPA[0].Act5);
				if (strlen(pomACR->Act5) == 0)
					strcpy(pomACR->Act5," ");
				strcpy(pomACR->Acti,olActCPA[0].Acti);
				strcpy(pomACR->Enty,olActCPA[0].Enty);
			}
			else
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING32);
			}
		}
		else
		{
			if(m_ACT3.GetWindowTextLength() != 0)
			{
				m_ACT3.GetWindowText(olAct3);
				sprintf(clWhere,"WHERE ACT3='%s'",olAct3);
				if(ogACTData.ReadSpecialData(&olActCPA,clWhere,"ACT3,ACT5,ACTI,ENTY",false) == true)
				{
					m_ACT3.GetWindowText(pomACR->Act3,4);
					strcpy(pomACR->Act5,olActCPA[0].Act5);
					strcpy(pomACR->Acti,olActCPA[0].Acti);
					strcpy(pomACR->Enty,olActCPA[0].Enty);
				}
				else
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING32);
				}
			}
			else
			{
				m_ACT5.GetWindowText(olAct5);
				sprintf(clWhere,"WHERE ACT5='%s'",olAct5);
				if(ogACTData.ReadSpecialData(&olActCPA,clWhere,"ACT3,ACT5,ACTI,ENTY",false) == true)
				{
					m_ACT5.GetWindowText(pomACR->Act5,6);
					strcpy(pomACR->Act3,olActCPA[0].Act3);
					strcpy(pomACR->Acti,olActCPA[0].Acti);
					strcpy(pomACR->Enty,olActCPA[0].Enty);
				}
				else
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING64);
				}

			}
		}

		olActCPA.DeleteAll();

		CCSPtrArray<ACRDATA> olAcrCPA;
		m_REGN.GetWindowText(olRegn);
		sprintf(clWhere,"WHERE REGN='%s'",olRegn);
		if(ogACRData.ReadSpecialData(&olAcrCPA,clWhere,"URNO,REGN",false) == true)
		{
			if(olAcrCPA[0].Urno != pomACR->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING182) + LoadStg(IDS_EXIST);
			}
		}
		olAcrCPA.DeleteAll();
	}

	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_REGN.GetWindowText(pomACR->Regn,13);
		m_REGI.GetWindowText(pomACR->Regi,13);

		m_ANNX.GetWindowText(pomACR->Annx,3);
		m_DEBI.GetWindowText(pomACR->Debi,11);
		//~~~~~ 10.08.99 SHA : m_ENNA.GetWindowText(pomACR->Enna,11);

		m_MOWE.GetWindowText(olTemp);
		if(olTemp.GetLength() != 0)
		{
			ilFind_P = olTemp.ReverseFind('.');
			ilFind_K = olTemp.ReverseFind(',');
			if(ilFind_P == -1 && ilFind_K == -1)
			{
				olTemp += ".00";
			}
			else
			{
				if(ilFind_K != -1)
				{
					olTemp.SetAt(ilFind_K,'.');
					ilFind_P = ilFind_K;
				}
				if(olTemp.GetLength() == ilFind_P+1) olTemp += "00";
				else if(olTemp.GetLength() == (ilFind_P+2)) olTemp += "0";
			}
		}
		sprintf(pomACR->Mowe,"%s",olTemp);

		m_MTOW.GetWindowText(olTemp);
		ilFind_P = olTemp.ReverseFind('.');
		ilFind_K = olTemp.ReverseFind(',');
		if(ilFind_P == -1 && ilFind_K == -1)
		{
			olTemp += ".00";
		}
		else
		{
			if(ilFind_K != -1)
			{
				olTemp.SetAt(ilFind_K,'.');
				ilFind_P = ilFind_K;
			}
			if(olTemp.GetLength() == ilFind_P+1) olTemp += "00";
			else if(olTemp.GetLength() == (ilFind_P+2)) olTemp += "0";
		}
		sprintf(pomACR->Mtow,"%s",olTemp);

		m_NOGA.GetWindowText(pomACR->Noga,3);
		m_NOSE.GetWindowText(pomACR->Nose,4);
		m_NOTO.GetWindowText(pomACR->Noto,3);
		m_OWNE.GetWindowText(pomACR->Owne,11);
		m_SELC.GetWindowText(pomACR->Selc,6);
		m_REMA.GetWindowText(pomACR->Rema,41);
//~~~~~ 10.08.99 SHA : 		m_GALL.GetWindowText(pomACR->Gall,34);
//~~~~~ 10.08.99 SHA : 		m_FRST.GetWindowText(pomACR->Frst,4);
//~~~~~ 10.08.99 SHA : 		m_REST.GetWindowText(pomACR->Rest,4);
//~~~~~ 10.08.99 SHA : 		m_EFIS.GetWindowText(pomACR->Efis,4);
		m_HOM2.GetWindowText(pomACR->Home,4);
		
		m_NACO.GetWindowText(pomACR->Naco,4);
		m_OADR.GetWindowText(pomACR->Oadr,256);
		m_TYPU.GetWindowText(pomACR->Typu,4);
		m_CRW1.GetWindowText(pomACR->Crw1,3);
		m_CRW2.GetWindowText(pomACR->Crw2,3);
		CString olLadpd,olLadpt;
//~~~~~ 10.08.99 SHA : 		m_LADPD.GetWindowText(olLadpd);
		olLadpd = dlgExt.m_Ladp_d;
//~~~~~ 10.08.99 SHA : 		m_LADPT.GetWindowText(olLadpt);
		olLadpt = dlgExt.m_Ladp_t;
		pomACR->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomACR->Vato = DateHourStringToDate(olVatod,olVatot);
		pomACR->Ladp = DateHourStringToDate(olLadpd,olLadpt);
		//~~~~~ 10.08.99 SHA : 
/*		m_LCOD.GetWindowText(pomACR->Lcod,4);
		m_NOIS.GetWindowText(pomACR->Nois,1);
		m_ODAT.GetWindowText(pomACR->Odat,4);
		m_POWD.GetWindowText(pomACR->Powd,21);
		m_SERI.GetWindowText(pomACR->Seri,16);
		m_STXT.GetWindowText(pomACR->Text,20);
		m_TYPE.GetWindowText(pomACR->Type,42);
		m_YEAR.GetWindowText(pomACR->Year,4);
		m_LSDF.GetWindowText(pomACR->Lsdf,20);
		m_ICAO.GetWindowText(pomACR->Icao,4);
		m_IATA.GetWindowText(pomACR->Iata,4);
		m_FUSE.GetWindowText(pomACR->Fuse,5);
		m_EXRG.GetWindowText(pomACR->Exrg,11);
		m_DELI.GetWindowText(pomACR->Deli,4);
*/		m_CTRY.GetWindowText(pomACR->Ctry,10);
//~~~~~ 10.08.99 SHA : 		m_CONT.GetWindowText(pomACR->Cont,2);
//~~~~~ 10.08.99 SHA : 		m_CONF.GetWindowText(pomACR->Conf,16);
//~~~~~ 10.08.99 SHA : 		m_PNUM.GetWindowText(pomACR->Pnum,10);
//~~~~~ 10.08.99 SHA : 		m_CNAM.GetWindowText(pomACR->Cnam,40);
		sprintf(pomACR->Cnam ,"%s", dlgExt.m_Cnam);
		sprintf(pomACR->Type,"%s", dlgExt.m_Type);
		sprintf(pomACR->Seri,"%s", dlgExt.m_Seri);
		sprintf(pomACR->Frst,"%s", dlgExt.m_Frst);
		sprintf(pomACR->Conf,"%s", dlgExt.m_Conf);
		sprintf(pomACR->Powd,"%s", dlgExt.m_Powd);
		sprintf(pomACR->Nois,"%s", dlgExt.m_Nois);
		sprintf(pomACR->Rest,"%s", dlgExt.m_Rest);
		sprintf(pomACR->Efis,"%s", dlgExt.m_Efis);
		sprintf(pomACR->Text,"%s", dlgExt.m_Stxt);
		sprintf(pomACR->Exrg,"%s", dlgExt.m_Exrg);
		sprintf(pomACR->Scod,"%s", dlgExt.m_Scod);
		sprintf(pomACR->Year,"%s", dlgExt.m_Year);
		sprintf(pomACR->Pnum,"%s", dlgExt.m_Pnum);
		sprintf(pomACR->Gall,"%s", dlgExt.m_Gall);
		sprintf(pomACR->Enna,"%s", dlgExt.m_Enna);
		sprintf(pomACR->Fuse,"%s", dlgExt.m_Fuse);
		sprintf(pomACR->Icao,"%s", dlgExt.m_Icao);
		sprintf(pomACR->Cont,"%s", dlgExt.m_Cont);
		sprintf(pomACR->Iata,"%s", dlgExt.m_Iata);
		sprintf(pomACR->Deli,"%s", dlgExt.m_Deli);
		sprintf(pomACR->Lsdf,"%s", dlgExt.m_Lsdf);
		sprintf(pomACR->Lcod,"%s", dlgExt.m_Lcod);
		sprintf(pomACR->Odat,"%s", dlgExt.m_Odat);
		sprintf(pomACR->Enty,"%s", dlgExt.m_Enty);
//*** 04.10.99 SHA ***
/*#ifdef CLIENT_SHA
		m_MING.GetWindowText(pomACR->Ming,13);
#endif*/


		m_ACRW.GetWindowText(olTemp);
		ilFind_P = olTemp.ReverseFind('.');
		ilFind_K = olTemp.ReverseFind(',');
		if(ilFind_P == -1 && ilFind_K == -1)
		{
			olTemp += ".00";
		}
		else
		{
			if(ilFind_K != -1)
			{
				olTemp.SetAt(ilFind_K,'.');
				ilFind_P = ilFind_K;
			}
			if(olTemp.GetLength() == ilFind_P+1) olTemp += "00";
			else if(olTemp.GetLength() == (ilFind_P+2)) olTemp += "0";
		}
		sprintf(pomACR->Acws,"%s",olTemp);
		m_ACRL.GetWindowText(olTemp);
		ilFind_P = olTemp.ReverseFind('.');
		ilFind_K = olTemp.ReverseFind(',');
		if(ilFind_P == -1 && ilFind_K == -1)
		{
			olTemp += ".00";
		}
		else
		{
			if(ilFind_K != -1)
			{
				olTemp.SetAt(ilFind_K,'.');
				ilFind_P = ilFind_K;
			}
			if(olTemp.GetLength() == ilFind_P+1) olTemp += "00";
			else if(olTemp.GetLength() == (ilFind_P+2)) olTemp += "0";
		}
		sprintf(pomACR->Acle,"%s",olTemp);
		m_ACRH.GetWindowText(olTemp);
		ilFind_P = olTemp.ReverseFind('.');
		ilFind_K = olTemp.ReverseFind(',');
		if(ilFind_P == -1 && ilFind_K == -1)
		{
			olTemp += ".00";
		}
		else
		{
			if(ilFind_K != -1)
			{
				olTemp.SetAt(ilFind_K,'.');
				ilFind_P = ilFind_K;
			}
			if(olTemp.GetLength() == ilFind_P+1) olTemp += "00";
			else if(olTemp.GetLength() == (ilFind_P+2)) olTemp += "0";
		}
		sprintf(pomACR->Ache,"%s",olTemp);
//~~~~~ 10.08.99 SHA : 

		(dlgExt.m_Lsdb == TRUE) ? pomACR->Lsdb[0] = 'X' : pomACR->Lsdb[0] = ' ';
		(dlgExt.m_Lsfb == TRUE) ? pomACR->Lsfb[0] = 'X' : pomACR->Lsfb[0] = ' ';
		(dlgExt.m_Lstb == TRUE) ? pomACR->Lstb[0] = 'X' : pomACR->Lstb[0] = ' ';
		(dlgExt.m_Oope == TRUE) ? pomACR->Oope[0] = 'X' : pomACR->Oope[0] = ' ';
		(dlgExt.m_Opbf == TRUE) ? pomACR->Opbf[0] = 'X' : pomACR->Opbf[0] = ' ';
		(dlgExt.m_Opwb == TRUE) ? pomACR->Opwb[0] = 'X' : pomACR->Opwb[0] = ' ';
		(dlgExt.m_Opbb == TRUE) ? pomACR->Opbb[0] = 'X' : pomACR->Opbb[0] = ' ';
		(dlgExt.m_Oord == TRUE) ? pomACR->Oord[0] = 'X' : pomACR->Oord[0] = ' ';
		(dlgExt.m_Oopt == TRUE) ? pomACR->Oopt[0] = 'X' : pomACR->Oopt[0] = ' ';
		(dlgExt.m_Wobo == TRUE) ? pomACR->Wobo[0] = 'X' : pomACR->Wobo[0] = ' ';
		(dlgExt.m_Wfub == TRUE) ? pomACR->Wfub[0] = 'X' : pomACR->Wfub[0] = ' ';
		(dlgExt.m_Regb == TRUE) ? pomACR->Regb[0] = 'X' : pomACR->Regb[0] = ' ';
		(dlgExt.m_Cvtd == TRUE) ? pomACR->Cvtd[0] = 'X' : pomACR->Cvtd[0] = ' ';
		(dlgExt.m_Strd == TRUE) ? pomACR->Strd[0] = 'X' : pomACR->Strd[0] = ' ';
/*		(m_CVTD.GetCheck() == 1) ? pomACR->Cvtd[0] = 'X' : pomACR->Cvtd[0] = ' ';
		(m_LSDB.GetCheck() == 1) ? pomACR->Lsdb[0] = 'X' : pomACR->Lsdb[0] = ' ';
		(m_LSFB.GetCheck() == 1) ? pomACR->Lsfb[0] = 'X' : pomACR->Lsfb[0] = ' ';
		(m_LSTB.GetCheck() == 1) ? pomACR->Lstb[0] = 'X' : pomACR->Lstb[0] = ' ';
		(m_OOPE.GetCheck() == 1) ? pomACR->Oope[0] = 'X' : pomACR->Oope[0] = ' ';
		(m_OOPT.GetCheck() == 1) ? pomACR->Oopt[0] = 'X' : pomACR->Oopt[0] = ' ';
		(m_OORD.GetCheck() == 1) ? pomACR->Oord[0] = 'X' : pomACR->Oord[0] = ' ';
		(m_OPBB.GetCheck() == 1) ? pomACR->Opbb[0] = 'X' : pomACR->Opbb[0] = ' ';
		(m_OPBF.GetCheck() == 1) ? pomACR->Opbf[0] = 'X' : pomACR->Opbf[0] = ' ';
		(m_OPWB.GetCheck() == 1) ? pomACR->Opwb[0] = 'X' : pomACR->Opwb[0] = ' ';
		(m_REGB.GetCheck() == 1) ? pomACR->Regb[0] = 'X' : pomACR->Regb[0] = ' ';
		(m_STRD.GetCheck() == 1) ? pomACR->Strd[0] = 'X' : pomACR->Strd[0] = ' ';
		(m_WFUB.GetCheck() == 1) ? pomACR->Wfub[0] = 'X' : pomACR->Wfub[0] = ' ';
		(m_WOBO.GetCheck() == 1) ? pomACR->Wobo[0] = 'X' : pomACR->Wobo[0] = ' ';
*/		(m_APUI.GetCheck() == 1) ? pomACR->Apui[0] = 'X' : pomACR->Apui[0] = ' ';
		(m_MAIN.GetCheck() == 1) ? pomACR->Main[0] = 'X' : pomACR->Main[0] = ' ';
		(m_UFIS.GetCheck() == 1) ? pomACR->Ufis[0] = '1' : pomACR->Ufis[0] = '0';

		char clStat = ogPrivList.GetStat("LFZREGISTRATIONDLG.m_MTOW_DETAIL");
		BOOL regEnh = IsPrivateProfileOn("REG_ENH", "NO" );
		CString strACT3;
		CString strACT5;
		CString strMTOW;
		CString strMOWE;
		if(regEnh)
		{
			if(!ombNew)
			{

				CString olWhere = CString(" ORDER BY ACT3");
				ogACRData.Read(olWhere.GetBuffer(0));

				ACRDATA *plACR = ogACRData.GetACRByUrno(pomACR->Urno);
				
				strcpy(pomACR->Act3,plACR->Act3);
				strcpy(pomACR->Act5,plACR->Act5);
				strcpy(pomACR->Mtow,plACR->Mtow);
				strcpy(pomACR->Mowe,plACR->Mowe);

			}
		}
		
		//Kill the process
		if (mdBlackListProcId!=NULL)			
		{		
			killProcess( mdBlackListProcId );	
		}	



		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_REGN.SetFocus();
		m_REGN.SetSel(0,-1);
	}

	if (mdCodeDetailProcId!=NULL)			
	{		
		killProcess( mdCodeDetailProcId );	
	}	

	if(mdMtowDetailProcId != NULL)
	{
		killProcess( mdMtowDetailProcId );	
	}

}

void LFZRegiDlg::OnCancel() 
{
	if (mdCodeDetailProcId!=NULL)			
	{		
		killProcess( mdCodeDetailProcId );	
	}	

	if(mdMtowDetailProcId != NULL)
	{
		killProcess( mdMtowDetailProcId );	
	}
	CDialog::OnCancel();
}

//----------------------------------------------------------------------------------------

LONG LFZRegiDlg::OnSelChange(UINT wParam, LONG lParam)
{
	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY *)lParam;
	if (m_ACT3.imID == wParam)
	{
		bmActSelect = false;
	}
	else if (m_ACT5.imID == wParam)
	{
		bmActSelect = false;
	}

	return 0L;
}


//----------------------------------------------------------------------------------------

LONG LFZRegiDlg::OnKillfocus(UINT lParam, LONG wParam) 
{
	if (ogBasicData.UseUniqueActPair())
	{
		CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY *)lParam;
		if (m_ACT3.imID == wParam && prlNotify->Text.IsEmpty() == false)
		{
			if (bmActSelect)
				return 0l;

			prlNotify->UserStatus = true;
			CString olWhere;
			olWhere.Format("WHERE act3 = '%s'",prlNotify->Text);

			CCSPtrArray<ACTDATA> olData;
			ogACTData.ReadSpecialData(&olData,olWhere.GetBuffer(0),ogACTData.pcmACTFieldList,false);

			if (olData.GetSize() == 0 || olData.GetSize() > 1)
			{
				OnAct3Code();
			}
			else
			{
				prlNotify->Status = true;
				m_ACT5.SetInitText(olData[0].Act5);
				m_ACTI.SetInitText(olData[0].Acti);
				m_ACTI.SetFocus();
			}
		}
		else if (m_ACT5.imID == wParam && prlNotify->Text.IsEmpty() == false)
		{
			if (bmActSelect)
				return 0l;

			prlNotify->UserStatus = true;
			CString olWhere;
			olWhere.Format("WHERE act5 = '%s'",prlNotify->Text);

			CCSPtrArray<ACTDATA> olData;
			ogACTData.ReadSpecialData(&olData,olWhere.GetBuffer(0),ogACTData.pcmACTFieldList,false);

			if (olData.GetSize() == 0 || olData.GetSize() > 1)
			{
				OnAct5Code();
			}
			else
			{
				prlNotify->Status = true;
				m_ACT3.SetInitText(olData[0].Act3);
				m_ACTI.SetInitText(olData[0].Acti);
				m_ACTI.SetFocus();
			}

		}

	}
	else
	{
		if(lParam == (UINT)m_ACT3.imID)
		{
			CString olAct3;
			m_ACT3.GetWindowText(olAct3);
			if (olAct3.GetLength() > 0)
			{
				if(m_ACT3.GetStatus() != false)
				{
					char clWhere[100];
					CCSPtrArray<ACTDATA> olActCPA;
					sprintf(clWhere,"WHERE ACT3='%s'",olAct3);
					if(ogACTData.ReadSpecialData(&olActCPA,clWhere,"ACT3,ACT5,ACTI,ENTY",false) == true)
					{
						m_ACT5.SetInitText(olActCPA[0].Act5);
						m_ACTI.SetInitText(olActCPA[0].Acti);
						//~~~~~ 12.08.99 SHA : m_ENTY.SetInitText(olActCPA[0].Enty);
						dlgExt.m_Enty = olActCPA[0].Enty;
					}
					else
					{
						m_ACT5.SetInitText("");
						m_ACTI.SetInitText("");
						//~~~~~ 12.08.99 SHA : m_ENTY.SetInitText("");
						dlgExt.m_Enty = "";
					}
					olActCPA.DeleteAll();
					m_ACT5.SetBKColor(WHITE);  
					m_ACT3.SetBKColor(YELLOW);  
				}
				else
				{
					m_ACT5.SetInitText("");
					m_ACTI.SetInitText("");
					//~~~~~ 12.08.99 SHA : m_ENTY.SetInitText("");
					dlgExt.m_Enty = "";
					m_ACT5.SetBKColor(YELLOW);  
					m_ACT3.SetBKColor(WHITE);  
				}
			}
		}
		if(lParam == (UINT)m_ACT5.imID)
		{
			CString olAct5;
			m_ACT5.GetWindowText(olAct5);
			if (olAct5.GetLength() > 0)
			{
				if(m_ACT5.GetStatus() != false)
				{
					char clWhere[100];
					CCSPtrArray<ACTDATA> olActCPA;
					sprintf(clWhere,"WHERE ACT5='%s'",olAct5);
					if(ogACTData.ReadSpecialData(&olActCPA,clWhere,"ACT3,ACT5,ACTI,ENTY",false) == true)
					{
						m_ACT3.SetInitText(olActCPA[0].Act3);
						m_ACTI.SetInitText(olActCPA[0].Acti);
						//~~~~~ 10.08.99 SHA : m_ENTY.SetInitText(olActCPA[0].Enty);
						dlgExt.m_Enty = olActCPA[0].Enty;
					}
					else
					{
						m_ACT3.SetInitText("");
						m_ACTI.SetInitText("");
						//~~~~~ 10.08.99 SHA : m_ENTY.SetInitText("");
						dlgExt.m_Enty = "";
					}
					olActCPA.DeleteAll();
					m_ACT5.SetBKColor(YELLOW);  
					m_ACT3.SetBKColor(WHITE);  
				}
				else
				{
					m_ACT3.SetInitText("");
					m_ACTI.SetInitText("");
					//~~~~~ 10.08.99 SHA : m_ENTY.SetInitText("");
					dlgExt.m_Enty = "";
					m_ACT5.SetBKColor(YELLOW);  
					m_ACT3.SetBKColor(WHITE);  
				}
			}
		}
	}
	return 0L;
}

//----------------------------------------------------------------------------------------

void LFZRegiDlg::OnExtended() 
{

	//*** OPEN THE EXTENDED ACR-DIALOG ***
	//*** 100899SHA ***

	dlgExt.DoModal();
}

void LFZRegiDlg::OnBSelCode() 
{
	CString	olAlc3;
	CString olAlc5;

	m_ACT3.GetWindowText(olAlc3);
	m_ACT5.GetWindowText(olAlc5);

	AwBasicDataDlg olDlg(this,"ACT","ACT3,ACT5,ACFN","ACT3+,ACT5+,ACFN+",olAlc3+","+olAlc5);
	olDlg.SetSecState("AIRCRAFTDLG.m_ACT3");
	if (olDlg.DoModal() == IDOK)
	{
		m_ACT3.SetInitText(olDlg.GetField("ACT3"),true);
		m_ACT5.SetInitText(olDlg.GetField("ACT5"),true);
		m_ACTI.SetInitText(olDlg.GetField("ACTI"),true);
		bmActSelect = true;
	}
}

void LFZRegiDlg::OnAct3Code() 
{
	CString	olAlc3;
	m_ACT3.GetWindowText(olAlc3);

	AwBasicDataDlg olDlg(this,"ACT","ACT3,ACT5,ACFN","ACT3+,ACT5+,ACFN+",olAlc3);
	olDlg.SetSecState("AIRCRAFTDLG.m_ACT3");
	if (olDlg.DoModal() == IDOK)
	{
		m_ACT3.SetInitText(olDlg.GetField("ACT3"),true);
		m_ACT5.SetInitText(olDlg.GetField("ACT5"),true);
		m_ACTI.SetInitText(olDlg.GetField("ACTI"),true);
		bmActSelect = true;
	}
}

void LFZRegiDlg::OnAct5Code() 
{
	CString	olAlc5;
	m_ACT5.GetWindowText(olAlc5);

	AwBasicDataDlg olDlg(this,"ACT","ACT5,ACT3,ACFN","ACT5+,ACT3+,ACFN+",olAlc5);
	olDlg.SetSecState("AIRCRAFTDLG.m_ACT3");
	if (olDlg.DoModal() == IDOK)
	{
		m_ACT3.SetInitText(olDlg.GetField("ACT3"),true);
		m_ACT5.SetInitText(olDlg.GetField("ACT5"),true);
		m_ACTI.SetInitText(olDlg.GetField("ACTI"),true);
		bmActSelect = true;
	}
}

void LFZRegiDlg::OnBMtow()
{
	/*
	MessageBox("Mtow");
	char pclCheck[256];
	char pclConfigPath[256];


	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "MULTI_BLOCK_FORM_NAME" , "BDPS-UIF", pclCheck, sizeof pclCheck, pclConfigPath);	
	omCodeDetailWindowName = pclCheck;	
	
	char clStat = ogPrivList.GetStat("m_BLOCKPST");
	//ConfigName for exe is "BDPSUIF_ADDON"
	mdMtowDetailProcId = LaunchTool( omCodeDetailWindowName, "0", "", clStat, "BDPSUIF_ADDON" , omCodeDetailWindowName, mdMtowDetailProcId );	
	*/	

	char urno[12];
	//itao(pomACR->Urno,urno,10);
	sprintf(urno,"%ld",pomACR->Urno);

	char clStat = ogPrivList.GetStat("LFZREGISTRATIONDLG.m_MTOW_DETAIL");
	if(GetConfigInfo("MTOW_DETAIL",omMtowDetailWindowName))
	{
		mdMtowDetailProcId = LaunchTool( omMtowDetailWindowName, urno, "", clStat, "BDPSUIF_ADDON" , omMtowDetailWindowName, mdMtowDetailProcId );	
	}
}

void LFZRegiDlg::OnBCode()
{
	//MessageBox("Code");	
	char urno[12];
	//itao(pomACR->Urno,urno,10);
	sprintf(urno,"%ld",pomACR->Urno);

	char clStat = ogPrivList.GetStat("LFZREGISTRATIONDLG.m_CODE_DETAIL");
	if(GetConfigInfo("CODE_DETAIL",omCodeDetailWindowName))
	{
		mdCodeDetailProcId = LaunchTool( omCodeDetailWindowName, urno, "", clStat, "BDPSUIF_ADDON" , omCodeDetailWindowName, mdCodeDetailProcId);	
	}

}


CString LFZRegiDlg::GetFormula()
{

	CString olObject ="NTS";
	CString olFields = "URNO,FDES";
	CString olWhere = "WHERE FNAM='ACR_WEIGHT_FORMULA' AND STAT='C'";
	CCSPtrArray<RecordSet> olBuffer;
	RecordSet olRecord;

	ogBCD.SetObject(olObject,olFields);
	int ilDataCount= ogBCD.ReadSpecial(olObject, olFields, olWhere, olBuffer);
	//int ilDataCount= ogBCD.Read(olObject, olWhere, olBuffer,true);


	omNtsFormula="";
	if(ilDataCount>0)
	{
		
		//BOOL blRead=ogBCD.GetRecord(olObject, 0,  olRecord);
		//omNtsFormula = olRecord.Values[ogBCD.GetFieldIndex(olObject,"FDES")];	
		
		if(olBuffer.GetSize() == 1)
		{
			olRecord = olBuffer[0];
			omNtsFormula = olRecord.Values[1];	//olRecord.Values[ogBCD.GetFieldIndex(olObject,"FDES")];
		}
	}	
	
	return omNtsFormula;

}


void LFZRegiDlg::OnBlackList() 
{
	
	char pclCheck[256];
	char pclConfigPath[256];
	
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "BDPSUIF_ADDON" , "BDPS-UIF", pclCheck, sizeof pclCheck, pclConfigPath);		

	
	//BWA_ACR ,URNO1,URNO2|UserName,Permit,Time|100,100|Password
	char clStat = ogPrivList.GetStat("LFZREGISTRATIONDLG.m_BLACK_LIST");
	if(pomACR != NULL)
	{
		char buf[33];
		mdBlackListProcId = LaunchTool( "BWA_ACR", CString(itoa(pomACR->Urno, buf, 10)), "", clStat, "BDPSUIF_ADDON",pclCheck , mdBlackListProcId );
	}
	
}
