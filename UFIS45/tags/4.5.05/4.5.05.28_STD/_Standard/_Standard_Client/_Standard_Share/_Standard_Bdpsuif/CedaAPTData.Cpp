// CedaAPTData.cpp
 
#include <stdafx.h>
#include <CedaAPTData.h>
#include <resource.h>
#include <resrc1.h>

// Local function prototype
static void ProcessAPTCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaAPTData::CedaAPTData() : CCSCedaData(&ogCommHandler)
{
    // Create an array of CEDARECINFO for APTDATA
	BEGIN_CEDARECINFO(APTDATA,APTDataRecInfo)
		FIELD_LONG     (Urno,"URNO")
		FIELD_DATE	   (Cdat,"CDAT")
		FIELD_CHAR_TRIM(Usec,"USEC")
		FIELD_DATE	   (Lstu,"LSTU")
		FIELD_CHAR_TRIM(Prfl,"PRFL")
		FIELD_CHAR_TRIM(Useu,"USEU")
		FIELD_CHAR_TRIM(Apc3,"APC3")
		FIELD_CHAR_TRIM(Apc4,"APC4")
		FIELD_CHAR_TRIM(Apfn,"APFN")
		FIELD_CHAR_TRIM(Land,"LAND")
		FIELD_CHAR_TRIM(Aptt,"APTT")
		FIELD_CHAR_TRIM(Etof,"ETOF")
		FIELD_DATE	   (Tich,"TICH")
		FIELD_CHAR_TRIM(Tdi1,"TDI1")
		FIELD_CHAR_TRIM(Tdi2,"TDI2")
		FIELD_CHAR_TRIM(Apsn,"APSN")
		FIELD_CHAR_TRIM(Apn2,"APN2")
		FIELD_CHAR_TRIM(Apn3,"APN3")
		FIELD_CHAR_TRIM(Apn4,"APN4")
		FIELD_DATE	   (Vafr,"VAFR")
		FIELD_DATE	   (Vato,"VATO")
		FIELD_CHAR_TRIM(Home,"HOME")
		FIELD_CHAR_TRIM(Tdis,"TDIS")
		FIELD_CHAR_TRIM(Tdiw,"TDIW")
		FIELD_CHAR_TRIM(Cont,"CONT")
	END_CEDARECINFO //(APTDataAStruct)

	// Copy the record structure
	for (int i=0; i< sizeof(APTDataRecInfo)/sizeof(APTDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&APTDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"APT");
	strcpy(pcmAPTFieldList,"URNO,CDAT,USEC,LSTU,PRFL,USEU,APC3,APC4,APFN,LAND,APTT,ETOF,TICH,TDI1,TDI2,APSN,APN2,APN3,APN4,VAFR,VATO,HOME,TDIS,TDIW");

	//Added by Christine
	if(ogBasicData.IsContinentAvailable())
	{
		strcat(pcmAPTFieldList,",CONT");
	}

	pcmFieldList = pcmAPTFieldList;

	omData.SetSize(0,1000);
	//omUrnoMap.InitHashTable(2001);
}

//----------------------------------------------------------------------------------------------------

void CedaAPTData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("APC3");
	ropFields.Add("APC4");
	ropFields.Add("APFN");
	ropFields.Add("APSN");
	ropFields.Add("APN2");
	ropFields.Add("APN3");
	ropFields.Add("APN4");
	ropFields.Add("APTT");
	ropFields.Add("CDAT");
	ropFields.Add("ETOF");
	ropFields.Add("LAND");
	ropFields.Add("LSTU");
	ropFields.Add("PRFL");
	ropFields.Add("TDI1");
	ropFields.Add("TDI2");
	ropFields.Add("TICH");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("VAFR");
	ropFields.Add("VATO");
	ropFields.Add("HOME");
	ropFields.Add("TDIS");
	ropFields.Add("TDIW");
	if(ogBasicData.IsContinentAvailable())
	{
		ropFields.Add("CONT");
	}

	ropDesription.Add(LoadStg(IDS_STRING495));
	ropDesription.Add(LoadStg(IDS_STRING533));
	ropDesription.Add(LoadStg(IDS_STRING534));
	ropDesription.Add(LoadStg(IDS_STRING535) + CString(" 1"));
	ropDesription.Add(LoadStg(IDS_STRING535) + CString(" 2"));
	ropDesription.Add(LoadStg(IDS_STRING535) + CString(" 3"));
	ropDesription.Add(LoadStg(IDS_STRING535) + CString(" 4"));
	ropDesription.Add(LoadStg(IDS_STRING536));
	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING537));
	ropDesription.Add(LoadStg(IDS_STRING538));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING539));
	ropDesription.Add(LoadStg(IDS_STRING540));
	ropDesription.Add(LoadStg(IDS_STRING541));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));
	ropDesription.Add(LoadStg(IDS_STRING711));
	ropDesription.Add(LoadStg(IDS_STRING990));
	ropDesription.Add(LoadStg(IDS_STRING991));
	
	//Added by Christine
	if(ogBasicData.IsContinentAvailable())
	{
		ropDesription.Add(LoadStg(IDS_STRING32827));
	}

	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	
	//Added by Christine
	if(ogBasicData.IsContinentAvailable())
	{
		ropType.Add("String");
	}

}

//----------------------------------------------------------------------------------------------------

void CedaAPTData::Register(void)
{
	ogDdx.Register((void *)this,BC_APT_CHANGE,CString("APTDATA"), CString("APT-changed"),ProcessAPTCf);
	ogDdx.Register((void *)this,BC_APT_DELETE,CString("APTDATA"), CString("APT-deleted"),ProcessAPTCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaAPTData::~CedaAPTData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaAPTData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaAPTData::Read(char *pcpWhere)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pcpWhere != NULL)
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pcpWhere);
	else
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		APTDATA *prpAPT = new APTDATA;
		if ((ilRc = GetFirstBufferRecord(prpAPT)) == true)
		{
			prpAPT->IsChanged = DATA_UNCHANGED;
			omData.Add(prpAPT);//Update omData
			omUrnoMap.SetAt((void *)prpAPT->Urno,prpAPT);
		}
		else
		{
			delete prpAPT;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaAPTData::InsertAPT(APTDATA *prpAPT,BOOL bpSendDdx)
{
	prpAPT->IsChanged = DATA_NEW;
	if(SaveAPT(prpAPT) == false) return false; //Update Database
	InsertAPTInternal(prpAPT);
    return true;
}

//--INSERT-INTERNAL-------------------------------------------------------------------------------------------

bool CedaAPTData::InsertAPTInternal(APTDATA *prpAPT)
{
	//PrepareAPTData(prpAPT);
	ogDdx.DataChanged((void *)this, APT_CHANGE,(void *)prpAPT ); //Update Viewer
	omData.Add(prpAPT);//Update omData
	omUrnoMap.SetAt((void *)prpAPT->Urno,prpAPT);
	return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaAPTData::DeleteAPT(long lpUrno)
{
	APTDATA *prlAPT = GetAPTByUrno(lpUrno);
	if (prlAPT != NULL)
	{
		prlAPT->IsChanged = DATA_DELETED;
		if(SaveAPT(prlAPT) == false) return false; //Update Database
		DeleteAPTInternal(prlAPT);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaAPTData::DeleteAPTInternal(APTDATA *prpAPT)
{
	ogDdx.DataChanged((void *)this,APT_DELETE,(void *)prpAPT); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpAPT->Urno);

	int ilAPTCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilAPTCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpAPT->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaAPTData::PrepareAPTData(APTDATA *prpAPT)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaAPTData::UpdateAPT(APTDATA *prpAPT,BOOL bpSendDdx)
{
	if (GetAPTByUrno(prpAPT->Urno) != NULL)
	{
		if (prpAPT->IsChanged == DATA_UNCHANGED)
		{
			prpAPT->IsChanged = DATA_CHANGED;
		}
		if(SaveAPT(prpAPT) == false) return false; //Update Database
		UpdateAPTInternal(prpAPT);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaAPTData::UpdateAPTInternal(APTDATA *prpAPT)
{
	APTDATA *prlAPT = GetAPTByUrno(prpAPT->Urno);
	if (prlAPT != NULL)
	{
		*prlAPT = *prpAPT; //Update omData
		ogDdx.DataChanged((void *)this,APT_CHANGE,(void *)prlAPT); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

APTDATA *CedaAPTData::GetAPTByUrno(long lpUrno)
{
	APTDATA  *prlAPT;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlAPT) == TRUE)
	{
		return prlAPT;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaAPTData::ReadSpecialData(CCSPtrArray<APTDATA> *popApt,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}

	//Added by Christine
	if(ogBasicData.IsContinentAvailable())
	{
		if( CString(pcmAPTFieldList).Find(",CONT") < 0 )
		{
			strcat(pcmAPTFieldList,",CONT");
		}
		
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","APT",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","APT",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popApt != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			APTDATA *prpApt = new APTDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpApt,CString(pclFieldList))) == true)
			{
				popApt->Add(prpApt);
			}
			else
			{
				delete prpApt;
			}
		if(popApt->GetSize() == 0) return false;
		}
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaAPTData::SaveAPT(APTDATA *prpAPT)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpAPT->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpAPT->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpAPT);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpAPT->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpAPT->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpAPT);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpAPT->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpAPT->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}

    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessAPTCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_APT_CHANGE :
	case BC_APT_DELETE :
		((CedaAPTData *)popInstance)->ProcessAPTBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaAPTData::ProcessAPTBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlAPTData = (struct BcStruct *) vpDataPointer;
	long llUrno = GetUrnoFromSelection(prlAPTData->Selection);

	APTDATA *prlAPT = GetAPTByUrno(llUrno);
	if(ipDDXType == BC_APT_CHANGE)
	{
		if (prlAPT != NULL)
		{
			GetRecordFromItemList(prlAPT,prlAPTData->Fields,prlAPTData->Data);
			if(ValidateAPTBcData(prlAPT->Urno)) //Prf: 8795
			{
				UpdateAPTInternal(prlAPT);
			}
			else
			{
				DeleteAPTInternal(prlAPT);
			}
		}
		else
		{
			prlAPT = new APTDATA;
			GetRecordFromItemList(prlAPT,prlAPTData->Fields,prlAPTData->Data);
			if(ValidateAPTBcData(prlAPT->Urno)) //Prf: 8795
			{
				InsertAPTInternal(prlAPT);
			}
			else
			{
				delete prlAPT;
			}
		}
	}
	if(ipDDXType == BC_APT_DELETE)
	{
		if (prlAPT != NULL)
		{
			DeleteAPTInternal(prlAPT);
		}
	}
}

//Prf: 8795
//--ValidateAPTBcData--------------------------------------------------------------------------------------

bool CedaAPTData::ValidateAPTBcData(const long& lrpUrno)
{
	bool blValidateAPTBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<APTDATA> olApts;
		if(!ReadSpecialData(&olApts,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateAPTBcData = false;
		}
	}
	return blValidateAPTBcData;
}
//---------------------------------------------------------------------------------------------------------
//Added for Continent selection
void CedaAPTData::SetTheSelectedContinent(CString strContinentUrno)
{
	m_strSelConUrno  = strContinentUrno;

}

//Added for Continent selection
CString CedaAPTData::GetTheSelectedContinent()
{
	return m_strSelConUrno;
}