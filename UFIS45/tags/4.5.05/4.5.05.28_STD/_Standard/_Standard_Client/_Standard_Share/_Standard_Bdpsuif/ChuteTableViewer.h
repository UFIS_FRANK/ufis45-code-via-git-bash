#ifndef __CHUTETABLEVIEWER_H__
#define __CHUTETABLEVIEWER_H__

#include <stdafx.h>
#include <CedaCHUData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>

struct CHUTETABLE_LINEDATA
{
	long	Urno;	//Unique Record Number
	CString	Cnam;   //Name
	CString	Term;   //Terminal
	CString	Sort;   //Sorter
	CString Maxf;   //Max. Flights
	CString	Vafr;   //Valid From
	CString	Vato;   //Valid To
};

/////////////////////////////////////////////////////////////////////////////
// ChuteTableViewer

class ChuteTableViewer : public CViewer
{
	// Constructions
public:
    ChuteTableViewer(CCSPtrArray<CHUDATA> *popData);
    ~ChuteTableViewer();
    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);

	// Internal data processing routines
private:
	bool IsPassFilter(CHUDATA *prpAirline);
	int  CompareChute(CHUTETABLE_LINEDATA *prpAirline1, CHUTETABLE_LINEDATA *prpAirline2);
    void MakeLines();
	void MakeLine(CHUDATA *prpAirline);
	bool FindLine(long lpUrno, int &rilLineno);
	CString PrepareDisplayDateFormat(const CString& ropDate,bool blSetDefaultIncaseEmptyDate = false) const;

	// Operations
public:
	void DeleteAll();
	void CreateLine(CHUTETABLE_LINEDATA *prpAirline);
	void DeleteLine(int ipLineno);

	// Window refreshing routines
public:
	void UpdateChute();
	void ProcessChuteChange(CHUDATA *prpAirline);
	void ProcessChuteDelete(CHUDATA *prpAirline);
	bool FindChute(char *prpChuteName, int& ilItem);

	// Attributes
private:
    CCSTable			*pomChuteTable;
	CCSPtrArray<CHUDATA>*pomData;
	
	// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<CHUTETABLE_LINEDATA> omLines;
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;

	// Print 
	void PrintTableView();
	bool PrintTableLine(int ilPage,CUIntArray& ropColsPerPage,CHUTETABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader(int ilPage,CUIntArray& ropColsPerPage);
	CCSPrint *pomPrint;	
	CString omTableName;
	CString	omFileName;
};

#endif //__CHUTETABLEVIEWER_H__
