#ifndef __DISPLAYTABLEVIEWER_H__
#define __DISPLAYTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaDSPData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct DISPLAYTABLE_LINEDATA
{
	long	Urno;
	CString	Dpid;
	CString	Dnop;
	CString	Dfpn;
	CString	Demt;
	CString	Dapn;
	CString	Dicf;
	CString	Pi[50];
	CString	Pn[50];
	CString	Vafr;
	CString	Vato;
	CString Rema;
};

/////////////////////////////////////////////////////////////////////////////
// DisplayTableViewer

class DisplayTableViewer : public CViewer
{
// Constructions
public:
    DisplayTableViewer(CCSPtrArray<DSPDATA> *popData);
    ~DisplayTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(DSPDATA *prpAirline);
	int  CompareDisplay(DISPLAYTABLE_LINEDATA *prpAirline1, DISPLAYTABLE_LINEDATA *prpAirline2);
    void MakeLines();
	void MakeLine(DSPDATA *prpAirline);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(DISPLAYTABLE_LINEDATA *prpAirline);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(DISPLAYTABLE_LINEDATA *prpLine);
	void ProcessDisplayChange(DSPDATA *prpAirline);
	void ProcessDisplayDelete(DSPDATA *prpAirline);
	bool FindDisplay(char *prpDisplayName, int& ilItem);
	bool CreateExcelFile(const CString& ropSeparator,const CString& ropListSeparator);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable			*pomDisplayTable;
	CCSPtrArray<DSPDATA>*pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<DISPLAYTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(int ilPage,CUIntArray& ropColsPerPage,DISPLAYTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader(int ilPage,CUIntArray& ropColsPerPage);
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;
	CString	omFileName;

};

#endif //__DISPLAYTABLEVIEWER_H__
