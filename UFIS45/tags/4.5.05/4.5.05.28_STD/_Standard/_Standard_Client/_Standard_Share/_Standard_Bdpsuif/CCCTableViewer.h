#ifndef __CounterclassTableViewer_H__
#define __CounterclassTableViewer_H__

#include <stdafx.h>
#include <CedaCccData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct COUNTERCLASSTABLE_LINEDATA
{
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	CString  Cicc; 	// Bemerkungen
	CString  Cicn; 	// Bemerkungen
	CString	Vafr;
	CString	Vato;

};

/////////////////////////////////////////////////////////////////////////////
// CounterclassTableViewer

	  
class CounterclassTableViewer : public CViewer
{
// Constructions
public:
    CounterclassTableViewer(CCSPtrArray<CCCDATA> *popData);
    ~CounterclassTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(CCCDATA *prpCounterclass);
	int CompareCounterclass(COUNTERCLASSTABLE_LINEDATA *prpCounterclass1, COUNTERCLASSTABLE_LINEDATA *prpCounterclass2);
    void MakeLines();
	void MakeLine(CCCDATA *prpCounterclass);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(COUNTERCLASSTABLE_LINEDATA *prpCounterclass);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(COUNTERCLASSTABLE_LINEDATA *prpLine);
	void ProcessCounterclassChange(CCCDATA *prpCounterclass);
	void ProcessCounterclassDelete(CCCDATA *prpCounterclass);
	bool FindCounterclass(char *prpCounterclassKeya, char *prpCounterclassKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomCounterClassTable;
	CCSPtrArray<CCCDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<COUNTERCLASSTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(COUNTERCLASSTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__CounterclassTableViewer_H__
