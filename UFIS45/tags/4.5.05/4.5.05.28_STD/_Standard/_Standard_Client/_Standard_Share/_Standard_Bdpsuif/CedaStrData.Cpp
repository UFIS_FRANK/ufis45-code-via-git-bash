// CedaStrData.cpp
 
#include <stdafx.h>
#include <CedaStrData.h>
#include <resource.h>


void ProcessStrCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaStrData::CedaStrData() : CCSCedaData(&ogCommHandler)
{
    BEGIN_CEDARECINFO(STRDATA, StrDataRecInfo)
		FIELD_CHAR_TRIM	(Act3,"ACT3")
		FIELD_LONG		(Actm,"ACTM")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Daya,"DAYA")
		FIELD_CHAR_TRIM	(Dayd,"DAYD")
		FIELD_CHAR_TRIM	(Flca,"FLCA")
		FIELD_CHAR_TRIM	(Flcd,"FLCD")
		FIELD_CHAR_TRIM	(Flna,"FLNA")
		FIELD_CHAR_TRIM	(Flnd,"FLND")
		FIELD_CHAR_TRIM	(Flsa,"FLSA")
		FIELD_CHAR_TRIM	(Flsd,"FLSD")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_DATE		(Vafr,"VAFR")
		FIELD_DATE		(Vato,"VATO")
	END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(StrDataRecInfo)/sizeof(StrDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&StrDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
    strcpy(pcmTableName,"STR");
    sprintf(pcmListOfFields,"ACT3,ACTM,CDAT,DAYA,DAYD,FLCA,FLCD,FLNA,FLND,FLSA,FLSD,LSTU,PRFL,URNO,USEC,USEU,VAFR,VATO");

	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaStrData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("ACT3");
	ropFields.Add("ACTM");
	ropFields.Add("CDAT");
	ropFields.Add("DAYA");
	ropFields.Add("DAYD");
	ropFields.Add("FLCA");
	ropFields.Add("FLCD");
	ropFields.Add("FLNA");
	ropFields.Add("FLND");
	ropFields.Add("FLSA");
	ropFields.Add("FLSD");
	ropFields.Add("LSTU");
	ropFields.Add("PRFL");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("VAFR");
	ropFields.Add("VATO");

	ropDesription.Add(LoadStg(IDS_STRING398));
	ropDesription.Add(LoadStg(IDS_STRING485));
	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING481));
	ropDesription.Add(LoadStg(IDS_STRING482));
	ropDesription.Add(LoadStg(IDS_STRING412));
	ropDesription.Add(LoadStg(IDS_STRING415));
	ropDesription.Add(LoadStg(IDS_STRING413));
	ropDesription.Add(LoadStg(IDS_STRING416));
	ropDesription.Add(LoadStg(IDS_STRING414));
	ropDesription.Add(LoadStg(IDS_STRING417));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));

	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaStrData::Register(void)
{
	ogDdx.Register((void *)this,BC_STR_CHANGE,	CString("STRDATA"), CString("Str-changed"),	ProcessStrCf);
	ogDdx.Register((void *)this,BC_STR_NEW,		CString("STRDATA"), CString("Str-new"),		ProcessStrCf);
	ogDdx.Register((void *)this,BC_STR_DELETE,	CString("STRDATA"), CString("Str-deleted"),	ProcessStrCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaStrData::~CedaStrData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaStrData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaStrData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		STRDATA *prlStr = new STRDATA;
		if ((ilRc = GetFirstBufferRecord(prlStr)) == true)
		{
			omData.Add(prlStr);//Update omData
			omUrnoMap.SetAt((void *)prlStr->Urno,prlStr);
		}
		else
		{
			delete prlStr;
		}
	}
    return true;

}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaStrData::Insert(STRDATA *prpStr)
{
	prpStr->IsChanged = DATA_NEW;
	if(Save(prpStr) == false) return false; //Update Database
	InsertInternal(prpStr);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaStrData::InsertInternal(STRDATA *prpStr)
{
	ogDdx.DataChanged((void *)this, STR_NEW,(void *)prpStr ); //Update Viewer
	omData.Add(prpStr);//Update omData
	omUrnoMap.SetAt((void *)prpStr->Urno,prpStr);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaStrData::Delete(long lpUrno)
{
	STRDATA *prlStr = GetStrByUrno(lpUrno);
	if (prlStr != NULL)
	{
		prlStr->IsChanged = DATA_DELETED;
		if(Save(prlStr) == false) return false; //Update Database
		DeleteInternal(prlStr);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaStrData::DeleteInternal(STRDATA *prpStr)
{
	ogDdx.DataChanged((void *)this,STR_DELETE,(void *)prpStr); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpStr->Urno);
	int ilStrCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilStrCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpStr->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaStrData::Update(STRDATA *prpStr)
{
	if (GetStrByUrno(prpStr->Urno) != NULL)
	{
		if (prpStr->IsChanged == DATA_UNCHANGED)
		{
			prpStr->IsChanged = DATA_CHANGED;
		}
		if(Save(prpStr) == false) return false; //Update Database
		UpdateInternal(prpStr);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaStrData::UpdateInternal(STRDATA *prpStr)
{
	STRDATA *prlStr = GetStrByUrno(prpStr->Urno);
	if (prlStr != NULL)
	{
		*prlStr = *prpStr; //Update omData
		ogDdx.DataChanged((void *)this,STR_CHANGE,(void *)prlStr); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

STRDATA *CedaStrData::GetStrByUrno(long lpUrno)
{
	STRDATA  *prlStr;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlStr) == TRUE)
	{
		return prlStr;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaStrData::ReadSpecialData(CCSPtrArray<STRDATA> *popStr,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","STR",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","STR",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popStr != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			STRDATA *prpStr = new STRDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpStr,CString(pclFieldList))) == true)
			{
				popStr->Add(prpStr);
			}
			else
			{
				delete prpStr;
			}
		}
		if(popStr->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaStrData::Save(STRDATA *prpStr)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpStr->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpStr->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpStr);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpStr->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpStr->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpStr);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpStr->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpStr->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessStrCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogStrData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaStrData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlStrData;
	prlStrData = (struct BcStruct *) vpDataPointer;
	STRDATA *prlStr;
	if(ipDDXType == BC_STR_NEW)
	{
		prlStr = new STRDATA;
		GetRecordFromItemList(prlStr,prlStrData->Fields,prlStrData->Data);
		InsertInternal(prlStr);
	}
	if(ipDDXType == BC_STR_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlStrData->Selection);
		prlStr = GetStrByUrno(llUrno);
		if(prlStr != NULL)
		{
			GetRecordFromItemList(prlStr,prlStrData->Fields,prlStrData->Data);
			UpdateInternal(prlStr);
		}
	}
	if(ipDDXType == BC_STR_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlStrData->Selection);

		prlStr = GetStrByUrno(llUrno);
		if (prlStr != NULL)
		{
			DeleteInternal(prlStr);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
