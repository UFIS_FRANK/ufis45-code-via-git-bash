#ifndef AFX_SPFFILTERPAGE_H__495C9510_A2E4_11D1_BD3B_0000B4392C49__INCLUDED_
#define AFX_SPFFILTERPAGE_H__495C9510_A2E4_11D1_BD3B_0000B4392C49__INCLUDED_

// PsSpfFilterPage.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CPsSpfFilterPage 

class CPsSpfFilterPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CPsSpfFilterPage)

// Konstruktion
public:
	CPsSpfFilterPage();
	~CPsSpfFilterPage();

	void SetCaption(const char *pcpCaption);


// Dialogfelddaten
	//{{AFX_DATA(CPsSpfFilterPage)
	enum { IDD = IDD_PSFILTER_PAGE };
	CButton	m_RemoveButton;
	CButton	m_AddButton;
	CListBox	m_ContentList;
	CListBox	m_InsertList;
	//}}AFX_DATA


// Überschreibungen
	// Der Klassen-Assistent generiert virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CPsSpfFilterPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CPsSpfFilterPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonAdd();
	afx_msg void OnButtonRemove();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()


public:
	CStringArray omPossibleItems;
	CStringArray omSelectedItems;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_SPFFILTERPAGE_H__495C9510_A2E4_11D1_BD3B_0000B4392C49__INCLUDED_
