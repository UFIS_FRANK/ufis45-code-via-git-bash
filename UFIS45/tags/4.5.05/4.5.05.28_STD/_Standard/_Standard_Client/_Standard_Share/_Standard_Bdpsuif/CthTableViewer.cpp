// CthTableViewer.cpp 
//

#include <stdafx.h>
#include <CthTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>
#include <resrc1.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void CthTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// CthTableViewer
//

CthTableViewer::CthTableViewer(CCSPtrArray<CTHDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomCthTable = NULL;
    ogDdx.Register(this, CTH_CHANGE, CString("CTHTABLEVIEWER"), CString("Cth Update"), CthTableCf);
    ogDdx.Register(this, CTH_NEW,    CString("CTHTABLEVIEWER"), CString("Cth New"),    CthTableCf);
    ogDdx.Register(this, CTH_DELETE, CString("CTHTABLEVIEWER"), CString("Cth Delete"), CthTableCf);
}

//-----------------------------------------------------------------------------------------------

CthTableViewer::~CthTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void CthTableViewer::Attach(CCSTable *popTable)
{
    pomCthTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void CthTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void CthTableViewer::MakeLines()
{
	int ilCthCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilCthCount; ilLc++)
	{
		CTHDATA *prlCthData = &pomData->GetAt(ilLc);
		MakeLine(prlCthData);
	}
}

//-----------------------------------------------------------------------------------------------

void CthTableViewer::MakeLine(CTHDATA *prpCth)
{

    //if( !IsPassFilter(prpCth)) return;

    // Update viewer data for this shift record
    CTHTABLE_LINEDATA rlCth;

	rlCth.Urno = prpCth->Urno;
	rlCth.Cthg = prpCth->Cthg;
	rlCth.Year = prpCth->Year;
	rlCth.Absf = prpCth->Absf;
	rlCth.Abst = prpCth->Abst;
	rlCth.Redu = prpCth->Redu;

	CreateLine(&rlCth);
}

//-----------------------------------------------------------------------------------------------

void CthTableViewer::CreateLine(CTHTABLE_LINEDATA *prpCth)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareCth(prpCth, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	CTHTABLE_LINEDATA rlCth;
	rlCth = *prpCth;
    omLines.NewAt(ilLineno, rlCth);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void CthTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 13;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomCthTable->SetShowSelection(TRUE);
	pomCthTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	int ilPos = 1;
	rlHeader.Length = 30; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING773),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 40; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING773),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	//uhi 26.3.01
	rlHeader.Length = 40; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING773),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 70; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING773),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 70; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING773),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	
	pomCthTable->SetHeaderFields(omHeaderDataArray);
	pomCthTable->SetDefaultSeparator();
	pomCthTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Cthg;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Year;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_RIGHT;
		//uhi 26.3.01
		rlColumnData.Text = omLines[ilLineNo].Redu;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Absf;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Abst;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		

		pomCthTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
		rlColumnData.Alignment = COLALIGN_LEFT;
	}
	pomCthTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString CthTableViewer::Format(CTHTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool CthTableViewer::FindCth(char *pcpCthKeya, char *pcpCthKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpCthKeya) &&
			 (omLines[ilItem].Keyd == pcpCthKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void CthTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    CthTableViewer *polViewer = (CthTableViewer *)popInstance;
    if (ipDDXType == CTH_CHANGE || ipDDXType == CTH_NEW) polViewer->ProcessCthChange((CTHDATA *)vpDataPointer);
    if (ipDDXType == CTH_DELETE) polViewer->ProcessCthDelete((CTHDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void CthTableViewer::ProcessCthChange(CTHDATA *prpCth)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpCth->Cthg;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpCth->Year;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_RIGHT;
	//uhi 26.3.01
	rlColumn.Text = prpCth->Redu;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpCth->Absf;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpCth->Abst;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	
	if (FindLine(prpCth->Urno, ilItem))
	{
        CTHTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Cthg = prpCth->Cthg;
		prlLine->Year = prpCth->Year;
		//uhi 26.3.01
		prlLine->Redu = prpCth->Redu;
		prlLine->Absf = prpCth->Absf;
		prlLine->Abst = prpCth->Abst;
		prlLine->Urno = prpCth->Urno;

		pomCthTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomCthTable->DisplayTable();
	}
	else
	{
		MakeLine(prpCth);
		if (FindLine(prpCth->Urno, ilItem))
		{
	        CTHTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomCthTable->AddTextLine(olLine, (void *)prlLine);
				pomCthTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void CthTableViewer::ProcessCthDelete(CTHDATA *prpCth)
{
	int ilItem;
	if (FindLine(prpCth->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomCthTable->DeleteTextLine(ilItem);
		pomCthTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool CthTableViewer::IsPassFilter(CTHDATA *prpCth)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int CthTableViewer::CompareCth(CTHTABLE_LINEDATA *prpCth1, CTHTABLE_LINEDATA *prpCth2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpCth1->Tifd == prpCth2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpCth1->Tifd > prpCth2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void CthTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool CthTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void CthTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void CthTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING772);
	int ilOrientation = PRINT_PORTRAET;// PRINT_PORTRAET   PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool CthTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = 5; //omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool CthTableViewer::PrintTableLine(CTHTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = 5; //omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Cthg;
				break;
			case 1:
				rlElement.Text		= prpLine->Year;
				break;
			//uhi 26.3.01 
			case 2:
				rlElement.Alignment  = PRINT_RIGHT;
				rlElement.Text		= prpLine->Redu;
				break;
			case 3:
				rlElement.Alignment   = PRINT_RIGHT;
				rlElement.Text		= prpLine->Absf;
				break;
			case 4:
				rlElement.Alignment  = PRINT_RIGHT;
				rlElement.FrameRight  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Abst;
				break;
			
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
