// ChgReasonCodeDlg.cpp : implementation file   //PRF 8378
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <ChgReasonCodeDlg.h>
#include <PrivList.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ChgReasonCodeDlg dialog


ChgReasonCodeDlg::ChgReasonCodeDlg(CRCDATA *popCrc, CWnd* pParent /*=NULL*/)
	: CDialog(ChgReasonCodeDlg::IDD, pParent)
{
	pomCrc = popCrc;
	//{{AFX_DATA_INIT(ChgReasonCodeDlg)
	//}}AFX_DATA_INIT
}


void ChgReasonCodeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ChgReasonCodeDlg)
	DDX_Control(pDX, IDC_CODE, m_CODE);
	DDX_Control(pDX, IDC_REMA, m_REMA);	
	DDX_Control(pDX, IDC_GATF, m_GATF);
	DDX_Control(pDX, IDC_CXXF, m_CXXF);
	DDX_Control(pDX, IDC_BELT, m_BELT);
	DDX_Control(pDX, IDC_POSF, m_POSF);
	DDX_Control(pDX, IDC_VAFR_D, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T, m_VATOT);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEC, m_USEC);
	DDX_Control(pDX, IDC_USEU, m_USEU);
	DDX_Control(pDX, IDOK, m_OK);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ChgReasonCodeDlg, CDialog)
	//{{AFX_MSG_MAP(ChgReasonCodeDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ChgReasonCodeDlg message handlers
//----------------------------------------------------------------------------------------

BOOL ChgReasonCodeDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_Caption = LoadStg(IDS_STRING32795) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("CHGREASONCODEDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CODE.SetFormat("X(5)");
	m_CODE.SetTextLimit(1,5);
	m_CODE.SetBKColor(YELLOW);
	m_CODE.SetTextErrColor(RED);
	m_CODE.SetInitText(pomCrc->Code);
	m_CODE.SetSecState(ogPrivList.GetStat("CHGREASONCODEDLG.m_CODE"));

	m_REMA.SetFormat("X(128)");
	m_REMA.SetTextLimit(1,128);
	m_REMA.SetBKColor(YELLOW);
	m_REMA.SetTextErrColor(RED);
	m_REMA.SetInitText(pomCrc->Rema);
	m_REMA.SetSecState(ogPrivList.GetStat("CHGREASONCODEDLG.m_REMA"));

	//------------------------------------
	if (pomCrc->Gatf[0] == 'X') m_GATF.SetCheck(1);
	clStat = ogPrivList.GetStat("CHGREASONCODEDLG.m_GATF");
	SetWndStatAll(clStat,m_GATF);
	//------------------------------------
	if (pomCrc->Posf[0] == 'X') m_POSF.SetCheck(1);
	clStat = ogPrivList.GetStat("CHGREASONCODEDLG.m_POSF");
	SetWndStatAll(clStat,m_POSF);

	if (pomCrc->Cxxf[0] == 'X') m_CXXF.SetCheck(1);
	clStat = ogPrivList.GetStat("CHGREASONCODEDLG.m_CXXF");
	if (bgCxxfDisp){
		SetWndStatAll(clStat,m_CXXF);
	}else{
		SetWndStatAll('-',m_CXXF);
	}


	if (pomCrc->Belt[0] == 'X') m_BELT.SetCheck(1);//UFIS-1184
	clStat = ogPrivList.GetStat("CHGREASONCODEDLG.m_BELT");
	SetWndStatAll(clStat,m_BELT);

	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(pomCrc->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("CHGREASONCODEDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetBKColor(YELLOW);
	m_VAFRT.SetInitText(pomCrc->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("CHGREASONCODEDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomCrc->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("CHGREASONCODEDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomCrc->Vato.Format("%H:%M"));
	m_VATOT.SetSecState(ogPrivList.GetStat("CHGREASONCODEDLG.m_VATO"));
	//------------------------------------
	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomCrc->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("CHGREASONCODEDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomCrc->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("CHGREASONCODEDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomCrc->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("CHGREASONCODEDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomCrc->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("CHGREASONCODEDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomCrc->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("CHGREASONCODEDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomCrc->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("CHGREASONCODEDLG.m_USEU"));
	//------------------------------------

	return TRUE;  
}

//----------------------------------------------------------------------------------------

void ChgReasonCodeDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_CODE.GetStatus() == false)
	{
		ilStatus = false;
		if(m_CODE.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING32797) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING32797) + ogNotFormat;
		}
	}
	if(m_REMA.GetStatus() == false)
	{
		ilStatus = false;
		if(m_REMA.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING32798) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING32798) + ogNotFormat;
		}
	}
	
	///////////////////////////////////////////////////////////////////
	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}
	///////////////////////////////////////////////////////////////////
	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}
	///////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		//Konsistenz-Check//
		CString olText;
		if(m_CODE.GetWindowTextLength() != 0)
		{
			CCSPtrArray<CRCDATA> olCrcData;
			char clWhere[100];
			m_CODE.GetWindowText(olText);
			sprintf(clWhere,"WHERE CODE='%s'",olText);

			if(ogCrcData.ReadSpecialData(&olCrcData,clWhere,"URNO,CODE",false) == true)
			{
				if(olCrcData[0].Urno != pomCrc->Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING32797) + LoadStg(IDS_EXIST);
				}
			}
			olCrcData.DeleteAll();
		}
	}
	
	
	if (ilStatus == true)
	{
		m_CODE.GetWindowText(pomCrc->Code,5);
		m_REMA.GetWindowText(pomCrc->Rema,130);		
		(m_GATF.GetCheck() == 1) ? pomCrc->Gatf[0] = 'X' : pomCrc->Gatf[0] = ' ';
		(m_POSF.GetCheck() == 1) ? pomCrc->Posf[0] = 'X' : pomCrc->Posf[0] = ' ';
		(m_CXXF.GetCheck() == 1) ? pomCrc->Cxxf[0] = 'X' : pomCrc->Cxxf[0] = ' ';
		(m_BELT.GetCheck() == 1) ? pomCrc->Belt[0] = 'X' : pomCrc->Belt[0] = ' ';

		pomCrc->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomCrc->Vato = DateHourStringToDate(olVatod,olVatot);

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_CODE.SetFocus();
	}	

	
}

//----------------------------------------------------------------------------------------
