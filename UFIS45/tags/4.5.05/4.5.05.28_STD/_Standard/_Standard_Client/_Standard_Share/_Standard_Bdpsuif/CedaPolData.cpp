// CedaPolData.cpp
 
#include <stdafx.h>
#include <CedaPolData.h>
#include <resource.h>


void ProcessPolCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaPolData::CedaPolData() : CCSCedaData(&ogCommHandler)
{ 
	// Create an array of CEDARECINFO for PolDataStruct
	BEGIN_CEDARECINFO(POLDATA,PolDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Name,"NAME")
		FIELD_CHAR_TRIM	(Pool,"POOL")
		FIELD_CHAR_TRIM	(Dtel,"DTEL")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
	END_CEDARECINFO //(PolDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(PolDataRecInfo)/sizeof(PolDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&PolDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"POL");
	strcpy(pcmListOfFields,"CDAT,LSTU,NAME,POOL,DTEL,URNO,USEC,USEU");
	
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer
	
	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaPolData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("LSTU");
	ropFields.Add("NAME");
	ropFields.Add("POOL");
	ropFields.Add("DTEL");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING844));
	ropDesription.Add(LoadStg(IDS_STRING845));
	ropDesription.Add(LoadStg(IDS_STRING846));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));

	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaPolData::Register(void)
{
	ogDdx.Register((void *)this,BC_POL_CHANGE,	CString("POLDATA"), CString("Pol-changed"),	ProcessPolCf);
	ogDdx.Register((void *)this,BC_POL_NEW,		CString("POLDATA"), CString("Pol-new"),		ProcessPolCf);
	ogDdx.Register((void *)this,BC_POL_DELETE,	CString("POLDATA"), CString("Pol-deleted"),	ProcessPolCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaPolData::~CedaPolData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaPolData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaPolData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		POLDATA *prlPol = new POLDATA;
		if ((ilRc = GetFirstBufferRecord(prlPol)) == true)
		{
			omData.Add(prlPol);//Update omData
			omUrnoMap.SetAt((void *)prlPol->Urno,prlPol);
		}
		else
		{
			delete prlPol;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaPolData::Insert(POLDATA *prpPol)
{
	prpPol->IsChanged = DATA_NEW;
	if(Save(prpPol) == false) return false; //Update Database
	InsertInternal(prpPol);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaPolData::InsertInternal(POLDATA *prpPol)
{
	ogDdx.DataChanged((void *)this, POL_NEW,(void *)prpPol ); //Update Viewer
	omData.Add(prpPol);//Update omData
	omUrnoMap.SetAt((void *)prpPol->Urno,prpPol);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaPolData::Delete(long lpUrno)
{
	POLDATA *prlPol = GetPolByUrno(lpUrno);
	if (prlPol != NULL)
	{
		prlPol->IsChanged = DATA_DELETED;
		if(Save(prlPol) == false) return false; //Update Database
		DeleteInternal(prlPol);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaPolData::DeleteInternal(POLDATA *prpPol)
{
	ogDdx.DataChanged((void *)this,POL_DELETE,(void *)prpPol); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpPol->Urno);
	int ilPolCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilPolCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpPol->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaPolData::Update(POLDATA *prpPol)
{
	if (GetPolByUrno(prpPol->Urno) != NULL)
	{
		if (prpPol->IsChanged == DATA_UNCHANGED)
		{
			prpPol->IsChanged = DATA_CHANGED;
		}
		if(Save(prpPol) == false) return false; //Update Database
		UpdateInternal(prpPol);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaPolData::UpdateInternal(POLDATA *prpPol)
{
	POLDATA *prlPol = GetPolByUrno(prpPol->Urno);
	if (prlPol != NULL)
	{
		*prlPol = *prpPol; //Update omData
		ogDdx.DataChanged((void *)this,POL_CHANGE,(void *)prlPol); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

POLDATA *CedaPolData::GetPolByUrno(long lpUrno)
{
	POLDATA  *prlPol;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlPol) == TRUE)
	{
		return prlPol;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaPolData::ReadSpecialData(CCSPtrArray<POLDATA> *popPol,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","POL",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","POL",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popPol != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			POLDATA *prpPol = new POLDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpPol,CString(pclFieldList))) == true)
			{
				popPol->Add(prpPol);
			}
			else
			{
				delete prpPol;
			}
		}
		if(popPol->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaPolData::Save(POLDATA *prpPol)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpPol->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpPol->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpPol);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpPol->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpPol->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpPol);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpPol->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpPol->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessPolCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogPolData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaPolData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlPolData;
	prlPolData = (struct BcStruct *) vpDataPointer;
	POLDATA *prlPol;
	if(ipDDXType == BC_POL_NEW)
	{
		prlPol = new POLDATA;
		GetRecordFromItemList(prlPol,prlPolData->Fields,prlPolData->Data);
		if(ValidatePolBcData(prlPol->Urno)) //Prf: 8795
		{
			InsertInternal(prlPol);
		}
		else
		{
			delete prlPol;
		}
	}
	if(ipDDXType == BC_POL_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlPolData->Selection);
		prlPol = GetPolByUrno(llUrno);
		if(prlPol != NULL)
		{
			GetRecordFromItemList(prlPol,prlPolData->Fields,prlPolData->Data);
			if(ValidatePolBcData(prlPol->Urno)) //Prf: 8795
			{
				UpdateInternal(prlPol);
			}
			else
			{
				DeleteInternal(prlPol);
			}
		}
	}
	if(ipDDXType == BC_POL_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlPolData->Selection);

		prlPol = GetPolByUrno(llUrno);
		if (prlPol != NULL)
		{
			DeleteInternal(prlPol);
		}
	}
}

//Prf: 8795
//--ValidatePolBcData--------------------------------------------------------------------------------------

bool CedaPolData::ValidatePolBcData(const long& lrpUrno)
{
	bool blValidatePolBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<POLDATA> olPols;
		if(!ReadSpecialData(&olPols,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidatePolBcData = false;
		}
	}
	return blValidatePolBcData;
}

//---------------------------------------------------------------------------
bool CedaPolData::PoolNameExits(CString opNewPoolName)
{
	bool blExits = false;
	CString olNewPoolName  = opNewPoolName;
	CString olPoolName  = "";

	olNewPoolName.TrimRight();
	for(int illc1 = 0; illc1 < omData.GetSize() && !blExits; illc1++)
	{
		olPoolName = omData[illc1].Name;
		olPoolName.TrimRight();
		if(olPoolName == olNewPoolName)
		{
			blExits = true;
		}
	}
	return blExits;
}

long	CedaPolData::GetUrnoByName(const CString& ropName)
{
	for (int i = 0; i < this->omData.GetSize(); i++)
	{
		if (this->omData[i].Name == ropName)
			return this->omData[i].Urno;
	}

	return 0;
}

CString	CedaPolData::GetNameByUrno(long lpUrno)
{
	POLDATA *polData = this->GetPolByUrno(lpUrno);
	if (!polData)
		return "";
	else
		return polData->Name;
}



