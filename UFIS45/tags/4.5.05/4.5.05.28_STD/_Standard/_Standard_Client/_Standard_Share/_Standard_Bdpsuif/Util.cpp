// UtilUC.cpp: implementation of the UtilUC class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Util.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


#include <CCSGlobl.h>
#include <BDPSUIF.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

UtilUC::UtilUC()
{

}

UtilUC::~UtilUC()
{

}


bool UtilUC::ConvByteArrToOctSt( BYTE* pbpByteArr, int len, CString& octVal )
{
	bool isOk = true;
	
	for (int i=0; i<len; i++)
	{
		char* tmpSt= new char[6];
		isOk = ConvByteToOctSt( pbpByteArr[i] , tmpSt );
		if (isOk)
		{
			//octVal = strcat( octVal, tmpSt);
			octVal = octVal + tmpSt;
		}
		else
		{
			break;
		}
	}
	return isOk;
}


bool UtilUC::ConvByteToOctSt( BYTE bpByte, char* pcpSt )
{
	bool isOk = true;
	char lowCh,midCh,hiCh;

	isOk = ConvIntToOctChar( ((int) (bpByte & 07)), lowCh );//Lower 3 bits
	isOk = ConvIntToOctChar( ((int) ((bpByte>>3) & 07)), midCh );//Middle 3 bits
	isOk = ConvIntToOctChar( ((int) ((bpByte>>6) & 07 )), hiCh );//Higher 2 bits
	if (isOk)
	{
		pcpSt[0] = '0';
		pcpSt[1] = '0';
		pcpSt[2] = '0';
		pcpSt[3] = hiCh;
		pcpSt[4] = midCh;
		pcpSt[5] = lowCh;
		pcpSt[6] = '\0';
	}else{
		pcpSt[0] = '\0';
	}
	return isOk;
}


bool UtilUC::ConvIntToOctChar( int num, char& ch )
{
	bool isOk = true;
    switch( num )
	{
		case 0  : ch = '0';	break;
		case 1  : ch = '1'; break;
		case 2  : ch = '2'; break;
		case 3  : ch = '3'; break;
		case 4  : ch = '4'; break;
		case 5  : ch = '5'; break;
		case 6  : ch = '6'; break;
		case 7  : ch = '7'; break;
		default : isOk = false;
	}
	return isOk;
}


bool UtilUC::ConvOctStToByteArr( CString pspSt, CString& pbpByteArr)
{
	bool isOk = true;
	int len = 0;
	//len = (strlen( pspSt )/sizeof(char));
	len = pspSt.GetLength();
	int res = len%3;
	len = len - res;
	if (len<0) len = 0;
	int byteCnt = 0;
	char olCh ;
	for (int i=0; i<len; i= i+6)
	{
		if(pspSt.GetLength() > i+5)
		{
			char tmpSt[3];
			tmpSt[0] = pspSt.GetAt(i+3);
			tmpSt[1] = pspSt.GetAt(i+4);
			tmpSt[2] = pspSt.GetAt(i+5);
			tmpSt[3] = '\0';
			BYTE num = 0;
			isOk = ConvOctStToByte( tmpSt, num );
			if (isOk)
			{
				olCh = (char)num;
				pbpByteArr = pbpByteArr + olCh;
				//pbpByteArr[byteCnt++] = num;
			}
			else
			{
				break;
			}
		}
	}
	if (!isOk) byteCnt = 0;
	return isOk;
}


bool UtilUC::ConvOctStToByte( char* st, BYTE& bpByte )
{//Convert 3 Oct Digits in String to Byte
	bpByte = 0;
	bool isOk = true;

	int lowNum,midNum,hiNum;
    if (strlen(st)!=3) isOk = false; //expecting 3 chars only

	if (isOk)
	{
		isOk = ConvOctCharToInt(st[0], hiNum);//convert higher 2 bits
		isOk = ConvOctCharToInt(st[1], midNum); //Convert middle 3 bits
		isOk = ConvOctCharToInt(st[2], lowNum); //Convert lower 3 bits
		if (isOk)
		{
			bpByte = (hiNum << 6 ) + (midNum<<3) + lowNum;
		}
	}
	return isOk;
}


bool UtilUC::ConvOctCharToInt( char cpCh , int& num)
{
	bool isOk = true;
	num = 0;
    switch( cpCh )
	{
		case '0' : num = 0;	break;
		case '1' : num = 1; break;
		case '2' : num = 2; break;
		case '3' : num = 3; break;
		case '4' : num = 4; break;
		case '5' : num = 5; break;
		case '6' : num = 6; break;
		case '7' : num = 7; break;
		default  : isOk = false;
	}
	return isOk;
}

//Launch Tool			
//Pass In			
//  1. opCmd - Command recorganized by the tool			
//  2. opArrUrno - Arrival Flight Urno OR ""			
//  3. opDepUrno - Departure Flight Urno OR ""			
//  4. opPermit  - Permission for this activity			
//         might be based on BDPS-SEC			
//         "1"/"0"/"-"/""			
//  5. opConfigName - Config id defined in CEDA.INI [FIPS] to get the Path of Tool to launch			
DWORD LaunchTool(			
	CString opCmd, 		
	CString opArrUrno, CString opDepUrno, 		
	CString opPermit,		
	CString opConfigName)		
{			
	DWORD dlProcId = NULL;		
	if (opArrUrno.IsEmpty() && opDepUrno.IsEmpty()) return dlProcId;		
			
	char pclPath[256] = "";		
	CString olPath;		
			
	if (GetConfigInfo( opConfigName, olPath ))		
	{		
		char slRunTxt[512] = ""; 	
		//CString olUser = ogBasicData.omUserID;	

			
		CString olTime = "L";	
		//if (bmLocalTime)	
		//	olTime = "L";
			
		CPoint point;	
		::GetCursorPos(&point);	
			
		//sprintf( slRunTxt,"%s,%s,%s|%s,%s,%s|%d,%d|%s", 	
		//	opCmd, opArrUrno, opDepUrno, pcgUser, opPermit, olTime, point.x, point.y, pcgPasswd );
		
		//mdBlockPstProcId = LaunchTool( "BDPS-UIF", "0", "", "", "BDPS-UIF" , omMultiBlkPstWindowName, mdBlockPstProcId );	

		sprintf( slRunTxt,"%s,%s,%s|%s,%s,%s|%d,%d|%s",	
			opCmd, opArrUrno, opDepUrno, cgUserName, opPermit, olTime, point.x, point.y,cgUserPWD);
		//"frmBlockParkingStand,222,555|UFIS$ADMIN,"",L|100,100|Password";

	
		

		//sprintf(slRunTxt, "%s", "Multiple-Blocking,222,555|UFIS$ADMIN,1,L|100,100|Password");
		//MessageBox( slRunTxt );	
			
		/*	
		char *args[4];	
		args[0] = "child";	
		args[1] = slRunTxt;	
		args[2] = NULL;	
		args[3] = NULL;	
		int ilReturn = _spawnv(_P_NOWAIT, olPath, args);	
		*/	
		dlProcId = startProcess( olPath, CString(slRunTxt));	
	}		
	return dlProcId;		
}			



DWORD LaunchTool(			
	CString opCmd, 		
	CString opArrUrno, CString opDepUrno, 		
	CString opPermit,		
	CString opConfigName,		
	CString processWindowName,		
	DWORD dpPrevId)		
{			
	DWORD returnProcId = NULL;		
	bool alive = false;		
	if ( dpPrevId!=NULL && IsProcessAlive( processWindowName, dpPrevId)) alive = true;	
	else
	returnProcId = LaunchTool( opCmd, opArrUrno, opDepUrno,		
		opPermit,	
		opConfigName);	
			
	if (alive) returnProcId = dpPrevId;		
			
	return returnProcId;		
}			
			
			
			
DWORD startProcess(CString opExePath, CString opArgs )			
{			
  BOOL bWorked;			
  STARTUPINFO suInfo;			
  PROCESS_INFORMATION procInfo;			
			
  char vip[512] = "";			
  sprintf( vip, "%s %s", 			
	  opExePath, opArgs );		
			
			
  memset (&suInfo, 0, sizeof(suInfo));			
  suInfo.cb = sizeof(suInfo);			
			
  bWorked = ::CreateProcess(opExePath,			
		     vip,      // can also be NULL			
			
		     NULL,			
		     NULL,			
		     TRUE,			
		     NORMAL_PRIORITY_CLASS,			
		     NULL,			
		     NULL,			
		     &suInfo,			
		     &procInfo);			
			
/*			
procInfo has these members			
	HANDLE hProcess;   // process handle			
	HANDLE hThread;    // primary thread handle			
	DWORD dwProcessId; // process PID			
	DWORD dwThreadId;  // thread ID			
*/			
  return procInfo.dwProcessId;			
}			
			
			
bool IsProcessAlive( CString opProcessWindowName, DWORD dpProcId )			
{			
	HWND windowHandle = FindWindow(NULL, opProcessWindowName);		
	DWORD processID = 0;		
	GetWindowThreadProcessId(windowHandle, &processID);		
	if (dpProcId==(processID)) return true;		
	else return false;		
}			
			
			
void killProcess(DWORD processPid)			
{			
  HANDLE ps = OpenProcess( SYNCHRONIZE|PROCESS_TERMINATE, 			
		                                FALSE, processPid);			
  // processPid = procInfo.dwProcessId;			
			
			
  // This function enumerates all widows,			
			
  // using the EnumWindowsProc callback			
			
  // function, passing the PID of the process			
			
  // you started earlier.			
			
  EnumWindows(EnumWindowsProc, processPid);			
			
  CloseHandle(ps) ;			
}			
			
BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM lParam)			
{			
  DWORD wndPid;			
  CString Title;			
  // lParam = procInfo.dwProcessId;			
			
			
  // This gets the windows handle and pid of enumerated window.			
			
  GetWindowThreadProcessId(hwnd, &wndPid);			
			
  // This gets the windows title text			
			
  // from the window, using the window handle			
			
  CWnd::FromHandle( hwnd )->GetWindowText(Title);			
			
  //  this makes sure that the PID matches that PID we started, and window			
			
  // text exists, before we kill it . I don't think this is really needed, 			
			
  // I included it because some apps have more than one window.			
			
  if ( wndPid == (DWORD)lParam && Title.GetLength() != 0)			
  {			
	//  Please kindly close this process			
			
	::PostMessage(hwnd, WM_CLOSE, 0, 0);			
	return false;			
  }			
  else			
  {			
	// Keep enumerating			
			
	return true;			
  }			
}			

//AM:20100421:Get the config Info from Ceda.ini					
//Return true when there is an entry, otherwise return false					
//Return the value of config in "pcpConfigValue"					
bool GetConfigInfo(const char* pcpConfigName, CString& opConfigValue ) //CString pcpConfigValue)					
{					
	opConfigValue = "";				
	bool blFound = false;				
					
	//strcpy( pcpConfigValue, "" );				
	CString olKey;				
					
	olKey.Format( "%s", pcpConfigName );				
					
	if (ogConfigMap.Lookup( olKey, opConfigValue))				
	{				
		blFound = true;			
	}				
	else				
	{				
		char pclConfigPath[256];			
		char pclValue[512];			
					
		if (getenv("CEDA") == NULL)			
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 		
		else			
			strcpy(pclConfigPath, getenv("CEDA"));		
			GetPrivateProfileString(ogAppName, pcpConfigName, "DEFAULT",		
				pclValue, sizeof pclValue, pclConfigPath);	

			

		if(!strcmp(pclValue, "DEFAULT"))			
		{			
			//char pclErrMsg[256] = "";		
			//sprintf( pclErrMsg, "Unable to get the setting for <%s>. Please check in <%s>", pcpConfigName, pclConfigPath );		
			//MessageBox(pclErrMsg, GetString(ST_FEHLER), MB_ICONERROR);		
			//CFPMSApp::MyTopmostMessageBox(NULL, pclErrMsg, GetString(ST_FEHLER), MB_ICONERROR);		
		}			
		else			
		{			
			opConfigValue.Format( "%s", pclValue );		
			blFound = true;		
					
			//CCS_TRY		
			ogConfigMap.SetAt( olKey, pclValue );		
			//CCS_CATCH_ALL		
		}			
	}				
					
	return blFound;				
}					


//WNY:20110419:Get the config Info from Ceda.ini 
//To get the config value use GetConfigInfo() method.
//Parameter: key - key name from the ceda.ini
//Parameter: defaultValue - NO or FALSE
//Return true when the config is turned on (i.e the value in the ceda is YES or TRUE)
//Return false when the config is turned off, no value or wrong value (i.e the value in the ceda is NO or FALSE OR something else)
BOOL IsPrivateProfileOn(CString key, CString defaultValue)
{
	BOOL pclResult = FALSE;
	char pclTmpText[100];	
	char pclConfigPath[512];
	if (getenv("CEDA") == NULL)
	{	
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	}
	else
	{
		strcpy(pclConfigPath, getenv("CEDA"));
	}

	GetPrivateProfileString(pcgAppName, key, defaultValue,pclTmpText, sizeof pclTmpText, pclConfigPath);

	if(!strcmp(pclTmpText,defaultValue))
	{
		pclResult = FALSE;
	}
	else
	{
		if(!strcmp(defaultValue,"NO"))
		{
			if(!strcmp(pclTmpText,"YES"))
			pclResult = TRUE;
		}
		else if(!strcmp(defaultValue,"FALSE"))
		{
			if(!strcmp(pclTmpText,"TRUE"))
			pclResult = TRUE;	
		}
	}
	return pclResult;
}