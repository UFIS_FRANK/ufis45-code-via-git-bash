// WisDlg.cpp : implementation file
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <WisDlg.h>
#include <CheckReferenz.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWisDlg dialog


CWisDlg::CWisDlg(WISDATA *popWis, CWnd* pParent /*=NULL*/) : CDialog(CWisDlg::IDD, pParent)
{
	pomWis	  = popWis;	
	pomStatus = NULL;	
	
	
	//{{AFX_DATA_INIT(CWisDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CWisDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CWisDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_WISC, m_WISC);
	DDX_Control(pDX, IDC_WISD, m_WISD);
	DDX_Control(pDX, IDC_ORGC, m_ORGC);
	DDX_Control(pDX, IDC_REMA, m_REMA);
	DDX_Control(pDX, IDC_SHIR,	m_SHIR);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEC, m_USEC);
	DDX_Control(pDX, IDC_USEU, m_USEU);
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CWisDlg, CDialog)
	//{{AFX_MSG_MAP(CWisDlg)
	ON_BN_CLICKED(IDC_B_ORG_AW, OnBOrgAw)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWisDlg message handlers

void CWisDlg::OnBOrgAw() 
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<ORGDATA> olList;
	if(ogOrgData.ReadSpecialData(&olList, "", "URNO,DPT1,DPTN", false) == true)
	{
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Dpt1));
			olCol2.Add(CString(olList[i].Dptn));
		}
		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);
		if(pomDlg->DoModal() == IDOK)
		{
			m_ORGC.SetWindowText(pomDlg->omReturnString);
			m_ORGC.SetFocus();
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONSTOP|MB_OK));
	}
	olList.DeleteAll();
	
}

BOOL CWisDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	if(m_Caption == LoadStg(IDS_STRING150))
		bmChangeAction = true;
	else
		bmChangeAction = false;

	m_Caption = LoadStg(IDS_STRING757) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);

	char clStat = ogPrivList.GetStat("WISDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomWis->Cdat.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomWis->Cdat.Format("%H:%M"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomWis->Lstu.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomWis->Lstu.Format("%H:%M"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomWis->Usec);
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomWis->Useu);
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	//------------------------------------
	//m_ORGC.SetFormat("x|#x|#x|#x|#x|#x|#x|#x|#");
	m_ORGC.SetTextLimit(1,8);
	m_ORGC.SetBKColor(YELLOW);  
	m_ORGC.SetTextErrColor(RED);
	m_ORGC.SetInitText(pomWis->Orgc);
	//m_Humi.SetSecState(ogPrivList.GetStat("AIRPORTWETDLG.m_HUMI"));
	//------------------------------------
	m_WISD.SetTypeToString("X(40)",40,0);
	m_WISD.SetTextLimit(0,40);
	m_WISD.SetTextErrColor(RED);
	m_WISD.SetInitText(pomWis->Wisd);
	//m_Temp.SetSecState(ogPrivList.GetStat("AIRPORTWETDLG.m_TEMP"));
	//------------------------------------
	//m_WISC.SetFormat("x|#x|#x|#x|#x|#x|#x|#x|#");
	m_WISC.SetTypeToString();
	m_WISC.SetTextLimit(1,8);
	m_WISC.SetBKColor(YELLOW);  
	m_WISC.SetTextErrColor(RED);
	m_WISC.SetInitText(pomWis->Wisc);
	this->omOldCode = pomWis->Wisc;

	//m_Wisc.SetSecState(ogPrivList.GetStat("AIRPORTWETDLG.m_VISI"));
	//------------------------------------
	m_REMA.SetTypeToString("X(60)",60,0);
	m_REMA.SetTextErrColor(RED);
	m_REMA.SetInitText(pomWis->Rema);
	//m_Rema.SetSecState(ogPrivList.GetStat("AIRPORTWETDLG.m_WDIR"));
	//------------------------------------
	SetWndStatAll(clStat,m_SHIR);
	if (pomWis->Shir[0] == '1') m_SHIR.SetCheck(1);
	//------------------------------------

	return TRUE;  
}

void CWisDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText,olNewCode;
	bool ilStatus = true;
	CString olTmp;
	if(m_WISC.GetStatus() == false)
	{
		m_WISC.GetWindowText(olTmp);
		if(olTmp.GetLength() == 0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING131) + ogNoData;
		}
		else
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING131) + ogNotFormat;
		}
	}
	if(m_WISD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING501) + ogNotFormat;//+ CString(" 2") 
	}
	if(m_ORGC.GetStatus() == false)
	{
		m_ORGC.GetWindowText(olTmp);
		if(olTmp.GetLength() == 0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING41) + ogNoData;
		}
		else
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING41) + ogNotFormat;//+ CString(" 2") 
		}
	}
	if(m_REMA.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING805) + ogNotFormat;//+ CString(" 2") 
	}
	///////////////////////////////////////////////////////////////////////////

	//Konsistenz-Check//
	CString olOrgc;
	if(ilStatus == true)
	{
		char clWhere[100];

		m_ORGC.GetWindowText(olOrgc);
		if(olOrgc.GetLength() >= 1)
		{
			sprintf(clWhere,"WHERE DPT1='%s'",olOrgc);
			if(ogOrgData.ReadSpecialData(NULL,clWhere,"DPT1",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING41);
			}
		}

	}
	if(ilStatus == true)
	{
		CString olText;//
		if(m_WISC.GetWindowTextLength() != 0)
		{
			CCSPtrArray<WISDATA> olWisCPA;
			char clWhere[100];
			m_WISC.GetWindowText(olText);
			olNewCode = olText;
			sprintf(clWhere,"WHERE WISC='%s' AND ORGC='%s'",olText,olOrgc);
			if(ogWisData.ReadSpecialData(&olWisCPA,clWhere,"URNO",false) == true)
			{
				if(olWisCPA[0].Urno != pomWis->Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING757) + LoadStg(IDS_EXIST);
				}
			}
			olWisCPA.DeleteAll();
		}
	}
	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if(ilStatus == true)
	{
		bool blChangeCode = true;
		if(olNewCode != omOldCode && bmChangeAction)
		{
			// Referenz Check
			CheckReferenz olCheckReferenz;
			if(pomStatus != NULL) pomStatus->SetWindowText(LoadStg(IDS_STRING120));
			int ilCount = olCheckReferenz.Check(_WIS, pomWis->Urno, omOldCode);
			if(pomStatus != NULL) pomStatus->SetWindowText(LoadStg(ST_OK));
			CString olTxt;
			if(ilCount>0)
			{
				blChangeCode = false;
				if (!ogBasicData.BackDoorEnabled())
				{
					olTxt.Format("%d %s",ilCount, LoadStg(IDS_STRING119));
					MessageBox(olTxt,LoadStg(IDS_STRING145),MB_ICONERROR);
				}
				else
				{
					olTxt.Format("%d %s",ilCount, LoadStg(IDS_STRING983));
					if(IDYES == MessageBox(olTxt,LoadStg(IDS_STRING149),(MB_ICONSTOP | MB_YESNOCANCEL | MB_DEFBUTTON2)))
					{
						blChangeCode = true;
					}
				}
			}
			// END Referenz Check
		}

		if(blChangeCode)
		{
			m_WISC.GetWindowText(pomWis->Wisc,9);
			//wandelt Return in Blank//
			CString olTemp;
			m_WISD.GetWindowText(olTemp);
			for(int i=0;i>-1;)
			{
				i = olTemp.Find("\r\n");
				if(i != -1)
				{
					if(olTemp.GetLength() > (i+2))
						olTemp = olTemp.Left(i) + " " + olTemp.Mid(i+2);
					else
						olTemp = olTemp.Left(i);
				}
			}
			strcpy(pomWis->Wisd,olTemp);
			m_ORGC.GetWindowText(pomWis->Orgc,9);
			//wandelt Return in Blank//
	//		CString olTemp;
			m_REMA.GetWindowText(olTemp);
			for(i=0;i>-1;)
			{
				i = olTemp.Find("\r\n");
				if(i != -1)
				{
					if(olTemp.GetLength() > (i+2))
						olTemp = olTemp.Left(i) + " " + olTemp.Mid(i+2);
					else
						olTemp = olTemp.Left(i);
				}
			}
			strcpy(pomWis->Rema,olTemp);

			(m_SHIR.GetCheck() == 1) ? pomWis->Shir[0] = '1' : pomWis->Shir[0] = '0';

			CDialog::OnOK();
		}
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_WISC.SetFocus();
		m_WISC.SetSel(0,-1);
	}
}
