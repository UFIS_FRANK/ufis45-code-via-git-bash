#ifndef __HANDLINGTYPETABLEVIEWER_H__
#define __HANDLINGTYPETABLEVIEWER_H__

#include <stdafx.h>
#include <CedaHTYData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct HANDLINGTYPETABLE_LINEDATA
{
	long	Urno;
	CString	Htyp;
	CString	Hnam;
	CString	Vafr;
	CString	Vato;
};

/////////////////////////////////////////////////////////////////////////////
// HandlingtypeTableViewer

class HandlingtypeTableViewer : public CViewer
{
// Constructions
public:
    HandlingtypeTableViewer(CCSPtrArray<HTYDATA> *popData);
    ~HandlingtypeTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(HTYDATA *prpHandlingtype);
	int CompareHandlingtype(HANDLINGTYPETABLE_LINEDATA *prpHandlingtype1, HANDLINGTYPETABLE_LINEDATA *prpHandlingtype2);
    void MakeLines();
	void MakeLine(HTYDATA *prpHandlingtype);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(HANDLINGTYPETABLE_LINEDATA *prpHandlingtype);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(HANDLINGTYPETABLE_LINEDATA *prpLine);
	void ProcessHandlingtypeChange(HTYDATA *prpHandlingtype);
	void ProcessHandlingtypeDelete(HTYDATA *prpHandlingtype);
	bool FindHandlingtype(char *prpHandlingtypeKeya, char *prpHandlingtypeKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomHandlingtypeTable;
	CCSPtrArray<HTYDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<HANDLINGTYPETABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(HANDLINGTYPETABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__HANDLINGTYPETABLEVIEWER_H__
