// LeistungskatalogTableViewer.cpp 
//

#include <stdafx.h>
#include <LeistungskatalogTableViewer.h>
#include <CCSDdx.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void LeistungskatalogTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// LeistungskatalogTableViewer
//

LeistungskatalogTableViewer::LeistungskatalogTableViewer(CCSPtrArray<GHSDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomLeistungskatalogTable = NULL;
    ogDdx.Register(this, GHS_NEW,	 CString("LEISTUNGSKATALOGTABLEVIEWER"), CString("Leistungskatalog New"),	 LeistungskatalogTableCf);
    ogDdx.Register(this, GHS_CHANGE, CString("LEISTUNGSKATALOGTABLEVIEWER"), CString("Leistungskatalog Update"), LeistungskatalogTableCf);
    ogDdx.Register(this, GHS_DELETE, CString("LEISTUNGSKATALOGTABLEVIEWER"), CString("Leistungskatalog Delete"), LeistungskatalogTableCf);
}

//-----------------------------------------------------------------------------------------------

LeistungskatalogTableViewer::~LeistungskatalogTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void LeistungskatalogTableViewer::Attach(CCSTable *popTable)
{
    pomLeistungskatalogTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void LeistungskatalogTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void LeistungskatalogTableViewer::MakeLines()
{
	int ilLeistungskatalogCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilLeistungskatalogCount; ilLc++)
	{
		GHSDATA *prlLeistungskatalogData = &pomData->GetAt(ilLc);
		MakeLine(prlLeistungskatalogData);
	}
}

//-----------------------------------------------------------------------------------------------

void LeistungskatalogTableViewer::MakeLine(GHSDATA *prpLeistungskatalog)
{

    //if( !IsPassFilter(prpLeistungskatalog)) return;

    // Update viewer data for this shift record
    LEISTUNGSKATALOGTABLE_LINEDATA rlLeistungskatalog;

	rlLeistungskatalog.Urno = prpLeistungskatalog->Urno; 
	rlLeistungskatalog.Lknm = prpLeistungskatalog->Lknm; 
	rlLeistungskatalog.Lkcc = prpLeistungskatalog->Lkcc; 
	rlLeistungskatalog.Lkco = prpLeistungskatalog->Lkco; 
	rlLeistungskatalog.Lkar = prpLeistungskatalog->Lkar; 
	rlLeistungskatalog.Atrn = prpLeistungskatalog->Atrn; 
	rlLeistungskatalog.Lkan = prpLeistungskatalog->Lkan; 
 	rlLeistungskatalog.Lkvb = prpLeistungskatalog->Lkvb; 
 	rlLeistungskatalog.Lknb = prpLeistungskatalog->Lknb; 
	rlLeistungskatalog.Lkda = prpLeistungskatalog->Lkda; 
	rlLeistungskatalog.Lkbz = prpLeistungskatalog->Lkbz; 
 	rlLeistungskatalog.Lkam = prpLeistungskatalog->Lkam; 
 	rlLeistungskatalog.Perm = prpLeistungskatalog->Perm; 
	rlLeistungskatalog.Vrgc = prpLeistungskatalog->Vrgc; 
	rlLeistungskatalog.Lkst = prpLeistungskatalog->Lkst; 
	rlLeistungskatalog.Disp = prpLeistungskatalog->Disp; 
	rlLeistungskatalog.Lkty = prpLeistungskatalog->Lkty; 
	rlLeistungskatalog.Lkhc = prpLeistungskatalog->Lkhc; 

	CreateLine(&rlLeistungskatalog);
}

//-----------------------------------------------------------------------------------------------

void LeistungskatalogTableViewer::CreateLine(LEISTUNGSKATALOGTABLE_LINEDATA *prpLeistungskatalog)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareLeistungskatalog(prpLeistungskatalog, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	LEISTUNGSKATALOGTABLE_LINEDATA rlLeistungskatalog;
	rlLeistungskatalog = *prpLeistungskatalog;
    omLines.NewAt(ilLineno, rlLeistungskatalog);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void LeistungskatalogTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 15;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomLeistungskatalogTable->SetShowSelection(TRUE);
	pomLeistungskatalogTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING272),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING272),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 100; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING272),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING272),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 100; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING272),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 83; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING272),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 166; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING272),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING272),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING272),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING272),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING272),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING272),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING272),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING272),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING272),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING272),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING272),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomLeistungskatalogTable->SetHeaderFields(omHeaderDataArray);
	pomLeistungskatalogTable->SetDefaultSeparator();
	pomLeistungskatalogTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Lknm;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Lkcc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Lkco;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Lkar;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Atrn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_RIGHT;
		rlColumnData.Text = omLines[ilLineNo].Lkhc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = omLines[ilLineNo].Lkan;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_RIGHT;
		rlColumnData.Text = omLines[ilLineNo].Lkam;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_LEFT;
		(omLines[ilLineNo].Lkst == 'X') ? rlColumnData.Text = cgYes : rlColumnData.Text = cgNo;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		(omLines[ilLineNo].Disp == 'X') ? rlColumnData.Text = cgYes : rlColumnData.Text = cgNo;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Lkbz;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_RIGHT;
		rlColumnData.Text = omLines[ilLineNo].Lkvb;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Lkda;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Lknb;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_LEFT;
		(omLines[ilLineNo].Perm.IsEmpty()) ? rlColumnData.Text = cgNo : rlColumnData.Text = cgYes;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Lkty;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		(omLines[ilLineNo].Vrgc.IsEmpty()) ? rlColumnData.Text = cgNo : rlColumnData.Text = cgYes;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomLeistungskatalogTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomLeistungskatalogTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString LeistungskatalogTableViewer::Format(LEISTUNGSKATALOGTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool LeistungskatalogTableViewer::FindLeistungskatalog(char *pcpLeistungskatalogKeya, char *pcpLeistungskatalogKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpLeistungskatalogKeya) &&
			 (omLines[ilItem].Keyd == pcpLeistungskatalogKeyd)    )
			return true;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void LeistungskatalogTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    LeistungskatalogTableViewer *polViewer = (LeistungskatalogTableViewer *)popInstance;
    if (ipDDXType == GHS_CHANGE || ipDDXType == GHS_NEW) polViewer->ProcessLeistungskatalogChange((GHSDATA *)vpDataPointer);
    if (ipDDXType == GHS_DELETE) polViewer->ProcessLeistungskatalogDelete((GHSDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void LeistungskatalogTableViewer::ProcessLeistungskatalogChange(GHSDATA *prpLeistungskatalog)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpLeistungskatalog->Lknm;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpLeistungskatalog->Lkcc;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpLeistungskatalog->Lkco;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpLeistungskatalog->Lkar;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpLeistungskatalog->Atrn;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_RIGHT;
	rlColumn.Text = prpLeistungskatalog->Lkhc;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_LEFT;
	rlColumn.Text = prpLeistungskatalog->Lkan;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_RIGHT;
	rlColumn.Text = prpLeistungskatalog->Lkam;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_LEFT;
	(*prpLeistungskatalog->Lkst == 'X') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	(*prpLeistungskatalog->Disp == 'X') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpLeistungskatalog->Lkbz;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_RIGHT;
	rlColumn.Text = prpLeistungskatalog->Lkvb;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpLeistungskatalog->Lkda;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpLeistungskatalog->Lknb;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_LEFT;
	(strlen(prpLeistungskatalog->Perm)>0) ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpLeistungskatalog->Lkty;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	(strlen(prpLeistungskatalog->Vrgc)>0) ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (FindLine(prpLeistungskatalog->Urno, ilItem))
	{
        LEISTUNGSKATALOGTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpLeistungskatalog->Urno;
		prlLine->Lknm = prpLeistungskatalog->Lknm;
		prlLine->Lkco = prpLeistungskatalog->Lkco;
		prlLine->Lkar = prpLeistungskatalog->Lkar;
		prlLine->Atrn = prpLeistungskatalog->Atrn;
		prlLine->Lkan = prpLeistungskatalog->Lkan;
		prlLine->Lkvb = prpLeistungskatalog->Lkvb;
		prlLine->Lknb = prpLeistungskatalog->Lknb;
		prlLine->Lkda = prpLeistungskatalog->Lkda;
		prlLine->Lkbz = prpLeistungskatalog->Lkbz;
		prlLine->Lkam = prpLeistungskatalog->Lkam;
		prlLine->Perm = prpLeistungskatalog->Perm;
		prlLine->Vrgc = prpLeistungskatalog->Vrgc;
		prlLine->Lkst = prpLeistungskatalog->Lkst;
		prlLine->Disp = prpLeistungskatalog->Disp;
		prlLine->Lkty = prpLeistungskatalog->Lkty;
		prlLine->Lkhc = prpLeistungskatalog->Lkhc;

		pomLeistungskatalogTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomLeistungskatalogTable->DisplayTable();
	}
	else
	{
		MakeLine(prpLeistungskatalog);
		if (FindLine(prpLeistungskatalog->Urno, ilItem))
		{
	        LEISTUNGSKATALOGTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomLeistungskatalogTable->AddTextLine(olLine, (void *)prlLine);
				pomLeistungskatalogTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void LeistungskatalogTableViewer::ProcessLeistungskatalogDelete(GHSDATA *prpLeistungskatalog)
{
	int ilItem;
	if (FindLine(prpLeistungskatalog->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomLeistungskatalogTable->DeleteTextLine(ilItem);
		pomLeistungskatalogTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool LeistungskatalogTableViewer::IsPassFilter(GHSDATA *prpLeistungskatalog)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int LeistungskatalogTableViewer::CompareLeistungskatalog(LEISTUNGSKATALOGTABLE_LINEDATA *prpLeistungskatalog1, LEISTUNGSKATALOGTABLE_LINEDATA *prpLeistungskatalog2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpLeistungskatalog1->Tifd == prpLeistungskatalog2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpLeistungskatalog1->Tifd > prpLeistungskatalog2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void LeistungskatalogTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool LeistungskatalogTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void LeistungskatalogTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void LeistungskatalogTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING181);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool LeistungskatalogTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		if(i!=14&&i!=16)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool LeistungskatalogTableViewer::PrintTableLine(LEISTUNGSKATALOGTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		if(i!=14&&i!=16)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Lknm;
				break;
			case 1:
				rlElement.Text		= prpLine->Lkcc;
				break;
			case 2:
				rlElement.Text		= prpLine->Lkco;
				break;
			case 3:
				rlElement.Text		= prpLine->Lkar;
				break;
			case 4:
				rlElement.Text		= prpLine->Atrn;
				break;
			case 5:
				rlElement.Alignment = PRINT_RIGHT;
				rlElement.Text		= prpLine->Lkhc;
				break;
			case 6:
				rlElement.Text		= prpLine->Lkan;
				break;
			case 7:
				rlElement.Alignment = PRINT_RIGHT;
				rlElement.Text		= prpLine->Lkam;
				break;
			case 8:
				rlElement.Text		= prpLine->Lkst;
				break;
			case 9:
				rlElement.Text		= prpLine->Disp;
				break;
			case 10:
				rlElement.Text		= prpLine->Lkbz;
				break;
			case 11:
				rlElement.Alignment = PRINT_RIGHT;
				rlElement.Text		= prpLine->Lkvb;
				break;
			case 12:
				rlElement.Alignment = PRINT_RIGHT;
				rlElement.Text		= prpLine->Lkda;
				break;
			case 13:
				rlElement.Alignment = PRINT_RIGHT;
				rlElement.Text		= prpLine->Lknb;
				break;
			case 14://nicht
				rlElement.Text		= prpLine->Perm;
				break;
			case 15:
				rlElement.FrameRight = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Lkty;
				break;
			case 16://nicht
				rlElement.Text		= prpLine->Vrgc;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
