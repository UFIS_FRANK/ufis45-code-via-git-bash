// ArbeitsvertragsartenTableViewer.cpp 
//

#include <stdafx.h>
#include <ArbeitsvertragsartenTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void ArbeitsvertragsartenTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// ArbeitsvertragsartenTableViewer
//

ArbeitsvertragsartenTableViewer::ArbeitsvertragsartenTableViewer(CCSPtrArray<COTDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomArbeitsvertragsartenTable = NULL;
    ogDdx.Register(this, COT_CHANGE, CString("ARBEITSVERTRAGSARTENTABLEVIEWER"), CString("Arbeitsvertragsarten Update"), ArbeitsvertragsartenTableCf);
    ogDdx.Register(this, COT_NEW,    CString("ARBEITSVERTRAGSARTENTABLEVIEWER"), CString("Arbeitsvertragsarten New"),    ArbeitsvertragsartenTableCf);
    ogDdx.Register(this, COT_DELETE, CString("ARBEITSVERTRAGSARTENTABLEVIEWER"), CString("Arbeitsvertragsarten Delete"), ArbeitsvertragsartenTableCf);
}

//-----------------------------------------------------------------------------------------------

ArbeitsvertragsartenTableViewer::~ArbeitsvertragsartenTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void ArbeitsvertragsartenTableViewer::Attach(CCSTable *popTable)
{
    pomArbeitsvertragsartenTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void ArbeitsvertragsartenTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void ArbeitsvertragsartenTableViewer::MakeLines()
{
	int ilArbeitsvertragsartenCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilArbeitsvertragsartenCount; ilLc++)
	{
		COTDATA *prlArbeitsvertragsartenData = &pomData->GetAt(ilLc);
		MakeLine(prlArbeitsvertragsartenData);
	}
}

//-----------------------------------------------------------------------------------------------

void ArbeitsvertragsartenTableViewer::MakeLine(COTDATA *prpArbeitsvertragsarten)
{
    // Update viewer data for this shift record
    ARBEITSVERTRAGSARTENTABLE_LINEDATA rlArbeitsvertragsarten;

	rlArbeitsvertragsarten.Urno = prpArbeitsvertragsarten->Urno;
	rlArbeitsvertragsarten.Ctrc = prpArbeitsvertragsarten->Ctrc;
	rlArbeitsvertragsarten.Ctrn = prpArbeitsvertragsarten->Ctrn;
	rlArbeitsvertragsarten.Dptc = prpArbeitsvertragsarten->Dptc;
	rlArbeitsvertragsarten.Rema = prpArbeitsvertragsarten->Rema;
	rlArbeitsvertragsarten.Whpw = prpArbeitsvertragsarten->Whpw;
	rlArbeitsvertragsarten.Inbu = prpArbeitsvertragsarten->Inbu;

	CreateLine(&rlArbeitsvertragsarten);
}

//-----------------------------------------------------------------------------------------------

void ArbeitsvertragsartenTableViewer::CreateLine(ARBEITSVERTRAGSARTENTABLE_LINEDATA *prpArbeitsvertragsarten)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareArbeitsvertragsarten(prpArbeitsvertragsarten, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	ARBEITSVERTRAGSARTENTABLE_LINEDATA rlArbeitsvertragsarten;
	rlArbeitsvertragsarten = *prpArbeitsvertragsarten;
    omLines.NewAt(ilLineno, rlArbeitsvertragsarten);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void ArbeitsvertragsartenTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	// alt: Code|Identifier|OCode|Days|h/W|D/W|MinS|MaxS|B|IR|Comment
	// neu: Code|Identifier|OCode|Days|h/Week|B|Comment

	const int ilColumns = 5;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomArbeitsvertragsartenTable->SetShowSelection(TRUE);
	pomArbeitsvertragsartenTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING254),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING254),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 70; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING254),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 58; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING254),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING254),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 450; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING254),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomArbeitsvertragsartenTable->SetHeaderFields(omHeaderDataArray);

	pomArbeitsvertragsartenTable->SetDefaultSeparator();
	pomArbeitsvertragsartenTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Ctrc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Ctrn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dptc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_RIGHT;
		rlColumnData.Text.Format("%s", omLines[ilLineNo].Whpw);
		rlColumnData.Text.Insert(2,":");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Alignment = COLALIGN_RIGHT;
		rlColumnData.Text = omLines[ilLineNo].Inbu;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = omLines[ilLineNo].Rema;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomArbeitsvertragsartenTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomArbeitsvertragsartenTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString ArbeitsvertragsartenTableViewer::Format(ARBEITSVERTRAGSARTENTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool ArbeitsvertragsartenTableViewer::FindArbeitsvertragsarten(char *pcpArbeitsvertragsartenKeya, char *pcpArbeitsvertragsartenKeyd, int& ilItem)
{
	return false;
}

//-----------------------------------------------------------------------------------------------

static void ArbeitsvertragsartenTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    ArbeitsvertragsartenTableViewer *polViewer = (ArbeitsvertragsartenTableViewer *)popInstance;
    if (ipDDXType == COT_CHANGE || ipDDXType == COT_NEW) polViewer->ProcessArbeitsvertragsartenChange((COTDATA *)vpDataPointer);
    if (ipDDXType == COT_DELETE) polViewer->ProcessArbeitsvertragsartenDelete((COTDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void ArbeitsvertragsartenTableViewer::ProcessArbeitsvertragsartenChange(COTDATA *prpArbeitsvertragsarten)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpArbeitsvertragsarten->Ctrc;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpArbeitsvertragsarten->Ctrn;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpArbeitsvertragsarten->Dptc;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_RIGHT;
	rlColumn.Text = prpArbeitsvertragsarten->Whpw;
	rlColumn.Text.Insert(2,":");
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_LEFT;
	rlColumn.Alignment = COLALIGN_RIGHT;
	rlColumn.Text = prpArbeitsvertragsarten->Inbu;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_LEFT;
	rlColumn.Text = prpArbeitsvertragsarten->Rema;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	
	if (FindLine(prpArbeitsvertragsarten->Urno, ilItem))
	{
        ARBEITSVERTRAGSARTENTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpArbeitsvertragsarten->Urno;
		prlLine->Ctrc = prpArbeitsvertragsarten->Ctrc;
		prlLine->Ctrn = prpArbeitsvertragsarten->Ctrn;
		prlLine->Dptc = prpArbeitsvertragsarten->Dptc;
		prlLine->Rema = prpArbeitsvertragsarten->Rema;
		prlLine->Whpw = prpArbeitsvertragsarten->Whpw;
		prlLine->Inbu = prpArbeitsvertragsarten->Inbu;

		pomArbeitsvertragsartenTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomArbeitsvertragsartenTable->DisplayTable();
	}
	else
	{
		MakeLine(prpArbeitsvertragsarten);
		if (FindLine(prpArbeitsvertragsarten->Urno, ilItem))
		{
	        ARBEITSVERTRAGSARTENTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomArbeitsvertragsartenTable->AddTextLine(olLine, (void *)prlLine);
				pomArbeitsvertragsartenTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void ArbeitsvertragsartenTableViewer::ProcessArbeitsvertragsartenDelete(COTDATA *prpArbeitsvertragsarten)
{
	int ilItem;
	if (FindLine(prpArbeitsvertragsarten->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomArbeitsvertragsartenTable->DeleteTextLine(ilItem);
		pomArbeitsvertragsartenTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool ArbeitsvertragsartenTableViewer::IsPassFilter(COTDATA *prpArbeitsvertragsarten)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int ArbeitsvertragsartenTableViewer::CompareArbeitsvertragsarten(ARBEITSVERTRAGSARTENTABLE_LINEDATA *prpArbeitsvertragsarten1, ARBEITSVERTRAGSARTENTABLE_LINEDATA *prpArbeitsvertragsarten2)
{
	return 0;
}

//-----------------------------------------------------------------------------------------------

void ArbeitsvertragsartenTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool ArbeitsvertragsartenTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void ArbeitsvertragsartenTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void ArbeitsvertragsartenTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING162);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for (int i = 0; i < ilLines; i++ ) 
			{
				if (pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if (pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		delete pomPrint;

		pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool ArbeitsvertragsartenTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for (int i = 0; i < ilSize; i++)
	{
		rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Text   = omHeaderDataArray[i].Text;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	return true;
}

//-----------------------------------------------------------------------------------------------

bool ArbeitsvertragsartenTableViewer::PrintTableLine(ARBEITSVERTRAGSARTENTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for (int i = 0; i < ilSize; i++)
	{
		rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameTop   = PRINT_FRAMETHIN;
		if(bpLastLine == true)
		{
			rlElement.FrameBottom = PRINT_FRAMETHIN;
		}
		else
		{
			rlElement.FrameBottom = PRINT_NOFRAME;
		}
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;

		switch(i)
		{
		case 0:
			rlElement.FrameLeft  = PRINT_FRAMETHIN;
			rlElement.Text		= prpLine->Ctrc;
			break;
		case 1:
			rlElement.Text		= prpLine->Ctrn;
			break;
		case 2:
			rlElement.Text		= prpLine->Dptc;
			break;
		case 3:
			rlElement.Alignment = PRINT_RIGHT;
			rlElement.Text = prpLine->Whpw;
			rlElement.Text.Insert(2,":");
			break;
		case 4:
			rlElement.Alignment = PRINT_CENTER;
			rlElement.Text		= prpLine->Inbu;
			break;
		case 5:
			rlElement.FrameRight = PRINT_FRAMETHIN;
			rlElement.Text		= prpLine->Rema;
			break;
		}
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
