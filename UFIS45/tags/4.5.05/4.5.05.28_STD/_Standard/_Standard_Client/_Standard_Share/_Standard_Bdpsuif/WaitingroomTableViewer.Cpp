// WaitingroomTableViewer.cpp 
//

#include <stdafx.h>
#include <WaitingroomTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>
#include <fstream.h>
#include <iomanip.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif
 
// Local function prototype
static void WaitingroomTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// WaitingroomTableViewer
//

WaitingroomTableViewer::WaitingroomTableViewer(CCSPtrArray<WRODATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomWaitingroomTable = NULL;
    ogDdx.Register(this, WRO_CHANGE, CString("WAITINGROOMTABLEVIEWER"), CString("Waitingroom Update/new"), WaitingroomTableCf);
    ogDdx.Register(this, WRO_DELETE, CString("WAITINGROOMTABLEVIEWER"), CString("Waitingroom Delete"), WaitingroomTableCf);
}

//-----------------------------------------------------------------------------------------------

WaitingroomTableViewer::~WaitingroomTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void WaitingroomTableViewer::Attach(CCSTable *popTable)
{
    pomWaitingroomTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void WaitingroomTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void WaitingroomTableViewer::MakeLines()
{
	int ilWaitingroomCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilWaitingroomCount; ilLc++)
	{
		WRODATA *prlWaitingroomData = &pomData->GetAt(ilLc);
		MakeLine(prlWaitingroomData);
	}
}

//-----------------------------------------------------------------------------------------------

void WaitingroomTableViewer::MakeLine(WRODATA *prpWaitingroom)
{
	//if( !IsPassFilter(prpWaitingroom)) return;

    // Update viewer data for this shift record
    WAITINGROOMTABLE_LINEDATA rlWaitingroom;

	rlWaitingroom.Urno = prpWaitingroom->Urno; 
	rlWaitingroom.Wnam = prpWaitingroom->Wnam; 
	rlWaitingroom.Gte1 = prpWaitingroom->Gte1; 
	rlWaitingroom.Gte2 = prpWaitingroom->Gte2; 
	rlWaitingroom.Brca = prpWaitingroom->Brca; 
	rlWaitingroom.Term = prpWaitingroom->Term; 
	rlWaitingroom.Tele = prpWaitingroom->Tele; 
	rlWaitingroom.Vafr = prpWaitingroom->Vafr.Format("%d.%m.%Y %H:%M"); 
	rlWaitingroom.Vato = prpWaitingroom->Vato.Format("%d.%m.%Y %H:%M"); 
	rlWaitingroom.Wrot = prpWaitingroom->Wrot; 
	rlWaitingroom.Shgn = prpWaitingroom->Shgn; 

	CreateLine(&rlWaitingroom);
}

//-----------------------------------------------------------------------------------------------

void WaitingroomTableViewer::CreateLine(WAITINGROOMTABLE_LINEDATA *prpWaitingroom)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareWaitingroom(prpWaitingroom, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	WAITINGROOMTABLE_LINEDATA rlWaitingroom;
	rlWaitingroom = *prpWaitingroom;
    omLines.NewAt(ilLineno, rlWaitingroom);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void WaitingroomTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	omHeaderDataArray.DeleteAll();

	pomWaitingroomTable->SetShowSelection(TRUE);
	pomWaitingroomTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	CString olString;
	if (ogBasicData.IsGatPosEnabled())
	{
		olString = LoadStg(IDS_STRING896);
	}
	else
	{
		olString = LoadStg(IDS_STRING283);
	}

	int ilPos = 1;
	rlHeader.Length = 42;													// Name
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	if (!ogBasicData.IsGatPosEnabled())
	{
		rlHeader.Length = 42;												// Gat. 1
		rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
		rlHeader.Length = 42;												// Gat. 2
		rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	}
	else
	{
		ilPos += 2;
	}
	rlHeader.Length = 42;													// L.Cap
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8;													// T
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 83;													// Phone no.
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133;													// Valid from
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133;													// Valid to
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 10;													// Type
	rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	if (ogBasicData.IsGatPosEnabled())
	{
		rlHeader.Length = 10;												// Schengen
		rlHeader.Text = GetListItem(olString,ilPos++,false,'|');
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	}

	pomWaitingroomTable->SetHeaderFields(omHeaderDataArray);
	pomWaitingroomTable->SetDefaultSeparator();
	pomWaitingroomTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Wnam;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		if (!ogBasicData.IsGatPosEnabled())
		{
			rlColumnData.Text = omLines[ilLineNo].Gte1;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = omLines[ilLineNo].Gte2;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}

		rlColumnData.Text = omLines[ilLineNo].Brca;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Term;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Tele;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Wrot;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		if (ogBasicData.IsGatPosEnabled())
		{
			rlColumnData.Text = omLines[ilLineNo].Shgn;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}

		pomWaitingroomTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomWaitingroomTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString WaitingroomTableViewer::Format(WAITINGROOMTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

BOOL WaitingroomTableViewer::FindWaitingroom(char *pcpWaitingroomKeya, char *pcpWaitingroomKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpWaitingroomKeya) &&
			 (omLines[ilItem].Keyd == pcpWaitingroomKeyd)    )
			return TRUE;*/
	}
	return FALSE;
}

//-----------------------------------------------------------------------------------------------

static void WaitingroomTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    WaitingroomTableViewer *polViewer = (WaitingroomTableViewer *)popInstance;
    switch(ipDDXType)
	{
	case WRO_CHANGE:
		polViewer->ProcessWaitingroomChange((WRODATA *)vpDataPointer);
	break;
	case WRO_DELETE:
		polViewer->ProcessWaitingroomDelete((WRODATA *)vpDataPointer);
	break;
	}
} 

//-----------------------------------------------------------------------------------------------
void WaitingroomTableViewer::ProcessWaitingroomChange(WRODATA *prpWaitingroom)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpWaitingroom->Wnam;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (!ogBasicData.IsGatPosEnabled())
	{
		rlColumn.Text = prpWaitingroom->Gte1;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpWaitingroom->Gte2;
		rlColumn.Columnno = ilColumnNo++;
	}

	rlColumn.Text = prpWaitingroom->Brca;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpWaitingroom->Term;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpWaitingroom->Tele;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpWaitingroom->Vafr.Format("%d.%m.%Y %H:%M");
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpWaitingroom->Vato.Format("%d.%m.%Y %H:%M");
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpWaitingroom->Wrot;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (ogBasicData.IsGatPosEnabled())
	{
		rlColumn.Text = prpWaitingroom->Shgn;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
	}

	if (FindLine(prpWaitingroom->Urno, ilItem))
	{
        WAITINGROOMTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpWaitingroom->Urno; 
		prlLine->Wnam = prpWaitingroom->Wnam; 
		prlLine->Gte1 = prpWaitingroom->Gte1; 
		prlLine->Gte2 = prpWaitingroom->Gte2; 
		prlLine->Brca = prpWaitingroom->Brca; 
		prlLine->Term = prpWaitingroom->Term;
		prlLine->Tele = prpWaitingroom->Tele; 
		prlLine->Vafr = prpWaitingroom->Vafr.Format("%d.%m.%Y %H:%M"); 
		prlLine->Vato = prpWaitingroom->Vato.Format("%d.%m.%Y %H:%M"); 
		prlLine->Wrot = prpWaitingroom->Wrot; 
		prlLine->Shgn = prpWaitingroom->Shgn; 

		pomWaitingroomTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomWaitingroomTable->DisplayTable();
	}
	else
	{
		MakeLine(prpWaitingroom);
		if (FindLine(prpWaitingroom->Urno, ilItem))
		{
	        WAITINGROOMTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomWaitingroomTable->AddTextLine(olLine, (void *)prlLine);
				pomWaitingroomTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void WaitingroomTableViewer::ProcessWaitingroomDelete(WRODATA *prpWaitingroom)
{
	int ilItem;
	if (FindLine(prpWaitingroom->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomWaitingroomTable->DeleteTextLine(ilItem);
		pomWaitingroomTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

BOOL WaitingroomTableViewer::IsPassFilter(WRODATA *prpWaitingroom)
{

	return TRUE;
}

//-----------------------------------------------------------------------------------------------

int WaitingroomTableViewer::CompareWaitingroom(WAITINGROOMTABLE_LINEDATA *prpWaitingroom1, WAITINGROOMTABLE_LINEDATA *prpWaitingroom2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpWaitingroom1->Tifd == prpWaitingroom2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpWaitingroom1->Tifd > prpWaitingroom2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void WaitingroomTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

BOOL WaitingroomTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return TRUE;
	}
    return FALSE;
}
//-----------------------------------------------------------------------------------------------

void WaitingroomTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void WaitingroomTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING195);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool WaitingroomTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool WaitingroomTableViewer::PrintTableLine(WAITINGROOMTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;

			if (!ogBasicData.IsGatPosEnabled())
			{
				switch(i)
				{
				case 0:
					{
						rlElement.FrameLeft  = PRINT_FRAMETHIN;
						rlElement.Text		= prpLine->Wnam;
					}
					break;
				case 1:
					{
						rlElement.Text		= prpLine->Gte1;
					}
					break;
				case 2:
					{
						rlElement.Text		= prpLine->Gte2;
					}
					break;
				case 3:
					{
						rlElement.Alignment = PRINT_RIGHT;
						rlElement.Text		= prpLine->Brca;
					}
					break;
				case 4:
					{
						rlElement.Text		= prpLine->Term;
					}
					break;
				case 5:
					{
						rlElement.Text		= prpLine->Tele;
					}
					break;
				case 6:
					{
						rlElement.Text		= prpLine->Vafr;
					}
					break;
				case 7:
					{
						rlElement.Text		= prpLine->Vato;
					}
					break;
				case 8:
					{
						rlElement.FrameRight = PRINT_FRAMETHIN;
						rlElement.Text		= prpLine->Wrot;
					}
					break;
				}
			}
			else
			{
				switch(i)
				{
				case 0:
					{
						rlElement.FrameLeft  = PRINT_FRAMETHIN;
						rlElement.Text		= prpLine->Wnam;
					}
					break;
				case 1:
					{
						rlElement.Alignment = PRINT_RIGHT;
						rlElement.Text		= prpLine->Brca;
					}
					break;
				case 2:
					{
						rlElement.Text		= prpLine->Term;
					}
					break;
				case 3:
					{
						rlElement.Text		= prpLine->Tele;
					}
					break;
				case 4:
					{
						rlElement.Text		= prpLine->Vafr;
					}
					break;
				case 5:
					{
						rlElement.Text		= prpLine->Vato;
					}
					break;
				case 6:
					{
						rlElement.Text		= prpLine->Wrot;
					}
					break;
				case 7:
					{
						rlElement.FrameRight = PRINT_FRAMETHIN;
						rlElement.Text		= prpLine->Shgn;
					}
					break;
				}
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
bool WaitingroomTableViewer::CreateExcelFile(const CString& opTrenner,const CString& ropListSeparator)
{
	ofstream of;

	CString olTmpPath = CCSLog::GetTmpPath();

	CString olTableName;
	olTableName.Format("%s -   %s", "WROTAB", (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );


	CString olFileName = olTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, olTmpPath);
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

	of.open( omFileName, ios::out);


	of  << setw(0) << "WROTAB" << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	CString olString;
	if (ogBasicData.IsGatPosEnabled())
	{
		olString = LoadStg(IDS_STRING896);
	}
	else
	{
		olString = LoadStg(IDS_STRING283);
	}

	CStringArray olStrArray;
	ExtractItemList(olString,&olStrArray,'|');

	of	<< setw(0) << olStrArray[0]	//CString("Name");
		<< setw(0) << opTrenner
	;

	if (!ogBasicData.IsGatPosEnabled())
	{
		of	<< setw(0) << olStrArray[1]	//CString("Gate 1");
			<< setw(0) << opTrenner
			<< setw(0) << olStrArray[2]	//CString("Gate 2");
		;
	}

	of	<< setw(0) << olStrArray[3]	//CString("L.Cap");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[4]	//CString("T");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[5]	//CString("Phone No");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[6]	//CString("Valid from");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[7]	//CString("Valid to");
		<< setw(0) << opTrenner
		<< setw(0) << olStrArray[8]	//CString("Type");
		;

	if (ogBasicData.IsGatPosEnabled())
	{
		of	<< setw(0) << opTrenner
			<< setw(0) << olStrArray[9]	//CString("Schengen");
			<< endl;
	}
	else
	{
		of << endl;
	}


	
	int ilCount = omLines.GetSize();
	COleDateTime olCurrTime = COleDateTime::GetCurrentTime();

	for(int i = 0; i < ilCount; i++)
	{
		WAITINGROOMTABLE_LINEDATA rlLine = omLines[i];

		of.setf(ios::left, ios::adjustfield);

		of   << setw(0) << rlLine.Wnam							// Name
		     << setw(0) << opTrenner 
		;

		if (!ogBasicData.IsGatPosEnabled())
		{
			of	<< setw(0) << rlLine.Gte1						// Gate. 1
				<< setw(0) << opTrenner 
				<< setw(0) << rlLine.Gte2						// Gate. 2
				<< setw(0) << opTrenner 
			;
		}

		of	<< setw(0) << rlLine.Brca							// L.Cap
		     << setw(0) << opTrenner 
		     << setw(0) << rlLine.Term							// Term
		     << setw(0) << opTrenner 
		     << setw(0) << rlLine.Tele							// Phone
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Vafr							// Valid from
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Vato							// Valid to
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Wrot							// Type
			 ;
		if (ogBasicData.IsGatPosEnabled())
		{
			of	<< setw(0)	<<	opTrenner
				<< setw(0)	<<	rlLine.Shgn						// Schengen
				<< endl; 
		}
		else
		{
			of	<< endl;
		}
		
	}

	of.close();
	return true;
}

//-----------------------------------------------------------------------------------------------
