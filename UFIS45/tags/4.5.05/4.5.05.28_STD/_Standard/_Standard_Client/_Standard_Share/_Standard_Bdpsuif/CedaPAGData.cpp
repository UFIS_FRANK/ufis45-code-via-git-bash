// CedaPAGData.cpp
 
#include <stdafx.h>
#include <CedaPAGData.h>
#include <resource.h>


// Local function prototype
static void ProcessPAGCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaPAGData::CedaPAGData() : CCSCedaData(&ogCommHandler)
{
    // Create an array of CEDARECINFO for PAGDATA
	BEGIN_CEDARECINFO(PAGDATA,PAGDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Pagi,"PAGI")
		FIELD_CHAR_TRIM	(Pcfn,"PCFN")
		FIELD_CHAR_TRIM	(Pdpt,"PDPT")
		FIELD_CHAR_TRIM	(Pdti,"PDTI")
		FIELD_CHAR_TRIM	(Pnof,"PNOF")
		FIELD_CHAR_TRIM	(Pfnt,"PFNT")
		FIELD_CHAR_TRIM	(Pdss,"PDSS")
		FIELD_CHAR_TRIM	(Pcti,"PCTI")
		FIELD_CHAR_TRIM	(Ptfb,"PTFB")
		FIELD_CHAR_TRIM	(Ptfe,"PTFE")
		FIELD_CHAR_TRIM	(Ptd1,"PTD1")
		FIELD_CHAR_TRIM	(Ptd2,"PTD2")
		FIELD_CHAR_TRIM	(Prfn,"PRFN")
		FIELD_CHAR_TRIM	(Ptdc,"PTDC")
		FIELD_CHAR_TRIM	(Cotb,"COTB")
	END_CEDARECINFO //(PAGDATA)

	bmRemarkExists = false;	

	// Copy the record structure
	for (int i=0; i< sizeof(PAGDataRecInfo)/sizeof(PAGDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&PAGDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"PAG");
	strcpy(pcmPAGFieldList,"URNO,CDAT,USEC,LSTU,USEU,PAGI,PCFN,PDPT,PDTI,PNOF,PFNT,PDSS,PCTI,PTFB,PTFE,PTD1,PTD2,PRFN,PTDC,COTB");
	pcmFieldList = pcmPAGFieldList;
	omData.SetSize(0,1000);
}

void CedaPAGData::AppendFields()
{	
	if(ogBasicData.DoesFieldExist(pcmTableName,"REMA") == true)
	{	
		if(strstr(pcmFieldList,"REMA") != NULL)
			return;

		bmRemarkExists = true;		

		BEGIN_CEDARECINFO(PAGDATA,PAGDataRecInfo)
			FIELD_CHAR_TRIM (Rema, "REMA")
		END_CEDARECINFO

		for (int i=0; i< sizeof(PAGDataRecInfo)/sizeof(PAGDataRecInfo[0]) ; i++)
		{
			CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
			memcpy(prpCedaRecInfo,&PAGDataRecInfo[0],sizeof(CEDARECINFO));
			omRecInfo.Add(prpCedaRecInfo);
		}
		
		strcat(pcmPAGFieldList,",REMA");
		pcmFieldList = pcmPAGFieldList;
	}
}
//---------------------------------------------------------------------------------------------------

void CedaPAGData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("URNO"); ropDesription.Add(LoadStg(IDS_STRING346)); ropType.Add("String");
	ropFields.Add("CDAT"); ropDesription.Add(LoadStg(IDS_STRING343)); ropType.Add("Date");
	ropFields.Add("USEC"); ropDesription.Add(LoadStg(IDS_STRING347)); ropType.Add("String");
	ropFields.Add("LSTU"); ropDesription.Add(LoadStg(IDS_STRING344)); ropType.Add("Date");
	ropFields.Add("USEU"); ropDesription.Add(LoadStg(IDS_STRING348)); ropType.Add("String");

	ropFields.Add("PAGI"); ropDesription.Add(LoadStg(IDS_STRING964)); ropType.Add("String");
	ropFields.Add("PCFN"); ropDesription.Add(LoadStg(IDS_STRING965)); ropType.Add("String");
	ropFields.Add("PDPT"); ropDesription.Add(LoadStg(IDS_STRING966)); ropType.Add("String");


	ropFields.Add("PDTI"); ropDesription.Add(LoadStg(IDS_STRING1076)); ropType.Add("String");
	ropFields.Add("PNOF"); ropDesription.Add(LoadStg(IDS_STRING1077)); ropType.Add("String");
	ropFields.Add("PFNT"); ropDesription.Add(LoadStg(IDS_STRING1078)); ropType.Add("String");
	ropFields.Add("PDSS"); ropDesription.Add(LoadStg(IDS_STRING1079)); ropType.Add("String");
	ropFields.Add("PCTI"); ropDesription.Add(LoadStg(IDS_STRING1080)); ropType.Add("String");
	ropFields.Add("PTFB"); ropDesription.Add(LoadStg(IDS_STRING1081)); ropType.Add("String");
	ropFields.Add("PTFE"); ropDesription.Add(LoadStg(IDS_STRING1082)); ropType.Add("String");
	ropFields.Add("PTD1"); ropDesription.Add(LoadStg(IDS_STRING1083)); ropType.Add("String");
	ropFields.Add("PTD2"); ropDesription.Add(LoadStg(IDS_STRING1084)); ropType.Add("String");
	ropFields.Add("PRFN"); ropDesription.Add(LoadStg(IDS_STRING1085)); ropType.Add("String");
	ropFields.Add("PTDC"); ropDesription.Add(LoadStg(IDS_STRING1086)); ropType.Add("String");
	ropFields.Add("DDGI"); ropDesription.Add(LoadStg(IDS_STRING1087)); ropType.Add("String");
	ropFields.Add("COTB"); ropDesription.Add(LoadStg(IDS_STRING1088)); ropType.Add("String");
	if(ogPAGData.DoesRemarkCodeExist() == true)
	{
		ropFields.Add("REMA"); ropDesription.Add(LoadStg(IDS_STRING1129)); ropType.Add("String");
	}
}

//-----------------------------------------------------------------------------------------------

void CedaPAGData::Register(void)
{
	ogDdx.Register((void *)this,BC_PAG_CHANGE,CString("PAGDATA"), CString("PAG-changed"),ProcessPAGCf);
	ogDdx.Register((void *)this,BC_PAG_DELETE,CString("PAGDATA"), CString("PAG-deleted"),ProcessPAGCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaPAGData::~CedaPAGData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaPAGData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
	omPagiMap.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaPAGData::Read(char *pcpWhere)
{
    // Select data from the database
	AppendFields();
	bool ilRc = true;
    omUrnoMap.RemoveAll();
	omPagiMap.RemoveAll();
    omData.DeleteAll();

	if(pcpWhere != NULL)
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pcpWhere);
	else
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		PAGDATA *prpPAG = new PAGDATA;
		if ((ilRc = GetFirstBufferRecord(prpPAG)) == true)
		{
			prpPAG->IsChanged = DATA_UNCHANGED;
			omData.Add(prpPAG);//Update omData
			omUrnoMap.SetAt((void *)prpPAG->Urno,prpPAG);
			omPagiMap.SetAt(prpPAG->Pagi,prpPAG);
		}
		else
		{
			delete prpPAG;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaPAGData::InsertPAG(PAGDATA *prpPAG,BOOL bpSendDdx)
{
	prpPAG->IsChanged = DATA_NEW;
	if (SavePAG(prpPAG) == false) 
		return false; //Update Database
	InsertPAGInternal(prpPAG);
    return true;
}

//--INSERT-INTERNAL-------------------------------------------------------------------------------------------

bool CedaPAGData::InsertPAGInternal(PAGDATA *prpPAG)
{
	//PreparePAGData(prpPAG);
	ogDdx.DataChanged((void *)this, PAG_CHANGE,(void *)prpPAG ); //Update Viewer
	omData.Add(prpPAG);//Update omData
	omUrnoMap.SetAt((void *)prpPAG->Urno,prpPAG);
	omPagiMap.SetAt(prpPAG->Pagi,prpPAG);

    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaPAGData::DeletePAG(long lpUrno)
{
	PAGDATA *prlPAG = GetPAGByUrno(lpUrno);
	if (prlPAG != NULL)
	{
		prlPAG->IsChanged = DATA_DELETED;
		if(SavePAG(prlPAG) == false) return false; //Update Database
		DeletePAGInternal(prlPAG);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaPAGData::DeletePAGInternal(PAGDATA *prpPAG)
{
	ogDdx.DataChanged((void *)this,PAG_DELETE,(void *)prpPAG); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpPAG->Urno);
	omPagiMap.RemoveKey(prpPAG->Pagi);

	int ilPAGCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilPAGCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpPAG->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaPAGData::PreparePAGData(PAGDATA *prpPAG)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaPAGData::UpdatePAG(PAGDATA *prpPAG,BOOL bpSendDdx)
{
	if (GetPAGByUrno(prpPAG->Urno) != NULL)
	{
		if (prpPAG->IsChanged == DATA_UNCHANGED)
		{
			prpPAG->IsChanged = DATA_CHANGED;
		}
		if(SavePAG(prpPAG) == false) return false; //Update Database
		UpdatePAGInternal(prpPAG);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaPAGData::UpdatePAGInternal(PAGDATA *prpPAG)
{
	PAGDATA *prlPAG = GetPAGByUrno(prpPAG->Urno);
	if (prlPAG != NULL)
	{
		omPagiMap.RemoveKey(prlPAG->Pagi);
		*prlPAG = *prpPAG; //Update omData
		omPagiMap.SetAt(prlPAG->Pagi,prlPAG);
		ogDdx.DataChanged((void *)this,PAG_CHANGE,(void *)prlPAG); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

PAGDATA *CedaPAGData::GetPAGByUrno(long lpUrno)
{
	PAGDATA  *prlPAG;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlPAG) == TRUE)
	{
		return prlPAG;
	}
	return NULL;
	
}

//--GET-BY-PAGI--------------------------------------------------------------------------------------------

PAGDATA *CedaPAGData::GetPAGByPagi(const char *prpPagi)
{
	PAGDATA  *prlPAG;
	if (omPagiMap.Lookup(prpPagi,(void *& )prlPAG) == TRUE)
	{
		return prlPAG;
	}
	return NULL;
	
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaPAGData::ReadSpecialData(CCSPtrArray<PAGDATA> *popAlt,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","PAG",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","PAG",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popAlt != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			PAGDATA *prpAlt = new PAGDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpAlt,CString(pclFieldList))) == true)
			{
				popAlt->Add(prpAlt);
			}
			else
			{
				delete prpAlt;
			}
		}
		if(popAlt->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaPAGData::SavePAG(PAGDATA *prpPAG)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[2048];

	if (prpPAG->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpPAG->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpPAG);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpPAG->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpPAG->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpPAG);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpPAG->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpPAG->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}

    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessPAGCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_PAG_CHANGE :
	case BC_PAG_DELETE :
		((CedaPAGData *)popInstance)->ProcessPAGBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaPAGData::ProcessPAGBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlPAGData = (struct BcStruct *) vpDataPointer;
	long llUrno = GetUrnoFromSelection(prlPAGData->Selection);

	PAGDATA *prlPAG = GetPAGByUrno(llUrno);
	if(ipDDXType == BC_PAG_CHANGE)
	{
		if(prlPAG != NULL)
		{
			GetRecordFromItemList(prlPAG,prlPAGData->Fields,prlPAGData->Data);
			if(ValidatePAGBcData(prlPAG->Urno)) //Prf: 8795
			{
				UpdatePAGInternal(prlPAG);
			}
			else
			{
				DeletePAGInternal(prlPAG);
			}
		}
		else
		{
			prlPAG = new PAGDATA;
			GetRecordFromItemList(prlPAG,prlPAGData->Fields,prlPAGData->Data);
			if(ValidatePAGBcData(prlPAG->Urno)) //Prf: 8795
			{
				InsertPAGInternal(prlPAG);
			}
			else
			{
				delete prlPAG;
			}
		}
	}
	if(ipDDXType == BC_PAG_DELETE)
	{
		if (prlPAG != NULL)
		{
			DeletePAGInternal(prlPAG);
		}
	}
}

//Prf: 8795
//--ValidatePAGBcData--------------------------------------------------------------------------------------

bool CedaPAGData::ValidatePAGBcData(const long& lrpUrno)
{
	bool blValidatePAGBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<PAGDATA> olPags;
		if(!ReadSpecialData(&olPags,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidatePAGBcData = false;
		}
	}
	return blValidatePAGBcData;
}

//---------------------------------------------------------------------------------------------------------
