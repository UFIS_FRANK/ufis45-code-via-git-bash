#ifndef AFX_TIMEPARAMETERSDLG_H__E75FC401_423C_11D2_8026_004095434A85__INCLUDED_
#define AFX_TIMEPARAMETERSDLG_H__E75FC401_423C_11D2_8026_004095434A85__INCLUDED_

// TimeParametersDlg.h : Header-Datei
//

#include <CCSEdit.h>
#include <CedaTipData.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld TimeParametersDlg 

class TimeParametersDlg : public CDialog
{
// Konstruktion
public:
	TimeParametersDlg(TIPDATA *popTip,CWnd* pParent = NULL);

// Dialogfelddaten
	//{{AFX_DATA(TimeParametersDlg)
	enum { IDD = IDD_TIMEPARAMETERSDLG };
	CButton	m_OK;
	CButton	m_TIPUH;
	CButton	m_TIPUM;
	CButton	m_TIPUS;
	CButton	m_TIPUD;
	CCSEdit	m_TIPC;
	CCSEdit	m_TIPV;
	CCSEdit	m_TIPT;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	CString m_Caption;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(TimeParametersDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(TimeParametersDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	TIPDATA *pomTip;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_TIMEPARAMETERSDLG_H__E75FC401_423C_11D2_8026_004095434A85__INCLUDED_
