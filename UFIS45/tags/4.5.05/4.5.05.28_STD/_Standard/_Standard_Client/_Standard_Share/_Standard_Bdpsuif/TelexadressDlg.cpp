// TelexadressDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <TelexadressDlg.h>
#include <CedaAPTData.h>
#include <CedaALTData.h>

#include <PrivList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// TelexadressDlg dialog


TelexadressDlg::TelexadressDlg(MVTDATA *popMVT,CWnd* pParent /*=NULL*/) : CDialog(TelexadressDlg::IDD, pParent)
{
	pomMVT = popMVT;

	//{{AFX_DATA_INIT(TelexadressDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void TelexadressDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(TelexadressDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_TEMA,	 m_TEMA);
	DDX_Control(pDX, IDC_SMVT,	 m_SMVT);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_VAFR_D, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T, m_VATOT);
	DDX_Control(pDX, IDC_ALC3,	 m_ALC3);
	DDX_Control(pDX, IDC_BEME,	 m_BEME);
	DDX_Control(pDX, IDC_TEAD,	 m_TEAD);
	DDX_Control(pDX, IDE_APC3,	 m_APC3);
	DDX_Control(pDX, IDE_PAYE,	 m_PAYE);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(TelexadressDlg, CDialog)
	//{{AFX_MSG_MAP(TelexadressDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// TelexadressDlg message handlers
//----------------------------------------------------------------------------------------

BOOL TelexadressDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING192) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);

	char clStat = ogPrivList.GetStat("TELEXADRESSDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomMVT->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("TELEXADRESSDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomMVT->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("TELEXADRESSDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomMVT->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("TELEXADRESSDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomMVT->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("TELEXADRESSDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomMVT->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("TELEXADRESSDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomMVT->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("TELEXADRESSDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_TEAD.SetTypeToString("X(7)",7,1);
	m_TEAD.SetBKColor(YELLOW);
	m_TEAD.SetTextErrColor(RED);
	m_TEAD.SetInitText(pomMVT->Tead);
	m_TEAD.SetSecState(ogPrivList.GetStat("TELEXADRESSDLG.m_TEAD"));
	//------------------------------------
	m_ALC3.SetFormat("x|#x|#x|#");
	m_ALC3.SetBKColor(YELLOW);  
	m_ALC3.SetTextErrColor(RED);
	m_ALC3.SetInitText(pomMVT->Alc3);
	m_ALC3.SetSecState(ogPrivList.GetStat("TELEXADRESSDLG.m_ALC3"));
	//------------------------------------
	m_APC3.SetFormat("x|#x|#x|#");
	m_APC3.SetTextLimit(-1,-1,true);
	m_APC3.SetTextErrColor(RED);
	m_APC3.SetInitText(pomMVT->Apc3);
	m_APC3.SetSecState(ogPrivList.GetStat("TELEXADRESSDLG.m_APC3"));
	//------------------------------------
	m_PAYE.SetFormat("x|#x|#x|#");
	m_PAYE.SetTextLimit(2,3,true);
	m_PAYE.SetTextErrColor(RED);
	m_PAYE.SetInitText(pomMVT->Paye);
	m_PAYE.SetSecState(ogPrivList.GetStat("TELEXADRESSDLG.m_PAYE"));
	//------------------------------------
	m_BEME.SetTypeToString("X(80)",80,0);
	m_BEME.SetTextErrColor(RED);
	m_BEME.SetInitText(pomMVT->Beme);
	m_BEME.SetSecState(ogPrivList.GetStat("TELEXADRESSDLG.m_BEME"));
	//------------------------------------
	if (pomMVT->Tema[0] == 'X') m_TEMA.SetCheck(1);
	clStat = ogPrivList.GetStat("TELEXADRESSDLG.m_TEMA");
	SetWndStatAll(clStat,m_TEMA);
	if (pomMVT->Smvt[0] == 'X') m_SMVT.SetCheck(1);
	clStat = ogPrivList.GetStat("TELEXADRESSDLG.m_SMVT");
	SetWndStatAll(clStat,m_SMVT);
	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(pomMVT->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("TELEXADRESSDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetBKColor(YELLOW);
	m_VAFRT.SetInitText(pomMVT->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("TELEXADRESSDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomMVT->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("TELEXADRESSDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomMVT->Vato.Format("%H:%M"));
	m_VATOT.SetSecState(ogPrivList.GetStat("TELEXADRESSDLG.m_VATO"));
	//------------------------------------
	return TRUE;  
}

//----------------------------------------------------------------------------------------

void TelexadressDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_TEAD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_TEAD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING396) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING396) + ogNotFormat;
		}
	}
	if(m_ALC3.GetStatus() == false)
	{
		ilStatus = false;
		if(m_ALC3.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING326) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING326) + ogNotFormat;
		}
	}
	if(m_APC3.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING23) + ogNotFormat;
	}
	if(m_PAYE.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING397) + ogNotFormat;
	}
	if(m_BEME.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING238) + ogNotFormat;
	}
	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////////////
	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olAlc3,olApc3,olTead;
		char clWhere[100];

		CCSPtrArray<MVTDATA> olMvtCPA;
		m_TEAD.GetWindowText(olTead);
		sprintf(clWhere,"WHERE TEAD='%s'",olTead);
		if(ogMVTData.ReadSpecialData(&olMvtCPA,clWhere,"URNO,TEAD",false) == true)
		{
			if(olMvtCPA[0].Urno != pomMVT->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING396) + LoadStg(IDS_EXIST);
			}
		}
		olMvtCPA.DeleteAll();
	
		if(m_ALC3.GetWindowTextLength() != 0)
		{
			m_ALC3.GetWindowText(olAlc3);
			sprintf(clWhere,"WHERE ALC3='%s'",olAlc3);
			if(ogALTData.ReadSpecialData(NULL,clWhere,"ALC3",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING327);
			}
		}

		if(m_APC3.GetWindowTextLength() != 0)
		{
			m_APC3.GetWindowText(olApc3);
			sprintf(clWhere,"WHERE APC3='%s'",olApc3);
			if(ogAPTData.ReadSpecialData(NULL,clWhere,"APC3",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING33);
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_TEAD.GetWindowText(pomMVT->Tead,8);
		m_ALC3.GetWindowText(pomMVT->Alc3,4);
		m_APC3.GetWindowText(pomMVT->Apc3,4);
		CString olTemp;
		m_BEME.GetWindowText(olTemp);
		for(int i=0;i>-1;)
		{
			i = olTemp.Find("\r\n");
			if(i != -1)
			{
				if(olTemp.GetLength() > (i+2))
					olTemp = olTemp.Left(i) + " " + olTemp.Mid(i+2);
				else
					olTemp = olTemp.Left(i);
			}
		}
		strcpy(pomMVT->Beme,olTemp);
				
		m_PAYE.GetWindowText(pomMVT->Paye,4);
		pomMVT->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomMVT->Vato = DateHourStringToDate(olVatod,olVatot);
		(m_TEMA.GetCheck() == 1) ? pomMVT->Tema[0] = 'X' : pomMVT->Tema[0] = ' ';
		(m_SMVT.GetCheck() == 1) ? pomMVT->Smvt[0] = 'X' : pomMVT->Smvt[0] = ' ';
	
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_TEAD.SetFocus();
		m_TEAD.SetSel(0,-1);
	}
}

//----------------------------------------------------------------------------------------
