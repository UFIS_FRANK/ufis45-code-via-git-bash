// ExtLMGatPosGateDlg.cpp : implementation file
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <ExtLMGatPosGateDlg.h>
#include <privlist.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ExtLMGatPosGateDlg dialog


ExtLMGatPosGateDlg::ExtLMGatPosGateDlg(GATDATA *popGAT,long lpOriginalUrno,CWnd* pParent /*=NULL*/)
	: GatPosGateDlg(popGAT,ExtLMGatPosGateDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(ExtLMGatPosGateDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	this->lmOriginalUrno = lpOriginalUrno;
}


void ExtLMGatPosGateDlg::DoDataExchange(CDataExchange* pDX)
{
	GatPosGateDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ExtLMGatPosGateDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ExtLMGatPosGateDlg, GatPosGateDlg)
	//{{AFX_MSG_MAP(ExtLMGatPosGateDlg)
		// NOTE: the ClassWizard will add message map macros here
	ON_BN_CLICKED(IDC_DAT_NEW, OnDatNew)
	ON_BN_CLICKED(IDC_DAT_DEL, OnDatDelete)
	ON_BN_CLICKED(IDC_DAT_COPY,OnDatCopy)
	ON_BN_CLICKED(IDC_DAT_ASSIGN, OnDatAssign)

	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ExtLMGatPosGateDlg message handlers
void ExtLMGatPosGateDlg::OnOK() 
{
	// TODO: Add extra validation here
	if (!UpdateData(TRUE))
		return;

	bool ilStatus = true;
	CString olErrorText;

	if (!this->omGrid.FinalCheck(this->omDatPtrA,this->omDeleteDatPtrA,olErrorText))
	{
		ilStatus = false;
	}

	if (ilStatus == true)
	{
		GatPosGateDlg::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
	}
}

BOOL ExtLMGatPosGateDlg::OnInitDialog() 
{
	GatPosGateDlg::OnInitDialog();
	
	// TODO: Add extra initialization here

	this->omGrid.SubClassDlgItem(IDC_DAT,this);


	CWnd *polWnd = NULL;
	switch(ogPrivList.GetStat("GATEDLG.m_DEFD"))
	{
	case '0' :	// Readonly
		polWnd = GetDlgItem(IDC_DAT_NEW);
		if (polWnd)
			polWnd->EnableWindow(FALSE);
		polWnd = GetDlgItem(IDC_DAT_DEL);
		if (polWnd)
			polWnd->EnableWindow(FALSE);

		if (this->lmOriginalUrno == 0)	// is it the original or a copy
			this->omGrid.Initialize("GAT",this->pomGAT->Urno,false,this->omDatPtrA,true);
		else
			this->omGrid.Initialize("GAT",lmOriginalUrno,true,this->omDatPtrA,true);
	break;
	case '-' :	// Hide
		polWnd = GetDlgItem(IDC_DAT_NEW);
		if (polWnd)
		{
			polWnd->EnableWindow(FALSE);
			polWnd->ShowWindow(SW_HIDE);
		}
		polWnd = GetDlgItem(IDC_DAT_DEL);
		if (polWnd)
		{
			polWnd->EnableWindow(FALSE);
			polWnd->ShowWindow(SW_HIDE);
		}
		if (this->lmOriginalUrno == 0)	// is it the original or a copy
			this->omGrid.Initialize("GAT",this->pomGAT->Urno,false,this->omDatPtrA,true);
		else
			this->omGrid.Initialize("GAT",lmOriginalUrno,true,this->omDatPtrA,true);
		this->omGrid.ShowWindow(SW_HIDE);
	break;
	default:
		if (this->lmOriginalUrno == 0)	// is it the original or a copy
			this->omGrid.Initialize("GAT",this->pomGAT->Urno,false,this->omDatPtrA,false);
		else
			this->omGrid.Initialize("GAT",lmOriginalUrno,true,this->omDatPtrA,false);

	}



	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//----------------------------------------------------------------------------------------

void ExtLMGatPosGateDlg::OnDatNew() 
{
	this->omGrid.Insert(this->omDatPtrA);
}

//----------------------------------------------------------------------------------------

void ExtLMGatPosGateDlg::OnDatDelete() 
{
	this->omGrid.Delete(this->omDatPtrA,this->omDeleteDatPtrA);
}

//----------------------------------------------------------------------------------------

void ExtLMGatPosGateDlg::OnDatCopy() 
{
	this->omGrid.Copy(this->omDatPtrA);
}

void ExtLMGatPosGateDlg::OnDatAssign() 
{
	// TODO: Add your control notification handler code here
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<GATDATA> olList;
	CString olOrderBy = "ORDER BY GNAM";
	if (ogGATData.ReadSpecialData(&olList, olOrderBy.GetBuffer(0), "URNO,GNAM", false) == true)
	{
		for(int i = 0; i < olList.GetSize(); i++)
		{
			if (this->pomGAT->Urno != olList[i].Urno)
			{
				char pclUrno[20]="";
				sprintf(pclUrno, "%ld", olList[i].Urno);
				olCol3.Add(CString(pclUrno));
				olCol1.Add(CString(olList[i].Gnam));
			}
		}

		this->omGrid.Assign(this->omDatPtrA,this->omDeleteDatPtrA,olCol1,olCol3,LoadStg(IDS_STRING178));
	}			
}
