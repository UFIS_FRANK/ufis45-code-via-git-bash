// PmxTableViewer.cpp 
//

#include <stdafx.h>
#include <PmxTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>
#include <resrc1.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void PmxTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// PmxTableViewer
//

PmxTableViewer::PmxTableViewer(CCSPtrArray<PMXDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomPmxTable = NULL;
    ogDdx.Register(this, PMX_CHANGE, CString("PMXTABLEVIEWER"), CString("Pmx Update"), PmxTableCf);
    ogDdx.Register(this, PMX_NEW,    CString("PMXTABLEVIEWER"), CString("Pmx New"),    PmxTableCf);
    ogDdx.Register(this, PMX_DELETE, CString("PMXTABLEVIEWER"), CString("Pmx Delete"), PmxTableCf);
}

//-----------------------------------------------------------------------------------------------

PmxTableViewer::~PmxTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void PmxTableViewer::Attach(CCSTable *popTable)
{
    pomPmxTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void PmxTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void PmxTableViewer::MakeLines()
{
	int ilPmxCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilPmxCount; ilLc++)
	{
		PMXDATA *prlPmxData = &pomData->GetAt(ilLc);
		MakeLine(prlPmxData);
	}
}

//-----------------------------------------------------------------------------------------------

void PmxTableViewer::MakeLine(PMXDATA *prpPmx)
{

    //if( !IsPassFilter(prpPmx)) return;

    // Update viewer data for this shift record
    PMXTABLE_LINEDATA rlPmx;

	rlPmx.Urno = prpPmx->Urno;
	rlPmx.Tifr = prpPmx->Tifr;
	rlPmx.Pomo = prpPmx->Pomo;
	rlPmx.Potu = prpPmx->Potu;
	rlPmx.Powe = prpPmx->Powe;
	rlPmx.Poth = prpPmx->Poth;
	rlPmx.Pofr = prpPmx->Pofr;
	rlPmx.Posa = prpPmx->Posa;
	rlPmx.Posu = prpPmx->Posu;
	//uhi 7.5.01
	if(prpPmx->Vafr.GetStatus() == COleDateTime::valid)
		rlPmx.Vafr = prpPmx->Vafr.Format("%d.%m.%Y");
	if(prpPmx->Vato.GetStatus() == COleDateTime::valid)
		rlPmx.Vato = prpPmx->Vato.Format("%d.%m.%Y");
	CreateLine(&rlPmx);
}

//-----------------------------------------------------------------------------------------------

void PmxTableViewer::CreateLine(PMXTABLE_LINEDATA *prpPmx)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (ComparePmx(prpPmx, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	PMXTABLE_LINEDATA rlPmx;
	rlPmx = *prpPmx;
    omLines.NewAt(ilLineno, rlPmx);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void PmxTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 9;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomPmxTable->SetShowSelection(TRUE);
	pomPmxTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	int ilPos = 1;
	//uhi 26.3.01
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING761),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 30; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING761),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 30; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING761),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 30; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING761),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 30; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING761),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 30; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING761),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 30; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING761),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	rlHeader.Length = 30; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING761),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	//uhi 7.5.01
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING761),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING761),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	
	pomPmxTable->SetHeaderFields(omHeaderDataArray);
	pomPmxTable->SetDefaultSeparator();
	pomPmxTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_RIGHT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		//uhi 26.3.01
		rlColumnData.Text = omLines[ilLineNo].Tifr.Left(2) + ":" + omLines[ilLineNo].Tifr.Right(2);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = omLines[ilLineNo].Pomo;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Potu;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Powe;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Poth;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Pofr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Posa;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Posu;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		//uhi 7.5.01
		rlColumnData.Text = omLines[ilLineNo].Vafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomPmxTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomPmxTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString PmxTableViewer::Format(PMXTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool PmxTableViewer::FindPmx(char *pcpPmxKeya, char *pcpPmxKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpPmxKeya) &&
			 (omLines[ilItem].Keyd == pcpPmxKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void PmxTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    PmxTableViewer *polViewer = (PmxTableViewer *)popInstance;
    if (ipDDXType == PMX_CHANGE || ipDDXType == PMX_NEW) polViewer->ProcessPmxChange((PMXDATA *)vpDataPointer);
    if (ipDDXType == PMX_DELETE) polViewer->ProcessPmxDelete((PMXDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void PmxTableViewer::ProcessPmxChange(PMXDATA *prpPmx)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_RIGHT;

	//uhi 26.3.01
	rlColumn.Text.Format("%.2s:%.2s",prpPmx->Tifr,prpPmx->Tifr + 2);
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpPmx->Pomo;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpPmx->Potu;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpPmx->Powe;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpPmx->Poth;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpPmx->Pofr;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpPmx->Posa;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpPmx->Posu;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	//uhi 7.5.01
	if(prpPmx->Vafr.GetStatus() == COleDateTime::valid)
		rlColumn.Text = prpPmx->Vafr.Format("%d.%m.%Y");
	else
		rlColumn.Text = "";

	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	
	if(prpPmx->Vato.GetStatus() == COleDateTime::valid)
		rlColumn.Text = prpPmx->Vato.Format("%d.%m.%Y");
	else
		rlColumn.Text = "";

	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (FindLine(prpPmx->Urno, ilItem))
	{
        PMXTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Tifr = prpPmx->Tifr;
		prlLine->Pomo = prpPmx->Pomo;
		prlLine->Potu = prpPmx->Potu;
		prlLine->Powe = prpPmx->Powe;
		prlLine->Poth = prpPmx->Poth;
		prlLine->Pofr = prpPmx->Pofr;
		prlLine->Posa = prpPmx->Posa;
		prlLine->Posu = prpPmx->Posu;
		
		prlLine->Urno = prpPmx->Urno;

		//uhi 7.5.01
		if(prpPmx->Vafr.GetStatus() == COleDateTime::valid)
			prlLine->Vafr = prpPmx->Vafr.Format("%d.%m.%Y");
		if(prpPmx->Vato.GetStatus() == COleDateTime::valid)
			prlLine->Vato = prpPmx->Vato.Format("%d.%m.%Y");

		pomPmxTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomPmxTable->DisplayTable();
	}
	else
	{
		MakeLine(prpPmx);
		if (FindLine(prpPmx->Urno, ilItem))
		{
	        PMXTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomPmxTable->AddTextLine(olLine, (void *)prlLine);
				pomPmxTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void PmxTableViewer::ProcessPmxDelete(PMXDATA *prpPmx)
{
	int ilItem;
	if (FindLine(prpPmx->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomPmxTable->DeleteTextLine(ilItem);
		pomPmxTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool PmxTableViewer::IsPassFilter(PMXDATA *prpPmx)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int PmxTableViewer::ComparePmx(PMXTABLE_LINEDATA *prpPmx1, PMXTABLE_LINEDATA *prpPmx2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpPmx1->Tifd == prpPmx2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpPmx1->Tifd > prpPmx2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void PmxTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool PmxTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void PmxTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void PmxTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING758);
	int ilOrientation = PRINT_PORTRAET;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool PmxTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = 8; //omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool PmxTableViewer::PrintTableLine(PMXTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = 8; //omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_RIGHT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			//uhi 26.3.01
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Tifr.Left(2) + ":" + prpLine->Tifr.Right(2);
				break;
			case 1:
				rlElement.Text		= prpLine->Pomo;
				break;
			case 2:
				rlElement.Text		= prpLine->Potu;
				break;
			case 3:
				rlElement.Text		= prpLine->Powe;
				break;
			case 4:
				rlElement.Text		= prpLine->Poth;
				break;
			case 5:
				rlElement.Text		= prpLine->Pofr;
				break;
			case 6:
				rlElement.Text		= prpLine->Posa;
				break;
			case 7:
				//rlElement.FrameRight  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Posu;
				break;
			//uhi 7.5.01
			case 8:
				rlElement.Text		= prpLine->Vafr;
				break;
			case 9:
				rlElement.FrameRight  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Vato;
				break;			
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
