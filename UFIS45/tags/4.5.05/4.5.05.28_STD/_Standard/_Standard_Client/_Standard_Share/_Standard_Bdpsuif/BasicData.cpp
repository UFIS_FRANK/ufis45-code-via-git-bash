// basicdat.cpp CBasicData class for providing general used methods

#include <stdafx.h>
#include <BasicData.h>
#include <CedaBasicData.h>
#include <CedaParData.h>
#include <CedaAloData.h>
#include <CedaSgrData.h>
#include <CedaSgmData.h>
#include <CedaGatData.h>
#include <CedaBltData.h>
#include <CedaWroData.h>
#include <CedaExtData.h>
#include <CedaValData.h>
#include <CedaAptData.h>
#include <PrivList.h>
#include <Bdpsuif.h>
#include <resource.h>

#define N_URNOS_AT_ONCE 50
 

static int CompareArray( const CString **e1, const CString **e2);

static int CompareBySortFlag(const SGMDATA **e1,const SGMDATA **e2);

//------------------------------------------------------------------------------------

bool IsNumeric(CString opString)
{
	if(opString.GetLength() == 0)
		return false;
	

	for(int i = 0; i < opString.GetLength() ; i++)
	{
		if( !( (opString[i] >= 48 && opString[i] <= 57) || opString[i] == 46))
			return false;


	}

	return true;
}

CString LoadStg(UINT nID)
{
	CString olString = "";
	olString.LoadString(nID);
	int i = olString.Find("*REM*");
	if(i>-1)
		olString = olString.Left(i);
	return olString;
}

//------------------------------------------------------------------------------------

CString GetMinutesFromHHMM(CString opHHMM)
{
	CString olMinutes = "";
	if((opHHMM.Find(":") == -1 && opHHMM.GetLength() == 4) || (opHHMM.Find(":") != -1 && opHHMM.GetLength() == 5))
	{
		olMinutes.Format("%d", (atoi(opHHMM.Left(2))*60 + atoi(opHHMM.Right(2))));
	}
	if(olMinutes == "0")
		olMinutes = "";
	return olMinutes;
}

//------------------------------------------------------------------------------------

CString GetHHMMFromMinutes(CString opMinutes, bool bpTrenner /*= false*/)
{
	CString olHHMM = "";
	if(opMinutes.GetLength() > 0)
	{
		CTime olTime(1981,3,3,0,0,0);
		int ilMinutes = atoi(opMinutes);
		if(ilMinutes > 0)
		{
			olTime += CTimeSpan(0,0,ilMinutes,0);
			if(bpTrenner)
				olHHMM = olTime.Format("%H:%M");
			else
				olHHMM = olTime.Format("%H%M");
		}
	}
	return olHHMM;
}

//------------------------------------------------------------------------------------

int GetItemCount(CString olList, char cpTrenner  )
{
	CStringArray olStrArray;
	return ExtractItemList(olList,&olStrArray,cpTrenner);

}

CString GetListItem(CString &opList, int ipPos, bool bpCut, char cpTrenner )
{
	CStringArray olStrArray;
	int ilAnz = ExtractItemList(opList, &olStrArray, cpTrenner);
	CString olReturn;

	if(ipPos == -1)
		ipPos = ilAnz;

	if((ipPos <= ilAnz) && (ipPos > 0))
		olReturn = olStrArray[ipPos - 1];
	if(bpCut)
	{
		opList = "";
		for(int ilLc = 0; ilLc < ilAnz; ilLc++)
		{
			if(ilLc != (ipPos - 1))
			{
				opList = opList + olStrArray[ilLc] + cpTrenner;
			}
		}
	}
	if(bpCut)
		opList = opList.Left(opList.GetLength() - 1);
	return olReturn;
}



CString DeleteListItem(CString &opList, CString olItem)
{
	CStringArray olStrArray;
	int ilAnz = ExtractItemList(opList, &olStrArray);
	opList = "";
	for(int ilLc = 0; ilLc < ilAnz; ilLc++)
	{
			if(olStrArray[ilLc] != olItem)
				opList = opList + olStrArray[ilLc] + ",";
	}
	opList = opList.Left(opList.GetLength() - 1);
	return opList;
}

CString SortItemList(CString opSubString, char cpTrenner)
{
	CString *polText;
	CCSPtrArray<CString> olArray;

	BOOL blEnd = FALSE;
	
	if(!opSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		while(blEnd == FALSE)
		{
			polText = new CString;
			pos = opSubString.Find(cpTrenner);
			if(pos == -1)
			{
				blEnd = TRUE;
				*polText = opSubString;
			}
			else
			{
				*polText = opSubString.Mid(0, opSubString.Find(cpTrenner));
				opSubString = opSubString.Mid(opSubString.Find(cpTrenner)+1, opSubString.GetLength( )-opSubString.Find(cpTrenner)+1);
			}
			olArray.Add(polText);
		}
	}

	CString olSortedString;
	olArray.Sort(CompareArray);
	for(int i=0; i<olArray.GetSize(); i++)
	{
		olSortedString += olArray[i] + cpTrenner;
	}
	
	olArray.DeleteAll();
	return olSortedString.Left(olSortedString.GetLength()-1);
}

CBasicData::CBasicData(void)
{
	char pclTmpText[512];
	char pclConfigPath[512];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	imNextOrder = 4711;
	*pcmCedaCmd = '\0';


	omDiaStartTime = CTime::GetCurrentTime();
	omDiaStartTime -= CTimeSpan(0, 1, 0, 0);
	omDiaEndTime = omDiaStartTime + CTimeSpan(0, 6, 0, 0);
	

	// now reading the ceda commands
	char pclComandBuf[24];
	omDefaultComands.Empty();
	omActualComands.Empty();
 
    GetPrivateProfileString(pcgAppName, "EIOHDL", "LLF",pclTmpText, sizeof pclTmpText, pclConfigPath);
	sprintf(pclComandBuf," %3s ","LLF");
	omDefaultComands += pclComandBuf;
	sprintf(pclComandBuf," %3s ",pclTmpText);
	omActualComands += pclComandBuf;

	bmDisplayDamagedDataWarningMessage = false;
    GetPrivateProfileString(pcgAppName, "DISPLAYWARNINGMESSAGE","NO", pclTmpText,sizeof pclTmpText, pclConfigPath);
	if (stricmp(pclTmpText,"YES") == 0)
	{
		bmDisplayDamagedDataWarningMessage = true;
	}

	bmUseBackDoor = false;
    GetPrivateProfileString(pcgAppName, "BACKDOORAAT","NO", pclTmpText,sizeof pclTmpText, pclConfigPath);
	if (stricmp(pclTmpText,"YES") == 0)
	{
		bmUseBackDoor = true;
	}

	bmUseLocalTimeAsDefault = true;
    GetPrivateProfileString(pcgAppName, "BLOCKING_TIMES_UTC","YES", pclTmpText,sizeof pclTmpText, pclConfigPath);
	if (stricmp(pclTmpText,"YES") == 0)
	{
		bmUseLocalTimeAsDefault = false;
	}


	omHtytabAdid = false;
    GetPrivateProfileString("FIPS", "HANDLING_AGENT_ASSIGNMENT","FALSE", pclTmpText,sizeof pclTmpText, pclConfigPath);
	if (stricmp(pclTmpText,"TRUE") == 0)
	{
		omHtytabAdid  = true;
	}



	omTich		 = CTime::GetCurrentTime();
	omLocalDiff1 = CTimeSpan(0,0,0,0);
	omLocalDiff2 = CTimeSpan(0,0,0,0);

}



CBasicData::~CBasicData(void)
{
}


long CBasicData::GetNextUrno(void)
{
	bool	olRc = true;
	long				llNextUrno = 0L;
	

	if (omUrnos.GetSize() == 0)
	{
		olRc = GetNurnos(N_URNOS_AT_ONCE);
	}

	if (omUrnos.GetSize() > 0)
	{
		llNextUrno = omUrnos[0];
		omUrnos.RemoveAt(0);

		if ( (llNextUrno != 0L) && (olRc == true) )
		{
			return(llNextUrno);
		}
	}
	::MessageBox(NULL,LoadStg(IDS_STRING12),LoadStg(IDS_STRING145),MB_ICONERROR);
	return -1;	
}


bool CBasicData::GetNurnos(int ipNrOfUrnos)
{
	bool	ilRc = false;
	char 	pclTmpDataBuf[12*N_URNOS_AT_ONCE];

	sprintf(pclTmpDataBuf, "%d", ipNrOfUrnos);

	ilRc = CedaAction("GMU", "", "", pclTmpDataBuf);

	if (ilRc == true)
	{
		for ( int ilItemNo=1; (ilItemNo <= N_URNOS_AT_ONCE) ; ilItemNo++ )
		{
			char pclTmpBuf[64];

			GetItem(ilItemNo, pclTmpDataBuf, pclTmpBuf);

			long llNewUrno = atol(pclTmpBuf);

			omUrnos.Add(llNewUrno);
		}
	}

	return ilRc;
}


int CBasicData::GetNextOrderNo()
{
	imNextOrder++;

	return (imNextOrder);
}


char *CBasicData::GetCedaCommand(CString opCmdType)
{ 

	int ilIndex;

	if ((ilIndex = omDefaultComands.Find(opCmdType)) != -1)
	{
		strcpy(pcmCedaComand,omActualComands.Mid(ilIndex,3));
	}
	else
	{
		strcpy(pcmCedaComand,opCmdType);
	}
	return pcmCedaComand;
}


void CBasicData::GetDiagramStartTime(CTime &opStart, CTime &opEnd)
{
	opStart = omDiaStartTime;
	opEnd = omDiaEndTime;
	return;
}


void CBasicData::SetDiagramStartTime(CTime opDiagramStartTime, CTime opDiagramEndTime)
{
	omDiaStartTime = opDiagramStartTime;
	omDiaEndTime = opDiagramEndTime;
}


void CBasicData::SetWorkstationName(CString opWsName)
{
	omWorkstationName = CString("WKS234");//opWsName;
}


CString CBasicData::GetWorkstationName()
{
	return omWorkstationName;
}


bool CBasicData::GetWindowPosition(CRect& rlPos,CString olMonitor)
{

	int XResolution = 1024;
	int YResolution = 768;
	if (ogCfgData.rmUserSetup.RESO[0] == '8')
	{
		XResolution = 800;
		YResolution = 600;
	}
	else
	{
		if (ogCfgData.rmUserSetup.RESO[0] == '1')
		{
			if (ogCfgData.rmUserSetup.RESO[1] == '0')
			{
				XResolution = 1024;
				YResolution = 768;
			}
		}
		else
		{
			XResolution = 1280;
			YResolution = 1024;
		}
	}
  

	int ilMonitor;
	if (olMonitor[0] == 'L')
		ilMonitor = 0;
	if (olMonitor[0] == 'M')
		ilMonitor = 1;
	if (olMonitor[0] == 'R')
		ilMonitor = 2;
	
	rlPos.top = ilMonitor == 0 ? 56 : 0;
	rlPos.bottom = YResolution;
	rlPos.left = XResolution * ilMonitor;
	rlPos.right = XResolution * (ilMonitor+1);

	return true;
}

static int CompareArray( const CString **e1, const CString **e2)
{
	return (strcmp((**e1),(**e2)));
}

void CBasicData::SetWindowStat(const char *pcpKey, CWnd *popWnd)
{
	if(popWnd != NULL)
	{
		char clStat;
		clStat = ogPrivList.GetStat(pcpKey);
		if(clStat == '1') // displayed and enabled
		{
			popWnd->ShowWindow(SW_SHOW);
			popWnd->EnableWindow(TRUE);
		}
		else if(clStat == '-') // hidden
		{
			popWnd->ShowWindow(SW_HIDE);
		}
		else // displayed and disabled
		{
			popWnd->ShowWindow(SW_SHOW);
			popWnd->EnableWindow(FALSE);
		}
	}
}

bool CBasicData::IsGatPosEnabled()
{
	static int ilIsGatPosEnabled = -1;
	if (ilIsGatPosEnabled == -1)
	{
		ogParData.Read();
		ogValData.Read();
		CString olValue = ogParData.GetParValue("GLOBAL","ID_GAT_POS");
		if (olValue.CompareNoCase("Y") == 0)
			ilIsGatPosEnabled = 1;		
		else
			ilIsGatPosEnabled = 0;		
	}

	return ilIsGatPosEnabled != 0;
}

bool CBasicData::UseUniqueActPair()
{
	static int ilUseUniqueActPair = -1;
	if (ilUseUniqueActPair == -1)
	{
		ogParData.Read();
		ogValData.Read();
		CString olValue = ogParData.GetParValue("GLOBAL","ID_UNIQUEACTPAIR");
		if (olValue.CompareNoCase("Y") == 0)
			ilUseUniqueActPair = 1;		
		else
			ilUseUniqueActPair = 0;		
	}

	return ilUseUniqueActPair != 0;
}

bool CBasicData::IsExtendedLMEnabled()
{
	static int ilIsExtLMEnabled = -1;
	if (ilIsExtLMEnabled == -1)
	{
		ogParData.Read();
		ogValData.Read();
		CString olValue = ogParData.GetParValue("GLOBAL","ID_EXT_LM");
		if (olValue.CompareNoCase("Y") == 0)
			ilIsExtLMEnabled = 1;		
		else
			ilIsExtLMEnabled = 0;		
	}

	return ilIsExtLMEnabled != 0;
}

CString	CBasicData::GetCustomerId()
{
	static CString olCustomer;
	if (olCustomer.GetLength() == 0)
	{
		ogParData.Read();
		ogValData.Read();
		olCustomer = ogParData.GetParValue("ROSTER","ID_PROD_CUSTOMER");
	}

	return olCustomer;
}

static int CompareBySortFlag(const SGMDATA **e1,const SGMDATA **e2)
{
	if ((**e1).Sort < (**e2).Sort)
		return -1;
	else if ((**e1).Sort > (**e2).Sort)
		return 1;
	else
		return 0;

}

bool CBasicData::GetCmdLineResource(CString& ropTable,long& rlpUrno)
{
	if (ogCmdLineStghArray.GetSize() < 5)
		return false;
	char pclTable[128];
	if (sscanf(ogCmdLineStghArray.GetAt(4),"Resource:%3s:%ld",&pclTable[0],&rlpUrno) != 2)
		return false;
	else
	{
		ropTable = pclTable;
		return true;
	}
}

int CBasicData::GetGatesForPosition(long lpPosUrno,CStringArray& ropGates)
{
	ALODATA *polAloc = ogAloData.GetAloByName("GATPOS");
	if (polAloc == NULL)
		return 0;
	SGRDATA *polSgr = ogSgrData.GetSgrByUgtyAndStyp(polAloc->Urno,lpPosUrno);
	if (polSgr == NULL)
		return 0;

	CCSPtrArray<SGMDATA> olSgmList;
	ogSgmData.GetSgmDataByGrnUrno(olSgmList,polSgr->Urno);

	olSgmList.Sort(CompareBySortFlag);

	for (int i = 0; i < olSgmList.GetSize(); i++)
	{
		GATDATA *polGat = ogGATData.GetGATByUrno(olSgmList[i].Uval);
		if (polGat)
			ropGates.Add(polGat->Gnam);
	}
	
	return ropGates.GetSize();
}


bool CBasicData::SetGatesForPosition(long lpPosUrno,const CStringArray& ropGates)
{
	ALODATA *polAloc = ogAloData.GetAloByName("GATPOS");
	if (polAloc == NULL)
		return false;
	SGRDATA *polSgr = ogSgrData.GetSgrByUgtyAndStyp(polAloc->Urno,lpPosUrno);
	if (polSgr == NULL)
	{
		polSgr = new SGRDATA;
		strcpy(polSgr->Appl,"GATPOS");
		polSgr->Cdat = CTime::GetCurrentTime();
//		polSgr->Fldn =
		strcpy(polSgr->Hopo,pcgHome);
		strcpy(polSgr->Grds,polAloc->Alod);
		strcpy(polSgr->Grpn,polAloc->Reft);
		strcpy(polSgr->Prfl,"0");
		polSgr->Styp = lpPosUrno;
		strncpy(polSgr->Tabn,polAloc->Reft,3);
		polSgr->Tabn[3] = '\0';
		polSgr->Ugty = polAloc->Urno;
		polSgr->Urno = ogBasicData.GetNextUrno();
		strcpy(polSgr->Usec,cgUserName);
//		polSgr->Useu =
		ogSgrData.Insert(polSgr,true);

	}

	CCSPtrArray<SGMDATA> olSgmList;
	ogSgmData.GetSgmDataByGrnUrno(olSgmList,polSgr->Urno);

	for (int i = 0; i < ropGates.GetSize(); i++)
	{
		GATDATA *polGat = ogGATData.GetGATByName(ropGates[i]);
		if (polGat)
		{
			bool blFound = false;
			for (int j = 0; j < olSgmList.GetSize(); j++)
			{
				if (olSgmList[j].Uval == polGat->Urno)
				{
					blFound = true;
					if (olSgmList[j].Sort != i)
					{
						olSgmList[j].Sort = i;
						ogSgmData.Update(&olSgmList[j],true);
					}

					break;
				}
			}

			if (!blFound)
			{
				SGMDATA *polSgm = new SGMDATA;
				polSgm->IsChanged = DATA_NEW;
				polSgm->Cdat = CTime::GetCurrentTime();
				strcpy(polSgm->Hopo,pcgHome);
//				polSgm->Prfl =
				strncpy(polSgm->Tabn,polAloc->Reft,3);
				polSgm->Tabn[3] = '\0';
				polSgm->Ugty = polAloc->Urno;
				polSgm->Urno = ogBasicData.GetNextUrno();
				strcpy(polSgm->Usec,cgUserName);
//				polSgm->Useu =
				polSgm->Usgr = polSgr->Urno;
				polSgm->Uval = polGat->Urno;
				polSgm->Valu = lpPosUrno;
				polSgm->Sort = i;

				ogSgmData.Insert(polSgm,true);									
			}
			else
			{
				olSgmList.RemoveAt(j);
			}
		}
	}
	
	for (i = 0; i < olSgmList.GetSize(); i++)
	{
		GATDATA *polGat = ogGATData.GetGATByUrno(olSgmList[i].Uval);
		if (polGat)
		{
			bool blFound = false;
			for (int j = 0; j < ropGates.GetSize(); j++)
			{
				if (polGat->Gnam == ropGates[j])
				{
					blFound = true;
					break;
				}
			}

			if (!blFound)
			{
				ogSgmData.Delete(&olSgmList[i],true);
			}
		}
	}
	return true;
}

int CBasicData::GetBaggageBeltsForGate(long lpGatUrno,CStringArray& ropBaggageBelts)
{
	ALODATA *polAloc = ogAloData.GetAloByName("BAGGAT");
	if (polAloc == NULL)
		return 0;
	SGRDATA *polSgr = ogSgrData.GetSgrByUgtyAndStyp(polAloc->Urno,lpGatUrno);
	if (polSgr == NULL)
		return 0;

	CCSPtrArray<SGMDATA> olSgmList;
	ogSgmData.GetSgmDataByGrnUrno(olSgmList,polSgr->Urno);

	olSgmList.Sort(CompareBySortFlag);

	for (int i = 0; i < olSgmList.GetSize(); i++)
	{
		BLTDATA *polBlt = ogBLTData.GetBLTByUrno(olSgmList[i].Uval);
		if (polBlt)
			ropBaggageBelts.Add(polBlt->Bnam);
	}
	
	return ropBaggageBelts.GetSize();
}


bool CBasicData::SetBaggageBeltsForGate(long lpGatUrno,const CStringArray& ropBaggageBelts)
{
	ALODATA *polAloc = ogAloData.GetAloByName("BAGGAT");
	if (polAloc == NULL)
		return false;
	SGRDATA *polSgr = ogSgrData.GetSgrByUgtyAndStyp(polAloc->Urno,lpGatUrno);
	if (polSgr == NULL)
	{
		polSgr = new SGRDATA;
		strcpy(polSgr->Appl,"BAGGAT");
		polSgr->Cdat = CTime::GetCurrentTime();
//		polSgr->Fldn =
		strcpy(polSgr->Hopo,pcgHome);
		strcpy(polSgr->Grds,polAloc->Alod);
		strcpy(polSgr->Grpn,polAloc->Reft);
		strcpy(polSgr->Prfl,"0");
		polSgr->Styp = lpGatUrno;
		strncpy(polSgr->Tabn,polAloc->Reft,3);
		polSgr->Tabn[3] = '\0';
		polSgr->Ugty = polAloc->Urno;
		polSgr->Urno = ogBasicData.GetNextUrno();
		strcpy(polSgr->Usec,cgUserName);
//		polSgr->Useu =
		ogSgrData.Insert(polSgr,true);

	}

	CCSPtrArray<SGMDATA> olSgmList;
	ogSgmData.GetSgmDataByGrnUrno(olSgmList,polSgr->Urno);

	for (int i = 0; i < ropBaggageBelts.GetSize(); i++)
	{
		BLTDATA *polBlt = ogBLTData.GetBLTByName(ropBaggageBelts[i]);
		if (polBlt)
		{
			bool blFound = false;
			for (int j = 0; j < olSgmList.GetSize(); j++)
			{
				if (olSgmList[j].Uval == polBlt->Urno)
				{
					blFound = true;
					if (olSgmList[j].Sort != i)
					{
						olSgmList[j].Sort = i;
						ogSgmData.Update(&olSgmList[j],true);
					}
					break;
				}
			}

			if (!blFound)
			{
				SGMDATA *polSgm = new SGMDATA;
				polSgm->IsChanged = DATA_NEW;
				polSgm->Cdat = CTime::GetCurrentTime();
				strcpy(polSgm->Hopo,pcgHome);
//				polSgm->Prfl =
				strncpy(polSgm->Tabn,polAloc->Reft,3);
				polSgm->Tabn[3] = '\0';
				polSgm->Ugty = polAloc->Urno;
				polSgm->Urno = ogBasicData.GetNextUrno();
				strcpy(polSgm->Usec,cgUserName);
//				polSgm->Useu =
				polSgm->Usgr = polSgr->Urno;
				polSgm->Uval = polBlt->Urno;
				polSgm->Valu = lpGatUrno;
				polSgm->Sort = i;
				ogSgmData.Insert(polSgm,true);									
			}
			else
			{
				olSgmList.RemoveAt(j);
			}
		}
	}
	
	for (i = 0; i < olSgmList.GetSize(); i++)
	{
		BLTDATA *polBlt = ogBLTData.GetBLTByUrno(olSgmList[i].Uval);
		if (polBlt)
		{
			bool blFound = false;
			for (int j = 0; j < ropBaggageBelts.GetSize(); j++)
			{
				if (polBlt->Bnam == ropBaggageBelts[j])
				{
					blFound = true;
					break;
				}
			}

			if (!blFound)
			{
				ogSgmData.Delete(&olSgmList[i],true);
			}
		}
	}
	return true;
}

int CBasicData::GetWaitingRoomsForGate(long lpGatUrno,CStringArray& ropWaitingRooms)
{
	ALODATA *polAloc = ogAloData.GetAloByName("WROGAT");
	if (polAloc == NULL)
		return 0;
	SGRDATA *polSgr = ogSgrData.GetSgrByUgtyAndStyp(polAloc->Urno,lpGatUrno);
	if (polSgr == NULL)
		return 0;

	CCSPtrArray<SGMDATA> olSgmList;
	ogSgmData.GetSgmDataByGrnUrno(olSgmList,polSgr->Urno);

	olSgmList.Sort(CompareBySortFlag);

	for (int i = 0; i < olSgmList.GetSize(); i++)
	{
		WRODATA *polWro = ogWROData.GetWROByUrno(olSgmList[i].Uval);
		if (polWro)
			ropWaitingRooms.Add(polWro->Wnam);
	}
	
	return ropWaitingRooms.GetSize();
}


bool CBasicData::SetWaitingRoomsForGate(long lpGatUrno,const CStringArray& ropWaitingRooms)
{
	ALODATA *polAloc = ogAloData.GetAloByName("WROGAT");
	if (polAloc == NULL)
		return false;
	SGRDATA *polSgr = ogSgrData.GetSgrByUgtyAndStyp(polAloc->Urno,lpGatUrno);
	if (polSgr == NULL)
	{
		polSgr = new SGRDATA;
		strcpy(polSgr->Appl,"WROGAT");
		polSgr->Cdat = CTime::GetCurrentTime();
//		polSgr->Fldn =
		strcpy(polSgr->Hopo,pcgHome);
		strcpy(polSgr->Grds,polAloc->Alod);
		strcpy(polSgr->Grpn,polAloc->Reft);
		strcpy(polSgr->Prfl,"0");
		polSgr->Styp = lpGatUrno;
		strncpy(polSgr->Tabn,polAloc->Reft,3);
		polSgr->Tabn[3] = '\0';
		polSgr->Ugty = polAloc->Urno;
		polSgr->Urno = ogBasicData.GetNextUrno();
		strcpy(polSgr->Usec,cgUserName);
//		polSgr->Useu =
		ogSgrData.Insert(polSgr,true);

	}

	CCSPtrArray<SGMDATA> olSgmList;
	ogSgmData.GetSgmDataByGrnUrno(olSgmList,polSgr->Urno);

	for (int i = 0; i < ropWaitingRooms.GetSize(); i++)
	{
		WRODATA *polWro = ogWROData.GetWROByName(ropWaitingRooms[i]);
		if (polWro)
		{
			bool blFound = false;
			for (int j = 0; j < olSgmList.GetSize(); j++)
			{
				if (olSgmList[j].Uval == polWro->Urno)
				{
					blFound = true;
					if (olSgmList[j].Sort != i)
					{
						olSgmList[j].Sort = i;
						ogSgmData.Update(&olSgmList[j],true);
					}
					break;
				}
			}

			if (!blFound)
			{
				SGMDATA *polSgm = new SGMDATA;
				polSgm->IsChanged = DATA_NEW;
				polSgm->Cdat = CTime::GetCurrentTime();
				strcpy(polSgm->Hopo,pcgHome);
//				polSgm->Prfl =
				strncpy(polSgm->Tabn,polAloc->Reft,3);
				polSgm->Tabn[3] = '\0';
				polSgm->Ugty = polAloc->Urno;
				polSgm->Urno = ogBasicData.GetNextUrno();
				strcpy(polSgm->Usec,cgUserName);
//				polSgm->Useu =
				polSgm->Usgr = polSgr->Urno;
				polSgm->Uval = polWro->Urno;
				polSgm->Valu = lpGatUrno;
				polSgm->Sort = i;

				ogSgmData.Insert(polSgm,true);									
			}
			else
			{
				olSgmList.RemoveAt(j);
			}
		}
	}
	
	for (i = 0; i < olSgmList.GetSize(); i++)
	{
		WRODATA *polWro = ogWROData.GetWROByUrno(olSgmList[i].Uval);
		if (polWro)
		{
			bool blFound = false;
			for (int j = 0; j < ropWaitingRooms.GetSize(); j++)
			{
				if (polWro->Wnam == ropWaitingRooms[j])
				{
					blFound = true;
					break;
				}
			}

			if (!blFound)
			{
				ogSgmData.Delete(&olSgmList[i],true);
			}
		}
	}
	return true;
}

int CBasicData::GetExitsForBaggageBelt(long lpBltUrno,CStringArray& ropExits)
{
	ALODATA *polAloc = ogAloData.GetAloByName("EXTBAG");
	if (polAloc == NULL)
		return 0;
	SGRDATA *polSgr = ogSgrData.GetSgrByUgtyAndStyp(polAloc->Urno,lpBltUrno);
	if (polSgr == NULL)
		return 0;

	CCSPtrArray<SGMDATA> olSgmList;
	ogSgmData.GetSgmDataByGrnUrno(olSgmList,polSgr->Urno);

	olSgmList.Sort(CompareBySortFlag);

	for (int i = 0; i < olSgmList.GetSize(); i++)
	{
		EXTDATA *polExt = ogEXTData.GetEXTByUrno(olSgmList[i].Uval);
		if (polExt)
			ropExits.Add(polExt->Enam);
	}
	
	return ropExits.GetSize();
}


bool CBasicData::SetExitsForBaggageBelt(long lpBltUrno,const CStringArray& ropExits)
{
	ALODATA *polAloc = ogAloData.GetAloByName("EXTBAG");
	if (polAloc == NULL)
		return false;
	SGRDATA *polSgr = ogSgrData.GetSgrByUgtyAndStyp(polAloc->Urno,lpBltUrno);
	if (polSgr == NULL)
	{
		polSgr = new SGRDATA;
		strcpy(polSgr->Appl,"EXTBAG");
		polSgr->Cdat = CTime::GetCurrentTime();
//		polSgr->Fldn =
		strcpy(polSgr->Hopo,pcgHome);
		strcpy(polSgr->Grds,polAloc->Alod);
		strcpy(polSgr->Grpn,polAloc->Reft);
		strcpy(polSgr->Prfl,"0");
		polSgr->Styp = lpBltUrno;
		strncpy(polSgr->Tabn,polAloc->Reft,3);
		polSgr->Tabn[3] = '\0';
		polSgr->Ugty = polAloc->Urno;
		polSgr->Urno = ogBasicData.GetNextUrno();
		strcpy(polSgr->Usec,cgUserName);
//		polSgr->Useu =
		ogSgrData.Insert(polSgr,true);

	}

	CCSPtrArray<SGMDATA> olSgmList;
	ogSgmData.GetSgmDataByGrnUrno(olSgmList,polSgr->Urno);

	for (int i = 0; i < ropExits.GetSize(); i++)
	{
		EXTDATA *polExt = ogEXTData.GetEXTByName(ropExits[i]);
		if (polExt)
		{
			bool blFound = false;
			for (int j = 0; j < olSgmList.GetSize(); j++)
			{
				if (olSgmList[j].Uval == polExt->Urno)
				{
					blFound = true;
					if (olSgmList[j].Sort != i)
					{
						olSgmList[j].Sort = i;
						ogSgmData.Update(&olSgmList[j],true);
					}
					break;
				}
			}

			if (!blFound)
			{
				SGMDATA *polSgm = new SGMDATA;
				polSgm->IsChanged = DATA_NEW;
				polSgm->Cdat = CTime::GetCurrentTime();
				strcpy(polSgm->Hopo,pcgHome);
//				polSgm->Prfl =
				strncpy(polSgm->Tabn,polAloc->Reft,3);
				polSgm->Tabn[3] = '\0';
				polSgm->Ugty = polAloc->Urno;
				polSgm->Urno = ogBasicData.GetNextUrno();
				strcpy(polSgm->Usec,cgUserName);
//				polSgm->Useu =
				polSgm->Usgr = polSgr->Urno;
				polSgm->Uval = polExt->Urno;
				polSgm->Valu = lpBltUrno;
				polSgm->Sort = i;

				ogSgmData.Insert(polSgm,true);									
			}
			else
			{
				olSgmList.RemoveAt(j);
			}
		}
	}
	
	for (i = 0; i < olSgmList.GetSize(); i++)
	{
		EXTDATA *polExt = ogEXTData.GetEXTByUrno(olSgmList[i].Uval);
		if (polExt)
		{
			bool blFound = false;
			for (int j = 0; j < ropExits.GetSize(); j++)
			{
				if (polExt->Enam == ropExits[j])
				{
					blFound = true;
					break;
				}
			}

			if (!blFound)
			{
				ogSgmData.Delete(&olSgmList[i],true);
			}
		}
	}
	return true;
}

BOOL CBasicData::LoadBitmapFromBMPFile( LPTSTR szFileName, HBITMAP *phBitmap,HPALETTE *phPalette )
{
     BITMAP  bm;

     *phBitmap = NULL;
     *phPalette = NULL;

     // Use LoadImage() to get the image loaded into a DIBSection
     *phBitmap = (HBITMAP)LoadImage( NULL, szFileName, IMAGE_BITMAP, 0, 0,
                 LR_CREATEDIBSECTION | LR_LOADFROMFILE );
     if( *phBitmap == NULL )
       return FALSE;

     // Get the color depth of the DIBSection
     GetObject(*phBitmap, sizeof(BITMAP), &bm );
     // If the DIBSection is 256 color or less, it has a color table
     if( ( bm.bmBitsPixel * bm.bmPlanes ) <= 8 )
     {
       HDC           hMemDC;
       HBITMAP       hOldBitmap;
       RGBQUAD       rgb[256];
       LPLOGPALETTE  pLogPal;
       WORD          i;

       // Create a memory DC and select the DIBSection into it
       hMemDC = CreateCompatibleDC( NULL );
       hOldBitmap = (HBITMAP)SelectObject( hMemDC, *phBitmap );
       // Get the DIBSection's color table
       GetDIBColorTable( hMemDC, 0, 256, rgb );
       // Create a palette from the color table
       pLogPal = (LOGPALETTE*)malloc( sizeof(LOGPALETTE) + (256*sizeof(PALETTEENTRY)) );
       pLogPal->palVersion = 0x300;
       pLogPal->palNumEntries = 256;
       for(i=0;i<256;i++)
       {
         pLogPal->palPalEntry[i].peRed = rgb[i].rgbRed;
         pLogPal->palPalEntry[i].peGreen = rgb[i].rgbGreen;
         pLogPal->palPalEntry[i].peBlue = rgb[i].rgbBlue;
         pLogPal->palPalEntry[i].peFlags = 0;
       }
       *phPalette = CreatePalette( pLogPal );
       // Clean up
       free( pLogPal );
       SelectObject( hMemDC, hOldBitmap );
       DeleteDC( hMemDC );
     }
     else   // It has no color table, so use a halftone palette
     {
       HDC    hRefDC;

       hRefDC = GetDC( NULL );
       *phPalette = CreateHalftonePalette( hRefDC );
       ReleaseDC( NULL, hRefDC );
     }
     return TRUE;
}

bool CBasicData::AddBlockingImages(CImageList& ropImageList)
{
	if (!ropImageList.Create(16,16,ILC_COLOR4,10,10))
		return false;



	char pclConfigPath[256];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	char pclBitmapCount[24];
    GetPrivateProfileString("GATPOS", "BlockedBitmapCount", "0", pclBitmapCount, sizeof pclBitmapCount, pclConfigPath);
	int ilBitmapCount = atoi(pclBitmapCount);
	for (int i = 0; i < ilBitmapCount; i++)
	{
		char pclBlockedBitmap[256];
		char pclBitmap[256];
		sprintf(pclBitmap,"BlockedBitmap%d",i+1);
	    GetPrivateProfileString("GATPOS",pclBitmap, "", pclBlockedBitmap, sizeof pclBlockedBitmap, pclConfigPath);

		HBITMAP hBitmap;
		HPALETTE hPalette;

		if (!this->LoadBitmapFromBMPFile(pclBlockedBitmap,&hBitmap,&hPalette))
			return false;

		CBitmap* polBitmap1 = new CBitmap;
		polBitmap1->Attach(hBitmap);

		if (ropImageList.Add(polBitmap1,(CBitmap *)NULL) < 0)
		{
			delete polBitmap1;
			return false;
		}

		polBitmap1->Detach();
		delete polBitmap1;
	}

	return true;
}

bool CBasicData::AddBlockingImages(AatBitmapComboBox& ropComboBox)
{
	char pclConfigPath[256];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	char pclBitmapCount[24];
    GetPrivateProfileString("GATPOS", "BlockedBitmapCount", "0", pclBitmapCount, sizeof pclBitmapCount, pclConfigPath);
	int ilBitmapCount = atoi(pclBitmapCount);
	for (int i = 0; i < ilBitmapCount; i++)
	{
		char pclBlockedBitmap[256];
		char pclBitmap[256];
		sprintf(pclBitmap,"BlockedBitmap%d",i+1);
	    GetPrivateProfileString("GATPOS",pclBitmap, "", pclBlockedBitmap, sizeof pclBlockedBitmap, pclConfigPath);

		if (ropComboBox.AddBitmap(pclBlockedBitmap) == CB_ERR)
			return false;
	}

	return true;
}

bool CBasicData::DisplayDamagedDataWarningMessage(bool bpDelete)
{
	if (bpDelete)
		return true;
	else
		return bmDisplayDamagedDataWarningMessage;
}


bool CBasicData::BackDoorEnabled()
{
	return bmUseBackDoor;
}

BOOL CBasicData::SetLocalDiff()
{
	BOOL	blResult = TRUE;
	int		ilTdi1;
	int		ilTdi2; 
	int		ilMin;
	int		ilHour;
	CTime	olTich ;	
	CTime olCurr;

	CCSPtrArray<APTDATA> olData;

	CString olWhere = CString("WHERE APC3 = '") + CString(pcgHome) + CString("'");

	if (ogAPTData.ReadSpecialData(&olData,olWhere.GetBuffer(0),ogAPTData.pcmAPTFieldList,false))
	{
		if(olData.GetSize() > 0)
		{
			olTich = olData[0].Tich;
			if (olTich == TIMENULL)
			{
				blResult = FALSE;
			}
			else
			{
				omTich = olTich;
			}

			if (strlen(olData[0].Tdi1) > 0)
			{
				ilTdi1 = atoi(olData[0].Tdi1);
				ilHour = ilTdi1 / 60;
				ilMin  = ilTdi1 % 60;
				omLocalDiff1 = CTimeSpan(0,ilHour ,ilMin ,0);
			}
			else
			{
				blResult = FALSE;
			}

			if (strlen(olData[0].Tdi2) > 0)
			{
				ilTdi2 = atoi(olData[0].Tdi2);
				ilHour = ilTdi2 / 60;
				ilMin  = ilTdi2 % 60;
				omLocalDiff2 = CTimeSpan(0,ilHour ,ilMin ,0);
			}
			else
			{
				blResult = FALSE;
			}
		}
		olData.DeleteAll();
	}
	else
	{
		blResult = FALSE;
	}

	return blResult;
}

void CBasicData::LocalToUtc(CTime &opTime)
{
	if(opTime != TIMENULL)
	{
		if(opTime < omTich)
		{
			opTime -= omLocalDiff1;
		}
		else
		{
			opTime -= omLocalDiff2;
		}
	}

}

void CBasicData::UtcToLocal(CTime &opTime)
{
	CTime olTichUtc;
	olTichUtc = omTich - CTimeSpan(omLocalDiff2);

	if(opTime != TIMENULL)
	{
		if(opTime < olTichUtc)
		{
			opTime += omLocalDiff1;
		}
		else
		{
			CString olFromDate;
			olFromDate.Format("%04d%02d%02d%02d%02d", omTich.GetYear(), omTich.GetMonth(), omTich.GetDay(), omTich.GetHour()-2, omTich.GetMinute());
			CString olToDate;
			olToDate.Format("%04d%02d%02d%02d%02d", omTich.GetYear(), omTich.GetMonth(), omTich.GetDay(), omTich.GetHour()-1, omTich.GetMinute());
			CString olOpDate = opTime.Format("%Y%m%d%H%M");
			if(olOpDate >= olFromDate && olOpDate <= olToDate)
			{
				opTime += omLocalDiff1;
			}
			else
			{
				opTime += omLocalDiff2;
			}
		}
	}
}

bool CBasicData::UseLocalTimeAsDefault()
{
	return bmUseLocalTimeAsDefault;
}

COleDateTime CBasicData::DBStringToDateTime(const CString& ropDBString)
{
	COleDateTime olTime;
	int year, month, day, hour = 0, min = 0, sec = 0;

    if (sscanf(ropDBString, "%4d%2d%2d%2d%2d%2d", &year, &month, &day, &hour, &min, &sec) < 3)
	{
		olTime.SetStatus(COleDateTime::invalid);
	}
	else
	{
		olTime = COleDateTime(year, month, day, hour, min, sec);
	}

	return olTime;
}

CString CBasicData::COleDateTimeToDBString(const COleDateTime &opDateTime)
{
	if (opDateTime.GetStatus() == COleDateTime::valid)
		return opDateTime.Format("%Y%m%d%H%M%S");
	else
		return "              ";
}


bool CBasicData::SendBroadcast()
{
	return this->CedaAction("SBC","SBCXXX", "", "", "","") == 0;
}

bool CBasicData::IsColorCodeAvailable()
{
	static int ilRc = -1;
	if (ilRc == -1)
	{
		char pclWhere[100], pclFields[100] = "TANA,FINA";
		char pclTable[100];
		sprintf(pclWhere,"WHERE Tana = 'ODA' AND Fina = 'RGBC'");
		sprintf(pclTable,"SYS%s",pcgTableExt);

		ilRc = CedaAction("RT", pclTable, pclFields, pclWhere, "", "BUF1");
	}

	if (ilRc)
		return true;
	else
		return false;

}

WORD CBasicData::AircraftTypeDescriptionLength()
{
	static int ilRc = -1;
	if (ilRc == -1)
	{
		char pclWhere[100], pclFields[100] = "TANA,FINA,FELE";
		char pclTable[100];
		sprintf(pclWhere,"WHERE Tana = 'ACT' AND Fina = 'ACFN'");
		sprintf(pclTable,"SYS%s",pcgTableExt);

		ilRc = CedaAction("RT", pclTable, pclFields, pclWhere, "", "BUF1");
		if (ilRc)
		{
			CString olData;
			if (this->GetBufferLine(0,olData))
			{
				CStringArray olList;
				::ExtractItemList(olData,&olList);
				ilRc = atoi(olList[2]);
			}
			else
				ilRc = 0;
		}
	}

	return ilRc;	
}

bool CBasicData::IsTerminalRestrictionForAirlinesAvailable()
{
	return CedaObject::IsFieldAvailable("ALT","TERG");
}

bool CBasicData::IsColumnForAbnormalFlightsReportAvailable()
{
	return CedaObject::IsFieldAvailable("DEN","RCOL");
}

bool CBasicData::DoesFieldExist(CString opTana, CString opFina)
{
	bool blDoesFieldExist = false;
	char pclWhere[100], pclFields[100] = "FINA", pclTable[100];
	sprintf(pclWhere,"WHERE TANA = '%s' AND FINA = '%s'", opTana, opFina);
	//sprintf(pclTable,"SYS%s",pcgTableExt);
	sprintf(pclTable,"SYSTAB");
	if(CedaAction("RT", pclTable, pclFields, pclWhere, "", "BUF1"))
	{
		CString olLine;
		blDoesFieldExist = GetBufferLine(0, olLine);
	}

	return blDoesFieldExist;
}


bool CBasicData::HasHtytabAdid()
{
	return omHtytabAdid;

}

bool CBasicData::IsPrmHandlingAgentEnabled()
{
	bool blIsPrmHandlingAgentEnabled = false;
	char pclConfigPath[142];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));
	char pclIsPrm[4];
	char pclUseHag[4];
	GetPrivateProfileString("GLOBAL","ISPRM","NO",pclIsPrm,sizeof(pclIsPrm),pclConfigPath);
	GetPrivateProfileString("GLOBAL","USEHAG","NO",pclUseHag,sizeof(pclUseHag),pclConfigPath);
	if(strcmp(pclIsPrm,"YES") == 0 && strcmp(pclUseHag,"YES") == 0)
	{
		blIsPrmHandlingAgentEnabled = true;
	}
	return blIsPrmHandlingAgentEnabled;
}

bool CBasicData::UseHandlingAgentFilter()
{
	bool blUseHandlingAgentFilter = false;
	char pclConfigPath[142];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));
	char pclUseHag[40];
	GetPrivateProfileString("BDPS-UIF","FILTERALTWITHHAG","NO",pclUseHag,sizeof(pclUseHag),pclConfigPath);
	if(strcmp(pclUseHag,"YES") == 0)
	{
		blUseHandlingAgentFilter = true;
	}
	return blUseHandlingAgentFilter;
}

bool CBasicData::IsChutesDisplayEnabled()
{
	bool blIsChutesDisplayEnabled = false;
	char pclConfigPath[142];
    if (getenv("CEDA") == NULL)
	{
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	}
    else
	{
        strcpy(pclConfigPath, getenv("CEDA"));
	}
	char pclIsChutes[10];;
	GetPrivateProfileString("FIPS","CHUTE_DISPLAY","NO",pclIsChutes,sizeof(pclIsChutes),pclConfigPath);
	if(strcmp(pclIsChutes,"TRUE") == 0)
	{
		blIsChutesDisplayEnabled = true;
	}
	return blIsChutesDisplayEnabled;
}

//Added by Christine
bool CBasicData::IsContinentAvailable()
{
	return CedaObject::IsFieldAvailable("APT","CONT");
	//return ogBasicData.DoesFieldExist("APT","CONT");
}

CString CBasicData::GetFieldByUrno( CString sTable, CString urno, CString sColumn )
{
	CString sItem = "" ;
	CString sUrno ;
	//sUrno.Format( "%d", urno );
	VERIFY( ogBCD.GetField( sTable, "URNO", urno, sColumn, sItem ) );		// data should be there, maybe using wrong table
	return sItem ;
};	// GetFieldByUrno


int CBasicData::GetGatesForGate(long lpGatUrno,CStringArray& ropGates)
{
	ALODATA *polAloc = ogAloData.GetAloByName("GATGAT");
	if (polAloc == NULL)
	{
		return 0;
	}
	SGRDATA *polSgr = ogSgrData.GetSgrByUgtyAndStyp(polAloc->Urno,lpGatUrno);
	
	if (polSgr == NULL)
	{
		return 0;
	}

	CCSPtrArray<SGMDATA> olSgmList;
	ogSgmData.GetSgmDataByGrnUrno(olSgmList,polSgr->Urno);

	olSgmList.Sort(CompareBySortFlag);

	for (int i = 0; i < olSgmList.GetSize(); i++)
	{
		
		GATDATA *polGat = ogGATData.GetGATByUrno(olSgmList[i].Uval);
		if (polGat)
		{
			ropGates.Add(polGat->Gnam);
		}
	}
	
	return ropGates.GetSize();
}


bool CBasicData::SetGatesForGate(long lpGatUrno,const CStringArray& ropGates)
{
	ALODATA *polAloc = ogAloData.GetAloByName("GATGAT");
	if (polAloc == NULL)
		return false;
	SGRDATA *polSgr = ogSgrData.GetSgrByUgtyAndStyp(polAloc->Urno,lpGatUrno);
	if (polSgr == NULL)
	{
		polSgr = new SGRDATA;
		strcpy(polSgr->Appl,"GATGAT");
		polSgr->Cdat = CTime::GetCurrentTime();
//		polSgr->Fldn =
		strcpy(polSgr->Hopo,pcgHome);
		strcpy(polSgr->Grds,polAloc->Alod);
		strcpy(polSgr->Grpn,polAloc->Reft);
		strcpy(polSgr->Prfl,"0");
		polSgr->Styp = lpGatUrno;
		strncpy(polSgr->Tabn,polAloc->Reft,3);
		polSgr->Tabn[3] = '\0';
		polSgr->Ugty = polAloc->Urno;
		polSgr->Urno = ogBasicData.GetNextUrno();
		strcpy(polSgr->Usec,cgUserName);
//		polSgr->Useu =
		ogSgrData.Insert(polSgr,true);

	}

	CCSPtrArray<SGMDATA> olSgmList;
	ogSgmData.GetSgmDataByGrnUrno(olSgmList,polSgr->Urno);

	for (int i = 0; i < ropGates.GetSize(); i++)
	{
		GATDATA *polGat = ogGATData.GetGATByName(ropGates[i]);
		if (polGat)
		{
			bool blFound = false;
			for (int j = 0; j < olSgmList.GetSize(); j++)
			{
				if (olSgmList[j].Uval == polGat->Urno)
				{
					blFound = true;
					if (olSgmList[j].Sort != i)
					{
						olSgmList[j].Sort = i;
						ogSgmData.Update(&olSgmList[j],true);
					}
					break;
				}
			}

			if (!blFound)
			{
				SGMDATA *polSgm = new SGMDATA;
				polSgm->IsChanged = DATA_NEW;
				polSgm->Cdat = CTime::GetCurrentTime();
				strcpy(polSgm->Hopo,pcgHome);
//				polSgm->Prfl =
				strncpy(polSgm->Tabn,polAloc->Reft,3);
				polSgm->Tabn[3] = '\0';
				polSgm->Ugty = polAloc->Urno;
				polSgm->Urno = ogBasicData.GetNextUrno();
				strcpy(polSgm->Usec,cgUserName);
//				polSgm->Useu =
				polSgm->Usgr = polSgr->Urno;
				polSgm->Uval = polGat->Urno;
				polSgm->Valu = lpGatUrno;
				polSgm->Sort = i;

				ogSgmData.Insert(polSgm,true);									
			}
			else
			{
				olSgmList.RemoveAt(j);
			}
		}
	}
	
	for (i = 0; i < olSgmList.GetSize(); i++)
	{
		
		GATDATA *polGat = ogGATData.GetGATByUrno(olSgmList[i].Uval);
		if (polGat)
		{
			bool blFound = false;
			for (int j = 0; j < ropGates.GetSize(); j++)
			{
				if (polGat->Gnam == ropGates[j])
				{
					blFound = true;
					break;
				}
			}

			if (!blFound)
			{
				ogSgmData.Delete(&olSgmList[i],true);
			}
		}
	}
	return true;
}