#if !defined(AFX_QUALIFIKATIONENDLG_H__C2353D41_6253_11D1_B3D0_0000C016B067__INCLUDED_)
#define AFX_QUALIFIKATIONENDLG_H__C2353D41_6253_11D1_B3D0_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// QualifikationenDlg.h : header file
//
#include <CedaPerData.h>
#include <CCSEdit.h>

/////////////////////////////////////////////////////////////////////////////
// QualifikationenDlg dialog

class QualifikationenDlg : public CDialog
{
// Construction
public:
	QualifikationenDlg(PERDATA *popPer,CWnd* pParent = NULL);   // standard constructor
	CStatic	*pomStatus;

// Dialog Data
	//{{AFX_DATA(QualifikationenDlg)
	enum { IDD = IDD_QUALIFIKATIONENDLG };
	CButton	m_OK;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_PRIO;
	CCSEdit	m_PRMC;
	CCSEdit	m_PRMN;
	CCSEdit	m_REMA;
	CCSEdit	m_GRUP;
	CString	m_Caption;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(QualifikationenDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(QualifikationenDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	PERDATA *pomPer;
	CString	omOldCode;
	bool	bmChangeAction;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_QUALIFIKATIONENDLG_H__C2353D41_6253_11D1_B3D0_0000C016B067__INCLUDED_)
