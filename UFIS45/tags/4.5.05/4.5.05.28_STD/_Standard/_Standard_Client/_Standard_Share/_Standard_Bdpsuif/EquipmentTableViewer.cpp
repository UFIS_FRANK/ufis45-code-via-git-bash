// EquipmentTableViewer.cpp 
//

#include <stdafx.h>
#include <EquipmentTableViewer.h>
#include <CedaEqtData.h>
#include <CcsDdx.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void EquipmentTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// EquipmentTableViewer
//

EquipmentTableViewer::EquipmentTableViewer(CCSPtrArray<EQUDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomEquipmentTable = NULL;
    ogDdx.Register(this, EQU_CHANGE, CString("EquipmentTableViewer"), CString("Equipment Update"), EquipmentTableCf);
    ogDdx.Register(this, EQU_NEW,    CString("EquipmentTableViewer"), CString("Equipment New"),    EquipmentTableCf);
    ogDdx.Register(this, EQU_DELETE, CString("EquipmentTableViewer"), CString("Equipment Delete"), EquipmentTableCf);
}

//-----------------------------------------------------------------------------------------------

EquipmentTableViewer::~EquipmentTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void EquipmentTableViewer::Attach(CCSTable *popTable)
{
    pomEquipmentTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void EquipmentTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void EquipmentTableViewer::MakeLines()
{
	int ilEquipmentCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilEquipmentCount; ilLc++)
	{
		EQUDATA *prlEquipmentData = &pomData->GetAt(ilLc);
		MakeLine(prlEquipmentData);
	}
}

//-----------------------------------------------------------------------------------------------

void EquipmentTableViewer::MakeLine(EQUDATA *prpEquipment)
{

    //if( !IsPassFilter(prpEquipment)) return;

    // Update viewer data for this shift record
    EQUIPMENTTABLE_LINEDATA rlEquipment;


	rlEquipment.Urno = prpEquipment->Urno;
	rlEquipment.GKey = prpEquipment->Gkey;
	rlEquipment.Gcde = prpEquipment->Gcde;
	rlEquipment.Enam = prpEquipment->Enam;
	rlEquipment.Eqps = prpEquipment->Eqps;
	rlEquipment.Etyp = prpEquipment->Etyp;
	rlEquipment.Ivnr = prpEquipment->Ivnr;
	rlEquipment.Tele = prpEquipment->Tele;
	rlEquipment.Rema = prpEquipment->Rema;
	rlEquipment.Crqu = prpEquipment->Crqu;
	
	CreateLine(&rlEquipment);
}

//-----------------------------------------------------------------------------------------------

void EquipmentTableViewer::CreateLine(EQUIPMENTTABLE_LINEDATA *prpEquipment)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareEquipment(prpEquipment, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	EQUIPMENTTABLE_LINEDATA rlEquipment;
	rlEquipment = *prpEquipment;
    omLines.NewAt(ilLineno, rlEquipment);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void EquipmentTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 6;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomEquipmentTable->SetShowSelection(TRUE);
	pomEquipmentTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 50;  //CODE 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING866),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 200;  //NAME
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING866),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 50;  //PARKING STAND
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING866),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 200;  //TYPE
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING866),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 110;  //MANUFACTURER
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING866),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 75;  //TELEPHON
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING866),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 150;  //QUALIFICATION
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING866),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332;  //REMARK
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING866),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	
	pomEquipmentTable->SetHeaderFields(omHeaderDataArray);
	pomEquipmentTable->SetDefaultSeparator();
	pomEquipmentTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Gcde;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Enam;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Eqps;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = this->GetEquipmentType(omLines[ilLineNo].GKey);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = omLines[ilLineNo].Etyp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = omLines[ilLineNo].Tele;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Crqu;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Rema;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		pomEquipmentTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomEquipmentTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString EquipmentTableViewer::Format(EQUIPMENTTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool EquipmentTableViewer::FindEquipment(char *pcpEquipmentKeya, char *pcpEquipmentKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpEquipmentKeya) &&
			 (omLines[ilItem].Keyd == pcpEquipmentKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void EquipmentTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    EquipmentTableViewer *polViewer = (EquipmentTableViewer *)popInstance;
    if (ipDDXType == EQU_CHANGE || ipDDXType == EQU_NEW) polViewer->ProcessEquipmentChange((EQUDATA *)vpDataPointer);
    if (ipDDXType == EQU_DELETE) polViewer->ProcessEquipmentDelete((EQUDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void EquipmentTableViewer::ProcessEquipmentChange(EQUDATA *prpEquipment)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;
	CString olText;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpEquipment->Gcde;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpEquipment->Enam;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpEquipment->Eqps;
	olLine.NewAt(olLine.GetSize(), rlColumn);


	rlColumn.Text = this->GetEquipmentType(prpEquipment->Gkey);
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpEquipment->Etyp;
	olLine.NewAt(olLine.GetSize(), rlColumn);


	rlColumn.Text = prpEquipment->Tele;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpEquipment->Crqu;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpEquipment->Rema;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	
	if (FindLine(prpEquipment->Urno, ilItem))
	{
        EQUIPMENTTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpEquipment->Urno;
		prlLine->GKey = prpEquipment->Gkey;
		prlLine->Gcde = prpEquipment->Gcde;
		prlLine->Eqps = prpEquipment->Eqps;
		prlLine->Etyp = prpEquipment->Etyp;
		prlLine->Ivnr = prpEquipment->Ivnr;
		prlLine->Tele = prpEquipment->Tele;
		prlLine->Crqu = prpEquipment->Crqu;
		prlLine->Rema = prpEquipment->Rema;
		
		pomEquipmentTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomEquipmentTable->DisplayTable();
	}
	else
	{
		MakeLine(prpEquipment);
		if (FindLine(prpEquipment->Urno, ilItem))
		{
	        EQUIPMENTTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomEquipmentTable->AddTextLine(olLine, (void *)prlLine);
				pomEquipmentTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void EquipmentTableViewer::ProcessEquipmentDelete(EQUDATA *prpEquipment)
{
	int ilItem;
	if (FindLine(prpEquipment->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomEquipmentTable->DeleteTextLine(ilItem);
		pomEquipmentTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool EquipmentTableViewer::IsPassFilter(EQUDATA *prpEquipment)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int EquipmentTableViewer::CompareEquipment(EQUIPMENTTABLE_LINEDATA *prpEquipment1, EQUIPMENTTABLE_LINEDATA *prpEquipment2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpEquipment1->Tifd == prpEquipment2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpEquipment1->Tifd > prpEquipment2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void EquipmentTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool EquipmentTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void EquipmentTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void EquipmentTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING982);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool EquipmentTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool EquipmentTableViewer::PrintTableLine(EQUIPMENTTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;


			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text = prpLine->Gcde;
				break;
			case 1:
				rlElement.Text = prpLine->Enam;
				break;
			case 2:
				rlElement.Text = prpLine->Eqps;
				break;
			case 3:
				rlElement.Text = this->GetEquipmentType(prpLine->GKey);
				break;
			case 4:
				rlElement.Text = prpLine->Etyp;
				break;
			case 5:
				rlElement.Text = prpLine->Tele;
				break;
			case 6:
				rlElement.Text = prpLine->Crqu;
				break;
			case 7:
				rlElement.FrameRight = PRINT_FRAMETHIN;
				rlElement.Text = prpLine->Rema;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

CString EquipmentTableViewer::GetEquipmentType(long lpGKey)
{
	CString olGkey;

	if(lpGKey > 0)
	{
		EQTDATA *prlEqt = ogEqtData.GetEqtByUrno(lpGKey);
		if (prlEqt == NULL)
			olGkey = "";
		else
			olGkey = prlEqt->Code;
	}

	return olGkey;
}

//-----------------------------------------------------------------------------------------------
