#if !defined(AFX_FIDSLOGONAMESDLG_H__E8F15454_4A26_11D7_8007_00010215BFDE__INCLUDED_)
#define AFX_FIDSLOGONAMESDLG_H__E8F15454_4A26_11D7_8007_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FidsLogoNamesDlg.h : header file
//
#include <CedaFLGData.h>
#include <CCSEdit.h>
#include <CCSTable.h>
#include <CCSComboBox.h>

/////////////////////////////////////////////////////////////////////////////
// FidsLogoNamesDlg dialog

class FidsLogoNamesDlg : public CDialog
{
// Construction
public:
	FidsLogoNamesDlg(FLGDATA *popFLG,const CString& ropCaption,CWnd* pParent = NULL);   // standard constructor
	~FidsLogoNamesDlg();

// Dialog Data
	//{{AFX_DATA(FidsLogoNamesDlg)
	enum { IDD = IDD_FIDS_LOGO_NAMES };
	
	CCSEdit	m_LGSN;
	CCSEdit	m_LGFN;
	CCSEdit m_TYPE;
	CCSEdit m_ALC2;
	CCSEdit m_ALC3;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;	
	
	CButton	m_BTN_TYPE;
	CButton	m_BTN_ALC2;
	CStatic	m_BTN_ALC3;
	
	CButton	m_OK;
	CButton	m_CANCEL;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FidsLogoNamesDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(FidsLogoNamesDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
    afx_msg void OnButtonTypeClicked();
	afx_msg void OnButtonAlc2Clicked();
	afx_msg void OnButtonAlc3Clicked();
	afx_msg LONG OnKillfocus(UINT wParam, LONG lParam);		
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	enum enumOperation{OP_INSERT,OP_DELETE};

public:	
	bool SaveDPTData();

private:
	CString		m_Caption;
	FLGDATA		*pomFLG;
	CCSTable	*pomTable;
	CMapStringToString omMapAlc2ToAlc3;
	CMapStringToString omMapAlc3ToAlc2;
	CStringList omListAlc2CombineAlc3;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIDSLOGONAMESDLG_H__E8F15454_4A26_11D7_8007_00010215BFDE__INCLUDED_)
