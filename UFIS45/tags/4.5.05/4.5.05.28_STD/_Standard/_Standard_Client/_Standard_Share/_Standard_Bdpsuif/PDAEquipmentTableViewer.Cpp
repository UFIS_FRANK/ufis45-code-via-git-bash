// PdaTableViewer.cpp 
//

#include <stdafx.h>
#include <PDAEquipmentTableViewer.h>
#include <CCSDdx.h>
#include <resource.h>
#include <cedastfdata.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void PdaTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// PdaTableViewer
//

PdaTableViewer::PdaTableViewer(CCSPtrArray<PDADATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomPdaTable = NULL;
    ogDdx.Register(this, PDA_NEW,	 CString("PDATABLEVIEWER"), CString("Pda New"),	 PdaTableCf);
    ogDdx.Register(this, PDA_CHANGE, CString("PDATABLEVIEWER"), CString("Pda Update"), PdaTableCf);
    ogDdx.Register(this, PDA_DELETE, CString("PDATABLEVIEWER"), CString("Pda Delete"), PdaTableCf);


	char pclConfigPath[256];
	char pclOrgCodes[256] = "";
	
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI");
	else
		strcpy(pclConfigPath, getenv("CEDA"));
	
	GetPrivateProfileString(ogAppName,"PDA_EMPLOYEE_ORG_CODE", "", pclOrgCodes, sizeof pclOrgCodes, pclConfigPath);


	CString olWhere ;

	if(CString(pclOrgCodes).IsEmpty())
		olWhere = "ORDER BY LANM";
	else
		olWhere = "where urno in (select surn from sortab where code='" + CString(pclOrgCodes) + CString("') ORDER BY LANM");;


	ogStfData.Read(olWhere.GetBuffer(0));


}

//-----------------------------------------------------------------------------------------------

PdaTableViewer::~PdaTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
	ogStfData.ClearAll();
}

//-----------------------------------------------------------------------------------------------

void PdaTableViewer::Attach(CCSTable *popTable)
{
    pomPdaTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void PdaTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void PdaTableViewer::MakeLines()
{
	int ilPdaCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilPdaCount; ilLc++)
	{
		PDADATA *prlPdaData = &pomData->GetAt(ilLc);
		MakeLine(prlPdaData);
	}
}

//-----------------------------------------------------------------------------------------------

void PdaTableViewer::MakeLine(PDADATA *prpPda)
{

    //if( !IsPassFilter(prpPda)) return;

    // Update viewer data for this shift record
    PDA_LINEDATA rlPda;

	rlPda.Urno = prpPda->Urno; 
	rlPda.Pdid = prpPda->Pdid; 
	rlPda.Ipad = prpPda->Ipad; 
	rlPda.Vafr = prpPda->Vafr.Format("%d.%m.%Y %H:%M");
	rlPda.Vato = prpPda->Vato.Format("%d.%m.%Y %H:%M");

	CreateLine(&rlPda);
}

//-----------------------------------------------------------------------------------------------

void PdaTableViewer::CreateLine(PDA_LINEDATA *prpPda)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (ComparePda(prpPda, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	PDA_LINEDATA rlPda;
	rlPda = *prpPda;
    omLines.NewAt(ilLineno, rlPda);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void PdaTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 3;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomPdaTable->SetShowSelection(TRUE);
	pomPdaTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING32814),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 150; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING32814),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING32814),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING32814),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomPdaTable->SetHeaderFields(omHeaderDataArray);
	pomPdaTable->SetDefaultSeparator();
	pomPdaTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Pdid;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Ipad;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomPdaTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomPdaTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString PdaTableViewer::Format(PDA_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool PdaTableViewer::FindPda(char *pcpPdaKeya, char *pcpPdaKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpPdaKeya) &&
			 (omLines[ilItem].Keyd == pcpPdaKeyd)    )
			return true;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void PdaTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    PdaTableViewer *polViewer = (PdaTableViewer *)popInstance;
    if (ipDDXType == PDA_CHANGE || ipDDXType == PDA_NEW) polViewer->ProcessPdaChange((PDADATA *)vpDataPointer);
    if (ipDDXType == PDA_DELETE) polViewer->ProcessPdaDelete((PDADATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void PdaTableViewer::ProcessPdaChange(PDADATA *prpPda)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpPda->Pdid;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpPda->Ipad;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);


	rlColumn.Text = prpPda->Vafr.Format("%d.%m.%Y %H:%M"); 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpPda->Vato.Format("%d.%m.%Y %H:%M");
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);


	if (FindLine(prpPda->Urno, ilItem))
	{
        PDA_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpPda->Urno;
		prlLine->Pdid = prpPda->Pdid;
		prlLine->Ipad = prpPda->Ipad;
		prlLine->Vafr = prpPda->Vafr.Format("%d.%m.%Y %H:%M");
		prlLine->Vato = prpPda->Vato.Format("%d.%m.%Y %H:%M");

		pomPdaTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomPdaTable->DisplayTable();
	}
	else
	{
		MakeLine(prpPda);
		if (FindLine(prpPda->Urno, ilItem))
		{
	        PDA_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomPdaTable->AddTextLine(olLine, (void *)prlLine);
				pomPdaTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void PdaTableViewer::ProcessPdaDelete(PDADATA *prpPda)
{
	int ilItem;
	if (FindLine(prpPda->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomPdaTable->DeleteTextLine(ilItem);
		pomPdaTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool PdaTableViewer::IsPassFilter(PDADATA *prpPda)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int PdaTableViewer::ComparePda(PDA_LINEDATA *prpPda1, PDA_LINEDATA *prpPda2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpPda1->Tifd == prpPda2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpPda1->Tifd > prpPda2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void PdaTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool PdaTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void PdaTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void PdaTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING32805);
	int ilOrientation = PRINT_PORTRAET;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool PdaTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool PdaTableViewer::PrintTableLine(PDA_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				{
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Pdid;
				}
				break;

			case 1:
				{
					rlElement.Text		= prpLine->Ipad;
				}
				break;
			case 2:
				{
					rlElement.Text		= prpLine->Vafr;
				}
				break;
			case 3:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Vato;
				}
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
