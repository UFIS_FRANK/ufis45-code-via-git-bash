// AbsencePatternTableViewer.cpp 
//

#include "stdafx.h"
#include "AbsencePatternTableViewer.h"
#include "CcsDdx.h"
#include "resource.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void AbsencePatternTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// AbsencePatternTableViewer
//

AbsencePatternTableViewer::AbsencePatternTableViewer(CCSPtrArray<MAWDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomAbsencePatternTable = NULL;
    ogDdx.Register(this, MAW_CHANGE, CString("ABSENCEPATTERNTABLEVIEWER"), CString("AbsencePattern Update"), AbsencePatternTableCf);
    ogDdx.Register(this, MAW_NEW,    CString("ABSENCEPATTERNTABLEVIEWER"), CString("AbsencePattern New"),    AbsencePatternTableCf);
    ogDdx.Register(this, MAW_DELETE, CString("ABSENCEPATTERNTABLEVIEWER"), CString("AbsencePattern Delete"), AbsencePatternTableCf);
}

//-----------------------------------------------------------------------------------------------

AbsencePatternTableViewer::~AbsencePatternTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void AbsencePatternTableViewer::Attach(CCSTable *popTable)
{
    pomAbsencePatternTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void AbsencePatternTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void AbsencePatternTableViewer::MakeLines()
{
	int ilAbsencePatternCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilAbsencePatternCount; ilLc++)
	{
		MAWDATA *prlAbsencePatternData = &pomData->GetAt(ilLc);
		MakeLine(prlAbsencePatternData);
	}
}

//-----------------------------------------------------------------------------------------------

void AbsencePatternTableViewer::MakeLine(MAWDATA *prpAbsencePattern)
{

    //if( !IsPassFilter(prpAbsencePattern)) return;

    // Update viewer data for this shift record
    ABSENCEPATTERNTABLE_LINEDATA rlAbsencePattern;

	rlAbsencePattern.Name = prpAbsencePattern->Name; 
	rlAbsencePattern.Dflt = prpAbsencePattern->Dflt; 
	rlAbsencePattern.Urno = prpAbsencePattern->Urno; 

	CreateLine(&rlAbsencePattern);
}

//-----------------------------------------------------------------------------------------------

void AbsencePatternTableViewer::CreateLine(ABSENCEPATTERNTABLE_LINEDATA *prpAbsencePattern)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareAbsencePattern(prpAbsencePattern, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	ABSENCEPATTERNTABLE_LINEDATA rlAbsencePattern;
	rlAbsencePattern = *prpAbsencePattern;
    omLines.NewAt(ilLineno, rlAbsencePattern);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void AbsencePatternTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 2;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomAbsencePatternTable->SetShowSelection(TRUE);
	pomAbsencePatternTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING852),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING852),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomAbsencePatternTable->SetHeaderFields(omHeaderDataArray);

	pomAbsencePatternTable->SetDefaultSeparator();
	pomAbsencePatternTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Name;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dflt;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		pomAbsencePatternTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomAbsencePatternTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString AbsencePatternTableViewer::Format(ABSENCEPATTERNTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool AbsencePatternTableViewer::FindAbsencePattern(char *pcpAbsencePatternKeya, char *pcpAbsencePatternKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpAbsencePatternKeya) &&
			 (omLines[ilItem].Keyd == pcpAbsencePatternKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void AbsencePatternTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    AbsencePatternTableViewer *polViewer = (AbsencePatternTableViewer *)popInstance;
    if (ipDDXType == MAW_CHANGE || ipDDXType == MAW_NEW) polViewer->ProcessAbsencePatternChange((MAWDATA *)vpDataPointer);
    if (ipDDXType == MAW_DELETE) polViewer->ProcessAbsencePatternDelete((MAWDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void AbsencePatternTableViewer::ProcessAbsencePatternChange(MAWDATA *prpAbsencePattern)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpAbsencePattern->Name;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAbsencePattern->Dflt;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (FindLine(prpAbsencePattern->Urno, ilItem))
	{
        ABSENCEPATTERNTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Name = prpAbsencePattern->Name;
		prlLine->Dflt = prpAbsencePattern->Dflt;
		prlLine->Urno = prpAbsencePattern->Urno;

		pomAbsencePatternTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomAbsencePatternTable->DisplayTable();
	}
	else
	{
		MakeLine(prpAbsencePattern);
		if (FindLine(prpAbsencePattern->Urno, ilItem))
		{
	        ABSENCEPATTERNTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomAbsencePatternTable->AddTextLine(olLine, (void *)prlLine);
				pomAbsencePatternTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void AbsencePatternTableViewer::ProcessAbsencePatternDelete(MAWDATA *prpAbsencePattern)
{
	int ilItem;
	if (FindLine(prpAbsencePattern->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomAbsencePatternTable->DeleteTextLine(ilItem);
		pomAbsencePatternTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool AbsencePatternTableViewer::IsPassFilter(MAWDATA *prpAbsencePattern)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int AbsencePatternTableViewer::CompareAbsencePattern(ABSENCEPATTERNTABLE_LINEDATA *prpAbsencePattern1, ABSENCEPATTERNTABLE_LINEDATA *prpAbsencePattern2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpAbsencePattern1->Tifd == prpAbsencePattern2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpAbsencePattern1->Tifd > prpAbsencePattern2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void AbsencePatternTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool AbsencePatternTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void AbsencePatternTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void AbsencePatternTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING897);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool AbsencePatternTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool AbsencePatternTableViewer::PrintTableLine(ABSENCEPATTERNTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Name;
				break;
			case 1:
				rlElement.FrameRight = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Dflt;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
