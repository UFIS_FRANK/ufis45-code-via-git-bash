#if !defined(AFX_HOLIDAYDLG_H__56BF0B42_2049_11D1_B38A_0000C016B063__INCLUDED_)
#define AFX_HOLIDAYDLG_H__56BF0B42_2049_11D1_B38A_0000C016B063__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// HolidayDlg.h : header file
//
#include <CedaHolData.h>
#include <CCSEdit.h>

/////////////////////////////////////////////////////////////////////////////
// HolidayDlg dialog

class HolidayDlg : public CDialog
{
// Construction
public:
	HolidayDlg(HOLDATA *popHol,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(HolidayDlg)
	enum { IDD = IDD_HOLIDAYDLG };
	CButton	m_OK;
	CString	m_Caption;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit	m_REMA;
	CCSEdit	m_TYPE;
	CCSEdit	m_HDAY;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(HolidayDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(HolidayDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	HOLDATA *pomHol;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HOLIDAYDLG_H__56BF0B42_2049_11D1_B38A_0000C016B063__INCLUDED_)
