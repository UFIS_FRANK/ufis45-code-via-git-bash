// AadTableViewer.cpp 
//

#include <stdafx.h>
#include <AirportArrivalDeskTableViewer.h>
#include <CCSDdx.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void AadTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// AadTableViewer
//

AadTableViewer::AadTableViewer(CCSPtrArray<AADDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomAadTable = NULL;
    ogDdx.Register(this, AAD_NEW,	 CString("AADTABLEVIEWER"), CString("Aad New"),	 AadTableCf);
    ogDdx.Register(this, AAD_CHANGE, CString("AADTABLEVIEWER"), CString("Aad Update"), AadTableCf);
    ogDdx.Register(this, AAD_DELETE, CString("AADTABLEVIEWER"), CString("Aad Delete"), AadTableCf);
}

//-----------------------------------------------------------------------------------------------

AadTableViewer::~AadTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void AadTableViewer::Attach(CCSTable *popTable)
{
    pomAadTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void AadTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void AadTableViewer::MakeLines()
{
	int ilAadCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilAadCount; ilLc++)
	{
		AADDATA *prlAadData = &pomData->GetAt(ilLc);
		MakeLine(prlAadData);
	}
}

//-----------------------------------------------------------------------------------------------

void AadTableViewer::MakeLine(AADDATA *prpAad)
{

    //if( !IsPassFilter(prpAad)) return;

    // Update viewer data for this shift record
    AAD_LINEDATA rlAad;

	rlAad.Urno = prpAad->Urno; 
	rlAad.Deid = prpAad->Deid; 
	rlAad.Name = prpAad->Name; 
	rlAad.Moti = prpAad->Moti; 
	rlAad.Vafr = prpAad->Vafr.Format("%d.%m.%Y %H:%M");
	rlAad.Vato = prpAad->Vato.Format("%d.%m.%Y %H:%M");

	CreateLine(&rlAad);
}

//-----------------------------------------------------------------------------------------------

void AadTableViewer::CreateLine(AAD_LINEDATA *prpAad)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareAad(prpAad, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	AAD_LINEDATA rlAad;
	rlAad = *prpAad;
    omLines.NewAt(ilLineno, rlAad);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void AadTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 3;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomAadTable->SetShowSelection(TRUE);
	pomAadTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING32812),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 166; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING32812),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING32812),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING32812),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING32812),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomAadTable->SetHeaderFields(omHeaderDataArray);
	pomAadTable->SetDefaultSeparator();
	pomAadTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Deid;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Name;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Moti;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomAadTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomAadTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString AadTableViewer::Format(AAD_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool AadTableViewer::FindAad(char *pcpAadKeya, char *pcpAadKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpAadKeya) &&
			 (omLines[ilItem].Keyd == pcpAadKeyd)    )
			return true;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void AadTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    AadTableViewer *polViewer = (AadTableViewer *)popInstance;
    if (ipDDXType == AAD_CHANGE || ipDDXType == AAD_NEW) polViewer->ProcessAadChange((AADDATA *)vpDataPointer);
    if (ipDDXType == AAD_DELETE) polViewer->ProcessAadDelete((AADDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void AadTableViewer::ProcessAadChange(AADDATA *prpAad)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpAad->Deid;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpAad->Name;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpAad->Moti;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpAad->Vafr.Format("%d.%m.%Y %H:%M"); 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAad->Vato.Format("%d.%m.%Y %H:%M");
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);


	if (FindLine(prpAad->Urno, ilItem))
	{
        AAD_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpAad->Urno;
		prlLine->Deid = prpAad->Deid;
		prlLine->Name = prpAad->Name;
		prlLine->Moti = prpAad->Moti;
		prlLine->Vafr = prpAad->Vafr.Format("%d.%m.%Y %H:%M");
		prlLine->Vato = prpAad->Vato.Format("%d.%m.%Y %H:%M");

		pomAadTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomAadTable->DisplayTable();
	}
	else
	{
		MakeLine(prpAad);
		if (FindLine(prpAad->Urno, ilItem))
		{
	        AAD_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomAadTable->AddTextLine(olLine, (void *)prlLine);
				pomAadTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void AadTableViewer::ProcessAadDelete(AADDATA *prpAad)
{
	int ilItem;
	if (FindLine(prpAad->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomAadTable->DeleteTextLine(ilItem);
		pomAadTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool AadTableViewer::IsPassFilter(AADDATA *prpAad)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int AadTableViewer::CompareAad(AAD_LINEDATA *prpAad1, AAD_LINEDATA *prpAad2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpAad1->Tifd == prpAad2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpAad1->Tifd > prpAad2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void AadTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool AadTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void AadTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void AadTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING32815);
	int ilOrientation = PRINT_PORTRAET;//  PRINT_PORTRAET  
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool AadTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool AadTableViewer::PrintTableLine(AAD_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				{
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Deid;
				}
				break;
			case 1:
				{
					rlElement.Text		= prpLine->Name;
				}
				break;
			case 2:
				{
					rlElement.Text		= prpLine->Moti;
				}
				break;
			case 3:
				{
					rlElement.Text		= prpLine->Vafr;
				}
				break;
			case 4:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Vato;
				}
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
