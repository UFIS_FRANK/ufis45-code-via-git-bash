#ifndef __MitarbeiterstammTableViewer_H__
#define __MitarbeiterstammTableViewer_H__

#include <stdafx.h>
#include <CedaStfData.h>
#include <CedaSorData.h>
#include <CedaSpfData.h>
#include <CedaSpeData.h>
#include <CedaSwgData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct MITARBEITERSTAMMTABLE_LINEDATA
{
	long 	Urno; 		// Eindeutige Datensatz-Nr.
	CString	Finm;		// Vorname
	CString	Gsmn;		// GSM-Number
	CString	Ktou;		// Urlaubsanspr�che
	CString	Lanm;		// Name
	CString	Lino;		// License Number
	CString	Makr; 		// Mitarbeiterkreis
	CString	Matr;		// Matricula
	CString	Peno;		// Personalnummer
	CString	Perc;		// K�rzel
	CString	Prfl;		// Protokollierungskennung
	CString	Rema; 		// Bemerkungen
	CString	Shnm;		// Kurzname
	CString	Sken;		// Schichtkennzeichen
	CString	Teld;		// Telefonnr. dienstlich
	CString	Telh;		// Telefonnr. Handy
	CString	Telp;		// Telefonnr. privat
	CString	Tohr;		// Soll an SAP-HR �bermittelt werden
	CString	Usec;		// Anwender (Ersteller)
	CString	Useu;		// Anwender (letzte �nderung)
	CString	Zutb;		// Zus�tzliche Urlaubstage Schwerbehinderung
	CString	Zutf;		// Zus�tzliche Urlaubstage Freistellungstage
	CString	Zutn; 		// Zus�tzliche Urlaubstage Nachtschicht
	CString	Zutw; 		// Zus�tzliche Urlaubstage Wechselschicht
	CString	Pnof;		// fiktive Personalnummer
	CString	Pdgl;		// pers�nliche durchschnittliche Ganztouenl�nge
	CString	Pdkl;		// pers�nliche durchschnittliche Kurztouenl�nge
	CString	Pmag;		// pers�nliche mtl Ganztouen anz.
	CString	Pmak;		// pers�nliche mtl Kurztouen anz.
	CString	Rels;		// Dienstplan/Mitarbeiter freigeben
	CString	Kind;		// Geschlecht
	CString	Kids;		// Schulpflichtige Kinder
	CString	Nati;		// Nationalit�t
	CString Gebu;		// Geburtstag
	CString Regi;		// Regelm�ssigkeitsindikator
	CString	Dodm; 		// Austrittsdatum
	CString Doem; 		// Eintrittsdatum
	CString Dobk;		// Wiedereintrittsdatum
	
	CString SorCode;	// Organisationseinheit
	CString SpfCode;	// Funktion
	CString SpeCode;	// Qualifikation
	CString	SwgCode;	// Arbeitsgruppe
	
};

/////////////////////////////////////////////////////////////////////////////
// MitarbeiterstammTableViewer

	  
class MitarbeiterstammTableViewer : public CViewer
{
// Constructions
public:
    MitarbeiterstammTableViewer(CCSPtrArray<STFDATA> *popData);
    ~MitarbeiterstammTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(STFDATA *prpMitarbeiterstamm);
	int CompareMitarbeiterstamm(MITARBEITERSTAMMTABLE_LINEDATA *prpMitarbeiterstamm1, MITARBEITERSTAMMTABLE_LINEDATA *prpMitarbeiterstamm2);
    void MakeLines();
	void MakeLine(STFDATA *prpMitarbeiterstamm);
	bool FindLine(long lpUrno, int &rilLineno);
	void PrepareFilter();
	void PrepareSorter();

// Operations
public:
	void DeleteAll();
	void CreateLine(MITARBEITERSTAMMTABLE_LINEDATA *prpMitarbeiterstamm);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(MITARBEITERSTAMMTABLE_LINEDATA *prpLine);
	void ProcessMitarbeiterstammChange(STFDATA *prpMitarbeiterstamm);
	void ProcessMitarbeiterstammDelete(STFDATA *prpMitarbeiterstamm);
	bool FindMitarbeiterstamm(char *prpMitarbeiterstammKeya, char *prpMitarbeiterstammKeyd, int& ilItem);
	bool CreateExcelFile(const CString& ropSeparator,const CString& ropListSeparator);

// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomMitarbeiterstammTable;
	CCSPtrArray<STFDATA> *pomData;

	CMapStringToPtr omCMapForSpf;		
	bool    bmUseAllSpf;

	CMapStringToPtr omCMapForSor;		
	bool    bmUseAllSor;

	CMapStringToPtr omCMapForSpe;		
	bool    bmUseAllSpe;

	CDWordArray omSortOrder;


// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<MITARBEITERSTAMMTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(int ilPage,CUIntArray& ropColsPerPage,MITARBEITERSTAMMTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader(int ilPage,CUIntArray& ropColsPerPage);
	CCSPrint *pomPrint;
	CString omTableName;
	CString	omFileName;
};

#endif //__MitarbeiterstammTableViewer_H__
