// NWHDlg.cpp : implementation file
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <NWHDlg.h>
#include <CedaCotData.h>
#include <AwDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNWHDlg dialog


CNWHDlg::CNWHDlg(NWHDATA *popNwh, CWnd* pParent /*=NULL*/) : CDialog(CNWHDlg::IDD, pParent)
{
	pomNwh = popNwh;
	//{{AFX_DATA_INIT(CNWHDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CNWHDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNWHDlg)
	DDX_Control(pDX, IDC_HR01, m_HR01);
	DDX_Control(pDX, IDC_HR02, m_HR02);
	DDX_Control(pDX, IDC_HR03, m_HR03);
	DDX_Control(pDX, IDC_HR04, m_HR04);
	DDX_Control(pDX, IDC_HR05, m_HR05);
	DDX_Control(pDX, IDC_HR06, m_HR06);
	DDX_Control(pDX, IDC_HR07, m_HR07);
	DDX_Control(pDX, IDC_HR08, m_HR08);
	DDX_Control(pDX, IDC_HR09, m_HR09);
	DDX_Control(pDX, IDC_HR10, m_HR10);
	DDX_Control(pDX, IDC_HR11, m_HR11);
	DDX_Control(pDX, IDC_HR12, m_HR12);
	DDX_Control(pDX, IDC_YEAR, m_YEAR);
	DDX_Control(pDX, IDC_REGI, m_REGI);
	DDX_Control(pDX, IDC_REMA, m_REMA);
	DDX_Control(pDX, IDOK,	   m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU,   m_USEU);
	DDX_Control(pDX, IDC_USEC,   m_USEC);
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CNWHDlg, CDialog)
	//{{AFX_MSG_MAP(CNWHDlg)
	ON_BN_CLICKED(IDC_B_CTRC_AW, OnBCtrcAw)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNWHDlg message handlers

BOOL CNWHDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_Caption = LoadStg(IDS_STRING763) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("NWHDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomNwh->Cdat.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomNwh->Cdat.Format("%H:%M"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomNwh->Lstu.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomNwh->Lstu.Format("%H:%M"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomNwh->Usec);
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomNwh->Useu);
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_REGI.SetTypeToString();
	m_REGI.SetTextLimit(1,8);
	m_REGI.SetBKColor(YELLOW);  
	m_REGI.SetTextErrColor(RED);
	/*CString olRegi;
	if(strcmp(pomNwh->Regi, "0") == 0)
		olRegi = CString("I");
	else if(strcmp(pomNwh->Regi, "1") == 0)
		olRegi = CString("R");
	else if (strcmp(pomNwh->Regi, "2") == 0)
		olRegi = CString("S");
	else
		olRegi = CString(" ");*/
	m_REGI.SetInitText(pomNwh->Ctrc);
	//------------------------------------
	m_REMA.SetTypeToString("X(60)",60,0);
	m_REMA.SetTextErrColor(RED);
	m_REMA.SetInitText(pomNwh->Rema);
	//------------------------------------
	m_YEAR.SetFormat("####");
	m_YEAR.SetTextLimit(4,4);
	m_YEAR.SetBKColor(YELLOW);  
	m_YEAR.SetTextErrColor(RED);
	m_YEAR.SetInitText(pomNwh->Year);
	//------------------------------------
	m_HR01.SetTypeToDouble(3, 2, 0,999.99);
	m_HR01.SetTextErrColor(RED);
	m_HR01.SetInitText(pomNwh->Hr01);
	//------------------------------------
	m_HR02.SetTypeToDouble(3, 2, 0,999.99);
	m_HR02.SetTextErrColor(RED);
	m_HR02.SetInitText(pomNwh->Hr02);
	//------------------------------------
	m_HR03.SetTypeToDouble(3, 2, 0,999.99);
	m_HR03.SetTextErrColor(RED);
	m_HR03.SetInitText(pomNwh->Hr03);
	//------------------------------------
	m_HR04.SetTypeToDouble(3, 2, 0,999.99);
	m_HR04.SetTextErrColor(RED);
	m_HR04.SetInitText(pomNwh->Hr04);
	//------------------------------------
	m_HR05.SetTypeToDouble(3, 2, 0,999.99);
	m_HR05.SetTextErrColor(RED);
	m_HR05.SetInitText(pomNwh->Hr05);
	//------------------------------------
	m_HR06.SetTypeToDouble(3, 2, 0,999.99);
	m_HR06.SetTextErrColor(RED);
	m_HR06.SetInitText(pomNwh->Hr06);
	//------------------------------------
	m_HR07.SetTypeToDouble(3, 2, 0,999.99);
	m_HR07.SetTextErrColor(RED);
	m_HR07.SetInitText(pomNwh->Hr07);
	//------------------------------------
	m_HR08.SetTypeToDouble(3, 2, 0,999.99);
	m_HR08.SetTextErrColor(RED);
	m_HR08.SetInitText(pomNwh->Hr08);
	//------------------------------------
	m_HR09.SetTypeToDouble(3, 2, 0,999.99);
	m_HR09.SetTextErrColor(RED);
	m_HR09.SetInitText(pomNwh->Hr09);
	//------------------------------------
	m_HR10.SetTypeToDouble(3, 2, 0,999.99);
	m_HR10.SetTextErrColor(RED);
	m_HR10.SetInitText(pomNwh->Hr10);
	//------------------------------------
	m_HR11.SetTypeToDouble(3, 2, 0,999.99);
	m_HR11.SetTextErrColor(RED);
	m_HR11.SetInitText(pomNwh->Hr11);
	//------------------------------------
	m_HR12.SetTypeToDouble(3, 2, 0,999.99);
	m_HR12.SetTextErrColor(RED);
	m_HR12.SetInitText(pomNwh->Hr12);
	//------------------------------------
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CNWHDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if (m_YEAR.GetStatus() == false)
	{
		ilStatus = false;
		if(m_YEAR.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING681) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING681) + ogNotFormat;
		}
	}
	if (m_REGI.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING1014) + ogNotFormat; //Contract code
	}
	if(m_REGI.GetWindowTextLength() == 0)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING1014) +  ogNoData; //Contract code
	}

	if(m_REMA.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING238) + ogNotFormat;
	}
	if(m_HR01.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING792) + ogNotFormat;
	}
	if(m_HR02.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING793) + ogNotFormat;
	}
	if(m_HR03.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING794) + ogNotFormat;
	}
	if(m_HR04.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING795) + ogNotFormat;
	}
	if(m_HR05.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING796) + ogNotFormat;
	}
	if(m_HR06.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING797) + ogNotFormat;
	}
	if(m_HR07.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING798) + ogNotFormat;
	}
	if(m_HR08.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING799) + ogNotFormat;
	}
	if(m_HR09.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING800) + ogNotFormat;
	}
	if(m_HR10.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING801) + ogNotFormat;
	}
	if(m_HR11.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING802) + ogNotFormat;
	}
	if(m_HR12.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING803) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////
	//Konsistenz-Check//

	CString olYear;
	CString olCtrc;

	if (ilStatus == true)
	{
		CCSPtrArray<NWHDATA> olNwhCPA;
		char clWhere[100];

		m_REGI.GetWindowText(olCtrc);
		m_YEAR.GetWindowText(olYear);

		if ((olYear.GetLength() == 4) && olCtrc.GetLength())
		{
			sprintf(clWhere,"WHERE CTRC='%s' AND YEAR='%s'", olCtrc, olYear);
			if (ogNwhData.ReadSpecialData(&olNwhCPA, clWhere, "URNO", false) == true)
			{
				if (olNwhCPA[0].Urno != pomNwh->Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING823) + LoadStg(IDS_EXIST);
				}
			}
		}
		else
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING763);
		}
	}
	///////////////////////////////////////////////////////////////////
	CWaitCursor olDummy;

	if (ilStatus == true)
	{
		CString	olTemp;
		int ilFind_P,ilFind_K;

		m_HR01.GetWindowText(olTemp);
		if(olTemp.IsEmpty()) olTemp = "0";
		ilFind_P = olTemp.ReverseFind('.');
		ilFind_K = olTemp.ReverseFind(',');
		if(ilFind_P == -1 && ilFind_K == -1)
		{
			olTemp += ".00";
		}
		else
		{
			if(ilFind_K != -1)
			{
				olTemp.SetAt(ilFind_K,'.');
				ilFind_P = ilFind_K;
			}
			if(olTemp.GetLength() == ilFind_P+1) olTemp += "00";
			else if(olTemp.GetLength() == (ilFind_P+2)) olTemp += "0";
		}
		sprintf(pomNwh->Hr01,"%s",olTemp);

		m_HR02.GetWindowText(olTemp);
		if(olTemp.IsEmpty()) olTemp = "0";
		ilFind_P = olTemp.ReverseFind('.');
		ilFind_K = olTemp.ReverseFind(',');
		if(ilFind_P == -1 && ilFind_K == -1)
		{
			olTemp += ".00";
		}
		else
		{
			if(ilFind_K != -1)
			{
				olTemp.SetAt(ilFind_K,'.');
				ilFind_P = ilFind_K;
			}
			if(olTemp.GetLength() == ilFind_P+1) olTemp += "00";
			else if(olTemp.GetLength() == (ilFind_P+2)) olTemp += "0";
		}
		sprintf(pomNwh->Hr02,"%s",olTemp);
		
		m_HR03.GetWindowText(olTemp);
		if(olTemp.IsEmpty()) olTemp = "0";
		ilFind_P = olTemp.ReverseFind('.');
		ilFind_K = olTemp.ReverseFind(',');
		if(ilFind_P == -1 && ilFind_K == -1)
		{
			olTemp += ".00";
		}
		else
		{
			if(ilFind_K != -1)
			{
				olTemp.SetAt(ilFind_K,'.');
				ilFind_P = ilFind_K;
			}
			if(olTemp.GetLength() == ilFind_P+1) olTemp += "00";
			else if(olTemp.GetLength() == (ilFind_P+2)) olTemp += "0";
		}
		sprintf(pomNwh->Hr03,"%s",olTemp);

		m_HR04.GetWindowText(olTemp);
		if(olTemp.IsEmpty()) olTemp = "0";
		ilFind_P = olTemp.ReverseFind('.');
		ilFind_K = olTemp.ReverseFind(',');
		if(ilFind_P == -1 && ilFind_K == -1)
		{
			olTemp += ".00";
		}
		else
		{
			if(ilFind_K != -1)
			{
				olTemp.SetAt(ilFind_K,'.');
				ilFind_P = ilFind_K;
			}
			if(olTemp.GetLength() == ilFind_P+1) olTemp += "00";
			else if(olTemp.GetLength() == (ilFind_P+2)) olTemp += "0";
		}
		sprintf(pomNwh->Hr04,"%s",olTemp);
		
		m_HR05.GetWindowText(olTemp);
		if(olTemp.IsEmpty()) olTemp = "0";
		ilFind_P = olTemp.ReverseFind('.');
		ilFind_K = olTemp.ReverseFind(',');
		if(ilFind_P == -1 && ilFind_K == -1)
		{
			olTemp += ".00";
		}
		else
		{
			if(ilFind_K != -1)
			{
				olTemp.SetAt(ilFind_K,'.');
				ilFind_P = ilFind_K;
			}
			if(olTemp.GetLength() == ilFind_P+1) olTemp += "00";
			else if(olTemp.GetLength() == (ilFind_P+2)) olTemp += "0";
		}
		sprintf(pomNwh->Hr05,"%s",olTemp);
		
		m_HR06.GetWindowText(olTemp);
		if(olTemp.IsEmpty()) olTemp = "0";
		ilFind_P = olTemp.ReverseFind('.');
		ilFind_K = olTemp.ReverseFind(',');
		if(ilFind_P == -1 && ilFind_K == -1)
		{
			olTemp += ".00";
		}
		else
		{
			if(ilFind_K != -1)
			{
				olTemp.SetAt(ilFind_K,'.');
				ilFind_P = ilFind_K;
			}
			if(olTemp.GetLength() == ilFind_P+1) olTemp += "00";
			else if(olTemp.GetLength() == (ilFind_P+2)) olTemp += "0";
		}
		sprintf(pomNwh->Hr06,"%s",olTemp);

		m_HR07.GetWindowText(olTemp);
		if(olTemp.IsEmpty()) olTemp = "0";
		ilFind_P = olTemp.ReverseFind('.');
		ilFind_K = olTemp.ReverseFind(',');
		if(ilFind_P == -1 && ilFind_K == -1)
		{
			olTemp += ".00";
		}
		else
		{
			if(ilFind_K != -1)
			{
				olTemp.SetAt(ilFind_K,'.');
				ilFind_P = ilFind_K;
			}
			if(olTemp.GetLength() == ilFind_P+1) olTemp += "00";
			else if(olTemp.GetLength() == (ilFind_P+2)) olTemp += "0";
		}
		sprintf(pomNwh->Hr07,"%s",olTemp);
		
		m_HR08.GetWindowText(olTemp);
		if(olTemp.IsEmpty()) olTemp = "0";
		ilFind_P = olTemp.ReverseFind('.');
		ilFind_K = olTemp.ReverseFind(',');
		if(ilFind_P == -1 && ilFind_K == -1)
		{
			olTemp += ".00";
		}
		else
		{
			if(ilFind_K != -1)
			{
				olTemp.SetAt(ilFind_K,'.');
				ilFind_P = ilFind_K;
			}
			if(olTemp.GetLength() == ilFind_P+1) olTemp += "00";
			else if(olTemp.GetLength() == (ilFind_P+2)) olTemp += "0";
		}
		sprintf(pomNwh->Hr08,"%s",olTemp);
		
		m_HR09.GetWindowText(olTemp);
		if(olTemp.IsEmpty()) olTemp = "0";
		ilFind_P = olTemp.ReverseFind('.');
		ilFind_K = olTemp.ReverseFind(',');
		if(ilFind_P == -1 && ilFind_K == -1)
		{
			olTemp += ".00";
		}
		else
		{
			if(ilFind_K != -1)
			{
				olTemp.SetAt(ilFind_K,'.');
				ilFind_P = ilFind_K;
			}
			if(olTemp.GetLength() == ilFind_P+1) olTemp += "00";
			else if(olTemp.GetLength() == (ilFind_P+2)) olTemp += "0";
		}
		sprintf(pomNwh->Hr09,"%s",olTemp);
		
		m_HR10.GetWindowText(olTemp);
		if(olTemp.IsEmpty()) olTemp = "0";
		ilFind_P = olTemp.ReverseFind('.');
		ilFind_K = olTemp.ReverseFind(',');
		if(ilFind_P == -1 && ilFind_K == -1)
		{
			olTemp += ".00";
		}
		else
		{
			if(ilFind_K != -1)
			{
				olTemp.SetAt(ilFind_K,'.');
				ilFind_P = ilFind_K;
			}
			if(olTemp.GetLength() == ilFind_P+1) olTemp += "00";
			else if(olTemp.GetLength() == (ilFind_P+2)) olTemp += "0";
		}
		sprintf(pomNwh->Hr10,"%s",olTemp);
		
		m_HR11.GetWindowText(olTemp);
		if(olTemp.IsEmpty()) olTemp = "0";
		ilFind_P = olTemp.ReverseFind('.');
		ilFind_K = olTemp.ReverseFind(',');
		if(ilFind_P == -1 && ilFind_K == -1)
		{
			olTemp += ".00";
		}
		else
		{
			if(ilFind_K != -1)
			{
				olTemp.SetAt(ilFind_K,'.');
				ilFind_P = ilFind_K;
			}
			if(olTemp.GetLength() == ilFind_P+1) olTemp += "00";
			else if(olTemp.GetLength() == (ilFind_P+2)) olTemp += "0";
		}
		sprintf(pomNwh->Hr11,"%s",olTemp);
		
		m_HR12.GetWindowText(olTemp);
		if(olTemp.IsEmpty()) olTemp = "0";
		ilFind_P = olTemp.ReverseFind('.');
		ilFind_K = olTemp.ReverseFind(',');
		if(ilFind_P == -1 && ilFind_K == -1)
		{
			olTemp += ".00";
		}
		else
		{
			if (ilFind_K != -1)
			{
				olTemp.SetAt(ilFind_K,'.');
				ilFind_P = ilFind_K;
			}
			if (olTemp.GetLength() == ilFind_P+1)
			{
				olTemp += "00";
			}
			else if(olTemp.GetLength() == (ilFind_P+2))
			{
				olTemp += "0";
			}
		}
		sprintf(pomNwh->Hr12,"%s",olTemp);

		//m_REGI.GetWindowText(pomNwh->Regi,3);
		//m_YEAR.GetWindowText(pomNwh->Year,6);
		strcpy(pomNwh->Ctrc, olCtrc);
		strcpy(pomNwh->Year, olYear);

		//wandelt Return in Blank//
		m_REMA.GetWindowText(olTemp);
		for (int i = 0; i > -1;)
		{
			i = olTemp.Find("\r\n");
			if (i != -1)
			{
				if(olTemp.GetLength() > (i+2))
					olTemp = olTemp.Left(i) + " " + olTemp.Mid(i+2);
				else
					olTemp = olTemp.Left(i);
			}
		}
		strcpy(pomNwh->Rema,olTemp);

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_YEAR.SetFocus();
	}	
}

void CNWHDlg::OnBCtrcAw() 
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<COTDATA> olList;

	char clWhere[100];
	sprintf(clWhere,"ORDER BY CTRC");
	if (ogCotData.ReadSpecialData(&olList, clWhere, "URNO,CTRC,CTRN", false) == true)
	{
		for (int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Ctrc));
			olCol2.Add(CString(olList[i].Ctrn));
		}
		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);
		if(pomDlg->DoModal() == IDOK)
		{
			m_REGI.SetWindowText(pomDlg->omReturnString);
			m_REGI.SetFocus();
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONSTOP|MB_OK));
	}
	olList.DeleteAll();	
}
