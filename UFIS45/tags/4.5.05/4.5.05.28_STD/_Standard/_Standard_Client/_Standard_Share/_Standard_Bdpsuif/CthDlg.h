#if !defined(AFX_CTHDLG_H__902B3013_ED2A_11D4_BFFE_00D0B7E2A4B5__INCLUDED_)
#define AFX_CTHDLG_H__902B3013_ED2A_11D4_BFFE_00D0B7E2A4B5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CthDlg.h : header file
//
#include <PrivList.h>
#include <CedaCthData.h>
#include <CCSEdit.h>

/////////////////////////////////////////////////////////////////////////////
// CCthDlg dialog

class CCthDlg : public CDialog
{
// Construction
public:
	CCthDlg(CTHDATA *popCth, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCthDlg)
	enum { IDD = IDD_CTHDLG };
	CButton	m_OK;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_CTHG;
	CCSEdit	m_YEAR;
	CCSEdit	m_REDU;
	CCSEdit	m_ABSF;
	CCSEdit	m_ABST;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CString m_Caption;
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCthDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

private:
	CTHDATA *pomCth;
// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCthDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CTHDLG_H__902B3013_ED2A_11D4_BFFE_00D0B7E2A4B5__INCLUDED_)
