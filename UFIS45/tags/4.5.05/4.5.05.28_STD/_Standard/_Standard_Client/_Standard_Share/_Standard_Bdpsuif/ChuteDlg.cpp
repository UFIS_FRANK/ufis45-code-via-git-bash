// ChuteDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <ChuteDlg.h>
#include <CedaGATData.h>
#include <GatPosNotAvailableDlg.h>
#include <PrivList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ChuteDlg dialog


ChuteDlg::ChuteDlg(CHUDATA *popCHU,CWnd* pParent /*=NULL*/) 
: CDialog(ChuteDlg::IDD, pParent)
{
	pomCHU = popCHU;
	pomTable = NULL;
	pomTable = new CCSTable;
	//{{AFX_DATA_INIT(ChuteDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

ChuteDlg::ChuteDlg(CHUDATA *popCHU,int ipDlg,CWnd* pParent /*=NULL*/) 
: CDialog(ipDlg, pParent)
{
	pomCHU = popCHU;
	pomTable = NULL;
	pomTable = new CCSTable;
	//{{AFX_DATA_INIT(ChuteDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

ChuteDlg::~ChuteDlg()
{
	omBlkPtrA.DeleteAll();
	omDeleteBlkPtrA.DeleteAll();
	delete pomTable;

}

void ChuteDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ChuteDlg)	
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_NOAVNEW, m_NOAVNEW);
	DDX_Control(pDX, IDC_NOAVDEL, m_NOAVDEL);
	DDX_Control(pDX, IDC_NOAV,	 m_NOAVFRAME);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_VAFR_D, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T, m_VATOT);
	DDX_Control(pDX, IDC_EDIT_NAME,	 m_CNAM);
	DDX_Control(pDX, IDC_EDIT_TERM,	 m_TERM);
	DDX_Control(pDX, IDC_EDIT_SORT,	 m_SORT);
	DDX_Control(pDX, IDC_EDIT_MAXF,	 m_MAXF);
	DDX_Control(pDX, IDC_RADIO_UTC,	 m_UTC);
	DDX_Control(pDX, IDC_RADIO_LOCAL,m_LOCAL);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ChuteDlg, CDialog)
	//{{AFX_MSG_MAP(ChuteDlg)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK,	OnTableLButtonDblclk)
	ON_BN_CLICKED(IDC_NOAVDEL, OnNoavDel)
	ON_BN_CLICKED(IDC_NOAVNEW, OnNoavNew)
	ON_BN_CLICKED(IDC_NOAVCOPY,OnNoavCopy)
	ON_BN_CLICKED(IDC_RADIO_UTC,	OnUTC)
	ON_BN_CLICKED(IDC_RADIO_LOCAL,	OnLocal)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ChuteDlg message handlers
//----------------------------------------------------------------------------------------

BOOL ChuteDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING1157) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);

	char clStat = ogPrivList.GetStat("CHUTEDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomCHU->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("CHUTEDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomCHU->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("CHUTEDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomCHU->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("CHUTEDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomCHU->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("CHUTEDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomCHU->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("CHUTEDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomCHU->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("CHUTEDLG.m_USEU"));
	//------------------------------------	
	m_CNAM.SetFormat("x|#x|#x|#x|#x|#");
	m_CNAM.SetTextLimit(1,5);
	m_CNAM.SetBKColor(YELLOW);
	m_CNAM.SetTextErrColor(RED);
	m_CNAM.SetInitText(pomCHU->Cnam);
	m_CNAM.SetSecState(ogPrivList.GetStat("CHUTEDLG.m_CNAM"));
	//------------------------------------
	m_TERM.SetFormat("x|#");
	m_TERM.SetTextLimit(0,1);
	m_TERM.SetTextErrColor(RED);
	m_TERM.SetInitText(pomCHU->Term);
	m_TERM.SetSecState(ogPrivList.GetStat("CHUTEDLG.m_TERM"));
	//------------------------------------
	m_SORT.SetFormat("x|#");
	m_SORT.SetTextLimit(0,1);
	m_SORT.SetTextErrColor(RED);
	m_SORT.SetInitText(pomCHU->Sort);
	m_SORT.SetSecState(ogPrivList.GetStat("CHUTEDLG.m_SORT"));
	//------------------------------------
	m_MAXF.SetFormat("#");
	m_MAXF.SetTextLimit(0,1);
	m_MAXF.SetTextErrColor(RED);
	m_MAXF.SetInitText(pomCHU->Maxf);
	m_MAXF.SetSecState(ogPrivList.GetStat("CHUTEDLG.m_MAXF"));
	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(pomCHU->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("CHUTEDLG.m_VAFR"));
	//------------------------------------
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetBKColor(YELLOW);
	m_VAFRT.SetInitText(pomCHU->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("CHUTEDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomCHU->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("CHUTEDLG.m_VATO"));
	//------------------------------------
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomCHU->Vato.Format("%H:%M"));
	m_VATOT.SetSecState(ogPrivList.GetStat("CHUTEDLG.m_VATO"));	
	//------------------------------------
	
	if (ogBasicData.UseLocalTimeAsDefault())
	{
		this->m_UTC.SetCheck(0);
		this->m_LOCAL.SetCheck(1);
		imLastSelection = 2;
	}
	else
	{
		this->m_UTC.SetCheck(1);
		this->m_LOCAL.SetCheck(0);
		imLastSelection = 1;
	}

	//------------------------------------
	CString olUrno;
	olUrno.Format("%d",pomCHU->Urno);
	MakeNoavTable("CHU", olUrno);

	return TRUE;
}

//----------------------------------------------------------------------------------------

void ChuteDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_CNAM.GetStatus() == false)
	{
		ilStatus = false;
		if(m_CNAM.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING288) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING288) + ogNotFormat;
		}
	}
	if(m_TERM.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING296) + ogNotFormat;
	}
	if(m_SORT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING1155) + ogNotFormat;
	}	
	if(m_MAXF.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING1156) + ogNotFormat;
	}
	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}


	///////////////////////////////////////////////////////////////////////////
	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olCnam;
		char clWhere[100];
		CCSPtrArray<CHUDATA> olChuCPA;

		m_CNAM.GetWindowText(olCnam);
		sprintf(clWhere,"WHERE CNAM='%s'",olCnam);
		if(ogCHUData.ReadSpecialData(&olChuCPA,clWhere,"URNO,CNAM",false) == true)
		{
			if(olChuCPA[0].Urno != pomCHU->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING288) + LoadStg(IDS_EXIST);
			}
		}
		olChuCPA.DeleteAll();
	}

	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_CNAM.GetWindowText(pomCHU->Cnam,6);
		m_TERM.GetWindowText(pomCHU->Term,2);
		m_SORT.GetWindowText(pomCHU->Sort,2);
		m_MAXF.GetWindowText(pomCHU->Maxf,2);
		pomCHU->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomCHU->Vato = DateHourStringToDate(olVatod,olVatot);
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_CNAM.SetFocus();
		m_CNAM.SetSel(0,-1);
	}
}

//----------------------------------------------------------------------------------------
void ChuteDlg::MakeNoavTable(CString opTabn, CString opBurn)
{
	CRect olRectBorder;
	m_NOAVFRAME.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);

    pomTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomTable->SetSelectMode(0);
	pomTable->SetShowSelection(TRUE);
	pomTable->ResetContent();

	CCSPtrArray <TABLE_HEADER_COLUMN> olHeaderDataArray;
	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	int ilPos = 1;
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 58; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING101),ilPos++,true,'|');
	olHeaderDataArray.NewAt(olHeaderDataArray.GetSize(), rlHeader);

	pomTable->SetHeaderFields(olHeaderDataArray);
	pomTable->SetDefaultSeparator();
	olHeaderDataArray.DeleteAll();

	char clWhere[100];
	CCSPtrArray<BLKDATA> olBlkCPA;
	sprintf(clWhere,"WHERE TABN='%s' AND BURN='%s'",opTabn,opBurn);
	ogBlkData.ReadSpecialData(&omBlkPtrA,clWhere,"",false);
	UpdateNoavTable();
}

//----------------------------------------------------------------------------------------
void ChuteDlg::UpdateNoavTable()
{
	int ilLines = omBlkPtrA.GetSize();

	pomTable->ResetContent();
	pomTable->SetDefaultSeparator();
	pomTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		CTime olNafr = omBlkPtrA[ilLineNo].Nafr;
		CTime olNato = omBlkPtrA[ilLineNo].Nato;
		if (this->imLastSelection == 2)	// local
		{
			ogBasicData.UtcToLocal(olNafr);
			ogBasicData.UtcToLocal(olNato);
		}
		rlColumnData.Text = olNafr.Format("%d.%m.%Y %H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = olNato.Format("%d.%m.%Y %H:%M");
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omBlkPtrA[ilLineNo].Days;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		char pclTmp[10]="";
		rlColumnData.Text = "";
		if(strlen(omBlkPtrA[ilLineNo].Tifr) > 0)
		{
			if (this->imLastSelection == 2)	// local
			{
				CString olText = omBlkPtrA[ilLineNo].Tifr;
				CTime olCurrent = CTime::GetCurrentTime();
				CTime olTifr(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);

				ogBasicData.UtcToLocal(olTifr);
				rlColumnData.Text = olTifr.Format("%H:%M");
			}
			else
			{
				sprintf(pclTmp, "%s:%s", CString(omBlkPtrA[ilLineNo].Tifr).Left(2), CString(omBlkPtrA[ilLineNo].Tifr).Right(2));
				rlColumnData.Text = pclTmp;
			}
		}
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = "";
		if(strlen(omBlkPtrA[ilLineNo].Tito) > 0)
		{
			if (this->imLastSelection == 2)	// local
			{
				CString olText = omBlkPtrA[ilLineNo].Tito;
				CTime olCurrent = CTime::GetCurrentTime();
				CTime olTito(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);

				ogBasicData.UtcToLocal(olTito);
				rlColumnData.Text = olTito.Format("%H:%M");
			}
			else
			{
				sprintf(pclTmp, "%s:%s", CString(omBlkPtrA[ilLineNo].Tito).Left(2), CString(omBlkPtrA[ilLineNo].Tito).Right(2));
				rlColumnData.Text = pclTmp;
			}
		}
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		(*omBlkPtrA[ilLineNo].Type == 'X') ? rlColumnData.Text = cgYes : rlColumnData.Text = cgNo;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omBlkPtrA[ilLineNo].Resn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomTable->DisplayTable();
}

//----------------------------------------------------------------------------------------

void ChuteDlg::ChangeNoavTable(BLKDATA *prpBlk, int ipLineNo)
{
	if(prpBlk != NULL)
	{
		TABLE_COLUMN rlColumn;
		CCSPtrArray <TABLE_COLUMN> olLine;

		rlColumn.Lineno = 0;
		rlColumn.VerticalSeparator = SEPA_NONE;
		rlColumn.SeparatorType = SEPA_NONE;
		rlColumn.BkColor = WHITE;
		rlColumn.Font = &ogCourier_Regular_10;
		rlColumn.Alignment = COLALIGN_LEFT;

		CTime olNafr = prpBlk->Nafr;
		CTime olNato = prpBlk->Nato;
		if (this->imLastSelection == 2)	// local
		{
			ogBasicData.UtcToLocal(olNafr);
			ogBasicData.UtcToLocal(olNato);
		}

		int ilColumnNo = 0;
		rlColumn.Text = olNafr.Format("%d.%m.%Y %H:%M");
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = olNato.Format("%d.%m.%Y %H:%M"); 
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpBlk->Days;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		char pclTmp[10]="";
		rlColumn.Text = "";
		if(strlen(prpBlk->Tifr) > 0)
		{
			if (this->imLastSelection == 2)	// local
			{
				CString olText = prpBlk->Tifr;
				CTime olCurrent = CTime::GetCurrentTime();
				CTime olTifr(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);

				ogBasicData.UtcToLocal(olTifr);
				rlColumn.Text = olTifr.Format("%H:%M");
			}
			else
			{
				sprintf(pclTmp, "%s:%s", CString(prpBlk->Tifr).Left(2), CString(prpBlk->Tifr).Right(2));
				rlColumn.Text = pclTmp;
			}
		}
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		rlColumn.Text = "";
		if(strlen(prpBlk->Tito) > 0)
		{
			if (this->imLastSelection == 2)	// local
			{
				CString olText = prpBlk->Tito;
				CTime olCurrent = CTime::GetCurrentTime();
				CTime olTito(olCurrent.GetYear(),olCurrent.GetMonth(),olCurrent.GetDay(),atoi(olText.Left(2)),atoi(olText.Right(2)),0);

				ogBasicData.UtcToLocal(olTito);
				rlColumn.Text = olTito.Format("%H:%M");
			}
			else
			{
				sprintf(pclTmp, "%s:%s", CString(prpBlk->Tito).Left(2), CString(prpBlk->Tito).Right(2));
				rlColumn.Text = pclTmp;
			}
		}
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		(*prpBlk->Type == 'X') ? rlColumn.Text = cgYes : rlColumn.Text = cgNo;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpBlk->Resn;
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);

		if(ipLineNo>-1)
		{
			pomTable->ChangeTextLine(ipLineNo, &olLine, (void *)prpBlk);
			pomTable->DisplayTable();
		}
		else
		{
			if(prpBlk != NULL)
			{
				pomTable->AddTextLine(olLine, (void *)prpBlk);
				pomTable->DisplayTable();
			}
		}
		olLine.DeleteAll();
	}
	else
	{
		if(ipLineNo>-1)
		{
			pomTable->DeleteTextLine(ipLineNo);
			pomTable->DisplayTable();
		}
	}
}

//----------------------------------------------------------------------------------------

LONG ChuteDlg::OnTableLButtonDblclk(UINT wParam, LONG lParam) 
{
	if(ogPrivList.GetStat("CHUTEDLG.m_NOAVCHA") == '1')
	{
		int ilLineNo = pomTable->GetCurSel();
		if (ilLineNo == -1)
		{
			MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONSTOP);
		}
		else
		{
			BLKDATA *prlBlk = &omBlkPtrA[ilLineNo];
			if (prlBlk != NULL)
			{
				BLKDATA rlBlk = *prlBlk;
				GatPosNotAvailableDlg *polDlg = new GatPosNotAvailableDlg(&rlBlk,this);
				m_CNAM.GetWindowText(polDlg->omName);
				if(polDlg->DoModal() == IDOK)
				{
					*prlBlk = rlBlk;
					if(prlBlk->IsChanged != DATA_NEW) prlBlk->IsChanged = DATA_CHANGED;
					ChangeNoavTable(prlBlk, ilLineNo);
				}
				delete polDlg;
			}
		}
	}
	return 0L;
}

//----------------------------------------------------------------------------------------

void ChuteDlg::OnNoavNew() 
{
	BLKDATA rlBlk;

	if(ogPrivList.GetStat("CHUTEDLG.m_NOAVNEW") == '1')
	{
		GatPosNotAvailableDlg *polDlg = new GatPosNotAvailableDlg(&rlBlk,this);
		m_CNAM.GetWindowText(polDlg->omName);
		if(polDlg->DoModal() == IDOK)
		{
			BLKDATA *prlBlk = new BLKDATA;
			*prlBlk = rlBlk;
			omBlkPtrA.Add(prlBlk);
			prlBlk->IsChanged = DATA_NEW;
			ChangeNoavTable(prlBlk, -1);
		}
		delete polDlg;
	}
}

//----------------------------------------------------------------------------------------

void ChuteDlg::OnNoavDel() 
{
	int ilLineNo = pomTable->GetCurSel();
	if (ilLineNo == -1)
	{
		MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONSTOP);
	}
	else
	{
		if(ogPrivList.GetStat("CHUTEDLG.m_NOAVDEL") == '1')
		{
			if (&omBlkPtrA[ilLineNo] != NULL)
			{
				BLKDATA *prlBlk = new BLKDATA;
				*prlBlk = omBlkPtrA[ilLineNo];
				prlBlk->IsChanged = DATA_DELETED;
				omDeleteBlkPtrA.Add(prlBlk);
				omBlkPtrA.DeleteAt(ilLineNo);
				ChangeNoavTable(NULL, ilLineNo);
			}
		}
	}
}

void ChuteDlg::OnNoavCopy() 
{
	int ilLineNo = pomTable->GetCurSel();
	if (ilLineNo == -1)
	{
		MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONSTOP);
	}
	else
	{
		if(ogPrivList.GetStat("CHUTEDLG.m_NOAVCOPY") == '1')
		{
			BLKDATA rlBlk;
			rlBlk = omBlkPtrA[ilLineNo];

			GatPosNotAvailableDlg *polDlg = new GatPosNotAvailableDlg(&rlBlk,this);			
			m_CNAM.GetWindowText(polDlg->omName);
			if (polDlg->DoModal() == IDOK)
			{	
				BLKDATA *prlBlk = new BLKDATA;
				*prlBlk = rlBlk;
			    omBlkPtrA.Add(prlBlk);
			    prlBlk->IsChanged = DATA_NEW;
			    ChangeNoavTable(prlBlk, -1);
			}
			delete polDlg;
		}
	}
}

//----------------------------------------------------------------------------------------
void ChuteDlg::OnUTC() 
{
	if (imLastSelection == 1)
		return;

	imLastSelection = 1;

	UpdateNoavTable();
}

//----------------------------------------------------------------------------------------
void ChuteDlg::OnLocal() 
{
	if (imLastSelection == 2)
		return;

	imLastSelection = 2;

	UpdateNoavTable();
}
