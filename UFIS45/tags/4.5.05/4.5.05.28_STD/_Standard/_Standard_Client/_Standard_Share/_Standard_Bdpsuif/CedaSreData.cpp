// CedaSreData.cpp
 
#include "stdafx.h"
#include "CedaSreData.h"
#include "resource.h"


void ProcessSreCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

static int CompareSurnAndTimes(const SREDATA **e1, const SREDATA **e2);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaSreData::CedaSreData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for SREDataStruct
	BEGIN_CEDARECINFO(SREDATA,SREDataRecInfo)
		FIELD_OLEDATE	(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Hopo,"HOPO")
		FIELD_OLEDATE	(Lstu,"LSTU")
		//FIELD_CHAR_TRIM	(Regc,"REGC")
		FIELD_CHAR_TRIM	(Regi,"REGI")
		//FIELD_CHAR_TRIM	(Regn,"REGN")
		FIELD_LONG		(Surn,"SURN")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_OLEDATE	(Vpfr,"VPFR")
		FIELD_OLEDATE	(Vpto,"VPTO")
	END_CEDARECINFO //(SREDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(SREDataRecInfo)/sizeof(SREDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SREDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"SRE");
	strcpy(pcmListOfFields,"CDAT,HOPO,LSTU,REGI,SURN,URNO,USEC,USEU,VPFR,VPTO");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaSreData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("URNO");
	ropFields.Add("SURN");
	ropFields.Add("VPFR");
	ropFields.Add("VPTO");

	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING464));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));

	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");
}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaSreData::Register(void)
{
	ogDdx.Register((void *)this,BC_SRE_CHANGE,	CString("SREDATA"), CString("Sre-changed"),	ProcessSreCf);
	ogDdx.Register((void *)this,BC_SRE_NEW,		CString("SREDATA"), CString("Sre-new"),		ProcessSreCf);
	ogDdx.Register((void *)this,BC_SRE_DELETE,	CString("SREDATA"), CString("Sre-deleted"),	ProcessSreCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaSreData::~CedaSreData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaSreData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omStaffUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaSreData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
	COleDateTime olCurrTime = COleDateTime::GetCurrentTime();
    omUrnoMap.RemoveAll();
    omStaffUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		// *** SHA 2001-01-15
		// *** Sort by validfrom (swissport!!! prf 82c)
		// *** sprintf nur in SRE ! rest hat gleiches where !
		char pclWhere2[256];
		sprintf(pclWhere2,"%s order by vpfr desc,regi",pspWhere);
		ilRc = CedaAction("RT", pclWhere2);


	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		SREDATA *prlSre = new SREDATA;
		if ((ilRc = GetFirstBufferRecord(prlSre)) == true)
		{
			omData.Add(prlSre);//Update omData
			omUrnoMap.SetAt((void *)prlSre->Urno,prlSre);
			bool blVptoIsOk = true;
			if(prlSre->Vpto.GetStatus() == COleDateTime::valid)
			{
				if(prlSre->Vpto < olCurrTime)
				{
					blVptoIsOk = false;
				}
			}

			if((prlSre->Vpfr < olCurrTime ) && blVptoIsOk)
			{
				omStaffUrnoMap.SetAt((void *)prlSre->Surn,prlSre);
			}

		}
		else
		{
			delete prlSre;
		}
	}

	omData.Sort(CompareSurnAndTimes);

    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaSreData::Insert(SREDATA *prpSre)
{
	prpSre->IsChanged = DATA_NEW;
	if(Save(prpSre) == false) return false; //Update Database
	InsertInternal(prpSre);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaSreData::InsertInternal(SREDATA *prpSre)
{
	COleDateTime olCurrTime = COleDateTime::GetCurrentTime();

	ogDdx.DataChanged((void *)this, SRE_NEW,(void *)prpSre ); //Update Viewer
	omData.Add(prpSre);//Update omData
	omUrnoMap.SetAt((void *)prpSre->Urno,prpSre);
	bool blVptoIsOk = true;
	if(prpSre->Vpto.GetStatus() == COleDateTime::valid)
	{
		if(prpSre->Vpto < olCurrTime)
		{
			blVptoIsOk = false;
		}
	}

	if((prpSre->Vpfr < olCurrTime ) && blVptoIsOk)
	{
		omStaffUrnoMap.SetAt((void *)prpSre->Surn,prpSre);
	}

	omData.Sort(CompareSurnAndTimes);

    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaSreData::Delete(long lpUrno)
{
	SREDATA *prlSre = GetSreByUrno(lpUrno);
	if (prlSre != NULL)
	{
		prlSre->IsChanged = DATA_DELETED;
		if(Save(prlSre) == false) return false; //Update Database
		DeleteInternal(prlSre);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaSreData::DeleteInternal(SREDATA *prpSre)
{
	COleDateTime olCurrTime = COleDateTime::GetCurrentTime();

	ogDdx.DataChanged((void *)this,SOR_DELETE,(void *)prpSre); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpSre->Urno);
	bool blVptoIsOk = true;
	if(prpSre->Vpto.GetStatus() == COleDateTime::valid)
	{
		if(prpSre->Vpto < olCurrTime)
		{
			blVptoIsOk = false;
		}
	}

	if((prpSre->Vpfr < olCurrTime ) && blVptoIsOk)
	{
		omStaffUrnoMap.RemoveKey((void *)prpSre->Surn);
	}

	int ilSreCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilSreCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpSre->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaSreData::Update(SREDATA *prpSre)
{
	if (GetSreByUrno(prpSre->Urno) != NULL)
	{
		if (prpSre->IsChanged == DATA_UNCHANGED)
		{
			prpSre->IsChanged = DATA_CHANGED;
		}
		if(Save(prpSre) == false) return false; //Update Database
		UpdateInternal(prpSre);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaSreData::UpdateInternal(SREDATA *prpSre)
{
	COleDateTime olCurrTime = COleDateTime::GetCurrentTime();

	SREDATA *prlSre = GetSreByUrno(prpSre->Urno);
	if (prlSre != NULL)
	{
		*prlSre = *prpSre; //Update omData
		bool blVptoIsOk = true;
		if(prlSre->Vpto.GetStatus() == COleDateTime::valid)
		{
			if(prlSre->Vpto < olCurrTime)
			{
				blVptoIsOk = false;
			}
		}

		if((prlSre->Vpfr < olCurrTime ) && blVptoIsOk)
		{
			omStaffUrnoMap.SetAt((void *)prlSre->Surn,prlSre);
		}

		ogDdx.DataChanged((void *)this,SRE_CHANGE,(void *)prlSre); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

SREDATA *CedaSreData::GetSreByUrno(long lpUrno)
{
	SREDATA  *prlSre;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlSre) == TRUE)
	{
		return prlSre;
	}
	return NULL;
}

//--GET-BY-SURN--------------------------------------------------------------------------------------------

SREDATA *CedaSreData::GetValidSreBySurn(long lpSurn)
{
	SREDATA  *prlSre;
	if (omStaffUrnoMap.Lookup((void *)lpSurn,(void *& )prlSre) == TRUE)
	{
		return prlSre;
	}
	return NULL;
}
//--GET-ALL BY-SURN--------------------------------------------------------------------------------------------

void CedaSreData::GetAllSreBySurn(long lpSurn,CCSPtrArray<SREDATA> &ropList)
{
	int ilSreCount = omData.GetSize();
	ropList.RemoveAll();
	for (int ilLc = 0; ilLc < ilSreCount; ilLc++)
	{
		if (omData[ilLc].Surn == lpSurn)
		{
			ropList.Add(&omData[ilLc]);
		}
	}
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaSreData::ReadSpecialData(CCSPtrArray<SREDATA> *popSre,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","SRE",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","SRE",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popSre != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			SREDATA *prpSre = new SREDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpSre,CString(pclFieldList))) == true)
			{
				popSre->Add(prpSre);
			}
			else
			{
				delete prpSre;
			}
		}
		if(popSre->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaSreData::Save(SREDATA *prpSre)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpSre->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpSre->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpSre);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpSre->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSre->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSre);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpSre->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSre->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessSreCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogSreData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaSreData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlSreData;
	prlSreData = (struct BcStruct *) vpDataPointer;
	SREDATA *prlSre;
	if(ipDDXType == BC_SRE_NEW)
	{
		prlSre = new SREDATA;
		GetRecordFromItemList(prlSre,prlSreData->Fields,prlSreData->Data);
		InsertInternal(prlSre);
	}
	if(ipDDXType == BC_SRE_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlSreData->Selection);
		prlSre = GetSreByUrno(llUrno);
		if(prlSre != NULL)
		{
			GetRecordFromItemList(prlSre,prlSreData->Fields,prlSreData->Data);
			UpdateInternal(prlSre);
		}
	}
	if(ipDDXType == BC_SRE_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlSreData->Selection);

		prlSre = GetSreByUrno(llUrno);
		if (prlSre != NULL)
		{
			DeleteInternal(prlSre);
		}
	}
}

//---------------------------------------------------------------------------------------------------------

CString CedaSreData::GetSreBySurnWithTime(long lpSurn, COleDateTime opDate)
{
	CString olOrg("");

	if(opDate.GetStatus() != COleDateTime::valid) return olOrg;

	SREDATA  *prlSre = 0;

	int ilCount = FindFirstOfSurn(lpSurn);
	if(ilCount == -1) return olOrg;	// lpSurn nicht gefunden

	for(; ilCount < omData.GetSize(); ilCount++)
	{
		prlSre = &omData[ilCount];

		if(prlSre->Surn > lpSurn) 
			break;		// omData ist ja nach Surn-Vpfr sortiert
		
		if(prlSre->Vpfr.GetStatus() != COleDateTime::valid) 
			continue;	// 1. Defekter Datensatz
		
		if(prlSre->Vpfr <= opDate)
		{
			switch(prlSre->Vpto.GetStatus())
			{
			default:
				// case COleDateTime::invalid:
				continue;	// gibts nicht
			case COleDateTime::valid:
				if(prlSre->Vpto < opDate) 
					continue;	// 3. Ende der G�ltigkeitszeit liegt vor dem opStart
				break;
			case COleDateTime::null:
				break;	// keine Endzeit, keine Beschr�nkung
			}
			olOrg = prlSre->Regi;	// gefunden!
			// wir pr�fen weiter, ob da noch passende Datens�tze vorliegen
		}
		else
			break;	// 2. Anfang der G�ltigkeitszeit liegt nach dem opDate, kein Datensatz mehr kann uns interessieren
	}
	return olOrg;
}

int CedaSreData::FindFirstOfSurn(long lpSurn)
{
	int ilSize = omData.GetSize();
	if(!ilSize) 
		return -1;
	
	int ilSearch;

	if(omData[ilSize-1].Surn != omData[0].Surn)
	{
		if(ilSize > 16)
		{
			// rekursives Suchen
			ilSearch = ilSize >> 1;
			int ilUp, ilDown;
			
			for(ilUp = ilSize-1, ilDown = 0;;)
			{
				if(lpSurn == omData[ilSearch].Surn)
				{
					if(!ilSearch || lpSurn != omData[ilSearch-1].Surn)
					{
						// gefunden!
						return ilSearch;
					}

					// lineares Suchen nach unten
					for(ilSearch--; ilSearch>=0;ilSearch--)
					{
						if(!ilSearch || lpSurn != omData[ilSearch-1].Surn)
						{
							// gefunden!
							return ilSearch;
						}
					}
				}
				
				if(ilUp-ilDown <= 1)
				{
					if(ilUp == ilDown)
					{
						// kein Surn in der Liste vorhanden!
						return -1;
					}

					if(lpSurn == omData[ilUp].Surn)
						return ilUp;
					else if(lpSurn == omData[ilDown].Surn)
						return ilDown;
					else 
						return -1;
				}

				if(lpSurn < omData[ilSearch].Surn)
				{
					ilUp = ilSearch;
					ilSearch = (ilUp + ilDown) >> 1;	// == (ilUp + ilDown) / 2
				}
				else //  if(lpSurn > omData[ilSearch].Surn)
				{
					ilDown = ilSearch;
					ilSearch = (ilUp + ilDown) >> 1;	// == (ilUp + ilDown) / 2
				}
			}
		}
		else
		{
			// lineares Suchen
			for(int ilSearch=0; ilSearch<ilSize; ilSearch++)
			{
				if(omData[ilSearch].Surn == lpSurn)
					return ilSearch;
			}
		}
	}
	else
	{
		return 0;	// alle gleich
	}
	
	return -1;	// kein gefunden!
}

static int CompareSurnAndTimes(const SREDATA **e1, const SREDATA **e2)
{
if((**e1).Surn>(**e2).Surn) 
		return 1;
	else if((**e1).Surn<(**e2).Surn) 
		return -1;

	if((**e1).Vpfr>(**e2).Vpfr) 
		return 1;
	else if((**e1).Vpfr<(**e2).Vpfr) 
		return -1;

	return 0;
}
