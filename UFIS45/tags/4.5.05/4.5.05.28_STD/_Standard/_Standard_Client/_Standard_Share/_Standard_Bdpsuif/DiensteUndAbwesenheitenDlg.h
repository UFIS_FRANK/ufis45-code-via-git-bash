#if !defined(AFX_DIENSTEUNDABWESENHEITENDLG_H__AB36A834_7540_11D1_B430_0000B45A33F5__INCLUDED_)
#define AFX_DIENSTEUNDABWESENHEITENDLG_H__AB36A834_7540_11D1_B430_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// DiensteUndAbwesenheitenDlg.h : header file
//
#include <CedaOdaData.h>
#include <CedaCotData.h>
#include <CedaOacData.h>
#include <CCSEdit.h>
#include <CCSTable.h>
#include <resrc1.h>		// main symbols#

/////////////////////////////////////////////////////////////////////////////
// DiensteUndAbwesenheitenDlg dialog

class DiensteUndAbwesenheitenDlg : public CDialog
{
// Construction
public:
	DiensteUndAbwesenheitenDlg(ODADATA *popOda, CCSPtrArray<OACDATA> *popOacData, CWnd* pParent = NULL, bool opUpdate = false);   // standard constructor
	~DiensteUndAbwesenheitenDlg();
	
	ODADATA *pomOda;
	CStatic *pomStatus;

// Dialog Data
	//{{AFX_DATA(DiensteUndAbwesenheitenDlg)
	enum { IDD = IDD_DIENSTEDLG };
	CCSEdit	m_Abto;
	CCSEdit	m_Abfr;
	CCSEdit	m_Blen;
	CCSEdit	m_SDAA;
	CButton	m_TBSD;
	CButton	m_WORK;
	CButton	m_R_Free;
	CButton	m_R_Compensation;
	CButton	m_R_Compensation_Night;
	CButton	m_R_Illness;
	CStatic	m_HELP_OAC;
	CButton	m_C_Upln;
	CButton	m_C_Tsap;
	CButton	m_OK;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_CTRC;
	CCSEdit	m_CTRC2;
	CCSEdit	m_DURA;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_REMA;
	CCSEdit	m_SDAC;
	CCSEdit	m_SDAE;
	CCSEdit	m_SDAK;
	CCSEdit	m_SDAN;
	CCSEdit	m_SDAS;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CString m_Caption;
	CButton	m_R_Training;
	CButton	m_R_Absence;
	CButton	m_R_Changeover;
	CButton	m_RGBC;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DiensteUndAbwesenheitenDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
private:
	bool bmUpdate;
	void OnBf_Button(int ipColY);
	LONG OnTableIPEditKillfocus( UINT wParam, LPARAM lParam);
	void InitTables();

protected:

	CCSTable *pomTable;
	OACDATA *pomOac;
	
	CCSPtrArray <COTDATA> *pomCotData;
	CCSPtrArray <OACDATA> *pomOacData;
	CString		omOldCode;
	bool		bmChangeAction;
	COLORREF		omRgbc;
	CBrush			omBrush;	

	// Generated message map functions
	//{{AFX_MSG(DiensteUndAbwesenheitenDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnBAwCot();
    afx_msg LONG OnKillfocus(UINT wParam, LONG lParam);
	afx_msg void OnB1();
	afx_msg void OnB2();
	afx_msg void OnB3();
	afx_msg void OnB4();
	afx_msg void OnRegularAbsence();
	afx_msg void OnRegularFree();
	afx_msg void OnShiftChangeoverDay();
	afx_msg void OnTraining();
	afx_msg void OnCompensation();
	afx_msg void OnCompensation_Night();
	afx_msg void OnIllness();
	afx_msg void OnRgbc();
	afx_msg HBRUSH OnCtlColor( CDC* pDC, CWnd* pWnd, UINT nCtlColor );
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIENSTEUNDABWESENHEITENDLG_H__AB36A834_7540_11D1_B430_0000B45A33F5__INCLUDED_)
