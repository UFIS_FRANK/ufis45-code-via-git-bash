// QualifikationenDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <QualifikationenDlg.h>
#include <PrivList.h>
#include <CheckReferenz.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// QualifikationenDlg dialog


QualifikationenDlg::QualifikationenDlg(PERDATA *popPer,CWnd* pParent) : CDialog(QualifikationenDlg::IDD, pParent)
{
	pomPer = popPer;

	//{{AFX_DATA_INIT(QualifikationenDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pomStatus = NULL;
}


void QualifikationenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(QualifikationenDlg)
	DDX_Control(pDX, IDOK,		 m_OK);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_GRUP,	 m_GRUP);
	DDX_Control(pDX, IDC_PRIO,	 m_PRIO);
	DDX_Control(pDX, IDC_PRMC,	 m_PRMC);
	DDX_Control(pDX, IDC_PRMN,	 m_PRMN);
	DDX_Control(pDX, IDC_REMA,	 m_REMA);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(QualifikationenDlg, CDialog)
	//{{AFX_MSG_MAP(QualifikationenDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// QualifikationenDlg message handlers
//----------------------------------------------------------------------------------------

BOOL QualifikationenDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if(m_Caption == LoadStg(IDS_STRING150))
		bmChangeAction = true;
	else
		bmChangeAction = false;

	m_Caption = LoadStg(IDS_STRING186) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("QUALIFIKATIONENDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomPer->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("QUALIFIKATIONENDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomPer->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("QUALIFIKATIONENDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomPer->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("QUALIFIKATIONENDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomPer->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("QUALIFIKATIONENDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomPer->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("QUALIFIKATIONENDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomPer->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("QUALIFIKATIONENDLG.m_USEU"));
	//------------------------------------
	m_GRUP.SetBKColor(SILVER);
	//m_GRUP.SetInitText(pomGAT->);
	m_GRUP.SetSecState(ogPrivList.GetStat("QUALIFIKATIONENDLG.m_GRUP"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_PRIO.SetTypeToString("#(3)", 3, 0);
//	m_PRIO.SetBKColor(YELLOW);  
//	m_PRIO.SetTextErrColor(RED);
	m_PRIO.SetInitText(pomPer->Prio);
	m_PRIO.SetSecState(ogPrivList.GetStat("QUALIFIKATIONENDLG.m_PRIO"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_PRMC.SetTypeToString("X(5)", 5, 1);
	m_PRMC.SetBKColor(YELLOW);  
	m_PRMC.SetTextErrColor(RED);
	m_PRMC.SetInitText(pomPer->Prmc);
	this->omOldCode = pomPer->Prmc;

	m_PRMC.SetSecState(ogPrivList.GetStat("QUALIFIKATIONENDLG.m_PRMC"));
	//------------------------------------
	m_PRMN.SetTypeToString("X(40)",40,1);
	m_PRMN.SetBKColor(YELLOW);  
	m_PRMN.SetTextErrColor(RED);
	m_PRMN.SetInitText(pomPer->Prmn);
	m_PRMN.SetSecState(ogPrivList.GetStat("QUALIFIKATIONENDLG.m_PRMN"));
	//------------------------------------
	m_REMA.SetTypeToString("X(60)",60,0);
	m_REMA.SetTextErrColor(RED);
	m_REMA.SetInitText(pomPer->Rema);
	m_REMA.SetSecState(ogPrivList.GetStat("QUALIFIKATIONENDLG.m_REMA"));
	//------------------------------------
	return TRUE; 
}

//----------------------------------------------------------------------------------------

void QualifikationenDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText,olNewCode;
	bool ilStatus = true;
	if(m_PRIO.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING637) + ogNotFormat;
	}

	if(m_PRMC.GetStatus() == false)
	{
		ilStatus = false;
		if(m_PRMC.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_PRMN.GetStatus() == false)
	{
		ilStatus = false;
		if(m_PRMN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING229) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING229) + ogNotFormat;
		}
	}
	if(m_REMA.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING238) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olPrmc;
		char clWhere[100];

		CCSPtrArray<PERDATA> olPerCPA;
		m_PRMC.GetWindowText(olPrmc);
		olNewCode = olPrmc;
		sprintf(clWhere,"WHERE PRMC='%s'",olPrmc);
		if(ogPerData.ReadSpecialData(&olPerCPA,clWhere,"URNO,PRMC",false) == true)
		{
			if(olPerCPA[0].Urno != pomPer->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
			}
		}
		olPerCPA.DeleteAll();
	}
	
	///////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		bool blChangeCode = true;
		if(olNewCode != omOldCode && bmChangeAction)
		{
			// Referenz Check
			CheckReferenz olCheckReferenz;
			if(pomStatus != NULL) pomStatus->SetWindowText(LoadStg(IDS_STRING120));
			int ilCount = olCheckReferenz.Check(_PER, pomPer->Urno, omOldCode);
			if(pomStatus != NULL) pomStatus->SetWindowText(LoadStg(ST_OK));
			CString olTxt;
			if(ilCount>0)
			{
				blChangeCode = false;
				if (!ogBasicData.BackDoorEnabled())
				{
					olTxt.Format("%d %s",ilCount, LoadStg(IDS_STRING119));
					MessageBox(olTxt,LoadStg(IDS_STRING145),MB_ICONERROR);
				}
				else
				{
					olTxt.Format("%d %s",ilCount, LoadStg(IDS_STRING983));
					if(IDYES == MessageBox(olTxt,LoadStg(IDS_STRING149),(MB_ICONSTOP | MB_YESNOCANCEL | MB_DEFBUTTON2)))
					{
						blChangeCode = true;
					}
				}
			}
			// END Referenz Check
		}

		if(blChangeCode)
		{
			m_PRMC.GetWindowText(pomPer->Prmc,6);
			m_PRMN.GetWindowText(pomPer->Prmn,41);
			m_PRIO.GetWindowText(pomPer->Prio,4);

			//wandelt Return in Blank//
			CString olTemp;
			m_REMA.GetWindowText(olTemp);
			for(int i=0;i>-1;)
			{
				i = olTemp.Find("\r\n");
				if(i != -1)
				{
					if(olTemp.GetLength() > (i+2))
						olTemp = olTemp.Left(i) + " " + olTemp.Mid(i+2);
					else
						olTemp = olTemp.Left(i);
				}
			}
			strcpy(pomPer->Rema,olTemp);
			////////////////////////////

			CDialog::OnOK();
		}
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_PRMC.SetFocus();
		m_PRMC.SetSel(0,-1);
  }
}
//----------------------------------------------------------------------------------------
