// CedaSrcData.cpp
 
#include <stdafx.h>
#include <CedaSrcData.h>
#include <resource.h>


void ProcessSrcCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaSrcData::CedaSrcData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for SRCDataStruct
	BEGIN_CEDARECINFO(SRCDATA,SRCDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Srcc,"SRCC")
		FIELD_CHAR_TRIM	(Srcd,"SRCD")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")

	END_CEDARECINFO //(SRCDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(SRCDataRecInfo)/sizeof(SRCDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SRCDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"SRC");
	strcpy(pcmListOfFields,"CDAT,SRCC,SRCD,LSTU,URNO,USEC,USEU");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaSrcData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("SRCC");
	ropFields.Add("SRCD");
	ropFields.Add("LSTU");
	ropFields.Add("USEC");
	ropFields.Add("USEU");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING826));
	ropDesription.Add(LoadStg(IDS_STRING827));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));

	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaSrcData::Register(void)
{
	ogDdx.Register((void *)this,BC_SRC_CHANGE,	CString("SRCDATA"), CString("Src-changed"),	ProcessSrcCf);
	ogDdx.Register((void *)this,BC_SRC_NEW,		CString("SRCDATA"), CString("Src-new"),		ProcessSrcCf);
	ogDdx.Register((void *)this,BC_SRC_DELETE,	CString("SRCDATA"), CString("Src-deleted"),	ProcessSrcCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaSrcData::~CedaSrcData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaSrcData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaSrcData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		SRCDATA *prlSrc = new SRCDATA;
		if ((ilRc = GetFirstBufferRecord(prlSrc)) == true)
		{
			omData.Add(prlSrc);//Update omData
			omUrnoMap.SetAt((void *)prlSrc->Urno,prlSrc);
		}
		else
		{
			delete prlSrc;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaSrcData::Insert(SRCDATA *prpSrc)
{
	prpSrc->IsChanged = DATA_NEW;
	if(Save(prpSrc) == false) return false; //Update Database
	InsertInternal(prpSrc);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaSrcData::InsertInternal(SRCDATA *prpSrc)
{
	ogDdx.DataChanged((void *)this, SRC_NEW,(void *)prpSrc ); //Update Viewer
	omData.Add(prpSrc);//Update omData
	omUrnoMap.SetAt((void *)prpSrc->Urno,prpSrc);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaSrcData::Delete(long lpUrno)
{
	SRCDATA *prlSrc = GetSrcByUrno(lpUrno);
	if (prlSrc != NULL)
	{
		prlSrc->IsChanged = DATA_DELETED;
		if(Save(prlSrc) == false) return false; //Update Database
		DeleteInternal(prlSrc);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaSrcData::DeleteInternal(SRCDATA *prpSrc)
{
	ogDdx.DataChanged((void *)this,SRC_DELETE,(void *)prpSrc); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpSrc->Urno);
	int ilSrcCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilSrcCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpSrc->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaSrcData::Update(SRCDATA *prpSrc)
{
	if (GetSrcByUrno(prpSrc->Urno) != NULL)
	{
		if (prpSrc->IsChanged == DATA_UNCHANGED)
		{
			prpSrc->IsChanged = DATA_CHANGED;
		}
		if(Save(prpSrc) == false) return false; //Update Database
		UpdateInternal(prpSrc);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaSrcData::UpdateInternal(SRCDATA *prpSrc)
{
	SRCDATA *prlSrc = GetSrcByUrno(prpSrc->Urno);
	if (prlSrc != NULL)
	{
		*prlSrc = *prpSrc; //Update omData
		ogDdx.DataChanged((void *)this,SRC_CHANGE,(void *)prlSrc); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

SRCDATA *CedaSrcData::GetSrcByUrno(long lpUrno)
{
	SRCDATA  *prlSrc;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlSrc) == TRUE)
	{
		return prlSrc;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaSrcData::ReadSpecialData(CCSPtrArray<SRCDATA> *popSrc,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","SRC",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","SRC",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popSrc != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			SRCDATA *prpSrc = new SRCDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpSrc,CString(pclFieldList))) == true)
			{
				popSrc->Add(prpSrc);
			}
			else
			{
				delete prpSrc;
			}
		}
		if(popSrc->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaSrcData::Save(SRCDATA *prpSrc)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpSrc->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpSrc->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpSrc);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpSrc->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSrc->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSrc);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpSrc->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSrc->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessSrcCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogSrcData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaSrcData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlSrcData;
	prlSrcData = (struct BcStruct *) vpDataPointer;
	SRCDATA *prlSrc;
	if(ipDDXType == BC_SRC_NEW)
	{
		prlSrc = new SRCDATA;
		GetRecordFromItemList(prlSrc,prlSrcData->Fields,prlSrcData->Data);
		if(ValidateSrcBcData(prlSrc->Urno)) //Prf: 8795
		{
			InsertInternal(prlSrc);
		}
		else
		{
			delete prlSrc;
		}
	}
	if(ipDDXType == BC_SRC_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlSrcData->Selection);
		prlSrc = GetSrcByUrno(llUrno);
		if(prlSrc != NULL)
		{
			GetRecordFromItemList(prlSrc,prlSrcData->Fields,prlSrcData->Data);
			if(ValidateSrcBcData(prlSrc->Urno)) //Prf: 8795
			{
				UpdateInternal(prlSrc);
			}
			else
			{
				DeleteInternal(prlSrc);
			}
		}
	}
	if(ipDDXType == BC_SRC_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlSrcData->Selection);

		prlSrc = GetSrcByUrno(llUrno);
		if (prlSrc != NULL)
		{
			DeleteInternal(prlSrc);
		}
	}
}

//Prf: 8795
//--ValidateSrcBcData--------------------------------------------------------------------------------------

bool CedaSrcData::ValidateSrcBcData(const long& lrpUrno)
{
	bool blValidateSrcBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<SRCDATA> olSrcs;
		if(!ReadSpecialData(&olSrcs,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateSrcBcData = false;
		}
	}
	return blValidateSrcBcData;
}

//---------------------------------------------------------------------------------------------------------
