// AircraftDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <AircraftDlg.h>
#include <CCSEdit.h>
#include <CCSTable.h>
#include <CCSComboBox.h>
#include <CedaEntData.h>
#include <CedaAfmData.h>
#include <CedaAltData.h>
#include <AwDlg.h>
#include <PrivList.h>
#include <util.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// AircraftDlg dialog
//----------------------------------------------------------------------------------------

AircraftDlg::AircraftDlg(ACTDATA *popACT,CWnd* pParent /*=NULL*/) : CDialog(AircraftDlg::IDD, pParent)
{
	pomACT = popACT;

	//{{AFX_DATA_INIT(AircraftDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void AircraftDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(AircraftDlg)
	DDX_Control(pDX, ID_BLACK_LIST, m_BLACK_LIST);
	DDX_Control(pDX, IDC_MIGT, m_MING);
	DDX_Control(pDX, IDCANCEL, m_CANCEL);
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_NADS,	 m_NADS);
	DDX_Control(pDX, IDC_ACFN,	 m_ACFN);
	DDX_Control(pDX, IDC_ACHE,	 m_ACHE);
	DDX_Control(pDX, IDC_ACLE,	 m_ACLE);
	DDX_Control(pDX, IDC_ACWS,	 m_ACWS);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_ENNO,	 m_ENNO);
	DDX_Control(pDX, IDC_ENTY,	 m_ENTY);
	DDX_Control(pDX, IDC_GRUP,	 m_GRUP);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_SEAT,	 m_SEAT);
	DDX_Control(pDX, IDC_SEAF,	 m_SEAF);
	DDX_Control(pDX, IDC_SEAB,	 m_SEAB);
	DDX_Control(pDX, IDC_SEAE,	 m_SEAE);
	DDX_Control(pDX, IDC_ACT3,	 m_ACT3);
	DDX_Control(pDX, IDC_ACT5,	 m_ACT5);
	DDX_Control(pDX, IDC_VAFR_D2, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T1, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D1, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T1, m_VATOT);
	DDX_Control(pDX, IDC_ACTI,	 m_ACTI);
	DDX_Control(pDX, IDC_AFMC,	 m_AFMC);
	DDX_Control(pDX, IDC_ALTC,	 m_ALTC);
	DDX_Control(pDX, IDC_MODC,	 m_MODC);
	DDX_Control(pDX, IDC_ACBT,	 m_ACBT);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(AircraftDlg, CDialog)
	//{{AFX_MSG_MAP(AircraftDlg)
	ON_BN_CLICKED(IDC_BSELENGINE, OnBSelEngine)
	ON_BN_CLICKED(IDC_BSELALT, OnBSelAlt3)
	ON_BN_CLICKED(IDC_BSELAFM, OnBSelAfm)
	ON_BN_CLICKED(ID_BLACK_LIST, OnBlackList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// AircraftDlg message handlers
//----------------------------------------------------------------------------------------

BOOL AircraftDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_Caption = LoadStg(IDS_STRING176) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	char clStat = ogPrivList.GetStat("AIRCRAFTDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);			
	m_CDATD.SetInitText(pomACT->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomACT->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomACT->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomACT->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomACT->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomACT->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_USEU"));
	//------------------------------------
	m_GRUP.SetBKColor(SILVER);
	//m_GRUP.SetInitText(???);
	m_GRUP.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_GRUP"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	if (ogBasicData.UseUniqueActPair())
	{
		m_ACT3.SetFormat("X(3)");
		m_ACT3.SetTextLimit(0,3,true);
		m_ACT3.SetBKColor(YELLOW);
		m_ACT3.SetTextErrColor(RED);
		m_ACT3.SetInitText(pomACT->Act3);
		m_ACT3.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_ACT3"));
		//------------------------------------
		m_ACT5.SetFormat("X(5)");
		m_ACT5.SetTextLimit(0,5,true);
		m_ACT5.SetBKColor(YELLOW);
		m_ACT5.SetTextErrColor(RED);
		m_ACT5.SetInitText(pomACT->Act5);
		m_ACT5.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_ACT5"));
	}
	else
	{
		m_ACT3.SetFormat("x|#x|#x|#");
		m_ACT3.SetTextLimit(-1,3,true);
		m_ACT3.SetTextErrColor(RED);
		m_ACT3.SetInitText(pomACT->Act3);
		m_ACT3.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_ACT3"));
		//------------------------------------
		m_ACT5.SetFormat("x|#x|#x|#x|#x|#");
		m_ACT5.SetTextLimit(3,5);
		m_ACT5.SetBKColor(YELLOW);
		m_ACT5.SetTextErrColor(RED);
		m_ACT5.SetInitText(pomACT->Act5);
		m_ACT5.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_ACT5"));
	}
	//------------------------------------
	m_ACTI.SetFormat("x|#x|#x|#x|#x|#");
	m_ACTI.SetTextLimit(3,5,true);
	m_ACTI.SetTextErrColor(RED);
	m_ACTI.SetInitText(pomACT->Acti);
	m_ACTI.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_ACTI"));
	//------------------------------------
	CString olFormat;
	olFormat.Format("X(%u)",ogBasicData.AircraftTypeDescriptionLength());
	m_ACFN.SetTypeToString(olFormat,ogBasicData.AircraftTypeDescriptionLength(),1);
	m_ACFN.SetBKColor(YELLOW);
	m_ACFN.SetTextErrColor(RED);
	m_ACFN.SetInitText(pomACT->Acfn);
	m_ACFN.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_ACFN"));
	//------------------------------------
// eventuell raus !!!! rdr	
	m_ACBT.SetTypeToString("X(1)",1,0);
//	m_ACBT.SetBKColor(YELLOW);
	m_ACBT.SetTextErrColor(RED);
	m_ACBT.SetInitText(pomACT->Acbt);
	m_ACBT.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_ACBT"));
	
	//------------------------------------
	m_ACWS.SetTypeToDouble(2,2,00.01,99.99);
	//m_ACWS.SetFormat("[##'.'##]");
//	m_ACWS.SetBKColor(YELLOW);
	m_ACWS.SetTextErrColor(RED);
	m_ACWS.SetInitText(pomACT->Acws);
//	m_ACWS.SetInitText(pomACT->Acws);
	m_ACWS.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_ACWS"));
	//------------------------------------
	m_ACLE.SetTypeToDouble(2,2,00.01,99.99);
	//m_ACLE.SetFormat("[##'.'##]");
//	m_ACLE.SetBKColor(YELLOW);
	m_ACLE.SetTextErrColor(RED);
	m_ACLE.SetInitText(pomACT->Acle);
//	m_ACLE.SetInitText(pomACT->Acle);
	m_ACLE.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_ACLE"));
	//------------------------------------
	m_ACHE.SetTypeToDouble(2,2,00.01,99.99);
	//m_ACHE.SetFormat("[##'.'##]");
	m_ACHE.SetBKColor(WHITE);
	m_ACHE.SetTextErrColor(RED);
	m_ACHE.SetInitText(pomACT->Ache);
//	m_ACHE.SetInitText(pomACT->Ache);
	m_ACHE.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_ACHE"));
	//------------------------------------
	m_SEAT.SetTypeToInt(0,999);
 	m_SEAT.SetBKColor(WHITE);
	m_SEAT.SetTextErrColor(RED);
	m_SEAT.SetInitText(pomACT->Seat);
	m_SEAT.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_SEAT"));
	//------------------------------------
	m_SEAF.SetTypeToInt(0,999);
	m_SEAF.SetBKColor(WHITE);
	m_SEAF.SetTextErrColor(RED);
	m_SEAF.SetInitText(pomACT->Seaf);
	m_SEAF.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_SEAF"));
	//------------------------------------
	m_SEAB.SetTypeToInt(0,999);
	m_SEAB.SetBKColor(WHITE);
	m_SEAB.SetTextErrColor(RED);
	m_SEAB.SetInitText(pomACT->Seab);
	m_SEAB.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_SEAB"));
	//------------------------------------
	m_SEAE.SetTypeToInt(0,999);
	m_SEAE.SetBKColor(WHITE);
	m_SEAE.SetTextErrColor(RED);
	m_SEAE.SetInitText(pomACT->Seae);
	m_SEAE.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_SEAE"));
	//------------------------------------
	if (pomACT->Nads[0] == 'X') m_NADS.SetCheck(1);
	clStat = ogPrivList.GetStat("AIRCRAFTDLG.m_NADS");
	SetWndStatAll(clStat,m_NADS);
	//------------------------------------
	m_ENTY.SetTypeToString("X(5)",5,0);//*** 07.09.99 SHA *** 3->5
	m_ENTY.SetBKColor(WHITE);
	m_ENTY.SetTextErrColor(RED);
	m_ENTY.SetInitText(pomACT->Enty);
	m_ENTY.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_ENTY"));
	//------------------------------------
	m_ENNO.SetTypeToInt(0,9);
	m_ENNO.SetBKColor(WHITE);
	m_ENNO.SetTextErrColor(RED);
	m_ENNO.SetInitText(pomACT->Enno);
	m_ENNO.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_ENNO"));
	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(pomACT->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetBKColor(YELLOW);
	m_VAFRT.SetInitText(pomACT->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomACT->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomACT->Vato.Format("%H:%M"));
	m_VATOT.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_VATO"));
	//------------------------------------
	m_AFMC.SetTypeToString("X(3)",3,0);
	m_AFMC.SetBKColor(WHITE);
	m_AFMC.SetTextErrColor(RED);
	m_AFMC.SetInitText(pomACT->Afmc);
	m_AFMC.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_AFMC"));
	//------------------------------------
	m_ALTC.SetTypeToString("X(3)",3,0);
	m_ALTC.SetBKColor(WHITE);
	m_ALTC.SetTextErrColor(RED);
	m_ALTC.SetInitText(pomACT->Altc);
	m_ALTC.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_ALTC"));
	//------------------------------------
	m_MODC.SetTypeToString("X(3)",3,0);
	m_MODC.SetBKColor(WHITE);
	m_MODC.SetTextErrColor(RED);
	m_MODC.SetInitText(pomACT->Modc);
	m_MODC.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_MODC"));
	//------------------------------------

	if (ogBasicData.GetCustomerId() == "ADR")
	{
		m_MING.SetTypeToString("#(3)",3,0);
		m_MING.SetBKColor(WHITE);
		m_MING.SetTextErrColor(RED);
		m_MING.SetInitText(pomACT->Ming);
	}
	else
	{
		this->GetDlgItem(IDC_MIGT_LABEL)->ShowWindow(SW_HIDE);
		this->m_MING.EnableWindow(FALSE);
		this->m_MING.ShowWindow(SW_HIDE);
	}

	//------------------------------------
	m_BLACK_LIST.SetSecState(ogPrivList.GetStat("AIRCRAFTDLG.m_BLACK_LIST"));
	mdBlackListProcId=NULL;
	
	
	return TRUE;  
}

//----------------------------------------------------------------------------------------

void AircraftDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_ACT3.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING240) + ogNotFormat;
	}

	if(m_ACT5.GetStatus() == false)
	{
		ilStatus = false;
		if(m_ACT5.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING241) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING241) + ogNotFormat;
		}
	}

	if (ogBasicData.UseUniqueActPair())
	{
		if (m_ACT3.GetWindowTextLength() == 0 && m_ACT5.GetWindowTextLength() == 0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING52);
		}
	}

	if(m_ACTI.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING242) + ogNotFormat;
	}

	if(m_ACFN.GetStatus() == false)
	{
		ilStatus = false;
		if(m_ACFN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING229) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING229) + ogNotFormat;
		} 
	}

	if(m_SEAT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING243) + ogNotFormat;
	}
	if(m_SEAF.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING243) + ogNotFormat;
	}
	if(m_SEAB.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING243) + ogNotFormat;
	}
	if(m_SEAE.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING243) + ogNotFormat;
	}
	if(m_ENTY.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING248) + ogNotFormat;
	}
	if(m_ENNO.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText +=  LoadStg(IDS_STRING249) + ogNotFormat;
	}
	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}
	if(m_AFMC.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING243) + ogNotFormat;
	}
	if(m_ALTC.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING243) + ogNotFormat;
	}
	if(m_MODC.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING243) + ogNotFormat;
	}

	/////////////////////////////////////////////////////////////////////////
	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		if (ogBasicData.UseUniqueActPair())
		{
			CString olAct3;
			CString olAct5;

			char clWhere[100];
			CCSPtrArray<ACTDATA> olActCPA;
			m_ACT3.GetWindowText(olAct3);
			m_ACT5.GetWindowText(olAct5);

			if (olAct3.GetLength() > 0)
			{
				if (olAct5.GetLength() == 0)
				{
					olAct5 = " ";
				}
			}
			else if (olAct5.GetLength() > 0)
			{
				olAct3 = " ";
			}

			sprintf(clWhere,"WHERE ACT3 = '%s' AND ACT5 = '%s'",olAct3,olAct5);
			if (ogACTData.ReadSpecialData(&olActCPA,clWhere,"URNO,ACT3,ACT5",false) == true)
			{
				if (olActCPA.GetSize() != 1 || olActCPA[0].Urno != pomACT->Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING240) + LoadStg(IDS_EXIST);
				}
			}

			olActCPA.DeleteAll();
		}
		else
		{
			CString olAct3;
			char clWhere[100];
			CCSPtrArray<ACTDATA> olActCPA;
			m_ACT3.GetWindowText(olAct3);
			if (olAct3.GetLength() > 0)
			{
				sprintf(clWhere,"WHERE ACT3='%s'",olAct3);
				if(ogACTData.ReadSpecialData(&olActCPA,clWhere,"URNO,ACT3",false) == true)
				{
					if(olActCPA[0].Urno != pomACT->Urno)
					{
						ilStatus = false;
						olErrorText += LoadStg(IDS_STRING240) + LoadStg(IDS_EXIST);
					}
				}
				olActCPA.DeleteAll();
			}
		}
	}

/*---------------------------
	if(ilStatus == true)
	{
		CString olEnt;
		char clWhere[100];
		CCSPtrArray<ENTDATA> olEntCPA;
		m_ENTY.GetWindowText(olEnt);
		sprintf(clWhere,"WHERE ENTC='%s'",olEnt);
		if(ogENTData.ReadSpecialData(&olEntCPA,clWhere,"URNO,ENTC",false) != true)
		{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING632) + LoadStg(IDS_STRING642);
		}
		olEntCPA.DeleteAll();
	}
//---------------------------
	if(ilStatus == true)
	{
		CString olAfmc;
		char clWhere[100];
		CCSPtrArray<AFMDATA> olAfmCPA;
		m_AFMC.GetWindowText(olAfmc);
		sprintf(clWhere,"WHERE AFMC='%s'",olAfmc);
		if(ogAFMData.ReadSpecialData(&olAfmCPA,clWhere,"URNO,AFMC",false) != true)
		{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING627) + LoadStg(IDS_STRING642);
		}
		olAfmCPA.DeleteAll();
	}
//---------------------------
	if(ilStatus == true)
	{
		CString olAlc3;
		char clWhere[100];
		CCSPtrArray<ALTDATA> olAlcCPA;
		m_ALTC.GetWindowText(olAlc3);
		sprintf(clWhere,"WHERE ALC3='%s'",olAlc3);
		if(ogALTData.ReadSpecialData(&olAlcCPA,clWhere,"URNO,ALC3",false) != true)
		{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING436) + LoadStg(IDS_STRING642);
		}
		olAlcCPA.DeleteAll();
	}
*/	
	/////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_ACT3.GetWindowText(pomACT->Act3,4);
		m_ACT5.GetWindowText(pomACT->Act5,6);
		m_ACFN.GetWindowText(pomACT->Acfn,ogBasicData.AircraftTypeDescriptionLength()+1);
		m_ACWS.GetWindowText(pomACT->Acws,6);
//		EliminateColons(pomACT->AcwsHelp);
		m_ACTI.GetWindowText(pomACT->Acti,6);
		m_ACHE.GetWindowText(pomACT->Ache,6);
		m_ACLE.GetWindowText(pomACT->Acle,6);
		m_ACWS.GetWindowText(pomACT->Acws,6);
		m_ACLE.GetWindowText(pomACT->Acle,6);
//		EliminateColons(pomACT->AcleHelp);
		m_ACHE.GetWindowText(pomACT->AcheHelp,6);
//		EliminateColons(pomACT->AcheHelp);
		m_SEAT.GetWindowText(pomACT->Seat,4);
		m_SEAF.GetWindowText(pomACT->Seaf,4);
		m_SEAB.GetWindowText(pomACT->Seab,4);
		m_SEAE.GetWindowText(pomACT->Seae,4);
		(m_NADS.GetCheck() == 1) ? pomACT->Nads[0] = 'X' : pomACT->Nads[0] = ' ';
		m_ENTY.GetWindowText(pomACT->Enty,6);//*** 07.09.99 SHA ***4->6
		m_ENNO.GetWindowText(pomACT->Enno,2);
		pomACT->Vafr = DateHourStringToDate(olVafrd,olVafrt); 
		pomACT->Vato = DateHourStringToDate(olVatod,olVatot);
		m_AFMC.GetWindowText(pomACT->Afmc,6);
		m_ALTC.GetWindowText(pomACT->Altc,4);
		m_MODC.GetWindowText(pomACT->Modc,7);
		m_ACBT.GetWindowText(pomACT->Acbt,2);

		if (ogBasicData.GetCustomerId() == "ADR")
		{
			m_MING.GetWindowText(pomACT->Ming,4);
		}

		//Kill the process
		if (mdBlackListProcId!=NULL)			
		{		
			killProcess( mdBlackListProcId );	
		}	


		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_ACFN.SetFocus();
		m_ACFN.SetSel(0,-1);

	}
}

void AircraftDlg::OnBSelEngine()
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<ENTDATA> olList;
//	ENTDATA	*prlEnt;
//	long	llUrno;

	AfxGetApp()->DoWaitCursor(1);
	if(ogENTData.ReadSpecialData(&olList, "", "URNO,ENTC,ENAM", false) == true)
	{
		AfxGetApp()->DoWaitCursor(-1);
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Entc));
			olCol2.Add(CString(olList[i].Enam));
		}
		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);
		if(pomDlg->DoModal() == IDOK)
		{
//		llUrno = pomDlg->lmReturnUrno;
//		prlEnt = ogENTData.GetEntByUrno(llUrno);
//		if (prlEnt > NULL)
//		{
			m_ENTY.SetWindowText(pomDlg->omReturnString);
			m_ENTY.SetFocus();
//		}
//		else {
//			m_ENTY.SetWindowText(" ");
//			m_ENTY.SetFocus();
//		}
	}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONSTOP|MB_OK));
	}
	olList.DeleteAll();

	AfxGetApp()->DoWaitCursor(-1);
//------------------------------------------------------------------------------------------------------------------------
	
}

void AircraftDlg::OnBSelAlt3() 
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<ALTDATA> olList;
//	ENTDATA	*prlEnt;
//	long	llUrno;

	AfxGetApp()->DoWaitCursor(1);
	if(ogALTData.ReadSpecialData(&olList, "", "URNO,ALC3,ALFN", false) == true)
	{
		AfxGetApp()->DoWaitCursor(-1);
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Alc3));
			olCol2.Add(CString(olList[i].Alfn));
		}
		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);
		if(pomDlg->DoModal() == IDOK)
		{
//		llUrno = pomDlg->lmReturnUrno;
//		prlEnt = ogENTData.GetEntByUrno(llUrno);
//		if (prlEnt > NULL)
//		{
			m_ALTC.SetWindowText(pomDlg->omReturnString);
			m_ALTC.SetFocus();
//		}
//		else {
//			m_ENTY.SetWindowText(" ");
//			m_ENTY.SetFocus();
//		}
	}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONSTOP|MB_OK));
	}
	olList.DeleteAll();

	AfxGetApp()->DoWaitCursor(-1);
//------------------------------------------------------------------------------------------------------------------------
	
}

void AircraftDlg::OnBSelAfm() 
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<AFMDATA> olList;
//	ENTDATA	*prlEnt;
//	long	llUrno;

	AfxGetApp()->DoWaitCursor(1);
	if(ogAFMData.ReadSpecialData(&olList, "", "URNO,AFMC,ANAM", false) == true)
	{
		AfxGetApp()->DoWaitCursor(-1);
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Afmc));
			olCol2.Add(CString(olList[i].Anam));
		}
		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);
		if(pomDlg->DoModal() == IDOK)
		{
//		llUrno = pomDlg->lmReturnUrno;
//		prlEnt = ogENTData.GetEntByUrno(llUrno);
//		if (prlEnt > NULL)
//		{
			m_AFMC.SetWindowText(pomDlg->omReturnString);
			m_AFMC.SetFocus();
//		}
//		else {
//			m_ENTY.SetWindowText(" ");
//			m_ENTY.SetFocus();
//		}
	}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONSTOP|MB_OK));
	}
	olList.DeleteAll();

	AfxGetApp()->DoWaitCursor(-1);
//------------------------------------------------------------------------------------------------------------------------
	
}


void EliminateColons(char *Buf)
{
	int	ili, ilj;
	bool bli = false;

	for (ili = 0; ili < (int) strlen(Buf); ili++)
	{
		if (Buf[ili] == '.')
		{	
			bli = true;
			ili++;
			for (ilj = 0; ilj < 2; ili++, ilj++)
			{
				if (Buf[ili] != 0)
					Buf[ili - 1] = Buf[ili];
				else
					Buf[ili - 1] = '0';
			}
			Buf[ili - 1] = 0;
			ili = (int) strlen(Buf);
		}
		else if (Buf[ili] == 0)
			ili = (int) strlen(Buf);
	}
	return;
}

void AircraftDlg::OnBlackList() 
{
	
	char pclCheck[256];
	char pclConfigPath[256];
	
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));
	
	GetPrivateProfileString(ogAppName, "BDPSUIF_ADDON" , "BDPS-UIF", pclCheck, sizeof pclCheck, pclConfigPath);	

	
	//BWA_ALT ,URNO1,URNO2|UserName,Permit,Time|100,100|Password
	char clStat = ogPrivList.GetStat("AIRCRAFTDLG.m_BLACK_LIST");
	if(pomACT != NULL)
	{
		char buf[33];
		mdBlackListProcId = LaunchTool( "BWA_ACT", CString(itoa(pomACT->Urno, buf, 10)), "", clStat, "BDPSUIF_ADDON",pclCheck , mdBlackListProcId );
	}
}
