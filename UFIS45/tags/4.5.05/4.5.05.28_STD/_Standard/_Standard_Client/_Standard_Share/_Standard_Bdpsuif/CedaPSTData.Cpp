// CedaPSTData.cpp
 
#include <stdafx.h>
#include <CedaPSTData.h>
#include <resource.h>
#include <BasicData.h>
#include <CedaSgrData.h>
#include <CedaSgmData.h>

// Local function prototype
static void ProcessPSTCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaPSTData::CedaPSTData() : CCSCedaData(&ogCommHandler)
{
    // Create an array of CEDARECINFO for PSTDATA
	BEGIN_CEDARECINFO(PSTDATA,PSTDataRecInfo)
		FIELD_CHAR_TRIM	(Acus,"ACUS")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Fuls,"FULS")
		FIELD_CHAR_TRIM	(Gpus,"GPUS")
		FIELD_DATE		(Lstu,"LSTU")
//		FIELD_DATE		(Nafr,"NAFR")
//		FIELD_DATE		(Nato,"NATO")
		FIELD_CHAR_TRIM	(Pcas,"PCAS")
		FIELD_CHAR_TRIM	(Pnam,"PNAM")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Pubk,"PUBK")
		FIELD_CHAR_TRIM	(Taxi,"TAXI")
		FIELD_CHAR_TRIM	(Tele,"TELE")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_DATE		(Vafr,"VAFR")
		FIELD_DATE		(Vato,"VATO")
//		FIELD_CHAR_TRIM	(Resn,"RESN")
		FIELD_CHAR_TRIM	(Dgss,"DGSS")
		FIELD_CHAR_TRIM	(Brgs,"BRGS")
		FIELD_CHAR_TRIM	(Home,"HOME")
		FIELD_CHAR_TRIM	(Mxac,"MXAC")
		FIELD_CHAR_TRIM	(Defd,"DEFD")
		FIELD_LONG		(Ibit,"IBIT")
		FIELD_CHAR_TRIM	(Posr,"POSR")
	END_CEDARECINFO //(PSTDataAStruct)

	// Copy the record structure
	for (int i=0; i< sizeof(PSTDataRecInfo)/sizeof(PSTDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&PSTDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"PST");
	strcpy(pcmPSTFieldList,"ACUS,CDAT,FULS,GPUS,LSTU,PCAS,PNAM,PRFL,PUBK,TAXI,TELE,URNO,USEC,USEU,VAFR,VATO,DGSS,BRGS,HOME,MXAC,DEFD,POSR");
	pcmFieldList = pcmPSTFieldList;

	omData.SetSize(0,1000);
	//omUrnoMap.InitHashTable(2001);
}

//----------------------------------------------------------------------------------------------------

void CedaPSTData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("ACUS");
	ropFields.Add("BRGS");
	ropFields.Add("CDAT");
	ropFields.Add("DGSS");
	ropFields.Add("FULS");
	ropFields.Add("GPUS");
	ropFields.Add("LSTU");
	ropFields.Add("NAFR");
	ropFields.Add("NATO");
	ropFields.Add("PCAS");
	ropFields.Add("PNAM");
	ropFields.Add("PRFL");
	ropFields.Add("PUBK");
	ropFields.Add("RESN");
	ropFields.Add("TAXI");
	ropFields.Add("TELE");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("VAFR");
	ropFields.Add("VATO");
	ropFields.Add("HOME");
	ropFields.Add("MXAC");
	ropFields.Add("DEFD");
	if (ogBasicData.IsGatPosEnabled())
		ropFields.Add("IBIT");

	ropDesription.Add(LoadStg(IDS_STRING514));
	ropDesription.Add(LoadStg(IDS_STRING515));
	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING516));
	ropDesription.Add(LoadStg(IDS_STRING517));
	ropDesription.Add(LoadStg(IDS_STRING518));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING364));
	ropDesription.Add(LoadStg(IDS_STRING365));
	ropDesription.Add(LoadStg(IDS_STRING519));
	ropDesription.Add(LoadStg(IDS_STRING520));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING521));
	ropDesription.Add(LoadStg(IDS_STRING366));
	ropDesription.Add(LoadStg(IDS_STRING522));
	ropDesription.Add(LoadStg(IDS_STRING294));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));
	ropDesription.Add(LoadStg(IDS_STRING711));
	ropDesription.Add(LoadStg(IDS_STRING1036));
	ropDesription.Add(LoadStg(IDS_STRING856));
	if (ogBasicData.IsGatPosEnabled())
		ropDesription.Add(LoadStg(IDS_STRING909));

	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	if (ogBasicData.IsGatPosEnabled())
		ropType.Add("String");
}

//----------------------------------------------------------------------------------------------------

void CedaPSTData::Register(void)
{
	ogDdx.Register((void *)this,BC_PST_CHANGE,CString("PSTDATA"), CString("PST-changed"),ProcessPSTCf);
	ogDdx.Register((void *)this,BC_PST_DELETE,CString("PSTDATA"), CString("PST-deleted"),ProcessPSTCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaPSTData::~CedaPSTData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaPSTData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaPSTData::Read(char *pcpWhere)
{
	if (ogBasicData.IsGatPosEnabled())
		strcpy(pcmPSTFieldList,"ACUS,CDAT,FULS,GPUS,LSTU,PCAS,PNAM,PRFL,PUBK,TAXI,TELE,URNO,USEC,USEU,VAFR,VATO,DGSS,BRGS,HOME,MXAC,DEFD,IBIT,POSR");
	pcmFieldList = pcmPSTFieldList;

    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pcpWhere != NULL)
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pcpWhere);
	else
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		PSTDATA *prpPST = new PSTDATA;
		if ((ilRc = GetFirstBufferRecord(prpPST)) == true)
		{
			prpPST->IsChanged = DATA_UNCHANGED;
			omData.Add(prpPST);//Update omData
			omUrnoMap.SetAt((void *)prpPST->Urno,prpPST);
		}
		else
		{
			delete prpPST;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaPSTData::InsertPST(PSTDATA *prpPST,BOOL bpSendDdx)
{
	prpPST->IsChanged = DATA_NEW;
	if(SavePST(prpPST) == false) return false; //Update Database
	InsertPSTInternal(prpPST);
    return true;
}

//--INSERT-INTERNAL---------------------------------------------------------------------------------------

bool CedaPSTData::InsertPSTInternal(PSTDATA *prpPST)
{
	//PreparePSTData(prpPST);
	ogDdx.DataChanged((void *)this, PST_CHANGE,(void *)prpPST ); //Update Viewer
	omData.Add(prpPST);//Update omData
	omUrnoMap.SetAt((void *)prpPST->Urno,prpPST);
	return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaPSTData::DeletePST(long lpUrno)
{
	PSTDATA *prlPST = GetPSTByUrno(lpUrno);
	if (prlPST != NULL)
	{
		prlPST->IsChanged = DATA_DELETED;
		if(SavePST(prlPST) == false) return false; //Update Database
		DeleteFromSgrSgm(*prlPST);
		DeletePSTInternal(prlPST);
	}
    return true;
}

//--DELETEFROMSGRSGM-------------------------------------------------------------------------------------------------

void CedaPSTData::DeleteFromSgrSgm(const PSTDATA &ropPST)
{
	SGRDATA olSgrData;		
	int oliCount = 0;
	char olpWhere[124];
	CCSPtrArray<SGRDATA> olaSgrData;	

	sprintf(olpWhere,"WHERE STYP = '%ld'",ropPST.Urno);
	ogSgrData.ReadSpecial(olaSgrData,olpWhere);	

	if(olaSgrData.GetSize() != 0)
	{
		for(int i = 0; i < olaSgrData.GetSize(); i++)
		{
			olSgrData = olaSgrData.GetAt(i);
			ogSgrData.Delete(&olSgrData,true);
		}
		olaSgrData.DeleteAll();
	}
	
	SGMDATA olSgmData;	
	CCSPtrArray<SGMDATA> olaSgmData;
	memset(olpWhere,'\0',124);
	sprintf(olpWhere,"WHERE VALU = '%ld'",ropPST.Urno);	
	ogSgmData.ReadSpecial(olaSgmData,olpWhere);
	if(olaSgmData.GetSize() != 0)
	{
		for(int i = 0; i < olaSgmData.GetSize(); i++)
		{
			olSgmData = olaSgmData.GetAt(i);
			ogSgmData.Delete(&olSgmData,true);
		}
		olaSgmData.DeleteAll();
	}
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaPSTData::DeletePSTInternal(PSTDATA *prpPST)
{
	ogDdx.DataChanged((void *)this,PST_DELETE,(void *)prpPST); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpPST->Urno);
	int ilPSTCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilPSTCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpPST->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaPSTData::PreparePSTData(PSTDATA *prpPST)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaPSTData::UpdatePST(PSTDATA *prpPST,BOOL bpSendDdx)
{
	if (GetPSTByUrno(prpPST->Urno) != NULL)
	{
		if (prpPST->IsChanged == DATA_UNCHANGED)
		{
			prpPST->IsChanged = DATA_CHANGED;
		}
		if(SavePST(prpPST) == false) return false; //Update Database
		UpdatePSTInternal(prpPST);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaPSTData::UpdatePSTInternal(PSTDATA *prpPST)
{
	PSTDATA *prlPST = GetPSTByUrno(prpPST->Urno);
	if (prlPST != NULL)
	{
		*prlPST = *prpPST; //Update omData
		ogDdx.DataChanged((void *)this,PST_CHANGE,(void *)prlPST); //Update Viewer
	}
    return true;
}

//--GET PST BY URNO----------------------------------------------------------------------------------------

PSTDATA *CedaPSTData::GetPSTByUrno(long lpUrno)
{
	PSTDATA  *prlPST;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlPST) == TRUE)
	{
		return prlPST;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaPSTData::ReadSpecialData(CCSPtrArray<PSTDATA> *popPst,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","PST",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","PST",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popPst != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			PSTDATA *prpPst = new PSTDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpPst,CString(pclFieldList))) == true)
			{
				popPst->Add(prpPst);
			}
			else
			{
				delete prpPst;
			}
		}
		if(popPst->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaPSTData::SavePST(PSTDATA *prpPST)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[10000];

	if (prpPST->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpPST->IsChanged)
	{
	case DATA_NEW:
		strcpy(prpPST->Posr, "-;-;-;;;0;;0;1;1;;;;0;;0;;0;;0;;0;;0;;0;;0;1;   ,,,   ,;;;;;;");
		MakeCedaData(&omRecInfo,olListOfData,prpPST);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpPST->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpPST->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpPST);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpPST->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpPST->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}

    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessPSTCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_PST_CHANGE :
	case BC_PST_DELETE :
		((CedaPSTData *)popInstance)->ProcessPSTBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaPSTData::ProcessPSTBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlPSTData = (struct BcStruct *) vpDataPointer;
	long llUrno = GetUrnoFromSelection(prlPSTData->Selection);

	PSTDATA *prlPST = GetPSTByUrno(llUrno);
	if(ipDDXType == BC_PST_CHANGE)
	{
		if (prlPST != NULL)
		{
			GetRecordFromItemList(prlPST,prlPSTData->Fields,prlPSTData->Data);
			if(ValidatePSTBcData(prlPST->Urno)) //Prf: 8795
			{
				UpdatePSTInternal(prlPST);
			}
			else
			{
				DeletePSTInternal(prlPST);
			}
		}
		else
		{
			prlPST = new PSTDATA;
			GetRecordFromItemList(prlPST,prlPSTData->Fields,prlPSTData->Data);
			if(ValidatePSTBcData(prlPST->Urno)) //Prf: 8795
			{
				InsertPSTInternal(prlPST);
			}
			else
			{
				delete prlPST;
			}
		}
	}
	if(ipDDXType == BC_PST_DELETE)
	{
		if (prlPST != NULL)
		{
			DeletePSTInternal(prlPST);
		}
	}
}

//Prf: 8795
//--ValidatePSTBcData--------------------------------------------------------------------------------------

bool CedaPSTData::ValidatePSTBcData(const long& lrpUrno)
{
	bool blValidatePSTBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<PSTDATA> olPsts;
		if(!ReadSpecialData(&olPsts,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidatePSTBcData = false;
		}
	}
	return blValidatePSTBcData;
}

//---------------------------------------------------------------------------------------------------------
bool CedaPSTData::MakeCedaData(CCSPtrArray<CEDARECINFO> *pomRecInfo,CString &pcpListOfData, void *pvpDataStruct)
{
    pcpListOfData = "";

	CString olFieldData;

    // Start conversion, field-by-field
    int nfields = pomRecInfo->GetSize();
    for (int fieldno = 0; fieldno < nfields; fieldno++)
    {

        CString s;
        char buf[33];   // ltoa() need buffer 33 bytes
        int type = (*pomRecInfo)[fieldno].Type;
        void *p = (void *)((char *)pvpDataStruct + (*pomRecInfo)[fieldno].Offset);
        int length = (*pomRecInfo)[fieldno].Length;

		if (strstr(this->pcmFieldList,(*pomRecInfo)[fieldno].Name) == NULL)
			continue;

        switch (type) {
        case CEDACHAR:
            s = (length == 0)? CString(" "): CString((char *)p).Left(length);
			MakeCedaString(s);
            olFieldData = s + ",";
            break;
        case CEDAINT:
            olFieldData = CString(itoa(*(int *)p, buf, 10)) + ",";
            break;
        case CEDALONG:
            olFieldData = CString(ltoa(*(long *)p, buf, 10)) + ",";
            break;
        case CEDADATE:  // format: YYYYMMDDHHMMSS
			olFieldData = ((CTime *)p)->Format("%Y%m%d%H%M%S") + ",";
            break;
        case CEDAOLEDATE:  // format: YYYYMMDDHHMMSS
			if(((COleDateTime *)p)->GetStatus() == COleDateTime::valid)
			{
				olFieldData = ((COleDateTime *)p)->Format("%Y%m%d%H%M%S") + ",";
			}
			else
			{
				olFieldData = " ,"; 
			}
            break;
		case CEDADOUBLE:
			char pclBuf[128];
//MWO 18.12.98
			sprintf(pclBuf,"%lf",(double *)p);
//			sprintf(pclBuf,"%.2f",(double *)p);
            olFieldData = CString(pclBuf) + ",";
			break;
        default:    // invalid field type
            return false;
        }
		pcpListOfData += olFieldData;
    }

	if(pcpListOfData.GetLength() > 0)
		pcpListOfData = pcpListOfData.Left(pcpListOfData.GetLength()-1);

	//pcpListOfData.SetAt(pcpListOfData.GetLength()-1,' ');
	//pcpListOfData.TrimRight( );

    return true;
}

//---------------------------------------------------------------------------------------------------------
