// StsTableViewer.cpp 
//

#include <stdafx.h>
#include <StsTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>
#include <resrc1.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void StsTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// StsTableViewer
//

StsTableViewer::StsTableViewer(CCSPtrArray<STSDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomStsTable = NULL;
    ogDdx.Register(this, STS_CHANGE, CString("STSTABLEVIEWER"), CString("Sts Update"), StsTableCf);
    ogDdx.Register(this, STS_NEW,    CString("STSTABLEVIEWER"), CString("Sts New"),    StsTableCf);
    ogDdx.Register(this, STS_DELETE, CString("STSTABLEVIEWER"), CString("Sts Delete"), StsTableCf);
}

//-----------------------------------------------------------------------------------------------

StsTableViewer::~StsTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void StsTableViewer::Attach(CCSTable *popTable)
{
    pomStsTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void StsTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void StsTableViewer::MakeLines()
{
	int ilStsCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilStsCount; ilLc++)
	{
		STSDATA *prlStsData = &pomData->GetAt(ilLc);
		MakeLine(prlStsData);
	}
}

//-----------------------------------------------------------------------------------------------

void StsTableViewer::MakeLine(STSDATA *prpSts)
{

    //if( !IsPassFilter(prpSts)) return;

    // Update viewer data for this shift record
    STSTABLE_LINEDATA rlSts;

	rlSts.Urno = prpSts->Urno;
	rlSts.Cdat = prpSts->Cdat;
	rlSts.Lstu = prpSts->Lstu;
	rlSts.Rema = prpSts->Rema;
	rlSts.Stsc = prpSts->Stsc;
	rlSts.Stsd = prpSts->Stsd;
	rlSts.Usec = prpSts->Usec;
	rlSts.Useu = prpSts->Useu;

	CreateLine(&rlSts);
}

//-----------------------------------------------------------------------------------------------

void StsTableViewer::CreateLine(STSTABLE_LINEDATA *prpSts)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareSts(prpSts, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	STSTABLE_LINEDATA rlSts;
	rlSts = *prpSts;
    omLines.NewAt(ilLineno, rlSts);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void StsTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 2;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomStsTable->SetShowSelection(TRUE);
	pomStsTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	int ilPos = 1;
	rlHeader.Length = 30; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING755),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 200; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING755),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 300; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING755),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);



	pomStsTable->SetHeaderFields(omHeaderDataArray);
	pomStsTable->SetDefaultSeparator();
	pomStsTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Stsc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Stsd;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Rema;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		pomStsTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomStsTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString StsTableViewer::Format(STSTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool StsTableViewer::FindSts(char *pcpStsKeya, char *pcpStsKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpStsKeya) &&
			 (omLines[ilItem].Keyd == pcpStsKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void StsTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    StsTableViewer *polViewer = (StsTableViewer *)popInstance;
    if (ipDDXType == STS_CHANGE || ipDDXType == STS_NEW) polViewer->ProcessStsChange((STSDATA *)vpDataPointer);
    if (ipDDXType == STS_DELETE) polViewer->ProcessStsDelete((STSDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void StsTableViewer::ProcessStsChange(STSDATA *prpSts)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpSts->Stsc;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpSts->Stsd;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpSts->Rema;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (FindLine(prpSts->Urno, ilItem))
	{
        STSTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Stsc = prpSts->Stsc;
		prlLine->Stsd = prpSts->Stsd;
		prlLine->Rema = prpSts->Rema;
		prlLine->Cdat = prpSts->Cdat;
		prlLine->Lstu = prpSts->Lstu;
		prlLine->Usec = prpSts->Usec;
		prlLine->Useu = prpSts->Useu;
		prlLine->Urno = prpSts->Urno;

		pomStsTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomStsTable->DisplayTable();
	}
	else
	{
		MakeLine(prpSts);
		if (FindLine(prpSts->Urno, ilItem))
		{
	        STSTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomStsTable->AddTextLine(olLine, (void *)prlLine);
				pomStsTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void StsTableViewer::ProcessStsDelete(STSDATA *prpSts)
{
	int ilItem;
	if (FindLine(prpSts->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomStsTable->DeleteTextLine(ilItem);
		pomStsTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool StsTableViewer::IsPassFilter(STSDATA *prpSts)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int StsTableViewer::CompareSts(STSTABLE_LINEDATA *prpSts1, STSTABLE_LINEDATA *prpSts2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpSts1->Tifd == prpSts2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpSts1->Tifd > prpSts2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void StsTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool StsTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void StsTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void StsTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING765);
	int ilOrientation = PRINT_PORTRAET;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool StsTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = 3;// omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool StsTableViewer::PrintTableLine(STSTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = 3; // omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Stsc;
				break;
			case 1:
				rlElement.Text		= prpLine->Stsd;
				break;
			case 2:
				rlElement.FrameRight  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Rema;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
