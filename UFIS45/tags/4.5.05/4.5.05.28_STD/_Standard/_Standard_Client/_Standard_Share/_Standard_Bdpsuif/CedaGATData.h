// CedaGATData.h

#ifndef __CEDAGATDATA__
#define __CEDAGATDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

struct GATDATA 
{
	CTime	 Bnaf; 		// Fluggastbr�cken nicht verf�gbar von
	CTime	 Bnat; 		// Fluggastbr�cken nicht verf�gbar bis
	CTime	 Cdat; 		// Erstellungsdatum
	char 	 Gnam[7]; 	// Gatename
	CTime	 Lstu; 		// Datum letzte �nderung
//	CTime	 Nafr; 		// Nicht verf�gbar von
//	CTime	 Nato; 		// Nicht verf�gbar bis
	char 	 Nobr[4]; 	// Anzahl Fluggastbr�cken
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Rga1[7]; 	// Verkn�pftes Gate 1
	char 	 Rga2[7]; 	// Verkn�pftes Gate 2
	char 	 Tele[12]; 	// Telefonnummer
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	CTime	 Vafr; 		// G�ltig von
	CTime	 Vato; 		// G�ltig bis
//	char 	 Resn[42]; 	// Grund f�r die Sperrung (Gate)
	char 	 Resb[42]; 	// Grund f�r die Sperrung (Fluggastbr�cken)
	char 	 Rbab[7]; 	// Verkn�pftes Gep�ckband (Ankunft)
	char 	 Busg[3]; 	// Bus-Gate
	char 	 Term[3]; 	// Terminal
	char 	 Home[4]; 	// Terminal
	char 	 Gtid[2]; 	// Terminal
	char 	 Gtyp[2]; 	// Terminal
	char	 Defd[5];	// Default allocation duration

	//*** 08.11.99 SHA ***
	char 	 Tel2[12]; 	// Telefonnummer 2 SWISSPORT!
	char 	 Tel3[12]; 	// Telefonnummer 3
	char 	 Tel4[12]; 	// Telefonnummer 4
	long	 Ibit;		// Index of bitmap for blocked times
	char 	 Gatr[2002]; 	// Telefonnummer 4
	//UFIS-1087
	char	Apis[3];//APIS
	char	Abpr[3];//Automatic Boarding Pass Reader Available
	char	Fids[3];//FIDS Installed

	char	Gcap[6];//Gate Capacity
	char	Nost[6];//No. of seat
	char	Nobp[4];//No. of Boarding Point
	char	Noxm[4];//No. of X Ray Machine
	char	Dcst[26];//DCS

	char     Fcom[2];   // Flag Common Gate UFIS-1320


	//DataCreated by this class
	int      IsChanged;

	GATDATA(void)
	{
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
//		Nafr=-1;
//		Nato=-1;
		Vafr=-1;
		Vato=-1;
		Bnaf=-1;
		Bnat=-1;
	}

}; // end GATDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino


class CedaGATData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr		 omUrnoMap;
	CMapStringToPtr		 omNameMap;
    CCSPtrArray<GATDATA> omData;

// Operations
public:
    CedaGATData();
	~CedaGATData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(void);

    bool Read(char *pcpWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<GATDATA> *popGat,char *pspWhere,char *pspFieldList,bool ipSYS = true);
	bool InsertGAT(GATDATA *prpGAT,BOOL bpSendDdx = TRUE);
	bool InsertGATInternal(GATDATA *prpGAT);
	bool UpdateGAT(GATDATA *prpGAT,BOOL bpSendDdx = TRUE);
	bool UpdateGATInternal(GATDATA *prpGAT);
	bool DeleteGAT(long lpUrno);
	bool DeleteGATInternal(GATDATA *prpGAT);
	GATDATA  *GetGATByUrno(long lpUrno);
	GATDATA	 *GetGATByName(const CString& ropName);
	bool SaveGAT(GATDATA *prpGAT);
	char pcmGATFieldList[2048];
	void ProcessGATBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	int	 GetGATList(CStringArray& ropGates);
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;} //Prf: 8795
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;} //Prf: 8795
	// Private methods
private:
    void PrepareGATData(GATDATA *prpGATData);
	void DeleteFromSgrSgm(const GATDATA &ropGAT);
    bool MakeCedaData(CCSPtrArray<CEDARECINFO> *pomRecInfo,CString &pcpListOfData, void *pvpDataStruct);
	CString omWhere; //Prf: 8795
	bool ValidateGATBcData(const long& lrpUnro); //Prf: 8795

};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDAGATDATA__
