// CedaDEVData.h

#ifndef __CEDADEVDATA__
#define __CEDADEVDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

struct DEVDATA 
{
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	CTime	 Cdat;		// Erstellungsdatum
	char 	 Usec[34]; 	// Anwender (Ersteller)
	CTime	 Lstu;	 	// Datum letzte �nderung
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	char 	 Devn[22]; 	// Ger�tename
	char 	 Grpn[14]; 	// Gruppenname
	char 	 Dadr[16]; 	// IP - Adresse oder COM - Port
	char 	 Dprt[6]; 	// Port
	char 	 Stat[2]; 	// Status ('U'=Up,'D'=Down,'M'=Maintenance)
	char 	 Dpid[14]; 	// Referenz auf DSPTAB.URNO
	char 	 Adid[14]; 	// Advertisment id
	char 	 Drnp[12]; 	// Ger�tenummer im Cluster
	char 	 Dffd[10]; 	// Feldfilter
	char 	 Dfco[129];	// Filterbeschreibung
	char 	 Dter[6]; 	// Terminal
	char 	 Dare[18]; 	// Ger�tebereich
	char 	 Algc[1024]; 	// Airline code
	char 	 Dalz[6]; 	// Alarm zone
	char 	 Ddgi[6]; 	// Degauss intervall
	char 	 Loca[130]; // Location
	char 	 Rema[130]; // Bemerkungen
	char 	 Divn[22]; 	// Inventarnummer
	char 	 Drna[12]; 	// Aktuelle Ger�tenummer im Cluster
	CTime 	 Dlct; 		// Last connection time
	CTime 	 Dldc; 		// Last disconnect time
	char 	 Dlrd[130];	// Letzte Wiederholung
	CTime 	 Dldt; 		// Letzte Degauss Zeit
	CTime	 Vafr;	 	// G�ltig von
	CTime	 Vato;	 	// G�ltig bis
	char	 Drgi[4];	// Asynch. Carousel
	char	 Dbrf[4];	// Brightness factor
	char	 Dcof[4];	// Contrast factor
	char	 Prfl[2];	// Protocol flag

	//DataCreated by this class
	int		 IsChanged;

	DEVDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
		Vafr=-1;
		Vato=-1;
	}

}; // end DEVDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaDEVData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr		 omUrnoMap;
	CMapStringToPtr		 omDadrMap;	
    CCSPtrArray<DEVDATA> omData;

// Operations
public:
    CedaDEVData();
	~CedaDEVData();

	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(void);

    bool Read(char *pcpWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<DEVDATA> *popAlt,char *pspWhere,char *pspFieldList,bool ipSYS = true);
	bool InsertDEV(DEVDATA *prpDEV,BOOL bpSendDdx = TRUE);
	bool InsertDEVInternal(DEVDATA *prpDEV);
	bool UpdateDEV(DEVDATA *prpDEV,BOOL bpSendDdx = TRUE);
	bool UpdateDEVInternal(DEVDATA *prpDEV);
	bool DeleteDEV(long lpUrno);
	bool DeleteDEVInternal(DEVDATA *prpDEV);
	DEVDATA  *GetDEVByUrno(long lpUrno);
	DEVDATA  *GetDEVByDadr(const CString& ropDadr);
	bool SaveDEV(DEVDATA *prpDEV);
	char pcmDEVFieldList[2048];
	void ProcessDEVBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	int GetALGCLength();
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;} //Prf: 8795
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;} //Prf: 8795
	
	// Private methods
private:
    void PrepareDEVData(DEVDATA *prpDEVData);
	CString	GetDadrFromSelection(CString opSelection);
	CString omWhere; //Prf: 8795
	bool ValidateDEVBcData(const long& lrpUnro); //Prf: 8795
	bool ValidateDEVBcData(const CString& ropDadr); //Prf: 8795
};


//---------------------------------------------------------------------------------------------------------

#endif //__CEDADEVDATA__
