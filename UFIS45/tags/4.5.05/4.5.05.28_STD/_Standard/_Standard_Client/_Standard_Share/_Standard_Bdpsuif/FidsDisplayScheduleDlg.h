#if !defined(AFX_FIDSDISPLAYSCHEDULEDLG_H__E8F15454_4A26_11D7_8007_00010215BFDE__INCLUDED_)
#define AFX_FIDSDISPLAYSCHEDULEDLG_H__E8F15454_4A26_11D7_8007_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FidsDisplayScheduleDlg.h : header file
//
#include <CedaDSCData.h>
#include <CedaDPTData.h>
#include <CCSEdit.h>
#include <CCSTable.h>
#include <CCSComboBox.h>

/////////////////////////////////////////////////////////////////////////////
// FidsDisplayScheduleDlg dialog

class FidsDisplayScheduleDlg : public CDialog
{
// Construction
public:
	FidsDisplayScheduleDlg(DSCDATA *popDSC,const CString& ropCaption,CWnd* pParent = NULL);   // standard constructor
	~FidsDisplayScheduleDlg();

// Dialog Data
	//{{AFX_DATA(FidsDisplayScheduleDlg)
	enum { IDD = IDD_FIDS_DISPLAY_SCHEDULE };
	CCSEdit	m_DEVN;	

	CButton	m_DPTNEW;
	CButton	m_DPTDEL;
	CStatic	m_DPTTABLEFRAME;

	CCSEdit	m_VAFRD;
	CCSEdit	m_VATOD;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;	
	CButton	m_CANCEL;
	CButton	m_OK;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FidsDisplayScheduleDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(FidsDisplayScheduleDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnDPTDel();
	afx_msg void OnDPTNew();
	afx_msg void OnButtonDeviceClicked();
	afx_msg void OnTableIPEdit();
	afx_msg void OnTableIPEditButtonDown(UINT wParam, LONG lParam);
	afx_msg void OnTableIPEditShiftButtonDown(UINT wParam, LONG lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	enum enumOperation{OP_INSERT,OP_DELETE};

public:	
	bool SaveDPTData();

private:
	void MakeDisplayPrototypeTable();
	void PopulateDisplayPrototypeTable();
	void InsertOrDelete(enumOperation InsertOrDel,int ipLineNo = -1);
	LONG OnTableIPEditKillfocus( UINT wParam, LPARAM lParam);	
	void CreateEditAttributes(CCSEDIT_ATTRIB* prpAttrib);
	void PrepareDPTDataArray();
	bool ValidateDPTData(CString& ropErrorMessage) const;
	CString PrepareDisplayDateFormat(const CString& ropDate, bool blSetDefaultIncaseEmptyDate = false) const;

private:
	CString		m_Caption;
	DSCDATA		*pomDSC;
	DPTDATA     *pomDPT;
	CCSTable	*pomTable;	
	CCSPtrArray<DPTDATA> omDPTDataArray;
	CCSPtrArray<DPTDATA> omDPTSaveDataArray;	
	CList<long,long> omTobeDeletedDptUrnosArray;

	class DisplayPrototypeLineData
	{
	public:
		long Urno;       //	Unique record number
		char Hopo[4];    // Home airport
		char Usec[33];   // User who created record
		char Useu[33];   // User who updated record
		CTime Cdat;      // Creation date/time
		CTime Lstu;      // Last update date/time
		long Rurn;       // Related URNO from DSCTAB
		char Defl[2];    // Default Flag
		char Doop[8];    // Days of operation
		char Dbgn[5];    // Time of schedule begin
		char Dend[5];    // Time of schedule end
		char Dpid[33];   // Display ID
		char Dffd[9];    // Filter Field
		char Dfco[129];  // Filter Contents
		char Dter[6];    // Terminal
		char Dare[11];   // Device Area
		char Algc[41];   // Airline codes

		DisplayPrototypeLineData()
		{
			memset(this,'\0',sizeof(*this));
			Cdat=-1;
			Lstu=-1;
		}

		DisplayPrototypeLineData(const DisplayPrototypeLineData& rhs)
		{
			this->Urno = rhs.Urno;
			strcpy(this->Hopo,rhs.Hopo);
			strcpy(this->Usec,rhs.Usec);
			strcpy(this->Useu,rhs.Useu);
			this->Cdat = rhs.Cdat;
			this->Lstu = rhs.Lstu;
			this->Rurn = rhs.Rurn;
			strcpy(this->Defl,rhs.Defl);
			strcpy(this->Doop,rhs.Doop);
			strcpy(this->Dbgn,rhs.Dbgn);
			strcpy(this->Dend,rhs.Dend);
			strcpy(this->Dpid,rhs.Dpid);
			strcpy(this->Dffd,rhs.Dffd);
			strcpy(this->Dfco,rhs.Dfco);
			strcpy(this->Dter,rhs.Dter);
			strcpy(this->Dare,rhs.Dare);
			strcpy(this->Algc,rhs.Algc);
		}

		DisplayPrototypeLineData& DisplayPrototypeLineData::operator=(const DisplayPrototypeLineData& rhs)
		{
			if (&rhs != this)
			{
				this->Urno = rhs.Urno;
				strcpy(this->Hopo,rhs.Hopo);
				strcpy(this->Usec,rhs.Usec);
				strcpy(this->Useu,rhs.Useu);
				this->Cdat = rhs.Cdat;
				this->Lstu = rhs.Lstu;
				this->Rurn = rhs.Rurn;
				strcpy(this->Defl,rhs.Defl);
				strcpy(this->Doop,rhs.Doop);
				strcpy(this->Dbgn,rhs.Dbgn);
				strcpy(this->Dend,rhs.Dend);
				strcpy(this->Dpid,rhs.Dpid);
				strcpy(this->Dffd,rhs.Dffd);
				strcpy(this->Dfco,rhs.Dfco);
				strcpy(this->Dter,rhs.Dter);
				strcpy(this->Dare,rhs.Dare);
				strcpy(this->Algc,rhs.Algc);				
			}
			return *this;
		}
	};

	CCSPtrArray<DisplayPrototypeLineData> omDisplayPrototypeLines;
	void PopulateDisplayPrototypeLineData(DisplayPrototypeLineData& ropDisplayPrototype,const DPTDATA& ropDptData);
	void PopulateDisplayPrototypeLineData();
	void PopulateDPTData(DPTDATA& ropDptData, int ilLineNo);	
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIDSDISPLAYSCHEDULEDLG_H__E8F15454_4A26_11D7_8007_00010215BFDE__INCLUDED_)
