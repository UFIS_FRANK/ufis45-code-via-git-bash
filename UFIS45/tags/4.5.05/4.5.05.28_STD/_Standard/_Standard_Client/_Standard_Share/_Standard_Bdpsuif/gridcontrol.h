// GridControl.h: interface for the CGridControl class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GRIDCONTROL_H__775E51E1_38ED_11D3_933B_00001C033B5D__INCLUDED_)
#define AFX_GRIDCONTROL_H__775E51E1_38ED_11D3_933B_00001C033B5D__INCLUDED_

#include <gxall.h>
#include <CCSGlobl.h>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

struct DOONMOUSECLICK
{
	UINT	iMsg;
	WPARAM  iWparam;
	bool	bOnlyInside; 	
};

#define WM_GRID_LBUTTONDBLCLK  WM_USER+800
#define WM_GRID_LBUTTONCLK  WM_USER+801
#define WM_GRID_LBUTTONDOWN  WM_USER+802
#define WM_GRID_DRAGBEGIN  WM_USER+803
#define WM_GRID_ENDEDIT  WM_USER+804


// Farben
#define MAXCOLORS 64
#define FIRSTCONFLICTCOLOR 31
// Symbolic colors (helper constants for CGateDiagramDialog -- testing purpose only)
#define BLACK		RGB(  0,   0,   0)
#define MAROON		RGB(128,   0,   0)  // dark red
#define GREEN		RGB(  0, 128,   0)  // dark green
#define DKGREEN		RGB(  0, 200,   0)  // dark green
#define OLIVE		RGB(128, 128,   0)  // dark yellow
#define NAVY		RGB(  0,   0, 128)  // dark blue
#define PURPLE		RGB(128,   0, 128)  // dark magenta
#define TEAL		RGB(  0, 128, 128)  // dark cyan
#define GRAY		RGB(128, 128, 128)  // dark gray
//#define SILVER		RGB(192, 192, 192)  // light gray
#define RED			RGB(255,   0,   0)
//#define ORANGE		RGB(255, 135,   0)
#define LIME		RGB(  0, 255,   0)  // green
//#define YELLOW		RGB(255, 255,   0)
#define LTYELLOW	RGB(255, 255, 160)
#define BLUE		RGB(  0,   0, 255)
#define FUCHSIA		RGB(255,   0, 255)  // magenta
#define AQUA		RGB(  0, 255, 255)  // cyan
#define WHITE		RGB(255, 255, 255)
#define LTGRAY		RGB(170, 170, 170)
#define MEDIUMDGRAY	RGB(164, 164, 164)

//COLORREF ogColors[MAXCOLORS+1];

/*void CreateBrushes()
{
	ogColors[0] = BLACK;
	ogColors[1] = WHITE;
	ogColors[2] = GRAY;
	ogColors[3] = GREEN;
	ogColors[4] = RED;
	ogColors[5] = BLUE;
	ogColors[6] = SILVER;
	ogColors[7] = MAROON;
	ogColors[8] = OLIVE;
	ogColors[9] = NAVY;
	ogColors[10] = PURPLE;
	ogColors[11] = TEAL;
	ogColors[12] = LIME;
	ogColors[13] = YELLOW;
	ogColors[14] = FUCHSIA;
	ogColors[15] = AQUA;
	ogColors[16] = WHITE;
	ogColors[17] = BLACK;
	ogColors[18] = ORANGE;
	ogColors[19] = GREEN;
	ogColors[20] = GREEN;
}
*/
//enum enumColorIndexes
//{
//	GRAY_IDX=2,GREEN_IDX,RED_IDX,BLUE_IDX,SILVER_IDX,MAROON_IDX,
//	OLIVE_IDX,NAVY_IDX,PURPLE_IDX,TEAL_IDX,LIME_IDX,
//	YELLOW_IDX,FUCHSIA_IDX,AQUA_IDX, WHITE_IDX,BLACK_IDX,ORANGE_IDX
//};


class CGridControl : public CGXGridWnd  
{
public:
	CGridControl ();
	CGridControl ( CWnd *popParent, UINT nID, ROWCOL nCols, ROWCOL nRows );
	virtual ~CGridControl();

//Implementation
	BOOL SetValue ( ROWCOL nRow, ROWCOL nCol, CString &cStr ) ;
	const CString& GetValue ( ROWCOL nRow, ROWCOL nCol ) ;
	virtual BOOL EnableGrid ( BOOL enable );
	void CopyStyleOfLine ( ROWCOL ipSourceLine, ROWCOL ipDestLine,
						   bool bpResetValues=true );
	void CopyStyleLastLineFromPrev ( bool bpResetValues=true );
	void EnableAutoGrow ( bool enable=true );
	void EnableSorting ( bool enable=true );
	void SetLbDblClickAction ( UINT ipMsg, WPARAM ipWparam, bool bpOnlyInside=true ); 
	void SetDirtyFlag ( bool dirty=true );
	bool IsGridDirty ();
	void SetParentWnd(CWnd *popWnd) {pomWnd = popWnd;}
	void SetEnableDnD(bool bpEnableDnD) {bmIsDnDEnabled = bpEnableDnD;}
	void SetToolTipArray(CStringArray *popToolTipArray) {pomToolTipArray = popToolTipArray;}
	int FindRowByUrnoDataPointer(long lpUrno);
	virtual BOOL InsertBottomRow ();
	
protected:
	BOOL OnEndEditing( ROWCOL nRow, ROWCOL nCol) ;
	virtual void DoAutoGrow ( ROWCOL nRow, ROWCOL nCol);
//	virtual BOOL InsertBottomRow ();
	BOOL OnRButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	BOOL OnLButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	BOOL OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	void OnTextNotFound(LPCTSTR);
	BOOL OnDeleteCell(ROWCOL nRow, ROWCOL nCol);
	virtual bool SortTable ( ROWCOL ipRowClicked, ROWCOL ipColClicked );
	int GetStyleRowCol(ROWCOL nRow, ROWCOL nCol, CGXStyle& style, GXModifyType mt = gxCopy, int nType = 0);


//{{AFX_MSG(CGridControl)
	afx_msg void OnLButtonUp(UINT nFlags, CPoint pt);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint pt);
//	afx_msg void OnKillFocus ( CWnd* pNewWnd );
	//}}AFX_MSG
DECLARE_MESSAGE_MAP()

	
protected:
	UINT	umID;
	CWnd	*pomWnd;
	bool    bmAutoGrow;
	bool    bmSortEnabled;
	bool    bmSortAscend;
	DOONMOUSECLICK smLButtonDblClick;	
	bool	bmIsDirty ;
	bool	bmIsDnDEnabled;
	CStringArray	*pomToolTipArray;
};

struct GRIDNOTIFY
{
	UINT	idc;
	ROWCOL	row;
	ROWCOL	col;
	ROWCOL	headerrows;
	ROWCOL	headercols;
	POINT	point;
	CString value1;
	void* value2;
	CGridControl* source;
};


class CTitleGridControl : public CGridControl
{
public:
	CTitleGridControl ();
	CTitleGridControl ( CWnd *popParent, UINT nID, ROWCOL nCols, ROWCOL nRows );
	virtual ~CTitleGridControl();
//Implementation
	void SetTitle ( CString &opTitle );
	CString GetTitle ();
	const CString &GetValue ( ROWCOL nRow, ROWCOL nCol ) ;
	BOOL SetValue ( ROWCOL nRow, ROWCOL nCol, CString &cStr ) ;
	BOOL EnableGrid ( BOOL enable );
	void RemoveOneRow ( ROWCOL nRow );
	bool SortTable ( ROWCOL ipRowClicked, ROWCOL ipColClicked );
protected:
	BOOL InsertBottomRow ();
	void SetRowHeadersText ();
	BOOL OnStartEditing(ROWCOL nRow, ROWCOL nCol);
private:
	CString omTitle;
};

#endif // !defined(AFX_GRIDCONTROL_H__775E51E1_38ED_11D3_933B_00001C033B5D__INCLUDED_)
