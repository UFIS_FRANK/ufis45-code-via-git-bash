#ifndef __NWHTABLEVIEWER_H__
#define __NWHTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaNwhData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct NWHTABLE_LINEDATA
{
	long 	Urno;
	CString	Year;
	CString Regi;
	CString Ctrc;
	CString Rema;
	CString	Hr01;
	CString	Hr02;
	CString	Hr03;
	CString	Hr04;
	CString	Hr05;
	CString	Hr06;
	CString	Hr07;
	CString	Hr08;
	CString	Hr09;
	CString	Hr10;
	CString	Hr11;
	CString	Hr12;
};

/////////////////////////////////////////////////////////////////////////////
// NwhTableViewer

class NwhTableViewer : public CViewer
{
// Constructions
public:
    NwhTableViewer(CCSPtrArray<NWHDATA> *popData);
    ~NwhTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(NWHDATA *prpNwh);
	int CompareNwh(NWHTABLE_LINEDATA *prpNwh1, NWHTABLE_LINEDATA *prpNwh2);
    void MakeLines();
	void MakeLine(NWHDATA *prpNwh);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(NWHTABLE_LINEDATA *prpNwh);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(NWHTABLE_LINEDATA *prpLine);
	void ProcessNwhChange(NWHDATA *prpNwh);
	void ProcessNwhDelete(NWHDATA *prpNwh);
	bool FindNwh(char *prpNwhKeya, char *prpNwhKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomNwhTable;
	CCSPtrArray<NWHDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<NWHTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(NWHTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__NWHTABLEVIEWER_H__
