// NwhTableViewer.cpp 
//

#include <stdafx.h>
#include <NwhTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>
#include <resrc1.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void NwhTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// NwhTableViewer
//

NwhTableViewer::NwhTableViewer(CCSPtrArray<NWHDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomNwhTable = NULL;
    ogDdx.Register(this, NWH_CHANGE, CString("NWHTABLEVIEWER"), CString("Nwh Update"), NwhTableCf);
    ogDdx.Register(this, NWH_NEW,    CString("NWHTABLEVIEWER"), CString("Nwh New"),    NwhTableCf);
    ogDdx.Register(this, NWH_DELETE, CString("NWHTABLEVIEWER"), CString("Nwh Delete"), NwhTableCf);
}

//-----------------------------------------------------------------------------------------------

NwhTableViewer::~NwhTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void NwhTableViewer::Attach(CCSTable *popTable)
{
    pomNwhTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void NwhTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void NwhTableViewer::MakeLines()
{
	int ilNwhCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilNwhCount; ilLc++)
	{
		NWHDATA *prlNwhData = &pomData->GetAt(ilLc);
		MakeLine(prlNwhData);
	}
}

//-----------------------------------------------------------------------------------------------

void NwhTableViewer::MakeLine(NWHDATA *prpNwh)
{
    // Update viewer data for this shift record
    NWHTABLE_LINEDATA rlNwh;

	rlNwh.Urno = prpNwh->Urno;
	rlNwh.Year = prpNwh->Year;
	/*CString olRegi;
	if(strcmp(prpNwh->Regi, "0") == 0)
		olRegi = CString("I");
	else if(strcmp(prpNwh->Regi, "1") == 0)
		olRegi = CString("R");
	else if (strcmp(prpNwh->Regi, "2") == 0)
		olRegi = CString("S");
	else
		olRegi = CString(" ");
	rlNwh.Regi = olRegi;*/
	rlNwh.Ctrc = prpNwh->Ctrc;
	rlNwh.Rema = prpNwh->Rema;
	rlNwh.Hr01 = prpNwh->Hr01;
	rlNwh.Hr02 = prpNwh->Hr02;
	rlNwh.Hr03 = prpNwh->Hr03;
	rlNwh.Hr04 = prpNwh->Hr04;
	rlNwh.Hr05 = prpNwh->Hr05;
	rlNwh.Hr06 = prpNwh->Hr06;
	rlNwh.Hr07 = prpNwh->Hr07;
	rlNwh.Hr08 = prpNwh->Hr08;
	rlNwh.Hr09 = prpNwh->Hr09;
	rlNwh.Hr10 = prpNwh->Hr10;
	rlNwh.Hr11 = prpNwh->Hr11;
	rlNwh.Hr12 = prpNwh->Hr12;

	CreateLine(&rlNwh);
}

//-----------------------------------------------------------------------------------------------

void NwhTableViewer::CreateLine(NWHTABLE_LINEDATA *prpNwh)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
	{
        if (CompareNwh(prpNwh, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	}
	NWHTABLE_LINEDATA rlNwh;
	rlNwh = *prpNwh;
    omLines.NewAt(ilLineno, rlNwh);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void NwhTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 13;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomNwhTable->SetShowSelection(TRUE);
	pomNwhTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;
	int ilPos = 1;
	rlHeader.Length = 30; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING759),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 40; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING759),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 60; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING759),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 60; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING759),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 60; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING759),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 60; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING759),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 60; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING759),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 60; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING759),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 60; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING759),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 60; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING759),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 60; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING759),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 60; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING759),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 60; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING759),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 60; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING759),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Length = 200; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING759),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);


	pomNwhTable->SetHeaderFields(omHeaderDataArray);
	pomNwhTable->SetDefaultSeparator();
	pomNwhTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Year;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_CENTER;
		rlColumnData.Text = omLines[ilLineNo].Ctrc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_RIGHT;
		rlColumnData.Text = omLines[ilLineNo].Hr01;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Hr02;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Hr03;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Hr04;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Hr05;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Hr06;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Hr07;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Hr08;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Hr09;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Hr10;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Hr11;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Hr12;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = omLines[ilLineNo].Rema;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomNwhTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomNwhTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString NwhTableViewer::Format(NWHTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool NwhTableViewer::FindNwh(char *pcpNwhKeya, char *pcpNwhKeyd, int& ilItem)
{
	return false;
}

//-----------------------------------------------------------------------------------------------

static void NwhTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
    NwhTableViewer *polViewer = (NwhTableViewer *)popInstance;
    if (ipDDXType == NWH_CHANGE || ipDDXType == NWH_NEW) polViewer->ProcessNwhChange((NWHDATA *)vpDataPointer);
    if (ipDDXType == NWH_DELETE) polViewer->ProcessNwhDelete((NWHDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void NwhTableViewer::ProcessNwhChange(NWHDATA *prpNwh)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpNwh->Year;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_CENTER;
	/*CString olRegi;
	if(strcmp(prpNwh->Regi, "0") == 0)
		olRegi = CString("I");
	else if(strcmp(prpNwh->Regi, "1") == 0)
		olRegi = CString("R");
	else if (strcmp(prpNwh->Regi, "2") == 0)
		olRegi = CString("S");
	else
		olRegi = CString(" ");*/
	rlColumn.Text = prpNwh->Ctrc;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_RIGHT;
	rlColumn.Text = prpNwh->Hr01;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpNwh->Hr02;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpNwh->Hr03;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpNwh->Hr04;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpNwh->Hr05;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpNwh->Hr06;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpNwh->Hr07;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpNwh->Hr08;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpNwh->Hr09;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpNwh->Hr10;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpNwh->Hr11;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpNwh->Hr12;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_LEFT;
	rlColumn.Text = prpNwh->Rema;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (FindLine(prpNwh->Urno, ilItem))
	{
        NWHTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpNwh->Urno;
		prlLine->Year = prpNwh->Year;
		//prlLine->Regi = prpNwh->Regi;
		//prlLine->Regi = olRegi;
		prlLine->Ctrc = prpNwh->Ctrc;
		prlLine->Rema = prpNwh->Rema;
		prlLine->Hr01 = prpNwh->Hr01;
		prlLine->Hr02 = prpNwh->Hr02;
		prlLine->Hr03 = prpNwh->Hr03;
		prlLine->Hr04 = prpNwh->Hr04;
		prlLine->Hr05 = prpNwh->Hr05;
		prlLine->Hr06 = prpNwh->Hr06;
		prlLine->Hr07 = prpNwh->Hr07;
		prlLine->Hr08 = prpNwh->Hr08;
		prlLine->Hr09 = prpNwh->Hr09;
		prlLine->Hr10 = prpNwh->Hr10;
		prlLine->Hr11 = prpNwh->Hr11;
		prlLine->Hr12 = prpNwh->Hr12;

		pomNwhTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomNwhTable->DisplayTable();
	}
	else
	{
		MakeLine(prpNwh);
		if (FindLine(prpNwh->Urno, ilItem))
		{
	        NWHTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomNwhTable->AddTextLine(olLine, (void *)prlLine);
				pomNwhTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void NwhTableViewer::ProcessNwhDelete(NWHDATA *prpNwh)
{
	int ilItem;
	if (FindLine(prpNwh->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomNwhTable->DeleteTextLine(ilItem);
		pomNwhTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool NwhTableViewer::IsPassFilter(NWHDATA *prpNwh)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int NwhTableViewer::CompareNwh(NWHTABLE_LINEDATA *prpNwh1, NWHTABLE_LINEDATA *prpNwh2)
{
	return 0;
}

//-----------------------------------------------------------------------------------------------

void NwhTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool NwhTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void NwhTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void NwhTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING763);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;

	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool NwhTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		rlElement.Alignment   = PRINT_LEFT;
		if(i>=2&&i<14)
		{
			rlElement.Alignment   = PRINT_RIGHT;
		}
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool NwhTableViewer::PrintTableLine(NWHTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Year;
				break;
			case 1:
				rlElement.Alignment  = PRINT_CENTER;
				rlElement.Text		= prpLine->Ctrc;
				break;
			case 2:
				rlElement.Alignment  = PRINT_RIGHT;
				rlElement.Text		= prpLine->Hr01;
				break;
			case 3:
				rlElement.Alignment  = PRINT_RIGHT;
				rlElement.Text		= prpLine->Hr02;
				break;
			case 4:
				rlElement.Alignment  = PRINT_RIGHT;
				rlElement.Text		= prpLine->Hr03;
				break;
			case 5:
				rlElement.Alignment  = PRINT_RIGHT;
				rlElement.Text		= prpLine->Hr04;
				break;
			case 6:
				rlElement.Alignment  = PRINT_RIGHT;
				rlElement.Text		= prpLine->Hr05;
				break;
			case 7:
				rlElement.Alignment  = PRINT_RIGHT;
				rlElement.Text		= prpLine->Hr06;
				break;
			case 8:
				rlElement.Alignment  = PRINT_RIGHT;
				rlElement.Text		= prpLine->Hr07;
				break;
			case 9:
				rlElement.Alignment  = PRINT_RIGHT;
				rlElement.Text		= prpLine->Hr08;
				break;
			case 10:
				rlElement.Alignment  = PRINT_RIGHT;
				rlElement.Text		= prpLine->Hr09;
				break;
			case 11:
				rlElement.Alignment  = PRINT_RIGHT;
				rlElement.Text		= prpLine->Hr10;
				break;
			case 12:
				rlElement.Alignment  = PRINT_RIGHT;
				rlElement.Text		= prpLine->Hr11;
				break;
			case 13:
				rlElement.Alignment  = PRINT_RIGHT;
				rlElement.Text		= prpLine->Hr12;
				break;
			case 14:
				rlElement.FrameRight  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Rema;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
