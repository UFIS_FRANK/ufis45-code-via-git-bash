#ifndef __LFZREGITABLEVIEWER_H__
#define __LFZREGITABLEVIEWER_H__

#include <stdafx.h>
#include <CedaACRData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>

struct LFZREGITABLE_LINEDATA
{
	long	Urno;
	CString	Regn;
	CString	Act3;
	CString	Act5;
	CString	Owne;
	CString	Selc;
	CString	Mtow;	
	CString	Mowe;
	CString	Apui;
	CString	Main;
	CString	Enna;
	CString	Annx;
	CString	Nose;
	CString	Noga;
	CString	Noto;
	CString	Debi;
	CString	Ladp;
	CString	Rema;
	CString	Vafr;
	CString	Vato;
	CString	Acti;
	CString	Enty;
	CString	Gall;
	CString	Frst;
	CString	Rest;
	CString	Efis;
	CString	Ache;
	CString	Acle;
	CString	Acws;
	CString	Aurn;
	CString	Conf;
	CString	Cont;
	CString	Ctry;
	CString	Cvtd;
	CString	Deli;
	CString	Etxt;
	CString	Exrg;
	CString	Fhrs;
	CString	Fuse;
	CString	Iata;
	CString	Icao;
	CString	Lcod;
	CString	Lcyc;
	CString	Lsdb;
	CString	Lsdf;
	CString	Lsfb;
	CString	Lstb;
	CString	Mode;
	CString	Nois;
	CString	Odat;
	CString	Oope;
	CString	Oopt;
	CString	Oord;
	CString	Opbb;
	CString	Opbf;
	CString	Opwb;
	CString	Powd;
	CString	Regb;
	CString	Scod;
	CString	Seri;
	CString	Strd;
	CString	Text;
	CString	Type;
	CString	Wfub;
	CString	Wobo;
	CString	Year;
	CString	Ufis;
	CString	Cnam;
	CString	Pnum;
	CString	Home;
/*	CString	Naco;
	CString	Oadr;
	CString	Typu;
	CString	Crw1;
	CString	Crw2;
*/};

/////////////////////////////////////////////////////////////////////////////
// LFZRegiTableViewer

class LFZRegiTableViewer : public CViewer
{
// Constructions
public:
    LFZRegiTableViewer(CCSPtrArray<ACRDATA> *popData);
    ~LFZRegiTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	BOOL IsPassFilter(ACRDATA *prpLFZRegi);
	int CompareLFZRegi(LFZREGITABLE_LINEDATA *prpLFZRegi1, LFZREGITABLE_LINEDATA *prpLFZRegi2);
    void MakeLines();
	void MakeLine(ACRDATA *prpLFZRegi);
	BOOL FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(LFZREGITABLE_LINEDATA *prpLFZRegi);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(LFZREGITABLE_LINEDATA *prpLine);
	void ProcessLFZRegiChange(ACRDATA *prpLFZRegi);
	void ProcessLFZRegiDelete(ACRDATA *prpLFZRegi);
	BOOL FindLFZRegi(char *prpLFZRegiKeya, char *prpLFZRegiKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	BOOL bmIsFromSearch;
// Attributes
private:
    CCSTable *pomLFZRegiTable;
	CCSPtrArray<ACRDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<LFZREGITABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(int ilPage,CUIntArray& ropColsPerPage,LFZREGITABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader(int ilPage,CUIntArray& ropColsPerPage);
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__LFZREGITABLEVIEWER_H__
