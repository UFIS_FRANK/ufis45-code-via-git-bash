// CedaRWYData.cpp
 
#include <stdafx.h>
#include <CedaRWYData.h>
#include <resource.h>


// Local function prototype
static void ProcessRWYCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaRWYData::CedaRWYData() : CCSCedaData(&ogCommHandler)
{
    // Create an array of CEDARECINFO for RWYDATA
	BEGIN_CEDARECINFO(RWYDATA,RWYDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Rnam,"RNAM")
		FIELD_CHAR_TRIM	(Rnum,"RNUM")
		FIELD_CHAR_TRIM	(Rtyp,"RTYP")
		FIELD_DATE		(Nafr,"NAFR")
		FIELD_DATE		(Nato,"NATO")
		FIELD_CHAR_TRIM	(Resn,"RESN")
		FIELD_DATE	    (Vafr,"VAFR")
		FIELD_DATE	    (Vato,"VATO")
		FIELD_CHAR_TRIM	(Home,"HOME")
	END_CEDARECINFO //(RWYDataAStruct)

	// Copy the record structure
	for (int i=0; i< sizeof(RWYDataRecInfo)/sizeof(RWYDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&RWYDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"RWY");
	strcpy(pcmRWYFieldList,"URNO,CDAT,USEC,LSTU,PRFL,USEU,RNAM,RNUM,RTYP,NAFR,NATO,RESN,VAFR,VATO,HOME");
	pcmFieldList = pcmRWYFieldList;

	omData.SetSize(0,1000);
	//omUrnoMap.InitHashTable(2001);
}

//----------------------------------------------------------------------------------------------------

void CedaRWYData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("LSTU");
	ropFields.Add("NAFR");
	ropFields.Add("NATO");
	ropFields.Add("PRFL");
	ropFields.Add("RESN");
	ropFields.Add("RNAM");
	ropFields.Add("RNUM");
	ropFields.Add("RTYP");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("VAFR");
	ropFields.Add("VATO");
	ropFields.Add("HOME");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING364));
	ropDesription.Add(LoadStg(IDS_STRING365));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING366));
	ropDesription.Add(LoadStg(IDS_STRING471));
	ropDesription.Add(LoadStg(IDS_STRING472));
	ropDesription.Add(LoadStg(IDS_STRING473));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));
	ropDesription.Add(LoadStg(IDS_STRING711));

	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");

}

//----------------------------------------------------------------------------------------------------

void CedaRWYData::Register(void)
{
	ogDdx.Register((void *)this,BC_RWY_CHANGE,CString("RWYDATA"), CString("RWY-changed"),ProcessRWYCf);
	ogDdx.Register((void *)this,BC_RWY_DELETE,CString("RWYDATA"), CString("RWY-deleted"),ProcessRWYCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaRWYData::~CedaRWYData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaRWYData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaRWYData::Read(char *pcpWhere)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pcpWhere != NULL)
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pcpWhere);
	else
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		RWYDATA *prpRWY = new RWYDATA;
		if ((ilRc = GetFirstBufferRecord(prpRWY)) == true)
		{
			prpRWY->IsChanged = DATA_UNCHANGED;
			omData.Add(prpRWY);//Update omData
			omUrnoMap.SetAt((void *)prpRWY->Urno,prpRWY);
		}
		else
		{
			delete prpRWY;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaRWYData::InsertRWY(RWYDATA *prpRWY,BOOL bpSendDdx)
{
	prpRWY->IsChanged = DATA_NEW;
	if(SaveRWY(prpRWY) == false) return false; //Update Database
	InsertRWYInternal(prpRWY);
    return true;
}

//--INSERT-INTERNAL-----------------------------------------------------------------------------------------

bool CedaRWYData::InsertRWYInternal(RWYDATA *prpRWY)
{
	//PrepareRWYData(prpRWY);
	ogDdx.DataChanged((void *)this, RWY_CHANGE,(void *)prpRWY ); //Update Viewer
	omData.Add(prpRWY);//Update omData
	omUrnoMap.SetAt((void *)prpRWY->Urno,prpRWY);
  return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaRWYData::DeleteRWY(long lpUrno)
{
	RWYDATA *prlRWY = GetRWYByUrno(lpUrno);
	if (prlRWY != NULL)
	{
		prlRWY->IsChanged = DATA_DELETED;
		if(SaveRWY(prlRWY) == false) return false; //Update Database
		DeleteRWYInternal(prlRWY);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaRWYData::DeleteRWYInternal(RWYDATA *prpRWY)
{
	ogDdx.DataChanged((void *)this,RWY_DELETE,(void *)prpRWY); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpRWY->Urno);
	int ilRWYCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilRWYCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpRWY->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaRWYData::PrepareRWYData(RWYDATA *prpRWY)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaRWYData::UpdateRWY(RWYDATA *prpRWY,BOOL bpSendDdx)
{
	if (GetRWYByUrno(prpRWY->Urno) != NULL)
	{
		if (prpRWY->IsChanged == DATA_UNCHANGED)
		{
			prpRWY->IsChanged = DATA_CHANGED;
		}
		if(SaveRWY(prpRWY) == false) return false; //Update Database
		UpdateRWYInternal(prpRWY);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaRWYData::UpdateRWYInternal(RWYDATA *prpRWY)
{
	RWYDATA *prlRWY = GetRWYByUrno(prpRWY->Urno);
	if (prlRWY != NULL)
	{
		*prlRWY = *prpRWY; //Update omData
		ogDdx.DataChanged((void *)this,RWY_CHANGE,(void *)prlRWY); //Update Viewer
	}
    return true;
}

//--GET RWY BY URNO----------------------------------------------------------------------------------------

RWYDATA *CedaRWYData::GetRWYByUrno(long lpUrno)
{
	RWYDATA  *prlRWY;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlRWY) == TRUE)
	{
		return prlRWY;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaRWYData::ReadSpecialData(CCSPtrArray<RWYDATA> *popRwy,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","RWY",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","RWY",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popRwy != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			RWYDATA *prpRwy = new RWYDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpRwy,CString(pclFieldList))) == true)
			{
				popRwy->Add(prpRwy);
			}
			else
			{
				delete prpRwy;
			}
		}
		if(popRwy->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaRWYData::SaveRWY(RWYDATA *prpRWY)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpRWY->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpRWY->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpRWY);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpRWY->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpRWY->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpRWY);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpRWY->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpRWY->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}

    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessRWYCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_RWY_CHANGE :
	case BC_RWY_DELETE :
		((CedaRWYData *)popInstance)->ProcessRWYBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaRWYData::ProcessRWYBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlRWYData = (struct BcStruct *) vpDataPointer;
	long llUrno = GetUrnoFromSelection(prlRWYData->Selection);

	RWYDATA *prlRWY = GetRWYByUrno(llUrno);
	if(ipDDXType == BC_RWY_CHANGE)
	{
		if (prlRWY != NULL)
		{
			GetRecordFromItemList(prlRWY,prlRWYData->Fields,prlRWYData->Data);
			if(ValidateRWYBcData(prlRWY->Urno)) //Prf: 8795
			{
				UpdateRWYInternal(prlRWY);
			}
			else
			{
				DeleteRWYInternal(prlRWY);
			}
		}
		else
		{
			prlRWY = new RWYDATA;
			GetRecordFromItemList(prlRWY,prlRWYData->Fields,prlRWYData->Data);
			if(ValidateRWYBcData(prlRWY->Urno)) //Prf: 8795
			{
				InsertRWYInternal(prlRWY);
			}
			else
			{
				delete prlRWY;
			}
		}
	}
	if(ipDDXType == BC_RWY_DELETE)
	{
		if (prlRWY != NULL)
		{
			DeleteRWYInternal(prlRWY);
		}
	}
}

//Prf: 8795
//--ValidateRWYBcData--------------------------------------------------------------------------------------

bool CedaRWYData::ValidateRWYBcData(const long& lrpUrno)
{
	bool blValidateRWYBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<RWYDATA> olRwys;
		if(!ReadSpecialData(&olRwys,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateRWYBcData = false;
		}
	}
	return blValidateRWYBcData;
}

//---------------------------------------------------------------------------------------------------------
