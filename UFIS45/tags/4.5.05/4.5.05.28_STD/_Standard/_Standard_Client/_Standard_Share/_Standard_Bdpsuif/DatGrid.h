#if !defined(AFX_DATGRID_H__558BAFC7_CCFD_4607_AD76_E7B7E3495920__INCLUDED_)
#define AFX_DATGRID_H__558BAFC7_CCFD_4607_AD76_E7B7E3495920__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DatGrid.h : header file
//
#include <CedaDatData.h>

/////////////////////////////////////////////////////////////////////////////
// CDatGrid window

class CDatGrid : public CGXGridWnd
{
// Construction
public:
	CDatGrid();

// Attributes
public:
// Operations
public:
	virtual bool Initialize(const CString& ropAloc,long lpAlid,bool bpCopy,CCSPtrArray<DATDATA>& ropDatPtr,bool bpReadOnly);
	virtual void Insert(CCSPtrArray<DATDATA>& ropDatPtr);
	virtual void Copy(CCSPtrArray<DATDATA>& ropDatPtr);
	virtual void Update(CCSPtrArray<DATDATA>& ropDatPtr);
	virtual void Delete(CCSPtrArray<DATDATA>& ropDatPtr,CCSPtrArray<DATDATA>& ropDeleteDatPtr);	
	virtual bool FinalCheck(CCSPtrArray<DATDATA>& ropDatPtr,CCSPtrArray<DATDATA>& ropDeleteDatPtr,CString& ropErrorText);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDatGrid)
	//}}AFX_VIRTUAL
			BOOL SubClassDlgItem(UINT nID, CWnd *pParent);
	
	virtual	BOOL OnLButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	virtual BOOL OnGridKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);

	virtual BOOL OnDeleteCell();
			BOOL OnEndEditing(ROWCOL nRow, ROWCOL nCol);
	virtual void OnModifyCell(ROWCOL nRow, ROWCOL nCol);

// Implementation
public:
	virtual ~CDatGrid();

	// Generated message map functions
protected:
	//{{AFX_MSG(CDatGrid)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
// Helpers
	void	Fill(CCSPtrArray<DATDATA>& ropDatPtr,bool bpReadOnly);
private:

	CString	omAloc;
	long	lmAloc;
	long	lmAlid;
	bool	bmCopy;
	bool	bmSortAscend;
	bool	bmSortNumerical;
	CString	omAllALT;
	CString	omAllGRN;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DATGRID_H__558BAFC7_CCFD_4607_AD76_E7B7E3495920__INCLUDED_)
