#if !defined(AFX_ARBEITSGRUPPENDLG_H__96252421_965F_11D2_ACEF_004095436A98__INCLUDED_)
#define AFX_ARBEITSGRUPPENDLG_H__96252421_965F_11D2_ACEF_004095436A98__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ArbeitsgruppenDlg.h : header file
//
#include <PrivList.h>
#include <CCSEdit.h>
#include <CCSComboBox.h>
#include <CedaWgpData.h>
#include <CedaPgpData.h>


/////////////////////////////////////////////////////////////////////////////
// CArbeitsgruppenDlg dialog

class CArbeitsgruppenDlg : public CDialog
{
// Construction
public:
	CArbeitsgruppenDlg(WGPDATA *popWgp,CWnd* pParent = NULL);   // standard constructor
	CStatic	*pomStatus;

// Dialog Data
	//{{AFX_DATA(CArbeitsgruppenDlg)
	enum { IDD = IDD_WORKGROUPS };
	CCSComboBox	m_PGP;
	CButton	m_OK;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_REMA;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CCSEdit	m_WGPC;
	CCSEdit	m_WGPN;
	CString m_Caption;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CArbeitsgruppenDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CArbeitsgruppenDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	
	WGPDATA	*pomWgp;
	CString	omOldCode;
	bool	bmChangeAction;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ARBEITSGRUPPENDLG_H__96252421_965F_11D2_ACEF_004095436A98__INCLUDED_)
