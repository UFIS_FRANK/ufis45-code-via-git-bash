#ifndef __ABSENCEPATTERNTABLEVIEWER_H__
#define __ABSENCEPATTERNTABLEVIEWER_H__

#include "stdafx.h"
#include "CedaMawData.h"
#include "CCSTable.h"
#include "CViewer.h"
#include "CCSPrint.h"


struct ABSENCEPATTERNTABLE_LINEDATA
{
	CString Dflt;
	CString Name;
	long Urno;
};

/////////////////////////////////////////////////////////////////////////////
// ABSENCEPATTERNTABLEVIEWER

class AbsencePatternTableViewer : public CViewer
{
// Constructions
public:
    AbsencePatternTableViewer(CCSPtrArray<MAWDATA> *popData);
    ~AbsencePatternTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(MAWDATA *prpAbsencePattern);
	int CompareAbsencePattern(ABSENCEPATTERNTABLE_LINEDATA *prpAbsencePattern1, ABSENCEPATTERNTABLE_LINEDATA *prpAbsencePattern2);
    void MakeLines();
	void MakeLine(MAWDATA *prpAbsencePattern);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(ABSENCEPATTERNTABLE_LINEDATA *prpAbsencePattern);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(ABSENCEPATTERNTABLE_LINEDATA *prpLine);
	void ProcessAbsencePatternChange(MAWDATA *prpAbsencePattern);
	void ProcessAbsencePatternDelete(MAWDATA *prpAbsencePattern);
	bool FindAbsencePattern(char *prpAbsencePatternKeya, char *prpAbsencePatternKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomAbsencePatternTable;
	CCSPtrArray<MAWDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<ABSENCEPATTERNTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(ABSENCEPATTERNTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__ABSENCEPATTERNTABLEVIEWER_H__
