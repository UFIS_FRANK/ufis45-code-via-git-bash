// MFM_Dlg.cpp : implementation file
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <MFM_Dlg.h>
#include <PrivList.h>
#include <AwDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// MFM_Dlg dialog


MFM_Dlg::MFM_Dlg(MFMDATA *popMfm, CWnd* pParent /*=NULL*/)
	: CDialog(MFM_Dlg::IDD, pParent)
{
	pomMfm = popMfm;
	//{{AFX_DATA_INIT(MFM_Dlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void MFM_Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(MFM_Dlg)
	DDX_Control(pDX, IDC_FM01, m_FM01);
	DDX_Control(pDX, IDC_FM02, m_FM02);
	DDX_Control(pDX, IDC_FM03, m_FM03);
	DDX_Control(pDX, IDC_FM04, m_FM04);
	DDX_Control(pDX, IDC_FM05, m_FM05);
	DDX_Control(pDX, IDC_FM06, m_FM06);
	DDX_Control(pDX, IDC_FM07, m_FM07);
	DDX_Control(pDX, IDC_FM08, m_FM08);
	DDX_Control(pDX, IDC_FM09, m_FM09);
	DDX_Control(pDX, IDC_FM10, m_FM10);
	DDX_Control(pDX, IDC_FM11, m_FM11);
	DDX_Control(pDX, IDC_FM12, m_FM12);
	DDX_Control(pDX, IDC_YEAR, m_YEAR);
	DDX_Control(pDX, IDOK,	   m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_DPT1,   m_DPT1);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU,   m_USEU);
	DDX_Control(pDX, IDC_USEC,   m_USEC);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(MFM_Dlg, CDialog)
	//{{AFX_MSG_MAP(MFM_Dlg)
	ON_BN_CLICKED(IDC_B_ORG_AW, OnBOrgAw)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// MFM_Dlg message handlers
BOOL MFM_Dlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_Caption = LoadStg(IDS_STRING136) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("MFMDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomMfm->Cdat.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomMfm->Cdat.Format("%H:%M"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomMfm->Lstu.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomMfm->Lstu.Format("%H:%M"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomMfm->Usec);
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomMfm->Useu);
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	//m_DPT1.SetFormat("x|#x|#x|#x|#x|#x|#x|#x|#");
	m_DPT1.SetTypeToString();
	m_DPT1.SetTextLimit(1,8);
	m_DPT1.SetBKColor(YELLOW);  
	m_DPT1.SetTextErrColor(RED);
	m_DPT1.SetInitText(pomMfm->Ctrc);
	//------------------------------------
	m_YEAR.SetFormat("####");
	m_YEAR.SetTextLimit(4,4);
	m_YEAR.SetBKColor(YELLOW);  
	m_YEAR.SetTextErrColor(RED);
	m_YEAR.SetInitText(pomMfm->Year);
	//------------------------------------
	m_FM01.SetTypeToInt(0, 99,false);
	m_FM01.SetTextErrColor(RED);
	m_FM01.SetInitText(pomMfm->Fm01);
	m_FM01.SetBKColor(YELLOW);  
	//------------------------------------
	m_FM02.SetTypeToInt(0, 99,false);
	m_FM02.SetTextErrColor(RED);
	m_FM02.SetInitText(pomMfm->Fm02);
	m_FM02.SetBKColor(YELLOW);  
	//------------------------------------
	m_FM03.SetTypeToInt(0, 99,false);
	m_FM03.SetTextErrColor(RED);
	m_FM03.SetInitText(pomMfm->Fm03);
	m_FM03.SetBKColor(YELLOW);  
	//------------------------------------
	m_FM04.SetTypeToInt(0, 99,false);
	m_FM04.SetTextErrColor(RED);
	m_FM04.SetInitText(pomMfm->Fm04);
	m_FM04.SetBKColor(YELLOW);  
	//------------------------------------
	m_FM05.SetTypeToInt(0, 99,false);
	m_FM05.SetTextErrColor(RED);
	m_FM05.SetInitText(pomMfm->Fm05);
	m_FM05.SetBKColor(YELLOW);  
	//------------------------------------
	m_FM06.SetTypeToInt(0, 99,false);
	m_FM06.SetTextErrColor(RED);
	m_FM06.SetInitText(pomMfm->Fm06);
	m_FM06.SetBKColor(YELLOW);  
	//------------------------------------
	m_FM07.SetTypeToInt(0, 99,false);
	m_FM07.SetTextErrColor(RED);
	m_FM07.SetInitText(pomMfm->Fm07);
	m_FM07.SetBKColor(YELLOW);  
	//------------------------------------
	m_FM08.SetTypeToInt(0, 99,false);
	m_FM08.SetTextErrColor(RED);
	m_FM08.SetInitText(pomMfm->Fm08);
	m_FM08.SetBKColor(YELLOW);  
	//------------------------------------
	m_FM09.SetTypeToInt(0, 99,false);
	m_FM09.SetTextErrColor(RED);
	m_FM09.SetInitText(pomMfm->Fm09);
	m_FM09.SetBKColor(YELLOW);  
	//------------------------------------
	m_FM10.SetTypeToInt(0, 99,false);
	m_FM10.SetTextErrColor(RED);
	m_FM10.SetInitText(pomMfm->Fm10);
	m_FM10.SetBKColor(YELLOW);  
	//------------------------------------
	m_FM11.SetTypeToInt(0, 99,false);
	m_FM11.SetTextErrColor(RED);
	m_FM11.SetInitText(pomMfm->Fm11);
	m_FM11.SetBKColor(YELLOW);  
	//------------------------------------
	m_FM12.SetTypeToInt(0, 99,false);
	m_FM12.SetTextErrColor(RED);
	m_FM12.SetInitText(pomMfm->Fm12);
	m_FM12.SetBKColor(YELLOW);  
	//------------------------------------
	return TRUE;
}

//----------------------------------------------------------------------------------------

void MFM_Dlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_DPT1.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
	}
	else
	{
		if(m_DPT1.GetWindowTextLength() == 0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
	}
	if(m_YEAR.GetStatus() == false)
	{
		ilStatus = false;
		if(m_YEAR.GetWindowTextLength() < 4)
		{
			olErrorText += LoadStg(IDS_STRING681) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING681) + ogNotFormat;
		}
	}
	if(m_FM01.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING780) + ogNotFormat;
	}
	if(m_FM02.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING781) + ogNotFormat;
	}
	if(m_FM03.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING782) + ogNotFormat;
	}
	if(m_FM04.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING783) + ogNotFormat;
	}
	if(m_FM05.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING784) + ogNotFormat;
	}
	if(m_FM06.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING785) + ogNotFormat;
	}
	if(m_FM07.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING786) + ogNotFormat;
	}
	if(m_FM08.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING787) + ogNotFormat;
	}
	if(m_FM09.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING788) + ogNotFormat;
	}
	if(m_FM10.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING789) + ogNotFormat;
	}
	if(m_FM11.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING790) + ogNotFormat;
	}
	if(m_FM12.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING791) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olDptc;//
		char clWhere[100];
		CCSPtrArray<COTDATA> olCotCPA;

		if(m_DPT1.GetWindowTextLength() != 0)
		{
			m_DPT1.GetWindowText(olDptc);
			sprintf(clWhere,"WHERE CTRC='%s'",olDptc);
			if(ogCotData.ReadSpecialData(&olCotCPA,clWhere,"URNO",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING1014);
			}
			olCotCPA.DeleteAll();
		}
	}
	if(ilStatus == true)
	{
		CString olYear;
		CString olDptc;
		CCSPtrArray<MFMDATA> olMfmCPA;
		MFMDATA olMfm;
		char clWhere[100];

		m_DPT1.GetWindowText(olDptc);
		m_YEAR.GetWindowText(olYear);
		if (olYear.GetLength() >= 4)
		{
			sprintf(clWhere,"WHERE CTRC='%s' AND YEAR='%s'",olDptc,olYear);
			if(ogMfmData.ReadSpecialData(&olMfmCPA,clWhere,"URNO",false) == true)
			{
				olMfm = olMfmCPA[0];
				if(olMfmCPA[0].Urno != pomMfm->Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING764) + LoadStg(IDS_EXIST);
				}
			}
		}
		else
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING764);
		}
	}


	///////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		CString olText;

		m_DPT1.GetWindowText(pomMfm->Ctrc,9);
		m_YEAR.GetWindowText(pomMfm->Year,4+1);
		m_FM01.GetWindowText(pomMfm->Fm01,2+1);
		m_FM02.GetWindowText(pomMfm->Fm02,2+1);
		m_FM03.GetWindowText(pomMfm->Fm03,2+1);
		m_FM04.GetWindowText(pomMfm->Fm04,2+1);
		m_FM05.GetWindowText(pomMfm->Fm05,2+1);
		m_FM06.GetWindowText(pomMfm->Fm06,2+1);
		m_FM07.GetWindowText(pomMfm->Fm07,2+1);
		m_FM08.GetWindowText(pomMfm->Fm08,2+1);
		m_FM09.GetWindowText(pomMfm->Fm09,2+1);
		m_FM10.GetWindowText(pomMfm->Fm10,2+1);
		m_FM11.GetWindowText(pomMfm->Fm11,2+1);
		m_FM12.GetWindowText(pomMfm->Fm12,2+1);

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_DPT1.SetFocus();
	}	
}

//----------------------------------------------------------------------------------------

void MFM_Dlg::OnBOrgAw() 
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<COTDATA> olList;
	//uhi 23.3.01
	char clWhere[100];
	sprintf(clWhere,"ORDER BY CTRC");
	if(ogCotData.ReadSpecialData(&olList, clWhere, "URNO,CTRC,CTRN", false) == true)
	{
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Ctrc));
			olCol2.Add(CString(olList[i].Ctrn));
		}
		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);
		if(pomDlg->DoModal() == IDOK)
		{
			m_DPT1.SetWindowText(pomDlg->omReturnString);
			m_DPT1.SetFocus();
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONSTOP|MB_OK));
	}
	olList.DeleteAll();
}

//----------------------------------------------------------------------------------------
