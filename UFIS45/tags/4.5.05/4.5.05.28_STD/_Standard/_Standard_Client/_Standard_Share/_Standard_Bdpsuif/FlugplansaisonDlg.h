#if !defined(AFX_FLUGPLANSAISONDLG_H__32F75442_55BF_11D1_B3C2_0000C016B067__INCLUDED_)
#define AFX_FLUGPLANSAISONDLG_H__32F75442_55BF_11D1_B3C2_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// FlugplansaisonDlg.h : header file
//
#include <CedaSeaData.h>
#include <CCSEdit.h>

/////////////////////////////////////////////////////////////////////////////
// FlugplansaisonDlg dialog

class FlugplansaisonDlg : public CDialog
{
// Construction
public:
	FlugplansaisonDlg(SEADATA *popSea,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(FlugplansaisonDlg)
	enum { IDD = IDD_FLUGPLANSAISONDLG };
	CButton	m_OK;
	CString	m_Caption;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CCSEdit	m_SEAS;
	CCSEdit	m_VPFRD;
	CCSEdit	m_VPFRT;
	CCSEdit	m_VPTOD;
	CCSEdit	m_VPTOT;
	CCSEdit	m_BEME;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FlugplansaisonDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(FlugplansaisonDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	SEADATA *pomSea;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FLUGPLANSAISONDLG_H__32F75442_55BF_11D1_B3C2_0000C016B067__INCLUDED_)
