// CedaPAGData.h

#ifndef __CEDAPAGDATA__
#define __CEDAPAGDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

struct PAGDATA 
{
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	CTime	 Cdat;		// Erstellungsdatum
	char 	 Usec[34]; 	// Anwender (Ersteller)
	CTime	 Lstu;	 	// Datum letzte �nderung
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	char 	 Pagi[14]; 	// Seitenname verweist auf DSPTAB.PInn
	char 	 Pcfn[14]; 	// Konfigurationsdateinname
	char 	 Pdpt[2]; 	// Display type ('A'=Arrival flights,'D'=Departure,'C'=Check-in counter,'L'=Other location,'X'=Free form page)
	char 	 Pdti[2]; 	// Interner Display type ('F'=Arrival || Departure,'C'=Check-in counter,'B'=Baggage belt,'G'=Gate,'H'=Chute)
	char 	 Pnof[6]; 	// No. of flights/Fields Format N.M (N=No of flights,M=No of display fields)
	char 	 Pfnt[2]; 	// Flight nature for type = 'A' oder 'D' only ('P'=display public flights,'S'=Display all flights)
	char 	 Pdss[4]; 	// Display sequence (type = 'A'||'D' :=)
	char 	 Pcti[4]; 	// Page carousel time
	char 	 Ptfb[6]; 	// Time frame begin
	char 	 Ptfe[6];	// Time frame end
	char 	 Ptd1[6]; 	// Delete from display after TIFA/TIFD
	char 	 Ptd2[6]; 	// Delete from display after PRFN
	char 	 Prfn[6]; 	// Feld name f�r PTD2
	char 	 Ptdc[6]; 	// Delete cancelled flight after FIFA/TIFD
	char 	 Ddgi[6]; 	// Degauss intervall
	char	 Cotb[6];	// Color table
	char     Rema[513]; // FIDS Remark

	//DataCreated by this class
	int		 IsChanged;

	PAGDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
	}

}; // end PAGDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaPAGData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr		omUrnoMap;
	CMapStringToPtr		omPagiMap;

    CCSPtrArray<PAGDATA> omData;

// Operations
public:
    CedaPAGData();
	~CedaPAGData();

	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(void);

    bool Read(char *pcpWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<PAGDATA> *popAlt,char *pspWhere,char *pspFieldList,bool ipSYS = true);
	bool InsertPAG(PAGDATA *prpPAG,BOOL bpSendDdx = TRUE);
	bool InsertPAGInternal(PAGDATA *prpPAG);
	bool UpdatePAG(PAGDATA *prpPAG,BOOL bpSendDdx = TRUE);
	bool UpdatePAGInternal(PAGDATA *prpPAG);
	bool DeletePAG(long lpUrno);
	bool DeletePAGInternal(PAGDATA *prpPAG);
	PAGDATA *GetPAGByUrno(long lpUrno);
	PAGDATA	*GetPAGByPagi(const char *prpPagi);
	bool SavePAG(PAGDATA *prpPAG);
	char pcmPAGFieldList[2048];
	void ProcessPAGBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	inline bool DoesRemarkCodeExist(){return bmRemarkExists;}
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;} //Prf: 8795
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;} //Prf: 8795

	// Private methods
private:
    void PreparePAGData(PAGDATA *prpPAGData);
	void AppendFields();
	bool bmRemarkExists;
	CString omWhere; //Prf: 8795
	bool ValidatePAGBcData(const long& lrpUnro); //Prf: 8795
};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDAPAGDATA__
