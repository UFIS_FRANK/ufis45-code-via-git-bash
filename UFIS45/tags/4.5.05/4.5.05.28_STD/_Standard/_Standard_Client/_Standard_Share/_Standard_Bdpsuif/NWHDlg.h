#if !defined(AFX_NWHDLG_H__EFDF8CC3_E7B1_11D4_BFF9_00D0B7E2A4B5__INCLUDED_)
#define AFX_NWHDLG_H__EFDF8CC3_E7B1_11D4_BFF9_00D0B7E2A4B5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NWHDlg.h : header file
//
#include <PrivList.h>
#include <CedaNwhData.h>
#include <CCSEdit.h>

/////////////////////////////////////////////////////////////////////////////
// CNWHDlg dialog

class CNWHDlg : public CDialog
{
// Construction
public:
	CNWHDlg(NWHDATA *popNwh, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CNWHDlg)
	enum { IDD = IDD_NWHDLG };
	CCSEdit	m_HR01;
	CCSEdit	m_HR02;
	CCSEdit	m_HR03;
	CCSEdit	m_HR04;
	CCSEdit	m_HR05;
	CCSEdit	m_HR06;
	CCSEdit	m_HR07;
	CCSEdit	m_HR08;
	CCSEdit	m_HR09;
	CCSEdit	m_HR10;
	CCSEdit	m_HR11;
	CCSEdit	m_HR12;
	CCSEdit	m_YEAR;
	CCSEdit	m_REGI;
	CCSEdit	m_REMA;
	CButton	m_OK;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEU;
	CString m_Caption;
	CCSEdit	m_USEC;
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNWHDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

private:
	NWHDATA *pomNwh;
// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CNWHDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnBCtrcAw();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NWHDLG_H__EFDF8CC3_E7B1_11D4_BFF9_00D0B7E2A4B5__INCLUDED_)
