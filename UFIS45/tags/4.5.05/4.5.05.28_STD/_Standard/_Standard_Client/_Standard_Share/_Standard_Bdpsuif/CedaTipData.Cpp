// CedaTipData.cpp
 
#include <stdafx.h>
#include <CedaTipData.h>
#include <resource.h>


void ProcessTipCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaTipData::CedaTipData() : CCSCedaData(&ogCommHandler)
{
    BEGIN_CEDARECINFO(TIPDATA, TipDataRecInfo)
		FIELD_CHAR_TRIM	(Tipc,"TIPC")
		FIELD_CHAR_TRIM	(Tipt,"TIPT")
		FIELD_CHAR_TRIM	(Tipu,"TIPU")
		FIELD_CHAR_TRIM	(Tipv,"TIPV")
		FIELD_DATE		(Vafr,"VAFR")
		FIELD_DATE		(Vato,"VATO")
		FIELD_LONG		(Urno,"URNO")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
	END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(TipDataRecInfo)/sizeof(TipDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&TipDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
    strcpy(pcmTableName,"TIP");
    sprintf(pcmListOfFields,"TIPC,TIPT,TIPU,TIPV,VAFR,VATO,URNO,CDAT,LSTU,PRFL,USEC,USEU");

	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaTipData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("TIPC");
	ropFields.Add("TIPT");
	ropFields.Add("TIPU");
	ropFields.Add("TIPV");
	ropFields.Add("CDAT");
	ropFields.Add("LSTU");
	ropFields.Add("PRFL");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("VAFR");
	ropFields.Add("VATO");

	ropDesription.Add(LoadStg(IDS_STRING237));
	ropDesription.Add(LoadStg(IDS_STRING501));
	ropDesription.Add(LoadStg(IDS_STRING586));
	ropDesription.Add(LoadStg(IDS_STRING587));
	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));

	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaTipData::Register(void)
{
	ogDdx.Register((void *)this,BC_TIP_CHANGE,	CString("TIPDATA"), CString("Tip-changed"),	ProcessTipCf);
	ogDdx.Register((void *)this,BC_TIP_NEW,		CString("TIPDATA"), CString("Tip-new"),		ProcessTipCf);
	ogDdx.Register((void *)this,BC_TIP_DELETE,	CString("TIPDATA"), CString("Tip-deleted"),	ProcessTipCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaTipData::~CedaTipData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaTipData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaTipData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		TIPDATA *prlTip = new TIPDATA;
		if ((ilRc = GetFirstBufferRecord(prlTip)) == true)
		{
			omData.Add(prlTip);//Update omData
			omUrnoMap.SetAt((void *)prlTip->Urno,prlTip);
		}
		else
		{
			delete prlTip;
		}
	}
    return true;

}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaTipData::Insert(TIPDATA *prpTip)
{
	prpTip->IsChanged = DATA_NEW;
	if(Save(prpTip) == false) return false; //Update Database
	InsertInternal(prpTip);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaTipData::InsertInternal(TIPDATA *prpTip)
{
	ogDdx.DataChanged((void *)this, TIP_NEW,(void *)prpTip ); //Update Viewer
	omData.Add(prpTip);//Update omData
	omUrnoMap.SetAt((void *)prpTip->Urno,prpTip);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaTipData::Delete(long lpUrno)
{
	TIPDATA *prlTip = GetTipByUrno(lpUrno);
	if (prlTip != NULL)
	{
		prlTip->IsChanged = DATA_DELETED;
		if(Save(prlTip) == false) return false; //Update Database
		DeleteInternal(prlTip);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaTipData::DeleteInternal(TIPDATA *prpTip)
{
	ogDdx.DataChanged((void *)this,TIP_DELETE,(void *)prpTip); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpTip->Urno);
	int ilTipCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilTipCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpTip->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaTipData::Update(TIPDATA *prpTip)
{
	if (GetTipByUrno(prpTip->Urno) != NULL)
	{
		if (prpTip->IsChanged == DATA_UNCHANGED)
		{
			prpTip->IsChanged = DATA_CHANGED;
		}
		if(Save(prpTip) == false) return false; //Update Database
		UpdateInternal(prpTip);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaTipData::UpdateInternal(TIPDATA *prpTip)
{
	TIPDATA *prlTip = GetTipByUrno(prpTip->Urno);
	if (prlTip != NULL)
	{
		*prlTip = *prpTip; //Update omData
		ogDdx.DataChanged((void *)this,TIP_CHANGE,(void *)prlTip); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

TIPDATA *CedaTipData::GetTipByUrno(long lpUrno)
{
	TIPDATA  *prlTip;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlTip) == TRUE)
	{
		return prlTip;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaTipData::ReadSpecialData(CCSPtrArray<TIPDATA> *popTip,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","TIP",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","TIP",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popTip != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			TIPDATA *prpTip = new TIPDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpTip,CString(pclFieldList))) == true)
			{
				popTip->Add(prpTip);
			}
			else
			{
				delete prpTip;
			}
		}
		if(popTip->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaTipData::Save(TIPDATA *prpTip)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpTip->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpTip->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpTip);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpTip->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpTip->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpTip);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpTip->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpTip->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessTipCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogTipData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaTipData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlTipData;
	prlTipData = (struct BcStruct *) vpDataPointer;
	TIPDATA *prlTip;
	if(ipDDXType == BC_TIP_NEW)
	{
		prlTip = new TIPDATA;
		GetRecordFromItemList(prlTip,prlTipData->Fields,prlTipData->Data);
		if(ValidateTipBcData(prlTip->Urno)) //Prf: 8795
		{
			InsertInternal(prlTip);
		}
		else
		{
			delete prlTip;
		}
	}
	if(ipDDXType == BC_TIP_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlTipData->Selection);
		prlTip = GetTipByUrno(llUrno);
		if(prlTip != NULL)
		{
			GetRecordFromItemList(prlTip,prlTipData->Fields,prlTipData->Data);
			if(ValidateTipBcData(prlTip->Urno)) //Prf: 8795
			{
				UpdateInternal(prlTip);
			}
			else
			{
				DeleteInternal(prlTip);
			}
		}
	}
	if(ipDDXType == BC_TIP_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlTipData->Selection);

		prlTip = GetTipByUrno(llUrno);
		if (prlTip != NULL)
		{
			DeleteInternal(prlTip);
		}
	}
}

//Prf: 8795
//--ValidateTipBcData--------------------------------------------------------------------------------------

bool CedaTipData::ValidateTipBcData(const long& lrpUrno)
{
	bool blValidateTipBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<TIPDATA> olTips;
		if(!ReadSpecialData(&olTips,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateTipBcData = false;
		}
	}
	return blValidateTipBcData;
}

//---------------------------------------------------------------------------------------------------------
