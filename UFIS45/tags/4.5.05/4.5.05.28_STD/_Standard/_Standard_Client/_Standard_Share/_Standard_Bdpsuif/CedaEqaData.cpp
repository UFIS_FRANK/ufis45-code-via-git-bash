// CedaEqaData.cpp
 
#include <stdafx.h>
#include <CedaEqaData.h>
#include <resource.h>


void ProcessEqaCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaEqaData::CedaEqaData() : CCSCedaData(&ogCommHandler)
{ 
	// Create an array of CEDARECINFO for EQADataStruct
	BEGIN_CEDARECINFO(EQADATA,EQADataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Hopo,"HOPO")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Name,"NAME")
		FIELD_LONG		(Uequ,"UEQU")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Valu,"VALU")

	END_CEDARECINFO //(EQADataStruct)	

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(EQADataRecInfo)/sizeof(EQADataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&EQADataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"EQA");
	//~~~~~ 13.08.99 SHA : 
	//*** ATTENTION FIELDS TATP AND TSAP REMOVED TEMPOR. !!!! ***
	strcpy(pcmListOfFields,"CDAT,HOPO,LSTU,NAME,UEQU,URNO,USEC,USEU,VALU");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaEqaData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("LSTU");
	ropFields.Add("NAME");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("VALU");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING288));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING587));

	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	
}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaEqaData::Register(void)
{
	ogDdx.Register((void *)this,BC_EQA_CHANGE,	CString("EQADATA"), CString("EQA-changed"),	ProcessEqaCf);
	ogDdx.Register((void *)this,BC_EQA_NEW,		CString("EQADATA"), CString("EQA-new"),		ProcessEqaCf);
	ogDdx.Register((void *)this,BC_EQA_DELETE,	CString("EQADATA"), CString("EQA-deleted"),	ProcessEqaCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaEqaData::~CedaEqaData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaEqaData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaEqaData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		EQADATA *prlEqa = new EQADATA;
		if ((ilRc = GetFirstBufferRecord(prlEqa)) == true)
		{
			omData.Add(prlEqa);//Update omData
			omUrnoMap.SetAt((void *)prlEqa->Urno,prlEqa);
		}
		else
		{
			delete prlEqa;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaEqaData::Insert(EQADATA *prpEqa)
{
	prpEqa->IsChanged = DATA_NEW;
	if(Save(prpEqa) == false) return false; //Update Database
	InsertInternal(prpEqa);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaEqaData::InsertInternal(EQADATA *prpEqa)
{
	ogDdx.DataChanged((void *)this, EQA_NEW,(void *)prpEqa ); //Update Viewer
	omData.Add(prpEqa);//Update omData
	omUrnoMap.SetAt((void *)prpEqa->Urno,prpEqa);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaEqaData::Delete(long lpUrno)
{
	EQADATA *prlEqa = GetEqaByUrno(lpUrno);
	if (prlEqa != NULL)
	{
		prlEqa->IsChanged = DATA_DELETED;
		if(Save(prlEqa) == false) return false; //Update Database
		DeleteInternal(prlEqa);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaEqaData::DeleteInternal(EQADATA *prpEqa)
{
	ogDdx.DataChanged((void *)this,EQA_DELETE,(void *)prpEqa); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpEqa->Urno);
	int ilEqaCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilEqaCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpEqa->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaEqaData::Update(EQADATA *prpEqa)
{
	if (GetEqaByUrno(prpEqa->Urno) != NULL)
	{
		if (prpEqa->IsChanged == DATA_UNCHANGED)
		{
			prpEqa->IsChanged = DATA_CHANGED;
		}
		if(Save(prpEqa) == false) return false; //Update Database
		UpdateInternal(prpEqa);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaEqaData::UpdateInternal(EQADATA *prpEqa)
{
	EQADATA *prlEqa = GetEqaByUrno(prpEqa->Urno);
	if (prlEqa != NULL)
	{
		*prlEqa = *prpEqa; //Update omData
		ogDdx.DataChanged((void *)this,EQA_CHANGE,(void *)prlEqa); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

EQADATA *CedaEqaData::GetEqaByUrno(long lpUrno)
{
	EQADATA  *prlEqa;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlEqa) == TRUE)
	{
		return prlEqa;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaEqaData::ReadSpecialData(CCSPtrArray<EQADATA> *popEqa,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	else
	{
		strcpy(pclFieldList, pcmListOfFields);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","EQA",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","EQA",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popEqa != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			EQADATA *prpEqa = new EQADATA;
			if ((ilRc = GetBufferRecord(ilLc,prpEqa,CString(pclFieldList))) == true)
			{
				popEqa->Add(prpEqa);
			}
			else
			{
				delete prpEqa;
			}
		}
		if(popEqa->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaEqaData::Save(EQADATA *prpEqa)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpEqa->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpEqa->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpEqa);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpEqa->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpEqa->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpEqa);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpEqa->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpEqa->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessEqaCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogEqaData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaEqaData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlEqaData;
	prlEqaData = (struct BcStruct *) vpDataPointer;
	EQADATA *prlEqa;
	if(ipDDXType == BC_EQA_NEW)
	{
		prlEqa = new EQADATA;
		GetRecordFromItemList(prlEqa,prlEqaData->Fields,prlEqaData->Data);
		InsertInternal(prlEqa);
	}
	if(ipDDXType == BC_EQA_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlEqaData->Selection);
		prlEqa = GetEqaByUrno(llUrno);
		if(prlEqa != NULL)
		{
			GetRecordFromItemList(prlEqa,prlEqaData->Fields,prlEqaData->Data);
			UpdateInternal(prlEqa);
		}
	}
	if(ipDDXType == BC_EQA_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlEqaData->Selection);

		prlEqa = GetEqaByUrno(llUrno);
		if (prlEqa != NULL)
		{
			DeleteInternal(prlEqa);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
