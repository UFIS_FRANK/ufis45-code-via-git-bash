﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ufis.Security
{
    public class SecurityUtils
    {
        /// <summary>
        /// Convert string to integer.
        /// </summary>
        /// <param name="value">String to convert.</param>
        /// <returns>Conversion result in integer.</returns>
        public static int StringToInt(string value)
        {
            int intResult;
            if (!int.TryParse(value, out intResult))
            {
                intResult = 0;
            }

            return intResult;
        }

        /// <summary>
        /// Validate the user's password.
        /// </summary>
        /// <param name="userName">A user's name.</param>
        /// <param name="oldPassword">A user's old password.</param>
        /// <param name="newPassword">A user's new password.</param>
        /// <param name="passwordRule">A password rule.</param>
        /// <returns>Password validation result.</returns>
        public static PasswordValidationResult ValidatePassword(string userName, string oldPassword, string newPassword, PasswordRule passwordRule)
        {
            PasswordValidationResult validationResult = PasswordValidationResult.None;

            if (string.IsNullOrWhiteSpace(newPassword))
            {
                validationResult |= PasswordValidationResult.BlankPassword;
            }

            if (oldPassword.Equals(newPassword))
            {
                validationResult |= PasswordValidationResult.SameOldAndNewPassword;
            }

            if (passwordRule != null)
            {
                if (newPassword.Length < passwordRule.MinLength)
                {
                    validationResult |= PasswordValidationResult.InvalidPasswordLength;
                }

                int intCapitalLetters = 0;
                int intSmallLetters = 0;
                int intNumericChars = 0;
                foreach (char cPass in newPassword)
                {
                    if (char.IsDigit(cPass))
                    {
                        intNumericChars++;
                    }
                    else if (char.IsUpper(cPass))
                    {
                        intCapitalLetters++;
                    }
                    else if (char.IsLower(cPass))
                    {
                        intSmallLetters++;
                    }
                }

                if (intCapitalLetters < passwordRule.MinCapitalLetters)
                {
                    validationResult |= PasswordValidationResult.InvalidCapitalLettersCount;
                }

                if (intSmallLetters < passwordRule.MinSmallLetters)
                {
                    validationResult |= PasswordValidationResult.InvalidSmallLettersCount;
                }

                if (intNumericChars < passwordRule.MinNumericChars)
                {
                    validationResult |= PasswordValidationResult.InvalidNumericCharsCount;
                }

                bool bInvalid = false;
                if (passwordRule.MaxIdenticalCharsWithUserName >= 2)
                {
                    for (int i = 0; i < userName.Length - passwordRule.MaxIdenticalCharsWithUserName + 1; i++)
                    {
                        string strPartOfUserName = userName.Substring(i, passwordRule.MaxIdenticalCharsWithUserName + 1);

                        if (newPassword.IndexOf(strPartOfUserName) >= 0)
                        {
                            bInvalid = true;
                            break;
                        }
                    }

                }
                if (bInvalid) validationResult |= PasswordValidationResult.InvalidSameCharsWithUserName;
            }

            if (validationResult.Equals(PasswordValidationResult.None)) validationResult = PasswordValidationResult.Succeeded;


            return validationResult;
        }
    }
}
