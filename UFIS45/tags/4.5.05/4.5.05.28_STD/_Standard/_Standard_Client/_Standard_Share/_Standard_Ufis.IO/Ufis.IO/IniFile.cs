﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Ufis.IO
{
    /// <summary>
	/// Due to the fact that .NET does not provide any functionality to
	/// read the old fashioned ini files this class helps to do this. 
	/// </summary>
    public class IniFile
    {
        /// <summary>
        /// The path to the ini file including the file name.
        /// </summary>
        /// <remarks>none</remarks>
        public string path;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section,
            string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section,
            string key, string def, StringBuilder retVal,
            int size, string filePath);

        /// <summary>
        /// IniFile Constructor.
        /// </summary>
        /// <param name="iniFilePath">The path including the file name of the ini file.</param>
        /// <remarks>none</remarks>
        public IniFile(string iniFilePath)
        {
            path = iniFilePath;
        }

        /// <summary>
        /// Write Data to the INI File
        /// </summary>
        /// <param name="Section">Section name in the ini file.</param>
        /// <param name="Key">Key Name within the section.</param>
        /// <param name="Value">Value Name</param>
        /// <remarks>none</remarks>
        public void IniWriteValue(string Section, string Key, string Value)
        {
            WritePrivateProfileString(Section, Key, Value, this.path);
        }

        /// <summary>
        /// Read Data Value From the Ini File
        /// </summary>
        /// <param name="Section">Section name within the ini file.</param>
        /// <param name="Key">Key name within the section.</param>
        /// <returns>The value for the Key.</returns>
        /// <remarks>none</remarks>
        public string IniReadValue(string Section, string Key)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", temp,
                255, this.path);
            return temp.ToString();

        }
    }
}
