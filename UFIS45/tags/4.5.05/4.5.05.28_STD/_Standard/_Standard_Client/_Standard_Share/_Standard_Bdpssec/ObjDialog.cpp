// ObjDialog.cpp : implementation file
//

#include <stdafx.h>
#include <CCSGlobl.h>
#include <BDPS_SEC.h>

#include <CedaPrvData.h>
#include <MessList.h>
#include <ObjDialog.h>
#include <CcsPrint.h>
#include <ObjList.h>
#include <PrintRightsDlg.h>
#include <ResRc1.h>
#include <fstream.h>
#include <iomanip.h>
#include <process.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const CString CONFIG_PATH = "C:\\UFIS\\SYSTEM\\CEDA.INI";
const CString CEDA = "CEDA";
const CString EXCEL = "EXCEL";
const CString DEFAULT = "DEFAULT";
const CString EXCELSEPARATOR = "EXCELSEPARATOR";
const CString FILENAME = "UserRights";
const CString EXCEL_PATH_NOT_FOUND = "Excel Path Not Found In CEDA.INI File.";
const CString EXCEL_PATH_NOT_CORRECT = "Excel Path In CEDA.INI Is Not Valid.";
const char SEPARATOR = ',';
const CString UNKNOWN_ERROR_EXCEL = "Unknown error ocurred in generating excel sheet";

// Local function prototype
static void ObjDialogCf(void *popInstance, int ipDDXType,
					   void *vpDataPointer, CString &ropInstanceName);

static int ByApplSubdFual(const ProfileListObject **ppp1, const ProfileListObject **ppp2);
static int ByApplSubdFual(const ProfileListObject **ppp1, const ProfileListObject **ppp2)
{
	int ilRc = strcmp((**ppp1).Sec->USID,(**ppp2).Sec->USID);
	if(ilRc == 0)
		ilRc = strcmp((**ppp1).Fkt->SUBD,(**ppp2).Fkt->SUBD);
	if(ilRc == 0)
		ilRc = strcmp((**ppp1).Fkt->FUAL,(**ppp2).Fkt->FUAL);
	return ilRc;
}


/////////////////////////////////////////////////////////////////////////////
// ObjDialog dialog


ObjDialog::ObjDialog(CWnd* pParent /*=NULL*/)
	: CDialog(ObjDialog::IDD, pParent)
{

	//{{AFX_DATA_INIT(ObjDialog)
	//}}AFX_DATA_INIT

	pomProfileListItem = NULL;

	pomApplTable = new CCSTable; // application list
	pomApplTable->SetStyle(WS_BORDER);
	pomApplTable->SetSelectMode(0);

	pomSubdTable = new CCSTable; // subdivision list
	pomSubdTable->SetStyle(WS_BORDER);
	pomSubdTable->SetSelectMode(0);

	pomFuncTable = new CCSTable; // function list
	pomFuncTable->SetStyle(WS_BORDER);
	pomFuncTable->SetSelectMode(LBS_MULTIPLESEL|LBS_EXTENDEDSEL);

	ogDdx.Register((void *) this, OBJ_DLG_CHANGE, CString("OBJDLG"),CString("OBJDLG CHANGE"), ObjDialogCf);

	ogDdx.Register((void *) this, BC_SBC_DELSEC, CString("OBJDLG"),CString("SECTAB DELSEC"), ObjDialogCf);
	ogDdx.Register((void *) this, BC_SBC_REMAPP, CString("OBJDLG"),CString("SECTAB REMAPP"), ObjDialogCf);
	ogDdx.Register((void *) this, BC_SBC_NEWAPP, CString("OBJDLG"),CString("SECTAB NEWAPP"), ObjDialogCf);
	ogDdx.Register((void *) this, BC_SBC_SETPRV, CString("OBJDLG"),CString("SECTAB SEPTRV"), ObjDialogCf);
	ogDdx.Register((void *) this, BC_SBC_SETFKT, CString("OBJDLG"),CString("SECTAB SETFKT"), ObjDialogCf);
	ogDdx.Register((void *) this, BC_SBC_ADDAPP, CString("OBJDLG"),CString("SECTAB ADDAPP"), ObjDialogCf);
	ogDdx.Register((void *) this, BC_SBC_DELAPP, CString("OBJDLG"),CString("SECTAB DELAPP"), ObjDialogCf);
	ogDdx.Register((void *) this, BC_SBC_UPDAPP, CString("OBJDLG"),CString("SECTAB UPDAPP"), ObjDialogCf);
	ogDdx.Register((void *) this, BC_SBC_NEWREL, CString("OBJDLG"),CString("SECTAB UPDAPP"), ObjDialogCf);
	ogDdx.Register((void *) this, BC_SBC_DELREL, CString("OBJDLG"),CString("SECTAB UPDAPP"), ObjDialogCf);
	ogDdx.Register((void *) this, BC_SBC_SMI, CString("OBJDLG"),CString("SECTAB SMI"), ObjDialogCf);
}

ObjDialog::~ObjDialog()
{
	delete pomApplTable;
	delete pomSubdTable;
	delete pomFuncTable;
	ogDdx.UnRegister(this, NOTUSED);
}


void ObjDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ObjDialog)
	DDX_Control(pDX, IDC_PROFILESUSED, m_ProfilesUsed);
	DDX_Control(pDX, IDC_PROFILEINFO, m_ProfileInfo);
	DDX_Control(pDX, IDC_PROFILELIST, m_ProfileList);
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDCANCEL, m_Cancel);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ObjDialog, CDialog)
	//{{AFX_MSG_MAP(ObjDialog)
	ON_MESSAGE(WM_TABLE_LBUTTONDOWN,OnTableLButtonDown)
	ON_MESSAGE(WM_TABLE_RBUTTONDOWN,OnTableRButtonDown)
	ON_MESSAGE(WM_TABLE_SELCHANGE,OnTableSelChange)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK,OnTableLButtonDblClk)
	ON_COMMAND(WM_INSTALL_MODULE,OnInstallModule)
	ON_COMMAND(WM_DEINSTALL_MODULE,OnDeinstallModule)
	ON_COMMAND(WM_REINSTALL_MODULE,OnReinstallModule)
	ON_COMMAND(WM_ENABLE_SELECTION,OnEnableSelection)
	ON_COMMAND(WM_DISABLE_SELECTION,OnDisableSelection)
	ON_COMMAND(WM_HIDE_SELECTION,OnHideSelection)
	ON_COMMAND(WM_ENABLE_ALL,OnEnableAll)
	ON_COMMAND(WM_DISABLE_ALL,OnDisableAll)
	ON_COMMAND(WM_HIDE_ALL,OnHideAll)
	ON_CBN_SELCHANGE(IDC_PROFILELIST, OnSelchangeProfilelist)
	ON_BN_CLICKED(ID_PRINTRIGHTS, OnPrintrights)
	//}}AFX_MSG_MAP
//	ON_WM_HELPINFO()
//	ON_WM_CONTEXTMENU()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ObjDialog message handlers


BOOL ObjDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// get the SEC rec for the selected line	
	SECDATA *prlSec = ogCedaSecData.GetSecByUrno(pcmFsec);


	// set titles, caption and USID field
	switch( rgCurrType )
	{
		case SEC_PROFILE:
			SetWindowText(GetString(IDS_OBJDIALOG_PROFILECAPTION));
			break;
		case SEC_GROUP:
			SetWindowText(GetString(IDS_OBJDIALOG_GROUPCAPTION));
			break;
		case SEC_WKS:
			SetWindowText(GetString(IDS_OBJDIALOG_WKSCAPTION));
			break;
		case SEC_WKSGROUP:
			SetWindowText(GetString(IDS_OBJDIALOG_WKSGROUPCAPTION));
			break;
		case SEC_APPL:
			SetWindowText(GetString(IDS_OBJDIALOG_MODULECAPTION));
			break;
		case SEC_USER:
		default:
			SetWindowText(GetString(IDS_OBJDIALOG_USERCAPTION));
			break;
	}

	GetDlgItem(ID_PRINTRIGHTS)->SetWindowText(GetString(IDS_OD_FREQ_HEAD));
	GetDlgItem(IDOK)->SetWindowText(GetString(IDS_OK));
	GetDlgItem(IDCANCEL)->SetWindowText(GetString(IDS_CANCEL));

	omImageList.Create(16,16,ILC_COLOR,7,0);

	HICON rlIcon;
	omImageList.SetBkColor(GetSysColor(COLOR_BTNFACE));
	rlIcon = ::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_VALIDFINALPROFILE));
	omImageList.Add(rlIcon);
	rlIcon = ::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_INVALIDFINALPROFILE));
	omImageList.Add(rlIcon);
	rlIcon = ::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_VALIDGROUP));
	omImageList.Add(rlIcon);
	rlIcon = ::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_INVALIDGROUP));
	omImageList.Add(rlIcon);
	rlIcon = ::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_VALIDPROFILE));
	omImageList.Add(rlIcon);
	rlIcon = ::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_INVALIDPROFILE));
	omImageList.Add(rlIcon);
	rlIcon = ::LoadIcon(AfxGetResourceHandle(),MAKEINTRESOURCE(IDI_ERRORRECURSION));
	omImageList.Add(rlIcon);
	m_ProfileList.SetImageList(&omImageList);

	omProfileListViewer.Attach(&m_ProfileList);

//	m_ProfileInfo.ModifyStyle(0,SS_ICON,0);

	// draw the 3 tables
	CRect rect;
	CWnd *polWnd = NULL; 
	const int ilTop = 70, ilBottom = 590;

	// applications table
	rect.left = 20;
	rect.right = 179;
	rect.top = ilTop;
	rect.bottom = ilBottom;
	if((polWnd = GetDlgItem(IDC_PLACEHOLDER1)) != NULL)
	{
		polWnd->GetWindowRect(rect);
		ScreenToClient(rect);
	}
	pomApplTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);
	omApplTableViewer.Attach(pomApplTable);

	// subdivisions table
	rect.left = 181;
	rect.right = 505;
	rect.top = ilTop;
	rect.bottom = ilBottom;
	if((polWnd = GetDlgItem(IDC_PLACEHOLDER2)) != NULL)
	{
		polWnd->GetWindowRect(rect);
		ScreenToClient(rect);
	}
	pomSubdTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);
	omSubdTableViewer.Attach(pomSubdTable);

	// functions table
	rect.left = 507;
	rect.right = 1000;
	rect.top = ilTop;
	rect.bottom = ilBottom;
	if((polWnd = GetDlgItem(IDC_PLACEHOLDER3)) != NULL)
	{
		polWnd->GetWindowRect(rect);
		ScreenToClient(rect);
	}
	pomFuncTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);
	omFuncTableViewer.Attach(pomFuncTable);


	// get and display the profile for the selected line
	CreateAndDisplayProfileList(0);


	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE

  
}

// ipSelectProfile - zero based profile num to select
void ObjDialog::CreateAndDisplayProfileList(int ipSelectProfile)
{
	AfxGetApp()->DoWaitCursor(1);
	pomProfileListItem = NULL;
	if( rgCurrType == SEC_APPL )
	{
		bmPersonalProf = true;
		m_ProfileList.EnableWindow(FALSE);
		DisplayProfile();
	}
	else
	{
		m_ProfileList.EnableWindow(TRUE);
		m_ProfileList.ResetContent();
		omProfileListViewer.ChangeViewTo(pcmFsec);
		if(m_ProfileList.GetCount() >= ipSelectProfile)
			m_ProfileList.SetCurSel(ipSelectProfile);
		OnSelchangeProfilelist();
	}
	AfxGetApp()->DoWaitCursor(-1);
}

void ObjDialog::DisplayProfile(void)
{
	// the following flag is used in case a MessageBox was open when a ddx was received
	bmMaskChanged = true;

	if( !bmPersonalProf )
	{
		m_OK.EnableWindow(false);
	}
	else
	{
		m_OK.EnableWindow(true);
	}

	
	UpdateView();


} // end DisplayProfile()


void ObjDialog::UpdateView()
{
	AfxGetApp()->DoWaitCursor(1);

	strcpy(omApplTableViewer.pcmFsec,pcmFsec); // URNO of selected user or profile
	strcpy(omSubdTableViewer.pcmFsec,pcmFsec); // URNO of selected user or profile
	strcpy(omFuncTableViewer.pcmFsec,pcmFsec); // URNO of selected user or profile

	if(rgCurrType != SEC_APPL)
	{
		omApplTableViewer.ChangeViewTo(pomProfileListItem->FfktMap);
	}
	else
	{
		CMapStringToPtr olDummyFfktMap;
		omApplTableViewer.ChangeViewTo(olDummyFfktMap);
	}

	omApplTableViewer.SelectLine(imLine);
	
	if(omApplTableViewer.omLines.GetSize() > 0)
	{
		strcpy(omSubdTableViewer.pcmFapp,omApplTableViewer.omLines[imLine].URNO); // URNO of selected application
		strcpy(omFuncTableViewer.pcmFapp,omApplTableViewer.omLines[imLine].URNO); // URNO of selected application
	}
	else
	{
		strcpy(omSubdTableViewer.pcmFapp,"");
		strcpy(omFuncTableViewer.pcmFapp,"");
	}

	if(rgCurrType != SEC_APPL)
	{
		omSubdTableViewer.ChangeViewTo(pomProfileListItem->Objects);
	}
	else
	{
		omSubdTableViewer.ChangeViewTo();
	}
	
	strcpy(omFuncTableViewer.pcmSubd,omSubdTableViewer.GetSubdByLine(0)); // select SUBD

	if(rgCurrType != SEC_APPL)
	{
		omFuncTableViewer.ChangeViewTo(pomProfileListItem->Objects);
	}
	else
	{
		omFuncTableViewer.ChangeViewTo();
	}
	
	omFuncTableViewer.SelectLine(0);

	AfxGetApp()->DoWaitCursor(-1);

} // end UpdateView()


static void ObjDialogCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlBcData = (struct BcStruct *) vpDataPointer;
    ObjDialog *polThis = (ObjDialog *)popInstance;

	switch(ipDDXType) {

	case OBJ_DLG_CHANGE:
		// profile allocation has been changed for a user in the modeless zuweisen dialog (alloc dialog)
		// check if this user was updated if so redisplay the profile
		if( rgCurrType != SEC_APPL && rgCurrType != SEC_PROFILE && !strcmp(polThis->pcmFsec,(char *)vpDataPointer) )
			polThis->DisplayProfile();
		break;
	case BC_SBC_DELSEC:
		polThis->ProcessDelSec(prlBcData->Fields,prlBcData->Data);
		break;
	case BC_SBC_REMAPP:
		polThis->ProcessRemApp(prlBcData->Fields,prlBcData->Data);
		break;
	case BC_SBC_NEWAPP:
		polThis->ProcessNewApp(prlBcData->Fields,prlBcData->Data);
		break;
	case BC_SBC_SETPRV:
		polThis->ProcessSetPrv(prlBcData->Fields,prlBcData->Data);
		break;
	case BC_SBC_SETFKT:
		polThis->ProcessSetFkt(prlBcData->Fields,prlBcData->Data);
		break;
	case BC_SBC_ADDAPP:
		polThis->ProcessAddApp(prlBcData->Fields,prlBcData->Data);
		break;
	case BC_SBC_DELAPP:
		polThis->ProcessDelApp(prlBcData->Fields,prlBcData->Data);
		break;
	case BC_SBC_UPDAPP:
		polThis->ProcessUpdApp(prlBcData->Fields,prlBcData->Data);
		break;
	case BC_SBC_NEWREL:
		polThis->ProcessNewRel(prlBcData->Fields,prlBcData->Data);
		break;
	case BC_SBC_DELREL:
		polThis->ProcessDelRel(prlBcData->Fields,prlBcData->Data);
		break;
	case BC_SBC_SMI:
		polThis->ProcessSmi(prlBcData->Fields,prlBcData->Data);
		break;
	default:
		break;
	}
}

void ObjDialog::ProcessDelSec(const char *pcpFieldList, const char *pcpDataList)
{
	if(!strcmp(pcpDataList,pcmFsec))
	{
		CString olMessage, olCaption;
		GetWindowText(olCaption);
		MessageBox(GetString(IDS_OBJDLG_BC1), olCaption, MB_ICONEXCLAMATION);
		CDialog::OnOK();
	}
	else if(BroadcastRequiresUpdate(pcpFieldList, pcpDataList))
	{
		HandleReload(GetString(IDS_OBJDLG_BC2));
	}
}

void ObjDialog::ProcessNewApp(const char *pcpFieldList, const char *pcpDataList)
{
	HandleReload(GetString(IDS_OBJDLG_BC3));
}

void ObjDialog::ProcessDelApp(const char *pcpFieldList, const char *pcpDataList)
{
	HandleReload(GetString(IDS_OBJDLG_BC4));
}

void ObjDialog::ProcessAddApp(const char *pcpFieldList, const char *pcpDataList)
{
	if(BroadcastRequiresUpdate(pcpFieldList, pcpDataList))
	{
		HandleReload(GetString(IDS_OBJDLG_BC5));
	}
}

void ObjDialog::ProcessUpdApp(const char *pcpFieldList, const char *pcpDataList)
{
	if(BroadcastRequiresUpdate(pcpFieldList, pcpDataList))
	{
		HandleReload(GetString(IDS_OBJDLG_BC6));
	}
}

void ObjDialog::ProcessRemApp(const char *pcpFieldList, const char *pcpDataList)
{
	if(BroadcastRequiresUpdate(pcpFieldList, pcpDataList))
	{
		HandleReload(GetString(IDS_OBJDLG_BC7));
	}
}

void ObjDialog::ProcessSetPrv(const char *pcpFieldList, const char *pcpDataList)
{
	if(BroadcastRequiresUpdate(pcpFieldList, pcpDataList))
	{
		HandleReload(GetString(IDS_OBJDLG_BC8));
	}
}

void ObjDialog::ProcessSetFkt(const char *pcpFieldList, const char *pcpDataList)
{
	if(rgCurrType == SEC_APPL)
	{
		HandleReload(GetString(IDS_OBJDLG_BC9));
	}
}

void ObjDialog::ProcessSmi(const char *pcpFieldList, const char *pcpDataList)
{
	HandleReload(GetString(IDS_OBJDLG_BC10));
}

void ObjDialog::ProcessNewRel(const char *pcpFieldList, const char *pcpDataList)
{
	// data list contains one or more FSECs
	CStringArray olFsecs;
	ExtractItemList(pcpDataList, &olFsecs);
	for(int i = 0; i < olFsecs.GetSize(); i++)
	{
		if(BroadcastRequiresUpdate("FSEC", olFsecs[i]))
		{
			HandleReload(GetString(IDS_OBJDLG_BC11));
			break;
		}
	}
}

void ObjDialog::ProcessDelRel(const char *pcpFieldList, const char *pcpDataList)
{
	// data list contains one or more GRPTAB.URNOS
	CStringArray olUrnos;
	ExtractItemList(pcpDataList, &olUrnos);
	for(int i = 0; i < olUrnos.GetSize(); i++)
	{
//		GRPDATA *prlGrp = ogCedaGrpData.GetGrpByUrno(olUrnos[i]);
//		if(prlGrp != NULL && BroadcastRequiresUpdate("FSEC", prlGrp->FSEC))
		if(BroadcastRequiresUpdate("FGRP", olUrnos[i]))
		{
			HandleReload(GetString(IDS_OBJDLG_BC12));
			break;
		}
	}
}

bool ObjDialog::BroadcastRequiresUpdate(const char *pcpFieldList, const char *pcpDataList)
{
	bool blBroadcastRequiresUpdate = false;

	char clFsec[20], clFapp[20];
	memset(clFsec, 0, sizeof(clFsec));
	memset(clFapp, 0, sizeof(clFapp));
	if(GetField(clFsec, "FSEC", pcpFieldList, pcpDataList) || GetField(clFsec, "FGRP", pcpFieldList, pcpDataList) || GetField(clFapp, "FAPP", pcpFieldList, pcpDataList))
	{
		if(rgCurrType == SEC_APPL)
		{
			if(!strcmp(clFsec, pcmFsec) || !strcmp(clFapp, pcmFsec))
				blBroadcastRequiresUpdate = true;
		}
		else if(strlen(clFsec) > 0)
		{
			// check if the SECTAB URNO is related to this profile
			blBroadcastRequiresUpdate = omProfileListViewer.UrnoLoaded(clFsec);
		}
		else if(strlen(clFapp) > 0)
		{
			// only the application URNO means that an application was inserted or deleted or updated
			blBroadcastRequiresUpdate = true;
		}
	}
	return blBroadcastRequiresUpdate;
}

void ObjDialog::HandleReload(CString opMessage)
{
	CString olMessage, olCaption;
	GetWindowText(olCaption);
	if(MakeListOfChanges())
	{
		olMessage = opMessage + CString("\n") + GetString(IDS_REFRESHSCREEN1);
		if(MessageBox(olMessage, olCaption, MB_ICONEXCLAMATION|MB_YESNO) == IDYES)
		{
			char pclErrorMessage[500];
			if(rgCurrType == SEC_APPL)
			{
				if(ogCedaFktData.SetFkt(omStatList, pclErrorMessage) != true)
					MessageBox(pclErrorMessage, GetString(IDS_DATABASE_ERROR), MB_ICONEXCLAMATION);
			}
			else
			{
				if(ogCedaPrvData.SetPrv(omStatList, pclErrorMessage) != true)
					MessageBox(pclErrorMessage, GetString(IDS_DATABASE_ERROR), MB_ICONEXCLAMATION);
			}
		}
	}
	else
	{
		olMessage = opMessage + CString("\n") + GetString(IDS_REFRESHSCREEN2);
		MessageBox(olMessage, olCaption, MB_ICONEXCLAMATION);
	}

	CreateAndDisplayProfileList(0);
}

//
// OnTableRButtonDown()
//
// receives a WM_TABLE_RBUTTONDOWN message from CCSTable
// when the function SelectLine() is called
// if the right mouse button was pressed, displays context sensetive menus
//
// ipParam contains flags indicating which button was pressed
// lpParam contains x,y co-ord where the mouse button was clicked
//
LONG ObjDialog::OnTableRButtonDown(UINT ipParam, LONG lpParam)
{
	CCSTABLENOTIFY *polNotify = (CCSTABLENOTIFY *) lpParam;
	int ilLine = polNotify->Line;

	if( polNotify->SourceTable == pomApplTable )
	{
		if( !bmPersonalProf )
			MessageBox(GetString(IDS_OBJDIALOG_NOTPERSONALPROFILE),"",MB_ICONINFORMATION);
		else
		{
			if( rgCurrType != SEC_APPL && bmPersonalProf && omApplTableViewer.omLines.GetSize() > 0)
			{
				strcpy(omSubdTableViewer.pcmFapp,omApplTableViewer.omLines[ilLine].URNO);
				strcpy(omFuncTableViewer.pcmFapp,omApplTableViewer.omLines[ilLine].URNO);
				omApplTableViewer.SelectLine(ilLine);
				omSubdTableViewer.UpdateDisplay();
				strcpy(omFuncTableViewer.pcmSubd,omSubdTableViewer.GetSubdByLine(0));
				omFuncTableViewer.UpdateDisplay();
				omFuncTableViewer.SelectLine(0);


				if(omApplTableViewer.omLines[ilLine].isInstalled == IS_INSTALLED)
				{
					omFuncTableViewer.SelectLine(0);
					CMenu olMenu;
					olMenu.CreatePopupMenu();
					for (int i = olMenu.GetMenuItemCount(); --i >= 0;)
  						olMenu.RemoveMenu(i, MF_BYPOSITION);
  					olMenu.AppendMenu(MF_STRING|MF_GRAYED,WM_INSTALL_MODULE, GetString(IDS_OBJDIALOG_MENUINSTALL));
  					olMenu.AppendMenu(MF_STRING,WM_DEINSTALL_MODULE, GetString(IDS_OBJDIALOG_MENUDEINSTALL));
  					olMenu.AppendMenu(MF_STRING|MF_GRAYED,WM_REINSTALL_MODULE, GetString(IDS_OBJDIALOG_MENUREINSTALL));
					pomApplTable->ClientToScreen(&polNotify->Point);
					olMenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, polNotify->Point.x, polNotify->Point.y, this, NULL);
				}
				else if(omApplTableViewer.omLines[ilLine].isInstalled == ISNOT_INSTALLED)
				{
					omFuncTableViewer.SelectLine(0);
					CMenu olMenu;
					olMenu.CreatePopupMenu();
					for (int i = olMenu.GetMenuItemCount(); --i >= 0;)
  						olMenu.RemoveMenu(i, MF_BYPOSITION);
  					olMenu.AppendMenu(MF_STRING,WM_INSTALL_MODULE, GetString(IDS_OBJDIALOG_MENUINSTALL));
  					olMenu.AppendMenu(MF_STRING|MF_GRAYED,WM_DEINSTALL_MODULE, GetString(IDS_OBJDIALOG_MENUDEINSTALL));
  					olMenu.AppendMenu(MF_STRING|MF_GRAYED,WM_REINSTALL_MODULE, GetString(IDS_OBJDIALOG_MENUREINSTALL));
					pomApplTable->ClientToScreen(&polNotify->Point);
					olMenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, polNotify->Point.x, polNotify->Point.y, this, NULL);
				}
				else if(omApplTableViewer.omLines[ilLine].isInstalled == PARTIALLY_INSTALLED)
				{
					omFuncTableViewer.SelectLine(0);
					CMenu olMenu;
					olMenu.CreatePopupMenu();
					for (int i = olMenu.GetMenuItemCount(); --i >= 0;)
  						olMenu.RemoveMenu(i, MF_BYPOSITION);
  					olMenu.AppendMenu(MF_STRING|MF_GRAYED,WM_INSTALL_MODULE, GetString(IDS_OBJDIALOG_MENUINSTALL));
  					olMenu.AppendMenu(MF_STRING,WM_DEINSTALL_MODULE, GetString(IDS_OBJDIALOG_MENUDEINSTALL));
  					olMenu.AppendMenu(MF_STRING,WM_REINSTALL_MODULE, GetString(IDS_OBJDIALOG_MENUREINSTALL));
					pomApplTable->ClientToScreen(&polNotify->Point);
					olMenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, polNotify->Point.x, polNotify->Point.y, this, NULL);
				}
			}
		}
	}
	else if( polNotify->SourceTable == pomSubdTable )
	{
		if( !bmPersonalProf )
			MessageBox(GetString(IDS_OBJDIALOG_NOTPERSONALPROFILE),"",MB_ICONINFORMATION);
		else
		{
			omSubdTableViewer.SelectLine(ilLine);
			strcpy(omFuncTableViewer.pcmSubd,omSubdTableViewer.GetSubdByLine(ilLine));
			omFuncTableViewer.UpdateDisplay();
			omFuncTableViewer.SelectLine(0);

			CMenu olMenu;
			olMenu.CreatePopupMenu();
			for (int i = olMenu.GetMenuItemCount(); --i >= 0;)
  				olMenu.RemoveMenu(i, MF_BYPOSITION);
  			olMenu.AppendMenu(MF_STRING,WM_ENABLE_ALL, GetString(IDS_OBJDIALOG_MENUENABLE));
  			olMenu.AppendMenu(MF_STRING,WM_DISABLE_ALL, GetString(IDS_OBJDIALOG_MENUDISABLE));
  			olMenu.AppendMenu(MF_STRING,WM_HIDE_ALL, GetString(IDS_OBJDIALOG_MENUHIDE));
			pomSubdTable->ClientToScreen(&polNotify->Point);
			olMenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, polNotify->Point.x, polNotify->Point.y, this, NULL);
		}
	}
	else if( polNotify->SourceTable == pomFuncTable )
	{
		if( !bmPersonalProf )
			MessageBox(GetString(IDS_OBJDIALOG_NOTPERSONALPROFILE),"",MB_ICONINFORMATION);
		else
		{
			CMenu olMenu;
			olMenu.CreatePopupMenu();
			for (int i = olMenu.GetMenuItemCount(); --i >= 0;)
  				olMenu.RemoveMenu(i, MF_BYPOSITION);
  			olMenu.AppendMenu(MF_STRING,WM_ENABLE_SELECTION, GetString(IDS_OBJDIALOG_MENUENABLE));
  			olMenu.AppendMenu(MF_STRING,WM_DISABLE_SELECTION, GetString(IDS_OBJDIALOG_MENUDISABLE));
  			olMenu.AppendMenu(MF_STRING,WM_HIDE_SELECTION, GetString(IDS_OBJDIALOG_MENUHIDE));
			pomFuncTable->ClientToScreen(&polNotify->Point);
			olMenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, polNotify->Point.x, polNotify->Point.y, this, NULL);
		}
	}
	
	return 0L;
}

//
// OnTableLButtonDown()
//
// receives a WM_TABLE_LBUTTONDOWN message from CCSTable
// when the function SelectLine() is called
// if the left mouse button was pressed, calls OnTableSelChange()
//
// ipParam contains flags indicating which button was pressed
// lpParam contains x,y co-ord where the mouse button was clicked
//
LONG ObjDialog::OnTableLButtonDown(UINT ipParam, LONG lpParam)
{
	CCSTABLENOTIFY *polNotify = (CCSTABLENOTIFY *) lpParam;
	int ilLine = polNotify->Line;

	if( polNotify->SourceTable == pomApplTable )
	{
		if(omApplTableViewer.omLines.GetSize() > 0)
		{
			strcpy(omSubdTableViewer.pcmFapp,omApplTableViewer.omLines[ilLine].URNO);
			strcpy(omFuncTableViewer.pcmFapp,omApplTableViewer.omLines[ilLine].URNO);
		}
		else
		{
			strcpy(omSubdTableViewer.pcmFapp,"");
			strcpy(omFuncTableViewer.pcmFapp,"");
		}

		if( rgCurrType == SEC_APPL )
		{
//			if( omApplTableViewer.omLines.GetSize() > 0)
//				m_Title2.SetWindowText(omApplTableViewer.omLines[ilLine].USID);
//			else
//				m_Title2.SetWindowText("");
		}

		omSubdTableViewer.UpdateDisplay();

		strcpy(omFuncTableViewer.pcmSubd,omSubdTableViewer.GetSubdByLine(0));
		omFuncTableViewer.UpdateDisplay();
		omFuncTableViewer.SelectLine(0);

		if( bmPersonalProf && omApplTableViewer.omLines.GetSize() > 0)
		{
			if(omApplTableViewer.omLines[ilLine].isInstalled == IS_INSTALLED)
				omFuncTableViewer.SelectLine(0);
			else if(omApplTableViewer.omLines[ilLine].isInstalled == ISNOT_INSTALLED)
				InstallApplication(ilLine);
			else if(omApplTableViewer.omLines[ilLine].isInstalled == PARTIALLY_INSTALLED)
				ReinstallApplication(ilLine);
		}
	}
	else if( polNotify->SourceTable == pomSubdTable )
	{
		strcpy(omFuncTableViewer.pcmSubd,omSubdTableViewer.GetSubdByLine(ilLine));
		omFuncTableViewer.UpdateDisplay();
		omFuncTableViewer.SelectLine(0);
	}
	else {
	}

	return 0L;
}

//
// OnTableLButtonDblClk()
//
// receives a WM_TABLE_LBUTTONDBLCLK message from CCSTable
//
LONG ObjDialog::OnTableLButtonDblClk(UINT ipParam, LONG lpParam)
{
	CCSTABLENOTIFY *polNotify = (CCSTABLENOTIFY *) lpParam;

	// if this is the functions table and is a personal profile
	if( polNotify->SourceTable == pomFuncTable )
	{
		if( bmPersonalProf )
		{
			omFuncTableViewer.UpdateStatus(polNotify->Line, this);
		}
		else
		{
			MessageBox(GetString(IDS_OBJDIALOG_NOTPERSONALPROFILE),"",MB_ICONINFORMATION);
		}
	}
	return 0L;
}

//
// OnTableSelChange()
//
// receives a WM_TABLE_SELCHANGE message from CCSTable
//
// ipParam contains flags indicating which button was pressed
// lpParam contains x,y co-ord where the mouse button was clicked
//
LONG ObjDialog::OnTableSelChange(UINT ipParam, LONG lpParam)
{
//	AfxMessageBox("OnTableSelChange");

	return 0L;
}


void ObjDialog::OnInstallModule()
{
	char pclCedaErrorMessage[ERRMESSLEN];
	imLine = pomApplTable->GetCurrentLine();
	APPLTAB_LINEDATA *prlApplLine = (APPLTAB_LINEDATA *) pomApplTable->GetTextLineData(imLine);

	AfxGetApp()->DoWaitCursor(1); // hourGlass
	if(ogCedaPrvData.AddApp( omApplTableViewer.omLines[imLine].URNO, pcmFsec, pclCedaErrorMessage ) != true )
		MessageBox(GetString(IDS_DATABASE_ERROR),"",MB_ICONEXCLAMATION);
	else
	{
		int ilOldSel = m_ProfileList.GetCurSel();
		CreateAndDisplayProfileList(ilOldSel);
	}
	AfxGetApp()->DoWaitCursor(-1); // hourGlass
}

void ObjDialog::OnDeinstallModule()
{
	char pclCedaErrorMessage[ERRMESSLEN];
	imLine = pomApplTable->GetCurrentLine();
	APPLTAB_LINEDATA *prlApplLine = (APPLTAB_LINEDATA *) pomApplTable->GetTextLineData(imLine);

	AfxGetApp()->DoWaitCursor(1); // hourGlass
	if(ogCedaPrvData.RemApp( omApplTableViewer.omLines[imLine].URNO, pcmFsec, pclCedaErrorMessage ) != true )
		MessageBox(GetString(IDS_DATABASE_ERROR),"",MB_ICONEXCLAMATION);
	else
	{
		int ilOldSel = m_ProfileList.GetCurSel();
		CreateAndDisplayProfileList(ilOldSel);
	}
	AfxGetApp()->DoWaitCursor(-1); // hourGlass
}

void ObjDialog::OnReinstallModule()
{
	char pclCedaErrorMessage[ERRMESSLEN];
	imLine = pomApplTable->GetCurrentLine();
	APPLTAB_LINEDATA *prlApplLine = (APPLTAB_LINEDATA *) pomApplTable->GetTextLineData(imLine);

	AfxGetApp()->DoWaitCursor(1); // hourGlass
	if(ogCedaPrvData.UpdApp( omApplTableViewer.omLines[imLine].URNO, pcmFsec, pclCedaErrorMessage ) != true )
		MessageBox(GetString(IDS_DATABASE_ERROR),"",MB_ICONEXCLAMATION);
	else
	{
		int ilOldSel = m_ProfileList.GetCurSel();
		CreateAndDisplayProfileList(ilOldSel);
	}
	AfxGetApp()->DoWaitCursor(-1); // hourGlass
}

void ObjDialog::OnEnableSelection()
{
	int ilNumSelLines = pomFuncTable->pomListBox->GetSelCount();
	int *pipSelLines = new int[ilNumSelLines];

	pomFuncTable->pomListBox->GetSelItems(ilNumSelLines,pipSelLines);

	omFuncTableViewer.UpdateStatus(pipSelLines,ilNumSelLines,ENABLED_STAT, this);

	delete [] pipSelLines;
}

void ObjDialog::OnDisableSelection()
{
	int ilNumSelLines = pomFuncTable->pomListBox->GetSelCount();
	int *pipSelLines = new int[ilNumSelLines];

	pomFuncTable->pomListBox->GetSelItems(ilNumSelLines,pipSelLines);

	omFuncTableViewer.UpdateStatus(pipSelLines,ilNumSelLines,DISABLED_STAT, this);

	delete [] pipSelLines;
}

void ObjDialog::OnHideSelection()
{
	int ilNumSelLines = pomFuncTable->pomListBox->GetSelCount();
	int *pipSelLines = new int[ilNumSelLines];

	pomFuncTable->pomListBox->GetSelItems(ilNumSelLines,pipSelLines);

	omFuncTableViewer.UpdateStatus(pipSelLines,ilNumSelLines,HIDDEN_STAT, this);

	delete [] pipSelLines;
}

void ObjDialog::OnEnableAll()
{
	int ilNumLines = pomFuncTable->GetLinesCount();
	int *pipLines = new int[ilNumLines];

	for(int ilLc=0; ilLc<ilNumLines; ilLc++ )
		pipLines[ilLc] = ilLc;

	omFuncTableViewer.UpdateStatus(pipLines,ilNumLines,ENABLED_STAT, this);

	delete [] pipLines;
}

void ObjDialog::OnDisableAll()
{
	int ilNumLines = pomFuncTable->GetLinesCount();
	int *pipLines = new int[ilNumLines];

	for(int ilLc=0; ilLc<ilNumLines; ilLc++ )
		pipLines[ilLc] = ilLc;

	omFuncTableViewer.UpdateStatus(pipLines,ilNumLines,DISABLED_STAT, this);

	delete [] pipLines;
}

void ObjDialog::OnHideAll()
{
	int ilNumLines = pomFuncTable->GetLinesCount();
	int *pipLines = new int[ilNumLines];

	for(int ilLc=0; ilLc<ilNumLines; ilLc++ )
		pipLines[ilLc] = ilLc;

	omFuncTableViewer.UpdateStatus(pipLines,ilNumLines,HIDDEN_STAT, this);

	delete [] pipLines;
}



void ObjDialog::InstallApplication(const int ipLine)
{
	char pclCedaErrorMessage[ERRMESSLEN];

	bmMaskChanged = false;
	if( MessageBox(GetString(IDS_OBJDIALOG_INSTALLAPPL),GetString(IDS_OBJDIALOG_INSTALLAPPLTITLE),MB_YESNO|MB_ICONQUESTION) == IDYES )
	{
		if(!bmMaskChanged) // check that while the question was asked the mask wasn't updated by ddx
		{
			AfxGetApp()->DoWaitCursor(1); // hourGlass
			if(ogCedaPrvData.AddApp( omApplTableViewer.omLines[ipLine].URNO, pcmFsec, pclCedaErrorMessage ) != true )
				MessageBox(GetString(IDS_DATABASE_ERROR),"",MB_ICONEXCLAMATION);
			else
			{
				// get and display the profile for the selected line
				imLine = ipLine; // reselect the original module
				int ilOldSel = m_ProfileList.GetCurSel();
				CreateAndDisplayProfileList(ilOldSel);
			}
			AfxGetApp()->DoWaitCursor(-1); // hourGlass
		}
	}
}



void ObjDialog::ReinstallApplication(const int ipLine)
{
	char pclCedaErrorMessage[ERRMESSLEN];

	bmMaskChanged = false;
	if( MessageBox(GetString(IDS_OBJDIALOG_REINSTALLAPPL),GetString(IDS_OBJDIALOG_REINSTALLAPPLTITLE),MB_YESNO|MB_ICONQUESTION) == IDYES )
	{
		if(!bmMaskChanged) // check that while the question was asked the mask wasn't updated by ddx
		{
			AfxGetApp()->DoWaitCursor(1); // hourGlass
			if(ogCedaPrvData.UpdApp( omApplTableViewer.omLines[ipLine].URNO, pcmFsec, pclCedaErrorMessage ) != true )
				MessageBox(GetString(IDS_DATABASE_ERROR),"",MB_ICONEXCLAMATION);
			else
			{
				imLine = ipLine; // reselect the original module
				int ilOldSel = m_ProfileList.GetCurSel();
				CreateAndDisplayProfileList(ilOldSel);
			}
			AfxGetApp()->DoWaitCursor(-1); // hourGlass
		}
	}
}



void ObjDialog::OnOK() 
{
	// loop through the functions list creating a list of URNO,STAT fields to be updated
	MakeListOfChanges();
	
	CDialog::OnOK();
}

bool ObjDialog::MakeListOfChanges()
{
	// loop through the functions list creating a list of URNO,STAT fields to be updated
	omStatList.Empty();
	int ilNumLines = omFuncTableViewer.omLines.GetSize();
	CString olComma(",");

	for(int ilLine=0; ilLine < ilNumLines; ilLine++ )
	{
		if( strcmp(omFuncTableViewer.omLines[ilLine].newSTAT,omFuncTableViewer.omLines[ilLine].oldSTAT) )
		{
			omStatList += CString(omFuncTableViewer.omLines[ilLine].URNO) + olComma + CString(omFuncTableViewer.omLines[ilLine].newSTAT) + olComma;
		}
	}

	return omStatList.IsEmpty() ? false : true;
}

void ObjDialog::OnSelchangeProfilelist() 
{
	//m_ProfileList.SetEditSel(-1,-1);
	
	int ilSel = m_ProfileList.GetCurSel();
	pomProfileListItem = (ProfileListItem *) m_ProfileList.GetItemData(ilSel);
	if(pomProfileListItem != NULL)
	{
		CString olInfo;
		if(!pomProfileListItem->IsValid)
		{
			olInfo = pomProfileListItem->DESC;
		}
		void *prlDummy;
		if(omProfileListViewer.NoProfilesDefined() || (pomProfileListItem->FrelList.GetCount() == 1 && pomProfileListItem->FrelList.Lookup(pcmFsec,(void *)prlDummy)))
		{
			bmPersonalProf = true;
		}
		else
		{
			bmPersonalProf = false;
		}

		m_ProfileInfo.SetWindowText(olInfo);
//		omProfileListViewer.CreateProfile(pomProfileListItem->FrelList,omPrvList,omFfktMap);
		CString olFrel;
		void *pvlTmp;
		CString olProfileNames;
		for(POSITION rlPos = pomProfileListItem->FrelList.GetStartPosition(); rlPos != NULL; )
		{
			pomProfileListItem->FrelList.GetNextAssoc(rlPos,olFrel,(void *& )pvlTmp);
			SECDATA *prlSec = ogCedaSecData.GetSecByUrno(olFrel);
			if(prlSec != NULL)
			{
				if(olProfileNames.IsEmpty())
				{
					olProfileNames = GetString(IDS_PROFILESUSED);
				}
				else
				{
					olProfileNames += " + ";
				}
				olProfileNames += "'" + CString(prlSec->USID) + "'";
			}
		}
		m_ProfilesUsed.SetWindowText(olProfileNames);
	}
	DisplayProfile();
}



void ObjDialog::OnPrintrights() 
{
	CPrintRightsDlg olDlg;
	if(olDlg.DoModal() != IDCANCEL)
	{
		int ilSel = olDlg.m_Sel;
		m_export  = olDlg.m_export;
		m_ExcelPath = olDlg.GetExcelPath();
		m_ExcelSeparator = olDlg.GetExcelSeparator();

		AfxGetApp()->DoWaitCursor(1);

		if(rgCurrType != SEC_APPL)
		{
			PrintPrvRights(ilSel);
		}
		else
		{
			PrintFktRights(ilSel);
		}

		AfxGetApp()->DoWaitCursor(1);
	}
}

void ObjDialog::PrintPrvRights(int ipSel)
{
	CString olFooter1,olFooter2;
	CString olTitle1;
	int ilSel = m_ProfileList.GetCurSel();
	ProfileListItem *prlItem = (ProfileListItem *) m_ProfileList.GetItemData(ilSel);
	if(prlItem != NULL)
	{
		olTitle1 = prlItem->NAME;
	}

	CString olFapp = omSubdTableViewer.pcmFapp;
	CString olSubd = omFuncTableViewer.pcmSubd;
	CString olTitle2;
	if(ipSel != PRINT_ALL)
	{
		SECDATA *prlSec = ogCedaSecData.GetSecByUrno(olFapp);
		if(prlSec != NULL)
		{
			olTitle2 = prlSec->USID;
		}
		if(ipSel == PRINT_SUBD)
		{
			olTitle2 += CString(" (") + olSubd + CString(")");
		}
	}

	CCSPtrArray <ProfileListObject> olObjects;
	int ilNumObjs = prlItem->Objects.GetSize();
	for(int ilObj = 0; ilObj < ilNumObjs; ilObj++)
	{
		ProfileListObject *prlObj = &prlItem->Objects[ilObj];
		FKTDATA *prlFkt = prlItem->Objects[ilObj].Fkt;

		if(ipSel == PRINT_ALL ||
		  (ipSel == PRINT_APPL && olFapp == prlFkt->FAPP) || 
		  (ipSel == PRINT_SUBD && olSubd == prlFkt->SUBD && olFapp == prlFkt->FAPP))
		{
			olObjects.Add(prlObj);
		}
	}
	olObjects.Sort(ByApplSubdFual);

	if(m_export == 1)
	{
		try
		{	
			GenerateExcelSheet(olObjects,olTitle1);
		}
		catch(CString& ropMessage)
		{
			AfxMessageBox(ropMessage);
		}
		catch(...)
		{
			AfxMessageBox(UNKNOWN_ERROR_EXCEL);
		}
	}
	else
	{
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE

	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(&ilOrientation) == TRUE)
		{
			int ilLinesPerPage = ilOrientation == PRINT_PORTRAET ? 57 : 38;

			pomPrint->imLineNo = ilLinesPerPage + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTitle1;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;

			int ilLines = olObjects.GetSize();
			int ilTotalPages = (((int) ilLines-1) / (int) ilLinesPerPage) + 1;
			int ilPageCount = 1;

			pomPrint->imLineNo = 0;
			olFooter1 = CTime::GetCurrentTime().Format("%d.%m.%Y %H:%M");
			for(int ilCurrLine = 0; ilCurrLine < ilLines; ilCurrLine++ ) 
			{
				if(pomPrint->imLineNo >= ilLinesPerPage)
				{
					// end of page reached
					olFooter2.Format("%d/%d",ilPageCount++,ilTotalPages);
					pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
					pomPrint->omCdc.EndPage();
					pomPrint->imLineNo = 0;
				}

				if( pomPrint->imLineNo == 0 )
				{
					// print the header at the start and when the page has changed
					pomPrint->PrintUIFHeader(olTitle1,olTitle2,pomPrint->imFirstLine-10);
				}

				// print a line
				PrintLine(&olObjects[ilCurrLine], ilOrientation);
			}

			olFooter2.Format("%d/%d",ilPageCount++,ilTotalPages);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}

		delete pomPrint;
		pomPrint = NULL;
		}
	}
}

// ipSel 0=All 1=WholeAppl 2=Subd
void ObjDialog::PrintFktRights(int ipSel)
{
	CString olFapp = omSubdTableViewer.pcmFapp;
	CString olSubd = omFuncTableViewer.pcmSubd;
	CString olTitle2;
	if(ipSel != PRINT_ALL)
	{
		SECDATA *prlSec = ogCedaSecData.GetSecByUrno(olFapp);
		if(prlSec != NULL)
		{
			olTitle2 = prlSec->USID;
		}
		if(ipSel == PRINT_SUBD)
		{
			olTitle2 += CString(" (") + olSubd + CString(")");
		}
	}

	CCSPtrArray <ProfileListObject> olObjects;
	int ilNumFkts = ogCedaFktData.omData.GetSize();
	for(int ilFkt = 0; ilFkt < ilNumFkts; ilFkt++)
	{
		FKTDATA *prlFkt = &ogCedaFktData.omData[ilFkt];
		if(ipSel == PRINT_ALL ||
		  (ipSel == PRINT_APPL && olFapp == prlFkt->FAPP) || 
		  (ipSel == PRINT_SUBD && olSubd == prlFkt->SUBD && olFapp == prlFkt->FAPP))
		{
			ProfileListObject *prlObj = new ProfileListObject;
			prlObj->Fkt = prlFkt;
			prlObj->Prv = NULL;
			prlObj->Sec = ogCedaSecData.GetSecByUrno(prlFkt->FAPP);
			if(prlObj->Sec != NULL)
				olObjects.Add(prlObj);
			else
				delete prlObj;
		}
	}
	olObjects.Sort(ByApplSubdFual);

	if(m_export == 1)
	{
		try
		{	
			GenerateExcelSheet(olObjects,olTitle2);			
		}		
		catch(CString& ropMessage)
		{
			AfxMessageBox(ropMessage);			
		}
		catch(...)
		{
			AfxMessageBox(UNKNOWN_ERROR_EXCEL);				
		}
	}
	else
	{		
	CString olFooter1,olFooter2;
	CString olTitle1;
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE

	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);
	GetWindowText(olTitle1);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(&ilOrientation) == TRUE)
		{
			int ilLinesPerPage = ilOrientation == PRINT_PORTRAET ? 57 : 38;

			pomPrint->imLineNo = ilLinesPerPage + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = olTitle1;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;

			int ilLines = olObjects.GetSize();
//if(ilLines > 300) ilLines = 300;
			int ilTotalPages = (((int) ilLines-1) / (int) ilLinesPerPage) + 1;
			int ilPageCount = 1;

			pomPrint->imLineNo = 0;
			olFooter1 = CTime::GetCurrentTime().Format("%d.%m.%Y %H:%M");
			for(int ilCurrLine = 0; ilCurrLine < ilLines; ilCurrLine++ ) 
			{
				if(pomPrint->imLineNo >= ilLinesPerPage)
				{
					// end of page reached
					olFooter2.Format("%d/%d",ilPageCount++,ilTotalPages);
					pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
					pomPrint->omCdc.EndPage();
					pomPrint->imLineNo = 0;
				}

				if( pomPrint->imLineNo == 0 )
				{
					// print the header at the start and when the page has changed
					pomPrint->PrintUIFHeader(olTitle1,olTitle2,pomPrint->imFirstLine-10);
				}

				// print a line
				PrintLine(&olObjects[ilCurrLine], ilOrientation);
			}

			olFooter2.Format("%d/%d",ilPageCount++,ilTotalPages);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}

		delete pomPrint;
		pomPrint = NULL;
	}
	}
	olObjects.DeleteAll();
}

void ObjDialog::GenerateExcelSheet(const CCSPtrArray <ProfileListObject>& ropObjects, const CString& ropTitle)
{	
	CTime olCurrentTime = CTime::GetCurrentTime();
	CString olFileName;
	olFileName.Format("%s\\%s_%s.csv", CCSLog::GetTmpPath(),FILENAME ,olCurrentTime.Format("%Y%m%d_%H%M%S"));
		
	CreateExcelSheet(ropObjects,olFileName,ropTitle);
	
	char pclTmp[256];
	strcpy(pclTmp, olFileName); 

   /* Set up parameters to be sent: */
	char *args[4];
	args[0] = "child";
	args[1] = pclTmp;
	args[2] = NULL;
	args[3] = NULL;

	int ilReturn = 0;
	
	if(!m_ExcelPath.IsEmpty())
		ilReturn = _spawnv( _P_NOWAIT , m_ExcelPath, args );	
	else
		throw EXCEL_PATH_NOT_FOUND;

	if(ilReturn == -1)
		throw EXCEL_PATH_NOT_CORRECT;

}

void ObjDialog::CreateExcelSheet(const CCSPtrArray <ProfileListObject>& ropObjects, const CString& ropFileName, const CString& ropTitle)
{
	CString olLine;
	CString olTitle;
	CString olHeader;
	const ProfileListObject* prlObject = NULL;
	
	ofstream of;
	of.open(ropFileName, ios::out);

	//Title
	if(!ropTitle.IsEmpty())
		of << setw(0) << ropTitle << endl;

	olHeader.Format("%s%s%s%s%s%s%s%s%s",
					GetString(IDS_APPLTABLEVIEWER_MODULENAME),m_ExcelSeparator,
					GetString(IDS_SUBDTABLEVIEWER_SUBDIVISION),m_ExcelSeparator,
					GetString(IDS_FUNCTABLEVIEWER_TYPE),m_ExcelSeparator,
					GetString(IDS_FUNCTABLEVIEWER_FUNCTION),m_ExcelSeparator,
					GetString(IDS_FUNCTABLEVIEWER_STATUS));
	
	//Name of the fields
	of  << setw(0) << olHeader << endl;
	
	char pclTypeDesc[100], pclStatDesc[100];
	for(int iLine = 0 ; iLine < ropObjects.GetSize(); iLine++)
	{
		prlObject = &ropObjects[iLine];
		if(prlObject->Prv == NULL)
		{
			ogObjList.GetDesc(prlObject->Fkt->TYPE,prlObject->Fkt->STAT,pclTypeDesc,pclStatDesc);
		}
		else
		{
			ogObjList.GetDesc(prlObject->Fkt->TYPE,prlObject->Prv->STAT,pclTypeDesc,pclStatDesc);
		}

		olLine.Format("%s%s%s%s%s%s%s%s%s",
		              ReplaceSeparator(prlObject->Sec->USID),m_ExcelSeparator,ReplaceSeparator(prlObject->Fkt->SUBD),m_ExcelSeparator,
					  ReplaceSeparator(pclTypeDesc),m_ExcelSeparator,ReplaceSeparator(strlen(prlObject->Fkt->FUAL) == 0 ? prlObject->Fkt->FUNC : prlObject->Fkt->FUAL),m_ExcelSeparator,
					  ReplaceSeparator(pclStatDesc));
		of << setw(0) << olLine << endl;
	}

	of.close();
}

inline CString ObjDialog::ReplaceSeparator(CString opField)
{
	opField.Replace(m_ExcelSeparator," ");	
	return opField;
}

bool ObjDialog::PrintLine(ProfileListObject *prpObj, int ipOrientation)
{
	bool blRc = false;

	// given "B" and "1" return "Button" and "Enabled"
	char pclTypeDesc[100], pclStatDesc[100];
	if(prpObj->Prv == NULL)
	{
		ogObjList.GetDesc(prpObj->Fkt->TYPE,prpObj->Fkt->STAT,pclTypeDesc,pclStatDesc);
	}
	else
	{
		ogObjList.GetDesc(prpObj->Fkt->TYPE,prpObj->Prv->STAT,pclTypeDesc,pclStatDesc);
	}

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	int ilNumChars;

	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	rlElement.Alignment  =  PRINT_LEFT;
	rlElement.FrameTop   =  PRINT_FRAMETHIN;
	rlElement.FrameBottom = PRINT_FRAMETHIN;
	rlElement.FrameLeft  =  PRINT_FRAMETHIN;
	rlElement.FrameRight =  PRINT_FRAMETHIN;

	ilNumChars = ipOrientation == PRINT_PORTRAET ? 20 : 30;
	rlElement.Length = (int)((ilNumChars*5)*dgCCSPrintFactor);
	rlElement.Text = CString(prpObj->Sec->USID);
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	ilNumChars = ipOrientation == PRINT_PORTRAET ? 40 : 70;
	rlElement.Length = (int)((ilNumChars*5)*dgCCSPrintFactor);
	rlElement.Text = CString(prpObj->Fkt->SUBD);
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	ilNumChars = 25;
	rlElement.Length = (int)((ilNumChars*5)*dgCCSPrintFactor);
	rlElement.Text = CString(pclTypeDesc);
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	ilNumChars = ipOrientation == PRINT_PORTRAET ? 60 : 95;
	rlElement.Length = (int)((ilNumChars*5)*dgCCSPrintFactor);
	rlElement.Text = CString(prpObj->Fkt->FUAL);
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	ilNumChars = 20;
	rlElement.Length = (int)((ilNumChars*5)*dgCCSPrintFactor);
	rlElement.Text = CString(pclStatDesc);
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	blRc = true;

	return blRc;
}
