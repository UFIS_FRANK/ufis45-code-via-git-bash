#if !defined(AFX_OBJECTLISTBOX_H__6C9D1C21_5CC8_11D1_A390_0080AD1DC9A3__INCLUDED_)
#define AFX_OBJECTLISTBOX_H__6C9D1C21_5CC8_11D1_A390_0080AD1DC9A3__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ObjectListBox.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CObjectListBox window

class CObjectListBox : public CListBox
{
// Construction
public:
	CObjectListBox(CWnd *popParent);

	CWnd *pomParent;
// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CObjectListBox)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CObjectListBox();

	int GetItemFromPoint(CPoint point);
	// Generated message map functions
protected:
	//{{AFX_MSG(CObjectListBox)
		// NOTE - the ClassWizard will add and remove member functions here.
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OBJECTLISTBOX_H__6C9D1C21_5CC8_11D1_A390_0080AD1DC9A3__INCLUDED_)
