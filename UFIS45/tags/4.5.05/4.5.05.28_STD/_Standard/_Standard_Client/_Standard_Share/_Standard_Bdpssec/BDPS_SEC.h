// BDPS_SEC.h : main header file for the BDPS_SEC application
//
#ifndef __BDPS_SEC_APP__
#define __BDPS_SEC_APP__

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include <resource.h>		// main symbols
#include <CCSGlobl.h>

/////////////////////////////////////////////////////////////////////////////
// CBDPS_SECApp:
// See BDPS_SEC.cpp for the implementation of this class
//

class CBDPS_SECApp : public CWinApp
{
public:

// Data
	char pcmSecCfgFilename[FILENAMELEN];
//	char pcmAdminUsid[USIDLEN]; // usid of system adminstrator, read from ceda.ini, cannot be deleted

	CBDPS_SECApp();
	~CBDPS_SECApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBDPS_SECApp)
	public:
	virtual BOOL InitInstance();
	virtual void WinHelp(DWORD dwData, UINT nCmd = HELP_CONTEXT);
	//}}AFX_VIRTUAL
	void SetLandscape();
	bool InitialLoad();
	void CreateObjects();
	void ReadCfgFile(const char *pcpThisAppl);
	void OnAppAbout();
	bool OnHelpContents();
	bool OnHelpIndex();
	bool OnHelpOnHelp();

// Implementation

	//{{AFX_MSG(CBDPS_SECApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	void AddTableCommand(CString opTable, CString opCommand, int ipDDXType, bool bpUseOwnBc = false,CString opAppl = "");
	void LogBroadcast(struct BcStruct *prpBcData);
	void Trace(char *pcpFormatList, ...);
};


extern SEC_REC_TYPE rgCurrType; // { SEC_USER,SEC_PROFILE etc }


/////////////////////////////////////////////////////////////////////////////
#endif //__BDPS_SEC_APP__