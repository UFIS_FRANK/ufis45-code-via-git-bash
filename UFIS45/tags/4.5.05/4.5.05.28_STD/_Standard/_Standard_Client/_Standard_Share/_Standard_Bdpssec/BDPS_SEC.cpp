// CBDPS_SECApp.cpp : Defines the class behaviors for the application.
//
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include <stdafx.h>
#include <BDPS_SEC.h>
#include <CCSGlobl.h>
#include <LoginDlg.h>
#include <ButtonListDlg.h>
#include <InitialLoadDlg.h>
#include <RegisterDlg.h>

#include <CedaSecData.h>
#include <CedaCalData.h>
#include <CedaGrpData.h>
#include <CedaPrvData.h>
#include <CedaFktData.h>

#include <MessList.h>
#include <ObjList.h>
#include <AboutSecDlg.h>
#include <AatHelp.h>
#include <AatLogin.h>
#include <CedaInitModuData.h>

#include <GUILng.h>
#include <resrc1.h>
/////////////////////////////////////////////////////////////////////////////

//

// CBDPS_SECApp
static void BdpsSecCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

BEGIN_MESSAGE_MAP(CBDPS_SECApp, CWinApp)
	//{{AFX_MSG_MAP(CBDPS_SECApp)
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_HELP, OnHelpContents)
	ON_COMMAND(IDD_HELP_CONTENTS, OnHelpContents)
//	ON_COMMAND(IDD_HELP_INDEX, OnHelpIndex)
	ON_COMMAND(IDD_HELP_USING, CWinApp::OnHelpUsing)
	ON_COMMAND(IDD_ABOUTBOX, OnAppAbout)
END_MESSAGE_MAP()





CBDPS_SECApp::CBDPS_SECApp()
{

} // end constructor

CBDPS_SECApp::~CBDPS_SECApp()
{
	DeleteBrushes();
	delete CGUILng::TheOne();
	AatHelp::ExitApp();

} // end destructor

void CBDPS_SECApp::WinHelp(DWORD dwData, UINT nCmd) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	AatHelp::WinHelp(dwData, nCmd);
}


bool CBDPS_SECApp::OnHelpContents()
{
	AfxGetApp()->WinHelp(0,HELP_CONTENTS);
	return true;
}

bool CBDPS_SECApp::OnHelpIndex()
{
	AfxGetApp()->WinHelp(0,HELP_INDEX);
	return true;
}

bool CBDPS_SECApp::OnHelpOnHelp()
{
	AfxGetApp()->WinHelp(0,HELP_HELPONHELP);
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CBDPS_SECApp object

CBDPS_SECApp theApp;
CButtonListDialog *pogMainScreenDlg;

void  ProcessTrafficLight(long BcNum, int ipState)
{
	if (pogMainScreenDlg != NULL)
	{
		pogMainScreenDlg->UpdateTrafficLight(ipState);
	}
}


/////////////////////////////////////////////////////////////////////////////
// CBDPS_SECApp initialization



void InitLogging(void)
{
	char pclConfigPath[142];
	char pclLogConfig[142];

    // retrieve the configuration path from OS environment
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	// Search the config file specified in pclConfigPath (CEDA.INI)
	// search in the section "CCSLOG" for the Keyword "LOGFILE"
	// return the log file in pclLogConfig
	// parameter 3 is the default if the search is not successful
    GetPrivateProfileString("CCSLOG", "LOGFILE", "C:\\UFIS\\SYSTEM\\PEPLOG.CFG",
            pclLogConfig, sizeof pclLogConfig, pclConfigPath);
	ogLog.Load(pclLogConfig);
	ogLog.Trace("START1","Test Start %d",1);
	ogLog.Trace("START2","Test Start %d",2);

	ogLog.Debug("START1",CCSLog::Most,(const char *)"Test Debug Most Start %d",1);
	ogLog.Debug("START2",CCSLog::Some,(const char *)"Test Debug Some Start %d",2);
	ogLog.Debug("START3",CCSLog::None,(const char *)"Test Debug None Start %d",3);
	ogLog.Error("START1","Test Error Start %d",1);

} // end InitLogging




BOOL CBDPS_SECApp::InitInstance()
{
	if (!AfxOleInit())
	{
		return FALSE;
	}

	AfxEnableControlContainer();

	if (!AatHelp::Initialize(pcgAppName))
	{
		CString olErrorMsg;
		AatHelp::GetLastError(olErrorMsg);
		MessageBox(NULL,olErrorMsg,"Error",MB_OK);
	}

	//SetDialogBkColor();        // Set dialog background color to gray
	LoadStdProfileSettings();  // Load standard INI file options (including MRU)


#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif


	char pclConfigPath[142];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));


	char pclUseBcProxy[256];
	GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclUseBcProxy, sizeof pclUseBcProxy, pclConfigPath);

	//MWO/RRO 25.03.03
	if (stricmp(pclUseBcProxy,"BCPROXY") == 0) 
	{
		ogBcHandle.UseBcProxy();
		ogBcHandle.StopBc();
	}
	else if (stricmp(pclUseBcProxy,"BCCOM") == 0)
	{
		ogBcHandle.UseBcCom();
		ogBcHandle.StopBc();
	}


	// read the configuration file for home airport and message file name
	ReadCfgFile(pcgAppName);

	strncpy(CCSCedaData::pcmTableExt, pcgTableExt,3);
	strncpy(CCSCedaData::pcmHomeAirport, pcgHome,3);
	strcpy(CCSCedaData::pcmApplName, pcgAppName);

	// load error and informational messages into ogMessList
	if( ! ogMessList.Load(pcmSecCfgFilename) )
		AfxMessageBox(CString("Error opening the message file!\n")+ogMessList.pcmLastError);

	// load object (buttons,fields etc) descriptions and statuses
	if( ! ogObjList.Load(pcmSecCfgFilename) )
		AfxMessageBox(CString("Error opening the object description file!\n")+ogObjList.pcmLastError);

	// Check condition of the communication with the server
    if (ogCommHandler.Initialize() != true)  // connection error?
    {
        AfxMessageBox(CString("CedaCom:\n") + ogCommHandler.LastError());
        return FALSE;
    }

	ogBCD.pomCommHandler = &ogCommHandler;
	CGUILng* ogGUILng = CGUILng::TheOne();

	if(ogBasicData.TextTableSupported())
	{
		char pclConfigFile[142];
		if (getenv("CEDA") == NULL)
			strcpy(pclConfigFile, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigFile, getenv("CEDA"));
		char pclTmp[100];
		GetPrivateProfileString("GLOBAL", "LANGUAGE", "", pclTmp, sizeof pclTmp, pclConfigFile);
		ogGUILng->SetLanguage(CString(pclTmp));	
		GetPrivateProfileString(pcgAppName, "DBParam", "", pclTmp, sizeof pclTmp, pclConfigFile);
		if(!strcmp(pclTmp,"1"))
		{
			ogGUILng->SetStoreInDB(CString(pclTmp));
		}
	}


	CString olTableExt = CString(pcgTableExt); olTableExt.TrimRight();
	// the following broadcast is sent once a minte - not actually used by any objects
	// but used to set the broadcast traffic light
	AddTableCommand(CString("TIMERS"), CString("TIME"), BROADCAST_CHECK, true, pcgAppName);

	AddTableCommand(CString("SEC") + olTableExt, CString("IRT"), BC_INSERT_SEC, false, pcgAppName);
	AddTableCommand(CString("SEC") + olTableExt, CString("URT"), BC_UPDATE_SEC, false, pcgAppName);
	AddTableCommand(CString("SEC") + olTableExt, CString("DRT"), BC_DELETE_SEC, false, pcgAppName);

	AddTableCommand(CString("CAL") + olTableExt, CString("IRT"), BC_INSERT_CAL, false, pcgAppName);
	AddTableCommand(CString("CAL") + olTableExt, CString("URT"), BC_UPDATE_CAL, false, pcgAppName);
	AddTableCommand(CString("CAL") + olTableExt, CString("DRT"), BC_DELETE_CAL, false, pcgAppName);

	AddTableCommand(CString("GRP") + olTableExt, CString("IRT"), BC_INSERT_GRP, false, pcgAppName);
	AddTableCommand(CString("GRP") + olTableExt, CString("URT"), BC_UPDATE_GRP, false, pcgAppName);
	AddTableCommand(CString("GRP") + olTableExt, CString("DRT"), BC_DELETE_GRP, false, pcgAppName);

	AddTableCommand(CString("PRV") + olTableExt, CString("IRT"), BC_INSERT_PRV, false, pcgAppName);
	AddTableCommand(CString("PRV") + olTableExt, CString("URT"), BC_UPDATE_PRV, false, pcgAppName);
	AddTableCommand(CString("PRV") + olTableExt, CString("DRT"), BC_DELETE_PRV, false, pcgAppName);

	AddTableCommand(CString("FKT") + olTableExt, CString("IRT"), BC_INSERT_FKT, false, pcgAppName);
	AddTableCommand(CString("FKT") + olTableExt, CString("URT"), BC_UPDATE_FKT, false, pcgAppName);
	AddTableCommand(CString("FKT") + olTableExt, CString("DRT"), BC_DELETE_FKT, false, pcgAppName);

	AddTableCommand(CString("DELSEC"), CString("SEC"), BC_SBC_DELSEC, false, pcgAppName);
	AddTableCommand(CString("REMAPP"), CString("SEC"), BC_SBC_REMAPP, false, pcgAppName);
	AddTableCommand(CString("NEWAPP"), CString("SEC"), BC_SBC_NEWAPP, false, pcgAppName);
	AddTableCommand(CString("SETPRV"), CString("SEC"), BC_SBC_SETPRV, false, pcgAppName);
	AddTableCommand(CString("SETFKT"), CString("SEC"), BC_SBC_SETFKT, false, pcgAppName);
	AddTableCommand(CString("ADDAPP"), CString("SEC"), BC_SBC_ADDAPP, false, pcgAppName);
	AddTableCommand(CString("DELAPP"), CString("SEC"), BC_SBC_DELAPP, false, pcgAppName);
	AddTableCommand(CString("UPDAPP"), CString("SEC"), BC_SBC_UPDAPP, false, pcgAppName);
	AddTableCommand(CString("NEWREL"), CString("SEC"), BC_SBC_NEWREL, false, pcgAppName);
	AddTableCommand(CString("DELREL"), CString("SEC"), BC_SBC_DELREL, false, pcgAppName);
	AddTableCommand(CString("SMI"), CString("SEC"), BC_SBC_SMI, false, pcgAppName);

#if	0
    // allow the user to log in
    CLoginDialog olLoginDlg(pcgOldHome,pcgAppName,pcgWks);

	CString olNewLine("\n");
/*
	olLoginDlg.INVALID_USERNAME = GetString(IDS_INVALID_USERNAME MESS);
	olLoginDlg.INVALID_PASSWORD = GetString(IDS_INVALID_PASSWORD MESS);
	olLoginDlg.EXPIRED_USERNAME = GetString(IDS_EXPIRED_USERNAME MESS) + olNewLine + GetString(IDS_CONTACT_SYS ADMIN);
	olLoginDlg.DISABLED_USERNAME = GetString(IDS_DISABLED_USERNAME MESS) + olNewLine + GetString(IDS_CONTACT_SYS ADMIN);
	olLoginDlg.EXPIRED_PROFILE = GetString(IDS_EXPIRED_PROFILE MESS) + olNewLine + GetString(IDS_CONTACT_SYS ADMIN);
	olLoginDlg.DISABLED_PROFILE = GetString(IDS_DISABLED_PROFILE MESS) + olNewLine + GetString(IDS_CONTACT_SYS ADMIN);
	olLoginDlg.UNDEFINED_PROFILE = GetString(IDS_UNDEFINED_PROFILE MESS) + olNewLine + GetString(IDS_CONTACT_SYS ADMIN);
	olLoginDlg.MESSAGE_BOX_CAPTION = GetString(IDS_MESSAGE_BOX CAPTION);
	olLoginDlg.USERNAME_CAPTION = GetString(IDS_USERNAME_CAPTION);
	olLoginDlg.PASSWORD_CAPTION = GetString(IDS_PASSWORD_CAPTION);
	olLoginDlg.OK_CAPTION = GetString(IDS_OK_CAPTION);
	olLoginDlg.CANCEL_CAPTION = GetString(IDS_CANCEL_CAPTION);
*/
	// display the login mask
	if (olLoginDlg.DoModal() == IDCANCEL)
	{
	    return FALSE;
	}

	ogUsername = olLoginDlg.omUsername;

	// Character mapping !!!
	ogPrivList.ReadUfisCedaConfig();

// uncomment the following to allow registration of SEC
//	else
//	{
//		int ilStartApp = IDOK;
//		if(ogPrivList.GetStat("InitModu") == '1')
//		{
//			RegisterDlg olRegisterDlg;
//			ilStartApp = olRegisterDlg.DoModal();
//		}
//		if(ilStartApp != IDOK)
//		{
//			return FALSE;
//		}
//	}

	bgIsSuperUser = olLoginDlg.bmIsSuperUser;

#else
	CDialog olDummyDlg;	
	olDummyDlg.Create(IDD_WAIT_DIALOG,NULL);
	CAatLogin olLoginCtrl;
	olLoginCtrl.Create(NULL,WS_CHILD|WS_DISABLED,CRect(0,0,100,100),&olDummyDlg,4711);
	olLoginCtrl.SetInfoButtonVisible(TRUE);
	olLoginCtrl.SetApplicationName(pcgAppName);
	olLoginCtrl.SetRegisterApplicationString(ogInitModuData.GetInitModuTxt());

	if (olLoginCtrl.ShowLoginDialog() != "OK")
	{
		olDummyDlg.DestroyWindow();
		return FALSE;
	}

	ogUsername = olLoginCtrl.GetUserName_();
	ogBasicData.omUserID = olLoginCtrl.GetUserName_();
	bgIsSuperUser = olLoginCtrl.GetUserName_() == CString(ogSuperUser);

	ogCommHandler.SetUser(olLoginCtrl.GetUserName_());
	strcpy(CCSCedaData::pcmUser, olLoginCtrl.GetUserName_());
	strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);

	ogPrivList.Add(olLoginCtrl.GetPrivilegList());

	olDummyDlg.DestroyWindow();

#endif
	
	InitLogging(); // read the config file and start writing to log

	
	InitFont();
	CreateBrushes();

    
	
	bool blRc = InitialLoad(); // load database tables - with progress bar

	if( blRc )
	{
		CButtonListDialog olMainScreenDlg;    // create the main screen dialog
	//???	olMainScreenDlg.omAdminUsid = CString(pcmAdminUsid); // name of the system adminstrator (cannot be deleted or it's name changed)
		pogMainScreenDlg = &olMainScreenDlg;  // global pointer to main screen so it can be used in other dialogs
		m_pMainWnd = &olMainScreenDlg;

		// initialize viewer before checking conflicts,but after loading all data !
		ogBcHandle.SetTrafficLightCallBack(ProcessTrafficLight);


		int nResponse = olMainScreenDlg.DoModal();
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;

} // end InitInstance()





bool CBDPS_SECApp::InitialLoad(void)
{
	bool blRc = true;

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	// create, init and display the progress bar
	pogInitialLoad = new InitialLoadDlg();
	CString olErr;
	
	if( blRc )
	{
		blRc = ogCedaSecData.Read();
		if( ! blRc )
		{
			olErr.Format("%s\n(omLastErrorMessage=<%s>)",GetString(IDS_DATABASE_ERROR),ogCedaSecData.omLastErrorMessage);
			AfxMessageBox(olErr);
		}
	}

	if( blRc )
	{
		blRc = ogCedaCalData.Read();
		if( ! blRc )
		{
			olErr.Format("%s\n(omLastErrorMessage=<%s>)",GetString(IDS_DATABASE_ERROR),ogCedaCalData.omLastErrorMessage);
			AfxMessageBox(olErr);
		}
	}

	if( blRc )
	{
		blRc = ogCedaGrpData.Read();
		if( ! blRc )
		{
			olErr.Format("%s\n(omLastErrorMessage=<%s>)",GetString(IDS_DATABASE_ERROR),ogCedaGrpData.omLastErrorMessage);
			AfxMessageBox(olErr);
		}
	}

	if( blRc )
	{
		blRc = ogCedaPrvData.Read();
		if( ! blRc )
		{
			olErr.Format("%s\n(omLastErrorMessage=<%s>)",GetString(IDS_DATABASE_ERROR),ogCedaPrvData.omLastErrorMessage);
			AfxMessageBox(olErr);
		}
	}

	if( blRc )
	{
		blRc = ogCedaFktData.Read();
		if( ! blRc )
		{
			olErr.Format("%s\n(omLastErrorMessage=<%s>)",GetString(IDS_DATABASE_ERROR),ogCedaFktData.omLastErrorMessage);
			AfxMessageBox(olErr);
		}
	}


	pogInitialLoad->SetMessage(GetString(IDS_ALL_TABLES_LOADED));
	pogInitialLoad->DestroyWindow();
	pogInitialLoad = NULL;


	return blRc;

} // end InitialLoad()


void CBDPS_SECApp::SetLandscape()
{
	// Get default printer settings.
	PRINTDLG   pd;
	pd.lStructSize = (DWORD) sizeof(PRINTDLG);
	if (GetPrinterDeviceDefaults(&pd))
	{
		// Lock memory handle.
		DEVMODE FAR* pDevMode =
			(DEVMODE FAR*)::GlobalLock(m_hDevMode);
		if (pDevMode)
		{
			// Change printer settings in here.
			pDevMode->dmOrientation = DMORIENT_LANDSCAPE;

			// Unlock memory handle.
			::GlobalUnlock(m_hDevMode);
		}
	}

} // end SetLandscape()




void CBDPS_SECApp::ReadCfgFile(const char *pcpThisAppl)
{
	char pclConfigFile[142];

    // retrieve the configuration path from OS environment
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigFile, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigFile, getenv("CEDA"));


	// Search the config file specified in pclConfigPath (CEDA.INI)
	// param 1 = section to search for,  param 2 = keyword to search for
	// param 3 = default value, params 4/5 = var to receive the value + size
	// param 6 = config file name

    // get the filename containing a list of all messages for BDPS-SEC
    // and a list object (buttons,fields etc) descriptions and statuses
	GetPrivateProfileString(pcpThisAppl, "INIFILE", "C:\\UFIS\\SYSTEM\\BDPS_SEC.CFG",
							pcmSecCfgFilename, sizeof(pcmSecCfgFilename), pclConfigFile);


	// get the home airport from [BDPS-SEC] segment
	// this is only used so that BDPS-SEC is backwards compatible
    GetPrivateProfileString(pcpThisAppl, "HOMEAIRPORT", "TAB",pcgOldHome, sizeof(pcgOldHome), pclConfigFile);

	char pclValidToDays[20];

	// get the valid to date (days after valid from)
    GetPrivateProfileString(pcpThisAppl, "VALIDTO_DAYS", "365",
							pclValidToDays, sizeof(pclValidToDays), pclConfigFile);

	igValidToDays = atoi(pclValidToDays);
	if(igValidToDays <= 0)
		igValidToDays = 365;


	CTime olCurr = CTime::GetCurrentTime();

	CTime olMax = CTime(2037,12,31,23,29,00);


	CTimeSpan  olSpan = olMax - olCurr;

	int ilMaxDays = olSpan.GetDays();


	if( ilMaxDays < igValidToDays)
		igValidToDays = ilMaxDays;



    GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "TAB", pcgHome, sizeof pcgHome, pclConfigFile);
    GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "", pcgTableExt, sizeof pcgTableExt, pclConfigFile);
	if(!strcmp(pcgTableExt,""))
	{
		strcpy(pcgTableExt,pcgOldHome);
	}
	strcat(pcgTableExt," ");


} // end InitLogging

void CBDPS_SECApp::OnAppAbout()
{
	CAboutSecDlg aboutDlg;
	aboutDlg.DoModal();
}

void CBDPS_SECApp::AddTableCommand(CString opTable, CString opCommand, int ipDDXType, bool bpUseOwnBc /*= false*/,CString opAppl /*= ""*/)
{
	ogBcHandle.AddTableCommand(opTable, opCommand, ipDDXType, bpUseOwnBc, opAppl);
	ogDdx.Register((void *)this, ipDDXType, opTable, opCommand, BdpsSecCf);
}

static void BdpsSecCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
    CBDPS_SECApp *polThis = (CBDPS_SECApp *)popInstance;
	struct BcStruct *prlBcData = (struct BcStruct *) vpDataPointer;
	polThis->LogBroadcast(prlBcData);
}

void CBDPS_SECApp::LogBroadcast(struct BcStruct *prpBcData)
{
	if(prpBcData != NULL)
	{
		Trace("--> BROADCAST RECEIVED Tab <%s> Cmd <%s> Sel <%s>",prpBcData->Object,prpBcData->Cmd,prpBcData->Selection);
		Trace("--> Fields <%s>",prpBcData->Fields);
		Trace("--> Data <%s>",prpBcData->Data);
		if(!prpBcData->Attachment.IsEmpty())
			Trace("--> Attachment <%s>",prpBcData->Attachment);
	}
}

// this is a safe version of TRACE (TRACE crashes when there are > 512 characters)
void CBDPS_SECApp::Trace(char *pcpFormatList, ...)
{
	char pclText[512];
	memset(pclText,'\0',512);
	va_list args;
	va_start(args, pcpFormatList);
	_vsnprintf( pclText, 500, pcpFormatList, args);

	CString olDateAndText;
	CTime olCurrTime = CTime::GetCurrentTime();
	olDateAndText.Format("%s: %s",olCurrTime.Format("%H:%M:%S"),pclText);

//	if(bmLoggingEnabled)
//	{
//		WriteLog(olDateAndText);
//	}
//	AddToDebugInfoLog(olDateAndText);
	TRACE(olDateAndText);
	TRACE("\n");

	va_end(args);
}
