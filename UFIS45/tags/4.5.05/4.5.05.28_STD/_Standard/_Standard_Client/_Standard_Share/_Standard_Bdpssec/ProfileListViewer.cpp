// ProfileListViewer.cpp : implementation file
//
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include <stdafx.h>
#include <CCSGlobl.h>

#include <BDPS_SEC.h>
#include <ProfileListViewer.h>
#include <CedaGrpData.h>
#include <CedaSecData.h>
#include <CedaPrvData.h>
#include <CedaFktData.h>

static int ProfilesBeforeGroups(const GRPDATA **ppp1, const GRPDATA **ppp2);
static int ProfilesBeforeGroups(const GRPDATA **ppp1, const GRPDATA **ppp2)
{
	return strcmp((**ppp2).TYPE,(**ppp1).TYPE);
}

static int ByApplSubdFual(const ProfileListObject **ppp1, const ProfileListObject **ppp2);
static int ByApplSubdFual(const ProfileListObject **ppp1, const ProfileListObject **ppp2)
{
	int ilRc = strcmp((**ppp1).Sec->USID,(**ppp2).Sec->USID);
	if(ilRc == 0)
		ilRc = strcmp((**ppp1).Fkt->SUBD,(**ppp2).Fkt->SUBD);
	if(ilRc == 0)
		ilRc = strcmp((**ppp1).Fkt->FUAL,(**ppp2).Fkt->FUAL);
	return ilRc;
}


#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif



//*************************************************************************************
//
// ProfileListViewer() - constructor
//
//*************************************************************************************

ProfileListViewer::ProfileListViewer()
{
	imProfileListItem = 0;
}


//*************************************************************************************
//
// ProfileListViewer() - destructor
//
//*************************************************************************************

ProfileListViewer::~ProfileListViewer()
{
	DeleteProfileListItem(omProfileList);
}

void ProfileListViewer::DeleteProfileListItem(ProfileListItem &rrpProfileListItem)
{
	int ilNumChildren = rrpProfileListItem.Children.GetSize();
	for(int ilChild = 0; ilChild < ilNumChildren; ilChild++)
	{
		DeleteProfileListItem(rrpProfileListItem.Children[ilChild]);
	}
	rrpProfileListItem.FrelList.RemoveAll();
	rrpProfileListItem.FfktMap.RemoveAll();
	rrpProfileListItem.Children.DeleteAll();
	rrpProfileListItem.Objects.DeleteAll();
	imProfileListItem = 0;
}


void ProfileListViewer::CreateProfileList(const char *pcpFsec)
{
	DeleteProfileListItem(omProfileList);
	omUrnoList.RemoveAll();
	omUrnoList.SetAt(pcpFsec, NULL);

	SECDATA *prlSec = ogCedaSecData.GetSecByUrno(pcpFsec);
	if(prlSec != NULL)
	{
		sprintf(omProfileList.NAME,GetString(IDS_CURRENTRIGHTS),prlSec->USID);
		strcpy(omProfileList.TYPE,"G");
		omProfileList.Level = 0;

		omProfileList.IsValid = false;
		if(*prlSec->STAT != '1')
		{
			sprintf(omProfileList.DESC,GetString(IDS_STATDISABLED),prlSec->USID);
		}
		else if(!ogCedaCalData.DateIsValidForFsec(prlSec->URNO))
		{
			sprintf(omProfileList.DESC,GetString(IDS_OUTOFDATE),prlSec->USID);
		}
		else
		{
			omProfileList.IsValid = true;
		}

		omProfileList.ImageType = omProfileList.IsValid ? VALIDFINALPROFILE : INVALIDFINALPROFILE;

		CreateProfileListItems(pcpFsec,omProfileList.Children, omProfileList.Level+1);
		CreateFrelList(omProfileList,omProfileList.Children);
		CreateObjectList(omProfileList);

		if(omProfileList.IsValid && omProfileList.Children.GetSize() <= 0)
		{
			sprintf(omProfileList.DESC,GetString(IDS_NORIGHTS),prlSec->USID);
			omProfileList.IsValid = false;
		}
	}
}

void ProfileListViewer::CreateObjectList(ProfileListItem &ropParent)
{
	int ilNumFrels = ropParent.FrelList.GetCount();
	if(ilNumFrels > 0)
	{
		CreateObjects(ropParent);
		int ilNumChildren = ropParent.Children.GetSize();
		for(int ilChild = 0; ilChild < ilNumChildren; ilChild++)
		{
			CreateObjectList(ropParent.Children[ilChild]);
		}
	}
}

// create a list of FRELs either for current profile or if group then all profiles below it
//
//	Head									FREL=""		FrelList="1","A","B","B1"
//		Profile1							FREL="1"	FrelList="1"
//		GroupA								FREL=""		FrelList="A"
//			ProfileGroupA					FREL="A"	FrelList="A"
//		GroupB								FREL=""		FrelList="B","B1"
//			ProfileGroupB					FREL="B"	FrelList="B","B1"
//			GroupB1							FREL=""		FrelList="B1"
//				ProfileGroupB1				FREL="B1"	FrelList="B1"
//
void ProfileListViewer::CreateFrelList(ProfileListItem &ropParent, CCSPtrArray <ProfileListItem> &ropChildren)
{
	int ilNumChildren = ropChildren.GetSize();
	for(int ilChild = 0; ilChild < ilNumChildren; ilChild++)
	{
		ProfileListItem *prlChild = &ropChildren[ilChild];
		if(*prlChild->TYPE == 'P') // profile
		{
			if(prlChild->IsValid) // not disabled or out of date
			{
				ropParent.FrelList.SetAt(prlChild->FREL,NULL); // parent
			}
			prlChild->FrelList.SetAt(prlChild->FREL,NULL); // child
		}
		else // group - ie a real group or a virtual one (collection of profiles)
		{
			CreateFrelList(ropParent, prlChild->Children); // create Frel list for parent
			CreateFrelList(*prlChild, prlChild->Children); // create Frel list for child
		}
	}
}

bool ProfileListViewer::NoProfilesDefined()
{
	return omProfileList.Children.GetSize() <= 0 ? true : false;
}

void ProfileListViewer::CreateProfileListItems(const char *pcpFsec, CCSPtrArray <ProfileListItem> &ropChildren, int ipLevel)
{
	if(ipLevel < 10) // insurance against a group being assigned to another group in both directions
	{
		CCSPtrArray <GRPDATA> olRelations;
		ogCedaGrpData.GetGroupsByFsec(pcpFsec,olRelations);
		olRelations.Sort(ProfilesBeforeGroups);
		int ilNumRels = olRelations.GetSize();
		for(int ilRel = 0; ilRel < ilNumRels; ilRel++)
		{
			GRPDATA *prlRel = &olRelations[ilRel];
			omUrnoList.SetAt(prlRel->URNO, NULL);
			SECDATA *prlSec = ogCedaSecData.GetSecByUrno(prlRel->FREL);
			if(prlSec != NULL)
			{
				omUrnoList.SetAt(prlSec->URNO, NULL);
				CString olName;
				ProfileListItem *prlProfileListItem = new ProfileListItem;
				ropChildren.Add(prlProfileListItem);
				prlProfileListItem->Level = ipLevel;

				if(*prlRel->TYPE == 'P') // profile or personal profile
				{
					strcpy(prlProfileListItem->FREL,prlRel->FREL);
					olName.Format(GetString(IDS_PROFILECAPTION),prlSec->USID);
					if(!strcmp(prlRel->FSEC,prlRel->FREL))
					{
						prlProfileListItem->IsPersonalProfile = true;
						olName += GetString(IDS_PERSONALPROFILE);
					}
					strcpy(prlProfileListItem->NAME,olName);
					strcpy(prlProfileListItem->TYPE,"P");

					prlProfileListItem->IsValid = false;
					if(*prlSec->STAT != '1')
					{
						sprintf(prlProfileListItem->DESC,GetString(IDS_PROFILEDISABLED),prlSec->USID);
					}
					else if(!ogCedaCalData.DateIsValidForFsec(prlSec->URNO))
					{
						sprintf(prlProfileListItem->DESC,GetString(IDS_PROFILEOUTOFDATE),prlSec->USID);
					}
					else if(!ogCedaCalData.DateIsValidForFsec(prlRel->URNO))
					{
						SECDATA *prlParent = ogCedaSecData.GetSecByUrno(prlRel->FSEC);
						if(prlParent != NULL)
							sprintf(prlProfileListItem->DESC,GetString(IDS_PROFILEASSOUTOFDATE),prlSec->USID,prlParent->USID);
					}
					else
					{
						prlProfileListItem->IsValid = true;
					}

					prlProfileListItem->ImageType = prlProfileListItem->IsValid ? VALIDPROFILE : INVALIDPROFILE;
				}
				else if(*prlRel->TYPE == 'G') // group
				{
					olName.Format(GetString(IDS_GROUPCAPTION),prlSec->USID);
					strcpy(prlProfileListItem->NAME,olName);
					strcpy(prlProfileListItem->TYPE,"G");
					prlProfileListItem->IsValid = false;
					if(*prlSec->STAT != '1')
					{
						sprintf(prlProfileListItem->DESC,GetString(IDS_GROUPDISABLED),prlSec->USID);
					}
					else if(!ogCedaCalData.DateIsValidForFsec(prlSec->URNO))
					{
						sprintf(prlProfileListItem->DESC,GetString(IDS_GROUPOUTOFDATE),prlSec->USID);
					}
					else if(!ogCedaCalData.DateIsValidForFsec(prlRel->URNO))
					{
						SECDATA *prlParent = ogCedaSecData.GetSecByUrno(prlRel->FSEC);
						if(prlParent != NULL)
							sprintf(prlProfileListItem->DESC,GetString(IDS_GROUPASSOUTOFDATE),prlSec->USID,prlParent->USID);
					}
					else
					{
						prlProfileListItem->IsValid = true;
					}

					prlProfileListItem->ImageType = prlProfileListItem->IsValid ? VALIDGROUP : INVALIDGROUP;

					CreateProfileListItems(prlSec->URNO, prlProfileListItem->Children, ipLevel+1);
				}
			}
		}
	}
	else
	{
		ProfileListItem *prlProfileListItem = new ProfileListItem;
		ropChildren.Add(prlProfileListItem);
		prlProfileListItem->Level = ipLevel;
		strcpy(prlProfileListItem->NAME,"ERROR RECURSION!");
		strcpy(prlProfileListItem->TYPE,"P");
		prlProfileListItem->ImageType = ERRORRECURSION;
	}
}


//*************************************************************************************
//
// Attach() - receives a pointer to the CCSTable
//
//*************************************************************************************

void ProfileListViewer::Attach(CComboBoxEx *popProfileListCtrl)
{
    pomProfileListCtrl = popProfileListCtrl;
}


//*************************************************************************************
//
// ChangeViewTo() - function responsible for updating the display
//
//*************************************************************************************

void ProfileListViewer::ChangeViewTo(const char *pcpFsec)
{
	CreateProfileList(pcpFsec);

	// draw the lines loaded in CCSTable
	UpdateDisplay();
}




//*************************************************************************************
//
// UpdateDisplay()
//
//*************************************************************************************

void ProfileListViewer::UpdateDisplay()
{
	DrawProfileListItem(omProfileList.ImageType, omProfileList.Level, omProfileList.NAME, (long) &omProfileList);
	DrawProfileListItems(omProfileList.Children);
}

void ProfileListViewer::DrawProfileListItems(CCSPtrArray <ProfileListItem> &ropChildren)
{
	int ilNumItems = ropChildren.GetSize();
	for(int ilItem = 0; ilItem < ilNumItems; ilItem++)
	{
		ProfileListItem *prlItem = &ropChildren[ilItem];
		DrawProfileListItem(prlItem->ImageType, prlItem->Level, prlItem->NAME, (long) prlItem);
		DrawProfileListItems(prlItem->Children);
	}
}

bool ProfileListViewer::DrawProfileListItem(ImageType epImageType, int ipIndent, const char *pcpString, LPARAM lpUserData /*0L*/)
{
	CString            olItemText;
	int                ilItem;

	omProfileListItem.mask = CBEIF_IMAGE|CBEIF_INDENT|CBEIF_OVERLAY|CBEIF_SELECTEDIMAGE|CBEIF_TEXT;
	omProfileListItem.iOverlay = 1;
	omProfileListItem.iIndent = (ipIndent & 0x03);   //Set indentation according to item position
	omProfileListItem.iItem = imProfileListItem++;
	olItemText = pcpString;
	omProfileListItem.pszText = (LPTSTR)(LPCTSTR)olItemText;
	omProfileListItem.cchTextMax = olItemText.GetLength();
	omProfileListItem.iImage = epImageType;
	omProfileListItem.iSelectedImage = epImageType; // image displayed in the edit part of the comboBox

	if((ilItem = pomProfileListCtrl->InsertItem(&omProfileListItem)) != -1)
	{
		pomProfileListCtrl->SetItemData(ilItem,lpUserData);
	}

	return ilItem == -1 ? false : true;
}

// create a profile map of all profiles where PRV.FSEC in ropFrelList
void ProfileListViewer::CreateProfile(CMapStringToPtr &ropFrelList, CCSPtrArray <PRVDATA> &ropProfile, CMapStringToPtr &ropFfktMap)
{
	ropFfktMap.RemoveAll();
	ropProfile.RemoveAll();

	CCSPtrArray <PRVDATA> olTmpPrvList;
	bool blFirstProfile = true;
	int ilNumPrvs, ilPrv, ilNumTmpPrvs;
	PRVDATA *prlOldPrv;
	CString olFrel;
	void *pvlTmp;

	for(POSITION rlPos = ropFrelList.GetStartPosition(); rlPos != NULL; )
	{
		ropFrelList.GetNextAssoc(rlPos,olFrel,(void *& )pvlTmp);
		if(blFirstProfile)
		{
			ogCedaPrvData.GetPrvListByFsec(olFrel,ropProfile);
			ilNumPrvs = ropProfile.GetSize();
			for(ilPrv = 0; ilPrv < ilNumPrvs; ilPrv++)
			{
				ropFfktMap.SetAt(ropProfile[ilPrv].FFKT,&ropProfile[ilPrv]);
			}
			blFirstProfile = false;
		}
		else
		{
			// merge profiles
			olTmpPrvList.RemoveAll();
			ogCedaPrvData.GetPrvListByFsec(olFrel,olTmpPrvList);
			ilNumTmpPrvs = olTmpPrvList.GetSize();
			for(ilPrv = 0; ilPrv < ilNumTmpPrvs; ilPrv++)
			{
				PRVDATA *prlNewPrv = &olTmpPrvList[ilPrv];
				if(ropFfktMap.Lookup(prlNewPrv->FFKT,(void *&)prlOldPrv))
				{
					// update the status
					if(prlNewPrv->STAT[0] == '1' || (prlOldPrv->tmpSTAT[0] == '-' && prlNewPrv->STAT[0] == '0'))
					{
						strcpy(prlOldPrv->tmpSTAT,prlNewPrv->STAT);
					}
				}
				else
				{
					// as yet undefined object so add it to the profile
					ropFfktMap.SetAt(prlNewPrv->FFKT,prlNewPrv);
					ropProfile.Add(prlNewPrv);
				}
			}
		}
	}
}


void ProfileListViewer::CreateObjects(ProfileListItem &ropProfileListItem)
{
	CCSPtrArray <PRVDATA> olProfile;
	CreateProfile(ropProfileListItem.FrelList, olProfile, ropProfileListItem.FfktMap);
	int ilNumPrvs = olProfile.GetSize();
	for(int ilPrv = 0; ilPrv < ilNumPrvs; ilPrv++)
	{
		PRVDATA *prlPrv = &olProfile[ilPrv];
		FKTDATA *prlFkt = ogCedaFktData.GetFktByUrno(prlPrv->FFKT);
		SECDATA *prlSec = ogCedaSecData.GetSecByUrno(prlPrv->FAPP);
		if(prlFkt != NULL && prlSec != NULL)
		{
			ProfileListObject *prlObj = new ProfileListObject;
			prlObj->Prv = prlPrv;
			prlObj->Fkt = prlFkt;
			prlObj->Sec = prlSec;
			ropProfileListItem.Objects.Add(prlObj);
		}
	}

	ropProfileListItem.Objects.Sort(ByApplSubdFual);
}

bool ProfileListViewer::UrnoLoaded(const char *pcpUrno)
{
	void *pvlDummy = NULL;
	return omUrnoList.Lookup(pcpUrno,(void *& )pvlDummy) ? true : false;
}