// PrintOption.cpp : implementation file
//

#include "stdafx.h"
#include "bdps_sec.h"
#include "PrintOption.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const CString WINDOW_TEXT = "Print Option";
/////////////////////////////////////////////////////////////////////////////
// CPrintOption dialog


CPrintOption::CPrintOption(CWnd* pParent /*=NULL*/)
	: CDialog(CPrintOption::IDD, pParent)
{
	m_Sel = 0;
	//{{AFX_DATA_INIT(CPrintOption)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CPrintOption::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Radio(pDX, IDC_RADIO_PRINT, m_Sel);	
	
	//{{AFX_DATA_MAP(CPrintOption)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BOOL CPrintOption::OnInitDialog()
{
	CDialog::OnInitDialog();
	SetWindowText(WINDOW_TEXT);
	return TRUE;
}


BEGIN_MESSAGE_MAP(CPrintOption, CDialog)
	//{{AFX_MSG_MAP(CPrintOption)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrintOption message handlers
