// BC_Simple.cpp : Implementation of CBC_SimpleApp and DLL registration.

#include "stdafx.h"
#include "BC_Simple.h"
#include <winsock.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


CBC_SimpleApp NEAR theApp;

const GUID CDECL BASED_CODE _tlid =
		{ 0x35f235e5, 0x333c, 0x4e4a, { 0xb7, 0x46, 0x13, 0x59, 0x39, 0x72, 0x44, 0xef } };
const WORD _wVerMajor = 1;
const WORD _wVerMinor = 0;


////////////////////////////////////////////////////////////////////////////
// CBC_SimpleApp::InitInstance - DLL initialization

BOOL CBC_SimpleApp::InitInstance()
{
	BOOL bInit = COleControlModule::InitInstance();

	if (!AfxSocketInit())
	{
		AfxMessageBox("Socket Init Failed");
		return FALSE;
	}
	if (bInit)
	{
		// TODO: Add your own module initialization code here.
	}

	return bInit;
}


////////////////////////////////////////////////////////////////////////////
// CBC_SimpleApp::ExitInstance - DLL termination

int CBC_SimpleApp::ExitInstance()
{
	// TODO: Add your own module termination code here.

	return COleControlModule::ExitInstance();
}


/////////////////////////////////////////////////////////////////////////////
// DllRegisterServer - Adds entries to the system registry

STDAPI DllRegisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleRegisterTypeLib(AfxGetInstanceHandle(), _tlid))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(TRUE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}


/////////////////////////////////////////////////////////////////////////////
// DllUnregisterServer - Removes entries from the system registry

STDAPI DllUnregisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleUnregisterTypeLib(_tlid, _wVerMajor, _wVerMinor))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(FALSE))
		return ResultFromScode(SELFREG_E_CLASS);

	return NOERROR;
}
