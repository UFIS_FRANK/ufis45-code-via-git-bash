#if !defined(AFX_BCSOCKET_H__111093D2_8661_461D_8C2B_C02CC2C1B2BE__INCLUDED_)
#define AFX_BCSOCKET_H__111093D2_8661_461D_8C2B_C02CC2C1B2BE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BCSocket.h : header file
//

#include <afxsock.h>		// MFC socket extensions

class CBC_SimpleCtrl;
struct STR_DESC
{
	long AllocatedSize;
	long UsedLen;
	char *Value;
};
/////////////////////////////////////////////////////////////////////////////
// BCSocket command target

/////////////////////////////////////////////////////////////////////////////
// BCSocket command target
class CBCTCPIPSimpleCtrl;
class BCSocket : public CAsyncSocket
{
// Attributes
public:

// Operations
public:
	BCSocket(CString opWhoAmI);
	BCSocket(CString opWhoAmI, CString opFilter);
	virtual ~BCSocket();

	CString omWhoAmI;
	CString omCurrentFilter;
	long lmTotalLen;
	CBC_SimpleCtrl *pomDlg;
	STR_DESC rmMsg;
	CString TotalBuffer;
	void Attach(CBC_SimpleCtrl *popDlg)
	{
		pomDlg = popDlg;
	}

// Overrides
public:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(BCSocket)
	public:
	virtual void OnReceive(int nErrorCode);
	virtual void OnConnect(int nErrorCode);
	virtual void OnClose(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(BCSocket)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BCSOCKET_H__111093D2_8661_461D_8C2B_C02CC2C1B2BE__INCLUDED_)
