// BC_SimpleCtl.cpp : Implementation of the CBC_SimpleCtrl ActiveX Control class.

#include "stdafx.h"
#include "BC_Simple.h"
#include "BC_SimpleCtl.h"
#include "BC_SimplePpg.h"
#include "msg.h"
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>
#include "BCSocket.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


ofstream of;


IMPLEMENT_DYNCREATE(CBC_SimpleCtrl, COleControl)


CBC_SimpleCtrl *pomThis;
VOID CALLBACK TimerProc(  HWND hwnd,     // handle of window for timer messages
						  UINT uMsg,     // WM_TIMER message
						  UINT idEvent,  // timer identifier
					      DWORD dwTime   // current system time
)
{
	pomThis->BC_To_Client();
}


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CBC_SimpleCtrl, COleControl)
	//{{AFX_MSG_MAP(CBC_SimpleCtrl)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Dispatch map

BEGIN_DISPATCH_MAP(CBC_SimpleCtrl, COleControl)
	//{{AFX_DISPATCH_MAP(CBC_SimpleCtrl)
	DISP_PROPERTY_NOTIFY(CBC_SimpleCtrl, "AmIConnected", m_amIConnected, OnAmIConnectedChanged, VT_BOOL)
	DISP_PROPERTY_NOTIFY(CBC_SimpleCtrl, "BCConnectionType", m_bCConnectionType, OnBCConnectionTypeChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CBC_SimpleCtrl, "Version", m_version, OnVersionChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CBC_SimpleCtrl, "BuildDate", m_buildDate, OnBuildDateChanged, VT_BSTR)
	DISP_FUNCTION(CBC_SimpleCtrl, "SetFilter", SetFilter, VT_EMPTY, VTS_BSTR VTS_BSTR VTS_BOOL VTS_BSTR)
	DISP_FUNCTION(CBC_SimpleCtrl, "TransmitFilter", TransmitFilter, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION(CBC_SimpleCtrl, "DisconnectFromBcComServer", DisconnectFromBcComServer, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CBC_SimpleCtrl, "ConnectToBcComServer", ConnectToBcComServer, VT_BOOL, VTS_NONE)
	//}}AFX_DISPATCH_MAP
	DISP_FUNCTION_ID(CBC_SimpleCtrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
END_DISPATCH_MAP()


/////////////////////////////////////////////////////////////////////////////
// Event map

BEGIN_EVENT_MAP(CBC_SimpleCtrl, COleControl)
	//{{AFX_EVENT_MAP(CBC_SimpleCtrl)
	EVENT_CUSTOM("OnBc", FireOnBc, VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR)
	//}}AFX_EVENT_MAP
END_EVENT_MAP()


/////////////////////////////////////////////////////////////////////////////
// Property pages

// TODO: Add more property pages as needed.  Remember to increase the count!
BEGIN_PROPPAGEIDS(CBC_SimpleCtrl, 1)
	PROPPAGEID(CBC_SimplePropPage::guid)
END_PROPPAGEIDS(CBC_SimpleCtrl)


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CBC_SimpleCtrl, "BCSIMPLE.BCSimpleCtrl.1",
	0x17bfcd5e, 0xeb01, 0x4b5e, 0x8b, 0xc0, 0x82, 0xc2, 0x54, 0xd, 0x9e, 0xcc)


/////////////////////////////////////////////////////////////////////////////
// Type library ID and version

IMPLEMENT_OLETYPELIB(CBC_SimpleCtrl, _tlid, _wVerMajor, _wVerMinor)


/////////////////////////////////////////////////////////////////////////////
// Interface IDs

const IID BASED_CODE IID_DBC_Simple =
		{ 0x47bc7f9f, 0xba86, 0x4687, { 0xad, 0xa1, 0x3, 0x56, 0xb9, 0xaf, 0x2d, 0x30 } };
const IID BASED_CODE IID_DBC_SimpleEvents =
		{ 0x954d573, 0x1454, 0x45e3, { 0xb3, 0xd1, 0x67, 0x72, 0x6e, 0x20, 0xf3, 0x73 } };


/////////////////////////////////////////////////////////////////////////////
// Control type information

static const DWORD BASED_CODE _dwBC_SimpleOleMisc =
	OLEMISC_INVISIBLEATRUNTIME |
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CBC_SimpleCtrl, IDS_BC_SIMPLE, _dwBC_SimpleOleMisc)


/////////////////////////////////////////////////////////////////////////////
// CBC_SimpleCtrl::CBC_SimpleCtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CBC_SimpleCtrl

BOOL CBC_SimpleCtrl::CBC_SimpleCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Verify that your control follows apartment-model threading rules.
	// Refer to MFC TechNote 64 for more information.
	// If your control does not conform to the apartment-model rules, then
	// you must modify the code below, changing the 6th parameter from
	// afxRegApartmentThreading to 0.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_BC_SIMPLE,
			IDB_BC_SIMPLE,
			afxRegApartmentThreading,
			_dwBC_SimpleOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}


/////////////////////////////////////////////////////////////////////////////
// CBC_SimpleCtrl::CBC_SimpleCtrl - Constructor

CBC_SimpleCtrl::CBC_SimpleCtrl()
{
	InitializeIIDs(&IID_DBC_Simple, &IID_DBC_SimpleEvents);

	pomBCSocket = NULL;
	pomBCOut = NULL;
	bmCedaSenderConnected = false;
	bmTimerIsSet = false;
	char pclConfigPath[256];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

/*	lmCurrWriteCount=0;
	lmCurrFileNo=0;
	CString olFileName;
	olFileName.Format("C:\\tmp\\TMPCL_BC%ld.log", lmCurrFileNo);

	omCurrFileName = olFileName;
*/
    GetPrivateProfileString("GLOBAL", "HOSTNAME", "", sgHostName, sizeof sgHostName, pclConfigPath);

//	of.open(olFileName.GetBuffer(0), ios::out);
}


/////////////////////////////////////////////////////////////////////////////
// CBC_SimpleCtrl::~CBC_SimpleCtrl - Destructor

CBC_SimpleCtrl::~CBC_SimpleCtrl()
{
//	of.close();
	if(pomBCOut != NULL)
	{
		pomBCOut->Close();
		delete pomBCOut;
		pomBCOut = NULL;
	}
	if(pomBCSocket != NULL)
	{
		pomBCSocket->Close();
		delete pomBCSocket;
		pomBCSocket = NULL;
	}
	mapFilter.RemoveAll();
}


/////////////////////////////////////////////////////////////////////////////
// CBC_SimpleCtrl::OnDraw - Drawing function

void CBC_SimpleCtrl::OnDraw(
			CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	CBrush olBlackBrush(COLORREF(RGB(0,0,0)));
	int ilH = rcBounds.bottom - rcBounds.top;
	CString olS;
	olS = "BC_Simple";
	CSize ilTH = pdc->GetTextExtent(olS);
	ilH = (int)(ilH/2);
	ilH -= (int)(ilTH.cy/2);
	pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(LTGRAY_BRUSH)));
	pdc->FrameRect(rcBounds, &olBlackBrush);
	pdc->SetTextColor(COLORREF(RGB(0,0,255)));
	pdc->SetBkMode(TRANSPARENT);
	pdc->TextOut(0, ilH, olS.GetBuffer(0));
}


/////////////////////////////////////////////////////////////////////////////
// CBC_SimpleCtrl::DoPropExchange - Persistence support

void CBC_SimpleCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: Call PX_ functions for each persistent custom property.

}


/////////////////////////////////////////////////////////////////////////////
// CBC_SimpleCtrl::OnResetState - Reset control to default state

void CBC_SimpleCtrl::OnResetState()
{
	COleControl::OnResetState();  // Resets defaults found in DoPropExchange

	// TODO: Reset any other control state here.
}


/////////////////////////////////////////////////////////////////////////////
// CBC_SimpleCtrl::AboutBox - Display an "About" box to the user

void CBC_SimpleCtrl::AboutBox()
{
	CDialog dlgAbout(IDD_ABOUTBOX_BC_SIMPLE);
	dlgAbout.DoModal();
}

char* CBC_SimpleCtrl::GetKeyItem(char *pcpResultBuff, long *plpResultSize, 
				 char *pcpTextBuff, char *pcpKeyWord, 
				 char *pcpItemEnd, bool bpCopyData) 
{ 
	long llDataSize = 0L; 
	char *pclDataBegin = NULL; 
	char *pclDataEnd = NULL; 
	pclDataBegin = strstr(pcpTextBuff, pcpKeyWord); 
	/* Search the keyword */ 
	if (pclDataBegin != NULL) 
	{ 
		/* Did we find it? Yes. */ 
		pclDataBegin += strlen(pcpKeyWord); 
		/* Skip behind the keyword */ 
		pclDataEnd = strstr(pclDataBegin, pcpItemEnd); 
		/* Search end of data */ 
		if (pclDataEnd == NULL) 
		{ 
			/* End not found? */ 
			pclDataEnd = pclDataBegin + strlen(pclDataBegin); 
			/* Take the whole string */ 
		} /* end if */ 
		llDataSize = pclDataEnd - pclDataBegin; 
		/* Now calculate the length */ 
		if (bpCopyData == true) 
		{ 
			/* Shall we copy? */ 
			strncpy(pcpResultBuff, pclDataBegin, llDataSize); 
			/* Yes, strip out the data */ 
		} /* end if */ 
	} /* end if */ 
	if (bpCopyData == true) 
	{ 
		/* Allowed to set EOS? */ 
		pcpResultBuff[llDataSize] = 0x00; 
		/* Yes, terminate string */ 
	} /* end if */ 
	*plpResultSize = llDataSize; 
	/* Pass the length back */ 
	return pclDataBegin; 
	/* Return the data's begin */ 
} /* end GetKeyItem */ 


/////////////////////////////////////////////////////////////////////////////
// CBC_SimpleCtrl message handlers
void CBC_SimpleCtrl::Message(LPCTSTR lpszMessage)
{
	//TO DO
/*	m_BCList.InsertString(0, lpszMessage);
	int ilCount = m_BCList.GetCount();
 	if (ilCount >= 500)
	{
		m_BCList.DeleteString (ilCount - 1);
	}
*/
}

void CBC_SimpleCtrl::SendBCData(char *pclBuf)
{
	omSpooler.Add(pclBuf);

	BC_To_Client();
/*	if (bmTimerIsSet == false)
	{
		::SetTimer(this->m_hWnd, 0, 100, TimerProc);
		bmTimerIsSet = true;
	}
*/
}


void CBC_SimpleCtrl::BC_To_Client()
{
	char *pclDataBegin;
	char pclKeyWord[100];
	char pclResult[25000];
	char *pclAttach=NULL;
	long llAttachSize;
	long llSize;
	CString ReqId,  
			Dest1,  
			Dest2,  
			Cmd,  
			Object,  
			Seq,  
			Tws,  
			Twe,  
			Selection,  
			Fields,  
			Data,  
			BcNum,
			Queue,
			olAttach;

	CString myBCData;
//	::KillTimer(this->m_hWnd, 0);	
//	this->bmTimerIsSet = false;
	if (omSpooler.GetSize() == 0)
	{
		return;
	}
	myBCData = omSpooler[0];
	strcpy(pclKeyWord,"{=QUE=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Queue = CString(pclResult);
		m_bCQueueID = Queue;
	}
	strcpy(pclKeyWord,"{=USR=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		ReqId = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=USR=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Dest1  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=WKS=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Dest2  = CString(pclResult);
		ReqId = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=CMD=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Cmd  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=TBL=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Object  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=XXX=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Seq  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=TWS=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Tws  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=TWE=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Twe  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=WHE=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Selection  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=FLD=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Fields  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=DAT=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		Data  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=BCNUM=}"); 
	pclDataBegin = GetKeyItem(pclResult, &llSize, myBCData.GetBuffer(0), pclKeyWord, "{=", true); 
	if (pclDataBegin != NULL) 
	{ 
		BcNum  = CString(pclResult);
	}
	strcpy(pclKeyWord,"{=ATTACH=}"); 
	pclAttach = GetKeyItem("", &llAttachSize, myBCData.GetBuffer(0), pclKeyWord, "{=\\ATTACH=}", false); 
	if (pclAttach != NULL) 
	{ 
		pclAttach[llAttachSize] = '\0';
		olAttach = CString(pclAttach);
	}

	if (Cmd == "BCOUT" && !Queue.IsEmpty())
	{
		//m_bCQueueID = Queue;
		m_amIConnected = TRUE;
	}
	else
	{
		CString myBC;
		myBC = CString(myBCData.GetBuffer(0));
		//myBC.Replace(",", "\017");
		//MWO Dont log, becaus this makes no sense in this control.
		//of << myBCData.GetBuffer(0) << endl;

/*		lmCurrWriteCount++;
		if(lmCurrWriteCount == 1000)
		{
			lmCurrWriteCount=0;
			of.close();
			lmCurrFileNo++;
			CString olFileName;
			olFileName.Format("C:\\tmp\\TMPCL_BC%ld.log", lmCurrFileNo);
			of.open(olFileName.GetBuffer(0), ios::out);
		}
*/
		//fire out
		FireOnBc( ReqId, Dest1, Dest2, Cmd, Object, Seq, Tws, Twe, Selection, Fields, Data, BcNum, olAttach, "");
	}
	if(omSpooler.GetSize() > 0)
	{
		omSpooler.RemoveAt(0);
	}
}


void CBC_SimpleCtrl::ShutDownSocket(CString opWho)
{
	if( opWho == "BCOFF" || opWho == "BCFILTER")
	{
		pomBCOut->Close();
		delete pomBCOut;
		pomBCOut = NULL;
		Message("ShutDownSocket " + opWho);
	}
}

void CBC_SimpleCtrl::Connect()
{
	if(pomBCSocket == NULL)
	{
		pomBCSocket = new BCSocket("RECEIVER");
		if (pomBCSocket->Create() == TRUE)
		{
			//TO DO correct server and port handling
			pomBCSocket->Connect( sgHostName, 3358);
			pomBCSocket->Attach(this);
			pomBCSocket->Listen();
		}
		else
		{
			//strResult = "FAILED";
			delete pomBCSocket;
			pomBCSocket = NULL;
		}
	}

}


/////////////////////////////////////////////////////////////////////////////
// CBC_SimpleCtrl message handlers

void CBC_SimpleCtrl::OnAmIConnectedChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CBC_SimpleCtrl::OnBCConnectionTypeChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CBC_SimpleCtrl::OnVersionChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CBC_SimpleCtrl::OnBuildDateChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CBC_SimpleCtrl::SetFilter(LPCTSTR strTable, LPCTSTR strCommand, BOOL bOwnBC, LPCTSTR application) 
{
	//CString *polKeyValue = new CString(strTable);

	this->mapFilter.SetAt(strTable, (void*)strTable);
	//omFilter.Add(strTable);

}

void CBC_SimpleCtrl::TransmitFilter() 
{
	CString olNewFilter;
	int i =0;

	for (POSITION pos2 = mapFilter.GetStartPosition(); pos2 != NULL;)
	{
		CString olString;
		CString *p = NULL;
		mapFilter.GetNextAssoc(pos2,olString,(void *&)p);
		if (!olString.IsEmpty())
			olNewFilter += olString + ";";
	}

/*	for( i = 0; i < omFilter.GetSize(); i++)
	{
		olNewFilter += omFilter[i] + ";";
	}
*/
	if(olNewFilter.GetLength() > 0)
	{
		olNewFilter = olNewFilter.Left(olNewFilter.GetLength()-1);
	}

	if(pomBCOut == NULL)
	{
		pomBCOut = new BCSocket("BCFILTER", olNewFilter);
		if (pomBCOut->Create() == TRUE)
		{
			//TO DO correct server and port handling
			pomBCOut->Connect( sgHostName, 3358);
			pomBCOut->Attach(this);
		}
		else
		{
			//strResult = "FAILED";
			delete pomBCOut;
			pomBCOut = NULL;
		}
	}
}

BOOL CBC_SimpleCtrl::DisconnectFromBcComServer() 
{
	// TODO: Add your dispatch handler code here

	return TRUE;
}

BOOL CBC_SimpleCtrl::ConnectToBcComServer() 
{
	Connect();

	return TRUE;
}
