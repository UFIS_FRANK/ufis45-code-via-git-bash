﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ufis.Utilities
{
    /// <summary>
    /// Represents the class for information on the project and customer.
    /// </summary>
    public class ProjectInfo
    {
        /// <summary>
        /// Gets or sets the project code.
        /// </summary>
        public string ProjectName { get; set; }
        /// <summary>
        /// Gets or sets the customer name.
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// Initializes a new instance of Ufis.Utilities.ProjectInfo class.
        /// </summary>
        public ProjectInfo() { }

        /// <summary>
        /// Initializes a new instance of Ufis.Utilities.ProjectInfo class 
        /// and populate the information based on configuration value.
        /// </summary>
        /// <param name="configString"></param>
        public ProjectInfo(string configString)
        {
            if (string.IsNullOrEmpty(configString))
                return;

            string[] arrProjectInfos = configString.Split('-');
            if (arrProjectInfos.Length < 2)
                return;

            ProjectName = arrProjectInfos[0];
            CustomerName = arrProjectInfos[1];
        }
    }
}
