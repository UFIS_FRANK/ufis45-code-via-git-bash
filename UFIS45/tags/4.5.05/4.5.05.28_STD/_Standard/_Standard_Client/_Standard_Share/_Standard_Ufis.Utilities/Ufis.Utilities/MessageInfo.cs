﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ufis.Utilities
{
    /// <summary>
    /// Represents the class for message and its severity status.
    /// </summary>
    public class MessageInfo
    {
        /// <summary>
        /// Enumeration for severity of the message.
        /// </summary>
        public enum MessageInfoSeverity
        {
            Information,
            Warning,
            Error,
            OK
        }

        /// <summary>
        /// Gets or sets the message text.
        /// </summary>
        public string InfoText { get; set; }
        /// <summary>
        /// Gets or sets the severity of the message.
        /// </summary>
        public MessageInfoSeverity Severity { get; set; }

        /// <summary>
        /// Initializes a new instance of Ufis.Utilities.MessageInfo class.
        /// </summary>
        public MessageInfo() { }
    }
}
