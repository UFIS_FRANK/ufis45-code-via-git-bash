VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form frmJobDetails 
   Caption         =   "Job Details"
   ClientHeight    =   10560
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7080
   LinkTopic       =   "Form1"
   ScaleHeight     =   10560
   ScaleWidth      =   7080
   StartUpPosition =   3  'Windows Default
   Begin MSComDlg.CommonDialog dlgSave 
      Left            =   4680
      Top             =   2520
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdJobExport 
      Caption         =   "Export to Excel"
      Height          =   375
      Left            =   2160
      TabIndex        =   4
      Top             =   2640
      Width           =   2295
   End
   Begin VB.Frame Frame1 
      Caption         =   "Event Details"
      Height          =   6975
      Left            =   120
      TabIndex        =   2
      Top             =   3360
      Width           =   6855
      Begin TABLib.TAB tab_EVENTDETAILS 
         Height          =   6255
         Left            =   240
         TabIndex        =   3
         Top             =   360
         Width           =   6255
         _Version        =   65536
         _ExtentX        =   11033
         _ExtentY        =   11033
         _StockProps     =   64
      End
   End
   Begin TABLib.TAB tab_EVENT 
      Height          =   2295
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   6855
      _Version        =   65536
      _ExtentX        =   12091
      _ExtentY        =   4048
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_R02TABEVENT 
      Height          =   495
      Left            =   4680
      TabIndex        =   1
      Tag             =   "{=TABLE=}R02TAB{=FIELDS=}ORNO,FLST,FVAL,USEC,TIME,SFKT,URNO"
      Top             =   960
      Width           =   2055
      _Version        =   65536
      _ExtentX        =   3625
      _ExtentY        =   873
      _StockProps     =   64
   End
End
Attribute VB_Name = "frmJobDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


'#######################################################################
'### SHOW THE JOB CHANGES FOR SELECTED JOB
'#######################################################################
Sub showJobEventDetails(jobOrnoSelected As String)
    
    Dim test As String
    Dim l As Integer
    Dim strFields As String
    Dim strValues As String
    Dim strLine As String
    Dim strQuery As String
    Dim strTemp As String
    Dim i As Integer
    Dim tmpVal As String
    Dim strKey As String
    
    
    
    'If frmJobDetails form closed then while showing call the initial settings
    If initFlag = False Then InitEventTabs
    
    If (jobOrnoSelected <> "") Then
        
        MousePointer = vbHourglass
        
        'strQuery = "where orno = '" & jobOrnoSelected & "'"
        
        If (jobOrnoSelected = "0") Then
            strQuery = globalR02Query & " and orno = '0'"
        Else
            strQuery = "where orno = '" & jobOrnoSelected & "'"
        End If
        
        
        tab_R02TABEVENT.ResetContent
        frmData.LoadData tab_R02TABEVENT, strQuery
        tab_R02TABEVENT.Visible = False
        
        tab_EVENT.ResetContent
        tab_EVENTDETAILS.ResetContent
        
        For l = 0 To tab_R02TABEVENT.GetLineCount - 1 Step 1
            
            'ornoTemp = tab_R02TABEVENT.GetColumnValues(l, 0)
        
            '### LOG COLUMNS
            strFields = tab_R02TABEVENT.GetColumnValues(l, 1)
            strFields = Replace(strFields, Chr(30), ",")
            strValues = tab_R02TABEVENT.GetColumnValues(l, 2)
            strValues = Replace(strValues, Chr(30), ",")
                       
            'strLine = User value
            'strLine = tab_R02TABEVENT.GetColumnValues(l, 3)
            
            'code change start
            strkey1 = tab_R02TABEVENT.GetColumnValues(l, 3)
            If strkey1 = "" Or UCase(strkey1) = "EVENTSPOOL" Then
                '### TAKE USEU
                strkey1 = GetFieldValue("USEU", strValues, strFields)
                '### STILL EMPTY TAKE USEC
                If strkey1 = "" Then
                    strkey1 = GetFieldValue("USEC", strValues, strFields)
                End If
                '### STILL EMPTY PUT DEFAULT
                If strkey1 = "" Then
                    strkey1 = "User not available"
                End If
            End If
            If strkey1 = "polhdlr" Then strkey1 = "System"
            strLine = strkey1
            
            
            '### TIME
            strLine = strLine + "," & DateAdd("h", gUTCOffset, CedaFullDateToVb(tab_R02TABEVENT.GetColumnValues(l, 4)))
            strLine = strLine + "," + tab_R02TABEVENT.GetColumnValues(l, 5)
            'strTemp = SortedR02Fields(strValues, strFields)
            'strLine = strLine + "," + strTemp
            
            
            
            For i = 3 To gR02items Step 1
                strKey = gR02TAB(i)
                tmpVal = GetFieldValue(strKey, strValues, strFields)
                
                'If InStr(1, "TIME", strKey) > 0 Then
                '    If tmpVal <> "" Then
                '        tmpVal = CedaFullDateToVb(tmpVal)
                '    End If
                'End If
                
                If InStr(1, "TIME,ACFR,ACTO,PLFR,PLTO,LSTU,CDAT", strKey) > 0 Then
                    If tmpVal <> "" Then
                        tmpVal = DateAdd("h", gUTCOffset, CedaFullDateToVb(tmpVal))
                    End If
                End If
                
                'UTPL - URNO Template
                If strKey = "UTPL" Then
                    tmpVal = frmMain.tab_TPLTAB.GetLinesByColumnValue(0, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmMain.tab_TPLTAB.GetColumnValue(CLng(tmpVal), 1)
                End If
                
                'UJTY - URNO JOBTYPE
                If strKey = "UJTY" Then
                    tmpVal = frmMain.tab_JTYTAB1.GetLinesByColumnValue(0, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmMain.tab_JTYTAB1.GetColumnValue(CLng(tmpVal), 1)
                End If
                
                'USTF - Urno Staff - get Staff info & show
                If strKey = "USTF" Then
                    tmpVal = frmMain.tab_STFCOMPLETE.GetLinesByColumnValue(1, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmMain.tab_STFCOMPLETE.GetColumnValue(CLng(tmpVal), 0)
                End If
                
                'UGHS - Service Name
                If strKey = "UGHS" Then
                    tmpVal = frmMain.tab_SERTAB.GetLinesByColumnValue(0, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmMain.tab_SERTAB.GetColumnValue(CLng(tmpVal), 1)
                End If
                
                'UALO - Allocation
                If strKey = "UALO" Then
                    tmpVal = frmData.tab_ALOTAB.GetLinesByColumnValue(0, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmData.tab_ALOTAB.GetColumnValue(CLng(tmpVal), 1)
                End If
                
                If strKey = "UAFT" Then
                    tmpVal = frmMain.tab_AFTTABall.GetLinesByColumnValue(0, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmMain.tab_AFTTABall.GetColumnValue(CLng(tmpVal), 1)
                End If
                
                strLine = strLine + "," + tmpVal
                
            Next i
            
            'Add the record to Log tab
            tab_EVENT.InsertTextLine strLine, False
               
        Next l
            
        tab_EVENT.ShowHorzScroller True
        tab_EVENT.AutoSizeColumns
        'tab_JOBLOG.Sort 1, True, True
        tab_EVENT.EnableHeaderSizing True
        tab_EVENT.Refresh
        
        MousePointer = vbDefault
        
    End If
    
End Sub


'#######################################################################
'### INIT TABLES FOR SHIFT DETAILS FORM
'#######################################################################
Sub InitEventTabs()

    frmData.InitTabGeneral tab_R02TABEVENT
    frmData.InitTabForCedaConnection tab_R02TABEVENT
    tab_R02TABEVENT.AutoSizeColumns
    
    tab_EVENT.ResetContent
    tab_EVENT.HeaderString = gR02FieldDescList
    tab_EVENT.AutoSizeColumns
    tab_EVENT.Refresh
    
    tab_EVENTDETAILS.ResetContent
    tab_EVENTDETAILS.HeaderString = "FIELD,DATA"
    tab_EVENTDETAILS.HeaderLengthString = "250,250"
    tab_EVENTDETAILS.FontName = "Arial"
    tab_EVENTDETAILS.FontSize = 15
    'tab_EVENTDETAILS.AutoSizeColumns
    tab_EVENTDETAILS.Refresh
    
    initFlag = True
    
End Sub

Private Sub cmdJobExport_Click()
    
    Dim l As Integer
    Dim i As Integer
    Dim llLineCount As Integer
    Dim strTemp As String
    Dim FileNumber
    
    dlgSave.DialogTitle = "Save as Excel"
    dlgSave.Filter = "Excel (CSV)|*.csv"
    dlgSave.FileName = "JobEvents"
    dlgSave.ShowSave
    
    If dlgSave.FileName <> "" Then
        
        l = tab_EVENT.GetLineCount
        
        If l < 1 Then
            MsgBox "No Data to export.", vbExclamation
            Exit Sub
        End If
        
        FileNumber = FreeFile   ' Get unused file number.
        Open dlgSave.FileName For Output As #FileNumber    ' Create file name.
        Print #FileNumber, gR02FieldDescList
        
        llLineCount = tab_EVENT.GetLineCount
        
        For l = 0 To llLineCount - 1 Step 1
            strTemp = ""
            For i = 0 To gR02items - 1 Step 1
                strTemp = strTemp & tab_EVENT.GetColumnValues(l, i) & ","
            Next i
            Print #FileNumber, strTemp
        Next l
        
        Close #FileNumber
        
    Else
        MsgBox ("Please enter filename to export")
    End If
    
End Sub

Private Sub tab_EVENT_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    
    Dim l As Integer
    Dim i As Integer
    Dim strValuesCurrent As String
    Dim strFieldsCurrent As String
    Dim strValuesPrev As String
    Dim strPrev As String
    Dim strFieldsPrev As String
    Dim strKey As String
    Dim tmpVal As String
    Dim tmpValPrev As String
    
    l = tab_EVENT.GetCurrentSelected
    
    If l <> -1 Then
        
        MousePointer = vbHourglass
        
        tab_EVENTDETAILS.ResetContent
        
        For i = 1 To gR02items
            tab_EVENTDETAILS.SetColumnValue i, 0, GetItem(gR02FieldDescList, i, ",")
        Next i
        
        'Assign compulsory values
        tab_EVENTDETAILS.SetColumnValue 0, 1, tab_R02TABEVENT.GetColumnValues(l, 3)
        tab_EVENTDETAILS.SetColumnValue 1, 1, DateAdd("h", gUTCOffset, CedaFullDateToVb(tab_R02TABEVENT.GetColumnValues(l, 4)))
        tab_EVENTDETAILS.SetColumnValue 2, 1, tab_R02TABEVENT.GetColumnValues(l, 5)
        
        ' Need to check whether any updates for each field or not? if so change the color of the fields.
        If (l <> 0) Then
        
            strFieldsCurrent = tab_R02TABEVENT.GetColumnValues(l, 1)
            strFieldsCurrent = Replace(strFieldsCurrent, Chr(30), ",")
            strValuesCurrent = tab_R02TABEVENT.GetColumnValues(l, 2)
            strValuesCurrent = Replace(strValuesCurrent, Chr(30), ",")
            
            strFieldsPrev = tab_R02TABEVENT.GetColumnValues(l - 1, 1)
            strFieldsPrev = Replace(strFieldsPrev, Chr(30), ",")
            strValuesPrev = tab_R02TABEVENT.GetColumnValues(l - 1, 2)
            strValuesPrev = Replace(strValuesPrev, Chr(30), ",")
            
            For i = 3 To gR02items Step 1
                strKey = gR02TAB(i)
                tmpVal = GetFieldValue(strKey, strValuesCurrent, strFieldsCurrent)
                tmpValPrev = GetFieldValue(strKey, strValuesPrev, strFieldsPrev)
                
                
                If tmpVal <> tmpValPrev And tmpVal <> "" Then tab_EVENTDETAILS.SetLineColor i, H000080FF, &HC0C0FF
                
                'If InStr(1, "TIME", strKey) > 0 Then
                '    If tmpVal <> "" Then
                '        tmpVal = CedaFullDateToVb(tmpVal)
                '    End If
                'End If
                
                If InStr(1, "TIME,ACFR,ACTO,PLFR,PLTO,LSTU,CDAT", strKey) > 0 Then
                    If tmpVal <> "" Then
                        tmpVal = DateAdd("h", gUTCOffset, CedaFullDateToVb(tmpVal))
                    End If
                End If
                
                'UTPL - URNO Template
                If strKey = "UTPL" Then
                    tmpVal = frmMain.tab_TPLTAB.GetLinesByColumnValue(0, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmMain.tab_TPLTAB.GetColumnValue(CLng(tmpVal), 1)
                End If
                
                'UJTY - URNO JOBTYPE
                If strKey = "UJTY" Then
                    tmpVal = frmMain.tab_JTYTAB1.GetLinesByColumnValue(0, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmMain.tab_JTYTAB1.GetColumnValue(CLng(tmpVal), 1)
                End If
                
                'USTF - Urno Staff - get Staff info & show
                If strKey = "USTF" Then
                    tmpVal = frmMain.tab_STFCOMPLETE.GetLinesByColumnValue(1, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmMain.tab_STFCOMPLETE.GetColumnValue(CLng(tmpVal), 0)
                End If
                
                'UGHS - Service Name
                If strKey = "UGHS" Then
                    tmpVal = frmMain.tab_SERTAB.GetLinesByColumnValue(0, tmpVal, 0)
                    If tmpVal <> "" And tmpVal <> "0" Then tmpVal = frmMain.tab_SERTAB.GetColumnValue(CLng(tmpVal), 1)
                End If
                
                'UALO - Allocation
                If strKey = "UALO" Then
                    tmpVal = frmData.tab_ALOTAB.GetLinesByColumnValue(0, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmData.tab_ALOTAB.GetColumnValue(CLng(tmpVal), 1)
                End If
                
                If strKey = "UAFT" Then
                    tmpVal = frmMain.tab_AFTTABall.GetLinesByColumnValue(0, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmMain.tab_AFTTABall.GetColumnValue(CLng(tmpVal), 1)
                End If
                
                tab_EVENTDETAILS.SetColumnValue i, 1, tmpVal
                
            Next i
            
        Else
        
            strFieldsCurrent = tab_R02TABEVENT.GetColumnValues(l, 1)
            strFieldsCurrent = Replace(strFieldsCurrent, Chr(30), ",")
            strValuesCurrent = tab_R02TABEVENT.GetColumnValues(l, 2)
            strValuesCurrent = Replace(strValuesCurrent, Chr(30), ",")
        
            For i = 3 To gR02items Step 1
                strKey = gR02TAB(i)
                tmpVal = GetFieldValue(strKey, strValuesCurrent, strFieldsCurrent)
                
                'If InStr(1, "TIME", strKey) > 0 Then
                '    If tmpVal <> "" Then
                '        tmpVal = CedaFullDateToVb(tmpVal)
                '    End If
                'End If
                
                If InStr(1, "TIME,ACFR,ACTO,PLFR,PLTO,CDAT,LSTU", strKey) > 0 Then
                    If tmpVal <> "" Then
                        tmpVal = DateAdd("h", gUTCOffset, CedaFullDateToVb(tmpVal))
                    End If
                End If
                
                'UTPL - URNO Template
                If strKey = "UTPL" Then
                    tmpVal = frmMain.tab_TPLTAB.GetLinesByColumnValue(0, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmMain.tab_TPLTAB.GetColumnValue(CLng(tmpVal), 1)
                End If
                
                'UJTY - URNO JOBTYPE
                If strKey = "UJTY" Then
                    tmpVal = frmMain.tab_JTYTAB1.GetLinesByColumnValue(0, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmMain.tab_JTYTAB1.GetColumnValue(CLng(tmpVal), 1)
                End If
                
                'USTF - Urno Staff - get Staff info & show
                If strKey = "USTF" Then
                    tmpVal = frmMain.tab_STFCOMPLETE.GetLinesByColumnValue(1, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmMain.tab_STFCOMPLETE.GetColumnValue(CLng(tmpVal), 0)
                End If
                
                'UGHS - Service Name
                If strKey = "UGHS" Then
                    tmpVal = frmMain.tab_SERTAB.GetLinesByColumnValue(0, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmMain.tab_SERTAB.GetColumnValue(CLng(tmpVal), 1)
                End If
                
                'UALO - Allocation
                If strKey = "UALO" Then
                    tmpVal = frmData.tab_ALOTAB.GetLinesByColumnValue(0, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmData.tab_ALOTAB.GetColumnValue(CLng(tmpVal), 1)
                End If
                
                If strKey = "UAFT" Then
                    tmpVal = frmMain.tab_AFTTABall.GetLinesByColumnValue(0, tmpVal, 0)
                    If tmpVal <> "" Then tmpVal = frmMain.tab_AFTTABall.GetColumnValue(CLng(tmpVal), 1)
                End If
                
                tab_EVENTDETAILS.SetColumnValue i, 1, tmpVal
                
            Next i
            
            'For i = 1 To gR02items - 2
            '    tab_EVENTDETAILS.SetColumnValue i + 2, 1, GetItem(strValues, i, ",")
            'Next i
        
        End If
            
        'tab_EVENTDETAILS.InsertBuffer
        
        tab_EVENTDETAILS.Refresh
        MousePointer = vbDefault
        
        
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    initFlag = False
End Sub
