using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace Ufis.LoginWindow
{
    /// <summary>
    /// Converts Ufis.LoginWindow.LoginDialogType to System.Windows.Visibility.
    /// </summary>
    [ValueConversion(typeof(LoginDialogType), typeof(Visibility))]
    public class LoginDialogTypeToVisibilityConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            string[] arrParams = parameter.ToString().Split(' ');
            string strLoginDialogType = arrParams[0];
            string strVisibility = arrParams[1];

            LoginDialogType loginDialogType = LoginDialogType.LoginDialog;
            switch (strLoginDialogType)
            {
                case "ChangePasswordDialog":
                    loginDialogType = LoginDialogType.ChangePasswordDialog;
                    break;
                case "RegisterModuleDialog":
                    loginDialogType = LoginDialogType.RegisterModuleDialog;
                    break;
                default:
                    //Do nothing
                    break;
            }

            Visibility visibility = Visibility.Visible;
            Visibility defVisibility = Visibility.Collapsed;
            switch (strVisibility)
            {
                case "Collapsed":
                    visibility = Visibility.Collapsed;
                    defVisibility = Visibility.Visible;
                    break;
                case "Hidden":
                    visibility = Visibility.Hidden;
                    defVisibility = Visibility.Visible;
                    break;
                default:
                    //Do nothing
                    break;
            }

            if (value != null)
            {
                LoginDialogType currentLoginDialogType = (LoginDialogType)value;

                if (loginDialogType == currentLoginDialogType)
                    defVisibility = visibility;
            }

            return defVisibility;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
