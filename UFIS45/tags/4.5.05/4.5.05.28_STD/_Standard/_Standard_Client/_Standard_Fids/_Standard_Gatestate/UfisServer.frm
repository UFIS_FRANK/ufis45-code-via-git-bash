VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form UfisServer 
   Caption         =   "Connection to UFIS Server (AODB)"
   ClientHeight    =   7335
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11160
   LinkTopic       =   "Form1"
   ScaleHeight     =   7335
   ScaleWidth      =   11160
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtFldRtyp 
      Height          =   375
      Left            =   9585
      TabIndex        =   20
      Top             =   1125
      Width           =   1185
   End
   Begin VB.TextBox txtGateType 
      Height          =   330
      Left            =   7965
      TabIndex        =   18
      Top             =   1170
      Width           =   1320
   End
   Begin VB.TextBox RemarkText 
      Height          =   285
      Left            =   6000
      TabIndex        =   12
      Top             =   600
      Width           =   1095
   End
   Begin VB.TextBox UTCorLocal 
      Height          =   285
      Left            =   4440
      TabIndex        =   11
      Top             =   600
      Width           =   1095
   End
   Begin VB.TextBox Offset2 
      Height          =   285
      Left            =   3000
      TabIndex        =   9
      Top             =   600
      Width           =   1095
   End
   Begin VB.TextBox Offset1 
      Height          =   285
      Left            =   1680
      TabIndex        =   8
      Top             =   600
      Width           =   1095
   End
   Begin VB.TextBox Gate 
      Height          =   285
      Left            =   240
      TabIndex        =   7
      Top             =   600
      Width           =   1095
   End
   Begin VB.TextBox ModName 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4140
      TabIndex        =   6
      ToolTipText     =   "CEDA Table Extension"
      Top             =   0
      Width           =   1425
   End
   Begin VB.TextBox HOPO 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2880
      TabIndex        =   5
      ToolTipText     =   "Home Airport (3LC)"
      Top             =   0
      Width           =   615
   End
   Begin VB.TextBox TblExt 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3510
      TabIndex        =   4
      ToolTipText     =   "CEDA Table Extension"
      Top             =   0
      Width           =   615
   End
   Begin VB.TextBox HostName 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   0
      TabIndex        =   3
      ToolTipText     =   "Host Name"
      Top             =   0
      Width           =   1395
   End
   Begin VB.TextBox HostType 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1410
      TabIndex        =   2
      ToolTipText     =   "Host Type"
      Top             =   0
      Width           =   1395
   End
   Begin TABLib.TAB DataBuffer 
      Height          =   1665
      Index           =   3
      Left            =   0
      TabIndex        =   0
      Top             =   4080
      Width           =   3645
      _Version        =   65536
      _ExtentX        =   6429
      _ExtentY        =   2937
      _StockProps     =   0
   End
   Begin TABLib.TAB DataBuffer 
      Height          =   1635
      Index           =   4
      Left            =   3720
      TabIndex        =   1
      Top             =   4080
      Width           =   3915
      _Version        =   65536
      _ExtentX        =   6906
      _ExtentY        =   2884
      _StockProps     =   0
      Columns         =   15
   End
   Begin TABLib.TAB DataBuffer 
      Height          =   1635
      Index           =   5
      Left            =   3720
      TabIndex        =   10
      Top             =   5760
      Width           =   3915
      _Version        =   65536
      _ExtentX        =   6906
      _ExtentY        =   2884
      _StockProps     =   0
   End
   Begin TABLib.TAB DataBuffer 
      Height          =   1635
      Index           =   6
      Left            =   -240
      TabIndex        =   13
      Top             =   5640
      Width           =   3915
      _Version        =   65536
      _ExtentX        =   6906
      _ExtentY        =   2884
      _StockProps     =   0
   End
   Begin TABLib.TAB DataBuffer 
      Height          =   1305
      Index           =   0
      Left            =   0
      TabIndex        =   14
      Top             =   1080
      Width           =   3645
      _Version        =   65536
      _ExtentX        =   6429
      _ExtentY        =   2302
      _StockProps     =   0
   End
   Begin TABLib.TAB DataBuffer 
      Height          =   1665
      Index           =   1
      Left            =   3600
      TabIndex        =   15
      Top             =   2520
      Width           =   3645
      _Version        =   65536
      _ExtentX        =   6429
      _ExtentY        =   2937
      _StockProps     =   0
   End
   Begin TABLib.TAB DataBuffer 
      Height          =   1665
      Index           =   2
      Left            =   0
      TabIndex        =   16
      Top             =   2400
      Width           =   3645
      _Version        =   65536
      _ExtentX        =   6429
      _ExtentY        =   2937
      _StockProps     =   0
   End
   Begin TABLib.TAB DataBuffer 
      Height          =   1635
      Index           =   7
      Left            =   7680
      TabIndex        =   17
      Top             =   5760
      Width           =   3915
      _Version        =   65536
      _ExtentX        =   6906
      _ExtentY        =   2884
      _StockProps     =   0
   End
   Begin VB.Label Label2 
      Caption         =   "FLDRTYP:"
      Height          =   330
      Left            =   9585
      TabIndex        =   21
      Top             =   765
      Width           =   1050
   End
   Begin VB.Label Label1 
      Caption         =   "GateType:"
      Height          =   285
      Left            =   8010
      TabIndex        =   19
      Top             =   810
      Width           =   1140
   End
End
Attribute VB_Name = "UfisServer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private WithEvents bcProx As BcProxyLib.BcPrxy
Attribute bcProx.VB_VarHelpID = -1

Private Type BusyIndicatorType
    Left As Long
    Top As Long
    LineBusy As Variant
    LogOn As Variant
    LogOff As Variant
    NoLine As Variant
End Type

Private BusyIndicator As BusyIndicatorType
Private Initialized As Boolean
Private BcCook As Long

Public Function ConnectToCeda() As Boolean
    Dim sServer As String
    Dim sHopo As String
    Dim sTableExt As String
    Dim sConnectType As String
    Dim ret As Integer
    
    If frmSplash.ProgressBar1.Value < frmSplash.ProgressBar1.Max Then
        frmSplash.List1.AddItem ("Connecting to CEDA ...")
        frmSplash.List1.Refresh
        frmSplash.ProgressBar1.Value = frmSplash.ProgressBar1.Value + 1
    End If
    CedaIsConnected = False

    frmSplash.UfisCom1.CleanupCom

    sServer = HostName.Text
    sHopo = HOPO.Text
    sTableExt = TblExt.Text
    sConnectType = "CEDA"

    If ogActualUser = "" Then
        frmSplash.UfisCom1.SetCedaPerameters "GATESTATE", sHopo, sTableExt
    Else
        frmSplash.UfisCom1.SetCedaPerameters ogActualUser, sHopo, sTableExt
    End If

    If sServer <> "LOCAL" Then
        ret = frmSplash.UfisCom1.InitCom(sServer, sConnectType)
    End If
    If ret = 0 Then
        MsgBox "Connection to CEDA failed!", vbCritical, "No connection!"
        End
    Else
        CedaIsConnected = True
    End If

    ConnectToCeda = CedaIsConnected
End Function

Public Sub ConnectToBcProxy()
    If CedaIsConnected Then
        Set bcProx = New BcPrxy
    End If
End Sub

Public Function CallCeda(CedaResult As String, _
        RouterCmd As String, TableName As String, FieldList As String, DataList As String, _
        WhereClause As String, OrderKey As String, UseBuffer As Integer, _
        FullAnswer As Boolean, AutoRequest As Boolean) As Integer
    
    Dim RetCode As Integer
    Dim Result As String
    Dim tmpCmd As String
    Dim tmpTable As String
    Dim tmpFields As String
    Dim tmpData As String
    Dim tmpSqlKey As String
    Dim CedaAnswer As String
    Dim LineCount As Long
    Dim CurRec As Long
    Dim CurLine As String
    Dim ReqHint As String
    
    tmpCmd = RouterCmd
    tmpTable = TableName
    tmpFields = FieldList
    tmpData = CleanNullValues(DataList)
    tmpSqlKey = WhereClause

    If CedaIsConnected Then
        If OrderKey <> "" Then tmpSqlKey = tmpSqlKey & " ORDER BY " & OrderKey
        If (UseBuffer >= 0) And (UseBuffer < DataBuffer.Count) Then
            ShowIndicator 1, True

            frmSplash.UfisCom1.ClearDataBuffer
            frmSplash.UfisCom1.HomeAirport = HOPO.Text
            frmSplash.UfisCom1.TableExt = TblExt.Text
            frmSplash.UfisCom1.Module = ModName.Text
            frmSplash.UfisCom1.Twe = ""
            frmSplash.UfisCom1.UserName = ogActualUser
            CedaAnswer = frmSplash.UfisCom1.CallServer(tmpCmd, tmpTable, tmpFields, tmpData, tmpSqlKey, "")
            
            LineCount = frmSplash.UfisCom1.GetBufferCount - 1
            If LineCount >= 0 Then
                'DataBuffer(UseBuffer).SetHeaderText tmpFields & ","
                DataBuffer(UseBuffer).HeaderString = tmpFields
                For CurRec = 0 To LineCount
                    CurLine = frmSplash.UfisCom1.GetBufferLine(CurRec)
                    DataBuffer(UseBuffer).InsertTextLine CurLine, False
                Next
                Result = "BUFFER" & UseBuffer & ",OK"
                If FullAnswer Then
                    Result = ""
                    For CurRec = 0 To LineCount
                        Result = Result & DataBuffer(UseBuffer).GetLineValues(CurRec) & vbNewLine
                    Next
                    Result = Left(Result, Len(Result) - 2)  'Cut the last vbNewLine
                    DataBuffer(UseBuffer).ResetContent
                End If
                RetCode = 0
            Else
                ReqHint = WhereClause
                RetCode = CC_RC_NOT_FOUND
            End If
            ShowIndicator 1, False
        Else
            ReqHint = "ERROR 1005: Invalid Data Buffer. (" & UseBuffer & ")"
            RetCode = CC_RC_WRONG_BUFFER
        End If
    End If
    If RetCode <> 0 Then Result = AutoDialog(RetCode, AutoRequest, ReqHint)
    CedaResult = Result
    CallCeda = RetCode
End Function
Private Function AutoDialog(RetCode As Integer, AutoRequest As Boolean, HintText As String) As String
    Dim PopUpWin As Boolean
    Dim ReqText As String
    Dim ReqHint As String
    Dim ReqTip As String
    Dim ReqButton As String
    Dim ReqIcon As String
    Dim MyAnswer As String
    
    UserAnswer = ""
    PopUpWin = True
    Select Case RetCode
        Case -3
            ReqText = "Action rejected by server:" & vbNewLine & HintText
            ReqHint = "Configured Server Name: " & HostName
            ReqTip = "Server: " & HostName
            ReqButton = ""
            ReqIcon = "stop"
            MyAnswer = "ERROR 1003: " & ReqText & "(" & HostName & ") "
        Case CC_RC_LOST_CONNECTION
            ReqText = "Lost connection."
            ReqHint = "Configured Server Name: " & HostName
            ReqTip = "Server: " & HostName
            ReqButton = ""
            ReqIcon = "netfail"
            MyAnswer = "ERROR 1002: " & ReqText & "(" & HostName & ") "
        Case CC_RC_NOT_CONNECTED
            ReqText = "Not connected !"
            ReqHint = "Configured Server Name: " & HostName
            ReqTip = "Server: " & HostName
            ReqButton = ""
            ReqIcon = "netfail"
            MyAnswer = "MSG 1001: " & ReqText & " (Server:" & HostName & ") "
        Case CC_RC_NOT_FOUND
            PopUpWin = AutoRequest
            ReqText = "No data found for this view or filter."
            ReqHint = HintText
            ReqTip = "SQL WhereClause ?"
            ReqButton = ""
            ReqIcon = "hand"
            MyAnswer = ReqText
        Case Else
            ReqText = HintText
            ReqButton = ""
            ReqIcon = "stop"
            MyAnswer = ReqText
    End Select
    AutoDialog = MyAnswer & vbNewLine & UserAnswer
End Function

Public Function ResetBuffer(UseBuffer As Integer) As Boolean
    If (UseBuffer >= 0) And (UseBuffer < DataBuffer.Count) Then
        DataBuffer(UseBuffer).ResetContent
        ResetBuffer = True
    Else
        ResetBuffer = False
    End If
End Function

Private Sub bcProx_OnBcReceive(ByVal ReqId As String, ByVal DestName As String, ByVal RecvName As String, ByVal CedaCmd As String, ByVal ObjName As String, ByVal Seq As String, ByVal Tws As String, ByVal Twe As String, ByVal CedaSqlKey As String, ByVal Fields As String, ByVal Data As String, ByVal BcNum As String)
    HandleBroadCast ReqId, DestName, RecvName, CedaCmd, ObjName, Seq, Tws, Twe, CedaSqlKey, Fields, Data, BcNum
End Sub

Private Sub Form_Load()
    CedaIsConnected = False
    Initialized = False
    InitMyForm
    ConnectToCeda
End Sub

Private Sub InitMyForm()
    HOPO.Text = GetIniEntry("", "GLOBAL", "", "HOMEAIRPORT", "???")
    TblExt.Text = GetIniEntry("", "GLOBAL", "", "TABLEEXTENSION", "TAB")
    HostName.Text = GetIniEntry("", "GLOBAL", "GATESTATE", "HOSTNAME", "UNKNOWN")
    HostType.Text = GetIniEntry("", "GLOBAL", "GATESTATE", "HOSTTYPE", "UNKNOWN")
    Gate.Text = GetIniEntry("", "GLOBAL", "GATESTATE", "GATE", "10")
    Offset1.Text = GetIniEntry("", "GLOBAL", "GATESTATE", "OFFSET_PREV", "-120")
    Offset2.Text = GetIniEntry("", "GLOBAL", "GATESTATE", "OFFSET_AFT", "120")
    UTCorLocal.Text = GetIniEntry("", "GLOBAL", "GATESTATE", "LOCAL", "FALSE")
    RemarkText.Text = GetIniEntry("", "GLOBAL", "GATESTATE", "FIDS_TEXT", "BEME")
    txtGateType.Text = GetIniEntry("", "GLOBAL", "GATESTATE", "GATETYP", "INT") 'INT or DOM
    txtFldRtyp.Text = GetIniEntry("", "GLOBAL", "GATESTATE", "FLDRTYP", "GTE")

    HomeAirport = HOPO.Text
    ModName.Text = "GATESTATE"
End Sub

Public Sub ShowIndicator(Index As Integer, SetVisible As Boolean)
    Dim Obj As Variant
    With BusyIndicator
        Select Case Index
            Case 1
                If .LineBusy <> Empty Then Set Obj = .LineBusy
            Case 2
                If .NoLine <> Empty Then Set Obj = .NoLine
            Case 3
                If .LogOn <> Empty Then Set Obj = .LogOn
            Case 4
                If .LogOff <> Empty Then Set Obj = .LogOff
        End Select
        If Obj <> Empty Then
            Obj.Left = .Left
            Obj.Top = .Top
            If SetVisible = True And Index = 1 Then
                If Obj.BackColor <> DarkGreen Then Obj.BackColor = DarkGreen Else Obj.BackColor = DarkestGreen
            End If
            Obj.Visible = SetVisible
            If SetVisible Then
                Obj.ZOrder
                Obj.Refresh
            End If
        End If
    End With
End Sub

Public Sub SetIndicator(PosLeft As Long, PosTop As Long, _
                        LineBusyFlag As Variant, NoLineFlag As Variant, _
                        LogOnFlag As Variant, LogOffFlag As Variant)
    With BusyIndicator
        .Left = PosLeft
        .Top = PosTop
        If LineBusyFlag <> Empty Then Set .LineBusy = LineBusyFlag
        If NoLineFlag <> Empty Then Set .NoLine = NoLineFlag
        If LogOnFlag <> Empty Then Set .LogOn = LogOnFlag
        If LogOffFlag <> Empty Then Set .LogOff = LogOffFlag
    End With
End Sub

Function GetCommandLine(Optional MaxArgs)

   Dim C, CmdLine, CmdLnLen, InArg, i, NumArgs
   Const CONNECTED_ARG = "/CONNECTED"
   'See if MaxArgs was provided.
   If IsMissing(MaxArgs) Then MaxArgs = 1
   'Make array of the correct size.
   ReDim argArray(MaxArgs)
   NumArgs = 0: InArg = False
   'Get command line arguments.
   CmdLine = Command()
   CmdLnLen = Len(CmdLine)
   'Go thru command line one character
   'at a time.
   If CmdLnLen = 0 Then
      GetCommandLine = False
      Exit Function
   End If
   For i = 1 To CmdLnLen
      C = Mid(CmdLine, i, 1)
      'Test for space or tab.
      If (C <> " " And C <> vbTab) Then
         'Neither space nor tab.
         'Test if already in argument.
         If Not InArg Then
         'New argument begins.
         'Test for too many arguments.
            If NumArgs = MaxArgs Then Exit For
            NumArgs = NumArgs + 1
            InArg = True
         End If
         'Concatenate character to current argument.
         argArray(NumArgs) = argArray(NumArgs) & C
      Else
         'Found a space or tab.
         'Set InArg flag to False.
         InArg = False
      End If
   Next i
   'Resize array just enough to hold arguments.
   ReDim Preserve argArray(NumArgs)
   'Return Array in Function name.
   If argArray(1) = CONNECTED_ARG Then
    GetCommandLine = True
   Else
    GetCommandLine = False
   End If
End Function
