<?php
/*********************************************************************************
 *       Filename: S_TMPL_MENUSRecord.php
 *       Generated with CodeCharge 1.1.16
 *       PHP & Templates build 03/28/2001
 *********************************************************************************/

include ("./common.php");
include ("./Admin.php");

session_start();

$filename = "S_TMPL_MENUSRecord.php";



check_security(3);

$tpl = new Template($app_path);
$tpl->load_file("S_TMPL_MENUSRecord.html", "main");
$tpl->load_file($header_filename, "Header");

$tpl->set_var("FileName", $filename);


$sS_TMPL_MENUS1Err = "";

$sAction = get_param("FormAction");
$sForm = get_param("FormName");
switch ($sForm) {
  case "S_TMPL_MENUS1":
    S_TMPL_MENUS1_action($sAction);
  break;
}Administration_show();
S_TMPL_MENUS_show();
S_TMPL_MENUS1_show();

$tpl->parse("Header", false);

$tpl->pparse("main", false);

//********************************************************************************



function S_TMPL_MENUS_show()
{

  
  global $tpl;
  global $db;
  global $sS_TMPL_MENUSErr;
  $sWhere = "";
  $sOrder = "";
  $sSQL = "";
  $HasParam = false;

  
  $tpl->set_var("TransitParams", "");
  $tpl->set_var("FormParams", "");
  // Build WHERE statement
  

  $sDirection = "";
  $sSortParams = "";
  
  // Build ORDER statement
  $sOrder = " order by S.MENUNAME Asc";
  $iSort = get_param("FormS_TMPL_MENUS_Sorting");
  $iSorted = get_param("FormS_TMPL_MENUS_Sorted");
  if(!$iSort)
  {
    $tpl->set_var("Form_Sorting", "");
  }
  else
  {
    if($iSort == $iSorted)
    {
      $tpl->set_var("Form_Sorting", "");
      $sDirection = " DESC";
      $sSortParams = "FormS_TMPL_MENUS_Sorting=" . $iSort . "&FormS_TMPL_MENUS_Sorted=" . $iSort . "&";
    }
    else
    {
      $tpl->set_var("Form_Sorting", $iSort);
      $sDirection = " ASC";
      $sSortParams = "FormS_TMPL_MENUS_Sorting=" . $iSort . "&FormS_TMPL_MENUS_Sorted=" . "&";
    }
    
    if ($iSort == 1) $sOrder = " order by S.MENUNAME" . $sDirection;
  }

  // Build full SQL statement
  
  $sSQL = "select S.MENUNAME as S_MENUNAME, " . 
    "S.TMPL_MENUID as S_TMPL_MENUID " . 
    " from S_TMPL_MENUS S ";
  
  $sSQL .= $sWhere . $sOrder;
  $tpl->set_var("FormAction", "S_TMPL_MENUSRecord.php");
  $tpl->set_var("SortParams", $sSortParams);

  // Execute SQL statement
  $db->query($sSQL);
  
  // Select current page
  $iPage = get_param("FormS_TMPL_MENUS_Page");
  if(!strlen($iPage)) $iPage = 1;
  $RecordsPerPage = 10;
  if(($iPage - 1) * $RecordsPerPage != 0)
    $db->seek(($iPage - 1) * $RecordsPerPage);
  $iCounter = 0;

  if($db->next_record())
  {  
    // Show main table based on SQL query
    do
    {
      $fldMENUNAME = $db->f("S_MENUNAME");
      $tpl->set_var("MENUNAME", tohtml($fldMENUNAME));
      $tpl->set_var("MENUNAME_URLLink", "S_TMPL_MENUSRecord.php");
      $tpl->set_var("Prm_TMPL_MENUID", tourl($db->f("S_TMPL_MENUID"))); 
      $tpl->parse("DListS_TMPL_MENUS", true);
      $iCounter++;
    } while($iCounter < $RecordsPerPage &&$db->next_record());
  }
  else
  {
    // No Records in DB
    $tpl->set_var("DListS_TMPL_MENUS", "");
    $tpl->parse("S_TMPL_MENUSNoRecords", false);
    $tpl->set_var("S_TMPL_MENUSScroller", "");
    $tpl->parse("FormS_TMPL_MENUS", false);
    return;
  }
  
  // Parse scroller
  if(@$db->next_record())
  {
    if ($iPage == 1)
    {
      $tpl->set_var("S_TMPL_MENUSScrollerPrevSwitch", "_");
    }
    else
    {
      $tpl->set_var("PrevPage", ($iPage - 1));
      $tpl->set_var("S_TMPL_MENUSScrollerPrevSwitch", "");
    }
    $tpl->set_var("NextPage", ($iPage + 1));
    $tpl->set_var("S_TMPL_MENUSScrollerNextSwitch", "");
    $tpl->set_var("S_TMPL_MENUSCurrentPage", $iPage);
    $tpl->parse("S_TMPL_MENUSScroller", false);
  }
  else
  {
    if ($iPage == 1)
    {
      $tpl->set_var("S_TMPL_MENUSScroller", "");
    }
    else
    {
      $tpl->set_var("S_TMPL_MENUSScrollerNextSwitch", "_");
      $tpl->set_var("PrevPage", ($iPage - 1));
      $tpl->set_var("S_TMPL_MENUSScrollerPrevSwitch", "");
      $tpl->set_var("S_TMPL_MENUSCurrentPage", $iPage);
      $tpl->parse("S_TMPL_MENUSScroller", false);
    }
  }
  $tpl->set_var("S_TMPL_MENUSNoRecords", "");
  $tpl->parse("FormS_TMPL_MENUS", false);
  
}



function S_TMPL_MENUS1_action($sAction)
{
  global $db;
  global $tpl;
  global $sForm;
  global $sS_TMPL_MENUS1Err;
  
  $sParams = "";
  $sActionFileName = "S_TMPL_MENUSRecord.php";

  

  $sWhere = "";
  $bErr = false;

  if($sAction == "cancel")
    header("Location: " . $sActionFileName); 

  
  // Create WHERE statement
  if($sAction == "update" || $sAction == "delete") 
  {
    $pPKTMPL_MENUID = get_param("PK_TMPL_MENUID");
    if( !strlen($pPKTMPL_MENUID)) return;
    $sWhere = "TMPL_MENUID=" . tosql($pPKTMPL_MENUID, "Number");
  }

  // Load all form fields into variables
  
  $fldMENUNAME = get_param("MENUNAME");

  //20070221 GFO: WAW Project: Second Language Support
  $fldMENUNAME_TR = get_param("MENUNAME_TR");
  
  $fldROWNUMBER = get_param("ROWNUMBER");
  $fldQUERY = get_param("QUERY");
  $fldPAGETITLE = get_param("PAGETITLE");
  $fldROWSPERPAGE = get_param("ROWSPERPAGE");
  $fldREFRESHRATE = get_param("REFRESHRATE");
  $fldHLCYCLE = get_param("HLCYCLE");
  $fldTFVAL1 = get_param("TFVAL1");
  $fldTFVAL2 = get_param("TFVAL2");
  $fldSHOWTIMEVALUES = get_param("SHOWTIMEVALUES");
  $fldORGDES = get_param("ORGDES");
  // Validate fields
  if($sAction == "insert" || $sAction == "update") 
  {
    if(!is_number($fldROWNUMBER))
      $sS_TMPL_MENUS1Err .= "The value in field Row Number is incorrect.<br>";
    if(!strlen($fldMENUNAME))
      $sS_TMPL_MENUS1Err .= "No Name for Menu set! Please correct.<br>";
    if(!strlen($fldQUERY))
      $sS_TMPL_MENUS1Err .= "No Query for Menu set! Please correct.<br>";
    

    if(strlen($sS_TMPL_MENUS1Err)) return;
  }
  

  $sSQL = "";
  // Create SQL statement
  
  switch(strtolower($sAction)) 
  {
    case "insert":
        //20070221 GFO: WAW Project: Second Language Support
        $sSQL = "insert into S_TMPL_MENUS (" . 
          "MENUNAME," . 
          "ROWNUMBER," . 
          "QUERY," . 
          "PAGETITLE," . 
          "ROWSPERPAGE," . 
          "REFRESHRATE," . 
          "HLCYCLE," . 
          "TFVAL1," . 
          "TFVAL2," . 
          "SHOWTIMEVALUES," . 
          "ORGDES,
		  MENUNAME_TR)" . 
          " values (" . 
          tosql($fldMENUNAME, "Text") . "," .
          tosql($fldROWNUMBER, "Number") . "," .
          tosql($fldQUERY, "Text") . "," .
          tosql($fldPAGETITLE, "Text") . "," .
          tosql($fldROWSPERPAGE, "Number") . "," .
          tosql($fldREFRESHRATE, "Number") . "," .
          tosql($fldHLCYCLE, "Number") . "," .
          tosql($fldTFVAL1, "Number") . "," .
          tosql($fldTFVAL2, "Number") . "," .
          tosql($fldSHOWTIMEVALUES, "Number") . "," .
          tosql($fldORGDES, "Text") . "," .
		  tosql($fldMENUNAME_TR, "Text") . ")";   
		   
    break;
    case "update":
        //20070221 GFO: WAW Project: Second Language Support
        $sSQL = "update S_TMPL_MENUS set " .
          "MENUNAME=" . tosql($fldMENUNAME, "Text") .
          ",ROWNUMBER=" . tosql($fldROWNUMBER, "Number") .
          ",QUERY=" . tosql($fldQUERY, "Text") .
          ",PAGETITLE=" . tosql($fldPAGETITLE, "Text") .
          ",ROWSPERPAGE=" . tosql($fldROWSPERPAGE, "Number") .
          ",REFRESHRATE=" . tosql($fldREFRESHRATE, "Number") .
          ",HLCYCLE=" . tosql($fldHLCYCLE, "Number") .
          ",TFVAL1=" . tosql($fldTFVAL1, "Number") .
          ",TFVAL2=" . tosql($fldTFVAL2, "Number") .
          ",SHOWTIMEVALUES=" . tosql($fldSHOWTIMEVALUES, "Number") .
          ",ORGDES=" . tosql($fldORGDES, "Text") .
		  ",MENUNAME_TR=" . tosql($fldMENUNAME_TR, "Text");
        $sSQL .= " where " . $sWhere;
    break;
    case "delete":
      
        $sSQL = "delete from S_TMPL_MENUS where " . $sWhere;
    break;
  }
	//echo "SQL:(".$sSQL.")";
  // Execute SQL statement
  if(strlen($sS_TMPL_MENUS1Err)) return;
  $db->query($sSQL);
  //echo $sSQL;
  header("Location: " . $sActionFileName);
  
}

function S_TMPL_MENUS1_show()
{
  global $db;
  global $tpl;
  global $sAction;
  global $sForm;
  global $sS_TMPL_MENUS1Err;

  $sWhere = "";
  
  $bPK = true;
  $fldTMPL_MENUID = "";
  $fldMENUNAME = "";
  $fldROWNUMBER = "";
  $fldQUERY = "";
  $fldPAGETITLE = "";
  $fldROWSPERPAGE = "";
  $fldREFRESHRATE = "";
  $fldHLCYCLE = "";
  $fldTFVAL1 = "";
  $fldTFVAL2 = "";
  $fldSHOWTIMEVALUES = "";
  $fldORGDES = "";
  
  //20070221 GFO: WAW Project: Second Language Support
  $fldMENUNAME_TR = "";
  
  
  if($sS_TMPL_MENUS1Err == "")
  {
    // Load primary key and form parameters
    $pTMPL_MENUID = get_param("TMPL_MENUID");
    $tpl->set_var("S_TMPL_MENUS1Error", "");
  }
  else
  {
    // Load primary key, form parameters and form fields
    $fldTMPL_MENUID = strip(get_param("TMPL_MENUID"));
    $fldMENUNAME = strip(get_param("MENUNAME"));
	
	//20070221 GFO: WAW Project: Second Language Support
	
	$fldMENUNAME_TR = strip(get_param("MENUNAME_TR"));
	
    $fldROWNUMBER = strip(get_param("ROWNUMBER"));
    $fldQUERY = strip(get_param("QUERY"));
    $fldPAGETITLE = strip(get_param("PAGETITLE"));
    $fldROWSPERPAGE = strip(get_param("ROWSPERPAGE"));
    $fldREFRESHRATE = strip(get_param("REFRESHRATE"));
    $fldHLCYCLE = strip(get_param("HLCYCLE"));
    $fldTFVAL1 = strip(get_param("TFVAL1"));
    $fldTFVAL2 = strip(get_param("TFVAL2"));
    $fldSHOWTIMEVALUES = strip(get_param("SHOWTIMEVALUES"));
    $fldORGDES = strip(get_param("ORGDES"));
    $pTMPL_MENUID = get_param("PK_TMPL_MENUID");
    $tpl->set_var("sS_TMPL_MENUS1Err", $sS_TMPL_MENUS1Err);
    $tpl->parse("S_TMPL_MENUS1Error", false);
  }

  
  if( !strlen($pTMPL_MENUID)) $bPK = false;
  
  $sWhere .= "TMPL_MENUID=" . tosql($pTMPL_MENUID, "Number");
  $tpl->set_var("PK_TMPL_MENUID", $pTMPL_MENUID);

  $sSQL = "select * from S_TMPL_MENUS where " . $sWhere;

  

  if($bPK && !($sAction == "insert" && $sForm == "S_TMPL_MENUS1"))
  {
    // Execute SQL statement
    $db->query($sSQL);
    $db->next_record();
    
    $fldTMPL_MENUID = $db->f("TMPL_MENUID");
    if($sS_TMPL_MENUS1Err == "") 
    {
      // Load data from recordset when form displayed first time
      $fldMENUNAME = $db->f("MENUNAME");
	  
	  //20070221 GFO: WAW Project: Second Language Support
      $fldMENUNAME_TR = $db->f("MENUNAME_TR");
	  
      $fldROWNUMBER = $db->f("ROWNUMBER");
      $fldQUERY = $db->f("QUERY");
      $fldPAGETITLE = $db->f("PAGETITLE");
      $fldROWSPERPAGE = $db->f("ROWSPERPAGE");
      $fldREFRESHRATE = $db->f("REFRESHRATE");
      $fldHLCYCLE = $db->f("HLCYCLE");
      $fldTFVAL1 = $db->f("TFVAL1");
      $fldTFVAL2 = $db->f("TFVAL2");
      $fldSHOWTIMEVALUES = $db->f("SHOWTIMEVALUES");
	//echo $fldSHOWTIMEVALUES;
      $fldORGDES = $db->f("ORGDES");
    }
    $tpl->set_var("S_TMPL_MENUS1Insert", "");
    $tpl->parse("S_TMPL_MENUS1Edit", false);
  }
  else
  {
    $tpl->set_var("S_TMPL_MENUS1Edit", "");
    $tpl->parse("S_TMPL_MENUS1Insert", false);
  }
  $tpl->parse("S_TMPL_MENUS1Cancel", false);

  // Show form field
  
    $tpl->set_var("TMPL_MENUID", tohtml($fldTMPL_MENUID));
    $tpl->set_var("MENUNAME", tohtml($fldMENUNAME));
	$tpl->set_var("MENUNAME_TR", tohtml($fldMENUNAME_TR));
    $tpl->set_var("ROWNUMBER", tohtml($fldROWNUMBER));
    $tpl->set_var("QUERY", tohtml($fldQUERY));
    $tpl->set_var("PAGETITLE", tohtml($fldPAGETITLE));
    $tpl->set_var("ROWSPERPAGE", tohtml($fldROWSPERPAGE));
    $tpl->set_var("REFRESHRATE", tohtml($fldREFRESHRATE));
    $tpl->set_var("HLCYCLE", tohtml($fldHLCYCLE));
		switch ($fldHLCYCLE) 
		{
			case "0":
    		$tpl->set_var("HLCYCLECHECKED_0","CHECKED");
				break;
			case "1":
    		$tpl->set_var("HLCYCLECHECKED_1","CHECKED");
				break;
			case "2":
    		$tpl->set_var("HLCYCLECHECKED_2","CHECKED");
				break;
			case "3":
    		$tpl->set_var("HLCYCLECHECKED_3","CHECKED");
				break;
			case "4":
    		$tpl->set_var("HLCYCLECHECKED_4","CHECKED");
				break;
			case "5":
    		$tpl->set_var("HLCYCLECHECKED_5","CHECKED");
				break;
			default:
				break;
	 }
   $tpl->set_var("TFVAL1", tohtml($fldTFVAL1));
   $tpl->set_var("TFVAL2", tohtml($fldTFVAL2));
   $tpl->set_var("SHOWTIMEVALUES", tohtml($fldSHOWTIMEVALUES));
		switch ($fldSHOWTIMEVALUES) 
		{
			case "0":
    		$tpl->set_var("SHOWTIMEVALUESCHECKED_0","CHECKED");
				break;
			case "1":
    		$tpl->set_var("SHOWTIMEVALUESCHECKED_1","CHECKED");
				break;
			default:
				break;
	 }
   $tpl->set_var("ORGDES", tohtml($fldORGDES));
 	 $tpl->parse("FormS_TMPL_MENUS1", false);
}

?>
