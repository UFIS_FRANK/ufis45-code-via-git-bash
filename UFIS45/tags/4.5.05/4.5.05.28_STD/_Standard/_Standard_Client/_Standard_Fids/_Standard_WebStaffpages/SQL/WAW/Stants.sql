/* Formatted on 2007/05/11 15:24 (Formatter Plus v4.8.7) */
SELECT  /*+ ALL_ROWS */  afttab.urno, psttab.pnam, psta, regn, act3, flno, onbl, via3, via3,
         via3, via3
    FROM psttab, ceda.afttab
   WHERE psttab.pnam = afttab.psta(+)
     AND (    (tifa(+) BETWEEN '$TfVal1' AND '$TfVal2')
          AND (psta(+) <> ' ')
          AND ftyp(+) = 'O'
          AND (onbl(+) <> ' ')
         )
ORDER BY psttab.pnam,flno
