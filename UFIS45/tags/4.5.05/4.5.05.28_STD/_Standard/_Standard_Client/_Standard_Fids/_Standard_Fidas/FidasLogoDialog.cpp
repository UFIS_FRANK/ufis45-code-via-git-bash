// FidasLogoDialog.cpp : implementation file
//

#include <stdafx.h>
#include <fidas.h>
#include <FidasLogoDialog.h>
#include <FidasUtilities.h>
#include <CFDDoc.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFidasLogoDialog dialog

CString CFidasLogoDialog::omLogoText;
CString CFidasLogoDialog::omGoodText;

CFidasLogoDialog::CFidasLogoDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CFidasLogoDialog::IDD, pParent)
{
	EnableAutomation();

	//{{AFX_DATA_INIT(CFidasLogoDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pomFieldDesc = NULL;
}


void CFidasLogoDialog::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CDialog::OnFinalRelease();
}

void CFidasLogoDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFidasLogoDialog)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	DDX_Control(pDX, IDC_UFIS_LOGO_FILESPEC_STATIC, m_FileSpec_Static);
	DDX_Control(pDX, IDC_UFIS_LOGO_FILESPEC_EDIT, m_FileSpec_Edit);
	DDX_Control(pDX, IDC_UFIS_LOGO_OK_BUTTON, m_OK_Button);
	DDX_Control(pDX, IDC_UFIS_LOGO_FRAME, m_Logo_Frame);
	DDX_Control(pDX, IDC_UFIS_LOGO_FILEEXT_STATIC, m_FileExt_Static);
	DDX_Control(pDX, IDC_UFIS_LOGO_FILEEXT_COMBO, m_FileExt_Combo);
	DDX_Control(pDX, IDC_UFIS_LOGO_CANCEL_BUTTON, m_Cancel_Button);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFidasLogoDialog, CDialog)
	//{{AFX_MSG_MAP(CFidasLogoDialog)
	ON_BN_CLICKED(IDC_UFIS_LOGO_OK_BUTTON, OnUfisLogoOkButton)
	ON_BN_CLICKED(IDC_UFIS_LOGO_CANCEL_BUTTON, OnUfisLogoCancelButton)
	ON_EN_CHANGE(IDC_UFIS_LOGO_FILESPEC_EDIT, OnChangeUfisLogoFilespecEdit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CFidasLogoDialog, CDialog)
	//{{AFX_DISPATCH_MAP(CFidasLogoDialog)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IFidasLogoDialog to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {DB78A156-5EED-11D5-812C-00010215BFDE}
static const IID IID_IFidasLogoDialog =
{ 0xdb78a156, 0x5eed, 0x11d5, { 0x81, 0x2c, 0x0, 0x1, 0x2, 0x15, 0xbf, 0xde } };

BEGIN_INTERFACE_MAP(CFidasLogoDialog, CDialog)
	INTERFACE_PART(CFidasLogoDialog, IID_IFidasLogoDialog, Dispatch)
END_INTERFACE_MAP()

void CFidasLogoDialog::SetFieldDescription(CFieldDesc *prpFieldDesc)
{
	ASSERT(prpFieldDesc);
	pomFieldDesc = prpFieldDesc;
}

/////////////////////////////////////////////////////////////////////////////
// CFidasLogoDialog message handlers

BOOL CFidasLogoDialog::OnInitDialog() 
{
	ASSERT(pomFieldDesc && pomFieldDesc->DpyType() && pomFieldDesc->FieldRef());

	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	this->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_LOGO_DIALOG));

	CWnd *pWnd = GetDlgItem(IDC_UFIS_LOGO_OK_BUTTON);
	if (pWnd)
	{
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_LOGO_OK_BUTTON));
	}

	pWnd = GetDlgItem(IDC_UFIS_LOGO_CANCEL_BUTTON);
	if (pWnd)
	{
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_LOGO_CANCEL_BUTTON));
	}

	pWnd = GetDlgItem(IDC_UFIS_LOGO_FRAME);
	if (pWnd)
	{
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_LOGO_FRAME));
	}

	pWnd = GetDlgItem(IDC_UFIS_LOGO_FILESPEC_STATIC);
	if (pWnd)
	{
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_LOGO_FILESPEC_STATIC));
	}

	pWnd = GetDlgItem(IDC_UFIS_LOGO_FILEEXT_STATIC);
	if (pWnd)
	{
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_LOGO_FILEEXT_STATIC));
	}

	m_FileExt_Combo.AddString(".GIF");
	m_FileExt_Combo.AddString(".JPG");
	m_FileExt_Combo.AddString(".MP1");
	m_FileExt_Combo.AddString(".MP2");
	m_FileExt_Combo.AddString(".VOB");

	const CDpyType_L *polDpyType = static_cast<const CDpyType_L *>(pomFieldDesc->DpyType());
	ASSERT(polDpyType);
	omLogoText = polDpyType->GetExtension();
	
	// split LogoText into filename extension and file extension
	int seppos = omLogoText.Find(".");							
	if (seppos < 1) 
		m_FileSpec_Edit.SetWindowText("");			
	else														
	{															
		omGoodText = omLogoText.Left(seppos);							
		if (!CFidasUtilities::IsValidString(omGoodText))
		{
			omGoodText = "";
			TRACE ("The following text contains invalid characters and is ignored: %s\n",omGoodText);
		}

		m_FileSpec_Edit.SetWindowText(omGoodText);
		omLogoText = omLogoText.Right(omLogoText.GetLength() - seppos);
	}													

	int logopos = m_FileExt_Combo.FindString(-1,omLogoText);
	if (logopos == -1) 
		m_FileExt_Combo.SetCurSel(0);
	else 
		m_FileExt_Combo.SetCurSel(logopos);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CFidasLogoDialog::OnUfisLogoOkButton() 
{
	// TODO: Add your control notification handler code here
	m_FileExt_Combo.GetLBText(m_FileExt_Combo.GetCurSel(),omLogoText);

	CString	olTempStr;
	m_FileSpec_Edit.GetWindowText(olTempStr);
	omLogoText = olTempStr + omLogoText;			

	CDpyType_L *polDpyType = static_cast<CDpyType_L *>(pomFieldDesc->GetDpyType());
	ASSERT(polDpyType);
	polDpyType->SetExtension(omLogoText);
	pomFieldDesc->SetDpyType(polDpyType);

	CDialog::EndDialog(IDOK);
	
}

void CFidasLogoDialog::OnUfisLogoCancelButton() 
{
	// TODO: Add your control notification handler code here
	CDialog::EndDialog(IDCANCEL);
}

void CFidasLogoDialog::OnChangeUfisLogoFilespecEdit() 
{
	CString olMySpec;
	m_FileSpec_Edit.GetWindowText(olMySpec);

	if (!CFidasUtilities::IsValidString(olMySpec)) 
		m_FileSpec_Edit.SetWindowText(omGoodText);
	else 
		omGoodText = olMySpec;
}
