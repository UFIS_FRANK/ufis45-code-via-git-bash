// CFidasSelectTableFieldDialog.cpp : implementation file
//

#include <stdafx.h>
#include <fidas.h>
#include <FidasSelectTableFieldDialog.h>
#include <FidasUtilities.h>
#include <CFDDoc.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFidasSelectTableFieldDialog dialog

CFidasSelectTableFieldDialog::CFidasSelectTableFieldDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CFidasSelectTableFieldDialog::IDD, pParent)
{
	EnableAutomation();

	//{{AFX_DATA_INIT(CFidasSelectTableFieldDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	imMaxFieldSize	= 0;
	imMinFieldRange	= 0;
	imMaxFieldRange	= 0;
	bmSecondField	= FALSE;
	bmShowFieldRange= FALSE;
	bmShowFieldSize	= FALSE;
}


void CFidasSelectTableFieldDialog::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CDialog::OnFinalRelease();
}

void CFidasSelectTableFieldDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFidasSelectTableFieldDialog)
	DDX_Control(pDX, IDC_UFIS_SELECTTABLEFIELD_SELECT_BUTTON, m_OK_Button);
	DDX_Control(pDX, IDC_UFIS_SELECTTABLEFIELD_TABLE_STATIC, m_Table_Static);
	DDX_Control(pDX, IDC_UFIS_SELECTTABLEFIELD_TABLE_COMBO, m_Table_Combo);
	DDX_Control(pDX, IDC_UFIS_SELECTTABLEFIELD_RANGE_START_STATIC, m_RangeStart_Static);
	DDX_Control(pDX, IDC_UFIS_SELECTTABLEFIELD_RANGE_START_EDIT, m_RangeStart_Edit);
	DDX_Control(pDX, IDC_UFIS_SELECTTABLEFIELD_RANGE_END_STATIC, m_RangeEnd_Static);
	DDX_Control(pDX, IDC_UFIS_SELECTTABLEFIELD_RANGE_END_EDIT, m_RangeEnd_Edit);
	DDX_Control(pDX, IDC_UFIS_SELECTTABLEFIELD_RANGE_CHECK, m_Range_Check);
	DDX_Control(pDX, IDC_UFIS_SELECTTABLEFIELD_MAXSIZE_EDIT, m_MaxSize_Edit);
	DDX_Control(pDX, IDC_UFIS_SELECTTABLEFIELD_MAXSIZE_CHECK, m_MaxSize_Check);
	DDX_Control(pDX, IDC_UFIS_SELECTTABLEFIELD_FIELD_STATIC, m_FieldList_Static);
	DDX_Control(pDX, IDC_UFIS_SELECTTABLEFIELD_FIELD_COMBO, m_FieldList_Combo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFidasSelectTableFieldDialog, CDialog)
	//{{AFX_MSG_MAP(CFidasSelectTableFieldDialog)
	ON_BN_CLICKED(IDC_UFIS_SELECTTABLEFIELD_CANCEL_BUTTON, OnUfisSelecttablefieldCancelButton)
	ON_BN_CLICKED(IDC_UFIS_SELECTTABLEFIELD_SELECT_BUTTON, OnUfisSelecttablefieldSelectButton)
	ON_BN_CLICKED(IDC_UFIS_SELECTTABLEFIELD_RANGE_CHECK, OnUfisSelecttablefieldRangeCheck)
	ON_BN_CLICKED(IDC_UFIS_SELECTTABLEFIELD_MAXSIZE_CHECK, OnUfisSelecttablefieldMaxsizeCheck)
	ON_CBN_SELCHANGE(IDC_UFIS_SELECTTABLEFIELD_TABLE_COMBO, OnSelchangeUfisSelecttablefieldTableCombo)
	ON_CBN_SELCHANGE(IDC_UFIS_SELECTTABLEFIELD_FIELD_COMBO, OnSelchangeUfisSelecttablefieldFieldCombo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CFidasSelectTableFieldDialog, CDialog)
	//{{AFX_DISPATCH_MAP(CFidasSelectTableFieldDialog)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IFidasSelectTableFieldDialog to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {4FE89CE6-6154-11D5-812F-00010215BFDE}
static const IID IID_IFidasSelectTableFieldDialog =
{ 0x4fe89ce6, 0x6154, 0x11d5, { 0x81, 0x2f, 0x0, 0x1, 0x2, 0x15, 0xbf, 0xde } };

BEGIN_INTERFACE_MAP(CFidasSelectTableFieldDialog, CDialog)
	INTERFACE_PART(CFidasSelectTableFieldDialog, IID_IFidasSelectTableFieldDialog, Dispatch)
END_INTERFACE_MAP()

void CFidasSelectTableFieldDialog::SetFieldDescription(CFieldDesc *prpFieldDesc)
{
	ASSERT(prpFieldDesc);
	pomFieldDesc = prpFieldDesc;
}

void CFidasSelectTableFieldDialog::SetSecondField(BOOL bpSecond)
{
	bmSecondField = bpSecond;
}

void CFidasSelectTableFieldDialog::ShowFieldRange(BOOL bpShow)
{
	bmShowFieldRange = bpShow;
}

void CFidasSelectTableFieldDialog::ShowFieldSize(BOOL bpShow)
{
	bmShowFieldSize = bpShow;
}

void CFidasSelectTableFieldDialog::SetMaxFieldSize(int ipMaxFieldSize,BOOL bpShow)
{
	imMaxFieldSize = ipMaxFieldSize;
	bmShowFieldSize= bpShow;
}

void CFidasSelectTableFieldDialog::SetFieldRange(int ipMinRange,int ipMaxRange,BOOL bpShow)
{
	ASSERT(ipMinRange >= 0 && ipMaxRange >= ipMinRange);

	imMinFieldRange = ipMinRange;
	imMaxFieldRange = ipMaxRange;
	bmShowFieldRange = bpShow;
}

void CFidasSelectTableFieldDialog::UpdateFieldList()
{
	int ind = m_Table_Combo.GetCurSel();
	if (ind < 0)
		return;

	CString olTable;
	m_Table_Combo.GetLBText(ind,olTable);

	omFieldInfos.clear();
	CFidasUtilities::GetFieldList(pomFieldDesc->DpyType()->GetType(),olTable,omFieldInfos);

	m_FieldList_Combo.ResetContent();
	for (int i = 0; i < omFieldInfos.size(); i++)
	{
		int ind = m_FieldList_Combo.AddString(omFieldInfos.at(i).csTranslation);
		ASSERT(ind != CB_ERR);
		m_FieldList_Combo.SetItemData(ind,i);
	}
		
	if (m_FieldList_Combo.GetCount())
	{
		const CFieldRef *polRef = pomFieldDesc->FieldRef();
		ASSERT(polRef);

		if (omTableNames.at(m_Table_Combo.GetItemData(ind)).Left(3) == polRef->GetTableName().Left(3))
		{
			
			CString olSelectedField;
			if (bmSecondField)
			{
				const CDpyType_R *polType = static_cast<const CDpyType_R *>(pomFieldDesc->DpyType());
				ASSERT(polType);
				olSelectedField = polType->GetFieldName();
			}
			else
			{
				olSelectedField = polRef->GetFieldName();
			}

			m_FieldList_Combo.SetCurSel(0);
			for (int i = 0; i < m_FieldList_Combo.GetCount(); i++)
			{
				if (omFieldInfos.at(m_FieldList_Combo.GetItemData(i)).csName == olSelectedField)
				{
					m_FieldList_Combo.SetCurSel(i);
					break;
				}
			}
		}
		else
		{
			m_FieldList_Combo.SetCurSel(0);
		}

		m_OK_Button.EnableWindow(TRUE);
	}
	else
	{
		m_OK_Button.EnableWindow(FALSE);
	}
	
}

/////////////////////////////////////////////////////////////////////////////
// CFidasSelectTableFieldDialog message handlers

BOOL CFidasSelectTableFieldDialog::OnInitDialog() 
{
	ASSERT(pomFieldDesc && pomFieldDesc->DpyType() && pomFieldDesc->FieldRef());
	if (!pomFieldDesc)
		return FALSE;
	ASSERT(!bmSecondField || pomFieldDesc->DpyType()->GetType() == CDpyType::R);
		
	CDialog::OnInitDialog();
	
	// localization
	this->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_SELECTTABLEFIELD));

	CWnd *pWnd = GetDlgItem(IDC_UFIS_SELECTTABLEFIELD_TABLE_STATIC);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_SELECTTABLEFIELD_TABLE_STATIC));

	pWnd = GetDlgItem(IDC_UFIS_SELECTTABLEFIELD_FIELD_STATIC);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_SELECTTABLEFIELD_FIELD_STATIC));

	pWnd = GetDlgItem(IDC_UFIS_SELECTTABLEFIELD_RANGE_CHECK);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_SELECTTABLEFIELD_RANGE_CHECK));

	pWnd = GetDlgItem(IDC_UFIS_SELECTTABLEFIELD_RANGE_START_STATIC);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDC_UFIS_SELECTTABLEFIELD_RANGE_START_STATIC));

	pWnd = GetDlgItem(IDC_UFIS_SELECTTABLEFIELD_RANGE_END_STATIC);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_SELECTTABLEFIELD_RANGE_END_STATIC));

	pWnd = GetDlgItem(IDC_UFIS_SELECTTABLEFIELD_MAXSIZE_CHECK);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_SELECTTABLEFIELD_MAXSIZE_CHECK));

	pWnd = GetDlgItem(IDC_UFIS_SELECTTABLEFIELD_SELECT_BUTTON);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_OK));
	
	pWnd = GetDlgItem(IDC_UFIS_SELECTTABLEFIELD_CANCEL_BUTTON);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_CANCEL));



	CFidasUtilities::GetTableList(pomFieldDesc->DpyType()->GetType(),omTableNames);
	for (int i = 0; i < omTableNames.size(); i++)
	{
		int ind = m_Table_Combo.AddString(omTableNames[i]);
		ASSERT(ind != CB_ERR);
		m_Table_Combo.SetItemData(ind,i);
	}
		
	if (m_Table_Combo.GetCount())
	{
		if (bmSecondField)	// second field of same table ?
		{
			const CFieldRef *polRef = pomFieldDesc->FieldRef();
			ASSERT(polRef);
			for (int i = 0; i < m_Table_Combo.GetCount(); i++)
			{
				if (omTableNames[m_Table_Combo.GetItemData(i)].Left(3) == polRef->GetTableName().Left(3))
				{
					m_Table_Combo.SetCurSel(i);
					UpdateFieldList();
					OnSelchangeUfisSelecttablefieldFieldCombo();
					m_Table_Combo.EnableWindow(FALSE);
				}
			}
		}
		else
		{
			const CFieldRef *polRef = pomFieldDesc->FieldRef();
			ASSERT(polRef);
			int ilFound = -1;
			for (int i = 0; i < m_Table_Combo.GetCount(); i++)
			{
				if (omTableNames[m_Table_Combo.GetItemData(i)].Left(3) == polRef->GetTableName().Left(3))
				{
					ilFound = i;
					break;
				}
			}

			if (ilFound >= 0)
			{
				m_Table_Combo.SetCurSel(ilFound);
				UpdateFieldList();
				OnSelchangeUfisSelecttablefieldFieldCombo();
			}
			else
			{
				m_Table_Combo.SetCurSel(0);
				UpdateFieldList();
				OnSelchangeUfisSelecttablefieldFieldCombo();
			}
		}
	}

	int ilMaxSize = pomFieldDesc->GetSize();
	CString olMaxSize;
	if (ilMaxSize > 0)
		olMaxSize.Format("%d",ilMaxSize);

	CString olRangeStart;
	if (pomFieldDesc->FieldRef()->GetStart() > 0)
		olRangeStart.Format("%d",pomFieldDesc->FieldRef()->GetStart());
	else
		olRangeStart.Format("%d",imMinFieldRange);

	CString olRangeEnd;
	if (pomFieldDesc->FieldRef()->GetEnd() > 0)
		olRangeEnd.Format("%d",pomFieldDesc->FieldRef()->GetEnd());
	else
		olRangeEnd.Format("%d",imMaxFieldRange);
	
	if (bmShowFieldSize) 
	{
		m_MaxSize_Check.SetCheck(pomFieldDesc->GetSize());						
		m_MaxSize_Edit.SetWindowText(olMaxSize);							
		m_MaxSize_Check.ShowWindow(SW_SHOW); 
		m_MaxSize_Edit.ShowWindow(SW_SHOW);
	}

	else 
	{
		m_MaxSize_Check.ShowWindow(SW_HIDE);
		m_MaxSize_Edit.ShowWindow(SW_HIDE);
		m_MaxSize_Check.SetCheck(0);
	}

	if (bmShowFieldRange) 
	{
		m_Range_Check.SetCheck(pomFieldDesc->FieldRef()->GetStart() > 0 || pomFieldDesc->FieldRef()->GetEnd());	
		m_RangeStart_Edit.SetWindowText(olRangeStart);					
		m_RangeEnd_Edit.SetWindowText(olRangeEnd);						

		OnUfisSelecttablefieldMaxsizeCheck();
		OnUfisSelecttablefieldRangeCheck();

		m_Range_Check.ShowWindow(SW_SHOW); 
		m_RangeStart_Static.ShowWindow(SW_SHOW); 
		m_RangeEnd_Static.ShowWindow(SW_SHOW);
		m_RangeStart_Edit.ShowWindow(SW_SHOW);
		m_RangeEnd_Edit.ShowWindow(SW_SHOW);
	}
	else
	{
		m_Range_Check.ShowWindow(SW_HIDE); 
		m_RangeStart_Static.ShowWindow(SW_HIDE);
		m_RangeEnd_Static.ShowWindow(SW_HIDE);
		m_RangeStart_Edit.ShowWindow(SW_HIDE);
		m_RangeEnd_Edit.ShowWindow(SW_HIDE);
		m_Range_Check.SetCheck(0);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CFidasSelectTableFieldDialog::OnUfisSelecttablefieldCancelButton() 
{
	// TODO: Add your control notification handler code here
	EndDialog(IDCANCEL);	
	
}

void CFidasSelectTableFieldDialog::OnUfisSelecttablefieldSelectButton() 
{
	// TODO: Add your control notification handler code here
	if (!bmSecondField)
	{
		CFieldRef *polRef = pomFieldDesc->GetFieldRef();
		ASSERT(polRef);

		CString olTableName;
		int ind = m_Table_Combo.GetCurSel();
		if (ind < 0)
		{
			pomFieldDesc->SetFieldRef(polRef);
			return;
		}
		
		olTableName = omTableNames.at(m_Table_Combo.GetItemData(ind));		
		polRef->SetTableName(olTableName.Left(3));

		CString olFieldName;
		ind = m_FieldList_Combo.GetCurSel();
		if (ind < 0)
		{
			pomFieldDesc->SetFieldRef(polRef);
			return;
		}

		olFieldName = omFieldInfos.at(m_FieldList_Combo.GetItemData(ind)).csName;		
		polRef->SetFieldName(olFieldName);

		if (m_MaxSize_Check.GetCheck() != 0) 
		{
			CString olMaxSize;
			m_MaxSize_Edit.GetWindowText(olMaxSize);
			int ilSize;
			if (sscanf(olMaxSize,"%d",&ilSize) == 1)
			{
				pomFieldDesc->SetSize(ilSize);
			}
		}
		else
		{
			pomFieldDesc->SetSize(0);
		}

		if (m_Range_Check.GetCheck() != 0)
		{
			CString olRangeStart;
			m_RangeStart_Edit.GetWindowText(olRangeStart);
			int ilSize;
			if (sscanf(olRangeStart,"%d",&ilSize) == 1)
			{
				polRef->SetStart(ilSize);
			}
			
			CString olRangeEnd;
			m_RangeEnd_Edit.GetWindowText(olRangeEnd);
			if (sscanf(olRangeEnd,"%d",&ilSize) == 1)
			{
				polRef->SetEnd(ilSize);
			}
		}
		else
		{
			polRef->SetStart(0);
			polRef->SetEnd(0);
		}

		pomFieldDesc->SetFieldRef(polRef);
	}
	else
	{
		CDpyType_R *polType = static_cast<CDpyType_R *>(pomFieldDesc->GetDpyType());
		ASSERT(polType);

		CString olTableName;
		int ind = m_Table_Combo.GetCurSel();
		if (ind < 0)
		{
			pomFieldDesc->SetDpyType(polType);
			return;
		}
		
		CString olFieldName;
		ind = m_FieldList_Combo.GetCurSel();
		if (ind < 0)
		{
			pomFieldDesc->SetDpyType(polType);
			return;
		}

		olFieldName = omFieldInfos.at(m_FieldList_Combo.GetItemData(ind)).csName;		
		polType->SetFieldName(olFieldName);
		polType->SetStart(0);
		polType->SetEnd(0);

		pomFieldDesc->SetDpyType(polType);
	}

	EndDialog(IDOK);	
}

void CFidasSelectTableFieldDialog::OnUfisSelecttablefieldRangeCheck() 
{
	// TODO: Add your control notification handler code here
	m_RangeStart_Static.EnableWindow(m_Range_Check.GetCheck());	
	m_RangeEnd_Static.EnableWindow(m_Range_Check.GetCheck());	
	m_RangeStart_Edit.EnableWindow(m_Range_Check.GetCheck());	
	m_RangeEnd_Edit.EnableWindow(m_Range_Check.GetCheck());	
	
}

void CFidasSelectTableFieldDialog::OnUfisSelecttablefieldMaxsizeCheck() 
{
	// TODO: Add your control notification handler code here
	m_MaxSize_Edit.EnableWindow(m_MaxSize_Check.GetCheck());	
	
}

void CFidasSelectTableFieldDialog::OnSelchangeUfisSelecttablefieldTableCombo() 
{
	// TODO: Add your control notification handler code here
	UpdateFieldList();	
}

void CFidasSelectTableFieldDialog::OnSelchangeUfisSelecttablefieldFieldCombo() 
{
	// TODO: Add your control notification handler code here
	int ind = m_FieldList_Combo.GetCurSel();
	if (ind < 0)
	{
		m_OK_Button.EnableWindow(FALSE);
		return;
	}

	m_OK_Button.EnableWindow(TRUE);

	const CFieldInfo& rolInfo = omFieldInfos.at(m_FieldList_Combo.GetItemData(ind));
	imMaxFieldSize = rolInfo.ciLength;
	switch(pomFieldDesc->DpyType()->GetType())
	{
	case CDpyType::T:
	case CDpyType::CT:
	case CDpyType::TT:
		imMinFieldRange = 9;
		imMaxFieldRange =12;
		bmShowFieldRange = TRUE;
	break;
	case CDpyType::D:
	case CDpyType::CD:
	case CDpyType::TD:
		imMinFieldRange = 1;
		imMaxFieldRange = 8;
		imMaxFieldSize  = 8;
		bmShowFieldRange = TRUE;
	break;
	case CDpyType::DT:
	case CDpyType::CDT:
	case CDpyType::TDT:
		imMinFieldRange = 1;
		imMaxFieldRange = imMaxFieldSize;
		bmShowFieldRange = TRUE;
	break;
	case CDpyType::N:	// NORMAL
		imMinFieldRange = 1;
		imMaxFieldRange = imMaxFieldSize;
		bmShowFieldRange = TRUE;
		bmShowFieldSize = TRUE;
	break;
	case CDpyType::L:	// LOGO
		imMinFieldRange = 1;
		imMaxFieldRange = imMaxFieldSize;
		bmShowFieldRange = TRUE;
	break;
	case CDpyType::B:	// REMARK
		imMinFieldRange = 1;
		imMaxFieldRange = imMaxFieldSize;
		bmShowFieldRange = TRUE;
		bmShowFieldSize	 = TRUE;
	break;
	case CDpyType::R:	// CONTENTS
		if (bmSecondField)	// second field ?
		{
			imMinFieldRange = 1;
			imMaxFieldRange = imMaxFieldSize;
			bmShowFieldRange = FALSE;
		}
		else
		{
			imMinFieldRange = 1;
			imMaxFieldRange = imMaxFieldSize;
			bmShowFieldRange = TRUE;
		}
	break;
	default:
		imMinFieldRange = 1;
		imMaxFieldRange = imMaxFieldSize;
		bmShowFieldRange = TRUE;
		bmShowFieldSize = TRUE;
	}

	CString olMaxSize;
	olMaxSize.Format("%d",imMaxFieldSize);

	CString olRangeStart;
	olRangeStart.Format("%d",imMinFieldRange);

	CString olRangeEnd;
	olRangeEnd.Format("%d",imMaxFieldRange);

	m_MaxSize_Edit.SetWindowText(olMaxSize);							
	m_RangeStart_Edit.SetWindowText(olRangeStart);					
	m_RangeEnd_Edit.SetWindowText(olRangeEnd);						
}
