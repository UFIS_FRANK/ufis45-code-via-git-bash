// FidasStaticFieldDialog.cpp : implementation file
//

#include <stdafx.h>
#include <fidas.h>
#include <FidasStaticFieldDialog.h>
#include <FidasUtilities.h>
#include <CFDDoc.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFidasStaticFieldDialog dialog


CFidasStaticFieldDialog::CFidasStaticFieldDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CFidasStaticFieldDialog::IDD, pParent)
{
	EnableAutomation();

	//{{AFX_DATA_INIT(CFidasStaticFieldDialog)
	m_EditText = _T("");
	//}}AFX_DATA_INIT
	pomFieldDesc = NULL;
}


void CFidasStaticFieldDialog::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CDialog::OnFinalRelease();
}

void CFidasStaticFieldDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFidasStaticFieldDialog)
	DDX_Text(pDX, IDC_EDIT_TEXT, m_EditText);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFidasStaticFieldDialog, CDialog)
	//{{AFX_MSG_MAP(CFidasStaticFieldDialog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CFidasStaticFieldDialog, CDialog)
	//{{AFX_DISPATCH_MAP(CFidasStaticFieldDialog)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IFidasStaticFieldDialog to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {4FE89CE9-6154-11D5-812F-00010215BFDE}
static const IID IID_IFidasStaticFieldDialog =
{ 0x4fe89ce9, 0x6154, 0x11d5, { 0x81, 0x2f, 0x0, 0x1, 0x2, 0x15, 0xbf, 0xde } };

BEGIN_INTERFACE_MAP(CFidasStaticFieldDialog, CDialog)
	INTERFACE_PART(CFidasStaticFieldDialog, IID_IFidasStaticFieldDialog, Dispatch)
END_INTERFACE_MAP()

void CFidasStaticFieldDialog::SetFieldDescription(CFieldDesc *prpFieldDesc)
{
	ASSERT(prpFieldDesc);
	pomFieldDesc = prpFieldDesc;
}

/////////////////////////////////////////////////////////////////////////////
// CFidasStaticFieldDialog message handlers

BOOL CFidasStaticFieldDialog::OnInitDialog() 
{
	ASSERT(pomFieldDesc && pomFieldDesc->DpyType() && pomFieldDesc->DpyType()->GetType() == CDpyType::S);

	const CDpyType_S *polType = static_cast<const CDpyType_S *>(pomFieldDesc->DpyType());
	ASSERT(polType);
	m_EditText = polType->GetText();

	CDialog::OnInitDialog();

	// localization
	SetWindowText(CFidasUtilities::GetString(IDS_UFIS_STATIC));
	
	CWnd *pWnd = GetDlgItem(IDC_UFIS_STATIC_TEXT);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_STATIC_TEXT));

	pWnd = GetDlgItem(IDOK);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_OK));
	
	pWnd = GetDlgItem(IDCANCEL);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_CANCEL));
	

	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CFidasStaticFieldDialog::OnOK() 
{
	// TODO: Add extra validation here
	if (!UpdateData())
		return;

	CDpyType_S *polType = static_cast<CDpyType_S *>(pomFieldDesc->GetDpyType());
	ASSERT(polType);
	polType->SetText(m_EditText);
	pomFieldDesc->SetDpyType(polType);
	CDialog::OnOK();
}
