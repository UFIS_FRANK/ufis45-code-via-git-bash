// FidasView.h : interface of the CFidasView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_FidasVIEW_H__7B1DCEE3_54EB_11D5_811D_00010215BFDE__INCLUDED_)
#define AFX_FidasVIEW_H__7B1DCEE3_54EB_11D5_811D_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <tab.h>
#include <FidasPrint.h>

class CFidasView : public CView
{
protected: // create from serialization only
	CFidasView();
	DECLARE_DYNCREATE(CFidasView)

// Attributes
public:
	CFidasDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFidasView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView);
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CFidasView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CFidasView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnProperties();
	afx_msg void OnInsertFielddef();
	afx_msg void OnCopyFielddef();
	afx_msg void OnRemoveFielddef();
	afx_msg void OnEditCopy();
	afx_msg void OnUpdateEditCopy(CCmdUI* pCmdUI);
	afx_msg void OnEditCut();
	afx_msg void OnUpdateEditCut(CCmdUI* pCmdUI);
	afx_msg void OnEditPaste();
	afx_msg void OnUpdateEditPaste(CCmdUI* pCmdUI);
	afx_msg void OnEditUndo();
	afx_msg void OnUpdateEditUndo(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFilePrintPreview(CCmdUI* pCmdUI);
	//}}AFX_MSG

	afx_msg	void OnSendLButtonClick(long LineNo, long ColNo);
	afx_msg	void OnSendLButtonDblClick(long LineNo, long ColNo);
	afx_msg	void OnSendRButtonClick(long LineNo, long ColNo);
	afx_msg	void OnSendMouseMove(long LineNo, long ColNo, const char *Flags);
	afx_msg	void OnColumnSelectionChanged(long ColNo, BOOL Selected);
	afx_msg	void OnVScroll(long LineNo);
	afx_msg	void OnInplaceEditCell(long LineNo, long ColNo,const char *NewValue,const char *OldValue);
	afx_msg	void OnOnHScroll(long ColNo);
	afx_msg	void OnRowSelectionChanged(long LineNo,BOOL Selected);
	afx_msg	void OnHitKeyOnLine(short Key, long LineNo);

	DECLARE_MESSAGE_MAP()
	DECLARE_EVENTSINK_MAP()
private:
	void	PrintLine(CFidasDoc *pDoc,UINT nLine);
	void	PrintPageHeader(CFidasDoc *pDoc,CDC *pDC,UINT nPageNumber);
	void	PrintPage(CFidasDoc *pDoc,CDC *pDC,UINT nPageNumber);
	void	PrintPageFooter(CFidasDoc *pDoc,CDC *pDC,UINT nPageNumber);
private:
	CTAB		omTabCtrl;
	CFidasPrint	omPrintCtrl;
	UINT		imLinesTotal;		
};

#ifndef _DEBUG  // debug version in FidasView.cpp
inline CFidasDoc* CFidasView::GetDocument()
   { return (CFidasDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FidasVIEW_H__7B1DCEE3_54EB_11D5_811D_00010215BFDE__INCLUDED_)
