/*
 * ABB/AAT tools lib starter version Jun 14 2001 
 * j.heilig
 *
 * Jun 22 01 released to test (jhe)
 *
 * $Log: Ufis/_Standard/_Standard_Client/_Standard_Fids/_Standard_Fidas/aatlib.h  $
 * Revision 1.1 2003/03/18 20:59:11SGT fei 
 * Initial revision
 * Member added to project /usr1/projects/UFIS4.5/_Standard_Fidas.pj
 * Revision 1.3 2002/08/14 09:40:50CEST fei 
 * Set additional include path to .\
 * Revision 1.2 2002/07/24 10:33:23GMT+02:00 tho 
 * Revision 1.1 2002/07/05 12:45:49GMT+02:00 fei 
 * Initial revision
 * Revision 1.1 2001/06/22 11:00:08GMT+02:00 tho 
 * Initial revision
 * -----
 * $Id: Ufis/_Standard/_Standard_Client/_Standard_Fids/_Standard_Fidas/aatlib.h 1.1 2003/03/18 20:59:11SGT fei Exp  $
 */

#ifndef aatlib_h_included
#define aatlib_h_included

#include <assert.h>
# include <iostream>
# include <strstream>

#define own_string_class
#define aatError(x) cerr<<"*** Error: "<<x<<endl
#define aatDebug(x) cerr<<">>> Debug: "<<x<<endl

#ifdef _MSC_VER
# include <stdafx.h>
# define Assert ASSERT
# undef own_string_class
  using namespace std;

#else
#	define Assert assert

# endif //__MSC_VER

#include <vector>

#ifdef own_string_class
class CString
{
public:
	CString();
	CString(const char *);
	CString(const CString &);
	~CString();
	int GetLength() const { return length; };
	CString &operator = (const char *);
	CString &operator = (const CString&);
	CString operator + (const char *) const;
	CString operator + (const CString&) const;
	CString &operator += (const char *);
	int operator == (const CString&) const;
	int operator == (const char *) const; 
	int operator != (const CString&) const;
	int operator != (const char *) const; 
	operator const char* () const;
private:
	int 	length;
	char *buffer;
	void set(const char *);
	void append(const char *);
// no MSVC support friend ostream& operator << (ostream &os, const CString &ss);
};

#endif // own_string_class

inline ostream& operator << (ostream &os, const CString &ss) { os<<(const char *)ss; return os;}

typedef std::vector<CString> CStringVector;
typedef std::vector<CString>::iterator CStringVector_it;
int StringVecEqual(const CStringVector *, const CStringVector *);

class CTokenZ
{
public:
	CTokenZ(istream &is, const char *, const char *skips=NULL, int trim=1);
	~CTokenZ();
	int Length() const;
	const char *String() const;
	int operator == (const char *) const;
	int operator != (const char *) const;
	operator const char* () const;
	static CStringVector *GetStrings(istream &, const char *);
	char *DupZ();
private:
	CTokenZ(const CTokenZ &);
	enum {QUANTUM=32};
	void add_char(char c);
	int trim_buff();
	char *buff;
	int  ncont, nmax;
};


#endif // aatlib_h_included
