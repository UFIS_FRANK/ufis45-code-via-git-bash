// FidasTestView.cpp : implementation file
//

#include <stdafx.h>
#include <fidas.h>
#include <FidasFormView.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFidasFormView

IMPLEMENT_DYNCREATE(CFidasFormView, CFormView)

CFidasFormView::CFidasFormView()
	: CFormView(CFidasFormView::IDD)
{
	EnableAutomation();
	//{{AFX_DATA_INIT(CFidasFormView)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CFidasFormView::~CFidasFormView()
{
}

void CFidasFormView::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CFormView::OnFinalRelease();
}

void CFidasFormView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFidasFormView)
	DDX_Control(pDX, IDC_TABCTRL1, m_TabCtrl);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFidasFormView, CFormView)
	//{{AFX_MSG_MAP(CFidasFormView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CFidasFormView, CFormView)
	//{{AFX_DISPATCH_MAP(CFidasFormView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IFidasTestView to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {8DE198B1-5B00-11D5-8126-00010215BFDE}
static const IID IID_IFidasFormView =
{ 0x8de198b1, 0x5b00, 0x11d5, { 0x81, 0x26, 0x0, 0x1, 0x2, 0x15, 0xbf, 0xde } };

BEGIN_INTERFACE_MAP(CFidasFormView, CFormView)
	INTERFACE_PART(CFidasFormView, IID_IFidasFormView, Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFidasFormView diagnostics

#ifdef _DEBUG
void CFidasFormView::AssertValid() const
{
	CFormView::AssertValid();
}

void CFidasFormView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CFidasFormView message handlers

BEGIN_EVENTSINK_MAP(CFidasFormView, CFormView)
    //{{AFX_EVENTSINK_MAP(CFidasFormView)
	ON_EVENT(CFidasFormView, IDC_TABCTRL1, 1 /* SendLButtonClick */, OnSendLButtonClickTabctrl1, VTS_I4 VTS_I4)
	ON_EVENT(CFidasFormView, IDC_TABCTRL1, 2 /* SendLButtonDblClick */, OnSendLButtonDblClickTabctrl1, VTS_I4 VTS_I4)
	ON_EVENT(CFidasFormView, IDC_TABCTRL1, 3 /* SendRButtonClick */, OnSendRButtonClickTabctrl1, VTS_I4 VTS_I4)
	ON_EVENT(CFidasFormView, IDC_TABCTRL1, 4 /* SendMouseMove */, OnSendMouseMoveTabctrl1, VTS_I4 VTS_I4 VTS_BSTR)
	ON_EVENT(CFidasFormView, IDC_TABCTRL1, 5 /* ColumnSelectionChanged */, OnColumnSelectionChangedTabctrl1, VTS_I4 VTS_BOOL)
	ON_EVENT(CFidasFormView, IDC_TABCTRL1, 6 /* OnVScroll */, OnOnVScrollTabctrl1, VTS_I4)
	ON_EVENT(CFidasFormView, IDC_TABCTRL1, 7 /* InplaceEditCell */, OnInplaceEditCellTabctrl1, VTS_I4 VTS_I4 VTS_BSTR VTS_BSTR)
	ON_EVENT(CFidasFormView, IDC_TABCTRL1, 8 /* OnHScroll */, OnOnHScrollTabctrl1, VTS_I4)
	ON_EVENT(CFidasFormView, IDC_TABCTRL1, 9 /* RowSelectionChanged */, OnRowSelectionChangedTabctrl1, VTS_I4 VTS_BOOL)
	ON_EVENT(CFidasFormView, IDC_TABCTRL1, 10 /* HitKeyOnLine */, OnHitKeyOnLineTabctrl1, VTS_I2 VTS_I4)
	//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()

void CFidasFormView::OnSendLButtonClickTabctrl1(long LineNo, long ColNo) 
{
	// TODO: Add your control notification handler code here
	
}

void CFidasFormView::OnSendLButtonDblClickTabctrl1(long LineNo, long ColNo) 
{
	// TODO: Add your control notification handler code here
	
}

void CFidasFormView::OnSendRButtonClickTabctrl1(long LineNo, long ColNo) 
{
	// TODO: Add your control notification handler code here
	
}

void CFidasFormView::OnSendMouseMoveTabctrl1(long LineNo, long ColNo, LPCTSTR Flags) 
{
	// TODO: Add your control notification handler code here
	
}

void CFidasFormView::OnColumnSelectionChangedTabctrl1(long ColNo, BOOL Selected) 
{
	// TODO: Add your control notification handler code here
	
}

void CFidasFormView::OnOnVScrollTabctrl1(long LineNo) 
{
	// TODO: Add your control notification handler code here
	
}

void CFidasFormView::OnInplaceEditCellTabctrl1(long LineNo, long ColNo, LPCTSTR NewValue, LPCTSTR OldValue) 
{
	// TODO: Add your control notification handler code here
	
}

void CFidasFormView::OnOnHScrollTabctrl1(long ColNo) 
{
	// TODO: Add your control notification handler code here
	
}

void CFidasFormView::OnRowSelectionChangedTabctrl1(long LineNo, BOOL Selected) 
{
	// TODO: Add your control notification handler code here
	
}

void CFidasFormView::OnHitKeyOnLineTabctrl1(short Key, long LineNo) 
{
	// TODO: Add your control notification handler code here
	
}

void CFidasFormView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	
	// TODO: Add your specialized code here and/or call the base class
	CString olFieldList = "Name,Description";
	m_TabCtrl.ResetContent();
	m_TabCtrl.SetFontName("Arial");
	m_TabCtrl.SetFontSize(12);
	m_TabCtrl.SetHeaderLengthString("70,100");
	m_TabCtrl.SetHeaderString(olFieldList);
	m_TabCtrl.RedrawTab();

	SCROLLINFO si;
	memset(&si,0,sizeof(si));
	si.cbSize = sizeof(si);
	si.fMask  = SIF_DISABLENOSCROLL;

	this->SetScrollInfo(SB_VERT,&si,false);	
	this->SetScrollInfo(SB_HORZ,&si,false);	


}

BOOL CFidasFormView::PreCreateWindow(CREATESTRUCT& cs) 
{
	// TODO: Add your specialized code here and/or call the base class

	if (cs.style & WS_VSCROLL)
	{
		TRACE("WS_VSCROLL style enabled\n");
	}

	if (cs.style & WS_HSCROLL)
	{
		TRACE("WS_HSCROLL style enabled\n");
	}

	if (cs.style & WS_THICKFRAME)
	{
		TRACE("WS_THICKFRAME style enabled\n");
	}

	if (cs.style & WS_BORDER)
	{
		TRACE("WS_BORDER style enabled\n");
		cs.style &= ~WS_BORDER;
	}

	BOOL blResult = CFormView::PreCreateWindow(cs);
	if (cs.style & WS_BORDER)
	{
		TRACE("WS_BORDER style enabled\n");
	}

	return blResult;
}

int CFidasFormView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	
	return 0;
}

void CFidasFormView::OnSize(UINT nType, int cx, int cy) 
{
	CFormView::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
//	m_TabCtrl.MoveWindow(0,0,cx,cy);
//	m_TabCtrl.RedrawTab();
	
}

CScrollBar* CFidasFormView::GetScrollBarCtrl(int nBar) const
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CFormView::GetScrollBarCtrl(nBar);
}

void CFidasFormView::OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView) 
{
	// TODO: Add your specialized code here and/or call the base class

	
	CFormView::OnActivateView(bActivate, pActivateView, pDeactiveView);
}
