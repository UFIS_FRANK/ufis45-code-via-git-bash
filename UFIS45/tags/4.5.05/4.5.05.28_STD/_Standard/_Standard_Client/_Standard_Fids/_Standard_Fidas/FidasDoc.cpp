// FidasDoc.cpp : implementation of the CFidasDoc class
//

#include <stdafx.h>
#include <Fidas.h>
#include <FidasNewDocumentDialog.h>
#include <FidasUtilities.h>
#include <FidasDoc.h>
#include <MainFrm.h>
#include <CFDDoc.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFidasDoc

IMPLEMENT_DYNCREATE(CFidasDoc, CDocument)

BEGIN_MESSAGE_MAP(CFidasDoc, CDocument)
	//{{AFX_MSG_MAP(CFidasDoc)
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_COMMAND(ID_FILE_SAVE_AS, OnFileSaveAs)
	ON_COMMAND(ID_PROPERTIES, OnProperties)
	ON_COMMAND(ID_INSERT_FIELDDEF, OnInsertFielddef)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CFidasDoc, CDocument)
	//{{AFX_DISPATCH_MAP(CFidasDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//      DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IFidas to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {7B1DCED5-54EB-11D5-811D-00010215BFDE}
static const IID IID_IFidas =
{ 0x7b1dced5, 0x54eb, 0x11d5, { 0x81, 0x1d, 0x0, 0x1, 0x2, 0x15, 0xbf, 0xde } };

BEGIN_INTERFACE_MAP(CFidasDoc, CDocument)
	INTERFACE_PART(CFidasDoc, IID_IFidas, Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFidasDoc construction/destruction

CFidasDoc::CFidasDoc(const CString& ropGroup)
: omFieldDefDoc(ropGroup)
{
	// TODO: add one-time construction code here
	SetTitle(ropGroup);

//	EnableAutomation();

//	AfxOleLockApp();
}


CFidasDoc::CFidasDoc()
: omFieldDefDoc("")
{
	// TODO: add one-time construction code here

//	EnableAutomation();

//	AfxOleLockApp();
}

CFidasDoc::~CFidasDoc()
{
//	AfxOleUnlockApp();
}

BOOL CFidasDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)
	SetTitle("");

	return TRUE;
}

CString CFidasDoc::GetGroup() const
{
	return omFieldDefDoc.GetDocName();
}

/////////////////////////////////////////////////////////////////////////////
// CFidasDoc serialization

void CFidasDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CFidasDoc diagnostics

#ifdef _DEBUG
void CFidasDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CFidasDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CFidasDoc commands

BOOL CFidasDoc::OnSaveDocument(LPCTSTR lpszPathName) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CDocument::OnSaveDocument(lpszPathName);
}

BOOL CFidasDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	if (!CDocument::OnOpenDocument(lpszPathName))
		return FALSE;
	
	// TODO: Add your specialized creation code here
	
	return TRUE;
}

void CFidasDoc::OnFileSave() 
{
	// already assigned to a group ?
	if (!omFieldDefDoc.GetDocName().GetLength())
	{
		CMainFrame *pMainFrm = (CMainFrame *)AfxGetMainWnd();
		ASSERT(pMainFrm);
		CStringVector olGroupList;
		CFieldDefDoc::GetDocList(olGroupList);
		CFidasNewDocumentDialog dlg(pMainFrm);
		dlg.SetDocList(olGroupList);
		dlg.SetMode(FALSE,"");
		if (dlg.DoModal() == IDOK)
		{
			CWaitCursor olWait;
			int ilStat = omFieldDefDoc.SaveDoc(dlg.m_GroupName);
			if (ilStat == 0)
			{
				SetTitle(dlg.m_GroupName);
				SetModifiedFlag(FALSE);
			}
			else
			{
				AfxMessageBox(CFidasUtilities::GetString(IDS_SAVE_DATABASE_FAILED),MB_ICONSTOP);
			}
		}
	}
	else if (IsModified())
	{
		CWaitCursor olWait;
		int ilStat = omFieldDefDoc.SaveDoc(omFieldDefDoc.GetDocName());
		if (ilStat == 0)
		{
			SetModifiedFlag(FALSE);
		}
		else
		{
			AfxMessageBox(CFidasUtilities::GetString(IDS_SAVE_DATABASE_FAILED),MB_ICONSTOP);
		}
	}
}

void CFidasDoc::OnFileSaveAs() 
{
	// TODO: Add your command handler code here
	CMainFrame *pMainFrm = (CMainFrame *)AfxGetMainWnd();
	ASSERT(pMainFrm);
	CStringVector olGroupList;
	CFieldDefDoc::GetDocList(olGroupList);

	CFidasNewDocumentDialog dlg(pMainFrm);
	dlg.SetDocList(olGroupList);
	dlg.m_GroupName = omFieldDefDoc.GetDocName();
	dlg.SetMode(TRUE,dlg.m_GroupName);
	if (dlg.DoModal() == IDOK)
	{
		// normal save
		if (omFieldDefDoc.GetDocName().CompareNoCase(dlg.m_GroupName) == 0)
		{
			CWaitCursor olWait;
			int ilStat = omFieldDefDoc.SaveDoc(dlg.m_GroupName);
			if (ilStat == 0)
			{
				SetModifiedFlag(FALSE);
			}
			else
			{
				AfxMessageBox(CFidasUtilities::GetString(IDS_SAVE_DATABASE_FAILED),MB_ICONSTOP);
			}
		}
		else
		{
			CWaitCursor olWait;
			int ilStat = omFieldDefDoc.SaveDoc(dlg.m_GroupName);
			if (ilStat == 0)
			{
				SetTitle(dlg.m_GroupName);
				SetModifiedFlag(FALSE);
			}
			else
			{
				AfxMessageBox(CFidasUtilities::GetString(IDS_SAVE_DATABASE_FAILED),MB_ICONSTOP);
			}
		}
	}
}

void CFidasDoc::OnCloseDocument() 
{

	CDocument::OnCloseDocument();
}

void CFidasDoc::OnProperties() 
{
	// TODO: Add your command handler code here
	TRACE("\nCFidasDoc::OnProperties called\n");
}

void CFidasDoc::OnInsertFielddef() 
{
	// TODO: Add your command handler code here
	AfxMessageBox("Not implemented yet");									
}

BOOL CFidasDoc::SaveModified() 
{
	// TODO: Add your specialized code here and/or call the base class
	// previously not saved ?
	BOOL blSave = IsModified();
	blSave |= !omFieldDefDoc.GetDocName().GetLength() && omFieldDefDoc.FieldDef() != NULL && omFieldDefDoc.FieldDef()->size() > 0;

	if (blSave)
	{
		CMainFrame *pMainFrm = (CMainFrame *)AfxGetMainWnd();
		ASSERT(pMainFrm);
		CStringVector olGroupList;
		CFieldDefDoc::GetDocList(olGroupList);

		CFidasNewDocumentDialog dlg(pMainFrm);
		dlg.SetDocList(olGroupList);
		dlg.m_GroupName = omFieldDefDoc.GetDocName();
		dlg.SetMode(TRUE,dlg.m_GroupName);
		if (dlg.DoModal() == IDOK)
		{
			// normal save
			if (omFieldDefDoc.GetDocName().CompareNoCase(dlg.m_GroupName) == 0)
			{
				CWaitCursor olWait;
				int ilStat = omFieldDefDoc.SaveDoc(dlg.m_GroupName);
				if (ilStat == 0)
				{
					SetModifiedFlag(FALSE);
				}
				else
				{
					AfxMessageBox(CFidasUtilities::GetString(IDS_SAVE_DATABASE_FAILED),MB_ICONSTOP);
					return FALSE;
				}
			}
			else
			{
				CWaitCursor olWait;
				int ilStat = omFieldDefDoc.SaveDoc(dlg.m_GroupName);
				if (ilStat == 0)
				{
					SetTitle(dlg.m_GroupName);
					SetModifiedFlag(FALSE);
				}
				else
				{
					AfxMessageBox(CFidasUtilities::GetString(IDS_SAVE_DATABASE_FAILED),MB_ICONSTOP);
					return FALSE;
				}
			}
		}
	}
	
	this->SetModifiedFlag(FALSE);
	return CDocument::SaveModified();
}
