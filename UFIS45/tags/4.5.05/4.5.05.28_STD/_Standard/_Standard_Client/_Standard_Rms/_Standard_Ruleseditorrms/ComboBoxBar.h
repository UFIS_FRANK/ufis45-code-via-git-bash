#if !defined(AFX_COMBOBOXBAR_H__85A94FD0_B9B5_11D2_AAF7_00001C018CF3__INCLUDED_)
#define AFX_COMBOBOXBAR_H__85A94FD0_B9B5_11D2_AAF7_00001C018CF3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <scrolltoolbar.h>
//////////////////////////////////////////////////////////////////////////////////
//
// ComboBoxBar.h : header file
//
// Notes: The Combobox Bar is derived from the standard CToolbar class and adds 
//        the additional features of displaying a ComboBox in the Toolbar.
//         
//        The Combobox in the Toolbar can be accesed through the pomComboBox Member.
//
//        Furthermore the Toolbar can load Text underneath its icons with the    
//        overwritten  -LoadToolBar- Function 
//
// Date : Februar 1999
//
// Author : EDE 
//
//
///////////////////////////////////////////////////////////////////////////////////
//
// Modification History:
// =====================
// 
// Apr 20, 1999 (MNE): 
//		Added a toggle button
//
//
///////////////////////////////////////////////////////////////////////////////////

class CComboBoxBar : public CScrollRebar 
{

public:

	 CComboBoxBar();
	~CComboBoxBar();

	 BOOL  LoadToolBar( UINT nIDResource );
	 void HideServicesListBtn(bool bpHide);
	BOOL  HideToolbarBtn(int ipID, bool bpHide);

//--- member ---
public:

	 CComboBox		*pomComboBox;
	 // CButton		*pomToggleButton;
	 CScrollToolBar	*pomToolBar;

public:

	// Generated message map functions
	//{{AFX_MSG(CComboBoxBar)
	//}}AFX_MSG

	 DECLARE_MESSAGE_MAP() 

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMBOBOXBAR_H__85A94FD0_B9B5_11D2_AAF7_00001C018CF3__INCLUDED_)

