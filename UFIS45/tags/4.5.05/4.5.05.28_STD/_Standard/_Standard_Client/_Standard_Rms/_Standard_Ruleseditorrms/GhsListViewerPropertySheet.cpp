//

// These following include files are project dependent
#include <stdafx.h>
#include <RegelWerk.h>
#include <CCSGlobl.h>
#include <CCSCedadata.h>

// These following include files are necessary
#include <cviewer.h>
#include <BasicData.h>
#include <GhsListViewerPropertySheet.h>
#include <StringConst.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CStringArray ogFlightAlcd;

/////////////////////////////////////////////////////////////////////////////
// GhsListViewerPropertySheet
//

GhsListViewerPropertySheet::GhsListViewerPropertySheet(CString opCalledFrom, CWnd* pParentWnd,
	CViewer *popViewer, UINT iSelectPage, LPCSTR pszCaption) : BasePropertySheet
	(pszCaption, pParentWnd, popViewer, iSelectPage)
{
	//-- exception handling
	CCS_TRY

	AddPage(&m_SerListView);

	//-- exception handling
	CCS_CATCH_ALL
}

void GhsListViewerPropertySheet::LoadDataFromViewer()
{
	pomViewer->GetFilter("SERLIST", m_SerListView.omValues);
}

void GhsListViewerPropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{
	//-- exception handling
	CCS_TRY

	pomViewer->SetFilter("SERLIST", m_SerListView.omValues);
	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}
	
	// MNE TEST
	/*for (int i = 0; i < m_SerListView.omValues.GetSize(); i++)
		TRACE("[GhsListViewerPropSheet::SaveDataToViewer] %s \n", m_SerListView.omValues[i]);*/
	// END MNE

	//-- exception handling
	CCS_CATCH_ALL

}

int GhsListViewerPropertySheet::QueryForDiscardChanges()
{
	return IDOK;
}
