// flplanps.h : header file
//



#ifndef _GhsListViewerPropertySheet_H_
#define _GhsListViewerPropertySheet_H_


#include <BasePropertySheet.h>
//#include "PSSearchFlightPage.h"
//#include "PSZeitraumPage.h"
//#include "PSGeometrie.h"
//#include "PSDispoRulesPage.h"
#include <StringConst.h>
#include <GhsListView.h>


/////////////////////////////////////////////////////////////////////////////
// SeasonFlightTablePropertySheet

class GhsListViewerPropertySheet: public BasePropertySheet
{
// Construction
public:
	GhsListViewerPropertySheet(CString opCalledFrom, CWnd* pParentWnd = NULL,
		CViewer *popViewer = NULL, UINT iSelectPage = 0, LPCSTR pszCaption = ID_SHEET_SERLIST_VIEWER);

// Attributes
public:
	GhsListView m_SerListView;
	
// Operations
public:
	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
	virtual int QueryForDiscardChanges();
};

/////////////////////////////////////////////////////////////////////////////

#endif // _GHSLISTVIEWERPROPERTYSHEET_H_
