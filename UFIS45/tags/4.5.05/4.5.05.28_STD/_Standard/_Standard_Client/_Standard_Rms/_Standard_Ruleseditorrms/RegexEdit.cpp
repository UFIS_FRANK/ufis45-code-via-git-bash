// DialogGrid.cpp : implementation file
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <basicdata.h>
#include <regexedit.h>


/////////////////////////////////////////////////////////////////////////////
// CRegexEdit

CRegexEdit::CRegexEdit (CGXGridCore* pGrid, UINT nID )
	:CGXEditControl(pGrid, nID)
{
}

CRegexEdit::~CRegexEdit()
{
}


BEGIN_MESSAGE_MAP(CRegexEdit, CGXEditControl)
	//{{AFX_MSG_MAP(CRegexEdit)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRegexEdit message handlers

BOOL CRegexEdit::ValidateString(const CString& sEdit)
{
	CString		olFormat;
	CGXStyle	olStyle;
	bool		blCheck = true;
	CGXGridCore *polGrid;

	polGrid = Grid();
	if ( polGrid && polGrid->GetStyleRowCol(m_nRow, m_nCol, olStyle) )
	{
		olStyle.GetUserAttribute(IDS_GRID_FORMATSTR, olFormat);
		
		if ( !olFormat.IsEmpty() && (olFormat != "NONE") )
		{
			blCheck = CheckRegExMatch(olFormat, sEdit);
		}
	}	
	return (BOOL)blCheck;
}