// ChangeInfo.h: interface for the ChangeInfo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CHANGEINFO_H__4F8A84F3_F0F1_11D3_A6EA_0000C007916B__INCLUDED_)
#define AFX_CHANGEINFO_H__4F8A84F3_F0F1_11D3_A6EA_0000C007916B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000



/*typedef struct RudDataStruct
{
	CString Urno;	
	CString Aloc;	// alocation
	CString Codu;	// required coverage by duties
	CString Dbar;	// flag demand begin abs/rel
	CString Dear;   // flag demand end abs/rel
	CString Debe;	// demand begin (date/time)
	CString Deco;	// demand condition
	CString Dedu;	// demand duration (sec)
	CString Deen;	// demand end
	CString Dide;	// divisable demand
	CString Dind;	// divisable demand number of duties
	CString Drty;	// duty requirement type
	CString Eadb;	// Earliest demand begin
	bool bFadd;		// flag activated / deactivated
	bool bFcnd;		// flag contractual / non-contractual demand
	bool bFfpd;		// flag full / partial demand
	bool bFncd;		// flag nominated / calculated demand
	bool bFond;		// flag operatzive / non-operative demand
	bool bFsad;		// flag standard / additional demand
	CString Lade;	// latest end of demand
	CString Lode;	// location of demand
	CString Maxd;	// max. duration 
	CString Mind;	// min. duration
	CString Nres;	// number of resources
	CString Rede;	// resource for linked demands
	CString Rend;	// reference to next demand
	CString Repd;	// reference to previous demand
	CString Rety;	// ressource type
	CString Rtdb;	// reference time demand begin
	CString Rtde;	// reference time demand end
	CString Sdti;	// setdown time
	CString Suti;	// setup time
	CString Stde;	// status of demand
	CString Tsdb;	// timespan to demand begin
	CString Tsde;	// timespan to demand end
	CString Tsnd;	// timespan to next demand
	CString Tspd;	// timespan to previous demand
	CString Ttgf;	// time to go from
	CString Ttgt;	// time to go to
	CString Ucru;	// urno of collective rule
	CString Udem;	// urno of demand
	CString Udgr;	// urno of demand group
	CString Uede;	// urno of eqwual demands
	CString Ughc;	// urno of contract
	CString Ughs;	// urno of service
	CString Ulnk;	// urno to link ressources
	CString Unde;	// urno of next demand
	CString Upde;	// urno of previous demand
	CString Upro;	// urno of pax profile
	CString Urue;	// urno of rule event

	RudDataStruct(void)
	{
		bFadd = false;
		bFcnd = false;
		bFfpd = false;
		bFncd = false;
		bFond = false;
		bFsad = false;
	}

} RUDDATA;*/



typedef struct RueDataStruct
{
	CString Urno;
	CString Evty;	// event type
	bool bExcl;		// exclusion flag 
	bool bFspl;		// Split Flag
	CString Maxt;	// max ground time
	CString Mist;	// min split time
	CString Runa;	// rule name
	CString Rusn;	// rule short name
	CString Rust;	// rule status (0=deactive, 1=active, 2=archived)
	CString Ruty;	// rule type (0=single, 1=collective, 2=independent)

	// CCSPtrArray <RUDDATA> *pRuds;

	RueDataStruct()
	{
		bExcl = false;
		bFspl = false;
		// pRuds = NULL;
	}

} RUEDATA;




class CRegelEditorFormView;
class FlightIndepRulesView;


class ChangeInfo  
{
public:
	ChangeInfo();
	virtual ~ChangeInfo();
	

// Implementation
	// void SaveRuleOrigInfo(CString opRuleUrno, bool bpNew);
	void SetCurrRueUrno(CString opRuleUrno);
	void SetChangeState(int ipChangeState = 0); // default: RULE_CHECK
	void SetCopiedRuleFlag(bool bpCopy = true);

	bool IsChanged();
	bool CheckChangesClosely();
	bool CheckResourceChanges(CString opOriTab, CString opTmpTab, CString opRefField, CString opRefVal);

	bool ArchiveRule(RecordSet &ropRecord);
	bool NeedToArchive(CString opOriTab, CString opTmpTab, CString opRefField, CString opRefVal, char *pcpFields );

// Attributes
private:
	// RUEDATA *pomRue;
	// CMapStringToPtr omRudMap;

	CString omCurrRueUrno;
	
	int  imChangeState;	// possible values: RULE_CHECK, RULE_NEW, RULE_COPY, RULE_DELETE

	bool bmNewFlag;
	bool bmIsCopiedFlag;
	bool bmIndep;		// independent or flight-related rule ?

};

#endif // !defined(AFX_CHANGEINFO_H__4F8A84F3_F0F1_11D3_A6EA_0000C007916B__INCLUDED_)
