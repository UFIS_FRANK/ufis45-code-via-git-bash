#if !defined(AFX_RULESLIST_H__F870E211_1CD3_11D3_A62D_0000C007916B__INCLUDED_)
#define AFX_RULESLIST_H__F870E211_1CD3_11D3_A62D_0000C007916B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RulesList.h : header file
//

#include <DialogGrid.h>

class CRulesFormView;

extern ROWCOL igRuleTopRow;

/////////////////////////////////////////////////////////////////////////////
// RulesList dialog

class RulesList : public CDialog
{
// Construction
public:
	RulesList(bool BpIsShowIndep, CWnd* pParent = NULL);   // standard constructor
	~RulesList();

// Dialog Data
	//{{AFX_DATA(RulesList)
	enum { IDD = IDD_SELECT_RULE };
	CButton	m_Chb_Indep;
	CComboBox	omComboBox;
	//}}AFX_DATA
	CWnd	pomDlgControls[5];

	// show flight independent rules
	bool bmIsShowIndep;
	//  Zeitintervall "Warnung vor Ablauf der Regel" in Tagen
	UINT imWarnBefore;

	//--- Daten f�r Regel
	CString  omDataStringRotation;
	CString  omDataStringArrival;
	CString  omDataStringDeparture;
	CString  omDataStringRuleName;
	CString  omDataStringRuleShortName;
	CString  omDataStringRuleUrno;
	

	//--- Auswahl in ComboBox
	CString  omTemplateName;
	CGridFenster *pomGrid;
	long	lmSelectedTplUrno;
	CString	omTxtSingle;
	CString	omTxtCollective;
	CString	omTxtIndependent;
	CRulesFormView *pomActView;
	int  imLastWidth;		//  letzte Gr��e der client area
	int  imLastHeight;		//				"

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(RulesList)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
public:
	int DoModal(CString opRueUrno = CString(""), long lpSelectedTplUrno = -1);
protected:
	
	void FillGrid(CString opTplName);
	void DisplayOneRule ( RecordSet *popRule );
	bool GetExpirationColor ( CString &ropVato, COLORREF &ipExpirationColor );
	void SetStaticTexts();
	void SetSorting( bool bpIgnoreActSorting=false); 
	void IniColWidths ();
	void SaveColWidths ();

	// Generated message map functions
	//{{AFX_MSG(RulesList)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnSelchangeCOMBObox();
	afx_msg void OnGridClick(WPARAM wParam, LPARAM lParam);
	afx_msg void OnGridDoubleClick(WPARAM wParam, LPARAM lParam);
	afx_msg void OnIndepCheckbox();
	afx_msg void OnFilePrint();
	afx_msg void OnFilePrintPreview();
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

int ValComp(const RecordSet **ppDateStr1, const RecordSet **ppDateStr2);

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RULESLIST_H__F870E211_1CD3_11D3_A62D_0000C007916B__INCLUDED_)
