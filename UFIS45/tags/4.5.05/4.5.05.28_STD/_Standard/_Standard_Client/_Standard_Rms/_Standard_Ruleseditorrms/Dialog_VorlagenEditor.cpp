// Dialog_TemplateEditor.cpp : implementation file
//

#include <stdafx.h>
#include <regelwerk.h>
#include <mainfrm.h>
#include <Dialog_VorlagenEditor.h>
#include <Dialog_Neuer_Name.h>

#include <CedaBasicData.h>
#include <BasicData.h>
#include <CCSGlobl.h>


#define _ROT_ 0
#define _ARR_ 1
#define _DEP_ 2
 


//----------------------------------------------------------------------------------------------------------------------
//					defines
//----------------------------------------------------------------------------------------------------------------------



//----------------------------------------------------------------------------------------------------------------------
//					construction / destruction
//----------------------------------------------------------------------------------------------------------------------
CDialog_TemplateEditor::CDialog_TemplateEditor(CWnd* pParent /*=NULL*/)
	: CDialog(CDialog_TemplateEditor::IDD, pParent)
{
	CCS_TRY
	
	//{{AFX_DATA_INIT(CDialog_TemplateEditor)
	imFlugUnabhaengig = 0;
	m_Chb_Deacti_Val = FALSE;
	//}}AFX_DATA_INIT

	omCurrentUrno = "";
	bmChangeFlag = false;
	imCurrSel = -1;
	bmRenamed = false;
	//--- create bitmap buttons
	pomHL2RBtn = new CBitmapButton();
	pomVL2RBtn = new CBitmapButton();
	pomHR2LBtn = new CBitmapButton();
	pomVR2LBtn = new CBitmapButton();

	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------------------------------------------------

CDialog_TemplateEditor::~CDialog_TemplateEditor()
{
	delete pomHL2RBtn; 
	delete pomVL2RBtn;
	delete pomHR2LBtn;
	delete pomVR2LBtn;

	delete pomLeftRotGrid;
	delete pomLeftArrGrid;
	delete pomLeftDepGrid;
	delete pomRightRotGrid;
	delete pomRightArrGrid;
	delete pomRightDepGrid;


}



//----------------------------------------------------------------------------------------------------------------------
//					data exchange, message map
//----------------------------------------------------------------------------------------------------------------------
void CDialog_TemplateEditor::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDialog_TemplateEditor)
	DDX_Control(pDX, IDC_LIST_FTYPES_OUT, m_FtypesOutLB);
	DDX_Control(pDX, IDC_LIST_FTYPES_IN, m_FtypesInLB);
	DDX_Control(pDX, TEMPL_COB_RELX, m_Cob_Relx);
	DDX_Control(pDX, TEMPL_COB_ALOD, m_Cob_Alod);
	DDX_Control(pDX, TEMPL_CHB_DEACTI, m_Chb_Deacti);
	DDX_Control(pDX, IDC_SAVE, m_SaveBtn);
	DDX_Control(pDX, IDC_Vorlagen_Name, m_TemplateList);
	DDX_Radio(pDX, IDC_RADIO_FLUGABHAENGIG, imFlugUnabhaengig);
	DDX_Check(pDX, TEMPL_CHB_DEACTI, m_Chb_Deacti_Val);
	//}}AFX_DATA_MAP
}
//----------------------------------------------------------------------------------------------------------------------

BEGIN_MESSAGE_MAP(CDialog_TemplateEditor, CDialog)
	//{{AFX_MSG_MAP(CDialog_TemplateEditor)
	ON_BN_CLICKED(IDC_NEW, OnNew)
	ON_BN_CLICKED(IDC_ARR_ADD, OnArrAdd)
	ON_BN_CLICKED(IDC_ARR_DELETE, OnArrDelete)
	ON_BN_CLICKED(IDC_ROT_ADD, OnRotAdd)
	ON_BN_CLICKED(IDC_ROT_DELETE, OnRotDelete)
	ON_CBN_SELCHANGE(IDC_Vorlagen_Name, OnSelchangeTemplateName)
	ON_CBN_EDITCHANGE(IDC_Vorlagen_Name, OnEditchangeTemplateName)
	ON_CBN_DROPDOWN(IDC_Vorlagen_Name, OnDropdownTemplateName)
	ON_BN_CLICKED(IDC_DELETE, OnDelete)
	ON_BN_CLICKED(IDC_SAVE, OnSave)
	ON_BN_CLICKED(IDC_SCHLIESSEN, OnClose)
	ON_BN_CLICKED(IDC_DEP_ADD, OnDepAdd)
	ON_BN_CLICKED(IDC_DEP_DELETE, OnDepDelete)
	ON_BN_CLICKED(IDC_HL2R_BTN, OnHLeft2Right)
	ON_BN_CLICKED(IDC_VL2R_BTN, OnVLeft2Right)
 	ON_BN_CLICKED(IDC_HR2L_BTN, OnHRight2Left)
	ON_BN_CLICKED(IDC_VR2L_BTN, OnVRight2Left)
	ON_BN_CLICKED(IDC_COPY_BTN, OnCopy)
	ON_MESSAGE(GRID_MESSAGE_CELLCLICK, OnGridFeld)
	ON_CBN_SELCHANGE(TEMPL_COB_ALOD, OnSelchangeAllocation)
	ON_BN_CLICKED(TEMPL_CHB_DEACTI, OnDeactivate)
	ON_BN_CLICKED(IDC_RADIO_FLUGABHAENGIG, OnRadioFlugabhaengig)
	ON_CBN_SELCHANGE(TEMPL_COB_RELX, OnSelchangeCobRelx)
	ON_LBN_SELCHANGE(IDC_LIST_FTYPES_IN, OnSelchangeFtypes)
	ON_BN_CLICKED(IDC_RADIO_FLUGUNHAENGIG, OnRadioFlugabhaengig)
	ON_LBN_SELCHANGE(IDC_LIST_FTYPES_OUT, OnSelchangeFtypes)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



//----------------------------------------------------------------------------------------------------------------------
//					message handlers
//----------------------------------------------------------------------------------------------------------------------
BOOL CDialog_TemplateEditor::OnInitDialog() 
{
	CCS_TRY

	//--- call parent class' method
	CDialog::OnInitDialog();
	IniStatics ();
	int ilRecordCount, i = 0, ilPos = 0;
	CCSPtrArray <RecordSet> olTplArr;
	CString olName, olUrno, olTpst, olRelx;

	if ( igTplFtyaIdx < 0 )
	{
		EnableDlgItem ( this, IDC_GRP_FTYPES_IN, FALSE, FALSE );
		EnableDlgItem ( this, IDC_LIST_FTYPES_IN, FALSE, FALSE );
	}

	if ( igTplFtydIdx < 0 )
	{
		EnableDlgItem ( this, IDC_GRP_FTYPES_OUT, FALSE, FALSE );
		EnableDlgItem ( this, IDC_LIST_FTYPES_OUT, FALSE, FALSE );
	}
	for ( i=IDS_FTYPE_D; i<=IDS_FTYPE_PROG; i++ )
	{
		olName = GetString (i);
		ilPos = olName.Find ( '(' );
		if ( (ilPos>0)  && (olName.GetLength() > ilPos+1) )
		{
			olRelx = olName.Mid ( ilPos+1, 1 );
			ilPos = m_FtypesOutLB.AddString ( olName );
			if ( ilPos >= 0 )
				m_FtypesOutLB.SetItemData ( ilPos, olRelx[0] );
			ilPos = m_FtypesInLB.AddString ( olName );
			if ( ilPos >= 0 )
				m_FtypesInLB.SetItemData ( ilPos, olRelx[0] );
		}
	}
	//---  fill templates combobox
	// pomTemplateListCombo = m_TemplateList.GetComboBoxCtrl();
	
	i=0;
	ilRecordCount = ogBCD.GetAllRecords("TPL", olTplArr);

	while (i < ilRecordCount)
	{
		olRelx = olTplArr[i].Values[igTplRelxIdx];

		if (olRelx == "AFT" || olRelx == "REG")
		{
			olName = olTplArr[i].Values[igTplTnamIdx];

			olUrno = olTplArr[i].Values[igTplUrnoIdx];
			olTpst = olTplArr[i].Values[igTplTpstIdx];
			
			if ((olTpst != '2') && (m_TemplateList.FindStringExact(-1, olName) == CB_ERR) &&
				 CanModifyTemplate ( olName ) )
			{	
				ilPos = m_TemplateList.AddString(olName);
				m_TemplateList.SetItemData(ilPos, atol(olUrno));
			}
		}

		i++;
	}
	olTplArr.DeleteAll();
	m_TemplateList.SetCurSel(0);
	imCurrSel = m_TemplateList.GetCurSel();


	//--- set current urno
	if (imCurrSel != CB_ERR)
	{
		omCurrentUrno.Format("%d", m_TemplateList.GetItemData(imCurrSel));
	}

	//--- set imFisu
	imFisu = 1;
	if (ogBCD.GetField("TPL", "URNO", omCurrentUrno, "FISU") == "0")
	{
		imFisu = 0;		// there are/were rules based on this template
	}
	
	//--- set template status (added by MNE 010109)
	if (ogBCD.GetField("TPL", "URNO", omCurrentUrno, "TPST") == "0")
		m_Chb_Deacti.SetCheck(CHECKED); // deactivated, TPL.TPST = 0
	else
		m_Chb_Deacti.SetCheck(UNCHECKED);	// activated, TPL.TPST = 1

	//--- fill locations combobox
	CString olAlod;
	ilRecordCount = ogBCD.GetDataCount("ALO");
	for (i = 0; i < ilRecordCount; i++)
	{
		olAlod = ogBCD.GetField("ALO", i, "ALOD");
		olUrno = ogBCD.GetField("ALO", i, "URNO");
		ilPos = m_Cob_Alod.AddString(olAlod);
		m_Cob_Alod.SetItemData(ilPos, atol(olUrno));
	}
	
	//--- fill relations combobox
	omList.Add(GetString(2002));	// AFT
	omList.Add(GetString(2001));	// REG

	for (i = 0; i < omList.GetSize(); i++)
	{
		ilPos = m_Cob_Relx.AddString(omList.GetAt(i));
	}

	//--- draw button bitmaps
	DrawBitmapButtons();
	
	CRect olDefaultRect;
	GetWindowRect (olDefaultRect);
	imDefaultHeight = olDefaultRect.Height();
	
	//--- create grids
	pomLeftRotGrid = new CGridFenster();
	pomLeftArrGrid = new CGridFenster();
	pomLeftDepGrid = new CGridFenster();
	pomRightRotGrid = new CGridFenster();
	pomRightArrGrid = new CGridFenster();
	pomRightDepGrid = new CGridFenster();


    //--- initialize grids
    pomLeftRotGrid->SubclassDlgItem(IDC_AW_ROTATION, this);
	pomLeftArrGrid->SubclassDlgItem(IDC_AW_ARRIVAL, this);
    pomLeftDepGrid->SubclassDlgItem(IDC_AW_DEPARTURE, this);
    pomRightRotGrid->SubclassDlgItem(IDC_SEL_ROTATION, this);
    pomRightArrGrid->SubclassDlgItem(IDC_SEL_ARRIVAL, this); 
    pomRightDepGrid->SubclassDlgItem(IDC_SEL_DEPARTURE, this);  



	InitializeTable(pomLeftRotGrid, pomRightRotGrid);	// turnaround
	InitializeTable(pomLeftArrGrid, pomRightArrGrid);	// arrival
	InitializeTable(pomLeftDepGrid, pomRightDepGrid);	// departure

    pomLeftRotGrid->SetReadOnly(TRUE);
    pomLeftArrGrid->SetReadOnly(TRUE);
    pomLeftDepGrid->SetReadOnly(TRUE);
    pomRightRotGrid->SetReadOnly(TRUE);
    pomRightArrGrid->SetReadOnly(TRUE);
    pomRightDepGrid->SetReadOnly(TRUE);

  	pomLeftRotGrid->GetParam()->EnableMoveRows(false);
	pomLeftArrGrid->GetParam()->EnableMoveRows(false);
	pomLeftDepGrid->GetParam()->EnableMoveRows(false);


	OnSelchangeTemplateName();
	
	CCS_CATCH_ALL

	return TRUE;
}
//----------------------------------------------------------------------------------------------------------------------

void CDialog_TemplateEditor::OnNew() 
{
	CCS_TRY
    

	// save previously made changes ?
	if ( !SaveChanges () )
		return ;

	//--- Dialog f�r Eingabe von Namen aufrufen 
	CDialog_Neuer_Name  olDialog;

	//--- create new template
	if(olDialog.DoModal() == IDOK) 
	{
		//--- update combobox
		int ilPos = m_TemplateList.AddString( olDialog.m_Name );
		m_TemplateList.SetCurSel(ilPos);
		OnSelchangeTemplateName();

		omCurrentUrno = "";
		imFisu = 0;

		// rechte Grids leeren
		ResetAllTables();
		FillTable(pomLeftRotGrid, pomRightRotGrid,"FLDT");
		if ( !imFlugUnabhaengig )
		{
			FillTable(pomLeftArrGrid, pomRightArrGrid,"FLDA");
			FillTable(pomLeftDepGrid, pomRightDepGrid,"FLDD");
		}
		bmChangeFlag = true;
		m_SaveBtn.EnableWindow(TRUE);
		m_FtypesInLB.SetSel( -1, FALSE );
		m_FtypesOutLB.SetSel( -1, FALSE );
	}

	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------------------------------------------------

void CDialog_TemplateEditor::OnCopy() 
{
	CCS_TRY
	
	// save previously made changes ?
	if ( !SaveChanges () )
		return;

	//--- Dialog f�r Eingabe von Namen aufrufen 
	CDialog_Neuer_Name  olDialog;

	//--- Neue Vorlage anlegen
	if(olDialog.DoModal() == IDOK) 
	{
		//--- Combox updaten
		int ilPos = m_TemplateList.AddString(olDialog.m_Name);
	    m_TemplateList.SetCurSel(ilPos);

		omCurrentUrno = "";
		imFisu = 0;

		bmChangeFlag = true;
		m_SaveBtn.EnableWindow(TRUE);
	}

	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------------------------------------------------



bool CDialog_TemplateEditor::InitializeTable(CGridFenster *popLeftTable, CGridFenster *popRightTable )
{
	CCS_TRY

	//--  Spalten Breiten ermitteln  -> Linke Seite
    CRect olRect;

	popLeftTable->GetClientRect(olRect);

    int ColWidth_0 = 30;
	int ColWidth_1 = (int)((olRect.Width() - ColWidth_0) * 0.2); 
	int ColWidth_2 = (int)((olRect.Width() - ColWidth_0) * 0.8);
	int ColWidth_3 = 0;

    popLeftTable->Initialize();

	popLeftTable->SetRowCount(0);
	popLeftTable->SetColCount(3);

	popLeftTable->SetColWidth(0, 0, ColWidth_0);
	popLeftTable->SetColWidth(1, 1, ColWidth_1);
	popLeftTable->SetColWidth(2, 2, ColWidth_2);
	popLeftTable->HideCols(3, 3);

    popLeftTable->SetValueRange(CGXRange().SetCols(0, 0), "");  
    popLeftTable->GetParam()->EnableTrackRowHeight(0); 


	//--  Spalten Breiten ermitteln  -> Rechte Seite
	popRightTable->GetClientRect(olRect);

    ColWidth_0 = 30;
	ColWidth_1 = (int)((olRect.Width() - ColWidth_0) * 0.1); 
	ColWidth_2 = (int)((olRect.Width() - ColWidth_0) * 0.2);
	ColWidth_3 = (int)((olRect.Width() - ColWidth_0) * 0.7);

    popRightTable->Initialize();

	popRightTable->SetRowCount(0);
	popRightTable->SetColCount(6);

	popRightTable->SetColWidth(0, 0, ColWidth_0); // row header
	popRightTable->SetColWidth(1, 1, ColWidth_1); // counter
	popRightTable->SetColWidth(2, 2, ColWidth_2); // base field
	popRightTable->SetColWidth(3, 3, ColWidth_3); // real name
	// column 4 (hidden) contains base table from TSR
	// column 5 (hidden) contains reference table from TSR
	// column 5 (hidden) contains reference field from TSR
	
	popRightTable->SetNumSort(true);
	popRightTable->HideCols(4, 6);

    popRightTable->SetValueRange(CGXRange().SetCols(0, 0), "");  
	popRightTable->GetParam()->EnableTrackRowHeight(0); 
	
	CCS_CATCH_ALL

	return true;
}
//----------------------------------------------------------------------------------------------------------------------

bool CDialog_TemplateEditor::FillTable(CGridFenster *popLeftTable, CGridFenster *popRightTable, const CString &ropWhichOne) 
{
	CCS_TRY

	ResetTable(popLeftTable);
	ResetTable(popRightTable);

	//--  switch off read-only-flag
    popLeftTable ->SetReadOnly(FALSE);
	popRightTable->SetReadOnly(FALSE);
		
	//-- Grids f�llen mit Daten aus TPL  Feldern FLDT, FLDA, FLDD

	CString olFLD;
	CString olFieldRel;
	CString olUrno;
	if ( !omCurrentUrno.IsEmpty () )
		olFLD = ogBCD.GetField("TPL", "URNO", omCurrentUrno, ropWhichOne);
	
	// mne 000920
	if (ropWhichOne == "FLDT")
		omFldt = olFLD;
	else if (ropWhichOne == "FLDA")
		omFlda = olFLD;
	else if (ropWhichOne == "FLDD")
		omFldd = olFLD;
	// end mne


	CStringArray olUsedUrnosArr;
    CStringArray olFldArr;

	popRightTable->LockUpdate(TRUE);
    
	int ilCount = ExtractItemList(olFLD, &olFldArr, ';');
	
	CString olRealName;
	CString olBaseTab, olBaseField;
	CString olRefTab, olRefField;

	CCSPtrArray <RecordSet> olTsrArr;

//-- right-hand side
	CString olText;
	TRACE("%s hat %d Eintr�ge\n", ropWhichOne, ilCount);
    for(int i = 0; i < ilCount; i++)
	{
		//-- new line
		popRightTable->InsertRows(i + 1, 1);

		//-- fill line
		olText = olFldArr.GetAt(i); 
		int ilPointPos = olText.Find(".");
		int ilStrLen = olText.GetLength();
		if ( ( ilPointPos >= 0 ) && (ilPointPos + 8 < ilStrLen) )
		{
			
			olBaseTab = olText.Mid(ilPointPos + 1, 3);
			olBaseField = olText.Mid(ilPointPos + 1 + 4, 4);
			
		// MNE 000919
			// At the time being we shouldn't get more than one TSR entry when using BTAB, BFLD
			// That might change in the near future, so it's allright to use an array to retrieve
			// the record. 
			ogBCD.GetRecordsExt("TSR", "BTAB", "BFLD", olBaseTab, olBaseField, &olTsrArr);
			
			if (olTsrArr.GetSize() > 0)
			{
				olRealName = olTsrArr[0].Values[igTsrNameIdx];
				olUrno = olTsrArr[0].Values[igTsrUrnoIdx];
				olRefTab = olTsrArr[0].Values[igTsrRtabIdx];
				olRefField = olTsrArr[0].Values[igTsrRfldIdx];
			}
			
			olTsrArr.DeleteAll();

			/*CString olRealName = ogBCD.GetFieldExt("TSR", "BTAB", "BFLD", olTable, olFieldName,"NAME");
			olUrno = ogBCD.GetFieldExt("TSR", "BTAB", "BFLD", olTable, olFieldName,"URNO");*/
		// END MNE

			if (!GetNameOfTSRRecord ( olUrno, olRealName ) )
				TRACE("Keinen Namen f�r TSR-Satz mit Urno=%s gefunden!\n", olUrno );

			popRightTable->SetValueRange(CGXRange(i + 1, 1), (WORD) (i +1));  
			popRightTable->SetValueRange(CGXRange(i + 1, 2),  olBaseField  );  
			popRightTable->SetValueRange(CGXRange(i + 1, 3),  olRealName);  
			popRightTable->SetValueRange(CGXRange(i + 1, 4),  olBaseTab);
			popRightTable->SetValueRange(CGXRange(i + 1, 5),  olRefTab);
			popRightTable->SetValueRange(CGXRange(i + 1, 6),  olRefField);

			olUsedUrnosArr.Add(olUrno);

			// MNE TEST
			// TRACE("rechts %s: %d) Basis: %s, Urno: %s, Name: %s [Added to olUsedUrnosArray]\n", ropWhichOne, i, olText, olUrno, olRealName);
			// END MNE
		}
	}

	popRightTable->LockUpdate(FALSE);
	popRightTable->Redraw();

//--  left-hand side
	int  ilRectOrdPos  = 1;   
	int  ilRecordCount = ogBCD.GetDataCount("TSR");

	popLeftTable->LockUpdate(TRUE);
			
	// MNE TEST
	// TRACE("TSR has %d Entries\n", ilRecordCount);
	// END MNE

	for(i = 0; i < ilRecordCount; i++)
	{
		CString olUrno;
		olUrno = ogBCD.GetField("TSR", i, "URNO");
		olFieldRel = ogBCD.GetField("TSR", i, "FIRE");
		
		bool blFound = false;
		bool blCanBeUsed = false;

		// first check if field can be used
		if (olFieldRel.GetLength() == 3)
		{
			if ( imFlugUnabhaengig )
				blCanBeUsed = olFieldRel=="000" ;
			else
			{
				if (ropWhichOne == CString("FLDT"))
				{
					if (olFieldRel.Left(1) == CString("1"))
					{
						blCanBeUsed = true;
					}
				}
				if (ropWhichOne == CString("FLDA"))
				{
					if (olFieldRel.Mid(1, 1) == CString("1"))
					{
						blCanBeUsed = true;
					}
				}
				if (ropWhichOne == CString("FLDD"))
				{
					if (olFieldRel.Right(1) == CString("1"))
					{
						blCanBeUsed = true;
					}
				}
			}
		}
		

		if (blCanBeUsed == true)				
		{
			//--- check if urno is already in use in the right table
			for(int j = 0; j < olUsedUrnosArr.GetSize(); j++)
			{
				 if (olUrno == olUsedUrnosArr[j])
				 {
					// This urno has been found. There's no need to continue searching.
					 blFound = true;			
					 break;	// jump out of the inner loop	(Conventions, I know. But it's quick...!)
				 }
			}

			if (blFound == false)
			{
				//--- Neue Zeile
				popLeftTable->InsertRows(ilRectOrdPos, 1);

				CString olRealName;
				CString olFieldName;
					
				CString olTable;
				RecordSet olTsrRecord;
				ogBCD.GetRecord("TSR", i, olTsrRecord);
				//olRealName = olTsrRecord.Values[igTsrNameIdx];
				if (!GetNameOfTSRRecord ( olUrno, olRealName ) )
					TRACE("Keinen Namen f�r TSR-Satz mit Urno=%s gefunden!\n", olUrno );
				olFieldName = olTsrRecord.Values[igTsrBfldIdx];
				olTable = olTsrRecord.Values[igTsrBtabIdx];

				popLeftTable->SetValueRange(CGXRange(ilRectOrdPos, 1), olFieldName);  
				popLeftTable->SetValueRange(CGXRange(ilRectOrdPos, 2), olRealName);  
				popLeftTable->SetValueRange(CGXRange(ilRectOrdPos, 3), olTable);  

				ilRectOrdPos++;
			}
		}
	}
		
	popLeftTable->LockUpdate(FALSE);
	popLeftTable->Redraw();

	//--  Read only anschalten
    popLeftTable ->SetReadOnly(TRUE);
	popRightTable->SetReadOnly(TRUE);

	CCS_CATCH_ALL

	return true;
}
//----------------------------------------------------------------------------------------------------------------------

void CDialog_TemplateEditor::OnVLeft2Right() 
{
	SetEvaluationOrder(1);
	bmChangeFlag = true;
	m_SaveBtn.EnableWindow(TRUE);
}
//----------------------------------------------------------------------------------------------------------------------

void CDialog_TemplateEditor::OnHLeft2Right() 
{
	SetEvaluationOrder(2);
	bmChangeFlag = true;
	m_SaveBtn.EnableWindow(TRUE);
}
//----------------------------------------------------------------------------------------------------------------------

void CDialog_TemplateEditor::OnHRight2Left() 
{
	SetEvaluationOrder(4);
	bmChangeFlag = true;
	m_SaveBtn.EnableWindow(TRUE);
}
//----------------------------------------------------------------------------------------------------------------------

void CDialog_TemplateEditor::OnVRight2Left() 
{
	SetEvaluationOrder(3);
	bmChangeFlag = true;
	m_SaveBtn.EnableWindow(TRUE);
}
//----------------------------------------------------------------------------------------------------------------------

void CDialog_TemplateEditor::OnDelete() 
{
	CCS_TRY
	
	CString olFisu, olFisu2, olCorrespUrno;
	int ilSel=IDNO; 
	if ( !omCurrentUrno.IsEmpty() )
	{
		olFisu = ogBCD.GetField("TPL", "URNO", omCurrentUrno, "FISU");
		olCorrespUrno = GetCorrespTplUrno ( omCurrentUrno );
		if ( !olCorrespUrno.IsEmpty() )
			olFisu2 = ogBCD.GetField("TPL", "URNO", olCorrespUrno, "FISU");
	}
	if ( (olFisu == "1") || (olFisu2 == "1") )
	{
		ilSel = MessageBox(GetString(1490), GetString(1488), MB_YESNO | MB_ICONQUESTION); //hag990913
		if ( ilSel == IDNO )
			return;
	}

	AfxGetApp()->DoWaitCursor(1);	

	// really delete  ?
	CString olText = GetString(1434);
	CString olReplace;

	// m_TemplateList.GetLBText(m_TemplateList.GetCurSel(), olReplace);
	m_TemplateList.GetLBText(m_TemplateList.GetCurSel(), olReplace);

	olText.Replace("%%", olReplace);
	if ( ilSel != IDYES )
		ilSel = AfxMessageBox(olText, MB_YESNO);
    if(ilSel == IDYES)
	{
		bool blOk = true;
			
		if ( !omCurrentUrno.IsEmpty() )
		{
			//  alle Regeln zu diesem Template l�schen 
			int ilRet = ogDataSet.OnDeleteTpl ( omCurrentUrno );		//hag990913
			//--- L�schen aus DB
			if ( !ilRet )
				blOk = ogBCD.DeleteRecord("TPL", "URNO", omCurrentUrno, true);
			else
			{
				// archive used record
				blOk = ogBCD.SetField("TPL", "URNO", omCurrentUrno, "TPST", "2", true);
			}
		}
		//  zugeh�riges 2. Template l�schen
		if ( !olCorrespUrno.IsEmpty() )
		{
			//  alle Regeln zu diesem Template l�schen 
			int ilRet = ogDataSet.OnDeleteTpl ( olCorrespUrno );		//hag990913
			//--- L�schen aus DB
			if ( !ilRet )
				blOk &= ogBCD.DeleteRecord("TPL", "URNO", olCorrespUrno, true);
			else
			{
				// archive used record
				blOk &= ogBCD.SetField("TPL", "URNO", olCorrespUrno, "TPST", "2", true);
			}
		}
		 //--- L�schen aus Combo Box 
		if ( blOk )
		{
			RecordSet olRecord;
			// set flag and disable save button
			bmChangeFlag = false;
			bmRenamed = false;
			m_SaveBtn.EnableWindow(FALSE);

			if ( !ogBCD.GetRecord ( "TPL", "TNAM", olReplace, olRecord ) )
			{	//  Zu diesem Namen gibt es kein Template mehr
				// int ilPos = m_TemplateList.GetCurSel();
				// m_TemplateList.DeleteString(ilPos); 
				// m_TemplateList.SetCurSel(0); 
				int ilPos = m_TemplateList.GetCurSel();
				m_TemplateList.DeleteString(ilPos); 
				m_TemplateList.SetCurSel(0); 
			}			
			//--- Setzen auf Position 1 
			OnSelchangeTemplateName();
		}
	}
	
	AfxGetApp()->DoWaitCursor(0);

	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------------------------------------------------

void CDialog_TemplateEditor::OnSave() 
{
	DoSave();
}
//----------------------------------------------------------------------------------------------------------------------

bool CDialog_TemplateEditor::DoSave() 
{
	bool blErr = false;

	CCS_TRY


	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	 // new record
	 RecordSet olRecord(ogBCD.GetFieldCount("TPL"));	 
	 
 	 bool		blUpdate = false;
	 CString	olName ;

	 //--- check if a template is chosen
	if (imCurrSel == -1)
	{
		CDialog_Neuer_Name  olDialog;
		if(olDialog.DoModal() == IDOK) 
		{
			olName = olDialog.m_Name;
		}
	}
	else
	{
		CString olUrno;
		int ilMsgId = 0;
		// int ilSel = m_TemplateList.GetCurSel();
		// m_TemplateList.GetLBText(ilSel, olName );
		//int ilSel = m_TemplateList.GetCurSel();
		//m_TemplateList.GetLBText(ilSel, olName );
		m_TemplateList.GetWindowText( olName );
		olName.TrimRight();
		if ( bmRenamed )
		{	
			if ( olName.IsEmpty() )
				ilMsgId = 160;
			else 		// new template name already used
				if ( ogBCD.GetField("TPL", "TNAM", olName, "URNO", olUrno ) )	
					ilMsgId = 159;
			if ( ilMsgId > 0 )
			{
                 AfxMessageBox(GetString(ilMsgId)) ;
				 return false;
			}
		}

	}


	 if (omCurrentUrno != CString(""))
	 {
		ogBCD.GetRecord("TPL", "URNO", omCurrentUrno, olRecord);
		blUpdate = true;
	 }


	// DALO (default allocation)
	 int ilSel = m_Cob_Alod.GetCurSel();
	 long llAloUrno = m_Cob_Alod.GetItemData(ilSel);
	 if (llAloUrno != CB_ERR)
	 {
		CString olAloUrno;
		CString olAloCode;
		olAloUrno.Format("%d", llAloUrno);
		olAloCode = ogBCD.GetField("ALO", "URNO", olAloUrno, "ALOC"); 
		olRecord.Values[igTplDaloIdx] = olAloCode;
	 }
	 else
	 {
		MessageBox(GetString(1515), GetString(1488), MB_OK | MB_ICONEXCLAMATION);	
		return false; 
	 }

	 
	int ilRotRowCount = pomRightRotGrid->GetRowCount();
	int ilArrRowCount = pomRightArrGrid->GetRowCount();
	int ilDepRowCount = pomRightDepGrid->GetRowCount();
	CString olFieldVal, olShortName, olTable;

//--- build string for FLDT
	CString  olFLDT;
	int  i = 1;
	while (i <= ilRotRowCount) 
	{
		olFieldVal = pomRightRotGrid->GetValueRowCol(i, 1); 
		olShortName = pomRightRotGrid->GetValueRowCol(i, 2); 
		olTable = pomRightRotGrid->GetValueRowCol(i, 4); 

		olFLDT += olFieldVal + "."  + olTable + "." + olShortName;
        i++;

		if (i <= ilRotRowCount)
		{
		    olFLDT += ";";
		}
	}



//--- build string for FLDA
	CString  olFLDA;
	i = 1;
	if (!imFlugUnabhaengig)
	{
		while(i <= ilArrRowCount) 
		{
			olFieldVal = pomRightArrGrid->GetValueRowCol(i, 1); 
			olShortName = pomRightArrGrid->GetValueRowCol(i, 2); 
			olTable = pomRightArrGrid->GetValueRowCol(i, 4); 

			olFLDA +=  olFieldVal + "."  + olTable    + "."  + olShortName;
			i++;

			if(i <= ilArrRowCount)
			{
				olFLDA += ";";
			}
		}
	}

//--- build string for FLDD
	 CString  olFLDD;
	 i = 1;
	 if (!imFlugUnabhaengig)
	 {
		 while(i <= ilDepRowCount) 
		 {
			 olFieldVal = pomRightDepGrid->GetValueRowCol(i, 1); 
			 olShortName = pomRightDepGrid->GetValueRowCol(i, 2); 
			 olTable = pomRightDepGrid->GetValueRowCol(i, 4); 

			 olFLDD +=  olFieldVal + "."  + olTable    + "."  + olShortName;
			 i++;

			 if(i <= ilDepRowCount)
			 {
				olFLDD += ";";
			 }
		 }
	 }

//--- evaluation order
	CString olEvalOrder = CString("1");

	if (!imFlugUnabhaengig)
	{
		UINT ilVL2R = pomVL2RBtn->GetState() & 0x0004;
		UINT ilHL2R = pomHL2RBtn->GetState() & 0x0004;
		UINT ilVR2L = pomVR2LBtn->GetState() & 0x0004;
		UINT ilHR2L = pomHR2LBtn->GetState() & 0x0004;

		if (ilVL2R != 0)
		{
			olEvalOrder = CString("1");
		}
		else if (ilHL2R != 0)
		{
			olEvalOrder = CString("2");
		}
		else if (ilVR2L != 0)
		{	
			olEvalOrder = CString("3");
		}
		else if (ilHR2L != 0)
		{
			olEvalOrder = CString("4");
		}
		else
		{
			MessageBox(GetString(1432), GetString(1433), MB_OK | MB_ICONEXCLAMATION);
			return false;
		}
	}

	CString olFtypes;
	char	clFtyp;

	olFtypes.Empty();
	for ( i=0; i<m_FtypesInLB.GetCount(); i++ )
	{
		if ( m_FtypesInLB.GetSel ( i ) )
		{
			clFtyp = (char)m_FtypesInLB.GetItemData(i); 	
			olFtypes += clFtyp;
		}
	}
	if ( igTplFtyaIdx >= 0 )
		olRecord.Values[igTplFtyaIdx] = olFtypes;

	olFtypes.Empty();
	for ( i=0; i<m_FtypesOutLB.GetCount(); i++ )
	{
		if ( m_FtypesOutLB.GetSel ( i ) )
		{
			clFtyp = (char)m_FtypesOutLB.GetItemData(i); 	
			olFtypes += clFtyp;
		}
	}
	if ( igTplFtydIdx >= 0 )
		olRecord.Values[igTplFtydIdx] = olFtypes;

	if (!blUpdate)
	{
		olRecord.Values[igTplTnamIdx] = olName;
		olRecord.Values[igTplFisuIdx] = CString("0");
	}
	if ( bmRenamed )
		olRecord.Values[igTplTnamIdx] = olName;
	
	// make sure there are no NULL-values given to CEDA
	if (olFLDT.IsEmpty() == TRUE)
	{
		olFLDT = CString(" ");
	}
	if (olFLDA.IsEmpty() == TRUE)
	{
		olFLDA = CString(" ");
	}
	if (olFLDD.IsEmpty() == TRUE)
	{
		olFLDD = CString(" ");
	}
	
	// MNE 000920
	// There are rules based on this template, or at least 
	// there used to be (TPL.FISU will never be reset once it is set to 1, 
	// i.e. the rules may have been deleted already, but we wouldn't know)
	if (imFisu == 1)  
	{
		

		if (!CheckAndChangePrioStrings(olFLDT, olFLDA, olFLDD, atoi(olEvalOrder)))
		{
			Beep(800,200);
			return false;
		}
	}
	// END MNE

	ilSel = m_Cob_Relx.GetCurSel();
	if (ilSel != -1)
	{
		CString olText;
		m_Cob_Relx.GetLBText(ilSel, olText);

		if (olText == GetString(2001))
		{
			olRecord.Values[igTplRelxIdx] = CString("REG");
		}
		else
		{
			olRecord.Values[igTplRelxIdx] = CString("AFT");
		}
	}
	
	

	// write values to record
	olRecord.Values[igTplFldtIdx] = olFLDT;
	olRecord.Values[igTplFldaIdx] = olFLDA;
	olRecord.Values[igTplFlddIdx] = olFLDD;
	olRecord.Values[igTplEvorIdx] = olEvalOrder;
	if ( igTplApplIdx >= 0 )
		olRecord.Values[igTplApplIdx] = imFlugUnabhaengig ? "RULE_FIR" 
														  : "RULE_AFT";

	// TPST (template status)
	if (m_Chb_Deacti.GetCheck() == CHECKED)
	{
		olRecord.Values[igTplTpstIdx] = CString("0");
	}
	else
	{
		olRecord.Values[igTplTpstIdx] = CString("1");
	}
	
	if (blUpdate == true)
	{
		//  Can lead to Assertion inside TRACE although everything is ok
		//TRACE("[TemplateEditor::DoSave] FLDT=%s\n", olFLDT);
		//TRACE("FLDA=%s\n", olFLDA);
		//TRACE("FLDD=%s\n", olFLDD);
		if ( bmRenamed )
		{	//  Change name of corresponding template
			CString olCorresUrno = GetCorrespTplUrno ( omCurrentUrno );
			if ( !olCorresUrno.IsEmpty () )
				blErr = ogBCD.SetField("TPL", "URNO", olCorresUrno, "TNAM", olName, false);
		}
		blErr &= ogBCD.SetRecord("TPL", "URNO", omCurrentUrno, olRecord.Values, true );
		if ( blErr && (imCurrSel>=0) )
		{
			m_TemplateList.DeleteString(imCurrSel); 
			m_TemplateList.InsertString(imCurrSel, olName); 
		}
	}
	else
	{
		omCurrentUrno.Format("%ld", ogBCD.GetNextUrno() );
		olRecord.Values[igTplUrnoIdx] = omCurrentUrno;
		blErr = ogBCD.InsertRecord("TPL", olRecord, true );
		if (blErr)
		{
			SaveCorrespTpl (omCurrentUrno);
		}
		else
		{
			omCurrentUrno = "";
		}
	}
	
	//--- reset change flag
	m_SaveBtn.EnableWindow(FALSE);
	bmChangeFlag = false;
	bmRenamed = false;
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	CCS_CATCH_ALL
	
	return blErr;
}
//----------------------------------------------------------------------------------------------------------------------

void CDialog_TemplateEditor::OnClose() 
{ 
	if (SaveChanges())
	{
		CDialog::OnCancel();
	}
}
//----------------------------------------------------------------------------------------------------------------------

void CDialog_TemplateEditor::OnRotAdd() 
{
	//*** MNE 000920
	OnGridRowAdd(pomLeftRotGrid, pomRightRotGrid);


	/*CCS_TRY

	 //--- Aus linker Liste entfernen 
     pomLeftRotGrid->SetReadOnly(FALSE);
     pomRightRotGrid->SetReadOnly(FALSE);

	 //--- ilCountahl links ausgew�hlter Zeilen
	 CRowColArray  olArray;
     pomLeftRotGrid->GetSelectedRows(olArray);

	 int  ilLeftRowCount  = olArray.GetSize();
	
	 int ilCurrCount = 0; 
	 if (CheckTotalItemCount(ilCurrCount, ilLeftRowCount) == true)
	 {
		 //--- Zahl rechts vorhandene Zeilen 
		 int  ilRightRowCount = pomRightRotGrid->GetRowCount();
		 
		 if (ilRightRowCount + ilLeftRowCount <= 50)
		 {
			 //--- markierte Zeilen von linker Seite rechts anh�ngen 
			 for(int  i = 0; i < ilLeftRowCount; i++) 
			 {
				 int  pos = olArray[i];

				 if(pos == 0)
					 continue;

				 CString  olFieldName = pomLeftRotGrid->GetValueRowCol(pos, 1); 
				 CString  olRealName = pomLeftRotGrid->GetValueRowCol(pos, 2); 
				 CString  olTable    = pomLeftRotGrid->GetValueRowCol(pos, 3); 

	 			 ilRightRowCount++;
				 pomRightRotGrid->InsertRows(ilRightRowCount, 1);

				 pomRightRotGrid->SetValueRange(CGXRange(ilRightRowCount, 1), (WORD) ilRightRowCount);  
				 pomRightRotGrid->SetValueRange(CGXRange(ilRightRowCount, 2), olFieldName);  
				 pomRightRotGrid->SetValueRange(CGXRange(ilRightRowCount, 3), olRealName);  
				 pomRightRotGrid->SetValueRange(CGXRange(ilRightRowCount, 4), olTable );  
			 }

			 //--- markierte Zeile links l�schen
			 pomLeftRotGrid->GetSelectedRows(olArray);

			 while(ilLeftRowCount) 
			 {
					ilLeftRowCount--;

					int pos = olArray[ilLeftRowCount];

					if(pos == 0)
						continue;

					BOOL ilErr = pomLeftRotGrid->RemoveRows(pos, pos);
			 }
		 }
		 else
		 {
			 MessageBox(GetString(1462), GetString(1433), MB_OK | MB_ICONEXCLAMATION);				
		 }

		 pomLeftRotGrid->Redraw();
		 pomRightRotGrid->Redraw();

		 pomLeftRotGrid->SetReadOnly(TRUE);
		 pomRightRotGrid->SetReadOnly(TRUE);

		 //--- Change Flag setzen
		 imCurrSel = m_TemplateList.GetCurSel();
		 m_SaveBtn.EnableWindow(TRUE);
		 bmChangeFlag = true;

		 pomLeftRotGrid->Redraw();
		 pomRightRotGrid->Redraw();
	 }
	 else
	 {
		Beep(800, 200);
	 }

	CCS_CATCH_ALL*/
}
//----------------------------------------------------------------------------------------------------------------------

void CDialog_TemplateEditor::OnRotDelete() 
{
	OnGridRowDelete(pomLeftRotGrid, pomRightRotGrid);


	/*CCS_TRY

	 //--- Aus linker Liste entfernen 
     pomLeftRotGrid->SetReadOnly(FALSE);
     pomRightRotGrid->SetReadOnly(FALSE);

	 //--- ilCountahl links ausgew�hlter Zeilen
	 CRowColArray  olArray;
     pomRightRotGrid->GetSelectedRows(olArray);

	 int  ilRightRowCount = olArray.GetSize();

	 //--- Zahl rechts vorhandene Zeilen 
	 int  ilLeftRowCount = pomLeftRotGrid->GetRowCount();


	 //--- markierte Zeilen von rechter Seite links anh�ngen 
	 for(int  i = 0; i < ilRightRowCount; i++) 
	 {
         int  pos = olArray[i];

		 if(pos == 0)
			 continue;

		 CString olFieldName = pomRightRotGrid->GetValueRowCol(pos, 2); 
		 CString olRealName = pomRightRotGrid->GetValueRowCol(pos, 3); 
		 CString olTable = pomRightRotGrid->GetValueRowCol(pos, 4); 

		 ilLeftRowCount++;
	     pomLeftRotGrid->InsertRows(ilLeftRowCount, 1);
                                    
         pomLeftRotGrid->SetValueRange(CGXRange(ilLeftRowCount, 1), olFieldName);  
         pomLeftRotGrid->SetValueRange(CGXRange(ilLeftRowCount, 2), olRealName);  
         pomLeftRotGrid->SetValueRange(CGXRange(ilLeftRowCount, 3), olTable);  
	 }

	
	 //--- markierte Zeilen rechts l�schen
	 CString olMsg, olRealName;
	 pomRightRotGrid->GetSelectedRows(olArray);
	 while (ilRightRowCount) 
	 {
		    ilRightRowCount--;

            int pos = olArray[ilRightRowCount];

		    if(pos == 0)
			    continue;
			
			// MNE 000920
			if (imFisu == 1)
			{
				if (!IsRowRemovable(pomRightRotGrid, pos))
				{
					// this row can't be removed coz
					// there's existing rules using the field
					ShowCannotRemoveFieldMsg(pomRightRotGrid, pos);
					continue;	
				}
			}
			// END MNE

			//--- Aktuelle Wertigkeit merken 
            int ilValue = (int) atoi(pomRightRotGrid->GetValueRowCol(pos, 1));

			//--- l�schen
            BOOL ilErr = pomRightRotGrid->RemoveRows(pos,  pos);

			//--- Rest neu numerieren 
	        int  ilRightRowCountNeu = pomRightRotGrid->GetRowCount();

	        for(int z = 1; z <= ilRightRowCountNeu; z++) 
			{
                 int ilPlaceValue = (int) atoi(pomRightRotGrid->GetValueRowCol(z, 1));

				 if(ilPlaceValue  > ilValue)
                     pomRightRotGrid->SetValueRange(CGXRange(z, 1), 
					                              (WORD) (ilPlaceValue - 1));  
			}
	 }



	 //--- links  neu sortieren       
      CGXSortInfoArray  sortInfo;
                        sortInfo.SetSize(1)    ;                       
                        sortInfo[0].nRC = pomLeftRotGrid->GetSortKey() ;                       
                        sortInfo[0].sortType = CGXSortInfo::autodetect;  
						sortInfo[0].sortOrder = CGXSortInfo::ascending;

      pomLeftRotGrid->SortRows(CGXRange().SetTable(), sortInfo); 


	 pomLeftRotGrid->Redraw();
     pomRightRotGrid->Redraw();

	 //--- reset read 
     pomLeftRotGrid->SetReadOnly(TRUE);
     pomRightRotGrid->SetReadOnly(TRUE);

	 //--- Change Flag setzen
	 imCurrSel = m_TemplateList.GetCurSel();
	 m_SaveBtn.EnableWindow(TRUE);
     bmChangeFlag = true;
	
	CCS_CATCH_ALL*/
}
//----------------------------------------------------------------------------------------------------------------------


//-------------------------------------
//   Reaktionen Buttons in Arrival
//-------------------------------------
void CDialog_TemplateEditor::OnArrAdd() 
{
	//*** MNE 000920
	OnGridRowAdd(pomLeftArrGrid, pomRightArrGrid);
		
	//-- exception handling
	/*CCS_TRY

	 //--- Aus linker Liste entfernen 
     pomLeftArrGrid->SetReadOnly(FALSE);
     pomRightArrGrid->SetReadOnly(FALSE);

	 //--- ilCountahl links ausgew�hlter Zeilen
	 CRowColArray  olArray;
     pomLeftArrGrid->GetSelectedRows(olArray);

	 int  ilLeftRowCount  = olArray.GetSize();

	 int ilCurrCount = 0;
	 if (CheckTotalItemCount(ilCurrCount, ilLeftRowCount) == true)
	 {
		 //--- Anzahl rechts vorhandene Zeilen 
		 int  ilRightRowCount = pomRightArrGrid->GetRowCount();
		 
		 if (ilRightRowCount + ilLeftRowCount <= 50)
		 {
			 //--- markierte Zeilen von linker Seite olRecthts anh�ngen 
			 for(int  i = 0; i < ilLeftRowCount; i++) 
			 {
				 int  pos = olArray[i];

				 if(pos == 0)
					 continue;

				 CString  olFieldName = pomLeftArrGrid->GetValueRowCol(pos, 1); 
				 CString  olRealName = pomLeftArrGrid->GetValueRowCol(pos, 2); 
				 CString  olTable    = pomLeftArrGrid->GetValueRowCol(pos, 3); 

	 										ilRightRowCount++;
				 pomRightArrGrid->InsertRows(ilRightRowCount, 1);

				 pomRightArrGrid->SetValueRange(CGXRange(ilRightRowCount, 1), (WORD) ilRightRowCount);  
				 pomRightArrGrid->SetValueRange(CGXRange(ilRightRowCount, 2), olFieldName);  
				 pomRightArrGrid->SetValueRange(CGXRange(ilRightRowCount, 3), olRealName);  
				 pomRightArrGrid->SetValueRange(CGXRange(ilRightRowCount, 4), olTable );  

			 }


			 //--- markierte Zeile links l�schen
			 pomLeftArrGrid->GetSelectedRows(olArray);

			 while(ilLeftRowCount) 
			 {
					ilLeftRowCount --;

					int pos = olArray[ilLeftRowCount];

					if(pos == 0)
						continue;

					BOOL ilErr = pomLeftArrGrid->RemoveRows(pos,  pos);
			 }
		 }
		 else
		 {
			MessageBox(GetString(1462), GetString(1433), MB_OK | MB_ICONEXCLAMATION);				
		 }

		 pomLeftArrGrid->Redraw();
		 pomRightArrGrid->Redraw();

		 pomLeftArrGrid->SetReadOnly(TRUE);
		 pomRightArrGrid->SetReadOnly(TRUE);

		 //--- Change Flag setzen
		 imCurrSel = m_TemplateList.GetCurSel();
		 m_SaveBtn.EnableWindow(TRUE);
		 bmChangeFlag = true;
	 }

	CCS_CATCH_ALL*/
}
//----------------------------------------------------------------------------------------------------------------------


void CDialog_TemplateEditor::OnArrDelete() 
{
	OnGridRowDelete(pomLeftArrGrid, pomRightArrGrid);


	/*CCS_TRY

	 //--- Aus linker Liste entfernen 
     pomLeftArrGrid->SetReadOnly(FALSE);
     pomRightArrGrid->SetReadOnly(FALSE);

	 //--- ilCountahl links ausgew�hlter Zeilen
	 CRowColArray  olArray;
     pomRightArrGrid->GetSelectedRows(olArray);

	 int  ilRightRowCount = olArray.GetSize();

	 //--- ilCountahl olRecthts vorhandene Zeilen 
	 int  ilLeftRowCount = pomLeftArrGrid->GetRowCount();


	 //--- markierte Zeilen von olRecthter Seite links anh�ngen 
	 for(int  i = 0; i < ilRightRowCount; i++) 
	 {
         int  pos = olArray[i];

		 if(pos == 0)
			 continue;

		 CString  olFieldName = pomRightArrGrid->GetValueRowCol(pos, 2); 
		 CString  olRealName = pomRightArrGrid->GetValueRowCol(pos, 3); 
		 CString  olTable    = pomRightArrGrid->GetValueRowCol(pos, 4); 

	 	 ilLeftRowCount++;
	     pomLeftArrGrid->InsertRows(ilLeftRowCount, 1);
                                    
         pomLeftArrGrid->SetValueRange(CGXRange(ilLeftRowCount, 1), olFieldName);  
         pomLeftArrGrid->SetValueRange(CGXRange(ilLeftRowCount, 2), olRealName);  
         pomLeftArrGrid->SetValueRange(CGXRange(ilLeftRowCount, 3), olTable );  

	 }

	 //--- markierte Zeilen rechts l�schen
	 pomRightArrGrid->GetSelectedRows(olArray);

	 while(ilRightRowCount) 
	 {
		    ilRightRowCount--;

            int pos = olArray[ilRightRowCount];

		    if(pos == 0)
			    continue;

			//--- Aktuelle Wertigkeit merken 
            int ilValue = (int) atoi(pomRightArrGrid->GetValueRowCol(pos, 1));

			//--- l�schen
            BOOL ilErr = pomRightArrGrid->RemoveRows(pos,  pos);

			//--- Rest neu numerieren 
	        int  ilRightRowCountNeu = pomRightArrGrid->GetRowCount();

	        for(int z = 1; z <= ilRightRowCountNeu; z++) 
			{
                 int ilPlaceValue = (int) atoi(pomRightArrGrid->GetValueRowCol(z, 1));

				 if(ilPlaceValue  > ilValue)
                     pomRightArrGrid->SetValueRange(CGXRange(z, 1), 
					                              (WORD) (ilPlaceValue - 1));  
			}

	 }

	 //--- links  neu sortieren       
      CGXSortInfoArray  sortInfo;
                        sortInfo.SetSize(1)    ;                       
                        sortInfo[0].nRC = pomLeftArrGrid->GetSortKey() ;                       
                        sortInfo[0].sortType = CGXSortInfo::autodetect;  
						sortInfo[0].sortOrder = CGXSortInfo::ascending;

     pomLeftArrGrid->SortRows(CGXRange().SetTable(), sortInfo); 

	 pomLeftArrGrid->Redraw();
     pomRightArrGrid->Redraw();

	 //--- reset read 
     pomLeftArrGrid->SetReadOnly(TRUE);
     pomRightArrGrid->SetReadOnly(TRUE);

	 //--- Change Flag setzen
	 imCurrSel = m_TemplateList.GetCurSel();
	 m_SaveBtn.EnableWindow(TRUE);
     bmChangeFlag = true;

	CCS_CATCH_ALL*/
}
//-----------------------------------------------------------------------------------------------------------------------



//   Function is called when button "remove from departure grid" is pressed
void CDialog_TemplateEditor::OnDepAdd() 
{
	//*** MNE 000920
	OnGridRowAdd(pomLeftDepGrid, pomRightDepGrid);

	/*CCS_TRY

	 //--- Aus linker Liste entfernen 
     pomLeftDepGrid->SetReadOnly(FALSE);
     pomRightDepGrid->SetReadOnly(FALSE);

	 //--- Zahl links ausgew�hlter Zeilen
	 CRowColArray  olArray;
     pomLeftDepGrid->GetSelectedRows(olArray);

	 int ilLeftRowCount = olArray.GetSize();

	 int ilCurrCount = 0;
	 
	 if (CheckTotalItemCount(ilCurrCount, ilLeftRowCount) == true)
	 {
		 //--- Anzahl rechts vorhandene Zeilen 
		 int ilRightRowCount = pomRightDepGrid->GetRowCount();

		 if (ilRightRowCount + ilLeftRowCount <= 50)
		 {
			 //--- markierte Zeilen von linker Seite olRecthts anh�ngen 
			 for(int  i = 0; i < ilLeftRowCount; i++) 
			 {
				 int  pos = olArray[i];

				 if(pos == 0)
					 continue;

				 CString  olFieldName = pomLeftDepGrid->GetValueRowCol(pos, 1); 
				 CString  olRealName = pomLeftDepGrid->GetValueRowCol(pos, 2); 
				 CString  olTable    = pomLeftDepGrid->GetValueRowCol(pos, 3); 

	 			 ilRightRowCount++;
				 pomRightDepGrid->InsertRows(ilRightRowCount, 1);

				 pomRightDepGrid->SetValueRange(CGXRange(ilRightRowCount, 1), (WORD) ilRightRowCount);  
				 pomRightDepGrid->SetValueRange(CGXRange(ilRightRowCount, 2), olFieldName);  
				 pomRightDepGrid->SetValueRange(CGXRange(ilRightRowCount, 3), olRealName);  
				 pomRightDepGrid->SetValueRange(CGXRange(ilRightRowCount, 4), olTable);  
			 }


			 //--- markierte Zeile links l�schen
			 pomLeftDepGrid->GetSelectedRows(olArray);

			 while(ilLeftRowCount) 
			 {
					ilLeftRowCount--;

					int pos = olArray[ilLeftRowCount];

					if(pos == 0)
						continue;

					BOOL ilErr = pomLeftDepGrid->RemoveRows(pos,  pos);
			 }
		 }
		 else
		 {
			MessageBox(GetString(1462), GetString(1433), MB_OK | MB_ICONEXCLAMATION);				
		 }

		 pomLeftDepGrid->Redraw();
		 pomRightDepGrid->Redraw();

		 pomLeftDepGrid->SetReadOnly(TRUE);
		 pomRightDepGrid->SetReadOnly(TRUE);

		 //--- Change Flag setzen
		 imCurrSel = m_TemplateList.GetCurSel();
		 m_SaveBtn.EnableWindow(TRUE);
		 bmChangeFlag = true;
		
		 pomLeftDepGrid->Redraw();
		 pomRightDepGrid->Redraw();
	 }

	CCS_CATCH_ALL*/
}
//-----------------------------------------------------------------------------------------------------------------------

void CDialog_TemplateEditor::OnDepDelete() 
{

	OnGridRowDelete(pomLeftDepGrid, pomRightDepGrid);
	


	/*CCS_TRY

	//--- Aus linker Liste entfernen 
    pomLeftDepGrid->SetReadOnly(FALSE);
    pomRightDepGrid->SetReadOnly(FALSE);

	//--- ilCountahl links ausgew�hlter Zeilen
	CRowColArray  olArray;
    pomRightDepGrid->GetSelectedRows(olArray);

	int  ilRightRowCount  = olArray.GetSize();

	//--- ilCountahl olRecthts vorhandene Zeilen 
	int  ilLeftRowCount = pomLeftDepGrid->GetRowCount();


	//--- markierte Zeilen von olRechter Seite links anh�ngen 
	for(int  i = 0; i < ilRightRowCount; i++) 
	{
		int  pos = olArray[i];

		if(pos == 0)
			continue;

		CString olFieldName = pomRightDepGrid->GetValueRowCol(pos, 2); 
		CString olRealName = pomRightDepGrid->GetValueRowCol(pos, 3); 
		CString olTable    = pomRightDepGrid->GetValueRowCol(pos, 4); 

		ilLeftRowCount++;
	    pomLeftDepGrid->InsertRows(ilLeftRowCount, 1);
                                   

        pomLeftDepGrid->SetValueRange(CGXRange(ilLeftRowCount, 1), olFieldName);  
        pomLeftDepGrid->SetValueRange(CGXRange(ilLeftRowCount, 2), olRealName);  
        pomLeftDepGrid->SetValueRange(CGXRange(ilLeftRowCount, 3), olTable );  

	}

	//--- markierte Zeilen olRechts l�schen
	pomRightDepGrid->GetSelectedRows(olArray);

	while(ilRightRowCount) 
	{
		ilRightRowCount--;

        int pos = olArray[ilRightRowCount];

		if(pos == 0)
		    continue;

		
		// MNE 000920
		if (imFisu == 1)
		{
			if (!IsRowRemovable(pomRightDepGrid, pos))
			{
				// this row can't be removed coz
				// there's existing rules using the field
				ShowCannotRemoveFieldMsg(pomRightRotGrid, pos);
				continue;	
			}
		}
		// END MNE


		//--- Aktuelle Wertigkeit merken 
        int ilValue = (int) atoi(pomRightDepGrid->GetValueRowCol(pos, 1));

			//--- l�schen
        BOOL ilErr = pomRightDepGrid->RemoveRows(pos,  pos);

			//--- Rest neu numerieren 
	    int  ilRightRowCountNeu = pomRightDepGrid->GetRowCount();

	    for(int z = 1; z <= ilRightRowCountNeu; z++) 
		{
			int ilPlaceValue = (int) atoi(pomRightDepGrid->GetValueRowCol(z, 1));

			if(ilPlaceValue  > ilValue)
				pomRightDepGrid->SetValueRange(CGXRange(z, 1), 
				                              (WORD) (ilPlaceValue - 1));  
		}

	}

	//--- links  neu sortieren       
    CGXSortInfoArray  sortInfo;
    sortInfo.SetSize(1)    ;                       
    sortInfo[0].nRC = pomLeftArrGrid->GetSortKey() ;                       
    sortInfo[0].sortType = CGXSortInfo::autodetect;  
	sortInfo[0].sortOrder = CGXSortInfo::ascending;

    pomLeftDepGrid->SortRows(CGXRange().SetTable(), sortInfo); 

	pomLeftDepGrid->Redraw();
    pomRightDepGrid->Redraw();

	//--- reset read 
    pomLeftDepGrid->SetReadOnly(TRUE);
    pomRightDepGrid->SetReadOnly(TRUE);

	//--- Change Flag setzen
	imCurrSel = m_TemplateList.GetCurSel();
	m_SaveBtn.EnableWindow(TRUE);
    bmChangeFlag = true;

	pomLeftDepGrid->Redraw();
    pomRightDepGrid->Redraw();

	CCS_CATCH_ALL*/
}
//-----------------------------------------------------------------------------------------------------------------------

void CDialog_TemplateEditor::OnSelchangeTemplateName() 
{
	CCS_TRY

	int ilSelected = m_TemplateList.GetCurSel();
	if ( ilSelected == imCurrSel )
	{
		bmRenamed = false;
		m_SaveBtn.EnableWindow(bmChangeFlag==true);
	}
	if ( !SaveChanges () )
	{
		// m_TemplateList.SetCurSel(imCurrSel);
		m_TemplateList.SetCurSel(imCurrSel);
		return;
	}


	AfxGetApp()->DoWaitCursor(1);

	// Das Update  verhindern bis alle Tabellen fertig sind
    pomLeftRotGrid->LockUpdate(TRUE);
    pomLeftArrGrid->LockUpdate(TRUE);
	pomLeftDepGrid->LockUpdate(TRUE);

    pomRightRotGrid->LockUpdate(TRUE);
	pomRightArrGrid->LockUpdate(TRUE);
	pomRightDepGrid->LockUpdate(TRUE);


	 //---  update current selection
	// imCurrSel = m_TemplateList.GetCurSel();
	imCurrSel = ilSelected;

	RecordSet olTpl(ogBCD.GetFieldCount("TPL"));

	//--- set evaluation order
	int ilEvalOrder = 0;
	if (GetSelectedTplUrno(m_TemplateList, omCurrentUrno, imFlugUnabhaengig))
	{
		ogBCD.GetRecord("TPL", "URNO", omCurrentUrno, olTpl);
		ilEvalOrder = atoi(olTpl.Values[igTplEvorIdx]);
	}
	SetEvaluationOrder(ilEvalOrder);
	
	//--- set imFisu
	imFisu = 1;	
	if (olTpl.Values[igTplFisuIdx] == "0")
	{
		imFisu = 0;		// there are/were rules based on this template
	}
	
	//--- set template status (added by MNE 010109)
	if (olTpl.Values[igTplTpstIdx] == "0")
		m_Chb_Deacti.SetCheck(CHECKED); // deactivated, TPL.TPST = 0
	else
		m_Chb_Deacti.SetCheck(UNCHECKED);	// activated, TPL.TPST = 1

	//--- set status checkbox
	int ilTplStatus = atoi(olTpl.Values[igTplTpstIdx]);
	if (ilTplStatus == 0)
	{
		m_Chb_Deacti_Val = FALSE; // deactivated
	}
	else
	{
		m_Chb_Deacti_Val = TRUE;  // activated
	}

	//--- set allocation units
	int ilPos = CB_ERR; 
	CString olAloCode, olAlod;
	long llAloUrno;

	if (!omCurrentUrno.IsEmpty())
	{
		olAloCode = olTpl.Values[igTplDaloIdx];
		llAloUrno = atol(ogBCD.GetField("ALO", "ALOC", olAloCode, "URNO"));
		olAlod = ogBCD.GetField("ALO", "ALOC", olAloCode, "ALOD");
		ilPos = m_Cob_Alod.SelectString(-1, olAlod);
	}
	if (ilPos != CB_ERR)
		m_Cob_Alod.SetItemData(ilPos, llAloUrno);
	else
		m_Cob_Alod.Clear();
	

	//--- set relation
	CString olRelx;
	
	if (!omCurrentUrno.IsEmpty())
	{
		olRelx = olTpl.Values[igTplRelxIdx];
		// olRelx = ogBCD.GetField("TPL", "URNO", omCurrentUrno, "RELX");
		if (olRelx == "REG")
		{
			ilPos = m_Cob_Relx.SelectString(-1, GetString(2001));
		}
		else
		{
			ilPos = m_Cob_Relx.SelectString(-1, GetString(2002));
		}

		if (ilPos == CB_ERR)
		{
			m_Cob_Relx.Clear();
		}
	}

	
	//--- fill grids with new data
	ResetAllTables ();
	FillTable(pomLeftRotGrid, pomRightRotGrid,"FLDT");
	if ( !imFlugUnabhaengig )
	{
		FillTable(pomLeftArrGrid, pomRightArrGrid,"FLDA");
		FillTable(pomLeftDepGrid, pomRightDepGrid,"FLDD");
	}

    //---  left sides sort alphanumerically
    CGXSortInfoArray  sortInfo;
    sortInfo.SetSize(1)    ;                       
    sortInfo[0].nRC = 1     ;                       
    sortInfo[0].sortType = CGXSortInfo::autodetect;  
	sortInfo[0].sortOrder = CGXSortInfo::ascending;

    pomLeftRotGrid->SortRows(CGXRange().SetTable(), sortInfo); 
    pomLeftArrGrid->SortRows(CGXRange().SetTable(), sortInfo);
	pomLeftDepGrid->SortRows(CGXRange().SetTable(), sortInfo);

	//--- Update 
    pomLeftArrGrid->LockUpdate(FALSE);
    pomLeftRotGrid->LockUpdate(FALSE);
	pomLeftDepGrid->LockUpdate(FALSE);
    pomRightRotGrid->LockUpdate(FALSE);
	pomRightArrGrid->LockUpdate(FALSE);
	pomRightDepGrid->LockUpdate(FALSE);

    pomLeftArrGrid->Redraw();
    pomLeftRotGrid->Redraw();
	pomLeftDepGrid->Redraw();
    pomRightRotGrid->Redraw();
	pomRightArrGrid->Redraw();
	pomRightDepGrid->Redraw();

	AfxGetApp()->DoWaitCursor(0);
    
	bmChangeFlag = false;
	bmRenamed = false;
	m_SaveBtn.EnableWindow(FALSE);

	m_FtypesInLB.SetSel( -1, FALSE );
	m_FtypesOutLB.SetSel( -1, FALSE );
	if ( !omCurrentUrno.IsEmpty () )
	{
		CString olFtypes;
		int i;
		char	clFtyp;

		olFtypes = 	ogBCD.GetField("TPL", "URNO", omCurrentUrno, "FTYA" );
		for ( i=0; i<m_FtypesInLB.GetCount(); i++ )
		{
			clFtyp = (char)m_FtypesInLB.GetItemData(i); 	
			if ( olFtypes.Find ( clFtyp ) >= 0 )
				m_FtypesInLB.SetSel ( i );
		}
		olFtypes = 	ogBCD.GetField("TPL", "URNO", omCurrentUrno, "FTYD" );
		for ( i=0; i<m_FtypesOutLB.GetCount(); i++ )
		{
			clFtyp = (char)m_FtypesOutLB.GetItemData(i); 	
			if ( olFtypes.Find ( clFtyp ) >= 0 )
				m_FtypesOutLB.SetSel ( i );
		}
	}

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void CDialog_TemplateEditor::OnSelchangeAllocation() 
{
	// Something changed, hence Save-Button will be enabled
	bmChangeFlag = true;
	m_SaveBtn.EnableWindow(TRUE);
}
//-----------------------------------------------------------------------------------------------------------------------

void CDialog_TemplateEditor::OnSelchangeCobRelx() 
{
	// Something changed, hence Save-Button will be enabled
	bmChangeFlag=true;
	m_SaveBtn.EnableWindow(TRUE);
}
//-----------------------------------------------------------------------------------------------------------------------

void CDialog_TemplateEditor::OnDeactivate() 
{
	// Something changed, hence Save-Button will be enabled
	bmChangeFlag = true;
	m_SaveBtn.EnableWindow(TRUE);
}
//-----------------------------------------------------------------------------------------------------------------------

void CDialog_TemplateEditor::OnGridFeld(WPARAM wParam, LPARAM lParam)
{
	CGridFenster *polGrid = (CGridFenster*)wParam;
	CELLPOS *prlPos = (CELLPOS*) lParam;
	
	CString olGrid;
	if (polGrid != NULL)
	{
		if (polGrid == pomLeftArrGrid)
			olGrid	= CString("Left-Arrival-Grid");
		else if (polGrid == pomLeftDepGrid)
			olGrid	= CString("Left-Departure-Grid");
		else if (polGrid == pomLeftRotGrid)
			olGrid	= CString("Left-Rotation-Grid");
		else if (polGrid == pomRightArrGrid)
			olGrid	= CString("Right-Arrival-Grid");
		else if (polGrid == pomRightDepGrid)
			olGrid	= CString("Right-Departure-Grid");
		else if (polGrid == pomRightRotGrid)
			olGrid	= CString("Right-Rotation-Grid");
/*		PRF5468: no longer using CritErr.txt
		else
			of_catch << "Error on Grid-Click in Tempate Editor. Check OnGridFeld() in Dialog_Vorlagen_Editor" << endl;
*/
		// Something changed, hence Save-Button will be enabled
		bmChangeFlag=true;
		m_SaveBtn.EnableWindow(TRUE);
	}
	
	
	// mne test
	// CString olText;
	// olText.Format("Click in %s: Row - %d, Col - %d", olGrid, prlPos->Row, prlPos->Col);
	// AfxMessageBox(olText);
	// end mne
}
//-----------------------------------------------------------------------------------------------------------------------

void CDialog_TemplateEditor::OnEditchangeTemplateName() 
{
	// change the name
	if ( imCurrSel >= 0)
	{
		CString olOld;
		CString olNew;
		m_TemplateList.GetLBText(imCurrSel, olOld);
		m_TemplateList.GetWindowText(olNew);
		olNew.TrimRight();
		if ( olOld != olNew )
		{
			bmRenamed = true;
			m_SaveBtn.EnableWindow(TRUE);
		}
		else
		{
			bmRenamed = false;
			m_SaveBtn.EnableWindow(bmChangeFlag==true);
		}	
	}
}
//-----------------------------------------------------------------------------------------------------------------------


void CDialog_TemplateEditor::OnDropdownTemplateName() 
{
	//---   Merken der Auswahl vor Wechsel
	 // imCurrSel = m_TemplateList.GetCurSel();
	//imCurrSel = m_TemplateList.GetCurSel();

}


//----------------------------------------------------------------------------------------------------------------------
//				all the rest
//----------------------------------------------------------------------------------------------------------------------
void CDialog_TemplateEditor::OnGridRowAdd(CGridFenster *popLeftGrid, CGridFenster *popRightGrid)
{
	CCS_TRY

	 //--- Aus linker Liste entfernen 
     popLeftGrid->SetReadOnly(FALSE);
     popRightGrid->SetReadOnly(FALSE);

	 //--- Zahl links ausgew�hlter Zeilen
	 CRowColArray  olArray;
     popLeftGrid->GetSelectedRows(olArray);

	 int ilLeftRowCount = olArray.GetSize();

	 int ilCurrCount = 0;
	 
	 if (CheckTotalItemCount(ilCurrCount, ilLeftRowCount) == true)
	 {
		 //--- Anzahl rechts vorhandene Zeilen 
		 int ilRightRowCount = popRightGrid->GetRowCount();

		 if (ilRightRowCount + ilLeftRowCount <= 50)
		 {
			 //--- markierte Zeilen von linker Seite olRecthts anh�ngen 
			 for(int  i = 0; i < ilLeftRowCount; i++) 
			 {
				 int  pos = olArray[i];

				 if(pos == 0)
					 continue;

				 CString  olFieldName = popLeftGrid->GetValueRowCol(pos, 1); 
				 CString  olRealName = popLeftGrid->GetValueRowCol(pos, 2); 
				 CString  olTable    = popLeftGrid->GetValueRowCol(pos, 3); 

	 			 ilRightRowCount++;
				 popRightGrid->InsertRows(ilRightRowCount, 1);

				 popRightGrid->SetValueRange(CGXRange(ilRightRowCount, 1), (WORD) ilRightRowCount);  
				 popRightGrid->SetValueRange(CGXRange(ilRightRowCount, 2), olFieldName);  
				 popRightGrid->SetValueRange(CGXRange(ilRightRowCount, 3), olRealName);  
				 popRightGrid->SetValueRange(CGXRange(ilRightRowCount, 4), olTable);  
			 }


			 //--- markierte Zeile links l�schen
			 popLeftGrid->GetSelectedRows(olArray);

			 while(ilLeftRowCount) 
			 {
					ilLeftRowCount--;

					int pos = olArray[ilLeftRowCount];

					if(pos == 0)
						continue;

					BOOL ilErr = popLeftGrid->RemoveRows(pos,  pos);
			 }
		 }
		 else
		 {
			MessageBox(GetString(1462), GetString(1433), MB_OK | MB_ICONEXCLAMATION);				
		 }

		 popLeftGrid->Redraw();
		 popRightGrid->Redraw();

		 popLeftGrid->SetReadOnly(TRUE);
		 popRightGrid->SetReadOnly(TRUE);

		 //--- Change Flag setzen
		 // imCurrSel = m_TemplateList.GetCurSel();
		 m_SaveBtn.EnableWindow(TRUE);
		 bmChangeFlag = true;
		
		 popLeftGrid->Redraw();
		 popRightGrid->Redraw();
	 }

	CCS_CATCH_ALL
}
//----------------------------------------------------------------------------------------------------------------------



void CDialog_TemplateEditor::OnGridRowDelete(CGridFenster *popLeftGrid, CGridFenster *popRightGrid) 
{
	CCS_TRY

	//--- Aus linker Liste entfernen 
    popLeftGrid->SetReadOnly(FALSE);
    popRightGrid->SetReadOnly(FALSE);

	//--- ilCountahl links ausgew�hlter Zeilen
	CRowColArray  olArray;
    popRightGrid->GetSelectedRows(olArray);

	int  ilRightRowCount  = olArray.GetSize();

	//--- ilCountahl olRecthts vorhandene Zeilen 
	int  ilLeftRowCount = popLeftGrid->GetRowCount();
	bool blIsRemovable = true;

	//--- markierte Zeilen von olRechter Seite links anh�ngen 
	for(int  i = 0; i < ilRightRowCount; i++) 
	{
		int  pos = olArray[i];

		if(pos == 0)
			continue;
		
		// MNE 000920
		if (imFisu == 1)
		{
			blIsRemovable = IsRowRemovable(popRightGrid, pos);
			if (!blIsRemovable)
			{
				// this row can't be removed coz
				// there's existing rules using the field
				ShowCannotRemoveFieldMsg(popRightGrid, pos);
				continue;	
			}
		}
		// END MNE

		CString olFieldName = popRightGrid->GetValueRowCol(pos, 2); 
		CString olRealName = popRightGrid->GetValueRowCol(pos, 3); 
		CString olTable    = popRightGrid->GetValueRowCol(pos, 4); 

		ilLeftRowCount++;
	    popLeftGrid->InsertRows(ilLeftRowCount, 1);
                                   

        popLeftGrid->SetValueRange(CGXRange(ilLeftRowCount, 1), olFieldName);  
        popLeftGrid->SetValueRange(CGXRange(ilLeftRowCount, 2), olRealName);  
        popLeftGrid->SetValueRange(CGXRange(ilLeftRowCount, 3), olTable );  

	}

	//--- markierte Zeilen olRechts l�schen
	popRightGrid->GetSelectedRows(olArray);

	while(ilRightRowCount) 
	{
		ilRightRowCount--;

        int pos = olArray[ilRightRowCount];

		if(pos == 0)
		    continue;

		
		// MNE 000920
		if (imFisu == 1 && !blIsRemovable)
		{
			// this row can't be removed coz
			// there's existing rules using the field
			// ShowCannotRemoveFieldMsg(popRightGrid, pos);
			continue;	
		}
		// END MNE


		//--- Aktuelle Wertigkeit merken 
        int ilValue = (int) atoi(popRightGrid->GetValueRowCol(pos, 1));

			//--- l�schen
        BOOL ilErr = popRightGrid->RemoveRows(pos,  pos);

			//--- Rest neu numerieren 
	    int  ilRightRowCountNeu = popRightGrid->GetRowCount();

	    for(int z = 1; z <= ilRightRowCountNeu; z++) 
		{
			int ilPlaceValue = (int) atoi(popRightGrid->GetValueRowCol(z, 1));

			if(ilPlaceValue  > ilValue)
				popRightGrid->SetValueRange(CGXRange(z, 1), 
				                              (WORD) (ilPlaceValue - 1));  
		}

	}

	//--- links  neu sortieren       
    CGXSortInfoArray  sortInfo;
    sortInfo.SetSize(1)    ;                       
    sortInfo[0].nRC = pomLeftArrGrid->GetSortKey() ;                       
    sortInfo[0].sortType = CGXSortInfo::autodetect;  
	sortInfo[0].sortOrder = CGXSortInfo::ascending;

    popLeftGrid->SortRows(CGXRange().SetTable(), sortInfo); 

	popLeftGrid->Redraw();
    popRightGrid->Redraw();

	//--- reset read 
    popLeftGrid->SetReadOnly(TRUE);
    popRightGrid->SetReadOnly(TRUE);

	//--- Change Flag setzen
	// imCurrSel = m_TemplateList.GetCurSel();
	m_SaveBtn.EnableWindow(TRUE);
    bmChangeFlag = true;

	popLeftGrid->Redraw();
    popRightGrid->Redraw();

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

bool CDialog_TemplateEditor::CheckTotalItemCount(int &ipCount, int ipAdd /* =0 */)
{
	//--------------
	// Checks if the total item count fits into the PRIO field in RUETAB
	// Prior to adding fields to the tables on the right hand-side, you can check
	// if total item count is in a valid range by specyfying the number of rows to
	// add as second parameter
	//--------------

	int ilRotCount = pomRightRotGrid->GetRowCount();
	int ilArrCount = pomRightArrGrid->GetRowCount();
	int ilDepCount = pomRightDepGrid->GetRowCount();
	
	/*CString olLength = ogBCD.GetFieldExt("SYS", "TANA", "FINA", "RUE", "PRIO", "FELE");
	int ilLengthAllowed = atoi(olLength);*/
	int ilLengthAllowed = ogBCD.GetMaxFieldLength("RUE", "PRIO" );

	ipCount =  ilRotCount + ilArrCount + ilDepCount;
	if ((ipCount + ipAdd) > ilLengthAllowed)
	{
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------------------------------------

void CDialog_TemplateEditor::DrawBitmapButtons()
{
	CRect olFrameRect;
	CRect olBtnRect;
	CWnd *polWnd = GetDlgItem(IDC_BTN_FRAME);
	
	if (polWnd != NULL)
	{
		//--- do some calculations
		polWnd->GetWindowRect(&olFrameRect);
		ScreenToClient(olFrameRect);
		int ilBtnWidth = olFrameRect.Width() / 9;
		int ilBtnHeight = olFrameRect.Height() - olFrameRect.Height() / 4;

		//--- draw first button
		olBtnRect.top = olFrameRect.top + olFrameRect.Height() / 8;
		olBtnRect.bottom = olFrameRect.bottom - olFrameRect.Height() / 8;
		olBtnRect.left = ilBtnWidth;
		olBtnRect.right = olBtnRect.left + ilBtnWidth;
		pomVL2RBtn->AutoLoad(IDC_VL2R_BTN, this);

		//--- draw second button
		olBtnRect.left = olBtnRect.right + ilBtnWidth;
		olBtnRect.right = olBtnRect.left + ilBtnWidth;
		pomHL2RBtn->AutoLoad(IDC_HL2R_BTN, this);
		
		//--- draw third button
		olBtnRect.left = olBtnRect.right + ilBtnWidth;
		olBtnRect.right = olBtnRect.left + ilBtnWidth;
		pomVR2LBtn->AutoLoad(IDC_VR2L_BTN, this);

		//--- draw fourth button
		olBtnRect.left = olBtnRect.right + ilBtnWidth;
		olBtnRect.right = olBtnRect.left + ilBtnWidth;
		pomHR2LBtn->AutoLoad(IDC_HR2L_BTN, this);
	}
}
//----------------------------------------------------------------------------------------------------------------------

void CDialog_TemplateEditor::SetEvaluationOrder(int ipEvalOrder)
{
	  switch (ipEvalOrder)
	  {
		case (1):
			pomVL2RBtn->SetState(TRUE);
			pomHL2RBtn->SetState(FALSE);
			pomVR2LBtn->SetState(FALSE);
			pomHR2LBtn->SetState(FALSE);
			break;

		case (2):
			pomVL2RBtn->SetState(FALSE);
			pomHL2RBtn->SetState(TRUE);
			pomVR2LBtn->SetState(FALSE);
			pomHR2LBtn->SetState(FALSE);
			break;

		case (3):
			pomVL2RBtn->SetState(FALSE);
			pomHL2RBtn->SetState(FALSE);
			pomVR2LBtn->SetState(TRUE);
			pomHR2LBtn->SetState(FALSE);
			break;

		case (4):
			pomVL2RBtn->SetState(FALSE);
			pomHL2RBtn->SetState(FALSE);
			pomVR2LBtn->SetState(FALSE);
			pomHR2LBtn->SetState(TRUE);
			break;

		default:
			pomVL2RBtn->SetState(FALSE);
			pomHL2RBtn->SetState(FALSE);
			pomVR2LBtn->SetState(FALSE);
			pomHR2LBtn->SetState(FALSE);
			break;
	  }
}
//----------------------------------------------------------------------------------------------------------------------

/*
//-----------------------------------------------------
//        Hilfsfunktionen zum suchen in TSR 
//-----------------------------------------------------
CString  CDialog_TemplateEditor::Find_Name_in_TSR(CString&  opSearchString)
{
	     //--- R�ckgabe 
         CString  olRealName;

	     //--- Alle Eintr�ge in TSR durchgehen
		 int ilRowCount  =  ogBCD.GetDataCount("TSR");
		 
		 for(int i = 0; i < ilRowCount; i++)
		 {
		      //--- String aus TSR 
              CString  olTSRFull  =  ogBCD.GetField("TSR", i, "TEXT");
			  CString  olTSRPart  =  olTSRFull.Left(8);

			  //--- Vollnamen extrahieren
			  if(olTSRPart == opSearchString)
			  {
                  CStringArray  olTSRArray;
                  ExtractItemList(olTSRFull, &olTSRArray, ';');

				  //--- Real Name
				  olRealName = olTSRArray[3];
			  }
		 }

		 //--- R�ckgabe
		 return  olRealName;
}


CString  CDialog_TemplateEditor::Find_Urno_in_TSR(CString&  opSearchString)
{
	     //--- R�ckgabe 
         CString  olUrno   ;

	     //--- Alle Eintr�ge in TSR durchgehen
		 int ilRowCount  =  ogBCD.GetDataCount("TSR");
		 
		 for(int i = 0; i < ilRowCount; i++)
		 {
		      //--- String aus TSR 
              CString  olTSRFull  =  ogBCD.GetField("TSR", i, "TEXT");
			  CString  olTSRPart  =  olTSRFull.Left(8);

			  //--- Vollnamen extrahieren
			  if(olTSRPart == opSearchString)
			  {
	  		      olUrno = ogBCD.GetField("TSR", i, "URNO");

			  }
		 }

		 //--- R�ckgabe
		 return  olUrno;
}

*/


void CDialog_TemplateEditor::OnRadioFlugabhaengig() 
{
	// TODO: Add your control notification handler code here
	if (!SaveChanges())
	{
		UpdateData(FALSE);
		return ;
	}
	UpdateData();
	ResizeWindow();
	OnSelchangeTemplateName();
}
//----------------------------------------------------------------------------------------------------------------------

// return: true, if line can be removed from template
//		   false, if line is used in one or more rules and therefore can't be removed
bool CDialog_TemplateEditor::IsRowRemovable(CGridFenster *popGrid, int ipLine)
{
	bool blRet = true;
	if (popGrid->GetRowCount() >= ipLine)
	{
		CString olRefField, olRefTab;
		CString olBaseField, olBaseTab;
		CString olString;

		olRefField = popGrid->GetValueRowCol(ipLine, 6);	// reference field from TSR
		olRefTab = popGrid->GetValueRowCol(ipLine, 5);		// reference table from TSR
		olBaseField = popGrid->GetValueRowCol(ipLine, 2);	// base field from TSR
		olBaseTab = popGrid->GetValueRowCol(ipLine, 4);		// base table from TSR


		// This if/else will have to be changed in case we introduce 
		// multiple references for a single base field some time 
		// in the future
		if (!(olRefField.IsEmpty() || olRefTab.IsEmpty()))
		{
			olString.Format("%s%c%s",olRefTab,1,olRefField);
		}
		else
		{
			olString.Format("%s%c%s", olBaseTab,1,olBaseField); 
		}

		CCSPtrArray <RecordSet> olRueArr;
		ogBCD.GetRecords("RUE", "UTPL", omCurrentUrno, &olRueArr);

		for (int i = 0; i < olRueArr.GetSize(); i++)
		{
			if (atoi(olRueArr[i].Values[igRueRustIdx]) < 2)
			{
				CString olTest = olRueArr[i].Values[igRueEvrmIdx];
				//  Can lead to Assertion inside TRACE although everything is ok
				//TRACE("[TemplateEditor::IsRowRemovable] Evrm: %s\n", olTest);
				if (olRueArr[i].Values[igRueEvrmIdx].Find(olString) != -1)
				{
					blRet = false;
					break;
				}
			}
		}

		olRueArr.DeleteAll();
	}

	return blRet;
}
//----------------------------------------------------------------------------------------------------------------------


// Shows a message that the line spezified by ipLine can't be removed 
// from the grid specified by popGrid, coz there's rules using this field
void CDialog_TemplateEditor::ShowCannotRemoveFieldMsg(CGridFenster *popGrid, int ipLine)
{
	CString olMsg, olRealName;
	
	olMsg = GetString(302);
	olRealName = popGrid->GetValueRowCol(ipLine, 3);
	olMsg.Replace("%s", olRealName);

	Beep(800, 200);
	AfxMessageBox(olMsg,MB_OK | MB_ICONINFORMATION);
}
//----------------------------------------------------------------------------------------------------------------------


bool CDialog_TemplateEditor::CheckAndChangePrioStrings(CString opFldt, CString opFlda, CString opFldd, int ipEvor)
{
	bool blRet = true;
	
	CCS_TRY
	
		
	CCSPtrArray <RecordSet> olRueArr;
	ogBCD.GetRecords("RUE", "UTPL", omCurrentUrno, &olRueArr);
	int ilRueArrSize = olRueArr.GetSize();

	if (ilRueArrSize > 0)
	{
		int ilRotCount = pomRightRotGrid->GetRowCount();
		int ilArrCount = pomRightArrGrid->GetRowCount();
		int ilDepCount = pomRightDepGrid->GetRowCount();

		CString olNewPrioString;
		DWORD   llStart,llEnd,llDuration;

		llStart = GetTickCount();
		for (int i = 0; i < ilRueArrSize; i++)
		{
			//  don't calculate priority for archived, collective and time-controlled 
			//	flight-independent rules
			if ( (olRueArr[i].Values[igRueRustIdx] != "2") &&
				 (olRueArr[i].Values[igRueRutyIdx] != "1") &&
				 (olRueArr[i].Values[igRueRutyIdx] != "2") )
			{
				TRACE("[TplEditor::CheckAndChangePrioStrings] Regel: %s Urno: %s, Prio: %s\n", 
				 olRueArr[i].Values[igRueRusnIdx], olRueArr[i].Values[igRueUrnoIdx], olRueArr[i].Values[igRuePrioIdx]);
			// calculate new prio string
				//olNewPrioString = GetPartialPrioString(_ROT_, ilRotCount, omFldt, opFldt, olRueArr[i].Values[igRuePrioIdx]);
				olNewPrioString = CalcPartialPrioString(ilRotCount, opFldt, olRueArr[i].Values[igRueEvttIdx]);


				switch (ipEvor)
				{
					case 1:	// vertically from left to right (arrival first, then departure)
						//olNewPrioString += GetPartialPrioString(_ARR_, ilArrCount, omFlda, opFlda, olRueArr[i].Values[igRuePrioIdx]);
						olNewPrioString += CalcPartialPrioString(ilArrCount, opFlda, olRueArr[i].Values[igRueEvtaIdx]);
						//olNewPrioString += GetPartialPrioString(_DEP_, ilDepCount, omFldd, opFldd, olRueArr[i].Values[igRuePrioIdx]);
						olNewPrioString += CalcPartialPrioString(ilDepCount, opFldd, olRueArr[i].Values[igRueEvtdIdx]);
						break;

					case 2:	// horizontally from left to right
						break;

					case 3: // vertically from left to right (arrival first, then departure)
						//olNewPrioString += GetPartialPrioString(_DEP_, ilDepCount, omFldd, opFldd, olRueArr[i].Values[igRuePrioIdx]);
						olNewPrioString += CalcPartialPrioString(ilDepCount, opFldd, olRueArr[i].Values[igRueEvtdIdx]);
						//olNewPrioString += GetPartialPrioString(_ARR_, ilArrCount, omFlda, opFlda, olRueArr[i].Values[igRuePrioIdx]);
						olNewPrioString += CalcPartialPrioString(ilArrCount, opFlda, olRueArr[i].Values[igRueEvtaIdx]);
						
					case 4: // horizontically from left to right
						break;
				}
				// write to database
				if( olRueArr[i].Values[igRuePrioIdx] != olNewPrioString )
				{
					olRueArr[i].Values[igRuePrioIdx] = olNewPrioString;
					blRet &= ogBCD.SetRecord("RUE", "URNO", olRueArr[i].Values[igRueUrnoIdx], olRueArr[i].Values, FALSE);
				}
				olNewPrioString.Empty();
			}
		}
		llEnd = GetTickCount();
		llDuration = llEnd - llStart;

		if (blRet)
			ogBCD.Save("RUE");
	}

	olRueArr.DeleteAll();

	CCS_CATCH_ALL

	return blRet;	
}
//----------------------------------------------------------------------------------------------------------------------



// IN:
//    ipTyp: = _ROT_, if partial string for rotation grid
//			 = _ARR_, if partial string for arrival grid
//			 = _DEP_, if partial string for departure grid
//
//	  ipCount: length required for this part of the new prio string
//    opOldStr: old (i.e. before change) FLDT, FLDA or FLDD (depending on ipTyp)
//    opNewStr: new FLDT, FLDA or FLDD (depending on ipTyp)
//    opOldPrioString: old prio string from RUE that is to be replaced by the new one
//    
CString CDialog_TemplateEditor::GetPartialPrioString(int ipTyp, int ipCount, CString opOldStr, CString opNewStr,
													 CString opOldPrioString)
{
	CString olTmpNewPrio;

	CCS_TRY

	
	for (int i = 0; i < ipCount; i++)
	{
		olTmpNewPrio += "0";
	}
	
	CString olTmpOldPrio;
	CStringArray olOldArr, olNewArr;
	ExtractItemList(opOldStr, &olOldArr, ';');
	ExtractItemList(opNewStr, &olNewArr, ';');
		
	switch (ipTyp)
	{
		case _ROT_:
			olTmpOldPrio = opOldPrioString.Left(olOldArr.GetSize());
			imIdx = olTmpOldPrio.GetLength();
			//TRACE("[Tpl-Editor::GetPartialPrioStr] ROT: imIdx=%d, OldPrio:%s\n", imIdx, olTmpOldPrio);
			break;

		case _ARR_:
			olTmpOldPrio = opOldPrioString.Mid(/*olTmpOldPrio.GetLength()*/imIdx, olOldArr.GetSize());
			imIdx += olTmpOldPrio.GetLength();
			//TRACE("[Tpl-Editor::GetPartialPrioStr] ARR: imIdx=%d, OldPrio:%s\n", imIdx, olTmpOldPrio);
			break;

		case _DEP_:
			olTmpOldPrio = opOldPrioString.Mid(/*olTmpOldPrio.GetLength()*/imIdx, olOldArr.GetSize());
			imIdx += olTmpOldPrio.GetLength();
			//TRACE("[Tpl-Editor::GetPartialPrioStr] DEP: imIdx=%d, OldPrio:%s\n", imIdx, olTmpOldPrio);
			break;
	}

	if (opOldStr != opNewStr)
	{
		CString olOld, olNew;
		int		ilPosOld = -1, ilPosNew = -1; //, ilLenOld, ilLenNew;
		/*	TRACE("[Tpl-Editor::GetPartialPrioStr] olOldArr: %d, olTmpOldPrio: %d == olNewArr: %d, olTmpNewPrio\n", 
					olOldArr.GetSize(), olTmpOldPrio.GetLength(), olNewArr.GetSize(), olTmpNewPrio.GetLength());*/
		for (i = 0; i < olOldArr.GetSize(); i++)
		{
			for (int k = 0; k < olNewArr.GetSize(); k++)
			{
				//  
				olOld = olOldArr[i];
				olNew = olNewArr[k];
				ilPosOld = olOld.Find ( '.', 0 );
				ilPosNew = olNew.Find ( '.', 0 );
				//  Form : "1.AFT.ACT3" 
				//  We only need "AFT.ACT3"
				if ( ilPosOld >= 0 ) 
					olOld = olOld.Right ( olOld.GetLength() - ilPosOld - 1 );
				if ( ilPosNew >= 0 )
					olNew = olNew.Right ( olNew.GetLength() - ilPosNew - 1 );
				if (olOld == olNew)
				{
					olTmpNewPrio.SetAt(k, olTmpOldPrio[i]);
					break;
				}
			}
		}
	}
	else
	{
		olTmpNewPrio = olTmpOldPrio;
	}

	CCS_CATCH_ALL

	return olTmpNewPrio;
}
//----------------------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------------------

bool CDialog_TemplateEditor::SaveChanges()
{
	bool blRet = true;

	if (bmChangeFlag||bmRenamed)
	{
		int ilSel = AfxMessageBox(GetString(161), MB_YESNO);
		if(ilSel == IDYES)
		{
		   //bmIsCopyPressed = true;
			blRet = DoSave();
		}
		else
		{
			bmChangeFlag = false;	
			bmRenamed = false;
		}
	}
	return blRet;
} 
//----------------------------------------------------------------------------------------------------------------------

void CDialog_TemplateEditor::ResetTable( CGridFenster *popTable ) 
{

	//--  delete existing rows
	int ilCount = popTable ->GetRowCount();
		
	if(ilCount != 0)
	{
		popTable ->SetReadOnly(FALSE);
	    popTable->RemoveRows(1, ilCount);
		popTable ->SetReadOnly(TRUE);
	}
}
//----------------------------------------------------------------------------------------------------------------------

void CDialog_TemplateEditor::ResetAllTables ()
{
	ResetTable(pomLeftRotGrid);
	ResetTable(pomLeftArrGrid);
	ResetTable(pomLeftDepGrid);
	ResetTable(pomRightRotGrid);
	ResetTable(pomRightArrGrid);
	ResetTable(pomRightDepGrid);
}
//----------------------------------------------------------------------------------------------------------------------

void CDialog_TemplateEditor::ResizeWindow()
{
	RECT rlWindowRect, rlCtrlRect;
	
	GetWindowRect ( &rlWindowRect );
	if ( imFlugUnabhaengig )
	{
		CWnd*polCtrl = GetDlgItem( IDC_BTN_FRAME );
		if ( !polCtrl )
			return;
		polCtrl->GetWindowRect ( &rlCtrlRect );
		rlWindowRect.bottom = rlCtrlRect.top - 1;
	}
	else
		rlWindowRect.bottom = rlWindowRect.top + imDefaultHeight;
	MoveWindow ( &rlWindowRect );
}
//----------------------------------------------------------------------------------------------------------------------

void CDialog_TemplateEditor::IniStatics ()
{
	SetWindowText ( GetString(1488) );

	SetDlgItemText ( IDC_SAVE, GetString(IDS_STR130_SAVE) );
	SetDlgItemText ( IDC_NEW, GetString(1372) );
	SetDlgItemText ( IDC_DELETE, GetString(IDS_STR131_DELETE) );

	SetDlgItemLangText ( this, IDC_COPY_BTN, IDS_KOPIEREN );
	SetDlgItemLangText ( this, TEMPL_CHB_DEACTI, IDS_DEAKTIVIERT );
	SetDlgItemText (IDC_SCHLIESSEN, GetString(1375));

	SetDlgItemLangText ( this, IDS_TEMPLATENAME, IDS_TPL_NAME );
	SetDlgItemLangText ( this, TEMPL_STA_ALOD, IDS_ZUORDNUNGSEINHEIT );

	SetDlgItemText ( IDC_RADIO_FLUGABHAENGIG, GetString(1534));
	SetDlgItemText ( IDC_RADIO_FLUGUNHAENGIG, GetString(1363));

	SetDlgItemLangText ( this, IDS_ROT_AW, IDS_ROT_AW_TXT );
	SetDlgItemLangText ( this, IDS_ROT_TMPL, IDS_ROT_TMPL_TXT );
	SetDlgItemLangText ( this, IDC_BTN_FRAME, IDS_AUSWERTE_FOLGE );
	
	SetDlgItemLangText ( this, IDS_ARR_AW, IDS_ARR_AW_TXT);
	SetDlgItemLangText ( this, IDS_ARR_TMPL, IDS_ARR_TMPL_TXT);
	 
	SetDlgItemLangText ( this, IDS_DEP_AW, IDS_DEP_AW_TXT );
	SetDlgItemLangText ( this, IDS_DEP_TMPL, IDS_DEP_TMPL_TXT );

	SetDlgItemLangText ( this, IDC_GRP_FTYPES_IN, IDS_FTYPES_IN );
	SetDlgItemLangText ( this, IDC_GRP_FTYPES_OUT, IDS_FTYPES_OUT );
}
//----------------------------------------------------------------------------------------------------------------------

//  Liefert die Urno f�r das in der Combobox ropCb ausgew�hlte Template
//	Wenn nicht f�r bpIndep aber f�r !bpIndep vorhanden, wird ein leeres
//  Template neu angelegt
//	return: false: wenn kein Templatename in Combobox augew�hlt, oder wenn 
//				   gew�hlter Name weder f�r flugunabh�ngige noch f�r flugab-
//				   h�ngige Regeln existiert
//			true: sonst
//  IN; ropCb:		Referenz auf Combobox mit Tpl-Namen
//		bpIndep:	Tpl. suchen f�r flugunabh�ngige Regeln, wenn true
//		bpMeldError:MessageBox anzeigen, wenn nicht erfolgreich
//  OUT:ropUrno:	Referenz auf String, der mit gesuchter Urno gef�llt wird
bool GetSelectedTplUrno(CComboBox &ropCb, CString &ropUrno, BOOL bpIndep, bool bpMeldError/*=false*/)
{
	CCSPtrArray <RecordSet>	olTplRecs;
	RecordSet olRecord;
	// CComboBox* polCb = ropCb.GetComboBoxCtrl();

	CString	olTnam, olAppl, olCorrespUrno, olStrInactive;
	int	ilFoundIdx = -1;

	int ilSel = ropCb.GetCurSel();

	if (ilSel != CB_ERR && ropCb.GetCount() > 0)
	{
		olStrInactive = CString(" ") + GetString(303);  /* Name (deactivated) */

		ropCb.GetLBText(ilSel, olTnam);

		/* if template is marked as inactive, delete this string */
		olTnam.Replace ( olStrInactive, "" );	 
		
		ogBCD.GetRecords("TPL", "TNAM", olTnam, &olTplRecs);
		for (int i = 0; (ilFoundIdx < 0) && (i < olTplRecs.GetSize()); i++)
		{
			if (igTplApplIdx < 0)
			{
				ilFoundIdx = i;
			}
			else 
			{
				olAppl = olTplRecs[i].Values[igTplApplIdx];
				if (bpIndep == TRUE && (olAppl == "RULE_FIR"))
				{
					ilFoundIdx = i;
				}
				if (bpIndep == FALSE && (olAppl != "RULE_FIR"))
				{
					ilFoundIdx = i;
				}
			}
		}
	}
	if (ilFoundIdx >= 0)
	{
		ropUrno = olTplRecs[ilFoundIdx].Values[igTplUrnoIdx];
	}
	else
	{
		ropUrno.Empty();
		//  vielleicht gibt es wenigstens das entsprechende Template f�r
		//	!bpIndep -> dann legen wir das ben�tigte noch schnell als 
		//				leeres  Template an
		if (olTplRecs.GetSize() > 0) 
		{
			olCorrespUrno = olTplRecs[0].Values[igTplUrnoIdx];
			ropUrno = SaveCorrespTpl(olCorrespUrno);
			
			if (ropUrno.IsEmpty () == FALSE)
			{
				ilFoundIdx = olTplRecs.GetSize();
			}
		}
		if ((ilFoundIdx<0) && bpMeldError)
		{
			CString olMsg = GetString(IDS_STRING1533);
			CString olRuleTyp = GetString(bpIndep ? IDS_STRING1363 : IDS_STRING1534);
			olMsg.Replace("%s", olRuleTyp);
			MessageBox(0, olMsg, GetString(1412), MB_OK | MB_ICONEXCLAMATION);
		}
	}
	olTplRecs.DeleteAll();
	return (ilFoundIdx >= 0);
}
//----------------------------------------------------------------------------------------------------------------------

//  Erzeugt leeres Template zu ropCorrespUrno, d.h. 
//  APPL = 'RULE_AFT', wenn APPL='RULE_FIR' f�r ropCorrespUrno (und umgekehrt)
//  FLDA, FLDD, FLDT leer, FISU='0', TPST='1', EVOR='1'
//  TNAM, RELX, DALO gleich
//  In:	ropCorrespUrno, Urno des dazugeh�rigen Templates
//  Return:				Urno des erzeugten Templates
CString SaveCorrespTpl ( CString &ropCorrespUrno )
{
	CString		olNewUrno, olWert;
	int			ilFields = ogBCD.GetFieldCount("TPL");
	RecordSet	olCorresp(ilFields), olNewRec(ilFields);
	if ( ogBCD.GetRecord ( "TPL", "URNO", ropCorrespUrno, olCorresp ) )
	{
		olWert = olCorresp.Values[igTplApplIdx];
		if ( olWert=="RULE_FIR") 
			olNewRec.Values[igTplApplIdx] = "RULE_AFT";
		else
			olNewRec.Values[igTplApplIdx] = "RULE_FIR";
		olNewRec.Values[igTplDaloIdx] = olCorresp.Values[igTplDaloIdx];
		olNewRec.Values[igTplRelxIdx] = olCorresp.Values[igTplRelxIdx];
		olNewRec.Values[igTplTnamIdx] = olCorresp.Values[igTplTnamIdx];

		olNewRec.Values[igTplEvorIdx] = 
		olNewRec.Values[igTplTpstIdx] = "1";

		olNewRec.Values[igTplFisuIdx] = "0";

		olNewRec.Values[igTplFldaIdx] = 
		olNewRec.Values[igTplFlddIdx] = 
		olNewRec.Values[igTplFldtIdx] = "";
		olWert.Format ( "%ld", ogBCD.GetNextUrno() );
		olNewRec.Values[igTplUrnoIdx] = olWert;

		if ( ogBCD.InsertRecord("TPL", olNewRec, true ) )
			olNewUrno = olWert ;		
	}
	return olNewUrno;
}
//----------------------------------------------------------------------------------------------------------------------

//  Liefert Urno des entsprechenden Template zu ropUrno, d.h. 
//  mit APPL = 'RULE_AFT', wenn APPL='RULE_FIR' f�r ropUrno (und umgekehrt)
//  In:		ropUrno: Urno eines Templates
//  Return:	 Urno des dazugeh�rigen Templates
CString GetCorrespTplUrno ( CString &ropUrno )
{
	CString olAppl, olCorresUrno, olTplName;
	olAppl = ogBCD.GetField ( "TPL", "URNO", ropUrno, "APPL" );

	olTplName = ogBCD.GetField ( "TPL", "URNO", ropUrno, "TNAM" );
	if ( olTplName.IsEmpty() )
		return olCorresUrno;
	if ( olAppl=="RULE_FIR" )
	{
		olCorresUrno = ogBCD.GetFieldExt ( "TPL", "TNAM", "APPL", 
										   olTplName, "RULE_AFT", "URNO" );
		if ( olCorresUrno.IsEmpty() )
			olCorresUrno = ogBCD.GetFieldExt ( "TPL", "TNAM", "APPL", 
											   olTplName, "", "URNO" );
	}
	else
		olCorresUrno = ogBCD.GetFieldExt ( "TPL", "TNAM", "APPL", 
										   olTplName, "RULE_FIR", "URNO" );
	return olCorresUrno;
}
//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------



void CDialog_TemplateEditor::OnSelchangeFtypes() 
{
	// TODO: Add your control notification handler code here
	bmChangeFlag=true;
	m_SaveBtn.EnableWindow(TRUE);
}


// IN:
//	  ipCount: length required for this part of the new prio string
//    opTplFldx: template's  FLDT, FLDA or FLDD 
//    opRueEvtx: rule's EVTT, EVTA or EVTD (corresponding to opTplFldx)
// RETURN:
//	  Part of Priostring for rotation, inbound or outbound depending on input

CString CDialog_TemplateEditor::CalcPartialPrioString(int ipCount, CString opTplFldx, CString opRueEvtx)
{
	CString olTmpNewPrio, olEvtx, olFldx, olValueType, olFieldName;
	CStringArray olFldxArr, olEvtxArr;
	int i,j, ilPos1, ilPos2, ilTplPos;
	
	ExtractItemList(opTplFldx, &olFldxArr, ';');
	ExtractItemList(opRueEvtx, &olEvtxArr, ';');

	for (i = 0; i < ipCount; i++)
	{
		olTmpNewPrio += "0";
	}
	
	for (i = 0; i < olEvtxArr.GetSize(); i++)
	{
		//  Get i. Condition String from rule
		olEvtx = olEvtxArr[i];
		if (olEvtx.IsEmpty() || olEvtx == CString(" ") )
			continue;
		ilPos1 = olEvtx.Find("=");
		if(ilPos1 > 0)
		{
			olValueType = olEvtx.Left(1);
			olFieldName = olEvtx.Mid(2,8);
	   
			if ( olFieldName.GetLength() != 8 )
				continue;
			for ( j=0; j< olFldxArr.GetSize(); j++)
			{
				olFldx = olFldxArr[j];
				//  Form olFldx: "12.AFT.ACT3" ,  
				if ( olFldx.Find ( olFieldName ) >= 0)
				{	/* found corresponding condition in template definition */
					ilPos2 = olFldx.Find ( '.', 0 );
					if ( ilPos2 > 0 ) 
					{
						/* get position for priostring */
						ilTplPos = atoi ( olFldx.Left(ilPos2) ) -1;
						if ( (ilTplPos>=0) && (ilTplPos<ipCount) )
						{
							if(atoi(olValueType) == 0)	// simple value
							{
								olTmpNewPrio.SetAt ( ilTplPos, '3');
							}
							if(atoi(olValueType) == 2)	// static group
							{
								olTmpNewPrio.SetAt ( ilTplPos, '2');
							}
							if(atoi(olValueType) == 1)	// dynamic group
							{
								olTmpNewPrio.SetAt ( ilTplPos, '1');
							}
							break;
						}	
				   }
			   }	/* endof if olFldx.Find */
		   }/* endof for - loop on olFldxArr */
		}
	}/* endof for - loop on olEvtxArr */

	return olTmpNewPrio;
}


