#if !defined(AFX_TABLEWITHGRID_H__5D869631_9B36_11D3_939F_00001C033B5D__INCLUDED_)
#define AFX_TABLEWITHGRID_H__5D869631_9B36_11D3_939F_00001C033B5D__INCLUDED_

#include <DialogGrid.h>
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TableWithGrid.h : header file
//
class CTableGrid;
template <class>class CCSPtrArray;

/////////////////////////////////////////////////////////////////////////////
// CTableWithGrid window

class CTableWithGrid : public CWnd
{
// Construction
public:
	CTableWithGrid();

friend CTableGrid;
// Attributes
public:
	WORD imSelectMode;
	CTableGrid *pomGrid;

protected:
    BOOL bmInited;
    CWnd *pomParentWindow;
    int imXStart, imXEnd;
    int imYStart, imYEnd;
    COLORREF lmTextColor;
    COLORREF lmTextBkColor;
    COLORREF lmHighlightColor;
    COLORREF lmHighlightBkColor;
	bool bmCalculateNormal;
	CStringArray omHeaderFields;    // array of column headers

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTableWithGrid)
	//}}AFX_VIRTUAL
     bool SetTableData(
        CWnd *popParentWindow,
        int ipXStart, int ipXEnd, int ipYStart, int ipYEnd,
        COLORREF lpTextColor = ::GetSysColor(COLOR_WINDOWTEXT),
        COLORREF lpTextBkColor = ::GetSysColor(COLOR_WINDOW),
        COLORREF lpHighlightColor = ::GetSysColor(COLOR_HIGHLIGHTTEXT),
        COLORREF lpHighlightBkColor = ::GetSysColor(COLOR_HIGHLIGHT),
        CFont *popTextFont = NULL,
        CFont *popHeaderFont = NULL,
		bool bpCalculateNormal = false
    );
    bool DisplayTable();
    bool SetPosition(int ipXStart, int ipXEnd, int ipYStart, int ipYEnd);
	bool SetHeaderFields(CString opHeaderFields);
	bool ResetContent();
	bool AddTextLine(CString opText, void *pvpData = NULL);
	bool GetTextLineValue(int ipLineNo, CString &opText);
	void SetSelectMode(WORD ipSelectMode);
	bool SetFormatList(CString opFormatList);
	UINT GetSelCount ( void *popSource );
	UINT GetSelDataPtrs( void*popSource, void**polSelDataPtr, UINT ipMaxItems );

// Implementation
public:
	virtual ~CTableWithGrid();

	// Generated message map functions
protected:
	//{{AFX_MSG(CTableWithGrid)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CTableGrid window

class CTableGrid : public CGridFenster
{
// Construction
public:
	CTableGrid(CWnd *pParent);

// Attributes
public:
	CTableWithGrid	*pomTableWindow;
	ROWCOL			imMinRowCount, imLastLineWithValues;
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTableGrid)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTableGrid();

	BOOL SetHeaderFields ( CStringArray &ropHeaderFields );
	BOOL AddTextLine( CString opText, void *pvpData );
	BOOL SetColWidths ( CCSPtrArray<double> &ropScaleFactors );
	void SetMinRowCount ( ROWCOL ipMinRowCount );
	BOOL ResetContent();
	void SortRows(CGXRange sortRange, CGXSortInfoArray& sortInfo, UINT flags = GX_UPDATENOW);
	BOOL DndStartDragDrop(ROWCOL nRow, ROWCOL nCol);
protected:
	void OnChangedSelection(const CGXRange* pRange, BOOL bIsDragging, BOOL bKey);
	// Generated message map functions
protected:
	//{{AFX_MSG(CTableGrid)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TABLEWITHGRID_H__5D869631_9B36_11D3_939F_00001C033B5D__INCLUDED_)
