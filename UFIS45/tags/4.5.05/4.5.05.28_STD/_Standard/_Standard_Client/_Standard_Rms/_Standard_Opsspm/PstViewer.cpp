// gaviewer.cpp : implementation file
//  
//
// Modification History:
// 19-Sep-96	Damkerng	Change many places since our database is changed.
//							Change the GetFmJobsByPrid() to be GetFmgJobsByAlid().
//							Since we now have a new job type FMJ for flight manager,
//							we don't use the field PRID to check if this pool job
//							is of a flight manager or a normal staff to create
//							managers information for each position area anymore. Instead,
//							we have a new separate job type FMG to do this function.
//							Check these places out by searching for "Id 19-Sep-96"

#include <stdafx.h>
#include <CCSGlobl.h>
#include <OpssPm.h>
#include <CCSCedaData.h>
#include <CCSCedaCom.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaAltData.h>
#include <gantt.h>
#include <gbar.h>
#include <CedaFlightData.h>
#include <CCSPtrArray.h>
#include <CedaFlightData.h>
#include <AllocData.h>
#include <cviewer.h>
#include <ccsddx.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <conflict.h>
#include <PstViewer.h>
#include <conflict.h>
#include <ConflictConfigTable.h>
#include <BasicData.h>
#include <DataSet.h>
#include <CedaPaxData.h>
#include <FieldConfigDlg.h>
#include <CedaActData.h>
#include <CedaRudData.h>
#include <CedaReqData.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsWithIn(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))

static int CompareJobStartTime(const JOBDATA **e1, const JOBDATA **e2)
{
	if ((strcmp((**e1).Jtco,JOBPOOL) != 0) && (strcmp((**e2).Jtco,JOBPOOL) == 0) )
	{
		return -1;
	}
	else
	{
		if ((strcmp((**e1).Jtco,JOBPOOL) == 0) && (strcmp((**e2).Jtco,JOBPOOL) != 0) )
		{
			return 1;
		}
		else
		{
			//if ((**e1).Acfr.GetTime() == (**e2).Acfr.GetTime() == 0)
			
			if ((**e2).Acfr.GetTime() < (**e1).Acfr.GetTime())
				return -1;
			if ((**e2).Acfr.GetTime() > (**e1).Acfr.GetTime())
				return 1;
				return 0;

	//		return (int)((**e1).Acfr.GetTime() - (**e2).Acfr.GetTime());
		}
	}
	
}


static int ByStartTime(const PST_BARDATA **pppBar1, const PST_BARDATA **pppBar2);
static int ByStartTime(const PST_BARDATA **pppBar1, const PST_BARDATA **pppBar2)
{
	return (int)((**pppBar1).StartTime.GetTime() - (**pppBar2).StartTime.GetTime());
}

/////////////////////////////////////////////////////////////////////////////
// PstDiagramViewer
//
PstDiagramViewer::PstDiagramViewer()
{
	pomAttachWnd = NULL;
	omStartTime = TIMENULL;
	omEndTime = TIMENULL;

	bmUseAllPstAreas = false;
	bmUseAllAlcds = false;
	bmUseAllAgents = false;
	bmUseAllKeycodes = false;
	bmUseAllNatures = false;
	bmUseAllActs = false;
	bmNoAct = false;
	bmDisplayJobConflicts = false;
	bmOneFlightPerLine = false;

	ogCCSDdx.Register(this, FLIGHT_DELETE,CString("PSTVIEWER"), CString("Flight Delete"), PstDiagramCf);
	ogCCSDdx.Register(this, FLIGHT_CHANGE,CString("PSTVIEWER"), CString("Flight Change"), PstDiagramCf);
	ogCCSDdx.Register(this, JOB_NEW,CString("PSTVIEWER"), CString("Job New"), PstDiagramCf);
	ogCCSDdx.Register(this, JOB_CHANGE,CString("PSTVIEWER"), CString("Job Changed"), PstDiagramCf);
	ogCCSDdx.Register(this, JOB_DELETE,CString("PSTVIEWER"), CString("Job Delete"), PstDiagramCf);
	ogCCSDdx.Register(this, DEMAND_NEW, CString("PSTVIEWER"), CString("Demand New"), PstDiagramCf);
	ogCCSDdx.Register(this, DEMAND_CHANGE, CString("PSTVIEWER"), CString("Demand Changed"), PstDiagramCf);
	ogCCSDdx.Register(this, DEMAND_DELETE, CString("PSTVIEWER"), CString("Demand Delete"), PstDiagramCf);
	ogCCSDdx.Register(this, PRM_DEMAND_CHANGE, CString("PSTVIEWER"), CString("Prm Demand Changed"), PstDiagramCf);
	ogCCSDdx.Register(this, REDISPLAY_ALL,CString("PSTVIEWER"), CString("Global Update"), PstDiagramCf);
	ogCCSDdx.Register(this, FLIGHT_SELECT_PSTGANTT,CString("PSTVIEWER"),CString("Flight Select"), PstDiagramCf);
	ogCCSDdx.Register(this, UPDATE_CONFLICT_SETUP,CString("PSTVIEWER"), CString("Update Conflict Setup"), PstDiagramCf);
	bmEmptyAirlineSelected = false;
	SetAdditionalPosForWithoutPosGrpMap();

	LoadDelegatedFlightUrnos();
}

void PstDiagramViewer::LoadDelegatedFlightUrnos()
{
	if(ogBasicData.bmDelegateFlights)
	{
		int n = ogJobData.omData.GetSize();
		for(int i = 0; i < n; i++)
		{
			AddDelegatedFlightUrno(&ogJobData.omData[i]);
		}
	}
}

void PstDiagramViewer::AddDelegatedFlightUrno(JOBDATA *prpJob)
{
	if(prpJob != NULL)
	{
		if(CString(prpJob->Jtco) == JOBDELEGATEDFLIGHT)
		{
			omDelegatedFlights.SetAt((void *) prpJob->Flur, NULL);
			if(ogBasicData.IsSinApron1() == true || ogBasicData.IsSinApron2() == true)
			{
				DEMANDDATA* polDemandData = ogDemandData.GetDemandByUrno(prpJob->Udem);
				FLIGHTDATA* polFlightData = ogFlightData.GetRotationFlight(prpJob->Flur);
				if(polDemandData != NULL && ogDemandData.IsTurnaroundDemand(polDemandData) && polFlightData != NULL)
				{
					omDelegatedFlights.SetAt((void *)polFlightData->Urno,NULL);
				}
			}
		}
	}
}

void PstDiagramViewer::RemoveDelegatedFlightUrno(JOBDATA *prpJob)
{
	if(prpJob != NULL)
	{
		bool blDFFound = false;
		CCSPtrArray <JOBDATA> olJobs;
		ogJobData.GetJobsByFlur(olJobs,prpJob->Flur,FALSE,"",ALLOCUNITTYPE_PST); // don't like JOBFM
		for (int i = 0; i < olJobs.GetSize(); i++)
		{
			JOBDATA *prlJob = &olJobs[i];
			if(CString(prlJob->Jtco) == JOBDELEGATEDFLIGHT)
			{
				blDFFound = true;
				break;
			}
		}
		if(!blDFFound)
		{
			omDelegatedFlights.RemoveKey((void *)prpJob->Flur);
			if(ogBasicData.IsSinApron1() == true || ogBasicData.IsSinApron2() == true)
			{
				DEMANDDATA* polDemandData = ogDemandData.GetDemandByUrno(prpJob->Udem);
				FLIGHTDATA* polFlightData = ogFlightData.GetRotationFlight(prpJob->Flur);
				if(polDemandData != NULL && ogDemandData.IsTurnaroundDemand(polDemandData) && polFlightData != NULL)
				{
					omDelegatedFlights.RemoveKey((void *)polFlightData->Urno);
				}
			}
		}
	}
}

void PstDiagramViewer::SetAdditionalPosForWithoutPosGrpMap()
{
	// create map containing additional positions which are included in the without positions group
	// these positions are defined in an extra static group

	char pclGroupName[512];
    GetPrivateProfileString(pcgAppName, "ADDITIONAL_WITHOUT_POSITIONS", "", pclGroupName, sizeof pclGroupName, ogBasicData.GetConfigFileName());
	if(strlen(pclGroupName) > 0)
	{
		CCSPtrArray <ALLOCUNIT> olPsts;
		ogAllocData.GetUnitsByGroup(ALLOCUNITTYPE_PSTGROUP,pclGroupName,olPsts);
		int ilNumPsts = olPsts.GetSize();
		for(int ilPst = 0; ilPst < ilNumPsts; ilPst++)
		{
			ALLOCUNIT *prlPst = &olPsts[ilPst];
			omAdditionalPosForWithoutPosGrpMap.SetAt(prlPst->Name, NULL);
		}
	}
}

PstDiagramViewer::~PstDiagramViewer()
{
	ogCCSDdx.UnRegister(this, NOTUSED);
	DeleteAll();
}

void PstDiagramViewer::Attach(CWnd *popAttachWnd)
{
	pomAttachWnd = popAttachWnd;
}


void PstDiagramViewer::PstDiagramCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	PstDiagramViewer *polViewer = (PstDiagramViewer *)popInstance;

	if (polViewer->bmNoUpdatesNow == FALSE)
	{
		if (ipDDXType == FLIGHT_CHANGE)
		{
			polViewer->ProcessFlightChange((FLIGHTDATA *)vpDataPointer);
		}
		else if (ipDDXType == FLIGHT_DELETE)
		{
			polViewer->ProcessFlightDelete((long)vpDataPointer);
		}
		else if (ipDDXType == DEMAND_CHANGE || ipDDXType == DEMAND_DELETE || ipDDXType == DEMAND_NEW)
		{
			polViewer->ProcessDemandChange((DEMANDDATA *)vpDataPointer);
		}
		else if (ipDDXType == PRM_DEMAND_CHANGE)
		{
			polViewer->ProcessDemandChange((DEMANDDATA *)vpDataPointer);
		}
		else if (ipDDXType == JOB_NEW)
		{
			polViewer->AddDelegatedFlightUrno((JOBDATA *)vpDataPointer);
			polViewer->ProcessFmgJobNew((JOBDATA *)vpDataPointer);
			polViewer->ProcessSpecialJobNew((JOBDATA *) vpDataPointer);
			polViewer->ProcessJobChange((JOBDATA *) vpDataPointer);
		}
		else if (ipDDXType == JOB_CHANGE)
		{
			// we have no ProcessFmgJobChange() for this moment, so I just use delete and add.
			polViewer->AddDelegatedFlightUrno((JOBDATA *)vpDataPointer);
			polViewer->ProcessFmgJobDelete((JOBDATA *)vpDataPointer);
			polViewer->ProcessFmgJobNew((JOBDATA *)vpDataPointer);
			polViewer->ProcessSpecialJobChange((JOBDATA *) vpDataPointer);
			polViewer->ProcessJobChange((JOBDATA *) vpDataPointer);
		}
		else if (ipDDXType == JOB_DELETE)
		{
			polViewer->RemoveDelegatedFlightUrno((JOBDATA *)vpDataPointer);
			polViewer->ProcessFmgJobDelete((JOBDATA *)vpDataPointer);
			polViewer->ProcessSpecialJobDelete((JOBDATA *) vpDataPointer);
			polViewer->ProcessJobChange((JOBDATA *) vpDataPointer);
		}
		else if (ipDDXType == REDISPLAY_ALL || ipDDXType == UPDATE_CONFLICT_SETUP)
		{
			polViewer->DeleteAll();
			polViewer->ChangeViewTo();
			polViewer->pomAttachWnd->Invalidate();
		}
		else if (ipDDXType == FLIGHT_SELECT_PSTGANTT)
		{
			polViewer->ProcessFlightSelect((FLIGHTDATA *)vpDataPointer);
		}
	}
	else
	{
		// should never reach this point
	//	ASSERT( 0 );
	}
}

void PstDiagramViewer::ProcessDemandChange(DEMANDDATA *prpDemand)
{
	if(prpDemand != NULL)
	{
		long llFlightUrno = ogDemandData.GetFlightUrno(prpDemand);

		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(llFlightUrno);
		if(prlFlight != NULL)
		{
			ProcessFlightChange(prlFlight);
		}
	}
}

void PstDiagramViewer::ProcessJobChange(JOBDATA *prpJob)
{
	if(prpJob != NULL)
	{
		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prpJob->Flur);
		if(prlFlight != NULL)
		{
			ProcessFlightChange(prlFlight);
		}
	}
}

void PstDiagramViewer::ProcessFlightDelete(long lpFlightUrno)
{
	DeleteBarsForFlight(lpFlightUrno);
}

void PstDiagramViewer::ProcessFlightChange(FLIGHTDATA *prpFlight, bool bpCheckRotation /*=true*/)
{
	if(prpFlight == NULL)
		return;

	int ilGroupno, ilLineno;
	FLIGHTDATA *prlRotationFlight;
	CString olAlc = ogAltData.FormatAlcString(prpFlight->Alc2,prpFlight->Alc3);;

	DeleteBarsForFlight(prpFlight->Urno);


	if(IsPassFilter(GetString(IDS_NO_POSITION),olAlc,prpFlight->Hara,prpFlight->Stev,prpFlight->Ttyp,prpFlight->Act3,prpFlight->Act5,prpFlight->Urno))
	{
		int ilWithoutPstGroup;
		if(FindGroup(GetString(IDS_NO_POSITION),ilWithoutPstGroup))
		{
			int ilLastLine	  = GetLineCount(ilWithoutPstGroup) - 1;
			int ilUpdatedLine = CreateLinesForFlightsWithoutPsts(prpFlight);
			if (ilUpdatedLine != -1)
			{
				if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
				{
					LONG lParam = MAKELONG(ilUpdatedLine, ilWithoutPstGroup);
					if (ilUpdatedLine > ilLastLine)
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_INSERTLINE, lParam);
					else
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
				}
			}
		}
	}
	if(IsPassFilter(GetString(IDS_ALL_FLIGHTS),olAlc,prpFlight->Hara,prpFlight->Stev,prpFlight->Ttyp,prpFlight->Act3,prpFlight->Act5,prpFlight->Urno))
	{
		int ilAllFlightsGroup;
		if(FindGroup(GetString(IDS_ALL_FLIGHTS),ilAllFlightsGroup))
		{
			int ilLastLine	  = GetLineCount(ilAllFlightsGroup) - 1;
			int ilUpdatedLine = CreateLinesForAllFlights(prpFlight);
			if(ilUpdatedLine != -1)
			{
				if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
				{
					LONG lParam = MAKELONG(ilUpdatedLine, ilAllFlightsGroup);
					if (ilUpdatedLine > ilLastLine)
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_INSERTLINE, lParam);
					else
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
				}
			}
		}
	}

	CUIntArray olReturnFlightTypes;
	int ilNumReturnFlightTypes = GetReturnFlightTypes(prpFlight, olReturnFlightTypes, "");
	for(int ilRetFlight = 0; ilRetFlight < ilNumReturnFlightTypes; ilRetFlight++)
	{
		CString olPst = ogFlightData.GetPosition(prpFlight,olReturnFlightTypes[ilRetFlight]);
		if(!olPst.IsEmpty())
		{
			ilGroupno = ilLineno = -1;
			while (FindGroupAndLine(olPst,ilGroupno,ilLineno))
			{
				if (IsPassFilter(GetGroup(ilGroupno)->PstAreaId,olAlc,prpFlight->Hara,prpFlight->Stev,prpFlight->Ttyp,prpFlight->Act3,prpFlight->Act5,prpFlight->Urno))
				{
					int ilOldMaxOverlapLevel = GetVisualMaxOverlapLevel(ilGroupno, ilLineno);
					ogConflicts.CheckConflicts(*prpFlight,FALSE,FALSE,TRUE);
					MakeBar(ilGroupno, ilLineno, prpFlight, false, olReturnFlightTypes[ilRetFlight]);
					if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
					{
						LONG lParam = MAKELONG(ilLineno, ilGroupno);
						if (GetVisualMaxOverlapLevel(ilGroupno, ilLineno) == ilOldMaxOverlapLevel)
							pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
						else
							pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
					}
				}
			}
		}

		if(bpCheckRotation && (prlRotationFlight = ogFlightData.GetRotationFlight(prpFlight, olReturnFlightTypes[ilRetFlight])) != NULL)
			ProcessFlightChange(prlRotationFlight, false);
	}
}

void PstDiagramViewer::ProcessFlightSelect(FLIGHTDATA *prpFlight)
{
	if(::IsWindow(pomAttachWnd->GetSafeHwnd()) && prpFlight != NULL)
	{
		PST_SELECTION	rolSelection;
		if(FindFlightBar(prpFlight->Urno,rolSelection.imGroupno,rolSelection.imLineno,rolSelection.imBarno,true, prpFlight->ReturnFlightType))
		{
			LONG lParam = reinterpret_cast<LONG>(&rolSelection);
			pomAttachWnd->SendMessage(WM_SELECTDIAGRAM,UD_SELECTBAR, lParam);
		}
	}
}

void PstDiagramViewer::ProcessSpecialJobChange(JOBDATA *prpJob)
{
	int ilGroupno, ilLineno, ilBarno;
	if (strcmp(prpJob->Jtco,JOBSTAIRCASE) != 0)
		return; // not interested in other jobtypes
	if (!FindFlightBar(prpJob->Urno, ilGroupno, ilLineno, ilBarno))
		return; // can't do anything

	// Update the Viewer's data
	int ilOldMaxOverlapLevel = GetVisualMaxOverlapLevel(ilGroupno, ilLineno);
	DeleteBar(ilGroupno, ilLineno, ilBarno);
	MakeSpecialBar(ilGroupno, ilLineno, prpJob);

	// Notifies the attached window (if exist)
	if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
	{
		LONG lParam = MAKELONG(ilLineno, ilGroupno);
		if (GetVisualMaxOverlapLevel(ilGroupno, ilLineno) == ilOldMaxOverlapLevel)
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
		else
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
	}
}

void PstDiagramViewer::ProcessSpecialJobDelete(JOBDATA *prpJob)
{
	int ilGroupno, ilLineno, ilBarno;
	if (strcmp(prpJob->Jtco,JOBSTAIRCASE) != 0)
	{
		return; // not interested in other jobtypes
	}
	if (!FindFlightBar(prpJob->Urno, ilGroupno, ilLineno, ilBarno))
		return; // can't do anything

	// Update the Viewer's data
	int ilOldMaxOverlapLevel = GetVisualMaxOverlapLevel(ilGroupno, ilLineno);
	DeleteBar(ilGroupno, ilLineno, ilBarno);

	// Notifies the attached window (if exist)
	if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
	{
		LONG lParam = MAKELONG(ilLineno, ilGroupno);
		if (GetVisualMaxOverlapLevel(ilGroupno, ilLineno) == ilOldMaxOverlapLevel)
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
		else
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
	}
}

void PstDiagramViewer::ProcessSpecialJobNew(JOBDATA *prpJob)
{
	int ilGroupno, ilLineno, ilBarno;
	if (strcmp(prpJob->Jtco,JOBSTAIRCASE) != 0)
	{
		return; // not interested in other jobtypes
	}
	if (FindFlightBar(prpJob->Urno, ilGroupno, ilLineno, ilBarno))
		ProcessSpecialJobChange(prpJob); // job already there

	// Update the Viewer's data
	//int ilOldMaxOverlapLevel = GetVisualMaxOverlapLevel(ilGroupno, ilLineno);
	ilGroupno = -1;
	ilLineno = -1;
	while(FindGroupAndLine(prpJob->Alid,ilGroupno,ilLineno))
	{
		int ilOldMaxOverlapLevel = GetVisualMaxOverlapLevel(ilGroupno, ilLineno);
		MakeSpecialBar(ilGroupno, ilLineno, prpJob);

		// Notifies the attached window (if exist)
		if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
		{
			LONG lParam = MAKELONG(ilLineno, ilGroupno);
			if (GetVisualMaxOverlapLevel(ilGroupno, ilLineno) == ilOldMaxOverlapLevel)
				pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
			else
				pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
		}
	}
}

void PstDiagramViewer::ProcessFmgJobNew(JOBDATA *prpJob)
{
// Id 19-Sep-96
	// Don't interest it if it's not a flight manager position job
	if (CString(prpJob->Jtco) != JOBFMPSTAREA)
		return;

	int ilGroupno;
	if (!FindGroup(prpJob->Alid, ilGroupno))
        return; // no such group in the viewer buffer right now, just do nothing
// end of Id 19-Sep-96

	// Update the Viewer's data
	MakeManager(ilGroupno, prpJob);

	// Notifies the attached window (if exist)
	if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
	{
		LONG lParam = MAKELONG(-1, ilGroupno);
		pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATEGROUP, lParam);
	}
}

void PstDiagramViewer::ProcessFmgJobDelete(JOBDATA *prpJob)
{
// Id 19-Sep-96
	// Don't interest it if it's not a flight manager position job
	if (CString(prpJob->Jtco) != JOBFMPSTAREA)
		return;
// end of Id 19-Sep-96

	int ilGroupno, ilManagerno;
	if (!FindManager(prpJob->Urno, ilGroupno, ilManagerno))
        return; // no such group in the viewer buffer right now, just do nothing

	// Update the Viewer's data
	DeleteManager(ilGroupno, ilManagerno);

	// Notifies the attached window (if exist)
	if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
	{
		LONG lParam = MAKELONG(-1, ilGroupno);
		pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATEGROUP, lParam);
	}
}

/////////////////////////////////////////////////////////////////////////////
// PstDiagramViewer -- Filtering, Sorting, and Grouping
//
// Requirements:
// In the Pst Gantt Diagram we have only one filter, by Pst Areas.
// It will be always groupped and sorted by Pst Areas.
//
// Methods for change the view:
// ChangeViewTo		Change view, reload everything from Ceda????Data
// UpdateManagers	Rebuild the manager lists for all groups
//
// PrepareFilter	Prepare CMapStringToPtr or CMapPtrToPtr for filtering
// PrepareSorter	Prepare sort criterias before sorting take place
//
// IsPassFilter		Return TRUE if the given record data satisfies the filter
// CompareGroup		Return 0 or -1 or 1 as the result of comparison of two groups
// CompareLine		Return 0 or -1 or 1 as the result of comparison of two lines
// CompareManager	Return 0 or -1 or 1 as the result of comparison of two managers
//
// Programmer notes:
// At the program start up, we will create a default view for each dialog. This will
// overwrite the <default> view saved in the last time execution.
//
// When the viewer is constructed or everytimes the view changes, we will clear
// everything from the viewer buffer and do the insertion sorting. We will insert
// the record only if it satisfies our filter. Conceptually we will filter the record
// first, then sort in (by insertion sort), and group in.
//
// To let the filter work fast, we will implement a CMapStringToPtr for every filter
// condition. For example, let's say that we have a CStringArray for every possible
// values of a filter. We will have a CMapStringToPtr which could tell us the given
// key value is selected in the filter or not.
//
void PstDiagramViewer::ChangeViewTo(const char *pcpViewName,
	BOOL bpIncludeArrivalFlights, BOOL bpIncludeDepartureFlights,
	CTime opStartTime, CTime opEndTime)
{
	DeleteAll();	// remove everything

	ogCfgData.rmUserSetup.PSCV = pcpViewName;
	SelectView(pcpViewName);
	PrepareFilter();
	bmIncludeArrivalFlights = bpIncludeArrivalFlights;
	bmIncludeDepartureFlights = bpIncludeDepartureFlights;
	omStartTime = opStartTime;
	omEndTime = opEndTime;

	ChangeViewTo();
}

void PstDiagramViewer::ChangeViewTo(void)
{
	MakeGroupsAndLines();
	MakeManagers();
	MakeBars();
}

void PstDiagramViewer::UpdateManagers(CTime opStartTime, CTime opEndTime)
{
	omStartTime = opStartTime;
	omEndTime = opEndTime;

	int ilGroupCount = GetGroupCount();
	for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
	{
	    while (GetManagerCount(ilGroupno) > 0)
		    DeleteManager(ilGroupno, 0);
	}
	MakeManagers();

	// Notifies the attached window (if exist)
	if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
	{
		for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
		{
			LONG lParam = MAKELONG(-1, ilGroupno);
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATEGROUP, lParam);
		}
	}
}

void PstDiagramViewer::PrepareFilter()
{
	CStringArray olFilterValues;
	int i, n;

	bmDisplayJobConflicts = (GetUserData("DISPLAYJOBCONFLICTS") == "YES") ? true : false;
	bmOneFlightPerLine = (GetUserData("ONEFLIGHTPERLINE") == "YES") ? true : false;


	// Prepare filter for all possible position areas
	omCMapForPstArea.RemoveAll();
	GetFilter("Pstbereich", olFilterValues);
	n = olFilterValues.GetSize();
	bmUseAllPstAreas = false;
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllPstAreas = true;
		else
			for (i = 0; i < n; i++)
				omCMapForPstArea[olFilterValues[i]] = NULL;
	}

    GetFilter("Airline", olFilterValues);
    omCMapForAlcd.RemoveAll();
    n = olFilterValues.GetSize();
	bmUseAllAlcds = false;
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllAlcds = true;
		else
			for (i = 0; i < n; i++)
				omCMapForAlcd[olFilterValues[i]] = NULL;
	}
	bmEmptyAirlineSelected = CheckForEmptyValue(omCMapForAlcd, GetString(IDS_NO_AIRLINE));

    GetFilter("Keycodes", olFilterValues);
    omCMapForKeycodes.RemoveAll();
    n = olFilterValues.GetSize();
	bmUseAllKeycodes = false;
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllKeycodes = true;
		else
			for (i = 0; i < n; i++)
				omCMapForKeycodes[olFilterValues[i]] = NULL;
	}

    if(GetFilter("Natures", olFilterValues))
		bmUseAllNatures = false;
	else
		bmUseAllNatures = true;
    omCMapForNatures.RemoveAll();
    n = olFilterValues.GetSize();
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllNatures = true;
		else
			for (i = 0; i < n; i++)
				omCMapForNatures[olFilterValues[i]] = NULL;
	}

    if(GetFilter("Act", olFilterValues))
		bmUseAllActs = false;
	else
		bmUseAllActs = true;
	bmNoAct = false;
    omCMapForAct3.RemoveAll();
    omCMapForAct5.RemoveAll();
	CString olAct3, olAct5, olAct;
    n = olFilterValues.GetSize();
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
		{
			bmUseAllActs = true;
		}
		else
		{
			for (i = 0; i < n; i++)
			{
				olAct = olFilterValues[i];
				if(!bmNoAct && olAct == GetString(IDS_NOACT))
				{
					bmNoAct = true;
				}
				else
				{
					ogActData.GetAct3AndAct5FromFormattedName(olAct, olAct3, olAct5);
					if(!olAct3.IsEmpty())
						omCMapForAct3[olAct3] = NULL;
					if(!olAct5.IsEmpty())
						omCMapForAct5[olAct5] = NULL;
				}
			}
		}
	}

	if(!ogBasicData.bmDisplayEquipment)
	{
		imDemandTypes = PERSONNELDEMANDS;
	}
	else
	{
		GetFilter("DemandType", olFilterValues);
		imDemandTypes = NODEMANDS;
		n = olFilterValues.GetSize();
		if(n > 0)
		{
			if(olFilterValues[0] == GetString(IDS_ALLSTRING))
			{
				imDemandTypes |= PERSONNELDEMANDS;
				imDemandTypes |= EQUIPMENTDEMANDS;
				imDemandTypes |= LOCATIONDEMANDS;
			}
			else
			{
				for (i = 0; i < n; i++)
				{
					if(!strcmp(olFilterValues[i],GetString(IDS_PERSONNEL_DEM)))
					{
						imDemandTypes |= PERSONNELDEMANDS;
					}
					else if(!strcmp(olFilterValues[i],GetString(IDS_EQUIPMENT_DEM)))
					{
						imDemandTypes |= EQUIPMENTDEMANDS;
					}
					else if(!strcmp(olFilterValues[i],GetString(IDS_LOCATION_DEM)))
					{
						imDemandTypes |= LOCATIONDEMANDS;
					}
				}
			}
		}
	}

    GetFilter("Agents", olFilterValues);
    omCMapForAgents.RemoveAll();
	n = olFilterValues.GetSize();
	bmUseAllAgents = false;
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllAgents = true;
		else
			for (i = 0; i < n; i++)
				omCMapForAgents[olFilterValues[i]] = NULL;
	}

	//PRF 8999
	GetFilter("Functions", olFilterValues);
    omCMapForFunctions.RemoveAll();
	n = olFilterValues.GetSize();
	bmUseAllFunctions = FALSE;
	/*if(GetViewName().Compare("<Default>") == 0)
	{
		bmUseAllFunctions = TRUE;		
		omCMapForFunctions[GetString(IDS_ALLSTRING)] = NULL;
	}*/
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
		{
			bmUseAllFunctions = TRUE;
			//omCMapForFunctions[olFilterValues[0]] = NULL;
		}
		else
		{
			for (i = 0; i < n; i++)
			{
				omCMapForFunctions[olFilterValues[i]] = NULL;
			}
		}
	}


	GetFilter("EquDemandGroups", olFilterValues);
    omCMapForEquDemandGroups.RemoveAll();
	n = olFilterValues.GetSize();
	bmUseAllEquDemandGroups = FALSE;
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
		{
			bmUseAllEquDemandGroups = TRUE;
			//omCMapForEquDemandGroups[olFilterValues[0]] = NULL;
		}
		else
		{
			for (i = 0; i < n; i++)
			{
				omCMapForEquDemandGroups[olFilterValues[i]] = NULL;
			}
		}
	}
}

// causes MakeBar to be called twice for return flights (return flight is a single record for both the arr and dep)
int PstDiagramViewer::GetReturnFlightTypes(FLIGHTDATA *prpFlight, CUIntArray &ropReturnFlightTypes, CString opAlid /*""*/)
{
	ropReturnFlightTypes.RemoveAll();
	if(prpFlight != NULL)
	{
		if(ogFlightData.IsReturnFlight(prpFlight))
		{
			// add 2 bars for return flights once for inbound once for outbound
			if(opAlid.IsEmpty() || !strcmp(prpFlight->Psta,opAlid))
				ropReturnFlightTypes.Add(ID_ARR_RETURNFLIGHT);
			if(opAlid.IsEmpty() || !strcmp(prpFlight->Pstd,opAlid))
				ropReturnFlightTypes.Add(ID_DEP_RETURNFLIGHT);
		}
		else
		{
			ropReturnFlightTypes.Add(ID_NOT_RETURNFLIGHT);
		}
	}

	return ropReturnFlightTypes.GetSize();
}

BOOL PstDiagramViewer::IsPassFilter(const char *pcpPstArea)
{
	void *p;
	// there is only one filter, so we just check it with possible position areas
	return bmUseAllPstAreas || omCMapForPstArea.Lookup(CString(pcpPstArea), p);
}

BOOL PstDiagramViewer::IsPassFilter(const char *pcpPstArea,const char *pcpAlcd,const char *pcpHara, const char *pcpKeycode, const char *pcpNature, const char *pcpAct3, const char *pcpAct5, const long lpFlightUrno)
{
	void *p;

	if(omDelegatedFlights.Lookup((void *)lpFlightUrno,(void *& )p) == TRUE)
		return TRUE;

	BOOL blIsPstAreaOK	= bmUseAllPstAreas || strlen(pcpPstArea) <= 0 || omCMapForPstArea.Lookup(CString(pcpPstArea), p);
    BOOL blIsAlcdOK		= bmUseAllAlcds || (strlen(pcpAlcd)== 0 && bmEmptyAirlineSelected == true) || omCMapForAlcd.Lookup(CString(pcpAlcd), p);
    BOOL blIsAgentOK	= bmUseAllAgents || (strlen(pcpHara) <= 0 && omCMapForAgents.Lookup(GetString(IDS_NOAGENT),p)) || omCMapForAgents.Lookup(CString(pcpHara), p);
    BOOL blIsKeycodeOK		= bmUseAllKeycodes || omCMapForKeycodes.Lookup(CString(pcpKeycode), p);
    BOOL blIsNatureOK	= bmUseAllNatures || (strlen(pcpNature) <= 0 && omCMapForNatures.Lookup(GetString(IDS_NONATURE),p)) || omCMapForNatures.Lookup(CString(pcpNature), p);

	//BOOL blIsActOK	= bmUseAllActs || (bmNoAct && strlen(pcpAct3) <= 0 && strlen(pcpAct5) <= 0) || omCMapForAct3.Lookup(CString(pcpAct3), p) || omCMapForAct5.Lookup(CString(pcpAct5), p);
	BOOL blIsActOK	= bmUseAllActs;
	if(!blIsActOK)
	{
		bool blAct3Empty = (strlen(pcpAct3) <= 0) ? true : false;
		bool blAct5Empty = (strlen(pcpAct5) <= 0) ? true : false;
		if(blAct3Empty && blAct5Empty)
			blIsActOK = bmNoAct;
		else if(!blAct3Empty && !blAct5Empty)
			blIsActOK = omCMapForAct3.Lookup(CString(pcpAct3), p) && omCMapForAct5.Lookup(CString(pcpAct5), p);
		else if(!blAct3Empty)
			blIsActOK = omCMapForAct3.Lookup(CString(pcpAct3), p);
		else if(!blAct5Empty)
			blIsActOK = omCMapForAct5.Lookup(CString(pcpAct5), p);
	}

	return blIsPstAreaOK && blIsAlcdOK && blIsAgentOK && blIsKeycodeOK && blIsNatureOK && blIsActOK;
}

int PstDiagramViewer::CompareGroup(PST_GROUPDATA *prpGroup1, PST_GROUPDATA *prpGroup2)
{
	// Groups in PstDiagram always ordered by the displayed text, so let's compare them.
	CString &s1 = prpGroup1->Text;
	CString &s2 = prpGroup2->Text;
	return (s1 == s2)? 0: (s1 > s2)? 1: -1;
}

int PstDiagramViewer::CompareLine(PST_LINEDATA *prpLine1, PST_LINEDATA *prpLine2)
{
	// Lines in PstDiagram always ordered by the displayed text, so let's compare them.
	CString &s1 = prpLine1->Text;
	CString &s2 = prpLine2->Text;
	return (s1 == s2)? 0: (s1 > s2)? 1: -1;
}

int PstDiagramViewer::CompareManager(PST_MANAGERDATA *prpManager1, PST_MANAGERDATA *prpManager2)
{
	// Compare manager -- we will sort them by name
	return  (prpManager1->EmpLnam == prpManager2->EmpLnam)? 0:
		(prpManager1->EmpLnam > prpManager2->EmpLnam)? 1: -1;
}


/////////////////////////////////////////////////////////////////////////////
// PstDiagramViewer -- Data for displaying graphical objects
//
// The related graphical class: PstDiagram, PstChart, and PstGantt will use this
// PstViewer as the source of data for displaying GanttLine and GanttBar objects.
//
// We let the previous section handles all filtering, sorting, and grouping. In this
// section, we will provide a set of methods which will help us to create the data
// represent the graphical GanttLine and GanttChart as easiest as possible.
//
// Methods for creating graphical objects:
// MakeGroupsAndLines		Create groups and lines that has to be displayed.
// MakeManagers				Create managers for all groups
// MakeBars					Create bars for all lines in all groups
// MakeManager				Create a manager for the specified group
// MakeBar					Create a flight bar (determine its length with demands)
// MakeIndicators			Create indicators for flight bar
//
// GetDemands				Get all demands for the given flight
//
// ArrivalFlightStatus		Return a string displayed for arrival flight
// DepartureFlightStatus	Return a string displayed for departure flight
// TurnAroundFlightStatus	Return a string displayed for turn-around flight
//
void PstDiagramViewer::MakeGroupsAndLines()
{
	// reset all flights (and shadow bars) to not displayed
	int ilFlightCount = ogFlightData.omData.GetSize();
	for ( int ilLc = 0; ilLc < ilFlightCount; ilLc++)
	{
		ogFlightData.omData[ilLc].IsDisplayed = FALSE;
		ogFlightData.omData[ilLc].IsShadoBarDisplayed = FALSE;
	}

	// ogPstAreas contains all metawerte for positions areas

	// Create a group for each position area (only areas which satisfy the condition)
	CCSPtrArray <ALLOCUNIT> olPstAreas;
	CCSPtrArray <ALLOCUNIT> olPsts;

	ogAllocData.GetGroupsByGroupType(ALLOCUNITTYPE_PSTGROUP,olPstAreas);
	int ilNumPstAreas = olPstAreas.GetSize();
	for(int ilPstArea = 0; ilPstArea < ilNumPstAreas; ilPstArea++)
	{
		ALLOCUNIT *prlPstArea = &olPstAreas[ilPstArea];

		if (!IsPassFilter(prlPstArea->Name))	// this position area is not in the filter?
			continue;

		// create a chart for each group
        PST_GROUPDATA rlGroup;
		rlGroup.PstAreaId = prlPstArea->Name;
		rlGroup.Text = prlPstArea->Name;
		int ilGroupno = CreateGroup(&rlGroup);

		ogAllocData.GetUnitsByGroup(ALLOCUNITTYPE_PSTGROUP,prlPstArea->Name,olPsts);
		int ilNumPsts = olPsts.GetSize();
		for(int ilPst = 0; ilPst < ilNumPsts; ilPst++)
		{
			ALLOCUNIT *prlPst = &olPsts[ilPst];
			PST_LINEDATA rlLine;
			rlLine.Text = prlPst->Name;
			rlLine.IsStairCasePst = false;
            CreateLine(ilGroupno, &rlLine);
		}
	}

	// create a group containing all flights
	CString olAllFlightsGrp = GetString(IDS_ALL_FLIGHTS);
	imAllFlightsGroup = -1;
	if(IsPassFilter(olAllFlightsGrp))
	{
		PST_GROUPDATA rlGroup;
		rlGroup.PstAreaId = olAllFlightsGrp; 
		rlGroup.Text = olAllFlightsGrp;
		CreateGroup(&rlGroup);
	}

	// create a group for flights without positions
	CString olWithoutPstName = GetString(IDS_NO_POSITION); // "Ohne Pst"
	imFlightsWithoutPstsGroup = -1;
	if(IsPassFilter(olWithoutPstName))
	{
		PST_GROUPDATA rlGroup;
		rlGroup.PstAreaId = olWithoutPstName; 
		rlGroup.Text = olWithoutPstName;
		CreateGroup(&rlGroup);
	}

	// get indexes - don't do this until both are created as inserting can change the indexes
	FindGroup(GetString(IDS_ALL_FLIGHTS),imAllFlightsGroup);
	FindGroup(GetString(IDS_NO_POSITION),imFlightsWithoutPstsGroup);
}

void PstDiagramViewer::MakeManagers()
{
	int ilGroupCount = GetGroupCount();
	for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
	{
		// Create managers associated with this CCI area
		CCSPtrArray<JOBDATA> olFmgJobs;
		ogJobData.GetFmgJobsByAlid(olFmgJobs, GetGroup(ilGroupno)->PstAreaId);
		int ilFmgCount = olFmgJobs.GetSize();
		for (int ilLc = 0; ilLc < ilFmgCount; ilLc++)
			MakeManager(ilGroupno, &olFmgJobs[ilLc]);
	}
}

void PstDiagramViewer::MakeBars()
{
	int ilLc;
	int ilGroupCount = GetGroupCount();
	for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
	{
		if(ilGroupno == imFlightsWithoutPstsGroup)
		{
			CreateLinesForFlightsWithoutPsts();
		}
		else if(ilGroupno == imAllFlightsGroup)
		{
			CreateLinesForAllFlights();  
		}
		else
		{
			PST_GROUPDATA *prlGroup = GetGroup(ilGroupno);
			FLIGHTDATA *prlFlight = NULL;

			CString olAlc;
			int ilLineCount = GetLineCount(ilGroupno);
			for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
			{
				if ((bmIncludeArrivalFlights) || (bmIncludeDepartureFlights))
				{
					CCSPtrArray<FLIGHTDATA> olFlights;
					CString olPosition = GetLineText(ilGroupno, ilLineno);
					ogFlightData.GetFlightsByPosition(olFlights, olPosition);
					for (ilLc = 0; ilLc < olFlights.GetSize(); ilLc++)
					{
						prlFlight = &olFlights[ilLc];
						olAlc = ogAltData.FormatAlcString(prlFlight->Alc2, prlFlight->Alc3);
						if (IsPassFilter(prlGroup->PstAreaId,olAlc,prlFlight->Hara,prlFlight->Stev,prlFlight->Ttyp,prlFlight->Act3,prlFlight->Act5,prlFlight->Urno))
						{	
							CUIntArray olReturnFlightTypes;
							int ilNumReturnFlightTypes = GetReturnFlightTypes(prlFlight, olReturnFlightTypes, olPosition);
							for(int ilF = 0; ilF < ilNumReturnFlightTypes; ilF++)
							{
								MakeBar(ilGroupno, ilLineno, prlFlight, false, olReturnFlightTypes[ilF]);
							}
						}
					}

					
				}
				CCSPtrArray<JOBDATA> olJobs;
				ogJobData.GetSpecialJobsByAlid(olJobs, GetLineText(ilGroupno, ilLineno), ALLOCUNITTYPE_PST);
				for (ilLc = 0; ilLc < olJobs.GetSize(); ilLc++)
				{
					MakeSpecialBar(ilGroupno, ilLineno, &olJobs[ilLc]);
				}
			}
		}

	}
}


void PstDiagramViewer::MakeManager(int ipGroupno, JOBDATA *prpJob)
{
	if (!IsOverlapped(prpJob->Acfr, prpJob->Acto, omStartTime, omEndTime))
		return;
	
	PST_MANAGERDATA rlFm;
	rlFm.JobUrno = prpJob->Urno;
	SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prpJob->Shur);
	rlFm.ShiftStid = prlShift != NULL ? prlShift->Sfca : "";
	rlFm.ShiftTmid = prlShift != NULL ? prlShift->Egrp : "";
	EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prpJob->Peno);
	rlFm.EmpLnam = CString(prlEmp != NULL ? prlEmp->Lanm : "");
	if (prlShift != NULL)
	{
		if ((prlShift->Avfa < prpJob->Acfr) || (prlShift->Avta > prpJob->Acto))
		{
			rlFm.ShiftStid += "'";
		}
	}
	CreateManager(ipGroupno, &rlFm);
}

// bpIgnorePosition -	if true then a turnaround bar will be created for turnaround flights with turnaround
// demands even if they don't have the same position (useful for the "All Flights" group)
//
// ipReturnFlightType - 0=not a return flight 1=Arrival return flight  2=Departure return flight
void PstDiagramViewer::MakeBar(int ipGroupno, int ipLineno, FLIGHTDATA *prpFlight, bool bpIgnorePosition, int ipReturnFlightType /*ID_NOT_RETURNFLIGHT*/)
{
	bool blIsArrival = (ogFlightData.IsArrival(prpFlight) || ipReturnFlightType == ID_ARR_RETURNFLIGHT);		// Inbound flight
	bool blIsDeparture = (ogFlightData.IsDeparture(prpFlight) || ipReturnFlightType == ID_DEP_RETURNFLIGHT);	// Outbound flight
//	bool blDisplayPosition = (ipGroupno == imAllFlightsGroup) ? true : false;

	CCSPtrArray<DEMANDDATA> olArrivalDemands;
	CCSPtrArray<DEMANDDATA> olDepartureDemands;

	FLIGHTDATA *prlRotationFlight = ogFlightData.GetRotationFlight(prpFlight, ipReturnFlightType);
	int ilTypeIdx = (ipReturnFlightType == ID_DEP_RETURNFLIGHT) ? 1 : 0;

	FLIGHTDATA *prlArr = NULL, *prlDep = NULL;
	if(blIsArrival)
	{
		prlArr = prpFlight;
		prlDep = prlRotationFlight;
	}
	else
	{
		prlArr = prlRotationFlight;
		prlDep = prpFlight;
	}

	if (prlDep != NULL)
	if (!strncmp(prlDep->Flno,"SQ",2))
	{
		int xx = 28;
	}
	if(blIsArrival)
	{
		if(!bmIncludeArrivalFlights)
		{
			return;
		}

		// load arrival and turnaround demands
		ogDemandData.GetDemandsByInboundFlight(olArrivalDemands, prpFlight->Urno, "", ALLOCUNITTYPE_PST,imDemandTypes);
		if(olArrivalDemands.GetSize() <= 0)
		{
			if (bmIncludeArrivalFlights)
			{
				MakeBarWithoutDemand(ipGroupno,ipLineno,prpFlight,ipReturnFlightType);
				return;
			}
		}

		if(prlRotationFlight != NULL)
		{
			// load departure and turnaround demands
			ogDemandData.GetDemandsByOutboundFlight(olDepartureDemands, prlRotationFlight->Urno, "", ALLOCUNITTYPE_PST,imDemandTypes);
		}
	}
	else if (blIsDeparture)
	{
		if(!bmIncludeDepartureFlights)
		{
			return;
		}

		// load departure and turnaround demands
		ogDemandData.GetDemandsByOutboundFlight(olDepartureDemands, prpFlight->Urno, "", ALLOCUNITTYPE_PST,imDemandTypes);
		if(olDepartureDemands.GetSize() <= 0)
		{
			if (ogBasicData.bmDelegateFlights)
			{
				ogDemandData.GetRestrictedDemandsByOutboundFlight(olDepartureDemands, prpFlight->Urno, "", ALLOCUNITTYPE_PST,imDemandTypes);
			}
			if(olDepartureDemands.GetSize() <= 0)
			{

				if (bmIncludeDepartureFlights)
				{
					MakeBarWithoutDemand(ipGroupno,ipLineno,prpFlight,ipReturnFlightType);
					return;
				}
			}
		}

		if(prlRotationFlight != NULL)
		{
			// load arrival and turnaround demands
			ogDemandData.GetDemandsByInboundFlight(olArrivalDemands, prlRotationFlight->Urno, "", ALLOCUNITTYPE_PST,imDemandTypes);
		}
		if(olArrivalDemands.GetSize() <= 0)
		{
			if (ogBasicData.bmDelegateFlights)
			{
				if (prlRotationFlight != NULL)
				ogDemandData.GetRestrictedDemandsByInboundFlight(olArrivalDemands, prlRotationFlight->Urno, "", ALLOCUNITTYPE_PST,imDemandTypes);
			}
		}

	}
	else	// Invalid data?
	{
		return;
	}


	bool blHasTurnaroundDemands = false;
	int ilNumDemands, ilDem;
	CTime olArrStart = TIMENULL, olArrEnd = TIMENULL;
	CTime olDepStart = TIMENULL, olDepEnd = TIMENULL;
	DEMANDDATA *prlDemand;

	bool blDemandFound = false;
	ilNumDemands = olArrivalDemands.GetSize();
	for(ilDem = 0; ilDem < ilNumDemands; ilDem++)
	{
		prlDemand = &olArrivalDemands[ilDem];
		
		if(CheckDemandValidity(prlDemand) == FALSE) //PRF 8999
		{			
			continue;
		}

		blDemandFound = true;
		if(olArrStart == TIMENULL || olArrStart > prlDemand->Debe)
		{
			olArrStart = prlDemand->Debe;
		}

		if(olArrEnd == TIMENULL || olArrEnd < prlDemand->Deen)
		{
			olArrEnd = prlDemand->Deen;
		}
	}
	
	if(ilNumDemands > 0 && blDemandFound == false)
	{
		for(ilDem = 0; ilDem < ilNumDemands; ilDem++)
		{
			if(olArrStart == TIMENULL || olArrStart < prlDemand->Debe)
			{
				olArrStart = prlDemand->Debe;
			}

			if(olArrEnd == TIMENULL || olArrEnd > prlDemand->Deen)
			{
				olArrEnd = prlDemand->Deen;
			}
		}
	}


	blDemandFound = false;
	ilNumDemands = olDepartureDemands.GetSize();
	for(ilDem = 0; ilDem < ilNumDemands; ilDem++)
	{
		prlDemand = &olDepartureDemands[ilDem];

		if(CheckDemandValidity(prlDemand) == FALSE) //PRF 8999
		{	
			continue;
		}

		blDemandFound = true;
		if(olDepStart == TIMENULL || olDepStart > prlDemand->Debe)
		{
			olDepStart = prlDemand->Debe;
		}

		if(olDepEnd == TIMENULL || olDepEnd < prlDemand->Deen)
		{
			olDepEnd = prlDemand->Deen;
		}

		if(*prlDemand->Dety == TURNAROUND_DEMAND)
		{
			if(olArrStart == TIMENULL || olArrStart > prlDemand->Debe)
			{
				olArrStart = prlDemand->Debe;
			}

			if(olArrEnd == TIMENULL || olArrEnd < prlDemand->Deen)
			{
				olArrEnd = prlDemand->Deen;
			}
			if(prlArr != NULL && prlDep != NULL && prlArr->Urno == prlDemand->Ouri && prlDep->Urno == prlDemand->Ouro)
			{
				blHasTurnaroundDemands = true;
			}
		}
	}

	if(ilNumDemands > 0 && blDemandFound == false)
	{
		for(ilDem = 0; ilDem < ilNumDemands; ilDem++)
		{
			if(olDepStart == TIMENULL || olDepStart < prlDemand->Debe)
			{
				olDepStart = prlDemand->Debe;
			}

			if(olDepEnd == TIMENULL || olDepEnd > prlDemand->Deen)
			{
				olDepEnd = prlDemand->Deen;
			}
			if(*prlDemand->Dety == TURNAROUND_DEMAND)
			{
				if(olArrStart == TIMENULL || olArrStart < prlDemand->Debe)
				{
					olArrStart = prlDemand->Debe;
				}

				if(olArrEnd == TIMENULL || olArrEnd > prlDemand->Deen)
				{
					olArrEnd = prlDemand->Deen;
				}
				if(prlArr != NULL && prlDep != NULL && prlArr->Urno == prlDemand->Ouri && prlDep->Urno == prlDemand->Ouro)
				{
					blHasTurnaroundDemands = true;
				}
			}
		}
	}

	bool blIsTurnaround = false;
	bool blHasSamePosition = (ogFlightData.GetPosition(prpFlight) == ogFlightData.GetPosition(prlRotationFlight) || bpIgnorePosition);

	if(ogBasicData.bmDisplayTurnaroundPositions && prlRotationFlight != NULL && 
		IsPassFilter("",ogAltData.FormatAlcString(prlRotationFlight->Alc2,prlRotationFlight->Alc3),prlRotationFlight->Hara,prlRotationFlight->Stev,prlRotationFlight->Ttyp,prlRotationFlight->Act3,prlRotationFlight->Act5,prlRotationFlight->Urno) &&
		blHasTurnaroundDemands && bmIncludeArrivalFlights && 
		bmIncludeDepartureFlights && blHasSamePosition)
	{
		if(blIsArrival)
		{
			// don't need to display the arrival if the departure half has turnaround demands
			return;
		}
		blIsTurnaround = true;

		// this is a turnaround so reload the arrival demands without the turnaround demands
		olArrivalDemands.RemoveAll();
		ogDemandData.GetInboundDemandsByInboundFlight(olArrivalDemands, prlRotationFlight->Urno, "", ALLOCUNITTYPE_PST,imDemandTypes);
	}


	PST_BARDATA rlBar;
	rlBar.IsDelayed = CFST_NOTSET;

//	CString olPstChangeText = ogConflicts.GetAdditionalConflictText(CFI_POSITIONCHANGE,prpFlight);

	CString olBarText = GetBarText(prpFlight, blIsArrival, blIsTurnaround ? prlRotationFlight : NULL);
	CString olStatusText = GetStatusText(prpFlight, blIsTurnaround ? prlRotationFlight : NULL);
	if (bmIncludeArrivalFlights && blIsArrival && !blIsTurnaround)
	{
//		if(blDisplayPosition && strlen(prpFlight->Psta) > 0)
//		{
//			olBarText = prpFlight->Psta + CString("/");
//		}
//		olBarText += CString(prpFlight->Fnum) + " " + olPstChangeText;
		rlBar.Text = olBarText;
		rlBar.StatusText = (olStatusText.IsEmpty()) ? olBarText + ArrivalFlightStatus(prpFlight, prlRotationFlight) : olStatusText;
		rlBar.StartTime = olArrStart;
		rlBar.EndTime = olArrEnd;
		rlBar.IsTmo = prpFlight->IsTmo;
		rlBar.NoDropOnThisBar = FALSE;
		rlBar.FlightType = PSTBAR_ARRIVAL;
	}
	else if (bmIncludeDepartureFlights && blIsDeparture && !blIsTurnaround)
	{
//		olBarText = CString(prpFlight->Fnum) + " " + olPstChangeText;
//		if(blDisplayPosition && strlen(prpFlight->Pstd) > 0)
//		{
//			olBarText += CString("/") + prpFlight->Pstd;
//		}
		rlBar.Text = olBarText;
		rlBar.StatusText = (olStatusText.IsEmpty()) ? olBarText + DepartureFlightStatus(prpFlight, prlRotationFlight) : olStatusText;
		rlBar.StartTime = olDepStart;
		rlBar.EndTime = olDepEnd;
		rlBar.IsTmo = FALSE;
		rlBar.NoDropOnThisBar = FALSE;
		rlBar.FlightType = PSTBAR_DEPARTURE;
	}
	else if (blIsTurnaround) 
	{
		if(olArrStart == TIMENULL)
			olArrStart = olDepStart;
		if(olArrEnd == TIMENULL)
			olArrEnd = olDepEnd;

//		if(blDisplayPosition && strlen(prlRotationFlight->Psta) > 0)
//		{
//			olBarText = prlRotationFlight->Psta + CString("/");
//		}
//		olBarText += prlRotationFlight->Fnum + CString("/") + prpFlight->Fnum;
//		if(blDisplayPosition && strlen(prpFlight->Pstd) > 0)
//		{
//			olBarText += CString("/") + prpFlight->Pstd;
//		}
		rlBar.Text = olBarText;
		rlBar.StatusText = (olStatusText.IsEmpty()) ? olBarText + TurnAroundFlightStatus(prpFlight, prlRotationFlight) : olStatusText;
		rlBar.StartTime = min(olArrStart,olDepStart);
		rlBar.EndTime = min(olArrEnd,olDepEnd);
		rlBar.IsTmo = prpFlight->IsTmo;
		rlBar.NoDropOnThisBar = FALSE;
		rlBar.DepartureUrno = prlRotationFlight->Urno;
		rlBar.FlightType = PSTBAR_TURNAROUND;
	}
	else	// undisplayed data?
	{
		return;
	}

	if (blIsTurnaround)	// Delay only for Departure
	{
		if (prlRotationFlight->IsDelayed != CFST_NOTSET)
		{
			rlBar.Delay = prlRotationFlight->Etod - prlRotationFlight->Stod;
			rlBar.IsDelayed = prlRotationFlight->IsDelayed;
		}
	}
	else if (blIsDeparture)	// Delay only for Departure
	{
		if (prpFlight->IsDelayed != CFST_NOTSET)
		{
			rlBar.Delay = prpFlight->Etod - prpFlight->Stod;
			rlBar.IsDelayed = prpFlight->IsDelayed;
		}
	}
	else
	{
		rlBar.Delay = CTimeSpan(0);
	}

	// Index in global Color Table
	int ilColorIndex;
	BOOL blIsPlanned;
	BOOL blIsInFuture;
	//bool blIsFinished;

	if (blIsTurnaround)
	{
		rlBar.DepReturnFlightType = ogFlightData.GetRotationReturnFlightType(prpFlight,prlRotationFlight,ipReturnFlightType);
		int ilDepTypeIdx = (rlBar.DepReturnFlightType == ID_DEP_RETURNFLIGHT) ? 1 : 0;

		ilColorIndex = ogConflicts.GetRotationConflictColor(prpFlight,prlRotationFlight,ALLOCUNITTYPE_PST,imDemandTypes,ipReturnFlightType,bmDisplayJobConflicts);
		blIsPlanned = prpFlight->PstIsPlanned[ilTypeIdx] || prlRotationFlight->PstIsPlanned[ilDepTypeIdx];
		blIsInFuture = ogFlightData.IsArrival(prpFlight) ? prpFlight->PstIsPlanned[ilTypeIdx] : prlRotationFlight->PstIsPlanned[ilDepTypeIdx];
		rlBar.IsFinished = ogBasicData.IsFinished(prpFlight,ALLOCUNITTYPE_PST,imDemandTypes,ipReturnFlightType) && ogBasicData.IsFinished(prlRotationFlight,ALLOCUNITTYPE_PST,imDemandTypes,rlBar.DepReturnFlightType);
	}
	else
	{
		ilColorIndex = ogConflicts.GetFlightConflictColor(prpFlight, ALLOCUNITTYPE_PST, imDemandTypes, ipReturnFlightType, bmDisplayJobConflicts);
		blIsPlanned = prpFlight->PstIsPlanned[ilTypeIdx];
		blIsInFuture = prpFlight->PstIsInFuture[ilTypeIdx];
		rlBar.IsFinished = ogBasicData.IsFinished(prpFlight,ALLOCUNITTYPE_PST,imDemandTypes,ipReturnFlightType);
	}

	rlBar.MarkerType = (blIsPlanned)? MARKLEFT: (blIsInFuture)? MARKNONE: MARKFULL;
	rlBar.MarkerBrush = ogBrushs[ilColorIndex];
	rlBar.FlightUrno = prpFlight->Urno;
	rlBar.FrameType = FRAMERECT;
	void *p;
	if(omDelegatedFlights.Lookup((void *)rlBar.FlightUrno,(void *& )p) == TRUE)
		rlBar.FrameColor = ORANGE;

	rlBar.IsShadowBar = FALSE;
	rlBar.Type = BAR_PSTFLIGHT;
	rlBar.ReturnFlightType = ipReturnFlightType;

	int ilBarno = CreateBar(ipGroupno, ipLineno, &rlBar);

	// Create indicators associated with this flight
	if (blIsTurnaround)
	{
		MakeIndicators(ipGroupno, ipLineno, ilBarno, olArrivalDemands,olDepartureDemands,prpFlight,prlRotationFlight);
	}
	else if(blIsArrival)
	{
		olDepartureDemands.RemoveAll();
		MakeIndicators(ipGroupno, ipLineno, ilBarno, olArrivalDemands,olDepartureDemands,prpFlight,NULL);
	}
	else if(blIsDeparture)
	{
		olArrivalDemands.RemoveAll();
		MakeIndicators(ipGroupno, ipLineno, ilBarno, olArrivalDemands,olDepartureDemands,NULL,prpFlight);
	}

	return;
}

CString PstDiagramViewer::GetBarText(FLIGHTDATA *prpFlight, bool bpIsArrival, FLIGHTDATA *prpRotation /*=NULL*/)
{
	CString olBarText;

	FLIGHTDATA *prlArr = (bpIsArrival) ? prpFlight : prpRotation;
	FLIGHTDATA *prlDep = (bpIsArrival) ? prpRotation : prpFlight;
	if(prlArr && prlDep)
	{
		olBarText = ogFieldConfig.ConfigureField(IDS_FIELDCONFIG_PSTBARTURN, "%s%s%s%s%s%s%s%s%d%s%s%s%s%s%s%d%s%s", 
												prlArr->Regn, prlArr->Act3, prlArr->Fnum, prlArr->Org3, prlArr->Stoa.Format("%H%M"), 
												prlArr->Etoa.Format("%H%M"), GetGateText(prlArr), GetPositionText(prlArr), prlArr->Pax1, 
												prlDep->Fnum, prlDep->Des3, prlDep->Stod.Format("%H%M"), prlDep->Etod.Format("%H%M"), 
												GetGateText(prlDep), GetPositionText(prlDep),prlDep->Pax1,
												prlArr->Flti, prlDep->Flti);
	}
	else if(prlArr)
	{
		olBarText = ogFieldConfig.ConfigureField(IDS_FIELDCONFIG_PSTBARARR, "%s%s%s%s%s%s%s%s%d%s", 
												prlArr->Fnum, prlArr->Regn, prlArr->Act3, prlArr->Org3, prlArr->Stoa.Format("%H%M"), 
												prlArr->Etoa.Format("%H%M"), GetGateText(prlArr), GetPositionText(prlArr), prlArr->Pax1,prlArr->Flti);
	}
	else if(prlDep)
	{
		olBarText = ogFieldConfig.ConfigureField(IDS_FIELDCONFIG_PSTBARDEP, "%s%s%s%s%s%s%s%s%d%s", 
												prlDep->Fnum, prlDep->Regn, prlDep->Act3, prlDep->Des3, prlDep->Stod.Format("%H%M"), 
												prlDep->Etod.Format("%H%M"), GetGateText(prlDep), GetPositionText(prlDep), prlDep->Pax1, prlDep->Flti);
	}

	if(olBarText.IsEmpty())
	{
		CString olSlash = "/", olDash = " - ", olBlank = " ";
		CString olPstChangeText1 = ogConflicts.GetAdditionalConflictText(CFI_POSITIONCHANGE,prpFlight), olPstChangeText2;
		if(prpRotation != NULL)
			olPstChangeText2 = ogConflicts.GetAdditionalConflictText(CFI_POSITIONCHANGE,prpRotation);

		if(bpIsArrival)
		{
			if(!olPstChangeText1.IsEmpty())
				olBarText += olPstChangeText1 + olSlash;
			else if(strlen(prpFlight->Psta) > 0)
				olBarText += prpFlight->Psta + olSlash;

			if(strlen(prpFlight->Gta1) > 0)
				olBarText += prpFlight->Gta1 + olSlash;

			olBarText += prpFlight->Fnum;
			if(prpRotation != NULL)
			{
				olBarText += olDash + prpRotation->Fnum;

				if(strlen(prpRotation->Gtd1) > 0)
					olBarText += olSlash + prpRotation->Gtd1;

				if(!olPstChangeText2.IsEmpty())
					olBarText += olSlash + olPstChangeText2;
				else if(strlen(prpRotation->Pstd) > 0)
					olBarText += olSlash + prpRotation->Pstd;
			}
		}
		else
		{
			if(prpRotation != NULL)
			{
				if(!olPstChangeText2.IsEmpty())
					olBarText += olPstChangeText2 + olSlash;
				else if(strlen(prpRotation->Psta) > 0)
					olBarText += prpRotation->Psta + olSlash;

				if(strlen(prpRotation->Gta1) > 0)
					olBarText += prpRotation->Gta1 + olSlash;

				olBarText += prpRotation->Fnum + olDash;
			}
			olBarText += prpFlight->Fnum;

			if(strlen(prpFlight->Gtd1) > 0)
				olBarText += olSlash + prpFlight->Gtd1;

			if(!olPstChangeText1.IsEmpty())
				olBarText += olSlash + olPstChangeText1;
			else if(strlen(prpFlight->Pstd) > 0)
				olBarText += olSlash + prpFlight->Pstd;
		}
	}

	return olBarText;
}

CString PstDiagramViewer::GetPositionText(FLIGHTDATA *prpFlight)
{
	CString olPstText = ogConflicts.GetAdditionalConflictText(CFI_POSITIONCHANGE,prpFlight);
	if(olPstText.IsEmpty()) olPstText = ogFlightData.GetPosition(prpFlight);
	return olPstText;
}

CString PstDiagramViewer::GetGateText(FLIGHTDATA *prpFlight)
{
	CString olGateText = ogConflicts.GetAdditionalConflictText(CFI_GATECHANGE,prpFlight);
	if(olGateText.IsEmpty()) olGateText = ogFlightData.GetGate(prpFlight);
	return olGateText;
}

CString PstDiagramViewer::GetStatusText(FLIGHTDATA *prpFlight, FLIGHTDATA *prpRotation)
{
	CString olText;
	bool blIsArrival = ogFlightData.IsArrival(prpFlight);
	FLIGHTDATA *prlArr = (blIsArrival) ? prpFlight : prpRotation;
	FLIGHTDATA *prlDep = (blIsArrival) ? prpRotation : prpFlight;
	if(prlArr && prlDep)
	{
		olText = ogFieldConfig.ConfigureField(IDS_FIELDCONFIG_PSTSTATUSTURN, "%s%s%s%s%s%s%s%s%d%s%s%s%s%s%s%d%s%s", 
												prlArr->Regn, prlArr->Act3, prlArr->Fnum, prlArr->Org3, prlArr->Stoa.Format("%H%M"), 
												prlArr->Etoa.Format("%H%M"), GetGateText(prlArr), GetPositionText(prlArr), prlArr->Pax1,
												prlDep->Fnum, prlDep->Des3, prlDep->Stod.Format("%H%M"), prlDep->Etod.Format("%H%M"), 
												GetGateText(prlDep), GetPositionText(prlDep), prlDep->Pax1,
												prlArr->Flti, prlDep->Flti);
	}
	else if(prlArr)
	{
		olText = ogFieldConfig.ConfigureField(IDS_FIELDCONFIG_PSTSTATUSARR, "%s%s%s%s%s%s%s%s%d%s", 
											    prlArr->Fnum, prlArr->Regn, prlArr->Act3, prlArr->Org3, prlArr->Stoa.Format("%H%M"), 
												prlArr->Etoa.Format("%H%M"), GetGateText(prlArr), GetPositionText(prlArr), prlArr->Pax1,prlArr->Flti);
	}
	else if(prlDep)
	{
		olText = ogFieldConfig.ConfigureField(IDS_FIELDCONFIG_PSTSTATUSDEP, "%s%s%s%s%s%s%s%s%d%s", 
												prlDep->Fnum, prlDep->Regn, prlDep->Act3, prlDep->Des3, prlDep->Stod.Format("%H%M"), 
												prlDep->Etod.Format("%H%M"), GetGateText(prlDep), GetPositionText(prlDep), prlDep->Pax1, prlDep->Flti);
	}


	return olText;
}

// a flight is set to finished when it is onblock/airborne and all jobs
// have finished and all unassigned demands are later than the current time
// check if there are unassigned position demands whose end time has now passed
// thus resulting in the flight bar colour being set to finished
void PstDiagramViewer::CheckForFinishedFlights()
{
	int ilNumGroups = omGroups.GetSize();
	for(int ilG = 0; ilG < ilNumGroups; ilG++)
	{
		int ilNumLines = omGroups[ilG].Lines.GetSize();
		for(int ilL = 0; ilL < ilNumLines; ilL++)
		{
			int ilNumBars = omGroups[ilG].Lines[ilL].Bars.GetSize();
			for(int ilB = 0; ilB < ilNumBars; ilB++)
			{
				PST_BARDATA *prlBar = &omGroups[ilG].Lines[ilL].Bars[ilB];
			    if(!prlBar->IsFinished && prlBar->EndTime < ogBasicData.GetTime())
				{
					ProcessFlightChange(ogFlightData.GetFlightByUrno(prlBar->FlightUrno));
				}
			}
		}
	}
}

void PstDiagramViewer::MakeShadowBar(int ipGroupno, int ipLineno, FLIGHTDATA *prpFlight)
{
	if (prpFlight->IsShadoBarDisplayed  == TRUE)
	{
		return;
	}

	prpFlight->IsShadoBarDisplayed = TRUE;
	bool blIsArrival = ogFlightData.IsArrival(prpFlight);// Inbound flight
	bool blIsDeparture = ogFlightData.IsDeparture(prpFlight);// Outbound flight
	CCSPtrArray<DEMANDDATA> olArrivalDemands;
	CTime olArrivalStartTime, olArrivalEndTime;
	CCSPtrArray<DEMANDDATA> olDepartureDemands;
	CTime olDepartureStartTime, olDepartureEndTime;
	FLIGHTDATA *prlRotationFlight = ogFlightData.GetRotationFlight(prpFlight);
	CString olBarText;


	// Read the range of time of demands associate with the given flight and its rotation.
	// Skip this flight if GetDemands() returns no demand or there is an invalid demand
	// time associated with this flight.
	//
	if (blIsArrival)
	{
		if (!bmIncludeArrivalFlights)
		{
			return;
		}
		if (GetDemands(prpFlight->Urno,ogFlightData.GetPosition(prpFlight),olArrivalDemands,
			olArrivalStartTime, olArrivalEndTime) == -1)
		{
			MakeShadowBarWithoutDemand(ipGroupno,ipLineno,prpFlight);
			return;
		}
		if (prlRotationFlight != NULL)
		{
			if (GetDemands(prlRotationFlight->Urno,ogFlightData.GetPosition(prlRotationFlight),olDepartureDemands,
				olDepartureStartTime, olDepartureEndTime) == -1)
				prlRotationFlight = NULL;
		}
	}
	else if (blIsDeparture)
	{
		if (!bmIncludeDepartureFlights)
		{
			return;
		}

		if (GetDemands(prpFlight->Urno,ogFlightData.GetPosition(prpFlight),olDepartureDemands,
			olDepartureStartTime, olDepartureEndTime) == -1)
		{
			MakeShadowBarWithoutDemand(ipGroupno,ipLineno,prpFlight);
			return;
		}
		if (prlRotationFlight != NULL)
		{
			if (GetDemands(prlRotationFlight->Urno,ogFlightData.GetPosition(prlRotationFlight), olArrivalDemands,
				olArrivalStartTime, olArrivalEndTime) == -1)
				prlRotationFlight = NULL;
		}
	}
	else	// Invalid data?
	{
		return;
	}

	// Check for a "turnaround" flight
	BOOL blIsTurnaround = ogFlightData.IsTurnaround(prpFlight);
	// Prepare non-common fields associated with this flight bar
	PST_BARDATA rlBar;
	rlBar.IsDelayed = CFST_NOTSET;

	if (bmIncludeArrivalFlights && blIsArrival && !blIsTurnaround)
	{
		olBarText = prpFlight->Fnum;
		rlBar.StatusText = ArrivalFlightStatus(prpFlight, prlRotationFlight);
		rlBar.StartTime = olArrivalStartTime;
		rlBar.EndTime = olArrivalEndTime;
		rlBar.IsTmo = prpFlight->IsTmo;
	}
	else if (bmIncludeDepartureFlights && blIsDeparture && !blIsTurnaround)
	{
		olBarText = prpFlight->Fnum;
		rlBar.StatusText = DepartureFlightStatus(prpFlight, prlRotationFlight);
		rlBar.StartTime = olDepartureStartTime;
		rlBar.EndTime = olDepartureEndTime;
		rlBar.IsTmo = FALSE;

	}
	else if (blIsTurnaround && !blIsDeparture)	// avoid duplicated turnaround
	{
		olBarText = CString(prpFlight->Fnum) + " -> " + prlRotationFlight->Fnum;
		rlBar.Text = CString(prpFlight->Fnum) + " -> " + prlRotationFlight->Fnum;
		rlBar.StatusText = TurnAroundFlightStatus(prpFlight, prlRotationFlight);
		rlBar.StartTime = min(olArrivalStartTime,olDepartureStartTime);
		rlBar.EndTime = olDepartureEndTime;
		rlBar.IsTmo = prpFlight->IsTmo;
	}
	else	// undisplayed data?
	{
		return;
	}

	if (blIsTurnaround)	// Delay only for Departure
	{
		if (prlRotationFlight->IsDelayed != CFST_NOTSET)
		{
			rlBar.Delay = prlRotationFlight->Etod - prlRotationFlight->Stod;
			rlBar.IsDelayed = prlRotationFlight->IsDelayed;
		}
	}
	else if (blIsDeparture)	// Delay only for Departure
	{
		if (prpFlight->IsDelayed != CFST_NOTSET)
		{
			rlBar.Delay = prpFlight->Etod - prpFlight->Stod;
			rlBar.IsDelayed = prpFlight->IsDelayed;
		}
	}
	else
	{
		rlBar.Delay = CTimeSpan(0);
	}

	int ilColorIndex = ogConflicts.GetFlightConflictColor(prpFlight, ALLOCUNITTYPE_PST, imDemandTypes);
	BOOL blIsPlanned;
	BOOL blIsInFuture;

	if (blIsTurnaround)
	{
		//ilColorIndex = prpFlight->ColorIndex;
		blIsPlanned = prpFlight->PstIsPlanned[0] || prlRotationFlight->PstIsPlanned[0];
		//blIsInFuture = ogFlightData.IsArrival(prpFlight) ? prpFlight->isInFuture : prlRotationFlight->isInFuture;
		blIsInFuture = ogFlightData.IsArrival(prpFlight) ? prpFlight->PstIsInFuture[0] : prlRotationFlight->PstIsInFuture[0];
	}
	else
	{
		//ilColorIndex = prpFlight->ColorIndex;
		blIsPlanned = prpFlight->PstIsPlanned[0];
		//blIsInFuture = prpFlight->isInFuture;
		blIsInFuture = prpFlight->PstIsInFuture[0];
	}

	rlBar.NoDropOnThisBar = TRUE;
	rlBar.MarkerType = (blIsPlanned)? MARKLEFT: (blIsInFuture)? MARKNONE: MARKFULL;
	rlBar.MarkerBrush = ogBrushs[ilColorIndex];
	rlBar.FlightUrno = prpFlight->Urno;
//	rlBar.DepartureUrno = blIsTurnaround ? prlRotationFlight->Urno : -1;
	rlBar.FrameType = FRAMERECT;
	rlBar.IsShadowBar = FALSE;
	rlBar.Type = BAR_PSTFLIGHT;

}



void PstDiagramViewer::MakeBarWithoutDemand(int ipGroupno, int ipLineno, FLIGHTDATA *prpFlight, int ipReturnFlightType /*ID_NOT_RETURNFLIGHT*/)
{
	if (ogBasicData.bmDemandsOnly)
		return;

	int ilTypeIdx = (ipReturnFlightType == ID_DEP_RETURNFLIGHT) ? 1 : 0;

	bool blIsArrival = (ogFlightData.IsArrival(prpFlight) || ipReturnFlightType == ID_ARR_RETURNFLIGHT);	 // Inbound flight
	bool blIsDeparture = (ogFlightData.IsDeparture(prpFlight) || ipReturnFlightType == ID_DEP_RETURNFLIGHT); // Outbound flight
//	CString olPstChangeText = ogConflicts.GetAdditionalConflictText(CFI_POSITIONCHANGE,prpFlight);
	bool blDisplayPosition = (ipGroupno == imAllFlightsGroup) ? true : false;

	FLIGHTDATA *prlRotationFlight = ogFlightData.GetRotationFlight(prpFlight, ipReturnFlightType);

	CString olBarText = GetBarText(prpFlight, blIsArrival, NULL);
	CString olStatusText = GetStatusText(prpFlight, NULL);
	PST_BARDATA rlBar;
	rlBar.IsDelayed = CFST_NOTSET;
	if (blIsArrival)
	{
		if (prpFlight->Stoa == TIMENULL)
			return;
//		if(blDisplayPosition && strlen(prpFlight->Psta) > 0)
//		{
//			olBarText = prpFlight->Psta + CString("/");
//		}
//		olBarText += CString(prpFlight->Fnum) + " " + olPstChangeText;
		rlBar.Text = olBarText;
		rlBar.StatusText = (olStatusText.IsEmpty()) ? olBarText + ArrivalFlightStatus(prpFlight, prlRotationFlight) : olStatusText;
		rlBar.StartTime = GetFlightArr(prpFlight, ipReturnFlightType);
		rlBar.EndTime = GetFlightDep(prpFlight, ipReturnFlightType);
		rlBar.Delay = CTimeSpan(0);
		rlBar.IsTmo = prpFlight->IsTmo;
		rlBar.FlightType = PSTBAR_ARRIVAL;
	}
	else if (blIsDeparture)
	{
		if (prpFlight->Stod == TIMENULL)
			return;
//		olBarText = CString(prpFlight->Fnum) + " " + olPstChangeText;
//		if(blDisplayPosition && strlen(prpFlight->Pstd) > 0)
//		{
//			olBarText += CString("/") + prpFlight->Pstd;
//		}
		rlBar.Text = olBarText;
		rlBar.StatusText = (olStatusText.IsEmpty()) ? olBarText + DepartureFlightStatus(prpFlight, prlRotationFlight) : olStatusText;
		rlBar.StartTime = GetFlightArr(prpFlight, ipReturnFlightType);
		rlBar.EndTime = GetFlightDep(prpFlight, ipReturnFlightType);
		if (prpFlight->IsDelayed != CFST_NOTSET)
		{
			rlBar.IsDelayed = prpFlight->IsDelayed;
			rlBar.Delay = prpFlight->Etod - prpFlight->Stod;
		}
		else
			rlBar.Delay = CTimeSpan(0);
		rlBar.IsTmo = FALSE;
		rlBar.FlightType = PSTBAR_DEPARTURE;
	}

	rlBar.MarkerType = prpFlight->PstIsPlanned[ilTypeIdx] ? MARKLEFT : prpFlight->PstIsInFuture[ilTypeIdx] ? MARKNONE : MARKFULL;

	rlBar.FlightUrno = prpFlight->Urno;
//	rlBar.DepartureUrno = prpFlight->Rkey;
	rlBar.FrameType = FRAMERECT;
	void *p;
	if(omDelegatedFlights.Lookup((void *)rlBar.FlightUrno,(void *& )p) == TRUE)
		rlBar.FrameColor = ORANGE;
	rlBar.IsShadowBar = FALSE;
	rlBar.Type = BAR_PSTFLIGHT;
	rlBar.NoDropOnThisBar = FALSE;
	rlBar.MarkerBrush = ogBrushs[ogConflicts.GetFlightConflictColor(prpFlight, ALLOCUNITTYPE_PST, imDemandTypes, ipReturnFlightType, bmDisplayJobConflicts)];
	rlBar.ReturnFlightType = ipReturnFlightType;
	int ilBarno = CreateBar(ipGroupno, ipLineno, &rlBar);

	// Create indicators associated with this flight
	// not here, no demand, no indicator
}


void PstDiagramViewer::MakeShadowBarWithoutDemand(int ipGroupno, int ipLineno, FLIGHTDATA *prpFlight)
{
	if (ogBasicData.bmDemandsOnly)
		return;
	FLIGHTDATA *prlRotationFlight = ogFlightData.GetRotationFlight(prpFlight);

	bool blIsArrival = ogFlightData.IsArrival(prpFlight);		// Inbound flight
	bool blIsDeparture = ogFlightData.IsDeparture(prpFlight);	// Outbound flight

	PST_BARDATA rlBar;
	rlBar.IsDelayed = CFST_NOTSET;
	if (blIsArrival)
	{
		if (prpFlight->Stoa == TIMENULL)
			return;
		rlBar.Text = prpFlight->Fnum;
		rlBar.StatusText = ArrivalFlightStatus(prpFlight, prlRotationFlight);
		rlBar.StartTime = GetFlightArr(prpFlight);
		rlBar.EndTime = GetFlightDep(prpFlight);
		rlBar.Delay = CTimeSpan(0);
		rlBar.IsTmo = prpFlight->IsTmo;
	}
	else if (blIsDeparture)
	{
		if (prpFlight->Stod == TIMENULL)
			return;
		rlBar.Text = prpFlight->Fnum;
		rlBar.StatusText = DepartureFlightStatus(prpFlight, prlRotationFlight);
		rlBar.StartTime = GetFlightArr(prpFlight);
		rlBar.EndTime = GetFlightDep(prpFlight);
		if (prpFlight->IsDelayed != CFST_NOTSET)
		{
			rlBar.IsDelayed = prpFlight->IsDelayed;
			rlBar.Delay = prpFlight->Etod - prpFlight->Stod;
		}
		else
			rlBar.Delay = CTimeSpan(0);
		rlBar.IsTmo = FALSE;
	}
	rlBar.MarkerType = MARKFULL;
	rlBar.MarkerBrush = ogBrushs[6];

	rlBar.FlightUrno = prpFlight->Urno;
//	rlBar.DepartureUrno = prpFlight->Rkey;
	rlBar.FrameType = FRAMERECT;
	rlBar.NoDropOnThisBar = TRUE;
	rlBar.IsShadowBar = TRUE;
	rlBar.Type = BAR_PSTFLIGHT;
	int ilBarno = CreateBar(ipGroupno, ipLineno, &rlBar);

	// Create indicators associated with this flight
	// not here, no demand, no indicator
}



void PstDiagramViewer::MakeIndicators(int ipGroupno, int ipLineno, int ipBarno, 
	CCSPtrArray<DEMANDDATA> &ropArrivalDemands,
	CCSPtrArray<DEMANDDATA> &ropDepartureDemands,
	FLIGHTDATA *prpArrivalFlight,FLIGHTDATA *prpDepartureFlight)
{
	int i;
	long llJobu;
	CCSPtrArray<JOBDATA> olJobs;
	CCSPtrArray<DEMANDDATA> olDemands;

	for (i = 0; i < ropArrivalDemands.GetSize(); i++)	// for each arrival flight?
	{
//		if (strcmp(ogFlightData.GetPosition(prpArrivalFlight),ropArrivalDemands[i].Alid) == 0)
		{
			DEMANDDATA* prlDemand;
			prlDemand = &ropArrivalDemands[i];
			if(CheckDemandValidity(prlDemand) == FALSE)
			{
				continue;
			}

			PST_INDICATORDATA rlIndicator;
			rlIndicator.StartTime = ropArrivalDemands[i].Debe;
			rlIndicator.EndTime = ropArrivalDemands[i].Deen;
			rlIndicator.IsArrival = TRUE;
			// MCU 05.07 we need the Urno of the demand for the job/demand connection
			rlIndicator.DemandUrno = ropArrivalDemands[i].Urno;
			rlIndicator.Color = 0;
			// MCU 07.07 the Indicator Color depends on the Color of the job
			// assigned to this demand, if no job is assigned, the color comes from
			// conflict CFI_NOTCOVERED
			if ((llJobu = ogJodData.GetFirstJobUrnoByDemand(ropArrivalDemands[i].Urno)) != 0)
			{
				JOBDATA *prlJob = ogJobData.GetJobByUrno(llJobu);
				if (prlJob != NULL)
				{
					//rlIndicator.Color = ogColors[prlJob->ColorIndex];
					rlIndicator.Color = ogColors[ogConflicts.GetJobConflictColor(prlJob)];
					rlIndicator.StartTime = prlJob->Acfr;
					rlIndicator.EndTime = prlJob->Acto;
				}
			}
			if (rlIndicator.Color == 0)
			{
				if(ogDemandData.DemandIsDeactivated(&ropArrivalDemands[i]))
				{
					rlIndicator.Color = ogColors[FIRSTCONFLICTCOLOR+(CFI_NOCONFLICT*2)];
				}
				else
				{
					rlIndicator.Color = ogColors[FIRSTCONFLICTCOLOR+(CFI_PST_NOTCOVERED*2)];	// just use simply red for now
				}
			}
			CreateIndicator(ipGroupno, ipLineno, ipBarno, &rlIndicator);
		}
	}
	if (prpArrivalFlight != NULL)
	{
		// now we create indicators for jobs without demand for this flight
		olJobs.RemoveAll();
		ogJobData.GetJobsByFlur(olJobs,prpArrivalFlight->Urno,FALSE,"",ALLOCUNITTYPE_PST); // don't like JOBFM
		for ( i = 0; i < olJobs.GetSize(); i++)
		{
			JOBDATA *prlJob = &olJobs[i];
			if(ogJodData.JobHasDemands(prlJob))
			{
				PST_INDICATORDATA rlIndicator;
				rlIndicator.StartTime = prlJob->Acfr;
				rlIndicator.EndTime = prlJob->Acto;
				rlIndicator.IsArrival = TRUE;
				rlIndicator.DemandUrno = 0L; // no demand
				//rlIndicator.Color = ogColors[prlJob->ColorIndex];
				rlIndicator.Color = ogColors[ogConflicts.GetJobConflictColor(prlJob)];
				CreateIndicator(ipGroupno, ipLineno, ipBarno, &rlIndicator);
			}
		}
	}
	for (i = 0; i < ropDepartureDemands.GetSize(); i++)	// for each departure flight?
	{
		CString olTest(ropDepartureDemands[i].Alid);
//		if (strcmp(ogFlightData.GetPosition(prpDepartureFlight),ropDepartureDemands[i].Alid) == 0)
		{
			DEMANDDATA* prlDemand;
			prlDemand = &ropDepartureDemands[i];
			if(CheckDemandValidity(prlDemand) == FALSE)
			{
				continue;
			}

			PST_INDICATORDATA rlIndicator;
			rlIndicator.StartTime = ropDepartureDemands[i].Debe;
			rlIndicator.EndTime = ropDepartureDemands[i].Deen;
			rlIndicator.IsArrival = FALSE;
			// MCU 05.07 we need the Urno of the demand for the job/demand connection
			rlIndicator.DemandUrno = ropDepartureDemands[i].Urno;
			rlIndicator.Color = 0;
			// MCU 07.07 the Indicator Color depends on the Color of the job
			// assigned to this demand, if no job is assigned, the color comes from
			// conflict CFI_NOTCOVERED
			if ((llJobu = ogJodData.GetFirstJobUrnoByDemand(ropDepartureDemands[i].Urno)) != 0)
			{
				JOBDATA *prlJob = ogJobData.GetJobByUrno(llJobu);
				if (prlJob != NULL)
				{
					//rlIndicator.Color = ogColors[prlJob->ColorIndex];
					rlIndicator.Color = ogColors[ogConflicts.GetJobConflictColor(prlJob)];
					rlIndicator.StartTime = prlJob->Acfr;
					rlIndicator.EndTime = prlJob->Acto;
				}
			}
			if (rlIndicator.Color == 0)
			{
				if(ogDemandData.DemandIsDeactivated(&ropDepartureDemands[i]))
				{
					rlIndicator.Color = ogColors[FIRSTCONFLICTCOLOR+(CFI_NOCONFLICT*2)];
				}
				else
				{
					rlIndicator.Color = ogColors[FIRSTCONFLICTCOLOR+(CFI_PST_NOTCOVERED*2)];	// just use simply red for now
				}
			}
			CreateIndicator(ipGroupno, ipLineno, ipBarno, &rlIndicator);
		}
	}
	if (prpDepartureFlight != NULL)
	{
		// now we create indicators for jobs without demand for this flight
		olJobs.RemoveAll();
		ogJobData.GetJobsByFlur(olJobs,prpDepartureFlight->Urno,FALSE,"",ALLOCUNITTYPE_PST); // don't like JOBFM
		for ( i = 0; i < olJobs.GetSize(); i++)
		{
			JOBDATA *prlJob = &olJobs[i];
			if(ogJodData.JobHasDemands(prlJob))
			{
				PST_INDICATORDATA rlIndicator;
				rlIndicator.StartTime = prlJob->Acfr;
				rlIndicator.EndTime = prlJob->Acto;
				rlIndicator.IsArrival = TRUE;
				rlIndicator.DemandUrno = 0L; // no demand
				//rlIndicator.Color = ogColors[prlJob->ColorIndex];
				rlIndicator.Color = ogColors[ogConflicts.GetJobConflictColor(prlJob)];
				CreateIndicator(ipGroupno, ipLineno, ipBarno, &rlIndicator);
			}
		}
	}
}

void PstDiagramViewer::MakeSpecialBar(int ipGroupno, int ipLineno, JOBDATA *prpJob)
{
	PST_BARDATA rlBar;
	EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prpJob->Peno);

	if (prlEmp != NULL)
	{
		CString olEmpNam = ogEmpData.GetEmpName(prlEmp);
		rlBar.Text = olEmpNam;
		rlBar.StatusText = prpJob->Acfr.Format("%H%M") + "-" + prpJob->Acto.Format("%H%M")
			+ olEmpNam; 
	}
	SHIFTDATA *prlShift;
	JOBDATA *prlDutyJob = ogJobData.GetJobByUrno(prpJob->Jour);
	if (prlDutyJob != NULL)
	{
		prlShift = ogShiftData.GetShiftByUrno(prlDutyJob->Shur);
		if (prlShift != NULL)
		{
			rlBar.StatusText += CString(prlShift->Sfca) + " " + prlShift->Egrp;
		}
	}
	rlBar.StartTime = prpJob->Acfr;
	rlBar.EndTime = prpJob->Acto;
	rlBar.MarkerType = (prpJob->Stat[0] == 'P')? MARKLEFT: MARKFULL;
	//rlBar.MarkerBrush = ogBrushs[prpJob->ColorIndex];
	rlBar.MarkerBrush = ogBrushs[ogConflicts.GetJobConflictColor(prpJob)];
	rlBar.FlightUrno = prpJob->Urno;
//	rlBar.DepartureUrno = -1;
	rlBar.FrameType = FRAMERECT;
	rlBar.IsShadowBar = FALSE;
 	rlBar.Type = BAR_PSTSPECIAL;
	rlBar.IsTmo = FALSE;
	rlBar.IsDelayed = FALSE;
	rlBar.Delay = CTimeSpan(0);

	int ilBarno = CreateBar(ipGroupno, ipLineno, &rlBar);

	// no Indicators
}

int PstDiagramViewer::GetDemands(long lpFlightUrno, const char *pcpAlid,
								  CCSPtrArray<DEMANDDATA> &ropDemands,
								  CTime &rolStartTime, CTime &rolEndTime)
{
	rolStartTime = TIMENULL;	// set default values
	rolEndTime = TIMENULL;

	ogDemandData.GetDemandsByFlur(ropDemands,lpFlightUrno,pcpAlid,ALLOCUNITTYPE_PST,imDemandTypes);
	int ilDemandCount = ropDemands.GetSize();
	if (ilDemandCount == 0)	// no demand?
		return -1;

	DEMANDDATA *prlDemand = NULL;
	for (int i = 0; i < ilDemandCount; i++)		// scan for the earliest and latest shift
	{
		prlDemand = &ropDemands[i];
		if(CheckDemandValidity(prlDemand) == FALSE) //PRF 8999
		{
			continue;
		}
		if (rolStartTime == TIMENULL || rolStartTime > (ropDemands[i].Debe))
			rolStartTime = ropDemands[i].Debe;
		if (rolEndTime == TIMENULL || rolEndTime < (ropDemands[i].Deen))
			rolEndTime = ropDemands[i].Deen;
	}

	if (rolStartTime == TIMENULL || rolEndTime == TIMENULL)
		return -1;
	return 0;	// success
}

CString PstDiagramViewer::ArrivalFlightStatus(FLIGHTDATA *prpFlight,FLIGHTDATA *prpRotationFlight)
{
	CString olText;
	if(prpFlight != NULL)
	{
		CString olEtoa = prpFlight->Etoa != TIMENULL ? CString("ETA ") + prpFlight->Etoa.Format("%H%M") : "";
		CString olOnbl = prpFlight->Onbl != TIMENULL ? CString("ONBL ") + prpFlight->Onbl.Format("%H%M") : "";
		olText.Format("   %s/%s  %s  STA %s %s %s   P:%s G:%s   PAX:%d",
			prpFlight->Regn, prpFlight->Act3,prpFlight->Org3,
			prpFlight->Stoa.Format("%H%M"),olEtoa,olOnbl,
			ogFlightData.GetPosition(prpFlight), ogFlightData.GetGate(prpFlight), prpFlight->Pax1);
	}
	
	if(prpRotationFlight != NULL)
	{
		CString olEtod = prpRotationFlight->Etod != TIMENULL ? CString("ETD ") + prpRotationFlight->Etod.Format("%H%M") : "";
		CString olOfbl = prpRotationFlight->Ofbl != TIMENULL ? CString("OFBL ") + prpRotationFlight->Ofbl.Format("%H%M") : "";
		CString olRotText;
		olRotText.Format("   (%s  STD %s %s %s  P:%s G:%s)",
			prpRotationFlight->Fnum,prpRotationFlight->Stod.Format("%H%M"),olEtod,olOfbl,
			ogFlightData.GetPosition(prpRotationFlight), ogFlightData.GetGate(prpRotationFlight));
		olText += olRotText;
	}

	return olText;
}

CString PstDiagramViewer::DepartureFlightStatus(FLIGHTDATA *prpFlight,FLIGHTDATA *prpRotationFlight)
{
	CString olText;
	if(prpFlight != NULL)
	{
		CString olEtod = prpFlight->Etod != TIMENULL ? CString("ETD ") + prpFlight->Etod.Format("%H%M") : "";
		CString olOfbl = prpFlight->Ofbl != TIMENULL ? CString("OFBL ") + prpFlight->Ofbl.Format("%H%M") : "";
		olText.Format("   %s/%s  %s %s  STD %s %s %s   P:%s  G:%s   PAX:%s",
			prpFlight->Regn, prpFlight->Act3,
			prpFlight->Des3, prpFlight->Via3,
			prpFlight->Stod.Format("%H%M"),olEtod,olOfbl,
			ogFlightData.GetPosition(prpFlight), ogFlightData.GetGate(prpFlight),
			ogPaxData.GetPaxString(prpFlight->Urno));
	}
	if(prpRotationFlight != NULL)
	{
		CString olEtoa = prpRotationFlight->Etoa != TIMENULL ? CString("ETA ") + prpRotationFlight->Etoa.Format("%H%M") : "";
		CString olOnbl = prpRotationFlight->Onbl != TIMENULL ? CString("ONBL ") + prpRotationFlight->Onbl.Format("%H%M") : "";
		CString olRotText;
		olRotText.Format("   (%s  STA %s %s %s  P:%s G:%s)",
			prpRotationFlight->Fnum,prpRotationFlight->Stoa.Format("%H%M"),olEtoa,olOnbl,
			ogFlightData.GetPosition(prpRotationFlight), ogFlightData.GetGate(prpRotationFlight));
		olText += olRotText;
	}

	return olText;
}

CString PstDiagramViewer::TurnAroundFlightStatus(FLIGHTDATA *prpFlight,	FLIGHTDATA *prpRotationFlight)
{
	CString olStatus;
	FLIGHTDATA *prlArr, *prlDep;
	if(ogFlightData.IsArrival(prpFlight))
	{
		prlArr = prpFlight;
		prlDep = prpRotationFlight;
	}
	else
	{
		prlArr = prpRotationFlight;
		prlDep = prpFlight;
	}
	olStatus.Format("%s  -->  %s", ArrivalFlightStatus(prlArr,NULL), DepartureFlightStatus(prlDep,NULL));

	if (prlArr->IsTmo)
	{
		CString olTmo = CString("   TMO --> ") + prlArr->Tmoa.Format("%H%M");
		olStatus += olTmo;
	}

	return olStatus;
}

BOOL PstDiagramViewer::FindGroup(const char *pcpPstAreaId, int &ripGroupno)
{
	for (ripGroupno = 0; ripGroupno < GetGroupCount(); ripGroupno++)
		if (GetGroup(ripGroupno)->PstAreaId == pcpPstAreaId)
			return TRUE;

	return FALSE;
}

// this new version of FindGroupAndLine allows iteration because it is now
// possible to have a flight displayed more than once ie if a position appears
// in more than one group eg "A01-A03" and "A01-A10". For this function to
// iterate properly, ripGroupno and ropLineno need to be initialized to -1
// - subsequent calls will then increment ripLineno.
BOOL PstDiagramViewer::FindGroupAndLine(const char *pcpPstId, int &ripGroupno, int &ripLineno)
{
	if(ripGroupno == -1)
		ripGroupno = 0;
	if(ripLineno == -1)
		ripLineno = 0;
	else
		ripLineno++;

	int ilNumGroups = GetGroupCount();
	for(; ripGroupno < ilNumGroups; ripGroupno++)
	{
		int ilNumLines = GetLineCount(ripGroupno);
		for(; ripLineno < ilNumLines; ripLineno++)
		{
			CString olLineText = GetLineText( ripGroupno, ripLineno);
			if(olLineText  == pcpPstId)
			{
				return TRUE;
			}
		}
		ripLineno = 0;
	}

    return FALSE;   // there is no such flight
}

BOOL PstDiagramViewer::FindLine(const char *pcpPstId, int ipGroupno, int &ripLineno)
{
	int ilNumLines = GetLineCount(ipGroupno);
	for(ripLineno = 0; ripLineno < ilNumLines; ripLineno++)
	{
		if(GetLineText(ipGroupno, ripLineno) == pcpPstId)
		{
			return TRUE;
		}
	}
	ripLineno = 0;
    return FALSE;
}




//BOOL PstDiagramViewer::FindGroupAndLine(const char *pcpPstId, int &ripGroupno, int &ripLineno)
//{
//	for (ripGroupno = 0; ripGroupno < GetGroupCount(); ripGroupno++)
//		for (ripLineno = 0; ripLineno < GetLineCount(ripGroupno); ripLineno++)
//               if ( GetLineText( ripGroupno, ripLineno) == pcpPstId)
//                    return TRUE;
//
//    return FALSE;   // there is no such flight
//}

BOOL PstDiagramViewer::FindManager(long lpUrno, int &ripGroupno, int &ripManagerno)
{
	for (ripGroupno = 0; ripGroupno < GetGroupCount(); ripGroupno++)
		for (ripManagerno = 0; ripManagerno < GetManagerCount(ripGroupno); ripManagerno++)
			if (GetManager(ripGroupno, ripManagerno)->JobUrno == lpUrno)
				return TRUE;

	return FALSE;
}

BOOL PstDiagramViewer::FindFlightBar(long lpUrno, int &ripGroupno, int &ripLineno, int &ripBarno, bool bpFindRotation /* = false */, int ipReturnFlightType /* = ID_NOT_RETURNFLIGHT*/)
{
	PST_BARDATA *prlBar;
	for (ripGroupno = 0; ripGroupno < GetGroupCount(); ripGroupno++)
	{
		for (ripLineno = 0; ripLineno < GetLineCount(ripGroupno); ripLineno++)
		{
            for (ripBarno = 0; ripBarno < GetBarCount(ripGroupno, ripLineno); ripBarno++)
			{
				prlBar = GetBar(ripGroupno, ripLineno, ripBarno);
				if(ipReturnFlightType == ID_NOT_RETURNFLIGHT)
				{
					if(prlBar->FlightUrno == lpUrno || (bpFindRotation && prlBar->DepartureUrno == lpUrno))
						return TRUE;
				}
				else
				{
					if((prlBar->FlightUrno == lpUrno && prlBar->ReturnFlightType == ipReturnFlightType) || 
						(bpFindRotation && prlBar->DepartureUrno == lpUrno && prlBar->DepReturnFlightType == ipReturnFlightType))
						return TRUE;
				}
			}
		}
	}

    return FALSE;   // there is no such flight
}


/////////////////////////////////////////////////////////////////////////////
// PstDiagramViewer -- Viewer basic operations

int PstDiagramViewer::GetGroupCount()
{
    return omGroups.GetSize();
}

PST_GROUPDATA *PstDiagramViewer::GetGroup(int ipGroupno)
{
    return &omGroups[ipGroupno];
}

CString PstDiagramViewer::GetGroupText(int ipGroupno)
{
    return omGroups[ipGroupno].Text;
}

CString PstDiagramViewer::GetGroupTopScaleText(int ipGroupno)
{
	CString s;
	for (int i = 0; i < GetManagerCount(ipGroupno); i++)
	{
		PST_MANAGERDATA *prlManager = GetManager(ipGroupno, i);
		s += s.IsEmpty()? "": " / ";
		s += CString(prlManager->EmpLnam) + " " + prlManager->ShiftTmid + " " + prlManager->ShiftStid;
	}
	return s;
}

int PstDiagramViewer::GetGroupColorIndex(int ipGroupno,CTime opStart,CTime opEnd)
{
	int ilColorIndex = FIRSTCONFLICTCOLOR+(CFI_NOCONFLICT*2);
	CBrush *prlBrush = ogBrushs[ilColorIndex];
	int ilLineCount = GetLineCount(ipGroupno);
	int ilColour, ilType;
	bool blConfirmed;
	for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
	{
		for (int ilBarno = 0; ilBarno < GetBarCount(ipGroupno, ilLineno); ilBarno++)
		{
			PST_BARDATA *prlBar;
			if ((prlBar = GetBar(ipGroupno, ilLineno, ilBarno)) != NULL)
			{
				if (IsOverlapped(opStart, opEnd, prlBar->StartTime, prlBar->EndTime))
				{
					FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prlBar->FlightUrno);
					if (prlFlight != NULL)
					{
						ogConflicts.GetFlightConflict(prlFlight, ilColour, ilType, blConfirmed, ALLOCUNITTYPE_PST, PERSONNELDEMANDS);
						if(ilType != CFI_NOCONFLICT && !blConfirmed)
						{
							// valid conflict found
							return FIRSTCONFLICTCOLOR+(CFI_PST_NOTCOVERED*2);
						}
					}
				}
			}
		}
	}
	return ilColorIndex;
}

int PstDiagramViewer::GetManagerCount(int ipGroupno)
{
    return omGroups[ipGroupno].Managers.GetSize();
}

PST_MANAGERDATA *PstDiagramViewer::GetManager(int ipGroupno, int ipManagerno)
{
    return &omGroups[ipGroupno].Managers[ipManagerno];
}

int PstDiagramViewer::GetLineCount(int ipGroupno)
{
    return omGroups[ipGroupno].Lines.GetSize();
}

PST_LINEDATA *PstDiagramViewer::GetLine(int ipGroupno, int ipLineno)
{
    return &omGroups[ipGroupno].Lines[ipLineno];
}

CString PstDiagramViewer::GetLineText(int ipGroupno, int ipLineno)
{
	if(ipLineno < omGroups[ipGroupno].Lines.GetSize())
		return omGroups[ipGroupno].Lines[ipLineno].Text;
	else
		return "";
}

int PstDiagramViewer::GetMaxOverlapLevel(int ipGroupno, int ipLineno)
{
    return omGroups[ipGroupno].Lines[ipLineno].MaxOverlapLevel;
}

int PstDiagramViewer::GetVisualMaxOverlapLevel(int ipGroupno, int ipLineno)
{
	// For max level 0,1,2 we will use 2 levels, 3,4,5 we will use 5 levels, 6,7,8 we use 8 levels, and so on ...
	return GetMaxOverlapLevel(ipGroupno, ipLineno) / 3 * 3 + 2;
}

int PstDiagramViewer::GetBarCount(int ipGroupno, int ipLineno)
{
    return omGroups[ipGroupno].Lines[ipLineno].Bars.GetSize();
}

PST_BARDATA *PstDiagramViewer::GetBar(int ipGroupno, int ipLineno, int ipBarno)
{
    return &omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno];
}

CString PstDiagramViewer::GetBarText(int ipGroupno, int ipLineno, int ipBarno)
{
    return omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Text;
}

int PstDiagramViewer::GetIndicatorCount(int ipGroupno, int ipLineno, int ipBarno)
{
    return omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators.GetSize();
}

PST_INDICATORDATA *PstDiagramViewer::GetIndicator(int ipGroupno, int ipLineno, int ipBarno, int ipIndicatorno)
{
    return &omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators[ipIndicatorno];
}

CString PstDiagramViewer::GetTmo(int ipGroupno, int ipLineno)
{
	CString olTmoText = CString("");
    for (int ilBarno = 0; ilBarno < GetBarCount(ipGroupno, ipLineno); ilBarno++)
	{
		PST_BARDATA *prlBar;
        if ((prlBar = GetBar(ipGroupno, ipLineno, ilBarno)) != NULL)
		{
			if (prlBar->IsTmo)
			{
				if (olTmoText.IsEmpty())
					olTmoText = CString(" TMO ");
				FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prlBar->FlightUrno);
				if (prlFlight != NULL)
				{
					if (ogFlightData.IsDeparture(prlFlight))
					{
						prlFlight = ogFlightData.GetRotationFlight(prlFlight);
					}
					if (prlFlight != NULL)
					{
						olTmoText += CString(prlFlight->Fnum) + " ";
					}
				}
			}
		}
	}
    return olTmoText;   
}

/////////////////////////////////////////////////////////////////////////////
// PstDiagramViewer - PST_BARDATA array maintenance (overlapping version)

void PstDiagramViewer::DeleteAll()
{
    while (GetGroupCount() > 0)
        DeleteGroup(0);
}

int PstDiagramViewer::CreateGroup(PST_GROUPDATA *prpGroup)
{
    int ilGroupCount = omGroups.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
		if (CompareGroup(prpGroup, GetGroup(ilGroupno)) <= 0)
            break;  // should be inserted before Groups[ilGroupno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilGroupno = ilGroupCount; ilGroupno > 0; ilGroupno--)
		if (CompareGroup(prpGroup, GetGroup(ilGroupno-1)) >= 0)
            break;  // should be inserted after Groups[ilGroupno-1]
#endif
    
    omGroups.NewAt(ilGroupno, *prpGroup);
    return ilGroupno;
}

void PstDiagramViewer::DeleteGroup(int ipGroupno)
{
    while (GetLineCount(ipGroupno) > 0)
        DeleteLine(ipGroupno, 0);
    while (GetManagerCount(ipGroupno) > 0)
        DeleteManager(ipGroupno, 0);

    omGroups.DeleteAt(ipGroupno);
}

int PstDiagramViewer::CreateManager(int ipGroupno, PST_MANAGERDATA *prpManager)
{
    int ilManagerCount = omGroups[ipGroupno].Managers.GetSize();
// first we check if this manager already exist
	for ( int ilLc = 0; ilLc < ilManagerCount; ilLc++)
	{
		if (omGroups[ipGroupno].Managers[ilLc].JobUrno == prpManager->JobUrno)
			return ilLc;
	}

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilManagerno = 0; ilManagerno < ilManagerCount; ilManagerno++)
		if (CompareManager(prpManager, GetManager(ipGroupno, ilManagerno)) <= 0)
            break;  // should be inserted before Lines[ilManagerno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilManagerno = ilManagerCount; ilManagerno > 0; ilManagerno--)
		if (CompareManager(prpManager, GetManager(ipGroupno, ilManagerno-1)) >= 0)
            break;  // should be inserted after Lines[ilManagerno-1]
#endif

    omGroups[ipGroupno].Managers.NewAt(ilManagerno, *prpManager);
    PST_MANAGERDATA *prlManager = &omGroups[ipGroupno].Managers[ilManagerno];
    return ilManagerno;
}

void PstDiagramViewer::DeleteManager(int ipGroupno, int ipManagerno)
{
    omGroups[ipGroupno].Managers.DeleteAt(ipManagerno);
}

int PstDiagramViewer::CreateLine(int ipGroupno, PST_LINEDATA *prpLine)
{
    int ilLineCount = omGroups[ipGroupno].Lines.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
		if (CompareLine(prpLine, GetLine(ipGroupno, ilLineno)) <= 0)
            break;  // should be inserted before Lines[ilLineno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
		if (CompareLine(prpLine, GetLine(ipGroupno, ilLineno-1)) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]
#endif

    omGroups[ipGroupno].Lines.NewAt(ilLineno, *prpLine);
    PST_LINEDATA *prlLine = &omGroups[ipGroupno].Lines[ilLineno];
    prlLine->MaxOverlapLevel = 0;   // there is no bar in this line right now
    return ilLineno;
}

void PstDiagramViewer::DeleteLine(int ipGroupno, int ipLineno)
{
	// Id 14-Sep-1996
	// Don't simply call DeleteBar() for sake of efficiency
	PST_LINEDATA *prlLine = &omGroups[ipGroupno].Lines[ipLineno];
	while (prlLine->Bars.GetSize() > 0)
	{
		prlLine->Bars[0].Indicators.DeleteAll();	// delete associated indicators first
		prlLine->Bars.DeleteAt(0);
	}

    omGroups[ipGroupno].Lines.DeleteAt(ipLineno);
}

// When Advise time is set the start of the bar is set to TIFA and the end to STOA+20mins this can result
// in a negative duration which in turn results in multiple bars being displayed on one line in the "All Flights" group
// check for negative duration and reset the bar time if necessary
void PstDiagramViewer::CheckBarDuration(PST_BARDATA &ropBar)
{
	CTimeSpan olBarLength = ropBar.EndTime - ropBar.StartTime;
	if(olBarLength.GetTotalMinutes() <= 0)
	{
		if(ropBar.FlightType != PSTBAR_DEPARTURE)
			ropBar.EndTime = ropBar.StartTime + CTimeSpan(0,0,20,0);
		else
			ropBar.StartTime = ropBar.EndTime - CTimeSpan(0,1,0,0);
	}
}

int PstDiagramViewer::CreateBar(int ipGroupno, int ipLineno, PST_BARDATA *prpBar, BOOL bpTopBar)
{
	CheckBarDuration(*prpBar);

    PST_LINEDATA *prlLine = GetLine(ipGroupno, ipLineno);
    int ilBarCount = prlLine->Bars.GetSize();

	// Id 14-Sep-1996
	// I use a new algorithm for bar insertion and new deletion.
	// This new algorithm will work somewhat slower than the previous version, but safer.
	// To give you a brief idea of this algorithm, here is the essential of insertion:
	//
	// Case A: Insert a new bar to be the topmost bar
	//		We will scan every bars, and for each bar which is in the same overlap group
	//		with the new bar, we will shift it downward one level, and update the field
	//		Line.MaxOverlapLevel. This new bar will be the last bar in painting order
	//		(for being topmost), and has overlapping level = 0.
	//
	// Case B: Insert a new bar to be the bottommost bar
	//		We will scan every bars for finding the maximum overlap level of bars which
	//		are in the same overlap group with this new bar (use level -1 if not found).
	//		Then use this maximum overlap level + 1 as overlap level of this new bar.
	//		This new bar will be the first bar in painting order (for being bottommost).
	//
	CUIntArray olBarnoList;
	GetOverlappedBarsFromTime(ipGroupno, ipLineno,
		prpBar->StartTime, prpBar->EndTime, olBarnoList);
	int ilListCount = olBarnoList.GetSize();

	if (bpTopBar)
	{
		for (int ilLc = 0; ilLc < ilListCount; ilLc++)
		{
			int ilBarno = olBarnoList[ilLc];
			PST_BARDATA *prlBar = &prlLine->Bars[ilBarno];
			int ilNewOverlapLevel = ++prlBar->OverlapLevel;
			prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, ilNewOverlapLevel);
		}
		prlLine->Bars.NewAt(ilBarCount, *prpBar);	// to be the last bar in painting order
		prlLine->Bars[ilBarCount].OverlapLevel = 0;
		return ilBarCount;
	}
	else
	{
		int ilNewBarLevel = -1;
		for (int ilLc = 0; ilLc < ilListCount; ilLc++)
		{
			int ilBarno = olBarnoList[ilLc];
			PST_BARDATA *prlBar = &prlLine->Bars[ilBarno];
			ilNewBarLevel = max(ilNewBarLevel, prlBar->OverlapLevel + 1);
				// the +1 means that we have to insert the new bar below this bar
		}
		prlLine->Bars.NewAt(0, *prpBar);	// to be the first bar in painting order
		prlLine->Bars[0].OverlapLevel = ilNewBarLevel;
		prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, ilNewBarLevel);
		return 0;
	}
}

void PstDiagramViewer::DeleteBarsForFlight(long lpFlightUrno)
{
	int ilGroupno, ilLineno, ilBarno;
	while (FindFlightBar(lpFlightUrno, ilGroupno, ilLineno, ilBarno))
	{
		PST_BARDATA *prlBar;
		if((prlBar = GetBar(ilGroupno, ilLineno, ilBarno)) != NULL)
		{
			int ilOldMaxOverlapLevel = GetVisualMaxOverlapLevel(ilGroupno, ilLineno);
			DeleteBar(ilGroupno, ilLineno, ilBarno);
			if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
			{
				LONG lParam = MAKELONG(ilLineno, ilGroupno);
				if (GetVisualMaxOverlapLevel(ilGroupno, ilLineno) == ilOldMaxOverlapLevel)
					pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
				else
					pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
			}
		}
	}
}


void PstDiagramViewer::DeleteBar(int ipGroupno, int ipLineno, int ipBarno)
{
    PST_LINEDATA *prlLine = GetLine(ipGroupno, ipLineno);
	PST_BARDATA *prlBar = &prlLine->Bars[ipBarno];

	// Delete all indicators of this bar
	while (GetIndicatorCount(ipGroupno, ipLineno, ipBarno) > 0)
		DeleteIndicator(ipGroupno, ipLineno, ipBarno, 0);

	// Id 14-Sep-1996
	// I use a new algorithm for bar insertion and new deletion.
	// This new algorithm will work somewhat slower than the previous version, but safer.
	// To give you a brief idea of this algorithm, here is the essential of deletion:
	//
	// We will scan every bars, and for each bar which is in the same overlap group with
	// the new bar, we will back up them to a temporary array. Then we remove every bars
	// in this overlapping group, and insert these bar from the temporary array except
	// the one which has to be deleted back to the viewer.
	//
	// In some situation, if this overlapping group controls the maximum overlap level of
	// the line, we may have to rescan every bars in this line to make sure that the
	// maximum overlap level still be correct.
	//
	CUIntArray olBarnoList;
	GetOverlappedBarsFromTime(ipGroupno, ipLineno,
		prlBar->StartTime, prlBar->EndTime, olBarnoList);
	int ilListCount = olBarnoList.GetSize();

	// Save the bar in this overlapping group to a temporary array
	CCSPtrArray<PST_BARDATA> olSavedBars;
	BOOL blMustRecomputeMaxOverlapLevel = FALSE;
	for (int ilLc = 0; ilLc < ilListCount; ilLc++)
	{
		int ilBarno = olBarnoList[ilLc];
		PST_BARDATA *prlBar = &prlLine->Bars[ilBarno];
		blMustRecomputeMaxOverlapLevel |= (prlBar->OverlapLevel == prlLine->MaxOverlapLevel);
			// check if deletion of this bar may change the line height
		if (ilBarno != ipBarno)	// must we save this bar?
			olSavedBars.NewAt(olSavedBars.GetSize(), prlLine->Bars[ilBarno]);
	}
	// Delete all bars (which are already backed up in a temporary array) from the viewer
	while (ilListCount-- > 0)
	{
		int ilBarno = olBarnoList[ilListCount];
		prlLine->Bars.DeleteAt(ilBarno);
	}
	// Then, we insert those bars back to the Gantt chart, except the specified one
	int ilSavedBarCount = olSavedBars.GetSize();
	for (int ilSavedBarno = 0; ilSavedBarno < ilSavedBarCount; ilSavedBarno++)
	{
		CreateBar(ipGroupno, ipLineno, &olSavedBars[ilSavedBarno]);
	}
	olSavedBars.DeleteAll();
	// Then recompute the MaxOverlapLevel if it's necessary
	if (blMustRecomputeMaxOverlapLevel)
	{
		prlLine->MaxOverlapLevel = 0;
	    int ilBarCount = prlLine->Bars.GetSize();
		for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
		{
			PST_BARDATA *prlBar = &prlLine->Bars[ilBarno];
			prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, prlBar->OverlapLevel);
		}
	}
}

int PstDiagramViewer::CreateIndicator(int ipGroupno, int ipLineno, int ipBarno, PST_INDICATORDATA *prpIndicator)
{
    int ilIndicatorCount = omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators.GetSize();

    // Search for the position which we want to insert this new indicator
    for (int ilIndicatorno = 0; ilIndicatorno < ilIndicatorCount; ilIndicatorno++)
	{
		PST_INDICATORDATA *prlTmpIndicator = GetIndicator(ipGroupno,ipLineno,ipBarno,ilIndicatorno);
		if (prlTmpIndicator == NULL)
			break; // should not happen

		// Trying to place the indicator in the correct position.
		// I don't like this block of code, but it works fine.
		// Somebody should help me revise it.
		// -- Id, 29-Sep-96
		//
		if (prpIndicator->DemandUrno != 0)	// new indicator has a demand
		{
			if (prlTmpIndicator->DemandUrno == 0)
				break;	// should be inseted before a job without demand

			// the job in the buffer has some demand, so we use the old algorithm
			if (prpIndicator->IsArrival)	// new indicator is for arrival demand
			{
				if (!prlTmpIndicator->IsArrival)
					break;	// should be inserted before indicator for departure demand
				if (prlTmpIndicator->StartTime < prpIndicator->StartTime)
					break;	// should place indicator in time order
				if (prlTmpIndicator->StartTime == prpIndicator->StartTime)
				{
					if (prlTmpIndicator->DemandUrno < prpIndicator->DemandUrno)
						break;	// if the time is equal, we will use the Urno instead
				}
			}
			else	// new indicator is for departure demand
			{
				if (prlTmpIndicator->IsArrival)
					continue;	// bar in buffer is for arrival demand, step to the next one
				if (prlTmpIndicator->StartTime < prpIndicator->StartTime)
					break;	// should place indicator in time order
				if (prlTmpIndicator->StartTime == prpIndicator->StartTime)
				{
					if (prlTmpIndicator->DemandUrno < prpIndicator->DemandUrno)
						break;	// should place indicator in demand URNO order
				}
			}
		}
		else	// new indicator has no demand
		{
			if (prlTmpIndicator->DemandUrno != 0)
				continue;	// bar in buffer has demand, step to the next one
			if (prlTmpIndicator->StartTime < prpIndicator->StartTime)
				break;	// should place indicator in time order
				if (prlTmpIndicator->StartTime == prpIndicator->StartTime)
				{
					if (prlTmpIndicator->JobUrno < prpIndicator->JobUrno)
						break;	// if the time is equal, we will use the Urno instead
				}
		}
	}

    omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators.
		NewAt(ilIndicatorno, *prpIndicator);
    return ilIndicatorno;
}

void PstDiagramViewer::DeleteIndicator(int ipGroupno, int ipLineno, int ipBarno, int ipIndicatorno)
{
    omGroups[ipGroupno].Lines[ipLineno].Bars[ipBarno].Indicators.DeleteAt(ipIndicatorno);
}


/////////////////////////////////////////////////////////////////////////////
// PstDiagramViewer - PST_BARDATA calculation and searching routines

// Return the bar number of the list box based on the given period [time1, time2]
int PstDiagramViewer::GetBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2,
    int ipOverlapLevel1, int ipOverlapLevel2)
{
    for (int i = GetBarCount(ipGroupno, ipLineno)-1; i >= 0; i--)
    {
        PST_BARDATA *prlBar = GetBar(ipGroupno, ipLineno, i);
        if (opTime1 <= prlBar->EndTime && prlBar->StartTime <= opTime2 &&
            ipOverlapLevel1 <= prlBar->OverlapLevel && prlBar->OverlapLevel <= ipOverlapLevel2)
            return i;
    }
    return -1;
}


/////////////////////////////////////////////////////////////////////////////
// PstDiagramViewer private helper methods

void PstDiagramViewer::GetOverlappedBarsFromTime(int ipGroupno, int ipLineno,
	CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList)
{
    PST_LINEDATA *prlLine = GetLine(ipGroupno, ipLineno);
    int ilBarCount = prlLine->Bars.GetSize();

	// Prepare the empty array of the involvement flag for each bar
	CCSPtrArray<BOOL> IsInvolvedBar;
	for (int i = 0; i < ilBarCount; i++)
		IsInvolvedBar.NewAt(i, FALSE);

	// We will loop again and again until there is no more bar in this overlapping found.
	// The overlapping group is at first determine by the given "opTime1" and "opTime2".
	// But if the bar we find is exceed this period of time, we will extend searching
	// to both left side and right side in the next round.
	//
	int nBarsFoundThisRound = -1;
	while (nBarsFoundThisRound != 0)
	{
		nBarsFoundThisRound = 0;
		for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
		{
			CTime olStartTime = prlLine->Bars[ilBarno].StartTime;
			CTime olEndTime = prlLine->Bars[ilBarno].EndTime;
			BOOL blIsOverlapped = IsReallyOverlapped(olStartTime, olEndTime, opTime1, opTime2);
			BOOL blIsWithIn = IsWithIn(olStartTime, olEndTime, opTime1, opTime2);
				// this IsWithIn() fix the real thin bar bug
			BOOL blIsInvolved = (blIsOverlapped || blIsWithIn);

			if (!IsInvolvedBar[ilBarno] && blIsInvolved)	// some more new involved bars?
			{
				IsInvolvedBar[ilBarno] = TRUE;
				nBarsFoundThisRound++;
				opTime1 = min(opTime1, prlLine->Bars[ilBarno].StartTime);
				opTime2 = max(opTime2, prlLine->Bars[ilBarno].EndTime);
			}
		}
	}

	// Create the list of involved bars, then store them to "ropBarnoList"
	ropBarnoList.RemoveAll();
	for (int ilBarno = 0; ilBarno < ilBarCount; ilBarno++)
	{
		if (IsInvolvedBar[ilBarno])
		{
			ropBarnoList.Add(ilBarno);
		}
	}
	IsInvolvedBar.DeleteAll();
}

void PstDiagramViewer::AllowUpdates(BOOL bpNoUpdatesNow)
{
	bmNoUpdatesNow = bpNoUpdatesNow;
}



///////////////////////////////////////////////////////////////////////////////////////////
// Printing routines

void PstDiagramViewer::PrintPstDiagramHeader(int ipGroupno)
{
	CString olPstAreaName = GetGroupText(ipGroupno);
	CString omTarget;
//	omTarget.Format("%s  von: %s  bis: %s",olPstAreaName,omStartTime.Format("%d.%m.%Y %H:%M"),omEndTime.Format("%d.%m.%Y  %H:%M"));
	omTarget.Format(GetString(IDS_STRING61285),olPstAreaName,omStartTime.Format("%d.%m.%Y %H:%M"),omEndTime.Format("%d.%m.%Y  %H:%M"));
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");

	pomPrint->omCdc.StartPage();
//	pomPrint->PrintHeader(CString("Pstdiagramm"),olPrintDate,omTarget);
	pomPrint->PrintHeader(GetString(IDS_PST_DIAGRAM_TITLE),olPrintDate,omTarget);
	pomPrint->PrintTimeScale(400,2800,omStartTime,omEndTime,olPstAreaName);
}

void PstDiagramViewer::PrintPrepareLineData(int ipGroupNo,int ipLineno,CCSPtrArray<PRINTBARDATA> &ropPrintLine)
{
	PST_LINEDATA *prlLine = &omGroups[ipGroupNo].Lines[ipLineno];
	ropPrintLine.DeleteAll();
	int ilBarCount = prlLine->Bars.GetSize();
	for( int i = 0; i < ilBarCount; i++)
	{
		if (IsOverlapped(prlLine->Bars[i].StartTime,prlLine->Bars[i].EndTime,
			omStartTime,omEndTime))
		{
			PRINTBARDATA rlPrintBar;
			rlPrintBar.Text = prlLine->Bars[i].Text;
			rlPrintBar.StartTime = prlLine->Bars[i].StartTime;
			rlPrintBar.EndTime = prlLine->Bars[i].EndTime;
			rlPrintBar.FrameType = prlLine->Bars[i].FrameType;
			rlPrintBar.MarkerType = prlLine->Bars[i].MarkerType;
			rlPrintBar.IsShadowBar = prlLine->Bars[i].IsShadowBar;
			rlPrintBar.OverlapLevel = 0;
			rlPrintBar.IsBackGroundBar = FALSE;
			ropPrintLine.NewAt(ropPrintLine.GetSize(),rlPrintBar);
		}
	}
}


void PstDiagramViewer::PrintManagers(int ipGroupno,int ipYOffset1,int ipYOffset2)
{
	CString olAllManagers;
	CCSPtrArray<JOBDATA> olFmgJobs;
	ogJobData.GetFmgJobsByAlid(olFmgJobs, GetGroup(ipGroupno)->PstAreaId);
	int ilFmgCount = olFmgJobs.GetSize();
	for (int ilLc = 0; ilLc < ilFmgCount; ilLc++)
	{
		if (IsOverlapped(olFmgJobs[ilLc].Acfr,olFmgJobs[ilLc].Acto,omStartTime,omEndTime ))
		{
			if (olAllManagers.IsEmpty() == FALSE)
			{
				olAllManagers += "   /   ";
			}
			SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(olFmgJobs[ilLc].Shur);
			olAllManagers += CString((prlShift != NULL ? prlShift->Sfca : "")) + ", ";
			olAllManagers += CString((prlShift != NULL ? prlShift->Egrp : "")) + ", ";
			EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(olFmgJobs[ilLc].Peno);
			olAllManagers += CString(prlEmp != NULL ? prlEmp->Lanm : "");
		} 
	} 
	if (olAllManagers.IsEmpty() == FALSE)
	{
		int ilYOffset = 0;
		if (igFirstGroupOnPage == FALSE)
		{
			pomPrint->PrintText(420,0,800,40,PRINT_SMALLBOLD,"Flightmanager:",TRUE);
			pomPrint->PrintText(420,40,0,130,PRINT_SMALL,olAllManagers,TRUE);
			pomPrint->PrintText(420,0,0,50,PRINT_SMALL,"");
		}
		else
		{
			pomPrint->PrintText(420,ipYOffset1,800,ipYOffset1+40,PRINT_SMALLBOLD,"Flightmanager:");
			pomPrint->PrintText(420,ipYOffset2,0,ipYOffset2+90,PRINT_SMALL,olAllManagers);
		}  
		igFirstGroupOnPage = FALSE;
		/********************
		pomPrint->PrintText(420,ipYOffset1+ilYOffset,800,ipYOffset1+40+ilYOffset,PRINT_SMALLBOLD,"Flightmanager:");
		pomPrint->PrintText(420,ipYOffset2+ilYOffset,2800,ipYOffset2+90+ilYOffset,PRINT_SMALL,olAllManagers);
		******************/
	}
}

void PstDiagramViewer::PrintPstArea(CPtrArray &opPtrArray,int ipGroupNo)
{

	if( (pomPrint->imLineNo+ogBasicData.imFreeBottomLines) > pomPrint->imMaxLines )
	{
		if ( pomPrint->imPageNo > 0)
		{
			pomPrint->PrintGanttBottom();
			pomPrint->PrintFooter("","");
			pomPrint->omCdc.EndPage();
			igFirstGroupOnPage = TRUE;
		}
		PrintManagers(ipGroupNo,380,420);
		PrintPstDiagramHeader(ipGroupNo);
	}
	else
	{
		PrintManagers(ipGroupNo,0,0);
		CString olPstAreaName = GetGroupText(ipGroupNo);
		pomPrint->PrintGanttHeader(400,2800,olPstAreaName);
	}

	CCSPtrArray<PRINTBARDATA> ropPrintLine;
	int ilLineCount = GetLineCount(ipGroupNo);
    for( int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
	{
		if( (pomPrint->imLineNo+ogBasicData.imFreeBottomLines) > pomPrint->imMaxLines )
		{
			if ( pomPrint->imPageNo > 0)
			{
				pomPrint->PrintGanttBottom();
				pomPrint->PrintFooter("","");
				pomPrint->omCdc.EndPage();
				igFirstGroupOnPage = TRUE;
			}
			PrintPstDiagramHeader(ipGroupNo);
		}

		PrintPrepareLineData(ipGroupNo,ilLineno,ropPrintLine);
		if (pomPrint->PrintGanttLine(GetLineText(ipGroupNo, ilLineno),ropPrintLine) != TRUE)
		{
			// page full
			pomPrint->PrintGanttBottom();
			pomPrint->PrintFooter("","");
			pomPrint->omCdc.EndPage();
			igFirstGroupOnPage = TRUE;
			pomPrint->imLineNo = 0;
			PrintPstDiagramHeader(ipGroupNo);
			// try it again, but don't continue with this line if it don't fit on page again!
			pomPrint->PrintGanttLine(GetLineText(ipGroupNo, ilLineno),ropPrintLine);
		}

	}
	pomPrint->PrintGanttBottom();
	//pomPrint->imLineNo = 999;
}

void PstDiagramViewer::PrintOneFm(SHIFTDATA *prpShift,EMPDATA *prpEmp,JOBDATA *prpJob,BOOL bpIsFirstLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	int ilTopFrame = bpIsFirstLine == TRUE ? PRINT_FRAMETHIN : PRINT_NOFRAME;

	pomPrint->imLineHeight = 82;

	rlElement.Alignment  = PRINT_LEFT;
	rlElement.Length     = 650;
	rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
	rlElement.FrameRight = PRINT_FRAMETHIN;
	rlElement.FrameTop   = bpIsFirstLine;
	rlElement.FrameBottom= PRINT_FRAMETHIN;
	rlElement.pFont       = &pomPrint->omMediumFont_Regular;
	rlElement.Text       = ogEmpData.GetEmpName(prpEmp);
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Alignment  = PRINT_CENTER;
	rlElement.Length     = 120;
	rlElement.FrameLeft  = PRINT_FRAMETHIN;
	rlElement.FrameRight = PRINT_FRAMETHIN;
	rlElement.Text       = prpShift != NULL ? CString(prpShift->Sfca) : "";
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	rlElement.Length     = 300;
	rlElement.Text       = prpShift != NULL ? 
								prpJob->Acfr.Format("%H:%M") + "-" +
								prpJob->Acto.Format("%H:%M") : "";
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

	CCSPtrArray <JOBDATA> olJobs;
	ogJobData.GetJobsByPkno(olJobs,prpJob->Peno);
	olJobs.Sort(CompareJobStartTime);

	CString olFlightText;

	for (int ilLc = olJobs.GetSize()-1; ilLc >= 0; ilLc--)
	{
		JOBDATA *prlJob = &olJobs[ilLc];
	    // If it's not a special flightmanager job, ignore it
		if (strcmp(prlJob->Jtco,JOBFM) == 0)
		{
			FLIGHTDATA *prlFlight = ogBasicData.GetFlightForJob(prlJob);
			if (prlFlight != NULL)
			{
				if (olFlightText.IsEmpty() == FALSE)
				{
					olFlightText += CString(", ");
				}
				olFlightText += prlFlight->Fnum;
			}
		}

	}
	olJobs.RemoveAll();

	rlElement.Length     = 1550;
	rlElement.Alignment  = PRINT_LEFT;
	rlElement.FrameRight = PRINT_FRAMEMEDIUM;
	rlElement.Text       = olFlightText;
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
}

void PstDiagramViewer::PrintFmHeader(int ipGroupNo)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	CString olPstAreaName = GetGroupText(ipGroupNo);
	pomPrint->imLineHeight = 130;

	rlElement.Alignment  = PRINT_CENTER;
	rlElement.Length     = 2620;
	rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
	rlElement.FrameRight = PRINT_FRAMEMEDIUM;
	rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
	rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->omLargeFont_Bold;
	rlElement.Text       = olPstAreaName;
	
	rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
}


void PstDiagramViewer::PrintFmForPstArea(CPtrArray &opPtrArray,int ipGroupNo)
{
	BOOL blIsFirstLine = FALSE;

	if(pomPrint->imLineTop > pomPrint->imHeight-400)
	{
		if ( pomPrint->imPageNo > 0)
		{
			pomPrint->PrintFooter("","");
			pomPrint->omCdc.EndPage();
			pomPrint->imLineTop = pomPrint->imFirstLine;
			pomPrint->omCdc.StartPage();
			pomPrint->PrintHeader();
		}
	}

	PrintFmHeader(ipGroupNo);
	
	CCSPtrArray<JOBDATA> olFmgJobs;
	ogJobData.GetFmgJobsByAlid(olFmgJobs, GetGroup(ipGroupNo)->PstAreaId);
	olFmgJobs.Sort(CompareJobStartTime);
	int ilFmgCount = olFmgJobs.GetSize();
	//for (int ilLc = 0; ilLc < ilFmgCount; ilLc++)
	for(int ilLc = ilFmgCount-1; ilLc >= 0; ilLc--)
	{
		if (IsOverlapped(olFmgJobs[ilLc].Acfr,olFmgJobs[ilLc].Acto,omStartTime,omEndTime ))
		{
			SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(olFmgJobs[ilLc].Shur);
			EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(olFmgJobs[ilLc].Peno);
			if(pomPrint->imLineTop > pomPrint->imHeight-200)
			{
				if ( pomPrint->imPageNo > 0)
				{
					pomPrint->PrintFooter("","");
					pomPrint->omCdc.EndPage();
					pomPrint->imLineTop = pomPrint->imFirstLine;
					pomPrint->omCdc.StartPage();
					pomPrint->PrintHeader();
					blIsFirstLine = TRUE;
				}
			}
			PrintOneFm(prlShift,prlEmp,&olFmgJobs[ilLc],blIsFirstLine);
			blIsFirstLine = FALSE;
		} 
	} 
	
}



void PstDiagramViewer::PrintDiagram(CPtrArray &opPtrArray)
{
	igFirstGroupOnPage = TRUE;
	CString omTarget = CString("Anzeigebegin: ") + CString(omStartTime.Format("%d.%m.%Y"))
		+  "     Ansicht: " + ogCfgData.rmUserSetup.PSCV;
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");
	pomPrint = new CCSPrint(pomAttachWnd,PRINT_PORTRAIT,80,500,200,
		CString("Pstdiagramm"),olPrintDate,omTarget);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(PRINT_LANDSCAPE,ogBasicData.pcmPrintForm,DMPAPER_A3)  == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = "Pstdiagramm";	
			pomPrint->omCdc.StartDoc( &rlDocInfo );

			int ilGroupCount = GetGroupCount();
			for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
			{
				PrintPstArea(opPtrArray,ilGroupno);
			}
			pomPrint->PrintFooter("","");
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
}

void PstDiagramViewer::PrintFm(CPtrArray &opPtrArray)
{
	igFirstGroupOnPage = TRUE;
	CString omTarget = CString("Ansicht: ") + ogCfgData.rmUserSetup.PSCV;
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");
	pomPrint = new CCSPrint(pomAttachWnd,PRINT_LANDSCAPE,80,500,200,
		CString("Flightmanager"),olPrintDate,omTarget);
	if (pomPrint != NULL)
	{

		if (pomPrint->InitializePrinter(PRINT_LANDSCAPE,ogBasicData.pcmPrintForm,DMPAPER_A3)  == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = "Flightmanager";	
			pomPrint->imLineHeight = 82;
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->omCdc.StartPage();
			pomPrint->PrintHeader();

			int ilGroupCount = GetGroupCount();
			for (int ilGroupno = 0; ilGroupno < ilGroupCount; ilGroupno++)
			{
				PrintFmForPstArea(opPtrArray,ilGroupno);
			}
			pomPrint->PrintFooter("","");
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
}

CTime PstDiagramViewer::GetFlightArr(FLIGHTDATA *prpFlight, int ipReturnFlightType /*ID_NOT_RETURNFLIGHT*/)
{	
	CTime olStartTime;
	if(prpFlight != NULL)
	{
		if(ipReturnFlightType == ID_ARR_RETURNFLIGHT || ogFlightData.IsArrival(prpFlight))
		{
			olStartTime = prpFlight->Tifa;
			if(olStartTime == TIMENULL)
			{
				olStartTime = prpFlight->Stoa;
			}
		}
		else
		{
			olStartTime = prpFlight->Tifd;
			if(olStartTime == TIMENULL)
			{
				olStartTime = prpFlight->Stod;
			}
			olStartTime -= CTimeSpan(0,1,0,0);
		}
	}
	return olStartTime;
}

//	if the next info is set then this flight is indefinately delayed so use STD/STA
//  unless we have a better time than the scheduled time i.e. ETD/OFBL/AIRB are have
//  replaced the next info time.
CTime PstDiagramViewer::GetFlightDep(FLIGHTDATA *prpFlight, int ipReturnFlightType /*ID_NOT_RETURNFLIGHT*/)
{
	CTime olEndTime;
	if(prpFlight != NULL)
	{
		if(ipReturnFlightType == ID_DEP_RETURNFLIGHT || ogFlightData.IsDeparture(prpFlight))
		{
			olEndTime = prpFlight->Stod;
			if((!ogFlightData.AdviceTimeSet(prpFlight) || prpFlight->Tisa[0] != 'S') && prpFlight->Tifd != TIMENULL)
			{
				olEndTime = prpFlight->Tifd;
			}
		}
		else
		{
			olEndTime = prpFlight->Stoa;
			if((!ogFlightData.AdviceTimeSet(prpFlight) || prpFlight->Tisd[0] != 'S') && prpFlight->Tifa != TIMENULL)
			{
				olEndTime = prpFlight->Tifa;
			}
			olEndTime += CTimeSpan(0,0,20,0);
		}
	}
	return olEndTime;
}

// get a list of Demand URNOs for all jobs displayed
void PstDiagramViewer::GetDemandList(CUIntArray  &ropDemandList)
{
	for(int ilGroupno = 0; ilGroupno < GetGroupCount(); ilGroupno++)
	{
		for(int ilLineno = 0; ilLineno < GetLineCount(ilGroupno); ilLineno++)
		{
            for(int ilBarno = 0; ilBarno < GetBarCount(ilGroupno, ilLineno); ilBarno++)
			{
	            for(int ilIndicatorno = 0; ilIndicatorno < GetIndicatorCount(ilGroupno, ilLineno, ilBarno); ilIndicatorno++)
				{
					PST_INDICATORDATA *prlIndicator = GetIndicator(ilGroupno, ilLineno, ilBarno, ilIndicatorno);
					DEMANDDATA *prlDem = ogDemandData.GetDemandByUrno(prlIndicator->DemandUrno);
					//if(prlDem != NULL && !strcmp(prlDem->Rety,PERSONNEL_DEMAND))
					if(prlDem != NULL)
					{
		                ropDemandList.Add(prlIndicator->DemandUrno);
					}
				}
			}
		}
	}
}



bool PstDiagramViewer::IsGroupWithoutPositions(int ipGroupno)
{
	static CString olWithoutPositionName = GetString(IDS_NO_POSITION); // "Ohne Position"
	if (ipGroupno < 0)
		return false;

	if (olWithoutPositionName == GetGroupText(ipGroupno))
		return true;
	else
		return false;

}

int PstDiagramViewer::GetGroupWithoutPositions()
{
	int ilSize = GetGroupCount();
	for (int ilC = 0; ilC < ilSize; ilC++)
	{
		if (IsGroupWithoutPositions(ilC))
			return ilC;
	}
	return -1;
}

int PstDiagramViewer::SortLineOfGroupWithoutPositions(int ipLineNo, long lpFlightUrno /* = 0L */)
{
	int ilInsertedBar = -1;
	int ilGroupNo = GetGroupWithoutPositions();
	if (ipLineNo >= 0 && ilGroupNo >= 0)
	{
		int ilLineSize = GetLineCount(ilGroupNo);
		ASSERT(ipLineNo < ilLineSize);

		PST_LINEDATA *prlLine = GetLine(ilGroupNo,ipLineNo);
		if(prlLine != NULL)
		{
			prlLine->Bars.Sort(ByStartTime);	

			if(lpFlightUrno != 0L)
			{
				// return the inserted bar number
				int ilNumBars = prlLine->Bars.GetSize();
				for(int ilBar = 0; ilBar < ilNumBars; ilBar++)
				{
					if(prlLine->Bars[ilBar].FlightUrno == lpFlightUrno)
					{
						ilInsertedBar = ilBar;
						break;
					}
				}
			}
		}
	}
	return ilInsertedBar;
}


COLORREF PstDiagramViewer::GetColourForArrFlight(long lpFlightUrno)
{
	COLORREF rlColour = BLACK;
	FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(lpFlightUrno);
	if(!ogFlightData.IsArrivalOrBoth(prlFlight))
	{
		prlFlight = ogFlightData.GetRotationFlight(prlFlight);
	}

	if(prlFlight != NULL)
	{
		int ilReturnFlightType = ogFlightData.IsReturnFlight(prlFlight) ? ID_ARR_RETURNFLIGHT : ID_NOT_RETURNFLIGHT;
		FLIGHTDATA *prlRotation = ogFlightData.GetRotationFlight(prlFlight,ilReturnFlightType);
		if(prlFlight->Onbl != TIMENULL)
		{
			rlColour = atol(GetUserData("ONBL"));
		}
		else if(prlFlight->Land != TIMENULL)
		{
			rlColour = atol(GetUserData("LAND"));
		}
		else if(prlRotation != NULL && (prlRotation->Ofbl != TIMENULL || prlRotation->Airb != TIMENULL))
		{
			// departure already offblock or airborne so this must be onbl
			rlColour = atol(GetUserData("ONBL"));
		}
		else if(prlFlight->Tmoa != TIMENULL)
		{
			rlColour = atol(GetUserData("TMOA"));
		}
		else if(prlFlight->Etoa != TIMENULL)
		{
			rlColour = atol(GetUserData("ETOA"));
		}
		else if(prlFlight->Stoa != TIMENULL)
		{
			rlColour = atol(GetUserData("STOA"));
		}
	}

	return rlColour;
}

COLORREF PstDiagramViewer::GetColourForDepFlight(long lpFlightUrno)
{
	COLORREF rlColour = BLACK;
	FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(lpFlightUrno);
	if(!ogFlightData.IsDepartureOrBoth(prlFlight))
	{
		prlFlight = ogFlightData.GetRotationFlight(prlFlight);
	}

	if(prlFlight != NULL)
	{

		

		if(prlFlight->Airb != TIMENULL)
		{
			rlColour = atol(GetUserData("AIRB"));
		}
		else if(prlFlight->Ofbl != TIMENULL)
		{
			rlColour = atol(GetUserData("OFBL"));
		}
		else if(prlFlight->Slot != TIMENULL && IsPrivateProfileOn("SHOW_PST_SLOT_MARKER","NO") )
		{
 			rlColour = atol(GetUserData("SLOT"));
		}
		else if(prlFlight->Etod != TIMENULL)
		{
			rlColour = atol(GetUserData("ETOD"));
		}
		else if(prlFlight->Stod != TIMENULL)
		{
			rlColour = atol(GetUserData("STOD"));
		}
	}

	return rlColour;
}

bool PstDiagramViewer::DisplayMarker(CString opMarkerType)
{
	return (GetUserData(opMarkerType) == "YES") ? true : false;
}

bool PstDiagramViewer::IsAllFlightsGroup(int ipGroupno)
{
	static CString olAllFlightsGroupName = GetString(IDS_ALL_FLIGHTS);
	if (ipGroupno < 0)
		return false;

	if (olAllFlightsGroupName == GetGroupText(ipGroupno))
		return true;
	else
		return false;

}

int PstDiagramViewer::GetAllFlightsGroup()
{
	int ilSize = GetGroupCount();
	for (int ilC = 0; ilC < ilSize; ilC++)
	{
		if (IsAllFlightsGroup(ilC))
			return ilC;
	}
	return -1;
}

int PstDiagramViewer::SortLineOfGroupAllFlights(int ipLineNo, long lpFlightUrno /* = 0L */)
{
	int ilInsertedBar = -1;
	int ilGroupNo = GetAllFlightsGroup();
	if (ipLineNo >= 0 && ilGroupNo >= 0)
	{
		int ilLineSize = GetLineCount(ilGroupNo);
		ASSERT(ipLineNo < ilLineSize);

		PST_LINEDATA *prlLine = GetLine(ilGroupNo,ipLineNo);
		if(prlLine != NULL)
		{
			prlLine->Bars.Sort(ByStartTime);	

			if(lpFlightUrno != 0L)
			{
				// return the inserted bar number
				int ilNumBars = prlLine->Bars.GetSize();
				for(int ilBar = 0; ilBar < ilNumBars; ilBar++)
				{
					if(prlLine->Bars[ilBar].FlightUrno == lpFlightUrno)
					{
						ilInsertedBar = ilBar;
						break;
					}
				}
			}
		}
	}
	return ilInsertedBar;
}


// if single flight is not NULL then recreate a bar for the single flight
// only, otherwise create bars for all flights without positions
int PstDiagramViewer::CreateLinesForAllFlights(FLIGHTDATA *prpSingleFlight)
{
	int ilLastLineUpdated = -1;
	int ilGroupno = 0L;
	if(FindGroup(GetString(IDS_ALL_FLIGHTS),ilGroupno) && (bmIncludeArrivalFlights || bmIncludeDepartureFlights))
	{
		if(prpSingleFlight == NULL)
		{
			ilLastLineUpdated = CreateGeneralGroupLines(ogFlightData.omData, ilGroupno);
		}
		else
		{
			CCSPtrArray <FLIGHTDATA> olFlights;
			olFlights.Add(prpSingleFlight);
			ilLastLineUpdated = CreateGeneralGroupLines(olFlights, ilGroupno);
		}
	}

	return ilLastLineUpdated;
}

// if single flight is not NULL then recreate a bar for the single flight
// only, otherwise create bars for all flights without positions
int PstDiagramViewer::CreateLinesForFlightsWithoutPsts(FLIGHTDATA *prpSingleFlight /*=NULL*/)
{
	int ilLastLineUpdated = -1;
	int ilGroupno = 0L;
	if(FindGroup(GetString(IDS_NO_POSITION),ilGroupno) && (bmIncludeArrivalFlights || bmIncludeDepartureFlights))
	{
		CCSPtrArray <FLIGHTDATA> olFlights;
		if(prpSingleFlight == NULL)
		{
			int ilNumFlights = ogFlightData.omData.GetSize();
			for(int ilFlight = 0; ilFlight < ilNumFlights; ilFlight++)
			{
				olFlights.Add(&ogFlightData.omData[ilFlight]);
			}
		}
		else
		{
			olFlights.Add(prpSingleFlight);
		}
		ilLastLineUpdated = CreateGeneralGroupLines(olFlights, ilGroupno);
	}

	return ilLastLineUpdated;
}

int PstDiagramViewer::CreateGeneralGroupLines(CCSPtrArray <FLIGHTDATA> &ropFlights, int ipGroupno)
{
	bool blIsAllFlightsGroup = (GetGroup(ipGroupno)->PstAreaId == GetString(IDS_ALL_FLIGHTS));

	int ilLastLineUpdated = -1;
	FLIGHTDATA *prlFlight;
	int ilNumFlights = ropFlights.GetSize();
	if(ilNumFlights > 0)
	{
		// Need to find a space on an existing line to insert the new bar.
		// In order to do this a dummy bar (on a dummy line) is created so
		// that the same function is used (MakeBar) to create the new bar,
		// and thus the new bar is garanteed to have the correct length.
		// This dummy bar is then used to find a space big enough on existing
		// lines to insert the bar. If no lines are found then a new line is created.

		PST_LINEDATA *prlLine;
		int ilLineCount, ilLineno;
		PST_LINEDATA rlDummyLine;
		CString olDummyLineText;
		CString olDummyLineText2; 
		olDummyLineText.Format("% 5d",0);
		rlDummyLine.Text = olDummyLineText;
		CreateLine(ipGroupno, &rlDummyLine);
		int ilDummyLineNum;
		for(int ilLc = 0; ilLc < ilNumFlights; ilLc++)
		{
			prlFlight = &ropFlights[ilLc];
			
			CUIntArray olReturnFlightTypes;
			int ilNumReturnFlightTypes = GetReturnFlightTypes(prlFlight, olReturnFlightTypes);
			for(int ilF = 0; ilF < ilNumReturnFlightTypes; ilF++)
			{
				CString olPos = ogFlightData.GetPosition(prlFlight,olReturnFlightTypes[ilF]);
				void *pvlDummy = NULL;
				if(blIsAllFlightsGroup || olPos.IsEmpty() || omAdditionalPosForWithoutPosGrpMap.Lookup(olPos, (void *&) pvlDummy))
				{
					CString olAlc = ogAltData.FormatAlcString(prlFlight->Alc2,prlFlight->Alc3);
					BOOL blPassFilter = IsPassFilter("",olAlc,prlFlight->Hara,prlFlight->Stev,prlFlight->Ttyp,prlFlight->Act3,prlFlight->Act5,prlFlight->Urno);
					if( blPassFilter && ((bmIncludeArrivalFlights && (ogFlightData.IsArrival(prlFlight) || olReturnFlightTypes[ilF] == ID_ARR_RETURNFLIGHT))
						|| (bmIncludeDepartureFlights && (ogFlightData.IsDeparture(prlFlight) || olReturnFlightTypes[ilF] == ID_DEP_RETURNFLIGHT))))
					{
						if(FindLine(olDummyLineText, ipGroupno, ilDummyLineNum))
						{
							MakeBar(ipGroupno, ilDummyLineNum, prlFlight, blIsAllFlightsGroup, olReturnFlightTypes[ilF]);
							PST_LINEDATA *prlDummyLine = GetLine(ipGroupno, ilDummyLineNum);
							if(prlDummyLine->Bars.GetSize() > 0) // make sure the dummy bar was created
							{
								PST_BARDATA *prlBarToAdd = GetBar(ipGroupno, ilDummyLineNum, 0);

								int ilSelectedLine = -1;
								ilLineCount = GetLineCount(ipGroupno);
								if (!bmOneFlightPerLine) 
								{
									for(ilLineno = 0; ilSelectedLine == -1 && ilLineno < ilLineCount; ilLineno++)
									{
										prlLine = GetLine(ipGroupno, ilLineno);
										if(prlLine->Text != olDummyLineText)
										{
											int ilNumBars = prlLine->Bars.GetSize();
											if(ilNumBars <= 0)
											{
												ilSelectedLine = ilLineno;
											}
											else
											{
												CTimeSpan olOneMinute(0,0,1,0);

												if(ilNumBars > 0 && prlLine->Bars[0].StartTime > prlBarToAdd->EndTime)
												{
													//  the new bar fits in before the first one
													ilSelectedLine = ilLineno;
												}

												for(int ilBar = 0; ilSelectedLine == -1 && ilBar < ilNumBars; ilBar++)
												{
													CTime olPrevEnd = prlLine->Bars[ilBar].EndTime;
													CTime olNextBegin = ((ilBar+1) < ilNumBars) ? prlLine->Bars[ilBar+1].StartTime : (prlBarToAdd->EndTime+olOneMinute);
													if(prlBarToAdd->StartTime > olPrevEnd && prlBarToAdd->EndTime < olNextBegin)
													{
														// the new bar fits in the free space
														ilSelectedLine = ilLineno;
													}
												}
											}
										}
									}
								}
								else
								{
									for(ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
									{
										prlLine = GetLine(ipGroupno, ilLineno);
										if(prlLine->Text != olDummyLineText)
										{
											if (ilSelectedLine == -1)
											{
												int ilNumBars = prlLine->Bars.GetSize();
												if(ilNumBars > 0)
												{
													CTimeSpan olOneMinute(0,0,1,0);

													if(ilNumBars > 0 && prlLine->Bars[0].StartTime > prlBarToAdd->StartTime)
													{
														//  the new bar fits in before the first one
														ilSelectedLine = ilLineno;
														olDummyLineText2.Format("% 5d",ilLineno + 1);
														prlLine->Text = olDummyLineText2;
													}
												}
											}
											else
											{
												olDummyLineText2.Format("% 5d",ilLineno + 1);
												prlLine->Text = olDummyLineText2;
											}
										}
									}
								}
									 
								if(ilSelectedLine != -1)
								{
									// existing line found so add the new bar and sort all bars in this line by duty start
									if (bmOneFlightPerLine)
									{
										PST_LINEDATA rlLine;
										rlLine.Text.Format("% 5d",ilSelectedLine);
										rlLine.IsStairCasePst = false;
										int ilLineNum = CreateLine(ipGroupno, &rlLine);
										MakeBar(ipGroupno, ilLineNum, prlFlight, blIsAllFlightsGroup, olReturnFlightTypes[ilF]);
										ilLastLineUpdated = ilLineNum;
									}
									else
									{
										MakeBar(ipGroupno, ilSelectedLine, prlFlight, blIsAllFlightsGroup, olReturnFlightTypes[ilF]);
										prlLine = GetLine(ipGroupno, ilSelectedLine);
										prlLine->Bars.Sort(ByStartTime);
										ilLastLineUpdated = ilSelectedLine;
									}
								}	
								else
								{
									// no lines free so create a new one
									PST_LINEDATA rlLine;
									rlLine.Text.Format("% 5d",ilLineCount);
									rlLine.IsStairCasePst = false;
									int ilLineNum = CreateLine(ipGroupno, &rlLine);
									MakeBar(ipGroupno, ilLineNum, prlFlight, blIsAllFlightsGroup, olReturnFlightTypes[ilF]);
									ilLastLineUpdated = ilLineNum;
								}
								DeleteBar(ipGroupno, ilDummyLineNum, 0);
							}
						}
					}
				}
			}
		}
		if(FindLine(olDummyLineText, ipGroupno, ilDummyLineNum))
		{
			DeleteLine(ipGroupno, ilDummyLineNum);
			if(ilDummyLineNum < ilLastLineUpdated)
			{
				ilLastLineUpdated--;
			}
		}
	}

	return ilLastLineUpdated;
}

//PRF 8999
CMapStringToPtr* PstDiagramViewer::GetMapForFunctions() const
{
	return const_cast<CMapStringToPtr*>(&omCMapForFunctions); 
}

//PRF 8999
BOOL PstDiagramViewer::CheckDemandValidity(const DEMANDDATA*& ropDemandData) const
{
	BOOL blDemandFunctionValidity = FALSE;
	void* p;
	RPFDATA *prlDemandFunction = NULL;
	if(bmUseAllFunctions)
	{
		blDemandFunctionValidity = true;
	}
	else if(strcmp(ropDemandData->Rety,"100") != 0)
	{
		blDemandFunctionValidity = TRUE;
	}
	else if((prlDemandFunction = ogRpfData.GetFunctionByUrud(ropDemandData->Urud)) != NULL)
	{
		blDemandFunctionValidity = omCMapForFunctions.Lookup(prlDemandFunction->Fcco,p);
	}

	BOOL blEquDemandGroupValidity = TRUE;
	if (bmUseAllEquDemandGroups)
	{
		blEquDemandGroupValidity = TRUE;
	}
	else if(ogRudData.DemandIsOfType(ropDemandData->Urud, EQUIPMENTDEMANDS))
	{
		REQDATA* polReq = ogReqData.GetReqByUrud(ropDemandData->Urud);
		if(polReq != NULL)
		{
			EQUDATA *prlEqu = NULL;

			if(strlen(polReq->Eqco) == 0)
			{
				SGRDATA *prlSgr = ogSgrData.GetSgrByUrno(polReq->Uequ);
				if(prlSgr != NULL)
				{
					blEquDemandGroupValidity = omCMapForEquDemandGroups.Lookup(prlSgr->Grpn,p);
				}
			}
			else
			{
				blEquDemandGroupValidity = FALSE;
			}
		}
	}

	return blDemandFunctionValidity & blEquDemandGroupValidity;
}

CMapStringToPtr* PstDiagramViewer::GetMapForEquDemandGroups() const
{		
	return const_cast<CMapStringToPtr*>(&omCMapForEquDemandGroups);
}