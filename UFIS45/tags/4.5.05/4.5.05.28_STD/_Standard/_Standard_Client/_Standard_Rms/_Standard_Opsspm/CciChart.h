// CciChart.h : header file
//

#ifndef _CCICHRT_
#define _CCICHRT_


#ifndef _CHART_STATE_
#define _CHART_STATE_

enum ChartState { Minimized, Normal, Maximized };

#endif // _CHART_STATE_

#include <CCSButtonCtrl.h>

/////////////////////////////////////////////////////////////////////////////
// CCIChart frame

class CCIDiagram;


class CCIChart : public CFrameWnd
{
	friend CCIDiagram;

    DECLARE_DYNCREATE(CCIChart)
public:
    CCIChart();           // protected constructor used by dynamic creation
    virtual ~CCIChart();

// Attributes
public:
    CCSDragDropCtrl   m_DragDropObject;   
    
// Operations
public:
    int GetHeight();
    ChartState GetState(void) { return imState; };
    void SetState(ChartState ipState) { imState = ipState; };
    
    CTimeScale *GetLeftTimeScale(void) { return pomLeftTimeScale; };
    CTimeScale *GetRightTimeScale(void) { return pomRightTimeScale; };
    void SetTimeScale(CTimeScale *popLeftTimeScale, CTimeScale *popRightTimeScale)
    {
        pomLeftTimeScale = popLeftTimeScale;
		pomRightTimeScale = popRightTimeScale;
    };

    CStatusBar *GetStatusBar(void) { return pomStatusBar; };
    void SetStatusBar(CStatusBar *popStatusBar)
    {
        pomStatusBar = popStatusBar;
    };
    
    CciDiagramViewer *GetViewer(void) { return pomViewer; };
    int GetGroupNo() { return imGroupNo; };
    void SetViewer(CciDiagramViewer *popViewer, int ipGroupNo)
    {
        pomViewer = popViewer;
        imGroupNo = ipGroupNo;
    };
    
    CTime GetStartTime(void) { return omStartTime; };
    void SetStartTime(CTime opStartTime) { omStartTime = opStartTime; };

    CTimeSpan GetInterval(void) { return omInterval; };
    void SetInterval(CTimeSpan opInterval) { omInterval = opInterval; };

    CciGantt *GetLeftGanttPtr(void) { return &omLeftGantt; };
    CciGantt *GetRightGanttPtr(void) { return &omRightGantt; };

    CCSButtonCtrl *GetChartButtonPtr(void) { return &omButton; };
    C3DStatic *GetTopScaleTextPtr(void) { return pomTopScaleText; };

// Overrides
public:

// Implementation
protected:

    // Generated message map functions
    //{{AFX_MSG(CCIChart)
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnPaint();
    afx_msg void OnChartButton();
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnDestroy();
    afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
    
protected:
    ChartState imState;
    int imHeight;
    
    CCSButtonCtrl omButton;
    C3DStatic *pomTopScaleText;
    
    CTimeScale *pomLeftTimeScale;
    CTimeScale *pomRightTimeScale;
    CStatusBar *pomStatusBar;
    CciDiagramViewer *pomViewer;
    int imGroupNo;
    CTime omStartTime;
    CTimeSpan omInterval;
    CciGantt omLeftGantt;
	CciGantt omRightGantt;

private:    
    static COLORREF lmBkColor;
    static COLORREF lmTextColor;
    static COLORREF lmHilightColor;
    static int imStartTopScaleTextPos;
    static int imStartVerticalScalePos;

// Drag-and-drop section
public:
	LONG ProcessDropShifts(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect);

public:
	bool bmScrolling;
	int  imScrollSpeed;
	void AutoScroll(UINT ipInitialScrollSpeed = 200);
	void OnAutoScroll(void);
    CCSDragDropCtrl m_ChartWindowDragDrop;
    CCSDragDropCtrl m_ChartButtonDragDrop;
    CCSDragDropCtrl m_TopScaleTextDragDrop;
};

/////////////////////////////////////////////////////////////////////////////

#endif // _CCICHRT_
