// dstfsort.cpp : implementation file
//

#include <stdafx.h>
#include <OpssPm.h>
#include <StaffSortDlg.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CStaffSortDialog dialog


CStaffSortDialog::CStaffSortDialog(CWnd* pParent /*=NULL*/)
    : CDialog(CStaffSortDialog::IDD, pParent)
{
    //{{AFX_DATA_INIT(CStaffSortDialog)
    m_SortCriteria2 = -1;
    m_SortCriteria0 = -1;
    m_SortCriteria1 = -1;
    //}}AFX_DATA_INIT
}

void CStaffSortDialog::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CStaffSortDialog)
    DDX_Radio(pDX, IDC_RADIO15, m_SortCriteria2);
    DDX_Radio(pDX, IDC_RADIO1, m_SortCriteria0);
    DDX_Radio(pDX, IDC_RADIO8, m_SortCriteria1);
    //}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CStaffSortDialog, CDialog)
    //{{AFX_MSG_MAP(CStaffSortDialog)
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CStaffSortDialog message handlers

BOOL CStaffSortDialog::OnInitDialog()
{
    CDialog::OnInitDialog();
    
    // move the window to be at the center over the parent window
    CRect rect, rectParent;
    GetWindowRect(&rect);
    GetParent()->GetWindowRect(&rectParent);
    int ilHorzOffset = (rectParent.Width() - rect.Width()) / 2;
    int ilVertOffset = (rectParent.Height() - rect.Height()) / 2;
    rect.OffsetRect(ilHorzOffset, ilVertOffset);
    MoveWindow(&rect, FALSE);
    
    return TRUE;  // return TRUE  unless you set the focus to a control
}
