// GateTable.cpp : implementation file
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaAloData.h>

#include <OpssPm.h>
#include <table.h>
#include <CedaJobData.h>
#include <CedaFlightData.h>
#include <CedaEmpData.h>
#include <GateTableViewer.h>
#include <GateTable.h>

#include <cviewer.h>
#include <FilterPage.h>
#include <ZeitPage.h>
#include <GateTableSortPage.h>
#include <BasePropertySheet.h>
#include <GateTablePropertySheet.h>
#include <BasicData.h>
#include <ccsddx.h>
#include <CedaCfgData.h>
#include <dataset.h>

#include <cxbutton.h>
#include <CedaShiftData.h>
#include <CCIDiagram.h>
#include <CCITable.h>
#include <ConflictTable.h>
#include <ConflictConfigTable.h>
#include <StaffDiagram.h>
#include <GateDiagram.h>
#include <RegnDiagram.h>
#include <FlightPlan.h>
#include <StaffTable.h>
#include <PrePlanTable.h>
#include <ButtonList.h>
#include <GateDetailWindow.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Prototypes

/////////////////////////////////////////////////////////////////////////////
// GateTable dialog

GateTable::GateTable(CWnd* pParent /*=NULL*/)
	: CDialog(GateTable::IDD, pParent)
{
	//{{AFX_DATA_INIT(GateTable)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	pomTable = new CTable;
    pomTable->tempFlag = 2;

    CDialog::Create(GateTable::IDD);
    CRect olRect;
	ogBasicData.GetWindowPosition(olRect,ogCfgData.rmUserSetup.GATB,ogCfgData.rmUserSetup.MONS);
	olRect.top += 40;
	BOOL blMinimized = FALSE;
	ogBasicData.GetDialogFromReg(olRect, COpssPmApp::GATES_TABLE_WINDOWPOS_REG_KEY,blMinimized);	

	//Multiple monitor setup
	ogBasicData.GetWindowPositionCorrect(olRect);

    MoveWindow(&olRect);
	bmIsViewOpen = FALSE;

	CTime tm = ogBasicData.GetTime();

    CRect rect;
    GetClientRect(&rect);
    rect.OffsetRect(0, m_nDialogBarHeight);
    rect.InflateRect(1, 1);     // hiding the CTable window border
    pomTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);

	omViewer.Attach(pomTable);
	UpdateView();

	if(blMinimized == TRUE)
	{
		ShowWindow(SW_MINIMIZE);
	}
}

GateTable::~GateTable()
{
	TRACE("GateTable::~GateTable()\n");
	omViewer.Attach(NULL);
	delete pomTable;	
}

void GateTable::SetCaptionText(void)
{
//	CString olCaptionText = CString("Gate Belegung: ") + 
//		omViewer.omStartTime.Format("%d/%H%M") + "-" + 
//			omViewer.omEndTime.Format("%H%M ");;
	CString olCaptionText = GetString(IDS_STRING61344) + omViewer.StartTime().Format("%d/%H%M") + "-" + omViewer.EndTime().Format("%H%M ");;
//	if (bgOnline)
//	{
//		//olCaptionText += "  Online";
//		olCaptionText += CString("  ") + GetString(IDS_STRING61301);
//	}
//	else
//	{
//		//olCaptionText += "  OFFLINE";
//		olCaptionText += CString("  ") + GetString(IDS_STRING61302);
//	}

	SetWindowText(olCaptionText);

}

void GateTable::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GateTable)
	DDX_Control(pDX, IDC_DATE, m_Date);
	//}}AFX_DATA_MAP
}

void GateTable::UpdateView()
{

	CString  olViewName = omViewer.GetViewName();
	olViewName = omViewer.GetViewName();

	if (olViewName.IsEmpty() == TRUE)
	{
		olViewName = ogCfgData.rmUserSetup.GBLV;
	}
	
	
	AfxGetApp()->DoWaitCursor(1);
    omViewer.ChangeViewTo(olViewName);
	AfxGetApp()->DoWaitCursor(-1);
	SetCaptionText();

	//Singapore
	OnTableUpdateDataCount();
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
//	pomTable->GetCTableListBox()->SetFocus();
}

void GateTable::UpdateComboBox()
{
	CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_VIEWCOMBO);
	polCB->ResetContent();
	CStringArray olStrArr;
	omViewer.GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = omViewer.GetViewName();
	if (olViewName.IsEmpty() == TRUE)
	{
		olViewName = ogCfgData.rmUserSetup.GBLV;
	}

	ilIndex = polCB->FindString(-1,olViewName);
		
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}

}

BEGIN_MESSAGE_MAP(GateTable, CDialog)
	//{{AFX_MSG_MAP(GateTable)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_MOVE() //PRF 8712
	ON_CBN_SELCHANGE(IDC_VIEWCOMBO, OnSelchangeViewcombo)
	ON_BN_CLICKED(IDC_VIEW, OnView)
	ON_BN_CLICKED(IDC_PRINT, OnPrint)
	ON_CBN_CLOSEUP(IDC_VIEWCOMBO, OnCloseupViewcombo)
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
	ON_WM_CLOSE()
	ON_CBN_SELCHANGE(IDC_DATE, OnSelchangeDate)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
	ON_MESSAGE(WM_TABLE_UPDATE_DATACOUNT,OnTableUpdateDataCount) //Singapore
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GateTable message handlers

BOOL GateTable::OnInitDialog() 
{
	CDialog::OnInitDialog();
	SetWindowText(GetString(IDS_STRING32986)); 

	CWnd *polWnd = GetDlgItem(IDC_VIEW); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61840));
	}
	polWnd = GetDlgItem(IDC_PRINT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61844));
	}
	
	//Singapore
	if(ogBasicData.IsPaxServiceT2() == false)
	{
		CWnd* polWnd = GetDlgItem(IDC_DATACOUNT);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}
	}
	// TODO: Add extra initialization here
    CRect rect;
    GetClientRect(&rect);
    m_nDialogBarHeight = rect.bottom - rect.top;

	UpdateComboBox();

    // extend dialog window to current screen width
    rect.left = 10;
    rect.top = 56;
    rect.right = ::GetSystemMetrics(SM_CXSCREEN) - 10;
    rect.bottom = ::GetSystemMetrics(SM_CYSCREEN) - 10;
    //MoveWindow(&rect); //Commented - Singapore PRF 8712

	m_DragDropTarget.RegisterTarget(this, this);

	// Register DDX call back function
	TRACE("GateTable: DDX Registration\n");
	ogCCSDdx.Register(this, REDISPLAY_ALL, CString("GATETABLE"),CString("Redisplay all"), GateTableCf);	// for what-if changes
	ogCCSDdx.Register(this, GLOBAL_DATE_UPDATE, CString("GATETABLE"),CString("Global Date Change"), GateTableCf); // change the date of the data displayed


	CStringArray olTimeframeList;
	ogBasicData.GetTimeframeList(olTimeframeList);
	int ilNumDays = olTimeframeList.GetSize();
	for(int ilDay = 0; ilDay < ilNumDays; ilDay++)
	{
		m_Date.AddString(olTimeframeList[ilDay]);
	}
	if(ilNumDays > 0)
	{
		m_Date.SetCurSel(ogBasicData.GetDisplayDateOffset());
		SetViewerDate();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void GateTable::OnDestroy() 
{
	if (bgModal == TRUE)
		return;	
	ogBasicData.WriteDialogToReg(this,COpssPmApp::GATES_TABLE_WINDOWPOS_REG_KEY,omWindowRect,this->IsIconic());
	// Unregister DDX call back function
	TRACE("GateTable: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogCCSDdx.UnRegister(this, NOTUSED);

	CDialog::OnDestroy();
}

void GateTable::OnCancel() 
{
	// Normally, we should just call CDialog::OnCancel().
	// However, the DestroyWindow() is required since this table can be destroyed in two ways.
	// First is by pressing ESC key which will call this routine.
	// Second, it may be destroyed directly by calling Manfred's DestroyWindow() in this class
	// which is not so compatible with MFC.
	DestroyWindow();
	AfxGetMainWnd()->SendMessage(WM_GATETABLE_EXIT);
   	pogButtonList->m_wndGateTable = NULL;
	pogButtonList->m_GateTableButton.Recess(FALSE);
}

void GateTable::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
   if (nType != SIZE_MINIMIZED)
   {
        pomTable->SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy+1);
        GetWindowRect(&omWindowRect); //PRF 8712
   }
}

void GateTable::OnView() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
//	pomTable->GetCTableListBox()->SetFocus();

	GateTablePropertySheet dialog(this, &omViewer);
	bmIsViewOpen = TRUE;
	if (dialog.DoModal() != IDCANCEL)
	{
		UpdateComboBox();
		UpdateView();
	}
	bmIsViewOpen = FALSE;
}

void GateTable::OnSelchangeViewcombo() 
{
    char clText[64];
    CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_VIEWCOMBO);
    polCB->GetLBText(polCB->GetCurSel(), clText);
	omViewer.SelectView(clText);
	UpdateView();		
}

void GateTable::OnCloseupViewcombo() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box when the combo-box is closed.
//    pomTable->GetCTableListBox()->SetFocus();
}

void GateTable::OnPrint() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
//	pomTable->GetCTableListBox()->SetFocus();

	omViewer.PrintView();
}

BOOL GateTable::DestroyWindow() 
{
	if ((bmIsViewOpen) || (bgModal == TRUE))
	{
		MessageBeep((UINT)-1);
		return FALSE;    // don't destroy window while view property sheet is still open
	}

	BOOL blRc = CDialog::DestroyWindow();
	delete this;
	return blRc;
}

////////////////////////////////////////////////////////////////////////
// GateTable -- implementation of DDX call back function

void GateTable::GateTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
	GateTable *polTable = (GateTable *)popInstance;

	if (ipDDXType == REDISPLAY_ALL)
	{
		polTable->UpdateView();
	}
	else if(ipDDXType == GLOBAL_DATE_UPDATE)
	{
		polTable->HandleGlobalDateUpdate();
	}
}

//*****
/////////////////////////////////////////////////////////////////////////////
// GateTable -- drag-and-drop functionalities
//
// The user may drag from:
//	- any non-empty line for create a JobFlight.
//
// and the user will have opportunities to:
//	-- drop from StaffTable (single or multiple staff shift) onto any non-empty lines.
//		We will create normal flight jobs for those staffs.
//	-- drop from DutyBar onto onto any non-empty lines.
//		We will create a normal flight job for that staff.
//
LONG GateTable::OnDragOver(UINT wParam, LONG lParam)
{
    int ilClass = m_DragDropTarget.GetDataClass(); 
	if (ilClass != DIT_DUTYBAR)
		return -1L;	// cannot interpret this object

	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);
	int ilDropLineno = pomTable->GetLinenoFromPoint(olDropPosition);
	if (!(0 <= ilDropLineno && ilDropLineno <= omViewer.Lines() - 1))
		return -1L;	// cannot drop on a blank line

	GATEDATA_LINE *prlLine = omViewer.GetLine(ilDropLineno);
	if (!prlLine)
		return -1L;

	imDropFlightUrno = prlLine->FlightUrno;
	int ilColumn = pomTable->GetLinenoFromPoint(olDropPosition);
	imDropDemandUrno = ilColumn;
	return 0L;
}

LONG GateTable::OnDrop(UINT wParam, LONG lParam)
{ 
	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);
	int ilDropLineno = pomTable->GetLinenoFromPoint(olDropPosition);
	if (!(0 <= ilDropLineno && ilDropLineno <= omViewer.Lines() - 1))
		return -1L;	// cannot drop on a blank line

	GATEDATA_LINE *prlLine = omViewer.GetLine(ilDropLineno);
	if (!prlLine)
		return -1L;

	imDropFlightUrno = prlLine->FlightUrno;
	int ilColumn = pomTable->GetColumnnoFromPoint(olDropPosition);
	int ilDemandNo = (ilColumn-6)/2;
	bool blTeamAlloc = false; // assigning a whole team ?

	
	CDWordArray olPoolJobUrnos;
	int ilDataCount = m_DragDropTarget.GetDataCount() - 1;
	if(ilDataCount > 0)
	{
		for(int ilPoolJob = 0; ilPoolJob < ilDataCount; ilPoolJob++)
		{
			long llPoolJobUrno = m_DragDropTarget.GetDataDWord(ilPoolJob);
			olPoolJobUrnos.Add(llPoolJobUrno);
		}
		blTeamAlloc = m_DragDropTarget.GetDataDWord(ilDataCount) == 1 ? true : false; // assigning a whole team ?
	}

	CString olGate = ogFlightData.GetGate(imDropFlightUrno);
	if ((ilDemandNo >= 0) && (ilDemandNo < prlLine->Indicators.GetSize()))
	{
		long llDemandUrno = prlLine->Indicators[ilDemandNo].DemandUrno;
		if (ogJodData.GetFirstJobUrnoByDemand(llDemandUrno) == 0L)
		{
			DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(llDemandUrno);
			if(prlDemand != NULL)
			{
				CCSPtrArray <DEMANDDATA> olDemands;
				olDemands.Add(prlDemand);
				ogDataSet.CreateJobFlightFromDuty(this, olPoolJobUrnos, imDropFlightUrno, ALLOCUNITTYPE_GATE, ogFlightData.GetGate(imDropFlightUrno), blTeamAlloc, &olDemands);
				return 0L;
			}
		}
	}
	// create job flight from duty with (default) demand
	if(ilDataCount > 0)
	{
		ogDataSet.CreateJobFlightFromDuty(this, olPoolJobUrnos, imDropFlightUrno, ALLOCUNITTYPE_GATE, ogFlightData.GetGate(imDropFlightUrno), blTeamAlloc);
	}

	return 0L;
}

void GateTable::OnClose() 
{
   	pogButtonList->m_wndGateTable = NULL;
	pogButtonList->m_GateTableButton.Recess(FALSE);
	CDialog::OnClose();
}

void GateTable::OnSelchangeDate() 
{
	SetViewerDate();
	UpdateView();
}

void GateTable::SetViewerDate()
{
	CTime olStart = ogBasicData.GetTimeframeStart();
	CTime olDay = CTime(olStart.GetYear(),olStart.GetMonth(),olStart.GetDay(),0,0,0)  + CTimeSpan(m_Date.GetCurSel(),0,0,0);

	omViewer.SetStartTime(CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0));
	omViewer.SetEndTime(CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59));
}

// called in response to a global date update - new day selected ie in preplanning so syschronise the date of this display
void GateTable::HandleGlobalDateUpdate()
{
	m_Date.SetCurSel(ogBasicData.GetDisplayDateOffset());
	OnSelchangeDate();
}

LONG GateTable::OnTableLButtonDblclk(UINT ipItem, LONG lpLParam)
{
	UINT ilNumLines = omViewer.Lines();
	if (ipItem >= 0 && ipItem < ilNumLines)
	{
		GATEDATA_LINE *prlLine = omViewer.GetLine(ipItem);
		if (prlLine != NULL)
		{
			BOOL blArrival   = TRUE;
			BOOL blDeparture = TRUE;
			new GateDetailWindow(this, prlLine->Alid,blArrival,blDeparture,omViewer.StartTime(),omViewer.EndTime());
		}
	}

	return 0L;
}

//Singapore
void GateTable::OnTableUpdateDataCount()
{	
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		CWnd* polWnd = GetDlgItem(IDC_DATACOUNT);
		if(polWnd != NULL)
		{
			char olDataCount[10];
			polWnd->EnableWindow();			
			polWnd->SetWindowText(itoa(omViewer.Lines(),olDataCount,10));
		}
	}
}

//PRF 8712
void GateTable::OnMove(int x, int y)
{	
	CDialog::OnMove(x,y);
	if(this->IsIconic() == FALSE)
	{
		GetWindowRect(&omWindowRect);
	}
}