// CCITable.h : header file
//
#ifndef __CCITABLE_H__
#define __CCITABLE_H__

/////////////////////////////////////////////////////////////////////////////
// CCITable dialog

#include <CciTableViewer.h>


class CCITable : public CDialog
{
// Construction
public:
	CCITable(CWnd* pParent = NULL);   // standard constructor
	~CCITable();
	void UpdateView();
	void SetCaptionText();


private :
    CTable *pomTable; // visual object, the table content
	BOOL bmIsViewOpen;
	int m_nDialogBarHeight;
	CciTableViewer omViewer;
	CRect omWindowRect; //PRF 8712

private:
	void SetViewerDate();
	void UpdateComboBox();
	CString Format(CCIDATA_LINE *prpLine);
	static void CCITableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
	void HandleGlobalDateUpdate();
 
// Dialog Data
	//{{AFX_DATA(CCITable)
	enum { IDD = IDD_CCITABLE };
	CComboBox	m_Date;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCITable)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCITable)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMove(int x, int y); //PRF 8712
	afx_msg void OnView();
	afx_msg void OnSelchangeViewcombo();
	afx_msg void OnCloseupViewcombo();
	afx_msg void OnPrint();
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	afx_msg void OnClose();
	afx_msg void OnSelchangeDate();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnTableUpdateDataCount(); //Singapore
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Drag-and-drop implementation
private:
	CCSDragDropCtrl m_DragDropTarget;
	int				imDropLineno;
};

#endif //__CCITABLE_H__
