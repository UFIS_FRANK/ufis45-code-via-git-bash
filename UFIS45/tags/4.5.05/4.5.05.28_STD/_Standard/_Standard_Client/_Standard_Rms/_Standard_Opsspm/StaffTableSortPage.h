// StaffTableSortPage.h : header file
//

#ifndef _STAFFLSO_H_
#define _STAFFLSO_H_

/////////////////////////////////////////////////////////////////////////////
// StaffTableSortPage dialog

class StaffTableSortPage : public CPropertyPage
{
	DECLARE_DYNCREATE(StaffTableSortPage)

// Construction
public:
	StaffTableSortPage();
	~StaffTableSortPage();

// Dialog Data
	CStringArray omSortOrders;
	CString omTitle;

	//{{AFX_DATA(StaffTableSortPage)
	enum { IDD = IDD_STAFFTABLE_SORT_PAGE };
	BOOL	m_Group;
	int		m_SortOrder0;
	int		m_SortOrder1;
	int		m_SortOrder2;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(StaffTableSortPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(StaffTableSortPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Helper routines
private:
	int GetSortOrder(const char *pcpSortKey);
	CString GetSortKey(int ipSortOrder);
};

#endif // _STAFFLSO_H_
