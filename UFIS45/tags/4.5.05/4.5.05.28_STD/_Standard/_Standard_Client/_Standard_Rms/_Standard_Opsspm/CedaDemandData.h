#ifndef _CDEMANDD_H_
#define _CDEMANDD_H_

#include <CCSCedaData.h>
#include <CCSGlobl.h>
#include <resource.h>
#include <CedaFlightData.h>

void ProcessDemandCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

#define NO_DEMAND			"000"
#define PERSONNEL_DEMAND	"100"
#define EQUIPMENT_DEMAND	"010"
#define LOCATION_DEMAND		"001"
#define ALL_DEMAND			"111"

#define NODEMANDS			0x00
#define PERSONNELDEMANDS	0x01
#define EQUIPMENTDEMANDS	0x02
#define LOCATIONDEMANDS		0x04
#define ALLDEMANDS			0x07

extern enum enumRecordState;

// if a new demand type is added for a demand that has a record in AZATAB
// then need to update omDemandsWithAzatabRecords in the constructor
#define NUM_DEMAND_TYPES 9 // note: if this increases above 10 then need to change all *prlDemand->Dety lines
#define TURNAROUND_DEMAND '0'
#define INBOUND_DEMAND '1'
#define OUTBOUND_DEMAND '2'
#define P05_DEMAND '3'
#define P05RES_DEMAND '4'
#define CAS_DEMAND '5'
#define	CCI_DEMAND			'6'
#define	FID_DEMAND			'7'
#define MANUAL_THIRDPARTY_DEMAND '8'


#define ACRURNO "ACR.URNO"
#define AFTDEM "AFT"

// should be not longer than 20.000
#define CDD_SELECTIONLEN 3000
/////////////////////////////////////////////////////////////////////////////
// Record structure declaration
#define CEDADEMANDDATA_FLGSLEN 11
#define CEDADEMANDDATA_FLGS_NOFLIGHT 0
#define CEDADEMANDDATA_FLGS_EXPIRED 1
#define CEDADEMANDDATA_FLGS_NOAUTOASS 5
#define CEDADEMANDDATA_FLGS_MANUALLYINSERTED 6
#define CEDADEMANDDATA_FLGS_DEACTIVATED 7

struct DemandDataStruct {
    long    Urno;           // Unique Record Number in DEMCKI
    long    Ouri;           // Inbound Flight URNO
    long    Ouro;           // Outbound Flight URNO
	char	Obty[11];		// 
    char    Alid[6];        // ID of Allocation Unit eg GateName,CicName,Regn,Pos
	char	Aloc[11];		// Allocation Unit Type eg 'GAT','CIC','REG','POS'
	long	Urud;			// URNO that links recs in DEMTAB, RPTTAB (functions) and RPQTAB (qualifications)

	long	Udgr;			// links a group of demands together from either DEMTAB.UDGR or RUDTAB.UDGR for P05 (DEMTAB.UDGR==AZATAB.URNO for P05)
	long	Uaza;			// URNO in AZATAB for P05, RES, CASP and 3rd Party demands

	long	Ucon;			// see note below (UCON NOTE)
    CTime   Debe;           // Begin of Demand
    CTime   Deen;           // End of Demand
	char	Dety[2];		// Duty Type 0=Turnaround/1=Arr/2=Dep (0+2 belong to Dep,1 belongs to Arr)
	char	Flgs[CEDADEMANDDATA_FLGSLEN];		// Flag string 
							// where [0] '0' = Ouri/Ouro contains a flight URNO but are disactivated
							// where [1] '1' = demand has been expired
							// where [5] '1' = demand is deactivated for automatic assignment
							// where [6] '1' = manually inserted demand (not changed by demhdl)
							// where [7] '1' = demand deactivate - not conflict checked/last to be assigned

	long	Uref;			// URNO to Aircraf Registration

	CTime	Cdat;			// Creation date
	char	Usec[33];		// Creator
	CTime	Lstu;			// Last Update
	char	Useu[33];		// Updater

	// non DB data
	char	Rety[4];		// FROM RUDTAB.RETY --> Type: where: "100"=Personnel "010"=Equipment "001"=Position
	long	Ulnk;			// FROM RUDTAB.ULNK --> used to link two demands together eg. a personnel demand may be linked to a location demand
	long	Urue;			// FROM RUDTAB.URUE --> Rule event URNO

	long	Utpl;			// URNO of template
	long	Uses;			// URNO linking an equipment demand to a personnel demand, so that when one is assigned so is the other (for fast links)
	long    Uprm;           // URNO of PRM request
	char    Filt[6];          // Filter for Handling Agents
	// additional local data
	int		IsChanged;
	int		ConflictType;	// Conflict Status
	int		ColorIndex;		// Index in global Color Table
	int		ConflictWeight;
	BOOL	ConflictConfirmed;

	DemandDataStruct(void)
	{
		Urno = 0L;
		Ouri = 0L;
		Ouro = 0L;
		strcpy(Alid,"");
		strcpy(Aloc,"");
		Urud = 0L;
		Udgr = 0L;
		Ucon = 0L;
		Debe = TIMENULL;
		Deen = TIMENULL;
		strcpy(Dety,"");
		strcpy(Flgs,"");
		Uref = 0L;
		strcpy(Rety,"");
		Ulnk = 0L;
		Urue = 0L;
		Uaza = 0L;
		Utpl = 0L;
		Uses = 0L;
		Uprm = 0L;
	}

	DemandDataStruct& DemandDataStruct::operator=(const DemandDataStruct& rhs)
	{
		if (&rhs != this)
		{
			Urno = rhs.Urno;
			Ouri = rhs.Ouri;
			Ouro = rhs.Ouro;
			strcpy(Obty,rhs.Obty);
			strcpy(Alid,rhs.Alid);
			strcpy(Aloc,rhs.Aloc);
			Urud = rhs.Urud;
			Udgr = rhs.Udgr;
			Uaza = rhs.Uaza;
			Ucon = rhs.Ucon;
			Debe = rhs.Debe;
			Deen = rhs.Deen;
			strcpy(Dety,rhs.Dety);
			strcpy(Flgs,rhs.Flgs);
			Uref = rhs.Uref;
			Cdat = rhs.Cdat;
			strcpy(Usec,rhs.Usec);
			Lstu = rhs.Lstu;
			strcpy(Useu,rhs.Useu);
			strcpy(Rety,rhs.Rety);
			Ulnk = rhs.Ulnk;
			Urue = rhs.Urue;
			Utpl = rhs.Utpl;
			Uses = rhs.Uses;
			Uprm = rhs.Uprm;
			IsChanged = rhs.IsChanged;
			ConflictType = rhs.ConflictType;
			ColorIndex = rhs.ColorIndex;
			ConflictWeight = rhs.ConflictWeight;
			ConflictConfirmed = rhs.ConflictConfirmed;
		}		
		return *this;
	}
};

// UCON NOTE: if a flight update is received the affects a demand which has started jobs
// then a new demand is inserted and the old one remains unchanged, in the new demand
// the field UCON contains the URNO of the original demand
typedef struct DemandDataStruct DEMANDDATA;
typedef struct DemandDataStruct *DEMANDDATAPTR;
CCSArray(DEMANDDATAPTR);    // For using CCSArrayType(DEMANDDATAPTR) as parameters


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaDemandData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;
    CMapPtrToPtr omRestrictedUrnoMap;
	CMapPtrToPtr omInboundFlightMap;
	CMapPtrToPtr omOutboundFlightMap;
	CMapPtrToPtr omUrefMap;
	CMapPtrToPtr omUsesMap;
	CMapPtrToPtr omUprmMap;
	CMapStringToPtr omAlidMap;
	CMapPtrToPtr omRestrictedInboundFlightMap;
	CMapPtrToPtr omRestrictedOutboundFlightMap;
    CCSPtrArray<DEMANDDATA> omData;
    CCSPtrArray<DEMANDDATA> omExtraData;
    CCSPtrArray<DEMANDDATA> omRestrictedData;
	CString GetTableName(void);
	char pcmFields[250];
	CMapPtrToPtr omUtplFilterMap;
	CString omDemandsWithAzatabRecords; // DEMTAB.DETY of those demand types that have an AZATAB record
	CString omTurnaroundDemandList; // list of which demands (DEM.DETY) are linked to both halves of a rotation, format "|XXX|YYY|ZZZ|"
	CTime omStartTime, omEndTime;
	CString omLoadFormat;
	bool bmIsSinApron1;  //Singapore
	bool bmIsSinApron2;  //Singapore

private:
	char clFieldList[1024];

// Operations

private:
	void CreateFieldList();

public:
    CedaDemandData();
    ~CedaDemandData();

	CString omWhere; // used to load JODs witht the same selection condition as the demands

    bool ReadAllDemands(CTime lpStartTime, CTime lpEndTime,const CDWordArray& ropTplUrnos);
	bool ReadDemands(char *pcpWhere, bool useRudHind = false, bool bpSendDdx = false, CDWordArray *popDemUrnosAdded = NULL);
	bool LoadDemandsByFlurAndUtpl(CCSPtrArray <DEMANDDATA> &ropDemands, long lpFlur, long lpUtpl);
	bool LoadDemandsForDelegatedFlightJob(CCSPtrArray <DEMANDDATA> &ropDemands, CString opUdemList);

	bool InsertDemand(DEMANDDATA *prpDemand);
	bool AddDemandInternal(DEMANDDATA *prpDemand);
	bool DeleteDemand(DEMANDDATA *prpDemand);
	void DeleteDemandInternal(DEMANDDATA *prpDemand);
	void AddDemandToInboundMap(DEMANDDATA *prpDemand);
	void DeleteDemandFromInboundMap(long lpFlur, long lpDemUrno);
	void AddDemandToOutboundMap(DEMANDDATA *prpDemand);
	void DeleteDemandFromOutboundMap(long lpFlur, long lpDemUrno);
	void AddRestrictedDemandToInboundMap(DEMANDDATA *prpDemand);
	void DeleteRestrictedDemandFromInboundMap(long lpFlur, long lpDemUrno);
	void AddRestrictedDemandToOutboundMap(DEMANDDATA *prpDemand);
	void DeleteRestrictedDemandFromOutboundMap(long lpFlur, long lpDemUrno);
	bool AddRestrictedDemandInternal(DEMANDDATA *prpDemand);
	DEMANDDATA *GetRestrictedDemandByUrno(long lpUrno);
	
	void AddDemandToUrefMap(DEMANDDATA *prpDemand);
	void DeleteDemandFromUrefMap(long lpUref, long lpDemUrno);
	void AddDemandToUprmMap(DEMANDDATA *prpDemand);
	void DeleteDemandFromUprmMap(long lpUref, long lpDemUrno);
	void AddDemandToUsesMap(DEMANDDATA *prpDemand);
	void DeleteDemandFromUsesMap(long lpUses, long lpDemUrno);
	void AddDemandToAlidMap(DEMANDDATA *prpDemand);

	void DeleteDemandFromAlidMap(CString opAlid, long lpDemUrno);
    bool GetInboundDemandsByInboundFlight(CCSPtrArray <DEMANDDATA> &ropDemands, long lpFlur,const char *pcpAlid = "",const char *pcpAloc = "", int ipDemandType = ALLDEMANDS);
    bool GetDemandsByInboundFlight(CCSPtrArray <DEMANDDATA> &ropDemands, long lpFlur,const char *pcpAlid = "",const char *pcpAloc = "", int ipDemandType = ALLDEMANDS);
    bool GetDemandsByOutboundFlight(CCSPtrArray <DEMANDDATA> &ropDemands, long lpFlur,const char *pcpAlid = "",const char *pcpAloc = "", int ipDemandType = ALLDEMANDS);
    bool GetRestrictedDemandsByInboundFlight(CCSPtrArray <DEMANDDATA> &ropDemands, long lpFlur,const char *pcpAlid = "",const char *pcpAloc = "", int ipDemandType = ALLDEMANDS);
    bool GetRestrictedDemandsByOutboundFlight(CCSPtrArray <DEMANDDATA> &ropDemands, long lpFlur,const char *pcpAlid = "",const char *pcpAloc = "", int ipDemandType = ALLDEMANDS);
	bool GetDemandsByUref(CCSPtrArray <DEMANDDATA> &ropDemands, long lpUref, CString opObty);
	bool GetDemandsByObty(CCSPtrArray <DEMANDDATA> &ropDemands, CString opObty, bool bpReset = true);
	bool GetDemandsByUprm(CCSPtrArray <DEMANDDATA> &ropDemands, long lpUprm, bool bpReset = true);
	void GetTeamDemands(DEMANDDATA *prlDemand,CCSPtrArray<DEMANDDATA> &ropTeamDemands);
	void GetOverallTeamTimeForDemand(DEMANDDATA *prpDemand,CTime &ropStart, CTime &ropEnd);
	void GetOverallTeamTimeForDemand(DEMANDDATA *prpDemand,CTime &ropStart, CTime &ropEnd, CCSPtrArray <DEMANDDATA> &ropTeamDemands);
	bool GetDemandsByRotationFlur(CCSPtrArray<DEMANDDATA> &ropDemands,long lpInboundFlur,long lpOutboundFlur,const char *pcpAlid = "" ,const char *pcpAloc = "",int ipDemandType = ALLDEMANDS);
	bool GetDemandsByFlur(CCSPtrArray<DEMANDDATA> &ropDemands,long lpFlur,const char *pcpAlid = "" ,const char *pcpAloc = "",int ipDemandType = ALLDEMANDS, int ipReturnFlightType = ID_NOT_RETURNFLIGHT);
	bool GetDemandsByFlurAndUrue(CCSPtrArray <DEMANDDATA> &ropDemands,long lpFlur,long lpUrue, const char *pcpAlid ="", const char *pcpAloc ="",int ipDemandType = ALLDEMANDS);
	DEMANDDATA *GetDemandByFlurAndUrud(long lpOuri, long lpOuro, long lpUrud);
	bool GetDemandsByAlid(CCSPtrArray <DEMANDDATA> &ropDemands,const char *pcpAlid = "",const char *pcpAloc = "",int ipDemandType = ALLDEMANDS);
	bool DemandIsOfType(DEMANDDATA *prpDemand, int ipDemandType);
	CString GetDemandTypeString(DEMANDDATA *prpDemand);
	int GetDemandType(DEMANDDATA *prpDemand);
	bool HasPrmDemands(long lpFlur);

	int		GetCountOfDemandTypes();
	CString GetStringForDemandType(DEMANDDATA &ropDemand);
	CString GetStringForDemandType(const char *pcpDemandType);
	CString GetStringForDemandType(char cpDemandType);
	CString GetDemandTypeFromString(const CString& opDemandTypeString);
	CString GetStringForResourceType(DEMANDDATA &ropDemand);
	CString GetStringForDemandAndResourceType(DEMANDDATA &ropDemand);

	//void GetFlightIndependentDemand(CCSPtrArray<DEMANDDATA> &ropDemands);
	DEMANDDATA *GetDemandByUrno(long lpUrno);
	DEMANDDATA *GetMatchingDemandByUses(DEMANDDATA *prpDemand);
	bool ClearAll();

	void ProcessDemandInsert(void *vpDataPointer);
	void ProcessDemandInsert(char *pcpFields, char *pcpData);
	void ProcessDemandUpdate(void *vpDataPointer);
	bool ProcessDemandUpdate(long lpDemandUrno, char *pcpFields, char *pcpData);
	void ProcessDemandDelete(void *vpDataPointer);
	void ProcessDemandDelete(long llDemUrno);
	void ProcessDemandSBC(void *vpDataPointer);
	void ReloadDemands(const char *pcpDemandTypes);
	void ProcessDemandUPD(void *vpDataPointer);
	void ReReadDemands(CString &ropDemUrnoList);
	bool ProcessAttachment(CString &ropAttachment);
	bool IsCompleteFieldList(const char *pcpFields, int ipNumFieldsRequired);

	void SendAllDdxs();
	bool bmDontSendDdx;
	CCSPtrArray <DEMANDDATA> omInsertDdxs;
	CCSPtrArray <DEMANDDATA> omUpdateDdxs;

	void ConvertDatesToUtc(DEMANDDATA *prpDemand);
	void ConvertDatesToLocal(DEMANDDATA *prpDemand);
	long GetFlightUrno(DEMANDDATA *prpDemand);
	bool FlightEnabled(DEMANDDATA *prpDemand);
	bool RuleIndependant(DEMANDDATA *prpDemand);
	bool AutomaticAssignmentEnabled(DEMANDDATA *prpDemand);
	bool EnableAutomaticAssignment(DEMANDDATA *prpDemand,bool bpEnable);
	bool SetFlgs(DEMANDDATA *prpDemand, char cpValue, int ipIndex);


	void LinkCheckinDemands(void);
	void LinkCheckinDemand(DEMANDDATA *prpDemand, bool bpReleaseNow = false);
	CString omUdemAlidList; // list of urno/alid combinations to be released
	bool AssignAlid(long lpUdem, const char *pcpAlid, bool bpReleaseNow=false);
	bool ReleaseAlidAssignments(void);
	bool HasExpired(DEMANDDATA *prpDemand);
	CString Dump(long lpUrno);
	void GetFlightIndependentDemands(CCSPtrArray<DEMANDDATA>& ropFIDs);
	bool IsTeamDemand(DEMANDDATA *prpDemand);
	int	 GetFlightsFromCciDemand(DEMANDDATA *prpDemand,CDWordArray& ropFlights);
	int	 GetFlightsFromCciDemand(CDWordArray &ropDemands, CStringArray &ropFlights);
	void PrepareDataAfterRead(DEMANDDATA *prpDemand);
	void PrepareDataForWrite(DEMANDDATA *prpDemand, const char *pcpText);
	bool HasAzatabRecord(DEMANDDATA *prpDemand);
	bool IsTurnaroundDemand(DEMANDDATA *prpDemand);
	bool IsTurnaroundDemand(const char *pcpDety);
	bool IsFlightDependingDemand(DEMANDDATA *prpDemand);
	bool IsFlightDependingDemand(const char *pcpDety);
	bool DemandIsDeactivated(long lpDemUrno);
	bool DemandIsDeactivated(DEMANDDATA *prpDemand);
	bool DeactivateDemand(DEMANDDATA *prpDemand, bool bpDeactivate);
	bool DeactivateDemand(long lpDemUrno, bool bpDeactivate);

	void CheckForMissingFlights(void);
	CMapPtrToPtr omMissingFlights;
	CUIntArray omDemandsWhichHaveMissingFlights; // used for TRACE only
	void CheckForMissingFlight(DEMANDDATA *prpDemand, const char *pcpText = NULL, bool bpReadMissingFlight = false);
	void ReadMissingFlights(const char *pcpText);
	CMapPtrToPtr omMissingJobs;
	void CheckForMissingJob(DEMANDDATA *prpDemand, const char *pcpText = NULL,  bool bpReadMissingJob = false);
	void ReadMissingJobs(const char *pcpText);
	BOOL GetLoadFormat();
	void CheckForMissingFlightDemands(CDWordArray &ropFlightUrnos);

	bool FlightHasDemands(long lpFlur);

	bool DemandHasCorrectTemplate(DEMANDDATA *prpDemand);
	bool DemandHasFullFieldList(DEMANDDATA *prpDemand);
	CString omFlightDemand; // set to "012" and used to search for flight demands
	bool DemandHasDelegatedFlight(long lpUdem);
	bool ReadDemandsForRestrictedJobs(char *pcpWhere , bool useRudHind /*= false*/);
};

#endif
