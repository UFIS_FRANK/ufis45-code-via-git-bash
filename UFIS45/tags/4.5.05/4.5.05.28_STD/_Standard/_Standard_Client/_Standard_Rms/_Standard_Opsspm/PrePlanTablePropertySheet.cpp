// preplnps.cpp : implementation file
//

// These following include files are project dependent
#include <stdafx.h>
#include <OpssPm.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <CedaAloData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaEmpData.h>
#include <CedaRnkData.h>
#include <CedaPerData.h>
#include <CedaShiftData.h>
#include <CedaShiftTypeData.h>

// These following include files are necessary
#include <cviewer.h>
#include <BasicData.h>
#include <FilterPage.h>
//#include "ZeitPage.h"
#include <PrePlanTableSortPage.h>
#include <BasePropertySheet.h>
#include <PrePlanTablePropertySheet.h>
#include <CedaWgpData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PrePlanTablePropertySheet
//

//PrePlanTablePropertySheet::PrePlanTablePropertySheet(CWnd* pParentWnd,
//	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
//	(ID_SHEET_PREPLAN_TABLE, pParentWnd, popViewer, iSelectPage)
PrePlanTablePropertySheet::PrePlanTablePropertySheet(CWnd* pParentWnd,
	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
	(GetString(IDS_STRING61590), pParentWnd, popViewer, iSelectPage)
{
	AddPage(&m_pageRank);
	AddPage(&m_pagePermits);
	AddPage(&m_pageShiftCode);
	AddPage(&m_pageTeam);
	AddPage(&m_pageSort);
//	AddPage(&m_pageZeit);

	// Change the caption in tab control of the PropertySheet
	//m_pageRank.SetCaption(ID_PAGE_FILTER_RANK);
	m_pageRank.SetCaption(GetString(IDS_STRING61604));
	m_pagePermits.SetCaption(GetString(IDS_PERMIT_TAB));
	//m_pageShiftCode.SetCaption(ID_PAGE_FILTER_SHIFTCODE);
	m_pageShiftCode.SetCaption(GetString(IDS_STRING61605));
	//m_pageTeam.SetCaption(ID_PAGE_FILTER_TEAM);
	m_pageTeam.SetCaption(GetString(IDS_STRING61606));

	// Prepare possible values for each PropertyPage
	ogRnkData.GetAllRanks(m_pageRank.omPossibleItems);
	m_pageRank.bmSelectAllEnabled = true;
	ogPerData.GetAllPermits(m_pagePermits.omPossibleItems);
	m_pagePermits.bmSelectAllEnabled = true;
	ogShiftTypes.GetAllShiftTypes(m_pageShiftCode.omPossibleItems);
	m_pageShiftCode.bmSelectAllEnabled = true;
	ogWgpData.GetAllTeams(m_pageTeam.omPossibleItems);
	m_pageTeam.omPossibleItems.Add(GetString(IDS_NOWORKGROUP));
	m_pageTeam.bmSelectAllEnabled = true;
}

void PrePlanTablePropertySheet::LoadDataFromViewer()
{
	pomViewer->GetFilter("Dienstrang", m_pageRank.omSelectedItems);
	pomViewer->GetFilter("Permits", m_pagePermits.omSelectedItems);
	pomViewer->GetFilter("Schichtcode", m_pageShiftCode.omSelectedItems);
	pomViewer->GetFilter("Team", m_pageTeam.omSelectedItems);
	pomViewer->GetSort(m_pageSort.omSortOrders);
	m_pageSort.m_Group = pomViewer->GetGroup()[0] - '0';
//	pomViewer->GetFilter("Zeit", m_pageZeit.omFilterValues);
}

void PrePlanTablePropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{
	pomViewer->SetFilter("Dienstrang", m_pageRank.omSelectedItems);
	pomViewer->SetFilter("Permits", m_pagePermits.omSelectedItems);
	pomViewer->SetFilter("Schichtcode", m_pageShiftCode.omSelectedItems);
	pomViewer->SetFilter("Team", m_pageTeam.omSelectedItems);
	pomViewer->SetSort(m_pageSort.omSortOrders);
	pomViewer->SetGroup(CString(m_pageSort.m_Group + '0'));
//	pomViewer->SetFilter("Zeit", m_pageZeit.omFilterValues);
	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}
}

int PrePlanTablePropertySheet::QueryForDiscardChanges()
{
	CStringArray olSelectedRanks;
	CStringArray olSelectedPermits;
	CStringArray olSelectedShifts;
	CStringArray olSelectedTeams;
	CStringArray olSortOrders;
	CString olGroupBy;
	CStringArray olSelectedTime;
	pomViewer->GetFilter("Dienstrang", olSelectedRanks);
	pomViewer->GetFilter("Permits", olSelectedPermits);
	pomViewer->GetFilter("Schichtcode", olSelectedShifts);
	pomViewer->GetFilter("Team", olSelectedTeams);
	pomViewer->GetSort(olSortOrders);
	olGroupBy = pomViewer->GetGroup();
//	pomViewer->GetFilter("Zeit", olSelectedTime);

//	if (!IsIdentical(olSelectedRanks, m_pageRank.omSelectedItems) ||
//		!IsIdentical(olSelectedShifts, m_pageShiftCode.omSelectedItems) ||
//		!IsIdentical(olSelectedTeams, m_pageTeam.omSelectedItems) ||
//		!IsIdentical(olSortOrders, m_pageSort.omSortOrders) ||
//		olGroupBy != CString(m_pageSort.m_Group + '0') ||
//		!IsIdentical(olSelectedTime, m_pageZeit.omFilterValues))
	if (!IsIdentical(olSelectedRanks, m_pageRank.omSelectedItems) ||
		!IsIdentical(olSelectedPermits, m_pagePermits.omSelectedItems) ||
		!IsIdentical(olSelectedShifts, m_pageShiftCode.omSelectedItems) ||
		!IsIdentical(olSelectedTeams, m_pageTeam.omSelectedItems) ||
		!IsIdentical(olSortOrders, m_pageSort.omSortOrders) ||
		olGroupBy != CString(m_pageSort.m_Group + '0'))
	{
		// Discard changes ?
		return MessageBox(GetString(IDS_STRING61583), NULL, MB_ICONQUESTION | MB_OKCANCEL);
	}

	return IDOK;
}
