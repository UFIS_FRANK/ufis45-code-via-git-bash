#ifndef __CCITBLVW_H__
#define __CCITBLVW_H__

#include <CCSCedaData.h>
#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <AllocData.h>
#include <CedaJobData.h>

#include <cviewer.h>
#include <table.h>
#include <ccsprint.h>

struct CCIDATA_JOB
{
	long	Urno;
	CString Peno;
	CString Name;
	CTime	Acfr;
	CTime	Acto;
};

struct CCIDATA_LINE
{
	CString	Alid;
	CString GroupName;
	CString DeskType;
	CCSPtrArray<CCIDATA_JOB> JobData;
};

class CciTableViewer: public CViewer
{
// Constructions
public:
			CciTableViewer();
    ~		CciTableViewer();

    void	Attach(CTable *popAttachWnd);
	void	ChangeViewTo(const char *pcpViewName, CString opDate);
    void	ChangeViewTo(const char *pcpViewName);
	CTime	StartTime() const;
	CTime	EndTime() const;
	int		Lines() const;
	CCIDATA_LINE*	GetLine(int ipLine);
	void	SetStartTime(CTime opStartTime);
	void	SetEndTime(CTime opStartTime);
private:
	void	PrepareGrouping();
    void	PrepareFilter();
	void	PrepareSorter();
	int		CompareCci(CCIDATA_LINE *prpCci1, CCIDATA_LINE *prpCci2);
	BOOL	IsPassFilter(const char *prpArea,const char *prpHall,const char *prpRegion,const char *prpLine);
	BOOL	IsSameGroup(CCIDATA_LINE *prpLine1, CCIDATA_LINE *prpLine2);

	void	MakeLines();
	void	MakeLines(const CString& ropGroup);
	void	MakeLinesWhenGroupByTerminal(const CString& ropGroupName);
	void	MakeLinesWhenGroupByHall(const CString& ropGroupName);
	void	MakeLinesWhenGroupByRegion(const CString& ropGroupName);
	void	MakeLinesWhenGroupByLine(const CString& ropGroupName);

	void	MakeLine(ALLOCUNIT *prpCicGroup);
	void	MakeLine(const CString& ropCicName,const CString& ropGroupName,const CString& ropDeskType);
	BOOL	FindDesk(const CString &ropDesk, int &rilLineno);
	BOOL	FindAssignment(long lpJobUrno, int &ripLineno, int &ripAssignmentno);

	void	DeleteAll();
	int		CreateLine(CCIDATA_LINE *prpCci);
	int		CreateAssignment(int ipLineno, CCIDATA_JOB *prpAssignment);
	void	DeleteAssignment(int ipLineno, int ipAssignmentno);

	void	UpdateDisplay();
	CString Format(CCIDATA_LINE *prpLine);

// Attributes used for filtering condition
private:
    int				omGroupBy;              
    CWordArray		omSortOrder;
	CMapStringToPtr omCMapForAlid;
	CMapStringToPtr omCMapForTerminal;
	CMapStringToPtr omCMapForHall;
	CMapStringToPtr omCMapForRegion;
	CMapStringToPtr	omCMapForLine;
	

// Attributes
private:
    CTable*						pomTable;
    CCSPtrArray<CCIDATA_LINE>	omLines;
    CString						omDate;
	CTime						omStartTime;
	CTime						omEndTime;
	CCSPrint*					pomPrint;

// Methods which handle changes (from Data Distributor)
private:
	static void CciTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
	BOOL ProcessJobNew(JOBDATA *prpJob);
	BOOL ProcessJobDelete(JOBDATA *prpJob);

////////////////////////////////////////////////////////////////////////////////////////////
// Printing functions

private:
	BOOL	PrintCCILine(CCIDATA_LINE *prpLine,BOOL bpIsLastLine);
	BOOL	PrintCCIHeader(CCSPrint *pomPrint);
public:
	void	PrintView();


};

#endif //__CCITBLVW_H__
