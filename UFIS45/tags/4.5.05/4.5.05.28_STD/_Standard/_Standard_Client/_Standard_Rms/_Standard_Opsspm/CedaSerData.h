//
// CedaSerData.h - rule demands - contains extra info about demands
//
#ifndef _CEDASERDATA_H_
#define _CEDASERDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct SerDataStruct
{
	long	Urno;			// the unique record number
	char	Snam[41];		// the service name
	char	Seco[10];		// the service code
	char	Dtyp[4];
	char	Frmt[4];
	char	Prmt[16];
	BOOL	Ffis;			// is flight independent service ?
	int		Sdut;
	int		Swtt;
	int		Swtf;
	int		Ssdt;
	int		Ssut;

	SerDataStruct(void)
	{
		Urno = 0L;
		memset(Snam,0,sizeof(Snam));
		memset(Seco,0,sizeof(Seco));
		Sdut = 0;
		Swtt = 0;
		Swtf = 0;
		Ssdt = 0;
		Ssut = 0;
	}
};

typedef struct SerDataStruct SERDATA;
 
/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaSerData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <SERDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapStringToPtr omSnamMap;
	CString GetTableName(void);
	CString Dump(long lpUrno);

private:
	char clFieldList[1024];

// Operations
public:
	CedaSerData();
	~CedaSerData();

	bool ReadSerData();
	SERDATA *GetSerByUrno(long lpUrno);
	void GetAllServiceNames(CStringArray& ropNames,BOOL bpFfis);	
	void CedaSerData::GetAllServiceNames(CStringArray& ropNames);
	void GetAllServiceCodes(CStringArray& ropCodes,BOOL bpFfis);	
	int GetServiceDuration(long lpSerUrno);
	int GetServiceDuration(SERDATA *prpSer);
	SERDATA *GetSerBySnam(const char *pcpSnam);
private:
	void AddSerInternal(SERDATA &rrpSer);
	void ClearAll();
};


extern CedaSerData ogSerData;
#endif _CEDASERDATA_H_
