// Class for Pstes
#ifndef _CEDAPSTDATA_H_
#define _CEDAPSTDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct PstDataStruct
{
	long	Urno;		// Unique Record Number
	char	Pnam[6];	// Pste Name
	CTime	Vafr;		// Valid from
	CTime	Vato;		// Valid to
	CTime	Nafr;		// Not available from
	CTime	Nato;		// Not available to

	PstDataStruct(void)
	{
		Urno = 0L;
		strcpy(Pnam,"");
		Vafr = TIMENULL;
		Vato = TIMENULL;
		Nafr = TIMENULL;
		Nato = TIMENULL;
	}
};

typedef struct PstDataStruct PSTDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaPstData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <PSTDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapStringToPtr omNameMap;
	CString GetTableName(void);

// Operations
public:
	CedaPstData();
	~CedaPstData();

	CCSReturnCode ReadPstData();
	PSTDATA* GetPstByUrno(long lpUrno);
	PSTDATA* GetPstByName(const char *pcpName);
	long GetPstUrnoByName(const char *pcpName);
	void GetAllPstNames(CStringArray &ropPsteNames);

private:
	void AddPstInternal(PSTDATA &rrpPst);
	void ClearAll();
};


extern CedaPstData ogPstData;
#endif _CEDAPSTDATA_H_
