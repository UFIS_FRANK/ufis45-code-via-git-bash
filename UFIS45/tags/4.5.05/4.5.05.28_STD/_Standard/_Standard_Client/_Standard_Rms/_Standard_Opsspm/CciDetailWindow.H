// CciDetailWindow.h : header file
//


#ifndef _CCIDW_
#define _CCIDW_

#include <CciDetailDlg.h>
#include <CciDetailViewer.h>
#include <CciDetailGantt.h>

#define CciData  CCIDATA

/////////////////////////////////////////////////////////////////////////////
// CciDetailWindow frame

class CciDetailWindow : public CFrameWnd
{
	DECLARE_DYNCREATE(CciDetailWindow)

protected:
    CciDetailWindow();

// Attributes
public:

// Operations
public:
	CciDetailWindow(CWnd *popParent,const char *pcpAlocAlid);
	virtual ~CciDetailWindow();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CciDetailWindow)
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CciDetailWindow)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

protected:
	char cmAlocAlid[24];

//	CCSPtrArray<JOBDATA> *pomJobs;

    CciDetailDialog omCciDetail;
    CTimeScale omTimeScale;
    CStatusBar omStatusBar;

    
    int imStartTimeScalePos;
    int imBottomPos;

    CTime omTSStartTime;
    CTimeSpan omTSDuration;
    CTimeSpan omTSInterval;

	
	CciDetailViewer omViewer;
public:
	CciDetailGantt omGantt;

private:      
    static CPoint omMaxTrackSize;
    static CPoint omMinTrackSize;
};

/////////////////////////////////////////////////////////////////////////////

#endif // _CCIDW_

