// Employee Groups (Teams)
#ifndef _CEDAREMDATA_H_
#define _CEDAREMDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration
#define REMDATA_TEXTLEN 256

struct RemDataStruct
{
	long	Urno;							// Unique Record Number
	char	Appl[9];						// Application
	char	Purp[129];						// purpose
	char	Rtab[4];						// reference table
	long	Rurn;							// URNO of reference table
	char	Text[REMDATA_TEXTLEN+1];		// text of the remark

	char    Usec[33];						// Creator
    CTime   Cdat;							// Creation Date
    char    Useu[33];						// Updater
    CTime   Lstu;							// Update Date

	RemDataStruct(void)
	{
		Urno = 0L;
		strcpy(Appl,"");
		strcpy(Purp,"");
		strcpy(Rtab,"");
		Rurn = 0L;
		strcpy(Text,"");
		strcpy(Usec,"");
		Cdat = TIMENULL;
		strcpy(Useu,"");
		Lstu = TIMENULL;
	}

	RemDataStruct& RemDataStruct::operator=(const RemDataStruct& rhs)
	{
		if (&rhs != this)
		{
			Urno = rhs.Urno;
			strcpy(Appl,rhs.Appl);
			strcpy(Purp,rhs.Purp);
			strcpy(Rtab,rhs.Rtab);
			Rurn = rhs.Rurn;
			strcpy(Text,rhs.Text);
			strcpy(Usec,rhs.Usec);
			Cdat = rhs.Cdat;
			strcpy(Useu,rhs.Useu);
			Lstu = rhs.Lstu;
		}		
		return *this;
	}
};

typedef struct RemDataStruct REMDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaRemData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <REMDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omRurnMap;

// Operations
public:
	CedaRemData();
	~CedaRemData();

	REMDATA *GetRemByUrno(long lpUrno);
	REMDATA *GetRemByRurn(long lpRurn);
	CString GetRemarkByRurn(long lpRurn);

	void ProcessRemBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	CCSReturnCode ReadRemData();
	CString GetTableName(void);
	CCSReturnCode UpdateRemRecord(REMDATA *prpRemData, REMDATA *prpOldRemData);
	CCSReturnCode DeleteRemRecord(long lpUrno);
	REMDATA *InsertRemRecord(long lpRurn, const char *pcpText, const char *pcpRtab, const char *pcpPurp);

private:
	void ClearAll();
	REMDATA *AddRemInternal(REMDATA &rrpRem);
	void DeleteRemInternal(long lpUrno);
	void PrepareDataAfterRead(REMDATA *prpRem);
	void PrepareDataForWrite(REMDATA *prpRem);
	void ConvertDatesToUtc(REMDATA *prpRem);
	void ConvertDatesToLocal(REMDATA *prpRem);
	bool GetChangedFields(REMDATA *prpRem, REMDATA *prpOldRem, char *pcpFieldList, char *pcpData);
};


extern CedaRemData ogRemData;
#endif _CEDAREMDATA_H_
