// RULE DEMANDS - needed because it contains extra info about demands
// CedaSerData.cpp
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaSerData.h>
#include <BasicData.h>

CedaSerData::CedaSerData()
{                  
    BEGIN_CEDARECINFO(SERDATA, SerDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Snam,"SNAM") 
		FIELD_CHAR_TRIM(Seco,"SECO")
		FIELD_INT(Ffis,"FFIS")
		FIELD_INT(Sdut,"SDUT")
		FIELD_INT(Swtt,"SWTT")
		FIELD_INT(Swtf,"SWTF")
		FIELD_INT(Ssdt,"SSDT")
		FIELD_INT(Ssut,"SSUT")
		FIELD_CHAR_TRIM(Dtyp,"DTYP") 
		FIELD_CHAR_TRIM(Frmt,"FRMT") 
		FIELD_CHAR_TRIM(Prmt,"PRMT") 
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(SerDataRecInfo)/sizeof(SerDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SerDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"SERTAB");

	strcpy(clFieldList,"URNO,SNAM,SECO,FFIS,SDUT,SWTT,SWTF,SSDT,SSUT");
	pcmFieldList = clFieldList; 

}
 
CedaSerData::~CedaSerData()
{
	TRACE("CedaSerData::~CedaSerData called\n");
	ClearAll();
}

void CedaSerData::ClearAll()
{
	omSnamMap.RemoveAll();
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
}

CCSReturnCode CedaSerData::ReadSerData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512] = "";



	if (bgIsPrm)
	{
		// lli: currently SATS loads all 
		// lli: load PRM-LOADALLSER setting from ceda.ini, default is loading only FRMT != ' '
		char pclTmpText[512];
		char pclConfigPath[512];
		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));

		GetPrivateProfileString(pcgAppName, "PRM-LOADALLSER",  "NO",pclTmpText, sizeof pclTmpText, pclConfigPath);
		if (!strcmp(pclTmpText,"NO")) {
			strcpy(pclWhere," WHERE FRMT != ' ' ");
		}
		strcpy(clFieldList,"URNO,SNAM,SECO,FFIS,SDUT,SWTT,SWTF,SSDT,SSUT,DTYP,FRMT,PRMT");
	}
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaSerData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		SERDATA rlSerData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlSerData)) == RCSuccess)
			{
				AddSerInternal(rlSerData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaSerData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(SERDATA), pclWhere);
    return ilRc;
}


void CedaSerData::AddSerInternal(SERDATA &rrpSer)
{
	SERDATA *prlSer = new SERDATA;
	*prlSer = rrpSer;
	omData.Add(prlSer);
	omUrnoMap.SetAt((void *)prlSer->Urno,prlSer);
	omSnamMap.SetAt(prlSer->Snam, prlSer);
}


SERDATA *CedaSerData::GetSerByUrno(long lpUrno)
{
	SERDATA *prlSer = NULL;
	omUrnoMap.Lookup((void *)lpUrno,(void *& )prlSer);
	return prlSer;
}

CString CedaSerData::GetTableName(void)
{
	return CString(pcmTableName);
}

CString CedaSerData::Dump(long lpUrno)
{
	CString olDumpStr;
	SERDATA *prlSer = GetSerByUrno(lpUrno);
	if(prlSer == NULL)
	{
		olDumpStr.Format("No SERDATA Found for SER.URNO <%ld>",lpUrno);
	}
	else
	{
		CString olData;
		MakeCedaData(&omRecInfo,olData,prlSer);
		CStringArray olFieldArray, olDataArray;
		ogBasicData.ExtractItemList(pcmFieldList, &olFieldArray);
		ogBasicData.ExtractItemList(olData, &olDataArray);
		int ilNumFields = olFieldArray.GetSize();
		int ilNumData = olDataArray.GetSize();
		if(ilNumFields != ilNumData)
		{
			olDumpStr.Format("<%s>\n<%s>\n",pcmFieldList,olData);
		}
		else
		{
			for(int ilD = 0; ilD < ilNumData; ilD++)
			{
				olDumpStr += olFieldArray[ilD] + " = <" + olDataArray[ilD] + ">    ";
			}
		}
	}
	return olDumpStr;
}

void CedaSerData::GetAllServiceNames(CStringArray& ropNames,BOOL bpFfis)
{
	for (int i = 0; i < omData.GetSize(); i++)
	{
		SERDATA *prlSer = &omData[i];
		if (prlSer && prlSer->Ffis == bpFfis)
			ropNames.Add(prlSer->Snam);
	}
}

void CedaSerData::GetAllServiceNames(CStringArray& ropNames)
{
	for (int i = 0; i < omData.GetSize(); i++)
	{
		SERDATA *prlSer = &omData[i];
		ropNames.Add(prlSer->Snam);
	}
}
void CedaSerData::GetAllServiceCodes(CStringArray& ropNames,BOOL bpFfis)
{
	for (int i = 0; i < omData.GetSize(); i++)
	{
		SERDATA *prlSer = &omData[i];
		if (prlSer && prlSer->Ffis == bpFfis)
			ropNames.Add(prlSer->Seco);
	}
}

int CedaSerData::GetServiceDuration(long lpSerUrno)
{
	SERDATA *prlSer = GetSerByUrno(lpSerUrno);
	return GetServiceDuration(prlSer);
}

int CedaSerData::GetServiceDuration(SERDATA *prpSer)
{
	int ilDuration = 0;
	if(prpSer != NULL)
	{
		ilDuration = prpSer->Sdut + prpSer->Swtt + prpSer->Swtf + prpSer->Ssdt + prpSer->Ssut;
	}

	return ilDuration;
}

SERDATA *CedaSerData::GetSerBySnam(const char *pcpSnam)
{
	SERDATA *prlSer = NULL;
	omSnamMap.Lookup(pcpSnam, (void *&) prlSer);
	return prlSer;
}