// CedaRemData.cpp - Class for employee groups (teams)
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaRemData.h>
#include <BasicData.h>

void ProcessRemCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaRemData::CedaRemData()
{                  
    BEGIN_CEDARECINFO(REMDATA, RemDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Appl,"APPL")
		FIELD_CHAR_TRIM(Purp,"PURP")
		FIELD_CHAR_TRIM(Rtab,"RTAB")
		FIELD_LONG(Rurn,"RURN")
		FIELD_CHAR_TRIM(Text,"TEXT")
		FIELD_CHAR_TRIM(Usec,"USEC")
        FIELD_DATE(Cdat,"CDAT")
		FIELD_CHAR_TRIM(Useu,"USEU")
        FIELD_DATE(Lstu,"LSTU")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(RemDataRecInfo)/sizeof(RemDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&RemDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"REMTAB");
    pcmFieldList = "URNO,APPL,PURP,RTAB,RURN,TEXT,USEC,CDAT,USEU,LSTU";

	ogCCSDdx.Register((void *)this, BC_REM_CHANGE, CString("REMDATA"), CString("Rem changed"),ProcessRemCf);
	ogCCSDdx.Register((void *)this, BC_REM_DELETE, CString("REMDATA"), CString("Rem deleted"),ProcessRemCf);
}
 
CedaRemData::~CedaRemData()
{
	TRACE("CedaRemData::~CedaRemData called\n");
	ClearAll();
}


void  ProcessRemCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaRemData *)popInstance)->ProcessRemBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void CedaRemData::ProcessRemBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlRemData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlRemData);
	REMDATA rlRem;
	GetRecordFromItemList(&rlRem,prlRemData->Fields,prlRemData->Data);
	REMDATA *prlRem = GetRemByUrno(rlRem.Urno);
	
	//For GRAMS application filter 
	if(rlRem.Appl != pcgAppName)
	{
		return;
	}
	
	if(ipDDXType == BC_REM_CHANGE && prlRem != NULL)
	{
		// update
		GetRecordFromItemList(prlRem,prlRemData->Fields,prlRemData->Data);
		PrepareDataAfterRead(prlRem);
		ogCCSDdx.DataChanged((void *)this, REM_CHANGE, (void *)prlRem);
	}
	else if(ipDDXType == BC_REM_CHANGE && prlRem == NULL)
	{
		// insert
		prlRem = AddRemInternal(rlRem);
		ogCCSDdx.DataChanged((void *)this, REM_CHANGE, (void *)prlRem);
	}
	else if(ipDDXType == BC_REM_DELETE)
	{
		if(prlRem == NULL)
		{
			prlRem = GetRemByUrno(GetUrnoFromSelection(prlRemData->Selection));
		}
		if(prlRem != NULL)
		{
			DeleteRemInternal(prlRem->Urno);
			ogCCSDdx.DataChanged((void *)this, REM_DELETE, (void *)prlRem);
		}
	}
}


void CedaRemData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omRurnMap.RemoveAll();
	omData.DeleteAll();
}

CCSReturnCode CedaRemData::ReadRemData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];
    sprintf(pclWhere,"WHERE APPL='%s'",pcgAppName);
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaRemData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		REMDATA rlRemData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlRemData)) == RCSuccess)
			{
				AddRemInternal(rlRemData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaRemData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(REMDATA), pclWhere);
    return ilRc;
}

REMDATA *CedaRemData::AddRemInternal(REMDATA &rrpRem)
{
	REMDATA *prlRem = new REMDATA;
	*prlRem = rrpRem;
	omData.Add(prlRem);
	omUrnoMap.SetAt((void *)prlRem->Urno,prlRem);
	omRurnMap.SetAt((void *)prlRem->Rurn,prlRem);
	return prlRem;
}

void CedaRemData::DeleteRemInternal(long lpUrno)
{
	int ilNumRems = omData.GetSize();
	for(int ilRem = (ilNumRems-1); ilRem >= 0; ilRem--)
	{
		if(omData[ilRem].Urno == lpUrno)
		{
			omRurnMap.RemoveKey((void *)omData[ilRem].Rurn);
			omData.DeleteAt(ilRem);
		}
	}
	omUrnoMap.RemoveKey((void *)lpUrno);
}

REMDATA *CedaRemData::GetRemByUrno(long lpUrno)
{
	REMDATA *prlRem = NULL;
	omUrnoMap.Lookup((void *)lpUrno,(void *& )prlRem);
	return prlRem;
}

REMDATA *CedaRemData::GetRemByRurn(long lpRurn)
{
	REMDATA *prlRem = NULL;
	omRurnMap.Lookup((void *)lpRurn,(void *& )prlRem);
	return prlRem;
}

CString CedaRemData::GetRemarkByRurn(long lpRurn)
{
	CString olRemark;
	REMDATA *prlRem = GetRemByRurn(lpRurn);
	if(prlRem != NULL)
	{
		olRemark = prlRem->Text;
	}
	return olRemark;
}

CString CedaRemData::GetTableName(void)
{
	return CString(pcmTableName);
}

void CedaRemData::PrepareDataAfterRead(REMDATA *prpRem)
{
	ConvertDatesToLocal(prpRem);
}

void CedaRemData::PrepareDataForWrite(REMDATA *prpRem)
{
	// set lstu/cdat etc
	CTime olCurrTime = ogBasicData.GetTime();
	CString olUserText = ogUsername;

	prpRem->Lstu = olCurrTime;
	strcpy(prpRem->Useu,olUserText.Left(31));

	if(prpRem->Cdat == TIMENULL || strlen(prpRem->Usec) <= 0)
	{
		prpRem->Cdat = olCurrTime;
		strcpy(prpRem->Usec,olUserText.Left(31));
	}

	ConvertDatesToUtc(prpRem);
}

void CedaRemData::ConvertDatesToUtc(REMDATA *prpRem)
{
	ogBasicData.ConvertDateToUtc(prpRem->Cdat);
	ogBasicData.ConvertDateToUtc(prpRem->Lstu);
}

void CedaRemData::ConvertDatesToLocal(REMDATA *prpRem)
{
	ogBasicData.ConvertDateToLocal(prpRem->Cdat);
	ogBasicData.ConvertDateToLocal(prpRem->Lstu);
}

// compare prpRem with prpOldRem and return only the fields (pcpFieldList) and data (pcpData) which have changed
bool CedaRemData::GetChangedFields(REMDATA *prpRem, REMDATA *prpOldRem, char *pcpFieldList, char *pcpData)
{
	bool blChangesFound = false;

	memset(pcpFieldList,0,sizeof(pcpFieldList));
	memset(pcpData,0,sizeof(pcpData));

	if(prpRem != NULL && prpOldRem != NULL)
	{
		REMDATA rlRem;
		CString olData;

		memcpy(&rlRem,prpRem,sizeof(REMDATA));
		MakeCedaData(&omRecInfo,olData,&rlRem);
		CStringArray olNewFieldArray, olNewDataArray;
		ogBasicData.ExtractItemList(pcmFieldList, &olNewFieldArray);
		ogBasicData.ExtractItemList(olData, &olNewDataArray);
		int ilNumNewFields = olNewFieldArray.GetSize();
		int ilNumNewData = olNewDataArray.GetSize();

		memcpy(&rlRem,prpOldRem,sizeof(REMDATA));
		MakeCedaData(&omRecInfo,olData,&rlRem);
		CStringArray olOldDataArray;
		ogBasicData.ExtractItemList(olData, &olOldDataArray);
		int ilNumOldData = olOldDataArray.GetSize();

		bool blListsAreEmpty = true;
		if(ilNumNewFields == ilNumNewData && ilNumNewData == ilNumOldData)
		{
			for(int ilD = 0; ilD < ilNumNewData; ilD++)
			{
				// check if the data item has changed - always write LSTU and USEU whether they change or not
				if(olNewDataArray[ilD] != olOldDataArray[ilD] || olNewFieldArray[ilD] == "LSTU" || olNewFieldArray[ilD] == "USEU")
				{
					CString olOldData = olOldDataArray[ilD];
					CString olNewData = olNewDataArray[ilD];
					if(blListsAreEmpty)
					{
						strcpy(pcpFieldList,olNewFieldArray[ilD]);
						strcpy(pcpData,olNewDataArray[ilD]);
						blListsAreEmpty = false;
					}
					else
					{
						CString olTmp;
						olTmp.Format(",%s",olNewFieldArray[ilD]);
						strcat(pcpFieldList,olTmp);
						olTmp.Format(",%s",olNewDataArray[ilD]);
						strcat(pcpData,olTmp);
					}

					if(olNewDataArray[ilD] != olOldDataArray[ilD] &&
						olNewFieldArray[ilD] != "CDAT" && olNewFieldArray[ilD] != "USEC" &&
						olNewFieldArray[ilD] != "LSTU" && olNewFieldArray[ilD] != "USEU")
					{
						blChangesFound = true;
					}
				}
			}
		}
	}

	return blChangesFound;
}

CCSReturnCode CedaRemData::UpdateRemRecord(REMDATA *prpRemData, REMDATA *prpOldRemData)
{
	CCSReturnCode olRc = RCSuccess;
	PrepareDataForWrite(prpRemData);

	char pclFieldList[500], pclData[5000];
	if(GetChangedFields(prpRemData, prpOldRemData, pclFieldList, pclData))
	{
		char pclCommand[10] = "URT";
		char pclSelection[100];
		sprintf(pclSelection," WHERE URNO = '%ld%'",prpRemData->Urno);
		if((olRc = CedaAction(pclCommand, pcmTableName, pclFieldList, pclSelection, "", pclData)) == RCSuccess)
		{
			ogCCSDdx.DataChanged((void *)this,REM_CHANGE,(void *)prpRemData);
		}
	}

	ConvertDatesToLocal(prpRemData);

	return olRc;
}

CCSReturnCode CedaRemData::DeleteRemRecord(long lpUrno)
{
	CCSReturnCode olRc = RCSuccess;

	char pclFieldList[500], pclData[1000];
	char pclCommand[10] = "DRT";
	char pclSelection[100];
	sprintf(pclSelection," WHERE URNO = '%ld%'",lpUrno);
	if((olRc = CedaAction(pclCommand, pcmTableName, pclFieldList, pclSelection, "", pclData)) == RCSuccess)
	{
		DeleteRemInternal(lpUrno);
		ogCCSDdx.DataChanged((void *)this,REM_DELETE,(void *)lpUrno);
	}

	return olRc;
}

REMDATA *CedaRemData::InsertRemRecord(long lpRurn, const char *pcpText, const char *pcpRtab, const char *pcpPurp)
{
	REMDATA *prlRem = new REMDATA;
	prlRem->Urno = ogBasicData.GetNextUrno();
	strncpy(prlRem->Text, pcpText, REMDATA_TEXTLEN);
	prlRem->Rurn = lpRurn;
	strcpy(prlRem->Rtab, pcpRtab);
	strcpy(prlRem->Purp, pcpPurp);
	strcpy(prlRem->Appl,pcgAppName);
	PrepareDataForWrite(prlRem);

	char pclData[5000];
	char pclCommand[10] = "IRT";
	char pclSelection[100] = "";
	CString olListOfData;
	MakeCedaData(&omRecInfo,olListOfData,prlRem);
	strcpy(pclData,olListOfData);
	if(CedaAction(pclCommand, pcmTableName, pcmFieldList, pclSelection, "", pclData) == RCSuccess)
	{
		AddRemInternal(*prlRem);
		ogCCSDdx.DataChanged((void *)this, REM_CHANGE, (void *)prlRem);
	}
	else
	{
		delete prlRem;
		prlRem = NULL;
	}

	return prlRem;
}

