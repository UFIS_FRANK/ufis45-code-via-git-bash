///////////////////////////////////////////////////
//

#ifndef _FREE_EMP_VIEWER_
#define _FREE_EMP_VIEWER_

#include <CCSPtrArray.h>
#include <CedaShiftData.h>
#include <CedaJobData.h>
#include <cviewer.h>
#include <table.h>

struct FREEEMPS_ADDITIONALS
{
	long			FlightUrno;
	char			FlightText[100];
	long			StaffJobUrno;
	char			StaffText[100];
	long			CCIJobUrno;
	char			JobText[100];
	char			CCILineText[100];
	CTime			StartTime;
	CTime			EndTime;
	CStringArray	Functions;	
	bool			TeamAllocation;

	FREEEMPS_ADDITIONALS(void)
	{
		FlightUrno		= 0;
		FlightText[0]	= '\0';
		StaffJobUrno	= 0;
		StaffText[0]	= '\0';
		CCIJobUrno		= 0;
		CCILineText[0]	= '\0';
		JobText[0]		= '\0';
		StartTime		= TIMENULL;
		EndTime			= TIMENULL;
		TeamAllocation	= false;
	}

	FREEEMPS_ADDITIONALS(const FREEEMPS_ADDITIONALS& ropRHS)
	{
		FlightUrno		= ropRHS.FlightUrno;
		strcpy(FlightText,ropRHS.FlightText);
		StaffJobUrno	= ropRHS.StaffJobUrno;
		strcpy(StaffText,ropRHS.StaffText);
		CCIJobUrno		= ropRHS.CCIJobUrno;
		strcpy(CCILineText,ropRHS.CCILineText);
		strcpy(JobText,ropRHS.JobText);
		StartTime		= ropRHS.StartTime;
		EndTime			= ropRHS.EndTime;
		for (int i = 0; i < ropRHS.Functions.GetSize(); i++)
		{
			Functions.Add(ropRHS.Functions[i]);
		}
		TeamAllocation	= ropRHS.TeamAllocation;
	}

	FREEEMPS_ADDITIONALS& operator=(const FREEEMPS_ADDITIONALS& ropRHS)
	{
		if (&ropRHS != this)
		{
			FlightUrno		= ropRHS.FlightUrno;
			strcpy(FlightText,ropRHS.FlightText);
			StaffJobUrno	= ropRHS.StaffJobUrno;
			strcpy(StaffText,ropRHS.StaffText);
			CCIJobUrno		= ropRHS.CCIJobUrno;
			strcpy(CCILineText,ropRHS.CCILineText);
			strcpy(JobText,ropRHS.JobText);
			StartTime		= ropRHS.StartTime;
			EndTime			= ropRHS.EndTime;
			for (int i = 0; i < ropRHS.Functions.GetSize(); i++)
			{
				Functions.Add(ropRHS.Functions[i]);
			}
			TeamAllocation	= ropRHS.TeamAllocation;
		}

		return *this;
	}
};

struct FREEEMPS_LINEDATA
{
	long Urno;
	char Tmid[12];		// Team ID
	char Peno[10];		// PK number
	char Rank[12];		// rank
	char Lnam[42];		// long name
	char Pool[20];		// Pool name
	char Sfca[20];		// Shift Code Actual
	long PoolJobUrno;	// URNO of the pool job for this emp
	CTime Avfr;			// start time
	CTime Avto;			// end time

	FREEEMPS_LINEDATA(void)
	{memset(this,'\0',sizeof(*this));Avfr=-1;Avto=-1;}
};

class FreeEmpViewer : public CViewer
{
public:
	FreeEmpViewer(CCSPtrArray<FREEEMPS_LINEDATA> *popFreeEmps);
	~FreeEmpViewer();

	CCSPtrArray<FREEEMPS_LINEDATA> *pomFreeEmps;
    void Attach(CTable *popAttachWnd);

private:
    void MakeLines();
	void MakeLine(FREEEMPS_LINEDATA  *prpFree);
	int CreateLine(FREEEMPS_LINEDATA*prpFree);
	int CompareFree(FREEEMPS_LINEDATA *prpFree1, FREEEMPS_LINEDATA *prpFree2);
	bool bmUseAllShiftCodes, bmUseAllRanks, bmUseAllPermits, bmUseAllTeams, bmUseAllPools;
	CMapStringToPtr omCMapForShiftCode, omCMapForRank, omCMapForPermits, omCMapForTeam, omCMapForPool;
	void PrepareFilter(void);
	bool IsPassFilter(FREEEMPS_LINEDATA *prpLine);

public:
	void DeleteAll();
	void DeleteLine(int ipLineno);

public:
	void UpdateView(const char *pcpViewName);
	void UpdateDisplay();
	CString Format(FREEEMPS_LINEDATA *prpLine);
	void ProcessShiftChange(SHIFTDATA *prpShift);
	void ProcessShiftChange(JOBDATA *prpJob);

// Attributes used for filtering condition
private:
    int omGroupBy;              // enumerated value -- define in "Sfviewer.cpp" (use first sorting)
    CStringArray omSortOrder;     // array of enumerated value -- defined in "FJviewer.cpp"
    CString omDate;
    CTime omStartTime;
	CTime omEndTime;
// Attributes
private:
    CTable *pomTable;
    CCSPtrArray<FREEEMPS_LINEDATA> omLines;

// Methods which handle changes (from Data Distributor)
public:

};
#endif