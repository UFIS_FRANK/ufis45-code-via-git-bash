// FlIndDemandTablePropertySheet.h : header file
//

#ifndef _FLINDDEMANDTABLEPS_H_
#define _FLINDDEMANDTABLEPS_H_

/////////////////////////////////////////////////////////////////////////////
// FlIndDemandTablePropertySheet

#include <BasePropertySheet.h>
#include <FilterPage.h>
#include <FlIndDemandTableSortPage.h>

class FlIndDemandTablePropertySheet : public BasePropertySheet
{
// Construction
public:
	FlIndDemandTablePropertySheet(CWnd* pParentWnd = NULL,CViewer *popViewer = NULL, UINT iSelectPage = 0);

// Attributes
public:
	FilterPage					m_pageServiceNames;
	FilterPage					m_pageServiceCodes;
	FilterPage					m_pageDety;
	FlIndDemandTableSortPage	m_pageSort;

// Operations
public:
	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
	virtual int	 QueryForDiscardChanges();
};

/////////////////////////////////////////////////////////////////////////////

#endif // _FLINDDEMANDTABLEPS_H_
