// Class for Tples
#ifndef _CEDATPLDATA_H_
#define _CEDATPLDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct TplDataStruct
{
	long	Urno;		// Unique Record Number
	char	Dalo[11];
	char	Evor[2];
	char	Fisu[2];
	char	Relx[7];
	char	Tnam[65];
	char	Tpst[2];
	char	Appl[9];
	
};

typedef struct TplDataStruct TPLDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaTplData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <TPLDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapStringToPtr omNameMap;
	CMapStringToPtr omDaloMap;
	CString GetTableName(void);

// Operations
public:
	CedaTplData();
	~CedaTplData();

	CCSReturnCode ReadTplData();
	TPLDATA* GetTplByUrno(long lpUrno);
	TPLDATA* GetTplByName(const char *pcpName);
	bool GetTplByDalo(CCSPtrArray<TPLDATA> &ropTpl, const char *pcpDalo);
	long GetTplUrnoByName(const char *pcpName);
	void GetTplNameByDalo(const char* pcpDalo, CStringArray &ropTnam);

private:
	void AddTplInternal(TPLDATA &rrpTpl);
	void ClearAll();
};


extern CedaTplData ogTplData;
#endif _CEDATPLDATA_H_
