//
// CedaAzaData.h - rule demands - contains extra info about demands
//
#ifndef _CEDAAZADATA_H_
#define _CEDAAZADATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

#define SOH 0x01
#define STX 0x02

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

// functions and qualifications are linked:
// FUNC = TDL<0x02>MDL<0x02>CS<0x02>
// SQUA = EN<0x01>EL<0x02>ME<0x02>NU<0x02>
// EN<0x01>EL belong to TDL, ME belongs to MDL and NU belongs to CS
// where <0x01> seperates qualifications for a single function
// and <0x02> seperates qualifications for seperate functions
//
// GQUA - group qualification - at least one of the emps in the group
// must have this qualification

// For each record there are one or more demands - the num of demands
// matches the number of functions. An assigned emp must have one of
// the listed functions and its related qualification. When an emp is
// assigned, the satisfied function/qualification(s) is effectively
// removed from the list. When all demands for this AZA record are
// assigned, all functions must be satisfied.

#define AZASTRLEN 125

struct FunctionAndPermitsData
{
	CString Function;		// single function
	CStringArray Permits;	// each permit is a seperate string
};
typedef struct FunctionAndPermitsData FUNCTIONANDPERMITSDATA;

struct FPStruct
{
//	CString Name;	  // Employee Name
	CString Function; // single function
	CString Permits;  // Permits in the format "|AA||BB||...||ZZ|"
};

typedef struct FPStruct FUNC_PERMITS;

struct AzaDataStruct
{
	long	Urno;				// Unique Record Number
	char	Func[AZASTRLEN];	// Function list
	char	Squa[AZASTRLEN];	// Qualifications (one or more per function)
	char	Gqua[AZASTRLEN];	// Group Qualification
	CTime	Acbe;				// Start Time of Activity
	CTime	Acen;				// End Time of Activity
	char	Acst[2];			// State
	char	Act3[4];			// TAM Type of Airplane
	char	Azso[7];			// Module Name
	CTime	Cdat;				// Creation date
	char	Usec[33];			// Creator
	CTime	Lstu;				// Last Update
	char	Useu[33];			// Updater
	char	Nres[3];			// Number of Resource
	char	Poty[2];			// Type Activity "H" Hangar, "L" Line.
	char	Prio[7];			// Priority
	char	Regn[6];			// Registration
	char	Seco[7];			// Code and Type of Damage
	char	Pkey[19];			// Not used here - read coz it must be non-unique when records are inserted (see ::Insert())
	char	Stat[11];			// Stat[0]=='1' default time values used


	// additional data not from DB - prepared list of functions and permits
    CCSPtrArray <FUNCTIONANDPERMITSDATA> FunctionAndPermits;

	AzaDataStruct(void)
	{
		Urno = 0L;
		memset(Func,'\0',AZASTRLEN);
		memset(Squa,'\0',AZASTRLEN);
		memset(Gqua,'\0',AZASTRLEN);
		Cdat = TIMENULL;
		Lstu = TIMENULL;
		strcpy(Usec,"");
		strcpy(Useu,"");
		strcpy(Pkey,"");
		strcpy(Stat,"0000000000");
	}
};

typedef struct AzaDataStruct AZADATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaAzaData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <AZADATA> omData;
	CMapPtrToPtr omUrnoMap;
	CString GetTableName(void);

// Operations
public:
	CedaAzaData();
	~CedaAzaData();

	CCSReturnCode ReadAzaData(CTime opStartTime, CTime opEndTime);
	bool Insert(AZADATA *prpAza);
	bool InsertAzaForThirdParty(AZADATA *prpAza);
	AZADATA *GetAzaByUrno(long lpUrno);
	void ProcessAzaBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	CString Dump(long lpUrno);
	bool DeleteAzaRecord(long lpAzaUrno);
	bool DeleteAzaRecord(AZADATA *prpAza);

private:
	AZADATA *AddAzaInternal(AZADATA &rrpAza);
	void DeleteAzaInternal(long lpUrno);
	void ClearAll();
	void PrepareData(AZADATA *prpAza);
	void CreateFunctionAndPermitsList(AZADATA *prpAza);
	void PrepareDataForWrite(AZADATA *prpAza, const char *pcpText);
};


extern CedaAzaData ogAzaData;
#endif _CEDAAZADATA_H_
