// PRMFlightSearch.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// PRMFlightSearch dialog
#ifndef _PRMFLIGHTSEARCH
#define _PRMFLIGHTSEARCH
  
class PRMFlightSearch : public CDialog
{
//	DECLARE_DYNCREATE(PRMFlightSearch)

// Construction
public:
	PRMFlightSearch();
	PRMFlightSearch(CWnd* pParent);
	~PRMFlightSearch();

// Dialog Data
	//{{AFX_DATA(PRMFlightSearch)
	enum { IDD = IDD_FLIGHTSEARCH };
	CString		m_Airline;
//	CTime		m_Date;
	CComboBox	m_Date;
	CString		m_Flightno;
	int			m_SingleFlight;
	//}}AFX_DATA
	CString omTitle; 

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(PRMFlightSearch)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(PRMFlightSearch)
	afx_msg void OnKillfocusFlightnr();
	virtual BOOL OnInitDialog();
	afx_msg void OnRadioGanttSelect();
	afx_msg void OnRadioDetailDisplay();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
//	static int imLastState;
//	int FlightDetailDisplayAsDefault() const;
};


#endif // _PRMFLIGHTSEARCH
