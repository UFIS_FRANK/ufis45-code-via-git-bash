// DemandTable.h : header file
//
#ifndef __DEMANDTABLE_H__
#define __DEMANDTABLE_H__

#include <DemandTableViewer.h>
#include <CCSDragDropCtrl.h>

#include <ccstable.h> //Singapore
/////////////////////////////////////////////////////////////////////////////
// DemandTable dialog

class DemandTable : public CDialog
{
// Construction
public:
	DemandTable(CWnd* pParent = NULL);   // standard constructor
	DemandTable(DemandTableViewer *popViewer,CWnd* pParent = NULL);
	~DemandTable();
	void UpdateView();
	void SetCaptionText(void);

	static const CString Terminal2; //Singapore
	static const CString Terminal3; //Singapore


private :
    CCSTable*			pomTable; // visual object, the table content
	CCSTable *pomActiveTable; //Singapore
	CCSTable *pomTableT3;     //Singapore	
	BOOL				bmIsViewOpen;
	DemandTableViewer*	pomViewer;
	DemandTableViewer*  pomViewerT3;     //Singapore
	DemandTableViewer*  pomActiveViewer; //Singapore
	int					m_nDialogBarHeight;
	CString omActiveTerminal; //Singapore
	BOOL bmIsT2Visible;       //Singapore
	BOOL bmIsT3Visible;       //Singapore
	CRect omWindowRect; //PRF 8712

private:
	static void			DemandTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
	void				HandleGlobalDateUpdate();
	void				SetViewerDate();
	void				UpdateComboBox();
	BOOL FindActiveTableAndViewer(CPoint& ropPoint); //Singapore
	BOOL SetActiveTableAndViewer(CCSTABLENOTIFY *prpNotify); //Singapore
	void CheckScrolling(CPoint& ropPoint); //Singapore

// Dialog Data
	//{{AFX_DATA(DemandTable)
	enum { IDD = IDD_DEMANDTABLE };
	CComboBox	m_Date;
	CCSButtonCtrl m_bTerminal2; //Singapore
	CCSButtonCtrl m_bTerminal3; //Singapore
	CButton m_bSetTime;//Singapore
	//}}AFX_DATA

 
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DemandTable)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DemandTable)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	virtual void OnCancel();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMove(int x, int y); //PRF 8712
	afx_msg void OnSelchangeViewcombo();
	afx_msg void OnView();
	afx_msg void OnPrint();
	afx_msg void OnCloseupViewcombo();
	afx_msg LONG OnTableDragBegin(UINT wParam, LONG lParam);
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	afx_msg void OnClose();
	afx_msg void OnSelchangeDate();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
    afx_msg LONG OnTableLButtonDown(UINT wParam, LONG lParam);
    afx_msg LONG OnTableRButtonDown(UINT wParam, LONG lParam);
	afx_msg void OnExpandGroup();
	afx_msg void OnCompressGroup();
	afx_msg LONG OnTableSelchange(UINT wParam,LONG lParam);
	afx_msg void OnMenuWorkOn();
	afx_msg void OnMenuAvailableEmployees();
	afx_msg void OnMenuAutomaticAssignment();
	afx_msg void OnMenuAvailableEquipment();
	afx_msg void OnMenuDeactivateDemand();
	afx_msg void OnButtonT2(); //Singapore
	afx_msg void OnButtonT3(); //Singapore
	afx_msg void OnTableUpdateDataCount(); //Singapore
	afx_msg void OnSetTime();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Drag-and-drop implementation
private:
	CCSDragDropCtrl m_DragDropTarget;
	long			imDropDemandUrno;
	int GetSelectedLines(CUIntArray &ropSelectedLines);
};

#endif //__DEMANDTABLE_H__
