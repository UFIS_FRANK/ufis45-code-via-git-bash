// StaffLst.cpp : implementation file
//
#include <stdafx.h>
#include <ccsglobl.h>
#include <OpssPm.h>
#include <CCSCedaCom.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <table.h>
#include <StaffTableViewer.h>
#include <StaffTable.h>
#include <StartDate.h>
#include <CedaSprData.h>

#include <cviewer.h>
#include <FilterPage.h>
#include <ZeitPage.h>
#include <StaffTableSortPage.h>
#include <BasePropertySheet.h>
#include <StaffTablePropertySheet.h>
#include <BasicData.h>

#include <CedaJobData.h>
#include <CCSDragDropCtrl.h>
#include <StaffDetailWindow.h>
#include <ccsddx.h>
#include <CedaCfgData.h>
#include <AbsenceDlg.h>

#include <cxbutton.h>
#include <CedaShiftData.h>
#include <CCIDiagram.h>
#include <CCITable.h>
#include <ConflictTable.h>
#include <ConflictConfigTable.h>
#include <StaffDiagram.h>
#include <GateDiagram.h>
#include <RegnDiagram.h>
#include <FlightPlan.h>
#include <GateTable.h>
#include <PrePlanTable.h>
#include <ButtonList.h>
#include <SetDisplayTimeframeDlg.h>
#include <SelReportDlg.h>
#include <FieldConfigDlg.h>
#include <process.h> // required for _spawnv
#include <PrintTerminalSelection.h> //Singapore
#include <CPrintPreviewView.h> //Singapore

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

const CString StaffTable::Terminal2 = "T2";
const CString StaffTable::Terminal3 = "T3";

/////////////////////////////////////////////////////////////////////////////
// StaffTable dialog

StaffTable::StaffTable(CTime opDate, BOOL blIsFromSearch, BOOL bpIsDetailDisplay)
	: CDialog(StaffTable::IDD, NULL)
{
	//{{AFX_DATA_INIT(StaffTable)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	ogCCSDdx.UnRegister(&omBothViewer, NOTUSED);
	omDate = opDate; // day to be displayed

	bmIsFromSearch = blIsFromSearch;
	bmIsDetailDisplay = bpIsDetailDisplay;
	pomTemplate = NULL; //Singpaore
	pomPrintPreviewView = NULL; //Singapore

	if(bmIsFromSearch == TRUE)
	{
		omViewer.SetIsFromSearchMode(TRUE);
	}
    pomStaffTable = new CTable;
	imSelectedTerminal = CPrintTerminalSelection::TERMINAL2_SELECTED;
	//pomStaffTable->imSelectMode = 0;

	if((bmIsPaxServiceT2 = ogBasicData.IsPaxServiceT2()) == true) //Singapore
	{
		pomStaffTableT3 = new CTable;
		pomActiveStaffTable = pomStaffTable;
		pomActiveViewer = &omViewer;
		if(bmIsFromSearch == TRUE)
		{
			omViewerT3.SetIsFromSearchMode(TRUE);
		}
	}

	BOOL blMinimized = FALSE;
    CDialog::Create(StaffTable::IDD);
    CRect olRect;
	ogBasicData.GetWindowPosition(olRect,ogCfgData.rmUserSetup.STTB,ogCfgData.rmUserSetup.MONS);
	olRect.top += 40;
	ogBasicData.GetDialogFromReg(olRect, COpssPmApp::EMPLOYEE_TABLE_WINDOWPOS_REG_KEY,blMinimized);	

	//Multiple monitor setup
	ogBasicData.GetWindowPositionCorrect(olRect);

    MoveWindow(&olRect);
	bmIsViewOpen = FALSE;

    CRect rect;
    GetClientRect(&rect);
    rect.OffsetRect(0, m_nDialogBarHeight);
    rect.InflateRect(1, 1);     // hiding the CTable window border

	if(bmIsPaxServiceT2 == true) //Singapore
	{
		pomStaffTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom/2);
		pomStaffTableT3->SetTableData(this, rect.left, rect.right, rect.bottom/2 + 1 - m_nDialogBarHeight, rect.bottom + rect.bottom/2 + 1 - m_nDialogBarHeight - m_nDialogBarHeight);
		omViewer.SetTerminal(Terminal2);
		omViewerT3.SetTerminal(Terminal3);
		omViewer.Attach(pomStaffTable);
		omViewerT3.Attach(pomStaffTableT3);

		bmIsT2Visible = TRUE;
		bmIsT3Visible = TRUE;
	}
	else
	{
		ogCCSDdx.UnRegister(&omViewerT3, NOTUSED);
		pomStaffTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);
		omViewer.Attach(pomStaffTable);
		pomActiveStaffTable = pomStaffTable;
		pomActiveViewer = &omViewer;
		bmIsT2Visible = FALSE;
		bmIsT3Visible = FALSE;
	}
    
	omContextItem = -1;
	UpdateView();

	if(blMinimized == TRUE)
	{
		ShowWindow(SW_MINIMIZE);
	}
}

StaffTable::~StaffTable()
{
	TRACE("StaffTable::~StaffTable()\n");
	delete pomStaffTable;
	if(bmIsPaxServiceT2 == true) //Singapore
	{
		omViewerT3.Attach(NULL);
		if(pomStaffTableT3 != NULL)
		{
			delete pomStaffTableT3;
		}
	}
}

void StaffTable::SetCaptionText(void)
{
	CString olCaptionText ;

	if (bmIsFromSearch == TRUE)
	{
		//olCaptionText = CString("gefundene Mitarbeiter ");
		olCaptionText = GetString(IDS_STRING61330);
	}
	else
	{
//		olCaptionText = CString("Mitarbeiter Check-In: ") +  
//			omViewer.omStartTime.Format("%d/%H%M") + "-" + 
//				omViewer.omEndTime.Format("%H%M ");
		olCaptionText = GetString(IDS_STRING61331) +  
			omViewer.omStartTime.Format("%d/%H%M") + "-" + 
				omViewer.omEndTime.Format("%H%M ");
	}

//	if (bgOnline)
//	{
//		//olCaptionText += "  Online";
//		olCaptionText += CString("  ") + GetString(IDS_STRING61301);
//	}
//	else
//	{
//		//olCaptionText += "  OFFLINE";
//		olCaptionText += CString("  ") + GetString(IDS_STRING61302);
//	}

	SetWindowText(olCaptionText);

}

void StaffTable::SetCaptionText(CString opDate)
{
	//CString olCaptionText = CString("Mitarbeiter Check-In am ") +  opDate;
	CString olCaptionText = GetString(IDS_STRING61334) +  opDate;

//	if (bgOnline)
//	{
//		//olCaptionText += "  Online";
//		olCaptionText += CString("  ") + GetString(IDS_STRING61301);
//	}
//	else
//	{
//		//olCaptionText += "  OFFLINE";
//		olCaptionText += CString("  ") + GetString(IDS_STRING61302);
//	}

	SetWindowText(olCaptionText);

}

    
void StaffTable::UpdateView()
{

	CString  olViewName = omViewer.GetViewName();
	
	if (olViewName.IsEmpty() == TRUE)
	{
		olViewName = ogCfgData.rmUserSetup.STLV;
	}

	AfxGetApp()->DoWaitCursor(1);	
	//Singapore
	if(bmIsPaxServiceT2 == true)
	{
		omViewer.ChangeViewTo(olViewName);
	    omViewerT3.ChangeViewTo(olViewName);
	}
	else
	{
		omViewer.ChangeViewTo(olViewName);
	}
    
	AfxGetApp()->DoWaitCursor(-1);
	if(bmIsFromSearch == TRUE)
	{
		SetCaptionText();
	}
	else
	{
		SetCaptionText();
	}

	//Singapore
	OnTableUpdateDataCount();
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
//	pomStaffTable->GetCTableListBox()->SetFocus();
}

void StaffTable::UpdateComboBox()
{
	CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_COMBO1);
	polCB->ResetContent();
	CStringArray olStrArr;
	omViewer.GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = omViewer.GetViewName();
	if (olViewName.IsEmpty() == TRUE)
	{
		olViewName = ogCfgData.rmUserSetup.STLV;
	}

	ilIndex = polCB->FindString(-1,olViewName);
		
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}
}

void StaffTable::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(StaffTable)
	DDX_Control(pDX, IDC_DATE, m_Date);
	DDX_Control(pDX, IDC_BUTTONT2,m_bTerminal2); //Singapore
	DDX_Control(pDX, IDC_BUTTONT3,m_bTerminal3); //Singapore
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(StaffTable, CDialog)
	//{{AFX_MSG_MAP(StaffTable)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_MOVE() //PRF 8712
	ON_CBN_SELCHANGE(IDC_COMBO1, OnSelchangeCombo1)
	ON_BN_CLICKED(IDC_VIEW, OnView)
	ON_BN_CLICKED(IDC_TAG_STAFFTABLE, OnTagStafftable)
	ON_BN_CLICKED(IDC_PRINT, OnPrint)
	ON_BN_CLICKED(IDC_SETTIME, OnSetTime)
	ON_BN_CLICKED(IDC_BUTTONT2,OnButtonT2) //Singapore
	ON_BN_CLICKED(IDC_BUTTONT3,OnButtonT3) //Singapore
	ON_BN_CLICKED(IDC_BUTTON_PRINT_PREVIEW,OnPrintPreview) //Singapore
	ON_MESSAGE(WM_BEGIN_PRINTING, OnBeginPrinting) //Singapore
	ON_MESSAGE(WM_END_PRINTING, OnEndPrinting) //Singapore
	ON_MESSAGE(WM_PRINT_PREVIEW, PrintPreview) //Singapore
	ON_MESSAGE(WM_PREVIEW_PRINT, OnPreviewPrint) //Singapore
	ON_COMMAND(11, OnMenuWorkOn)
	ON_COMMAND(12, OnMenuAbsent)
	ON_COMMAND_RANGE(15, 40,OnMenuDelete)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
    ON_MESSAGE(WM_TABLE_RBUTTONDOWN, OnTableRButtonDown)
	ON_CBN_CLOSEUP(IDC_COMBO1, OnCloseupCombo1)
    ON_MESSAGE(WM_TABLE_DRAGBEGIN, OnTableDragBegin)
	ON_WM_CLOSE()
	ON_CBN_SELCHANGE(IDC_DATE, OnSelchangeDate)
	ON_MESSAGE(WM_TABLE_SETTABLEWINDOW,OnTableSetTableWindow) //Singapore
	ON_MESSAGE(WM_TABLE_UPDATE_DATACOUNT,OnTableUpdateDataCount) //Singapore
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// StaffTable message handlers

BOOL StaffTable::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CWnd *polWnd = GetDlgItem(IDC_VIEW); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61840));
	}
	polWnd = GetDlgItem(IDC_PRINT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61844));
	}
	polWnd = GetDlgItem(IDC_TAG_STAFFTABLE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61851));
		CTime olCurrTime = ogBasicData.GetLocalTime();
		if(olCurrTime < ogBasicData.GetTimeframeStart() || olCurrTime > ogBasicData.GetTimeframeEnd())
		{
			polWnd->EnableWindow(false);
		}
	}

	//Singapore
	if(bmIsPaxServiceT2 == false)
	{
		CWnd* polWnd = GetDlgItem(IDC_DATACOUNT);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}
		polWnd = GetDlgItem(IDC_STATIC_T2T3);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}
		polWnd = GetDlgItem(IDC_BUTTONT2);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}
		polWnd = GetDlgItem(IDC_BUTTONT3);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}
	}
	else
	{
		m_bTerminal2.SetColors(LIME, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		m_bTerminal3.SetColors(LIME, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}

	if(bmIsFromSearch == TRUE)
	{
		//Hide the controls of dialogbar
		CWnd *polViewButton = GetDlgItem(IDC_VIEW);
		CWnd *polCB1 = GetDlgItem(IDC_COMBO1);
		CWnd *polPrintButton = GetDlgItem(IDC_PRINT);
		CWnd *polDayButton = GetDlgItem(IDC_TAG_STAFFTABLE);
		if (polViewButton != NULL)
		{
			polViewButton->SetWindowPos(&wndBottom,0,0,0,0,SWP_HIDEWINDOW | SWP_NOMOVE );
			polViewButton->EnableWindow(FALSE);
		}
		if(polCB1 != NULL)
		{
			polCB1->SetWindowPos(&wndBottom,0,0,0,0,SWP_HIDEWINDOW | SWP_NOMOVE );
			polCB1->EnableWindow(FALSE);
		}
		if(polPrintButton != NULL)
		{
			polPrintButton->SetWindowPos(&wndBottom,0,0,0,0,SWP_HIDEWINDOW | SWP_NOMOVE );
			polPrintButton->EnableWindow(FALSE);
		}
		if(polDayButton != NULL)
		{
			polDayButton->SetWindowPos(&wndBottom,0,0,0,0,SWP_HIDEWINDOW | SWP_NOMOVE );
			polDayButton->EnableWindow(FALSE);
		}
	}
	else
	{
		UpdateComboBox();
	}

    // calculate the window height
    CRect rect;
    GetClientRect(&rect);
    m_nDialogBarHeight = rect.bottom - rect.top;

    // extend dialog window to current screen width
    rect.left = 10;
    rect.top = 56;
    rect.right = ::GetSystemMetrics(SM_CXSCREEN) - 10;
    rect.bottom = ::GetSystemMetrics(SM_CYSCREEN) - 10;
    //MoveWindow(&rect); //Commented - Singapore PRF 8712
	
	// Register DDX call back function
	TRACE("StaffTable: DDX Registration\n");
	ogCCSDdx.Register(this, REDISPLAY_ALL, CString("STAFFLST"),CString("Redisplay all from What-If"), StaffTableCf);	// for what-if changes
	ogCCSDdx.Register(this, GLOBAL_DATE_UPDATE, CString("STAFFLST"),CString("Global Date Change"), StaffTableCf); // change the date of the data displayed
	ogCCSDdx.Register(this, PREPLAN_DATE_UPDATE, CString("STAFFLST"),CString("PrePlan Date Change"), StaffTableCf); // change the date of the data displayed

	ogBasicData.SetWindowStat("STAFFTABLE IDC_COMBO1",GetDlgItem(IDC_COMBO1));
	ogBasicData.SetWindowStat("STAFFTABLE IDC_VIEW",GetDlgItem(IDC_VIEW));
	ogBasicData.SetWindowStat("STAFFTABLE IDC_TAG_STAFFTABLE",GetDlgItem(IDC_TAG_STAFFTABLE));
	ogBasicData.SetWindowStat("STAFFTABLE IDC_PRINT",GetDlgItem(IDC_PRINT));


	CStringArray olTimeframeList;
	ogBasicData.GetTimeframeList(olTimeframeList);
	int ilNumDays = olTimeframeList.GetSize();
	for(int ilDay = 0; ilDay < ilNumDays; ilDay++)
	{
		m_Date.AddString(olTimeframeList[ilDay]);
	}
	if(ilNumDays > 0)
	{
		if(omDate == TIMENULL)
		{
			m_Date.SetCurSel(ogBasicData.GetDisplayDateOffset());
		}
		else
		{
			m_Date.SetCurSel(ogBasicData.GetDisplayDateOffsetByDate(omDate));
		}
		SetViewerDate();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void StaffTable::OnDestroy() 
{
	if (bgModal == TRUE)
		return;
	ogBasicData.WriteDialogToReg(this,COpssPmApp::EMPLOYEE_TABLE_WINDOWPOS_REG_KEY,omWindowRect,this->IsIconic());
	// Unregister DDX call back function
	TRACE("StaffTable: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogCCSDdx.UnRegister(this, NOTUSED);

	CDialog::OnDestroy();
}

void StaffTable::OnCancel() 
{
	// Normally, we should just call CDialog::OnCancel().
	// However, the DestroyWindow() is required since this table can be destroyed in two ways.
	// First is by pressing ESC key which will call this routine.
	// Second, it may be destroyed directly by calling Manfred's DestroyWindow() in this class
	// which is not so compatible with MFC.
	pogButtonList->m_wndStaffTable = NULL;
	pogButtonList->m_StaffTableButton.Recess(FALSE);
	DestroyWindow();
}

void StaffTable::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
    if (nType != SIZE_MINIMIZED)
	{	
		if(bmIsPaxServiceT2 == true)
		{
			if(bmIsT2Visible == TRUE && bmIsT3Visible == TRUE)
			{
				pomStaffTable->SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy/2+1);
				pomStaffTableT3->SetPosition(-1, cx+1, cy/2+2, cy+1);
			}
			else if(bmIsT2Visible == TRUE && bmIsT3Visible == FALSE)
			{
				pomStaffTable->SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy);			
			}
			else if(bmIsT2Visible == FALSE && bmIsT3Visible == TRUE)
			{
				pomStaffTableT3->SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy+1);
			}
		}
		else
		{
			pomStaffTable->SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy+1);
		}
		GetWindowRect(&omWindowRect); //PRF 8712
	}
}

void StaffTable::OnView() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
	//pomStaffTable->GetCTableListBox()->SetFocus();

	StaffTablePropertySheet dialog(this, &omViewer);
	bmIsViewOpen = TRUE;
	if (dialog.DoModal() != IDCANCEL)
		UpdateView();
	bmIsViewOpen = FALSE;
	UpdateComboBox();
}

void StaffTable::OnSelchangeCombo1() 
{
	char clText[64];
    CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_COMBO1);
    polCB->GetLBText(polCB->GetCurSel(), clText);
    TRACE("StaffTable::OnComboBox() [%s]", clText);
	//Singapore
	if(bmIsPaxServiceT2 == true)
	{
		omViewer.SelectView(clText);
		omViewerT3.SelectView(clText);
	}
	else
	{
		omViewer.SelectView(clText);
	}
	
	UpdateView();
	UpdateComboBox();
}

void StaffTable::OnCloseupCombo1() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box when the combo-box is closed.
    //pomStaffTable->GetCTableListBox()->SetFocus();
}

void StaffTable::OnTagStafftable() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box when the combo-box is closed.
    //pomStaffTable->GetCTableListBox()->SetFocus();
}

void StaffTable::OnPrint() 
{
	CFieldDataArray olFieldConfig;
	ogFieldConfig.ConfigureField(IDS_FIELDCONFIG_STAFFTABLEPRINT1, olFieldConfig, "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s", "","","","","","","","","","","","","","","","");
	if(olFieldConfig.GetSize() > 0)
	{
		omViewer.PrintViewConfigurable();
	}
	else
	{
		CSelReportDlg olSelReportDlg;
		olSelReportDlg.SetReports(GetString(IDS_STAFFTABLE_PRINT1));
		olSelReportDlg.SetReports(GetString(IDS_STAFFTABLE_PRINT2));
		olSelReportDlg.SetReports(GetString(IDS_STAFFTABLE_PRINT3));
		olSelReportDlg.SetReports(GetString(IDS_STAFFTABLE_EXPORT1));
		if(olSelReportDlg.DoModal() == IDOK)
		{
			omSelectedReport = olSelReportDlg.omSelectedReport;
			
			CPrintTerminalSelection olTerminalSelection;
			olTerminalSelection.SetWindowHeaderText(omSelectedReport);
			if(bmIsPaxServiceT2 == true)
			{
				if(omSelectedReport.IsEmpty())
				{
					return;
				}
				UINT iResponse = 0;
				if(strcmp(omSelectedReport,GetString(IDS_STAFFTABLE_EXPORT1)) != 0
				   && (iResponse = olTerminalSelection.DoModal()) == IDOK)
				{
					if(olTerminalSelection.IsTerminal3Selected() == TRUE)
					{
						imSelectedTerminal = CPrintTerminalSelection::TERMINAL3_SELECTED;
					}
					else if(olTerminalSelection.IsBothTerminalSelected() == TRUE)
					{
						imSelectedTerminal = CPrintTerminalSelection::BOTH_TERMINAL_SELECTED;
					}
				}
				else if(iResponse == IDCANCEL)
				{
					return;
				}
			}
	
			if(!strcmp(omSelectedReport,GetString(IDS_STAFFTABLE_EXPORT1)))
				OnExport();
			else				
				PrintData();
		}
	}
}

void StaffTable::PrintData()
{
	StaffTableViewer olTableViewer;
	if(imSelectedTerminal == CPrintTerminalSelection::BOTH_TERMINAL_SELECTED)
	{
		olTableViewer.Attach(pomStaffTable);
		PrepareBothTerminalPrintData(olTableViewer);
	}

	if(!strcmp(omSelectedReport,GetString(IDS_STAFFTABLE_PRINT1)))
	{	
		if(imSelectedTerminal == CPrintTerminalSelection::TERMINAL3_SELECTED)
		{
			omViewerT3.PrintView();
		}
		else if(imSelectedTerminal == CPrintTerminalSelection::BOTH_TERMINAL_SELECTED)
		{		
			olTableViewer.PrintView();
			DeleteViewerLineData(olTableViewer);
			olTableViewer.Attach(NULL);	
		}
		else
		{
			omViewer.PrintView();
		}
	}
	else if(!strcmp(omSelectedReport,GetString(IDS_STAFFTABLE_PRINT2)))
	{
		if(imSelectedTerminal == CPrintTerminalSelection::TERMINAL3_SELECTED)
		{
			omViewerT3.PrintView2();
		}
		else if(imSelectedTerminal == CPrintTerminalSelection::BOTH_TERMINAL_SELECTED)
		{
			olTableViewer.PrintView2();
			DeleteViewerLineData(olTableViewer);
			olTableViewer.Attach(NULL);	
		}
		else
		{
			omViewer.PrintView2();
		}
	}
	else if(!strcmp(omSelectedReport,GetString(IDS_STAFFTABLE_PRINT3)))
	{
		if(imSelectedTerminal == CPrintTerminalSelection::TERMINAL3_SELECTED)
		{
			omViewerT3.PrintView3();
		}
		else if(imSelectedTerminal == CPrintTerminalSelection::BOTH_TERMINAL_SELECTED)
		{
			olTableViewer.PrintView3();
			DeleteViewerLineData(olTableViewer);
			olTableViewer.Attach(NULL);	
		}
		else
		{
			omViewer.PrintView3();
		}
	}
}
// On Excel button press ...
void StaffTable::OnExport() 
{
	char pclConfigPath[256];
	char pclExcelPath[256];
	char pclSeperator[64];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(pcgAppName, "EXCEL", "DEFAULT",pclExcelPath, sizeof pclExcelPath, pclConfigPath);
	GetPrivateProfileString(pcgAppName, "EXCELSEPARATOR", ";",pclSeperator, sizeof pclSeperator, pclConfigPath);

	if (strcmp(pclExcelPath, "DEFAULT") == 0)
	{
//		MessageBox(GetString(IDS_STRING992),GetString(IDS_STRING145), MB_ICONERROR);
		return;
	}

	CWaitCursor olWait;

	CTime olCurrTime = CTime::GetCurrentTime();
	CString olFileName;
	olFileName.Format("%s\\EmployeeList%s.csv", CCSLog::GetTmpPath(), olCurrTime.Format("%Y%m%d%H%M%S"));
	CString olTitle;
	GetWindowText(olTitle);


	if(bmIsPaxServiceT2 == true)
	{
		CPrintTerminalSelection olTerminalSelection;
		StaffTableViewer olTableViewer;
		olTerminalSelection.SetWindowHeaderText(GetString(IDS_STAFFTABLE_EXPORT1));
		if(olTerminalSelection.DoModal() == IDOK)
		{
			if(olTerminalSelection.IsTerminal2Selected() == TRUE)
			{
				omViewer.CreateExport(olFileName, pclSeperator, olTitle);
			}
			else if(olTerminalSelection.IsTerminal3Selected() == TRUE)
			{
				omViewerT3.CreateExport(olFileName, pclSeperator, olTitle);
			}
			else if(olTerminalSelection.IsBothTerminalSelected() == TRUE)
			{	
				olTableViewer.Attach(pomStaffTable);
				PrepareBothTerminalPrintData(olTableViewer);	
				//olTableViewer.SetImColumns(omViewer.GetLines().GetSize() > 0 ? omViewer.GetImColumns() : omViewerT3.GetImColumns());
				olTableViewer.CreateExport(olFileName, pclSeperator, olTitle);
				DeleteViewerLineData(olTableViewer);
				olTableViewer.Attach(NULL);
			}
		}
		else
		{
			return;
		}
	}
	else
	{
		omViewer.CreateExport(olFileName, pclSeperator, olTitle);
	}

	bool test = true; //only for testing error
	if (olFileName.IsEmpty() || olFileName.GetLength() > 255 || !test)
	{
		if (olFileName.IsEmpty())
		{
			CString mess = "Filename is empty !" + olFileName;
			MessageBox(mess, "Error", MB_ICONERROR);
			return;
		}
		if (olFileName.GetLength() > 255 )
		{
			CString mess = "Length of the Filename > 255: " + olFileName;
			MessageBox(mess, "Error", MB_ICONERROR);
			return;
		}

		CString mess = "Filename invalid: " + olFileName;
		MessageBox(mess, "Error", MB_ICONERROR);
		return;
	}

	char pclTmp[256];
	strcpy(pclTmp, olFileName); 

   /* Set up parameters to be sent: */
	char *args[4];
	args[0] = "child";
	args[1] = pclTmp;
	args[2] = NULL;
	args[3] = NULL;

	_spawnv( _P_NOWAIT , pclExcelPath, args );	// start excel application, load and display the output file !
}


void StaffTable::OnSetTime() 
{
	CSetDisplayTimeframeDlg olDlg;
	olDlg.omFrom = omViewer.omStartTime;
	olDlg.omTo = omViewer.omEndTime;
	if(olDlg.DoModal() == IDOK)
	{
		omViewer.omStartTime = olDlg.omFrom;
		omViewer.omEndTime = olDlg.omTo;
		if(bmIsPaxServiceT2 == true)
		{
			omViewerT3.omStartTime = olDlg.omFrom;
			omViewerT3.omEndTime = olDlg.omTo;
		}
		UpdateView();
	}
}

BOOL StaffTable::DestroyWindow() 
{
	if ((bmIsViewOpen) || (bgModal == TRUE))
	{
		MessageBeep((UINT)-1);
		return FALSE;    // don't destroy window while view property sheet is still open
	}

	BOOL blRc = CDialog::DestroyWindow();
	delete this;
	return blRc;
}

LONG StaffTable::OnTableLButtonDblclk(UINT ipItem, LONG lpLParam)
{
	STAFFTABLE_LINEDATA *prlLine = (STAFFTABLE_LINEDATA *)pomActiveStaffTable->GetTextLineData(ipItem);
	if (prlLine != NULL)
	{
		CCSPtrArray <JOBDATA> olJobs;
		ogJobData.GetJobsByPkno(olJobs,prlLine->Peno);
		int ilNumJobs = olJobs.GetSize();
		for(int ilLc = 0; ilLc < ilNumJobs; ilLc++)
		{
			JOBDATA *prlJob = &olJobs[ilLc];
			if(!strcmp(prlJob->Jtco,JOBPOOL) && prlJob->Shur == prlLine->ShiftUrno)
			{
				if (bmIsDetailDisplay)
				{
					new StaffDetailWindow(this, prlJob->Urno);
				}
				else
				{
					SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prlJob->Shur);
					if (prlShift)
						ogCCSDdx.DataChanged((void *)this,SHIFT_SELECT,(void *)prlShift);
				}
				break;
			}
		}
	}
	return 0L;
}

LONG StaffTable::OnTableRButtonDown(UINT ipItem, LONG lpLParam)
{
	pomActiveStaffTable->GetCTableListBox()->SetSel(-1,FALSE);

	CMenu menu;
	CPoint olPoint(LOWORD(lpLParam),HIWORD(lpLParam));	
	
	if(bmIsPaxServiceT2 == true)
	{
		if(omActiveTerminal == Terminal3 && bmIsT2Visible == TRUE)
		{
			CRect olRect;
			pomStaffTable->GetClientRect(&olRect);
			olPoint.y += olRect.Height();
		}
	}

	omContextItem = -1;
	omAssignUrnos.RemoveAll();

	STAFFTABLE_LINEDATA *prlLine = (STAFFTABLE_LINEDATA *)pomActiveStaffTable->GetTextLineData(ipItem);
	if (prlLine != NULL)
	{
		int ilLc;

		menu.CreatePopupMenu();
		for (ilLc = menu.GetMenuItemCount(); --ilLc >= 0;)
			menu.RemoveMenu(ilLc, MF_BYPOSITION);
		//menu.AppendMenu(MF_STRING,11,"Bearbeiten");	
		menu.AppendMenu(MF_STRING,11,GetString(IDS_STRING61352));	
		//menu.AppendMenu(MF_STRING,12,"Abwesend");	
		//menu.AppendMenu(MF_STRING,12,GetString(IDS_STRING61398));	


		CDWordArray olJobUrnos;
		for ( ilLc = 0; ilLc < prlLine->Assignment.GetSize(); ilLc++)
		{
			//CString olMenuText = "L�schen: Einsatz " + 
			CString olMenuText = GetString(IDS_STRING61401) + " " +
				prlLine->Assignment[ilLc].Alid + " " +
				prlLine->Assignment[ilLc].Acfr.Format("%H%M") + " - " +
				prlLine->Assignment[ilLc].Acto.Format("%H%M");
				menu.AppendMenu(MF_STRING,15 + ilLc, olMenuText);	
			omAssignUrnos.Add(prlLine->Assignment[ilLc].Urno);
			olJobUrnos.Add(prlLine->Assignment[ilLc].Urno);
		}
		
		if(ogSprData.bmNoChangesAllowedAfterClockout && ogSprData.ClockedOut(olJobUrnos))
			ogBasicData.DisableAllMenuItems(menu);
		
		ClientToScreen( &olPoint);
		menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, olPoint.x, olPoint.y + 55, this, NULL);
		omContextItem = prlLine->ShiftUrno;
	}

	return 0L;
}

void StaffTable::OnMenuWorkOn()
{
	if (omContextItem != -1)
	{
		int ilItem = -1;
		if (pomActiveViewer->FindShift(omContextItem,ilItem))
		{
			STAFFTABLE_LINEDATA *prlLine = (STAFFTABLE_LINEDATA *) pomActiveStaffTable->GetTextLineData(ilItem);
			if (prlLine != NULL)
			{
				CCSPtrArray <JOBDATA> olJobs;
				ogJobData.GetJobsByPkno(olJobs,prlLine->Peno);
				for(int ilLc = 0; ilLc < olJobs.GetSize(); ilLc++)
				{
					if (strcmp(olJobs[ilLc].Jtco,JOBPOOL) == 0)
					{
						new StaffDetailWindow(this,olJobs[ilLc].Urno);
						break;
					}
				}
			} 	
		}
	}
	omContextItem = -1;
}

void StaffTable::OnMenuAbsent()
{
	if (omContextItem != -1)
	{
		int ilItem = -1;
		if (pomActiveViewer->FindShift(omContextItem,ilItem))
		{
			STAFFTABLE_LINEDATA *prlLine = (STAFFTABLE_LINEDATA *) pomActiveStaffTable->GetTextLineData(ilItem);
			if (prlLine != NULL)
			{
				CAbsenceDlg olDlg;
				if(olDlg.DoModal() == IDOK && !olDlg.omAbsenceCode.IsEmpty())
				{
					long llShiftUrno = prlLine->ShiftUrno;
					if (DeleteAssignments(ilItem) == TRUE)
					{
						bool blRc = true;
						SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(llShiftUrno);
						if (prlShift != NULL)
						{
							SHIFTDATA rlOldShift = *prlShift;
							strcpy(prlShift->Sfca,olDlg.omAbsenceCode);
							strcpy(prlShift->Ctyp,"A"); // IsAbsent
							if(ogShiftData.UpdateShift(prlShift,&rlOldShift) == RCFailure)
							{
								// "Error on update staff data"
								MessageBox(GetString(IDS_STRING61497) + ogShiftData.LastError());
								blRc = false;
								*prlShift = rlOldShift;
							}
						}
					}
				}
			}
		}
	}
	omContextItem = -1;
}

//void StaffTable::OnMenuSchulung()
//{
//	if (omContextItem != -1)
//	{
//		STAFFTABLE_LINEDATA *prlLine = (STAFFTABLE_LINEDATA *) pomStaffTable->GetTextLineData(omContextItem);
//		if (prlLine != NULL)
//		{
//			long llShiftUrno = prlLine->ShiftUrno;
//			if (DeleteAssignments(omContextItem) == TRUE)
//			{
//				SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(llShiftUrno);
//				if (prlShift != NULL)
//				{
//					strcpy(prlShift->Sfca,"L");
//					strcpy(prlShift->Ctyp,"A"); // IsAbsent
//					ogShiftData.UpdateShift(prlShift);
//				}
//			}
//		}
//	}
//	omContextItem = -1;
//}
//
//void StaffTable::OnMenuUrlaub()
//{
//	if (omContextItem != -1)
//	{
//		STAFFTABLE_LINEDATA *prlLine = (STAFFTABLE_LINEDATA *) pomStaffTable->GetTextLineData(omContextItem);
//		if (prlLine != NULL)
//		{
//			long llShiftUrno = prlLine->ShiftUrno;
//			if (DeleteAssignments(omContextItem) == TRUE)
//			{
//				SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(llShiftUrno);
//				if (prlShift != NULL)
//				{
//					strcpy(prlShift->Sfca,"U");
//					strcpy(prlShift->Ctyp,"A"); // IsAbsent
//					ogShiftData.UpdateShift(prlShift);
//				}
//			}
//		}
//	}
//	omContextItem = -1;
//}


void StaffTable::OnMenuDelete(UINT nID)
{
	DeleteAssignment(nID - 15);
}

void StaffTable::DeleteAssignment(int ipIndex)
{
	if (omContextItem != -1)
	{
		int ilItem = -1;
		if (pomActiveViewer->FindShift(omContextItem,ilItem) && ipIndex < omAssignUrnos.GetSize())
		{
			pomActiveViewer->DeleteAssignmentJob(omAssignUrnos[ipIndex]);
		}
	}
}


BOOL StaffTable::DeleteAssignments(int ipItem)
{
	int ilRc = TRUE;
	STAFFTABLE_LINEDATA *prlLine = (STAFFTABLE_LINEDATA *) pomActiveStaffTable->GetTextLineData(ipItem);
	if (prlLine != NULL)
	{
		while (prlLine->Assignment.GetSize() > 0 && ilRc == TRUE)
		{
			int iltmp = prlLine->Assignment.GetSize();

			STAFFTABLE_ASSIGNMENT *prlAssignment = &prlLine->Assignment[0];
			if (prlAssignment != NULL)
			{
				ilRc = pomActiveViewer->DeleteAssignmentJob(prlAssignment->Urno);
			}
		}
	}
	return ilRc;
}

////////////////////////////////////////////////////////////////////////
// StaffTable -- implementation of DDX call back function

void StaffTable::StaffTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
	StaffTable *polTable = (StaffTable *)popInstance;

	if(ipDDXType == REDISPLAY_ALL)
	{
		polTable->UpdateView();
		polTable->SetCaptionText();
	}
	else if(ipDDXType == GLOBAL_DATE_UPDATE)
	{
		polTable->HandleGlobalDateUpdate();
	}
}

//*****
// Id 28-Sep-96
// Dropping to the staff table is still missing. It was not included in our final
// drag-and-drop list (may be unimportant). But it looks like that we should do
// this some days later. So I leave the comment about dropping below.
//
/////////////////////////////////////////////////////////////////////////////
// StaffTable -- drag-and-drop functionalities
//
// The user may drag from:
//	- any non-empty line (for dragging the duty job).
//
// and the user will have opportunities to:
//	-- drop from FlightBar onto any non-empty line.
//		We will create a normal flight job for that staff. If the staff is a
//		flight manager, we will create JobFlightManager (JTCO = "FMJ") for that flight.
//	-- drop from the background area from FlightDetailWindow onto the Vertical Scale,
//		the duty bar, or the job bars.
//		(Check the correct GateArea-Pool combination before dropping)
//		We will create a normal flight job (without demand) for that staff.
//	-- drop from DemandBar onto the Vertical Scale, the duty bar, or the job bars.
//		(Check the correct GateArea-Pool combination before dropping)
//		We will create a normal flight job for that staff to resolve that demand.
//	-- drop from gate (vertical scale) in GateDiagram onto any non-empty line.
//		We will ask the user for the period of time and create many flight jobs for
//		this employees for demands in that Gate.
//	-- drop from CCI-Desk (vertical scale) in CciDiagram (to create JobCci)
//		We will ask the user for the period of time and create a JobCciDesk for
//		this employee. (use the default time as the default).
//
LONG StaffTable::OnTableDragBegin(UINT wParam, LONG lParam)
{
    CListBox *polListBox = pomActiveStaffTable->GetCTableListBox();
	if (polListBox && polListBox->GetSelCount() > 0)
	{
		int *polSelectedLines = new int[polListBox->GetSelCount()];
		polListBox->GetSelItems(polListBox->GetSelCount(),polSelectedLines);

		int ilNumSelLines = polListBox->GetSelCount();
		if(ilNumSelLines > 0)
		{
			CCSPtrArray <JOBDATA> olPoolJobs;
			for (int i = 0; i < ilNumSelLines; i++)
			{
				STAFFTABLE_LINEDATA *prlLine = (STAFFTABLE_LINEDATA *) pomActiveStaffTable->GetTextLineData(polSelectedLines[i]);
				if(prlLine)
				{
					ogJobData.GetJobsByShur(olPoolJobs, prlLine->ShiftUrno, TRUE, false);
				}
			}
			int ilNumPoolJobs = olPoolJobs.GetSize();
			if(ilNumPoolJobs > 0)
			{
				omDragDropObject.CreateDWordData(DIT_DUTYBAR, ilNumPoolJobs+1);
				for(int ilPJ = 0; ilPJ < ilNumPoolJobs; ilPJ++)
				{
					omDragDropObject.AddDWord(olPoolJobs[ilPJ].Urno);
				}
				omDragDropObject.AddDWord(0); // team allocation
				omDragDropObject.BeginDrag();
			}
		}
	}

	
//	STAFFTABLE_LINEDATA *prlLine = (STAFFTABLE_LINEDATA *)pomStaffTable->GetTextLineData(wParam);
//	if(prlLine != NULL && prlLine->Assignment.GetSize() > 0)
//	{
//		// searching for a pool job for this staff (we assume there is at least one assignment)
//		const int TITLE_COLUMNS = 8;
//		const int COLUMNS_PER_ASSIGNMENT = 2;
//		int ilAssignmentno = (pomStaffTable->imCurrentColumn - TITLE_COLUMNS) / COLUMNS_PER_ASSIGNMENT;
//		if (!(0 <= ilAssignmentno && ilAssignmentno <= prlLine->Assignment.GetSize()-1))
//			ilAssignmentno = 0;
//
//		// we have to detect the type of the job
//		JOBDATA *prlJob = ogJobData.GetJobByUrno(prlLine->Assignment[ilAssignmentno].Urno);
//		if (CString(prlJob->Jtco) == JOBPOOL)	// dragging a job bar?
//		{
//			omDragDropObject.CreateDWordData(DIT_DUTYBAR, 2);
//			omDragDropObject.AddDWord(prlJob->Urno);
//			omDragDropObject.AddDWord(0); // team allocation is false
//			omDragDropObject.BeginDrag();
//			pomStaffTable->GetCTableListBox()->SetSel(-1,FALSE);
//		}
//		else if (CString(prlJob->Jtco) == JOBFMGATEAREA)	// dragging a flight manager gate area job?
//		{
//			omDragDropObject.CreateDWordData(DIT_FMDETAIL, 1);
//			omDragDropObject.AddDWord(prlJob->Urno);
//			omDragDropObject.BeginDrag();
//			pomStaffTable->GetCTableListBox()->SetSel(-1,FALSE);
//		}
//	}
	return 0L;
}

void StaffTable::OnClose() 
{
   	pogButtonList->m_wndStaffTable = NULL;
	pogButtonList->m_StaffTableButton.Recess(FALSE);
	CDialog::OnClose();
}

void StaffTable::OnSelchangeDate() 
{
	SetViewerDate();
	UpdateView();
}

void StaffTable::SetViewerDate()
{
	CTime olStart = ogBasicData.GetTimeframeStart();
	CTime olDay = CTime(olStart.GetYear(),olStart.GetMonth(),olStart.GetDay(),0,0,0)  + CTimeSpan(m_Date.GetCurSel(),0,0,0);

	omViewer.omStartTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0);
	omViewer.omEndTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59);
	
	//Singapore
	if(bmIsPaxServiceT2 == true)
	{
		omViewerT3.omStartTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0);
		omViewerT3.omEndTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59);
	}
}

// called in response to a global date update - new day selected ie in preplanning so syschronise the date of this display
void StaffTable::HandleGlobalDateUpdate()
{
	m_Date.SetCurSel(ogBasicData.GetDisplayDateOffset());
	OnSelchangeDate();
}

void StaffTable::SetDate(CTime opDate)
{
	m_Date.SetCurSel(ogBasicData.GetDisplayDateOffsetByDate(opDate));
	OnSelchangeDate();
}

//Singapore
void StaffTable::OnTableUpdateDataCount()
{	
	if(bmIsPaxServiceT2 == true)
	{
		CWnd* polWnd = GetDlgItem(IDC_DATACOUNT);
		if(polWnd != NULL)
		{
			char olDataCountT2[10];
			char olDataCountT3[10];
			polWnd->EnableWindow();			
			itoa(omViewer.Lines(),olDataCountT2,10);
			itoa(omViewerT3.Lines(),olDataCountT3,10);		
			CString olText;
			olText.Format("%s/%s",olDataCountT2,olDataCountT3);
			polWnd->SetWindowText(olText);
		}
	}
}

//Singapore
void StaffTable::OnTableSetTableWindow(WPARAM wParam, LPARAM lParam)
{
	if(bmIsPaxServiceT2 == true)
	{
		CTable* polTable = (CTable*)lParam;	
		if(pomStaffTable == polTable)
		{
			omActiveTerminal = Terminal2;
			pomActiveStaffTable = polTable;
			pomActiveViewer = &omViewer;
			pomStaffTableT3->GetCTableListBox()->SetSel(-1,FALSE);
		}
		else if(pomStaffTableT3 == polTable)
		{
			omActiveTerminal = Terminal3;
			pomActiveStaffTable = polTable;
			pomActiveViewer = &omViewerT3;
			pomStaffTable->GetCTableListBox()->SetSel(-1,FALSE);
		}
	}
}

//Singapore
void StaffTable::OnButtonT2()
{
	CRect olClientRect;
	GetWindowRect(&olClientRect);
	ScreenToClient(&olClientRect);

	CButton* polButton = (CButton*)GetDlgItem(IDC_BUTTONT2);	
	bmIsT2Visible = !bmIsT2Visible;
	
	if(bmIsT2Visible == TRUE)
	{
		m_bTerminal2.SetColors(LIME, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}
	else
	{
		m_bTerminal2.SetColors(::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}
	
	if(bmIsT2Visible == FALSE && bmIsT3Visible == FALSE)
	{	
		pomStaffTable->ShowWindow(SW_HIDE);
	}	
	else if(bmIsT2Visible == TRUE && bmIsT3Visible == FALSE)
	{	
		pomStaffTable->ShowWindow(SW_SHOW);
				
		CRect olRectT2;
		pomStaffTable->GetWindowRect(&olRectT2);
		ScreenToClient(&olRectT2);
		
		CRect olRectT3;
		pomStaffTableT3->GetWindowRect(&olRectT3);
		ScreenToClient(&olRectT3);

		pomStaffTable->SetPosition(-1,olClientRect.right-3,olRectT2.top,olClientRect.bottom-3);
	}
	if(bmIsT2Visible == FALSE && bmIsT3Visible == TRUE)
	{		
		CRect olRectT2;
		pomStaffTable->GetWindowRect(&olRectT2);
		ScreenToClient(&olRectT2);

		CRect olRectT3;
		pomStaffTableT3->GetWindowRect(&olRectT3);
		ScreenToClient(&olRectT3);

		pomStaffTable->ShowWindow(SW_HIDE);
		pomStaffTableT3->SetPosition(-1,olClientRect.right-3,olRectT2.top,olClientRect.bottom-3);
	}
	else if(bmIsT2Visible == TRUE && bmIsT3Visible == TRUE)
	{		
		pomStaffTable->ShowWindow(SW_SHOW);
		
		CRect olRectT2;
		pomStaffTable->GetWindowRect(&olRectT2);
		ScreenToClient(&olRectT2);

		CRect olRectT3;
		pomStaffTableT3->GetWindowRect(&olRectT3);
		ScreenToClient(&olRectT3);

		pomStaffTable->SetPosition(-1,olClientRect.right-3,olRectT2.top,(olClientRect.bottom - olRectT2.top)/2);
		pomStaffTableT3->SetPosition(-1,olClientRect.right-3,(olClientRect.bottom - olRectT2.top)/2,olClientRect.bottom-3);
	}
}

//Singapore
void StaffTable::OnButtonT3()
{
	CRect olClientRect;
	GetWindowRect(&olClientRect);
	ScreenToClient(&olClientRect);

	CButton* polButton = (CButton*)GetDlgItem(IDC_BUTTONT3);	
	bmIsT3Visible = !bmIsT3Visible;
	
	if(bmIsT3Visible == TRUE)
	{
		m_bTerminal3.SetColors(LIME, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}
	else
	{
		m_bTerminal3.SetColors(::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}

	if(bmIsT3Visible == FALSE && bmIsT2Visible == FALSE)
	{	
		pomStaffTableT3->ShowWindow(SW_HIDE);

	}	
	else if(bmIsT3Visible == TRUE && bmIsT2Visible == FALSE)
	{	
		pomStaffTableT3->ShowWindow(SW_SHOW);
				
		CRect olRectT2;
		pomStaffTable->GetWindowRect(&olRectT2);
		ScreenToClient(&olRectT2);
		
		CRect olRectT3;
		pomStaffTableT3->GetWindowRect(&olRectT3);
		ScreenToClient(&olRectT3);

		pomStaffTableT3->SetPosition(-1,olClientRect.right-3,olRectT2.top,olClientRect.bottom-3);
	}
	else if(bmIsT3Visible == FALSE && bmIsT2Visible == TRUE)
	{	
		CRect olRectT2;
		pomStaffTable->GetWindowRect(&olRectT2);
		ScreenToClient(&olRectT2);

		CRect olRectT3;
		pomStaffTableT3->GetWindowRect(&olRectT3);
		ScreenToClient(&olRectT3);
						
		pomStaffTableT3->ShowWindow(SW_HIDE);
		pomStaffTable->SetPosition(-1,olClientRect.right-3,olRectT2.top,olClientRect.bottom-3);
	}
	else if(bmIsT3Visible == TRUE && bmIsT2Visible == TRUE)
	{
		pomStaffTableT3->ShowWindow(SW_SHOW);

		CRect olRectT2;
		pomStaffTable->GetWindowRect(&olRectT2);
		ScreenToClient(&olRectT2);

		CRect olRectT3;
		pomStaffTableT3->GetWindowRect(&olRectT3);
		ScreenToClient(&olRectT3);
		
		pomStaffTable->SetPosition(-1,olClientRect.right-3,olRectT2.top,(olClientRect.bottom - olRectT2.top)/2);
		pomStaffTableT3->SetPosition(-1,olClientRect.right-3,(olClientRect.bottom - olRectT2.top)/2,olClientRect.bottom -3);
	}
}

void StaffTable::PrepareBothTerminalPrintData(StaffTableViewer& ropStaffTableViewer/*OUT*/) const
{
	CCSPtrArray<STAFFTABLE_LINEDATA> olLines;
	STAFFTABLE_LINEDATA olLineData;					
	omViewer.GetLines(olLines);
	for(int i = 0 ; i < olLines.GetSize(); i++)
	{
		olLineData = olLines.GetAt(i);
		ropStaffTableViewer.GetLines().NewAt(ropStaffTableViewer.GetLines().GetSize(),olLineData);
	}
	omViewerT3.GetLines(olLines);
	for(i = 0 ; i < olLines.GetSize(); i++)
	{
		olLineData = olLines.GetAt(i);
		ropStaffTableViewer.GetLines().NewAt(ropStaffTableViewer.GetLines().GetSize(),olLineData);
	}
}

void StaffTable::DeleteViewerLineData(StaffTableViewer& ropStaffTableViewer/*IN*/)
{
	CCSPtrArray<STAFFTABLE_LINEDATA> olLines;
	ropStaffTableViewer.GetLines(olLines);
	for(int i = 0; i < olLines.GetSize(); i++)
	{
		olLines.GetAt(i).Assignment.DeleteAll();
	}
	ropStaffTableViewer.GetLines().DeleteAll();
}

void StaffTable::OnPrintPreview()
{
	CSelReportDlg olSelReportDlg;
	olSelReportDlg.SetReports(GetString(IDS_STAFFTABLE_PRINT1));
	olSelReportDlg.SetReports(GetString(IDS_STAFFTABLE_PRINT2));
	olSelReportDlg.SetReports(GetString(IDS_STAFFTABLE_PRINT3));
	if(olSelReportDlg.DoModal() == IDOK)
	{
		omSelectedReport = olSelReportDlg.omSelectedReport;
		
		CPrintTerminalSelection olTerminalSelection;
		olTerminalSelection.SetWindowHeaderText(omSelectedReport);
		if(bmIsPaxServiceT2 == true)
		{
			if(omSelectedReport.IsEmpty())
			{
				return;
			}
			UINT iResponse = 0;
			if(strcmp(omSelectedReport,GetString(IDS_STAFFTABLE_EXPORT1)) != 0
			   && (iResponse = olTerminalSelection.DoModal()) == IDOK)
			{
				if(olTerminalSelection.IsTerminal3Selected() == TRUE)
				{
					imSelectedTerminal = CPrintTerminalSelection::TERMINAL3_SELECTED;
				}
				else if(olTerminalSelection.IsBothTerminalSelected() == TRUE)
				{
					imSelectedTerminal = CPrintTerminalSelection::BOTH_TERMINAL_SELECTED;
				}
				else
				{
					imSelectedTerminal = CPrintTerminalSelection::TERMINAL2_SELECTED;
				}
			}
			else if(iResponse == IDCANCEL)
			{
				return;
			}
		}

		if (!pomTemplate)
		{
			pomTemplate = new CSingleDocTemplate(
				IDR_MENU_PRINT_PREVIEW,
				NULL,
				RUNTIME_CLASS(CFrameWnd),
				RUNTIME_CLASS(CPrintPreviewView));
			AfxGetApp()->AddDocTemplate(pomTemplate);
		}

		CFrameWnd * pFrameWnd = pomTemplate->CreateNewFrame( NULL,NULL);	
		pomTemplate->InitialUpdateFrame( pFrameWnd, NULL, FALSE);
		pomPrintPreviewView =(CPrintPreviewView*)pFrameWnd->GetActiveView();
		pomPrintPreviewView->pomCallMsgWindow=this;	
		pomPrintPreviewView->SetFrameWindow(pFrameWnd);
		pFrameWnd->SetWindowText(_T("Preplan Print Preview"));	
		pomPrintPreviewView->OnFilePrintPreview();	
		return;
		//PrintData();
	}
}

void StaffTable::OnBeginPrinting(WPARAM wParam, LPARAM lParam)
{
	PRINT_PREVIEW_DCINFO* polPrintPreviewDcInfo = (PRINT_PREVIEW_DCINFO*)lParam;
	if (!polPrintPreviewDcInfo->pomDC|| !polPrintPreviewDcInfo->pomPrintInfo) return;
	
	if(imSelectedTerminal == CPrintTerminalSelection::BOTH_TERMINAL_SELECTED)
	{
		omBothViewer.Attach(pomStaffTable);
		PrepareBothTerminalPrintData(omBothViewer);
	}

	int ilMaxPage = 0;

	if(imSelectedTerminal == CPrintTerminalSelection::TERMINAL3_SELECTED)
	{
		pomStaffTableViewer = &omViewerT3;
	}
	else if(imSelectedTerminal == CPrintTerminalSelection::BOTH_TERMINAL_SELECTED)
	{		
		pomStaffTableViewer = &omBothViewer;
	}
	else
	{
		pomStaffTableViewer = &omViewer;
	}
	
	CString omTarget = GetString(IDS_STRING33141) + CString(pomStaffTableViewer->omStartTime.Format("%d.%m.%Y %H:%M")) + GetString(IDS_STRING33142) + CString(pomStaffTableViewer->omEndTime.Format("%d.%m.%Y %H:%M"));
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y %H:%M");
	pomStaffTableViewer->GetCCSPrinter() = new CCSPrint(this,PRINT_LANDSCAPE,60,500,150,GetString(IDS_STAFFJOBS),olPrintDate,omTarget);
	//}
	pomStaffTableViewer->GetCCSPrinter()->pomCdc = polPrintPreviewDcInfo->pomDC;
	pomStaffTableViewer->GetCCSPrinter()->InitializePrintSetup(CCSPrint::PRINT_PREVIEW);

	int ilNoOfPages = (pomStaffTableViewer->GetLines().GetSize() % pomStaffTableViewer->GetCCSPrinter()->imMaxLines) == 0 ? (pomStaffTableViewer->GetLines().GetSize() / pomStaffTableViewer->GetCCSPrinter()->imMaxLines) : (pomStaffTableViewer->GetLines().GetSize() / pomStaffTableViewer->GetCCSPrinter()->imMaxLines) + 1;
	polPrintPreviewDcInfo->pomPrintInfo->SetMaxPage(ilNoOfPages);
	polPrintPreviewDcInfo->pomPrintInfo->m_nCurPage = 1;
	bgPrintPreviewMode = TRUE;
	pogPreviewPrintWnd = this;
	pomPrintPreviewView->GetFrameWindow()->BeginModalState();
}

void StaffTable::PrintPreview(WPARAM wParam, LPARAM lParam)
{
	PRINT_PREVIEW_DCINFO* polPrintPreviewDcInfo = (PRINT_PREVIEW_DCINFO*)lParam;
	if (!polPrintPreviewDcInfo->pomDC|| !polPrintPreviewDcInfo->pomPrintInfo) return;

	pomStaffTableViewer->GetCCSPrinter()->pomCdc = polPrintPreviewDcInfo->pomDC;
	pomStaffTableViewer->GetCCSPrinter()->InitializePrintSetup(CCSPrint::PRINT_PREVIEW);
	pomStaffTableViewer->GetCCSPrinter()->pomCdc->SetMapMode(MM_ANISOTROPIC);	
	pomStaffTableViewer->GetCCSPrinter()->pomCdc->SetWindowExt(pomStaffTableViewer->GetCCSPrinter()->pomCdc->GetDeviceCaps(HORZRES)*1.1,pomStaffTableViewer->GetCCSPrinter()->pomCdc->GetDeviceCaps(VERTRES)*1.1);
	pomStaffTableViewer->GetCCSPrinter()->pomCdc->SetViewportExt(pomStaffTableViewer->GetCCSPrinter()->pomCdc->GetDeviceCaps(HORZRES),pomStaffTableViewer->GetCCSPrinter()->pomCdc->GetDeviceCaps(VERTRES));	

	//PrintPreviewData(pomPrintPreview,polPrintPreviewDcInfo->pomPrintInfo->m_nCurPage);

	if(!strcmp(omSelectedReport,GetString(IDS_STAFFTABLE_PRINT1)))
	{	
		pomStaffTableViewer->PrintPreview(polPrintPreviewDcInfo->pomPrintInfo->m_nCurPage);
	}
	else if(!strcmp(omSelectedReport,GetString(IDS_STAFFTABLE_PRINT2)))
	{
		//pomStaffTableViewer->PrintView2();
	}
	else if(!strcmp(omSelectedReport,GetString(IDS_STAFFTABLE_PRINT3)))
	{
		//pomStaffTableViewer->PrintView3();
	}

}

void StaffTable::OnEndPrinting(WPARAM wParam, LPARAM lParam)
{	
	pomPrintPreviewView->GetFrameWindow()->EndModalState();
	if(pomStaffTableViewer->GetCCSPrinter() != NULL)
	{
		delete pomStaffTableViewer->GetCCSPrinter();
	}
	omBothViewer.Attach(NULL);
	pomPrintPreviewView = NULL;
	pomStaffTableViewer = NULL;
	bgPrintPreviewMode = FALSE;
}

void StaffTable::OnPreviewPrint(WPARAM wParam, LPARAM lParam)
{
	PrintData();
	pogPreviewPrintWnd = NULL;
}

void StaffTable::OnMove(int x, int y)
{	
	CDialog::OnMove(x,y);
	if(this->IsIconic() == FALSE)
	{
		GetWindowRect(&omWindowRect);
	}
}