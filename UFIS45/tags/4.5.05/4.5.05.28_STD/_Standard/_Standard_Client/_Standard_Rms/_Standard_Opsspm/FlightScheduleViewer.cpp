// flviewer.cpp : implementation file
//

#include <stdafx.h>
#include <CCSGlobl.h>

#include <CedaEmpData.h>
#include <CedaJobData.h>
#include <CedaFlightData.h>
#include <PrintControl.h>
#include <FlightScheduleViewer.h>
#include <Search.h>
#include <ccsddx.h>
#include <BasicData.h>
#include <CedaAltData.h>
#include <CedaCcaData.h>
#include <CedaPstData.h>
#include <CedaPaxData.h>

class FlightPlan; //Singapore
#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsWithIn(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))

// Local function prototype
static void FlightScheduleTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

extern CStringArray ogFlightAlcd;

enum {
    BY_ALCD,
    BY_GATA,
    BY_GATD,
    BY_PSTA,
    BY_PSTD,
	BY_STOA,
	BY_STOD,
    BY_NONE
};

FlightScheduleViewer::FlightScheduleViewer()
{
	bmIsFromSearch = FALSE;
    SetViewerKey("FltSched");
    pomTable = NULL;
	ogCCSDdx.Register(this, FLIGHT_CHANGE, CString("FLPLAN"),CString("Flight Change"), FlightScheduleTableCf);
	ogCCSDdx.Register(this, FLIGHT_DELETE, CString("FLPLAN"),CString("Flight Delete"), FlightScheduleTableCf);
	ogCCSDdx.Register(this, FLIGHT_SELECT_FLIGHTLIST, CString("FLPLAN"),CString("Flight Select"), FlightScheduleTableCf);


	CTime olDay = ogBasicData.GetTimeframeStart();
	omStartTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0);
	omEndTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59);
}

FlightScheduleViewer::~FlightScheduleViewer()
{
	TRACE("FlightScheduleViewer::~FlightScheduleViewer()\n");
	ogCCSDdx.UnRegister(this, NOTUSED);
    DeleteAll();
}

void FlightScheduleViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

void FlightScheduleViewer::DeleteLine(int ipLineno)
{
	if(ipLineno >= 0 && ipLineno < omLines.GetSize())
	{
		omArrMap.RemoveKey((void *) omLines[ipLineno].ArrUrno);
		omDepMap.RemoveKey((void *) omLines[ipLineno].DepUrno);
		omLines.DeleteAt(ipLineno);
	}
}

void FlightScheduleViewer::Attach(CTable *popTable)
{
    pomTable = popTable;
}

void FlightScheduleViewer::ChangeViewTo(const char *pcpViewName)
{

	AllowUpdates(TRUE);

    DeleteAll();    

	ogCfgData.rmUserSetup.FPLV = pcpViewName;
    SelectView(pcpViewName);

    PrepareGrouping();
    PrepareFilter();
	PrepareSorter();

	MakeLines();

	UpdateDisplay();
	AllowUpdates(FALSE);
}


void FlightScheduleViewer::PrepareGrouping()
{
    CStringArray olSortOrder;
    CViewer::GetSort(olSortOrder);  // we'll always use the first sorting criteria for grouping
	BOOL blIsGrouped = *CViewer::GetGroup() - '0';


	omGroupBy = BY_NONE;
    if (olSortOrder.GetSize() > 0 && blIsGrouped)
    {
        if (olSortOrder[0] == "Airline")
            omGroupBy = BY_ALCD;
        else if (olSortOrder[0] == "Gate Arrival")
            omGroupBy = BY_GATA;
        else if (olSortOrder[0] == "Gate Departure")
            omGroupBy = BY_GATD;
        else if (olSortOrder[0] == "PosArr")
            omGroupBy = BY_PSTA;
        else if (olSortOrder[0] == "PosDep")
            omGroupBy = BY_PSTD;
        else if (olSortOrder[0] == "Stoa")
            omGroupBy = BY_STOA;
        else if (olSortOrder[0] == "Stod")
            omGroupBy = BY_STOD;
    }
}



void FlightScheduleViewer::PrepareFilter()
{
	bmUseAllArrGates = SetFilterMap("Gate Arrival", omCMapForGata, GetString(IDS_ALLSTRING));
	bmUseAllDepGates = SetFilterMap("Gate Departure", omCMapForGatd, GetString(IDS_ALLSTRING));
	bmUseAllArrPositions = SetFilterMap("PosArr", omCMapForPsta, GetString(IDS_ALLSTRING));
	bmUseAllDepPositions = SetFilterMap("PosDep", omCMapForPstd, GetString(IDS_ALLSTRING));
	bmUseAllAirlines = SetFilterMap("Airline", omCMapForAlcd, GetString(IDS_ALLSTRING));

	CStringArray olFilterValues;
	bmDisplayInbound = FALSE;
	bmDisplayOutbound = FALSE;
	bmDisplayPax = FALSE;
	GetFilter("Inbound", olFilterValues);
	if (olFilterValues.GetSize())
	if (olFilterValues[0][0] == '1')
		bmDisplayInbound = TRUE;
	GetFilter("Outbound", olFilterValues);
	if (olFilterValues.GetSize())
		if (olFilterValues[0][0] == '1')
		bmDisplayOutbound = TRUE;
	GetFilter("DisplayPax", olFilterValues);
	if (olFilterValues.GetSize())
	if (olFilterValues[0][0] == '1')
		bmDisplayPax = TRUE;
	
	bmInboundOnly = bmDisplayInbound && !bmDisplayOutbound;
	bmOutboundOnly = bmDisplayOutbound && !bmDisplayInbound;

//	CTime olTmpDate = ogBasicData.GetTime();
//	omStartTime = CTime(olTmpDate.GetYear(),olTmpDate.GetMonth(),olTmpDate.GetDay(),0,0,0);
//	omEndTime = CTime(olTmpDate.GetYear(),olTmpDate.GetMonth(),olTmpDate.GetDay(),0,0,0);
//	GetFilter("Zeit", olFilterValues);
//	int ilDayOffset;
//	CString olFrom;
//	CString olTo;
//	if (olFilterValues.GetSize() > 1)
//	{
//		ilDayOffset = olFilterValues[2][0] - '0';
//		olFrom = olFilterValues[0];
//		olTo = olFilterValues[1];
//		omStartTime += CTimeSpan(ilDayOffset,atoi(olFrom.Left(2)),atoi(olFrom.Right(2)),0);
//		omEndTime += CTimeSpan(ilDayOffset,atoi(olTo.Left(2)),atoi(olTo.Right(2)),0);
//		m_StartDay = omStartTime.Format("%Y%m%d");
//	}
}

void FlightScheduleViewer::PrepareSorter()
{
    CStringArray olSortOrder;
    CViewer::GetSort(olSortOrder);

    omSortOrder.RemoveAll();
    for (int i = 0; i < olSortOrder.GetSize(); i++)
    {
        int ilSortOrderEnumeratedValue;
        if (olSortOrder[i] == "Gate Arrival")
            ilSortOrderEnumeratedValue = BY_GATA;
        else if (olSortOrder[i] == "Gate Departure")
            ilSortOrderEnumeratedValue = BY_GATD;
        else if (olSortOrder[i] == "PosArr")
            ilSortOrderEnumeratedValue = BY_PSTA;
        else if (olSortOrder[i] == "PosDep")
            ilSortOrderEnumeratedValue = BY_PSTD;
        else if (olSortOrder[i] == "Airline")
            ilSortOrderEnumeratedValue = BY_ALCD;
        else if (olSortOrder[i] == "Stoa")
            ilSortOrderEnumeratedValue = BY_STOA;
        else if (olSortOrder[i] == "Stod")
            ilSortOrderEnumeratedValue = BY_STOD;
        else
            ilSortOrderEnumeratedValue = BY_NONE;
        omSortOrder.Add(ilSortOrderEnumeratedValue);
    }
}


BOOL FlightScheduleViewer::IsPassFilter(FLIGHTDATA *prpFlightArr, FLIGHTDATA *prpFlightDep)
{
    void *p;

	if(!bmDisplayInbound && prpFlightDep == NULL)
		return FALSE;

	if(!bmDisplayOutbound && prpFlightArr == NULL)
		return FALSE;

	BOOL blIsArrTimeOk = FALSE;
	if(prpFlightArr != NULL &&
		(IsBetween(prpFlightArr->Stoa,omStartTime,omEndTime) || 
		 IsBetween(prpFlightArr->Etoa,omStartTime,omEndTime) || 
		 IsBetween(prpFlightArr->Tifa,omStartTime,omEndTime)))
	{
		blIsArrTimeOk = TRUE;
	}

	BOOL blIsDepTimeOk = FALSE;
	if(prpFlightDep != NULL && 
		(IsBetween(prpFlightDep->Stod,omStartTime,omEndTime) || 
		 IsBetween(prpFlightDep->Etod,omStartTime,omEndTime) || 
		 IsBetween(prpFlightDep->Tifd,omStartTime,omEndTime)))
	{
		blIsDepTimeOk = TRUE;
	}

	if(bmInboundOnly && !blIsArrTimeOk)
		return FALSE;
	else if(bmOutboundOnly && !blIsDepTimeOk)
		return FALSE;
	else if(!blIsArrTimeOk && !blIsDepTimeOk)
		return FALSE;


	BOOL blIsGataOk = FALSE;
	if(prpFlightArr != NULL)
	{
		if(bmUseAllArrGates)
			blIsGataOk = TRUE;
		else if(strlen(prpFlightArr->Gta1) > 0)
			blIsGataOk = omCMapForGata.Lookup(prpFlightArr->Gta1, p);
		else
			blIsGataOk = omCMapForGata.Lookup(GetString(IDS_WITHOUTGATE_FILTER), p);
	}

	BOOL blIsGatdOk = FALSE;
	if(prpFlightDep != NULL)
	{
		if(bmUseAllDepGates)
			blIsGatdOk = TRUE;
		else if(strlen(prpFlightDep->Gtd1) > 0)
			blIsGatdOk = omCMapForGatd.Lookup(prpFlightDep->Gtd1, p);
		else
			blIsGatdOk = omCMapForGatd.Lookup(GetString(IDS_WITHOUTGATE_FILTER), p);
	}

	if(!blIsGataOk && !blIsGatdOk)
		return FALSE;


	BOOL blIsPstaOk = FALSE;
	if(prpFlightArr != NULL)
	{
		if(bmUseAllArrPositions)
			blIsPstaOk = TRUE;
		else if(strlen(prpFlightArr->Psta) > 0)
			blIsPstaOk = omCMapForPsta.Lookup(prpFlightArr->Psta, p);
		else
			blIsPstaOk = omCMapForPsta.Lookup(GetString(IDS_WITHOUTPOSITION_FILTER), p);
	}

	BOOL blIsPstdOk = FALSE;
	if(prpFlightDep != NULL)
	{
		if(bmUseAllDepPositions)
			blIsPstdOk = TRUE;
		else if(strlen(prpFlightDep->Pstd) > 0)
			blIsPstdOk = omCMapForPstd.Lookup(prpFlightDep->Pstd, p);
		else
			blIsPstdOk = omCMapForPstd.Lookup(GetString(IDS_WITHOUTPOSITION_FILTER), p);
	}

	if(!blIsPstaOk && !blIsPstdOk)
		return FALSE;


	BOOL blIsAlcdOk = bmUseAllAirlines;
	if(!bmUseAllAirlines)
	{
		if(prpFlightArr != NULL && omCMapForAlcd.Lookup(ogAltData.FormatAlcString(prpFlightArr->Alc2,prpFlightArr->Alc3), p))
		{
			blIsAlcdOk = TRUE;
		}
		else if(prpFlightDep != NULL && omCMapForAlcd.Lookup(ogAltData.FormatAlcString(prpFlightDep->Alc2,prpFlightDep->Alc3), p))
		{
			blIsAlcdOk = TRUE;
		}
	}

	if(!blIsAlcdOk)
		return FALSE;

	return TRUE;
}

int FlightScheduleViewer::CompareFlight(FLIGHTSCHEDULE_LINEDATA *prpFlight1, 
									    FLIGHTSCHEDULE_LINEDATA *prpFlight2)
{
	CTime olTmp1, olTmp2;
    int n = omSortOrder.GetSize();
    for (int i = 0; i < n; i++)
    {
        int ilCompareResult = 0;

        switch (omSortOrder[i])
        {
        case BY_GATA:
            ilCompareResult = (prpFlight1->Alida == prpFlight2->Alida)? 0:
                (prpFlight1->Alida > prpFlight2->Alida)? 1: -1;
            break;
        case BY_GATD:
            ilCompareResult = (prpFlight1->Alidd == prpFlight2->Alidd)? 0:
                (prpFlight1->Alidd > prpFlight2->Alidd)? 1: -1;
            break;
        case BY_PSTA:
            ilCompareResult = (prpFlight1->Posia == prpFlight2->Posia)? 0:
                (prpFlight1->Posia > prpFlight2->Posia)? 1: -1;
            break;
        case BY_PSTD:
            ilCompareResult = (prpFlight1->Posid == prpFlight2->Posid)? 0:
                (prpFlight1->Posid > prpFlight2->Posid)? 1: -1;
            break;
        case BY_ALCD:
            ilCompareResult = (prpFlight1->Alcd == prpFlight2->Alcd)? 0:
                (prpFlight1->Alcd > prpFlight2->Alcd)? 1: -1;
            break;
        case BY_STOA:
			olTmp1 = (prpFlight1->Stoa != TIMENULL) ? prpFlight1->Stoa : prpFlight1->Stod;
			olTmp2 = (prpFlight2->Stoa != TIMENULL) ? prpFlight2->Stoa : prpFlight2->Stod;
            ilCompareResult = olTmp1.GetTime() - olTmp2.GetTime();
            break;
        case BY_STOD:
			olTmp1 = (prpFlight1->Stod != TIMENULL) ? prpFlight1->Stod : prpFlight1->Stoa;
			olTmp2 = (prpFlight2->Stod != TIMENULL) ? prpFlight2->Stod : prpFlight2->Stoa;
            ilCompareResult = olTmp1.GetTime() - olTmp2.GetTime();
            break;
        }
        if (ilCompareResult != 0)
            return ilCompareResult;
    }

    return 0;   

}


/////////////////////////////////////////////////////////////////////////////
// FlightScheduleViewer -- code specific to this class


BOOL FlightScheduleViewer::IsSameGroup(FLIGHTSCHEDULE_LINEDATA *prpFlight1,
	FLIGHTSCHEDULE_LINEDATA *prpFlight2)
{


    // Compare in the sort order, from the outermost to the innermost
    switch (omGroupBy)
    {
    case BY_ALCD:
        return (prpFlight1->Alcd == prpFlight2->Alcd);
    case BY_GATA:
        return (prpFlight1->Alida == prpFlight2->Alida);
    case BY_GATD:
        return (prpFlight1->Alidd == prpFlight2->Alidd);
    case BY_PSTA:
        return (prpFlight1->Posia == prpFlight2->Posia);
    case BY_PSTD:
        return (prpFlight1->Posid == prpFlight2->Posid);
    case BY_STOA:
        return (prpFlight1->Stoa == prpFlight2->Stoa);
    case BY_STOD:
        return (prpFlight1->Stod == prpFlight2->Stod);
    }

    return TRUE;    // assume there's no grouping at all
}


void FlightScheduleViewer::MakeLines()
{
	omArrMap.RemoveAll();
	omDepMap.RemoveAll();

	int ilFlightCount = ogFlightData.omData.GetSize();
	for (int i = 0; i < ilFlightCount; i++) 
	{
		MakeLine(&ogFlightData.omData[i]);
	}
}

void FlightScheduleViewer::MakeLine(FLIGHTDATA *prpFlight1, bool bpUpdateTable /*false*/)
{
	if(prpFlight1 != NULL)
	{
		FLIGHTDATA *prlFlight2 = NULL;
		if(*prpFlight1->Adid == 'B')
		{
			// handle it as an arrival
			prlFlight2 = ogFlightData.GetRotationFlight(prpFlight1, ID_ARR_RETURNFLIGHT);
			MakeLine(prpFlight1,prlFlight2,bpUpdateTable);

			// handle it as a departure
			prlFlight2 = ogFlightData.GetRotationFlight(prpFlight1, ID_DEP_RETURNFLIGHT);
			MakeLine(prlFlight2,prpFlight1,bpUpdateTable);
		}
		else
		{
			prlFlight2 = ogFlightData.GetRotationFlight(prpFlight1);
			if(ogFlightData.IsDeparture(prpFlight1) || ogFlightData.IsArrival(prlFlight2))
			{
				FLIGHTDATA *prlTmp = prpFlight1;
				prpFlight1 = prlFlight2;
				prlFlight2 = prlTmp;
			}
			MakeLine(prpFlight1,prlFlight2,bpUpdateTable);
		}
	}
}

void FlightScheduleViewer::MakeLine(FLIGHTDATA *prpFlightArr, FLIGHTDATA *prpFlightDep, bool bpUpdateTable /*false*/)
{
	if(!IsPassFilter(prpFlightArr,prpFlightDep))
		return;

	void *pvlDummy;
	if(prpFlightArr != NULL)
	{
		if(omArrMap.Lookup((void *) prpFlightArr->Urno, (void *) pvlDummy))
			return;
		omArrMap.SetAt((void *) prpFlightArr->Urno, (void *) NULL);
	}
	if(prpFlightDep != NULL)
	{
		if(omDepMap.Lookup((void *) prpFlightDep->Urno, (void *) pvlDummy))
			return;
		omDepMap.SetAt((void *) prpFlightDep->Urno, (void *) NULL);
	}


	FLIGHTSCHEDULE_LINEDATA rlFlight;
	
	rlFlight.Alcd = prpFlightArr ? ogAltData.FormatAlcString(prpFlightArr->Alc2,prpFlightArr->Alc3) : ogAltData.FormatAlcString(prpFlightDep->Alc2,prpFlightDep->Alc3); 
	rlFlight.Regn = (prpFlightArr ? prpFlightArr->Regn : prpFlightDep->Regn);
	rlFlight.Actl = (prpFlightArr ? prpFlightArr->Act3:prpFlightDep->Act3);

	rlFlight.Fnuma = (prpFlightArr ? prpFlightArr->Fnum : CString(""));
	rlFlight.Rou1a = (prpFlightArr ? prpFlightArr->Org3 : CString(""));
	rlFlight.Rou2a = (prpFlightArr ? prpFlightArr->Via3 : CString(""));
	rlFlight.Stoa  = (prpFlightArr ? prpFlightArr->Stoa : TIMENULL);
	rlFlight.Etoa  = (prpFlightArr ? prpFlightArr->Etoa : TIMENULL);
	rlFlight.Tmot  = (prpFlightArr ? prpFlightArr->Tmoa : TIMENULL);
	rlFlight.Land  = (prpFlightArr ? prpFlightArr->Land : TIMENULL);
	rlFlight.Onbl  = (prpFlightArr ? prpFlightArr->Onbl : TIMENULL);
	rlFlight.Posia = (prpFlightArr ? prpFlightArr->Psta : CString(""));
	rlFlight.Alida = (prpFlightArr ? prpFlightArr->Gta1 : CString(""));
	rlFlight.VaArr = (prpFlightArr ? prpFlightArr->Ttyp : CString("")); 	
	rlFlight.Blt1  = (prpFlightArr ? prpFlightArr->Blt1 : CString("")); 	
	rlFlight.ArrUrno =  (prpFlightArr ? prpFlightArr->Urno : -1L);
	if(ogFlightData.IsReturnFlight(prpFlightArr))
		rlFlight.ArrReturnFlightType = ID_ARR_RETURNFLIGHT;

	rlFlight.Fnumd = (prpFlightDep ? prpFlightDep->Fnum : CString(""));
	rlFlight.Rou1d = (prpFlightDep ? prpFlightDep->Des3 : CString(""));
	rlFlight.Rou2d = (prpFlightDep ? prpFlightDep->Via3 : CString(""));
	rlFlight.Stod  = (prpFlightDep ? prpFlightDep->Stod : TIMENULL);
	rlFlight.Etod  = (prpFlightDep ? prpFlightDep->Etod : TIMENULL);
	rlFlight.Ofbl  = (prpFlightDep ? prpFlightDep->Ofbl : TIMENULL);
	rlFlight.Airb  = (prpFlightDep ? prpFlightDep->Airb : TIMENULL);
	rlFlight.Posid = (prpFlightDep ? prpFlightDep->Pstd : CString(""));
	rlFlight.Alidd = (prpFlightDep ? prpFlightDep->Gtd1 : CString(""));
	rlFlight.VaDep = (prpFlightDep ? prpFlightDep->Ttyp : CString("")); 	
	rlFlight.Cki   = (prpFlightDep ? ogCcaData.GetCounterRangeForFlight(prpFlightDep->Urno):CString("")); // return range og checkin counters for the flight eg. "321-323"

	long llFlur = (prpFlightDep) ? prpFlightDep->Urno : 0L;
	int ilF, ilB, ilE, ilP, ilT;
	ogPaxData.GetBookedPassengers(llFlur, ilF, ilB, ilE, ilP, ilT);
	rlFlight.Pax.Format("%d/%d/%d/%d", ilF, ilB, ilE, ilP);

	rlFlight.DepUrno =  (prpFlightDep ? prpFlightDep->Urno : -1L);
	if(ogFlightData.IsReturnFlight(prpFlightDep))
		rlFlight.DepReturnFlightType = ID_DEP_RETURNFLIGHT;

	int ilNewLine = CreateLine(&rlFlight);
	if(bpUpdateTable && ilNewLine >= 0 && ilNewLine < omLines.GetSize())
	{
        pomTable->InsertTextLine(ilNewLine, Format(&omLines[ilNewLine]), &omLines[ilNewLine]);
	}
}

int FlightScheduleViewer::CreateLine(FLIGHTSCHEDULE_LINEDATA *prpFlight)
{
    int ilLineCount = omLines.GetSize();

#ifndef SCANBACKWARD
    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
        if (CompareFlight(prpFlight, &omLines[ilLineno]) <= 0)
            break;  
#else
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareFlight(prpFlight, &omLines[ilLineno-1]) >= 0)
            break;  
#endif

	FLIGHTSCHEDULE_LINEDATA rlLine;
    rlLine = *prpFlight;
	omLines.NewAt(ilLineno, rlLine);

	return ilLineno;
}

/////////////////////////////////////////////////////////////////////////////
// FlightScheduleViewer - display drawing routine

void FlightScheduleViewer::UpdateDisplay()
{
	if (bmDisplayOutbound && bmDisplayInbound)
	{
		//pomTable->SetHeaderFields("Flight|Ori|Via|Sta|Eta|Ata|Stat|Pos|Gate|Nature|Band1|Regist|A/C|Flight|Des|Via|Std|Etd|Atd|Stat|Pos|Gate|Nature|Ckeckin counters");
		pomTable->SetHeaderFields(GetString(IDS_STRING61564));
		pomTable->SetFormatList("9|3|3|7|4|4|4|4|4|7|4|7|3|9|3|3|7|4|4|4|4|4|7|9|12");
	}
	else
	{
		if (bmDisplayInbound)
		{
			//pomTable->SetHeaderFields("Flight|Ori|Via|Sta|Eta|Ata|Stat|Pos|Gate|Nature|Band1|Regist|A/C");
			pomTable->SetHeaderFields(GetString(IDS_STRING61565));
			pomTable->SetFormatList("9|3|3|7|4|4|4|4|4|7|4|7|3|12");
		}
		else
		{
			if (bmDisplayOutbound)
			{
				//pomTable->SetHeaderFields("Flight No.|Des|Via|Std|Etd|Atd|Stat|Pos|Gate|Nature(Dep)|Ckin|Regist|A/C");
				pomTable->SetHeaderFields(GetString(IDS_STRING61566));
				pomTable->SetFormatList("9|3|3|7|4|4|4|4|4|7|9|7|3|12");
			}
		}
	}
    pomTable->ResetContent();
    for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
    {
        pomTable->AddTextLine(Format(&omLines[ilLc]), &omLines[ilLc]);

        // Display grouping effect
        if (ilLc == omLines.GetSize()-1 ||
			!IsSameGroup(&omLines[ilLc], &omLines[ilLc+1]))
            pomTable->SetTextLineSeparator(ilLc, ST_THICK);
    }
    pomTable->DisplayTable();
}

CString FlightScheduleViewer::Format(FLIGHTSCHEDULE_LINEDATA *prpLine)
{
	CString s;

	if (bmDisplayInbound)
	{

		CTime olActual = TIMENULL;
		CString olActualMark;
			
		if (prpLine->Onbl != TIMENULL)
		{
			olActual = prpLine->Onbl;
			olActualMark = "ONBL";
		}
		else
			if (prpLine->Land != TIMENULL)
			{
				olActual = prpLine->Land;
				olActualMark = "LAND";
			}
			else
				if (prpLine->Tmot != TIMENULL)
				{
					olActual = prpLine->Tmot;
					olActualMark = "TMO";
				}


		s = prpLine->Fnuma;
		s += CString("|") + prpLine->Rou1a;
		s += CString("|") + prpLine->Rou2a;
		s += CString("|") + ogBasicData.FormatDate(prpLine->Stoa,omStartTime);
		s += CString("|") + ogBasicData.FormatDate(prpLine->Etoa,omStartTime);
		s += CString("|") + olActual.Format("%H%M");
		s += CString("|") + olActualMark;
		s += CString("|") + prpLine->Posia;
		s += CString("|") + prpLine->Alida;
		s += CString("|") + prpLine->VaArr;
		s += CString("|") + prpLine->Blt1;
		s += CString("|") + prpLine->Regn;
		s += CString("|") + prpLine->Actl;

	}
	if (bmDisplayOutbound)
	{
		CTime olActual = TIMENULL;
		CString olActualMark;
			
		if (prpLine->Ofbl != TIMENULL)
		{
			olActual = prpLine->Ofbl;
			olActualMark = "OFBL";
		}
		else
			if (prpLine->Airb != TIMENULL)
			{
				olActual = prpLine->Airb;
				olActualMark = "TKOF";
			}

		if ( bmDisplayInbound)
		{
			s += CString("|");
		}
		s += prpLine->Fnumd;
		s += CString("|") + prpLine->Rou1d;
		s += CString("|") + prpLine->Rou2d;
		s += CString("|") + ogBasicData.FormatDate(prpLine->Stod,omStartTime);
		s += CString("|") + ogBasicData.FormatDate(prpLine->Etod,omStartTime);
		s += CString("|") + olActual.Format("%H%M");
		s += CString("|") + olActualMark;
		s += CString("|") + prpLine->Posid;
		s += CString("|") + prpLine->Alidd;
		s += CString("|") + prpLine->VaDep;
		s += CString("|") + prpLine->Cki;
		if (! bmDisplayInbound)
		{
			s += CString("|") + prpLine->Regn;
			s += CString("|") + prpLine->Actl;
		}
		s += CString("|") + prpLine->Pax;
	}
    return s;
}

void FlightScheduleViewer::SetIsFromSearchMode(BOOL bpIsFromSearch)
{
	bmIsFromSearch = bpIsFromSearch;
}


BOOL FlightScheduleViewer::FindInboundFlight(long lpFlightUrno,int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
		if (omLines[ilItem].ArrUrno == lpFlightUrno)
			return TRUE;
	}
	ilItem = -1;
	return FALSE;
}

BOOL FlightScheduleViewer::FindOutboundFlight(long lpFlightUrno,int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
		if (omLines[ilItem].DepUrno == lpFlightUrno)
			return TRUE;
	}
	ilItem = -1;
	return FALSE;
}

int FlightScheduleViewer::FindFlightLine(long lpFlightUrno)
{
	int ilCount = omLines.GetSize();
    for(int ilLine = 0; ilLine < ilCount; ilLine++)
    {
		if(omLines[ilLine].ArrUrno == lpFlightUrno || omLines[ilLine].DepUrno == lpFlightUrno)
		{
			return ilLine;
		}
	}
	return -1;
}

static void FlightScheduleTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{

    FlightScheduleViewer *polViewer = (FlightScheduleViewer *)popInstance;

	if (polViewer->bmNoUpdatesNow == FALSE)
    {
		if (ipDDXType == FLIGHT_CHANGE)
			polViewer->ProcessFlightChange((FLIGHTDATA *)vpDataPointer);
		else if (ipDDXType == FLIGHT_DELETE)
			polViewer->ProcessFlightDelete((long)vpDataPointer);
		else if (ipDDXType == FLIGHT_SELECT_FLIGHTLIST)
			polViewer->ProcessFlightSelect((FLIGHTDATA *)vpDataPointer);
	}
	
	if(ogBasicData.IsPaxServiceT2() == true) //Singapore
	{	
		if(polViewer->pomTable != NULL && polViewer->pomTable->m_hWnd != NULL)
		{	
			CWnd* polWnd = polViewer->pomTable->GetParent();
			(FlightPlan*)(polWnd)->SendMessage(WM_TABLE_UPDATE_DATACOUNT,0,0);
		}
	}
}

void FlightScheduleViewer::AllowUpdates(BOOL bpNoUpdatesNow)
{
	bmNoUpdatesNow = bpNoUpdatesNow;
}

void FlightScheduleViewer::ProcessFlightDelete(long lpFlightUrno)
{
	FLIGHTDATA *prlRotation = NULL;
	int ilLine = -1;

	if(FindInboundFlight(lpFlightUrno,ilLine))
	{
		prlRotation = ogFlightData.GetFlightByUrno(omLines[ilLine].DepUrno);
	}
	else if(FindOutboundFlight(lpFlightUrno,ilLine))
	{
		prlRotation = ogFlightData.GetFlightByUrno(omLines[ilLine].ArrUrno);
	}

	if(ilLine != -1)
	{
		if(prlRotation != NULL)
		{
			ProcessFlightChange(prlRotation);
		}
		else
		{
			DeleteLine(ilLine);
			pomTable->DeleteTextLine(ilLine);
			pomTable->DisplayTable();
		}
	}
}

void FlightScheduleViewer::ProcessFlightChange(FLIGHTDATA *prpFlight)
{
	if(prpFlight == NULL)
		return;

	FLIGHTDATA *prlRotation = ogFlightData.GetRotationFlight(prpFlight);

	FLIGHTSCHEDULE_LINEDATA *prlFlightLine = NULL;
	int ilLine = -1;
	CDWordArray olFlightUrnos;

	int ilCount = omLines.GetSize();
    for(ilLine = (ilCount - 1); ilLine >= 0; ilLine--)
    {
		prlFlightLine = &omLines[ilLine];
		bool blFound = false;
		if(prlFlightLine->ArrUrno == prpFlight->Urno || prlFlightLine->DepUrno == prpFlight->Urno)
		{
			blFound = true;
		}
		else if(prlRotation != NULL && (prlFlightLine->ArrUrno == prlRotation->Urno || prlFlightLine->DepUrno == prlRotation->Urno))
		{
			blFound = true;
		}

		if(blFound)
		{
			if(prlFlightLine->ArrUrno != 0L)
				olFlightUrnos.Add(prlFlightLine->ArrUrno);
			if(prlFlightLine->DepUrno != 0L)
				olFlightUrnos.Add(prlFlightLine->DepUrno);

			DeleteLine(ilLine);
			pomTable->DeleteTextLine(ilLine);
		}
	}

	FLIGHTDATA *prlFlight = NULL;
	int ilNumFlightUrnos = olFlightUrnos.GetSize();
	for(int ilF = 0; ilF < ilNumFlightUrnos; ilF++)
	{
		if((prlFlight = ogFlightData.GetFlightByUrno(olFlightUrnos[ilF])) != NULL)
		{
			MakeLine(prlFlight, true);
		}
	}
	pomTable->DisplayTable();
}


void FlightScheduleViewer::ProcessFlightSelect(FLIGHTDATA *prpFlight)
{
	if ((prpFlight->ReturnFlightType == ID_NOT_RETURNFLIGHT && ogFlightData.IsArrival(prpFlight)) || prpFlight->ReturnFlightType == ID_ARR_RETURNFLIGHT)
	{
		int ilItem;
		if (FindInboundFlight(prpFlight->Urno,ilItem))
		{
			CListBox *polListBox = pomTable->GetCTableListBox();
			if (polListBox)
				polListBox->SetCurSel(ilItem);
		}
	}
	else
	{
		int ilItem;
		if (FindOutboundFlight(prpFlight->Urno,ilItem))
		{
			CListBox *polListBox = pomTable->GetCTableListBox();
			if (polListBox)
				polListBox->SetCurSel(ilItem);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
// Printing functions

BOOL FlightScheduleViewer::PrintFlightScheduleLine(FLIGHTSCHEDULE_LINEDATA *prpLine,BOOL bpIsLastLine)
{
	if (bmDisplayOutbound && bmDisplayInbound)
	{
		PrintFlightScheduleLineTurnaround(prpLine,bpIsLastLine);
	}
	else
	{
		if (bmDisplayInbound)
		{
			PrintFlightScheduleLineInbound(prpLine,bpIsLastLine);
		}
		else
		{
			if (bmDisplayOutbound)
			{
				PrintFlightScheduleLineOutbound(prpLine,bpIsLastLine);
			}
		}
	}
	return TRUE;
}


BOOL FlightScheduleViewer::PrintFlightScheduleLineInbound(FLIGHTSCHEDULE_LINEDATA *prpLine,BOOL bpIsLastLine)
{
	//	if (bmDisplayInbound)
	//	pomTable->SetHeaderFields("Flight|Ori|Via|Sta|Eta|Ata|Stat|Pos|Gate|Regist|A/C");


	int ilBottomFrame;
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	ilBottomFrame = PRINT_FRAMETHIN;

	if (bpIsLastLine == TRUE)
	{
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.Length     = 1130;
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= PRINT_NOFRAME;
		rlElement.pFont       = &pomPrint->omMediumFont_Bold;
		rlElement.Text.Empty();
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	else
	{
		if (pomPrint->imLineNo == 0)
		{

			rlElement.Alignment  = PRINT_CENTER;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
			rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;

			rlElement.FrameLeft  = PRINT_FRAMETHIN;
			
			rlElement.Length     = 150;
			rlElement.Text       = "Flight";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 100;
			rlElement.Text       = "Ori";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 100;
			rlElement.Text       = "Via";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 130;
			rlElement.Text       = "Sta";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 130;
			rlElement.Text       = "Eta";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 130;
			rlElement.Text       = "Ata";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 100;
			rlElement.Text       = "Stat";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 80;
			rlElement.Text       = "Pos";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 80;
			rlElement.Text       = "Gate";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 120;
			rlElement.Text       = CString("Regist.");
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 100;
			rlElement.Text       = "A/C";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			
			rlElement.FrameRight = PRINT_FRAMEMEDIUM;
			rlElement.Length     = 500;
			rlElement.Text       = bmDisplayPax ? "PAX F/C/Y/P" : "";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			
			pomPrint->PrintLine(rlPrintLine);
			rlPrintLine.DeleteAll();

		}

		CTime olActual = TIMENULL;
		CString olActualMark;
			
		// prepare arrival time fields 

		if (prpLine->Onbl != TIMENULL)
		{
			olActual = prpLine->Onbl;
			olActualMark = "ONBL";
		}
		else
			if (prpLine->Land != TIMENULL)
			{
				olActual = prpLine->Land;
				olActualMark = "LAND";
			}
			else
				if (prpLine->Tmot != TIMENULL)
				{
					olActual = prpLine->Tmot;
					olActualMark = "TMO";
				}

		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= ilBottomFrame;
		rlElement.pFont       = &pomPrint->omSmallFont_Bold;

		rlElement.FrameLeft  = PRINT_FRAMETHIN;
		rlElement.FrameTop   = PRINT_NOFRAME;
			
		rlElement.Length     = 150;
		rlElement.Text       = prpLine->Fnuma;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 100;
		rlElement.Text       = prpLine->Rou1a;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 100;
		rlElement.Text       = prpLine->Rou2a;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 130;
		rlElement.Text       = ogBasicData.FormatDate2(prpLine->Stoa,omStartTime);
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 130;
		rlElement.Text       = ogBasicData.FormatDate2(prpLine->Etoa,omStartTime);
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 130;
		rlElement.Text       = ogBasicData.FormatDate2(olActual,omStartTime);
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 100;
		rlElement.Text       = olActualMark;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 80;
		rlElement.Text       = prpLine->Posia;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 80;
		rlElement.Text       = prpLine->Alida;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 120;
		rlElement.Text       = prpLine->Regn;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 100;
		rlElement.Text       = prpLine->Actl;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 500;
		rlElement.FrameRight = PRINT_FRAMEMEDIUM;
		rlElement.Text       = "";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return TRUE;
}

BOOL FlightScheduleViewer::PrintFlightScheduleLineOutbound(FLIGHTSCHEDULE_LINEDATA *prpLine,BOOL bpIsLastLine)
{
	// if (bmDisplayOutbound)
	//		pomTable->SetHeaderFields("Flight No.|Des|Via|Std|Etd|Atd|Stat|Pos|Gate|Regist|A/C");


	int ilBottomFrame;
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	ilBottomFrame = PRINT_FRAMETHIN;

	if (bpIsLastLine == TRUE)
	{
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.Length     = 1150;
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= PRINT_NOFRAME;
		rlElement.pFont       = &pomPrint->omMediumFont_Bold;
		rlElement.Text.Empty();
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	else
	{
		if (pomPrint->imLineNo == 0)
		{
			rlElement.Alignment  = PRINT_CENTER;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
			rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;

			rlElement.FrameLeft  = PRINT_FRAMETHIN;
			
			rlElement.Length     = 150;
			rlElement.Text       = "Flight";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 100;
			rlElement.Text       = "Des";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 100;
			rlElement.Text       = "Via";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 130;
			rlElement.Text       = "Std";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 130;
			rlElement.Text       = "Etd";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 130;
			rlElement.Text       = "Atd";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 100;
			rlElement.Text       = "Stat";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 90;
			rlElement.Text       = "Pos";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 90;
			rlElement.Text       = "Gate";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 120;
			rlElement.Text       = CString("Regist.");
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 100;
			rlElement.Text       = "A/C";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.FrameRight = PRINT_FRAMEMEDIUM;
			rlElement.Length     = 500;
			rlElement.Text       = bmDisplayPax ? "PAX F/C/Y/P" : "";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			
			pomPrint->PrintLine(rlPrintLine);
			rlPrintLine.DeleteAll();

		}
		CTime olActual = TIMENULL;
		CString olActualMark;


			// prepare outbound times
		olActualMark.Empty();
		if (prpLine->Ofbl != TIMENULL)
		{
			olActual = prpLine->Ofbl;
			olActualMark = "OFBL";
		}
		else
			if (prpLine->Airb != TIMENULL)
			{
				olActual = prpLine->Airb;
				olActualMark = "TKOF";
			}


		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= ilBottomFrame;
		rlElement.pFont       = &pomPrint->omSmallFont_Bold;

		
		rlElement.FrameLeft  = PRINT_FRAMETHIN;
		rlElement.FrameTop   = PRINT_NOFRAME;

		rlElement.Length     = 150;
		rlElement.Text       = prpLine->Fnumd;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 100;
		rlElement.Text       = prpLine->Rou1d;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 100;
		rlElement.Text       = prpLine->Rou2d;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 130;
		rlElement.Text       = ogBasicData.FormatDate2(prpLine->Stod,omStartTime);
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 130;
		rlElement.Text       = ogBasicData.FormatDate2(prpLine->Etod,omStartTime);
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 130;
		rlElement.Text       = ogBasicData.FormatDate2(olActual,omStartTime);
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 100;
		rlElement.Text       = olActualMark;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 90;
		rlElement.Text       = prpLine->Posid;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 90;
		rlElement.Text       = prpLine->Alidd;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 120;
		rlElement.Text       = prpLine->Regn;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 100;
		rlElement.Text       = prpLine->Actl;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 500;
		rlElement.FrameRight = PRINT_FRAMEMEDIUM;
		rlElement.Text       = (bmDisplayPax) ? prpLine->Pax : "";
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return TRUE;
}

BOOL FlightScheduleViewer::PrintFlightScheduleLineTurnaround(FLIGHTSCHEDULE_LINEDATA *prpLine,BOOL bpIsLastLine)
{
	//	if (bmDisplayOutbound && bmDisplayInbound)
    // pomTable->SetHeaderFields("Flight|Ori|Via|Sta|Eta|Ata|Stat|Pos|Gate|Regist|A/C|
	// Flight|Des|Via|Std|Etd|Atd|Stat|Pos|Gate");

	int ilBottomFrame;
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	ilBottomFrame = PRINT_FRAMETHIN;

	if (bpIsLastLine == TRUE)
	{
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.Length     = 1910;
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= PRINT_NOFRAME;
		rlElement.pFont       = &pomPrint->omMediumFont_Bold;
		rlElement.Text.Empty();
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	else
	{
		if (pomPrint->imLineNo == 0)
		{
			rlElement.Alignment  = PRINT_CENTER;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
			rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;

			rlElement.Length     = 150;
			rlElement.Text       = "Flight";
			rlElement.FrameLeft  = PRINT_FRAMETHIN;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 100;
			rlElement.Text       = "Ori";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 100;
			rlElement.Text       = "Via";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 130;
			rlElement.Text       = "Sta";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 130;
			rlElement.Text       = "Eta";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 130;
			rlElement.Text       = "Ata";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 100;
			rlElement.Text       = "Stat";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 90;
			rlElement.Text       = "Pos";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 90;
			rlElement.Text       = "Gate";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 120;
			rlElement.Text       = CString("Regist.");
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 100;
			rlElement.Text       = "A/C";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.FrameLeft  = PRINT_FRAMETHICK;
			rlElement.Length     = 150;
			rlElement.Text       = "Flight";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.FrameLeft  = PRINT_FRAMETHIN;
			rlElement.Length     = 100;
			rlElement.Text       = "Des";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 100;
			rlElement.Text       = "Via";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 130;
			rlElement.Text       = "Std";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 130;
			rlElement.Text       = "Etd";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 130;
			rlElement.Text       = "Atd";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 100;
			rlElement.Text       = "Stat";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 90;
			rlElement.Text       = "Pos";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 90;
			rlElement.Text       = "Gate";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 500;
			rlElement.FrameRight = PRINT_FRAMEMEDIUM;
			rlElement.Text       = "";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			
			pomPrint->PrintLine(rlPrintLine);
			rlPrintLine.DeleteAll();
		}

		CTime olActual = TIMENULL;
		CString olActualMark;
			
		if (bmDisplayInbound)
		{
			// prepare arrival time fields 

			if (prpLine->Onbl != TIMENULL)
			{
				olActual = prpLine->Onbl;
				olActualMark = "ONBL";
			}
			else
				if (prpLine->Land != TIMENULL)
				{
					olActual = prpLine->Land;
					olActualMark = "LAND";
				}
				else
					if (prpLine->Tmot != TIMENULL)
					{
						olActual = prpLine->Tmot;
						olActualMark = "TMO";
					}
		}

		rlElement.Alignment  = PRINT_LEFT;
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= ilBottomFrame;
		rlElement.pFont       = &pomPrint->omSmallFont_Bold;

		
		rlElement.FrameLeft  = PRINT_FRAMETHIN;
		rlElement.FrameTop   = PRINT_NOFRAME;

		rlElement.Length     = 150;
		rlElement.Text       = prpLine->Fnuma;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 100;
		rlElement.Text       = prpLine->Rou1a;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 100;
		rlElement.Text       = prpLine->Rou2a;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 130;
		rlElement.Text       = ogBasicData.FormatDate2(prpLine->Stoa,omStartTime);
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 130;
		rlElement.Text       = ogBasicData.FormatDate2(prpLine->Etoa,omStartTime);
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 130;
		rlElement.Text       = ogBasicData.FormatDate2(olActual,omStartTime);
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 100;
		rlElement.Text       = olActualMark;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 90;
		rlElement.Text       = prpLine->Posia;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 90;
		rlElement.Text       = prpLine->Alida;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 120;
		rlElement.Text       = prpLine->Regn;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 100;
		rlElement.Text       = prpLine->Actl;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

// pomTable->SetHeaderFields("Flight|Ori|Via|Sta|Eta|Ata|Stat|Pos|Gate|Regist|A/C|
// Flight|Des|Via|Std|Etd|Atd|Stat|Pos|Gate");

			// prepare outbound times
				
		olActual = TIMENULL;
		olActualMark.Empty();

		if (prpLine->Ofbl != TIMENULL)
		{
			olActual = prpLine->Ofbl;
			olActualMark = "OFBL";
		}
		else
			if (prpLine->Airb != TIMENULL)
			{
				olActual = prpLine->Airb;
				olActualMark = "TKOF";
			}

		
		rlElement.FrameLeft  = PRINT_FRAMETHICK;
		rlElement.Length     = 150;
		rlElement.Text       = prpLine->Fnumd;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.FrameLeft  = PRINT_FRAMETHIN;

		rlElement.Length     = 100;
		rlElement.Text       = prpLine->Rou1d;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 100;
		rlElement.Text       = prpLine->Rou2d;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 130;
		rlElement.Text       = ogBasicData.FormatDate2(prpLine->Stod,omStartTime);
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 130;
		rlElement.Text       = ogBasicData.FormatDate2(prpLine->Etod,omStartTime);
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 130;
		rlElement.Text       = ogBasicData.FormatDate2(olActual,omStartTime);
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 100;
		rlElement.Text       = olActualMark;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 90;
		rlElement.Text       = prpLine->Posid;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 90;
		rlElement.Text       = prpLine->Alidd;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		rlElement.Length     = 500;
		rlElement.Text       = (bmDisplayPax) ? prpLine->Pax : "";
		rlElement.FrameRight = PRINT_FRAMEMEDIUM;
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}

	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return TRUE;
}


BOOL FlightScheduleViewer::PrintFlightScheduleHeader(CCSPrint *pomPrint)
{
	pomPrint->omCdc.StartPage();
	pomPrint->PrintHeader();
	return TRUE;
}
BOOL FlightScheduleViewer::PrintPreviewFlightScheduleHeader(CCSPrint *pomPrint)
{
	//pomPrint->omCdc.StartPage();
	pomPrint->pomCdc->StartPage();
	pomPrint->PrintHeader();
	return TRUE;
}
void FlightScheduleViewer::PrintView()
{  
	CString omTarget;
	CTime olPrintStart = (ogBasicData.GetTimeframeStart()  > omStartTime) ? ogBasicData.GetTimeframeStart() : omStartTime;
	CTime olPrintEnd = (ogBasicData.GetTimeframeEnd()  < omEndTime) ? ogBasicData.GetTimeframeEnd() : omEndTime;
	omTarget.Format(GetString(IDS_STRING61285),"",olPrintStart.Format("%d.%m.%Y %H:%M"),olPrintEnd.Format("%d.%m.%Y %H:%M"));
	omTarget += GetString(IDS_STRING61292) + ogCfgData.rmUserSetup.FPLV;		
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");
	pomPrint = new CCSPrint(pomTable,PRINT_LANDSCAPE,60,450,50,GetString(IDS_STRING61364),olPrintDate,omTarget);
	pomPrint->imMaxLines = 21;

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(PRINT_LANDSCAPE,ogBasicData.pcmPrintForm,DMPAPER_A3)  == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			pomPrint->imLineHeight = 62;
			rlDocInfo.cbSize = sizeof( DOCINFO );

			char pclDocName[100];
			strcpy(pclDocName,GetString(IDS_STRING61364));
			rlDocInfo.lpszDocName = pclDocName;	

			pomPrint->omCdc.StartDoc( &rlDocInfo );
			for(int i = 0; i < omLines.GetSize(); i++ ) 
			{
				if( pomPrint->imLineNo >= pomPrint->imMaxLines )
				{
					if ( pomPrint->imPageNo > 0)
					{
						PrintFlightScheduleLine(&omLines[i],TRUE);
						pomPrint->PrintFooter("","");
						pomPrint->imLineNo = pomPrint->imMaxLines + 1;
						pomPrint->omCdc.EndPage();
						pomPrint->imLineTop = pomPrint->imFirstLine;
					}
					PrintFlightScheduleHeader(pomPrint);
				}
				PrintFlightScheduleLine(&omLines[i],FALSE);
			}
			PrintFlightScheduleLine(NULL,TRUE);
			pomPrint->PrintFooter("","");
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	pomPrint = NULL;
	}

	/*
	CPrintCtrl *prn = new CPrintCtrl(FALSE);
	prn->FlightSchedulePrint(&ogShiftData, (char *)(const char *)omDate);
	delete prn;
	*/
}

void FlightScheduleViewer::PrintPreview(const int& ripPageNo)
{  
	if(ripPageNo <= 0)
		return;

	static ilLineNo = 0;
	if(ripPageNo == 1)
	{
		ilLineNo = 0;
	}
	

	/*
	CString omTarget;
	CTime olPrintStart = (ogBasicData.GetTimeframeStart()  > omStartTime) ? ogBasicData.GetTimeframeStart() : omStartTime;
	CTime olPrintEnd = (ogBasicData.GetTimeframeEnd()  < omEndTime) ? ogBasicData.GetTimeframeEnd() : omEndTime;
	omTarget.Format(GetString(IDS_STRING61285),"",olPrintStart.Format("%d.%m.%Y %H:%M"),olPrintEnd.Format("%d.%m.%Y %H:%M"));
	omTarget += GetString(IDS_STRING61292) + ogCfgData.rmUserSetup.FPLV;		
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");

	//set header title
	DOCINFO	rlDocInfo;
	memset(&rlDocInfo, 0, sizeof(DOCINFO));
	rlDocInfo.cbSize = sizeof( DOCINFO );

	char pclDocName[100];
	strcpy(pclDocName,GetString(IDS_STRING61364));
	rlDocInfo.lpszDocName = pclDocName;	

	pomPrint->pomCdc->StartDoc( &rlDocInfo );
	*/

	if (pomPrint != NULL)
	{
		//reduce one, as need add one line of table name
		pomPrint->imMaxLines = pomPrint->imMaxLines-1;	
		ilLineNo = (ripPageNo-1) * pomPrint->imMaxLines;
		//pomPrint->imLineHeight = 62;
		//pomPrint->imLineNo = (ripPageNo-1) * pomPrint->imMaxLines;
		//int imMaxJobsPerLine = 3;
	
		pomPrint->imPageNo = ripPageNo;
		PrintPreviewFlightScheduleHeader(pomPrint);
		for(int i = (ripPageNo-1) * pomPrint->imMaxLines; 
			(pomPrint->imLineNo < (pomPrint->imMaxLines + 1)) && i < omLines.GetSize(); 
			i++ ) 
		{
			pomPrint->imPageNo = ripPageNo;
			PrintFlightScheduleLine(&omLines[i],FALSE);
			ilLineNo++;
		}

		PrintFlightScheduleLine(NULL,TRUE);		
		pomPrint->PrintFooter("","");		
		pomPrint->pomCdc->EndPage();
		pomPrint->imLineTop = pomPrint->imFirstLine;
		pomPrint->imLineNo = 0;	
	}
	
}
