// Class for DRGTAB - Staff Working Group Deviations
// group deviation - the emp is given a new function temporarily so that they can belong to a group

#include <afxwin.h>
#include <ccsglobl.h>
#include <OpssPm.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaDrgData.h>
#include <BasicData.h>

extern CCSDdx ogCCSDdx;
void  ProcessDrgCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaDrgData::CedaDrgData()
{                  
    BEGIN_CEDARECINFO(DRGDATA, DRGDATARecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Wgpc,"WGPC")
		FIELD_LONG(Stfu,"STFU")
		FIELD_CHAR_TRIM(Sday,"SDAY")
		FIELD_CHAR_TRIM(Fctc,"FCTC")
		FIELD_CHAR_TRIM(Drrn,"DRRN")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(DRGDATARecInfo)/sizeof(DRGDATARecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&DRGDATARecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"DRGTAB");
    pcmFieldList = "URNO,WGPC,STFU,SDAY,FCTC,DRRN";

	ogCCSDdx.Register((void *)this,BC_DRG_CHANGE,CString("DRGDATA"), CString("Drg changed"),ProcessDrgCf);
	ogCCSDdx.Register((void *)this,BC_DRG_DELETE,CString("DRGDATA"), CString("Drg deleted"),ProcessDrgCf);
}
 
CedaDrgData::~CedaDrgData()
{
	TRACE("CedaDrgData::~CedaDrgData called\n");
	ClearAll();
	ogCCSDdx.UnRegister(this,NOTUSED);
}

void CedaDrgData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
	CMapPtrToPtr *polSingleMap;
	long llUrno;
	for(POSITION rlPos = omStfuMap.GetStartPosition(); rlPos != NULL; )
	{
		omStfuMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& )polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	omStfuMap.RemoveAll();
}

void  ProcessDrgCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaDrgData *)popInstance)->ProcessDrgBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void  CedaDrgData::ProcessDrgBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlDrgData = (struct BcStruct *) vpDataPointer;
	long llUrno = GetUrnoFromSelection(prlDrgData->Selection);
	DRGDATA *prlDrg = GetDrgByUrno(llUrno);
	ogBasicData.LogBroadcast(prlDrgData);

	if(ipDDXType == BC_DRG_CHANGE && prlDrg != NULL)
	{
		// update
		//ogBasicData.Trace("BC_DRG_CHANGE - UPDATE DRGTAB URNO %ld\n",prlDrg->Urno);
		GetRecordFromItemList(prlDrg,prlDrgData->Fields,prlDrgData->Data);
		ogCCSDdx.DataChanged((void *)this,DRG_CHANGE,(void *)prlDrg);
	}
	else if(ipDDXType == BC_DRG_CHANGE && prlDrg == NULL)
	{
		// insert
		DRGDATA rlDrg;
		GetRecordFromItemList(&rlDrg,prlDrgData->Fields,prlDrgData->Data);
		//ogBasicData.Trace("BC_DRG_CHANGE - INSERT DRGTAB URNO %ld\n",rlDrg.Urno);
		prlDrg = AddDrgInternal(rlDrg);
		ogCCSDdx.DataChanged((void *)this,DRG_CHANGE,(void *)prlDrg);
	}
	else if(ipDDXType == BC_DRG_DELETE && prlDrg != NULL)
	{
		// delete
		DRGDATA rolDrg = *prlDrg;
		//ogBasicData.Trace("BC_DRG_DELETE - DELETE DRGTAB URNO %ld\n",llDrgUrno);
		DeleteDrgInternal(prlDrg->Urno);
		ogCCSDdx.DataChanged((void *)this,DRG_DELETE,(void *)&rolDrg);
	}
}

CCSReturnCode CedaDrgData::ReadDrgData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];
    sprintf(pclWhere,"WHERE SDAY BETWEEN '%s' AND '%s'", ogBasicData.omShiftFirstSDAY.Format("%Y%m%d"), 
														 ogBasicData.omShiftLastSDAY.Format("%Y%m%d"));
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaDrgData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		DRGDATA rlDrgData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlDrgData)) == RCSuccess)
			{
				AddDrgInternal(rlDrgData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaDrgData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(DRGDATA), pclWhere);
    return ilRc;
}


DRGDATA *CedaDrgData::AddDrgInternal(DRGDATA &rrpDrg)
{
	DRGDATA *prlDrg = new DRGDATA;
	*prlDrg = rrpDrg;
	PrepareData(prlDrg);
	omData.Add(prlDrg);
	omUrnoMap.SetAt((void *)prlDrg->Urno,prlDrg);
	CMapPtrToPtr *polSingleMap;
	if(omStfuMap.Lookup((void *) prlDrg->Stfu, (void *&) polSingleMap))
	{
		polSingleMap->SetAt((void *)prlDrg->Urno,prlDrg);
	}
	else
	{
		polSingleMap = new CMapPtrToPtr;
		polSingleMap->SetAt((void *)prlDrg->Urno,prlDrg);
		omStfuMap.SetAt((void *)prlDrg->Stfu,polSingleMap);
	}
	return prlDrg;
}

void CedaDrgData::DeleteDrgInternal(long lpUrno)
{
	int ilNumDrgs = omData.GetSize();
	for(int ilDel = (ilNumDrgs-1); ilDel >= 0; ilDel--)
	{
		DRGDATA *prlDrg = &omData[ilDel];
		if(prlDrg->Urno == lpUrno)
		{
			omData.DeleteAt(ilDel);
		}
	}
	omUrnoMap.RemoveKey((void *)lpUrno);
}

void CedaDrgData::PrepareData(DRGDATA *prpDrg)
{
}

DRGDATA* CedaDrgData::GetDrgByUrno(long lpUrno)
{
	DRGDATA *prlDrg = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlDrg);
	return prlDrg;
}

CString CedaDrgData::GetTableName(void)
{
	return CString(pcmTableName);
}

DRGDATA *CedaDrgData::GetDrgByShift(SHIFTDATA *prpShift)
{
	DRGDATA *prlDrg = NULL;

	if(prpShift != NULL)
	{
		CMapPtrToPtr *polSingleMap;
		long llUrno;
		DRGDATA *prlTmpDrg;

		if(omStfuMap.Lookup((void *)prpShift->Stfu, (void *& )polSingleMap))
		{
			for(POSITION rlPos = polSingleMap->GetStartPosition(); prlDrg == NULL && rlPos != NULL; )
			{
				polSingleMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlTmpDrg);
				if(!strcmp(prpShift->Sday,prlTmpDrg->Sday) && !strcmp(prpShift->Drrn,prlTmpDrg->Drrn))
				{
					prlDrg = prlTmpDrg;
				}
			}
		}
	}
	return prlDrg;
}

bool CedaDrgData::UpdateDrgCodeForShift(SHIFTDATA *prpShift)
{
	bool blRc = false;
	if(prpShift != NULL)
	{
		DRGDATA *prlDrg = GetDrgByShift(prpShift);
		if(prlDrg != NULL)
		{
			if(UpdateDrg(prlDrg->Urno, prpShift->Egrp, prpShift->Fctc) == RCSuccess)
				blRc = true;
		}
		else
		{
			if(InsertDrg(prpShift->Stfu, prpShift->Sday, prpShift->Egrp, prpShift->Fctc, prpShift->Drrn) != NULL)
				blRc = true;
		}
	}
	return blRc;
}


CCSReturnCode CedaDrgData::UpdateDrg(long lpDrgUrno, const char *pcpWgpc, const char *pcpFctc)
{
	CCSReturnCode olRc = RCSuccess;

	DRGDATA *prlDrg = GetDrgByUrno(lpDrgUrno);
	if(prlDrg != NULL)
	{
		//if((strcmp(prlDrg->Wgpc,pcpWgpc) && strlen(pcpWgpc) > 0) || (strcmp(prlDrg->Fctc,pcpFctc) && strlen(pcpFctc) > 0))
		if(strcmp(prlDrg->Wgpc,pcpWgpc) || strcmp(prlDrg->Fctc,pcpFctc))
		{
			strcpy(prlDrg->Wgpc,pcpWgpc);
			strcpy(prlDrg->Fctc,pcpFctc);

			CString olListOfData;
			char pclSelection[100], pclData[100], pclCmd[10] = "URT";

			sprintf(pclSelection,"WHERE URNO = '%ld'",prlDrg->Urno);
			MakeCedaData(&omRecInfo, olListOfData, prlDrg);
			strcpy(pclData,olListOfData);
			ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, pcmFieldList, pclData);
			if((olRc = CedaAction(pclCmd,pclSelection,"",pclData)) == RCSuccess)
			{
				ogCCSDdx.DataChanged((void *)this,DRG_CHANGE,(void *)prlDrg);
			}

//			char pclFieldList[100], pclSelection[100], pclData[100];
//			strcpy(pclFieldList,"WGPC,FCTC");
//			sprintf(pclData, "%s,%s", pcpWgpc, pcpFctc);
//			sprintf(pclSelection," WHERE URNO = '%ld%'",prlDrg->Urno);
//
//			if((olRc = CedaAction("URT", pcmTableName, pclFieldList, pclSelection,"", pclData)) == RCSuccess)
//			{
//				strcpy(prlDrg->Wgpc,pcpWgpc);
//				strcpy(prlDrg->Fctc,pcpFctc);
//				ogCCSDdx.DataChanged((void *)this,DRG_CHANGE,(void *)prlDrg);
//			}
		}
	}
	return olRc;
}

DRGDATA *CedaDrgData::InsertDrg(long lpStfu, const char *pcpSday, const char *pcpWgpc, const char *pcpFctc, const char *pcpDrrn)
{
	DRGDATA rlDrg, *prlDrg = NULL;
	rlDrg.Urno = ogBasicData.GetNextUrno();
	rlDrg.Stfu = lpStfu;
	strcpy(rlDrg.Wgpc, pcpWgpc);
	strcpy(rlDrg.Sday, pcpSday);
	strcpy(rlDrg.Fctc, pcpFctc);
	strcpy(rlDrg.Drrn, pcpDrrn);

	char pclData[5000];
	char pclCommand[10] = "IRT";
	char pclSelection[100] = "";
	CString olListOfData;
	MakeCedaData(&omRecInfo,olListOfData,&rlDrg);
	strcpy(pclData,olListOfData);
	if(CedaAction(pclCommand, pcmTableName, pcmFieldList, pclSelection, "", pclData) == RCSuccess)
	{
		if((prlDrg = AddDrgInternal(rlDrg)) != NULL)
		{
			ogCCSDdx.DataChanged((void *)this, DRG_INSERT, (void *)prlDrg);
		}
	}

	return prlDrg;
}

CString CedaDrgData::Dump(long lpUrno)
{
	CString olDumpStr;
	DRGDATA *prlDrg = GetDrgByUrno(lpUrno);
	if(prlDrg == NULL)
	{
		olDumpStr.Format("No DRGDATA Found for DRG.URNO <%ld>",lpUrno);
	}
	else
	{
		DRGDATA rlDrg;
		memcpy(&rlDrg,prlDrg,sizeof(DRGDATA));
		CString olData;
		MakeCedaData(&omRecInfo,olData,&rlDrg);
		CStringArray olFieldArray, olDataArray;
		ogBasicData.ExtractItemList(pcmFieldList, &olFieldArray);
		ogBasicData.ExtractItemList(olData, &olDataArray);
		int ilNumFields = olFieldArray.GetSize();
		int ilNumData = olDataArray.GetSize();
		if(ilNumFields != ilNumData)
		{
			olDumpStr.Format("<%s>\n<%s>\n",pcmFieldList,olData);
		}
		else
		{
			for(int ilD = 0; ilD < ilNumData; ilD++)
			{
				olDumpStr += olFieldArray[ilD] + " = <" + olDataArray[ilD] + "> ";
			}
		}
	}
	return olDumpStr;
}
