// EquipmentDiagramGroupPage.h : header file
//

#ifndef __EQUIPMENTDIAGRAMGROUPPAGE_H__
#define __EQUIPMENTDIAGRAMGROUPPAGE_H__

/////////////////////////////////////////////////////////////////////////////
// EquipmentDiagramGroupPage dialog

class EquipmentDiagramGroupPage : public CPropertyPage
{
	DECLARE_DYNCREATE(EquipmentDiagramGroupPage)

// Construction
public:
	EquipmentDiagramGroupPage();
	~EquipmentDiagramGroupPage();

// Dialog Data
	CString omGroupBy;

	//{{AFX_DATA(EquipmentDiagramGroupPage)
	enum { IDD = IDD_EQUIPMENTDIAGRAM_GROUP_PAGE };
	int		m_GroupBy;
	BOOL	m_NotDisplayUnavailable;
	//}}AFX_DATA
	CString omTitle;

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(EquipmentDiagramGroupPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(EquipmentDiagramGroupPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Helper routines
private:
	int GetGroupId(const char *pcpGroupKey);
	CString GetGroupKey(int ipGroupId);
};

#endif // __EQUIPMENTDIAGRAMGROUPPAGE_H__
