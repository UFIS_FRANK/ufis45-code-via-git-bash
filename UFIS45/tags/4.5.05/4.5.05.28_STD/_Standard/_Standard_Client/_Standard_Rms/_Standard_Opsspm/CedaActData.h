// Class for Actes
#ifndef _CEDAACTDATA_H_
#define _CEDAACTDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct ActDataStruct
{
	long	Urno;		// Unique Record Number
	char	Act3[4];	// 3 letter aircraft code
	char    Act5[6];    // 5 letter aircraft code

	ActDataStruct(void)
	{
		Urno = 0L;
		strcpy(Act3,"");
		strcpy(Act5,"");
	}
};

typedef struct ActDataStruct ACTDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaActData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <ACTDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CString GetTableName(void);

// Operations
public:
	CedaActData();
	~CedaActData();

	CCSReturnCode ReadActData();
	ACTDATA* GetActByUrno(long lpUrno);
	void GetAct3Names(CStringArray &ropActNames);
	void GetAct5Names(CStringArray &ropActNames);
	void GetFormattedActNames(CStringArray &ropActNames);
	void GetAct3AndAct5FromFormattedName(const char *pcpFormattedName, CString &ropAct3, CString &ropAct5);

private:
	void AddActInternal(ACTDATA &rrpAct);
	void ClearAll();
};


extern CedaActData ogActData;
#endif _CEDAACTDATA_H_
