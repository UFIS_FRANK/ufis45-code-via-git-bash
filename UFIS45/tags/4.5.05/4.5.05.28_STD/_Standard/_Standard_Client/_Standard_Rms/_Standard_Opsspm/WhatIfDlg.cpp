// WhatIfD.cpp : implementation file
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <OpssPm.h>
#include <CCSPtrArray.h>
#include <CLIENTWN.H>
#include <CCSCedaData.h>
#include <BasicData.h>
#include <CedaOptData.h>
#include <WhatIfDlg.h>
#include <CCSTime.h>
#include <CedaEmpData.h>
#include <CedaShiftData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaFlightData.h>
#include <tscale.h>
#include <cviewer.h>
#include <CCSDragDropCtrl.h>
#include <gbar.h>
#include <cxbutton.h>
#include <PrePlanTable.h>
#include <CCITable.h>
#include <GateTable.h>
#include <FlightPlan.h>
#include <StaffTable.h>
#include <StaffDiagram.h>
#include <GateDiagram.h>
#include <CciDiagram.h>
#include <StartDate.h>
#include <ConflictConfigTable.h>
#include <ConflictTable.h>
#include <ufis.h>
#include <CCSBcHandle.h>
#include <conflict.h>
#include <Optimization.h>
#include <NewParaSet.h>
#include <ButtonList.h>

/*
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
*/

/////////////////////////////////////////////////////////////////////////////
// CWhatIf dialog


CWhatIf::CWhatIf(CWnd* pParent, int ipKind, OPTDATA *prpOpt, char *pcpParaSet)
	: CDialog(CWhatIf::IDD, pParent)
{
	isSetFromProgram = FALSE;
	omAdditionalCmd	 = CString("");
	imMode			 = INSERT_MODE;
	imActualKind	 = ipKind;
	pcmParaSet		 = pcpParaSet;
	prmOpt			 = prpOpt;

	//{{AFX_DATA_INIT(CWhatIf)
	m_Solp = prpOpt->Solp;
	m_Stec = prpOpt->Stec;
	m_Sdtl = prpOpt->Sdtl;
	m_Squa = prpOpt->Squa;
	m_Sbre = prpOpt->Sbre;
	m_Swol = prpOpt->Swol;
	m_Smrt = prpOpt->Smrt;
	m_Sdis = prpOpt->Sdis;
	m_Shis = prpOpt->Shis;
	m_Sjmp = prpOpt->Sjmp;
	m_Svmf = prpOpt->Svmf;
	m_Spra = prpOpt->Spra;
	m_Snea = prpOpt->Snea;
	m_Saop = prpOpt->Saop;
	m_Srmx = prpOpt->Srmx;
	//}}AFX_DATA_INIT


	//if (strcmp(pcmParaSet, "Neuer Parameterset") != 0)
	if (strcmp(pcmParaSet, GetString(IDS_STRING61295)) != 0)
	{
		int	 ilRun = 0;
		BOOL blWhatIfFound = FALSE;
		int  ilCount = ogOptData.omData.GetSize();

		if (ilCount > 0)
		{
			do
			{
				OPTDATA *prlLocalOpt = &ogOptData.omData[ilRun];
				if ( (strcmp(prlLocalOpt->Ckey, pcmParaSet) == 0) &&
					 (strncmp(prlLocalOpt->Ctyp, "PARASET",7) == 0)      )
				{
					blWhatIfFound = TRUE;
					m_Solp = prlLocalOpt->Solp;
					m_Stec = prlLocalOpt->Stec;
					m_Sdtl = prlLocalOpt->Sdtl;
					m_Squa = prlLocalOpt->Squa;
					m_Sbre = prlLocalOpt->Sbre;
					m_Swol = prlLocalOpt->Swol;
					m_Smrt = prlLocalOpt->Smrt;
					m_Sdis = prlLocalOpt->Sdis;
					m_Shis = prlLocalOpt->Shis;
					m_Sjmp = prlLocalOpt->Sjmp;
					m_Svmf = prlLocalOpt->Svmf;
					m_Spra = prlLocalOpt->Spra;
					m_Snea = prlLocalOpt->Snea;
					m_Saop = prlLocalOpt->Saop;
					m_Srmx = prlLocalOpt->Srmx;
				}
				ilRun++;
			} while ( (ilRun < ilCount) && !blWhatIfFound);
		}
	}
	else
	{
		m_Solp = prmOpt->Solp;
		m_Stec = prmOpt->Stec;
		m_Sdtl = prmOpt->Sdtl;
		m_Squa = prmOpt->Squa;
		m_Sbre = prmOpt->Sbre;
		m_Swol = prmOpt->Swol;
		m_Smrt = prmOpt->Smrt;
		m_Sdis = prmOpt->Sdis;
		m_Shis = prmOpt->Shis;
		m_Sjmp = prmOpt->Sjmp;
		m_Svmf = prmOpt->Svmf;
		m_Spra = prmOpt->Spra;
		m_Snea = prmOpt->Snea;
		m_Saop = prmOpt->Saop;
		m_Srmx = prmOpt->Srmx;
	}
}


void CWhatIf::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CWhatIf)
	DDX_Control(pDX, IDC_EDIT_WOL, m_WolEdit);
	DDX_Control(pDX, IDC_EDIT_VMF, m_VmfEdit);
	DDX_Control(pDX, IDC_EDIT_TEC, m_TecEdit);
	DDX_Control(pDX, IDC_EDIT_RMX, m_RmxEdit);
	DDX_Control(pDX, IDC_EDIT_QUA, m_QuaEdit);
	DDX_Control(pDX, IDC_EDIT_PRA, m_PraEdit);
	DDX_Control(pDX, IDC_EDIT_OLP, m_OlpEdit);
	DDX_Control(pDX, IDC_EDIT_NEA, m_NeaEdit);
	DDX_Control(pDX, IDC_EDIT_MRT, m_MrtEdit);
	DDX_Control(pDX, IDC_EDIT_JMP, m_JmpEdit);
	DDX_Control(pDX, IDC_EDIT_HIS, m_HisEdit);
	DDX_Control(pDX, IDC_EDIT_DTL, m_DtlEdit);
	DDX_Control(pDX, IDC_EDIT_DIS, m_DisEdit);
	DDX_Control(pDX, IDC_EDIT_BRE, m_BreEdit);
	DDX_Control(pDX, IDC_EDIT_AOP, m_SaopEdit);
	DDX_Control(pDX, IDC_COMMENT, m_Comment);
	DDX_Control(pDX, IDC_FREETXT, m_Freetxt);
	DDX_Control(pDX, IDC_CHK_EMF, m_Emf);
	DDX_Control(pDX, IDC_CHK_FMF, m_Fmf);
	DDX_Control(pDX, IDC_SL_WOL, m_SliderWol);
	DDX_Control(pDX, IDC_SL_TEC, m_SliderTec);
	DDX_Control(pDX, IDC_SL_RMX, m_SliderRmx);
	DDX_Control(pDX, IDC_SL_QUA, m_SliderQua);
	DDX_Control(pDX, IDC_SL_OLP, m_SliderOlp);
	DDX_Control(pDX, IDC_SL_DTL, m_SliderDtl);
	DDX_Control(pDX, IDC_SL_BRE, m_SliderBre);
	DDX_Control(pDX, IDC_SL_AOP, m_SliderAop);
	DDX_Control(pDX, IDC_SL_DIS, m_SliderDis);
	DDX_Control(pDX, IDC_SL_HIS, m_SliderHis);
	DDX_Control(pDX, IDC_SL_JMP, m_SliderJmp);
	DDX_Control(pDX, IDC_SL_VMF, m_SliderVmf);
	DDX_Control(pDX, IDC_SL_PRA, m_SliderPra);
	DDX_Control(pDX, IDC_SL_NEA, m_SliderNea);
	DDX_Text(pDX, IDC_EDIT_OLP, m_Solp);
	DDX_Text(pDX, IDC_EDIT_TEC, m_Stec);
	DDX_Text(pDX, IDC_EDIT_DTL, m_Sdtl);
	DDX_Text(pDX, IDC_EDIT_QUA, m_Squa);
	DDX_Text(pDX, IDC_EDIT_BRE, m_Sbre);
	DDX_Text(pDX, IDC_EDIT_WOL, m_Swol);
	DDX_Text(pDX, IDC_EDIT_MRT, m_Smrt);
	DDX_Text(pDX, IDC_EDIT_DIS, m_Sdis);
	DDX_Text(pDX, IDC_EDIT_HIS, m_Shis);
	DDX_Text(pDX, IDC_EDIT_JMP, m_Sjmp);
	DDX_Text(pDX, IDC_EDIT_VMF, m_Svmf);
	DDX_Text(pDX, IDC_EDIT_PRA, m_Spra);
	DDX_Text(pDX, IDC_EDIT_NEA, m_Snea);
	DDX_Text(pDX, IDC_EDIT_AOP, m_Saop);
	DDX_Text(pDX, IDC_EDIT_RMX, m_Srmx);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CWhatIf, CDialog)
	//{{AFX_MSG_MAP(CWhatIf)
	ON_WM_HSCROLL()
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_CHK_EMF, OnChkEmf)
	ON_BN_CLICKED(IDC_CHK_FMF, OnChkFmf)
	ON_BN_CLICKED(IDRESET, OnReset)
	ON_BN_CLICKED(IDDELETE, OnDelete)
	ON_BN_CLICKED(IDSTORE, OnStore)
	ON_BN_CLICKED(IDSTORETO, OnStoreTo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWhatIf message handlers

void CWhatIf::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// TODO: Add your message handler code here and/or call default
	if(isSetFromProgram != TRUE)
	{
		UpdateData(FALSE);

		MakeParameters();
	}
}

void CWhatIf::MakeParameters()
{
	m_Swol = m_SliderWol.GetPos();
	m_Stec = m_SliderTec.GetPos();
	m_Srmx = m_SliderRmx.GetPos();
	m_Squa = m_SliderQua.GetPos();
	m_Sdtl = m_SliderDtl.GetPos();
	m_Sbre = m_SliderBre.GetPos();
	m_Solp = m_SliderOlp.GetPos();
	//m_Smrt = m_SliderMrt.GetPos();
	m_Sdis = m_SliderDis.GetPos();
	m_Shis = m_SliderHis.GetPos();
	m_Sjmp = m_SliderJmp.GetPos();
	m_Svmf = m_SliderVmf.GetPos();
	m_Spra = m_SliderPra.GetPos();
	m_Snea = m_SliderNea.GetPos();
	m_Saop = m_SliderAop.GetPos();
	m_Srmx = m_SliderRmx.GetPos();
}

BOOL CWhatIf::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(GetString(IDS_STRING33086));
	CWnd *polWnd = GetDlgItem(IDC_MAINPARAMETERS); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33087));
	}
	polWnd = GetDlgItem(IDC_CHK_EMF); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33088));
	}
	polWnd = GetDlgItem(IDC_CHK_FMF); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33089));
	}
	polWnd = GetDlgItem(IDC_ST_MRT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33090));
	}
	polWnd = GetDlgItem(IDC_ST_AOP); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33091));
	}
	polWnd = GetDlgItem(IDC_SECONDARYPARAMETERS); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33092));
	}
	polWnd = GetDlgItem(IDC_LOW); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33093));
	}
	polWnd = GetDlgItem(IDC_MEDIUM); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33094));
	}
	polWnd = GetDlgItem(IDC_HIGH); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33095));
	}
	polWnd = GetDlgItem(IDC_ST_DIS); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33096));
	}
	polWnd = GetDlgItem(IDC_ST_HIS); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33097));
	}
	polWnd = GetDlgItem(IDC_ST_JMP); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33098));
	}
	polWnd = GetDlgItem(IDC_ST_WOL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33099));
	}
	polWnd = GetDlgItem(IDC_ST_VMF); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33100));
	}
	polWnd = GetDlgItem(IDC_ST_BRE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33101));
	}
	polWnd = GetDlgItem(IDC_ST_QUA); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33102));
	}
	polWnd = GetDlgItem(IDC_ST_DTL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33103));
	}
	polWnd = GetDlgItem(IDC_ST_TEC); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33104));
	}
	polWnd = GetDlgItem(IDC_ST_OLP); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33105));
	}
	polWnd = GetDlgItem(IDC_ST_RMX); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33106));
	}
	polWnd = GetDlgItem(IDC_ST_PRA); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33107));
	}
	polWnd = GetDlgItem(IDC_ST_NEA); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33108));
	}
	polWnd = GetDlgItem(IDC_REMARK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32885));
	}
	polWnd = GetDlgItem(IDC_FREE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33109));
	}
	polWnd = GetDlgItem(IDSTORE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33055));
	}
	polWnd = GetDlgItem(IDSTORETO); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33110));
	}
	polWnd = GetDlgItem(IDDELETE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33111));
	}
	polWnd = GetDlgItem(IDRESET); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33112));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61698));
	}
	polWnd = GetDlgItem(IDOK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61697));
	}

	// TODO: Add extra initialization here
	m_SliderWol.SetRange(0, 100);	
	m_SliderWol.SetTicFreq(5);
    m_SliderWol.SetPos(m_Swol);

	m_SliderTec.SetRange(0, 100);	
	m_SliderTec.SetTicFreq(5);
    m_SliderTec.SetPos(m_Stec);

	m_SliderRmx.SetRange(0, 100);	
	m_SliderRmx.SetTicFreq(5);
    m_SliderRmx.SetPos(m_Srmx);

	m_SliderQua.SetRange(0, 100);	
	m_SliderQua.SetTicFreq(5);
    m_SliderQua.SetPos(m_Squa);

	m_SliderOlp.SetRange(0, 100);	
	m_SliderOlp.SetTicFreq(5);
    m_SliderOlp.SetPos(m_Solp);

	m_SliderDtl.SetRange(0, 100);	
	m_SliderDtl.SetTicFreq(5);
    m_SliderDtl.SetPos(m_Sdtl);

	m_SliderBre.SetRange(0, 100);	
	m_SliderBre.SetTicFreq(5);
    m_SliderBre.SetPos(m_Sbre);

	m_SliderAop.SetRange(0, 100);	
	m_SliderAop.SetTicFreq(5);
    m_SliderAop.SetPos(m_Saop);

/*
	m_SliderMrt.SetRange(0, 100);
	m_SliderMrt.SetTicFreq(5);
    m_SliderMrt.SetPos(m_Smrt);
*/

	m_SliderDis.SetRange(0, 100);
	m_SliderDis.SetTicFreq(5);
    m_SliderDis.SetPos(m_Sdis);

	m_SliderHis.SetRange(0, 100);
	m_SliderHis.SetTicFreq(5);
    m_SliderHis.SetPos(m_Shis);

	m_SliderJmp.SetRange(0, 100);
	m_SliderJmp.SetTicFreq(5);
    m_SliderJmp.SetPos(m_Sjmp);

	m_SliderVmf.SetRange(0, 100);
	m_SliderVmf.SetTicFreq(5);
    m_SliderVmf.SetPos(m_Svmf);

	m_SliderPra.SetRange(0, 100);
	m_SliderPra.SetTicFreq(5);
    m_SliderPra.SetPos(m_Spra);

	m_SliderNea.SetRange(0, 100);
	m_SliderNea.SetTicFreq(5);
    m_SliderNea.SetPos(m_Snea);

	int	 ilRun = 0;
	BOOL blWhatIfFound = FALSE;
	int  ilCount = ogOptData.omData.GetSize();
	if (ilCount > 0)
	{
		do
		{
			OPTDATA *prlLocalOpt = &ogOptData.omData[ilRun];
//			if ( ( (strcmp(prlLocalOpt->Ckey, pcmParaSet) == 0) &&
//				   (strncmp(prlLocalOpt->Ctyp, "PARASET",7) == 0)     ) ||
//				 ( (strcmp(prlLocalOpt->Ckey, ogWhatIfKey) == 0) &&
//				   (strcmp("Neuer Parameterset", pcmParaSet) == 0)     &&
//				   (strcmp(prlLocalOpt->Ctyp, "WHAT-IF") == 0)      )    )
			if ( ( (strcmp(prlLocalOpt->Ckey, pcmParaSet) == 0) &&
				   (strncmp(prlLocalOpt->Ctyp, "PARASET",7) == 0)     ) ||
				 ( (strcmp(prlLocalOpt->Ckey, ogWhatIfKey) == 0) &&
				   (strcmp(GetString(IDS_STRING61295), pcmParaSet) == 0)     &&
				   (strcmp(prlLocalOpt->Ctyp, "WHAT-IF") == 0)      )    )

			{
				((CButton *)GetDlgItem(IDC_CHK_EMF))->SetCheck(prlLocalOpt->Cemf);
				((CButton *)GetDlgItem(IDC_CHK_FMF))->SetCheck(prlLocalOpt->Cfmf);

				m_Comment.SetWindowText(prlLocalOpt->Text);
				m_Freetxt.SetWindowText(prlLocalOpt->Ftxt);
				blWhatIfFound = TRUE;
			}
			ilRun++;
		} while ( (ilRun < ilCount) && !blWhatIfFound);
	}

	if (imActualKind == VORPLANUNG)
		SetSlider(TRUE,FALSE,TRUE,TRUE,TRUE,TRUE,TRUE,FALSE,FALSE,FALSE,FALSE,TRUE,TRUE,FALSE,FALSE,TRUE,FALSE);
	else if (imActualKind == EINSATZPLANUNG)
		SetSlider(TRUE,TRUE,TRUE,FALSE,FALSE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE);
	else if (imActualKind == CCI_PLANUNG)
		SetSlider(TRUE,FALSE,TRUE,FALSE,FALSE,TRUE,TRUE,TRUE,FALSE,TRUE,TRUE,FALSE,TRUE,FALSE,FALSE,TRUE,FALSE);

	GetDlgItem(IDSTORE)->EnableWindow(TRUE);

	SetCaption();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CWhatIf::SetCaption()
{
	//CString olCaption = "Parameter ";
	CString olCaption = GetString(IDS_STRING61339);
	
	switch(imActualKind)
	{
	case VORPLANUNG:
		//olCaption += "Vorplanung   ";
		olCaption += GetString(IDS_STRING61340);
		break;
	case EINSATZPLANUNG:
		//olCaption += "Einsatzplanung   ";
		olCaption += GetString(IDS_STRING61341);
		break;
	case CCI_PLANUNG:
		//olCaption += "CCI-Planung   ";
		olCaption += GetString(IDS_STRING61342);
		break;
	}

	//olCaption += CString("Parameterset: ") + pcmParaSet;
	olCaption += GetString(IDS_STRING61343) + pcmParaSet;
	SetWindowText(olCaption);

}

HBRUSH CWhatIf::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
		HBRUSH hbr = 0;
		long llRc;

		CWnd *polWnd = GetDlgItem(IDC_EDIT_OLP);
		if (polWnd->m_hWnd == pWnd->m_hWnd)
		{
			llRc = pDC->SetBkColor(::GetSysColor(COLOR_BTNFACE));
			llRc = pDC->SetTextColor(BLACK);

			hbr = CreateSolidBrush(::GetSysColor(COLOR_BTNFACE));

			RECT rpRect;
			pWnd->GetClientRect(&rpRect);

			return hbr;
		}
		polWnd = GetDlgItem(IDC_EDIT_TEC);
		if (polWnd->m_hWnd == pWnd->m_hWnd)
		{
			llRc = pDC->SetBkColor(::GetSysColor(COLOR_BTNFACE));
			llRc = pDC->SetTextColor(BLACK);

			hbr = CreateSolidBrush(::GetSysColor(COLOR_BTNFACE));

			RECT rpRect;
			pWnd->GetClientRect(&rpRect);

			return hbr;
		}
		polWnd = GetDlgItem(IDC_EDIT_DTL);
		if (polWnd->m_hWnd == pWnd->m_hWnd)
		{
			llRc = pDC->SetBkColor(::GetSysColor(COLOR_BTNFACE));
			llRc = pDC->SetTextColor(BLACK);

			hbr = CreateSolidBrush(::GetSysColor(COLOR_BTNFACE));

			RECT rpRect;
			pWnd->GetClientRect(&rpRect);

			return hbr;
		}
		polWnd = GetDlgItem(IDC_EDIT_QUA);
		if (polWnd->m_hWnd == pWnd->m_hWnd)
		{
			llRc = pDC->SetBkColor(::GetSysColor(COLOR_BTNFACE));
			llRc = pDC->SetTextColor(BLACK);

			hbr = CreateSolidBrush(::GetSysColor(COLOR_BTNFACE));

			RECT rpRect;
			pWnd->GetClientRect(&rpRect);

			return hbr;
		}
		polWnd = GetDlgItem(IDC_EDIT_BRE);
		if (polWnd->m_hWnd == pWnd->m_hWnd)
		{
			llRc = pDC->SetBkColor(::GetSysColor(COLOR_BTNFACE));
			llRc = pDC->SetTextColor(BLACK);

			hbr = CreateSolidBrush(::GetSysColor(COLOR_BTNFACE));

			RECT rpRect;
			pWnd->GetClientRect(&rpRect);

			return hbr;
		}
		polWnd = GetDlgItem(IDC_EDIT_WOL);
		if (polWnd->m_hWnd == pWnd->m_hWnd)
		{
			llRc = pDC->SetBkColor(::GetSysColor(COLOR_BTNFACE));
			llRc = pDC->SetTextColor(BLACK);

			hbr = CreateSolidBrush(::GetSysColor(COLOR_BTNFACE));

			RECT rpRect;
			pWnd->GetClientRect(&rpRect);

			return hbr;
		}
	return CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
}


void CWhatIf::GetParaData(OPTDATA *prpOpt) 
{
	char	pclBuf[512];

	
	UpdateData(TRUE);
	prpOpt->Cemf = m_Emf.GetCheck();
	prpOpt->Cfmf = m_Fmf.GetCheck();
	prpOpt->Smrt = m_Smrt;
	prpOpt->Saop = m_Saop;
	prpOpt->Sdis = m_Sdis;
	prpOpt->Shis = m_Shis;
	prpOpt->Sjmp = m_Sjmp;
	prpOpt->Swol = m_Swol;
	prpOpt->Svmf = m_Svmf;
	prpOpt->Sbre = m_Sbre;
	prpOpt->Squa = m_Squa;
	prpOpt->Sdtl = m_Sdtl;
	prpOpt->Stec = m_Stec;
	prpOpt->Solp = m_Solp;
	prpOpt->Srmx = m_Srmx;
	prpOpt->Spra = m_Spra;
	prpOpt->Snea = m_Snea;

	m_Comment.GetWindowText(pclBuf, sizeof(pclBuf));
	strcpy(prpOpt->Text, pclBuf);

	m_Freetxt.GetWindowText(pclBuf, sizeof(pclBuf));
	strcpy(prpOpt->Ftxt, pclBuf);
}


void CWhatIf::OnOK() 
{
	UpdateData(TRUE);

	GetParaData(prmOpt);

	EndDialog(IDOK);
}


void CWhatIf::ClearScreen()
{
	m_Solp = 0;
	m_Stec = 0;
	m_Sdtl = 0;
	m_Squa = 0;
	m_Swol = 0;
	m_Sbre = 0;
	m_Smrt = 0;
	m_Sdis = 0;
	m_Shis = 0;
	m_Sjmp = 0;
	m_Svmf = 0;
	m_Spra = 0;
	m_Snea = 0;
	m_Saop = 0;
	m_Srmx = 0;
	m_SliderWol.SetPos(0);
	m_SliderTec.SetPos(0);
	m_SliderRmx.SetPos(0);
	m_SliderQua.SetPos(0);
	m_SliderOlp.SetPos(0);
	m_SliderDtl.SetPos(0);
	m_SliderBre.SetPos(0);
	m_SliderAop.SetPos(0);
	//m_SliderMrt.SetPos(0);
	m_SliderDis.SetPos(0);
	m_SliderHis.SetPos(0);
	m_SliderJmp.SetPos(0);
	m_SliderVmf.SetPos(0);
	m_SliderPra.SetPos(0);
	m_SliderNea.SetPos(0);
	m_SliderAop.SetPos(0);
	m_SliderRmx.SetPos(0);

	((CButton *)GetDlgItem(IDC_CHK_EMF))->SetCheck(0);
	((CButton *)GetDlgItem(IDC_CHK_FMF))->SetCheck(0);

	m_Comment.SetWindowText("");
	m_Freetxt.SetWindowText("");

	UpdateData(FALSE);
}

void CWhatIf::OnChkEmf() 
{
	MakeParameters();
}

void CWhatIf::OnChkFmf() 
{
	MakeParameters();
}



void CWhatIf::SetSlider(BOOL ipVal1,BOOL ipVal2,BOOL ipVal3,BOOL ipVal4,BOOL ipVal5,BOOL ipVal6,BOOL ipVal7,BOOL ipVal8,BOOL ipVal9,BOOL ipVal10,BOOL ipVal11,BOOL ipVal12,BOOL ipVal13,BOOL ipVal14,BOOL ipVal15,BOOL ipVal16,BOOL ipVal17 ) 
{
	m_MrtEdit.EnableWindow(ipVal1);
	GetDlgItem(IDC_ST_MRT)->EnableWindow(ipVal1);
	GetDlgItem(IDC_SL_AOP)->EnableWindow(ipVal2);
	GetDlgItem(IDC_ST_AOP)->EnableWindow(ipVal2);
	m_SaopEdit.EnableWindow(ipVal2);

	GetDlgItem(IDC_CHK_EMF)->EnableWindow(ipVal3);
	GetDlgItem(IDC_CHK_FMF)->EnableWindow(ipVal4);

	GetDlgItem(IDC_SL_DIS)->EnableWindow(ipVal5);
	GetDlgItem(IDC_ST_DIS)->EnableWindow(ipVal5);
	m_DisEdit.EnableWindow(ipVal5);
	GetDlgItem(IDC_SL_HIS)->EnableWindow(ipVal6);
	GetDlgItem(IDC_ST_HIS)->EnableWindow(ipVal6);
	m_HisEdit.EnableWindow(ipVal6);
	GetDlgItem(IDC_SL_JMP)->EnableWindow(ipVal7);
	GetDlgItem(IDC_ST_JMP)->EnableWindow(ipVal7);
	m_JmpEdit.EnableWindow(ipVal7);
	GetDlgItem(IDC_SL_WOL)->EnableWindow(ipVal8);
	GetDlgItem(IDC_ST_WOL)->EnableWindow(ipVal8);
	m_WolEdit.EnableWindow(ipVal8);
	GetDlgItem(IDC_SL_VMF)->EnableWindow(ipVal9);
	GetDlgItem(IDC_ST_VMF)->EnableWindow(ipVal9);
	m_VmfEdit.EnableWindow(ipVal9);
	GetDlgItem(IDC_SL_BRE)->EnableWindow(ipVal10);
	GetDlgItem(IDC_ST_BRE)->EnableWindow(ipVal10);
	m_BreEdit.EnableWindow(ipVal10);
	GetDlgItem(IDC_SL_QUA)->EnableWindow(ipVal11);
	GetDlgItem(IDC_ST_QUA)->EnableWindow(ipVal11);
	m_QuaEdit.EnableWindow(ipVal11);
	GetDlgItem(IDC_SL_DTL)->EnableWindow(ipVal12);
	GetDlgItem(IDC_ST_DTL)->EnableWindow(ipVal12);
	m_DtlEdit.EnableWindow(ipVal12);
	GetDlgItem(IDC_SL_TEC)->EnableWindow(ipVal13);
	GetDlgItem(IDC_ST_TEC)->EnableWindow(ipVal13);
	m_TecEdit.EnableWindow(ipVal13);
	GetDlgItem(IDC_SL_OLP)->EnableWindow(ipVal14);
	GetDlgItem(IDC_ST_OLP)->EnableWindow(ipVal14);
	m_OlpEdit.EnableWindow(ipVal14);
	GetDlgItem(IDC_SL_RMX)->EnableWindow(ipVal15);
	GetDlgItem(IDC_ST_RMX)->EnableWindow(ipVal15);
	m_RmxEdit.EnableWindow(ipVal15);
	GetDlgItem(IDC_SL_PRA)->EnableWindow(ipVal16);
	GetDlgItem(IDC_ST_PRA)->EnableWindow(ipVal16);
	m_PraEdit.EnableWindow(ipVal16);
	GetDlgItem(IDC_SL_NEA)->EnableWindow(ipVal17);
	GetDlgItem(IDC_ST_NEA)->EnableWindow(ipVal17);
	m_NeaEdit.EnableWindow(ipVal17);
}

void CWhatIf::OnReset() 
{
	//Reset display 
	m_Swol = 0;
	m_Solp = 0;
	m_Stec = 0;
	m_Sdtl = 0;
	m_Squa = 0;
	m_Sbre = 0;
	m_Smrt = 0;
	m_Sdis = 0;
	m_Shis = 0;
	m_Sjmp = 0;
	m_Svmf = 0;
	m_Spra = 0;
	m_Snea = 0;
	m_Saop = 0;
	m_Srmx = 0;
	m_SliderWol.SetPos(0);
	m_SliderTec.SetPos(0);
	m_SliderRmx.SetPos(0);
	m_SliderQua.SetPos(0);
	m_SliderOlp.SetPos(0);
	m_SliderDtl.SetPos(0);
	m_SliderBre.SetPos(0);
	m_SliderAop.SetPos(0);
	//m_SliderMrt.SetPos(0);
	m_SliderDis.SetPos(0);
	m_SliderHis.SetPos(0);
	m_SliderJmp.SetPos(0);
	m_SliderVmf.SetPos(0);
	m_SliderPra.SetPos(0);
	m_SliderNea.SetPos(0);
	m_SliderAop.SetPos(0);
	m_SliderRmx.SetPos(0);

	((CButton *)GetDlgItem(IDC_CHK_EMF))->SetCheck(0);
	((CButton *)GetDlgItem(IDC_CHK_FMF))->SetCheck(0);

	m_Comment.SetWindowText("");
	m_Freetxt.SetWindowText("");

	UpdateData(FALSE);
}

void CWhatIf::OnDelete() 
{
/*
	BOOL bpDoDelete = FALSE;
	char pclDelParaSet[82];
	OPTDATA *prlOpt = new OPTDATA;

	sprintf(pclDelParaSet, "");
	CNewParaSet *polNewParaSetDlg = new CNewParaSet((CWnd *)this, pclDelParaSet);
	if(polNewParaSetDlg->DoModal() == IDOK)
	{
		if ( (strcmp(pclDelParaSet, "") != 0)			  &&
		     (strcmp(pclDelParaSet, "Neuer Parameterset") != 0)    )
		{
			int ilCount = ogOptData.omData.GetSize();
			for (int i = 0; i < ilCount; i++)
			{
				OPTDATA *prlLocalOpt = &ogOptData.omData[i];
				if ( (strcmp(prlLocalOpt->Ctyp, "PARASET") == 0) &&
					 (strcmp(prlLocalOpt->Ckey, pclDelParaSet) == 0)       )
				{
					bpDoDelete = TRUE;
					prlOpt = prlLocalOpt;
				}
			}

			if (bpDoDelete)
			{
				if(ogOptData.DeleteOpt(prlOpt->Urno) == RCSuccess)
				{
					//Yeeha
				}
				else
					MessageBox("L�schen abgebrochen.");
			}
			else
				MessageBox("Dieser Parameterset existiert noch nicht. L�schen abgebrochen.");
		}
		else if (strcmp(pclDelParaSet, "Neuer Parameterset") == 0)
			MessageBox("Dieser Parameterset kann nicht gel�scht werden. L�schen abgebrochen.");
		else
			MessageBox("L�schen abgebrochen.");
	}
	delete polNewParaSetDlg;
*/
	//if (MessageBox("Soll der Parameterset wirklich gel�scht werden?", "L�schen", MB_YESNO) == IDYES)
	if (MessageBox(GetString(IDS_STRING61516), GetString(IDS_STRING61210), MB_YESNO) == IDYES)
	{
		BOOL bpWhatIfFound = FALSE;

		//if ( (strcmp(pcmParaSet, "") != 0) && (strcmp(pcmParaSet, "Neuer Parameterset") != 0)    )
		if((strcmp(pcmParaSet, "") != 0) && (strcmp(pcmParaSet, GetString(IDS_STRING61295)) != 0))
		{
			int ilCount = ogOptData.omData.GetSize();
			for (int i = 0; (i < ilCount) && (bpWhatIfFound == FALSE); i++)
			{
				OPTDATA *prlLocalOpt = &ogOptData.omData[i];
				if ( (strncmp(prlLocalOpt->Ctyp, "PARASET",7) == 0)  &&
					 (strcmp(prlLocalOpt->Ckey, pcmParaSet) == 0)    )
				{
					if(ogOptData.DeleteOpt(prlLocalOpt->Urno) == RCSuccess)
					{
						//Yeeha
						GetDlgItem(IDSTORE)->EnableWindow(FALSE);
					}
					else
					{
						//MessageBox("L�schen abgebrochen.");
						MessageBox(GetString(IDS_STRING61518));
					}

					bpWhatIfFound = TRUE;
				}
			}
		}
		//else if (strcmp(pcmParaSet, "Neuer Parameterset") == 0)
		else if (strcmp(pcmParaSet, GetString(IDS_STRING61295)) == 0)
		{
			//MessageBox("Dieser Parameterset kann nicht gel�scht werden. L�schen abgebrochen.");
			MessageBox(GetString(IDS_STRING61519));
		}
		else
		{
			//MessageBox("L�schen abgebrochen.");
			MessageBox(GetString(IDS_STRING61518));
		}
	}
}

void CWhatIf::OnStore() 
{
	BOOL bpDoUpdate = FALSE;
	OPTDATA *prlOpt = new OPTDATA;
	UpdateData(TRUE);

	//if((strcmp(pcmParaSet, "") != 0) && (strcmp(pcmParaSet, "Neuer Parameterset") != 0))
	if((strcmp(pcmParaSet, "") != 0) && (strcmp(pcmParaSet, GetString(IDS_STRING61295)) != 0))
	{
		int	 ilRun = 0;
		BOOL blWhatIfFound = FALSE;
		int  ilCount = ogOptData.omData.GetSize();

		if (ilCount > 0)
		{
			do
			{
				OPTDATA *prlLocalOpt = &ogOptData.omData[ilRun];
				if ( (strcmp(prlLocalOpt->Ckey, pcmParaSet) == 0) &&
					 (strncmp(prlLocalOpt->Ctyp, "PARASET",7) == 0)     )
				{
					blWhatIfFound = TRUE;
					prlOpt = prlLocalOpt;
				}
				ilRun++;
			} while ( (ilRun < ilCount) && !blWhatIfFound);

			if (blWhatIfFound)
			{
				GetParaData(prlOpt);

				//Updates only to the REAL-Data
					BOOL bpWhatIfFound = FALSE;
					int ilCount = ogOptData.omData.GetSize();
					for (int i = 0; (i < ilCount) && (bpWhatIfFound == FALSE); i++)
					{
						OPTDATA *prlAktOpt = &ogOptData.omData[i];
						if ( (strncmp(prlAktOpt->Ctyp, "PARASET",7) == 0)    &&
							 (strcmp(prlAktOpt->Ckey, prlOpt->Ckey) == 0)    )
						{
							bpWhatIfFound = TRUE;
							prlAktOpt->IsChanged = DATA_CHANGED;
							memcpy(prlAktOpt,prlOpt,sizeof(OPTDATA));
							ogOptData.ChangeOpt(prlAktOpt);
						}
					}
			}
			else
			{
				//MessageBox("Speichern abgebrochen.");
				MessageBox(GetString(IDS_STRING61520));
			}
		}
		else
		{
			//MessageBox("Speichern abgebrochen.");
			MessageBox(GetString(IDS_STRING61520));
		}
	}
	//else if (strcmp(pcmParaSet, "Neuer Parameterset") == 0)
	else if (strcmp(pcmParaSet, GetString(IDS_STRING61295)) == 0)
	{
		//MessageBox("Dieser Parameterset kann nicht gespeichert werden. Speichern abgebrochen.");
		MessageBox(GetString(IDS_STRING61521));
	}
	else
	{
		//MessageBox("Speichern abgebrochen.");
		MessageBox(GetString(IDS_STRING61520));
	}
}

void CWhatIf::OnStoreTo() 
{
	BOOL bpDoUpdate = FALSE;
	char pclNewParaSet[82];
	CString olParaset = "PARASET";
	UpdateData(TRUE);

	switch(imActualKind)
	{
	case VORPLANUNG:
		olParaset += "V";
		break;
	case EINSATZPLANUNG:
		olParaset += "E";
		break;
	case CCI_PLANUNG: 
		olParaset += "C";
		break;
	}


	sprintf(pclNewParaSet, "");
	CNewParaSet *polNewParaSetDlg = new CNewParaSet((CWnd *)this, pclNewParaSet);
	if(polNewParaSetDlg->DoModal() == IDOK)
	{
		//if((strcmp(pclNewParaSet,"") != 0) && (strcmp(pclNewParaSet, "Neuer Parameterset") != 0)    )
		if((strcmp(pclNewParaSet,"") != 0) && (strcmp(pclNewParaSet, GetString(IDS_STRING61295)) != 0)    )
		{
			int ilCount = ogOptData.omData.GetSize();
			for (int i = 0; i < ilCount; i++)
			{
				OPTDATA rlOpt = ogOptData.omData[i];
				if ( (strcmp(rlOpt.Ctyp, olParaset) == 0) &&
					 (strcmp(rlOpt.Ckey, pclNewParaSet) == 0)       )
				{
					bpDoUpdate = TRUE;
				}
			}

			OPTDATA *prlOpt = new OPTDATA;

			GetParaData(prlOpt);

			strcpy(prlOpt->Ctyp, olParaset);
			strcpy(prlOpt->Ckey, pclNewParaSet);
			prlOpt->Urno = ogBasicData.GetNextUrno();

			if (!bpDoUpdate)
			{
				prlOpt->IsChanged = DATA_NEW;
				ogOptData.AddOpt(prlOpt);

				GetDlgItem(IDSTORE)->EnableWindow(TRUE);
			}
			else if (bpDoUpdate)
			{
 				prlOpt->IsChanged = DATA_CHANGED;
				ogOptData.ChangeOpt(prlOpt);

				GetDlgItem(IDSTORE)->EnableWindow(TRUE);
			}

			sprintf(pcmParaSet, "%s", pclNewParaSet);
		}
		else
		{
			//MessageBox("Ung�ltige Eingabe. Speichern abgebrochen.");
			MessageBox(GetString(IDS_STRING61522));
		}
	}
	delete polNewParaSetDlg;
	SetCaption();
}

