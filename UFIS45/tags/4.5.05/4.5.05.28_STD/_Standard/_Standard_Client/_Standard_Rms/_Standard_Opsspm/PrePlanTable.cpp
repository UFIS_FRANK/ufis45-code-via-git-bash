// preplant.cpp - Class for performing the pre-planning functionality
//
//
// Written by:
// Damkerng Thammathakerngkit   April 27, 1996
//
// Modification History:
// May 3, 1996      Damkerng    Add the private data member "lmDate" for InsertEmployee().

#include <time.h>
#include <stdlib.h>
#include <stdafx.h>
#include <ccsglobl.h>
#include <ccsobj.h>
#include <resource.h>
#include <ufis.h>
#include <CCSCedaCom.h>
#include <CCSCedaData.h>
#include <table.h>
#include <StaffFilterDlg.h>
#include <StaffSortDlg.h>
#include <CedaEmpData.h>
#include <CedaJobData.h>
#include <CedaSprData.h>
#include <CedaShiftData.h>
#include <CedaShiftTypeData.h>
#include <CedaEmpData.h>
#include <EmpDialog.h>
#include <CCSDragDropCtrl.h>
#include <Search.h>
#include <FilterData.h>

#include <cviewer.h>
#include <FilterPage.h>
#include <ZeitPage.h>
#include <PrePlanTableSortPage.h>
#include <PrePlanTable.h>
#include <BasePropertySheet.h>
#include <PrePlanTablePropertySheet.h>
#include <ccsddx.h>
#include <PrePlanTeamTable.h>
#include <BasicData.h>
#include <Dataset.h>
#include <CedaCfgData.h>
#include <table.h>
#include <CedaOdaData.h>
#include <CedaDrgData.h>
#include <CedaDrdData.h>
#include <CedaDraData.h>
#include <cxbutton.h>
#include <CedaShiftData.h>
#include <CCIDiagram.h>
#include <CCITable.h>
#include <FlightPlan.h>
#include <ConflictConfigTable.h>
#include <StaffDiagram.h>
#include <GateDiagram.h>
#include <RegnDiagram.h>
#include <StaffTable.h>
#include <GateTable.h>
#include <ConflictTable.h>
#include <ButtonList.h>
#include <TransactionManager.h>
#include <SwapShiftDlg.h>
#include <PrintTerminalSelection.h>
#include <CPrintPreviewView.h>
#include <AfxPriv.h>
#include <OpssPm.h>

const CString PrePlanTable::Terminal2 = "T2";
const CString PrePlanTable::Terminal3 = "T3";

/////////////////////////////////////////////////////////////////////////////
// Compare routines -- used internally
static int ByShiftStart(const SHIFTDATA **pppShift1, const SHIFTDATA **pppShift2);
static int ByShiftStart(const SHIFTDATA **pppShift1, const SHIFTDATA **pppShift2)
{
	return (int)((**pppShift1).Avfa.GetTime() - (**pppShift2).Avfa.GetTime());
}


static int CompareJobStartTime(const JOBDATA **e1, const JOBDATA **e2)
{
	if ((strcmp((**e1).Jtco,JOBPOOL) != 0) && (strcmp((**e2).Jtco,JOBPOOL) == 0) )
	{
		return -1;
	}
	else
	{
		if ((strcmp((**e1).Jtco,JOBPOOL) == 0) && (strcmp((**e2).Jtco,JOBPOOL) != 0) )
		{
			return 1;
		}
		else
		{
			//if ((**e1).Acfr.GetTime() == (**e2).Acfr.GetTime() == 0)
			
			if ((**e2).Acfr.GetTime() < (**e1).Acfr.GetTime())
				return -1;
			if ((**e2).Acfr.GetTime() > (**e1).Acfr.GetTime())
				return 1;
				return 0;

	//		return (int)((**e1).Acfr.GetTime() - (**e2).Acfr.GetTime());
		}
	}
	
}


static int CompareShiftRank(const SHIFTDATA **e1, const SHIFTDATA **e2)
{

	/* Empdata is not required, we can compare the rank of Shiftdata 
	EMPDATA *prlEmp1 = ogEmpData.GetEmpByPeno((**e1).Peno);
	EMPDATA *prlEmp2 = ogEmpData.GetEmpByPeno((**e2).Peno);
	if ((prlEmp1 != NULL) && (prlEmp2 != NULL))
	{
		int ilOffset1 = pclRanks.Find(CString(":")+prlEmp1->Rank+":");
		int ilOffset2 = pclRanks.Find(CString(":")+prlEmp2->Rank+":");
		return ilOffset2 - ilOffset1;
	}
	*/
//		int ilOffset1 = pclRanks.Find(CString(":")+(**e1).Efct+":");
//		int ilOffset2 = pclRanks.Find(CString(":")+(**e2).Efct+":");
//		return ilOffset2 - ilOffset1;
	return 0;
}

/////////////////////////////////////////////////////////////////////////////
// PrePlanTable dialog

PrePlanTable::PrePlanTable(CString opDate)
    : CDialog(PrePlanTable::IDD, NULL)
{
    //{{AFX_DATA_INIT(PrePlanTable)
    m_bCompleteTeam = FALSE;
    m_bAbsent = FALSE;
    //}}AFX_DATA_INIT

	pomTable = &omTable;   //Singapore
	pomViewer = &omViewer; //Singapore
	pomPrintPreviewView = NULL; //Singapore
	pomTeamTable = NULL;
	bmIsTeamDetailOpen = FALSE;
	bmIsFromSearch = FALSE;
	bmIsDetailDisplay = FALSE;

    // Create the preplan table windows
    CDialog::Create(PrePlanTable::IDD);
    CRect olRect;
	ogBasicData.GetWindowPosition(olRect,ogCfgData.rmUserSetup.VPTB,ogCfgData.rmUserSetup.MONS);
    olRect.top += 40;
	BOOL blMinimized = FALSE;
	ogBasicData.GetDialogFromReg(olRect, COpssPmApp::PREPLANNING_TABLE_WINDOWPOS_REG_KEY,blMinimized);	

	//Multiple monitor setup
	ogBasicData.GetWindowPositionCorrect(olRect);

	MoveWindow(&olRect);
	bmIsViewOpen = FALSE;	
	pomTemplate = NULL;
	pomPrint = NULL;
	imTerminalSelected = CPrintTerminalSelection::BOTH_TERMINAL_SELECTED;

    // Display table content
	CRect rect;
    GetClientRect(&rect);
    rect.OffsetRect(0, m_nDialogBarHeight);
    rect.InflateRect(1, 1);     // hiding the CTable window border
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		omTable.SetTableData(this, rect.left, rect.right, rect.top, rect.bottom/2);
		omTableT3.SetTableData(this, rect.left, rect.right, rect.bottom/2 + 1 - m_nDialogBarHeight, rect.bottom + rect.bottom/2 + 1 - m_nDialogBarHeight - 35);
		omViewer.SetTerminal(Terminal2);
		omViewerT3.SetTerminal(Terminal3);
		omViewer.Attach(&omTable);
		omViewerT3.Attach(&omTableT3);

		bmIsT2Visible = TRUE;
		bmIsT3Visible = TRUE;
	}
	else
	{
		ogCCSDdx.UnRegister(&omViewerT3, NOTUSED);
		pomTable = &omTable;
		pomViewer = &omViewer;
        omTable.SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);
		omViewer.Attach(&omTable);
		
		bmIsT2Visible = FALSE;
		bmIsT3Visible = FALSE;
	}
	omContextItem = -1;
	
	UpdateView(omViewer.GetViewName());
	omDate = omViewer.StartTime().Format("%Y%m%d");
	SetCaptionText();
	if(blMinimized == TRUE)
	{
		ShowWindow(SW_MINIMIZE);
	}
}


PrePlanTable::PrePlanTable(BOOL bpIsFromSearch,BOOL bpIsDetailDisplay)
{
    //{{AFX_DATA_INIT(PrePlanTable)
    m_bCompleteTeam = FALSE;
    m_bAbsent = FALSE;
    //}}AFX_DATA_INIT

	pomTable = &omTable;   //Singapore
	pomViewer = &omViewer; //Singapore
	pomPrintPreviewView = NULL; //Singapore
	pomTeamTable = NULL;
	bmIsTeamDetailOpen = FALSE;
	bmIsFromSearch = bpIsFromSearch;
	bmIsDetailDisplay = bpIsDetailDisplay;

	pomTemplate = NULL;
	pomPrint = NULL;
	imTerminalSelected = CPrintTerminalSelection::BOTH_TERMINAL_SELECTED;
    // Create the preplan table windows
    CDialog::Create(PrePlanTable::IDD);
    CRect olRect;
	ogBasicData.GetWindowPosition(olRect,ogCfgData.rmUserSetup.VPTB,ogCfgData.rmUserSetup.MONS);	
	BOOL blMinimized = FALSE;
	ogBasicData.GetDialogFromReg(olRect, COpssPmApp::PREPLANNING_TABLE_WINDOWPOS_REG_KEY,blMinimized);	
	
	//Multiple monitor setup
	ogBasicData.GetWindowPositionCorrect(olRect);

    MoveWindow(&olRect);
	bmIsViewOpen = FALSE;

    // Display table content
	CRect rect;
    GetClientRect(&rect);
    rect.OffsetRect(0, m_nDialogBarHeight);
    rect.InflateRect(1, 1);     // hiding the CTable window border
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		omTable.SetTableData(this, rect.left, rect.right, rect.top, rect.bottom/2);
		omTableT3.SetTableData(this, rect.left, rect.right, rect.bottom/2 + 1 - m_nDialogBarHeight, rect.bottom + rect.bottom/2 + 1 - m_nDialogBarHeight - 35);
		omViewer.SetTerminal(Terminal2);
		omViewerT3.SetTerminal(Terminal3);
		omViewer.Attach(&omTable);
		omViewerT3.Attach(&omTableT3);

		bmIsT2Visible = TRUE;
		bmIsT3Visible = TRUE;
	}
	else
	{
		ogCCSDdx.UnRegister(&omViewerT3, NOTUSED);
		pomTable = &omTable;
		pomViewer = &omViewer;
        omTable.SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);
		omViewer.Attach(&omTable);
	}

	omContextItem = -1;

	if(ogBasicData.IsPaxServiceT2() == true)
	{
	    omViewer.ChangeView();
		omViewerT3.ChangeView();
	}
	else
	{
		omViewer.ChangeView();
	}
	SetCaptionText();
	omDate = omViewer.StartTime().Format("%Y%m%d");
	if(blMinimized == TRUE)
	{
		ShowWindow(SW_MINIMIZE);
	}
}

PrePlanTable::~PrePlanTable()
{
	TRACE("PrePlanTable::~PrePlanTable()\n");
	bgIsPreplanMode = FALSE;

	if(pomPrint != NULL)
	{
		delete pomPrint;
		pomPrint = NULL;	
	}

	WORD dWord;
	PRINT_PREVIEW_PAGEDATA* polPrintPreviewPageData = NULL;
	POSITION olPos = omMapPageNoToPreviewData.GetStartPosition();
	while(olPos != NULL)
	{
		omMapPageNoToPreviewData.GetNextAssoc(olPos,dWord,(void*&)polPrintPreviewPageData);
		if(polPrintPreviewPageData != NULL)
		{
			delete polPrintPreviewPageData;
		}
	}
	omMapPageNoToPreviewData.RemoveAll();
}


void PrePlanTable::SetCaptionText(void)
{
	CString olCaptionText;

	if (bmIsFromSearch == TRUE)
	{
		//olCaptionText = CString("Gefundene Mitarbeiter: ");
		olCaptionText = GetString(IDS_STRING61317);
	}
	else
	{
		//olCaptionText.Format("Preplanning %s-%s",omViewer.omStartTime.Format("%d/%H%M"),omViewer.omEndTime.Format("%H%M "));
		olCaptionText.Format(GetString(IDS_STRING61869));
		olCaptionText += omViewer.StartTime().Format("%d/%H%M");
		olCaptionText += omViewer.EndTime().Format("%H%M ");
	}
//	if (bgOnline)
//	{
//		//olCaptionText += "  Online";
//		olCaptionText += CString("  ") + GetString(IDS_STRING61301);
//	}
//	else
//	{
//		//olCaptionText += "  OFFLINE";
//		olCaptionText += CString("  ") + GetString(IDS_STRING61302);
//	}

	SetWindowText(olCaptionText);

}

CTime PrePlanTable::GetPrePlanTime(void)
{
	return omPrePlanDate;
}

void PrePlanTable::UpdateView(CString olViewName)
{
	if (olViewName.IsEmpty() == TRUE)
	{
		olViewName = omViewer.GetViewName();
	}
	if (olViewName.IsEmpty() == TRUE)
	{
		olViewName = ogCfgData.rmUserSetup.VPLV;
	}

	AfxGetApp()->DoWaitCursor(1);
	if(ogBasicData.IsPaxServiceT2() == true)
	{
        omViewer.ChangeViewTo(olViewName, m_bCompleteTeam,m_bAbsent);
		omViewerT3.ChangeViewTo(olViewName, m_bCompleteTeam,m_bAbsent);
		OnTableUpdateDataCount();
	}
	else
	{
		omViewer.ChangeViewTo(olViewName, m_bCompleteTeam,m_bAbsent);
		/*CWnd* polWndDataCount = GetDlgItem(IDC_DATACOUNT);
		if(polWndDataCount != NULL)
		{
			//polWndDataCount->ShowWindow(SW_HIDE);
			char olEmpCount[10];
			itoa(omViewer.omLines.GetSize(),olEmpCount,10);
			polWndDataCount->SetWindowText(olEmpCount);
		}*/
	}
	AfxGetApp()->DoWaitCursor(-1);
	omDate = omViewer.StartTime().Format("%Y%m%d");


	SetCaptionText();

	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
	//omTable.GetCTableListBox()->SetFocus();
}

void PrePlanTable::UpdateComboBox()
{
	CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_VIEW2);
	polCB->ResetContent();
	CStringArray olStrArr;
	omViewer.GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = omViewer.GetViewName();
	if (olViewName.IsEmpty() == TRUE)
	{
		olViewName = ogCfgData.rmUserSetup.VPLV;
	}

	ilIndex = polCB->FindString(-1,olViewName);
		
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}

}

void PrePlanTable::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(PrePlanTable)
    DDX_Control(pDX, IDC_UPDATE, m_UpdateButton);
    DDX_Control(pDX, IDC_INSERT, m_InsertButton);
    DDX_Check(pDX, IDC_COMPLETETEAM, m_bCompleteTeam);
    DDX_Check(pDX, IDC_ABSENT, m_bAbsent);
	DDX_Control(pDX, IDC_DATE, m_Date);
	DDX_Control(pDX, IDC_BUTTONT2,m_bTerminal2); //Singapore
	DDX_Control(pDX, IDC_BUTTONT3,m_bTerminal3); //Singapore
    //}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(PrePlanTable, CDialog)
    //{{AFX_MSG_MAP(PrePlanTable)
    ON_WM_DESTROY()
    ON_WM_ERASEBKGND()
    ON_WM_PAINT()
    ON_WM_SIZE()
    ON_WM_MOVE() //PRF 8712
    ON_WM_CLOSE()
    ON_BN_CLICKED(IDC_COMPLETETEAM, OnCompleteteam)
    ON_BN_CLICKED(IDC_ABSENT, OnAbsent)
    ON_BN_CLICKED(IDC_UPDATE, OnUpdateEmployee)
    ON_BN_CLICKED(IDC_PRINT, OnPrint)
    ON_BN_CLICKED(IDC_BUTTON_PRINT_PREVIEW,OnPrintPreview) //Singapore
    ON_MESSAGE(WM_TABLE_DRAGBEGIN, OnTableDragBegin)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblClk)
    ON_MESSAGE(WM_TABLE_RBUTTONDOWN, OnTableRButtonDown)
    ON_MESSAGE(WM_TABLE_SETTABLEWINDOW,OnTableSetTableWindow) //Singapore
    ON_MESSAGE(WM_TABLE_UPDATE_DATACOUNT,OnTableUpdateDataCount) //Singapore
    ON_MESSAGE(WM_PREPLANTEAMTABLE_EXIT, OnPrePlanTeamTableExit)
    ON_MESSAGE(WM_BEGIN_PRINTING, OnBeginPrinting) //Singapore
    ON_MESSAGE(WM_END_PRINTING, OnEndPrinting) //Singapore
    ON_MESSAGE(WM_PRINT_PREVIEW, PrintPreview) //Singapore
    ON_MESSAGE(WM_PREVIEW_PRINT, OnPreviewPrint) //Singapore
	ON_CBN_SELCHANGE(IDC_VIEW2, OnSelchangeView)
	ON_BN_CLICKED(IDC_ANSICHT, OnAnsicht)
	ON_BN_CLICKED(IDC_BUTTONT2,OnButtonT2) //Singapore
	ON_BN_CLICKED(IDC_BUTTONT3,OnButtonT3) //Singapore
	ON_COMMAND(10, OnMenuSwapShift)
	ON_COMMAND_RANGE(11, 40,OnMenuDelete)
	ON_COMMAND(100, OnMenuDebugInfo)
	ON_CBN_CLOSEUP(IDC_VIEW2, OnCloseupView2)
	ON_CBN_SELCHANGE(IDC_DATE, OnSelchangeDate)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// PrePlanTable message handlers

BOOL PrePlanTable::OnInitDialog()
{
    CDialog::OnInitDialog();

	CWnd *polWnd = GetDlgItem(IDC_ANSICHT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61840));
	}
	polWnd = GetDlgItem(IDC_COMPLETETEAM); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33011));
	}
	polWnd = GetDlgItem(IDC_ABSENT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33012));
	}
	polWnd = GetDlgItem(IDC_INSERT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33013));
	}
	polWnd = GetDlgItem(IDC_UPDATE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61352));
	}
	polWnd = GetDlgItem(IDC_PRINT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61844));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33014));
	}

	//Singapore
	if(ogBasicData.IsPaxServiceT2() == false)
	{
		CWnd* polWnd = GetDlgItem(IDC_STATIC_T2T3);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}
		polWnd = GetDlgItem(IDC_DATACOUNT);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}
		polWnd = GetDlgItem(IDC_BUTTONT2);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}
		polWnd = GetDlgItem(IDC_BUTTONT3);
		if(polWnd != NULL)
		{
			polWnd->ShowWindow(SW_HIDE);
		}
	}
	else
	{
		m_bTerminal2.SetColors(LIME, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		m_bTerminal3.SetColors(LIME, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}

    // calculate the window height
    CRect rect;
    GetClientRect(&rect);
    m_nDialogBarHeight = rect.bottom - rect.top;
    rect.left = 0;
    rect.top = 56;
    rect.right = ::GetSystemMetrics(SM_CXSCREEN) - 1;
    rect.bottom = ::GetSystemMetrics(SM_CYSCREEN) - 1;

	if (bmIsFromSearch == TRUE)
	{
		m_UpdateButton.EnableWindow(SW_HIDE);
		m_InsertButton.EnableWindow(SW_HIDE);
		GetDlgItem(IDC_COMPLETETEAM)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_ABSENT)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_PRINT)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_VIEW2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_ANSICHT)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_UPDATE)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_INSERT)->ShowWindow(SW_HIDE);
		//GetDlgItem(IDCANCEL)->SetWindowText("Schlie�en");
		GetDlgItem(IDCANCEL)->SetWindowText(GetString(IDS_STRING61322));
		
	}
	else
	{

		UpdateComboBox();

		// Register DDX call back function
		TRACE("PrePlanTable: DDX Registration\n");
		ogCCSDdx.Register(this, REDISPLAY_ALL, CString("PREPLANT"),CString("Redisplay All"), PrePlanTableCf);
		ogCCSDdx.Register(this, GLOBAL_DATE_UPDATE, CString("PREPLANT"),CString("Global Date Change"), PrePlanTableCf);

		if(ogBasicData.IsPaxServiceT2() == true)
		{
			omViewer.SelectView(ogCfgData.rmUserSetup.VPLV);
			omViewerT3.SelectView(ogCfgData.rmUserSetup.VPLV);
		}
		else
		{
			omViewer.SelectView(ogCfgData.rmUserSetup.VPLV);
		}
	}


	CStringArray olTimeframeList;
	ogBasicData.GetTimeframeList(olTimeframeList);
	int ilNumDays = olTimeframeList.GetSize();
	for(int ilDay = 0; ilDay < ilNumDays; ilDay++)
	{
		m_Date.AddString(olTimeframeList[ilDay]);
	}
	if(ilNumDays > 0)
	{
		m_Date.SetCurSel(ogBasicData.GetDisplayDateOffset());
		SendUpdatePrePlanDate(ogBasicData.GetDisplayDate());
	}
	SetCaptionText();
	
	//UpdateView(omViewer.GetViewName());
    return TRUE;  // return TRUE  unless you set the focus to a control
}

void PrePlanTable::OnDestroy() 
{
	if (bgModal == TRUE)
		return;	
	ogBasicData.WriteDialogToReg(this,COpssPmApp::PREPLANNING_TABLE_WINDOWPOS_REG_KEY,omWindowRect,this->IsIconic());
	// Unregister DDX call back function
	TRACE("PrePlanTable: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogCCSDdx.UnRegister(this, NOTUSED);
	CDialog::OnDestroy();
}

void PrePlanTable::OnCancel()
{
	/*if (bmIsFromSearch == FALSE)
	{
		AfxGetMainWnd()->SendMessage(WM_PREPLANTABLE_EXIT);
		DestroyWindow();
	}
	else
	{
		AfxGetMainWnd()->SendMessage(WM_PREPLANSEARCHTABLE_EXIT);
		DestroyWindow();
	}*/
	ClosePrePlan();
}

void PrePlanTable::ClosePrePlan()
{
	if (bmIsFromSearch == FALSE)
	{
		pogButtonList->SendMessage(WM_PREPLANTABLE_EXIT);
		DestroyWindow();
	}
	else
	{
		AfxGetMainWnd()->SendMessage(WM_PREPLANSEARCHTABLE_EXIT);//Maybe AfxGetMainWnd() needs to be changed with pogButtonList, too.
		DestroyWindow();
	}
}

BOOL PrePlanTable::OnEraseBkgnd(CDC* pDC) 
{
	return CDialog::OnEraseBkgnd(pDC);
}

void PrePlanTable::OnPaint()
{
    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    CRect rect;
    GetUpdateRect(&rect);
	if(ogBasicData.IsPaxServiceT2() == true)
	{
        if (omTable.m_hWnd != NULL)
        {
            MapWindowPoints(&omTable, &rect);
            omTable.InvalidateRect(&rect, FALSE);
		}
		if (omTableT3.m_hWnd != NULL)
		{
			MapWindowPoints(&omTableT3, &rect);
			omTableT3.InvalidateRect(&rect, FALSE);
		}
	}
	else
	{
		if (omTable.m_hWnd != NULL)
		{
			MapWindowPoints(&omTable, &rect);
			omTable.InvalidateRect(&rect, FALSE);
		}
    }

    // Do not call CDialog::OnPaint() for painting messages
}

void PrePlanTable::OnSize(UINT nType, int cx, int cy)
{
    CDialog::OnSize(nType, cx, cy);
    
    // TODO: Add your message handler code here
    if (nType != SIZE_MINIMIZED)
	{
		if(ogBasicData.IsPaxServiceT2() == true)
		{
			if(bmIsT2Visible == TRUE && bmIsT3Visible == TRUE)
			{
				omTable.SetPosition(-1, cx, m_nDialogBarHeight-1, cy/2+1);
				omTableT3.SetPosition(-1, cx+1, cy/2+2, cy+1);
			}
			else if(bmIsT2Visible == TRUE && bmIsT3Visible == FALSE)
			{
				omTable.SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy);			
			}
			else if(bmIsT2Visible == FALSE && bmIsT3Visible == TRUE)
			{
				omTableT3.SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy+1);
			}
		}
		else
		{
             omTable.SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy+1);
        }
		GetWindowRect(&omWindowRect); //PRF 8712
	}
}

void PrePlanTable::OnAnsicht()
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
	//omTable.GetCTableListBox()->SetFocus();

	if (bmIsTeamDetailOpen == TRUE)
	{
		MessageBeep((UINT)-1);
		return;
	}

	PrePlanTablePropertySheet dialog(this, &omViewer);
	bmIsViewOpen = TRUE;

	if (dialog.DoModal() != IDCANCEL)
		UpdateView(omViewer.GetViewName());
	bmIsViewOpen = FALSE;
	UpdateComboBox();
}

void PrePlanTable::OnSelchangeView() 
{
	TRACE("OnSelchangeView");
	if (bmIsTeamDetailOpen == TRUE)
	{
		MessageBeep((UINT)-1);
		return;
	}

    char clText[64];
    CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_VIEW2);
    polCB->GetLBText(polCB->GetCurSel(), clText);
    TRACE("PrePlanTable::OnComboBox() [%s]", clText);
	if(ogBasicData.IsPaxServiceT2() == true)
	{
	    omViewer.SelectView(clText);
		omViewerT3.SelectView(clText);
	}
	else
	{
		omViewer.SelectView(clText);
	}
	UpdateView(clText);
}

void PrePlanTable::OnCloseupView2() 
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box when the combo-box is closed.
    //omTable.GetCTableListBox()->SetFocus();
}

void PrePlanTable::OnCompleteteam()
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
	//omTable.GetCTableListBox()->SetFocus();

	if (bmIsTeamDetailOpen == TRUE)
	{
		CWnd *prlBtn = GetDlgItem(IDC_COMPLETETEAM);
		if (prlBtn != NULL)
		{
			((CButton *)prlBtn)->SetCheck(m_bCompleteTeam);
		}
		MessageBeep((UINT)-1);
		return;
	}

	UpdateData(TRUE);
    UpdateView(omViewer.GetViewName());
}

void PrePlanTable::OnAbsent()
{
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
	//omTable.GetCTableListBox()->SetFocus();

	UpdateData(TRUE);

    UpdateView(omViewer.GetViewName());
}


void PrePlanTable::OnUpdateEmployee()
{
	int ilLc;
	int ilLineno = pomTable->GetCurrentLine();
	if (!(0 <= ilLineno && ilLineno <= pomViewer->Lines()-1))
		return;	// cannot update employee on a blank line

	if(m_bCompleteTeam == TRUE) 
	{
		PREPLANT_LINEDATA *prlLine = (PREPLANT_LINEDATA *)pomTable->GetTextLineData( pomTable->GetCurrentLine() );
		if (prlLine != NULL)
		{
			CString olTeamID = prlLine->Line.Tmid;
			int ilItemHeight = pomTable->imItemHeight;

			if(bmIsTeamDetailOpen && pomTeamTable != NULL)
			{
				pomTeamTable->SelectNewTeam(olTeamID, &prlLine->TeamMember, ilItemHeight);
			}
			else
			{
				pomTeamTable = new PrePlanTeamTable((CWnd *)this, olTeamID, &prlLine->TeamMember, ilItemHeight);
				bmIsTeamDetailOpen = TRUE;
			}
		}
	}
	else
	{
		PREPLANT_LINEDATA *prlLine = (PREPLANT_LINEDATA *)pomTable->GetTextLineData( pomTable->GetCurrentLine() );
		if (prlLine != NULL)
		{
			CEmpDialog olEmpDialog;
			SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prlLine->Line.ShiftUrno);
			if(prlShift != NULL)
			{
				SHIFTDATA olOldShift = *prlShift;
				olEmpDialog.omShift = *prlShift;

				if (olEmpDialog.DoModal() != IDOK)
					return;

				// if while the EmpDialog was open the shift was changed or deleted
				// by another application then do not update the shift
				if((prlShift = ogShiftData.GetShiftByUrno(prlLine->Line.ShiftUrno)) == NULL ||
					*prlShift != olOldShift)
				{
					MessageBox(GetString(IDS_SHIFTHASCHANGED),ogEmpData.GetEmpName(olOldShift.Stfu),MB_ICONERROR);
					return;
				}

				if (strcmp(olOldShift.Fctc, olEmpDialog.m_Rank) || 
					olEmpDialog.omShift.Avfa != olOldShift.Avfa || 
 					olEmpDialog.omShift.Avta != olOldShift.Avta)
				{
					if(!CheckForDeviationOrAbsence(&(olEmpDialog.omShift)))
						return;
				}

				*prlShift = olEmpDialog.omShift;

				int ilRc = TRUE;
				if (ogShiftData.IsAbsent(prlShift))
				{
					// Employee is absence, delete all Pooljobs
					for (int ilLc = prlLine->Line.Assignment.GetSize()-1; (ilLc >= 0) && (ilRc == TRUE); ilLc--)
					{
						if (ilLc < prlLine->Line.Assignment.GetSize())
						{
							PREPLANT_ASSIGNMENT *prlAssignment = &prlLine->Line.Assignment[ilLc];
							if (prlAssignment != NULL)
							{
								ilRc = pomViewer->DeleteAssignmentJob(prlAssignment->Urno);
							}
						}
					}
				}
				else
				{
					if((olOldShift.Avfa < prlShift->Avfa) || (olOldShift.Avta > prlShift->Avta))
					{
						int ilResult;

						CString olPrompt = GetString(IDS_STRING61496);
						if ((ilResult = ogBasicData.MessageBox(this,olPrompt, NULL, MB_ICONEXCLAMATION | MB_YESNO)) != IDYES)
							return ;

						// Actual shift time is shortened, change dependent pool jobs
						for (ilLc = prlLine->Line.Assignment.GetSize()-1;(ilLc >= 0)  && (ilRc == TRUE); ilLc--)
						{
							PREPLANT_ASSIGNMENT *prlAssignment = &prlLine->Line.Assignment[ilLc];
							if (prlAssignment != NULL)
							{
								if ((prlAssignment->Acfr > prlShift->Avta) || (prlAssignment->Acto <  prlShift->Avfa))
								{ 
									// delete pool job because it is fully outside of new actual shift time
									ilRc = pomViewer->DeleteAssignmentJob(prlAssignment->Urno);
								}
								else
								{
// Commented out because pool job times are changed by polhdl in response to shift changes
//									if (prlAssignment->Acfr < prlShift->Avfa)
//									{
//										ilRc = ogDataSet.ChangeTimeOfPoolJob(prlAssignment->Urno,prlShift->Avfa,
//											prlShift->Avta > prlAssignment->Acto ? prlAssignment->Acto : prlShift->Avta);
//									}
//									else
//									{
//										if (prlAssignment->Acto > prlShift->Avta)
//										{
//											ilRc = ogDataSet.ChangeTimeOfPoolJob(prlAssignment->Urno,
//												prlShift->Avfa < prlAssignment->Acfr ? prlAssignment->Acfr : prlShift->Avfa,
//												prlShift->Avta);
//										}
//									}
								}
							}
						}
					}
				}
				if (ilRc == TRUE)
				{
					DRGDATA *prlDrg = NULL;
					if((prlDrg = ogDrgData.GetDrgByShift(prlShift)) != NULL)
					{
						if(strcmp(olEmpDialog.m_WorkGroup, prlDrg->Wgpc) || strcmp(olEmpDialog.m_Rank, prlDrg->Fctc))
						{
							if((ilRc = ogDrgData.UpdateDrg(prlDrg->Urno, prlShift->Egrp, olEmpDialog.m_Rank)) != true)
								MessageBox(GetString(IDS_STRING61497) + ogDrgData.LastError());
						}
						strcpy(prlShift->Fctc, olEmpDialog.m_Rank); 
					}
					else if(strcmp(olEmpDialog.m_WorkGroup, olOldShift.Egrp))
					{
						if(ogDrgData.InsertDrg(prlShift->Stfu, prlShift->Sday, olEmpDialog.m_WorkGroup, olEmpDialog.m_Rank, prlShift->Drrn) == NULL)
						{
							MessageBox(GetString(IDS_STRING61497) + ogDrgData.LastError());
							ilRc = FALSE;
						}
						strcpy(prlShift->Fctc, olEmpDialog.m_Rank); 
					}
					else
					{
						strcpy(prlShift->Fctc, olEmpDialog.m_Rank);
					}

					if(ilRc && ((ilRc = ogShiftData.UpdateShift(prlShift,&olOldShift)) != true))
						MessageBox(GetString(IDS_STRING61497) + ogShiftData.LastError());
				}
				if(ilRc == FALSE)
				{
					// error detected, copy saved old data back
					*prlShift = olOldShift;
				}
				//ogCCSDdx.DataChanged((void *)this,SHIFT_CHANGE,(void *)prlShift);
			}
		}
	}
}

void PrePlanTable::OnPrint()
{  
	// Id 24-Sep-96
	// This will set the keyboard focus back to the list-box after this button was clicked.
	//omTable.GetCTableListBox()->SetFocus();
	if (PreplanPreparePrintData(omDate) == TRUE)
	{
		if(ogBasicData.IsPaxServiceT2() == true && SelectPrintTerminal() == FALSE)
		{
			return;
		}
		PrintData();
		/*
		CPrintCtrl *prn = new CPrintCtrl(FALSE);
		prn->PreplanPrint(&ogShiftData, (char *)(const char *)omDate);
		delete prn;
		*/
	}
}

BOOL PrePlanTable::DestroyWindow() 
{
	if ((bmIsViewOpen) || (bgModal == TRUE))
	{
		MessageBeep((UINT)-1);
		return FALSE;    // don't destroy window while view property sheet is still open
	}

	if (::IsWindow(pomTeamTable->GetSafeHwnd()))
	{
		pomTeamTable->DestroyWindow();
	}

	BOOL blRc = CDialog::DestroyWindow();
	delete this;
	return blRc;
}

LONG PrePlanTable::OnTableLButtonDblClk(UINT wParam, LONG lParam)
{
	OnUpdateEmployee();
	return 0L;
}

//Singapore
void PrePlanTable::OnTableSetTableWindow(WPARAM wParam, LPARAM lParam)
{
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		CTable* polTable = (CTable*)lParam;	
		if(&omTable == polTable)
		{
			omActiveTerminal = Terminal2;
			pomTable = polTable;
			pomViewer = &omViewer;
			omTableT3.GetCTableListBox()->SetSel(-1,FALSE);
		}
		else if(&omTableT3 == polTable)
		{
			pomTable = polTable;
			omActiveTerminal = Terminal3;
			pomViewer = &omViewerT3;
			omTable.GetCTableListBox()->SetSel(-1,FALSE);
		}
	}
}
//Singapore
LONG PrePlanTable::OnTableRButtonDown(UINT ipItem, LONG lpLParam) 
{
	CMenu menu;
	CPoint olPoint(LOWORD(lpLParam),HIWORD(lpLParam));
	
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		if(omActiveTerminal == Terminal3 && bmIsT2Visible == TRUE)
		{
			CRect olRect;
			omTable.GetClientRect(&olRect);
			olPoint.y += olRect.Height();
		}
	}
	
	omContextItem = -1;
	omContextShiftUrnos.RemoveAll();
	omContextAssignUrnos.RemoveAll();

	int ilMaxContextItems = 0;
	CListBox *polListBox = pomTable->GetCTableListBox();	
	if (polListBox != NULL)
	{
		ilMaxContextItems = polListBox->GetSelCount();
	}

	if (ilMaxContextItems > 1)
	{
		int *pilContextIndices = new int [ilMaxContextItems];
		polListBox->GetSelItems(ilMaxContextItems, pilContextIndices);
		for (int i = 0; i < ilMaxContextItems; i++)
		{
			PREPLANT_LINEDATA *prlLine = (PREPLANT_LINEDATA *)pomTable->GetTextLineData(pilContextIndices[i]);
			if (prlLine)
				omContextShiftUrnos.Add(prlLine->Line.ShiftUrno);
		}
		
		delete [] pilContextIndices;

		menu.CreatePopupMenu();
		//menu.AppendMenu(MF_STRING,11, "Alle Zuordnungen l�schen");	
		menu.AppendMenu(MF_STRING,11, GetString(IDS_STRING61379));	
		ClientToScreen( &olPoint);
		menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, olPoint.x, olPoint.y + 55, this, NULL);
		PREPLANT_LINEDATA *prlLine = (PREPLANT_LINEDATA *)pomTable->GetTextLineData(ipItem);
		if (prlLine)
			omContextItem = prlLine->Line.ShiftUrno;
	}
	else
	{

		PREPLANT_LINEDATA *prlLine = (PREPLANT_LINEDATA *)pomTable->GetTextLineData(ipItem);
		if (prlLine != NULL)
		{
			int ilLc;

			menu.CreatePopupMenu();
			for (ilLc = menu.GetMenuItemCount(); --ilLc >= 0;)
				menu.RemoveMenu(ilLc, MF_BYPOSITION);

			if(ogBasicData.bmSwapShiftEnabled)
				menu.AppendMenu(MF_STRING, 10, GetString(IDS_SWAPSHIFTSDLG_TITLE)); // swap shift

			CDWordArray olJobUrnos;
			for ( ilLc = 0; ilLc < prlLine->Line.Assignment.GetSize(); ilLc++)
			{
				//CString olMenuText = "L�schen: Einsatz " + 
				CString olMenuText = GetString(IDS_STRING61380) + 
					prlLine->Line.Assignment[ilLc].Alid + " " +
					prlLine->Line.Assignment[ilLc].Acfr.Format("%H%M") + " - " +
					prlLine->Line.Assignment[ilLc].Acto.Format("%H%M");
					menu.AppendMenu(MF_STRING,11 + ilLc, olMenuText);
				omContextAssignUrnos.Add(prlLine->Line.Assignment[ilLc].Urno);
				olJobUrnos.Add(prlLine->Line.Assignment[ilLc].Urno);
			}

			if(ogSprData.bmNoChangesAllowedAfterClockout && ogSprData.ClockedOut(olJobUrnos))
				ogBasicData.DisableAllMenuItems(menu);

			if(ogBasicData.DisplayDebugInfo())
			{
				menu.AppendMenu(MF_STRING, 100, "Debug Info");
			}

			if ( prlLine->Line.Assignment.GetSize() > 0 || ogBasicData.DisplayDebugInfo())
			{
				ClientToScreen( &olPoint);
				menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, olPoint.x, olPoint.y + 55, this, NULL);
				omContextItem = prlLine->Line.ShiftUrno;
			}
		}
	}
	return 0L;
}

void PrePlanTable::OnMenuDelete(UINT nID)
{
	if (omContextShiftUrnos.GetSize())
	{
		ogTransactionManager.Start();

		for ( int ilLc = 0; ilLc < omContextShiftUrnos.GetSize(); ilLc++)
		{
			int ilItem = -1;
			if (pomViewer->FindShift(omContextShiftUrnos[ilLc],ilItem))
				DeleteAllAssignments(ilItem);
		}

		omContextShiftUrnos.RemoveAll();

		ogTransactionManager.End();
	}
	else
	{
		DeleteAssignment(nID - 11);
	}
}


void PrePlanTable::OnOK() 
{
}


void PrePlanTable::DeleteAssignment(int ipIndex)
{
	int ilItem = -1;
	if (pomViewer->FindShift(omContextItem,ilItem))
	{
		if (!m_bCompleteTeam)
		{
			if (ipIndex >= 0 && ipIndex < omContextAssignUrnos.GetSize())
				pomViewer->DeleteAssignmentJob(omContextAssignUrnos[ipIndex]);
		}
		else	// deleting same assignment for the whole team
		{
			PREPLANT_LINEDATA *prlLine = (PREPLANT_LINEDATA *)pomTable->GetTextLineData(ilItem);
			if (prlLine && ipIndex >= 0 && ipIndex < omContextAssignUrnos.GetSize() && prlLine->Line.Assignment[ipIndex].Urno == omContextAssignUrnos[ipIndex])
			{
				CString olAlid = prlLine->Line.Assignment[ipIndex].Alid;
				CTime   olAcfr = prlLine->Line.Assignment[ipIndex].Acfr;
				CTime   olActo = prlLine->Line.Assignment[ipIndex].Acto;

				for (int i = 0; i < prlLine->TeamMember.GetSize(); i++)
				{
					PREPLANT_SHIFTDATA *prlShift = &prlLine->TeamMember[i];
					for (int ilLc = prlShift->Assignment.GetSize()-1; ilLc >= 0; ilLc--)
					{
						PREPLANT_ASSIGNMENT *prlAssignment = &prlShift->Assignment[ilLc];
						if ((prlAssignment->Alid == olAlid) && 
							(prlAssignment->Acfr == olAcfr) &&
							 (prlAssignment->Acto == olActo))
						{
							pomViewer->DeleteAssignmentJob(prlAssignment->Urno);
							prlShift->Assignment.DeleteAt(ilLc);
						}
						pomViewer->RecalcAssignmentStatus(prlShift);
					}
				}
				pomViewer->RecalcTeamAssignmentStatus(ilItem);
				CString olRank = prlLine->Line.Rank;
				 BOOL blIsFlightManager = ogBasicData.IsManager(olRank);
				if (blIsFlightManager)
				{
					pomTable->SetTextLineFont(ilItem, &ogCourier_Bold_10);
				}

				switch (prlLine->Line.AssignmentStatus)
				{
				case PARTIAL_ASSIGNED:
					  pomTable->SetTextLineColor(ilItem, (blIsFlightManager? BLUE: BLACK), SILVER);
					pomTable->SetTextLineDragEnable(ilItem, TRUE);
					break;
				case FULLY_ASSIGNED:
					  pomTable->SetTextLineColor(ilItem, (blIsFlightManager? BLUE: WHITE), GRAY);
					pomTable->SetTextLineDragEnable(ilItem, FALSE);
					break;
				case UNASSIGNED:
					pomTable->SetTextLineColor(ilItem, (blIsFlightManager? BLUE: BLACK), WHITE);
					pomTable->SetTextLineDragEnable(ilItem, TRUE);
					break;
				}

				pomTable->ChangeTextLine(ilItem, pomViewer->Format(prlLine), prlLine);
			}
		}
	}
	omContextItem = -1;
}

void PrePlanTable::DeleteAllAssignments(int ipLineNo)
{
	if (!m_bCompleteTeam)
	{
		pomViewer->DeleteAllAssignments(ipLineNo);
	}
	else	// deleting all assignments for the whole team
	{
		PREPLANT_LINEDATA *prlLine = (PREPLANT_LINEDATA *)pomTable->GetTextLineData(ipLineNo);
		for (int i = 0; i < prlLine->TeamMember.GetSize(); i++)
		{
			PREPLANT_SHIFTDATA *prlShift = &prlLine->TeamMember[i];

			int ilNumJobs = prlShift->Assignment.GetSize();
			for(int ilJ = (ilNumJobs-1); ilJ >= 0; ilJ--)
			{
				PREPLANT_ASSIGNMENT *prlAssignment = &prlShift->Assignment[ilJ];
				pomViewer->DeleteAssignmentJob(prlAssignment->Urno);
			}
		}
	}
	omContextItem = -1;
}


LONG PrePlanTable::OnPrePlanTeamTableExit(UINT /*wParam*/, LONG /*lParam*/)
{
    if (pomTeamTable != NULL)  // this window is open?
    {
		bmIsTeamDetailOpen = FALSE;
    }

    return 0L;
}

BOOL PrePlanTable::PreplanPreparePrintData(CString popDutyDay)
{
	return PreplanSelectTeams(popDutyDay);
}


BOOL PrePlanTable::PreplanSelectTeams(CString popDutyDay)
{
	BOOL blRc = true;

	if(ogBasicData.IsPaxServiceT2() == true)
	{

		CStringList olTeamList;
		CStringList olTeamListT3;
		omViewer.GetTeams(olTeamList);
		omViewerT3.GetTeams(olTeamListT3);
		POSITION olPosition = NULL;
		olPosition = olTeamListT3.GetHeadPosition();
		CString olTeam;
		while(olPosition != NULL)
		{
			olTeam = olTeamListT3.GetNext(olPosition);
			if(olTeamList.Find(olTeam) == NULL)
			{
				olTeamList.AddTail(olTeam);
			}
		}
		olPosition = olTeamList.GetHeadPosition();
	    omTeamArray.RemoveAll();
		while(olPosition != NULL)
		{
			olTeam = olTeamList.GetNext(olPosition);
			omTeamArray.Add(olTeam);
		}
	}
	else
	{
		omTeamArray.RemoveAll();
		pomViewer->GetTeams(omTeamArray);
	}

	if(omTeamArray.GetSize() >= 1)
	{
		omTeamDlg.m_TeamArray = &omTeamArray;
		if( omTeamDlg.DoModal() == IDCANCEL )
		{
			blRc = FALSE;
		}
	}

	return blRc;
}


BOOL PrePlanTable::PrintPreplanLine(CCSPrint *pomPrint,SHIFTDATA *popShift,CString opTeam,int ipIndex,BOOL bpIsLastLine,CString opDutyDate)
{
	int ilLc;
	int ilBottomFrame;
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	BOOL blSpecialJobPrinted = FALSE;

	ilBottomFrame = PRINT_FRAMETHIN;

	if (bpIsLastLine == TRUE)
	{
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.Length     = 2590;
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= PRINT_NOFRAME;
		rlElement.pFont       = &pomPrint->omMediumFont_Bold;
		rlElement.Text.Empty();
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

		pomPrint->PrintLine(rlPrintLine);
		rlPrintLine.DeleteAll();

	}
	else
	{

		if (pomPrint->imLineNo == 0)
		{

			rlElement.Alignment  = PRINT_LEFT;
			rlElement.Length     = 400;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			rlElement.FrameTop   = PRINT_NOFRAME;
			rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
			rlElement.pFont       = &pomPrint->omMediumFont_Bold;
//			rlElement.Text       = GetString(IDS_STRING33146) + opTeam;
			rlElement.Text       = CString("");
			
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Alignment  = PRINT_CENTER;
			rlElement.Length     = 120;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameRight = PRINT_NOFRAME;
			rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
			rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;
			rlElement.Text       = GetString(IDS_STRING61613);
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);


			rlElement.Alignment  = PRINT_CENTER;
			rlElement.Length     = 100;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
			rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;
			rlElement.Text       = GetString(IDS_STRING61362);
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Alignment  = PRINT_CENTER;
			rlElement.Length     = 100;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
			rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;
			rlElement.Text       = GetString(IDS_STRING61363);
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Alignment  = PRINT_CENTER;
			rlElement.Length     = 190;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameRight = PRINT_NOFRAME;
			rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
			rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;
			rlElement.Text       = GetString(IDS_STRING33147);
//			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);


			rlElement.Alignment  = PRINT_CENTER;
			rlElement.Length     = 80;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
			rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;
			rlElement.Text       = GetString(IDS_STRING61362);
//			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Alignment  = PRINT_CENTER;
			rlElement.Length     = 80;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
			rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;
			rlElement.Text       = GetString(IDS_STRING61363);
//			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			
			for(ilLc = 0; ilLc < 3; ilLc++)
			{
				rlElement.Alignment  = PRINT_CENTER;
				rlElement.Length     = 190;
				rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
				rlElement.Text       = GetString(IDS_STRING61610);
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

				rlElement.Length     = 80;
				rlElement.FrameLeft  = PRINT_NOFRAME;
				rlElement.Text       = GetString(IDS_STRING61362);
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

				rlElement.FrameLeft  = PRINT_NOFRAME;
				rlElement.Text       = GetString(IDS_STRING61363);
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			}

			rlElement.Length     = 470;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameRight = PRINT_FRAMEMEDIUM;
			rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
			rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;
			rlElement.Text       = GetString(IDS_STRING33148);

			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			if( pomPrint->imLineNo >= (pomPrint->imMaxLines-1) )
			{
				if ( pomPrint->imPageNo > 0)
				{
					pomPrint->PrintFooter(GetString(IDS_STRING33145),GetString(IDS_STRING33149));
					pomPrint->imLineNo = pomPrint->imMaxLines + 1;
					pomPrint->pomCdc->EndPage();
					pomPrint->imLineTop = pomPrint->imFirstLine;
				}
				PrintPreplanHeader(pomPrint,opTeam,opDutyDate);
			}

			pomPrint->PrintLine(rlPrintLine);
			rlPrintLine.DeleteAll();
			rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		}
//		else
		{
			EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(popShift->Peno);
			CString olName;

			SHIFTDATA rlShift;
			memcpy(&rlShift,popShift,sizeof(rlShift));
			if (ogShiftData.IsAbsent(&rlShift))
			{
				rlShift.Avfa = TIMENULL;
				rlShift.Avta = TIMENULL;
			}


			if (prlEmp != NULL)
			{
				olName = CString(rlShift.Efct) + ",  " + ogEmpData.GetEmpName(prlEmp);
			}

			rlElement.Alignment  = PRINT_LEFT;
			rlElement.Length     = 400;
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
			rlElement.FrameBottom= ilBottomFrame;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;
			rlElement.Text       = olName;
			
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Alignment  = PRINT_CENTER;
			rlElement.Length     = 120;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameRight = PRINT_NOFRAME;
			rlElement.Text       = CString(rlShift.Sfca);

			BOOL blIsTimeChanged;
			SHIFTTYPEDATA *prlShiftType = ogShiftTypes.GetShiftTypeById(rlShift.Sfcs);
			if (prlShiftType != NULL)
			{
				blIsTimeChanged = (rlShift.Avfa.Format("%H%M") != prlShiftType->Esbg ||
									rlShift.Avta.Format("%H%M") != prlShiftType->Lsen);
			}
			if (blIsTimeChanged == TRUE)
			{
				rlElement.Text += "'";
			}
			
			
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);


			rlElement.Length     = 100;
			rlElement.FrameLeft  = PRINT_FRAMETHIN;
			rlElement.Text       = rlShift.Avfa.Format("%H:%M");
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 100;
			rlElement.Text       = rlShift.Avta.Format("%H:%M");
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
			CCSPtrArray <JOBDATA> olJobs;
			ogJobData.GetJobsByShur(olJobs,popShift->Urno,FALSE);
/************************
			CString olVorher;
			for (ilLc = olJobs.GetSize()-1; ilLc >= 0; ilLc--)
			{
				JOBDATA *prlJob = &olJobs[ilLc];
				olVorher += prlJob->Alid + CString(" ") + prlJob->Text + " ";
			}
  *****************/
			olJobs.Sort(CompareJobStartTime);
			/**************************
			CString olNachher;
			for (ilLc = olJobs.GetSize()-1; ilLc >= 0; ilLc--)
			{
				JOBDATA *prlJob = &olJobs[ilLc];
				olNachher += prlJob->Alid + CString(" ") + prlJob->Text + " ";
			}
			*********************************/
			for (ilLc = olJobs.GetSize()-1; ilLc >= 0; ilLc--)
			{

				JOBDATA *prlJob = &olJobs[ilLc];
				
				BOOL blIsJobPool = CString(prlJob->Jtco) == JOBPOOL;
				BOOL blIsJobFlightSpecial = ((CString(prlJob->Jtco) == JOBFLIGHT) && 
					(prlJob->DisplayInPrePlanTable == TRUE));
				BOOL blIsJobCciSpecial = ((CString(prlJob->Jtco) == JOBCCI) && 
					(prlJob->DisplayInPrePlanTable == TRUE));

				if (!(blIsJobPool || blIsJobFlightSpecial || blIsJobCciSpecial))
				if (!blIsJobPool)
				{
					olJobs.RemoveAt(ilLc);
				}
				else
				{
					if ((blIsJobFlightSpecial || blIsJobCciSpecial) && 
						blSpecialJobPrinted == TRUE)
					{
						olJobs.RemoveAt(ilLc);
					}
					else
					{
						if (blIsJobFlightSpecial || blIsJobCciSpecial)
						{
							blSpecialJobPrinted = TRUE;
						}
					}
				}
		
			}
			olJobs.Sort(CompareJobStartTime);
			int ilCount = olJobs.GetSize();
			int ilJobNo = 0;
//			for(int ilJobIndex = 0; (ilJobIndex < ilCount) || (ilJobNo % 4 != 0) || (ilJobIndex==0); )
			for(int ilJobIndex = 0; (ilJobIndex < ilCount) || (ilJobNo % 3 != 0) || (ilJobIndex==0); )
			{
				CString olText;
				CString olFrom;
				CString olTo;

				if (ilJobNo > 3)
				{
					ilJobNo = 0;
					// add last column (sign)
					rlElement.Alignment  = PRINT_LEFT;
					rlElement.Length     = 470;
					rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
					rlElement.FrameRight = PRINT_FRAMEMEDIUM;
					rlElement.pFont       = &pomPrint->omSmallFont_Bold;
					rlElement.Text.Empty();

					rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

					pomPrint->PrintLine(rlPrintLine);
					rlPrintLine.DeleteAll();

					rlElement.Alignment  = PRINT_CENTER;
					rlElement.FrameLeft  = PRINT_NOFRAME;
					rlElement.FrameRight = PRINT_NOFRAME;
					rlElement.FrameTop   = PRINT_NOFRAME;
					rlElement.FrameBottom= PRINT_NOFRAME;
					rlElement.pFont       = &pomPrint->omSmallFont_Bold;
					rlElement.Length     = 720;
					rlElement.Text       = "";
					rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

					rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
					rlElement.FrameBottom= ilBottomFrame;
				
				}

				rlElement.FrameTop   = PRINT_FRAMEMEDIUM;

				if (ilJobIndex < ilCount)
				{
					if (strcmp(olJobs[ilJobIndex].Jtco,JOBPOOL) != 0)
					{
						if (ilJobNo == 0)
						{
							if(strcmp(olJobs[ilJobIndex].Jtco,JOBFLIGHT) == 0)
							{
								olText = olJobs[ilJobIndex].Text;
							}
							else
							{
								if(!strcmp(olJobs[ilJobIndex].Jtco,JOBCCI))
								{
									olText = olJobs[ilJobIndex].Alid;
								}
								else
								{
								olText = (olJobs[ilJobIndex].Text[0] != '\0')?
									omViewer.GetGateAreaOrCCIAreaName(olJobs[ilJobIndex].Text):
									olJobs[ilJobIndex].Alid;
								}
							}
							olFrom = olJobs[ilJobIndex].Acfr.Format("%H:%M");
							olTo   = olJobs[ilJobIndex].Acto.Format("%H:%M");
						}
						ilJobIndex++;
					}
					else
					{
//						if (ilJobNo > 0)
						{
							olText = (olJobs[ilJobIndex].Text[0] != '\0')?
								omViewer.GetGateAreaOrCCIAreaName(olJobs[ilJobIndex].Text):
								olJobs[ilJobIndex].Alid;

							olFrom = olJobs[ilJobIndex].Acfr.Format("%H:%M");
							olTo   = olJobs[ilJobIndex].Acto.Format("%H:%M");
							ilJobIndex++;
						}
//						else
//						{
//							olFrom.Empty();
//							olTo.Empty();
//							olText.Empty();
//						}

					}
				}
				else
				{
					olFrom.Empty();
					olTo.Empty();
					olText.Empty();
					ilJobIndex++;
				}

				rlElement.Length     = 190;
				rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
				rlElement.Text       = olText;
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

				rlElement.Length     = 80;
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text       = olFrom;
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

				rlElement.Text       = olTo;
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

				ilJobNo++;
			}
			olJobs.RemoveAll();

			rlElement.Alignment  = PRINT_LEFT;
			rlElement.Length     = 470;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameRight = PRINT_FRAMEMEDIUM;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;
			rlElement.Text.Empty();

			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			pomPrint->PrintLine(rlPrintLine);
			rlPrintLine.DeleteAll();
		}
	}

	return TRUE;
}

BOOL PrePlanTable::PrintPreplanHeader(CCSPrint *pomPrint,CString opTeam, CString opDutyDate)
{
	pomPrint->pomCdc->StartPage();
	pomPrint->PrintHeader();

	return TRUE;
}

BOOL PrePlanTable::PrintPreplanTeam(CCSPrint *pomPrint,CString opTeam, CString opDutyDate)
{
	int i;
	SHIFTDATA *polShift;
	CCSPtrArray <SHIFTDATA> olShifts;

	int ilShiftCount = ogShiftData.omData.GetSize();
	for( i = 0; i < ilShiftCount; i++ ) 
	{
		// print only shifts of current duty date
		polShift = &ogShiftData.omData[i];
		CString olTeam = ogBasicData.GetTeamForShift(polShift);
		if(!strcmp(polShift->Sday,opDutyDate) && opTeam == olTeam)
		{
			if(imTerminalSelected != CPrintTerminalSelection::BOTH_TERMINAL_SELECTED)
			{
				SHIFTTYPEDATA* polShiftType = ogShiftTypes.GetShiftTypeByCode(polShift->Sfca);		
				CString olEmpTerminal = CString(polShift->Sfca).Mid(0,2);
				if(polShiftType != NULL)
				{
					olEmpTerminal = strlen(polShiftType->Bsds) == 0 ? olEmpTerminal : polShiftType->Bsds;
				}
				if((imTerminalSelected == CPrintTerminalSelection::TERMINAL2_SELECTED && olEmpTerminal.CompareNoCase(Terminal2) != 0)
					|| (imTerminalSelected == CPrintTerminalSelection::TERMINAL3_SELECTED && olEmpTerminal.CompareNoCase(Terminal2) == 0))
				{
					continue;
				}
			}
			olShifts.Add(polShift);
		}
	}

	ilShiftCount = olShifts.GetSize();
	olShifts.Sort(CompareShiftRank);
	ilShiftCount = olShifts.GetSize();
	if (ilShiftCount > 0)
	{
		if(pomPrint->IsPageSelected())
		{
			if(pomPrint->imPageNo <= pomPrint->GetToPage() - 1)
			{
				PrintPreplanHeader(pomPrint,opTeam,opDutyDate);
			}
			else
			{
				return TRUE;
			}
		}
		else
		{
			PrintPreplanHeader(pomPrint,opTeam,opDutyDate);
		}
	}
	for( i = 0; i < ilShiftCount; i++ ) 
	{
		SHIFTDATA *prlTestShift = &olShifts[i];
		if( pomPrint->imLineNo >= (pomPrint->imMaxLines-1) )
		{
			if ( pomPrint->imPageNo > 0)
			{
				PrintPreplanLine(pomPrint,&olShifts[i],opTeam,i,TRUE,opDutyDate);
//				pomPrint->PrintFooter(GetString(IDS_STRING33145),GetString(IDS_STRING33149));
				pomPrint->PrintFooter(CString(""));
				pomPrint->imLineNo = pomPrint->imMaxLines + 1;
				pomPrint->pomCdc->EndPage();
				pomPrint->imLineTop = pomPrint->imFirstLine;
			}
			if(pomPrint->IsPageSelected())
			{
				if(pomPrint->imPageNo <= pomPrint->GetToPage() - 1)
				{
					PrintPreplanHeader(pomPrint,opTeam,opDutyDate);
				}
				else
				{
					return TRUE;
				}
			}
			else
			{
				PrintPreplanHeader(pomPrint,opTeam,opDutyDate);
			}
		//	PrintPreplanLine(&olShifts[i],opTeam,i,FALSE);
		}
		PrintPreplanLine(pomPrint,&olShifts[i],opTeam,i,FALSE,opDutyDate);
	}

	PrintPreplanLine(pomPrint,polShift,opTeam,i,TRUE,opDutyDate);
	pomPrint->PrintFooter(GetString(IDS_STRING33145),GetString(IDS_STRING33149));
	pomPrint->pomCdc->EndPage();
	pomPrint->imLineTop = pomPrint->imFirstLine;
	pomPrint->imLineNo = 0;

	return TRUE;
}

////////////////////////////////////////////////////////////////////////
// PrePlanTable -- implementation of DDX call back function

void PrePlanTable::PrePlanTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
	PrePlanTable *polTable = (PrePlanTable *)popInstance;

	if(ipDDXType == REDISPLAY_ALL)
	{
		polTable->UpdateView("");
		polTable->SetCaptionText();
	}
	else if(ipDDXType == GLOBAL_DATE_UPDATE)
	{
		polTable->HandleGlobalDateUpdate();
	}
}

/////////////////////////////////////////////////////////////////////////////
// PrePlanTable -- drag-and-drop functionalities
//
// The user may drag from:
//	- any non-empty line for create a DutyBar in the StaffDiagram.
//
LONG PrePlanTable::OnTableDragBegin(UINT wParam, LONG lParam)
{
	CListBox *pListBox = (CListBox *)lParam;
	int nMaxItems;
	if ((nMaxItems = pListBox->GetSelCount()) == LB_ERR)
		return 0L;

/////////////////////////////////////////////////////////////////////////////
// DTT - Jul.18
// Change the algorithm for creating data structure on dragging from
// PrePlanTable. We put only still available staff from the PrePlanTable into
// the DragDropObject data structure.
//
	int *rgIndex = new int [nMaxItems];
	pListBox->GetSelItems(nMaxItems, rgIndex);
	if (!m_bCompleteTeam)
	{
		int i, ilItemCount = 0;
		for (i = 0; i < nMaxItems; i++)
		{
			PREPLANT_LINEDATA *prlLine = (PREPLANT_LINEDATA *)pomTable->GetTextLineData(rgIndex[i]);
			if ((prlLine->Line.AssignmentStatus != FULLY_ASSIGNED) &&
				(ogOdaData.GetOdaByType(prlLine->Line.Sfca) == NULL))
				/*
				(strcmp(prlLine->Line.Sfca,"K") != 0) &&
				(strcmp(prlLine->Line.Sfca,"L") != 0) &&
				(strcmp(prlLine->Line.Sfca,"U") != 0))
				*/
				ilItemCount++;
		}
		if (ilItemCount > 0)
		{
			omDragDropObject.CreateDWordData(DIT_SHIFT, ilItemCount);
			for (i = 0; i < nMaxItems; i++)
			{
				PREPLANT_LINEDATA *prlLine = (PREPLANT_LINEDATA *)pomTable->GetTextLineData(rgIndex[i]);
				if ((prlLine->Line.AssignmentStatus != FULLY_ASSIGNED) &&
					(ogOdaData.GetOdaByType(prlLine->Line.Sfca) == NULL))
					/*
					(strcmp(prlLine->Line.Sfca,"K") != 0) &&
					(strcmp(prlLine->Line.Sfca,"L") != 0) &&
					(strcmp(prlLine->Line.Sfca,"U") != 0))
					*/
					omDragDropObject.AddDWord(prlLine->Line.ShiftUrno);
			}
			omDragDropObject.BeginDrag();
			pListBox->SetSel(-1,FALSE);
		}
	}
	else	// dragging the whole team
	{
		int i, ilItemCount = 0;
		for (i = 0; i < nMaxItems; i++)
		{
			PREPLANT_LINEDATA *prlLine = (PREPLANT_LINEDATA *)pomTable->GetTextLineData(rgIndex[i]);
			for (int i = 0; i < prlLine->TeamMember.GetSize(); i++)
			{
				if ((prlLine->TeamMember[i].AssignmentStatus != FULLY_ASSIGNED) &&
					(ogOdaData.GetOdaByType(prlLine->TeamMember[i].Sfca) == NULL))
					/*
				(strcmp(prlLine->TeamMember[i].Sfca,"K") != 0) &&
				(strcmp(prlLine->TeamMember[i].Sfca,"L") != 0) &&
				(strcmp(prlLine->TeamMember[i].Sfca,"U") != 0))
				*/
					ilItemCount++;
			}
		}

		if (ilItemCount > 0)
		{
			omDragDropObject.CreateDWordData(DIT_SHIFT, ilItemCount);
			for (i = 0; i < nMaxItems; i++)
			{
				PREPLANT_LINEDATA *prlLine = (PREPLANT_LINEDATA *)pomTable->GetTextLineData(rgIndex[i]);
				for (int i = 0; i < prlLine->TeamMember.GetSize(); i++)
				{
					if ((prlLine->TeamMember[i].AssignmentStatus != FULLY_ASSIGNED) &&
						(ogOdaData.GetOdaByType(prlLine->TeamMember[i].Sfca) == NULL))
						/*
					(strcmp(prlLine->TeamMember[i].Sfca,"K") != 0) &&
					(strcmp(prlLine->TeamMember[i].Sfca,"L") != 0) &&
					(strcmp(prlLine->TeamMember[i].Sfca,"U") != 0))
					*/
						omDragDropObject.AddDWord(prlLine->TeamMember[i].ShiftUrno);
				}
			}
		omDragDropObject.BeginDrag();
		pListBox->SetSel(-1,FALSE);
		}
	}
/////////////////////////////////////////////////////////////////////////////
	delete rgIndex;

    return 0L;
}

void PrePlanTable::OnClose() 
{
	ClosePrePlan();

	pogButtonList->m_wndPrePlanTable = NULL;
	pogButtonList->m_PrePlanningButton.Recess(FALSE);
	//CDialog::OnClose();

}


void PrePlanTable::OnSelchangeDate() 
{
	SetViewerDate();

	// setting the time in preplanning results in the date being changed so that all other diagrams/gantts are synchronised
	SendUpdatePrePlanDate(omViewer.StartTime());

	UpdateView(omViewer.GetViewName());	
}

void PrePlanTable::SetViewerDate()
{
	CTime olStart = ogBasicData.GetTimeframeStart();
	CTime olDay = CTime(olStart.GetYear(),olStart.GetMonth(),olStart.GetDay(),0,0,0)  + CTimeSpan(m_Date.GetCurSel(),0,0,0);

	omViewer.SetStartTime(CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0));
	omViewer.SetEndTime(CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59));

	//Singapore
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		omViewerT3.SetStartTime(CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0));
		omViewerT3.SetEndTime(CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59));
	}
}

// called in response to a global date update - new day selected ie in preplanning so syschronise the date of this display
void PrePlanTable::HandleGlobalDateUpdate()
{
	m_Date.SetCurSel(ogBasicData.GetDisplayDateOffset());
	SetViewerDate();
	UpdateView(omViewer.GetViewName());	
}

void PrePlanTable::SetDate(CTime opDate)
{
	m_Date.SetCurSel(ogBasicData.GetDisplayDateOffsetByDate(opDate));
	OnSelchangeDate();
}

void PrePlanTable::SendUpdatePrePlanDate(CTime opDate)
{
	omPrePlanDate = opDate;

	// sending this message results in all other gantts/tables that need to be syschronized with the preplantable
//	ogCCSDdx.DataChanged((void *)this,PREPLAN_DATE_UPDATE,(void *)&omPrePlanDate);
}


void PrePlanTable::OnMenuDebugInfo()
{
	PREPLANT_LINEDATA *prlLine = (PREPLANT_LINEDATA *)pomTable->GetTextLineData( pomTable->GetCurrentLine() );
	if(prlLine != NULL)
	{
		CString olText;
		SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prlLine->Line.ShiftUrno);
		if(prlShift == NULL)
		{
			olText += "No shift data found for the pool job (Ustf)\n";
		}
		else
		{
			olText += "Shift Record:\n" + ogShiftData.Dump(prlLine->Line.ShiftUrno) + "\n";
			EMPDATA *prlEmp = ogEmpData.GetEmpByUrno(prlShift->Stfu);
			if(prlEmp == NULL)
			{
				olText += "No emp data found for the shift (Stfu)\n";
			}
			else
			{
				olText += "Emp Record:\n" + ogEmpData.Dump(prlShift->Stfu) + "\n";
			}
			DRGDATA *prlDrg = ogDrgData.GetDrgByShift(prlShift);
			if(prlDrg != NULL)
				olText += "DRG Group Record (DRG overrides SWG record):\n" + ogDrgData.Dump(prlDrg->Urno) + "\n";
			else
				olText += "No DRG group data found for the shift (Stfu/Sday)\n";
			olText += "Swg Group Records:\n" + ogSwgData.DumpForUstf(prlShift->Stfu, prlShift->Avfa);
		}
		JOBDATA *prlJob = NULL;
		for(int ilLc = 0; ilLc < prlLine->Line.Assignment.GetSize(); ilLc++)
		{
			olText += "Assignment:\n" + ogJobData.Dump(prlLine->Line.Assignment[ilLc].Urno) + "\n";
		}
		ogBasicData.DebugInfoMsgBox(olText);
	}
}

void PrePlanTable::OnMenuSwapShift()
{
	PREPLANT_LINEDATA *prlLine = (PREPLANT_LINEDATA *)pomTable->GetTextLineData( pomTable->GetCurrentLine() );
	if(prlLine != NULL)
	{
		SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prlLine->Line.ShiftUrno);
		if(prlShift != NULL)
		{
			if(strcmp(prlShift->Drrn,"1"))
			{
				MessageBox(GetString(IDS_SWAPSHIFT_INVALIDDRRN), GetString(IDS_SWAPSHIFTSDLG_TITLE), MB_ICONSTOP);
			}
			else
			{
				CSwapShiftDlg olSwapShiftDlg;
				olSwapShiftDlg.SetSday(prlShift->Avfa.Format("%Y%m%d"));
				olSwapShiftDlg.SetShifts(prlShift, NULL);
				for(int ilL = 0; ilL < pomViewer->omLines.GetSize(); ilL++)
				{
					if((prlShift = ogShiftData.GetShiftByUrno(pomViewer->omLines[ilL].Line.ShiftUrno)) != NULL)
						olSwapShiftDlg.omShifts.Add(prlShift);
				}

				olSwapShiftDlg.omShifts.Sort(ByShiftStart);

				if(olSwapShiftDlg.DoModal() == IDOK)
				{
				}
			}
		}
	}
}

//Singapore
void PrePlanTable::OnTableUpdateDataCount()
{
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		CWnd* polWndTermText = GetDlgItem(IDC_STATIC_T2T3);
		CWnd* polWndDataCount = GetDlgItem(IDC_DATACOUNT);
		if(polWndTermText != NULL && polWndDataCount != NULL)
		{
			char olDataCount[20];
			CString olEmpCount;
			itoa(omViewer.omLines.GetSize(),olDataCount,10);
			strcat(olDataCount,"/");
			olEmpCount = olDataCount;
			itoa(omViewerT3.omLines.GetSize(),olDataCount,10);
			olEmpCount += olDataCount;
			polWndDataCount->SetWindowText(olEmpCount);
		}
	}
}
//Singapore
void PrePlanTable::OnButtonT2()
{
	CRect olClientRect;
	GetWindowRect(&olClientRect);
	ScreenToClient(&olClientRect);

	CButton* polButton = (CButton*)GetDlgItem(IDC_BUTTONT2);	
	bmIsT2Visible = !bmIsT2Visible;
	
	if(bmIsT2Visible == TRUE)
	{
		m_bTerminal2.SetColors(LIME, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}
	else
	{
		m_bTerminal2.SetColors(::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}

	if(bmIsT2Visible == FALSE && bmIsT3Visible == FALSE)
	{
		omTable.ShowWindow(SW_HIDE);
	}	
	else if(bmIsT2Visible == TRUE && bmIsT3Visible == FALSE)
	{
		omTable.ShowWindow(SW_SHOW);
				
		CRect olRectT2;
		omTable.GetWindowRect(&olRectT2);
		ScreenToClient(&olRectT2);
		
		CRect olRectT3;
		omTableT3.GetWindowRect(&olRectT3);
		ScreenToClient(&olRectT3);

		omTable.SetPosition(-1,olClientRect.right-3,olRectT2.top,olClientRect.bottom-3);
	}
	if(bmIsT2Visible == FALSE && bmIsT3Visible == TRUE)
	{						
		CRect olRectT2;
		omTable.GetWindowRect(&olRectT2);
		ScreenToClient(&olRectT2);

		CRect olRectT3;
		omTableT3.GetWindowRect(&olRectT3);
		ScreenToClient(&olRectT3);

		omTable.ShowWindow(SW_HIDE);
		omTableT3.SetPosition(-1,olClientRect.right-3,olRectT2.top,olClientRect.bottom-3);
	}
	else if(bmIsT2Visible == TRUE && bmIsT3Visible == TRUE)
	{	
		omTable.ShowWindow(SW_SHOW);
		
		CRect olRectT2;
		omTable.GetWindowRect(&olRectT2);
		ScreenToClient(&olRectT2);

		CRect olRectT3;
		omTableT3.GetWindowRect(&olRectT3);
		ScreenToClient(&olRectT3);

		omTable.SetPosition(-1,olClientRect.right-3,olRectT2.top,(olClientRect.bottom - olRectT2.top)/2);
		omTableT3.SetPosition(-1,olClientRect.right-3,(olClientRect.bottom - olRectT2.top)/2,olClientRect.bottom-3);
	}
}

//Singapore
void PrePlanTable::OnButtonT3()
{
	CRect olClientRect;
	GetWindowRect(&olClientRect);
	ScreenToClient(&olClientRect);

	CButton* polButton = (CButton*)GetDlgItem(IDC_BUTTONT3);	
	bmIsT3Visible = !bmIsT3Visible;

	if(bmIsT3Visible == TRUE)
	{
		m_bTerminal3.SetColors(LIME, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}
	else
	{
		m_bTerminal3.SetColors(::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	}

	if(bmIsT3Visible == FALSE && bmIsT2Visible == FALSE)
	{
		omTableT3.ShowWindow(SW_HIDE);

	}	
	else if(bmIsT3Visible == TRUE && bmIsT2Visible == FALSE)
	{
		omTableT3.ShowWindow(SW_SHOW);
				
		CRect olRectT2;
		omTable.GetWindowRect(&olRectT2);
		ScreenToClient(&olRectT2);
		
		CRect olRectT3;
		omTableT3.GetWindowRect(&olRectT3);
		ScreenToClient(&olRectT3);

		omTableT3.SetPosition(-1,olClientRect.right-3,olRectT2.top,olClientRect.bottom-3);
	}
	else if(bmIsT3Visible == FALSE && bmIsT2Visible == TRUE)
	{		
		CRect olRectT2;
		omTable.GetWindowRect(&olRectT2);
		ScreenToClient(&olRectT2);

		CRect olRectT3;
		omTableT3.GetWindowRect(&olRectT3);
		ScreenToClient(&olRectT3);
						
		omTableT3.ShowWindow(SW_HIDE);
		omTable.SetPosition(-1,olClientRect.right-3,olRectT2.top,olClientRect.bottom-3);
	}
	else if(bmIsT3Visible == TRUE && bmIsT2Visible == TRUE)
	{	
		omTableT3.ShowWindow(SW_SHOW);

		CRect olRectT2;
		omTable.GetWindowRect(&olRectT2);
		ScreenToClient(&olRectT2);

		CRect olRectT3;
		omTableT3.GetWindowRect(&olRectT3);
		ScreenToClient(&olRectT3);
		
		omTable.SetPosition(-1,olClientRect.right-3,olRectT2.top,(olClientRect.bottom - olRectT2.top)/2);
		omTableT3.SetPosition(-1,olClientRect.right-3,(olClientRect.bottom - olRectT2.top)/2,olClientRect.bottom-3);
	}
}

void PrePlanTable::OnPrintPreview()
{	
	if (PreplanPreparePrintData(omDate) == FALSE)
	{
		return;
	}
	
	if(ogBasicData.IsPaxServiceT2() == true && SelectPrintTerminal() == FALSE)
	{
		return;
	}
	
	if (!pomTemplate)
	{
		pomTemplate = new CSingleDocTemplate(
			IDR_MENU_PRINT_PREVIEW,
			NULL,
			RUNTIME_CLASS(CFrameWnd),
			RUNTIME_CLASS(CPrintPreviewView));
		AfxGetApp()->AddDocTemplate(pomTemplate);
	}

	CFrameWnd * pFrameWnd = pomTemplate->CreateNewFrame( NULL,NULL );
	pomTemplate->InitialUpdateFrame( pFrameWnd, NULL, FALSE);
	pomPrintPreviewView =(CPrintPreviewView*)pFrameWnd->GetActiveView();
	pomPrintPreviewView->pomCallMsgWindow=this;
	pomPrintPreviewView->SetFrameWindow(pFrameWnd);
	pFrameWnd->SetWindowText(_T("Preplan Print Preview"));
	pomPrintPreviewView->OnFilePrintPreview();
	return;
}

void PrePlanTable::OnBeginPrinting(WPARAM wParam, LPARAM lParam)
{
	PRINT_PREVIEW_DCINFO* polPrintPreviewDcInfo = (PRINT_PREVIEW_DCINFO*)lParam;
	if (!polPrintPreviewDcInfo->pomDC|| !polPrintPreviewDcInfo->pomPrintInfo) return;

	CString omTarget = CString("Date: ") + CString(omViewer.StartTime().Format("%d.%m.%Y"))
		+  "     View: " + ogCfgData.rmUserSetup.VPLV;
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");
	if(pomPrint == NULL)
	{
		pomPrint = new CCSPrint(this,PRINT_LANDSCAPE,80,500,200,
		CString("Preplanning - Attendance List"),olPrintDate,omTarget);
		pomPrint->pomCdc = polPrintPreviewDcInfo->pomDC;
	}	
	pomPrint->InitializePrintSetup(CCSPrint::PRINT_PREVIEW);
	imPrintMaxLines = pomPrint->imMaxLines;
	SetupPrintPreviewPageInfo();
	polPrintPreviewDcInfo->pomPrintInfo->SetMaxPage(omMapPageNoToPreviewData.GetCount());
	polPrintPreviewDcInfo->pomPrintInfo->m_nCurPage = 1;
	bgPrintPreviewMode = TRUE;
	pogPreviewPrintWnd = this;
	pomPrintPreviewView->GetFrameWindow()->BeginModalState();
}

void PrePlanTable::PrintPreview(WPARAM wParam, LPARAM lParam)
{
	PRINT_PREVIEW_DCINFO* polPrintPreviewDcInfo = (PRINT_PREVIEW_DCINFO*)lParam;
	if (!polPrintPreviewDcInfo->pomDC|| !polPrintPreviewDcInfo->pomPrintInfo) return;
	
	pomPrint->pomCdc = polPrintPreviewDcInfo->pomDC;
	pomPrint->pomCdc->SetMapMode(MM_ANISOTROPIC);
 	pomPrint->pomCdc->SetWindowExt(pomPrint->pomCdc->GetDeviceCaps(HORZRES),pomPrint->pomCdc->GetDeviceCaps(VERTRES));
	pomPrint->pomCdc->SetViewportExt(pomPrint->pomCdc->GetDeviceCaps(HORZRES),pomPrint->pomCdc->GetDeviceCaps(VERTRES));	
	pomPrint->InitializePrintSetup(CCSPrint::PRINT_PREVIEW);
	PrintData(pomPrint,polPrintPreviewDcInfo->pomPrintInfo->m_nCurPage);
}

void PrePlanTable::OnEndPrinting(WPARAM wParam, LPARAM lParam)
{
	if(pomPrint != NULL)
	{
		delete pomPrint;
		pomPrint = NULL;	
	}
	pomPrintPreviewView->GetFrameWindow()->EndModalState();
	pomPrintPreviewView = NULL;
	bgPrintPreviewMode = FALSE;
}

void PrePlanTable::OnPreviewPrint(WPARAM wParam, LPARAM lParam)
{
	PrintData();
	pogPreviewPrintWnd = NULL;
}

BOOL PrePlanTable::GetShiftDataArray(CCSPtrArray<SHIFTDATA>& ropShiftArray,const CString& ropTeam,
									 const CString& ropDutyDate) const
{
	ropShiftArray.RemoveAll();
	int i;
	SHIFTDATA *polShift = NULL;

	int ilShiftCount = ogShiftData.omData.GetSize();
	for( i = 0; i < ilShiftCount; i++ ) 
	{
		polShift = &ogShiftData.omData[i];
		CString olTeam = ogBasicData.GetTeamForShift(polShift);
		if(polShift != NULL && !strcmp(polShift->Sday,ropDutyDate) && ropTeam == olTeam)
		{
			if(imTerminalSelected != CPrintTerminalSelection::BOTH_TERMINAL_SELECTED)
			{
				SHIFTTYPEDATA* polShiftType = ogShiftTypes.GetShiftTypeByCode(polShift->Sfca);		
				CString olEmpTerminal = CString(polShift->Sfca).Mid(0,2);
				if(polShiftType != NULL)
				{
					olEmpTerminal = strlen(polShiftType->Bsds) == 0 ? olEmpTerminal : polShiftType->Bsds;
				}
				if((imTerminalSelected  == CPrintTerminalSelection::TERMINAL2_SELECTED && olEmpTerminal.CompareNoCase(Terminal2) != 0)
					|| (imTerminalSelected  == CPrintTerminalSelection::TERMINAL3_SELECTED && olEmpTerminal.CompareNoCase(Terminal2) == 0))
				{
					continue;
				}
			}
			ropShiftArray.Add(polShift);
		}
	}

	return ropShiftArray.GetSize()>0;
}
void PrePlanTable::SetupPrintPageInfo()
{
	WORD dWord;
	PRINT_PREVIEW_PAGEDATA* polPrintPreviewPageData = NULL;
	POSITION olPos = omMapPageNoToPreviewData.GetStartPosition();
	while(olPos != NULL)
	{
		omMapPageNoToPreviewData.GetNextAssoc(olPos,dWord,(void*&)polPrintPreviewPageData);
		if(polPrintPreviewPageData != NULL)
		{
			delete polPrintPreviewPageData;
		}
	}
	omMapPageNoToPreviewData.RemoveAll();

	CCSPtrArray<SHIFTDATA> olShifts;
	int ilPageNos = -1;
	int ilRemainder = -1;
	int ilPageNo = 1;
	for(int i = 0; i < omTeamDlg.m_TeamSelect.GetSize(); i++)
	{
		int ilEndLineNo = 0;
		olShifts.RemoveAll();
		GetShiftDataArray(olShifts,omTeamDlg.m_TeamSelect[i],omDate);
		ilRemainder = olShifts.GetSize() % imPrintMaxLines;
		ilPageNos = ilRemainder == 0 ? olShifts.GetSize() / imPrintMaxLines : (olShifts.GetSize() / imPrintMaxLines) + 1;
		
		for(int j = 0; j < ilPageNos; j++)
		{
			polPrintPreviewPageData = new PRINT_PREVIEW_PAGEDATA();
			polPrintPreviewPageData->imPageNo = ilPageNo;
			polPrintPreviewPageData->imStartLineNo = (j == 0 ? 0 : ilEndLineNo);
			ilEndLineNo = polPrintPreviewPageData->imEndLineNo = (olShifts.GetSize() > imPrintMaxLines ? (j == (ilPageNos - 1) ? (imPrintMaxLines)*(j) + ilRemainder : (imPrintMaxLines)*(j+1)) : olShifts.GetSize());
			polPrintPreviewPageData->omTeamName = omTeamDlg.m_TeamSelect[i];
			omMapPageNoToPreviewData.SetAt(ilPageNo,(void*&)polPrintPreviewPageData);
			ilPageNo++;			
		}
	}	
}

void PrePlanTable::SetupPrintPreviewPageInfo()
{	
	SetupPrintPageInfo();
}

void PrePlanTable::PrintData(CCSPrint *pomPrint,const int& ripPageNo)
{
	if(ripPageNo == 2)
	{
		int il = 20;
	}
	PRINT_PREVIEW_PAGEDATA* polPrintPreviewPageData = NULL;
	omMapPageNoToPreviewData.Lookup(ripPageNo,(void*&)polPrintPreviewPageData);
	if(polPrintPreviewPageData == NULL)
	{
		return;
	}

	CCSPtrArray <SHIFTDATA> olShifts;
	GetShiftDataArray(olShifts,polPrintPreviewPageData->omTeamName,omDate);	
	//olShifts.Sort(CompareShiftRank);

	if (polPrintPreviewPageData->imEndLineNo  > polPrintPreviewPageData->imStartLineNo)
	{
		pomPrint->imPageNo = ripPageNo;
		PrintPreplanHeader(pomPrint,polPrintPreviewPageData->omTeamName,omDate);
	}	

	for(int i = polPrintPreviewPageData->imStartLineNo; i < polPrintPreviewPageData->imEndLineNo; i++ ) 
	{		
		pomPrint->imPageNo = ripPageNo;
		PrintPreplanLine(pomPrint,&olShifts[i],polPrintPreviewPageData->omTeamName,i,FALSE,omDate);
	}
	
	pomPrint->PrintFooter(GetString(IDS_STRING33145),GetString(IDS_STRING33149));
	pomPrint->pomCdc->EndPage();
	pomPrint->imLineTop = pomPrint->imFirstLine;
	pomPrint->imLineNo = 0;	
}

BOOL PrePlanTable::SelectPrintTerminal()
{
	BOOL blSelectPrintTerminal = TRUE;
	CPrintTerminalSelection olTerminalSelection;
	if(olTerminalSelection.DoModal() == IDOK)
	{
		if(olTerminalSelection.IsTerminal2Selected() == TRUE)
		{
			imTerminalSelected = CPrintTerminalSelection::TERMINAL2_SELECTED;
		}
		else if(olTerminalSelection.IsTerminal3Selected() == TRUE)
		{
			imTerminalSelected = CPrintTerminalSelection::TERMINAL3_SELECTED;
		}
		else if(olTerminalSelection.IsBothTerminalSelected() == TRUE)
		{
			imTerminalSelected = CPrintTerminalSelection::BOTH_TERMINAL_SELECTED;
		}
	}
	else
	{					
		blSelectPrintTerminal = FALSE;
	}
	return blSelectPrintTerminal;
}

void PrePlanTable::PrintData()
{	
	CString omTarget = CString("Date: ") + CString(omViewer.StartTime().Format("%d.%m.%Y"))
			+  "     View: " + ogCfgData.rmUserSetup.VPLV;
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");
	pomPrint = new CCSPrint(this,PRINT_LANDSCAPE,80,500,200,
		CString("Preplanning - Attendance List"),olPrintDate,omTarget);		
	if (pomPrint != NULL)
	{
		//pomPrint->EnablePageSelection();
		if (pomPrint->InitializePrinter(PRINT_LANDSCAPE,ogBasicData.pcmPrintForm,DMPAPER_A3)  == TRUE)
		{
			imPrintMaxLines = pomPrint->imMaxLines;
			SetupPrintPageInfo();

			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = "AttendanceList";	
			pomPrint->imLineHeight = 82;
			pomPrint->omCdc.StartDoc( &rlDocInfo );				
			
			if(pomPrint->IsPageSelected() == TRUE)
			{
				for(int i = pomPrint->GetFromPage() ; i <= pomPrint->GetToPage(); i++)
				{
					PrintData(pomPrint,i);
				}
			}
			else
			{
				for(int i = 0; i < omTeamDlg.m_TeamSelect.GetSize(); i++ ) 
				{
					PrintPreplanTeam(pomPrint,omTeamDlg.m_TeamSelect[i],omDate);
				}
			}
			pomPrint->omCdc.EndDoc();
		}
		delete pomPrint;
		pomPrint = NULL;
	}
}

//PRF 8712
void PrePlanTable::OnMove(int x, int y)
{
	CDialog::OnMove(x,y);
	if(this->IsIconic() == FALSE)
	{
		GetWindowRect(&omWindowRect);
	}
}

// cannot update shift with deviation, temp absence or shadow shift
bool PrePlanTable::CheckForDeviationOrAbsence(SHIFTDATA *prpShift)
{
	bool blOk = true;

	CString olErr, olErr2;
	int i;
	CCSPtrArray <JOBDATA> olJobs;
	CCSPtrArray <DRDDATA> olDrds;
	DRDDATA *prlDrd = NULL;
	bool blErr = false;

	ogJobData.GetJobsByShur(olJobs, prpShift->Urno, TRUE);
	blErr = false;
	for(i = 0; i < olJobs.GetSize(); i++)
	{
		if((prlDrd = ogDrdData.GetDrdByUrno(olJobs[i].Udrd)) != NULL)
		{
			olDrds.Add(prlDrd);
			blErr = true;
		}
	}
	if(blErr)
	{
		olErr2.Format("%s: %s\n", ogEmpData.GetEmpName(prpShift->Stfu), GetString(IDS_SWAPSHIFT_DEVIATION));
		olErr += olErr2;
	}

	CCSPtrArray <JOBDATA> olTempAbsences;
	CCSPtrArray <JOBDATA> olAllTempAbsences;

	ogJobData.GetJobsByPkno(olTempAbsences, prpShift->Peno, JOBTEMPABSENCE);
	blErr = false;
	for(i = 0; i < olTempAbsences.GetSize(); i++)
	{
		if(IsOverlapped(olTempAbsences[i].Acfr, olTempAbsences[i].Acto, prpShift->Avfa, prpShift->Avta))
		{
			olAllTempAbsences.Add(&olTempAbsences[i]);
			blErr = true;
		}
	}
	if(blErr)
	{
		olErr2.Format("%s: %s\n", ogEmpData.GetEmpName(prpShift->Stfu), GetString(IDS_SWAPSHIFT_TEMPABS));
		olErr += olErr2;
	}

	if(!olErr.IsEmpty())
	{
		if(MessageBox(olErr, GetString(IDS_SWAPSHIFTSDLG_TITLE), MB_ICONEXCLAMATION|MB_OKCANCEL) == IDOK)
		{
			for(i = 0; i < olDrds.GetSize(); i++)
			{
				ogDrdData.DeleteDrdRecord(olDrds[i].Urno);
			}
			for(i = 0; i < olAllTempAbsences.GetSize(); i++)
			{
				JOBDATA *prlJob = &olAllTempAbsences[i];
				if(ogDraData.DeleteDraRecord(prlJob->Shur))
					ogJobData.DeleteJob(prlJob->Urno);
			}
		}
		else
		{
			blOk = false;
		}
	}

	return blOk;
}
