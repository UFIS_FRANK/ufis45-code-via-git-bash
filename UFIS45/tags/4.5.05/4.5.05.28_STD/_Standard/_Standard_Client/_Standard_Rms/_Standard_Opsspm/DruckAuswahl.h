// DruckAuswahl.h : header file
//
#ifndef _CDRUCKAUSWAHL_H_
#define _CDRUCKAUSWAHL_H_

/////////////////////////////////////////////////////////////////////////////
// CDruckAuswahl dialog

class CDruckAuswahl : public CDialog
{
// Construction
public:
	CDruckAuswahl(CWnd* pParent = NULL);   // standard constructor

// Dialog Data

	//{{AFX_DATA(CDruckAuswahl)
	enum { IDD = IDD_DRUCKAUSW };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	int m_DruckAusw;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDruckAuswahl)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDruckAuswahl)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
#endif _CDRUCKAUSWAHL_H_