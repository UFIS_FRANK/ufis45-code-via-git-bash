// RegnDetailWindow.cpp : implementation file
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <OpssPm.h>
#include <table.h>
#include <RegnDetailWindow.h>

#include <CCSPtrArray.h>

#include <CCSCedaData.h>
#include <AllocData.h>
#include <CedaJobData.h>
#include <CedaFlightData.h>
#include <CedaEmpData.h>

#include <cviewer.h>
#include <RegnDetailViewer.h>
#include <CedaAcrData.h>

#include <ccsddx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
//static char THIS_FILE[] = __FILE__;
#endif

RegnDetailWindow* RegnDetailWindow::omCurrent = NULL;

/////////////////////////////////////////////////////////////////////////////
// RegnDetailWindow dialog

RegnDetailWindow::RegnDetailWindow(CWnd* pParent,const char *pcpAlid,
	BOOL bpArrival, BOOL bpDeparture, CTime opStartTime, CTime opEndTime)
	: CDialog(RegnDetailWindow::IDD, pParent)
{
	if (omCurrent)
	{
		if (omCurrent->GetSafeHwnd())
			omCurrent->DestroyWindow();
		else
			delete omCurrent;
	}

	omCurrent = this;

	pcmAlid = &cmAlid[0];
	strcpy(pcmAlid, pcpAlid);
	bmArrival = bpArrival;
	bmDeparture = bmDeparture;
	omStartTime = opStartTime;
	omEndTime = opEndTime;

	Create(IDD, pParent);
	CenterWindow(pParent);

	CString olCaptionText;
	//olCaptionText.Format("Regn Info %s von: %s bis: %s",pcpAlid,omStartTime.Format("%H%M"),omEndTime.Format("%H%M"));
	olCaptionText.Format(GetString(IDS_STRING61311),pcpAlid,omStartTime.Format("%H%M"),omEndTime.Format("%H%M"));
	SetWindowText(olCaptionText);
	//
	char clText[sizeof(cmAlid) + 1];
	strcpy(clText, GetString(IDS_STRING33133));
	strcat(clText, pcmAlid);
	strcat(clText, " :");
	SetDlgItemText(IDC_ALID1, clText);

	strcpy(clText, GetString(IDS_STRING33132));
	strcat(clText, pcmAlid);
	SetDlgItemText(IDC_ALID2, clText);

	//
	char clTeln[82];
	strcpy(clTeln, "");

//	const METAALLOCDATA *prlMA;
//	prlMA = ogRegnAreas.GetSingleAllocUnitByAlid(pcmAlid);
//	if (prlMA != NULL)
//		strcpy(clTeln, prlMA->Teln);

	ACRDATA *prlRegn = ogAcrData.GetAcrByName(pcmAlid);
	/*
	if(prlRegn != NULL)
	{
		strcpy(clTeln,prlRegn->Tele);
	}
	*/
	SetDlgItemText(IDC_TELN, clTeln); 

	m_nDialogBarHeight = 60;

	omTable.tempFlag = 2;

    CRect rect;
	GetClientRect(&rect);
    rect.OffsetRect(0, m_nDialogBarHeight);
    rect.InflateRect(1, 1);     // hiding the CTable window border
	
	//GetDlgItem(IDC_TABLE_REGN)->GetClientRect(&rect);
    omTable.SetTableData(this, rect.left, rect.right, rect.top, rect.bottom);

    omViewer.Attach(&omTable,omStartTime,omEndTime,pcmAlid);

	omViewer.ChangeView();
	//

	//{{AFX_DATA_INIT(RegnDetailWindow)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void RegnDetailWindow::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RegnDetailWindow)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(RegnDetailWindow, CDialog)
	//{{AFX_MSG_MAP(RegnDetailWindow)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// RegnDetailWindow message handlers

int RegnDetailWindow::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// Register DDX call back function
	TRACE("RegnDetailWindow: DDX Registration\n");
	ogCCSDdx.Register(this, REDISPLAY_ALL, CString("REGNDW"),
		CString("Redisplay all from What-If"), RegnDetailWindowCf);	// for what-if changes
	
	return 0;
}

void RegnDetailWindow::OnDestroy() 
{
	if (bgModal == TRUE)
		return;

	// Unregister DDX call back function
	TRACE("RegnDetailWindow: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogCCSDdx.UnRegister(this, NOTUSED);

	CDialog::OnDestroy();
	omCurrent = NULL;
	delete this;
}

void RegnDetailWindow::OnCancel() 
{
	// Normally, we should just call CDialog::OnCancel().
	// However, the DestroyWindow() is required since this table can be destroyed in two ways.
	// First is by pressing ESC key which will call this routine.
	// Second, it may be destroyed directly by calling Manfred's DestroyWindow() in this class
	// which is not so compatible with MFC.
	DestroyWindow();
}

void RegnDetailWindow::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
    if (nType != SIZE_MINIMIZED)
		omTable.SetPosition(-1, cx+1, m_nDialogBarHeight-1, cy+1);
}

////////////////////////////////////////////////////////////////////////
// RegnDetailWindow -- implementation of DDX call back function

void RegnDetailWindow::RegnDetailWindowCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
	RegnDetailWindow *polTable = (RegnDetailWindow *)popInstance;

	if (ipDDXType == REDISPLAY_ALL)
		polTable->DestroyWindow();
}

BOOL RegnDetailWindow::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	SetWindowText(GetString(IDS_STRING33020));
	CWnd *polWnd = GetDlgItem(IDC_ALID1); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32984));
	}
	polWnd = GetDlgItem(IDC_ALID2); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33021));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61708));
	}
	
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

/////////////////////////////////////////////////////////////////////////////
// RegnDetailWindow -- drag-and-drop functionalities
//
// The user may drag from:
//	- any non-empty line for create a JobFlightManager (JTCO = "FMJ").
//		(It's DIT_FLIGHTBAR when the user drag from this detail window).
//
// and the user will have opportunities to:
//	-- drop from StaffTable (single or multiple staff shift) onto any non-empty lines.
//		We will create JobFlightManagers for employees who are managers.
//	-- drop from DutyBar onto onto any non-empty lines.
//		We will create JobFlightManagers for employees who are managers.
//

