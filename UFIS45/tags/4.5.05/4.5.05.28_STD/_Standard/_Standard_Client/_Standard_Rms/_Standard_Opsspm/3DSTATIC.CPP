// static3d.cpp : implementation file
//
// Written By:
// Pichet on some day in the beginning time of the project -- Damkerng
//
// Modification History:
// Damkerng Thammathakerngkit	Sep 29th, 1996
//	Fix the painting routine which paints outside the window.
//	The fixed places will be commented as "-- this will paint outside the window".

#include <stdafx.h>
#include <OpssPm.h>
#include <3dstatic.h>
   
#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif

/////////////////////////////////////////////////////////////////////////////
// C3DStatic

C3DStatic::C3DStatic(BOOL bpReverse)
{
    bmReverse = bpReverse;
}

C3DStatic::~C3DStatic()
{
}


BEGIN_MESSAGE_MAP(C3DStatic, CStatic)
    //{{AFX_MSG_MAP(C3DStatic)
    ON_WM_PAINT()
    ON_WM_ERASEBKGND()
    ON_MESSAGE(WM_GETTEXT, OnGetText)
    ON_MESSAGE(WM_SETTEXT, OnSetText)
    ON_WM_CREATE()
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// C3DStatic message handlers

void C3DStatic::OnPaint() 
{
    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    // Do not call CStatic::OnPaint() for painting messages

    CFont *polOldFont = (CFont *) dc.SelectObject(omFont);
    CRect olClientRect; GetClientRect(&olClientRect);

    CPen ol1Pen;
    CPen ol2Pen;
    CPen ol3Pen;
    CPen ol4Pen;

    if (!bmReverse)
    {
        ol1Pen.CreatePen(PS_SOLID, 1, GetSysColor(COLOR_BTNFACE));
        ol2Pen.CreatePen(PS_SOLID, 1, GetSysColor(COLOR_WINDOWFRAME));
        ol3Pen.CreatePen(PS_SOLID, 1, GetSysColor(COLOR_WINDOW));
        ol4Pen.CreatePen(PS_SOLID, 1, GetSysColor(COLOR_BTNSHADOW));
    }
    else
    {
        ol4Pen.CreatePen(PS_SOLID, 1, GetSysColor(COLOR_BTNFACE));
        ol3Pen.CreatePen(PS_SOLID, 1, GetSysColor(COLOR_WINDOWFRAME));
        ol2Pen.CreatePen(PS_SOLID, 1, GetSysColor(COLOR_WINDOW));
        ol1Pen.CreatePen(PS_SOLID, 1, GetSysColor(COLOR_BTNSHADOW));
    }

    // draw top & left light-gray line
    CPen *polOldPen = dc.SelectObject(&ol1Pen);
    dc.MoveTo(olClientRect.right - 1, olClientRect.top);
    dc.LineTo(olClientRect.TopLeft());
    //dc.LineTo(olClientRect.left, olClientRect.bottom); -- this will paint outside the window
	dc.LineTo(olClientRect.left, olClientRect.bottom-1);

    // draw bottom & right black line
    dc.SelectObject(&ol2Pen);
    //dc.LineTo(olInnerRect.BottomRight());	-- this will paint outside the window
    //dc.LineTo(olClientRect.right, olClientRect.top - 1);
    dc.LineTo(olClientRect.right-1, olClientRect.bottom-1);
    dc.LineTo(olClientRect.right-1, olClientRect.top - 1);
    
    // draw top & left white line
    CRect olInnerRect(olClientRect.left+1, olClientRect.top+1, olClientRect.right-1, olClientRect.bottom-1);
    dc.SelectObject(&ol3Pen);
    dc.MoveTo(olInnerRect.right - 1, olInnerRect.top);
    dc.LineTo(olInnerRect.TopLeft());
    //dc.LineTo(olInnerRect.left, olInnerRect.bottom); -- this will paint outside the window
	dc.LineTo(olInnerRect.left, olInnerRect.bottom-1);

    // draw bottom & right dark-gray line
    dc.SelectObject(&ol4Pen);
    //dc.LineTo(olInnerRect.BottomRight()); -- this will paint outside the window
    //dc.LineTo(olInnerRect.right, olInnerRect.top - 1);
    dc.LineTo(olInnerRect.right-1, olInnerRect.bottom-1);
    dc.LineTo(olInnerRect.right-1, olInnerRect.top - 1);
    
    CString olWindowText;
    char clBuf[255]; GetWindowText(clBuf, sizeof (clBuf)); olWindowText = clBuf;
    
    dc.SetBkColor(lmBkColor);
    dc.SetTextColor(lmTextColor);
    
    int ilLeftPos;
    DWORD llStyle = GetStyle();
    
    // all static have SS_LEFT so we check SS_LEFT last
    if ((llStyle & SS_RIGHT) == SS_RIGHT)
    {
        ilLeftPos = olClientRect.right - dc.GetTextExtent(olWindowText, olWindowText.GetLength()).cx - 5;
        dc.TextOut(ilLeftPos, (olClientRect.Height() - dc.GetTextExtent(olWindowText, olWindowText.GetLength()).cy) / 2, olWindowText);
    }
    else if ((llStyle & SS_CENTER) == SS_CENTER)
    {
        ilLeftPos = (olClientRect.Width() - dc.GetTextExtent(olWindowText, olWindowText.GetLength()).cx) / 2;
        
        int ilSlashPosition = olWindowText.Find('/');
        if (ilSlashPosition != -1)
        {                                          
            ilSlashPosition++;
        
            CString olLeftText = olWindowText.Left(ilSlashPosition);
            CString olRightText = olWindowText.Mid(ilSlashPosition);
        
            dc.TextOut(ilLeftPos, (olClientRect.Height() - dc.GetTextExtent(olLeftText, olLeftText.GetLength()).cy) / 2, olLeftText);

            dc.SetTextColor(lmHilightColor);
        
            ilLeftPos = ((olClientRect.Width() - dc.GetTextExtent(olWindowText, olWindowText.GetLength()).cx) / 2) +
                dc.GetTextExtent(olLeftText, olLeftText.GetLength()).cx;
            dc.TextOut(ilLeftPos, (olClientRect.Height() - dc.GetTextExtent(olWindowText, olWindowText.GetLength()).cy) / 2, olRightText);
        }
        else
        {
            dc.TextOut(ilLeftPos, (olClientRect.Height() - dc.GetTextExtent(olWindowText, olWindowText.GetLength()).cy) / 2, olWindowText);
        }
    }
    else if ((llStyle & SS_LEFT) == SS_LEFT)
    {
        ilLeftPos = 5;
        dc.TextOut(ilLeftPos, (olClientRect.Height() - dc.GetTextExtent(olWindowText, olWindowText.GetLength()).cy) / 2, olWindowText);
    }

    dc.SelectObject(polOldPen);
    dc.SelectObject(polOldFont);
}

BOOL C3DStatic::OnEraseBkgnd(CDC* pDC) 
{
    CRect olClipRect;
    //pDC->GetClipBox(&olClipBox);     // can't be used
    GetClientRect(&olClipRect);

    CBrush olBrush(lmBkColor);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);
    
    return TRUE;
}

LONG C3DStatic::OnGetText(WPARAM wParam, LPARAM lParam)
{
    strncpy((LPSTR) lParam, omText, wParam);
    return omText.GetLength();
}

LONG C3DStatic::OnSetText(WPARAM wParam, LPARAM lParam)
{
    omText = (LPSTR)lParam;
    return 0L;
}

int C3DStatic::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CStatic::OnCreate(lpCreateStruct) == -1)
        return -1;
    
    SetWindowText(lpCreateStruct->lpszName);
    return 0;
}
