// CedaAptData.cpp - Class for Airline Types
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaAptData.h>
#include <BasicData.h>

CedaAptData::CedaAptData()
{                  
    BEGIN_CEDARECINFO(APTDATA, AptDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Apc3,"APC3")
		FIELD_CHAR_TRIM(Apc4,"APC4")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(AptDataRecInfo)/sizeof(AptDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&AptDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"APTTAB");
    pcmFieldList = "URNO,APC3,APC4";
}
 
CedaAptData::~CedaAptData()
{
	TRACE("CedaAptData::~CedaAptData called\n");
	ClearAll();
}

void CedaAptData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omApc3Map.RemoveAll();
	omApc4Map.RemoveAll();
	omData.DeleteAll();
}

CCSReturnCode CedaAptData::ReadAptData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];
    strcpy(pclWhere,"");
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaAptData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		APTDATA rlAptData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlAptData)) == RCSuccess)
			{
				AddAptInternal(rlAptData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaAptData Read %d Records (Rec Size %d) %s",omData.GetSize(),sizeof(APTDATA), pclWhere);
    return ilRc;
}


void CedaAptData::AddAptInternal(APTDATA &rrpApt)
{
	APTDATA *prlApt = new APTDATA;
	*prlApt = rrpApt;
	omData.Add(prlApt);
	omUrnoMap.SetAt((void *)prlApt->Urno,prlApt);
	if(strlen(prlApt->Apc3) > 0 && strcmp(prlApt->Apc3," "))
	{
		omApc3Map.SetAt(prlApt->Apc3,prlApt);
	}
	if(strlen(prlApt->Apc4) > 0 && strcmp(prlApt->Apc4," "))
	{
		omApc4Map.SetAt(prlApt->Apc4,prlApt);
	}
}


APTDATA* CedaAptData::GetAptByUrno(long lpUrno)
{
	APTDATA *prlApt = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlApt);
	return prlApt;
}

APTDATA* CedaAptData::GetAptByApc3(const char *pcpApc3)
{
	APTDATA *prlApt = NULL;
	if(strlen(pcpApc3) > 0 && strcmp(pcpApc3," "))
	{
		omApc3Map.Lookup(pcpApc3, (void *&) prlApt);
	}
	return prlApt;
}

APTDATA* CedaAptData::GetAptByApc4(const char *pcpApc4)
{
	APTDATA *prlApt = NULL;
	if(strlen(pcpApc4) > 0 && strcmp(pcpApc4," "))
	{
		omApc4Map.Lookup(pcpApc4, (void *&) prlApt);
	}
	return prlApt;
}

CString CedaAptData::FormatAptString(const char *pcpApc3, const char *pcpApc4)
{
	CString olApcString;
	APTDATA *prlApt = GetAptByApc3(pcpApc3);
	if(prlApt == NULL)
	{
		prlApt = GetAptByApc4(pcpApc4);
	}
	if(prlApt != NULL)
	{
		olApcString = FormatAptString(prlApt);
	}

	return olApcString;
}

CString CedaAptData::FormatAptString(APTDATA *prpApt)
{
	CString olApcString;
	if(prpApt != NULL)
	{
		olApcString.Format("%s/%s",prpApt->Apc3,prpApt->Apc4);
	}

	return olApcString;
}

int CedaAptData::GetApc3Apc4Strings(CStringArray &ropApc3Apc4Strings)
{
	CString olApc;
	int ilNumApts = omData.GetSize();
	for(int ilApt = 0; ilApt < ilNumApts; ilApt++)
	{
		APTDATA *prlApt = &omData[ilApt];
		olApc = FormatAptString(prlApt);
		if(!olApc.IsEmpty())
		{
			ropApc3Apc4Strings.Add(olApc);
		}
	}

	return ropApc3Apc4Strings.GetSize();
}

CString CedaAptData::GetTableName(void)
{
	return CString(pcmTableName);
}