// Class for Check In Counter Classes
#ifndef _CEDAVALDATA_H_
#define _CEDAVALDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>
#include <CedaBlkData.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct ValDataStruct
{
	long	Urno;		// Unique Record Number
	long	Urue;		// URNO of rule
	long	Uval;		// Foreign URNO
	char	Freq[8];	// Which days of the week is valid
	CTime	Vafr;		// Valid from
	CTime	Vato;		// Valid to
	char	Timf[5];	// Time from HHMM eg. valid each day from 1220 to 1510
	char	Timt[5];	// To to HHMM eg. valid each day from 1220 to 1510

	ValDataStruct(void) 
	{
		memset(this,'\0',sizeof(*this));
		Vafr = TIMENULL;
		Vato = TIMENULL;
	}


	ValDataStruct& ValDataStruct::operator=(const ValDataStruct& rhs)
	{
		if(&rhs != this)
		{
			Urno = rhs.Urno;
			Urue = rhs.Urue;
			Uval = rhs.Uval;
			strcpy(Freq, rhs.Freq);
			Vafr = rhs.Vafr;
			Vato = rhs.Vato;
			strcpy(Timf, rhs.Timf);
			strcpy(Timt, rhs.Timt);
		}
		return *this;
	}
};

typedef struct ValDataStruct VALDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaValData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <VALDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omUrueMap;
	CMapPtrToPtr omUvalMap;
	CString GetTableName(void);

// Operations
public:
	CedaValData();
	~CedaValData();

	CCSReturnCode ReadValData();
	void GetValListByUrue(long lpUrue, CCSPtrArray <VALDATA> &ropValList, bool bpReset=true);
	bool IsCurrentlyValid(long lpUrue, bool bpRecordNotFoundMeansValid = true);
	int GetBlockedTimes(long lpUval, const CTime &ropStartTime, const CTime &ropEndTime, CCSPtrArray <BLKBLOCKEDTIMES> &ropBlockedTimes);
	bool IsCompletelyBlocked(long lpUval, const CTime &ropStartTime, const CTime &ropEndTime);
	VALDATA *GetValByUval(long lpUval);
	VALDATA *GetValByUrno(long lpUrno);
	void ProcessValBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

private:
	VALDATA *AddValInternal(VALDATA &rrpVal);
	void DeleteValInternal(long lpUrno);
	void ClearAll();
};


extern CedaValData ogValData;
#endif _CEDAVALDATA_H_
