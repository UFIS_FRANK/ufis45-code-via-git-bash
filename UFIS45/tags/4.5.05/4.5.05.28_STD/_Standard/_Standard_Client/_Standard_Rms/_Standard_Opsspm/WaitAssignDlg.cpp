// WaitAssignDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <WaitAssignDlg.h>
#include <Ccsglobl.h>
#include <ccsddx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WaitAssignDlg dialog


WaitAssignDlg::WaitAssignDlg(CWnd* pParent /*=NULL*/,CString csText)
	: CDialog(WaitAssignDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(WaitAssignDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_csText = csText;

	ogCCSDdx.Register(this, BC_AFLEND_SBC ,CString("AUTOASSDLG"), CString("AFL END"), WaitAssignDlgCf);

//	// Nicht-modaler Dialog mu� per CDialog::Create() erzeugt werden
//	CDialog::Create(IDD, pParent);
}

WaitAssignDlg::~WaitAssignDlg()
{
}

void WaitAssignDlg::SetAssignCount(DWORD upCount)
{
	omTitle.Format(GetString(IDS_STRING61862),upCount);
}

void WaitAssignDlg::SetDeleteCount(DWORD upCount)
{
	omTitle.Format(GetString(IDS_STRING61863),upCount);
}

void WaitAssignDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(WaitAssignDlg)
	DDX_Control(pDX, IDC_ANIMATE1, m_oAnimate);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(WaitAssignDlg, CDialog)
	//{{AFX_MSG_MAP(WaitAssignDlg)
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// WaitAssignDlg message handlers
BOOL WaitAssignDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(GetString(IDS_STRING33081));
	CWnd *polWnd = GetDlgItem(1003); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(omTitle);
	}

	// Warte Mouse
	CWaitCursor olWait;
	
	// Laden des AVI Files und abspielen
	CAnimateCtrl *pCtrl=(CAnimateCtrl*)GetDlgItem(IDC_ANIMATE1);
	if (pCtrl)
	{
		pCtrl->Open(IDR_FILECOPY);
		pCtrl->Play(0,-1,-1);
	}

	// Default Text setzen
	if (m_csText == "")	
		m_csText = "Please Wait...";

	SetDlgItemText(IDC_TEXT,m_csText);
	
	return TRUE;  
}

//***************************************************************************************
//
//***************************************************************************************

void WaitAssignDlg::OnClose() 
{
	// Warte Mouse
	CDialog::OnClose();
}

void WaitAssignDlg::WaitAssignDlgCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
    WaitAssignDlg *polDlg = (WaitAssignDlg *)popInstance;

	if(ipDDXType == BC_AFLEND_SBC)
	{
		polDlg->ProcessAflEnd();
	}
}

void WaitAssignDlg::ProcessAflEnd(void)
{
	// check for a valid window handle and unregister immediately due to:
	// PRF 4556 - 2 AFL_END's are sent one immediately after the other
	// the first of which destroys the window and the second causes an assert
	if(::IsWindow(this->m_hWnd))
	{
	    ogCCSDdx.UnRegister(this, NOTUSED);
		CDialog::OnOK();
	}
}
