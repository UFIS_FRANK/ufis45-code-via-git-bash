//
// CedaPfcData.cpp - Function Stammdaten
//
#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaPfcData.h>
#include <BasicData.h>

void ProcessPfcCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaPfcData::CedaPfcData()
{                  
    BEGIN_CEDARECINFO(PFCDATA, PfcDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Fctc,"FCTC")
		FIELD_CHAR_TRIM(Fctn,"FCTN")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(PfcDataRecInfo)/sizeof(PfcDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&PfcDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	ogCCSDdx.Register((void *)this, BC_PFC_INSERT, CString("PFCDATA"), CString("Pfc changed"),ProcessPfcCf);
	ogCCSDdx.Register((void *)this, BC_PFC_UPDATE, CString("PFCDATA"), CString("Pfc changed"),ProcessPfcCf);
	ogCCSDdx.Register((void *)this, BC_PFC_DELETE, CString("PFCDATA"), CString("Pfc deleted"),ProcessPfcCf);

    // Initialize table names and field names
    strcpy(pcmTableName,"PFCTAB");
    pcmFieldList = "URNO,FCTC,FCTN";
}

void ProcessPfcCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaPfcData *)popInstance)->ProcessPfcBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void CedaPfcData::ProcessPfcBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlPfcData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlPfcData);
	PFCDATA rlPfc, *prlPfc = NULL;
	long llUrno = GetUrnoFromSelection(prlPfcData->Selection);
	GetRecordFromItemList(&rlPfc,prlPfcData->Fields,prlPfcData->Data);
	if(llUrno == 0L) llUrno = rlPfc.Urno;

	switch(ipDDXType)
	{
		case BC_PFC_INSERT:
		{
			if((prlPfc = AddPfcInternal(rlPfc)) != NULL)
			{
				ogCCSDdx.DataChanged((void *)this, PFC_INSERT, (void *)prlPfc);
			}
			break;
		}
		case BC_PFC_UPDATE:
		{
			if((prlPfc = GetPfcByUrno(llUrno)) != NULL)
			{
				GetRecordFromItemList(prlPfc,prlPfcData->Fields,prlPfcData->Data);
				//PrepareDataAfterRead(prlPfc);
				ogCCSDdx.DataChanged((void *)this, PFC_UPDATE, (void *)prlPfc);
			}
			break;
		}
		case BC_PFC_DELETE:
		{
			if((prlPfc = GetPfcByUrno(llUrno)) != NULL)
			{
				DeletePfcInternal(prlPfc->Urno);
				ogCCSDdx.DataChanged((void *)this, PFC_DELETE, (void *)prlPfc);
			}
			break;
		}
	}
}
 
CedaPfcData::~CedaPfcData()
{
	TRACE("CedaPfcData::~CedaPfcData called\n");
	ClearAll();
}

void CedaPfcData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omFctcMap.RemoveAll();
	omData.DeleteAll();
}

bool CedaPfcData::ReadPfcData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512] = "";
	if ((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaPfcData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		PFCDATA rlPfcData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlPfcData)) == RCSuccess)
			{
				AddPfcInternal(rlPfcData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaPfcData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(PFCDATA), pclWhere);
    return ilRc;
}


PFCDATA *CedaPfcData::AddPfcInternal(PFCDATA &rrpPfc)
{
	PFCDATA *prlPfc = new PFCDATA;
	*prlPfc = rrpPfc;
	omData.Add(prlPfc);
	omUrnoMap.SetAt((void *)prlPfc->Urno,prlPfc);
	omFctcMap.SetAt(prlPfc->Fctc,prlPfc);
	return prlPfc;
}

void CedaPfcData::DeletePfcInternal(long lpUrno)
{
	int ilNumPfcs = omData.GetSize();
	for(int ilPfc = (ilNumPfcs-1); ilPfc >= 0; ilPfc--)
	{
		if(omData[ilPfc].Urno == lpUrno)
		{
			omData.DeleteAt(ilPfc);
		}
	}
	omUrnoMap.RemoveKey((void *)lpUrno);
}

PFCDATA *CedaPfcData::GetPfcByUrno(long lpUrno)
{
	PFCDATA *prlPfc = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlPfc);
	return prlPfc;
}

PFCDATA* CedaPfcData::GetPfcByFctc(const char *pcpFctc)
{
	PFCDATA *prlPfc = NULL;
	omFctcMap.Lookup(pcpFctc, (void *&) prlPfc);
	return prlPfc;
}

CString CedaPfcData::GetTableName(void)
{
	return CString(pcmTableName);
}

//PRF 8999
void CedaPfcData::GetAllFunctions(CStringArray& ropTypes)
{
	ropTypes.RemoveAll();
	for (int i = 0; i < omData.GetSize(); i++)
	{
		ropTypes.Add(omData[i].Fctc);
	}
}