#ifndef _CEDACNFDATA_H_
#define _CEDACNFDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct CnfDataStruct
{
	long	Urno;		// Unique Record Number
	long	Utyp;		// URNO in CNTTAB of the conflict type
	CTime	Coti;		// Conflict Time
	int		Stat;		// Status of the conflict 0=New,1=Confirmed,2=Deleted
	char	Olva[33];	// Old Value
	long	Ourn;		// Object URNO eg. URNO of the flight causing this conflict
	CTime	Lstu;		// Last Update Time

	CnfDataStruct(void)
	{
		Urno = 0L;
		Utyp = 0L;
		Coti = TIMENULL;
		Stat = 0;
		strcpy(Olva,"");
		Ourn = 0L;
		Lstu = TIMENULL;
	}
};

typedef struct CnfDataStruct CNFDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaCnfData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <CNFDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omOurnMap;
	CString GetTableName(void);

// Operations
public:
	CedaCnfData();
	~CedaCnfData();

	CCSReturnCode ReadCnfData();
	bool AddCnfToOurnMap(CNFDATA &rrpCnf);
	bool GetCnfByOurn(long lpOurn, CCSPtrArray <CNFDATA> &ropOurnCnfList, bool bpReset=true);
	bool GetOldValueByOurn(long lpOurn, long lpUtyp, char *pcpOldValue);

private:
	void AddCnfInternal(CNFDATA &rrpCnfData);
	void ClearAll();
};


extern CedaCnfData ogCnfData;
#endif _CEDACNFDATA_H_
