// PrmDiagramFilterPage.h : header file
//
#ifndef _PRMDIAGRAMFILTERPAGE_H_
#define _PRMDIAGRAMFILTERPAGE_H_

#include <FilterPage.h>

/////////////////////////////////////////////////////////////////////////////
// PRMFilterPage dialog

class PRMDiagramFilterPage : public FilterPage
{
// Construction
public:
	PRMDiagramFilterPage();
	~PRMDiagramFilterPage();

	void	SetSorted(BOOL bpSorted);

// Dialog Data
	BOOL m_bEnabled;

	//{{AFX_DATA(PRMDiagramFilterPage)
	enum { IDD = IDD_FILTER_PAGE };
	CButton	m_AddButton;
	CButton	m_RemoveButton;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(PRMDiagramFilterPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(PRMDiagramFilterPage)
	afx_msg void OnFilteradd();
	afx_msg void OnFilterremove();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	bool bmSorted;

};

#endif // _PRMDIAGRAMFILTERPAGE_H_