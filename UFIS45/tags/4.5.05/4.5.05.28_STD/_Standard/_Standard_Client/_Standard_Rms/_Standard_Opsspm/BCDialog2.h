//{{AFX_INCLUDES()
#include "resource.h"
#include <afxmt.h>
//}}AFX_INCLUDES
#if !defined(AFX_BCDIALOG2_H__41AB7CAC_9B8F_4D28_BDF2_ECAA1C0FF5E1__INCLUDED_)
#define AFX_BCDIALOG2_H__41AB7CAC_9B8F_4D28_BDF2_ECAA1C0FF5E1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BCDialog2.h : header file
//


struct _StringDescriptor
{
        char* Value;                    /* string with values */
        long  AllocatedSize;    /* byte count from malloc or realloc */
        long  UsedLen;                  /* calulated value from strlen */
};
typedef struct _StringDescriptor STR_DESC;

/////////////////////////////////////////////////////////////////////////////
// BCDialog dialog
class CBlockingSocket;
typedef void (*TCPIP_TRAFFIC_LIGHT_CALLBACK)(void);

class BCDialog : public CDialog
{
// Construction
public:
	BCDialog(CWnd* pParent = NULL);   // standard constructor
	~BCDialog();
// Dialog Data
	//{{AFX_DATA(BCDialog)
	enum { IDD = IDD_BCDialog };
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(BCDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(BCDialog)
	afx_msg void OnOnBcBccomclientctrl1(LPCTSTR ReqId, LPCTSTR Dest1, LPCTSTR Dest2, LPCTSTR Cmd, LPCTSTR Object, LPCTSTR Seq, LPCTSTR Tws, LPCTSTR Twe, LPCTSTR Selection, LPCTSTR Fields, LPCTSTR Data, LPCTSTR BcNum, LPCTSTR Attachment, LPCTSTR Additional);
	afx_msg void OnPaint();
	virtual BOOL OnInitDialog();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSimulate();
	afx_msg void OnAboutButton();
    afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnClose();
	afx_msg void OnThreadLostBc(WPARAM wParam,LPARAM lParam);
	afx_msg LONG OnMessageLostBc(WPARAM wParam,LPARAM lParam);
	DECLARE_EVENTSINK_MAP()
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	void SetStartBroadcastCallback(TCPIP_TRAFFIC_LIGHT_CALLBACK pfpStartBroadcastCallback);
	void SetEndBroadcastCallback(TCPIP_TRAFFIC_LIGHT_CALLBACK pfpStartBroadcastCallback);

	static int	SpooledBroadcasts();
private:
	TCPIP_TRAFFIC_LIGHT_CALLBACK pfmStartBroadcastCallback;
	TCPIP_TRAFFIC_LIGHT_CALLBACK pfmEndBroadcastCallback;

public:
	long lmCurrWriteCount;
	long lmCurrFileNo;
	long lmMaxBCRecords; //0=No logging, any other value: The number of BCs for one file
	long lmBCSpoolTimer;

	BOOL m_amIConnected;
	CString omCurrFileName;
	char pcmHostName[512];
	CMapStringToPtr mapFilter;

	
	char* GetKeyItem(char *pcpResultBuff, long *plpResultSize, 
					 char *pcpTextBuff, char *pcpKeyWord, 
					 char *pcpItemEnd, bool bpCopyData);
	void Message(LPCTSTR lpszMessage);
	void SendBCData(char *pclBuf);
	void BC_To_Client();
	CString m_bCQueueID;

	CStringArray omSpooler;
	bool bmTimerIsSet;
	bool bmCedaSenderConnected;
	void ShutDownSocket(CString opWho);
	void Connect();
	static void SetFilter(char* strTable, char* strCommand, long bOwnBC, char* application);
	void TransmitFilter();
	void FireEvent(LPCTSTR ReqId, LPCTSTR Dest1, LPCTSTR Dest2, LPCTSTR Cmd, LPCTSTR Object, LPCTSTR Seq, LPCTSTR Tws, LPCTSTR Twe, LPCTSTR Selection, LPCTSTR Fields, LPCTSTR Data, LPCTSTR BcNum, LPCTSTR Attachment, LPCTSTR Additional);

	bool CreateThread();
	static UINT WorkerFunction( LPVOID pParam );
	CWinThread	*pomWorkerThread;
	static BOOL ReadMsgFromSock(FILE *fp,BCDialog *popDlg, CBlockingSocket &ropSock, STR_DESC *rpMsg, BOOL ipGetAck);
	CCriticalSection	omSection;
};

#define DATA_TOKEN "{=DATA=}"
#define DATA_ENDTOKEN "{=\\DATA=}"

class CAttachment
{
public:
	CAttachment(CString &ropAttachment);   // standard constructor

	bool bmAttachmentValid;
	CString omTableName;
	CString omFieldList;
	CStringArray omInsertDataList;
	CStringArray omUpdateDataList;
	CStringArray omDeleteDataList;

private:
	char *GetAttachmentData(char *pcpData, char *pcpToken, CStringArray &ropDataList, char *pcpRecSeparator);
	void Trace(char *pcpFormatList, ...);
};


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BCDIALOG2_H__41AB7CAC_9B8F_4D28_BDF2_ECAA1C0FF5E1__INCLUDED_)
