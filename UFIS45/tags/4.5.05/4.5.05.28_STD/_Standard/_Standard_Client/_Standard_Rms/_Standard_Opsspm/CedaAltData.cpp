// CedaAltData.cpp - Class for Airline Types
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaAltData.h>
#include <BasicData.h>
#include <CedaHaiData.h>

CedaAltData::CedaAltData()
{                  
    BEGIN_CEDARECINFO(ALTDATA, AltDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Alc2,"ALC2")
		FIELD_CHAR_TRIM(Alc3,"ALC3")
		FIELD_CHAR_TRIM(Cash,"CASH")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(AltDataRecInfo)/sizeof(AltDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&AltDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"ALTTAB");
    pcmFieldList = "URNO,ALC2,ALC3,CASH";
}
 
CedaAltData::~CedaAltData()
{
	TRACE("CedaAltData::~CedaAltData called\n");
	ClearAll();
}

void CedaAltData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omAlc2Map.RemoveAll();
	omAlc3Map.RemoveAll();
	omData.DeleteAll();
}

CCSReturnCode CedaAltData::ReadAltData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512];
    strcpy(pclWhere,"");

	if (ogBasicData.IsPrmHandlingAgentEnabled())
	{
		static char clFieldList[512];
//		strcpy(clFieldList,"URNO,ALC2,ALC3,CASH,FILT");
		strcpy(clFieldList,"URNO,ALC2,ALC3,CASH");
		pcmFieldList = clFieldList;
	}
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaAltData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		ALTDATA rlAltData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlAltData)) == RCSuccess)
			{
				AddAltInternal(rlAltData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaAltData Read %d Records (Rec Size %d) %s",omData.GetSize(),sizeof(ALTDATA), pclWhere);
    return ilRc;
}


void CedaAltData::AddAltInternal(ALTDATA &rrpAlt)
{
	ALTDATA *prlAlt = new ALTDATA;
	*prlAlt = rrpAlt;
	omData.Add(prlAlt);
	omUrnoMap.SetAt((void *)prlAlt->Urno,prlAlt);
	if(strlen(prlAlt->Alc2) > 0 && strcmp(prlAlt->Alc2," "))
	{
		omAlc2Map.SetAt(prlAlt->Alc2,prlAlt);
	}
	if(strlen(prlAlt->Alc3) > 0 && strcmp(prlAlt->Alc3," "))
	{
		omAlc3Map.SetAt(prlAlt->Alc3,prlAlt);
	}
}


ALTDATA* CedaAltData::GetAltByUrno(long lpUrno)
{
	ALTDATA *prlAlt = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlAlt);
	return prlAlt;
}

ALTDATA* CedaAltData::GetAltByAlc2(const char *pcpAlc2)
{
	ALTDATA *prlAlt = NULL;
	if(strlen(pcpAlc2) > 0 && strcmp(pcpAlc2," "))
	{
		omAlc2Map.Lookup(pcpAlc2, (void *&) prlAlt);
	}
	return prlAlt;
}

ALTDATA* CedaAltData::GetAltByAlc3(const char *pcpAlc3)
{
	ALTDATA *prlAlt = NULL;
	if(strlen(pcpAlc3) > 0 && strcmp(pcpAlc3," "))
	{
		omAlc3Map.Lookup(pcpAlc3, (void *&) prlAlt);
	}
	return prlAlt;
}

CString CedaAltData::FormatAlcString(const char *pcpAlc2, const char *pcpAlc3)
{
	CString olAlcString;
	ALTDATA *prlAlt = GetAltByAlc2(pcpAlc2);
	if(prlAlt == NULL)
	{
		prlAlt = GetAltByAlc3(pcpAlc3);
	}
	if(prlAlt != NULL)
	{
		olAlcString = FormatAlcString(prlAlt);
	}

	return olAlcString;
}

CString CedaAltData::FormatAlcString(ALTDATA *prpAlt)
{
	CString olAlcString;
	if(prpAlt != NULL)
	{
		olAlcString.Format("%s/%s",prpAlt->Alc2,prpAlt->Alc3);
	}

	return olAlcString;
}

int CedaAltData::GetAlc2Alc3Strings(CStringArray &ropAlc2Alc3Strings)
{
	CString olAlc;
	int ilNumAlts = omData.GetSize();
	for(int ilAlt = 0; ilAlt < ilNumAlts; ilAlt++)
	{
		ALTDATA *prlAlt = &omData[ilAlt];
		olAlc = FormatAlcString(prlAlt);
		if(!olAlc.IsEmpty())
		{
			ropAlc2Alc3Strings.Add(olAlc);
		}
	}

	return ropAlc2Alc3Strings.GetSize();
}

CString CedaAltData::GetTableName(void)
{
	return CString(pcmTableName);
}

bool CedaAltData::IsCashCustomer(const char *pcpAlt)
{
	// by default the customer pays by cash
	bool blIsCashCustomer = true;

	ALTDATA *prlAlt = GetAltByAlc3(pcpAlt);
	if(prlAlt == NULL)
	{
		prlAlt = GetAltByAlc2(pcpAlt);
	}

	if(prlAlt != NULL)
	{
		if(*prlAlt->Cash == ALT_BANK_CUSTOMER)
		{
			blIsCashCustomer = false;
		}
	}

	return blIsCashCustomer;
}

void CedaAltData::GetAirlineList(char *pcpHsna, char *pcpList)
{
	CString olAlc;
	int ilNumAlts = omData.GetSize();
	*pcpList = '\0';

	for(int ilAlt = 0; ilAlt < ilNumAlts; ilAlt++)
	{
		ALTDATA *prlAlt = &omData[ilAlt];
		HAIDATA *prlHai = ogHaiData.GetHaiByAltu(prlAlt->Urno);
		if (prlHai != NULL)
		{
			if(!strcmp(prlHai->Hsna,pcpHsna))
			{
				char clTmp[24];
				if (*pcpList == '\0')
				{
					sprintf(clTmp,"'%s'",prlAlt->Alc3);
				}
				else
				{
					sprintf(clTmp,",'%s'",prlAlt->Alc3);
				}
				strcat(pcpList,clTmp);
			}
		}
	}

}