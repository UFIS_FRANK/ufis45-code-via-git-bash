// BreakTable.h : header file
//
#ifndef __BREAKTABLE_H__
#define __BREAKTABLE_H__

#include <BreakTableViewer.h>
#include <CCSDragDropCtrl.h>


#define BREAKTABLE_CONFIRMJOB_MENUITEM 20

/////////////////////////////////////////////////////////////////////////////
// DemandTable dialog

class BreakTable : public CDialog
{
// Construction
public:
	BreakTable(CWnd* pParent = NULL);   // standard constructor
	BreakTable(BreakTableViewer *popViewer, CWnd* pParent = NULL);
	~BreakTable();
	void InitSelectViewComboBox(void);
	void UpdateComboBox(void);

private :
    CCSTable*				pomTable; // visual object, the table content
	BreakTableViewer		*pomViewer;
	int						m_nDialogBarHeight;
	CWnd*					pomParentWnd;
private:
	static void				BreakTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
	void					HandleGlobalDateUpdate();
	void					SetViewerDate();

// Dialog Data
	//{{AFX_DATA(BreakTable)
	enum { IDD = IDD_BREAKTABLE };
	CComboBox	m_Date;
	BOOL	m_DisplayNonOverlappingOnly;
	//}}AFX_DATA

 
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(BreakTable)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(BreakTable)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	virtual void OnCancel();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg LONG OnTableDragBegin(UINT wParam, LONG lParam);
    afx_msg LONG OnDragOver(UINT, LONG); 
    afx_msg LONG OnDrop(UINT, LONG); 
	afx_msg void OnClose();
	afx_msg void OnSelchangeDate();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
    afx_msg LONG OnTableRButtonDown(UINT wParam, LONG lParam);
	afx_msg LONG OnTableSelchange(UINT wParam,LONG lParam);
	afx_msg void OnDisplayNonOverlappingOnly();
	afx_msg void OnMenuStaffConfirm();
	afx_msg void UpdateView();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Drag-and-drop implementation
private:
	CCSDragDropCtrl m_DragDropTarget;
	long			imDropDemandUrno;
	CDWordArray		omSelectedBreakUrnos;
};

#endif //__BREAKTABLE_H__
