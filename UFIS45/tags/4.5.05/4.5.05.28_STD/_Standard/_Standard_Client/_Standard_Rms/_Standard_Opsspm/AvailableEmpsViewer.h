///////////////////////////////////////////////////
//

#ifndef _AVAILABLE_EMP_VIEWER_
#define _AVAILABLE_EMP_VIEWER_

#include <CCSPtrArray.h>
#include <CedaShiftData.h>
#include <CedaJobData.h>
#include <cviewer.h>
#include <table.h>

struct AVAILABLEEMPS_ADDITIONALS
{
	long			FlightUrno;
	char			FlightText[100];
	long			StaffJobUrno;
	char			StaffText[100];
	long			CCIJobUrno;
	char			JobText[100];
	char			CCILineText[100];
	CTime			StartTime;
	CTime			EndTime;
	CStringArray	Functions;	
	bool			TeamAllocation;

	AVAILABLEEMPS_ADDITIONALS(void)
	{
		FlightUrno		= 0;
		FlightText[0]	= '\0';
		StaffJobUrno	= 0;
		StaffText[0]	= '\0';
		CCIJobUrno		= 0;
		CCILineText[0]	= '\0';
		JobText[0]		= '\0';
		StartTime		= TIMENULL;
		EndTime			= TIMENULL;
		TeamAllocation	= false;
	}

	AVAILABLEEMPS_ADDITIONALS(const AVAILABLEEMPS_ADDITIONALS& ropRHS)
	{
		FlightUrno		= ropRHS.FlightUrno;
		strcpy(FlightText,ropRHS.FlightText);
		StaffJobUrno	= ropRHS.StaffJobUrno;
		strcpy(StaffText,ropRHS.StaffText);
		CCIJobUrno		= ropRHS.CCIJobUrno;
		strcpy(CCILineText,ropRHS.CCILineText);
		strcpy(JobText,ropRHS.JobText);
		StartTime		= ropRHS.StartTime;
		EndTime			= ropRHS.EndTime;
		for (int i = 0; i < ropRHS.Functions.GetSize(); i++)
		{
			Functions.Add(ropRHS.Functions[i]);
		}
		TeamAllocation	= ropRHS.TeamAllocation;
	}

	AVAILABLEEMPS_ADDITIONALS& operator=(const AVAILABLEEMPS_ADDITIONALS& ropRHS)
	{
		if (&ropRHS != this)
		{
			FlightUrno		= ropRHS.FlightUrno;
			strcpy(FlightText,ropRHS.FlightText);
			StaffJobUrno	= ropRHS.StaffJobUrno;
			strcpy(StaffText,ropRHS.StaffText);
			CCIJobUrno		= ropRHS.CCIJobUrno;
			strcpy(CCILineText,ropRHS.CCILineText);
			strcpy(JobText,ropRHS.JobText);
			StartTime		= ropRHS.StartTime;
			EndTime			= ropRHS.EndTime;
			for (int i = 0; i < ropRHS.Functions.GetSize(); i++)
			{
				Functions.Add(ropRHS.Functions[i]);
			}
			TeamAllocation	= ropRHS.TeamAllocation;
		}

		return *this;
	}
};

#define MAXFUNCLEN 200

struct AVAILABLE_EMPS_LINEDATA
{
	long Urno;
	char Tmid[12];		// Team ID
	char Peno[10];		// PK number
	char NonEmpFunction[MAXFUNCLEN]; // function for the shift/group/delegation/deviation
	char EmpFunctions[MAXFUNCLEN]; // Employee functions from the basicdata
	char EmpPermits[MAXFUNCLEN];
	char Rank[42];
	char Lnam[42];		// long name
	char Pool[20];		// Pool name
	char Sfca[20];		// Shift Code Actual
	long PoolJobUrno;	// URNO of the pool job for this emp

	int Weight; // See ogBasicData.GetAssignmentSuitabiltyWeighting()
	bool OverlapsBreak;
	bool OverlapConflict;
	bool OutsideShiftConflict;

	CTime Avfr;			// start time
	CTime Avto;			// end time

	AVAILABLE_EMPS_LINEDATA(void)
	{
		memset(this,'\0',sizeof(*this));
		Avfr = TIMENULL;
		Avto = TIMENULL;
		OverlapsBreak = false;
		OverlapConflict = false;
		OutsideShiftConflict = false;
	}
};

class AvailableEmpViewer : public CViewer
{
public:
	AvailableEmpViewer(CCSPtrArray<AVAILABLE_EMPS_LINEDATA> *popAvailableEmps);
	~AvailableEmpViewer();

	CCSPtrArray<AVAILABLE_EMPS_LINEDATA> *pomAvailableEmps;
    void Attach(CTable *popAttachWnd);

private:
    void MakeLines();
	void MakeLine(AVAILABLE_EMPS_LINEDATA  *prpAvailable);
	int CreateLine(AVAILABLE_EMPS_LINEDATA*prpAvailable);
	int CompareAvailable(AVAILABLE_EMPS_LINEDATA *prpAvailable1, AVAILABLE_EMPS_LINEDATA *prpAvailable2);
	bool bmUseAllShiftCodes, bmUseAllRanks, bmUseAllPermits, bmUseAllTeams, bmUseAllPools, bmEmptyTeamSelected;
	CMapStringToPtr omCMapForShiftCode, omCMapForRank, omCMapForPermits, omCMapForTeam, omCMapForPool;
	void PrepareFilter(void);
	bool IsPassFilter(AVAILABLE_EMPS_LINEDATA *prpLine);
	bool IsPassFilter(const char *pcpPoolId, const char *pcpTeamId,	const char *pcpShiftCode, const char *pcpRank, bool bpIsAbsent, CStringArray &ropPermits);

public:
	void DeleteAll();
	void DeleteLine(int ipLineno);

public:
	void UpdateView(const char *pcpViewName);
	void UpdateDisplay();
	CString Format(AVAILABLE_EMPS_LINEDATA *prpLine);
	void ProcessShiftChange(SHIFTDATA *prpShift);
	void ProcessShiftChange(JOBDATA *prpJob);
	int GetLineCount();

// Attributes used for filtering condition
private:
    int omGroupBy;              // enumerated value -- define in "Sfviewer.cpp" (use first sorting)
    CStringArray omSortOrder;     // array of enumerated value -- defined in "FJviewer.cpp"
    CString omDate;
    CTime omStartTime;
	CTime omEndTime;
// Attributes
private:
    CTable *pomTable;
    CCSPtrArray<AVAILABLE_EMPS_LINEDATA> omLines;

// Methods which handle changes (from Data Distributor)
public:

};
#endif