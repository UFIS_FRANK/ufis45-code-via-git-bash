// FilterPage.h : header file
//
#ifndef _FILTERPAGE_H_
#define _FILTERPAGE_H_

/////////////////////////////////////////////////////////////////////////////
// FilterPage dialog

class FilterPage : public CPropertyPage
{
	DECLARE_DYNCREATE(FilterPage)

// Construction
public:
	FilterPage();
	~FilterPage();

	char pcmCaption[100];
	void SetCaption(const char *pcpCaption);


// Dialog Data
	CStringArray omPossibleItems;
	CStringArray omSelectedItems;
	bool bmSelectAllEnabled;   //hag
	CString omAllString;		//hag

	bool bmGroupEnabled;//wny

	//{{AFX_DATA(FilterPage)
	enum { IDD = IDD_FILTER_PAGE };
	CListBox	m_List2;
	CListBox	m_List1;
	CComboBox   m_ComboBox1;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(FilterPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(FilterPage)
	afx_msg void OnFilterAdd();
	afx_msg void OnFilterAddAll();
	afx_msg void OnFilterRemove();
	afx_msg void OnFilterRemoveAll();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};
#endif // _FILTERPAGE_H_
