#if !defined(AFX_OtherColoursDetailDlg_H__4CCF4E00_F83A_465A_B6D8_E2536CF28FC2__INCLUDED_)
#define AFX_OtherColoursDetailDlg_H__4CCF4E00_F83A_465A_B6D8_E2536CF28FC2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// OtherColoursDetailDlg.h : header file
//
#include <CCSButtonctrl.h>

/////////////////////////////////////////////////////////////////////////////
// COtherColoursDetailDlg dialog

class COtherColoursDetailDlg : public CDialog
{
// Construction
public:
	COtherColoursDetailDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(COtherColoursDetailDlg)
	enum { IDD = IDD_OTHERCOLOURSDETAIL_DLG };
	CString	m_ColourTitle;
	CString	m_CfgNameTitle;
	CString	m_CfgName;
	BOOL	m_Enabled;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COtherColoursDetailDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(COtherColoursDetailDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnColour();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	COLORREF omColour;
	CCSButtonCtrl omColourButton;

public:
	void SetData(CString opCfgn, bool bpEnabled, COLORREF opColour);
	void GetData(CString &ropCfgn, bool &rbpEnabled, COLORREF &ropColour);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OtherColoursDetailDlg_H__4CCF4E00_F83A_465A_B6D8_E2536CF28FC2__INCLUDED_)
