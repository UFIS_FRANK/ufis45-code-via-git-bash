// AssignConflictDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <AssignConflictDlg.h>
#include <Ccsglobl.h>

//#ifdef _DEBUG
//#define new DEBUG_NEW
//#undef THIS_FILE
//static char THIS_FILE[] = __FILE__;
//#endif

/////////////////////////////////////////////////////////////////////////////
// CAssignConflictDlg dialog


CAssignConflictDlg::CAssignConflictDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAssignConflictDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAssignConflictDlg)
	m_Title = _T("");
	m_Choice = -1;
	//}}AFX_DATA_INIT
}


void CAssignConflictDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAssignConflictDlg)
	DDX_Control(pDX, IDC_CONFLICTTYPE, m_ConflictList);
	DDX_Control(pDX, IDC_DEMANDNAME, m_DemandList);
	DDX_Text(pDX, IDC_TITLE, m_Title);
	DDX_Radio(pDX, IDC_OK, m_Choice);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAssignConflictDlg, CDialog)
	//{{AFX_MSG_MAP(CAssignConflictDlg)
	ON_LBN_SELCHANGE(IDC_DEMANDNAME, OnSelchangeDemandname)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAssignConflictDlg message handlers

BOOL CAssignConflictDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(GetString(IDS_STRING32832)); 

	CWnd *polWnd = GetDlgItem(IDC_CONFLICTTYPE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32833));
	}
	polWnd = GetDlgItem(IDC_OK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32834));
	}
	polWnd = GetDlgItem(IDC_CANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32835));
	}
	polWnd = GetDlgItem(IDC_ABORT); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32836));
	}
	polWnd = GetDlgItem(IDOK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61697));
	}


	m_Choice = 0;
	UpdateData(false);

	int ilNumDemands = omDemands.GetSize();
	if(ilNumDemands > 0)
	{
		for(int ilD = 0; ilD < ilNumDemands; ilD++)
		{
			m_DemandList.AddString(omDemands[ilD]);
		}
		imSelectedDemandIndex = 0;
		m_DemandList.SetSel(0);
		m_ConflictList.SetWindowText(omConflicts[0]);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CAssignConflictDlg::OnOK() 
{
	if(UpdateData(true))
	{
		imSelectedDemandIndex = m_DemandList.GetCurSel();
		if(imSelectedDemandIndex == LB_ERR || imSelectedDemandIndex < 0 || imSelectedDemandIndex >= omConflicts.GetSize())
		{
			imSelectedDemandIndex = 0;
		}

		if(m_Choice == 1)
		{
			CDialog::EndDialog(IDCANCEL);
		}
		else if(m_Choice == 2)
		{
			CDialog::EndDialog(IDABORT);
		}
		else
		{
			CDialog::OnOK();
		}
	}
}

void CAssignConflictDlg::OnSelchangeDemandname() 
{
	imSelectedDemandIndex = m_DemandList.GetCurSel();
	if(imSelectedDemandIndex != LB_ERR && imSelectedDemandIndex >= 0 && imSelectedDemandIndex < omConflicts.GetSize())
	{
		m_ConflictList.SetWindowText(omConflicts[imSelectedDemandIndex]);
	}
}
