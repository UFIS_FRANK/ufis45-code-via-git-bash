// WorkGroupBackgroundBars.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <CCSGlobl.h>
#include <WorkGroupBackgroundBars.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWorkGroupBackgroundBars

CWorkGroupBackgroundBars::CWorkGroupBackgroundBars()
{
	omWorkGroupBkColours[0].Colour = new CBrush(RGB(0,202,202));
	omWorkGroupBkColours[1].Colour = new CBrush(RGB(118,123,184));
	omWorkGroupBkColours[2].Colour = new CBrush(ORANGE);
	omWorkGroupBkColours[3].Colour = new CBrush(YELLOW);
	omWorkGroupBkColours[4].Colour = new CBrush(WHITE);
	omWorkGroupBkColours[5].Colour = new CBrush(FUCHSIA);
	omWorkGroupBkColours[6].Colour = new CBrush(GREEN);
	omWorkGroupBkColours[7].Colour = new CBrush(RED);
	omWorkGroupBkColours[8].Colour = new CBrush(TEAL);
	omWorkGroupBkColours[9].Colour = new CBrush(PURPLE);
	omWorkGroupBkColours[10].Colour = new CBrush(LIME);
	omWorkGroupBkColours[11].Colour = new CBrush(MAROON);
	omWorkGroupBkColours[12].Colour = new CBrush(OLIVE);
	omWorkGroupBkColours[13].Colour = new CBrush(NAVY);
	omWorkGroupBkColours[14].Colour = new CBrush(BLUE);
	omWorkGroupBkColours[15].Colour = new CBrush(AQUA);
}

CWorkGroupBackgroundBars::~CWorkGroupBackgroundBars()
{
	for(int ilLc = 0; ilLc < NUMWGBKCOLOURS; ilLc++)
	{
		delete omWorkGroupBkColours[ilLc].Colour;
	}
}

CBrush *CWorkGroupBackgroundBars::GetWorkGroupBkColour(long lpUlnk)
{
	CBrush *prlWorkGroupColour = omWorkGroupBkColours[NUMWGBKCOLOURS-1].Colour;

	for(int ilWorkGroup = 0; ilWorkGroup < NUMWGBKCOLOURS; ilWorkGroup++)
	{
		WGBKCOLOURS *prlWorkGroupBkColour = &omWorkGroupBkColours[ilWorkGroup];
		if(prlWorkGroupBkColour->Ulnk == lpUlnk)
		{
			prlWorkGroupColour = prlWorkGroupBkColour->Colour;
			break;
		}
		else if(prlWorkGroupBkColour->Ulnk == 0L)
		{
			prlWorkGroupColour = prlWorkGroupBkColour->Colour;
			prlWorkGroupBkColour->Ulnk = lpUlnk;
			break;
		}
	}

	return prlWorkGroupColour;
}

