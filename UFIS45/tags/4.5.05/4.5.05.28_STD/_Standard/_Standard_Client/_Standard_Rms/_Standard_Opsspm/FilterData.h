////////////////////////////////////////////////////
// FilterData.h
// 
// To store defined Filters for reuse
// 
// Author: MWO
//

#ifndef _FILTER_DATA_
#define _FILTER_DATA_

enum {PS_NONE=-1,PS_FLIGHTS, PS_EMPLOYEES};

struct EMPSEARCHFILTER
{
	char Name[84];
	char Shift[15];
	char Rank[15];
	CTime Date;
	EMPSEARCHFILTER(void)
	{memset(this,'\0',sizeof(*this)); Date=-1;}
};

struct FLIGHTSEARCHFILTER
{
	char Airline[4];
	char Flno[6];
	CTime Date;
	FLIGHTSEARCHFILTER(void)
	{memset(this,'\0',sizeof(*this)); Date=-1;}
};

struct SEARCHFILTER
{
	EMPSEARCHFILTER		rlEmpFilterData;
	FLIGHTSEARCHFILTER	rlFlightFilterData;
};

extern SEARCHFILTER ogSearchFilter;

#endif  //_FILTER_DATA_