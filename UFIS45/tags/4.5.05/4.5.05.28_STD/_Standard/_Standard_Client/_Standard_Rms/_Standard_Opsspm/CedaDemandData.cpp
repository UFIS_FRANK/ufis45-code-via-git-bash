// CedaDemandData.cpp - Read and write demand data 
//
//
// Written by:
// Damkerng Thammathakerngkit   May 5, 1996
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <OpssPm.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <BasicData.h>
#include <CedaDemandData.h>
#include <CedaJobData.h>
#include <CedaJodData.h>
#include <CedaFlightData.h>
#include <CedaShiftData.h>
#include <CedaEmpData.h>
#include <conflict.h>
#include <DataSet.h>
#include <CedaRudData.h>
#include <CedaRueData.h>
#include <AllocData.h>
#include <CedaCcaData.h>
#include <waitdlg.h>
#include <CedaTplData.h> //Singapore
#include <PrmConfig.h>

//igu
#include <ButtonList.h>

extern CCSDdx ogCCSDdx;

void  ProcessDemandCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaDemandData::CedaDemandData()
{
    // Create an array of CEDARECINFO for DEMANDDATA
    BEGIN_CEDARECINFO(DEMANDDATA, DemandDataRecInfo)
        FIELD_LONG(Urno,"URNO")
        FIELD_LONG(Ouri,"OURI")
        FIELD_LONG(Ouro,"OURO")
		FIELD_CHAR_TRIM(Obty,"OBTY")
        FIELD_LONG(Urud,"URUD")
        FIELD_LONG(Udgr,"UDGR")
        FIELD_LONG(Ucon,"UCON")
        FIELD_CHAR_TRIM(Alid,"ALID")
        FIELD_CHAR_TRIM(Aloc,"ALOC")
        FIELD_DATE(Debe,"DEBE")
        FIELD_DATE(Deen,"DEEN")
        FIELD_CHAR_TRIM(Dety,"DETY")
        FIELD_CHAR(Flgs,' ',"FLGS")
		FIELD_LONG(Uref,"UREF")
        FIELD_DATE(Cdat,"CDAT")
        FIELD_CHAR_TRIM(Usec,"USEC")
        FIELD_DATE(Lstu,"LSTU")
        FIELD_CHAR_TRIM(Useu,"USEU")
        FIELD_CHAR_TRIM(Rety,"RETY")
		FIELD_LONG(Utpl,"UTPL")
		FIELD_LONG(Uses,"USES")
		FIELD_LONG(Uprm,"UPRM")
		FIELD_LONG(Filt,"FILT")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(DemandDataRecInfo)/sizeof(DemandDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&DemandDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	omFlightDemand.Format("%c%c%c", TURNAROUND_DEMAND, INBOUND_DEMAND, OUTBOUND_DEMAND);
    
	// Initialize table names and field names

    char pcmConfigPath[80];

	// omDemandsWithAzatabRecords: used for searching for demand types which have records in AZATAB
	omDemandsWithAzatabRecords.Format("%c%c%c%c",P05_DEMAND,P05RES_DEMAND,CAS_DEMAND,MANUAL_THIRDPARTY_DEMAND);

    if (getenv("CEDA") == NULL)
        strcpy(pcmConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pcmConfigPath, getenv("CEDA"));

	strcpy(pcmTableName,"DEMTAB");

	*pcmFields = '\0';
    pcmFieldList = pcmFields;

	ogCCSDdx.Register((void *)this,BC_DEMAND_NEW,CString("DEMANDDATA"), CString("Demand inserted"),ProcessDemandCf);
	ogCCSDdx.Register((void *)this,BC_DEMAND_CHANGE,CString("DEMANDDATA"), CString("Demand changed"),ProcessDemandCf);
	ogCCSDdx.Register((void *)this,BC_DEMAND_DELETE,CString("DEMANDDATA"), CString("Demand deleted"),ProcessDemandCf);
	ogCCSDdx.Register((void *)this,BC_DEMAND_REL,CString("DEMANDDATA"), CString("Demand Release"),ProcessDemandCf);
	ogCCSDdx.Register((void *)this,BC_DEMAND_UPD,CString("DEMANDDATA"), CString("Demand Update"),ProcessDemandCf);

	omUrnoMap.InitHashTable( 2001 );
	omInboundFlightMap.InitHashTable( 2001 );
	omOutboundFlightMap.InitHashTable( 2001 );
	omUrefMap.InitHashTable( 2001 );
	omUprmMap.InitHashTable( 2001 );
	omData.SetSize(0,1480);	
	omUdemAlidList.Empty();

	omTurnaroundDemandList += CString("|") + TURNAROUND_DEMAND + CString("|");
	omTurnaroundDemandList += CString("|") + P05_DEMAND + CString("|");
	omTurnaroundDemandList += CString("|") + P05RES_DEMAND + CString("|");
	omTurnaroundDemandList += CString("|") + CAS_DEMAND + CString("|");
	omLoadFormat.Empty();

	bmDontSendDdx = false;
	bmIsSinApron1 = false; //Singapore
	bmIsSinApron2 = false; //Singapore
}


CedaDemandData::~CedaDemandData()
{
	ogCCSDdx.UnRegister(this, NOTUSED);
	ClearAll();
}

bool CedaDemandData::ClearAll()
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();

	POSITION rlPos;
	CMapPtrToPtr *polSingleMap;

	for(rlPos = omInboundFlightMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omInboundFlightMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& )polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	omInboundFlightMap.RemoveAll();

	for(rlPos = omOutboundFlightMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omOutboundFlightMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& )polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	omOutboundFlightMap.RemoveAll();

	for(rlPos = omUrefMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omUrefMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& )polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	omUrefMap.RemoveAll();

	for(rlPos = omUprmMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omUprmMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& )polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	omUprmMap.RemoveAll();

	for(rlPos = omUsesMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omUsesMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& )polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	omUsesMap.RemoveAll();

	for(rlPos = omAlidMap.GetStartPosition(); rlPos != NULL; )
	{
		CString olAlid;
		omAlidMap.GetNextAssoc(rlPos, olAlid,(void *& )polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	omAlidMap.RemoveAll();
    return true;
}

 bool CedaDemandData::ReadAllDemands(CTime lpStartTime, CTime lpEndTime,const CDWordArray& ropTplUrnos)
{
	bool blRet = true;
	CString olTplUrnos, olTmp;
	CString olExtraTpls;

	omStartTime = lpStartTime;
	omEndTime = lpEndTime; 


	omUtplFilterMap.RemoveAll();

	for (int i = 0; i < ropTplUrnos.GetSize(); i++)
	{
		omUtplFilterMap.SetAt((void *)ropTplUrnos[i],NULL);
		if(i != 0)
			olTplUrnos += ",";
		olTmp.Format("'%d'", ropTplUrnos[i]);
		olTplUrnos += olTmp;
	}

	///
	bmIsSinApron1 = ogBasicData.IsSinApron1();
	bmIsSinApron2 = ogBasicData.IsSinApron2();
	if(bmIsSinApron1 == true)
	{
		TPLDATA* polTplData;
		for(int i = 0; i< ogTplData.omData.GetSize(); i++)
		{
			polTplData = &ogTplData.omData.GetAt(i);
			if(strcmp(polTplData->Tnam,CCSBasicData::TPL_APRON2) == 0)
			{
				omUtplFilterMap.SetAt((void *)polTplData->Urno,NULL);				
				olTmp.Format(",'%d'", polTplData->Urno);
				olTplUrnos += olTmp;
			}
		}
	}
	else if(bmIsSinApron2 == true)
	{
		TPLDATA* polTplData;
		for(int i = 0; i< ogTplData.omData.GetSize(); i++)
		{
			polTplData = &ogTplData.omData.GetAt(i);
			if(strcmp(polTplData->Tnam,CCSBasicData::TPL_APRON1) == 0)
			{
				omUtplFilterMap.SetAt((void *)polTplData->Urno,NULL);
				olTmp.Format(",'%d'", polTplData->Urno);
				olTplUrnos += olTmp;
			}
		}
	}

	///

	CTime olStartMinusOffset = omStartTime - CTimeSpan(2,0,0,0);
//	CTime olJodStart = omStartTime - CTimeSpan(1,0,0,0), olJodEnd = omEndTime + CTimeSpan(1,0,0,0);


	if ( omLoadFormat == "DEM" )
	{
		if (ogBasicData.IsPrmHandlingAgentEnabled()) 
		{
			sprintf(cgSelection, "WHERE DEBE between '%s' and '%s' and DEEN > '%s' and UTPL in (%s)  AND FILT = '%s'  ",
				olStartMinusOffset.Format("%Y%m%d%H%M%S"), 
				omEndTime.Format("%Y%m%d%H%M%S"),omStartTime.Format("%Y%m%d%H%M%S"), olTplUrnos ,
					ogPrmConfig.getHandlingAgentShortName());
		}
		else
		{
			sprintf(cgSelection, "WHERE DEBE between '%s' and '%s' and DEEN > '%s' and UTPL in (%s)", olStartMinusOffset.Format("%Y%m%d%H%M%S"), omEndTime.Format("%Y%m%d%H%M%S"),omStartTime.Format("%Y%m%d%H%M%S"), olTplUrnos );
		}
		ClearAll();
		blRet = ReadDemands(cgSelection, false);
		if (ogBasicData.bmDelegateFlights)
		{
			char pclTmpTplUrnoText1[512];
			char pclTmpTplUrnoText2[512];

			*pclTmpTplUrnoText2 = '\0';
			*pclTmpTplUrnoText1 = '\0';

			if (*pclTmpTplUrnoText1 != '\0')
			{
				CString olConfigFileName = ogBasicData.GetConfigFileName();
				GetPrivateProfileString(pcgAppName, "APRON1_UTPL", "",pclTmpTplUrnoText1, sizeof pclTmpTplUrnoText1, olConfigFileName);
				GetPrivateProfileString(pcgAppName, "APRON2_UTPL", "",pclTmpTplUrnoText2, sizeof pclTmpTplUrnoText2, olConfigFileName);
				
				for (i = 0; i < ropTplUrnos.GetSize(); i++)
				{
					char pclTmpTplUrno[24];

						sprintf(pclTmpTplUrno,"%ld",ropTplUrnos[i]);
						if (strstr(pclTmpTplUrnoText1,pclTmpTplUrno) != NULL)
						{
							olExtraTpls = pclTmpTplUrnoText2;
							break;
						}
						else if (strstr(pclTmpTplUrnoText2,pclTmpTplUrno) != NULL)
						{
							olExtraTpls = pclTmpTplUrnoText1;
							break;
						}
				}			

				// make where clause for
				//		omWhere.Format("WHERE DEBE between '%s' and '%s' and UTPL in (%s)", olJodStart.Format("%Y%m%d%H%M%S"), olJodEnd.Format("%Y%m%d%H%M%S"), olTplUrnos );
			sprintf(cgSelection, "WHERE DEBE between '%s' and '%s' and DEEN > '%s' and UTPL in ('%s')", olStartMinusOffset.Format("%Y%m%d%H%M%S"), omEndTime.Format("%Y%m%d%H%M%S"),omStartTime.Format("%Y%m%d%H%M%S"), olExtraTpls );

			blRet = ReadDemandsForRestrictedJobs(cgSelection, false);
			}
		}
	}
	else if ( omLoadFormat == "RUD" )
	{
		sprintf(cgSelection, "WHERE DEBE between '%s' and '%s' and DEEN > '%s' and URUD in (select URNO from RUDTAB WHERE UTPL in (%s))", 
				olStartMinusOffset.Format("%Y%m%d%H%M%S"), omEndTime.Format("%Y%m%d%H%M%S"),omStartTime.Format("%Y%m%d%H%M%S"), olTplUrnos);
//		omWhere.Format("WHERE DEBE between '%s' and '%s' and DEEN > '%s' and URUD in (select URNO from RUDTAB WHERE UTPL in (%s))", 
//							olJodStart.Format("%Y%m%d%H%M%S"), olJodEnd.Format("%Y%m%d%H%M%S"), olTplUrnos);
		ClearAll();
		blRet = ReadDemands(cgSelection, true);
	}
	else
	{
		sprintf(cgSelection, "WHERE DEBE between '%s' and '%s' and DEEN > '%s'", olStartMinusOffset.Format("%Y%m%d%H%M%S"), omEndTime.Format("%Y%m%d%H%M%S"),omStartTime.Format("%Y%m%d%H%M%S"));
		ClearAll();
		blRet = ReadDemands(cgSelection, false);
	}
//	if(omWhere.IsEmpty())
//		omWhere = pclWhere;

	return blRet;
}

void CedaDemandData::CreateFieldList()
{
	strcpy(pcmFields,"URNO,OURI,OURO,OBTY,URUD,UDGR,UCON,ALID,ALOC,DEBE,DEEN,DETY,FLGS,UREF,CDAT,USEC,LSTU,USEU,RETY,UTPL");
	if(ogBasicData.bmDisplayEquipment)
	{
		strcat(pcmFields,",USES");
	}
	if (bgIsPrm)
	{
		strcat(pcmFields,",UPRM");
	}
	if(ogBasicData.DoesFieldExist("DEM", "FILT") )
	{
		CString olField;
		strcat(pcmFields,",FILT");
	}

	pcmFieldList = pcmFields;

}
bool CedaDemandData::ReadDemandsForRestrictedJobs(char *pcpWhere , bool useRudHind /*= false*/)
{
	char pclCom[10] = "RT";
	if (*pcmFields == '\0')
	{
		CreateFieldList();
	}
	if(useRudHind == true)
	{
		if (CedaAction2(pclCom, pcpWhere, NULL, pcgDataBuf, "BUF1", false, 0, true, NULL, NULL, "/*+ INDEX(RUDTAB RUDTAB_UTPL) */") == RCFailure)
		{
			ogBasicData.LogCedaError("CedaDemandData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pcpWhere);
			return RCFailure;
		}
	}
	else
	{
		if (CedaAction2(pclCom, pcpWhere) == RCFailure)
		{
			ogBasicData.LogCedaError("CedaDemandData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pcpWhere);
			return RCFailure;
		}
	}
	int ilCount = 0;
	bool blRc = true;
	for(int ilLc = 0; blRc; ilLc++)
	{
		DEMANDDATA *prlDemand = new DEMANDDATA;
		if ((blRc = GetBufferRecord2(ilLc,prlDemand)) == RCSuccess && prlDemand->Ucon == 0L)
		{
			if(AddRestrictedDemandInternal(prlDemand))
			{
				// no conflict check here for restriced demands
				/***
				if(popDemUrnosAdded != NULL)
				{
					popDemUrnosAdded->Add((DWORD) prlDemand->Urno);
				}
				if(bpSendDdx)
				{
					ogConflicts.CheckConflicts(*prlDemand,FALSE,TRUE,TRUE,FALSE);
					ogCCSDdx.DataChanged((void *)this,DEMAND_NEW,(void *)prlDemand);
				}
				***/
				ilCount++;
			}
			else
			{
				delete prlDemand;
			}
		}
		else
		{
			delete prlDemand;
		}
	}

	ogBasicData.Trace("CedaDemData Read %d Records (Rec Size %d) %s ",ilCount, sizeof(DEMANDDATA), pcpWhere);

	return true;
}

bool CedaDemandData::ReadDemands(char *pcpWhere , bool useRudHind /*= false*/, bool bpSendDdx /*= false*/, CDWordArray *popDemUrnosAdded /*= NULL*/)
{
	char pclCom[10] = "RT";


	if (*pcmFields == '\0')
	{
		CreateFieldList();
	}


	if(useRudHind == true)
	{
		if (CedaAction2(pclCom, pcpWhere, NULL, pcgDataBuf, "BUF1", false, 0, true, NULL, NULL, "/*+ INDEX(RUDTAB RUDTAB_UTPL) */") == RCFailure)
		{
			ogBasicData.LogCedaError("CedaDemandData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pcpWhere);
			return RCFailure;
		}
	}
	else
	{
		if (CedaAction2(pclCom, pcpWhere) == RCFailure)
		{
			ogBasicData.LogCedaError("CedaDemandData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pcpWhere);
			return RCFailure;
		}
	}
	int ilCount = 0;
	bool blRc = true;
	for(int ilLc = 0; blRc; ilLc++)
	{
		DEMANDDATA *prlDemand = new DEMANDDATA;
		if ((blRc = GetBufferRecord2(ilLc,prlDemand)) == RCSuccess && prlDemand->Ucon == 0L)
		{
			if(AddDemandInternal(prlDemand))
			{
				if(popDemUrnosAdded != NULL)
				{
					popDemUrnosAdded->Add((DWORD) prlDemand->Urno);
				}
				if(bpSendDdx)
				{
					ogConflicts.CheckConflicts(*prlDemand,FALSE,TRUE,TRUE,FALSE);
					ogCCSDdx.DataChanged((void *)this,DEMAND_NEW,(void *)prlDemand);
				}
				ilCount++;
			}
			else
			{
				delete prlDemand;
			}
		}
		else
		{
			delete prlDemand;
		}
	}

	ogBasicData.Trace("CedaDemData Read %d Records (Rec Size %d) %s ",ilCount, sizeof(DEMANDDATA), pcpWhere);

	return true;
}


bool CedaDemandData::AddDemandInternal(DEMANDDATA *prpDemand)
{
	if(prpDemand != NULL)
	{
		if(GetDemandByUrno(prpDemand->Urno) != NULL) // demand already in mem - can happen when checking for missing demands
			return false;

		PrepareDataAfterRead(prpDemand);

		// omStartTime and omEndTime are in UTC
		if(!IsOverlapped(prpDemand->Debe, prpDemand->Deen, omStartTime, omEndTime) && ogFlightData.GetFlightByUrno(prpDemand->Ouri) == NULL && ogFlightData.GetFlightByUrno(prpDemand->Ouro) == NULL)
			return false;

		ConvertDatesToLocal(prpDemand);

		RUDDATA *prlRud = ogRudData.GetRudByUrno(prpDemand->Urud);
		if(prlRud == NULL)
		{
//			ogBasicData.Trace("CedaDemandData::AddDemandInternal() Error RUD not found for a demand DEM.URNO <%ld> DEM.URUD <%ld>\n",prpDemand->Urno,prpDemand->Urud);
			return false;
		}
		else
		{
			RUEDATA *prlRue = ogRueData.GetRueByUrno(prlRud->Urue);
			if (prlRue == NULL)
			{
//				ogBasicData.Trace("CedaDemandData::AddDemandInternal() Error RUE not found for a demand DEM.URNO <%ld> DEM.URUE <%ld>\n",prpDemand->Urno,prpDemand->Urue);
				return false;
			}
			else
			{
				void *p;
				if (!omUtplFilterMap.Lookup((void*)prlRue->Utpl,p))
					return false;
			}
		}

		///
		if(bmIsSinApron1 == true || bmIsSinApron2 == true)
		{
			if(prpDemand->Ouri != 0L && prpDemand->Dety[0] != '0')
			{
				FLIGHTDATA* polFlightDataIb = ogFlightData.GetFlightByUrno(prpDemand->Ouri);
				if(polFlightDataIb != NULL)
				{
					TPLDATA* polTplData = ogTplData.GetTplByUrno(prpDemand->Utpl);
					if((bmIsSinApron1 == true && strcmp(polTplData->Tnam,CCSBasicData::TPL_APRON1) != 0)
						 || (bmIsSinApron2 == true && strcmp(polTplData->Tnam,CCSBasicData::TPL_APRON2) != 0))
					{						
						return false;
					}
				}
			}
			else if(prpDemand->Ouro != 0L && prpDemand->Dety[0] != '0')
			{
				FLIGHTDATA* polFlightDataOb = ogFlightData.GetFlightByUrno(prpDemand->Ouro);
				if(polFlightDataOb != NULL)
				{					
					TPLDATA* polTplData = ogTplData.GetTplByUrno(prpDemand->Utpl);
					if((bmIsSinApron1 == true && strcmp(polTplData->Tnam,CCSBasicData::TPL_APRON1) != 0)
						|| (bmIsSinApron2 == true && strcmp(polTplData->Tnam,CCSBasicData::TPL_APRON2) != 0))
					{
						return false;
					}
				}
			}
		}
		///


		// can specify which demand types to load in ceda.ini LOAD_DEMAND_TYPES
		if(!ogBasicData.DemandsToLoad(prpDemand->Dety))
			return false;

		omData.Add(prpDemand);
		omUrnoMap.SetAt((void *)prpDemand->Urno,prpDemand);

		if(prpDemand->Ouri != 0L)
		{
			AddDemandToInboundMap(prpDemand);
		}
		if(prpDemand->Ouro != 0L)
		{
			AddDemandToOutboundMap(prpDemand);
		}
		if(prpDemand->Uref != 0L)
		{
			AddDemandToUrefMap(prpDemand);
		}
		if(prpDemand->Uprm != 0L)
		{
			AddDemandToUprmMap(prpDemand);
		}
		if(prpDemand->Uses != 0L)
		{
			AddDemandToUsesMap(prpDemand);
		}
		AddDemandToAlidMap(prpDemand);
	}
	return true;
}


bool CedaDemandData::AddRestrictedDemandInternal(DEMANDDATA *prpDemand)
{
	if(prpDemand != NULL)
	{
		if(GetRestrictedDemandByUrno(prpDemand->Urno) != NULL) // demand already in mem - can happen when checking for missing demands
			return false;

		// not for restricted demands
		//PrepareDataAfterRead(prpDemand);

		// omStartTime and omEndTime are in UTC
		if(!IsOverlapped(prpDemand->Debe, prpDemand->Deen, omStartTime, omEndTime) && ogFlightData.GetFlightByUrno(prpDemand->Ouri) == NULL && ogFlightData.GetFlightByUrno(prpDemand->Ouro) == NULL)
			return false;

		ConvertDatesToLocal(prpDemand);

		// can specify which demand types to load in ceda.ini LOAD_DEMAND_TYPES
		if(!ogBasicData.DemandsToLoad(prpDemand->Dety))
			return false;

		omRestrictedData.Add(prpDemand);
		omRestrictedUrnoMap.SetAt((void *)prpDemand->Urno,prpDemand);

		if(prpDemand->Ouri != 0L)
		{
			AddRestrictedDemandToInboundMap(prpDemand);
		}
		if(prpDemand->Ouro != 0L)
		{
			AddRestrictedDemandToOutboundMap(prpDemand);
		}
		/******
		if(prpDemand->Uref != 0L)
		{
			AddDemandToUrefMap(prpDemand);
		}
		if(prpDemand->Uses != 0L)
		{
			AddDemandToUsesMap(prpDemand);
		}
		AddDemandToAlidMap(prpDemand);
		****/
	}
	return true;
}


bool CedaDemandData::DeleteDemand(DEMANDDATA *prpDemand)
{
	bool blRc = false;

	if (*pcmFields == '\0')
	{
		CreateFieldList();
	}

	if(prpDemand != NULL)
	{
		char pclSelection[100];
		sprintf(pclSelection,"WHERE URNO = '%ld'",prpDemand->Urno);
		char pclCmd[100] = "DRT";

		ogBasicData.LogCedaAction(pcmTableName, pclCmd, pclSelection, "", "");

		if((blRc = CedaAction(pclCmd,pclSelection)))
		{
			DEMANDDATA rlOriginalDemand = *prpDemand;
			DeleteDemandInternal(prpDemand);
			//SendFlightChange(rlOriginalDemand.Ouri);
			//SendFlightChange(rlOriginalDemand.Ouro);
			ogCCSDdx.DataChanged((void *)this,DEMAND_DELETE,(void *) &rlOriginalDemand);
		}
		else
		{
			ogBasicData.LogCedaError("CedaDemandData Error",omLastErrorMessage,pclCmd,"",pcmTableName,pclSelection);
		}
	}
	return blRc;
}

void CedaDemandData::DeleteDemandInternal(DEMANDDATA *prpDemand)
{
	if(prpDemand != NULL)
	{
		omUrnoMap.RemoveKey((void *)prpDemand->Urno);
		DeleteDemandFromInboundMap(prpDemand->Ouri,prpDemand->Urno);
		DeleteDemandFromOutboundMap(prpDemand->Ouro,prpDemand->Urno);
		DeleteDemandFromUrefMap(prpDemand->Uref,prpDemand->Urno);
		DeleteDemandFromUprmMap(prpDemand->Uprm,prpDemand->Urno);
		DeleteDemandFromUsesMap(prpDemand->Uses,prpDemand->Urno);
		DeleteDemandFromAlidMap(prpDemand->Alid,prpDemand->Urno);

		bool blNotFound = true;
		int ilNumDemands = omData.GetSize();
		for(int ilD = 0; blNotFound && ilD < ilNumDemands; ilD++)
		{
			if(omData[ilD].Urno == prpDemand->Urno)
			{
				blNotFound = false;
				omData.DeleteAt(ilD);
			}
		}
	}
}


bool CedaDemandData::InsertDemand(DEMANDDATA *prpDemand)
{
	bool blRc = false;
	if(prpDemand != NULL)
	{
		bool ilRc = true;
		CString olListOfData;
		char pclData[524];

		char pclCmd[10];

		if (*pcmFields == '\0')
		{
			CreateFieldList();
		}

		if (bgIsPrm)
		{
			strcpy(pclCmd,"IPRM");
		}
		else
		{
			strcpy(pclCmd,"IDR");
		}
		ogBasicData.LogCedaAction(pcmTableName, pclCmd, "", pcmFieldList, pclData);
		prpDemand->Cdat = TIMENULL;
		prpDemand->Lstu = TIMENULL;
		strcpy(prpDemand->Usec,"");
		strcpy(prpDemand->Useu,"");

		PrepareDataForWrite(prpDemand, "IDR OpssPm");
		MakeCedaData(&omRecInfo,olListOfData,prpDemand);
		strcpy(pclData,olListOfData);
		if((blRc = CedaAction(pclCmd,"","",pclData)) == true)
		{
			if (AddDemandInternal(prpDemand))
			{
				ogConflicts.CheckConflicts(*prpDemand,FALSE,TRUE,TRUE,FALSE);
				//SendFlightChange(prpDemand->Ouri);
				//SendFlightChange(prpDemand->Ouro);
				//ogCCSDdx.DataChanged((void *)this,DEMAND_CHANGE,(void *)prpDemand);
				ogCCSDdx.DataChanged((void *)this,DEMAND_NEW,(void *)prpDemand);
			}
			else
			{
				blRc = false;
			}
		}
	}

	return blRc;
}

void CedaDemandData::AddDemandToInboundMap(DEMANDDATA *prpDemand)
{
	// Inbound demands belong to the Inbound flight (eeeeeee...extraodinary)
	if(prpDemand != NULL && prpDemand->Ouri != 0L)
	{
		long llFlur = prpDemand->Ouri;
		CMapPtrToPtr *polSingleMap;
		if(omInboundFlightMap.Lookup((void *) llFlur, (void *&) polSingleMap))
		{
			polSingleMap->SetAt((void *)prpDemand->Urno,prpDemand);
		}
		else
		{
			polSingleMap = new CMapPtrToPtr;
			polSingleMap->SetAt((void *)prpDemand->Urno,prpDemand);
			omInboundFlightMap.SetAt((void *)llFlur,polSingleMap);
		}
	}
}


void CedaDemandData::AddRestrictedDemandToInboundMap(DEMANDDATA *prpDemand)
{
	// Inbound demands belong to the Inbound flight (eeeeeee...extraodinary)
	if(prpDemand != NULL && prpDemand->Ouri != 0L)
	{
		long llFlur = prpDemand->Ouri;
		CMapPtrToPtr *polSingleMap;
		if(omRestrictedInboundFlightMap.Lookup((void *) llFlur, (void *&) polSingleMap))
		{
			polSingleMap->SetAt((void *)prpDemand->Urno,prpDemand);
		}
		else
		{
			polSingleMap = new CMapPtrToPtr;
			polSingleMap->SetAt((void *)prpDemand->Urno,prpDemand);
			omRestrictedInboundFlightMap.SetAt((void *)llFlur,polSingleMap);
		}
	}
}

void CedaDemandData::DeleteDemandFromInboundMap(long lpFlur, long lpDemUrno)
{
	CMapPtrToPtr *polSingleMap;
	if(omInboundFlightMap.Lookup((void *)lpFlur,(void *& )polSingleMap))
	{
		polSingleMap->RemoveKey((void *)lpDemUrno);
	}
}

void CedaDemandData::AddDemandToOutboundMap(DEMANDDATA *prpDemand)
{
	// Turnaround and Outbound demands belong to the Outbound flight
	if(prpDemand != NULL && prpDemand->Ouro != 0L)
	{
		long llFlur = prpDemand->Ouro;
		CMapPtrToPtr *polSingleMap;
		if(omOutboundFlightMap.Lookup((void *) llFlur, (void *&) polSingleMap))
		{
			polSingleMap->SetAt((void *)prpDemand->Urno,prpDemand);
		}
		else
		{
			polSingleMap = new CMapPtrToPtr;
			polSingleMap->SetAt((void *)prpDemand->Urno,prpDemand);
			omOutboundFlightMap.SetAt((void *)llFlur,polSingleMap);
		}
	}
}

void CedaDemandData::DeleteDemandFromOutboundMap(long lpFlur, long lpDemUrno)
{
	CMapPtrToPtr *polSingleMap;
	if(omOutboundFlightMap.Lookup((void *)lpFlur,(void *& )polSingleMap))
	{
		polSingleMap->RemoveKey((void *)lpDemUrno);
	}
}



void CedaDemandData::DeleteRestrictedDemandFromInboundMap(long lpFlur, long lpDemUrno)
{
	CMapPtrToPtr *polSingleMap;
	if(omInboundFlightMap.Lookup((void *)lpFlur,(void *& )polSingleMap))
	{
		polSingleMap->RemoveKey((void *)lpDemUrno);
	}
}

void CedaDemandData::AddRestrictedDemandToOutboundMap(DEMANDDATA *prpDemand)
{
	// Turnaround and Outbound demands belong to the Outbound flight
	if(prpDemand != NULL && prpDemand->Ouro != 0L)
	{
		long llFlur = prpDemand->Ouro;
		CMapPtrToPtr *polSingleMap;
		if(omRestrictedOutboundFlightMap.Lookup((void *) llFlur, (void *&) polSingleMap))
		{
			polSingleMap->SetAt((void *)prpDemand->Urno,prpDemand);
		}
		else
		{
			polSingleMap = new CMapPtrToPtr;
			polSingleMap->SetAt((void *)prpDemand->Urno,prpDemand);
			omRestrictedOutboundFlightMap.SetAt((void *)llFlur,polSingleMap);
		}
	}
}

void CedaDemandData::DeleteRestrictedDemandFromOutboundMap(long lpFlur, long lpDemUrno)
{
	CMapPtrToPtr *polSingleMap;
	if(omOutboundFlightMap.Lookup((void *)lpFlur,(void *& )polSingleMap))
	{
		polSingleMap->RemoveKey((void *)lpDemUrno);
	}
}

void CedaDemandData::AddDemandToUrefMap(DEMANDDATA *prpDemand)
{
	if(prpDemand != NULL)
	{
		long llUref = prpDemand->Uref;
		CMapPtrToPtr *polSingleMap;
		if(omUrefMap.Lookup((void *) llUref, (void *&) polSingleMap))
		{
			polSingleMap->SetAt((void *)prpDemand->Urno,prpDemand);
		}
		else
		{
			polSingleMap = new CMapPtrToPtr;
			polSingleMap->SetAt((void *)prpDemand->Urno,prpDemand);
			omUrefMap.SetAt((void *)llUref,polSingleMap);
		}
	}
}

void CedaDemandData::DeleteDemandFromUrefMap(long lpUref, long lpDemUrno)
{
	CMapPtrToPtr *polSingleMap;
	if(omUrefMap.Lookup((void *)lpUref,(void *& )polSingleMap))
	{
		polSingleMap->RemoveKey((void *)lpDemUrno);
	}
}

void CedaDemandData::AddDemandToUprmMap(DEMANDDATA *prpDemand)
{
	if(prpDemand != NULL)
	{
		long llUprm = prpDemand->Uprm;
		CMapPtrToPtr *polSingleMap;
		if(omUprmMap.Lookup((void *) llUprm, (void *&) polSingleMap))
		{
			polSingleMap->SetAt((void *)prpDemand->Urno,prpDemand);
		}
		else
		{
			polSingleMap = new CMapPtrToPtr;
			polSingleMap->SetAt((void *)prpDemand->Urno,prpDemand);
			omUprmMap.SetAt((void *)llUprm,polSingleMap);
		}
	}
}

void CedaDemandData::DeleteDemandFromUprmMap(long lpUprm, long lpDemUrno)
{
	CMapPtrToPtr *polSingleMap;
	if(omUprmMap.Lookup((void *)lpUprm,(void *& )polSingleMap))
	{
		polSingleMap->RemoveKey((void *)lpDemUrno);
	}
}

void CedaDemandData::AddDemandToUsesMap(DEMANDDATA *prpDemand)
{
	if(prpDemand != NULL)
	{
		long llUses = prpDemand->Uses;
		CMapPtrToPtr *polSingleMap;
		if(omUsesMap.Lookup((void *) llUses, (void *&) polSingleMap))
		{
			polSingleMap->SetAt((void *)prpDemand->Urno,prpDemand);
		}
		else
		{
			polSingleMap = new CMapPtrToPtr;
			polSingleMap->SetAt((void *)prpDemand->Urno,prpDemand);
			omUsesMap.SetAt((void *)llUses,polSingleMap);
		}
	}
}

void CedaDemandData::DeleteDemandFromUsesMap(long lpUses, long lpDemUrno)
{
	CMapPtrToPtr *polSingleMap;
	if(omUsesMap.Lookup((void *)lpUses,(void *& )polSingleMap))
	{
		polSingleMap->RemoveKey((void *)lpDemUrno);
	}
}

void CedaDemandData::AddDemandToAlidMap(DEMANDDATA *prpDemand)
{
	if(prpDemand != NULL && strlen(prpDemand->Alid) > 0)
	{
		CMapPtrToPtr *polSingleMap;
		if(omAlidMap.Lookup(prpDemand->Alid, (void *&) polSingleMap))
		{
			polSingleMap->SetAt((void *)prpDemand->Urno,prpDemand);
		}
		else
		{
			polSingleMap = new CMapPtrToPtr;
			polSingleMap->SetAt((void *)prpDemand->Urno,prpDemand);
			omAlidMap.SetAt(prpDemand->Alid,polSingleMap);
		}
	}
}

void CedaDemandData::DeleteDemandFromAlidMap(CString opAlid, long lpDemUrno)
{
	CMapPtrToPtr *polSingleMap;
	if(omAlidMap.Lookup(opAlid,(void *& )polSingleMap))
	{
		polSingleMap->RemoveKey((void *)lpDemUrno);
	}
}

DEMANDDATA *CedaDemandData::GetDemandByUrno(long lpUrno)
{
	DEMANDDATA  *prlDemand;

	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlDemand) == TRUE)
	{
	//	TRACE("GetDemandByUrno %ld \n",prlDemand->Urno);
		return prlDemand;
	}
	else if(ogBasicData.bmDelegateFlights)
	{
		int l = omExtraData.GetSize();
		for(int i = 0; i < l; i++)
		{
			if(omExtraData[i].Urno == lpUrno)
				return &omExtraData[i];
		}
	}
	return NULL;
}

DEMANDDATA *CedaDemandData::GetRestrictedDemandByUrno(long lpUrno)
{
	DEMANDDATA  *prlDemand;

	if (omRestrictedUrnoMap.Lookup((void *)lpUrno,(void *& )prlDemand) == TRUE)
	{
	//	TRACE("GetDemandByUrno %ld \n",prlDemand->Urno);
		return prlDemand;
	}
	else if(ogBasicData.bmDelegateFlights)
	{
		int l = omExtraData.GetSize();
		for(int i = 0; i < l; i++)
		{
			if(omExtraData[i].Urno == lpUrno)
				return &omExtraData[i];
		}
	}
	return NULL;
}


// the field USES matches up equipment and personnel demands for a flight
// so that if one is assigned, so will the other automatically via a fast link
DEMANDDATA *CedaDemandData::GetMatchingDemandByUses(DEMANDDATA *prpDemand)
{
	DEMANDDATA *prlMatchingDemand = NULL, *prlDemand = NULL;

	if(prpDemand != NULL && prpDemand->Uses != 0L)
	{
		CString olRety = NO_DEMAND;
		if(!strcmp(prpDemand->Rety,PERSONNEL_DEMAND))
		{
			olRety = EQUIPMENT_DEMAND;
		}
		else if(!strcmp(prpDemand->Rety,EQUIPMENT_DEMAND))
		{
			olRety = PERSONNEL_DEMAND;
		}

		CMapPtrToPtr *polSingleMap;

		if(omUsesMap.Lookup((void *)prpDemand->Uses, (void *& )polSingleMap) == TRUE)
		{
			POSITION rlPos;

			for ( rlPos =  polSingleMap->GetStartPosition(); rlPos != NULL; )
			{
				long llUrno;
				polSingleMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlDemand);
				if(!strcmp(prlDemand->Rety,olRety) && prlDemand->Ouri == prpDemand->Ouri && prlDemand->Ouro == prlDemand->Ouro)
				{
					prlMatchingDemand = prlDemand;
					break;
				}
			}
		}
	}

	return prlMatchingDemand;
}

// GetInboundDemandsByInboundFlight()
//
// This function returns all demands for an inbound flight, this means demands
// that are exclusively for an inbound flight (only OURI set)
//
// ropDemands - list of demands for the flight and allocation unit
// lpFlur - URNO of flight
// pcpAlid - Allocation Unit eg GateName,Position,Registration etc
// pcpAloc - Allocation Unit Type eg 'GAT','POS','REG'
// ipDemandType - { ALLDEMANDS | PERSONNELDEMANDS | EQUIPMENTDEMANDS | LOCATIONDEMANDS }
bool CedaDemandData::GetInboundDemandsByInboundFlight(CCSPtrArray<DEMANDDATA> &ropDemands,long lpFlur, const char *pcpAlid /*=""*/, 
													  const char *pcpAloc /*=""*/, int ipDemandType /* = ALLDEMANDS */)
{
	DEMANDDATA  *prlDemand;
	CMapPtrToPtr *polSingleMap;

	bool blIgnoreAlid = strlen(pcpAlid) <= 0 ? true : false;
	bool blIgnoreAloc = strlen(pcpAloc) <= 0 ? true : false;

	if (omInboundFlightMap.Lookup((void *)lpFlur,(void *& )polSingleMap) == TRUE)
	{
		POSITION rlPos;

		for ( rlPos =  polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			polSingleMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlDemand);
			if ((blIgnoreAlid || !strcmp(pcpAlid,prlDemand->Alid)) && (blIgnoreAloc || !strcmp(pcpAloc,prlDemand->Aloc)) &&
				*prlDemand->Dety == INBOUND_DEMAND && DemandIsOfType(prlDemand,ipDemandType))
			{
				ropDemands.Add(prlDemand);
			}
		}
	}
    return (ropDemands.GetSize() > 0) ? true : false;
}

// GetDemandsByInboundFlight()
//
// This function returns all demands for an inbound flight, this means demands
// that are exclusively for an inbound flight (only OURI set) plus flights
// for a rotation (both OURI and OURO set)
// 
// ropDemands - list of demands for the flight and allocation unit
// lpFlur - URNO of flight
// pcpAlid - Allocation Unit eg GateName,Position,Registration etc
// pcpAloc - Allocation Unit Type eg 'GAT','POS','REG'
// ipDemandType - { ALLDEMANDS | PERSONNELDEMANDS | EQUIPMENTDEMANDS | LOCATIONDEMANDS }
bool CedaDemandData::GetDemandsByInboundFlight(CCSPtrArray<DEMANDDATA> &ropDemands,long lpFlur, const char *pcpAlid /*=""*/, 
											   const char *pcpAloc /*=""*/, int ipDemandType /* = ALLDEMANDS */)
{
	DEMANDDATA  *prlDemand;
	CMapPtrToPtr *polSingleMap;

	bool blIgnoreAlid = strlen(pcpAlid) <= 0 ? true : false;
	bool blIgnoreAloc = strlen(pcpAloc) <= 0 ? true : false;

	if (omInboundFlightMap.Lookup((void *)lpFlur,(void *& )polSingleMap) == TRUE)
	{
		POSITION rlPos;

		for ( rlPos =  polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			// return not only inbound dems but also turnaround/P05/P05RES/CASP
			// otherwise inbound flights with the above dems have the conflict "No demands"
			long llUrno;
			polSingleMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlDemand);
			if ((blIgnoreAlid || !strcmp(pcpAlid,prlDemand->Alid)) && (blIgnoreAloc || !strcmp(pcpAloc,prlDemand->Aloc)) &&
				*prlDemand->Dety != OUTBOUND_DEMAND && DemandIsOfType(prlDemand,ipDemandType))
			{
				if(prlDemand->Ouri != 0L && prlDemand->Ouro != 0L && prlDemand->Dety[0] == '0')
				{											
					JOBDATA olJobData;
					bool blDelegatedFlightJob = false;
					CCSPtrArray<JOBDATA> olJobs;
					if(DemandHasDelegatedFlight(prlDemand->Urno) == true)
					{
						blDelegatedFlightJob = true;
					}
								
					if(ogFlightData.GetFlightByUrno(prlDemand->Ouri) != NULL
						&& ogFlightData.GetFlightByUrno(prlDemand->Ouro) != NULL)
					{
						TPLDATA* polTplData = ogTplData.GetTplByUrno(prlDemand->Utpl);
						if((bmIsSinApron1 == true && strcmp(polTplData->Tnam,CCSBasicData::TPL_APRON1) != 0 && blDelegatedFlightJob == false)
						 ||(bmIsSinApron2 == true && strcmp(polTplData->Tnam,CCSBasicData::TPL_APRON2) != 0 && blDelegatedFlightJob == false))
						
						{
							continue;
						}
					}
				}

				ropDemands.Add(prlDemand);
			}
		}
	}
    return (ropDemands.GetSize() > 0) ? true : false;
}

bool CedaDemandData::HasPrmDemands(long lpFlur)
{
	DEMANDDATA  *prlDemand;
	CMapPtrToPtr *polSingleMap;

	if (omInboundFlightMap.Lookup((void *)lpFlur,(void *& )polSingleMap) == TRUE)
	{
		POSITION rlPos;

		for ( rlPos =  polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			polSingleMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlDemand);

			if(prlDemand->Uprm != 0L)
			{						
				return true;
			}
		}
	}
	if (omOutboundFlightMap.Lookup((void *)lpFlur,(void *& )polSingleMap) == TRUE)
	{
		POSITION rlPos;

		for ( rlPos =  polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			polSingleMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlDemand);

			if(prlDemand->Uprm != 0L)
			{						
				return true;
			}
		}
	}
    return false;
}


bool CedaDemandData::GetRestrictedDemandsByInboundFlight(CCSPtrArray<DEMANDDATA> &ropDemands,long lpFlur, const char *pcpAlid /*=""*/, 
											   const char *pcpAloc /*=""*/, int ipDemandType /* = ALLDEMANDS */)
{
	DEMANDDATA  *prlDemand;
	CMapPtrToPtr *polSingleMap;

	bool blIgnoreAlid = strlen(pcpAlid) <= 0 ? true : false;
	bool blIgnoreAloc = strlen(pcpAloc) <= 0 ? true : false;

	if (omRestrictedInboundFlightMap.Lookup((void *)lpFlur,(void *& )polSingleMap) == TRUE)
	{
		POSITION rlPos;

		for ( rlPos =  polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			// return not only inbound dems but also turnaround/P05/P05RES/CASP
			// otherwise inbound flights with the above dems have the conflict "No demands"
			long llUrno;
			polSingleMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlDemand);
			if ((blIgnoreAlid || !strcmp(pcpAlid,prlDemand->Alid)) && (blIgnoreAloc || !strcmp(pcpAloc,prlDemand->Aloc)) &&
				*prlDemand->Dety != OUTBOUND_DEMAND && DemandIsOfType(prlDemand,ipDemandType))
			{
				ropDemands.Add(prlDemand);
			}
		}
	}
    return (ropDemands.GetSize() > 0) ? true : false;
}

// ipDemandType - { ALLDEMANDS | PERSONNELDEMANDS | EQUIPMENTDEMANDS | LOCATIONDEMANDS }
bool CedaDemandData::DemandIsOfType(DEMANDDATA *prpDemand, int ipDemandType)
{
	bool blDemandIsOfType = false;
	ASSERT(prpDemand);

	// check that the demands are of the correct type {PERSONNEL/EQUIPMENT/POSITION}
	// (P05RES or CAS demands don't have a RUD)
	if(ipDemandType == ALLDEMANDS || prpDemand->Urud == 0L || ogRudData.DemandIsOfType(prpDemand->Urud,ipDemandType))
	{
		blDemandIsOfType = true;
	}

	return blDemandIsOfType;
}

CString CedaDemandData::GetDemandTypeString(DEMANDDATA *prpDemand)
{
	CString olText = "Unknown";

	if(DemandIsOfType(prpDemand, PERSONNELDEMANDS))
	{
		olText = GetString(IDS_PERSONNEL_DEM);
	}
	else if(DemandIsOfType(prpDemand, EQUIPMENTDEMANDS))
	{
		olText = GetString(IDS_EQUIPMENT_DEM);
	}
	else if(DemandIsOfType(prpDemand, LOCATIONDEMANDS))
	{
		olText = GetString(IDS_LOCATION_DEM);
	}

	return olText;
}

// ilDemandType - { ALLDEMANDS | PERSONNELDEMANDS | EQUIPMENTDEMANDS | LOCATIONDEMANDS }
int CedaDemandData::GetDemandType(DEMANDDATA *prpDemand)
{
	int ilDemandType = NODEMANDS;
	if(prpDemand != NULL)
	{
		RUDDATA *prlRud = ogRudData.GetRudByUrno(prpDemand->Urud);
		if(prlRud != NULL)
		{
			ilDemandType = ogRudData.GetDemandType(prlRud->Rety);
		}
	}

	return ilDemandType;
}

// GetDemandsByOutboundFlight()
//
// This function returns all demands for an outbound flight, this means demands
// (except those of type inbound) that have OURO set and possibly also OURI. 
//
// ropDemands - list of demands for the flight and allocation unit
// lpFlur - URNO of flight
// pcpAlid - Allocation Unit eg GateName,Position,Registration etc
// pcpAloc - Allocation Unit Type eg 'GAT','POS','REG'
// ipDemandType - { ALLDEMANDS | PERSONNELDEMANDS | EQUIPMENTDEMANDS | LOCATIONDEMANDS }
bool CedaDemandData::GetDemandsByOutboundFlight(CCSPtrArray<DEMANDDATA> &ropDemands,long lpFlur,const char *pcpAlid /*=""*/,
												const char *pcpAloc /*=""*/, int ipDemandType /* = ALL_DEMAND */)
{
	DEMANDDATA  *prlDemand;
	CMapPtrToPtr *polSingleMap;

	bool blIgnoreAlid = strlen(pcpAlid) <= 0 ? true : false;
	bool blIgnoreAloc = strlen(pcpAloc) <= 0 ? true : false;

	if(omOutboundFlightMap.Lookup((void *)lpFlur,(void *& )polSingleMap) == TRUE)
	{
		POSITION rlPos;

		for ( rlPos =  polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			polSingleMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlDemand);
			if ((blIgnoreAlid || !strcmp(pcpAlid,prlDemand->Alid)) && (blIgnoreAloc || !strcmp(pcpAloc,prlDemand->Aloc)) &&
				*prlDemand->Dety != INBOUND_DEMAND && DemandIsOfType(prlDemand,ipDemandType))
			{
				if(prlDemand->Ouri != 0L && prlDemand->Ouro != 0L && prlDemand->Dety[0] == '0')
				{											
					JOBDATA olJobData;
					bool blDelegatedFlightJob = false;
					CCSPtrArray<JOBDATA> olJobs;
					if(DemandHasDelegatedFlight(prlDemand->Urno) == true)
					{
						blDelegatedFlightJob = true;
					}
							
					if(ogFlightData.GetFlightByUrno(prlDemand->Ouri) != NULL 
						&& ogFlightData.GetFlightByUrno(prlDemand->Ouro) != NULL)
					{
						TPLDATA* polTplData = ogTplData.GetTplByUrno(prlDemand->Utpl);
						if((bmIsSinApron1 == true && strcmp(polTplData->Tnam,CCSBasicData::TPL_APRON1) != 0 && blDelegatedFlightJob == false)
						 ||(bmIsSinApron2 == true && strcmp(polTplData->Tnam,CCSBasicData::TPL_APRON2) != 0 && blDelegatedFlightJob == false))
						
						{
							continue;
						}
					}
				}
				ropDemands.Add(prlDemand);
			}
		}
	}
    return true;
}

bool CedaDemandData::GetRestrictedDemandsByOutboundFlight(CCSPtrArray<DEMANDDATA> &ropDemands,long lpFlur,const char *pcpAlid /*=""*/,
												const char *pcpAloc /*=""*/, int ipDemandType /* = ALL_DEMAND */)
{
	DEMANDDATA  *prlDemand;
	CMapPtrToPtr *polSingleMap;

	bool blIgnoreAlid = strlen(pcpAlid) <= 0 ? true : false;
	bool blIgnoreAloc = strlen(pcpAloc) <= 0 ? true : false;

	if(omRestrictedOutboundFlightMap.Lookup((void *)lpFlur,(void *& )polSingleMap) == TRUE)
	{
		POSITION rlPos;

		for ( rlPos =  polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			polSingleMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlDemand);
			if ((blIgnoreAlid || !strcmp(pcpAlid,prlDemand->Alid)) && (blIgnoreAloc || !strcmp(pcpAloc,prlDemand->Aloc)) &&
				*prlDemand->Dety != INBOUND_DEMAND && DemandIsOfType(prlDemand,ipDemandType))
			{
				ropDemands.Add(prlDemand);
			}
		}
	}
    return true;
}

bool CedaDemandData::GetDemandsByUref(CCSPtrArray<DEMANDDATA> &ropDemands, long lpUref, CString opObty)
{
	DEMANDDATA  *prlDemand;
	CMapPtrToPtr *polSingleMap;

	if (omUrefMap.Lookup((void *)lpUref,(void *& )polSingleMap) == TRUE)
	{
		POSITION rlPos;

		for ( rlPos =  polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			polSingleMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlDemand);
			if (opObty == (CString)prlDemand->Obty)
				ropDemands.Add(prlDemand);
		}
	}
    return true;
}

bool CedaDemandData::GetDemandsByObty(CCSPtrArray<DEMANDDATA> &ropDemands, CString opObty, bool bpReset /*=true*/)
{
	DEMANDDATA  *prlDemand;

	if(bpReset)
	{
		ropDemands.RemoveAll();
	}

	int ilNumDems = omData.GetSize();
	for(int ilD = 0; ilD < ilNumDems; ilD++)
	{
		prlDemand = &omData[ilD];
		if (opObty == (CString)prlDemand->Obty)
		{
			ropDemands.Add(prlDemand);
		}
	}
    return (ropDemands.GetSize() > 0) ? true : false;
}

// lli: make use of the omUprmMap to find demands by UPRM. 
bool CedaDemandData::GetDemandsByUprm(CCSPtrArray<DEMANDDATA> &ropDemands, long lpUprm, bool bpReset /* = true */)
{
	if(bpReset)
	{
		ropDemands.RemoveAll();
	}

	DEMANDDATA *prlDemand;
	CMapPtrToPtr *polUprmDemandMap;

	if(omUprmMap.Lookup((void *)lpUprm,(void *& )polUprmDemandMap) == TRUE)
	{
		for(POSITION rlPos = polUprmDemandMap->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			polUprmDemandMap->GetNextAssoc(rlPos,(void *&) llUrno, (void *& )prlDemand);
			ropDemands.Add(prlDemand);
		}
	}
	return (ropDemands.GetSize() > 0) ? true : false;
}

// get all demands that are part of the same group/team as prpDemand
void CedaDemandData::GetTeamDemands(DEMANDDATA *prpDemand,CCSPtrArray <DEMANDDATA> &ropTeamDemands)
{
	CCSPtrArray <DEMANDDATA> olFlightDemands;
	GetDemandsByRotationFlur(olFlightDemands, prpDemand->Ouri, prpDemand->Ouro, prpDemand->Alid, prpDemand->Aloc);

	int ilNumFlightDemands = olFlightDemands.GetSize();
	for(int ilFD = 0; ilFD < ilNumFlightDemands; ilFD++)
	{
		DEMANDDATA *prlFlightDemand = &olFlightDemands[ilFD];
		if(prpDemand->Ulnk == prlFlightDemand->Ulnk)
		{
			ropTeamDemands.Add(prlFlightDemand);
		}
	}
}

// return the start time of the earliest demand in a team and the end time of the latest
void CedaDemandData::GetOverallTeamTimeForDemand(DEMANDDATA *prpDemand,CTime &ropStart, CTime &ropEnd)
{
	if(prpDemand != NULL)
	{
		CCSPtrArray <DEMANDDATA> olTeamDemands;
		GetTeamDemands(prpDemand,olTeamDemands);
		GetOverallTeamTimeForDemand(prpDemand, ropStart, ropEnd, olTeamDemands);
	}
}

void CedaDemandData::GetOverallTeamTimeForDemand(DEMANDDATA *prpDemand,CTime &ropStart, CTime &ropEnd, CCSPtrArray <DEMANDDATA> &ropTeamDemands)
{
	if(prpDemand != NULL)
	{
		ropStart = prpDemand->Debe;
		ropEnd = prpDemand->Deen;
		int ilNumTD = ropTeamDemands.GetSize();
		for(int ilTD = 0; ilTD < ilNumTD; ilTD++)
		{
			if(ropTeamDemands[ilTD].Debe < ropStart)
			{
				ropStart = ropTeamDemands[ilTD].Debe;
			}
			if(ropTeamDemands[ilTD].Deen > ropEnd)
			{
				ropEnd = ropTeamDemands[ilTD].Deen;
			}
		}
	}
}

// ipDemandType - { ALLDEMANDS | PERSONNELDEMANDS | EQUIPMENTDEMANDS | LOCATIONDEMANDS }
bool CedaDemandData::GetDemandsByFlur(CCSPtrArray<DEMANDDATA> &ropDemands,long lpFlur,const char *pcpAlid /* = "" */,const char *pcpAloc /* = "" */,int ipDemandType /* = ALL_DEMAND */, int ipReturnFlightType /*ID_NOT_RETURNFLIGHT*/)
{
	// get demands for the specified flight (lpFlur) and allocation unit (pcpAlid)
	if(ipReturnFlightType != ID_DEP_RETURNFLIGHT)
		GetDemandsByInboundFlight(ropDemands,lpFlur,pcpAlid,pcpAloc,ipDemandType);
	if(ipReturnFlightType != ID_ARR_RETURNFLIGHT)
		GetDemandsByOutboundFlight(ropDemands,lpFlur,pcpAlid,pcpAloc,ipDemandType);
    return (ropDemands.GetSize() > 0) ? true : false;
}

// ipDemandType - { ALLDEMANDS | PERSONNELDEMANDS | EQUIPMENTDEMANDS | LOCATIONDEMANDS }
bool CedaDemandData::GetDemandsByRotationFlur(CCSPtrArray<DEMANDDATA> &ropDemands,long lpInboundFlur,long lpOutboundFlur,const char *pcpAlid /* = "" */,const char *pcpAloc /* = "" */,int ipDemandType /* = ALL_DEMAND */)
{
	// get demands for the specified flight (lpFlur) and allocation unit (pcpAlid)
	GetInboundDemandsByInboundFlight(ropDemands,lpInboundFlur,pcpAlid,pcpAloc,ipDemandType);
	GetDemandsByOutboundFlight(ropDemands,lpOutboundFlur,pcpAlid,pcpAloc,ipDemandType);
    return (ropDemands.GetSize() > 0) ? true : false;
}

// ipDemandType - { ALLDEMANDS | PERSONNELDEMANDS | EQUIPMENTDEMANDS | LOCATIONDEMANDS }
bool CedaDemandData::GetDemandsByFlurAndUrue(CCSPtrArray <DEMANDDATA> &ropDemands,long lpFlur,long lpUrue, const char *pcpAlid /*=""*/, const char *pcpAloc /*=""*/,int ipDemandType /* = ALL_DEMAND */)
{
	CCSPtrArray <DEMANDDATA> olFlightDemands;
	GetDemandsByInboundFlight(olFlightDemands,lpFlur,pcpAlid,pcpAloc,ipDemandType);
	GetDemandsByOutboundFlight(olFlightDemands,lpFlur,pcpAlid,pcpAloc,ipDemandType);
    return (ropDemands.GetSize() > 0) ? true : false;
}

DEMANDDATA *CedaDemandData::GetDemandByFlurAndUrud(long lpOuri, long lpOuro, long lpUrud)
{
	DEMANDDATA *prlDemand = NULL;

	CCSPtrArray <DEMANDDATA> olDems;
	GetDemandsByInboundFlight(olDems,lpOuri);
	GetDemandsByOutboundFlight(olDems,lpOuro);

	int ilNumDems = olDems.GetSize();
	for(int ilD = 0; ilD < ilNumDems; ilD++)
	{
		if(olDems[ilD].Urud == lpUrud)
		{
			prlDemand = &olDems[ilD];
			break;
		}
	}

	return prlDemand;
}

// ipDemandType - { ALLDEMANDS | PERSONNELDEMANDS | EQUIPMENTDEMANDS | LOCATIONDEMANDS }
bool CedaDemandData::GetDemandsByAlid(CCSPtrArray <DEMANDDATA> &ropDemands,const char *pcpAlid /*=""*/, const char *pcpAloc /*=""*/,int ipDemandType /* = ALL_DEMAND */)
{
	bool blIgnoreAlid = strlen(pcpAlid) <= 0 ? true : false;
	bool blIgnoreAloc = strlen(pcpAloc) <= 0 ? true : false;

	DEMANDDATA *prlDemand = NULL;
	if(!blIgnoreAlid)
	{
		long llUrno;
		CMapPtrToPtr *polSingleMap;
		if(omAlidMap.Lookup(pcpAlid, (void *& )polSingleMap) == TRUE)
		{
			for(POSITION rlPos =  polSingleMap->GetStartPosition(); rlPos != NULL; )
			{
				polSingleMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlDemand);
				if(DemandIsOfType(prlDemand,ipDemandType) && (blIgnoreAloc || strcmp(prlDemand->Aloc,pcpAloc) == 0))
					ropDemands.Add(prlDemand);
			}
		}
	}
	else
	{
		int ilTotDem = omData.GetSize();
		for (int i = 0; i < ilTotDem; i++)
		{
			prlDemand = &omData[i];
			if(DemandIsOfType(prlDemand,ipDemandType) && (blIgnoreAloc || strcmp(prlDemand->Aloc,pcpAloc) == 0))
				ropDemands.Add(prlDemand);
		}
	}
	return true;
}

//typedef void (*DDXCALLBACK)(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
// the callback function for storing broadcasted fligthdata in FLIGHTDATA array

void  ProcessDemandCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	if(ipDDXType == BC_DEMAND_NEW)
	{
		((CedaDemandData *)popInstance)->ProcessDemandInsert(vpDataPointer);
	}
	if(ipDDXType == BC_DEMAND_CHANGE)
	{
		((CedaDemandData *)popInstance)->ProcessDemandUpdate(vpDataPointer);
	}
	else if(ipDDXType == BC_DEMAND_DELETE)
	{
		((CedaDemandData *)popInstance)->ProcessDemandDelete(vpDataPointer);
	}
	else if(ipDDXType == BC_DEMAND_REL)
	{
		((CedaDemandData *)popInstance)->ProcessDemandSBC(vpDataPointer);
	}
	else if(ipDDXType == BC_DEMAND_UPD)
	{
		((CedaDemandData *)popInstance)->ProcessDemandUPD(vpDataPointer);
	}
}

void CedaDemandData::SendAllDdxs()
{
	bmDontSendDdx = false;
	for(int ilI = 0; ilI < omInsertDdxs.GetSize(); ilI++)
		ogCCSDdx.DataChanged((void *)this, DEMAND_NEW, (void *)&omInsertDdxs[ilI]);
	omInsertDdxs.RemoveAll();
	for(int ilU = 0; ilU < omUpdateDdxs.GetSize(); ilU++)
		ogCCSDdx.DataChanged((void *)this, DEMAND_CHANGE, (void *)&omUpdateDdxs[ilU]);
	omUpdateDdxs.RemoveAll();
}

void CedaDemandData::ProcessDemandInsert(void *vpDataPointer)
{
	struct BcStruct *prlDemandData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlDemandData);

	ProcessDemandInsert(prlDemandData->Fields,prlDemandData->Data);
}

void CedaDemandData::ProcessDemandInsert(char *pcpFields, char *pcpData)
{
	DEMANDDATA *prlDemand = new DEMANDDATA;
	GetRecordFromItemList(prlDemand, pcpFields, pcpData);
	if (bgIsPrm)
	{
		if (ogBasicData.IsPrmHandlingAgentEnabled())
		{
			if (strcmp(prlDemand->Filt,ogPrmConfig.getHandlingAgentShortName()))
			{
				return;
			}
		}
	}
	if(prlDemand->Ucon == 0L)
	{
		//ogBasicData.Trace("Insert Demand URNO <%ld>\n",prlDemand->Urno);
		if(GetDemandByUrno(prlDemand->Urno) != NULL)
		{
			ProcessDemandUpdate(prlDemand->Urno, pcpFields, pcpData);
		}
		else
		{
			if (AddDemandInternal(prlDemand))
			{
				// the demand may be within the timeframe but the flight not. In this case read the flight and any jobs
				//CheckForMissingFlight(prlDemand, "Single demand insert Read Missing Flights", true);
				if(IsFlightDependingDemand(prlDemand))
				{
					ogFlightData.HandleMissingFlight(prlDemand->Ouri);
					ogFlightData.HandleMissingFlight(prlDemand->Ouro);
				}
				ogConflicts.CheckConflicts(*prlDemand,FALSE,TRUE,TRUE,FALSE);
				if(bmDontSendDdx)
					omInsertDdxs.Add(prlDemand);
				else
				{
					ogCCSDdx.DataChanged((void *)this,DEMAND_NEW,(void *)prlDemand);
					
					//igu
					if (pogButtonList->m_wndPrmTable != NULL)
					{
						DPXDATA *prlDpx = pogButtonList->m_wndPrmTable->GetDpx();
						
						if (prlDpx != NULL)
						{
							if (pogButtonList->m_wndPrmTable->IsPassFilter(prlDpx))							
								ogCCSDdx.DataChanged((void *)this,DPX_NEW,(void *)prlDpx);
							pogButtonList->m_wndPrmTable->SetDpx(NULL);
						}
						else
						{
							BOOL IsSelfInsert = pogButtonList->m_wndPrmTable->IsSelfInsert();

							if (IsSelfInsert)
							{
								pogButtonList->m_wndPrmTable->UpdateCurrentView();

								pogButtonList->m_wndPrmTable->SetSelfInsert(FALSE);
							}
						}
					}
				}	
			}
			else
			{
				delete prlDemand;
			}
		}
	}
	else
	{
		// UCON contains the urno of the original demand - ie when a demand is already started
		// and a flight change is received, then a new demand is created. The original is not
		// deleted but its URNO is written in the UCON field of the new demand
		ogBasicData.Trace("Ignoring Insert of Demand URNO %ld because UCON <%ld> is not empty\n",prlDemand->Urno,prlDemand->Ucon);
		ogBasicData.Trace("(Insert of demand after flight update received for demand with begonnen jobs)\n");
		delete prlDemand;
	}
}

void CedaDemandData::ProcessDemandUpdate(void *vpDataPointer)
{
	struct BcStruct *prlDemandData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlDemandData);
	long llUrno = GetUrnoFromSelection(prlDemandData->Selection);
	if(llUrno == 0L)
	{
		DEMANDDATA rlDemand;
		GetRecordFromItemList(&omRecInfo,&rlDemand,prlDemandData->Fields,prlDemandData->Data);
		llUrno = rlDemand.Urno;
	}
	if(!ProcessDemandUpdate(llUrno, prlDemandData->Fields, prlDemandData->Data))
	{
		DEMANDDATA rlDemand;
		GetRecordFromItemList(&omRecInfo,&rlDemand,prlDemandData->Fields,prlDemandData->Data);

		if(DemandHasCorrectTemplate(&rlDemand))
		{
			if((rlDemand.Debe != TIMENULL && IsBetween(rlDemand.Debe, omStartTime, omEndTime)) ||
				(rlDemand.Deen != TIMENULL && IsBetween(rlDemand.Deen, omStartTime, omEndTime)))
			{
				if(DemandHasFullFieldList(&rlDemand))
				{
					ProcessDemandInsert(prlDemandData->Fields,prlDemandData->Data);
				}
				else
				{
					char pclSelection[100];
					sprintf(pclSelection, "WHERE URNO = '%ld'", llUrno);
					ReadDemands(pclSelection);
					DEMANDDATA *prlDemand = NULL;
					if((prlDemand = GetDemandByUrno(llUrno)) != NULL)
					{
						// the demand may be within the timeframe but the flight not. In this case read the flight and any jobs
						//CheckForMissingFlight(prlDemand, "Single demand update Read Missing Flights", true);
						//CheckForMissingJob(prlDemand, "Single demand update Read Missing Jobs", true);
						if(IsFlightDependingDemand(prlDemand))
						{
							ogFlightData.HandleMissingFlight(prlDemand->Ouri);
							ogFlightData.HandleMissingFlight(prlDemand->Ouro);
						}
						ogConflicts.CheckJobConflictsForDemand(prlDemand->Urno);
						ogConflicts.CheckConflicts(*prlDemand,FALSE,TRUE,TRUE,FALSE);
						ogCCSDdx.DataChanged((void *)this,DEMAND_CHANGE,(void *)prlDemand);
					}
				}
			}
		}
	}
}

bool CedaDemandData::DemandHasCorrectTemplate(DEMANDDATA *prpDemand)
{
	void *p = NULL;
	if(prpDemand->Utpl != 0L && !omUtplFilterMap.Lookup((void*)prpDemand->Utpl,p))
	{
		return false;
	}
	return true;
}

bool CedaDemandData::DemandHasFullFieldList(DEMANDDATA *prpDemand)
{
	if(prpDemand != NULL && prpDemand->Urno != 0L && prpDemand->Debe != TIMENULL && prpDemand->Deen != TIMENULL && strlen(prpDemand->Dety) > 0 && 
		strlen(prpDemand->Obty) > 0 && strlen(prpDemand->Rety) > 0 && prpDemand->Urud != 0L && prpDemand->Utpl != 0L)
	{
		if(omFlightDemand.Find(prpDemand->Dety) && prpDemand->Ouri == 0L && prpDemand->Ouro == 0L)
			return false;
		return true;
	}
	return false;
}

bool CedaDemandData::ProcessDemandUpdate(long lpDemandUrno, char *pcpFields, char *pcpData)
{
	bool blFound = false;

	DEMANDDATA *prlDemand = GetDemandByUrno(lpDemandUrno);
	if(prlDemand != NULL)
	{
		blFound = true;
		// update demand
		DEMANDDATA rlOriginalDemand = *prlDemand;

		// before reading the newly broadcasted data into the record found,
		// convert the existing dates to UTC so that PrepareDataAfterRead
		// converts them and any new dates from the broadcast to local
		ConvertDatesToUtc(prlDemand);
		GetRecordFromItemList(prlDemand, pcpFields, pcpData);
		ConvertDatesToLocal(prlDemand);

		if(rlOriginalDemand.Uref != prlDemand->Uref)
		{
			if(rlOriginalDemand.Uref != 0L)
			{
				DeleteDemandFromUrefMap(rlOriginalDemand.Uref,rlOriginalDemand.Urno);
			}
			if(prlDemand->Uref != 0L)
			{
				AddDemandToUrefMap(prlDemand);
			}
		}
		if(rlOriginalDemand.Uprm != prlDemand->Uprm)
		{
			if(rlOriginalDemand.Uprm != 0L)
			{
				DeleteDemandFromUprmMap(rlOriginalDemand.Uprm,rlOriginalDemand.Urno);
			}
			if(prlDemand->Uprm != 0L)
			{
				AddDemandToUprmMap(prlDemand);
			}
		}
		if(rlOriginalDemand.Ouri != prlDemand->Ouri)
		{
			DeleteDemandFromInboundMap(rlOriginalDemand.Ouri,prlDemand->Urno);
			AddDemandToInboundMap(prlDemand);
			//SendFlightChange(rlOriginalDemand.Ouri);
		}
		if(rlOriginalDemand.Ouro != prlDemand->Ouro)
		{
			DeleteDemandFromOutboundMap(rlOriginalDemand.Ouro,prlDemand->Urno);
			AddDemandToOutboundMap(prlDemand);
			//SendFlightChange(rlOriginalDemand.Ouro);
		}
		if(strcmp(rlOriginalDemand.Alid,prlDemand->Alid))
		{
			DeleteDemandFromAlidMap(rlOriginalDemand.Alid, prlDemand->Urno);
			AddDemandToAlidMap(prlDemand);
		}

		// check for conflicts for jobs that are allocated to this demand
		ogConflicts.CheckJobConflictsForDemand(prlDemand->Urno);
		ogConflicts.CheckConflicts(*prlDemand,FALSE,TRUE,TRUE,FALSE);
		if(bmDontSendDdx)
			omUpdateDdxs.Add(prlDemand);
		else
			ogCCSDdx.DataChanged((void *)this,DEMAND_CHANGE,(void *)prlDemand);
	}

	return blFound;
}

void CedaDemandData::ProcessDemandDelete(void *vpDataPointer)
{
	struct BcStruct *prlDemandData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlDemandData);
	long llUrno = GetUrnoFromSelection(prlDemandData->Selection);
	ProcessDemandDelete(llUrno);
}

void CedaDemandData::ProcessDemandDelete(long llDemUrno)
{
	DEMANDDATA *prlDemand = GetDemandByUrno(llDemUrno);
	if(prlDemand != NULL)
	{
		DEMANDDATA rlOriginalDemand = *prlDemand;
		DeleteDemandInternal(prlDemand);
		ogCCSDdx.DataChanged((void *)this,DEMAND_DELETE,(void *) &rlOriginalDemand);
	}
}

void CedaDemandData::ProcessDemandSBC(void *vpDataPointer)
{
	struct BcStruct *prlDemandData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlDemandData);
	CString olData = prlDemandData->Data;

	// data list format "DEBE-DEEN|TPLTAB.URNO1,...,TPLTAB.URNOn"
	//opData = "20030320160000-20030321160000|113682509�10527014�113682509";
	MakeClientString(olData);

	CString olTmp;
	CStringArray olUrnos;
	CTime olDebe, olDeen;
	int ilIdx1 = -1, ilIdx2 = -1;
	if((ilIdx1 = olData.Find("-",0)) != -1)
	{
		olTmp = olData.Left(ilIdx1);
		StoreDate(olTmp, (CTime *)&olDebe);
		if((ilIdx2 = olData.Find("|",0)) != -1)
		{
			ilIdx1++;
			olTmp = olData.Mid(ilIdx1, ilIdx2-ilIdx1);
			StoreDate(olTmp, (CTime *)&olDeen);

			// dates within the timeframe
			if(IsOverlapped(olDebe, olDeen, omStartTime, omEndTime))
			{
				ilIdx2++;
				CStringArray olTemplateUrnos;
				ogBasicData.ExtractItemList(olData.Right(olData.GetLength() - ilIdx2), &olTemplateUrnos);

				int ilNumTemplateUrnos = olTemplateUrnos.GetSize();
				for(int ilTU = 0; ilTU < ilNumTemplateUrnos; ilTU++)
				{
					long llUtpl = atol(olTemplateUrnos[ilTU]);
					// template is loaded
					void *p;
					if(llUtpl != 0L && omUtplFilterMap.Lookup((void*)llUtpl,p))
					{
						// UDP broadcast
						ReloadDemands(prlDemandData->Selection);
						break;
					}
				}
			}
		}
	}
}

bool CedaDemandData::ProcessAttachment(CString &ropAttachment)
{
	bool blSuccess = false;
	if(!ropAttachment.IsEmpty())
	{
		CAttachment olAttachment(ropAttachment);
		if(olAttachment.bmAttachmentValid)
		{
			blSuccess = true;
			bmDontSendDdx = true;
			for(int ilI = 0; ilI < olAttachment.omInsertDataList.GetSize(); ilI++)
			{
				ProcessDemandInsert(olAttachment.omFieldList.GetBuffer(0), olAttachment.omInsertDataList[ilI].GetBuffer(0));
			}

			for(int ilU = 0; ilU < olAttachment.omUpdateDataList.GetSize(); ilU++)
			{
				DEMANDDATA rlDemand;
				GetRecordFromItemList(&omRecInfo,&rlDemand, olAttachment.omFieldList.GetBuffer(0), olAttachment.omUpdateDataList[ilU].GetBuffer(0));
				if(!ProcessDemandUpdate(rlDemand.Urno, olAttachment.omFieldList.GetBuffer(0), olAttachment.omUpdateDataList[ilU].GetBuffer(0)))
					ProcessDemandInsert(olAttachment.omFieldList.GetBuffer(0), olAttachment.omUpdateDataList[ilU].GetBuffer(0));
			}

			for(int ilD = 0; ilD < olAttachment.omDeleteDataList.GetSize(); ilD++)
			{
				ProcessDemandDelete(atol(olAttachment.omDeleteDataList[ilD]));
			}
			SendAllDdxs();
		}
	}
	return blSuccess;
}

void CedaDemandData::ReloadDemands(const char *pcpDemandTypes)
{
	CStringArray olDemandTypes;
	ogBasicData.ExtractItemList(pcpDemandTypes, &olDemandTypes);
	CString olFormattedDemandTypes, olTmp, olDemandTypeText = GetString(IDS_FLIGHT_RELATED);
	int ilNumDT = olDemandTypes.GetSize();
	for(int ilDT = 0; ilDT < ilNumDT; ilDT++)
	{
		if(!olFormattedDemandTypes.IsEmpty())
		{
			olFormattedDemandTypes += ",";
			//olDemandTypeText += "/";
		}
		olTmp.Format("'%s'", olDemandTypes[ilDT]);
		olFormattedDemandTypes += olTmp;

		if(olDemandTypes[ilDT].GetAt(0) == FID_DEMAND)
		{
			olDemandTypeText = GetString(IDS_FLIGHT_INDEPENDENT);
		}
		//olDemandTypeText += GetStringForDemandType(olDemandTypes[ilDT]);
	}

	CString olMsg;
	olMsg.Format(GetString(IDS_DEMRELOAD), olDemandTypeText);
	WaitDlg olWaitDlg(NULL,olMsg);

   // Select data from the database
	int ilLc ;

	if (*pcmFields == '\0')
	{
		CreateFieldList();
	}


	ogBasicData.MarkTime();
	CTime olStartMinusOffset = omStartTime - CTimeSpan(2,0,0,0);
		if (ogBasicData.IsPrmHandlingAgentEnabled()) 
		{
			sprintf(cgSelection, "WHERE DEBE between '%s' and '%s' and DEEN > '%s' AND DETY IN (%s)  AND FILT = '%s'  ",
				olStartMinusOffset.Format("%Y%m%d%H%M%S"), omEndTime.Format("%Y%m%d%H%M%S"),
				omStartTime.Format("%Y%m%d%H%M%S"),olFormattedDemandTypes,
				ogPrmConfig.getHandlingAgentShortName());
		}
		else
		{
			sprintf(cgSelection, "WHERE DEBE between '%s' and '%s' and DEEN > '%s' AND DETY IN (%s)", olStartMinusOffset.Format("%Y%m%d%H%M%S"), omEndTime.Format("%Y%m%d%H%M%S"),omStartTime.Format("%Y%m%d%H%M%S"),olFormattedDemandTypes);
		}
	char pclCom[10] = "RT";
	if (CedaAction2(pclCom, cgSelection) == RCFailure)
	{
		ogBasicData.LogCedaError("CedaDemandData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,cgSelection);
		return;
	}

	// delete the demands that have been regenerated
	DEMANDDATA *prlDemand = NULL;
	int ilNumDemands = omData.GetSize();
	for(int ilD = (ilNumDemands-1); ilD >= 0; ilD--)
	{
		prlDemand = &omData[ilD];
		for(int ilDT = 0; ilDT < ilNumDT; ilDT++)
		{
			if(!strcmp(prlDemand->Dety, olDemandTypes[ilDT]))
			{
				DEMANDDATA rlDemCopy = *prlDemand;
				DeleteDemandInternal(prlDemand);
//				ogCCSDdx.DataChanged((void *)this,DEMAND_DELETE,(void *) &rlDemCopy);
				break;
			}
		}
	}

	CDWordArray olFlightUrnos;
	bool blUpdate = false;
	bool blRc = true;
	for (ilLc = 0; blRc; ilLc++)
	{
		DEMANDDATA *prlNewDemand = new DEMANDDATA;
		if ((blRc = GetBufferRecord2(ilLc,prlNewDemand)) == RCSuccess && prlNewDemand->Ucon == 0L)
		{
			ogBasicData.SaveTimeTakenForReread("RELDEM (coverage release)", cgSelection);
			if (!AddDemandInternal(prlNewDemand))
			{
				delete prlNewDemand;
			}
			else
			{
				// the demand may be within the timeframe but the flight not. In this case read the flight and any jobs
				if(IsFlightDependingDemand(prlNewDemand))
				{
					olFlightUrnos.Add(prlNewDemand->Ouri);
					olFlightUrnos.Add(prlNewDemand->Ouro);
				}
				//CheckForMissingFlight(prlNewDemand);
				//CheckForMissingJob(prlNewDemand);
				blUpdate = true;
			}
		}
		else
		{
			delete prlNewDemand;
		}
	}

	if(blUpdate)
	{
		for(int i = 0; i < olFlightUrnos.GetSize(); i++)
			ogFlightData.HandleMissingFlight(olFlightUrnos[i]);
		//ReadMissingFlights("RELDEM (coverage release) Read Missing Flights");
		//ReadMissingJobs("RELDEM (coverage release) Read Missing Jobs");
		ogConflicts.CheckAllConflicts(TRUE);
		ogCCSDdx.DataChanged(this,REDISPLAY_ALL,NULL);
	}

	olWaitDlg.CloseWindow();
}

void CedaDemandData::ProcessDemandUPD(void *vpDataPointer)
{
	struct BcStruct *prlDemandData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlDemandData);
	CString olData = prlDemandData->Data;
	int ilMaxSelectionLen = CDD_SELECTIONLEN - 40;

	// olData = "VON,BIS,DEL:n,URNO1,....,URNOn,UPD:n,URNO1,....,URNOn"
	MakeClientString(olData);

	CStringArray olDataList;
	ogBasicData.ExtractItemList(olData, &olDataList);
	if(olDataList.GetSize() < 2)
		return;

	int ilDrtCount = 0;
	bool blDelete = false, blUpdate = false;
	CTime olDebe, olDeen;

	StoreDate(olDataList[0], (CTime *)&olDebe);
	StoreDate(olDataList[1], (CTime *)&olDeen);
	// dates within the timeframe
	if(IsOverlapped(olDebe, olDeen, omStartTime, omEndTime))
	{
		if(!ProcessAttachment(prlDemandData->Attachment)) // TCP/IP briadcast - contains attachment with data
		{
			// UDP broadcast - requires re-read
			CString olTmp1, olTmp2, olDemUrnoList;
			long llUrno;
			int ilNumItems = olDataList.GetSize();
			for(int ilItem = 2; ilItem < ilNumItems; ilItem++)
			{
				olTmp1 = olDataList[ilItem];
				if(olTmp1.Find("DEL:") != -1)
				{
					blDelete = true;
					blUpdate = false;
				}
				else if(olTmp1.Find("UPD:") != -1)
				{
					blDelete = false;
					blUpdate = true;
				}
				else if(blDelete)
				{
					llUrno = atol(olTmp1);
					ProcessDemandDelete(llUrno);
					ilDrtCount++;
				}
				else if(blUpdate)
				{
					if(!olDemUrnoList.IsEmpty())
					{
						olDemUrnoList += ",";
					}
					olTmp2.Format("'%s'",olTmp1);
					olDemUrnoList += olTmp2;
					if(olDemUrnoList.GetLength() >= ilMaxSelectionLen)
					{
						ReReadDemands(olDemUrnoList);
						olDemUrnoList.Empty();
					}
				}
			}

			if(!olDemUrnoList.IsEmpty())
			{
				ReReadDemands(olDemUrnoList);
			}

			ogBasicData.Trace("CedaDemData-ReRead: %d Deleted Records", ilDrtCount);
		}
	}
}

void CedaDemandData::ReReadDemands(CString &ropDemUrnoList)
{
   // Select data from the database
	int ilLc ;

	if (*pcmFields == '\0')
	{
		CreateFieldList();
	}

	WaitDlg olWaitDlg(NULL,"Some demands have been recalculated and are in the process of being reloaded.");

	ogBasicData.MarkTime();
	sprintf(cgSelection, "WHERE URNO IN (%s)", ropDemUrnoList);
	char pclCom[10] = "RT";
	if (CedaAction2(pclCom, cgSelection) == RCFailure)
	{
		ogBasicData.LogCedaError("CedaDemandData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,cgSelection);
		return;
	}
	ogBasicData.SaveTimeTakenForReread("UPDDEM (block of demand changes)", cgSelection);
	olWaitDlg.CloseWindow();

	CDWordArray olFlightUrnos;
	CMapPtrToPtr olOldDemands;
	CUIntArray olNewDemands;
	bool blUpdate = false;
	bool blDemandAdded = false;
	bool blRc = true;
	for (ilLc = 0; blRc; ilLc++)
	{
		blDemandAdded = false;
		DEMANDDATA *prlNewDemand = new DEMANDDATA;
		if ((blRc = GetBufferRecord2(ilLc,prlNewDemand)) == RCSuccess)
		{
			DEMANDDATA *prlOldDemand = GetDemandByUrno(prlNewDemand->Urno);
			if(prlOldDemand != NULL)
			{
				olOldDemands.SetAt((void *) prlOldDemand->Urno, NULL);
				DeleteDemandInternal(prlOldDemand);
			}
			if(prlNewDemand->Ucon == 0L)
			{
				if(AddDemandInternal(prlNewDemand))
				{
					olNewDemands.Add(prlNewDemand->Urno);
					blDemandAdded = true;

					// the demand may be within the timeframe but the flight not. In this case read the flight and any jobs
					//CheckForMissingFlight(prlNewDemand);
					//CheckForMissingJob(prlNewDemand);
					if(IsFlightDependingDemand(prlNewDemand))
					{
						olFlightUrnos.Add(prlNewDemand->Ouri);
						olFlightUrnos.Add(prlNewDemand->Ouro);
					}
					blUpdate = true;
				}
			}
		}

		if(!blDemandAdded)
		{
			delete prlNewDemand;
		}
	}

	if(blUpdate)
	{
		for(int i = 0; i < olFlightUrnos.GetSize(); i++)
			ogFlightData.HandleMissingFlight(olFlightUrnos[i]);
		int ilUrtCount = 0, ilIrtCount = 0;
		//ReadMissingFlights("UPDDEM (block of demand changes) Read Missing Flights");
		//ReadMissingJobs("UPDDEM (block of demand changes) Read Missing Jobs");

		void *pvlDummy = NULL;
		DEMANDDATA *prlNewDemand = NULL;
		int ilNumDemands = olNewDemands.GetSize();
		for(int ilD = 0; ilD < ilNumDemands; ilD++)
		{
			if((prlNewDemand = GetDemandByUrno(olNewDemands[ilD])) != NULL)
			{
				ogConflicts.CheckConflicts(*prlNewDemand,FALSE,TRUE,TRUE,FALSE);
				if(olOldDemands.Lookup((void *) prlNewDemand->Urno, (void *&) pvlDummy))
				{
					ilUrtCount++;
					ogCCSDdx.DataChanged((void *)this,DEMAND_CHANGE,(void *)prlNewDemand);
				}
				else
				{
					ilIrtCount++;
					ogCCSDdx.DataChanged((void *)this,DEMAND_NEW,(void *)prlNewDemand);
				}
			}
		}

		ogBasicData.Trace("CedaDemData-ReRead: %d Inserted Records %d Updated Records", ilIrtCount, ilUrtCount);
	}
}

void CedaDemandData::ConvertDatesToUtc(DEMANDDATA *prpDemand)
{
	ogBasicData.ConvertDateToUtc(prpDemand->Debe);
	ogBasicData.ConvertDateToUtc(prpDemand->Deen);
	ogBasicData.ConvertDateToUtc(prpDemand->Cdat);
	ogBasicData.ConvertDateToUtc(prpDemand->Lstu);
}

void CedaDemandData::ConvertDatesToLocal(DEMANDDATA *prpDemand)
{
	ogBasicData.ConvertDateToLocal(prpDemand->Debe);
	ogBasicData.ConvertDateToLocal(prpDemand->Deen);
	ogBasicData.ConvertDateToLocal(prpDemand->Cdat);
	ogBasicData.ConvertDateToLocal(prpDemand->Lstu);
}

/* replaced - see below
long CedaDemandData::GetFlightUrno(DEMANDDATA *prpDemand)
{
	long llFlightUrno = 0L;

	if(prpDemand != NULL)
	{
		if(*prpDemand->Dety == '1') // Inbound
		{
			llFlightUrno = prpDemand->Ouri;
		}
		// fpa
		else if (*prpDemand->Dety == '2' || *prpDemand->Dety == '0') // Outbound '2' or Turnaround '0'
		{
			llFlightUrno = prpDemand->Ouro;
		}
		else if (*prpDemand->Dety == '3' || *prpDemand->Dety == '4' || *prpDemand->Dety == '5') // Registrations
		{
			if (prpDemand->Ouri > 0)
			{
				if(ogFlightData.GetFlightByUrno(prpDemand->Ouri) != NULL)
				{
					llFlightUrno = prpDemand->Ouri;
				}
				else
				{
					llFlightUrno = prpDemand->Ouro;
				}
			}
			else
			{
				llFlightUrno = prpDemand->Ouro;
			}
		}
		// end fpa
	}

	return llFlightUrno;
}
*/

bool CedaDemandData::FlightEnabled(DEMANDDATA *prpDemand)
{
	bool blFlightEnabled = true;

	if(prpDemand != NULL)
	{
		if(prpDemand->Flgs[CEDADEMANDDATA_FLGS_NOFLIGHT] == '0')
		{
			blFlightEnabled = false;
		}
	}
	return blFlightEnabled;
}

bool CedaDemandData::RuleIndependant(DEMANDDATA *prpDemand)
{
	bool blRuleIndependant = false;

	if(prpDemand != NULL)
	{
		if(prpDemand->Urud == 0L)
		{
			blRuleIndependant = true;
		}
	}
	return blRuleIndependant;
}

// set DEM.ALID to pcpAlid.
bool CedaDemandData::AssignAlid(long lpUdem, const char *pcpAlid, bool bpReleaseNow /*=false*/)
{
	bool blRc = false;
	DEMANDDATA *prlDemand = GetDemandByUrno(lpUdem);
	if(prlDemand != NULL)
	{
		DeleteDemandFromAlidMap(prlDemand->Alid,prlDemand->Urno);
		strcpy(prlDemand->Alid,pcpAlid);
		AddDemandToAlidMap(prlDemand);

		prlDemand->Lstu = ogBasicData.GetTime();
		strcpy(prlDemand->Useu,ogUsername.Left(31));
		
		CString olTmp;
		olTmp.Format("%ld,%s,%s,%s\n",lpUdem,pcpAlid,prlDemand->Lstu.Format("%Y%m%d%H%M%S"),prlDemand->Useu);
		omUdemAlidList += olTmp;
		blRc = true;
		if(bpReleaseNow)
		{
			ReleaseAlidAssignments();
		}
	}
	return blRc;
}

bool CedaDemandData::ReleaseAlidAssignments(void)
{
	bool blRc = false;
	if(!omUdemAlidList.IsEmpty())
	{
		char pclWhere[100] = "";
		char *pclData = new char[omUdemAlidList.GetLength()+10];
		strcpy(pclData,omUdemAlidList);
		if(!(blRc = CedaAction("UDR", pcmTableName, "URNO,ALID,LSTU,USEU", pclWhere,"", pclData)))
		{
			ogBasicData.Trace("CedaDemandData::UpdateDemandAllocationUnits() CedaAction Error\nomLastErrorMessage = <%s>\n",omLastErrorMessage);
			ogBasicData.Trace("Command <UDR> Table <%s> Fields <URNO,ALID>\n",pcmTableName);
			ogBasicData.Trace("Data <%s>\n",pclData);
		}

		// send notifications
		char *pclDelimiter = strchr(pclData,'\n');
		char *pclToken = pclData;
		while (pclDelimiter)
		{
			long llDemUrno = 0L;
			if (sscanf(pclToken,"%ld,",&llDemUrno) == 1)
			{
				DEMANDDATA *prlDemand = GetDemandByUrno(llDemUrno);
				if(prlDemand != NULL)
					ogCCSDdx.DataChanged((void *)this,DEMAND_CHANGE,(void *)prlDemand);
			}
			
			pclToken	 = pclDelimiter + 1;
			pclDelimiter = strchr(pclToken,'\n');
		}

		if(pclData != NULL)
		{
			delete [] pclData;
		}
		omUdemAlidList.Empty();
	}
	return blRc;
}

// loop through all demands and check if there should be a link between
// a personnel checkin demand and a location checkin demand
void CedaDemandData::LinkCheckinDemands(void)
{
	int ilNumDemands = omData.GetSize();
	for(int ilDemand = 0; ilDemand < ilNumDemands; ilDemand++)
	{
		LinkCheckinDemand(&omData[ilDemand]);
	}
	ReleaseAlidAssignments();
}

// if this demand is a Checkin personnel demand (checkin staff) and is not linked to
// to a checkin location demand (checkin counter) then link it
void CedaDemandData::LinkCheckinDemand(DEMANDDATA *prpDemand, bool bpReleaseNow /* = false */)
{
	bool blLinkCreated = false;

	if(prpDemand != NULL && !strcmp(prpDemand->Aloc,ALLOCUNITTYPE_CIC) &&
		!strcmp(prpDemand->Rety,PERSONNEL_DEMAND) && prpDemand->Ulnk == 0L && 
		strlen(prpDemand->Alid) <= 0)
	{
		// unassigned checkin personnel demand found
		CCADATA *prlCca = ogCcaData.GetBestCcaForDemand(prpDemand);
		if(prlCca != NULL)
		{
			ogBasicData.Trace("CedaDemandData::LinkCheckinDemand() Linking Personnel Demand <%ld> to Location <%s>\n",prpDemand->Urno,prlCca->Cnam);
			AssignAlid(prpDemand->Urno,prlCca->Cnam);
			if(bpReleaseNow)
			{
				ReleaseAlidAssignments();
			}
		}
	}
}

CString CedaDemandData::GetTableName(void)
{
	return CString(pcmTableName);
}

// if a new rule is found for a flight that has demands with jobs - 
// 1. new demands will be created for the new rule
// 2. old demands without jobs assigned will be deleted
// 3. old demands with jobs will be in conflict - "Job has expired demand"
bool CedaDemandData::HasExpired(DEMANDDATA *prpDemand)
{
	return (prpDemand != NULL && prpDemand->Flgs[CEDADEMANDDATA_FLGS_EXPIRED] == '1') ? true : false;
}


CString CedaDemandData::Dump(long lpUrno)
{
	CString olDumpStr;
	DEMANDDATA *prlDemand = GetDemandByUrno(lpUrno);
	if(prlDemand == NULL)
	{
		olDumpStr.Format("No DEMANDDATA Found for DEMAND.URNO <%ld>",lpUrno);
	}
	else
	{
		CString olData;
		MakeCedaData(&omRecInfo,olData,prlDemand);
		CStringArray olFieldArray, olDataArray;
		ogBasicData.ExtractItemList(pcmFieldList, &olFieldArray);
		ogBasicData.ExtractItemList(olData, &olDataArray);
		int ilNumFields = olFieldArray.GetSize();
		int ilNumData = olDataArray.GetSize();
		if(ilNumFields != ilNumData)
		{
			olDumpStr.Format("<%s>\n<%s>\n",pcmFieldList,olData);
		}
		else
		{
			for(int ilD = 0; ilD < ilNumData; ilD++)
			{
				olDumpStr += olFieldArray[ilD] + " = <" + olDataArray[ilD] + ">    ";
			}
		}

		// add virtual fields
		CString olTmpText;
		olTmpText.Format("UAZA = <%ld>    ",prlDemand->Uaza);
		olDumpStr += olTmpText;
		olTmpText.Format("RETY = <%s>    ",prlDemand->Rety);
		olDumpStr += olTmpText;
		olTmpText.Format("ULNK = <%ld>    ",prlDemand->Ulnk);
		olDumpStr += olTmpText;
		olTmpText.Format("URUE = <%ld>    ",prlDemand->Urue);
		olDumpStr += olTmpText;
	}
	return olDumpStr;
}

void CedaDemandData::GetFlightIndependentDemands(CCSPtrArray<DEMANDDATA>& ropFIDs)
{
	int ilTotDem = omData.GetSize();
	for (int i = 0; i < ilTotDem; i++)
	{
		if (strcmp("FID",omData[i].Obty) == 0)
		{
			ropFIDs.New(omData[i]);
		}
	}
}

bool CedaDemandData::IsTeamDemand(DEMANDDATA *prpDemand)
{
	bool blIsTeamDemand = false;

	// DEM.ULNK is used to link team demands unless the demand is used for
	// a checkin counter in which case it is used to link a personnel demand to a location demand
	if(prpDemand != NULL && prpDemand->Ulnk != 0L && strcmp(prpDemand->Aloc,ALLOCUNITTYPE_CIC))
	{
		blIsTeamDemand = true;
	}

	return blIsTeamDemand;
}

long CedaDemandData::GetFlightUrno(DEMANDDATA *prpDemand)
{
	long llFlightUrno = 0L;

	if(prpDemand != NULL)
	{
		if(*prpDemand->Dety == INBOUND_DEMAND || prpDemand->Ouro == 0L)
		{
			llFlightUrno = prpDemand->Ouri;
		}
		else
		{
			llFlightUrno = prpDemand->Ouro;
		}
	}

	return llFlightUrno;
}


void CedaDemandData::PrepareDataAfterRead(DEMANDDATA *prpDemand)
{
	if(prpDemand != NULL)
	{
		// UGDR --> comes from either DEMTAB.UDGR or RUDTAB.UDGR for P05 (DEMTAB.UDGR==AZATAB.URNO for P05)
		// Uaza --> URNO in AZATAB for P05, RES, CASP and 3rd Party demands
		if(HasAzatabRecord(prpDemand))
		{
			prpDemand->Uaza = prpDemand->Udgr;
		}
		RUDDATA *prlRud = ogRudData.GetRudByUrno(prpDemand->Urud);
		if(prlRud != NULL)
		{
			prpDemand->Udgr = prlRud->Udgr;
			prpDemand->Ulnk = prlRud->Ulnk;
			prpDemand->Urue = prlRud->Urue;
			strcpy(prpDemand->Rety,prlRud->Rety);
		}
	}
}

void CedaDemandData::PrepareDataForWrite(DEMANDDATA *prpDemand, const char *pcpText)
{
	if(prpDemand != NULL)
	{
		ConvertDatesToUtc(prpDemand);

		if(omDemandsWithAzatabRecords.Find(*prpDemand->Dety) != -1)
		{
			prpDemand->Udgr = prpDemand->Uaza;
		}

		CTime olCurrTime = ogBasicData.GetTime();
		CString olUserText;
		olUserText.Format("%s %s %s",ogUsername,pcpText,ogBasicData.GetVersion()); // username + version number and function number (which function did the change)

		if(prpDemand->Cdat == TIMENULL || strlen(prpDemand->Usec) <= 0)
		{
			prpDemand->Cdat = olCurrTime;
			strcpy(prpDemand->Usec,olUserText.Left(31));
		}
		else
		{
			prpDemand->Lstu = olCurrTime;
			strcpy(prpDemand->Useu,olUserText.Left(31));
		}
	}
}

int	 CedaDemandData::GetFlightsFromCciDemand(DEMANDDATA *prpDemand, CDWordArray& ropFlights)
{
	if (!prpDemand || prpDemand->Dety[0] != CCI_DEMAND)
		return 0;

	char pclFields[128];
	strcpy(pclFields,"URUD,DEBE,DEEN");
	char pclData[2048];

	CTime olDebe = ogBasicData.GetUtcFromLocal(prpDemand->Debe);
	CTime olDeen = ogBasicData.GetUtcFromLocal(prpDemand->Deen);
	sprintf(pclData,"%ld,%s,%s",prpDemand->Urud,olDebe.Format("%Y%m%d%H%M%S"),olDeen.Format("%Y%m%d%H%M%S"));
	if (!CedaAction("CDF","DEM",pclFields,"","",pclData,"BUF1"))
	{
		ogBasicData.LogCedaError("CCSBasicData::GetFlightsFromCciDemand Error",omLastErrorMessage,"CDF",pclFields,"DEM","");	
	}
	else
	{
		CString olBuf;
		int ilLine = 0;
		while(GetBufferLine(ilLine++, olBuf))
		{
//		for (int i = 0; i < omDataBuf.GetSize(); i++)
//		{
//			CString olBuf = omDataBuf[i];
			char *pclToken = strtok(olBuf.GetBuffer(0),"|");
			while(pclToken)
			{
				long llUrno = 0L;
				if (sscanf(pclToken,"%ld",&llUrno) == 1 && llUrno != 0)
				{
					ropFlights.Add(llUrno);
				}
				pclToken = strtok(NULL,"|");
			}
		}
	}
	return ropFlights.GetSize();	
}

// prpDemand - list of demand urnos
// ropFlights - string array used to return list of dem urnos and their flights
//              format: "DEM.URNO,FLIGHT.URNO1,...,FLIGHT.URNOn"
int	 CedaDemandData::GetFlightsFromCciDemand(CDWordArray &ropDemands, CStringArray &ropFlights)
{
	int ilNumDemands = ropDemands.GetSize();
	if(ilNumDemands <= 0)
		return 0;

	CStringArray olFlights;

	CString olData, olTmpStr;
	for(int ilD = 0; ilD < ilNumDemands; ilD++)
	{
		if(!olData.IsEmpty())
			olData += ",";
		olTmpStr.Format("%ld", ropDemands[ilD]);
		olData += olTmpStr;

		if((ilD != 0 && ((ilD+1) % 1000) == 0) || ilD == (ilNumDemands-1)) // max 1000 URNOs in a WHERE clause
		{
			char *pclData = new char[olData.GetLength() + 100];
			strcpy(pclData, olData);

			if (!CedaAction("FLC","DEM","","","",pclData,"BUF1"))
			{
				ogBasicData.LogCedaError("CCSBasicData::GetFlightsFromCciDemand Error",omLastErrorMessage,"FLC","","DEM","");	
			}
			else
			{
				CString olBuf;
				int ilLine = 0;
				while(GetBufferLine(ilLine++, olBuf))
					ogBasicData.ExtractItemList(olBuf, &olFlights, ';');
			}

			ropFlights.Append(olFlights);
			olFlights.RemoveAll();
			delete [] pclData;
			olData.Empty();
		}
	}

	return ropFlights.GetSize();	
}

bool CedaDemandData::HasAzatabRecord(DEMANDDATA *prpDemand)
{
	bool blHasAzatabRecord = false;
	if(prpDemand != NULL)
	{
		if(omDemandsWithAzatabRecords.Find(*prpDemand->Dety) != -1)
		{
			blHasAzatabRecord = true;
		}
	}
	return blHasAzatabRecord;
}

bool CedaDemandData::AutomaticAssignmentEnabled(DEMANDDATA *prpDemand)
{
	bool blAutomaticAssignmentEnabled = true;

	if(prpDemand != NULL)
	{
		if(prpDemand->Flgs[CEDADEMANDDATA_FLGS_NOAUTOASS] == '1')
		{
			blAutomaticAssignmentEnabled = false;
		}
	}
	return blAutomaticAssignmentEnabled;
}

bool CedaDemandData::EnableAutomaticAssignment(DEMANDDATA *prpDemand, bool bpEnable)
{
	char clEnable = bpEnable ? ' ' : '1';
	return SetFlgs(prpDemand, clEnable, CEDADEMANDDATA_FLGS_NOAUTOASS);
}

bool CedaDemandData::IsTurnaroundDemand(DEMANDDATA *prpDemand)
{
	return (prpDemand != NULL) ? IsTurnaroundDemand(prpDemand->Dety) : false;
}

bool CedaDemandData::IsTurnaroundDemand(const char *pcpDety)
{
	CString olDety;
	olDety.Format("|%s|",pcpDety);
	return (omTurnaroundDemandList.Find(olDety) != -1) ? true : false;
}

bool CedaDemandData::IsFlightDependingDemand(DEMANDDATA *prpDemand)
{
	if (prpDemand == NULL)
		return false;
	return IsFlightDependingDemand(prpDemand->Dety);
}

bool CedaDemandData::IsFlightDependingDemand(const char *pcpDety)
{
	if (pcpDety == NULL)
		return false;

	return (pcpDety[0] == TURNAROUND_DEMAND || pcpDety[0] == INBOUND_DEMAND || pcpDety[0] == OUTBOUND_DEMAND);
}

bool CedaDemandData::DemandIsDeactivated(long lpDemUrno)
{
	return DemandIsDeactivated(GetDemandByUrno(lpDemUrno));
}

bool CedaDemandData::DemandIsDeactivated(DEMANDDATA *prpDemand)
{
	bool DemandIsDeactivated = false;
	if(prpDemand != NULL)
	{
		if(prpDemand->Flgs[CEDADEMANDDATA_FLGS_DEACTIVATED] == '1')
		{
			DemandIsDeactivated = true;
		}
	}

	return DemandIsDeactivated;
}

bool CedaDemandData::DeactivateDemand(long lpDemUrno, bool bpDeactivate)
{
	return DeactivateDemand(GetDemandByUrno(lpDemUrno),bpDeactivate);
}

bool CedaDemandData::SetFlgs(DEMANDDATA *prpDemand, char cpValue, int ipIndex)
{
	bool blRc = false;
	if(prpDemand != NULL)
	{
		for(int ilIdx = 0; ilIdx < CEDADEMANDDATA_FLGSLEN; ilIdx++)
		{
			if(ipIndex == ilIdx)
			{
				prpDemand->Flgs[ilIdx] = cpValue;
			}
			else if(prpDemand->Flgs[ilIdx] == '\0')
			{
				prpDemand->Flgs[ilIdx] = ' ';
			}
		}

		prpDemand->Flgs[CEDADEMANDDATA_FLGSLEN-1] = '\0';


		char pclWhere[100] = "";

		char pclFieldList[100];
		strcpy(pclFieldList, "URNO,FLGS,USEU,LSTU");


		prpDemand->Lstu = ogBasicData.GetTime();
		strcpy(prpDemand->Useu,ogUsername.Left(31));

		char pclData[255];
		sprintf(pclData,"%ld,%s",prpDemand->Urno, prpDemand->Flgs, prpDemand->Useu, prpDemand->Lstu.Format("%Y%m%d%H%M%S"));

		if(!(blRc = CedaAction("UDR", pcmTableName, pclFieldList, pclWhere,"",pclData)))
		{
			ogBasicData.Trace("CedaDemandData::SetFlgs() CedaAction Error\nomLastErrorMessage = <%s>\n",omLastErrorMessage);
			ogBasicData.Trace("Command <UDR> Table <%s> Fields <%s>\n", pcmTableName, pclFieldList);
			ogBasicData.Trace("Data <%s>\n", prpDemand->Flgs);
		}
	}

	return blRc;

}

bool CedaDemandData::DeactivateDemand(DEMANDDATA *prpDemand, bool bpDeactivate)
{
	char clDeactivate = bpDeactivate ? '1' : '0';
	return SetFlgs(prpDemand, clDeactivate, CEDADEMANDDATA_FLGS_DEACTIVATED);
}

int CedaDemandData::GetCountOfDemandTypes()
{
	return NUM_DEMAND_TYPES;
}

CString CedaDemandData::GetStringForDemandType(DEMANDDATA &ropDemand)
{
	return GetStringForDemandType(ropDemand.Dety);
}

CString CedaDemandData::GetStringForDemandType(const char *pcpDemandType)
{
	return GetStringForDemandType(pcpDemandType[0]);
}

CString CedaDemandData::GetStringForDemandType(char cpDemandType)
{
	CString olDemandTypeText;
	if(cpDemandType == TURNAROUND_DEMAND)
	{
		olDemandTypeText = GetString(IDS_TURNAROUNDDEMAND); // Rotazione
	}
	else if(cpDemandType == INBOUND_DEMAND)
	{
		olDemandTypeText = GetString(IDS_INBOUNDDEMAND); // Arrivo
	}
	else if(cpDemandType == OUTBOUND_DEMAND)
	{
		olDemandTypeText = GetString(IDS_OUTBOUNDDEMAND); // Partenza
	}
	else if(cpDemandType == P05_DEMAND)
	{
		olDemandTypeText = GetString(IDS_P05DEMAND); // P05
	}
	else if(cpDemandType == P05RES_DEMAND)
	{
		olDemandTypeText = GetString(IDS_P05RESDEMAND); // P05Res
	}
	else if(cpDemandType == CAS_DEMAND)
	{
		olDemandTypeText = GetString(IDS_CASDEMAND); // Casp
	}
	else if(cpDemandType == '6')
	{
		olDemandTypeText = GetString(IDS_CCIDEMAND); // CCI
	}
	else if(cpDemandType == '7')
	{
		olDemandTypeText = GetString(IDS_FIDDEMAND); // FID
	}
	else if(cpDemandType == MANUAL_THIRDPARTY_DEMAND)
	{
		olDemandTypeText = GetString(IDS_MANUAL3RDDEMAND); // Manual 3rd Party
	}
	else if (cpDemandType == ' ')
	{
		olDemandTypeText = GetString(IDS_FIDDEMAND); // FID
	}
	else if (cpDemandType == '\0')
	{
		olDemandTypeText = GetString(IDS_FIDDEMAND); // FID
	}

	return olDemandTypeText;
}

CString CedaDemandData::GetDemandTypeFromString(const CString& opDemandTypeString)
{
	CString olDemandType;

	if (opDemandTypeString == GetString(IDS_TURNAROUNDDEMAND))
		olDemandType = "0";
	else if (opDemandTypeString == GetString(IDS_INBOUNDDEMAND))
		olDemandType = "1";
	else if (opDemandTypeString == GetString(IDS_OUTBOUNDDEMAND))
		olDemandType = "2";
	else if (opDemandTypeString == GetString(IDS_P05DEMAND))
		olDemandType = "3";
	else if (opDemandTypeString == GetString(IDS_P05RESDEMAND))
		olDemandType = "4";
	else if (opDemandTypeString == GetString(IDS_CASDEMAND))
		olDemandType = "5";
	else if (opDemandTypeString == GetString(IDS_CCIDEMAND))
		olDemandType = "6";
	else if (opDemandTypeString == GetString(IDS_FIDDEMAND))
		olDemandType = "7";
	else if (opDemandTypeString == GetString(IDS_MANUAL3RDDEMAND))
		olDemandType = "8";

	return olDemandType;
}

CString CedaDemandData::GetStringForResourceType(DEMANDDATA &ropDemand)
{
	CString olResourceTypeText;

	if(ogDemandData.DemandIsOfType(&ropDemand,PERSONNELDEMANDS))
	{
		olResourceTypeText = GetString(IDS_PERSONNEL_DEM);
	}
	else if(ogDemandData.DemandIsOfType(&ropDemand,EQUIPMENTDEMANDS))
	{
		olResourceTypeText = GetString(IDS_EQUIPMENT_DEM);
	}
	else if(ogDemandData.DemandIsOfType(&ropDemand,LOCATIONDEMANDS))
	{
		olResourceTypeText = GetString(IDS_LOCATION_DEM);
	}

	return olResourceTypeText;
}

CString CedaDemandData::GetStringForDemandAndResourceType(DEMANDDATA &ropDemand)
{
	CString olDemandType = GetStringForDemandType(ropDemand);
	CString olResourceType = GetStringForResourceType(ropDemand);
	return olDemandType + CString("/") + olResourceType;
}


// Demands may be loaded which overlap with the timeframe but their flights do not.
// Need to read these missing flights
void CedaDemandData::CheckForMissingFlights(void)
{
	int ilNumDemands = omData.GetSize();
	for(int ilD = 0; ilD < ilNumDemands; ilD++)
	{
		CheckForMissingFlight(&omData[ilD]);
		//CheckForMissingJob(&omData[ilD]);
	}

	ReadMissingFlights("Initial Load... Read Missing Flights");
	//ReadMissingJobs("Initial Load... Read Missing Jobs");
}

void CedaDemandData::CheckForMissingFlight(DEMANDDATA *prpDemand, const char *pcpText /* = NULL */, bool bpReadMissingFlight /* = false */)
{
	if(prpDemand != NULL)
	{
		if(*prpDemand->Dety == INBOUND_DEMAND || *prpDemand->Dety == TURNAROUND_DEMAND)
		{
			if(ogFlightData.GetFlightByUrno(prpDemand->Ouri) == NULL)
			{
				omMissingFlights.SetAt((void *) prpDemand->Ouri, NULL);
				omDemandsWhichHaveMissingFlights.Add(prpDemand->Urno);
			}
		}
		if(*prpDemand->Dety == OUTBOUND_DEMAND || *prpDemand->Dety == TURNAROUND_DEMAND)
		{
			if(ogFlightData.GetFlightByUrno(prpDemand->Ouro) == NULL)
			{
				omMissingFlights.SetAt((void *) prpDemand->Ouro, NULL);
				omDemandsWhichHaveMissingFlights.Add(prpDemand->Urno);
			}
		}

		if(bpReadMissingFlight)
		{
			ReadMissingFlights(pcpText);
		}
	}
}

void CedaDemandData::ReadMissingFlights(const char *pcpText)
{
	if(omMissingFlights.GetCount() > 0)
	{
		CString olUrnos, olTmp, olWhere = "WHERE URNO IN (%s)", olComma = ",";
		char pclSelection[2500];
		void *pvlDummy = NULL;
		long llFlur = 0L;

		for(POSITION rlPos = omMissingFlights.GetStartPosition(); rlPos != NULL; )
		{
			omMissingFlights.GetNextAssoc(rlPos, (void *&) llFlur, (void *& ) pvlDummy);
			olTmp.Format("'%d'",llFlur);
			if(!olUrnos.IsEmpty()) olUrnos += olComma;
			olUrnos += olTmp;
			if(olUrnos.GetLength() > 2000)
			{
				ogBasicData.MarkTime();
				sprintf(pclSelection, olWhere, olUrnos);
				ogFlightData.ReadAllFlights(TIMENULL, TIMENULL, pclSelection);
				olUrnos.Empty();
				ogBasicData.SaveTimeTakenForReread(pcpText, pclSelection);
			}
		}
		if(!olUrnos.IsEmpty())
		{
			ogBasicData.MarkTime();
			sprintf(pclSelection, olWhere, olUrnos);
			ogFlightData.ReadAllFlights(TIMENULL, TIMENULL, pclSelection);
			ogBasicData.SaveTimeTakenForReread(pcpText, pclSelection);
		}

		// The following code TRACES the missing flight/demands
		DEMANDDATA *prlDemand = NULL;
		FLIGHTDATA *prlFlight = NULL;
		CString olFlightTime;
		int ilNumDems = omDemandsWhichHaveMissingFlights.GetSize();
		for(int ilD = 0; ilD < ilNumDems; ilD++)
		{
			if((prlDemand = GetDemandByUrno(omDemandsWhichHaveMissingFlights[ilD])) != NULL)
			{
				if((prlFlight = ogFlightData.GetFlightByUrno(GetFlightUrno(prlDemand))) != NULL)
				{
					olFlightTime = (*prlFlight->Adid == 'A') ? prlFlight->Tifa.Format("%H:%M/%d") : prlFlight->Tifd.Format("%H:%M/%d");
//					ogBasicData.Trace("Read Missing Flight %s%s%s ADID <%s> TIME <%s> for Demand DEBE <%s> DEEN <%s> <%s> <%s>",
//						prlFlight->Alc2, prlFlight->Fltn, prlFlight->Flns, prlFlight->Adid, olFlightTime,
//						prlDemand->Debe.Format("%H:%M/%d"), prlDemand->Deen.Format("%H:%M/%d"), GetStringForDemandType(prlDemand->Dety), prlDemand->Aloc);
				}
			}
		}
		omMissingFlights.RemoveAll();
		omDemandsWhichHaveMissingFlights.RemoveAll();
	}
}

void CedaDemandData::CheckForMissingJob(DEMANDDATA *prpDemand, const char *pcpText /* = NULL */, bool bpReadMissingJob /* = false */)
{
	if(prpDemand != NULL)
	{
		CCSPtrArray <JODDATA> olJods;
		long llJobUrno = 0L;
		JOBDATA *prlJob = NULL;
		ogJodData.GetJodsByDemand(olJods, prpDemand->Urno);
		int ilNumJods = olJods.GetSize();
		for(int ilJod = 0; ilJod < ilNumJods; ilJod++)
		{
			llJobUrno = olJods[ilJod].Ujob;
			if((prlJob = ogJobData.GetJobByUrno(llJobUrno)) == NULL)
			{
				omMissingJobs.SetAt((void *) llJobUrno, NULL);
			}
		}

		if(bpReadMissingJob)
		{
			ReadMissingJobs(pcpText);
		}
	}
}

void CedaDemandData::ReadMissingJobs(const char *pcpText)
{
	if(omMissingJobs.GetCount() > 0)
	{
		CString olUrnos, olTmp, olWhere = "WHERE URNO IN (%s)", olComma = ",";
		char pclSelection[2500];
		void *pvlDummy = NULL;
		long llJobUrno = 0L;
		POSITION rlPos = NULL;

		for(rlPos = omMissingJobs.GetStartPosition(); rlPos != NULL; )
		{
			omMissingJobs.GetNextAssoc(rlPos, (void *&) llJobUrno, (void *& ) pvlDummy);
			olTmp.Format("'%d'",llJobUrno);
			if(!olUrnos.IsEmpty()) olUrnos += olComma;
			olUrnos += olTmp;
			if(olUrnos.GetLength() > 2000)
			{
				ogBasicData.MarkTime();
				sprintf(pclSelection, olWhere, olUrnos);
				ogJobData.Read(pclSelection, false);
				olUrnos.Empty();
				ogBasicData.SaveTimeTakenForReread(pcpText, pclSelection);
			}
		}
		if(!olUrnos.IsEmpty())
		{
			ogBasicData.MarkTime();
			sprintf(pclSelection, olWhere, olUrnos);
			ogJobData.Read(pclSelection, false);
			ogBasicData.SaveTimeTakenForReread(pcpText, pclSelection);
		}

		JOBDATA *prlJob = NULL;
		for(rlPos = omMissingJobs.GetStartPosition(); rlPos != NULL; )
		{
			omMissingJobs.GetNextAssoc(rlPos, (void *&) llJobUrno, (void *& ) pvlDummy);
			if((prlJob = ogJobData.GetJobByUrno(llJobUrno)) != NULL)
			{
				ogBasicData.Trace("Read missing job URNO <%ld> JTCO <%s> ALOC <%s> AVFR <%s> ACTO <%s>" , prlJob->Urno, prlJob->Jtco, prlJob->Aloc, prlJob->Acfr.Format("%H:%M/%d"), prlJob->Acto.Format("%H:%M/%d"));
			}
		}

		omMissingJobs.RemoveAll();
	}
}

void CedaDemandData::CheckForMissingFlightDemands(CDWordArray &ropFlightUrnos)
{
	CString olData, olTmpStr;
	int ilNumFlightUrnos = ropFlightUrnos.GetSize();
	for(int i = 0; i < ilNumFlightUrnos; i++)
	{
		if(!olData.IsEmpty())
			olData += ",";
		olTmpStr.Format("'%ld'", (long) ropFlightUrnos[i]);
		olData += olTmpStr;

		if((i != 0 && ((i+1) % 1000) == 0) || i == (ilNumFlightUrnos-1)) // max 1000 URNOs in a WHERE clause
		{
			int ilDataLen = olData.GetLength();
			char *pclUrnoList = new char[ilDataLen + 50];
			char *pclSelection = new char[ilDataLen + 50];
			memset(pclUrnoList, 0, sizeof(pclUrnoList));
			memset(pclSelection, 0, sizeof(pclSelection));

			strcpy(pclUrnoList, olData);
			if (ogBasicData.IsPrmHandlingAgentEnabled()) 
			{
				sprintf(pclSelection, "WHERE OURI IN (%s)  AND FILT = '%s'", pclUrnoList,
						ogPrmConfig.getHandlingAgentShortName());
			}
			else
			{
				sprintf(pclSelection, "WHERE OURI IN (%s)", pclUrnoList);
			}
			ReadDemands(pclSelection);

			if (ogBasicData.IsPrmHandlingAgentEnabled()) 
			{
				sprintf(pclSelection, "WHERE OURO IN (%s)  AND FILT = '%s'", pclUrnoList,
						ogPrmConfig.getHandlingAgentShortName());
			}
			else
			{
				sprintf(pclSelection, "WHERE OURO IN (%s)", pclUrnoList);
			}
			ReadDemands(pclSelection);
		
			delete [] pclUrnoList;
			delete [] pclSelection;
			olData.Empty();
		}
	}
}

bool CedaDemandData::IsCompleteFieldList(const char *pcpFields, int ipNumFieldsRequired)
{
	int ilLen = strlen(pcpFields), ilNumFields = 1;
	for(int i = 0; i < ilLen; i++)
	{
		if(pcpFields[i] == ',')
		{
			if(++ilNumFields >= ipNumFieldsRequired)
				return true;
		}
	}
	return false;
}



BOOL CedaDemandData::GetLoadFormat()
{
	omLoadFormat.Empty ();

	if (bgIsPrm)
	{
		omLoadFormat = "DEM";
	}
	else if (CedaAction("DLF","DEM","","","","","BUF1"))
	{
		CString olBuf;
		if(GetBufferLine(0, olBuf))
		{
//		if ( omDataBuf.GetSize() > 0 )
//		{
//			CString olBuf = omDataBuf[0];
			if ( olBuf.Find("DEM") >=0 )
				omLoadFormat = "DEM";
			else if ( olBuf.Find("RUD") >=0 )
				omLoadFormat = "RUD";
		}
	}
	return omLoadFormat.IsEmpty();	
}

bool CedaDemandData::FlightHasDemands(long lpFlur)
{
	CMapPtrToPtr *polSingleMap;
	if(omInboundFlightMap.Lookup((void *)lpFlur,(void *& )polSingleMap) ||
		omOutboundFlightMap.Lookup((void *)lpFlur,(void *& )polSingleMap))
		return true;

	return false;
}

bool CedaDemandData::LoadDemandsByFlurAndUtpl(CCSPtrArray <DEMANDDATA> &ropDemands, long lpFlur, long lpUtpl)
{
	omExtraData.DeleteAll();
	char pclCom[10] = "RT";
	sprintf(cgSelection, "WHERE (OURO = '%d' OR OURI = '%d') AND UTPL = '%d'", lpFlur, lpFlur, lpUtpl);
	if (CedaAction2(pclCom, cgSelection) == RCFailure)
	{
		ogBasicData.LogCedaError("CedaDemandData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,cgSelection);
		return false;
	}
	int ilCount = 0;
	bool blRc = true;
	for(int ilLc = 0; blRc; ilLc++)
	{
		DEMANDDATA *prlDemand = new DEMANDDATA;
		if ((blRc = GetBufferRecord2(ilLc,prlDemand)) == RCSuccess && prlDemand->Ucon == 0L)
		{
			if(!strcmp(prlDemand->Rety,PERSONNEL_DEMAND))
			{
				ConvertDatesToLocal(prlDemand);
				ropDemands.Add(prlDemand);
				omExtraData.Add(prlDemand);
			}
		}
	}

	return true;
}


bool CedaDemandData::LoadDemandsForDelegatedFlightJob(CCSPtrArray <DEMANDDATA> &ropDemands, CString opUdemList)
{
	omExtraData.DeleteAll();
	char pclCom[10] = "RT";
	sprintf(cgSelection, "WHERE URNO IN (%s)", opUdemList);
	if (CedaAction2(pclCom, cgSelection) == RCFailure)
	{
		ogBasicData.LogCedaError("CedaDemandData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,cgSelection);
		return false;
	}
	int ilCount = 0;
	bool blRc = true;
	for(int ilLc = 0; blRc; ilLc++)
	{
		DEMANDDATA *prlDemand = new DEMANDDATA;
		if ((blRc = GetBufferRecord2(ilLc,prlDemand)) == RCSuccess && prlDemand->Ucon == 0L)
		{
			ConvertDatesToLocal(prlDemand);
			ropDemands.Add(prlDemand);
			omExtraData.Add(prlDemand);
		}
	}

	return true;
}

bool CedaDemandData::DemandHasDelegatedFlight(long lpUdem)
{
	CCSPtrArray <JOBDATA> olJobs;
	ogJodData.GetJobsByDemand(olJobs, lpUdem);
	int x = olJobs.GetSize();
	for(int i = 0; i < x; i++)
	{
		JOBDATA *prlJob = &olJobs[i];
		//if(!strcmp(prlJob->Jtco,JOBDELEGATEDFLIGHT) || !strcmp(prlJob->Jtco,JOBRESTRICTEDFLIGHT))
		if(!strcmp(prlJob->Jtco,JOBDELEGATEDFLIGHT))
			return true;
	}

	return false;
}