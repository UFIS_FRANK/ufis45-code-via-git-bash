#ifndef __CFORMAT_H__
#define __CFORMAT_H__

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions (including VB)

class CFormat
{
  	private:
		char    m_point;
		char    m_seperate;
    	void    Init(void);
   	protected:                      
    	CString  omDataBuf; 
    public:    
  		void	Concat(const CFormat&);
    	void	Concat(const char*);
        void	Concat(const CString&);
    	CFormat(void);
   		CFormat(const char *s);
  		CFormat(const char *s,const char *fmt="%s");
   		CFormat(const int i,const char *fmt="%d");
   		CFormat(const int long l,const char *fmt="%ld");
   		CFormat(const float f,const char *fmt="%f");
   		CFormat(const double d,const char *fmt="%g");
   		virtual CFormat::~CFormat(void);
   		const CString& MakeFormat(const char *s,const char *fmt="%s");
   		const CString& MakeFormat(const int i,const char *fmt="%d");
   		const CString& MakeFormat(const int long l,const char *fmt="%ld");
   		const CString& MakeFormat(const float f,const char *fmt="%f");
   		const CString& MakeFormat(const double d,const char *fmt="%g");
   		const CString& Encode(const double d,const char *fmt="%g");
   		const CString& GetBuffer(void);
   		const void SetFormat(char pcSeperate=',',char pcPoint='.') ;
   		operator const char*() const;
   		friend CFormat operator+(const CFormat& as, const CFormat& bs) ;
    	friend CFormat operator+(const CFormat& as, const char* s) ;
    	friend CFormat operator+(const char* s, const CFormat& as) ;
    	friend CFormat operator+(const CFormat& as, const CString& s) ;
    	friend CFormat operator+(const CString& s,const CFormat& as) ;
};

#endif
