// DChange.cpp : implementation file
//

#include <stdafx.h>
#include <CcsGlobl.h>
#include <OpssPm.h>
#include <DChange.h>
#include <CCSTime.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DChange dialog


DChange::DChange(CWnd* pParent,CString opName,CTime opAcfr,CTime opActo)
	: CDialog(DChange::IDD, pParent)
{
	m_Acfr = opAcfr;
	m_Acto = opActo;
	m_Name = opName;
	
	m_FromDate = opAcfr;
	m_FromTime = opAcfr;
	m_ToDate   = opActo;
	m_ToTime   = opActo;

	//{{AFX_DATA_INIT(DChange)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void DChange::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DChange)
	DDX_Text(pDX, IDC_NAME, m_Name);
	DDX_CCSddmmyy(pDX, IDC_FROMDATE, m_FromDate);
	DDX_CCSTime(pDX, IDC_FROMTIME, m_FromTime);
	DDX_CCSddmmyy(pDX, IDC_TODATE, m_ToDate);
	DDX_CCSTime(pDX, IDC_TOTIME, m_ToTime);
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DChange, CDialog)
	//{{AFX_MSG_MAP(DChange)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DChange message handlers

void DChange::OnOK() 
{
	UpdateData(TRUE);

	m_Acfr = CTime (m_FromDate.GetYear(),m_FromDate.GetMonth(),m_FromDate.GetDay(),
					m_FromTime.GetHour(),m_FromTime.GetMinute(),m_FromTime.GetSecond());

	m_Acto = CTime (m_ToDate.GetYear(),m_ToDate.GetMonth(),m_ToDate.GetDay(),
					m_ToTime.GetHour(),m_ToTime.GetMinute(),m_ToTime.GetSecond());

	if (m_Acfr >= m_Acto)
	{
//		MessageBox("Ung�ltige Einsatzdauer");
		MessageBox(GetString(IDS_STRING61262));
	}
	else
	{
		EndDialog(IDOK);
	}
}

BOOL DChange::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if(omCaption.IsEmpty())
	{
		SetWindowText(GetString(IDS_STRING32903)); 
	}
	else
	{
		SetWindowText(omCaption);
	}

	CWnd *polWnd = GetDlgItem(IDC_TXT9_DUTYEMPLOYEE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32849));
	}
	polWnd = GetDlgItem(IDC_TXT9_DUTYFROM); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32845));
	}
	polWnd = GetDlgItem(IDC_TXT9_DUTYTO); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32846));
	}
	polWnd = GetDlgItem(IDOK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61697));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61698));
	}

	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
