// DemandFilterPage.h : header file
//
#ifndef _DEMANDFILTERPAGE_H_
#define _DEMANDFILTERPAGE_H_

#include <FilterPage.h>

/////////////////////////////////////////////////////////////////////////////
// DemandFilterPage dialog

class DemandFilterPage : public FilterPage
{
	DECLARE_DYNCREATE(DemandFilterPage)

// Construction
public:
	DemandFilterPage();
	~DemandFilterPage();

	void	SetSorted(BOOL bpSorted);

// Dialog Data
	//{{AFX_DATA(FilterPage)
	enum { IDD = IDD_FILTER_PAGE };
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(DemandFilterPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(DemandFilterPage)
	afx_msg void OnFilteradd();
	afx_msg void OnFilterremove();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	bool bmSorted;

};
#endif // _DEMANDFILTERPAGE_H_
