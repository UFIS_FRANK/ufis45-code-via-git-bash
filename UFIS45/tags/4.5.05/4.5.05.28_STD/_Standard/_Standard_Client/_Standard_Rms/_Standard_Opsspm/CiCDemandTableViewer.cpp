// CiCDemandTableViewer.cpp -- the viewer for Demand Table
//
 
#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaAloData.h>
#include <CedaDemandData.h>
#include <OpssPm.h>
#include <ccstable.h>
#include <CedaJobData.h>
#include <CedaJodData.h>
#include <CedaFlightData.h>
#include <CedaEmpData.h>
#include <PrintControl.h>
#include <CiCDemandTableViewer.h>
#include <BasicData.h>
#include <DataSet.h>
#include <ccsddx.h>
#include <CedaRudData.h>
#include <CedaRpqData.h>
#include <CedaPfcdata.h>
#include <CedaPerData.h>
#include <CedaAcrData.h>
#include <CedaCccData.h>
#include <CedaRloData.h>
#include <CedaCicData.h>
#include <CedaAltData.h>

#include <Buttonlist.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsWithIn(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))

// Grouping and Sorting criterias definition:
// This will make the program faster a little bit because it will replace string comparison.
enum {
	BY_DETY,
	BY_DEBE,
	BY_DEEN,
	BY_DEDU,
	BY_DFNUM,
	BY_ULNK,
	BY_NONE
};



static struct CICDEMDATA_FIELD osfields[]	= 
	{
		{	CICDEMDATA_FIELD("DETY","",		100,	250)	},
		{	CICDEMDATA_FIELD("DFNUM","",	80,		250)	},
		{	CICDEMDATA_FIELD("DEBE","",		50,		100)	},
		{	CICDEMDATA_FIELD("DEEN","",		50,		100)	},
		{	CICDEMDATA_FIELD("DEDU","",		50,		150)	},
		{	CICDEMDATA_FIELD("FCT","",		80,		250)	},
		{	CICDEMDATA_FIELD("PERMITS","",	80,		250)	},
		{	CICDEMDATA_FIELD("ULNK","",		80,		150)	},
		{	CICDEMDATA_FIELD("CLASS","",	200,	300)	},
		{	CICDEMDATA_FIELD("COUNTERS","",	200,	300)	},
		{	CICDEMDATA_FIELD("COUNTERSFIPS","",	80,	80)	},
	};

/////////////////////////////////////////////////////////////////////////////
// CicDemandTableViewer

CicDemandTableViewer::CicDemandTableViewer(CciDiagramViewer *popParentViewer)
{
    SetViewerKey("CicDemTab");
	pomParentViewer = popParentViewer;
    pomTable = NULL;
    ogCCSDdx.Register(this, DEMAND_CHANGE,	CString("CICDTVIEWER"), CString("Demand New"),	CicDemandTableCf);
    ogCCSDdx.Register(this, DEMAND_NEW,	CString("CICDTVIEWER"), CString("Demand Change"),	CicDemandTableCf);
    ogCCSDdx.Register(this, DEMAND_DELETE,	CString("CICDTVIEWER"), CString("Demand Delete"),	CicDemandTableCf);
	ogCCSDdx.Register(this, FLIGHT_CHANGE,	CString("CICDTVIEWER"), CString("Flight Change"),	CicDemandTableCf);
	ogCCSDdx.Register(this, FLIGHT_SELECT,	CString("CICDTVIEWER"), CString("Flight Select"),	CicDemandTableCf);
	
	CTime olDay = ogBasicData.GetTimeframeStart();
	omStartTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0);
	omEndTime   = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59);

	bmUseAllDetys	 = true;
	bmCompressGroups = false;
	bmUseAllAirlines = true;

	// must initialize names here because resource handle is not available during static initialization
	if (!osfields[0].Name.GetLength())
	{
		osfields[0].Name = GetString(IDS_STRING61879);
		osfields[1].Name = GetString(IDS_STRING61882);
		osfields[2].Name = GetString(IDS_STRING61883);
		osfields[3].Name = GetString(IDS_STRING61884);
		osfields[4].Name = GetString(IDS_STRING61885);
		osfields[5].Name = GetString(IDS_STRING61886);
		osfields[6].Name = GetString(IDS_STRING61887);
		osfields[7].Name = GetString(IDS_STRING61891);
		osfields[8].Name = GetString(IDS_STRING61957);
		osfields[9].Name = GetString(IDS_STRING61958);
		osfields[10].Name = GetString(IDS_COUNTERSFIPS);
	}

}

CicDemandTableViewer::~CicDemandTableViewer()
{
	TRACE("CicDemandTableViewer: DDX Unregistration \n");
    ogCCSDdx.UnRegister(this, NOTUSED);
	omTableFields.DeleteAll();
	DeleteAll();
}

void CicDemandTableViewer::Attach(CCSTable *popTable)
{
	pomTable = popTable;
}

void CicDemandTableViewer::ChangeViewTo(const char *pcpViewName, CString opDate)
{
	omDate = opDate;
	ChangeViewTo(pcpViewName);
}

void CicDemandTableViewer::ChangeViewTo(const char *pcpViewName)
{
	ogCfgData.rmUserSetup.CCDV = pcpViewName;
    SelectView(pcpViewName);
    PrepareFilter();
    PrepareSorter();
    PrepareGrouping();
    DeleteAll();    

	EvaluateTableFields();
    MakeLines();
	UpdateDisplay();
}

void CicDemandTableViewer::CompressGroups(bool blCompress)
{
	if (blCompress != bmCompressGroups)
	{
		bmCompressGroups = blCompress;
		if (bmCompressGroups)
		{
			CCSPtrArray<CICDEMDATA_LINE> olLinesToRemove;
			for (int i = 0; i < omLines.GetSize(); i++)
			{
				CICDEMDATA_LINE *prlLine = &omLines[i];
				if (prlLine && prlLine->IsTeamLine)
				{
					olLinesToRemove.Append(prlLine->TeamLines);					
					CompressGroup(prlLine);
				}
			}
			
			for (i = 0; i < olLinesToRemove.GetSize(); i++)
			{
				RemoveLine(&olLinesToRemove[i]);	// remove lines from display, but keep them in team list				
			}
		}
		else
		{
			CCSPtrArray<CICDEMDATA_LINE> olOldLines = omLines;

			for (int i = 0; i < olOldLines.GetSize(); i++)
			{
				CICDEMDATA_LINE *prlLine = &olOldLines[i];
				if (prlLine && prlLine->IsTeamLine)
				{
					ExpandGroup(prlLine);
				}
			}
		}

		UpdateDisplay(FALSE);
	}
}

void CicDemandTableViewer::CompressGroup(CICDEMDATA_LINE *prpGroup)
{
	if (!prpGroup || !prpGroup->IsTeamLine)
		return;

	prpGroup->Debe = prpGroup->OriDebe;
	prpGroup->Deen = prpGroup->OriDeen;
	prpGroup->Dedu = prpGroup->OriDedu;
	prpGroup->Staff= 1;
	prpGroup->Expanded = false;

	for (int i = 0; i < prpGroup->TeamLines.GetSize(); i++)
	{
		CICDEMDATA_LINE *prlLine = &prpGroup->TeamLines[i];
		if (prlLine)
		{
			// recalculate team leader line
			if (prlLine->Debe < prpGroup->Debe)
				prpGroup->Debe = prlLine->Debe;
			if (prlLine->Deen > prpGroup->Deen)
				prpGroup->Deen = prlLine->Deen;
			prpGroup->Dedu = prpGroup->Deen - prpGroup->Debe;
			prpGroup->Staff += prlLine->Staff;
		}
	}

	ChangeLine(prpGroup);
}

void CicDemandTableViewer::ExpandGroup(CICDEMDATA_LINE *prpGroup)
{
	if (!prpGroup || !prpGroup->IsTeamLine)
		return;

	for (int i = 0; i < prpGroup->TeamLines.GetSize(); i++)
	{
		CICDEMDATA_LINE *prlLine = &prpGroup->TeamLines[i];
		if (prlLine)
		{
			int ilLinenno = InsertLine(prlLine);			
		}
	}

	prpGroup->Debe = prpGroup->OriDebe;
	prpGroup->Deen = prpGroup->OriDeen;
	prpGroup->Dedu = prpGroup->OriDedu;
	prpGroup->Staff= 1;
	prpGroup->Expanded = true;

	ChangeLine(prpGroup);
}

int CicDemandTableViewer::RemoveLine(CICDEMDATA_LINE *prlLine)
{
	if (!prlLine)
		return -1;

	for (int i = 0; i < omLines.GetSize(); i++)
	{
		if (omLines[i].DemUrno == prlLine->DemUrno)
		{
			omMapUrnoToLine.SetAt(reinterpret_cast<void *>(prlLine->DemUrno),NULL);
			if (pomTable)
				pomTable->DeleteTextLine(i);
			omLines.DeleteAt(i);
			return i;
		}
	}

	return -1;
}


int CicDemandTableViewer::ChangeLine(CICDEMDATA_LINE *prlLine,BOOL bpResort)
{
	if (!prlLine)
		return -1;

	// must we resort the changed line ?
	if (bpResort)
	{
		for (int i = 0; i < omLines.GetSize(); i++)
		{
			if (omLines[i].DemUrno == prlLine->DemUrno)
			{
				CCSPtrArray<TABLE_COLUMN> rrlColumns;
				for (int ilC = 0; ilC < omTableFields.GetSize(); ilC++)
				{
					rrlColumns.NewAt(ilC,TABLE_COLUMN());
				}

				Format(prlLine,rrlColumns);

				CICDEMDATA_LINE *prlDisplayedLine = &omLines[i];
				if (prlDisplayedLine != prlLine)
				{
					*prlDisplayedLine = *prlLine;
				}

				omLines.RemoveAt(i);				
				if (pomTable)
					pomTable->DeleteTextLine(i);
				int ilLineCount = omLines.GetSize();

#ifndef SCANBACKWARD
				for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
				{
					if (CompareDemand(prlLine, &omLines[ilLineno]) <= 0)
						break;  
				}
#else
				for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
				{
					if (CompareDemand(prlLine, &omLines[ilLineno-1]) >= 0)
						break;  
				}
#endif

				omLines.InsertAt(ilLineno,prlDisplayedLine);
				if (pomTable)
					pomTable->InsertTextLine(ilLineno,rrlColumns,&omLines[ilLineno]);

				rrlColumns.DeleteAll();
				return ilLineno;
			}
		}
	}
	else	// no resort neccessary
	{
		for (int i = 0; i < omLines.GetSize(); i++)
		{
			if (omLines[i].DemUrno == prlLine->DemUrno)
			{
				CCSPtrArray<TABLE_COLUMN> rrlColumns;
				if (pomTable)
					pomTable->GetTextLineColumns(&rrlColumns,i);

				Format(prlLine,rrlColumns);

				CICDEMDATA_LINE *prlDisplayedLine = &omLines[i];
				if (prlDisplayedLine != prlLine)
				{
					*prlDisplayedLine = *prlLine;
				}

				if (pomTable)
					pomTable->ChangeTextLine(i,&rrlColumns,&omLines[i]);
				return i;
			}
		}
	}
	return -1;
}

BOOL CicDemandTableViewer::FindTeamLeaderOf(DEMANDDATA *prpDem,int &ripLineno)
{
	if (!prpDem || prpDem->Ulnk == 0)
		return FALSE;

	for (int i = 0; i < omLines.GetSize(); i++)
	{
		// is it the team leader itself ?
		if (omLines[i].DemUrno == prpDem->Urno)
		{
			ripLineno = i;
			return TRUE;
		}

		// same group ?
		if (omLines[i].Ulnk == prpDem->Ulnk)
		{
			for (int j = 0; j < omLines[i].TeamLines.GetSize(); j++)			
			{
				if (omLines[i].TeamLines[j].DemUrno == prpDem->Urno)
				{
					ripLineno = i;
					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

BOOL CicDemandTableViewer::FindTeamLeaderOf(long lpDemUrno,int &ripLineno)
{
	for (int i = 0; i < omLines.GetSize(); i++)
	{
		// is it the team leader itself ?
		if (omLines[i].DemUrno == lpDemUrno)
		{
			if (omLines[i].Ulnk == 0)
				return FALSE;
			else
			{
				ripLineno = i;
				return TRUE;
			}
		}

		// is this line a team line ?
		if (omLines[i].Ulnk != 0 && omLines[i].IsTeamLine)
		{
			for (int j = 0; j < omLines[i].TeamLines.GetSize(); j++)			
			{
				if (omLines[i].TeamLines[j].DemUrno == lpDemUrno)
				{
					ripLineno = i;
					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

// find team of a newly created demand
BOOL CicDemandTableViewer::FindTeamOf(DEMANDDATA *prpDem,int &ripLineno)
{
	if (!prpDem || prpDem->Ulnk == 0)
		return FALSE;

	for (int i = 0; i < omLines.GetSize(); i++)
	{
		// only groups are interesting
		if (omLines[i].Ulnk == 0)
			continue;

		// only team leders are interesting
		if (!omLines[i].IsTeamLine)
			continue;

		if (omLines[i].Ulnk != prpDem->Ulnk)
			continue;

		if (strcmp(omLines[i].Obty,prpDem->Obty) != 0)
			continue;

		DEMANDDATA *prlTeamDem = ogDemandData.GetDemandByUrno(omLines[i].DemUrno);
		if (!prlTeamDem)
			continue;

		BOOL blSameGroup = prlTeamDem->Ouri == prpDem->Ouri;
		blSameGroup = blSameGroup && prlTeamDem->Ouro == prpDem->Ouro;

		// same group ?
		if (blSameGroup)
		{
			ripLineno = i;
			return TRUE;
		}
	}

	return FALSE;
}

void CicDemandTableViewer::PrepareGrouping()
{
    CStringArray olSortOrder;
    CViewer::GetSort(olSortOrder);  // we'll always use the first sorting criteria for grouping
	BOOL blIsGrouped = *CViewer::GetGroup() - '0';

	imGroupBy = BY_NONE;
    if (olSortOrder.GetSize() > 0 && blIsGrouped)
    {
        if (olSortOrder[0] == "Dety")
            imGroupBy = BY_DETY;
        else if (olSortOrder[0] == "Debe")
            imGroupBy = BY_DEBE;
        else if (olSortOrder[0] == "Deen")
            imGroupBy = BY_DEEN;
        else if (olSortOrder[0] == "Dedu")
            imGroupBy = BY_DEDU;
        else if (olSortOrder[0] == "DFnum")
            imGroupBy = BY_DFNUM;
        else if (olSortOrder[0] == "Ulnk")
            imGroupBy = BY_ULNK;
    }
}


void CicDemandTableViewer::PrepareFilter()
{
    CStringArray olFilterValues;
    int i, n;

    GetFilter("Dety", olFilterValues);
    omCMapForDety.RemoveAll();
    n = olFilterValues.GetSize();
	if (n >= 1 && olFilterValues[0] == GetString(IDS_ALLSTRING))
		bmUseAllDetys = true;
	else
	{
		bmUseAllDetys = false;
		for (i = 0; i < n; i++)
		{
			omCMapForDety[ogDemandData.GetDemandTypeFromString(olFilterValues[i])] = NULL;
		}
	}

    GetFilter("Airline", olFilterValues);
    omCMapForAlcd.RemoveAll();
    n = olFilterValues.GetSize();
	if (n >= 1 && olFilterValues[0] == GetString(IDS_ALLSTRING))
		bmUseAllAirlines = true;
	else
	{
		bmUseAllAirlines = false;
		for (i = 0; i < n; i++)
			omCMapForAlcd[olFilterValues[i]] = NULL;
	}

    GetFilter("Functions", olFilterValues);
    omCMapForFunctions.RemoveAll();
    n = olFilterValues.GetSize();
	if (n >= 1 && olFilterValues[0] == GetString(IDS_ALLSTRING))
		bmUseAllFunctions = true;
	else
	{
		bmUseAllFunctions = false;
		for (i = 0; i < n; i++)
			omCMapForFunctions[olFilterValues[i]] = NULL;
	}

    GetFilter("Classes", olFilterValues);
	CStringArray olClassNames;
	ogCccData.GetClassNameList(olFilterValues,olClassNames);

    omCMapForClass.RemoveAll();
    n = olClassNames.GetSize();
	if (n >= 1 && olClassNames[0] == GetString(IDS_ALLSTRING))
		bmUseAllClasses = true;
	else
	{
		bmUseAllClasses = false;
		for (i = 0; i < n; i++)
		{
			omCMapForClass[olClassNames[i]] = NULL;
		}
	}

	if (!pomParentViewer)
	{
		bmUseAllGroups = true;
	}
	else
	{
		GetFilter("Groups",olFilterValues);
		omCMapForGroups.RemoveAll();
		n = olFilterValues.GetSize();
		if (n >= 1 && olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllGroups = true;
		else
		{
			bmUseAllGroups = false;
			for (i = 0; i < n; i++)
			{
				CCSPtrArray<CICDATA> olCounters; 
				pomParentViewer->GetCountersByGroup(olFilterValues[i],olCounters);
				for (int j = 0; j < olCounters.GetSize(); j++)
					omCMapForGroups[(void *)olCounters[j].Urno] = NULL;
			}
		}
	}	
}

void CicDemandTableViewer::PrepareSorter()
{
    CStringArray olSortOrder;
    CViewer::GetSort(olSortOrder);

    omSortOrder.RemoveAll();


    for (int i = 0; i < olSortOrder.GetSize(); i++)
    {
        int ilSortOrderEnumeratedValue;

        if (olSortOrder[i] == "Dety")
            ilSortOrderEnumeratedValue = BY_DETY;
        else if (olSortOrder[i] == "Debe")
            ilSortOrderEnumeratedValue = BY_DEBE;
        else if (olSortOrder[i] == "Deen")
            ilSortOrderEnumeratedValue = BY_DEEN;
        else if (olSortOrder[i] == "Dedu")
            ilSortOrderEnumeratedValue = BY_DEDU;
        else if (olSortOrder[i] == "DFnum")
            ilSortOrderEnumeratedValue = BY_DFNUM;
		else
            ilSortOrderEnumeratedValue = BY_NONE;

        omSortOrder.Add(ilSortOrderEnumeratedValue);
    }
}

int CicDemandTableViewer::CompareDemand(CICDEMDATA_LINE *prpDem1, CICDEMDATA_LINE *prpDem2)
{
    int n = omSortOrder.GetSize();
    for (int i = 0; i < n; i++)
    {
        int ilCompareResult = 0;

        switch (omSortOrder[i])
        {
        case BY_DETY:
            ilCompareResult = strcmp(prpDem1->Dety,prpDem2->Dety);
            break;
        case BY_DEBE:
            ilCompareResult = (prpDem1->Debe == prpDem2->Debe)? 0:
                (prpDem1->Debe > prpDem2->Debe)? 1: -1;
            break;
        case BY_DEEN:
            ilCompareResult = (prpDem1->Deen == prpDem2->Deen)? 0:
                (prpDem1->Deen > prpDem2->Deen)? 1: -1;
            break;
        case BY_DEDU:
            ilCompareResult = (prpDem1->Dedu == prpDem2->Dedu)? 0:
                (prpDem1->Dedu > prpDem2->Dedu)? 1: -1;
            break;
        case BY_DFNUM:
            ilCompareResult = strcmp(prpDem1->DFnum,prpDem2->DFnum);
            break;
        case BY_ULNK:
            ilCompareResult = (prpDem1->Ulnk == prpDem2->Ulnk)? 0:
                (prpDem1->Ulnk > prpDem2->Ulnk)? 1: -1;
            break;
        }
        if (ilCompareResult != 0)
            return ilCompareResult;
    }

    return 0;   
}


BOOL CicDemandTableViewer::IsSameGroup(CICDEMDATA_LINE *prpDem1,CICDEMDATA_LINE *prpDem2)
{
    // Compare in the sort order, from the outermost to the innermost
    switch (imGroupBy)
    {
    case BY_DETY:
        return strcmp(prpDem1->Dety,prpDem2->Dety) == 0;
    case BY_DEBE:
        return (prpDem1->Debe == prpDem2->Debe);
    case BY_DEEN:
        return (prpDem1->Deen == prpDem2->Deen);
    case BY_DEDU:
        return (prpDem1->Dedu == prpDem2->Dedu);
    case BY_DFNUM:
        return strcmp(prpDem1->DFnum,prpDem2->DFnum) == 0;
    case BY_ULNK:
		if (prpDem1->Dety[0] == prpDem2->Dety[0])
		{
			if (prpDem1->Dety[0] == '2')
			{
				if (strcmp(prpDem1->DFnum,prpDem2->DFnum) == 0)
				{
					if (!prpDem1->Ulnk)
						return FALSE;

					if (!prpDem2->Ulnk)
						return FALSE;

					return (prpDem1->Ulnk == prpDem2->Ulnk);
				}
				else
					return FALSE;
			}
			else
				return FALSE;
		}
		else
			return FALSE;
    }

    return TRUE;    // assume there's no grouping at all
}


BOOL CicDemandTableViewer::IsPassFilter(CICDEMDATA_LINE *prpLine)
{
    void *p;
    BOOL blIsDetyOk = bmUseAllDetys ? true : prpLine->Dety[0] == '\0' ? omCMapForDety.Lookup(CString("7"),p) : omCMapForDety.Lookup(CString(prpLine->Dety), p);
	BOOL blIsAlcdOk = bmUseAllAirlines ? true : (ogDemandData.IsFlightDependingDemand(prpLine->Dety) ? omCMapForAlcd.Lookup(prpLine->Alc2,p) : true);
	BOOL blIsClassOk= bmUseAllClasses || (prpLine->Class.IsEmpty() ? omCMapForClass.Lookup(GetString(IDS_UNDEFINED),p) : omCMapForClass.Lookup(prpLine->Class,p));
	BOOL blIsGroupOk= bmUseAllGroups;
	BOOL blIsFunctionOk = bmUseAllFunctions;
	if(!bmUseAllFunctions)
	{
		for(int ilF = 0; ilF < prpLine->Functions.GetSize(); ilF++)
		{
			if(omCMapForFunctions.Lookup(prpLine->Functions[ilF],p))
			{
				blIsFunctionOk = true;
				break;
			}
		}
	}
	if (!bmUseAllGroups)
	{
		CDWordArray olCounters;
		DEMANDDATA *prlDem = ogDemandData.GetDemandByUrno(prpLine->DemUrno);
		ogBasicData.GetCounters(prlDem,olCounters);
		void *polValue;
		for (int i = 0; !blIsGroupOk && i < olCounters.GetSize(); i++)
		{
			blIsGroupOk = omCMapForGroups.Lookup((void *)olCounters[i],polValue);
		}
	}

	BOOL blIsTimeOk = IsReallyOverlapped(prpLine->Debe,prpLine->Deen,omStartTime,omEndTime);

    return (blIsDetyOk && blIsAlcdOk && blIsClassOk && blIsGroupOk && blIsTimeOk && blIsFunctionOk);
}

/////////////////////////////////////////////////////////////////////////////
// CciTableViewer -- code specific to this class

void CicDemandTableViewer::MakeLines()
{
	int ilNumDemands = ogDemandData.omData.GetSize();
	for(int ilDemand = 0; ilDemand < ilNumDemands; ilDemand++)
	{
		DEMANDDATA *prlDem = &ogDemandData.omData[ilDemand];
		// check, if personnel demand and CIC demand type only
		if (strcmp(prlDem->Aloc,ALLOCUNITTYPE_CIC) == 0 && strlen(prlDem->Alid) <= 0 && strcmp(prlDem->Rety,PERSONNEL_DEMAND) == 0)
		{
			MakeLine(prlDem);
		}
	}
}

void CicDemandTableViewer::MakeLine(DEMANDDATA *prpDemand,CICDEMDATA_LINE& opLine)
{
	strcpy(opLine.Dety,prpDemand->Dety);
	opLine.Debe = prpDemand->Debe;
	opLine.Deen = prpDemand->Deen;
	opLine.Dedu = prpDemand->Deen - prpDemand->Debe;
	opLine.OriDebe = prpDemand->Debe;
	opLine.OriDeen = prpDemand->Deen;
	opLine.OriDedu = prpDemand->Deen - prpDemand->Debe;
	opLine.Ulnk = prpDemand->Ulnk;
	opLine.DemUrno = prpDemand->Urno;
	strcpy(opLine.Obty,prpDemand->Obty);
	opLine.Class = GetCounterClass(prpDemand);
	opLine.CountersRules = GetCountersRules(prpDemand);
	opLine.Staff= 1;

	if (ogDemandData.IsFlightDependingDemand(prpDemand))
	{
		FLIGHTDATA *prlFlight;
		prlFlight = ogFlightData.GetFlightByUrno(prpDemand->Ouro);
		if (prlFlight)
		{
			strcpy(opLine.Alc2,ogAltData.FormatAlcString(prlFlight->Alc2,prlFlight->Alc3));
			strcpy(opLine.DFnum,prlFlight->Fnum);

			opLine.CountersFips = ogCcaData.GetCounterRangeForFlight(prlFlight->Urno);
		}
	}

	RPFDATA *prlDemFct = ogRpfData.GetFunctionByUrud(prpDemand->Urud);
	if (prlDemFct)
	{
		if (strcmp(prlDemFct->Fcco,"") == 0)
		{
			opLine.Fcco = "";
			// group of optional functions
			CCSPtrArray <SGMDATA> olSgms;
			ogSgmData.GetSgmListByUsgr(prlDemFct->Upfc,olSgms);
			int ilNumSgms = olSgms.GetSize();
			for(int ilSgm = 0; ilSgm < ilNumSgms; ilSgm++)
			{
				PFCDATA *prlPfc = ogPfcData.GetPfcByUrno(olSgms[ilSgm].Uval);
				if(prlPfc != NULL)
				{
					if (!opLine.Fcco.IsEmpty())
						opLine.Fcco += ',';
					opLine.Fcco += prlPfc->Fctc;
					opLine.Functions.Add(prlPfc->Fctc);
				}
			}

		}
		else
		{
			opLine.Fcco = prlDemFct->Fcco;
			opLine.Functions.Add(prlDemFct->Fcco);
		}
	}

	CCSPtrArray<RPQDATA> olDemandPermits;
	CCSPtrArray<RPQDATA> olDemandPermits2;
	ogRpqData.GetRpqsByUrud(prpDemand->Urud,olDemandPermits);
	ogRpqData.GetRpqsByUdgr(prpDemand->Udgr,olDemandPermits2);
	olDemandPermits.Append(olDemandPermits2);
	for (int ilC = 0; ilC < olDemandPermits.GetSize(); ilC++)
	{
		RPQDATA *prlDemandPermit = &olDemandPermits[ilC];
		if (prlDemandPermit)
		{
			if (strcmp(prlDemandPermit->Quco,"") == 0)
			{
				opLine.Permits = "";
				// this is a header for a group of permits, one of which the emp must have
				CCSPtrArray <SGMDATA> olSgms;
				ogSgmData.GetSgmListByUsgr(prlDemandPermit->Uper,olSgms);
				int ilNumSgms = olSgms.GetSize();
				for(int ilSgm = 0; ilSgm < ilNumSgms; ilSgm++)
				{
					PERDATA *prlPer = ogPerData.GetPerByUrno(olSgms[ilSgm].Uval);
					if(prlPer != NULL)
					{
						if (!opLine.Permits.IsEmpty())
							opLine.Permits += ',';
						opLine.Permits += prlPer->Prmc;
					}
				}
			}
			else	// single permit
				opLine.Permits = prlDemandPermit->Quco;
		}
	}

}

void CicDemandTableViewer::MakeLine(DEMANDDATA *prpDemand)
{
	CICDEMDATA_LINE olLine;

	MakeLine(prpDemand,olLine);

	if (IsPassFilter(&olLine))
	{
		int ilLineno = CreateLine(&olLine);
	}
}

void CicDemandTableViewer::MakeLine(CCSPtrArray<DEMANDDATA>& ropTeamDemands)
{
	CICDEMDATA_LINE olTeamLine;
	for (int i = 0; i < ropTeamDemands.GetSize(); i++)
	{
		CICDEMDATA_LINE olLine;
		MakeLine(&ropTeamDemands[i],olLine);
		if (!IsPassFilter(&olLine))
			continue;
		if (olTeamLine.DemUrno == 0)
		{
			olTeamLine = olLine;
			olTeamLine.IsTeamLine = true;
			olTeamLine.Expanded = !bmCompressGroups;
		}
		else
		{
			// add group demand to team leader
			olTeamLine.TeamLines.New(olLine);
			// add demand to map to prevent from adding it multiple times
			omMapUrnoToLine.SetAt(reinterpret_cast<void *>(ropTeamDemands[i].Urno),NULL);

			if (bmCompressGroups)
			{
				// recalculate team leader line
				if (olLine.Debe < olTeamLine.Debe)
					olTeamLine.Debe = olLine.Debe;
				if (olLine.Deen > olTeamLine.Deen)
					olTeamLine.Deen = olLine.Deen;
				olTeamLine.Dedu = olTeamLine.Deen - olTeamLine.Debe;
				olTeamLine.Staff += olLine.Staff;
			}
			else
			{
				int ilLineno = CreateLine(&olLine);
			}
		}
	}

	if (olTeamLine.DemUrno != 0)
	{
		int ilLineno = CreateLine(&olTeamLine);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CicDemandTableViewer - DEMDATA_LINEDATA array maintenance

void CicDemandTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);

	omMapUrnoToLine.RemoveAll();

}

int CicDemandTableViewer::CreateLine(CICDEMDATA_LINE *prpDem)
{
    int ilLineCount = omLines.GetSize();

#ifndef SCANBACKWARD
    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
        if (CompareDemand(prpDem, &omLines[ilLineno]) <= 0)
            break;  
#else
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareDemand(prpDem, &omLines[ilLineno-1]) >= 0)
            break;  
#endif

    omLines.NewAt(ilLineno, *prpDem);
	CICDEMDATA_LINE *prlDem = &omLines[ilLineno];
	if (prlDem)
		omMapUrnoToLine.SetAt(reinterpret_cast<void *>(prpDem->DemUrno),prlDem);
    return ilLineno;
}

void CicDemandTableViewer::DeleteLine(int ipLineno)
{
    omLines.DeleteAt(ipLineno);
}

int CicDemandTableViewer::InsertLine(CICDEMDATA_LINE *prpDem)
{
	int ilLineno = CreateLine(prpDem);
	if (ilLineno >= 0)
	{
		CCSPtrArray<TABLE_COLUMN> rrlColumns;
		for (int ilC = 0; ilC < omTableFields.GetSize(); ilC++)
		{
			rrlColumns.NewAt(ilC,TABLE_COLUMN());
		}


		Format(&omLines[ilLineno],rrlColumns);
		if (strcmp(omLines[ilLineno].Obty,"AFT") == 0)
		{
			switch(omLines[ilLineno].Dety[0])
			{
			case INBOUND_DEMAND :
				if (TableFieldIndex("AFNUM") >= 0)
					rrlColumns[TableFieldIndex("AFNUM")].TextColor = RGB(0,0,0);
				if (TableFieldIndex("DFNUM") >= 0)
				rrlColumns[TableFieldIndex("DFNUM")].TextColor = ::GetSysColor(COLOR_GRAYTEXT);
			break;
			case OUTBOUND_DEMAND :
				if (TableFieldIndex("AFNUM") >= 0)
					rrlColumns[TableFieldIndex("AFNUM")].TextColor = ::GetSysColor(COLOR_GRAYTEXT);
				if (TableFieldIndex("DFNUM") >= 0)
					rrlColumns[TableFieldIndex("DFNUM")].TextColor = RGB(0,0,0);
			break;
			case TURNAROUND_DEMAND :
				if (TableFieldIndex("AFNUM") >= 0)
					rrlColumns[TableFieldIndex("AFNUM")].TextColor = RGB(0,0,0);
				if (TableFieldIndex("DFNUM") >= 0)
					rrlColumns[TableFieldIndex("DFNUM")].TextColor = ::GetSysColor(COLOR_GRAYTEXT);
			break;
			}
		}

		if (pomTable)
			pomTable->InsertTextLine(ilLineno,rrlColumns, &omLines[ilLineno]);
		rrlColumns.DeleteAll();
	}

	return ilLineno;
}

/////////////////////////////////////////////////////////////////////////////
// CicDemandTableViewer - display drawing routine

void CicDemandTableViewer::UpdateDisplay(BOOL bEraseAll)
{
	if (!pomTable)
		return;

	if (bEraseAll)
	{
		CCSPtrArray<TABLE_HEADER_COLUMN> header;
		CCSPtrArray<TABLE_COLUMN> columns;
		for (int ilC = 0; ilC < omTableFields.GetSize(); ilC++)
		{
			header.NewAt(ilC,TABLE_HEADER_COLUMN());
			header[ilC].Text   = omTableFields[ilC].Name;
			header[ilC].Length = omTableFields[ilC].Length;
			columns.NewAt(ilC,TABLE_COLUMN());
		}


		pomTable->SetHeaderFields(header);

		pomTable->ResetContent();
		int nLineCount = 0;
		nLineCount = omLines.GetSize();


		for (int i = 0; i < nLineCount; i++) 
		{
			Format(&omLines[i],columns);
			// Display grouping effect
#if	0
			if (i == omLines.GetSize()-1 || !IsSameGroup(&omLines[i], &omLines[i+1]))
			{
	//            pomTable->SetTextLineSeparator(i, ST_THICK);
				for (int j = 0; j < columns.GetSize(); j++)
				{
					columns[j].SeparatorType = SEPA_THICK;
					columns[j].HorizontalSeparator = SEPA_THICK;
				}
			}
#endif
			if (strcmp(omLines[i].Obty,"AFT") == 0)
			{
				switch(omLines[i].Dety[0])
				{
				case INBOUND_DEMAND :
					if (TableFieldIndex("AFNUM") >= 0)
						columns[TableFieldIndex("AFNUM")].TextColor = RGB(0,0,0);
					if (TableFieldIndex("DFNUM") >= 0)
						columns[TableFieldIndex("DFNUM")].TextColor = ::GetSysColor(COLOR_GRAYTEXT);
				break;
				case OUTBOUND_DEMAND :
					if (TableFieldIndex("AFNUM") >= 0)
						columns[TableFieldIndex("AFNUM")].TextColor = ::GetSysColor(COLOR_GRAYTEXT);
					if (TableFieldIndex("DFNUM") >= 0)
						columns[TableFieldIndex("DFNUM")].TextColor = RGB(0,0,0);
				break;
				case TURNAROUND_DEMAND :
					if (TableFieldIndex("AFNUM") >= 0)
						columns[TableFieldIndex("AFNUM")].TextColor = RGB(0,0,0);
					if (TableFieldIndex("DFNUM") >= 0)
						columns[TableFieldIndex("DFNUM")].TextColor = ::GetSysColor(COLOR_GRAYTEXT);
				break;
				}
			}

			pomTable->AddTextLine(columns, &omLines[i]);
		}


		pomTable->DisplayTable();
		columns.DeleteAll();
		header.DeleteAll();

		for (i = 0; i < nLineCount; i++) 
		{
			// Display grouping effect
			if (i == omLines.GetSize()-1 || !IsSameGroup(&omLines[i], &omLines[i+1]))
			{
				pomTable->SetTextLineSeparator(i, SEPA_THICK);
			}
		
		}

		pomTable->DisplayTable();
	}
	else
	{
		int nLineCount = omLines.GetSize();

		for (int i = 0; i < nLineCount; i++) 
		{
			// Display grouping effect
			if (i == omLines.GetSize()-1 || !IsSameGroup(&omLines[i], &omLines[i+1]))
			{
				pomTable->SetTextLineSeparator(i, SEPA_THICK);
			}
			else
			{
				pomTable->SetTextLineSeparator(i, SEPA_NORMAL);
			}
			
		}

		pomTable->DisplayTable();
	}
}

void CicDemandTableViewer::Format(CICDEMDATA_LINE *prpLine,CCSPtrArray<TABLE_COLUMN>& rrpColumns)
{
	if (rrpColumns.GetSize() != omTableFields.GetSize())
		return;

	int ilIndex = TableFieldIndex("DETY");
	if (ilIndex >= 0)
	{
		DEMANDDATA rlDemand;
		strcpy(rlDemand.Dety,prpLine->Dety);
		rrpColumns[ilIndex].Text = ogDemandData.GetStringForDemandAndResourceType(rlDemand);
	}

	ilIndex = TableFieldIndex("DFNUM");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = prpLine->DFnum;

	ilIndex = TableFieldIndex("DEBE");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = prpLine->Debe.Format("%H%M");

	ilIndex = TableFieldIndex("DEEN");
	if (ilIndex >= 0)
		rrpColumns[ilIndex].Text = prpLine->Deen.Format("%H%M");

	ilIndex = TableFieldIndex("DEDU");
	if (ilIndex >= 0)
	{
		CString olMinutes;
		olMinutes.Format("%ld",(long)prpLine->Dedu.GetTotalMinutes());
		rrpColumns[ilIndex].Text = olMinutes;
	}

	ilIndex = TableFieldIndex("FCT");
	if (ilIndex >= 0)
	{
		if (prpLine->IsTeamLine && bmCompressGroups)
			rrpColumns[ilIndex].Text = "";
		else
			rrpColumns[ilIndex].Text = prpLine->Fcco;
	}

	ilIndex = TableFieldIndex("PERMITS");
	if (ilIndex >= 0)
	{
		if (prpLine->IsTeamLine && bmCompressGroups)
			rrpColumns[ilIndex].Text = "";
		else
			rrpColumns[ilIndex].Text = prpLine->Permits;
	}

	ilIndex = TableFieldIndex("ULNK");
	if (ilIndex >= 0)
	{
		CString szUlnk;
		szUlnk.Format("%ld",prpLine->Ulnk);
		rrpColumns[ilIndex].Text = szUlnk;
	}

	ilIndex = TableFieldIndex("STAFF");
	if (ilIndex >= 0)
	{
		CString szStaff;
		szStaff.Format("%d",prpLine->Staff);
		rrpColumns[ilIndex].Text = szStaff;
	}

	ilIndex = TableFieldIndex("CLASS");
	if (ilIndex >= 0)
	{
		rrpColumns[ilIndex].Text = prpLine->Class;
	}

	ilIndex = TableFieldIndex("COUNTERS");
	if (ilIndex >= 0)
	{
		rrpColumns[ilIndex].Text = prpLine->CountersRules;
	}

	ilIndex = TableFieldIndex("COUNTERSFIPS");
	if (ilIndex >= 0)
	{
		rrpColumns[ilIndex].Text = prpLine->CountersFips;
	}
}

void CicDemandTableViewer::CicDemandTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
    CicDemandTableViewer *polViewer = (CicDemandTableViewer *)popInstance;

	if (ipDDXType == DEMAND_CHANGE || ipDDXType == DEMAND_NEW)
	{
		polViewer->ProcessDemandChange((DEMANDDATA *)vpDataPointer);
	}
	else if (ipDDXType == DEMAND_DELETE)
	{
		polViewer->ProcessDemandDelete((DEMANDDATA *)vpDataPointer);
	}
	else if (ipDDXType == FLIGHT_CHANGE || ipDDXType == FLIGHT_SELECT)
	{
		polViewer->ProcessFlightChanges(ipDDXType,(FLIGHTDATA *)vpDataPointer);
	}
}


void CicDemandTableViewer::ProcessDemandChange(DEMANDDATA *prpDemand)
{
	if (!prpDemand)
		return;

	if (strcmp(prpDemand->Aloc,ALLOCUNITTYPE_CIC) != 0)
		return;

	// check, if personnel demand
	if (strlen(prpDemand->Rety) <= 0 || strcmp(prpDemand->Rety,PERSONNEL_DEMAND) == 0)
	{
		CICDEMDATA_LINE *prlLine = NULL;

		// change already viewed demand ?
		if (omMapUrnoToLine.Lookup(reinterpret_cast<void *>(prpDemand->Urno),reinterpret_cast<void*&>(prlLine)))
		{
	//		pogButtonList->SetDemandColor(RED);

			int ilLineno = -1;
			if (FindTeamLeaderOf(prpDemand,ilLineno))	// is part of a team
			{
				CICDEMDATA_LINE *prlTeamLine = &omLines[ilLineno];
				if (prlTeamLine->DemUrno == prpDemand->Urno)	// the team leader
				{
					if (prlTeamLine->TeamLines.GetSize() > 0)	// any members ?
					{
						for (int ilL = 0; ilL < prlTeamLine->TeamLines.GetSize(); ilL++)
						{
							DEMANDDATA *prlTeamDem = ogDemandData.GetDemandByUrno(prlTeamLine->TeamLines[ilL].DemUrno);
							if (prlTeamDem)
							{
								CICDEMDATA_LINE olNewTeamLine;
								MakeLine(prlTeamDem,olNewTeamLine);
								olNewTeamLine.IsTeamLine = true;
								for (int i = 0; i < prlTeamLine->TeamLines.GetSize(); i++)
								{
									if (i != ilL)
									{
										olNewTeamLine.TeamLines.New(prlTeamLine->TeamLines[i]);
									}
								}

								if (prlLine)	// now delete the old team leader
								{
									ilLineno = DeleteLine(prlLine);
								}

								// and create a new line from changed demand
								CICDEMDATA_LINE olLine;
								MakeLine(prpDemand,olLine);

								// check if the changed demand line matches the filter criteria
								if (strlen(prpDemand->Alid) <= 0 && IsPassFilter(&olLine))
								{
									olNewTeamLine.TeamLines.New(olLine);
									omMapUrnoToLine.SetAt(reinterpret_cast<void *>(prpDemand->Urno),NULL);

									if (!bmCompressGroups)
									{
										ilLineno = InsertLine(&olLine);
									}
								}

								// update new team line
								RecalculateTeamLine(&olNewTeamLine);
								if (bmCompressGroups)
								{
									ilLineno = InsertLine(&olNewTeamLine);
								}
								else
								{
									ilLineno = ChangeLine(&olNewTeamLine,TRUE);
								}
								
								break;
							}
						}
					}
					else	// no team lines
					{
						if (prlLine)
						{
							ilLineno = DeleteLine(prlLine);
						}

						// and create a new line from changed demand
						CICDEMDATA_LINE olLine;
						MakeLine(prpDemand,olLine);

						// check if the changed demand line matches the filter criteria
						if (strlen(prpDemand->Alid) <= 0 && IsPassFilter(&olLine))
						{
							ilLineno = InsertLine(&olLine);
						}

						UpdateDisplay(FALSE);
					}
				}
				else	// only part of a team
				{
					for (int i = 0; i < prlTeamLine->TeamLines.GetSize(); i++)
					{
						if (prlTeamLine->TeamLines[i].DemUrno == prpDemand->Urno)
						{
							prlTeamLine->TeamLines.DeleteAt(i);
							break;
						}
					}

					// delete the line, if visible
					if (prlLine)
					{
						ilLineno = DeleteLine(prlLine);
					}

					// create a new line with the changed demand
					CICDEMDATA_LINE olLine;
					MakeLine(prpDemand,olLine);
					if (strlen(prpDemand->Alid) <= 0 && IsPassFilter(&olLine))
					{
						prlTeamLine->TeamLines.New(olLine);
						omMapUrnoToLine.SetAt(reinterpret_cast<void *>(prpDemand->Urno),NULL);

						if (!bmCompressGroups)
						{
							ilLineno = InsertLine(&olLine);
						}
					}
					
					// update team line
					RecalculateTeamLine(prlTeamLine);
					
					ilLineno = ChangeLine(prlTeamLine);
					
					UpdateDisplay(FALSE);
				}
			}
			else	// not a team demand
			{
				// simply delete existing demand
				if (prlLine)
				{
					ilLineno = DeleteLine(prlLine);
				}

				// and create a new line from changed demand
				CICDEMDATA_LINE olLine;
				MakeLine(prpDemand,olLine);

				// check if the changed demand line matches the filter criteria
				if (strlen(prpDemand->Alid) <= 0 && IsPassFilter(&olLine))
				{
					ilLineno = InsertLine(&olLine);
				}

				UpdateDisplay(FALSE);
			}
		}
		else	// add a newly created demand	
		{
			CICDEMDATA_LINE olLine;
			MakeLine(prpDemand,olLine);
			if (strlen(prpDemand->Alid) <= 0 && IsPassFilter(&olLine))
			{
				int ilLineno;
				if (FindTeamOf(prpDemand,ilLineno))
				{
					CICDEMDATA_LINE *prlTeamLine = &omLines[ilLineno];

					// add group demand to team leader
					prlTeamLine->TeamLines.New(olLine);

					// add demand to map to prevent from adding it multiple times
					omMapUrnoToLine.SetAt(reinterpret_cast<void *>(prpDemand->Urno),NULL);

					// update team line
					RecalculateTeamLine(prlTeamLine);

					ilLineno = ChangeLine(prlTeamLine);

					if (!bmCompressGroups)
					{
						int ilLineno = InsertLine(&olLine);
					}

	//				pogButtonList->SetDemandColor(RED);
					UpdateDisplay(FALSE);
				}
				else
				{
					int ilLineno = InsertLine(&olLine);
	//				pogButtonList->SetDemandColor(RED);
					UpdateDisplay(FALSE);
				}
			}
		}
	}
}

void CicDemandTableViewer::ProcessDemandDelete(DEMANDDATA *prpDemand)
{
	if(prpDemand == NULL)
		return;

	long llDemUrno = prpDemand->Urno;

	// anything to do ?
	CICDEMDATA_LINE *prlLine;
	if (!omMapUrnoToLine.Lookup(reinterpret_cast<void *>(llDemUrno),reinterpret_cast<void*&>(prlLine)))
		return;

//	pogButtonList->SetDemandColor(RED);

	int ilLineno = -1;
	// check, if demand is part of a team
	if (FindTeamLeaderOf(llDemUrno,ilLineno))
	{
		CICDEMDATA_LINE *prlTeamLine = &omLines[ilLineno];
		if (prlTeamLine->DemUrno == llDemUrno)	// the team leader ?
		{
			if (prlTeamLine->TeamLines.GetSize() > 0)	// any members available to be the new team leader ?
			{
				for (int ilL = 0; ilL < prlLine->TeamLines.GetSize();ilL++)
				{
					// use first member as new team leader
					DEMANDDATA *prlTeamDem = ogDemandData.GetDemandByUrno(prlLine->TeamLines[ilL].DemUrno);
					if (prlTeamDem)	// may be deleted meanwhile ...
					{
						CICDEMDATA_LINE olNewTeamLine;
						MakeLine(prlTeamDem,olNewTeamLine);
						olNewTeamLine.IsTeamLine = true;
						for (int i = 0; i < prlLine->TeamLines.GetSize(); i++)
						{
							if (i != ilL)
							{
								olNewTeamLine.TeamLines.New(prlLine->TeamLines[i]);
							}
						}

						// update team line
						RecalculateTeamLine(&olNewTeamLine);

						// check, if we must insert a new line or change an existing one
						if (bmCompressGroups)
						{
							int ilLineno = InsertLine(&olNewTeamLine);
						}
						else
						{
							int ilLineno = ChangeLine(&olNewTeamLine);
						}

						break;
					}
				}

				DeleteLine(prlTeamLine);
				UpdateDisplay(FALSE);
			}
			else if (prlLine)	// no team lines and visible
			{
				ilLineno = DeleteLine(prlLine);
				if (pomTable)
					pomTable->DisplayTable();
			}
		}
		else	// only part of a team
		{
			for (int i = 0; i < prlTeamLine->TeamLines.GetSize(); i++)
			{
				if (prlTeamLine->TeamLines[i].DemUrno == llDemUrno)
				{
					prlTeamLine->TeamLines.DeleteAt(i);
					break;
				}
			}

			// update team line
			RecalculateTeamLine(prlTeamLine);
			ilLineno = ChangeLine(prlTeamLine);

			if (!bmCompressGroups && prlLine)
			{
				ilLineno = DeleteLine(prlLine);
			}
			else
			{
				omMapUrnoToLine.RemoveKey(reinterpret_cast<void *>(llDemUrno));
			}

			UpdateDisplay(FALSE);
		}
	}
	else	// non team demand
	{
		if (prlLine) // visible ?
		{
			ilLineno = DeleteLine(prlLine);
			if (pomTable)
				pomTable->DisplayTable();
		}
	}
}

void CicDemandTableViewer::ProcessFlightChanges(int ipDDXType,FLIGHTDATA *prpFlight)
{
	if (ipDDXType == FLIGHT_CHANGE)
	{
		bool blUpdateDisplay = false;
		CCSPtrArray<DEMANDDATA> rolDemands;
		ogDemandData.GetDemandsByFlur(rolDemands,prpFlight->Urno);
		for (int i = 0; i < rolDemands.GetSize(); i++)
		{
			DEMANDDATA *prlDemand = &rolDemands[i];
			CICDEMDATA_LINE *prlLine = NULL;
			if (prlDemand && omMapUrnoToLine.Lookup((void *)prlDemand->Urno,(void *&)prlLine) && prlLine != NULL)
			{
				EvaluateFlight(prpFlight,*prlLine);	
				ChangeLine(prlLine);
				blUpdateDisplay = true;
			}
		}

		if (blUpdateDisplay)
			UpdateDisplay(false);
	}
	else if (ipDDXType == FLIGHT_SELECT)
	{
		if (!pomTable)
			return;

		CCSPtrArray<DEMANDDATA> rolDemands;
		ogDemandData.GetDemandsByFlur(rolDemands,prpFlight->Urno);
		for (int i = 0; i < rolDemands.GetSize(); i++)
		{
			DEMANDDATA *prlDemand = &rolDemands[i];
			CICDEMDATA_LINE *prlLine = NULL;
			if (prlDemand && omMapUrnoToLine.Lookup((void *)prlDemand->Urno,(void *&)prlLine) && prlLine != NULL)
			{
				for (int i = 0; i < omLines.GetSize(); i++)
				{
					if (omLines[i].DemUrno == prlLine->DemUrno)
					{
						pomTable->SelectLine(i,TRUE);						
					}
				}
			}
		}
		
	}
}

int CicDemandTableViewer::DeleteLine(CICDEMDATA_LINE *prpLine)
{
	int ilLineno = -1;
	for (int ilC = 0; ilC < omLines.GetSize(); ilC++)
	{
		CICDEMDATA_LINE *prlDem = &omLines[ilC];
		if (prlDem && prlDem->DemUrno == prpLine->DemUrno)
		{
			omMapUrnoToLine.RemoveKey(reinterpret_cast<void *>(prpLine->DemUrno));
			omLines.DeleteAt(ilC);
			if (pomTable)
				pomTable->DeleteTextLine(ilC);
			ilLineno = ilC;
			break;
		}
	}

	return ilLineno;

}

////////////////////////////////////////////////////////////////////////////////////////////
// Printing functions

BOOL CicDemandTableViewer::PrintDemandLine(CICDEMDATA_LINE *prpLine,BOOL bpIsLastLine)
{
	int ilBottomFrame;
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	ilBottomFrame = PRINT_FRAMETHIN;

	if (bpIsLastLine == TRUE)
	{
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.Length     = 1910;
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= PRINT_NOFRAME;
		rlElement.pFont       = &pomPrint->omMediumFont_Bold;
		rlElement.Text.Empty();
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	else
	{
		if (pomPrint->imLineNo == 0)
		{
			for (int i = 0; i < omTableFields.GetSize(); i++)
			{
				if (i == 0)
				{
					rlElement.Alignment  = PRINT_LEFT;
					rlElement.Length     = omTableFields[i].PrintLength;
					rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
					rlElement.FrameRight = PRINT_NOFRAME;
					rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
					rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
					rlElement.pFont      = &pomPrint->omSmallFont_Bold;
					rlElement.Text       = omTableFields[i].Name;
			
					rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
				}
				else
				{
					rlElement.Length     = omTableFields[i].PrintLength;
					rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
					rlElement.Text		 = omTableFields[i].Name;
					if (i == omTableFields.GetSize() -1)
						rlElement.FrameRight = PRINT_FRAMEMEDIUM;
					rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
				}

			}
			pomPrint->PrintLine(rlPrintLine);
			rlPrintLine.DeleteAll();
		}

		CTime olActual = TIMENULL;
		CString olActualMark;
			
		for (int i = 0; i < omTableFields.GetSize(); i++)
		{
			if (i == 0)
			{
				rlElement.Alignment  = PRINT_LEFT;
				rlElement.Length     = omTableFields[i].PrintLength;
				rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
				rlElement.FrameRight = PRINT_NOFRAME;
				rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
				rlElement.FrameBottom= ilBottomFrame;
				rlElement.pFont       = &pomPrint->omSmallFont_Bold;
				
			}
			else
			{
				rlElement.Length     = omTableFields[i].PrintLength;
				rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
				rlElement.FrameTop   = PRINT_NOFRAME;
				if (i == omTableFields.GetSize() -1)
					rlElement.FrameRight = PRINT_FRAMEMEDIUM;
			}

			CString olField = omTableFields[i].Field;
			if (olField == "DETY")
			{
				DEMANDDATA rlDemand;
				strcpy(rlDemand.Dety,prpLine->Dety);
				rlElement.Text = ogDemandData.GetStringForDemandAndResourceType(rlDemand);
			}
			else if (olField == "DFNUM")
				rlElement.Text       = prpLine->DFnum;
			else if (olField == "DEBE")
				rlElement.Text       = prpLine->Debe.Format("%H%M");
			else if (olField == "DEEN")
				rlElement.Text       = prpLine->Debe.Format("%H%M");
			else if (olField == "DEDU")
			{
				CString olMinutes;
				olMinutes.Format("%ld",(long)prpLine->Dedu.GetTotalMinutes());
				rlElement.Text		= olMinutes;
			}
			else if (olField == "FCT")			
			{
				if (prpLine->IsTeamLine && bmCompressGroups)
					rlElement.Text       = "";
				else
					rlElement.Text       = prpLine->Fcco;
			}
			else if (olField == "PERMITS")
			{
				if (prpLine->IsTeamLine && bmCompressGroups)
					rlElement.Text       = "";
				else
					rlElement.Text       = prpLine->Permits;
			}
			else if (olField == "ULNK")
			{
				CString olValue;
				olValue.Format("%ld",prpLine->Ulnk);
				rlElement.Text = olValue;
			}
			else if (olField == "STAFF")
			{
				CString olValue;
				olValue.Format("%d",prpLine->Staff);
				rlElement.Text = olValue;
			}
			else if (olField == "CLASS")
			{
				rlElement.Text = prpLine->Class;
			}
			else if (olField == "COUNTERS")
			{
				rlElement.Text = prpLine->CountersRules;
			}
			else if (olField == "COUNTERSFIPS")
			{
				rlElement.Text = prpLine->CountersFips;
			}

			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}

		pomPrint->PrintLine(rlPrintLine);
		rlPrintLine.DeleteAll();

	}
	return TRUE;
}

BOOL CicDemandTableViewer::PrintDemandHeader(CCSPrint *pomPrint)
{
	pomPrint->omCdc.StartPage();
	pomPrint->PrintHeader();
	return TRUE;
}

void CicDemandTableViewer::PrintView()
{  
	CString omTarget = GetString(IDS_STRING33141) + CString(omStartTime.Format("%d.%m.%Y %H:%M")) + GetString(IDS_STRING33142) +
	CString(omEndTime.Format("%d.%m.%Y %H:%M"))
			+  GetString(IDS_STRING33143) + ogCfgData.rmUserSetup.CCDV;		

	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");
	pomPrint = new CCSPrint(pomTable,PRINT_LANDSCAPE,60,500,100,
		GetString(IDS_STRING61878),olPrintDate,omTarget);
	pomPrint->imMaxLines = 20;

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(PRINT_LANDSCAPE,ogBasicData.pcmPrintForm,DMPAPER_A3)  == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			pomPrint->imLineHeight = 62;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );

			char pclDocName[100];
			strcpy(pclDocName,GetString(IDS_STRING33144));
			rlDocInfo.lpszDocName = pclDocName;	

			pomPrint->omCdc.StartDoc( &rlDocInfo );
			for(int i = 0; i < omLines.GetSize(); i++ ) 
			{
				if( pomPrint->imLineNo >= pomPrint->imMaxLines )
				{
					if ( pomPrint->imPageNo > 0)
					{
						PrintDemandLine(&omLines[i],TRUE);
						pomPrint->PrintFooter("","");
						pomPrint->imLineNo = pomPrint->imMaxLines + 1;
						pomPrint->omCdc.EndPage();
						pomPrint->imLineTop = pomPrint->imFirstLine;
//						break;
					}
					PrintDemandHeader(pomPrint);
				}
				PrintDemandLine(&omLines[i],FALSE);
			}
			PrintDemandLine(NULL,TRUE);
			pomPrint->PrintFooter("","");
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	pomPrint = NULL;
	}

}

CTime CicDemandTableViewer::StartTime() const
{
	return omStartTime;
}

CTime CicDemandTableViewer::EndTime() const
{
	return omEndTime;
}

int CicDemandTableViewer::Lines() const
{
	return omLines.GetSize();
}

CICDEMDATA_LINE*	CicDemandTableViewer::GetLine(int ipLineNo)
{
	if (ipLineNo >= 0 && ipLineNo < omLines.GetSize())
		return &omLines[ipLineNo];
	else
		return NULL;
}

void CicDemandTableViewer::SetStartTime(CTime opStartTime)
{
	omStartTime = opStartTime;
}

void CicDemandTableViewer::SetEndTime(CTime opEndTime)
{
	omEndTime = opEndTime;
}

void CicDemandTableViewer::GetDefaultViewColumns(CStringArray& ropColumnNames)
{
	CString pclTableFields("DETY,DFNUM,CLASS,COUNTERS,COUNTERSFIPS,DEBE,DEEN,DEDU,FCT,PERMITS");

	char *token;
	token = strtok(pclTableFields.GetBuffer(0),",");
	while(token)
	{
		ropColumnNames.Add(token);
		token = strtok(NULL,",");
	}
}

void CicDemandTableViewer::GetAllViewColumns(CStringArray& ropColumnNames)
{
	int ilSize = sizeof(osfields) / sizeof(osfields[0]);
	for (int i = 0; i < ilSize; i++)
	{
		if (!ogBasicData.DisplayDebugInfo() && strcmp(osfields[i].Field,"ULNK") == 0)
			continue;
		ropColumnNames.Add(osfields[i].Field);	
	}
	
}

CString CicDemandTableViewer::GetViewColumnName(const CString& ropColumnField)
{
	int ilSize = sizeof(osfields) / sizeof(osfields[0]);
	for (int i = 0; i < ilSize; i++)
	{
		if (osfields[i].Field == ropColumnField)
			return osfields[i].Name;	
	}
	
	return "";
}

CString CicDemandTableViewer::GetViewColumnField(const CString& ropColumnName)
{
	int ilSize = sizeof(osfields) / sizeof(osfields[0]);
	for (int i = 0; i < ilSize; i++)
	{
		if (osfields[i].Name == ropColumnName)
			return osfields[i].Field;	
	}
	
	return "";
}

bool CicDemandTableViewer::EvaluateTableFields()
{

	CStringArray olViewColumns;
	CViewer::GetFilter("Columns",olViewColumns);
	if (!olViewColumns.GetSize())
		GetDefaultViewColumns(olViewColumns);							

	omTableFields.DeleteAll();

	int ilSize = sizeof(osfields) / sizeof(osfields[0]);
	for (int ilC = 0; ilC < olViewColumns.GetSize();ilC++)
	{
		for (int i = 0; i < ilSize; i++)
		{
			if (osfields[i].Field.CompareNoCase(olViewColumns[ilC]) == 0)
			{
				omTableFields.New(osfields[i]);
			}
		}

	}

	return true;
}

int CicDemandTableViewer::TableFieldIndex(const CString& ropField)
{
	for (int i = 0; i < omTableFields.GetSize(); i++)
	{
		if (omTableFields[i].Field == ropField)
			return i;
	}

	return -1;
}

BOOL CicDemandTableViewer::RecalculateTeamLine(CICDEMDATA_LINE *prlTeamLine)
{
	if (!prlTeamLine || !prlTeamLine->IsTeamLine)
		return FALSE;

	// setup team leader data
	DEMANDDATA *prlTeamDem = ogDemandData.GetDemandByUrno(prlTeamLine->DemUrno);
	MakeLine(prlTeamDem,*prlTeamLine);
	prlTeamLine->IsTeamLine = true;
	prlTeamLine->Debe = prlTeamLine->Debe;
	prlTeamLine->Deen = prlTeamLine->Deen;
	prlTeamLine->Dedu = prlTeamLine->Dedu;
	prlTeamLine->Staff= 1;
	
		
	for (int i = 0; i < prlTeamLine->TeamLines.GetSize(); i++)
	{
		// recalculate team leader line
		if (prlTeamLine->TeamLines[i].Debe < prlTeamLine->Debe)
			prlTeamLine->Debe = prlTeamLine->TeamLines[i].Debe;
		if (prlTeamLine->TeamLines[i].Deen > prlTeamLine->Deen)
			prlTeamLine->Deen = prlTeamLine->TeamLines[i].Deen;
		prlTeamLine->Dedu = prlTeamLine->Deen - prlTeamLine->Debe;
		prlTeamLine->Staff += prlTeamLine->TeamLines[i].Staff;
	}

	return TRUE;
}

BOOL CicDemandTableViewer::EvaluateFlight(FLIGHTDATA *prpFlight,CICDEMDATA_LINE& rrpLine)
{
	if (!prpFlight)
		return FALSE;

	BOOL blUpdate = FALSE;
	DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(rrpLine.DemUrno);
	if (prlDemand)
	{
		if (prlDemand->Dety[0] == INBOUND_DEMAND && prlDemand->Ouri == prpFlight->Urno)
		{
		}
		else if (prlDemand->Ouro == prpFlight->Urno)
		{
			if (strcmp(rrpLine.DFnum,prpFlight->Fnum) != 0)
			{
				strcpy(rrpLine.DFnum,prpFlight->Fnum);
				blUpdate = TRUE;
			}

		}
	}

	return blUpdate;
}


CICDEMDATA_LINE *CicDemandTableViewer::GetLine(DEMANDDATA *prpDem)
{
	if (!prpDem)
		return NULL;

	CICDEMDATA_LINE *prlLine = NULL;
	if (omMapUrnoToLine.Lookup((void *)prpDem->Urno,(void *&)prlLine))
		return prlLine;
	else
		return NULL;
}

CString	CicDemandTableViewer::GetCounterClass(DEMANDDATA *prpDem)
{
	CString olClass;

	if (!prpDem)
		return olClass;

	RUDDATA *prlRud = ogRudData.GetRudByUrno(prpDem->Urud);
	if (prlRud)
	{
		CCCDATA *prlCcc = ogCccData.GetCccByUrno(prlRud->Vref);
		if (prlCcc)
		{
			olClass = prlCcc->Cicn;
		}
	}

	return olClass;
}

CString CicDemandTableViewer::GetCountersRules(DEMANDDATA *prpDem)
{
	CString olCounters;

	if (!prpDem)
		return olCounters;

	RUDDATA *prlRud = ogRudData.GetRudByUrno(prpDem->Urud);
	if (prlRud)
	{
		if (prlRud->Ulnk != 0)
		{
			CCSPtrArray<RUDDATA> olRuds;
			if (ogRudData.GetRudsByUlnk(prlRud->Ulnk,LOCATION_DEMAND,olRuds) > 0)
			{
				int ilSize = olRuds.GetSize();
				for (int ilC = 0; ilC < ilSize; ilC++)
				{
					if (olRuds[ilC].Urue != prpDem->Urue)
						continue;

					RLODATA *polRlo = ogRloData.GetRloByUrud(olRuds[ilC].Urno);
					if (polRlo)
					{
						if (strlen(polRlo->Gtab) && strcmp(polRlo->Gtab,"SGR.GRPN") == 0)
						{
							SGRDATA *polSgr = ogSgrData.GetSgrByUrno(polRlo->Rloc);
							if (polSgr && strcmp(polSgr->Tabn,"CIC") == 0)
							{
								if (!olCounters.IsEmpty())
									olCounters += ',';
								olCounters += polSgr->Grpn;
							}
						}
						else if (strlen(polRlo->Reft) && strcmp(polRlo->Reft,"CIC.CNAM") == 0)
						{
							CICDATA *polCic = ogCicData.GetCicByUrno(polRlo->Rloc);
							if (polCic)
							{
								if (!olCounters.IsEmpty())
									olCounters += ',';
								olCounters += polCic->Cnam;
							}
						}
					}
				}
			}
		}
	}

	return olCounters;
}