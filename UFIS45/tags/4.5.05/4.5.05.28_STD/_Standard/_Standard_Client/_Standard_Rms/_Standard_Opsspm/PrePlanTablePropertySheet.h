// PrePlanTablePropertySheet.h : header file
//

#ifndef _PREPLNPS_H_
#define _PREPLNPS_H_

/////////////////////////////////////////////////////////////////////////////
// PrePlanTablePropertySheet

class PrePlanTablePropertySheet : public BasePropertySheet
{
// Construction
public:
	PrePlanTablePropertySheet(CWnd* pParentWnd = NULL,
		CViewer *popViewer = NULL, UINT iSelectPage = 0);

// Attributes
public:
	FilterPage m_pageRank;
	FilterPage m_pagePermits;
	FilterPage m_pageShiftCode;
	FilterPage m_pageTeam;
	PrePlanTableSortPage m_pageSort;
//	ZeitPage m_pageZeit;

// Operations
public:
	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
	virtual int QueryForDiscardChanges();
};

/////////////////////////////////////////////////////////////////////////////

#endif // _PREPLNPS_H_
