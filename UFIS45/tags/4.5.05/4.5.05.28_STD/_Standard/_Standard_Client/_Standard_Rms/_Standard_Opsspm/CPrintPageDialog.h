#ifndef CPRINTPAGEDIALOG_H__8550A1C3_43CB_4E4C_905D_73B949FB5FA6__INCLUDED
#define CPRINTPAGEDIALOG_H__8550A1C3_43CB_4E4C_905D_73B949FB5FA6__INCLUDED

/////////////////////////////////////////////////////////////////////////////
// CPrintPageDialog dialog
#include "resource.h"
class CPrintPageDialog : public CPrintDialog
{
// Construction
public:
	CPrintPageDialog(BOOL bPrintSetupOnly,
		// TRUE for Print Setup, FALSE for Print Dialog
		DWORD dwFlags = PD_ALLPAGES | PD_USEDEVMODECOPIES | PD_NOPAGENUMS
			| PD_HIDEPRINTTOFILE | PD_NOSELECTION,BOOL bpEnablePageSelection = FALSE,
		CWnd* pParentWnd = NULL);   // standard constructor

	//{{AFX_DATA(CPrintPageDialog)
	enum { IDD = IDD_PRINTPAGE_DLG };
		//}}AFX_DATA


	BOOL IsPageSelected() const;
	BOOL IsPageNoValid() const;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	virtual void OnOK();
	virtual BOOL OnInitDialog();

private:
	CString omStrFromPage;
	CString omStrToPage;
	int imPageBtnChecked;
	BOOL bmPageNoValid;
	int imTotalPageCount;
	BOOL bmEnablePageSelection;
	
	DECLARE_MESSAGE_MAP()
};


#endif