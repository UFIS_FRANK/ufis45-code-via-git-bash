#ifndef _COVERAGEVIEWDLG
#define _COVERAGEVIEWDLG

#include <Coverage.h>


//TFI_XXX_START_TIME  #define TOTALITEMS	19

#define TOTALITEMS	17

// There is no reason to derive a class from CObject because in this specific
// sample application there will be no serialization.  
class CDialogItem
{
public:
	// define the enum with values to match whatever DLGITEMTEMPLATE requires

	DLGITEMTEMPLATE  m_dlgItemTemplate;

	enum			controltype {BUTTON = 0x0080, EDITCONTROL = 0x0081, STATICTEXT = 0x0082};

	controltype		m_controltype;
	CString			m_strCaption;

public:
	CDialogItem(enum controltype cType);  // default constructor will fill in default values
	CDialogItem() {};  // default constructor, not to be called directly

	void Initialize(enum controltype cType, UINT nID, CRect* prect = NULL, LPCTSTR pszCaption = NULL, BOOL blIsPassWord = FALSE);
};

class CTheRessLessDlg : public CDialog
{
public:
	CTheRessLessDlg(CWnd* pParent, CCoverage *popCoverage, float fpScaleFactor, 
						int ipValuesPerXaxis, int ipMarksPerYaxis, int ipStartTime,
						int ipIntervallTime);
	BOOL InitModalIndirect(DLGTEMPLATE* popDlgTemplate);

	BOOL GetAktualScalingFactor(float &fpScaleFactor);
	BOOL GetAktualValuesPerXaxis(int &ipValuesPerXaxis);
	BOOL GetAktualMarksPerYaxis(int &ipMarksPerYaxis);
	BOOL GetAktualStartTime(int &ipStartTime);
	BOOL GetAktualIntervallTime(int &ipIntervallTime);

protected:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTheRessLessDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	//}}AFX_VIRTUAL
	// Generated message map functions
	//{{AFX_MSG(CTheRessLessDlg)
	afx_msg void OnPaint();
	afx_msg void OnColor();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnKillfocusScaleFactor();
	afx_msg void OnKillfocusXValues();
	afx_msg void OnKillfocusMarkY();
	//TFI_XXX_START_TIME  afx_msg void OnKillfocusStartTime();
	afx_msg void OnCheckFiveMin();
	afx_msg void OnCheckFullHour();
	afx_msg void OnCheckHalfHour();
	afx_msg void OnCheckQuartHour();
	afx_msg void OnCheckTenMin();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	//Attributes
public:
	HICON m_hIcon;
	BOOL blIsInitialized;

	// Dialog Data
	//{{AFX_DATA(CTheRessLessDlg)
	float	m_ScaleFactor;
	int		m_ValuesXaxis;
	int		m_MarkY;
	//TFI_XXX_START_TIME  int		m_StartTime;
	//}}AFX_DATA

protected:

private:
	CWnd*			pomParent;
	CCoverage	*pomCoverage;
	float			fmScaleFactor;
	int			imValuesPerXaxis;
	int			imMarksPerYaxis;
	int			imStartTime;
	int			imIntervallTime;

};

class CCoverageViewDlg// : public CDialog 
{
// Construction
public:
    CCoverageViewDlg(CWnd* pParent = NULL);     // standard constructor

	DLGTEMPLATE m_dlgTempl;
	CDialogItem	m_rgDlgItem[TOTALITEMS];  // the 3 controls to be inserted

	void MakeDialog(const char *pspCaption, CWnd* pParent, CCoverage *popCoverage, float &fpScaleFactor, 
						int &ipValuesPerXaxis, int &ipMarksPerYaxis, int &ipStartTime, int &ipIntervallTime);
	void SetValues(CCoverage *popCoverage, float &fpScaleFactor, int &ipValuesPerXaxis, int &ipMarksPerYaxis, 
									  int &ipStartTime, int &ipIntervallTime);
// Implementation
protected:

};
#endif
