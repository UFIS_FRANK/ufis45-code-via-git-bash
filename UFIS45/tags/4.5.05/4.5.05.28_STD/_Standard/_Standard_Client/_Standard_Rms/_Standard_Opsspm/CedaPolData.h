// Class for Pools
#ifndef _CEDAPOLDATA_H_
#define _CEDAPOLDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct PolDataStruct
{
	long	Urno;		// Unique Record Number
	char	Name[13];	// Pool Name
	char	Pool[33];	// Pool Description

	PolDataStruct(void)
	{
		Urno = 0L;
		strcpy(Name,"");
		strcpy(Pool,"");
	}
};

typedef struct PolDataStruct POLDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaPolData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <POLDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapStringToPtr omNameMap;
	CString GetTableName(void);

// Operations
public:
	CedaPolData();
	~CedaPolData();

	CCSReturnCode ReadPolData();
	POLDATA* GetPolByUrno(long lpUrno);
	POLDATA* GetPolByName(const char *pcpName);
	long GetPolUrnoByName(const char *pcpName);
	void GetPoolNames(CStringArray &ropPoolNames);

private:
	void AddPolInternal(POLDATA &rrpPol);
	void ClearAll();
};


extern CedaPolData ogPolData;
#endif _CEDAPOLDATA_H_
