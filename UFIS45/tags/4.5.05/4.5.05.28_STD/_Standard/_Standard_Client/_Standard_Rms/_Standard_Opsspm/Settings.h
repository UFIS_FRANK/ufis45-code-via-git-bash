// class to allow settings to be saved locally
#if !defined(AFX_SETTINGS_H__E0A66A77_31C8_11D7_81FF_00010215BFE5__INCLUDED_)
#define AFX_SETTINGS_H__E0A66A77_31C8_11D7_81FF_00010215BFE5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Settings.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSettings window

class CSettings
{
// Construction
public:
	CSettings();
	~CSettings();
	void AddSetting(CString opKey, CString opValue);
	void AddSetting(CString opKey, long lpValue);
	void AddSetting(CString opKey, int ipValue);
	void AddSetting(CString opKey, bool bpValue);

	CString GetSetting(CString opKey);
	long GetLongSetting(CString opKey);
	int GetIntSetting(CString opKey);
	bool GetBoolSetting(CString opKey);

private:
	CMapStringToString omSettings;
	CString omValue;
};

extern CSettings ogSettings;

#endif // !defined(AFX_SETTINGS_H__E0A66A77_31C8_11D7_81FF_00010215BFE5__INCLUDED_)
