// CedaDelData.h - Class for Shift Delegations
// shift delegations are extra records for a shift
// eg 'F1' to indicate that all emps that have this shift
// are delegated to other shifts - these have their own
// functions (optional) but not permits
#ifndef _CEDADELDATA_H_
#define _CEDADELDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct DelDataStruct
{
	long	Urno;		// Unique Record Number
	char	Fctc[6];	// Function code
	char	Sdac[9];	// Absence code
};

typedef struct DelDataStruct DELDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaDelData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <DELDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CString GetTableName(void);

// Operations
public:
	CedaDelData();
	~CedaDelData();

	bool ReadDelData();
	DELDATA* GetDelByUrno(long lpUrno);
	CString GetFunctionByUrno(long lpUrno);
	void ProcessDelBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

private:
	void AddDelInternal(DELDATA &rrpDel);
	void DeleteDelInternal(long lpUrno);
	void ClearAll();
};


extern CedaDelData ogDelData;
#endif _CEDADELDATA_H_
