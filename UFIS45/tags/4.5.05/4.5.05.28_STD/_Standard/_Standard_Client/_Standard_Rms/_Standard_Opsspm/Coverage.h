
#ifndef COVERAGE_H
#define COVERAGE_H

#define bool BOOL
#define false FALSE
#define true TRUE

#include <CCSPtrArray.h>
#include <CCSTree.h>
#include <CoverageGraphic.h>

/////////////////////////////////////////////////////////////////////////////
// Define's
	#define IDC_FRAME_GRAPHIC						4
	#define IDC_FRAME_TREE							5

//The possible formats for a Coverage-Dialog. This value is stored in 'imCoverageSize'.
	#define MAXIMUM_SIZE								9999
	#define DESIGNED_SIZE							9998

//These parameters define a few standarts to the Coverage-Dialog. These values are
//fixed in this Header-File but can be set during runtime.
	#define FRAME_TEXT_HEIGHT						20		//This Parameter is ALWAYS FIXED!!!Don't touch it!!!
	#define TOP_OFFSET								100	//Distance between the upper side of the Coverage-Tool and the screen
	#define BOTTOM_OFFSET							0		//Dist. betw. the lower side of the Coverage-Tool and the screen
	#define LEFT_OFFSET								0		//Dist. betw. the left side of the Coverage-Tool and the screen
	#define RIGHT_OFFSET								0		//Dist. betw. the right side of the Coverage-Tool and the screen 
	#define FRAME_LEFT_OFFSET						10		//Dist. betw. each frame and the left side of the Coverage-Tool
	#define FRAME_RIGHT_OFFSET						10		//Dist. betw. each frame and the right side of the Coverage-Tool
	#define FRAME_TOP_OFFSET						40		//Dist. betw. each frame and the upper side of the Coverage-Tool
	#define FRAME_BOTTOM_OFFSET					10		//Dist. betw. each frame and the lower side of the Coverage-Tool
	#define CANCEL_BUTTON_RIGHT_OFFSET			40		//Dist. between the 'Cancel'-Button and the right side of the Coverage-Tool
//More graphical element parameters:
	#define CURVE_DESCR_AXIS_OFFSET				20		//Additional Dist. betw. x-axis and curve description part
	#define CURVE_DESCR_X_OFFSET					5
	#define CURVE_DESCR_Y_OFFSET					5
	#define CURVE_DESCR_WIDTH						10
	#define CURVE_DESCR_HEIGHT						10
	#define CURVE_TEXT_WIDTH						50
	#define CURVE_TEXT_HEIGHT						10
	#define GRAPHIC_OFFSET							10		//Dist. betw. graphic and the graphical frame
	#define GRAPHIC_SCROLL_OFFSET					30		//Extra offset due to the horiz. scrollbar
//Some more parameters
	#define MAX_VALUES_PER_X_AXIS					40
	#define MAX_VALUES_PER_Y_AXIS					20
	#define MAX_MARKS_PER_X_AXIS					5
	#define MAX_MARKS_PER_Y_AXIS					5
//Some axis parameters
	#define AXIS_WIDTH								4
	#define SIMPLE_UNIT_LENGTH						5
	#define MAIN_UNIT_LENGTH						8
//Button parameters
	#define BUTTON_WIDTH								80		//The real width of a Button
	#define BUTTON_HEIGHT							22		//The real height of a Button
	#define BUTTON_OFFSET							20		//Offset between two buttons
	#define RADIOBUTTON_WIDTH						110

/////////////////////////////////////////////////////////////////////////////
// Class declaration

//@Man:
//@Memo: ccfgd data
//@See: CCSCedaData
/*@Doc:
	No comment on this class.
*/
class CCoverage : public CWnd
{
// Operations
public:
    //@ManMemo: Default constructor
		CCoverage(CWnd *popMainDialog, int ipCoverageWidth, int ipCoverageHeight, int ipTreeWidth, int ipTreeHeight, 
						int ipGraphWidth, int ipGraphHeight);
		CCoverage(CWnd *popMainDialog, int ipTreeWidth, int ipGraphWidth);
    //@ManMemo: Default Destructor
		~CCoverage();

	//@ManMemo: Initialize and resize the dialog with variable parameters
    /*@Doc:
		Initialization of the coverage tool dialog:
		These parameters define the layout of the coverage-dialog. The method
		resizes the dialog and its elements according to these parameters.
		The first parameter is a pointer to the entire dialog.
		The next two parameters define the width/height of the dialog. 
		The next two parameters define the width/height of the tree-element part. 
		The next two parameters define the width/height of the graphical part. 
		According to these parameters scrollbars are placed on the right 
		and	bottom of this area as they are used due to a windowresize 
		(or a compareable action).
		This function should be called from the InitDialog function of the dialog class.
	*/
		void InitGraphicArea(CWnd *popMainDialog);

	//@ManMemo: Initialize and resize the dialog to a fullscreen size
    /*@Doc:
		Initialization of the coverage tool dialog:
		These parameters define the layout of the coverage-dialog. The method
		resizes the elements of the dialog according to these parameters.
		The first parameter is a pointer to the entire dialog.
		The next parameter defines the width of the tree-element part. 
		The next parameter defines the width of the graphical part. 
		According to these parameters scrollbars are placed on the right
		and	bottom of this area as they are used due to a windowresize 
		(or a compareable action).
		This function should be called from the InitDialog function of the dialog class.
	*/
		void InitGraphicArea(CWnd *popMainDialog, int ipTst);

    //@ManMemo: UpdateDisplay
		BOOL UpdateDisplay();

    //@ManMemo: Configure a special Curves.
		BOOL ConfigureAcurve(int ipCurveIndex, CURVE *popCurve);

    //@ManMemo: Set new counter for values per x-axis.
		BOOL SetValuesPerXaxis(int &ipValuesPerXaxis);

    //@ManMemo: Set new values for the horizontal scrolling
		BOOL SetNewScrolling(int ipScrollPage, int ipScrollLine);

    //@ManMemo: Set new width for scrolling.
		BOOL SetNewWidth();

	//@ManMemo: Pass the treeframe to the calling class
		BOOL GetTree(CCSTree *popTree);

	//@ManMemo: Create a tree element according to the defined borders set through the treeframe
		BOOL CreateTree();

    //@ManMemo: Set the data for a special curve. This is usually called by the Viewer.
		BOOL CurveData(char *popCurveName, CCSPtrArray<int> &ropValues);


protected:
	// Generated message map functions
	//{{AFX_MSG(CCoverage)
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	//@ManMemo: Init. the parameters for the null point of the diagram
		void SetNullPointParas();


// Attributes
public:
	CCoverageGraphic	*pomGraphic;
	CCSTree				*pomTree;

protected:

private:
	int					imCoverageSize;
	int					imCoverageWidth;
	int					imCoverageHeight;
	int					imTreeWidth;
	int					imTreeHeight;
	int					imGraphicalWidth;
	int					imGraphicalHeight;
	int					imCoverageNullPointX;
	int					imCoverageNullPointY;
	CButton				*pomTreeFrame;
};

#endif