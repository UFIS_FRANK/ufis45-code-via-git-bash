// CedaFlightData.cpp - Read and write flight data 
//
// Programmer's Notes:
// In the first draft of the specification, the Rtur in FLIGHDATA was char[12]. I changed
// it to be a long integer since it's a Unique Record Number.
//
//
// Written by:
// Damkerng Thammathakerngkit   May 6, 1996
//
// Modification History:
// May 28, 1996     Damkerng    Remove linking pointer in FLIGHTDATA according to the
//                              redesign of CedaXXXXData classes.

#include <afxwin.h>
#include <ccsglobl.h>
#include <ccsobj.h>
#include <OpssPm.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CedaJobData.h>
#include <CCSBcHandle.h>
#include <CedaFlightData.h>
#include <BasicData.h>
#include <CedaShiftData.h>
#include <CedaDemandData.h>
#include <CedaEmpData.h>
#include <conflict.h>
#include <ufis.h>
#include <DataSet.h>
#include <ccsbasicfunc.h>
#include <PrmConfig.h>

//igu
#include <ButtonList.h>

static int CompareFlightStartTime(const FLIGHTDATA **pppFlight1, const FLIGHTDATA **pppFlight2);

static int CompareFlightStartTime(const FLIGHTDATA **pppFlight1, const FLIGHTDATA **pppFlight2)
{
	CTime olTime1 = !strcmp((**pppFlight1).Adid,"A") ? ((**pppFlight1).Tifa + CTimeSpan(0,0,20,0)) : ((**pppFlight1).Tifd - CTimeSpan(0,1,0,0));
	CTime olTime2 = !strcmp((**pppFlight2).Adid,"A") ? ((**pppFlight2).Tifa + CTimeSpan(0,0,20,0)) : ((**pppFlight2).Tifd - CTimeSpan(0,1,0,0));
	return (int)(olTime1.GetTime() - olTime2.GetTime());
}

CedaFlightData::CedaFlightData()
{
    // Create an array of CEDARECINFO for FLIGHTDATA
    BEGIN_CEDARECINFO(FLIGHTDATA, FlightDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_CHAR_TRIM(Fdat,"FDAT")
		FIELD_CHAR_TRIM(Flno,"FLNO")
		FIELD_CHAR_TRIM(Alc2,"ALC2")
		FIELD_CHAR_TRIM(Alc3,"ALC3")	//hag
		FIELD_CHAR_TRIM(Fltn,"FLTN")
		FIELD_CHAR_TRIM(Flns,"FLNS")
		FIELD_CHAR_TRIM(Adid,"ADID")
		FIELD_CHAR_TRIM(Org3,"ORG3")
		FIELD_CHAR_TRIM(Des3,"DES3")
		FIELD_CHAR_TRIM(Des4,"DES4")
		FIELD_CHAR_TRIM(Via3,"VIA3")
        FIELD_INT(Vian,"VIAN")
		FIELD_CHAR_TRIM(Vial,"VIAL")
		FIELD_CHAR_TRIM(Gta1,"GTA1")
		FIELD_CHAR_TRIM(Gta2,"GTA2")
		FIELD_CHAR_TRIM(Gtd1,"GTD1")
		FIELD_CHAR_TRIM(Gtd2,"GTD2")
        FIELD_CHAR_TRIM(Regn,"REGN")
        FIELD_CHAR_TRIM(Act3,"ACT3")
        FIELD_INT(Nose,"NOSE")
		FIELD_DATE(Stod,"STOD")
		FIELD_DATE(Stoa,"STOA")
//		FIELD_DATE(Etod,"ETOD")
//		FIELD_DATE(Etoa,"ETOA")
		FIELD_DATE(Etod,"ETDI")
		FIELD_DATE(Etoa,"ETAI")
		FIELD_DATE(Tifd,"TIFD")
		FIELD_DATE(Tifa,"TIFA")
		FIELD_DATE(Land,"LAND")
		FIELD_DATE(Slot,"SLOT")
		FIELD_DATE(Airb,"AIRB")
		FIELD_DATE(Onbl,"ONBL")
		FIELD_DATE(Onbe,"ONBE")
		FIELD_DATE(Ofbl,"OFBL")
		FIELD_DATE(Tmoa,"TMOA")
        FIELD_CHAR_TRIM(Tisd,"TISD")
        FIELD_CHAR_TRIM(Tisa,"TISA")
        FIELD_CHAR_TRIM(Styp,"STYP")
		FIELD_CHAR_TRIM(Dcd1,"DCD1")
		FIELD_DATE(Dtd1,"DTD1")
		FIELD_CHAR_TRIM(Dcd2,"DCD2")
		FIELD_DATE(Dtd2,"DTD2")
		FIELD_DATE(Nxti,"NXTI")
		FIELD_DATE(Iskd,"ISKD")
		FIELD_CHAR_TRIM(Ftyp,"FTYP")
		FIELD_CHAR_TRIM(Psta,"PSTA")
		FIELD_CHAR_TRIM(Pstd,"PSTD")
        FIELD_INT(Pax1,"PAXT")
        FIELD_INT(Pax2,"PAX2")
        FIELD_INT(Pax3,"PAX3")
		FIELD_DATE(Lstu,"LSTU")
		FIELD_LONG(Rkey,"RKEY")
		FIELD_CHAR_TRIM(Stev,"STEV")
		FIELD_LONG(Skey,"SKEY")
		FIELD_CHAR_TRIM(Jfno,"JFNO")
        FIELD_INT(Jcnt,"JCNT")
        FIELD_CHAR_TRIM(Act5,"ACT5")
        FIELD_CHAR_TRIM(Ttyp,"TTYP")
		FIELD_INT(Paxt,"PAXT")
		FIELD_INT(Cgot,"CGOT")
		FIELD_INT(Bagn,"BAGN")
		FIELD_INT(Mail,"MAIL")
        FIELD_CHAR_TRIM(Blt1,"BLT1")
        FIELD_CHAR_TRIM(Baz1,"BAZ1")
        FIELD_INT(Ddlf,"DDLF")
        FIELD_INT(Bagw,"BAGW")
		FIELD_CHAR_TRIM(Hapx,"HAPX")
		FIELD_CHAR_TRIM(Hara,"HARA")
		FIELD_CHAR_TRIM(Tga1,"TGA1")
		FIELD_CHAR_TRIM(Tgd1,"TGD1")
        FIELD_CHAR_TRIM(Prmc,"PRMC")
        FIELD_CHAR_TRIM(Prmu,"PRMU")
        FIELD_CHAR_TRIM(Prms,"PRMS")
		FIELD_CHAR_TRIM(Flti,"FLTI")
		FIELD_CHAR_TRIM(Csgn,"CSGN")

    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(FlightDataRecInfo)/sizeof(FlightDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&FlightDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}


    // Initialize table name
    strcpy(pcmTableName,"AFTTAB");
	pcmFieldList = NULL;	// initialize field list later

	ogCCSDdx.Register((void *)this,BC_FLIGHT_CHANGE,CString("FLIGHTDATA"), CString("Flight changed"),ProcessFlightCf);
	ogCCSDdx.Register((void *)this,BC_FLIGHT_INSERT,CString("FLIGHTDATA"), CString("Flight ISF"),ProcessFlightCf);
	ogCCSDdx.Register((void *)this,BC_FLIGHT_DELETE,CString("FLIGHTDATA"), CString("Flight deleted"),ProcessFlightCf);
	ogCCSDdx.Register((void *)this,BC_FLIGHT_RAC,CString("FLIGHTDATA"), CString("RAC Broadcast"),ProcessFlightCf);

	omData.SetSize(0,240);
	omUrnoMap.InitHashTable( 2001 );
	omGateMap.InitHashTable( 111 );
	omAlioMap.InitHashTable( 111 );
	omRegnMap.InitHashTable( 111 );
}


CedaFlightData::~CedaFlightData(void)
{
	TRACE("CedaFlightData::~CedaFlightData called\n");
	ogCCSDdx.UnRegister(this,NOTUSED);
	omRecInfo.DeleteAll();
	ClearAll();
}

void CedaFlightData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
    omJointFlightMap.RemoveAll();
    omData.DeleteAll();

	POSITION rlPos;
	CMapPtrToPtr *pomGateFlightMap;
	CString olAnything;

	for(rlPos = omGateMap.GetStartPosition(); rlPos != NULL; )
	{
		omGateMap.GetNextAssoc(rlPos,olAnything,(void *&)pomGateFlightMap);
		pomGateFlightMap->RemoveAll();
		delete pomGateFlightMap;
	}
	omGateMap.RemoveAll();

	FLIGHTURNO *prlFlightUrno;
	long llRkey;
	for(rlPos = omRkeyMap.GetStartPosition(); rlPos != NULL; )
	{
		omRkeyMap.GetNextAssoc(rlPos,(void *&) llRkey,(void *&) prlFlightUrno);
		delete prlFlightUrno;
	}
	omRkeyMap.RemoveAll();

	CMapPtrToPtr *pomRegnFlightMap;
	for(rlPos = omRegnMap.GetStartPosition(); rlPos != NULL; )
	{
		omRegnMap.GetNextAssoc(rlPos,olAnything,(void *&)pomRegnFlightMap);
		pomRegnFlightMap->RemoveAll();
		delete pomRegnFlightMap;
	}
	omRegnMap.RemoveAll();

	CMapPtrToPtr *pomPositionFlightMap;
	for(rlPos = omPositionMap.GetStartPosition(); rlPos != NULL; )
	{
		omPositionMap.GetNextAssoc(rlPos,olAnything,(void *&)pomPositionFlightMap);
		pomPositionFlightMap->RemoveAll();
		delete pomPositionFlightMap;
	}
	omPositionMap.RemoveAll();

//	for ( rlPos =  omAlioMap.GetStartPosition(); rlPos != NULL; )
//	{
//		omAlioMap.GetNextAssoc(rlPos,olAnything,(void *&)pomGateFlightMap);
//		pomGateFlightMap->RemoveAll();
//		delete pomGateFlightMap;
//	}
//	omAlioMap.RemoveAll();

	omUseAlc3.RemoveAll();
}

bool CedaFlightData::SavePrm(FLIGHTDATA *prpFlight)
{
	bool ilRc = true;
	char *pclTmpFieldList;
	CString olListOfData;
	char pclSelection[124];
	char pclData[5240];

	sprintf(pclSelection, "WHERE URNO = %ld", prpFlight->Urno);
	//	MakeCedaData(&omRecInfo,olListOfData,prpFlight);
	pclTmpFieldList = pcmFieldList;
	if (bgPrmUseStat&&ogBasicData.DoesFieldExist("AFT", "PRMS"))
	{
		sprintf (pclData,"%s,%s,%s",prpFlight->Prmc,prpFlight->Prmu,prpFlight->Prms);
		pcmFieldList = "PRMC,PRMU,PRMS";
	}
	else
	{
		strcpy(pclData,prpFlight->Prmc);
		pcmFieldList = "PRMC";
	}
	ilRc = CedaAction("UFR",pclSelection,"",pclData); 
	pcmFieldList = pclTmpFieldList;
	//prpFlight->IsChanged = DATA_UNCHANGED;
	CreateDemands(prpFlight->Urno);
	return ilRc;
}

bool CedaFlightData::ReadAllFlights(CTime opStartTime, CTime opEndTime, char *pcpSelection /* = NULL */, int ipSendDdx /* = false */)
{
    // Select data from the database
	char clPrmSelection[5000] = "";
	bool ilRc = true;
	bool blReadingInsertedFlights = (pcpSelection != NULL) ? true : false;
	if(!blReadingInsertedFlights)
	{
		ClearAll();
		if (pogInitialLoad != NULL)
		{
			pogInitialLoad->SetProgress(5);
			pogInitialLoad->UpdateWindow();
		}
	}

	// Initialize table names and field names
	if (ogBasicData.HandlingAgentsSupported())
	{
			if (ogBasicData.DeadloadPerFlightSupported())
			{
				strcpy(clFieldList,"URNO,FDAT,FLNO,ALC2,ALC3,FLTN,FLNS,ADID,ORG3,DES3,DES4,VIA3,VIAN,VIAL,GTA1,GTA2,GTD1,GTD2,"
								"REGN,ACT3,NOSE,STOD,STOA,ETDI,ETAI,TIFD,TIFA,LAND,SLOT,AIRB,ONBL,ONBE,OFBL,TMOA,TISD,"
								"TISA,STYP,DCD1,DTD1,DCD2,DTD2,NXTI,ISKD,FTYP,PSTA,PSTD,PAXT,PAX2,PAX3,LSTU,RKEY,"
								"STEV,SKEY,JFNO,JCNT,ACT5,TTYP,PAXT,CGOT,BAGN,MAIL,BLT1,BAZ1,DDLF,BAGW,FLTI,HAPX,HARA,TGA1,TGD1,CSGN");
			}
			else
			{
				strcpy(clFieldList,"URNO,FDAT,FLNO,ALC2,ALC3,FLTN,FLNS,ADID,ORG3,DES3,DES4,VIA3,VIAN,VIAL,GTA1,GTA2,GTD1,GTD2,"
								"REGN,ACT3,NOSE,STOD,STOA,ETDI,ETAI,TIFD,TIFA,LAND,SLOT,AIRB,ONBL,ONBE,OFBL,TMOA,TISD,"
								"TISA,STYP,DCD1,DTD1,DCD2,DTD2,NXTI,ISKD,FTYP,PSTA,PSTD,PAXT,PAX2,PAX3,LSTU,RKEY,"
								"STEV,SKEY,JFNO,JCNT,ACT5,TTYP,PAXT,CGOT,BAGN,MAIL,BLT1,BAZ1,BAGW,FLTI,HAPX,HARA,CSGN");
			}
	}
	else
	{
			if (ogBasicData.DeadloadPerFlightSupported())
			{
				strcpy(clFieldList,"URNO,FDAT,FLNO,ALC2,ALC3,FLTN,FLNS,ADID,ORG3,DES3,DES4,VIA3,VIAN,VIAL,GTA1,GTA2,GTD1,GTD2,"
								"REGN,ACT3,NOSE,STOD,STOA,ETDI,ETAI,TIFD,TIFA,LAND,SLOT,AIRB,ONBL,ONBE,OFBL,TMOA,TISD,"
								"TISA,STYP,DCD1,DTD1,DCD2,DTD2,NXTI,ISKD,FTYP,PSTA,PSTD,PAXT,PAX2,PAX3,LSTU,RKEY,"
								"STEV,SKEY,JFNO,JCNT,ACT5,TTYP,PAXT,CGOT,BAGN,MAIL,BLT1,BAZ1,DDLF,BAGW,FLTI,CSGN");
			}
			else
			{
				strcpy(clFieldList,"URNO,FDAT,FLNO,ALC2,ALC3,FLTN,FLNS,ADID,ORG3,DES3,DES4,VIA3,VIAN,VIAL,GTA1,GTA2,GTD1,GTD2,"
								"REGN,ACT3,NOSE,STOD,STOA,ETDI,ETAI,TIFD,TIFA,LAND,SLOT,AIRB,ONBL,ONBE,OFBL,TMOA,TISD,"
								"TISA,STYP,DCD1,DTD1,DCD2,DTD2,NXTI,ISKD,FTYP,PSTA,PSTD,PAXT,PAX2,PAX3,LSTU,RKEY,"
								"STEV,SKEY,JFNO,JCNT,ACT5,TTYP,PAXT,CGOT,BAGN,MAIL,BLT1,BAZ1,BAGW,FLTI,CSGN");
			}
	}

	if (bgIsPrm)
	{
		strcat(clFieldList,",Prmc");
		if (bgPrmUseStat&&ogBasicData.DoesFieldExist("AFT", "PRMS"))
		{
			strcat(clFieldList,",Prmu,Prms");
		}
		if (ogBasicData.IsPrmHandlingAgentEnabled())
		{
			sprintf(clPrmSelection," AND ALC3 in (%s) ",ogPrmConfig.getAirlineList());
		}
	}
	pcmFieldList = clFieldList;

	char pclCom[10] = "GFR";
	int ilWhereLen = (pcpSelection != NULL) ? strlen(pcpSelection)+1 : 1000;
    char *pclWhere = new char[ilWhereLen];

	CString olStartTime, olEndTime;

	if(blReadingInsertedFlights)
	{
		strcpy(pclWhere, pcpSelection);
	}
	else
	{
		olStartTime = opStartTime.Format("%Y%m%d%H%M%S");
		olEndTime = opEndTime.Format("%Y%m%d%H%M%S");
		// ORDER BY JCNT so that flights containing lists of joint flights (in JFNO) will be loaded first
		// - the joint flights listed in JFNO will be added to a map and subsequently not loaded
		//sprintf(pclWhere,"WHERE ((TIFA BETWEEN '%s' AND '%s') OR (TIFD BETWEEN '%s' AND '%s')) [ROTATIONS]  ORDER BY JCNT DESC",olStartTime,olEndTime,olStartTime,olEndTime); 
		//sprintf(pclWhere,"WHERE ((ADID = 'A' AND TIFA BETWEEN '%s' AND '%s') OR (ADID = 'D' AND TIFD BETWEEN '%s' AND '%s')) [ROTATIONS]  ORDER BY JCNT DESC",olStartTime,olEndTime,olStartTime,olEndTime); 
		sprintf(pclWhere,"WHERE ((((ADID = 'A' OR ADID = 'B') AND TIFA BETWEEN '%s' AND '%s') OR ((ADID = 'D' OR ADID = 'B') AND TIFD BETWEEN '%s' AND '%s'))) ",olStartTime,olEndTime,olStartTime,olEndTime);
		if (*clPrmSelection != '\0')
		{
			strcat(pclWhere,clPrmSelection);
		}
		strcat (pclWhere,"[ROTATIONS]");
	}


	int ilCount = 0; 
	if ((ilRc = CedaAction(pclCom, pclWhere)) != true)  
	{
		ogBasicData.LogCedaError("CedaFlightData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			bool blFlightAdded = false;

			FLIGHTDATA *prlFlight = new FLIGHTDATA;
			//if ((ilRc = GetBufferRecord(ilLc,prpFlight)) == true && *prpFlight->Ftyp != 'P')
			// don't load Planned or Noop flights
			//if ((ilRc = GetBufferRecord(ilLc,prlFlight)) == true) 
			if ((ilRc = GetBufferRecord(&omRecInfo,ilLc,prlFlight,CString(pcmFieldList))) == true)
			{
				if(UpdateExistingFlight(prlFlight))
				{
					long llFlur = prlFlight->Urno;

					delete prlFlight; // existing flight was updated so can delete new flight struct

					if((prlFlight = GetFlightByUrno(llFlur)) != NULL)
						blFlightAdded = true;
				}
				else
				{
					blFlightAdded = AddFlight(prlFlight);
				}
			}

			if(blFlightAdded)
			{
				ilCount++;
				if(ipSendDdx)
				{
					ogConflicts.CheckConflicts(*prlFlight,FALSE,FALSE,TRUE);
					ogCCSDdx.DataChanged((void *)this,FLIGHT_CHANGE,(void *)prlFlight);
				}
			}
			else
			{
				delete prlFlight;
				prlFlight = NULL;
			}
		}

		ilRc = true;
	}

	if(ilCount > 0)
	{
		ogBasicData.Trace("CedaFlightData Read %d Records (Rec Size %d) %s",ilCount, sizeof(FLIGHTDATA), pclWhere);
	}

	static bool blFirstTime = true;
	if(blFirstTime)
	{
		char pclConfigPath[128];
		char pclUseAlc3[128];

		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));

		GetPrivateProfileString("OPSSPM", "USEALC3", "  ,YY",pclUseAlc3, sizeof pclUseAlc3, pclConfigPath);
		char *token = strtok(pclUseAlc3,",\n");
		while(token)
		{
			omUseAlc3.Add(token);
			token = strtok(NULL,",\n");
		}
		blFirstTime = false;
	}

	delete pclWhere;
    return ilRc;
}

int CedaFlightData::GetKeycodes(CStringArray &ropKeycodes)
{
	for(POSITION rlPos = omPossibleKeycodes.GetStartPosition(); rlPos != NULL; )
	{
		void *pvlDummy;
		CString olKeycode;
		omPossibleKeycodes.GetNextAssoc(rlPos, olKeycode, (void *&)pvlDummy);
		ropKeycodes.Add(olKeycode);
	}

	return ropKeycodes.GetSize();
}

// called when re-reading flight data
bool CedaFlightData::UpdateExistingFlight(FLIGHTDATA *prpFlight)
{
	bool blUpdated = false;
	if(prpFlight != NULL)
	{
		FLIGHTDATA *prlFlight = GetFlightByUrno(prpFlight->Urno);
		if(prlFlight != NULL)
		{
			CString olData;
			MakeCedaData(&omRecInfo, olData, prpFlight);
			char *pclData = new char[olData.GetLength() + 10];
			strcpy(pclData, olData);
			blUpdated = UpdateFlight(prlFlight, pcmFieldList, pclData);
			delete [] pclData;
		}
	}

	return blUpdated;
}

// called for broadcasted flight data
bool CedaFlightData::UpdateExistingFlight(long lpFlur, char *pcpFieldList, char *pcpData)
{
	bool blUpdated = false;
	FLIGHTDATA *prlFlight = GetFlightByUrno(lpFlur);
	if(prlFlight != NULL)
	{
		blUpdated = UpdateFlight(prlFlight, pcpFieldList, pcpData);
	}

	return blUpdated;
}

bool CedaFlightData::UpdateFlight(FLIGHTDATA *prpFlight, char *pcpFieldList, char *pcpData)
{
	bool blUpdated = false;
	if(prpFlight != NULL)
	{
		FLIGHTDATA rlOldFlight = *prpFlight;
		ConvertDatesToUtc(prpFlight);
		GetRecordFromItemList(prpFlight, pcpFieldList, pcpData);
		PrepareFlightData(prpFlight);
		//Singapore
		if(ogBasicData.IsPaxServiceT2() == true)
		{
			ogConflicts.CheckConflictsForFlightTerminal(&rlOldFlight,prpFlight);
		}
		UpdateMaps(&rlOldFlight, prpFlight);
		blUpdated = true;
	}

	return blUpdated;
}

bool CedaFlightData::AddFlight(FLIGHTDATA *prpFlight)
{
	bool blAdded = false;
	if(prpFlight != NULL)
	{
//		FLIGHTDATA *prlFlight = GetFlightByUrno(prpFlight->Urno);
//		if(prlFlight != NULL)
//		{
//			// flight already exists - can happen after a re-read, update the flight
//			CString olData;
//			MakeCedaData(&omRecInfo, olData, prpFlight);
//			char *pclData = new char[olData.GetLength() + 10];
//			strcpy(pclData, olData);
//
//			FLIGHTDATA rlOldFlight = *prlFlight;
//			ConvertDatesToUtc(prlFlight);
//			GetRecordFromItemList(prlFlight, pcmFieldList, olData);
//			PrepareFlightData(prlFlight);
//			UpdateMaps(&rlOldFlight, prlFlight);
//
//			delete [] pclData;
//			blAdded = true;
//		}
//		else
		{
			void *pvlDummy;
			if(!omPossibleKeycodes.Lookup(prpFlight->Stev, (void *&) pvlDummy))
			{
				omPossibleKeycodes.SetAt(prpFlight->Stev, (void *) NULL);
			}

			bool blAddFlight = true;
			CString olFnum = MakeFnum(prpFlight);
			if(prpFlight->Urno <= 0)
			{
				//ogBasicData.Trace("Flight <%s> not loaded because URNO <= 0",olFnum);
				blAddFlight = false;
			}
			else if(strlen(prpFlight->Fltn) <= 0)
			{
				//ogBasicData.Trace("Flight <%s> not loaded because FLTN = <%s>",olFnum,prpFlight->Fltn);
				//blAddFlight = false;
				strcpy(prpFlight->Flno ,prpFlight->Csgn);
			}
			else if(IsJointFlight(prpFlight))
			{
				//ogBasicData.Trace("Flight <%s> not loaded because it is a joint flight",olFnum);
				blAddFlight = false;
			}
			else if(!IsPassFtypFilter(prpFlight->Ftyp))
			{
				// default don't load FTYPs: "PNJ"
				//ogBasicData.Trace("Flight <%s> not loaded because FTYP=<%s>",olFnum,prpFlight->Ftyp);

				if(!ogDemandData.FlightHasDemands(prpFlight->Urno) && !ogJobData.FlightHasJobs(prpFlight->Urno))
				{
					// invalid flights will not be filtered out if the flight has jobs or demands loaded
					blAddFlight = false;
				}
			}

			if(blAddFlight)
			{
				PrepareFlightData(prpFlight);
				omData.Add(prpFlight);
				omUrnoMap.SetAt((void *)prpFlight->Urno,prpFlight);

				if(prpFlight->Jcnt > 0)
				{
					// this is a joint flight ie a single flight with several flight numbers
					// this is the master flight with a list of slave flights in Jfno
					// we don't want to load the slave flights so create a map containing 
					// the slave flight numbers which will be ignored
					AddToJointFlightMap(prpFlight);
				}

				AddFlightToRkeyMap(prpFlight);

				if(IsArrivalOrBoth(prpFlight))
				{
					// add arrival gate(s)
					AddFlightToGateMap(prpFlight->Gta1,prpFlight);
					AddFlightToGateMap(prpFlight->Gta2,prpFlight);

					// add arrival position
					AddFlightToPositionMap(prpFlight->Psta,prpFlight);
				}

				if(IsDepartureOrBoth(prpFlight))
				{
					// add departure gate(s)
					AddFlightToGateMap(prpFlight->Gtd1,prpFlight);
					AddFlightToGateMap(prpFlight->Gtd2,prpFlight);

					// add arrival position
					AddFlightToPositionMap(prpFlight->Pstd,prpFlight);
				}


				AddFlightToRegnMap(prpFlight->Regn,prpFlight);

				blAdded = true;
			}
		}
	}
	return blAdded;
}

void CedaFlightData::DeleteFlight(FLIGHTDATA *prpFlight, bool bpSendDdx /*=true*/)
{
	if(prpFlight != NULL)
	{
		long llFlightUrno = prpFlight->Urno;

		DeleteFlightFromGateMap(prpFlight->Gta1, llFlightUrno);
		DeleteFlightFromGateMap(prpFlight->Gta2, llFlightUrno);
		DeleteFlightFromGateMap(prpFlight->Gtd1, llFlightUrno);
		DeleteFlightFromGateMap(prpFlight->Gtd2, llFlightUrno);

		DeleteFlightFromPositionMap(prpFlight->Psta, llFlightUrno);
		DeleteFlightFromPositionMap(prpFlight->Pstd, llFlightUrno);

		DeleteFlightFromRkeyMap(prpFlight);
		DeleteFlightFromRegnMap(prpFlight->Regn,llFlightUrno);

		omUrnoMap.RemoveKey((void *)llFlightUrno);

		bool blSomethingDeleted = true;
		while(blSomethingDeleted)
		{
			blSomethingDeleted = false;
			for(POSITION rlPos = omJointFlightMap.GetStartPosition(); rlPos != NULL; )
			{
				FLIGHTDATA *prlFlight;
				CString olJointFlight;
				omJointFlightMap.GetNextAssoc(rlPos, olJointFlight, (void *&) prlFlight);
				if(prlFlight->Urno == llFlightUrno)
				{
					omJointFlightMap.RemoveKey(olJointFlight);
					blSomethingDeleted = true;
					break;
				}
			}
		}

		int ilC = omData.GetSize();
		for(int i = (ilC-1); i >= 0; i--)
		{
			if(omData[i].Urno == llFlightUrno)
			{
				omData.DeleteAt(i);
			}
		}

		if(bpSendDdx)
		{
			ogCCSDdx.DataChanged((void *)this, FLIGHT_DELETE,(void *)llFlightUrno);
		}
	}
}


bool CedaFlightData::IsPassFtypFilter(const char *pcpFtyp)
{
	return (ogBasicData.omFlightFtypsNotToLoad.Find(*pcpFtyp) == -1) ? true : false;
}

bool CedaFlightData::FlightInTimeframe(CTime opTifa, CTime opTifd)
{
	bool blInTimeframe = false;
	if(opTifa != TIMENULL && opTifd != TIMENULL && opTifa <= ogBasicData.GetTimeframeEnd() && opTifd >= ogBasicData.GetTimeframeStart())
	{
		blInTimeframe = true;
	}

	return blInTimeframe;
}


// this is a joint flight ie a single flight with several flight numbers
// this is the master flight with a list of slave flights in Jfno
// we don't want to load the slave flights so create a map containing 
// the slave flight numbers which will be ignored
void CedaFlightData::AddToJointFlightMap(FLIGHTDATA *prpFlight)
{
	if(prpFlight != NULL && prpFlight->Jcnt > 0)
	{
		//TRACE("Joint flight found <%s>\n",prpFlight->Jfno);
		CString olJointFlight;
		int ilLen = strlen(prpFlight->Jfno);
		for(int ilChar = 0; ilChar < ilLen; ilChar++)
		{
			if((ilChar == (ilLen-1) || prpFlight->Jfno[ilChar] == ' ') && olJointFlight.GetLength() > 3)
			{
				if(ilChar == (ilLen-1))
				{
					olJointFlight += prpFlight->Jfno[ilChar];
				}
				olJointFlight.TrimRight();
				olJointFlight.TrimLeft();
				//TRACE("Add <%s> to omJointFlightMap\n",olJointFlight);
				omJointFlightMap.SetAt(olJointFlight,prpFlight);
				olJointFlight.Empty();
			}
			else
			{
				olJointFlight += prpFlight->Jfno[ilChar];
			}
		}
	}
}

bool CedaFlightData::IsJointFlight(FLIGHTDATA *prpFlight)
{
	bool blIsJointFlight = false;

	CString olFnum = MakeFnum(prpFlight);
	FLIGHTDATA *prlMasterFlight = NULL;
	if(omJointFlightMap.Lookup(olFnum, (void *&) prlMasterFlight))
	{
		blIsJointFlight = true;
	}

	return blIsJointFlight;
}

void CedaFlightData::AddFlightToRkeyMap(FLIGHTDATA *prpFlight)
{
	// Rkey is the URNO of the arrival flight
	if (prpFlight != NULL && prpFlight->Rkey > 0)
	{
		FLIGHTURNOPTR prlFlightUrnos;
		if(prpFlight->Rkey != prpFlight->Urno)
		{
			// dep flight so have all info (urna/rkey and urnd)
			prlFlightUrnos = new FLIGHTURNO;

			prlFlightUrnos->Rkey = prpFlight->Rkey;
			prlFlightUrnos->Urna = prpFlight->Rkey;
			prlFlightUrnos->Urnd = prpFlight->Urno;

			omRkeyMap.SetAt((void *)prlFlightUrnos->Rkey,prlFlightUrnos);
		}
		else
		{
			// arr flight - this may have been re-read after receiving a RAC command, in which
			// case it was first deleted (urna set to 0) in AddFlight() before being re-inserted into local mem
			if(omRkeyMap.Lookup((void *)prpFlight->Rkey,(void *& )prlFlightUrnos))
			{
				prlFlightUrnos->Urna = prpFlight->Urno;
			}
		}
	}
} 

void CedaFlightData::DeleteFlightFromRkeyMap(FLIGHTDATA *prpFlight)
{
	if(prpFlight != NULL && prpFlight->Rkey > 0)
	{
		FLIGHTURNOPTR prlFlightUrnos;

		if(omRkeyMap.Lookup((void *)prpFlight->Rkey,(void *& )prlFlightUrnos))
		{
			if(prlFlightUrnos->Urnd == prpFlight->Urno)
			{
				prlFlightUrnos->Urnd = 0L;
			}
			if(prlFlightUrnos->Urna == prpFlight->Urno)
			{
				prlFlightUrnos->Urna = 0L;
			}
			if(prlFlightUrnos->Urna == 0L && prlFlightUrnos->Urnd == 0L)
			{
				delete prlFlightUrnos;
				omRkeyMap.RemoveKey((void *)prpFlight->Rkey);
			}
		}
	}
}

void  CedaFlightData::UpdateMaps(FLIGHTDATA *prpOldFlight, FLIGHTDATA *prpNewFlight)
{
	if(prpOldFlight != NULL && prpNewFlight != NULL)
	{
		UpdateFlightForGate(prpOldFlight->Gta1,prpOldFlight,prpNewFlight->Gta1,prpNewFlight);
		UpdateFlightForGate(prpOldFlight->Gta2,prpOldFlight,prpNewFlight->Gta2,prpNewFlight);
		UpdateFlightForGate(prpOldFlight->Gtd1,prpOldFlight,prpNewFlight->Gtd1,prpNewFlight);
		UpdateFlightForGate(prpOldFlight->Gtd2,prpOldFlight,prpNewFlight->Gtd2,prpNewFlight);

		UpdateFlightForPosition(prpOldFlight->Psta,prpOldFlight,prpNewFlight->Psta,prpNewFlight);
		UpdateFlightForPosition(prpOldFlight->Pstd,prpOldFlight,prpNewFlight->Pstd,prpNewFlight);

		UpdateFlightForRegn(prpOldFlight->Regn,prpOldFlight,prpNewFlight->Regn,prpNewFlight);
		UpdateRkeyMap(prpOldFlight,prpNewFlight);
	}
}

void CedaFlightData::UpdateRkeyMap(FLIGHTDATA *prpOldFlight, FLIGHTDATA *prpNewFlight)
{
	if(prpOldFlight != NULL && prpNewFlight != NULL && prpOldFlight->Rkey != prpNewFlight->Rkey)
	{
		DeleteFlightFromRkeyMap(prpOldFlight);
		AddFlightToRkeyMap(prpNewFlight);
	}
}

// flight can have one or more gates - this function adds the flight
// to a map for a single gate
bool CedaFlightData::AddFlightToGateMap(const char *pcpGate, FLIGHTDATA *prpFlight)
{	
	bool blAdded = false;

	if(strlen(pcpGate) > 0)
	{
		CMapPtrToPtr *pomGateFlightMap = NULL;

		if(omGateMap.Lookup(pcpGate, (void *&) pomGateFlightMap))
		{
			pomGateFlightMap->SetAt((void *)prpFlight->Urno,prpFlight);
			blAdded = true;
		}
		else
		{
			pomGateFlightMap = new CMapPtrToPtr;
			pomGateFlightMap->SetAt((void *)prpFlight->Urno,prpFlight);
			omGateMap.SetAt(pcpGate,pomGateFlightMap);
			blAdded = true;
		}
	}

	return blAdded;
}

bool CedaFlightData::DeleteFlightFromGateMap(const char *pcpGate, long lpFlightUrno)
{
	bool blDeleted = false;

	if(strlen(pcpGate) > 0)
	{
		CMapPtrToPtr *pomGateFlightMap = NULL;

		if(omGateMap.Lookup(pcpGate, (void *&) pomGateFlightMap))
		{
			if(pomGateFlightMap != NULL)
			{
				pomGateFlightMap->RemoveKey((void *)lpFlightUrno);
				blDeleted = true;
			}
		}
	}

	return blDeleted;
}

bool CedaFlightData::UpdateFlightForGate(const char *pcpOldGate, FLIGHTDATA *prpOldFlight,
										 const char *pcpNewGate, FLIGHTDATA *prpNewFlight)
{
	bool blUpdated = false;

	if(strcmp(pcpOldGate,pcpNewGate))
	{
		if(DeleteFlightFromGateMap(pcpOldGate,prpOldFlight->Urno))
		{
			blUpdated = true;
		}
		if(AddFlightToGateMap(pcpNewGate,prpNewFlight))
		{
			blUpdated = true;
		}
	}

	return blUpdated;
}


// flight can have one or more gates - this function adds the flight
// to a map for a single gate
bool CedaFlightData::AddFlightToPositionMap(const char *pcpPosition, FLIGHTDATA *prpFlight)
{	
	bool blAdded = false;

	if(strlen(pcpPosition) > 0)
	{
		CMapPtrToPtr *pomPositionFlightMap = NULL;

		if(omPositionMap.Lookup(pcpPosition, (void *&) pomPositionFlightMap))
		{
			pomPositionFlightMap->SetAt((void *)prpFlight->Urno,prpFlight);
			blAdded = true;
		}
		else
		{
			pomPositionFlightMap = new CMapPtrToPtr;
			pomPositionFlightMap->SetAt((void *)prpFlight->Urno,prpFlight);
			omPositionMap.SetAt(pcpPosition,pomPositionFlightMap);
			blAdded = true;
		}
	}

	return blAdded;
}

bool CedaFlightData::DeleteFlightFromPositionMap(const char *pcpPosition, long lpFlightUrno)
{
	bool blDeleted = false;

	if(strlen(pcpPosition) > 0)
	{
		CMapPtrToPtr *pomPositionFlightMap = NULL;

		if(omPositionMap.Lookup(pcpPosition, (void *&) pomPositionFlightMap))
		{
			if(pomPositionFlightMap != NULL)
			{
				pomPositionFlightMap->RemoveKey((void *)lpFlightUrno);
				blDeleted = true;
			}
		}
	}

	return blDeleted;
}

bool CedaFlightData::UpdateFlightForPosition(const char *pcpOldPosition, FLIGHTDATA *prpOldFlight,
										 const char *pcpNewPosition, FLIGHTDATA *prpNewFlight)
{
	bool blUpdated = false;

	if(strcmp(pcpOldPosition,pcpNewPosition))
	{
		if(DeleteFlightFromPositionMap(pcpOldPosition,prpOldFlight->Urno))
		{
			blUpdated = true;
		}
		if(AddFlightToPositionMap(pcpNewPosition,prpNewFlight))
		{
			blUpdated = true;
		}
	}

	return blUpdated;
}

bool CedaFlightData::AddFlightToRegnMap(const char *pcpRegn, FLIGHTDATA *prpFlight)
{	
	bool blAdded = false;

	if(strlen(pcpRegn) > 0)
	{
		CMapPtrToPtr *pomRegnFlightMap = NULL;

		if(omRegnMap.Lookup(pcpRegn, (void *&) pomRegnFlightMap))
		{
			pomRegnFlightMap->SetAt((void *)prpFlight->Urno,prpFlight);
			blAdded = true;
		}
		else
		{
			pomRegnFlightMap = new CMapPtrToPtr;
			pomRegnFlightMap->SetAt((void *)prpFlight->Urno,prpFlight);
			omRegnMap.SetAt(pcpRegn,pomRegnFlightMap);
			blAdded = true;
		}
	}

	return blAdded;
}



bool CedaFlightData::DeleteFlightFromRegnMap(const char *pcpRegn, long lpFlightUrno)
{
	bool blDeleted = false;

	if(strlen(pcpRegn) > 0)
	{
		CMapPtrToPtr *pomRegnFlightMap = NULL;

		if(omRegnMap.Lookup(pcpRegn, (void *&) pomRegnFlightMap))
		{
			if(pomRegnFlightMap != NULL)
			{
				pomRegnFlightMap->RemoveKey((void *)lpFlightUrno);
				blDeleted = true;
			}
		}
	}

	return blDeleted;
}

bool CedaFlightData::UpdateFlightForRegn(const char *pcpOldRegn, FLIGHTDATA *prpOldFlight,
										 const char *pcpNewRegn, FLIGHTDATA *prpNewFlight)
{
	bool blUpdated = false;

	if(strcmp(pcpOldRegn,pcpNewRegn))
	{
		if(DeleteFlightFromRegnMap(pcpOldRegn,prpOldFlight->Urno))
		{
			blUpdated = true;
		}
		if(AddFlightToRegnMap(pcpNewRegn,prpNewFlight))
		{
			blUpdated = true;
		}
	}

	return blUpdated;
}

FLIGHTDATA  *CedaFlightData::GetFlightByFnum(const char *pcpFnum,const char *pcpFdat)
{
	CString olFnum = "";	// suppress blank and convert to upppercase
	for (const char *p = pcpFnum; *p != '\0'; p++)
		if (*p != ' ')
			olFnum += toupper(*p);

	for(POSITION rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		FLIGHTDATA *prlFlight;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlFlight);
		CString olThisFlightFnum = "";	// suppress blank and convert to upppercase
		for (const char *p = prlFlight->Fnum; *p != '\0'; p++)
			if (*p != ' ')
				olThisFlightFnum += toupper(*p);

		// Check if this is the specified flight or not
		if (((olFnum == olThisFlightFnum) && (strcmp(pcpFdat,prlFlight->Fdat) == 0)))
			return prlFlight;
	}
	return NULL;
}

bool CedaFlightData::GetFlightsByAlcFltnFlns(CCSPtrArray<FLIGHTDATA> &ropFlights,
							const char *pcpAlc,const char *pcpFltn, 
							const char *pcpFlns,const char *pcpFdat,char cpAdid, bool bpReset /*=true*/)
{
	if(bpReset)
	{
	    ropFlights.RemoveAll();
	}
	bool blIsAlc3 = strlen(pcpAlc) == 3;
	FLIGHTDATA *prlFlight;


	for(POSITION rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		bool blAlcFound = false;
		bool blNumberFound = false;
		bool blSuffixFound = false;

		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlFlight);

		if (blIsAlc3)
		{
			if (!stricmp(pcpAlc,prlFlight->Alc3) ) 
			{
				blAlcFound =  true;
			}
		}
		else
		{
			if (!stricmp(pcpAlc,prlFlight->Alc2) ) 
			{
				blAlcFound =  true;
			}
		}

		if (atoi(pcpFltn) == atoi(prlFlight->Fltn))
		{
			blNumberFound = true;
		}
		if (*pcpFlns == *prlFlight->Flns)
		{
			blSuffixFound = true;
		}

		
		if (blAlcFound && blNumberFound && blSuffixFound && *prlFlight->Adid == cpAdid)
		{
			char clFlightDate[26];
			if (*prlFlight->Adid == 'A' || *prlFlight->Adid == 'B')
			{
				strncpy(clFlightDate,prlFlight->Tifa.Format("%Y%m%d"),8);
			}
			else
			{
				strncpy(clFlightDate,prlFlight->Tifd.Format("%Y%m%d"),8);
			}
			clFlightDate[8] = '\0';
			if (!strcmp(pcpFdat,clFlightDate))
			{
				ropFlights.Add(prlFlight);;
			}
		}
	}
	return true;
}

FLIGHTDATA  *CedaFlightData::GetFlightByAlcFltnFlns(const char *pcpAlc,const char *pcpFltn, 
							const char *pcpFlns,const char *pcpFdat,char cpAdid)
{
	bool blIsAlc3 = strlen(pcpAlc) == 3;


	for(POSITION rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		bool blAlcFound = false;
		bool blNumberFound = false;
		bool blSuffixFound = false;

		long llUrno;
		FLIGHTDATA *prlFlight;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlFlight);

		if (blIsAlc3)
		{
			if (!stricmp(pcpAlc,prlFlight->Alc3) ) 
			{
				blAlcFound =  true;
			}
		}
		else
		{
			if (!stricmp(pcpAlc,prlFlight->Alc2) ) 
			{
				blAlcFound =  true;
			}
		}

		if (atoi(pcpFltn) == atoi(prlFlight->Fltn))
		{
			blNumberFound = true;
		}
		if (*pcpFlns == *prlFlight->Flns)
		{
			blSuffixFound = true;
		}

		
		if (blAlcFound && blNumberFound && blSuffixFound && *prlFlight->Adid == cpAdid)
		{
			char clFlightDate[26];
			if (*prlFlight->Adid == 'A' || *prlFlight->Adid == 'B')
			{
				strncpy(clFlightDate,prlFlight->Tifa.Format("%Y%m%d"),8);
			}
			else
			{
				strncpy(clFlightDate,prlFlight->Tifd.Format("%Y%m%d"),8);
			}
			clFlightDate[8] = '\0';
			if (!strcmp(pcpFdat,clFlightDate))
			{
				return prlFlight;
			}
		}
	}
	return NULL;
}


FLIGHTDATA  *CedaFlightData::GetRotationFlight(long lpFlightUrno, int ipReturnFlightType /*ID_NOT_RETURNFLIGHT*/)
{
	return GetRotationFlight(GetFlightByUrno(lpFlightUrno), ipReturnFlightType);
}

FLIGHTDATA  *CedaFlightData::GetRotationFlight(FLIGHTDATA *prpFlight, int ipReturnFlightType /*ID_NOT_RETURNFLIGHT*/)
{
	FLIGHTDATA *prlRotation = NULL;
	if(prpFlight != NULL)
	{
		switch(ipReturnFlightType)
		{
		case ID_ARR_RETURNFLIGHT:
			prlRotation = ogFlightData.GetRotationFlight(prpFlight->Urno, prpFlight->Urno);
			if(!ogFlightData.IsDepartureOrBoth(prlRotation))
				prlRotation = NULL;
			break;
		case ID_DEP_RETURNFLIGHT:
			prlRotation = ogFlightData.GetRotationFlight(prpFlight->Urno, prpFlight->Rkey);
			if(!ogFlightData.IsArrivalOrBoth(prlRotation))
				prlRotation = NULL;
			break;
		default:
			prlRotation = GetRotationFlight(prpFlight->Urno, prpFlight->Rkey);
			break;
		}
	}

	return prlRotation;
}

// given the URNO and RKEY of a flight, return the other flight in the rotation
FLIGHTDATA  *CedaFlightData::GetRotationFlight(long lpUrno, long lpRkey)
{
	FLIGHTDATA *prlRotationFlight = NULL;
	FLIGHTURNOPTR prlFlightUrnos;

	if (omRkeyMap.Lookup((void *)lpRkey,(void *& )prlFlightUrnos) == TRUE)
	{
		if(prlFlightUrnos->Urna == lpUrno)
		{
			prlRotationFlight = GetFlightByUrno(prlFlightUrnos->Urnd);
		}
		else if(prlFlightUrnos->Urnd == lpUrno)
		{
			prlRotationFlight = GetFlightByUrno(prlFlightUrnos->Urna);
		}
	}
	return prlRotationFlight;
}


FLIGHTDATA  *CedaFlightData::GetFlightByUrno(long lpUrno)
{
	FLIGHTDATA  *prlFlight;

	if (lpUrno > 0L && omUrnoMap.Lookup((void *)lpUrno,(void *& )prlFlight))
	{
		//TRACE("GetFlightByUrno %ld \n",prlFlight->Urno);
		if (prlFlight->Urno == 0)
		{
			int ilNumFlights = omData.GetSize();
			for(int ilFlight = 0; ilFlight < ilNumFlights; ilFlight++)
			{
				prlFlight = &omData[ilFlight];
				if  (prlFlight->Urno == lpUrno)
				{
					return prlFlight;
				}
			}
		}
		return prlFlight;
	}
	return NULL;
}

bool CedaFlightData::GetFlightsByGate(CCSPtrArray <FLIGHTDATA> &ropFlights, const char *pcpGate, bool bpReset /*=true*/)
{
	FLIGHTDATA  *prlFlight;

	if(bpReset)
	{
	    ropFlights.RemoveAll();
	}
	CMapPtrToPtr *pomGateFlightMap;

	if(omGateMap.Lookup(pcpGate,(void *& )pomGateFlightMap) == TRUE)
	{
		for(POSITION rlPos =  pomGateFlightMap->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			pomGateFlightMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlFlight);
			ropFlights.Add(prlFlight);
		}
	}

    return true;
}

bool CedaFlightData::GetFlightsByCallsign(CCSPtrArray <FLIGHTDATA> &ropFlights, const char *pcpCallsign ,const char *pcpFdat ,char cpAdid, bool bpReset /*=true*/)
{
	if(bpReset)
	{
	    ropFlights.RemoveAll();
	}
	FLIGHTDATA *prlFlight;


	for(POSITION rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		bool blCsgnFound = false;

		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlFlight);

		if (!stricmp(pcpCallsign,prlFlight->Csgn) ) 
			{
				blCsgnFound =  true;
			}

		
		if (blCsgnFound && *prlFlight->Adid == cpAdid)
		{
			//ropFlights.Add(prlFlight);

			char clFlightDate[26];
			if (*prlFlight->Adid == 'A' || *prlFlight->Adid == 'B')
			{
				strncpy(clFlightDate,prlFlight->Tifa.Format("%Y%m%d"),8);
			}
			else
			{
				strncpy(clFlightDate,prlFlight->Tifd.Format("%Y%m%d"),8);
			}
			clFlightDate[8] = '\0';
			if (!strcmp(pcpFdat,clFlightDate))
			{
				ropFlights.Add(prlFlight);;
			}

		}
	}
	return true;
}

bool CedaFlightData::GetFlightsByPosition(CCSPtrArray <FLIGHTDATA> &ropFlights, const char *pcpPosition, bool bpReset /*=true*/)
{
	FLIGHTDATA  *prlFlight;

	if(bpReset)
	{
	    ropFlights.RemoveAll();
	}
	CMapPtrToPtr *pomPositionFlightMap;

	if(omPositionMap.Lookup(pcpPosition,(void *& )pomPositionFlightMap) == TRUE)
	{
		for(POSITION rlPos =  pomPositionFlightMap->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			pomPositionFlightMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlFlight);
			ropFlights.Add(prlFlight);
		}
	}

    return true;
}

bool CedaFlightData::GetFlightsByAlio(CCSPtrArray<FLIGHTDATA> &ropFlights, const char *pcpAlio)
{
	FLIGHTDATA  *prlFlight;

    ropFlights.RemoveAll();
	CMapPtrToPtr *pomGateFlightMap;

	if (omAlioMap.Lookup(pcpAlio,(void *& )pomGateFlightMap) == TRUE)
	{
		for(POSITION rlPos =  pomGateFlightMap->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			pomGateFlightMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlFlight);
			ropFlights.Add(prlFlight);
		}
	}

    return true;
}

bool CedaFlightData::GetFlightsByRegn(CCSPtrArray<FLIGHTDATA> &ropFlights,const char *pcpRegn)
{
	FLIGHTDATA  *prlFlight;

    ropFlights.RemoveAll();
	CMapPtrToPtr *pomRegnFlightMap;

	if(omRegnMap.Lookup(pcpRegn,(void *& )pomRegnFlightMap) == TRUE)
	{
		for(POSITION rlPos =  pomRegnFlightMap->GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			pomRegnFlightMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlFlight);
			ropFlights.Add(prlFlight);
		}
	}

    return true;
}

void CedaFlightData::GetFlightsByRegnAndTime(CCSPtrArray <FLIGHTDATA> &ropFlights,const char *pcpRegn,CTime opStartTime,CTime opEndTime)
{
	CCSPtrArray <FLIGHTDATA> olFlights;
	if(GetFlightsByRegn(olFlights,pcpRegn))
	{
		int ilNumFlights = olFlights.GetSize();
		for(int ilFlight = 0; ilFlight < ilNumFlights; ilFlight++)
		{
			FLIGHTDATA *prlFlight = &olFlights[ilFlight];
			if(IsArrival(prlFlight))
			{
				if(prlFlight->Tifa >= opStartTime && prlFlight->Tifa <= opEndTime)
				{
					ropFlights.Add(prlFlight);
				}
			}
			else
			{
				if(prlFlight->Tifd >= opStartTime && prlFlight->Tifd <= opEndTime)
				{
					ropFlights.Add(prlFlight);
				}
			}
		}
	}
}


bool CedaFlightData::GetFlightsWithoutGates(CCSPtrArray<FLIGHTDATA> &ropFlights)
{
	FLIGHTDATA  *prlFlight;
    ropFlights.RemoveAll();

	int ilNumFlights = omData.GetSize();
	for(int ilFlight = 0; ilFlight < ilNumFlights; ilFlight++)
	{
		prlFlight = &omData[ilFlight];
		if(ogFlightData.GetGate(prlFlight).IsEmpty())
			ropFlights.Add(prlFlight);
	}

	ropFlights.Sort(CompareFlightStartTime);

    return true;
}

bool CedaFlightData::GetFlightsWithoutPositions(CCSPtrArray<FLIGHTDATA> &ropFlights)
{
	FLIGHTDATA  *prlFlight;
    ropFlights.RemoveAll();

	int ilNumFlights = omData.GetSize();
	for(int ilFlight = 0; ilFlight < ilNumFlights; ilFlight++)
	{
		prlFlight = &omData[ilFlight];
		if(ogFlightData.GetPosition(prlFlight).IsEmpty())
			ropFlights.Add(prlFlight);
	}

	ropFlights.Sort(CompareFlightStartTime);

    return true;
}


// Prepare formatted flight number
void CedaFlightData::PrepareFlightData(FLIGHTDATA *prpFlight)
{
	strcpy(prpFlight->Fnum,MakeFnum(prpFlight));
	ConvertDatesToLocal(prpFlight);
}


// Format flight number in example format "LH 017C"
CString CedaFlightData::MakeFnum(FLIGHTDATA *prpFlight)
{
	if(strlen(prpFlight->Fltn) <= 0)
	{
		strcpy(prpFlight->Flno ,prpFlight->Csgn);
	}

	return CString(prpFlight->Flno);

//	CString olFnum;
//    olFnum.Format("%s %03ld%s",MakeAlc(prpFlight), atoi(prpFlight->Fltn), prpFlight->Flns);
//	return olFnum;
}

// Format airline code
CString CedaFlightData::MakeAlc(FLIGHTDATA *prpFlight)
{
	if (!prpFlight)
		return "";

	CString olAlc;
	if (prpFlight->Alc2[0])
	{
		for (int i = 0; i < omUseAlc3.GetSize(); i++)
		{
			if (omUseAlc3[i].CompareNoCase(prpFlight->Alc2) == 0)
			{
				olAlc = prpFlight->Alc3;				
				break;
			}
		}

		if (!olAlc.GetLength())
		{
			olAlc = prpFlight->Alc2;				
		}
	}
	else
	{
		olAlc = prpFlight->Alc3;				
	}

	return olAlc;
}

// Use 3 - Letter airline code instead of 2 - Letter code
BOOL CedaFlightData::UseAlc3(FLIGHTDATA *prpFlight)
{
	if (!prpFlight)
		return FALSE;

	BOOL blAlc = FALSE;
	if (prpFlight->Alc2[0])
	{
		for (int i = 0; i < omUseAlc3.GetSize(); i++)
		{
			if (omUseAlc3[i].CompareNoCase(prpFlight->Alc2) == 0)
			{
				blAlc = TRUE;				
				break;
			}
		}
	}
	else
	{
		blAlc = TRUE;				
	}

	return blAlc;
}

//igu	
BOOL CedaFlightData::IsViewLocked()
{
	BOOL bRet = FALSE;
	
	if (pogButtonList->m_wndPrmTable != NULL)
		bRet = (pogButtonList->m_wndPrmTable->IsViewLocked());
	
	return bRet;
}


//typedef void (*DDXCALLBACK)(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
// the callback function for storing broadcasted fligthdata in FLIGHTDATA array

void  ProcessFlightCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_FLIGHT_CHANGE :
		((CedaFlightData *)popInstance)->ProcessFlightBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	case BC_FLIGHT_INSERT :
		((CedaFlightData *)popInstance)->ProcessInsertFlightBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	case BC_FLIGHT_DELETE :
		((CedaFlightData *)popInstance)->ProcessDeleteFlightBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	case BC_FLIGHT_RAC :
		((CedaFlightData *)popInstance)->ProcessRacBroadcast(vpDataPointer);
		break;
	}
}

// sent after flights have been split or joined
// Command:		RAC
// Table:		AFTTAB
// Selection:	REGN,Begin,End
// Fields:		RKEY
// Data:		RKEY1,...,RKEYn
void CedaFlightData::ProcessRacBroadcast(void *vpDataPointer)
{
	BcStruct *prlBcStruct = (BcStruct *) vpDataPointer;
	if(prlBcStruct == NULL || strlen(prlBcStruct->Data) <= 0)
		return;

	CStringArray olSelection;
	ExtractItemList(CString(prlBcStruct->Selection), &olSelection);
	if(olSelection.GetSize() < 3)
		return;

	CTime olReloadFrom = DBStringToDateTime(olSelection[1]);
	CTime olReloadTo = DBStringToDateTime(olSelection[2]);
	if (olReloadFrom == TIMENULL || olReloadTo == TIMENULL)
		return;

	CDWordArray olDeletedFlightUrnos; 
	if (IsOverlapped(olReloadFrom, olReloadTo, ogBasicData.GetTimeframeStartUtc(), ogBasicData.GetTimeframeEndUtc()))
	{
		CStringArray olRkeys;

		ExtractItemList(CString(prlBcStruct->Data), &olRkeys);

		char pclSelection[2500];
		CString olUrnos, olComma = ",";

		FLIGHTURNOPTR prlFlightUrnos = NULL;
	
		int ilNumRkey = olRkeys.GetSize();
		for(int ilRkey = 0; ilRkey < ilNumRkey; ilRkey++)
		{
			if (!olUrnos.IsEmpty()) 
			{
				olUrnos += olComma;
			}

			olUrnos += olRkeys[ilRkey];

			if(olUrnos.GetLength() > 2000 || ilRkey == (ilNumRkey-1))
			{
				ogBasicData.MarkTime();
				sprintf(pclSelection, "WHERE RKEY IN (%s)", olUrnos);
				ReadAllFlights(TIMENULL, TIMENULL, pclSelection, true);
				olUrnos.Empty();
				ogBasicData.SaveTimeTakenForReread("RAC (join/split from roghdl)", pclSelection);
			}
		}

		// the following is insurance against a flight being deleted and then not being
		// re-loaded which would result in "Flight Deleted" being displayed on job bars
		// this is of course IMPOSSIBLE
		for(int d = 0; d < olDeletedFlightUrnos.GetSize(); d++)
		{
			long llFlur = olDeletedFlightUrnos[d];
			if(GetFlightByUrno(llFlur) == NULL && (ogJobData.FlightHasJobs(llFlur) || ogDemandData.FlightHasDemands(llFlur)))
			{
				HandleMissingFlight(llFlur);
			}
		}
	}
}

void  CedaFlightData::ProcessDeleteFlightBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlFlightData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlFlightData);

	CStringArray olFieldList;
	ogBasicData.ExtractItemList(prlFlightData->Selection, &olFieldList,',');

	long llUrno = 0L;
	
	if(olFieldList.GetSize() > 0) llUrno = atol(olFieldList[0]);

	FLIGHTDATA *prlFlight = GetFlightByUrno(llUrno);
	if(prlFlight == NULL)
	{
		olFieldList.RemoveAll();
		ogBasicData.ExtractItemList(prlFlightData->Fields, &olFieldList,',');
		if(olFieldList.GetSize() > 0) llUrno = atol(olFieldList[0]);
		prlFlight = GetFlightByUrno(llUrno);
	}

	long llFlur = NULL;
	if(prlFlight != NULL)
	{
		llFlur = prlFlight->Urno;
		DeleteFlight(prlFlight);
	}

	// insurance against a flight being deleted errorneously - should never happen
	if(GetFlightByUrno(llFlur) == NULL && (ogJobData.FlightHasJobs(llFlur) || ogDemandData.FlightHasDemands(llFlur)))
	{
		HandleMissingFlight(llFlur);
	}
}


void  CedaFlightData::ProcessInsertFlightBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	// received:
	//Cmd      : ISF
	//Object   : AFTTAB
	//Selection: WHERE 20010426,20010426,1234567,1
	//Fields   : SKEY,FTYP,TIFA,TIFD
	//Data     : 42603781,N,20010426000000,20010426235959

	struct BcStruct *prlFlightData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlFlightData);

	FLIGHTDATA rlFlight;
	GetRecordFromItemList(&rlFlight,prlFlightData->Fields,prlFlightData->Data);
	ConvertDatesToLocal(&rlFlight);

	if(FlightInTimeframe(rlFlight.Tifa, rlFlight.Tifd) &&	IsPassFtypFilter(rlFlight.Ftyp))
	{
		ogBasicData.MarkTime();
		char pclSelection[100];
		sprintf(pclSelection,"WHERE SKEY = '%ld'",rlFlight.Skey);
		ReadAllFlights(TIMENULL, TIMENULL, pclSelection);
		ogBasicData.SaveTimeTakenForReread("ISF (insert flight)", pclSelection);
	}
}

void  CedaFlightData::ProcessFlightBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	if (ipDDXType == BC_FLIGHT_CHANGE)
	{
		FLIGHTDATA *prlFlight;
		struct BcStruct *prlFlightData = (struct BcStruct *) vpDataPointer;
		ogBasicData.LogBroadcast(prlFlightData);
		long llUrno;

		if (strstr(prlFlightData->Selection,"WHERE") != NULL)
		{
			llUrno = GetUrnoFromSelection(prlFlightData->Selection);
		}
 		else
		{
			llUrno = atol(prlFlightData->Selection);
		}

		if(UpdateExistingFlight(llUrno, prlFlightData->Fields, prlFlightData->Data))
		{
			prlFlight = GetFlightByUrno(llUrno);
		}
		else
		{
			// flight not found, check if ETD/ETA change has resulted in the flight entering OpssPm's timeframe, if so then load it
			bool blFlightAdded = false;
			prlFlight = new FLIGHTDATA;
			GetRecordFromItemList(prlFlight, prlFlightData->Fields, prlFlightData->Data);
			CTime olFlightTime = GetFlightTime(prlFlight);
			if(olFlightTime != TIMENULL && olFlightTime >= ogBasicData.GetTimeframeStartUtc() && olFlightTime <= ogBasicData.GetTimeframeEndUtc())
			{
				if(FlightHasFullFieldList(prlFlight))
				{
					blFlightAdded = AddFlight(prlFlight);
				}
				else
				{
					ogBasicData.MarkTime();
					char pclSelection[250];
					sprintf(pclSelection, "WHERE URNO = %ld", llUrno);
					ReadAllFlights(TIMENULL, TIMENULL, pclSelection);
					prlFlight = GetFlightByUrno(llUrno);
					ogBasicData.SaveTimeTakenForReread("UPD on AFTTAB (flight update)", pclSelection);
				}
			}

			if(!blFlightAdded)
			{
				delete prlFlight;
				prlFlight = NULL;
			}
		}

		if(prlFlight != NULL)
		{
			ogConflicts.CheckConflicts(*prlFlight,FALSE,FALSE,TRUE);
			if (!IsViewLocked()) //igu
				ogCCSDdx.DataChanged((void *)this,FLIGHT_CHANGE,(void *)prlFlight);
		}
	}
}

bool CedaFlightData::FlightHasFullFieldList(FLIGHTDATA *prpFlight)
{
	if(prpFlight != NULL && prpFlight->Urno != 0L && strlen(prpFlight->Ftyp) && strlen(prpFlight->Adid))
	{
		return true;
	}
	return false;
}

// this function was used to update the old gate field with the current
// gate, thereby causing the shadow bar to be deleted - now set conflict
// with the old field to deleted
void  CedaFlightData::SaveGate(FLIGHTDATA  *prpFlight)
{
	char pclSelection[82];
	
	char pclData[182];
	CTime olNow = ogBasicData.GetTime();
	sprintf (pclData,"%s,%s,%s,%s,%s",prpFlight->Gta1,prpFlight->Gta2,prpFlight->Gtd1,prpFlight->Gtd2,olNow.Format("%Y%m%d%H%M%S"));


	if(IsArrivalOrBoth(prpFlight)) // Arrival or Both
	{
		DeleteFlightFromGateMap(prpFlight->Gta1, prpFlight->Urno);
		DeleteFlightFromGateMap(prpFlight->Gta2, prpFlight->Urno);
	}
	if(IsDepartureOrBoth(prpFlight)) // Departure or both
	{
		DeleteFlightFromGateMap(prpFlight->Gtd1, prpFlight->Urno);
		DeleteFlightFromGateMap(prpFlight->Gtd2, prpFlight->Urno);
	}


	sprintf(pclSelection,"WHERE URNO = '%ld' ",prpFlight->Urno);
	if (CedaAction("URT","FLTFRA","GTA1,GTA2,GTD1,GTD2,LSTU",pclSelection,"",pclData,"RETURN") != true)
		TRACE("Schattenbalken f�r Flug %ld %s nicht gespeichert\n",prpFlight->Urno,prpFlight->Fnum);
}

// this function was used to update the old position field with the current position
void  CedaFlightData::SavePosition(FLIGHTDATA  *prpFlight)
{
	char pclSelection[82];
	
	char pclData[182];
	CTime olNow = ogBasicData.GetTime();
	sprintf (pclData,"%s,%s,%s",prpFlight->Psta,prpFlight->Pstd,olNow.Format("%Y%m%d%H%M%S"));


	if(IsArrivalOrBoth(prpFlight)) // Arrival or Both
	{
		DeleteFlightFromPositionMap(prpFlight->Psta, prpFlight->Urno);
	}
	if(IsDepartureOrBoth(prpFlight)) // Departure or both
	{
		DeleteFlightFromPositionMap(prpFlight->Pstd, prpFlight->Urno);
	}


	sprintf(pclSelection,"WHERE URNO = '%ld' ",prpFlight->Urno);
	if (CedaAction("URT","FLTFRA","PSTA,PSTD,LSTU",pclSelection,"",pclData,"RETURN") != true)
		TRACE("Schattenbalken f�r Flug %ld %s nicht gespeichert\n",prpFlight->Urno,prpFlight->Fnum);
}

// given prpFlight1 and the return flight type of prpFlight1 (may not be a return flight), return the retrun flight type of prpFlight2
int CedaFlightData::GetRotationReturnFlightType(FLIGHTDATA *prpFlight1, FLIGHTDATA *prpFlight2, int ipReturnFlightType1 /*ID_NOT_RETURNFLIGHT*/)
{
	int ilReturnFlightType2 = ID_NOT_RETURNFLIGHT;
	if(IsReturnFlight(prpFlight2))
	{
		ilReturnFlightType2 = (ipReturnFlightType1 == ID_ARR_RETURNFLIGHT || IsArrival(prpFlight1)) ? ID_DEP_RETURNFLIGHT : ID_ARR_RETURNFLIGHT;
	}

	return ilReturnFlightType2;
}

// returns true if the flight is a return flight (destination and origin are the home airport)
bool CedaFlightData::IsReturnFlight(FLIGHTDATA *prpFlight)
{
	if(prpFlight != NULL && *prpFlight->Adid == 'B')
		return true;
	return false;
}

// returns true if the flight is an arrival
bool CedaFlightData::IsArrival(FLIGHTDATA *prpFlight)
{
	if(prpFlight != NULL && *prpFlight->Adid == 'A')
		return true;
	return false;
}

// returns true if the flight is an arrival or both (Rundflug)
bool CedaFlightData::IsArrivalOrBoth(FLIGHTDATA *prpFlight)
{
	if(prpFlight != NULL && *prpFlight->Adid != 'D')
		return true;
	return false;
}

// returns true if the flight is a departure (or Rundfluge)
bool CedaFlightData::IsDeparture(FLIGHTDATA *prpFlight)
{
	if(prpFlight != NULL && *prpFlight->Adid == 'D')
		return true;
	return false;
}

// returns true if the flight is a departure or both (Rundflug)
bool CedaFlightData::IsDepartureOrBoth(FLIGHTDATA *prpFlight)
{
	if(prpFlight != NULL && *prpFlight->Adid != 'A')
		return true;
	return false;
}

// returns true if the flight is a turnaround
bool CedaFlightData::IsTurnaround(FLIGHTDATA *prpFlight)
{
	return (prpFlight != NULL && GetRotationFlight(prpFlight) != NULL);
}

bool CedaFlightData::IsThirdPartyFlight(FLIGHTDATA *prpFlight)
{
	bool blIsThirdPartyFlight = false;
	if(prpFlight != NULL)
	{
		blIsThirdPartyFlight = (strcmp(prpFlight->Alc2,"AZ")) ? true : false;
	}
	return blIsThirdPartyFlight;
}

// true if the advice time is set indicating that the flight is indefinately delayed
bool CedaFlightData::AdviceTimeSet(FLIGHTDATA *prpFlight)
{
	return prpFlight->Nxti == TIMENULL ? false : true;
}

CString CedaFlightData::GetGate(FLIGHTDATA *prpFlight, int ipReturnFlightType /* = ID_NOT_RETURNFLIGHT*/)
{
	CString olGate;
	if(prpFlight != NULL)
	{
		olGate = ((IsArrival(prpFlight) && ipReturnFlightType == ID_NOT_RETURNFLIGHT) || ipReturnFlightType == ID_ARR_RETURNFLIGHT) ? prpFlight->Gta1 : prpFlight->Gtd1;
	}
	return olGate;
}

CString CedaFlightData::GetGate(long lpFlightUrno, int ipReturnFlightType /* = ID_NOT_RETURNFLIGHT*/)
{
	return GetGate(GetFlightByUrno(lpFlightUrno), ipReturnFlightType);
}

//Singapore
CString CedaFlightData::GetTerminal(FLIGHTDATA *prpFlight, int ipReturnFlightType /* = ID_NOT_RETURNFLIGHT*/)
{
	CString olTerminal;
	if(prpFlight != NULL)
	{
		olTerminal = ((IsArrival(prpFlight) && ipReturnFlightType == ID_NOT_RETURNFLIGHT) || ipReturnFlightType == ID_ARR_RETURNFLIGHT) ? prpFlight->Tga1 : prpFlight->Tgd1;
	}
	return olTerminal;
}
//Singapore
CString CedaFlightData::GetTerminal(long lpFlightUrno, int ipReturnFlightType /* = ID_NOT_RETURNFLIGHT*/)
{
	return GetTerminal(GetFlightByUrno(lpFlightUrno), ipReturnFlightType);
}

CString CedaFlightData::GetPosition(FLIGHTDATA *prpFlight, int ipReturnFlightType /* = ID_NOT_RETURNFLIGHT*/)
{
	CString olPos;
	if(prpFlight != NULL)
	{
		olPos = ((IsArrival(prpFlight) && ipReturnFlightType == ID_NOT_RETURNFLIGHT) || ipReturnFlightType == ID_ARR_RETURNFLIGHT) ? prpFlight->Psta : prpFlight->Pstd;
	}
	return olPos;
}

CString CedaFlightData::GetPosition(long lpFlightUrno, int ipReturnFlightType /* = ID_NOT_RETURNFLIGHT*/)
{
	return GetPosition(GetFlightByUrno(lpFlightUrno), ipReturnFlightType);
}


void CedaFlightData::ConvertDatesToUtc(FLIGHTDATA *prpFlight)
{
	ogBasicData.ConvertDateToUtc(prpFlight->Stod);
	ogBasicData.ConvertDateToUtc(prpFlight->Stoa);
	ogBasicData.ConvertDateToUtc(prpFlight->Etod);
	ogBasicData.ConvertDateToUtc(prpFlight->Etoa);
	ogBasicData.ConvertDateToUtc(prpFlight->Tifd);
	ogBasicData.ConvertDateToUtc(prpFlight->Tifa);
	ogBasicData.ConvertDateToUtc(prpFlight->Land);
	ogBasicData.ConvertDateToUtc(prpFlight->Slot);
	ogBasicData.ConvertDateToUtc(prpFlight->Airb);
	ogBasicData.ConvertDateToUtc(prpFlight->Onbl);
	ogBasicData.ConvertDateToUtc(prpFlight->Onbe);
	ogBasicData.ConvertDateToUtc(prpFlight->Ofbl);
	ogBasicData.ConvertDateToUtc(prpFlight->Tmoa);
	ogBasicData.ConvertDateToUtc(prpFlight->Dtd1);
	ogBasicData.ConvertDateToUtc(prpFlight->Dtd2);
	ogBasicData.ConvertDateToUtc(prpFlight->Nxti);
	ogBasicData.ConvertDateToUtc(prpFlight->Iskd);
	ogBasicData.ConvertDateToUtc(prpFlight->Lstu);
}

void CedaFlightData::ConvertDatesToLocal(FLIGHTDATA *prpFlight)
{
	ogBasicData.ConvertDateToLocal(prpFlight->Stod);
	ogBasicData.ConvertDateToLocal(prpFlight->Stoa);
	ogBasicData.ConvertDateToLocal(prpFlight->Etod);
	ogBasicData.ConvertDateToLocal(prpFlight->Etoa);
	ogBasicData.ConvertDateToLocal(prpFlight->Tifd);
	ogBasicData.ConvertDateToLocal(prpFlight->Tifa);
	ogBasicData.ConvertDateToLocal(prpFlight->Land);
	ogBasicData.ConvertDateToLocal(prpFlight->Slot);
	ogBasicData.ConvertDateToLocal(prpFlight->Airb);
	ogBasicData.ConvertDateToLocal(prpFlight->Onbl);
	ogBasicData.ConvertDateToLocal(prpFlight->Onbe);
	ogBasicData.ConvertDateToLocal(prpFlight->Ofbl);
	ogBasicData.ConvertDateToLocal(prpFlight->Tmoa);
	ogBasicData.ConvertDateToLocal(prpFlight->Dtd1);
	ogBasicData.ConvertDateToLocal(prpFlight->Dtd2);
	ogBasicData.ConvertDateToLocal(prpFlight->Nxti);
	ogBasicData.ConvertDateToLocal(prpFlight->Iskd);
	ogBasicData.ConvertDateToLocal(prpFlight->Lstu);
}

CString CedaFlightData::GetFlightNum(long lpUrno)
{
	CString olFlightNum(GetString(IDS_STRING61387)); // "Flug nicht gefunden!"
	FLIGHTDATA *prlFlight = GetFlightByUrno(lpUrno);
	if(prlFlight != NULL)
	{
		olFlightNum = prlFlight->Fnum;
	}
	return olFlightNum;
}

CString CedaFlightData::GetTableName(void)
{
	return CString(pcmTableName);
}

CString CedaFlightData::GetFtypText(FLIGHTDATA *prpFlight)
{
	CString olFtypText;
	if(prpFlight != NULL)
	{
		switch(*prpFlight->Ftyp)
		{
		case 'I':
			olFtypText = GetString(IDS_FTYP_IMPORT); //"Import";
			break;
		case 'S':
			olFtypText = GetString(IDS_FTYP_SCHEDULED); //"Scheduled";
			break;
		case 'O':
			olFtypText = GetString(IDS_FTYP_OPERATION); //"Operation";
			break;
		case 'D':
			olFtypText = GetString(IDS_FTYP_DIVERTED); //"Diverted";
			break;
		case 'R':
			olFtypText = GetString(IDS_FTYP_REROUTED); //"Rerouted";
			break;
		case 'X':
			olFtypText = GetString(IDS_FTYP_CANCELLATION); //"Cancellation";
			break;
		case 'N':
			olFtypText = GetString(IDS_FTYP_NOOP); //"Noop";
			break;
		case 'T':
			olFtypText = GetString(IDS_FTYP_TOWING); //"Towing";
			break;
		case 'G':
			olFtypText = GetString(IDS_FTYP_GROUNDMOV); //"Ground Movement";
			break;
		case 'E':
			olFtypText = GetString(IDS_FTYP_ERROR); //"Error";
			break;
		case 'Z':
			olFtypText = GetString(IDS_FTYP_RETURN); //"Return Flight";
			break;
		case 'B':
			olFtypText = GetString(IDS_FTYP_RETURNFROMTAXI); //"Return From Taxi";
			break;
		default:
			olFtypText.Format(GetString(IDS_FTYP_UNKNOWN),prpFlight->Ftyp); // "Type Unknown <%s>"
			break;
		}
	}
	return olFtypText;
}

CString CedaFlightData::Dump(long lpUrno)
{
	CString olDumpStr;
	FLIGHTDATA *prlFlight = GetFlightByUrno(lpUrno);
	if(prlFlight == NULL)
	{
		olDumpStr.Format("No FLIGHTDATA Found for FLIGHT.URNO <%ld>",lpUrno);
	}
	else
	{
		FLIGHTDATA rlFlight;
		memcpy(&rlFlight,prlFlight,sizeof(FLIGHTDATA));
		CString olData;
		MakeCedaData(&omRecInfo,olData,&rlFlight);
		CStringArray olFieldArray, olDataArray;
		ogBasicData.ExtractItemList(pcmFieldList, &olFieldArray);
		ogBasicData.ExtractItemList(olData, &olDataArray);
		int ilNumData = min(olDataArray.GetSize(),olFieldArray.GetSize());
		for(int ilD = 0; ilD < ilNumData; ilD++)
		{
			olDumpStr += olFieldArray[ilD] + " = <" + olDataArray[ilD] + ">    ";
		}
	}
	return olDumpStr;
}

CTime CedaFlightData::GetFlightTime(FLIGHTDATA *prpFlight)
{
	CTime olTime = TIMENULL;

	if(prpFlight != NULL)
	{
		if(IsArrival(prpFlight))
		{
			olTime = prpFlight->Tifa;
		}
		else
		{
			olTime = prpFlight->Tifd;
		}
	}
	return olTime;
}

void CedaFlightData::GetFlightUrnos(CDWordArray &ropFlightUrnos)
{
	for(int ilF = 0; ilF < omData.GetSize(); ilF++)
	{
		if(IsDeparture(&omData[ilF]))
			ropFlightUrnos.Add((DWORD) omData[ilF].Urno);
	}
}

void CedaFlightData::HandleMissingFlight(long lpFlightUrno)
{
	if(lpFlightUrno != 0L)
	{
		FLIGHTDATA *prlFlight = GetFlightByUrno(lpFlightUrno);
		if(prlFlight == NULL)
		{
			char pclSelection[500];
			sprintf(pclSelection, "WHERE (URNO = '%ld')[ROTATIONS]", lpFlightUrno);
			ReadAllFlights(TIMENULL, TIMENULL, pclSelection);
			if((prlFlight = GetFlightByUrno(lpFlightUrno)) != NULL)
			{
				long llRotationUrno = 0L;
				FLIGHTDATA *prlRotation = GetRotationFlight(prlFlight);
				if(prlRotation != NULL) llRotationUrno = prlRotation->Urno;

				ReadJobsAndDemandsForMissingFlight(lpFlightUrno, llRotationUrno);

				ogConflicts.CheckConflicts(*prlFlight,FALSE,FALSE,TRUE);
				ogCCSDdx.DataChanged((void *)this,FLIGHT_CHANGE,(void *)prlFlight);
			}
		}
	}
}

void CedaFlightData::ReadJobsAndDemandsForMissingFlight(long lpFlightUrno, long lpRotationUrno /*= 0L*/)
{
	char pclSelection[500];

	sprintf(pclSelection, "WHERE OURI = '%ld' OR OURO = '%ld'", lpFlightUrno, lpFlightUrno);
	CDWordArray olDemUrnosAdded;
	ogDemandData.ReadDemands(pclSelection, false, true, &olDemUrnosAdded);

	int ilNumDems = olDemUrnosAdded.GetSize();
	if(ilNumDems > 0)
	{
		CString olSelection, olTmp;
		olSelection.Format("WHERE UDEM IN ('%ld'", olDemUrnosAdded[0]);
		for(int ilD = 1; ilD < ilNumDems; ilD++)
		{
			olTmp.Format(",'%ld'", olDemUrnosAdded[ilD]);
			olSelection += olTmp;
		}
		olSelection += CString(")");
		char *pclSelection = new char[olSelection.GetLength()+100];
		strcpy(pclSelection, olSelection);
		ogJodData.ReadJods(pclSelection);
		delete [] pclSelection;
	}

	if(lpRotationUrno == 0L)
	{
		sprintf(pclSelection, "WHERE UAFT = '%ld'", lpFlightUrno);
	}
	else
	{
		sprintf(pclSelection, "WHERE UAFT = '%ld' OR UAFT = '%ld'", lpFlightUrno, lpRotationUrno);
	}
	ogJobData.Read(pclSelection, false, true);
}

int CedaFlightData::GetAllFlightUrnos(CDWordArray &ropFlightUrnos)
{
	for(int i = 0; i < omData.GetSize(); i++)
	{
		ropFlightUrnos.Add((DWORD) omData[i].Urno);
	}

	return ropFlightUrnos.GetSize();
}

bool CedaFlightData::CreateDemands(long lpUrno)
{
	
	char clSelection[2048] = "";
	sprintf(clSelection,"WHERE URNO IN (%ld)",lpUrno);

	return CedaAction("SFC", clSelection);

} 