// StaffDiagramGroupPage.h : header file
//

#ifndef _STFDIAGR_H_
#define _STFDIAGR_H_

/////////////////////////////////////////////////////////////////////////////
// StaffDiagramGroupPage dialog

class StaffDiagramGroupPage : public CPropertyPage
{
	DECLARE_DYNCREATE(StaffDiagramGroupPage)

// Construction
public:
	StaffDiagramGroupPage();
	~StaffDiagramGroupPage();

// Dialog Data
	CString omGroupBy;

	//{{AFX_DATA(StaffDiagramGroupPage)
	enum { IDD = IDD_STAFFDIAGRAM_GROUP_PAGE };
	CButton	m_GroupViewCtrl;
	CButton	m_HideDelegatedGroupsCtrl;
	CButton	m_HideAbsentEmpsCtrl;
	CButton	m_HideFidEmpsCtrl;
	CButton	m_HideStandbyEmpsCtrl;
	int		m_GroupBy;
	BOOL	m_GroupView;
	BOOL	m_HideDelegatedGroups;
	BOOL	m_HideAbsentEmps;
	BOOL	m_HideFidEmps;
	BOOL	m_HideStandbyEmps;
	//}}AFX_DATA
	CString omTitle;

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(StaffDiagramGroupPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(StaffDiagramGroupPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Helper routines
private:
	int GetGroupId(const char *pcpGroupKey);
	CString GetGroupKey(int ipGroupId);
};

#endif // _STFDIAGR_H_
