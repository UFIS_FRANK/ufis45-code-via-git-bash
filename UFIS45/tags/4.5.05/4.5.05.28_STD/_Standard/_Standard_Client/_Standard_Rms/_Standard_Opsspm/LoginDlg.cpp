// LoginDlg.cpp : implementation file
//

#include <stdafx.h>
#include <CCSCedaData.h>
#include <CCSCedaCom.h>
#include <CCSGlobl.h>
#include <LoginDlg.h>
#include <PrivList.h>
#include <BasicData.h>

extern CCSCedaCom ogCommHandler;

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLoginDlg dialog

//extern CCSBasicData ogBasicData;


CLoginDlg::CLoginDlg(const char *pcpHomeAirport, const char *pcpAppl, const char *pcpWks, CWnd* pParent /*=NULL*/)
    : CDialog(CLoginDlg::IDD, pParent)
{
    //{{AFX_DATA_INIT(CLoginDlg)
	//}}AFX_DATA_INIT

	strcpy(pcmHomeAirport,pcpHomeAirport);
	strcpy(pcmAppl,pcpAppl);
	strcpy(pcmWks,pcpWks);

}

void CLoginDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CLoginDlg)
	DDX_Control(pDX, IDC_USERNAME, m_UsernameCtrl);
	DDX_Control(pDX, IDC_PASSWORD, m_PasswordCtrl);
	DDX_Control(pDX, IDC_USIDCAPTION, m_UsidCaption);
	DDX_Control(pDX, IDC_PASSCAPTION, m_PassCaption);
	DDX_Control(pDX, IDOK, m_OkCaption);
	DDX_Control(pDX, IDCANCEL, m_CancelCaption);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CLoginDlg, CDialog)
    //{{AFX_MSG_MAP(CLoginDlg)
    ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CLoginDlg message handlers

void CLoginDlg::OnPaint() 
{
    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    CBrush olBrush( RGB( 192, 192, 192 ) );
    CBrush *polOldBrush = dc.SelectObject( &olBrush );
    CRect olRect;
    GetClientRect( &olRect );
    dc.FillRect( &olRect, &olBrush );
    dc.SelectObject( polOldBrush );
    
    CDC olDC;
    olDC.CreateCompatibleDC( &dc );
    CBitmap olBitmap;
    olBitmap.LoadBitmap( IDB_LOGIN );
    olDC.SelectObject( &olBitmap );
    dc.BitBlt( 0, 140, 409, 52, &olDC, 0, 0, SRCCOPY );
    olDC.DeleteDC( );
    
    // Do not call CDialog::OnPaint() for painting messages
}

void CLoginDlg::OnOK() 
{
	bool blRc = true;
	CWnd *polFocusCtrl = &m_UsernameCtrl;
	omErrTxt.Empty();

	// get the username
	m_UsernameCtrl.GetWindowText(omUsername);
	m_PasswordCtrl.GetWindowText(omPassword);

	ogCommHandler.SetUser(omUsername);
	strcpy(CCSCedaData::pcmUser, omUsername);
	strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);

	// check the username + password
	AfxGetApp()->DoWaitCursor(1);
	blRc = ogPrivList.Login(pcmHomeAirport,omUsername,omPassword,pcmAppl,pcmWks);
	if (bgIsPrm)
	{
		ogPrmPrivList.Login(pcmHomeAirport,omUsername,omPassword,"GlobalPar",pcmWks);
	}
	AfxGetApp()->DoWaitCursor(-1);


	if( blRc )
	{
		CDialog::OnOK();
	}
	else
	{
		if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_USER") ) { 
			omErrTxt = GetString(IDS_INVALID_USERNAME);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_APPLICATION") ) {
			omErrTxt = GetString(IDS_INVALID_APPLICATION);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_PASSWORD") ) {
			omErrTxt = GetString(IDS_INVALID_PASSWORD);
			polFocusCtrl = &m_PasswordCtrl;
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_USER") ) {
			omErrTxt = GetString(IDS_EXPIRED_USERNAME);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_APPLICATION") ) {
			omErrTxt = GetString(IDS_EXPIRED_APPLICATION);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_WORKSTATION") ) {
			omErrTxt = GetString(IDS_EXPIRED_WORKSTATION);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_USER") ) {
			omErrTxt = GetString(IDS_DISABLED_USERNAME);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_APPLICATION") ) {
			omErrTxt = GetString(IDS_DISABLED_APPLICATION);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_WORKSTATION") ) {
			omErrTxt = GetString(IDS_DISABLED_WORKSTATION);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR UNDEFINED_PROFILE") ) {
			omErrTxt = GetString(IDS_UNDEFINED_PROFILE);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR MUST_CHANGE_PASSWORD_FIRST") ) {
			omErrTxt = GetString(IDS_CHANGE_PASSWORD);
		}
		else {
//			omErrTxt = ogPrivList.omErrorMessage;
			omErrTxt.Format(GetString(IDS_DB_ERROR),ogPrivList.omErrorMessage);
		}

		MessageBox(omErrTxt,GetString(IDS_INVALIDLOGIN_CAPTION),MB_ICONINFORMATION);
	
		imLoginCount++;

		if( imLoginCount >= MAX_LOGIN )
			OnCancel();
		else
		{
			if( polFocusCtrl != NULL )
				polFocusCtrl->SetFocus(); // set the focus to the offending control
		}
	}
}


bool CLoginDlg::Login(const char *pcpUsername, const char *pcpPassword)
{
	bool blRc = true;

	omErrTxt.Empty();

	ogCommHandler.SetUser(pcpUsername);
	strcpy(CCSCedaData::pcmUser, pcpUsername);
	strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);

	// check the username + password
	AfxGetApp()->DoWaitCursor(1);
	blRc = ogPrivList.Login(pcmHomeAirport,pcpUsername,pcpPassword,pcmAppl,pcmWks);
	AfxGetApp()->DoWaitCursor(-1);

	if( ! blRc )
	{
		if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_USER") ) { 
			omErrTxt = GetString(IDS_INVALID_USERNAME);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_APPLICATION") ) {
			omErrTxt = GetString(IDS_INVALID_APPLICATION);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR INVALID_PASSWORD") ) {
			omErrTxt = GetString(IDS_INVALID_PASSWORD);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_USER") ) {
			omErrTxt = GetString(IDS_EXPIRED_USERNAME);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_APPLICATION") ) {
			omErrTxt = GetString(IDS_EXPIRED_APPLICATION);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR EXPIRED_WORKSTATION") ) {
			omErrTxt = GetString(IDS_EXPIRED_WORKSTATION);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_USER") ) {
			omErrTxt = GetString(IDS_DISABLED_USERNAME);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_APPLICATION") ) {
			omErrTxt = GetString(IDS_DISABLED_APPLICATION);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR DISABLED_WORKSTATION") ) {
			omErrTxt = GetString(IDS_DISABLED_WORKSTATION);
		}
		else if( !strcmp(ogPrivList.omErrorMessage,"LOGINERROR UNDEFINED_PROFILE") ) {
			omErrTxt = GetString(IDS_UNDEFINED_PROFILE);
		}
		else {
//			omErrTxt = ogPrivList.omErrorMessage;
			omErrTxt.Format(GetString(IDS_DB_ERROR),ogPrivList.omErrorMessage);
		}

		MessageBox(omErrTxt,GetString(IDS_INVALIDLOGIN_CAPTION),MB_ICONINFORMATION);
	}

	return blRc;
}

void CLoginDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}




BOOL CLoginDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	SetWindowText(GetString(IDS_STRING32996));
	CWnd *polWnd = GetDlgItem(IDC_USIDCAPTION);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32995));
	}
	polWnd = GetDlgItem(IDC_PASSCAPTION);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32997));
	}
	polWnd = GetDlgItem(IDOK);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61697));
	}
	polWnd = GetDlgItem(IDCANCEL);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61698));
	}

	polWnd = GetDlgItem(IDC_STATIC_VERSION);
	if (polWnd)
	{
		polWnd->SetWindowText(ogBasicData.GetVersionString());
	}

	imLoginCount = 0;

	m_UsernameCtrl.SetTypeToString("X(32)",32,1);
	m_UsernameCtrl.SetBKColor(YELLOW);
	m_UsernameCtrl.SetTextErrColor(RED);

	m_PasswordCtrl.SetTypeToString("X(32)",32,1);
	m_PasswordCtrl.SetBKColor(YELLOW);
	m_PasswordCtrl.SetTextErrColor(RED);


	m_UsernameCtrl.SetFocus();

	CString olCaption;
	olCaption.Format(GetString(IDS_LOGIN_CAPTION),pcgAppName,ogCommHandler.pcmHostName,ogCommHandler.pcmHostType);
	SetWindowText(olCaption);


	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

