// CicDemandTableSortPage.h : header file
//
#ifndef _CICDEMANDTBSO_H_
#define _CICDEMANDTBSO_H_

/////////////////////////////////////////////////////////////////////////////
// CicDemandTableSortPage dialog

class CicDemandTableSortPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CicDemandTableSortPage)

// Construction
public:
	CicDemandTableSortPage();
	~CicDemandTableSortPage();

// Dialog Data
	CStringArray omSortOrders;
	CString		 omTitle;

	//{{AFX_DATA(CicDemandTableSortPage)
	enum { IDD = IDD_CIC_DEMANDTABLE_SORT_PAGE };
	int		m_SortOrder0;
	int		m_SortOrder1;
	int		m_SortOrder2;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CicDemandTableSortPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CicDemandTableSortPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Helper routines
private:
	int		GetSortOrder(const char *pcpSortKey);
	CString GetSortKey(int ipSortOrder);
};

#endif // _CICDEMANDTBSO_H_
