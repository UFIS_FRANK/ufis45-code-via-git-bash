// Class for Equipment
#ifndef _CEDAEQUDATA_H_
#define _CEDAEQUDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct EquDataStruct
{
	long	Urno;		// Unique Record Number
	char	Enam[41];	// name/number
	char	Eqps[6];	// parking stand (position)
	char	Etyp[41];	// type/manufacturer
	char	Gcde[6];	// code
	long	Gkey;		// equipment type (EQTTAB.URNO)
	char	Ivnr[21];	// inventory number
	char	Rema[61];	// remark
	char	Crqu[21];	// required qualification
	char	Tele[21];	// Telephone Number

	EquDataStruct(void)
	{
		Urno = 0L;
		strcpy(Enam,"");
		strcpy(Eqps,"");
		strcpy(Etyp,"");
		strcpy(Gcde,"");
		Gkey = 0L;
		strcpy(Ivnr,"");
		strcpy(Rema,"");
		strcpy(Crqu,"");
		strcpy(Tele,"");
	}
};

typedef struct EquDataStruct EQUDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaEquData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <EQUDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omGkeyMap;

// Operations
public:
	CedaEquData();
	~CedaEquData();

	CCSReturnCode ReadEquData();
	EQUDATA* GetEquByUrno(long lpUrno);
	bool GetEquipmentByType(long lpGkey, CCSPtrArray <EQUDATA> &ropEquipmentList, bool bpReset = true);
	EQUDATA *GetEquipmentByTypeAndName(long lpGkey, CString opEnam);
	void GetEquipmentNames(CStringArray &ropEquipmentNames) const;
	void ProcessEquBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	CString GetTableName(void);
	CString Dump(long lpUrno);

private:
	EQUDATA *AddEquInternal(EQUDATA &rrpEqu);
	void DeleteEquInternal(long lpUrno);
	void ClearAll();
};


extern CedaEquData ogEquData;
#endif _CEDAEQUDATA_H_
