// PrintCard.cpp : implementation file
//  

#include <stdafx.h>
#include <PrintCard.h>
#include <resource.h>
#include <WINSPOOL.H>
#include <CedaJobData.h>
#include <CedaJodData.h>
#include <CedaAzaData.h>
#include <CedaEmpData.h>
#include <CedaRudData.h>
#include <CedaSerData.h>
#include <CedaTlxData.h>
#include <CedaCcaData.h>
#include <CedaRemData.h>
#include <basicdata.h>
#include <FieldConfigDlg.h>
#include <CedaCfgData.h>
#include <TelexTypeDlg.h>

#include <math.h>

#ifndef	INCH
#	define INCH 0.284
#endif

#ifndef	MMX
#	define MMX(x) ((int)(MulDiv((x),imLogPixelsX, 72)*INCH))
#endif

#ifndef	MMY
#	define MMY(x) ((int)(MulDiv((x),imLogPixelsY, 72)*INCH))
#endif

static int ByStartTime(const DEMANDDATA **pppDemand1, const DEMANDDATA **pppDemand2)
{
	int ilRc = (int)((**pppDemand1).Debe.GetTime() - (**pppDemand2).Debe.GetTime());
	if (ilRc == 0)
		ilRc = (int)((**pppDemand1).Urno - (**pppDemand2).Urno);
	return ilRc;
}

//-----------------------------------------------------------------------------------------------

PrintCard::CardData::CardData()
{
	// common
	AFT_Regn  = "";
	AFT_Act3  = "";
	AFT_Act5  = "";

	// arrival
	AFT_UrnoA = 0;
	AFT_FlnoA = "";
	AFT_Stoa  = -1;
	AFT_Tifa  = -1;
	AFT_Etoa  = -1;
	AFT_Psta  = "";
	AFT_Org3 = "";
	AFT_Via3A = "";
	AFT_Gta1  = "";
	AFT_TtypA = "";
	AFT_PaxtA = 0;
	AFT_CgotA = 0;
	AFT_BagnA = 0;
	AFT_MailA = 0;
	AFT_Blt1A = "";

	// departure
	AFT_UrnoD = 0;
	AFT_FlnoD = "";
	AFT_Stod  = -1;
	AFT_Tifd  = -1;
	AFT_Etod  = -1;
	AFT_CIC  = "";
	AFT_Pstd  = "";
	AFT_Des3 = "";
	AFT_Via3D = "";
	AFT_Gtd1  = "";
	AFT_TtypD = "";
	AFT_PaxtD = 0;
	AFT_CgotD = 0;
	AFT_BagnD = 0;
	AFT_MailD = 0;
	AFT_Blt1D = "";

	TLX_Text     = "";
	GHP_Rema     = "";
	imGhdLines   = 0;
	imRemaLines  = 0;
	imTelexLines = 0;
}

PrintCard::CardData::~CardData()
{
    CardDutyData.DeleteAll();
}

PrintCard::CardData::CardData(const CardData& Cd)
{
	this->CardDutyData.DeleteAll();
	
	this->AFT_Regn   = Cd.AFT_Regn;
	this->AFT_Act3   = Cd.AFT_Act3;
	this->AFT_Act5   = Cd.AFT_Act5;
	this->AFT_UrnoA  = Cd.AFT_UrnoA;
	this->AFT_FlnoA  = Cd.AFT_FlnoA;
	this->AFT_Stoa   = Cd.AFT_Stoa;
	this->AFT_Tifa   = Cd.AFT_Tifa;
	this->AFT_Etoa   = Cd.AFT_Etoa;
	this->AFT_Psta   = Cd.AFT_Psta;
	this->AFT_Org3  = Cd.AFT_Org3;
	this->AFT_Via3A  = Cd.AFT_Via3A;
	this->AFT_Gta1   = Cd.AFT_Gta1;
	this->AFT_TtypA  = Cd.AFT_TtypA;
	this->AFT_PaxtA  = Cd.AFT_PaxtA;
	this->AFT_CgotA  = Cd.AFT_CgotA;
	this->AFT_BagnA  = Cd.AFT_BagnA;
	this->AFT_MailA  = Cd.AFT_MailA;
	this->AFT_Blt1A	 = Cd.AFT_Blt1A;

	this->AFT_UrnoD  = Cd.AFT_UrnoD;
	this->AFT_FlnoD  = Cd.AFT_FlnoD;
	this->AFT_Stod   = Cd.AFT_Stod;
	this->AFT_Tifd   = Cd.AFT_Tifd;
	this->AFT_Etod   = Cd.AFT_Etod;
	this->AFT_CIC   = Cd.AFT_CIC;
	this->AFT_Pstd   = Cd.AFT_Pstd;
	this->AFT_Des3  = Cd.AFT_Des3;
	this->AFT_Via3D  = Cd.AFT_Via3D;
	this->AFT_Gtd1   = Cd.AFT_Gtd1;
	this->AFT_TtypD  = Cd.AFT_TtypD;
	this->AFT_PaxtD  = Cd.AFT_PaxtD;
	this->AFT_CgotD  = Cd.AFT_CgotD;
	this->AFT_BagnD  = Cd.AFT_BagnD;
	this->AFT_MailD  = Cd.AFT_MailD;
	this->AFT_Blt1D	 = Cd.AFT_Blt1D;

	this->TLX_Text   = Cd.TLX_Text;
	this->GHP_Rema   = Cd.GHP_Rema;
	this->imGhdLines = Cd.imGhdLines;
	this->imRemaLines= Cd.imRemaLines;
	this->imTelexLines= Cd.imTelexLines;


	for(int i = 0; i < Cd.CardDutyData.GetSize(); i++)
	{
		this->CardDutyData.NewAt(CardDutyData.GetSize(),Cd.CardDutyData[i]);
	}
}


const PrintCard::CardData& PrintCard::CardData::operator= ( const CardData& Cd)
{
	if(this == &Cd)
	{
		return *this;
	}

	this->CardDutyData.DeleteAll();
	
	this->AFT_Regn   = Cd.AFT_Regn;
	this->AFT_Act3   = Cd.AFT_Act3;
	this->AFT_Act5   = Cd.AFT_Act5;
	this->AFT_UrnoA  = Cd.AFT_UrnoA;
	this->AFT_FlnoA  = Cd.AFT_FlnoA;
	this->AFT_Stoa   = Cd.AFT_Stoa;
	this->AFT_Tifa   = Cd.AFT_Tifa;
	this->AFT_Etoa   = Cd.AFT_Etoa;
	this->AFT_Psta   = Cd.AFT_Psta;
	this->AFT_Org3  = Cd.AFT_Org3;
	this->AFT_Via3A  = Cd.AFT_Via3A;
	this->AFT_Gta1   = Cd.AFT_Gta1;
	this->AFT_TtypA  = Cd.AFT_TtypA;
	this->AFT_PaxtA  = Cd.AFT_PaxtA;
	this->AFT_CgotA  = Cd.AFT_CgotA;
	this->AFT_BagnA  = Cd.AFT_BagnA;
	this->AFT_MailA  = Cd.AFT_MailA;
	this->AFT_Blt1A	 = Cd.AFT_Blt1A;

	this->AFT_UrnoD  = Cd.AFT_UrnoD;
	this->AFT_FlnoD  = Cd.AFT_FlnoD;
	this->AFT_Stod   = Cd.AFT_Stod;
	this->AFT_Tifd   = Cd.AFT_Tifd;
	this->AFT_Etod   = Cd.AFT_Etod;
	this->AFT_CIC   = Cd.AFT_CIC;
	this->AFT_Pstd   = Cd.AFT_Pstd;
	this->AFT_Des3  = Cd.AFT_Des3;
	this->AFT_Via3D  = Cd.AFT_Via3D;
	this->AFT_Gtd1   = Cd.AFT_Gtd1;
	this->AFT_TtypD  = Cd.AFT_TtypD;
	this->AFT_PaxtD  = Cd.AFT_PaxtD;
	this->AFT_CgotD  = Cd.AFT_CgotD;
	this->AFT_BagnD  = Cd.AFT_BagnD;
	this->AFT_MailD  = Cd.AFT_MailD;
	this->AFT_Blt1D	 = Cd.AFT_Blt1D;

	this->TLX_Text      = Cd.TLX_Text;
	this->GHP_Rema      = Cd.GHP_Rema;
	this->imGhdLines    = Cd.imGhdLines;
	this->imRemaLines   = Cd.imRemaLines;
	this->imTelexLines  = Cd.imTelexLines;


	for(int i = 0; i < Cd.CardDutyData.GetSize(); i++)
	{
		this->CardDutyData.NewAt(CardDutyData.GetSize(),Cd.CardDutyData[i]);
	}
	return *this;

}
//-----------------------------------------------------------------------------------------------

PrintCard::PrintCard(CWnd *opParent)
{
	bmIsInitialized = false;
	pomParent = opParent;
	
	omCardName = CString(GetString(IDS_PRINTCARD01));
	imOrientation = PRINTCARD_PORTRAIT;
	imCardNo = 0;
	imLineHeight = 50;
	imActualLine = 0;
	
	if (imOrientation == PRINTCARD_LANDSCAPE)
	{
		imFirstPos  = 200;
		imLeftOffset = 50;
		imMaxLines = 26;
	}
	else
	{
		imFirstPos  = 200;
		imLeftOffset = 50;
		imMaxLines = 42;
	}
	imActualPos = imFirstPos;
	hmBitmap = NULL;
	hmPalette= NULL;
}

//-----------------------------------------------------------------------------------------------

PrintCard::~PrintCard()
{
	omCardData.DeleteAll();

	if (bmIsInitialized == true)
	{
		pomCdc->DeleteDC();
		omThinPen.DeleteObject();
		omMediumPen.DeleteObject();
		omThickPen.DeleteObject();
		omDottedPen.DeleteObject();
		omRgn.DeleteObject();
	}
}

//-----------------------------------------------------------------------------------------------

bool PrintCard::InitializePrinter(egPrintCardInfo ipOrientation)
{
	int ilRc;
	HDC hlHdc;
	char pclDevices[182];
	::GetProfileString("windows", "device","",pclDevices,180);
	CString olDevices = pclDevices;
	CString olDeviceName;
	int ilKomma = olDevices.Find(',');
	if (ilKomma > -1) olDeviceName = olDevices.Left(ilKomma);
	char pclDeviceName[182];
	strcpy(pclDeviceName,olDeviceName);
	HANDLE  hlPrinter = 0;
	static HANDLE hDevMode = 0;
	//if (hDevMode == 0)
	{
		if (OpenPrinter(pclDeviceName,&hlPrinter,NULL) == TRUE)
		{
			//if (GetPrinter(hlPrinter,2,(unsigned char *)&rlPrinterInfo,sizeof(rlPrinterInfo),&llBytesReceived) == TRUE)
			{
				LONG llDevModeLen = DocumentProperties(NULL,hlPrinter,pclDeviceName,NULL,NULL,0);
				if (llDevModeLen > 0)
				{
					hDevMode = GlobalAlloc(GMEM_MOVEABLE,llDevModeLen);
					DEVMODE *prlDevMode = (DEVMODE *) GlobalLock(hDevMode);
					DocumentProperties(NULL,hlPrinter,pclDeviceName,prlDevMode,NULL,DM_OUT_BUFFER);
					DWORD dm_orientation =  DM_ORIENTATION;
					prlDevMode->dmFields = DM_ORIENTATION;
					if(imOrientation == PRINTCARD_LANDSCAPE)
					{
						prlDevMode->dmOrientation = DMORIENT_LANDSCAPE;
					}
					else
					{
						prlDevMode->dmOrientation = DMORIENT_PORTRAIT;
					}
					DocumentProperties(NULL,hlPrinter,pclDeviceName,prlDevMode,prlDevMode,DM_IN_BUFFER|DM_OUT_BUFFER);
					GlobalUnlock(hDevMode);
				}
			}
			ClosePrinter(hlPrinter);
		}
	}
	CPrintDialog *polPrintDialog = new CPrintDialog(FALSE,PD_ALLPAGES|PD_NOPAGENUMS|PD_NOSELECTION|PD_USEDEVMODECOPIESANDCOLLATE,pomParent);
	if (hDevMode != 0) 
		polPrintDialog->m_pd.hDevMode = hDevMode;

	LPDEVMODE prlOldDevMode = polPrintDialog->GetDevMode( );
	ilRc = polPrintDialog->DoModal();
	if (ilRc != IDCANCEL )
	{
		LPDEVMODE prlDevMode = polPrintDialog->GetDevMode( );

		hlHdc = polPrintDialog->GetPrinterDC();
		if(!prlDevMode || hlHdc == NULL)
		{
			return false;
		}
		else
		{
			omCdc.Attach(hlHdc);
			delete polPrintDialog;
			pomCdc = &omCdc;
			if (prlDevMode->dmOrientation == DMORIENT_PORTRAIT)
			{
				imOrientation = PRINTCARD_PORTRAIT;
			}
			else
			{
				imOrientation = PRINTCARD_LANDSCAPE;
			}
			return true;
		}
	}
	else
	{
		return false;
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

bool PrintCard::InitializePrinter(CDC* popCDC, egPrintCardInfo ipOrientation)
{
	pomCdc = popCDC;
	return true;
}

bool PrintCard::InitializePrintSetup(PRINT_OPTION epPrintOption, egPrintCardInfo ipOrientation)
{	
	int ilRc;

	if (epPrintOption ==  PRINT_PRINTER)
	{
		pomCdc = &omCdc;
		imPreviewPage = 0;
	}

	if(ipOrientation  == PRINTCARD_LANDSCAPE)
	{
		imFirstPos  = 200;
		imLeftOffset = 50;
		imMaxLines = 28;
		imOrientation = PRINTCARD_LANDSCAPE;
	}
	else
	{
		imFirstPos  = 200;
		imLeftOffset = 50;
		imMaxLines = 42;
		imOrientation = PRINTCARD_PORTRAIT;
	}
	pomCdc->SetMapMode(MM_TEXT);

	omRgn.DeleteObject();

	imLogPixelsY = pomCdc->GetDeviceCaps(LOGPIXELSY);
	imLogPixelsX = pomCdc->GetDeviceCaps(LOGPIXELSX);
	omRgn.CreateRectRgn(0,0,pomCdc->GetDeviceCaps(HORZRES),pomCdc->GetDeviceCaps(VERTRES));
	pomCdc->SelectClipRgn(&omRgn);
	LOGFONT rlLf;
	memset(&rlLf, 0, sizeof(LOGFONT));

	omCourierNew_Regular_8.DeleteObject();
	omCourierNew_Bold_8.DeleteObject();
	omArial_Regular_8.DeleteObject();
	omArial_Regular_10.DeleteObject();
	omArial_Regular_12.DeleteObject();
	omArial_Regular_18.DeleteObject();
	omArial_Bold_8.DeleteObject();
	omArial_Bold_10.DeleteObject();
	omArial_Bold_11.DeleteObject();
	omArial_Bold_12.DeleteObject();
	omArial_Bold_18.DeleteObject();

	//Courier New 8 
	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight = - MulDiv(8, imLogPixelsY, 72);
	rlLf.lfWeight = FW_NORMAL;
	rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
	lstrcpy(rlLf.lfFaceName,"Courier New");
	ilRc = omCourierNew_Regular_8.CreateFontIndirect(&rlLf);
	//Courier New 8 Bold
	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfWeight = FW_BOLD;
	ilRc = omCourierNew_Bold_8.CreateFontIndirect(&rlLf);
	//Arial 8
	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfWeight = FW_NORMAL;
	rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
	lstrcpy(rlLf.lfFaceName,"Arial");
	ilRc = omArial_Regular_8.CreateFontIndirect(&rlLf);
	//Arial 10
	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight = - MulDiv(10, imLogPixelsY, 72); 
	ilRc = omArial_Regular_10.CreateFontIndirect(&rlLf);
	//Arial 12
	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight = - MulDiv(12, imLogPixelsY, 72); 
	ilRc = omArial_Regular_12.CreateFontIndirect(&rlLf);
	//Arial 18
	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight =  - MulDiv(18, imLogPixelsY, 72);   
	ilRc = omArial_Regular_18.CreateFontIndirect(&rlLf);
	//Arial 8 Bold
	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight = - MulDiv(8, imLogPixelsY, 72);
	rlLf.lfWeight = FW_BOLD;
	ilRc = omArial_Bold_8.CreateFontIndirect(&rlLf);
	//Arial 10 Bold
	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight = - MulDiv(10, imLogPixelsY, 72); 
	ilRc = omArial_Bold_10.CreateFontIndirect(&rlLf);
	//Arial 11 Bold
	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight = - MulDiv(11, imLogPixelsY, 72); 
	ilRc = omArial_Bold_11.CreateFontIndirect(&rlLf);
	//Arial 12 Bold
	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight = - MulDiv(12, imLogPixelsY, 72); 
	ilRc = omArial_Bold_12.CreateFontIndirect(&rlLf);
	//Arial 18 Bold
	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight =  - MulDiv(18, imLogPixelsY, 72);   
	ilRc = omArial_Bold_18.CreateFontIndirect(&rlLf);
	///////////////

	omThinPen.DeleteObject();
	omMediumPen.DeleteObject();
	omThickPen.DeleteObject();
	omDottedPen.DeleteObject();

	omThinPen.CreatePen(PS_SOLID, MulDiv(2, imLogPixelsX, 254), RGB(0,0,0));
	omMediumPen.CreatePen(PS_SOLID, MulDiv(4, imLogPixelsX, 254), RGB(0,0,0));
	omThickPen.CreatePen(PS_SOLID, MulDiv(8, imLogPixelsX, 254), RGB(0,0,0));
	omDottedPen.CreatePen(PS_DOT,1, RGB(0,0,0));

	// loading of the header bitmap from ceda.ini defined resource
	char pclTmpText[512];
	char pclConfigPath[512];
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(pcgAppName, "LOGO","",
			pclTmpText, sizeof pclTmpText, pclConfigPath);

	// userdefined "logo" available ?
	if (strlen(pclTmpText))
	{
		if (!LoadBitmapFromBMPFile(pclTmpText,&hmBitmap,&hmPalette))
		{
			hmBitmap = NULL;
			hmPalette= NULL;
			if (omBitmap.LoadBitmap(IDB_OPSSPM))
			{
				hmBitmap = NULL;
				hmPalette= NULL;
				omMemDc.CreateCompatibleDC(pomCdc);
				omMemDc.SelectObject(&omBitmap);
			}
		}
	}
	else
	{
		if (omBitmap.LoadBitmap(IDB_OPSSPM))
		{
			hmBitmap = NULL;
			hmPalette= NULL;
			omMemDc.CreateCompatibleDC(pomCdc);
			omMemDc.SelectObject(&omBitmap);
		}
	}

	bmIsInitialized = true;

	return true;
}

//-----------------------------------------------------------------------------------------------

bool PrintCard::Print(FLIGHTDATA *prpArrFlight,FLIGHTDATA *prpDepFlight, CCSPtrArray <DEMANDDATA> &ropDemands, CCSPtrArray <JOBDATA> &ropJobsWithoutDemands)
{
	if (!prpArrFlight && !prpDepFlight)
		return false;


	omCardData.DeleteAll();
	GetCardData(prpArrFlight,prpDepFlight,ropDemands,ropJobsWithoutDemands);

	return Print(0);
}

//-----------------------------------------------------------------------------------------------

bool PrintCard::Print(int ipPageNumber)
{
	if (ipPageNumber > imMaxPage)
	{
		imPreviewPage = imMaxPage;
	}
	else if (ipPageNumber < 0)
	{
		imPreviewPage = 0;
	}
	else
	{
		imPreviewPage = ipPageNumber;
	}

	DOCINFO	rlDocInfo;
	memset(&rlDocInfo, 0, sizeof(DOCINFO));
	rlDocInfo.cbSize = sizeof( DOCINFO );
	rlDocInfo.lpszDocName = omCardName;	
	pomCdc->StartDoc( &rlDocInfo );
	imCardNo = 0;
	int ilCards = omCardData.GetSize();
	for(int ilCardNo = 0; ilCardNo < ilCards; ilCardNo++ ) 
	{
		imCardNo = ilCardNo;
		imActualPage = 0;
		PrintHeader();
		PrintTable();
		PrintRemark();
		PrintTelex();
		if (imActualPage == imPreviewPage || imPreviewPage == 0)
		{
			pomCdc->EndPage();
		}
	}
	pomCdc->EndDoc();

	return true;
}

//-----------------------------------------------------------------------------------------------

bool PrintCard::GetCardData(FLIGHTDATA *prpArrivalFlight,FLIGHTDATA *prpDepartureFlight,CCSPtrArray<DEMANDDATA>& ropDemands,CCSPtrArray <JOBDATA> &ropJobsWithoutDemands)
{

	CardData *prpCard = new CardData;
	omCardData.Add(prpCard);

	if (prpArrivalFlight != NULL)
	{
		prpCard->AFT_Regn   = prpArrivalFlight->Regn;
		prpCard->AFT_Act3   = prpArrivalFlight->Act3;
		prpCard->AFT_Act5   = prpArrivalFlight->Act5;
		prpCard->AFT_UrnoA  = prpArrivalFlight->Urno;
		prpCard->AFT_FlnoA  = prpArrivalFlight->Fnum;
		prpCard->AFT_Stoa   = prpArrivalFlight->Stoa;
		prpCard->AFT_Tifa   = prpArrivalFlight->Tifa;
		prpCard->AFT_Etoa   = prpArrivalFlight->Etoa;
		prpCard->AFT_Psta   = prpArrivalFlight->Psta;
		prpCard->AFT_Org3   = prpArrivalFlight->Org3;
		prpCard->AFT_Via3A  = prpArrivalFlight->Via3;
		prpCard->AFT_Gta1   = prpArrivalFlight->Gta1;
		prpCard->AFT_TtypA  = prpArrivalFlight->Ttyp;
		prpCard->AFT_PaxtA  = prpArrivalFlight->Paxt;
		prpCard->AFT_CgotA  = prpArrivalFlight->Cgot;
		prpCard->AFT_BagnA  = prpArrivalFlight->Bagn;
		prpCard->AFT_MailA  = prpArrivalFlight->Mail;
		prpCard->AFT_Blt1A  = CString(prpArrivalFlight->Blt1);

		CString olRemark = ogRemData.GetRemarkByRurn(prpArrivalFlight->Urno);
		if(!olRemark.IsEmpty())
		{
			prpCard->GHP_Rema += olRemark + CString(";  ");
			if(prpCard->GHP_Rema.GetLength() > 178) 
			{
				prpCard->imRemaLines += 3;
			}
			else if (prpCard->GHP_Rema.GetLength() > 89)
			{
				prpCard->imRemaLines += 2;
			}
			else if (prpCard->GHP_Rema.GetLength() > 0)
			{
				prpCard->imRemaLines += 1;
			}
		}
	}

	if (prpDepartureFlight != NULL)
	{
		prpCard->AFT_Regn   = prpDepartureFlight->Regn;
		prpCard->AFT_Act3   = prpDepartureFlight->Act3;
		prpCard->AFT_Act5   = prpDepartureFlight->Act5;
		prpCard->AFT_UrnoD  = prpDepartureFlight->Urno;
		prpCard->AFT_FlnoD  = prpDepartureFlight->Fnum;
		prpCard->AFT_Stod   = prpDepartureFlight->Stod;
		prpCard->AFT_Tifd   = prpDepartureFlight->Tifd;
		prpCard->AFT_Etod   = prpDepartureFlight->Etod;
		prpCard->AFT_CIC   =  ogCcaData.GetCounterRangeForFlight(prpDepartureFlight->Urno);
		prpCard->AFT_Pstd   = prpDepartureFlight->Pstd;
		prpCard->AFT_Des3   = prpDepartureFlight->Des3;
		prpCard->AFT_Via3D  = prpDepartureFlight->Via3;
		prpCard->AFT_Gtd1   = prpDepartureFlight->Gtd1;
		prpCard->AFT_TtypD  = prpDepartureFlight->Ttyp;
		prpCard->AFT_PaxtD  = prpDepartureFlight->Paxt;
		prpCard->AFT_CgotD  = prpDepartureFlight->Cgot;
		prpCard->AFT_BagnD  = prpDepartureFlight->Bagn;
		prpCard->AFT_MailD  = prpDepartureFlight->Mail;
		prpCard->AFT_Blt1D  = CString(prpDepartureFlight->Baz1);//CString(prpDepartureFlight->Blt1);

		CString olRemark = ogRemData.GetRemarkByRurn(prpDepartureFlight->Urno);
		if(!olRemark.IsEmpty())
		{
			prpCard->GHP_Rema += olRemark + CString(";  ");
			if(prpCard->GHP_Rema.GetLength() > 178) 
			{
				prpCard->imRemaLines += 3;
			}
			else if (prpCard->GHP_Rema.GetLength() > 89)
			{
				prpCard->imRemaLines += 2;
			}
			else if (prpCard->GHP_Rema.GetLength() > 0)
			{
				prpCard->imRemaLines += 1;
			}
		}
	}
	

	int ilDemandsCount = ropDemands.GetSize();
	int i = 0;
	for(; i < ilDemandsCount; i++)
	{
		DEMANDDATA rlDemData = ropDemands[i];

		CCSPtrArray<JOBDATA> olJobs;
		ogJodData.GetJobsByDemand(olJobs,rlDemData.Urno);
		
		if (olJobs.GetSize() == 0)
		{
			CARDDUTYDATA *prpCardDuty = new CARDDUTYDATA;
			prpCard->CardDutyData.Add(prpCardDuty);
			prpCard->imGhdLines++;
			prpCardDuty->Ujob = rlDemData.Urno;
			prpCardDuty->Ustf = 0;
			prpCardDuty->Acfr = rlDemData.Debe;
			prpCardDuty->Acto = rlDemData.Deen;
			prpCardDuty->DemandFunction = ogBasicData.GetFunctionsForDemand(&rlDemData);

			RUDDATA *prlRudData = ogRudData.GetRudByUrno(rlDemData.Urud);
			if (prlRudData != NULL)
			{
				SERDATA *prlSerData = ogSerData.GetSerByUrno(prlRudData->Ughs);
				if (prlSerData != NULL)
				{
					prpCardDuty->GHS_Urno = prlSerData->Urno;
					prpCardDuty->GHS_Lknm.Format("%d. %s", i+1, prlSerData->Snam);
				}
			}
			else
			{
				AZADATA *prlAzaData = ogAzaData.GetAzaByUrno(rlDemData.Udgr);
				if (prlAzaData != NULL)
				{
					if (*rlDemData.Dety == P05RES_DEMAND)
					{
						prpCardDuty->GHS_Urno = rlDemData.Udgr;
						prpCardDuty->GHS_Lknm.Format("%d. %s", i+1, prlAzaData->Seco);
					}
					else if(*rlDemData.Dety == CAS_DEMAND)
					{
					}
					else if(*rlDemData.Dety == MANUAL_THIRDPARTY_DEMAND)
					{
						prpCardDuty->GHS_Urno = rlDemData.Udgr;
						prpCardDuty->GHS_Lknm.Format("%d. %s", i+1, prlAzaData->Seco);
					}
				}
			}
						
			if (prpCardDuty->DSR_Esnm.GetLength() == 0) 
			{
				prpCardDuty->DSR_Esnm = "...";
			}

			if (prpCardDuty->GHS_Lknm.GetLength() == 0) 
			{
				prpCardDuty->GHS_Lknm.Format("%d. %s", i+1, "...");
			}

		}
		else
		{
			for (int j = 0; j < olJobs.GetSize(); j++)
			{
				JOBDATA rlJobData = olJobs[j];

				CARDDUTYDATA *prpCardDuty = new CARDDUTYDATA;
				prpCard->CardDutyData.Add(prpCardDuty);
				prpCard->imGhdLines++;
				prpCardDuty->Ujob = rlJobData.Urno;
				prpCardDuty->Ustf = rlJobData.Ustf;
				prpCardDuty->Acfr = rlJobData.Acfr;
				prpCardDuty->Acto = rlJobData.Acto;
				prpCardDuty->DemandFunction = ogBasicData.GetFunctionsForDemand(&rlDemData);
				prpCardDuty->Team = ogBasicData.GetTeamForShift(ogShiftData.GetShiftByUrno(prpCardDuty->Ustf));

				RUDDATA *prlRudData = ogRudData.GetRudByUrno(rlDemData.Urud);
				if (prlRudData != NULL)
				{
					SERDATA *prlSerData = ogSerData.GetSerByUrno(prlRudData->Ughs);
					if (prlSerData != NULL)
					{
						prpCardDuty->GHS_Urno = prlSerData->Urno;
						prpCardDuty->GHS_Lknm.Format("%d. %s", i+1, prlSerData->Snam);
					}
				}
				else
				{
					AZADATA *prlAzaData = ogAzaData.GetAzaByUrno(rlDemData.Udgr);
					if (prlAzaData != NULL)
					{
						if (*rlDemData.Dety == P05RES_DEMAND)
						{
							prpCardDuty->GHS_Urno = rlDemData.Udgr;
							prpCardDuty->GHS_Lknm.Format("%d. %s", i+1, prlAzaData->Seco);
						}
						else if(*rlDemData.Dety == CAS_DEMAND)
						{
						}
						else if(*rlDemData.Dety == MANUAL_THIRDPARTY_DEMAND)
						{
							prpCardDuty->GHS_Urno = rlDemData.Udgr;
							prpCardDuty->GHS_Lknm.Format("%d. %s", i+1, prlAzaData->Seco);
						}
					}
				}
						

				if (strcmp(rlJobData.Jtco,JOBEQUIPMENT) == 0)
				{
					EQUDATA *prlEqu = NULL;
					if((prlEqu = ogEquData.GetEquByUrno(rlJobData.Uequ)) != NULL)
					{
						prpCardDuty->DSR_Esnm = prlEqu->Enam + CString(".*");
					}
				}
				else
				{
					EMPDATA *prlEmp = ogEmpData.GetEmpByUrno(rlJobData.Ustf);
					if (prlEmp != NULL)
					{
						prpCardDuty->DSR_Esnm = ogEmpData.GetEmpName(prlEmp);
						JOBDATA *prlPJ = ogJobData.GetJobByUrno(rlJobData.Jour);
						if(prlPJ != NULL)
							prpCardDuty->OpssPmRemark = CString(prlPJ->Text);
					}
				}

				if (prpCardDuty->DSR_Esnm.GetLength() == 0) 
				{
					prpCardDuty->DSR_Esnm = "...";
				}

				if (prpCardDuty->GHS_Lknm.GetLength() == 0) 
				{
					prpCardDuty->GHS_Lknm.Format("%d. %s", j+1, "...");
				}
			}
		}

	}		


	int ilJobsCount = ropJobsWithoutDemands.GetSize();
	for (int j = 0; j < ropJobsWithoutDemands.GetSize(); j++)
	{
		JOBDATA rlJobData = ropJobsWithoutDemands[j];

		CARDDUTYDATA *prpCardDuty = new CARDDUTYDATA;
		prpCard->CardDutyData.Add(prpCardDuty);
		prpCard->imGhdLines++;
		prpCardDuty->Ujob = rlJobData.Urno;
		prpCardDuty->Ustf = rlJobData.Ustf;
		prpCardDuty->Acfr = rlJobData.Acfr;
		prpCardDuty->Acto = rlJobData.Acto;

		prpCardDuty->GHS_Urno = 0L;
		prpCardDuty->GHS_Lknm.Format("%d. ...", i+1);

		if (strcmp(rlJobData.Jtco,JOBEQUIPMENT) == 0)
		{
			EQUDATA *prlEqu = NULL;
			if((prlEqu = ogEquData.GetEquByUrno(rlJobData.Uequ)) != NULL)
			{
				prpCardDuty->DSR_Esnm = prlEqu->Enam + CString(".*");
			}
		}
		else
		{
			EMPDATA *prlEmp = ogEmpData.GetEmpByUrno(rlJobData.Ustf);
			if (prlEmp != NULL)
			{
				prpCardDuty->DSR_Esnm = ogEmpData.GetEmpName(prlEmp);
				JOBDATA *prlPJ = ogJobData.GetJobByUrno(rlJobData.Jour);
				if(prlPJ != NULL)
					prpCardDuty->OpssPmRemark = CString(prlPJ->Text);
			}
		}

		if (prpCardDuty->DSR_Esnm.GetLength() == 0) 
		{
			prpCardDuty->DSR_Esnm = "...";
		}

		if (prpCardDuty->GHS_Lknm.GetLength() == 0) 
		{
			prpCardDuty->GHS_Lknm.Format("%d. %s", j+1, "...");
		}
	}

	//Telex-Daten
	CString olTelex;
	char clWhere[200];
	CCSPtrArray<TLXDATA> olTlxData;
	CString olAlc3 = (prpArrivalFlight==NULL) ? "" : prpArrivalFlight->Alc3;
	CString olFlns = (prpArrivalFlight==NULL) ? "" : prpArrivalFlight->Flns;
	if(olAlc3.GetLength() == 0)
	{
		olAlc3 = (prpArrivalFlight==NULL) ? "" : prpArrivalFlight->Alc2;
	}
	if(olFlns.GetLength() == 0)
	{
		olFlns = " ";		
	}

	ogBasicData.omTelexTypesToLoad = ogCfgData.GetValueByCtypAndCkey(TELEXTYPE_KEY, TELEXTYPE_TEXT, ogBasicData.omTelexTypes); // default is all telexes
	if(prpArrivalFlight != NULL && !ogBasicData.omTelexTypesToLoad.IsEmpty())
	{
//		if(ogBasicData.omTelexTypesToLoad.IsEmpty())
//		{
//			sprintf(clWhere, "WHERE FLNU=%ld", prpArrivalFlight->Urno);
//			sprintf(clWhere,"WHERE ALC3='%s' AND FLTN='%s' AND FLNS='%s' AND CDAT>='%s' AND CDAT<='%s'",
//					olAlc3,	prpArrivalFlight->Fltn,	olFlns,	prpArrivalFlight->Tifa.Format("%Y%m%d000000"),
//					prpArrivalFlight->Tifa.Format("%Y%m%d235900"));
//		}
//		else
//		{
			sprintf(clWhere,"WHERE FLNU=%ld AND TTYP IN (%s)", prpArrivalFlight->Urno, FormatTelexTypeString(ogBasicData.omTelexTypesToLoad));
			//sprintf(clWhere,"WHERE (TTYP='LDM' OR TTYP='CPM') AND ALC3='%s' AND FLTN='%s' AND FLNS='%s' AND CDAT>='%s' AND CDAT<='%s'",
//			sprintf(clWhere,"WHERE TTYP IN (%s) AND ALC3='%s' AND FLTN='%s' AND FLNS='%s' AND CDAT>='%s' AND CDAT<='%s'",
//					FormatTelexTypeString(ogBasicData.omTelexTypesToLoad), olAlc3, prpArrivalFlight->Fltn,
//					olFlns,	prpArrivalFlight->Tifa.Format("%Y%m%d000000"), prpArrivalFlight->Tifa.Format("%Y%m%d235900"));
//		}
		
		if(ogTlxData.ReadSpecialData(&olTlxData,clWhere,"TXT1,TXT2,TTYP,TIME",false) == true)
		{
			CheckForLastTelex(olTlxData);

			int ilNumTlx = olTlxData.GetSize();
			for(i = 0; i < ilNumTlx; i++)
			{
				TLXDATA *polTlxRec = &olTlxData[i];
				ogTlxData.TranslateText(polTlxRec,olTelex,true);
				olTelex.TrimRight(" \n\r");

				//CString olTelextMessageBegin = GetString(IDS_TELEXMESSAGEBEGIN);
				CString olTelextMessageBegin = "\n.";
				int ilTelexStartPos = olTelex.Find(olTelextMessageBegin);
				int ilTelexLen = olTelex.GetLength() - ilTelexStartPos;
				if(ilTelexStartPos != -1 && ilTelexLen > 0)
				{
					prpCard->TLX_Text += olTelex.Mid(ilTelexStartPos+1, ilTelexLen);
				}
				else
				{
					prpCard->TLX_Text += olTelex;
				}
				if((i+1)<olTlxData.GetSize())
				{
					prpCard->TLX_Text += CString("\n================================================\n");
				}
			}
			if(prpCard->TLX_Text.GetLength()>0)
			{
				prpCard->imTelexLines++;
				int ilLength = prpCard->TLX_Text.GetLength();
				for(int ilPos = 0; ilPos<ilLength-1; ilPos++)
				{
					if(prpCard->TLX_Text[ilPos] == '\n' || prpCard->TLX_Text[ilPos] == 28)
					{
						prpCard->imTelexLines++;
					}
				}
			}
		}
		olTlxData.DeleteAll();
	}

	if(prpCard->imRemaLines == 0)
	{
		prpCard->imRemaLines = 1;
		prpCard->GHP_Rema = CString(GetString(IDS_PRINTCARD02));
	}

	if(prpCard->imTelexLines == 0)
	{
		prpCard->imTelexLines = 1;
		prpCard->TLX_Text = CString(GetString(IDS_PRINTCARD02));
	}
	
	imMaxPage = int (ceil((double)(prpCard->imGhdLines+prpCard->imRemaLines+prpCard->imTelexLines+6)/(double)imMaxLines));
	return true;
}

// Format from "MVT,CPM" to "'MVT','CPM'"
CString PrintCard::FormatTelexTypeString(CString opTelexTypeString)
{
	CString olFormattedStr;
	olFormattedStr = "'";

	int ilLen = opTelexTypeString.GetLength();
	for(int i = 0; i < ilLen; i++)
	{
		if(opTelexTypeString[i] == ',')
		{
			olFormattedStr += "','";
		}
		else
		{
			olFormattedStr += opTelexTypeString[i];
		}
	}

	olFormattedStr += "'";

	return olFormattedStr;
}

void PrintCard::CheckForLastTelex(CCSPtrArray <TLXDATA> &ropTlxData)
{
	CMapStringToPtr olTelexTypeMap;
	TLXDATA *prlTlx1 = NULL, *prlTlx2 = NULL;

	if(!ogBasicData.omTelexTypesPrintOnlyLast.IsEmpty())
	{
		int ilNumTlxs = ropTlxData.GetSize();
		for(int i = (ilNumTlxs-1); i >= 0; i--)
		{
			prlTlx1 = &ropTlxData[i];
			if(ogBasicData.omTelexTypesPrintOnlyLast.Find(prlTlx1->TTYP) != -1)
			{
				// has been specified in ceda.ini that only the last telexes of the telex type
				// held in ogBasicData.omTelexTypesPrintOnlyLast should be printed
				if(!olTelexTypeMap.Lookup(prlTlx1->TTYP, (void *&) prlTlx2))
				{
					olTelexTypeMap.SetAt(prlTlx1->TTYP, (void *&) prlTlx1);
					ropTlxData.RemoveAt(i);
				}
				else if(prlTlx1->TIME > prlTlx2->TIME)
				{
					olTelexTypeMap.SetAt(prlTlx1->TTYP, (void *&) prlTlx1);
					ropTlxData.RemoveAt(i);
					delete prlTlx2;
				}
				else
				{
					ropTlxData.DeleteAt(i);
				}
			}
		}
	}

	CString olTlxType;
	for(POSITION rlPos = olTelexTypeMap.GetStartPosition(); rlPos != NULL;)
	{
		olTelexTypeMap.GetNextAssoc(rlPos, olTlxType, (void *& ) prlTlx1);
		ropTlxData.Add(prlTlx1);
	}
}

void PrintCard::PrintHeader()
{
	imActualPage++;
	if (imActualPage == imPreviewPage || imPreviewPage == 0)
	{
		pomCdc->StartPage();
	}
	imActualLine = 0;


	CSize olSize;
	CString olTmpText;
	int ilPos = imFirstPos - 10;
	int ilLineHight = imLineHeight;

	int ilHorzRes = pomCdc->GetDeviceCaps(HORZRES);
	int ilVertRes = pomCdc->GetDeviceCaps(VERTRES);
	int ilVerLinePosX = ((ilHorzRes-MMX(20))+(MMX(imLeftOffset)))/2;


	
	if (imActualPage == imPreviewPage || imPreviewPage == 0)
	{
		pomCdc->SelectObject(&omMediumPen);
	}

	int ilCol1 = MMX(14);	
	int ilCol2 = MMX(606);	
	int ilCol3 = MMX(1330);	
	int ilCol4 = MMX(350);	
	int ilCol5 = MMX(686);	     
	int ilCol6 = ilVerLinePosX+ilCol1;	
	int ilCol7 = ilVerLinePosX+ilCol4;	
	int ilCol8 = ilVerLinePosX+ilCol5;
	int ilEtdOffset = MMX(80);
		
	int ilPosUp = 4;


	if (imActualPage == imPreviewPage || imPreviewPage == 0)
	{
		if (hmBitmap)	// user defined logo bitmap available ?
		{
			HBITMAP  hlOldBitmap;
			HPALETTE hlOldPalette;

			BITMAP rlBitmap;
			HDC	 hlMemDC;

			::GetObject(hmBitmap,sizeof(BITMAP),&rlBitmap);	
			hlMemDC = ::CreateCompatibleDC(pomCdc->m_hDC);
			hlOldBitmap = (HBITMAP)::SelectObject(hlMemDC,hmBitmap);
			hlOldPalette= ::SelectPalette(pomCdc->m_hDC,hmPalette,FALSE);
			::RealizePalette(pomCdc->m_hDC);
			CSize rlSize(rlBitmap.bmWidth,rlBitmap.bmHeight);
			pomCdc->DPtoLP(&rlSize);
			BOOL bResult = ::StretchBlt(pomCdc->m_hDC,MMX(imLeftOffset+1000),MMY(ilPos-175),MMX(750),MMY(180),hlMemDC,0,0,rlSize.cx,rlSize.cy,SRCCOPY);
			::SelectObject(hlMemDC,hlOldBitmap);
			::SelectPalette(pomCdc->m_hDC,hlOldPalette,FALSE);
		}
		else
		{
			pomCdc->StretchBlt(MMX(imLeftOffset+1000),MMY(ilPos-175),MMX(750),MMY(180),&omMemDc,0,0,519,519, SRCCOPY);
		}
		
		// Print Hader
		//*************
		// Print Frame

		if (imActualPage == imPreviewPage || imPreviewPage == 0)
		{
			pomCdc->MoveTo(MMX(imLeftOffset),MMY(ilPos));
			pomCdc->LineTo(ilHorzRes-MMX(20),MMY(ilPos));
			pomCdc->LineTo(ilHorzRes-MMX(20),ilVertRes-MMY(50));
			pomCdc->LineTo(MMX(imLeftOffset),ilVertRes-MMY(50));
			pomCdc->LineTo(MMX(imLeftOffset),MMY(ilPos));
		}

		// Header-positions
		// 1     2               3
		// 1                6           
		// 1    4     5     6    7     8
		// 1    4           6    7      
		// 1    4           6    7      
		// 1    4           6    7      
		// 1    4           6    7      


		// Print: Tablename
		pomCdc->SelectObject(&omArial_Bold_18);
		pomCdc->TextOut(MMX(imLeftOffset+ilCol1),MMY(ilPos-80), omCardName, strlen(omCardName));

		// Print Flight Data
		//*******************

		CardData *polCard = &omCardData[imCardNo];

		//############## Beschreibung ##########################################
		pomCdc->SelectObject(&omArial_Bold_10);
		//******* LINE 1 **
		ilPos = imFirstPos+10;
		// Print: Registration
		olTmpText = GetString(IDS_PRINTCARD03);
		pomCdc->TextOut(MMX(imLeftOffset)+ilCol1,MMY(ilPos-ilPosUp), olTmpText, strlen(olTmpText));
		// Print: ACT
		olTmpText = GetString(IDS_PRINTCARD04);
		pomCdc->TextOut(MMX(imLeftOffset)+ilCol2,MMY(ilPos-ilPosUp), olTmpText, strlen(olTmpText));
		// Print: Date
		olTmpText = GetString(IDS_PRINTCARD05);
		pomCdc->TextOut(MMX(imLeftOffset)+ilCol3,MMY(ilPos-ilPosUp), olTmpText, strlen(olTmpText));
		//******* LINE 2 **
		ilPos += ilLineHight;
		// Print: FlugnummerA
		olTmpText = GetString(IDS_PRINTCARD06);
		pomCdc->TextOut(MMX(imLeftOffset)+ilCol1,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: FlugnummerD
		pomCdc->TextOut(ilCol6,MMY(ilPos), olTmpText, strlen(olTmpText));
		//******* LINE 3 **
		ilPos += ilLineHight;
		// Print: Orig
		olTmpText = GetString(IDS_PRINTCARD07);
		pomCdc->TextOut(MMX(imLeftOffset)+ilCol1,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: ViaA
		olTmpText = GetString(IDS_PRINTCARD08);
		pomCdc->TextOut(MMX(imLeftOffset)+ilCol4,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: ViaD
		pomCdc->TextOut(ilCol7,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: VerkehrsartA
		olTmpText = GetString(IDS_PRINTCARD09);
		pomCdc->TextOut(MMX(imLeftOffset)+ilCol5,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: VerkehrsartD
		pomCdc->TextOut(ilCol8,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: Dest
		olTmpText = GetString(IDS_PRINTCARD10);
		olTmpText = "Dest.:";
		pomCdc->TextOut(ilCol6,MMY(ilPos), olTmpText, strlen(olTmpText));
		//******* LINE 4 **
		ilPos += ilLineHight;
		// Print: STOA
		olTmpText = GetString(IDS_PRINTCARD11);
		pomCdc->TextOut(MMX(imLeftOffset)+ilCol1,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: ETOA
		olTmpText = GetString(IDS_PRINTCARD12);
		pomCdc->TextOut(MMX(imLeftOffset)+ilCol4,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: STOD
		olTmpText = GetString(IDS_PRINTCARD13);
		pomCdc->TextOut(ilCol6,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: ETOD
		olTmpText = GetString(IDS_PRINTCARD14);
		pomCdc->TextOut(ilCol7-ilEtdOffset,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: Check-in counters
		olTmpText = GetString(IDS_PRINTCARD_CHECKIN);
		pomCdc->TextOut(ilCol8-(ilEtdOffset*2),MMY(ilPos), olTmpText, strlen(olTmpText));
		//******* LINE 5 **
		ilPos += ilLineHight;
		// Print: POSA
		olTmpText = GetString(IDS_PRINTCARD15);
		pomCdc->TextOut(MMX(imLeftOffset)+ilCol1,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: POSD
		pomCdc->TextOut(ilCol6,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: GateA
		olTmpText = GetString(IDS_PRINTCARD16);
		pomCdc->TextOut(MMX(imLeftOffset)+ilCol4,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: GateD
		pomCdc->TextOut(ilCol7,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: Blt1A
		olTmpText = GetString(IDS_PRINTCARD17);
		pomCdc->TextOut(MMX(imLeftOffset)+ilCol5,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: Blt1D
		pomCdc->TextOut(ilCol8,MMY(ilPos), olTmpText, strlen(olTmpText));
		//******* LINE 6 **
		ilPos += ilLineHight;
		// Print: PaxA
		olTmpText = GetString(IDS_PRINTCARD18);
		pomCdc->TextOut(MMX(imLeftOffset)+ilCol1,MMY(ilPos+ilPosUp), olTmpText, strlen(olTmpText));
		// Print: PaxD
		pomCdc->TextOut(ilCol6,MMY(ilPos+ilPosUp), olTmpText, strlen(olTmpText));
		// Print: CargoA
		olTmpText = GetString(IDS_PRINTCARD19);
		pomCdc->TextOut(MMX(imLeftOffset)+ilCol4,MMY(ilPos+ilPosUp), olTmpText, strlen(olTmpText));
		// Print: CargoD
		pomCdc->TextOut(ilCol7,MMY(ilPos+ilPosUp), olTmpText, strlen(olTmpText));
		//******* LINE 7 **
		ilPos += ilLineHight;
		// Print: BaggageA
		olTmpText = GetString(IDS_PRINTCARD20);
		pomCdc->TextOut(MMX(imLeftOffset)+ilCol1,MMY(ilPos+ilPosUp), olTmpText, strlen(olTmpText));
		// Print: BaggageD
		pomCdc->TextOut(ilCol6,MMY(ilPos+ilPosUp), olTmpText, strlen(olTmpText));
		// Print: MailA
		olTmpText = GetString(IDS_PRINTCARD21);
		pomCdc->TextOut(MMX(imLeftOffset)+ilCol4,MMY(ilPos+ilPosUp), olTmpText, strlen(olTmpText));
		// Print: MailD
		pomCdc->TextOut(ilCol7,MMY(ilPos+ilPosUp), olTmpText, strlen(olTmpText));

		//############## Werte ########################################################
		pomCdc->SelectObject(&omArial_Regular_10);
		//******* LINE 1 **
		ilPos = imFirstPos+10;
		int ilRight = 130;
		int ilRight2 = 160;
		// Print: Registration
		olTmpText = polCard->AFT_Regn;
		pomCdc->TextOut(MMX(imLeftOffset+150)+ilCol1,MMY(ilPos-ilPosUp), olTmpText, strlen(olTmpText));
		// Print: ACT
		if(polCard->AFT_Act3.GetLength() > 0)
			olTmpText = polCard->AFT_Act3;
		else
			olTmpText = polCard->AFT_Act5;
		pomCdc->TextOut(MMX(imLeftOffset+100)+ilCol2,MMY(ilPos-ilPosUp), olTmpText, strlen(olTmpText));
		// Print: Date
		if(polCard->AFT_Tifa == TIMENULL)
		{
			olTmpText = CString(polCard->AFT_Tifd.Format("%d.%m.%Y"));
		}
		else
		{
			olTmpText = CString(polCard->AFT_Tifa.Format("%d.%m.%Y"));
		}
		pomCdc->TextOut(MMX(imLeftOffset+160)+ilCol3,MMY(ilPos-ilPosUp), olTmpText, strlen(olTmpText));
		
		int ilVerLinePosY = ilPos+ilLineHight-7;
		pomCdc->SelectObject(&omThinPen);
		pomCdc->MoveTo(MMX(imLeftOffset),MMY(ilVerLinePosY));
		pomCdc->LineTo(ilHorzRes-MMX(20),MMY(ilVerLinePosY));
		pomCdc->SelectObject(&omArial_Regular_10);
			//******* LINE 2 **
		ilPos += ilLineHight;
		// Print: FlugnummerA
		olTmpText = polCard->AFT_FlnoA;
		pomCdc->TextOut(MMX(imLeftOffset+ilRight2)+ilCol1,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: FlugnummerD
		olTmpText = polCard->AFT_FlnoD;
		pomCdc->TextOut(MMX(ilRight2)+ilCol6,MMY(ilPos), olTmpText, strlen(olTmpText));
		//******* LINE 3 **
		ilPos += ilLineHight;
		// Print: Orig
		olTmpText = polCard->AFT_Org3;
		pomCdc->TextOut(MMX(imLeftOffset+ilRight)+ilCol1,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: Via
		olTmpText = polCard->AFT_Via3A;
		pomCdc->TextOut(MMX(imLeftOffset+ilRight)+ilCol4,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: VerkehrsartA
		olTmpText = polCard->AFT_TtypA;
		pomCdc->TextOut(MMX(imLeftOffset+90)+ilCol5,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: Dest
		olTmpText = polCard->AFT_Des3;
		pomCdc->TextOut(MMX(ilRight)+ilCol6,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: Via
		olTmpText = polCard->AFT_Via3D;
		pomCdc->TextOut(MMX(ilRight)+ilCol7,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: VerkehrsartD
		olTmpText = polCard->AFT_TtypD;
		pomCdc->TextOut(MMX(90)+ilCol8,MMY(ilPos), olTmpText, strlen(olTmpText));
		//******* LINE 4 **
		ilPos += ilLineHight;
		// Print: STOA
		olTmpText = CString(polCard->AFT_Stoa.Format("%H:%M"));
		pomCdc->TextOut(MMX(imLeftOffset+ilRight)+ilCol1,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: ETOA
		olTmpText = (polCard->AFT_Etoa != TIMENULL) ? CString(polCard->AFT_Etoa.Format("%H:%M")) : "";
		pomCdc->TextOut(MMX(imLeftOffset+ilRight)+ilCol4,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: STOD
		olTmpText = CString(polCard->AFT_Stod.Format("%H:%M"));
		pomCdc->TextOut(MMX(ilRight)+ilCol6-15,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: ETOD
		olTmpText = (polCard->AFT_Etod != TIMENULL) ? CString(polCard->AFT_Etod.Format("%H:%M")) : "";
		pomCdc->TextOut(MMX(ilRight)+ilCol7-ilEtdOffset,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: Check-in
		olTmpText = polCard->AFT_CIC;
		pomCdc->TextOut(MMX(ilRight+50)+ilCol8-(ilEtdOffset*2),MMY(ilPos), olTmpText, strlen(olTmpText));
		//******* LINE 5 **
		ilPos += ilLineHight;
		// Print: POSA
		olTmpText = polCard->AFT_Psta;
		pomCdc->TextOut(MMX(imLeftOffset+ilRight)+ilCol1,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: GateA
		olTmpText = polCard->AFT_Gta1;
		pomCdc->TextOut(MMX(imLeftOffset+ilRight)+ilCol4,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print Blt1A
		olTmpText = polCard->AFT_Blt1A;
		pomCdc->TextOut(MMX(imLeftOffset+90)+ilCol5,MMY(ilPos), olTmpText, strlen(olTmpText));
		//pomCdc->TextOut(MMX(imLeftOffset+ilRight)+ilCol5,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: POSD
		olTmpText = polCard->AFT_Pstd;
		pomCdc->TextOut(MMX(ilRight)+ilCol6,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print: GateD
		olTmpText = polCard->AFT_Gtd1;
		pomCdc->TextOut(MMX(ilRight)+ilCol7,MMY(ilPos), olTmpText, strlen(olTmpText));
		// Print Blt1D
		olTmpText = polCard->AFT_Blt1D;
		pomCdc->TextOut(MMX(90)+ilCol8,MMY(ilPos), olTmpText, strlen(olTmpText));
		//pomCdc->TextOut(MMX(imLeftOffset+ilRight)+ilCol8,MMY(ilPos), olTmpText, strlen(olTmpText));
		pomCdc->SelectObject(&omThinPen);
		pomCdc->MoveTo(MMX(imLeftOffset),MMY(ilPos+ilLineHight-7));
		pomCdc->LineTo(ilHorzRes-MMX(20),MMY(ilPos+ilLineHight-7));
		pomCdc->SelectObject(&omArial_Regular_10);
			
		//******* LINE 6 **
		ilPos += ilLineHight;
		// Print: PaxA
		olTmpText.Format("%d",polCard->AFT_PaxtA);
		pomCdc->TextOut(MMX(imLeftOffset+ilRight)+ilCol1,MMY(ilPos+ilPosUp), olTmpText, strlen(olTmpText));
		// Print: CargoA
		olTmpText.Format("%d t",polCard->AFT_CgotA);
		pomCdc->TextOut(MMX(imLeftOffset+ilRight)+ilCol4,MMY(ilPos+ilPosUp), olTmpText, strlen(olTmpText));
		// Print: PaxD
		olTmpText.Format("%d",polCard->AFT_PaxtD);
		pomCdc->TextOut(MMX(ilRight)+ilCol6,MMY(ilPos+ilPosUp), olTmpText, strlen(olTmpText));
		// Print: CargoD
		olTmpText.Format("%d t",polCard->AFT_CgotD);
		pomCdc->TextOut(MMX(ilRight)+ilCol7,MMY(ilPos+ilPosUp), olTmpText, strlen(olTmpText));
		//******* LINE 7 **
		ilPos += ilLineHight;
		// Print: BaggageA
		olTmpText.Format("%s/%d St.",polCard->AFT_Blt1A, polCard->AFT_BagnA);
		pomCdc->TextOut(MMX(imLeftOffset+ilRight)+ilCol1,MMY(ilPos+ilPosUp), olTmpText, strlen(olTmpText));
		// Print: MailA
		olTmpText.Format("%d St.",polCard->AFT_MailA);
		pomCdc->TextOut(MMX(imLeftOffset+ilRight)+ilCol4,MMY(ilPos+ilPosUp), olTmpText, strlen(olTmpText));
		// Print: BaggageD
		olTmpText.Format("%s/%d St.",polCard->AFT_Blt1D,polCard->AFT_BagnD);
		pomCdc->TextOut(MMX(ilRight)+ilCol6,MMY(ilPos+ilPosUp), olTmpText, strlen(olTmpText));
		// Print: MailD
		olTmpText.Format("%d St.",polCard->AFT_MailD);
		pomCdc->TextOut(MMX(ilRight)+ilCol7,MMY(ilPos+ilPosUp), olTmpText, strlen(olTmpText));

		ilPos += ilLineHight;

		pomCdc->SelectObject(&omMediumPen);
		pomCdc->MoveTo(MMX(imLeftOffset),MMY(ilPos));
		pomCdc->LineTo(ilHorzRes-MMX(20),MMY(ilPos));

		pomCdc->SelectObject(&omThinPen);
		pomCdc->MoveTo(ilVerLinePosX,MMY(ilVerLinePosY));
		pomCdc->LineTo(ilVerLinePosX,MMY(ilPos));

		imActualPos = ilPos - ilLineHight;
		PrintFooter();
	}
}

//-----------------------------------------------------------------------------------------------

void PrintCard::PrintTable()
{
	CardData *polCard = &omCardData[imCardNo];

	CSize olSize;
	CString olTmpText;
	imActualPos += 150;
	int ilPos = imActualPos - 10;
	int ilLineHight = imLineHeight;

	int ilHorzRes = pomCdc->GetDeviceCaps(HORZRES);
	int ilVertRes = pomCdc->GetDeviceCaps(VERTRES);


	// Print Tablehader
	//******************
	imActualLine += 2;

	int ilCol1, ilCol2, ilCol3;
	if(imOrientation == PRINTCARD_LANDSCAPE)
	{
		ilCol1 = 14;
		ilCol2 = 1400;
		ilCol3 = 2400;	
	}
	else
	{
		ilCol1 = 14;
		ilCol2 = 920;
		ilCol3 = 1700;	
	}


	CFieldDataArray olFieldConfig;

	int ilCharWidth = 18;

	if (imActualPage == imPreviewPage || imPreviewPage == 0)
	{
		pomCdc->SelectObject(&omArial_Bold_10);
		// print title
		ogFieldConfig.ConfigureField(IDS_FIELDCONFIG_JOBCARD, olFieldConfig, "%s%s%s%s%s%s", "", "", "", "", "", "");
		if(olFieldConfig.GetSize() > 0)
		{
			int ilOffset = imLeftOffset + 14;
			for(int ilFC = 0; ilFC < olFieldConfig.GetSize(); ilFC++)
			{
				pomCdc->TextOut(MMX(ilOffset),MMY(ilPos-45), olFieldConfig[ilFC].Field, olFieldConfig[ilFC].Field.GetLength());
				ilOffset += (olFieldConfig[ilFC].NumChars * ilCharWidth);
			}
		}
		else
		{
			// Print: Leistung
			olTmpText = CString(GetString(IDS_PRINTCARD22));
			pomCdc->TextOut(MMX(imLeftOffset+ilCol1),MMY(ilPos-45), olTmpText, strlen(olTmpText));

			// Print: Mitarbeiter
			olTmpText = CString(GetString(IDS_PRINTCARD23));
			pomCdc->TextOut(MMX(imLeftOffset+ilCol2),MMY(ilPos-45), olTmpText, strlen(olTmpText));

			// Print: Einsatzzeit
			olTmpText = CString(GetString(IDS_PRINTCARD24));
			pomCdc->TextOut(MMX(imLeftOffset+ilCol3),MMY(ilPos-45), olTmpText, strlen(olTmpText));
		}

		pomCdc->SelectObject(&omThinPen);
		pomCdc->MoveTo(MMX(imLeftOffset),MMY(ilPos));
		pomCdc->LineTo(ilHorzRes-MMX(20),MMY(ilPos));
	}

	// Print Tablelines
	//******************
	CString olDutyTime;
	ilPos = imActualPos + 4;
	pomCdc->SelectObject(&omArial_Regular_8);
	int ilGhdsCount = polCard->CardDutyData.GetSize();
	for(int i=0; i<ilGhdsCount; i++)
	{
		if(imActualLine >= imMaxLines)
		{
			if (imActualPage == imPreviewPage || imPreviewPage == 0)
			{
				pomCdc->EndPage();
			}
			PrintHeader();
			ilPos = imActualPos + 54;
			pomCdc->SelectObject(&omArial_Regular_8);

		}
		imActualLine++;
		olFieldConfig.RemoveAll();
		CARDDUTYDATA *polCardDuty = &polCard->CardDutyData[i];
		olDutyTime.Format("%s-%s",CString(polCardDuty->Acfr.Format("%H%M")), CString(polCardDuty->Acto.Format("%H%M")));
		ogFieldConfig.ConfigureField(IDS_FIELDCONFIG_JOBCARD, olFieldConfig, "%s%s%s%s%s%s", polCardDuty->GHS_Lknm, 
										polCardDuty->Team, polCardDuty->DemandFunction, polCardDuty->DSR_Esnm, olDutyTime, 
										polCardDuty->OpssPmRemark);

		if(olFieldConfig.GetSize() > 0)
		{
			int ilOffset = imLeftOffset + 14;
			if (imActualPage == imPreviewPage || imPreviewPage == 0)
			{
				for(int ilFC = 0; ilFC < olFieldConfig.GetSize(); ilFC++)
				{
					pomCdc->TextOut(MMX(ilOffset),MMY(ilPos), olFieldConfig[ilFC].Text, olFieldConfig[ilFC].Text.GetLength());
					ilOffset += (olFieldConfig[ilFC].NumChars * ilCharWidth);
				}
			}
		}
		else
		{
			if (imActualPage == imPreviewPage || imPreviewPage == 0)
			{
				// not configured so print the default columns
				// Print: Leistung
				olTmpText.Format("%s",polCardDuty->GHS_Lknm);
				pomCdc->TextOut(MMX(imLeftOffset+ilCol1),MMY(ilPos), olTmpText, strlen(olTmpText));

				// Print: Mitarbeiter
				olTmpText.Format("%s",polCardDuty->DSR_Esnm);
				pomCdc->TextOut(MMX(imLeftOffset+ilCol2),MMY(ilPos), olTmpText, strlen(olTmpText));

				// Print: Einsatzzeit von-bis
				olTmpText.Format("%s-%s",CString(polCardDuty->Acfr.Format("%H:%M")), CString(polCardDuty->Acto.Format("%H:%M")));
				pomCdc->TextOut(MMX(imLeftOffset+ilCol3),MMY(ilPos), olTmpText, strlen(olTmpText));
			}
		}
		ilPos += ilLineHight;
	}
	imActualPos = ilPos-ilLineHight;
}

//-----------------------------------------------------------------------------------------------

void PrintCard::PrintRemark()
{
	CSize olSize;
	CString olTmpText;
	imActualPos += 150;
	int ilPos = imActualPos - 10;
	int ilLineHight = imLineHeight;

	int ilHorzRes = pomCdc->GetDeviceCaps(HORZRES);
	int ilVertRes = pomCdc->GetDeviceCaps(VERTRES);

	// Print Remarkhader
	//******************
	if(imActualLine+3 >= imMaxLines)
	{
		if (imActualPage == imPreviewPage || imPreviewPage == 0)
		{
			pomCdc->EndPage();
		}
		PrintHeader();
		imActualPos += 150;
		ilPos = imActualPos - 10;
	}
	imActualLine += 2;

	int ilCol1 = 14;	// 1

	if (imActualPage == imPreviewPage || imPreviewPage == 0)
	{
		pomCdc->SelectObject(&omArial_Bold_11);

		olTmpText = CString(GetString(IDS_PRINTCARD25));
		pomCdc->TextOut(MMX(imLeftOffset+ilCol1),MMY(ilPos-45), olTmpText, strlen(olTmpText));

		pomCdc->SelectObject(&omThinPen);
		pomCdc->MoveTo(MMX(imLeftOffset),MMY(ilPos));
		pomCdc->LineTo(ilHorzRes-MMX(20),MMY(ilPos));
	}


	// Print Remarklines
	//******************
	ilPos = imActualPos + 10;
	CardData *polCard = &omCardData[imCardNo];
	pomCdc->SelectObject(&omArial_Regular_8);

	int ilFrom = 0;
	int ilTo = 89;
	while(polCard->GHP_Rema.GetLength() > ilFrom)
	{
		if(imActualLine >= imMaxLines)
		{
			if (imActualPage == imPreviewPage || imPreviewPage == 0)
			{
				pomCdc->EndPage();
			}
			PrintHeader();
			ilPos = imActualPos + 95;
			pomCdc->SelectObject(&omArial_Regular_8);
		}
		if (imActualPage == imPreviewPage || imPreviewPage == 0)
		{
			olTmpText = polCard->GHP_Rema.Mid(ilFrom,ilTo);
			pomCdc->TextOut(MMX(imLeftOffset+ilCol1),MMY(ilPos), olTmpText, strlen(olTmpText));
		}
		imActualLine++;
		ilPos += ilLineHight;
		ilFrom += ilTo;
	}
	imActualPos = ilPos-ilLineHight;
}

//-----------------------------------------------------------------------------------------------

void PrintCard::PrintTelex()
{
	CSize olSize;
	CString olTmpText;
	imActualPos += 150;
	int ilPos = imActualPos - 10;
	int ilLineHight = imLineHeight;

	int ilHorzRes = pomCdc->GetDeviceCaps(HORZRES);
	int ilVertRes = pomCdc->GetDeviceCaps(VERTRES);

	// Print Telexheader
	//******************
	if(imActualLine+3 >= imMaxLines)
	{
		if (imActualPage == imPreviewPage || imPreviewPage == 0)
		{
			pomCdc->EndPage();
		}
		PrintHeader();
		imActualPos += 150;
		ilPos = imActualPos - 10;
	}
	imActualLine += 2;
	int ilCol1 = 14;	// 1

	if (imActualPage == imPreviewPage || imPreviewPage == 0)
	{
		pomCdc->SelectObject(&omArial_Bold_11);

		olTmpText = CString(GetString(IDS_PRINTCARD26));
		pomCdc->TextOut(MMX(imLeftOffset+ilCol1),MMY(ilPos-45), olTmpText, strlen(olTmpText));

		pomCdc->SelectObject(&omThinPen);
		pomCdc->MoveTo(MMX(imLeftOffset),MMY(ilPos));
		pomCdc->LineTo(ilHorzRes-MMX(20),MMY(ilPos));
	}

	// Print Telex
	//*************
	ilPos = imActualPos + 10;
	pomCdc->SelectObject(&omArial_Regular_8);
	CardData *polCard = &omCardData[imCardNo];
	if(polCard->TLX_Text.GetLength()>0)
	{
		int ilLength = polCard->TLX_Text.GetLength();
		int ilStrStartPos=0;
		int ilStrPos = 0;
		for(; ilStrPos<ilLength; ilStrPos++)
		{
			int ilNewLineCharCount = 0;
			if(polCard->TLX_Text.Mid(ilStrPos,2)== "\r\n")
			{
				ilNewLineCharCount = 2;
			}
			else if(polCard->TLX_Text[ilStrPos] == '\n')
			{
				ilNewLineCharCount = 1;
			}

			if(ilNewLineCharCount > 0)
			{
				if(imActualLine >= imMaxLines)
				{
					if (imActualPage == imPreviewPage || imPreviewPage == 0)
					{
						pomCdc->EndPage();
					}
					PrintHeader();
					ilPos = imActualPos + 95;
					pomCdc->SelectObject(&omArial_Regular_8);
				}
				if (imActualPage == imPreviewPage || imPreviewPage == 0)
				{
					olTmpText = polCard->TLX_Text.Mid(ilStrStartPos,ilStrPos-ilStrStartPos);
					pomCdc->TextOut(MMX(imLeftOffset+ilCol1),MMY(ilPos), olTmpText, strlen(olTmpText));
				}
				imActualLine++;
				ilPos += ilLineHight;
				ilStrStartPos = ilStrPos + ilNewLineCharCount;
			}
		}
		if(ilStrStartPos<ilLength)
		{
			if(imActualLine >= imMaxLines)
			{
				if (imActualPage == imPreviewPage || imPreviewPage == 0)
				{
					pomCdc->EndPage();
				}
				PrintHeader();
				ilPos = imActualPos + 95;
				pomCdc->SelectObject(&omArial_Regular_8);
			}
			if (imActualPage == imPreviewPage || imPreviewPage == 0)
			{
				olTmpText = polCard->TLX_Text.Mid(ilStrStartPos,ilLength-ilStrStartPos);
				pomCdc->TextOut(MMX(imLeftOffset+ilCol1),MMY(ilPos), olTmpText, strlen(olTmpText));
			}
			imActualLine++;
			ilPos += ilLineHight;
		}
	}

	imActualPos = ilPos-ilLineHight;
}

//-----------------------------------------------------------------------------------------------

void PrintCard::PrintFooter()
{
	CSize olSize;
	CString olTmpText;
	int ilHorzRes = pomCdc->GetDeviceCaps(HORZRES);
	int ilVertRes = pomCdc->GetDeviceCaps(VERTRES);

	if (imActualPage == imPreviewPage || imPreviewPage == 0)
	{
		pomCdc->SelectObject(&omArial_Regular_8);
		CardData *polCard = &omCardData[imCardNo];

		// Erstellt von
		olTmpText.Format(GetString(IDS_PRINTCARD27),ogUsername);
		pomCdc->TextOut(MMX(imLeftOffset+10),ilVertRes-MMY(40), olTmpText, strlen(olTmpText));

		// Druckdatum
		olTmpText = CString(GetString(IDS_PRINTCARD28)) + CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M"));
		olSize = pomCdc->GetTextExtent(olTmpText);
		pomCdc->TextOut((ilHorzRes/2)-(olSize.cx/2)+MMX(80),ilVertRes-MMY(40), olTmpText, strlen(olTmpText));
			
		// Seitenzahl ermitteln
		double dlPages = ceil((double)(polCard->imGhdLines+polCard->imRemaLines+polCard->imTelexLines+6)/(double)imMaxLines);
		// Seitenzahl
		olTmpText.Format(GetString(IDS_PRINTCARD29),imActualPage,dlPages);
		olSize = pomCdc->GetTextExtent(olTmpText);
		pomCdc->TextOut(ilHorzRes-(MMX(30)+olSize.cx),ilVertRes-MMY(40), olTmpText, strlen(olTmpText));
	}
}

//-----------------------------------------------------------------------------------------------

BOOL PrintCard::LoadBitmapFromBMPFile( LPTSTR szFileName, HBITMAP *phBitmap,HPALETTE *phPalette )
{
     BITMAP  bm;

     *phBitmap = NULL;
     *phPalette = NULL;

     // Use LoadImage() to get the image loaded into a DIBSection
     *phBitmap = (HBITMAP)LoadImage( NULL, szFileName, IMAGE_BITMAP, 0, 0,
                 LR_CREATEDIBSECTION | LR_LOADFROMFILE );
     if( *phBitmap == NULL )
       return FALSE;

     // Get the color depth of the DIBSection
     GetObject(*phBitmap, sizeof(BITMAP), &bm );
     // If the DIBSection is 256 color or less, it has a color table
     if( ( bm.bmBitsPixel * bm.bmPlanes ) <= 8 )
     {
       HDC           hMemDC;
       HBITMAP       hOldBitmap;
       RGBQUAD       rgb[256];
       LPLOGPALETTE  pLogPal;
       WORD          i;

       // Create a memory DC and select the DIBSection into it
       hMemDC = CreateCompatibleDC( NULL );
       hOldBitmap = (HBITMAP)SelectObject( hMemDC, *phBitmap );
       // Get the DIBSection's color table
       GetDIBColorTable( hMemDC, 0, 256, rgb );
       // Create a palette from the color table
       pLogPal = (LOGPALETTE*)malloc( sizeof(LOGPALETTE) + (256*sizeof(PALETTEENTRY)) );
       pLogPal->palVersion = 0x300;
       pLogPal->palNumEntries = 256;
       for(i=0;i<256;i++)
       {
         pLogPal->palPalEntry[i].peRed = rgb[i].rgbRed;
         pLogPal->palPalEntry[i].peGreen = rgb[i].rgbGreen;
         pLogPal->palPalEntry[i].peBlue = rgb[i].rgbBlue;
         pLogPal->palPalEntry[i].peFlags = 0;
       }
       *phPalette = CreatePalette( pLogPal );
       // Clean up
       free( pLogPal );
       SelectObject( hMemDC, hOldBitmap );
       DeleteDC( hMemDC );
     }
     else   // It has no color table, so use a halftone palette
     {
       HDC    hRefDC;

       hRefDC = GetDC( NULL );
       *phPalette = CreateHalftonePalette( hRefDC );
       ReleaseDC( NULL, hRefDC );
     }
     return TRUE;
}

//-----------------------------------------------------------------------------------------------



