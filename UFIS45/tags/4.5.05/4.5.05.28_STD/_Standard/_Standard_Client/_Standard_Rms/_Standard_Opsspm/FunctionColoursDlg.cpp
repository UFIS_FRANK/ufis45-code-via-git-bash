// FunctionColoursDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <BasicData.h>
#include <FunctionColoursDlg.h>
#include <FunctionColoursDetailDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFunctionColoursDlg dialog


CFunctionColoursDlg::CFunctionColoursDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFunctionColoursDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFunctionColoursDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CFunctionColoursDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFunctionColoursDlg)
	DDX_Control(pDX, IDC_DISPLAYDEFINEDONLY, m_DisplayDefinedOnlyCheckboxCtrl);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFunctionColoursDlg, CDialog)
	//{{AFX_MSG_MAP(CFunctionColoursDlg)
	ON_BN_CLICKED(IDC_DISPLAYDEFINEDONLY, OnDisplayDefinedOnlyClicked)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFunctionColoursDlg message handlers

BOOL CFunctionColoursDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(GetString(IDS_FUNCCOLOUR_TITLE));
	m_DisplayDefinedOnlyCheckboxCtrl.SetWindowText(GetString(IDS_FUNCCOLOUR_DEFONLY));

	pomTable = new CCSTable;
	pomTable->SetSelectMode(0);
    CRect olRect;
    GetClientRect(&olRect);
	olRect.left += 10; olRect.right -= 10; olRect.top += 40; olRect.bottom -= 60;
    pomTable->SetTableData(this, olRect.left, olRect.right, olRect.top, olRect.bottom);

	pomViewer = new FunctionColoursViewer();
	pomViewer->Attach(pomTable);
	pomViewer->ChangeViewTo();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

CFunctionColoursDlg::~CFunctionColoursDlg()
{
	pomViewer->Attach(NULL);
	delete pomViewer;
	delete pomTable;	
}

// view selected (or initializing)
void CFunctionColoursDlg::UpdateDisplay() 
{
	AfxGetApp()->DoWaitCursor(1);
	pomViewer->UpdateDisplay();
	AfxGetApp()->DoWaitCursor(-1);
}

void CFunctionColoursDlg::OnOK() 
{
	pomViewer->SaveLineData();
	
	CDialog::OnOK();
}

void CFunctionColoursDlg::OnDisplayDefinedOnlyClicked() 
{
	pomViewer->bmDisplayDefinedOnly = (m_DisplayDefinedOnlyCheckboxCtrl.GetCheck() == 1) ? true : false;
	UpdateDisplay();
}

LONG CFunctionColoursDlg::OnTableLButtonDblclk(UINT ipItem, LONG lpLParam)
{
	UINT ilNumLines = pomViewer->LineCount();
	if (ipItem >= 0 && ipItem < ilNumLines)
	{
		FUNCTIONCOLOURS_LINE *prlLine = pomViewer->GetLine(ipItem);
		if (prlLine != NULL)
		{
			CFunctionColoursDetailDlg olDlg;
			olDlg.SetData(prlLine->Fctc, prlLine->Fctn, prlLine->Enabled, prlLine->Colour);
			if(olDlg.DoModal() == IDOK)
			{
				olDlg.GetData(prlLine->Fctc, prlLine->Fctn, prlLine->Enabled, prlLine->Colour);
				pomViewer->UpdateTableLine(ipItem);
			}
		}
	}

	return 0L;
}
