// dselectf.cpp : implementation file
//

#include <stdafx.h>
#include <OpssPm.h>
#include <CCSGlobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <cviewer.h>
#include <CedaShiftData.h>
#include <CedaJobData.h>
#include <CedaFlightData.h>
#include <CedaDemandData.h>
#include <CedaEmpData.h>
#include <conflict.h>
#include <gantt.h>
#include <gbar.h>
#include <ccsddx.h>
#include <CedaAloData.h>
#include <StaffViewer.h>
#include <dataset.h>
#include <AllocData.h>

#include <SelectPoolStaffDlg.h>
#include <SelectPoolStaffForJobFlightDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelectPoolStaffForJobFlightDialog dialog


CSelectPoolStaffForJobFlightDialog::CSelectPoolStaffForJobFlightDialog(CWnd* pParent,
	StaffDiagramViewer *popViewer, int ipGroupno, long lpFlightUrno)
	: CSelectPoolStaffDialog(pParent, popViewer, ipGroupno)
{
	lmFlightUrno = lpFlightUrno;
	//{{AFX_DATA_INIT(CSelectPoolStaffForJobFlightDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CSelectPoolStaffForJobFlightDialog::DoDataExchange(CDataExchange* pDX)
{
	CSelectPoolStaffDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectPoolStaffForJobFlightDialog)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelectPoolStaffForJobFlightDialog, CSelectPoolStaffDialog)
	//{{AFX_MSG_MAP(CSelectPoolStaffForJobFlightDialog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectPoolStaffForJobFlightDialog message handlers

void CSelectPoolStaffForJobFlightDialog::OnOK() 
{
	if (!UpdateData(TRUE))	// error on data validation?
		return;

	CDWordArray olDutyUrnos;
	for (int i = 0; i < omSelectedItems.GetSize(); i++)
		olDutyUrnos.Add(pomViewer->GetLine(imGroupno, omSelectedItems[i])->JobUrno);

	// Assign jobs for user selected duty bars
	int nResult = ogDataSet.CreateJobFlightFromDuty(this, olDutyUrnos, lmFlightUrno, omAloc, omAlid);
	switch (nResult)
	{
	case IDOK:
	case IDYES:	// data is already saved
		EndDialog(IDOK);
		break;
	case IDNO:	// user choose to cancel the operation
		EndDialog(IDCANCEL);
		break;
	case IDCANCEL:	// user want a chance to correct data before re-saving
		break;
	}
}

BOOL CSelectPoolStaffForJobFlightDialog::OnInitDialog() 
{
	CSelectPoolStaffDialog::OnInitDialog();
	
	SetWindowText(GetString(IDS_STRING33025));
	CWnd *polWnd = GetDlgItem(IDC_SELECTEMPLOYEE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32861));
	}
	polWnd = GetDlgItem(IDOK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61697));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61698));
	}

	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
