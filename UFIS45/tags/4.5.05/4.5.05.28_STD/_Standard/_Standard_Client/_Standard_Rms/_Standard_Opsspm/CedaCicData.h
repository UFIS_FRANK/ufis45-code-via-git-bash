// Class for Check In Counter
#ifndef _CEDACICDATA_H_
#define _CEDACICDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct CicDataStruct
{
	long	Urno;		// Unique Record Number
	char	Cnam[6];	// Check In Counter Name
	CTime	Vafr;		// Valid from
	CTime	Vato;		// Valid to
	CTime	Nafr;		// Not available from
	CTime	Nato;		// Not available to
	char	Tele[11];	// Telephone
	char	Term[2];	// Terminal
	char	Hall[7];	// Hall
	char	Region[11];	// Region	
	char	Line[4];	// Line

	CicDataStruct(void)
	{
		Urno = 0L;
		strcpy(Cnam,"");
		Vafr = TIMENULL;
		Vato = TIMENULL;
		Nafr = TIMENULL;
		Nato = TIMENULL;
		strcpy(Tele,"");
		strcpy(Term,"");
		strcpy(Hall,"");
		strcpy(Region,"");
		strcpy(Line,"");
	}
};

typedef struct CicDataStruct CICDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaCicData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <CICDATA>	omData;
	CMapPtrToPtr			omUrnoMap;
	CMapStringToPtr			omNameMap;
	CMapStringToPtr			omHallMap;
	CMapStringToPtr			omRegionMap;
	CMapStringToPtr			omLineMap;

// Operations
public:
	CedaCicData();
	~CedaCicData();

	CString GetTableName(void);
	CCSReturnCode ReadCicData();
	CICDATA* GetCicByUrno(long lpUrno);
	CICDATA* GetCicByName(const char *pcpName);
	long GetCicUrnoByName(const char *pcpName);
	void GetHalls(CStringArray &ropHalls);
	void GetCicsByHall(CCSPtrArray <CICDATA> &ropCics, CString opHall, bool bpReset = true);
	void GetLines(CStringArray &ropLines);
	void GetCicsByLine(CCSPtrArray <CICDATA> &ropCics, CString opLine, bool bpReset = true);
	void GetRegions(CStringArray &ropRegions);
	void GetCicsByRegion(CCSPtrArray <CICDATA> &ropCics, CString opRegion, bool bpReset = true);
	void GetAllCics(CStringArray& ropCics, bool bpReset = true);

private:
	void AddCicInternal(CICDATA &rrpCic);
	void AddCicToHallMap(CICDATA *prpCic);
	void AddCicToLineMap(CICDATA *prpCic);
	void AddCicToRegionMap(CICDATA *prpCic);
	void ClearAll();
};


extern CedaCicData ogCicData;
#endif _CEDACICDATA_H_
