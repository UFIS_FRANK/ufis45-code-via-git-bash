//
// CedaRpfData.cpp - demand<->function list
//
#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaRpfData.h>
#include <BasicData.h>
#include <CedaRudData.h>

void ProcessRpfCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

CedaRpfData::CedaRpfData()
{                  
    BEGIN_CEDARECINFO(RPFDATA, RpfDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_LONG(Urud,"URUD")
		FIELD_CHAR_TRIM(Fcco,"FCCO")
		FIELD_LONG(Upfc,"UPFC")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(RpfDataRecInfo)/sizeof(RpfDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&RpfDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	ogCCSDdx.Register((void *)this, BC_RPF_INSERT, CString("RPFDATA"), CString("Rpf changed"),ProcessRpfCf);
	ogCCSDdx.Register((void *)this, BC_RPF_UPDATE, CString("RPFDATA"), CString("Rpf changed"),ProcessRpfCf);
	ogCCSDdx.Register((void *)this, BC_RPF_DELETE, CString("RPFDATA"), CString("Rpf deleted"),ProcessRpfCf);

    // Initialize table names and field names
    strcpy(pcmTableName,"RPFTAB");
    pcmFieldList = "URNO,URUD,FCCO,UPFC";
}
 
CedaRpfData::~CedaRpfData()
{
	ogCCSDdx.UnRegister(this,NOTUSED);
	ClearAll();
}

void CedaRpfData::ClearAll()
{
	omUrnoMap.RemoveAll();
	omUrudMap.RemoveAll();
	omData.DeleteAll();
} 

bool CedaRpfData::ReadRpfDataByUrno(CString &myRpfUrnos)
{
	CCSReturnCode ilRc = RCSuccess;

	char pclCom[10] = "RT";
    char pclWhere[5000] = "";
	
	sprintf(pclWhere, "WHERE URUD in (%s)", myRpfUrnos);

	if ((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaRpfData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		RPFDATA rlRpfData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlRpfData)) == RCSuccess)
			{
				AddRpfInternal(rlRpfData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaRpfData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(RPFDATA), pclWhere);
    return ilRc;
}
bool CedaRpfData::ReadRpfData(CDWordArray &ropTplUrnos )
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[5000] = "";

	if ( ogRudData.bmUtplExists && !ogBasicData.bmDelegateFlights)
	{
		CString olTplUrnos, olTmp;
		int ilNumTplUrnos = ropTplUrnos.GetSize();
		for(int ilTpl = 0; ilTpl < ilNumTplUrnos; ilTpl++)
		{
			if(ilTpl != 0)
				olTplUrnos += ",";
			olTmp.Format("'%d'", ropTplUrnos[ilTpl]);
			olTplUrnos += olTmp;
		}
		sprintf(pclWhere, "WHERE URUD in (select urno from rudtab where UTPL IN (%s))", olTplUrnos);
	}
	if ((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaRpfData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		RPFDATA rlRpfData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlRpfData)) == RCSuccess)
			{
				AddRpfInternal(rlRpfData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaRpfData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(RPFDATA), pclWhere);
    return ilRc;
}


RPFDATA *CedaRpfData::AddRpfInternal(RPFDATA &rrpRpf)
{
	RPFDATA *prlRpf = new RPFDATA;
	*prlRpf = rrpRpf;
	omData.Add(prlRpf);
	omUrnoMap.SetAt((void *)prlRpf->Urno,prlRpf);
	omUrudMap.SetAt((void *)prlRpf->Urud,prlRpf);

	return prlRpf;
}

void CedaRpfData::DeleteRpfInternal(long lpUrno)
{
	int ilNumRpfs = omData.GetSize();
	for(int ilRpf = (ilNumRpfs-1); ilRpf >= 0; ilRpf--)
	{
		if(omData[ilRpf].Urno == lpUrno)
		{
			omUrudMap.RemoveKey((void *)omData[ilRpf].Urud);
			omData.DeleteAt(ilRpf);
		}
	}
	omUrnoMap.RemoveKey((void *)lpUrno);
}

RPFDATA *CedaRpfData::GetFunctionByUrud(long lpUrud)
{
	RPFDATA *prlRpf = NULL;
	omUrudMap.Lookup((void *)lpUrud, (void *&) prlRpf);
	return prlRpf;
}

RPFDATA *CedaRpfData::GetRpfByUrno(long lpUrno)
{
	RPFDATA *prlRpf = NULL;
	omUrnoMap.Lookup((void *)lpUrno, (void *&) prlRpf);
	return prlRpf;
}

CString CedaRpfData::GetTableName(void)
{
	return CString(pcmTableName);
}

void ProcessRpfCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	((CedaRpfData *)popInstance)->ProcessRpfBc(ipDDXType,vpDataPointer,ropInstanceName);
}

void CedaRpfData::ProcessRpfBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlRpfData = (struct BcStruct *) vpDataPointer;
	ogBasicData.LogBroadcast(prlRpfData);
	RPFDATA rlRpf, *prlRpf = NULL;
	long llUrno = GetUrnoFromSelection(prlRpfData->Selection);
	GetRecordFromItemList(&rlRpf,prlRpfData->Fields,prlRpfData->Data);
	if(llUrno == 0L) llUrno = rlRpf.Urno;

	switch(ipDDXType)
	{
		case BC_RPF_INSERT:
		{
			if((prlRpf = AddRpfInternal(rlRpf)) != NULL)
			{
				ogCCSDdx.DataChanged((void *)this, RPF_INSERT, (void *)prlRpf);
			}
			break;
		}
		case BC_RPF_UPDATE:
		{
			if((prlRpf = GetRpfByUrno(llUrno)) != NULL)
			{
				GetRecordFromItemList(prlRpf,prlRpfData->Fields,prlRpfData->Data);
				//PrepareDataAfterRead(prlRpf);
				ogCCSDdx.DataChanged((void *)this, RPF_UPDATE, (void *)prlRpf);
			}
			break;
		}
		case BC_RPF_DELETE:
		{
			if((prlRpf = GetRpfByUrno(llUrno)) != NULL)
			{
				DeleteRpfInternal(prlRpf->Urno);
				ogCCSDdx.DataChanged((void *)this, RPF_DELETE, (void *)prlRpf);
			}
			break;
		}
	}
}
