#ifndef _CDPRD_H_
#define _CDPRD_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

//@Man: DPRDATA
/*@Doc:

  \begin{verbatim}
    SELECT  stid,vafr,vato,days,shfr,shto,
    FROM    shfcki
  \end{verbatim}
*/
//struct ShiftDataStruct {
 struct DprDataStruct
{
	long  Urno;            // Dpr Id
	CTime Sgfr;		  // Begin of profile segment
	CTime Sgto;		  // End of profile segment
	char  Empr[290];      // Number of employees required (96 intervals, 4 bytes each)
	int   Ivln;           // Interval length
	char  Alid[12];       // Alid
	char  Rank[12];       // Rank ID
	char  Qual[12];       // Qualification Code

	DprDataStruct() 
	{ memset(Empr,'\0',sizeof(Empr));
	Sgfr = TIMENULL;Sgto = TIMENULL;}
};

typedef struct DprDataStruct DPRDATA;

struct DProfileStruct
{
	CString	Alid;				// Alid = CCI-x (x= F,C or M)
	CString	Rank;				// Rank ID
	CString	Qual;				// Qualification Code
	CString Sgfr;
	int		Empr[(3*288)+1];	// Number of employees required (96 intervals, 4 bytes each)

	void PeakDataStruct(void) 
	{ memset(Empr,'\0',sizeof(Empr));}

};

 typedef struct DProfileStruct DPROFILE;

/////////////////////////////////////////////////////////////////////////////
// Class declaration

//@Man:
//@Memo: Class for handling shift type data
//@See: CCSCedaData, CedaJobData, PrePlanTable
/*@Doc:
  Reads and writes shift data from/to database and stores it in memory. 
  The shift data class will be used in {\bf ShiftTable} table and the 
  {\bf PrePlanTable}.

*/
class CedaDprData: public CCSCedaData
{
// Attributes
public:
    //@ManMemo: DPRDATA records read by ReadShiftData()
    CCSPtrArray<DPROFILE> omData;
    //@ManMemo: A map, containing the PKNO fields of all loaded employees.
    CMapStringToPtr omAlidMap;
	CTime omStartDate;
	CString GetTableName(void);

// Operations
public:
    //@ManMemo: Default constructor
    /*@Doc:
      Initialize {\bf omRecInfo} for offsets and types for each fields in {\bf SHIFTDATA},
      the {\bf pcmTableName} with table name {\bf STFCKI}, the data members {\bf omFieldList}
      contains a list of used fields of database view {\bf STFCKI}.
    */
    CedaDprData();
    //@ManMemo: Destructor, Unregisters the CedaShiftData object from DataDistribution
	~CedaDprData();

    //@ManMemo: Read all shift types from database at program start
	CCSReturnCode ReadDprData();
	//DPROFILE  *GetDProfileByAlid(const char *Alid);
	DPROFILE  *GetDProfileByAlid(const char *Alid,CString opDate);
	void Add(DPRDATA& rrlDprData);
private:

};


extern CedaDprData ogDprs;
#endif
