#if !defined(AFX_DEBUGINFOMSGBOX_H__3453D1B1_5EDE_11D4_996F_400000401850__INCLUDED_)
#define AFX_DEBUGINFOMSGBOX_H__3453D1B1_5EDE_11D4_996F_400000401850__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DebugInfoMsgBox.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDebugInfoMsgBox dialog

class CDebugInfoMsgBox : public CDialog
{
// Construction
public:
	CDebugInfoMsgBox(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDebugInfoMsgBox)
	enum { IDD = IDD_DEBUG_INFO_MSGBOX };
	CString	m_MsgBox;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDebugInfoMsgBox)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDebugInfoMsgBox)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEBUGINFOMSGBOX_H__3453D1B1_5EDE_11D4_996F_400000401850__INCLUDED_)
