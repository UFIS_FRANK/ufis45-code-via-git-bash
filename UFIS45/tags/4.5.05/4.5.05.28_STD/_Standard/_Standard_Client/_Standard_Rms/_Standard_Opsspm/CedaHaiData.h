// Class for Airline Types
#ifndef _CEDAHAIDATA_H_
#define _CEDAHAIDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

#define ALT_BANK_CUSTOMER 'K'

struct HaiDataStruct
{
	long	Urno;		// Unique Record Number
	char	Hsna[8];	// Handling Agent Code
	long	Altu;	    // Airline Urno

	HaiDataStruct(void)
	{
		Urno = 0L;
		Altu = 0L;
		strcpy(Hsna,"");
	}
};

typedef struct HaiDataStruct HAIDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaHaiData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <HAIDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omAltuMap;
//	CMapStringToPtr omHsnaMap;
	CString GetTableName(void);

// Operations
public:
	CedaHaiData();
	~CedaHaiData();

	CCSReturnCode ReadHaiData();
	HAIDATA* GetHaiByUrno(long lpUrno);
	HAIDATA* GetHaiByAltu(long lpAltu);

private:
	void AddHaiInternal(HAIDATA &rrpHai);
	void ClearAll();
};


extern CedaHaiData ogHaiData;
#endif _CEDAHAIDATA_H_
