// PrePlanTableSortPage.h : header file
//

#ifndef _PREPLNSO_H_
#define _PREPLNSO_H_

/////////////////////////////////////////////////////////////////////////////
// PrePlanTableSortPage dialog

class PrePlanTableSortPage : public CPropertyPage
{
	DECLARE_DYNCREATE(PrePlanTableSortPage)

// Construction
public:
	PrePlanTableSortPage();
	~PrePlanTableSortPage();

// Dialog Data
	CStringArray omSortOrders;
	CString omTitle;

	//{{AFX_DATA(PrePlanTableSortPage)
	enum { IDD = IDD_PREPLANTABLE_SORT_PAGE };
	BOOL	m_Group;
	int		m_SortOrder0;
	int		m_SortOrder1;
	int		m_SortOrder2;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(PrePlanTableSortPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(PrePlanTableSortPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Helper routines
private:
	int GetSortOrder(const char *pcpSortKey);
	CString GetSortKey(int ipSortOrder);
};

#endif // _PREPLNSO_H_
