#ifndef __GATEDV_H__
#define __GATEDV_H__

#include <CCSPtrArray.h>
#include <CedaShiftData.h>
#include <cviewer.h>
#include <table.h>
#include <CedaJobData.h>
#include <CedaFlightData.h>

struct PSTDETAIL_ASSIGNMENT  // each assignment record
{
    CString Name;   // assigned to gate or flight
	long Urno;      // flight job Urno 
    CTime Acfr;     // assigned from
    CTime Acto;     // assigned until
};

struct PSTDETAIL_LINEDATA
{
	long	FlightUrno;
	CString Fnum;	// Flight number
	CString Rou1;	// Origin/Destination
	CString Iofl;	// I/O flag
	CTime   FlightTime;	// Stoa/Stod depends on I/O flag

    CCSPtrArray<PSTDETAIL_ASSIGNMENT> Assignment;
};

/////////////////////////////////////////////////////////////////////////////
// PstDetailViewer

class PstDetailViewer: public CViewer
{
// Constructions
public:
    PstDetailViewer();
    ~PstDetailViewer();

    void Attach(CTable *popTable,CTime opStartTime,CTime opEndTime,CString opAlid);
	void ChangeView();
  	int	  Lines() const;
	PSTDETAIL_LINEDATA*	GetLine(int ipLineNo);

// Internal data processing routines
private:
	void PrepareGrouping();
    void PrepareFilter();
	void PrepareSorter();
	BOOL IsPassFilter(CTime opAcfr);

    void MakeLines();
	void MakeLine(FLIGHTDATA *prpFlight);
	void MakeAssignments(long lpFlightUrno,PSTDETAIL_LINEDATA &rrpFlightLine);
	int CreateAssignment(PSTDETAIL_LINEDATA &rrlStaff, PSTDETAIL_ASSIGNMENT *prpAssignment);

	BOOL FindFlight(long lpFlightUrno, int &rilLineno);

// Operations
public:
	void DeleteAll();
	int CreateLine(PSTDETAIL_LINEDATA *prpStaff);
	void DeleteLine(int ipLineno);

// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(PSTDETAIL_LINEDATA *prpLine);
	void ProcessFlightChange(FLIGHTDATA *prpFlight);

// Attributes used for filtering condition
private:
    CString omDate;
    CTime omStartTime;
	CTime omEndTime;

// Attributes
private:
    CTable *pomTable;
    CCSPtrArray<PSTDETAIL_LINEDATA> omLines;
	CString omAlid;
// Methods which handle changes (from Data Distributor)
public:
};

#endif //__PSTDV_H__
