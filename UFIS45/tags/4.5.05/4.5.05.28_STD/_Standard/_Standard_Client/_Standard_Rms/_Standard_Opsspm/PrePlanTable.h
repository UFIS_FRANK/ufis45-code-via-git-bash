#ifndef _PREPLANT_H_
#define _PREPLANT_H_

#include <CCSPtrArray.h>
#include <cviewer.h>
#include <table.h>
#include <CCSDragDropCtrl.h>
#include <PrintControl.h>
#include <PrePlanTeamTable.h>
#include <PrePlanTableTeamViewer.h>
#include <PrePlanTableViewer.h>
#include <PrePlanTeamPrintDlg.h>
#include <CCSPrint.h>
#include <afxtempl.h>

class CPrintPreviewView;
/////////////////////////////////////////////////////////////////////////////
// PrePlanTable frame

class PrePlanTable: public CDialog
{
// Construction
public:
    PrePlanTable(CString opDate = "");
    PrePlanTable(BOOL bpIsFromSearch,BOOL bpDetailDisplay = TRUE);
	~PrePlanTable();

	CTime GetPrePlanTime(void);

// Private Attributes
private:
    CString omDate;
    CTable omTable; // visual object, the table content
	CTable omTableT3; //Singapore
	CTable* pomTable; //Singapore
	PrePlanTableViewer* pomViewer; //Singapore
	CPrintPreviewView* pomPrintPreviewView; //Singapore
	CString omActiveTerminal; //Singapore
	CRect omWindowRect; //PRF 8712
	long			  omContextItem;
	CArray<long,long> omContextShiftUrnos;
	CArray<long,long> omContextAssignUrnos;

	BOOL bmIsTeamDetailOpen;
    // This member data is required for the "InsertEmployee" method. Since the user may
    // insert a data with a different date from current selected date of the PrePlanTable.
    // The "CEmpDataDialog" dialog will need this "lmDate" to check if the inserted shift
    // should update the PrePlanTable or not.
    //
    CTime lmDate;   // the date of the selected and displayed EMPDATA in the table window

	PrePlanTableViewer omViewer;
	PrePlanTableViewer omViewerT3; //Singapore

	// Definition of drag-and-drop object
	CCSDragDropCtrl omDragDropObject;

// Helper functions for displaying data in the table content
public:
    void UpdateView(CString olViewName);
	void UpdateComboBox();
	void SetCaptionText(void);
	void SetViewerDate();
	void HandleGlobalDateUpdate();
	void SetDate(CTime opDate);
	void SendUpdatePrePlanDate(CTime opDate);
	CTable* GetActiveTableWindow(){return &omTable;}
	CTime omPrePlanDate;
	CSingleDocTemplate* pomTemplate;

	static const CString Terminal2;
	static const CString Terminal3;

// Printing implemantation
private:
	static void PrePlanTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName);
	BOOL PreplanPreparePrintData(CString popDutyDay);
	BOOL PreplanSelectTeams(CString popDutyDay);
	BOOL PrintPreplanHeader(CCSPrint *pomPrint,CString opTeam, CString opDutyDate);
	BOOL PrintPreplanTeam(CCSPrint *pomPrint,CString opTeam, CString opDutyDate);
	BOOL PrintPreplanLine(CCSPrint *pomPrint,SHIFTDATA *popShift,CString opTeam,int ipIndex,BOOL bpIsLastLine,CString opDutyDate);
	void DeleteAllAssignments(int ipLineNo);
	void ClosePrePlan();
	void DeleteAssignment(int ipIndex);
	BOOL GetShiftDataArray(CCSPtrArray<SHIFTDATA>& ropShiftArray,const CString& ropTeam,
		                   const CString& ropDutyDate) const; //Singapore
	void SetupPrintPreviewPageInfo(); //Singapore
	void SetupPrintPageInfo(); //Singapore
	void PrintData(CCSPrint *pomPrint,const int& ripPageNo); //Singapore
	BOOL SelectPrintTerminal();
	void PrintData();
	bool CheckForDeviationOrAbsence(SHIFTDATA *prpShift);

// Printing attributes
private:
	CStringArray omTeamArray;
	PrePlanTeamPrintDialog omTeamDlg;
	BOOL bmIsViewOpen;
	BOOL bmIsFromSearch;
	BOOL bmIsDetailDisplay;
	CCSPrint *pomPrint;
	CPtrArray omPtrCCSPrintArray; //Singapore
	BOOL bmIsT2Visible; //Singapore
	BOOL bmIsT3Visible; //Singapore
	CMapWordToPtr omMapPageNoToPreviewData;
	BOOL bmPrintPreviewPageInfo;
	int imPrintMaxLines;
	int imTerminalSelected;

// Dialog Data
private:
    int m_nDialogBarHeight;
	PrePlanTeamTable *pomTeamTable;

    //{{AFX_DATA(PrePlanTable)
    enum { IDD = IDD_PREPLANTABLE };
    CButton m_UpdateButton;
    CButton m_InsertButton;
    BOOL    m_bCompleteTeam;
    BOOL    m_bAbsent;
	CComboBox	m_Date;
	CCSButtonCtrl m_bTerminal2; //Singapore
	CCSButtonCtrl m_bTerminal3; //Singapore
    //}}AFX_DATA

// Implementation
public:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	BOOL DestroyWindow(); 

    // Generated message map functions
    //{{AFX_MSG(PrePlanTable)
	virtual void OnCancel();
	virtual void OnOK();
    virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnPaint();
	afx_msg void OnClose();
    afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMove(int x, int y); //PRF 8712
    afx_msg void OnCompleteteam();
    afx_msg void OnAbsent();
    afx_msg void OnInsertEmployee();
    afx_msg void OnUpdateEmployee();
	afx_msg void OnPrint();	
    afx_msg LONG OnTableDragBegin(UINT wParam, LONG lParam);
    afx_msg LONG OnTableLButtonDblClk(UINT wParam, LONG lParam);
    afx_msg LONG OnTableRButtonDown(UINT wParam, LONG lParam);
	afx_msg void OnTableSetTableWindow(WPARAM wParam, LPARAM lParam); //Singapore
	afx_msg LONG OnPrePlanTeamTableExit(UINT /*wParam*/, LONG /*lParam*/);
	afx_msg void OnSelchangeView();
	afx_msg void OnAnsicht();
	afx_msg void OnButtonT2(); //Singapore
	afx_msg void OnButtonT3(); //Singapore
	afx_msg void OnTableUpdateDataCount(); //Singapore
	afx_msg void OnPrintPreview(); //Singapore
	afx_msg void OnBeginPrinting(WPARAM wParam, LPARAM lParam); //Singapore
	afx_msg void PrintPreview(WPARAM wParam, LPARAM lParam); //Singapore
	afx_msg void OnEndPrinting(WPARAM wParam, LPARAM lParam); //Singapore
	afx_msg void OnPreviewPrint(WPARAM wParam, LPARAM lParam); //Singapore
	afx_msg void OnMenuDelete(UINT nID);
	afx_msg void OnCloseupView2();
	afx_msg void OnSelchangeDate();
	afx_msg void OnMenuSwapShift();
	afx_msg void OnMenuDebugInfo();
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};

#endif
