#if !defined(AFX_SETDISPLAYTIMEFRAMEDLG_H__4261498F_CE48_4A32_AE9A_D1703224D622__INCLUDED_)
#define AFX_SETDISPLAYTIMEFRAMEDLG_H__4261498F_CE48_4A32_AE9A_D1703224D622__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SetDisplayTimeframeDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSetDisplayTimeframeDlg dialog

class CSetDisplayTimeframeDlg : public CDialog
{
// Construction
public:
	CSetDisplayTimeframeDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSetDisplayTimeframeDlg)
	enum { IDD = IDD_TABLETIMESDLG };
	CTime		m_FromDate;
	CTime		m_FromTime;
	CTime		m_ToDate;
	CTime		m_ToTime;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetDisplayTimeframeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSetDisplayTimeframeDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	

public:
	CTime omFrom, omTo;
	BOOL omFromReadOnly,omToReadOnly;
	
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SETDISPLAYTIMEFRAMEDLG_H__4261498F_CE48_4A32_AE9A_D1703224D622__INCLUDED_)
