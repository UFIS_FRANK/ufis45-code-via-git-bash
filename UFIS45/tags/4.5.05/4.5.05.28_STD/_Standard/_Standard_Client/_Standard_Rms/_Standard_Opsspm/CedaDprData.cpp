// CedaDprData.cpp - Class for handling demand profile data
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaDprData.h>
#include <BasicData.h>

// MCU 07.07 no more pogPrePlanShiftData, it's now ogShiftData
//extern CedaShiftData *pogPrePlanShiftData;



CedaDprData::CedaDprData()
{                  
    // Create an array of CEDARECINFO for SHIFTTYPEDATA
    BEGIN_CEDARECINFO(DPRDATA, DprDataRecInfo)
        FIELD_LONG(Urno,"URNO")
        FIELD_DATE(Sgfr,"SGFR")
        FIELD_DATE(Sgto,"SGTO")
        FIELD_CHAR_TRIM(Empr,"EMPR")
        FIELD_INT(Ivln,"IVLN")
        FIELD_CHAR_TRIM(Alid,"ALID")
        FIELD_CHAR_TRIM(Rank,"RANK")
        FIELD_CHAR_TRIM(Qual,"QUAL")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(DprDataRecInfo)/sizeof(DprDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&DprDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"DPRCKI");
    pcmFieldList = "URNO,SGFR,SGTO,EMPR,IVLN,ALID,RANK,QUAL";
}
 
CedaDprData::~CedaDprData()
{
	TRACE("CedaDprData::~CedaDprData called\n");
	omData.DeleteAll();
	omAlidMap.RemoveAll();
}

CCSReturnCode CedaDprData::ReadDprData()
{
	char pclCom[10] = "RT";
    char pclWhere[512];
	CCSReturnCode ilRc = RCSuccess;

	CTime olTmpTime = ogBasicData.GetTime();
	omStartDate = CTime(olTmpTime.GetYear(), olTmpTime.GetMonth(),
		olTmpTime.GetDay(), 0, 0, 0);
	CTime olEndDate = omStartDate + CTimeSpan(3,0,0,0);
	DPRDATA rlDprData;

    // Select data from the database
    sprintf(pclWhere, "WHERE (SGFR >= '%s000000') and (SGTO <= '%s235900')"
							" AND (MIRQ = 'N' AND CSID = 'PDI' AND REUR = 'LH')", 
		(const char *)omStartDate.Format("%Y%m%d"),(const char *)olEndDate.Format("%Y%m%d"));
    if((ilRc = CedaAction2(pclCom, pclWhere)) == RCFailure)
	{
		ogBasicData.LogCedaError("CedaDprData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
        return RCFailure;
	}

    // Load data from CCSCedaData into the dynamic array of record
    omAlidMap.RemoveAll();
    omData.DeleteAll();

    for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
    {
		if ((ilRc = GetBufferRecord2(ilLc,&rlDprData)) == RCSuccess)
		{
			Add(rlDprData);
		}
	}

	ogBasicData.Trace("CedaDprData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(DPRDATA), pclWhere);
    return RCSuccess;
}


void CedaDprData::Add(DPRDATA& rrlDprData)
{

	DPROFILE *prlProfile;

	CString olKey = rrlDprData.Alid + rrlDprData.Sgfr.Format("%Y%m%d");
	if (omAlidMap.Lookup(olKey,(void *& )prlProfile) != TRUE)
	{
		prlProfile = &omData[omData.New()];
		memset(prlProfile->Empr,0,sizeof(prlProfile->Empr));
		prlProfile->Alid = rrlDprData.Alid;
		prlProfile->Rank = rrlDprData.Rank;
		prlProfile->Qual = rrlDprData.Qual;
		prlProfile->Sgfr = rrlDprData.Sgfr.Format("%Y%m%d");
		omAlidMap.SetAt(olKey,prlProfile);
	}
//	CTimeSpan olOffset = CTimeSpan(rrlDprData.Sgfr.GetDay(),rrlDprData.Sgfr.GetHour(),
//					rrlDprData.Sgfr.GetMinute(),rrlDprData.Sgfr.GetSecond())
//			- 
//		CTimeSpan(omStartDate.GetDay(),omStartDate.GetHour(),
//					omStartDate.GetMinute(),omStartDate.GetSecond());
	CTime olStart(rrlDprData.Sgfr.GetYear(),rrlDprData.Sgfr.GetMonth(),
		rrlDprData.Sgfr.GetDay(),0,0,0);
	CTimeSpan olOffset = rrlDprData.Sgfr - olStart;
	long llOffset = olOffset.GetTotalMinutes()/5;
	for(int ilLc = 0; ilLc < 288; ilLc += 3, llOffset++)
	{
		char pclEmpr[12];
		strncpy(pclEmpr,&rrlDprData.Empr[ilLc],3);
		pclEmpr[3] = '\0';
		prlProfile->Empr[llOffset] = atoi(pclEmpr);
		if (prlProfile->Empr[llOffset] > 1000 || prlProfile->Empr[llOffset] < -1000)
		{
			prlProfile->Empr[llOffset] = 0;
		}
	}
}

DPROFILE  *CedaDprData::GetDProfileByAlid(const char *Alid,CString opDate)
{
	DPROFILE  *prlDpr;

	int ilCount = omData.GetSize();
	for ( int ilLc = 0; ilLc < ilCount; ilLc++)
	{
		prlDpr = &omData[ilLc];
		if ((strcmp(prlDpr->Alid,Alid) == 0) &&
			(prlDpr->Sgfr == opDate))
		{
			return prlDpr;
		}

	}
	/*
	if (omAlidMap.Lookup(Alid,(void *& )prlDpr) == TRUE)
	{
		TRACE("GetDprById %s \n",Alid);
		return prlDpr;
	}
	*/
	return NULL;
}

CString CedaDprData::GetTableName(void)
{
	return CString(pcmTableName);
}