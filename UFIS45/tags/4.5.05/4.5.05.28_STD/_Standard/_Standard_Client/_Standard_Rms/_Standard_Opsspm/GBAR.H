// gbar.h : header file
//

#ifndef _GBAR_H_
#define _GBAR_H_

#include <CCSPtrArray.h>

// Frame types
enum { FRAMENONE, FRAMERECT, FRAMEBACKGROUND };

enum { BD_BAR, BD_STRING, BD_CIRCLE, BD_REGION };
struct BARDECO
{
	int			type;		// Decorationtype
	CString		Value;		// String to be positioned in the CRect
	CRect		Rect;		// possibility of drawing an additional rect in bar
							// may be used to paint a line as specialized rect
	COLORREF    Color;
	CFont       *DecoFont;
	int			Hatched;	// set to -1 (NONE) or HS_HORIZONTAL,HS_VERTICAL,HS_FDIAGONAL,HS_BDIAGONAL,HS_CROSS,HS_DIAGCROSS
	CBitmap		*Bitmap;		

	CRgn		*Region;

	BARDECO(void)
	{
		Hatched = -1;
		Bitmap = NULL;
	}
};
// Marker types
enum { MARKNONE, MARKLEFT, MARKRIGHT, MARKFULL, MARKHATCHED };

/////////////////////////////////////////////////////////////////////////////
// CTestViewer window
 
class GanttBar
{
// Construction
public:
    GanttBar();
	/* fpa
    GanttBar(CDC *popDC, const CRect &opRect, int ipFrameType,
        int ipMarkerType, CBrush *popMarkerBrush,
        const char *pcpText, CFont *popTextFont, COLORREF lpTextColor, int ipFramePixel = 1, 
		BOOL isRightMarker = FALSE, const CRect &opRightMarkerRect = CRect(0,0,0,0), 
		CBrush *popRightMarkerBrush = NULL, CCSPtrArray<BARDECO> *ropDeco = NULL );
	*/
    GanttBar(CDC *popDC, const CRect &opRect, int ipFrameType,
        int ipMarkerType, CBrush *popMarkerBrush,
        const char *pcpText, CFont *popTextFont, COLORREF lpTextColor, int ipFramePixel = 1, 
		BOOL isRightMarker = FALSE, const CRect &opRightMarkerRect = CRect(0,0,0,0), 
		CBrush *popRightMarkerBrush = NULL, CCSPtrArray<BARDECO> *ropDeco = NULL, UINT upAlignText = TA_CENTER,
		const char *pcpAddictionalText = NULL, UINT upAlignAdditionalText = 0, COLORREF lpFrameColor = RGB(0,0,0));

	static void MakeOfflineSymbol(CCSPtrArray <BARDECO> &ropDecoData, CRect &ropBarRect);
	static void MakeOfflineSymbol(CCSPtrArray <BARDECO> &ropDecoData, int ipLeft, int ipTop, int ipRight, int ipBottom);
	static void MakeFunctionSymbol(CCSPtrArray <BARDECO> &ropDecoData, CRect &ropBarRect);
	static void MakeFunctionSymbol(CCSPtrArray <BARDECO> &ropDecoData, int ipLeft, int ipTop, int ipRight, int ipBottom);
	static void MakeEmpInformedSymbol(CCSPtrArray <BARDECO> &ropDecoData, CRect &ropBarRect);
	static void MakeEmpInformedSymbol(CCSPtrArray <BARDECO> &ropDecoData, int ipLeft, int ipTop, int ipRight, int ipBottom);
	static void MakeEmpInformedSymbol(CCSPtrArray <BARDECO> &ropDecoData, int ipLeft, int ipTop, int ipRight, int ipBottom,COLORREF ipColor);

// Operations
public:
	/*
    void Paint(CDC *popDC, const CRect &opRect, int ipFrameType,
        int ipMarkerType, int ipFramePixel, CBrush *popMarkerBrush,
        const char *pcpText, CFont *popTextFont, COLORREF lpTextColor,
		BOOL isRightMarker = FALSE, const CRect &opRightMarkerRect = CRect(0,0,0,0), 
		CBrush *popRightMarkerBrush = NULL, CCSPtrArray<BARDECO> *ropDeco = NULL );
	*/

    void Paint(CDC *popDC, const CRect &opRect, int ipFrameType,
        int ipMarkerType, int ipFramePixel, CBrush *popMarkerBrush,
        const char *pcpText, CFont *popTextFont, COLORREF lpTextColor,
		BOOL isRightMarker = FALSE, const CRect &opRightMarkerRect = CRect(0,0,0,0), 
		CBrush *popRightMarkerBrush = NULL, CCSPtrArray<BARDECO> *ropDeco = NULL, UINT upAlignText = TA_CENTER,
		const char *pcpAddictionalText = NULL, UINT upAlignAdditionalText = 0, COLORREF lpFrameColor = RGB(0,0,0));
};

#endif
