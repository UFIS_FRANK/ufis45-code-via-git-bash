#if !defined(AFX_OtherColoursDlg_H__350392D1_88CB_4E20_A04F_476C3C9D3C76__INCLUDED_)
#define AFX_OtherColoursDlg_H__350392D1_88CB_4E20_A04F_476C3C9D3C76__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// OtherColoursDlg.h : header file
//

#include <CCSTable.h>
#include <OtherColoursViewer.h>

/////////////////////////////////////////////////////////////////////////////
// COtherColoursDlg dialog

class COtherColoursDlg : public CDialog
{
// Construction
public:
	COtherColoursDlg(CWnd* pParent = NULL);   // standard constructor
	~COtherColoursDlg();

// Dialog Data
	//{{AFX_DATA(COtherColoursDlg)
	enum { IDD = IDD_OTHERCOLOURS_DLG };
	CButton	m_DisplayDefinedOnlyCheckboxCtrl;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COtherColoursDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(COtherColoursDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnDisplayDefinedOnlyClicked();
	afx_msg LONG OnTableLButtonDblclk(UINT ipItem, LONG lpLParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CCSTable *pomTable;
	OtherColoursViewer *pomViewer;
	void UpdateDisplay();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OtherColoursDlg_H__350392D1_88CB_4E20_A04F_476C3C9D3C76__INCLUDED_)
