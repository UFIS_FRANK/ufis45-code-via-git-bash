// CopyDemandDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <CopyDemandDlg.h>
#include <BasicData.h>
#include <CedaCicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCopyDemandDlg dialog


CCopyDemandDlg::CCopyDemandDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCopyDemandDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCopyDemandDlg)
	//}}AFX_DATA_INIT
}


void CCopyDemandDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCopyDemandDlg)
	DDX_CCSddmmyy(pDX, IDC_FROMDATE, m_FromDate);
	DDX_CCSTime(pDX, IDC_FROMTIME, m_FromTime);
	DDX_CCSddmmyy(pDX, IDC_TODATE, m_ToDate);
	DDX_CCSTime(pDX, IDC_TOTIME, m_ToTime);
	DDX_Control(pDX, IDC_ALID, m_CounterList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCopyDemandDlg, CDialog)
	//{{AFX_MSG_MAP(CCopyDemandDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCopyDemandDlg message handlers

void CCopyDemandDlg::Init(CWnd* pParent, CString opSelectedCounter, CTime opFrom, CTime opTo)
{
	omFrom = opFrom;
	omTo = opTo;
	omSelectedCounter = opSelectedCounter;

	m_FromDate = opFrom;
	m_FromTime = opFrom;
	m_ToDate   = opTo;
	m_ToTime   = opTo;
}

BOOL CCopyDemandDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(GetString(IDS_COPYDEMANDDLG_TITLE));
	GetDlgItem(IDC_EDITFROM)->SetWindowText(GetString(IDS_COPYDEMANDDLG_FROM));
	GetDlgItem(IDC_EDITTO)->SetWindowText(GetString(IDS_COPYDEMANDDLG_TO));
	GetDlgItem(IDC_COUNTER)->SetWindowText(GetString(IDS_COPYDEMANDDLG_COUNTER));

	int ilNumCics = ogCicData.omData.GetSize();
	for(int i = 0; i < ilNumCics; i++)
	{
		CICDATA *prlCic = & ogCicData.omData[i];
		m_CounterList.AddString(prlCic->Cnam);
	}
	m_CounterList.AddString(GetString(IDS_STRING62514)); // "Without Counter"
	if(omSelectedCounter.IsEmpty())
	{
		omSelectedCounter = GetString(IDS_STRING62514);
	}

	m_CounterList.SelectString(-1, omSelectedCounter);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCopyDemandDlg::OnOK() 
{
	if(UpdateData(TRUE))
	{
		omFrom = CTime (m_FromDate.GetYear(),m_FromDate.GetMonth(),m_FromDate.GetDay(),
						m_FromTime.GetHour(),m_FromTime.GetMinute(),m_FromTime.GetSecond());

		omTo = CTime (m_ToDate.GetYear(),m_ToDate.GetMonth(),m_ToDate.GetDay(),
						m_ToTime.GetHour(),m_ToTime.GetMinute(),m_ToTime.GetSecond());

		int ilSel = m_CounterList.GetCurSel();
		if(ilSel != CB_ERR)
		    m_CounterList.GetLBText(ilSel, omSelectedCounter);

		if(omSelectedCounter == GetString(IDS_STRING62514)) // "Without Counter"
			omSelectedCounter.Empty();

		if (omFrom >= omTo)
			MessageBox(GetString(IDS_STRING61262));
		else
			EndDialog(IDOK);
	}
}
