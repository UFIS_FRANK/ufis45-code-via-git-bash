// WhatIfDlg.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// CWhatIf dialog
#ifndef _WHATIF_DLG_H_
#define _WHATIF_DLG_H_

#include <CedaOptData.h>

enum {INSERT_MODE, UPDATE_MODE, DELETE_MODE};
class CWhatIf : public CDialog
{
// Construction
public:
	CWhatIf(CWnd* pParent, int ilKind, OPTDATA *prpOpt, char *pcpParaSet);   // standard constructor

	BOOL isSetFromProgram;
	int  imActualKind;
// Dialog Data
	//{{AFX_DATA(CWhatIf)
	enum { IDD = IDD_WHAT_IF };
	CEdit	m_WolEdit;
	CEdit	m_VmfEdit;
	CEdit	m_TecEdit;
	CEdit	m_RmxEdit;
	CEdit	m_QuaEdit;
	CEdit	m_PraEdit;
	CEdit	m_OlpEdit;
	CEdit	m_NeaEdit;
	CEdit	m_MrtEdit;
	CEdit	m_JmpEdit;
	CEdit	m_HisEdit;
	CEdit	m_DtlEdit;
	CEdit	m_DisEdit;
	CEdit	m_BreEdit;
	CEdit	m_SaopEdit;
	CEdit	m_Comment;
	CEdit	m_Freetxt;
	CButton	m_Emf;
	CButton	m_Fmf;
	CSliderCtrl	m_SliderWol;
	CSliderCtrl	m_SliderTec;
	CSliderCtrl	m_SliderRmx;
	CSliderCtrl	m_SliderQua;
	CSliderCtrl	m_SliderOlp;
	CSliderCtrl	m_SliderDtl;
	CSliderCtrl	m_SliderBre;
	CSliderCtrl	m_SliderAop;
	CSliderCtrl	m_SliderDis;
	CSliderCtrl	m_SliderHis;
	CSliderCtrl	m_SliderJmp;
	CSliderCtrl	m_SliderVmf;
	CSliderCtrl	m_SliderPra;
	CSliderCtrl	m_SliderNea;
	int		m_Solp;
	int		m_Stec;
	int		m_Sdtl;
	int		m_Squa;
	int		m_Sbre;
	int		m_Swol;
	int		m_Smrt;
	int		m_Sdis;
	int		m_Shis;
	int		m_Sjmp;
	int		m_Svmf;
	int		m_Spra;
	int		m_Snea;
	int		m_Saop;
	int		m_Srmx;
	//}}AFX_DATA

	CString omAdditionalCmd;
	int imMode;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWhatIf)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
	void MakeParameters();
	void ClearScreen();
protected:

	// Generated message map functions
	//{{AFX_MSG(CWhatIf)
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnChkEmf();
	afx_msg void OnChkFmf();
	afx_msg void OnReset();
	afx_msg void OnDelete();
	afx_msg void OnStore();
	afx_msg void OnStoreTo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	void GetParaData(OPTDATA *prpOpt);
	void SetSlider(BOOL ipVal1,BOOL ipVal2,BOOL ipVal3,BOOL ipVal4,BOOL ipVal5,BOOL ipVal6,BOOL ipVal7,BOOL ipVal8,BOOL ipVal9,BOOL ipVal10,BOOL ipVal11,BOOL ipVal12,BOOL ipVal13,BOOL ipVal14,BOOL ipVal15,BOOL ipVal16,BOOL ipVal17);
	void SetCaption();

	OPTDATA *prmOpt;
	char *pcmParaSet;
};


#endif //_WHATIF_DLG_H_
