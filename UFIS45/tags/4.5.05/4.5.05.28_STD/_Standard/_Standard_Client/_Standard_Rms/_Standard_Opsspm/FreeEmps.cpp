// FreeEmps.cpp : implementation file
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <OpssPm.h>
#include <dataset.h>
#include <FreeEmps.h>
#include <BasicData.h>
#include <AllocData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FreeEmps dialog


FreeEmps::FreeEmps(CWnd* pParent /*=NULL*/, CCSPtrArray<FREEEMPS_LINEDATA> *ropEmps, 
				   CRect *popParentRect, CString opCaller, FREEEMPS_ADDITIONALS *prpAddi)
	: CDialog(FreeEmps::IDD, pParent)
{
	//{{AFX_DATA_INIT(FreeEmps)
		// NOTE: the ClassWizard will add member initialization here
		bmTeamAlloc = false;
	//}}AFX_DATA_INIT
	
	omCaller = opCaller;
	pomParentRect = popParentRect;
	pomFreeEmps = ropEmps;

	if(prpAddi != NULL)
	{
		rmAddi = *prpAddi;
	}

	pomViewer = new FreeEmpViewer(pomFreeEmps);
	pomFreeEmpTable = new CTable;
	if(omCaller != CString("GATE") && omCaller != CString("PST"))
	{
		pomFreeEmpTable->SetSelectMode(0);
	}

	// display the same emps that are displayed on the StaffGantt
	pomViewer->SetViewerKey("StaffDia"); // REGN

    //CDialog::Create(FreeEmps::IDD);

}

FreeEmps::~FreeEmps()
{
	delete pomFreeEmpTable;
	delete pomViewer;
}


void FreeEmps::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FreeEmps)
		// NOTE: the ClassWizard will add DDX and DDV calls here
		DDX_Check(pDX,IDC_TEAM_ALLOC,bmTeamAlloc);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(FreeEmps, CDialog)
	//{{AFX_MSG_MAP(FreeEmps)
    ON_MESSAGE(WM_TABLE_DRAGBEGIN, OnTableDragBegin)
    ON_MESSAGE(WM_TABLE_RBUTTONDOWN, OnTableRButtonDown)
	ON_WM_SIZE()
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
    ON_CBN_SELCHANGE(IDC_VIEW, UpdateView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FreeEmps message handlers

BOOL FreeEmps::OnInitDialog() 
{
	CDialog::OnInitDialog();

	SetWindowText(GetString(IDS_STRING61376));
	CWnd *polWnd = GetDlgItem(IDOK); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61697));
	}
	polWnd = GetDlgItem(IDCANCEL); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61698));
	}

	polWnd = GetDlgItem(IDC_FUNCTIONS);
	if (polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61930));
	}

	polWnd = GetDlgItem(IDC_TEAM_ALLOC);
	if (polWnd)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61931));
	}


	// TODO: Add extra initialization here
	SetWindowPos(&wndTop, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);	

    CRect rect;
    GetClientRect(&rect);
    rect.InflateRect(1, 1);     // hiding the CTable window border
    pomFreeEmpTable->SetTableData(this, rect.left, rect.right, rect.top+75, rect.bottom-0);


	pomViewer->Attach(pomFreeEmpTable);

	UpdateComboBox(); // set the list of views
	ogBasicData.SetWindowStat("AVAILABLE_EMPS_DLG IDC_VIEW",GetDlgItem(IDC_VIEW));
	UpdateView(); // update the table of available emps

	if(omCaller == CString("GATE"))
	{
		char pclText[100]="";
		//sprintf(pclText, "Gesuchtes Zeitintervall: %s - %s", rmAddi.StartTime.Format("%H%M"), rmAddi.EndTime.Format("%H%M"));
		sprintf(pclText, GetString(IDS_STRING61540), rmAddi.StartTime.Format("%H%M"), rmAddi.EndTime.Format("%H%M"));
		::SetDlgItemText(GetSafeHwnd(), IDC_TIMES, pclText);
		//sprintf(pclText, "Flug: %s", rmAddi.FlightText);
		sprintf(pclText, GetString(IDS_STRING61541), rmAddi.FlightText);
		::SetDlgItemText(GetSafeHwnd(), IDC_JOB, pclText);
	}
	else if(omCaller == CString("CCI"))
	{
		char pclText[100]="";
		//sprintf(pclText, "Gesuchtes Zeitintervall: %s - %s", rmAddi.StartTime.Format("%H%M"), rmAddi.EndTime.Format("%H%M"));
		sprintf(pclText, GetString(IDS_STRING61540), rmAddi.StartTime.Format("%H%M"), rmAddi.EndTime.Format("%H%M"));
		::SetDlgItemText(GetSafeHwnd(), IDC_TIMES, pclText);
		//sprintf(pclText, "Flug: %s", rmAddi.JobText);
		sprintf(pclText, GetString(IDS_STRING61541), rmAddi.JobText);
		::SetDlgItemText(GetSafeHwnd(), IDC_JOB, pclText);
	}
	else if(omCaller == CString("PST"))
	{
		char pclText[100]="";
		//sprintf(pclText, "Gesuchtes Zeitintervall: %s - %s", rmAddi.StartTime.Format("%H%M"), rmAddi.EndTime.Format("%H%M"));
		sprintf(pclText, GetString(IDS_STRING61540), rmAddi.StartTime.Format("%H%M"), rmAddi.EndTime.Format("%H%M"));
		::SetDlgItemText(GetSafeHwnd(), IDC_TIMES, pclText);
		//sprintf(pclText, "Flug: %s", rmAddi.JobText);
		sprintf(pclText, GetString(IDS_STRING61541), rmAddi.FlightText);
		::SetDlgItemText(GetSafeHwnd(), IDC_JOB, pclText);
	}
	else if(omCaller == CString("Regn"))
	{
		char pclText[100]="";
		//sprintf(pclText, "Gesuchtes Zeitintervall: %s - %s", rmAddi.StartTime.Format("%H%M"), rmAddi.EndTime.Format("%H%M"));
		sprintf(pclText, GetString(IDS_STRING61540), rmAddi.StartTime.Format("%H%M"), rmAddi.EndTime.Format("%H%M"));
		::SetDlgItemText(GetSafeHwnd(), IDC_TIMES, pclText);
		//sprintf(pclText, "Flug: %s", rmAddi.JobText);
		sprintf(pclText, GetString(IDS_STRING61541), rmAddi.FlightText);
		::SetDlgItemText(GetSafeHwnd(), IDC_JOB, pclText);
	}
	else if(omCaller == CString("FID"))
	{
		char pclText[100]="";
		//sprintf(pclText, "Gesuchtes Zeitintervall: %s - %s", rmAddi.StartTime.Format("%H%M"), rmAddi.EndTime.Format("%H%M"));
		sprintf(pclText, GetString(IDS_STRING61540), rmAddi.StartTime.Format("%H%M"), rmAddi.EndTime.Format("%H%M"));
		::SetDlgItemText(GetSafeHwnd(), IDC_TIMES, pclText);
		::SetDlgItemText(GetSafeHwnd(), IDC_JOB,"");
	}
	else if(omCaller == CString("STAFF"))
	{
		char pclText[100]="";
		//sprintf(pclText, "Gesuchtes Zeitintervall: %s - %s", rmAddi.StartTime.Format("%H%M"), rmAddi.EndTime.Format("%H%M"));
		sprintf(pclText, GetString(IDS_STRING61540), rmAddi.StartTime.Format("%H%M"), rmAddi.EndTime.Format("%H%M"));
		::SetDlgItemText(GetSafeHwnd(), IDC_TIMES, pclText);
		//sprintf(pclText, "Flug: %s", rmAddi.StaffText);
		sprintf(pclText, GetString(IDS_STRING61541), rmAddi.StaffText);
		::SetDlgItemText(GetSafeHwnd(), IDC_JOB, pclText);
	}
	else
	{
		char pclText[100]="";
		::SetDlgItemText(GetSafeHwnd(), IDC_TIMES, pclText);
		::SetDlgItemText(GetSafeHwnd(), IDC_JOB, pclText);
	}

	CString olCaption;
	//olCaption.Format("%d gefundene freie Mitarbeiter", pomFreeEmps->GetSize());
	olCaption.Format(GetString(IDS_STRING61310), pomFreeEmps->GetSize());
	SetWindowText(olCaption);

	// fill function list
	CListBox *pListBox = (CListBox *)GetDlgItem(IDC_FUNCTION_LIST);
	if (pListBox)
	{
		if (rmAddi.Functions.GetSize())
		{
			for (int i = 0; i < rmAddi.Functions.GetSize(); i++)
			{
				pListBox->AddString(rmAddi.Functions[i]);
			}
		}
		else
		{
			pListBox->ShowWindow(SW_HIDE);
			CWnd *pWnd = GetDlgItem(IDC_FUNCTIONS);
			if (pWnd)
				pWnd->ShowWindow(SW_HIDE);
		}
	}

	// set team allocation
	if (rmAddi.TeamAllocation)
	{
		bmTeamAlloc = true;
		UpdateData(FALSE);
	}
	else
	{
		CWnd *pWnd = GetDlgItem(IDC_TEAM_ALLOC);
		if (pWnd)
			pWnd->ShowWindow(SW_HIDE);
	}

	// resize table window
	if (pomFreeEmps)
	{
		int ilHeight = pomFreeEmps->GetSize() * pomFreeEmpTable->imItemHeight;
		int ilMaxHeight = int(::GetSystemMetrics(SM_CYSCREEN) * 0.7e0);		
		// height must not be greater than 70 % of screen height
		int ilNewHeight = min(ilHeight,ilMaxHeight);
		// must we resize ?
		if (ilNewHeight > ilHeight)
		{
			CRect olRect;
			GetWindowRect(olRect);
			int ilYResize = ilNewHeight - olRect.Height();
			olRect.bottom = olRect.top + ilNewHeight;
			MoveWindow(olRect);			

			// move the OK button
			CWnd *pWnd = GetDlgItem(IDOK);
			if (pWnd && ::IsWindow(pWnd->GetSafeHwnd()))
			{
				pWnd->GetWindowRect(olRect);
				ScreenToClient(&olRect);
				CPoint olPt(0,0);
				olPt.y += ilYResize;
				olRect += olPt;
				pWnd->MoveWindow(olRect);
			}

			// move the CANCEL button
			pWnd = GetDlgItem(IDCANCEL);
			if (pWnd && ::IsWindow(pWnd->GetSafeHwnd()))
			{
				pWnd->GetWindowRect(olRect);
				ScreenToClient(&olRect);
				CPoint olPt(0,0);
				olPt.y += ilYResize;
				olRect += olPt;
				pWnd->MoveWindow(olRect);
			}


			// resize the free employee table
			if (pomFreeEmpTable && ::IsWindow(pomFreeEmpTable->GetSafeHwnd()))
			{
				pomFreeEmpTable->GetWindowRect(olRect);
				ScreenToClient(&olRect);
				olRect.InflateRect(0,0,0,ilYResize);
			    pomFreeEmpTable->SetTableData(this, olRect.left,olRect.right,olRect.top, olRect.bottom);
			}

		}
	}


	// in dependency to the parent we must move the window
	if(pomParentRect != NULL)
	{
		CenterWindow();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


LONG FreeEmps::OnTableRButtonDown(UINT ipItem, LONG lpLParam)

{
	return 0L;
}

LONG FreeEmps::OnTableDragBegin(UINT wParam, LONG lParam)
{
//perhaps there will be drag & drop in futur times ??
/*	if(omCaller == CString("GATE"))
	{
		CListBox *pListBox = (CListBox *)lParam;
		int nMaxItems;
		if ((nMaxItems = pListBox->GetSelCount()) == LB_ERR)
			return 0L;

		int *rgIndex = new int [nMaxItems];
		pListBox->GetSelItems(nMaxItems, rgIndex);


		omDragDropObject.CreateDWordData(DIT_DUTYBAR, nMaxItems);
		for (int i = 0; i < nMaxItems; i++)
		{
			FREEEMPS_LINEDATA *prlLine = (FREEEMPS_LINEDATA *)pomFreeEmpTable->GetTextLineData(rgIndex[i]);
			omDragDropObject.AddDWord(prlLine->Urno);
		}
		delete rgIndex;

		omDragDropObject.BeginDrag();
	    return 0L;
	}
	else if(omCaller == CString("CCI"))
	{
		int ilItemID = pomFreeEmpTable->GetCurrentLine();
		FREEEMPS_LINEDATA *prlLine = (FREEEMPS_LINEDATA *)pomFreeEmpTable->GetTextLineData(ilItemID);
		omDragDropObject.CreateDWordData(DIT_DUTYBAR, 1);
		long llJobUrno = prlLine->Urno;
		omDragDropObject.AddDWord(llJobUrno);
		omDragDropObject.BeginDrag();
	}
	else if(omCaller == CString("STAFF"))
	{
	}
	else
	{
		return 0L;
	}
	*/
    return 0L;
}

void FreeEmps::OnOK() 
{
	UpdateData(TRUE);

	omDutyUrnos.RemoveAll();
	CListBox *polListBox = pomFreeEmpTable->GetCTableListBox();
	int nMaxItems, i;
	if (pomFreeEmpTable->imSelectMode & (LBS_MULTIPLESEL| LBS_EXTENDEDSEL))	// multiple selection? 
	{
		if ((nMaxItems = polListBox->GetSelCount()) == LB_ERR)
			return;
		int *rgIndex = new int [nMaxItems];
		polListBox->GetSelItems(nMaxItems, rgIndex);
		for ( i = 0; i < nMaxItems; i++)
		{
			FREEEMPS_LINEDATA *prlLine = (FREEEMPS_LINEDATA *)pomFreeEmpTable->GetTextLineData(rgIndex[i]);
			if(prlLine != NULL)
			{
				omDutyUrnos.Add(prlLine->Urno);
			}
		}
		delete rgIndex;
	}
	else	//  single Selection Listbox
	{
		i = polListBox->GetCurSel ();
		if ( i<0 )
			return;  //  nothing selected
		FREEEMPS_LINEDATA *prlLine = (FREEEMPS_LINEDATA *)pomFreeEmpTable->GetTextLineData(i);
		if(prlLine != NULL)
		{
			omDutyUrnos.Add(prlLine->Urno);
		}
	}	
	CDialog::OnOK();
}

void FreeEmps::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
}

LONG FreeEmps::OnTableLButtonDblclk(UINT wParam, LONG lParam)
{
	FreeEmps::OnOK();
	return 0L;
}

int FreeEmps::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}

void FreeEmps::UpdateComboBox()
{
	// check if the view box is enabled and displayed depending on the status in BDPSSEC
	CComboBox *polCB = (CComboBox *) GetDlgItem(IDC_VIEW);
	if(polCB != NULL && !ogBasicData.IsHidden("AVAILABLE_EMPS_DLG IDC_VIEW"))
	{
		polCB->ResetContent();
		CStringArray olStrArr;
		pomViewer->GetViews(olStrArr);
		for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
		{
			polCB->AddString(olStrArr[ilIndex]);
		}
		CString olViewName = pomViewer->GetViewName();
		if (olViewName.IsEmpty() == TRUE)
		{
			olViewName = ogCfgData.rmUserSetup.STCV;
		}

		ilIndex = polCB->FindString(-1,olViewName);
			
		if (ilIndex != CB_ERR)
		{
			polCB->SetCurSel(ilIndex);
		}
	}
}

void FreeEmps::UpdateView() 
{
	char pclViewName[200];
	strcpy(pclViewName,GetString(IDS_STRINGDEFAULT));

	// check if the view box is enabled and displayed depending on the status in BDPSSEC
    CComboBox *polCB = (CComboBox *)GetDlgItem(IDC_VIEW);
	if(polCB != NULL && !ogBasicData.IsHidden("AVAILABLE_EMPS_DLG IDC_VIEW"))
	{
		polCB->GetLBText(polCB->GetCurSel(), pclViewName);
	}

	AfxGetApp()->DoWaitCursor(1);
	pomViewer->UpdateView(pclViewName);
	AfxGetApp()->DoWaitCursor(-1);
}
