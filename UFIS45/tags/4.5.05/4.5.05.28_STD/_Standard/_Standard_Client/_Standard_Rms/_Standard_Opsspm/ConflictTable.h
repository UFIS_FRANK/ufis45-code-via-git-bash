// ConflictTable.h : header file
//
#ifndef _CCFTABLE_H
#define _CCFTABLE_H

#include <ConflictViewer.h>

//enum enumConflictTableType{CFLTABLE_CONFLICT,CFLTABLE_ATTENTION};

extern enum enumConflictTableType egConflictTableType;

/////////////////////////////////////////////////////////////////////////////
// ConflictTable dialog

class ConflictTable : public CDialog
{
// Construction
public:
//	ConflictTable(CWnd* pParent = NULL);   // standard constructor
	ConflictTable(int ipTableType, CWnd* pParent = NULL);   // standard constructor
	ConflictTable(int ipTableType, ConflictTableViewer *popViewer,CWnd* pParent = NULL);
	~ConflictTable();
	void UpdateView();

	int m_nDialogBarHeight;
	void SetViewerDate();
	void HandleGlobalDateUpdate();

private :
    CTable *pomTable; // visual object, the table content
	void SetTimeband(CONFLICTTABLE_LINEDATA *prpLine);

private:
	int						bmTableType;
	ConflictTableViewer*	pomViewer;
	BOOL					bmIsViewerCreated;
	BOOL					bmIsViewOpen;
	CRect                   omWindowRect; //PRF 8712

	void UpdateComboBox();
	void UpdateDisplay();

// Dialog Data
	//{{AFX_DATA(ConflictTable)
	enum { IDD = IDD_CONFLICTTABLE };
	CComboBox	m_Date;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ConflictTable)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ConflictTable)
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMove(int x, int y); //PRF 8712
	afx_msg void OnView();
	afx_msg void OnSelchangeViewcombo();
	afx_msg void OnCloseupViewcombo();
	afx_msg void OnDestroy();
	afx_msg void OnClose();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
    afx_msg LONG OnTableSelectionChanged(UINT wParam, LONG lParam);
	afx_msg void OnSelchangeDate();
	afx_msg void OnTableUpdateDataCount(); //Singapore
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CString Format(CONFLICTENTRY *prpCfltEntry,int ipCflIndex);
};

#endif
