#ifndef _COVERAGECOLORDLG
#define _COVERAGECOLORDLG

#include <Coverage.h>


#define TOTALCOLORITEMS	13


// There is no reason to derive a class from CObject because in this specific
// sample application there will be no serialization.  
class CColorDialogItem
{
public:
	// define the enum with values to match whatever DLGITEMTEMPLATE requires

	DLGITEMTEMPLATE  m_dlgItemTemplate;

	enum			controltype {BUTTON = 0x0080, EDITCONTROL = 0x0081, STATICTEXT = 0x0082,
									 LISTBOX = 0x0083, SCROLLBAR = 0x0084, COMBOBOX = 0x0085};

	controltype		m_controltype;
	CString			m_strCaption;

public:
	CColorDialogItem(enum controltype cType);  // default constructor will fill in default values
	CColorDialogItem() {};  // default constructor, not to be called directly

	void Initialize(enum controltype cType, UINT nID, CRect* prect = NULL, LPCTSTR pszCaption = NULL, BOOL blIsPassWord = FALSE);
};

class CColorRessLessDlg : public CDialog
{
public:
	CColorRessLessDlg(CCoverage *popCoverage, CCoverageGraphic *popCoverageGraphic);
	BOOL InitModalIndirect(DLGTEMPLATE* popDlgTemplate);

protected:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CColorRessLessDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CColorRessLessDlg)
	afx_msg void OnPaint();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnTakeColor();
	afx_msg void OnSelchangeColorcombo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CCoverageGraphic *pomCoverageGraphic;
	//Attributes
public:
	HICON			m_hIcon;
	BOOL			blIsInitialized;
	CCoverage	*pomCoverage;

	// Dialog Data
	//{{AFX_DATA(CColorRessLessDlg)
	CComboBox	m_DescrCombo;
	CComboBox	m_CurveCombo;
	CComboBox	m_ColorCombo;
	//}}AFX_DATA

protected:

private:
	CString	omNewColor;
	int		imNewColor;

};

class CCoverageColorDlg// : public CDialog 
{
// Construction
public:
    CCoverageColorDlg(CWnd* pParent = NULL);     // standard constructor

	DLGTEMPLATE m_dlgTempl;
	CColorDialogItem	m_rgDlgItem[TOTALCOLORITEMS];  // the 3 controls to be inserted

	void MakeDialog(const char *pspCaption, CCoverage *popCoverage);
// Implementation
protected:

};
#endif
