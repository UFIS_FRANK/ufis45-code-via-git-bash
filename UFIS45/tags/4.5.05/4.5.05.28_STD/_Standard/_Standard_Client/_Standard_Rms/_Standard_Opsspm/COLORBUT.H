// colorbut.h : header file
//
#ifndef _CCOLORBUTTON_H_
#define _CCOLORBUTTON_H_

/////////////////////////////////////////////////////////////////////////////
// CColorButton window

class CColorButton : public CButton
{
// Construction
public:
    CColorButton();

// Attributes
public:

// Operations
public:

// Overrides

// Implementation
public:
    virtual ~CColorButton();
    virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
    
    // Generated message map functions
protected:
    //{{AFX_MSG(CColorButton)
    afx_msg LONG OnSetCheck (UINT wParam, LONG lParam);
    afx_msg LONG OnSetStyle (UINT wParam, LONG lParam);
    afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
#endif _CCOLORBUTTON_H_