// FmDv.cpp : implementation file
//
//
// Modification History:
// 19-Sep-96	Damkerng	Change the GetFmJobsByPrid() to be GetFmgJobsByAlid().
//							WARNING!! From now on Flight Manager Detail Window
//							will work only with a gate area or a CCI area, not with a
//							pool any more. In other word, our program will look
//							for jobs type FMJ only.

#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSPtrArray.h>
#include <cviewer.h>
#include <table.h>
#include <CedaShiftData.h>
#include <CedaJobData.h>
#include <CedaFlightData.h>
#include <CedaEmpData.h>
#include <ccsddx.h>
#include <CedaAloData.h>
#include <BasicData.h>
#include <FlightMgrDetailViewer.h>
#include <dataset.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// General macros for testing a point against an interval

// Local function prototype
static void FmDetailCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

FlightMgrDetailViewer::FlightMgrDetailViewer()
{
    pomTable = NULL;
    ogCCSDdx.Register(this, JOB_NEW, CString("FMDV"), CString("Job New"), FmDetailCf);
    ogCCSDdx.Register(this, JOB_DELETE, CString("FMDV"), CString("Job Delete"), FmDetailCf);
}

FlightMgrDetailViewer::~FlightMgrDetailViewer()
{
    ogCCSDdx.UnRegister(this, NOTUSED);
    DeleteAll();
}

void FlightMgrDetailViewer::Attach(CTable *popTable,CTime opStartTime,CTime opEndTime,CString pcpAlid)
{
    pomTable = popTable;
	pcmAlid = pcpAlid;
	omStartTime = opStartTime;
	omEndTime = opEndTime;

}

BOOL FlightMgrDetailViewer::IsPassFilter(CTime opAcfr,CTime opActo)
{
	BOOL blIsTimeOk = (opAcfr < omEndTime && opActo > omStartTime);

    return (blIsTimeOk);
}

/////////////////////////////////////////////////////////////////////////////
// FlightMgrDetailViewer -- code specific to this class

void FlightMgrDetailViewer::MakeLines()
{
	CCSPtrArray<JOBDATA> olFmgJobs;
	ogJobData.GetFmgJobsByAlid(olFmgJobs, pcmAlid);
		// WARNING: this "pcmAlid" must be a gate area or a CCI area only
	int ilFmgCount = olFmgJobs.GetSize();
	JOBDATA olFmgJob;
	CString olText;
	for (int ilLc = 0; ilLc < ilFmgCount; ilLc++)
	{
		MakeLine(&olFmgJobs[ilLc]);
	}
}

void FlightMgrDetailViewer::MakeAssignments()
{
    POSITION rlPos;
    for ( rlPos = ogJobData.omUrnoMap.GetStartPosition(); rlPos != NULL; )
    {
        long llUrno;
        JOBDATA *prlJob;
        ogJobData.omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlJob);

	    // If it's not a special flightmanager job, ignore it
		if (strcmp(prlJob->Jtco,JOBFM) != 0)
			continue;

		FLIGHTDATA *prlFlight = ogBasicData.GetFlightForJob(prlJob);
		if (prlFlight != NULL)
		{
			FMDW_ASSIGNMENT rlAssignment;
			rlAssignment.Fnum = CString(prlFlight->Fnum);
			rlAssignment.JobUrno = prlJob->Urno;
			rlAssignment.Acfr = prlJob->Acfr;

			int ilLineno;
			if (FindLine(prlJob->Peno, ilLineno))  // corresponding shift found?
			{   // (for some reason, we have saved SHFCKI.URNO in JOBCKI.FLUR)
				CreateAssignment(ilLineno, &rlAssignment);
			}

	    }
	}
}

void FlightMgrDetailViewer::MakeLine(JOBDATA *prpJob)
{

	if ((prpJob->Acfr < omEndTime) && (prpJob->Acto > omStartTime))
	{
		SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prpJob->Shur);
		EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prpJob->Peno);

		FMDW_LINEDATA rlLine;

		rlLine.ShiftUrno = prlShift != NULL ? prlShift->Urno : 0L;
		rlLine.DutyUrno = prpJob->Jour;  // Duty job Urno
		rlLine.Peno   = CString(prpJob->Peno);
		rlLine.Name  = CString(prlEmp != NULL ? prlEmp->Lanm : "");
		rlLine.Tmid  = prlShift != NULL ? prlShift->Egrp : "";
		rlLine.Sfca  = prlShift != NULL ? prlShift->Sfca : "";
		rlLine.Acfr = prpJob->Acfr;
		rlLine.Acto = prpJob->Acto;

        CreateLine(rlLine);
    }
}

BOOL FlightMgrDetailViewer::FindLine(CString opPkno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
        if (opPkno == omLines[rilLineno].Peno)
            return TRUE;
    return FALSE;
}


/////////////////////////////////////////////////////////////////////////////
// FlightMgrDetailViewer - FMDETAIL_LINEDATA array maintenance

void FlightMgrDetailViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

int FlightMgrDetailViewer::CompareLine(FMDW_LINEDATA &rrpLine1,FMDW_LINEDATA &rrpLine2)
{
	int ilCompareResult = (rrpLine1.Name == rrpLine2.Name)? 0:
                (rrpLine1.Name > rrpLine2.Name)? 1: -1;
	return ilCompareResult;
}

int FlightMgrDetailViewer::CreateLine(FMDW_LINEDATA &rrpLine)
{
    int ilLineCount = omLines.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
    // Search for the position which we want to insert this new bar
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareLine(rrpLine, omLines[ilLineno-1]) >= 0)
            break;  // should be inserted after Lines[ilLineno-1]

    omLines.NewAt(ilLineno, rrpLine);

    return ilLineno;
}

void FlightMgrDetailViewer::DeleteLine(int ipLineno)
{
	if (ipLineno < omLines.GetSize())
	{
		while (omLines[ipLineno].Assignment.GetSize() > 0)
			DeleteAssignment(ipLineno, 0);

		omLines.DeleteAt(ipLineno);
	}
}

int FlightMgrDetailViewer::CreateAssignment(int ipLineno, FMDW_ASSIGNMENT *prpAssignment)
{
    int ilAssignmentCount = omLines[ipLineno].Assignment.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
    // Search for the position which we want to insert this new bar
    for (int ilAssignmentno = ilAssignmentCount; ilAssignmentno > 0; ilAssignmentno--)
        if (prpAssignment->Acfr >= omLines[ipLineno].Assignment[ilAssignmentno-1].Acfr)
            break;  // should be inserted after Lines[ilAssignmentno-1]
 
	omLines[ipLineno].Assignment.NewAt(ilAssignmentno, *prpAssignment);

    return ilAssignmentno;
}


void FlightMgrDetailViewer::DeleteAssignmentJob(long lpJobUrno)
{
	JOBDATA *prlJob = ogJobData.GetJobByUrno(lpJobUrno);
	if (prlJob != NULL)
	{
		if (CString(prlJob->Jtco) == JOBFM)
		{
			ogDataSet.DeleteJob(pomTable, lpJobUrno);
		}
	}
}

void FlightMgrDetailViewer::DeleteAssignment(int ipLineno, int ipAssignmentno)
{
	if (ipLineno < omLines.GetSize())
	{
		if (omLines[ipLineno].Assignment.GetSize() > ipAssignmentno)
		{
		//	DeleteAssignmentJob(omLines[ipLineno].Assignment[ipAssignmentno].JobUrno);
			omLines[ipLineno].Assignment.DeleteAt(ipAssignmentno);
		}
	}
}


/////////////////////////////////////////////////////////////////////////////
// FlightMgrDetailViewer - display drawing routine

// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"
void FlightMgrDetailViewer::UpdateDisplay()
{
    DeleteAll();    // remove everything
    MakeLines();
    MakeAssignments();

    // Set table header and format
    //pomTable->SetHeaderFields("Name|Team|Sft|Von|Bis|Flug|Flug|Flug|Flug|Flug|Flug|Flug|Flug|Flug|Flug|");
    pomTable->SetHeaderFields(GetString(IDS_STRING61563));
    pomTable->SetFormatList("20|4|4|5|5|9|9|9|9|9|9|9|9|9|9|");

    // Load filtered and sorted data into the table content
    pomTable->ResetContent();
    for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
    {
        pomTable->AddTextLine(Format(&omLines[ilLc]), &omLines[ilLc]);
    }

    // Update the table content in the display
    pomTable->DisplayTable();
}

CString FlightMgrDetailViewer::Format(FMDW_LINEDATA *prpLine)
{
    CString olText = CString(prpLine->Name) + "|"
        + prpLine->Tmid + "|"
        + prpLine->Sfca + "|"
		+ prpLine->Acfr.Format("%H:%M") + "|"
		+ prpLine->Acto.Format("%H:%M") + "|";

    for (int ilLc = 0; ilLc < prpLine->Assignment.GetSize(); ilLc++)
    {
        FMDW_ASSIGNMENT *prlAssignment = &prpLine->Assignment[ilLc];
        olText += "|" + prlAssignment->Fnum;
    }
    return olText;
}


/////////////////////////////////////////////////////////////////////////////
// FlightMgrDetailViewer Jobs creation methods

static void FmDetailCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    FlightMgrDetailViewer *polViewer = (FlightMgrDetailViewer *)popInstance;

    if (ipDDXType == JOB_NEW)
        polViewer->ProcessJobNew((JOBDATA *)vpDataPointer);
	else
    if (ipDDXType == JOB_DELETE)
        polViewer->ProcessJobDelete((JOBDATA *)vpDataPointer);

}


BOOL FlightMgrDetailViewer::FindItemByUrno(JOBDATA *prpJob,int& ripItem, int& ripAssignment)
{
	int ilCount = omLines.GetSize();
    for (int ilLc = 0; ilLc < ilCount; ilLc++)
    {
		int ilJobCount = omLines[ilLc].Assignment.GetSize();
		for ( int ilJc = 0; ilJc < ilJobCount; ilJc++)
		{
			if (omLines[ilLc].Assignment[ilJc].JobUrno == prpJob->Urno)
			{
				ripAssignment = ilJc;
				ripItem = ilLc;
				return TRUE;
			}
		}
	}
	return FALSE;
}

void FlightMgrDetailViewer::ProcessJobDelete(JOBDATA *prpJob)
{
	// If it's not a special flightmanager job, ignore it
	if (strcmp(prpJob->Jtco,JOBFM) != 0)
		return;

	int ilItem;
	int ilAssignment;
	if (FindItemByUrno(prpJob,ilItem,ilAssignment))
	{
		{
			DeleteAssignment(ilItem,ilAssignment);
			pomTable->ChangeTextLine(ilItem, Format(&omLines[ilItem]), &omLines[ilItem]);
		}
	}
}

void FlightMgrDetailViewer::ProcessJobNew(JOBDATA *prpJob)
{
	// If it's not a special flightmanager job, ignore it
	if (strcmp(prpJob->Jtco,JOBFM) != 0)
		return;

	// Prepare shift URNO and displayed text for this assignment
	long llShiftUrno;

	llShiftUrno = prpJob->Jour;
	FLIGHTDATA *prlFlight = ogBasicData.GetFlightForJob(prpJob);

    // Create a new assignment
    BOOL blHasToUpdateLine = FALSE;
    FMDW_ASSIGNMENT rlAssignment;
    rlAssignment.Fnum = (prlFlight == NULL)? "ERR": prlFlight->Fnum;
    rlAssignment.FlightUrno = (prlFlight == NULL)? 0L : prlFlight->Urno;
	rlAssignment.JobUrno = prpJob->Urno;
	rlAssignment.Acfr = prpJob->Acfr;
    int ilLineno;
    if (FindLine(prpJob->Peno, ilLineno))  // corresponding line found?
	{
        CreateAssignment(ilLineno, &rlAssignment);
        blHasToUpdateLine = TRUE;
    }

    // Update the table window if necessary
    if (blHasToUpdateLine)
    {
        pomTable->ChangeTextLine(ilLineno, Format(&omLines[ilLineno]), &omLines[ilLineno]);
	}
}

