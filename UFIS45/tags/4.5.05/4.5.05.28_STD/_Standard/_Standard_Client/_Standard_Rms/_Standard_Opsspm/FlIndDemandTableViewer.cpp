// flindemandtableviewer.cpp -- the viewer for Flinddemand Table
 
#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <CedaSerData.h>
#include <CedaRudData.h>
#include <CedaDemandData.h>
#include <CCSCedaData.h>
#include <OpssPm.h>
#include <table.h>
#include <CedaEmpData.h>
#include <PrintControl.h>
#include <FlIndDemandTableViewer.h>
#include <BasicData.h>
#include <DataSet.h>
#include <ccsddx.h>
#include <CedaPfcData.h>
#include <CedaRpqData.h>
#include <CedaPerData.h>
#include <CedaReqData.h>
#include <FlIndDemandTable.h> //Singapore

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// Grouping and Sorting criterias definition:
// This will make the program faster a little bit because it will replace string comparison.
enum {
    BY_NAME,
	BY_CODE,
	BY_DEBE,
	BY_DEEN,
	BY_DEDU,
	BY_NONE
};

// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsWithIn(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))

/////////////////////////////////////////////////////////////////////////////
// FlIndDemandTableViewer

FlIndDemandTableViewer::FlIndDemandTableViewer()
{
    SetViewerKey("FidTab");
    pomTable = NULL;
    ogCCSDdx.Register(this, JOB_NEW, CString("FIDVIEWER"), CString("Job New"), FlIndDemandTableCf);
    ogCCSDdx.Register(this, JOB_CHANGE, CString("FIDVIEWER"), CString("Job Change"), FlIndDemandTableCf);
    ogCCSDdx.Register(this, JOB_DELETE, CString("FIDVIEWER"), CString("Job Delete"), FlIndDemandTableCf);
    ogCCSDdx.Register(this, JOD_NEW, CString("FIDVIEWER"), CString("Jod New"), FlIndDemandTableCf);
    ogCCSDdx.Register(this, JOD_CHANGE, CString("FIDVIEWER"), CString("Jod Change"), FlIndDemandTableCf);
    ogCCSDdx.Register(this, DEMAND_NEW, CString("FIDVIEWER"), CString("Demand New"), FlIndDemandTableCf);
    ogCCSDdx.Register(this, DEMAND_CHANGE, CString("FIDVIEWER"), CString("Demand Change"), FlIndDemandTableCf);
    ogCCSDdx.Register(this, DEMAND_DELETE, CString("FIDVIEWER"), CString("Demand Delete"), FlIndDemandTableCf);

	CTime olDay = ogBasicData.GetTimeframeStart();
	omStartTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),0,0,0);
	omEndTime = CTime(olDay.GetYear(),olDay.GetMonth(),olDay.GetDay(),23,59,59);

	bmUseAllNames = false;
	bmUseAllCodes = false;
}

FlIndDemandTableViewer::~FlIndDemandTableViewer()
{
	TRACE("FlIndDemandTableViewer: DDX Unregistration \n");
    ogCCSDdx.UnRegister(this, NOTUSED);

	DeleteAll();
}

void FlIndDemandTableViewer::Attach(CTable *popTable)
{
	pomTable = popTable;
}

void FlIndDemandTableViewer::ChangeViewTo(const char *pcpViewName, CString opDate)
{
	omDate = opDate;
	ChangeViewTo(pcpViewName);
}

void FlIndDemandTableViewer::ChangeViewTo(const char *pcpViewName)
{
	ogCfgData.rmUserSetup.GBLV = pcpViewName;
    SelectView(pcpViewName);
    PrepareFilter();
    PrepareSorter();

    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

void FlIndDemandTableViewer::PrepareFilter()
{
    CStringArray olFilterValues;
    int i, n;

    GetFilter("Name", olFilterValues);
    omCMapForName.RemoveAll();
	bmUseAllNames = false;
    n = olFilterValues.GetSize();
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllNames = true;
		else
		{
			for (i = 0; i < n; i++)
				omCMapForName[olFilterValues[i]] = NULL;
		}
	}

    GetFilter("Code", olFilterValues);
    omCMapForCode.RemoveAll();
	bmUseAllCodes = false;
    n = olFilterValues.GetSize();
	if (n > 0)
	{
		if (olFilterValues[0] == GetString(IDS_ALLSTRING))
			bmUseAllCodes = true;
		else
		{
			for (i = 0; i < n; i++)
				omCMapForCode[olFilterValues[i]] = NULL;
		}
	}

    GetFilter("Dety", olFilterValues);
	imDemandTypes = NODEMANDS;
    n = olFilterValues.GetSize();
	if(n > 0)
	{
		if(olFilterValues[0] == GetString(IDS_ALLSTRING))
		{
			imDemandTypes |= PERSONNELDEMANDS;
			imDemandTypes |= EQUIPMENTDEMANDS;
			//imDemandTypes |= LOCATIONDEMANDS;
		}
		else
		{
			for (i = 0; i < n; i++)
			{
				if(!strcmp(olFilterValues[i],GetString(IDS_PERSONNEL_DEM)))
				{
					imDemandTypes |= PERSONNELDEMANDS;
				}
				else if(!strcmp(olFilterValues[i],GetString(IDS_EQUIPMENT_DEM)))
				{
					imDemandTypes |= EQUIPMENTDEMANDS;
				}
//				else if(!strcmp(olFilterValues[i],GetString(IDS_LOCATION_DEM)))
//				{
//					imDemandTypes |= LOCATIONDEMANDS;
//				}
			}
		}
	}
}		

void FlIndDemandTableViewer::PrepareSorter()
{
    CStringArray olSortOrder;
    CViewer::GetSort(olSortOrder);

	bmSortDemStartAscending = GetUserData("DEMSTARTORDER") == "YES" ? true : false;
	bmSortDemEndAscending = GetUserData("DEMENDORDER") == "YES" ? true : false;

	bmSortDemTerminalAscending = GetUserData("DEMTERMINALORDER") == "YES" ? true : false;

    omSortOrder.RemoveAll();
    for (int i = 0; i < olSortOrder.GetSize(); i++)
    {
        int ilSortOrderEnumeratedValue;
        if (olSortOrder[i] == "Name")
            ilSortOrderEnumeratedValue = BY_NAME;
        else if (olSortOrder[i] == "Code")
            ilSortOrderEnumeratedValue = BY_CODE;
        else if (olSortOrder[i] == "Debe")
            ilSortOrderEnumeratedValue = BY_DEBE;
        else if (olSortOrder[i] == "Deen")
            ilSortOrderEnumeratedValue = BY_DEEN;
        else if (olSortOrder[i] == "Dedu")
            ilSortOrderEnumeratedValue = BY_DEDU;
		else
            ilSortOrderEnumeratedValue = BY_NONE;

        omSortOrder.Add(ilSortOrderEnumeratedValue);
    }
}

int FlIndDemandTableViewer::CompareDem(FLINDEM_LINE *prpDem1, FLINDEM_LINE *prpDem2)
{
    int n = omSortOrder.GetSize();
    for (int i = 0; i < n; i++)
    {
        int ilCompareResult = 0;

        switch (omSortOrder[i])
        {
        case BY_NAME:
            ilCompareResult = (prpDem1->Name == prpDem2->Name)? 0:
                (prpDem1->Name > prpDem2->Name)? 1: -1;
            break;
        case BY_CODE:
            ilCompareResult = (prpDem1->Code == prpDem2->Code)? 0:
                (prpDem1->Code > prpDem2->Code)? 1: -1;
            break;
        case BY_DEBE:
			ilCompareResult = (bmSortDemStartAscending) ? 
				prpDem1->From.GetTime() - prpDem2->From.GetTime() :
				prpDem2->From.GetTime() - prpDem1->From.GetTime();
            break;
        case BY_DEEN:
			ilCompareResult = (bmSortDemEndAscending) ? 
				prpDem1->Until.GetTime() - prpDem2->Until.GetTime() :
				prpDem2->Until.GetTime() - prpDem1->Until.GetTime();
            break;
		case BY_DEDU:
            ilCompareResult = (prpDem1->Duration == prpDem2->Duration)? 0:
                (prpDem1->Duration > prpDem2->Duration)? 1: -1;
			break;
        }
        if (ilCompareResult != 0)
            return ilCompareResult;
    }


	// if all sort params are identical then the order depends on the URNO
	// - do this otherwise as updates arrive, identical demands are constantly moved
	// around within groups of identical demands
	return  prpDem1->DemandUrno - prpDem2->DemandUrno;
}


BOOL FlIndDemandTableViewer::IsPassFilter(const char *pcpName, const char *pcpCode, DEMANDDATA *prpDem)
{
    void *p;
    BOOL blIsNameOk = bmUseAllNames ? true : omCMapForName.Lookup(CString(pcpName), p);
    BOOL blIsCodeOk = bmUseAllCodes ? true : omCMapForCode.Lookup(CString(pcpCode), p);
    BOOL blIsDetyOk = ogDemandData.DemandIsOfType(prpDem, imDemandTypes);

	BOOL blIsServiceCodeOk = TRUE;
	//Singapore
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		if((omTerminal == FlIndDemandTable::Terminal2) && strncmp(pcpCode,FlIndDemandTable::Terminal2,(FlIndDemandTable::Terminal2).GetLength()) != 0)
		{
			blIsServiceCodeOk = FALSE;
		}
		else if((omTerminal == FlIndDemandTable::Terminal3) && strncmp(pcpCode,FlIndDemandTable::Terminal2,(FlIndDemandTable::Terminal2).GetLength()) == 0)
		{
			blIsServiceCodeOk = FALSE;
		}
	}
    return (blIsNameOk && blIsCodeOk && blIsDetyOk && blIsServiceCodeOk); //Added blIsServiceCodeOk Singapore
}

/////////////////////////////////////////////////////////////////////////////
// CciTableViewer -- code specific to this class

void FlIndDemandTableViewer::MakeLines()
{
	DeleteAll();

	CCSPtrArray <DEMANDDATA> olDemands;
	ogDemandData.GetDemandsByObty(olDemands, "FID");
	int ilNumDems = olDemands.GetSize();
	for(int ilD = 0; ilD < ilNumDems; ilD++)
	{
		MakeLine(&olDemands[ilD]);
	}

}

int FlIndDemandTableViewer::MakeLine(DEMANDDATA *prpDem)
{
	int ilLine = -1;
	FLINDEM_LINE olLine;
	if(FormatLine(olLine, prpDem))
	{
		ilLine = CreateLine(&olLine);
	}

	return ilLine;
}

bool FlIndDemandTableViewer::FormatLine(FLINDEM_LINE &ropLine, DEMANDDATA *prpDem)
{
	RUDDATA* prlRud = ogRudData.GetRudByUrno(prpDem->Urud);
	if (!prlRud)
		return false;

	SERDATA* prlSer = ogSerData.GetSerByUrno(prlRud->Ughs);
	if (!prlSer)
		return false;

	if (!IsReallyOverlapped(prpDem->Debe,prpDem->Deen,omStartTime,omEndTime))
		return false;

	if (!IsPassFilter(prlSer->Snam, prlSer->Seco, prpDem))
		return false;

	ropLine.DemandUrno = prpDem->Urno;
	ropLine.Name = prlSer->Snam;
	ropLine.Code = prlSer->Seco;
	ropLine.Dety = ogDemandData.GetDemandTypeString(prpDem);
	ropLine.Function = GetFunctionForDemand(prpDem);
	ropLine.Permit = GetPermitForDemand(prpDem);
	ropLine.From = prpDem->Debe;
	ropLine.Until = prpDem->Deen;
	ropLine.Duration = prpDem->Deen - prpDem->Debe;
	ropLine.InvalidDemand = ogDemandData.HasExpired(prpDem);

	ropLine.Jobs.RemoveAll();
	CCSPtrArray <JOBDATA> olJobs;
	if(ogJodData.GetJobsByDemand(olJobs,prpDem->Urno))
	{
		int ilNumJobs = olJobs.GetSize();
		for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
		{
			ropLine.Jobs.Add(GetAssignmentStringForJob(&olJobs[ilJ]));
		}
		if(ilNumJobs > imJobCount)
		{
			imJobCount = ilNumJobs;
		}
	}

	return true;
}

CString FlIndDemandTableViewer::GetAssignmentStringForJob(JOBDATA *prpJob)
{
	CString olTxt;
	if(prpJob != NULL)
	{
		if(prpJob->Uequ != 0L)
		{
			EQUDATA *prlEqu = ogEquData.GetEquByUrno(prpJob->Uequ);
			if(prlEqu != NULL)
			{
				olTxt.Format("%s-%s %s", ogBasicData.FormatDate(prpJob->Acfr, omStartTime), ogBasicData.FormatDate(prpJob->Acto, omStartTime), prlEqu->Enam);
			}
		}
		else
		{	
			char pclTmpText[512];
			char pclConfigPath[512];
			if (getenv("CEDA") == NULL)
			{	
				strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
			}
			else
			{
				strcpy(pclConfigPath, getenv("CEDA"));
			}
			
			GetPrivateProfileString(pcgAppName, "FLIND_DEMANDLIST_REMARK",  "NO",pclTmpText, sizeof pclTmpText, pclConfigPath);
			if (!strcmp(pclTmpText,"YES")) {
				olTxt.Format("%s-%s %s %s", ogBasicData.FormatDate(prpJob->Acfr, omStartTime), ogBasicData.FormatDate(prpJob->Acto, omStartTime), ogEmpData.GetEmpName(prpJob->Ustf),ogJobData.GetJobPoolByUrno(prpJob->Urno)->Text);
			}
			else
			{
				olTxt.Format("%s-%s %s", ogBasicData.FormatDate(prpJob->Acfr, omStartTime), ogBasicData.FormatDate(prpJob->Acto, omStartTime), ogEmpData.GetEmpName(prpJob->Ustf));
			}
		}
	}

	return olTxt;
}

/////////////////////////////////////////////////////////////////////////////
// FlIndDemandTableViewer - FLINDEM_LINEDATA array maintenance

void FlIndDemandTableViewer::DeleteAll()
{
	imJobCount = 4;
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

int FlIndDemandTableViewer::CreateLine(FLINDEM_LINE *prpLine)
{
    int ilLineCount = omLines.GetSize();

#ifndef SCANBACKWARD
    for (int ilLineno = 0; ilLineno < ilLineCount; ilLineno++)
        if (CompareDem(prpLine, &omLines[ilLineno]) <= 0)
            break;  
#else
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareDem(prpLine, &omLines[ilLineno-1]) >= 0)
            break;  
#endif

	//Singapore
	if(ogBasicData.IsPaxServiceT2() == true)
	{
		if(GetTerminal() == FlIndDemandTable::Terminal2)
		{
			if(prpLine->Code.Mid(0,2).CompareNoCase(FlIndDemandTable::Terminal2) != 0)
				return -1;
		}
		else if(GetTerminal() == FlIndDemandTable::Terminal3)
		{
			if(prpLine->Code.Mid(0,2).CompareNoCase(FlIndDemandTable::Terminal2) == 0)
				return -1;
		}
        omLines.NewAt(ilLineno, *prpLine);
		
	}
	else
	{
		omLines.NewAt(ilLineno, *prpLine);
	}
    return ilLineno;
}

void FlIndDemandTableViewer::DeleteLine(int ipLineno)
{
    omLines.DeleteAt(ipLineno);
}

/////////////////////////////////////////////////////////////////////////////
// FlIndDemandTableViewer - display drawing routine

void FlIndDemandTableViewer::UpdateDisplay()
{
	CString olHeaderFields = GetString(IDS_STRING_CAPTION_LIST_FID);
	CString olFormatList = "40|8|10|10|14|8|8|14";

	for(int ilJ = 0; ilJ < imJobCount; ilJ++)
	{
		olHeaderFields += CString("|") + GetString(IDS_FID_ASSIGNMENT);
		olFormatList += "|40";
	}

    pomTable->SetHeaderFields(olHeaderFields);
    pomTable->SetFormatList(olFormatList);

	pomTable->ResetContent();
	int nLineCount = 0;
	nLineCount = omLines.GetSize();
	for (int i = 0; i < nLineCount; i++) 
	{
		FLINDEM_LINE *prlLine = &omLines[i];
		pomTable->AddTextLine(Format(prlLine), prlLine);

        // Display grouping effect
        if (i == nLineCount-1)
            pomTable->SetTextLineSeparator(i, ST_THICK);

		if(prlLine->InvalidDemand)
			pomTable->SetTextLineColor(i, WHITE, RED);

	}

	pomTable->DisplayTable();
}

CString FlIndDemandTableViewer::Format(FLINDEM_LINE *prpLine)
{
	CString olDuration;
	olDuration.Format("%ld",prpLine->Duration.GetTotalMinutes());
    CString olLine;
	olLine =  prpLine->Name + "|" + prpLine->Code + "|" + prpLine->Dety + "|"
		+ prpLine->Function + "|" + prpLine->Permit + "|"
		+ ogBasicData.FormatDate(prpLine->From, omStartTime) + "|" 
		+ ogBasicData.FormatDate(prpLine->Until, omStartTime) + "|" + olDuration;

	int ilNumJobs = prpLine->Jobs.GetSize();
	for(int ilJ = 0; ilJ < ilNumJobs; ilJ++)
	{
		olLine += CString("|") + prpLine->Jobs[ilJ];
	}

    return olLine;
}

CString FlIndDemandTableViewer::GetFunctionForDemand(DEMANDDATA *prpDemand)
{
	CString olFunction;

	if(prpDemand != NULL)
	{
		if(ogRudData.DemandIsOfType(prpDemand->Urud, PERSONNELDEMANDS))
		{
			RPFDATA *prlDemFct = ogRpfData.GetFunctionByUrud(prpDemand->Urud);
			if (prlDemFct)
			{
				if (strcmp(prlDemFct->Fcco,"") == 0)
				{
					olFunction = "";
					// group of optional functions
					CCSPtrArray <SGMDATA> olSgms;
					ogSgmData.GetSgmListByUsgr(prlDemFct->Upfc,olSgms);
					int ilNumSgms = olSgms.GetSize();
					for(int ilSgm = 0; ilSgm < ilNumSgms; ilSgm++)
					{
						PFCDATA *prlPfc = ogPfcData.GetPfcByUrno(olSgms[ilSgm].Uval);
						if(prlPfc != NULL)
						{
							if (!olFunction.IsEmpty())
								olFunction += ',';
							olFunction += prlPfc->Fctc;
						}
					}
				}
				else
				{
					olFunction = prlDemFct->Fcco;
				}
			}
		}
		else if(ogRudData.DemandIsOfType(prpDemand->Urud, EQUIPMENTDEMANDS))
		{
			REQDATA *prlReq = ogReqData.GetReqByUrud(prpDemand->Urud);
			if(prlReq != NULL)
			{
				EQUDATA *prlEqu = NULL;

				if(strlen(prlReq->Eqco) > 0)
				{
					// single piece of equipment required for this demand
					if((prlEqu = ogEquData.GetEquByUrno(prlReq->Uequ)) != NULL)
					{
						olFunction = prlEqu->Enam;
					}
				}
				else
				{
					SGRDATA *prlSgr = ogSgrData.GetSgrByUrno(prlReq->Uequ);
					if(prlSgr != NULL)
					{
						olFunction = prlSgr->Grpn;
					}
				}
			}
		}
	}

	return olFunction;
}

CString FlIndDemandTableViewer::GetPermitForDemand(DEMANDDATA *prpDemand)
{
	CString olPermits;

	if(prpDemand != NULL)
	{
		// lli: need to check both urud and udgr for permits
		CCSPtrArray<RPQDATA> olDemandPermits;
		CCSPtrArray<RPQDATA> olDemandPermits2;
		ogRpqData.GetRpqsByUrud(prpDemand->Urud,olDemandPermits);
		ogRpqData.GetRpqsByUdgr(prpDemand->Udgr,olDemandPermits2);
		olDemandPermits.Append(olDemandPermits2);
		for (int ilC = 0; ilC < olDemandPermits.GetSize(); ilC++)
		{
			RPQDATA *prlDemandPermit = &olDemandPermits[ilC];
			if (prlDemandPermit)
			{
				if (strcmp(prlDemandPermit->Quco,"") == 0)
				{
					olPermits = "";
					// this is a header for a group of permits, one of which the emp must have
					CCSPtrArray <SGMDATA> olSgms;
					ogSgmData.GetSgmListByUsgr(prlDemandPermit->Uper,olSgms);
					int ilNumSgms = olSgms.GetSize();
					for(int ilSgm = 0; ilSgm < ilNumSgms; ilSgm++)
					{
						PERDATA *prlPer = ogPerData.GetPerByUrno(olSgms[ilSgm].Uval);
						if(prlPer != NULL)
						{
							if (!olPermits.IsEmpty())
								olPermits += ',';
							olPermits += prlPer->Prmc;
						}
					}
				}
				else	// single permit
				{
					olPermits = prlDemandPermit->Quco;
				}
			}
		}
	}

	return olPermits;
}
	

void FlIndDemandTableViewer::FlIndDemandTableCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
    FlIndDemandTableViewer *polViewer = (FlIndDemandTableViewer *)popInstance;

	if(ipDDXType == JOB_NEW || ipDDXType == JOB_CHANGE || ipDDXType == JOB_DELETE)
	{
		polViewer->ProcessJobChange((JOBDATA *)vpDataPointer);
	}
	if(ipDDXType == JOD_NEW || ipDDXType == JOD_CHANGE)
	{
		polViewer->ProcessJodChange((JODDATA *)vpDataPointer);
	}
	else if(ipDDXType == DEMAND_CHANGE || ipDDXType == DEMAND_NEW)
	{
		polViewer->ProcessDemandChange((DEMANDDATA *)vpDataPointer);
	}
	else if(ipDDXType == DEMAND_DELETE)
	{
		polViewer->ProcessDemandDelete((DEMANDDATA *)vpDataPointer);
	}
	
	if(ogBasicData.IsPaxServiceT2() == true) //Singapore
	{	
		if(polViewer->pomTable != NULL && polViewer->pomTable->m_hWnd != NULL)
		{
			CWnd* polWnd = polViewer->pomTable->GetParent();
			(FlIndDemandTable*)(polWnd)->SendMessage(WM_TABLE_UPDATE_DATACOUNT,0,0);
		}
	}
}

void FlIndDemandTableViewer::ProcessDemandDelete(DEMANDDATA *prpDemand, bool bpDisplayTable /* = true */)
{
	if(prpDemand != NULL && prpDemand->Dety[0] == FID_DEMAND)
	{
		int ilLine = FindDemandLine(prpDemand->Urno);
		if(ilLine != -1)
		{
			DeleteLine(ilLine);
			pomTable->DeleteTextLine(ilLine);
			if(bpDisplayTable)
			{
				pomTable->DisplayTable();
			}
		}
	}
}

void FlIndDemandTableViewer::ProcessJobChange(JOBDATA *prpJob)
{
	if(prpJob != NULL)
	{
		DEMANDDATA *prlDemand = ogJodData.GetDemandForJob(prpJob);
		if(prlDemand != NULL)
		{
			ProcessDemandChange(prlDemand);
		}
	}
}

void FlIndDemandTableViewer::ProcessJodChange(JODDATA *prpJod)
{
	if(prpJod != NULL)
	{
		DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno(prpJod->Udem);
		if(prlDemand != NULL)
		{
			ProcessDemandChange(prlDemand);
		}
	}
}

void FlIndDemandTableViewer::ProcessDemandChange(DEMANDDATA *prpDemand)
{
	if(prpDemand != NULL && prpDemand->Dety[0] == FID_DEMAND)
	{
		int ilLine = -1;
		if((ilLine = FindDemandLine(prpDemand->Urno)) != -1)
		{
			int ilNumJobs = imJobCount;
			if(FormatLine(omLines[ilLine], prpDemand))
			{
				if(imJobCount <= ilNumJobs)
				{
					if(omLines[ilLine].InvalidDemand)
						pomTable->SetTextLineColor(ilLine, WHITE, RED);
					else
						pomTable->SetTextLineColor(ilLine, BLACK, WHITE);
			        pomTable->ChangeTextLine(ilLine, Format(&omLines[ilLine]), &omLines[ilLine]);
				}
				else
				{
					// the number of jobs assigned to the demand has exceeded the number of
					// table columns, so redisplay the whole table with an extra column
					CListBox *polListBox = pomTable->GetCTableListBox();
					int ilTopPos = polListBox->GetTopIndex();
					int ilSelLine = polListBox->GetCurSel();
					ChangeViewTo(ogCfgData.rmUserSetup.GBLV);
					int ilNumLines = polListBox->GetCount();
					if(ilTopPos < ilNumLines)
					{
						polListBox->SetTopIndex(ilTopPos);
					}
					if(ilSelLine < ilNumLines)
					{
						polListBox->SetSel(ilSelLine);
					}
				}
			}
		}
		else if((ilLine = MakeLine(prpDemand)) != -1)
		{
			pomTable->InsertTextLine(ilLine, Format(&omLines[ilLine]), &omLines[ilLine]);
		}
	}
}

DEMANDDATA *FlIndDemandTableViewer::GetDemandForJob(long lpJobUrno)
{
	return ogJodData.GetDemandForJob(lpJobUrno);
}

int FlIndDemandTableViewer::FindDemandLine(long lpDemandUrno)
{
	int ilLine = -1;
	int ilNumLines = omLines.GetSize();
	for(int ilL = 0; ilL < ilNumLines; ilL++)
	{
		if(omLines[ilL].DemandUrno == lpDemandUrno)
		{
			ilLine = ilL;
			break;
		}
	}

	return ilLine;
}

BOOL FlIndDemandTableViewer::PrintFidLine(FLINDEM_LINE *prpLine,BOOL bpIsLastLine)
{
	int ilBottomFrame;
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	ilBottomFrame = PRINT_FRAMETHIN;

	int ilJobFieldWidth = 580;
	if (bpIsLastLine == TRUE)
	{
		rlElement.Alignment  = PRINT_LEFT;
		rlElement.Length     = 1910;
		rlElement.FrameLeft  = PRINT_NOFRAME;
		rlElement.FrameRight = PRINT_NOFRAME;
		rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
		rlElement.FrameBottom= PRINT_NOFRAME;
		rlElement.pFont       = &pomPrint->omMediumFont_Bold;
		rlElement.Text.Empty();
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
	}
	else
	{
		if (pomPrint->imLineNo == 0)
		{
			rlElement.Alignment  = PRINT_CENTER;
			rlElement.Length     = 400;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameRight = PRINT_NOFRAME;
			rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
			rlElement.FrameBottom= PRINT_FRAMEMEDIUM;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;
			rlElement.Text       = GetString(IDS_STRING61919);	// Name
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 240;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = GetString(IDS_STRING61920);	// Code
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 100;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = GetString(IDS_STRING61921);	// From
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 100;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = GetString(IDS_STRING61922);	// Until
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Length     = 100;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = GetString(IDS_STRING61923);	// Duration
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			for(int ilJ = 0; ilJ < imMaxJobsPerLine; ilJ++)
			{
				rlElement.FrameRight = PRINT_NOFRAME;

				rlElement.Length     = ilJobFieldWidth;
				rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
				rlElement.FrameRight = PRINT_FRAMEMEDIUM;
				rlElement.Text       = GetString(IDS_STRING61924); // Employee
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			}
			pomPrint->PrintLine(rlPrintLine);
			rlPrintLine.DeleteAll();
		}


		bool bpFirst = true;
		int ilNumJobs = prpLine->Jobs.GetSize();
		int ilJob = 0;
		
		while(ilJob < ilNumJobs || bpFirst)
		{
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.Length     = 400;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.FrameRight = PRINT_NOFRAME;
			rlElement.FrameTop   = PRINT_FRAMEMEDIUM;
			rlElement.FrameBottom= ilBottomFrame;
			rlElement.pFont       = &pomPrint->omSmallFont_Bold;
			rlElement.Text       = bpFirst ? prpLine->Name : "";
			
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);

			rlElement.Alignment  = PRINT_LEFT;
			rlElement.Length     = 240;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = bpFirst ? prpLine->Code : "";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			rlElement.FrameTop   = PRINT_NOFRAME;

			rlElement.Length     = 100;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = bpFirst ? ogBasicData.FormatDate(prpLine->From, omStartTime) : "";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			rlElement.FrameTop   = PRINT_NOFRAME;

			rlElement.Length     = 100;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = bpFirst ? ogBasicData.FormatDate(prpLine->Until, omStartTime) : "";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			rlElement.FrameTop   = PRINT_NOFRAME;

			CString olDuration;
			olDuration.Format("%ld",prpLine->Duration.GetTotalMinutes());
			rlElement.Length     = 100;
			rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
			rlElement.Text       = bpFirst ? olDuration : "";
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
			rlElement.FrameTop   = PRINT_NOFRAME;

			for(int ilCurrJob = 0; ilCurrJob < imMaxJobsPerLine; ilCurrJob++, ilJob++)
			{
				rlElement.Alignment  = PRINT_LEFT;
				rlElement.Length     = ilJobFieldWidth;
				rlElement.FrameLeft  = PRINT_FRAMEMEDIUM;
				rlElement.FrameRight = PRINT_FRAMEMEDIUM;
				rlElement.Text       = (ilJob < ilNumJobs) ? prpLine->Jobs[ilJob] : "";
				rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
				rlElement.FrameTop   = PRINT_NOFRAME;
			}
			pomPrint->PrintLine(rlPrintLine);
			rlPrintLine.DeleteAll();
			bpFirst = false;
		} // end while

	}

	return TRUE;
}

BOOL FlIndDemandTableViewer::PrintFidHeader(CCSPrint *pomPrint)
{
	pomPrint->omCdc.StartPage();
	pomPrint->PrintHeader();
	return TRUE;
}

void FlIndDemandTableViewer::PrintView()
{  
//	CTime olStartTime = ogBasicData.GetTime();
///	CString omTarget = CString("vom ") + CString(olStartTime.Format("%d.%m.%Y %H:%M"));// + "  bis  " +	CString(olEndTime.Format("%d.%m.%Y %H:%M"));		

	CString omTarget = GetString(IDS_STRING33141) + CString(omStartTime.Format("%d.%m.%Y %H:%M")) + GetString(IDS_STRING33142) +
	CString(omEndTime.Format("%d.%m.%Y %H:%M"))	+  GetString(IDS_STRING33143) + ogCfgData.rmUserSetup.GBLV;		

	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");
	pomPrint = new CCSPrint(pomTable,PRINT_LANDSCAPE,60,500,100,
		GetString(IDS_STRING61925),olPrintDate,omTarget);
	pomPrint->imMaxLines = 20;

	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(PRINT_LANDSCAPE,ogBasicData.pcmPrintForm,DMPAPER_A3)  == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			imMaxJobsPerLine = (pomPrint->smPaperSize == DMPAPER_A3) ? 5 : 3; // A3 = 5 , A4  = 3 jobs printed per line.

			DOCINFO	rlDocInfo;
			pomPrint->imLineHeight = 62;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );

			char pclDocName[100];
			strcpy(pclDocName,GetString(IDS_STRING61925));
			rlDocInfo.lpszDocName = pclDocName;	

			pomPrint->omCdc.StartDoc( &rlDocInfo );
			for(int i = 0; i < omLines.GetSize(); i++ ) 
			{
				if( pomPrint->imLineNo >= pomPrint->imMaxLines )
				{
					if ( pomPrint->imPageNo > 0)
					{
						PrintFidLine(&omLines[i],TRUE);
						pomPrint->PrintFooter(GetString(IDS_STRING61926),GetString(IDS_STRING61925));
						pomPrint->imLineNo = pomPrint->imMaxLines + 1;
						pomPrint->omCdc.EndPage();
						pomPrint->imLineTop = pomPrint->imFirstLine;
					}
					PrintFidHeader(pomPrint);
				}
				PrintFidLine(&omLines[i],FALSE);
			}
			PrintFidLine(NULL,TRUE);
			pomPrint->PrintFooter(GetString(IDS_STRING61926),GetString(IDS_STRING61925));
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	pomPrint = NULL;
	}

	/*
	CPrintCtrl *prn = new CPrintCtrl(FALSE);
	prn->FlightSchedulePrint(&ogShiftData, (char *)(const char *)omDate);
	delete prn;
	*/
}

FLINDEM_LINE* FlIndDemandTableViewer::GetLine(DEMANDDATA *prpDem)
{
	if (!prpDem)
		return NULL;

	for (int i = 0; i < omLines.GetSize();i++)
	{
		FLINDEM_LINE *prlLine = &omLines[i];
		if (prlLine && prlLine->DemandUrno == prpDem->Urno)
			return prlLine;
	}

	return NULL;
}

void FlIndDemandTableViewer::SetStartTime(CTime opStartTime)
{
	omStartTime = opStartTime;
}

void FlIndDemandTableViewer::SetEndTime(CTime opEndTime)
{
	omEndTime = opEndTime;
}

