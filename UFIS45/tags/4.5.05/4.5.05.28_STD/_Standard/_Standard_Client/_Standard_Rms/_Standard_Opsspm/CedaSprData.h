// CedaSprData.h - Clocking-on/off times
#ifndef _CEDASPRDATA_H_
#define _CEDASPRDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>
#include <CedaShiftData.h>
#include <CedaJobData.h>

#define SPR_ENDSHIFT "0"
#define SPR_STARTSHIFT "1"
#define SPR_STARTBREAK "2"
#define SPR_ENDBREAK "3"

 struct SprDataStruct
{
	long	Urno;
	long	Ustf;			// Employee URNO - STFTAB.URNO
	char	Fcol[2];		// Event Type: 0 EndShift, 1 StartShift, 2 StartBreak, 3 EndBreak
	CTime	Acti;			// Time of event
	char	Peno[21];		// Personnel number
    char	Usec[33];		// Creator
    CTime	Cdat;			// Creation Date
    char	Useu[33];		// Updater
    CTime	Lstu;			// Update Date

	// non DB data
	enum State
	{
		NEW,
		CHANGED,
		DELETED,
		UNCHANGED
	};

	State emState;	

	SprDataStruct() 
	{
		Urno = 0L;
		Ustf = 0L;
		memset(Fcol,0,sizeof(Fcol));
		Acti = TIMENULL;
		strcpy(Peno," ");
		strcpy(Usec," ");
		Cdat	= TIMENULL;
		strcpy(Useu," ");
		Lstu	= TIMENULL;
		emState = NEW;
	}
};

typedef struct SprDataStruct SPRDATA;


class CedaSprData: public CCSCedaData
{
public:
    CCSPtrArray <SPRDATA> omData;
public:
	CString GetTableName(void);
    CedaSprData();
	~CedaSprData();
	CCSReturnCode ReadSprData(CTime opStartTime, CTime opEndTime);

	BOOL InitNewSprRecord(long lpUstf, const char *pcpFcol, CTime opActi,SPRDATA& rrpData);
	BOOL AddSprRecord(const SPRDATA& rpData,BOOL bpSendDDX = TRUE);
	BOOL DeleteSprRecord(long lpSpr,BOOL IsFromBC = FALSE);
	CTime GetTimeForUstf(long lpUstf, const char *pcpFcol, CTime opShiftStart, CTime opShiftEnd);
	SPRDATA	*GetSprByUstf(long lpUstf, const char *pcpFcol, CTime opShiftStart, CTime opShiftEnd);
	SPRDATA *GetSprByUrno(long lpUrno);
	BOOL ChangeTime(long lpSpr,CTime opActi);
	BOOL IsValidTime(CTime opShiftStart, CTime opShiftEnd,CTime opActi);
	void GetValidTime(CTime opShiftStart, CTime opShiftEnd,CTime& opStart,CTime& opEnd);
	bool ClockedOut(JOBDATA *prpJob);
	bool ClockedOut(CDWordArray &ropJobUrnos);
	bool ClockedOut(CCSPtrArray <JOBDATA> &ropJobs);
	bool ClockedOut(long lpShur, CTime opJobTime = TIMENULL);
	bool ClockedOut(SHIFTDATA *prpShift, CTime opJobTime = TIMENULL);
	bool ClockedOut(long lpStfu, CTime opFrom, CTime opTo);
	bool SplitShiftClockedOut(SHIFTDATA *prpShift, CTime opJobTime = TIMENULL);
	bool IsSplitShift(SHIFTDATA *prpShift);

	bool bmNoChangesAllowedAfterClockout;
	CTimeSpan omSplitShiftMinClockoutTimeBeforeShiftEnd;
	int bmSplitShiftMinDuration;
	int bmSplitShiftMinBreakTime;

	CString Dump(long lpUrno);
	
private:
	SPRDATA *AddInternal(SPRDATA &rrpSpr);
	void DeleteInternal(long lpUrno);
	BOOL SaveSprRecord(SPRDATA *prpSpr,SPRDATA *prpOldSpr = NULL);

	void ProcessSprBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	void ConvertDatesToUtc(SPRDATA *prpSpr);
	void ConvertDatesToLocal(SPRDATA *prpSpr);
	friend void  ProcessSprCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

private:
    CMapPtrToPtr omUstfMap, omUrnoMap;
};


extern CedaSprData ogSprData;
#endif
