// CedaOdaData.cpp - OdaenceCodes
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <ccsddx.h>
#include <CedaOdaData.h>
#include <BasicData.h>

// MCU 07.07 no more pogPrePlanShiftData, it's now ogShiftData
//extern CedaShiftData *pogPrePlanShiftData;



CedaOdaData::CedaOdaData()
{                  
    // Create an array of CEDARECINFO for SHIFTTYPEDATA
    BEGIN_CEDARECINFO(ODADATA, OdaDataRecInfo)
		FIELD_LONG(Urno,"URNO")
        FIELD_CHAR_TRIM(Sdac,"SDAC")
        FIELD_CHAR_TRIM(Sdan,"SDAN")
        FIELD_CHAR_TRIM(Work,"WORK")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(OdaDataRecInfo)/sizeof(OdaDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&OdaDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"ODATAB");
    pcmFieldList = "URNO,SDAC,SDAN";
}
 
CedaOdaData::~CedaOdaData()
{
	TRACE("CedaOdaData::~CedaOdaData called\n");
	omData.DeleteAll();
	omOdaTypeMap.RemoveAll();
	omUrnoMap.RemoveAll();
}

bool CedaOdaData::ReadOdaData()
{
    char pclWhere[512];
	bool blRc = true;

	ODADATA rlOdaData;

    // Select data from the database
	char pclCom[10] = "RT";
    strcpy(pclWhere,"");
    if ((blRc = CedaAction2(pclCom, pclWhere)) == false)
	{
		ogBasicData.LogCedaError("CedaOdaData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
        return false;
	}

    // Load data from CCSCedaData into the dynamic array of record
    omOdaTypeMap.RemoveAll();
    omData.DeleteAll();

    for (int ilLc = 0; blRc; ilLc++)
    {
		if ((blRc = GetBufferRecord2(ilLc,&rlOdaData)) == true)
		{
			AddOdaInternal(rlOdaData);
		}
	}

	ogBasicData.Trace("CedaOdaData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(ODADATA), pclWhere);
    return true;
}


void CedaOdaData::AddOdaInternal(ODADATA &rrpOdaData)
{
	ODADATA *prlOda = new ODADATA;
	*prlOda = rrpOdaData;
	omData.Add(prlOda);
	omOdaTypeMap.SetAt(prlOda->Sdac,prlOda);
	omUrnoMap.SetAt((void *)prlOda->Urno,prlOda);
}


ODADATA *CedaOdaData::GetOdaByUrno(long lpUrno)
{
	ODADATA *prlOda = NULL;
	omUrnoMap.Lookup((void *)lpUrno,(void *& )prlOda);
	return prlOda;
}


void CedaOdaData::GetAllOdaTypes(CCSPtrArray <ODADATA> &ropOdaTypes)
{
	ropOdaTypes.RemoveAll();
	for(int ilLc = 0; ilLc < omData.GetSize(); ilLc++)
	{
		ropOdaTypes.Add(&omData[ilLc]);
	}

}

ODADATA *CedaOdaData::GetOdaByType(CString opOdaType)
{
	ODADATA *prlOdaType;

	if (omOdaTypeMap.Lookup(opOdaType,(void *&)prlOdaType) == TRUE)
	{
		return prlOdaType;
	}
	else
	{
		return NULL;
	}
}

CString CedaOdaData::GetTableName(void)
{
	return CString(pcmTableName);
}