// CedaInitModuData.cpp
 
#include <stdafx.h>
#include <CedaInitModuData.h>
#include <resource.h>
#include <CCSGlobl.h>

CedaInitModuData ogInitModuData;


//--CCSCedaData-----------------------------------------------------------------------------------------------

CedaInitModuData::CedaInitModuData()
{
	strcpy(pcmInitModuFieldList,"APPL,SUBD,FUNC,FUAL,TYPE,STAT");
}

//--~CCSCedaData----------------------------------------------------------------------------------------------

CedaInitModuData::~CedaInitModuData(void)
{
}

//--SEND INIT MODULS-------------------------------------------------------------------------------------------

bool CedaInitModuData::SendInitModu()
{
	CCSReturnCode ilRc = true;

	CString olInitModuData = this->GetInitModuTxt();

	char *pclInitModuData = new char[olInitModuData.GetLength()+300];
	strcpy(pclInitModuData,olInitModuData);

	char pclTable[11];
//	sprintf("%s ",pcgTableExt);
	char pclCom[10] = "SMI";
	char pclSel[100] = "", pclSort[100] = "";
	ilRc = CedaAction(pclCom,pclTable,pcmInitModuFieldList,pclSel,pclSort,pclInitModuData) == RCSuccess ? true : false;
	if(ilRc==false && !omLastErrorMessage.IsEmpty())
	{
		AfxMessageBox(CString("SendInitModu:\n") + omLastErrorMessage,MB_ICONEXCLAMATION);
	}
	delete pclInitModuData;
	return ilRc;
}

//--------------------------------------------------------------------------------------------------------------

CString CedaInitModuData::GetInitModuTxt() 
{
//INITMODU//////////////////////////////////////////////////////////////
	CString olInitModuData = CString(pcgAppName) + CString(",InitModu,InitModu,InitModu,B,-");
		olInitModuData += ","+GetString(IDS_STRING_TIMESPANDLG)+CString(",LOAD_TIMESPAN_DLG,")+GetString(IDS_STRING_TIMESPANDLG)+CString(",P,1");
		//////////////////////////////////////////////// ButtonList
		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_STAFFDIAGRAM,")+GetString(IDS_STRING61814)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_GATEDIAGRAM,")+GetString(IDS_STRING61815)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_CCIDIAGRAM,")+GetString(IDS_STRING61816)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_STAFFTABLE,")+GetString(IDS_STRING61817)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_FLIGHTPLAN,")+GetString(IDS_STRING61818)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_GATEBELEGUNG,")+GetString(IDS_STRING61819)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_FLIGHTJOBSTABLE,")+GetString(IDS_FLIGHTJOBSTABLE)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_CCIBELEGUNG,")+GetString(IDS_STRING61820)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_REQUEST,")+GetString(IDS_STRING61821)+CString(",B,1");
//		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_PEAKTABLE,")+GetString(IDS_STRING61822)+CString(",B,1");
//		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_COVERAGE,")+GetString(IDS_STRING61823)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_PREPLANING,")+GetString(IDS_STRING61824)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_REGNDIAGRAM,")+GetString(IDS_STRING61822)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_POSITIONS,")+GetString(IDS_STRING61830)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_ATTENTION,")+GetString(IDS_STRING61825)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_CONFLICT,")+GetString(IDS_STRING61826)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_UNDO,")+GetString(IDS_STRING61827)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_SEARCH,")+GetString(IDS_STRING61828)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_ONLINE,")+GetString(IDS_STRING61829)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_RELEASE,")+GetString(IDS_STRING61831)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_INFO,")+GetString(IDS_STRING61834)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_SETUP,")+GetString(IDS_STRING61835)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_HELPBUTTON,")+GetString(IDS_STRING61836)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_PRINT_COMMON,")+GetString(IDS_STRING61837)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_UNRESOLVED_DEMANDS,")+GetString(IDS_STRING61893)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_EQUIPMENTDIAGRAM,")+GetString(IDS_EQUIPMENTCHART)+CString(",B,1");
//		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_EQUIPMENTTABLE,")+GetString(IDS_EQUIPMENTLIST)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61813)+CString(",BUTTONLIST IDC_FLIGHT_IND_DEMAND,")+GetString(IDS_FIDTABLE)+CString(",B,1");
		/////////////////////////////////////////////// CCI Diagram
		olInitModuData += ","+GetString(IDS_STRING61845)+CString(",CCIDIAGRAM IDC_ANSICHT,")+GetString(IDS_STRING61840)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61845)+CString(",CCIDIAGRAM IDC_VIEW,")+GetString(IDS_STRING61841)+CString(",C,1");
		olInitModuData += ","+GetString(IDS_STRING61845)+CString(",CCIDIAGRAM IDC_MABSTAB,")+GetString(IDS_STRING61842)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61845)+CString(",CCIDIAGRAM IDC_ZEIT,")+GetString(IDS_STRING61843)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61845)+CString(",CCIDIAGRAM IDC_PRINT,")+GetString(IDS_STRING61844)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61845)+CString(",CCIDIAGRAM IDC_DEMANDLIST,")+GetString(IDS_STRING61973)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61845)+CString(",CCIDIAGRAM IDC_ASSIGN,")+GetString(IDS_STRING61974)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61845)+CString(",CCIDIAGRAM IDC_ALLOCATE,")+GetString(IDS_STRING61975)+CString(",B,1");
		/////////////////////////////////////////////// Gate Diagram
		olInitModuData += ","+GetString(IDS_STRING61846)+CString(",GATEDIAGRAM IDC_ANSICHT,")+GetString(IDS_STRING61840)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61846)+CString(",GATEDIAGRAM IDC_VIEW,")+GetString(IDS_STRING61841)+CString(",C,1");
		olInitModuData += ","+GetString(IDS_STRING61846)+CString(",GATEDIAGRAM IDC_MABSTAB,")+GetString(IDS_STRING61842)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61846)+CString(",GATEDIAGRAM IDC_ZEIT,")+GetString(IDS_STRING61843)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61846)+CString(",GATEDIAGRAM IDC_PRINT,")+GetString(IDS_STRING61844)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61846)+CString(",GATEDIAGRAM IDC_ASSIGN,")+GetString(IDS_STRING61976)+CString(",B,1");
		/////////////////////////////////////////////// Equipment Diagram
		olInitModuData += ","+GetString(IDS_EQUIPMENTCHART)+CString(",EQUIPMENTDIAGRAM IDC_ANSICHT,")+GetString(IDS_STRING61840)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_EQUIPMENTCHART)+CString(",EQUIPMENTDIAGRAM IDC_VIEW,")+GetString(IDS_STRING61841)+CString(",C,1");
		olInitModuData += ","+GetString(IDS_EQUIPMENTCHART)+CString(",EQUIPMENTDIAGRAM IDC_MABSTAB,")+GetString(IDS_STRING61842)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_EQUIPMENTCHART)+CString(",EQUIPMENTDIAGRAM IDC_ZEIT,")+GetString(IDS_STRING61843)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_EQUIPMENTCHART)+CString(",EQUIPMENTDIAGRAM IDC_PRINT,")+GetString(IDS_STRING61844)+CString(",B,1");
//		olInitModuData += ","+GetString(IDS_EQUIPMENTCHART)+CString(",EQUIPMENTDIAGRAM IDC_ASSIGN,")+GetString(IDS_STRING61976)+CString(",B,1");
		/////////////////////////////////////////////// Position Diagram
		olInitModuData += ","+GetString(IDS_STRING61860)+CString(",PSTDIAGRAM IDC_ANSICHT,")+GetString(IDS_STRING61840)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61860)+CString(",PSTDIAGRAM IDC_VIEW,")+GetString(IDS_STRING61841)+CString(",C,1");
		olInitModuData += ","+GetString(IDS_STRING61860)+CString(",PSTDIAGRAM IDC_MABSTAB,")+GetString(IDS_STRING61842)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61860)+CString(",PSTDIAGRAM IDC_ZEIT,")+GetString(IDS_STRING61843)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61860)+CString(",PSTDIAGRAM IDC_PRINT,")+GetString(IDS_STRING61844)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61860)+CString(",PSTDIAGRAM IDC_ASSIGN,")+GetString(IDS_STRING61976)+CString(",B,1");
		/////////////////////////////////////////////// Registration Diagram
		olInitModuData += ","+GetString(IDS_REGNDIAGRAM)+CString(",REGNDIAGRAM IDC_ANSICHT,")+GetString(IDS_STRING61840)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_REGNDIAGRAM)+CString(",REGNDIAGRAM IDC_VIEW,")+GetString(IDS_STRING61841)+CString(",C,1");
		olInitModuData += ","+GetString(IDS_REGNDIAGRAM)+CString(",REGNDIAGRAM IDC_MABSTAB,")+GetString(IDS_STRING61842)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_REGNDIAGRAM)+CString(",REGNDIAGRAM IDC_ZEIT,")+GetString(IDS_STRING61843)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_REGNDIAGRAM)+CString(",REGNDIAGRAM IDC_PRINT,")+GetString(IDS_STRING61844)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_REGNDIAGRAM)+CString(",REGNDIAGRAM IDC_ASSIGN,")+GetString(IDS_STRING61976)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_REGNDIAGRAM)+CString(",REGNDIAGRAM 3RDPARTY,")+GetString(IDS_3RDPARTYBUTTON)+CString(",B,1");
		/////////////////////////////////////////////// Staff Diagram
		olInitModuData += ","+GetString(IDS_STRING61847)+CString(",STAFFDIAGRAM IDC_ANSICHT,")+GetString(IDS_STRING61840)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61847)+CString(",STAFFDIAGRAM IDC_VIEW,")+GetString(IDS_STRING61841)+CString(",C,1");
		olInitModuData += ","+GetString(IDS_STRING61847)+CString(",STAFFDIAGRAM IDC_MABSTAB,")+GetString(IDS_STRING61842)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61847)+CString(",STAFFDIAGRAM IDC_ZEIT,")+GetString(IDS_STRING61843)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61847)+CString(",STAFFDIAGRAM IDC_PRINT,")+GetString(IDS_STRING61844)+CString(",B,1");
		/////////////////////////////////////////////// Flight Plan
		olInitModuData += ","+GetString(IDS_STRING61853)+CString(",FLIGHTPLAN IDC_VIEW,")+GetString(IDS_STRING61848)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61853)+CString(",FLIGHTPLAN IDC_COMBO2,")+GetString(IDS_STRING61849)+CString(",C,1");
		olInitModuData += ","+GetString(IDS_STRING61853)+CString(",FLIGHTPLAN IDC_PRINT,")+GetString(IDS_STRING61850)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61853)+CString(",FLIGHTPLAN IDC_TAB_FLIGHTTABLE,")+GetString(IDS_STRING61851)+CString(",B,1");
		/////////////////////////////////////////////// Staff Table
		olInitModuData += ","+GetString(IDS_STRING61854)+CString(",STAFFTABLE IDC_VIEW,")+GetString(IDS_STRING61848)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61854)+CString(",STAFFTABLE IDC_COMBO1,")+GetString(IDS_STRING61849)+CString(",C,1");
		olInitModuData += ","+GetString(IDS_STRING61854)+CString(",STAFFTABLE IDC_PRINT,")+GetString(IDS_STRING61850)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61854)+CString(",STAFFTABLE IDC_TAB_STAFFTABLE,")+GetString(IDS_STRING61851)+CString(",B,1");
		/////////////////////////////////////////////// Setup Dialog
		olInitModuData += ","+GetString(IDS_SETUPDLG)+CString(",SETUPDLG IDC_CONFLICT_CONFIG,")+GetString(IDS_STRING33049)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_SETUPDLG)+CString(",SETUPDLG IDC_NAME_CONFIG,")+GetString(IDS_NAME_CONFIG)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_SETUPDLG)+CString(",SETUPDLG IDC_TELEXTYPE_CONFIG,")+GetString(IDS_TELEXTYPE_CONFIG)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_SETUPDLG)+CString(",SETUPDLG IDC_FIELD_CONFIG,")+GetString(IDS_FIELD_CONFIG)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_SETUPDLG)+CString(",SETUPDLG IDC_FUNCCOLOUR_CONFIG,")+GetString(IDS_FUNCCOLOUR_CONFIG)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_SETUPDLG)+CString(",SETUPDLG IDC_OTHERCOLOUR_CONFIG,")+GetString(IDS_OTHERCOLOUR_CONFIG)+CString(",B,1");

		/////////////////////////////////////////////// Conflict Setup
		olInitModuData += ","+GetString(IDS_STRING61859)+CString(",CONFLICT IDC_LOADFROMDEFAULT,")+GetString(IDS_STRING61856)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61859)+CString(",CONFLICT IDC_SAVEASDEFAULT,")+GetString(IDS_STRING61857)+CString(",B,1");
		/////////////////////////////////////////////// Flight Detail Window
		olInitModuData += ","+GetString(IDS_STRING61866)+CString(",FLIGHTDETAIL IDC_PRINT_FLIGHT,")+GetString(IDS_STRING61837)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61866)+CString(",FLIGHTDETAIL IDC_BEMERKUNG,")+GetString(IDS_FDD_REMARK)+CString(",E,1");
		olInitModuData += ","+GetString(IDS_STRING61866)+CString(",FLIGHTDETAIL COPYDEMAND,")+GetString(IDS_FDD_COPYDEMANDS)+CString(",Z,1");
		olInitModuData += ","+GetString(IDS_STRING61866)+CString(",FLIGHTDETAIL DELETEDEMAND,")+GetString(IDS_FDD_DELETEDEMANDS)+CString(",Z,1");
		olInitModuData += ","+GetString(IDS_STRING61866)+CString(",FLIGHTDETAIL BOOKEDPAX,")+GetString(IDS_FDD_BOOKEDPAX)+CString(",Z,1");
		olInitModuData += ","+GetString(IDS_STRING61866)+CString(",FLIGHTDETAIL CHECKEDINPAX,")+GetString(IDS_FDD_CHECKEDINPAX)+CString(",Z,1");
		/////////////////////////////////////////////// Demand Table
		olInitModuData += ","+GetString(IDS_STRING61894)+CString(",DEMANDTABLE IDC_VIEW,")+GetString(IDS_STRING61848)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61894)+CString(",DEMANDTABLE IDC_VIEWCOMBO,")+GetString(IDS_STRING61849)+CString(",C,1");
		olInitModuData += ","+GetString(IDS_STRING61894)+CString(",DEMANDTABLE IDC_PRINT,")+GetString(IDS_STRING61850)+CString(",B,1");
		/////////////////////////////////////////////// Available Emps Dlg
		olInitModuData += ","+GetString(IDS_AVAILEMPSDLG)+CString(",AVAILABLE_EMPS_DLG IDC_VIEW,")+GetString(IDS_AVAILEMPSDLGVIEW)+CString(",C,1");
		/////////////////////////////////////////////// Available Equipment Dlg
		olInitModuData += ","+GetString(IDS_AVAILEQUDLG)+CString(",AVAILABLE_EQU_DLG IDC_VIEW,")+GetString(IDS_AVAILEQUDLGVIEW)+CString(",C,1");
		/////////////////////////////////////////////// Equipment Detail Dlg
		olInitModuData += ","+GetString(IDS_EQUIPMENT_DETDLG)+CString(",EQUIPMENTDETDLG IDC_ATTRIBUTES,")+GetString(IDS_EQDD_EDITATTRIBUTES)+CString(",Z,1");
		/////////////////////////////////////////////// Checkin counter demand Table
		olInitModuData += ","+GetString(IDS_STRING61977)+CString(",CICDEMANDTABLE IDC_VIEW,")+GetString(IDS_STRING61848)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61977)+CString(",CICDEMANDTABLE IDC_VIEWCOMBO,")+GetString(IDS_STRING61849)+CString(",C,1");
		olInitModuData += ","+GetString(IDS_STRING61977)+CString(",CICDEMANDTABLE IDC_PRINT,")+GetString(IDS_STRING61850)+CString(",B,1");
		olInitModuData += ","+GetString(IDS_STRING61977)+CString(",CICDEMANDTABLE IDC_ALLOCATE,")+GetString(IDS_STRING61975)+CString(",B,1");

		/////////////////////////////////////////////// Job time menu
		olInitModuData += ","+GetString(IDS_STRING62509)+CString(",JOBTIMEHANDLER,")+GetString(IDS_STRING62510)+CString(",B,1");

		/////////////////////////////////////////////// general function
		olInitModuData += ","+GetString(IDS_GENERAL)+CString(",GENERAL SUB1SUB2,")+GetString(IDS_SUB1SUB2)+CString(",C,-");
		olInitModuData += ","+GetString(IDS_GENERAL)+CString(",GENERAL RO AFTER CLOCKOUT,")+GetString(IDS_READOLY_AFTERCLOCKOUT)+CString(",C,0");
		olInitModuData += ","+GetString(IDS_GENERAL)+CString(",GENERAL TELEPHONENO,")+GetString(IDS_STAFF_TELEPHONE_NO)+CString(",E,1");
		/////////////////////////////////////////////// change shift function to any function
		olInitModuData += ","+GetString(IDS_PREP_DETAIL_DIALOG)+CString(",ONLY BASE FUNCTIONS,")+GetString(IDS_ONLY_BASE_FUNCTIONS)+CString(",Z,0");

		/***
		if (bgIsPrm)
		{
			olInitModuData += ","+GetString(IDS_PRMFLT)+CString("PRM-FILTER")+CString(",Z,0");
		}
		**/

//INITMODU END//////////////////////////////////////////////////////////
		return olInitModuData;
}

//--------------------------------------------------------------------------------------------------------------

CString CedaInitModuData::GetTableName(void)
{
	return CString(pcmTableName);
}