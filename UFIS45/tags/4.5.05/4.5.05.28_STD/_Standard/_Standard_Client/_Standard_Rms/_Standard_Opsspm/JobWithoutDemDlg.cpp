// JobWithoutDemDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <JobWithoutDemDlg.h>
#include <CCSTime.h>
#include <BasicData.h>
#include <CedaTplData.h>
#include <AllocData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// JobWithoutDemDlg dialog


JobWithoutDemDlg::JobWithoutDemDlg(CWnd* pParent /*=NULL*/)
	: CDialog(JobWithoutDemDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(JobWithoutDemDlg)
	m_FromDate = NULL;
	m_FromTime = NULL;
	m_ToDate = NULL;
	m_ToTime = NULL;
	//}}AFX_DATA_INIT
	bmDisableAlocationUnit = false;
	bmDisableTemplate = false;
	lmUtpl = 0L;
}

void JobWithoutDemDlg::InitDefaultValues(CTime opFrom, CTime opTo, CString opAloc /*= ""*/, long lpUtpl /*= 0L*/)
{
	omFrom = opFrom;
	omTo = opTo;
	omAloc = opAloc;
	lmUtpl = lpUtpl;
}

void JobWithoutDemDlg::DisableAlocationUnit()
{
	bmDisableAlocationUnit = true;
}

void JobWithoutDemDlg::DisableTemplate()
{
	bmDisableTemplate = true;
}

void JobWithoutDemDlg::DoDataExchange(CDataExchange* pDX)
{
	if(pDX->m_bSaveAndValidate == FALSE)
	{
		m_FromDate = omFrom;
		m_FromTime = omFrom;
		m_ToDate = omTo;
		m_ToTime = omTo;
	}

	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(JobWithoutDemDlg)
	DDX_CCSddmmyy(pDX, IDC_FROMDATE, m_FromDate);
	DDX_CCSTime(pDX, IDC_FROMTIME, m_FromTime);
	DDX_CCSddmmyy(pDX, IDC_TODATE, m_ToDate);
	DDX_CCSTime(pDX, IDC_TOTIME, m_ToTime);
	//}}AFX_DATA_MAP

	if(pDX->m_bSaveAndValidate == TRUE)
	{
		// Convert the date and time back
		omFrom = CTime(m_FromDate.GetYear(), m_FromDate.GetMonth(), m_FromDate.GetDay(), 
						m_FromTime.GetHour(), m_FromTime.GetMinute(),	m_FromTime.GetSecond());
		omTo = CTime(m_ToDate.GetYear(), m_ToDate.GetMonth(),m_ToDate.GetDay(),
						m_ToTime.GetHour(), m_ToTime.GetMinute(),	m_ToTime.GetSecond());
	}
}


BEGIN_MESSAGE_MAP(JobWithoutDemDlg, CDialog)
	//{{AFX_MSG_MAP(JobWithoutDemDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// JobWithoutDemDlg message handlers

BOOL JobWithoutDemDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CWnd *polWnd = NULL;

	SetWindowText(GetString(IDS_JWDD_TITLE));
	polWnd = GetDlgItem(IDC_FROMTITLE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_JWDD_FROMTITLE));
	}
	polWnd = GetDlgItem(IDC_TOTITLE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_JWDD_TOTITLE));
	}
	polWnd = GetDlgItem(IDC_TEMPLATETITLE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_JWDD_TEMPLATE));
	}

	CComboBox *polTemplates = (CComboBox *) GetDlgItem(IDC_TEMPLATE);
	if(polTemplates != NULL)
	{
		polTemplates->ResetContent();

		int ilIndex = -1;
		if(lmUtpl == 0L)
		{
			void *pvlDummy;
			CMapStringToPtr olTemplatesSoFar; // template names are duplicated (for Flight dependendent and indie)
			CDWordArray olTplUrnos;
			ogBasicData.GetTemplateFilters(olTplUrnos);
			int ilNumTpls = olTplUrnos.GetSize();
			for(int ilTpl = 0; ilTpl < ilNumTpls; ilTpl++)
			{
				TPLDATA *prlTpl = ogTplData.GetTplByUrno(olTplUrnos[ilTpl]);
				if(prlTpl != NULL && !olTemplatesSoFar.Lookup(prlTpl->Tnam,(void *&) pvlDummy))
				{
					if((ilIndex = polTemplates->AddString(prlTpl->Tnam)) != CB_ERR)
					{
						polTemplates->SetItemData(ilIndex, prlTpl->Urno);
						olTemplatesSoFar.SetAt(prlTpl->Tnam,NULL);
					}
				}
			}
		}
		else
		{
			TPLDATA *prlTpl = ogTplData.GetTplByUrno(lmUtpl);
			if(prlTpl != NULL)
				if((ilIndex = polTemplates->AddString(prlTpl->Tnam)) != CB_ERR)
					polTemplates->SetItemData(ilIndex, prlTpl->Urno);
		}

		if(polTemplates->GetCount() > 0)
		{
			polTemplates->SetCurSel(0);
		}

		if(bmDisableTemplate)
		{
			polWnd = GetDlgItem(IDC_TEMPLATE);
			polWnd->EnableWindow(FALSE);
		}
	}
	polWnd = GetDlgItem(IDC_ALLOCUNITTITLE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_JWDD_ALLOCUNIT));
	}

	CComboBox *polAllocTypes = (CComboBox *) GetDlgItem(IDC_ALLOCUNIT);
	if(polAllocTypes != NULL)
	{
		polAllocTypes->ResetContent();
		if(!bmDisableAlocationUnit)
		{
			if(ogBasicData.bmDisplayCheckins)
				polAllocTypes->AddString(ogBasicData.GetStringForAllocUnitType(ALLOCUNITTYPE_CIC));

			if(ogBasicData.bmDisplayGates)
				polAllocTypes->AddString(ogBasicData.GetStringForAllocUnitType(ALLOCUNITTYPE_GATE));

			if(ogBasicData.bmDisplayPositions)
				polAllocTypes->AddString(ogBasicData.GetStringForAllocUnitType(ALLOCUNITTYPE_PST));

			if(ogBasicData.bmDisplayRegistrations)
				polAllocTypes->AddString(ogBasicData.GetStringForAllocUnitType(ALLOCUNITTYPE_REGN));
			if(!omAloc.IsEmpty())
			{
				polAllocTypes->SelectString(-1, ogBasicData.GetStringForAllocUnitType(omAloc));
			}
			else if(polAllocTypes->GetCount() > 0)
			{
				polAllocTypes->SetCurSel(0);
			}
			else
			{
				polAllocTypes->EnableWindow(FALSE);
			}
		}
		else
		{
			polAllocTypes->AddString(ogBasicData.GetStringForAllocUnitType(omAloc));
			polAllocTypes->SetCurSel(0);
			polAllocTypes->EnableWindow(FALSE);
		}
	}

	polWnd = GetDlgItem(IDC_OK);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_JWDD_OK));
	}
	polWnd = GetDlgItem(IDC_CANCEL);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_JWDD_CANCEL));
	}
	
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void JobWithoutDemDlg::OnOK() 
{
	if(UpdateData(TRUE))
	{
		bool blRc = true;
		if(blRc && omFrom >= omTo)
		{
			MessageBox("The first job must start before it ends!","Split Job Error", MB_ICONERROR);
			blRc = false;
		}

		if(blRc)
		{
			CComboBox *polTemplates = (CComboBox *) GetDlgItem(IDC_TEMPLATE);
			if(polTemplates != NULL)
			{
				int ilIndex = polTemplates->GetCurSel();
				if(ilIndex != CB_ERR)
				{
					lmUtpl = (long) polTemplates->GetItemData(ilIndex);
				}
			}
		}
		if(blRc)
		{
			CString olAloc = "";
			CComboBox *polAllocUnits = (CComboBox *) GetDlgItem(IDC_ALLOCUNIT);
			if(polAllocUnits != NULL)
			{
				int ilIndex = polAllocUnits->GetCurSel();
				if(ilIndex != CB_ERR)
				{
					polAllocUnits->GetLBText(ilIndex, olAloc);
				}
			}
			if(!olAloc.IsEmpty())
			{
				omAloc = ogBasicData.GetAllocUnitTypeFromString(olAloc);
			}
		}
		
		if(blRc)
		{
			CDialog::OnOK();
		}
	}
	
}
