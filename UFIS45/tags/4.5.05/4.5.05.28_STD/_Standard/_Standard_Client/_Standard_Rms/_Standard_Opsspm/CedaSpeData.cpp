// STAFF PERMITS
// CedaSpeData.cpp - table containing one or more permits for each employee
//

#include <afxwin.h>
#include <ccsglobl.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <CedaSpeData.h>
#include <BasicData.h>

CedaSpeData::CedaSpeData()
{                  
    BEGIN_CEDARECINFO(SPEDATA, SpeDataRecInfo)
		FIELD_LONG(Urno,"URNO")
		FIELD_LONG(Surn,"SURN")
		FIELD_CHAR_TRIM(Code,"CODE")
		FIELD_DATE(Vpfr,"VPFR")
		FIELD_DATE(Vpto,"VPTO")
		FIELD_CHAR_TRIM(Prio,"PRIO")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(SpeDataRecInfo)/sizeof(SpeDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SpeDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

    // Initialize table names and field names
    strcpy(pcmTableName,"SPETAB");
    pcmFieldList = "URNO,SURN,CODE,VPFR,VPTO,PRIO";
}
 
CedaSpeData::~CedaSpeData()
{
	TRACE("CedaSpeData::~CedaSpeData called\n");
	ClearAll();
}

void CedaSpeData::ClearAll()
{
	CMapPtrToPtr *polSingleMap;
	long llUrno;
	for(POSITION rlPos = omSurnMap.GetStartPosition(); rlPos != NULL; )
	{
		omSurnMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& )polSingleMap);
		polSingleMap->RemoveAll();
		delete polSingleMap;
	}
	omSurnMap.RemoveAll();
	omData.DeleteAll();
}

CCSReturnCode CedaSpeData::ReadSpeData()
{
	CCSReturnCode ilRc = RCSuccess;
	ClearAll();

	char pclCom[10] = "RT";
    char pclWhere[512] = "";
	CTime olDay = ogBasicData.GetTimeframeStart();

	sprintf ( pclWhere, "WHERE VPTO=' ' OR VPTO>'%s'", olDay.Format("%Y%m%d%H%M%S") );
    if((ilRc = CedaAction2(pclCom, pclWhere)) != RCSuccess)
	{
		ogBasicData.LogCedaError("CedaSpeData Error",omLastErrorMessage,pclCom,pcmFieldList,pcmTableName,pclWhere);
	}
	else
	{
		SPEDATA rlSpeData;
		for (int ilLc = 0; ilRc == RCSuccess; ilLc++)
		{
			if ((ilRc = GetBufferRecord2(ilLc,&rlSpeData)) == RCSuccess)
			{
				AddSpeInternal(rlSpeData);
			}
		}
		ilRc = RCSuccess;
	}

	ogBasicData.Trace("CedaSpeData Read %d Records (Rec Size %d) %s",omData.GetSize(), sizeof(SPEDATA), pclWhere);
    return ilRc;
}


void CedaSpeData::AddSpeInternal(SPEDATA &rrpSpe)
{
	SPEDATA *prlSpe = new SPEDATA;
	*prlSpe = rrpSpe;
	omData.Add(prlSpe);
	CMapPtrToPtr *polSingleMap;
	if(omSurnMap.Lookup((void *) prlSpe->Surn, (void *&) polSingleMap))
	{
		polSingleMap->SetAt((void *)prlSpe->Urno,prlSpe);
	}
	else
	{
		polSingleMap = new CMapPtrToPtr;
		polSingleMap->SetAt((void *)prlSpe->Urno,prlSpe);
		omSurnMap.SetAt((void *)prlSpe->Surn,polSingleMap);
	}
}


void CedaSpeData::GetPermitsBySurn(long lpSurn, CStringArray &ropEmpPermits, bool bpReset /*true*/)
{
	if(bpReset)
	{
	    ropEmpPermits.RemoveAll();
	}

	CMapPtrToPtr *polSingleMap;
	long llUrno;
	SPEDATA *prlSpe;

	if(omSurnMap.Lookup((void *)lpSurn,(void *& )polSingleMap))
	{
		CTime olCurrTime = ogBasicData.GetTime();
		for(POSITION rlPos = polSingleMap->GetStartPosition(); rlPos != NULL; )
		{
			polSingleMap->GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlSpe);
			if(prlSpe->Vpfr != TIMENULL && prlSpe->Vpfr <= olCurrTime &&
				(prlSpe->Vpto == TIMENULL || prlSpe->Vpto > olCurrTime))
			{
				ropEmpPermits.Add(prlSpe->Code);
			}
		}
	}
}


CString CedaSpeData::GetTableName(void)
{
	return CString(pcmTableName);
}