// SUViewsDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// SUViewsDlg dialog
#ifndef _SETUP_VIEWS_DLG_
#define _SETUP_VIEWS_DLG_

#include <ccsglobl.h>
#include <CCSCedaData.h>
#include <BasicData.h>
#include <CedaCfgData.h>

class SUViewsDlg : public CDialog
{
// Construction
public:
	SUViewsDlg(CWnd* pParent = NULL, CString *popString = NULL, 
			   USERSETUPDATA *popUserSetup = NULL, CString popMode = CString("INSERT_MODE"));   // standard constructor

	CString *pomString;
	USERSETUPDATA *pomUserSetup;
// Dialog Data
	//{{AFX_DATA(SUViewsDlg)
	enum { IDD = IDD_VIEWS };
	CComboBox	c_Attentiontbl;
	CComboBox	c_Stafflist;
	CComboBox	c_Staffchart;
	CComboBox	c_Requests;
	CComboBox	c_Preplanttbl;
	CComboBox	c_Gatechrt;
	CComboBox	c_EquipmentChart;
	CComboBox	c_Regnchrt;
	CComboBox	c_Poschrt;
	CComboBox	c_Gatebel;
	CComboBox	c_Flightplan;
	CComboBox	c_conflicttbl;
	CComboBox	c_Ccichart;
	CComboBox	c_Prmtbl;
	CComboBox	c_CciBel;
	//CComboBox	c_Prmtbl;
	CString	m_Attentiontbl;
	CString	m_CCIBel;
	CString	m_PRMtbl;;
	CString	m_CCIChrt;
	CString	m_Conflikttbl;
	CString	m_Flightplan;
	CString	m_Gatebel;
	CString	m_Gatechrt;
	CString	m_EquipmentChart;
	CString m_Poschrt;
	CString	m_Regnchrt;
	CString	m_Preplanttbl;
	CString	m_Requests;
	CString	m_Staffchart;
	CString	m_Stafflist;
	//}}AFX_DATA

	CString imMode;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(SUViewsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(SUViewsDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif // _SETUP_VIEWS_DLG_
