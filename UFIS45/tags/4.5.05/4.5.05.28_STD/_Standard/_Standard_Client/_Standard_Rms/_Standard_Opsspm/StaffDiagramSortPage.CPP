// stfdiaso.cpp : implementation file
//

#include <stdafx.h>
#include <resource.h>
#include <CCSGlobl.h>
#include <StaffDiagramSortPage.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Change this key identifiers for name used in the registry database
#define NUMBER_OF_SORTKEYS	8
static CString ogSortKeys[NUMBER_OF_SORTKEYS] =
	{ "Schichtbegin", "Schichtend", "Schichtcode", "Team", "Name", "Dienstrang", "WorkGroupType", "none" };
#define NOSORT	(NUMBER_OF_SORTKEYS - 1)	// must be "none"

/////////////////////////////////////////////////////////////////////////////
// StaffDiagramSortPage property page

IMPLEMENT_DYNCREATE(StaffDiagramSortPage, CPropertyPage)

StaffDiagramSortPage::StaffDiagramSortPage() : CPropertyPage(StaffDiagramSortPage::IDD)
{
	//{{AFX_DATA_INIT(StaffDiagramSortPage)
	m_SortOrder0 = -1;
	m_SortOrder1 = -1;
	m_SortOrder2 = -1;
	//}}AFX_DATA_INIT
	omTitle = GetString(IDS_STRING32900);
	m_psp.pszTitle = omTitle;
	m_psp.dwFlags |= PSP_USETITLE;
}

StaffDiagramSortPage::~StaffDiagramSortPage()
{
}

void StaffDiagramSortPage::DoDataExchange(CDataExchange* pDX)
{
	// Extended data exchange -- for member variables with Value type
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		omSortOrders.SetSize(3);
		m_SortOrder0 = GetSortOrder(omSortOrders[0]);
		m_SortOrder1 = GetSortOrder(omSortOrders[1]);
		m_SortOrder2 = GetSortOrder(omSortOrders[2]);
	}

	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(StaffDiagramSortPage)
	DDX_Radio(pDX, IDC_RADIO1, m_SortOrder0);
	DDX_Radio(pDX, IDC_RADIO8, m_SortOrder1);
	DDX_Radio(pDX, IDC_RADIO15, m_SortOrder2);
	//}}AFX_DATA_MAP

	// Extended data validation
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		omSortOrders.RemoveAll();
		omSortOrders.Add(GetSortKey(m_SortOrder0));
		omSortOrders.Add(GetSortKey(m_SortOrder1));
		omSortOrders.Add(GetSortKey(m_SortOrder2));
	}
}


BEGIN_MESSAGE_MAP(StaffDiagramSortPage, CPropertyPage)
	//{{AFX_MSG_MAP(StaffDiagramSortPage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// StaffDiagramSortPage message handlers

BOOL StaffDiagramSortPage::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	BOOL bResult = CPropertyPage::OnCommand(wParam, lParam);

	if (HIWORD(wParam) == BN_CLICKED)
	{
		CancelToClose();
		UpdateData();	// caution: this assume there is always no error

		// Disallow using the same sorting order more than once
		if (m_SortOrder1 == m_SortOrder0)
			omSortOrders[1] = "none";
		if (m_SortOrder2 == m_SortOrder0 || m_SortOrder2 == m_SortOrder1)
			omSortOrders[2] = "none";
		UpdateData(FALSE);
	}
	return bResult;
}

/////////////////////////////////////////////////////////////////////////////
// StaffDiagramSortPage -- helper routines

int StaffDiagramSortPage::GetSortOrder(const char *pcpSortKey)
{
	for (int i = 0; i < NUMBER_OF_SORTKEYS; i++)
		if (ogSortKeys[i] == pcpSortKey)
			return i;

	// If there is no sorting order matched, assume "none" for no sorting
	return NOSORT;
}

CString StaffDiagramSortPage::GetSortKey(int ipSortOrder)
{
	if (0 <= ipSortOrder && ipSortOrder <= NUMBER_OF_SORTKEYS-1)
		return ogSortKeys[ipSortOrder];

	return "";	// invalid sort order
}

BOOL StaffDiagramSortPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

//	SetWindowText(GetString(IDS_STRING32900));
	CWnd *polWnd = GetDlgItem(IDC_TXT7_SORTSTART); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33015));
	}
	polWnd = GetDlgItem(IDC_TXT7_SORTEND); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING33016));
	}
	polWnd = GetDlgItem(IDC_TXT7_SORTCODE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61612));
	}
	polWnd = GetDlgItem(IDC_TXT7_SORTTEAM); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61611));
	}
	polWnd = GetDlgItem(IDC_TXT7_SORTNAME); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING32823));
	}
	polWnd = GetDlgItem(IDC_TXT7_SORTGRADE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61613));
	}
	polWnd = GetDlgItem(IDC_TXT7_SORTNONE); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STRING61328));
	}
	polWnd = GetDlgItem(IDC_SORTINFO); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STAFFGANTT_SORTINFO));
	}
	polWnd = GetDlgItem(IDC_TXT7_SORTWORKGROUP); 
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_STAFFSORT_WORKGROUPTYPE));
	}

	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
