// Class for Job Types
//
//
#ifndef _CEDAJTYDATA_H_
#define _CEDAJTYDATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>


struct JtyDataStruct
{
	long	Urno;		// Unique Record Number
	char	Name[9];	// Name of Job Type eg. "POL"
	char	Dscr[81];	// Job Type Description eg. "Pool Job"
};

typedef struct JtyDataStruct JTYDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaJtyData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <JTYDATA> omData;
	CMapPtrToPtr omUrnoMap;
	CString GetTableName(void);

// Operations
public:
	CedaJtyData();
	~CedaJtyData();

	CCSReturnCode ReadJtyData();
	JTYDATA *GetJtyByUrno(long lpUrno);
	CString GetJtyNameByUrno(long lpUrno);
	long GetJtyUrnoByName(const char *pcpName);

private:
	void AddJtyInternal(JTYDATA &rrpJty);
	void ClearAll();
};


extern CedaJtyData ogJtyData;
#endif _CEDAJTYDATA_H_
