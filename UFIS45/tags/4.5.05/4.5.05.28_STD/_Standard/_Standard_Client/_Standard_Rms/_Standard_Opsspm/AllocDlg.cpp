// AllocDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <AllocDlg.h>
#include <BasicData.h>
#include <DataSet.h>
#include <AllocData.h>
#include <CedaFlightData.h>
#include <CedaPolData.h>
#include <CedaAcrData.h>
#include <CedaCicData.h>
#include <CedaCcaData.h>
#include <CedaGatData.h>
#include <CedaPstData.h>
#include <CedaWgpData.h>
#include <TeamDelegationDlg.h>
#include <CedaSerData.h>
#include <CedaRudData.h>
#include <afxtempl.h>
#include <TimePacket.h>
#include <Settings.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAllocDlg dialog
/*
static int CompareFlightStartTime(const FLIGHTDATA **pppFlight1, const FLIGHTDATA **pppFlight2);

static int CompareFlightStartTime(const FLIGHTDATA **pppFlight1, const FLIGHTDATA **pppFlight2)
{
	CTime olTime1 = !strcmp((**pppFlight1).Adid,"A") ? ((**pppFlight1).Tifa + CTimeSpan(0,0,20,0)) : ((**pppFlight1).Tifd - CTimeSpan(0,1,0,0));
	CTime olTime2 = !strcmp((**pppFlight2).Adid,"A") ? ((**pppFlight2).Tifa + CTimeSpan(0,0,20,0)) : ((**pppFlight2).Tifd - CTimeSpan(0,1,0,0));
	return (int)(olTime1.GetTime() - olTime2.GetTime());
}
*/

const CString TERMINAL2 = "T2";
const CString TERMINAL3 = "T3";

static int CompareDemandStartTime(const DEMANDDATA **pppDemand1, const DEMANDDATA **pppDemand2);

static int CompareDemandStartTime(const DEMANDDATA **pppDemand1, const DEMANDDATA **pppDemand2)
{
	int ilRc = (int)((**pppDemand1).Debe.GetTime() - (**pppDemand2).Debe.GetTime());
	if (ilRc == 0)
		ilRc = (int)((**pppDemand1).Urno - (**pppDemand2).Urno);
	return ilRc;
}


CAllocDlg::CAllocDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAllocDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAllocDlg)
	m_FlightCarrier = _T("");
	m_FlightNum = _T("");
	m_FlightSuffix = _T("");
	m_AllocUnitRadioButtons = 0;
	m_Description = _T("");
	m_EndDate = NULL;
	m_EndTime = NULL;
	m_StartDate = NULL;
	m_StartTime = NULL;
	m_Pool = _T("");
	m_SelectTemplate = ogBasicData.bmDelegateFlights ? TRUE : FALSE;
	//}}AFX_DATA_INIT

	omStartTime = TIMENULL;
	omEndTime = TIMENULL;
	imPrevRadioButtonID = -1;
	imRadioButtonID = -1;
	bmTeamAlloc = false; // true if a team is being allocated
	bmValidateData = true;
}

CAllocDlg::~CAllocDlg()
{
	omFlightData.DeleteAll();
}

void CAllocDlg::DoDataExchange(CDataExchange* pDX)
{
	// Extended data exchange -- for member variables with Value type
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		m_StartDate = omStartTime;
		m_StartTime = omStartTime;
		m_EndDate = omEndTime;
		m_EndTime = omEndTime;
	}

	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAllocDlg)
	DDX_Control(pDX, IDC_DESCRIPTION, m_DescriptionCtrl);
	DDX_Control(pDX, IDC_EMP_LIST, m_EmpList);
	DDX_Control(pDX, IDC_RESULTS_LIST, m_ResultsList);
	DDX_Text(pDX, IDC_FLIGHTCARRIER, m_FlightCarrier);
	DDV_MaxChars(pDX, m_FlightCarrier, 2);
	DDX_Text(pDX, IDC_FLIGHTNUM, m_FlightNum);
	DDV_MaxChars(pDX, m_FlightNum, 5);
	DDX_Text(pDX, IDC_FLIGHTSUFFIX, m_FlightSuffix);
	DDV_MaxChars(pDX, m_FlightSuffix, 1);
	DDX_Radio(pDX, IDC_RADIO_FLIGHT, m_AllocUnitRadioButtons);
	DDX_Text(pDX, IDC_DESCRIPTION, m_Description);
	DDX_CCSddmmyy(pDX, IDC_ENDDATE, m_EndDate);
	DDX_CCSTime(pDX, IDC_ENDTIME, m_EndTime);
	DDX_CCSddmmyy(pDX, IDC_STARTDATE, m_StartDate);
	DDX_CCSTime(pDX, IDC_STARTTIME, m_StartTime);
	DDX_Text(pDX, IDC_POOL, m_Pool);
	DDX_Control(pDX, IDC_ALLOCUNIT_LIST, m_AllocUnitList);
	DDX_Check(pDX, IDC_SELECTTEMPLATE, m_SelectTemplate);
	DDX_Check(pDX, IDC_CHECK_T2, m_CheckT2); //Singapore
	DDX_Check(pDX, IDC_CHECK_T3, m_CheckT3); //Singapore
	//}}AFX_DATA_MAP

	// Extended data validation
	if(pDX->m_bSaveAndValidate == TRUE)
	{
		// Convert the date and time back
		omStartTime = CTime(m_StartDate.GetYear(), m_StartDate.GetMonth(),
			m_StartDate.GetDay(), m_StartTime.GetHour(), m_StartTime.GetMinute(),
			m_StartTime.GetSecond());
		omEndTime = CTime(m_EndDate.GetYear(), m_EndDate.GetMonth(),
			m_EndDate.GetDay(),	m_EndTime.GetHour(), m_EndTime.GetMinute(),
			m_EndTime.GetSecond());
		if(omEndTime <= omStartTime && bmValidateData)	// don't allow end time greater than start time
		{
			// "Ung�ltige Zeit"
			MessageBox(GetString(IDS_STRING61213), NULL, MB_ICONEXCLAMATION);
			pDX->PrepareEditCtrl(IDC_ENDTIME);
			pDX->Fail();
		}
	}
}


BEGIN_MESSAGE_MAP(CAllocDlg, CDialog)
	//{{AFX_MSG_MAP(CAllocDlg)
	ON_EN_CHANGE(IDC_DESCRIPTION, OnChangeDescription)
	ON_EN_CHANGE(IDC_FLIGHTCARRIER, OnChangeFlightcarrier)
	ON_EN_CHANGE(IDC_FLIGHTNUM, OnChangeFlightnum)
	ON_EN_CHANGE(IDC_FLIGHTSUFFIX, OnChangeFlightsuffix)
	ON_LBN_DBLCLK(IDC_RESULTS_LIST, OnDblclkResultsList)
	ON_LBN_SELCHANGE(IDC_RESULTS_LIST, OnSelchangeResultsList)
	ON_CBN_SELCHANGE(IDC_ALLOCUNIT_LIST, OnSelchangeAllocUnitList)
	ON_COMMAND_RANGE(IDC_RADIO_FLIGHT,IDC_RADIO_OTHER,OnCommandRange)
	ON_EN_KILLFOCUS(IDC_STARTDATE, OnKillfocusStartdate)
	ON_EN_KILLFOCUS(IDC_STARTTIME, OnKillfocusStarttime)
	ON_EN_KILLFOCUS(IDC_ENDDATE, OnKillfocusEnddate)
	ON_EN_KILLFOCUS(IDC_ENDTIME, OnKillfocusEndtime)
	ON_BN_CLICKED(IDC_CHECK_T2, OnCheckT2) //Singapore
	ON_BN_CLICKED(IDC_CHECK_T3, OnCheckT3) //Singapore

	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAllocDlg message handlers

// used by the class that opens this dialog - add employee (pool job URNO) to the lsit of emps
void CAllocDlg::AddPoolJobUrno(long lpPoolJobUrno)
{
	JOBDATA *prlPoolJob;
	EMPDATA *prlEmp;
	if ((prlPoolJob = ogJobData.GetJobByUrno(lpPoolJobUrno)) != NULL &&
		(prlEmp = ogEmpData.GetEmpByPeno(prlPoolJob->Peno)) != NULL)
	{
		omPoolJobs.Add(prlPoolJob);

		if(prlPoolJob->Acfr < omStartTime || omStartTime == TIMENULL)
		{
			omStartTime = prlPoolJob->Acfr;
		}
		if(prlPoolJob->Acto > omEndTime || omEndTime == TIMENULL)
		{
			omEndTime = prlPoolJob->Acto;
		}
	}
}


BOOL CAllocDlg::OnInitDialog() 
{
	m_Pool = omPool;

	CDialog::OnInitDialog();

	// set the text displayed depending on the language defined
	SetWindowText(GetString(IDS_STRING_ALLOCDLG_CAPTION));
	GetDlgItem(IDC_GROUP_ALLOCUNIT)->SetWindowText(GetString(IDS_STRING_ALLOCDLG_ALLOCUNIT));
	GetDlgItem(IDC_RADIO_FLIGHT)->SetWindowText(GetString(IDS_STRING_ALLOCDLG_FLIGHT));
	GetDlgItem(IDC_RADIO_GATE)->SetWindowText(GetString(IDS_STRING_ALLOCDLG_GATE));
	GetDlgItem(IDC_RADIO_GATEAREA)->SetWindowText(GetString(IDS_STRING_ALLOCDLG_GATEAREA));
	GetDlgItem(IDC_RADIO_CCIDESK)->SetWindowText(GetString(IDS_STRING_ALLOCDLG_CCIDESK));
	GetDlgItem(IDC_RADIO_POOL)->SetWindowText(GetString(IDS_STRING_ALLOCDLG_POOL));
	GetDlgItem(IDC_RADIO_REGN)->SetWindowText(GetString(IDS_STRING_ALLOCDLG_REGN));
	GetDlgItem(IDC_RADIO_POS)->SetWindowText(GetString(IDS_STRING_ALLOCDLG_POS));
	GetDlgItem(IDC_RADIO_TEAM)->SetWindowText(GetString(IDS_STRING_ALLOCDLG_TEAM));
	GetDlgItem(IDC_RADIO_FID)->SetWindowText(GetString(IDS_STRING_ALLOCDLG_FID));
	GetDlgItem(IDC_RADIO_OTHER)->SetWindowText(GetString(IDS_STRING_ALLOCDLG_OTHER));
	GetDlgItem(IDC_CREATEAS)->SetWindowText(GetString(IDS_CREATEAS));
	GetDlgItem(IDC_FROM)->SetWindowText(GetString(IDS_STRING_ALLOCDLG_FROM));
	GetDlgItem(IDC_TO)->SetWindowText(GetString(IDS_STRING_ALLOCDLG_TO));
	GetDlgItem(IDOK)->SetWindowText(GetString(IDS_STRING61697)); // OK
	GetDlgItem(IDCANCEL)->SetWindowText(GetString(IDS_STRING61698)); // Cancel

	if(ogBasicData.bmDelegateFlights && (ogBasicData.IsSinApron1() == true || ogBasicData.IsSinApron2() == true))
	{
		GetDlgItem(IDC_SELECTTEMPLATE)->SetWindowText(GetString(IDS_SELECTTEMPLATE));
		GetDlgItem(IDC_SELECTTEMPLATE)->ShowWindow(SW_SHOW);
	}
	else
	{
		GetDlgItem(IDC_SELECTTEMPLATE)->ShowWindow(SW_HIDE);
	}



	// the name of rule services can be used to set the description and time fields for "other" jobs
	bmLoadServicesForOtherJobs = false;
	char pclTmpText[100];
    GetPrivateProfileString(pcgAppName, "LOAD_SERVICES_FOR_OTHERJOBS", "NO",pclTmpText, sizeof pclTmpText, ogBasicData.GetConfigFileName());
	if(!stricmp(pclTmpText,"YES"))
	{
		bmLoadServicesForOtherJobs = true;
	}

	// depending on which gantts are defined, disable/enble the check boxes
	m_AllocUnitRadioButtons = 6; // Other checkbox selected by default
	if(!ogBasicData.bmDisplayRegistrations)
	{
		GetDlgItem(IDC_RADIO_REGN)->EnableWindow(FALSE);
	}
	else
	{
		m_AllocUnitRadioButtons = 5; // Registration checkbox selected by default
	}

	if(ogBasicData.bmDisplayCheckins)
	{
		m_AllocUnitRadioButtons = 1; // Checkin checkbox selected by default
	}

	if(!ogBasicData.bmDisplayGates && !ogBasicData.bmDisplayPositions && !ogBasicData.bmDisplayCheckins)
	{
		GetDlgItem(IDC_RADIO_FLIGHT)->EnableWindow(FALSE);
		SetResultsList(SW_HIDE);
	}
	else
	{
		m_AllocUnitRadioButtons = 0; // Flight checkbox selected by default
	}

	CStringArray olAllocUnitTypes;

	if(!ogBasicData.bmDisplayCheckins)
	{
		GetDlgItem(IDC_RADIO_CCIDESK)->EnableWindow(FALSE);
	}
	else
	{
		olAllocUnitTypes.Add(ogBasicData.GetStringForAllocUnitType(ALLOCUNITTYPE_CIC));
	}

	if(!ogBasicData.bmDisplayGates)
	{
		GetDlgItem(IDC_RADIO_GATE)->EnableWindow(FALSE);
		GetDlgItem(IDC_RADIO_GATEAREA)->EnableWindow(FALSE);
	}
	else
	{
		olAllocUnitTypes.Add(ogBasicData.GetStringForAllocUnitType(ALLOCUNITTYPE_GATE));
	}

	if(!ogBasicData.bmDisplayPositions)
	{
		GetDlgItem(IDC_RADIO_POS)->EnableWindow(FALSE);
	}
	else
	{
		olAllocUnitTypes.Add(ogBasicData.GetStringForAllocUnitType(ALLOCUNITTYPE_PST));
	}

	int ilNumAllocUnitTypes = olAllocUnitTypes.GetSize();
	if(ilNumAllocUnitTypes > 0)
	{
		for(int ilAUT = 0; ilAUT < ilNumAllocUnitTypes; ilAUT++)
		{
			m_AllocUnitList.AddString(olAllocUnitTypes[ilAUT]);
		}

		CString olAllocUnitType = ogSettings.GetSetting(ALLOCDLG_ALLOCUNITTYPE);
		if(ogBasicData.bmDelegateFlights)
		{
			olAllocUnitType = ogBasicData.GetStringForAllocUnitType(ALLOCUNITTYPE_PST);
		}
		if(m_AllocUnitList.SelectString(-1,olAllocUnitType) != CB_ERR)
		{
			omAllocUnitType = ogBasicData.GetAllocUnitTypeFromString(olAllocUnitType);
		}
		else
		{
			m_AllocUnitList.SetCurSel(0);
			omAllocUnitType = ogBasicData.GetAllocUnitTypeFromString(olAllocUnitTypes[0]);
			ogSettings.AddSetting(ALLOCDLG_ALLOCUNITTYPE,olAllocUnitTypes[0]);
		}

	}
	else
	{
		m_AllocUnitList.ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CREATEAS)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SELECTTEMPLATE)->ShowWindow(SW_HIDE);
		ogSettings.AddSetting(ALLOCDLG_ALLOCUNITTYPE,CString(""));
	}


	if(!ogBasicData.bmDisplayTeams)
	{
		GetDlgItem(IDC_RADIO_TEAM)->EnableWindow(FALSE);
	}

	//Singapore
	CWnd* polWnd = GetDlgItem(IDC_CHECK_T2);
	if(polWnd != NULL)
	{
		polWnd->ShowWindow(SW_HIDE);
	}
	polWnd = GetDlgItem(IDC_CHECK_T3);
	if(polWnd != NULL)
	{
		polWnd->ShowWindow(SW_HIDE);
	}	

	m_ResultsList.GetWindowRect(omSmallRect);
	ScreenToClient(omSmallRect);

	omLargeRect = omSmallRect;
	CRect olTmpRect;
	m_AllocUnitList.GetWindowRect(olTmpRect);
	ScreenToClient(olTmpRect);
	omLargeRect.top = olTmpRect.top;
	


	UpdateData(FALSE);


	CRect olCciDeskRadioButtonRect;
	GetDlgItem(IDC_RADIO_CCIDESK)->GetWindowRect(olCciDeskRadioButtonRect);
	ScreenToClient(olCciDeskRadioButtonRect);
	imTopOfCciDeskRadioButton = olCciDeskRadioButtonRect.top;
	GetDlgItem(IDC_DESCRIPTION)->GetWindowRect(omDescriptionRect);
	ScreenToClient(omDescriptionRect);
	SetEditControlFields();
	SetEmpList();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CAllocDlg::SetResultsList(int ipStatus)
{
	if(ipStatus == SW_HIDE)
	{
		if(m_AllocUnitList.IsWindowVisible())
		{
			m_AllocUnitList.ShowWindow(SW_HIDE);
			GetDlgItem(IDC_CREATEAS)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_SELECTTEMPLATE)->ShowWindow(SW_HIDE);
			m_ResultsList.MoveWindow(omLargeRect);
		}
	}
	else
	{
		if(!m_AllocUnitList.IsWindowVisible())
		{
			m_AllocUnitList.ShowWindow(SW_SHOW);
			GetDlgItem(IDC_CREATEAS)->ShowWindow(SW_SHOW);
			if(ogBasicData.IsSinApron1() == false && ogBasicData.IsSinApron2() == false)
			{
				GetDlgItem(IDC_SELECTTEMPLATE)->ShowWindow(SW_HIDE);
			}
			else
			{
				GetDlgItem(IDC_SELECTTEMPLATE)->ShowWindow(SW_SHOW);
			}
			m_ResultsList.MoveWindow(omSmallRect);
		}
	}
}

// loop through the pool jobs URNOs and add emps to the list of emps
void CAllocDlg::SetEmpList(void)
{
	EMPDATA *prlTmpEmp;
	JOBDATA *prlPoolJob;
	EMPDATA *prlEmp;
	CString olText;

	int ilNumPoolJobs = omPoolJobs.GetSize();
	for(int ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
	{
		prlPoolJob = &omPoolJobs[ilPoolJob];
		if((prlEmp = ogEmpData.GetEmpByPeno(prlPoolJob->Peno)) == NULL ||
			omEmpList.Lookup((void *) prlEmp->Urno, (void *&) prlTmpEmp))
			continue;

		// don't add the emp more than once if they have >1 pool jobs
		omEmpList.SetAt((void *) prlEmp->Urno, (void *&) prlEmp);
		olText = ogEmpData.GetEmpName(prlEmp);
		int ilLine = m_EmpList.AddString(olText);
		if(ilLine != LB_ERR)
		{
			m_EmpList.SetItemData(ilLine,(DWORD)prlPoolJob);
		}
	}

	m_EmpList.SetSel(-1,TRUE);
}

// respond to a button click by moving the edit field(s) to the check box selected
BOOL CAllocDlg::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	if (HIWORD(wParam) == BN_CLICKED)
	{
		SetEditControlFields();
	}
	
	return CDialog::OnCommand(wParam, lParam);
}

void CAllocDlg::OnCommandRange(UINT) 
{
	UpdateData(TRUE);
	m_Description = "";
	UpdateData(FALSE);
}

// move the edit field(s) to the check box selected
void CAllocDlg::SetEditControlFields()
{
	imPrevRadioButtonID = imRadioButtonID;
	imRadioButtonID = GetCheckedRadioButton(IDC_RADIO_FLIGHT, IDC_RADIO_OTHER);

	if(imPrevRadioButtonID != imRadioButtonID)
	{
		omAllocUnits.RemoveAll();
		GetDlgItem(IDC_DESCRIPTION)->SetWindowText("");
		if(ogBasicData.IsPaxServiceT2() == true)
		{
			if(imRadioButtonID != IDC_RADIO_FID)
			{	
				CWnd* polWnd = GetDlgItem(IDC_CHECK_T2);
				if(polWnd != NULL)
				{
					polWnd->ShowWindow(SW_HIDE);
				}
				polWnd = GetDlgItem(IDC_CHECK_T3);
				if(polWnd != NULL)
				{
					polWnd->ShowWindow(SW_HIDE);
				}
			}
		}

		if(imRadioButtonID == IDC_RADIO_FLIGHT)
		{
			GetDlgItem(IDC_DESCRIPTION)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_FLIGHTCARRIER)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_FLIGHTNUM)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_FLIGHTSUFFIX)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_FLIGHTCARRIER)->SetFocus();
			SetResultsList(SW_SHOW);

			OnChangeFlightFields();
		}
		else if(imRadioButtonID > 0)
		{
			if(ogBasicData.IsPaxServiceT2() == true)
			{
				if(imRadioButtonID == IDC_RADIO_FID)
				{	
					CWnd* polWnd = GetDlgItem(IDC_CHECK_T2);
					if(polWnd != NULL)
					{
						polWnd->ShowWindow(SW_SHOW);
					}
					polWnd = GetDlgItem(IDC_CHECK_T3);
					if(polWnd != NULL)
					{
						polWnd->ShowWindow(SW_SHOW);
					}
				}
			}
	
			GetDlgItem(IDC_DESCRIPTION)->ShowWindow(SW_SHOW);
			GetDlgItem(IDC_FLIGHTCARRIER)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_FLIGHTNUM)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_FLIGHTSUFFIX)->ShowWindow(SW_HIDE);
			SetResultsList(SW_HIDE);

			CRect olRadioButtonRect;
			GetDlgItem(imRadioButtonID)->GetWindowRect(olRadioButtonRect);
			ScreenToClient(olRadioButtonRect);
			CRect olDescriptionRect = omDescriptionRect;
			int ilHeight = olDescriptionRect.bottom - olDescriptionRect.top;
			olDescriptionRect.top = olRadioButtonRect.top;
			olDescriptionRect.bottom = olRadioButtonRect.top + ilHeight;
//			olDescriptionRect.OffsetRect(0, olRadioButtonRect.top - imTopOfCciDeskRadioButton);
			GetDlgItem(IDC_DESCRIPTION)->MoveWindow(olDescriptionRect);
			GetDlgItem(IDC_DESCRIPTION)->SetFocus();

			OnChangeDescription();
		}
		else
		{
			GetDlgItem(IDC_DESCRIPTION)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_FLIGHTCARRIER)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_FLIGHTNUM)->ShowWindow(SW_HIDE);
			GetDlgItem(IDC_FLIGHTSUFFIX)->ShowWindow(SW_HIDE);
			SetResultsList(SW_HIDE);
		}
	}
}


// edit field was updated so update the results list box
void CAllocDlg::OnChangeDescription() 
{
	CString olText;
	GetDlgItem(IDC_DESCRIPTION)->GetWindowText(olText);
	UpdateListField(olText);
}


// carrier edit field was updated so update the results list box
void CAllocDlg::OnChangeFlightcarrier() 
{
	OnChangeFlightFields();
}

// flight num edit field was updated so update the results list box
void CAllocDlg::OnChangeFlightnum() 
{
	OnChangeFlightFields();
}

// flight suffix edit field was updated so update the results list box
void CAllocDlg::OnChangeFlightsuffix() 
{
	OnChangeFlightFields();
}

// a flight edit field was updated so update the results list box
void CAllocDlg::OnChangeFlightFields() 
{
	CString olCarrier, olFlightNum, olSuffix, olText;
	GetDlgItem(IDC_FLIGHTCARRIER)->GetWindowText(olCarrier);
	GetDlgItem(IDC_FLIGHTNUM)->GetWindowText(olFlightNum);
	GetDlgItem(IDC_FLIGHTSUFFIX)->GetWindowText(olSuffix);
	UpdateFlightList(olCarrier, olFlightNum, olSuffix);
}

void CAllocDlg::UpdateListField(CString &ropText)
{
	//CArray <long,long> olItemData; //Commented Singapore
//	if(ropText.IsEmpty())
	{
//		if(imRadioButtonID != imPrevRadioButtonID)
		{
			omAllocUnits.RemoveAll();
			int ilNumPools, ilPool,ilC;

			switch(imRadioButtonID)
			{
				case IDC_RADIO_CCIDESK:
					ogBasicData.GetAlocUnitsByPool(ALLOCUNITTYPE_CIC, omPool, omAllocUnits);
					break;
				case IDC_RADIO_POOL:
					ogPolData.GetPoolNames(omAllocUnits);
					ilNumPools = omAllocUnits.GetSize();
					for(ilPool = (ilNumPools-1); ilPool >= 0; ilPool--)
					{
						if(omAllocUnits[ilPool] == omPool)
						{
							omAllocUnits.RemoveAt(ilPool);
						}
					}
					SetStartDate();
					break;
				case IDC_RADIO_GATE:
					ogBasicData.GetAlocUnitsByPool(ALLOCUNITTYPE_GATE, omPool, omAllocUnits);
					if(omAllocUnits.GetSize() <= 0)
					{
						ogGatData.GetAllGateNames(omAllocUnits);
					}
					SetStartDate();
					break;
				case IDC_RADIO_POS:
					ogBasicData.GetAlocUnitsByPool(ALLOCUNITTYPE_PST, omPool, omAllocUnits);
					if(omAllocUnits.GetSize() <= 0)
					{
						ogPstData.GetAllPstNames(omAllocUnits);
					}
					SetStartDate();
					break;
				case IDC_RADIO_TEAM:
					ogWgpData.GetAllTeams(omAllocUnits);
					SetStartDate();
					break;
				case IDC_RADIO_GATEAREA:
					ogAllocData.GetGroupNamesByGroupType(ALLOCUNITTYPE_GATEGROUP,omAllocUnits);
					SetStartDate();
					break;
				case IDC_RADIO_REGN:
					ogAcrData.GetAcrNames(omAllocUnits);
					break;
				case IDC_RADIO_FID :
					{
						omDemands.DeleteAll();
						ogDemandData.GetFlightIndependentDemands(omDemands);
						for (ilC = 0; ilC < omDemands.GetSize();ilC++)
						{
							DEMANDDATA *prlDem = &omDemands[ilC];
							if (!prlDem)
								continue;

							RUDDATA* prlRud = ogRudData.GetRudByUrno(prlDem->Urud);
							if (!prlRud)
								continue;

							SERDATA* prlSer = ogSerData.GetSerByUrno(prlRud->Ughs);
							if (!prlSer)
								continue;

							if(!IsOverlapped(prlDem->Debe, prlDem->Deen, omStartTime, omEndTime))
								continue;

							CString olName;
							olName.Format("%s  %s - %s",(const char *)prlSer->Snam, prlDem->Debe.Format("%H%M/%d"), prlDem->Deen.Format("%H%M/%d"));
							omAllocUnits.Add(olName);
							omItemData.Add(prlDem->Urno); //Changed from olItemData to omItemData Singapore
						}
						
					}
					break;
				case IDC_RADIO_OTHER:
					if(bmLoadServicesForOtherJobs)
					{
						ogSerData.GetAllServiceNames(omAllocUnits,TRUE);
					}
					SetStartDate();
					break;
				default:
					break;
			}
		}
	}

	m_ResultsList.ResetContent();

	CString olFilter = ropText, olAlocUnit;
	olFilter.MakeUpper();
	int ilFilterLen = olFilter.GetLength();

	int ilNumItems = omItemData.GetSize();
	int ilNumUnits = omAllocUnits.GetSize();
	for(int ilUnit = 0; ilUnit < ilNumUnits; ilUnit++)
	{
		olAlocUnit = omAllocUnits[ilUnit];
		olAlocUnit.MakeUpper();
		if(olFilter.IsEmpty() || !strncmp(olAlocUnit,olFilter,ilFilterLen))
		{
			int ilIndex = m_ResultsList.AddString(omAllocUnits[ilUnit]);
			if(ilIndex != LB_ERR && ilUnit < ilNumItems)
			{
				m_ResultsList.SetItemData(ilIndex,omItemData[ilUnit]);
			}
		}
	}

	if(imRadioButtonID != IDC_RADIO_OTHER || bmLoadServicesForOtherJobs)
	{
		if(m_ResultsList.GetCount() <= 0)
		{
			CString olText = GetString(IDS_NODATAFOUND);
			m_ResultsList.AddString(olText);
		}
		else if(m_ResultsList.GetCount() == 1)
		{
			m_ResultsList.SetCurSel(0);
		}
	}
	omDemands.DeleteAll();
}


void CAllocDlg::UpdateFlightList(CString opCarrier,CString opFlightNum,CString opSuffix)
{
	if(UpdateData(TRUE))
	{
		opCarrier.MakeUpper();
		int ilCarrierLen = opCarrier.GetLength();
		opFlightNum.MakeUpper();
		int ilFlightNumLen = opFlightNum.GetLength();
		opSuffix.MakeUpper();
		int ilSuffixLen = opSuffix.GetLength();

		int ilNumFlights, ilFlight, ilLine;
		CTime olStartTime, olEndTime;
		FLIGHTDATA *prlFlight;

		if(opCarrier.IsEmpty() && opFlightNum.IsEmpty() && opSuffix.IsEmpty())
		{
			omFlights.RemoveAll();

			ilNumFlights = ogFlightData.omData.GetSize();
			for(ilFlight = 0; ilFlight < ilNumFlights; ilFlight++)
			{
				prlFlight = &ogFlightData.omData[ilFlight];
				CString olAlid = ogBasicData.GetFlightAlidByAloc(prlFlight, omAllocUnitType);
				if(olAlid.IsEmpty() || ogBasicData.IsInPool(omAllocUnitType,olAlid,omPool))
				{
					olStartTime = ogFlightData.IsArrival(prlFlight)?prlFlight->Tifa :prlFlight->Tifd;
					olEndTime = olStartTime;
					CCSPtrArray <DEMANDDATA> olDemands;
					ogDemandData.GetDemandsByFlur(olDemands, prlFlight->Urno, olAlid, omAllocUnitType, PERSONNELDEMANDS);
					int ilNumDemands = olDemands.GetSize();
					for(int ilD = 0; ilD < ilNumDemands; ilD++)
					{
						DEMANDDATA *prlDemand = &olDemands[ilD];
						if(!ogJodData.DemandHasJobs(prlDemand->Urno))
						{
							if(prlDemand->Debe < olStartTime)
								olStartTime = prlDemand->Debe;
							if(prlDemand->Deen > olEndTime)
								olEndTime = prlDemand->Deen;
						}
					}
					if(IsOverlapped(olStartTime, olEndTime, omStartTime, omEndTime))
					{
						omFlights.Add(prlFlight);
					}
				}
			}
		}

		m_ResultsList.ResetContent();
		omFlightData.DeleteAll();
		CString olText;

		ilNumFlights = omFlights.GetSize();
		for(ilFlight = 0; ilFlight < ilNumFlights; ilFlight++)
		{
			prlFlight = &omFlights[ilFlight];
			if((opCarrier.IsEmpty() || !strncmp(opCarrier,prlFlight->Alc2,ilCarrierLen)) &&
				(opFlightNum.IsEmpty() || !strncmp(opFlightNum,prlFlight->Fltn,ilFlightNumLen)) &&
				(opSuffix.IsEmpty() || !strncmp(opSuffix,prlFlight->Flns,ilSuffixLen)))
			{
				if(ogFlightData.IsReturnFlight(prlFlight))
				{
					AddFlightToResultsList(prlFlight, ID_ARR_RETURNFLIGHT);
					AddFlightToResultsList(prlFlight, ID_DEP_RETURNFLIGHT);
				}
				else
				{
					AddFlightToResultsList(prlFlight);
				}
			}
		}


		if(m_ResultsList.GetCount() <= 0)
		{
			olText = GetString(IDS_NODATAFOUND);
			ilLine = m_ResultsList.AddString(olText);
			if(ilLine != LB_ERR)
			{
				m_ResultsList.SetItemData(ilLine,0L);
			}
		}
		else if(m_ResultsList.GetCount() == 1)
		{
			m_ResultsList.SetCurSel(0);
		}
	}
}

int CAllocDlg::AddFlightToResultsList(FLIGHTDATA *prpFlight, int ipReturnFlightType /*ID_NOT_RETURNFLIGHT*/)
{
	int ilLine = LB_ERR;
	if(prpFlight != NULL)
	{
		CString olFlightText, olFlightTime, olText, olUnit;
		if(ogFlightData.IsArrival(prpFlight) || ipReturnFlightType == ID_ARR_RETURNFLIGHT)
		{
			olFlightText = "(Arr)";
			olFlightTime = prpFlight->Tifa.Format("%H%M/%d");
		}
		else
		{
			olFlightText = "(Dep)";
			olFlightTime = prpFlight->Tifd.Format("%H%M/%d");
		}
		if(omAllocUnitType == ALLOCUNITTYPE_GATE)
			olUnit.Format("%s: <%s>",ogBasicData.GetStringForAllocUnitType(omAllocUnitType),ogFlightData.GetGate(prpFlight));
		else if(omAllocUnitType == ALLOCUNITTYPE_PST)
			olUnit.Format("%s: <%s>",ogBasicData.GetStringForAllocUnitType(omAllocUnitType),ogFlightData.GetPosition(prpFlight));
		else if(omAllocUnitType == ALLOCUNITTYPE_CIC)
			olUnit.Format("%s: <%s>",ogBasicData.GetStringForAllocUnitType(omAllocUnitType),ogCcaData.GetCounterRangeForFlight(prpFlight->Urno));
		olText.Format("%s %s %s  -->  %s",prpFlight->Fnum,olFlightText,olFlightTime,olUnit);

		if((ilLine = m_ResultsList.AddString(olText)) != LB_ERR)
		{
			ALLOCDLG_FLIGHTDATA *prlFlightData = new ALLOCDLG_FLIGHTDATA;
			prlFlightData->Urno = prpFlight->Urno;
			prlFlightData->ReturnFlightType = ipReturnFlightType;
			omFlightData.Add(prlFlightData);
			m_ResultsList.SetItemDataPtr(ilLine,prlFlightData);
		}
	}

	return ilLine;
}

void CAllocDlg::OnOK() 
{
	if(UpdateData(TRUE))
	{
		CString olSelectedText;
		int ilResult = IDCANCEL;

		long llSelectedUrno = 0L;
		int ilSelLine = m_ResultsList.GetCurSel();
		ALLOCDLG_FLIGHTDATA *prlFlightData = NULL;
		if(ilSelLine != LB_ERR && ilSelLine >= 0)
		{
			m_ResultsList.GetText(ilSelLine,olSelectedText);
			llSelectedUrno = (long) m_ResultsList.GetItemData(ilSelLine);
			prlFlightData = (ALLOCDLG_FLIGHTDATA *) m_ResultsList.GetItemDataPtr(ilSelLine);
		}

		CCSPtrArray <JOBDATA> olAllPoolJobs;		// all pool jobs per emp
		CDWordArray olPoolJobUrnos;	// one pool job per emp

		int ilNumSelEmps = m_EmpList.GetSelCount();
		if(ilNumSelEmps != LB_ERR && ilNumSelEmps > 0)
		{
			int *pilSelectedEmps = new int[ilNumSelEmps];
			m_EmpList.GetSelItems(ilNumSelEmps,pilSelectedEmps);
			JOBDATA *prlPoolJob = NULL;
			int ilNumPJ = omPoolJobs.GetSize();

			for(int ilEmp = 0; ilEmp < ilNumSelEmps; ilEmp++)
			{
				prlPoolJob = (JOBDATA *) m_EmpList.GetItemData(pilSelectedEmps[ilEmp]);

				olPoolJobUrnos.Add((DWORD) prlPoolJob->Urno);

				for(int ilPJ = 0; ilPJ < ilNumPJ; ilPJ++)
				{
					if(prlPoolJob->Ustf == omPoolJobs[ilPJ].Ustf)
					{
						olAllPoolJobs.Add(&omPoolJobs[ilPJ]);
					}
				}
			}

			delete [] pilSelectedEmps;
		}

		FLIGHTDATA *prlFlight = NULL;
		POLDATA *prlPool = NULL;
		WGPDATA *prlTeam = NULL;
		GATDATA *prlGate = NULL;
		CICDATA *prlCic = NULL;
		ACRDATA *prlAcr = NULL;
		PSTDATA *prlPosition = NULL;
		CString olErr;
		CString olGateArea, olOther, olCic, olGate, olRegn, olPool, olPosition, olAlid, olTeam;

		switch(imRadioButtonID)
		{
			case IDC_RADIO_FLIGHT:
				if(prlFlightData != NULL)
				{
					llSelectedUrno = prlFlightData->Urno;
					olAlid = ogBasicData.GetFlightAlidByAloc(llSelectedUrno,omAllocUnitType);
					prlFlight = ogFlightData.GetFlightByUrno(llSelectedUrno);
					if (prlFlight == NULL)
					{
						//"Flight not found!"
						MessageBox(GetString(IDS_STRING61215),NULL,MB_ICONEXCLAMATION|MB_OK);
					}
					else if(!olAlid.IsEmpty() && !ogBasicData.IsInPool(omAllocUnitType,olAlid,omPool))
					{
						//"%s not cannot be assigned to %s !"
						olErr.Format(GetString(IDS_STRING_CANNOTASSIGN),ogFlightData.GetGate(prlFlight),omPool);
						MessageBox(olErr,NULL,MB_ICONEXCLAMATION|MB_OK);
					}
					else
					{						
						if(m_SelectTemplate && (ogBasicData.IsSinApron1() == true || ogBasicData.IsSinApron2() == true))
						{
							ogBasicData.bmDelegatingFlight = true;
							ilResult = ogDataSet.CreateDelegatedFlightJob(this,olPoolJobUrnos, prlFlight->Urno, omAllocUnitType, olAlid, bmTeamAlloc, NULL, 
																	omStartTime, omEndTime, true, 0L, false, false, 0L,prlFlightData->ReturnFlightType);
							ogBasicData.bmDelegatingFlight = false;
						}
						else
						{
							ilResult = ogDataSet.CreateJobFlightFromDuty(this,olPoolJobUrnos, prlFlight->Urno, omAllocUnitType, olAlid, bmTeamAlloc, NULL, 
																TIMENULL, TIMENULL, true, 0L, false, false, 0L,prlFlightData->ReturnFlightType);
						}
					}
				}
				break;
			case IDC_RADIO_CCIDESK:
				olCic = GetSelectedText();
				prlCic = ogCicData.GetCicByName(olCic);
				if(prlCic == NULL)
				{
					//"CCI-Desk nicht gefunden!"
					olErr.Format("'%s'\n%s",olCic,GetString(IDS_STRING61217));
					MessageBox(olErr,NULL,MB_ICONEXCLAMATION|MB_OK);
				}
				else if(!ogBasicData.IsInPool(ALLOCUNITTYPE_CIC,prlCic->Cnam,omPool))
				{
					//"%s not cannot be assigned to %s !"
					olErr.Format(GetString(IDS_STRING_CANNOTASSIGN),prlCic->Cnam,omPool);
					MessageBox(olErr,NULL,MB_ICONEXCLAMATION|MB_OK);
				}
				else
				{
					ilResult = ogDataSet.CreateNormalJobs(this, JOBCCI, olPoolJobUrnos, ALLOCUNITTYPE_CIC, prlCic->Cnam, omStartTime, omEndTime, bmTeamAlloc);
					//ilResult = ogDataSet.CreateJobCciFromDuty(this,olPoolJobUrnos, prlCic->Cnam,omStartTime,omEndTime);
				}
				break;
			case IDC_RADIO_POOL:
				olPool = GetSelectedText();
				prlPool = ogPolData.GetPolByName(olPool);
				if (prlPool == NULL)
				{
					//"Pool nicht gefunden!"
					olErr.Format("'%s'\n%s",olPool,GetString(IDS_STRING61218));
					MessageBox(olErr,NULL,MB_ICONEXCLAMATION|MB_OK);
				}
				else if(!strcmp(prlPool->Name,omPool))
				{
					//"%s not cannot be assigned to %s !"
					olErr.Format(GetString(IDS_STRING_CANNOTASSIGN),prlPool->Name,omPool);
					MessageBox(olErr,NULL,MB_ICONEXCLAMATION|MB_OK);
				}
				else
				{
					ilResult = ogDataSet.CreateJobDetachFromDuty(this,olPoolJobUrnos,prlPool->Name,omStartTime,omEndTime);
				}
				break;
			case IDC_RADIO_GATE:
				olGate = GetSelectedText();
				prlGate = ogGatData.GetGatByName(olGate);
				if (prlGate == NULL)
				{
					//"Gate not found!"
					olErr.Format("'%s'\n%s",olGate,GetString(IDS_STRING_GATENOTFOUND));
					MessageBox(olErr,NULL,MB_ICONEXCLAMATION|MB_OK);
				}
				else if(!ogBasicData.IsInPool(ALLOCUNITTYPE_GATE,prlGate->Gnam,omPool))
				{
					//"%s not cannot be assigned to %s !"
					olErr.Format(GetString(IDS_STRING_CANNOTASSIGN),prlGate->Gnam,omPool);
					MessageBox(olErr,NULL,MB_ICONEXCLAMATION|MB_OK);
				}
				else
				{
					ilResult = ogDataSet.CreateNormalJobs(this, JOBGATE, olPoolJobUrnos, ALLOCUNITTYPE_GATE, prlGate->Gnam, omStartTime, omEndTime, bmTeamAlloc);
					//ilResult = ogDataSet.CreateGateJob(this,olPoolJobUrnos,omStartTime,omEndTime,prlGate->Gnam);
				}
				break;
			case IDC_RADIO_POS:
				olPosition = GetSelectedText();
				prlPosition = ogPstData.GetPstByName(olPosition);
				if (prlPosition == NULL)
				{
					//"Position not found!"
					olErr.Format("'%s'\n%s",olPosition,GetString(IDS_STRING_POSNOTFOUND));
					MessageBox(olErr,NULL,MB_ICONEXCLAMATION|MB_OK);
				}
				else if(!ogBasicData.IsInPool(ALLOCUNITTYPE_PST,prlPosition->Pnam,omPool))
				{
					//"%s not cannot be assigned to %s !"
					olErr.Format(GetString(IDS_STRING_CANNOTASSIGN),prlPosition->Pnam,omPool);
					MessageBox(olErr,NULL,MB_ICONEXCLAMATION|MB_OK);
				}
				else
				{
					ilResult = ogDataSet.CreateNormalJobs(this, JOBPST, olPoolJobUrnos, ALLOCUNITTYPE_PST, prlPosition->Pnam, omStartTime, omEndTime, bmTeamAlloc);
				}
				break;
			case IDC_RADIO_GATEAREA:
				olGateArea = GetSelectedText();
				if(olGateArea.IsEmpty())
				{
				}
				else
				{
					ilResult = ogDataSet.CreateJobGateAreaFromDuty(this,olPoolJobUrnos,olGateArea,omStartTime,omEndTime);	
				}
				break;
			case IDC_RADIO_TEAM:
				olTeam = GetSelectedText();
				prlTeam = ogWgpData.GetWgpByWgpc(olTeam);
				if (prlTeam == NULL)
				{
					//"Team nicht gefunden!"
					olErr.Format("'%s'\n%s",olTeam,GetString(IDS_TEAMNOTFOUND));
					MessageBox(olErr,NULL,MB_ICONEXCLAMATION|MB_OK);
				}
				else
				{
					// allow the user to select a pool and change the emp function codes if necessary
					CDWordArray olPoolJobUrnos2;
					CTeamDelegationDlg olTeamDelegationDlg;
					int ilNumPoolJobs = olAllPoolJobs.GetSize();
					JOBDATA *prlPoolJob;
					EMPDATA *prlEmp;
					CString olEmp, olFunction;
					for(int ilPoolJob = 0; ilPoolJob < ilNumPoolJobs; ilPoolJob++)
					{
						prlPoolJob = &olAllPoolJobs[ilPoolJob];
						if((prlEmp = ogEmpData.GetEmpByPeno(prlPoolJob->Peno)) == NULL)
							continue;

						olEmp = ogEmpData.GetEmpName(prlEmp);
						olTeamDelegationDlg.omEmpList.Add(olEmp);

						olFunction = ogBasicData.GetFunctionForPoolJob(prlPoolJob);
						olTeamDelegationDlg.omFunctionList.Add(olFunction);
						olPoolJobUrnos2.Add(prlPoolJob->Urno);
					}
					if(olPoolJobUrnos2.GetSize() > 0)
					{
						olTeamDelegationDlg.omPool = omPool;
						olTeamDelegationDlg.omTeam = prlTeam->Wgpc;
						olTeamDelegationDlg.omFrom = omStartTime;
						olTeamDelegationDlg.omTo = omEndTime;

						if((ilResult = olTeamDelegationDlg.DoModal()) == IDOK)
						{
							ilResult = ogDataSet.CreateGroupDelegation(this,olTeamDelegationDlg.omTeam,olTeamDelegationDlg.omPool,
																olPoolJobUrnos2,olTeamDelegationDlg.omFunctionList,
																olTeamDelegationDlg.omFrom,olTeamDelegationDlg.omTo);
						}
					}
				}
				break;
			case IDC_RADIO_REGN:
				olRegn = GetSelectedText();
				prlAcr = ogAcrData.GetAcrByName(olRegn);
				if (prlAcr == NULL)
				{
					//"Registration not found!"
					olErr.Format("'%s'\n%s",olRegn,GetString(IDS_STRING_REGNNOTFOUND));
					MessageBox(olErr,NULL,MB_ICONEXCLAMATION|MB_OK);
				}
				else if(!ogBasicData.IsInPool(ALLOCUNITTYPE_REGN,prlAcr->Regn,omPool))
				{
					//"%s not cannot be assigned to %s !"
					olErr.Format(GetString(IDS_STRING_CANNOTASSIGN),prlAcr->Regn,omPool);
					MessageBox(olErr,NULL,MB_ICONEXCLAMATION|MB_OK);
				}
				else
				{
/*					if((prlFlight = GetFlightByRegnAndTime(prlAcr->Regn,omStartTime,omEndTime)) == NULL)
					{
						//"No registrations found for '%s' between %s and %s"
						olErr.Format(GetString(IDS_STRING_NOREGNFOUND),prlAcr->Regn,omStartTime.Format("%H:%M/%d"),omEndTime.Format("%H:%M/%d"));
						MessageBox(olErr,NULL,MB_ICONEXCLAMATION|MB_OK);
					}
					else
					{
						ilResult = ogDataSet.CreateJobRegnFromDuty(this,olPoolJobUrnos,prlFlight->Urno, ALLOCUNITTYPE_REGN);
					} */
					ilResult = ogDataSet.CreateJobRegnFromDuty(this,olPoolJobUrnos,NULL, prlAcr->Regn, NULL, omStartTime, omEndTime);
				}
				break;
			case IDC_RADIO_FID :
				if (llSelectedUrno)
				{
					ilResult = ogDataSet.CreateFlightIndependentJobs(this, olPoolJobUrnos, llSelectedUrno, omStartTime, omEndTime);
				}
				break;
			case IDC_RADIO_OTHER:
				olOther = GetSelectedText();
				if(olOther.IsEmpty())
				{
					// "Please enter a description for this job"
					olErr.Format(GetString(IDS_STRING_NEEDOTHERTEXT));
					MessageBox(olErr,NULL,MB_ICONEXCLAMATION|MB_OK);
				}
				else
				{
					ilResult = ogDataSet.CreateJobSpecialFromDuty(this,olPoolJobUrnos,m_Description,omStartTime,omEndTime);
				}
			default:
				break;
		}

		if(ilResult == IDOK)
		{
			CDialog::OnOK();
		}
	}
}

// update the edit box(es) with the gate,flight,regn etc selected in the list box
void CAllocDlg::OnSelchangeResultsList() 
{
	bmValidateData = false;
	UpdateData(TRUE);

	int ind = m_ResultsList.GetCurSel();
	if (ind != LB_ERR)
	{
		CString olName;
		m_ResultsList.GetText(ind,olName);
		m_Description = olName;
		UpdateEndDateForOtherJob();
		UpdateDateForFlightIndep();
		UpdateData(FALSE);
	}
	else
	{
		m_Description = "";
		UpdateData(FALSE);
	}

	bmValidateData = true;
}

// return the text selected in the results list box
CString CAllocDlg::GetSelectedText()
{
	CString olSelectedText;
	int ilSelLine = m_ResultsList.GetCurSel();
	if(ilSelLine != LB_ERR && ilSelLine >= 0)
	{
		// get text from list box if selected
		m_ResultsList.GetText(ilSelLine,olSelectedText);
	}
	if(olSelectedText.IsEmpty())
	{
		// else get the text from the edit field
		olSelectedText = m_Description;
	}
	return olSelectedText;
}


void CAllocDlg::OnDblclkResultsList() 
{
	OnOK();	
}

FLIGHTDATA *CAllocDlg::GetFlightByRegnAndTime(CString opRegn,CTime opStartTime,CTime opEndTime)
{
	FLIGHTDATA *prlFlight = NULL;
	CCSPtrArray<DEMANDDATA> olDemands;
	ogDataSet.GetOpenDemandsForRegn(NULL, opRegn, olDemands, opStartTime, opEndTime);
	int ilNumDemands = olDemands.GetSize();
	if(ilNumDemands > 0)
	{
		// return the flight of the first demand
		olDemands.Sort(CompareDemandStartTime);
		for(int ilD = 0; prlFlight == NULL && ilD < ilNumDemands; ilD++)
		{
			DEMANDDATA *prlDemand = &olDemands[ilD];
			if((prlFlight = ogFlightData.GetFlightByUrno(prlDemand->Ouri)) == NULL)
			{
				prlFlight = ogFlightData.GetFlightByUrno(prlDemand->Ouro);
			}
		}
	}

	return prlFlight;
}

void CAllocDlg::OnSelchangeAllocUnitList() 
{
	CString olAllocUnitTypeText;
    m_AllocUnitList.GetLBText(m_AllocUnitList.GetCurSel(), olAllocUnitTypeText);
	omAllocUnitType = ogBasicData.GetAllocUnitTypeFromString(olAllocUnitTypeText);
	ogSettings.AddSetting(ALLOCDLG_ALLOCUNITTYPE,olAllocUnitTypeText);
	OnChangeFlightFields();
}

void CAllocDlg::OnKillfocusStartdate() 
{
	bmValidateData = false;
	UpdateData(TRUE);
	UpdateEndDateForOtherJob();
	if(imRadioButtonID == IDC_RADIO_FLIGHT)
	{
		OnChangeFlightFields();
	}
	else
	{
		OnChangeDescription();
	}
	bmValidateData = true;
}

void CAllocDlg::OnKillfocusStarttime() 
{
	bmValidateData = false;
	UpdateData(TRUE);
	UpdateEndDateForOtherJob();
	if(imRadioButtonID == IDC_RADIO_FLIGHT)
	{
		OnChangeFlightFields();
	}
	else
	{
		OnChangeDescription();
	}
	bmValidateData = true;
}

void CAllocDlg::OnKillfocusEnddate() 
{
	bmValidateData = false;
	UpdateData(TRUE);
	if(imRadioButtonID == IDC_RADIO_FLIGHT)
	{
		OnChangeFlightFields();
	}
	else
	{
		OnChangeDescription();
	}
	bmValidateData = true;
}

void CAllocDlg::OnKillfocusEndtime() 
{
	bmValidateData = false;
	UpdateData(TRUE);
	if(imRadioButtonID == IDC_RADIO_FLIGHT)
	{
		OnChangeFlightFields();
	}
	else
	{
		OnChangeDescription();
	}
	bmValidateData = true;
}

void CAllocDlg::UpdateEndDateForOtherJob(void)
{
	if(imRadioButtonID == IDC_RADIO_OTHER && bmLoadServicesForOtherJobs)
	{
		SERDATA *prlSer = ogSerData.GetSerBySnam(GetSelectedText());
		if(prlSer != NULL)
		{
			CTimeSpan olDuration(0, 0, ogSerData.GetServiceDuration(prlSer), 0);
			omEndTime = omStartTime + olDuration;
			UpdateData(FALSE);
		}
	}
}

void CAllocDlg::UpdateDateForFlightIndep(void)
{
	if(imRadioButtonID == IDC_RADIO_FID)
	{
		int ilSelLine = m_ResultsList.GetCurSel();
		if(ilSelLine != LB_ERR && ilSelLine >= 0)
		{
			DEMANDDATA *prlDemand = ogDemandData.GetDemandByUrno((long) m_ResultsList.GetItemData(ilSelLine));
			if(prlDemand != NULL)
			{
				omStartTime = prlDemand->Debe;
				omEndTime = prlDemand->Deen;

				TIMEPACKET olTimePacket;
				olTimePacket.StartTime = prlDemand->Debe;
				olTimePacket.EndTime = prlDemand->Deen;
				ogCCSDdx.DataChanged((void *)this, STAFFDIAGRAM_UPDATETIMEBAND, &olTimePacket);
			}
		}
	}
}

// for most jobs, set the job start time to the current time + 2 mins
void CAllocDlg::SetStartDate(void)
{
	CTime olCurrTime = ogBasicData.GetTime();
	if(olCurrTime > omStartTime && olCurrTime < omEndTime)
	{
		UpdateData(TRUE);
		omStartTime = olCurrTime + CTimeSpan(0, 0, 2, 0);
		UpdateData(FALSE);
	}
}

//Singapore
void CAllocDlg::OnCheckT2()
{
	UpdateData();
	CString olAlocUnit;
	int ilNumItems = omItemData.GetSize();
	int ilNumUnits = omAllocUnits.GetSize();

	if(m_CheckT2 == TRUE)
	{
		CButton* polButton = (CButton*)GetDlgItem(IDC_CHECK_T3);
		if(polButton != NULL)
		{
			polButton->SetCheck(0);
		}
		m_ResultsList.ResetContent();
		for(int ilUnit = 0; ilUnit < ilNumUnits; ilUnit++)
		{
			olAlocUnit = omAllocUnits[ilUnit];
			olAlocUnit.MakeUpper();

			long lUrno = omItemData[ilUnit];
			DEMANDDATA* prlDem = ogDemandData.GetDemandByUrno(lUrno);
			RUDDATA* prlRud = ogRudData.GetRudByUrno(prlDem->Urud);
			SERDATA* prlSer = ogSerData.GetSerByUrno(prlRud->Ughs);
						
			if(strstr(prlSer->Seco,TERMINAL2) != NULL)
			{
				int ilIndex = m_ResultsList.AddString(olAlocUnit);
				if(ilIndex != LB_ERR && ilUnit < ilNumItems)
				{
					m_ResultsList.SetItemData(ilIndex,omItemData[ilUnit]);
				}
			}
		}
	}
	else if(m_CheckT2 == FALSE)
	{
		for(int ilUnit = 0; ilUnit < ilNumUnits; ilUnit++)
		{
			olAlocUnit = omAllocUnits[ilUnit];
			olAlocUnit.MakeUpper();

			long lUrno = omItemData[ilUnit];
			DEMANDDATA* prlDem = ogDemandData.GetDemandByUrno(lUrno);
			RUDDATA* prlRud = ogRudData.GetRudByUrno(prlDem->Urud);
			SERDATA* prlSer = ogSerData.GetSerByUrno(prlRud->Ughs);

			if(strstr(prlSer->Seco,TERMINAL2) == NULL)
			{
				int ilIndex = m_ResultsList.AddString(olAlocUnit);
				if(ilIndex != LB_ERR && ilUnit < ilNumItems)
				{
					m_ResultsList.SetItemData(ilIndex,omItemData[ilUnit]);
				}
			}
		}
	}
}

//Singapore
void CAllocDlg::OnCheckT3()
{	
	UpdateData();
	CString olAlocUnit;
	int ilNumItems = omItemData.GetSize();
	int ilNumUnits = omAllocUnits.GetSize();

	if(m_CheckT3 == TRUE)
	{
		CButton* polButton = (CButton*)GetDlgItem(IDC_CHECK_T2);
		if(polButton != NULL)
		{
			polButton->SetCheck(0);
		}
		m_ResultsList.ResetContent();
		for(int ilUnit = 0; ilUnit < ilNumUnits; ilUnit++)
		{
			olAlocUnit = omAllocUnits[ilUnit];
			olAlocUnit.MakeUpper();

			long lUrno = omItemData[ilUnit];
			DEMANDDATA* prlDem = ogDemandData.GetDemandByUrno(lUrno);
			RUDDATA* prlRud = ogRudData.GetRudByUrno(prlDem->Urud);
			SERDATA* prlSer = ogSerData.GetSerByUrno(prlRud->Ughs);
						
			if(strstr(prlSer->Seco,TERMINAL3) != NULL)
			{			
				int ilIndex = m_ResultsList.AddString(olAlocUnit);
				if(ilIndex != LB_ERR && ilUnit < ilNumItems)
				{
					m_ResultsList.SetItemData(ilIndex,omItemData[ilUnit]);
				}
			}
		}
	}
	else if(m_CheckT3 == FALSE)
	{
		for(int ilUnit = 0; ilUnit < ilNumUnits; ilUnit++)
		{
			olAlocUnit = omAllocUnits[ilUnit];
			olAlocUnit.MakeUpper();

			long lUrno = omItemData[ilUnit];
			DEMANDDATA* prlDem = ogDemandData.GetDemandByUrno(lUrno);
			RUDDATA* prlRud = ogRudData.GetRudByUrno(prlDem->Urud);
			SERDATA* prlSer = ogSerData.GetSerByUrno(prlRud->Ughs);
						
			if(strstr(prlSer->Seco,TERMINAL3) == NULL)
			{
				int ilIndex = m_ResultsList.AddString(olAlocUnit);
				if(ilIndex != LB_ERR && ilUnit < ilNumItems)
				{
					m_ResultsList.SetItemData(ilIndex,omItemData[ilUnit]);
				}
			}
		}
	}
}