// OpssPm.cpp : Defines the class behaviors for the application.
//

#include <stdafx.h>
#include <ccsglobl.h>
#include <AatHelp.h>
#include <OpssPm.h>
#include <CCSCedaCom.h>
#include <CCSCedaData.h>
//#include "dlogin.h"
#include <LoginDlg.h>
#include <ctl3d.h>
#include <cxbutton.h>
#include <CCSDragDropCtrl.h>
#include <ccsddx.h>
#include <CCSBcHandle.h>
#include <CedaEmpData.h>
#include <CedaJobData.h>
#include <CedaDemandData.h>
#include <CedaJodData.h>
#include <CedaFlightData.h>
#include <CedaShiftData.h>
#include <tscale.h>
#include <StaffViewer.h>
#include <StaffGantt.h>
#include <StaffDiagram.h>
#include <StaffChart.h>
#include <GateDiagram.h>
#include <GateGantt.h>
#include <GateChart.h>
#include <RegnDiagram.h>
#include <RegnGantt.h>
#include <RegnChart.h>
#include <PstDiagram.h>
#include <PstGantt.h>
#include <PstChart.h>
#include <EquipmentDiagram.h>
#include <EquipmentGantt.h>
#include <EquipmentChart.h>
#include <CciDiagram.h>
#include <CciGantt.h>
#include <CciChart.h>
#include <conflict.h>
#include <CciDeskDlg.h>
#include <CedaAloData.h>
#include <CedaJtyData.h>
#include <PrePlanTable.h>
#include <ccitable.h>
#include <gatetable.h>
#include <BasicData.h>
#include <AllocData.h>
#include <CedaWroData.h>

#include <FlightPlan.h>
#include <StaffTable.h>
#include <StartDate.h>
#include <ConflictConfigTable.h>
#include <ufis.h>
#include <ConflictTable.h>
#include <ButtonList.h>
#include <FlightDetailWindow.h>
#include <StaffDetailWindow.h>
#include <EquipmentDetailWindow.h>
#include <CciDetailWindow.h>
#include <CedaDprData.h>
#include <CedaShiftTypeData.h>
#include <CedaRequestData.h>
#include <CedaCfgData.h>
#include <dataset.h>
#include <CiCDemandTableViewer.h>

#include <FilterData.h>
#include <Search.h>
#include <CedaOdaData.h>
#include <CedaAloData.h>
#include <CedaRnkData.h>
#include <PrivList.h>
#include <RegisterDlg.h>
#include <CedaAzaData.h>
#include <CedaWgpData.h>
#include <CedaRemData.h>
#include <CedaValData.h>
#include <CedaCicData.h>
#include <CedaGatData.h>
#include <CedaEquData.h>
#include <CedaEqtData.h>
#include <CedaEqaData.h>
#include <CedaPstData.h>
#include <CedaPolData.h>
#include <CedaAcrData.h>
#include <CedaActData.h>
#include <CedaAltData.h>
#include <CedaSgmData.h>
#include <CedaSgrData.h>
#include <CedaSpfData.h>
#include <CedaRpfData.h>
#include <CedaSpeData.h>
#include <CedaRpqData.h>
#include <CedaRudData.h>
#include <CedaSerData.h>
#include <CedaPerData.h>
#include <CedaPfcData.h>
#include <CedaSwgData.h>
#include <CedaDrgData.h>
#include <CedaPgpData.h>
#include <CedaDrdData.h>
#include <CedaDelData.h>
#include <CedaCcaData.h>
#include <CedaDraData.h>
#include <CedaTplData.h>
#include <CedaRueData.h>
#include <CedaPaxData.h>
#include <CedaDlgData.h>
#include <CedaPrmData.h>
#include <CedaPdaData.h>
#include <CedaBlkData.h>
#include <CedaHagData.h>
#include <CedaCccData.h>
#include <CedaRloData.h>
#include <CedaDrsData.h>
#include <CedaSprData.h>
#include <CedaReqData.h>
#include <CedaNatData.h>
#include <CedaBltData.h>
#include <CedaAptData.h>
#include <CedaPdaData.h>
#include <CedaLoaData.h>
#include <SearchDlg.h>
#include <CedaHaiData.h>

#include <MainFrm.h>
#include <GUILng.h>

#include <LoadTimeSpanDlg.h>
#include <afxmt.h>
#include <TransactionManager.h>
#include <Settings.h>
#include <NameConfig.h>
#include <VersionInfo.h>
#include <AatLogin.h>
#include <CedaInitModuData.h>
#include <DlgSettings.h>
#include <FieldConfigDlg.h>
#include <PrmConfig.h>

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Global Variable

char pcgAppName[10] = "OPSS-PM";
//char pcgAppName[10] = "OPSS-PRM";
char pcgTableExt[10] = "TAB";
char pcgHomeAirport[10] = "XXX";
CWnd* pogMainWnd;
CWnd* pogPreviewPrintWnd = NULL;;

const CString COpssPmApp::OPSSPM_DIALOG_WINDOWPOS_REG_KEY         = "DialogPosition\\Opsspm_Dialog";

const CString COpssPmApp::EMPLOYEE_DIAGRAM_WINDOWPOS_REG_KEY      = "DialogPosition\\Employee_Diagram";
const CString COpssPmApp::GATES_DIAGRAM_WINDOWPOS_REG_KEY         = "DialogPosition\\Gates_Diagram";
const CString COpssPmApp::CHECKIN_DIAGRAM_WINDOWPOS_REG_KEY       = "DialogPosition\\Check-In_Diagram";
const CString COpssPmApp::EQUIPMENT_DIAGRAM_WINDOWPOS_REG_KEY     = "DialogPosition\\Equipment_Diagram";	
const CString COpssPmApp::REGISTRATION_DIAGRAM_WINDOWPOS_REG_KEY  = "DialogPosition\\Registration_Diagram";	
const CString COpssPmApp::POSITION_DIAGRAM_WINDOWPOS_REG_KEY      = "DialogPosition\\Position_Diagram";

const CString COpssPmApp::EMPLOYEE_TABLE_WINDOWPOS_REG_KEY        = "DialogPosition\\Employee_Table";
const CString COpssPmApp::GATES_TABLE_WINDOWPOS_REG_KEY           = "DialogPosition\\Gates_Table";
const CString COpssPmApp::CHECKIN_TABLE_WINDOWPOS_REG_KEY         = "DialogPosition\\Check-In_Table";
const CString COpssPmApp::FLIGHTPLAN_TABLE_WINDOWPOS_REG_KEY      = "DialogPosition\\FlightPlan_Table";
const CString COpssPmApp::DEMAND_TABLE_WINDOWPOS_REG_KEY          = "DialogPosition\\Demand_Table";
const CString COpssPmApp::EQUIPMENT_TABLE_WINDOWPOS_REG_KEY       = "DialogPosition\\Equipment_Table";	
const CString COpssPmApp::FLIND_TABLE_WINDOWPOS_REG_KEY           = "DialogPosition\\Flight-Independent_Table";
const CString COpssPmApp::FLIGHTJOBS_TABLE_WINDOWPOS_REG_KEY      = "DialogPosition\\FlightJobs_Table";
const CString COpssPmApp::PREPLANNING_TABLE_WINDOWPOS_REG_KEY     = "DialogPosition\\Preplanning_Table";
const CString COpssPmApp::CONFLICTS_TABLE_WINDOWPOS_REG_KEY       = "DialogPosition\\Conflicts_Table";
const CString COpssPmApp::ATTENTION_TABLE_WINDOWPOS_REG_KEY       = "DialogPosition\\Attention_Table";

const CString COpssPmApp::UNDO_DIALOG_WINDOWPOS_REG_KEY           = "DialogPosition\\Undo_Dialog";
const CString COpssPmApp::SEARCH_DIALOG_WINDOWPOS_REG_KEY         = "DialogPosition\\Search_Dialog";
const CString COpssPmApp::SETUP_DIALOG_WINDOWPOS_REG_KEY          = "DialogPosition\\Setup_Dialog"; 

SEARCHFILTER ogSearchFilter;
Search ogSearch;

CBrush *ogBrushs[MAXCOLORS+1];
COLORREF ogColors[MAXCOLORS+1];

CCSLog ogLog(pcgAppName);
CCSDdx ogCCSDdx;
CCSCedaCom ogCommHandler(&ogLog, pcgAppName);  // The one and only CedaCom object
CCSBcHandle ogBcHandle(&ogCCSDdx, &ogCommHandler, &ogLog);

CString ogUsername; // Username of the user that logged in
CString ogPassword;
CString ogWks;

PrivList ogPrivList;
PrivList ogPrmPrivList;
          
PrmConfig ogPrmConfig;
AllocData ogAllocData; // tree of AllocTypes =< AllocGroups [=< AllocGroups] =< AllocUnits
CedaCfgData ogCfgData;
CedaFlightData ogFlightData;
CedaDemandData ogDemandData;
CedaRudData ogRudData;
CedaSerData ogSerData;
CedaPfcData ogPfcData;
CedaPerData ogPerData;
// MCU 07.05 we need a global Jod object also
CedaJodData ogJodData;
CedaWgpData ogWgpData;
CedaRemData ogRemData;
CedaAzaData ogAzaData;
CedaJobData ogJobData;
CedaEmpData ogEmpData;
// MCU 07.07 Shift Data now outside of Preplan Table
CedaShiftData ogShiftData;
CedaShiftTypeData  ogShiftTypes;
CedaOdaData ogOdaData;
CedaDprData  ogDprs;

CedaCicData ogCicData;
CedaGatData ogGatData;
CedaEquData ogEquData;
CedaEqtData ogEqtData;
CedaEqaData ogEqaData;
CedaPstData ogPstData;
CedaPolData ogPolData;
CedaAcrData ogAcrData;
CedaActData ogActData;
CedaAltData ogAltData;
CedaHaiData ogHaiData;
CedaAptData ogAptData;
CedaSgmData ogSgmData;
CedaValData ogValData;
CedaSgrData ogSgrData;
CedaJtyData ogJtyData;
CedaDelData ogDelData;
CedaDrdData ogDrdData;
CedaCcaData ogCcaData;
CedaDraData ogDraData;
CedaPaxData ogPaxData;
CedaRueData ogRueData;
CedaTplData ogTplData;

CedaReqData ogReqData;
CedaRpfData ogRpfData;
CedaSpfData ogSpfData;
CedaRpqData ogRpqData;
CedaSpeData ogSpeData;
CedaSwgData ogSwgData;
CedaDrgData ogDrgData;
CedaDlgData ogDlgData;
CedaPgpData ogPgpData;
CedaRnkData ogRnkData;
CedaPrmData ogPrmData;
CedaPdaData ogPdaData;
CedaBlkData	ogBlkData;
CedaHagData	ogHagData;
CedaCccData	ogCccData;
CedaDrsData ogDrsData;
CedaSprData ogSprData;
CedaNatData ogNatData;
CedaBltData ogBltData;
CedaLoaData ogLoaData;
CedaDpxData ogDpxData;
CedaWroData ogWroData;

CTransactionManager ogTransactionManager;
CSettings ogSettings;
COfflineDescription ogOfflineDescription;

CedaRequestData ogRequests("R");
CedaRequestData ogInfos("I");

DataSet ogDataSet;
COldSearchValues ogOldSearchValues;

CCSConflicts ogConflicts;
CDlgSettings ogDlgSettings;
CNameConfig ogNameConfig; // configuration of employee name
CFieldConfig ogFieldConfig; // configuration of other fields eg vertical scale, status bars, tool tips etc


BOOL bgModal = FALSE;
bool bgIsPrm = true;
bool bgShowPrmClos = true;
bool bgPrmUseRema = false; // lli: Remark flag for Using Remarks in PRM request
bool bgPrmShowFlightNumber = false; // lli: flag for showing flight number in Staff Chart status bar
bool bgPrmShowAssignment = false; // lli: flag for showing Assignment in PRM List
bool bgPrmShowStaStd = false; // lli: flag for showing Sta, Std in PRM List
bool bgPrmUseStat = false; // lli: Status flag for Using Status in PRM request
bool bgOnline = true;
BOOL bgIsPreplanMode = FALSE;
BOOL bgPrintPreviewMode = FALSE;
SYSTEMTIME ogSysTime;
BOOL bgIsInitialized;

InitialLoadDlg *pogInitialLoad;

// Global variable

CFont ogSmallFonts_Regular_6;
CFont ogSmallFonts_Regular_7;
CFont ogMSSansSerif_Regular_6;
CFont ogMSSansSerif_Regular_8;
CFont ogMSSansSerif_Regular_12;
CFont ogMSSansSerif_Regular_16;
CFont ogMSSansSerif_Bold_8;
CFont ogCourier_Bold_10;

CFont ogScalingFonts[4];
FONT_INDEXES ogStaffIndexes;
FONT_INDEXES ogGateIndexes;
FONT_INDEXES ogCCiIndexes;
FONT_INDEXES ogRegnIndexes;
FONT_INDEXES ogPstIndexes;
FONT_INDEXES ogEquipmentIndexes;

CCSBasicData ogBasicData;

//CPoint ogMaxTrackSize = CPoint(1280, 1280);
//CPoint ogMinTrackSize = CPoint(1024 / 4, 768 / 4);
//Singapore
char chData[20];
DWORD dWord1  = ::GetPrivateProfileString(pcgAppName,"MAX_INITX","1280",chData,sizeof(chData),ogBasicData.GetConfigFileName());
long lMaxX = atol(chData);
DWORD dWord2  = ::GetPrivateProfileString(pcgAppName,"MAX_INITY","1280",chData,sizeof(chData),ogBasicData.GetConfigFileName());
long lMaxY = atol(chData);
DWORD dWord3  = ::GetPrivateProfileString(pcgAppName,"MIN_INITX","1024",chData,sizeof(chData),ogBasicData.GetConfigFileName());
long lMinX = atol(chData);
DWORD dWord4  = ::GetPrivateProfileString(pcgAppName,"MIN_INITY","768",chData,sizeof(chData),ogBasicData.GetConfigFileName());
long lMinY = atol(chData);

CPoint ogMaxTrackSize = CPoint(lMaxX, lMaxY);
CPoint ogMinTrackSize = CPoint(lMinX / 4, lMinY / 4);

COLORREF lgBkColor = SILVER;
COLORREF lgTextColor = BLACK;
COLORREF lgHilightColor = WHITE;

int igPreplanSearchCount = 0; //Singapore
int igEmployeeSearchCount = 0; //Singpaore

/////////////////////////////////////////////////////////////////////////////
// Class variable

CPoint StaffDiagram::omMaxTrackSize = ogMaxTrackSize;
CPoint StaffDiagram::omMinTrackSize = ogMinTrackSize;
COLORREF StaffDiagram::lmBkColor = lgBkColor;
COLORREF StaffDiagram::lmTextColor = lgTextColor;
COLORREF StaffDiagram::lmHilightColor = lgHilightColor;

int StaffChart::imStartTopScaleTextPos = 200;
int StaffChart::imStartVerticalScalePos = 30;
COLORREF StaffChart::lmBkColor = lgBkColor;
COLORREF StaffChart::lmTextColor = lgTextColor;
COLORREF StaffChart::lmHilightColor = lgHilightColor;
//


CPoint GateDiagram::omMaxTrackSize = ogMaxTrackSize;
CPoint GateDiagram::omMinTrackSize = ogMinTrackSize;
COLORREF GateDiagram::lmBkColor = lgBkColor;
COLORREF GateDiagram::lmTextColor = lgTextColor;
COLORREF GateDiagram::lmHilightColor = lgHilightColor;

int GateChart::imStartTopScaleTextPos = 120;
int GateChart::imStartVerticalScalePos = 30;
COLORREF GateChart::lmBkColor = lgBkColor;
COLORREF GateChart::lmTextColor = lgTextColor;
COLORREF GateChart::lmHilightColor = lgHilightColor;
//

CPoint CCIDiagram::omMaxTrackSize = ogMaxTrackSize;
CPoint CCIDiagram::omMinTrackSize = ogMinTrackSize;
COLORREF CCIDiagram::lmBkColor = lgBkColor;
COLORREF CCIDiagram::lmTextColor = lgTextColor;
COLORREF CCIDiagram::lmHilightColor = lgHilightColor;

int CCIChart::imStartTopScaleTextPos = 120;
int CCIChart::imStartVerticalScalePos = 30;
COLORREF CCIChart::lmBkColor = lgBkColor;
COLORREF CCIChart::lmTextColor = lgTextColor;
COLORREF CCIChart::lmHilightColor = lgHilightColor;
//

CPoint RegnDiagram::omMaxTrackSize = ogMaxTrackSize;
CPoint RegnDiagram::omMinTrackSize = ogMinTrackSize;
COLORREF RegnDiagram::lmBkColor = lgBkColor;
COLORREF RegnDiagram::lmTextColor = lgTextColor;
COLORREF RegnDiagram::lmHilightColor = lgHilightColor;

int RegnChart::imStartTopScaleTextPos = 140;
int RegnChart::imStartVerticalScalePos = 30;
COLORREF RegnChart::lmBkColor = lgBkColor;
COLORREF RegnChart::lmTextColor = lgTextColor;
COLORREF RegnChart::lmHilightColor = lgHilightColor;

CPoint PstDiagram::omMaxTrackSize = ogMaxTrackSize;
CPoint PstDiagram::omMinTrackSize = ogMinTrackSize;
COLORREF PstDiagram::lmBkColor = lgBkColor;
COLORREF PstDiagram::lmTextColor = lgTextColor;
COLORREF PstDiagram::lmHilightColor = lgHilightColor;

int PstChart::imStartTopScaleTextPos = 120;
int PstChart::imStartVerticalScalePos = 30;
COLORREF PstChart::lmBkColor = lgBkColor;
COLORREF PstChart::lmTextColor = lgTextColor;
COLORREF PstChart::lmHilightColor = lgHilightColor;

CPoint EquipmentDiagram::omMaxTrackSize = ogMaxTrackSize;
CPoint EquipmentDiagram::omMinTrackSize = ogMinTrackSize;
COLORREF EquipmentDiagram::lmBkColor = lgBkColor;
COLORREF EquipmentDiagram::lmTextColor = lgTextColor;
COLORREF EquipmentDiagram::lmHilightColor = lgHilightColor;

int EquipmentChart::imStartTopScaleTextPos = 120;
int EquipmentChart::imStartVerticalScalePos = 30;
COLORREF EquipmentChart::lmBkColor = lgBkColor;
COLORREF EquipmentChart::lmTextColor = lgTextColor;
COLORREF EquipmentChart::lmHilightColor = lgHilightColor;

// Create possible values for filtering key used in PropertySheet.
CStringArray ogFlightAlcd;


CPoint FlightDetailWindow::omMaxTrackSize = ogMaxTrackSize;
CPoint FlightDetailWindow::omMinTrackSize = ogMinTrackSize;

CPoint StaffDetailWindow::omMaxTrackSize = ogMaxTrackSize;
CPoint StaffDetailWindow::omMinTrackSize = ogMinTrackSize;

CPoint CciDetailWindow::omMaxTrackSize = ogMaxTrackSize;
CPoint CciDetailWindow::omMinTrackSize = ogMinTrackSize;

CPoint EquipmentDetailWindow::omMaxTrackSize = ogMaxTrackSize;
CPoint EquipmentDetailWindow::omMinTrackSize = ogMinTrackSize;


//

COLORREF CClientWnd::lmBkColor = lgBkColor;
COLORREF CClientWnd::lmTextColor = lgTextColor;
COLORREF CClientWnd::lmHilightColor = lgHilightColor;

CFont *C3DStatic::omFont = &ogMSSansSerif_Bold_8;
COLORREF C3DStatic::lmBkColor = lgBkColor;
COLORREF C3DStatic::lmTextColor = BLACK;
COLORREF C3DStatic::lmHilightColor = RED;

CFont *CTimeScale::omFont = &ogSmallFonts_Regular_7;
COLORREF CTimeScale::lmBkColor = lgBkColor;
COLORREF CTimeScale::lmTextColor = NAVY;
COLORREF CTimeScale::lmHilightColor = RED;

/////////////////////////////////////////////////////////////////////////////

//

// COpssPmApp

BEGIN_MESSAGE_MAP(COpssPmApp, CWinApp)
	//{{AFX_MSG_MAP(COpssPmApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COpssPmApp construction

COpssPmApp::COpssPmApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
	pomMutex = new CMutex(FALSE,"OPSS-PM",NULL);
}

/////////////////////////////////////////////////////////////////////////////
// The one and only COpssPmApp object

COpssPmApp theApp;
CButtonListDialog *pogButtonList;

void  ProcessTrafficLight(long BcNum, int ipState)
{
	if (pogButtonList != NULL)
	{
		pogButtonList->UpdateTrafficLight(ipState);
	}
}



/////////////////////////////////////////////////////////////////////////////
// COpssPmApp initialization

#if 0
#define BLACK   RGB(  0,   0,   0)
#define MAROON  RGB(128,   0,   0)          // dark red
#define GREEN   RGB(  0, 128,   0)          // dark green
#define OLIVE   RGB(128, 128,   0)          // dark yellow
#define NAVY    RGB(  0,   0, 128)          // dark blue
#define PURPLE  RGB(128,   0, 128)          // dark magenta
#define TEAL    RGB(  0, 128, 128)          // dark cyan
#define GRAY    RGB(128, 128, 128)          // dark gray
#define SILVER  ::GetSysColor(COLOR_BTNFACE)          // light gray
#define RED     RGB(255,   0,   0)
#define LIME    RGB(  0, 255,   0)          // green
#define YELLOW  RGB(255, 255,   0)
#define BLUE    RGB(  0,   0, 255)
#define FUCHSIA RGB(255,   0, 255)          // magenta
#define AQUA    RGB(  0, 255, 255)          // cyan
#define WHITE   RGB(255, 255, 255)
#endif

static void CreateBrushes()
{
	int ilLc;

	for( ilLc = 0; ilLc < FIRSTCONFLICTCOLOR; ilLc++)
		ogColors[ilLc] = RED;
	ogColors[2] = GRAY;
	ogColors[3] = GREEN;
	ogColors[4] = RED;
	ogColors[5] = BLUE;
	ogColors[6] = SILVER;
	ogColors[7] = WHITE;
	ogColors[8] = LIME;

// don't use index 21 it's used for Break jobs already

	for(ilLc = 0; ilLc < MAXCOLORS; ilLc++)
	{
		ogBrushs[ilLc] = new CBrush(ogColors[ilLc]);
	}


	// create a break job brush pattern
	delete ogBrushs[21];
	delete ogBrushs[22];

	CBitmap olBitmap1, olBitmap2;
	olBitmap1.LoadBitmap(IDB_BREAK);
	ogBrushs[21] = new CBrush(&olBitmap1);
	olBitmap2.LoadBitmap(IDB_COFFEEBREAK);
	ogBrushs[22] = new CBrush(&olBitmap2);
}

void DeleteBrushes()
{
	for( int ilLc = 0; ilLc < MAXCOLORS; ilLc++)
	{
		CBrush *olBrush = ogBrushs[ilLc];
		delete olBrush;
	}
}


static void InitFont() 
{
    CDC dc;
    ASSERT(dc.CreateCompatibleDC(NULL));

    LOGFONT logFont;
    memset(&logFont, 0, sizeof(LOGFONT));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(6, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts");
    ASSERT(ogSmallFonts_Regular_6.CreateFontIndirect(&logFont));
        
	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(7, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts");
    ASSERT(ogSmallFonts_Regular_7.CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(10, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ASSERT(ogCourier_Bold_10.CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(6, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts"); /*"MS Sans Serif""Arial"*/
    ASSERT(ogScalingFonts[MS_SANS6].CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogScalingFonts[MS_SANS8].CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(12, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogScalingFonts[MS_SANS12].CreateFontIndirect(&logFont));

	//logFont.lfHeight = - MulDiv(16, dc.GetDeviceCaps(LOGPIXELSY), 72);
	logFont.lfCharSet= DEFAULT_CHARSET;
	logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogScalingFonts[MS_SANS16].CreateFontIndirect(&logFont));
 
	ogStaffIndexes.VerticalScale = MS_SANS8;
	ogStaffIndexes.Chart = MS_SANS6;
	ogGateIndexes.VerticalScale = MS_SANS8;
	ogGateIndexes.Chart = MS_SANS6;
	ogCCiIndexes.VerticalScale = MS_SANS8;
	ogCCiIndexes.Chart = MS_SANS6;
	ogRegnIndexes.VerticalScale = MS_SANS8;
	ogRegnIndexes.Chart = MS_SANS6;
	ogPstIndexes.VerticalScale = MS_SANS8;
	ogPstIndexes.Chart = MS_SANS6;
	ogEquipmentIndexes.VerticalScale = MS_SANS8;
	ogEquipmentIndexes.Chart = MS_SANS6;


	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogMSSansSerif_Bold_8.CreateFontIndirect(&logFont));

    dc.DeleteDC();
}


void InitLogging(void)
{
	char pclLogConfig[142];

    GetPrivateProfileString("CCSLOG", "LOGFILE", CCSLog::GetUfisSystemPath("\\OPSSPM_LOG.CFG"),pclLogConfig, sizeof pclLogConfig, ogBasicData.GetConfigFileName());

	ogLog.Load(pclLogConfig);
	ogLog.Trace("START1","Test Start %d",1);
	ogLog.Trace("START2","Test Start %d",2);

	ogLog.Debug("START1",CCSLog::Most,(const char *)"Test Debug Most Start %d",1);
	ogLog.Debug("START2",CCSLog::Some,(const char *)"Test Debug Some Start %d",2);
	ogLog.Debug("START3",CCSLog::None,(const char *)"Test Debug None Start %d",3);
	ogLog.Error("START1","Test Error Start %d",1);
}

// Began to expand this module for view property sheets.
// Should be in somewhere else, but we cannot afford changing header files.
static void CreateCMapForAllPossibleValuesOfFlightAlcd()
{
	CMapStringToPtr olMap;

	int n = ogFlightData.omData.GetSize();
	for (int i = 0; i < n; i++)
	{
		if (ogFlightData.UseAlc3(&ogFlightData.omData[i]))
		{
			olMap[ogFlightData.omData[i].Alc3] = NULL;
			// we can use NULL since we don't interest its value
		}
		else
		{
			olMap[ogFlightData.omData[i].Alc2] = NULL;
			// we can use NULL since we don't interest its value
		}
	}

	// create a string array for easier programming
	POSITION rlPos;
	for ( rlPos = olMap.GetStartPosition(); rlPos != NULL; )
	{
		void *p;
		CString olAlcd;
		olMap.GetNextAssoc(rlPos, olAlcd, p);
		ogFlightAlcd.Add(olAlcd);
	}
}

static void InitStaffDiagramDefaultView()
{
	CViewer olViewer;
	CStringArray olPossibleFilters;
	CStringArray olSortOrder;
	CStringArray olPoolFilter;
	CStringArray olTeamFilter;
	CStringArray olShiftCodeFilter;
	CStringArray olRankFilter;
	CStringArray olPermitsFilter;

	// Read the default value from the database in the server
	TRACE("InitStaffDiagramDefaultView\n");
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("Pool");
	//olPossibleFilters.Add(GetString(IDS_STRING61610));
	olPossibleFilters.Add("Team");
	//olPossibleFilters.Add(GetString(IDS_STRING61611));
	olPossibleFilters.Add("Schichtcode");
	//olPossibleFilters.Add(GetString(IDS_STRING61612));
	olPossibleFilters.Add("Dienstrang");
	//olPossibleFilters.Add(GetString(IDS_STRING61613));
	olPossibleFilters.Add("Permits");

	olSortOrder.RemoveAll();
	olSortOrder.Add("Schichtcode");
	olSortOrder.Add("none");
	olSortOrder.Add("none");

	olPoolFilter.Add(GetString(IDS_ALLSTRING));
	olRankFilter.Add (GetString(IDS_ALLSTRING));
	olPermitsFilter.Add(GetString(IDS_ALLSTRING));
	olTeamFilter.Add(GetString(IDS_ALLSTRING));
	olShiftCodeFilter.Add(GetString(IDS_ALLSTRING));

	// Initialize the default view for the gate diagram
	olViewer.SetViewerKey("StaffDia");
	CString olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView(GetString(IDS_STRINGDEFAULT), olPossibleFilters,FALSE);
	olViewer.SelectView(GetString(IDS_STRINGDEFAULT));
	olViewer.SetGroup("Pool");
	olViewer.SetUserData("GROUPVIEW","NO");
	olViewer.SetSort(olSortOrder);
	olViewer.SetFilter("Pool", olPoolFilter);
	//olViewer.SetFilter(GetString(IDS_STRING61610), olPoolFilter);
	olViewer.SetFilter("Team", olTeamFilter);
	//olViewer.SetFilter(GetString(IDS_STRING61611), olTeamFilter);
	olViewer.SetFilter("Schichtcode", olShiftCodeFilter);
	//olViewer.SetFilter(GetString(IDS_STRING61612), olShiftCodeFilter);
	olViewer.SetFilter("Dienstrang", olRankFilter);
	//olViewer.SetFilter(GetString(IDS_STRING61613), olRankFilter);
	olViewer.SetFilter("Permits", olPermitsFilter);

	CString olColour;
	olColour.Format("%ld",SILVER);	olViewer.SetUserData("STOA",olColour);
	olColour.Format("%ld",WHITE);	olViewer.SetUserData("ETOA",olColour);
	olColour.Format("%ld",YELLOW);	olViewer.SetUserData("TMOA",olColour);
	olColour.Format("%ld",GREEN);	olViewer.SetUserData("LAND",olColour);
	olColour.Format("%ld",LIME);	olViewer.SetUserData("ONBL",olColour);
	olColour.Format("%ld",SILVER);	olViewer.SetUserData("STOD",olColour);
	olColour.Format("%ld",WHITE);	olViewer.SetUserData("ETOD",olColour);
	olColour.Format("%ld",YELLOW);	olViewer.SetUserData("SLOT",olColour);
	olColour.Format("%ld",LIME);	olViewer.SetUserData("OFBL",olColour);
	olColour.Format("%ld",GREEN);	olViewer.SetUserData("AIRB",olColour);

	olViewer.SetUserData("ARRIVAL",((ogBasicData.omDepArrMarkerStaff.Find("ARRIVAL") != -1) ? "YES" : "NO"));
	olViewer.SetUserData("DEPARTURE",((ogBasicData.omDepArrMarkerStaff.Find("DEPARTURE") != -1) ? "YES" : "NO"));
	olViewer.SetUserData("TURNAROUND",((ogBasicData.omDepArrMarkerStaff.Find("TURNAROUND") != -1) ? "YES" : "NO"));
	
	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view
}

static void InitGateDiagramDefaultView()
{
	CViewer olViewer;
	CStringArray olPossibleFilters;
	CStringArray olSortOrder;
	CStringArray olGateAreaFilter;
	CStringArray olAlcdFilter;
	CStringArray olAgentsFilter;

	// Read the default value from the database in the server
	TRACE("InitGateDiagramDefaultView\n");
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("Gatebereich");
	olPossibleFilters.Add("Airline");
	olPossibleFilters.Add("Agents");

	olSortOrder.RemoveAll();
	olSortOrder.Add("Gatebereich");

	olGateAreaFilter.Add(GetString(IDS_ALLSTRING));
	olAlcdFilter.Add(GetString(IDS_ALLSTRING));
	olAgentsFilter.Add(GetString(IDS_ALLSTRING));


	// Initialize the default view for the gate diagram
	olViewer.SetViewerKey("GateDia");
	CString olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView(GetString(IDS_STRINGDEFAULT), olPossibleFilters,FALSE);
	olViewer.SelectView(GetString(IDS_STRINGDEFAULT));
	olViewer.SetGroup("Gatebereich");
	olViewer.SetSort(olSortOrder);
	olViewer.SetFilter("Gatebereich", olGateAreaFilter);
	olViewer.SetFilter("Airline", olAlcdFilter);
	olViewer.SetFilter("Agents",  olAgentsFilter);

	CString olColour;
	olColour.Format("%ld",SILVER);	olViewer.SetUserData("STOA",olColour);
	olColour.Format("%ld",WHITE);	olViewer.SetUserData("ETOA",olColour);
	olColour.Format("%ld",YELLOW);	olViewer.SetUserData("TMOA",olColour);
	olColour.Format("%ld",GREEN);	olViewer.SetUserData("LAND",olColour);
	olColour.Format("%ld",LIME);	olViewer.SetUserData("ONBL",olColour);
	olColour.Format("%ld",SILVER);	olViewer.SetUserData("STOD",olColour);
	olColour.Format("%ld",WHITE);	olViewer.SetUserData("ETOD",olColour);
	olColour.Format("%ld",YELLOW);	olViewer.SetUserData("SLOT",olColour);
	olColour.Format("%ld",LIME);	olViewer.SetUserData("OFBL",olColour);
	olColour.Format("%ld",GREEN);	olViewer.SetUserData("AIRB",olColour);

	olViewer.SetUserData("ARRIVAL",((ogBasicData.omDepArrMarkerGate.Find("ARRIVAL") != -1) ? "YES" : "NO"));
	olViewer.SetUserData("DEPARTURE",((ogBasicData.omDepArrMarkerGate.Find("DEPARTURE") != -1) ? "YES" : "NO"));
	olViewer.SetUserData("TURNAROUND",((ogBasicData.omDepArrMarkerGate.Find("TURNAROUND") != -1) ? "YES" : "NO"));

	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view
}

static void InitEquipmentDiagramDefaultView()
{
	CViewer olViewer;
	CStringArray olPossibleFilters;
//	CStringArray olSortOrder;

	// Read the default value from the database in the server
	TRACE("InitEquipmentDiagramDefaultView\n");
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("EquipmentGroup");
	olPossibleFilters.Add("EquipmentType");
	olPossibleFilters.Add("EquipmentName");

//	olSortOrder.RemoveAll();
//	olSortOrder.Add("EquipmentName");

	// Initialize the default view for the Equipment diagram
	olViewer.SetViewerKey("EquipmentDiagram");
	CString olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView(GetString(IDS_STRINGDEFAULT), olPossibleFilters,FALSE);
	olViewer.SelectView(GetString(IDS_STRINGDEFAULT));
	olViewer.SetGroup("EquipmentType");
//	olViewer.SetSort(olSortOrder);
	CStringArray olAll; olAll.Add(GetString(IDS_ALLSTRING));

	olViewer.SetFilter("EquipmentGroup", olAll);
	olViewer.SetFilter("EquipmentType", olAll);
	olViewer.SetFilter("EquipmentName", olAll);

	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view
}

static void InitPstDiagramDefaultView()
{
	CViewer olViewer;
	CStringArray olPossibleFilters;
	CStringArray olSortOrder;
	CStringArray olPstAreaFilter;
	CStringArray olAlcdFilter;
	CStringArray olAgentsFilter;
	CStringArray olDemandTypesFilter;
	CStringArray olKeycodesFilter;
	CStringArray olActFilter;
	CStringArray olNaturesFilter;
	CStringArray olConflictsFilter;
	CStringArray olDemandFunctionsFilter;
	CStringArray olEquDemandGroupsFilter;

	// Read the default value from the database in the server
	TRACE("InitPstDiagramDefaultView\n");
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("Pstbereich");
	olSortOrder.RemoveAll();
	olSortOrder.Add("Pstbereich");
	olPossibleFilters.Add("Airline");
	olPossibleFilters.Add("Agents");
	olPossibleFilters.Add("DemandType"); // Personnel/Equipment/Location
	olPossibleFilters.Add("Keycodes");
	olPossibleFilters.Add("Conflicts");
	olPossibleFilters.Add("Natures");
	olPossibleFilters.Add("Functions");
	olPossibleFilters.Add("EquDemandGroups");

//	ogAllocData.GetGroupNamesByGroupType(ALLOCUNITTYPE_PSTGROUP,olPstAreaFilter);
//	olPstAreaFilter.Add(GetString(IDS_NO_POSITION)); // "Ohne Pst"
//	olPstAreaFilter.Add(GetString(IDS_ALL_FLIGHTS));

	// airline codes format ALC2/ALC3
//	ogAltData.GetAlc2Alc3Strings(olAlcdFilter);

	olPstAreaFilter.Add(GetString(IDS_ALLSTRING));
	olAlcdFilter.Add(GetString(IDS_ALLSTRING));
	olAgentsFilter.Add(GetString(IDS_ALLSTRING));
	olDemandTypesFilter.Add(GetString(IDS_ALLSTRING));
	olKeycodesFilter.Add(GetString(IDS_ALLSTRING));
	olNaturesFilter.Add(GetString(IDS_ALLSTRING));
	olActFilter.Add(GetString(IDS_ALLSTRING));
	olConflictsFilter.Add(GetString(IDS_ALLSTRING));
	olDemandFunctionsFilter.Add(GetString(IDS_ALLSTRING));
	olEquDemandGroupsFilter.Add(GetString(IDS_ALLSTRING));

//	olDemandTypesFilter.Add(GetString(IDS_ALLSTRING));
//	olDemandTypesFilter.Add(GetString(IDS_EQUIPMENT_DEM));
//	olDemandTypesFilter.Add(GetString(IDS_LOCATION_DEM));


	// Initialize the default view for the gate diagram
	olViewer.SetViewerKey("PstDia");
	CString olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView(GetString(IDS_STRINGDEFAULT), olPossibleFilters,FALSE);
	olViewer.SelectView(GetString(IDS_STRINGDEFAULT));
	olViewer.SetGroup("Pstbereich");
	olViewer.SetSort(olSortOrder);
	olViewer.SetFilter("Pstbereich", olPstAreaFilter);
	olViewer.SetFilter("Airline", olAlcdFilter);
	olViewer.SetFilter("Agents",  olAgentsFilter);
	olViewer.SetFilter("DemandType",  olDemandTypesFilter);
	olViewer.SetFilter("Keycodes",  olKeycodesFilter);
	olViewer.SetFilter("Conflicts",  olConflictsFilter);
	olViewer.SetFilter("Natures",  olNaturesFilter);
	olViewer.SetFilter("Act",  olActFilter);
	olViewer.SetFilter("Functions", olDemandFunctionsFilter);
	olViewer.SetFilter("EquDemandGroups",olEquDemandGroupsFilter);

	CString olColour;
	olColour.Format("%ld",SILVER);	olViewer.SetUserData("STOA",olColour);
	olColour.Format("%ld",WHITE);	olViewer.SetUserData("ETOA",olColour);
	olColour.Format("%ld",YELLOW);	olViewer.SetUserData("TMOA",olColour);
	olColour.Format("%ld",GREEN);	olViewer.SetUserData("LAND",olColour);
	olColour.Format("%ld",LIME);	olViewer.SetUserData("ONBL",olColour);
	olColour.Format("%ld",SILVER);	olViewer.SetUserData("STOD",olColour);
	olColour.Format("%ld",WHITE);	olViewer.SetUserData("ETOD",olColour);
	olColour.Format("%ld",YELLOW);	olViewer.SetUserData("SLOT",olColour);
	olColour.Format("%ld",LIME);	olViewer.SetUserData("OFBL",olColour);
	olColour.Format("%ld",GREEN);	olViewer.SetUserData("AIRB",olColour);

	olViewer.SetUserData("ARRIVAL",((ogBasicData.omDepArrMarkerPst.Find("ARRIVAL") != -1) ? "YES" : "NO"));
	olViewer.SetUserData("DEPARTURE",((ogBasicData.omDepArrMarkerPst.Find("DEPARTURE") != -1) ? "YES" : "NO"));
	olViewer.SetUserData("TURNAROUND",((ogBasicData.omDepArrMarkerPst.Find("TURNAROUND") != -1) ? "YES" : "NO"));

	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view
}

static void InitRegnDiagramDefaultView()
{
	if(ogBasicData.bmDisplayRegistrations)
	{
		CViewer olViewer;
		CStringArray olPossibleFilters;
		CStringArray olSortOrder;
		CStringArray olRegnAreaFilter;
		CStringArray olTplFilter;

		// Read the default value from the database in the server
		TRACE("InitRegnDiagramDefaultView\n");
		olPossibleFilters.RemoveAll();
		olPossibleFilters.Add("RegnArea");
		olPossibleFilters.Add("Template");
		olPossibleFilters.Add("ThirdPartyFlights");
		olSortOrder.RemoveAll();
		olSortOrder.Add("RegnArea");
		olSortOrder.Add("Template");

		ogAllocData.GetGroupNamesByGroupType(ALLOCUNITTYPE_REGNGROUP,olRegnAreaFilter);
		ogTplData.GetTplNameByDalo(ALLOCUNITTYPE_REGN,olTplFilter);

		olTplFilter.Add(GetString(IDS_WITHOUT_TEMPLATE));
		olRegnAreaFilter.Add(GetString(IDS_THIRDPARTY)); // "Third Party"

		// Initialize the default view for the gate diagram
		olViewer.SetViewerKey("RegnDia");
		CString olLastView = olViewer.SelectView();	// remember the current view
		olViewer.CreateView(GetString(IDS_STRINGDEFAULT), olPossibleFilters,FALSE);
		olViewer.SelectView(GetString(IDS_STRINGDEFAULT));
		olViewer.SetGroup("RegnArea");
		olViewer.SetGroup("Template");
		olViewer.SetSort(olSortOrder);
		olViewer.SetFilter("RegnArea", olRegnAreaFilter);
		olViewer.SetFilter("Template", olTplFilter);
		if (olLastView != "")
			olViewer.SelectView(olLastView);	// restore the previously selected view
	}
}

static void InitCciDiagramDefaultView()
{
	CViewer olViewer;
	CStringArray olPossibleFilters;
	CStringArray olSortOrder;
	CStringArray olCciAreaFilter;
	CStringArray olCicHallFilter;
	CStringArray olCicRegionFilter;
	CStringArray olCicLineFilter;
	CStringArray olCicAirlineFilter;
	CStringArray olCicFunctionsFilter;

	// Read the default value from the database in the server
	TRACE("InitCciDiagramDefaultView\n");
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("Halle");
	olPossibleFilters.Add("Hall");
	olPossibleFilters.Add("Region");
	olPossibleFilters.Add("Line");
	olPossibleFilters.Add("Airline");
	olPossibleFilters.Add("Functions");
	olSortOrder.RemoveAll();
	olSortOrder.Add("none");

	ogAllocData.GetGroupNamesByGroupType(ALLOCUNITTYPE_CICGROUP,olCciAreaFilter);
	olCciAreaFilter.Add(GetString(IDS_STRING62514)); // "Without Counter"

	ogCicData.GetHalls(olCicHallFilter);
	olCicHallFilter.Add(GetString(IDS_STRING62514)); // "Without Counter"

	ogCicData.GetRegions(olCicRegionFilter);
	olCicRegionFilter.Add(GetString(IDS_STRING62514)); // "Without Counter"

	ogCicData.GetLines(olCicLineFilter);
	olCicLineFilter.Add(GetString(IDS_STRING62514)); // "Without Counter"

	olCicAirlineFilter.Add(GetString(IDS_ALLSTRING));

	olCicFunctionsFilter.Add(GetString(IDS_ALLSTRING));

	// Initialize the default view for the gate diagram
	olViewer.SetViewerKey("CciDia");
	CString olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView(GetString(IDS_STRINGDEFAULT), olPossibleFilters,FALSE);
	olViewer.SelectView(GetString(IDS_STRINGDEFAULT));
	olViewer.SetGroup("Halle");
	olViewer.SetSort(olSortOrder);
	olViewer.SetFilter("Halle", olCciAreaFilter);
	olViewer.SetFilter("Hall",	olCicHallFilter);
	olViewer.SetFilter("Region",olCicRegionFilter);
	olViewer.SetFilter("Line",	olCicLineFilter);
	olViewer.SetFilter("Airline",	olCicAirlineFilter);
	olViewer.SetFilter("Functions",	olCicFunctionsFilter);

	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view
}

static void InitStaffTableDefaultView()
{	
	CViewer olViewer;
	CStringArray olPossibleFilters;
	CStringArray olSortOrder;
	CStringArray olTeamFilter;
	CStringArray olShiftCodeFilter;
	CStringArray olRankFilter;
	CStringArray olPermitsFilter;
	CStringArray olTimeFilter;

	// Read the default value from the database in the server
	TRACE("InitStaffTableDefaultView\n");
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("Team");
	olPossibleFilters.Add("Schichtcode");
	olPossibleFilters.Add("Dienstrang");
	olPossibleFilters.Add("Permits");
	olPossibleFilters.Add("Zeit");
	olSortOrder.RemoveAll();
	olSortOrder.Add("Team");
	olSortOrder.Add("none");
	olSortOrder.Add("none");
	//hag  ogRnkData.GetAllRanks(olRankFilter);
	olRankFilter.Add(GetString(IDS_ALLSTRING));
	olPermitsFilter.Add(GetString(IDS_ALLSTRING));
	olTeamFilter.Add(GetString(IDS_ALLSTRING));

	//ogWgpData.GetAllTeams(olTeamFilter);
	//olTeamFilter.Add(""); // allows selection of emps without a team code


	//hag  ogShiftTypes.GetAllShiftTypes(olShiftCodeFilter);
	olShiftCodeFilter.Add ( GetString(IDS_ALLSTRING) );

	olTimeFilter.Add("0000");
	olTimeFilter.Add("2359");
	olTimeFilter.Add("0");  // 0=today, 1=tomorrow, 2=day after tomorrow

	// Initialize the default view for the gate diagram
	olViewer.SetViewerKey("StaffTab");
	CString olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView(GetString(IDS_STRINGDEFAULT), olPossibleFilters,FALSE);
	olViewer.SelectView(GetString(IDS_STRINGDEFAULT));
	olViewer.SetGroup("0");
	olViewer.SetSort(olSortOrder);
	olViewer.SetFilter("Team", olTeamFilter);
	olViewer.SetFilter("Schichtcode", olShiftCodeFilter);
	olViewer.SetFilter("Dienstrang", olRankFilter);
	olViewer.SetFilter("Permits", olPermitsFilter);
	olViewer.SetFilter("Zeit", olTimeFilter);
	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view
}

static void InitFlightScheduleDefaultView()
{
	CViewer olViewer;
	CStringArray olPossibleFilters;
	CStringArray olSortOrder;
	CStringArray olGataFilter;
	CStringArray olGatdFilter;
	CStringArray olPstaFilter;
	CStringArray olPstdFilter;
	CStringArray olAlcdFilter;
	CStringArray olInboundFilter;
	CStringArray olOutboundFilter;
	CStringArray olTimeFilter;

	// Read the default value from the database in the server
	TRACE("InitFlightScheduleDefaultView\n");
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("Gate Arrival");
	olPossibleFilters.Add("Gate Departure");
	olPossibleFilters.Add("PosArr");
	olPossibleFilters.Add("PosDep");
	olPossibleFilters.Add("Airline");
	olPossibleFilters.Add("Inbound");
	olPossibleFilters.Add("Outbound");
	olPossibleFilters.Add("Zeit");
	olSortOrder.RemoveAll();
	olSortOrder.Add("Gate Arrival");
	olSortOrder.Add("none");
	olSortOrder.Add("none");

	olGataFilter.Add(GetString(IDS_ALLSTRING));
	olGatdFilter.Add(GetString(IDS_ALLSTRING));
	olPstaFilter.Add(GetString(IDS_ALLSTRING));
	olPstdFilter.Add(GetString(IDS_ALLSTRING));
	olAlcdFilter.Add(GetString(IDS_ALLSTRING));

	olInboundFilter.Add("1");
	olOutboundFilter.Add("1");
	olTimeFilter.Add("0000");
	olTimeFilter.Add("2359");
	olTimeFilter.Add("0");  // 0=today, 1=tomorrow, 2=day after tomorrow

	// Initialize the default view for the gate diagram
	olViewer.SetViewerKey("FltSched");
	CString olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView(GetString(IDS_STRINGDEFAULT), olPossibleFilters,FALSE);
	olViewer.SelectView(GetString(IDS_STRINGDEFAULT));
	olViewer.SetGroup("0");
	olViewer.SetSort(olSortOrder);
	olViewer.SetFilter("Gate Arrival", olGataFilter);
	olViewer.SetFilter("Gate Departure", olGatdFilter);
	olViewer.SetFilter("PosArr", olPstaFilter);
	olViewer.SetFilter("PosDep", olPstdFilter);
	olViewer.SetFilter("Airline", olAlcdFilter);
	olViewer.SetFilter("Inbound", olInboundFilter);
	olViewer.SetFilter("Outbound", olOutboundFilter);
	olViewer.SetFilter("Zeit", olTimeFilter);
	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view
}

static void InitFlightJobsTableDefaultView()
{
	CViewer olViewer;
	CStringArray olPossibleFilters;
	CStringArray olSortOrder;
	CStringArray olAlcdFilter;
	CStringArray olTimeFilter;
//	CStringArray olAlocFilter;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("Airline");
	olPossibleFilters.Add("Time");
//	olPossibleFilters.Add("Aloc");

	olSortOrder.RemoveAll();
	olSortOrder.Add("none");
	olSortOrder.Add("none");

	olAlcdFilter.Add(GetString(IDS_ALLSTRING));
//	olAlocFilter.Add(GetString(IDS_ALLSTRING));

	olTimeFilter.Add("0000");
	olTimeFilter.Add("2359");
	olTimeFilter.Add("0");  // 0=today, 1=tomorrow, 2=day after tomorrow

	// Initialize the default view for the gate diagram
	olViewer.SetViewerKey("FlightJobsTable");
	CString olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView(GetString(IDS_STRINGDEFAULT), olPossibleFilters,FALSE);
	olViewer.SelectView(GetString(IDS_STRINGDEFAULT));
	olViewer.SetGroup("0");


//	// get all possible demand types
//	for (int i = 0; i < ogDemandData.GetCountOfDemandTypes(); i++)
//	{
//		DEMANDDATA rlDemand;
//		rlDemand.Aloc[0] = '0' + i;
//		olAlocFilter.Add(ogDemandData.GetStringForDemandType(rlDemand));
//	}


	olViewer.SetSort(olSortOrder);
	olViewer.SetFilter("Airline", olAlcdFilter);
	olViewer.SetFilter("Time", olTimeFilter);
//	olViewer.SetFilter("Aloc", olAlocFilter);

	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view
}

static void InitGateTableDefaultView()
{
	CViewer olViewer;
	CStringArray olPossibleFilters;
	CStringArray olSortOrder;
	CStringArray olGateAreaFilter;
	CStringArray olAlcdFilter;
	CStringArray olTimeFilter;

	// Read the default value from the database in the server
	TRACE("InitGateTableDefaultView\n");
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("Gate");
	olPossibleFilters.Add("Airline");
	//olPossibleFilters.Add("Zeit");
	olSortOrder.RemoveAll();
	olSortOrder.Add("Gate Bereich");

	/*
	CCSPtrArray <ALLOCUNIT> olGates;
	ogAllocData.GetUnitsByGroupType(ALLOCUNITTYPE_GATEGROUP,olGates);
	int ilNumGates = olGates.GetSize();
	for(int ilGate = 0; ilGate < ilNumGates; ilGate++)
	{
		ALLOCUNIT *prlGate = &olGates[ilGate];
		olGateAreaFilter.Add(prlGate->Name);
	}
	*/

	//wny : 23/06/2011 
	//Get the Gate Name from basic data instead of static group
	ogGatData.GetAllValidGateNames(olGateAreaFilter);

	// airline codes format ALC2/ALC3
	ogAltData.GetAlc2Alc3Strings(olAlcdFilter);

	//olTimeFilter.Add("0000");
	//olTimeFilter.Add("2359");
	//olTimeFilter.Add("0");  // 0=today, 1=tomorrow, 2=day after tomorrow

	// Initialize the default view for the gate diagram
	olViewer.SetViewerKey("GateTab");
	CString olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView(GetString(IDS_STRINGDEFAULT), olPossibleFilters,FALSE);
	olViewer.SelectView(GetString(IDS_STRINGDEFAULT));
	olViewer.SetGroup("0");
	olViewer.SetSort(olSortOrder);
	olViewer.SetFilter("Gate", olGateAreaFilter);
	olViewer.SetFilter("Airline", olAlcdFilter);
	//olViewer.SetFilter("Zeit", olTimeFilter);
	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view
}

static void InitCciTableDefaultView()
{
	// Initialize the default view for the checkin table
	CViewer olViewer;
	olViewer.SetViewerKey("CciTab");

	CStringArray olPossibleFilters;
	CStringArray olSortOrder;
	CStringArray olCciAreaFilter;
	CStringArray olCicHallFilter;
	CStringArray olCicRegionFilter;
	CStringArray olCicLineFilter;

	// Read the default value from the database in the server
	TRACE("InitCciTableDefaultView\n");
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("Halle");
	olPossibleFilters.Add("Hall");
	olPossibleFilters.Add("Region");
	olPossibleFilters.Add("Line");
	olSortOrder.RemoveAll();
	olSortOrder.Add("none");

	ogAllocData.GetGroupNamesByGroupType(ALLOCUNITTYPE_CICGROUP,olCciAreaFilter);

	ogCicData.GetHalls(olCicHallFilter);
	ogCicData.GetRegions(olCicRegionFilter);
	ogCicData.GetLines(olCicLineFilter);

	CString olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView(GetString(IDS_STRINGDEFAULT), olPossibleFilters,FALSE);
	olViewer.SelectView(GetString(IDS_STRINGDEFAULT));
	olViewer.SetGroup("Halle");
	olViewer.SetSort(olSortOrder);
	olViewer.SetFilter("Halle", olCciAreaFilter);
	olViewer.SetFilter("Hall",	olCicHallFilter);
	olViewer.SetFilter("Region",olCicRegionFilter);
	olViewer.SetFilter("Line",	olCicLineFilter);

	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view
}


static void InitPrmTableDefaultView()
{
	// Initialize the default view for the checkin table
	CViewer olViewer;
	olViewer.SetViewerKey("PrmTab");

	CStringArray olPossibleFilters;
	CStringArray olSortOrder;
	CStringArray olCciAreaFilter;
	CStringArray olCicHallFilter;
	CStringArray olCicRegionFilter;
	CStringArray olCicLineFilter;
	CStringArray olAlcdFilter;
	CStringArray olServiceFilter;
	CStringArray olPrmtFilter;
	//added by MAX
	CStringArray olPrmLAFilter;

	CStringArray olAdidFilter;
	CStringArray olColumnFilter;
	CStringArray olTermFilter;

	// Read the default value from the database in the server
	TRACE("InitCciTableDefaultView\n");
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("Halle");
	olPossibleFilters.Add("Hall");
	olPossibleFilters.Add("Region");
	olPossibleFilters.Add("Line");
	olPossibleFilters.Add("Service");
	olPossibleFilters.Add("Prmt");
	olPossibleFilters.Add("Adid");
	olPossibleFilters.Add("Term");

	olPossibleFilters.Add("PrmAL");

	olSortOrder.RemoveAll();
	olSortOrder.Add("none");
	olAlcdFilter.Add(GetString(IDS_ALLSTRING));
	olServiceFilter.Add(GetString(IDS_ALLSTRING));
	olPrmtFilter.Add(GetString(IDS_ALLSTRING));
	olAdidFilter.Add(GetString(IDS_ALLSTRING));
	olTermFilter.Add(GetString(IDS_ALLSTRING));

	olPrmLAFilter.Add(GetString(IDS_ALLSTRING));

	ogAllocData.GetGroupNamesByGroupType(ALLOCUNITTYPE_CICGROUP,olCciAreaFilter);

	ogCicData.GetHalls(olCicHallFilter);
	ogCicData.GetRegions(olCicRegionFilter);
	ogCicData.GetLines(olCicLineFilter);

	// get all possible view columns
	PrmTableViewer::GetDefaultViewColumns(olColumnFilter);	

	CString olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView(GetString(IDS_STRINGDEFAULT), olPossibleFilters,FALSE);
	olViewer.SelectView(GetString(IDS_STRINGDEFAULT));
	olViewer.SetGroup("Halle");
	olViewer.SetSort(olSortOrder);
	olViewer.SetFilter("Halle", olCciAreaFilter);
	olViewer.SetFilter("Hall",	olCicHallFilter);
	olViewer.SetFilter("Region",olCicRegionFilter);
	olViewer.SetFilter("Line",	olCicLineFilter);
	olViewer.SetFilter("Airline", olAlcdFilter);
	olViewer.SetFilter("Service", olServiceFilter);
	olViewer.SetFilter("Prmt", olPrmtFilter);
	olViewer.SetFilter("Adid", olAdidFilter);
	olViewer.SetFilter("Adid", olAdidFilter);
	olViewer.SetFilter("Columns",olColumnFilter);
	olViewer.SetFilter("Terminal",olTermFilter);

	olViewer.SetFilter("PrmAL",olPrmLAFilter);


	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view
}

static void InitRequestTableDefaultView()
{
	CViewer olViewer;
	CStringArray olPossibleFilters;
	CStringArray olSortOrder;
//	CStringArray olValdFilter;
//	CStringArray olCratFilter;

	// Read the default value from the database in the server
	TRACE("InitRequestTableDefaultView\n");
	olPossibleFilters.RemoveAll();
//	olPossibleFilters.Add("Vald");
//	olPossibleFilters.Add("Crat");
	olSortOrder.RemoveAll();
	olSortOrder.Add("none");
//	olValdFilter.Add("0000");
//	olValdFilter.Add("2359");
//	olValdFilter.Add("0");  // 0=today, 1=tomorrow, 2=day after tomorrow
//	olCratFilter.Add("0000");
//	olCratFilter.Add("2359");
//	olCratFilter.Add("0");  // 0=today, 1=tomorrow, 2=day after tomorrow

	// Initialize the default view for the gate diagram
	olViewer.SetViewerKey("ReqTab");
	CString olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView(GetString(IDS_STRINGDEFAULT), olPossibleFilters,FALSE);
	olViewer.SelectView(GetString(IDS_STRINGDEFAULT));
	olViewer.SetGroup("0");
	olViewer.SetSort(olSortOrder);
//	olViewer.SetFilter("Vald", olValdFilter);
//	olViewer.SetFilter("Crat", olCratFilter);
	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view
}

static void InitConflictTableDefaultView()
{
	CViewer olViewer;
	CStringArray olPossibleFilters;
	CStringArray olSortOrder;
	CStringArray olConflictTypeFilter;
	CStringArray olObjectTypeFilter;
	CStringArray olTimeFilter;
	CStringArray olAlcdFilter;
	CStringArray olAlocFilter;
	CStringArray olAgentsFilter;
	CStringArray olPoolsFilter;
	CStringArray olFunctionsFilter;

	// Read the default value from the database in the server
	TRACE("InitConflictTableDefaultView\n");
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("ConflictType");
	olPossibleFilters.Add("Object Typ");
	olPossibleFilters.Add("Zeit");
	olPossibleFilters.Add("Airline");
	olPossibleFilters.Add("Aloc");
	olPossibleFilters.Add("Agents");
	olPossibleFilters.Add("Pools");
	olPossibleFilters.Add("Functions");
	olSortOrder.RemoveAll();
	olSortOrder.Add("none");
	// Before finish: we should save all possible conflict types and alid for
	// the default view.
	olObjectTypeFilter.Add(GetString(IDS_STRING61608)); // "Job"
	olObjectTypeFilter.Add(GetString(IDS_STRING61609)); // "Flight"
	olObjectTypeFilter.Add(GetString(IDS_DEMOBJTYPE)); // "Demand"

	olTimeFilter.Add("0000");
	olTimeFilter.Add("2359");
	olTimeFilter.Add("0");  // 0=today, 1=tomorrow, 2=day after tomorrow

	ogConflicts.GetAllConflictTypes(olConflictTypeFilter);

	// airline codes format ALC2/ALC3
	ogAltData.GetAlc2Alc3Strings(olAlcdFilter);

	olAlocFilter.Add(GetString(IDS_ALLSTRING));

	// get all possible aloc's
	const char **pclAllocGroupType = DemandTableViewer::omAllocGroupTypes;
	while(*pclAllocGroupType)
	{
		olAlocFilter.Add(*pclAllocGroupType);
		olPossibleFilters.Add(*pclAllocGroupType++);
	}

	olAgentsFilter.Add ( GetString(IDS_ALLSTRING) );
	olPoolsFilter.Add ( GetString(IDS_ALLSTRING) );
	olFunctionsFilter.Add ( GetString(IDS_ALLSTRING) );

	// Initialize the default view for the gate diagram
	olViewer.SetViewerKey("ConfTab");
	CString olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView(GetString(IDS_STRINGDEFAULT), olPossibleFilters,FALSE);
	olViewer.SelectView(GetString(IDS_STRINGDEFAULT));
	olViewer.SetGroup("0");
	olViewer.SetSort(olSortOrder);
	olViewer.SetFilter("ConflictType", olConflictTypeFilter);
	olViewer.SetFilter("Object Typ", olObjectTypeFilter);
	olViewer.SetFilter("Airline", olAlcdFilter);
	olViewer.SetFilter("Zeit", olTimeFilter);
	olViewer.SetFilter("Aloc", olAlocFilter);
	olViewer.SetFilter("Agents",  olAgentsFilter);
	olViewer.SetFilter("Functions",  olFunctionsFilter);
	olViewer.SetFilter("Pools",  olPoolsFilter);

	// get all possible alids
	pclAllocGroupType = DemandTableViewer::omAllocGroupTypes;
	while(*pclAllocGroupType)
	{
		CStringArray olAlidFilter;
		olAlidFilter.Add(GetString(IDS_ALLSTRING));

		CCSPtrArray <ALLOCUNIT> olAllocGroups;
		ogAllocData.GetGroupsByGroupType(*pclAllocGroupType,olAllocGroups);
		int ilNumGroups = olAllocGroups.GetSize();
		for(int ilGroup = 0; ilGroup < ilNumGroups; ilGroup++)
		{
			ALLOCUNIT *prlGroup = &olAllocGroups[ilGroup];
			olAlidFilter.Add(prlGroup->Name);
		}

		olViewer.SetFilter(*pclAllocGroupType++,olAlidFilter);
	}

	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view
}

static void InitPrePlanTableDefaultView()
{
	CViewer olViewer;
	CStringArray olPossibleFilters;
	CStringArray olSortOrder;
	CStringArray olTeamFilter;
	CStringArray olShiftCodeFilter;
	CStringArray olRankFilter;
	CStringArray olPermitsFilter;
	CStringArray olTimeFilter;

	// Read the default value from the database in the server
	TRACE("InitPrePlanTableDefaultView\n");
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("Team");
	olPossibleFilters.Add("Schichtcode");
	olPossibleFilters.Add("Dienstrang");
	olPossibleFilters.Add("Zeit");
	olPossibleFilters.Add("Permits");

	olSortOrder.RemoveAll();
	olSortOrder.Add("Team");
	olSortOrder.Add("Schichtbegin");
	olSortOrder.Add("Dienstrang");
	//hag  ogRnkData.GetAllRanks(olRankFilter);
	olRankFilter.Add ( GetString(IDS_ALLSTRING) );
	olPermitsFilter.Add ( GetString(IDS_ALLSTRING) );
	olTeamFilter.Add ( GetString(IDS_ALLSTRING) );
	//ogWgpData.GetAllTeams(olTeamFilter);
	//olTeamFilter.Add(""); // allows selection of emps without a team code

	
	//hag  ogShiftTypes.GetAllShiftTypes(olShiftCodeFilter);
	olShiftCodeFilter.Add ( GetString(IDS_ALLSTRING) );

	olTimeFilter.Add("0000");
	olTimeFilter.Add("2359");
	olTimeFilter.Add("1");  // 0=today, 1=tomorrow, 2=day after tomorrow

	// Initialize the default view for the gate diagram
	olViewer.SetViewerKey("PPTab");
	CString olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView(GetString(IDS_STRINGDEFAULT), olPossibleFilters,FALSE);
	olViewer.SelectView(GetString(IDS_STRINGDEFAULT));
	olViewer.SetGroup("1");
	olViewer.SetSort(olSortOrder);
	olViewer.SetFilter("Team", olTeamFilter);
	olViewer.SetFilter("Schichtcode", olShiftCodeFilter);
	olViewer.SetFilter("Dienstrang", olRankFilter);
	olViewer.SetFilter("Permits", olPermitsFilter);
	olViewer.SetFilter("Zeit", olTimeFilter);
	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view
}

static void InitFlIndDemandTableDefaultView()
{
	CViewer olViewer;
	CStringArray olPossibleFilters;
	CStringArray olNameFilters;
	CStringArray olCodeFilters;
	CStringArray olDetyFilters;
	CStringArray olSortOrder;

	// Read the default value from the database in the server
	TRACE("InitFlIndDemandTableDefaultView\n");
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("Name");
	olPossibleFilters.Add("Code");
	olPossibleFilters.Add("Dety");
	olPossibleFilters.Add(GetString(IDS_ALLSTRING));

	olNameFilters.Add(GetString(IDS_ALLSTRING));
	olCodeFilters.Add(GetString(IDS_ALLSTRING));
	olDetyFilters.Add(GetString(IDS_ALLSTRING));

	olSortOrder.RemoveAll();
	olSortOrder.Add("Name");
	olSortOrder.Add("Code");

	// Initialize the default view for the flight independent table viewer
	olViewer.SetViewerKey("FidTab");
	CString olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView(GetString(IDS_STRINGDEFAULT), olPossibleFilters,FALSE);
	olViewer.SelectView(GetString(IDS_STRINGDEFAULT));
	olViewer.SetSort(olSortOrder);
	olViewer.SetFilter("Name", olNameFilters);
	olViewer.SetFilter("Code", olCodeFilters);
	olViewer.SetFilter("Dety", olCodeFilters);

	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view
}

static void InitDemandTableDefaultView()
{
	CViewer olViewer;
	CStringArray olPossibleFilters;
	CStringArray olSortOrder;
	CStringArray olAlocFilter;
	CStringArray olDetyFilter;
	CStringArray olRetyFilter;
	CStringArray olAirlineFilter;
	CStringArray olNatureFilter;
	CStringArray olAirportFilter;
	CStringArray olColumnFilter;
	CStringArray olRankFilter;
	CStringArray olServiceFilter;
	CStringArray olPermitsFilter;
	CStringArray olCicFilter;
	CStringArray olBelt1Filter;

	// Read the default value from the database in the server
	TRACE("InitDemandTableDefaultView\n");
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("Aloc");
	olPossibleFilters.Add("Dety");
	olPossibleFilters.Add("Airline");
	olPossibleFilters.Add("Rety");
	olPossibleFilters.Add("Nature");
	olPossibleFilters.Add("Airport");
	olPossibleFilters.Add("Columns");
	olPossibleFilters.Add("Dienstrang");
	olPossibleFilters.Add("Service");
	olPossibleFilters.Add("Permits");
	olPossibleFilters.Add("Cic");
	olPossibleFilters.Add("Belt1");

	olSortOrder.RemoveAll();
	olSortOrder.Add("Aloc");
	olSortOrder.Add("Debe");
	olSortOrder.Add("Deen");
	olSortOrder.Add("Dedu");
	olSortOrder.Add("Obty");
	olSortOrder.Add("Dety");
	olSortOrder.Add("AFnum");
	olSortOrder.Add("DFnum");

	olAirlineFilter.Add(GetString(IDS_ALLSTRING));
	olAlocFilter.Add(GetString(IDS_ALLSTRING));
	olDetyFilter.Add(GetString(IDS_ALLSTRING));
	olRankFilter.Add(GetString(IDS_ALLSTRING));
	olServiceFilter.Add(GetString(IDS_ALLSTRING));
	olPermitsFilter.Add(GetString(IDS_ALLSTRING));
	olNatureFilter.Add(GetString(IDS_ALLSTRING));
	olAirportFilter.Add(GetString(IDS_ALLSTRING));
	olCicFilter.Add(GetString(IDS_ALLSTRING));
	olBelt1Filter.Add(GetString(IDS_ALLSTRING));

	// airline codes format ALC2/ALC3
	ogAltData.GetAlc2Alc3Strings(olAirlineFilter);

	// get all possible aloc's
	const char **pclAllocGroupType = DemandTableViewer::omAllocGroupTypes;
	while(*pclAllocGroupType)
	{
		olAlocFilter.Add(*pclAllocGroupType);
		olPossibleFilters.Add(*pclAllocGroupType++);
	}

	// get all possible demand types
	for (int i = 0; i < ogDemandData.GetCountOfDemandTypes(); i++)
	{
		DEMANDDATA rlDemand;
		rlDemand.Dety[0] = '0' + i;
		olDetyFilter.Add(ogDemandData.GetStringForDemandType(rlDemand));
	}

	olRetyFilter.Add(GetString(IDS_PERSONNEL_DEM));
	olRetyFilter.Add(GetString(IDS_EQUIPMENT_DEM));
	olRetyFilter.Add(GetString(IDS_LOCATION_DEM));
	

	// get all possible view columns
	DemandTableViewer::GetDefaultViewColumns(olColumnFilter);	

	// Initialize the default view for the gate diagram
	olViewer.SetViewerKey("DemandTab");
	CString olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView(GetString(IDS_STRINGDEFAULT), olPossibleFilters,FALSE);
	olViewer.SelectView(GetString(IDS_STRINGDEFAULT));
	olViewer.SetGroup("1");
	olViewer.SetSort(olSortOrder);
	olViewer.SetFilter("Aloc", olAlocFilter);
	olViewer.SetFilter("Dety", olDetyFilter);
	olViewer.SetFilter("Rety", olRetyFilter);
	olViewer.SetFilter("Airline",olAirlineFilter);
	olViewer.SetFilter("Nature",olNatureFilter);
	olViewer.SetFilter("Airport",olNatureFilter);
	olViewer.SetFilter("Columns",olColumnFilter);
	olViewer.SetFilter("Dienstrang", olRankFilter);
	olViewer.SetFilter("Service", olServiceFilter);
	olViewer.SetFilter("Permits", olPermitsFilter);
	olViewer.SetFilter("Cic", olCicFilter);
	olViewer.SetFilter("Belt1", olBelt1Filter);

	// get all possible alids
	pclAllocGroupType = DemandTableViewer::omAllocGroupTypes;
	while(*pclAllocGroupType)
	{
		CStringArray olAlidFilter;
		olAlidFilter.Add(GetString(IDS_ALLSTRING));

		CCSPtrArray <ALLOCUNIT> olAllocGroups;
		ogAllocData.GetGroupsByGroupType(*pclAllocGroupType,olAllocGroups);
		int ilNumGroups = olAllocGroups.GetSize();
		for(int ilGroup = 0; ilGroup < ilNumGroups; ilGroup++)
		{
			ALLOCUNIT *prlGroup = &olAllocGroups[ilGroup];
			olAlidFilter.Add(prlGroup->Name);
		}

		olViewer.SetFilter(*pclAllocGroupType++,olAlidFilter);
	}

	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view
}

static void InitCicDemandTableDefaultView()
{
	CViewer olViewer;
	CStringArray olPossibleFilters;
	CStringArray olSortOrder;
	CStringArray olDetyFilter;
	CStringArray olAirlineFilter;
	CStringArray olClassesFilter;
	CStringArray olGroupsFilter;
	CStringArray olColumnFilter;
	CStringArray olFunctionsFilter;

	// Read the default value from the database in the server
	TRACE("InitCicDemandTableDefaultView\n");
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("Dety");
	olPossibleFilters.Add("Airline");
	olPossibleFilters.Add("Classes");
	olPossibleFilters.Add("Groups");
	olPossibleFilters.Add("Columns");
	olPossibleFilters.Add("Functions");

	olSortOrder.RemoveAll();
	olSortOrder.Add("Debe");
	olSortOrder.Add("Deen");
	olSortOrder.Add("Dedu");
	olSortOrder.Add("Dety");
	olSortOrder.Add("DFnum");

	olDetyFilter.Add(GetString(IDS_ALLSTRING));

	// get all possible demand types
	for (int i = 0; i < ogDemandData.GetCountOfDemandTypes(); i++)
	{
		DEMANDDATA rlDemand;
		rlDemand.Dety[0] = '0' + i;
		olDetyFilter.Add(ogDemandData.GetStringForDemandAndResourceType(rlDemand));
	}

	// airline codes format ALC2/ALC3
	//ogAltData.GetAlc2Alc3Strings(olAirlineFilter);
	olAirlineFilter.Add(GetString(IDS_ALLSTRING));
	olFunctionsFilter.Add(GetString(IDS_ALLSTRING));
	
	olClassesFilter.Add(GetString(IDS_ALLSTRING));
	olGroupsFilter.Add(GetString(IDS_ALLSTRING));

	// get all possible view columns
	CicDemandTableViewer::GetDefaultViewColumns(olColumnFilter);	

	// Initialize the default view for the gate diagram
	olViewer.SetViewerKey("CicDemTab");
	CString olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView(GetString(IDS_STRINGDEFAULT), olPossibleFilters,FALSE);
	olViewer.SelectView(GetString(IDS_STRINGDEFAULT));
//	olViewer.SetGroup("1");
	olViewer.SetSort(olSortOrder);
	olViewer.SetFilter("Dety", olDetyFilter);
	olViewer.SetFilter("Airline",olAirlineFilter);
	olViewer.SetFilter("Classes",olClassesFilter);
	olViewer.SetFilter("Groups",olGroupsFilter);
	olViewer.SetFilter("Columns",olColumnFilter);
	olViewer.SetFilter("Functions",olFunctionsFilter);

	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view
}


COpssPmApp::~COpssPmApp()
{
	TRACE("COpssPmApp::~COpssPmApp\n");
	
//	ogConflictConfData.DeleteAll();

	DeleteBrushes();
	delete CGUILng::TheOne();
	delete pomMutex;
	AatHelp::ExitApp();
}

BOOL COpssPmApp::InitInstance()
{
	bgIsInitialized = FALSE;


	AfxEnableControlContainer();

	if (!AfxOleInit())
	{
	//	AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}

	if (!AfxSocketInit())
	{
		AfxMessageBox("Failed to init sockets!");
		return FALSE;
	}

	AfxInitRichEdit();

	if (!AatHelp::Initialize(pcgAppName))
	{
		CString olErrorMsg;
		AatHelp::GetLastError(olErrorMsg);
		MessageBox(NULL,olErrorMsg,"Error",MB_OK);
	}

	VersionInfo rlInfo; 
	VersionInfo::GetVersionInfo(NULL,rlInfo); 
	CCSCedaData::bmVersCheck = true; 
	strcpy(CCSCedaData::pcmVersion, rlInfo.omFileVersion); 
	strcpy(CCSCedaData::pcmInternalBuild, rlInfo.omPrivateBuild); 

	strncpy(CCSCedaData::pcmTableExt, "TAB",3);
	strncpy(CCSCedaData::pcmHomeAirport, "CKI",3);
	strcpy(CCSCedaData::pcmApplName, pcgAppName);

//    SetDialogBkColor();        // Set dialog background color to gray
    LoadStdProfileSettings();  // Load standard INI file options (including MRU)


#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	SetCurrDate();

	SetRegistryKey("UFIS");
	// read the configuration file for home airport and message file name
	ReadCfgFile(pcgAppName);

	strncpy(CCSCedaData::pcmTableExt, pcgTableExt,3);
	strncpy(CCSCedaData::pcmHomeAirport, pcgHomeAirport,3);
	strcpy(CCSCedaData::pcmApplName, pcgAppName);

	// Check condition of the communication with the server
    if (ogCommHandler.Initialize() != true)  // connection error?
    {
        AfxMessageBox(CString("CedaCom:\n") + ogCommHandler.LastError());
		CleanUp();
        return FALSE;
    }

	ogBCD.pomCommHandler = &ogCommHandler;
	char pclTmp[128];
	CGUILng* ogGUILng = CGUILng::TheOne();

	CString olConfigFileName = ogBasicData.GetConfigFileName();

//	char pclDBParam[100];
//	CString olErrorTxt;
//    GetPrivateProfileString("GLOBAL", "LANGUAGE", "", pclTmp, sizeof pclTmp, olConfigFileName);
//    GetPrivateProfileString(pcgAppName, "DBParam", "", pclDBParam, sizeof pclDBParam, olConfigFileName);
//	bool blLangInitOk = ogGUILng->MemberInit(&olErrorTxt, "XXX", pcgAppName, pcgHomeAirport, pclTmp, pclDBParam );

    GetPrivateProfileString("GLOBAL", "LANGUAGE", "", pclTmp, sizeof pclTmp, olConfigFileName);
	ogGUILng->SetLanguage(CString(pclTmp));	

    GetPrivateProfileString(pcgAppName, "DBParam", "", pclTmp, sizeof pclTmp, olConfigFileName);
	if(!strcmp(pclTmp,"1"))
	{
		ogGUILng->SetStoreInDB(CString(pclTmp));
	}

	char pclConfigPath[256];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));


	char pclUseBcProxy[256];
	GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclUseBcProxy, sizeof pclUseBcProxy, pclConfigPath);

	//MWO/RRO 25.03.03
	if (stricmp(pclUseBcProxy,"BCPROXY") == 0) 
	{
		ogBcHandle.UseBcProxy();
		ogBcHandle.StopBc();
	}
	else if (stricmp(pclUseBcProxy,"BCCOM") == 0)
	{
		ogBcHandle.UseBcCom();
		ogBcHandle.StopBc();
	}

	GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclUseBcProxy, sizeof pclUseBcProxy, pclConfigPath);


	if (IsAlreadyRunning())
	{
		if (MessageBox(NULL,GetString(IDS_STRING61991),GetString(IDS_STRING61269),MB_ICONSTOP|MB_YESNO) == IDNO)
			return FALSE;
	}

	
	ogBasicData.SetUtcDifferenceFromApttab();

	// the following broadcast is sent once a minte - not actually used by any objects
	// but used to set the broadcast traffic light
	ogBcHandle.AddTableCommand(CString("TIMERS"), CString("TIME"), BROADCAST_CHECK,true);

	// Insert Flight (skey list)
	if(HandleIsfBroadcasts())
	{
		ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("ISF"), BC_FLIGHT_INSERT, true);
	}
	// Insert Flight (all fields)
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("IFR"), BC_FLIGHT_CHANGE, true);
	// Update Flight
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("UFR"), BC_FLIGHT_CHANGE, true);
	// Delete Flight
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("DFR"), BC_FLIGHT_DELETE, true);
	// Split
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("SPR"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("UPS"), BC_FLIGHT_CHANGE, true);
	// Join
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("UPJ"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("JOF"), BC_FLIGHT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("AFT") + CString(pcgTableExt), CString("RAC"), BC_FLIGHT_RAC, true);

	// demands
	ogBcHandle.AddTableCommand(CString("DEM") + CString(pcgTableExt), CString("IRT"), BC_DEMAND_NEW, false,pcgAppName);
	ogBcHandle.AddTableCommand(CString("DEM") + CString(pcgTableExt), CString("URT"), BC_DEMAND_CHANGE, false,pcgAppName);
	ogBcHandle.AddTableCommand(CString("DEM") + CString(pcgTableExt), CString("DRT"), BC_DEMAND_DELETE, false,pcgAppName);
	ogBcHandle.AddTableCommand(CString("RELDEM"), CString("SBC"), BC_DEMAND_REL, true);
	ogBcHandle.AddTableCommand(CString("UPDDEM"), CString("SBC"), BC_DEMAND_UPD, true);

	// jobs
	ogBcHandle.AddTableCommand(CString("JOB") + CString(pcgTableExt), CString("IRT"), BC_JOB_NEW, false,pcgAppName);
	ogBcHandle.AddTableCommand(CString("JOB") + CString(pcgTableExt), CString("URT"), BC_JOB_CHANGE, false,pcgAppName);
	ogBcHandle.AddTableCommand(CString("JOB") + CString(pcgTableExt), CString("DRT"), BC_JOB_DELETE, false,pcgAppName);
	ogBcHandle.AddTableCommand(CString("RELJOB"), CString("SBC"), BC_JOB_REL, true);
	ogBcHandle.AddTableCommand(CString("UPDJOB"), CString("SBC"), BC_JOB_UPD, true);

	// jod
	ogBcHandle.AddTableCommand(CString("JOD") + CString(pcgTableExt), CString("URT"), BC_JOD_CHANGE, false,pcgAppName);
	ogBcHandle.AddTableCommand(CString("JOD") + CString(pcgTableExt), CString("IRT"), BC_JOD_CHANGE, false,pcgAppName);
	ogBcHandle.AddTableCommand(CString("JOD") + CString(pcgTableExt), CString("DRT"), BC_JOD_DELETE, false,pcgAppName);
	ogBcHandle.AddTableCommand(CString("UPDJOD"), CString("SBC"), BC_JOD_UPD, true);

	// PRM (DPXTAB)
	ogBcHandle.AddTableCommand(CString("DPX") + CString(pcgTableExt), CString("IRT"), BC_DPX_NEW, false,pcgAppName);
	ogBcHandle.AddTableCommand(CString("DPX") + CString(pcgTableExt), CString("URT"), BC_DPX_CHANGE, false,pcgAppName);
	ogBcHandle.AddTableCommand(CString("DPX") + CString(pcgTableExt), CString("DRT"), BC_DPX_DELETE, false,pcgAppName);

	// lli: PRM - PDA (PDATAB) Add commands for PDA updates
	ogBcHandle.AddTableCommand(CString("PDA") + CString(pcgTableExt), CString("IRT"), BC_PDA_NEW, false,pcgAppName);
	ogBcHandle.AddTableCommand(CString("PDA") + CString(pcgTableExt), CString("URT"), BC_PDA_CHANGE, false,pcgAppName);
	ogBcHandle.AddTableCommand(CString("PDA") + CString(pcgTableExt), CString("DRT"), BC_PDA_DELETE, false,pcgAppName);

	// aza table
	if(ogBasicData.bmDisplayRegistrations)
	{
		ogBcHandle.AddTableCommand(CString("AZA") + CString(pcgTableExt), CString("IAZ"), BC_AZA_CHANGE, false,pcgAppName);
		ogBcHandle.AddTableCommand(CString("AZA") + CString(pcgTableExt), CString("UAZ"), BC_AZA_CHANGE, false,pcgAppName);
		ogBcHandle.AddTableCommand(CString("AZA") + CString(pcgTableExt), CString("DAZ"), BC_AZA_DELETE, false,pcgAppName);
	}

	// checkin counter jobs
	if(ogBasicData.bmDisplayCheckins)
	{
		ogBcHandle.AddTableCommand(CString("CCA") + CString(pcgTableExt), CString("IRT"), BC_CCA_CHANGE, true);
		ogBcHandle.AddTableCommand(CString("CCA") + CString(pcgTableExt), CString("URT"), BC_CCA_CHANGE, true);
		ogBcHandle.AddTableCommand(CString("CCA") + CString(pcgTableExt), CString("DRT"), BC_CCA_DELETE, true);
	}

	// passenger table - passenger/seat info
	ogBcHandle.AddTableCommand(CString("PAX") + CString(pcgTableExt), CString("IRT"), BC_PAX_CHANGE, false,pcgAppName);
	ogBcHandle.AddTableCommand(CString("PAX") + CString(pcgTableExt), CString("URT"), BC_PAX_CHANGE, false,pcgAppName);


	// Shift Changes
	// 1. Shift
	ogBcHandle.AddTableCommand(CString("DRR") + CString(pcgTableExt), CString("DRT"), BC_SHIFT_DELETE, true);
	ogBcHandle.AddTableCommand(CString("DRR") + CString(pcgTableExt), CString("IRT"), BC_SHIFT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("DRR") + CString(pcgTableExt), CString("URT"), BC_SHIFT_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("RELDRR"), CString("SBC"), BC_SHIFT_SBC, true);
	// 2. Delegations
	ogBcHandle.AddTableCommand(CString("DEL") + CString(pcgTableExt), CString("IRT"), BC_DEL_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("DEL") + CString(pcgTableExt), CString("URT"), BC_DEL_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("DEL") + CString(pcgTableExt), CString("DRT"), BC_DEL_DELETE, true);
	// 3. Deviations
	ogBcHandle.AddTableCommand(CString("DRD") + CString(pcgTableExt), CString("IRT"), BC_DRD_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("DRD") + CString(pcgTableExt), CString("URT"), BC_DRD_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("DRD") + CString(pcgTableExt), CString("DRT"), BC_DRD_DELETE, true);
	// 4. Temporary Absences
	ogBcHandle.AddTableCommand(CString("DRA") + CString(pcgTableExt), CString("IRT"), BC_DRA_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("DRA") + CString(pcgTableExt), CString("URT"), BC_DRA_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("DRA") + CString(pcgTableExt), CString("DRT"), BC_DRA_DELETE, true);
	// 5. Staff Working Group Deviations
	ogBcHandle.AddTableCommand(CString("DRG") + CString(pcgTableExt), CString("IRT"), BC_DRG_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("DRG") + CString(pcgTableExt), CString("URT"), BC_DRG_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("DRG") + CString(pcgTableExt), CString("DRT"), BC_DRG_DELETE, true);

	// Group Delegations
	ogBcHandle.AddTableCommand(CString("DLG") + CString(pcgTableExt), CString("IRT"), BC_DLG_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("DLG") + CString(pcgTableExt), CString("URT"), BC_DLG_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("DLG") + CString(pcgTableExt), CString("DRT"), BC_DLG_DELETE, true);

	// double-tour data (links between trainers and trainees)
	ogBcHandle.AddTableCommand(CString("DRS") + CString(pcgTableExt), CString("IRT"), BC_DRS_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("DRS") + CString(pcgTableExt), CString("URT"), BC_DRS_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("DRS") + CString(pcgTableExt), CString("DRT"), BC_DRS_DELETE, true);

	// clocking on/off times
	ogBcHandle.AddTableCommand(CString("SPR") + CString(pcgTableExt), CString("IRT"), BC_SPR_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("SPR") + CString(pcgTableExt), CString("URT"), BC_SPR_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("SPR") + CString(pcgTableExt), CString("DRT"), BC_SPR_DELETE, true);

	// rule demand
	ogBcHandle.AddTableCommand(CString("RUD") + CString(pcgTableExt), CString("IRT"), BC_RUD_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("RUD") + CString(pcgTableExt), CString("URT"), BC_RUD_CHANGE, true);

	// rule event
	ogBcHandle.AddTableCommand(CString("RUE") + CString(pcgTableExt), CString("IRT"), BC_RUE_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("RUE") + CString(pcgTableExt), CString("URT"), BC_RUE_CHANGE, true);

	// equipment attributes
	ogBcHandle.AddTableCommand(CString("EQA") + CString(pcgTableExt), CString("IRT"), BC_EQA_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("EQA") + CString(pcgTableExt), CString("URT"), BC_EQA_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("EQA") + CString(pcgTableExt), CString("DRT"), BC_EQA_DELETE, true);

	// blocking data (NAFR/NATO)
	ogBcHandle.AddTableCommand(CString("BLK") + CString(pcgTableExt), CString("IRT"), BC_BLK_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("BLK") + CString(pcgTableExt), CString("URT"), BC_BLK_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("BLK") + CString(pcgTableExt), CString("DRT"), BC_BLK_DELETE, true);

	// remarks
	ogBcHandle.AddTableCommand(CString("REM") + CString(pcgTableExt), CString("IRT"), BC_REM_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("REM") + CString(pcgTableExt), CString("URT"), BC_REM_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("REM") + CString(pcgTableExt), CString("DRT"), BC_REM_DELETE, true);

	// demand functions
	ogBcHandle.AddTableCommand(CString("RPF") + CString(pcgTableExt), CString("IRT"), BC_RPF_INSERT, true);
	ogBcHandle.AddTableCommand(CString("RPF") + CString(pcgTableExt), CString("URT"), BC_RPF_UPDATE, true);
	ogBcHandle.AddTableCommand(CString("RPF") + CString(pcgTableExt), CString("DRT"), BC_RPF_DELETE, true);

	// demand equipment
	ogBcHandle.AddTableCommand(CString("REQ") + CString(pcgTableExt), CString("IRT"), BC_REQ_INSERT, true);
	ogBcHandle.AddTableCommand(CString("REQ") + CString(pcgTableExt), CString("URT"), BC_REQ_UPDATE, true);
	ogBcHandle.AddTableCommand(CString("REQ") + CString(pcgTableExt), CString("DRT"), BC_REQ_DELETE, true);

	// basic functions
	ogBcHandle.AddTableCommand(CString("PFC") + CString(pcgTableExt), CString("IRT"), BC_PFC_INSERT, true);
	ogBcHandle.AddTableCommand(CString("PFC") + CString(pcgTableExt), CString("URT"), BC_PFC_UPDATE, true);
	ogBcHandle.AddTableCommand(CString("PFC") + CString(pcgTableExt), CString("DRT"), BC_PFC_DELETE, true);

	// basic permits
	ogBcHandle.AddTableCommand(CString("PER") + CString(pcgTableExt), CString("IRT"), BC_PER_INSERT, true);
	ogBcHandle.AddTableCommand(CString("PER") + CString(pcgTableExt), CString("URT"), BC_PER_UPDATE, true);
	ogBcHandle.AddTableCommand(CString("PER") + CString(pcgTableExt), CString("DRT"), BC_PER_DELETE, true);

	ogBcHandle.AddTableCommand(CString("RPQ") + CString(pcgTableExt), CString("IRT"), BC_RPQ_INSERT, true);
	ogBcHandle.AddTableCommand(CString("RPQ") + CString(pcgTableExt), CString("URT"), BC_RPQ_UPDATE, true);
	ogBcHandle.AddTableCommand(CString("RPQ") + CString(pcgTableExt), CString("DRT"), BC_RPQ_DELETE, true);

	ogBcHandle.AddTableCommand(CString("SGM") + CString(pcgTableExt), CString("IRT"), BC_SGM_INSERT, true);
	ogBcHandle.AddTableCommand(CString("SGM") + CString(pcgTableExt), CString("URT"), BC_SGM_UPDATE, true);
	ogBcHandle.AddTableCommand(CString("SGM") + CString(pcgTableExt), CString("DRT"), BC_SGM_DELETE, true);

	ogBcHandle.AddTableCommand(CString("SGR") + CString(pcgTableExt), CString("IRT"), BC_SGR_INSERT, true);
	ogBcHandle.AddTableCommand(CString("SGR") + CString(pcgTableExt), CString("URT"), BC_SGR_UPDATE, true);
	ogBcHandle.AddTableCommand(CString("SGR") + CString(pcgTableExt), CString("DRT"), BC_SGR_DELETE, true);

	ogBcHandle.AddTableCommand(CString("EQU") + CString(pcgTableExt), CString("IRT"), BC_EQU_INSERT, true);
	ogBcHandle.AddTableCommand(CString("EQU") + CString(pcgTableExt), CString("URT"), BC_EQU_UPDATE, true);
	ogBcHandle.AddTableCommand(CString("EQU") + CString(pcgTableExt), CString("DRT"), BC_EQU_DELETE, true);

	ogBcHandle.AddTableCommand(CString("VAL") + CString(pcgTableExt), CString("IRT"), BC_VAL_INSERT, true);
	ogBcHandle.AddTableCommand(CString("VAL") + CString(pcgTableExt), CString("URT"), BC_VAL_UPDATE, true);
	ogBcHandle.AddTableCommand(CString("VAL") + CString(pcgTableExt), CString("DRT"), BC_VAL_DELETE, true);

	ogBcHandle.AddTableCommand(CString("SWG") + CString(pcgTableExt), CString("IRT"), BC_SWG_INSERT, true);
	ogBcHandle.AddTableCommand(CString("SWG") + CString(pcgTableExt), CString("URT"), BC_SWG_UPDATE, true);
	ogBcHandle.AddTableCommand(CString("SWG") + CString(pcgTableExt), CString("DRT"), BC_SWG_DELETE, true);

	ogBcHandle.AddTableCommand(CString("LOA") + CString(pcgTableExt), CString("IRT"), BC_LOA_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("LOA") + CString(pcgTableExt), CString("URT"), BC_LOA_CHANGE, true);
	ogBcHandle.AddTableCommand(CString("LOA") + CString(pcgTableExt), CString("DRT"), BC_LOA_DELETE, true);

	// jobhdl has finished auto allocation
	ogBcHandle.AddTableCommand(CString("AFLEND"), CString("SBC"), BC_AFLEND_SBC, true);

	CString olWorkstation = ogCommHandler.pcmReqId;


#if	0
	CLoginDlg olLoginDlg(pcgTableExt,pcgAppName,olWorkstation);	// HomeAirport,ApplName,WkstName

	// this program is standalone so allow the user to login
	bool blRc = true;
	if( olLoginDlg.DoModal() == IDOK )
	{
		ogUsername = olLoginDlg.omUsername;

		if(ogPrivList.GetStat("InitModu") == '1')
		{
			RegisterDlg olRegisterDlg;
			if( olRegisterDlg.DoModal() != IDOK )
				blRc = false;
			else
				blRc = true;
		}
		else
		{
			blRc = true;
		}
	}
	else
	{
		// invalid login or cancel pressed
		blRc = false;
	}

	if(!blRc)
	{
		CleanUp();
        return FALSE;
	}

#else
	CDialog olDummyDlg;	
	olDummyDlg.Create(IDD_WAIT_DIALOG,NULL);
	//olDummyDlg.ShowWindow(SW_HIDE);
	CAatLogin olLoginCtrl;
	olLoginCtrl.Create(NULL,WS_CHILD|WS_DISABLED,CRect(0,0,100,100),&olDummyDlg,4711);
	olLoginCtrl.SetInfoButtonVisible(TRUE);
	olLoginCtrl.SetApplicationName(pcgAppName); 
	olLoginCtrl.SetRegisterApplicationString(ogInitModuData.GetInitModuTxt());
	if (olLoginCtrl.ShowLoginDialog() != "OK")
	{
		olDummyDlg.DestroyWindow();
		return FALSE;
	}

	ogUsername = olLoginCtrl.GetUserName_();
	ogPassword = olLoginCtrl.GetUserPassword();
	ogWks = olLoginCtrl.GetWorkstationName();

	ogCommHandler.SetUser(olLoginCtrl.GetUserName_());
	strcpy(CCSCedaData::pcmUser, olLoginCtrl.GetUserName_());
	strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);

	ogPrivList.Add(olLoginCtrl.GetPrivilegList());

	olDummyDlg.DestroyWindow();
#endif


	InitLogging();
	InitFont();

	if (bgIsPrm)
	{
		ogPrmPrivList.Login(pcgHomeAirport,ogUsername,ogPassword,"GlobalPar",ogWks);
	}


	char clStat = ogPrivList.GetStat("LOAD_TIMESPAN_DLG");

	if(clStat != '-') // not hidden
	{
		// load time spans
		bool blAllFieldsDisabled = (clStat == '1' ? false : true);
		CLoadTimeSpanDlg olLoadTimeSpanDlg(NULL,ogBasicData.omLoadTimeValues,ogBasicData.bmLoadRel,blAllFieldsDisabled);
		if(olLoadTimeSpanDlg.DoModal() != IDOK)
		{
			CleanUp();
			return FALSE;
		}

		ogBasicData.omLoadTimeValues.RemoveAll();
		for(int illc = 0; illc < olLoadTimeSpanDlg.omValues.GetSize() ;illc++)
		{
			ogBasicData.omLoadTimeValues.Add(olLoadTimeSpanDlg.omValues.GetAt(illc));
		}
		ogBasicData.bmLoadRel = (olLoadTimeSpanDlg.m_AbsTimeVal == 1) ? true : false;
		ogBasicData.SetTimeframe(olLoadTimeSpanDlg.omStartTime,olLoadTimeSpanDlg.omEndTime);
		ogBasicData.SetTemplateFilters(olLoadTimeSpanDlg.omTplUrnos);
	}
//	// uncomment the lines below for default timeframe of 3 days from 00:00 today
//	CTime olCurrTime = CTime::GetCurrentTime();
//	CTime olFrom = CTime(olCurrTime.GetYear(),olCurrTime.GetMonth(),olCurrTime.GetDay(),0,0,0);
//	CTime olTo = olFrom + CTimeSpan(3,0,0,0);
//	ogBasicData.SetTimeframe(olFrom,olTo);

	ogCfgData.omServerName = ogCommHandler.pcmRealHostName;
	ogConflicts.ReadAllConfigData();

	CreateBrushes();

	ogUndoManager.RegisterDDX();
	ogUndoManager.SetUndoSize(ogBasicData.imNumberOfUndoRedoActions);


	CMainFrame* pMainFrame = new CMainFrame;
	if (!pMainFrame->LoadFrame(IDR_MAINFRAME))
		return FALSE;
	m_pMainWnd = pMainFrame;
	pogMainWnd = pMainFrame;
//	ogCommHandler.RegisterBcWindow(pMainFrame);

	InitialLoad();

	//pMainFrame->ShowWindow(SW_SHOWMAXIMIZED);	 //Commented PRF 8712

	//PRF 8712
	CRect olRect;
	BOOL blMinimized = FALSE;
	pMainFrame->GetWindowRect(&olRect);
	if(ogBasicData.GetDialogFromReg(olRect, COpssPmApp::OPSSPM_DIALOG_WINDOWPOS_REG_KEY,blMinimized) == TRUE)
	{
		//Multiple monitor setup
		ogBasicData.GetWindowPositionCorrect(olRect);

		pMainFrame->SetWindowPos(&(CWnd::wndTop), olRect.left,olRect.top, olRect.Width(), olRect.Height(), SWP_SHOWWINDOW);
		if(blMinimized == TRUE)
		{
			pMainFrame->CloseWindow();
		}
	}
	else
	{
		pMainFrame->ShowWindow(SW_SHOWMAXIMIZED);
	}

	pMainFrame->UpdateWindow();


	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return TRUE;
}

void COpssPmApp::CleanUp(void)
{
	if (ogSysTime.wYear != 0)
	{
		SYSTEMTIME olTmpTime;

		GetSystemTime(&olTmpTime);

		ogSysTime.wHour = olTmpTime.wHour;
		ogSysTime.wMinute = olTmpTime.wMinute;
		ogSysTime.wSecond = olTmpTime.wSecond;

		SetSystemTime(&ogSysTime);
	}
 	ogCommHandler.CleanUpCom();
}

void COpssPmApp::SetLoadMessage(const char *pcpMessage, int ipProgress /*=0*/)
{
	if(pogInitialLoad != NULL)
	{
		if(strlen(pcpMessage) > 0)
		{
			pogInitialLoad->SetMessage(pcpMessage);
		}
		if(ipProgress > 0)
		{
			pogInitialLoad->SetProgress(ipProgress);
		}
	}
}


void COpssPmApp::InitialLoad(void)
{
	CCSReturnCode ret = RCSuccess;
	int count = 0;
	pogInitialLoad = new InitialLoadDlg();

	bgIsInitialized = FALSE;
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	ogRudData.bmUtplExists = ogBasicData.DoesFieldExist("RUD", "UTPL" );

	if(!ogBasicData.DontLoadTable(ogOdaData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING32156)); // "Abwesenheittypen werden geladen..."
		ogOdaData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogOdaData.ReadOdaData(); // Absence Codes
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogOdaData.ReadOdaData(); // Absence Codes
		}
		SetLoadMessage(GetNumRecsReadString(ogOdaData.omData.GetSize(),GetString(IDS_STRING32157),ogOdaData.GetTableName()),2); //"Abwesenheittypen geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogRnkData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING32158)); //"Funktion Codes werden geladen..."
		ogRnkData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogRnkData.ReadRnkData(); // Function Codes
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogRnkData.ReadRnkData(); // Function Codes
		}
		SetLoadMessage(GetNumRecsReadString(ogRnkData.omData.GetSize(),GetString(IDS_STRING32159),ogRnkData.GetTableName()),2); //"Funktion Codes geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogWgpData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING32160)); //"Mitarbeitergruppen (Teams) werden geladen..."
		ogWgpData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogWgpData.ReadWgpData(); // Employee Groups (Teams)
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogWgpData.ReadWgpData(); // Employee Groups (Teams)
		}
		SetLoadMessage(GetNumRecsReadString(ogWgpData.omData.GetSize(),GetString(IDS_STRING32161),ogWgpData.GetTableName()),2); //"Mitarbeitergruppen (Teams) geladen"
		pogInitialLoad->RedrawWindow();
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogRemData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_LOADREM));
		ogRemData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogRemData.ReadRemData(); // Remarks Table
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogRemData.ReadRemData(); // Remarks Table
		}
		SetLoadMessage(GetNumRecsReadString(ogRemData.omData.GetSize(),GetString(IDS_LOADEDREM),ogRemData.GetTableName()),2);
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogJtyData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING32162)); //"Einsatzarten werden geladen..."
		ogJtyData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogJtyData.ReadJtyData(); // Job Types
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogJtyData.ReadJtyData(); // Job Types
		}
		SetLoadMessage(GetNumRecsReadString(ogJtyData.omData.GetSize(),GetString(IDS_STRING32163),ogJtyData.GetTableName()),2); //"Einsatzarten geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogSpeData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING32168)); //"Zugeteilte Qualifikationen werden geladen..."
		ogSpeData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogSpeData.ReadSpeData(); // Emp-Permits
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogSpeData.ReadSpeData(); // Emp-Permits
		}
		SetLoadMessage(GetNumRecsReadString(ogSpeData.omData.GetSize(),GetString(IDS_STRING32169),ogSpeData.GetTableName()),2); //"Zugeteilte Qualifikationen geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogSprData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_LOADING_SPR));
		ogSprData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogSprData.ReadSprData(ogBasicData.GetTimeframeStartUtc(),ogBasicData.GetTimeframeEndUtc()); // Clocking-on/off values
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogSprData.ReadSprData(ogBasicData.GetTimeframeStartUtc(),ogBasicData.GetTimeframeEndUtc()); // Clocking-on/off values
		}
		SetLoadMessage(GetNumRecsReadString(ogSprData.omData.GetSize(),GetString(IDS_LOADED_SPR),ogSprData.GetTableName()),2);
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogSpfData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING32164)); //"Mitarbeiter Funktionen werden geladen..."
		ogSpfData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogSpfData.ReadSpfData(); // Emp-Functions
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogSpfData.ReadSpfData(); // Emp-Functions
		}
		SetLoadMessage(GetNumRecsReadString(ogSpfData.omData.GetSize(),GetString(IDS_STRING32165),ogSpfData.GetTableName()),2); //"Mitarbeiter Funktionen geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogSwgData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING32170)); //"Mitarbeiter Gruppen werden geladen..."
		ogSwgData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogSwgData.ReadSwgData(); // Staff Working Groups
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogSwgData.ReadSwgData(); // Staff Working Groups
		}
		SetLoadMessage(GetNumRecsReadString(ogSwgData.omData.GetSize(),GetString(IDS_STRING32171),ogSwgData.GetTableName()),2); //"Mitarbeiter Qualifikationen geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogDrgData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_LOAD_DRG1)); //"Mitarbeiter Gruppen werden geladen..."
		ogDrgData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogDrgData.ReadDrgData(); // Staff Working Group Deviations
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogDrgData.ReadDrgData(); // Staff Working Group Deviations
		}
		SetLoadMessage(GetNumRecsReadString(ogDrgData.omData.GetSize(),GetString(IDS_LOAD_DRG2),ogDrgData.GetTableName()),2); //"Mitarbeiter Qualifikationen geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogDlgData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_LOAD_DLG1)); //"Mitarbeiter Gruppen werden geladen..."
		ogDlgData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogDlgData.ReadDlgData(); // Staff Working Group Deviations
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogDlgData.ReadDlgData(); // Staff Working Group Deviations
		}
		SetLoadMessage(GetNumRecsReadString(ogDlgData.omData.GetSize(),GetString(IDS_LOAD_DLG2),ogDlgData.GetTableName()),2); //"Mitarbeiter Qualifikationen geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogPgpData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_LOAD_PGP1)); //"Mitarbeiter Gruppen werden geladen..."
		ogPgpData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogPgpData.ReadPgpData(); // Planning Groups
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogPgpData.ReadPgpData(); // Planning Groups
		}
		SetLoadMessage(GetNumRecsReadString(ogPgpData.omData.GetSize(),GetString(IDS_LOAD_PGP2),ogPgpData.GetTableName()),2); //"Mitarbeiter Qualifikationen geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogPfcData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING32174)); //"Funktionen (Stammdaten) werden geladen..."
		ogPfcData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogPfcData.ReadPfcData(); // Function Stammdaten
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogPfcData.ReadPfcData(); // Function Stammdaten
		}
		SetLoadMessage(GetNumRecsReadString(ogPfcData.omData.GetSize(),GetString(IDS_STRING32175),ogPfcData.GetTableName()),2); //"Funktionen geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogPerData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING32176)); //"Qualifikationen (Stammdaten) werden geladen..."
		ogPerData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogPerData.ReadPerData(); // Permit Stammdaten
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogPerData.ReadPerData(); // Permit Stammdaten
		}
		SetLoadMessage(GetNumRecsReadString(ogPerData.omData.GetSize(),GetString(IDS_STRING32177),ogPerData.GetTableName()),2); //"Qualifikationen (Stammdaten) geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogDrdData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING32178)); //"Schichtwechsel werden geladen..."
		ogDrdData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogDrdData.ReadDrdData(); // Shift deviations
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogDrdData.ReadDrdData(); // Shift deviations
		}
		SetLoadMessage(GetNumRecsReadString(ogDrdData.omData.GetSize(),GetString(IDS_STRING32179),ogDrdData.GetTableName()),2); //"Schichtwechsel geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogDrsData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_LOADDRS1));
		ogDrsData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogDrsData.ReadDrsData(); // Double tour data
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogDrsData.ReadDrsData(); // Double tour data
		}
		SetLoadMessage(GetNumRecsReadString(ogDrsData.omData.GetSize(),GetString(IDS_LOADDRS2),ogDrsData.GetTableName()),2);
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogDelData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING32180)); //"Basisschichten werden geladen..."
		ogDelData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogDelData.ReadDelData(); // Basis Shift delegations
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogDelData.ReadDelData(); // Basis Shift delegations
		}
		SetLoadMessage(GetNumRecsReadString(ogDelData.omData.GetSize(),GetString(IDS_STRING32181),ogDelData.GetTableName()),2); //"Basisschichten geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogDraData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_TEMPABS1)); //"temporary absences werden geladen..."
		ogDraData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogDraData.ReadDraData(); // temporary absences
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogDraData.ReadDraData(); // temporary absences
		}
		SetLoadMessage(GetNumRecsReadString(ogDraData.omData.GetSize(),GetString(IDS_TEMPABS2),ogDraData.GetTableName()),2); //"temporary absences geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogAzaData.GetTableName()))
	{
		if(ogBasicData.bmDisplayRegistrations)
		{
			SetLoadMessage(GetString(IDS_STRING32184)); //"Eingef�hrte Bedarfe werden geladen..."
			ogAzaData.omServerName = ogCommHandler.pcmRealHostName;
			ret = ogAzaData.ReadAzaData(ogBasicData.GetTimeframeStartUtc(),ogBasicData.GetTimeframeEndUtc());
			count = 0;
			while (ret != RCSuccess && count <=3)
			{
				count++;
				ret = ogAzaData.ReadAzaData(ogBasicData.GetTimeframeStartUtc(),ogBasicData.GetTimeframeEndUtc());
			}
			SetLoadMessage(GetNumRecsReadString(ogAzaData.omData.GetSize(),GetString(IDS_STRING32185),ogAzaData.GetTableName()),3); //"Eingef�hrte Bedarfe geladen"
		}
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogPaxData.GetTableName()))
	{
		//SetLoadMessage(GetString(IDS_LOA_STRING1)); //"Loading data werden geladen..."
		SetLoadMessage(GetString(IDS_PAX_STRING1)); //"Passenger data werden geladen..."
		ogPaxData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogPaxData.ReadPaxData();
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogPaxData.ReadPaxData();
		}
		//SetLoadMessage(GetNumRecsReadString(ogLoaData.omData.GetSize(),GetString(IDS_LOA_STRING2),ogLoaData.GetTableName()),10); //"Loading data geladen"
		SetLoadMessage(GetNumRecsReadString(ogPaxData.omData.GetSize(),GetString(IDS_PAX_STRING2),ogPaxData.GetTableName()),3); //"Passenger data geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogSerData.GetTableName()))
	{
		ogSerData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogSerData.ReadSerData();
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogSerData.ReadSerData();
		}
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogTplData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING32214)); //"Regeln Templates werden geladen..."
		ogTplData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogTplData.ReadTplData(); // Rule templates
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogTplData.ReadTplData(); // Rule templates
		}
		SetLoadMessage(GetNumRecsReadString(ogTplData.omData.GetSize(),GetString(IDS_STRING32215),ogTplData.GetTableName()),2); //"Regeln Templates geladen"
	}
	CDWordArray olTplUrnos;
	ogBasicData.GetTemplateFilters(olTplUrnos);

	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogRpfData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING32166)); //"Funktionen-Bedarfe werden geladen..."
		ogRpfData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogRpfData.ReadRpfData(olTplUrnos); // Demand-Functions
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogRpfData.ReadRpfData(olTplUrnos); // Demand-Functions
		}
		SetLoadMessage(GetNumRecsReadString(ogRpfData.omData.GetSize(),GetString(IDS_STRING32167),ogRpfData.GetTableName()),2); //"Funktionen-Bedarfe geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogRpqData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING32172)); //"Qualifikationen-Bedarfe werden geladen..."
		ogRpqData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogRpqData.ReadRpqData(olTplUrnos); // Demand-Permits
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogRpqData.ReadRpqData(olTplUrnos); // Demand-Permits
		}
		SetLoadMessage(GetNumRecsReadString(ogRpqData.omData.GetSize(),GetString(IDS_STRING32173),ogRpqData.GetTableName()),2); //"Qualifikationen-Bedarfe geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogReqData.GetTableName()))
	{
		if(ogBasicData.bmDisplayEquipment)
		{
			SetLoadMessage(GetString(IDS_LOADREQ1)); //"Reqipment werden geladen..."
			ogReqData.omServerName = ogCommHandler.pcmRealHostName;
			ret = ogReqData.ReadReqData(olTplUrnos);
			count = 0;
			while (ret != RCSuccess && count <=3)
			{
				count++;
				ret = ogReqData.ReadReqData(olTplUrnos);
			}
			SetLoadMessage(GetNumRecsReadString(ogReqData.omData.GetSize(),GetString(IDS_LOADREQ2),ogReqData.GetTableName()),2); //"Reqipment geladen"
		}
	}

	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogRueData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING32190)); //"Rule Events werden geladen..."
		ogRueData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogRueData.ReadRueData(olTplUrnos); // Rule Events
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogRueData.ReadRueData(olTplUrnos); // Rule Events
		}
		SetLoadMessage(GetNumRecsReadString(ogRueData.omData.GetSize(),GetString(IDS_STRING32191),ogRueData.GetTableName()),2); //"Rule Events geladen"
	}

	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogRudData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING32188)); //"Bedarferegeln werden geladen..."
		ogRudData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogRudData.ReadRudData(olTplUrnos); // Rule Demands
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogRudData.ReadRudData(olTplUrnos); // Rule Demands
		}
		SetLoadMessage(GetNumRecsReadString(ogRudData.omData.GetSize(),GetString(IDS_STRING32189),ogRudData.GetTableName()),2); //"Bedarferegeln geladen"
	}

	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogAltData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_LOADALT1)); //"Loading Airline Types..."
		ogAltData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogAltData.ReadAltData(); // Airline Types
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogAltData.ReadAltData(); // Airline Types
		}
		SetLoadMessage(GetNumRecsReadString(ogAltData.omData.GetSize(),GetString(IDS_LOADALT2),ogAltData.GetTableName()),2); //"Airline Types loaded"
	}

	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogHagData.GetTableName()))
	{
		if (ogBasicData.HandlingAgentsSupported() || ogBasicData.IsPrmHandlingAgentEnabled())
		{
			SetLoadMessage(GetString(IDS_LOADHAG1)); //"Loading handling agents..."
			ogHagData.omServerName = ogCommHandler.pcmRealHostName;
			ret = ogHagData.ReadHagData(); // Handling agents
			count = 0;
			while (ret != RCSuccess && count <=3)
			{
				count++;
				ret = ogHagData.ReadHagData(); // Handling agents
			}
			SetLoadMessage(GetNumRecsReadString(ogHagData.GetCountOfRecords(),GetString(IDS_LOADHAG2),ogHagData.GetTableName()),2); //"Handling agents loaded"
		}
	}
	if (ogBasicData.IsPrmHandlingAgentEnabled())
	{
		ret = ogHaiData.ReadHaiData();
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogHaiData.ReadHaiData();
		}
	}
	//Singapore
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogFlightData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING61668)); //"Fl�ge werden geladen..."
		ogFlightData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogFlightData.ReadAllFlights(ogBasicData.GetTimeframeStartUtc(),ogBasicData.GetTimeframeEndUtc());
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogFlightData.ReadAllFlights(ogBasicData.GetTimeframeStartUtc(),ogBasicData.GetTimeframeEndUtc());
		}
		SetLoadMessage(GetNumRecsReadString(ogFlightData.omData.GetSize(),GetString(IDS_STRING32187),ogFlightData.GetTableName()),15); //"Fl�ge geladen"
	}

	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogDemandData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING61669)); //"Bedarfe werden geladen..."
		ogDemandData.omServerName = ogCommHandler.pcmRealHostName;
		ogDemandData.GetLoadFormat();
		ret = ogDemandData.ReadAllDemands(ogBasicData.GetTimeframeStartUtc(),ogBasicData.GetTimeframeEndUtc(),olTplUrnos);
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogDemandData.ReadAllDemands(ogBasicData.GetTimeframeStartUtc(),ogBasicData.GetTimeframeEndUtc(),olTplUrnos);
		}
		SetLoadMessage(GetNumRecsReadString(ogDemandData.omData.GetSize(),GetString(IDS_STRING32193),ogDemandData.GetTableName()),2); //"Bedarfe geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogCcaData.GetTableName()))
	{
		if(ogBasicData.bmDisplayCheckins)
		{
			// Checkin counter jobs
			if (ogCfgData.rmUserSetup.OPTIONS.CCA != USERSETUPDATA::NONE)
			{
				SetLoadMessage(GetString(IDS_STRING32182)); //"Check-in counter jobs werden geladen..."
				ogCcaData.omServerName = ogCommHandler.pcmRealHostName;
				ret = ogCcaData.ReadCcaData(ogBasicData.GetTimeframeStartUtc(),ogBasicData.GetTimeframeEndUtc());
				count = 0;
				while (ret != RCSuccess && count <=3)
				{
					count++;
					ret = ogCcaData.ReadCcaData(ogBasicData.GetTimeframeStartUtc(),ogBasicData.GetTimeframeEndUtc());
				}
//$$			ogDemandData.LinkCheckinDemands(); // link location and personnel demands for checkins
				SetLoadMessage(GetNumRecsReadString(ogCcaData.omData.GetSize(),GetString(IDS_STRING32183),ogCcaData.GetTableName()),2); //"Check-in counter jobs geladen"
			}
		}
	}

	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogBlkData.GetTableName()))
	{
//$$		SetLoadMessage(GetString(IDS_STRING61669)); //"B werden geladen..."
		ogBlkData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogBlkData.ReadBlkData(ogBasicData.GetTimeframeStartUtc(),ogBasicData.GetTimeframeEndUtc());
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogBlkData.ReadBlkData(ogBasicData.GetTimeframeStartUtc(),ogBasicData.GetTimeframeEndUtc());
		}
//$$		SetLoadMessage(GetNumRecsReadString(ogDemandData.omData.GetSize(),GetString(IDS_STRING32193),ogDemandData.GetTableName()),3); //" geladen"
	}

	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogBltData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_LOADBLT)); //"Baggage Belt Stammdaten werden geladen..."
		ogBltData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogBltData.ReadBltData();
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogBltData.ReadBltData();
		}
		SetLoadMessage(GetNumRecsReadString(ogBltData.omData.GetSize(),GetString(IDS_LOADEDBLT),ogBltData.GetTableName()),2); //"Baggage Stammdaten geladen"
	}

	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogNatData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING633)); //"Loading flight natures..."
		ogNatData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogNatData.ReadNatData(); // flight natures
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogNatData.ReadNatData(); // flight natures
		}
		SetLoadMessage(GetNumRecsReadString(ogNatData.GetCountOfRecords(),GetString(IDS_STRING634),ogNatData.GetTableName()),2); //"Flight natures loaded"
	}

	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogEmpData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING61670)); //"Mitarbeiter Stammdaten werden geladen..."
		ogEmpData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogEmpData.ReadEmpData();
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogEmpData.ReadEmpData();
		}
		SetLoadMessage(GetNumRecsReadString(ogEmpData.omData.GetSize(),GetString(IDS_STRING32195),ogEmpData.GetTableName()),2); //"Mitarbeiter Stammdaten geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogCfgData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING32196)); //"Ansichten Konfigurationen werden geladen..."
		ogCfgData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogCfgData.ReadCfgData(); // config data ir "Views"
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogCfgData.ReadCfgData(); // config data ir "Views"
		}
		SetLoadMessage(GetNumRecsReadString(ogCfgData.omData.GetSize(),GetString(IDS_STRING32197),ogCfgData.GetTableName()),2); //"Ansichten Konfigurationen geladen"

		ogOldSearchValues.LoadOldSearchValuesFromDb();
	}

	ogCfgData.SetCfgData();		


	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogAloData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING32198)); //"Allocation Units werden geladen..."
		ogAloData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogAloData.ReadAloData(); // Allocation units
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogAloData.ReadAloData(); // Allocation units
		}
		SetLoadMessage(GetNumRecsReadString(ogAloData.omData.GetSize(),GetString(IDS_STRING32199),ogAloData.GetTableName()),2); //"Allocation Units geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogValData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING32200)); //"Daten werden geladen..."
		ogValData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogValData.ReadValData(); // Dates
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogValData.ReadValData(); // Dates
		}
		SetLoadMessage(GetNumRecsReadString(ogValData.omData.GetSize(),GetString(IDS_STRING32201),ogValData.GetTableName()),2); //"Daten geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogGatData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING32202)); //"Gates werden geladen..."
		ogGatData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogGatData.ReadGatData(); // Gates
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogGatData.ReadGatData(); // Gates
		}
		SetLoadMessage(GetNumRecsReadString(ogGatData.omData.GetSize(),GetString(IDS_STRING32203),ogGatData.GetTableName()),2); //"Gates geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogEquData.GetTableName()))
	{
		if(ogBasicData.bmDisplayEquipment)
		{
			SetLoadMessage(GetString(IDS_LOADEQU1)); //"Equipment werden geladen..."
			ogEquData.omServerName = ogCommHandler.pcmRealHostName;
			ret = ogEquData.ReadEquData();
			count = 0;
			while (ret != RCSuccess && count <=3)
			{
				count++;
				ret = ogEquData.ReadEquData();
			}
			SetLoadMessage(GetNumRecsReadString(ogEquData.omData.GetSize(),GetString(IDS_LOADEQU2),ogEquData.GetTableName()),2); //"Equipment geladen"
		}
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogEqtData.GetTableName()))
	{
		if(ogBasicData.bmDisplayEquipment)
		{
			SetLoadMessage(GetString(IDS_LOADEQT1)); //"Equipment Types geladen..."
			ogEqtData.omServerName = ogCommHandler.pcmRealHostName;
			ret = ogEqtData.ReadEqtData();
			count = 0;
			while (ret != RCSuccess && count <=3)
			{
				count++;
				ret = ogEqtData.ReadEqtData();
			}
			SetLoadMessage(GetNumRecsReadString(ogEqtData.omData.GetSize(),GetString(IDS_LOADEQT2),ogEqtData.GetTableName()),2); //"Equipment Types geladen"
		}
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogEqaData.GetTableName()))
	{
		if(ogBasicData.bmDisplayEquipment)
		{
			SetLoadMessage(GetString(IDS_LOADEQA1)); //"Equipment Attributes werden geladen..."
			ogEqaData.omServerName = ogCommHandler.pcmRealHostName;
			ret = ogEqaData.ReadEqaData();
			count = 0;
			while (ret != RCSuccess && count <=3)
			{
				count++;
				ret = ogEqaData.ReadEqaData();
			}
			SetLoadMessage(GetNumRecsReadString(ogEqaData.omData.GetSize(),GetString(IDS_LOADEQA2),ogEqaData.GetTableName()),2); //"Equipment Attributes geladen"
		}
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogPstData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_LOADPST1)); //"Positions werden geladen..."
		ogPstData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogPstData.ReadPstData(); // Psts
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogPstData.ReadPstData(); // Psts
		}
		SetLoadMessage(GetNumRecsReadString(ogPstData.omData.GetSize(),GetString(IDS_LOADPST2),ogPstData.GetTableName()),2); //"Positions geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogPolData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING32204)); //"Pools werden geladen..."
		ogPolData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogPolData.ReadPolData(); // Pools
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogPolData.ReadPolData(); // Pools
		}
		SetLoadMessage(GetNumRecsReadString(ogPolData.omData.GetSize(),GetString(IDS_STRING32205),ogPolData.GetTableName()),2); //"Pools geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogCicData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING32206)); //"Check-in Counters werden geladen..."
		ogCicData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogCicData.ReadCicData(); // Checkin Counters
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogCicData.ReadCicData(); // Checkin Counters
		}
		SetLoadMessage(GetNumRecsReadString(ogCicData.omData.GetSize(),GetString(IDS_STRING32207),ogCicData.GetTableName()),2); //"Check-in Counters geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogSgmData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING32208)); //"Gruppen Mitglieder werden geladen..."
		ogSgmData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogSgmData.ReadSgmData(); // Group members (eg Gates in a gate group)
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogSgmData.ReadSgmData(); // Group members (eg Gates in a gate group)
		}
		SetLoadMessage(GetNumRecsReadString(ogSgmData.omData.GetSize(),GetString(IDS_STRING32209),ogSgmData.GetTableName()),2); //"Gruppen Mitglieder geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogSgrData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING32210)); //"Gruppen werden geladen..."
		ogSgrData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogSgrData.ReadSgrData(); // Groups (eg Gate Groups)
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogSgrData.ReadSgrData(); // Groups (eg Gate Groups)
		}
		SetLoadMessage(GetNumRecsReadString(ogSgrData.omData.GetSize(),GetString(IDS_STRING32211),ogSgrData.GetTableName()),2); //"Gruppen geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogAcrData.GetTableName()))
	{
		if(ogBasicData.bmDisplayRegistrations)
		{
			SetLoadMessage(GetString(IDS_STRING32212)); //"Registrierungen werden geladen..."
			ogAcrData.omServerName = ogCommHandler.pcmRealHostName;
			ret = ogAcrData.ReadAcrData(); // Registrations
			count = 0;
			while (ret != RCSuccess && count <=3)
			{
				count++;
				ret = ogAcrData.ReadAcrData(); // Registrations
			}
			SetLoadMessage(GetNumRecsReadString(ogAcrData.omData.GetSize(),GetString(IDS_STRING32213),ogAcrData.GetTableName()),2); //"Registrierungen geladen"
		}
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogActData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_LOADACT1));
		ogActData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogActData.ReadActData();
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogActData.ReadActData();
		}
		SetLoadMessage(GetNumRecsReadString(ogActData.omData.GetSize(),GetString(IDS_LOADACT2),ogActData.GetTableName()),2); //"Registrierungen geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogAptData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_LOADAPT1)); //"Loading Airports..."
		ogAptData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogAptData.ReadAptData(); // Airports
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogAptData.ReadAptData(); // Airports
		}
		SetLoadMessage(GetNumRecsReadString(ogAptData.omData.GetSize(),GetString(IDS_LOADAPT2),ogAptData.GetTableName()),2); //"Airline Types loaded"
	}

	ogAllocData.CreateAllocMap();
	ogBasicData.CreateListOfManagers();


	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogShiftTypes.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING32216)); //"Schicht Typen werden geladen..."
		ogShiftTypes.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogShiftTypes.ReadShiftTypeData();
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogShiftTypes.ReadShiftTypeData();
		}
		SetLoadMessage(GetNumRecsReadString(ogShiftTypes.omData.GetSize(),GetString(IDS_STRING32217),ogShiftTypes.GetTableName()),2); //"Schicht Typen geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogShiftData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING61673)); //"Schichten werden geladen..."
		ogShiftData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogShiftData.ReadAllShifts(ogBasicData.GetTimeframeStart(),ogBasicData.GetTimeframeEnd());
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogShiftData.ReadAllShifts(ogBasicData.GetTimeframeStart(),ogBasicData.GetTimeframeEnd());
		}
		SetLoadMessage(GetNumRecsReadString(ogShiftData.imNumShiftsLoaded,GetString(IDS_STRING32219),ogShiftData.GetTableName()),2); //"Schichten geladen"
		SetLoadMessage(GetNumRecsReadString(ogShiftData.imNumAbsencesLoaded,GetString(IDS_ABSENTEMPSLOADED),ogShiftData.GetTableName()),2); //"Schichten geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogJodData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING61674)); // "Einsatzverkn�pfungen werden geladen..."
		ogJodData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogJodData.ReadAllJods(ogBasicData.GetTimeframeStartUtc(),ogBasicData.GetTimeframeEndUtc());
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogJodData.ReadAllJods(ogBasicData.GetTimeframeStartUtc(),ogBasicData.GetTimeframeEndUtc());
		}
		SetLoadMessage(GetNumRecsReadString(ogJodData.omData.GetSize(),GetString(IDS_STRING32221),ogJodData.GetTableName()),2); //"Einsatzverkn�pfungen geladen"
	}
	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogPrmData.GetTableName()))
	{
		if(ogBasicData.bmDisplayRegistrations)
		{
			SetLoadMessage(GetString(IDS_LOADPRM1)); // "Line Maintenence Auto Assignment Parameters"
			ogPrmData.omServerName = ogCommHandler.pcmRealHostName;
			ret = ogPrmData.ReadPrmData();
			count = 0;
			while (ret != RCSuccess && count <=3)
			{
				count++;
				ret = ogPrmData.ReadPrmData();
			}
			SetLoadMessage(GetNumRecsReadString(ogPrmData.omData.GetSize(),GetString(IDS_LOADPRM2),ogPrmData.GetTableName()),2); //"Einsatzverkn�pfungen geladen"
		}
	}
//	if(!ogBasicData.DontLoadTable(ogRequests.GetTableName()))
//	{
//		SetLoadMessage(GetString(IDS_STRING32222)); // "Requests werden geladen..."
//		ogRequests.ReadRequestData();
//		SetLoadMessage(GetNumRecsReadString(ogRequests.omData.GetSize(),GetString(IDS_STRING32223),ogRequests.GetTableName()),3); //"Requests geladen"
//	}
//	if(!ogBasicData.DontLoadTable(ogInfos.GetTableName()))
//	{
//		SetLoadMessage(GetString(IDS_STRING32224)); // "Infos werden geladen..."
//		ogInfos.ReadRequestData();
//		SetLoadMessage(GetNumRecsReadString(ogInfos.omData.GetSize(),GetString(IDS_STRING32225),ogInfos.GetTableName()),3); //"Infos geladen"
//	}

	CreateCMapForAllPossibleValuesOfFlightAlcd();

	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogJobData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING61671)); // "Eins�tze werden geladen..."
		ogJobData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogJobData.ReadAllJobs(ogBasicData.GetTimeframeStartUtc(),ogBasicData.GetTimeframeEndUtc());
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogJobData.ReadAllJobs(ogBasicData.GetTimeframeStartUtc(),ogBasicData.GetTimeframeEndUtc());
		}
		SetLoadMessage(GetNumRecsReadString(ogJobData.omData.GetSize(),GetString(IDS_STRING32227),ogJobData.GetTableName()),2); //"Eins�tze geladen"
	}

	if (bgIsPrm)
	{
			// PRM
		SetLoadMessage(GetString(IDS_STRING61679)); // "PRM Requests werden geladen"
		ret = ogDpxData.ReadDpxData(ogBasicData.GetTimeframeStartUtc(),ogBasicData.GetTimeframeEndUtc());
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogDpxData.ReadDpxData(ogBasicData.GetTimeframeStartUtc(),ogBasicData.GetTimeframeEndUtc());
		}
		SetLoadMessage("",25); // ""
		SetLoadMessage(GetString(IDS_STRING61679)); // "Warteraeume werden geladen"
		ret = ogWroData.ReadWroData();
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogWroData.ReadWroData();
		}
		SetLoadMessage("",25); // ""
		ret = ogPdaData.Read();
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogPdaData.Read();
		}
		SetLoadMessage("",25); // ""
	}

	ogBasicData.CreateAlocUnitToPoolMap();
//	if(ogBasicData.bmAutomaticPauseAllocation)
//	{
//		SetLoadMessage(GetString(IDS_PAUSEALLOC1)); // "Automatic Pause Allocation..."
//		ogDataSet.CreatePausesForShifts();
//		SetLoadMessage(GetString(IDS_PAUSEALLOC2),3); //"Automatic pause allocation completed"
//	}


//	SetLoadMessage(GetString(IDS_CHECKJOBS1)); // "Checking Duties"
//	ogDataSet.UpdateJobsWithFlightChanges();
//	SetLoadMessage(GetString(IDS_CHECKJOBS2),3); //"Duty check completed"


	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogCccData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_LOADCCC1)); //"Loading counter classes..."
		ogCccData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogCccData.ReadCccData(); // counter classes
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogCccData.ReadCccData(); // counter classes
		}
		SetLoadMessage(GetNumRecsReadString(ogCccData.GetCountOfRecords(),GetString(IDS_LOADCCC2),ogCccData.GetTableName()),2); //"Counter classes loaded"
	}

	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogRloData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_LOADRLO1)); //"Loading resource locations..."
		ogRloData.omServerName = ogCommHandler.pcmRealHostName;
		ret = ogRloData.ReadRloData(); // counter classes
		count = 0;
		while (ret != RCSuccess && count <=3)
		{
			count++;
			ret = ogRloData.ReadRloData(); // counter classes
		}
		SetLoadMessage(GetNumRecsReadString(ogRloData.GetCountOfRecords(),GetString(IDS_LOADRLO2),ogRloData.GetTableName()),2); //"Resource locations loaded"
	}
	/*pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogFlightData.GetTableName()))
	{
		SetLoadMessage(GetString(IDS_STRING61668)); //"Fl�ge werden geladen..."
		ogFlightData.omServerName = ogCommHandler.pcmRealHostName;
		ogFlightData.ReadAllFlights(ogBasicData.GetTimeframeStartUtc(),ogBasicData.GetTimeframeEndUtc());
		SetLoadMessage(GetNumRecsReadString(ogFlightData.omData.GetSize(),GetString(IDS_STRING32187),ogFlightData.GetTableName()),15); //"Fl�ge geladen"
	}*/

	pogInitialLoad->RedrawWindow();
	if(!ogBasicData.DontLoadTable(ogJodData.GetTableName()) &&
		!ogBasicData.DontLoadTable(ogJobData.GetTableName()) &&
		!ogBasicData.DontLoadTable(ogDemandData.GetTableName()))
	{
		// some flights may have been loaded whose jobs or demands are outside the timeframe - check for these and load them
		CDWordArray olFlightUrnos;
		ogFlightData.GetAllFlightUrnos(olFlightUrnos);
		ogDemandData.CheckForMissingFlightDemands(olFlightUrnos);
		ogJobData.CheckForMissingFlightJobs(olFlightUrnos);
		ogJodData.CheckForMissingFlightJods(ogJobData.omTmpUrnoList);

		// loop through loaded demands and jobs - if any point to flights not in memory then load the missing flights
		ogDemandData.CheckForMissingFlights();
		ogJobData.CheckForMissingFlights();

		ogJodData.ReadJodsForDelegatedFlight();
	}

//	if(!ogBasicData.DontLoadTable(ogLoaData.GetTableName()))
//	{
//		SetLoadMessage("Loading Pax data from LOATAB"/*GetString(IDS_LOADLOA1)*/); // "Line Maintenence Auto Assignment Parameters"
//		ogLoaData.omServerName = ogCommHandler.pcmRealHostName;
//		CDWordArray olFlightUrnos;
//		ogFlightData.GetFlightUrnos(olFlightUrnos);
//		ogLoaData.ReadLoaData(olFlightUrnos);
//		SetLoadMessage(GetNumRecsReadString(ogLoaData.omData.GetSize()," PAX data loaded from LOATAB"/*GetString(IDS_LOADLOA2)*/,ogLoaData.GetTableName()),2);
//	}


	ogDlgSettings.LoadSettingsFromDb("FUNCTIONCOLOURS"); // employee function colours
	ogFieldConfig.Initialize();

	ogDlgSettings.LoadSettingsFromDb("OTHERCOLOURS"); // other colours such as (Finalized PRM and Lounge)

	ogDlgSettings.LoadSettingsFromDb("DiffFuncColour"); // colour of bar indicating that the emp+demand have different functions
	ogBasicData.InitDiffFuncColour();

	ogDlgSettings.LoadSettingsFromDb("FlightJobsTable"); // functions for excel export
	ogDlgSettings.LoadSettingsFromDb("CICPRINTDLG");

	//PRF5802: Load default break buffer from DB and set in ogBasicData
	CString olValue;
	ogDlgSettings.LoadSettingsFromDb("Defaults");
	pogInitialLoad->RedrawWindow();
	if(ogDlgSettings.GetValue("Defaults", "Break Buffer", olValue))
	{
		ogBasicData.imDefaultBreakBuffer = atoi ( olValue );
	}

	SetLoadMessage(GetString(IDS_FINISHED),100); //"Finished"

	InitDefaultViews();

	// initialize viewer before checking conflicts,but after loading all data !
	pogInitialLoad->RedrawWindow();
	if (pogButtonList)
	{
		ogBcHandle.SetTrafficLightCallBack(ProcessTrafficLight);
		pogButtonList->InitializeViewer();
	}

	ogNameConfig.Initialise();

	ogConflicts.CheckAllConflicts(TRUE);

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	bgIsInitialized = TRUE;

	if(pogInitialLoad != NULL)
	{
		pogInitialLoad->DestroyWindow();
		pogInitialLoad = NULL;
	}

	ogBasicData.CheckDatabaseIntegrity();

	Beep(400,250);
}


CString COpssPmApp::GetNumRecsReadString(int ilNumRecs, CString opMsg, CString opTable)
{
	CString olRecsReadString;
	olRecsReadString.Format("%d %s (%s)",ilNumRecs,opMsg,opTable);
	ogBasicData.AddInfo(olRecsReadString);
	return olRecsReadString;
}

void COpssPmApp::InitDefaultViews()
{
	InitStaffDiagramDefaultView();
	InitGateDiagramDefaultView();
	InitEquipmentDiagramDefaultView();
	InitPstDiagramDefaultView();
	InitRegnDiagramDefaultView();
	InitCciDiagramDefaultView();
	InitStaffTableDefaultView();
	InitFlightScheduleDefaultView();
	InitCciTableDefaultView();
	InitPrmTableDefaultView();
	InitGateTableDefaultView();
	InitRequestTableDefaultView();
	InitConflictTableDefaultView();
	//InitAttentionTableDefaultView();
	InitPrePlanTableDefaultView();
	InitFlIndDemandTableDefaultView();
	InitDemandTableDefaultView();
	InitCicDemandTableDefaultView();
	InitFlightJobsTableDefaultView();
}

void COpssPmApp::SetLandscape()
{
	// Get default printer settings.
	PRINTDLG   pd;
	pd.lStructSize = (DWORD) sizeof(PRINTDLG);
	if (GetPrinterDeviceDefaults(&pd))
	{
		// Lock memory handle.
		DEVMODE FAR* pDevMode =
			(DEVMODE FAR*)::GlobalLock(m_hDevMode);
		if (pDevMode)
		{
			// Change printer settings in here.
			pDevMode->dmOrientation = DMORIENT_LANDSCAPE;

			// Unlock memory handle.
			::GlobalUnlock(m_hDevMode);
		}
	}
}

void COpssPmApp::SetPortrait()
{
	// Get default printer settings.
	PRINTDLG   pd;
	pd.lStructSize = (DWORD) sizeof(PRINTDLG);
	if (GetPrinterDeviceDefaults(&pd))
	{
		// Lock memory handle.
		DEVMODE FAR* pDevMode =
			(DEVMODE FAR*)::GlobalLock(m_hDevMode);
		if (pDevMode)
		{
			// Change printer settings in here.
			pDevMode->dmOrientation = DMORIENT_PORTRAIT;

			// Unlock memory handle.
			::GlobalUnlock(m_hDevMode);
		}
	}
}

bool COpssPmApp::HandleIsfBroadcasts(void)
{
	bool blHandleIsfBroadcasts = true;
	char pclTmpText[100];
	GetPrivateProfileString(pcgAppName, "HANDLE_ISF_BROADCASTS", "YES",pclTmpText, sizeof pclTmpText, ogBasicData.GetConfigFileName());
	if(strcmp(pclTmpText,"YES"))
	{
		blHandleIsfBroadcasts = false;
	}

	return blHandleIsfBroadcasts;
}

void COpssPmApp::SetCurrDate(void)
{
	char pclCurrentDate[142];
	GetPrivateProfileString(pcgAppName, "CURRENTDATE", "",pclCurrentDate, sizeof pclCurrentDate, ogBasicData.GetConfigFileName());
	if (*pclCurrentDate != '\0')
	{
		SYSTEMTIME olTmpTime;

		GetSystemTime(&ogSysTime);
		GetSystemTime(&olTmpTime);
		olTmpTime.wYear = atoi(&pclCurrentDate[6]);
		olTmpTime.wMonth = atoi(&pclCurrentDate[3]);
		olTmpTime.wDay = atoi(&pclCurrentDate[0]);
	
		SetSystemTime(&olTmpTime);
	}
}


void COpssPmApp::ReadCfgFile(const char *pcpThisAppl)
{
	CString olConfigFileName = ogBasicData.GetConfigFileName();

    GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "XXX", pcgHomeAirport, sizeof pcgHomeAirport, olConfigFileName);
    GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "TAB", pcgTableExt, sizeof pcgTableExt, olConfigFileName);


	GetPrivateProfileString(pcgReportName, "PROG", "NONE",pcgReportingPath, sizeof pcgReportingPath, olConfigFileName);
	GetPrivateProfileString(pcgReportName, "OPSS-PM_GENERAL","NONE",pcgMacroPrintCommon, sizeof pcgMacroPrintCommon, olConfigFileName);
	GetPrivateProfileString(pcgReportName, "OPSS-PM_WORK",	 "NONE",pcgMacroPrintWork, sizeof pcgMacroPrintWork, olConfigFileName);
	GetPrivateProfileString(pcgReportName, "OPSS-PM_BREAK",  "NONE",pcgMacroPrintBreak, sizeof pcgMacroPrintBreak, olConfigFileName);
	
	char tmpIsPrm[124];
	;
	GetPrivateProfileString(pcpThisAppl, "ISPRM",  "NO",tmpIsPrm, sizeof tmpIsPrm, olConfigFileName);
	if (!strcmp(tmpIsPrm,"YES")) {
		bgIsPrm = true;
	}
	else {
 		bgIsPrm = false;
	}
	GetPrivateProfileString(pcpThisAppl, "PRM-SHOWCLOS",  "NO",tmpIsPrm, sizeof tmpIsPrm, olConfigFileName);
	if (!strcmp(tmpIsPrm,"YES")) {
		bgShowPrmClos = true;
	}
	else {
 		bgShowPrmClos = false;
	}
	GetPrivateProfileString(pcpThisAppl, "PRM-SHOWASSIGNMENTS",  "NO",tmpIsPrm, sizeof tmpIsPrm, olConfigFileName);
	if (!strcmp(tmpIsPrm,"YES")) {
		bgPrmShowAssignment = true;
	}
	else {
 		bgPrmShowAssignment = false;
	}
	GetPrivateProfileString(pcpThisAppl, "PRM-SHOWSTASTD",  "NO",tmpIsPrm, sizeof tmpIsPrm, olConfigFileName);
	if (!strcmp(tmpIsPrm,"YES")) {
		bgPrmShowStaStd = true;
	}
	else {
 		bgPrmShowStaStd = false;
	}


} // end ReadCfgFile


void COpssPmApp::WinHelp(DWORD dwData, UINT nCmd) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	AatHelp::WinHelp(dwData, nCmd);
}

BOOL COpssPmApp::IsAlreadyRunning()
{
	if (pomMutex)
		return !pomMutex->Lock(0);
	else
		return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// InitialLoad dialog


InitialLoadDlg::InitialLoadDlg(CWnd* pParent /*=NULL*/)
	: CDialog(InitialLoadDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(InitialLoad)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	Create(IDD,pParent);
	ShowWindow(SW_SHOWNORMAL);
	m_Progress.SetRange(0,160);
;
}


InitialLoadDlg::~InitialLoadDlg(void)
{
	pogInitialLoad = NULL;
}

void InitialLoadDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(InitialLoadDlg)
	DDX_Control(pDX, IDC_PROGRESS1, m_Progress);
	DDX_Control(pDX, IDC_MSGLIST, m_MsgList);
	//}}AFX_DATA_MAP
}

BOOL InitialLoadDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	CString olText;
	olText = GetString(IDS_STRING32988) + CString(" ") + ogBasicData.GetTimeframeString();
	SetWindowText(olText); 
	return TRUE;
}

void InitialLoadDlg::SetProgress(int ipProgress)
{
	m_Progress.OffsetPos(ipProgress);
}

void InitialLoadDlg::SetMessage(CString opMessage)
{
	m_MsgList.AddString(opMessage);
	if ((m_MsgList.GetCount()-12) > 0)
		m_MsgList.SetTopIndex(m_MsgList.GetCount()-12);
	pogInitialLoad->UpdateWindow();
}

BOOL InitialLoadDlg::DestroyWindow() 
{
	BOOL blRc = CDialog::DestroyWindow();
	delete this;
	return blRc;
}

BEGIN_MESSAGE_MAP(InitialLoadDlg, CDialog)
	//{{AFX_MSG_MAP(InitialLoadDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// InitialLoadDlg message handlers

int COpssPmApp::ExitInstance() 
{
	ogBcHandle.ExitApp();
	
	return CWinApp::ExitInstance();
}
