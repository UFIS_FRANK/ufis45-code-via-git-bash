// FlightDetailViewer.h : header file
//

#ifndef _FLIGHTDV_H_
#define _FLIGHTDV_H_

#include <CedaAcrData.h>
#include <CedaJodData.h>
#include <WorkGroupBackgroundBars.h>


#define FlightData	FLIGHTDATA
#define PYELLOW RGB(255, 255, 180)			// pastel yellow
#define PGREEN  RGB(190, 255, 190)			// pastel green
#define PBLUE   RGB(157, 255, 255)			// pastel blue
#define PRED	RGB(255, 203, 174)			// pastel red
#define PPINK	RGB(253, 202, 255)			// pastel pink

struct FLIGHT_BARDATA 
{
	long FlightUrno;
	long DepartureUrno;
	long Urno;
    CString Text;
	CString StatusText;
    CTime StartTime;
    CTime EndTime;
    int FrameType;
	COLORREF FrameColor;
    int MarkerType;
    CBrush *MarkerBrush;
    int OverlapLevel;       // zero for the top-most level, each level lowered by 4 pixels
	bool IsTeamBar;			// true if this is a background bar used to display a team (grouped demands)
	bool Infm;				// true if the emp has been informed of a job but the job has not yet started -> display lefthand black dot on the bar
	bool Selected;
	bool OfflineBar;
	bool DifferentFunction;
	bool DelegatedFlight;

	FLIGHT_BARDATA(void)
	{
		FrameColor = BLACK;
		IsTeamBar = false;
		Infm = false;
		Selected = false;
		OfflineBar = false;
		DifferentFunction = false;
		DelegatedFlight = false;
	}
};

struct FLIGHT_LINEDATA
{
	// standard data for lines in general Diagram's chart
    CString Text1;
    CString Text2;
	CString ToolTip;
    int MaxOverlapLevel;    // maximum number of overlapped level of bars in this line
	bool FillerLine;
	CCSPtrArray <FLIGHT_BARDATA> DemandBars;
    CCSPtrArray <FLIGHT_BARDATA> JobBars;

	FLIGHT_LINEDATA(void)
	{
		FillerLine = false;
	}
};

class FlightDetailViewer: public CViewer
{
public:
    FlightDetailViewer();
    ~FlightDetailViewer();


	void Init(void);
	//
	void GetDemands(CCSPtrArray <DEMANDDATA> &ropDemands);
	void GetJobsWithoutDemands(CCSPtrArray <JOBDATA> &ropJobsWithoutDemands);
	void GetRegnDemands(CCSPtrArray <DEMANDDATA> &ropDemands);
	int GetUpperBound();


	void Attach(CWnd *popAttachWnd);

	
	// Line
    int GetLineCount(int ipGroupno);
    FLIGHT_LINEDATA *GetLine(int ipGroupno, int ipLineno);
    CString GetLineText(int ipGroupno, int ipLineno);

	// ForeGround Bar
    int CreateBar(int ipGroupno, int ipLineno, FLIGHT_BARDATA *prpBar, BOOL bpFrontBar = TRUE);
    void DeleteBar(int ipGroupno, int ipLineno, int ipBarno);
    int GetBarCount(int ipGroupno, int ipLineno);
    FLIGHT_BARDATA *GetBar(int ipGroupno, int ipLineno, int ipBarno);
    int GetBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2, int ipOverlapLevel1, int ipOverlapLevel2);

	// BackGround Bar
    int GetBkBarCount(int ipGroupno, int ipLineno);
    FLIGHT_BARDATA *GetBkBar(int ipGroupno, int ipLineno, int ipBarno);
	int GetBkBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2, int ipOverlapLevel1, int ipOverlapLevel2);
	
	void ProcessJodChange(JODDATA *prpJod);
	void ProcessJobChange(JOBDATA *prpJob);
	void ProcessJobDelete(JOBDATA *prpJob);
	void ProcessDemandChange(DEMANDDATA *prpDemand);
	void ProcessDemandDelete(DEMANDDATA *prpDemand);
	bool IsPassFilter(JOBDATA *prpJob);

	int	 FindLineByUrno(long lpUrno);
	
	void RemoveAll();
	CString GetRuleEventText(DEMANDDATA &ropDemand);
	CString GetDemandTypeText(DEMANDDATA &ropDemand);

protected:
	CWnd *pomAttachWnd;
	CBrush *pomInboundBrush,*pomOutboundBrush,*pomTurnaroundBrush,*pomQualityBrush,*pomResBrush;
	CWorkGroupBackgroundBars omWGBkBars;

public:	

    FlightData	*pomAF;
    FlightData	*pomDF;
	ACRDATA		*prmAcr;

	int imDemandType; // { ALLDEMANDS | PERSONNELDEMANDS | EQUIPMENTDEMANDS | LOCATIONDEMANDS }
	CString omAllocUnitType; // eg GATE or POSITION
	long lmFlightPrimaryKey;
	long lmFlightSecondaryKey;
	void *pmBroadCastInstanceCallerFromFDViewer;

	CCSPtrArray <FLIGHT_LINEDATA> omFlightLines;

	bool bmUseDemandSortValues;
	bool bmHasDifferentTemplates;
	bool bmHasDifferentRules;
	void SetSortOrderFlags(CCSPtrArray <DEMANDDATA> &ropDemands);
	void LoadDataForDelegatedFlight(CCSPtrArray <DEMANDDATA> &ropDemands);
	CCSPtrArray <DEMANDDATA> omDelegatedFlightDemands;

	void MakeLineForDemand(DEMANDDATA *prpDemand, bool bpUpdateGantt);
	void MakeLineForJobWithoutDemand(JOBDATA *prpJob, bool bpUpdateGantt);
	void AddFillerLine(bool bpUpdateGantt);
	void DeleteFillerLine(bool bpUpdateGantt);
	void DeleteLine(int ipLine, bool bpUpdateGantt);
	void AddDemandToLine(FLIGHT_LINEDATA *prpLine, DEMANDDATA *prpDemand, bool bpUpdateGantt);
	void DeleteDemandFromLine(FLIGHT_LINEDATA *prpLine, long lpDemandUrno, bool bpUpdateGantt);
	void AddJobToLine(FLIGHT_LINEDATA *prpLine, JOBDATA *prpJob, bool bpUpdateGantt);
	void DeleteJobFromLine(FLIGHT_LINEDATA *prpLine, long lpJobUrno, bool bpUpdateGantt);

	bool IsPassFilter(DEMANDDATA *prpDemand);
	int CreateLine(FLIGHT_LINEDATA *prpNewLine);

	int CompareLine(CTime opJob1StartTime, CTime opJob2StartTime);
	int CompareLine(long lpDem1Urno, long lpDem2Urno);
	int CompareDemandFlightTypeAndTime(const DEMANDDATA *prpDem1, const DEMANDDATA *prpDem2);
	int CompareDemandDisp(const DEMANDDATA *prpDem1, const DEMANDDATA *prpDem2);
	int CompareDemandTypeAndDisp(const DEMANDDATA *prpDem1, const DEMANDDATA *prpDem2);
	int CompareDemandTemplateTypeAndDisp(const DEMANDDATA *prpDem1, const DEMANDDATA *prpDem2);

	void GetGanttTimes(CTime &ropStartTime, CTime &ropEndTime);

	bool BringBarToFront(int ipLine, int ipBar, bool bpSetMap = true);
	CMapPtrToPtr omSelectedBarsMap;
	void ReselectBars(void);

	CMapPtrToPtr omSelectedBars;
	bool SelectBar(FLIGHT_BARDATA *prpBar, bool bpSelect = true, bool bpRepaint = true);
	void CheckSelectBar(FLIGHT_BARDATA *prpBar);
	void DeselectAllBars();
	void SelectedJobDeleted(JOBDATA *prpJob);
	void SelectedJobDeleted(long lpJobUrno);
	int GetSelectedJobs(CCSPtrArray <JOBDATA> &ropJobs);
	int GetDisplayedDemands(CCSPtrArray <DEMANDDATA> &ropDemands);
	void CaleculateMinMaxDisplayTime();
	void SetFunctionCodeMap(const CMapStringToPtr*& prpMapFunctionCode); //PRF 8999
	void SetEquDemandGroupMap(const CMapStringToPtr*& prpMapEquDemandGroup);
public:
	bool bmJob_Moved_Added_Deleted_Flag;
	CTime omMinStartTime;
	CTime omMaxEndTime;
	int GetFirstDisplayLine(CTime opDisplayStart, CTime opDisplayEnd);
private:
	CMapStringToPtr* pomMapFunctionCode; //PRF 8999	
	CMapStringToPtr* pomMapEquDemandGropus;
};


/////////////////////////////////////////////////////////////////////////////

#endif
