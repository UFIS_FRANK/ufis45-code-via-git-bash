// Class for DRATAB - temporary unpaid absences
#ifndef _CEDADRADATA_H_
#define _CEDADRADATA_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct DraDataStruct
{
	long Urno;
	char Sdac[6];	// Absence code in ODATAB

	DraDataStruct(void)
	{
		Urno = 0L;
		strcpy(Sdac,"");
	}
};

typedef struct DraDataStruct DRADATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaDraData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <DRADATA> omData;
	CMapPtrToPtr omUrnoMap;
	CString GetTableName(void);

// Operations
public:
	CedaDraData();
	~CedaDraData();

	void ProcessDraBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	CCSReturnCode ReadDraData();
	DRADATA* GetDraByUrno(long lpUrno);
	void PrepareData(DRADATA *prpDra);
	CCSReturnCode DeleteDraRecord(long lpUrno);

private:
	DRADATA *AddDraInternal(DRADATA &rrpDra);
	void DeleteDraInternal(long lpUrno);
	void ClearAll();
};

extern CedaDraData ogDraData;

#endif _CEDADRADATA_H_
