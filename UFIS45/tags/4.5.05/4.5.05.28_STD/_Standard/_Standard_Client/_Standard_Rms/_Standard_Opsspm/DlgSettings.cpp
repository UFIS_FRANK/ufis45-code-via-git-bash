// DlgSettings.cpp : object to store the values held in dialog fields
//					 so that the dialog can be reinitialised with the
//					 last values used when the dialog was closed
//

#include <stdafx.h>
#include <opsspm.h>
#include <DlgSettings.h>
#include <GUILng.h>
#include <CCSGlobl.h>
#include <BasicData.h>
#include <CedaCfgData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgSettings dialog
const CString CDlgSettings::CTYP_OTHER_COLOURS = "OTHERCOLOURS";
const CString CDlgSettings::CKEY_OTHER_COLOURS_PRM_FINALIZED        = "Finalized PRM";
const CString CDlgSettings::CKEY_OTHER_COLOURS_PRM_LOCATION_LOUNGE  = "Location is lounge";

CDlgSettings::CDlgSettings(CWnd* pParent /*=NULL*/)
{
}

CDlgSettings::~CDlgSettings()
{
	CString olDlgName;
	CMapStringToString *polValueMap;
	for(POSITION rlPos = omDlgMap.GetStartPosition(); rlPos != NULL; )
	{
		omDlgMap.GetNextAssoc(rlPos, olDlgName,(void *& ) polValueMap);
		polValueMap->RemoveAll();
		delete polValueMap;
	}
	omDlgMap.RemoveAll();
}

void CDlgSettings::EnableSaveToDb(const char *pcpDlgName)
{
	omSaveToDb.SetAt(pcpDlgName, (void *) NULL);
}

bool CDlgSettings::SaveToDbEnabled(const char *pcpDlgName)
{
	void *pvlDummy = NULL;
	if(omSaveToDb.Lookup(pcpDlgName, (void *&) pvlDummy))
		return true;
	return false;
}

void CDlgSettings::LoadSettingsFromDb(const char *pcpDlgName)
{
	CCSPtrArray <CFGDATA> olCfgs;
	int ilNumCfgs = ogCfgData.GetCfgsByCtyp(pcpDlgName, olCfgs);
	for(int ilCfg = 0; ilCfg < ilNumCfgs; ilCfg++)
	{
		CFGDATA *prlCfg = &olCfgs[ilCfg];
		AddValue(pcpDlgName, prlCfg->Ckey, prlCfg->Text);
	}
}

void CDlgSettings::SaveSettingsToDb(void)
{
	for(int ilDD = 0; ilDD < omDeletedDialogs.GetSize(); ilDD++)
		ogCfgData.DeleteByCtyp(omDeletedDialogs[ilDD]);
	omDeletedDialogs.RemoveAll();

	CString olDlgName, olFieldName, olValue;
	CMapStringToString *polValueMap;
	for(POSITION rlPos = omDlgMap.GetStartPosition(); rlPos != NULL; )
	{
		omDlgMap.GetNextAssoc(rlPos, olDlgName,(void *& ) polValueMap);
		if(SaveToDbEnabled(olDlgName))
		{
			ogCfgData.DeleteByCtyp(olDlgName);
			for(POSITION rlPos2 = polValueMap->GetStartPosition(); rlPos2 != NULL; )
			{
				polValueMap->GetNextAssoc(rlPos2, olFieldName, olValue);
				ogCfgData.CreateCfg(olDlgName, olFieldName, olValue);
			}
		}
	}
}

void CDlgSettings::AddValue(const char *pcpDlgName, const char *pcpFieldName, const char *pcpValue)
{
	CMapStringToString *polValueMap;
	if(omDlgMap.Lookup(pcpDlgName, (void *&) polValueMap))
	{
		polValueMap->SetAt(pcpFieldName, pcpValue);
	}
	else
	{
		polValueMap = new CMapStringToString;
		polValueMap->SetAt(pcpFieldName, pcpValue);
		omDlgMap.SetAt(pcpDlgName, polValueMap);
	}
}

bool CDlgSettings::GetValue(const char *pcpDlgName, const char *pcpFieldName, CString &ropValue)
{
	bool blFound = false;

	CMapStringToString *polValueMap;
	if(omDlgMap.Lookup(pcpDlgName, (void *&) polValueMap))
	{
		if(polValueMap->Lookup(pcpFieldName, ropValue))
			blFound = true;
	}

	return blFound;
}

CString CDlgSettings::GetValue(const char *pcpDlgName, UINT ipFieldId)
{
	CString olValue;
	GetValue(pcpDlgName, GetFieldName(ipFieldId), olValue);
	return olValue;
}

void CDlgSettings::GetAllValuesForDialog(const char *pcpDlgName, CStringArray &ropFieldNames, CStringArray &ropValues)
{
	CMapStringToString *polValueMap;
	CString olFieldName, olValue;
	if(omDlgMap.Lookup(pcpDlgName, (void *&) polValueMap))
	{
		for(POSITION rlPos = polValueMap->GetStartPosition(); rlPos != NULL; )
		{
			polValueMap->GetNextAssoc(rlPos, olFieldName, olValue);
			ropFieldNames.Add(olFieldName);
			ropValues.Add(olValue);
		}
	}
}

int CDlgSettings::GetRadioButtonValue(const char *pcpDlgName, UINT ipFieldId)
{
	return atoi(ogDlgSettings.GetValue(pcpDlgName, ipFieldId));
}

BOOL CDlgSettings::GetCheckBoxValue(const char *pcpDlgName, UINT ipFieldId)
{
	return (GetValue(pcpDlgName, ipFieldId) == "1") ? TRUE : FALSE;
}

void CDlgSettings::DeleteSettingsByDialog(const char *pcpDlgName)
{
	CMapStringToString *polValueMap;
	if(omDlgMap.Lookup(pcpDlgName, (void *&) polValueMap))
	{
		polValueMap->RemoveAll();
		delete polValueMap;
		omDlgMap.RemoveKey(pcpDlgName);
		if(SaveToDbEnabled(pcpDlgName))
			omDeletedDialogs.Add(pcpDlgName);
//		ogCfgData.DeleteByCtyp(pcpDlgName);
	}
}

CString CDlgSettings::GetFieldName(UINT ipFieldId)
{
	CString olFieldName;
	olFieldName.Format("%d", ipFieldId);
	return olFieldName;
}

void CDlgSettings::SetFieldValue(CDialog *popDlg, const char *pcpDlgName, UINT ipFieldId)
{
	CheckIfLoadedFromDb(pcpDlgName);

	CWnd *polField = NULL;
	if(popDlg != NULL && (polField = popDlg->GetDlgItem(ipFieldId)) != NULL)
	{
		CString olValue;
		if(GetValue(pcpDlgName, GetFieldName(ipFieldId), olValue))
			polField->SetWindowText(olValue);		
	}
}

void CDlgSettings::SetCheckboxValue(CDialog *popDlg, const char *pcpDlgName, UINT ipFieldId)
{
	CheckIfLoadedFromDb(pcpDlgName);

	CButton *polCheckbox = NULL;
	if(popDlg != NULL && (polCheckbox = (CButton *) popDlg->GetDlgItem(ipFieldId)) != NULL)
	{
		CString olValue;
		if(GetValue(pcpDlgName, GetFieldName(ipFieldId), olValue))
			polCheckbox->SetCheck(atoi(olValue));
	}
}

void CDlgSettings::CheckIfLoadedFromDb(const char *pcpDlgName)
{
	CMapStringToString *polValueMap;
	if(SaveToDbEnabled(pcpDlgName) && !omDlgMap.Lookup(pcpDlgName, (void *&) polValueMap))
	{
		ogDlgSettings.LoadSettingsFromDb(pcpDlgName);
	}
}

void CDlgSettings::SaveFieldValue(CDialog *popDlg, const char *pcpDlgName, UINT ipFieldId)
{
	CWnd *polField = NULL;
	if(popDlg != NULL && (polField = popDlg->GetDlgItem(ipFieldId)) != NULL)
	{
		CString olValue;
		polField->GetWindowText(olValue);
		AddValue(pcpDlgName, GetFieldName(ipFieldId), olValue);
	}
}

void CDlgSettings::SaveCheckboxValue(CDialog *popDlg, const char *pcpDlgName, UINT ipFieldId)
{
	CButton *polCheckbox = NULL;
	if(popDlg != NULL && (polCheckbox = (CButton *) popDlg->GetDlgItem(ipFieldId)) != NULL)
	{
		CString olValue;
		olValue.Format("%d",polCheckbox->GetCheck());
		AddValue(pcpDlgName, GetFieldName(ipFieldId), olValue);
	}
}

void CDlgSettings::SaveRadioButtonValue(const char *pcpDlgName, UINT ipFieldId, int ipValue)
{
	CString olValue;
	olValue.Format("%d",ipValue);
	AddValue(pcpDlgName, GetFieldName(ipFieldId), olValue);
}
