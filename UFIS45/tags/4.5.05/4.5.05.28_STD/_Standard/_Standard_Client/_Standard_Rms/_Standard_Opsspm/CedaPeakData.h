#ifndef _CPEAKD_H_
#define _CPEAKD_H_

#include <CCSCedaData.h>
#include <CCSPtrArray.h>
#include <cedaalodata.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration


#define MAXPEAKVALUES 288

//@Man: PEAKDATA
/*@Doc:

  \begin{verbatim}
    SELECT  stid,vafr,vato,days,shfr,shto,
    FROM    shfcki
  \end{verbatim}
*/
//struct ShiftDataStruct {
 struct PeakDataStruct
{
	long  Urno;           // Unique record number
	char  Peid[12];       // Peak Id
	char  Days[12];       // Day of operation
	char  Tmfr[8];        // Time from
	char  Tmto[8];        // Time to
	int	 Olpc;
	CMapPtrToPtr *ShiftMap; // map of shifts covering this peak
	int   SollA;
	int   DefaultFmA; 
	int   DefaultMaA;
	int   PoolFmA;
	int   PoolMaA;
	int   SollB;
	int   DefaultFmB;
	int   DefaultMaB;
	int   PoolFmB;
	int   PoolMaB;
	int   SollHalle;
	int   DefaultFmHalle;
	int   DefaultMaHalle;
	int   PoolFmHalle;
	int   PoolMaHalle;
	int   SollSumme;
	int   DefaultFmSumme;
	int   DefaultMaSumme;
	int   PoolFmSumme;
	int   PoolMaSumme;
	int   FmAArray[MAXPEAKVALUES+1];
	int   FmBArray[MAXPEAKVALUES+1];
	int   FmHalleArray[MAXPEAKVALUES+1];
	int   MaAArray[MAXPEAKVALUES+1];
	int   MaBArray[MAXPEAKVALUES+1];
	int   MaHalleArray[MAXPEAKVALUES+1];
	
 	PeakDataStruct(void) 
	{ memset(this,'\0',sizeof(*this));}
};

typedef struct PeakDataStruct PEAKDATA;


/////////////////////////////////////////////////////////////////////////////
// Class declaration

//@Man:
//@Memo: Class for handling peak data
//@See: CCSCedaData, CedaJobData, PrePlanTable
/*@Doc:
  Reads and writes shift data from/to database and stores it in memory. 
  The shift data class will be used in {\bf ShiftTable} table and the 
  {\bf PrePlanTable}.

*/
class CedaPeakData: public CCSCedaData
{
// Attributes
public:
    //@ManMemo: PEAKDATA records read by ReadShiftData()
    CCSPtrArray<PEAKDATA> omData;
    //@ManMemo: A map, containing the PKNO fields of all loaded employees.
	void CreateNewItem(ALODATA *prpAlo,CString &opAloText);
	void GetAlidText(char *pcpStation,CString &opAloText);
	CString GetTableName(void);

	int pimDemandsA[MAXPEAKVALUES+1];
	int pimDemandsB[MAXPEAKVALUES+1];
// Operations
public:
    //@ManMemo: Default constructor
    /*@Doc:
      Initialize {\bf omRecInfo} for offsets and types for each fields in {\bf SHIFTDATA},
      the {\bf pcmTableName} with table name {\bf STFCKI}, the data members {\bf omFieldList}
      contains a list of used fields of database view {\bf STFCKI}.
    */
    CedaPeakData();
    //@ManMemo: Destructor, Unregisters the CedaShiftData object from DataDistribution
	~CedaPeakData();

    //@ManMemo: Read all shift types from database at program start
	CCSReturnCode ReadPeakData();
	void Add(PEAKDATA& rrlPeakData);
	void ClearAllValues(int ipPeakNo);
	void ClearArrays(int ipPeakNo);
	void Recalculate(CTime opDate);
	void RecalculatePoolJobs(CTime opDate);
	void AddToPeak(int *pipValues,CTime opFrom,CTime opTo);
	int GetMaxValueOfPeak(int *pipValues);

private:

};


extern CedaPeakData ogPeaks;

#endif
