// PrmJobWithoutDemDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <PrmJobWithoutDemDlg.h>
#include <CCSTime.h>
#include <BasicData.h>
#include <CedaTplData.h>
#include <AllocData.h>
#include <CedaSerData.h>
#include <CedaDpxData.h>
#include <CedaWroData.h>
#include <CedaEmpData.h>
#include <DataSet.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PrmJobWithoutDemDlg dialog


PrmJobWithoutDemDlg::PrmJobWithoutDemDlg(CWnd* pParent /*=NULL*/)
	: CDialog(PrmJobWithoutDemDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(PrmJobWithoutDemDlg)
	m_FromDate = NULL;
	m_FromTime = NULL;
	m_ToDate = NULL;
	m_ToTime = NULL;

	//}}AFX_DATA_INIT
	bmDisableAlocationUnit = false;
	bmDisableTemplate = false;
	lmUtpl = 0L;
}

void PrmJobWithoutDemDlg::InitDefaultValues(long lpStfUrno, long lpPooljobUrno, CTime opFrom, CTime opTo, CString opAloc /*= ""*/, long lpUtpl /*= 0L*/)
{
	omFrom = opFrom;
	omTo = opTo;
	omAloc = opAloc;
	lmUtpl = lpUtpl;
	lmStfUrno = lpStfUrno;
	lmPooljobUrno = lpPooljobUrno;
}

void PrmJobWithoutDemDlg::DisableAlocationUnit()
{
	bmDisableAlocationUnit = true;
}

void PrmJobWithoutDemDlg::DisableTemplate()
{
	bmDisableTemplate = true;
}

void PrmJobWithoutDemDlg::DoDataExchange(CDataExchange* pDX)
{
	if(pDX->m_bSaveAndValidate == FALSE)
	{
		m_FromDate = omFrom;
		m_FromTime = omFrom;
		m_ToDate = omTo;
		m_ToTime = omTo;
	}

	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PrmJobWithoutDemDlg)
	DDX_CCSddmmyy(pDX, IDC_FROMDATE, m_FromDate);
	DDX_CCSTime(pDX, IDC_FROMTIME, m_FromTime);
	DDX_CCSddmmyy(pDX, IDC_TODATE, m_ToDate);
	DDX_CCSTime(pDX, IDC_TOTIME, m_ToTime);
	//}}AFX_DATA_MAP

	if(pDX->m_bSaveAndValidate == TRUE)
	{
		// Convert the date and time back
		omFrom = CTime(m_FromDate.GetYear(), m_FromDate.GetMonth(), m_FromDate.GetDay(), 
						m_FromTime.GetHour(), m_FromTime.GetMinute(),	m_FromTime.GetSecond());
		omTo = CTime(m_ToDate.GetYear(), m_ToDate.GetMonth(),m_ToDate.GetDay(),
						m_ToTime.GetHour(), m_ToTime.GetMinute(),	m_ToTime.GetSecond());
	}
}


BEGIN_MESSAGE_MAP(PrmJobWithoutDemDlg, CDialog)
	//{{AFX_MSG_MAP(PrmJobWithoutDemDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PrmJobWithoutDemDlg message handlers

BOOL PrmJobWithoutDemDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CWnd *polWnd = NULL;


	EMPDATA *prlStaff = ogEmpData.GetEmpByUrno(lmStfUrno);
	if (prlStaff != NULL)
	{
		char clTemp[256];
		sprintf(clTemp,"%s, %s",prlStaff->Finm,prlStaff->Lanm);
		SetWindowText(clTemp);
	}

//	SetWindowText(GetString(IDS_JWDD_TITLE));
	polWnd = GetDlgItem(IDC_FROMTITLE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_JWDD_FROMTITLE));
	}
	polWnd = GetDlgItem(IDC_TOTITLE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_JWDD_TOTITLE));
	}
	polWnd = GetDlgItem(IDC_TEMPLATETITLE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_JWDD_TEMPLATE));
	}

	CComboBox *polTemplates = (CComboBox *) GetDlgItem(IDC_TEMPLATE);
	if(polTemplates != NULL)
	{
		polTemplates->ResetContent();

		int ilIndex = -1;
		if(lmUtpl == 0L)
		{
			void *pvlDummy;
			CMapStringToPtr olTemplatesSoFar; // template names are duplicated (for Flight dependendent and indie)
			CDWordArray olTplUrnos;
			ogBasicData.GetTemplateFilters(olTplUrnos);
			int ilNumTpls = olTplUrnos.GetSize();
			for(int ilTpl = 0; ilTpl < ilNumTpls; ilTpl++)
			{
				TPLDATA *prlTpl = ogTplData.GetTplByUrno(olTplUrnos[ilTpl]);
				if(prlTpl != NULL && !olTemplatesSoFar.Lookup(prlTpl->Tnam,(void *&) pvlDummy))
				{
					if((ilIndex = polTemplates->AddString(prlTpl->Tnam)) != CB_ERR)
					{
						polTemplates->SetItemData(ilIndex, prlTpl->Urno);
						olTemplatesSoFar.SetAt(prlTpl->Tnam,NULL);
					}
				}
			}
		}
		else
		{
			TPLDATA *prlTpl = ogTplData.GetTplByUrno(lmUtpl);
			if(prlTpl != NULL)
				if((ilIndex = polTemplates->AddString(prlTpl->Tnam)) != CB_ERR)
					polTemplates->SetItemData(ilIndex, prlTpl->Urno);
		}

		if(polTemplates->GetCount() > 0)
		{
			polTemplates->SetCurSel(0);
		}

		if(bmDisableTemplate)
		{
			polWnd = GetDlgItem(IDC_TEMPLATE);
			polWnd->EnableWindow(FALSE);
		}
	}
	polWnd = GetDlgItem(IDC_ALLOCUNITTITLE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_JWDD_ALLOCUNIT));
	}

	CComboBox *polServices = (CComboBox *) GetDlgItem(IDC_SERVICETYPE);
	if(polServices != NULL)
	{
		polServices->ResetContent();

		int ilSerCount = ogSerData.omData.GetSize();
		SERDATA *blpSerData;
		int ilIndex;

		// lli: load PRM-LOADALLSER setting from ceda.ini, default is loading only Dtyp == 'O'
		char pclTmpText[512];
		char pclConfigPath[512];
		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));

		GetPrivateProfileString(pcgAppName, "PRM-LOADALLSER",  "NO",pclTmpText, sizeof pclTmpText, pclConfigPath);
		if (!strcmp(pclTmpText,"YES")) {
			for (int ilLc = 0; ilLc < ilSerCount; ilLc++) 
			{
				blpSerData = &ogSerData.omData[ilLc];
				// lli: SATS disable the PRM types text box in ServiceCatalog so SATS can't use this column
		//			if (*blpSerData->Dtyp == 'O')
				{
					ilIndex = polServices->AddString(blpSerData->Snam);
					polServices->SetItemData(ilIndex,blpSerData->Urno);
				}
			}
		}
		else
		{
			for (int ilLc = 0; ilLc < ilSerCount; ilLc++) 
			{
				blpSerData = &ogSerData.omData[ilLc];
				if (*blpSerData->Dtyp == 'O')
				{
					ilIndex = polServices->AddString(blpSerData->Snam);
					polServices->SetItemData(ilIndex,blpSerData->Urno);
				}
			}
		}
	}

	CComboBox *polPrm = (CComboBox *) GetDlgItem(IDC_PRM);
	if(polPrm != NULL)
	{
		polPrm->ResetContent();

		int ilDpxCount = ogDpxData.omData.GetSize();
		DPXDATA *blpDpxData;
		int ilIndex;

		for (int ilLc = 0; ilLc < ilDpxCount; ilLc++) 
		{
			blpDpxData = &ogDpxData.omData[ilLc];
			ilIndex = polPrm->AddString(blpDpxData->Name);
			polPrm->SetItemData(ilIndex,blpDpxData->Urno);
		}
	}


	CComboBox *polWro = (CComboBox *) GetDlgItem(IDC_ALLOCUNIT);
	if(polWro != NULL)
	{
		polWro->ResetContent();

		int ilWroCount = ogWroData.omData.GetSize();
		WRODATA *blpWroData;
		int ilIndex;

		for (int ilLc = 0; ilLc < ilWroCount; ilLc++) 
		{
			blpWroData = &ogWroData.omData[ilLc];
			ilIndex = polWro->AddString(blpWroData->Wnam);
			polWro->SetItemData(ilIndex,blpWroData->Urno);
		}
	
	}


	polWnd = GetDlgItem(IDC_OK);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_JWDD_OK));
	}
	polWnd = GetDlgItem(IDC_CANCEL);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(GetString(IDS_JWDD_CANCEL));
	}
	
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void PrmJobWithoutDemDlg::OnOK() 
{
	if(UpdateData(TRUE))
	{
		bool blRc = true;
		if(blRc && omFrom >= omTo)
		{
			MessageBox("The job must start before it ends!","Split Job Error", MB_ICONERROR);
			blRc = false;
		}

		if(blRc)
		{
			CComboBox *polTemplates = (CComboBox *) GetDlgItem(IDC_TEMPLATE);
			if(polTemplates != NULL)
			{
				int ilIndex = polTemplates->GetCurSel();
				if(ilIndex != CB_ERR)
				{
					lmUtpl = (long) polTemplates->GetItemData(ilIndex);
				}
			}
		}
		if(blRc)
		{
			CComboBox *polService = (CComboBox *) GetDlgItem(IDC_SERVICETYPE);
			if(polService != NULL)
			{
				int ilIndex = polService->GetCurSel();
				if(ilIndex != CB_ERR)
				{
					lmUghs = (long) polService->GetItemData(ilIndex);
				}
				else
				{
					ogBasicData.MessageBox(this,GetString(IDS_ALLOC2011), GetString(IDS_ALLOC2012), MB_ICONEXCLAMATION );
					return ;
				}
			}

			CComboBox *polWro = (CComboBox *) GetDlgItem(IDC_ALLOCUNIT);
			if(polWro != NULL)
			{
				int ilIndex = polWro->GetCurSel();
				if(ilIndex != CB_ERR)
				{
					lmWroUrno = (long) polWro->GetItemData(ilIndex);
				}
			}
			CComboBox *polPrm = (CComboBox *) GetDlgItem(IDC_PRM);
			if(polPrm != NULL)
			{
				int ilIndex = polPrm->GetCurSel();
				if(ilIndex != CB_ERR)
				{
					lmPrm = (long) polPrm->GetItemDataPtr(ilIndex);
				}
				else
				{
					ogBasicData.MessageBox(this,GetString(IDS_ALLOC2009), GetString(IDS_ALLOC2010), MB_ICONEXCLAMATION );
					return ;
				}
			}
			WRODATA *prlWro = ogWroData.GetWroByUrno(lmWroUrno);
			CString olWroWnam;
			if (prlWro != NULL)
			{
				olWroWnam = prlWro->Wnam;
			}
			ogDataSet.CreatePrmJob(this, lmPooljobUrno, olWroWnam, lmUtpl, lmPrm, lmUghs, omFrom, omTo);
		}
		
		if(blRc)
		{

			CDialog::OnOK();
		}
	}
	
}
