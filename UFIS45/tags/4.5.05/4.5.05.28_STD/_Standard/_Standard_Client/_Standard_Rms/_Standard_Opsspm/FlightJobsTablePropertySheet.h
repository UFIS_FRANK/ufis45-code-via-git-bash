// FlightJobsTablePropertySheet.h : header file
//
#ifndef __FLIGHTTABLEPROPSHEET__
#define __FLIGHTTABLEPROPSHEET__

#include <FlightJobsTableSortPage.h>

/////////////////////////////////////////////////////////////////////////////
// FlightJobsTablePropertySheet

class FlightJobsTablePropertySheet : public BasePropertySheet
{
// Construction
public:
	FlightJobsTablePropertySheet(CWnd* pParentWnd = NULL,
		CViewer *popViewer = NULL, UINT iSelectPage = 0);

// Attributes
public:
	FilterPage m_pageAirline;
//	FilterPage m_pageAloc;
	FlightJobsTableSortPage m_pageSort;

// Operations
public:
	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
	virtual int QueryForDiscardChanges();
};

#endif // __FLIGHTTABLEPROPSHEET__
/////////////////////////////////////////////////////////////////////////////

