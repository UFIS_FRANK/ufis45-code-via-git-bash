// ConflictTablePropertySheet.h : header file
//
#ifndef _CFTBLPS_H_
#define _CFTBLPS_H_

#include <DemandFilterPage.h>
#include <FilterPage.h>
#include <ConflictTableSortPage.h>

/////////////////////////////////////////////////////////////////////////////
// ConflictTablePropertySheet

class ConflictTablePropertySheet : public BasePropertySheet
{
// Construction
public:
	ConflictTablePropertySheet(CWnd* pParentWnd = NULL,CViewer *popViewer = NULL, UINT iSelectPage = 0);
	~ConflictTablePropertySheet();

// Attributes
public:
	FilterPage				m_pageType;
	DemandFilterPage		m_pageObjectType;
	DemandFilterPage		m_pageAloc;
	ConflictTableSortPage	m_pageSort;
	FilterPage				m_pageAirlines;
	FilterPage				m_pageAgents;
	FilterPage				m_pagePools;
	FilterPage				m_pageFunctions;
	CMapStringToPtr			m_pageAlids;

// Operations
public:
	virtual void LoadDataFromViewer();
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);
	virtual int QueryForDiscardChanges();

	// Generated message map functions
protected:
	//{{AFX_MSG(ConflictTablePropertySheet)
	afx_msg LONG OnUpdateAllPages(UINT wParam, LONG lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif // _CFTBLPS_H_
/////////////////////////////////////////////////////////////////////////////

