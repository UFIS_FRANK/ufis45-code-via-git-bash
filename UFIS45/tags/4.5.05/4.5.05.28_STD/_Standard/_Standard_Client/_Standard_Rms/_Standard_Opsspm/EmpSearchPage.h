// EmpSearchPage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// EmpSearchPage dialog
#ifndef _EMPSEARCH_PAGE_
#define _EMPSEARCH_PAGE_

class EmpSearchPage : public CPropertyPage
{
	DECLARE_DYNCREATE(EmpSearchPage)

// Construction
public:
	EmpSearchPage();
	~EmpSearchPage();

// Dialog Data
	//{{AFX_DATA(EmpSearchPage)
	enum { IDD = IDD_EMP_SORT_PAGE };
//	CTime	m_Date;
	CComboBox	m_Date;
	CString	m_Name;
	CString	m_Rank;
	CString	m_Shift;
	int		m_SingleEmployee;
	//}}AFX_DATA
	CString omTitle;

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(EmpSearchPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(EmpSearchPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnRadioGanttSelect();
	afx_msg void OnRadioDetailDisplay();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	static int imLastState;
	int EmployeeDetailDisplayAsDefault() const;

};

#endif // _EMPSEARCH_PAGE_
