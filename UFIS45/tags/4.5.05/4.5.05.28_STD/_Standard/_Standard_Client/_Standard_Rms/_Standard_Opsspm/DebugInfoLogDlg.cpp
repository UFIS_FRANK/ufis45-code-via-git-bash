// DebugInfoLogDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <DebugInfoLogDlg.h>
#include <BasicData.h>
#include <ccsddx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDebugInfoLogDlg dialog
static void DebugInfoLogDlgCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);


CDebugInfoLogDlg::CDebugInfoLogDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDebugInfoLogDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDebugInfoLogDlg)
	m_List = _T("");
	m_ShowUpdates = TRUE;
	m_Foreground = TRUE;
	//}}AFX_DATA_INIT

 	pomParent = pParent;
	imID = CDebugInfoLogDlg::IDD;
	bmShowUpdates = true;
	bmForeground = true;
	ogCCSDdx.Register(this, DEBUG_INFO_LOG_UPDATE,CString("DebugInfoLogDlg"), CString("DEBUG_INFO_LOG_UPDATE"), DebugInfoLogDlgCf);
}

CDebugInfoLogDlg::~CDebugInfoLogDlg()
{
    ogCCSDdx.UnRegister(this, NOTUSED);
}

void CDebugInfoLogDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDebugInfoLogDlg)
	DDX_Control(pDX, IDC_LIST, m_ListCtrl);
	DDX_LBString(pDX, IDC_LIST, m_List);
	DDX_Check(pDX, IDC_STOPSTART, m_ShowUpdates);
	DDX_Check(pDX, IDC_FOREGROUND, m_Foreground);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDebugInfoLogDlg, CDialog)
	//{{AFX_MSG_MAP(CDebugInfoLogDlg)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_STOPSTART, OnStopstart)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_FOREGROUND, OnForeground)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static void DebugInfoLogDlgCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
    CDebugInfoLogDlg *polDlg = (CDebugInfoLogDlg *)popInstance;
	polDlg->UpdateList((CString *)vpDataPointer);
}


/////////////////////////////////////////////////////////////////////////////
// CDebugInfoLogDlg message handlers

BOOL CDebugInfoLogDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	int ilNumLines = ogBasicData.omDebugInfoLog.GetSize();
	for(int ilLine = 0; ilLine < ilNumLines; ilLine++)
	{
		m_ListCtrl.AddString(ogBasicData.omDebugInfoLog[ilLine]);
	}

	SetWindowPos(&wndTopMost,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDebugInfoLogDlg::UpdateList(CString *popMessage) 
{
	if(bmShowUpdates)
	{
		m_ListCtrl.SetTopIndex(m_ListCtrl.AddString(*popMessage));
	}
	else
	{
		omBufferedUpdates.Add(*popMessage);
	}
}

void CDebugInfoLogDlg::OnDestroy() 
{
	CDialog::OnDestroy();
}

void CDebugInfoLogDlg::PostNcDestroy() 
{
	delete this;	
	CDialog::PostNcDestroy();
}

void CDebugInfoLogDlg::OnCancel() 
{
	CDialog::OnCancel();
}

BOOL CDebugInfoLogDlg::Create() 
{
	return CDialog::Create(imID, pomParent);
}

void CDebugInfoLogDlg::OnStopstart() 
{
	if(bmShowUpdates)
	{
		bmShowUpdates = false;
	}
	else
	{
		bmShowUpdates = true;
		int ilNumBufferedUpdates = omBufferedUpdates.GetSize();
		for(int ilBU = 0; ilBU < ilNumBufferedUpdates; ilBU++)
		{
			UpdateList(&omBufferedUpdates[ilBU]);
		}
		omBufferedUpdates.RemoveAll();
	}
}

void CDebugInfoLogDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	if (nType != SIZE_MINIMIZED && ::IsWindow(m_ListCtrl.GetSafeHwnd()))
	{
        CRect olRect;
        GetClientRect(&olRect);
		olRect.OffsetRect(-1, -1);
		olRect.top += 25;
        m_ListCtrl.MoveWindow(&olRect, true);
	}
}

void CDebugInfoLogDlg::OnForeground() 
{
	if(bmForeground)
	{
		bmForeground = false;
		SetWindowPos(&wndNoTopMost,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
	}
	else
	{
		bmForeground = true;
		SetWindowPos(&wndTopMost,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
	}
}
