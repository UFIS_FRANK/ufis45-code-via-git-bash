// cciviewer.cpp : implementation file
//  

#include <stdafx.h>
#include <CCSGlobl.h>
#include <OpssPm.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <AllocData.h>
#include <CedaJobData.h>
#include <CciViewer.h>
#include <CedaJodData.h>
#include <CedaEmpData.h>
#include <gantt.h>
#include <gbar.h>
#include <BasicData.h>
#include <ccsddx.h>
#include <cviewer.h>
#include <CciViewer.h>
#include <CedaShiftData.h>
#include <conflict.h>
#include <CCSCedaCom.h>
#include <CedaRudData.h>
#include <CedaSerData.h>
#include <CedaCicData.h>
#include <CedaCcaData.h>
#include <CedaDemandData.h>
#include <CedaAltData.h>
#include <DataSet.h>
#include <CedaPaxData.h>
#include <CedaPfcData.h>
#include <FieldConfigDlg.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


static int ByStart(const CCI_EXTRAJOB **pppJob1, const CCI_EXTRAJOB **pppJob2);
static int ByStart(const CCI_EXTRAJOB **pppJob1, const CCI_EXTRAJOB **pppJob2)
{
	return (int)(strcmp((*pppJob1)->SortString,(*pppJob2)->SortString));
}

static int ByJobStart(const JOBDATA **pppJob1, const JOBDATA **pppJob2);
static int ByJobStart(const JOBDATA **pppJob1, const JOBDATA **pppJob2)
{
	return (int)((**pppJob1).Acfr.GetTime() - (**pppJob2).Acfr.GetTime());
}

static int ByStod(const FLIGHTDATA **pppFlight1, const FLIGHTDATA **pppFlight2);
static int ByStod(const FLIGHTDATA **pppFlight1, const FLIGHTDATA **pppFlight2)
{
	return (int)((**pppFlight1).Stod.GetTime() - (**pppFlight2).Stod.GetTime());
}

static int ByCounter(const FLIGHTDATA **pppFlight1, const FLIGHTDATA **pppFlight2);
static int ByCounter(const FLIGHTDATA **pppFlight1, const FLIGHTDATA **pppFlight2)
{
	int ilRes = (int)(strcmp((*pppFlight1)->Line,(*pppFlight2)->Line));
	if(ilRes == 0) ilRes = ((**pppFlight1).Stod.GetTime() - (**pppFlight2).Stod.GetTime());
	return ilRes;
}

static int ByShiftTimeAndFunc(const CCI_PRINTSHIFTS **ppp1, const CCI_PRINTSHIFTS **ppp2);
static int ByShiftTimeAndFunc(const CCI_PRINTSHIFTS **ppp1, const CCI_PRINTSHIFTS **ppp2)
{
	int ilRes = (int)((*ppp1)->prlShift->Avfa.GetTime() - (*ppp2)->prlShift->Avfa.GetTime());
	if(ilRes == 0)
	{
		if((*ppp1)->ilMainFunctionIndex != -1)
			ilRes = (int)((*ppp1)->ilMainFunctionIndex - (*ppp2)->ilMainFunctionIndex);
		else
			ilRes = (int)(strcmp((*ppp1)->olMainFunction,(*ppp2)->olMainFunction));
	}
	return ilRes;
}

static int ByFuncAndShiftTime(const CCI_PRINTSHIFTS **ppp1, const CCI_PRINTSHIFTS **ppp2);
static int ByFuncAndShiftTime(const CCI_PRINTSHIFTS **ppp1, const CCI_PRINTSHIFTS **ppp2)
{
	int ilRes = 0;
	if((*ppp1)->ilMainFunctionIndex != -1)
		ilRes = (int)((*ppp1)->ilMainFunctionIndex - (*ppp2)->ilMainFunctionIndex);
	else
		ilRes = (int)(strcmp((*ppp1)->olMainFunction,(*ppp2)->olMainFunction));
	if(ilRes == 0) ilRes = (int)((*ppp1)->prlShift->Avfa.GetTime() - (*ppp2)->prlShift->Avfa.GetTime());
	return ilRes;
}

//static int ByShiftTimeAndFunc(const CCI_PRINTSHIFTS **ppp1, const CCI_PRINTSHIFTS **ppp2);
//static int ByShiftTimeAndFunc(const CCI_PRINTSHIFTS **ppp1, const CCI_PRINTSHIFTS **ppp2)
//{
//	int ilRes = (int)((*ppp1)->prlShift->Avfa.GetTime() - (*ppp2)->prlShift->Avfa.GetTime());
//	if(ilRes == 0) return (int)(strcmp((*ppp1)->olMainFunction,(*ppp2)->olMainFunction));
//	return ilRes;
//}
//
//static int ByFuncAndShiftTime(const CCI_PRINTSHIFTS **ppp1, const CCI_PRINTSHIFTS **ppp2);
//static int ByFuncAndShiftTime(const CCI_PRINTSHIFTS **ppp1, const CCI_PRINTSHIFTS **ppp2)
//{
//	int ilRes = (int)(strcmp((*ppp1)->olMainFunction,(*ppp2)->olMainFunction));
//	if(ilRes == 0) return (int)((*ppp1)->prlShift->Avfa.GetTime() - (*ppp2)->prlShift->Avfa.GetTime());
//	return ilRes;
//}

static int ByName(const ALLOCUNIT **ppp1, const ALLOCUNIT **ppp2)
{
	return (int)(strcmp((*ppp1)->Name,(*ppp2)->Name));
}

static int ByName(const CICDATA **ppp1, const CICDATA **ppp2)
{
	return (int)(strcmp((*ppp1)->Cnam,(*ppp2)->Cnam));
}

static int ByTime(const CCI_BKBARDATA **pppBkBar1, const CCI_BKBARDATA **pppBkBar2)
{
	return (int)((**pppBkBar1).StartTime.GetTime() - (**pppBkBar2).StartTime.GetTime());
}

static int ByStartTime(const CCI_BKBARDATA **pppBar1, const CCI_BKBARDATA **pppBar2)
{
	return (int)((**pppBar1).StartTime.GetTime() - (**pppBar2).StartTime.GetTime());
}

static int SortCcaByTime(const CCADATA **pppCca1, const CCADATA **pppCca2);
static int SortCcaByTime(const CCADATA **pppCca1, const CCADATA **pppCca2)
{
	int ilRes = (int)((**pppCca1).Ckba.GetTime() - (**pppCca2).Ckba.GetTime());
	if(ilRes == 0)
		ilRes = (int)((**pppCca1).Ckea.GetTime() - (**pppCca2).Ckea.GetTime());
	return ilRes;
}

static int SortDemByTime(const DEMANDDATA **pppDem1, const DEMANDDATA **pppDem2);
static int SortDemByTime(const DEMANDDATA **pppDem1, const DEMANDDATA **pppDem2)
{
	int ilRes = (int)((**pppDem1).Debe.GetTime() - (**pppDem2).Debe.GetTime());
	if(ilRes == 0)
		ilRes = (int)((**pppDem1).Deen.GetTime() - (**pppDem2).Deen.GetTime());
	return ilRes;
}


//#define HAVE_TO_RUN_WITHOUT_VIEW_PROPERTY

// General macros for testing a point against an interval
#define IsBetween(val, start, end)  ((start) <= (val) && (val) <= (end))
#define IsOverlapped(start1, end1, start2, end2)    ((start1) <= (end2) && (start2) <= (end1))
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))
#define IsWithIn(start1, end1, start2, end2)	((start1) >= (start2) && (end1) <= (end2))

// Groupping definition:
// This will make the program faster a little bit because it will replace string comparison.
enum {
	GROUP_BY_TERMINAL,
	GROUP_BY_HALL,
	GROUP_BY_REGION,
	GROUP_BY_LINE,
};

#define GROUP_BY_DEFAULT	GROUP_BY_TERMINAL	// define a bullet-proof grouping symbol

/////////////////////////////////////////////////////////////////////////////
// CciDiagramViewer
//
CciDiagramViewer::CciDiagramViewer()
{
	pomAttachWnd = NULL;
	imDemandsWithoutCounterGroup = -1;

	omDedicatedCcaBkBrush.CreateSolidBrush(RGB(173,160,156));
	omCommonCcaBkBrush.CreateSolidBrush(GREEN);
	omDemandBrush.CreateSolidBrush(AQUA);

	CBitmap olBitmap;
	olBitmap.LoadBitmap(IDB_NOTAVAIL);
	omBlockedBrush.CreatePatternBrush(&olBitmap);

	ogCCSDdx.Register(this, JOB_NEW,CString("CCIVIEWER"), CString("Job New"), CciDiagramCf);
	ogCCSDdx.Register(this, JOB_CHANGE,CString("CCIVIEWER"), CString("Job Change"), CciDiagramCf);
	ogCCSDdx.Register(this, JOB_DELETE,CString("CCIVIEWER"), CString("Job Delete"), CciDiagramCf);
	ogCCSDdx.Register(this, JOD_NEW,CString("CCIVIEWER"), CString("Jod New"), CciDiagramCf);
	ogCCSDdx.Register(this, JOD_CHANGE,CString("CCIVIEWER"), CString("Jod Change"), CciDiagramCf);
	ogCCSDdx.Register((void *)this,CCA_CHANGE,CString("CCIVIEWER"), CString("Cca changed"),CciDiagramCf);
	ogCCSDdx.Register((void *)this,CCA_DELETE,CString("CCIVIEWER"), CString("Cca deleted"),CciDiagramCf);
	ogCCSDdx.Register((void *)this,DEMAND_NEW,CString("CCIVIEWER"), CString("Dem New"),CciDiagramCf);
	ogCCSDdx.Register((void *)this,DEMAND_CHANGE,CString("CCIVIEWER"), CString("Dem changed"),CciDiagramCf);
	ogCCSDdx.Register((void *)this,DEMAND_DELETE,CString("CCIVIEWER"), CString("Dem deleted"),CciDiagramCf);
	ogCCSDdx.Register(this, REDISPLAY_ALL,CString("CCIVIEWER"), CString("Global Update"), CciDiagramCf);
	ogCCSDdx.Register(this, UPDATE_CONFLICT_SETUP,CString("CCIVIEWER"), CString("Update Conflict Setup"), CciDiagramCf);

	om2Mins = CTimeSpan(0,0,2,0);
	bmUseAllAirlines = true;
}

CciDiagramViewer::~CciDiagramViewer()
{
    ogCCSDdx.UnRegister(this, NOTUSED);
    DeleteAll();
}

void CciDiagramViewer::Attach(CWnd *popAttachWnd)
{
	pomAttachWnd = popAttachWnd;
}

/////////////////////////////////////////////////////////////////////////////
// CciDiagramViewer -- Filtering, Sorting, and Grouping
//
// Requirements:
// In the CCI Diagram we can show the data in two completely different ways,
// by Terminal or by Desk Type.
//
// Methods for change the view:
// ChangeViewTo			Change view, reload everything from Ceda????Data
//
// PrepareGroupping		Prepare the enumerate group value
// PrepareFilter		Prepare CMapStringToPtr or CMapPtrToPtr for filtering
// PrepareSorter		Prepare sort criterias before sorting take place
//
// IsPassFilter			Return TRUE if the given record satisfies the filter
// CompareGroup			Return 0 or -1 or 1 as the result of comparison of two groups
// CompareLine			Return 0 or -1 or 1 as the result of comparison of two lines
//
void CciDiagramViewer::ChangeViewTo(const char *pcpViewName,
	CTime opStartTime, CTime opEndTime)
{
	DeleteAll();	// remove everything

	ogCfgData.rmUserSetup.CCCV = pcpViewName;
	SelectView(pcpViewName);        
	PrepareGrouping();
	PrepareSorter();
	PrepareFilter();
	omStartTime = opStartTime;
	omEndTime = opEndTime;

	MakeGroupsAndLines();
	MakeBackgroundBars();
	MakeBars();
}


void CciDiagramViewer::UpdateManagers(CTime opStartTime, CTime opEndTime)
{
	omStartTime = opStartTime;
	omEndTime = opEndTime;

	int ilGroupCount = GetGroupCount();
	for (int ilGroup = 0; ilGroup < ilGroupCount; ilGroup++)
	{
	    while (GetManagerCount(ilGroup) > 0)
		    DeleteManager(ilGroup, 0);
	}
	MakeManagers();

	// Notifies the attached window (if exist)
	if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
	{
		for (int ilGroup = 0; ilGroup < ilGroupCount; ilGroup++)
		{
			LONG lParam = MAKELONG(-1, ilGroup);
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATEGROUP, lParam);
		}
	}
}

void CciDiagramViewer::PrepareGrouping()
{
	CString olGroupBy = CViewer::GetGroup();
	if (olGroupBy == "Halle")
		omGroupBy = GROUP_BY_TERMINAL;
	else if (olGroupBy == "Hall")
		omGroupBy = GROUP_BY_HALL;
	else if (olGroupBy == "Region")
		omGroupBy = GROUP_BY_REGION;
	else if (olGroupBy == "Line")
		omGroupBy = GROUP_BY_LINE;
	else
		omGroupBy = GROUP_BY_DEFAULT;
}

void CciDiagramViewer::PrepareFilter()
{
	CStringArray olFilterValues;
	int i, n;

	GetFilter("Halle", olFilterValues);
	omCMapForTerminal.RemoveAll();
	n = olFilterValues.GetSize();
	for (i = 0; i < n; i++)
		omCMapForTerminal[olFilterValues[i]] = NULL;

	GetFilter("Hall", olFilterValues);
	omCMapForHall.RemoveAll();
	n = olFilterValues.GetSize();
	for (i = 0; i < n; i++)
		omCMapForHall[olFilterValues[i]] = NULL;

	GetFilter("Region", olFilterValues);
	omCMapForRegion.RemoveAll();
	n = olFilterValues.GetSize();
	for (i = 0; i < n; i++)
		omCMapForRegion[olFilterValues[i]] = NULL;

	GetFilter("Line", olFilterValues);
	omCMapForLine.RemoveAll();
	n = olFilterValues.GetSize();
	for (i = 0; i < n; i++)
		omCMapForLine[olFilterValues[i]] = NULL;

    GetFilter("Airline", olFilterValues);
    omCMapForAirline.RemoveAll();
    n = olFilterValues.GetSize();
	if (n >= 1 && olFilterValues[0] == GetString(IDS_ALLSTRING))
		bmUseAllAirlines = true;
	else
	{
		bmUseAllAirlines = false;
		for (i = 0; i < n; i++)
			omCMapForAirline[olFilterValues[i]] = NULL;
	}

    GetFilter("Functions", olFilterValues);
    omCMapForFunctions.RemoveAll();
    n = olFilterValues.GetSize();
	if (n >= 1 && olFilterValues[0] == GetString(IDS_ALLSTRING))
	{
		bmUseAllFunctions = true;
	}
	else
	{
		bmUseAllFunctions = false;
		for (i = 0; i < n; i++)
		{
			CString olFunction = olFilterValues[i];
			omCMapForFunctions.SetAt(olFunction, (void *) i);
		}
	}
}

void CciDiagramViewer::PrepareSorter()
{
	// sorting order is unnecessary here
}

// terminal is grouping of the cic
// desk type is economy,business etc
BOOL CciDiagramViewer::IsPassFilter(const char *pcpWithoutCounterGroup)
{
	void *p;
	switch (omGroupBy)
	{
	case GROUP_BY_TERMINAL: return omCMapForTerminal.Lookup(CString(pcpWithoutCounterGroup), p);
	case GROUP_BY_HALL:		return omCMapForHall.Lookup(CString(pcpWithoutCounterGroup), p);
	case GROUP_BY_REGION:	return omCMapForRegion.Lookup(CString(pcpWithoutCounterGroup), p);
	case GROUP_BY_LINE:		return omCMapForLine.Lookup(CString(pcpWithoutCounterGroup), p);
	}
	return FALSE;
}

BOOL CciDiagramViewer::IsPassFilter(JOBDATA *prpJob)
{
	BOOL blIsPassFilter = TRUE;
	if(!bmUseAllAirlines && prpJob != NULL)
	{
		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prpJob->Flur);
		if(prlFlight != NULL)
		{
			void *p;
			blIsPassFilter = omCMapForAirline.Lookup(ogAltData.FormatAlcString(prlFlight->Alc2,prlFlight->Alc3),p);
		}
	}

	return blIsPassFilter;
}

BOOL CciDiagramViewer::IsPassFilter(DEMANDDATA *prpDemand)
{
	BOOL blIsPassFilter = TRUE;
	if(!bmUseAllAirlines && prpDemand != NULL && ogDemandData.IsFlightDependingDemand(prpDemand->Dety))
	{
		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(ogDemandData.GetFlightUrno(prpDemand));
		if(prlFlight != NULL)
		{
			void *p;
			blIsPassFilter = omCMapForAirline.Lookup(ogAltData.FormatAlcString(prlFlight->Alc2,prlFlight->Alc3),p);
		}
	}

	return blIsPassFilter;
}

BOOL CciDiagramViewer::IsPassFilter(CCADATA *prpCca)
{
	BOOL blIsPassFilter = TRUE;
	if(!bmUseAllAirlines && prpCca != NULL && !prpCca->IsCommonCheckin())
	{
		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prpCca->Uaft);
		if(prlFlight != NULL)
		{
			void *p;
			blIsPassFilter = omCMapForAirline.Lookup(ogAltData.FormatAlcString(prlFlight->Alc2,prlFlight->Alc3),p);
		}
	}

	return blIsPassFilter;
}

// terminal is grouping of the cic
// desk type is economy,business etc
BOOL CciDiagramViewer::IsPassFilter(const char *pcpTerminal, const char *pcpHall,const char *pcpRegion,const char *pcpLine)
{
	// Since we will filter data in the level of group already, and when we group
	// data by terminal, we ignore desk-type (display all types), and when we group
	// data by desk-type, we ignore terminal (display data from all terminals), so,
	// this IsPassFilter() is actually unnecessary.
	return TRUE;

	void *p;
	BOOL blIsTerminalOk = omCMapForTerminal.Lookup(CString(pcpTerminal), p);
	BOOL blIsHallOk	 = omCMapForHall.Lookup(CString(pcpLine), p);
	BOOL blIsRegionOk= omCMapForRegion.Lookup(CString(pcpLine), p);
	BOOL blIsLineOk  = omCMapForLine.Lookup(CString(pcpLine), p);

	bool blCounterOk = false;
	switch (omGroupBy)
	{
	case GROUP_BY_TERMINAL: return blIsTerminalOk;
	case GROUP_BY_HALL:		return blIsHallOk;
	case GROUP_BY_REGION:	return blIsRegionOk;
	case GROUP_BY_LINE:		return blIsLineOk;
	}
	return FALSE;
}

int CciDiagramViewer::CompareGroup(CCI_GROUPDATA *prpGroup1, CCI_GROUPDATA *prpGroup2)
{
	// Groups in CciDiagram always ordered by GroupId, so let's compare them.
#if	0
	CString &s1 = prpGroup1->GroupId;
	CString &s2 = prpGroup2->GroupId;
	if (omGroupBy == GROUP_BY_TERMINAL)
	{
		return (s1 == s2)? 0: (s1 < s2)? 1: -1;
	}
	else
	{
		return (s1 == s2)? 0: (s1 > s2)? 1: -1;
	}
#else
	return 0;
#endif
}

int CciDiagramViewer::CompareLine(CCI_LINEDATA *prpLine1, CCI_LINEDATA *prpLine2)
{
	// Lines in CciDiagram always use the predefined order in ALOALO.DISP
	return 0;	// let's say there are always equal
}

int CciDiagramViewer::CompareManager(CCI_MANAGERDATA *prpManager1, CCI_MANAGERDATA *prpManager2)
{
	// Compare manager -- we will sort them by name
	return  (prpManager1->EmpLnam == prpManager2->EmpLnam)? 0:
		(prpManager1->EmpLnam > prpManager2->EmpLnam)? 1: -1;
}

/////////////////////////////////////////////////////////////////////////////
// CciDiagramViewer -- Data for displaying graphical objects
//
// The related graphical class: GateDiagram, GateChart, and GateGantt will use this
// GateViewer as the source of data for displaying GanttLine and GanttBar objects.
//
// We let the previous section handles all filtering, sorting, and grouping. In this
// section, we will provide a set of methods which will help us to create the data
// represent the graphical GanttLine and GanttChart as easiest as possible.
//
// Methods for creating graphical objects:
// MakeGroupsAndLines		Create groups and lines that has to be displayed.
// MakeManagers				Create managers for all groups
// MakeLines				Create lines for all groups
// MakeBars					Create bars for all lines in all groups
// MakeLinesWhenGroupByTerminal
// MakeLinesWhenGroupByDeskType
// MakeManager				Create a manager for the specified group
// MakeBar					Create a bar with an indicator for each job
//
void CciDiagramViewer::MakeGroupsAndLines()
{
	CString olGroupBy;
	switch (omGroupBy)
	{
	case GROUP_BY_TERMINAL:
		olGroupBy = "Halle";
		break;
	case GROUP_BY_HALL:
		olGroupBy = "Hall";
		break;
	case GROUP_BY_REGION:
		olGroupBy = "Region";
		break;
	case GROUP_BY_LINE:
		olGroupBy = "Line";
		break;
	default:	// error: invalid groupping specified
		return;
	}

	// Create a group for each value found in the corresponding filter
	CString olWithoutCounterName = GetString(IDS_STRING62514); // "Without Counter"
	imDemandsWithoutCounterGroup = -1;

	CStringArray olGroupIds;
	GetFilter(olGroupBy, olGroupIds);
	int ilNumGroups = olGroupIds.GetSize();
	for(int i = 0; i < ilNumGroups ; i++)
	{
		if (olGroupIds[i] == olWithoutCounterName) // "Without Counter"
		{
			CCI_GROUPDATA rlGroup;
			rlGroup.GroupId = olWithoutCounterName; 
			rlGroup.Text = olWithoutCounterName;
			imDemandsWithoutCounterGroup = CreateGroup(&rlGroup);
			MakeLinesForDemandsWithoutCounter();
		}
		else
		{
			CCI_GROUPDATA rlGroup;
			rlGroup.GroupId = olGroupIds[i];
			rlGroup.Text = olGroupIds[i];
			int ilGroup = CreateGroup(&rlGroup);
			MakeLines(ilGroup);
		}
	}

	MakeManagers();
}


void CciDiagramViewer::MakeManagers()
{
return;
	int ilGroupCount = GetGroupCount();
	for (int ilGroup = 0; ilGroup < ilGroupCount; ilGroup++)
	{
		// Create managers associated with this gate area
		CCSPtrArray<JOBDATA> olFmcJobs;
		ogJobData.GetFmcJobsByAlid(olFmcJobs, GetGroup(ilGroup)->Alid);
		int ilFmcCount = olFmcJobs.GetSize();
		for (int ilLc = 0; ilLc < ilFmcCount; ilLc++)
			MakeManager(ilGroup, &olFmcJobs[ilLc]);
	}
}

void CciDiagramViewer::MakeLinesForDemandsWithoutCounter()
{
	if (imDemandsWithoutCounterGroup == -1)
		return;

	int ilNumDemands = ogDemandData.omData.GetSize();
	for(int ilDemand = 0; ilDemand < ilNumDemands; ilDemand++)
	{
		DEMANDDATA *prlDem = &ogDemandData.omData[ilDemand];
		// check, if personnel demand and CIC demand type only
		if (strcmp(prlDem->Aloc,ALLOCUNITTYPE_CIC) == 0 && strlen(prlDem->Alid) <= 0 && strcmp(prlDem->Rety,PERSONNEL_DEMAND) == 0)
		{
			if(IsPassFilter(prlDem))
			{
				MakeLineForDemandWithoutCounter(prlDem);
			}
		}
	}
}

int CciDiagramViewer::MakeLineForDemandWithoutCounter(DEMANDDATA *prpDem)
{
	int ilLastLineUpdated = -1;

	CCI_LINEDATA *prlLine;
	int ilLineCount, ilLineno;
	CCI_LINEDATA rlDummyLine;
	CString olDummyLineText;

	olDummyLineText.Format("% 5d",0);
	rlDummyLine.Text = olDummyLineText;
	int ilDummyLineno = CreateLine(imDemandsWithoutCounterGroup, &rlDummyLine);

	if (ilDummyLineno >= 0)	// created ?
	{
		CCI_BKBARDATA *prlBarToAdd = MakeDemBkBar(imDemandsWithoutCounterGroup, 0,ilDummyLineno, prpDem);
		if (prlBarToAdd)
		{
			int ilSelectedLine = -1;
			int ilSelectedSide = -1;

			ilLineCount = GetLineCount(imDemandsWithoutCounterGroup);
			for(ilLineno = 0; ilSelectedLine == -1 && ilLineno < ilLineCount; ilLineno++)
			{
				if (ilLineno != ilDummyLineno)
				{
					for (int ilSideno = 0; ilSideno < 2; ilSideno++)
					{
						prlLine = GetLine(imDemandsWithoutCounterGroup,ilSideno,ilLineno);
						int ilNumBars = prlLine->DemBkBars.GetSize();
						if (ilNumBars <= 0)
						{
							ilSelectedLine = ilLineno;
							ilSelectedSide = ilSideno;
						}
						else
						{
							CTimeSpan olOneMinute(0,0,1,0);
							if (ilNumBars > 0 && prlLine->DemBkBars[0].StartTime > prlBarToAdd->EndTime)
							{
								//  the new bar fits in before the first one
									ilSelectedLine = ilLineno;
									ilSelectedSide = ilSideno;
							}

							for(int ilBar = 0; ilSelectedLine == -1 && ilBar < ilNumBars; ilBar++)
							{
								CTime olPrevEnd = prlLine->DemBkBars[ilBar].EndTime;
								CTime olNextBegin = ((ilBar+1) < ilNumBars) ? prlLine->DemBkBars[ilBar+1].StartTime : (prlBarToAdd->EndTime+olOneMinute);
								if (prlBarToAdd->StartTime > olPrevEnd && prlBarToAdd->EndTime < olNextBegin)
								{
									// the new bar fits in the free space
									ilSelectedLine = ilLineno;
									ilSelectedSide = ilSideno;
								}
							}
						}
					}
				}
			}								 

			if (ilSelectedLine != -1)
			{
				// existing line found so add the new bar and sort all bars in this line by duty start
				MakeDemBkBar(imDemandsWithoutCounterGroup, ilSelectedSide,ilSelectedLine, prpDem);
				prlLine = GetLine(imDemandsWithoutCounterGroup, ilSelectedSide,ilSelectedLine);
				prlLine->DemBkBars.Sort(ByStartTime);
				if (ilSelectedLine > ilDummyLineno)
					ilLastLineUpdated = ilSelectedLine - 1; // because we delete line no. 0 later !!!!
				else
					ilLastLineUpdated = ilSelectedLine;		// because we delete line no. 0 later !!!!
			}	
			else
			{
				// no lines free so create a new one
				CCI_LINEDATA rlLine;
				rlLine.Text.Format("% 5d",ilLineCount);
				int ilLineno = CreateLine(imDemandsWithoutCounterGroup, &rlLine);
				MakeDemBkBar(imDemandsWithoutCounterGroup, 0,ilLineno, prpDem);
				if (ilLineno > ilDummyLineno)
					ilLastLineUpdated = ilLineno - 1;		// because we delete line no. 0 later !!!
				else
					ilLastLineUpdated = ilLineno;			// because we delete line no. 0 later !!!
			}

		}
	}

	if (ilDummyLineno >= 0)
	{
		DeleteLine(imDemandsWithoutCounterGroup, ilDummyLineno);
	}

	return ilLastLineUpdated;
		
}

void CciDiagramViewer::MakeLines(int ipGroupno)
{
	if (ipGroupno == -1)
		return;	// avoid adding lines for invalid group (happened for terminal "B Sonst.")

	switch (omGroupBy)
	{
		case GROUP_BY_TERMINAL: MakeLinesWhenGroupByTerminal(ipGroupno); break;
		case GROUP_BY_HALL:		MakeLinesWhenGroupByHall(ipGroupno); break;
		case GROUP_BY_REGION:	MakeLinesWhenGroupByRegion(ipGroupno); break;
		case GROUP_BY_LINE:		MakeLinesWhenGroupByLine(ipGroupno); break;
	}
}

void CciDiagramViewer::MakeBackgroundBars()
{
	CString olUdemAlidList, olTmp;

	int ilNumGroups = omGroups.GetSize();
	for(int ilGroup = 0; ilGroup < ilNumGroups; ilGroup++)
	{
		for(int ilSide = 0; ilSide < 2; ilSide++)
		{
			int ilNumLines = omGroups[ilGroup].Lines[ilSide].GetSize();
			for(int ilLine = 0; ilLine < ilNumLines; ilLine++)
			{
				CCI_LINEDATA *prlLine = &omGroups[ilGroup].Lines[ilSide][ilLine];

				// create demand bk bars only ?
				if (ogCfgData.rmUserSetup.OPTIONS.CCA == USERSETUPDATA::NONE)
				{
					CICDATA *prlCic = ogCicData.GetCicByUrno(prlLine->CicUrno);
					if (prlCic)
					{
						CCSPtrArray<DEMANDDATA> rolDemands;
						ogDemandData.GetDemandsByAlid(rolDemands,prlCic->Cnam,ALLOCUNITTYPE_CIC,PERSONNELDEMANDS);
						for (int ilC = 0; ilC < rolDemands.GetSize(); ilC++)
						{
							DEMANDDATA *prlDem = &rolDemands[ilC];
							if(IsPassFilter(prlDem))
							{
								CCI_BKBARDATA *prlBkBar = MakeDemBkBar(prlLine, prlDem);
							}

						}


						CCSPtrArray<BLKDATA> rolBlocked;
						if (ogBlkData.GetBlocked("CIC",prlCic->Urno,rolBlocked) > 0)
						{
							for (int ilC = 0; ilC < rolBlocked.GetSize(); ilC ++)
							{
								MakeBlkBkBar(prlLine,&rolBlocked[ilC]);
							}
						}
					}

				}
				else
				{
					// create each location demand (CCA records)
					CCSPtrArray <CCADATA> olCounterJobs;
					ogCcaData.GetCcaListByCheckinCounter(prlLine->Text, olCounterJobs);
					olCounterJobs.Sort(SortCcaByTime);
					int ilNumCcas = olCounterJobs.GetSize();
					for(int ilCca = 0; ilCca < ilNumCcas; ilCca++)
					{
						if(IsPassFilter(&olCounterJobs[ilCca]))
							MakeCcaBkBar(prlLine,&olCounterJobs[ilCca]);
					}

					CICDATA *prlCic = ogCicData.GetCicByUrno(prlLine->CicUrno);
					if (prlCic)
					{
						CCSPtrArray <DEMANDDATA> olDemands;
						ogDemandData.GetDemandsByAlid(olDemands,prlCic->Cnam,ALLOCUNITTYPE_CIC,PERSONNELDEMANDS);
						olDemands.Sort(SortDemByTime);
						int ilNumDemands = olDemands.GetSize();
						for (int ilD = 0; ilD < ilNumDemands; ilD++)
						{
							if(IsPassFilter(&olDemands[ilD]))
							{
								MakeDemBkBar(prlLine, &olDemands[ilD]);
							}
						}
						CCSPtrArray <BLKDATA> olBlocked;
						if(ogBlkData.GetBlocked("CIC",prlCic->Urno,olBlocked) > 0)
						{
							int ilNumBlks = olBlocked.GetSize();
							for(int ilB = 0; ilB < ilNumBlks; ilB++)
							{
								MakeBlkBkBar(prlLine,&olBlocked[ilB]);
							}
						}
					}
				}

				prlLine->BlkBkBars.Sort(ByTime);
			}
		}
	}
}

// if the CCA.ULNK contains an URNO then a direct link between the
// location demand (CCA record) and the personnel demand was specified
// otherwise the two records are linked by ALID (they were linked in this
// program at startup in DataSet::LinkCheckinDemands()
void CciDiagramViewer::MakeDemBkBarForCcaBkBar(CCI_LINEDATA *prpLine, CCI_BKBARDATA *prpCcaBkBar)
{
	if(prpLine != NULL && prpCcaBkBar != NULL)
	{
		CString olDesk = prpLine->Text;

		// create each personnel demand - link them to the location demand
		bool blDeskFound = false;
		CCSPtrArray <DEMANDDATA> olPersonnelDemands;
		ogDemandData.GetDemandsByFlurAndUrue(olPersonnelDemands,prpCcaBkBar->Uaft,prpCcaBkBar->Urue,"",ALLOCUNITTYPE_CIC,PERSONNELDEMANDS);
		int ilNumDems = olPersonnelDemands.GetSize(), ilDem;

		// search for a personnel demand with the same counter as the CCA
		// (there can be several personnel demands for a singe counter)
		for(ilDem = 0; ilDem < ilNumDems; ilDem++)
		{
			DEMANDDATA *prlDem = &olPersonnelDemands[ilDem];
			if(prlDem->Ulnk != 0L || prpCcaBkBar->Ulnk != 0L)
			{
				if(prpCcaBkBar->Ulnk == prlDem->Ulnk)
				{
					// direct link
					if(IsPassFilter(prlDem))
						MakeDemBkBar(prpLine, prlDem, prpCcaBkBar->OverlapLevel);
				}
			}
			else if(olDesk == prlDem->Alid)
			{
				// linked by Uaft, Urue and Alid
				if(IsPassFilter(prlDem))
					MakeDemBkBar(prpLine, prlDem);
			}
		}
	}
}

void CciDiagramViewer::MakeBars()
{
	// make bars for jobs
	JOBDATA *prlJob = NULL;
	int ilNumGroups = omGroups.GetSize();
	for(int ilGroup = 0; ilGroup < ilNumGroups; ilGroup++)
	{
		for(int ilSide = 0; ilSide < 2; ilSide++)
		{
			int ilNumLines = omGroups[ilGroup].Lines[ilSide].GetSize();
			for(int ilLine = 0; ilLine < ilNumLines; ilLine++)
			{
				if (ilGroup == imDemandsWithoutCounterGroup)	// "Without Group"
				{
					CCI_LINEDATA *prlLine = &omGroups[ilGroup].Lines[ilSide][ilLine];
					for (int ilDemBkBar = 0; ilDemBkBar < prlLine->DemBkBars.GetSize(); ilDemBkBar++)
					{
						CCI_BKBARDATA *prlBar = &prlLine->DemBkBars[ilDemBkBar];
						CCSPtrArray <JOBDATA> olJobs;
						ogJodData.GetJobsByDemand(olJobs,prlBar->Urno);
						int ilNumJobs = olJobs.GetSize();
						for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
						{
							if(IsPassFilter(&olJobs[ilJob]))
							{
								MakeBar(ilGroup, ilSide, ilLine, &olJobs[ilJob], prlBar->OverlapLevel);
							}
						}
					}
				}
				else
				{
					CCI_LINEDATA *prlLine = &omGroups[ilGroup].Lines[ilSide][ilLine];
					if(!prlLine->Text.IsEmpty())
					{
						CCSPtrArray <JOBDATA> olJobs;
						ogJobData.GetJobsByAlid(olJobs, prlLine->Text, ALLOCUNITTYPE_CIC);
						int ilNumJobs = olJobs.GetSize();
						for(int ilJob = 0; ilJob < ilNumJobs; ilJob++)
						{
							if(IsPassFilter(&olJobs[ilJob]))
							{
								MakeBar(ilGroup, ilSide, ilLine, &olJobs[ilJob]);
							}
						}
					}
				}
			}
		}
	}
}

void CciDiagramViewer::MakeLinesWhenGroupByTerminal(int ipGroupno)
{
//	int i;
	int ilDisplayPosition = 0, ilLeftCount = 0, ilRightCount = 0;

	// Create 40 GanttLines in advanced
	CCI_LINEDATA rlLine;	// default no text, overlaplevel = 0
	rlLine.Text = "";

	CString olGroupName = GetGroup(ipGroupno)->GroupId;

	// if blListAcross is true the create lines so:
	//	123	-->	124
	//	125	-->	126
	//	127	-->	128
	// etc
	// if false then:
	//	123	 |	126
	//	124	 |	127
	//	125  V	128
	// etc
	bool blListAcross = false;
	int ilNumCics;

	// Create desks from this found terminal
	CCSPtrArray <CICDATA> olCics;
	CCSPtrArray <ALLOCUNIT> olAllocUnitCics;

	// counters grouped according to the static group
	ogAllocData.GetUnitsByGroup(ALLOCUNITTYPE_CICGROUP,olGroupName,olAllocUnitCics);
	olAllocUnitCics.Sort(ByName);
	ilNumCics = olAllocUnitCics.GetSize();

	CICDATA *prlCicDesk = NULL;
	int ilHalfwayCount  = ilNumCics/2;
	if (ilNumCics % 2)
		++ilHalfwayCount;

	for(int ilCic = 0; ilCic < ilNumCics; ilCic++)
	{
		prlCicDesk = ogCicData.GetCicByUrno(olAllocUnitCics[ilCic].Urno);

		if(prlCicDesk != NULL)
		{
			CString olDeskType = ogBasicData.GetDeskType(prlCicDesk->Cnam);

			if (!IsPassFilter(olGroupName, prlCicDesk->Hall,prlCicDesk->Region,prlCicDesk->Line))
				continue;

			CCI_LINEDATA *prlLine = NULL;
			if (blListAcross)	//	Horizontal
			{
				if (!(ilDisplayPosition % 2))
				{
					if(omGroups[ipGroupno].Lines[0].GetSize() <= ilLeftCount)
					{
						rlLine.Text = "";
						CreateLine(ipGroupno, &rlLine);
					}
					prlLine = GetLine(ipGroupno, 0, ilLeftCount++);
				}
				else
				{
					if(omGroups[ipGroupno].Lines[1].GetSize() <= ilRightCount)
					{
						rlLine.Text = "";
						CreateLine(ipGroupno, &rlLine);
					}
					prlLine = GetLine(ipGroupno, 1, ilRightCount++);
				}
				++ilDisplayPosition;
			}
			else				//	Vertical
			{
				if (ilDisplayPosition < ilHalfwayCount)	// left side
				{
					if(omGroups[ipGroupno].Lines[0].GetSize() <= ilLeftCount)
					{
						rlLine.Text = "";
						CreateLine(ipGroupno, &rlLine);
					}
					prlLine = GetLine(ipGroupno, 0, ilLeftCount++);
				}
				else	// right side
				{
					if(omGroups[ipGroupno].Lines[1].GetSize() <= ilRightCount)
					{
						rlLine.Text = "";
						CreateLine(ipGroupno, &rlLine);
					}
					prlLine = GetLine(ipGroupno, 1, ilRightCount++);
				}
				++ilDisplayPosition;
			}

			if (prlLine != NULL)
			{
				prlLine->Text = prlCicDesk->Cnam;
				prlLine->GroupName = olGroupName;
				prlLine->Telno = prlCicDesk->Tele;
				prlLine->CicUrno = prlCicDesk->Urno;
				prlLine->StatusText = CString(prlCicDesk->Cnam);
			}
		}
    }


	// remove blank lines on the top/bottom
	int ilLineCount;
	while ((ilLineCount = GetLineCount(ipGroupno)) > 0 &&
		GetLineText(ipGroupno,0,0) == "" && GetLineText(ipGroupno,1,0) == "")
		DeleteLine(ipGroupno,0);
	while ((ilLineCount = GetLineCount(ipGroupno)) > 0 &&
		GetLineText(ipGroupno,0,ilLineCount-1) == "" &&
		GetLineText(ipGroupno,1,ilLineCount-1) == "")
		DeleteLine(ipGroupno,ilLineCount-1);
}

void CciDiagramViewer::MakeLinesWhenGroupByHall(int ipGroupno)
{
//	int i;
	int ilDisplayPosition = 0, ilLeftCount = 0, ilRightCount = 0;

	// Create 40 GanttLines in advanced
	CCI_LINEDATA rlLine;	// default no text, overlaplevel = 0
	rlLine.Text = "";

	CString olGroupName = GetGroup(ipGroupno)->GroupId;

	// if blListAcross is true the create lines so:
	//	123	-->	124
	//	125	-->	126
	//	127	-->	128
	// etc
	// if false then:
	//	123	 |	126
	//	124	 |	127
	//	125  V	128
	// etc
	bool blListAcross = false;
	int ilNumCics;

	// Create desks from this found terminal
	CCSPtrArray <CICDATA> olCics;
	CCSPtrArray <ALLOCUNIT> olAllocUnitCics;

	// counters grouped by the hall defined in CICTAB
	ogCicData.GetCicsByHall(olCics, olGroupName);
	olCics.Sort(ByName);
	ilNumCics = olCics.GetSize();

	CICDATA *prlCicDesk = NULL;
	int ilHalfwayCount = ilNumCics / 2;
	if (ilHalfwayCount % 2)
		++ilHalfwayCount;

	for(int ilCic = 0; ilCic < ilNumCics; ilCic++)
	{
		prlCicDesk = &olCics[ilCic];

		if(prlCicDesk != NULL)
		{
			CString olDeskType = ogBasicData.GetDeskType(prlCicDesk->Cnam);

			if (!IsPassFilter(olGroupName,prlCicDesk->Hall,prlCicDesk->Region,prlCicDesk->Line))
				continue;

			CCI_LINEDATA *prlLine = NULL;
			if (blListAcross)	//	Horizontal
			{
				if (!(ilDisplayPosition % 2))
				{
					if(omGroups[ipGroupno].Lines[0].GetSize() <= ilLeftCount)
					{
						rlLine.Text = "";
						CreateLine(ipGroupno, &rlLine);
					}
					prlLine = GetLine(ipGroupno, 0, ilLeftCount++);
				}
				else
				{
					if(omGroups[ipGroupno].Lines[1].GetSize() <= ilRightCount)
					{
						rlLine.Text = "";
						CreateLine(ipGroupno, &rlLine);
					}
					prlLine = GetLine(ipGroupno, 1, ilRightCount++);
				}
				++ilDisplayPosition;
			}
			else				//	Vertical
			{
				if (ilDisplayPosition < ilHalfwayCount)	// left side
				{
					if(omGroups[ipGroupno].Lines[0].GetSize() <= ilLeftCount)
					{
						rlLine.Text = "";
						CreateLine(ipGroupno, &rlLine);
					}
					prlLine = GetLine(ipGroupno, 0, ilLeftCount++);
				}
				else	// right side
				{
					if(omGroups[ipGroupno].Lines[1].GetSize() <= ilRightCount)
					{
						rlLine.Text = "";
						CreateLine(ipGroupno, &rlLine);
					}
					prlLine = GetLine(ipGroupno, 1, ilRightCount++);
				}
				++ilDisplayPosition;
			}

			if (prlLine != NULL)
			{
				prlLine->Text = prlCicDesk->Cnam;
				prlLine->GroupName = olGroupName;
				prlLine->Telno = prlCicDesk->Tele;
				prlLine->CicUrno = prlCicDesk->Urno;
				prlLine->StatusText = CString(prlCicDesk->Cnam);
			}
		}
    }


	// remove blank lines on the top/bottom
	int ilLineCount;
	while ((ilLineCount = GetLineCount(ipGroupno)) > 0 &&
		GetLineText(ipGroupno,0,0) == "" && GetLineText(ipGroupno,1,0) == "")
		DeleteLine(ipGroupno,0);
	while ((ilLineCount = GetLineCount(ipGroupno)) > 0 &&
		GetLineText(ipGroupno,0,ilLineCount-1) == "" &&
		GetLineText(ipGroupno,1,ilLineCount-1) == "")
		DeleteLine(ipGroupno,ilLineCount-1);
}

void CciDiagramViewer::MakeLinesWhenGroupByRegion(int ipGroupno)
{
	// Line is a string
	CString olGroupName = GetGroup(ipGroupno)->GroupId;

	CCSPtrArray <CICDATA> olCics;
	ogCicData.GetCicsByRegion(olCics,olGroupName);
	olCics.Sort(ByName);

	int ilNumCics = olCics.GetSize();
	for(int ilCic = 0; ilCic < ilNumCics; ilCic++)
	{
		CICDATA *prlCicDesk = ogCicData.GetCicByUrno(olCics[ilCic].Urno);
		if(prlCicDesk != NULL)
		{
			CString olDeskType = ogBasicData.GetDeskType(prlCicDesk->Cnam);
			if (!IsPassFilter(olGroupName, prlCicDesk->Hall,prlCicDesk->Region,prlCicDesk->Line))
				continue;

			CCI_LINEDATA rlLine;
			rlLine.CicUrno = prlCicDesk->Urno;
			rlLine.Text	= prlCicDesk->Cnam;
			rlLine.GroupName = olGroupName;
			rlLine.StatusText = CString(prlCicDesk->Cnam) + ", " + olGroupName;
			CreateLine(ipGroupno, &rlLine); 
		}
	}

	// Now for nice presentation, we will cut it out half and create the right side data
	int n = GetLineCount(ipGroupno);
	int ilRightSideStart = n / 2;
	if (n % 2)
	{
		++ilRightSideStart;
	}

	for (int i = 0; i+ilRightSideStart < n; i++)
	{
		GetLine(ipGroupno,1,i)->CicUrno		= GetLine(ipGroupno,0,ilRightSideStart)->CicUrno;
		GetLine(ipGroupno,1,i)->Text		= GetLine(ipGroupno,0,ilRightSideStart)->Text;
		GetLine(ipGroupno,1,i)->GroupName	= GetLine(ipGroupno,0,ilRightSideStart)->GroupName;
		GetLine(ipGroupno,1,i)->StatusText	= GetLine(ipGroupno,0,ilRightSideStart)->StatusText;
		DeleteLine(ipGroupno, ilRightSideStart);
	}

	if (n % 2)	// odd
	{
		CCI_LINEDATA *prlLine = GetLine(ipGroupno,1,n / 2);
		if (prlLine)
		{
			prlLine->CicUrno	= 0;
			prlLine->Text		= "";
			prlLine->GroupName	= "";
			prlLine->StatusText	= "";
		}
	}

}

void CciDiagramViewer::MakeLinesWhenGroupByLine(int ipGroupno)
{
	// Line is a string
	CString olGroupName = GetGroup(ipGroupno)->GroupId;

	CCSPtrArray <CICDATA> olCics;
	ogCicData.GetCicsByLine(olCics,olGroupName);
	olCics.Sort(ByName);

	int ilNumCics = olCics.GetSize();
	for(int ilCic = 0; ilCic < ilNumCics; ilCic++)
	{
		CICDATA *prlCicDesk = ogCicData.GetCicByUrno(olCics[ilCic].Urno);
		if(prlCicDesk != NULL)
		{
			CString olDeskType = ogBasicData.GetDeskType(prlCicDesk->Cnam);
			if (!IsPassFilter(olGroupName, prlCicDesk->Hall,prlCicDesk->Region,prlCicDesk->Line))
				continue;

			CCI_LINEDATA rlLine;
			rlLine.CicUrno = prlCicDesk->Urno;
			rlLine.Text	= prlCicDesk->Cnam;
			rlLine.GroupName = olGroupName;
			rlLine.StatusText = CString(prlCicDesk->Cnam) + ", " + olGroupName;
			CreateLine(ipGroupno, &rlLine); 
		}
	}

	// Now for nice presentation, we will cut it out half and create the right side data
	int n = GetLineCount(ipGroupno);
	int ilRightSideStart = n / 2;
	if (n % 2)
	{
		++ilRightSideStart;
	}

	for (int i = 0; i+ilRightSideStart < n; i++)
	{
		GetLine(ipGroupno,1,i)->CicUrno		= GetLine(ipGroupno,0,ilRightSideStart)->CicUrno;
		GetLine(ipGroupno,1,i)->Text		= GetLine(ipGroupno,0,ilRightSideStart)->Text;
		GetLine(ipGroupno,1,i)->GroupName	= GetLine(ipGroupno,0,ilRightSideStart)->GroupName;
		GetLine(ipGroupno,1,i)->StatusText	= GetLine(ipGroupno,0,ilRightSideStart)->StatusText;
		DeleteLine(ipGroupno, ilRightSideStart);
	}
 
	if (n % 2)	// odd
	{
		CCI_LINEDATA *prlLine = GetLine(ipGroupno,1,n / 2);
		if (prlLine)
		{
			prlLine->CicUrno	= 0;
			prlLine->Text		= "";
			prlLine->GroupName	= "";
			prlLine->StatusText	= "";
		}
	}
}

void CciDiagramViewer::MakeManager(int ipGroupno, JOBDATA *prpJob)
{
	if (!IsGrouppedByTerminal() ||
		!IsOverlapped(prpJob->Acfr, prpJob->Acto, omStartTime, omEndTime))
		return;

	CCI_MANAGERDATA rlFm;
	rlFm.JobUrno = prpJob->Urno;
	SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prpJob->Shur);
	rlFm.ShiftStid = prlShift != NULL ? prlShift->Sfca : "";
	rlFm.ShiftTmid = prlShift != NULL ? prlShift->Egrp : "";
	EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prpJob->Peno);
	rlFm.EmpLnam = CString(prlEmp != NULL ? prlEmp->Lanm : "");
	if (prlShift != NULL)
	{
		if ((prlShift->Avfa < prpJob->Acfr) || (prlShift->Avta > prpJob->Acto))
		{
			rlFm.ShiftStid += "'";
		}
	}

	CreateManager(ipGroupno, &rlFm);
}

void CciDiagramViewer::MakeBar(int ipGroupno, int ipSide, int ipLineno, JOBDATA *prpJob, int ipOverlapLevel /*=-1*/)
{
	if(prpJob != NULL && (strcmp(prpJob->Jtco,JOBFLIGHT) == 0 || strcmp(prpJob->Jtco,JOBCCC) == 0) || strcmp(prpJob->Jtco,JOBCCI) == 0 || strcmp(prpJob->Jtco,JOBFID) == 0)
	{
		long llFlightUrno = 0L;
		CString olText;

		FLIGHTDATA *prlFlight = ogBasicData.GetFlightForJob(prpJob);
		if(prlFlight != NULL)
		{
			olText = CString(prlFlight->Fnum);
			llFlightUrno = prlFlight->Urno;
		}

		CCI_BARDATA rlBar;
		rlBar.JobUrno = prpJob->Urno;
		rlBar.FlightUrno = llFlightUrno;
		if (olText.IsEmpty())
			rlBar.Text = BarText(prpJob);
		else
			rlBar.Text = olText + ", " + BarText(prpJob);
		rlBar.StatusText = StatusText(prpJob);
		rlBar.StartTime = prpJob->Acfr;
		rlBar.EndTime = prpJob->Acto;
		rlBar.FrameType = FRAMERECT;
		rlBar.MarkerType = *prpJob->Stat == 'P' ? MARKLEFT : MARKFULL;
		rlBar.MarkerBrush = ogBrushs[ogConflicts.GetJobConflictColor(prpJob)];
		rlBar.FrameColor = (*prpJob->Ignr == '1') ? WHITE : BLACK;
		rlBar.DifferentFunction = ogBasicData.JobAndDemHaveDiffFuncs(prpJob);

		if(prpJob->Stat[0] == 'P' && prpJob->Infm)
			rlBar.Infm = true;
		else
			rlBar.Infm = false;
		
		rlBar.OfflineBar = (!bgOnline && (ogJobData.JobChangedOffline(prpJob) || ogJodData.JodChangedOffline(prpJob->Urno))) ? true : false;
		if(ipOverlapLevel != -1)
		{
			rlBar.OverlapLevel = ipOverlapLevel;
		}
		else
		{
			CCI_LINEDATA *prlLine = GetLine(ipGroupno, ipSide, ipLineno);
			DEMANDDATA *prlDemand = ogJodData.GetDemandForJob(prpJob);
			if(prlLine != NULL && prlDemand != NULL)
			{
				int ilNumDemBkBars = prlLine->DemBkBars.GetSize();
				for(int ilD = 0; ilD < ilNumDemBkBars; ilD++)
				{
					if(prlLine->DemBkBars[ilD].Urno == prlDemand->Urno)
					{
						rlBar.OverlapLevel = prlLine->DemBkBars[ilD].OverlapLevel;
						break;
					}
				}
			}
		}

		int ilBar = CreateBar(ipGroupno, ipSide, ipLineno, &rlBar);

		// We also have to create an indicator for showing the range of time on TimeScale
		CCI_INDICATORDATA rlIndicator;
		rlIndicator.StartTime = rlBar.StartTime;
		rlIndicator.EndTime = rlBar.EndTime;
		rlIndicator.Color = ogColors[ogConflicts.GetJobConflictColor(prpJob)];
		CreateIndicator(ipGroupno, ipSide, ipLineno, ilBar, &rlIndicator);
	}
}

CCI_BKBARDATA *CciDiagramViewer::MakeCcaBkBar(int ipGroupno, int ipSide, int ipLineno, CCADATA *prpCca)
{
	CCI_BKBARDATA *prlBkBar = NULL;
	CCI_LINEDATA *prlLine = GetLine(ipGroupno, ipSide, ipLineno);
	if(prlLine != NULL)
	{
		prlBkBar = MakeCcaBkBar(prlLine, prpCca);
	}
	return prlBkBar;
}

CCI_BKBARDATA *CciDiagramViewer::MakeCcaBkBar(CCI_LINEDATA *prpLine, CCADATA *prlCca)
{
	CCI_BKBARDATA *prlBkBar = NULL;
	if (prlCca->IsCommonCheckin())
	{
		prlBkBar = new CCI_BKBARDATA;
		prlBkBar->Type = CCI_BKBARDATA::COMMON;
		prlBkBar->Text = "CCI";
		if (prlCca->Uaft)
		{
			ALTDATA *prlAlt = ogAltData.GetAltByUrno(prlCca->Ualt);
			if (prlAlt)
				prlBkBar->Text.Format("CCI %s",prlAlt->Alc2);
		}

		prlBkBar->StatusText.Format("%s - %s %s", ogBasicData.FormatDate(prlCca->Ckba), ogBasicData.FormatDate(prlCca->Ckea), GetRuleEventText(prlCca->Urue));
		prlBkBar->Uaft = prlCca->Uaft;
		prlBkBar->Ualt = prlCca->Ualt;
		prlBkBar->Urno = prlCca->Urno;
		prlBkBar->Urue = prlCca->Urue;
		prlBkBar->Ulnk = 0;
		prlBkBar->StartTime = prlCca->Ckba;
		prlBkBar->EndTime = prlCca->Ckea;
		prlBkBar->MarkerBrush = &omCommonCcaBkBrush;
		GetOverlapLevelForBkBar(prpLine, prlBkBar);
		prpLine->CcaBkBars.Add(prlBkBar);
	}
	else
	{
		FLIGHTDATA *prlFlight = ogFlightData.GetFlightByUrno(prlCca->Uaft);
		if(prlFlight == NULL)
		{
			//ogBasicData.Trace("CciDiagramViewer::MakeCcaBkBar() ERROR CCA.FLNU <%ld> not found in flight data CCA.URNO=<%ld>\n",prlCca->Uaft,prlCca->Urno);
		}
		else
		{
			prlBkBar = new CCI_BKBARDATA;
			prlBkBar->Type = CCI_BKBARDATA::DEDICATED;
			prlBkBar->Text.Format("%s",prlFlight->Fnum);
			prlBkBar->StatusText.Format("%s - %s %s %s", ogBasicData.FormatDate(prlCca->Ckba), ogBasicData.FormatDate(prlCca->Ckea), prlFlight->Fnum, GetRuleEventText(prlCca->Urue));
			prlBkBar->Uaft = prlCca->Uaft;
			prlBkBar->Ualt = prlCca->Ualt;
			prlBkBar->Urno = prlCca->Urno;
			prlBkBar->Urue = prlCca->Urue;
			DEMANDDATA *prlLocationDemand = ogDemandData.GetDemandByUrno(prlCca->Udem);
			if(prlLocationDemand == NULL)
			{
				//ogBasicData.Trace("CciDiagramViewer::MakeCcaBkBar() CCA record has invalid pointer to a demand! COUNTER <%s> FROM <%s> TO <%s> CCA.URNO <%ld> DEM.URNO <%ld>\n",prlCca->Cnam,prlCca->Ckbs.Format("%H:%M/%d"),prlCca->Ckes.Format("%H:%M/%d"),prlCca->Urno,prlCca->Udem);
				prlBkBar->Ulnk = 0;
			}
			else
			{
				prlBkBar->Ulnk = prlLocationDemand->Ulnk;
			}

			prlBkBar->StartTime = prlCca->Ckba;
			prlBkBar->EndTime = prlCca->Ckea;
			prlBkBar->MarkerBrush = &omDedicatedCcaBkBrush;
			GetOverlapLevelForBkBar(prpLine, prlBkBar);
			prpLine->CcaBkBars.Add(prlBkBar);
		}
	}
	return prlBkBar;
}

void CciDiagramViewer::GetOverlapLevelForBkBar(CCI_LINEDATA *prpLine, CCI_BKBARDATA *prpNewBkBar)
{
	prpNewBkBar->OverlapLevel = 0;

	bool blFreeLineNotFound = true;
	int ilLine = 0;
	while(blFreeLineNotFound)
	{
		if(!HasOverlappedBkBars(prpLine, prpNewBkBar->StartTime-om2Mins, prpNewBkBar->EndTime+om2Mins, ilLine))
		{
			blFreeLineNotFound = false;
			prpNewBkBar->OverlapLevel = ilLine;
		}
		else if(ilLine >= 10)
		{
			// any more than 10 bars per line cause an error when adding a string to the list box
			blFreeLineNotFound = false;
			prpNewBkBar->OverlapLevel = 10;
			prpNewBkBar->FrameColor = YELLOW;
		}
		else
		{
			ilLine++;
		}
	}
	prpLine->MaxOverlapLevel = max(prpLine->MaxOverlapLevel, prpNewBkBar->OverlapLevel);
}

bool CciDiagramViewer::HasOverlappedBkBars(CCI_LINEDATA *prpLine, CTime opStartTime, CTime opEndTime, int ipLine)
{
	int ilNumBkBars = prpLine->CcaBkBars.GetSize(), ilBkBar;
	for(ilBkBar = 0; ilBkBar < ilNumBkBars; ilBkBar++)
	{
		if(prpLine->CcaBkBars[ilBkBar].OverlapLevel == ipLine &&
			IsOverlapped(opStartTime, opEndTime, prpLine->CcaBkBars[ilBkBar].StartTime, prpLine->CcaBkBars[ilBkBar].EndTime))
		{
			return true;
		}
	}

	ilNumBkBars = prpLine->DemBkBars.GetSize();
	for(ilBkBar = 0; ilBkBar < ilNumBkBars; ilBkBar++)
	{
		if(prpLine->DemBkBars[ilBkBar].OverlapLevel == ipLine &&
			IsReallyOverlapped(opStartTime, opEndTime, prpLine->DemBkBars[ilBkBar].StartTime, prpLine->DemBkBars[ilBkBar].EndTime))
		{
			return true;
		}
	}

	return false;
}

CCI_BKBARDATA *CciDiagramViewer::MakeDemBkBar(int ipGroupno, int ipSide, int ipLineno, DEMANDDATA *prpDem, int ipOverlapLevel /*=-1*/)
{
	CCI_BKBARDATA *prlBkBar = NULL;
	CCI_LINEDATA *prlLine = GetLine(ipGroupno, ipSide, ipLineno);
	if(prlLine != NULL)
	{
		prlBkBar = MakeDemBkBar(prlLine, prpDem, ipOverlapLevel);
	}
	return prlBkBar;
}

CCI_BKBARDATA *CciDiagramViewer::MakeDemBkBar(CCI_LINEDATA *prpLine, DEMANDDATA *prpDem, int ipOverlapLevel /*=-1*/)
{
	CCI_BKBARDATA *prlBkBar = new CCI_BKBARDATA;

	CString olText, olStatusText;
	long llFlightUrno = 0L;
	CString olDetyString = ogDemandData.GetStringForDemandType(prpDem->Dety);

	// common checkin demand ?
	if (ogDemandData.IsFlightDependingDemand(prpDem))
	{
		FLIGHTDATA *prlFlight = ogDataSet.GetFlightByDemand(prpDem);
		if(prlFlight != NULL)
		{
			olText = CString(prlFlight->Fnum);
			llFlightUrno = prlFlight->Urno;
		}
		else
		{
			if(ogDemandData.IsFlightDependingDemand(prpDem))
			{
				//ogBasicData.Trace("CciDiagramViewer::MakeDemandBar() Flight not found for demand URNO <%ld> OURI <%ld> OURO <%ld> (%s flight)\n",prpDem->Urno,prpDem->Ouri,prpDem->Ouro,*prpDem->Dety == '1' ? "Inbound" : "Outbound");
				olText = GetString(IDS_STRING61387); // Flight not found
			}
			else
			{
				olText = olDetyString;
			}
		}
	}
	else
	{
		olText = olDetyString;
	}

	olText += CString(" ") + ogBasicData.GetDemandBarText(prpDem);
	CString olTypeText = GetString(IDS_STRING61984);
	if(*prpDem->Dety == '6')
	{
		RUEDATA *prlRue = ogRueData.GetRueByUrno(prpDem->Urue);
		olText.Format("%s: %s", olDetyString, (prlRue != NULL) ? prlRue->Rusn : "");
	}
	else if(*prpDem->Dety == '7')
	{
		CString olService = "";
		RUDDATA* prlRud = ogRudData.GetRudByUrno(prpDem->Urud);
		if(prlRud != NULL)
		{
			SERDATA* prlSer = ogSerData.GetSerByUrno(prlRud->Ughs);
			if(prlSer != NULL)
			{
				olService = prlSer->Seco;
			}
		}
		CString olFunction = "";
		RPFDATA *prlDemFct = ogRpfData.GetFunctionByUrud(prpDem->Urud);
		if (prlDemFct)
		{
			if (strcmp(prlDemFct->Fcco,"") == 0)
			{
				olFunction = "";
				// group of optional functions
				CCSPtrArray <SGMDATA> olSgms;
				ogSgmData.GetSgmListByUsgr(prlDemFct->Upfc,olSgms);
				int ilNumSgms = olSgms.GetSize();
				for(int ilSgm = 0; ilSgm < ilNumSgms; ilSgm++)
				{
					PFCDATA *prlPfc = ogPfcData.GetPfcByUrno(olSgms[ilSgm].Uval);
					if(prlPfc != NULL)
					{
						if (!olFunction.IsEmpty())
							olFunction += ',';
						olFunction += prlPfc->Fctc;
					}
				}

			}
			else
			{
				olFunction = prlDemFct->Fcco;
			}
		}
		olText = ogFieldConfig.ConfigureField(IDS_FIELDCONFIG_CHECKINFIDBAR, "%s%s%s", olDetyString, olService, olFunction);
		if(olText.IsEmpty()) olText.Format("FID: %s", olService);
	}

	olStatusText.Format("%s - %s  %s %s", ogBasicData.FormatDate(prpDem->Debe), ogBasicData.FormatDate(prpDem->Deen), olText, GetRuleEventText(prpDem->Urue));
	
	prlBkBar->Type = CCI_BKBARDATA::PERSONNEL;
	prlBkBar->Text = olText;
	prlBkBar->StatusText = olStatusText;
	prlBkBar->Uaft = llFlightUrno;
	prlBkBar->Urno = prpDem->Urno;
	prlBkBar->Urue = 0L;
	prlBkBar->StartTime = prpDem->Debe;
	prlBkBar->EndTime = prpDem->Deen;
	prlBkBar->MarkerBrush = &omDemandBrush;
	strcpy(prlBkBar->Dety,prpDem->Dety);
	if(ogDemandData.HasExpired(prpDem))
		prlBkBar->FrameColor = RED;
	else if(ogDemandData.DemandIsDeactivated(prpDem))
		prlBkBar->FrameColor = BLUE;
	if(ipOverlapLevel != -1)
	{
		// position of corresponding CCA bar defined in call to function
		prlBkBar->OverlapLevel = ipOverlapLevel;
	}
	else
	{
		CCI_BKBARDATA *prlCcaBkBar = FindCcaBkBarForDemBkBar(prpLine, prlBkBar);
		if(prlCcaBkBar != NULL)
		{
			if(prlCcaBkBar->NumBars <= 0)
			{
				// found corresponding CCA bar for the demand
				prlBkBar->OverlapLevel = prlCcaBkBar->OverlapLevel;
			}
			else
			{
				GetOverlapLevelForBkBar(prpLine, prlBkBar);
			}
			prlCcaBkBar->NumBars++;
		}
		else
		{
			// no corresponding CCA bar found so find the next free space
			GetOverlapLevelForBkBar(prpLine, prlBkBar);
		}
	}

	prpLine->DemBkBars.Add(prlBkBar);

	return prlBkBar;
}

CCI_BKBARDATA *CciDiagramViewer::FindCcaBkBarForDemBkBar(CCI_LINEDATA *prpLine, CCI_BKBARDATA *prpDemBkBar)
{
	CCI_BKBARDATA *prlFoundCcaBkBar = NULL;
	int ilNumCcaBkBars = prpLine->CcaBkBars.GetSize();
	for(int ilB = 0; ilB < ilNumCcaBkBars; ilB++)
	{
		CCI_BKBARDATA *prlCcaBkBar = &prpLine->CcaBkBars[ilB];
		if(prlCcaBkBar->Ulnk == prpDemBkBar->Ulnk ||
			(prlCcaBkBar->Uaft == prpDemBkBar->Uaft && prlCcaBkBar->Urue == prpDemBkBar->Urue))
		{
			prlFoundCcaBkBar = prlCcaBkBar;
			break;
		}
	}

	return prlFoundCcaBkBar;
}

void CciDiagramViewer::MakeBlkBkBar(CCI_LINEDATA *prpLine, BLKDATA *prpBlk)
{
	if (!prpLine || !prpBlk)
		return;

	CCSPtrArray<BLKBLOCKEDTIMES> olBlockedTimes;
	if (ogBlkData.GetBlockedTimes(prpBlk,ogBasicData.GetTimeframeStart(),ogBasicData.GetTimeframeEnd(),olBlockedTimes) > 0)
	{
		for (int ilC = 0; ilC < olBlockedTimes.GetSize(); ilC++)
		{
			MakeBlkBkBar(prpLine,olBlockedTimes[ilC].Tifr,olBlockedTimes[ilC].Tito,prpBlk->Resn);
		}

		olBlockedTimes.DeleteAll();
	}

}

CCI_BKBARDATA* CciDiagramViewer::MakeBlkBkBar(CCI_LINEDATA *prpLine,const CTime& ropFrom,const CTime& ropTo,const CString& ropReason)
{
	CCI_BKBARDATA *prlBkBar = new CCI_BKBARDATA;
	long llFlightUrno = 0L;
	CString olText, olStatusText;

	olText = ropReason;
	olStatusText.Format("%s - %s  %s",ropFrom.Format("%d/%H%M"),ropTo.Format("%d/%H%M"),ropReason);

	prlBkBar->Type = CCI_BKBARDATA::BLOCKED;
	prlBkBar->Text = olText;
	prlBkBar->StatusText = olStatusText;
	prlBkBar->Uaft = 0L;
	prlBkBar->Urno = 0L;
	prlBkBar->Urue = 0L;
	prlBkBar->StartTime = ropFrom;
	prlBkBar->EndTime	= ropTo;
	prlBkBar->MarkerBrush = &omBlockedBrush;
	prpLine->BlkBkBars.Add(prlBkBar);

	return prlBkBar;
}


void CciDiagramViewer::MakeDemandBar(int ipGroupno, int ipSide, int ipLineno, DEMANDDATA *prpDemand)
{
	CString olText, olStatusText;
	long llFlightUrno = 0L;
	FLIGHTDATA *prlFlight = ogDataSet.GetFlightByDemand(prpDemand);
	if(prlFlight != NULL)
	{
		olText = CString(prlFlight->Fnum);
		llFlightUrno = prlFlight->Urno;
	}
	else
	{
		if(ogDemandData.IsFlightDependingDemand(prpDemand))
		{
			//ogBasicData.Trace("CciDiagramViewer::MakeDemandBar() Flight not found for demand URNO <%ld> OURI <%ld> OURO <%ld> (%s flight)\n",prpDemand->Urno,prpDemand->Ouri,prpDemand->Ouro,*prpDemand->Dety == '1' ? "Inbound" : "Outbound");
			olText = GetString(IDS_STRING61387); // Flight not found
		}
		else
		{
			olText = ogDemandData.GetStringForDemandType(prpDemand->Dety);
		}
	}
	olText += CString(" ") + ogBasicData.GetDemandBarText(prpDemand);
	olStatusText.Format("%s - %s  %s RUD.URNO <%ld>",prpDemand->Debe.Format("%d/%H%M"),prpDemand->Deen.Format("%d/%H%M"),olText,prpDemand->Urud);

	CCI_BARDATA rlBar;
	rlBar.JobUrno = prpDemand->Urno;
	rlBar.FlightUrno = llFlightUrno;
	rlBar.IsDemand = true;
    rlBar.Text = olText;
    rlBar.StatusText = olStatusText;
    rlBar.StartTime = prpDemand->Debe;
    rlBar.EndTime = prpDemand->Deen;
    rlBar.FrameType = FRAMERECT;
	//rlBar.MarkerType = *prpJob->Stat == 'P' ? MARKLEFT : MARKFULL;
	rlBar.MarkerType = MARKFULL;
	rlBar.MarkerBrush = &omDemandBrush;
    int ilBar = CreateBar(ipGroupno, ipSide, ipLineno, &rlBar);

	// We also have to create an indicator for showing the range of time on TimeScale
	CCI_INDICATORDATA rlIndicator;
	rlIndicator.StartTime = rlBar.StartTime;
	rlIndicator.EndTime = rlBar.EndTime;
	rlIndicator.Color = AQUA;
	CreateIndicator(ipGroupno, ipSide, ipLineno, ilBar, &rlIndicator);
}

BOOL CciDiagramViewer::FindGroup(const char *pcpGroupId, int &ripGroupno)
{
	for (ripGroupno = 0; ripGroupno < GetGroupCount(); ripGroupno++)
		if (GetGroup(ripGroupno)->Alid == pcpGroupId)
			return TRUE;

	return FALSE;
}

// this new version of FindCounter allows iteration because it is now
// possible to have a counter in more than group.  For this function to
// iterate properly, ripGroup/ripLine need to be initialized to -1
// - subsequent calls will then increment ripLineno.
bool CciDiagramViewer::FindCounter(CString opCounter, int &ripGroup, int &ripSide, int &ripLine)
{
	if(ripGroup == -1)
		ripGroup = 0;
	if(ripLine == -1)
		ripLine = 0;
	else
		ripLine++;

	for(; ripGroup < GetGroupCount(); ripGroup++)
	{
		for(; ripLine < GetLineCount(ripGroup); ripLine++)
		{
			if(GetLineText(ripGroup, ripSide = 0, ripLine) == opCounter || GetLineText(ripGroup, ripSide = 1, ripLine) == opCounter)
				return TRUE;
		}
		ripLine = 0;
	}

	return FALSE;
}

BOOL CciDiagramViewer::FindDesk(CString opDesk, int &rilGroup, int &rilSide, int &rilLine)
{
	for (rilGroup = 0; rilGroup < GetGroupCount(); rilGroup++)
		for (rilLine = 0; rilLine < GetLineCount(rilGroup); rilLine++)
			if (GetLineText(rilGroup, rilSide = 0, rilLine) == opDesk)
				return TRUE;
			else if (GetLineText(rilGroup, rilSide = 1, rilLine) == opDesk)
				return TRUE;

	return FALSE;
}

BOOL CciDiagramViewer::AssignDemToCca(DEMANDDATA *prpDemand, int &ripGroup, int &ripSide, int &ripLine)
{
	if(prpDemand != NULL)
	{
		long llUlnk = prpDemand->Ulnk;
		CString olAlid = prpDemand->Alid;
		long llUaft = ogDemandData.GetFlightUrno(prpDemand);
		long llUrue = ogBasicData.GetUrueForDemand(prpDemand);

		for (ripGroup = 0; ripGroup < GetGroupCount(); ripGroup++)
		{
			for (ripLine = 0; ripLine < GetLineCount(ripGroup); ripLine++)
			{
				for(ripSide = 0; ripSide < 2; ripSide++)
				{
					int ilBkBarCount = GetCcaBkBarCount(ripGroup, ripSide, ripLine);
					for (int ilBkBar = 0; ilBkBar < ilBkBarCount; ilBkBar++)
					{
						CCI_BKBARDATA *prlCcaBkBar = GetCcaBkBar(ripGroup, ripSide, ripLine, ilBkBar);
						if(llUlnk != 0L || prlCcaBkBar->Ulnk != 0L)
						{
							if(llUlnk = prlCcaBkBar->Ulnk)
							{
								// direct match found via ULNK
								if(IsPassFilter(prpDemand))
									MakeDemBkBar(ripGroup, ripSide, ripLine, prpDemand, prlCcaBkBar->OverlapLevel);
								return TRUE;
							}
						}
						else if(llUaft == prlCcaBkBar->Uaft && llUrue == prlCcaBkBar->Urue)
						{
							if(olAlid.IsEmpty())
							{
								// assign to first free counter
								if(IsPassFilter(prpDemand))
									MakeDemBkBar(ripGroup, ripSide, ripLine, prpDemand);
								return TRUE;
							}
							else if(olAlid == prlCcaBkBar->Text)
							{
								// direct match found via UAFT, URUE and ALID
								if(IsPassFilter(prpDemand))
									MakeDemBkBar(ripGroup, ripSide, ripLine, prpDemand, prlCcaBkBar->OverlapLevel);
								return TRUE;
							}
						}
					}
				}
			}
		}
	}
	return FALSE;
}

BOOL CciDiagramViewer::FindManager(long lpUrno, int &ripGroupno, int &ripManagerno)
{
	for (ripGroupno = 0; ripGroupno < GetGroupCount(); ripGroupno++)
		for (ripManagerno = 0; ripManagerno < GetManagerCount(ripGroupno); ripManagerno++)
			if (GetManager(ripGroupno, ripManagerno)->JobUrno == lpUrno)
				return TRUE;

	return FALSE;
}

// Searching the bar which specified by "lpUrno"
BOOL CciDiagramViewer::FindBar(long lpUrno, int &ripGroupno, int &ripSide, int &ripLineno, int &ripBarno)
{
	if(ripGroupno == -1)
		ripGroupno = 0;
	if(ripLineno == -1)
		ripLineno = 0;
	else
		ripLineno++;

	for(; ripGroupno < GetGroupCount(); ripGroupno++)
	{
		for(; ripLineno < GetLineCount(ripGroupno); ripLineno++)
		{
			for(ripSide = 0; ripSide < 2; ripSide++)
			{
				for(ripBarno = 0; ripBarno < GetBarCount(ripGroupno, ripSide, ripLineno); ripBarno++)
				{
					if(GetBar(ripGroupno, ripSide, ripLineno, ripBarno)->JobUrno == lpUrno)
						return TRUE;
				}
			}
		}
		ripLineno = 0;
	}

	return FALSE;
}

BOOL CciDiagramViewer::FindCcaBkBar(long lpUrno, int &ripGroupno, int &ripSide, int &ripLineno, int &ripBarno)
{
	if(ripGroupno == -1)
		ripGroupno = 0;
	if(ripLineno == -1)
		ripLineno = 0;
	else
		ripLineno++;

	for(; ripGroupno < GetGroupCount(); ripGroupno++)
	{
		for(; ripLineno < GetLineCount(ripGroupno); ripLineno++)
		{
			for(ripSide = 0; ripSide < 2; ripSide++)
			{
				for(ripBarno = 0; ripBarno < GetCcaBkBarCount(ripGroupno, ripSide, ripLineno); ripBarno++)
				{
					if (GetCcaBkBar(ripGroupno, ripSide, ripLineno, ripBarno)->Urno == lpUrno)
						return TRUE;
				}
			}
		}
		ripLineno = 0;
	}

	return FALSE;
}

BOOL CciDiagramViewer::FindDemBkBar(long lpUrno, int &ripGroupno, int &ripSide, int &ripLineno, int &ripBarno)
{
	if(ripGroupno == -1)
		ripGroupno = 0;
	if(ripLineno == -1)
		ripLineno = 0;
	else
		ripLineno++;

	for (; ripGroupno < GetGroupCount(); ripGroupno++)
	{
		for (; ripLineno < GetLineCount(ripGroupno); ripLineno++)
		{
			for (ripSide = 0; ripSide < 2; ripSide++)
			{
				for (ripBarno = 0; ripBarno < GetDemBkBarCount(ripGroupno, ripSide, ripLineno); ripBarno++)
				{
					if (GetDemBkBar(ripGroupno, ripSide, ripLineno, ripBarno)->Urno == lpUrno)
						return TRUE;
				}
			}
		}
		ripLineno = 0;
	}

	return FALSE;
}


CString CciDiagramViewer::BarText(JOBDATA *prpJob)
{
	CString olText;
	if(prpJob != NULL)
	{
		EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prpJob->Peno);
		if(prlEmp != NULL)
		{
			olText = ogEmpData.GetEmpName(prlEmp);
		}
		else
		{
			olText = GetString(IDS_STRING61335); // Employee not found
		}
	}
	return olText;
}

CString CciDiagramViewer::StatusText(JOBDATA *prpJob)
{
	char pclText[512];
	SHIFTDATA *prlShift = NULL;
	EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prpJob->Peno);
		JOBDATA *prlPoolJob = ogJobData.GetJobByUrno(prpJob->Jour);
	if (prlPoolJob != NULL)
		prlShift = ogShiftData.GetShiftByUrno(prlPoolJob->Shur);
	sprintf(pclText, GetString(IDS_STRING61336),
			prlShift != NULL ? prlShift->Egrp : "",
			prlShift != NULL ? prlShift->Sfca : "",
			prlShift != NULL ? prlShift->Avfa.Format("%H%M") : "",
			prlShift != NULL ? prlShift->Avta.Format("%H%M") : "",
			ogEmpData.GetEmpName(prlEmp),
			prpJob->Acfr.Format("%H%M"),prpJob->Acto.Format("%H%M"));
	return pclText;
}


/////////////////////////////////////////////////////////////////////////////
// CciDiagramViewer operations

int CciDiagramViewer::GetGroupCount()
{
    return omGroups.GetSize();
}

CCI_GROUPDATA *CciDiagramViewer::GetGroup(int ipGroupno)
{
    //return (CCI_GROUPDATA *)omGroups[ipGroupno];
    return &omGroups[ipGroupno];
}

CString CciDiagramViewer::GetGroupText(int ipGroupno)
{
    //return ((CCI_GROUPDATA *)omGroups[ipGroupno])->Text;
    return omGroups[ipGroupno].Text;
}

CString CciDiagramViewer::GetGroupTopScaleText(int ipGroupno)
{
	CString s;
	for (int i = 0; i < GetManagerCount(ipGroupno); i++)
	{
		CCI_MANAGERDATA *prlManager = GetManager(ipGroupno, i);
		s += s.IsEmpty()? "": " / ";
		s += CString(prlManager->EmpLnam) + " " + prlManager->ShiftTmid + " " + prlManager->ShiftStid;
	}
	return s;
}

int CciDiagramViewer::GetManagerCount(int ipGroupno)
{
    return omGroups[ipGroupno].Managers.GetSize();
}

CCI_MANAGERDATA *CciDiagramViewer::GetManager(int ipGroupno, int ipManagerno)
{
    return &omGroups[ipGroupno].Managers[ipManagerno];
}


int CciDiagramViewer::GetGroupColorIndex(int ipGroupno,CTime opStart,CTime opEnd)
{
	int ilColorIndex = FIRSTCONFLICTCOLOR+(CFI_NOCONFLICT*2);
	CBrush *prlBrush = ogBrushs[ilColorIndex];
	int ilLineCount = GetLineCount(ipGroupno);
	for (int ilLine = 0; ilLine < ilLineCount; ilLine++)
	{
		for ( int ilSide = 0; ilSide < 2; ilSide++)
		{
			for (int ilBar = 0; ilBar < GetBarCount(ipGroupno,ilSide, ilLine); ilBar++)
			{
				CCI_BARDATA *prlBar;
				if ((prlBar = GetBar(ipGroupno, ilSide, ilLine, ilBar)) != NULL)
				{
					if (IsOverlapped(opStart, opEnd, prlBar->StartTime, prlBar->EndTime))
					{
						JOBDATA *prlJob = ogJobData.GetJobByUrno(prlBar->JobUrno);
						if (prlJob != NULL)
						{
							if ((prlJob->ConflictType !=  CFI_NOCONFLICT) && (!prlJob->ConflictConfirmed))
							{
								return 4; // RED
								//return FIRSTCONFLICTCOLOR+(CFI_NOTCOVERED*2);
							}
						}
					}
				}
			}
		}
	}
	return ilColorIndex;
}

int CciDiagramViewer::GetLineCount(int ipGroupno)
{
    //return ((CCI_GROUPDATA *)omGroups[ipGroupno])->Lines[0].GetSize();
    return omGroups[ipGroupno].Lines[0].GetSize();
}

CCI_LINEDATA *CciDiagramViewer::GetLine(int ipGroupno, int ipSide, int ipLineno)
{
    //return (CCI_LINEDATA *)((CCI_GROUPDATA *)omGroups[ipGroupno])->Lines[ipSide][ipLineno];
    return &omGroups[ipGroupno].Lines[ipSide][ipLineno];
}

CString CciDiagramViewer::GetLineText(int ipGroupno, int ipSide, int ipLineno)
{
    //return ((CCI_LINEDATA *)((CCI_GROUPDATA *)omGroups[ipGroupno])->Lines[ipSide][ipLineno])->Text;
    return omGroups[ipGroupno].Lines[ipSide][ipLineno].Text;
}

int CciDiagramViewer::GetMaxOverlapLevel(int ipGroupno, int ipSide, int ipLineno)
{
    //return ((CCI_LINEDATA *)((CCI_GROUPDATA *)omGroups[ipGroupno])->Lines[ipSide][ipLineno])->MaxOverlapLevel;
    return omGroups[ipGroupno].Lines[ipSide][ipLineno].MaxOverlapLevel;
}

int CciDiagramViewer::GetBarCount(int ipGroupno, int ipSide, int ipLineno)
{
    //return ((CCI_LINEDATA *)((CCI_GROUPDATA *)omGroups[ipGroupno])->Lines[ipSide][ipLineno])->Bars.GetSize();
    return omGroups[ipGroupno].Lines[ipSide][ipLineno].Bars.GetSize();
}

CCI_BARDATA *CciDiagramViewer::GetBar(int ipGroupno, int ipSide, int ipLineno, int ipBarno)
{
    //CCI_LINEDATA *prlLine = (CCI_LINEDATA *)((CCI_GROUPDATA *)omGroups[ipGroupno])->Lines[ipSide][ipLineno];
    //return (CCI_BARDATA *)prlLine->Bars[ipBarno];
    return &omGroups[ipGroupno].Lines[ipSide][ipLineno].Bars[ipBarno];
}

CString CciDiagramViewer::GetBarText(int ipGroupno, int ipSide, int ipLineno, int ipBarno)
{
    //CCI_LINEDATA *prlLine = (CCI_LINEDATA *)((CCI_GROUPDATA *)omGroups[ipGroupno])->Lines[ipSide][ipLineno];
    //return ((CCI_BARDATA *)prlLine->Bars[ipBarno])->Text;
    return omGroups[ipGroupno].Lines[ipSide][ipLineno].Bars[ipBarno].Text;
}

int CciDiagramViewer::GetBlkBkBarCount(int ipGroupno, int ipSide, int ipLineno)
{
    return omGroups[ipGroupno].Lines[ipSide][ipLineno].BlkBkBars.GetSize();
}

CCI_BKBARDATA *CciDiagramViewer::GetBlkBkBar(int ipGroupno, int ipSide, int ipLineno, int ipBkBarno)
{
	if(ipBkBarno < GetBlkBkBarCount(ipGroupno, ipSide, ipLineno))
	    return &omGroups[ipGroupno].Lines[ipSide][ipLineno].BlkBkBars[ipBkBarno];
	else
		return NULL;
}

int CciDiagramViewer::GetCcaBkBarCount(int ipGroupno, int ipSide, int ipLineno)
{
    return omGroups[ipGroupno].Lines[ipSide][ipLineno].CcaBkBars.GetSize();
}

CCI_BKBARDATA *CciDiagramViewer::GetCcaBkBar(int ipGroupno, int ipSide, int ipLineno, int ipBkBarno)
{
	if(ipBkBarno < GetCcaBkBarCount(ipGroupno, ipSide, ipLineno))
	    return &omGroups[ipGroupno].Lines[ipSide][ipLineno].CcaBkBars[ipBkBarno];
	else
		return NULL;
}

CString CciDiagramViewer::GetCcaBkBarText(int ipGroupno, int ipSide, int ipLineno, int ipBkBarno)
{
    return omGroups[ipGroupno].Lines[ipSide][ipLineno].CcaBkBars[ipBkBarno].Text;
}

int CciDiagramViewer::GetDemBkBarCount(int ipGroupno, int ipSide, int ipLineno)
{
    return omGroups[ipGroupno].Lines[ipSide][ipLineno].DemBkBars.GetSize();
}

CCI_BKBARDATA *CciDiagramViewer::GetDemBkBar(int ipGroupno, int ipSide, int ipLineno, int ipBkBarno)
{
	if(ipBkBarno < GetDemBkBarCount(ipGroupno, ipSide, ipLineno))
	    return &omGroups[ipGroupno].Lines[ipSide][ipLineno].DemBkBars[ipBkBarno];
	else
		return NULL;
}

CString CciDiagramViewer::GetDemBkBarText(int ipGroupno, int ipSide, int ipLineno, int ipBkBarno)
{
    return omGroups[ipGroupno].Lines[ipSide][ipLineno].DemBkBars[ipBkBarno].Text;
}

int CciDiagramViewer::GetIndicatorCount(int ipGroupno, int ipSide, int ipLineno, int ipBarno)
{
    //return ((CCI_BARDATA *)((CCI_LINEDATA *)((CCI_GROUPDATA *)omGroups[ipGroupno])->Lines[ipSide][ipLineno])->Bars[ipBarno])->Indicators.GetSize();
    return omGroups[ipGroupno].Lines[ipSide][ipLineno].Bars[ipBarno].Indicators.GetSize();
}

CCI_INDICATORDATA *CciDiagramViewer::GetIndicator(int ipGroupno, int ipSide, int ipLineno, int ipBarno, int ipIndicatorno)
{
    //CCI_BARDATA *prlBar = (CCI_BARDATA *)((CCI_LINEDATA *)((CCI_GROUPDATA *)omGroups[ipGroupno])->Lines[ipSide][ipLineno])->Bars[ipBarno];
    //return (CCI_INDICATORDATA *)prlBar->Indicators[ipIndicatorno];
    return &omGroups[ipGroupno].Lines[ipSide][ipLineno].Bars[ipBarno].Indicators[ipIndicatorno];
}


/////////////////////////////////////////////////////////////////////////////
// CciDiagramViewer - CCI_BARDATA array maintenance (overlapping version)

void CciDiagramViewer::DeleteAll()
{
    while (GetGroupCount() > 0)
		DeleteGroup(0);
}



int CciDiagramViewer::CreateGroup(CCI_GROUPDATA *prpGroup)
{
	// Error on adding a group for terminal "B Sonst."
//	if (omGroupBy == GROUP_BY_TERMINAL && prpGroup->GroupId[3] == 'X' ||	// B Sonst.?
//		omGroupBy == GROUP_BY_DESKTYPE && prpGroup->GroupId[0] == 'X')		// B Sonst.?
//		return -1;

    //int ilGroup = omGroups.Add(new CCI_GROUPDATA);
    //((CCI_GROUPDATA *)omGroups[ilGroup])->Text = prpGroup->Text;
    //return ilGroup;

    int ilGroupCount = omGroups.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilGroup = 0; ilGroup < ilGroupCount; ilGroup++)
		if (CompareGroup(prpGroup, GetGroup(ilGroup)) <= 0)
            break;  // should be inserted before Groups[ilGroup]
#else
    // Search for the position which we want to insert this new bar
    for (int ilGroup = ilGroupCount; ilGroup > 0; ilGroup--)
		if (CompareGroup(prpGroup, GetGroup(ilGroup-1)) >= 0)
            break;  // should be inserted after Groups[ilGroup-1]
#endif
    
    omGroups.NewAt(ilGroup, *prpGroup);
	return ilGroup;
}

void CciDiagramViewer::DeleteGroup(int ipGroupno)
{
    while (GetLineCount(ipGroupno) > 0)
        DeleteLine(ipGroupno, 0);
    while (GetManagerCount(ipGroupno) > 0)
        DeleteManager(ipGroupno, 0);

	omGroups.DeleteAt(ipGroupno);
}

int CciDiagramViewer::CreateManager(int ipGroupno, CCI_MANAGERDATA *prpManager)
{
    int ilManagerCount = omGroups[ipGroupno].Managers.GetSize();

// Generally, raw data will be sorted from left to right.
// So, scanning for the place of insertion backward is a little bit faster
//
#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilManagerno = 0; ilManagerno < ilManagerCount; ilManagerno++)
		if (CompareManager(prpManager, GetManager(ipGroupno, ilManagerno)) <= 0)
            break;  // should be inserted before Lines[ilManagerno]
#else
    // Search for the position which we want to insert this new bar
    for (int ilManagerno = ilManagerCount; ilManagerno > 0; ilManagerno--)
		if (CompareManager(prpManager, GetManager(ipGroupno, ilManagerno-1)) >= 0)
            break;  // should be inserted after Lines[ilManagerno-1]
#endif

    omGroups[ipGroupno].Managers.NewAt(ilManagerno, *prpManager);
    CCI_MANAGERDATA *prlManager = &omGroups[ipGroupno].Managers[ilManagerno];
    return ilManagerno;
}

void CciDiagramViewer::DeleteManager(int ipGroupno, int ipManagerno)
{
    omGroups[ipGroupno].Managers.DeleteAt(ipManagerno);
}

int CciDiagramViewer::CreateLine(int ipGroupno, CCI_LINEDATA *prpLine)
{
	int ilLine = GetLineCount(ipGroupno);
    omGroups[ipGroupno].Lines[0].NewAt(ilLine, *prpLine);
    omGroups[ipGroupno].Lines[1].NewAt(ilLine, *prpLine);
    CCI_LINEDATA *prlLine0 = &omGroups[ipGroupno].Lines[0][ilLine];
    CCI_LINEDATA *prlLine1 = &omGroups[ipGroupno].Lines[1][ilLine];
    prlLine0->MaxOverlapLevel = 0;   // there is no bar in this line right now
    prlLine1->MaxOverlapLevel = 0;   // there is no bar in this line right now
    return ilLine;
}

void CciDiagramViewer::DeleteLine(int ipGroupno, int ipLineno)
{
	CCI_LINEDATA *prlLine;

	// left side
	prlLine = &omGroups[ipGroupno].Lines[0][ipLineno];
	prlLine->BlkBkBars.DeleteAll();
	prlLine->CcaBkBars.DeleteAll();
	prlLine->DemBkBars.DeleteAll();
	while (prlLine->Bars.GetSize() > 0)
	{
		prlLine->Bars[0].Indicators.DeleteAll();
		prlLine->Bars.DeleteAt(0);
	}

	// right side
	prlLine = &omGroups[ipGroupno].Lines[1][ipLineno];
	prlLine->BlkBkBars.DeleteAll();
	prlLine->CcaBkBars.DeleteAll();
	prlLine->DemBkBars.DeleteAll();
	while (prlLine->Bars.GetSize() > 0)
	{
		prlLine->Bars[0].Indicators.DeleteAll();
		prlLine->Bars.DeleteAt(0);
	}

    omGroups[ipGroupno].Lines[0].DeleteAt(ipLineno);
    omGroups[ipGroupno].Lines[1].DeleteAt(ipLineno);
}

int CciDiagramViewer::CreateBar(int ipGroupno, int ipSideno, int ipLineno, CCI_BARDATA *prpBar, BOOL bpTopBar)
{
    CCI_LINEDATA *prlLine = GetLine(ipGroupno, ipSideno, ipLineno);
    int ilBarCount = prlLine->Bars.GetSize();

	// added bch
	prlLine->Bars.NewAt(ilBarCount, *prpBar);	// to be the last bar in painting order
	prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, prpBar->OverlapLevel);
	return ilBarCount;


	// Id 14-Sep-1996
	// I use a new algorithm for bar insertion and new deletion.
	// This new algorithm will work somewhat slower than the previous version, but safer.
	// To give you a brief idea of this algorithm, here is the essential of insertion:
	//
	// Case A: Insert a new bar to be the topmost bar
	//		We will scan every bars, and for each bar which is in the same overlap group
	//		with the new bar, we will shift it downward one level, and update the field
	//		Line.MaxOverlapLevel. This new bar will be the last bar in painting order
	//		(for being topmost), and has overlapping level = 0.
	//
	// Case B: Insert a new bar to be the bottommost bar
	//		We will scan every bars for finding the maximum overlap level of bars which
	//		are in the same overlap group with this new bar (use level -1 if not found).
	//		Then use this maximum overlap level + 1 as overlap level of this new bar.
	//		This new bar will be the first bar in painting order (for being bottommost).
	//
	CUIntArray olBarnoList;
	GetOverlappedBarsFromTime(ipGroupno, ipSideno, ipLineno,
		prpBar->StartTime, prpBar->EndTime, olBarnoList);
	int ilListCount = olBarnoList.GetSize();
	if (bpTopBar)
	{
		for (int ilLc = 0; ilLc < ilListCount; ilLc++)
		{
			int ilBar = olBarnoList[ilLc];
			CCI_BARDATA *prlBar = &prlLine->Bars[ilBar];
			int ilNewOverlapLevel = ++prlBar->OverlapLevel;
			prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, ilNewOverlapLevel);
		}
		prlLine->Bars.NewAt(ilBarCount, *prpBar);	// to be the last bar in painting order
		prlLine->Bars[ilBarCount].OverlapLevel = 0;
		return ilBarCount;
	}
	else
	{
		int ilNewBarLevel = -1;
		for (int ilLc = 0; ilLc < ilListCount; ilLc++)
		{
			int ilBar = olBarnoList[ilLc];
			CCI_BARDATA *prlBar = &prlLine->Bars[ilBar];
			ilNewBarLevel = max(ilNewBarLevel, prlBar->OverlapLevel + 1);
				// the +1 means that we have to insert the new bar below this bar
		}
		prlLine->Bars.NewAt(0, *prpBar);	// to be the first bar in painting order
		prlLine->Bars[0].OverlapLevel = ilNewBarLevel;
		prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, ilNewBarLevel);
		return 0;
	}
}

void CciDiagramViewer::DeleteCcaBkBar(int ipGroupno, int ipSideno, int ipLineno, int ipBkBarno)
{
    long llUcca = omGroups[ipGroupno].Lines[ipSideno][ipLineno].CcaBkBars[ipBkBarno].Urno;
    omGroups[ipGroupno].Lines[ipSideno][ipLineno].CcaBkBars.DeleteAt(ipBkBarno);
}

void CciDiagramViewer::DeleteDemBkBar(int ipGroupno, int ipSideno, int ipLineno, int ipBkBarno)
{
	long llUdem = omGroups[ipGroupno].Lines[ipSideno][ipLineno].DemBkBars[ipBkBarno].Urno;
    omGroups[ipGroupno].Lines[ipSideno][ipLineno].DemBkBars.DeleteAt(ipBkBarno);

	// recalculate the max overlap level
	omGroups[ipGroupno].Lines[ipSideno][ipLineno].MaxOverlapLevel = 0;
	int ilNumBkBars = omGroups[ipGroupno].Lines[ipSideno][ipLineno].DemBkBars.GetSize(), ilBkBar;
	for(ilBkBar = 0; ilBkBar < ilNumBkBars; ilBkBar++)
	{
		omGroups[ipGroupno].Lines[ipSideno][ipLineno].MaxOverlapLevel = 
			max(omGroups[ipGroupno].Lines[ipSideno][ipLineno].MaxOverlapLevel, 
			omGroups[ipGroupno].Lines[ipSideno][ipLineno].DemBkBars[ilBkBar].OverlapLevel);
	}
	ilNumBkBars = omGroups[ipGroupno].Lines[ipSideno][ipLineno].CcaBkBars.GetSize(), ilBkBar;
	for(ilBkBar = 0; ilBkBar < ilNumBkBars; ilBkBar++)
	{
		omGroups[ipGroupno].Lines[ipSideno][ipLineno].MaxOverlapLevel = 
			max(omGroups[ipGroupno].Lines[ipSideno][ipLineno].MaxOverlapLevel, 
			omGroups[ipGroupno].Lines[ipSideno][ipLineno].CcaBkBars[ilBkBar].OverlapLevel);
	}
}

void CciDiagramViewer::DeleteBar(int ipGroupno, int ipSideno, int ipLineno, int ipBarno)
{
	// Delete all indicators of this bar
	while (GetIndicatorCount(ipGroupno, ipSideno, ipLineno, ipBarno) > 0)
		DeleteIndicator(ipGroupno, ipSideno, ipLineno, ipBarno, 0);

    CCI_LINEDATA *prlLine = GetLine(ipGroupno, ipSideno, ipLineno);
	CCI_BARDATA *prlBar = &prlLine->Bars[ipBarno];

	// Id 14-Sep-1996
	// I use a new algorithm for bar insertion and new deletion.
	// This new algorithm will work somewhat slower than the previous version, but safer.
	// To give you a brief idea of this algorithm, here is the essential of deletion:
	//
	// We will scan every bars, and for each bar which is in the same overlap group with
	// the new bar, we will back up them to a temporary array. Then we remove every bars
	// in this overlapping group, and insert these bar from the temporary array except
	// the one which has to be deleted back to the viewer.
	//
	// In some situation, if this overlapping group controls the maximum overlap level of
	// the line, we may have to rescan every bars in this line to make sure that the
	// maximum overlap level still be correct.
	//
	CUIntArray olBarnoList;
	GetOverlappedBarsFromTime(ipGroupno, ipSideno, ipLineno,
		prlBar->StartTime, prlBar->EndTime, olBarnoList);
	int ilListCount = olBarnoList.GetSize();

	// Save the bar in this overlapping group to a temporary array
	CCSPtrArray<CCI_BARDATA> olSavedBars;
	BOOL blMustRecomputeMaxOverlapLevel = FALSE;
	for (int ilLc = 0; ilLc < ilListCount; ilLc++)
	{
		int ilBar = olBarnoList[ilLc];
		CCI_BARDATA *prlBar = &prlLine->Bars[ilBar];
		blMustRecomputeMaxOverlapLevel |= (prlBar->OverlapLevel == prlLine->MaxOverlapLevel);
			// check if deletion of this bar may change the line height
		if (ilBar != ipBarno)	// must we save this bar?
			olSavedBars.NewAt(olSavedBars.GetSize(), prlLine->Bars[ilBar]);
	}
	// Delete all bars (which are already backed up in a temporary array) from the viewer
	while (ilListCount-- > 0)
	{
		int ilBar = olBarnoList[ilListCount];
		prlLine->Bars.DeleteAt(ilBar);
	}
	// Then, we insert those bars back to the Gantt chart, except the specified one
	int ilSavedBarCount = olSavedBars.GetSize();
	for (int ilSavedBarno = 0; ilSavedBarno < ilSavedBarCount; ilSavedBarno++)
	{
		CreateBar(ipGroupno, ipSideno, ipLineno, &olSavedBars[ilSavedBarno]);
	}
	olSavedBars.DeleteAll();
	// Then recompute the MaxOverlapLevel if it's necessary
	if (blMustRecomputeMaxOverlapLevel)
	{
		prlLine->MaxOverlapLevel = 0;
	    int ilBarCount = prlLine->Bars.GetSize();
		for (int ilBar = 0; ilBar < ilBarCount; ilBar++)
		{
			CCI_BARDATA *prlBar = &prlLine->Bars[ilBar];
			prlLine->MaxOverlapLevel = max(prlLine->MaxOverlapLevel, prlBar->OverlapLevel);
		}
	}
}

int CciDiagramViewer::CreateIndicator(int ipGroupno, int ipSide, int ipLineno, int ipBarno, CCI_INDICATORDATA *prpIndicator)
{
    //CCI_BARDATA *prlBar = GetBar(ipGroupno, ipSide, ipLineno, ipBarno);
    //int ilIndicatorCount = prlBar->Indicators.GetSize();
    int ilIndicatorCount = omGroups[ipGroupno].Lines[ipSide][ipLineno].Bars[ipBarno].Indicators.GetSize();

#ifndef SCANBACKWARD
    // Search for the position which we want to insert this new bar
    for (int ilIndicatorno = 0; ilIndicatorno < ilIndicatorCount; ilIndicatorno++)
        if (prpIndicator->StartTime <= GetIndicator(ipGroupno,ipSide,ipLineno,ipBarno,ilIndicatorno)->StartTime)
            break;  // should be inserted before Indicators[ilBar]
#else
    // Search for the position which we want to insert this new bar
    for (int ilIndicatorno = ilIndicatorCount; ilIndicatorno > 0; ilIndicatorno--)
        if (prpIndicator->StartTime >= GetIndicator(ipGroupno,ipSide,ipLineno,ipBarno,ilIndicatorno-1)->StartTime)
            break;  // should be inserted before Indicators[ilBar]
#endif

    //prlBar->Indicators.InsertAt(ilIndicatorno, new CCI_INDICATORDATA);
    //CCI_INDICATORDATA *prlIndicator = (CCI_INDICATORDATA *)prlBar->Indicators[ilIndicatorno];
	//prlIndicator->StartTime = prpIndicator->StartTime;
	//prlIndicator->EndTime = prpIndicator->EndTime;
	//prlIndicator->Color = prpIndicator->Color;
    //return ilIndicatorno;
    omGroups[ipGroupno].Lines[ipSide][ipLineno].Bars[ipBarno].Indicators.
		NewAt(ilIndicatorno, *prpIndicator);
    return ilIndicatorno;
}

void CciDiagramViewer::DeleteIndicator(int ipGroupno, int ipSide, int ipLineno, int ipBarno, int ipIndicatorno)
{
    //delete GetIndicator(ipGroupno, ipSide, ipLineno, ipBarno, ipIndicatorno);
    //GetBar(ipGroupno,ipSide,ipLineno,ipBarno)->Indicators.RemoveAt(ipIndicatorno);
    omGroups[ipGroupno].Lines[ipSide][ipLineno].Bars[ipBarno].Indicators.DeleteAt(ipIndicatorno);
}


/////////////////////////////////////////////////////////////////////////////
// CciDiagramViewer - CCI_BARDATA calculation routines

// Return the bar number of the list box based on the given period [time1, time2]
int CciDiagramViewer::GetBarnoFromTime(int ipGroupno, int ipSide, int ipLineno, 
									   CTime opTime1, CTime opTime2,
									   int ipOverlapLevel1, int ipOverlapLevel2)
{
    for (int i = GetBarCount(ipGroupno,ipSide,ipLineno)-1; i >= 0; i--)
    {
        CCI_BARDATA *prlBar = GetBar(ipGroupno, ipSide, ipLineno, i);
        if (prlBar != NULL && opTime1 <= prlBar->EndTime && prlBar->StartTime <= opTime2 &&
            ipOverlapLevel1 <= prlBar->OverlapLevel && prlBar->OverlapLevel <= ipOverlapLevel2)
            return i;
    }
    return -1;
}

int CciDiagramViewer::GetCcaBkBarnoFromTime(int ipGroupno, int ipSide, int ipLineno, 
											CTime opTime1, CTime opTime2,
											int ipOverlapLevel1, int ipOverlapLevel2)
{
	int i;
    for (i = GetCcaBkBarCount(ipGroupno,ipSide,ipLineno)-1; i >= 0; i--)
    {
        CCI_BKBARDATA *prlBkBar = GetCcaBkBar(ipGroupno, ipSide, ipLineno, i);
		if(prlBkBar != NULL && opTime1 <= prlBkBar->EndTime && prlBkBar->StartTime <= opTime2 &&
            ipOverlapLevel1 <= prlBkBar->OverlapLevel && prlBkBar->OverlapLevel <= ipOverlapLevel2)
				return i;
    }
    return -1;
}

int CciDiagramViewer::GetDemBkBarnoFromTime(int ipGroupno, int ipSide, int ipLineno, 
											CTime opTime1, CTime opTime2,
											int ipOverlapLevel1, int ipOverlapLevel2)
{
	int i;
    for (i = GetDemBkBarCount(ipGroupno,ipSide,ipLineno)-1; i >= 0; i--)
    {
        CCI_BKBARDATA *prlBkBar = GetDemBkBar(ipGroupno, ipSide, ipLineno, i);
		if(prlBkBar != NULL && opTime1 <= prlBkBar->EndTime && prlBkBar->StartTime <= opTime2 &&
            ipOverlapLevel1 <= prlBkBar->OverlapLevel && prlBkBar->OverlapLevel <= ipOverlapLevel2)
				return i;
    }
    return -1;
}

int CciDiagramViewer::GetBlkBkBarnoFromTime(int ipGroupno, int ipSide, int ipLineno, CTime opTime1, CTime opTime2)
{
	int i;
    for (i = GetBlkBkBarCount(ipGroupno,ipSide,ipLineno)-1; i >= 0; i--)
    {
        CCI_BKBARDATA *prlBkBar = GetBlkBkBar(ipGroupno, ipSide, ipLineno, i);
		if(opTime1 <= prlBkBar->EndTime && prlBkBar->StartTime <= opTime2)
				return i;
    }
    return -1;
}

/////////////////////////////////////////////////////////////////////////////
// CciDiagramViewer private helper methods

void CciDiagramViewer::GetOverlappedBarsFromTime(int ipGroupno, int ipSideno, int ipLineno,
	CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList)
{
    CCI_LINEDATA *prlLine = GetLine(ipGroupno, ipSideno, ipLineno);
	GetOverlappedBarsFromTime(prlLine, opTime1, opTime2, ropBarnoList);
}

void CciDiagramViewer::GetOverlappedBarsFromTime(CCI_LINEDATA *prpLine,	CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList)
{
    int ilBarCount = prpLine->Bars.GetSize();

	// Prepare the empty array of the involvement flag for each bar
	CCSPtrArray<BOOL> IsInvolvedBar;
	for (int i = 0; i < ilBarCount; i++)
		IsInvolvedBar.NewAt(i, FALSE);

	// We will loop again and again until there is no more bar in this overlapping found.
	// The overlapping group is at first determine by the given "opTime1" and "opTime2".
	// But if the bar we find is exceed this period of time, we will extend searching
	// to both left side and right side in the next round.
	//
	int nBarsFoundThisRound = -1;
	while (nBarsFoundThisRound != 0)
	{
		nBarsFoundThisRound = 0;
		for (int ilBar = 0; ilBar < ilBarCount; ilBar++)
		{
			CTime olStartTime = prpLine->Bars[ilBar].StartTime;
			CTime olEndTime = prpLine->Bars[ilBar].EndTime;
			BOOL blIsOverlapped = IsReallyOverlapped(olStartTime, olEndTime, opTime1, opTime2);
			BOOL blIsWithIn = IsWithIn(olStartTime, olEndTime, opTime1, opTime2);
				// this IsWithIn() fix the real thin bar bug
			BOOL blIsInvolved = (blIsOverlapped || blIsWithIn);

			if (!IsInvolvedBar[ilBar] && blIsInvolved)	// some more new involved bars?
			{
				IsInvolvedBar[ilBar] = TRUE;
				nBarsFoundThisRound++;
				opTime1 = min(opTime1, prpLine->Bars[ilBar].StartTime);
				opTime2 = max(opTime2, prpLine->Bars[ilBar].EndTime);
			}
		}
	}

	// Create the list of involved bars, then store them to "ropBarnoList"
	ropBarnoList.RemoveAll();
	for (int ilBar = 0; ilBar < ilBarCount; ilBar++)
	{
		if (IsInvolvedBar[ilBar])
		{
			ropBarnoList.Add(ilBar);
		}
	}
	IsInvolvedBar.DeleteAll();
}

void CciDiagramViewer::GetOverlappedBkBarsFromTime(CCSPtrArray <CCI_BKBARDATA> &ropBkBars, CTime opTime1, CTime opTime2, CCSPtrArray <CCI_BKBARDATA> &ropSelectedBkBars)
{
    int ilBarCount = ropBkBars.GetSize();
	CTimeSpan ol2Mins(0, 0, 2, 0);

	for (int ilBar = 0; ilBar < ilBarCount; ilBar++)
	{
		CTime olStartTime = ropBkBars[ilBar].StartTime - ol2Mins;
		CTime olEndTime = ropBkBars[ilBar].EndTime + ol2Mins;
		if(IsReallyOverlapped(olStartTime, olEndTime, opTime1, opTime2))
			ropSelectedBkBars.Add(&ropBkBars[ilBar]);
	}
}
/////////////////////////////////////////////////////////////////////////////
// Co-working routines with CCI Chart

BOOL CciDiagramViewer::IsGrouppedByTerminal()
{
	return omGroupBy == GROUP_BY_TERMINAL;
}

/////////////////////////////////////////////////////////////////////////////
// Data updating methods

void CciDiagramViewer::CciDiagramCf(void *popInstance, int ipDDXType,void *vpDataPointer, CString &ropInstanceName)
{
    CciDiagramViewer *polViewer = (CciDiagramViewer *)popInstance;

	if (ipDDXType == JOB_NEW)
	{
		polViewer->ProcessFmcJobNew((JOBDATA *)vpDataPointer);
		polViewer->ProcessJobChange((JOBDATA *)vpDataPointer);
	}
	else if (ipDDXType == JOB_CHANGE)
	{
		polViewer->ProcessFmcJobNew((JOBDATA *)vpDataPointer);
		polViewer->ProcessFmcJobDelete((JOBDATA *)vpDataPointer);
		polViewer->ProcessJobChange((JOBDATA *)vpDataPointer);
	}
	else if (ipDDXType == JOB_DELETE)
	{
		polViewer->ProcessFmcJobDelete((JOBDATA *)vpDataPointer);
		polViewer->ProcessJobDelete((JOBDATA *)vpDataPointer);
	}
	else if (ipDDXType == JOD_NEW || ipDDXType == JOD_CHANGE)
	{
		polViewer->ProcessJodChange((JODDATA *)vpDataPointer);
	}
	else if (ipDDXType == CCA_CHANGE)
	{
		polViewer->ProcessCcaChange((CCADATA *)vpDataPointer);
	}
	else if (ipDDXType == CCA_DELETE)
	{
		polViewer->ProcessCcaDelete((long)vpDataPointer);
	}
	else if (ipDDXType == DEMAND_CHANGE || ipDDXType == DEMAND_NEW)
	{
		polViewer->ProcessDemandChange((DEMANDDATA *)vpDataPointer);
	}
	else if (ipDDXType == DEMAND_DELETE)
	{
		polViewer->ProcessDemandDelete((DEMANDDATA *)vpDataPointer);
	}
	else if (ipDDXType == REDISPLAY_ALL || ipDDXType == UPDATE_CONFLICT_SETUP)
	{
		polViewer->ChangeViewTo(polViewer->SelectView(),polViewer->omStartTime,polViewer->omEndTime);
		polViewer->pomAttachWnd->Invalidate();
	}
}


void CciDiagramViewer::ProcessJodChange(JODDATA *prpJod)
{
	if(prpJod != NULL)
	{
		ProcessJobChange(ogJobData.GetJobByUrno(prpJod->Ujob));
	}
}

void CciDiagramViewer::ProcessJobChange(JOBDATA *prpJob)
{
	if(prpJob == NULL)
		return;

	int ilGroup, ilSide, ilLine, ilBar;

//	if(prpJob == NULL || (strcmp(prpJob->Jtco, JOBFLIGHT) && strcmp(prpJob->Jtco, JOBCCI) && strcmp(prpJob->Jtco,JOBCCC) && strcmp(prpJob->Jtco,JOBFID)))
//		return;

	// For other kinds of job, create a new bar in appropriate line
	ilGroup = ilLine = -1;
	while(FindBar(prpJob->Urno, ilGroup, ilSide, ilLine, ilBar))	// corresponding duty bar found
	{
		int ilOldMaxOverlapLevel = GetMaxOverlapLevel(ilGroup, ilSide, ilLine);
		DeleteBar(ilGroup, ilSide, ilLine, ilBar);
		LONG lParam = MAKELONG(ilLine, ilGroup);
		if (GetMaxOverlapLevel(ilGroup, ilSide, ilLine) == ilOldMaxOverlapLevel)
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
		else
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
	}

	DEMANDDATA *prlDemand = ogJodData.GetDemandForJob(prpJob);
	ilGroup = ilLine = -1;
	if(prlDemand != NULL && FindDemBkBar(prlDemand->Urno, ilGroup, ilSide, ilLine, ilBar))
	{
		// each counter may be displayed in more than one group so search for all
		ilGroup = ilLine = -1;
		while(FindDemBkBar(prlDemand->Urno, ilGroup, ilSide, ilLine, ilBar))
		{
			int ilOldMaxOverlapLevel = GetMaxOverlapLevel(ilGroup, ilSide, ilLine);
			if(IsPassFilter(prpJob))
			{
				MakeBar(ilGroup, ilSide, ilLine,prpJob);
				LONG lParam = MAKELONG(ilLine, ilGroup);
				if (GetMaxOverlapLevel(ilGroup, ilSide, ilLine) == ilOldMaxOverlapLevel)
					pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
				else
					pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
			}
		}
	}
	else if(!strcmp(prpJob->Aloc,ALLOCUNITTYPE_CIC) && strlen(prpJob->Alid) > 0)
	{
		// each counter may be displayed in more than one group so search for all
		ilGroup = ilLine = -1;
		while(FindCounter(prpJob->Alid,ilGroup,ilSide,ilLine))
		{
			int ilOldMaxOverlapLevel = GetMaxOverlapLevel(ilGroup, ilSide, ilLine);
			if(IsPassFilter(prpJob))
			{
				MakeBar(ilGroup, ilSide, ilLine,prpJob);
				LONG lParam = MAKELONG(ilLine, ilGroup);
				if (GetMaxOverlapLevel(ilGroup, ilSide, ilLine) == ilOldMaxOverlapLevel)
					pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
				else
					pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
			}
		}
	}
}

void CciDiagramViewer::ProcessJobDelete(JOBDATA *prpJob)
{
	int ilGroup, ilSide, ilLine, ilBar;

	if(prpJob == NULL || (strcmp(prpJob->Jtco, JOBFLIGHT) && strcmp(prpJob->Jtco, JOBCCI) && strcmp(prpJob->Jtco,JOBCCC) && strcmp(prpJob->Jtco,JOBFID)))	// uninterest job types
		return;

	// For other kinds of job, create a new bar in appropriate line
	ilGroup = ilLine = -1;
	while(FindBar(prpJob->Urno, ilGroup, ilSide, ilLine, ilBar))	// corresponding duty bar found
	{
		int ilOldMaxOverlapLevel = GetMaxOverlapLevel(ilGroup, ilSide, ilLine);
		DeleteBar(ilGroup, ilSide, ilLine, ilBar);
		LONG lParam = MAKELONG(ilLine, ilGroup);
		if (GetMaxOverlapLevel(ilGroup, ilSide, ilLine) == ilOldMaxOverlapLevel)
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
		else
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
    }
}


void CciDiagramViewer::ProcessFmcJobNew(JOBDATA *prpJob)
{
	// Don't interest it if it's not a flight manager CCI area job
	if (prpJob == NULL || CString(prpJob->Jtco) != JOBFMCCIAREA)
		return;

	int ilGroup;
	if (!FindGroup(prpJob->Alid, ilGroup))
        return; // no such group in the viewer buffer right now, just do nothing

	// Update the Viewer's data
	MakeManager(ilGroup, prpJob);

	// Notifies the attached window (if exist)
	if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
	{
		LONG lParam = MAKELONG(-1, ilGroup);
		pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATEGROUP, lParam);
	}
}

void CciDiagramViewer::ProcessFmcJobDelete(JOBDATA *prpJob)
{
	// Don't interest it if it's not a flight manager CCI area job
	if (prpJob == NULL || CString(prpJob->Jtco) != JOBFMCCIAREA)
		return;

	int ilGroup, ilManagerno;
	if (!FindManager(prpJob->Urno, ilGroup, ilManagerno))
        return; // no such group in the viewer buffer right now, just do nothing

	// Update the Viewer's data
	DeleteManager(ilGroup, ilManagerno);

	// Notifies the attached window (if exist)
	if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
	{
		LONG lParam = MAKELONG(-1, ilGroup);
		pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATEGROUP, lParam);
	}
}

void CciDiagramViewer::ProcessCcaChange(CCADATA *prpCca)
{
	if(prpCca == NULL)
		return;

	int ilOldGroup = 0, ilOldSide = 0, ilOldLine = 0, ilOldBar = 0;
	int ilGroup = 0, ilSide = 0, ilLine = 0;

	if(FindCcaBkBar(prpCca->Urno, ilOldGroup, ilOldSide, ilOldLine, ilOldBar))
	{
		// update bar
		ilOldGroup = ilOldSide = ilOldLine = -1;
		while(FindCcaBkBar(prpCca->Urno, ilOldGroup, ilOldSide, ilOldLine, ilOldBar))
		{
			CCI_BKBARDATA *prlBkBar = GetCcaBkBar(ilOldGroup, ilOldSide, ilOldLine, ilOldBar);
			DeleteCcaBkBar(ilOldGroup, ilOldSide, ilOldLine, ilOldBar);
			if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
			{
				LONG lParam = MAKELONG(ilOldLine, ilOldGroup);
				pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
			}
		}
		ilGroup = -1;
		ilLine = -1;
		while(FindCounter(prpCca->Cnam,ilGroup,ilSide,ilLine))
		{
			if(IsPassFilter(prpCca))
			{
				MakeCcaBkBar(ilGroup, ilSide, ilLine, prpCca);
				GetLine(ilGroup, ilSide, ilLine)->CcaBkBars.Sort(ByTime);
				if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
				{
					LONG lParam = MAKELONG(ilLine, ilGroup);
					pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
				}
			}
		}
	}
	else
	{
		// insert bar
		int ilGroup, ilSide, ilLine;
		ilGroup = -1;
		ilLine = -1;
		while(FindCounter(prpCca->Cnam,ilGroup,ilSide,ilLine))
		{
			if(IsPassFilter(prpCca))
			{
				CCI_BKBARDATA *prlCcaBkBar = MakeCcaBkBar(ilGroup, ilSide, ilLine, prpCca);
				if(prlCcaBkBar != NULL)
				{
					CCI_LINEDATA *prlLine = GetLine(ilGroup, ilSide, ilLine);
					if(prlLine != NULL)
					{
						MakeDemBkBarForCcaBkBar(prlLine, prlCcaBkBar);
						prlLine->CcaBkBars.Sort(ByTime);
						prlLine->DemBkBars.Sort(ByTime);
						if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
						{
							LONG lParam = MAKELONG(ilLine, ilGroup);
							pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
						}
					}
				}
			}
		}
	}
}

void CciDiagramViewer::ProcessCcaDelete(long lpCcaUrno)
{
	int ilGroup = -1, ilSide = 0, ilLine = -1, ilBar = 0;
	while(FindCcaBkBar(lpCcaUrno, ilGroup, ilSide, ilLine, ilBar))	// corresponding duty bar found
	{
		DeleteCcaBkBar(ilGroup, ilSide, ilLine, ilBar);
		if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
		{
			LONG lParam = MAKELONG(ilLine, ilGroup);
			pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
		}
    }
}

void CciDiagramViewer::ProcessJodNew(JODDATA *prpJod)
{
	if(prpJod == NULL)
		return;

	JOBDATA *prlJob = ogJobData.GetJobByUrno(prpJod->Ujob);
	ProcessJobChange(prlJob);
}

void CciDiagramViewer::ProcessDemandChange(DEMANDDATA *prpDem)
{
	if(prpDem == NULL)
		return;

	if (strcmp(prpDem->Aloc,ALLOCUNITTYPE_CIC) != 0)
		return;

	if (strcmp(prpDem->Rety,PERSONNEL_DEMAND) != 0)
		return;

	int ilGroup = -1, ilSide = 0, ilLine = -1, ilBar = 0;
	if(FindDemBkBar(prpDem->Urno, ilGroup, ilSide, ilLine, ilBar))
	{
		// update bar
		ilGroup = ilLine = -1;
		while(FindDemBkBar(prpDem->Urno, ilGroup, ilSide, ilLine, ilBar))
		{
			DeleteDemBkBar(ilGroup, ilSide, ilLine, ilBar);
			GetLine(ilGroup, ilSide, ilLine)->DemBkBars.Sort(ByTime);
			if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
			{
				LONG lParam = MAKELONG(ilLine, ilGroup);
				pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
			}
		}

		if (strlen(prpDem->Alid) > 0)
		{
			int ilGroupno,ilSideno,ilLineno;
			ilGroupno = ilLineno = -1;
			while(FindCounter(prpDem->Alid,ilGroupno,ilSideno,ilLineno))
			{
				if(IsPassFilter(prpDem))
				{
					MakeDemBkBar(ilGroupno, ilSideno, ilLineno, prpDem);
				}
				if (ilGroup != ilGroupno || ilSide != ilSideno || ilLine != ilLineno)
				{
					GetLine(ilGroupno, ilSideno, ilLineno)->DemBkBars.Sort(ByTime);
					if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
					{
						LONG lParam = MAKELONG(ilLineno, ilGroupno);
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
					}
				}
			}
		}
		else if (imDemandsWithoutCounterGroup >= 0)	// "Without Counter" group
		{
			int ilLastLine = GetLineCount(imDemandsWithoutCounterGroup) - 1;
			if(IsPassFilter(prpDem))
			{
				int ilUpdatedLine = MakeLineForDemandWithoutCounter(prpDem);
				if(ilUpdatedLine != -1)
				{
					LONG lParam = MAKELONG(ilUpdatedLine, imDemandsWithoutCounterGroup);
					if (ilUpdatedLine > ilLastLine)
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_INSERTLINE, lParam);
					else
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
				}
			}
		}

	}
	else if (ogCfgData.rmUserSetup.OPTIONS.CCA != USERSETUPDATA::READWRITE)
	{
		// insert bar
		if (strlen(prpDem->Alid) > 0)
		{
			ilGroup = -1;
			ilLine = -1;
			while(FindCounter(prpDem->Alid,ilGroup,ilSide,ilLine))
			{
				CCI_BKBARDATA *prlBkBar = MakeDemBkBar(ilGroup, ilSide, ilLine,prpDem);
				if (prlBkBar)
				{
					GetLine(ilGroup, ilSide, ilLine)->DemBkBars.Sort(ByTime);
					if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
					{
						LONG lParam = MAKELONG(ilLine, ilGroup);
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
					}
				}
			}
		}
		else if (imDemandsWithoutCounterGroup >= 0)	// "Without Counter" group
		{
			int ilLastLine = GetLineCount(imDemandsWithoutCounterGroup) - 1;
			if(IsPassFilter(prpDem))
			{
				int ilUpdatedLine = MakeLineForDemandWithoutCounter(prpDem);
				if(ilUpdatedLine != -1)
				{
					LONG lParam = MAKELONG(ilUpdatedLine, imDemandsWithoutCounterGroup);
					if (ilUpdatedLine > ilLastLine)
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_INSERTLINE, lParam);
					else
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
				}
			}
		}
	}
	else
	{
		// insert bar
		if (strlen(prpDem->Alid) > 0)
		{
			ilGroup = -1;
			ilLine = -1;
			while(FindCounter(prpDem->Alid,ilGroup,ilSide,ilLine))
			{
				CCI_BKBARDATA *prlBkBar = MakeDemBkBar(ilGroup, ilSide, ilLine,prpDem);
				if (prlBkBar)
				{
					GetLine(ilGroup, ilSide, ilLine)->DemBkBars.Sort(ByTime);
					if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
					{
						LONG lParam = MAKELONG(ilLine, ilGroup);
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
					}
				}
			}
		}
		else if (imDemandsWithoutCounterGroup >= 0)	// "Without Counter" group
		{
			int ilLastLine = GetLineCount(imDemandsWithoutCounterGroup) - 1;
			if(IsPassFilter(prpDem))
			{
				int ilUpdatedLine = MakeLineForDemandWithoutCounter(prpDem);
				if(ilUpdatedLine != -1)
				{
					LONG lParam = MAKELONG(ilUpdatedLine, imDemandsWithoutCounterGroup);
					if (ilUpdatedLine > ilLastLine)
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_INSERTLINE, lParam);
					else
						pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINE, lParam);
				}
			}
		}
	}
}

void CciDiagramViewer::ProcessDemandDelete(DEMANDDATA *prpDemand)
{
	int ilGroup = -1, ilSide = 0, ilLine = -1, ilBar = 0;
	if (prpDemand != NULL)
	{
		while(FindDemBkBar(prpDemand->Urno, ilGroup, ilSide, ilLine, ilBar))
		{
			DeleteDemBkBar(ilGroup, ilSide, ilLine, ilBar);
			if (::IsWindow(pomAttachWnd->GetSafeHwnd()))
			{
				LONG lParam = MAKELONG(ilLine, ilGroup);
				pomAttachWnd->SendMessage(WM_UPDATEDIAGRAM,	UD_UPDATELINEHEIGHT, lParam);
			}
		}
    }
}


///////////////////////////////////////////////////////////////////////////////////////////
// Printing routines

void CciDiagramViewer::PrintCciDiagramHeader(int ipGroupno)
{
	CString olCciAreaName = GetGroupText(ipGroupno);
    CTime olActStart(omStartTime.GetYear(),omStartTime.GetMonth(),omStartTime.GetDay(),
		omStartTime.GetHour(),0,0);

    CTimeSpan olDuration = omEndTime - omStartTime;
	int ilHours = olDuration.GetTotalHours();
	CTime olActEnd = olActStart + CTimeSpan(0,ilHours,0,0);

	CString omTarget;
	omTarget.Format(GetString(IDS_STRING61285),olCciAreaName,olActStart.Format("%d.%m.%Y %H:%M"),olActEnd.Format("%d.%m.%Y  %H:%M"));
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");
	pomPrint->omCdc.StartPage();
	pomPrint->PrintHeader(CString("CCI-Diagram"),olPrintDate,omTarget);
	pomPrint->PrintTimeScale(700,2800,olActStart,olActEnd,olCciAreaName);
}


BOOL CciDiagramViewer::PrintPrepareLineData(int ipGroupNo,int ipSideNo,int ipLineno,
	CCSPtrArray<PRINTBARDATA> &ropPrintLine)
{

	CString olLineText = GetLineText(ipGroupNo,ipSideNo,ipLineno);
	if (olLineText.IsEmpty() == FALSE)
	{
		CCI_LINEDATA *prlLine = &omGroups[ipGroupNo].Lines[ipSideNo][ipLineno];
		ropPrintLine.DeleteAll();
		int ilBarCount = prlLine->Bars.GetSize();
		for( int i = 0; i < ilBarCount; i++)
		{
			if (IsOverlapped(prlLine->Bars[i].StartTime,prlLine->Bars[i].EndTime,
				omStartTime,omEndTime))
			{
				PRINTBARDATA rlPrintBar;
				rlPrintBar.Text = prlLine->Bars[i].Text;
				rlPrintBar.StartTime = prlLine->Bars[i].StartTime;
				rlPrintBar.EndTime = prlLine->Bars[i].EndTime;
				rlPrintBar.FrameType = prlLine->Bars[i].FrameType;
				rlPrintBar.MarkerType = prlLine->Bars[i].MarkerType;
				rlPrintBar.IsShadowBar = FALSE;
				rlPrintBar.OverlapLevel = 0;
				rlPrintBar.IsBackGroundBar = FALSE;
				ropPrintLine.NewAt(ropPrintLine.GetSize(),rlPrintBar);
			}
		}
		return TRUE;
	}
	return FALSE;
}

void CciDiagramViewer::PrintManagers(int ipGroupno)
{
	CString olAllManagers;

	POSITION rlPos;
	for ( rlPos = ogJobData.omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		JOBDATA *prlJob;
		ogJobData.omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlJob);
		if (strcmp(prlJob->Jtco, JOBPOOL) != 0)	// uninterest non-duty bars
			continue;

		// Check if this duty is in the filter and has some part visible in the display window
		SHIFTDATA *prlShift;
		EMPDATA *prlEmp;
		if ((prlShift = ogShiftData.GetShiftByUrno(prlJob->Shur)) == NULL ||
			(prlEmp = ogEmpData.GetEmpByPeno(prlShift->Peno)) == NULL ||
			!IsPassFilter(prlJob->Alid, "","","") ||
			!IsOverlapped(prlJob->Acfr, prlJob->Acto, omStartTime, omEndTime))
			continue;

		int ilGroup;
		if (FindGroup(prlJob->Alid,ilGroup ) == FALSE)
			continue;	// manager is not in this group
		if (ilGroup != ipGroupno)
			continue;	// manager is not in this group
		BOOL blIsFlightManager = ogBasicData.IsManager(prlShift->Efct);
		if (blIsFlightManager)
		{
			if (olAllManagers.IsEmpty() == FALSE)
			{
				olAllManagers += "   /   ";
			}
			SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prlJob->Shur);
			olAllManagers += CString((prlShift != NULL ? prlShift->Sfca : "")) + ", ";
			olAllManagers += CString((prlShift != NULL ? prlShift->Egrp : "")) + ", ";
			EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prlJob->Peno);
			olAllManagers += CString(prlEmp != NULL ? prlEmp->Lanm : "");
		}
	}
	if (olAllManagers.IsEmpty() == FALSE)
	{
		int ilYOffset = 0;
		if (bmIsFirstGroupOnPage == FALSE)
		{
			pomPrint->PrintText(720,0,1100,40,PRINT_SMALLBOLD,"Flightmanager:",TRUE);
			pomPrint->PrintText(720,40,0,130,PRINT_SMALL,olAllManagers,TRUE);
			pomPrint->PrintText(720,0,0,50,PRINT_SMALL,"");
		}
		else
		{
			pomPrint->PrintText(720,380,1100,420,PRINT_SMALLBOLD,"Flightmanager:");
			pomPrint->PrintText(720,420,0,510,PRINT_SMALL,olAllManagers);
		}  
		bmIsFirstGroupOnPage = FALSE;
	}
}


/***************************************************
void CciDiagramViewer::PrintManagers(int ipGroupno)
{
	CString olAllManagers;

	POSITION rlPos;
	for ( rlPos = ogJobData.omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		JOBDATA *prlJob;
		ogJobData.omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlJob);
		if (strcmp(prlJob->Jtco, JOBPOOL) != 0)	// uninterest non-duty bars
			continue;

		// Check if this duty is in the filter and has some part visible in the display window
		SHIFTDATA *prlShift;
		EMPDATA *prlEmp;
		if ((prlShift = ogShiftData.GetShiftByUrno(prlJob->Shur)) == NULL ||
			(prlEmp = ogEmpData.GetEmpByPeno(prlShift->Peno)) == NULL ||
			!IsPassFilter(prlJob->Alid, prlShift->Tmid, prlShift->Sfca, prlEmp->Rank) ||
			!IsOverlapped(prlJob->Acfr, prlJob->Acto, omStartTime, omEndTime))
			continue;

		int ilGroup;
		if ((ilGroup = FindGroup(prlJob->Alid, prlShift->Tmid, prlShift->Sfca)) != ipGroupno)
			continue;	// manager is not in this group

		BOOL blIsFlightManager = (ogBasicData.omManager.Find(CString(":")+prlEmp->Rank+":") != -1);
		if (blIsFlightManager)
		{
			if (olAllManagers.IsEmpty() == FALSE)
			{
				olAllManagers += "   /   ";
			}
			SHIFTDATA *prlShift = ogShiftData.GetShiftByUrno(prlJob->Shur);
			olAllManagers += CString((prlShift != NULL ? prlShift->Sfca : "")) + ", ";
			olAllManagers += CString((prlShift != NULL ? prlShift->Tmid : "")) + ", ";
			EMPDATA *prlEmp = ogEmpData.GetEmpByPeno(prlJob->Peno);
			olAllManagers += CString(prlEmp != NULL ? prlEmp->Lnam : "");
		}
	}
	if (olAllManagers.IsEmpty() == FALSE)
	{
		pomPrint->PrintText(700,380,1000,450,PRINT_SMALLBOLD,"Flightmanager:");
		pomPrint->PrintText(700,420,2800,550,PRINT_SMALL,olAllManagers);
	}
}
***********************************/

void CciDiagramViewer::PrintCciArea(CPtrArray &opPtrArray,int ipGroupNo)
{
	CCSPtrArray<PRINTBARDATA> ropPrintLine;
	for ( int ipSideNo = 0; ipSideNo < 2; ipSideNo++)
	{
		int ilLineCount = GetLineCount(ipGroupNo);
		for( int ilLine = 0; ilLine < ilLineCount; ilLine++)
		{
			if( (pomPrint->imLineNo) > pomPrint->imMaxLines) // (pomPrint->imMaxLines - ogBasicData.imFreeBottomLines))
			{
				if ( pomPrint->imPageNo > 0)
				{
					pomPrint->PrintGanttBottom();
					pomPrint->PrintFooter("","");
					pomPrint->omCdc.EndPage();
					bmIsFirstGroupOnPage = TRUE;
					pomPrint->imLineNo = 0;
				}
				PrintManagers(ipGroupNo);
				PrintCciDiagramHeader(ipGroupNo);
			}
			if (PrintPrepareLineData(ipGroupNo,ipSideNo,ilLine,ropPrintLine))
			{
				if (pomPrint->PrintGanttLine(GetLineText(ipGroupNo,ipSideNo,ilLine),ropPrintLine) != TRUE)
				{
					// page full
					pomPrint->PrintGanttBottom();
					pomPrint->PrintFooter("","");
					pomPrint->omCdc.EndPage();
					pomPrint->imLineNo = 0;
					PrintManagers(ipGroupNo);
					PrintCciDiagramHeader(ipGroupNo);
					// try it again, but don't continue with this line if it don't fit on page again!
					pomPrint->PrintGanttLine(GetLineText(ipGroupNo,ipSideNo,ilLine),ropPrintLine);
				}
			}
		}
	}
//	pomPrint->PrintGanttBottom();
	pomPrint->imLineNo = 999;
}

void CciDiagramViewer::PrintDiagram(CPtrArray &opPtrArray)
{
	CString omTarget = CString("Anzeigebegin: ") + CString(omStartTime.Format("%d.%m.%Y"))
			+  "     Ansicht: " + ogCfgData.rmUserSetup.CCCV;
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y");
	pomPrint = new CCSPrint(pomAttachWnd,PRINT_LANDSCAPE,70,500,200,
		CString("CCI-Diagramm"),olPrintDate,omTarget);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(PRINT_LANDSCAPE,ogBasicData.pcmPrintForm,DMPAPER_A3)  == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = "CCI-Diagramm";	
			pomPrint->omCdc.StartDoc( &rlDocInfo );

			int ilGroupCount = GetGroupCount();
			for (int ilGroup = 0; ilGroup < ilGroupCount; ilGroup++)
			{
				PrintCciArea(opPtrArray,ilGroup);
			}
			pomPrint->PrintGanttBottom();
			pomPrint->PrintFooter("","");
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}

}

// get a list of Demand URNOs for all jobs displayed
void CciDiagramViewer::GetDemandList(CUIntArray  &ropDemandList)
{
	for(int ilGroupno = 0; ilGroupno < GetGroupCount(); ilGroupno++)
	{
		for (int ilSideno = 0; ilSideno < 2; ilSideno++)
		{
			for(int ilLineno = 0; ilLineno < GetLineCount(ilGroupno); ilLineno++)
			{
				for(int ilBkBarno = 0; ilBkBarno < GetDemBkBarCount(ilGroupno, ilSideno,ilLineno); ilBkBarno++)
				{
					CCI_BKBARDATA *prlBkBar = GetDemBkBar(ilGroupno,ilSideno,ilLineno,ilBkBarno);
					if (prlBkBar)
						ropDemandList.Add(prlBkBar->Urno);
				}
			}
		}
	}
}

CTime CciDiagramViewer::GetStartTime()
{
	return omStartTime;
}

CTime CciDiagramViewer::GetEndTime()
{
	return omEndTime;
}


void CciDiagramViewer::GetGroupNames(CStringArray& ropGroupNames)
{
	CMapStringToPtr *polGroupMap = NULL;

	switch(omGroupBy)
	{
	case GROUP_BY_TERMINAL:
		polGroupMap = &omCMapForTerminal;
	break;
	case GROUP_BY_HALL:
		polGroupMap = &omCMapForHall;
	break;
	case GROUP_BY_REGION:
		polGroupMap = &omCMapForRegion;
	break;
	case GROUP_BY_LINE:
		polGroupMap = &omCMapForLine;
	break;
	}

	if (!polGroupMap)
		return;

	POSITION pos = polGroupMap->GetStartPosition();
	while(pos)
	{
		CString olKey;
		void *polValue;
		polGroupMap->GetNextAssoc(pos,olKey,polValue);
		ropGroupNames.Add(olKey);
	}
}

void CciDiagramViewer::GetCountersByGroup(const CString& ropGroupName,CCSPtrArray<CICDATA>& ropCicList)
{
	switch(omGroupBy)
	{
	case GROUP_BY_TERMINAL:
		{
			CCSPtrArray <ALLOCUNIT> olAllocUnitCics;
			ogAllocData.GetUnitsByGroup(ALLOCUNITTYPE_CICGROUP,ropGroupName,olAllocUnitCics);
			olAllocUnitCics.Sort(ByName);
			int ilNumCics = olAllocUnitCics.GetSize();
			for (int ilCic = 0; ilCic < ilNumCics;ilCic++)
			{
				CICDATA *prlCicDesk = ogCicData.GetCicByUrno(olAllocUnitCics[ilCic].Urno);
				if (prlCicDesk)
					ropCicList.Add(prlCicDesk);
			}
		}
	break;
	case GROUP_BY_HALL:
		ogCicData.GetCicsByHall(ropCicList,ropGroupName);
		ropCicList.Sort(ByName);
	break;
	case GROUP_BY_REGION:
		ogCicData.GetCicsByRegion(ropCicList,ropGroupName);
		ropCicList.Sort(ByName);
	break;
	case GROUP_BY_LINE:
		ogCicData.GetCicsByLine(ropCicList,ropGroupName);
		ropCicList.Sort(ByName);
	break;
	}
	
}

void CciDiagramViewer::SortCounters(CCSPtrArray<CICDATA>& ropCicList)
{
	ropCicList.Sort(ByName);
}

BOOL CciDiagramViewer::IsGroupWithoutCounter(int ipGroupno)
{
	if (imDemandsWithoutCounterGroup == -1)
		return FALSE;
	return imDemandsWithoutCounterGroup == ipGroupno;
}

int	CciDiagramViewer::GetGroupWithoutCounter()
{
	return imDemandsWithoutCounterGroup;
}

CString CciDiagramViewer::GetRuleEventText(long lpRueUrno)
{
	CString olRueText;

	RUEDATA *prlRue = ogRueData.GetRueByUrno(lpRueUrno);
	if(prlRue != NULL)
	{
		olRueText = GetRuleEventText(prlRue);
	}
//	else
//	{
//		olRueText.Format("Rule Event Not Found RUE.URNO <%ld>",lpRueUrno);
//	}

	return olRueText;
}

CString CciDiagramViewer::GetRuleEventText(RUEDATA *prpRue)
{
	CString olRueText;

	if(prpRue != NULL)
	{
		olRueText.Format("%s (%s)",prpRue->Rusn,prpRue->Runa);
	}

	return olRueText;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
void CciDiagramViewer::PrintView(CTime opFrom, CTime opTo, bool bpSortByShift, bool bpSortByStd, bool bpDisplaySta, bool bpDisplayEta, bool bpDisplayStd, bool bpDisplayEtd, bool bpDisplayGate, bool bpDisplayArr)
{
	CTime olStart = opFrom; //ogBasicData.GetTimeframeStart();
	CTime olEnd = opTo; //ogBasicData.GetTimeframeEnd();
	CString omTarget = GetString(IDS_STRING33141) + CString(olStart.Format("%d.%m.%Y %H:%M")) + GetString(IDS_STRING33142) + CString(olEnd.Format("%d.%m.%Y %H:%M"));
	CString olPrintDate = ogBasicData.GetTime().Format("%d.%m.%Y %H:%M");

//#define DEBUG_PRINTING
	int ilNumExtraRows = 0;
	if(bpDisplaySta) ilNumExtraRows++;	if(bpDisplayEta) ilNumExtraRows++; if(bpDisplayStd) ilNumExtraRows++;	
	if(bpDisplayEtd) ilNumExtraRows++;	if(bpDisplayGate) ilNumExtraRows++; if(bpDisplayArr) ilNumExtraRows++;

#ifndef DEBUG_PRINTING
	CString olTitle;
	olTitle.Format("%s - %s", GetString(IDS_STAFFJOBS), ogCfgData.rmUserSetup.CCCV);
	pomPrint = new CCSPrint(pomAttachWnd,PRINT_PORTRAIT,30,390,1,olTitle,olPrintDate,omTarget);
	pomPrint->imMaxLines = 24;
#endif

	imCharWidth = 20;
	imTotalLen = 0;
	imTotalLen += (imFuncLen = 6 * imCharWidth);
	imTotalLen += (imNameLen = 15 * imCharWidth);
	imTotalLen += (imShiftLen = 10 * imCharWidth);
	imTotalLen += (imBreakLen = 4 * imCharWidth);
	imFlightLen = 4 * imCharWidth;
	imTotalLen += (imFidLen = 15 * imCharWidth);
	imTotalLen += (imOtherLen = 15 * imCharWidth);

#ifndef DEBUG_PRINTING
	if (pomPrint != NULL)
#endif
	{
#ifndef DEBUG_PRINTING
		if (pomPrint->InitializePrinter(PRINT_LANDSCAPE,ogBasicData.pcmPrintForm,DMPAPER_A4)  == TRUE)
#endif
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			int ilMaxFlightsPerLine = 19;
			int ilMaxLines = 18;
#ifndef DEBUG_PRINTING
			pomPrint->imLineNo = 0;
			if(pomPrint->smPaperSize == DMPAPER_A3)
			{
				if(pomPrint->imOrientation == PRINT_PORTRAIT)
				{
					ilMaxFlightsPerLine = 19;
					ilMaxLines = 51 - ilNumExtraRows;
				}
				else
				{
					ilMaxFlightsPerLine = 34;
					ilMaxLines = 31 - ilNumExtraRows;
				}
			}
			else
			{
				if(pomPrint->imOrientation == PRINT_PORTRAIT)
				{
					ilMaxFlightsPerLine = 8;
					ilMaxLines = 31 - ilNumExtraRows;
				}
				else
				{
					ilMaxFlightsPerLine = 19;
					ilMaxLines = 17 - ilNumExtraRows;
				}
			}
			pomPrint->imMaxLines = ilMaxLines;
#endif

			imTotalLen += (ilMaxFlightsPerLine * imFlightLen);

#ifndef DEBUG_PRINTING
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			pomPrint->imLineHeight = 62;
			rlDocInfo.cbSize = sizeof( DOCINFO );
			char pclDocName[100];
			strcpy(pclDocName,GetString(IDS_STAFFJOBS));
			rlDocInfo.lpszDocName = pclDocName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
#endif
			CCSPtrArray <CCI_PRINTSHIFTS> olPrintShifts;
			CCI_PRINTSHIFTS *prlPrintShift = NULL;
			CMapPtrToPtr olPrintShiftMap;
			JOBDATA *prlJob = NULL;
			SHIFTDATA *prlShift = NULL;
			FLIGHTDATA *prlFlight = NULL;
			DEMANDDATA *prlDemand = NULL;
			RUDDATA *prlRud = NULL;
			SERDATA *prlSer = NULL;
			CCI_BARDATA *prlBar = NULL;
			void *pvlDummy = NULL;
			int ilNumShifts;
			CString olText, olEmpty = "";
#ifndef DEBUG_PRINTING
			CCSPtrArray <PRINTELEDATA> rlPrintLine;
			PRINTELEDATA rlElement;
#endif

			// create a list of
			CDWordArray olCommonCheckinDemUrnos;

			// create a list of employee shifts and their jobs and sort it by shift code and main function
			for(int ilGroup = 0; ilGroup < omGroups.GetSize(); ilGroup++)
			{
				for(int ilSide = 0; ilSide < 2; ilSide++)
				{
					for(int ilLine = 0; ilLine < omGroups[ilGroup].Lines[ilSide].GetSize(); ilLine++)
					{
						for(int ilBar = 0; ilBar < omGroups[ilGroup].Lines[ilSide][ilLine].Bars.GetSize(); ilBar++)
						{
							prlBar = &omGroups[ilGroup].Lines[ilSide][ilLine].Bars[ilBar];
							if((prlJob = ogJobData.GetJobByUrno(prlBar->JobUrno)) != NULL &&
								(prlShift = ogShiftData.GetShiftByUrno(prlJob->Shur)) != NULL &&
								IsBetween(prlShift->Avfa,opFrom,opTo))
							{
								bool blCheckAdd = false, blAdded = false;
								if(!olPrintShiftMap.Lookup((void *) prlJob->Shur, (void *&) prlPrintShift))
								{
									prlPrintShift = new CCI_PRINTSHIFTS;
									prlPrintShift->prlShift = prlShift;
									prlPrintShift->olMainFunction = ogBasicData.GetFunctionForShift(prlShift);
									olPrintShifts.Add(prlPrintShift);
									blCheckAdd = true;
								}
								if(prlJob->Flur != 0L && ogFlightData.GetFlightByUrno(prlJob->Flur))
								{
//									prlPrintShift->olFlightJobs.Add(prlJob);
//									blAdded = true;
									blAdded = prlPrintShift->AddJob(prlJob, prlPrintShift->olFlightJobs);
								}
								else if(!strcmp(prlJob->Jtco,JOBFID))
								{
//									prlPrintShift->olFlightIndependentJobs.Add(prlJob);
//									blAdded = true;
									blAdded = prlPrintShift->AddJob(prlJob, prlPrintShift->olFlightIndependentJobs);
								}
								else if(!strcmp(prlJob->Jtco,JOBCCC)) // common checkin
								{
									if((prlDemand = ogJodData.GetDemandForJob(prlJob)) != NULL)
									{
										if(blAdded = (prlPrintShift->AddJob(prlJob, prlPrintShift->olCommonCheckinJobs)) == true)
											olCommonCheckinDemUrnos.Add(prlDemand->Urno);
//										olCommonCheckinDemUrnos.Add(prlDemand->Urno);
//										prlPrintShift->olCommonCheckinJobs.Add(prlJob);
//										blAdded = true;
									}
								}
								if(blCheckAdd)
								{
									if(blAdded)
										olPrintShiftMap.SetAt((void *) prlJob->Shur, prlPrintShift);
									else
										delete prlPrintShift;
								}
							}
						}
					}
				}
			}

			CMapPtrToPtr olAllFlightsMap; // map of all flight urnos used to get gate jobs
			for(int ilPS = 0; ilPS < olPrintShifts.GetSize(); ilPS++)
			{
				prlPrintShift = &olPrintShifts[ilPS];
				for(int ilF = (prlPrintShift->olFlightJobs.GetSize()-1); ilF >= 0 ; ilF--)
				{
					if(!olAllFlightsMap.Lookup((void *) prlPrintShift->olFlightJobs[ilF].Flur, (void *&) pvlDummy))
						olAllFlightsMap.SetAt((void *) prlPrintShift->olFlightJobs[ilF].Flur, NULL);
				}
			}

			// get flights for common checkin jobs
			CMapPtrToPtr olCommonCheckinDemandFlightsMap, *polFlights, *polShifts;
			if(olCommonCheckinDemUrnos.GetSize() > 0)
			{
				long llDemUrno, llFlightUrno;
				CStringArray olFlights, olUrnos;
				int ilNumFlights = ogDemandData.GetFlightsFromCciDemand(olCommonCheckinDemUrnos, olFlights);
				for(int ilCCF = 0; ilCCF < ilNumFlights; ilCCF++)
				{
					//ogBasicData.Trace("FLC Returns %s\n", olFlights[ilCCF]);
					ogBasicData.ExtractItemList(olFlights[ilCCF], &olUrnos, ',');
					if(olUrnos.GetSize() > 1) // olUrnos format: "DEM.URNO,FLIGHT.URNO1,...,FLIGHT.URNOn"
					{
						if((llDemUrno = atol(olUrnos[0])) != 0L)
						{
							if(!olCommonCheckinDemandFlightsMap.Lookup((void *) llDemUrno, (void *&) polFlights))
							{
								polFlights = new CMapPtrToPtr;
								olCommonCheckinDemandFlightsMap.SetAt((void *)llDemUrno,polFlights);
							}
							for(int ilCCD = 1; ilCCD < olUrnos.GetSize(); ilCCD++)
							{
								if((llFlightUrno = atol(olUrnos[ilCCD])) != 0L)
									polFlights->SetAt((void *)llFlightUrno, NULL);
								if(!olAllFlightsMap.Lookup((void *) llFlightUrno, (void *&) pvlDummy))
									olAllFlightsMap.SetAt((void *) llFlightUrno, NULL);
							}
						}
					}
				}
			}

			// get additional jobs for all the flights that have check-in jobs (gate jobs and check-in jobs without demands)
			CCSPtrArray <JOBDATA> olAdditionalFlightJobs;
			CCSPtrArray <JOBDATA> olTmpJobs;
			for(POSITION rlPos = olAllFlightsMap.GetStartPosition(); rlPos != NULL; )
			{
				olTmpJobs.RemoveAll();
				long llFlightUrno;
				olAllFlightsMap.GetNextAssoc(rlPos,(void *&) llFlightUrno,(void *& )pvlDummy);
				ogJobData.GetJobsByFlur(olTmpJobs,llFlightUrno,FALSE,"","");
				for(int ilFJ = 0; ilFJ < olTmpJobs.GetSize(); ilFJ++)
				{
					JOBDATA *prlTmpJob = &olTmpJobs[ilFJ];
					if(!strcmp(prlTmpJob->Aloc,ALLOCUNITTYPE_GATE))
					{
						olAdditionalFlightJobs.Add(prlTmpJob);
					}
					else if(!strcmp(prlTmpJob->Aloc,ALLOCUNITTYPE_CIC) && ogJodData.GetDemandForJob(prlTmpJob) == NULL)
					{
						olAdditionalFlightJobs.Add(prlTmpJob);
					}
				}
			}
			bool blShiftFound;
			for(int ilAJ = 0; ilAJ < olAdditionalFlightJobs.GetSize(); ilAJ++)
			{
				JOBDATA *prlAdditionalFlightJob = &olAdditionalFlightJobs[ilAJ];
				blShiftFound = false;
				for(int ilSh = 0; ilSh < olPrintShifts.GetSize(); ilSh++)
				{
					prlPrintShift = &olPrintShifts[ilSh];
					if(prlPrintShift->prlShift->Urno == prlAdditionalFlightJob->Shur)
					{
						//prlPrintShift->olFlightJobs.Add(prlAdditionalFlightJob);
						prlPrintShift->AddJob(prlAdditionalFlightJob, prlPrintShift->olFlightJobs);
						blShiftFound = true;
						break;
					}
				}
				if(!blShiftFound)
				{
					// add the shift if not found
					if((prlShift = ogShiftData.GetShiftByUrno(prlAdditionalFlightJob->Shur)) != NULL)
					{
						if(IsBetween(prlShift->Avfa,opFrom,opTo))
						{
							if(!olPrintShiftMap.Lookup((void *) prlAdditionalFlightJob->Shur, (void *&) prlPrintShift))
							{
								prlPrintShift = new CCI_PRINTSHIFTS;
								prlPrintShift->prlShift = prlShift;
								prlPrintShift->olMainFunction = ogBasicData.GetFunctionForShift(prlShift);
								olPrintShifts.Add(prlPrintShift);
								olPrintShiftMap.SetAt((void *) prlAdditionalFlightJob->Shur, prlPrintShift);
							}
							//prlPrintShift->olFlightJobs.Add(prlAdditionalFlightJob);
							prlPrintShift->AddJob(prlAdditionalFlightJob, prlPrintShift->olFlightJobs);
						}
					}
				}
			}
			

			CCSPtrArray <JOBDATA> olJobs;
			// print flight indep/other jobs only for emps with cic jobs
			for(int ilS = 0; ilS < olPrintShifts.GetSize(); ilS++)
			{
				// other jobs
				prlPrintShift = &olPrintShifts[ilS];
				for(int ilOJ = 0; ilOJ < ogJobData.GetJobsByShurAndType(olJobs, prlPrintShift->prlShift->Urno, JOBSPECIAL); ilOJ++)
					prlPrintShift->AddJob(&olJobs[ilOJ], prlPrintShift->olOtherJobs);
//					prlPrintShift->olOtherJobs.Add(&olJobs[ilOJ]);
				prlPrintShift->olOtherJobs.Sort(ByStart);

				// flight independent jobs
				olJobs.RemoveAll();
				for(int ilFIJ = 0; ilFIJ < ogJobData.GetJobsByShurAndType(olJobs, prlPrintShift->prlShift->Urno, JOBFID); ilFIJ++)
				{
					if(strcmp(olJobs[ilFIJ].Aloc,ALLOCUNITTYPE_CIC)) // have already added the check-in FIDs
						prlPrintShift->AddJob(&olJobs[ilFIJ], prlPrintShift->olFlightIndependentJobs);
						//prlPrintShift->olFlightIndependentJobs.Add(&olJobs[ilFIJ]);
				}
				prlPrintShift->olFlightIndependentJobs.Sort(ByStart);

//				// gate jobs (also added to flight jobs)
//				olJobs.RemoveAll();
//				for(int ilGJ = 0; ilGJ < ogJobData.GetJobsByShurAndType(olJobs, prlPrintShift->prlShift->Urno, JOBFLIGHT); ilGJ++)
//				{
//					if(!strcmp(olJobs[ilGJ].Aloc,ALLOCUNITTYPE_GATE))
//						prlPrintShift->olFlightJobs.Add(&olJobs[ilGJ]);
//				}
			}
//			// print flight indep/other jobs for all emps 
//			for(int ilS = 0; ilS < ogShiftData.omData.GetSize(); ilS++)
//			{
//				SHIFTDATA *prlShift = &ogShiftData.omData[ilS];
//				if(!olPrintShiftMap.Lookup((void *) prlShift->Urno, (void *&) prlPrintShift))
//				{
//					prlPrintShift = new CCI_PRINTSHIFTS;
//					prlPrintShift->prlShift = prlShift;
//					prlPrintShift->olMainFunction = ogBasicData.GetFunctionForShift(prlShift);
//					olPrintShifts.Add(prlPrintShift);
//					olPrintShiftMap.SetAt((void *) prlJob->Shur, prlPrintShift);
//				}
//				for(int ilOJ = 0; ilOJ < ogJobData.GetJobsByShurAndType(olJobs, prlPrintShift->prlShift->Urno, JOBSPECIAL); ilOJ++)
//					prlPrintShift->olOtherJobs.Add(&olJobs[ilOJ]);
//				prlPrintShift->olOtherJobs.Sort(ByJobStart);
//
////				for(int ilFIJ = 0; ilFIJ < ogJobData.GetJobsByShurAndType(olJobs, prlPrintShift->prlShift->Urno, JOBFID); ilFIJ++)
////					prlPrintShift->olFlightIndependentJobs.Add(&olJobs[ilFIJ]);
////				prlPrintShift->olFlightIndependentJobs.Sort(ByJobStart);
//			}


			// filter out those functions not specified in the View Filter and set ilMainFunctionIndex (used for sorting)
			if(!bmUseAllFunctions)
			{
				for(int ilPS = (olPrintShifts.GetSize()-1); ilPS >= 0; ilPS--)
				{
					CCI_PRINTSHIFTS *prlPS = &olPrintShifts[ilPS];
					if(!omCMapForFunctions.Lookup(prlPS->olMainFunction, (void *&) prlPS->ilMainFunctionIndex))
					{
						olPrintShifts.RemoveAt(ilPS);
					}
				}
			}
			
			// sort by shift code and main function so that the emps are printed in the correct order
			if(bpSortByShift)
				olPrintShifts.Sort(ByShiftTimeAndFunc);
			else
				olPrintShifts.Sort(ByFuncAndShiftTime);

			if((ilNumShifts = olPrintShifts.GetSize()) <= 0 && pogMainWnd != NULL)
			{
				CString olText;
				olText.Format(GetString(IDS_NOSHIFTSTOPRINT), opFrom.Format("%H:%M %d.%m.%Y"), opTo.Format("%H:%M %d.%m.%Y"));
			
				if(!bmUseAllFunctions)
				{
					CString olFunction, olFunctions;
					void *pvlDummy;
					for(POSITION rlPos = omCMapForFunctions.GetStartPosition(); rlPos != NULL; )
					{
						omCMapForFunctions.GetNextAssoc(rlPos, olFunction, (void *& ) pvlDummy);
						if(!olFunctions.IsEmpty())
							olFunctions += ",";
						olFunctions += olFunction;
					}
					olText += CString("\nSelected Functions: ") + olFunctions;
				}
				pogMainWnd->MessageBox(olText, GetString(IDS_CCIGANTT_PRINT2), MB_ICONEXCLAMATION);
			}

			while(ilNumShifts > 0)
			{
				// calculate how many shifts can be displayed on this page and 
				// create a list of flights for the jobs for each shift, then sort the flights by STD
				CCSPtrArray <FLIGHTDATA> olFlights;
				CMapPtrToPtr olFlightMap; // map used so that the same flight is not added twice
				for(int ilShiftCnt = 0, ilLineCnt = 0; ilShiftCnt < ilNumShifts && ilLineCnt < ilMaxLines && 
					olFlightMap.GetCount() < ilMaxFlightsPerLine; ilShiftCnt++)
				{
					prlPrintShift = &olPrintShifts[ilShiftCnt];

					// make list of flights for flight jobs
					for(int ilF = (prlPrintShift->olFlightJobs.GetSize()-1); ilF >= 0 ; ilF--)
					{
						if(!olFlightMap.Lookup((void *) prlPrintShift->olFlightJobs[ilF].Flur, (void *&) pvlDummy))
						{
							if((prlFlight = ogFlightData.GetFlightByUrno(prlPrintShift->olFlightJobs[ilF].Flur)) != NULL)
							{
								olFlights.Add(prlFlight);
								olFlightMap.SetAt((void *) prlFlight->Urno, NULL);
							}
							else
							{
								prlPrintShift->olFlightJobs.RemoveAt(ilF);
							}
						}
					}
					
					// make list of flights for common checkin jobs
					for(int ilCCJ = (prlPrintShift->olCommonCheckinJobs.GetSize()-1); ilCCJ >= 0; ilCCJ--)
					{
						bool blFlightAdded = false;
						if((prlDemand = ogJodData.GetDemandForJob(&prlPrintShift->olCommonCheckinJobs[ilCCJ])) != NULL &&
							olCommonCheckinDemandFlightsMap.Lookup((void *) prlDemand->Urno, (void *&) polFlights))
						{
							for(POSITION rlPos = polFlights->GetStartPosition(); rlPos != NULL; )
							{
								long llFlightUrno;
								polFlights->GetNextAssoc(rlPos,(void *&) llFlightUrno,(void *& )pvlDummy);
								if(olFlightMap.Lookup((void *) llFlightUrno, (void *&) pvlDummy))
								{
									// flight was already added for previous employee so don't remove it
									// from this employee's common checkin job list else it won't be printed
									blFlightAdded = true;
								}
								else if((prlFlight = ogFlightData.GetFlightByUrno(llFlightUrno)) != NULL)
								{
									olFlights.Add(prlFlight);
									olFlightMap.SetAt((void *) prlFlight->Urno, NULL);
									blFlightAdded = true;
								}
							}
						}
						if(!blFlightAdded)
						{
							prlPrintShift->olCommonCheckinJobs.RemoveAt(ilCCJ);
						}
					}

					// calculate number of lines required by this shift line
					int ilNumLinesToPrintThisShift = max(prlPrintShift->olFlightIndependentJobs.GetSize(),prlPrintShift->olOtherJobs.GetSize());
					ilLineCnt += max(ilNumLinesToPrintThisShift,1);
				}

				for(int ilFF = 0; ilFF < olFlights.GetSize(); ilFF++)
					strcpy(olFlights[ilFF].Line,GetLineForFlight(olFlights[ilFF].Urno));

				if(bpSortByStd)
					olFlights.Sort(ByStod);
				else
					olFlights.Sort(ByCounter);


				// print the header, the list of flights on the top horizontal scale, the pax and column title
#ifndef DEBUG_PRINTING
				PrintStartOfPage(olFlights, ilMaxFlightsPerLine, bpDisplaySta, bpDisplayEta, bpDisplayStd, bpDisplayEtd, bpDisplayGate, bpDisplayArr);
#endif

				// start printing each shift line - print until all shifts have been removed:
				// - when an employee's job has been printed then the job is removed from olPrintShifts
				// - when all the employee's jobs have been removed, then the shift is removed from olPrintShifts
				int ilFrameTop = PRINT_FRAMEMEDIUM;
				int ilFrameBottom = PRINT_NOFRAME;
				for(ilShiftCnt = 0, ilLineCnt = 0; ilShiftCnt < ilNumShifts && ilLineCnt < ilMaxLines; )
				{
					prlPrintShift = &olPrintShifts[ilShiftCnt];

					// print the main function/employee name/shift time/break time
					if(ilFrameTop == PRINT_NOFRAME)
					{
#ifndef DEBUG_PRINTING
						AddPrintElement(rlPrintLine, olEmpty, imFuncLen, pomPrint->omSmallFont_Regular, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, ilFrameTop, ilFrameBottom);
						AddPrintElement(rlPrintLine, olEmpty, imNameLen, pomPrint->omSmallFont_Regular, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, ilFrameTop, ilFrameBottom);
						AddPrintElement(rlPrintLine, olEmpty, imShiftLen, pomPrint->omSmallFont_Regular, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, ilFrameTop, ilFrameBottom);
						AddPrintElement(rlPrintLine, olEmpty, imBreakLen, pomPrint->omSmallFont_Regular, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, ilFrameTop, ilFrameBottom);
#endif
					}
					else
					{
#ifndef DEBUG_PRINTING
						AddPrintElement(rlPrintLine, prlPrintShift->olMainFunction, imFuncLen, pomPrint->omSmallFont_Regular, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, ilFrameBottom);
						AddPrintElement(rlPrintLine, ogEmpData.GetEmpName(prlPrintShift->prlShift->Stfu), imNameLen, pomPrint->omSmallFont_Regular, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, ilFrameBottom);
						olText.Format("%s-%s", prlPrintShift->prlShift->Avfa.Format("%H%M/%d"), prlPrintShift->prlShift->Avta.Format("%H%M"));
						AddPrintElement(rlPrintLine, olText, imShiftLen, pomPrint->omSmallFont_Regular, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, ilFrameBottom);
#endif
						CCSPtrArray <JOBDATA> olBreaks;
						ogJobData.GetJobsByShurAndType(olBreaks, prlPrintShift->prlShift->Urno, JOBBREAK, true);
						if(olBreaks.GetSize() > 0) olText.Format("%s", olBreaks[0].Acfr.Format("%H%M"));
						else olText.Empty();
#ifndef DEBUG_PRINTING
						AddPrintElement(rlPrintLine, olText, imBreakLen, pomPrint->omSmallFont_Regular, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, ilFrameBottom);
#endif
					}
					// loop through flights and print which jobs are handled
					for(int ilF = 0; ilF < ilMaxFlightsPerLine; ilF++)
					{
						if(ilF >= olFlights.GetSize())
						{
							// no more flights so print an empty box
#ifndef DEBUG_PRINTING
							AddPrintElement(rlPrintLine, olEmpty, imFlightLen, pomPrint->omSmallFont_Regular, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, ilFrameTop, ilFrameBottom);
#endif
						}
						else
						{
							// see if there is a job for this flight-shift combination
							prlJob = NULL;
							for(int ilJ = 0; ilJ < prlPrintShift->olFlightJobs.GetSize(); ilJ++)
							{
								if(prlPrintShift->olFlightJobs[ilJ].Flur == olFlights[ilF].Urno)
								{
									prlJob = &prlPrintShift->olFlightJobs[ilJ];

									// remove the job so that it won't be printed again - when all jobs have been removed then the emp is removed
									prlPrintShift->olFlightJobs.RemoveAt(ilJ);
									break;
								}
							}

							// see if there is a common check-in job for this flight
							if(prlJob == NULL)
							{
								for(int ilJ = 0; ilJ < prlPrintShift->olCommonCheckinJobs.GetSize(); ilJ++)
								{
									if((prlDemand = ogJodData.GetDemandForJob(prlPrintShift->olCommonCheckinJobs[ilJ].Urno)) != NULL &&
										olCommonCheckinDemandFlightsMap.Lookup((void *) prlDemand->Urno, (void *&) polFlights) &&
										polFlights->Lookup((void *) olFlights[ilF].Urno, (void *&) polShifts))
									{
										if(polShifts == NULL)
										{
											polShifts = new CMapPtrToPtr;
											polFlights->SetAt((void *)olFlights[ilF].Urno,polShifts);
										}
										if(!polShifts->Lookup((void *) prlPrintShift->prlShift->Urno, pvlDummy))
										{
											prlJob = &prlPrintShift->olCommonCheckinJobs[ilJ];
											polShifts->SetAt((void *) prlPrintShift->prlShift->Urno, NULL);
										
											// for the specified common check-in demand check if the employee
											// has been printed for all flights for the demand...
											bool blShiftFoundForAllFlightsForThisDemand = true;
											for(rlPos = polFlights->GetStartPosition(); rlPos != NULL; )
											{
												long llFlightUrno;
												polFlights->GetNextAssoc(rlPos,(void *&) llFlightUrno,(void *& )polShifts);
												if(polShifts == NULL || !polShifts->Lookup((void *) prlPrintShift->prlShift->Urno, pvlDummy))
												{
													blShiftFoundForAllFlightsForThisDemand = false;
													break;
												}
											}
											if(blShiftFoundForAllFlightsForThisDemand)
											{
												// ... remove the job so that it won't be printed again - when all jobs have been removed then the emp is removed
												prlPrintShift->olCommonCheckinJobs.RemoveAt(ilJ);
											}
										}
									}
								}
							}

							if(prlJob == NULL)
							{
								// no job for this flight so print empty box
#ifndef DEBUG_PRINTING
								AddPrintElement(rlPrintLine, olEmpty, imFlightLen, pomPrint->omSmallFont_Regular, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, ilFrameTop, ilFrameBottom);
#endif
							}
							else
							{
								if((prlDemand = ogJodData.GetDemandForJob(prlJob)) != NULL &&
									(prlRud = ogRudData.GetRudByUrno(prlDemand->Urud)) != NULL &&
									(prlSer = ogSerData.GetSerByUrno(prlRud->Ughs)) != NULL)
								{
									//olText = prlSer->Snam;
									olText = prlSer->Seco;
								}
								else if(prlDemand == NULL)
								{
									olText = "JWD"; // job without demand
								}
								else
								{
									olText = "???";
								}

#ifndef DEBUG_PRINTING
								AddPrintElement(rlPrintLine, olText.Left(3), imFlightLen, pomPrint->omSmallFont_Regular, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, ilFrameTop, ilFrameBottom);
#endif
							}
						}
					}


					// print flight independent job
					if(prlPrintShift->olFlightIndependentJobs.GetSize() > 0)
					{
						int ilOldFrameBottom = ilFrameBottom;
						if(prlPrintShift->olFlightIndependentJobs[0].DrawLine)
							ilFrameBottom = PRINT_FRAMEMEDIUM;
#ifndef DEBUG_PRINTING
						AddPrintElement(rlPrintLine, prlPrintShift->olFlightIndependentJobs[0].Text, imFidLen, pomPrint->omCourierNormal7, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, ilFrameTop, ilFrameBottom);
#endif
						prlPrintShift->olFlightIndependentJobs.DeleteAt(0);
						ilFrameBottom = ilOldFrameBottom;
					}
					else
					{
#ifndef DEBUG_PRINTING
						AddPrintElement(rlPrintLine, olEmpty, imFidLen, pomPrint->omSmallFont_Regular, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, ilFrameTop, ilFrameBottom);
#endif
					}

					// print other job
					if(prlPrintShift->olOtherJobs.GetSize() > 0)
					{
						int ilOldFrameBottom = ilFrameBottom;
						if(prlPrintShift->olOtherJobs[0].DrawLine)
							ilFrameBottom = PRINT_FRAMEMEDIUM;
#ifndef DEBUG_PRINTING
						AddPrintElement(rlPrintLine, prlPrintShift->olOtherJobs[0].Text, imOtherLen, pomPrint->omCourierNormal7, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, ilFrameTop, ilFrameBottom);
#endif
						prlPrintShift->olOtherJobs.DeleteAt(0);
						ilFrameBottom = ilOldFrameBottom;
					}
					else
					{
#ifndef DEBUG_PRINTING
						AddPrintElement(rlPrintLine, olEmpty, imOtherLen, pomPrint->omSmallFont_Regular, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, ilFrameTop, ilFrameBottom);
#endif
					}

					// print the prepared line
#ifndef DEBUG_PRINTING
					pomPrint->PrintLine(rlPrintLine);
					rlPrintLine.DeleteAll();
#endif
					ilLineCnt++;

					if(NoMoreJobsToPrint(prlPrintShift))
					{
						ilFrameTop = PRINT_FRAMEMEDIUM;
						ilShiftCnt++;
					}
					else
					{
						ilFrameTop = PRINT_NOFRAME;
					}

//					// print any extra flight independent jobs or other jobs
//					while(ilLineCnt < ilMaxLines && (prlPrintShift->olOtherJobs.GetSize() > 0 || prlPrintShift->olFlightIndependentJobs.GetSize() > 0))
//					{
//						ilFrameTop = PRINT_NOFRAME;
//#ifndef DEBUG_PRINTING
//						AddPrintElement(rlPrintLine, olEmpty, imFuncLen, pomPrint->omSmallFont_Regular, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, ilFrameTop, ilFrameBottom);
//						AddPrintElement(rlPrintLine, olEmpty, imNameLen, pomPrint->omSmallFont_Regular, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, ilFrameTop, ilFrameBottom);
//						AddPrintElement(rlPrintLine, olEmpty, imShiftLen, pomPrint->omSmallFont_Regular, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, ilFrameTop, ilFrameBottom);
//						AddPrintElement(rlPrintLine, olEmpty, imBreakLen, pomPrint->omSmallFont_Regular, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, ilFrameTop, ilFrameBottom);
//						for(int ilE = 0; ilE < ilMaxFlightsPerLine; ilE++)
//							AddPrintElement(rlPrintLine, olEmpty, imFlightLen, pomPrint->omSmallFont_Regular, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, ilFrameTop, ilFrameBottom);
//#endif
//						if(prlPrintShift->olFlightIndependentJobs.GetSize() > 0)
//						{
//#ifndef DEBUG_PRINTING
//							AddPrintElement(rlPrintLine, ogDataSet.JobBarText(&prlPrintShift->olFlightIndependentJobs[0]), imFidLen, pomPrint->omSmallFont_Regular, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, ilFrameTop, ilFrameBottom);
//#endif
//							prlPrintShift->olFlightIndependentJobs.RemoveAt(0);
//						}
//						else
//						{
//#ifndef DEBUG_PRINTING
//							AddPrintElement(rlPrintLine, olEmpty, imFidLen, pomPrint->omSmallFont_Regular, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, ilFrameTop, ilFrameBottom);
//#endif
//						}
//
//						if(prlPrintShift->olOtherJobs.GetSize() > 0)
//						{
//#ifndef DEBUG_PRINTING
//							AddPrintElement(rlPrintLine, ogDataSet.JobBarText(&prlPrintShift->olOtherJobs[0]), imOtherLen, pomPrint->omSmallFont_Regular, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, ilFrameTop, ilFrameBottom);
//#endif
//							prlPrintShift->olOtherJobs.RemoveAt(0);
//						}
//						else
//						{
//#ifndef DEBUG_PRINTING
//							AddPrintElement(rlPrintLine, olEmpty, imOtherLen, pomPrint->omSmallFont_Regular, PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, ilFrameTop, ilFrameBottom);
//#endif
//						}
//						// print the prepared line
//#ifndef DEBUG_PRINTING
//						pomPrint->PrintLine(rlPrintLine);
//						rlPrintLine.DeleteAll();
//#endif
//						ilLineCnt++;
//					}

				} // end page

#ifndef DEBUG_PRINTING
				if(pomPrint->imLineNo != 0)
				{
					pomPrint->imLineNo = pomPrint->imMaxLines + 1;
					PrintEndOfPage();
				}
#endif

				// remove shifts whose jobs have all been printed
				for(ilShiftCnt = min(ilNumShifts,ilMaxLines) - 1; ilShiftCnt >= 0; ilShiftCnt--)
				{
					if(NoMoreJobsToPrint(&olPrintShifts[ilShiftCnt]))
						olPrintShifts.DeleteAt(ilShiftCnt);
				}
				ilNumShifts = olPrintShifts.GetSize();

			} // end while(ilNumShifts > 0)

#ifndef DEBUG_PRINTING
			if(pomPrint->imLineNo != 0)
			{
				pomPrint->imLineNo = pomPrint->imMaxLines + 1;
				PrintEndOfPage();
			}
			pomPrint->omCdc.EndDoc();
#endif
			for(rlPos = olCommonCheckinDemandFlightsMap.GetStartPosition(); rlPos != NULL; )
			{
				long llDemUrno;
				olCommonCheckinDemandFlightsMap.GetNextAssoc(rlPos,(void *&) llDemUrno,(void *& )polFlights);
				for(POSITION rlPos2 = polFlights->GetStartPosition(); rlPos2 != NULL; )
				{
					long llFlightUrno;
					polFlights->GetNextAssoc(rlPos2,(void *&) llFlightUrno,(void *& )polShifts);
					if(polShifts != NULL)
					{
						polShifts->RemoveAll();
						delete polShifts;
					}
				}
				polFlights->RemoveAll();
				delete polFlights;
			}
			olCommonCheckinDemandFlightsMap.RemoveAll();

			// release memory used by employee shift/jobs lists
			olPrintShifts.DeleteAll();
		}
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
#ifndef DEBUG_PRINTING
		delete pomPrint;
		pomPrint = NULL;
#endif
	}
}

bool CciDiagramViewer::NoMoreJobsToPrint(CCI_PRINTSHIFTS *prpPrintShift)
{
	if(prpPrintShift->olFlightJobs.GetSize() <= 0 && prpPrintShift->olFlightIndependentJobs.GetSize() <= 0  && 
		prpPrintShift->olOtherJobs.GetSize() <= 0 && prpPrintShift->olCommonCheckinJobs.GetSize() <= 0)
		return true;
	return false;
}

bool CciDiagramViewer::PrintStartOfPage(CCSPtrArray <FLIGHTDATA> &ropFlights, int ipMaxFlightsPerLine, bool bpDisplaySta, bool bpDisplayEta, bool bpDisplayStd, bool bpDisplayEtd, bool bpDisplayGate, bool bpDisplayArr)
{
	bool blStartOfPage = false;
	if(pomPrint->imLineNo == 0)
	{
		blStartOfPage = true;
		CString olText;
		PrintHeader(pomPrint);

		CCSPtrArray <PRINTELEDATA> rlPrintLine;
		PRINTELEDATA rlElement;

		for(int ilLine = 0; ilLine < 12; ilLine++)
		{
			bool blPrintLine = true;
			// empty space above where the funct/emp/shift columns go
			if(ilLine == 3)
			{
				if(bpDisplaySta)
				{
					olText.Empty();
					AddPrintElement(rlPrintLine, olText, imFuncLen + imNameLen + imShiftLen, pomPrint->omSmallFont_Regular, 
									PRINT_LEFT, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME);
					olText = GetString(IDS_CICPRINTDLG_STA);
					AddPrintElement(rlPrintLine, olText, imBreakLen, pomPrint->omSmallFont_Regular, 
									PRINT_LEFT, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME);
				}
				else
				{
					blPrintLine = false;
				}
			}
			else if(ilLine == 4)
			{
				if(bpDisplayEta)
				{
					olText.Empty();
					AddPrintElement(rlPrintLine, olText, imFuncLen + imNameLen + imShiftLen, pomPrint->omSmallFont_Regular, 
									PRINT_LEFT, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME);
					olText = GetString(IDS_CICPRINTDLG_ETA);
					AddPrintElement(rlPrintLine, olText, imBreakLen, pomPrint->omSmallFont_Regular, 
									PRINT_LEFT, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME);
				}
				else
				{
					blPrintLine = false;
				}
			}
			else if(ilLine == 5)
			{
				if(bpDisplayStd)
				{
					olText.Empty();
					AddPrintElement(rlPrintLine, olText, imFuncLen + imNameLen + imShiftLen, pomPrint->omSmallFont_Regular, 
									PRINT_LEFT, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME);
					olText = GetString(IDS_CICPRINTDLG_STD);
					AddPrintElement(rlPrintLine, olText, imBreakLen, pomPrint->omSmallFont_Regular, 
									PRINT_LEFT, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME);
				}
				else
				{
					blPrintLine = false;
				}
			}
			else if(ilLine == 6)
			{
				if(bpDisplayEtd)
				{
					olText.Empty();
					AddPrintElement(rlPrintLine, olText, imFuncLen + imNameLen + imShiftLen, pomPrint->omSmallFont_Regular, 
									PRINT_LEFT, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME);
					olText = GetString(IDS_CICPRINTDLG_ETD);
					AddPrintElement(rlPrintLine, olText, imBreakLen, pomPrint->omSmallFont_Regular, 
									PRINT_LEFT, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME);
				}
				else
				{
					blPrintLine = false;
				}
			}
			else if(ilLine == 7)
			{
				if(bpDisplayGate)
				{
					olText.Empty();
					AddPrintElement(rlPrintLine, olText, imFuncLen + imNameLen + imShiftLen, pomPrint->omSmallFont_Regular, 
									PRINT_LEFT, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME);
					olText = GetString(IDS_CICPRINTDLG_GATE);
					AddPrintElement(rlPrintLine, olText, imBreakLen, pomPrint->omSmallFont_Regular, 
									PRINT_LEFT, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME);
				}
				else
				{
					blPrintLine = false;
				}
			}
			else if(ilLine == 8)
			{
				olText.Empty();
				AddPrintElement(rlPrintLine, olText, imFuncLen + imNameLen + imShiftLen, pomPrint->omSmallFont_Regular, 
								PRINT_LEFT, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME);
				olText = GetString(IDS_CICPRINT_ROW);
				AddPrintElement(rlPrintLine, olText, imBreakLen, pomPrint->omSmallFont_Regular, 
								PRINT_LEFT, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME);
			}
			else if(ilLine == 11)
			{
				AddPrintElement(rlPrintLine, GetString(IDS_CCICHART_PRINTFUNC), imFuncLen, pomPrint->omSmallFont_Regular, 
								PRINT_LEFT, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME);
				AddPrintElement(rlPrintLine, GetString(IDS_CCICHART_PRINTNAME), imNameLen, pomPrint->omSmallFont_Regular, 
								PRINT_LEFT, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME);
				AddPrintElement(rlPrintLine, GetString(IDS_CCICHART_PRINTSHIFT), imShiftLen, pomPrint->omSmallFont_Regular, 
								PRINT_LEFT, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME);
				AddPrintElement(rlPrintLine, GetString(IDS_CCICHART_PRINTBREAK), imBreakLen, pomPrint->omSmallFont_Regular, 
								PRINT_LEFT, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME);
			}
			else
			{
				if(ilLine == 2)
					blPrintLine = bpDisplayArr;

				olText.Empty();
				AddPrintElement(rlPrintLine, olText, imFuncLen + imNameLen + imShiftLen + imBreakLen, pomPrint->omSmallFont_Regular, 
								PRINT_LEFT, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME);
			}

			int ilPax = 0;
			bool blLineAdded = true;
			FLIGHTDATA *prlFlight = NULL, *prlRotation = NULL;
			int ilTopFrame = (ilLine == 1 || (bpDisplayArr && ilLine == 2)) ? PRINT_NOFRAME : PRINT_FRAMEMEDIUM;
			for(int ilF = 0; ilF < ipMaxFlightsPerLine; ilF++)
			{
				olText.Empty();
				bool blHighlight = false;
				if(ilF < ropFlights.GetSize())
				{
					prlFlight = &ropFlights[ilF];
					switch(ilLine)
					{
					case 0:
						olText = prlFlight->Alc2;
						break;
					case 1:
						if(bpDisplayArr && (prlRotation = ogFlightData.GetRotationFlight(prlFlight)) != NULL)
						{
							if(ogFlightData.IsArrival(prlFlight))
							{
								olText = prlFlight->Fltn;
								blHighlight = true;
							}
							else
							{
								olText = prlRotation->Fltn;
							}
						}
						else
						{
							olText = prlFlight->Fltn;
						}
						break;
					case 2:
						if(bpDisplayArr && (prlRotation = ogFlightData.GetRotationFlight(prlFlight)) != NULL)
						{
							if(ogFlightData.IsDeparture(prlFlight))
							{
								olText = prlFlight->Fltn;
								blHighlight = true;
							}
							else
							{
								olText = prlRotation->Fltn;
							}
						}
						else
						{
							olText = "";
						}
						break;
					case 3:
						if(ogFlightData.IsArrival(prlFlight))
							olText = prlFlight->Stoa.Format("%H%M");
						else if(bpDisplayArr && (prlRotation = ogFlightData.GetRotationFlight(prlFlight)) != NULL)
							olText = prlRotation->Stoa.Format("%H%M");
						else
							olText = "";
						break;
					case 4:
						if(ogFlightData.IsArrival(prlFlight))
							olText = prlFlight->Etoa.Format("%H%M");
						else if(bpDisplayArr && (prlRotation = ogFlightData.GetRotationFlight(prlFlight)) != NULL)
							olText = prlRotation->Etoa.Format("%H%M");
						else
							olText = "";
						break;
					case 5:
						if(ogFlightData.IsDeparture(prlFlight))
							olText = prlFlight->Stod.Format("%H%M");
						else if(bpDisplayArr && (prlRotation = ogFlightData.GetRotationFlight(prlFlight)) != NULL)
							olText = prlRotation->Stod.Format("%H%M");
						else
							olText = "";
						break;
					case 6:
						if(ogFlightData.IsDeparture(prlFlight))
							olText = prlFlight->Etod.Format("%H%M");
						else if(bpDisplayArr && (prlRotation = ogFlightData.GetRotationFlight(prlFlight)) != NULL)
							olText = prlRotation->Etod.Format("%H%M");
						else
							olText = "";
						break;
					case 7:
						if(ogFlightData.IsDeparture(prlFlight))
							olText = prlFlight->Gtd1;
						else
							olText = prlFlight->Gta1;
						break;
					case 8:
						//olText = GetLineForFlight(prlFlight->Urno);
						olText = prlFlight->Line;
						break;
					case 9:
						if((ilPax = ogPaxData.GetBookedPassengers(prlFlight->Urno,PAX_FIRST)) > 0)
							olText.Format("%d", ilPax);
						break;
					case 10:
						if((ilPax = ogPaxData.GetBookedPassengers(prlFlight->Urno,PAX_BUSINESS)) > 0)
							olText.Format("%d", ilPax);
						break;
					case 11:
						if((ilPax = ogPaxData.GetBookedPassengers(prlFlight->Urno,PAX_ECONOMY)) > 0)
							olText.Format("%d", ilPax);
						break;
					}
				}

				AddPrintElement(rlPrintLine, olText, imFlightLen, blHighlight ? pomPrint->omSmallFont_UnderlineBold : pomPrint->omSmallFont_Regular, 
					PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM, ilTopFrame, PRINT_NOFRAME);
			}

//			AddPrintElement(rlPrintLine, CString(" "), imFidLen, pomPrint->omSmallFont_Regular, 
//				PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM,  ilTopFrame, PRINT_NOFRAME);
//
//			AddPrintElement(rlPrintLine, CString(" "), imOtherLen, pomPrint->omSmallFont_Regular, 
//				PRINT_LEFT, PRINT_FRAMEMEDIUM, PRINT_FRAMEMEDIUM,  ilTopFrame, PRINT_NOFRAME);

			if(ilLine == 11)
			{
				AddPrintElement(rlPrintLine, GetString(IDS_CCICHART_PRINTFLIND), imFidLen, pomPrint->omSmallFont_Regular, 
								PRINT_LEFT, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME);
				AddPrintElement(rlPrintLine, GetString(IDS_CCICHART_PRINTOTHER), imOtherLen, pomPrint->omSmallFont_Regular, 
								PRINT_LEFT, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_NOFRAME);
			}

			if(blPrintLine)
				pomPrint->PrintLine(rlPrintLine);
			rlPrintLine.DeleteAll();
		}
	}

	return blStartOfPage;
}

void CciDiagramViewer::PrintEndOfPage()
{
	if(pomPrint->imLineNo >= pomPrint->imMaxLines &&  pomPrint->imPageNo > 0)
	{
		CCSPtrArray <PRINTELEDATA> rlPrintLine;
		PRINTELEDATA rlElement;

		AddPrintElement(rlPrintLine, CString(" "), imTotalLen, pomPrint->omMediumFont_Bold, 
						PRINT_LEFT, PRINT_NOFRAME, PRINT_NOFRAME, PRINT_FRAMEMEDIUM, PRINT_NOFRAME);

		pomPrint->PrintLine(rlPrintLine);
		rlPrintLine.DeleteAll();

		pomPrint->PrintFooter("","");
		pomPrint->omCdc.EndPage();
		pomPrint->imLineNo = 0;
		pomPrint->imLineTop = pomPrint->imFirstLine;
	}
}

void CciDiagramViewer::AddPrintElement(CCSPtrArray <PRINTELEDATA> &ropPrintLine, CString &ropText, int ipLength, 
									   CFont &ropFont, int ipAlignment /* = PRINT_LEFT */, 
									   int ipFrameLeft /* = PRINT_FRAMEMEDIUM */, int ipFrameRight /* = PRINT_FRAMEMEDIUM */, 
									   int ipFrameTop /* = PRINT_FRAMEMEDIUM */, int ipFrameBottom /* = PRINT_FRAMEMEDIUM */)
{
	PRINTELEDATA rlElement;
	rlElement.Length		= ipLength;
	rlElement.Text			= ropText;
	rlElement.Alignment		= ipAlignment;
	rlElement.FrameLeft		= ipFrameLeft;
	rlElement.FrameRight	= ipFrameRight;
	rlElement.FrameTop		= ipFrameTop;
	rlElement.FrameBottom	= ipFrameBottom;
	rlElement.pFont			= &ropFont;
	ropPrintLine.NewAt(ropPrintLine.GetSize(),rlElement);
}

BOOL CciDiagramViewer::PrintHeader(CCSPrint *pomPrint)
{
	pomPrint->omCdc.StartPage();
	pomPrint->PrintHeader();
	return TRUE;
}

CString CciDiagramViewer::GetLineForFlight(long lpFlightUrno)
{
	CString olLine;
	CCSPtrArray <CCADATA> olCcaList;
	CICDATA *prlCic = NULL;
	if(ogCcaData.GetCcaListByUaft(lpFlightUrno, olCcaList) > 0)
	{
		// get the line/row for the flight
		for(int i = 0; i < olCcaList.GetSize(); i++)
		{
			if((prlCic = ogCicData.GetCicByName(olCcaList[0].Cnam)) != NULL)
			{
				olLine = prlCic->Line;
				break;
			}
		}
	}
	if(olLine.IsEmpty())
	{
		// no line/row found for the flight, check if any CIC jobs are allocated to counters
		CCSPtrArray <JOBDATA> olJobs;
		ogJobData.GetJobsByFlur(olJobs, lpFlightUrno, FALSE, "", ALLOCUNITTYPE_CIC);
		for(int ilJ = 0; ilJ < olJobs.GetSize(); ilJ++)
		{
			if(strcmp(olJobs[ilJ].Alid,"") && (prlCic = ogCicData.GetCicByName(olJobs[ilJ].Alid)) != NULL)
			{
				olLine = prlCic->Line;
				break;
			}
		}
	}

	return olLine;
}