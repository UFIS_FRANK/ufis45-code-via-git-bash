// buttonct.cpp : implementation file
//

#include <stdafx.h>
#include <buttonct.h>
#include <ccsglobl.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

#define MoveTo(a,b,c) {::MoveToEx(a,b,c,NULL);}
#define GetTextExtent(a,b,c) {SIZE rlSize;::GetTextExtentPoint32(a,b,c, &rlSize);}
/////////////////////////////////////////////////////////////////////////////
// ButtonCtl

IMPLEMENT_DYNCREATE(ButtonCtl, CButton)

ButtonCtl::ButtonCtl()
{
    m_BtnFace =   ::GetSysColor(COLOR_BTNFACE);;
    m_BtnShadow =  ::GetSysColor(COLOR_BTNSHADOW);
    m_BtnHilite = ::GetSysColor(COLOR_BTNHIGHLIGHT);
	bmIsRecessed = FALSE;
}

ButtonCtl::~ButtonCtl()
{
}


BEGIN_MESSAGE_MAP(ButtonCtl, CButton)
    //{{AFX_MSG_MAP(ButtonCtl)
	ON_WM_ERASEBKGND()
	ON_WM_RBUTTONDOWN()
	ON_WM_GETDLGCODE()
	ON_WM_KILLFOCUS()
	ON_WM_LBUTTONUP()
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


void ButtonCtl::Recess(BOOL bpIsRecessed)
{
	bmIsRecessed = bpIsRecessed;
	SetState(bpIsRecessed);
	Invalidate(TRUE);
}

BOOL ButtonCtl::Recess()
{
   bmIsRecessed = GetState();
   return bmIsRecessed;
}

void ButtonCtl::SetColors(COLORREF lpBtnFace,COLORREF lpBtnShadow,COLORREF BtnHilite)
{
	m_BtnFace =  lpBtnFace;
    m_BtnShadow = lpBtnShadow;
    m_BtnHilite = BtnHilite;
	if (::IsWindow(this->GetSafeHwnd()))
	{
		Invalidate();
	}
}

/////////////////////////////////////////////////////////////////////////////
// ButtonCtl message handlers

void ButtonCtl::DrawItem(LPDRAWITEMSTRUCT lpDIS)
{   
    //CDC *pDC = CDC::FromHandle(lpDIS->hDC);
    HDC dc = lpDIS->hDC;
    
    UINT state = lpDIS->itemState;
    CRect rect(&(lpDIS->rcItem));

	COLORREF    clrBtnFace =   m_BtnFace;
    COLORREF    clrBtnShadow = m_BtnShadow;
    COLORREF    clrBtnHilite = m_BtnHilite;
    
    HBRUSH      hbrBtnFace = ::CreateSolidBrush(clrBtnFace);
    HBRUSH      hbrBtnHilite = ::CreateSolidBrush(clrBtnHilite);
    HBRUSH      hbrBtnShadow = ::CreateSolidBrush(clrBtnShadow);

    // Fill the entire button with the button brush (gray)
    HBRUSH hOldBrush = (HBRUSH) ::SelectObject(dc, hbrBtnFace );

    CRect InnerRect = rect;
    InnerRect.InflateRect(-2,-2);
    FillRect(dc, InnerRect, hbrBtnFace);
    
    // Draw the outer border first (thick if we're focused)
    BOOL bThick = state & ODS_FOCUS;
    
    DrawOuterFrame(dc, rect, FALSE /* bThick */);
    
    // The rect is now inset from the border either 1 or 2 pixels
    // Draw the highlight and shadow next
   // if (state & ODS_SELECTED)
	if (bmIsRecessed) // use our own state info
    {
        HPEN shadowPen = ::CreatePen(PS_SOLID, 1, clrBtnShadow);
        HPEN OldPen = (HPEN) ::SelectObject(dc, shadowPen);

        MoveTo(dc, rect.left, rect.bottom-1);
		::LineTo(dc, rect.left, rect.top);
        ::LineTo(dc, rect.right, rect.top);      
        ::SelectObject(dc, OldPen);
        ::DeleteObject(shadowPen);
    } else {
        // Button is up
        HPEN hilitePen = ::CreatePen(PS_SOLID, 2, clrBtnHilite);
        HPEN shadowPen = ::CreatePen(PS_SOLID, 1, clrBtnShadow);
        
        HPEN OldPen = (HPEN) ::SelectObject(dc, hilitePen);

        // Draw the hilite (white) part with the 2 pixel width pen
        ::Rectangle(dc, rect.left+1, rect.top+1, rect.right, rect.bottom);       

        // Draw the shadow on the bottom half
        ::SelectObject(dc, shadowPen);

        MoveTo(dc, rect.left, rect.bottom-1);
        ::LineTo(dc, rect.right-1, rect.bottom-1);
        ::LineTo(dc, rect.right-1, rect.top-1);
        
        MoveTo(dc, rect.left+1, rect.bottom-2);
        ::LineTo(dc, rect.right-2, rect.bottom-2);
        ::LineTo(dc, rect.right-2, rect.top);
        
        ::SelectObject(dc, OldPen);
        ::DeleteObject(hilitePen);
        ::DeleteObject(shadowPen);
    }  

    // Calculate the width and height of the text
    CDC *pDC = CDC::FromHandle(lpDIS->hDC);
    CString text;
    CFont *polOldFont = pDC->SelectObject(&ogScalingFonts[MS_SANS8]);
	
    GetWindowText(text);
    //CSize szText = GetTextExtent(dc, text, text.GetLength());
	SIZE rlSize;
	GetTextExtentPoint32(dc, text, text.GetLength(), &rlSize);
	CSize szText(rlSize); 

    CRect textRect = rect;
    textRect.InflateRect(-((rect.Width() - szText.cx)/2), 
        -((rect.Height() - szText.cy)/2));

    // Draw the text
    ::SetBkMode(dc, TRANSPARENT);
    //pDC->SetBkColor ( RGB ( 255, 255, 0));
    if (state & ODS_SELECTED)
        textRect.OffsetRect(1, 1);
    // Draw the text
    DrawText(dc, text, -1, textRect, DT_SINGLELINE | DT_CENTER);
 
	pDC->SelectObject(polOldFont);

	if (bThick)
	{
    	HPEN blackPen = CreatePen(PS_DOT, 1, BLACK);
	    HPEN olOldPen = (HPEN) ::SelectObject(dc, blackPen);
	    textRect.left--;
	    textRect.right += 2;
	   	DrawFocusRect(dc, &textRect);
	    SelectObject(dc, olOldPen);
	    DeleteObject(blackPen);
    }

  	SelectObject(dc, hOldBrush);
  	
    ::DeleteObject((HGDIOBJ) hbrBtnFace);
    ::DeleteObject((HGDIOBJ) hbrBtnShadow);
    ::DeleteObject((HGDIOBJ) hbrBtnHilite);
}

void ButtonCtl::DrawOuterFrame(HDC dc, CRect& rect, BOOL bThick)
{
    HPEN blackPen = ::CreatePen(PS_SOLID, 1, BLACK);
    HBRUSH brBlack = ::CreateSolidBrush(BLACK);
    
    // Draw the first outside rectangle
    HPEN OldPen = (HPEN) ::SelectObject(dc, (HGDIOBJ)blackPen);
    
    // top side
    MoveTo(dc, rect.left+1, rect.top); 
    ::LineTo(dc, rect.right-1, rect.top);
    
    // right side
    MoveTo(dc, rect.right-1, rect.top+1);
    ::LineTo(dc, rect.right-1, rect.bottom-1);

    // bottom side  
    MoveTo(dc, rect.right-2, rect.bottom-1); 
    ::LineTo(dc, rect.left, rect.bottom-1);

    // left side    
    MoveTo(dc, rect.left, rect.bottom-2);
    ::LineTo(dc, rect.left, rect.top);

    rect.InflateRect(-1, -1);
    if (bThick)
    {
        ::FrameRect(dc, rect, brBlack);
        rect.InflateRect(-1, -1);
    }

    ::SelectObject(dc, OldPen);
    ::DeleteObject(brBlack);
    ::DeleteObject(blackPen);
}

BOOL ButtonCtl::OnEraseBkgnd(CDC* pDC)
{
	return TRUE;
}

void ButtonCtl::OnRButtonDown(UINT nFlags, CPoint point) 
{
	long lParam = MAKELPARAM(point.x,point.y); 
    GetParent()->SendMessage(WM_CCSBUTTON_RBUTTONDOWN,nFlags,(LPARAM) lParam);
}

void ButtonCtl::OnLButtonUp(UINT nFlags, CPoint point)
{
    CButton::OnLButtonUp(nFlags, point);
    CButton::SetState(bmIsRecessed);    // redraw button
}                                          

void ButtonCtl::OnKillFocus(CWnd* pNewWnd)
{
    CButton::SetState(FALSE);   // allows OnKillFocus() to work correctly
    CButton::OnKillFocus(pNewWnd);
    CButton::SetState(bmIsRecessed);    // redraw button
}

UINT ButtonCtl::OnGetDlgCode() 
{
    // Disallow the button for being a default button.
    // Since Windows has some bugs when tab processing on a a focused button
    // has a discrepancy. (It's usually draw that push button as a default
    // button, and does not clear that button to be normal again.)
    //
    return CButton::OnGetDlgCode() & ~(DLGC_DEFPUSHBUTTON | DLGC_UNDEFPUSHBUTTON);
}

void ButtonCtl::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	GetParent()->SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
}

void ButtonCtl::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	GetParent()->SendMessage(WM_KEYUP, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
}
