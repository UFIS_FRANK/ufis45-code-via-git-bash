// PoolJobDemandAssignments.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <PoolJobDemandAssignments.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPoolJobDemandAssignments


CPoolJobDemandAssignments::CPoolJobDemandAssignments()
{
}

CPoolJobDemandAssignments::~CPoolJobDemandAssignments()
{
	while(omData.GetSize() > 0)
		omData.DeleteAt(0);
}

POOJJOBDEMANDASSIGNMENTS *CPoolJobDemandAssignments::Add(JOBDATA *prpPoolJob, DEMANDDATA *prpDemand)
{
	ASSERT(prpPoolJob);

	POOJJOBDEMANDASSIGNMENTS *prlPoolJobDemandAssignment = new POOJJOBDEMANDASSIGNMENTS;

	prlPoolJobDemandAssignment->PoolJob = prpPoolJob;
	prlPoolJobDemandAssignment->Demand = prpDemand;

	omData.Add(prlPoolJobDemandAssignment);

	return prlPoolJobDemandAssignment;
}

bool CPoolJobDemandAssignments::PoolJobAssignmentExists(long lpPoolJobUrno)
{
	for(int i = 0; i < omData.GetSize(); i++)
		if(omData[i].PoolJob->Urno == lpPoolJobUrno)
			return true;

	return false;
}

int CPoolJobDemandAssignments::GetCount(void)
{
	return omData.GetSize();
}

JOBDATA *CPoolJobDemandAssignments::GetPoolJob(int ilIndex)
{
	if(ilIndex >= 0 && ilIndex < GetCount())
		return omData[ilIndex].PoolJob;
	return NULL;
}

DEMANDDATA *CPoolJobDemandAssignments::GetDemand(int ilIndex)
{
	if(ilIndex >= 0 && ilIndex < GetCount())
		return omData[ilIndex].Demand;
	return NULL;
}