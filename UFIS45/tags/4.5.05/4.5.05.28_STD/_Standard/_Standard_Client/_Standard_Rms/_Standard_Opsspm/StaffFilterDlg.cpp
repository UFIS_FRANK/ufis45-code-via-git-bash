// dstffilt.cpp : implementation file
//

#include <stdafx.h>
#include <OpssPm.h>
#include <StaffFilterDlg.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CStaffFilterDialog dialog


CStaffFilterDialog::CStaffFilterDialog(CWnd* pParent /*=NULL*/)
    : CDialog(CStaffFilterDialog::IDD, pParent)
{
    //{{AFX_DATA_INIT(CStaffFilterDialog)
    m_bEarlyShift = FALSE;
    m_bLateShift = FALSE;
    m_bStationA = FALSE;
    m_bStationB = FALSE;
    //}}AFX_DATA_INIT
}

void CStaffFilterDialog::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CStaffFilterDialog)
    DDX_Check(pDX, IDC_EARLYSHIFT, m_bEarlyShift);
    DDX_Check(pDX, IDC_LATESHIFT, m_bLateShift);
    DDX_Check(pDX, IDC_STATIONA, m_bStationA);
    DDX_Check(pDX, IDC_STATIONB, m_bStationB);
    //}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CStaffFilterDialog, CDialog)
    //{{AFX_MSG_MAP(CStaffFilterDialog)
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CStaffFilterDialog message handlers

BOOL CStaffFilterDialog::OnInitDialog()
{
    CDialog::OnInitDialog();

    // move the window to be at the center over the parent window
    CRect rect, rectParent;
    GetWindowRect(&rect);
    GetParent()->GetWindowRect(&rectParent);
    int ilHorzOffset = (rectParent.Width() - rect.Width()) / 2;
    int ilVertOffset = (rectParent.Height() - rect.Height()) / 2;
    rect.OffsetRect(ilHorzOffset, ilVertOffset);
    MoveWindow(&rect, FALSE);

    return TRUE;  // return TRUE  unless you set the focus to a control
}
