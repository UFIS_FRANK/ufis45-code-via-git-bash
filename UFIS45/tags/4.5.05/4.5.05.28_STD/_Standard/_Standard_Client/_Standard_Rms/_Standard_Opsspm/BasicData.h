// BasicData.h CCSBasicData class for providing general used methods

#ifndef _BASICDAT
#define _BASICDAT

#define URNOS_AT_ONCE	500
#define MAX_DEBUG_INFO_LOG_MESSAGES 1000
#define EMP_NOT_MATCH_DEMAND 10000

#include <CedaSgmData.h>
#include <CedaSgrData.h>
#include <CedaSpfData.h>
#include <CedaRpfData.h>
#include <CedaEmpData.h>
#include <CedaDemandData.h>
#include <CedaFlightData.h>
#include <CedaJobData.h>
#include <CCSBcHandle.h>
#include <resource.h>
#include <CedaAzaData.h>
#include <CedaEquData.h>
#include <CedaShiftData.h>

extern enum enumPoolsToGate {NOTFOUND=-1,POOLA,POOLB1,POOLB2,POOLB12,POOLB1116,TERMC,POOLSONDER,GATESTOTAL,POOLHALLE};

struct GATEAREA_DATA
{
	CString Alid;
	CCSPtrArray <CString> Gates;
};

struct POOL_DATA
{
	CString Alid;
	CCSPtrArray <GATEAREA_DATA> GateAreas;
};



struct PERM_PAGE
{
	CString		 Name;
	CCSPtrArray <CString> Functions;
};


// see HungarianAlgorith()
struct MATCHDATA
{
	int Stat;
	int Tsta;
	CString Upjb;
	CString Udem;
	int Mark;
	int Weight;
};

class CCSBasicData : public CCSCedaData
{

public:

	CCSBasicData(void);
	~CCSBasicData(void);
	
	CCSPtrArray <PERM_PAGE> omPermissions;
	CCSPtrArray <POOL_DATA> omPoolArray;

	bool bmDiffFuncColourEnabled;
	COLORREF omDiffFuncColour;
	bool GetDiffFuncColour(COLORREF &ropColour);
	void InitDiffFuncColour();

	bool bmLoadRel;
	CStringArray omLoadTimeValues;
	
	CMapStringToPtr omManagerFunctionMap;  // list of functions which constitute a manager

	CStringArray omInfo; // information displayed in the About OpssPm Dialog
	void AddInfo(CString opInfoString);

	void CreateListOfManagers(void);
	bool IsManager(const char *pcpFunction);

	// omAlocUnitMap -->
	// CMapStringToPtr: ALOCUNITTYPE + Ptr       ALOTAB.ALOC eg "GateArea" or "Gate"
	//		Ptr = CMapStringToPtr: ALOCUNIT + Ptr		eg "A01-A03" or "A01"
	//			Ptr = CMapStringToString: POOL + ""			eg "PoolA"
	CMapStringToPtr omAlocUnitToPoolMap;
	CMapStringToString omListOfPoolsWithRestrictions;

	CMapStringToString omTempPoolJobFunction;
	void SetTempFunctionForPoolJob(long lpPjUrno, CString opFunction);
	void ClearTempFunctionForPoolJob();
	CString GetTempFunctionForPoolJob(long lpPjUrno);

	void CreateAlocUnitToPoolMap(void);
	bool ClearAll();
	void ClearAlocUnitToPoolMap(void);
	void AddToAlocUnitToPoolMap(CString opAlocUnitType, CString opAlocUnit, CString opPool);
	void AddToAlocUnitMap(CMapStringToPtr &ropAlocUnitMap, CString opAlocUnit, CString opPool);
	bool AssignmentIsAllowed(CString opAlocUnitType, CString opAlocUnit, CString opPool);
	void GetPoolsWithoutRestrictions(CStringArray &ropPools, bool bpReset = true);
	bool IsInPool(CString opAlocUnitType, CString opAlocUnit, CString opPool);
	int GetPoolsByAlocUnit(CString opAlocUnitType, CString opAlocUnit, CStringArray &ropPools, bool bpReset = true);
	int GetAlocUnitsByPool(CString opAlocUnitType, CString opPool, CStringArray &ropAlocUnits, bool bpReset = true);
	CString GetPool(CWnd *popParent, CString opAlocUnitType, CString opAlocUnit);
	void LogCedaError(const char *pcpText, const char *pcpLastErrorMessage, const char *pcpCommand, const char *pcpFields, const char *pcpTable, const char *pcpWhere);
	void LogCedaAction(char *pcpTable, char *pcpCmd, char *pcpSelection, char *pcpFields, char *pcpData);
	CString GetJobAlid(JOBDATA *prpJob);
	void GetDebugData(void);

	CString omConfigFileName;
	CString GetConfigFileName(void);
	
	int imScreenResolutionX;
	int imScreenResolutionY;
	int imMonitorCount;
	long ConfirmTime;
	CString omUserID;
	CStringArray omTeamIds;
	CTimeSpan omNumDaysToLoadShifts;

	long GetNextUrno(void);

	CTime omTimeframeStart,omTimeframeEnd;
	void SetTimeframe(CTime opTimeframeStart, CTime opTimeframeEnd);
	CTime GetTimeframeStart(void);
	CTime GetTimeframeEnd(void);
	CTime GetTimeframeStartUtc(void);
	CTime GetTimeframeEndUtc(void);
	CString GetTimeframeString(void);
	void GetTimeframeList(CStringArray &ropTimeframeList);
	BOOL IsDisplayDateInsideTimeFrame();

	CTime omDisplayDate;
	CTimeSpan omDisplayOffset;
	void InitDisplayDate();
	void InitShiftDate();
	void SetDisplayDate(CTime opDisplayDate);
	int GetDisplayDateOffset();
	CTimeSpan GetDisplayOffset();
	CTime GetDisplayDate();
	int GetDisplayDateOffsetByDate(CTime opDate);
	CTime GetDateByDisplayDateOffset(int ipOffset);
	void SynchronizeDisplayDates(CTime opDay);

	CTime ZeroHHMMSS(CTime opDate);

	CString GetDeskType(const char *pcpCicName);
	CString GetIdByText(CString opKey);
	CString GetTextById(CString opKey);
	bool GetWindowPosition(CRect& rlPos,CString olName, CString opNumMonitors = "0");
	long GetUniqeIndex();
	CString GetGateAreaName(const char *pcpGateAreaId);
	CString GetPstAreaName(const char *pcpPstAreaId);
	CString GetPoolName(const char *pcpPoolId);
	CString GetAlidForJob(long lpUaid,long lpUalo);
	long GetUaidForJob(const char *pcpAlid,long lpUalo);
	
	void SetPermission(CString opPage,CString opFunction);
	PERM_PAGE *FindPermissionPage(CString opPage);
	BOOL GetPermission(CString opPage,CString opFunction);

	bool bmUseUtc;
	int imUtcDifference;
	int GetUtcDifference(void);
	int SetUtcDifference(void);
	void SetUtcDifferenceFromApttab(void);
	CTime GetLocalTime();
	CTime GetUtcTime();
	CTime GetTime();
	void ConvertDateToUtc(CTime &ropDate);
	void ConvertDateToLocal(CTime &ropDate);
	CTime GetUtcFromLocal(CTime opDate);
	CTime GetLocalFromUtc(CTime opDate);
	CTime omTich;
	CTimeSpan omLocalDiff1, omLocalDiff2;
	CTimeSpan omShiftStartTolerance, omShiftEndTolerance;
	CTime omShiftFirstSDAY, omShiftLastSDAY;

public:
	/*
	void GetPoolJobFunctionsAndPermits(JOBDATA *prpPoolJob, CString &ropFunction, CStringArray &ropPermits);
	CString GetFunctionForPoolJob(JOBDATA *prpPoolJob);
	void GetPermitsForPoolJob(JOBDATA *prpPoolJob, CStringArray &ropPermits);
	*/
	CString GetMultiFunctionText(long lpEmpUrno);
	void GetPermitsForPoolJob(JOBDATA *prpPoolJob, CStringArray &ropPermits);
	void GetFunctionsForPoolJob(JOBDATA *prpPoolJob, CStringArray &ropFunctions);
	CString GetFunctionForPoolJob(JOBDATA *prpPoolJob);
	CString GetNonEmployeeFunctionForPoolJob(JOBDATA *prpPoolJob);
	void GetFunctionsForShift(SHIFTDATA *prpShift, CStringArray &ropFunctions);
	CString GetFunctionForShift(SHIFTDATA *prpShift);
	CString GetNonEmployeeFunctionForShift(SHIFTDATA *prpShift);
	CString GetNonEmployeeFunction(long lpPoolJobUrno, long lpEmpUrno, CTime opShiftStart, long lpDrdUrno, long lpDelUrno, long lpShiftUrno);

	DEMANDDATA *GetBestDemandForEquipment(CCSPtrArray <DEMANDDATA> &ropDemands, EQUDATA *prpEqu);
	bool NoOverlap(CCSPtrArray <JOBDATA> &ropJobs, CTime opBegin, CTime opEnd);
	bool CheckDemandForCorrectEquipment(DEMANDDATA *prpDemand, EQUDATA *prpEqu);
	void GetEquipmentForDemand(DEMANDDATA *prpDemand, CCSPtrArray <EQUDATA> &ropEquList);

	bool CheckFunctionsAndPermits(JOBDATA *prpPoolJob, DEMANDDATA *prpDem, CStringArray &ropErrors);
	bool CheckFunctionsAndPermits2(JOBDATA *prpPoolJobs, DEMANDDATA *prpDemands, bool bpAlwaysAssignSingleEmpAndDemand = true);
	bool CheckFunctionsAndPermits2(CCSPtrArray <JOBDATA> &ropPoolJobs, CCSPtrArray <DEMANDDATA> &ropDemands, CMapPtrToPtr &ropPoolDemands, bool bpAlwaysAssignSingleEmpAndDemand = true);
	bool CheckFunctionsAndPermitsInRule(JOBDATA *prpPoolJob, DEMANDDATA *prpDemand, CStringArray &ropErrors);
	bool CheckFunctionsAndPermitsInRule(CStringArray &ropEmpFunctions, CStringArray &ropEmpPermits, DEMANDDATA *prpDemand, CStringArray &ropErrors);
	bool CheckFunctionsAndPermitsInAzatab(JOBDATA *prpPoolJob, DEMANDDATA *prpDemand, CStringArray &ropErrors);
	bool CheckFunctionsAndPermitsInAzatab(CString &ropFunction, CStringArray &ropPermits, DEMANDDATA *prpDemand, CStringArray &ropErrors);
	bool CheckFunctionsAndPermitsInAzatab(FUNC_PERMITS &ropNewEmp, DEMANDDATA *prpDemand, CStringArray &ropErrors);
	void GetDemandInfo(DEMANDDATA *prpDemand, CString &ropDemandInfo, bool bpFullInfo = true);
	void GetDemandInfoAzatab(DEMANDDATA *prpDemand, CString &ropDemandInfo);
	CString GetFunctionsForDemand(DEMANDDATA *prpDemand);
	CString GetFunctionsForDemand(DEMANDDATA *prpDemand, CStringArray &ropFunctions);
	void GetFunctionAndPermits(DEMANDDATA *prpDemand, CStringArray &ropDemandInfo, bool bpUseTeamInfo = false);
	int GetAssignmentSuitabiltyWeighting(JOBDATA *prpPoolJob, DEMANDDATA *prpDemand);
	void GetJobInfo(JOBDATA *prpJob, CString &ropJobInfo);
	CString GetDemandBarText(DEMANDDATA *prpDemand, bool bpDisplayGroupMembers = false);
	CString GetDemandFunctionsString(DEMANDDATA *prpDemand, char cpSeperator = ',');
	int GetDemandFunctions(DEMANDDATA *prpDemand, CStringArray &ropFunctions, bool bpReset = true);
	CString GetDemandPermitsString(DEMANDDATA *prpDemand, char cpSeperator = ',');
	int GetDemandPermits(DEMANDDATA *prpDemand, CStringArray &ropPermits, bool bpReset = true);
	void AddElem(CString &ropString, CString opElem);
	void AddElem2(CString &ropString, CString opElem);
	static void TrimLeft(char *s);
	static void TrimRight(char *s);
	char pcmPrintForm[42];
	char pcmInterchangeable[10]; //Singapore
	BOOL bmDeleteViews;
	bool SelectBestEmpDemandCombination(CCSPtrArray <MATCHDATA> &ropMatchList, int ipNumDemands);

	static COLORREF GetCciDeskColor(CString opDeskTyp);

	int MessageBox(CWnd *popWnd,LPCTSTR lpszText,LPCTSTR lpszCaption = NULL,UINT nType = MB_OK );
	void SetWindowStat(const char *pcpKey, CWnd *popWnd, bool bpEnabledInCedaIni = true);
	void SetWindowStat(char cpStat, CWnd *popWnd, bool bpEnabledInCedaIni = true);
	bool IsEnabled(const char *pcpKey);
	bool IsDisabled(const char *pcpKey);
	bool IsHidden(const char *pcpKey);
	
	bool GetNurnos(int ipNrOfUrnos);
	void Trace(char *pcpFormatList, ...);
	void AddToDebugInfoLog(CString &ropMessage);
	CStringArray omDebugInfoLog;

	int GetItemCount(CString olList, char cpTrenner  = ',' );
	int ExtractItemList(CString opSubString, CStringArray *popStrArray, char cpTrenner = ',');
	CString GetListItem(CString &opList, int ipPos, bool bpCut, char cpTrenner  = ',');
	CString DeleteListItem(CString &opList, CString olItem);
	void FormatDemand(DEMANDDATA *prpDem, CString &ropString);
	CString GetFmJobTypeByAloc(CString opAloc);
	CString GetFlightAlidByAloc(long lpFlightUrno, CString opAloc);
	CString GetFlightAlidByAloc(FLIGHTDATA *prlFlight, CString opAloc);
	long GetUrueForDemand(DEMANDDATA *prpDem);
	bool IsFinished(FLIGHTDATA *prpFlight, const char *pcpAloc = "", int ipDemType = ALLDEMANDS, int ipReturnFlightType = ID_NOT_RETURNFLIGHT);
	bool LoadJob(long lpJobUrno);
	bool IsSinApron1(); //Singapore
	bool IsSinApron2(); //Singapore
	bool IsPaxServiceT2(); //SINGAPORE
	bool IsPRMRelatedTemplate(); //SINGAPORE

	static BOOL WriteDialogToReg(const CWnd* ropWnd, const CString& ropKey,const CRect& ropRect = NULL,const BOOL& rbpMinimized = FALSE);  //Singapore
	static BOOL GetDialogFromReg(CRect& ropRect,const CString& ropKey,BOOL& rbpMinimized);  //Singapore
	void   GetWindowPositionCorrect(CRect& ropRect) const; //Singapore
	bool IsPrmHandlingAgentEnabled();
	char *GetHandlingAgentForPrm();
	CString GetConfirmedPSMEntry();
	COLORREF GetConfirmedPSMColor();

public:
	bool bmDisplayAssignConflictMessage; // within NewAssDlg conflict displayed when assiging emps with incorrect funcs/Permits
	bool bmAutomaticPauseAllocation; // Pauses created automatically
	BOOL bmDemandsOnly;			// Display only flights which have demands
	BOOL bmTurnaroundBars;		// If set, create a single turnaround bar for arrival and departure flight
								// if they are at the same gate and the arrival and first departure demand overlap
	BOOL bmShowShadowBars;      // if set, shows shadow bars (in background color) at the old gate after gate change
	BOOL bmVerticalScaling;		// allows the changing of vertical scaling when true
	int  imFreeBottomLines;
    BOOL bmIsConnected;			// are we connected to server now?
	BOOL bmOfflineEnabled;		// is Offline Button enabled ?
	int imNumberOfUndoRedoActions;

	bool bmDelegateFlights;
	bool bmDelegatingFlight;

	CString omTelexTypes; // comma seperated list of telex types to be loaded eg MVT,LDM etc
	CString omTelexTypesToLoad; // comma seperated list of telex types to be loaded eg MVT,LDM etc
	CString omTelexTypesPrintOnlyLast; // comma seperated list of telex types - print only the last telex of this type on the work card
	char cmHandlingTypeForPRM[128];   // Handling Agent type for the PRM services
	CString omTablesNotToLoad;  // comma seperated list of tables we don't want to load
	CString omDemandsToLoad; // either comma seperated list of the demand types to load or blank for all demands
	bool DemandsToLoad(const char *pcpDety);

	bool bmShowMatchingEmpsByDefault;
	bool bmSwapShiftEnabled;
	bool bmIgnoreDeactivatedDemandConflicts;
	bool bmAssignErrorDlgShowAllDemands;
	bool bmIgnorePausesByDefault; // available emps dlg
	bool bmIgnoreOverlapByDefault; // available emps dlg
	bool bmIgnoreOutsideShiftByDefault; // available emps dlg
	bool bmAvailEmpsMultiAssign;
	bool bmAvailEquMultiAssign;
	bool bmDisplayTeams; // Gruppendisposition
	bool bmDisplayPrePlanning;
	bool bmDisplayGates, bmDisplayTurnaroundGates;
	bool myNewConflict; //for conflict type 56 ,added by MAX
	bool bmDisplayFlightJobsTable;
	bool bmDisplayCheckins;
	bool bmDisplayPrm;
	bool bmDisplayRegistrations;
	bool bmDisplayPositions, bmDisplayTurnaroundPositions;
	bool bmDisplayEquipment;
	bool bmDisplayDemands;
	bool bmPrintWorkingCard;
	bool bmUseDemandSortValues;
	int  imSingleBarLen;
	CString	omPriorityForDemand;
	CString omFlightFtypsNotToLoad;
	int  imStandardLenghtDemand;
	CString omDepArrMarkerPst, omDepArrMarkerGate, omDepArrMarkerEquipment,omDepArrMarkerStaff;
	bool bmReassignOverlappingBreaks;
	int imExtendShiftOffset;
	int imDefaultBreakBuffer;	// hag 21.09.2004 PRF5802
	CStringArray omPRMRelatedTemplates; // lli handle prm related templates functions

	CTime omTimebandStart, omTimebandEnd; // used to set the yellow lines indicating the time of the currently selected bar when Gantts are opened

	bool DisplayDebugInfo(void);
	void LogBroadcast(struct BcStruct *prpBcData);

	void CheckLog(void);
	bool OpenLog(CString opFilename, UINT ilFlag = 0);
	bool CloseLog(void);
	bool DeleteOldLogs();
	bool WriteLog(const char *pcpData);
	CString GetFileErr(int ipErr);
	CString omLogFilePath, omOldLogName;
	bool bmLoggingEnabled;
	CFile *pomLogFile;

	CString GetVersionString(void);
	CString GetVersion(void);
	CString GetUfisVersion(void);
	CString GetSpecialBuild(void);

	bool JobHasExpiredDemand(long lpJobu);
	bool DontLoadTable(CString &ropTableName);
	CString GetDemandAlid(DEMANDDATA *prpDemand);
	void DebugInfoMsgBox(CString &ropMsg);
	void CheckDatabaseIntegrity(void);
	CString GetTeamForShift(SHIFTDATA *prpShift);
	CString GetTeamForPoolJob(JOBDATA *prpPoolJob);
	int GetFunctionWeightWithinWorkgroup(JOBDATA *prpPoolJob);
	bool IsGroupLeader(JOBDATA *prpPoolJob);
	bool IsGroupLeader(SHIFTDATA *prpShift);

	CString GetStringForAllocUnitType(CString opAllocUnitType);
	CString GetAllocUnitTypeFromString(CString opAllocUnitTypeString);

	CString FormatDate(CTime opDate, CTime opDisplayDate = TIMENULL);
	CString FormatDate2(CTime opDate, CTime opDisplayDate = TIMENULL);

	//bool	JobtabHasUtpl(void);
	//bool	JobtabHasUequ(void);
	bool	DoesFieldExist(CString opTana, CString opFina);
	bool	HandlingAgentsSupported();
	bool	DeadloadPerFlightSupported();
	bool	PremiumClassSupported();
	bool	GetTemplateFilters(CDWordArray& ropTplUrnos);
	bool	GetDefaultTemplateFilters(CDWordArray& ropTplUrnos);
	bool	SetTemplateFilters(const CDWordArray& ropTplUrnos);

	bool	ShiftHasFunctionCode(void);
	BOOL	GetCounterClass(DEMANDDATA *prpDem,CString& ropClass);
	BOOL	GetCounterClassName(DEMANDDATA *prpDem,CString& ropName);
	BOOL	GetCounterGroups(DEMANDDATA *prpDem,CStringArray& ropCounterGroups,BOOL bpGroupsOnly = TRUE);
	long	GetCounterClassUrno(DEMANDDATA *prpDem);
	BOOL	GetCounters(DEMANDDATA *prpDem,CDWordArray& ropCounters);
	
	long GetFlightUrnoForJob(JOBDATA *prpJob, long *plpArrUrno = NULL, long *plpDepUrno = NULL);
	//long GetFlightUrnoForJob(JOBDATA *prpJob);
	FLIGHTDATA *GetFlightForJob(JOBDATA *prpJob);

	DEMANDDATA*	GetFirstDemandWithoutJobByFlur(long lpFlightUrno,const char *pcpAlocUnitType, int ipDemandType = PERSONNELDEMANDS, int ipReturnFlightType = ID_NOT_RETURNFLIGHT);

	COLORREF	cmEmployeeInformedColor;
	WORD		wmEmployeeInformedThickness;

	bool JobIsOfType(long lpJobUrno, int ipDemandType);

	clock_t prmMarkTime;
	void MarkTime();
	void DisplayElapsedTimeSinceLastMark();
	CString GetElapsedTimeSinceLastMark();

	void GetUrnoFromString(CUIntArray &ropUrnos, CString &ropUrnoString);
	void GetUrnoFromStringArray(CUIntArray &ropUrnos, CStringArray &ropUrnoStringArray);
	CString FormatUrnoList(CUIntArray &ropUrnos);
	CString FormatUrnoList(CStringArray &ropUrnos);
	bool GetDatesFromString(CTime &ropFrom, CTime &ropTo, CString &ropDateString);
	bool JobAndDemHaveDiffFuncs(JOBDATA *prpJob);
	static const CString TPL_APRON1; //Singapore
	static const CString TPL_APRON2; //Singapore

private:
	CDWordArray omUrnos;
	CDWordArray	omTemplateFilterUrnos;

public:
	CStringArray omRereadDebugInfo;
	void DumpRereadStatistics();
	void SaveTimeTakenForReread(const char *pcpText, const char *pcpSelection, int ipNumRecsRead = -1);
	CString FormatFieldForExport(const char *pcpField, const char *pcpSeperator);
	void DisableAllMenuItems(CMenu &ropMenu);

	CString GetDrrIndexHint();
};

extern CCSBasicData ogBasicData;

#endif
