// DebugCountDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsspm.h>
#include <DebugCountDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDebugCountDlg dialog


CDebugCountDlg::CDebugCountDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDebugCountDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDebugCountDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDebugCountDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDebugCountDlg)
	DDX_Control(pDX, IDC_COUNTLIST, m_CountList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDebugCountDlg, CDialog)
	//{{AFX_MSG_MAP(CDebugCountDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDebugCountDlg message handlers

BOOL CDebugCountDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	int ilNumLines = omLines.GetSize();
	for(int ilLine = 0; ilLine < ilNumLines; ilLine++)
	{
		m_CountList.AddString(omLines[ilLine]);
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDebugCountDlg::AddLine(CString opDate, CString opDrrCount, CString opJobCount, CString opDemCount, CString opAftCount)
{
	CString olNewLine;
	//				  %DATE      %DRRTAB    %JOBTAB    %DEMTAB    AFTTAB
	olNewLine.Format("%s   %6s    %6s    %6s",opDate, opDrrCount, opJobCount, opDemCount);
	omLines.Add(olNewLine);
}
