// CedaInitModuData.cpp
 
#include <stdafx.h>
#include <CedaInitModuData.h>
#include <resource.h>
#include <BasicData.h>

CedaInitModuData ogInitModuData;


//--CEDADATA-----------------------------------------------------------------------------------------------

CedaInitModuData::CedaInitModuData()
{
	strcpy(pcmInitModuFieldList,"APPL,SUBD,FUNC,FUAL,TYPE,STAT");
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaInitModuData::~CedaInitModuData(void)
{
}

//--SEND INIT MODULS-------------------------------------------------------------------------------------------

bool CedaInitModuData::SendInitModu()
{
	bool ilRc = true;

	CString olInitModuData = GetInitModuTxt();

	char *pclInitModuData = new char[olInitModuData.GetLength()+3];
	strcpy(pclInitModuData,olInitModuData);
	char pclTable[5];
	sprintf(pclTable,"%s ",pcgTableExt);

	ilRc = CedaAction("SMI",pclTable,pcmInitModuFieldList,"","",pclInitModuData);
	if(ilRc==false)
	{
		AfxMessageBox(LoadStg(IDS_STRING104) + omLastErrorMessage,MB_ICONEXCLAMATION);
	}
	delete pclInitModuData;
	return ilRc;
}

//--------------------------------------------------------------------------------------------------------------

CString CedaInitModuData::GetInitModuTxt()
{
	CString olInitModuData;

	olInitModuData  = "WGRTool,";
	olInitModuData += LoadStg(IDS_STRING102)+",InitModu,"+LoadStg(IDS_STRING103)+",B,-";
	olInitModuData += GetInitModuTxt1();
	olInitModuData += GetInitModuTxt2();
	olInitModuData += GetInitModuTxt3();

	return olInitModuData;
}
//--------------------------------------------------------------------------------------------------------------

CString CedaInitModuData::GetInitModuTxt1()
{
	CString olInitModuData;

/*	olInitModuData += om_ShBut+",m_ANSICHT,"+LoadStg(IDS_STRING555)+",B,1";
	olInitModuData += om_ShBut+",m_AnzeigeComboBox,"+om_ComVie+",C,1";
	olInitModuData += om_ShBut+",m_EINFUEGEN,"+LoadStg(IDS_STRING556)+",B,1";
	olInitModuData += om_ShBut+",m_AENDERN,"+LoadStg(IDS_STRING557)+",B,1";
	olInitModuData += om_ShBut+",m_LOESCHEN,"+LoadStg(IDS_STRING558)+",B,1";
	olInitModuData += om_ShBut+",m_KOPIEREN,"+LoadStg(IDS_STRING559)+",B,1";
	olInitModuData += om_ShBut+",m_ERSETZEN,"+LoadStg(IDS_STRING560)+",B,1";
	olInitModuData += om_ShBut+",m_DRUCKEN,"+LoadStg(IDS_STRING561)+",B,1";		
*/	///////////////////////////////////////////////

	return olInitModuData;
}

//--------------------------------------------------------------------------------------------------------------

CString CedaInitModuData::GetInitModuTxt2()
{
	CString olInitModuData;

	return olInitModuData;
}

//--------------------------------------------------------------------------------------------------------------

CString CedaInitModuData::GetInitModuTxt3()
{
	CString olInitModuData;

	return olInitModuData;
}

//--------------------------------------------------------------------------------------------------------------
