// AloFilterPage.cpp : implementation file
//

#include <stdafx.h>
#include <WGRTool.h>
#include <AloFilterPage.h>
#include <GridFenster.h>
#include <BasicData.h>
#include <CedaBasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CAloFilterPage property page

IMPLEMENT_DYNCREATE(CAloFilterPage, CPropertyPage)

CAloFilterPage::CAloFilterPage() : CPropertyPage(CAloFilterPage::IDD)
{
	//{{AFX_DATA_INIT(CAloFilterPage)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	// preset caption
	static CString olCaption = LoadStg(IDS_STRING418);
	m_psp.pszTitle = olCaption.GetBuffer(0);
	m_psp.dwFlags |= PSP_USETITLE;

	// create possible items grid
	pomPossilbeList = new CGridFenster(this);
	// create selected items grid
	pomSelectedList = new CGridFenster(this);

	// evaluate table types from ceda.ini
	CStringArray olTableTypes;
	bool blUseTableTypes = EvaluateTableTypes(olTableTypes);

	// get list of possible values from Allocation table
	int ilCount = ogBCD.GetDataCount("ALO");
	CString olTmpText;
	CString olTmpText1;
	CString olTmpText2;

	for(int i = 0; i < ilCount; i++)
	{
		//	get reference column from ALO table 
		olTmpText1 = ogBCD.GetField("ALO", i, "REFT");

		//	get allocation description from ALO table
		olTmpText2 = ogBCD.GetField("ALO", i, "ALOD");

		bool blFoundInTableTypes = blUseTableTypes ? false : true;
		if (blUseTableTypes)
		{
			olTmpText  = ogBCD.GetField("ALO", i, "ALOC");

			for (int j = 0; j < olTableTypes.GetSize(); j++)
			{
				if (olTmpText == olTableTypes[j])
				{
					blFoundInTableTypes = true;
					break;
				}
			}
		}

		// ignore tables not wanted
		if (!blFoundInTableTypes)
			continue;

		if(!olTmpText1.IsEmpty() && !olTmpText2.IsEmpty())
		{
			olTmpText = olTmpText1;
			olTmpText += "#";		
			olTmpText += olTmpText2;
			omPossibleItems.Add(olTmpText);
		}
	}

	bmIsInitialized = false;
	imHideColStart = 1;
	CStringArray olItemList;
	if(omPossibleItems.GetSize() > 0)
	{
		ExtractItemList(omPossibleItems[0], &olItemList, ';');
	}

	// we start the hiding columns at least
	imHideColStart = olItemList.GetSize() +1;
	imColCount = imHideColStart + 2;
}

CAloFilterPage::~CAloFilterPage()
{
	delete pomPossilbeList;
	delete pomSelectedList;
}

void CAloFilterPage::DoDataExchange(CDataExchange* pDX)
{
	CString olText;
	CGXStyle olStyle;
	CPropertyPage::DoDataExchange(pDX);

	//{{AFX_DATA_MAP(CAloFilterPage)
	DDX_Control(pDX, IDC_BUTTON_REMOVE, m_RemoveButton);
	DDX_Control(pDX, IDC_BUTTON_ADD, m_AddButton);
	DDX_Control(pDX, IDC_INSERTLIST, m_InsertList);
	DDX_Control(pDX, IDC_CONTENTLIST, m_ContentList);
	//}}AFX_DATA_MAP

	// initialize controls from member variables
	if (!pDX->m_bSaveAndValidate)
	{
		int ilLc;
		
		// Do not update window while processing new data
		m_InsertList.SetRedraw(FALSE);
		m_ContentList.SetRedraw(FALSE);

		// Initialize all possible items into the left list box
		m_InsertList.ResetContent();

		// add '*All' as first item
		m_InsertList.AddString(LoadStg(IDS_STRING423));
		for (ilLc = 0; ilLc < omPossibleItems.GetSize(); ilLc++)
		{
			CString olTest = omPossibleItems[ilLc];
			m_InsertList.AddString(omPossibleItems[ilLc]);
		}

		// Initialize the selected items into the right list box,
		// also remove the corresponding items from the left list box.
		m_ContentList.ResetContent();
		bool blFoundAlle = false;
		for (ilLc = 0; ilLc < omSelectedItems.GetSize()  && blFoundAlle == false; ilLc++)
		{
			CString olTmpText = omSelectedItems[ilLc];
			if(!olTmpText.IsEmpty())
			{
				// '*All' selected ?
				if (omSelectedItems[ilLc] == LoadStg(IDS_STRING423))
				{
					// nothing more to add
					m_AddButton.EnableWindow(FALSE);
					blFoundAlle = true;
					// remove everything from contents
					m_ContentList.ResetContent();
					// except '*All' 
					m_ContentList.AddString(LoadStg(IDS_STRING423));	
					// all inserted
					m_InsertList.ResetContent();
				
				}
				else	
				{
					olTmpText += "#";
					int ilPos = m_InsertList.FindString(-1, olTmpText);
					if(ilPos > -1)
					{
						m_InsertList.GetText(ilPos,olTmpText);
						// add to contents
						m_ContentList.AddString(olTmpText);
						// and remove from insertable
						m_InsertList.DeleteString(ilPos);
					}
				}
			}
		}

		// enable window update 
		m_InsertList.SetRedraw(TRUE);
		m_ContentList.SetRedraw(TRUE);

		if(bmIsInitialized)
		{

			m_AddButton.EnableWindow(TRUE);


			CString olText;
			int ilCount = m_InsertList.GetCount() + 1;
			
			olStyle.SetEnabled(FALSE);
			olStyle.SetReadOnly(TRUE);
		
			pomPossilbeList->SetReadOnly(FALSE);
			pomSelectedList->SetReadOnly(FALSE);

			// delete all
			pomSelectedList->RemoveRows(1,  pomSelectedList->GetRowCount());
			pomPossilbeList->RemoveRows(1,  pomPossilbeList->GetRowCount());

			// display 18 rows at once only
			pomPossilbeList->SetRowCount(max(18,ilCount));

			CStringArray olItemList;
			CString olOrgText;

			for ( ilLc = 0; ilLc < ilCount-1; ilLc++)
			{
				m_InsertList.GetText(ilLc,olText);
				olOrgText = olText;
				ExtractItemList(olText, &olItemList, '#');
				if(olItemList.GetSize() > 1)
				{
					olText = olItemList[1];
					pomPossilbeList->SetStyleRange(CGXRange(ilLc+1, imHideColStart+1, ilLc+1, imHideColStart+1), olStyle.SetValue(olItemList[0]));
				}

				ExtractItemList(olText, &olItemList, ';');
				for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
					pomPossilbeList->SetStyleRange(CGXRange(ilLc+1, ilItem+1, ilLc+1, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
				pomPossilbeList->SetStyleRange(CGXRange(ilLc+1, imHideColStart, ilLc+1, imHideColStart), olStyle.SetValue(olOrgText));

			}

			ilCount = m_ContentList.GetCount()+ 1;;
				
			pomSelectedList->SetRowCount(max(18,ilCount));

			for (ilLc = 0; ilLc < ilCount-1; ilLc++)
			{
				m_ContentList.GetText(ilLc,olText);
				olOrgText = olText;
				ExtractItemList(olText, &olItemList, '#');
				if(olItemList.GetSize() > 1)
				{
					olText = olItemList[1];
					pomSelectedList->SetStyleRange(CGXRange(ilLc+1, imHideColStart+1, ilLc+1, imHideColStart+1), olStyle.SetValue(olItemList[0]));

				}
				ExtractItemList(olText, &olItemList, ';');
				for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
					pomSelectedList->SetStyleRange(CGXRange(ilLc+1, ilItem+1, ilLc+1, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
				pomSelectedList->SetStyleRange(CGXRange(ilLc+1, imHideColStart, ilLc+1, imHideColStart), olStyle.SetValue(olOrgText));

			}

			// grid is now no longer editable
			pomPossilbeList->SetReadOnly(TRUE);
			pomSelectedList->SetReadOnly(TRUE);

			// force grid update
			pomSelectedList->Redraw();
			pomPossilbeList->Redraw();
		}
	}

	// update member variables from controls
	if (pDX->m_bSaveAndValidate)
	{
		CString olValue1;
		CString olValue2;
		omSelectedItems.RemoveAll();
		omSelectedItems.SetSize(m_ContentList.GetCount()+ 1);
		for (int ilLc = 0; ilLc < (int)pomSelectedList->GetRowCount(); ilLc++)
		{
			// get the hiden value
			olValue1 = pomSelectedList->GetValueRowCol(ilLc, imHideColStart );
			olValue2 = pomSelectedList->GetValueRowCol(ilLc, imHideColStart + 1 );
			if(!olValue1.IsEmpty())
			{
				if(olValue2.IsEmpty())
					omSelectedItems.Add(olValue1);
				else
					omSelectedItems.Add(olValue2);
			}

		}
	}

}


bool CAloFilterPage::EvaluateTableTypes(CStringArray& opTableTypes)
{
	// use '*ALL' as Default
	CString olDefault = LoadStg(IDS_STRING423);
	CString olResult;

	char pclTmpText[512];
	char pclConfigPath[512];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

    GetPrivateProfileString(pcgAppName, "TABLETYPES", olDefault,
		pclTmpText, sizeof pclTmpText, pclConfigPath);

	olResult = pclTmpText;
	// must we check for table types ?
	if (olResult == olDefault)
		return false;
	char *token = strtok(pclTmpText,",\n");
	while(token)
	{
		CString olResult = token;
		olResult.MakeUpper();
		opTableTypes.Add(olResult);
		token = strtok(NULL,",\n");
	}
	return true;
}

bool CAloFilterPage::GetAllFilters(CStringArray& opFilterList)
{
	// evaluate table types from ceda.ini
	CStringArray olTableTypes;
	bool blUseTableTypes = EvaluateTableTypes(olTableTypes);

	// get list of possible values from Allocation table
	int ilCount = ogBCD.GetDataCount("ALO");
	CString olTmpText;
	CString olTmpText1;
	CString olTmpText2;

	for(int i = 0; i < ilCount; i++)
	{
		//	get reference column from ALO table 
		olTmpText1 = ogBCD.GetField("ALO", i, "REFT");

		//	get allocation description from ALO table
		olTmpText2 = ogBCD.GetField("ALO", i, "ALOD");

		bool blFoundInTableTypes = blUseTableTypes ? false : true;
		if (blUseTableTypes)
		{
			olTmpText  = ogBCD.GetField("ALO", i, "ALOC");

			for (int j = 0; j < olTableTypes.GetSize(); j++)
			{
				if (olTmpText == olTableTypes[j])
				{
					blFoundInTableTypes = true;
					break;
				}
			}
		}

		// ignore tables not wanted
		if (!blFoundInTableTypes)
			continue;

		if(!olTmpText1.IsEmpty() && !olTmpText2.IsEmpty())
		{
			opFilterList.Add(olTmpText1);
		}
	}
	return true;
}

BEGIN_MESSAGE_MAP(CAloFilterPage, CPropertyPage)
	//{{AFX_MSG_MAP(CAloFilterPage)
	ON_BN_CLICKED(IDC_BUTTON_ADD, OnButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_REMOVE, OnButtonRemove)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAloFilterPage message handlers

BOOL CAloFilterPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

	// set language specific window text
	CWnd *polWnd = GetDlgItem(IDC_BUTTON_ADD);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING416));
	}

	// set language specific window text
	polWnd = GetDlgItem(IDC_BUTTON_REMOVE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING417));
	}

	// replace existing window procedure with our handler
	pomSelectedList->SubclassDlgItem(IDC_SELLIST, this);
	pomPossilbeList->SubclassDlgItem(IDC_POSLIST, this);

	// initialize selection grid
	pomSelectedList->Initialize();

	// disable immediate update
	pomSelectedList->LockUpdate(TRUE);
	pomSelectedList->GetParam()->EnableUndo(FALSE);
	// column width is static
	pomSelectedList->GetParam()->EnableTrackColWidth(FALSE);
	// row height is static
	pomSelectedList->GetParam()->EnableTrackRowHeight(FALSE);
	// column headers aren't numbered
	pomSelectedList->GetParam()->SetNumberedColHeaders(FALSE);

	// initialize possible grid
	pomPossilbeList->Initialize();
	pomPossilbeList->LockUpdate(TRUE);
	pomPossilbeList->GetParam()->EnableUndo(FALSE);
	// column width is static
	pomPossilbeList->GetParam()->EnableTrackColWidth(FALSE);
	// row height is static
	pomPossilbeList->GetParam()->EnableTrackRowHeight(FALSE);
	// column headers aren't numbered
	pomPossilbeList->GetParam()->SetNumberedColHeaders(FALSE);

	pomPossilbeList->SetRowHeight(0, 0, 12);
	pomPossilbeList->SetColCount(imColCount);

	for(int illc = 0; illc < imColCount; illc++)
		pomPossilbeList->SetColWidth(0,illc,40);

	// set the size of the allocation description 	
	pomPossilbeList->SetColWidth(1,1,200);
	// set the size of the row number
	pomPossilbeList->SetColWidth(0,0,30);


	pomSelectedList->SetRowHeight(0, 0, 12);
	pomSelectedList->SetColCount(imColCount);

	for( illc = 0; illc < imColCount; illc++)
		pomSelectedList->SetColWidth(0,illc,40);
	

	// set the size of the allocation description 	
	pomSelectedList->SetColWidth(1,1,200);
	// set the size of the row number
	pomSelectedList->SetColWidth(0,0,30);

	CGXStyle olStyle;
	olStyle.SetEnabled(FALSE);
	olStyle.SetReadOnly(TRUE);

	// enable immediate update
	pomPossilbeList->LockUpdate(FALSE);
	pomSelectedList->LockUpdate(FALSE);
	
	// hide following columns
	pomSelectedList->HideCols(imHideColStart, imColCount); 
	pomPossilbeList->HideCols(imHideColStart, imColCount); 

	CString olText;
	CString olOrgText;
	int ilCount = m_InsertList.GetCount() + 1;

	CStringArray olItemList;
	pomPossilbeList->SetRowCount(max(18,ilCount));
	for (int ilLc = 0; ilLc < ilCount-1; ilLc++)
	{
		m_InsertList.GetText(ilLc,olText);
		olOrgText = olText;
		ExtractItemList(olText, &olItemList, '#');
		if(olItemList.GetSize() > 1)
		{
			olText = olItemList[1];
			pomPossilbeList->SetStyleRange(CGXRange(ilLc+1, imHideColStart+1, ilLc+1, imHideColStart+1), olStyle.SetValue(olItemList[0]));

		}

		ExtractItemList(olText, &olItemList, ';');
		for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
			pomPossilbeList->SetStyleRange(CGXRange(ilLc+1, ilItem+1, ilLc+1, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
		pomPossilbeList->SetStyleRange(CGXRange(ilLc+1, imHideColStart, ilLc+1, imHideColStart), olStyle.SetValue(olOrgText));

	}

	ilCount = m_ContentList.GetCount()+ 1;
		
	pomSelectedList->SetRowCount(max(18,ilCount));

	for (ilLc = 0; ilLc < ilCount-1; ilLc++)
	{
		m_ContentList.GetText(ilLc,olText);
		olOrgText = olText;
		ExtractItemList(olText, &olItemList, '#');
		if(olItemList.GetSize() > 1)
		{
			olText = olItemList[1];
			pomSelectedList->SetStyleRange(CGXRange(ilLc+1, imHideColStart+1, ilLc+1, imHideColStart+1), olStyle.SetValue(olItemList[0]));

		}
		ExtractItemList(olText, &olItemList, ';');
		for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
			pomSelectedList->SetStyleRange(CGXRange(ilLc+1, ilItem+1, ilLc+1, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
		pomSelectedList->SetStyleRange(CGXRange(ilLc+1, imHideColStart, ilLc+1, imHideColStart), olStyle.SetValue(olOrgText));

	}

	pomSelectedList->Redraw();
	pomPossilbeList->Redraw();

	bmIsInitialized = true;
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CAloFilterPage::OnButtonAdd() 
{
	// TODO: Add your control notification handler code here
	CString olText;
	bool blFoundAlle = false;
	CGXStyle olStyle;
	CRowColArray olRows;

	// get count of selected rows
	int ilSelCount = (int)pomPossilbeList->GetSelectedRows( olRows);

	// anything selected ?
	if(ilSelCount > 0)
	{
		// enable modification
		pomPossilbeList->SetReadOnly(FALSE);
	    pomSelectedList->SetReadOnly(FALSE);

#pragma message ("The following line corrected due to prevent generating empty lines")
//		int ilContCount = m_InsertList.GetCount() + 1;
		int ilContCount = m_InsertList.GetCount();
		m_InsertList.ResetContent();
		for (int ilLc = 1 ; ilLc < min(ilContCount+1,(int)pomPossilbeList->GetRowCount()); ilLc++)
		{
			olText = pomPossilbeList->GetValueRowCol(ilLc, imHideColStart);
			m_InsertList.AddString(olText);
		}

		int ilDummy = m_InsertList.GetCount();
		

		CString olComplText;
		// Move selected items from left list box to right list box
		for ( ilLc = olRows.GetSize()-1; 
		ilLc >= 0 && blFoundAlle == false; ilLc--)
		{
			int ilDummy = (int)olRows[ilLc];
			olText = pomPossilbeList->GetValueRowCol(olRows[ilLc], 1);
			olComplText = pomPossilbeList->GetValueRowCol(olRows[ilLc], imHideColStart);
		
			if(ilDummy <= 0 || olComplText.IsEmpty())
				continue;
			//	'*ALL' selected ?
			if (olText == LoadStg(IDS_STRING423))
			{
				// nothing more to add available
				m_AddButton.EnableWindow(FALSE);
				blFoundAlle = true;
				// remove everything from contents list
				m_ContentList.ResetContent();
				// except '*ALL' entry
				m_ContentList.AddString(LoadStg(IDS_STRING423));	
				// remove everything from insert list
				m_InsertList.ResetContent();	
			}
			else
			{
				int iltest = m_InsertList.FindStringExact(-1, olComplText);
				m_ContentList.AddString(olComplText);	// move string from left to right box
				m_InsertList.DeleteString(m_InsertList.FindStringExact(-1, olComplText));
			}
		}
//		pomPossilbeList->Clear(FALSE);


		olStyle.SetEnabled(FALSE);
		olStyle.SetReadOnly(TRUE);

		// save current sort information and top row
		int ilTopRow = pomPossilbeList->GetTopRow();
		
		pomPossilbeList->RemoveRows(1,  pomPossilbeList->GetRowCount());

		CStringArray olItemList;

		int ilCount = m_InsertList.GetCount() + 1;

		CString olOrgText;

		int ilRealCount = 0;
		pomPossilbeList->SetRowCount(max(18,ilCount));
		for ( ilLc = 0; ilLc < ilCount-1; ilLc++)
		{
			m_InsertList.GetText(ilLc,olText);
			olOrgText = olText;
			if(!olText.IsEmpty())
			{
				ilRealCount++;
				ExtractItemList(olText, &olItemList, '#');
				if(olItemList.GetSize() > 1)
				{
					olText = olItemList[1];
					pomPossilbeList->SetStyleRange(CGXRange(ilRealCount, imHideColStart+1, ilRealCount, imHideColStart+1), olStyle.SetValue(olItemList[0]));

				}
				ExtractItemList(olText, &olItemList, ';');
				for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
					pomPossilbeList->SetStyleRange(CGXRange(ilRealCount, ilItem+1, ilRealCount, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
				pomPossilbeList->SetStyleRange(CGXRange(ilRealCount, imHideColStart, ilRealCount, imHideColStart), olStyle.SetValue(olOrgText));
			}
		}

		pomPossilbeList->SortTable();
		pomPossilbeList->SetTopRow(ilTopRow);


		// save current sort information and top row
		ilTopRow = pomSelectedList->GetTopRow();

		pomSelectedList->RemoveRows(1,  pomSelectedList->GetRowCount());
		ilCount = m_ContentList.GetCount()+ 1;
		ilRealCount = 0;
		pomSelectedList->SetRowCount(max(18,ilCount));
		for (ilLc = 0; ilLc < ilCount-1; ilLc++)
		{
			m_ContentList.GetText(ilLc,olText);
			olOrgText = olText;
			if(!olText.IsEmpty())
			{
				ilRealCount++;
				ExtractItemList(olText, &olItemList, '#');
				if(olItemList.GetSize() > 1)
				{
					olText = olItemList[1];
					pomSelectedList->SetStyleRange(CGXRange(ilRealCount, imHideColStart+1, ilRealCount, imHideColStart+1), olStyle.SetValue(olItemList[0]));

				}
				ExtractItemList(olText, &olItemList, ';');
				for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
					pomSelectedList->SetStyleRange(CGXRange(ilRealCount, ilItem+1, ilRealCount, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
				pomSelectedList->SetStyleRange(CGXRange(ilRealCount, imHideColStart, ilRealCount, imHideColStart), olStyle.SetValue(olOrgText));
			}
		}

		pomSelectedList->SortTable();
		pomSelectedList->SetTopRow(ilTopRow);


	    pomPossilbeList->SetReadOnly(TRUE);
	    pomSelectedList->SetReadOnly(TRUE);
	}
	
}

void CAloFilterPage::OnButtonRemove() 
{
	// TODO: Add your control notification handler code here
	CString olText;
	CGXStyle olStyle;
	CRowColArray olRows;
	bool blFoundAlle = false;

	// Move selected items from right list box to left list box
	int ilSelCount = (int)pomSelectedList->GetSelectedRows( olRows);
	
	// Anything to do ?
	if(ilSelCount > 0)
	{
		// enable grid modification
	    pomPossilbeList->SetReadOnly(FALSE);
	    pomSelectedList->SetReadOnly(FALSE);

#pragma message ("The following line corrected due to prevent generating empty lines")
//		int ilContCount = m_ContentList.GetCount()+ 1;
		int ilContCount = m_ContentList.GetCount();
		m_ContentList.ResetContent();
		for (int ilLc = 1; ilLc < min(ilContCount+1,(int)pomSelectedList->GetRowCount()); ilLc++)
		{
			olText = pomSelectedList->GetValueRowCol(ilLc, imHideColStart);
			m_ContentList.AddString(olText);
		}


		// Move selected items from left list box to right list box
		CString olComplText;

		for (ilLc = olRows.GetSize()-1;ilLc >= 0 && blFoundAlle == false; ilLc--)
		{
			olText = pomSelectedList->GetValueRowCol(olRows[ilLc], 1);
			olComplText = pomSelectedList->GetValueRowCol(olRows[ilLc], imHideColStart);

			int ilDummy = (int)olRows[ilLc];
			if(ilDummy <= 0 || olComplText.IsEmpty())
				continue;

			// '*ALL' selected ?
			if (olText == LoadStg(IDS_STRING423))
			{
				m_AddButton.EnableWindow(TRUE);
				blFoundAlle = true;
				// show empty contents list
				m_ContentList.ResetContent();
				// rebuild insert list
				m_InsertList.ResetContent();
				m_InsertList.AddString(LoadStg(IDS_STRING423));
				for (ilLc = 0; ilLc < omPossibleItems.GetSize(); ilLc++)
				{
					m_InsertList.AddString(omPossibleItems[ilLc]);
				}

			}
			else
			{
				int iltest = m_ContentList.FindStringExact(-1, olComplText);
				m_InsertList.AddString(olComplText);	// move string from right to left box
				m_ContentList.DeleteString(m_ContentList.FindStringExact(-1, olComplText));
			}
		}
	
		olStyle.SetEnabled(FALSE);
		olStyle.SetReadOnly(TRUE);

		// save current sort information and top row
		int ilTopRow = pomPossilbeList->GetTopRow();
		
		pomPossilbeList->RemoveRows(1,  pomPossilbeList->GetRowCount());

		CStringArray olItemList;

		int ilCount = m_InsertList.GetCount() + 1;
		
		CString olOrgText;

		pomPossilbeList->SetRowCount(max(18,ilCount));
		for ( ilLc = 0; ilLc < ilCount-1; ilLc++)
		{
			m_InsertList.GetText(ilLc,olText);
			olOrgText = olText;
			ExtractItemList(olText, &olItemList, '#');
			if(olItemList.GetSize() > 1)
			{
				olText = olItemList[1];
				pomPossilbeList->SetStyleRange(CGXRange(ilLc+1, imHideColStart+1, ilLc+1, imHideColStart+1), olStyle.SetValue(olItemList[0]));

			}

			ExtractItemList(olText, &olItemList, ';');
			for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
				pomPossilbeList->SetStyleRange(CGXRange(ilLc+1, ilItem+1, ilLc+1, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
			pomPossilbeList->SetStyleRange(CGXRange(ilLc+1, imHideColStart, ilLc+1, imHideColStart), olStyle.SetValue(olOrgText));

		}

		pomPossilbeList->SortTable();
		pomPossilbeList->SetTopRow(ilTopRow);

		
		// save current sort information and top row
		ilTopRow = pomSelectedList->GetTopRow();

		pomSelectedList->RemoveRows(1,  pomSelectedList->GetRowCount());
		ilCount = m_ContentList.GetCount()+ 1;;
		pomSelectedList->SetRowCount(max(18,ilCount));
		for (ilLc = 0; ilLc < ilCount-1; ilLc++)
		{
			m_ContentList.GetText(ilLc,olText);
			olOrgText = olText;
			ExtractItemList(olText, &olItemList, '#');
			if(olItemList.GetSize() > 1)
			{
				olText = olItemList[1];
				pomSelectedList->SetStyleRange(CGXRange(ilLc+1, imHideColStart+1, ilLc+1, imHideColStart+1), olStyle.SetValue(olItemList[0]));

			}
			ExtractItemList(olText, &olItemList, ';');
			for(int ilItem = 0 ; ilItem < olItemList.GetSize(); ilItem++)
				pomSelectedList->SetStyleRange(CGXRange(ilLc+1, ilItem+1, ilLc+1, ilItem+1), olStyle.SetValue(olItemList[ilItem]));
			pomSelectedList->SetStyleRange(CGXRange(ilLc+1, imHideColStart, ilLc+1, imHideColStart), olStyle.SetValue(olOrgText));

		}

		pomSelectedList->SortTable();
		pomSelectedList->SetTopRow(ilTopRow);

		pomPossilbeList->SetReadOnly(TRUE);
	    pomSelectedList->SetReadOnly(TRUE);

	}
	
}
