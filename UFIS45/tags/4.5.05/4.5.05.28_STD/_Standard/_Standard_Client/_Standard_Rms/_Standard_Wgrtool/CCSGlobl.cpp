// CCSGlobl.cpp: implementation of the CCSGlobl .
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <CCSCedaCom.h>
#include <CCSDdx.h>
#include <CCSLog.h>
#include <CCSBcHandle.h>
#include <BasicData.h>
#include <PrivList.h>

#include <CedaCfgData.h>
#include <CCSBasic.h>
#include <CedaBasicData.h>
 

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Global Variable Section

const char *pcgAppName = "WGRTool";
CString ogAppName = "WGRTool"; // Name of *.exe file of this

char pcgHome[4] = "   "; 
char pcgTableExt[4]= "   "; 

char pcgUser[33];
char pcgPasswd[33];

CTime ogLoginTime		= -1;

CString ogHome;
CString ogTableExt;

CCSLog          ogLog(pcgAppName);
CCSDdx			ogDdx;
CCSCedaCom      ogCommHandler(&ogLog, pcgAppName);  
CCSBcHandle     ogBcHandle(&ogDdx, &ogCommHandler, &ogLog);
CBasicData      ogBasicData;
CCSBasic		ogCCSBasic;
CedaBasicData	ogBCD("", &ogDdx, &ogBcHandle, &ogCommHandler, &ogCCSBasic);

CedaCfgData		ogCfgData;

				
PrivList		ogPrivList;


long  igWhatIfUrno;
CString ogWhatIfKey;

bool bgNoScroll;
bool bgOnline = true;
bool bgIsButtonListMovable = false;
CInitialLoadDlg *pogInitialLoad;

CFont ogSmallFonts_Regular_6;
CFont ogSmallFonts_Regular_7;
CFont ogMSSansSerif_Regular_6;
CFont ogMSSansSerif_Regular_8;
CFont ogMSSansSerif_Regular_12;
CFont ogMSSansSerif_Regular_16;
CFont ogMSSansSerif_Bold_8;
CFont ogMSSansSerif_Bold_12;
CFont ogCourier_Bold_10;
CFont ogCourier_Regular_10;
CFont ogCourier_Regular_8;
CFont ogCourier_Regular_9;

CFont ogScalingFonts[4];
FONT_INDEXES ogStaffIndex;
FONT_INDEXES ogFlightIndex;
FONT_INDEXES ogHwIndex;
FONT_INDEXES ogRotIndex;

BOOL bgIsInitialized = FALSE;

CBrush *ogBrushs[MAXCOLORS+1];
COLORREF ogColors[MAXCOLORS+1];

COLORREF lgBkColor = SILVER;
COLORREF lgTextColor = BLACK;
COLORREF lgHilightColor = WHITE;

CPoint ogMaxTrackSize = CPoint(1024, 768);
CPoint ogMinTrackSize = CPoint(1024 / 4, 768 / 4);

CString ogAnsicht;
/////////////////////////////////////////////////////////////////////////////

void CreateBrushes()
{
	 int ilLc;

	for( ilLc = 0; ilLc < FIRSTCONFLICTCOLOR; ilLc++)
		ogColors[ilLc] = RED;
	ogColors[2] = GRAY;
	ogColors[3] = GREEN;
	ogColors[4] = RED;
	ogColors[5] = BLUE;
	ogColors[6] = SILVER;
	ogColors[7] = MAROON;
	ogColors[8] = OLIVE;
	ogColors[9] = NAVY;
	ogColors[10] = PURPLE;
	ogColors[11] = TEAL;
	ogColors[12] = LIME;
	ogColors[13] = YELLOW;
	ogColors[14] = FUCHSIA;
	ogColors[15] = AQUA;
	ogColors[16] = WHITE;
	ogColors[17] = BLACK;
	ogColors[18] = ORANGE;


// don't use index 21 it's used for Break jobs already

	for(ilLc = 0; ilLc < MAXCOLORS; ilLc++)
	{
		ogBrushs[ilLc] = new CBrush(ogColors[ilLc]);
	}

	// create a break job brush pattern
	/*
    delete ogBrushs[21];
	CBitmap omBitmap;
	omBitmap.LoadBitmap(IDB_BREAK);
	ogBrushs[21] = new CBrush(&omBitmap);
    */
}

void DeleteBrushes()
{
	for( int ilLc = 0; ilLc < MAXCOLORS; ilLc++)
	{
		CBrush *olBrush = ogBrushs[ilLc];
		delete olBrush;
	}
}


void InitFont() 
{
    CDC dc;
    ASSERT(dc.CreateCompatibleDC(NULL));

    LOGFONT logFont;
    memset(&logFont, 0, sizeof(LOGFONT));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(6, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts");
    ASSERT(ogSmallFonts_Regular_6.CreateFontIndirect(&logFont));
        
	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(7, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts");
    ASSERT(ogSmallFonts_Regular_7.CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(10, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ASSERT(ogCourier_Bold_10.CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(10, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ASSERT(ogCourier_Regular_10.CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ASSERT(ogCourier_Regular_8.CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(9, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ASSERT(ogCourier_Regular_9.CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(6, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts"); /*"MS Sans Serif""Arial"*/
    ASSERT(ogScalingFonts[MS_SANS6].CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogScalingFonts[MS_SANS8].CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(12, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogScalingFonts[MS_SANS12].CreateFontIndirect(&logFont));

	// logFont.lfHeight = - MulDiv(16, dc.GetDeviceCaps(LOGPIXELSY), 72);
	logFont.lfCharSet= DEFAULT_CHARSET;
	logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogScalingFonts[MS_SANS16].CreateFontIndirect(&logFont));
 
	ogStaffIndex.VerticalScale = MS_SANS8;
	ogStaffIndex.Chart = MS_SANS6;
	ogFlightIndex.VerticalScale = MS_SANS8;
	ogFlightIndex.Chart = MS_SANS6;
	ogHwIndex.VerticalScale = MS_SANS8;
	ogHwIndex.Chart = MS_SANS6;

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogMSSansSerif_Bold_8.CreateFontIndirect(&logFont));

	logFont.lfCharSet= DEFAULT_CHARSET;
    logFont.lfHeight = - MulDiv(12, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogMSSansSerif_Bold_12.CreateFontIndirect(&logFont));

    dc.DeleteDC();
}

