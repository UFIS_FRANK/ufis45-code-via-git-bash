#if !defined(AFX_NEWGROUPDLG_H__CB0F3B81_9E73_11D4_BFE2_00010215BFDE__INCLUDED_)
#define AFX_NEWGROUPDLG_H__CB0F3B81_9E73_11D4_BFE2_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NewGroupDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CNewGroupDlg dialog

class CNewGroupDlg : public CDialog
{
// Construction
public:
	CNewGroupDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CNewGroupDlg)
	enum { IDD = IDD_NEW_GROUP };
	CButton	m_OK;
	CStatic	m_Description;
	CString	m_GroupDescription;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNewGroupDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CNewGroupDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnUpdateEditDescription();
	afx_msg void OnChangeEditDescription();
	afx_msg void OnKillfocusEditDescription();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private: // helpers
	CComboBox*	pomGroups;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NEWGROUPDLG_H__CB0F3B81_9E73_11D4_BFE2_00010215BFDE__INCLUDED_)
