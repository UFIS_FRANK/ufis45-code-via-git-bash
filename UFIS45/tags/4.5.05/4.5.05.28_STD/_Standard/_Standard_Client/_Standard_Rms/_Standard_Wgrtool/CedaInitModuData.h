// CedaInitModuData.h

#ifndef __CEDAINITMODUDATA__
#define __CEDAINITMODUDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//--Class declaratino-------------------------------------------------------------------------------------------------------

class CedaInitModuData: public CCSCedaData
{
public:

// Operations
	CedaInitModuData();
	~CedaInitModuData();

	bool SendInitModu();
	CString GetInitModuTxt();
	CString GetInitModuTxt1();
	CString GetInitModuTxt2();
	CString GetInitModuTxt3();

// Variaben
	char pcmInitModuFieldList[200];

};

//---------------------------------------------------------------------------------------------------------

extern CedaInitModuData ogInitModuData;


#endif //__CEDAINITMODUDATA__

