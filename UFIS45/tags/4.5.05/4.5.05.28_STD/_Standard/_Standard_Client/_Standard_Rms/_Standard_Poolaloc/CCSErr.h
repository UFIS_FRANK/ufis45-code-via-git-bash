// CCSErr.h: interface for the CCSErr class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CCSERR_H__F6245E64_0C5B_11D4_A713_00010204AA63__INCLUDED_)
#define AFX_CCSERR_H__F6245E64_0C5B_11D4_A713_00010204AA63__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000



class ostream; // forward decl.


// Simple class for #int#-like error codes. Objects can be assigned,
//  compared and printed.

class CCSReturnCode
{
public:
    // constructor
    CCSReturnCode(int c = 0, const char *s = "Not set");

    // #int# type conversion operator
    operator int() const;

    // assignment operator
    CCSReturnCode& operator=(const CCSReturnCode& other);

    // test on equality operator
    BOOL operator==(const CCSReturnCode& other) const;

    // output to stream
    friend ostream& operator<<(ostream&, const CCSReturnCode&);


private:
    const char *pcmName; // name (for debugging)
    int         imCode;  // integer code
};


// indicates success, good, ok, ...
extern const CCSReturnCode RCSuccess;

// indicates failure, bad, error, ...
extern const CCSReturnCode RCFailure;

#endif // !defined(AFX_CCSERR_H__F6245E64_0C5B_11D4_A713_00010204AA63__INCLUDED_)
