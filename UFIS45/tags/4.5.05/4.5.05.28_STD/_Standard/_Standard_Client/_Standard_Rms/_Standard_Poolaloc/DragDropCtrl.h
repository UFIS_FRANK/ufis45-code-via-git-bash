// DragDropCtrl.h: interface for the DragDropCtrl class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DRAGDROPCTRL_H__F6245E66_0C5B_11D4_A713_00010204AA63__INCLUDED_)
#define AFX_DRAGDROPCTRL_H__F6245E66_0C5B_11D4_A713_00010204AA63__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class DragDropCtrl  
{
public:
	DragDropCtrl();
	virtual ~DragDropCtrl();

};

#endif // !defined(AFX_DRAGDROPCTRL_H__F6245E66_0C5B_11D4_A713_00010204AA63__INCLUDED_)
