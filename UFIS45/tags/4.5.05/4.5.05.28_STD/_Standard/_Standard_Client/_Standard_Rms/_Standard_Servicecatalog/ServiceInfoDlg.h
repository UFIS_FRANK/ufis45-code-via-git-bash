#if !defined(AFX_SERVICEINFODLG_H__B74E3344_59EE_11D3_935A_00001C033B5D__INCLUDED_)
#define AFX_SERVICEINFODLG_H__B74E3344_59EE_11D3_935A_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ServiceInfoDlg.h : header file
//

class RecordSet;

/////////////////////////////////////////////////////////////////////////////
// CServiceInfoDlg dialog

class CServiceInfoDlg : public CDialog
{
// Construction
public:
	CServiceInfoDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CServiceInfoDlg)
	enum { IDD = IDD_SERVICE_INFO };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA
protected:
	RecordSet *pomAktService;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CServiceInfoDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
public:
	int DoModal(RecordSet *popActService) ;

// Implementation
protected:
	void IniStatics ();
	void SetInfoFields ();

	// Generated message map functions
	//{{AFX_MSG(CServiceInfoDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERVICEINFODLG_H__B74E3344_59EE_11D3_935A_00001C033B5D__INCLUDED_)
