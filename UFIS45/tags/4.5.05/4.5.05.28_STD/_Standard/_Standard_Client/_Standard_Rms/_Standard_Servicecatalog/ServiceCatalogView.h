// ServiceCatalogView.h : interface of the CServiceCatalogView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SERVICECATALOGVIEW_H__E523203F_3849_11D3_933A_00001C033B5D__INCLUDED_)
#define AFX_SERVICECATALOGVIEW_H__E523203F_3849_11D3_933A_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include <CCSEdit.h>
#include <CCSGlobl.h>
#include <PersonellData.h>

class CGXGridWnd;
class CGridControl;
class CServiceGrid;
class CControlGrid;
class CVarServiceGrid;
class RecordSet;
class CGridRecordXChange;
class CPersonalXChange;
class CLocationGridRecordXChange;
class CServiceList;
class CFunctionsGrid;
class CQualificationsGrid;
class CServiceCatalogDoc;
 

class CServiceCatalogView : public CFormView
{
protected: // create from serialization only
	CServiceCatalogView();
	DECLARE_DYNCREATE(CServiceCatalogView)
public:  //  Member variables
	//  CGridControl *pomTables[6];
	//CServiceGrid *pomPFCGrid;
	CFunctionsGrid *pomPFCGrid;
	//CServiceGrid *pomPERGrid;
	CQualificationsGrid *pomPERGrid;
	CControlGrid	*pomEQTGrid;
	CVarServiceGrid *pomEQUGrid;
	CControlGrid	*pomLLGGrid;
	CVarServiceGrid *pomLGSGrid;
	CGridRecordXChange	*pomSEEXChange;
//	CGridRecordXChange	*pomSEFXChange;
//	CGridRecordXChange	*pomSEQXChange;
	CPersonalXChange    *pomPersXChange;
	CLocationGridRecordXChange *pomSELXChange;
	RecordSet		*pomDspSrv;		//  aktuell angezeigter Service
//	RecordSet		*pomClipBoard;  //  Service in Zwischenablage
	CGXGridWnd		*pomGridToPrint;
	CServiceList	*pomHiddenList;
	bool			bmModified;
	bool			bmFunctionsExpanded;
	CPersonellData	omServicePersonal;
public:
	//{{AFX_DATA(CServiceCatalogView)
	enum { IDD = IDD_SERVICECATALOG_FORM };
	CButton	omCheckBoxPersonell;
	CButton	omCheckBoxLocation;
	CButton	omCheckBoxEquip;
	int		m_Lkbz;
	CString	m_Lknm;
	CString	m_Dtyp;
	CString	m_Frmt;
	CString	m_Prmt;
	CString	m_Lkcc;
	CString	m_Lkan;
	CString	m_Lkar;
	CString	m_Atrn;
	CString	m_Lkhc;
	CCSEdit	m_LkccEdit;
	CCSEdit	m_LknmEdit;
	CCSEdit	m_LkhcEdit;
	CCSEdit	m_DtypEdit;
	CCSEdit	m_FrmtEdit;
	CCSEdit	m_PrmtEdit;
	CStatic m_FrmtText;
	CStatic m_PrmtText;
	CStatic m_DtypText;
	BOOL	m_bEquipment;
	BOOL	m_bLocation;
	BOOL	m_bPersonnel;
	int		m_Rtwf;
	int		m_Rtwt;
	int		m_Fset;
	BOOL	m_Fdut;
	BOOL	m_Fsdt;
	BOOL	m_Fsut;
	BOOL	m_Fwtf;
	BOOL	m_Fwtt;
	CCSEdit	m_SdutEdit;
	CString	m_Sdut;
	CCSEdit	m_SsutEdit;
	CString	m_Ssut;
	CString	m_Ssdt;
	CCSEdit	m_SsdtEdit;
	CString	m_Swtf;
	CCSEdit	m_SwtfEdit;
	CString	m_Swtt;
	CCSEdit	m_SwttEdit;
	BOOL	m_Ffis;
	//}}AFX_DATA
	/*CString	m_CdatTime;
	CString	m_CdatDate;
	CString	m_LstuDate;
	CString	m_LstuTime;
	CString	m_Usec;
	CString	m_Useu;*/
	CString	m_Vafr,	m_Vato;
	int     m_DefLkbz;	//  Standard default reference (Initialisierung)
	
// Attributes
public:
	CServiceCatalogDoc* GetDocument();
	void PrintGrid ( CGXGridWnd *popGridToPrint );
	void DoPrintSettings ();
	void OnCedaTablesLoaded ();
//	void PrintPreviewGrid ( CGXGridWnd *popGridToPrint );
	void UpdateChoiceLists ( CString opTable, int ipBCType, 
							 RecordSet* popData, CString opResTable );

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CServiceCatalogView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL
	virtual void OnPrepareDC( CDC* pDC, CPrintInfo* pInfo=NULL);

// Implementation
public:
	virtual ~CServiceCatalogView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	void DisplayService ( RecordSet &opService );
	bool HandleModifiedService ();

protected:
	void InitializeGrids ();
	void ResetValues ();
	void ResetGridValues ();
	bool CheckInput ();
	void DisplaySelectedResources ( RecordSet &opService );
	void EnablePersonnelGrids ( BOOL enable );
	void EnableEquipmentGrids ( BOOL enable );
	void EnableLocationsGrids ( BOOL enable );
	void EnableAllGrids ( BOOL enable );
	void EnableControls ();
	int SaveAllRecords  ();
	void InitializeGridXChanges ();
	void ResetGridXChanges ();
	void SetDocTitle ();
	void IniStatics ();
	inline void SetDlgText (UINT idc, UINT ids)		
			{SetDlgItemText(idc, GetString(ids) ); }
	void SetGridDirtyFlags ( bool dirty=true );
	int GetGridDirtyFlags ();
	int GetCCSEditDirtyFlags();
	bool DoSave ();
	void SetCCSEditDirtyFlags ( bool dirty=true );
	void SetDefaultValitidy ();
	void SetBdpsState();
	// Generated message map functions
protected:
	//{{AFX_MSG(CServiceCatalogView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnFileOpen();
	afx_msg void OnSave();
	afx_msg void OnNew();
	afx_msg void OnCheckEquipment();
	afx_msg void OnCheckLocation();
	afx_msg void OnCheckPersonnel();
	afx_msg void OnCut();
	afx_msg void OnPaste();
	afx_msg void OnCopy();
	afx_msg void OnUpdateCut(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCopy(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePaste(CCmdUI* pCmdUI);
	afx_msg void OnFilePrint();
	afx_msg void OnFilePrintPreview();
	afx_msg void OnChange(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeEdit();
	afx_msg void OnClickedButton();
	afx_msg void OnValidity();
	afx_msg void OnClickedTimeFlag();
	afx_msg void OnUpdateOpen(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSave(CCmdUI* pCmdUI);
	afx_msg void OnUpdateNew(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePrint(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePrintPreview(CCmdUI* pCmdUI);
	afx_msg void OnUpdateValidity(CCmdUI* pCmdUI);
	//}}AFX_MSG
	//afx_msg void OnDateTimeChange( UINT id, NMHDR * pNotifyStruct, LRESULT * result );
	afx_msg LONG OnGridLButtonDblClk( UINT wParam, LPARAM lParam);
	afx_msg LONG OnBcAdd(UINT wParam, LONG /*lParam*/);
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in ServiceCatalogView.cpp
inline CServiceCatalogDoc* CServiceCatalogView::GetDocument()
   { return (CServiceCatalogDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERVICECATALOGVIEW_H__E523203F_3849_11D3_933A_00001C033B5D__INCLUDED_)
