// ServiceCatalog.h : main header file for the SERVICECATALOG application
//

#if !defined(AFX_SERVICECATALOG_H__E5232037_3849_11D3_933A_00001C033B5D__INCLUDED_)
#define AFX_SERVICECATALOG_H__E5232037_3849_11D3_933A_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include <resource.h>       // main symbols

class CServiceCatalogDoc;

/////////////////////////////////////////////////////////////////////////////
// CServiceCatalogApp:
// See ServiceCatalog.cpp for the implementation of this class
//

class CServiceCatalogApp : public CWinApp
{
public:
	CServiceCatalogApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CServiceCatalogApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL
//  Data members
	bool bmLoginByCommandLine;

// Implementation
	//{{AFX_MSG(CServiceCatalogApp)
	afx_msg void OnAppAbout();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CServiceCatalogDoc* GetOpenDocument ();
extern CServiceCatalogApp theApp;

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERVICECATALOG_H__E5232037_3849_11D3_933A_00001C033B5D__INCLUDED_)
