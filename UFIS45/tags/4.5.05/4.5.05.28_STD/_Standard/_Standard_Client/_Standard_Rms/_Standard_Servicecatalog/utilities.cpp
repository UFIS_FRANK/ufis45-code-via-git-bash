#include <stdafx.h>

#include <CCSGlobl.h>
#include <recordset.h>
#include <CCSPtrArray.h>

#include <mainfrm.h>
#include <servicecatalog.h>
#include <utilities.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



//  erstellt eine CMapStringToOb f�r das Feld mit dem Index ipRefFieldIndex
//	auf die Indices innerhalb der ropRecordList 
void GetMapForRecordList ( CCSPtrArray<RecordSet> &ropRecordList, 
						   CMapStringToPtr &ropRecordMap, int ipRefFieldIndex )
{
	int i, ilAnz = ropRecordList.GetSize ();
	RecordSet *polActRec;
	CString olStr;

	ropRecordMap.RemoveAll();
	for ( i=0; i<ilAnz; i++ )
	{
		polActRec = &(ropRecordList.ElementAt(i));
		olStr = polActRec->Values.ElementAt (ipRefFieldIndex);
		ropRecordMap.SetAt ( polActRec->Values[ipRefFieldIndex], polActRec );
	}	
}


bool GetPrivProfInt( LPCTSTR pcpSection, LPCTSTR pcpEntry, int ipDefault, 
					 int &ripValue, LPCTSTR pcpFileName )
{
	char buffer[81];
	bool blRet = false;
	int  ilHelp;

	if ( GetPrivProfString ( pcpSection,  pcpEntry, "", buffer, 81, pcpFileName ) &&
		 sscanf ( buffer, "%d", &ilHelp ) )
	{
		 ripValue = ilHelp;
		 blRet = true;
	}
	else
		ripValue = ipDefault;
	return blRet;
}

void WriPrivProfInt( LPCTSTR pcpSection, LPCTSTR pcpEntry, int ipValue, 
					 LPCTSTR pcpFileName )
{
	char buffer[81];
	sprintf ( buffer, "%d", ipValue );
	WritePrivateProfileString ( pcpSection, pcpEntry, buffer, pcpFileName );
}
	

bool GetPrivProfString ( LPCTSTR pcpSection, LPCTSTR pcpEntry, LPCTSTR pcpDefault, 
					     LPTSTR pcpValue, DWORD ipSize, LPCTSTR pcpFileName )
{
	char buffer1[11], buffer2[11];
	bool blRet = false;

	GetPrivateProfileString ( pcpSection, pcpEntry, "A", buffer1, 10, pcpFileName );
	GetPrivateProfileString ( pcpSection, pcpEntry, "B", buffer2, 10, pcpFileName );
	  
	blRet = strcmp ( buffer1, buffer2 ) ? false : true;
	GetPrivateProfileString ( pcpSection, pcpEntry, pcpDefault, pcpValue,
							  ipSize, pcpFileName );
	return blRet;


}

int FileErrMsg ( HWND hwnd, char *lpText, char *filna, char* lpCaption/*=0*/, 
				 UINT uType/*=MB_OK*/ )
{
	char *line, pclEmpty[2]="" ;
	int  iret;
	char *pclCaption = lpCaption ? lpCaption : pclEmpty;
	int  length = strlen (lpText) + strlen (filna) + 1;
	if ( !(line = new char[length]) )
		iret = MessageBox ( hwnd, lpText, pclCaption, uType );
	else
	{
		sprintf ( line, lpText, filna );
		iret = MessageBox ( hwnd, line, pclCaption, uType );
		delete line;
	}
	return iret;
}

int FileErrMsg ( HWND hwnd, UINT idcText, char *filna, UINT idcCaption/*=0*/, 
				 UINT uType/*=MB_OK*/ )	
{
	CString olText, olCaption;

	if ( idcText )
		olText = GetString ( idcText );
	if ( idcCaption )
		olCaption = GetString ( idcCaption );
	return FileErrMsg ( hwnd, pCHAR(olText), filna, pCHAR(olCaption), uType );
}

int LangMsgBox ( HWND hwnd, UINT idcText, UINT idcCaption/*=0*/, 
				 UINT uType/*=MB_OK*/ )   
{
	CString olText, olCaption;

	if ( idcText )
		olText = GetString ( idcText );
	if ( idcCaption )
		olCaption = GetString ( idcCaption );
	return MessageBox ( hwnd, pCHAR(olText), pCHAR(olCaption), uType );
}

void SetStatusBarText ( CString &ropText )
{
	CMainFrame *polFrm = (CMainFrame *)theApp.m_pMainWnd;
	if ( polFrm )
		polFrm->SetMessageText ( ropText );
}


bool SaveWindowPosition ( CWnd *popWnd, char *pcpIniEntry )
{
	char polEintrag[81];
	RECT rect;
	if ( !popWnd || !pcpIniEntry )
		return false;
	popWnd->GetWindowRect ( &rect );
	sprintf ( polEintrag, "%d %d %d %d", rect.left, rect.top, rect.right, 
										 rect.bottom );
	if ( WritePrivateProfileString ( "FENSTER", pcpIniEntry, polEintrag, 
									 ogIniFile ) )
		return true;
	else
		return false;
}


bool IniWindowPosition ( CWnd *popWnd, char *pcpIniEntry )
{
	char polEintrag[81];
	RECT rect;
	if ( !popWnd || !pcpIniEntry )
		return false;
	if ( !GetPrivProfString ( "FENSTER", pcpIniEntry, "", polEintrag, 80,
							  ogIniFile )  || 
		 ( sscanf ( polEintrag, "%d %d %d %d", &rect.left, &rect.top, &rect.right, 
										 &rect.bottom ) < 4 )
	   )
		return false;
	popWnd->MoveWindow ( &rect );
	return true;
}

int FindStringInside ( CString opWhole, CString opSub, 
					   BOOL bpCaseSensitiv/*=TRUE*/ )
{
	CString olWhole = opWhole;
	CString olSub = opSub;
	if ( !bpCaseSensitiv )
	{
		olWhole.MakeUpper();
		olSub.MakeUpper();
	}
	return olWhole.Find ( olSub );
}

BOOL EnableDlgItem ( CWnd *popWnd, UINT idc, BOOL enable/*=TRUE*/ )
{
	CWnd * polCtrl=0;
	
	if (popWnd) 
		polCtrl = popWnd->GetDlgItem ( idc );
	if ( !polCtrl )
		return FALSE;
	polCtrl->EnableWindow ( enable );
	return TRUE;
}


/*  F�r einen ausgewaehlten Resourcenamen aus EQUTAB oder SGRTAB holen */
CString GetEquipmentType ( CString &ropResCode )
{
	CString olTypeUrno;
	int		ilLen = ropResCode.GetLength ();

	if ( ropResCode.Left (1) == "*" )	//  static group
		olTypeUrno = ogBCD.GetFieldExt("SGR", "GRPN", "APPL", 
										ropResCode.Right(ilLen-1),
										"RULE_AFT", "STYP" );
	else	//  single equipment number
		olTypeUrno = ogBCD.GetField("EQU", "GCDE", ropResCode, "GKEY" );
	return olTypeUrno;
}