// ServiceGrid.cpp: implementation of the CServiceGrid class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include <CCSGlobl.h>
#include <CedaBasicData.h>

#include <ServiceCatalog.h>
#include <ServiceCatalogDoc.h>
#include <ServiceCatalogView.h>
#include <ControlGrid.h>
#include <utilities.h>
#include <ServiceGrid.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CServiceGrid::CServiceGrid( CWnd *popParent, UINT nID, CString opTitle )
	:CTitleGridControl ( popParent, nID, 3, 1 )
{
	SetTitle ( opTitle );
	GetParam()->EnableSelection	(GX_SELNONE );
}

CServiceGrid::CServiceGrid( CWnd *popParent, UINT nID, char * pcpTableName, 
							CServiceCatalogDoc *popDoc )
	:CTitleGridControl ( popParent, nID, 3, 1 )
{
	RESOURCETABLEINFO *polResInfo=0;
	ASSERT ( popDoc );
	
	IniLayOut ();
	if ( polResInfo = popDoc->GetResourceTableInfo ( pcpTableName ) )
	{
		SetTitle ( polResInfo->resourcetyp );
	    omTableName = polResInfo->table;
		omCodeField = polResInfo->codefield;
		omDisplayField = polResInfo->dspfield;
		CreateComboBoxes ( polResInfo->choicelist);
	}
	else
		CreateComboBoxes ();

	GetParam()->EnableSelection	(GX_SELNONE );
}

CServiceGrid::~CServiceGrid()
{

}

bool CServiceGrid::CreateComboBoxes ()
{
	char *pclChoiceList=0;
	int  ilLen, ilBuffLen;
	CString olCode; 
	ROWCOL	ilRowCount = GetRowCount();
	
	imDBRecords = ogBCD.GetDataCount(omTableName);
	ilLen = ogBCD.GetMaxFieldLength ( omTableName, omCodeField ) ;
	ilBuffLen = (ilLen+1)*imDBRecords + 1;
	pclChoiceList = new char[ilBuffLen];
	pclChoiceList[0] = '\0';
	if ( !pclChoiceList )
	{
		MessageBox ( "Allocation failure in >CServiceGrid::CreateComboBoxes<", 
					 "", MB_ICONEXCLAMATION );
		return false;
	}

	for ( int i=0; i<imDBRecords; i++ )
	{
		olCode = ogBCD.GetField ( omTableName, i, omCodeField );
		strcat ( pclChoiceList, (const char*)olCode );
		strcat ( pclChoiceList, "\n" );
	}

	SetStyleRange( CGXRange(2,1,ilRowCount,1), 
				   CGXStyle().SetControl(GX_IDS_CTRL_TEXTFIT).SetValue("")
				   .SetChoiceList(pclChoiceList) );
	delete pclChoiceList;

	return true;
}

bool CServiceGrid::CreateComboBoxes ( CString &ropChoiceList )
{
	ROWCOL	ilRowCount = GetRowCount();

	SetStyleRange( CGXRange(2,1,ilRowCount,1), 
				   CGXStyle().SetControl(GX_IDS_CTRL_TEXTFIT).SetValue("")
				   .SetChoiceList( pCHAR(ropChoiceList) ) );
	return true;
}

void CServiceGrid::IniLayOut ()
{
	ROWCOL	ilRowCount = GetRowCount();

	SetValue ( 0, 1, (CString)"Code" );
	SetValue ( 0, 2, (CString)"No." );
//	SetValue ( 0, 3, (CString)"Prio." );
	SetStyleRange(CGXRange(2,2,ilRowCount,3),CGXStyle()
          .SetControl(GX_IDS_CTRL_SPINEDIT)
		  .SetMaxLength(2).SetUserAttribute(GX_IDS_UA_SPINBOUND_MIN, "1")
		  .SetUserAttribute(GX_IDS_UA_SPINBOUND_MAX, "99")
		  .SetUserAttribute(GX_IDS_UA_VALID_MIN, "1")
		  .SetUserAttribute(GX_IDS_UA_VALID_MAX, "99")
		  .SetValueType (GX_VT_NUMERIC)
		  .SetFormat(GX_FMT_FIXED).SetPlaces(0) );
	EnableGridToolTips(TRUE);

	SetColWidth(0, 0, 20);
	//  SetColWidth(1, 1, 90);	/* PRF3710 */
	SetColWidth(1, 1, 140);		/* PRF3710 */
	SetColWidth(2, 3, 50);
	HideCols ( 3,3);			/* PRF3710 */

}

void CServiceGrid::DisplayInfo ( ROWCOL nRow, ROWCOL nCol )
{
	CString olWert, olAnzeige;
	
	olWert = GetValueRowCol ( nRow, nCol );
	if ( !olWert.IsEmpty() && olWert[0]!='*' )
		olAnzeige = ogBCD.GetField( omTableName, omCodeField, olWert, omDisplayField );
	SetStyleRange(CGXRange(nRow, nCol), CGXStyle()
				.SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, olAnzeige ));
	olAnzeige.Empty ();
	SetStatusBarText ( olAnzeige );
}

BOOL CServiceGrid::OnEndEditing( ROWCOL nRow, ROWCOL nCol) 
{
	BOOL ilRet = CTitleGridControl::OnEndEditing( nRow, nCol) ;

	//  Der Rest wird nur gemacht, 
	//  wenn die Spalte mit den Resourcen (d.h. Comboboxen) editiert worden ist
	if (nRow>=2 )
	{
		CGXStyle olStyle = LookupStyleRowCol ( nRow, nCol );
	//	BOOL ok = Undo( );
		SetDefaultValues ( nRow );
		DisplayInfo ( nRow, nCol );
		DeleteEmptyRows ( nRow, nCol ) ;
	}
	return ilRet;
}

void CServiceGrid::DoAutoGrow ( ROWCOL nRow, ROWCOL nCol)
{
	//  nur bei Eingabe in der ersten Spalte und letzter Zeile wird die
	//	Tabelle automatisch erweitert
	if ( nCol == 1 )		
	{
		CTitleGridControl::DoAutoGrow ( nRow, nCol );	
		ROWCOL ilRowCount = GetRowCount ();
		//  wurde wirklich eine Zeile eingef�gt
		if ( ilRowCount > nRow )				//  damit der von oben kopierte 
			DisplayInfo ( ilRowCount, nCol );	//  Tooltip zur�ckgesetzt wird
	}
}


void CServiceGrid::OnModifyCell(ROWCOL nRow, ROWCOL nCol)
{
	CTitleGridControl::OnModifyCell(nRow, nCol);

	if ( ( nCol != 1 ) || ( nRow<2 ) )
		return;
   
   	CString olWert, olAnzeige;
	CGXControl* pControl = GetControl(nRow, nCol);
	if ( !pControl->GetValue(olWert) )
		return;
	if ( !olWert.IsEmpty() )
		olAnzeige = ogBCD.GetField( omTableName, omCodeField, olWert, omDisplayField );
	SetStatusBarText ( olAnzeige );
}


bool CServiceGrid::DoppelteResourcen ()
{
	//  suche doppelt vergebene Resourcen
	CString olWert1, olWert2;
	ROWCOL	i, j, ilRowCount = GetRowCount();
	for ( i=2; i<ilRowCount; i++ )
	{
		olWert1 = GetValueRowCol ( i, 1 );
		for ( j=i+1; j<=ilRowCount; j++ )
		{
			olWert2 = GetValueRowCol ( j, 1 );
			if ( !olWert1.IsEmpty () && ( olWert1==olWert2 ) )
				return true;
		}
	}
	return false;
}

bool CServiceGrid::IsResourceAllReadySelected ( CString &ropResource, 
											    CString &ropTableName,
												ROWCOL ipDontTest, 
												bool bpErrMsg/*=true*/ )
{
	CString olWert;
	ROWCOL	ilRowCount = GetRowCount();

	for ( ROWCOL i=2; i<=ilRowCount; i ++ )
	{
		if ( i==ipDontTest )
			continue;
		olWert = GetValueRowCol ( i, 1 );
		if ( olWert == ropResource )
		{
			if ( bpErrMsg )
				MessageBox ( GetString(IDS_RES_ALREADY_SEL) );
			return true;
		}

	}
	return false;
}


//  DeleteEmptyRows:  
//	Checkt, ob �bergebene Zelle in der Spalte der Resourcen liegt und ,wenn ja,
//	ob diese Zelle ohne Inhalt verlassen worden ist
//  In:  nRow, nCol:  Zelle, die modifiziert worden ist
//	return:  true, wenn Zeile gel�scht wurde
//			 false sonst
bool CServiceGrid::DeleteEmptyRows ( ROWCOL nRow, ROWCOL nCol ) 
{
	ROWCOL  ilRowCount = GetRowCount();
	//  Wenn nicht in der ersten Spalte gel�scht bzw. editiert worden ist
	//  gibt es keine Zeile zu l�schen
	if ( nCol != 1 )
		return false;
	if ( ( nRow>=2 ) && ( nRow<ilRowCount ) &&
		  GetValueRowCol ( nRow, nCol ).IsEmpty() )
	{
		//RemoveRows( nRow, nRow );
		//SetRowHeadersText ();
		RemoveOneRow( nRow );
		return true;
	}
	else 
		return false;
}

void CServiceGrid::ResetValues ()
{
	ROWCOL ilRowCount = GetRowCount();
	SetValueRange ( CGXRange(2,1,ilRowCount,3), "" );
	if ( ilRowCount >= 3 )
		RemoveRows( 3, ilRowCount );
	SetDirtyFlag ( false );
}


bool CServiceGrid::SetLineValues ( int ipResNo, CString &ropTable, CString &ropCode,
								   CString &ropAmount, CString &ropPriority )
{
	int			index;
	CGXComboBox	*polCB;
	CString		olFound, olChoiceList;
	CGXStyle	olGXStyle;
	//ROWCOL		ilRowCount = GetRowCount();
	int			ilActRow = ipResNo + 2;

	polCB = (CGXComboBox*)GetControl ( ilActRow, 1 );
	ComposeStyleRowCol ( ilActRow, 1, &olGXStyle );
	if ( polCB && olGXStyle.GetIncludeChoiceList() ) 
	{
		olChoiceList = olGXStyle.GetChoiceList();
		index = polCB->FindStringInChoiceList( olFound, pCHAR(ropCode), 
											   pCHAR(olChoiceList), TRUE );
		if ( index>=0 ) 
		{
			SetValueRange ( CGXRange(ilActRow,1), ropCode );
			SetValueRange ( CGXRange(ilActRow,2), ropAmount );
			SetValueRange ( CGXRange(ilActRow,3), ropPriority );
			OnEndEditing ( ilActRow, 1 );
			return true;
		}
	}
	return false;
}

//  GetLineCount:  Liefert die Anzahl der Zeilen (ohne die beiden Headerzeilen)
//				   zur�ck; k�nnen aber auch leer sein
int CServiceGrid::GetLineCount ()
{
	ROWCOL ilRowCount = GetRowCount ();
	return ilRowCount-1;
}


void CServiceGrid::SetDefaultValues ( ROWCOL nRow )
{
	if ( nRow < 2 )
		return;
	CString olResource = GetValueRowCol ( nRow, 1 );
	//  Default-Wert f�r Anzahl '1', wenn noch kein Wert gesetzt ist
	CString olAnzahl = GetValueRowCol ( nRow, 2 );
	if ( !olResource.IsEmpty () && olAnzahl.IsEmpty() )
		SetValue ( nRow-1, 2, (CString)"1" );	
}


//  GetLineValues:  
//  IN:		ipLine, Nr. der gew�nschten Zeile [1..GetLineCount ()]
//	OUT:	ropTable: TableName, EQU, EXT....
//			ropUrno: Urno der gew�hlten Resource
//			ropCode: Code bzw. Name der gew�hlten Resource
//			ropAmount:	eingetragene Anzahl
//			ropPriority: eingetragene Prio
//  return: true, wenn in der Zeile eine Resource ausgew�hlt, false sonst
bool CServiceGrid::GetLineValues ( int ipLine, CString &ropTable, CString &ropUrno, 
								   CString &ropCode, CString &ropAmount, 
								   CString &ropPriority, CString &ropSubType, 
								   bool &rbpStaticGroup )
{
	CString olWert = GetValueRowCol ( ipLine+1, 1 );
	if ( olWert.IsEmpty() )
		return false;
	ropTable = omTableName;
	rbpStaticGroup = !IsSingleResourceSelected ( ipLine+1, ropTable, olWert );
	if (rbpStaticGroup) 
		ropUrno = ogBCD.GetField( "SGR", "GRPN", olWert, "URNO" );
	else
		ropUrno = ogBCD.GetField( omTableName, omCodeField, olWert, "URNO" );
	ropCode = olWert;
	ropAmount = GetValueRowCol ( ipLine+1, 2 ); 
	ropPriority = GetValueRowCol ( ipLine+1, 3 );
	ropSubType.Empty();
	return true;
}

int CServiceGrid::GetMaxAmount ()
{
	ROWCOL	ilRowCount = GetRowCount ();
	int     ilMax = 0, ilWert;
	CString olWert;

	for ( ROWCOL ilRow=2; ilRow<=ilRowCount; ilRow++ )
	{
		olWert = GetValueRowCol ( ilRow, 1 );
		if ( olWert.IsEmpty() )
			continue;	//  Keine Resource ausgew�hlt
		olWert = GetValueRowCol ( ilRow, 2 );
		if ( olWert.IsEmpty() || !sscanf ( pCHAR(olWert), "%u", &ilWert ) )
			ilWert = 1;		//  Default-Wert
		ilMax = max ( ilWert, ilMax );
	}
	return ilMax;
}

int CServiceGrid::GetSumOfAmounts ()
{
	ROWCOL	ilRowCount = GetRowCount ();
	int     ilSum = 0, ilWert;
	CString olWert;

	for ( ROWCOL ilRow=2; ilRow<=ilRowCount; ilRow++ )
	{
		olWert = GetValueRowCol ( ilRow, 1 );
		if ( olWert.IsEmpty() )
			continue;	//  Keine Resource ausgew�hlt
		olWert = GetValueRowCol ( ilRow, 2 );
		if ( olWert.IsEmpty() || !sscanf ( pCHAR(olWert), "%u", &ilWert ) )
			ilWert = 1;		//  Default-Wert
		ilSum += ilWert;
	}
	return ilSum;
}

BOOL CServiceGrid::OnValidateCell(ROWCOL nRow, ROWCOL nCol)
{
	BOOL blRet = CTitleGridControl::OnValidateCell(nRow, nCol);
	if ( blRet )
	{
		if (  ( nCol == 1 ) && ( nRow>=2 ) )
		{
			CString olWert ;
			CGXControl* pControl = GetControl(nRow, nCol);
			pControl->GetValue(olWert);
			//CString olWert = GetValueRowCol ( nRow, nCol );
			if ( !olWert.IsEmpty()  && 
				 IsResourceAllReadySelected ( olWert, GetTableName(nRow), nRow ) )
			{
				BOOL ok = Undo( );
				return FALSE;
			}
		}
	}
	return blRet;

}

CString CServiceGrid::GetTableName ( ROWCOL ipRow )
{
	return omTableName;
}


bool CServiceGrid::IsSingleResourceSelected ( ROWCOL ipLine, CString &ropTable, 
											  CString &ropCode )
{
	int			index;
	CString		olFound;
	CServiceCatalogDoc	*polDoc;
	RESOURCETABLEINFO	*pslTableInfo = 0;
	CGXComboBox		*polCBWnd;
	
	polDoc = GetOpenDocument ();
	if ( polDoc )
		pslTableInfo = polDoc->GetResourceTableInfo ( pCHAR(ropTable) );
	if ( !pslTableInfo )
		return true;		// default-Fall
	polCBWnd = (CGXComboBox*)GetControl ( ipLine, 1 );
	if ( !polCBWnd )
		return true;
	index = polCBWnd->FindStringInChoiceList( olFound, pCHAR(ropCode), 
											  pCHAR(pslTableInfo->choicelist),
											  TRUE );
	if ( !ropCode.IsEmpty() && ropCode[0] == '*' )
		ropCode = ropCode.Right ( ropCode.GetLength()-1 );
	return ( index<(int)pslTableInfo->singlevalues );
}

void CServiceGrid::ModifyChoiceLists ( CString opTable, CString opSubtype )
{
	CServiceCatalogDoc	*polDoc;
	RESOURCETABLEINFO	*polResInfo=0;
	
	if ( opTable == omTableName )
	{
		polDoc = GetOpenDocument ();
		ROWCOL	ilRowCount = GetRowCount();
		
		if ( polDoc )
			polResInfo = polDoc->GetResourceTableInfo ( pCHAR(opTable) );
		if ( polResInfo )
		{
			SetStyleRange( CGXRange(2,1,ilRowCount,1), 
					       CGXStyle().SetControl(GX_IDS_CTRL_TEXTFIT).
						   SetChoiceList( pCHAR(polResInfo->choicelist) ) );
		}
	}
}

/*
BOOL CServiceGrid::GetStyleRowCol( ROWCOL nRow, ROWCOL nCol, CGXStyle& style,
							     GXModifyType mt =gxCopy, int nType =0 )
{
	BOOL blRet = GetStyleRowCol(nRow, nCol, style, mt, nType );
	if ( style.GetIncludeChoiceList() )
	{
		CString& GetChoiceList() 
		CGXStyle& SetIncludeChoiceList(BOOL b);
	const const;
	const CString& GetChoiceListRef() const;
	CGXStyle& SetChoiceList(LPCTSTR s);
	CGXStyle& SetChoiceList(const CString& s);

}
*/

//  Class CVarServiceGrid:
//	spezielle Klasse "variable ServiceGrid" f�r Locations-Table, das f�r jede
//  Zeile unterschiedliche DB-Tabelle verwalten kann

CVarServiceGrid::CVarServiceGrid( CWnd *popParent, UINT nID, CString opTitle )
	:CServiceGrid( popParent, nID, opTitle )
{
	IniLayOut ();
}


bool CVarServiceGrid::SetOneTable ( CString opTableName, ROWCOL ipLine, CString opSubtype )
{
	CServiceCatalogDoc	*polDoc;
	RESOURCETABLEINFO	*pslTableInfo = 0;
	CString				olMsg, olActualTable, olActualSubtype, olChoiceList;
	bool				ok = false;
	ROWCOL	ilRowCount = GetRowCount();

	for ( UINT i= ilRowCount; !opTableName.IsEmpty() &&(i<=ipLine); i++ )
		InsertBottomRow ();

	if ( opTableName.IsEmpty () ) 
	{	//  Zeile in Grid "Location Tables wurde gel�scht
		EnableLine ( ipLine, FALSE );
		//omTableNames.SetAtGrow ( ipLine, "" );
		SetTableName ( ipLine, 0 );
		return true;
	}
	//  if ( (int)ipLine <= omTableNames.GetUpperBound () )
	//		olActualTable = omTableNames.GetAt ( ipLine );
	olActualTable = GetTableName ( ipLine );
	olActualSubtype = GetSubtype ( ipLine );

	//  Wenn neuer Tablename, bzw. Subtype bereits aktuell angezeigt wird, fertig
	if ( ( olActualTable == opTableName ) && ( olActualSubtype == opSubtype ) )
		return true;

	polDoc = GetOpenDocument ();
	ASSERT ( polDoc );
	if ( polDoc )
		pslTableInfo = polDoc->GetResourceTableInfo ( pCHAR(opTableName) );
	if ( !pslTableInfo )
	{
		olMsg = "Table information not found for table\n" + opTableName;
		MessageBox ( olMsg, "<CVarServiceGrid::SetOneTable>", MB_OK | MB_ICONEXCLAMATION );
	}
	else
	{
//		if ( !pslTableInfo->choicelist.IsEmpty () )
			if ( opTableName == "EQU" )
			{
				polDoc->GetEquChoiceList ( opSubtype, olChoiceList );
				ok = SetComboBox ( olChoiceList, ipLine );
			}
			else
				ok = SetComboBox ( pslTableInfo->choicelist, ipLine );
//		else
//			ok = SetComboBox ( pCHAR(pslTableInfo->table), pCHAR(pslTableInfo->codefield), ipLine );
	}
	EnableLine ( ipLine, ok );
	//omTableNames.SetAtGrow ( ipLine, ok ? opTableName : "" );
	if ( opTableName == "EQU" )
		SetEquTableName ( ipLine, (ok) ? pCHAR(opSubtype) : 0 );
	else
		SetTableName ( ipLine, (ok && pslTableInfo) ? pCHAR(pslTableInfo->table) : 0 );
/*
	if ( ok )
		SetStyleRange ( CGXRange (ipLine,1), CGXStyle().SetItemDataPtr( pslTableInfo->table ) );
	else
		SetStyleRange ( CGXRange (ipLine,1), CGXStyle().SetItemDataPtr( 0 ) );
*/
	return ok;
}


BOOL CVarServiceGrid::OnEndEditing( ROWCOL nRow, ROWCOL nCol) 
{
	BOOL blRet = CTitleGridControl::OnEndEditing( nRow, nCol) ;
	//  Der Rest wird nur gemacht, 
	//  wenn die Spalte mit den Resourcen (d.h. Comboboxen) editiert worden ist
	if (nRow>=2 )
		SetDefaultValues ( nRow );
	return blRet;
}


bool CVarServiceGrid::SetComboBox ( char *pcpTableName, char *pcpCodeField, ROWCOL ipLine )
{
	char *pclChoiceList=0;
	int  ilLen, ilBuffLen;
	CString olCode; 

	imDBRecords = ogBCD.GetDataCount(pcpTableName);
	ilLen = ogBCD.GetMaxFieldLength ( pcpTableName, pcpCodeField ) ;
	ilBuffLen = (ilLen+1)*imDBRecords + 1;
	pclChoiceList = new char[ilBuffLen];
	pclChoiceList[0] = '\0';
	if ( !pclChoiceList )
	{
		MessageBox ( "Allocation failure in >CVarServiceGrid::SetComboBox<", 
					 "", MB_ICONEXCLAMATION );
		return false;
	}

	for ( int i=0; i<imDBRecords; i++ )
	{
		olCode = ogBCD.GetField ( pcpTableName, i, pcpCodeField );
		strcat ( pclChoiceList, (const char*)olCode );
		strcat ( pclChoiceList, "\n" );
	}

	SetStyleRange( CGXRange(ipLine,1), 
				   CGXStyle().SetControl(GX_IDS_CTRL_TEXTFIT).SetValue("")
				   .SetChoiceList(pclChoiceList) );
	SetValueRange ( CGXRange(ipLine,2,ipLine,3), "" ); 
	delete pclChoiceList;
	return true;
}

bool CVarServiceGrid::SetComboBox ( CString &ropChoiceList, ROWCOL ipLine )
{
	SetStyleRange( CGXRange(ipLine,1), 
				   CGXStyle().SetControl(GX_IDS_CTRL_TEXTFIT).SetValue("")
				   .SetChoiceList(pCHAR(ropChoiceList) ) );
	SetValueRange ( CGXRange(ipLine,2,ipLine,3), "" ); 
	return true;
}

void CVarServiceGrid::EnableLine ( ROWCOL ipLine, BOOL enable )
{
	if ( ipLine > GetRowCount() )
		return;
	if ( !enable )
	{
		SetStyleRange (CGXRange(ipLine,1), CGXStyle().SetDefault() );
	//	SetValueRange ( olRangeLine, "" ); 
	}
	SetStyleRange( CGXRange().SetRows(ipLine), CGXStyle().SetEnabled(enable) );

}

void CVarServiceGrid::ResetValues ()
{
	CServiceGrid::ResetValues ();
	SetStyleRange ( CGXRange(2,1,2,1), CGXStyle().SetDefault() );
	//omTableNames.RemoveAll();
}


BOOL CVarServiceGrid::InsertBottomRow ()
{
	BOOL blRet = CTitleGridControl::InsertBottomRow ();
	ROWCOL ilRowCount = GetRowCount ();
	EnableLine ( ilRowCount, FALSE );
	return blRet;
}

//  GetLineValues:  
//  IN:		ipLine, Nr. der gew�nschten Zeile [1..GetLineCount ()]
//	OUT:	ropTable: TableName, EQU, EXT....
//			ropUrno: Urno der gew�hlten Resource
//			ropCode: Code bzw. Name der gew�hlten Resource
//			ropAmount:	eingetragene Anzahl
//			ropPriority: eingetragene Prio
//  return: true, wenn in der Zeile eine Resource ausgew�hlt, false sonst
bool CVarServiceGrid::GetLineValues ( int ipLine, CString &ropTable, CString &ropUrno, 
								   CString &ropCode, CString &ropAmount, 
								   CString &ropPriority, CString &ropSubType, bool &rbpStaticGroup )
{
	CServiceCatalogDoc	*polDoc;
	RESOURCETABLEINFO	*pslTableInfo = 0;
	CString				olMsg;
	CString				olWert = GetValueRowCol ( ipLine+1, 1 );
	if ( olWert.IsEmpty() )
		return false;
	//  ropTable = omTableNames.GetAt ( ipLine+1 );
	ropTable = GetTableName ( ipLine+1 );
	ropSubType = GetSubtype ( ipLine+1 );

	polDoc = GetOpenDocument ();
	ASSERT ( polDoc );
	pslTableInfo = polDoc->GetResourceTableInfo ( pCHAR(ropTable) );
	if ( !pslTableInfo )
	{
		olMsg = "Table information not found for table\n" + ropTable;
		MessageBox ( olMsg, "<CVarServiceGrid::GetLineValues>", MB_OK | MB_ICONEXCLAMATION );
	}
	else
	{
		rbpStaticGroup = !IsSingleResourceSelected ( ipLine+1, ropTable, olWert );
		if (rbpStaticGroup) 
			ropUrno = ogBCD.GetField( "SGR", "GRPN", olWert, "URNO" );
		else
			ropUrno = ogBCD.GetField( ropTable, pslTableInfo->codefield, olWert, "URNO" );
	}
	ropCode = olWert;
	ropAmount = GetValueRowCol ( ipLine+1, 2 ); 
	ropPriority = GetValueRowCol ( ipLine+1, 3 );
	return true;
}


bool CVarServiceGrid::SetLineValues ( int ipResNo, CString &ropTable, CString &ropCode,
									  CString &ropAmount, CString &ropPriority )
{
	CServiceCatalogView *polView = (CServiceCatalogView *)GetParent(); 
	//ROWCOL				ilRowCount = GetRowCount();
	bool				blRet;
	CString				olEquipType;
	//SetOneTable ( ropTable, ilRowCount );

	//  Vor dem Aufruf der Baseclass-Funktion, mu� mit implizitem "SetOneTable" von 
	//  locationGrid aus AuswahlListe der Combobox initialisiert werden
	if ( polView )
	{
		if ( ropTable != "EQU" )
			polView->pomLLGGrid->SelectResource ( ropTable, ipResNo );
		else
		{
			olEquipType = GetEquipmentType ( ropCode );
			polView->pomEQTGrid->SelectResource ( olEquipType, ipResNo );
		}
	}
	blRet = CServiceGrid::SetLineValues ( ipResNo, ropTable, ropCode, ropAmount, ropPriority );
	//if ( blRet && polView )
	//	polView->pomLLGGrid->SelectLocation ( ropTable, ilRowCount );
	return blRet;
}

CString CVarServiceGrid::GetTableName ( ROWCOL ipRow )
{
	char		*polData;
	int			ilDelChar;
	CGXStyle	olStyle;
	CString		olTable;
	
	ComposeStyleRowCol( ipRow,1, &olStyle );
	polData = (char*)olStyle.GetItemDataPtr();
	if ( polData )
	{
		olTable = CString( polData );
		ilDelChar = olTable.Find('\r');
		if ( ilDelChar >= 0) 
			olTable = olTable.Left ( ilDelChar );
		return olTable;
	}
	else
		return CString( "" );
}

CString CVarServiceGrid::GetSubtype ( ROWCOL ipRow )
{
	char		*polData, *polDelChar;
	CGXStyle	olStyle;
	CString		olTableType;
	
	ComposeStyleRowCol( ipRow,1, &olStyle );
	polData = (char*)olStyle.GetItemDataPtr();
	if ( polData && ( polDelChar = strchr(polData,'\r') ) )
	{
		return CString( polDelChar+1 );
	}
	else
		return CString( "" );
}

void CVarServiceGrid::SetEquTableName ( ROWCOL ipRow, char *pcpEquType )
{
	char *polItemData = 0;
	CString	&rolGridLineInfo=(CString)"";
	CServiceCatalogDoc	*polDoc = GetOpenDocument ();
	
	if ( polDoc && pcpEquType && 
		 polDoc->omEquTableTypeStrings.Lookup ( pcpEquType, rolGridLineInfo ) )
	{
		polItemData = pCHAR(rolGridLineInfo);
	}
	if ( ipRow <= GetRowCount() ) 
		SetStyleRange ( CGXRange (ipRow,1), CGXStyle().SetItemDataPtr( polItemData ) );

}

void CVarServiceGrid::SetTableName ( ROWCOL ipRow, char *pcpTableName )
{
	if ( ipRow <= GetRowCount() ) 
		SetStyleRange ( CGXRange (ipRow,1), CGXStyle().SetItemDataPtr( pcpTableName ) );
}

bool CVarServiceGrid::SortTable ( ROWCOL ipRowClicked, ROWCOL ipColClicked )
{
	bool blRet = CTitleGridControl::SortTable ( ipRowClicked, ipColClicked );
	CString  olTableName, olEquipType;
	CServiceCatalogView *polView = (CServiceCatalogView *)GetParent(); 
	
	if ( blRet && polView )
	{
		for ( UINT i=2; i<=GetRowCount(); i++ )
		{
			olTableName = GetTableName ( i );
			if ( olTableName.IsEmpty() )
				continue;
			if ( olTableName != "EQU" )
				polView->pomLLGGrid->SelectResource ( olTableName, i-2, false ) ;
			else
			{
				olEquipType = GetSubtype ( i );
				polView->pomEQTGrid->SelectResource ( olEquipType, i-2, false );
			}
		}

	}
	return blRet;
}


bool CVarServiceGrid::IsResourceAllReadySelected ( CString &ropResource, 
												   CString &ropTableName,
												   ROWCOL ipDontTest, 
												   bool bpErrMsg/*=true*/ )
{
	CString olWert, olTableName;
	ROWCOL	ilRowCount = GetRowCount();

	for ( ROWCOL i=2; i<=ilRowCount; i ++ )
	{
		if ( i==ipDontTest )
			continue;
		olTableName = GetTableName ( i );
		olWert = GetValueRowCol ( i, 1 );
		if ( (olWert==ropResource) && (olTableName==ropTableName) )
		{
			if ( bpErrMsg )
				MessageBox ( GetString(IDS_RES_ALREADY_SEL) );
			return true;
		}

	}
	return false;
}


bool CVarServiceGrid::DoppelteResourcen ()
{
	//  suche doppelt vergebene Resourcen
	CString olWert1, olWert2;
	CString olTable1, olTable2;
		
	ROWCOL	i, j, ilRowCount = GetRowCount();
	for ( i=2; i<ilRowCount; i++ )
	{
		olWert1 = GetValueRowCol ( i, 1 );
		olTable1 = GetTableName ( i );
		for ( j=i+1; j<=ilRowCount; j++ )
		{
			olWert2 = GetValueRowCol ( j, 1 );
			olTable2 = GetTableName ( j );
			if ( !olWert1.IsEmpty () && ( olWert1==olWert2 ) 
				 && ( olTable1==olTable2 ) )
				return true;
		}
	}
	return false;
}

void CVarServiceGrid::ModifyChoiceLists ( CString opTable, CString opSubtype )
{
	CServiceCatalogDoc *polDoc;
	RESOURCETABLEINFO *polResInfo=0;
	CString olTable, olSubtype, olChoiceList;
	ROWCOL  ilActRow;
	ROWCOL	ilRowCount = GetRowCount();
	
	polDoc = GetOpenDocument ();
			
	if ( !polDoc )
		return;
	if ( opTable == "EQU" )
		polDoc->GetEquChoiceList ( opSubtype, olChoiceList );
	else
	{
		polResInfo = polDoc->GetResourceTableInfo ( pCHAR(opTable) );
		if ( polResInfo ) 
			olChoiceList = polResInfo->choicelist;
	}

	for ( ilActRow=2; ilActRow<=ilRowCount; ilActRow++ )
	{
		ROWCOL	ilRowCount = GetRowCount();
	
		olTable = GetTableName ( ilActRow );
		olSubtype = GetSubtype ( ilActRow );
				
		if ( olTable.IsEmpty() )
			continue;
		
		if ( (olTable==opTable) && (olSubtype==opSubtype) )
			SetStyleRange( CGXRange(ilActRow,1), 
					       CGXStyle().SetControl(GX_IDS_CTRL_TEXTFIT).
						   SetChoiceList( pCHAR(olChoiceList) ) );
	}
}
