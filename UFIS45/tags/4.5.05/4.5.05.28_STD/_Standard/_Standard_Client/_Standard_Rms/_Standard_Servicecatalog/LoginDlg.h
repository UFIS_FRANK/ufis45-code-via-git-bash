// LoginDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLoginDialog dialog

#ifndef __LOGINDLG__
#define __LOGINDLG__

//#include "cedadata.h"
//#include "PrivList.h"
#include <resource.h>
#include <CCSEdit.h>
#include <CCSGlobl.h>

#define MAX_LOGIN 3

class CLoginDialog : public CDialog 
{
// Construction
public:

	//@ManDoc: Constructor.
    CLoginDialog(const char *pcpHomeAirport, const char *pcpAppl, CWnd* pParent = NULL );     // standard constructor


	bool Login(const char *pcpUsername, const char *pcpPassword);


// Dialog Data
    //{{AFX_DATA(CLoginDialog)
	enum { IDD = IDD_LOGIN };
	CCSEdit	m_UsernameCtrl;
	CCSEdit	m_PasswordCtrl;
	CStatic	m_UsidCaption;
	CStatic	m_PassCaption;
	CButton	m_OkCaption;
	CButton	m_CancelCaption;
	//}}AFX_DATA


private:
	int	imLoginCount;
	char pcmHomeAirport[20], pcmAppl[20];

// Implementation
protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	void ErrorMessageBox ();

    // Generated message map functions
    //{{AFX_MSG(CLoginDialog)
    afx_msg void OnPaint();
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};


#endif // __LOGINDLG__
