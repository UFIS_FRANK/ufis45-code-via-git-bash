// ControlGrid.h: interface for the CControlGrid class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CONTROLGRID_H__9DAB8541_0A5E_11D6_960D_005004BCC8AF__INCLUDED_)
#define AFX_CONTROLGRID_H__9DAB8541_0A5E_11D6_960D_005004BCC8AF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <GridControl.h>

#define LOCATIONS 1
#define EQUIPMENT 2

class CControlGrid : public CTitleGridControl  
{
public:
	CControlGrid();
	virtual ~CControlGrid();
	CControlGrid( CWnd *popParent, UINT nID, CString opTitle, UINT ipResourceType );

//  Data members
private:
	CMapStringToString omResourcesList;
//  Implementation
public:
	void ResetValues ();
	void SelectResource ( CString &ropResource, int pResNo, 
						  bool bpCallControlledGrid =true ) ;
	void ModifyChoiceLists ();
	void IniResourcesList ( int ipResourceType );
protected:
	bool CreateComboBoxes ( int ipLenthOfChoice );
	BOOL OnEndEditing( ROWCOL nRow, ROWCOL nCol) ;
	bool DeleteEmptyRows ( ROWCOL nRow, ROWCOL nCol ) ;
	void DoAutoGrow ( ROWCOL nRow, ROWCOL nCol);	
	
};

#endif // !defined(AFX_CONTROLGRID_H__9DAB8541_0A5E_11D6_960D_005004BCC8AF__INCLUDED_)
