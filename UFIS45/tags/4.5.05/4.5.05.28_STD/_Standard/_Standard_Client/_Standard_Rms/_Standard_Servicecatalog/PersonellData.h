// PersonellData.h: interface for the CPersonellData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PERSONELLDATA_H__595C48C5_6C0A_11D3_936D_00001C033B5D__INCLUDED_)
#define AFX_PERSONELLDATA_H__595C48C5_6C0A_11D3_936D_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

template <class>class CCSPtrArray;

struct PERSONALRECORD
{
	CString code;
	CString urno;
	CString prio;
	UINT	number;
};

struct ASSIGNRECORD
{
	CString urno;
	PERSONALRECORD *pFunc;
	PERSONALRECORD *pQual;
};

#define TYP_FUNC 1
#define TYP_QUALI 2

class CPersonellData  
{
public:
	CPersonellData();
	virtual ~CPersonellData();
//  Implemenetation
	void Reset ();
	void AddPersonal ( UINT ipType, CString &ropCode, CString &ropUrno, 
					   CString &ropPrio, UINT ipCount=1 );
	bool DeletePersonal ( UINT ipType, CString &ropCode, UINT ipCount=1);
	bool DeletePersonal ( UINT ipType, PERSONALRECORD *prpPersonal );
	bool DeleteAllPersonal ( UINT ipType, CString &ropCode );
	UINT GetPersonalCount ( UINT ipType, CString &ropCode );
	bool ChangePersonalCount ( UINT ipType, CString &ropCode, CString &ropPrio,
							   UINT ipNewCount );
	bool ChangePersonalPrio ( UINT ipType, CString &ropCode, 
							  CString &ropNewPrio );
	bool AddAssignment ( CString &ropUrno, CString &ropFuncUrno, 
										 CString &ropQualiUrno  );
	void AddAssignment ( CString &ropUrno, PERSONALRECORD *prpFunc, 
						 PERSONALRECORD *prpQuali );
	bool DeleteAssignment ( ASSIGNRECORD *prpAssign, bool bpDelQuali=true );
	PERSONALRECORD* FindPersonalByUrno ( UINT ipType, CString &ropUrno );
	PERSONALRECORD* GetUnAssignedQuali ( CString &ropCode, bool bpCreate=true );
	bool IsPersonalAssigned ( UINT ipType, PERSONALRECORD *prpRec );
	void OnDeletePersonal ( UINT ipType, PERSONALRECORD *prpPersonal );
	void DisplayPersonal(UINT ipType);	//  MessageBox f�r Debug-Zwecke
	int GetSumOfPersonal(UINT ipType);
	int GetMaxAmountOfPersonal (UINT ipType);

protected:
	CString GetNextInternUrno ();
	
//  Data-Members
public:
	CCSPtrArray<PERSONALRECORD> omFuncs;
	CCSPtrArray<PERSONALRECORD> omQualis;
	CCSPtrArray<ASSIGNRECORD> omAssigns;
protected:
	UINT imMyInternUrno ;
};

#endif // !defined(AFX_PERSONELLDATA_H__595C48C5_6C0A_11D3_936D_00001C033B5D__INCLUDED_)
