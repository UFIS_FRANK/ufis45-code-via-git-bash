// PersonellData.cpp: implementation of the CPersonellData class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include <CCSPtrArray.h>
#include <PersonellData.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPersonellData::CPersonellData()
{
	imMyInternUrno = 0;
}

CPersonellData::~CPersonellData()
{	
	Reset ();
}

CString CPersonellData::GetNextInternUrno ()
{
	CString olUrno;
	olUrno.Format ( "-%lu", ++imMyInternUrno );
	return olUrno;
}

void CPersonellData::Reset ()
{
	omFuncs.DeleteAll();
	omQualis.DeleteAll();
	omAssigns.DeleteAll();
}

void CPersonellData::AddPersonal ( UINT ipType, CString &ropCode, 
								  CString &ropUrno, CString &ropPrio, 
								  UINT ipCount/*=1*/ )
{
	PERSONALRECORD rlRec;
	UINT		   ilBisher;
	CCSPtrArray<PERSONALRECORD> *polPersonal = (ipType==TYP_FUNC) ? &omFuncs :
																	&omQualis;
	ilBisher = GetPersonalCount ( ipType, ropCode );
	for ( UINT i=1; i<= ipCount; i++ )
	{
		rlRec.code = ropCode;
		rlRec.prio = ropPrio;
		if ( (i==1) && !ropUrno.IsEmpty() )
			rlRec.urno = ropUrno;
		else
			rlRec.urno = GetNextInternUrno ();
		rlRec.number = ilBisher + i;
		polPersonal->New(rlRec);
	}
}

bool CPersonellData::DeletePersonal ( UINT ipType, CString &ropCode, 
									  UINT ipCount/*=1*/)
{
	CCSPtrArray<PERSONALRECORD> *polPersonal = (ipType==TYP_FUNC) ? &omFuncs :
																	&omQualis;
	PERSONALRECORD *polRec;
	UINT		   ilDeleted = 0;

	for ( int i=polPersonal->GetSize()-1; (i>=0) && (ilDeleted<ipCount); i-- )
	{
		polRec = &polPersonal->ElementAt(i);
		if ( polRec && (polRec->code == ropCode ) )
		{
			OnDeletePersonal ( ipType, polRec );
			polPersonal->DeleteAt(i);
			ilDeleted++;
		}
	}
	return (ilDeleted==ipCount);
}


bool CPersonellData::DeletePersonal ( UINT ipType, PERSONALRECORD *prpPersonal )
{
	CCSPtrArray<PERSONALRECORD> *polPersonal = (ipType==TYP_FUNC) ? &omFuncs :
																	&omQualis;
	PERSONALRECORD *polRec;
	for ( int i=0; i<polPersonal->GetSize(); i++ )
	{
		polRec = &polPersonal->ElementAt(i);
		if ( polRec && (polRec == prpPersonal ) )
		{
			OnDeletePersonal ( ipType, prpPersonal );
			polPersonal->DeleteAt(i);
			return true;
		}
	}
	return false;
}

bool CPersonellData::DeleteAllPersonal ( UINT ipType, CString &ropCode )
{
	CCSPtrArray<PERSONALRECORD> *polPersonal = (ipType==TYP_FUNC) ? &omFuncs :
																	&omQualis;
	bool blRet = false;
	PERSONALRECORD *polRec;
	for ( int i=polPersonal->GetSize()-1; i>=0; i-- )
	{
		polRec = &polPersonal->ElementAt(i);
		if ( polRec && (polRec->code == ropCode ) )
		{
			OnDeletePersonal ( ipType, polRec );
			polPersonal->DeleteAt(i);
		}
		blRet = true;
	}
	return blRet;
}

UINT CPersonellData::GetPersonalCount ( UINT ipType, CString &ropCode )
{
	CCSPtrArray<PERSONALRECORD> *polPersonal = (ipType==TYP_FUNC) ? &omFuncs :
																	&omQualis;
	PERSONALRECORD *polRec;
	UINT ilRet=0;

	for ( int i=0; i<polPersonal->GetSize(); i++ )
	{
		polRec = &polPersonal->ElementAt(i);
		if ( polRec && (polRec->code == ropCode ) )
			ilRet++;
	}
	return ilRet;
}

bool CPersonellData::ChangePersonalCount ( UINT ipType, CString &ropCode, 
										   CString &ropPrio, UINT ipNewCount )
{
	UINT			ilBisher;
	int				diff;
	bool			blRet = true;

	ilBisher = GetPersonalCount ( ipType, ropCode );
	diff = ipNewCount - ilBisher;
	if ( diff > 0 )
		AddPersonal ( ipType, ropCode, CString (""), ropPrio, abs(diff) );
	else
		if ( diff < 0 )
			blRet = DeletePersonal ( ipType, ropCode, abs(diff) );
	return blRet;
}

bool CPersonellData::ChangePersonalPrio ( UINT ipType, CString &ropCode, 
										  CString &ropNewPrio  )
{
	bool	blRet = false;
	CCSPtrArray<PERSONALRECORD> *polPersonal = (ipType==TYP_FUNC) ? &omFuncs :
																	&omQualis;
	PERSONALRECORD *polRec;

	for ( int i=0; i<polPersonal->GetSize(); i++ )
	{
		polRec = &polPersonal->ElementAt(i);
		if ( polRec && (polRec->code == ropCode ) )
		{
			polRec->prio = ropNewPrio;
			blRet = true;
		}
	}
	return blRet;
}

void CPersonellData::OnDeletePersonal ( UINT ipType, PERSONALRECORD *prpPersonal )
{
	ASSIGNRECORD *polAssign; 
	//  Delete all assignments made for This function inclusive qualifications
	for ( int i=omAssigns.GetSize()-1; i>=0; i-- )
	{
		polAssign = &omAssigns[i];
		if ( polAssign )
		{
			if ( ( ipType == TYP_FUNC ) && ( polAssign->pFunc==prpPersonal ) )
				DeleteAssignment ( polAssign );
			if ( ( ipType == TYP_QUALI ) && ( polAssign->pQual==prpPersonal ) )
				DeleteAssignment ( polAssign, false );
		}
	}
}
	
bool CPersonellData::AddAssignment ( CString &ropUrno, CString &ropFuncUrno, 
									 CString &ropQualiUrno  )
{
	PERSONALRECORD *prlFunc;
	PERSONALRECORD *prlQual;
	
	prlFunc = FindPersonalByUrno ( TYP_FUNC, ropFuncUrno );
	prlQual = FindPersonalByUrno ( TYP_QUALI, ropQualiUrno );

	if ( prlFunc && prlQual )
	{
		AddAssignment ( ropUrno, prlFunc, prlQual ); 
		return true;
	}
	return false;
}

void CPersonellData::AddAssignment ( CString &ropUrno, PERSONALRECORD *prpFunc, 
									 PERSONALRECORD *prpQuali )
{
	ASSIGNRECORD   prlAssign;	

	prlAssign.pFunc = prpFunc;
	prlAssign.pQual = prpQuali;
	if ( !ropUrno.IsEmpty() )
		prlAssign.urno = ropUrno;
	else
		prlAssign.urno = GetNextInternUrno ();
	omAssigns.New(prlAssign);
}

bool CPersonellData::DeleteAssignment ( ASSIGNRECORD *prpAssign, 
									    bool bpDelQuali/*=true*/ )
{
	bool			blRet=true;
	ASSIGNRECORD	*polRec;
	if ( bpDelQuali )
		blRet = DeletePersonal ( TYP_QUALI, prpAssign->pQual );

	for ( int i=0; i<omAssigns.GetSize(); i++ )
	{
		polRec = &omAssigns[i];
		if ( polRec && (polRec == prpAssign ) )
		{
			omAssigns.DeleteAt(i);
			return blRet;
		}	
	}
	return false;
}


PERSONALRECORD* CPersonellData::FindPersonalByUrno ( UINT ipType, 
													 CString &ropUrno )
{
	CCSPtrArray<PERSONALRECORD> *polPersonal = (ipType==TYP_FUNC) ? &omFuncs :
																	&omQualis;
	PERSONALRECORD *polRec;

	for ( int i=0; i<polPersonal->GetSize(); i++ )
	{
		polRec = &polPersonal->ElementAt(i);
		if ( polRec && (polRec->urno == ropUrno ) )
			return polRec;
	}
	return 0;
}

	
PERSONALRECORD* CPersonellData::GetUnAssignedQuali ( CString &ropCode, 
													 bool bpCreate/*=true*/ )
{
	PERSONALRECORD *prlQual;
	int				ilOldCount, ilNewCount;
	ilOldCount = omQualis.GetSize();
	for ( int i=0; i<ilOldCount; i++ )
	{
		prlQual =  &omQualis[i];
		if ( prlQual && ( prlQual->code == ropCode ) &&
			 !IsPersonalAssigned ( TYP_QUALI, prlQual ) )
			 return prlQual;
	}
	if ( !bpCreate )
		return 0;

	AddPersonal ( TYP_QUALI, ropCode, CString(""), CString("") );
	ilNewCount = omQualis.GetSize();
	if ( ilNewCount > ilOldCount )
		return &(omQualis[ilNewCount-1]);
	else
		return 0;
}

bool CPersonellData::IsPersonalAssigned ( UINT ipType, PERSONALRECORD *prpRec )
{
	ASSIGNRECORD *polAssingRec;
	bool		blRet = false;
	for ( int i=0; (i<omAssigns.GetSize()) && !blRet; i++ )
	{
		polAssingRec = &omAssigns[i];
		if ( !polAssingRec )
			continue;
		if ( ipType == TYP_FUNC )
			blRet = ( prpRec == polAssingRec->pFunc );
		else
			blRet = ( prpRec == polAssingRec->pQual );
	}
	return blRet;
}

void CPersonellData::DisplayPersonal(UINT ipType)
{
	CCSPtrArray<PERSONALRECORD> *polPersonal = (ipType==TYP_FUNC) ? &omFuncs :
																	&omQualis;
	CString	olMsg;
	olMsg.Format("Size=%d\n", polPersonal->GetSize() );
	PERSONALRECORD *polRec;

	for ( int i=0; i<polPersonal->GetSize(); i++ )
	{
		polRec = &polPersonal->ElementAt(i);
		if ( polRec )
		{
			CString olTmp;
			olTmp.Format("%s  %s\n",polRec->code, polRec->urno ); 
			olMsg += olTmp;
		}
	}
	AfxMessageBox  ( olMsg );
}

int CPersonellData::GetSumOfPersonal(UINT ipType)
{
	CCSPtrArray<PERSONALRECORD> *prlPersonal = (ipType==TYP_FUNC) ? &omFuncs :
																	&omQualis;
	return prlPersonal->GetSize ();
}


int CPersonellData::GetMaxAmountOfPersonal (UINT ipType)
{
	CCSPtrArray<PERSONALRECORD> *polPersonal = (ipType==TYP_FUNC) ? &omFuncs :
																	&omQualis;
	PERSONALRECORD *prlRec;
	int ilMax=0;																		
	for ( int i=0; i<polPersonal->GetSize(); i++ )
	{
		prlRec = &polPersonal->ElementAt(i);
		if ( prlRec && !prlRec->code.IsEmpty() )
			ilMax = max ( ilMax, (int)GetPersonalCount(ipType, prlRec->code) );
	}
	return ilMax;
}


