#if !defined(AFX_SELECTSERVICE_H__775E51E5_38ED_11D3_933B_00001C033B5D__INCLUDED_)
#define AFX_SELECTSERVICE_H__775E51E5_38ED_11D3_933B_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelectService.h : header file
//

#include <CViewer.h>

class CGridControl;
class RecordSet;


#define DISPLAYED_SERVICEINFO 6

/////////////////////////////////////////////////////////////////////////////
// CSelectService dialog

class CSelectService : public CDialog
{
// Construction
public:
	CSelectService(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	CWnd	pomDlgButtons[5];
	//{{AFX_DATA(CSelectService)
	enum { IDD = IDD_SELECTSERVICE };
	CComboBox	m_ViewCB;
	//}}AFX_DATA
protected:
	char pcmTabName[11];
	RecordSet *pomSelectedService;
	int  imServiceCount;
	CGridControl *pomServiceList;
	int  imLastWidth;		//  letzte Gr��e der client area
	int  imLastHeight;		//				"

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectService)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
	public:
	int DoModal ( const char *pcpTabName, RecordSet *popSelectedService );
	CGridControl *GetServiceList ();
protected:
	void IniGrid ();
	void IniColWidths ();
	void SaveColWidths ();
	void IniStatics ();
	void PreDestroyWindow ();
	void UpdateComboBox();
	void FillGrid ();

	// Generated message map functions
	//{{AFX_MSG(CSelectService)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnDestroy();
	afx_msg void OnFilePrint();
	virtual void OnCancel();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnEditFind();
	afx_msg void OnClose();
	afx_msg void OnView();
	afx_msg void OnSelendokViewCb();
	//}}AFX_MSG
	afx_msg void OnGridLbDblClicked ( WPARAM wparam, LPARAM lparam );
	afx_msg void OnGridLbClicked( WPARAM wparam, LPARAM lparam );
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTSERVICE_H__775E51E5_38ED_11D3_933B_00001C033B5D__INCLUDED_)
