// basicdat.cpp CBasicData class for providing general used methods

#include <stdafx.h>

#include <CedaSysTabData.H>
#include <CedaBasicData.H>

#include <resource.h>
#include <utilities.h>
#include <BasicData.h>


int GetItemCount(CString olList, char cpTrenner  )
{
	CStringArray olStrArray;
	return ExtractItemList(olList,&olStrArray,cpTrenner);
}

/*
CString LoadStg(UINT nID)
{
	CString olString = "";
	olString.LoadString(nID);
	int i = olString.Find("*REM*");
	if(i>-1)
		olString = olString.Left(i);
	return olString;
}
*/
int ExtractItemList(CString opSubString, CStringArray *popStrArray, char cpTrenner)
{

	CString olText;

	popStrArray->RemoveAll();

	BOOL blEnd = FALSE;
	
	if(!opSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		while(blEnd == FALSE)
		{
			pos = opSubString.Find(cpTrenner);
			if(pos == -1)
			{
				blEnd = TRUE;
				olText = opSubString;
			}
			else
			{
				olText = opSubString.Mid(0, opSubString.Find(cpTrenner));
				opSubString = opSubString.Mid(opSubString.Find(cpTrenner)+1, opSubString.GetLength( )-opSubString.Find(cpTrenner)+1);
			}
			popStrArray->Add(olText);
		}
	}
	return	popStrArray->GetSize();

}


CString GetListItem(CString &opList, int ipPos, bool bpCut, char cpTrenner )
{
	CStringArray olStrArray;
	int ilAnz = ExtractItemList(opList, &olStrArray, cpTrenner);
	CString olReturn;

	if(ipPos == -1)
		ipPos = ilAnz;

	if((ipPos <= ilAnz) && (ipPos > 0))
		olReturn = olStrArray[ipPos - 1];
	if(bpCut)
	{
		opList = "";
		for(int ilLc = 0; ilLc < ilAnz; ilLc++)
		{
			if(ilLc != (ipPos - 1))
			{
				opList = opList + olStrArray[ilLc] + cpTrenner;
			}
		}
	}
	if(bpCut)
		opList = opList.Left(opList.GetLength() - 1);
	return olReturn;
}



CString DeleteListItem(CString &opList, CString olItem)
{
	CStringArray olStrArray;
	int ilAnz = ExtractItemList(opList, &olStrArray);
	opList = "";
	for(int ilLc = 0; ilLc < ilAnz; ilLc++)
	{
			if(olStrArray[ilLc] != olItem)
				opList = opList + olStrArray[ilLc] + ",";
	}
	opList = opList.Left(opList.GetLength() - 1);
	return opList;
}

//------------------------------------------------------------------------------------

CTime COleDateTimeToCTime(COleDateTime &opTime)
{
	CTime olTime = -1;
	if(opTime.GetStatus() == COleDateTime::valid)
	{
		olTime = CTime(opTime.GetYear(), opTime.GetMonth(), opTime.GetDay(), opTime.GetHour(), opTime.GetMinute(), opTime.GetSecond());
	}
	return olTime;
}

COleDateTime CTimeToCOleDateTime(CTime &opTime)
{
	COleDateTime olTime;
	olTime.SetStatus(COleDateTime::invalid);
	if(opTime != -1)
	{
		olTime = COleDateTime(opTime.GetYear(), opTime.GetMonth(), opTime.GetDay(), opTime.GetHour(), opTime.GetMinute(), opTime.GetSecond());
	}
	return olTime;
}


COleDateTime OneCOleDateTimeFromTwo(COleDateTime &opDate, COleDateTime &opTime)
{
	COleDateTime olTime;
	olTime.SetStatus(COleDateTime::invalid);

	if( (opDate.GetStatus() == COleDateTime::valid) &&
		(opTime.GetStatus() == COleDateTime::valid) )
	{
		olTime = COleDateTime(opDate.GetYear(), opDate.GetMonth(), opDate.GetDay(), 
							  opTime.GetHour(), opTime.GetMinute(), opTime.GetSecond());
	}
	return olTime;
}

CTime CTimeFromTwoCOleDateTime(COleDateTime &opDate, COleDateTime &opTime)
{
	CTime olTime = -1;

	if( (opDate.GetStatus() == COleDateTime::valid) &&
		(opTime.GetStatus() == COleDateTime::valid) )
	{
		olTime = CTime(opDate.GetYear(), opDate.GetMonth(), opDate.GetDay(), 
							  opTime.GetHour(), opTime.GetMinute(), opTime.GetSecond());
	}
	return olTime;
}

bool CTimeToTwoCOleDateTime( CTime &opDateTime, 
							 COleDateTime &opDate, COleDateTime &opTime )
{
	if( opDateTime != TIMENULL )
	{
		opDate.SetDate ( opDateTime.GetYear(), opDateTime.GetMonth(), 
						 opDateTime.GetDay() );
		opTime.SetTime ( opDateTime.GetHour(), opDateTime.GetMinute(), 
						 opDateTime.GetSecond() );
		return true;
	}
	else 
		return false;
}

CString CTwoOleDateTimeToDBString(COleDateTime &opDate,COleDateTime &opTime)
{
	CString erg;
	if(  ( opDate.GetStatus() == COleDateTime::valid ) &&
		 ( opTime.GetStatus() == COleDateTime::valid ) )
	{
		erg = opDate.Format("%Y%m%d");
		erg += opTime.Format("%H%M%S");
		return erg;
	}
	else
		return "              ";
}

CString COneOleDateTimeToDBString(COleDateTime &opDateTime)
{
	if( opDateTime.GetStatus() == COleDateTime::valid )
		return opDateTime.Format("%Y%m%d%H%M%S");
	else
		return "              ";
}

CBasicData::CBasicData(void)
{
	char pclTmpText[512];
	char pclConfigPath[512];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	imNextOrder = 4711;
	*pcmCedaCmd = '\0';


	omDiaStartTime = CTime::GetCurrentTime();
	omDiaStartTime -= CTimeSpan(0, 1, 0, 0);
	omDiaEndTime = omDiaStartTime + CTimeSpan(0, 6, 0, 0);
	

	// now reading the ceda commands
	char pclComandBuf[24];
	omDefaultComands.Empty();
	omActualComands.Empty();
 
    GetPrivateProfileString(pcgAppName, "EIOHDL", "LLF",
		pclTmpText, sizeof pclTmpText, pclConfigPath);
	sprintf(pclComandBuf," %3s ","LLF");
	omDefaultComands += pclComandBuf;
	sprintf(pclComandBuf," %3s ",pclTmpText);
	omActualComands += pclComandBuf;
}



CBasicData::~CBasicData(void)
{
}


long CBasicData::GetNextUrno(void)
{
	return ogBCD.GetNextUrno();
}


int CBasicData::GetNextOrderNo()
{
	imNextOrder++;

	return (imNextOrder);
}


char *CBasicData::GetCedaCommand(CString opCmdType)
{ 

	int ilIndex;

	if ((ilIndex = omDefaultComands.Find(opCmdType)) != -1)
	{
		strcpy(pcmCedaComand,omActualComands.Mid(ilIndex,3));
	}
	else
	{
		strcpy(pcmCedaComand,opCmdType);
	}
	return pcmCedaComand;
}


void CBasicData::GetDiagramStartTime(CTime &opStart, CTime &opEnd)
{
	opStart = omDiaStartTime;
	opEnd = omDiaEndTime;
	return;
}


void CBasicData::SetDiagramStartTime(CTime opDiagramStartTime, CTime opDiagramEndTime)
{
	omDiaStartTime = opDiagramStartTime;
	omDiaEndTime = opDiagramEndTime;
}


void CBasicData::SetWorkstationName(CString opWsName)
{
	omWorkstationName = CString("WKS234");//opWsName;
}


CString CBasicData::GetWorkstationName()
{
	return omWorkstationName;
}




void MakeUrnoListFromVC2 ( CString &opVc2, CStringList &list )
{
	CString olUrno;
	CString olSubString = opVc2;
	bool blEnd = false;

	list.RemoveAll ();

	if(!olSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		while(blEnd == false)
		{
			pos = olSubString.Find(';');
			if(pos == -1)
			{
				blEnd = true;
				olUrno = olSubString;
			}
			else
			{
				olUrno = olSubString.Mid(0, olSubString.Find(';'));
				olSubString = olSubString.Mid(olSubString.Find(';')+1, olSubString.GetLength( )-olSubString.Find(';')+1);
			}
			list.AddTail ( olUrno );
		}
	}
}


bool DoesTableExist ( char *table )
{
	CedaSysTabData olSystab;	
	char pclSelection[81];

	sprintf(pclSelection, "WHERE TANA='%s'", table );
	olSystab.SetTableExtension(pcgTableExt);

	return olSystab.Read(pclSelection, true, "FINA,TYPE,FELE,FITY,SYST");
}

bool SaveServiceValidity ( CString &ropServiceUrno, CString opVafr, 
						   CString opVato )
{
	RecordSet olValRecord(ogBCD.GetFieldCount("VAL") );
	CString   olVafr, olVato, olValUrno;
	bool      blRet;

	if ( ogBCD.GetRecord( "VAL", "UVAL", ropServiceUrno, olValRecord ) )
	{	//  Record f�r diesen Service existiert bereits
		olVafr = olValRecord.Values[ogBCD.GetFieldIndex("VAL","VAFR")];
		olVato = olValRecord.Values[ogBCD.GetFieldIndex("VAL","VATO")];
		//  Hat sich etwas ge�ndert ?
		if ( ( olVafr == opVafr ) && ( olVato == opVato ) )
			return true;  //  gleicher Zeitraum, fertig
		olValUrno = olValRecord.Values[ogBCD.GetFieldIndex("VAL","URNO")] ;
	}
	else
	{	//  neuer Record
		olValRecord.Values[ogBCD.GetFieldIndex("VAL","TABN")] = "SER";
		olValRecord.Values[ogBCD.GetFieldIndex("VAL","UVAL")] = ropServiceUrno;
	}
	olValRecord.Values[ogBCD.GetFieldIndex("VAL","VAFR")] = opVafr;
	olValRecord.Values[ogBCD.GetFieldIndex("VAL","VATO")] = opVato;
	olValRecord.Values[ogBCD.GetFieldIndex("VAL","FREQ")] = "1111111";	
	olValRecord.Values[ogBCD.GetFieldIndex("VAL","APPL")] = "SERVICE";				
	if ( olValUrno.IsEmpty() )		//  neuer Record
		blRet = ogBCD.InsertRecord("VAL", olValRecord, true );
	else							//  ge�nderter Record
		blRet = ogBCD.SetRecord("VAL", "URNO", olValUrno, olValRecord.Values, true );
	if ( !blRet )
	{
		CString olErrorTxt = GetString(IDS_WRITE_TABLE_ERR);
		FileErrMsg ( 0, pCHAR(olErrorTxt), "VAL", "<SaveServiceValidity>",
					 MB_ICONEXCLAMATION|MB_OK);
	}
	return blRet;
}

bool LoadServiceValidity ( CString &ropServiceUrno, CString &ropVafr,
						   CString &ropVato )
{
	RecordSet olValRecord(ogBCD.GetFieldCount("VAL") );

	if ( !ogBCD.GetRecord( "VAL", "UVAL", ropServiceUrno, olValRecord ) )
		return false;
	ropVafr = olValRecord.Values[ogBCD.GetFieldIndex("VAL","VAFR")];
	ropVato = olValRecord.Values[ogBCD.GetFieldIndex("VAL","VATO")];
	return true;
}	
	
bool DeleteServiceValidity ( CString &ropServiceUrno )
{
	RecordSet olValRecord(ogBCD.GetFieldCount("VAL") );
	bool blRet;

	if ( !ogBCD.GetRecord( "VAL", "UVAL", ropServiceUrno, olValRecord ) )
		return true;
	
	blRet = ogBCD.DeleteRecord("VAL", "UVAL", ropServiceUrno, true );
	if ( !blRet )
	{
		CString olErrorTxt = GetString(IDS_WRITE_TABLE_ERR);
		FileErrMsg ( 0, pCHAR(olErrorTxt), "VAL", "<DeleteServiceValidity>",
					 MB_ICONEXCLAMATION|MB_OK);
	}
	return blRet;
}	
