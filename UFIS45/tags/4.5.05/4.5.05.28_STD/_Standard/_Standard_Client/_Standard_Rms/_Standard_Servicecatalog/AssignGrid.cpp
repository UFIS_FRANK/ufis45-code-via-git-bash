// AssignGrid.cpp: implementation of the CAssignGrid class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include <CCSGlobl.h>
#include <CedaBasicData.h>

#include <ServiceCatalog.h>
#include <ServiceCatalogDoc.h>
#include <utilities.h>
#include <AssignGrid.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAssignGrid::CAssignGrid(CWnd *popParent, UINT nID, char * pcpTableName, 
						 CServiceCatalogDoc *popDoc )
	:CTitleGridControl ( popParent, nID, 2, 1 )
{
	RESOURCETABLEINFO *polResInfo=0;
	ASSERT ( popDoc );
	
	IniLayOut ();
	if ( polResInfo = popDoc->GetResourceTableInfo ( pcpTableName ) )
	{
	    omTableName = polResInfo->table;
		omCodeField = polResInfo->codefield;
		omDisplayField = polResInfo->dspfield;
		CreateComboBoxes ( polResInfo->choicelist);
	}
	else
		CreateComboBoxes ();

}


CAssignGrid::~CAssignGrid()
{

}

void CAssignGrid::IniLayOut ()
{
	ROWCOL	ilRowCount = GetRowCount();

	SetValue ( 0, 1, (CString)"Code" );
	SetValue ( 0, 2, (CString)"No." );
	SetStyleRange(CGXRange(2,2),CGXStyle()
          .SetMaxLength(2)
		  .SetUserAttribute(GX_IDS_UA_VALID_MIN, "1")
		  .SetUserAttribute(GX_IDS_UA_VALID_MAX, "1")
		  .SetValueType (GX_VT_NUMERIC)
		  .SetFormat(GX_FMT_FIXED).SetPlaces(0).SetReadOnly(TRUE) );
	EnableGridToolTips(TRUE);

	SetColWidth(0, 0, 20);
	SetColWidth(1, 1, 140);
	SetColWidth(2, 2, 50);

}

bool CAssignGrid::CreateComboBoxes ()
{
	char *pclChoiceList=0;
	int  ilLen, ilBuffLen, ilDBRecords;
	CString olCode; 
	ROWCOL	ilRowCount = GetRowCount();
	
	ilDBRecords = ogBCD.GetDataCount(omTableName);
	ilLen = ogBCD.GetMaxFieldLength ( omTableName, omCodeField ) ;
	ilBuffLen = (ilLen+1)*ilDBRecords  + 1;
	pclChoiceList = new char[ilBuffLen];
	pclChoiceList[0] = '\0';
	if ( !pclChoiceList )
	{
		MessageBox ( "Allocation failure in >CServiceGrid::CreateComboBoxes<", 
					 "", MB_ICONEXCLAMATION );
		return false;
	}

	for ( int i=0; i<ilDBRecords ; i++ )
	{
		olCode = ogBCD.GetField ( omTableName, i, omCodeField );
		strcat ( pclChoiceList, (const char*)olCode );
		strcat ( pclChoiceList, "\n" );
	}

	SetStyleRange( CGXRange(2,1,ilRowCount,1), 
				   CGXStyle().SetControl(GX_IDS_CTRL_TEXTFIT).SetValue("")
				   .SetChoiceList(pclChoiceList) );
	delete pclChoiceList;

	return true;
}

bool CAssignGrid::CreateComboBoxes ( CString &ropChoiceList )
{
	ROWCOL	ilRowCount = GetRowCount();

	SetStyleRange( CGXRange(2,1,ilRowCount,1), 
				   CGXStyle().SetControl(GX_IDS_CTRL_TEXTFIT).SetValue("")
				   .SetChoiceList( pCHAR(ropChoiceList) ) );
	return true;
}


void CAssignGrid::DisplayInfo ( ROWCOL nRow, ROWCOL nCol )
{
	CString olWert, olAnzeige;
	
	olWert = GetValueRowCol ( nRow, nCol );
	if ( !olWert.IsEmpty() && olWert[0]!='*' )
		olAnzeige = ogBCD.GetField( omTableName, omCodeField, olWert, omDisplayField );
	SetStyleRange(CGXRange(nRow, nCol), CGXStyle()
				.SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, olAnzeige ));
	olAnzeige.Empty ();
	SetStatusBarText ( olAnzeige );
}

BOOL CAssignGrid::OnEndEditing( ROWCOL nRow, ROWCOL nCol) 
{
	BOOL ilRet = CTitleGridControl::OnEndEditing( nRow, nCol) ;

	//  Der Rest wird nur gemacht, 
	//  wenn die Spalte mit den Resourcen (d.h. Comboboxen) editiert worden ist
	if (nRow>=2 )
	{
		CGXStyle olStyle = LookupStyleRowCol ( nRow, nCol );
		SetDefaultValues ( nRow );
		DisplayInfo ( nRow, nCol );
		DeleteEmptyRows ( nRow, nCol ) ;
	}
	return ilRet;
}

void CAssignGrid::DoAutoGrow ( ROWCOL nRow, ROWCOL nCol)
{
	//  nur bei Eingabe in der ersten Spalte und letzter Zeile wird die
	//	Tabelle automatisch erweitert
	if ( nCol == 1 )		
	{
		CTitleGridControl::DoAutoGrow ( nRow, nCol );	
		ROWCOL ilRowCount = GetRowCount ();
		//  wurde wirklich eine Zeile eingef�gt
		if ( ilRowCount > nRow )				//  damit der von oben kopierte 
			DisplayInfo ( ilRowCount, nCol );	//  Tooltip zur�ckgesetzt wird
	}
}


void CAssignGrid::OnModifyCell(ROWCOL nRow, ROWCOL nCol)
{
	CTitleGridControl::OnModifyCell(nRow, nCol);

	if ( ( nCol != 1 ) || ( nRow<2 ) )
		return;
   
   	CString olWert, olAnzeige;
	CGXControl* pControl = GetControl(nRow, nCol);
	if ( !pControl->GetValue(olWert) )
		return;
	if ( !olWert.IsEmpty() )
		olAnzeige = ogBCD.GetField( omTableName, omCodeField, olWert, omDisplayField );
	SetStatusBarText ( olAnzeige );
}


//  DeleteEmptyRows:  
//	Checkt, ob �bergebene Zelle in der Spalte der Resourcen liegt und ,wenn ja,
//	ob diese Zelle ohne Inhalt verlassen worden ist
//  In:  nRow, nCol:  Zelle, die modifiziert worden ist
//	return:  true, wenn Zeile gel�scht wurde
//			 false sonst
bool CAssignGrid::DeleteEmptyRows ( ROWCOL nRow, ROWCOL nCol ) 
{
	ROWCOL  ilRowCount = GetRowCount();
	//  Wenn nicht in der ersten Spalte gel�scht bzw. editiert worden ist
	//  gibt es keine Zeile zu l�schen
	if ( nCol != 1 )
		return false;
	if ( ( nRow>=2 ) && ( nRow<ilRowCount ) &&
		  GetValueRowCol ( nRow, nCol ).IsEmpty() )
	{
		//RemoveRows( nRow, nRow );
		//SetRowHeadersText ();
		RemoveOneRow( nRow );
		return true;
	}
	else 
		return false;
}

void CAssignGrid::ResetValues ()
{
	ROWCOL ilRowCount = GetRowCount();
	SetValueRange ( CGXRange(2,1,ilRowCount,2), "" );
	if ( ilRowCount >= 3 )
		RemoveRows( 3, ilRowCount );
	SetDirtyFlag ( false );
}


bool CAssignGrid::SetLineValue ( int ipResNo, CString &ropCode )
{
	int			index;
	CGXComboBox	*polCB;
	CString		olFound, olChoiceList;
	CGXStyle	olGXStyle;
	int			ilActRow = ipResNo + 2;

	polCB = (CGXComboBox*)GetControl ( ilActRow, 1 );
	ComposeStyleRowCol ( ilActRow, 1, &olGXStyle );
	if ( polCB && olGXStyle.GetIncludeChoiceList() ) 
	{
		olChoiceList = olGXStyle.GetChoiceList();
		index = polCB->FindStringInChoiceList( olFound, pCHAR(ropCode), 
											   pCHAR(olChoiceList), TRUE );
		if ( index>=0 ) 
		{
			SetValueRange ( CGXRange(ilActRow,1), ropCode );
			OnEndEditing ( ilActRow, 1 );
			return true;
		}
	}
	return false;
}


void CAssignGrid::SetDefaultValues ( ROWCOL nRow )
{
	if ( nRow < 2 )
		return;
	CString olResource = GetValueRowCol ( nRow, 1 );
	//  Default-Wert f�r Anzahl '1', wenn noch kein Wert gesetzt ist
	CString olAnzahl = GetValueRowCol ( nRow, 2 );
	if ( !olResource.IsEmpty () && olAnzahl.IsEmpty() )
		SetValue ( nRow-1, 2, (CString)"1" );	
}



//////////////////////////////////////////////////////////////////////
// CAssignFuncGrid Class
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAssignFuncGrid::CAssignFuncGrid(CWnd *popParent, UINT nID, char * pcpTableName, 
								 CServiceCatalogDoc *popDoc, 
								 CAssignQualiGrid *pQualiGrid )
	:CAssignGrid( popParent, nID, pcpTableName, popDoc )
{
	SetTitle ( CString("Einzelfunktionen") );
	GetParam()->EnableSelection	(GX_SELROW );
	prmZuordnung = 0;
	pomQualiGrid = pQualiGrid;
}

CAssignFuncGrid::~CAssignFuncGrid()
{

}

void CAssignFuncGrid::SetAssignment( CPtrArray *prpZuordnung )
{
	QUALIZUORDNUNG *polAss;
	int	ilDisplayed=0;
	ResetValues ();
	prmZuordnung = prpZuordnung;
	for ( int i=0; i<prmZuordnung->GetSize(); i++ )
	{
		polAss = (QUALIZUORDNUNG*)prmZuordnung->GetAt(i); 
		if ( polAss && SetLineValue ( ilDisplayed, polAss->func ) )
			ilDisplayed++;
	}
}

void CAssignFuncGrid::OnMovedCurrentCell(ROWCOL nRow, ROWCOL nCol)
{
	ROWCOL ilLastRow, ilLastCol;
	BOOL   blOk;
	blOk = GetCurrentCell ( ilLastRow, ilLastCol );
}


//////////////////////////////////////////////////////////////////////
// CAssignQualiGrid Class
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAssignQualiGrid::CAssignQualiGrid(CWnd *popParent, UINT nID, char * pcpTableName, 
								   CServiceCatalogDoc *popDoc )
	:CAssignGrid( popParent, nID, pcpTableName, popDoc )
{
	SetTitle ( CString("Einzelqualifikationen") );
}

CAssignQualiGrid::~CAssignQualiGrid()
{

}

void CAssignQualiGrid::SetSelection ( CStringArray &ropSelection )
{
	int	ilDisplayed=0;
	ResetValues ();
	for ( int i=0; i<ropSelection.GetSize(); i++ )
		if ( SetLineValue ( ilDisplayed, ropSelection[i] ) )
			ilDisplayed++;
}

void CAssignQualiGrid::GetSelection ( CStringArray &ropSelection )
{
	ropSelection.RemoveAll();
	for ( UINT i=2; i<=GetRowCount(); i++ )
	{
		CString olWert = GetValueRowCol ( i, 1 );
		if ( !olWert.IsEmpty() )
			ropSelection.Add ( olWert );
	}
}