#if !defined (UTILITIES_H)
	#define UTILITIES_H


template <class>class CCSPtrArray;
class RecordSet;


void GetMapForRecordList ( CCSPtrArray<RecordSet> &ropRecordList, 
						   CMapStringToPtr &ropRecordMap, int ipRefFieldIndex );

bool GetPrivProfInt( LPCTSTR pcpSection, LPCTSTR pcpEntry, int ipDefault, 
					 int &ripValue, LPCTSTR pcpFileName );
void WriPrivProfInt( LPCTSTR pcpSection, LPCTSTR pcpEntry, int ipValue, 
					 LPCTSTR pcpFileName );
bool GetPrivProfString ( LPCTSTR pcpSection, LPCTSTR pcpEntry, LPCTSTR pcpDefault, 
					     LPTSTR pcpValue, DWORD ipSize, LPCTSTR pcpFileName );
int FileErrMsg ( HWND hwnd, char *lpText, char *filna, char* lpCaption=0, 
				 UINT uType=MB_OK );
int FileErrMsg ( HWND hwnd, UINT idcText, char *filna, UINT idcCaption=0, 
				 UINT uType=MB_OK );
int LangMsgBox ( HWND hwnd, UINT idcText, UINT idcCaption=0, UINT uType=MB_OK );
void SetStatusBarText ( CString &ropText );
bool SaveWindowPosition ( CWnd *popWnd, char *pcpIniEntry );
bool IniWindowPosition ( CWnd *popWnd, char *pcpIniEntry );
int FindStringInside ( CString opWhole, CString opSub, 
					   BOOL bpCaseSensitiv=TRUE );
BOOL EnableDlgItem ( CWnd *popWnd, UINT idc, BOOL enable=TRUE );
CString GetEquipmentType ( CString &ropResCode );

#endif
