// ServiceCatalogDoc.cpp : implementation of the CServiceCatalogDoc class
//
#include <stdafx.h>

#include <cedabasicdata.h>

#include <ccsglobl.h>
#include <basicdata.h>
#include <initialloaddlg.h>
#include <GridRecordXChange.h>
#include <ServiceCatalog.h>

#include <ServiceCatalogDoc.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

char* pgResourceTables[RESORUCETABLEANZ] = 
				{ "PFC",  "PER",  "EQU",  "BLT",  "WRO",  "GAT",  
				  "PST",  "CIC",  "EXT" };
char* pgResourceCode[RESORUCETABLEANZ] = 
				{ "FCTC", "PRMC", "GCDE", "BNAM", "WNAM", "GNAM",
				"PNAM", "CNAM", "ENAM" };
char* pgResourceDisplay[RESORUCETABLEANZ] = 
				{ "FCTN", "PRMN", "ENAM", "",	  "",	  "",
				  "", "", "" };
UINT pgResourceIDS[RESORUCETABLEANZ] =
				{ IDS_PFC_HEADER, IDS_PER_HEADER, IDS_EQU_HEADER, 
				  IDS_BLT_HEADER, IDS_WRO_HEADER, IDS_GAT_HEADER, 
				  IDS_PST_HEADER, IDS_CIC_HEADER, IDS_EXT_HEADER };

/////////////////////////////////////////////////////////////////////////////
// CServiceCatalogDoc

IMPLEMENT_DYNCREATE(CServiceCatalogDoc, CDocument)

BEGIN_MESSAGE_MAP(CServiceCatalogDoc, CDocument)
	//{{AFX_MSG_MAP(CServiceCatalogDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CServiceCatalogDoc construction/destruction

CServiceCatalogDoc::CServiceCatalogDoc()
{
	// TODO: add one-time construction code here
	ogBCD.SetTableExtension(pcgTableExt);

	imResTableAnz = FillResourceTableInfos ();

	if ( DoesTableExist("EQT") && ogBCD.SetObject ( "EQT" ) )
	{
		ogBCD.Read ( "EQT" );
		ogBCD.SetSort("EQT", "NAME+", true );
	}

	// for reference check only
	ogBCD.SetObject("RUD");
}

CServiceCatalogDoc::~CServiceCatalogDoc()
{
	delete [] psmUsedResourceTables;
	omResourceTableList.RemoveAll();
	omEquChoiceLists.RemoveAll ();
	omEquTableTypeStrings.RemoveAll ();
}

BOOL CServiceCatalogDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)
	if ( !theApp.bmLoginByCommandLine )
		LoadCedaTable ();
	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CServiceCatalogDoc serialization

void CServiceCatalogDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CServiceCatalogDoc diagnostics

#ifdef _DEBUG
void CServiceCatalogDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CServiceCatalogDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CServiceCatalogDoc commands


bool CServiceCatalogDoc::LoadCedaTable ()
{
	CWaitCursor olWait;
	int		ilDataCount =0, ilAnz ;
	CInitialLoadDlg *polLoadDialog = 0;
	CString	olMsgStr, olSort;
	CString	olLoadOneStr = GetString ( IDS_LOADING_EINZAHL );
	CString	olLoadMultipleStr = GetString (	IDS_LOADING_MEHRZAHL );
	CString olFieldList;
	bool	blRet = true;
	int		ilDdxNr = 0;

	
	int step = 100 / ( imResTableAnz + 9 );
	// *** Load Data from Ceda
    //  nun im Konstruktor: ogBCD.SetTableExtension(pcgTableExt);

	if ( !theApp.bmLoginByCommandLine )
		polLoadDialog = new CInitialLoadDlg(CWnd::GetActiveWindow());

	// *** TABLE INFO
	if ( polLoadDialog )
	{
		olMsgStr = GetString(IDS_SERVICES)+olLoadMultipleStr;
		polLoadDialog->SetMessage(olMsgStr);
	}
	
	if ( ogBCD.SetObject(pcgTableName) )
	{
		ogBCD.Read( CString(pcgTableName) );
		ilDataCount = ogBCD.GetDataCount(pcgTableName);
	}
	else
		blRet = false;
			
	if ( polLoadDialog )
		polLoadDialog->SetProgress(step);

	//  neue Tabellen SERTAB, SEFTAB, SEQTAB,SEGTAB, SEETAB, SELTAB laden
	
	//  SEFTAB
	if ( polLoadDialog )
	{
		olMsgStr = GetString(IDS_SEF_TABLE_TXT)+olLoadOneStr;
		polLoadDialog->SetMessage(olMsgStr);
	}
	if ( ogBCD.SetObject("SEF") )
	{
		ogBCD.Read( CString("SEF") );
		ilDataCount = ogBCD.GetDataCount("SEF");
	}
	else
		blRet = false;
			
	if ( polLoadDialog )
		polLoadDialog->SetProgress(step);
	
	//  SEETAB
	if ( polLoadDialog )
	{
		olMsgStr = GetString(IDS_SEE_TABLE_TXT)+olLoadOneStr;
		polLoadDialog->SetMessage(olMsgStr);
	}
	if ( ogBCD.SetObject("SEE") )
	{
		ogBCD.Read( CString("SEE") );
		ilDataCount = ogBCD.GetDataCount("SEE");
	}
	else
		blRet = false;

	if ( polLoadDialog )
		polLoadDialog->SetProgress(step);
	
	//  SEQTAB
	if ( polLoadDialog )
	{
		olMsgStr = GetString(IDS_SEQ_TABLE_TXT)+olLoadOneStr;
		polLoadDialog->SetMessage(olMsgStr);
	}
	if ( ogBCD.SetObject("SEQ") )
	{
		ogBCD.Read( CString("SEQ") );
		ilDataCount = ogBCD.GetDataCount("SEQ");
	}
	else
		blRet = false;

	if ( polLoadDialog )
		polLoadDialog->SetProgress(step);
	
	//  SELTAB
	if ( polLoadDialog )
	{
		olMsgStr = GetString(IDS_SEL_TABLE_TXT)+olLoadOneStr;
		polLoadDialog->SetMessage(olMsgStr);
	}
	if ( ogBCD.SetObject("SEL") )
	{
		ogBCD.Read( CString("SEL") );
		ilDataCount = ogBCD.GetDataCount("SEL");
	}
	else
		blRet = false;

	if ( polLoadDialog )
		polLoadDialog->SetProgress(step);
	
	//  SFQTAB
	if ( polLoadDialog )
	{
		olMsgStr = GetString(IDS_SFQ_TABLE_TXT)+olLoadMultipleStr;
		polLoadDialog->SetMessage(olMsgStr);
	}
	if ( ogBCD.SetObject("SFQ") )
	{
		ogBCD.Read( CString("SFQ") );
		ilDataCount = ogBCD.GetDataCount("SFQ");
	}
	else
		blRet = false;

	if ( polLoadDialog )
		polLoadDialog->SetProgress(step);
	
	for ( UINT i = 0; i<imResTableAnz; i++ )
	{
		if ( polLoadDialog )
		{
			olMsgStr = psmUsedResourceTables[i].resourcetyp+olLoadMultipleStr;
			polLoadDialog->SetMessage( olMsgStr );
		}
		olFieldList = "URNO,";
		olFieldList += psmUsedResourceTables[i].codefield;
		if ( ! psmUsedResourceTables[i].dspfield.IsEmpty() )
		{
			olFieldList += ",";
			olFieldList += psmUsedResourceTables[i].dspfield;
		}
		/*  bugfix for PRF3435
			if this is required more often a new global Array 
			pgResourceAddfields for additional fields should be introduced */
		if ( !strcmp(psmUsedResourceTables[i].table, "EQU" ) )
			olFieldList += ",GKEY";
		if ( ogBCD.SetObject(psmUsedResourceTables[i].table, olFieldList ) )
		{
			ogBCD.Read( CString(psmUsedResourceTables[i].table) );
			olSort = psmUsedResourceTables[i].codefield ;
			olSort += "+";
			ogBCD.SetSort(psmUsedResourceTables[i].table, olSort, true );
			ilAnz = ogBCD.GetDataCount(psmUsedResourceTables[i].table);
			psmUsedResourceTables[i].initialized = true ;

			ilDdxNr = BC_RES1_IRT + 3*i;
			ogBCD.SetDdxType(psmUsedResourceTables[i].table, "IRT", ilDdxNr );
			ogBCD.SetDdxType(psmUsedResourceTables[i].table, "URT", ilDdxNr+URT_OFFSET );
			ogBCD.SetDdxType(psmUsedResourceTables[i].table, "DRT", ilDdxNr+DRT_OFFSET );
			omBCTables.SetAtGrow ( ilDdxNr, psmUsedResourceTables[i].table );
			omBCTables.SetAtGrow ( ilDdxNr+URT_OFFSET, psmUsedResourceTables[i].table);
			omBCTables.SetAtGrow ( ilDdxNr+DRT_OFFSET, psmUsedResourceTables[i].table);
			TRACE ( "DDX IRT <%s>: %d\n", psmUsedResourceTables[i].table, ilDdxNr ); 
		}
		else
		{
			blRet = false;
			psmUsedResourceTables[i].initialized = false;
		}
		if ( polLoadDialog )
			polLoadDialog->SetProgress(step);
	}	

	if ( polLoadDialog )
	{
		olMsgStr = GetString(IDS_STATIC_GROUPS)+olLoadMultipleStr;
	 	polLoadDialog->SetMessage(olMsgStr);
	}
	//   SGR
	//ogBCD.SetObject("SGR", "URNO,GRPN,GRDS,APPL" );
	if ( ogBCD.SetObject("SGR") )
	{
		ogBCD.Read( "SGR","WHERE APPL='RULE_AFT'" );
		olSort = "GRPN+";
		ogBCD.SetSort("SGR", olSort, true );
		ilAnz = ogBCD.GetDataCount("SGR");

		ogBCD.SetDdxType("SGR", "IRT", BC_SGR_IRT );
		ogBCD.SetDdxType("SGR", "URT", BC_SGR_URT );
		ogBCD.SetDdxType("SGR", "DRT", BC_SGR_DRT );
		omBCTables.SetAtGrow ( BC_SGR_IRT, "SGR" );
		omBCTables.SetAtGrow ( BC_SGR_URT, "SGR" );
		omBCTables.SetAtGrow ( BC_SGR_DRT, "SGR" );
		TRACE ( "DDX IRT <SGR>: %d\n", BC_SGR_IRT ); 

	}
	else
		blRet = false;

	//   SGM
	if ( NESTED_GROUPS )
	{
		if ( ogBCD.SetObject("SGM", "URNO,TABN,USGR,SURN" ) )
		{
			ogBCD.Read( "SGM", "where USGR in (select urno from SGRTAB where APPL='RULE_AFT')" );
			ilAnz = ogBCD.GetDataCount("SGM");
		}
		else
			blRet = false;

	}
	if ( polLoadDialog )
		polLoadDialog->SetProgress(step);
	
	FillChoiceLists ();

	if ( polLoadDialog )
	{
		olMsgStr = GetString(IDS_VALIDITY_TXT)+olLoadOneStr;
		polLoadDialog->SetMessage(olMsgStr);
	}

	//   VAL:  Güligkeiten
	if ( ogBCD.SetObject("VAL" ) )
	{
		ogBCD.Read( "VAL", "WHERE TABN = 'SER\'" );
		ilAnz = ogBCD.GetDataCount("VAL");
	}
	else
		blRet = false;

	if ( polLoadDialog )
		polLoadDialog->SetProgress(step);

	if ( polLoadDialog )
		polLoadDialog->DestroyWindow ();
	return blRet;
}

RESOURCETABLEINFO* CServiceCatalogDoc::GetResourceTableInfo ( char *tablename )
{
	CObject *polObj;
	if ( omResourceTableList.Lookup	( tablename, polObj ) )
		return (RESOURCETABLEINFO*)polObj;
	else
		return 0;
}

int CServiceCatalogDoc::FillResourceTableInfos ()
{
	CCSPtrArray<RecordSet>  olLocations;
	int ilAnzInDB=0, ilAlodInd, ilReftInd, ilAnzTotal=0;
	int ilFailed = 0;
	CString olAlod, olReft;
	CObject *polObj;
	
	bool blALODa = DoesTableExist ( "ALO" );
	if ( blALODa )
	{
		// *** Load Data from Ceda
		ogBCD.SetObject ( "ALO", "ALOD,REFT" );
		ilAnzInDB = ogBCD.ReadSpecial( "ALO", "ALOD,REFT", "WHERE ALOT='0' ORDER BY ALOD", olLocations );
	}
	if ( ilAnzInDB > 0 )
		ilAnzTotal = STARTINDLOCATIONS + ilAnzInDB;
	else
		ilAnzTotal = RESORUCETABLEANZ;

	psmUsedResourceTables = new RESOURCETABLEINFO[ilAnzTotal] ;
	for ( int i=0; i<STARTINDLOCATIONS; i++ )
	{
		psmUsedResourceTables[i].table = pgResourceTables[i];
		psmUsedResourceTables[i].codefield = pgResourceCode[i];
		psmUsedResourceTables[i].dspfield = pgResourceDisplay[i];
		psmUsedResourceTables[i].resourcetyp = GetString(pgResourceIDS[i]);
		psmUsedResourceTables[i].initialized = false;
		omResourceTableList.SetAt ( pgResourceTables[i], 
									(CObject*)&(psmUsedResourceTables[i]) );
	}
	if ( ilAnzInDB > 0 )
	{
		//ilAlodInd = ogBCD.GetFieldIndex("ALO","ALOD");
		//ilReftInd = ogBCD.GetFieldIndex("ALO","REFT");
		ilAlodInd = 0;
		ilReftInd = 1;
		for ( int i=STARTINDLOCATIONS; i<ilAnzTotal; i++ )
		{
			olAlod = olLocations[i-STARTINDLOCATIONS].Values[ilAlodInd];
			olReft = olLocations[i-STARTINDLOCATIONS].Values[ilReftInd];
			if ( olReft.Find ( "ACR.", 0 ) >= 0 )
			{
				ilFailed++;
				continue;	//  ACRTAB ignorieren, da Registrations keine Locations fuer Services sind
			}
			if ( !CLocationGridRecordXChange::ParseResCodeFieldEntry ( olReft, psmUsedResourceTables[i-ilFailed].table, 
																 psmUsedResourceTables[i-ilFailed].codefield ) )
			{
				ilFailed++;
				continue;	//  wenn Reft-Feld nicht sinnvoll, diesen ALOTAB-Eintrag ignorieren
			}
			if ( omResourceTableList.Lookup ( psmUsedResourceTables[i-ilFailed].table, 
											  polObj ) )
			{	//  table is already in list
				ilFailed++;
				continue;	
			}
			psmUsedResourceTables[i-ilFailed].dspfield = "";
			psmUsedResourceTables[i-ilFailed].resourcetyp = olAlod;
			psmUsedResourceTables[i-ilFailed].initialized = false;
			omResourceTableList.SetAt ( psmUsedResourceTables[i-ilFailed].table, 
										(CObject*)&(psmUsedResourceTables[i-ilFailed]) );
		}
		olLocations.DeleteAll();
	}
	else
		for ( int i=STARTINDLOCATIONS; i<ilAnzTotal; i++ )
		{
			psmUsedResourceTables[i].table = pgResourceTables[i];
			psmUsedResourceTables[i].codefield = pgResourceCode[i];
			psmUsedResourceTables[i].dspfield = pgResourceDisplay[i];
			psmUsedResourceTables[i].resourcetyp = GetString(pgResourceIDS[i]);
			psmUsedResourceTables[i].initialized = false;
			omResourceTableList.SetAt ( pgResourceTables[i], 
										(CObject*)&(psmUsedResourceTables[i]) );
		}
	if ( blALODa )
		ogBCD.RemoveObject("ALO");		//  wird nicht mehr gebraucht
	
	return ilAnzTotal-ilFailed;
}

void CServiceCatalogDoc::SetTitle(LPCTSTR lpszTitle) 
{
	// TODO: Add your specialized code here and/or call the base class
	CDocument::SetTitle(lpszTitle);
	if ( theApp.m_pMainWnd )
		theApp.m_pMainWnd->SetWindowText ( ogAppName );
}


void CServiceCatalogDoc::FillChoiceLists ()
{
	UINT	i;

	for ( i=0; i<imResTableAnz; i++ )
	{
		SetResourceChoiceList ( i );
	}
}


void CServiceCatalogDoc::SetResourceChoiceList ( int ipIdx )
{
	CStringList	olGruppen;
	int			ilGrpAnz, ilDBRecords, j;
	CString		olCode, olUrno;
	POSITION	slPos;

	psmUsedResourceTables[ipIdx].choicelist = "";
	ilDBRecords = ogBCD.GetDataCount(psmUsedResourceTables[ipIdx].table);
	for ( j=0; j<ilDBRecords; j++ )
	{
		olCode = ogBCD.GetField ( psmUsedResourceTables[ipIdx].table, j, 
								  psmUsedResourceTables[ipIdx].codefield );
		psmUsedResourceTables[ipIdx].choicelist += olCode;
		psmUsedResourceTables[ipIdx].choicelist += "\n" ;
	}
	psmUsedResourceTables[ipIdx].singlevalues = ilDBRecords;
	
	//  Gruppen, die diese Resource enthalten
	ilGrpAnz = FindGroupsWithResourceType ( psmUsedResourceTables[ipIdx].table, olGruppen );
	slPos = olGruppen.GetHeadPosition ();
	while ( slPos )
	{
		olUrno = olGruppen.GetNext ( slPos );
		olCode = ogBCD.GetField ( "SGR", "URNO", olUrno, "GRPN" );
		psmUsedResourceTables[ipIdx].choicelist += "*";		//  Kennzeichnung von Gruppen
		psmUsedResourceTables[ipIdx].choicelist += olCode;
		psmUsedResourceTables[ipIdx].choicelist += "\n" ;
	}
	olGruppen.RemoveAll ();
	psmUsedResourceTables[ipIdx].all = ilDBRecords +ilGrpAnz;
}

void CServiceCatalogDoc::SetEquChoiceList ( CString &ropType )
{
	int			ilDBRecords, j;
	CString		olCode, olChoiceList, olGridLineInfo;
	CCSPtrArray<RecordSet> olRecords;
	int			ilEquCodeIdx = ogBCD.GetFieldIndex ( "EQU", "GCDE" );
	int			ilSgrCodeIdx = ogBCD.GetFieldIndex ( "SGR", "GRPN" );
	
	if ( (ilEquCodeIdx>=0) && (ilSgrCodeIdx>=0) )
	{
		ogBCD.GetRecords ( "EQU", "GKEY", ropType, &olRecords );

		ilDBRecords = olRecords.GetSize(); 
		for ( j=ilDBRecords-1; j>=0; j-- )
		{
			olCode = olRecords[j].Values[ilEquCodeIdx];
			olChoiceList += olCode;
			olChoiceList += "\n" ;
		}
		olRecords.DeleteAll ();	
		
		//  Gruppen, die diese Resource enthalten
		ogBCD.GetRecords ( "SGR", "STYP", ropType, &olRecords );
		ilDBRecords = olRecords.GetSize(); 
		for ( j=ilDBRecords-1; j>=0; j-- )
		{
		 	olCode = olRecords[j].Values[ilSgrCodeIdx];
			olChoiceList  += "*";		//  Kennzeichnung von Gruppen
			olChoiceList  += olCode;
			olChoiceList  += "\n" ;
		}
		olRecords.DeleteAll ();	

		omEquChoiceLists.SetAt ( ropType, olChoiceList );
		olGridLineInfo = "EQU\r";
		olGridLineInfo += ropType;
		omEquTableTypeStrings.SetAt ( ropType, olGridLineInfo );
	}
}


bool CServiceCatalogDoc::GetEquChoiceList ( CString &ropType, CString &ropChoiceList )
{
	
	if ( omEquChoiceLists.Lookup ( ropType, ropChoiceList ) )
		return true;
	else
	{
		ropChoiceList.Empty();
		return false;
	}
}

bool CServiceCatalogDoc::UpdateChoiceLists ( CString opTable, int ipBCType, 
											 RecordSet* popData, CString &ropResTable ) 
{
	int		ilSgrTabnIdx;
	UINT	i;
	bool	blModified;
	CString  olChoiceListOld, olChoiceListNew;
	int		 ilEqtIdx = -1;
	CString	 olEquType;

	ropResTable = opTable;
	if ( !popData )
		return false;

	if ( opTable == "SGR" )
	{
		ilSgrTabnIdx = ogBCD.GetFieldIndex("SGR", "TABN");
		if ( ilSgrTabnIdx < 0 )
			return false;
		ropResTable = popData->Values[ilSgrTabnIdx];
	}

	for ( i=0; i<imResTableAnz; i++ )
	{
		if ( psmUsedResourceTables[i].table == ropResTable )
		{
			olChoiceListOld = psmUsedResourceTables[i].choicelist;
			SetResourceChoiceList ( i );
			olChoiceListNew = psmUsedResourceTables[i].choicelist;
			if ( olChoiceListOld != olChoiceListNew )
				blModified = true;
		}
	}
	if ( ropResTable == "EQU" )
	{
		if ( opTable == "EQU" )
			ilEqtIdx = ogBCD.GetFieldIndex("EQU", "GKEY");
		if ( opTable == "SGR" )
			ilEqtIdx = ogBCD.GetFieldIndex("SGR", "STYP");
		if ( ilEqtIdx > 0 )
		{
			olEquType = popData->Values[ilEqtIdx];
			SetEquChoiceList ( olEquType );
		}
	}
	return blModified;
}