// HistoColumn.h: interface for the CHistoColumn class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HISTOCOLUMN_H__3E54BDC2_D32F_11D3_93D0_00001C033B5D__INCLUDED_)
#define AFX_HISTOCOLUMN_H__3E54BDC2_D32F_11D3_93D0_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CHistogramm;
struct ValInPeriod;

class CHistoColumn  
{
public:
	CHistoColumn(CHistogramm *popParent, int ipStart, float fpValue);
	CHistoColumn(CHistogramm *popParent, ValInPeriod &rrpValRec );
	virtual ~CHistoColumn();

//  Data members
	int			imStart;
	float		fmValue;
	CRect		omWorldRect;
	CRect		omDisplayRect;
	CHistogramm	*pomHistogramm;
	short	ilColor;

	void Draw ( CDC *popCDC );
	bool CalcWorldRect();
	bool CalcDisplayRect();
};

#endif // !defined(AFX_HISTOCOLUMN_H__3E54BDC2_D32F_11D3_93D0_00001C033B5D__INCLUDED_)
