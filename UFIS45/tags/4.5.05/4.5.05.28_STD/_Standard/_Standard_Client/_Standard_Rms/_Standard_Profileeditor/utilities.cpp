// utilities.cpp : implementation file
//

#include <stdafx.h>

#include <recordset.h>
#include <CCSPtrArray.h>
#include <CedaBasicData.h>

#include <utilities.h>
#include <mainfrm.h>
#include <CCSGlobl.h>
#include <profileeditor.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

bool SetDlgItemLangText ( CWnd *popWnd, UINT ipIDC, UINT ipIDS )
{
	CString olText;
	CWnd	*polItem;
	
	if ( !popWnd )
		return false;
	polItem = popWnd->GetDlgItem(ipIDC);
	if ( !polItem )
		return false;
	olText = GetString ( ipIDS );
	polItem->SetWindowText(olText);
	return true;
}

bool SetWindowLangText ( CWnd *popWnd, UINT ipIDS )
{
	CString olText;
	
	if ( !popWnd )
		return false;
	olText = GetString ( ipIDS );
	popWnd->SetWindowText(olText);
	return true;
}



bool EnableDlgItem ( CWnd *popWnd, UINT ipIDC, BOOL bpEnable, BOOL bpShow )
{
	CWnd *polCtrl = 0;
	if ( popWnd )
		polCtrl = popWnd->GetDlgItem ( ipIDC );
	if ( !polCtrl )
		return false;
	polCtrl->EnableWindow ( bpEnable );
	polCtrl->ShowWindow ( bpShow ? SW_SHOW : SW_HIDE );
	return true;
}

BOOL MoveDlgItem ( CWnd *popWnd, UINT ipIDC, int x, int y )
{
	CWnd *polCtrl = 0;
	if ( popWnd )
		polCtrl = popWnd->GetDlgItem ( ipIDC );
	if ( !polCtrl )
		return FALSE;
	return polCtrl->SetWindowPos( 0, x, y, 0, 0, SWP_NOSIZE | SWP_NOZORDER  );
}

BOOL ResizeDlgItem ( CWnd *popWnd, UINT ipIDC, int cx, int cy )
{
	CWnd *polCtrl = 0;
	if ( popWnd )
		polCtrl = popWnd->GetDlgItem ( ipIDC );
	if ( !polCtrl )
		return FALSE;
	return polCtrl->SetWindowPos( 0, 0, 0, cx, cy, SWP_NOMOVE | SWP_NOZORDER  );
}

//  erstellt eine CMapStringToOb f�r das Feld mit dem Index ipRefFieldIndex
//	auf die Indices innerhalb der ropRecordList 
void GetMapForRecordList ( CCSPtrArray<RecordSet> &ropRecordList, 
						   CMapStringToPtr &ropRecordMap, int ipRefFieldIndex )
{
	int i, ilAnz = ropRecordList.GetSize ();
	RecordSet *polActRec;
	CString olStr;

	ropRecordMap.RemoveAll();
	for ( i=0; i<ilAnz; i++ )
	{
		polActRec = &(ropRecordList.ElementAt(i));
		olStr = polActRec->Values.ElementAt (ipRefFieldIndex);
		ropRecordMap.SetAt ( polActRec->Values[ipRefFieldIndex], polActRec );
	}	
}


bool GetPrivProfInt( LPCTSTR pcpSection, LPCTSTR pcpEntry, int ipDefault, 
					 int &ripValue, LPCTSTR pcpFileName )
{
	char buffer[81];
	bool blRet = false;
	int  ilHelp;

	if ( GetPrivProfString ( pcpSection,  pcpEntry, "", buffer, 81, pcpFileName ) &&
		 sscanf ( buffer, "%d", &ilHelp ) )
	{
		 ripValue = ilHelp;
		 blRet = true;
	}
	else
		ripValue = ipDefault;
	return blRet;
}

void WriPrivProfInt( LPCTSTR pcpSection, LPCTSTR pcpEntry, int ipValue, 
					 LPCTSTR pcpFileName )
{
	char buffer[81];
	sprintf ( buffer, "%d", ipValue );
	WritePrivateProfileString ( pcpSection, pcpEntry, buffer, pcpFileName );
}
	

bool GetPrivProfString ( LPCTSTR pcpSection, LPCTSTR pcpEntry, LPCTSTR pcpDefault, 
					     LPTSTR pcpValue, DWORD ipSize, LPCTSTR pcpFileName )
{
	char buffer1[11], buffer2[11];
	bool blRet = false;

	GetPrivateProfileString ( pcpSection, pcpEntry, "A", buffer1, 10, pcpFileName );
	GetPrivateProfileString ( pcpSection, pcpEntry, "B", buffer2, 10, pcpFileName );
	  
	blRet = strcmp ( buffer1, buffer2 ) ? false : true;
	GetPrivateProfileString ( pcpSection, pcpEntry, pcpDefault, pcpValue,
							  ipSize, pcpFileName );
	return blRet;


}

int FileErrMsg ( HWND hwnd, char *lpText, char *filna, char* lpCaption/*=0*/, 
				 UINT uType/*=MB_OK*/ )
{
	char *line, pclEmpty[2]="" ;
	int  iret;
	char *pclCaption = lpCaption ? lpCaption : pclEmpty;
	int  length = strlen (lpText) + strlen (filna) + 1;
	if ( !(line = new char[length]) )
		iret = MessageBox ( hwnd, lpText, pclCaption, uType );
	else
	{
		sprintf ( line, lpText, filna );
		iret = MessageBox ( hwnd, line, pclCaption, uType );
		delete line;
	}
	return iret;
}

int FileErrMsg ( HWND hwnd, UINT idcText, char *filna, UINT idcCaption/*=0*/, 
				 UINT uType/*=MB_OK*/ )
{
	CString olText, olCaption;

	if ( idcText )
		olText = GetString ( idcText );
	if ( idcCaption )
		olCaption = GetString ( idcCaption );
	return FileErrMsg ( hwnd, pCHAR(olText), filna, pCHAR(olCaption), uType );
}

int LangMsgBox ( HWND hwnd, UINT idcText, UINT idcCaption/*=0*/, 
				 UINT uType/*=MB_OK*/ )
{
	CString olText, olCaption;

	if ( idcText )
		olText = GetString ( idcText );
	if ( idcCaption )
		olCaption = GetString ( idcCaption );
	return MessageBox ( hwnd, pCHAR(olText), pCHAR(olCaption), uType );
}

void SetStatusBarText ( CString &ropText )
{
	CMainFrame *polFrm = (CMainFrame *)theApp.m_pMainWnd;
	if ( polFrm )
		polFrm->SetMessageText ( ropText );
}


bool SaveWindowPosition ( CWnd *popWnd, char *pcpIniEntry )
{
	char polEintrag[81];
	RECT rect;
	if ( !popWnd || !pcpIniEntry )
		return false;
	popWnd->GetWindowRect ( &rect );
	sprintf ( polEintrag, "%d %d %d %d", rect.left, rect.top, rect.right, 
										 rect.bottom );
	if ( WritePrivateProfileString ( "FENSTER", pcpIniEntry, polEintrag, 
									 pcgIniFile ) )
		return true;
	else
		return false;
}


bool IniWindowPosition ( CWnd *popWnd, char *pcpIniEntry )
{
	char polEintrag[81];
	RECT rect;
	if ( !popWnd || !pcpIniEntry )
		return false;
	if ( !GetPrivProfString ( "FENSTER", pcpIniEntry, "", polEintrag, 80,
							  pcgIniFile )  || 
		 ( sscanf ( polEintrag, "%d %d %d %d", &rect.left, &rect.top, &rect.right, 
										 &rect.bottom ) < 4 )
	   )
		return false;
	popWnd->MoveWindow ( &rect );
	return true;
}

int FindStringInside ( CString opWhole, CString opSub, 
					   BOOL bpCaseSensitiv/*=TRUE*/ )
{
	CString olWhole = opWhole;
	CString olSub = opSub;
	if ( !bpCaseSensitiv )
	{
		olWhole.MakeUpper();
		olSub.MakeUpper();
	}
	return olWhole.Find ( olSub );
}

void ClientToView( CScrollView *popView, LPRECT lpRect )
{
	CPoint olScrollPos = popView->GetDeviceScrollPosition() ; 
	lpRect->left   -= olScrollPos.x;
	lpRect->top	   -= olScrollPos.y;
	lpRect->right  -= olScrollPos.x;
	lpRect->bottom -= olScrollPos.y;
}

void ClientToView( CScrollView *popView, LPPOINT lpPoint ) 
{
	CPoint olScrollPos = popView->GetDeviceScrollPosition() ; 
	lpPoint->x -= olScrollPos.x;
	lpPoint->y -= olScrollPos.y;
}

CString GetToolTip ( CHOICE_LISTS *popChoice, CString &ropSelValue )
{
	CString olRefTable, olRefCodeField;
	CString	olToolTip;
	if ( ParseReftFieldEntry ( popChoice->RefTabField, olRefTable, 
							   olRefCodeField ) )
		olToolTip = ogBCD.GetField( olRefTable, olRefCodeField, ropSelValue, 
								    popChoice->ToolTipField );
	return olToolTip ;
}

CString GetGrpToolTip ( CHOICE_LISTS *popChoice, CString &ropSelValue )
{
	int						ilCount;
	CString					olToolTip, olSgrUrno, olMemberCode, olRefUrno;
	CCSPtrArray<RecordSet>	olGroupMembers;

	CString olRefTable, olRefCodeField;
	if ( !ParseReftFieldEntry ( popChoice->RefTabField, olRefTable, 
							    olRefCodeField ) )
		return olToolTip;
	olSgrUrno = ogBCD.GetFieldExt( "SGR", "TABN", "GRPN", olRefTable, 
								   ropSelValue, "URNO" );
	if ( !olSgrUrno.IsEmpty () )
		ogBCD.GetRecords( "SGM", "USGR", olSgrUrno, &olGroupMembers );

	ilCount = olGroupMembers.GetSize();
	int ilSgmUvalIdx = ogBCD.GetFieldIndex ( "SGM", "UVAL" );
	for ( int i=0; (i<ilCount) && (ilSgmUvalIdx>=0); i++ )
	{
		olRefUrno = olGroupMembers[i].Values[ilSgmUvalIdx];
		olMemberCode = ogBCD.GetField ( olRefTable, "URNO", olRefUrno, 
										olRefCodeField );
		if ( !olMemberCode.IsEmpty () )
			olToolTip += olMemberCode + ",";
	}
	int n = olToolTip.ReverseFind( ',' );
	if ( n>0 )
		olToolTip = olToolTip.Left ( n );
	olGroupMembers.DeleteAll();
	return olToolTip;
}	

bool DeleteByWhere(CString opTable, CString opRefField, CString opRefValue, bool bpDbSave/*=true*/)
{
	// ---------------------------------------------------------------------------
	//  CedaBasicData::DeleteByWhere funktioniert nur auf dem Server (d.h. wirkt sich nicht auf 
	//  logische Objekte aus), Broadcasts f�r lokale Datenhaltung kommen nur an, 
	//  wenn URNO im Where-String vorkommt
	// ---------------------------------------------------------------------------

	CCSPtrArray <RecordSet> olRecArr;
	CString					olUrno;
	bool					blRet = true;
	int						ilUrnoIdx;

	ogBCD.GetRecords(opTable, opRefField, opRefValue, &olRecArr);
	if ( olRecArr.GetSize() > 0 )
	{
		ilUrnoIdx = ogBCD.GetFieldIndex ( opTable, "URNO" );
		if ( ilUrnoIdx >= 0 )
		{
			for ( int i=olRecArr.GetSize()-1; i>=0; i-- )
			{
				olUrno = olRecArr[i].Values[ilUrnoIdx];
				blRet &= ogBCD.DeleteRecord(opTable, "URNO", olUrno );
			}
			if ( bpDbSave )
				blRet &= ogBCD.Save ( opTable );
		}
		else 
			blRet = false;
		olRecArr.DeleteAll();
	}
	return blRet;
}

//  Setze BroadCast-Typen in ogBCD f�r Tabelle opTable
//  ruft intern SetDdxType mit "IRT", "DRT" und "URT" auf.
bool SetBCTypes ( CString opTable )
{
	int ilIndex;

	if ( opTable.GetLength () > 3 )
		opTable = opTable.Left(3) ;
	if ( ogBCTableIndices.Lookup ( opTable, (void*&)ilIndex ) && 
		 ( ilIndex < ogBroadCastTables.GetSize() ) )
		 return false;		//  DdxType f�r diese Tabelle bereits gesetzt

	ilIndex = ogBroadCastTables.Add ( opTable );
	ogBCTableIndices.SetAt ( opTable, (void*)ilIndex );

	ilIndex *= 10;
	ogBCD.SetDdxType( opTable, "IRT", ilIndex+BC_NEW );
	ogBCD.SetDdxType( opTable, "URT", ilIndex+BC_CHANGED );
	ogBCD.SetDdxType( opTable, "DRT", ilIndex+BC_DELETED );
	return true;			
}

//  Registriere vpInstance f�r die BroadCast-Typen, die sich aus Tabelle 
//	opTable und den in ipTypes gesetzten Bits ergeben
//  ruft intern CCSDdx::Register mit den resultierenden BC-Typen auf
void RegisterBC ( void *vpInstance, CString opInstanceName, CString opTable, 
				  DDXCALLBACK pfpCallBack, UINT ipTypes/*=BC_ALL*/ )
{
	int  ilBCNr;
	CString olDDXName;

	if ( opTable.GetLength () > 3 )
		opTable = opTable.Left(3) ;

	if ( (ipTypes&BC_NEW) && (ilBCNr=CalcBCNumber(opTable, BC_NEW) ) )
	{
		olDDXName.Format( "%s New", opTable );
		ogDdx.Register ( vpInstance, ilBCNr, olDDXName, opInstanceName, 
						 pfpCallBack );
	}	
	if ( (ipTypes&BC_CHANGED) && (ilBCNr=CalcBCNumber(opTable, BC_CHANGED) ) )
	{
		olDDXName.Format( "%s Changed", opTable );
		ogDdx.Register ( vpInstance, ilBCNr, olDDXName, opInstanceName, 
						 pfpCallBack );
	}	
	if ( (ipTypes&BC_DELETED) && (ilBCNr=CalcBCNumber(opTable, BC_DELETED) ) )
	{
		olDDXName.Format( "%s Deleted", opTable );
		ogDdx.Register ( vpInstance, ilBCNr, olDDXName, opInstanceName, 
						 pfpCallBack );
	}	
}

//  Berechne BroadCast-Typ, der sich aus Tabelle opTable und dem in ipType
//	gesetzten Bits ergibt
int CalcBCNumber ( CString &ropTable, UINT ipType )
{
	int ilIndex = 0, ilRet=0;
	if ( !ogBCTableIndices.Lookup ( ropTable, (void*&)ilIndex ) )
	{
		SetBCTypes ( ropTable );
		if ( !ogBCTableIndices.Lookup ( ropTable, (void*&)ilIndex ) )
			return 0;		//  irgend etwas lief schief
	}
	if ( (ipType & BC_ALL) == ipType )
		ilRet = ilIndex*10+ipType;
	return ilRet;
}

//  Berechne aus dem BroadCast-Typ, der im BroadCast gemeldet wurde, 
//	die betroffene Tabelle und die Art der �nderung ("IRT","URT","DRT")
bool GetBCType ( int ipDDXType, CString &ropTable, UINT &ripType )
{
	int ilIndex=ipDDXType/10;
	ripType = ipDDXType % 10;

	if ( ( ripType != BC_NEW ) && ( ripType != BC_CHANGED ) && 
		 ( ripType != BC_DELETED ) )
		 return false;
	if ( ilIndex >= ogBroadCastTables.GetSize() )
		return false;
	ropTable = ogBroadCastTables.GetAt(ilIndex);
	return true;
}

//  L�st internen BroadCast f�r Tabelle opTable aus
//  ruft intern DataChanged mit dem BroadCast-Typ auf.
bool InternalBC ( void *vpInstance, CString opTable, UINT ipType, 
				  void *vpDataPointer )
{
	if ( opTable.GetLength () > 3 )
		opTable = opTable.Left(3) ;
	int ilBCNr = CalcBCNumber ( opTable, ipType );
	if ( ilBCNr )
		ogDdx.DataChanged(vpInstance, ilBCNr, vpDataPointer );
	return (ilBCNr !=0);
}

