// ValidityDlg.cpp : implementation file
//

#include <stdafx.h>
#include <gxall.h>

#include <CedaBasicData.h>

#include <ValidityDlg.h>
#include <CCSGlobl.h>
#include <BasicData.h>
#include <utilities.h>
#include <ddxddv.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


//-----------------------------------------------------------------------------------------------------------------------
//				defines
//-----------------------------------------------------------------------------------------------------------------------



//-----------------------------------------------------------------------------------------------------------------------
//				construction / destruction
//-----------------------------------------------------------------------------------------------------------------------
ValidityDlg::ValidityDlg(bool bpNew, CWnd* pParent /*=NULL*/)
	: CDialog(ValidityDlg::IDD, pParent)
{
	//--- exception handling
	CCS_TRY
	
	bmFixedTimes = false;
	bmIsGridEnabled = false;
	bmNewFlag = bpNew;

	//{{AFX_DATA_INIT(ValidityDlg)
	m_bUnlimited = FALSE;
	//}}AFX_DATA_INIT
	
	pomParent = pParent;
	pomGrid = 0;
	
	imExclCount = 0;

	//--- exception handling
	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

ValidityDlg::~ValidityDlg()
{
	if (rmValData.Excls.GetSize() > 0)
	{
		rmValData.Excls.DeleteAll();
	}
	delete pomGrid;
}


//-----------------------------------------------------------------------------------------------------------------------
//				data exchange, message map
//-----------------------------------------------------------------------------------------------------------------------
void ValidityDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ValidityDlg)
	DDX_Control(pDX, IDC_UNLIMITED_CHK, m_Chb_Unlimited);
	DDX_Control(pDX, IDC_TIMETO, m_TimeToCtrl);
	DDX_Control(pDX, IDC_TIMEFROM, m_TimeFromCtrl);
	DDX_Control(pDX, IDC_FIXED_TO, m_FixTimeTo);
	DDX_Control(pDX, IDC_FIXED_FROM, m_FixTimeFrom);
	DDX_Control(pDX, IDC_DATEFROM, m_DateFromCtrl);
	DDX_Control(pDX, IDC_DATETO, m_DateToCtrl);
	DDX_Control(pDX, IDC_FIXED_BTN, m_FixedBtn);
	DDX_Control(pDX, IDC_TIME_LBL4, m_TimeStatic4);
	DDX_Control(pDX, IDC_TIME_LBL3, m_TimeStatic3);
	DDX_Control(pDX, IDC_TIME_LBL2, m_TimeStatic2);
	DDX_Control(pDX, IDC_TIME_LBL1, m_TimeStatic);
	DDX_Control(pDX, IDC_UNTIL_STATIC, m_UntilFrame);
	DDX_Control(pDX, IDC_FROM_STATIC, m_FromFrame);
	DDX_Control(pDX, IDC_DETAILS_BTN, m_DetailsBtn);
	DDX_Control(pDX, IDC_WEDNESDAY_CHB, m_WednesdayChb);
	DDX_Control(pDX, IDC_TUESDAY_CHB, m_TuesdayChb);
	DDX_Control(pDX, IDC_THURSDAY_CHB, m_ThursdayChb);
	DDX_Control(pDX, IDC_SUNDAY_CHB, m_SundayChb);
	DDX_Control(pDX, IDC_SATURDAY_CHB, m_SaturdayChb);
	DDX_Control(pDX, IDC_MONDAY_CHB, m_MondayChb);
	DDX_Control(pDX, IDC_FRIDAY_CHB, m_FridayChb);
	DDX_Control(pDX, IDC_UNTIL_SLIDER, m_UntilSlider);
	DDX_Control(pDX, IDC_FROM_SLIDER, m_FromSlider);
	DDX_Check(pDX, IDC_UNLIMITED_CHK, m_bUnlimited);
	//}}AFX_DATA_MAP
}
//-----------------------------------------------------------------------------------------------------------------------

BEGIN_MESSAGE_MAP(ValidityDlg, CDialog)
	//{{AFX_MSG_MAP(ValidityDlg)
	ON_BN_CLICKED(IDC_INVERT_BTN, OnInvert)
	ON_BN_CLICKED(IDC_DETAILS_BTN, OnDetails)
	ON_WM_LBUTTONDOWN()
	ON_WM_HSCROLL()
	ON_MESSAGE(WM_GRID_STARTEDITING, OnStartEditing)
	ON_MESSAGE(WM_GRID_ENDEDITING, OnEndEditing)
	ON_MESSAGE(WM_GRID_LBUTTONCLICK, OnGridClick)
	ON_BN_CLICKED(IDC_FIXED_BTN, OnFixedBtn)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_FIXED_FROM, OnChangeFixedFrom)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_FIXED_TO, OnChangeFixedTo)
	ON_BN_CLICKED(IDC_UNLIMITED_CHK, OnUnlimitedChk)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

 
//-----------------------------------------------------------------------------------------------------------------------
//				message handlers
//-----------------------------------------------------------------------------------------------------------------------
BOOL ValidityDlg::OnInitDialog() 
{
	CCS_TRY

	CDialog::OnInitDialog();
	
	SetStaticTexts();

	//---    Initialize grids
	//BOOL blErr = pomGrid->SubclassDlgItem(IDC_CUSTOM, this);
	pomGrid = new CGridControl ( this, IDC_CUSTOM, 0, 0 );

	//pomGrid->Initialize();
    InitializeTable();
	
	// valid from 
	CString olTmp;
	CTime olTime;
	olTmp = rmValData.Vafr;
	if (olTmp.GetLength() == 14)
	{
		olTime = DBStringToDateTime(olTmp);
		m_DateFromCtrl.SetTime(&olTime);
		m_TimeFromCtrl.SetTime(&olTime);
	}
	else
	{
		olTime = CTime::GetCurrentTime();
		olTime = CTime(olTime.GetYear(), olTime.GetMonth(), olTime.GetDay(), 0, 0, 0);

		m_DateFromCtrl.SetTime(&olTime);
		m_TimeFromCtrl.SetTime(&olTime);
	}

	// valid to
	olTmp = rmValData.Vato;
	m_bUnlimited = (olTmp.GetLength() < 14);
	if ( !m_bUnlimited )
	{
		olTime = DBStringToDateTime(olTmp);
		m_DateToCtrl.SetTime(&olTime);
		m_TimeToCtrl.SetTime(&olTime);
	}
	else
	{
		olTime = CTime::GetCurrentTime();
		int ilYear = olTime.GetYear();
		int ilMonth = olTime.GetMonth() ;
		int ilDay = olTime.GetDay() ;         

		if ( (ilMonth==2) && (ilDay==29 ) )		//  wenn heute Schalttag, 
			ilDay--;							//	dann nicht in 10 Jahren
		olTime = CTime(ilYear+10, ilMonth, ilDay, 23,59,59 );
		olTmp = olTime.Format("%Y%m%d%H%M%S");	//  nur Test

		m_DateToCtrl.SetTime(&olTime);
		m_TimeToCtrl.SetTime(&olTime);
	}

	// frequency
	CString olFreq;
	olFreq = rmValData.Freq;
	if (olFreq.IsEmpty() == FALSE || olFreq != CString(" "))
	{
		olFreq.Mid(0, 1) == CString("1")  ?  m_MondayChb.SetCheck(1)    :  m_MondayChb.SetCheck(0);
		olFreq.Mid(1, 1) == CString("1")  ?  m_TuesdayChb.SetCheck(1)   :  m_TuesdayChb.SetCheck(0);
		olFreq.Mid(2, 1) == CString("1")  ?  m_WednesdayChb.SetCheck(1) :  m_WednesdayChb.SetCheck(0);
		olFreq.Mid(3, 1) == CString("1")  ?  m_ThursdayChb.SetCheck(1)  :  m_ThursdayChb.SetCheck(0);
		olFreq.Mid(4, 1) == CString("1")  ?  m_FridayChb.SetCheck(1)    :  m_FridayChb.SetCheck(0);
		olFreq.Mid(5, 1) == CString("1")  ?  m_SaturdayChb.SetCheck(1)  :  m_SaturdayChb.SetCheck(0);
		olFreq.Mid(6, 1) == CString("1")  ?  m_SundayChb.SetCheck(1)    :  m_SundayChb.SetCheck(0);
	}
	else
	{
		m_MondayChb.SetCheck(1);
		m_TuesdayChb.SetCheck(1);
		m_WednesdayChb.SetCheck(1);
		m_ThursdayChb.SetCheck(1);
		m_FridayChb.SetCheck(1);
		m_SaturdayChb.SetCheck(1);
		m_SundayChb.SetCheck(1);
	}
	
	// excludes
	int ilExclCount = rmValData.Excls.GetSize();
	pomGrid->SetRowCount(1);
	CString olStartDate, olEndDate;
	
	for (int i = 0; i < ilExclCount; i++)
	{
		olStartDate = rmValData.Excls[i].FirstDay.Format("%d.%m.%Y");
		olEndDate = rmValData.Excls[i].LastDay.Format("%d.%m.%Y");

		pomGrid->SetValueRange(CGXRange(i + 1, 1), olStartDate);
		pomGrid->SetValueRange(CGXRange(i + 1, 2), olEndDate);
		pomGrid->DoAutoGrow(i + 1, 1);
	}

	//-- fixed times 
	int n = 0, ilFixedFrom, ilFixedTill;

	if (rmValData.FixF[0])
	{
		n = sscanf(rmValData.FixF, "%d", &ilFixedFrom);
	}

	if (rmValData.FixT[0])
	{
		n += sscanf(rmValData.FixT, "%d", &ilFixedTill);
	}
	
	bmFixedTimes = (n == 2);
	if (bmFixedTimes)
	{
		CTime olTime;
		olTime = GetTimeFromMinutes(ilFixedFrom);
		m_FixTimeFrom.SetTime(&olTime);

		olTime = GetTimeFromMinutes(ilFixedTill);
		m_FixTimeTo.SetTime(&olTime);
	}
	
	m_FromSlider.SetRange(0, 1439, FALSE);
	m_FromSlider.SetTicFreq(60);
	m_UntilSlider.SetRange(0, 1439, TRUE);
	m_UntilSlider.SetTicFreq(60);

	m_FromSlider.SetPos(bmFixedTimes ? ilFixedFrom : 0);
	m_UntilSlider.SetPos(bmFixedTimes ? ilFixedTill : 0);

	/*m_FromSlider.ClearTics(bmFixedTimes);
	m_UntilSlider.ClearTics(bmFixedTimes);*/
	
	pomGrid->EnableSorting(false);
		
	m_DateToCtrl.EnableWindow(!m_bUnlimited);
	m_TimeToCtrl.EnableWindow(!m_bUnlimited);
	UpdateData(FALSE);

	// hide details part of window
	if ((bmFixedTimes == false) && (ilExclCount < 1) && (olFreq == CString("1111111")))
	{
		CRect olRect;
		GetWindowRect(&olRect);
		//olRect.top -= 20;
		olRect.bottom -= 460;
		MoveWindow(olRect);
		bmDetails = false;
	}
	else
	{
		CString olText;
		m_DetailsBtn.GetWindowText(olText);
		olText.Replace(">","<");
		m_DetailsBtn.SetWindowText(olText);
		bmDetails = true;
	}
	

	SetFixedTimesControls ();


	CCS_CATCH_ALL

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::OnOK() 
{
	CCS_TRY

	bool blCanSave = true;		//true: Everything OK, data can be written to DB

//--- validate data and fill data struct
	CTime olVafr;
	CTime olVato;
	CTime olDafr;
	CTime olDato;
	CTime olTito;
	CTime olTifr;
	CTime olToday;

	//-- get dates and times
	m_DateFromCtrl.GetTime(olDafr);
	m_TimeFromCtrl.GetTime(olTifr);
	m_DateToCtrl.GetTime(olDato);
	m_TimeToCtrl.GetTime(olTito);

	olToday = CTime::GetCurrentTime();
	olToday = CTime(olToday.GetYear(), olToday.GetMonth(), olToday.GetDay(), 0, 0, 0);
	olVafr = CTime(olDafr.GetYear(), olDafr.GetMonth(), olDafr.GetDay(), olTifr.GetHour(), olTifr.GetMinute(), olTifr.GetSecond());
	olVato = CTime(olDato.GetYear(), olDato.GetMonth(), olDato.GetDay(), olTito.GetHour(), olTito.GetMinute(), olTito.GetSecond());

	
	//-- validate
	if ((!m_bUnlimited &&(olVafr >= olVato)) || ((olVafr < olToday) && bmNewFlag))
	{
		MessageBox(GetString(IDS_WRONG_VALIDITY), GetString(AFX_IDS_APP_TITLE), 
				   MB_OK | MB_ICONEXCLAMATION);
	}
	else
	{
		//-- vafr string
		CString olText;
		
		olText = CTimeToDBString(olVafr, olVafr); 
		strcpy(rmValData.Vafr, olText);
		
		//-- vato string
		//olText = olVato.Format("%Y%m%d%H%M59"); 
		if (m_bUnlimited)
		{
			rmValData.Vato[0]='\0';
		}
		else
		{
			olText = CTimeToDBString(olVato, olVato);
			strcpy(rmValData.Vato, olText);
		}

		//-- frequency
		if ((m_MondayChb.GetCheck() || m_TuesdayChb.GetCheck() || m_WednesdayChb.GetCheck() || m_ThursdayChb.GetCheck() || 
			 m_FridayChb.GetCheck() || m_SaturdayChb.GetCheck() || m_SundayChb.GetCheck()) == 0)
		{
			int ilReply = MessageBox(GetString(IDS_SAVE_INVALID_PROFILE), 
									 GetString(IDS_WRONG_INPUT), 
									  MB_ICONQUESTION|MB_YESNO);
			if (ilReply == IDNO)
			{
				blCanSave = false;
			}
		}

		olText = "";
		m_MondayChb.GetCheck()    ?  olText += CString("1")  :  olText += CString("0");
		m_TuesdayChb.GetCheck()   ?  olText += CString("1")  :  olText += CString("0");
		m_WednesdayChb.GetCheck() ?  olText += CString("1")  :  olText += CString("0");
		m_ThursdayChb.GetCheck()  ?  olText += CString("1")  :  olText += CString("0");
		m_FridayChb.GetCheck()    ?  olText += CString("1")  :  olText += CString("0");
		m_SaturdayChb.GetCheck()  ?  olText += CString("1")  :  olText += CString("0");
		m_SundayChb.GetCheck()    ?  olText += CString("1")  :  olText += CString("0");
		strcpy(rmValData.Freq, olText);
		

		//-- get excluded days from grid
		CString olStartDate, olEndDate;
		EXCLUDES rlExcl;

		int ilCount = pomGrid->GetRowCount();
		if (ilCount > 0)
		{
			SortExcludes();
		}
		rmValData.Excls.DeleteAll ();	// hag991129
		for (int i = 1; i <= ilCount; i++)
		{
			olStartDate = pomGrid->GetValueRowCol(i, 1);
			olEndDate = pomGrid->GetValueRowCol(i, 2);
			
			if (olStartDate.IsEmpty() == FALSE && olStartDate != CString(" "))
			{
				if (olEndDate.IsEmpty() == TRUE)
				{
					olEndDate = olStartDate;
				}

				rlExcl.FirstDay = DateStringToDate(olStartDate);
				rlExcl.LastDay = DateStringToDate(olEndDate);
				rlExcl.LastDay += CTimeSpan(0,23,59,59);
					
				rmValData.Excls.New(rlExcl);
			}
		}

		//-- fixed times 
		if (bmFixedTimes == true) 
		{
			CTime olTimeFrom;
			m_FixTimeFrom.GetTime(olTimeFrom);
			// ***** Hier liegt noch ein Fehler vor ! GetMinutesFromTime liefert keine Zeit !!!
			sprintf(rmValData.FixF, "%d", GetMinutesFromTime(olTimeFrom));

			CTime olTimeTo;
			m_FixTimeTo.GetTime(olTimeTo);
			sprintf(rmValData.FixT, "%d", GetMinutesFromTime(olTimeTo));
		}
		else
			rmValData.FixT[0] = rmValData.FixF[0] = '\0';

		
	//--- save data to member struct
		if (blCanSave == true)
		{
			rmValData.IsFilled = true;
			CDialog::OnOK();
		}
		else
		{
			Beep(900, 150);
		}
	}

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::OnStartEditing(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY

	ROWCOL ilCol = 0;
	ROWCOL ilRow = 0;
	GRIDNOTIFY rlNotify = *((GRIDNOTIFY*)lParam);

	ilCol = rlNotify.col;
	ilRow = rlNotify.row;

	if (ilRow > 0)
	{
		if (ilCol == 2)
		{
			CString olText, olText2;
			olText = pomGrid->GetValueRowCol(ilRow, 1);
			olText2 = pomGrid->GetValueRowCol(ilRow, 2);
			if ( olText2.IsEmpty () )
				pomGrid->SetValueRange(CGXRange(ilRow, 2), olText);
		}
	}

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::OnEndEditing(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY

	ROWCOL ilCol = 0;
	ROWCOL ilRow = 0;
	GRIDNOTIFY rlNotify = *((GRIDNOTIFY*)lParam);

	ilCol = rlNotify.col;
	ilRow = rlNotify.row;

	bool blValid = false;
	CTime olVafr, olVato, olDate2Test;
	CTime olDafr, olDato, olTifr, olTito;

	CString olText;
	olText = pomGrid->GetValueRowCol(ilRow, ilCol);

	if (ilRow > 0)
	{
		m_DateFromCtrl.GetTime(olDafr);
		m_TimeFromCtrl.GetTime(olTifr);
		m_DateToCtrl.GetTime(olDato);
		m_TimeToCtrl.GetTime(olTito);

		olVafr = CTime(olDafr.GetYear(), olDafr.GetMonth(), olDafr.GetDay(), olTifr.GetHour(), olTifr.GetMinute(), 0);
		olVato = CTime(olDato.GetYear(), olDato.GetMonth(), olDato.GetDay(), olTito.GetHour(), olTito.GetMinute(), 0);
		
		if (ilCol == 1)
		{
			blValid = (DateStringToDateExt(olText, olDate2Test) == TRUE);
			blValid &= CheckExclIsValid(olDate2Test, ilRow, ilCol);
			if ((blValid == false)/* || (olDate2Test <= olVafr) || 
					((olDate2Test >= olVato) && (m_Chb_Unlimited.GetCheck() == 0))*/)
			{
				Beep(800, 200);
				pomGrid->SetValueRange(CGXRange(ilRow, ilCol), CString(""));
				pomGrid->DeleteEmptyRows(ilRow, ilCol);
			}
		}
	
		if (ilCol == 2)
		{
			CString olLeft = pomGrid->GetValueRowCol(ilRow, 1);
			if (olLeft.IsEmpty() == TRUE)
			{
				Beep(800, 200);
				pomGrid->SetValueRange(CGXRange(ilRow, ilCol), CString(""));
				pomGrid->DeleteEmptyRows(ilRow, ilCol);
			}
			else
			{
				CTime olLeftDate;
				blValid = (DateStringToDateExt(olLeft, olLeftDate) ==TRUE);
				blValid &= (DateStringToDateExt(olText, olDate2Test)==TRUE);
				if ((blValid == false) || ((olDate2Test > olVato) && (m_Chb_Unlimited.GetCheck() == 0)) ||
						(olDate2Test < olLeftDate))
				{
					Beep(800, 200);
					pomGrid->SetValueRange(CGXRange(ilRow, ilCol), pomGrid->GetValueRowCol(ilRow, 1));
				}
			}
		}
	}
	
	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::OnGridClick(WPARAM wParam, LPARAM lParam)
{
	GRIDNOTIFY rlNotify = *((GRIDNOTIFY*)lParam);

	ROWCOL ilCol = rlNotify.col;
	ROWCOL ilRow = rlNotify.row;

	if (ilRow == 0)
	{
		SortExcludes();
	}
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::OnCancel() 
{
	CDialog::OnCancel();
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::OnInvert() 
{
	CCS_TRY

	UpdateData();

	m_MondayChb.GetCheck()	  ? m_MondayChb.SetCheck(0) : m_MondayChb.SetCheck(1);
	m_TuesdayChb.GetCheck()	  ? m_TuesdayChb.SetCheck(0) : m_TuesdayChb.SetCheck(1);
	m_WednesdayChb.GetCheck() ? m_WednesdayChb.SetCheck(0) : m_WednesdayChb.SetCheck(1);
	m_ThursdayChb.GetCheck()  ? m_ThursdayChb.SetCheck(0) : m_ThursdayChb.SetCheck(1);
	m_FridayChb.GetCheck()	  ? m_FridayChb.SetCheck(0) : m_FridayChb.SetCheck(1);
	m_SaturdayChb.GetCheck()  ? m_SaturdayChb.SetCheck(0) : m_SaturdayChb.SetCheck(1);
	m_SundayChb.GetCheck()	  ? m_SundayChb.SetCheck(0) : m_SundayChb.SetCheck(1);

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::OnDetails() 
{
	CCS_TRY

	CString olText;
	CRect olRect;
	GetWindowRect(&olRect);

	if (bmDetails == false)
	{
		//--- disable fixed time controls
		/*
		m_FromSlider.EnableWindow(bmFixedTimes);
		m_FixTimeFrom.EnableWindow(bmFixedTimes);
		m_FromFrame.EnableWindow(bmFixedTimes);
		m_TimeStatic.EnableWindow(bmFixedTimes);
		m_TimeStatic2.EnableWindow(bmFixedTimes);
		m_TimeStatic3.EnableWindow(bmFixedTimes);
		m_TimeStatic4.EnableWindow(bmFixedTimes);
		m_FixTimeTo.EnableWindow(bmFixedTimes);
		m_UntilSlider.EnableWindow(bmFixedTimes);
		m_UntilFrame.EnableWindow(bmFixedTimes);
		*/
		SetFixedTimesControls ();

		//--- increase window
		olRect.bottom += 460;

		//--- set button text
		m_DetailsBtn.GetWindowText(olText);
		olText.Replace(">","<");
		m_DetailsBtn.SetWindowText(olText);

		//--- set activation flag
		bmDetails = true;
	}
	else
	{
		//--- increase window
		olRect.bottom -= 460;

		//--- set button text
		m_DetailsBtn.GetWindowText(olText);
		olText.Replace("<",">");
		m_DetailsBtn.SetWindowText(olText);

		//--- set activation flag
		bmDetails = false;
	}

	MoveWindow(olRect);

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::OnFixedBtn() 
{
	CCS_TRY
	
/*
	if (bmFixedTimes == false)
	{
		bmFixedTimes = true;
		m_TimeStatic.EnableWindow(TRUE);
		m_TimeStatic2.EnableWindow(TRUE);
		m_TimeStatic3.EnableWindow(TRUE);
		m_TimeStatic4.EnableWindow(TRUE);
		m_FixedBtn.SetWindowText(GetString(1425));
		m_FromSlider.EnableWindow(TRUE);
		m_FixTimeFrom.EnableWindow(TRUE);
		m_FromFrame.EnableWindow(TRUE);
		m_UntilSlider.EnableWindow(TRUE);
		m_FixTimeTo.EnableWindow(TRUE);
		m_UntilFrame.EnableWindow(TRUE);
	}
	else
	{
		bmFixedTimes = false;
		m_TimeStatic.EnableWindow(FALSE);
		m_TimeStatic2.EnableWindow(FALSE);
		m_TimeStatic3.EnableWindow(FALSE);
		m_TimeStatic4.EnableWindow(FALSE);
		m_FixedBtn.SetWindowText(GetString(1424));
		m_FromSlider.EnableWindow(FALSE);
		m_FixTimeFrom.EnableWindow(FALSE);
		m_FromFrame.EnableWindow(FALSE);
		m_UntilSlider.EnableWindow(FALSE);
		m_FixTimeTo.EnableWindow(FALSE);
		m_UntilFrame.EnableWindow(FALSE);
	}
*/
	bmFixedTimes = !bmFixedTimes ;
	SetFixedTimesControls ();

	m_FromSlider.SetPos(0);
	m_UntilSlider.SetPos(1439);

	CTime olTime = CTime(1980, 1, 1, 0, 0, 0);
	m_FixTimeFrom.SetTime(&olTime);
	olTime = CTime(1980, 1, 1, 23, 59, 0);
	m_FixTimeTo.SetTime(&olTime);

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::OnChangeFixedFrom(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CTime olTime;
	m_FixTimeFrom.GetTime(olTime);
	m_FromSlider.SetPos(GetMinutesFromTime(olTime));
	
	*pResult = 0;
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::OnChangeFixedTo(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CTime olTime;
	m_FixTimeTo.GetTime(olTime);
	m_UntilSlider.SetPos(GetMinutesFromTime(olTime));
	
	*pResult = 0;
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CDialog::OnLButtonDown(nFlags, point);
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// compare handles to find out which slider is used
	// Don't insert exception handling code here, it might slow down the program too much !

	if (m_FromSlider.m_hWnd == pScrollBar->m_hWnd)
	{
		// set FROM text in DateTimeCtrl according to slider
		int ilPos = m_FromSlider.GetPos();
		CTime olTime = GetTimeFromMinutes(ilPos);
		m_FixTimeFrom.SetTime(&olTime);
	}

	if (m_UntilSlider.m_hWnd == pScrollBar->m_hWnd)
	{
		// set UNTIL text in DateTimeCtrl according to slider
		int ilPos = m_UntilSlider.GetPos();
		CTime olTime = GetTimeFromMinutes(ilPos);
		m_FixTimeTo.SetTime(&olTime);
	}
	
	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}



//-----------------------------------------------------------------------------------------------------------------------
//						get methods						
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::GetValData(VALDATA &rrpValData)
{/*
	EXCLUDES rlExcl;
	
	rrpValData.Excls.DeleteAll();

	int ilCount = rmValData.Excls.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		rlExcl = rmValData.Excls[i];
		rrpValData.Excls.New(rlExcl);

		CTime test1 = rlExcl.FirstDay;
		CTime test2 = rlExcl.LastDay;
	}
	
	strcpy(rrpValData.FixF, rmValData.FixF);
	strcpy(rrpValData.FixT, rmValData.FixT);
	strcpy(rrpValData.Freq, rmValData.Freq);
	strcpy(rrpValData.Vafr, rmValData.Vafr);
	strcpy(rrpValData.Vato, rmValData.Vato);
	
	rrpValData.IsFilled = rmValData.IsFilled;
	*/
	CopyValData(&rrpValData, &rmValData );
}




//-----------------------------------------------------------------------------------------------------------------------
//						set methods						
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::SetValData(VALDATA *prpValData)
{/*
	EXCLUDES rlExcl;
	
	int ilCount = prpValData->Excls.GetSize();

	for (int i = 0; i < ilCount; i++)
	{
		rlExcl = prpValData->Excls[i];
		rmValData.Excls.New(rlExcl);

		CTime test1 = rlExcl.FirstDay;
		CTime test2 = rlExcl.LastDay;
	}

	strcpy(rmValData.FixF, prpValData->FixF);
	strcpy(rmValData.FixT, prpValData->FixT);
	strcpy(rmValData.Freq, prpValData->Freq);
	strcpy(rmValData.Vafr, prpValData->Vafr);
	strcpy(rmValData.Vato, prpValData->Vato);
	
	rmValData.IsFilled = prpValData->IsFilled;   */
	CopyValData(&rmValData, prpValData );
}




//-----------------------------------------------------------------------------------------------------------------------
//						helper functions
//-----------------------------------------------------------------------------------------------------------------------
int ValidityDlg::GetMinutesFromTime(CTime opTime)
{
	int ilReturn = 0;

	CCS_TRY
	

	int ilHour = atoi(opTime.Format("%H"));
	int ilMinute = atoi(opTime.Format("%M"));

	ilReturn = ilHour * 60 + ilMinute;


	CCS_CATCH_ALL

	return ilReturn;
}
//-----------------------------------------------------------------------------------------------------------------------

CTime ValidityDlg::GetTimeFromMinutes(int ipMinutes)
{
	CTime olReturn;

//--- exception handling
	CCS_TRY

	int ilHour = (int)(ipMinutes / 60);
	int ilMinute = ipMinutes % 60;

	olReturn = CTime(1980, 1, 1, ilHour, ilMinute, 0);

//--- exception handling
	CCS_CATCH_ALL

	return olReturn;
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::InitializeTable()
{
	CCS_TRY

//--- set background color to white
	pomGrid->GetParam()->GetProperties()->SetColor(GX_COLOR_BACKGROUND, RGB(255,255,255));

	pomGrid->EnableAutoGrow (true);

//--- set col and row count 
    BOOL blTest = pomGrid->SetRowCount(1);
	blTest = pomGrid->SetColCount(3);

	
//--- make header	
	pomGrid->SetColWidth(0, 0, 30);
	pomGrid->SetColWidth(1, 1, 180);
	pomGrid->SetColWidth(2, 2, 180);
	
	pomGrid->SetValueRange(CGXRange(0,1), GetString(1571));
	pomGrid->SetValueRange(CGXRange(0,2), GetString(1572));
	
	pomGrid->HideCols(3, 3);
	pomGrid->GetParam()->EnableMoveCols(false);

//--- make date/time controls
	CString olText = CString ("...\n");
	int ilRowCount = (int)(pomGrid->GetRowCount());
	for (int ilRow = 1; ilRow < ilRowCount + 1; ilRow++)
	{
		pomGrid->SetStyleRange(CGXRange(ilRow, 1), CGXStyle().SetControl(GX_IDS_CTRL_DATETIME)
					 .SetUserAttribute(GX_IDS_UA_CUSTOMFORMAT, "dd.MM.yyyy"));
		pomGrid->SetStyleRange(CGXRange(ilRow, 2), CGXStyle().SetControl(GX_IDS_CTRL_DATETIME) 
		    		 .SetUserAttribute(GX_IDS_UA_CUSTOMFORMAT, "dd.MM.yyyy"));
	}

//--- disable numbering, col resizing, row moving
	pomGrid->GetParam()->SetNumberedColHeaders(FALSE);                 
    pomGrid->GetParam()->EnableTrackRowHeight(0); 
    pomGrid->GetParam()->EnableMoveRows(false);
	pomGrid->GetParam()->SetNumberedColHeaders(FALSE);                 
	pomGrid->GetParam()->SetNumberedRowHeaders(FALSE); 

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::SetStaticTexts()
{
	SetWindowLangText ( this, IDS_VALIDITY_TXT );

	SetDlgItemLangText ( this, IDOK, IDS_OK );
	SetDlgItemLangText ( this, IDCANCEL, IDS_CANCEL );

	SetDlgItemLangText ( this, IDC_DETAILS_BTN, IDS_DETAILS );

	SetDlgItemLangText ( this, IDC_VATO_STATIC, IDS_VALTO );
	SetDlgItemLangText ( this, IDS_VAFR_STATIC, IDS_VALFROM );
	SetDlgItemLangText ( this, IDC_UNLIMITED_CHK, IDS_UNLIMITED );

	SetDlgItemLangText ( this, IDC_MONDAY_CHB, IDS_MONDAY );
	SetDlgItemLangText ( this, IDC_TUESDAY_CHB, IDS_TUESDAY );
	SetDlgItemLangText ( this, IDC_WEDNESDAY_CHB, IDS_WEDNESDAY );
	SetDlgItemLangText ( this, IDC_THURSDAY_CHB, IDS_THURSDAY );
	SetDlgItemLangText ( this, IDC_FRIDAY_CHB, IDS_FRIDAY );
	SetDlgItemLangText ( this, IDC_SATURDAY_CHB, IDS_SATURDAY );
	SetDlgItemLangText ( this, IDC_SUNDAY_CHB, IDS_SUNDAY );

	SetDlgItemLangText ( this, IDC_INVERT_BTN, IDS_INVERT_SELECTION );

	SetDlgItemLangText ( this, IDC_FROM_STATIC, IDS_VON );
	SetDlgItemLangText ( this, IDC_UNTIL_STATIC, IDS_BIS );

	SetDlgItemLangText ( this, IDC_FIXED_BTN, IDS_FESTE_ZEITEN_AN );
}
//-----------------------------------------------------------------------------------------------------------------------

bool LoadValidity( CString &ropUval, VALDATA &ropValData ) 
{
	bool blReturn = true;
	CString olFreq;
	CString olExclBegin;
	CString olExclEnd;

	CCS_TRY

	CCSPtrArray <RecordSet> olValRecs;
	ogBCD.GetRecords("VAL", "UVAL", ropUval, &olValRecs);
	int ilSize = olValRecs.GetSize();
	if ( ilSize<= 0 )
		return false;
	olValRecs.Sort(&ValComp);		//  Validity-Datens�tze sortieren
	
	RecordSet olRecord(ogBCD.GetFieldCount("VAL"));

	EXCLUDES rlExcl;
	ropValData.Excls.DeleteAll();

	for (int i = 0; i < ilSize; i++)
	{
		if ( i==0 )			//  Vafr vom 1. Datensatz �bernehmen
		{
			strcpy(ropValData.Vafr, olValRecs[i].Values[igValVafrIdx] );
			olRecord = olValRecs[i];
		}
		else						//  sp�ter
		{
			olExclEnd = olValRecs[i].Values[igValVafrIdx];

			rlExcl.FirstDay = DBStringToDateTime(olExclBegin);
			rlExcl.FirstDay += CTimeSpan(0, 0, 0, 1);	// last valid time + 1 sec = first excluded time
			rlExcl.LastDay = DBStringToDateTime(olExclEnd);
			rlExcl.LastDay -= CTimeSpan(0, 0, 0, 1);	// first valid time - 1 sec = last excluded time
			ropValData.Excls.New(rlExcl);
		}
		olExclBegin = olValRecs[i].Values[igValVatoIdx];
	}
	olValRecs.DeleteAll();
		
	strcpy(ropValData.Vato, olExclBegin);	//  letzter Vato-Eintrag wird Vato

	// FREQ
	olFreq = olRecord.Values[igValFreqIdx];
	if (olFreq.IsEmpty() == TRUE || olFreq == CString(" "))
	{
		olFreq = CString("1111111");
	}
	strcpy(ropValData.Freq, olFreq);
	
	// FIXT, FIXF
	strcpy(ropValData.FixT, olRecord.Values[igValTimtIdx]);
	strcpy(ropValData.FixF, olRecord.Values[igValTimfIdx]);

	// rest
	ropValData.IsFilled = true;

	CCS_CATCH_ALL

	return blReturn;
}

//-----------------------------------------------------------------------------------------------------------------------

bool SaveValidity(CString &ropUval, VALDATA &ropValData ) 
{
	bool blReturn=true ;

	CCS_TRY
	RecordSet olRecord(ogBCD.GetFieldCount("VAL"));


	blReturn = DeleteByWhere("VAL", "UVAL", ropUval, false );
	
	olRecord.Values[igValUvalIdx] = ropUval;
	//olRecord.Values[igValApplIdx] = "RULE_AFT";
	olRecord.Values[igValTimtIdx] = ropValData.FixT;
	olRecord.Values[igValTimfIdx] = ropValData.FixF;
	
	// freq
	CString olFreq;
	olFreq = ropValData.Freq;
	if ( olFreq.IsEmpty() || (olFreq==" ") ) 
	{
		olFreq = CString("1111111");
	}
	olRecord.Values[igValFreqIdx] = olFreq;

//--- get exclusion periods and create VAL records
	CTime olValidFrom;
	CTime olValidTo;
	
	int ilCount = ropValData.Excls.GetSize();
	int i;
	olRecord.Values[igValTabnIdx] = "PRO";
	olRecord.Values[igValApplIdx] = "PROFILE";
	if (ilCount > 0)
	{
		for (i = 0; i <= ilCount; i++)	//hag991207 
		{
			if ( i<ilCount )
			{
				olValidTo = ropValData.Excls[i].FirstDay;
				olValidTo -= CTimeSpan(0, 0, 0, 1); 
				olRecord.Values[igValVatoIdx] = CTimeToDBString(olValidTo, olValidTo);
			}
			else
				olRecord.Values[igValVatoIdx] = ropValData.Vato;
			if ( i==0 ) 
				olRecord.Values[igValVafrIdx] = ropValData.Vafr;
			else
			{
				olValidFrom = ropValData.Excls[i - 1].LastDay;
				olValidFrom += CTimeSpan(0, 0, 0, 1);
				olRecord.Values[igValVafrIdx] = CTimeToDBString(olValidFrom, olValidFrom);
			}
				
			//  urno leer setzten, sonst werden alle Val-Datens�tze mit der 
			//  gleichen Urno gespeichert !!!      hag991207
			olRecord.Values[igValUrnoIdx] = "";
			blReturn &= ogBCD.InsertRecord("VAL", olRecord, false);
		}
	}
	else
	{
		olRecord.Values[igValVafrIdx] = ropValData.Vafr;
		olRecord.Values[igValVatoIdx] = ropValData.Vato;
		for ( int j=0; j<ogBCD.GetFieldCount("VAL"); j++ )
		{
			CString olStr;
			olStr = olRecord[j];
			int len = olStr.GetLength();
		}
		blReturn &= ogBCD.InsertRecord("VAL", olRecord, false);
	}
	
	if ( !blReturn )
	{
		ogLog.Trace("DEBUG", "[CRulesFormView::SaveValidity] Failed appending a VAL record to object.");
	}
	else
	{	// save VAL records to database
		if (!ogBCD.Save("VAL"))
		{
			blReturn = false;
			ogLog.Trace("DEBUG","Failed saving VAL records to database");
		}
	}
	
	CCS_CATCH_ALL
	
	return blReturn;
}

//-----------------------------------------------------------------------------------------------------------------------
/*
CTimeSpan GetUrnoTimeTillExpiration ( CString &ropUval )
{
	CTimeSpan	olDiff(20000,0,0,0);
	CString		olVato;		

	CCSPtrArray <RecordSet> olValArray;
	ogBCD.GetRecords("VAL", "UVAL", ropUval, &olValArray);
	
	int ilSize = olValArray.GetSize();
	if (ilSize > 0)
	{
		olValArray.Sort(&ValComp);
		olVato = olValArray[ilSize - 1].Values[igValVatoIdx];
		olDiff = GetVatoTimeTillExpiration ( olVato );
	}
	olValArray.DeleteAll();

	return olDiff;
}
//-----------------------------------------------------------------------------------------------------------------------
	
CTimeSpan GetVatoTimeTillExpiration ( CString &ropVato )
{
	CTime		olNow, olExpiration;
	CTimeSpan	olDiff(20000,0,0,0);

	olNow = CTime::GetCurrentTime ();
	olExpiration = DBStringToDateTime(ropVato);
	if ( olExpiration != TIMENULL )
		olDiff = olExpiration - olNow;
	return olDiff;
}
//-----------------------------------------------------------------------------------------------------------------------

bool CheckForExpirationToWarn ()
{
	int ilCount ;
	RecordSet olRueRecord;
	int ilRust ;
	CString	olRueDBVato;
	CTimeSpan olTimeSpan0(0,0,0,0);
	CTimeSpan olTimeTillExpiraton;

	UINT  ilWarnBevor = GetPrivateProfileInt ( ogAppName, "EXPIRATION_WARNING",
											   0, pcgConfigPath);
	if ( ilWarnBevor == 0 )
		return false;
	ilCount = ogBCD.GetDataCount("RUE");
	if ( ilCount == 0 )
		return false;
	CTimeSpan olWarnBevor(ilWarnBevor, 0, 0, 0 );
			
	for (int j = 0; j < ilCount; j++)
	{	
		ogBCD.GetRecord("RUE", j, olRueRecord);
					
		// wenn rule status ==  archiviert, Regel nicht untersuchen
		ilRust = atoi(olRueRecord.Values[igRueRustIdx]);
		if ( ilRust == 2 )
			continue;

		olTimeTillExpiraton = GetUrnoTimeTillExpiration ( olRueRecord.Values[igRueUrnoIdx] );
		if ( ( olTimeTillExpiraton > olTimeSpan0 )	&&  //  noch nicht abgelaufen und
			 ( olTimeTillExpiraton <= olWarnBevor ) )	//  l�uft in den n�chsten 
			return true;								//  ilWarnBevor Tagen aus
	}
	return false;
}
//-----------------------------------------------------------------------------------------------------------------------

COleDateTime CTimeToCOleDateTime(CTime &opTime)
{
	COleDateTime olTime;
	olTime.SetStatus(COleDateTime::invalid);
	if(opTime != -1)
	{
		olTime = COleDateTime(opTime.GetYear(), opTime.GetMonth(), opTime.GetDay(), opTime.GetHour(), opTime.GetMinute(), opTime.GetSecond());
	}
	return olTime;
}
*/
//-----------------------------------------------------------------------------------------------------------------------

COleDateTime DBStringToOleDateTime(CString &opString)
{
	int ilYear, ilMonth, ilDay, ilHour, ilMin, ilSec;
	COleDateTime olTime;
	olTime.SetStatus(COleDateTime::invalid);

	if(opString.GetLength() < 14)
		return olTime;


	ilYear = atoi(opString.Left(4));
	ilMonth = atoi(opString.Mid(4,2));
	ilDay = atoi(opString.Mid(6,2));
	ilHour = atoi(opString.Mid(8,2));
	ilMin =  atoi(opString.Mid(10,2));
	ilSec =  atoi(opString.Mid(12,2));

	if(!((ilYear >= 1900) && (ilYear <= 2200)))
		return olTime;

	if(!((ilMonth >= 1) && (ilMonth <= 12)))
		return olTime;
	
	if(!((ilDay >= 1) && (ilDay <= 31)))
		return olTime;
	
	if(!((ilHour >= 0) && (ilHour <= 23)))
		return olTime;
	
	if(!((ilMin >= 0) && (ilMin <= 60)))
		return olTime;
	
	if(!((ilSec >= 0) && (ilSec <= 60)))
		return olTime;
	
	return  COleDateTime::COleDateTime(ilYear, ilMonth, ilDay, ilHour, ilMin, ilSec);
}
//-----------------------------------------------------------------------------------------------------------------------

void ValidityDlg::OnUnlimitedChk() 
{
	// TODO: Add your control notification handler code here
	UpdateData ();
	m_DateToCtrl.EnableWindow(!m_bUnlimited);
	m_TimeToCtrl.EnableWindow(!m_bUnlimited);
}
//-----------------------------------------------------------------------------------------------------------------------

bool ValidityDlg::CheckExclIsValid(CTime opTestVal, int ipRow, int ipCol)
{
	bool blRet = true;

	CCS_TRY


	// olTestVal must not be out of validity range (vafr - vato)
	CTime olVafr, olDafr, olTifr;
	CTime olVato, olDato, olTito;

	m_DateFromCtrl.GetTime(olDafr);
	m_TimeFromCtrl.GetTime(olTifr);
	m_DateToCtrl.GetTime(olDato);
	m_TimeToCtrl.GetTime(olTito);

	olVafr = CTime(olDafr.GetYear(), olDafr.GetMonth(), olDafr.GetDay(), olTifr.GetHour(), olTifr.GetMinute(), 0);
	olVato = CTime(olDato.GetYear(), olDato.GetMonth(), olDato.GetDay(), olTito.GetHour(), olTito.GetMinute(), 0);

	if ((opTestVal <= olVafr) || ((opTestVal >= olVato) && (m_bUnlimited == false)))
	{
		blRet = false;
	}


	// olTestVal must not lie within any other exclude period
	CString olStartVal, olEndVal;
	CTime olStartExcl, olEndExcl;

	int ilRowCount = (int)pomGrid->GetRowCount();
	for (int i = 1; i <= ilRowCount; i++)
	{
		if (i != ipRow)	// don't compare value with itself
		{
			olStartVal = pomGrid->GetValueRowCol(i, 1);
			olEndVal = pomGrid->GetValueRowCol(i, 2);

			if (olStartVal.IsEmpty() == FALSE)
			{
				DateStringToDateExt(olStartVal, olStartExcl);
				DateStringToDateExt(olEndVal, olEndExcl);
			
				if ((opTestVal >= olStartExcl) && (opTestVal <= olEndExcl))
				{
					blRet = false;
				}
			}
		}
	}


	// end date has to be later than start date
	if (ipCol == 2)
	{
		olStartVal = pomGrid->GetValueRowCol(ipRow, 1);

		olStartVal = pomGrid->GetValueRowCol(i, 1);

		if (olStartVal.IsEmpty() == FALSE)
		{
			DateStringToDateExt(olStartVal, olStartExcl);
			if (opTestVal <= olStartExcl)
			{
				blRet = false;
			}
		}
	}


	CCS_CATCH_ALL

	return blRet;
}
//-----------------------------------------------------------------------------------------------------------------------

// sorts exclude grid by start date
void ValidityDlg::SortExcludes()
{
	int ilRowCount = pomGrid->GetRowCount();

	CString olText;
	CTime olTime;

//--- set sorting key
	for (int ilRow = 1; ilRow <= ilRowCount; ilRow++)
	{
		olText = pomGrid->GetValueRowCol(ilRow, 1);
		if (olText.IsEmpty() == FALSE)
		{
			DateStringToDateExt(olText, olTime);
			olText.Format("%04d%02d%02d", olTime.GetYear(), olTime.GetMonth(), olTime.GetDay());
		}
		else
		{
			olText = CString("99999999");
		}

		pomGrid->SetValueRange(CGXRange(ilRow, 3), olText);
	}

	
//--- sort grid
	CGXSortInfoArray rlSortInfo;
	rlSortInfo.SetSize(1);       // 1 key only
			
	rlSortInfo[0].nRC = 3;		 // column 3 is the key
	rlSortInfo[0].bCase = FALSE; // not case sensitive
	rlSortInfo[0].sortType = CGXSortInfo::numeric;   
	rlSortInfo[0].sortOrder = CGXSortInfo::ascending;

	pomGrid->m_nTopRow = (ROWCOL) 1;
	pomGrid->SortRows(CGXRange().SetTable(), rlSortInfo);
	pomGrid->Redraw();


}

void ValidityDlg::SetFixedTimesControls ()
{
	m_TimeStatic.EnableWindow(bmFixedTimes);
	m_TimeStatic2.EnableWindow(bmFixedTimes);
	m_TimeStatic3.EnableWindow(bmFixedTimes);
	m_TimeStatic4.EnableWindow(bmFixedTimes);
	m_FromSlider.EnableWindow(bmFixedTimes);
	m_FixTimeFrom.EnableWindow(bmFixedTimes);
	m_FromFrame.EnableWindow(bmFixedTimes);
	m_UntilSlider.EnableWindow(bmFixedTimes);
	m_FixTimeTo.EnableWindow(bmFixedTimes);
	m_UntilFrame.EnableWindow(bmFixedTimes);
	m_FixedBtn.SetWindowText(GetString(bmFixedTimes?1425:1424));
}


//-----------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------
//					statics, globals, defines, etc
//-----------------------------------------------------------------------------------------------------------------------
int ValComp(const RecordSet **ppDateStr1, const RecordSet **ppDateStr2)
{
	int ilValVafrIdx = ogBCD.GetFieldIndex("VAL","VAFR" );
	int ilCompareResult = (((**ppDateStr1).Values[ilValVafrIdx]) == ((**ppDateStr2).Values[ilValVafrIdx]))? 0:
                (((**ppDateStr1).Values[ilValVafrIdx])  > ((**ppDateStr2).Values[ilValVafrIdx]) )? 1: -1;

	return ilCompareResult;
}

//-----------------------------------------------------------------------------------------------------------------------

void CopyValData(VALDATA *prpDestination, VALDATA *prpSource)
{
	EXCLUDES rlExcl;
	
	int ilCount = prpSource->Excls.GetSize();

	prpDestination->Excls.DeleteAll();
	for (int i = 0; i < ilCount; i++)
	{
		rlExcl = prpSource->Excls[i];
		prpDestination->Excls.New(rlExcl);

		CTime test1 = rlExcl.FirstDay;
		CTime test2 = rlExcl.LastDay;
	}

	strcpy(prpDestination->FixF, prpSource->FixF);
	strcpy(prpDestination->FixT, prpSource->FixT);
	strcpy(prpDestination->Freq, prpSource->Freq);
	strcpy(prpDestination->Vafr, prpSource->Vafr);
	strcpy(prpDestination->Vato, prpSource->Vato);
	
	prpDestination->IsFilled = prpSource->IsFilled;   
}


int DoValidityDlg ( CWnd *popParent, VALDATA &rrpData, bool bpNew )
{
	ValidityDlg olDlg(bpNew, popParent);

	olDlg.SetValData(&rrpData);

	// open validity dialog
	int ilRet = olDlg.DoModal();
	if (ilRet == IDOK)
		olDlg.GetValData(rrpData);
	return ilRet;
}
