// ProfileEditorDoc.cpp : implementation of the CProfileEditorDoc class
//

#include <stdafx.h>

#include <cedabasicdata.h>
#include <ccsglobl.h>
#include <ProfileEditor.h>
#include <ChartData.h>
#include <mainfrm.h>
#include <utilities.h>
#include <basicdata.h>

#include <ProfileEditorDoc.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CProfileEditorDoc

IMPLEMENT_DYNCREATE(CProfileEditorDoc, CDocument)

BEGIN_MESSAGE_MAP(CProfileEditorDoc, CDocument)
	//{{AFX_MSG_MAP(CProfileEditorDoc)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProfileEditorDoc construction/destruction

CProfileEditorDoc::CProfileEditorDoc()
{
	// TODO: add one-time construction code here
	imProDcopIdx = ogBCD.GetFieldIndex ( "PRO","DCOP" );
	imProDcupIdx = ogBCD.GetFieldIndex ( "PRO","DCUP" );
	imProDgapIdx = ogBCD.GetFieldIndex ( "PRO","DGAP" );
	imProDsepIdx = ogBCD.GetFieldIndex ( "PRO","DSEP" );
	imProEvtdIdx = ogBCD.GetFieldIndex ( "PRO","EVTD" );
	imProFdcoIdx = ogBCD.GetFieldIndex ( "PRO","FDCO" );
	imProFdcuIdx = ogBCD.GetFieldIndex ( "PRO","FDCU" );
	imProFdgaIdx = ogBCD.GetFieldIndex ( "PRO","FDGA" );
	imProFdseIdx = ogBCD.GetFieldIndex ( "PRO","FDSE" );
	imProFisuIdx = ogBCD.GetFieldIndex ( "PRO","FISU" );
	imProNameIdx = ogBCD.GetFieldIndex ( "PRO","NAME" );
	imProPrioIdx = ogBCD.GetFieldIndex ( "PRO","PRIO" );
	imProPrstIdx = ogBCD.GetFieldIndex ( "PRO","PRST" );
	imProRemaIdx = ogBCD.GetFieldIndex ( "PRO","REMA" );
	imProUarcIdx = ogBCD.GetFieldIndex ( "PRO","UARC" );
	imProUrnoIdx = ogBCD.GetFieldIndex ( "PRO","URNO" );
	imProUtplIdx = ogBCD.GetFieldIndex ( "PRO","UTPL" );
	imProExclIdx = ogBCD.GetFieldIndex ( "PRO","EXCL" );
	imModifications = NO_CHANGE;

//	RegisterBC ( this, "CProfileEditorDoc", "PRO", ProcessBCInDoc, BC_CHANGED|BC_DELETED );
//	RegisterBC ( this, "CProfileEditorDoc", "PXC", ProcessBCInDoc, BC_ALL );
//	RegisterBC ( this, "CProfileEditorDoc", "PXA", ProcessBCInDoc, BC_CHANGED|BC_DELETED );
	bmValid = true;
	bmDeletedLocal = false;
}

CProfileEditorDoc::~CProfileEditorDoc()
{
	ogDdx.UnRegister(this, NOTUSED);
	omChartData.DeleteAll();
}

BOOL CProfileEditorDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	CString olDocString, olUrno, olTitle = m_strTitle;
	CDocTemplate* polTemplate = GetDocTemplate() ;
	static int ilIdx=1;
	bool blNew = false;

	if ( polTemplate && 
		 polTemplate->GetDocString(olDocString, CDocTemplate::docName) )
	{
		while ( ( ogBCD.GetField( "PRO", "NAME", olTitle, "URNO", olUrno ) ||
				theApp.FindDocument(olTitle ) )
				&& (ilIdx<10000) )
		{	//  Profilename is already used in DB or open window
			olTitle.Format ( "%s%d", olDocString, ilIdx++ );
			blNew = true;
		}
		if ( blNew && ( ilIdx <10000 ) )
			SetTitle ( olTitle );
	}
	// (SDI documents will reuse this document)
	omDcop = omDcup = omDgap = omDsep = "0.0";
	bmFdco = bmFdcu = bmFdga = bmFdse = false;
	omEvtd = omRemark = omUrno = omPrio = omUarc = "";
	
	omPrst = "1";
	omExcl = "0";
	CMainFrame *polFrm = (CMainFrame*)(theApp.m_pMainWnd);
	if ( polFrm )
		CopyValData(&rmValData, &(polFrm->rmValData) );
	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CProfileEditorDoc serialization

void CProfileEditorDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CProfileEditorDoc diagnostics

#ifdef _DEBUG
void CProfileEditorDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CProfileEditorDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CProfileEditorDoc commands
bool CProfileEditorDoc::AddClass ( CView *popSender, long lpClassUrno, 
								   bool bpSendUpdate/*=true*/ )
{
	if ( FindClass(lpClassUrno ) >= 0 )
		return false;	//  Counterclass already in use
	CChartData *polData;
	polData = new CChartData(this);
	polData->lmCounterClass = lpClassUrno;
	omChartData.Add ( polData );
	
	SetModified (PXC_CHANGED);
	CDWordArray olHintObject;
	olHintObject.Add (lpClassUrno);
	if ( bpSendUpdate )
		UpdateAllViews ( popSender, HINT_CLASS_CREATED, &olHintObject );

	return true;
}

bool CProfileEditorDoc::DeleteClass ( CView *popSender, long lpClassUrno )
{
	int ilIdx = FindClass(lpClassUrno ) ;
	if ( ilIdx>= 0 )	//  Counterclass found
	{
		omChartData.DeleteAt ( ilIdx );
		SetModified (PXC_CHANGED);

		CDWordArray olHintObject;
		olHintObject.Add (lpClassUrno);
		UpdateAllViews ( popSender, HINT_CLASS_DELETED, &olHintObject );
	}
	return ( ilIdx>= 0 );
}


int CProfileEditorDoc::FindClass ( long lpClassUrno )
{
	int ilRet = -1;

	for ( int i=0; (ilRet==-1)&&( i<omChartData.GetSize() ); i++ )
	{
		if ( omChartData[i].lmCounterClass == lpClassUrno )
			ilRet = i;
	}
	return ilRet;
}

bool CProfileEditorDoc::SetOneDistribution ( CView *popSender, char *pcpDBField,
											 bool bpValFlag, float fpVal )
{
	bool	*pblFlag=0;
	CString *polValue=0, olStr;
	
	if ( !stricmp ( pcpDBField, "DCOP" ) )
	{
		//omDcop.Format ( "%.2lf", fpVal );
		//bmFdco = bpValFlag;
		polValue = &omDcop;
		pblFlag = &bmFdco;
	}
	else
		if ( !stricmp ( pcpDBField, "DCUP" ) )
		{
			//omDcup.Format ( "%.2lf", fpVal );
			//bmFdcu = bpValFlag;
			polValue = &omDcup;
			pblFlag = &bmFdcu;
		}
	else
		if ( !stricmp ( pcpDBField, "DGAP" ) )
		{
			//omDgap.Format ( "%.2lf", fpVal );
			//bmFdga = bpValFlag;
			polValue = &omDgap;
			pblFlag = &bmFdga;
		}
	else
		if ( !stricmp ( pcpDBField, "DSEP" ) )
		{
			//omDsep.Format ( "%.2lf", fpVal );
			//bmFdse = bpValFlag;
			polValue = &omDsep;
			pblFlag = &bmFdse;
		}
	if ( polValue && pblFlag )
	{
		float flValue;
		if ( (sscanf(*polValue, "%f", &flValue)<1) || 
			 ( fabs(flValue-fpVal)>0.001 ) || (*pblFlag!=bpValFlag) )
		{	//  Werte ge�ndert
			olStr.Format ( "%.2lf", fpVal );
			*polValue = olStr.Left(5);	/*hag010607: 100.00 -> 100.0*/
			*pblFlag=bpValFlag;
			SetModified (PRO_CHANGED);
			CStringArray olHintObject;
			olHintObject.Add ( pcpDBField );
			UpdateAllViews ( 0, HINT_HEADER_CHANGED, 0 );
		}
		return true;
	}
	return false;
}

bool CProfileEditorDoc::SetSingleDBField ( CView *popSender, CString opField, 
										   CString opValue )
{
	bool blRet = true, blChanged=false, blSendUpd =true;
	CString *polString=0;

	if ( ( opField =="NAME" )&& ( opValue!=GetTitle() ) )
	{
		SetTitle(opValue);
		blChanged = true;
	}
	else
	{
		if ( opField=="REMA" )
			polString = &omRemark;
		else 
			if ( opField =="PRST" )
				polString = &omPrst;
		else 
			if ( opField =="EVTD" )
				polString = &omEvtd ;
		else
			if ( opField =="EXCL" )
				polString = &omExcl ;
		else
			if ( opField =="PRIO" )
			{
				polString = &omPrio  ;
				blSendUpd = false;	//  kein UpdateAllViews, da Prio 
			}						//  nirgends angezeigt wird

		if ( polString )
		{
			if ( *polString!=opValue )
			{
				*polString=opValue ;
				blChanged=true;
			}
		}
		else	//  Feld unbekannt !
			blRet = false;
	}
	if ( blRet && blChanged )
	{
		SetModified(PRO_CHANGED);
		if ( blSendUpd )
			UpdateAllViews ( popSender, HINT_HEADER_CHANGED, 0 );
	}
	return blRet;
}

//  CProfileEditorDoc::SaveModified
//	�berschreibt CDocument::SaveModified 
//	wird aufgerufen, wenn Document bzw. FrameWnd mit Document geschlossen werden soll
BOOL CProfileEditorDoc::SaveModified() 
{
	// TODO: Add your specialized code here and/or call the base class
	SendMessageToAllViews (WM_PREPARE_CLOSING);
	if (!IsModified())
		return TRUE;        // ok to continue
	CString olTitle = GetTitle();
	if ( olTitle.IsEmpty () )
	{
		//AfxMessageBox ( "Error: Profile name is empty!", MB_OK );
		LangMsgBox ( 0, IDS_NO_PROFILE_NAME );
		return FALSE;
	}
	CString olText = GetString ( IDS_ASK_TO_SAVE );
	olText.Replace ( "%1", olTitle );
	switch ( AfxMessageBox(olText, MB_ICONQUESTION|MB_YESNOCANCEL) )
	{
		case IDCANCEL:
			return FALSE;       // don't continue

		case IDYES:
			// If so, either Save or Update, as appropriate
			if (!DoFileSave())
				return FALSE;       // don't continue
			break;

		case IDNO:
			// If not saving changes, revert the document
			break;

		default:
			ASSERT(FALSE);
			break;
	}
	return TRUE;    // keep going
}

//  CProfileEditorDoc::DoFileSave
//	�berschreibt CDocument::DoFileSave 
//	wird aufgerufen, wenn Document geschlossen werden soll 
//	oder �ber Menupunkt ID_FILE_SAVE
BOOL CProfileEditorDoc::DoFileSave()
{
	CString olName = GetTitle();
	bool    blUpdate=false;
	BOOL    blRet;
	int		i;

	SendMessageToAllViews (WM_KILLFOCUS);

	if ( olName.IsEmpty() )
	{
		//AfxMessageBox ( "Error: Profile name is empty!", MB_OK );
		LangMsgBox ( 0, IDS_NO_PROFILE_NAME );
		return FALSE;
	}
	//  Update der PRO-, PXC- oder VAL-Tabelle, Test ob Profile-Fisu-Flag ==1
	//  dann mu� Original-Profile archiviert werden
	if ( ( imModifications & (PRO_CHANGED|PXC_CHANGED|VAL_CHANGED) ) && 
		 !omUrno.IsEmpty() )
	{
		CString olFisu;
		olFisu = ogBCD.GetField ( "PRO", "URNO", omUrno, "FISU" );
		if ( olFisu == "1" )	//  Profile is used ?
		{
			ogBCD.SetField("PRO", "URNO", omUrno, "PRST", "2");		//  archivieren
			omUarc = omUrno;				//  Urno des Originals merken
			omUrno.Empty();					//  Urno leer, d.h. neues Profil
			SetModified ( PRO_CHANGED|PXC_CHANGED|VAL_CHANGED );

			//  damit alle PXC-s�tze gespeichert werden
			for ( i=0; i<omChartData.GetSize(); i++ )	
				omChartData[i].SetModified (PXC_CHANGED);	 
		}
	}
	
	blUpdate = !omUrno.IsEmpty();

	CCSPtrArray<RecordSet> olSameNameRecs; 
	ogBCD.GetRecords ( "PRO", "NAME", olName, &olSameNameRecs );
	for ( i=0; i<olSameNameRecs.GetSize(); i++ )
//	if ( ogBCD.GetField ( "PRO", "NAME", olName, "URNO", olUrno ) )
	{	//  Name bereits vergeben, wenn urno != aktueller urno und 
		//	Status != archiviert 
		if ( (olSameNameRecs[i].Values[imProPrstIdx] != "2") && 
			 (olSameNameRecs[i].Values[imProUrnoIdx] != omUrno ) )
		{
			LangMsgBox ( 0, IDS_DUPL_PROFILE_NAME );
			olSameNameRecs.DeleteAll();
			return FALSE;
		}
	}
	olSameNameRecs.DeleteAll();

	blRet = SaveProRecord (blUpdate) ;
	if ( blRet )
	{
		if ( !blUpdate || (imModifications&VAL_CHANGED) )
		{
			if ( !SaveValidity( omUrno, rmValData ) )
			{
				//olText = "Error on saving validity for profile %s.";
				//olText.Replace ( "%s", olName );
				//AfxMessageBox ( olText, MB_OK );
				FileErrMsg ( 0, IDS_SAVE_VALIDIY_ERR, pCHAR(olName) );
			}
		}
	}
	if ( blRet )
		blRet = SaveClasses ();
	if ( blRet )
		SetModified ( NO_CHANGE );
	return blRet;
}

bool CProfileEditorDoc::SaveProRecord (bool bpUpdate)
{
	bool	  blOk;

	//  Update eines bestehenden Datensatzes, aber nichts im PRO-Record ge�ndert
	if ( bpUpdate && !(imModifications&PRO_CHANGED) )
		return true;
	RecordSet olProRec(ogBCD.GetFieldCount("PRO"));
	//  alle Felder leer vorbesetzen
	for ( int i=0; i<olProRec.Values.GetSize(); i++ )
		olProRec.Values[i].Empty();

	if ( !bpUpdate )
	{
		if ( !omUrno.IsEmpty () )
			TRACE ( "Warning: Urno isn't empty \n" );
		omUrno.Format ( "%lu", ogBCD.GetNextUrno() );
	}
	olProRec.Values[imProUrnoIdx] = omUrno;
	olProRec.Values[imProFdcoIdx] = bmFdco ? "1" : "0";
	if ( bmFdco )
		olProRec.Values[imProDcopIdx] = omDcop ;
	olProRec.Values[imProFdcuIdx] = bmFdcu ? "1" : "0";
	if ( bmFdcu )
		olProRec.Values[imProDcupIdx] = omDcup;
	olProRec.Values[imProFdgaIdx] = bmFdga ? "1" : "0";
	if ( bmFdga )
		olProRec.Values[imProDgapIdx] = omDgap;
	olProRec.Values[imProFdseIdx] = bmFdse ? "1" : "0";
	if ( bmFdse )
		olProRec.Values[imProDsepIdx] = omDsep;
	olProRec.Values[imProRemaIdx] = omRemark;
	olProRec.Values[imProNameIdx] = GetTitle();
	olProRec.Values[imProEvtdIdx] = omEvtd;
	olProRec.Values[imProPrstIdx] = omPrst;
	olProRec.Values[imProUtplIdx] = ogTPLUrno;
	if (imProExclIdx>=0)	//  da das Feld erst sp�ter dazukam
		olProRec.Values[imProExclIdx] = omExcl;

	olProRec.Values[imProPrioIdx] = omPrio;

	olProRec.Values[imProUarcIdx] = omUarc;
	olProRec.Values[imProFisuIdx] = "0";	//  hier gespeicherte Profile sind 
											//  sind immer not used

	SetPathName ( GetTitle() );
	if ( bpUpdate )
		blOk = ogBCD.SetRecord ( "PRO", "URNO", omUrno, olProRec.Values, true );
	else
		blOk = ogBCD.InsertRecord( "PRO", olProRec, true );
	if ( blOk )
		InternalBC ( this, "PRO", bpUpdate?BC_CHANGED:BC_NEW, &olProRec );
	return blOk;
}
	

bool CProfileEditorDoc::SaveClasses ()
{
	int				i;
	bool			blOk=true;
	CString			olUsedPXCUrnos, olActPXCUrno;
	CStringArray	olPXCUrnos;	

	//  liste aller Urnos der PXC-Records f�r dieses Profil anlegen
	if ( !omUrno.IsEmpty () )
		olUsedPXCUrnos = ogBCD.GetValueList("PXC", "UPRO", omUrno, "URNO");

	for ( i=0; i<omChartData.GetSize(); i++ )
	{
		blOk &= omChartData[i].SaveModified ();
		olActPXCUrno = omChartData[i].omUrno;
		//  Urno des PXC-Records aus Liste l�schen, da weiterhin vorhanden
		DeleteListItem ( olUsedPXCUrnos, olActPXCUrno );
	}

	ExtractItemList ( olUsedPXCUrnos, &olPXCUrnos );
	//  PXC-Records, deren Urno jetzt noch in der Liste ist, l�schen
	for ( i=0; i<olPXCUrnos.GetSize(); i++ )
		blOk &= ogBCD.DeleteRecord  ( "PXC", "URNO", olPXCUrnos[i], true );
	return blOk;
}

void CProfileEditorDoc::SetPathName(LPCTSTR lpszPathName, BOOL bAddToMRU)
{
	// store the path fully qualified
	m_strPathName = lpszPathName;
	ASSERT(!m_strPathName.IsEmpty());       // must be set to something
	m_bEmbedded = FALSE;
	ASSERT_VALID(this);

	// set the document title based on path name
	SetTitle(lpszPathName);

	// add it to the file MRU list
	if (bAddToMRU)
		AfxGetApp()->AddToRecentFileList(m_strPathName);

	ASSERT_VALID(this);
}

bool CProfileEditorDoc::LoadProRecord (CString &ropProfileUrno)
{
	bool	  blOk=true;
	RecordSet olProRec(ogBCD.GetFieldCount("PRO"));
	
	blOk = ogBCD.GetRecord ( "PRO", "URNO", ropProfileUrno, olProRec );
	if ( blOk )
	{
		bmFdco = (olProRec.Values[imProFdcoIdx] == "1" );
		omDcop = bmFdco ? olProRec.Values[imProDcopIdx] : "0.0"  ;

		bmFdcu = (olProRec.Values[imProFdcuIdx] == "1" );
		omDcup = bmFdcu ? olProRec.Values[imProDcupIdx] : "0.0"  ;

		bmFdga = (olProRec.Values[imProFdgaIdx] == "1");
		omDgap = bmFdga ? olProRec.Values[imProDgapIdx] : "0.0"  ;

		bmFdse = (olProRec.Values[imProFdseIdx] == "1");
		omDsep = bmFdse ? olProRec.Values[imProDsepIdx] : "0.0"  ;

		omRemark = olProRec.Values[imProRemaIdx] ;
		SetTitle(olProRec.Values[imProNameIdx]);

		omEvtd = olProRec.Values[imProEvtdIdx];
		omPrst = olProRec.Values[imProPrstIdx];
		omPrio = olProRec.Values[imProPrioIdx];
		
		if (imProExclIdx>=0)	//  da das Feld erst sp�ter dazukam
			omExcl = olProRec.Values[imProExclIdx] ;
		
		//TPLUrno = olProRec.Values[imProUtplIdx] ;
		//  Diese Felder m�ssen noch behandelt werden
		omUarc = olProRec.Values[imProUarcIdx];
	}
	return blOk;
}
	

bool CProfileEditorDoc::LoadClasses (CString &ropProfileUrno)
{
	CCSPtrArray<RecordSet>	olClasses;
	bool					blRet=true;
	
	ogBCD.GetRecords ( "PXC", "UPRO", ropProfileUrno, &olClasses );
	for ( int i = 0; i<olClasses.GetSize(); i++ )
	{
		blRet &= LoadOneClass ( olClasses[i] );
	}
	olClasses.DeleteAll();
	return blRet;
}


bool CProfileEditorDoc::LoadOneClass ( RecordSet &ropPXCRecord )
{
	long		llCCCUrno;
	CChartData	*polData=0;
	RecordSet	olPXARecord;

	if ( sscanf ( ropPXCRecord.Values[ogBCD.GetFieldIndex("PXC","UCCC")],
				  "%lu", &llCCCUrno ) <= 0 )
		return false;
/*	if ( !*/AddClass ( 0, llCCCUrno, false );/* )
		return false;
	else
	{*/
		int ilIdx = FindClass(llCCCUrno ) ;
		if ( ilIdx>= 0 )	//  Counterclass found
			polData = &(omChartData[ilIdx]);
/*	}*/
	if ( !polData )
		return false;

	polData->omUrno = ropPXCRecord.Values[ogBCD.GetFieldIndex("PXC","URNO")];
	polData->omDBAG = ropPXCRecord.Values[ogBCD.GetFieldIndex("PXC","DBAG")];
	polData->omDPAX = ropPXCRecord.Values[ogBCD.GetFieldIndex("PXC","DPAX")];

	if ( igPxcTtbgIdx>=0 ) // New field "TTBG" already in DB
		polData->omTTBG = ropPXCRecord.Values[igPxcTtbgIdx] ;
	if ( igPxcTtwoIdx>=0 ) // New field "TTWO" already in DB
		polData->omTTWO = ropPXCRecord.Values[igPxcTtwoIdx] ;

	if ( sscanf ( ropPXCRecord.Values[ogBCD.GetFieldIndex("PXC","SCXM")], "%d", 
				   &(polData->imDspStep) ) <= 0 )
		polData->imDspStep = 5;
	polData->omUval = ropPXCRecord.Values[ogBCD.GetFieldIndex("PXC","UPXA")];
	bool blOk = polData->ReadData ();
	blOk &= polData->CalcDspData ();

	return blOk;
}

BOOL CProfileEditorDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	bool	blRet = false, blValOk = true;
	//CString	olStr;

	if (IsModified())
		TRACE0("Warning: OnOpenDocument replaces an unsaved document.\n");
	DeleteContents();
	SetModifiedFlag();  // dirty during de-serialize
	//  load PRO-Data set
	//omUrno = ogBCD.GetField ( "PRO", "NAME", lpszPathName, "URNO" );
	omUrno.Empty(); 
	CCSPtrArray<RecordSet> olRecords;
	ogBCD.GetRecords( "PRO", "NAME", lpszPathName, &olRecords );
	for ( int i=0; (i<olRecords.GetSize())&&omUrno.IsEmpty(); i++ )
		if ( olRecords[i].Values[imProPrstIdx] != "2" ) 
			omUrno = olRecords[i].Values[imProUrnoIdx] ;
	olRecords.DeleteAll ();

	if ( !omUrno.IsEmpty() )
	{
		blRet = LoadProRecord(omUrno);
		blRet &= LoadClasses(omUrno);
		if ( !LoadValidity( omUrno, rmValData ) )
		{
			//olStr = "Error on loading validity for profile %s.";
			//olStr.Replace ( "%s", lpszPathName );
			//AfxMessageBox ( olStr, MB_OK );
			FileErrMsg ( 0, IDS_LOAD_VALIDIY_ERR, (char*)lpszPathName );
			CMainFrame *polFrm = (CMainFrame*)(theApp.m_pMainWnd);
			if ( polFrm )
				CopyValData(&rmValData, &(polFrm->rmValData) );
			blValOk = false;
		}
	}
	else
		FileErrMsg ( 0, IDS_PROFILE_NOT_FOUND, (char*)lpszPathName );
	SetModified(NO_CHANGE);     // start off with unmodified
	if ( !blValOk )
		SetModified(VAL_CHANGED);     // start off with unmodified
	return blRet;
}

int CProfileEditorDoc::GetDataDisplayIndex (CChartData *popData)
{
	for ( int i=0; i<omChartData.GetSize(); i++ )
		if ( &(omChartData[i]) == popData )
			return i;
	return -1;
}

bool CProfileEditorDoc::DeleteFromDB ()
{
	bool blRet ;

	if ( omUrno.IsEmpty() )	//  noch nicht in Datenbank gespeichert
		return true;
	CString olFisu = ogBCD.GetField("PRO","URNO", omUrno, "FISU" );
	if ( olFisu=="1" )		//  Profil wird z.Zt. benutzt -> archivieren
		blRet = ogBCD.SetField("PRO","URNO", omUrno, "PRST", "2", true );
	else
	{
		bmDeletedLocal = true;
		blRet = DeleteByWhere("PXC", "UPRO", omUrno );
		if ( blRet )
			blRet = ogBCD.DeleteRecord("PRO", "URNO", omUrno, true );
	}
	if ( !blRet )
	{
		//CString olText = "Error on deleting profile %s";
		//olText.Replace ( "%s", GetTitle () );
		//AfxMessageBox ( olText, MB_OK );
		FileErrMsg ( 0, IDS_DELETE_PROFILE_ERR, pCHAR( GetTitle() ) );
		bmDeletedLocal = false;
	}
	return blRet;
}

bool CProfileEditorDoc::ChangeDPAXandDBAG ( CView *popSender, long lpClassUrno, 
										    CString *popDPAX, CString *popDBAG )
{
	if ( !popDPAX && !popDBAG )
		return true;			//  nothing to do
	int ilIdx = FindClass(lpClassUrno ) ;
	if ( ilIdx>= 0 )	//  Counterclass found
	{
		CChartData *polData;
		polData = &(omChartData.ElementAt ( ilIdx ) );
		if ( popDPAX )
			polData->omDPAX = *popDPAX;
		if ( popDBAG )
			polData->omDBAG = *popDBAG;
		polData->SetModified (PXC_CHANGED);

		//  for Updating other views
		CDWordArray olHintObject;
		olHintObject.Add (lpClassUrno);
		UpdateAllViews ( popSender, HINT_CLASS_HEAD_CHANGED, &olHintObject );
	}
	return ( ilIdx>= 0 );
}

bool CProfileEditorDoc::ChangeTransactionTimes ( CView *popSender, long lpClassUrno, 
												 CString *popTTWO, CString *popTTBG )
{
	if ( !popTTWO && !popTTBG )
		return true;			//  nothing to do
	int ilIdx = FindClass(lpClassUrno ) ;
	if ( ilIdx>= 0 )	//  Counterclass found
	{
		CChartData *polData;
		polData = &(omChartData.ElementAt ( ilIdx ) );
		if ( popTTWO )
			polData->omTTWO = *popTTWO;
		if ( popTTBG )
			polData->omTTBG = *popTTBG;
		polData->SetModified (PXC_CHANGED);

		//  for Updating other views
		CDWordArray olHintObject;
		olHintObject.Add (lpClassUrno);
		UpdateAllViews ( popSender, HINT_CLASS_HEAD_CHANGED, &olHintObject );
	}
	return ( ilIdx>= 0 );
}

void CProfileEditorDoc::SendMessageToAllViews(UINT message, WPARAM wParam/*=0*/,
											  LPARAM lParam/*=0*/)
{
	POSITION pos = GetFirstViewPosition ();
	while ( pos )
	{
		CView *polView = GetNextView ( pos );
		if ( polView )
			polView->SendMessage ( message, 0, 0L );
	}
}
	
void CProfileEditorDoc::SetModified ( UINT ipBits )
{
	SetModifiedFlag (ipBits!=NO_CHANGE);
	if ( ipBits==NO_CHANGE )
		imModifications = ipBits;
	else
		imModifications |= ipBits;
}

void CProfileEditorDoc::UpdClassModifiedBits ()
{
	UINT ilKompl = PXC_CHANGED | PXA_CHANGED | VTP_CHANGED;
	//  Bits, die sich auf Chart-Data beziehen, ausblenden...
	ilKompl = ~ilKompl;
	imModifications &= ilKompl;
	//  ...und f�r alle vorhandenen Klassen erneut pr�fen
	for ( int i=0; i<omChartData.GetSize(); i++ )
	{
		if ( omChartData[i].bmPXAModified )
			imModifications |= PXA_CHANGED;
		if ( omChartData[i].bmVTPModified )
			imModifications |= VTP_CHANGED;
		if ( omChartData[i].bmPXCModified )
			imModifications |= PXC_CHANGED;
	}
	SetModifiedFlag (imModifications!=NO_CHANGE);
}

void CProfileEditorDoc::OnEditCopy() 
{
	// TODO: Add your command handler code here
	CDocTemplate* polTemplate = GetDocTemplate() ;
	CProfileEditorDoc *polNewDoc=0;

	if ( polTemplate )
		polNewDoc = (CProfileEditorDoc*)polTemplate->OpenDocumentFile(0);
	if ( polNewDoc )
	{
		polNewDoc->omDcop = omDcop;
		polNewDoc->bmFdco = bmFdco;
		polNewDoc->omDcup = omDcup;
		polNewDoc->bmFdcu = bmFdcu;
		polNewDoc->omDgap = omDgap;
		polNewDoc->bmFdga = bmFdga;
		polNewDoc->omDsep = omDsep;
		polNewDoc->bmFdse = bmFdse;
		polNewDoc->omEvtd = omEvtd;
		polNewDoc->omPrst = omPrst;
		polNewDoc->omExcl = omExcl;
		polNewDoc->omRemark = omRemark ;
		polNewDoc->omPrio = omPrio ;
		CopyValData(&(polNewDoc->rmValData), &rmValData );
		polNewDoc->UpdateAllViews ( 0, HINT_HEADER_CHANGED, 0 );
		polNewDoc->CopyClasses ( this );
		polNewDoc->SetModified (PRO_CHANGED |PXC_CHANGED);
	}
}

int CProfileEditorDoc::CopyClasses ( CProfileEditorDoc *popSource )
{
	int i; 
	CChartData *polData;
	CDWordArray olHintObject;

	for ( i=0; i<popSource->omChartData.GetSize(); i++ )
	{
		polData = new CChartData(this);
		*polData = popSource->omChartData[i];
		polData->pomDocument = this;
		omChartData.Add ( polData );
	
		olHintObject.Add (polData->lmCounterClass);
	}
	
	UpdateAllViews ( 0, HINT_CLASS_CREATED, &olHintObject );
	return olHintObject.GetSize();
}

void CProfileEditorDoc::ProcessPXCChange ( UINT ipBCType, RecordSet *popData )
{
	CString				olUrno, olUccc, olMsg;
	long				llCCCUrno;
	int					ilClassIdx;
	bool				blDspMsg = false;
	
	olUrno = popData->Values[ogBCD.GetFieldIndex("PXC","UPRO")];
	if ( olUrno != omUrno )
		return ;	//	�nderung betrifft nicht das dargestellte Profil
	olUccc = popData->Values[ogBCD.GetFieldIndex("PXC","UCCC")];
	if ( sscanf ( olUccc, "%lu", &llCCCUrno ) < 1 )
		return;
	ilClassIdx = FindClass( llCCCUrno );
	CDWordArray olHintObject;
	olHintObject.Add ( llCCCUrno );

	switch ( ipBCType )
	{
		case BC_NEW:
			//olMsg = "Class \"%s\" has been added to profile outside.";
			olMsg = GetString ( IDS_PXC_INS_OUTSIDE );
			if ( ilClassIdx < 0 )		//  PXC noch nicht im Profil
			{
				blDspMsg = true;
				LoadOneClass ( *popData );
				UpdateAllViews ( 0, HINT_CLASS_CREATED, &olHintObject );
			}
			break;
		case BC_CHANGED:
			if ( ilClassIdx >= 0 )		//  PXC bereits im Profil
			{
				LoadOneClass ( *popData );
				UpdateAllViews ( 0, HINT_CLASS_CHANGED, &olHintObject );
			}
			break;
		case BC_DELETED:
			//olMsg = "Class \"%s\" has been removed from profile outside.";
			olMsg = GetString(IDS_PXC_DEL_OUTSIDE);
			if ( ilClassIdx >= 0 )		//  PXC bereits im Profil
			{
				blDspMsg = true;
				DeleteClass ( 0, llCCCUrno );
			}
	}
	if ( blDspMsg && !bmDeletedLocal )
	{
		olMsg.Replace ( "%s", popData->Values[ogBCD.GetFieldIndex("PXC","PXCN")] );
		AfxMessageBox ( olMsg, MB_OK|MB_ICONEXCLAMATION );
	}
}

void CProfileEditorDoc::ProcessPROChange ( UINT ipBCType, RecordSet *popData )
{
	CString		/*olMsg,*/ olProUrno;

	olProUrno = popData->Values[ogBCD.GetFieldIndex("PRO","URNO" )];
	if ( olProUrno != omUrno )
		return ;   //  �nderung betrifft nicht dieses Profil

	if ( ipBCType == BC_DELETED )
	{
		/*olMsg = "Profile %s has been deleted outside!" ;
		olMsg.Replace ( "%s", GetTitle () );
		AfxMessageBox ( olMsg, MB_OK|MB_ICONEXCLAMATION );*/
		if ( !bmDeletedLocal )
			FileErrMsg ( 0, IDS_PRO_DEL_OUTSIDE, pCHAR(GetTitle()), 0, 
						 MB_OK|MB_ICONEXCLAMATION );
		if ( theApp.IsDocumentPointer ( this ) && bmValid )
		OnCloseDocument ();
	}
	if ( ipBCType == BC_CHANGED )
	{	
		LoadProRecord ( olProUrno );
		CDWordArray olHintObject;
		long		llUrno;
		if ( sscanf( olProUrno, "%lu", &llUrno ) >= 1 )
		{
			olHintObject.Add ( llUrno );
			UpdateAllViews ( 0, HINT_HEADER_CHANGED, &olHintObject );
		}
	}
}

void CProfileEditorDoc::ProcessPXAChange ( UINT ipBCType, RecordSet *popData )
{
	CString olPXAUrno, olForm, olMsg;
	olPXAUrno = popData->Values[ogBCD.GetFieldIndex("PXA","URNO" )];
	CDWordArray olHintObject;

	for ( int i=0; i<omChartData.GetSize(); i++ )
	{
		if ( olPXAUrno != omChartData[i].omUval )
			continue;	
		//  Diese Klasse verwendet das ge�nderte Checkin-Pattern
		olHintObject.Add ( omChartData[i].lmCounterClass );
		if ( ipBCType == BC_DELETED )
		{
			omChartData[i].Reset ();
		}
		if ( ipBCType == BC_CHANGED )
		{
			omChartData[i].ReadData ();
			omChartData[i].CalcDspData ();
		}
	}
	if ( olHintObject.GetSize() > 0 )
	{
		UpdateAllViews ( 0, HINT_CLASS_CHANGED, &olHintObject );
		if ( !bmDeletedLocal && (ipBCType == BC_DELETED) )
		{
			//  olForm = "Checkin-Pattern %s used for profile %s has been deleted outside.";
			olForm = GetString ( IDS_PXA_DEL_OUTSIDE );
			olMsg.Format ( olForm, popData->Values[ogBCD.GetFieldIndex("PXA","PXAN")],
						   GetTitle() );
			AfxMessageBox ( olMsg, MB_OK|MB_ICONEXCLAMATION );
		}
	}
		
}



void CProfileEditorDoc::OnCloseDocument ()
{
	if ( bmValid && theApp.IsDocumentPointer ( this ) )
	{
		bmValid = false;
		CDocument::OnCloseDocument();
	}
}


void ProcessBCInDoc ( void *popInstance, int ipDDXType,
					  void *vpDataPointer, CString &ropInstanceName)
{
	CString				olTable;
	UINT				ilBCType;
	CProfileEditorDoc	*polDoc;
	RecordSet			*polRec ;
	bool blOk = GetBCType ( ipDDXType, olTable, ilBCType );

	if ( !blOk || olTable.IsEmpty() )
		return;
	polRec = (RecordSet*)vpDataPointer;
	if ( !AfxIsValidAddress ( polRec, sizeof(RecordSet) ) ||
 		 !AfxIsValidAddress ( &(polRec->Values), sizeof(CStringArray) ) 
	   )
		return;
	//call Routine Proces... for all open Documents
	CDocTemplate* polTemplate = theApp.FindTemplate( "Profil" );
	if ( polTemplate )
	{
		POSITION ilPos;
		CDocument *polDoku;
		ilPos = polTemplate->GetFirstDocPosition ();
		while ( ilPos )
		{
			polDoku = polTemplate->GetNextDoc ( ilPos );
			if ( polDoku  &&
				( ((CObject*)polDoku)->GetRuntimeClass() == 
				  RUNTIME_CLASS(CProfileEditorDoc) )
			   )
			{
				polDoc = (CProfileEditorDoc*)polDoku;
				TRACE ( "ProcessBCInDoc Document <%s> Table <%s>\n", 
						polDoc->GetTitle(), olTable );
				if ( olTable=="PRO" ) 
					polDoc->ProcessPROChange ( ilBCType, polRec );
				else
					if ( olTable=="PXC" ) 
						polDoc->ProcessPXCChange ( ilBCType, polRec );
				else
					if ( olTable=="PXA" ) 
						polDoc->ProcessPXAChange ( ilBCType, polRec );
			}

		}
	}
}


void CProfileEditorDoc::PatternChanged ( CChartData *popData, UINT ipBits )
{
	CChartData *polAktData;

	if ( !popData )
		return;
	if ( ipBits&PXA_CHANGED )
	{
		for ( int i=0; i<omChartData.GetSize(); i++ )
		{	
			polAktData = &(omChartData[i]);
			if ( polAktData == popData )
				continue;
			if ( polAktData && (polAktData->omUval == popData->omUval) )
			{
				polAktData->imMinStep = popData->imMinStep;
				polAktData->bmPXAModified = true;
			}
		}
	}
	if (ipBits&VTP_CHANGED)
	{
		for ( int i=0; i<omChartData.GetSize(); i++ )
		{	
			polAktData = &(omChartData[i]);
			if ( polAktData == popData )
				continue;
			if ( polAktData && (polAktData->omUval == popData->omUval) )
			{
				polAktData->CopyValues ( (const CChartData&)*popData );
				polAktData->bmVTPModified = true;
			}
		}
	}
	if ( (ipBits&PXA_CHANGED) || (ipBits&VTP_CHANGED) )
	{
		CDWordArray olHintObject;
		long llUval = atol ( popData->omUval );
		if ( llUval > 0 )
		{
			olHintObject.Add (llUval);
			UpdateAllViews ( 0, HINT_PATTERN_CHANGED, &olHintObject );
		}
	}
}
