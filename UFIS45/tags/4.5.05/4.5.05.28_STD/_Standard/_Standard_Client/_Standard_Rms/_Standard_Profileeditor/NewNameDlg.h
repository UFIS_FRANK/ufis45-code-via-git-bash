#if !defined(AFX_DIALOG_NEUER_NAME_H__AE9AABB3_B758_11D2_AAF6_00001C018CF3__INCLUDED_)
#define AFX_DIALOG_NEUER_NAME_H__AE9AABB3_B758_11D2_AAF6_00001C018CF3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Dialog_Neuer_Name.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CNewNameDlg dialog

class CNewNameDlg : public CDialog
{
// Construction
public:
	CNewNameDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CNewNameDlg)
	enum { IDD = IDD_NEW_NAME_DLG };
	CString	m_Name;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNewNameDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CNewNameDlg)
	virtual void OnOK();
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIALOG_NEUER_NAME_H__AE9AABB3_B758_11D2_AAF6_00001C018CF3__INCLUDED_)
