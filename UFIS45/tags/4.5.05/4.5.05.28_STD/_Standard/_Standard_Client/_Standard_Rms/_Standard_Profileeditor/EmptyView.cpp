// EmptyView.cpp : implementation file
//

#include <stdafx.h>
#include <ProfileEditor.h>
#include <EmptyView.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEmptyView

IMPLEMENT_DYNCREATE(CEmptyView, CScrollView)

CEmptyView::CEmptyView()
{
}

CEmptyView::~CEmptyView()
{
}


BEGIN_MESSAGE_MAP(CEmptyView, CScrollView)
	//{{AFX_MSG_MAP(CEmptyView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEmptyView drawing

void CEmptyView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();

	CSize sizeTotal;
	// TODO: calculate the total size of this view
	sizeTotal.cx = sizeTotal.cy = 100;
	SetScrollSizes(MM_TEXT, sizeTotal);
}

void CEmptyView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}

/////////////////////////////////////////////////////////////////////////////
// CEmptyView diagnostics

#ifdef _DEBUG
void CEmptyView::AssertValid() const
{
	CScrollView::AssertValid();
}

void CEmptyView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CEmptyView message handlers
