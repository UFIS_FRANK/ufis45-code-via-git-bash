// ProfileEditorView.cpp : implementation of the CProfileEditorView class
//

#include <stdafx.h>

#include <cedabasicdata.h>

#include <ccsglobl.h>
#include <ProfileEditor.h>
#include <ProfileEditorDoc.h>
#include <ProfileEditorView.h>
#include <GridControl.h>
#include <ProfileChart.h>
#include <utilities.h>
#include <emptyview.h>
#include <childfrm.h>
#include <ConditionsGrid.h>
#include <basicdata.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CProfileEditorView

IMPLEMENT_DYNCREATE(CProfileEditorView, CFormView)

BEGIN_MESSAGE_MAP(CProfileEditorView, CFormView)
	//{{AFX_MSG_MAP(CProfileEditorView)
	ON_WM_CREATE()
	ON_BN_CLICKED(IDC_DCOP_CHK, OnDcopChk)
	ON_BN_CLICKED(IDC_DCUP_CHK, OnDcupChk)
	ON_BN_CLICKED(IDC_DGAP_CHK, OnDgapChk)
	ON_BN_CLICKED(IDC_DSEP_CHK, OnDsepChk)
	ON_WM_SIZE()
	ON_EN_KILLFOCUS(IDC_REMA, OnKillfocusRema)
	ON_BN_CLICKED(IDC_PRST_CHK, OnPrstChk)
	ON_EN_KILLFOCUS(IDC_NAME_IO, OnKillfocusName)
	ON_WM_KILLFOCUS()
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT, OnUpdateFilePrint)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT_PREVIEW, OnUpdateFilePrintPreview)
	ON_BN_CLICKED(IDC_EXCLUDE_CHB, OnExcludeChb)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CFormView::OnFilePrintPreview)
	ON_MESSAGE(WM_GRID_ENDEDITING, OnEndEditing)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnCCSEditKillFocus)
	ON_MESSAGE(WM_PREPARE_CLOSING,OnPrepareClosing)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProfileEditorView construction/destruction

CProfileEditorView::CProfileEditorView()
	: CFormView(CProfileEditorView::IDD)
{
	//{{AFX_DATA_INIT(CProfileEditorView)
	fmDcop = 0.0f;
	fmDcup = 0.0f;
	fmDgap = 0.0f;
	fmDsep = 0.0f;
	//}}AFX_DATA_INIT
	// TODO: add construction code here
	pomDepartureGrid = 0;
	pomClassesGrid = 0;

	RegisterBC ( this, "CProfileEditorView", "DGR", ProcessBCInView, BC_NEW|BC_DELETED );
	RegisterBC ( this, "CProfileEditorView", "SGR", ProcessBCInView, BC_NEW|BC_DELETED );
	RegisterBC ( this, "CProfileEditorView", "CCC", ProcessBCInView, BC_NEW|BC_DELETED );	
}

CProfileEditorView::~CProfileEditorView()
{
	ogDdx.UnRegister(this, NOTUSED);
	if ( pomDepartureGrid )
		delete pomDepartureGrid;
	if ( pomClassesGrid )
		delete pomClassesGrid;
}

void CProfileEditorView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CProfileEditorView)
	DDX_Control(pDX, IDC_NAME_IO, omNameEdit);
	DDX_Control(pDX, IDC_TOTAL_EDIT, m_TotalDistributionEdit);
	DDX_Control(pDX, IDC_DCOP_EDIT, m_DcopEdit);
	DDX_Control(pDX, IDC_DCUP_EDIT, m_DcupEdit);
	DDX_Control(pDX, IDC_DGAP_EDIT, m_DgapEdit);
	DDX_Control(pDX, IDC_DSEP_EDIT, m_DsepEdit);
	DDX_Text(pDX, IDC_DCOP_EDIT, fmDcop);
	DDX_Text(pDX, IDC_DCUP_EDIT, fmDcup);
	DDX_Text(pDX, IDC_DGAP_EDIT, fmDgap);
	DDX_Text(pDX, IDC_DSEP_EDIT, fmDsep);
	//}}AFX_DATA_MAP
}

BOOL CProfileEditorView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CProfileEditorView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	pomDepartureGrid = new CConditionsGrid ( this, IDC_DEPARTURE_LIST, 5, 3 );
	pomDepartureGrid->IniLayoutForConditions ();
	if ( !ogTPLUrno.IsEmpty() )
	{
		CString olTPLString = ogBCD.GetField("TPL", "URNO", ogTPLUrno, "FLDD" );
		pomDepartureGrid->IniGridFromTplString ( olTPLString );
	}

	pomClassesGrid = new CGridControl ( this, IDC_CLASSES_LIST, 5, 1 );
	ResizeParentToFit(FALSE);
	pomParent = (CChildFrame*)GetParentFrame();
	IniClassesGrid (); 
	IniStatics();

	m_DcopEdit.SetTypeToDouble(3, 2, 0.0, 100.0);
	m_DcupEdit.SetTypeToDouble(3, 2, 0.0, 100.0);
	m_DgapEdit.SetTypeToDouble(3, 2, 0.0, 100.0);
	m_DsepEdit.SetTypeToDouble(3, 2, 0.0, 100.0);
	m_TotalDistributionEdit.SetTypeToDouble(3, 2, 0.0, 100.0);
	m_DcopEdit.SetTextErrColor(RED);
	m_DcupEdit.SetTextErrColor(RED);
	m_DgapEdit.SetTextErrColor(RED);
	m_DsepEdit.SetTextErrColor(RED);
	m_TotalDistributionEdit.SetTextErrColor(RED);


	omNameEdit.SetTypeToString("X(14)",14,1);;		
	omNameEdit.SetBKColor(YELLOW);  
	omNameEdit.SetTextErrColor(RED);
	
	DisplayDocument ( );

	m_DcopEdit.EnableWindow(IsDlgButtonChecked(IDC_DCOP_CHK) );	
	m_DcupEdit.EnableWindow(IsDlgButtonChecked(IDC_DCUP_CHK) );	
	m_DgapEdit.EnableWindow(IsDlgButtonChecked(IDC_DGAP_CHK) );	
	m_DsepEdit.EnableWindow(IsDlgButtonChecked(IDC_DSEP_CHK) );	

	SetBdpsState();
}

/////////////////////////////////////////////////////////////////////////////
// CProfileEditorView printing

BOOL CProfileEditorView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CProfileEditorView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CProfileEditorView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void CProfileEditorView::OnPrint(CDC* pDC, CPrintInfo* /*pInfo*/)
{
	// TODO: add customized printing code here
}

/////////////////////////////////////////////////////////////////////////////
// CProfileEditorView diagnostics

#ifdef _DEBUG
void CProfileEditorView::AssertValid() const
{
	CFormView::AssertValid();
}

void CProfileEditorView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CProfileEditorDoc* CProfileEditorView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CProfileEditorDoc)));
	return (CProfileEditorDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CProfileEditorView message handlers

int CProfileEditorView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	

	return 0;
}

void CProfileEditorView::OnEndEditing(WPARAM wParam, LPARAM lParam)
{
	GRIDNOTIFY			rlNotify = *((GRIDNOTIFY*)lParam);
	CSize				olSize = GetTotalSize();
	CProfileEditorDoc	*polDoc = GetDocument();
	float				flValue=0.0;
	long				ilOldUrno=-1, ilNewUrno=-1;
	CGXStyle			olStyle;
	CString				olValStr;
	int					ilValue=0;
			
	ASSERT ( polDoc );

	if ( (rlNotify.idc == IDC_CLASSES_LIST) && (rlNotify.col >= 1) && 
		 (rlNotify.row >= 1)  )
	{
		CString olCellText;
	
		olCellText = pomClassesGrid->GetValueRowCol ( rlNotify.row, rlNotify.col );
		if ( pomClassesGrid->GetStyleRowCol ( rlNotify.row, 1, olStyle ) &&
			 olStyle.GetIncludeItemDataPtr () )
			ilOldUrno = (long)olStyle.GetItemDataPtr() ;

		switch ( rlNotify.col )
		{	
			case 1:
				if ( !olCellText.IsEmpty () )
					ilNewUrno = GetClassUrno ( olCellText );
				if ( ilOldUrno != ilNewUrno )
				{	
					if ( ilOldUrno!=-1 )
						polDoc->DeleteClass ( this, ilOldUrno );
					if ( ( ilNewUrno!=-1 ) && polDoc->AddClass ( this, ilNewUrno ) )
					{
						olStyle.SetItemDataPtr ( (void*)ilNewUrno );
					}
					else
					{
						olStyle.SetItemDataPtr ( (void*)-1 );
						olStyle.SetValue ("" );
					}
					pomClassesGrid->SetStyleRange( CGXRange(rlNotify.row, rlNotify.col), olStyle );
				}
				break;
			case DPAX_COL:
			case DBAG_COL:
				if ( sscanf ( olCellText, "%f", &flValue ) > 0 )
				{
					olValStr.Format ( "%.2f", flValue ) ;
					if ( olValStr.GetLength () >5)				
						olValStr = olValStr.Left(5);
				}
				break;
			case TTWO_COL:
			case TTBG_COL:
				if ( sscanf ( olCellText, "%d", &ilValue ) > 0 )
				{
					olValStr.Format ( "%d", ilValue ) ;
					if ( olValStr.GetLength () >4)				
						olValStr = olValStr.Left(4);
				}
				break;
		}
		if ( !pomClassesGrid->DeleteEmptyRows ( rlNotify.row, 1 ) )
		{
			if ( rlNotify.col == DPAX_COL )
				polDoc->ChangeDPAXandDBAG ( this, ilOldUrno, &olValStr, 0 );
			if ( rlNotify.col == DBAG_COL )
				polDoc->ChangeDPAXandDBAG ( this, ilOldUrno, 0, &olValStr );
			if ( rlNotify.col == TTWO_COL )
				polDoc->ChangeTransactionTimes ( this, ilOldUrno, &olValStr, 0 );
			if ( rlNotify.col == TTBG_COL )
				polDoc->ChangeTransactionTimes ( this, ilOldUrno, 0, &olValStr );
		}
		CheckPAXPercentages ();
	}
	if ( rlNotify.idc == IDC_DEPARTURE_LIST )
	{
		CString olText;
		ConstructString( pomDepartureGrid, olText );
		polDoc->SetSingleDBField ( this, "EVTD", olText );
		olText = pomDepartureGrid->ConstructPrioString();	// priority string
		polDoc->SetSingleDBField ( this, "PRIO", olText );
		
	}
}


void CProfileEditorView::IniClassesGrid ()
{
	CString olChoiceList, olClassName;

	int ilClasses = ogBCD.GetDataCount ( "CCC" );
	for ( int i=0; i<ilClasses; i++ )
	{
		olClassName = ogBCD.GetField ( "CCC", i, "CICN" );
		if ( !olClassName.IsEmpty() )
		{
			olChoiceList += olClassName;
			if ( i<ilClasses )
				olChoiceList += '\n';
		}
	}
	pomClassesGrid->EnableGridToolTips(TRUE);
	BOOL ok = pomClassesGrid->SetStyleRange( CGXRange(1,1), 
				   CGXStyle().SetControl(GX_IDS_CTRL_TEXTFIT).SetValue("")
				   .SetChoiceList(olChoiceList) );
	ok &= pomClassesGrid->SetStyleRange( CGXRange(1,DPAX_COL,1,DBAG_COL), 
				   CGXStyle().SetControl(GX_IDS_CTRL_EDIT).SetValueType(GX_VT_NUMERIC)
							 .SetFormat(GX_FMT_FIXED).SetPlaces(2).SetValue("")
 				 		     .SetUserAttribute(GX_IDS_UA_VALID_MIN, "0")
							 .SetUserAttribute(GX_IDS_UA_VALID_MAX, "100") );
	ok &= pomClassesGrid->SetStyleRange( CGXRange(1,TTWO_COL,1,TTBG_COL), 
				   CGXStyle().SetControl(GX_IDS_CTRL_EDIT).SetValueType(GX_VT_NUMERIC)
							 .SetFormat(GX_FMT_FIXED).SetPlaces(0).SetValue("")
 				 		     .SetUserAttribute(GX_IDS_UA_VALID_MIN, "0")
							 .SetUserAttribute(GX_IDS_UA_VALID_MAX, "9999") );
							 
	pomClassesGrid->SetValueRange ( CGXRange(0,1), GetString(IDS_CLASS) );
	pomClassesGrid->SetValueRange ( CGXRange(0,DPAX_COL), GetString(IDS_PAX_PCNT) );
	pomClassesGrid->SetValueRange ( CGXRange(0,DBAG_COL), GetString(IDS_BAG_PCNT) );
	pomClassesGrid->SetValueRange ( CGXRange(0,TTWO_COL), GetString(IDS_TT_WO_BAG)  );
	pomClassesGrid->SetValueRange ( CGXRange(0,TTBG_COL), GetString(IDS_TT_BAG)  );

	pomClassesGrid->SetStyleRange( CGXRange().SetCols(DPAX_COL), 
						   CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, GetString(IDS_TOOLTIP_DPAX) ));
	pomClassesGrid->SetStyleRange( CGXRange().SetCols(DBAG_COL), 
						   CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, GetString(IDS_TOOLTIP_DBAG) ));
	pomClassesGrid->SetStyleRange( CGXRange().SetCols(TTWO_COL), 
					   CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, GetString(IDS_TOOLTIP_TTWO) ));
	pomClassesGrid->SetStyleRange( CGXRange().SetCols(TTBG_COL), 
					   CGXStyle().SetUserAttribute(GX_IDS_UA_TOOLTIPTEXT, GetString(IDS_TOOLTIP_TTBG) ));
	
	pomClassesGrid->SetColWidth ( 0, 0, 15 );
	pomClassesGrid->SetColWidth ( 1, 1, 100	);

	CRect olRect;
	pomClassesGrid->GetClientRect(&olRect);
	int ilWidth = olRect.Width() - 15 - 100;
	
	pomClassesGrid->SetColWidth ( 2, 5, ilWidth / 4 );
	
	if ( igPxcTtbgIdx<0 ) // New field "TTBG" not yet in DB
	{
		pomClassesGrid->DisableRange ( CGXRange().SetCols(TTWO_COL) );	
	}
	if ( igPxcTtwoIdx<0 ) // New field "TTWO" not yet in DB
	{
		pomClassesGrid->DisableRange ( CGXRange().SetCols(TTBG_COL) );
	}
	
	CheckPAXPercentages ();
}		


void CProfileEditorView::CheckPAXPercentages ()
{
	int ilRows = pomClassesGrid->GetRowCount ();
	float flSum=0.0, flValue;
	CString olValue;

	for ( int i = 1; i<ilRows; i++ )
	{
		olValue = pomClassesGrid->GetValueRowCol ( i, 2 );
		if ( sscanf ( olValue, "%f", &flValue ) > 0 )
			flSum += flValue;
	}
	COLORREF ilColor = flSum>100.0 ? RGB(255,0,0) : RGB(0,0,0);
	pomClassesGrid->SetStyleRange ( CGXRange(ilRows,2), CGXStyle().SetTextColor(ilColor) );
	pomClassesGrid->SetValueRange ( CGXRange(ilRows,2), 100.0-flSum );
}

void CProfileEditorView::OnDcopChk() 
{
	// TODO: Add your control notification handler code here
	m_DcopEdit.EnableWindow(IsDlgButtonChecked(IDC_DCOP_CHK) );	
	OnDistributionChanged ( IDC_DCOP_CHK );
	if ( IsDlgButtonChecked(IDC_DCOP_CHK) )
		SendMessage ( WM_NEXTDLGCTL, 0, 0L );
}

void CProfileEditorView::OnDcupChk() 
{
	// TODO: Add your control notification handler code here
	m_DcupEdit.EnableWindow(IsDlgButtonChecked(IDC_DCUP_CHK) );	
	OnDistributionChanged ( IDC_DCUP_CHK );
	if ( IsDlgButtonChecked(IDC_DCUP_CHK) )
		SendMessage ( WM_NEXTDLGCTL, 0, 0L );
}

void CProfileEditorView::OnDgapChk() 
{
	// TODO: Add your control notification handler code here
	m_DgapEdit.EnableWindow(IsDlgButtonChecked(IDC_DGAP_CHK) );	
	OnDistributionChanged ( IDC_DGAP_CHK );
	if ( IsDlgButtonChecked(IDC_DGAP_CHK) )
		SendMessage ( WM_NEXTDLGCTL, 0, 0L );
}

void CProfileEditorView::OnDsepChk() 
{
	// TODO: Add your control notification handler code here
	m_DsepEdit.EnableWindow(IsDlgButtonChecked(IDC_DSEP_CHK) );	

	OnDistributionChanged ( IDC_DSEP_CHK );
	if ( IsDlgButtonChecked(IDC_DSEP_CHK) )
		SendMessage ( WM_NEXTDLGCTL, 0, 0L );
}

void CProfileEditorView::DisplayTotalDistribution ()
{
	float flSum=0.0;
	UpdateData();

	CProfileEditorDoc	*polDoc = GetDocument();
	if ( IsDlgButtonChecked(IDC_DSEP_CHK) )
		flSum += fmDsep;
	if ( IsDlgButtonChecked(IDC_DCUP_CHK) )
		flSum += fmDcup;
	if ( IsDlgButtonChecked(IDC_DCOP_CHK) )
		flSum += fmDcop;
	if ( IsDlgButtonChecked(IDC_DGAP_CHK) )
		flSum += fmDgap;
	CString olText;
	olText.Format ( "%.2f", flSum );
	m_TotalDistributionEdit.SetInitText ( olText, true );
	m_TotalDistributionEdit.SetStatus( (flSum<=100.0) && (flSum>=0.0) );
	
	//COLORREF ilColor = (flSum>100.0) || (flSum<0.0) ? RED : BLACK;
	//m_TotalDistributionEdit.SetTextColor(ilColor);
}

void CProfileEditorView::OnCCSEditKillFocus ( WPARAM wparam, LPARAM lparam )
{
	CCSEDITNOTIFY	*prpNotify = (CCSEDITNOTIFY*)lparam;

	if ( !UpdateData() )
	{
		 //prpNotify->SourceControl->SetFocus();
		 return;
	}
	if ( prpNotify->SourceControl == &m_DcopEdit )
		OnDistributionChanged ( IDC_DCOP_CHK );
	if ( prpNotify->SourceControl == &m_DcupEdit )
		OnDistributionChanged ( IDC_DCUP_CHK );
	if ( prpNotify->SourceControl == &m_DgapEdit )
		OnDistributionChanged ( IDC_DGAP_CHK );
	if ( prpNotify->SourceControl == &m_DsepEdit )
		OnDistributionChanged ( IDC_DSEP_CHK );
 	if ( prpNotify->SourceControl == &omNameEdit )
		OnKillfocusName() ;
	
}

void CProfileEditorView::OnSize(UINT nType, int cx, int cy) 
{
	CFormView::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	
}

void CProfileEditorView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	// TODO: Add your specialized code here and/or call the base class
	if ( lHint==HINT_HEADER_CHANGED )
	{
		DisplayDocument ();
	}

	CDWordArray *polUrnoArray=0;
	if ( (lHint==HINT_CLASS_DELETED) || (lHint==HINT_CLASS_CREATED) || 
		 (lHint==HINT_CLASS_CHANGED) || (lHint==HINT_CLASS_HEAD_CHANGED) )
		polUrnoArray = (CDWordArray*)pHint;
	if ( polUrnoArray )
		for ( int i=0; i<polUrnoArray->GetSize(); i++ )
		{
			if ( lHint==HINT_CLASS_CREATED )
				OnNewClass ( polUrnoArray->ElementAt ( i ) );
			if ( lHint==HINT_CLASS_DELETED ) 
				OnDeleteClass ( polUrnoArray->ElementAt ( i ) );
			if ( (lHint==HINT_CLASS_CHANGED) || (lHint==HINT_CLASS_HEAD_CHANGED) )
				OnChangeClass ( polUrnoArray->ElementAt ( i ) );
		}

}

bool CProfileEditorView::DisplayDocument ( )
{
	CProfileEditorDoc	*polDoc = GetDocument();
	if ( !polDoc )
		return false;
	bool blScanOk;

	blScanOk = ( sscanf ( polDoc->omDcop, "%f", &fmDcop ) > 0 );
	if ( !blScanOk )
		fmDcop = 0.0;
	CheckDlgButton( IDC_DCOP_CHK, blScanOk&&polDoc->bmFdco );
	m_DcopEdit.EnableWindow(IsDlgButtonChecked(IDC_DCOP_CHK) );	

	blScanOk = ( sscanf ( polDoc->omDcup, "%f", &fmDcup ) > 0 );
	if ( !blScanOk )
		fmDcup = 0.0;
	CheckDlgButton( IDC_DCUP_CHK, blScanOk&&polDoc->bmFdcu );
	m_DcupEdit.EnableWindow(IsDlgButtonChecked(IDC_DCUP_CHK) );	

	blScanOk = ( sscanf ( polDoc->omDgap, "%f", &fmDgap ) > 0 );
	if ( !blScanOk )
		fmDgap = 0.0;
	CheckDlgButton( IDC_DGAP_CHK, blScanOk&&polDoc->bmFdga );
	m_DgapEdit.EnableWindow(IsDlgButtonChecked(IDC_DGAP_CHK) );	

	blScanOk = ( sscanf ( polDoc->omDsep, "%f", &fmDsep ) > 0 );
	if ( !blScanOk )
		fmDsep = 0.0;
	CheckDlgButton( IDC_DSEP_CHK, blScanOk&&polDoc->bmFdse );
	m_DsepEdit.EnableWindow(IsDlgButtonChecked(IDC_DSEP_CHK) );	

	CheckDlgButton( IDC_PRST_CHK, polDoc->omPrst!="1" );
	CheckDlgButton( IDC_EXCLUDE_CHB, polDoc->omExcl=="1" );

	SetDlgItemText ( IDC_REMA, polDoc->omRemark );
	SetDlgItemText ( IDC_NAME_IO, polDoc->GetTitle() );
	ReConstructString( pomDepartureGrid, polDoc->omEvtd );
	UpdateData(FALSE);
	DisplayTotalDistribution ();

	for ( int i=0; i<polDoc->omChartData.GetSize(); i++ )
	{
		OnNewClass ( polDoc->omChartData[i].lmCounterClass );
		OnChangeClass ( polDoc->omChartData[i].lmCounterClass );
	}
	return true;
}

void CProfileEditorView::OnKillfocusRema() 
{
	// TODO: Add your control notification handler code here
	CString olText;
	GetDlgItemText( IDC_REMA, olText ); 
	GetDocument()->SetSingleDBField ( this, "REMA", olText );
}

void CProfileEditorView::OnPrstChk() 
{
	// TODO: Add your control notification handler code here
	CString olText = IsDlgButtonChecked(IDC_PRST_CHK) ? "0" : "1";
	GetDocument()->SetSingleDBField ( this, "PRST", olText );

}

void CProfileEditorView::OnKillfocusName() 
{
	// TODO: Add your control notification handler code here
	CString olText;
	GetDlgItemText( IDC_NAME_IO, olText ); 
	GetDocument()->SetSingleDBField ( this, "NAME", olText );
}

void CProfileEditorView::OnNewClass ( long lpClassUrno )
{
	CString		olClassName = GetClassName ( lpClassUrno );
	int			ilRows = pomClassesGrid->GetRowCount();
	CGXStyle	olStyle;

	int ilIdx = FindInClassesGrid ( lpClassUrno );

	if ( ilIdx<0 )
	{
		if ( !pomClassesGrid->GetStyleRowCol(ilRows,1, olStyle) )
			olStyle.SetDefault();
		olStyle.SetValue ( olClassName );
		olStyle.SetItemDataPtr ( (void*)lpClassUrno );
		pomClassesGrid->SetStyleRange ( CGXRange(ilRows,1), olStyle );
		pomClassesGrid->DoAutoGrow ( ilRows, 1 );
		CheckPAXPercentages ();
	}
}

void CProfileEditorView::OnDeleteClass ( long lpClassUrno )
{
	CString		olClassName = GetClassName ( lpClassUrno );
	int			ilRows = pomClassesGrid->GetRowCount();
	CGXStyle	olStyle;

	int ilIdx = FindInClassesGrid ( lpClassUrno );
	if ( ilIdx > 0 )
	{
		olStyle.SetDefault();
		olStyle.SetValue ( "" );
		olStyle.SetItemDataPtr ( (void*)-1 );
		pomClassesGrid->SetStyleRange ( CGXRange(ilIdx,1), olStyle );
		pomClassesGrid->DeleteEmptyRows ( ilIdx, 1 );
		CheckPAXPercentages ();
	}
}


void CProfileEditorView::OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CFormView::OnActivateView(bActivate, pActivateView, pDeactiveView);
}



void  CProfileEditorView::ConstructString(CGridControl* popGrid, CString &ropResult) 
{
	//--- exception handling
	CCS_TRY

	ropResult.Empty();
    int ilCount = popGrid->GetRowCount();

    for(int i = 0; i < ilCount; i++)
	{   
		CString  olTable  = popGrid->GetValueRowCol(i + 1, 7); // Tabelle
		CString  olSimpleVal = popGrid->GetValueRowCol(i + 1, SIMPLE_VAL); 
		CString  olDynGrp = popGrid->GetValueRowCol(i + 1, DYNAMIC_GRP); 
		CString  olStatGrp = popGrid->GetValueRowCol(i + 1, STATIC_GRP); 

		if(olSimpleVal.IsEmpty() == FALSE && olSimpleVal != CString(" "))
		{
			popGrid->UnFormatSpecial ( i + 1, SIMPLE_VAL, olSimpleVal ); 
            ropResult += CString("0.") + olTable + CString("=") + olSimpleVal + CString(";");
		}
		else if(olDynGrp.IsEmpty() == FALSE && olDynGrp != CString(" "))
		{
            ropResult += CString("1.") + olTable + CString("=") + olDynGrp + CString(";");
		}
		else if(olStatGrp.IsEmpty() == FALSE && olStatGrp != CString(" "))
		{
            ropResult += CString("2.") + olTable + CString("=") + olStatGrp + CString(";");
		}
	}

	// remove ";" from end of string
	if (ropResult.Right(1) == CString(";"))
	{
		ropResult = ropResult.Left(ropResult.GetLength() - 1);
	}

	//--- exception handling
	CCS_CATCH_ALL
}
//------------------------------------------------------------------------------------------------------------------

void  CProfileEditorView::ReConstructString(CConditionsGrid* popGrid, CString &ropString) 
{
	//--- exception handling
	CCS_TRY

    //--- Zeilen in der Tabelle
    int ilRowCount = popGrid->GetRowCount();

	//--- String zerlegen 
	CStringArray olDataArray;
	int ilData = ExtractItemList(ropString, &olDataArray, ';');

	//--- more data than rows in grid ? Uuups !!!
	if(ilRowCount < ilData)
	{ 
		  AfxMessageBox(GetString(165));
		  return;
	}

	popGrid->ResetValues();
	//--- write data to grid
	int ilPos = -1;
	CString olData;
	CString olColumn;
	CString olValue;
	CString olFieldName;
	CString olRowContents;
    for(int i = 0; i < ilData; i++)
	{   
		olData = olDataArray[i];
		if (olData.IsEmpty() == FALSE && olData != CString(" "))
		{
			ilPos = olData.Find("=");
			if(ilPos > 0)
			{
				olColumn = olData.Left(1);
				olFieldName = olData.Mid(2,8);
				olValue = olData.Right(olData.GetLength() - ilPos - 1);
				
				for (int ilRow = 1; ilRow <= ilRowCount; ilRow++)
				{
					olRowContents = popGrid->GetValueRowCol(ilRow, 7);
					if (olFieldName == olRowContents)
					{
						if(atoi(olColumn) == 0)
						{
							popGrid->FormatSpecial ( ilRow, SIMPLE_VAL, olValue );
							popGrid->SetValueRange(CGXRange(ilRow, 2), olValue);
							//popGrid->DisplayInfo ( ilRow, SIMPLE_VAL, true );
							popGrid->SetToolTipForValue ( ilRow, SIMPLE_VAL, olValue );
							popGrid->MarkInValid ( ilRow, SIMPLE_VAL, olValue );
						}
						if(atoi(olColumn) == 1)
						{
							popGrid->SetValueRange(CGXRange(ilRow, DYNAMIC_GRP), olValue);
							//popGrid->DisplayInfo ( ilRow, DYNAMIC_GRP, true );
							popGrid->SetToolTipForValue ( ilRow, DYNAMIC_GRP, olValue );
							popGrid->MarkInValid ( ilRow, DYNAMIC_GRP, olValue );
						}
					    if(atoi(olColumn) == 2)
						{
							popGrid->SetValueRange(CGXRange(ilRow, STATIC_GRP), olValue);
							//popGrid->DisplayInfo ( ilRow, STATIC_GRP, true );
							popGrid->SetToolTipForValue ( ilRow, STATIC_GRP, olValue );
							popGrid->MarkInValid ( ilRow, STATIC_GRP, olValue );
						}
						break;	//  conventions !? Mmh...ain't it faster this way !?
					}
				}
			}
		}
	}

	  //--- exception handling
	CCS_CATCH_ALL
}

//  CProfileEditorView::FindInClassesGrid 
//  Sucht Zeile im Grid der Counter-Klassen, die sich auf Datensatz
//	aus "CCC" mit URNO == lpClassUrno bezieht.
//  return:   -1,  wenn Counter-Class nicht im Grid
//			  Index der entspr. Zeile, sonst
int CProfileEditorView::FindInClassesGrid ( long lpClassUrno )
{
	CString	olClassName = GetClassName ( lpClassUrno );
	CString olValue;
	long    llParam;
	bool    blFound=false;
	int ilRows = pomClassesGrid->GetRowCount();
	CGXStyle olStyle;

	for ( int i=1; i<ilRows; i++ )
	{
		if ( !pomClassesGrid->GetStyleRowCol(i,1, olStyle) )
			olStyle.SetDefault ();
		olValue = pomClassesGrid->GetValueRowCol ( i, 1 );
		if ( pomClassesGrid->GetStyleRowCol ( i, 1, olStyle ) &&
			 olStyle.GetIncludeItemDataPtr () )
		{
			llParam = (long)olStyle.GetItemDataPtr() ;
			if ( ( llParam == lpClassUrno ) && ( olValue==olClassName ) )
				return i;
		}
	}
	return -1;
}

void CProfileEditorView::OnChangeClass ( long lpClassUrno )
{
	CString		olClassName = GetClassName ( lpClassUrno );
	int			ilRows = pomClassesGrid->GetRowCount();
	CGXStyle	olStyle;
	CProfileEditorDoc	*polDoc = (CProfileEditorDoc*)GetDocument();
	CChartData	*polData=0;
	int			ilDataIdx;
	int			ilIdx = FindInClassesGrid ( lpClassUrno );

	if ( ilIdx>0 )
	{
		if ( polDoc )
		{
			ilDataIdx = polDoc->FindClass(lpClassUrno) ;
			polData = (ilDataIdx>=0) ? &(polDoc->omChartData[ilDataIdx]) : 0;
		}
		if ( polData )
		{
			pomClassesGrid->SetValueRange( CGXRange(ilIdx,DPAX_COL), polData->omDPAX );
			pomClassesGrid->SetValueRange( CGXRange(ilIdx,DBAG_COL), polData->omDBAG );
			if ( igPxcTtbgIdx>=0 ) // New field "TTBG" already in DB
				pomClassesGrid->SetValueRange( CGXRange(ilIdx,TTBG_COL), polData->omTTBG );
			if ( igPxcTtwoIdx>=0 ) // New field "TTWO" already in DB
				pomClassesGrid->SetValueRange( CGXRange(ilIdx,TTWO_COL), polData->omTTWO );
		}
		CheckPAXPercentages ();
	}
}


void CProfileEditorView::OnPrepareClosing ( WPARAM wparam, LPARAM lparam )
{
}

void CProfileEditorView::OnKillFocus(CWnd* pNewWnd) 
{
	CFormView::OnKillFocus(pNewWnd);
	
	// TODO: Add your message handler code here
/*	CWnd *pWnd1 = GetFocus();	
	if ( pWnd1 )
		pWnd1->SendMessage ( WM_KILLFOCUS, 0, 0 );*/
	SendMessageToDescendants( WM_KILLFOCUS, 0, 0, TRUE );
}

void CProfileEditorView::IniStatics()
{
	SetDlgItemLangText ( this, IDC_NAME_TXT, IDS_PROFILE_NAME );
	SetDlgItemLangText ( this, IDC_DEPARTURE_TXT, IDS_DEPARTURE );
	SetDlgItemLangText ( this, IDC_CLASSES_TXT, IDS_CLASSES );
	SetDlgItemLangText ( this, IDC_PRST_CHK, IDS_DEAKTIVIERT );
	SetDlgItemLangText ( this, IDC_DISTRIBUTION_GRP, IDS_DISTRIBUTION );
	SetDlgItemLangText ( this, IDC_DCOP_CHK, IDS_COMMON );
	SetDlgItemLangText ( this, IDC_DGAP_CHK, IDS_GATE );
	SetDlgItemLangText ( this, IDC_DSEP_CHK, IDS_SELF );
	SetDlgItemLangText ( this, IDC_DCUP_CHK, IDS_CURB );
	SetDlgItemLangText ( this, IDC_TOTAL_TXT, IDS_TOTAL );
	SetDlgItemLangText ( this, IDC_REMA_TXT, IDS_REMARK );
	SetDlgItemLangText ( this, IDC_EXCLUDE_CHB, IDS_EXCLUDE );

}

void CProfileEditorView::OnUpdateFilePrint(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable ( FALSE );	
}

void CProfileEditorView::OnUpdateFilePrintPreview(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	pCmdUI->Enable ( FALSE );	
	
}

void CProfileEditorView::ProcessCCCChange ( bool bpAdd, RecordSet *popData )
{
	CProfileEditorDoc	*polDoc = GetDocument();
	CString				olUrno, olName;
	long				llUrno;
	CString				olMsg;

	olUrno = popData->Values[ogBCD.GetFieldIndex("CCC","URNO")];
	olName = popData->Values[ogBCD.GetFieldIndex("CCC","CICN")];
	if ( !bpAdd &&  ( sscanf ( olUrno, "%ld", &llUrno ) > 0 )  )
		polDoc->DeleteClass ( this, llUrno );
	for ( ROWCOL i = pomClassesGrid->GetRowCount(); i>=1; i-- )
	{
		if ( !bpAdd && ( pomClassesGrid->GetValueRowCol ( i, 1 ) == olName ) )
		{/*
			olMsg ="Counter class %s has been deleted outside! Class will be removed from profile." ;
			olMsg.Replace ( "%s", olName );
			MessageBox ( olMsg, "", MB_ICONEXCLAMATION|MB_OK );*/
			FileErrMsg ( m_hWnd, IDS_CCC_DEL_OUTSIDE, pCHAR(olName), 0, 
						 MB_ICONEXCLAMATION|MB_OK );
			pomClassesGrid->RemoveRows (i,i );
		}
		else
			pomClassesGrid->ModifyChoiceListRowCol ( i, 1, GX_IDS_CTRL_TEXTFIT, 
													 olName, bpAdd );
	}
}


void ProcessBCInView(void *popInstance, int ipDDXType,
						   void *vpDataPointer, CString &ropInstanceName)
{
	CString				olTable;
	UINT				ilBCType;
	CProfileEditorView	*polView;
	RecordSet			*polRec ;
	bool blAdd ;
	bool blOk = GetBCType ( ipDDXType, olTable, ilBCType );

	if ( !blOk || !popInstance || olTable.IsEmpty() ||
		 ( ((CObject*)popInstance)->GetRuntimeClass() != 
		   RUNTIME_CLASS(CProfileEditorView) 
		 )
	   )
	   return;
	polRec = (RecordSet*)vpDataPointer;
	blAdd = (ilBCType!=BC_DELETED);
	polView = (CProfileEditorView *)popInstance;
	if ( olTable=="CCC" ) 
		polView->ProcessCCCChange ( blAdd, polRec );
	else
	{
		if ( polView->pomDepartureGrid )
			polView->pomDepartureGrid->ModifyChoiceList ( olTable, blAdd, 
														  polRec );
	}
}

  


void CProfileEditorView::OnExcludeChb() 
{
	// TODO: Add your control notification handler code here
	CString olText = IsDlgButtonChecked(IDC_EXCLUDE_CHB) ? "1" : "0";
	GetDocument()->SetSingleDBField ( this, "EXCL", olText );
}

void CProfileEditorView::OnDistributionChanged ( int ipChkBoxID )
{
	CProfileEditorDoc	*polDoc = GetDocument();
	if  ( !polDoc )
		return;
	UINT ilCheck = IsDlgButtonChecked(ipChkBoxID);
	switch ( ipChkBoxID )
	{
		case IDC_DCOP_CHK:
			polDoc->SetOneDistribution (this, "DCOP", (ilCheck>0), fmDcop );
			break;
		case IDC_DCUP_CHK:
			polDoc->SetOneDistribution (this, "DCUP", (ilCheck>0), fmDcup );
			break;
		case IDC_DGAP_CHK:
			polDoc->SetOneDistribution (this, "DGAP", (ilCheck>0), fmDgap );
			break;
		case IDC_DSEP_CHK:
			polDoc->SetOneDistribution (this, "DSEP", (ilCheck>0), fmDsep );
			break;
	}
	DisplayTotalDistribution ();
}

void CProfileEditorView::SetBdpsState()
{
	char	clStat;
	CWnd	*pWnd;

	clStat = ogPrivList.GetStat("IDC_DCOP_EDIT");
	SetWndStatAll(clStat, m_DcopEdit);
	if ( pWnd = GetDlgItem ( IDC_DCOP_CHK ) )
		SetpWndStatAll(clStat, pWnd );

	clStat = ogPrivList.GetStat("IDC_DCUP_EDIT");
	SetWndStatAll(clStat, m_DcupEdit);
	if ( pWnd = GetDlgItem ( IDC_DCUP_CHK ) )
		SetpWndStatAll(clStat, pWnd );

	clStat = ogPrivList.GetStat("IDC_DGAP_EDIT");
	SetWndStatAll(clStat, m_DgapEdit);
	if ( pWnd = GetDlgItem ( IDC_DGAP_CHK ) )
		SetpWndStatAll(clStat, pWnd );

	clStat = ogPrivList.GetStat("IDC_DSEP_EDIT");
	SetWndStatAll(clStat, m_DsepEdit);
	if ( pWnd = GetDlgItem ( IDC_DSEP_CHK ) )
		SetpWndStatAll(clStat, pWnd );

	clStat = ogPrivList.GetStat("IDC_TOTAL_EDIT");
	SetWndStatAll(clStat, m_TotalDistributionEdit);
	if ( pWnd = GetDlgItem ( IDC_TOTAL_TXT ) )
		SetpWndStatAll(clStat, pWnd );
}