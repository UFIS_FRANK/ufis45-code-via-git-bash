//////////////////////////////////////////////////////////////////////
//	GUILng.cpp: Implementierung der Klasse CGUILng. 
//////////////////////////////////////////////////////////////////////
//
//	Wrapperclass to support different languages with DB
//
//	all controlls must be set at runtime at fct "OnInitDialog" with "SetDlgItemText()"
//	all captions must be set at runtime with "SetWindowText()"
//	all Menu-Strings must be set at Runtime with MainFrame Fct ::TranslateMenu (not implemented yet)
//	virtual MainFrame and ChildFrame Fct ::OnToolTip must be overridden
//	virtual MainFrame Fct ::GetMessageString must be overridden
//
//////////////////////////////////////////////////////////////////////
//
//	!!!!!!!!!!!!!!!! ATTENTION !!!!!!!!!!!!!
//
//	add to "ceda.ini" GLOBAL Section!!!!!!!!
//	'DE','US','IT' or 'DE,Test' ect.
//	LANGUAGE=DE
//
//////////////////////////////////////////////////////////////////////
//	History:
//		rdr		03.01.2000		created
//		rdr		04.01.2000		Add2DB added, connected to LoadStg
//		rdr		05.01.2000		STID was unique in DB !!! 
//								changed to normal index
//		rdr		06.01.2000		check, if all Res are in DB, if not store all needed res
//		rdr		10.01.2000		if no language found in ini, use normal res-string
//		rdr		11.01.2000		handle placeholders and only APPL-relevant strings
//		rdr		12.01.2000		added new remarks
//		rdr		13.01.2000		redesign of Class (add2DB and UpdDB merged, ...)
//								GetString overloaded (input UINT or CString)
//		rdr		17.01.2000		Class implemented as "Singelton"
//		rdr		19.01.2000		Get and Set om_Param implemented, look at Init()!
//		rdr		20.01.2000		use const, not #define
//		rdr		03.02.2000		update Range implemented (use DBParam in cedaini)
//		rdr		06.03.2000		DB update with *.dat-File implemented
//								HdlTxtDat() implemented
//								Clean DB with DBParam = 2
//		rdr		08.03.2000		init changed, ogBCD where clause without appl
//								new GetString, Wrapperfunction for ogBCD.GetFieldExt
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <GUILng.h>
#include <BasicData.h>
#include <waitdlg.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CGUILng* CGUILng::_theOne = 0;

//////////////////////////////////////////////////////////////////////
// Definition of Class Constants  !!!! PRIVAT !!!!
//////////////////////////////////////////////////////////////////////

	const int		CGUILng::GUI_MAX_RES			= 64000;
	const int		CGUILng::MAX_TEMPSTR			= 128;
	const CString	CGUILng::RES_DESCRIPTION		= "*REM*";		// the Part after this is ignored
	const CString	CGUILng::APPL_CODE				= "*INTXT*";	// Part of String = this is an Appl-relevant string
	const CString	CGUILng::RES_FORMAT				= "*FORM*";		// for future use, the class is only searching for '%'
	const CString	CGUILng::TAB_NAME				= "TXT";
	const CString	CGUILng::APPL_FIELD				= "APPL";
	const CString	CGUILng::CDAT_FIELD				= "CDAT";
	const CString	CGUILng::COCO_FIELD				= "COCO";
	const CString	CGUILng::HOPO_FIELD				= "HOPO";
	const CString	CGUILng::LSTU_FIELD				= "LSTU";
	const CString	CGUILng::STAT_FIELD				= "STAT";
	const CString	CGUILng::STID_FIELD				= "STID";
	const CString	CGUILng::STRG_FIELD				= "STRG";
	const CString	CGUILng::TXID_FIELD				= "TXID";
	const CString	CGUILng::URNO_FIELD				= "URNO";
	const CString	CGUILng::USEC_FIELD				= "USEC";
	const CString	CGUILng::USEU_FIELD				= "USEU";
	const CString	CGUILng::ClearDB				= "2";
	const CString	CGUILng::StoreInDB				= "1";
	const CString	CGUILng::StoreNotInDB			= "0";
	const CString	CGUILng::UpdateRangeInDB		= "upd (";
	const CString	CGUILng::DatDir					= CCSLog::GetTmpPath("\\");
	const CString	CGUILng::FileExt				= "txt.dat";
	const int		CGUILng::NEW					= 1;
	const int		CGUILng::UPD					= 2;
	const int		CGUILng::DEL					= 3;

//////////////////////////////////////////////////////////////////////
// Construction / Destruction  !!!! PRIVAT !!!!
//////////////////////////////////////////////////////////////////////

// Constructor
CGUILng::CGUILng()
{
	bm_GlobalInit = false;
	bm_UseDef = false;
	bm_UseRes=true;//hag000426
	om_User = "";
}

// Destructor
CGUILng::~CGUILng()
{
}

//////////////////////////////////////////////////////////////////////
// Menberfunctions, public - Interface
//////////////////////////////////////////////////////////////////////

// The One and Only Object
// refer to "Pattern" from Gamma, Helm, Johnson and Vlissides, Capter "Singelton"
// User has to use THIS and only THIS interface
CGUILng* CGUILng::TheOne()
{
	if (_theOne == 0)
	{
		_theOne = new CGUILng();
	}
	return _theOne;
}

// Maininitialisation !!!!!!
bool CGUILng::MemberInit(CString *OutError, CString User, CString Appl, CString HoPo, CString Lng, CString DBParam)
{
	int count = 0;

	CString olError = "";

	if(!User.IsEmpty())
	{
		om_User = User;
		count++;
	}
	else
		olError = "User ";

	if(!Appl.IsEmpty())
	{
		om_APPL = Appl;
		count++;
	}
	else
		olError += "Appl ";

	if(!HoPo.IsEmpty())
	{
		om_HoPo = HoPo;
		count++;
	}
	else
		olError += "HoPo ";

	if(!Lng.IsEmpty())
	{
		om_CurrLng = Lng;
		count++;
	}
	else
		olError += "Lng ";

	if(!DBParam.IsEmpty())
	{
		om_Param = DBParam;
		count++;
	}
	else
		olError += "DBParam ";
	

	if (count == 5)
	{
		_theOne->InitData();								// initialize ogBCD
		bm_GlobalInit = true;
	}
	else
		bm_GlobalInit = false;

	OutError->operator =( olError );

	return bm_GlobalInit;
}

// new function to get a String from DB or RES, 
// GetResString is used, if String is'nt ogBCD
// called by LoadStg !!!
const CString CGUILng::GetString(UINT nID)
{
	CString olTxt="???";
	if (!bm_UseRes)										// Use no Res-String !!
	{
		CString olUrno, olIDS;
		bool blFound = false;
		if (ogBCD.GetDataCount(TAB_NAME)>0)				// process only if ogBCD is'nt empty
		{
			olIDS.Format("%ld", nID);					// convert UINT to CString
			olUrno = ogBCD.GetFieldExt(TAB_NAME, STID_FIELD, APPL_FIELD, olIDS, om_APPL, URNO_FIELD);
			if (!olUrno.IsEmpty())						// get URNO from DB with APPL
			{
				blFound = ogBCD.GetField(TAB_NAME, URNO_FIELD, olUrno, STRG_FIELD, olTxt);
			}											// fill olTxt
			if (!blFound)								// olTxt not filled
			{											// get URNO from DB with GENERAL
				olUrno= ogBCD.GetFieldExt(TAB_NAME, STID_FIELD, APPL_FIELD, olIDS, "GENERAL", URNO_FIELD);
				if(!olUrno.IsEmpty())					
				{										// fill olTxt
					blFound = ogBCD.GetField(TAB_NAME, URNO_FIELD, olUrno, STRG_FIELD, olTxt);
				}										
			}
		}	// end ogBCD is'nt empty
		if(!blFound)									// is olTxt not realy filled
		{
			olTxt = GetResString (nID);					// get the Res-String
		}
	}	// end use no res-String
	else
	{
		olTxt = GetResString (nID);
	}
	return olTxt;
}


// fills OutStrg and returns true for succes
const bool CGUILng::GetString(CString *OutStrg, CString RefField1, CString RefField2, CString RefValue1, CString RefValue2)
{
	OutStrg->operator =( GetString(RefField1, RefField2, RefValue1, RefValue2));
	if (!OutStrg->IsEmpty())
		return true;
	else
		return false;
}


// cut RES_DESCRIPTION, APPL_CODE and RES_FORMAT from string
const CString CGUILng::GetString(CString olTxt)
{

	int i = olTxt.Find(RES_DESCRIPTION);
	int j = olTxt.Find(APPL_CODE);
	int k = olTxt.Find(RES_FORMAT);
	int l = -1;
	l = GetFirstMatch(i, j, k);
	// l contains the 1. match of RES_DESCRIPTION or APPL_CODE or RES_FORMAT !!
	if(l>-1)
	{
		olTxt = olTxt.Left(l);
	}
	return olTxt;
}

// Wrapperfunction for ogBCD.GetFieldExt
const CString CGUILng::GetString(CString RefField1, CString RefField2, CString RefValue1, CString RefValue2)
{
	CString olTxt = "";
	if (ogBCD.GetDataCount(TAB_NAME)>0)				// process only if ogBCD is'nt empty
	{
		olTxt = ogBCD.GetFieldExt(TAB_NAME, RefField1, RefField2, RefValue1, RefValue2, STRG_FIELD);
	}
	return olTxt;
}

/*
// obsolete, use MenberInit();
// use this Function to set the currend Language Membervariable
void CGUILng::SetLanguage(CString Lng)
{
	if (!bm_GlobalInit)
	{
		om_CurrLng = Lng;
		_theOne->InitData();								// initialize ogBCD
	}
}
*/

// use this Function to get the currend Language
const CString CGUILng::GetLanguage()
{
	return om_CurrLng;
}

/*
// obsolete, use MenberInit();
// use this Function to set the Store in DB Param
void CGUILng::SetStoreInDB(CString olParam)
{
	if (!bm_GlobalInit)
	{
		om_Param = olParam;
		_theOne->InitData();								// initialize ogBCD
	}
}
*/

// use this Function to get the Store in DB Param
const CString CGUILng::GetStoreInDB()
{
	return om_Param;
}

//////////////////////////////////////////////////////////////////////
// Menberfunctions, privat - Implementation
//////////////////////////////////////////////////////////////////////

// Initialisation of "local" Data (ogBCD)
void CGUILng::InitData()
{
	if (om_CurrLng.IsEmpty())
	{
		bm_UseRes=true;		// No Language found in ini	=>		use Res
	}
	else
	{
		bm_UseRes=false;	// found Language in ini	=>		use DB
	}

	if (om_CurrLng.Find("Test") > 0)
	{
		bm_UseDef=true;		// found "Test"-String in ini
//		bm_UseRes=true;	
	}
	else
	{
		bm_UseDef=false;
	}

	if (!bm_UseRes)			// USE DB and no RES !!
	{

		char querystr[MAX_TEMPSTR];

		ogBCD.SetObject(TAB_NAME,"APPL,CDAT,COCO,HOPO,LSTU,STAT,STID,STRG,TXID,URNO,USEC,USEU");

		imPos_APPL=ogBCD.GetFieldIndex(TAB_NAME,APPL_FIELD);	//	Application name
		imPos_CDAT=ogBCD.GetFieldIndex(TAB_NAME,CDAT_FIELD);	//	CreationDate
		imPos_COCO=ogBCD.GetFieldIndex(TAB_NAME,COCO_FIELD);	//	Country code
		imPos_HOPO=ogBCD.GetFieldIndex(TAB_NAME,HOPO_FIELD);	//	Home airport
		imPos_LSTU=ogBCD.GetFieldIndex(TAB_NAME,LSTU_FIELD);	//	Last Update
		imPos_STAT=ogBCD.GetFieldIndex(TAB_NAME,STAT_FIELD);	//	Status
		imPos_STID=ogBCD.GetFieldIndex(TAB_NAME,STID_FIELD);	//	String ID
		imPos_STRG=ogBCD.GetFieldIndex(TAB_NAME,STRG_FIELD);	//	String Text
		imPos_TXID=ogBCD.GetFieldIndex(TAB_NAME,TXID_FIELD);	//	Text ID
		imPos_URNO=ogBCD.GetFieldIndex(TAB_NAME,URNO_FIELD);	//	Unique record nr.
		imPos_USEC=ogBCD.GetFieldIndex(TAB_NAME,USEC_FIELD);	//	User code
		imPos_USEU=ogBCD.GetFieldIndex(TAB_NAME,USEU_FIELD);	//	User (last update)

//		sprintf(querystr,"WHERE COCO='%s' AND APPL='%s'",om_CurrLng.Left(2),om_APPL);
		sprintf(querystr,"WHERE COCO='%s'",om_CurrLng.Left(2));

		ogBCD.Read(TAB_NAME,querystr);		// fill lokal DB

		HdlTxtDat();	// search for "c\tmp\'appl'txt.dat" and update DB

		int il = ogBCD.GetDataCount(TAB_NAME); // only info in Tracefile

		if (((om_Param.IsEmpty()) || (StoreNotInDB == om_Param)) == false)	// no param or StoreNotInDB is set,
		{														// ignore db handling
			if ((StoreInDB == om_Param)||(ClearDB == om_Param))	// only if StoreInDB or ClearDB is set
			{													// do anything
				UINT rdrstore = HdlAllRes();					// call the "real" initialisation
			}
			else
				// special key to update db-stringrange with res
				// DBParam = upd (12,34)
				if( UpdateRangeInDB == om_Param.Left(UpdateRangeInDB.GetLength()))
				{
					CString	olRange = om_Param.Mid(UpdateRangeInDB.GetLength(),om_Param.GetLength()-(UpdateRangeInDB.GetLength()+1));
					int i = olRange.Find(',');
					CString olItem1 = olRange.Mid(0,i);
					CString olItem2 = olRange.Mid(i+1,olRange.GetLength()); 
					UpdateIDRange(atoi(olItem1),atoi(olItem2));
				}
		}
		TRACE("\nCGUILng::init(): %d Strings loaded from TXTTAB, \nwith COCO = '%s' APPL = '%s'! and DBParam = '%s'\n",il,om_CurrLng,om_APPL,om_Param);
	}
	else
	{
		TRACE("\nCGUILng::init(): TXTTAB not used, Strings loaded from Res, \nwith COCO = '%s' APPL = '%s'! and DBParam = '%s'\n",om_CurrLng,om_APPL,om_Param);
	}
}

// old function to get a Res-String
CString CGUILng::GetResString(UINT nID)
{
	CString olString = "";			// cleare string
	olString.LoadString(nID);		// load string from Res
	if (!bm_UseDef)
	{
		olString = GetString(olString);	// cut all not needed "Extensions" (like *REM* ...)
	}
	else
	{
		if (StoreInDB == om_Param)
		{
			olString = GetString(olString);	// s.o. store never '#' in DB
		}
		else
		{
			olString = "#" + GetString(olString) + "#";	// s.o. + '#' for testing
		}
	}
	return olString;
}

// olS == NEW  -> stores string in   DB "TXTTAB"
// olS == UPD  -> update string in   DB "TXTTAB"
// olS == DEL  -> delete string from DB "TXTTAB"
bool CGUILng::HdlDBRes(CString olTxt, UINT nID, int olS)
{
	int       cnt	= ogBCD.GetFieldCount(TAB_NAME);// to init recordset	
	bool	blerr	= false;						// return value
	RecordSet olTRecord(cnt);						// init recordset
	CString olStid	= "";							// init lokal Stid
	olStid.Format("%ld", nID);						// convert UINT to CString

	olTRecord.Values[imPos_STID] = olStid;			// set recordset STID
	olTRecord.Values[imPos_STRG] = olTxt;			// set recordset STRG
	olTRecord.Values[imPos_APPL] = om_APPL;			// set recordset APPL
	olTRecord.Values[imPos_TXID] = "";				// set recordset TXID (contains the same information as STID) 
	olTRecord.Values[imPos_STAT] = "NEW";			// set recordset STAT
	olTRecord.Values[imPos_HOPO] = om_HoPo;			// set recordset HOPO
	olTRecord.Values[imPos_COCO] = om_CurrLng.Left(2);		// set recordset COCO

	switch (olS)
	{
		case 3:
			{			// DELETE
				blerr = ogBCD.DeleteRecord(TAB_NAME,STID_FIELD, olStid, true);
			}
			break;
		case 2:
			{			// UPDATE 
				CString olUrno	= "";						// init lokal Urno
															// set lokal Urno
				olUrno = ogBCD.GetFieldExt(TAB_NAME, STID_FIELD, APPL_FIELD, olStid, om_APPL, URNO_FIELD);
				if (!olUrno.IsEmpty())						// found Urno
				{
					olTRecord.Values[imPos_STAT] = "UPD";	// set recordset STAT
					olTRecord.Values[imPos_URNO] = olUrno.Left(10);	// set recordset URNO
					olTRecord.Values[imPos_USEC] = om_User;	// set recordset USEC
					olTRecord.Values[imPos_LSTU] = om_User;	// set recordset LSTU
															// update string in DB
					blerr = ogBCD.SetRecord(TAB_NAME,URNO_FIELD,olUrno,olTRecord.Values,true);
				}
				else
				{											// no Urno found
					blerr = false;
				}
			}
			break;
		case 1:
			{			// NEW
				blerr = ogBCD.InsertRecord(TAB_NAME, olTRecord, true);
			}
			break;
		default:
			break;
	}
	return 	blerr;
}

// search all Res-Strings and handle
UINT CGUILng::HdlAllRes()
{
	UINT i,i_max,i_stored,i_updated,i_deleted;
	i_max             = GUI_MAX_RES;
	i_stored		  = 0;
	i_updated		  = 0;
	i_deleted		  = 0;
	int j			  = 0;
	int	k			  = 0;
	int l			  = 0;
	CString olResStrg = "";
	CWaitDlg olWaitDlg(NULL,"Please wait!");			// show the waitdlg

	for(i = 0; i < i_max; i++)							// do for 0 till i_max
	{	
		if(olResStrg.LoadString(i))						// is this ID an Res-String
		{												// yes, this is an Res-String			
			j = olResStrg.Find(APPL_CODE);				// check if this is an APPL relevant String
			if (j>-1)									// j is >= 0 if this is an APPL relevant Res-String			
			{											// special handling	!!
				olResStrg = GetString(olResStrg);
				k = GetItemCount(olResStrg, '%');		// string from res and '%'-Count
				if (k-1)								// GetItemCount returns 1 (size of itemlist) if no item found
				{
					CString olDBStrg = GetString(i);	// string from db
					l = GetItemCount(olDBStrg, '%');	// '%'-Count
					if (k==l)							// if count equal compare "placeholder"
					{//OK	
						CString olDBTmp  = olDBStrg;	// store DB-String
						CString olResTmp = olResStrg;	// store Res-String
						for( int m=1;m<k;m++)			// do for all counts
						{
							int n = olDBTmp.Find("%");	// store 1. match in DB string
							int o = olResTmp.Find("%");	// store 1. match in Res string

							CString olDBComp  = olDBTmp.Mid(n,3);	// get match 'details"	(%s or %d or %l ...)
							CString olResComp = olResTmp.Mid(o,3);	// get match 'details"

							if (!(olDBComp == olResComp))	// compare db and res placeholder
							{
								i_updated++;			// update dbstring with resstring 
								if (HdlDBRes(GetResString(i), i, UPD))
								{
									m=k;				// breake for all counts if update returns true
								}
							}	// end compare
							olDBTmp  = olDBTmp.Mid(n+1,olDBTmp.GetLength());	// set next match
							olResTmp = olResTmp.Mid(o+1,olResTmp.GetLength());	// set next match
						}	// end for all counts
					}	// end count compare equal
					else
					{									// placeholder-count not equal!!
						i_updated++;					// update dbstring with resstring 
						HdlDBRes(GetResString(i), i, UPD);
					}	// end count compare not euqal
				}	// end '%'-processing
				// at this point, all 'valid' Res-String _with_ '%' are processed!
				// and now handle the others
				
				if (!IsResInDB(i))						// is this Res in DB (with appl)?
				{										// no, load it 
					i_stored++;
					// only APPL relevant string must be stored !!!!
					HdlDBRes(GetResString(i), i, NEW);		// and store _pure_ string in DB !
				}	// end handle the others
			}	// end APPL relevant String, others are ignored
		}	// end valid res ID
		else
		{	// "String" ID is in DB and not in RES. Delete it!
			if( ClearDB == om_Param) 
			{
				CString olIDS = "";
				olIDS.Format("%ld", i);					// convert UINT to CString
				CString olTmp = ogBCD.GetFieldExt(TAB_NAME, STID_FIELD, APPL_FIELD, olIDS, om_APPL, URNO_FIELD);
//				olTmp.LoadString(i);		// call MFC-routine (it's faster)
				if (olTmp.IsEmpty()==false) // is it a valid DB-String
				{	// i means string ID to delete from DB !!!
					// delete String from DB
					HdlDBRes("", i, DEL);
					i_deleted++;
				}
			}
		}
	}	// end do for all ID
	olWaitDlg.CloseWindow();
	TRACE("\nCGUILng::HdlAllRes(): %d Resurces added, %d Resources updated and %d Strings from DB deleted!\n",i_stored,i_updated,i_deleted);
	return i_stored;
}

// return  1 if res is in DB with APPL=om_APPL
// return  2 if res is in DB with APPL="GENERAL"
// return  0 if res is not in DB
int CGUILng::IsResInDB(UINT nID)
{
//	bool blFound = false;
	if (ogBCD.GetDataCount(TAB_NAME)>0)
	{
		CString olUrno, olIDS;
		olIDS.Format("%ld", nID);
		olUrno = ogBCD.GetFieldExt(TAB_NAME, STID_FIELD, APPL_FIELD, olIDS, om_APPL, URNO_FIELD);
		if (!olUrno.IsEmpty())
		{
			return 1;
		}
		else  //		if (!blFound)
		{
			olUrno= ogBCD.GetFieldExt(TAB_NAME, STID_FIELD, APPL_FIELD, olIDS, "GENERAL", URNO_FIELD);
			if(!olUrno.IsEmpty())
			{
				return 2;
			}
		}
	}
	return 0;
}

// updates ID-Range until error (ID is no RES-String)
// obsolete???, use HdlTxtDat()!
bool CGUILng::UpdateIDRange(UINT nIDmin,UINT nIDmax)
{
	bool blerr	= false;
	UINT i		= __min(nIDmin,nIDmax);
	UINT j		= __max(nIDmin,nIDmax);

	for(i; i < j+1; i++)
	{
		blerr = HdlDBRes(GetResString(i), i, UPD);
		if (!blerr) 
			return blerr;
	}
	return 	blerr;
}

// try to open ...txt.dat-File and hdl all listed ID's
void CGUILng::HdlTxtDat()
{									// set Filename like c:\tmp\rostertxt.dat
	CString FileName	= DatDir + om_APPL + FileExt;
	CStdioFile csfile;
	CFileException ferror;
	CString olItemList	= "";
	char	szBuffer[1024]; 
	char	olTrenner	= ',';
	UINT	nActual		= 0; 

	// do anything, if file can be found
	if (csfile.Open( FileName, CFile::modeRead, &ferror)) 
	{
		csfile.SeekToBegin();		// set file pointer to the beginning of the file
		nActual = csfile.Read( szBuffer, sizeof( szBuffer ) ); // read buffer (1024Byte)
		szBuffer[nActual] = '\0';
		olItemList = (CString) szBuffer;		// converts buffer to CString 
		olItemList = olItemList.Left(nActual);
		olItemList.Replace("\n","");
		olItemList.Replace("\r","");

		int i = GetItemCount(olItemList,olTrenner);	// count Items
		if (i-1)					// GetItemCount returns 1 if no Item found
		{
			for(int j=0;j<i;j++)	// do for all Items
			{
				CString olSingleItem = GetListItem(olItemList,j+1,FALSE,olTrenner);
				UINT uilID = atoi(olSingleItem);
				CString Item = GetResString(uilID);
				if (!Item.IsEmpty())
				{
					HdlDBRes(Item, uilID, UPD);
				}
			}
		}
		csfile.Close();
	    CFile::Remove(FileName);
	}
	else
	{
		// no file found or could not open file
		// error msg ???
		// do nothing
	}
}

// returns the smallest unsigned value of i,j,k (>(-1)) !!!
int CGUILng::GetFirstMatch(int i, int j, int k)
{
/*	all cases are tested
//i= 1;j= 2;k= 3;	//	i OK
//i= 1;j= 3;k= 2;	//	i OK
//i= 2;j= 1;k= 3;	//	j OK
//i= 3;j= 1;k= 2;	//	j OK
//i= 2;j= 3;k= 1;	//	k OK
//i= 3;j= 2;k= 1;	//	k OK

//i= 1;j= 1;k= 2;	//	i OK
//i= 2;j= 1;k= 1;	//	j OK
//i= 1;j= 2;k= 1;	//	k OK

//i= 1;j= 2;k= 2;	//	i OK
//i= 2;j= 1;k= 2;	//	j OK
//i= 2;j= 2;k= 1;	//	k OK

//i= 1;j= 1;k= 1;	//	k OK

//i= 1;j= 1;k=-1;	//	i OK
//i= 1;j=-1;k= 1;	//	j OK
//i=-1;j= 1;k= 1;	//	k OK

//i= 1;j=-1;k= 2;	//	i OK
//i=-1;j= 1;k= 2;	//	j OK
//i=-1;j= 2;k= 1;	//	k OK

//i= 1;j= 2;k=-1;	//	i OK
//i= 2;j= 1;k=-1;	//	j OK
//i= 2;j=-1;k= 1;	//	k OK

//i= 1;j=-1;k=-1;	//	i OK
//i=-1;j= 1;k=-1;	//	j OK
//i=-1;j=-1;k= 1;	//	k OK

//i=-1;j=-1;k=-1;	//	l OK
*/	
	int l=-1;

	if ((i>-1) && (j>-1) && (k>-1))
	{
		l=__min(__min(i,j),__min(j,k));
	}
	else
	{
		if ((i>-1) && (j>-1))
		{
			l=__min(i,j);
		}
		else
		{
			if ((j>-1) && (k>-1))
			{
				l=__min(j,k);
			}
			else
			{
				if ((i>-1) && (k>-1))
				{
					l=__min(i,k);
				}
				else
				{
					if (i>-1)
					{
						l=i;
					}
					else
					{
						if (j>-1)
						{
							l=j;
						}
						else
						{
							l=k;
						}
					}
				}
			}
		}
	}
	return l;
}

// TESTFUNCTION, make it public to test the class outside
void CGUILng::CGUILngTest()
{
	//Constructor
	CGUILng* ogGUILng = CGUILng::TheOne();

	// adress of ogGUILng2 is the same as ogGUILng !!!
	CGUILng* ogGUILng2 = CGUILng::TheOne();	 

	// set-function
	// and intialisation
	CString Error;
	ogGUILng->MemberInit(&Error,"rdr","ROSTER","FRA","DE","1");	
	
	// get-function
	CString Lng = GetLanguage();	
	
	// get pure string
	CString RDRTest = "RDRTEST" + (CString)RES_DESCRIPTION + (CString)APPL_CODE + (CString)RES_FORMAT;
	RDRTest = GetString((CString) RDRTest);

	// get string from DB
	CString RDRTest2 = ""; 
	RDRTest2 = GetString((UINT) 42);

	// get string from RES
	CString RDRTest3 = ""; 
	RDRTest3 = GetResString(IDR_MAINFRAME);

	// init internal Datahandling
	ogBCD.SetObject(TAB_NAME,"APPL,CDAT,COCO,HOPO,LSTU,STAT,STID,STRG,TXID,URNO,USEC,USEU");
	char querystr[MAX_TEMPSTR];

	sprintf(querystr,"WHERE COCO='%s' AND APPL='%s'",om_CurrLng.Left(2),om_APPL);

	ogBCD.Read(TAB_NAME,querystr);

	// count data
	int il = ogBCD.GetDataCount(TAB_NAME);

	// Samples for DB access

	CString olUrno	= "";						// init lokal Urno
	CString olStid	= "";							// init lokal Stid
	olStid.Format("%ld", 42);						// convert UINT to CString
	olUrno = ogBCD.GetFieldExt(TAB_NAME, STID_FIELD, APPL_FIELD, olStid, om_APPL, URNO_FIELD);
	RecordSet olTRecord(ogBCD.GetFieldCount(TAB_NAME));						// init recordset
	olTRecord.Values[imPos_STID] = "42";			// set recordset STID
	olTRecord.Values[imPos_STRG] = "RDR TEST";		// set recordset STRG
	olTRecord.Values[imPos_APPL] = om_APPL;			// set recordset APPL
	olTRecord.Values[imPos_TXID] = "";				// set recordset TXID (contains the same information as STID) 
	olTRecord.Values[imPos_STAT] = "NEW";			// set recordset STAT
	olTRecord.Values[imPos_HOPO] = om_HoPo;			// set recordset HOPO
	olTRecord.Values[imPos_COCO] = om_CurrLng;		// set recordset COCO

	// Read
	olUrno = ogBCD.GetFieldExt(TAB_NAME, STID_FIELD, APPL_FIELD, "42", om_APPL, URNO_FIELD);
	CString olStat = ogBCD.GetFieldExt(TAB_NAME, STID_FIELD, APPL_FIELD, "42", om_APPL, STAT_FIELD);

	// Update
	bool blerr;
	olTRecord.Values[imPos_STAT] = "TEST";			// set recordset STAT
	blerr = ogBCD.SetRecord(TAB_NAME,URNO_FIELD,olUrno,olTRecord.Values,true);

	// Insert
	blerr = ogBCD.InsertRecord(TAB_NAME, olTRecord, true);

	// Delete
	blerr = ogBCD.DeleteRecord(TAB_NAME, URNO_FIELD, olUrno, true);
}
