#ifndef _CcaCedaFlightConfData_H_
#define _CcaCedaFlightConfData_H_
 
#include <BasicData.h>
#include <CCSGlobl.h>
#include <CCSDefines.h>

 
struct CCAFLIGHTCONFDATA 
{
	char 	 Act3[4]; 	// Flugzeug-3-Letter Code (IATA)
	char 	 Act5[6]; 	// Flugzeug-5-Letter Code
	char 	 Des3[4]; 	// Bestimmungsflughafen 3-Lettercode
	char 	 Flno[10]; 	// komplette Flugnummer (Airline, Nummer, Suffix)
	char	 Ftyp[2];
	char 	 Org3[4]; 	// Ausgangsflughafen 3-Lettercode
	long 	 Rkey; 	// Rotationsschl�ssel
	CTime 	 Stoa; 	// Planm��ige Ankunftszeit STA
	CTime 	 Stod; 	// Planm��ige Abflugzeit
	long 	 Urno; 	// Eindeutige Datensatz-Nr.

	// Data from cca - table

	CCAFLIGHTCONFDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Stoa = -1;
		Stod = -1;
	}

}; // end CCAFLIGHTCONFDATA
	




/////////////////////////////////////////////////////////////////////////////
// Class declaration

//@Man:
//@Memo: Read and write flight data
//@See: CedaData
/*@Doc:
  Reads and writes flight data from and to database. Stores flight data in memory 
  and provides methods for searching and retrieving flights from its internal list. 
  Does some data preparations like pre-formatting the flight number, storing flight 
  number and gate information for the corresponding inbound/outbound flight.

  Data of this class has to be updated by CEDA through broadcasts. This procedure 
  and the necessary methods will be described later with the description of 
  broadcast handling.

  {\bf CcaCedaFlightConfData} handles data from the EIOHDL server process.
	See the specification for table descriptions.
*/
class CcaCedaFlightConfData: public CCSCedaData
{
// Attributes
public:
    //@ManMemo: A map, containing the Keya/Keyd fields of all loaded flights.
    //CMapStringToPtr omKeyMap;
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<CCAFLIGHTCONFDATA> omData;

 	char *pcmAftConfFieldList;

// Operations
public:
    //@ManMemo: Default constructor
    /*@Doc:
      Initialize {\bf omRecInfo} for offsets and types for each fields in 
	  {\bf CCAFLIGHTCONFDATA}, the {\bf pcmTableName} with table name {\bf AFTLSG}, 
	  the data members {\bf pcmFieldList} contains a list of used fields of 
	  {\bf CCAFLIGHTCONFDATAStruct}.
    */
    //CcaCedaFlightConfData(CCSCedaCom *popCommHandler = NULL);  : CCSCedaData(&ogCommHandler)
    CcaCedaFlightConfData();
    //@ManMemo: Default destructor
	~CcaCedaFlightConfData();
	
	void UnRegister();
	void Register();

    //@ManMemo: Clear all flights.
    /*@Doc:
      Clear all flights.
      This method will clear {\bf omData} and {\bf omKeyMap}.
    */
	void ClearAll(void);

    // internal data access.
	bool DeleteFlightInternal(long lpUrno, bool bpDdx = true);
	bool AddFlightInternal(CCAFLIGHTCONFDATA *prpFlight, bool bpDdx);

	bool ReadFlights(const CString &popSelection, const CString &opUrnos, bool bpClearAll, bool bpDdx = true);

	bool AddToKeyMap(CCAFLIGHTCONFDATA *prpFlight );
	bool DeleteFromKeyMap(CCAFLIGHTCONFDATA *prpFlight );
	CCAFLIGHTCONFDATA *GetFlightByUrno(long lpUrno) const;


};

#endif
