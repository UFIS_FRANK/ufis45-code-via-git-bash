#include <stdafx.h>
#include <ccsglobl.h>
#include <winreg.h>
#include <CedaCfgData.h>
#include <cviewer.h> 
#include <iostream.h>
#include <fstream.h>
#include <iomanip.h>
#include <CCSTime.h>
#include <BasicData.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CViewer

CViewer::CViewer()
{
	/*
	if( !CheckKey( "Pepper" ) ) 
	{
		CreateKey( "Pepper" );
	}
	*/
	omFlightSearchConnection.Add(" AND ");
	omFlightSearchConnection.Add(" AND ");
	omFlightSearchConnection.Add(" AND ");
	omFlightSearchConnection.Add(" AND ");
	omFlightSearchConnection.Add(" AND ");
	omFlightSearchConnection.Add(" AND ");
	omFlightSearchConnection.Add(" AND ");
	omFlightSearchConnection.Add(" AND ");
	omFlightSearchConnection.Add(" AND ");
	omFlightSearchConnection.Add(" AND ");
	omFlightSearchConnection.Add(" AND ");
	omFlightSearchConnection.Add(" AND ");
	omFlightSearchConnection.Add(" AND ");
	omFlightSearchConnection.Add(" AND ");
	omFlightSearchConnection.Add(" AND ");
	omFlightSearchConnection.Add(" AND ");
	omFlightSearchConnection.Add(" AND ");
	omFlightSearchConnection.Add(" AND ");
	omFlightSearchConnection.Add(" OR ");
	omFlightSearchConnection.Add(" OR ");
	omFlightSearchConnection.Add(" OR ");
	omFlightSearchConnection.Add(" OR ");
	omFlightSearchConnection.Add(" OR ");
	omFlightSearchConnection.Add(" OR ");
	omFlightSearchConnection.Add(" OR ");

	//omGpeSearchConnection;

	omModul = "";
}
         
CViewer::~CViewer()
{
}


void CViewer::SetModul(CString opName)
{
	omModul = opName;
}

    
BOOL CViewer::CheckKey(char* pStrKey)
{
    HKEY    hKey = HKEY_CURRENT_USER;
    HKEY    hSubKey;
    
    if( RegOpenKey( hKey, "Software", &hSubKey ) != ERROR_SUCCESS ) {
        RegCloseKey( hKey );
        return FALSE;
    }
    RegCloseKey( hKey );
    hKey = hSubKey;

	char*	pKey;
	char	cTempStr[100];

	strcpy( cTempStr, pStrKey);
	pKey = strtok( cTempStr, "\\");
	while( pKey != NULL )
	{
		if( RegOpenKey( hKey, pKey, &hSubKey ) != ERROR_SUCCESS )
		{
			RegCloseKey( hKey );
			return FALSE;
		}
        RegCloseKey( hKey );
		hKey = hSubKey;
		pKey = strtok( NULL, "\\");
	}

    return TRUE;
}


BOOL CViewer::CreateKey(char* pStrKey)
{
    HKEY    hKey = HKEY_CURRENT_USER;
    HKEY    hSubKey;
    
    if( RegOpenKey( hKey, "Software", &hSubKey ) != ERROR_SUCCESS ) {
        RegCloseKey( hKey );
        return FALSE;
    }
    RegCloseKey( hKey );
    hKey = hSubKey;

	char*	pKey;
	char	cTempStr[100];

	strcpy( cTempStr, pStrKey);
	pKey = strtok( cTempStr, "\\");
	while( pKey != NULL )
	{
		if( RegCreateKey( hKey, pKey, &hSubKey ) != ERROR_SUCCESS )
		{
			RegCloseKey( hKey );
			return FALSE;
		}
        RegCloseKey( hKey );
		hKey = hSubKey;
		pKey = strtok( NULL, "\\");
	}
    return TRUE;

}

BOOL CViewer::SetValue(char* pStrKey, char* pStrValue)
{
	BOOL bRet = TRUE;
    HKEY    hKey = HKEY_CURRENT_USER;
    HKEY    hSubKey;
    
    if( RegOpenKey( hKey, "Software", &hSubKey ) != ERROR_SUCCESS ) {
        RegCloseKey( hKey );
        return FALSE;
    }
    RegCloseKey( hKey );
    hKey = hSubKey;

	char*	pKey;
	char	cTempStr[100];

	strcpy( cTempStr, pStrKey);
	pKey = strtok( cTempStr, "\\");
	while( pKey != NULL )
	{
		if( RegOpenKey( hKey, 
						pKey, 
						&hSubKey ) != ERROR_SUCCESS )
		{
			RegCloseKey( hKey );
			return FALSE;
		}
		hKey = hSubKey;
		pKey = strtok( NULL, "\\");
	}

	BYTE *lpData = (BYTE *)malloc( strlen(pStrValue) + 1 );
	strcpy( (char*)lpData, pStrValue );
    if( RegSetValueEx( hKey, 
					   "Value", 
					   NULL, 
					   REG_SZ, 
					   lpData, 
					   strlen(pStrValue) ) != ERROR_SUCCESS ) {
        RegCloseKey( hKey );
        return FALSE;
    }
	free((BYTE *)lpData);
	RegCloseKey( hKey );

	return bRet;
}

BOOL CViewer::GetValue(char* pStrKey, char* pStrValue)
{
	BOOL blFound = FALSE;


	return TRUE;
}

void CViewer::SetViewerKey(CString strKey)
{
	CFGDATA rlCfg;
	VIEWDATA rlViewData;
	BOOL blFound = FALSE;
	int ilCount = ogCfgData.omViews.GetSize();

	//Search for the key
	for(int i = 0; i < ilCount; i++)
	{
		CString olKey = ogCfgData.omViews[i].Ckey;
		if( olKey == strKey)
			blFound = TRUE;
	}
	if(blFound == FALSE)
	{
		rlViewData.Ckey = strKey;
		ogCfgData.omViews.NewAt(ogCfgData.omViews.GetSize(), rlViewData);
	}
	m_BaseViewName = strKey;

}

BOOL CViewer::CreateView(CString strView, const CStringArray &possibleFilters, bool bpWithDBDelete)
{
	BOOL blFound = FALSE;
	if( m_BaseViewName.IsEmpty() ) return FALSE;
	int ilCount = ogCfgData.omViews.GetSize();

	//Search for the key
	for(int i = 0; i < ilCount; i++)
	{
		if(ogCfgData.omViews[i].Ckey == m_BaseViewName)
		{
			DeleteView(strView, bpWithDBDelete);
			VIEW_VIEWNAMES rlViewNames;
			VIEW_TYPEDATA rlViewTypeData;
			rlViewNames.ViewName = strView;
			rlViewTypeData.Type = CString("FILTER");
			
			for(int ili = 0; ili < possibleFilters.GetSize(); ili++ ) 
			{
				VIEW_TEXTDATA rlTextData;
				rlTextData.Page = possibleFilters.GetAt(ili);
				rlViewTypeData.omTextData.NewAt(rlViewTypeData.omTextData.GetSize(), rlTextData);
			}
			rlViewNames.omTypeData.NewAt(rlViewNames.omTypeData.GetSize(), rlViewTypeData);
			ogCfgData.omViews[i].omNameData.NewAt(ogCfgData.omViews[i].omNameData.GetSize(), rlViewNames);
			blFound = TRUE;
			i = ilCount; // we break at this point
		}
	}
	return blFound;

}

void CViewer::GetViews(CStringArray &strArray)
{
	int ilCount = ogCfgData.omViews.GetSize();

	strArray.RemoveAll();
	for(int i = 0; i < ilCount; i++)
	{
		if(ogCfgData.omViews[i].Ckey == m_BaseViewName)
		{
			int ilC2 = ogCfgData.omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				strArray.Add(ogCfgData.omViews[i].omNameData[j].ViewName);
			}
		}
	}
}

BOOL CViewer::SelectView(CString strView)
{
	BOOL blFound = FALSE;
	int ilC1 = ogCfgData.omViews.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if(ogCfgData.omViews[i].Ckey == m_BaseViewName)
		{
			int ilC2 = ogCfgData.omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(ogCfgData.omViews[i].omNameData[j].ViewName == strView)
				{
					m_ViewName = strView;
					blFound = TRUE;
					j = ilC2; // let's break
					i = ilC1;
				}
			}
		}
	}
	return blFound;
}

CString CViewer::SelectView()
{
	if(m_ViewName != CString(""))
		return m_ViewName;
	for(int i = 0; i < ogCfgData.omViews.GetSize(); i++)
	{
		CString olk = ogCfgData.omViews[i].Ckey;
		if(ogCfgData.omViews[i].Ckey == m_BaseViewName)
		{
			if(ogCfgData.omViews[i].omNameData.GetSize() > 0)
			{
				return ogCfgData.omViews[i].omNameData[0].ViewName;
			}
		}
	}
	return m_ViewName;
}


BOOL CViewer::DeleteView(CString strView, bool bpWithDBDelete)
{
	BOOL blFound = FALSE;
	int ilC1 = ogCfgData.omViews.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if(ogCfgData.omViews[i].Ckey == m_BaseViewName)
		{
			int ilC2 = ogCfgData.omViews[i].omNameData.GetSize();
			for(int j=0; j< ilC2; j++)
			{
				if(ogCfgData.omViews[i].omNameData[j].ViewName == strView)
				{
					VIEW_VIEWNAMES *prlViewName;
					prlViewName = &ogCfgData.omViews[i].omNameData[j];
					int ilC3 = prlViewName->omTypeData.GetSize()-1;
					for(int k = ilC3; k >= 0; k--)
					{
						prlViewName->omTypeData[k].omValues.DeleteAll();
						int ilC4 = prlViewName->omTypeData[k].omTextData.GetSize()-1;
						for(int l = ilC4; l >= 0; l--)
						{
							prlViewName->omTypeData[k].omTextData[l].omValues.DeleteAll();
						}
						prlViewName->omTypeData[k].omTextData.DeleteAll();
					}
					ogCfgData.omViews[i].omNameData[j].omTypeData.DeleteAll(); //New
					ogCfgData.omViews[i].omNameData.DeleteAt(j);
					blFound = TRUE;
					if(bpWithDBDelete == true)
					{
						ogCfgData.DeleteViewFromDiagram(m_BaseViewName, strView);
					}
					j = ilC2;
					i = ilC1; // let's break
				}
			}
		}
	}
	return blFound;
}

BOOL CViewer::DeleteFilter(CString strView)
{
	return TRUE;
}

void CViewer::GetFilterPage(CStringArray &strArray)
{
	VIEW_VIEWNAMES *prlViewName;
	VIEW_TYPEDATA rlTypeData;
	strArray.RemoveAll();

	prlViewName = GetActiveView();
	if(prlViewName == NULL)
		return;
	int ilC1 = prlViewName->omTypeData.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if(prlViewName->omTypeData[i].Type == "FILTER")
		{
			int ilC2 = prlViewName->omTypeData[i].omTextData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				strArray.Add(prlViewName->omTypeData[i].omTextData[j].Page);
			}
		}
	}
}

void CViewer::SetFilter(CString strFilter, const CStringArray &opFilter)
{
	VIEW_TEXTDATA *prlTextData;
	prlTextData = GetActiveFilter(strFilter);
	if(prlTextData == NULL)
		return;
	int ilCount = opFilter.GetSize();
	//first we must delete current set filters
	prlTextData->omValues.DeleteAll();

	if(ilCount == 0)
	{
		prlTextData->omValues.NewAt(prlTextData->omValues.GetSize(), "");
	}
	else
	{
		for(int i = 0; i < ilCount; i++)
		{
			prlTextData->omValues.NewAt(prlTextData->omValues.GetSize(), opFilter.GetAt(i));
		}
	}
}

void CViewer::GetFilter(CString strFilter, CStringArray &opFilter)
{
	opFilter.RemoveAll();
	VIEW_TEXTDATA *prlTextData;
	prlTextData = GetActiveFilter(strFilter);
	if(prlTextData == NULL)
		return;
	for(int i = 0; i < prlTextData->omValues.GetSize(); i++)	
	{
		CString olTmp = prlTextData->omValues[i];
		opFilter.Add(prlTextData->omValues[i]);
	}

}

void CViewer::SetSort(CString opName, const CStringArray &opSort)
{
	VIEW_VIEWNAMES *prlViewName;
	VIEW_TYPEDATA rlTypeData;

	prlViewName = GetActiveView();
	if(prlViewName == NULL)
		return;
	rlTypeData.Type = CString(opName);
	int ilCount = opSort.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		rlTypeData.omValues.NewAt(rlTypeData.omValues.GetSize(), opSort.GetAt(i));
	}
	prlViewName->omTypeData.NewAt(prlViewName->omTypeData.GetSize(), rlTypeData);
}

void CViewer::GetSort(CString opName, CStringArray &opSort)
{

	opSort.RemoveAll();
	VIEW_VIEWNAMES *prlViewName;
	prlViewName = GetActiveView();
	if(prlViewName == NULL)
		return;
	
	int ilC1 = prlViewName->omTypeData.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if(prlViewName->omTypeData[i].Type == opName/*"SORT"*/)
		{
			int ilC2 = prlViewName->omTypeData[i].omValues.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				opSort.Add(prlViewName->omTypeData[i].omValues[j]);
			}
		}
	}

}
void CViewer::SetSearch(CString opSearchName, const CStringArray &opValues)
{
	VIEW_VIEWNAMES *prlViewName;
	VIEW_TYPEDATA rlTypeData;

	prlViewName = GetActiveView();
	if(prlViewName == NULL)
		return;
	rlTypeData.Type = CString(opSearchName);
	int ilCount = opValues.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		CString olT = opValues.GetAt(i);
		rlTypeData.omValues.NewAt(rlTypeData.omValues.GetSize(), opValues.GetAt(i));
	}
	prlViewName->omTypeData.NewAt(prlViewName->omTypeData.GetSize(), rlTypeData);
}

void CViewer::GetSearch(CString opSearchName, CStringArray &opValues)
{
	opValues.RemoveAll();
	VIEW_VIEWNAMES *prlViewName;
	prlViewName = GetActiveView();
	if(prlViewName == NULL)
		return;
	
	int ilC1 = prlViewName->omTypeData.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if(prlViewName->omTypeData[i].Type == opSearchName)
		{
			int ilC2 = prlViewName->omTypeData[i].omValues.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				opValues.Add(prlViewName->omTypeData[i].omValues[j]);
			}
		}
	}
}
void CViewer::GetFlightTimeSpan(CString &ropFrom, CString &ropTo)
{
	CStringArray olValues;
	GetSearch("FLIGHTSEARCH", olValues);
	//GetFilter("UNIFILTER", olValues);
	if(olValues.GetSize() < 5)
		return;
	for(int i = 0; i < olValues.GetSize(); i++)
	{
		if(i == 5)
		{
			ropFrom = olValues[i];
			ropFrom.TrimLeft();
			ropFrom.TrimRight();
		}
		if(i == 6)
		{
			ropTo = olValues[i];
			ropFrom.TrimLeft();
			ropFrom.TrimRight();
		}
	}
}

void CViewer::GetTimeSpanAndFtyp(CTime &ropFrom, CTime &ropTo, CString &ropFtyps)
{
	ropFrom = TIMENULL;
	ropTo = TIMENULL;
	ropFtyps = "";

	CTime olTime;

	bool blTifa = false;
	bool blTifd = false;

	CStringArray olValues;
	GetSearch("FLIGHTSEARCH", olValues);


	if(olValues.GetSize() < 5)
		return;

	for(int i = 0; i < olValues.GetSize(); i++)
	{
		switch (i)
		{
			case 0:// Zeitraum absolut von
				{
					olTime = DBStringToDateTime(olValues[i]);
					if(omModul == "SEASON" && bgSeasonLocal) ogBasicData.LocalToUtc(olTime);
					ropFrom = olTime;
					if(ropFrom != TIMENULL) blTifa = true;
					
				}
				break;
			case 1:// Zeitraum absolut bis
				{
					olTime = DBStringToDateTime(olValues[i]);
					if(omModul == "SEASON" && bgSeasonLocal) ogBasicData.LocalToUtc(olTime);
					ropTo = olTime;
					if(ropTo != TIMENULL) blTifd = true;
				}
				break;
			case 5://before curr
				{

					if(!blTifa)
					{
						CTime olTime = CTime::GetCurrentTime();
						ogBasicData.LocalToUtc(olTime);
						char pclHours[10]="";
						sprintf(pclHours, "%s", olValues[i]);
						olTime -= CTimeSpan((atoi(pclHours)*60*60));
						if((ropFrom == TIMENULL) || (olTime < ropFrom))
							ropFrom = olTime;
					}
				}
				break;
			case 6://
				{
					if(!blTifd)
					{
						CTime olTime = CTime::GetCurrentTime();
						ogBasicData.LocalToUtc(olTime);
						char pclHours[10]="";
						sprintf(pclHours, "%s", olValues[i]);
						olTime += CTimeSpan((atoi(pclHours)*60*60));
						if((ropTo == TIMENULL) || (olTime > ropTo))
							ropTo = olTime;
					}
				}
				break;

			case 20://Betrieb
				if(olValues[i] == "J")
				{
					if(ropFtyps.IsEmpty())					
						ropFtyps += CString("'O'");
					else
						ropFtyps += CString(",'O'");
				}
				break;
			case 21://Planung
				if(olValues[i] == "J")
				{
					if(ropFtyps.IsEmpty())					
						ropFtyps += CString("'S'");
					else
						ropFtyps += CString(",'S'");
				}
				break;
			case 22://Prognose
				if(olValues[i] == "J")
				{
					if(ropFtyps.IsEmpty())					
						ropFtyps += CString("' '");
					else
						ropFtyps += CString(",' '");
					
				}
				break;
			case 23://Canceled
				if(olValues[i] == "J")
				{
					if(ropFtyps.IsEmpty())					
						ropFtyps += CString("'X'");
					else
						ropFtyps += CString(",'X'");
				}
				break;
			case 24:// noop
				if(olValues[i] == "J")
				{
					if(ropFtyps.IsEmpty())					
						ropFtyps += CString("'N'");
					else
						ropFtyps += CString(",'N'");
				}
				break;
			case 25: //Deleted geht hier nicht !!!
				break;
			case 26:// kompl. Rotatation
				break;
				/*
				if(olValues[i] == "J")
				{
					bpRotation = true;
				}
				*/
			case 27:// Towing
				if(olValues[i] == "J")
				{
					if(ropFtyps.IsEmpty())					
						ropFtyps += CString("'T'");
					else
						ropFtyps += CString(",'T'");
				}
				break;
			case 28:// Ground Movement
				if(olValues[i] == "J")
				{
					if(ropFtyps.IsEmpty())					
						ropFtyps += CString("'G'");
					else
						ropFtyps += CString(",'G'");
				}
				break;
			case 29:// Diverted
				if(olValues[i] == "J")
				{
					if(ropFtyps.IsEmpty())					
						ropFtyps += CString("'D'");
					else
						ropFtyps += CString(",'D'");
				}
				break;

			case 30:// Rerouted
				if(olValues[i] == "J")
				{
					if(ropFtyps.IsEmpty())					
						ropFtyps += CString("'R'");
					else
						ropFtyps += CString(",'R'");
				}
				break;
			default:
				break;
		} // end switch
	}

}



void CViewer::SetTimeScale(CStringArray &opValues)
{
	VIEW_VIEWNAMES *prlViewName;
	VIEW_TYPEDATA rlTypeData;

	prlViewName = GetActiveView();
	if(prlViewName == NULL)
		return;
	rlTypeData.Type = CString("TIMESCALE");
	int ilCount = opValues.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		rlTypeData.omValues.NewAt(rlTypeData.omValues.GetSize(), opValues.GetAt(i));
	}
	prlViewName->omTypeData.NewAt(prlViewName->omTypeData.GetSize(), rlTypeData);
}
void CViewer::GetTimeScale(CStringArray &opValues)
{
	opValues.RemoveAll();
	VIEW_VIEWNAMES *prlViewName;
	prlViewName = GetActiveView();
	if(prlViewName == NULL)
		return;
	
	int ilC1 = prlViewName->omTypeData.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if(prlViewName->omTypeData[i].Type == "TIMESCALE")
		{
			int ilC2 = prlViewName->omTypeData[i].omValues.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				opValues.Add(prlViewName->omTypeData[i].omValues[j]);
			}
		}
	}
}


void CViewer::SetGroup(CString strGroup)
{
	VIEW_VIEWNAMES *prlViewName;
	VIEW_TYPEDATA rlTypeData;
	prlViewName = GetActiveView();
	if(prlViewName == NULL)
		return;
	rlTypeData.Type = CString("GROUP");
	rlTypeData.omValues.DeleteAll();
	rlTypeData.omValues.NewAt(rlTypeData.omValues.GetSize(), strGroup);
	prlViewName->omTypeData.NewAt(prlViewName->omTypeData.GetSize(), rlTypeData);


}

CString CViewer::GetGroup()
{
	VIEW_VIEWNAMES *prlViewName;
	prlViewName = GetActiveView();
	if(prlViewName == NULL)
		return CString("");
	
	int ilC1 = prlViewName->omTypeData.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if(prlViewName->omTypeData[i].Type == "GROUP")
		{
			if(prlViewName->omTypeData[i].omValues.GetSize() > 0)
			{
				return prlViewName->omTypeData[i].omValues[0];
			}
			else
			{
				return CString("");
			}
		}
	}
	return CString("");
}

VIEW_VIEWNAMES * CViewer::GetActiveView()
{
	int ilC1 = ogCfgData.omViews.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if(ogCfgData.omViews[i].Ckey == m_BaseViewName)
		{
			int ilC2 = ogCfgData.omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(ogCfgData.omViews[i].omNameData[j].ViewName == m_ViewName)
				{
					return &ogCfgData.omViews[i].omNameData[j];
				}
			}
		}
	}
	return NULL;
}

////////////////////////////////////////////////////////////////////////////
// MWO: 09.10.1996 finds a filter
VIEW_TEXTDATA * CViewer::GetActiveFilter(CString opFilter)
{
	int ilC1 = ogCfgData.omViews.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if(ogCfgData.omViews[i].Ckey == m_BaseViewName)
		{
			int ilC2 = ogCfgData.omViews[i].omNameData.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				if(ogCfgData.omViews[i].omNameData[j].ViewName == m_ViewName)
				{
					int ilC3 = ogCfgData.omViews[i].omNameData[j].omTypeData.GetSize();
					for(int k = 0; k < ilC3; k++)
					{
						if(ogCfgData.omViews[i].omNameData[j].omTypeData[k].Type == "FILTER")
						{
							int ilC4 = ogCfgData.omViews[i].omNameData[j].omTypeData[k].omTextData.GetSize();
							for(int l = 0; l < ilC4; l++)
							{
								if(ogCfgData.omViews[i].omNameData[j].omTypeData[k].omTextData[l].Page == opFilter)
								{
									return &ogCfgData.omViews[i].omNameData[j].omTypeData[k].omTextData[l];
								}
							}
						}
					}
				}
			}
		}
	}
	return NULL;
}


CString CViewer::GetBaseViewName()
{
	return m_BaseViewName;
}

CString CViewer::GetViewName()
{
	return m_ViewName;
}


void CViewer::SafeDataToDB(CString opViewName)
{	
	int ilC1 = ogCfgData.omViews.GetSize();
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	for(int i = 0; i < ilC1; i++)
	{
		if(ogCfgData.omViews[i].Ckey == m_BaseViewName)
		{
			ogCfgData.UpdateViewForDiagram(m_BaseViewName, ogCfgData.omViews[i],opViewName);
			i = ilC1;
		}
	}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}



bool CViewer::GetZeitraumWhereString(CString &ropWhere, bool &bpRotation)
{

	return GetFlightSearchWhereString(ropWhere, bpRotation);
}






bool CViewer::GetFlightWhereString(CString &ropWhere, bool &bpRotation)
{

	bpRotation = false;

	CStringArray olValues;
	CString olWhere;

	GetFlightSearchWhereString(ropWhere, bpRotation);
	

	//Jetzt kommt der UNIFILTER und wird hinten angeh�ngt
    olValues.RemoveAll();
	GetFilter("UNIFILTER", olValues);
	if(olValues.GetSize() > 0)
	{
		if(!ropWhere.IsEmpty())
		{
			CString olTmp = olValues[0];
			if(olValues[0].GetLength() > 4)
				ropWhere += " AND " + CString("(") + olValues[0] + CString(")");
		}
		else
		{
			CString olTmp = olValues[0];
			if(olValues[0].GetLength() > 4)
				ropWhere += olValues[0];
		}
	}
	//Jetzt kommt der SPECIALFILTER und wird hinten angeh�ngt
	olValues.RemoveAll();
	GetFilter("SPECFILTER", olValues);
	int ilCount = olValues.GetSize();
	bool blIsFirst = true;
	CString olPartWhere = CString("");
	for( int i = 0; i < ilCount; i++)
	{
		CString olSubString;
		CStringArray olItems;
		CString olField;
		CString olValue = olValues[i];
		int ilIdx = olValue.Find('@');
		if(ilIdx != -1)
		{
			olValue.SetAt(ilIdx, ' ');
			olValue.TrimRight();
		}
		//Extract the Substring with values;
		olSubString = olValue.Mid(olValue.Find('=')+1, olValue.GetLength( ));
		if(!olSubString.IsEmpty())
		{
			int pos;
			int olPos = 0;
			bool blEnd = false;
			CString olText;
			while(blEnd == false)
			{
				pos = olSubString.Find('|');
				if(pos == -1)
				{
					blEnd = true;
					olText = olSubString;
				}
				else
				{
					olText = olSubString.Mid(0, olSubString.Find('|'));
					olSubString = olSubString.Mid(olSubString.Find('|')+1, olSubString.GetLength( )-olSubString.Find('|')+1);
				}
				olItems.Add(olText);
			}
		}


		if(olValue.Find("Abfertigungsarten") != -1) //hty.hnam => aft.htyp
		{
			for(int j = 0; j < olItems.GetSize(); j++)
			{
				if(blIsFirst == true)
				{
					blIsFirst = false;
				}
				else
				{
					olPartWhere += " OR ";
				}
				olPartWhere += "HTYP = '" + olItems[i] + CString("' ");
			}
		}
		if(olValue.Find("Airlines") != -1) // alt.alc3 => aft. alc3
		{
			for(int j = 0; j < olItems.GetSize(); j++)
			{
				if(blIsFirst == true)
				{
					blIsFirst = false;
				}
				else
				{
					olPartWhere += " OR ";
				}
				olPartWhere += "ALC3 = '" + olItems[i] + CString("' ");
			}
		}
		if(olValue.Find("Airports") != -1) // apt.Apc3 => aft.Apc3
		{
			for(int j = 0; j < olItems.GetSize(); j++)
			{
				if(blIsFirst == true)
				{
					blIsFirst = false;
				}
				else
				{
					olPartWhere += " OR ";
				}
				olPartWhere += "APC3 = '" + olItems[i] + CString("' ");
			}
		}
		if(olValue.Find("Ausg�nge") != -1) // ext.Enam => aft.ext1 OR aft.ext2
		{
			for(int j = 0; j < olItems.GetSize(); j++)
			{
				if(blIsFirst == true)
				{
					blIsFirst = false;
				}
				else
				{
					olPartWhere += " OR ";
				}
				olPartWhere += "EXT1 = '" + olItems[i] + CString("' OR EXT2 = '") + olItems[i] + CString("' ");
			}
		}
		if(olValue.Find("Delay-Codes") != -1) // den.Deca => aft.dcd1 OR aft.dcd2 OR aft.dtd1 OR aft.dtd2
		{
			for(int j = 0; j < olItems.GetSize(); j++)
			{
				if(blIsFirst == true)
				{
					blIsFirst = false;
				}
				else
				{
					olPartWhere += " OR ";
				}
				olPartWhere += "DCD1 = '" + olItems[i] + CString("' OR DCD2 = '") + olItems[i] + 
							   CString("' OR DTD1 = '") + olItems[i] + CString("' OR DTD2 = '") 
							    + olItems[i] + CString("' ");
			}
		}
		if(olValue.Find("Gates") != -1) // gat.Gnam => aft.gta1 OR aft gta2 OR aft.gtd1 OR aft.gtd2
		{
			for(int j = 0; j < olItems.GetSize(); j++)
			{
				if(blIsFirst == true)
				{
					blIsFirst = false;
				}
				else
				{
					olPartWhere += " OR ";
				}
				olPartWhere += "GTA1 = '" + olItems[i] + CString("' OR GTA2 = '") + olItems[i] + 
							   CString("' OR GTD1 = '") + olItems[i] + CString("' OR GTD2 = '") + 
							    olItems[i] + CString("' ");
			}
		}
		if(olValue.Find("Gep�ckb�nder") != -1) //blt.Bnam => aft.blt1 OR aft.blt2
		{
			for(int j = 0; j < olItems.GetSize(); j++)
			{
				if(blIsFirst == true)
				{
					blIsFirst = false;
				}
				else
				{
					olPartWhere += " OR ";
				}
				olPartWhere += "BLT1 = '" + olItems[i] + CString("' OR BLT2 = '") + olItems[i] + CString("' ");
			}
		}
		if(olValue.Find("Positionen") != -1) //pst.Pnam => aft.pstd OR aft .psta
		{
			blIsFirst = true;
			for(int j = 0; j < olItems.GetSize(); j++)
			{
				if(blIsFirst == true)
				{
					blIsFirst = false;
				}
				else
				{
					olPartWhere += " OR ";
				}
				olPartWhere += "PSTA = '" + olItems[i] + CString("' OR PSTD = '") + olItems[i] + CString("' ");
			}
		}
		if(olValue.Find("Verkehrsarten") != -1) //Nat.ttyp => aft.ttyp
		{
			for(int j = 0; j < olItems.GetSize(); j++)
			{
				if(blIsFirst == true)
				{
					blIsFirst = false;
				}
				else
				{
					olPartWhere += " OR ";
				}
				olPartWhere += "TTYP = '" + olItems[i] + CString("' ");
			}
		}
		if(olValue.Find("Warter�ume") != -1) //wro.Wnam => aft.wro1 
		{
			for(int j = 0; j < olItems.GetSize(); j++)
			{
				if(blIsFirst == true)
				{
					blIsFirst = false;
				}
				else
				{
					olPartWhere += " OR ";
				}
				olPartWhere += "WRO1 = '" + olItems[i] + CString("' ");
			}
		}
		olItems.RemoveAll();
	}

	if(!olPartWhere.IsEmpty() && !ropWhere.IsEmpty())
	{
		ropWhere += " AND " + CString("(") + olPartWhere + CString(")");
	}
	if(!olPartWhere.IsEmpty() && ropWhere.IsEmpty())
	{
		ropWhere = olPartWhere;
	}



	if((ropWhere.Find("FTYP") < 0) || (!((ropWhere.Find("STO") >= 0) )))
		ropWhere = "";

	if(ropWhere.IsEmpty())
	{
		return false;
	}
	else
	{
		bool blFound = true;
		
		//replace '@' with ' '
		while(blFound == true)
		{
			int pos = ropWhere.Find("@");
			if(pos != -1)
				ropWhere.SetAt(pos, ' ');
			else
				blFound = false;
		}

		return true;
	}
}





bool CViewer::GetFlightSearchWhereString(CString &ropWhere, bool &bpRotation, CTime opFrom, CTime opTo)
{


	bpRotation = false;

	CStringArray olValues;
	CString olWhere;
	CString olTmpTifaWhere;
	CString olTmpTifdWhere;
	CString olTmpFtypWhere;
	bool blIsTifSet = false;
	bool blIsCase0 = false;
	CTime olStrTime;
	bool blAdidA = false;
	bool blAdidB = false;
	bool blAdidD = false;

	CString olDOOD;
	CString olDOOA;

	CTime olFrom = TIMENULL;
	CTime olTo = TIMENULL;


	VIEW_VIEWNAMES *prlViewName;
	prlViewName = GetActiveView();
	if(prlViewName == NULL)
		return false;

	int ilC1 = prlViewName->omTypeData.GetSize();
	for(int i = 0; i < ilC1; i++)
	{
		if(prlViewName->omTypeData[i].Type == "FLIGHTSEARCH")
		{
			int ilC2 = prlViewName->omTypeData[i].omValues.GetSize();
			for(int j = 0; j < ilC2; j++)
			{
				olValues.Add(prlViewName->omTypeData[i].omValues[j]);
			}
		}
	}

// TIFA From || TIFD From; Date
// TIFA From || TIFD From; Time
// TIFA To || TIFD To; Date
// TIFA To || TIFD To; Time
// SearchHours
// SearchDay
// DOOA || DOOD
// RelHBefore
// RelHAfter
// LSTU; Date
// LSTU; Time
// FDAT; Date
// FDAT; Time
// ACL2
// FLTN
// FLNS
// REGN
// ACT3
// ORG3 || ORG4 || DES3 || DES4
// TIFA Flugzeit || TIFD Flugzeit; Date
// TIFA Flugzeit || TIFD Flugzeit; Time
// TTYP
// STEV


	//CString ropWhere;
	if(olValues.GetSize() < 26)
	{
		if(olValues.GetSize() == 0)
		{
			for(int j = 0; j < 26; j++)
			{
				olValues.Add(CString(""));
			}
		}
		else if(olValues.GetSize() > 0)
		{
			for(int j = olValues.GetSize()-1; j < 26; j++)
			{
				olValues.Add(CString(""));
			}
		}
	}
	bool blIsFirst = true;
	for( i = 0; i < olValues.GetSize(); i++)
	{
		CString olTmpVal = olValues[i];
		if(olValues[i] != CString("") && olValues[i] != CString(" "))
		{
			if(blIsFirst == true)
			{
				blIsFirst = false;
			}
			else
			{
				if(i <= 17)
				{
					if( i == 0 || i == 1 || i == 2 || i == 3 || i == 4 || i == 5 || i == 6 )
					{
						; // Pr�fung erfolgt unten
					}
					else
					{
						ropWhere += omFlightSearchConnection[i];
					}
				}
			}
			switch (i)
			{
			case 0: // Zeitraum absolut von
				{
				olFrom = DBStringToDateTime(olValues[i]);
				if(omModul == "SEASON" && bgSeasonLocal) ogBasicData.LocalToUtc(olFrom);
				}
				break;
			case 1: //Zeitraum bis
				olTo = DBStringToDateTime(olValues[i]);
				if(omModul == "SEASON" && bgSeasonLocal)  ogBasicData.LocalToUtc(olTo);
				break;
			case 2: // TO DO Stunden ???
				break;
			case 3: //TO DO Tage ???
				break;
			case 4: //DOOA / DOOD
				{
						if(olValues[i].GetLength() > 0)
						{
							if(olValues[i].GetLength() == 1)
								olDOOA += " DOOA = '" + olValues[i] + CString("' ");
							else
							{
								olDOOA += " DOOA IN (";
								CString olTmp = olValues[i];
								for(int k = 0; k < olTmp.GetLength(); k++)
								{
									olDOOA += CString("'") + olTmp[k] + CString("',");
								}
								olDOOA = olDOOA.Left(olDOOA.GetLength() - 1);
								olDOOA += ") ";
							}
						}

						if(olValues[i].GetLength() > 0)
						{
							if(olValues[i].GetLength() == 1)
								olDOOD += " DOOD = '" + olValues[i] + CString("' ");
							else
							{
								olDOOD += "DOOD IN (";
								CString olTmp = olValues[i];
								for(int k = 0; k < olTmp.GetLength(); k++)
								{
									olDOOD += CString("'") + olTmp[k] + CString("',");
								}
								olDOOD = olDOOD.Left(olDOOD.GetLength() - 1);
								olDOOD += ")";
							}
						}
				}
				break;
			case 5: //Rel. Stunden vor akt. Zeit ==> ist eigentlich tifa und tifd
				{
					if(olFrom == TIMENULL)
					{
						olFrom = CTime::GetCurrentTime();
						//RST!
						ogBasicData.LocalToUtc(olFrom);

						char pclHours[10]="";
						sprintf(pclHours, "%s", olValues[i]);
						olFrom -= CTimeSpan((atoi(pclHours)*60*60));
					}
				}
				break;
			case 6: //Rel. Stunden nach akt. Zeit
				{
					if(olTo == TIMENULL)
					{
						olTo = CTime::GetCurrentTime();
						//RST!
						ogBasicData.LocalToUtc(olTo);
						char pclHours[10]="";
						sprintf(pclHours, "%s", olValues[i]);
						olTo += CTimeSpan((atoi(pclHours)*60*60));
					}
				}
				break;
			case 7: // LSTU
				ropWhere += "LSTU >= '" + olValues[i] + CString("' ");
				break;
			case 8: // FDAT 
				ropWhere += "FDAT >= '" + olValues[i] + CString("' ");
				break;
			case 9: // ALC2
				if(olValues[i].GetLength() > 2)
				{
					ropWhere += "ALC3 = '" + olValues[i] + CString("' ");
				}
				else
				{
					ropWhere += "ALC2 = '" + olValues[i] + CString("' ");
				}
				break;
			case 10: //FLTN Alles incl. Suffix
				{
					CString olFltn = olValues[i];
					if(olFltn.GetLength() < 3)
					{
						char pclTmp[10]="";
						sprintf(pclTmp, "%03s", olFltn);
						olFltn = CString(pclTmp);
					}
					ropWhere += "FLTN = '" + olFltn/*olValues[i]*/ + CString("' ");
				}
				break;
			case 11: // TO DO FLTS  aber wie??
				ropWhere += "FLNS = '" + olValues[i] + CString("' ");
				break;
			case 12: //REGN
				ropWhere += "REGN = '" + olValues[i] + CString("' ");
				break;
			case 13: // ACT3/ACT4
				if( olValues[i].GetLength() > 3)
				{
					ropWhere += "ACT5 = '" + olValues[i] + CString("' ");
				}
				else
				{
					ropWhere += "ACT3 = '" + olValues[i] + CString("' ");
				}
				break;
			case 14: //ORG3/4 DES3/4
				{
					CString olT1;
					bool blAnd = false;
					if (olValues[18] == "J")
					{
						blAnd = true;
						olT1 += CString("ORG") + CString((olValues[i].GetLength() > 3 ? "4" : "3")) + 
									  CString(" = '") + olValues[i] + CString("' ");
					}
					if (olValues[19] == "J")
					{
						if(blAnd == true)
						{
							olT1 += "OR ";
						}
						olT1 += CString("DES") + CString((olValues[18].GetLength() > 3 ? "4" : "3")) +
									  CString(" = '") + olValues[i] + CString("'");// + CString("')");
					}
					if(!olT1.IsEmpty())
					{
						ropWhere += CString("(") + olT1 + CString(")");
					}
				}
				break;
			case 15: //Flugzeit
				{
					bool blAnd = false;
					if (olValues[18] == "J")
					{
						blAnd = true;
						ropWhere += "TIFA = '" + olValues[i] + CString("' ");
					}
					if (olValues[19] == "J")
					{
						if(blAnd == true)
						{
							ropWhere += "OR ";
						}
						ropWhere += "TIFD = '" + olValues[i] + CString("' ");
					}
				}
				break;
			case 16: //TTYP
				ropWhere += "TTYP = '" + olValues[i] + CString("' ");
				break;
			case 17: // STEV
				ropWhere += "STEV = '" + olValues[i] + CString("' ");
				break;
			case 18://Nix ==> nur indicator f�r Anflug
				blAdidA = true;
				blAdidB = true;
				break; 
			case 19://Nix ==> nur indicator f�r Abflug
				blAdidD = true;
				blAdidB = true;
				break; 
			case 20://Betrieb
				if(olValues[i] == "J")
				{
					if(olTmpFtypWhere.IsEmpty())					
						olTmpFtypWhere += CString("'O'");
					else
						olTmpFtypWhere += CString(",'O'");
				}
				break;
			case 21://Planung
				if(olValues[i] == "J")
				{
					if(olTmpFtypWhere.IsEmpty())					
						olTmpFtypWhere += CString("'S'");
					else
						olTmpFtypWhere += CString(",'S'");
				}
				break;
			case 22://Prognose
				if(olValues[i] == "J")
				{
					if(olTmpFtypWhere.IsEmpty())					
						olTmpFtypWhere += CString("' '");
					else
						olTmpFtypWhere += CString(",' '");
					
				}
				break;
			case 23://Canceled
				if(olValues[i] == "J")
				{
					if(olTmpFtypWhere.IsEmpty())					
						olTmpFtypWhere += CString("'X'");
					else
						olTmpFtypWhere += CString(",'X'");
				}
				break;
			case 24:// noop
				if(olValues[i] == "J")
				{
					if(olTmpFtypWhere.IsEmpty())					
						olTmpFtypWhere += CString("'N'");
					else
						olTmpFtypWhere += CString(",'N'");
				}
				break;
			case 25: //Deleted geht hier nicht !!!
				break;
			case 26:// kompl. Rotatation
				if(olValues[i] == "J")
				{
					bpRotation = true;
				}
				break;
			case 27:// Towing
				if(olValues[i] == "J")
				{
					if(olTmpFtypWhere.IsEmpty())					
						olTmpFtypWhere += CString("'T'");
					else
						olTmpFtypWhere += CString(",'T'");
				}
				break;
			case 28:// Ground Movement
				if(olValues[i] == "J")
				{
					if(olTmpFtypWhere.IsEmpty())					
						olTmpFtypWhere += CString("'G'");
					else
						olTmpFtypWhere += CString(",'G'");
				}
				break;
			case 29:// Diverted
				if(olValues[i] == "J")
				{
					if(olTmpFtypWhere.IsEmpty())					
						olTmpFtypWhere += CString("'D'");
					else
						olTmpFtypWhere += CString(",'D'");
				}
				break;

			case 30:// Rerouted
				if(olValues[i] == "J")
				{
					if(olTmpFtypWhere.IsEmpty())					
						olTmpFtypWhere += CString("'R'");
					else
						olTmpFtypWhere += CString(",'R'");
				}
				break;
			default:
				break;
			} // end switch
		}
	}


	if((olTo == TIMENULL) || (olFrom == TIMENULL))
	{
		ropWhere = "";
		return false;

	}


	CString olFromStr;
	CString olToStr;
	CString olZeitRaumA;
	CString olZeitRaumD;


	if(opFrom != TIMENULL && opTo != TIMENULL)
	{
		olFromStr = CTimeToDBString(opFrom, TIMENULL);
		olToStr =	CTimeToDBString(opTo,   TIMENULL);
	}
	else
	{
		olFromStr = CTimeToDBString(olFrom, TIMENULL);
		olToStr =	CTimeToDBString(olTo,   TIMENULL);
	}



	if(omModul == "SEASON" )
	{
			olZeitRaumA =		CString("(STOA BETWEEN '") +
								olFromStr + CString("' AND '") + olToStr + CString("'") +
								CString(" AND DES3 = '") + CString(pcgHome) + CString("')");


			olZeitRaumD =		CString("(STOD BETWEEN '") +
								olFromStr + CString("' AND '") + olToStr + CString("'") +
								CString(" AND ORG3 = '") + CString(pcgHome) + CString("')");
	}
	else
	{


			olZeitRaumA =		CString("((STOA BETWEEN '") +
								olFromStr + CString("' AND '") + olToStr + CString("'") +
								CString(" AND DES3 = '") + CString(pcgHome) + CString("') OR (") +
								CString("TIFA BETWEEN '") +
								olFromStr + CString("' AND '") + olToStr + CString("'") +
								CString(" AND DES3 = '") + CString(pcgHome) + CString("'))");

			olZeitRaumD =		CString("((STOD BETWEEN '") +
								olFromStr + CString("' AND '") + olToStr + CString("'") +
								CString(" AND ORG3 = '") + CString(pcgHome) + CString("') OR (") +
								CString("TIFD BETWEEN '") +
								olFromStr + CString("' AND '") + olToStr + CString("'") +
								CString(" AND ORG3 = '") + CString(pcgHome) + CString("'))");


	}



	CString olRet;


	if(blAdidA && !blAdidD)
	{
		olRet += olZeitRaumA;
	}
	else
	{
		if(!blAdidA && blAdidD)
		{
			olRet += olZeitRaumD;
		}
		else
		{
			olRet += CString("(") + olZeitRaumA + CString(" OR ") + olZeitRaumD + CString(")");
		}
	}


	if(opFrom == TIMENULL && opTo == TIMENULL)	
	{
		if(blAdidA && !blAdidD)
		{
			if(!olDOOA.IsEmpty())
			{
				olRet  += CString(" AND ") + olDOOA;
			}
		}
		else
		{
			if(!blAdidA && blAdidD)
			{
				if(!olDOOD.IsEmpty())
				{
					olRet  += CString(" AND ") + olDOOD;
				}
			}
			else
			{
				if(!olDOOD.IsEmpty() && !olDOOA.IsEmpty())
				{
					olRet += CString(" AND (") + olDOOA + CString(" OR ") + olDOOD + CString(")");
				}
			}
		}
	}
	
	if(!olTmpFtypWhere.IsEmpty())
	{
		olRet += CString(" AND FTYP IN ( ") + olTmpFtypWhere + CString(")");
	}

	if(!ropWhere.IsEmpty())
	{
		olRet +=  ropWhere;
	}


	ropWhere = olRet;


	return true;

}




int CViewer::GetDOO()
{
	int ilRet = -1;

	CStringArray olValues;
	GetSearch("FLIGHTSEARCH", olValues);


	if(olValues.GetSize() < 5)
		return ilRet;


	CString olDOO = olValues[4];

	olDOO.TrimRight();


	if(olDOO.GetLength() == 1)
	{
		ilRet = atoi(olDOO);
	}

	return ilRet;
}

