// ReportSeasonTableViewer.cpp : implementation file
// 
// Modification History: 


#include <stdafx.h>
#include <ReportSeasonTableViewer.h>
#include <CcsGlobl.h>
#include <BasicData.h>
#include <CedaBasicData.h>
#include <CcaCedaFlightData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif 


static int CompareFlightsTifd(const CCAFLIGHTDATA **e1, const CCAFLIGHTDATA **e2)
{
	if ((**e1).Tifd < (**e2).Tifd)
		return -1;
	else if ((**e1).Tifd > (**e2).Tifd)
		return 1;
	else
		return 0;
}


int ReportSeasonTableViewer::imTableColCharWidths[REPORTSEASONTABLE_COLCOUNT]={12, 5, 19, 3, 23, 4, 3, 7, 6, 9};


/////////////////////////////////////////////////////////////////////////////
// ReportSeasonTableViewer
//

ReportSeasonTableViewer::ReportSeasonTableViewer() :
	imOrientation(PRINT_LANDSCAPE),
	omPrint(NULL, imOrientation, 45),
	romTableHeaderFont(ogCourier_Bold_10), romTableLinesFont(ogCourier_Regular_9),
	fmTableHeaderFontWidth(10), fmTableLinesFontWidth(7.5),
	romPrintHeaderFont(omPrint.ogCourierNew_Bold_12), romPrintLinesFont(omPrint.ogCourierNew_Regular_12),
	fmPrintHeaderFontWidth(10), fmPrintLinesFontWidth(10)
	
{

	dgCCSPrintFactor = 2.7 ;

	// Table header strings
	omTableHeadlines[0]=GetString(IDS_STRING347);	// Flugnummer
	omTableHeadlines[1]=GetString(IDS_STRING1504);	// Ftyp 
	omTableHeadlines[2]=GetString(IDS_VIA);			// Via  
	omTableHeadlines[3]=GetString(IDS_STRING1693);	// Dest 
	omTableHeadlines[4]=GetString(IDS_FROMTODAYS);		// Von - Bis / Tage
	omTableHeadlines[5]=GetString(IDS_STOD);		// Stod
	omTableHeadlines[6]=GetString(IDS_COUNT);		// Anz
	omTableHeadlines[7]=GetString(IDS_COUNTER);		// Schalter 
	omTableHeadlines[8]=GetString(IDS_DURATION);	// Dauer
	omTableHeadlines[9]=GetString(IDS_FROMTO);		// Von / Bis  

	// calculate table column widths
	for (int i=0; i < REPORTSEASONTABLE_COLCOUNT; i++)
	{
 		imTableColWidths[i] = (int) max(imTableColCharWidths[i]*fmTableLinesFontWidth, 
								        omTableHeadlines[i].GetLength()*fmTableHeaderFontWidth);
 		imPrintColWidths[i] = (int) max(imTableColCharWidths[i]*fmPrintLinesFontWidth, 
								        omTableHeadlines[i].GetLength()*fmPrintHeaderFontWidth);
	}


	pomGXGridWnd = NULL;
}



ReportSeasonTableViewer::~ReportSeasonTableViewer()
{
     DeleteAll();
}


// Connects the viewer with a table
void ReportSeasonTableViewer::Attach(CGXGridWnd *popAttachWnd)
{
    pomGXGridWnd = popAttachWnd;

	// Set font for headers
	LOGFONT olLogFont;
	romTableHeaderFont.GetLogFont(&olLogFont);
	pomGXGridWnd->ChangeColHeaderStyle(
		CGXStyle().SetFont(CGXFont(olLogFont))
					.SetHorizontalAlignment(DT_CENTER));

	// Set the columns width
	pomGXGridWnd->SetColWidth(1, REPORTSEASONTABLE_COLCOUNT, 0, imTableColWidths);


	// Set font for table lines
	romTableLinesFont.GetLogFont(&olLogFont);
	pomGXGridWnd->ChangeStandardStyle(
		CGXStyle().SetFont(CGXFont(olLogFont))
					.SetHorizontalAlignment(DT_LEFT));

}


// Clears the table of the viewer
void ReportSeasonTableViewer::ClearAll()
{
    DeleteAll();
    pomGXGridWnd->SetRowCount(1);
	
}


void ReportSeasonTableViewer::DeleteAll()
{
    omLines.DeleteAll();
}



void ReportSeasonTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
	pomGXGridWnd->RemoveRows(ipLineno, ipLineno);
}




// Change the viewed database 
void ReportSeasonTableViewer::ChangeViewTo(CcaDiagramViewer &ropViewer)
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

 	CString olFtyps;	
	ropViewer.GetTimeSpanAndFtyp(omMinDate, omMaxDate, olFtyps);

	pomGXGridWnd->LockUpdate(TRUE);
	pomGXGridWnd->SetReadOnly(false);
	DrawHeader();
	ClearAll();
  	MakeLines(ropViewer);
 	// Delete last line
	/*
	if (pomGXGridWnd->GetRowCount() > 0)
	{
		pomGXGridWnd->RemoveRows(pomGXGridWnd->GetRowCount(), pomGXGridWnd->GetRowCount());
	}
	*/
	//pomGXGridWnd->ResizeColWidthsToFit(CGXRange( ).SetTable());
	pomGXGridWnd->SetReadOnly(true);
   	pomGXGridWnd->LockUpdate(FALSE);
	pomGXGridWnd->Redraw();

 	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

}


// Transfer the data of the database to the intern data structures
void ReportSeasonTableViewer::MakeLines(CcaDiagramViewer &ropViewer)
{ 
 
	//// Creates the lines intern first
	REPORTSEASONTABLE_LINEDATA rlLineData;
	bool blMakeLineData=false;
 
	//CUIntArray olGroups, olLines;	
	
	CCSPtrArray<DIACCADATA> olCcas;

	for(int j = ogCcaDiaFlightData.omCcaData.omKKeyArray.GetSize() -1 ; j >= 0; j --)
	{
		olCcas.RemoveAll();
		if(ogCcaDiaFlightData.omCcaData.GetCcasByKKey(olCcas, ogCcaDiaFlightData.omCcaData.omKKeyArray[j]))
		{
			// only counters with valid counter number
			if (olCcas[0].Ckic[0]==' ' || olCcas[0].Ckic[0]=='\0') continue;
 
			if (MakeLineData(rlLineData, olCcas))
			{
				CreateLine(rlLineData);
			}
		}
	}


	CompressCcaLines();

	// Display the lines
	UpdateDisplay();
}



bool ReportSeasonTableViewer::CompressCcaLines(void) 
{
	int ilCicf;
	int ilCict;

	for (int i=0; i < omLines.GetSize(); i++)
	{
		ilCicf = atoi( omLines[i].Cicf );
		ilCict = atoi( omLines[i].Cict );

		for (int j=0; j < omLines.GetSize(); j++)
		{
			// K�nnen die beiden Linien zusammengefa�t werden?
			if (omLines[i].Alc == omLines[j].Alc &&
				omLines[i].Flno == omLines[j].Flno &&
				omLines[i].Act == omLines[j].Act &&
				omLines[i].ViaOut == omLines[j].ViaOut &&
				omLines[i].Freq == omLines[j].Freq &&
				omLines[i].Ckbs == omLines[j].Ckbs &&
				omLines[i].Ckes == omLines[j].Ckes)
			{
				// Sind Schalternummern fortlaufend?
				if ( atoi(omLines[j].Cict) + 1 == ilCicf ||
					 atoi(omLines[j].Cicf) - 1 == ilCict)
				{	
					// Anzahl der Schalter addieren
					omLines[i].CAnz += omLines[j].CAnz;
					if (atoi(omLines[j].Cict) + 1 == ilCicf)
					{
						// �ffnungzeitpunkt anpassen
						omLines[i].Cicf = omLines[j].Cicf;
					}
					else
					{
						// Schlie�ungszeitpunkt anpassen
						omLines[i].Cict = omLines[j].Cict;
					}
					// Schalterbelegung l�schen
					omLines.DeleteAt(j);
					// Wenn sich der Schalter vor dem untersuchten Schalter im Array befunden hat,
					//   Z�hler entsprechend anpassen
					if ( j < i ) i --;
					// Gleiche Schalterbelegung nochmals untersuchen
					i--;
					break;

				}
			}
		}
	}


	return true;
}



// Copy the data from the db-record to the table-record
bool ReportSeasonTableViewer::MakeLineData(REPORTSEASONTABLE_LINEDATA &rrpLineData, const CCSPtrArray<DIACCADATA> &ropCcas) const
{

	// Only blocked and common check-in counter
	if (ropCcas[0].Ctyp[0] == 'N' || ropCcas[0].Ctyp[0] == 'C')
	{
		rrpLineData.BlkCom = true;
		rrpLineData.Flnu = 0;
		rrpLineData.Alc = "";
		rrpLineData.Flno = "";
		if (ropCcas[0].Ctyp[0] == 'N')
		{
 			rrpLineData.Act = "BLK";
		}
		else
		{
			// Get Airline-Code
			CString olTmpStr;
			olTmpStr.Format("%ld", ropCcas[0].Flnu);
			if (!ogBCD.GetField("ALT", "URNO", olTmpStr, "ALC2", rrpLineData.Alc))
			{
				rrpLineData.Alc.Empty();
			}
			rrpLineData.Act = "COM";

			CCSPtrArray<DEMANDDATA> ropDemands;
			for (int ilLc = 0; ilLc < ropCcas.GetSize(); ilLc++)
			{
				DEMANDDATA *prlDemand = ogCcaDiaFlightData.omDemandData.GetDemandByUrno(ropCcas[ilLc].Ghpu);
				if (prlDemand)
					ropDemands.Add(prlDemand);
			}

			CString olText = ogCcaDiaFlightData.omDemandData.GetTextFromCciDemand(ropDemands);
			if (olText.IsEmpty())
				rrpLineData.Flno = "COMMON";
			else
				rrpLineData.Flno = olText;



		}
		DIACCADATA t = ropCcas[0];
		rrpLineData.ViaOut = ropCcas[0].Disp;
		rrpLineData.Des = "";
		rrpLineData.Stod = ropCcas[0].Ckes;
//		rrpLineData.Freq.Empty();
		if(ropCcas[0].Freq > 0)
			rrpLineData.Freq.Format("%d", ropCcas[0].Freq / 7);
		else
			rrpLineData.Freq.Format("?");

		rrpLineData.CAnz = 1;

	}
	else
	{
		// Get flight record
		const CCAFLIGHTDATA *prlFlight = ogCcaDiaFlightData.GetFlightByUrno(ropCcas[0].Flnu);
		if (!prlFlight) return false;

		rrpLineData.BlkCom = false;
		rrpLineData.Flnu = ropCcas[0].Flnu;

		rrpLineData.Alc = prlFlight->Alc2;
		if( CString(prlFlight->Alc2).IsEmpty() || Is2LtrCode(prlFlight->Alc2) )
		{
			rrpLineData.Alc += CString("/")+prlFlight->Alc3 ;
		}
		rrpLineData.Flno = prlFlight->Fltn;
		if (strcmp( prlFlight->Flns, " " ) != 0) 
		{
			rrpLineData.Flno += prlFlight->Flns;
		}
		rrpLineData.Act = prlFlight->Act5;
		GetVias(rrpLineData.ViaOut, *prlFlight);
		rrpLineData.Des = prlFlight->Des3;
		rrpLineData.Stod = prlFlight->Tifd;

		if(prlFlight->Freq > 0)
		{
			rrpLineData.Freq.Format("%d", prlFlight->Freq / 7);
		}
		else
		{
			rrpLineData.Freq.Format("?");
		}

 		rrpLineData.CAnz = 1;
	}

	
	rrpLineData.Cicf = ropCcas[0].Ckic;
	rrpLineData.Cict = ropCcas[0].Ckic;

	rrpLineData.Ckbs = ropCcas[0].Ckbs;
	rrpLineData.Ckes = ropCcas[ropCcas.GetSize()-1].Ckes;
	
	if(bgReportLocal)
	{
		ogBasicData.UtcToLocal(rrpLineData.Stod);
		ogBasicData.UtcToLocal(rrpLineData.Ckbs);
		ogBasicData.UtcToLocal(rrpLineData.Ckes);
	}


	return true;		
}



int ReportSeasonTableViewer::CreateLine(const REPORTSEASONTABLE_LINEDATA &rrpLine)
{
    for (int ilLineno = omLines.GetSize(); ilLineno > 0; ilLineno--)
	{
		if (CompareLines(&rrpLine, &omLines[ilLineno-1]) >= 0)
		{
            break;  // should be inserted after Lines[ilLineno-1]
		}
	}

    omLines.NewAt(ilLineno, rrpLine);
	return ilLineno;
}



/////////////////////////////////////////////////////////////////////////////
// ReportSeasonTableViewer - display drawing routine



// UpdateDisplay: Load data to the display by using "omTable"
void ReportSeasonTableViewer::UpdateDisplay()
{
	pomGXGridWnd->SetRowCount(0);
 
	DrawHeader();
    
	for (int ilLc = 0; ilLc < omLines.GetSize(); ilLc++)
	{
 		InsertTextLine(ilLc, omLines[ilLc]);
	}


}

 


void ReportSeasonTableViewer::DrawHeader()
{

	for (int i=1; i<=REPORTSEASONTABLE_COLCOUNT; i++)
	{
 		pomGXGridWnd->SetStyleRange(CGXRange(0, i), CGXStyle( ).SetValue(omTableHeadlines[i-1]));	
	}
  
	pomGXGridWnd->UpdateStyleRange(CGXRange(0,1,0,REPORTSEASONTABLE_COLCOUNT));
}




// Fills one row of the table
bool ReportSeasonTableViewer::InsertTextLine(int ipLineNo, const REPORTSEASONTABLE_LINEDATA &rrpLine) const
{
	
	ipLineNo++; // for the header

	// Insert new row
	pomGXGridWnd->InsertRows(ipLineNo, 1);   
	pomGXGridWnd->SetStyleRange(CGXRange(ipLineNo, 0, ipLineNo, REPORTSEASONTABLE_COLCOUNT), CGXStyle( ).SetControl(GX_IDS_CTRL_STATIC));   

	pomGXGridWnd->SetStyleRange(CGXRange().SetCols(7), 
		CGXStyle().SetHorizontalAlignment(DT_RIGHT));
	pomGXGridWnd->SetStyleRange(CGXRange().SetCols(9), 
		CGXStyle().SetHorizontalAlignment(DT_RIGHT));
 	
	// Fill new row
	CString olTmpStr;
	olTmpStr.Format("%-7s%s", rrpLine.Alc, rrpLine.Flno);
	pomGXGridWnd->SetValueRange(CGXRange(ipLineNo, 1), olTmpStr); // Airline + Flugnummer
	pomGXGridWnd->SetValueRange(CGXRange(ipLineNo, 2), rrpLine.Act); // FTyp
	pomGXGridWnd->SetValueRange(CGXRange(ipLineNo, 3), rrpLine.ViaOut); // Via
	pomGXGridWnd->SetValueRange(CGXRange(ipLineNo, 4), rrpLine.Des);
	if (rrpLine.BlkCom)
	{
		if (rrpLine.Act == "BLK")
			olTmpStr.Format("%s - %s", rrpLine.Ckbs.Format("%d.%m.%y"), rrpLine.Ckes.Format("%d.%m.%y"));
		else
			olTmpStr.Format("%s - %s / %s", rrpLine.Ckbs.Format("%d.%m.%y"), rrpLine.Ckes.Format("%d.%m.%y"), rrpLine.Freq);
	}
	else
	{
		olTmpStr.Format("%s - %s / %s", rrpLine.Ckbs.Format("%d.%m.%y"), rrpLine.Ckes.Format("%d.%m.%y"), rrpLine.Freq);
	}
	pomGXGridWnd->SetValueRange(CGXRange(ipLineNo, 5), olTmpStr);
	pomGXGridWnd->SetValueRange(CGXRange(ipLineNo, 6), rrpLine.Stod.Format("%H%M"));
	pomGXGridWnd->SetValueRange(CGXRange(ipLineNo, 7), (LONG)rrpLine.CAnz);
	pomGXGridWnd->SetValueRange(CGXRange(ipLineNo, 8), rrpLine.Cicf + "-" + rrpLine.Cict);

	if (rrpLine.Ckes != TIMENULL && rrpLine.Ckbs != TIMENULL)
	{
		CTimeSpan olSpan = rrpLine.Ckes - rrpLine.Ckbs;
		long llDuration = olSpan.GetHours() * 60 + olSpan.GetMinutes();

		if (rrpLine.Act == "BLK")
			pomGXGridWnd->SetValueRange(CGXRange(ipLineNo, 9), (rrpLine.Ckes - rrpLine.Ckbs).GetTotalMinutes());
		else
			pomGXGridWnd->SetValueRange(CGXRange(ipLineNo, 9), llDuration);
	}
	else
	{
		pomGXGridWnd->SetValueRange(CGXRange(ipLineNo, 9), (short)0);
	}

	CString olBegin("    ");
	CString olEnd=olBegin;
	if (rrpLine.Ckbs != TIMENULL)
	{
		olBegin = rrpLine.Ckbs.Format("%H%M");
	}
	if (rrpLine.Ckes != TIMENULL)
	{
		olEnd = rrpLine.Ckes.Format("%H%M");
	}
	pomGXGridWnd->SetValueRange(CGXRange(ipLineNo, 10), olBegin + "-" + olEnd);
 
	return true;
}


// Extract the vias from the given flight 
bool ReportSeasonTableViewer::GetVias(CString &opDestStr, const CCAFLIGHTDATA &rrpFlight) const
{
	int ilAnzVias = 0 ;
	CCSPtrArray<VIADATA> olViaListe ;

	opDestStr="";
	if( (ilAnzVias = rrpFlight.GetViaArray( &olViaListe )) < 1) return false ;
	
	for( int ilVia=0; ilVia < ilAnzVias; ilVia++ )
	{
		opDestStr += olViaListe[ilVia].Apc3 ;
		opDestStr += " " ;
	}

	return true ;	
}


 

void ReportSeasonTableViewer::PrintTable(void) {

	CString olFooter1,olFooter2,olFooterName;

 	olFooterName.Format("%s   %s %s", GetString(IDS_STRINGFAG), GetString(IDS_STRING1481),
		(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")));
 
	CString olTableName = GetString(IDS_STRING1462);
 
  	if (omPrint.InitializePrinter(imOrientation) == TRUE)
	{
		bool blFirstFlightCca = true ;

		omPrint.imMaxLines = 57;	// Def. imMaxLines: 57 Portrait / 38 Landscape
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
		omPrint.imLineNo = omPrint.imMaxLines + 1;	
		DOCINFO	rlDocInfo;
		memset(&rlDocInfo, 0, sizeof(DOCINFO));
		rlDocInfo.cbSize = sizeof( DOCINFO );
		rlDocInfo.lpszDocName = olTableName;	
		omPrint.omCdc.StartDoc( &rlDocInfo );
		omPrint.imPageNo = 0;
 		olFooter1.Format("%s",olFooterName);
		for (unsigned int ilLc = 1; ilLc <= pomGXGridWnd->GetRowCount(); ilLc++)
		{
			// Page break
 			if(omPrint.imLineNo >= omPrint.imMaxLines)
			{
				if(omPrint.imPageNo > 0)
				{
					// print footer
					olFooter2.Format(GetString(IDS_STRING1198),omPrint.imPageNo);
					omPrint.PrintUIFFooter(olFooter1,"",olFooter2);
					omPrint.omCdc.EndPage();
				}
				// print header
				PrintTableHeader(omPrint);
			}				
			// print line
			PrintTableLine(omPrint, ilLc);
		}
		// print footer
		olFooter2.Format(GetString(IDS_STRING1198),omPrint.imPageNo);
		omPrint.PrintUIFFooter(olFooter1,"",olFooter2);
		omPrint.omCdc.EndPage();
		omPrint.omCdc.EndDoc();
	}  // if (omPrint.InitializePrin...
 	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
}




bool ReportSeasonTableViewer::PrintTableHeader(CCSPrint &ropPrint)
{
	ropPrint.omCdc.StartPage();
	ropPrint.imPageNo++;
	ropPrint.imLineNo = 0;
	//double dgCCSPrintFactor = 2.7 ;
	CString olHeadline;

	// Headline
	if ((omMaxDate - omMinDate).GetDays() < 2)
	{
		olHeadline.Format("%s %s", GetString(IDS_STRING1462), omMinDate.Format("%d.%m.%Y"));
	}
	else
	{
		olHeadline.Format("%s %s - %s", GetString(IDS_STRING1462), omMinDate.Format("%d.%m.%Y"), omMaxDate.Format("%d.%m.%Y"));
	}

	// Table Header
	ropPrint.imLeftOffset = 50 ;
	ropPrint.PrintUIFHeader(CString(), olHeadline, ropPrint.imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_NOFRAME;
	rlElement.pFont       = &romPrintHeaderFont;

	for (unsigned int ilCc = 1; ilCc <= pomGXGridWnd->GetColCount(); ilCc++)
	{
		rlElement.Length = (int)(imPrintColWidths[ilCc-1]*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength) 
			rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Text   = pomGXGridWnd->GetValueRowCol(0, ilCc); 
		
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
 	}

 	ropPrint.PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();

	return true;
}


//-----------------------------------------------------------------------------------------------
// eine Tabellenzeile ausgeben



bool ReportSeasonTableViewer::PrintTableLine(CCSPrint &ropPrint, int ipLineNo) {
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	
	//double dgCCSPrintFactor = 2.7 ;

	PRINTELEDATA rlElement;
	rlElement.pFont = &romPrintLinesFont;
	rlElement.FrameTop = PRINT_FRAMETHIN;
 	rlElement.FrameBottom = PRINT_FRAMETHIN;
	rlElement.FrameLeft  = PRINT_FRAMETHIN;
	rlElement.FrameRight = PRINT_FRAMETHIN;


	// alle Tabellenzellen ausgeben
	for (unsigned int ilCc = 1; ilCc <= pomGXGridWnd->GetColCount(); ilCc++)
	{
		rlElement.Alignment  = PRINT_LEFT;
		if (ilCc == 7 || ilCc == 9)
			rlElement.Alignment  = PRINT_RIGHT;
		rlElement.Length = (int)(imPrintColWidths[ilCc-1]*dgCCSPrintFactor);
		if(rlElement.Length < igCCSPrintMinLength) 
			rlElement.Length+=igCCSPrintMoreLength; 
		rlElement.Text   = pomGXGridWnd->GetValueRowCol(ipLineNo, ilCc); 
		
		//TRACE(" i [%d] : Length [%d]\n", ilCc, rlElement.Length ) ;
		
		rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		
	} // end for( i = ... alle ITEMS 

	ropPrint.PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll(); 
	
	return true;
}


 

bool ReportSeasonTableViewer::PrintExcelFile(const char *popFilePath) const
{

	if(popFilePath && strlen(popFilePath) != 0)
	{
		// Get Seperator for Excel-File
		char pclConfigPath[256];
		char pclTrenner[2];
		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
		    strcpy(pclConfigPath, getenv("CEDA"));

		GetPrivateProfileString(ogAppName, "ExcelSeperator", ";",
		    pclTrenner, sizeof pclTrenner, pclConfigPath);

		ofstream of;
		of.open(popFilePath, ios::out);

		 // Lines
		for (unsigned int ilLc = 0; ilLc <= pomGXGridWnd->GetRowCount(); ilLc++)
		{
			for (unsigned int ilCc = 1; ilCc <= pomGXGridWnd->GetColCount(); ilCc++)
			{
				of  << pomGXGridWnd->GetValueRowCol(ilLc, ilCc); 
				if (ilCc < pomGXGridWnd->GetColCount())
				{
					of << pclTrenner;
				}
			}
			of << endl;
		}

		of.close();
 
		return true;
	} // 	if(GetSaveFileName(polOfn) != 0)

	return false;
}




// Compares two lines of the table
int ReportSeasonTableViewer::CompareLines(const REPORTSEASONTABLE_LINEDATA *prpLine1, const REPORTSEASONTABLE_LINEDATA *prpLine2) const 
{
	// Sorted by Type(BLK, COM, _), FromToDays, Stod
	
	if (prpLine1->Act != prpLine2->Act)
	{
		// First blocked counters
		if (prpLine1->Act == "BLK") return -1;
		if (prpLine2->Act == "BLK") return 1;
		// then common counters
		if (prpLine1->Act == "COM") return -1;
		if (prpLine2->Act == "COM") return 1;
	}

	// Sort to 'From - To / Days'
	CTime olBeg1(prpLine1->Ckbs.GetYear(), prpLine1->Ckbs.GetMonth(), prpLine1->Ckbs.GetDay(), 0, 0, 0);
	CTime olBeg2(prpLine2->Ckbs.GetYear(), prpLine2->Ckbs.GetMonth(), prpLine2->Ckbs.GetDay(), 0, 0, 0);
 	if (olBeg1 != olBeg2)
	{
		//TRACE("Line1:%s: Ckbs: %s, Line2:%s: Ckbs: %s\n", prpLine1->Flno, prpLine1->Ckbs.Format("%d.%m.%y"), prpLine2->Flno, prpLine2->Ckbs.Format("%d.%m.%y"));
		return (olBeg1 > olBeg2) ? 1 : -1;
	}

	CTime olEnd1(prpLine1->Ckes.GetYear(), prpLine1->Ckes.GetMonth(), prpLine1->Ckes.GetDay(), 0, 0, 0);
	CTime olEnd2(prpLine2->Ckes.GetYear(), prpLine2->Ckes.GetMonth(), prpLine2->Ckes.GetDay(), 0, 0, 0);
 	if (olEnd1 != olEnd2)
	{
		//TRACE("Line1:%s: Ckbs: %s, Line2:%s: Ckbs: %s\n", prpLine1->Flno, prpLine1->Ckbs.Format("%d.%m.%y"), prpLine2->Flno, prpLine2->Ckbs.Format("%d.%m.%y"));
		return (olEnd1 > olEnd2) ? 1 : -1;
	}
  
	if (prpLine1->Freq != prpLine2->Freq)
	{
		return (prpLine1->Freq > prpLine2->Freq) ? 1 : -1;
	}
	// Sort to stod
	return (prpLine1->Stod == prpLine2->Stod)? 0:	
		   (prpLine1->Stod > prpLine2->Stod)? 1: -1;
	
}



 