
#include <StdAfx.h>

#include <Timer.H>

#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



long round(double dlNum) 
{
	long llRet=(long)floor(dlNum);

	if (dlNum - llRet >= 0.5)
		return llRet+1;
	else
		return llRet;
}


Timer::Timer(unsigned long lpLoopNum): 
	lmLoopNum(lpLoopNum), lmLoopCount(0)
{
	Start();
}


void Timer::Start(void)
{
	omStart = CTime::GetCurrentTime();
	lmLoopCount=0;
	omElapsedTime2 = CTimeSpan(0,0,0,0);
	imMin = imSec = 0;
	bmValid = false;
}


bool Timer::Step(void) 
{
	
	lmLoopCount++;
	if (lmLoopCount > lmLoopNum) return false;

	omElapsedTime = CTime::GetCurrentTime() - omStart;

	long llTotalSecs = omElapsedTime.GetTotalSeconds();
				
	long llRestSecs = (long) (llTotalSecs * ((lmLoopNum / (float) lmLoopCount)-1));
	//TRACE("Rest Secs = %f\n", flRestSecs);
		
	if (llTotalSecs > 3 && omElapsedTime != omElapsedTime2)
	{
		omElapsedTime2 = omElapsedTime;
		bmValid = true;
		imMin = llRestSecs / 60;
		imSec = llRestSecs - (imMin * 60);
	}				

	return true;
}


unsigned int Timer::GetMin(void) const
{
	return imMin;
}


unsigned int Timer::GetSec(void) const
{
	return imSec;
}


bool Timer::Valid(void) const
{
	return bmValid;
}