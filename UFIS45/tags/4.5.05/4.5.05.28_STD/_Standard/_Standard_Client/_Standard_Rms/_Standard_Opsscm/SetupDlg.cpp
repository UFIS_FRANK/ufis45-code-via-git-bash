// SetupDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <CCSEdit.h>
#include <SetupDlg.h>
#include <CedaCfgData.h>
#include <BasicData.h>
#include <CCSGlobl.h>
#include <CCSDDX.h>
#include <PrivList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld SetupDlg 


SetupDlg::SetupDlg(CWnd* pParent /*=NULL*/)
	: CDialog(SetupDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(SetupDlg)
	m_Monitors = -1;
	m_Batch1 = -1;
	m_Daily1 = -1;
	m_DailyRot1 = -1;
	m_FlightDia1 = -1;
	m_Season1 = -1;
	m_SeasonRot1 = -1;
	m_CcaDia1 = -1;
	m_PopsReportDailyAlc = _T("");
	//}}AFX_DATA_INIT
}


void SetupDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SetupDlg)
	DDX_Control(pDX, IDC_STATIC_POPS, m_CS_Pops_Report_Daily_Alc);
	DDX_Control(pDX, IDC_POPS_REPORT_DAILY_ALC, m_CE_PopsReportDailyAlc);
	DDX_Control(pDX, IDC_STATIC_FONT, m_CS_Font);
	DDX_Control(pDX, IDC_PKNO_CB, m_UserCB);
	DDX_Radio(pDX, IDC_RADIO1, m_Monitors);
	DDX_Radio(pDX, IDC_BATCH1, m_Batch1);
	DDX_Radio(pDX, IDC_DAILY1, m_Daily1);
	DDX_Radio(pDX, IDC_DAILYROT1, m_DailyRot1);
	DDX_Radio(pDX, IDC_FLIGHTDIA1, m_FlightDia1);
	DDX_Radio(pDX, IDC_SEASON1, m_Season1);
	DDX_Radio(pDX, IDC_SEASONROT1, m_SeasonRot1);
	DDX_Radio(pDX, IDC_CCADIA1, m_CcaDia1);
	DDX_Text(pDX, IDC_POPS_REPORT_DAILY_ALC, m_PopsReportDailyAlc);
	//}}AFX_DATA_MAP
}
SetupDlg::~SetupDlg()
{
	omMonitorButtons1.RemoveAll();
	omMonitorButtons2.RemoveAll();
	omMonitorButtons3.RemoveAll();
}


BEGIN_MESSAGE_MAP(SetupDlg, CDialog)
	//{{AFX_MSG_MAP(SetupDlg)
	ON_CBN_SELCHANGE(IDC_PKNO_CB, OnSelchangePknoCb)
	ON_BN_CLICKED(IDC_RADIO1, OnRadio1)
	ON_BN_CLICKED(IDC_RADIO2, OnRadio2)
	ON_BN_CLICKED(IDC_RADIO3, OnRadio3)
	ON_BN_CLICKED(IDC_FONT, OnFont)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten SetupDlg 




void SetupDlg::OnSelchangePknoCb() 
{
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	
}

void SetupDlg::OnRadio1() 
{
	CObject *polObj;
	POSITION pos;
    for(pos = omMonitorButtons1.GetHeadPosition(); pos != NULL; )
    {
	    polObj = omMonitorButtons1.GetNext(pos);
		if(polObj != NULL)
		{
			((CButton *)polObj)->EnableWindow(TRUE);
			((CButton *)polObj)->SetCheck(1);
		}
    }	
    for(pos = omMonitorButtons2.GetHeadPosition(); pos != NULL; )
    {
	    polObj = omMonitorButtons2.GetNext(pos);
		if(polObj != NULL)
		{
			((CButton *)polObj)->EnableWindow(FALSE);
			((CButton *)polObj)->SetCheck(0);
		}
    }	
    for(pos = omMonitorButtons3.GetHeadPosition(); pos != NULL; )
    {
	    polObj = omMonitorButtons3.GetNext(pos);
		if(polObj != NULL)
		{
			((CButton *)polObj)->EnableWindow(FALSE);
			((CButton *)polObj)->SetCheck(0);
		}
    }	 
}
 
void SetupDlg::OnRadio2() 
{

	CObject *polObj;
	POSITION pos;
    for(pos = omMonitorButtons1.GetHeadPosition(); pos != NULL; )
    {
	    polObj = omMonitorButtons1.GetNext(pos);
		if(polObj != NULL)
		{
			((CButton *)polObj)->EnableWindow(TRUE);
			((CButton *)polObj)->SetCheck(1);
		}
    }	
    for(pos = omMonitorButtons2.GetHeadPosition(); pos != NULL; )
    {
	    polObj = omMonitorButtons2.GetNext(pos);
		if(polObj != NULL)
		{
			((CButton *)polObj)->EnableWindow(TRUE);
			((CButton *)polObj)->SetCheck(0);
		}
    }	
    for(pos = omMonitorButtons3.GetHeadPosition(); pos != NULL; )
    {
	    polObj = omMonitorButtons3.GetNext(pos);
		if(polObj != NULL)
		{
			((CButton *)polObj)->EnableWindow(FALSE);
			((CButton *)polObj)->SetCheck(0);
		}
    }	
}

void SetupDlg::OnRadio3() 
{
	CObject *polObj;
	POSITION pos;
    for(pos = omMonitorButtons1.GetHeadPosition(); pos != NULL; )
    {
	    polObj = omMonitorButtons1.GetNext(pos);
		if(polObj != NULL)
		{
			((CButton *)polObj)->EnableWindow(TRUE);
		}
    }	
    for(pos = omMonitorButtons2.GetHeadPosition(); pos != NULL; )
    {
	    polObj = omMonitorButtons2.GetNext(pos);
		if(polObj != NULL)
		{
			((CButton *)polObj)->EnableWindow(TRUE);
		}
    }	
    for(pos = omMonitorButtons3.GetHeadPosition(); pos != NULL; )
    {
	    polObj = omMonitorButtons3.GetNext(pos);
		if(polObj != NULL)
		{
			((CButton *)polObj)->EnableWindow(TRUE);
		}
    }	
}


void SetupDlg::OnOK() 
{
	if(UpdateData() == TRUE)
	{
		char plcS1[20]="";
		char plcS2[20]="";
		char plcS3[20]="";
		char plcS4[20]="";
		char plcS5[20]="";
		char plcS6[20]="";
		char plcS7[20]="";
		char plcS8[20]="";

		sprintf(plcS1, "%d", m_Monitors+1);
		sprintf(plcS2, "%d", m_Season1+1);
		sprintf(plcS3, "%d", m_Daily1+1);
		sprintf(plcS4, "%d", m_SeasonRot1+1);
		sprintf(plcS5, "%d", m_Batch1+1);
		sprintf(plcS6, "%d", m_DailyRot1+1);
		sprintf(plcS7, "%d", m_FlightDia1+1);
		sprintf(plcS8, "%d", m_CcaDia1+1);

		sprintf(ogCfgData.rmMonitorSetup.Text, "%s=%s#%s=%s#%s=%s#%s=%s#%s=%s#%s=%s#%s=%s#%s=%s",
								MON_COUNT_STRING, plcS1,			
								MON_SEASONSCHEDULE_STRING, plcS2,	
								MON_DAILYSCHEDULE_STRING, plcS3,	
								MON_SEASONROTDLG_STRING, plcS4,	    
								MON_SEASONBATCH_STRING, plcS5,	    
								MON_DAILYROTDLG_STRING, plcS6,	    
								MON_FLIGHTDIA_STRING, plcS7,	    
								MON_CCADIA_STRING, plcS8);	    



		if(ogCfgData.rmMonitorSetup.IsChanged != DATA_NEW)
		{
			ogCfgData.rmMonitorSetup.IsChanged = DATA_CHANGED;
		}
		ogCfgData.SaveCfg(&ogCfgData.rmMonitorSetup);


		// FONT 

		if(ogCfgData.rmFontSetup.IsChanged != DATA_NEW)
		{
			ogCfgData.rmFontSetup.IsChanged = DATA_CHANGED;
		}
		ogCfgData.SaveCfg(&ogCfgData.rmFontSetup);


		if(!CString(ogCfgData.rmFontSetup.Text).IsEmpty())
		{
		
			CStringArray olValues;
			
			if(ExtractItemList(CString(ogCfgData.rmFontSetup.Text), &olValues, '#') == 14)
			{
				ogSetupFont.DeleteObject();
				ogSetupFont.CreateFont( atoi(olValues[0]), atoi(olValues[1]), atoi(olValues[2]), atoi(olValues[3]), atoi(olValues[4]), atoi(olValues[5]), atoi(olValues[6]), atoi(olValues[7]), atoi(olValues[8]), atoi(olValues[9]), atoi(olValues[10]), atoi(olValues[11]), atoi(olValues[12]), olValues[13]);
				ogDdx.DataChanged((void *)this, TABLE_FONT_CHANGED, NULL);

			}
		}

		// POPS REPORT DAILY ALC

		strcpy(ogCfgData.rmPopsReportDailyAlcSetup.Text , m_PopsReportDailyAlc);

		if(ogCfgData.rmPopsReportDailyAlcSetup.IsChanged != DATA_NEW)
		{
			ogCfgData.rmPopsReportDailyAlcSetup.IsChanged = DATA_CHANGED;
		}
		ogCfgData.SaveCfg(&ogCfgData.rmPopsReportDailyAlcSetup);


		CDialog::OnOK();
	}
}


BOOL SetupDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CWnd *polWnd;
//Mon 1
	polWnd = GetDlgItem(IDC_SEASON1);
	if(polWnd != NULL)
	{
		omMonitorButtons1.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_SEASONROT1);
	if(polWnd != NULL)
	{
		omMonitorButtons1.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_BATCH1);
	if(polWnd != NULL)
	{
		omMonitorButtons1.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_DAILY1);
	if(polWnd != NULL)
	{
		omMonitorButtons1.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_DAILYROT1);
	if(polWnd != NULL)
	{
		omMonitorButtons1.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_FLIGHTDIA1);
	if(polWnd != NULL)
	{
		omMonitorButtons1.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_CCADIA1);
	if(polWnd != NULL)
	{
		omMonitorButtons1.AddTail(polWnd);
	}
// Mon2 
	polWnd = GetDlgItem(IDC_SEASON2);
	if(polWnd != NULL)
	{
		omMonitorButtons2.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_SEASONROT2);
	if(polWnd != NULL)
	{
		omMonitorButtons2.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_BATCH2);
	if(polWnd != NULL)
	{
		omMonitorButtons2.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_DAILY2);
	if(polWnd != NULL)
	{
		omMonitorButtons2.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_DAILYROT2);
	if(polWnd != NULL)
	{
		omMonitorButtons2.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_FLIGHTDIA2);
	if(polWnd != NULL)
	{
		omMonitorButtons2.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_CCADIA2);
	if(polWnd != NULL)
	{
		omMonitorButtons2.AddTail(polWnd);
	}
//Mon 3
	polWnd = GetDlgItem(IDC_SEASON3);
	if(polWnd != NULL)
	{
		omMonitorButtons3.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_SEASONROT3);
	if(polWnd != NULL)
	{
		omMonitorButtons3.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_BATCH3);
	if(polWnd != NULL)
	{
		omMonitorButtons3.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_DAILY3);
	if(polWnd != NULL)
	{
		omMonitorButtons3.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_DAILYROT3);
	if(polWnd != NULL)
	{
		omMonitorButtons3.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_FLIGHTDIA3);
	if(polWnd != NULL)
	{
		omMonitorButtons3.AddTail(polWnd);
	}
	polWnd = GetDlgItem(IDC_CCADIA3);
	if(polWnd != NULL)
	{
		omMonitorButtons3.AddTail(polWnd);
	}


	
	m_Monitors = ogCfgData.GetMonitorForWindow(MON_COUNT_STRING) - 1;
	m_Season1 = ogCfgData.GetMonitorForWindow(MON_SEASONSCHEDULE_STRING) -1;
	m_Daily1 = ogCfgData.GetMonitorForWindow(MON_DAILYSCHEDULE_STRING)-1;
	m_SeasonRot1 = ogCfgData.GetMonitorForWindow(MON_SEASONROTDLG_STRING)-1;
	m_Batch1 = ogCfgData.GetMonitorForWindow(MON_SEASONBATCH_STRING)-1;
	m_DailyRot1 = ogCfgData.GetMonitorForWindow(MON_DAILYROTDLG_STRING)-1;
	m_FlightDia1 = ogCfgData.GetMonitorForWindow(MON_FLIGHTDIA_STRING)-1;
	m_CcaDia1 = ogCfgData.GetMonitorForWindow(MON_CCADIA_STRING)-1;


	if(m_Monitors == 0)
	{
		OnRadio1();
	}
	else if(m_Monitors == 1)
	{
		OnRadio2();
	}
	m_UserCB.AddString(ogBasicData.omUserID);
	m_UserCB.SetCurSel(0);
	UpdateData(FALSE);


	m_CS_Font.SetFont(&ogSetupFont);

	m_CS_Font.SetWindowText("LH 4711");



	CRect olRect;

	GetWindowRect(olRect);

	int left = (1024 - (olRect.right - olRect.left)) /2;
	int top =  (768  - (olRect.bottom - olRect.top)) /2;

	SetWindowPos(&wndTop,left, top, 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE);


	m_PopsReportDailyAlc = CString(ogCfgData.rmPopsReportDailyAlcSetup.Text);

	m_CE_PopsReportDailyAlc.SetWindowText(m_PopsReportDailyAlc);


	if(!((ogCustomer == "BVD") && (ogAppName == "POPS")))
	{
		m_CE_PopsReportDailyAlc.ShowWindow(SW_HIDE);
		m_CS_Pops_Report_Daily_Alc.ShowWindow(SW_HIDE);
	}


	m_CE_PopsReportDailyAlc.SetSecState(ogPrivList.GetStat("SETUP_POPS_REPORT_DAILY_ALC"));		

	UpdateWindow();

	m_CE_PopsReportDailyAlc.UpdateWindow();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}


void SetupDlg::OnFont() 
{
	
	CFontDialog olDlg;
	LPLOGFONT lpLogFont = new LOGFONT;

	if(olDlg.DoModal() == IDOK)
	{

	  olDlg.GetCurrentFont( lpLogFont );

	//test


		CFont olFont;

		int nHeight					= lpLogFont->lfHeight; 
		int nWidth					= lpLogFont->lfWidth; 
		int nEscapement				= lpLogFont->lfEscapement; 
		int nOrientation			= lpLogFont->lfOrientation; 
		int nWeight					= lpLogFont->lfWeight; 
		int bItalic					= lpLogFont->lfItalic; 
		int bUnderline				= lpLogFont->lfUnderline; 
		int cStrikeOut				= lpLogFont->lfStrikeOut; 
		int nCharSet				= lpLogFont->lfCharSet; 
		int nOutPrecision			= lpLogFont->lfOutPrecision; 
		int nClipPrecision			= lpLogFont->lfClipPrecision; 
		int nQuality				= lpLogFont->lfQuality; 
		int nPitchAndFamily			= lpLogFont->lfPitchAndFamily; 
		CString lpszFacename		= CString(lpLogFont->lfFaceName); 





		sprintf(ogCfgData.rmFontSetup.Text, "%d#%d#%d#%d#%d#%d#%d#%d#%d#%d#%d#%d#%d#%s",
																								nHeight,			
																								nWidth,			
																								nEscapement,		
																								nOrientation,	
																								nWeight,			
																								bItalic,			
																								bUnderline,		
																								cStrikeOut,		
																								nCharSet,		
																								nOutPrecision,	
																								nClipPrecision,	
																								nQuality,		
																								nPitchAndFamily,	
																								lpszFacename);

		olFont. CreateFont( nHeight, nWidth, nEscapement, nOrientation, nWeight, bItalic, bUnderline, cStrikeOut, nCharSet, nOutPrecision, nClipPrecision, nQuality, nPitchAndFamily, lpszFacename );


		m_CS_Font.SetFont(&olFont);

		m_CS_Font.SetWindowText("LH 4711");



	}



}
