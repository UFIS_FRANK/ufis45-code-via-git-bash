; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CicFConfTableDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "opsscm.h"
LastPage=0

ClassCount=36
Class1=AllocateCcaParameter
Class2=BasePropertySheet
Class3=CCAChangeTimes
Class4=CcaChart
Class5=CCcaCommonDlg
Class6=CcaDiagram
Class7=CCAExpandDlg
Class8=CcaGantt
Class9=CChildView
Class10=CicConfTableDlg
Class11=CicCreateDemandsDlg
Class12=CicDemandTableDlg
Class13=CicFConfTableDlg
Class14=CicNoDemandTableDlg
Class15=CDailyCcaPrintDlg
Class16=DelelteCounterDlg
Class17=CInitialLoadDlg
Class18=List
Class19=CLoginDialog
Class20=CMainFrame
Class21=COPSSCMApp
Class22=CAboutDlg
Class23=PSGeometrie
Class24=CSearchFlightPage
Class25=CSortFlightPage
Class26=CSpecFilterPage
Class27=CPsUniFilter
Class28=CPSUniSortPage
Class29=PSZeitraumPage
Class30=RegisterDlg
Class31=CReportSeasonTableDlg
Class32=CReportTableDlg
Class33=CScrollToolBar
Class34=SetupDlg
Class35=CSplashWnd
Class36=UniEingabe

ResourceCount=98
Resource1=IDD_CICFCONFTABLE (English (U.K.) - _ENGLISH)
Resource2=IDD_CICDEMANDTABLE (English (U.K.) - _ENGLISH)
Resource3=IDD_CCAALLOCATE_PARAMETER
Resource4=IDD_PSGEOMETRIE
Resource5=IDD_LOGIN
Resource6=IDR_MAINFRAME2
Resource7=IDR_LEFT_BAR
Resource8=IDD_REPORTTABLE (English (U.K.) - _ENGLISH)
Resource9=IDD_CICFCONFTABLE
Resource10=IDR_MAINFRAME1 (English (U.K.) - _ENGLISH)
Resource11=IDD_AWBASICDATA (English (U.K.) - _ENGLISH)
Resource12=IDD_SEASON (English (U.K.) - _ENGLISH)
Resource13=IDD_REPORTTABLE
Resource14=IDD_CCAALLOCATE_PARAMETER (English (U.K.) - _ENGLISH)
Resource15=IDD_CCA_DELETE_FROMTO
Resource16=IDR_MAINFRAME2 (English (U.K.) - _ENGLISH)
Resource17=IDD_DAILYCCAPRINT (English (U.K.) - _ENGLISH)
Resource18=IDR_FOPEN (German (Germany))
Resource19=IDD_PSSEARCH_FLIGHT_PAGE (English (U.K.) - _ENGLISH)
Resource20=IDD_CREATE_DEMANDS (English (U.K.) - _ENGLISH)
Resource21=IDD_CCAEXPAND
Resource22=IDD_CICNODEMANDTABLE (English (U.K.) - _ENGLISH)
Resource23=IDD_UNI_EINGABE (English (U.K.) - _ENGLISH)
Resource24=IDD_TIMESCALE
Resource25=IDD_CCA_CHANGE_TIMES (English (U.K.) - _ENGLISH)
Resource26=IDD_INITIALLOAD (English (U.K.) - _ENGLISH)
Resource27=IDR_RIGHT_BAR (German (Germany))
Resource28=IDD_CICDEMANDTABLE
Resource29=IDD_INITIALLOAD
Resource30=IDD_UNI_EINGABE
Resource31=IDR_LEFT_BAR (English (U.K.) - _ENGLISH)
Resource32=IDR_MAINFRAME
Resource33=IDD_CICNODEMANDTABLE
Resource34=IDD_CCA_COMMON_DLG (English (U.K.) - _ENGLISH)
Resource35=IDD_REGISTERDLG (English (U.K.) - _ENGLISH)
Resource36=IDR_RIGHT_BAR (English (U.K.) - _ENGLISH)
Resource37=IDD_CCAEXPAND (English (U.K.) - _ENGLISH)
Resource38=IDD_CCADIAGRAM
Resource39=IDD_ABOUTBOX
Resource40=IDD_REGISTERDLG
Resource41=IDD_LOGIN (English (U.K.) - _ENGLISH)
Resource42=IDR_BLANK_BAR
Resource43=IDD_AWBASICDATA
Resource44=IDR_FOPEN
Resource45=IDD_DAILYCCAPRINT
Resource46=IDD_LISTE
Resource47=IDD_PSGEOMETRIE (English (U.K.) - _ENGLISH)
Resource48=IDR_RIGHT_BAR
Resource49=IDD_CREATE_DEMANDS
Resource50=IDD_PSSEARCH_FLIGHT_PAGE
Resource51=IDD_VIATABLE (English (U.K.) - _ENGLISH)
Resource52=IDD_ABOUTBOX (English (U.K.) - _ENGLISH)
Resource53=IDD_CCA_DELETE_FROMTO (English (U.K.) - _ENGLISH)
Resource54=IDD_SEASONASK (English (U.K.) - _ENGLISH)
Resource55=IDR_MAINFRAME1
Resource56=IDD_CICCONFTABLE
Resource57=IDD_CCA_CHANGE_TIMES
Resource58=IDD_CCADIAGRAM (English (U.K.) - _ENGLISH)
Resource59=IDD_TIMESCALE (English (U.K.) - _ENGLISH)
Resource60=IDD_SEASON (German (Germany))
Resource61=IDD_CICCONFTABLE (English (U.K.) - _ENGLISH)
Resource62=IDD_VIATABLE (German (Germany))
Resource63=IDD_LISTE (English (U.K.) - _ENGLISH)
Resource64=IDR_BLANK_BAR (English (U.K.) - _ENGLISH)
Resource65=IDR_MAINFRAME2 (German (Germany))
Resource66=IDD_ROTATION_HAI (English (U.K.) - _ENGLISH)
Resource67=IDD_CCI_DEMANDDETAIL (English (U.K.))
Resource68=IDD_ABOUTBOX (German (Germany))
Resource69=IDD_CCA_CHANGE_TIMES (German (Germany))
Resource70=IDD_CCA_DELETE_FROMTO (German (Germany))
Resource71=IDD_CCAALLOCATE_PARAMETER (German (Germany))
Resource72=IDD_CCADIAGRAM (German (Germany))
Resource73=IDD_CCAEXPAND (German (Germany))
Resource74=IDD_CICCONFTABLE (German (Germany))
Resource75=IDD_CICDEMANDTABLE (German (Germany))
Resource76=IDD_CICFCONFTABLE (German (Germany))
Resource77=IDD_CICNODEMANDTABLE (German (Germany))
Resource78=IDD_INITIALLOAD (German (Germany))
Resource79=IDD_LISTE (German (Germany))
Resource80=IDD_LOGIN (German (Germany))
Resource81=IDD_PSGEOMETRIE (German (Germany))
Resource82=IDD_PSSEARCH_FLIGHT_PAGE (German (Germany))
Resource83=IDD_REGISTERDLG (German (Germany))
Resource84=IDD_TIMESCALE (German (Germany))
Resource85=IDD_UNI_EINGABE (German (Germany))
Resource86=IDD_CREATE_DEMANDS (German (Germany))
Resource87=IDD_DAILYCCAPRINT (German (Germany))
Resource88=IDD_REPORTTABLE (German (Germany))
Resource89=IDD_AWBASICDATA (German (Germany))
Resource90=IDD_CCA_COMMON_DLG (German (Germany))
Resource91=IDD_CCI_DEMANDDETAIL (German (Germany))
Resource92=IDD_ROTATION_HAI (German (Germany))
Resource93=IDR_MAINFRAME (German (Germany))
Resource94=IDR_MAINFRAME (English (U.K.) - _ENGLISH)
Resource95=IDR_BLANK_BAR (German (Germany))
Resource96=IDR_LEFT_BAR (German (Germany))
Resource97=IDR_MAINFRAME1 (German (Germany))
Resource98=IDR_FOPEN (English (U.K.) - _ENGLISH)

[CLS:AllocateCcaParameter]
Type=0
BaseClass=CDialog
HeaderFile=AllocateCcaParameter.h
ImplementationFile=AllocateCcaParameter.cpp
LastObject=AllocateCcaParameter

[CLS:BasePropertySheet]
Type=0
BaseClass=CPropertySheet
HeaderFile=BasePropertySheet.H
ImplementationFile=BasePropertySheet.CPP

[CLS:CCAChangeTimes]
Type=0
BaseClass=CDialog
HeaderFile=CCAChangeTimes.h
ImplementationFile=CCAChangeTimes.cpp

[CLS:CcaChart]
Type=0
BaseClass=CFrameWnd
HeaderFile=CcaChart.H
ImplementationFile=CcaChart.CPP

[CLS:CCcaCommonDlg]
Type=0
BaseClass=CDialog
HeaderFile=CcaCommonDlg.h
ImplementationFile=CcaCommonDlg.cpp
Filter=D
VirtualFilter=dWC
LastObject=IDC_ACT_END_TIME

[CLS:CcaDiagram]
Type=0
BaseClass=CFrameWnd
HeaderFile=CcaDiagram.h
ImplementationFile=CcaDiagram.cpp

[CLS:CCAExpandDlg]
Type=0
BaseClass=CDialog
HeaderFile=CCAExpandDlg.h
ImplementationFile=CCAExpandDlg.cpp

[CLS:CcaGantt]
Type=0
BaseClass=CWnd
HeaderFile=CcaGantt.H
ImplementationFile=CcaGantt.CPP

[CLS:CChildView]
Type=0
BaseClass=CWnd
HeaderFile=ChildView.h
ImplementationFile=ChildView.cpp

[CLS:CicConfTableDlg]
Type=0
BaseClass=CDialog
HeaderFile=CicConfTableDlg.h
ImplementationFile=CicConfTableDlg.cpp

[CLS:CicCreateDemandsDlg]
Type=0
BaseClass=CDialog
HeaderFile=CicCreateDemandsDlg.h
ImplementationFile=CicCreateDemandsDlg.cpp

[CLS:CicDemandTableDlg]
Type=0
BaseClass=CDialog
HeaderFile=CicDemandTableDlg.h
ImplementationFile=CicDemandTableDlg.cpp

[CLS:CicFConfTableDlg]
Type=0
BaseClass=CDialog
HeaderFile=CicFConfTableDlg.h
ImplementationFile=CicFConfTableDlg.cpp
LastObject=CicFConfTableDlg

[CLS:CicNoDemandTableDlg]
Type=0
BaseClass=CDialog
HeaderFile=CicNoDemandTableDlg.h
ImplementationFile=CicNoDemandTableDlg.cpp

[CLS:CDailyCcaPrintDlg]
Type=0
BaseClass=CDialog
HeaderFile=DailyCcaPrintDlg.h
ImplementationFile=DailyCcaPrintDlg.cpp

[CLS:DelelteCounterDlg]
Type=0
BaseClass=CDialog
HeaderFile=DelelteCounterDlg.h
ImplementationFile=DelelteCounterDlg.cpp
LastObject=IDC_DATEFROM

[CLS:CInitialLoadDlg]
Type=0
BaseClass=CDialog
HeaderFile=InitialLoadDlg.h
ImplementationFile=InitialLoadDlg.cpp

[CLS:List]
Type=0
BaseClass=CDialog
HeaderFile=List.h
ImplementationFile=List.cpp

[CLS:CLoginDialog]
Type=0
BaseClass=CDialog
HeaderFile=LoginDlg.h
ImplementationFile=LoginDlg.cpp

[CLS:CMainFrame]
Type=0
BaseClass=CFrameWnd
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp

[CLS:COPSSCMApp]
Type=0
BaseClass=CWinApp
HeaderFile=OPSSCM.h
ImplementationFile=OPSSCM.cpp

[CLS:CAboutDlg]
Type=0
BaseClass=CDialog
HeaderFile=OPSSCM.cpp
ImplementationFile=OPSSCM.cpp

[CLS:PSGeometrie]
Type=0
BaseClass=CPropertyPage
HeaderFile=PSGeometrie.h
ImplementationFile=PSGeometrie.cpp

[CLS:CSearchFlightPage]
Type=0
BaseClass=CPropertyPage
HeaderFile=PSSearchFlightPage.h
ImplementationFile=PSSearchFlightPage.cpp

[CLS:CSortFlightPage]
Type=0
BaseClass=CPropertyPage
HeaderFile=PSSortFlightPage.h
ImplementationFile=PSSortFlightPage.cpp

[CLS:CSpecFilterPage]
Type=0
BaseClass=CPropertyPage
HeaderFile=PSSpecFilterPage.h
ImplementationFile=PSSpecFilterPage.cpp

[CLS:CPsUniFilter]
Type=0
BaseClass=CPropertyPage
HeaderFile=PSUniFilterPage.h
ImplementationFile=PSUniFilterPage.cpp

[CLS:CPSUniSortPage]
Type=0
BaseClass=CPropertyPage
HeaderFile=PSUniSortPage.h
ImplementationFile=PSUniSortPage.cpp

[CLS:PSZeitraumPage]
Type=0
BaseClass=CDialog
HeaderFile=PSZeitraumPage.h
ImplementationFile=PSZeitraumPage.cpp

[CLS:RegisterDlg]
Type=0
BaseClass=CDialog
HeaderFile=RegisterDlg.h
ImplementationFile=RegisterDlg.cpp

[CLS:CReportSeasonTableDlg]
Type=0
BaseClass=CDialog
HeaderFile=ReportSeasonTableDlg.h
ImplementationFile=ReportSeasonTableDlg.cpp

[CLS:CReportTableDlg]
Type=0
BaseClass=CDialog
HeaderFile=reporttabledlg.h
ImplementationFile=ReportTableDlg.cpp

[CLS:CScrollToolBar]
Type=0
BaseClass=CToolBar
HeaderFile=ScrollToolBar.h
ImplementationFile=ScrollToolBar.cpp

[CLS:SetupDlg]
Type=0
BaseClass=CDialog
HeaderFile=SetupDlg.h
ImplementationFile=SetupDlg.cpp

[CLS:CSplashWnd]
Type=0
BaseClass=CWnd
HeaderFile=Splash.h
ImplementationFile=Splash.cpp

[CLS:UniEingabe]
Type=0
BaseClass=CDialog
HeaderFile=UniEingabe.h
ImplementationFile=UniEingabe.cpp

[DLG:IDD_CCAALLOCATE_PARAMETER]
Type=1
Class=AllocateCcaParameter
ControlCount=5
Control1=IDC_ALL,button,1342242816
Control2=IDC_CANCEL,button,1342242817
Control3=IDC_PROGRESS1,msctls_progress32,1350565888
Control4=IDC_STATIC,static,1342308352
Control5=IDC_TIME,static,1342312450

[DLG:IDD_CCA_CHANGE_TIMES]
Type=1
Class=CCAChangeTimes
ControlCount=6
Control1=IDC_CKBS,edit,1350631552
Control2=IDC_CKES,edit,1350631552
Control3=IDOK,button,1342242817
Control4=IDCANCEL,button,1342242816
Control5=65535,static,1342308352
Control6=65535,static,1342308352

[DLG:IDD_CCA_COMMON_DLG]
Type=1
Class=CCcaCommonDlg

[DLG:IDD_CCAEXPAND]
Type=1
Class=CCAExpandDlg
ControlCount=9
Control1=IDC_ALL,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_PROG,msctls_progress32,1350565888
Control4=IDC_ONLY,button,1342242817
Control5=IDC_DATEFROM,edit,1350631552
Control6=IDC_DATETO,edit,1350631552
Control7=IDC_STATIC,static,1342308352
Control8=IDC_STATIC,static,1342308352
Control9=IDC_ALLVT,button,1342242819

[DLG:IDD_CICCONFTABLE]
Type=1
Class=CicConfTableDlg
ControlCount=0

[DLG:IDD_CREATE_DEMANDS]
Type=1
Class=CicCreateDemandsDlg
ControlCount=31
Control1=IDC_ZEITRAUMSTATIC,button,1342177287
Control2=IDC_STIMEFROM,static,1342177280
Control3=IDC_STIMETO,static,1342177280
Control4=IDC_SFROMDATE,edit,1350631552
Control5=IDC_SFROMTIME,edit,1350631552
Control6=IDC_STODATE,edit,1350631552
Control7=IDC_STOTIME,edit,1350631552
Control8=IDC_LSTUSTATIC,button,1342177287
Control9=IDC_LSTUFROM,static,1342177280
Control10=IDC_LSTUDATE,edit,1350631552
Control11=IDC_LSTUTIME,edit,1350631552
Control12=IDC_FLUGSTATIC,button,1342177287
Control13=IDC_FLIGHTCODE,static,1342308352
Control14=IDC_FLIGHTAIRLINE,edit,1350631552
Control15=IDC_FLIGHTNR,edit,1350631552
Control16=IDC_FLIGHTSUFFIX,edit,1350631552
Control17=IDC_REGISTRATION,static,1342308352
Control18=IDC_REGN,edit,1350631560
Control19=IDC_ACTYPE,static,1342308352
Control20=IDC_ACT3,edit,1350631560
Control21=IDC_AIRPORT,static,1342308352
Control22=IDC_AIRP,edit,1350631560
Control23=IDC_AIRPORT2,static,1342308352
Control24=IDC_AIRP2,edit,1350631560
Control25=IDC_BEMERKUNG,static,1342308352
Control26=IDC_REMA,edit,1350631560
Control27=IDC_FTYPSTATIC,button,1342177287
Control28=IDC_BETRIEB,button,1342242819
Control29=IDC_PLANUNG,button,1342242819
Control30=ID_CREATE_DEMANDS_OK,button,1342242817
Control31=IDCANCEL,button,1342242816

[DLG:IDD_CICDEMANDTABLE]
Type=1
Class=CicDemandTableDlg
ControlCount=0

[DLG:IDD_CICFCONFTABLE]
Type=1
Class=CicFConfTableDlg
ControlCount=4
Control1=IDC_B_CONFIRM,button,1342242816
Control2=IDC_B_CONFIRMALL,button,1342242816
Control3=IDC_R_ALLCONF,button,1342308361
Control4=IDC_R_FLIGHTCONF,button,1342177289

[DLG:IDD_CICNODEMANDTABLE]
Type=1
Class=CicNoDemandTableDlg
ControlCount=0

[DLG:IDD_DAILYCCAPRINT]
Type=1
Class=CDailyCcaPrintDlg
ControlCount=5
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_VOMTAG,edit,1350631552
Control4=IDC_VONZEIT,static,1476526592
Control5=IDC_STATIC,static,1342308352

[DLG:IDD_CCA_DELETE_FROMTO]
Type=1
Class=DelelteCounterDlg
ControlCount=8
Control1=IDC_DATEFROM,edit,1350631552
Control2=IDC_DATETO,edit,1350631552
Control3=IDC_VK,edit,1350631552
Control4=IDOK,button,1342242817
Control5=IDCANCEL,button,1342242816
Control6=IDC_STATIC,static,1342308352
Control7=IDC_STATIC,static,1342308352
Control8=IDC_STATIC,static,1342308352

[DLG:IDD_INITIALLOAD]
Type=1
Class=CInitialLoadDlg
ControlCount=2
Control1=IDC_MSGLIST,listbox,1352745216
Control2=IDC_PROGRESS1,msctls_progress32,1350565888

[DLG:IDD_LISTE]
Type=1
Class=List
ControlCount=2
Control1=IDOK,button,1342242817
Control2=IDC_LIST,listbox,1352728840

[DLG:IDD_LOGIN]
Type=1
Class=CLoginDialog
ControlCount=6
Control1=IDC_PASSWORD,edit,1350631584
Control2=IDOK,button,1342242817
Control3=IDCANCEL,button,1342242816
Control4=IDC_USIDCAPTION,static,1342308352
Control5=IDC_PASSCAPTION,static,1342308352
Control6=IDC_USERNAME,edit,1350631552

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_PSGEOMETRIE]
Type=1
Class=PSGeometrie
ControlCount=38
Control1=IDC_PFC,button,1207959561
Control2=IDC_GEG,button,1207959561
Control3=IDC_LIST1,listbox,1218445577
Control4=IDC_NEW,button,1207959552
Control5=IDC_DELETE,button,1207959552
Control6=IDC_RB_MIN,button,1342373897
Control7=IDC_RB_MAX,button,1342242825
Control8=IDC_FLT_TIMELINES,button,1342242819
Control9=IDC_STARTDATE,edit,1350631552
Control10=IDC_STARTTIME,edit,1350631552
Control11=IDC_24H,button,1342373897
Control12=IDC_8H,button,1342242825
Control13=IDC_12H,button,1342242825
Control14=IDC_6H,button,1342242825
Control15=IDC_10H,button,1342242825
Control16=IDC_4H,button,1342242825
Control17=IDC_HEIGHT,msctls_trackbar32,1342242821
Control18=IDC_100PERCENT,button,1342373897
Control19=IDC_75PERCENT,button,1342242825
Control20=IDC_50PERCENT,button,1342242825
Control21=IDC_GRUPPE,static,1207959552
Control22=IDC_STATIC,button,1342177287
Control23=IDC_STATIC,static,1342308352
Control24=IDC_STATIC,static,1342308352
Control25=IDC_STATIC,static,1342308352
Control26=IDC_STATIC,static,1342308352
Control27=IDC_STATIC,static,1342308352
Control28=IDC_STATIC,static,1342308352
Control29=IDC_STATIC,static,1342308352
Control30=IDC_STATIC,button,1342177287
Control31=IDC_STATIC,button,1342177287
Control32=IDC_STATIC,button,1342177287
Control33=IDC_FONT,msctls_trackbar32,1342242821
Control34=IDC_TEST,static,1342308865
Control35=IDC_STATIC,static,1342308352
Control36=IDC_STATIC,static,1342308352
Control37=IDC_STATIC,static,1342308352
Control38=IDC_STATIC,static,1342308352

[DLG:IDD_PSSEARCH_FLIGHT_PAGE]
Type=1
Class=CSearchFlightPage
ControlCount=52
Control1=IDC_EDIT1,edit,1350631552
Control2=IDC_EDIT2,edit,1350631552
Control3=IDC_EDIT3,edit,1350631552
Control4=IDC_EDIT4,edit,1350631552
Control5=IDC_EDIT7,edit,1350631552
Control6=IDC_STATIC_1,static,1342308352
Control7=IDC_STATIC_2,static,1073872896
Control8=IDC_STATIC_3,static,1073872896
Control9=IDC_STATIC_4,static,1073872896
Control10=IDC_STATIC_5,static,1342308352
Control11=IDC_STATIC_6,static,1342308352
Control12=IDC_STATIC_7,static,1342308352
Control13=IDC_STATIC_8,static,1342308352
Control14=IDC_STATIC_9,static,1342308352
Control15=IDC_STATIC_10,static,1342308352
Control16=IDC_STATIC_11,static,1342308352
Control17=IDC_STATIC_12,static,1342308352
Control18=IDC_STATIC_13,static,1342308352
Control19=IDC_STATIC_14,static,1073872896
Control20=IDC_STATIC_15,static,1342308352
Control21=IDC_STATIC_16,static,1342308352
Control22=IDC_EDIT5,edit,1216413824
Control23=IDC_EDIT6,edit,1216413824
Control24=IDC_EDIT8,edit,1350631552
Control25=IDC_EDIT9,edit,1350631552
Control26=IDC_EDIT10,edit,1350631552
Control27=IDC_EDIT11,edit,1350631552
Control28=IDC_EDIT12,edit,1350631552
Control29=IDC_EDIT13,edit,1350631552
Control30=IDC_EDIT14,edit,1350631560
Control31=IDC_EDIT15,edit,1350631560
Control32=IDC_EDIT16,edit,1350631560
Control33=IDC_EDIT17,edit,1350631560
Control34=IDC_EDIT18,edit,1350631552
Control35=IDC_EDIT19,edit,1350631552
Control36=IDC_EDIT20,edit,1216413824
Control37=IDC_EDIT21,edit,1216413824
Control38=IDC_EDIT22,edit,1350631552
Control39=IDC_EDIT23,edit,1350631552
Control40=IDC_CHECK1,button,1208025091
Control41=IDC_CHECK2,button,1208025091
Control42=IDC_CHECK3,button,1342242819
Control43=IDC_CHECK4,button,1342242819
Control44=IDC_CHECK5,button,1342242819
Control45=IDC_CHECK6,button,1342242819
Control46=IDC_CHECK7,button,1342242819
Control47=IDC_CHECK8,button,1208025091
Control48=IDC_KOMPL_ROT,button,1208025091
Control49=IDC_CHECK9,button,1208025091
Control50=IDC_CHECK10,button,1208025091
Control51=IDC_CHECK11,button,1342242819
Control52=IDC_CHECK12,button,1342242819

[DLG:IDD_PSSORT_FLIGHTPAGE]
Type=1
Class=CSortFlightPage

[DLG:IDD_PSSPECFILTER_PAGE]
Type=1
Class=CSpecFilterPage

[DLG:IDD_PSUNIFILTER_PAGE]
Type=1
Class=CPsUniFilter

[DLG:IDD_PSUNISORT_PAGE]
Type=1
Class=CPSUniSortPage

[DLG:IDD_PSZEEITRAUM_FLIGHT_PAGE]
Type=1
Class=PSZeitraumPage

[DLG:IDD_REGISTERDLG]
Type=1
Class=RegisterDlg
ControlCount=5
Control1=IDC_STARTEN,button,1342242816
Control2=IDC_ABBRECHEN,button,1342242816
Control3=IDC_REGISTRIEREN,button,1342242816
Control4=IDC_TIME,edit,1342179332
Control5=IDC_TEXT,edit,1342179332

[DLG:IDD_REPORTTABLE]
Type=1
Class=CReportSeasonTableDlg
ControlCount=6
Control1=IDC_FILE,button,1342373897
Control2=IDC_PRINTER,button,1342242825
Control3=IDC_DRUCKEN,button,1342242817
Control4=IDC_BEENDEN,button,1342242816
Control5=IDC_STATIC,static,1342308864
Control6=IDC_CUSTOM,GXWND,1353777152

[DLG:IDD_MONITOR_SETUP]
Type=1
Class=SetupDlg

[DLG:IDD_UNI_EINGABE]
Type=1
Class=UniEingabe
ControlCount=4
Control1=IDC_EINGABE,edit,1350631552
Control2=IDOK,button,1342242817
Control3=IDCANCEL,button,1342242816
Control4=IDC_LABEL,static,1342308352

[DLG:IDD_CCA_COMMON_DLG (English (U.K.) - _ENGLISH)]
Type=1
Class=CCcaCommonDlg
ControlCount=26
Control1=IDC_SCHALTER,edit,1350631560
Control2=IDC_TERMINAL,edit,1216413832
Control3=IDC_FLUGGS,edit,1216413832
Control4=IDC_BEGINN_D,edit,1350631552
Control5=IDC_BEGINN_T,edit,1350631552
Control6=IDC_ENDE_D,edit,1350631552
Control7=IDC_ENDE_T,edit,1350631552
Control8=IDC_BEMERKUNG,edit,1216413824
Control9=IDOK,button,1342242817
Control10=IDCANCEL,button,1342242816
Control11=IDC_STATIC,static,1342308352
Control12=IDC_BEM_TEXT,static,1208090624
Control13=IDC_STATIC,static,1342308352
Control14=IDC_STATIC,static,1342308352
Control15=IDC_STATIC,static,1208090624
Control16=IDC_STATIC,static,1208090624
Control17=IDC_DISP,edit,1350631552
Control18=IDC_DISP_TEXT,static,1342308352
Control19=IDC_FIDS_REM_LIST,button,1208025088
Control20=IDC_ACT_START_DATE,edit,1216413824
Control21=IDC_ACT_START_TIME,edit,1216413824
Control22=IDC_ACT_END_DATE,edit,1216413824
Control23=IDC_ACT_END_TIME,edit,1216413824
Control24=IDC_STATIC,static,1208090624
Control25=IDC_STATIC,static,1208090624
Control26=IDC_SCROLLBAR1,scrollbar,1073741825

[ACL:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_EDIT_COPY
Command2=ID_EDIT_PASTE
Command3=ID_EDIT_UNDO
Command4=ID_EDIT_CUT
Command5=ID_NEXT_PANE
Command6=ID_PREV_PANE
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_EDIT_CUT
Command10=ID_EDIT_UNDO
Command11=ID_CONTEXT_HELP
Command12=ID_HELP
CommandCount=12

[DLG:IDD_CCADIAGRAM]
Type=1
Class=?
ControlCount=0

[DLG:IDD_TIMESCALE]
Type=1
Class=?
ControlCount=26
Control1=65535,static,1342308352
Control2=IDC_STARTDATE,edit,1350631552
Control3=IDC_STARTTIME,edit,1350631552
Control4=65535,static,1342308352
Control5=IDC_24H,button,1342308361
Control6=IDC_12H,button,1342177289
Control7=IDC_10H,button,1342177289
Control8=IDC_8H,button,1342177289
Control9=IDC_6H,button,1342177289
Control10=IDC_4H,button,1342177289
Control11=65535,static,1342308352
Control12=65535,static,1342177287
Control13=IDC_100PERCENT,button,1476526089
Control14=IDC_75PERCENT,button,1476395017
Control15=IDC_50PERCENT,button,1476395017
Control16=65535,static,1342308352
Control17=IDC_HEIGHT,msctls_trackbar32,1350631429
Control18=65535,static,1342177287
Control19=65535,static,1342308352
Control20=IDOK,button,1342242817
Control21=IDCANCEL,button,1342242816
Control22=65535,static,1342308352
Control23=65535,static,1342308352
Control24=65535,static,1342308352
Control25=65535,static,1342308352
Control26=65535,static,1342308352

[DLG:IDD_ABOUTBOX (English (U.K.) - _ENGLISH)]
Type=1
Class=?
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_CCA_CHANGE_TIMES (English (U.K.) - _ENGLISH)]
Type=1
Class=?
ControlCount=6
Control1=IDC_CKBS,edit,1350631552
Control2=IDC_CKES,edit,1350631552
Control3=IDOK,button,1342242817
Control4=IDCANCEL,button,1342242816
Control5=65535,static,1342308352
Control6=65535,static,1342308352

[DLG:IDD_CCA_DELETE_FROMTO (English (U.K.) - _ENGLISH)]
Type=1
Class=DelelteCounterDlg
ControlCount=8
Control1=IDC_DATEFROM,edit,1350631552
Control2=IDC_DATETO,edit,1350631552
Control3=IDC_VK,edit,1350631552
Control4=IDOK,button,1342242817
Control5=IDCANCEL,button,1342242816
Control6=IDC_STATIC,static,1342308352
Control7=IDC_STATIC,static,1342308352
Control8=IDC_STATIC,static,1342308352

[DLG:IDD_CCAALLOCATE_PARAMETER (English (U.K.) - _ENGLISH)]
Type=1
Class=?
ControlCount=5
Control1=IDC_ALL,button,1342242816
Control2=IDC_CANCEL,button,1342242817
Control3=IDC_PROGRESS1,msctls_progress32,1350565888
Control4=IDC_STATIC,static,1342308352
Control5=IDC_TIME,static,1342312450

[DLG:IDD_CCADIAGRAM (English (U.K.) - _ENGLISH)]
Type=1
Class=?
ControlCount=13
Control1=IDC_ANSICHT,button,1342242816
Control2=IDC_VIEW,combobox,1344340227
Control3=IDC_ZEIT,button,1342242816
Control4=IDC_BEENDEN,button,1342242816
Control5=IDC_EXPAND,button,1342242816
Control6=IDC_DELETE_TIMEFRAME,button,1342242816
Control7=IDC_ALLOCATE,button,1342242816
Control8=IDC_FLIGHTS_WITHOUT_DEM,button,1342242816
Control9=IDC_PRINT_GANTT,button,1342242816
Control10=IDC_COMMON_CCA,button,1342242816
Control11=IDC_CICDEMAND,button,1342242816
Control12=IDC_CIC_CONF,button,1342242816
Control13=IDC_SEASON_SEL,combobox,1344339971

[DLG:IDD_CCAEXPAND (English (U.K.) - _ENGLISH)]
Type=1
Class=?
ControlCount=9
Control1=IDC_ALL,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_PROG,msctls_progress32,1350565888
Control4=IDC_ONLY,button,1342242817
Control5=IDC_DATEFROM,edit,1350631552
Control6=IDC_DATETO,edit,1350631552
Control7=IDC_STATIC,static,1342308352
Control8=IDC_STATIC,static,1342308352
Control9=IDC_ALLVT,button,1342242819

[DLG:IDD_CICCONFTABLE (English (U.K.) - _ENGLISH)]
Type=1
Class=?
ControlCount=0

[DLG:IDD_CICDEMANDTABLE (English (U.K.) - _ENGLISH)]
Type=1
Class=?
ControlCount=0

[DLG:IDD_CICFCONFTABLE (English (U.K.) - _ENGLISH)]
Type=1
Class=?
ControlCount=4
Control1=IDC_B_CONFIRM,button,1342242816
Control2=IDC_B_CONFIRMALL,button,1342242816
Control3=IDC_R_ALLCONF,button,1342308361
Control4=IDC_R_FLIGHTCONF,button,1342177289

[DLG:IDD_CICNODEMANDTABLE (English (U.K.) - _ENGLISH)]
Type=1
Class=?
ControlCount=0

[DLG:IDD_INITIALLOAD (English (U.K.) - _ENGLISH)]
Type=1
Class=?
ControlCount=2
Control1=IDC_MSGLIST,listbox,1352745216
Control2=IDC_PROGRESS1,msctls_progress32,1350565888

[DLG:IDD_LISTE (English (U.K.) - _ENGLISH)]
Type=1
Class=?
ControlCount=2
Control1=IDOK,button,1342242817
Control2=IDC_LIST,listbox,1352728840

[DLG:IDD_LOGIN (English (U.K.) - _ENGLISH)]
Type=1
Class=?
ControlCount=6
Control1=IDC_PASSWORD,edit,1350631584
Control2=IDOK,button,1342242817
Control3=IDCANCEL,button,1342242816
Control4=IDC_USIDCAPTION,static,1342308352
Control5=IDC_PASSCAPTION,static,1342308352
Control6=IDC_USERNAME,edit,1350631552

[DLG:IDD_PSGEOMETRIE (English (U.K.) - _ENGLISH)]
Type=1
Class=?
ControlCount=38
Control1=IDC_PFC,button,1207959561
Control2=IDC_GEG,button,1207959561
Control3=IDC_LIST1,listbox,1218445577
Control4=IDC_NEW,button,1207959552
Control5=IDC_DELETE,button,1207959552
Control6=IDC_RB_MIN,button,1342373897
Control7=IDC_RB_MAX,button,1342242825
Control8=IDC_FLT_TIMELINES,button,1342242819
Control9=IDC_STARTDATE,edit,1350631552
Control10=IDC_STARTTIME,edit,1350631552
Control11=IDC_24H,button,1342373897
Control12=IDC_8H,button,1342242825
Control13=IDC_12H,button,1342242825
Control14=IDC_6H,button,1342242825
Control15=IDC_10H,button,1342242825
Control16=IDC_4H,button,1342242825
Control17=IDC_HEIGHT,msctls_trackbar32,1342242821
Control18=IDC_100PERCENT,button,1342373897
Control19=IDC_75PERCENT,button,1342242825
Control20=IDC_50PERCENT,button,1342242825
Control21=IDC_GRUPPE,static,1207959552
Control22=IDC_STATIC,button,1342177287
Control23=IDC_STATIC,static,1342308352
Control24=IDC_STATIC,static,1342308352
Control25=IDC_STATIC,static,1342308352
Control26=IDC_STATIC,static,1342308352
Control27=IDC_STATIC,static,1342308352
Control28=IDC_STATIC,static,1342308352
Control29=IDC_STATIC,static,1342308352
Control30=IDC_STATIC,button,1342177287
Control31=IDC_STATIC,button,1342177287
Control32=IDC_STATIC,button,1342177287
Control33=IDC_FONT,msctls_trackbar32,1342242821
Control34=IDC_TEST,static,1342308865
Control35=IDC_STATIC,static,1342308352
Control36=IDC_STATIC,static,1342308352
Control37=IDC_STATIC,static,1342308352
Control38=IDC_STATIC,static,1342308352

[DLG:IDD_PSSEARCH_FLIGHT_PAGE (English (U.K.) - _ENGLISH)]
Type=1
Class=?
ControlCount=52
Control1=IDC_STATIC_1,static,1342308352
Control2=IDC_STATIC_2,static,1073872896
Control3=IDC_STATIC_3,static,1073872896
Control4=IDC_STATIC_4,static,1073872896
Control5=IDC_STATIC_5,static,1342308352
Control6=IDC_STATIC_6,static,1342308352
Control7=IDC_STATIC_7,static,1342308352
Control8=IDC_STATIC_8,static,1342308352
Control9=IDC_STATIC_9,static,1342308352
Control10=IDC_STATIC_10,static,1342308352
Control11=IDC_STATIC_11,static,1342308352
Control12=IDC_STATIC_12,static,1342308352
Control13=IDC_STATIC_13,static,1342308352
Control14=IDC_STATIC_14,static,1073872898
Control15=IDC_STATIC_15,static,1342308352
Control16=IDC_STATIC_16,static,1342308352
Control17=IDC_EDIT1,edit,1350631552
Control18=IDC_EDIT2,edit,1350631552
Control19=IDC_EDIT3,edit,1350631552
Control20=IDC_EDIT4,edit,1350631552
Control21=IDC_EDIT5,edit,1216413824
Control22=IDC_EDIT6,edit,1216413824
Control23=IDC_EDIT7,edit,1350631552
Control24=IDC_EDIT8,edit,1350631552
Control25=IDC_EDIT9,edit,1350631552
Control26=IDC_EDIT10,edit,1350631552
Control27=IDC_EDIT11,edit,1350631552
Control28=IDC_EDIT12,edit,1350631552
Control29=IDC_EDIT13,edit,1350631552
Control30=IDC_EDIT14,edit,1350631560
Control31=IDC_EDIT15,edit,1350631560
Control32=IDC_EDIT16,edit,1350631560
Control33=IDC_EDIT17,edit,1350631560
Control34=IDC_EDIT18,edit,1350631552
Control35=IDC_EDIT19,edit,1350631552
Control36=IDC_EDIT20,edit,1216413824
Control37=IDC_EDIT21,edit,1216413824
Control38=IDC_EDIT22,edit,1350631552
Control39=IDC_EDIT23,edit,1350631552
Control40=IDC_CHECK1,button,1208025091
Control41=IDC_CHECK2,button,1208025091
Control42=IDC_CHECK3,button,1342242819
Control43=IDC_CHECK4,button,1342242819
Control44=IDC_CHECK5,button,1342242819
Control45=IDC_CHECK6,button,1342242819
Control46=IDC_CHECK7,button,1342242819
Control47=IDC_CHECK8,button,1208025091
Control48=IDC_KOMPL_ROT,button,1208025091
Control49=IDC_CHECK9,button,1208025091
Control50=IDC_CHECK10,button,1208025091
Control51=IDC_CHECK11,button,1342242819
Control52=IDC_CHECK12,button,1342242819

[DLG:IDD_REGISTERDLG (English (U.K.) - _ENGLISH)]
Type=1
Class=?
ControlCount=5
Control1=IDC_STARTEN,button,1342242816
Control2=IDC_ABBRECHEN,button,1342242816
Control3=IDC_REGISTRIEREN,button,1342242816
Control4=IDC_TIME,edit,1342179332
Control5=IDC_TEXT,edit,1342179332

[DLG:IDD_TIMESCALE (English (U.K.) - _ENGLISH)]
Type=1
Class=?
ControlCount=26
Control1=65535,static,1342308352
Control2=IDC_STARTDATE,edit,1350631552
Control3=IDC_STARTTIME,edit,1350631552
Control4=65535,static,1342308352
Control5=IDC_24H,button,1342308361
Control6=IDC_12H,button,1342177289
Control7=IDC_10H,button,1342177289
Control8=IDC_8H,button,1342177289
Control9=IDC_6H,button,1342177289
Control10=IDC_4H,button,1342177289
Control11=65535,static,1342308352
Control12=65535,static,1342177287
Control13=IDC_100PERCENT,button,1476526089
Control14=IDC_75PERCENT,button,1476395017
Control15=IDC_50PERCENT,button,1476395017
Control16=65535,static,1342308352
Control17=IDC_HEIGHT,msctls_trackbar32,1350631429
Control18=65535,static,1342177287
Control19=65535,static,1342308352
Control20=IDOK,button,1342242817
Control21=IDCANCEL,button,1342242816
Control22=65535,static,1342308352
Control23=65535,static,1342308352
Control24=65535,static,1342308352
Control25=65535,static,1342308352
Control26=65535,static,1342308352

[DLG:IDD_UNI_EINGABE (English (U.K.) - _ENGLISH)]
Type=1
Class=?
ControlCount=4
Control1=IDC_EINGABE,edit,1350631552
Control2=IDOK,button,1342242817
Control3=IDCANCEL,button,1342242816
Control4=IDC_LABEL,static,1342308352

[DLG:IDD_CREATE_DEMANDS (English (U.K.) - _ENGLISH)]
Type=1
Class=?
ControlCount=31
Control1=IDC_ZEITRAUMSTATIC,button,1342177287
Control2=IDC_STIMEFROM,static,1342177280
Control3=IDC_STIMETO,static,1342177280
Control4=IDC_SFROMDATE,edit,1350631552
Control5=IDC_SFROMTIME,edit,1350631552
Control6=IDC_STODATE,edit,1350631552
Control7=IDC_STOTIME,edit,1350631552
Control8=IDC_LSTUSTATIC,button,1342177287
Control9=IDC_LSTUFROM,static,1342177280
Control10=IDC_LSTUDATE,edit,1350631552
Control11=IDC_LSTUTIME,edit,1350631552
Control12=IDC_FLUGSTATIC,button,1342177287
Control13=IDC_FLIGHTCODE,static,1342308352
Control14=IDC_FLIGHTAIRLINE,edit,1350631552
Control15=IDC_FLIGHTNR,edit,1350631552
Control16=IDC_FLIGHTSUFFIX,edit,1350631552
Control17=IDC_REGISTRATION,static,1342308352
Control18=IDC_REGN,edit,1350631560
Control19=IDC_ACTYPE,static,1342308352
Control20=IDC_ACT3,edit,1350631560
Control21=IDC_AIRPORT,static,1342308352
Control22=IDC_AIRP,edit,1350631560
Control23=IDC_AIRPORT2,static,1342308352
Control24=IDC_AIRP2,edit,1350631560
Control25=IDC_BEMERKUNG,static,1342308352
Control26=IDC_REMA,edit,1350631560
Control27=IDC_FTYPSTATIC,button,1342177287
Control28=IDC_BETRIEB,button,1342242819
Control29=IDC_PLANUNG,button,1342242819
Control30=ID_CREATE_DEMANDS_OK,button,1342242817
Control31=IDCANCEL,button,1342242816

[DLG:IDD_DAILYCCAPRINT (English (U.K.) - _ENGLISH)]
Type=1
Class=?
ControlCount=5
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_VOMTAG,edit,1350631552
Control4=IDC_VONZEIT,static,1476526592
Control5=IDC_STATIC,static,1342308352

[DLG:IDD_REPORTTABLE (English (U.K.) - _ENGLISH)]
Type=1
Class=?
ControlCount=6
Control1=IDC_FILE,button,1342373897
Control2=IDC_PRINTER,button,1342242825
Control3=IDC_DRUCKEN,button,1342242817
Control4=IDC_BEENDEN,button,1342242816
Control5=IDC_STATIC,static,1342308864
Control6=IDC_CUSTOM,GXWND,1353777152

[MNU:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_VIEW
Command2=ID_RULES
Command3=ID_BDPSUIF
Command4=ID_PRINT
Command5=ID_APP_EXIT
Command6=ID_NOW
Command7=ID_DELETE
Command8=ID_ALLOCATE
Command9=ID_CREATE_DEMANDS
Command10=ID_REPORT
Command11=ID_REPORT_SEASON
Command12=ID_CONFLICTS
Command13=ID_FLIGHT_CONFLICTS
Command14=ID_DEMAND
Command15=ID_WO_DEMAND
Command16=ID_SYMBOLTEXT
Command17=ID_HELP_FINDER
Command18=ID_APP_ABOUT
CommandCount=18

[MNU:IDR_MAINFRAME (English (U.K.) - _ENGLISH)]
Type=1
Class=?
Command1=ID_VIEW
Command2=ID_RULES
Command3=ID_BDPSUIF
Command4=ID_PRINT
Command5=ID_APP_EXIT
Command6=ID_NOW
Command7=ID_DELETE
Command8=ID_ALLOCATE
Command9=ID_CREATE_DEMANDS
Command10=ID_CREATE_COMMON
Command11=ID_REPORT
Command12=ID_REPORT_SEASON
Command13=ID_CONFLICTS
Command14=ID_FLIGHT_CONFLICTS
Command15=ID_DEMAND
Command16=ID_WO_DEMAND
Command17=ID_SYMBOLTEXT
Command18=ID_HELP_FINDER
Command19=ID_APP_ABOUT
CommandCount=19

[TB:IDR_BLANK_BAR]
Type=1
Class=?
Command1=ID_BB_BLANK
CommandCount=1

[TB:IDR_LEFT_BAR]
Type=1
Class=?
Command1=LEFT
CommandCount=1

[TB:IDR_MAINFRAME1]
Type=1
Class=?
Command1=ID_VIEW
CommandCount=1

[TB:IDR_MAINFRAME2]
Type=1
Class=?
Command1=ID_NOW
Command2=ID_DELETE
Command3=ID_ALLOCATE
Command4=ID_WO_DEMAND
Command5=ID_DEMAND
Command6=ID_FLIGHT_CONFLICTS
Command7=ID_CONFLICTS
Command8=ID_BDPSUIF
Command9=ID_RULES
Command10=ID_PRINT
Command11=ID_APP_EXIT
CommandCount=11

[TB:IDR_RIGHT_BAR]
Type=1
Class=?
Command1=RIGHT
CommandCount=1

[TB:IDR_FOPEN]
Type=1
Class=?
Command1=ID_FILE_OPEN
CommandCount=1

[TB:IDR_MAINFRAME1 (English (U.K.) - _ENGLISH)]
Type=1
Class=?
Command1=ID_VIEW
CommandCount=1

[TB:IDR_MAINFRAME2 (English (U.K.) - _ENGLISH)]
Type=1
Class=?
Command1=ID_NOW
Command2=ID_DELETE
Command3=ID_ALLOCATE
Command4=ID_WO_DEMAND
Command5=ID_DEMAND
Command6=ID_FLIGHT_CONFLICTS
Command7=ID_CONFLICTS
Command8=ID_BDPSUIF
Command9=ID_RULES
Command10=ID_PRINT
Command11=ID_APP_EXIT
CommandCount=11

[TB:IDR_RIGHT_BAR (English (U.K.) - _ENGLISH)]
Type=1
Class=?
Command1=RIGHT
CommandCount=1

[TB:IDR_BLANK_BAR (English (U.K.) - _ENGLISH)]
Type=1
Class=?
Command1=ID_BB_BLANK
CommandCount=1

[TB:IDR_LEFT_BAR (English (U.K.) - _ENGLISH)]
Type=1
Class=?
Command1=LEFT
CommandCount=1

[TB:IDR_FOPEN (English (U.K.) - _ENGLISH)]
Type=1
Class=?
Command1=ID_FILE_OPEN
CommandCount=1

[DLG:IDD_SEASON (English (U.K.) - _ENGLISH)]
Type=1
Class=?
ControlCount=205
Control1=IDC_PVOM,edit,1350631552
Control2=IDC_PBIS,edit,1350631552
Control3=IDC_FREQ,edit,1350631552
Control4=IDC_ACT3,edit,1350631560
Control5=IDC_ACT5,edit,1350631560
Control6=IDC_MING,edit,1350631552
Control7=IDC_AALC3,edit,1350631560
Control8=IDC_AFLTN,edit,1350631552
Control9=IDC_AFLNS,edit,1350631552
Control10=IDC_ASHOWJFNO,button,1342242816
Control11=IDC_ADAYS,edit,1350631552
Control12=IDC_ADALY,button,1342242816
Control13=IDC_ACXX,button,1342242819
Control14=IDC_ANOOP,button,1342242819
Control15=IDC_ATTYP,edit,1350631560
Control16=IDC_ASTYP,edit,1350631560
Control17=IDC_ASTEV,edit,1350631560
Control18=IDC_APSTA,edit,1350631560
Control19=IDC_APABS,edit,1350631552
Control20=IDC_APAES,edit,1350631552
Control21=IDC_AGTA1,edit,1350631560
Control22=IDC_ATGA1,edit,1350631560
Control23=IDC_AGA1B,edit,1350631552
Control24=IDC_AGA1E,edit,1350631552
Control25=IDC_AGTA2,edit,1350631560
Control26=IDC_ATGA2,edit,1350631560
Control27=IDC_AGA2B,edit,1350631552
Control28=IDC_AGA2E,edit,1350631552
Control29=IDC_ABLT1,edit,1350631560
Control30=IDC_ATMB1,edit,1350631560
Control31=IDC_AB1BS,edit,1350631552
Control32=IDC_AB1ES,edit,1350631552
Control33=IDC_ABLT2,edit,1350631560
Control34=IDC_ATMB2,edit,1350631560
Control35=IDC_AB2BS,edit,1350631552
Control36=IDC_AB2ES,edit,1350631552
Control37=IDC_AEXT1,edit,1350631560
Control38=IDC_ATET1,edit,1350631560
Control39=IDC_AEXT2,edit,1350631560
Control40=IDC_ATET2,edit,1350631560
Control41=IDC_ASTATUS,button,1342373897
Control42=IDC_ASTATUS2,button,1342242825
Control43=IDC_ASTATUS3,button,1342242825
Control44=IDC_AREM1,edit,1350631492
Control45=IDC_DALC3,edit,1350631560
Control46=IDC_DFLTN,edit,1350631552
Control47=IDC_DFLNS,edit,1350631552
Control48=IDC_DSHOWJFNO,button,1342242816
Control49=IDC_DDAYS,edit,1350631552
Control50=IDC_DDALY,button,1342242816
Control51=IDC_DCXX,button,1342242819
Control52=IDC_DNOOP,button,1342242819
Control53=IDC_DTTYP,edit,1350631560
Control54=IDC_DSTYP,edit,1350631560
Control55=IDC_DSTEV,edit,1350631560
Control56=IDC_DPSTD,edit,1350631560
Control57=IDC_DPDBS,edit,1350631552
Control58=IDC_DPDES,edit,1350631552
Control59=IDC_DGTD1,edit,1350631560
Control60=IDC_DTGD1,edit,1350631560
Control61=IDC_DGD1B,edit,1350631552
Control62=IDC_DGD1E,edit,1350631552
Control63=IDC_DGTD2,edit,1350631560
Control64=IDC_DTGD2,edit,1350631560
Control65=IDC_DGD2B,edit,1350631552
Control66=IDC_DGD2E,edit,1350631552
Control67=IDC_DWRO1,edit,1350631560
Control68=IDC_DTWR1,edit,1350631560
Control69=IDC_DW1BS,edit,1350631552
Control70=IDC_DW1ES,edit,1350631552
Control71=IDC_DSTATUS,button,1342373897
Control72=IDC_DSTATUS2,button,1342242825
Control73=IDC_DSTATUS3,button,1342242825
Control74=IDC_DREM1,edit,1350631492
Control75=IDOK,button,1342242827
Control76=IDCANCEL,button,1342242816
Control77=IDC_PREVFLIGHT,button,1208025088
Control78=IDC_NEXTFLIGHT,button,1208025088
Control79=IDC_ARESET,button,1208025088
Control80=IDC_DRESET,button,1208025088
Control81=IDC_AJFNO_BORDER,static,1476526080
Control82=IDC_ASTOACALC,button,1207959552
Control83=IDC_AFLUKO,edit,1350568064
Control84=IDC_ACREATED,edit,1350568064
Control85=IDC_ALASTCHANGE,edit,1350568064
Control86=IDC_STATIC,static,1342308352
Control87=IDC_STATIC,static,1342308352
Control88=IDC_STATIC,static,1342308352
Control89=IDC_STATIC,button,1342177287
Control90=IDC_STATIC,static,1342308352
Control91=IDC_STATIC,static,1342308352
Control92=IDC_STATIC,static,1342308352
Control93=IDC_STATIC,static,1342308352
Control94=IDC_SEAS,edit,1350568064
Control95=IDC_STATIC,static,1342308352
Control96=IDC_STATIC,static,1342308352
Control97=IDC_STATIC_ANKUNFT,button,1342177287
Control98=IDC_STATIC_ABFLUG,button,1342177287
Control99=IDC_STATIC,static,1342308352
Control100=IDC_STATIC,static,1342308352
Control101=IDC_STATIC,static,1342308352
Control102=IDC_STATIC,static,1342308352
Control103=IDC_STATIC,static,1342308352
Control104=IDC_STATIC,static,1342308352
Control105=IDC_STATIC,static,1342308352
Control106=IDC_STATIC,static,1342308352
Control107=IDC_STATIC,static,1342308352
Control108=IDC_STATIC,static,1342308352
Control109=IDC_STATIC,static,1342308352
Control110=IDC_STATIC,static,1342308352
Control111=IDC_STATIC,static,1342308352
Control112=IDC_STATIC,static,1342308352
Control113=IDC_STATIC,static,1342308352
Control114=IDC_STATIC,button,1342177287
Control115=IDC_STATIC,button,1342177287
Control116=IDC_DSTODCALC,button,1207959552
Control117=IDC_APSTALIST,button,1342177280
Control118=IDC_AGTA1LIST,button,1342177280
Control119=IDC_ABLT2LIST,button,1342177280
Control120=IDC_AEXT1LIST,button,1342177280
Control121=IDC_AGTA2LIST,button,1342177280
Control122=IDC_ABLT1LIST,button,1342177280
Control123=IDC_AEXT2LIST,button,1342177280
Control124=IDC_DALC3LIST,button,1342177280
Control125=IDC_ATTYPLIST,button,1342177280
Control126=IDC_ASTYPLIST,button,1342177280
Control127=IDC_DTTYPLIST,button,1342177280
Control128=IDC_DSTYPLIST,button,1342177280
Control129=IDC_ACT35LIST,button,1342242816
Control130=IDC_STATIC,static,1342308352
Control131=IDC_AAlc3LIST2,button,1342177280
Control132=IDC_AAlc3LIST3,button,1342177280
Control133=IDC_AAlc3LIST,button,1342242816
Control134=IDC_DJFNO_BORDER,static,1476526080
Control135=IDC_DFLUKO,edit,1350568064
Control136=IDC_DCREATED,edit,1350568064
Control137=IDC_DLASTCHANGE,edit,1350568064
Control138=IDC_STATIC,static,1342308352
Control139=IDC_STATIC,static,1342308352
Control140=IDC_STATIC,static,1342308352
Control141=IDC_DALC3LIST2,button,1342177280
Control142=IDC_DALC3LIST3,button,1342177280
Control143=IDC_DEFAULT,button,1207959552
Control144=IDC_STATIC,static,1342308352
Control145=IDC_STATIC,static,1342308352
Control146=IDC_STATIC,static,1342308352
Control147=IDC_DPSTDLIST,button,1342177280
Control148=IDC_DGTD1LIST,button,1342177280
Control149=IDC_DWRO1LIST,button,1342177280
Control150=IDC_DGTD2LIST,button,1342177280
Control151=IDC_STATIC,static,1342308352
Control152=IDC_DCINSBORDER,static,1476526080
Control153=IDC_DCINSLIST1,button,1342177280
Control154=IDC_DCINSLIST2,button,1342177280
Control155=IDC_DCINSLIST3,button,1342177280
Control156=IDC_DCINSLIST4,button,1342177280
Control157=IDC_STATIC,static,1342308352
Control158=IDC_DCINSMAL4,button,1342177280
Control159=IDC_NOSE,edit,1350631552
Control160=IDC_STATIC_NOSE,static,1342308352
Control161=IDC_DCHT3,edit,1350631560
Control162=IDC_STATIC_DCHT3,static,1342308352
Control163=IDC_ACHT3,edit,1350631560
Control164=IDC_STATIC_ACHT3,static,1342308352
Control165=IDC_DREM2,edit,1350631492
Control166=IDC_AREM2,edit,1350631492
Control167=IDC_AORG3,edit,1350631560
Control168=IDC_ASTOD,edit,1350631552
Control169=IDC_ASTOA,edit,1350631552
Control170=IDC_AETAI,edit,1350631552
Control171=IDC_DSTOD,edit,1350631552
Control172=IDC_DETDI,edit,1350631552
Control173=IDC_DDES3,edit,1350631560
Control174=IDC_DSTOA,edit,1350631552
Control175=IDC_STATIC,static,1342308352
Control176=IDC_STATIC,static,1342308352
Control177=IDC_ADES3,edit,1350568064
Control178=IDC_STATIC,static,1342308352
Control179=IDC_STATIC,static,1342308352
Control180=IDC_DORG3,edit,1350568064
Control181=IDC_AORG3LIST,button,1342177280
Control182=IDC_DDES3LIST,button,1342177280
Control183=IDC_AORG4,edit,1350631560
Control184=IDC_STATIC,static,1342308352
Control185=IDC_STATIC,static,1342308352
Control186=IDC_STATIC,static,1342308352
Control187=IDC_STATIC,static,1342308352
Control188=IDC_STATIC,static,1342308352
Control189=IDC_DDES4,edit,1350631560
Control190=IDC_STATIC,static,1342308352
Control191=IDC_STATIC,static,1342308352
Control192=IDC_STATIC,static,1342308352
Control193=IDC_STATIC,static,1342308352
Control194=IDC_STATIC,static,1342308352
Control195=IDC_AREMP,combobox,1344339971
Control196=IDC_DREMP,combobox,1344339971
Control197=IDC_STATIC,static,1342308352
Control198=IDC_STATIC,static,1342308352
Control199=IDC_AFLTI,edit,1350631560
Control200=IDC_DFLTI,edit,1350631560
Control201=IDC_STATIC,static,1342308352
Control202=IDC_DBAZ1,edit,1350631552
Control203=IDC_DBAZ4,edit,1350631552
Control204=IDC_AGENTS,button,1342242816
Control205=IDC_DAILY_MASK,button,1208025099

[DLG:IDD_SEASONASK (English (U.K.) - _ENGLISH)]
Type=1
Class=?
ControlCount=4
Control1=IDC_MSGLIST,listbox,1352745216
Control2=IDC_DOWNLOAD,button,1342242816
Control3=IDCANCEL,button,1342242816
Control4=IDC_VIEWACTIV,button,1208025091

[DLG:IDD_VIATABLE (English (U.K.) - _ENGLISH)]
Type=1
Class=?
ControlCount=0

[DLG:IDD_AWBASICDATA (English (U.K.) - _ENGLISH)]
Type=1
Class=?
ControlCount=4
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_LIST,listbox,1352728833
Control4=IDC_HEADER,static,1342312448

[DLG:IDD_AWBASICDATA]
Type=1
Class=?
ControlCount=4
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_LIST,listbox,1352728833
Control4=IDC_HEADER,static,1342312448

[DLG:IDD_ROTATION_HAI (English (U.K.) - _ENGLISH)]
Type=1
Class=?
ControlCount=5
Control1=IDC_UPDATE,button,1073807360
Control2=IDC_INSERT,button,1073807360
Control3=IDC_DELETE,button,1073807360
Control4=IDC_CLOSE,button,1342242816
Control5=IDC_GMBORDER,static,1476526080

[DLG:IDD_CCI_DEMANDDETAIL (English (U.K.))]
Type=1
Class=?
ControlCount=5
Control1=IDC_STATIC_RULE,button,1342177287
Control2=IDC_SCHLIEBEN,button,1342242816
Control3=IDC_STATIC_RULENAME,static,1342308352
Control4=IDC_EDIT_RULENAME,edit,1350633600
Control5=IDC_STATIC_FLIGHTS,button,1342177287

[ACL:IDR_MAINFRAME (German (Germany))]
Type=1
Class=?
Command1=ID_EDIT_COPY
Command2=ID_EDIT_PASTE
Command3=ID_EDIT_UNDO
Command4=ID_EDIT_CUT
Command5=ID_NEXT_PANE
Command6=ID_PREV_PANE
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_EDIT_CUT
Command10=ID_EDIT_UNDO
Command11=ID_CONTEXT_HELP
Command12=ID_HELP
CommandCount=12

[DLG:IDD_ABOUTBOX (German (Germany))]
Type=1
Class=?
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_CCA_CHANGE_TIMES (German (Germany))]
Type=1
Class=?
ControlCount=6
Control1=IDC_CKBS,edit,1350631552
Control2=IDC_CKES,edit,1350631552
Control3=IDOK,button,1342242817
Control4=IDCANCEL,button,1342242816
Control5=65535,static,1342308352
Control6=65535,static,1342308352

[DLG:IDD_CCA_DELETE_FROMTO (German (Germany))]
Type=1
Class=?
ControlCount=8
Control1=IDC_DATEFROM,edit,1350631552
Control2=IDC_DATETO,edit,1350631552
Control3=IDC_VK,edit,1350631552
Control4=IDOK,button,1342242817
Control5=IDCANCEL,button,1342242816
Control6=IDC_STATIC,static,1342308352
Control7=IDC_STATIC,static,1342308352
Control8=IDC_STATIC,static,1342308352

[DLG:IDD_CCAALLOCATE_PARAMETER (German (Germany))]
Type=1
Class=?
ControlCount=5
Control1=IDC_ALL,button,1342242816
Control2=IDC_CANCEL,button,1342242817
Control3=IDC_PROGRESS1,msctls_progress32,1350565888
Control4=IDC_STATIC,static,1342308352
Control5=IDC_TIME,static,1342312450

[DLG:IDD_CCADIAGRAM (German (Germany))]
Type=1
Class=?
ControlCount=0

[DLG:IDD_CCAEXPAND (German (Germany))]
Type=1
Class=?
ControlCount=9
Control1=IDC_ALL,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_PROG,msctls_progress32,1350565888
Control4=IDC_ONLY,button,1342242817
Control5=IDC_DATEFROM,edit,1350631552
Control6=IDC_DATETO,edit,1350631552
Control7=IDC_STATIC,static,1342308352
Control8=IDC_STATIC,static,1342308352
Control9=IDC_ALLVT,button,1342242819

[DLG:IDD_CICCONFTABLE (German (Germany))]
Type=1
Class=?
ControlCount=0

[DLG:IDD_CICDEMANDTABLE (German (Germany))]
Type=1
Class=?
ControlCount=0

[DLG:IDD_CICFCONFTABLE (German (Germany))]
Type=1
Class=?
ControlCount=4
Control1=IDC_B_CONFIRM,button,1342242816
Control2=IDC_B_CONFIRMALL,button,1342242816
Control3=IDC_R_ALLCONF,button,1342308361
Control4=IDC_R_FLIGHTCONF,button,1342177289

[DLG:IDD_CICNODEMANDTABLE (German (Germany))]
Type=1
Class=?
ControlCount=0

[DLG:IDD_INITIALLOAD (German (Germany))]
Type=1
Class=?
ControlCount=2
Control1=IDC_MSGLIST,listbox,1352745216
Control2=IDC_PROGRESS1,msctls_progress32,1350565888

[DLG:IDD_LISTE (German (Germany))]
Type=1
Class=?
ControlCount=2
Control1=IDOK,button,1342242817
Control2=IDC_LIST,listbox,1352728840

[DLG:IDD_LOGIN (German (Germany))]
Type=1
Class=?
ControlCount=6
Control1=IDC_PASSWORD,edit,1350631584
Control2=IDOK,button,1342242817
Control3=IDCANCEL,button,1342242816
Control4=IDC_USIDCAPTION,static,1342308352
Control5=IDC_PASSCAPTION,static,1342308352
Control6=IDC_USERNAME,edit,1350631552

[DLG:IDD_PSGEOMETRIE (German (Germany))]
Type=1
Class=?
ControlCount=38
Control1=IDC_PFC,button,1207959561
Control2=IDC_GEG,button,1207959561
Control3=IDC_LIST1,listbox,1218445577
Control4=IDC_NEW,button,1207959552
Control5=IDC_DELETE,button,1207959552
Control6=IDC_RB_MIN,button,1342373897
Control7=IDC_RB_MAX,button,1342242825
Control8=IDC_FLT_TIMELINES,button,1342242819
Control9=IDC_STARTDATE,edit,1350631552
Control10=IDC_STARTTIME,edit,1350631552
Control11=IDC_24H,button,1342373897
Control12=IDC_8H,button,1342242825
Control13=IDC_12H,button,1342242825
Control14=IDC_6H,button,1342242825
Control15=IDC_10H,button,1342242825
Control16=IDC_4H,button,1342242825
Control17=IDC_HEIGHT,msctls_trackbar32,1342242821
Control18=IDC_100PERCENT,button,1342373897
Control19=IDC_75PERCENT,button,1342242825
Control20=IDC_50PERCENT,button,1342242825
Control21=IDC_GRUPPE,static,1207959552
Control22=IDC_STATIC,button,1342177287
Control23=IDC_STATIC,static,1342308352
Control24=IDC_STATIC,static,1342308352
Control25=IDC_STATIC,static,1342308352
Control26=IDC_STATIC,static,1342308352
Control27=IDC_STATIC,static,1342308352
Control28=IDC_STATIC,static,1342308352
Control29=IDC_STATIC,static,1342308352
Control30=IDC_STATIC,button,1342177287
Control31=IDC_STATIC,button,1342177287
Control32=IDC_STATIC,button,1342177287
Control33=IDC_FONT,msctls_trackbar32,1342242821
Control34=IDC_TEST,static,1342308865
Control35=IDC_STATIC,static,1342308352
Control36=IDC_STATIC,static,1342308352
Control37=IDC_STATIC,static,1342308352
Control38=IDC_STATIC,static,1342308352

[DLG:IDD_PSSEARCH_FLIGHT_PAGE (German (Germany))]
Type=1
Class=?
ControlCount=52
Control1=IDC_EDIT1,edit,1350631552
Control2=IDC_EDIT2,edit,1350631552
Control3=IDC_EDIT3,edit,1350631552
Control4=IDC_EDIT4,edit,1350631552
Control5=IDC_EDIT7,edit,1350631552
Control6=IDC_STATIC_1,static,1342308352
Control7=IDC_STATIC_2,static,1073872896
Control8=IDC_STATIC_3,static,1073872896
Control9=IDC_STATIC_4,static,1073872896
Control10=IDC_STATIC_5,static,1342308352
Control11=IDC_STATIC_6,static,1342308352
Control12=IDC_STATIC_7,static,1342308352
Control13=IDC_STATIC_8,static,1342308352
Control14=IDC_STATIC_9,static,1342308352
Control15=IDC_STATIC_10,static,1342308352
Control16=IDC_STATIC_11,static,1342308352
Control17=IDC_STATIC_12,static,1342308352
Control18=IDC_STATIC_13,static,1342308352
Control19=IDC_STATIC_14,static,1073872896
Control20=IDC_STATIC_15,static,1342308352
Control21=IDC_STATIC_16,static,1342308352
Control22=IDC_EDIT5,edit,1216413824
Control23=IDC_EDIT6,edit,1216413824
Control24=IDC_EDIT8,edit,1350631552
Control25=IDC_EDIT9,edit,1350631552
Control26=IDC_EDIT10,edit,1350631552
Control27=IDC_EDIT11,edit,1350631552
Control28=IDC_EDIT12,edit,1350631552
Control29=IDC_EDIT13,edit,1350631552
Control30=IDC_EDIT14,edit,1350631560
Control31=IDC_EDIT15,edit,1350631560
Control32=IDC_EDIT16,edit,1350631560
Control33=IDC_EDIT17,edit,1350631560
Control34=IDC_EDIT18,edit,1350631552
Control35=IDC_EDIT19,edit,1350631552
Control36=IDC_EDIT20,edit,1216413824
Control37=IDC_EDIT21,edit,1216413824
Control38=IDC_EDIT22,edit,1350631552
Control39=IDC_EDIT23,edit,1350631552
Control40=IDC_CHECK1,button,1208025091
Control41=IDC_CHECK2,button,1208025091
Control42=IDC_CHECK3,button,1342242819
Control43=IDC_CHECK4,button,1342242819
Control44=IDC_CHECK5,button,1342242819
Control45=IDC_CHECK6,button,1342242819
Control46=IDC_CHECK7,button,1342242819
Control47=IDC_CHECK8,button,1208025091
Control48=IDC_KOMPL_ROT,button,1208025091
Control49=IDC_CHECK9,button,1208025091
Control50=IDC_CHECK10,button,1208025091
Control51=IDC_CHECK11,button,1342242819
Control52=IDC_CHECK12,button,1342242819

[DLG:IDD_REGISTERDLG (German (Germany))]
Type=1
Class=?
ControlCount=5
Control1=IDC_STARTEN,button,1342242816
Control2=IDC_ABBRECHEN,button,1342242816
Control3=IDC_REGISTRIEREN,button,1342242816
Control4=IDC_TIME,edit,1342179332
Control5=IDC_TEXT,edit,1342179332

[DLG:IDD_TIMESCALE (German (Germany))]
Type=1
Class=?
ControlCount=26
Control1=65535,static,1342308352
Control2=IDC_STARTDATE,edit,1350631552
Control3=IDC_STARTTIME,edit,1350631552
Control4=65535,static,1342308352
Control5=IDC_24H,button,1342308361
Control6=IDC_12H,button,1342177289
Control7=IDC_10H,button,1342177289
Control8=IDC_8H,button,1342177289
Control9=IDC_6H,button,1342177289
Control10=IDC_4H,button,1342177289
Control11=65535,static,1342308352
Control12=65535,static,1342177287
Control13=IDC_100PERCENT,button,1476526089
Control14=IDC_75PERCENT,button,1476395017
Control15=IDC_50PERCENT,button,1476395017
Control16=65535,static,1342308352
Control17=IDC_HEIGHT,msctls_trackbar32,1350631429
Control18=65535,static,1342177287
Control19=65535,static,1342308352
Control20=IDOK,button,1342242817
Control21=IDCANCEL,button,1342242816
Control22=65535,static,1342308352
Control23=65535,static,1342308352
Control24=65535,static,1342308352
Control25=65535,static,1342308352
Control26=65535,static,1342308352

[DLG:IDD_UNI_EINGABE (German (Germany))]
Type=1
Class=?
ControlCount=4
Control1=IDC_EINGABE,edit,1350631552
Control2=IDOK,button,1342242817
Control3=IDCANCEL,button,1342242816
Control4=IDC_LABEL,static,1342308352

[DLG:IDD_CREATE_DEMANDS (German (Germany))]
Type=1
Class=?
ControlCount=31
Control1=IDC_ZEITRAUMSTATIC,button,1342177287
Control2=IDC_STIMEFROM,static,1342177280
Control3=IDC_STIMETO,static,1342177280
Control4=IDC_SFROMDATE,edit,1350631552
Control5=IDC_SFROMTIME,edit,1350631552
Control6=IDC_STODATE,edit,1350631552
Control7=IDC_STOTIME,edit,1350631552
Control8=IDC_LSTUSTATIC,button,1342177287
Control9=IDC_LSTUFROM,static,1342177280
Control10=IDC_LSTUDATE,edit,1350631552
Control11=IDC_LSTUTIME,edit,1350631552
Control12=IDC_FLUGSTATIC,button,1342177287
Control13=IDC_FLIGHTCODE,static,1342308352
Control14=IDC_FLIGHTAIRLINE,edit,1350631552
Control15=IDC_FLIGHTNR,edit,1350631552
Control16=IDC_FLIGHTSUFFIX,edit,1350631552
Control17=IDC_REGISTRATION,static,1342308352
Control18=IDC_REGN,edit,1350631560
Control19=IDC_ACTYPE,static,1342308352
Control20=IDC_ACT3,edit,1350631560
Control21=IDC_AIRPORT,static,1342308352
Control22=IDC_AIRP,edit,1350631560
Control23=IDC_AIRPORT2,static,1342308352
Control24=IDC_AIRP2,edit,1350631560
Control25=IDC_BEMERKUNG,static,1342308352
Control26=IDC_REMA,edit,1350631560
Control27=IDC_FTYPSTATIC,button,1342177287
Control28=IDC_BETRIEB,button,1342242819
Control29=IDC_PLANUNG,button,1342242819
Control30=ID_CREATE_DEMANDS_OK,button,1342242817
Control31=IDCANCEL,button,1342242816

[DLG:IDD_DAILYCCAPRINT (German (Germany))]
Type=1
Class=?
ControlCount=5
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_VOMTAG,edit,1350631552
Control4=IDC_VONZEIT,static,1476526592
Control5=IDC_STATIC,static,1342308352

[DLG:IDD_REPORTTABLE (German (Germany))]
Type=1
Class=?
ControlCount=6
Control1=IDC_FILE,button,1342373897
Control2=IDC_PRINTER,button,1342242825
Control3=IDC_DRUCKEN,button,1342242817
Control4=IDC_BEENDEN,button,1342242816
Control5=IDC_STATIC,static,1342308864
Control6=IDC_CUSTOM,GXWND,1353777152

[DLG:IDD_AWBASICDATA (German (Germany))]
Type=1
Class=?
ControlCount=4
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_LIST,listbox,1352728833
Control4=IDC_HEADER,static,1342312448

[DLG:IDD_CCA_COMMON_DLG (German (Germany))]
Type=1
Class=?
ControlCount=26
Control1=IDC_SCHALTER,edit,1350631560
Control2=IDC_TERMINAL,edit,1216413832
Control3=IDC_FLUGGS,edit,1216413832
Control4=IDC_BEGINN_D,edit,1350631552
Control5=IDC_BEGINN_T,edit,1350631552
Control6=IDC_ENDE_D,edit,1350631552
Control7=IDC_ENDE_T,edit,1350631552
Control8=IDC_BEMERKUNG,edit,1216413824
Control9=IDOK,button,1342242817
Control10=IDCANCEL,button,1342242816
Control11=IDC_STATIC,static,1342308352
Control12=IDC_BEM_TEXT,static,1208090624
Control13=IDC_STATIC,static,1342308352
Control14=IDC_STATIC,static,1342308352
Control15=IDC_STATIC,static,1208090624
Control16=IDC_STATIC,static,1208090624
Control17=IDC_DISP,edit,1350631552
Control18=IDC_DISP_TEXT,static,1342308352
Control19=IDC_FIDS_REM_LIST,button,1208025088
Control20=IDC_ACT_START_DATE,edit,1216413824
Control21=IDC_ACT_START_TIME,edit,1216413824
Control22=IDC_ACT_END_DATE,edit,1216413824
Control23=IDC_ACT_END_TIME,edit,1216413824
Control24=IDC_STATIC,static,1208090624
Control25=IDC_STATIC,static,1208090624
Control26=IDC_SCROLLBAR1,scrollbar,1073741825

[DLG:IDD_CCI_DEMANDDETAIL (German (Germany))]
Type=1
Class=?
ControlCount=5
Control1=IDC_STATIC_RULE,button,1342177287
Control2=IDC_SCHLIEBEN,button,1342242816
Control3=IDC_STATIC_RULENAME,static,1342308352
Control4=IDC_EDIT_RULENAME,edit,1350633600
Control5=IDC_STATIC_FLIGHTS,button,1342177287

[DLG:IDD_ROTATION_HAI (German (Germany))]
Type=1
Class=?
ControlCount=5
Control1=IDC_UPDATE,button,1073807360
Control2=IDC_INSERT,button,1073807360
Control3=IDC_DELETE,button,1073807360
Control4=IDC_CLOSE,button,1342242816
Control5=IDC_GMBORDER,static,1476526080

[DLG:IDD_VIATABLE (German (Germany))]
Type=1
Class=?
ControlCount=0

[DLG:IDD_SEASON (German (Germany))]
Type=1
Class=?
ControlCount=205
Control1=IDC_PVOM,edit,1350631552
Control2=IDC_PBIS,edit,1350631552
Control3=IDC_FREQ,edit,1350631552
Control4=IDC_ACT3,edit,1350631560
Control5=IDC_ACT5,edit,1350631560
Control6=IDC_MING,edit,1350631552
Control7=IDC_AALC3,edit,1350631560
Control8=IDC_AFLTN,edit,1350631552
Control9=IDC_AFLNS,edit,1350631552
Control10=IDC_ASHOWJFNO,button,1342242816
Control11=IDC_ADAYS,edit,1350631552
Control12=IDC_ADALY,button,1342242816
Control13=IDC_ACXX,button,1342242819
Control14=IDC_ANOOP,button,1342242819
Control15=IDC_ATTYP,edit,1350631560
Control16=IDC_ASTYP,edit,1350631560
Control17=IDC_ASTEV,edit,1350631560
Control18=IDC_APSTA,edit,1350631560
Control19=IDC_APABS,edit,1350631552
Control20=IDC_APAES,edit,1350631552
Control21=IDC_AGTA1,edit,1350631560
Control22=IDC_ATGA1,edit,1350631560
Control23=IDC_AGA1B,edit,1350631552
Control24=IDC_AGA1E,edit,1350631552
Control25=IDC_AGTA2,edit,1350631560
Control26=IDC_ATGA2,edit,1350631560
Control27=IDC_AGA2B,edit,1350631552
Control28=IDC_AGA2E,edit,1350631552
Control29=IDC_ABLT1,edit,1350631560
Control30=IDC_ATMB1,edit,1350631560
Control31=IDC_AB1BS,edit,1350631552
Control32=IDC_AB1ES,edit,1350631552
Control33=IDC_ABLT2,edit,1350631560
Control34=IDC_ATMB2,edit,1350631560
Control35=IDC_AB2BS,edit,1350631552
Control36=IDC_AB2ES,edit,1350631552
Control37=IDC_AEXT1,edit,1350631560
Control38=IDC_ATET1,edit,1350631560
Control39=IDC_AEXT2,edit,1350631560
Control40=IDC_ATET2,edit,1350631560
Control41=IDC_ASTATUS,button,1342373897
Control42=IDC_ASTATUS2,button,1342242825
Control43=IDC_ASTATUS3,button,1342242825
Control44=IDC_AREM1,edit,1350631492
Control45=IDC_DALC3,edit,1350631560
Control46=IDC_DFLTN,edit,1350631552
Control47=IDC_DFLNS,edit,1350631552
Control48=IDC_DSHOWJFNO,button,1342242816
Control49=IDC_DDAYS,edit,1350631552
Control50=IDC_DDALY,button,1342242816
Control51=IDC_DCXX,button,1342242819
Control52=IDC_DNOOP,button,1342242819
Control53=IDC_DTTYP,edit,1350631560
Control54=IDC_DSTYP,edit,1350631560
Control55=IDC_DSTEV,edit,1350631560
Control56=IDC_DPSTD,edit,1350631560
Control57=IDC_DPDBS,edit,1350631552
Control58=IDC_DPDES,edit,1350631552
Control59=IDC_DGTD1,edit,1350631560
Control60=IDC_DTGD1,edit,1350631560
Control61=IDC_DGD1B,edit,1350631552
Control62=IDC_DGD1E,edit,1350631552
Control63=IDC_DGTD2,edit,1350631560
Control64=IDC_DTGD2,edit,1350631560
Control65=IDC_DGD2B,edit,1350631552
Control66=IDC_DGD2E,edit,1350631552
Control67=IDC_DWRO1,edit,1350631560
Control68=IDC_DTWR1,edit,1350631560
Control69=IDC_DW1BS,edit,1350631552
Control70=IDC_DW1ES,edit,1350631552
Control71=IDC_DSTATUS,button,1342373897
Control72=IDC_DSTATUS2,button,1342242825
Control73=IDC_DSTATUS3,button,1342242825
Control74=IDC_DREM1,edit,1350631492
Control75=IDOK,button,1342242827
Control76=IDCANCEL,button,1342242816
Control77=IDC_PREVFLIGHT,button,1208025088
Control78=IDC_NEXTFLIGHT,button,1208025088
Control79=IDC_ARESET,button,1208025088
Control80=IDC_DRESET,button,1208025088
Control81=IDC_AJFNO_BORDER,static,1476526080
Control82=IDC_ASTOACALC,button,1207959552
Control83=IDC_AFLUKO,edit,1350568064
Control84=IDC_ACREATED,edit,1350568064
Control85=IDC_ALASTCHANGE,edit,1350568064
Control86=IDC_STATIC,static,1342308352
Control87=IDC_STATIC,static,1342308352
Control88=IDC_STATIC,static,1342308352
Control89=IDC_STATIC,button,1342177287
Control90=IDC_STATIC,static,1342308352
Control91=IDC_STATIC,static,1342308352
Control92=IDC_STATIC,static,1342308352
Control93=IDC_STATIC,static,1342308352
Control94=IDC_SEAS,edit,1350568064
Control95=IDC_STATIC,static,1342308352
Control96=IDC_STATIC,static,1342308352
Control97=IDC_STATIC_ANKUNFT,button,1342177287
Control98=IDC_STATIC_ABFLUG,button,1342177287
Control99=IDC_STATIC,static,1342308352
Control100=IDC_STATIC,static,1342308352
Control101=IDC_STATIC,static,1342308352
Control102=IDC_STATIC,static,1342308352
Control103=IDC_STATIC,static,1342308352
Control104=IDC_STATIC,static,1342308352
Control105=IDC_STATIC,static,1342308352
Control106=IDC_STATIC,static,1342308352
Control107=IDC_STATIC,static,1342308352
Control108=IDC_STATIC,static,1342308352
Control109=IDC_STATIC,static,1342308352
Control110=IDC_STATIC,static,1342308352
Control111=IDC_STATIC,static,1342308352
Control112=IDC_STATIC,static,1342308352
Control113=IDC_STATIC,static,1342308352
Control114=IDC_STATIC,button,1342177287
Control115=IDC_STATIC,button,1342177287
Control116=IDC_DSTODCALC,button,1207959552
Control117=IDC_APSTALIST,button,1342177280
Control118=IDC_AGTA1LIST,button,1342177280
Control119=IDC_ABLT2LIST,button,1342177280
Control120=IDC_AEXT1LIST,button,1342177280
Control121=IDC_AGTA2LIST,button,1342177280
Control122=IDC_ABLT1LIST,button,1342177280
Control123=IDC_AEXT2LIST,button,1342177280
Control124=IDC_DALC3LIST,button,1342177280
Control125=IDC_ATTYPLIST,button,1342177280
Control126=IDC_ASTYPLIST,button,1342177280
Control127=IDC_DTTYPLIST,button,1342177280
Control128=IDC_DSTYPLIST,button,1342177280
Control129=IDC_ACT35LIST,button,1342242816
Control130=IDC_STATIC,static,1342308352
Control131=IDC_AAlc3LIST2,button,1342177280
Control132=IDC_AAlc3LIST3,button,1342177280
Control133=IDC_AAlc3LIST,button,1342242816
Control134=IDC_DJFNO_BORDER,static,1476526080
Control135=IDC_DFLUKO,edit,1350568064
Control136=IDC_DCREATED,edit,1350568064
Control137=IDC_DLASTCHANGE,edit,1350568064
Control138=IDC_STATIC,static,1342308352
Control139=IDC_STATIC,static,1342308352
Control140=IDC_STATIC,static,1342308352
Control141=IDC_DALC3LIST2,button,1342177280
Control142=IDC_DALC3LIST3,button,1342177280
Control143=IDC_DEFAULT,button,1207959552
Control144=IDC_STATIC,static,1342308352
Control145=IDC_STATIC,static,1342308352
Control146=IDC_STATIC,static,1342308352
Control147=IDC_DPSTDLIST,button,1342177280
Control148=IDC_DGTD1LIST,button,1342177280
Control149=IDC_DWRO1LIST,button,1342177280
Control150=IDC_DGTD2LIST,button,1342177280
Control151=IDC_STATIC,static,1342308352
Control152=IDC_DCINSBORDER,static,1476526080
Control153=IDC_DCINSLIST1,button,1342177280
Control154=IDC_DCINSLIST2,button,1342177280
Control155=IDC_DCINSLIST3,button,1342177280
Control156=IDC_DCINSLIST4,button,1342177280
Control157=IDC_STATIC,static,1342308352
Control158=IDC_DCINSMAL4,button,1342177280
Control159=IDC_NOSE,edit,1350631552
Control160=IDC_STATIC_NOSE,static,1342308352
Control161=IDC_DCHT3,edit,1350631560
Control162=IDC_STATIC_DCHT3,static,1342308352
Control163=IDC_ACHT3,edit,1350631560
Control164=IDC_STATIC_ACHT3,static,1342308352
Control165=IDC_DREM2,edit,1350631492
Control166=IDC_AREM2,edit,1350631492
Control167=IDC_AORG3,edit,1350631560
Control168=IDC_ASTOD,edit,1350631552
Control169=IDC_ASTOA,edit,1350631552
Control170=IDC_AETAI,edit,1350631552
Control171=IDC_DSTOD,edit,1350631552
Control172=IDC_DETDI,edit,1350631552
Control173=IDC_DDES3,edit,1350631560
Control174=IDC_DSTOA,edit,1350631552
Control175=IDC_STATIC,static,1342308352
Control176=IDC_STATIC,static,1342308352
Control177=IDC_ADES3,edit,1350568064
Control178=IDC_STATIC,static,1342308352
Control179=IDC_STATIC,static,1342308352
Control180=IDC_DORG3,edit,1350568064
Control181=IDC_AORG3LIST,button,1342177280
Control182=IDC_DDES3LIST,button,1342177280
Control183=IDC_AORG4,edit,1350631560
Control184=IDC_STATIC,static,1342308352
Control185=IDC_STATIC,static,1342308352
Control186=IDC_STATIC,static,1342308352
Control187=IDC_STATIC,static,1342308352
Control188=IDC_STATIC,static,1342308352
Control189=IDC_DDES4,edit,1350631560
Control190=IDC_STATIC,static,1342308352
Control191=IDC_STATIC,static,1342308352
Control192=IDC_STATIC,static,1342308352
Control193=IDC_STATIC,static,1342308352
Control194=IDC_STATIC,static,1342308352
Control195=IDC_AREMP,combobox,1344339971
Control196=IDC_DREMP,combobox,1344339971
Control197=IDC_STATIC,static,1342308352
Control198=IDC_STATIC,static,1342308352
Control199=IDC_AFLTI,edit,1350631560
Control200=IDC_DFLTI,edit,1350631560
Control201=IDC_STATIC,static,1342308352
Control202=IDC_DBAZ1,edit,1350631552
Control203=IDC_DBAZ4,edit,1350631552
Control204=IDC_AGENTS,button,1342242816
Control205=IDC_DAILY_MASK,button,1208025099

[MNU:IDR_MAINFRAME (German (Germany))]
Type=1
Class=?
Command1=ID_VIEW
Command2=ID_RULES
Command3=ID_BDPSUIF
Command4=ID_PRINT
Command5=ID_APP_EXIT
Command6=ID_NOW
Command7=ID_DELETE
Command8=ID_ALLOCATE
Command9=ID_CREATE_DEMANDS
Command10=ID_CREATE_COMMON
Command11=ID_REPORT
Command12=ID_REPORT_SEASON
Command13=ID_CONFLICTS
Command14=ID_FLIGHT_CONFLICTS
Command15=ID_DEMAND
Command16=ID_WO_DEMAND
Command17=ID_SYMBOLTEXT
Command18=ID_HELP_FINDER
Command19=ID_APP_ABOUT
CommandCount=19

[TB:IDR_BLANK_BAR (German (Germany))]
Type=1
Class=?
Command1=ID_BB_BLANK
CommandCount=1

[TB:IDR_LEFT_BAR (German (Germany))]
Type=1
Class=?
Command1=LEFT
CommandCount=1

[TB:IDR_MAINFRAME1 (German (Germany))]
Type=1
Class=?
Command1=ID_VIEW
CommandCount=1

[TB:IDR_MAINFRAME2 (German (Germany))]
Type=1
Class=?
Command1=ID_NOW
Command2=ID_DELETE
Command3=ID_ALLOCATE
Command4=ID_WO_DEMAND
Command5=ID_DEMAND
Command6=ID_FLIGHT_CONFLICTS
Command7=ID_CONFLICTS
Command8=ID_BDPSUIF
Command9=ID_RULES
Command10=ID_PRINT
Command11=ID_APP_EXIT
CommandCount=11

[TB:IDR_RIGHT_BAR (German (Germany))]
Type=1
Class=?
Command1=RIGHT
CommandCount=1

[TB:IDR_FOPEN (German (Germany))]
Type=1
Class=?
Command1=ID_FILE_OPEN
CommandCount=1

