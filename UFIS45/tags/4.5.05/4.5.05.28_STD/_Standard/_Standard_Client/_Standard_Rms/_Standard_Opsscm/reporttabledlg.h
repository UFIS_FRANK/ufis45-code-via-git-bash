#ifndef AFX_REPORTTABLEDLG_H__CDB257E5_8BF5_11D1_8127_0000B43C4B01__INCLUDED_
#define AFX_REPORTTABLEDLG_H__CDB257E5_8BF5_11D1_8127_0000B43C4B01__INCLUDED_

// ReportTableDlg.h : Header-Datei
//
//#include "CCSTable.h" 
#include <ReportTableViewer.h>


/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportTableDlg 

class CReportTableDlg : public CDialog
{
// Konstruktion
public:
	CReportTableDlg(CWnd* pParent, const CTime &ropMinDate, const CTime &ropMaxDate);
	~CReportTableDlg();

// Dialogfelddaten
	//{{AFX_DATA(CReportTableDlg)
	enum { IDD = IDD_REPORTTABLE };
	//CButton	m_CB_Beenden;
	CButton	m_RB_Printer;
	CButton	m_RB_File;
	//}}AFX_DATA
	CString omHeadline;	//Dialogbox-Überschrift


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CReportTableDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

public:

	int imTopSpace;
	
	ReportTableViewer omReportViewer;

	CGXGridWnd *pomReportGXGrid;

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CReportTableDlg)
	virtual BOOL OnInitDialog();
 	afx_msg void OnDrucken();
 	afx_msg void OnBeenden();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	const CTime &romMinDate;
	const CTime &romMaxDate;

	const int imDlgBorder;
private:
  	//CCSPtrArray<ROTATIONDLGFLIGHTDATA> *pomData;
	//RotationDlgCedaFlightData* pomRotDlgData;
	bool bmIsCreated;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_REPORTTABLEDLG_H__CDB257E5_8BF5_11D1_8127_0000B43C4B01__INCLUDED_
