#if !defined(AFX_CCAEXPANDDLG_H__75021EC1_3BE9_11D2_859B_0000C04D916B__INCLUDED_)
#define AFX_CCAEXPANDDLG_H__75021EC1_3BE9_11D2_859B_0000C04D916B__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// CCAExpandDlg.h : header file
//

#include <CCSEdit.h>
/////////////////////////////////////////////////////////////////////////////
// CCAExpandDlg dialog

class CCAExpandDlg : public CDialog
{
// Construction
public:
	CCAExpandDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCAExpandDlg)
	enum { IDD = IDD_CCAEXPAND };
	CButton	m_CB_AllVT;
	CProgressCtrl	m_CP_Progess;
	CCSEdit	m_From;
	CCSEdit	m_To;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCAExpandDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCAExpandDlg)
	afx_msg void OnOnly();
	afx_msg void OnAll();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CCAEXPANDDLG_H__75021EC1_3BE9_11D2_859B_0000C04D916B__INCLUDED_)
