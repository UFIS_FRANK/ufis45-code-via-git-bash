# Microsoft Developer Studio Project File - Name="OPSSCM" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=OPSSCM - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "OPSSCM.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "OPSSCM.mak" CFG="OPSSCM - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "OPSSCM - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "OPSSCM - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName "OPSS-CM"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "OPSSCM - Win32 Release"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "c:\Ufis_Bin\Release"
# PROP Intermediate_Dir "c:\Ufis_Intermediate\Opsscm\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I ".\\" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 C:\Ufis_Bin\release\ufis32.lib C:\Ufis_Bin\ClassLib\release\ccsclass.lib Wsock32.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "OPSSCM - Win32 Debug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "c:\Ufis_Bin\Debug"
# PROP Intermediate_Dir "c:\Ufis_Intermediate\Opsscm\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG _ENGLISH" /d "_ENGLISH"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 C:\Ufis_Bin\debug\ufis32.lib C:\Ufis_Bin\ClassLib\debug\ccsclass.lib Wsock32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "OPSSCM - Win32 Release"
# Name "OPSSCM - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\..\_Standard_Share\_Standard_Wrapper\aatlogin.cpp
# End Source File
# Begin Source File

SOURCE=.\AllocateCcaParameter.cpp
# End Source File
# Begin Source File

SOURCE=.\AwBasicDataDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\BasePropertySheet.CPP
# End Source File
# Begin Source File

SOURCE=.\BasicData.cpp
# End Source File
# Begin Source File

SOURCE=.\CcaCedaFlightConfData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CcaCedaFlightData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CCAChangeTimes.cpp
# End Source File
# Begin Source File

SOURCE=.\CcaChart.CPP
# End Source File
# Begin Source File

SOURCE=.\CcaCommonDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CcaDiaPropertySheet.CPP
# End Source File
# Begin Source File

SOURCE=.\CcaDiaViewer.CPP
# End Source File
# Begin Source File

SOURCE=.\CCAExpandDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CcaGantt.CPP
# End Source File
# Begin Source File

SOURCE=.\CciDemandDetailDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSGlobl.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSPrint.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaCfgData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaCflData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaDemandData.cpp
# End Source File
# Begin Source File

SOURCE=.\CedaDiaCcaData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaInitModuData.Cpp
# End Source File
# Begin Source File

SOURCE=.\CedaRloData.cpp
# End Source File
# Begin Source File

SOURCE=.\CicConfTableDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CicConfTableViewer.Cpp
# End Source File
# Begin Source File

SOURCE=.\CicCreateDemandsDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CicDemandTableDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CicDemandTableViewer.Cpp
# End Source File
# Begin Source File

SOURCE=.\CicFConfTableDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CicFConfTableViewer.Cpp
# End Source File
# Begin Source File

SOURCE=.\CicNoDemandTableDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CicNoDemandTableViewer.Cpp
# End Source File
# Begin Source File

SOURCE=.\CVIEWER.CPP
# End Source File
# Begin Source File

SOURCE=.\DailyCcaPrintDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DataSet.cpp
# End Source File
# Begin Source File

SOURCE=.\DelelteCounterDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\InitialLoadDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\List.cpp
# End Source File
# Begin Source File

SOURCE=.\LoginDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\OPSSCM.cpp
# End Source File
# Begin Source File

SOURCE=.\hlp\OPSSCM.hpj
# PROP Ignore_Default_Tool 1
# End Source File
# Begin Source File

SOURCE=.\OPSSCM.rc
# End Source File
# Begin Source File

SOURCE=.\PrivList.cpp
# End Source File
# Begin Source File

SOURCE=.\PSGeometrie.cpp
# End Source File
# Begin Source File

SOURCE=.\PSSearchFlightPage.cpp
# End Source File
# Begin Source File

SOURCE=.\RegisterDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportSeasonTableDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportSeasonTableViewer.Cpp
# End Source File
# Begin Source File

SOURCE=.\ReportTableDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ReportTableViewer.Cpp
# End Source File
# Begin Source File

SOURCE=.\RotationHAIDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ScrollToolBar.cpp
# End Source File
# Begin Source File

SOURCE=.\SeasonDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SeasonDlgCedaFlightData.Cpp
# End Source File
# Begin Source File

SOURCE=.\Splash.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\Timer.cpp
# End Source File
# Begin Source File

SOURCE=.\UniEingabe.cpp
# End Source File
# Begin Source File

SOURCE=.\Utils.cpp
# End Source File
# Begin Source File

SOURCE=.\ViaTableCtrl.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\..\_Standard_Share\_Standard_Wrapper\aatlogin.h
# End Source File
# Begin Source File

SOURCE=.\AllocateCcaParameter.h
# End Source File
# Begin Source File

SOURCE=.\Ansicht.h
# End Source File
# Begin Source File

SOURCE=.\AwBasicDataDlg.h
# End Source File
# Begin Source File

SOURCE=.\BasePropertySheet.H
# End Source File
# Begin Source File

SOURCE=.\BasicData.h
# End Source File
# Begin Source File

SOURCE=.\CcaCedaFlightConfData.H
# End Source File
# Begin Source File

SOURCE=.\CcaCedaFlightData.H
# End Source File
# Begin Source File

SOURCE=.\CCAChangeTimes.h
# End Source File
# Begin Source File

SOURCE=.\CcaChart.H
# End Source File
# Begin Source File

SOURCE=.\CcaCommonDlg.h
# End Source File
# Begin Source File

SOURCE=.\CcaDiaPropertySheet.H
# End Source File
# Begin Source File

SOURCE=.\CcaDiaViewer.H
# End Source File
# Begin Source File

SOURCE=.\CCAExpandDlg.h
# End Source File
# Begin Source File

SOURCE=.\CcaGantt.H
# End Source File
# Begin Source File

SOURCE=.\CciDemandDetailDlg.h
# End Source File
# Begin Source File

SOURCE=.\CCSGLOBL.H
# End Source File
# Begin Source File

SOURCE=.\CCSPrint.h
# End Source File
# Begin Source File

SOURCE=.\CedaCfgData.h
# End Source File
# Begin Source File

SOURCE=.\CedaCflData.h
# End Source File
# Begin Source File

SOURCE=.\CedaDemandData.h
# End Source File
# Begin Source File

SOURCE=.\CedaDiaCcaData.h
# End Source File
# Begin Source File

SOURCE=.\CedaInitModuData.h
# End Source File
# Begin Source File

SOURCE=.\CedaRloData.h
# End Source File
# Begin Source File

SOURCE=.\CicConfTableDlg.h
# End Source File
# Begin Source File

SOURCE=.\CicConfTableViewer.H
# End Source File
# Begin Source File

SOURCE=.\CicCreateDemandsDlg.h
# End Source File
# Begin Source File

SOURCE=.\CicDemandTableDlg.h
# End Source File
# Begin Source File

SOURCE=.\CicDemandTableViewer.H
# End Source File
# Begin Source File

SOURCE=.\CicFConfTableDlg.h
# End Source File
# Begin Source File

SOURCE=.\CicFConfTableViewer.H
# End Source File
# Begin Source File

SOURCE=.\CicNoDemandTableDlg.h
# End Source File
# Begin Source File

SOURCE=.\CicNoDemandTableViewer.H
# End Source File
# Begin Source File

SOURCE=.\CVIEWER.H
# End Source File
# Begin Source File

SOURCE=.\DailyCcaPrintDlg.h
# End Source File
# Begin Source File

SOURCE=.\DATASET.H
# End Source File
# Begin Source File

SOURCE=.\DelelteCounterDlg.h
# End Source File
# Begin Source File

SOURCE=.\GANTT.H
# End Source File
# Begin Source File

SOURCE=.\InitialLoadDlg.h
# End Source File
# Begin Source File

SOURCE=.\List.h
# End Source File
# Begin Source File

SOURCE=.\LoginDlg.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\OPSSCM.h
# End Source File
# Begin Source File

SOURCE=.\PrivList.h
# End Source File
# Begin Source File

SOURCE=.\PSGeometrie.h
# End Source File
# Begin Source File

SOURCE=.\PSSearchFlightPage.h
# End Source File
# Begin Source File

SOURCE=.\RegisterDlg.h
# End Source File
# Begin Source File

SOURCE=.\ReportSeasonTableDlg.h
# End Source File
# Begin Source File

SOURCE=.\ReportSeasonTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\ReportTableDlg.h
# End Source File
# Begin Source File

SOURCE=.\ReportTableViewer.h
# End Source File
# Begin Source File

SOURCE=.\RotationHAIDlg.h
# End Source File
# Begin Source File

SOURCE=.\ScrollToolBar.h
# End Source File
# Begin Source File

SOURCE=.\SeasonDlg.h
# End Source File
# Begin Source File

SOURCE=.\SeasonDlgCedaFlightData.H
# End Source File
# Begin Source File

SOURCE=.\Splash.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\StringConst.h
# End Source File
# Begin Source File

SOURCE=.\Timer.h
# End Source File
# Begin Source File

SOURCE=.\UniEingabe.h
# End Source File
# Begin Source File

SOURCE=.\Utils.h
# End Source File
# Begin Source File

SOURCE=.\ViaTableCtrl.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\blank_ba.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00001.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00002.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00003.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00004.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00005.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00006.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00007.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00008.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00009.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00010.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00011.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00012.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00013.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00014.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00015.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00016.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00017.bmp
# End Source File
# Begin Source File

SOURCE=.\res\faglogo.bmp
# End Source File
# Begin Source File

SOURCE=.\res\hajlogo.bmp
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\left_bar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\LOGIN.BMP
# End Source File
# Begin Source File

SOURCE=.\res\mainfram.bmp
# End Source File
# Begin Source File

SOURCE=.\res\NEXTD.BMP
# End Source File
# Begin Source File

SOURCE=.\res\NEXTU.BMP
# End Source File
# Begin Source File

SOURCE=.\res\nextx.bmp
# End Source File
# Begin Source File

SOURCE=.\res\notavail.bmp
# End Source File
# Begin Source File

SOURCE=.\res\notvalid.bmp
# End Source File
# Begin Source File

SOURCE=.\res\open.bmp
# End Source File
# Begin Source File

SOURCE=.\res\OPSSCM.ico
# End Source File
# Begin Source File

SOURCE=.\res\OPSSCM.rc2
# End Source File
# Begin Source File

SOURCE=.\res\PREVD.BMP
# End Source File
# Begin Source File

SOURCE=.\res\PREVU.BMP
# End Source File
# Begin Source File

SOURCE=.\res\prevx.bmp
# End Source File
# Begin Source File

SOURCE=.\res\right_ba.bmp
# End Source File
# Begin Source File

SOURCE=.\sdi.ico
# End Source File
# Begin Source File

SOURCE=.\Splsh16.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# End Group
# Begin Group "Help Files"

# PROP Default_Filter "cnt;rtf"
# Begin Source File

SOURCE=.\hlp\AfxCore.rtf
# End Source File
# Begin Source File

SOURCE=.\hlp\AppExit.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\Bullet.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\CurArw2.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\CurArw4.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\CurHelp.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\EditCopy.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\EditCut.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\EditPast.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\EditUndo.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\FileNew.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\FileOpen.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\FilePrnt.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\FileSave.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\HlpSBar.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\HlpTBar.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\RecFirst.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\RecLast.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\RecNext.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\RecPrev.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\Scmax.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\ScMenu.bmp
# End Source File
# Begin Source File

SOURCE=.\hlp\Scmin.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
# Section OPSSCM : {72ADFD78-2C39-11D0-9903-00A0C91BC942}
# 	1:10:IDB_SPLASH:1689
# 	2:21:SplashScreenInsertKey:4.0
# End Section
