// CCAExpandDlg.cpp : implementation file
//

#include <stdafx.h>
#include <opsscm.h>
#include <CCAExpandDlg.h>
#include <CCSGlobl.h>
#include <CcaCedaFlightData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCAExpandDlg dialog

//
CCAExpandDlg::CCAExpandDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCAExpandDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCAExpandDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}
BOOL CCAExpandDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_From.SetTypeToDate(true);
	m_To.SetTypeToDate(true);
	return TRUE;
}


void CCAExpandDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCAExpandDlg)
	DDX_Control(pDX, IDC_ALLVT, m_CB_AllVT);
	DDX_Control(pDX, IDC_PROG, m_CP_Progess);
	DDX_Control(pDX, IDC_DATEFROM, m_From);
	DDX_Control(pDX, IDC_DATETO, m_To);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCAExpandDlg, CDialog)
	//{{AFX_MSG_MAP(CCAExpandDlg)
	ON_BN_CLICKED(IDC_ONLY, OnOnly)
	ON_BN_CLICKED(IDC_ALL, OnAll)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCAExpandDlg message handlers

void CCAExpandDlg::OnOnly() 
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	CUIntArray olUrnos;
	CUIntArray olCommonUrnos;
	CCSPtrArray<DIACCADATA> olData;

	CTime olFrom(1998,10,25,0,0,0);
	CTime olTo(1999,03,27,23,59,59);


	if(!m_From.GetStatus())
	{
		MessageBox(GetString(IDS_NO_DATE), GetString(IDS_STRING908), MB_OK);
		return;
	}
	if(!m_To.GetStatus())
	{
		MessageBox(GetString(IDS_NO_DATE), GetString(IDS_STRING908), MB_OK);
		return;
	}
	CString olDatFrom, olDatTo;
	m_From.GetWindowText(olDatFrom);
	m_To.GetWindowText(olDatTo);
	olFrom = DateStringToDate(olDatFrom);
	olTo = DateStringToDate(olDatTo);


	if(olFrom == TIMENULL || olTo == TIMENULL)
	{
		MessageBox( GetString(IDS_STRING1657) ,  GetString(ST_FEHLER));
		return;
	}



	for(int i = ogCcaDiaFlightData.omData.GetSize() - 1; i >= 0; i--)
	{
		olData.RemoveAll();
		if(ogCcaDiaFlightData.omCcaData.GetCcaArray(ogCcaDiaFlightData.omData[i].Urno, olData))
		{

			for(int j = olData.GetSize() - 1; j >= 0; j--)
			{

				if(olData[j].IsSelected)
				{
					olUrnos.Add(ogCcaDiaFlightData.omData[i].Urno);
					break;
				}
			}
		}
	}


	//CTime olTime = CTime::GetCurrentTime();
	//TRACE("\nBEGIN %s", olTime.Format("%H:%M:%S"));


	ogCcaDiaFlightData.omCcaData.GetCommonUrnos(olCommonUrnos, true);


	int ilCount = olUrnos.GetSize();
	int ilCommonCount = olCommonUrnos.GetSize();

	m_CP_Progess.SetRange(0,ilCount + 1 + ilCommonCount);
	m_CP_Progess.SetStep(1);

	CTime olRefDate = ogCcaDiaFlightData.omFrom;


	bool blAll = false;
	
	if(m_CB_AllVT.GetCheck())
		blAll = true;


	for( i = 0; i < ilCommonCount; i++)
	{
		m_CP_Progess.OffsetPos(1);
		ogCcaDiaFlightData.omCcaData.Expand(olCommonUrnos[i], olFrom, olTo,olRefDate, blAll);
	}

/*
	for( i = 0; i < ilCount; i++)
	{
		m_CP_Progess.OffsetPos(1);
		ogCcaDiaFlightData.Expand(olUrnos[i], olFrom, olTo);
	}
*/
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	m_CP_Progess.SetPos(0);
	
	//olTime = CTime::GetCurrentTime();
	//TRACE("\nEND %s", olTime.Format("%H:%M:%S"));






	
}

void CCAExpandDlg::OnAll() 
{
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));


	CTime olTime = CTime::GetCurrentTime();
	TRACE("\nBEGIN %s", olTime.Format("%H:%M:%S"));


	CUIntArray olCommonUrnos;


	CTime olFrom(1998,10,25,0,0,0);
	CTime olTo(1999,03,27,23,59,59);


	if(!m_From.GetStatus())
	{
		MessageBox(GetString(IDS_NO_DATE), GetString(IDS_STRING908), MB_OK);
		return;
	}
	if(!m_To.GetStatus())
	{
		MessageBox(GetString(IDS_NO_DATE), GetString(IDS_STRING908), MB_OK);
		return;
	}
	CString olDatFrom, olDatTo;
	m_From.GetWindowText(olDatFrom);
	m_To.GetWindowText(olDatTo);
	olFrom = DateStringToDate(olDatFrom);
	olTo = DateStringToDate(olDatTo);
	
	if(olFrom == TIMENULL || olTo == TIMENULL)
	{
		MessageBox( GetString(IDS_STRING1657) ,  GetString(ST_FEHLER));
		return;
	}
	
	
	ogCcaDiaFlightData.omCcaData.GetCommonUrnos(olCommonUrnos, false);


	int ilCount = ogCcaDiaFlightData.omData.GetSize();
	int ilCommonCount = olCommonUrnos.GetSize();

	m_CP_Progess.SetRange(0,ilCount + 1 + ilCommonCount);
	m_CP_Progess.SetStep(1);


	bool blAll = false;
	
	if(m_CB_AllVT.GetCheck())
		blAll = true;

	CTime olRefDate = ogCcaDiaFlightData.omFrom;

	for(int  i = 0; i < ilCommonCount; i++)
	{
		m_CP_Progess.OffsetPos(1);
		ogCcaDiaFlightData.omCcaData.Expand(olCommonUrnos[i], olFrom, olTo, olRefDate, blAll);
	}


/*	for( i = 0; i < ilCount; i++)
	{
		m_CP_Progess.OffsetPos(1);
		ogCcaDiaFlightData.Expand(ogCcaDiaFlightData.omData[i].Urno, olFrom, olTo);
	}
*/
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	m_CP_Progess.SetPos(0);
	
	olTime = CTime::GetCurrentTime();
	TRACE("\nEND %s", olTime.Format("%H:%M:%S"));


}
