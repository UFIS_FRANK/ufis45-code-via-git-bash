#ifndef _DATASET_H_
#define _DATASET_H_


#include <ccsglobl.h>
#include <BasicData.h>
#include <RecordSet.h>
#include <CcaCedaFlightData.h>
#include <CcaDiaViewer.h>


#define REGEL_T			21
#define REGEL_A			12 //Annahme
#define REGEL_B			12 //Bereitstellung

#define REGEL_T_GHP			31
#define REGEL_A_GHP			17 //Annahme
#define REGEL_B_GHP			17 //Bereitstellung



#define EQUAL				0
#define FIND_IN_FLIGHT		1
#define FIND_IN_RULE		2
#define FIND_IN_FLIGHT_SPEZ	3

struct PREMIS_ORDER
{
	RecordSet Pgr;
	char	Prio[REGEL_T+1];
	int		UserPrio;
	PREMIS_ORDER(void)
	{
		UserPrio = 0;
		strcpy(Prio, "0000000000000000000");
	}
};


struct RULES_ENTRY
{
	char Prio;			// M or S
	int  Weight;
	char Type;          // E-->Single G-->Group
	CString Urno;
	CString Pnam;
};


/////////////////////////////////////////////////////////////////////////////
// DataSet class

class DataSet
{
// Job creation routine
public:
	DataSet();
	~DataSet();

	
	bool DeAssignCca(long lpCcaUrno);
	bool DeAssignKKeyCca(long lpCcaKKey);


	CStringArray omCcaConflictTextList;
	
	void GetConflictBrushByCcaStatus(DIACCADATA *prpCca, CcaDIA_BARDATA *prlBar);
	
	int GetFistCcaConflict(DIACCADATA *prpCca);
	


	bool CheckKompCCaForOverlapping(CString opCkic);


	void InitCcaConflictStrings()
	{
		omCcaConflictTextList.RemoveAll();
		omCcaConflictTextList.Add(GetString(IDS_STRING1466));
		omCcaConflictTextList.Add(CString(" "));
		omCcaConflictTextList.Add(GetString(IDS_STRING1467));
		omCcaConflictTextList.Add(CString(" "));
		omCcaConflictTextList.Add(CString(" "));
		omCcaConflictTextList.Add(CString(" "));
		omCcaConflictTextList.Add(CString(" "));
		omCcaConflictTextList.Add(CString(" "));
		omCcaConflictTextList.Add(CString(" "));
		omCcaConflictTextList.Add(CString(" "));
		omCcaConflictTextList.Add(CString(" "));
		omCcaConflictTextList.Add(CString(" "));
		omCcaConflictTextList.Add(CString(" "));
		omCcaConflictTextList.Add(CString(" "));
	}


	bool CheckCCaForOverlapping(CString opCkic);


	CString CreateFlno(CString opAlc, CString opFltn, CString opFlns);




};

#endif	// _DATASET_H_
