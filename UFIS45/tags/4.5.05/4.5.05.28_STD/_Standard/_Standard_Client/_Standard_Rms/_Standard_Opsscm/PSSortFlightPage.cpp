// SortFlightPage.cpp : implementation file
//

#include <stdafx.h>
#include <PSSortFlightPage.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Change this key identifiers for name used in the registry database
#define NUMBER_OF_SORTKEYS	7
static CString ogSortKeys[NUMBER_OF_SORTKEYS] =
	{ "Flugnummer", "A/C-Type", "Origin", "Destination", "Ankunftzeit", "Abflugzeit", "none" };
#define NOSORT	(NUMBER_OF_SORTKEYS - 1)	// must be "none"

/////////////////////////////////////////////////////////////////////////////
// CSortFlightPage property page

IMPLEMENT_DYNCREATE(CSortFlightPage, CPropertyPage)

CSortFlightPage::CSortFlightPage() : CPropertyPage(CSortFlightPage::IDD)
{
	//{{AFX_DATA_INIT(CSortFlightPage)
		// NOTE: the ClassWizard will add member initialization here
	m_Group = FALSE;
	m_SortOrder0 = -1;
	m_SortOrder1 = -1;
	m_SortOrder2 = -1;
	//}}AFX_DATA_INIT
	//pomPageBuffer = (CPageBuffer *)pomDataBuffer;
}

CSortFlightPage::~CSortFlightPage()
{
	//pomPageBuffer = NULL;
}

void CSortFlightPage::DoDataExchange(CDataExchange* pDX)
{
	//{{AFX_DATA_MAP(CSortFlightPage)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
	// Extended data exchange -- for member variables with Value type
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		omSortOrders.SetSize(3);
		m_SortOrder0 = GetSortOrder(omSortOrders[0]);
		m_SortOrder1 = GetSortOrder(omSortOrders[1]);
		m_SortOrder2 = GetSortOrder(omSortOrders[2]);
/*		if(omSortOrders.GetSize() == 2)
		{
			m_SortOrder0 = GetSortOrder(omSortOrders[0]);
			m_SortOrder1 = GetSortOrder(omSortOrders[1]);
			m_SortOrder2 = 6;
		}
		if(omSortOrders.GetSize() == 1)
		{
			m_SortOrder0 = GetSortOrder(omSortOrders[0]);
			m_SortOrder1 = 6;
			m_SortOrder2 = 6;
		}
		else
		{
			m_SortOrder0 = 6;
			m_SortOrder1 = 6;
			m_SortOrder2 = 6;
		}
*/
	}

	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FlightTableSortPage)
	DDX_Check(pDX, IDC_CHECK1, m_Group);
	DDX_Radio(pDX, IDC_RADIO1, m_SortOrder0);
	DDX_Radio(pDX, IDC_RADIO2, m_SortOrder1);
	DDX_Radio(pDX, IDC_RADIO3, m_SortOrder2);
	//}}AFX_DATA_MAP

	// Extended data validation
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		omSortOrders.RemoveAll();
		omSortOrders.Add(GetSortKey(m_SortOrder0));
		omSortOrders.Add(GetSortKey(m_SortOrder1));
		omSortOrders.Add(GetSortKey(m_SortOrder2));
	}
}


BEGIN_MESSAGE_MAP(CSortFlightPage, CPropertyPage)
	//{{AFX_MSG_MAP(CSortFlightPage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSortFlightPage message handlers

BOOL CSortFlightPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	// TODO: Add extra initialization here
	//pomPageBuffer = (CPageBuffer *)pomDataBuffer;
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CSortFlightPage::OnCommand(WPARAM wParam, LPARAM lParam) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	BOOL bResult = CPropertyPage::OnCommand(wParam, lParam);

	if (HIWORD(wParam) == BN_CLICKED)
	{
		CancelToClose();
		UpdateData();	// caution: this assume there is always no error

		// Disallow using the same sorting order more than once
		if (m_SortOrder1 == m_SortOrder0)
			omSortOrders[1] = "none";
		if (m_SortOrder2 == m_SortOrder0 || m_SortOrder2 == m_SortOrder1)
			omSortOrders[2] = "none";
		UpdateData(FALSE);
	}
	return bResult;
}

/////////////////////////////////////////////////////////////////////////////
// FlightTableSortPage -- helper routines

int CSortFlightPage::GetSortOrder(const char *pcpSortKey)
{
	for (int i = 0; i < NUMBER_OF_SORTKEYS; i++)
		if (ogSortKeys[i] == pcpSortKey)
			return i;

	// If there is no sorting order matched, assume "none" for no sorting
	return NOSORT;
}

CString CSortFlightPage::GetSortKey(int ipSortOrder)
{
	if (0 <= ipSortOrder && ipSortOrder <= NUMBER_OF_SORTKEYS-1)
		return ogSortKeys[ipSortOrder];

	return "";	// invalid sort order
}

#define CTABLE_NAME   "Flug"
#define CSHOW_GROUP   "Show Group"
CString CSortFlightPage::GetPrivList()
{
	//char aclInitFieldList[] = "APPL,SUBD,FUNC,FUAL,TYPE,STAT";
	CString olInitPageData = 
		CString("SortFlightPage,Sortby_Flugnummer,Flugnummer,B,1,")+
		CString("SortFlightPage,Sortby_ACType,ACType,B,1,")+
		CString("SortFlightPage,Sortby_Origin,Origin,B,1,")+
		CString("SortFlightPage,Sortby_Destination,Destination,B,1,")+
		CString("SortFlightPage,Sortby_Ankunft,Ankunft,B,1,")+
		CString("SortFlightPage,Sortby_Abflug,Abflug,B,1,");
return(olInitPageData);
}