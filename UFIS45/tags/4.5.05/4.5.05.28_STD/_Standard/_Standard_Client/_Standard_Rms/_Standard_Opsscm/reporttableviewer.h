#if !defined(AFX_REPORTTABLEVIEWER_H__657FF802_4EDA_11D2_8369_0000C03B916B__INCLUDED_)
#define AFX_REPORTTABLEVIEWER_H__657FF802_4EDA_11D2_8369_0000C03B916B__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ReportTableViewer.h : header file
//

//#pragma warning(disable: 4786)

#include <stdafx.h>
//#include "CCSTable.h"
#include <CViewer.h>
#include <CCSPrint.h>
#include <CcaCedaFlightData.H>

#define REPORTTABLE_COLCOUNT 9

struct REPORTTABLE_LINEDATA
{
	long	CcaUrno; 	// Eindeutige Datensatz-Nr.
	long	Flnu;		// Urno des flights
	bool	BlkCom;		// Blocked oder common?

	CString	Alc ;	   // Airline	
	CString	Flno;  // Flightnumber
	CString	Act ;      // Aircraft-type
	CString	ViaOut ;   // via
	CString	Des ;	   // Destination
	CTime	Stod ;      // Departuretime
	

	int CAnz ;    // Counter count
 	CString Cicf;		// from counter 
	CString Cict;		// to counter
	CTime	Ckbs;
	CTime	Ckes;
 
 	
	REPORTTABLE_LINEDATA()
	{ 
		CcaUrno = 0;
		Flnu = 0;
		BlkCom = false;
		Alc.Empty() ;		
		Flno.Empty() ;
		Act.Empty() ;
		Des.Empty() ;
		ViaOut.Empty() ;

		CAnz = 0;
 		Cicf.Empty();
		Cict.Empty();
	}
};

 



/////////////////////////////////////////////////////////////////////////////
// Befehlsziel ReportTableViewer 


class ReportTableViewer : public CViewer
{
// Constructions
public:
    ReportTableViewer();
    ~ReportTableViewer();

	// Clears the table of the viewer
	void ClearAll();

	// Connects the viewer with a table
    void Attach(CGXGridWnd *popAttachWnd);
	// Change the viewed database 
    void ChangeViewTo(const CTime &ropMinDate, const CTime &ropMaxDate);

  	void DeleteLine(int ipLineno);
 
	// UpdateDisplay: Load data to the display by using "omTable"
 	void UpdateDisplay();

	void PrintTable(void);
	bool PrintExcelFile(const char *popFilePath) const;
	
 	
private:

	// columns width of the table in char
	static int imTableColCharWidths[REPORTTABLE_COLCOUNT];
	int imTableColWidths[REPORTTABLE_COLCOUNT];
	CString omTableHeadlines[REPORTTABLE_COLCOUNT];

 	int CompareLines(const REPORTTABLE_LINEDATA *prpLine1, const REPORTTABLE_LINEDATA *prpLine2) const;
	bool CompressCcaLines(void);


	void DrawHeader();
	// Transfer the data of the database to the intern data structures
	void MakeLines(void);
	// Copy the data from the db-record to the table-record
	bool MakeLineData(REPORTTABLE_LINEDATA &rrpLineData, const DIACCADATA &rrpCca) const;
 	int  CreateLine(const REPORTTABLE_LINEDATA &rrpLine);
	// Fills one row of the table
 	bool InsertTextLine(int ipLineNo, const REPORTTABLE_LINEDATA &rrpLine) const;

	bool PrintTableHeader(CCSPrint &ropPrint);
	//bool PrintTableLine(REPORTTABLE_LINEDATA *prpUrno);
	bool PrintTableLine(CCSPrint &ropPrint, int ipLineNo);

	REPORTTABLE_LINEDATA *ReportTableViewer::SearchLine(const REPORTTABLE_LINEDATA &rrpNewLine);
	// Extract the vias from the given flight
	bool GetVias(CString &ropDestStr, const CCAFLIGHTDATA &rrpFlight) const;
   	void DeleteAll();

	// Timespan
	const CTime *pomMinDate;
	const CTime *pomMaxDate;

 	// Grid
 	CGXGridWnd *pomGXGridWnd;

	// Table Data
	CCSPtrArray<REPORTTABLE_LINEDATA> omLines;
 
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_REPORTTABLEVIEWER_H__657FF802_4EDA_11D2_8369_0000C03B916B__INCLUDED_)
