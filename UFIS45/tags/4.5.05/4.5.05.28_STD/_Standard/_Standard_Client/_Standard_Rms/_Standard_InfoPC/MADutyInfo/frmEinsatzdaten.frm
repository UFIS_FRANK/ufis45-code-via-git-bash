VERSION 5.00
Object = "{A2F31E92-C74F-11D3-A251-00500437F607}#1.0#0"; "UfisCom.ocx"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmEinsatzdaten 
   Caption         =   "Einsatzdaten"
   ClientHeight    =   10080
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   13230
   Icon            =   "frmEinsatzdaten.frx":0000
   LinkTopic       =   "Form2"
   ScaleHeight     =   672
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   882
   StartUpPosition =   1  'CenterOwner
   Tag             =   "170"
   Begin UFISCOMLib.UfisCom UfisCom1 
      Left            =   5760
      Top             =   6705
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   1085
      _StockProps     =   0
   End
   Begin VB.Frame Frame4 
      Height          =   7575
      Left            =   4620
      TabIndex        =   22
      Top             =   240
      Width           =   8280
      Begin VB.TextBox txtMitarbeiter 
         BackColor       =   &H8000000A&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   1140
         Left            =   2340
         MultiLine       =   -1  'True
         TabIndex        =   38
         Top             =   540
         Width           =   5745
      End
      Begin VB.Label lblEmployee 
         Caption         =   "160"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   37
         Tag             =   "160"
         Top             =   525
         Width           =   1665
      End
      Begin VB.Label lblJobType 
         Caption         =   "161"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   285
         TabIndex        =   36
         Tag             =   "161"
         Top             =   1875
         Width           =   1665
      End
      Begin VB.Label lblEinsatztyp 
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   405
         Left            =   2325
         TabIndex        =   35
         Top             =   1845
         Width           =   3645
      End
      Begin VB.Label lblTimeFrom 
         Caption         =   "162"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   285
         TabIndex        =   34
         Tag             =   "162"
         Top             =   2445
         Width           =   1665
      End
      Begin VB.Label lblZeitVon 
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   405
         Left            =   2325
         TabIndex        =   33
         Top             =   2415
         Width           =   2745
      End
      Begin VB.Label lblTimeTo 
         Caption         =   "163"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   285
         TabIndex        =   32
         Tag             =   "163"
         Top             =   3030
         Width           =   1665
      End
      Begin VB.Label lblZeitBis 
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   405
         Left            =   2325
         TabIndex        =   31
         Top             =   2985
         Width           =   2745
      End
      Begin VB.Label lblDepartement 
         Caption         =   "164"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   285
         TabIndex        =   30
         Tag             =   "164"
         Top             =   3555
         Width           =   1665
      End
      Begin VB.Label lblEinheit 
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   405
         Left            =   2325
         TabIndex        =   29
         Top             =   3555
         Width           =   3645
      End
      Begin VB.Label lblFightNo 
         Caption         =   "165"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   285
         TabIndex        =   28
         Tag             =   "165"
         Top             =   4155
         Width           =   1665
      End
      Begin VB.Label lblFlugNr 
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   405
         Left            =   2325
         TabIndex        =   27
         Top             =   4125
         Width           =   1545
      End
      Begin VB.Label lblWaytime 
         Caption         =   "166"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   285
         TabIndex        =   26
         Tag             =   "166"
         Top             =   4725
         Width           =   1665
      End
      Begin VB.Label lblWegeZeit 
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   405
         Left            =   2325
         TabIndex        =   25
         Top             =   4695
         Width           =   1545
      End
      Begin VB.Label lblComment 
         Caption         =   "167"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   285
         TabIndex        =   24
         Tag             =   "167"
         Top             =   5325
         Width           =   1785
      End
      Begin VB.Label lblKommentar 
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   1365
         Left            =   2325
         TabIndex        =   23
         Top             =   5295
         Width           =   5745
      End
   End
   Begin TABLib.TAB TabJOB 
      Height          =   1095
      Left            =   5640
      TabIndex        =   16
      Top             =   6360
      Visible         =   0   'False
      Width           =   1215
      _Version        =   65536
      _ExtentX        =   2143
      _ExtentY        =   1931
      _StockProps     =   64
   End
   Begin VB.Frame Frame1 
      Height          =   5520
      Left            =   300
      TabIndex        =   17
      Top             =   2295
      Width           =   3975
      Begin VB.CommandButton NmbBlock 
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1035
         Index           =   0
         Left            =   240
         TabIndex        =   2
         Top             =   4275
         Width           =   1035
      End
      Begin VB.CommandButton NmbBlock 
         Caption         =   "9"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1035
         Index           =   9
         Left            =   2640
         TabIndex        =   11
         Top             =   660
         Width           =   1035
      End
      Begin VB.CommandButton NmbBlock 
         Caption         =   "8"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1035
         Index           =   8
         Left            =   1440
         TabIndex        =   10
         Top             =   660
         Width           =   1035
      End
      Begin VB.CommandButton NmbBlock 
         Caption         =   "7"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1035
         Index           =   7
         Left            =   240
         TabIndex        =   9
         Top             =   660
         Width           =   1035
      End
      Begin VB.CommandButton NmbBlock 
         Caption         =   "6"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1035
         Index           =   6
         Left            =   2640
         TabIndex        =   8
         Top             =   1860
         Width           =   1035
      End
      Begin VB.CommandButton NmbBlock 
         Caption         =   "5"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1035
         Index           =   5
         Left            =   1440
         TabIndex        =   7
         Top             =   1860
         Width           =   1035
      End
      Begin VB.CommandButton NmbBlock 
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1035
         Index           =   4
         Left            =   240
         TabIndex        =   6
         Top             =   1860
         Width           =   1035
      End
      Begin VB.CommandButton NmbBlock 
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1035
         Index           =   3
         Left            =   2640
         TabIndex        =   5
         Top             =   3060
         Width           =   1035
      End
      Begin VB.CommandButton NmbBlock 
         Caption         =   "2"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1035
         Index           =   2
         Left            =   1440
         TabIndex        =   4
         Top             =   3060
         Width           =   1035
      End
      Begin VB.CommandButton NmbBlock 
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1035
         Index           =   1
         Left            =   240
         TabIndex        =   3
         Top             =   3060
         Width           =   1035
      End
      Begin VB.CommandButton cmdBack 
         Caption         =   "103"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1035
         Left            =   1440
         TabIndex        =   12
         Tag             =   "103"
         Top             =   4275
         Width           =   2235
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         Caption         =   "111"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   180
         TabIndex        =   18
         Tag             =   "111"
         Top             =   180
         Width           =   3645
      End
   End
   Begin VB.Frame Frame2 
      Height          =   1935
      Left            =   300
      TabIndex        =   19
      Top             =   240
      Width           =   3975
      Begin VB.TextBox txtUserName 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         IMEMode         =   3  'DISABLE
         Left            =   240
         PasswordChar    =   "*"
         TabIndex        =   0
         Top             =   600
         Width           =   3405
      End
      Begin VB.CommandButton cmdCE 
         Caption         =   "108"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   585
         Left            =   240
         TabIndex        =   1
         Tag             =   "108"
         Top             =   1200
         Width           =   3435
      End
      Begin VB.Label Label18 
         Caption         =   "110"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   20
         Tag             =   "110"
         Top             =   240
         Width           =   3465
      End
   End
   Begin VB.Frame Frame3 
      Height          =   1575
      Left            =   300
      TabIndex        =   21
      Top             =   8040
      Width           =   12600
      Begin VB.CommandButton cmdEinsatzAnzeigen 
         Caption         =   "105"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   945
         Left            =   240
         TabIndex        =   13
         Tag             =   "105"
         Top             =   360
         Width           =   3615
      End
      Begin VB.CommandButton cmdEinsatzAnnehmen 
         Caption         =   "106"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   945
         Left            =   4475
         TabIndex        =   14
         Tag             =   "106"
         Top             =   360
         Width           =   3615
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "107"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   945
         Left            =   8700
         TabIndex        =   15
         Tag             =   "107"
         Top             =   360
         Width           =   3615
      End
   End
End
Attribute VB_Name = "frmEinsatzdaten"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private blLoaded As Boolean
Public blWegezeit As Boolean

Private Sub Form_Load()
    LoadResStrings Me
    InitValues
    ConnectToCeda UfisCom1, sAppl, sHopo, sTableExt, sServer, sConnectType
    sgUTCOffsetHours = GetUTCOffset

    blWegezeit = False

    If blWegezeit = False Then
        'removing the information concerning the "Wegezeit"
        lblWaytime.Visible = False
        lblWegeZeit.Visible = False
        lblComment.Top = lblWegeZeit.Top
        lblKommentar.Top = lblWegeZeit.Top
    End If
    'txtUserName = "522494"
    'txtUserName = "393717"
    txtUserName.SelStart = Len(txtUserName.Text)
    blLoaded = False
End Sub

Private Sub cmdBack_Click()
    If Len(txtUserName.Text) > 0 Then
        txtUserName.Text = Left(txtUserName.Text, Len(txtUserName.Text) - 1)
    End If
    txtUserName.SelStart = Len(txtUserName.Text)
    txtUserName.SetFocus
End Sub

Private Sub NmbBlock_Click(Index As Integer)
    txtUserName.Text = txtUserName.Text & CStr(Index)
    txtUserName.SelStart = Len(txtUserName.Text)
    txtUserName.SetFocus
End Sub

Private Sub cmdEinsatzAnzeigen_Click()
    Dim blRet As Boolean

    Me.MousePointer = vbArrowHourglass

    blRet = VerifyPENO(txtUserName.Text)
    If blRet = False Then
         ' "Die Personalnummer existiert mehrmals!"
         ' "Die Personalnummer existiert nicht!"
        MsgBox strLastError & vbCrLf & "PENO: " & txtUserName.Text, vbInformation, LoadResString(172)
    End If

    ' loading the data from the JOBTAB
    If blRet = True Then
        blRet = LoadJOBTAB
        If blRet = False Then
            blRet = False
            'MsgBox strLastError & vbCrLf & "Name: " & strFINM & " " & strLANM, vbInformation, "Keine Jobs"
            MsgBox strLastError, vbInformation, LoadResString(173)
            'Keine Jobs f�r den Mitarbeiter!
        End If
    End If

    ' there are data in the JOBTAB. So let's look after the actual job.
    If blRet = True Then
        blRet = FindActualJob
        If blRet = False Then
            'MsgBox strLastError & vbCrLf & "Name: " & strFINM & " " & strLANM, vbInformation, "Keine Eins�tze"
            MsgBox strLastError, vbInformation, LoadResString(174)
            'Kein offener Einsatz gefunden!
        End If
    End If

    ' the last point: fill the fields
    If blRet = True Then
        FindEinsatzTyp
        FindEinheit
        FindFlugNummer
        If blWegezeit = True Then
            FindWegezeit
        End If

        FillTextBoxes
        blLoaded = True
    Else
        DeleteTextBoxes
        blLoaded = False
    End If

    txtUserName.SelStart = Len(txtUserName.Text)
    txtUserName.SetFocus
    Me.MousePointer = vbDefault
End Sub

Private Sub cmdCE_Click()
    DeleteTextBoxes

    txtUserName.Text = ""
    txtUserName.SetFocus
End Sub

Private Sub cmdEinsatzAnnehmen_Click()
    If JobAnnehmen = True Then
        DeleteTextBoxes
        txtUserName.Text = ""
        txtUserName.SetFocus
        blLoaded = False
    End If
End Sub

Private Sub cmdPrint_Click()
    If JobAnnehmen = True Then
        frmPrint.Show vbModeless
        DeleteTextBoxes
        txtUserName.Text = ""
        txtUserName.SetFocus
        blLoaded = False
    End If
End Sub

Private Function VerifyPENO(ByRef rPENO As String) As Boolean
    Dim strTable As String
    Dim strFieldlist As String
    Dim strWhere As String
    Dim llRecords As Long
    Dim tmpStr As String
    Dim i As Integer

    strTable = "STFTAB"
    strFieldlist = "URNO,FINM,LANM"
    strWhere = "WHERE PENO = '" & rPENO & "'"

    UfisCom1.Twe = sHopo + "," + sTableExt + "," + sAppl
    ' Read from ceda and insert the buffer directly into the grid
    If UfisCom1.CallServer("RT", strTable, strFieldlist, "", strWhere, "360") = 0 Then
        llRecords = UfisCom1.GetBufferCount
        If llRecords > 1 Then
            'the PENO is not unique!
            VerifyPENO = False
            strLastError = LoadResString(116) ' "Die Personalnummer existiert mehrmals!"
        Else
            'the PENO exists once
            VerifyPENO = True
            tmpStr = UfisCom1.GetBufferLine(i)
            strPENO = txtUserName.Text
            strURNO = GetItem(tmpStr, 1, ",")
            strFINM = GetItem(tmpStr, 2, ",")
            strLANM = GetItem(tmpStr, 3, ",")
        End If
    Else
        'the PENO does not exist
        VerifyPENO = False
        strLastError = LoadResString(117) ' "Die Personalnummer existiert nicht!"
    End If
End Function

Private Function LoadJOBTAB() As Boolean
    Dim strTable As String
    Dim strFieldlist As String
    Dim strWhere As String
    Dim strHeaderLenStr As String
    Dim ilItemCount As Integer
    Dim llRecords As Long
    Dim i As Long
    Dim tmpStr As String

    Dim strArr() As String
    Dim olTomorrow As Date
    Dim olYesterday As Date

    olTomorrow = Now() + 1 + (sgUTCOffsetHours / 24)
    olYesterday = Now() - 1 + (sgUTCOffsetHours / 24)

    strTable = "JOBTAB"
    strFieldlist = "URNO,ACFR,ACTO,STAT,DETY,UALO,TEXT,UAFT,UAID,UJTY"
    strWhere = "WHERE USTF = '" & strURNO & "' AND (ACFR BETWEEN '" & _
        CStr(Year(olYesterday)) & CStr(Format(Month(olYesterday), "00")) & _
        CStr(Format(Day(olYesterday), "00")) & "120000' AND '" & _
        CStr(Year(olTomorrow)) & CStr(Format(Month(olTomorrow), "00")) & _
        CStr(Format(Day(olTomorrow), "00")) & "120000') AND (UJTY IN (" & sJobTypes & "))"

    TabJOB.ResetContent
    TabJOB.ColumnWidthString = "10,14,14,10,1,5,10,30,10"
    TabJOB.ShowHorzScroller True
    TabJOB.EnableHeaderSizing True

    'set headertext of grid
    strArr = Split(strFieldlist, ",")
    ilItemCount = UBound(strArr)
    For i = 0 To ilItemCount
        If i + 1 > ilItemCount Then
            strHeaderLenStr = strHeaderLenStr & "120"
        Else
            strHeaderLenStr = strHeaderLenStr & "120,"
        End If
    Next i
    TabJOB.HeaderLengthString = strHeaderLenStr
    TabJOB.HeaderString = strFieldlist

    ' Read from ceda and insert the buffer directly into the grid
    UfisCom1.Twe = sHopo + "," + sTableExt + "," + sAppl
    If UfisCom1.CallServer("RT", strTable, strFieldlist, "", strWhere, "360") = 0 Then
        llRecords = UfisCom1.GetBufferCount
        For i = 0 To (llRecords - 1)
            tmpStr = tmpStr + UfisCom1.GetBufferLine(i) + Chr(10)
            If (i > 0) And (i Mod 50) = 0 Then
                TabJOB.InsertBuffer tmpStr, Chr(10)
                tmpStr = ""
            End If
        Next i
        TabJOB.InsertBuffer tmpStr, Chr(10)
        LoadJOBTAB = True
    Else
        LoadJOBTAB = False
        strLastError = LoadResString(118) 'Keine Jobs f�r den Mitarbeiter!
    End If
End Function

Private Function FindActualJob() As Boolean
    'jetzt sortieren wir, um die jobs in der richtigen Reihenfolge zu bekommen
    TabJOB.Sort 1, True, True

    'und nu geht's los ...
    Dim blFoundActualRecord As Boolean
    Dim blCheckEmpInformed As Boolean
    Dim strTmp As String
    Dim strSTAT As String
    Dim iIdx As Integer

    blFoundActualRecord = False
    While (blFoundActualRecord = False) And (TabJOB.GetLineCount > 0)
        blCheckEmpInformed = False
        iIdx = GetItemNo(TabJOB.HeaderString, "STAT") - 1
        strSTAT = TabJOB.GetColumnValue(0, iIdx)

        If Len(strSTAT) > 0 Then
            '1. Zeichen gibt an, ob job bereits angetreten
            strTmp = Left(strSTAT, 1)
            If strTmp <> "P" Then
                blCheckEmpInformed = True
            End If

            '5. Zeichen gibt an, ob job offen oder bereits angetreten
            If blCheckEmpInformed = False Then
                strTmp = ""
                If Len(strSTAT) > 4 Then
                    strTmp = Mid(strSTAT, 5, 1)
                    If strTmp = "1" Then
                        blCheckEmpInformed = True
                    End If
                End If
            End If

            'nun schaun wir, ob wir nen job gefunden haben oder die erste Zeile l�schen
            If blCheckEmpInformed = True Then
                TabJOB.DeleteLine 0
            Else
                blFoundActualRecord = True
            End If
        Else
            blFoundActualRecord = True
        End If
    Wend

    If blFoundActualRecord = False Then
        strLastError = LoadResString(119) 'Kein offener Einsatz gefunden!
    End If
    FindActualJob = blFoundActualRecord
End Function

Private Sub FindEinsatzTyp()
    Dim strFieldlist As String
    Dim strTable As String
    Dim strWhere As String
    Dim strURNO As String
    Dim strUDEM As String
    Dim strTmp As String
    Dim iIdx As Integer

    strDETY = ""
    iIdx = GetItemNo(TabJOB.HeaderString, "URNO") - 1
    strURNO = TabJOB.GetColumnValue(0, iIdx)

    strTable = "JODTAB"
    strFieldlist = "UDEM"
    strWhere = "WHERE UJOB = '" & strURNO & "'"

    UfisCom1.Twe = sHopo + "," + sTableExt + "," + sAppl
    If UfisCom1.CallServer("RT", strTable, strFieldlist, "", strWhere, "360") = 0 Then
        strUDEM = UfisCom1.GetBufferLine(0)
        If strUDEM <> "" Then
            strTable = "DEMTAB"
            strFieldlist = "DETY"
            strWhere = "WHERE URNO = '" & strUDEM & "'"
            UfisCom1.Twe = sHopo + "," + sTableExt + "," + sAppl
            If UfisCom1.CallServer("RT", strTable, strFieldlist, "", strWhere, "360") = 0 Then
                strTmp = UfisCom1.GetBufferLine(0)
            End If
        End If
    End If
    strDETY = strTmp
End Sub

Private Sub FindEinheit()
    Dim strFieldlist As String
    Dim strTable As String
    Dim strWhere As String
    Dim strREFT As String
    Dim strUALO As String
    Dim strUAID As String
    Dim iIdx As Integer

    iIdx = GetItemNo(TabJOB.HeaderString, "UALO") - 1
    strUALO = TabJOB.GetColumnValue(0, iIdx)

    strTable = "ALOTAB"
    strFieldlist = "REFT"
    strWhere = "WHERE URNO = '" & strUALO & "'"

    ' Read from ceda and insert the buffer directly into the grid
    UfisCom1.Twe = sHopo + "," + sTableExt + "," + sAppl
    If UfisCom1.CallServer("RT", strTable, strFieldlist, "", strWhere, "360") = 0 Then
        strREFT = UfisCom1.GetBufferLine(0)
        strTable = GetItem(strREFT, 1, ".")
        Select Case strTable
            Case "ACR":
                strEINHEIT = LoadResString(140)     ' "Registrierung: "
            Case "BLT":
                strEINHEIT = LoadResString(141)     ' "Gep�ck Band: "
            Case "CIC":
                strEINHEIT = LoadResString(142)     ' "Checkin Counter: "
            Case "EXT":
                strEINHEIT = LoadResString(143)     ' "Ausgang: "
            Case "GAT":
                strEINHEIT = LoadResString(144)     ' "Gate: "
            Case "POL":
                strEINHEIT = LoadResString(145)     ' "Pool: "
            Case "PST":
                strEINHEIT = LoadResString(146)     ' "Position: "
            Case "WRO":
                strEINHEIT = LoadResString(147)     ' "Warteraum: "
            Case Else:
                strEINHEIT = strTable & ": "
        End Select

        iIdx = GetItemNo(TabJOB.HeaderString, "UAID") - 1
        strUAID = TabJOB.GetColumnValue(0, iIdx)

        strTable = strTable & "TAB"
        strFieldlist = GetItem(strREFT, 2, ".")
        strWhere = "WHERE URNO = '" & strUAID & "'"

        If UfisCom1.CallServer("RT", strTable, strFieldlist, "", strWhere, "360") = 0 Then
            strEINHEIT = strEINHEIT & UfisCom1.GetBufferLine(0)
        End If
    Else
        strEINHEIT = ""
    End If
End Sub

Private Sub FindFlugNummer()
    Dim strFieldlist As String
    Dim strTable As String
    Dim strWhere As String
    Dim strUAFT As String
    Dim iIdx As Integer

    iIdx = GetItemNo(TabJOB.HeaderString, "UAFT") - 1
    strUAFT = TabJOB.GetColumnValue(0, iIdx)

    strTable = "AFTTAB"
    strFieldlist = "FLNO"
    strWhere = "WHERE URNO = '" & strUAFT & "'"

    UfisCom1.Twe = sHopo + "," + sTableExt + "," + sAppl
    If UfisCom1.CallServer("RT", strTable, strFieldlist, "", strWhere, "360") = 0 Then
        strFLNO = UfisCom1.GetBufferLine(0)
    Else
        strFLNO = ""
    End If
End Sub

Private Sub FindWegezeit()
    Dim strFieldlist As String
    Dim strTable As String
    Dim strWhere As String

    Dim strURNO As String
    Dim strUDEM As String
    Dim strURUD As String
    Dim strTmp As String

    Dim iIdx As Integer

    iIdx = GetItemNo(TabJOB.HeaderString, "URNO") - 1
    strURNO = TabJOB.GetColumnValue(0, iIdx)

    strTable = "JODTAB"
    strFieldlist = "UDEM"
    strWhere = "WHERE UJOB = '" & strURNO & "'"

    UfisCom1.Twe = sHopo + "," + sTableExt + "," + sAppl
    '*** 1.: we look in the JODTAB
    If UfisCom1.CallServer("RT", strTable, strFieldlist, "", strWhere, "360") = 0 Then
        strUDEM = UfisCom1.GetBufferLine(0)
        If strUDEM <> "" Then
        '*** 2.: we look in the DEMTAB
            strTable = "DEMTAB"
            strFieldlist = "URUD"
            strWhere = "WHERE URNO = '" & strUDEM & "'"
            If UfisCom1.CallServer("RT", strTable, strFieldlist, "", strWhere, "360") = 0 Then
                strURUD = UfisCom1.GetBufferLine(0)
                If strURUD <> "" Then
                '*** 3.: we look in the RUDTAB
                    strTable = "RUDTAB"
                    strFieldlist = "TTGT"
                    strWhere = "WHERE URNO = '" & strURUD & "'"
                    If UfisCom1.CallServer("RT", strTable, strFieldlist, "", strWhere, "360") = 0 Then
                        strTmp = Trim(UfisCom1.GetBufferLine(0))
                        If IsNumeric(strTmp) = True Then
                            strTTGT = CStr(CInt((CInt(strTmp) / 60))) & " min."
                        Else
                            strTTGT = "0 min."
                        End If
                    End If
                End If
            Else
                strTTGT = ""
            End If
        Else
            strTTGT = ""
        End If
    Else
        strTTGT = ""
    End If
End Sub

Private Sub FillTextBoxes()
    Dim ilTmp As Integer
    Dim iIdx As Integer

    'Name des Mitarbeiters
    txtMitarbeiter.Text = strFINM & " " & strLANM

    'Einsatztyp
    iIdx = GetItemNo(TabJOB.HeaderString, "UJTY") - 1
    If TabJOB.GetColumnValue(0, iIdx) <> 2011 Then
        If IsNumeric(strDETY) = True Then
            ilTmp = CInt(strDETY)   ' 0 => Durchgang
                                    ' 1 => Annahme
                                    ' 2 => Bereitstellung
                                    ' 6 => Common Checkin
                                    ' 7 => Flugunabh�ngig
         Else
            ilTmp = 7               ' 7 => Flugunabh�ngig
        End If
    Else
        ilTmp = 8                   ' 8 => Pause
    End If
    lblEinsatztyp.Caption = LoadResString(130 + ilTmp)

    'Zeit von
    iIdx = GetItemNo(TabJOB.HeaderString, "ACFR") - 1
    lblZeitVon.Caption = Format(CDate(CedaFullDateToVb(TabJOB.GetColumnValue(0, iIdx))) + (sgUTCOffsetHours / 24), "DD.MM.YYYY hh:mm")

    'Zeit bis
    iIdx = GetItemNo(TabJOB.HeaderString, "ACTO") - 1
    lblZeitBis.Caption = Format(CDate(CedaFullDateToVb(TabJOB.GetColumnValue(0, iIdx))) + (sgUTCOffsetHours / 24), "DD.MM.YYYY hh:mm")

    'Einheit (z.B. Gate: A27)
    lblEinheit.Caption = strEINHEIT

    'Flugnummer
    lblFlugNr.Caption = strFLNO

    'Wegezeit
    lblWegeZeit.Caption = strTTGT

    'Kommentar
    iIdx = GetItemNo(TabJOB.HeaderString, "TEXT") - 1
    lblKommentar.Caption = TabJOB.GetColumnValue(0, iIdx)
End Sub

Private Sub DeleteTextBoxes()
    txtMitarbeiter.Text = ""
    lblEinsatztyp.Caption = ""
    lblZeitVon.Caption = ""
    lblZeitBis.Caption = ""
    lblEinheit.Caption = ""
    lblFlugNr.Caption = ""
    lblWegeZeit.Caption = ""
    lblKommentar.Caption = ""
End Sub

Private Function JobAnnehmen() As Boolean
    JobAnnehmen = False

    If blLoaded = True Then
        Me.MousePointer = vbArrowHourglass

        Dim strFieldlist As String
        Dim strTable As String
        Dim strWhere As String
        Dim strURNO As String
        Dim strSTAT As String

        Dim iIdx As Integer

        iIdx = GetItemNo(TabJOB.HeaderString, "URNO") - 1
        strURNO = TabJOB.GetColumnValue(0, iIdx)

        iIdx = GetItemNo(TabJOB.HeaderString, "STAT") - 1
        strSTAT = TabJOB.GetColumnValue(0, iIdx)

        Select Case Len(strSTAT)
            Case 0:
                strSTAT = strSTAT & "    " & CStr("1")
            Case 1:
                strSTAT = strSTAT & "   " & CStr("1")
            Case 2:
                strSTAT = strSTAT & "  " & CStr("1")
            Case 3:
                strSTAT = strSTAT & " " & CStr("1")
            Case 4:
                strSTAT = strSTAT & CStr("1")
            Case 5:
                strSTAT = Left(strSTAT, 4) & CStr("1")
            Case Else:
                strSTAT = Left(strSTAT, 4) & CStr("1") & Right(strSTAT, Len(strSTAT) - 5)
        End Select

        strTable = "JOBTAB"
        strFieldlist = "STAT"
        strWhere = "WHERE URNO = '" & strURNO & "'"

        UfisCom1.Twe = sHopo + "," + sTableExt + "," + sAppl
        If UfisCom1.CallServer("URT", strTable, strFieldlist, strSTAT, strWhere, "360") = 0 Then
            Me.MousePointer = vbDefault
            frmInfo.Show vbModal
            JobAnnehmen = True
        End If
        Me.MousePointer = vbDefault
    End If
End Function

Private Sub InitValues()
    Dim strResult As String
    Dim strConfigString As String
    Dim strConfigFileName As String

    If DEFAULT_CEDA_INI = "" Then GetUfisDir
    'strConfigFileName = "C:\Ufis\System\EmployeeList.cfg"
    strConfigFileName = UFIS_SYSTEM & "\EmployeeList.cfg"
    strConfigString = GetFileContent(strConfigFileName)
'SERVER
    GetKeyItem strResult, strConfigString, "{=SERVER=}", "{=\=}"
    If strResult <> "" Then
        sServer = strResult
    End If
'HOPO
    GetKeyItem strResult, strConfigString, "{=HOPO=}", "{=\=}"
    If strResult <> "" Then
        sHopo = strResult
    End If
'JOBTYPES
    GetKeyItem strResult, strConfigString, "{=JOBTYPES=}", "{=\=}"
    If strResult <> "" Then
        sJobTypes = strResult
    End If
'TABLE EXTENSION
    sTableExt = "TAB"
'APPLICATION
    sAppl = "InfoPC"
'CONNECTION TYPE
    sConnectType = "CEDA"
'LOGO
    GetKeyItem strResult, strConfigString, "{=LOGO=}", "{=\=}"
    If strResult <> "" Then
        strLOGO = strResult
    End If
End Sub

Private Function GetUTCOffset() As Single
    Dim strUtcArr() As String
    Dim strArr() As String
    Dim CurrStr As String
    Dim tmpStr As String

    Dim istrRet As Integer
    Dim count As Integer
    Dim i As Integer

    GetUTCOffset = 0

    UfisCom1.Twe = sHopo + "," + sTableExt + "," + sAppl
    If UfisCom1.CallServer("GFR", "", "", "", "[CONFIG]", "360") = 0 Then
        tmpStr = UfisCom1.GetDataBuffer(True)
    End If
    strArr = Split(tmpStr, Chr(10))
    count = UBound(strArr)
    For i = 0 To count
        CurrStr = strArr(i)
        istrRet = InStr(1, CurrStr, "UTCD", 0)
        If istrRet <> 0 Then
            strUtcArr = Split(strArr(i), ",")
            i = count + 1
            GetUTCOffset = CSng(strUtcArr(1) / 60)
        End If
    Next i
End Function
