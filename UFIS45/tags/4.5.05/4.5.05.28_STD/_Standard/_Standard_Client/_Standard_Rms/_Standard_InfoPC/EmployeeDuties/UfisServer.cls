VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "UFisServer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public WithEvents bcProx As BcProxyLib.BcPrxy
Attribute bcProx.VB_VarHelpID = -1

Private Sub bcProx_OnBcReceive(ByVal ReqId As String, ByVal Dest1 As String, _
                               ByVal Dest2 As String, ByVal Cmd As String, _
                               ByVal Object As String, ByVal Seq As String, _
                               ByVal Tws As String, ByVal Twe As String, _
                               ByVal Selection As String, ByVal Fields As String, _
                               ByVal Data As String, ByVal BcNum As String)
    If Cmd = "CLO" Then
        MsgBox "Lost Connection to server. The application will terminate now !!!"
        'UnregisterBroadcasts
        End
    Else
        Select Case (Object)
        Case "JOBTAB"
            DistributeJobBc BcNum, Dest1, ReqId, Object, Cmd, Selection, Fields, Data
        Case "DELTAB"
            DistributeDelBc BcNum, Dest1, ReqId, Object, Cmd, Selection, Fields, Data
        Case "DRRTAB"
            DistributeDrrBc BcNum, Dest1, ReqId, Object, Cmd, Selection, Fields, Data
        Case "STFTAB"
            DistributeNormalBroadcast BcNum, Dest1, ReqId, Object, Cmd, Selection, Fields, Data
        Case "JTYTAB"
            DistributeNormalBroadcast BcNum, Dest1, ReqId, Object, Cmd, Selection, Fields, Data
        Case "BSDTAB"
            DistributeNormalBroadcast BcNum, Dest1, ReqId, Object, Cmd, Selection, Fields, Data
        Case "SPFTAB"
            DistributeNormalBroadcast BcNum, Dest1, ReqId, Object, Cmd, Selection, Fields, Data
        Case "PFCTAB"
            DistributeNormalBroadcast BcNum, Dest1, ReqId, Object, Cmd, Selection, Fields, Data
        Case Else
            Exit Sub
        End Select
    End If
End Sub

Public Sub InitBroadcasts()
    Set bcProx = New BcPrxy
End Sub

Public Sub DistributeJobBc(BcNum As String, DestName As String, RecvName As String, _
                           cpTable As String, cpCmd As String, cpSelKey As String, _
                           cpFields As String, cpData As String)

    Dim LineIdx 'As Variant

    Dim strFieldList As String
    Dim strDataLine As String
    Dim tmpFldNam As String
    Dim tmpFldVal As String
    Dim strLineNo As String
    Dim tmpList As String
    Dim tmpUrno As String

    Dim UrnoIdx As Integer
    Dim ItmNbr As Integer
    Dim FldIdx As Integer

    Dim myTab As TABLib.TAB
    Set myTab = frmTables.tabJOB

    tmpUrno = GetFieldValue("URNO", cpData, cpFields)
    If tmpUrno = "" Then
        tmpUrno = GetUrnoFromSqlKey(cpSelKey)
    End If

    ' Update events
    If cpCmd = "URT" Then
        If tmpUrno <> "" Then
            If IsPassJobFilter(tmpUrno, cpFields, cpData) = True Then
                UrnoIdx = GetItemNo(myTab.HeaderString, "URNO") - 1
                tmpList = myTab.GetLinesByColumnValue(UrnoIdx, tmpUrno, 1)
                If tmpList <> "" Then
                    LineIdx = GetItem(tmpList, 1, ",")
                    ItmNbr = 0
                    Do
                        ItmNbr = ItmNbr + 1
                        tmpFldNam = GetItem(cpFields, ItmNbr, ",")
                        tmpFldVal = GetItem(cpData, ItmNbr, ",")
                        If tmpFldNam <> "" Then
                            FldIdx = GetItemNo(myTab.HeaderString, tmpFldNam) - 1
                            If FldIdx >= 0 Then
                                myTab.SetColumnValue LineIdx, FldIdx, tmpFldVal
                            End If
                        End If
                    Loop While tmpFldNam <> ""
                    'frmTables.PrepareResultList
                    strLineNo = frmTables.tabJOB.GetLinesByColumnValue(0, tmpUrno, 1)
                    If strLineNo <> "" Then
                        strDataLine = frmTables.tabJOB.GetLineValues(CLng(strLineNo))
                        frmTables.UpdateJobResultList strDataLine
                        SortTabResult
                    End If
                    MainForm.RebuildScreen
                    MainForm.HighlightCells (tmpUrno)
                End If
            End If
        End If
    End If
    
    'Insert events
    If cpCmd = "IRT" Then
        If IsPassJobFilter(tmpUrno, cpFields, cpData) = True Then
            myTab.InsertTextLine frmTables.EmptyJobLine, True
            LineIdx = myTab.GetLineCount - 1
            If tmpUrno <> "" Then
                UrnoIdx = GetItemNo(myTab.HeaderString, "URNO") - 1
                ItmNbr = 0
                Do
                    ItmNbr = ItmNbr + 1
                    tmpFldNam = GetItem(cpFields, ItmNbr, ",")
                    tmpFldVal = GetItem(cpData, ItmNbr, ",")
                    If tmpFldNam <> "" Then
                        FldIdx = GetItemNo(myTab.HeaderString, tmpFldNam) - 1
                        If FldIdx >= 0 Then
                            myTab.SetColumnValue LineIdx, FldIdx, tmpFldVal
                        End If
                    End If
                Loop While tmpFldNam <> ""
                frmTables.PrepareResultList
                SortTabResult
                MainForm.RebuildScreen
                MainForm.HighlightCells (tmpUrno)
            End If
        End If
    End If
    
    'Delete Events
    If cpCmd = "DRT" Then
        If tmpUrno <> "" Then
            UrnoIdx = GetItemNo(myTab.HeaderString, "URNO") - 1
            tmpList = myTab.GetLinesByColumnValue(UrnoIdx, tmpUrno, 1)
            If tmpList <> "" Then
                LineIdx = GetItem(tmpList, 1, ",")
                myTab.DeleteLine LineIdx
                frmTables.PrepareResultList
                SortTabResult
                MainForm.RebuildScreen
            End If
        End If
    End If
    myTab.RedrawTab
End Sub
' ------------------------------------------------------------
' Check the timeframe and the job status [4] = 0 then job
' must be visible
' ------------------------------------------------------------
Public Function IsPassJobFilter(cpUrno As String, cpFields As String, cpData As String) As Boolean
    Dim blRet As Boolean
    Dim ItemIdx As Integer
    Dim Item As String
    Dim strACFR As String, strACTO As String
    Dim ItemNbr As Integer
    Dim tmpStr As String
    
    blRet = False
    ' First check for UJTY
    ItemIdx = GetItemNo(cpFields, "UJTY")
    Item = GetItem(cpData, ItemIdx, ",")
    If Item <> "" Then
        Item = "'" + Item + "'"
        If InStr(MainForm.JOBTYPES, Item) > 0 Then
            blRet = True
        End If
    Else
        tmpStr = frmTables.tabJOB.GetLinesByColumnValue(0, cpUrno, 1)
        If tmpStr <> "" Then
            Item = frmTables.tabJOB.GetColumnValue(CLng(tmpStr), 7)
            If Item <> "" Then
                Item = "'" + Item + "'"
                If InStr(MainForm.JOBTYPES, Item) > 0 Then
                    blRet = True
                End If
            End If
        End If
    End If
    'Check the timeframe of the job
    'Get ACFR and ACTO and look for validity inside the time frame
    'RRO 29.10.01: check only ACFR
    If blRet = True Then
        ItemIdx = GetItemNo(cpFields, "ACFR")
        Item = GetItem(cpData, ItemIdx, ",")
        If Item = "" Then
            tmpStr = frmTables.tabJOB.GetLinesByColumnValue(0, cpUrno, 1)
            If tmpStr <> "" Then
                If IsNumeric(tmpStr) = True Then
                    Item = frmTables.tabJOB.GetColumnValue(CLng(tmpStr), 4)
                End If
            End If
        End If
        strACFR = Item
'        ItemIdx = GetItemNo(cpFields, "ACTO")
'        Item = GetItem(cpData, ItemIdx, ",")
'        If Item = "" Then
'            tmpStr = frmTables.tabJOB.GetLinesByColumnValue(0, cpUrno, 1)
'            If tmpStr <> "" Then
'                Item = frmTables.tabJOB.GetColumnValue(CLng(tmpStr), 5)
'            End If
'        End If
'        strACTO = Item
        If strACFR <> "" Then 'And strACTO <> "" Then
            If (IsWhithin(strACFR, frmTables.TimeFrameStart, frmTables.TimeFrameEnd)) Then 'And _
                'IsWhithin(strACTO, frmTables.TimeFrameStart, frmTables.TimeFrameEnd)) Then
                blRet = True
            Else
                blRet = False
            End If
        Else
            blRet = False
        End If
    End If

    IsPassJobFilter = blRet
End Function
Public Function IsWhithin(cpVal As String, cpStart As String, cpEnd As String) As Boolean
    If (((cpStart) <= (cpVal)) And ((cpVal) <= (cpEnd))) Then
        IsWhithin = True
    Else
        IsWhithin = False
    End If
End Function
Public Sub DistributeDelBc(BcNum As String, DestName As String, RecvName As String, _
                           cpTable As String, cpCmd As String, cpSelKey As String, _
                           cpFields As String, cpData As String)
    Dim tmpUrno As String
    Dim tmpStat As String
    Dim UrnoIdx As Integer
    Dim LineIdx 'As Variant
    Dim FldIdx As Integer
    Dim tmpList As String
    Dim CurRecDat As String
    Dim tmpFldNam As String
    Dim tmpFldVal As String
    Dim ItmNbr As Integer
    Dim myTextColor As Long
    Dim myBackColor As Long
    Dim myTab As TABLib.TAB
    Dim strFieldList As String

End Sub
Public Sub DistributeDrrBc(BcNum As String, DestName As String, RecvName As String, _
                           cpTable As String, cpCmd As String, cpSelKey As String, _
                           cpFields As String, cpData As String)
    Dim tmpUrno As String
    Dim tmpStat As String
    Dim UrnoIdx As Integer
    Dim LineIdx 'As Variant
    Dim FldIdx As Integer
    Dim tmpList As String
    Dim CurRecDat As String
    Dim tmpFldNam As String
    Dim tmpFldVal As String
    Dim ItmNbr As Integer
    Dim myTextColor As Long
    Dim myBackColor As Long
    Dim myTab As TABLib.TAB
    Dim strFieldList As String

End Sub
Public Sub DistributeNormalBroadcast(BcNum As String, DestName As String, RecvName As String, _
                                     cpTable As String, cpCmd As String, cpSelKey As String, _
                                     cpFields As String, cpData As String)
    Dim tmpUrno As String
    Dim tmpStat As String
    Dim UrnoIdx As Integer
    Dim LineIdx 'As Variant
    Dim FldIdx As Integer
    Dim tmpList As String
    Dim CurRecDat As String
    Dim tmpFldNam As String
    Dim tmpFldVal As String
    Dim ItmNbr As Integer
    Dim myTextColor As Long
    Dim myBackColor As Long
    Dim myTab As TABLib.TAB
    Dim strFieldList As String
    Dim EmptyLine As String
    
    Select Case (cpTable)
    Case "STFTAB"
        Set myTab = frmTables.tabSTF
        strFieldList = frmTables.StfFields
        EmptyLine = frmTables.EmptyStfLine
    Case "JTYTAB"
        Set myTab = frmTables.tabJTY
        strFieldList = frmTables.JtyFields
        EmptyLine = frmTables.EmptyJtyLine
    Case "BSDTAB"
        Set myTab = frmTables.tabBSD
        strFieldList = frmTables.BsdFields
        EmptyLine = frmTables.EmptyBsdLine
    Case "SPFTAB"
        Set myTab = frmTables.tabSPF
        strFieldList = frmTables.SpfFields
        EmptyLine = frmTables.EmptySpfLine
    Case "PFCTAB"
        Set myTab = frmTables.tabPFC
        strFieldList = frmTables.PfcFields
        EmptyLine = frmTables.EmptyPfcLine
    Case Else
        Exit Sub
    End Select
    'only called by URT or UBT
    tmpUrno = GetFieldValue("URNO", cpData, cpFields)

    If tmpUrno = "" Then tmpUrno = GetUrnoFromSqlKey(cpSelKey)
    If cpCmd = "URT" Then
        If tmpUrno <> "" Then
            UrnoIdx = GetItemNo(strFieldList, "URNO") - 1
            tmpList = myTab.GetLinesByColumnValue(UrnoIdx, tmpUrno, 1)
            If tmpList <> "" Then
                LineIdx = GetItem(tmpList, 1, ",")
                ItmNbr = 0
                Do
                    ItmNbr = ItmNbr + 1
                    tmpFldNam = GetItem(cpFields, ItmNbr, ",")
                    tmpFldVal = GetItem(cpData, ItmNbr, ",")
                    If tmpFldNam <> "" Then
                        FldIdx = GetItemNo(strFieldList, tmpFldNam) - 1
                        If FldIdx >= 0 Then
                            myTab.SetColumnValue LineIdx, FldIdx, RTrim(tmpFldVal)
                        End If
                    End If
                Loop While tmpFldNam <> ""
            End If
        End If
    End If
    If cpCmd = "IRT" Then
        myTab.InsertTextLine EmptyLine, True
        LineIdx = myTab.GetLineCount - 1
        If tmpUrno <> "" Then
            UrnoIdx = GetItemNo(strFieldList, "URNO") - 1
            ItmNbr = 0
            Do
                ItmNbr = ItmNbr + 1
                tmpFldNam = GetItem(cpFields, ItmNbr, ",")
                tmpFldVal = GetItem(cpData, ItmNbr, ",")
                If tmpFldNam <> "" Then
                    FldIdx = GetItemNo(strFieldList, tmpFldNam) - 1
                    If FldIdx >= 0 Then
                        myTab.SetColumnValue LineIdx, FldIdx, tmpFldVal
                    End If
                End If
            Loop While tmpFldNam <> ""
        End If
    End If

    'Delete Events
    If cpCmd = "DRT" Then
        If tmpUrno <> "" Then
            UrnoIdx = GetItemNo(strFieldList, "URNO") - 1
            tmpList = myTab.GetLinesByColumnValue(UrnoIdx, tmpUrno, 1)
            If tmpList <> "" Then
                LineIdx = GetItem(tmpList, 1, ",")
                myTab.DeleteLine LineIdx
            End If
        End If
    End If
End Sub
Private Sub SortTabResult()
    frmTables.tabResult.Sort "1", True, True
    MainForm.ManageColors
End Sub
