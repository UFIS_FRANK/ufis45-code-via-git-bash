// SelectAppl.cpp : implementation file
//

#include "stdafx.h"

#include "basicdata.h"
#include "ccsglobl.h"

#include "Grp.h"
#include "SelectAppl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelectAppl dialog


CSelectAppl::CSelectAppl(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectAppl::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelectAppl)
	imSelectedAppl = -1;
	//}}AFX_DATA_INIT
}


void CSelectAppl::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectAppl)
	DDX_Control(pDX, IDC_SEL_APPL_LIST, omSelApplLB);
	DDX_LBIndex(pDX, IDC_SEL_APPL_LIST, imSelectedAppl);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelectAppl, CDialog)
	//{{AFX_MSG_MAP(CSelectAppl)
	ON_LBN_DBLCLK(IDC_SEL_APPL_LIST, OnDblclkSelApplList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectAppl message handlers

void CSelectAppl::OnDblclkSelApplList() 
{
	// TODO: Add your control notification handler code here
	OnOK();
}

void CSelectAppl::OnOK() 
{
	// TODO: Add extra validation here
	CString olText,olAppl;
	UpdateData();
	if ( ( imSelectedAppl >= 0 ) && ( imSelectedAppl<omApplList.GetCount() ) )
	{
		POSITION pos = omApplList.GetStartPosition();
		for ( int i=0; i<=imSelectedAppl; i++ )
			omApplList.GetNextAssoc ( pos, olAppl, olText );
		omSelectedAppl = olAppl;
	}
	CDialog::OnOK();
}

BOOL CSelectAppl::OnInitDialog() 
{
	CDialog::OnInitDialog();

	IniStatics();

	// TODO: Add extra initialization here
	char clTable[11], clValue[10], clEntry[21], clToIgnore[256], clTmp[64];
	int i;
	CString	olText, olAppl, olDBStr;

	GetPrivateProfileString("APPLICATIONS", "IGNORE", "", clToIgnore, 255, ogIniFile);

	sprintf ( clTable, "SGR%s", pcgTableExt );
	bool blOk = ogBasicData.CedaAction( "RT", clTable, "DISTINCT APPL", 
										(char*)0, (char*)0, (char*)0 );
	if ( blOk )
	{
		CStringArray *olAppls = ogBasicData.GetDataBuff();
		if ( olAppls )
		{
			for ( i=0; i<olAppls->GetSize(); i++ )
			{
				olAppl = (*olAppls)[i] ;
				olAppl.TrimRight ();
				if ( olAppl!=" " && !olAppl.IsEmpty() && olAppl!="EMPTY" &&
					 !omApplList.Lookup( olAppl, olText ) && !strstr(clToIgnore,olAppl) ) 
					omApplList.SetAt ( olAppl, olAppl );
			}
		}
	}
	i=1;
	do 
	{
		sprintf ( clEntry, "APPL%d", i );
		GetPrivateProfileString("APPLICATIONS", clEntry, "", clValue, 9, ogIniFile);
		if ( clValue[0] && !omApplList.Lookup( clValue, olText )  && !strstr(clToIgnore,clValue)  ) 
		{
			olAppl = clValue;
			omApplList.SetAt ( olAppl, olAppl );
		}
		i++;
	}while ( clValue[0]!='\0' );
	POSITION pos = omApplList.GetStartPosition();

	while ( pos )
	{
		omApplList.GetNextAssoc ( pos, olAppl, olText );
		if ( ogGUILng && ogGUILng->GetString(&olDBStr, "APPL", "TXID", ogAppName, olAppl ) )
		{
			omApplList.SetAt ( olAppl, olDBStr );
			omSelApplLB.AddString ( olDBStr );
		}
		else 
		{
			GetPrivateProfileString("APPL_NAMES", olAppl, "", clTmp, 63, ogIniFile);
			if ( clTmp[0] )
			{
				omApplList.SetAt ( olAppl, clTmp );
				omSelApplLB.AddString ( clTmp );
			}
			else
				omSelApplLB.AddString ( olText );
		}
	}
	if ( omSelApplLB.GetCount() > 0 )
		omSelApplLB.SetCurSel ( 0 );
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CSelectAppl::IniStatics()
{
	CString olStr;
	
	olStr = GetString(IDS_SEL_APPL_TEXT );
	SetDlgItemText ( IDC_SEL_APPL_TXT, olStr );
	olStr = GetString(IDS_SEL_APPL_TIT );
	SetWindowText ( olStr );
}


void CSelectAppl::OnCancel()
{
}