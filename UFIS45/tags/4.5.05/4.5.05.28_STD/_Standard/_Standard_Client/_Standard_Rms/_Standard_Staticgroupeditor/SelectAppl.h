#if !defined(AFX_SELECTAPPL_H__48608700_13A3_11D5_9545_005004BCC8AF__INCLUDED_)
#define AFX_SELECTAPPL_H__48608700_13A3_11D5_9545_005004BCC8AF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SelectAppl.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSelectAppl dialog

class CSelectAppl : public CDialog
{
// Construction
public:
	CSelectAppl(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSelectAppl)
	enum { IDD = IDD_SELECT_APPL };
	CListBox	omSelApplLB;
	int		imSelectedAppl;
	//}}AFX_DATA
	CMapStringToString	omApplList;
	CString				omSelectedAppl;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectAppl)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void IniStatics();
	// Generated message map functions
	//{{AFX_MSG(CSelectAppl)
	afx_msg void OnDblclkSelApplList();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	virtual void OnCancel();

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SELECTAPPL_H__48608700_13A3_11D5_9545_005004BCC8AF__INCLUDED_)
