// HelperWnd.cpp : implementation file
//

#include "stdafx.h"
#include "Grp.h"
#include "HelperWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CHelperWnd

CHelperWnd::CHelperWnd()
{
}

CHelperWnd::~CHelperWnd()
{
}


BEGIN_MESSAGE_MAP(CHelperWnd, CWnd)
	//{{AFX_MSG_MAP(CHelperWnd)
	ON_WM_ACTIVATEAPP()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CHelperWnd message handlers

void CHelperWnd::OnActivateApp(BOOL bActive, HTASK hTask) 
{
	CWnd::OnActivateApp(bActive, hTask);
	
	// TODO: Add your message handler code here
	if ( bActive && theApp.m_pMainWnd && 
		 theApp.m_pMainWnd->IsWindowVisible() )
	{
		theApp.m_pMainWnd->ShowWindow(SW_SHOWNORMAL );
		theApp.m_pMainWnd->SetForegroundWindow ();
	}
}
