/////////////////////////////////////////////////////////////////////////////
// CInitialLoadDlg dialog
#include "stdafx.h"

#include "ccsglobl.h"
#include "InitialLoadDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CInitialLoadDlg::CInitialLoadDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CInitialLoadDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CInitialLoadDlg)
	//}}AFX_DATA_INIT
	Create(IDD,pParent);
	SetWindowText ( GetString (IDS_INIT_APP) );
	ShowWindow(SW_SHOWNORMAL);

	m_Progress.SetRange(0,100);

}

CInitialLoadDlg::~CInitialLoadDlg(void)
{
	pogInitialLoad = NULL;
}

void CInitialLoadDlg::SetProgress(int ipProgress)
{
	m_Progress.OffsetPos(ipProgress);
}

void CInitialLoadDlg::SetMessage(CString opMessage)
{
	m_MsgList.AddString(opMessage);
	if ((m_MsgList.GetCount()-12) > 0)
		m_MsgList.SetTopIndex(m_MsgList.GetCount()-12);
	pogInitialLoad->UpdateWindow();
}
 
BOOL CInitialLoadDlg::DestroyWindow() 
{
	BOOL blRc = CDialog::DestroyWindow();
	delete this;
	return blRc;
}

void CInitialLoadDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CInitialLoadDlg)
	DDX_Control(pDX, IDC_MSGLIST, m_MsgList);
	DDX_Control(pDX, IDC_PROGRESS1, m_Progress);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CInitialLoadDlg, CDialog)
	//{{AFX_MSG_MAP(CInitialLoadDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInitialLoadDlg message handlers
