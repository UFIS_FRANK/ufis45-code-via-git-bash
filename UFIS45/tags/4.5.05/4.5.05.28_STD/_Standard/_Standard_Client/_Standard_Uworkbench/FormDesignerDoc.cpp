// FormDesignerDoc.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "FormDesignerDoc.h"
#include "FormDesignerView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FormDesignerDoc

IMPLEMENT_DYNCREATE(FormDesignerDoc, CDocument)

FormDesignerDoc::FormDesignerDoc()
{
	imMapMode = MM_ANISOTROPIC;
	omPaperColor = RGB(255, 255, 255);
//	omPaperColor = RGB(192, 192, 192);
	ComputePageSize();
}

BOOL FormDesignerDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;
	pomForm = new UFormProperties();
	int ilC = ogForms.GetSize()+1;
	CString olStr;
	olStr.Format("%s%d", pomForm->omName, ilC); 
	pomForm->omName = olStr;
	ogForms.Add(pomForm);
	CMultiDocTemplate* pTpl;
	pTpl = (CMultiDocTemplate*)((CUWorkBenchApp*)AfxGetApp())->pDesignerTpl;
	SetTitle(pomForm->omName);
	if(pTpl != NULL)
	{
		POSITION rlPos = pTpl->GetFirstDocPosition();
		if(rlPos != NULL)
		{
			while(rlPos != NULL)
			{
				CDocument *polDoc = pTpl->GetNextDoc( rlPos );
				if(polDoc != NULL)
				{
					polDoc->UpdateAllViews(NULL);
				}
			}
		}
	}
	return TRUE;
}

FormDesignerDoc::~FormDesignerDoc()
{
	POSITION pos = omObjects.GetHeadPosition();
	while (pos != NULL)
		delete omObjects.GetNext(pos);
//	pomForm->omProperties.DeleteAll();
//	pomForm->omDBFields.DeleteAll();
//	delete pomForm;
}

void FormDesignerDoc::Draw(CDC* pDC, FormDesignerView* popView)
{
	POSITION pos = omObjects.GetHeadPosition();
	while (pos != NULL)
	{
		UFormControlObj* popObj = omObjects.GetNext(pos);
		popObj->Draw(pDC, popView);
		
		if (popView->bmActive && !pDC->IsPrinting() && popView->IsSelected(popObj))
			popObj->DrawTracker(pDC, UFormControlObj::selected);
	}
}

void FormDesignerDoc::Add(UFormControlObj* popObj)
{
	omObjects.AddTail(popObj);
	popObj->pomDocument = this;
	SetModifiedFlag();
}

void FormDesignerDoc::Remove(UFormControlObj* popObj)
{
	// Find and remove from document
	POSITION pos = omObjects.Find(popObj);
	if (pos != NULL)
		omObjects.RemoveAt(pos);
	// set document modified flag
	SetModifiedFlag();

	// call remove for each view so that the view can remove from m_selection
	pos = GetFirstViewPosition();
	while (pos != NULL)
		((FormDesignerView*)GetNextView(pos))->Remove(popObj);
}

// point is in logical coordinates
UFormControlObj* FormDesignerDoc::ObjectAt(const CPoint& point)
{
	CRect rect(point, CSize(1, 1));
	POSITION pos = omObjects.GetTailPosition();
	while (pos != NULL)
	{
		UFormControlObj* popObj = omObjects.GetPrev(pos);
		if (popObj->Intersects(rect))
			return popObj;
	}

	return NULL;
}


void FormDesignerDoc::ComputePageSize()
{
	CSize new_size(850, 1100);  // 8.5" x 11" default

	CPrintDialog dlg(FALSE);
	if (AfxGetApp()->GetPrinterDeviceDefaults(&dlg.m_pd))
	{
		// GetPrinterDC returns a HDC so attach it
		CDC dc;
		HDC hDC= dlg.CreatePrinterDC();
		ASSERT(hDC != NULL);
		dc.Attach(hDC);

		// Get the size of the page in loenglish
		new_size.cx = MulDiv(dc.GetDeviceCaps(HORZSIZE), 1000, 254);
		new_size.cy = MulDiv(dc.GetDeviceCaps(VERTSIZE), 1000, 254);
	}

	// if size changed then iterate over views and reset
	if (new_size != omSize)
	{
		omSize = new_size;
		POSITION pos = GetFirstViewPosition();
		while (pos != NULL)
			((FormDesignerView*)GetNextView(pos))->SetPageSize(omSize);
	}
}


BEGIN_MESSAGE_MAP(FormDesignerDoc, CDocument)
	//{{AFX_MSG_MAP(FormDesignerDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FormDesignerDoc diagnostics

#ifdef _DEBUG
void FormDesignerDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void FormDesignerDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// FormDesignerDoc serialization

void FormDesignerDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// FormDesignerDoc commands
void FormDesignerDoc::AddCtrl(UCtrlProperties *popCtrl)
{
	pomForm->omProperties.Add(popCtrl);
}
void FormDesignerDoc::AddDBField(UFormField* popField)
{
	pomForm->omDBFields.Add(popField);
}
void FormDesignerDoc::RemoveCtrl(UCtrlProperties *popCtrl)
{
	for(int i = pomForm->omProperties.GetSize()-1; i >= 0; i--)
	{
		if(&(pomForm->omProperties[i]) == popCtrl)
		{
			pomForm->omProperties.DeleteAt(i);
		}
	}
}
void FormDesignerDoc::RemoveDBField(UFormField* popField)
{
	for(int i = pomForm->omDBFields.GetSize()-1; i >= 0; i--)
	{
		if(&pomForm->omDBFields[i] == popField)
		{
			pomForm->omDBFields.DeleteAt(i);
		}
	}
}
int  FormDesignerDoc::IncStaticCounter()
{
	return ++pomForm->imStaticCount;
}
int  FormDesignerDoc::IncEditCounter()
{
	return ++pomForm->imEditCount;
}
int  FormDesignerDoc::IncComboboxCounter()
{
	return ++pomForm->imComboboxCount;
}
int  FormDesignerDoc::IncCheckboxCounter()
{
	return ++pomForm->imCheckboxCount;
}
int  FormDesignerDoc::IncRadioCounter()
{
	return ++pomForm->imRadioCount;
}
int  FormDesignerDoc::IncListBoxCounter()
{
	return ++pomForm->imListBoxCount;
}
int  FormDesignerDoc::IncButtonCounter()
{
	return ++pomForm->imButtonCount;
}
int  FormDesignerDoc::IncRectCounter()
{
	return ++pomForm->imRectCount;
}
int  FormDesignerDoc::IncLineCounter()
{
	return ++pomForm->imLineCount;
}

int  FormDesignerDoc::IncGridCounter()
{
	return ++pomForm->imGridCount;
}

BOOL FormDesignerDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
//	CDocument::OnNewDocument();
//	if (CDocument::OnOpenDocument(""))
//		return FALSE;
	
	pomForm = (UFormProperties*)lpszPathName;

	SetTitle(pomForm->omName);
	for(int i = 0; i < pomForm->omProperties.GetSize(); i++)
	{
		if(pomForm->omProperties[i].omCtrlType == "button")
		{
			UFormButtonControl *polObj = new UFormButtonControl(pomForm->omProperties[i].omRect);
			polObj->pomProperties = (UCtrlProperties*)&(pomForm->omProperties[i]); 
			Add((UFormControlObj*)polObj);
		}
		if(pomForm->omProperties[i].omCtrlType == "edit")
		{
			UFormEditControl *polObj = new UFormEditControl(pomForm->omProperties[i].omRect);
			polObj->pomProperties = (UCtrlProperties*)&(pomForm->omProperties[i]); 
			Add((UFormControlObj*)polObj);
		}
		if(pomForm->omProperties[i].omCtrlType == "checkbox")
		{
			UFormCheckboxControl *polObj = new UFormCheckboxControl(pomForm->omProperties[i].omRect);
			polObj->pomProperties = (UCtrlProperties*)&(pomForm->omProperties[i]); 
			Add((UFormControlObj*)polObj);
		}
		if(pomForm->omProperties[i].omCtrlType == "combobox")
		{
			UFormComboboxControl *polObj = new UFormComboboxControl(pomForm->omProperties[i].omRect);
			polObj->pomProperties = (UCtrlProperties*)&(pomForm->omProperties[i]); 
			Add((UFormControlObj*)polObj);
		}
		if(pomForm->omProperties[i].omCtrlType == "static")
		{
			UFormStaticControl *polObj = new UFormStaticControl(pomForm->omProperties[i].omRect);
			polObj->pomProperties = (UCtrlProperties*)&(pomForm->omProperties[i]); 
			Add((UFormControlObj*)polObj);
		}
		if(pomForm->omProperties[i].omCtrlType == "line")
		{
			UFormRectControl *polObj = new UFormRectControl(pomForm->omProperties[i].omRect);
			polObj->pomProperties = (UCtrlProperties*)&(pomForm->omProperties[i]); 
			polObj->omShape = UFormRectControl::line;
			Add((UFormControlObj*)polObj);
		}
		if(pomForm->omProperties[i].omCtrlType == "rectangle")
		{
			UFormRectControl *polObj = new UFormRectControl(pomForm->omProperties[i].omRect);
			polObj->pomProperties = (UCtrlProperties*)&(pomForm->omProperties[i]); 
			polObj->omShape = UFormRectControl::rectangle;
			Add((UFormControlObj*)polObj);
		}
		if(pomForm->omProperties[i].omCtrlType == "grid")
		{
			UFormGridControl  *polObj = new UFormGridControl(pomForm->omProperties[i].omRect);
			polObj->pomProperties = (UCtrlProperties*)&(pomForm->omProperties[i]); 
			Add((UFormControlObj*)polObj);
		}
		if(pomForm->omProperties[i].omCtrlType == "radio")
		{
			UFormRadioButtonControl *polObj = new UFormRadioButtonControl(pomForm->omProperties[i].omRect);
			polObj->pomProperties = (UCtrlProperties*)&(pomForm->omProperties[i]); 
			Add((UFormControlObj*)polObj);
		}
		if(pomForm->omProperties[i].omCtrlType == "group")
		{
			UFormGroupControl *polObj = new UFormGroupControl(pomForm->omProperties[i].omRect);
			polObj->pomProperties = (UCtrlProperties*)&(pomForm->omProperties[i]); 
			Add((UFormControlObj*)polObj);
		}
		if(pomForm->omProperties[i].omCtrlType == "listbox")
		{
			//TO DO
		}

	}
	// TODO: Add your specialized creation code here
	
	return TRUE;
}

BOOL FormDesignerDoc::CanCloseFrame(CFrameWnd* pFrame) 
{
	// TODO: Add your specialized code here and/or call the base class
	
//	return CDocument::CanCloseFrame(pFrame);
	return TRUE;
}
