// ScriptHost.h: interface for the CScriptHost class.
//
//////////////////////////////////////////////////////////////////////
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *	UWorkBench - Project
 *	
 *		ScriptHost Wrapper class to get a single instance of 
 *		the script host.
 *
 *	ABB Airport Technologies GmbH 2000
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *
 *		Date		Author			Comment
 *		-------------------------------------------------------------
 *
 *		17/10/00	cla AAT/IR		Initial version
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#if !defined(AFX_SCRIPTHOST_H__71160713_8A26_11D4_904C_00010204AA51__INCLUDED_)
#define AFX_SCRIPTHOST_H__71160713_8A26_11D4_904C_00010204AA51__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#import "..\Share\Scripthost\msscript.ocx" no_namespace named_guids

class CScriptHost  
{
private:
	CScriptHost();

public:
	static CScriptHost* CreateInstance();

	virtual ~CScriptHost();

	IScriptControlPtr pScriptControl;

private:
	static CScriptHost* s_pScriptHost;
};

#endif // !defined(AFX_SCRIPTHOST_H__71160713_8A26_11D4_904C_00010204AA51__INCLUDED_)
