// UDragDropListBox.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "UDragDropListBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include "CCSGlobl.h"
/////////////////////////////////////////////////////////////////////////////
// UDragDropListBox

UDragDropListBox::UDragDropListBox(CWnd *popParent)
{
	pomParent = popParent;
}

UDragDropListBox::~UDragDropListBox()
{
}


BEGIN_MESSAGE_MAP(UDragDropListBox, CListBox)
	//{{AFX_MSG_MAP(UDragDropListBox)
	ON_WM_QUERYDRAGICON()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
	ON_WM_CREATE()
	ON_WM_DESTROY()
    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
    ON_MESSAGE(WM_DROP, OnDrop)  
//    ON_MESSAGE(WM_DRAGOVER, OnDragOver)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// UDragDropListBox message handlers

HCURSOR UDragDropListBox::OnQueryDragIcon() 
{
	// TODO: Add your message handler code here and/or call default
	
	return CListBox::OnQueryDragIcon();
}

void UDragDropListBox::OnMouseMove(UINT nFlags, CPoint point) 
{
	if (nFlags & MK_LBUTTON)	
	{
		BOOL b;
		UINT ilItem;
		ilItem = ItemFromPoint(point, b);
		m_DragDropSource.CreateDWordData(WM_DND_LBOX_BEGIN_DRAG, 1);
		m_DragDropSource.AddDWord((DWORD)ilItem);
		pomParent->SendMessage(WM_DND_LBOX_BEGIN_DRAG, ilItem, 0);
		m_DragDropSource.BeginDrag();

	}
	
	CListBox::OnMouseMove(nFlags, point);
}

void UDragDropListBox::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CListBox::OnLButtonDown(nFlags, point);
}

void UDragDropListBox::OnRButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	CListBox::OnRButtonDown(nFlags, point);
}

void UDragDropListBox::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	// TODO: Add your code to draw the specified item
	
}


int UDragDropListBox::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CListBox::OnCreate(lpCreateStruct) == -1)
		return -1;
	
    m_DragDropTarget.RegisterTarget(this, this);
//	m_DragDropSource.RegisterTarget(this, this);
	
	return 0;
}

void UDragDropListBox::OnDestroy() 
{
	CListBox::OnDestroy();
	m_DragDropTarget.Revoke();
//	m_DragDropSource.Revoke();
}

LONG UDragDropListBox::OnDragOver(UINT wParam, LONG lParam)
{
	CCSDragDropCtrl *pomDragDropCtrl = &m_DragDropTarget;

    ::GetCursorPos(&omDropPosition);
	ScreenToClient(&omDropPosition);    

	BOOL b;
	UINT ilSourceItem = pomDragDropCtrl->GetDataDWord(0);


	UINT ilDestItem = ItemFromPoint(omDropPosition, b);
	pomParent->SendMessage(WM_DND_LBOX_DRAG_OVER, ilSourceItem, ilDestItem);

	return 0L;

	//WM_DND_LBOX_DRAG_OVER
}
LONG UDragDropListBox::OnDrop(UINT wParam, LONG lParam)
{
	CCSDragDropCtrl *pomDragDropCtrl = &m_DragDropTarget;

    ::GetCursorPos(&omDropPosition);
	ScreenToClient(&omDropPosition);    

	BOOL b;
	UINT ilSourceItem = pomDragDropCtrl->GetDataDWord(0);


	UINT ilDestItem = ItemFromPoint(omDropPosition, b);
	pomParent->SendMessage(WM_DND_LBOX_DROP, ilSourceItem, ilDestItem);
	return 0L;
}
