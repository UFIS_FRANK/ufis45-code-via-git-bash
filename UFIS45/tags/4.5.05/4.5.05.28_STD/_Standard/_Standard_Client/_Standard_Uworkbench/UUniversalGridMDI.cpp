// UUniversalGridMDI.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "UUniversalGridMDI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// UUniversalGridMDI 

IMPLEMENT_DYNCREATE(UUniversalGridMDI, CMDIChildWnd)

UUniversalGridMDI::UUniversalGridMDI()
{
}

UUniversalGridMDI::~UUniversalGridMDI()
{
}


BEGIN_MESSAGE_MAP(UUniversalGridMDI, CMDIChildWnd)
	//{{AFX_MSG_MAP(UUniversalGridMDI)
	ON_WM_CREATE()
	ON_COMMAND(ID_LOAD_TIMEFRAME, OnGridTimeFrame)
	ON_COMMAND(ID_UNIGRID_FILTER, OnGridFilter)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// UUniversalGridMDI message handlers

BOOL UUniversalGridMDI::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CMDIChildWnd::OnCreateClient(lpcs, pContext);
}

int UUniversalGridMDI::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this) ||
		!m_wndToolBar.LoadToolBar(IDR_UNIVERSAL_GRID_TOOLBAR))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}
	// TODO: Remove this if you don't want tool tips
	m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY);
	
	return 0;
}

void UUniversalGridMDI::OnGridTimeFrame()
{
}
void UUniversalGridMDI::OnGridFilter()
{
}
