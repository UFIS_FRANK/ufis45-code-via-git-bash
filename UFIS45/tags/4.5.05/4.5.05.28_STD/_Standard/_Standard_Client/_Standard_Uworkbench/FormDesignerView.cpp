// FormDesignerView.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "FormDesignerDoc.h"
#include "UFormDrawTool.h"
#include "FormDesignerView.h"
#include "ActivationOrderDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


CLIPFORMAT FormDesignerView::m_cfDraw = (CLIPFORMAT)
	::RegisterClipboardFormat(_T("MFC Draw Sample"));
CLIPFORMAT FormDesignerView::m_cfObjectDescriptor = NULL;

/////////////////////////////////////////////////////////////////////////////
// FormDesignerView

IMPLEMENT_DYNCREATE(FormDesignerView, CScrollView)

FormDesignerView::FormDesignerView()
{
}

FormDesignerView::~FormDesignerView()
{
}


BEGIN_MESSAGE_MAP(FormDesignerView, CScrollView)
	//{{AFX_MSG_MAP(FormDesignerView)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDBLCLK()
	ON_COMMAND(ID_ARROW_CTRL, OnDrawSelect)
	ON_COMMAND(ID_RECT_CTRL, OnDrawRect)
	ON_COMMAND(ID_LINE_CTRL, OnDrawLine)
	ON_COMMAND(ID_EDIT_CTRL, OnDrawEditCtrl)
	ON_COMMAND(ID_GRID_CTRL, OnDrawGrid)
	ON_UPDATE_COMMAND_UI(ID_GRID_CTRL,   OnUpdateDrawGridCtrl)
	ON_COMMAND(ID_CHECKBOX_CTRL, OnDrawCheckboxCtrl)
	ON_UPDATE_COMMAND_UI(ID_CHECKBOX_CTRL, OnUpdateDrawCheckboxCtrl)
	ON_COMMAND(ID_RADIO_CTRL, OnDrawRadiobuttonCtrl)
	ON_UPDATE_COMMAND_UI(ID_RADIO_CTRL, OnUpdateDrawRadiobuttonCtrl)
	ON_COMMAND(ID_COMBOBOX_CTRL, OnDrawComboboxCtrl)
	ON_COMMAND(ID_BUTTON_CTRL,   OnDrawButtonCtrl)
	ON_UPDATE_COMMAND_UI(ID_BUTTON_CTRL,   OnUpdateDrawButtonCtrl)
	ON_COMMAND(ID_GROUP_CTRL,   OnDrawGroupCtrl)
	ON_UPDATE_COMMAND_UI(ID_GROUP_CTRL,   OnUpdateDrawGroupCtrl)
	ON_COMMAND(ID_LABEL_CTRL, OnDrawStaticCtrl)
	ON_UPDATE_COMMAND_UI(ID_LABEL_CTRL, OnUpdateDrawStatic)
	ON_UPDATE_COMMAND_UI(ID_COMBOBOX_CTRL, OnUpdateDrawComboboxCtrl)
	ON_UPDATE_COMMAND_UI(ID_LINE_CTRL, OnUpdateDrawLine)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CTRL, OnUpdateDrawEdit)
	ON_UPDATE_COMMAND_UI(ID_RECT_CTRL, OnUpdateDrawRect)
	ON_UPDATE_COMMAND_UI(ID_ARROW_CTRL, OnUpdateDrawSelect)
	ON_COMMAND(ID_EDIT_SELECT_ALL, OnEditSelectAll)
	ON_COMMAND(ID_EDIT_CLEAR, OnEditClear)
	ON_COMMAND(ID_ARROW_LEFT, OnKeyLeft)
	ON_COMMAND(ID_ARROW_RIGHT, OnKeyRight)
	ON_COMMAND(ID_ARROW_UP, OnKeyArrowUp)
	ON_COMMAND(ID_ARROW_DOWN, OnKeyDown)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CLEAR, OnUpdateAnySelect)
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateEditCopy)
	ON_COMMAND(ID_EDIT_CUT, OnEditCut)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CUT, OnUpdateEditCut)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, OnUpdateEditPaste)
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	ON_UPDATE_COMMAND_UI(ID_EDIT_SELECT_ALL, OnUpdateEditSelectAll)
	ON_WM_CREATE()
	ON_WM_CONTEXTMENU()
	ON_MESSAGE(WM_KEYUP, OnKeyUp)
	ON_WM_RBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



/////////////////////////////////////////////////////////////////////////////
// FormDesignerView drawing

void FormDesignerView::OnLButtonDown(UINT nFlags, CPoint point)
{
	if (!bmActive)
		return;
	UFormDrawTool* polTool = UFormDrawTool::FindTool(UFormDrawTool::c_drawShape);
	if (polTool != NULL)
		polTool->OnLButtonDown(this, nFlags, point);
}

void FormDesignerView::OnLButtonUp(UINT nFlags, CPoint point)
{
	if (!bmActive)
		return;
	UFormDrawTool* polTool = UFormDrawTool::FindTool(UFormDrawTool::c_drawShape);
	if (polTool != NULL)
		polTool->OnLButtonUp(this, nFlags, point);
}

void FormDesignerView::OnMouseMove(UINT nFlags, CPoint point)
{
	if (!bmActive)
		return;
	UFormDrawTool* polTool = UFormDrawTool::FindTool(UFormDrawTool::c_drawShape);
	if (polTool != NULL)
		polTool->OnMouseMove(this, nFlags, point);
}

void FormDesignerView::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	if (!bmActive)
		return;
	UFormDrawTool* polTool = UFormDrawTool::FindTool(UFormDrawTool::c_drawShape);
	if (polTool != NULL)
		polTool->OnLButtonDblClk(this, nFlags, point);
	else //must be the background
	{
		if(pogPropertiesDlg != NULL)
		{
			pogPropertiesDlg->SetObjectProperties(GetDocument()->pomForm, (UFormControlObj *)this, GetDocument()->pomForm->omTable);
		}
		else
		{
			pogPropertiesDlg = new PropertiesDlg(NULL, GetDocument()->pomForm, (UFormControlObj *)this, GetDocument()->pomForm->omTable);
		}
	}
}

void FormDesignerView::OnDestroy()
{
	CScrollView::OnDestroy();
}

void FormDesignerView::OnDrawSelect()
{
	UFormDrawTool::c_drawShape = selection;
}


void FormDesignerView::OnDrawRect()
{
	UFormDrawTool::c_drawShape = rect;
}

void FormDesignerView::OnDrawLine()
{
	UFormDrawTool::c_drawShape = line;
}

void FormDesignerView::OnDrawGrid()
{
	UFormDrawTool::c_drawShape = grid;
}
void FormDesignerView::OnUpdateDrawGridCtrl(CCmdUI* pCmdUI)
{
	pCmdUI->SetRadio(UFormDrawTool::c_drawShape == grid);
}

void FormDesignerView::OnDrawEditCtrl()
{
	UFormDrawTool::c_drawShape = edit;
}
void FormDesignerView::OnDrawComboboxCtrl()
{
	UFormDrawTool::c_drawShape = combobox;
}
void FormDesignerView::OnDrawStaticCtrl()
{
	UFormDrawTool::c_drawShape = c_static;
}
void FormDesignerView::OnUpdateDrawStatic(CCmdUI* pCmdUI)
{
	pCmdUI->SetRadio(UFormDrawTool::c_drawShape == c_static);
}

void FormDesignerView::OnUpdateDrawComboboxCtrl(CCmdUI* pCmdUI)
{
	pCmdUI->SetRadio(UFormDrawTool::c_drawShape == combobox);
}
void FormDesignerView::OnDrawCheckboxCtrl()
{
	UFormDrawTool::c_drawShape = checkbox;
}
void FormDesignerView::OnUpdateDrawCheckboxCtrl(CCmdUI* pCmdUI)
{
	pCmdUI->SetRadio(UFormDrawTool::c_drawShape == checkbox);
}

void FormDesignerView::OnDrawRadiobuttonCtrl()
{
	UFormDrawTool::c_drawShape = radiobutton;
}

void FormDesignerView::OnUpdateDrawRadiobuttonCtrl(CCmdUI* pCmdUI)
{
	pCmdUI->SetRadio(UFormDrawTool::c_drawShape == radiobutton);
}

void FormDesignerView::OnDrawButtonCtrl()
{
	UFormDrawTool::c_drawShape = button;
}

void FormDesignerView::OnUpdateDrawButtonCtrl(CCmdUI* pCmdUI)
{
	pCmdUI->SetRadio(UFormDrawTool::c_drawShape == button);
}

void FormDesignerView::OnDrawGroupCtrl()
{
	UFormDrawTool::c_drawShape = group;
}

void FormDesignerView::OnUpdateDrawGroupCtrl(CCmdUI* pCmdUI)
{
	pCmdUI->SetRadio(UFormDrawTool::c_drawShape == group);
}

void FormDesignerView::OnUpdateDrawEdit(CCmdUI* pCmdUI)
{
	pCmdUI->SetRadio(UFormDrawTool::c_drawShape == edit);
}

void FormDesignerView::OnUpdateDrawLine(CCmdUI* pCmdUI)
{
	pCmdUI->SetRadio(UFormDrawTool::c_drawShape == line);
}

void FormDesignerView::OnUpdateDrawRect(CCmdUI* pCmdUI)
{
	pCmdUI->SetRadio(UFormDrawTool::c_drawShape == rect);
}


void FormDesignerView::OnUpdateDrawSelect(CCmdUI* pCmdUI)
{
	pCmdUI->SetRadio(UFormDrawTool::c_drawShape == selection);
}

/*void FormDesignerView::OnUpdateSingleSelect(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(omSelection.GetCount() == 1);
}
*/
void FormDesignerView::OnEditSelectAll()
{
	UFormControlObjList* polObList = GetDocument()->GetObjects();
	POSITION pos = polObList->GetHeadPosition();
	while (pos != NULL)
		Select(polObList->GetNext(pos), TRUE);
}

void FormDesignerView::OnUpdateEditSelectAll(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(GetDocument()->GetObjects()->GetCount() != 0);
}

void FormDesignerView::OnEditClear()
{
	// update all the views before the selection goes away
	GetDocument()->UpdateAllViews(NULL, HINT_DELETE_SELECTION, &omSelection);
	OnUpdate(NULL, HINT_UPDATE_SELECTION, NULL);

	// now remove the selection from the document
	POSITION pos = omSelection.GetHeadPosition();
	while (pos != NULL)
	{
		UFormControlObj* polObj = omSelection.GetNext(pos);
		GetDocument()->Remove(polObj);
		GetDocument()->RemoveCtrl(polObj->pomProperties);
		polObj->Remove();
	}
	omSelection.RemoveAll();
	if(pogPropertiesDlg != NULL)
	{
		pogPropertiesDlg->SetObjectProperties(NULL, NULL);
	}

}

void FormDesignerView::OnKeyLeft()
{
	POSITION pos = omSelection.GetHeadPosition();
	CSize delta = CSize(-1,0);
	while (pos != NULL)
	{
		UFormControlObj* polObj = omSelection.GetNext(pos);
		CRect position = polObj->omPosition;

		position += delta;
		polObj->MoveTo(position, this);

	}
}
void FormDesignerView::OnKeyRight()
{
	POSITION pos = omSelection.GetHeadPosition();
	CSize delta = CSize(1,0);
	while (pos != NULL)
	{
		UFormControlObj* polObj = omSelection.GetNext(pos);
		CRect position = polObj->omPosition;

		position += delta;
		polObj->MoveTo(position, this);
	}
}
void FormDesignerView::OnKeyArrowUp()
{
	POSITION pos = omSelection.GetHeadPosition();
	CSize delta = CSize(0,-1);
	while (pos != NULL)
	{
		UFormControlObj* polObj = omSelection.GetNext(pos);
		CRect position = polObj->omPosition;

		position += delta;
		polObj->MoveTo(position, this);
	}
}
void FormDesignerView::OnKeyDown()
{
	POSITION pos = omSelection.GetHeadPosition();
	CSize delta = CSize(0,1);
	while (pos != NULL)
	{
		UFormControlObj* polObj = omSelection.GetNext(pos);
		CRect position = polObj->omPosition;

		position += delta;
		polObj->MoveTo(position, this);
	}
}

void FormDesignerView::OnKeyUp( UINT nChar, UINT nRepCnt, UINT nFlags )
{
	POSITION pos = omSelection.GetHeadPosition();
	CSize delta = CSize(0,1);
//	UFormControlObj *polObj;
/*	if(omSelection.GetCount() == 1)
	{
		polObj = omSelection.GetTail();
		if(pogPropertiesDlg != NULL)
		{
			pogPropertiesDlg->SetObjectProperties(polObj->pomProperties, polObj);
		}
	}
*/
}


void FormDesignerView::OnUpdateAnySelect(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!omSelection.IsEmpty());
}

void FormDesignerView::OnSize(UINT nType, int cx, int cy)
{
	CScrollView::OnSize(nType, cx, cy);
	UpdateActiveItem();
}

BOOL FormDesignerView::OnEraseBkgnd(CDC*)
{
	return TRUE;
}


void FormDesignerView::OnEditCopy()
{
/*	ASSERT_VALID(this);
	ASSERT(m_cfDraw != NULL);

	// Create a shared file and associate a CArchive with it
	CSharedFile file;
	CArchive ar(&file, CArchive::store);

	// Serialize selected objects to the archive
	m_selection.Serialize(ar);
	ar.Close();

	COleDataSource* pDataSource = NULL;
	TRY
	{
		pDataSource = new COleDataSource;
		// put on local format instead of or in addation to
		pDataSource->CacheGlobalData(m_cfDraw, file.Detach());

		// if only one item and it is a COleClientItem then also
		// paste in that format
		CDrawObj* pDrawObj = m_selection.GetHead();
		if (m_selection.GetCount() == 1 &&
			pDrawObj->IsKindOf(RUNTIME_CLASS(CDrawOleObj)))
		{
			CDrawOleObj* pDrawOle = (CDrawOleObj*)pDrawObj;
			pDrawOle->m_pClientItem->GetClipboardData(pDataSource, FALSE);
		}
		pDataSource->SetClipboard();
	}
	CATCH_ALL(e)
	{
		delete pDataSource;
		THROW_LAST();
	}
	END_CATCH_ALL
*/
}

void FormDesignerView::OnUpdateEditCopy(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!omSelection.IsEmpty());
}

void FormDesignerView::OnEditCut()
{
	OnEditCopy();
	OnEditClear();
}

void FormDesignerView::OnUpdateEditCut(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!omSelection.IsEmpty());
}

void FormDesignerView::OnEditPaste()
{
/*
	COleDataObject dataObject;
	dataObject.AttachClipboard();

	// invalidate current selection since it will be deselected
	OnUpdate(NULL, HINT_UPDATE_SELECTION, NULL);
	m_selection.RemoveAll();
	if (dataObject.IsDataAvailable(m_cfDraw))
	{
		PasteNative(dataObject);

		// now add all items in m_selection to document
		POSITION pos = m_selection.GetHeadPosition();
		while (pos != NULL)
			GetDocument()->Add(m_selection.GetNext(pos));
	}
	else
		PasteEmbedded(dataObject, GetInitialPosition().TopLeft() );

	GetDocument()->SetModifiedFlag();

	// invalidate new pasted stuff
	GetDocument()->UpdateAllViews(NULL, HINT_UPDATE_SELECTION, &m_selection);
*/
}

void FormDesignerView::OnUpdateEditPaste(CCmdUI* pCmdUI)
{
/*
	// determine if private or standard OLE formats are on the clipboard
	COleDataObject dataObject;
	BOOL bEnable = dataObject.AttachClipboard() &&
		(dataObject.IsDataAvailable(m_cfDraw) ||
		 COleClientItem::CanCreateFromData(&dataObject));

	// enable command based on availability
	pCmdUI->Enable(bEnable);
*/
}

/*void FormDesignerView::OnFilePrint()
{
	CScrollView::OnFilePrint();
	GetDocument()->ComputePageSize();
}
*/
/*void FormDesignerView::OnViewShowObjects()
{
//	CDrawOleObj::c_bShowItems = !CDrawOleObj::c_bShowItems;
//	GetDocument()->UpdateAllViews(NULL, HINT_UPDATE_OLE_ITEMS, NULL);
}
*/
/*void FormDesignerView::OnUpdateViewShowObjects(CCmdUI* pCmdUI)
{
//	pCmdUI->SetCheck(CDrawOleObj::c_bShowItems);
}
*/
int FormDesignerView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CScrollView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// register drop target
	if( m_dropTarget.Register( this ) )
		return 0;
	else
		return -1;
}


BOOL FormDesignerView::GetObjectInfo(COleDataObject* pDataObject,
	CSize* pSize, CSize* pOffset)
{
/*	ASSERT(pSize != NULL);

	// get object descriptor data
	HGLOBAL hObjDesc = pDataObject->GetGlobalData(m_cfObjectDescriptor);
	if (hObjDesc == NULL)
	{
		if (pOffset != NULL)
			*pOffset = CSize(0, 0); // fill in defaults instead
		*pSize = CSize(0, 0);
		return FALSE;
	}
	ASSERT(hObjDesc != NULL);

	// otherwise, got CF_OBJECTDESCRIPTOR ok.  Lock it down and extract size.
	LPOBJECTDESCRIPTOR pObjDesc = (LPOBJECTDESCRIPTOR)GlobalLock(hObjDesc);
	ASSERT(pObjDesc != NULL);
	pSize->cx = (int)pObjDesc->sizel.cx;
	pSize->cy = (int)pObjDesc->sizel.cy;
	if (pOffset != NULL)
	{
		pOffset->cx = (int)pObjDesc->pointl.x;
		pOffset->cy = (int)pObjDesc->pointl.y;
	}
	GlobalUnlock(hObjDesc);
	GlobalFree(hObjDesc);

	// successfully retrieved pSize & pOffset info
*/
	return TRUE;
}

void FormDesignerView::OnContextMenu(CWnd* /*pWnd*/, CPoint point)
{
	// make sure window is active
	GetParentFrame()->ActivateFrame();

	CPoint local = point;
	ScreenToClient(&local);
	ClientToDoc(local);

	UFormControlObj* polObj;
	polObj = GetDocument()->ObjectAt(local);
	if(polObj != NULL)
	{
		if(!IsSelected(polObj))
			Select( polObj, FALSE );          // reselect item if appropriate
		UpdateWindow();

		CMenu menu;
		if (menu.LoadMenu(ID_POPUP_MENU))
		{
			CMenu* pPopup = menu.GetSubMenu(0);
			if(pPopup != NULL)
			{
				pPopup->TrackPopupMenu(TPM_RIGHTBUTTON | TPM_LEFTALIGN,
									   point.x, point.y,
									   AfxGetMainWnd()); // route commands through main window
			}
		}
	}
}


void FormDesignerView::OnPrint(CDC* pDC, CPrintInfo* pInfo)
{
	OnDraw(pDC);
}

////////////////////
void FormDesignerView::OnInitialUpdate()
{
	CSize size = GetDocument()->GetSize();
	CClientDC dc(NULL);
	size.cx = MulDiv(size.cx, dc.GetDeviceCaps(LOGPIXELSX), 100);
	size.cy = MulDiv(size.cy, dc.GetDeviceCaps(LOGPIXELSY), 100);
	SetScrollSizes(MM_TEXT, size);
	GetDocument()->SetTitle(GetDocument()->pomForm->omName);
/*	CScrollView::OnInitialUpdate();

	CSize sizeTotal;
	// TODO: calculate the total size of this view
	sizeTotal.cx = sizeTotal.cy = 100;
	SetScrollSizes(MM_TEXT, sizeTotal);
	*/
}

void FormDesignerView::OnDraw(CDC* pDC)
{
	FormDesignerDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	CDC dc;
	CDC* pDrawDC = pDC;
	CBitmap bitmap;
	CBitmap* pOldBitmap;

	// only paint the rect that needs repainting
	CRect client;
	pDC->GetClipBox(client);
	CRect rect = client;
	DocToClient(rect);

	if (!pDC->IsPrinting())
	{
		// draw to offscreen bitmap for fast looking repaints
		if (dc.CreateCompatibleDC(pDC))
		{
			if (bitmap.CreateCompatibleBitmap(pDC, rect.Width(), rect.Height()))
			{
				OnPrepareDC(&dc, NULL);
				pDrawDC = &dc;

				// offset origin more because bitmap is just piece of the whole drawing
				dc.OffsetViewportOrg(-rect.left, -rect.top);
				pOldBitmap = dc.SelectObject(&bitmap);
				dc.SetBrushOrg(rect.left % 8, rect.top % 8);

				// might as well clip to the same rectangle
				dc.IntersectClipRect(client);
			}
		}
	}

	// paint background
	CBrush olClientBrush;
	CBrush olFormRectBrush;
	if (!olClientBrush.CreateSolidBrush(COLORREF(RGB(100,100,100))))//pDoc->GetPaperColor()))
//	if (!brush.CreateSolidBrush(COLORREF(RGB(255,255,255))))//pDoc->GetPaperColor()))
		return;

	if (!olFormRectBrush.CreateSolidBrush(GetDocument()->pomForm->omBackColor))
		return;
	olClientBrush.UnrealizeObject();
	pDrawDC->FillRect(client, &olClientBrush);
	pDrawDC->FillRect(pDoc->pomForm->omRect, &olFormRectBrush);

	if (!pDC->IsPrinting() && bmGrid)
		DrawGrid(pDrawDC);

	pDoc->Draw(pDrawDC, this);

	if (pDrawDC != pDC)
	{
		pDC->SetViewportOrg(0, 0);
		pDC->SetWindowOrg(0,0);
		pDC->SetMapMode(MM_TEXT);
		dc.SetViewportOrg(0, 0);
		dc.SetWindowOrg(0,0);
		dc.SetMapMode(MM_TEXT);
		pDC->BitBlt(rect.left, rect.top, rect.Width(), rect.Height(),
			&dc, 0, 0, SRCCOPY);
		dc.SelectObject(pOldBitmap);
	}
}

void FormDesignerView::DrawGrid(CDC* pDC)
{
/*	FormDesignerDoc* pDoc = GetDocument();

	COLORREF oldBkColor = pDC->SetBkColor(COLORREF(RGB(192,192,192)));

	CRect rect;
	rect.left = -pDoc->GetSize().cx / 2;
	rect.top = -pDoc->GetSize().cy / 2;
	rect.right = rect.left + pDoc->GetSize().cx;
	rect.bottom = rect.top + pDoc->GetSize().cy;

	// Center lines
	CPen penDash;
	penDash.CreatePen(PS_DASH, 1, omGridColor);
	CPen* pOldPen = pDC->SelectObject(&penDash);

	pDC->MoveTo(0, rect.top);
	pDC->LineTo(0, rect.bottom);
	pDC->MoveTo(rect.left, 0);
	pDC->LineTo(rect.right, 0);

	// Major unit lines
	CPen penDot;
	penDot.CreatePen(PS_DOT, 1, omGridColor);
	pDC->SelectObject(&penDot);

	for (int x = rect.left / 100 * 100; x < rect.right; x += 100)
	{
		if (x != 0)
		{
			pDC->MoveTo(x, rect.top);
			pDC->LineTo(x, rect.bottom);
		}
	}

	for (int y = rect.top / 100 * 100; y < rect.bottom; y += 100)
	{
		if (y != 0)
		{
			pDC->MoveTo(rect.left, y);
			pDC->LineTo(rect.right, y);
		}
	}

	// Outlines
	CPen penSolid;
	penSolid.CreatePen(PS_SOLID, 1, omGridColor);
	pDC->SelectObject(&penSolid);
	pDC->MoveTo(rect.left, rect.top);
	pDC->LineTo(rect.right, rect.top);
	pDC->LineTo(rect.right, rect.bottom);
	pDC->LineTo(rect.left, rect.bottom);
	pDC->LineTo(rect.left, rect.top);

	pDC->SelectObject(pOldPen);
	pDC->SetBkColor(oldBkColor);
*/
}

void FormDesignerView::ClientToDoc(CPoint& point)
{
	CClientDC dc(this);
	OnPrepareDC(&dc, NULL);
	dc.DPtoLP(&point);

}

void FormDesignerView::ClientToDoc(CRect& rect)
{
	CClientDC dc(this);
	OnPrepareDC(&dc, NULL);
	dc.DPtoLP(rect);
//	ASSERT(rect.left <= rect.right);
//	ASSERT(rect.bottom <= rect.top);

}

void FormDesignerView::DocToClient(CPoint& point)
{

	CClientDC dc(this);
	OnPrepareDC(&dc, NULL);
	dc.LPtoDP(&point);

}

void FormDesignerView::DocToClient(CRect& rect)
{
	CClientDC dc(this);
	OnPrepareDC(&dc, NULL);
	dc.LPtoDP(rect);
	rect.NormalizeRect();

}

void FormDesignerView::InvalObj(UFormControlObj* popObj)
{
 	CRect rect = popObj->omPosition;
	DocToClient(rect);
	if (bmActive && IsSelected(popObj))
	{
		rect.left -= 4;
		rect.top -= 5;
		rect.right += 5;
		rect.bottom += 4;

	}
	rect.InflateRect(1, 1); // handles CDrawOleObj objects

	InvalidateRect(rect, FALSE);
}

void FormDesignerView::SetPageSize(CSize size)
{
	CClientDC dc(NULL);
	size.cx = MulDiv(size.cx, dc.GetDeviceCaps(LOGPIXELSX), 100);
	size.cy = MulDiv(size.cy, dc.GetDeviceCaps(LOGPIXELSY), 100);
	SetScrollSizes(MM_TEXT, size);
	GetDocument()->UpdateAllViews(NULL, HINT_UPDATE_WINDOW, NULL);
}

void FormDesignerView::Remove(UFormControlObj* popObj)
{
	POSITION pos = omSelection.Find(popObj);
	if (pos != NULL)
		omSelection.RemoveAt(pos);
}

void FormDesignerView::OnSetFocus(CWnd* pOldWnd)
{
/*	COleClientItem* pActiveItem = GetDocument()->GetInPlaceActiveItem(this);
	if (pActiveItem != NULL &&
		pActiveItem->GetItemState() == COleClientItem::activeUIState)
	{
		// need to set focus to this item if it is in the same view
		CWnd* pWnd = pActiveItem->GetInPlaceWindow();
		if (pWnd != NULL)
		{
			pWnd->SetFocus();
			return;
		}
	}
*/
	CScrollView::OnSetFocus(pOldWnd);
}

void FormDesignerView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void FormDesignerView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	CScrollView::OnBeginPrinting(pDC,pInfo);

	// check page size -- user could have gone into print setup
	// from print dialog and changed paper or orientation
	GetDocument()->ComputePageSize();
}

BOOL FormDesignerView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}


void FormDesignerView::OnUpdate(CView* , LPARAM lHint, CObject* pHint)
{
	switch (lHint)
	{
	case HINT_UPDATE_WINDOW:    // redraw entire window
		Invalidate(FALSE);
		break;

	case HINT_UPDATE_DRAWOBJ:   // a single object has changed
		InvalObj((UFormControlObj*)pHint);
		break;

	case HINT_UPDATE_SELECTION: // an entire selection has changed
		{
			UFormControlObjList* pList = pHint != NULL ?
				(UFormControlObjList*)pHint : &omSelection;
			POSITION pos = pList->GetHeadPosition();
			while (pos != NULL)
				InvalObj(pList->GetNext(pos));
		}
		break;

	case HINT_DELETE_SELECTION: // an entire selection has been removed
		if (pHint != &omSelection)
		{
			UFormControlObjList* pList = (UFormControlObjList*)pHint;
			POSITION pos = pList->GetHeadPosition();
			while (pos != NULL)
			{
				UFormControlObj* pObj = pList->GetNext(pos);
				InvalObj(pObj);
				Remove(pObj);   // remove it from this view's selection
			}
		}
		break;

	default:
		ASSERT(FALSE);
		break;
	}
}

void FormDesignerView::OnActivateView(BOOL bActivate, CView* pActiveView,
	CView* pDeactiveView)
{
	CView::OnActivateView(bActivate, pActiveView, pDeactiveView);

	// invalidate selections when active status changes
	if (bmActive != bActivate)
	{
		if (bActivate)  // if becoming active update as if active
			bmActive = bActivate;
		if (!omSelection.IsEmpty())
			OnUpdate(NULL, HINT_UPDATE_SELECTION, NULL);
		bmActive = bActivate;
	}
}
void FormDesignerView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo)
{
	CScrollView::OnPrepareDC(pDC, pInfo);

	// mapping mode is MM_ANISOTROPIC
	// these extents setup a mode similar to MM_LOENGLISH
	// MM_LOENGLISH is in .01 physical inches
	// these extents provide .01 logical inches

	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetViewportExt(pDC->GetDeviceCaps(LOGPIXELSX),
		pDC->GetDeviceCaps(LOGPIXELSY));
//	pDC->SetWindowExt(100, -100);
	pDC->SetWindowExt(100, 100);

	// set the origin of the coordinate system to the center of the page
	CPoint ptOrg;
	ptOrg.x = GetDocument()->GetSize().cx / 2;
	ptOrg.y = GetDocument()->GetSize().cy / 2;
	// ptOrg is in logical coordinates
	pDC->SetWindowOrg(0,0);
//	pDC->OffsetWindowOrg(-ptOrg.x,ptOrg.y);

}

BOOL FormDesignerView::OnScrollBy(CSize sizeScroll, BOOL bDoScroll)
{
	// do the scroll
	if (!CScrollView::OnScrollBy(sizeScroll, bDoScroll))
		return FALSE;

	// update the position of any in-place active item
	if (bDoScroll)
	{
		UpdateActiveItem();
		UpdateWindow();
	}
	return TRUE;
}

BOOL FormDesignerView::IsSelected(const CObject* pDocItem) const
{
	UFormControlObj* pDrawObj = (UFormControlObj*)pDocItem;
	return omSelection.Find(pDrawObj) != NULL;
}

BOOL FormDesignerView::PreCreateWindow(CREATESTRUCT& cs)
{
	ASSERT(cs.style & WS_CHILD);
	if (cs.lpszClass == NULL)
		cs.lpszClass = AfxRegisterWndClass(CS_DBLCLKS);
	return TRUE;
}


void FormDesignerView::Select(UFormControlObj* pObj, BOOL bAdd)
{
	if (!bAdd)
	{
		OnUpdate(NULL, HINT_UPDATE_SELECTION, NULL);
		omSelection.RemoveAll();
	}

	if (pObj == NULL || IsSelected(pObj))
		return;

	omSelection.AddTail(pObj);
	InvalObj(pObj);
}

void FormDesignerView::UpdateActiveItem()
{
/*	COleClientItem* pActiveItem = GetDocument()->GetInPlaceActiveItem(this);
	if (pActiveItem != NULL &&
		pActiveItem->GetItemState() == COleClientItem::activeUIState)
	{
		// this will update the item rectangles by calling
		//  OnGetPosRect & OnGetClipRect.
		pActiveItem->SetItemRects();
	}
*/
}

void FormDesignerView::CloneSelection()
{
	POSITION pos = omSelection.GetHeadPosition();
	while (pos != NULL)
	{
		UFormControlObj* pObj = omSelection.GetNext(pos);
		pObj->Clone(pObj->pomDocument, pObj);
			// copies object and adds it to the document
	}
}

void FormDesignerView::Deselect(UFormControlObj* pObj)
{
	POSITION pos = omSelection.Find(pObj);
	if (pos != NULL)
	{
		InvalObj(pObj);
		omSelection.RemoveAt(pos);
	}
}


void FormDesignerView::SelectWithinRect(CRect rect, BOOL bAdd)
{
	if (!bAdd)
		Select(NULL);

	ClientToDoc(rect);

	UFormControlObjList* pObList = GetDocument()->GetObjects();
	POSITION posObj = pObList->GetHeadPosition();
	while (posObj != NULL)
	{
		UFormControlObj* pObj = pObList->GetNext(posObj);
		if (pObj->Intersects(rect))
			Select(pObj, TRUE);
	}
}


/////////////////////////////////////////////////////////////////////////////
// FormDesignerView diagnostics

#ifdef _DEBUG
void FormDesignerView::AssertValid() const
{
	CScrollView::AssertValid();
}

void FormDesignerView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// FormDesignerView message handlers

void FormDesignerView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	GetParentFrame()->ActivateFrame();

	CPoint local = point;
	ScreenToClient(&local);
	ClientToDoc(local);

	UFormControlObj* polObj;
	polObj = GetDocument()->ObjectAt(local);
	if (polObj == NULL) // Must be the background
	{
		//Show Dlg for activation order
		//GetDocument()->pomForm
		ActivationOrderDlg *polDlg = new ActivationOrderDlg(this, GetDocument()->pomForm);
		polDlg->DoModal();
		delete polDlg;
	}
}
