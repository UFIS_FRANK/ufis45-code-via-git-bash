#if !defined(AFX_TABLELISTFRAMEWND_H__8EF758C6_2D57_11D3_A1C7_0000B4984BBE__INCLUDED_)
#define AFX_TABLELISTFRAMEWND_H__8EF758C6_2D57_11D3_A1C7_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TableListFrameWnd.h : header file
//

#include "UDragDropListBox.h"

/////////////////////////////////////////////////////////////////////////////
// TableListFrameWnd frame

class TableListFrameWnd : public CFrameWnd
{
	DECLARE_DYNCREATE(TableListFrameWnd)
protected:
	TableListFrameWnd();
// Attributes
public:
	TableListFrameWnd(CWnd *pParent, CString opTable);           // protected constructor used by dynamic creation

	CWnd *pomParent;
	CString omTable;
// Operations
public:

	//CListBox *pomList;
	UDragDropListBox *pomList;

	CString GetName(){return omTable;};

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(TableListFrameWnd)
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~TableListFrameWnd();
	void OnDeleteTable();

	// Generated message map functions
	//{{AFX_MSG(TableListFrameWnd)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMove(int x, int y);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnClose();
	afx_msg LONG OnBeginDrag(UINT wParam, LONG lParam);
	afx_msg LONG OnDragOver(UINT wParam, LONG lParam);
	afx_msg LONG OnDrop(UINT wParam, LONG lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TABLELISTFRAMEWND_H__8EF758C6_2D57_11D3_A1C7_0000B4984BBE__INCLUDED_)
