#if !defined(AFX_COMBUTTONSINK_H__8FCDE201_894B_11D4_904C_00010204AA51__INCLUDED_)
#define AFX_COMBUTTONSINK_H__8FCDE201_894B_11D4_904C_00010204AA51__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *	UWorkBench - Project
 *	
 *		Button Sink class
 *		
 *
 *	ABB Airport Technologies GmbH 2000
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *
 *		Date		Author			Comment
 *		-------------------------------------------------------------
 *
 *		02/11/00	cla AAT/IR		Initial version
 *		14/11/00	cla AAT/ID		adapted version for Workbench
 *									Scripthost, RunTimeCtrl ...
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

//#include <INITGUID.H>
//#include "GenGuids.h"
//#include ComButtonSink.h : header file
#include "ScriptHost.h"
#include "RunTimeCtrl.h"

/*
 *  D E F I N E S
 */
/*
 * This Part String will form the Event Extension for the VB Sub
 */
#define CLICK_EVENT_EXTENSION _T("_Click")
//
/*----------------------------------------------------------------------------*/

/* From import directive
 * IID IID_IComButtonSink =uuid("7b020ec1-af6c-11ce-9f46-00aa00574a4f");
 */
//DEFINE_GUID(IID_IComButtonSink,0x7b020ec1,0xaf6c,0x11ce,0x9f,0x46,0x0,0xaa,0x0,0x57,0x4a,0x4f);
const GUID CDECL IID_IComButtonSink = {0x7b020ec1,0xaf6c,0x11ce,{0x9f,0x46,0x0,0xaa,0x0,0x57,0x4a,0x4f}};
/*----------------------------------------------------------------------------*/

/////////////////////////////////////////////////////////////////////////////
// CComButtonSink command target
#include "ConnectionAdvisor.h"
//class CScript2View;
class CComButtonSink : public CCmdTarget
{
	DECLARE_DYNCREATE(CComButtonSink)


	CComButtonSink();           // protected constructor used by dynamic creation
	virtual ~CComButtonSink();

// Attributes
public:
	CScriptHost* m_scriptHost;
	RunTimeCtrl *pomRunCtrl;
// Operations
public:
	BOOL Advise(IUnknown* pSource, REFIID iid);
	BOOL Unadvise(REFIID iid);
	void SetCtrl(RunTimeCtrl* pRunCtrl);
	void SetScriptHost(CScriptHost* pScriptHost);
	void SetAlias(CString& alias);
	
	/*
	 * The user visisble name is stored in this variable
	 */

	CString m_cntlAlias;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CComButtonSink)
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CComButtonSink)
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CUFISAmSink)
	afx_msg void OnClick();
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
private:
	CConnectionAdvisor m_ComButtonEventsAdvisor;
//	CScript2View* m_pScript2View;
	void DispatchEvent2VB(DISPID dispId);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMBUTTONSINK_H__8FCDE201_894B_11D4_904C_00010204AA51__INCLUDED_)
