#ifndef __USCRIPT_H__
#define __USCRIPT_H__

// UScript.h

// Classes for UScript-Language


/*******************************************************************************
 Function
*******************************************************************************/
class UUserFunction : public CObject
{
public:
	UUserFunction():CObject(){;}
	~UUserFunction(){;}

	CString			omName;					// Name of function
	CStringArray	omParams;				// Values of parameters
	CStringArray    omParaTypes;			// Types of parameters
	CStringArray	omCodeLines;			// Lines of code

	CString			omReturnType;			// Type of the returnvalue
};

/*******************************************************************************
 Internal Function
*******************************************************************************/
class UInternalFuncs : public CObject
{
public:
	UInternalFuncs():CObject(){;}
	~UInternalFuncs();

	CString			omName;					// Name of function
	CStringArray	omParams;				// Values of parameters
	CStringArray    omParaTypes;			// Types of parameters
	CString			omReturnType;			// Type of the returnvalue
};

/********************************************************************************
	last return object
	depending on the return typ ==> user is able to cast into the correct pointer

	each function call sets the
	 - return value pointer
	 - the return value type
********************************************************************************/
extern void	   *pogLUFRV;		//global object, which contains the (l)ast (u)ser (f)unction (r)eturn (v)alue;
extern CString ogLUFRT;			//global object, which contains the (l)ast (u)ser (f)unction (r)eturn (t)ype;
extern void	   *ogLIFRV;		//global object, which contains the (l)ast (i)nternal (f)unction (r)eturn (v)alue;
extern CString ogLIFRT;			//global object, which contains the (l)ast (i)nternal (f)unction (r)eturn (t)ype;

typedef CTypedPtrList<CObList, UFunctionDef*>	UUserFuncs;
typedef CTypedPtrList<CObList, UUserFunction*> UIntFuncs;

#endif //__USCRIPT_H__