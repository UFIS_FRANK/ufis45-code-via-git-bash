#if !defined(AFX_UUFISOBJECTSMDI_H__0408F144_2E00_11D3_B07D_00001C019205__INCLUDED_)
#define AFX_UUFISOBJECTSMDI_H__0408F144_2E00_11D3_B07D_00001C019205__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UUFisObjectsMDI.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// UUFisObjectsMDI frame

class UUFisObjectsMDI : public CMDIChildWnd
{
	DECLARE_DYNCREATE(UUFisObjectsMDI)
protected:
	UUFisObjectsMDI();           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(UUFisObjectsMDI)
	protected:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~UUFisObjectsMDI();

	// Generated message map functions
	//{{AFX_MSG(UUFisObjectsMDI)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UUFISOBJECTSMDI_H__0408F144_2E00_11D3_B07D_00001C019205__INCLUDED_)
