#ifndef __CTRL_PROPERTIES__
#define __CTRL_PROPERTIES__
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *	UWorkBench - Project
 *	
 *		Property wrapper calsses   
 *		
 *
 *	ABB Airport Technologies GmbH 2000
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *
 *		Date		Author			Comment
 *		-------------------------------------------------------------
 *
 *		??/??/??	mwo	AAT/IR		Initial version
 *		24/10/00	cla AAT/IR		Support for events added
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

extern CedaBasicData ogBCD;
#include <vector>
#include "CntlEvent.h"

#pragma warning( disable : 4786 )

/*
 *   D E F I N E S
 */
// For Events in vector class

#define INITIAL_SIZE 5
/*
 *   D E F I N E
 *   Size of EventDesc - Data structure
 */

#define EV_SIZE_OF(a) (sizeof (a) / sizeof(EventDesc)); 

struct EventDesc
{
	CString EventName;
	DISPID EventId;
};
//-****************************************************************************************
//
//-****************************************************************************************
class UFormField : public CObject
{
public:
	UFormField() : CObject()
	{;}
	~UFormField(){;}
	CString Table,						// Tablename
		    Field,						// Fieldname
			Type,						// Datatype
			Header,						// Used as lablename
			Visible,					// Field visible/invisible
			Key,						// For the moment REFE from SYSTAB ==> may be changed later
			Refe,						// Reference Field (e.g. for lookup comboboxes)
			HiddenRefe,					// Hidden refe field
			Reqf;						// Required Field ==> Mandatory


	UFormField(const UFormField &opO)
	{
		this->Table     = opO.Table;
		this->Field		= opO.Field	;
		this->Type		= opO.Type	;
		this->Header	= opO.Header;
		this->Visible   = opO.Visible;
		this->Key		= opO.Key	;
	}
	const UFormField &operator =(const UFormField &opO) 
	{
 		this->Table     = opO.Table;
		this->Field		= opO.Field	;
		this->Type		= opO.Type	;
		this->Header	= opO.Header;
		this->Visible   = opO.Visible;
		this->Key		= opO.Key	;
		return *this;
	}

};
/*****************************************************************************
 The following logical datatype are supported:
 =============================================

	DATE		: Date formated ("dd.mm.YYYY")
	TIME		: Time formated ("HH:MM")
	DTIM		: Date and time
	TRIM		: Both side trimed string
	LONG		: Number
	BOOL		: This value will be saved with TRUE='1' and FALSE='0'
				  if omCheckedValue is defined ==> TRUE=omTrueValue and FALSE = ' '
	URNO		: Specifies the db-field as URNO

	//--- special time formatting types
	MIN2HHMM	: DB -> 90min == GUI -> 01:30 or 0130
	MIN2HH		: DB -> 90min == GUI -> 1.5h
	HHMM2MIN	: DB -> 01:30 or 01300 == GUI 90min
	HHMM2HH		: DB -> 01:30 or 01300 == GUI -> 1.5h
	HH2MIN		: DB -> 1.5h == GUI -> 90min
	HH2HHMM		: DB -> 1.5h == 01:30 or 0130

//---- Not yet supported
	LSTU	: Last update date
	CDAT	: Creation date of the record
	REAL	: Floatingpoint number
	CHAR	: Right side trimed string
	USER	: User last update
	CUSR	: User Createor
	NMBR	: Number filled with leading '0'
	WDAY	: Weekday 1234567 (1 is monday)
	SLST	: (Not yet) Combobox is used
	MLST	: (Not yet) Checklist combobox is used

  The following formatting sequences are supported:
  =================================================
  A) Simple commands
	getitem <NO> <sepa>						// nst word e.g.: "getitem 5 ,"
	setitem <string> <NO> <Separator>		// nst word e.g.: "getitem hallo 5 ,"
	setstring <string> <Frompos> <Count>	// e.g. setstring hallo 8 4 => if stringlen < 12 ==> fill with leading blanks pos 0-7
	getstring <Frompos> <Count>				// e.g. getstring  8 4
	string_to_time <string>					// e.g. string_to_time 1130
	string_to_date <string>					// e.g. string_to_date 19991010 ==> 10.10.1999
	string_to_dtim <string>					// e.g. string_to_dtim 19991010113000 ==> 10.10.1999 11:30
	time_to_string <time>
	date_to_string <date>
	asnumber <string>
	asreal <string>
	asdate <string>
	astime <string>
	asdtim <string>

  B) Complex commands
	getitem <NO> <sepa> | asnumber <string>

*****************************************************************************/
class UCtrlProperties : public CObject
{
public:
	UCtrlProperties():CObject()
	{//Preinitialize default values
		omCtrlType		= CString("NULL");		
		omLabel			= CString("Label");		
		omDBField		= CString("");
		omName			= CString("");			
		omOrientation	= CString("LEFT");	
		omRect			= CRect(0, 0, 23, 23);
		omBackColor		= COLORREF(RGB(192,192,192));
		omFontColor		= COLORREF(RGB(0,0,0));
		omBorderColor	= COLORREF(RGB(0,0,0));
		bmEnabled		= TRUE;
		bmVisible		= TRUE;
		imBorderWidth   = 1;
		bmDBField		= TRUE;
		imTextLimit		= 999999;		// Max. chars for entry
		bmMultiLine		= FALSE;		// for "edit" fields
		bmModal			= FALSE;
		//omDefaultValues;	// e.g. fixed values for the combobox
		omCheckedValue		= CString("");		// for various true values in the database
										// 0/1, Y/N, J/N, X etc.
		bmMandatory		= FALSE;		// Default from SYSTAB but overridable for customers
		omMasterName	= CString("");		// if the field is a child ==> set the mastername to identify the
										// master (parent)
		bmIsMaster		= FALSE;		// For radiobuttons
		omEventList.reserve(INITIAL_SIZE);

	}
	UCtrlProperties(const UCtrlProperties &opO):CObject()
	{
		int i ;
		this->omCtrlType		= opO.omCtrlType	;
		this->omLabel			= opO.omLabel		;
		this->omName			= opO.omName		;
		this->omDBField			= opO.omDBField;
		this->omOrientation	= opO.omOrientation;
		this->omRect			= opO.omRect		;
		this->omBackColor		= opO.omBackColor	;
		this->omFontColor		= opO.omFontColor	;
		this->omBorderColor	= opO.omBorderColor;
		this->bmEnabled			= opO.bmEnabled;
		this->bmVisible			= opO.bmVisible;
		this->imBorderWidth     = opO.imBorderWidth;
		this->bmDBField			= opO.bmDBField;
		this->omFieldType		= opO.omFieldType;
		this->omFormatString	= opO.omFormatString;
		this->omPropLabels.RemoveAll();
		for( i = 0; i < opO.omPropLabels.GetSize(); i++)
		{
			this->omPropLabels.Add(opO.omPropLabels[i]);
		}

		this->imTextLimit		= opO.imTextLimit;
		this->bmMultiLine		= opO.bmMultiLine;
		this->omDefaultValues.RemoveAll();
		for(i = 0; i < opO.omDefaultValues.GetSize(); i++)
		{
			this->omDefaultValues.Add(opO.omDefaultValues[i]);
		}
		this->omCheckedValue		= opO.omCheckedValue;
		this->bmMandatory		= opO.bmMandatory;
		this->omMasterName		= opO.omMasterName;
		this->bmIsMaster = opO.bmIsMaster;
		this->omValidationRule = opO.omValidationRule;
		this->Refe			= opO.Refe;
		this->HiddenRefe    = opO.HiddenRefe;
//-----------Grid
		this->omGridTable = opO.omGridTable;
		this->omMasterDetail = opO.omMasterDetail;
		this->omFieldlist.RemoveAll();
		this->omTypelist.RemoveAll();
		this->omHaederlist.RemoveAll();
		this->omVisiblelist.RemoveAll();
		this->omLookupfieldList.RemoveAll();
		this->omMandatorylist.RemoveAll();
		this->omHeadersizes.RemoveAll();
		for(i = 0; i < opO.omFieldlist.GetSize(); i++)
			this->omFieldlist.Add(opO.omFieldlist[i]);
		for( i = 0; i < opO.omTypelist.GetSize(); i++)
			this->omTypelist.Add(opO.omTypelist[i]);
		for( i = 0; i < opO.omHaederlist.GetSize(); i++)
			this->omHaederlist.Add(opO.omHaederlist[i]);
		for( i = 0; i < opO.omVisiblelist.GetSize(); i++)
			this->omVisiblelist.Add(opO.omVisiblelist[i]);
		for( i = 0; i < opO.omLookupfieldList.GetSize(); i++)
			this->omLookupfieldList.Add(opO.omLookupfieldList[i]);
		for( i = 0; i < opO.omMandatorylist.GetSize(); i++)
			this->omMandatorylist.Add(opO.omMandatorylist[i]);
		for( i = 0; i < opO.omHeadersizes.GetSize(); i++)
			this->omHeadersizes.Add(opO.omHeadersizes[i]);

		omEventList.reserve(INITIAL_SIZE);

	}
	const UCtrlProperties &operator =(const UCtrlProperties &opO) 
	{
		int i ;
		this->omCtrlType		= opO.omCtrlType	;
		this->omLabel			= opO.omLabel		;
		this->omName			= opO.omName		;
		this->omDBField			= opO.omDBField	;
		this->omOrientation		= opO.omOrientation;
		this->omRect			= opO.omRect		;
		this->omBackColor		= opO.omBackColor	;
		this->omFontColor		= opO.omFontColor	;
		this->omBorderColor		= opO.omBorderColor;
		this->bmEnabled			= opO.bmEnabled;
		this->bmVisible			= opO.bmVisible;
		this->imBorderWidth     = opO.imBorderWidth;
		this->bmDBField			= opO.bmDBField;
		this->omFieldType		= opO.omFieldType;
		this->omFormatString	= opO.omFormatString;
		this->omPropLabels.RemoveAll();
		for( i = 0; i < opO.omPropLabels.GetSize(); i++)
		{
			this->omPropLabels.Add(opO.omPropLabels[i]);
		}
		this->imTextLimit		= opO.imTextLimit;
		this->bmMultiLine		= opO.bmMultiLine;
		this->omDefaultValues.RemoveAll();
		for(i = 0; i < opO.omDefaultValues.GetSize(); i++)
		{
			this->omDefaultValues.Add(opO.omDefaultValues[i]);
		}
		this->omCheckedValue		= opO.omCheckedValue;
		this->bmMandatory		= opO.bmMandatory;
		this->omMasterName		= opO.omMasterName;
		this->bmIsMaster = opO.bmIsMaster;
		this->omValidationRule = opO.omValidationRule;
		this->Refe			= opO.Refe;
		this->HiddenRefe    = opO.HiddenRefe;

		//-----------Grid
		this->omGridTable = opO.omGridTable;
		this->omMasterDetail = opO.omMasterDetail;
		this->omFieldlist.RemoveAll();
		this->omTypelist.RemoveAll();
		this->omHaederlist.RemoveAll();
		this->omVisiblelist.RemoveAll();
		this->omLookupfieldList.RemoveAll();
		this->omMandatorylist.RemoveAll();
		this->omHeadersizes.RemoveAll();
		for( i = 0; i < opO.omFieldlist.GetSize(); i++)
			this->omFieldlist.Add(opO.omFieldlist[i]);
		for( i = 0; i < opO.omTypelist.GetSize(); i++)
			this->omTypelist.Add(opO.omTypelist[i]);
		for( i = 0; i < opO.omHaederlist.GetSize(); i++)
			this->omHaederlist.Add(opO.omHaederlist[i]);
		for( i = 0; i < opO.omVisiblelist.GetSize(); i++)
			this->omVisiblelist.Add(opO.omVisiblelist[i]);
		for( i = 0; i < opO.omLookupfieldList.GetSize(); i++)
			this->omLookupfieldList.Add(opO.omLookupfieldList[i]);
		for( i = 0; i < opO.omMandatorylist.GetSize(); i++)
			this->omMandatorylist.Add(opO.omMandatorylist[i]);
		for( i = 0; i < opO.omHeadersizes.GetSize(); i++)
			this->omHeadersizes.Add(opO.omHeadersizes[i]);

		return *this;
	}
	CString		omCtrlType;		// "button", "edit", "checkbox", "combobox", "static", 
								// "line", "rectangle", "radio", "group", "listbox", "grid"
	CString		omLabel;		// Beschriftung
	CString     omName;			// quasi die ID des Controls
	CString		omDBField;		// connection to a database field.Format: "TAB.FIELD"
	CString     omOrientation;	// LEFT,RIGHT,CENTER ==> linsb�ndig, rechtsb�ndig, zentriert
	CString     omFieldType;	// TRIM,DTIM,BOOL,DATE,TIME,INT(=LONG),DOUBLE,FORMAT,NONE
	CString		omFormatString; // IF omFieldType == FORMAT, the formatstring for CCSEdit
								// but as a regular expression !!!!!!
	CRect		omRect;
	COLORREF	omBackColor,
				omFontColor,
				omBorderColor;
	BOOL		bmEnabled;
	BOOL		bmVisible;
	int			imBorderWidth;
	CStringArray omPropLabels;	//F�r die Anzeige (Label) im Property-Grid
	CStringArray omEventLabels;	//Text for Labels in the Event page of Property dialog
	BOOL		bmDBField;
	BOOL		bmModal;		// F�r Forms modal oder modless

	int			imTextLimit;		// Max. chars for entry
	BOOL		bmMultiLine;		// for "edit" fields
	CStringArray omDefaultValues;	// e.g. fixed values for the combobox
	CString		omCheckedValue;		// for various true values in the database
									// 0/1, Y/N, J/N, X etc.
	BOOL		bmMandatory;		// Default from SYSTAB but overridable for customers
	CString		omMasterName;		// if the field is a child ==> set the mastername to identify the
									// master (parent)
	BOOL		bmIsMaster;			// for radiobuttons

	CString		omValidationRule;	// Validation-Rule ( can be defined by a rule-editor )
									// e.g. 
									// Field:	LTF FLD			==> Less than			<
									//          LEF FLD			==> Less Equal			<=
									//          GTF FLD			==> Greater				>
									//          GEF FLD			==> Greater Equal		>=
									//          EQF FLD			==> Equal				==
									//          BWF FLD1 FLD2	==> BETWEEN F1 AND F2
									//          NEF FLD			==> Not Equal			!=
									// Values	LTV Val			==> Less than			<
									//          LEV Val			==> Less Equal			<=
									//          GTV Val			==> Greater				>
									//          GEV Val			==> Greater Equal		>=
									//          EQV Val			==> Equal				==
									//          BWV Val1 Val2	==> BETWEEN V1 AND V2
									//          NEV Val			==> Not Equal			!=

	CString Refe;		// Refe
	CString HiddenRefe;  // Hidden Reference

//-----Grid
	CString		 omGridTable;
	CString		 omMasterDetail;
	CStringArray omFieldlist;	
	CStringArray omTypelist;
	CStringArray omHaederlist;
	CStringArray omVisiblelist; 
	CStringArray omLookupfieldList;
	CStringArray omMandatorylist;
	CStringArray omHeadersizes;
	std::vector<CCntlEvent> omEventList;
	EventDesc omEvents[100];
	int imNumEvents;
};
//-****************************************************************************************
//
//-****************************************************************************************
class UEditCtrlProperties : public UCtrlProperties
{
public:
	UEditCtrlProperties():UCtrlProperties()
	{//Preinitialize default values
		omName			= CString("Edit");
		omBackColor     = COLORREF(RGB(255,255,255));
		omPropLabels.Add("Not supported");
		omPropLabels.Add("Name");
		omPropLabels.Add("DB-Field");		
		omPropLabels.Add("Type");
		omPropLabels.Add("Regular Expression");
		omPropLabels.Add("Orientation");	
		omPropLabels.Add("Left");
		omPropLabels.Add("Top");
		omPropLabels.Add("Width");
		omPropLabels.Add("Height");
		omPropLabels.Add("BackColor");
		omPropLabels.Add("FontColor");
		omPropLabels.Add("BorderColor");
		omPropLabels.Add("Enabled");
		omPropLabels.Add("Visible");
		omPropLabels.Add("BorderWidth");
		omPropLabels.Add("TextLimit");
		omPropLabels.Add("Mandatory");
		omPropLabels.Add("MultiLine");
		omPropLabels.Add("MasterName");
		omEvents[0].EventName = "LButtonDown";
		omEvents[0].EventId = 2;
		omEvents[1].EventName = "KillFocus";
		omEvents[1].EventId = 3;
		imNumEvents = 2;

		omCtrlType = CString("edit");

	}
	UEditCtrlProperties(const UEditCtrlProperties &opO)
		: UCtrlProperties((UCtrlProperties)opO)
	{
	}
	const UEditCtrlProperties &operator =(const UEditCtrlProperties &opO) 
	{
		(UCtrlProperties)*this = (UCtrlProperties)opO;
		return *this;
	}

	CString omType;		// string,date,time,int,double
	CString omFormat;	// formats see CCSEdit.h
};
//-****************************************************************************************
//
//-****************************************************************************************
class UStaticCtrlProperties : public UCtrlProperties
{
public:
	UStaticCtrlProperties():UCtrlProperties()
	{//Preinitialize default values
		omName			= CString("Static");
		omBorderColor = COLORREF(RGB(172,172,172));
		bmEnabled = TRUE;
		omPropLabels.Add("Label");
		omPropLabels.Add("Name");
		omPropLabels.Add("DB-Field");		
		omPropLabels.Add("Orientation");	
		omPropLabels.Add("Left");
		omPropLabels.Add("Top");
		omPropLabels.Add("Width");
		omPropLabels.Add("Height");
		omPropLabels.Add("BackColor");
		omPropLabels.Add("FontColor");
		omPropLabels.Add("BorderColor");
		omPropLabels.Add("Visible");
		omPropLabels.Add("BorderWidth");
		omCtrlType = CString("static");
	}
	UStaticCtrlProperties(const UStaticCtrlProperties &opO)
		: UCtrlProperties((UCtrlProperties)opO)
	{
		;
	}
	const UStaticCtrlProperties &operator =(const UStaticCtrlProperties &opO) 
	{
		(UCtrlProperties)*this = (UCtrlProperties)opO;
		return *this;
	}

};
//-****************************************************************************************
//
//-****************************************************************************************
class UCheckboxCtrlProperties : public UCtrlProperties
{
public:
	UCheckboxCtrlProperties():UCtrlProperties()
	{//Preinitialize default values
		omName			= CString("Checkbox");
		omCheckedValue		= CString("1");		// for various true values in the database
		omPropLabels.Add("Label");
		omPropLabels.Add("Name");
		omPropLabels.Add("DB-Field");		
		omPropLabels.Add("Orientation");	
		omPropLabels.Add("Left");
		omPropLabels.Add("Top");
		omPropLabels.Add("Width");
		omPropLabels.Add("Height");
		omPropLabels.Add("BackColor");
		omPropLabels.Add("FontColor");
		omPropLabels.Add("BorderColor");
		omPropLabels.Add("Enabled");
		omPropLabels.Add("Visible");
		omPropLabels.Add("BorderWidth");
		omPropLabels.Add("True Value");
		omCtrlType = CString("checkbox");
	}
	UCheckboxCtrlProperties(const UCheckboxCtrlProperties &opO)
		: UCtrlProperties((UCtrlProperties)opO)
	{
		;
	}
};
//-****************************************************************************************
//
//-****************************************************************************************
class UComboboxCtrlProperties : public UCtrlProperties
{
public:
	UComboboxCtrlProperties():UCtrlProperties()
	{//Preinitialize default values
		omBackColor     = COLORREF(RGB(255,255,255));
		omName			= CString("Combobox");
		omPropLabels.Add("Label");
		omPropLabels.Add("Name");
		omPropLabels.Add("DB-Field");		
		omPropLabels.Add("Orientation");	
		omPropLabels.Add("Left");
		omPropLabels.Add("Top");
		omPropLabels.Add("Width");
		omPropLabels.Add("Height");
		omPropLabels.Add("BackColor");
		omPropLabels.Add("FontColor");
		omPropLabels.Add("BorderColor");
		omPropLabels.Add("Enabled");
		omPropLabels.Add("Visible");
		omPropLabels.Add("BorderWidth");
		omPropLabels.Add("TextLimit");
		omPropLabels.Add("Mandatory");
		omPropLabels.Add("DefaultValues Sepa.: ';'");
		omPropLabels.Add("Reference Field <Tab.Fld>");
		omPropLabels.Add("Hidden Reference Field <Tab.Fld>");
		omCtrlType = CString("combobox");
	}
	UComboboxCtrlProperties(const UComboboxCtrlProperties &opO)
		: UCtrlProperties((UCtrlProperties)opO)
	{
		;
	}
	const UComboboxCtrlProperties &operator =(const UComboboxCtrlProperties &opO) 
	{
		(UCtrlProperties)*this = (UCtrlProperties)opO;
		return *this;
	}
	CString	omType;		//edit, listonly
};
//-****************************************************************************************
//
//-****************************************************************************************
class UButtonCtrlProperties : public UCtrlProperties
{
public:

	//static int imEventsSize = EV_SIZE_OF(omEvents);

	UButtonCtrlProperties():UCtrlProperties()
	{//Preinitialize default values
		omName			= CString("Button");
		omLabel			= CString("Button");
		omOrientation = CString("CENTER");
//		omBackColor = COLORREF(RGB(192,192,192));
//		omFontColor = COLORREF(RGB(0,0,0));
		omPropLabels.Add("Label");
		omPropLabels.Add("Name");
		omPropLabels.Add("DB-Field");		
		omPropLabels.Add("Orientation");	
		omPropLabels.Add("Left");
		omPropLabels.Add("Top");
		omPropLabels.Add("Width");
		omPropLabels.Add("Height");
		omPropLabels.Add("BackColor");
		omPropLabels.Add("FontColor");
		omPropLabels.Add("BorderColor");
		omPropLabels.Add("Enabled");
		omPropLabels.Add("Visible");
		omPropLabels.Add("BorderWidth");
		omCtrlType = CString("button");
		omEventLabels.Add("Clicked");
		omEvents[0].EventName = "Click";
		omEvents[0].EventId = 0xfffffda8;
		omEvents[1].EventName = "Bla_Bla";
		omEvents[1].EventId = 423;
		imNumEvents = 2;
	}
	UButtonCtrlProperties(const UButtonCtrlProperties &opO)
		: UCtrlProperties((UCtrlProperties)opO)
	{
		;
	}
	const UButtonCtrlProperties &operator =(const UButtonCtrlProperties &opO) 
	{
		(UCtrlProperties)*this = (UCtrlProperties)opO;
		return *this;
	}
};

//-****************************************************************************************
//
//-****************************************************************************************
class UGroupCtrlProperties : public UCtrlProperties
{
public:
	UGroupCtrlProperties():UCtrlProperties()
	{//Preinitialize default values
		omName			= CString("Groupbox");
		omLabel			= CString("Groupbox");
		omOrientation = CString("CENTER");
//		omBackColor = COLORREF(RGB(192,192,192));
//		omFontColor = COLORREF(RGB(0,0,0));
		omPropLabels.Add("Label");
		omPropLabels.Add("Name");
		omPropLabels.Add("DB-Field");		
		omPropLabels.Add("Orientation");	
		omPropLabels.Add("Left");
		omPropLabels.Add("Top");
		omPropLabels.Add("Width");
		omPropLabels.Add("Height");
		omPropLabels.Add("BackColor");
		omPropLabels.Add("FontColor");
		omPropLabels.Add("BorderColor");
		omPropLabels.Add("Enabled");
		omPropLabels.Add("Visible");
		omPropLabels.Add("BorderWidth");
		omCtrlType = CString("group");
	}
	UGroupCtrlProperties(const UButtonCtrlProperties &opO)
		: UCtrlProperties((UCtrlProperties)opO)
	{
		;
	}
	const UGroupCtrlProperties &operator =(const UGroupCtrlProperties &opO) 
	{
		(UCtrlProperties)*this = (UCtrlProperties)opO;
		return *this;
	}
};



//-****************************************************************************************
//
//-****************************************************************************************
class UGridCtrlProperties : public UCtrlProperties
{
public:
	UGridCtrlProperties():UCtrlProperties()
	{//Preinitialize default values
		omName			= CString("Grid");
		omOrientation = CString("LEFT");
		omBackColor     = COLORREF(RGB(255,255,255));
		omPropLabels.Add("Name");
		omPropLabels.Add("DB-Table");
		omPropLabels.Add("Masterreference");
		omPropLabels.Add("Fieldlist");
		omPropLabels.Add("Typelist");
		omPropLabels.Add("Haederlist");
		omPropLabels.Add("Visiblelist");
		omPropLabels.Add("Lookupfield-List");//REFE
		omPropLabels.Add("Mandatorylist");
		omPropLabels.Add("Headersizes");
		omPropLabels.Add("Left");
		omPropLabels.Add("Top");
		omPropLabels.Add("Width");
		omPropLabels.Add("Height");
		omPropLabels.Add("Enabled");
		omPropLabels.Add("Visible");
		omCtrlType = CString("grid");
	}
	UGridCtrlProperties(const UGridCtrlProperties &opO)
		: UCtrlProperties((UCtrlProperties)opO)
	{
		;
	}
	const UGridCtrlProperties &operator =(const UGridCtrlProperties &opO) 
	{
		//(UCtrlProperties)*this = (UCtrlProperties)opO;
		return *this;
	}
};
//-****************************************************************************************
//
//-****************************************************************************************
class URadioCtrlProperties : public UCtrlProperties
{
public:
	URadioCtrlProperties():UCtrlProperties()
	{//Preinitialize default values
		omName			= CString("Radio");
		omPropLabels.Add("Label");
		omPropLabels.Add("Name");
		omPropLabels.Add("DB-Field");		
		omPropLabels.Add("Orientation");	
		omPropLabels.Add("Left");
		omPropLabels.Add("Top");
		omPropLabels.Add("Width");
		omPropLabels.Add("Height");
		omPropLabels.Add("BackColor");
		omPropLabels.Add("FontColor");
		omPropLabels.Add("BorderColor");
		omPropLabels.Add("Enabled");
		omPropLabels.Add("Visible");
		omPropLabels.Add("Is Master");
		omPropLabels.Add("Correspond. Master");
		omPropLabels.Add("DB-Value (if checked)");
		omCtrlType = CString("radio");
	}
	URadioCtrlProperties(const URadioCtrlProperties &opO)
		: UCtrlProperties((UCtrlProperties)opO)
	{
		;
	}
	const URadioCtrlProperties &operator =(const URadioCtrlProperties &opO) 
	{
		(UCtrlProperties)*this = (UCtrlProperties)opO;
		return *this;
	}
};
//-****************************************************************************************
//
//-****************************************************************************************
class ULineCtrlProperties : public UCtrlProperties
{
public:
	ULineCtrlProperties():UCtrlProperties()
	{//Preinitialize default values
		omName			= CString("Line");
		omPropLabels.Add("Name");
		omPropLabels.Add("Left");
		omPropLabels.Add("Top");
		omPropLabels.Add("Width");
		omPropLabels.Add("Height");
		omPropLabels.Add("BackColor");
		omPropLabels.Add("FontColor");
		omPropLabels.Add("BorderColor");
		omPropLabels.Add("Visible");
		omPropLabels.Add("BorderWidth");
		omCtrlType = CString("line");
	}
	ULineCtrlProperties(const ULineCtrlProperties &opO)
		: UCtrlProperties((UCtrlProperties)opO)
	{
		;
	}
	const ULineCtrlProperties &operator =(const ULineCtrlProperties &opO) 
	{
		(UCtrlProperties)*this = (UCtrlProperties)opO;
		return *this;
	}
};
//-****************************************************************************************
//
//-****************************************************************************************
class URectCtrlProperties : public UCtrlProperties
{
public:
	URectCtrlProperties():UCtrlProperties()
	{//Preinitialize default values
		omName			= CString("Rect");
		omPropLabels.Add("Name");
		omPropLabels.Add("Left");
		omPropLabels.Add("Top");
		omPropLabels.Add("Width");
		omPropLabels.Add("Height");
		omPropLabels.Add("BackColor");
		omPropLabels.Add("FontColor");
		omPropLabels.Add("BorderColor");
		omPropLabels.Add("Visible");
		omPropLabels.Add("BorderWidth");
		omCtrlType = CString("rectangle");
	}
	URectCtrlProperties(const URectCtrlProperties &opO)
		: UCtrlProperties((UCtrlProperties)opO)
	{
		;
	}
	const URectCtrlProperties &operator =(const URectCtrlProperties &opO) 
	{
		(UCtrlProperties)*this = (UCtrlProperties)opO;
		return *this;
	}
};

//******************************************************************************************
// class UFormProperties
//******************************************************************************************
class UFormProperties : public UCtrlProperties
{
public:
	UFormProperties():UCtrlProperties()
	{
		omRect = CRect(0, 0, 400, 400);
		imStaticCount = 0;
		imEditCount = 0;
		imComboboxCount = 0;
		imCheckboxCount = 0;
		imRadioCount = 0;
		imListBoxCount = 0;
		imButtonCount = 0;
		imRectCount = 0;
		imLineCount = 0;
		imGridCount = 0;
//inherited properties
		omCtrlType		= CString("Form");		
		omLabel			= CString("Label");		
		omDBField		= CString("");
		omName			= CString("");			
		omOrientation	= CString("LEFT");	
		omBackColor		= COLORREF(RGB(192,192,192));
		omFontColor		= COLORREF(RGB(0,0,0));
		omBorderColor	= COLORREF(RGB(0,0,0));
		bmEnabled		= TRUE;
		bmVisible		= TRUE;
		imBorderWidth   = 1;
		bmDBField		= TRUE;
		bmModal			= TRUE;
//---Labels
		omName			= CString("Form");
		omLabel			= CString("Form");
		omOrientation = CString("CENTER");
		omPropLabels.Add("Label");
		omPropLabels.Add("Name");
		omPropLabels.Add("DB-Table");		
		omPropLabels.Add("Orientation");	
		omPropLabels.Add("Left");
		omPropLabels.Add("Top");
		omPropLabels.Add("Width");
		omPropLabels.Add("Height");
		omPropLabels.Add("BackColor");
		omPropLabels.Add("FontColor");
		omPropLabels.Add("BorderColor");
		omPropLabels.Add("Enabled");
		omPropLabels.Add("Visible");
		omPropLabels.Add("BorderWidth");
		omPropLabels.Add("Modal");
		omCtrlType = CString("Form");

	}

	UFormProperties(const UFormProperties &opO)
		: UCtrlProperties((UCtrlProperties)opO)
	{
		this->omTable			= opO.omTable	  ;
		this->omBorderType		= opO.omBorderType ;
		this->omWindowType		= opO.omWindowType  ;

		this->imStaticCount		= opO.imStaticCount;
		this->imEditCount		= opO.imEditCount;
		this->imComboboxCount	= opO.imComboboxCount;
		this->imCheckboxCount	= opO.imCheckboxCount;
		this->imRadioCount		= opO.imRadioCount;
		this->imListBoxCount	= opO.imListBoxCount;
		this->imButtonCount		= opO.imButtonCount;
		this->imRectCount		= opO.imRectCount;
		this->imLineCount		= opO.imLineCount;

		this->omProperties.DeleteAll();
		for(int i = 0; i < opO.omProperties.GetSize(); i++)
		{
		// "button", "edit", "checkbox", "combobox", "static", "line", "rectangle", "radio", "group", "listbox"
			if(opO.omProperties[i].omCtrlType == "edit")
			{
				UEditCtrlProperties *polEdit = (UEditCtrlProperties*)&(opO.omProperties[i]);
				UEditCtrlProperties *polNewEdit = new UEditCtrlProperties();
				*polNewEdit = *polEdit;
				this->omProperties.Add((UCtrlProperties *)polNewEdit);
			}
			if(opO.omProperties[i].omCtrlType == "grid")
			{
				UGridCtrlProperties *polCtrl = (UGridCtrlProperties *)&(opO.omProperties[i]);
				UGridCtrlProperties *polNewCtrl = new UGridCtrlProperties();
				*polNewCtrl = *polCtrl;
				this->omProperties.Add((UCtrlProperties *)polNewCtrl);
			}
			if(opO.omProperties[i].omCtrlType == "button")
			{
				UButtonCtrlProperties *polCtrl = (UButtonCtrlProperties *)&(opO.omProperties[i]);
				UButtonCtrlProperties *polNewCtrl = new UButtonCtrlProperties();
				*polNewCtrl = *polCtrl;
				this->omProperties.Add((UCtrlProperties *)polNewCtrl);
			}
			if(opO.omProperties[i].omCtrlType == "checkbox")
			{
				UCheckboxCtrlProperties *polCtrl = (UCheckboxCtrlProperties *)&(opO.omProperties[i]);
				UCheckboxCtrlProperties *polNewCtrl = new UCheckboxCtrlProperties();
				*polNewCtrl = *polCtrl;
				this->omProperties.Add((UCtrlProperties *)polNewCtrl);
			}
			if(opO.omProperties[i].omCtrlType == "combobox")
			{
				UComboboxCtrlProperties *polCtrl = (UComboboxCtrlProperties *)&(opO.omProperties[i]);
				UComboboxCtrlProperties *polNewCtrl = new UComboboxCtrlProperties();
				*polNewCtrl = *polCtrl;
				this->omProperties.Add((UCtrlProperties *)polNewCtrl);
			}
			if(opO.omProperties[i].omCtrlType == "static")
			{
				UStaticCtrlProperties *polCtrl = (UStaticCtrlProperties *)&(opO.omProperties[i]);
				UStaticCtrlProperties *polNewCtrl = new UStaticCtrlProperties();
				*polNewCtrl = *polCtrl;
				this->omProperties.Add((UCtrlProperties *)polNewCtrl);
			}
			if(opO.omProperties[i].omCtrlType == "line")
			{
				ULineCtrlProperties *polCtrl = (ULineCtrlProperties *)&(opO.omProperties[i]);
				ULineCtrlProperties *polNewCtrl = new ULineCtrlProperties();
				*polNewCtrl = *polCtrl;
				this->omProperties.Add((UCtrlProperties *)polNewCtrl);
			}
			if(opO.omProperties[i].omCtrlType == "rectangle")
			{
				URectCtrlProperties *polCtrl = (URectCtrlProperties *)&(opO.omProperties[i]);
				URectCtrlProperties *polNewCtrl = new URectCtrlProperties();
				*polNewCtrl = *polCtrl;
				this->omProperties.Add((UCtrlProperties *)polNewCtrl);
			}
			if(opO.omProperties[i].omCtrlType == "radio")
			{
				URadioCtrlProperties *polCtrl = (URadioCtrlProperties *)&(opO.omProperties[i]);
				URadioCtrlProperties *polNewCtrl = new URadioCtrlProperties();
				*polNewCtrl = *polCtrl;
				this->omProperties.Add((UCtrlProperties *)polNewCtrl);
			}
			if(opO.omProperties[i].omCtrlType == "group")
			{
				UGroupCtrlProperties *polCtrl = (UGroupCtrlProperties *)&(opO.omProperties[i]);
				UGroupCtrlProperties *polNewCtrl = new UGroupCtrlProperties();
				*polNewCtrl = *polCtrl;
				this->omProperties.Add((UCtrlProperties *)polNewCtrl);
			}
			if(opO.omProperties[i].omCtrlType == "listbox")
			{
				//TO DO Implement Listboxes
			}
//			this->omProperties.NewAt(this->omProperties.GetSize(), opO.omProperties[i]);
		}
		this->omDBFields.DeleteAll();
		for(i = 0; i < opO.omDBFields.GetSize(); i++)
		{
			this->omDBFields.NewAt(this->omDBFields.GetSize(), opO.omDBFields[i]);
		}
//inherited properties
		this->omCtrlType		= opO.omCtrlType	;
		this->omLabel			= opO.omLabel		;
		this->omName			= opO.omName		;
		this->omDBField			= opO.omDBField;
		this->omOrientation	= opO.omOrientation;
		this->omRect			= opO.omRect		;
		this->omBackColor		= opO.omBackColor	;
		this->omFontColor		= opO.omFontColor	;
		this->omBorderColor	= opO.omBorderColor;
		this->bmEnabled			= opO.bmEnabled;
		this->bmVisible			= opO.bmVisible;
		this->imBorderWidth     = opO.imBorderWidth;
		this->bmDBField			= opO.bmDBField;
		this->bmModal			= opO.bmModal;
		this->omFieldType		= opO.omFieldType;
		this->omFormatString	= opO.omFormatString;
		this->omPropLabels.RemoveAll();
		for( i = 0; i < opO.omPropLabels.GetSize(); i++)
		{
			this->omPropLabels.Add(opO.omPropLabels[i]);
		}
		this->omTables.RemoveAll();
		for( i = 0; i < opO.omTables.GetSize(); i++)
		{
			this->omTables.Add(opO.omTables[i]);
		}

	}
	~UFormProperties()
	{
		omProperties.DeleteAll();
		omDBFields.DeleteAll();
	}

	const UFormProperties &operator =(const UFormProperties &opO) 
	{
		(UCtrlProperties)*this = (UCtrlProperties)opO;
		this->omTable			= opO.omTable	  ;
		this->omBorderType		= opO.omBorderType ;
		this->omWindowType		= opO.omWindowType  ;

		this->imStaticCount		= opO.imStaticCount;
		this->imEditCount		= opO.imEditCount;
		this->imComboboxCount	= opO.imComboboxCount;
		this->imCheckboxCount	= opO.imCheckboxCount;
		this->imRadioCount		= opO.imRadioCount;
		this->imListBoxCount	= opO.imListBoxCount;
		this->imButtonCount		= opO.imButtonCount;
		this->imRectCount		= opO.imRectCount;
		this->imLineCount		= opO.imLineCount;

		this->omProperties.DeleteAll();
		for(int i = 0; i < opO.omProperties.GetSize(); i++)
		{
		// "button", "edit", "checkbox", "combobox", "static", "line", "rectangle", "radio", "group", "listbox"
			if(opO.omProperties[i].omCtrlType == "edit")
			{
				UEditCtrlProperties *polEdit = (UEditCtrlProperties*)&(opO.omProperties[i]);
				//UEditCtrlProperties *polNewEdit = new UEditCtrlProperties();
				//*polNewEdit = *polEdit;
				this->omProperties.NewAt(this->omProperties.GetSize(), (UCtrlProperties)*polEdit);
			}
			if(opO.omProperties[i].omCtrlType == "grid")
			{
				//UGridCtrlProperties *polCtrl = (UGridCtrlProperties *)&(opO.omProperties[i]);
				//UGridCtrlProperties *polNewCtrl = new UGridCtrlProperties(/**polCtrl*/);
				//*polNewCtrl = *polCtrl;
				this->omProperties.NewAt(this->omProperties.GetSize(), opO.omProperties[i]);
			}
			if(opO.omProperties[i].omCtrlType == "button")
			{
				UButtonCtrlProperties *polCtrl = (UButtonCtrlProperties *)&(opO.omProperties[i]);
				UButtonCtrlProperties *polNewCtrl = new UButtonCtrlProperties();
				*polNewCtrl = *polCtrl;
				this->omProperties.Add((UCtrlProperties *)polNewCtrl);
			}
			if(opO.omProperties[i].omCtrlType == "checkbox")
			{
				UCheckboxCtrlProperties *polCtrl = (UCheckboxCtrlProperties *)&(opO.omProperties[i]);
				UCheckboxCtrlProperties *polNewCtrl = new UCheckboxCtrlProperties();
				*polNewCtrl = *polCtrl;
				this->omProperties.Add((UCtrlProperties *)polNewCtrl);
			}
			if(opO.omProperties[i].omCtrlType == "combobox")
			{
				UComboboxCtrlProperties *polCtrl = (UComboboxCtrlProperties *)&(opO.omProperties[i]);
				UComboboxCtrlProperties *polNewCtrl = new UComboboxCtrlProperties();
				*polNewCtrl = *polCtrl;
				this->omProperties.Add((UCtrlProperties *)polNewCtrl);
			}
			if(opO.omProperties[i].omCtrlType == "static")
			{
				UStaticCtrlProperties *polCtrl = (UStaticCtrlProperties *)&(opO.omProperties[i]);
				UStaticCtrlProperties *polNewCtrl = new UStaticCtrlProperties();
				*polNewCtrl = *polCtrl;
				this->omProperties.Add((UCtrlProperties *)polNewCtrl);
			}
			if(opO.omProperties[i].omCtrlType == "line")
			{
				ULineCtrlProperties *polCtrl = (ULineCtrlProperties *)&(opO.omProperties[i]);
				ULineCtrlProperties *polNewCtrl = new ULineCtrlProperties();
				*polNewCtrl = *polCtrl;
				this->omProperties.Add((UCtrlProperties *)polNewCtrl);
			}
			if(opO.omProperties[i].omCtrlType == "rectangle")
			{
				URectCtrlProperties *polCtrl = (URectCtrlProperties *)&(opO.omProperties[i]);
				URectCtrlProperties *polNewCtrl = new URectCtrlProperties();
				*polNewCtrl = *polCtrl;
				this->omProperties.Add((UCtrlProperties *)polNewCtrl);
			}
			if(opO.omProperties[i].omCtrlType == "radio")
			{
				URadioCtrlProperties *polCtrl = (URadioCtrlProperties *)&(opO.omProperties[i]);
				URadioCtrlProperties *polNewCtrl = new URadioCtrlProperties();
				*polNewCtrl = *polCtrl;
				this->omProperties.Add((UCtrlProperties *)polNewCtrl);
			}
			if(opO.omProperties[i].omCtrlType == "group")
			{
				UGroupCtrlProperties *polCtrl = (UGroupCtrlProperties *)&(opO.omProperties[i]);
				UGroupCtrlProperties *polNewCtrl = new UGroupCtrlProperties();
				*polNewCtrl = *polCtrl;
				this->omProperties.Add((UCtrlProperties *)polNewCtrl);
			}
			if(opO.omProperties[i].omCtrlType == "listbox")
			{
				//TO DO Implement Listboxes
			}
//			this->omProperties.NewAt(this->omProperties.GetSize(), opO.omProperties[i]);
		}
		this->omDBFields.DeleteAll();
		for(i = 0; i < opO.omDBFields.GetSize(); i++)
		{
			this->omDBFields.NewAt(this->omDBFields.GetSize(), opO.omDBFields[i]);
		}
//inherited properties
		this->omCtrlType		= opO.omCtrlType	;
		this->omLabel			= opO.omLabel		;
		this->omName			= opO.omName		;
		this->omOrientation		= opO.omOrientation;
		this->omRect			= opO.omRect		;
		this->omBackColor		= opO.omBackColor	;
		this->omFontColor		= opO.omFontColor	;
		this->omBorderColor		= opO.omBorderColor;
		this->bmEnabled			= opO.bmEnabled;
		this->bmVisible			= opO.bmVisible;
		this->imBorderWidth     = opO.imBorderWidth;
		this->bmDBField			= opO.bmDBField;
		this->bmModal			= opO.bmModal;
		this->omFieldType		= opO.omFieldType;
		this->omFormatString	= opO.omFormatString;
		this->omPropLabels.RemoveAll();
		for( i = 0; i < opO.omPropLabels.GetSize(); i++)
		{
			this->omPropLabels.Add(opO.omPropLabels[i]);
		}
		this->omTables.RemoveAll();
		for( i = 0; i < opO.omTables.GetSize(); i++)
		{
			this->omTables.Add(opO.omTables[i]);
		}
		return *this;

	}
	UFormField * GetFieldInfo(CString opField, CString opTable)
	{
		for(int i = 0; i < omDBFields.GetSize(); i++)
		{
			if(omDBFields[i].Table == opTable && omDBFields[i].Field == opField)
			{
				return &omDBFields[i];
			}
		}
		return NULL;
	}

	void SetFieldInfo(CString opField, CString opTable)
	{
		if(GetFieldInfo(opField, opTable) == NULL) // Field does not exist
		{
			CString olUrno = ogBCD.GetFieldExt("SYS", "TANA", "FINA", opTable, opField, "URNO");
			if(!olUrno.IsEmpty())
			{
				CString olHeader;
				CString olType;
				CString olRefe;
				CString olReqf;
				RecordSet olRec;
				ogBCD.GetRecord("SYS", "URNO", olUrno, olRec);
				olHeader = olRec[ogBCD.GetFieldIndex("SYS", "LABL")];
				olHeader.TrimLeft();olHeader.TrimRight();
				olType = olRec[ogBCD.GetFieldIndex("SYS", "TYPE")];
				olReqf = olRec[ogBCD.GetFieldIndex("SYS", "REQF")];
				if(olHeader.IsEmpty())
				{
					olHeader = olRec[ogBCD.GetFieldIndex("SYS", "ADDI")];
				}
				olRefe = olRec[ogBCD.GetFieldIndex("SYS", "REFE")];
				if(olRefe ==".")
				{
					olRefe.Empty();
				}
				UFormField *polFormFields = new UFormField();
				polFormFields->Table =		opTable;
				polFormFields->Field =		opField;
				polFormFields->Type =		olType;
				polFormFields->Header =		olHeader;
				polFormFields->Visible =	CString("1");
				polFormFields->Key =		CString("");
				polFormFields->Refe =		olRefe;
				polFormFields->Reqf =	olReqf;
				this->omDBFields.Add(polFormFields);

			}
		}
	}

	CString							omTable;	  // DB-Table (Maintable for the window)
	CStringArray					omTables;	  // may be there are more tables
	CString							omBorderType; // "Dialog", "Size"
	CString							omWindowType; // "Modal", "Modless"
	
	int								imStaticCount, imEditCount,
									imComboboxCount,	imCheckboxCount,
									imRadioCount, imListBoxCount,
									imButtonCount, // For Button and Groupboxes
									imRectCount,
									imLineCount, imGridCount; 

	CCSPtrArray<UCtrlProperties>    omProperties;
	CCSPtrArray<UFormField>         omDBFields;
};
#endif //__CTRL_PROPERTIES__