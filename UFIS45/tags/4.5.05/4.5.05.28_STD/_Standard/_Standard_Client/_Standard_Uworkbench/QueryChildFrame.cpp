// QueryChildFrame.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "QueryChildFrame.h"
#include "QueryDesingerTables.h"
#include "GridView.h"
#include "QueryDesignerGridView.h"
#include "UQueryDesignerTableView.h"
#include "UQueryRelationsView.h"
#include "UUniListView.h"
#include "UScrollContainerView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CQueryChildFrame

IMPLEMENT_DYNCREATE(CQueryChildFrame, CMDIChildWnd)

CQueryChildFrame::CQueryChildFrame()
{
}

CQueryChildFrame::~CQueryChildFrame()
{
	bmIsInit = false;
}


BEGIN_MESSAGE_MAP(CQueryChildFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CQueryChildFrame)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CQueryChildFrame message handlers

BOOL CQueryChildFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext) 
{
	m_wndSplitter.CreateStatic(this, 2,1);   //(this, 3, 3);
	CRect olRect;
	GetClientRect(&olRect);
	int ilHeight = (int)((olRect.bottom - olRect.top)/2);
	int ilWidth = (int)((olRect.right  - olRect.left)/2);
	//m_wndSplitter.CreateView(

	// add the first splitter pane - the default view in column 0
/*	if (!m_wndSplitter.CreateView(0, 0,
		pContext->m_pNewViewClass, CSize(ilWidth, ilHeight), pContext))
	{
		TRACE0("Failed to create first pane\n");
		return FALSE;
	}
*/	

	if (!m_wndSplitter2.CreateStatic(
		&m_wndSplitter,     // our parent window is the first splitter
		1, 2,               // the new splitter is 2 rows, 1 column
		WS_CHILD | WS_VISIBLE | WS_BORDER,  // style, WS_BORDER is needed
		m_wndSplitter.IdFromRowCol(0, 0)
			// new splitter is in the first row, first column of first splitter
	   ))
	{
		TRACE0("Failed to create nested splitter\n");
		return FALSE;
	}

	pomTableArea = RUNTIME_CLASS( /*UQueryDesignerTableView*/UScrollContainerView );
	pomGridArea  = RUNTIME_CLASS(CQueryDesignerGridView);
	pomRelationArea  = RUNTIME_CLASS(UQueryRelationsView);
	if (!m_wndSplitter2.CreateView(0, 0, pomTableArea, CSize((int)(ilWidth/4)*3, ilHeight), pContext))
	{
		TRACE0("Failed to create second pane\n");
		return FALSE;
	}
	if (!m_wndSplitter2.CreateView(0, 1, RUNTIME_CLASS(UUniListView)/*pomRelationArea*/, CSize((int)(ilWidth/4), ilHeight), pContext))
	{	
		TRACE0("Failed to create third pane\n");
		return FALSE;
	}
/*	if (!m_wndSplitter.CreateView(0, 0, pomTableArea, CSize(ilWidth, ilHeight), pContext))
	{
		TRACE0("Failed to create QueryDesingerTables pane\n");
		return FALSE;
	}
*/

	if (!m_wndSplitter.CreateView(1, 0, pomGridArea, CSize(/*ilWidth, ilHeight*/0,0), pContext))
	{
		TRACE0("Failed to create CGridView pane\n");
		return FALSE;
	}
	m_wndSplitter.SetRowInfo( 0, ilHeight, 0);
	m_wndSplitter.SetRowInfo( 1, ilHeight, 0);
	m_wndSplitter2.SetColumnInfo( 0, (int)(((olRect.bottom - olRect.top)/4)*5), 0);

//	UQueryRelationsView
//	pom1 = (UQueryDesignerTableView *) GetDlgItem(1);
//	pom2 = (CQueryDesignerGridView *) GetDlgItem(2);


	
	//p.FindWindow(CRunTimeClass(
	bmIsInit = true;
	return TRUE;//CMDIChildWnd::OnCreateClient(lpcs, pContext);
}

BOOL CQueryChildFrame::PreCreateWindow(CREATESTRUCT& cs) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CMDIChildWnd::PreCreateWindow(cs);
}

void CQueryChildFrame::OnSize(UINT nType, int cx, int cy) 
{
	CMDIChildWnd::OnSize(nType, cx, cy);


	if(bmIsInit == true)
	{
/*		CRect olRect;
		GetClientRect(&olRect);
		int ilHeight = (int)((olRect.bottom - olRect.top)/2);
		int ilWidth = (int)((olRect.right  - olRect.left)/2);

		m_wndSplitter.SetRowInfo( 0, ilHeight, 0);
*/
	}
	
}

BOOL CQueryChildFrame::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CMDIFrameWnd* pParentWnd, CCreateContext* pContext) 
{
	BOOL blRet = CMDIChildWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, pContext);
	//SetActiveView(pom2);
	return blRet;
	
}
