#if !defined(AFX_UCOMBOBOX_H__66893424_91DE_11D3_A213_00500437F607__INCLUDED_)
#define AFX_UCOMBOBOX_H__66893424_91DE_11D3_A213_00500437F607__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UComboBox.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// UComboBox window

#define WM_UCOMBOBOX_SELCHANGE	(WM_USER+9700)
#define WM_UCOMBOBOX_KILLFOCUS	(WM_USER+9701)

class UComboBox : public CComboBox
{
// Construction
public:
	UComboBox(CWnd *popParent,
			  COLORREF opBkColor = COLORREF(RGB(192,192,192)), 
			  COLORREF opTextColor = COLORREF(RGB(0,0,0)));


	CWnd *pomParent;
	COLORREF omBKColor;
	COLORREF omTextColor;
	CBrush omBrush;


	void SetBkColor(COLORREF opColor);
	void SetTextColor(COLORREF opColor);
// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(UComboBox)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~UComboBox();

	// Generated message map functions
protected:
	//{{AFX_MSG(UComboBox)
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	afx_msg void OnSelchange();
	afx_msg void OnKillfocus();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UCOMBOBOX_H__66893424_91DE_11D3_A213_00500437F607__INCLUDED_)
