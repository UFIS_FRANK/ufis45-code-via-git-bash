#if !defined(AFX_FORMEXECUTEDOC_H__3596C793_2F83_11D3_A200_00500437F607__INCLUDED_)
#define AFX_FORMEXECUTEDOC_H__3596C793_2F83_11D3_A200_00500437F607__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FormExecuteDoc.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// FormExecuteDoc document
#include "CCSGlobl.h"

class FormExecuteDoc : public CDocument
{
protected:
	FormExecuteDoc();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(FormExecuteDoc)

// Attributes
public:

	UFormProperties *pomForm;
// Operations
public:

	CString omRecordMode; //New, Unchanged, Deleted
	CString omMainTable;
	RecordSet omCurrentRecord;
	bool GetFirstRecord(RecordSet& opRecord);
	bool GetNextRecord(RecordSet& opRecord);
	bool GetPrevRecord(RecordSet& opRecord);
	bool GetLastRecord(RecordSet& opRecord);
	bool NewRecord();
	bool SaveRecord();
	bool DeleteRecord();
	int  imCursorPos;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FormExecuteDoc)
	public:
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual BOOL CanCloseFrame(CFrameWnd* pFrame);
	protected:
	virtual BOOL OnNewDocument();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~FormExecuteDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(FormExecuteDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FORMEXECUTEDOC_H__3596C793_2F83_11D3_A200_00500437F607__INCLUDED_)
