#if !defined(AFX_QUERYDESINGERTABLES_H__8EF758C4_2D57_11D3_A1C7_0000B4984BBE__INCLUDED_)
#define AFX_QUERYDESINGERTABLES_H__8EF758C4_2D57_11D3_A1C7_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// QueryDesingerTables.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// QueryDesingerTables form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include "resource.h"
#include "CCSGlobl.h"
#include "TableListFrameWnd.h"




class QueryDesingerTables : public CFormView
{
protected:
	QueryDesingerTables();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(QueryDesingerTables)

// Form Data
public:
	//{{AFX_DATA(QueryDesingerTables)
	enum { IDD = IDD_QUERYDES_TABLES };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA
	CCSPtrArray<FLY_WINDOWS> omTabWnds;
	CStringArray omTables;


// Attributes
public:

// Operations
public:
	CRect CalcNextFreeRect();
	void OnTablesChanged();
	void CreateFlyWindows(CStringArray &ropList);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(QueryDesingerTables)
	public:
	virtual BOOL OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pLResult);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	virtual void OnUpdate( CView* pSender, LPARAM lHint, CObject* pHint );
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~QueryDesingerTables();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(QueryDesingerTables)
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnAddTables();
	afx_msg LONG OnDestroyFlyWnd(UINT wParam, LONG lParam);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_QUERYDESINGERTABLES_H__8EF758C4_2D57_11D3_A1C7_0000B4984BBE__INCLUDED_)
