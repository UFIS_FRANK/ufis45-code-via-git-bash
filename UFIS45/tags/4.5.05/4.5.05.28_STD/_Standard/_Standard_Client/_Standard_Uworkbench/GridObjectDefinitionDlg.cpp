// GridObjectDefinitionDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "GridObjectDefinitionDlg.h"
#include "CCSGlobl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GridObjectDefinitionDlg dialog


GridObjectDefinitionDlg::GridObjectDefinitionDlg(CWnd* pParent, UGridDefinition *popGridDef)
	: CDialog(GridObjectDefinitionDlg::IDD, pParent)
{

	pomGridDef = popGridDef;
	//{{AFX_DATA_INIT(GridObjectDefinitionDlg)
	v_AsFrameWindow = FALSE;
	v_AsChoiceList = FALSE;
	v_DataSource = _T("");
	v_Filter = _T("");
	v_GridName = _T("");
	v_GroupBy = _T("");
	v_HasViewButton = FALSE;
	v_HeaderSize = FALSE;
	v_IsEditable = FALSE;
	v_MultiSelect = FALSE;
	v_Printable = FALSE;
	v_ReturnField = _T("");
	v_SaveUserLayout = FALSE;
	v_SensitiveForChanges = FALSE;
	v_SetDefaultTimeFrame = FALSE;
	v_ShowTimeFrameDialog = FALSE;
	v_Today = -1;
	v_TodayFrom = _T("");
	v_TodayTo = _T("");
	v_TomorrowFrom = _T("");
	v_TomorrowTo = _T("");
	v_UrnoVisible = FALSE;
	//}}AFX_DATA_INIT
	v_AsFrameWindow         = pomGridDef->AsFrameWindow      ;
	v_AsChoiceList          = pomGridDef->AsChoiceList       ;
//	v_DataSource            = pomGridDef->DataSource         ;
	v_Filter                = pomGridDef->Filter             ;
	v_GridName              = pomGridDef->GridName           ;
//	v_GroupBy               = pomGridDef->GroupBy            ;
	v_HasViewButton         = pomGridDef->HasViewButton      ;
	v_HeaderSize            = pomGridDef->HeaderSize         ;
	v_IsEditable            = pomGridDef->IsEditable         ;
	v_MultiSelect           = pomGridDef->MultiSelect        ;
	v_Printable             = pomGridDef->Printable          ;
	v_ReturnField           = pomGridDef->ReturnField        ;
//	v_SaveUserLayout        = pomGridDef->SaveUserLayout     ;
	v_SensitiveForChanges   = pomGridDef->SensitiveForChanges;
	v_SetDefaultTimeFrame   = pomGridDef->SetDefaultTimeFrame;
	v_ShowTimeFrameDialog   = pomGridDef->ShowTimeFrameDialog;
	v_Today                 = pomGridDef->Today              ;
	v_TodayFrom             = pomGridDef->TodayFrom          ;
	v_TodayTo               = pomGridDef->TodayTo            ;
	v_TomorrowFrom          = pomGridDef->TomorrowFrom       ;
	v_TomorrowTo            = pomGridDef->TomorrowTo         ;
//	v_UrnoVisible           = pomGridDef->UrnoVisible        ;
}


void GridObjectDefinitionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GridObjectDefinitionDlg)
	DDX_Control(pDX, IDC_RETURNSTATIC, m_ReturnLabel);
	DDX_Control(pDX, IDC_URNO_VISIBLE, m_UrnoVisible);
	DDX_Control(pDX, IDC_TOMORROW_TO, m_TomorrowTo);
	DDX_Control(pDX, IDC_TOMORROW_FROM, m_TomorrowFrom);
	DDX_Control(pDX, IDC_TODAY_TO, m_TodayTo);
	DDX_Control(pDX, IDC_TODAY_FROM, m_TodayFrom);
	DDX_Control(pDX, IDC_TODAY, m_Today);
	DDX_Control(pDX, IDC_SHOW_TIMEFRAME_DIALOG, m_ShowTimeFrameDialog);
	DDX_Control(pDX, IDC_SET_DEFAULT_TIMEFRAM, m_SetDefaultTimeFrame);
	DDX_Control(pDX, IDC_SEPA_BMP2, m_SepaBmp2);
	DDX_Control(pDX, IDC_SEPA_BMP1, m_SepaBmp);
	DDX_Control(pDX, IDC_SENSITIV_FOR_DATACHANGES, m_SensitiveForChanges);
	DDX_Control(pDX, IDC_SAVE_USERLAYOUT, m_SaveUserLayout);
	DDX_Control(pDX, IDC_RETURN_FIELD_CB, m_ReturnField);
	DDX_Control(pDX, IDC_PRINTABLE, m_IsPrintable);
	DDX_Control(pDX, IDC_MULTISELECT, m_MultiSelect);
	DDX_Control(pDX, IDC_IS_EDITABLE, m_IsEditable);
	DDX_Control(pDX, IDC_HEADER_SIZE, m_HeaderSize);
	DDX_Control(pDX, IDC_HAS_VIEW_BUTTON, m_HasViewButton);
	DDX_Control(pDX, IDC_GROUPBY_CB, m_GroupBy);
	DDX_Control(pDX, IDC_GRID_NAME, m_GridName);
	DDX_Control(pDX, IDC_FILTER_CB, m_Filter);
	DDX_Control(pDX, IDC_DATASOURCE_CB, m_DataSource);
	DDX_Control(pDX, IDC_ASCHOICELIST, m_AsChoiceList);
	DDX_Control(pDX, IDC_AS_FRAMEWINDOW, m_AsFrameWindow);
	DDX_Check(pDX, IDC_AS_FRAMEWINDOW, v_AsFrameWindow);
	DDX_Check(pDX, IDC_ASCHOICELIST, v_AsChoiceList);
	DDX_CBString(pDX, IDC_DATASOURCE_CB, v_DataSource);
	DDX_CBString(pDX, IDC_FILTER_CB, v_Filter);
	DDX_Text(pDX, IDC_GRID_NAME, v_GridName);
	DDX_CBString(pDX, IDC_GROUPBY_CB, v_GroupBy);
	DDX_Check(pDX, IDC_HAS_VIEW_BUTTON, v_HasViewButton);
	DDX_Check(pDX, IDC_HEADER_SIZE, v_HeaderSize);
	DDX_Check(pDX, IDC_IS_EDITABLE, v_IsEditable);
	DDX_Check(pDX, IDC_MULTISELECT, v_MultiSelect);
	DDX_Check(pDX, IDC_PRINTABLE, v_Printable);
	DDX_CBString(pDX, IDC_RETURN_FIELD_CB, v_ReturnField);
	DDX_Check(pDX, IDC_SAVE_USERLAYOUT, v_SaveUserLayout);
	DDX_Check(pDX, IDC_SENSITIV_FOR_DATACHANGES, v_SensitiveForChanges);
	DDX_Check(pDX, IDC_SET_DEFAULT_TIMEFRAM, v_SetDefaultTimeFrame);
	DDX_Check(pDX, IDC_SHOW_TIMEFRAME_DIALOG, v_ShowTimeFrameDialog);
	DDX_Radio(pDX, IDC_TODAY, v_Today);
	DDX_Text(pDX, IDC_TODAY_FROM, v_TodayFrom);
	DDX_Text(pDX, IDC_TODAY_TO, v_TodayTo);
	DDX_Text(pDX, IDC_TOMORROW_FROM, v_TomorrowFrom);
	DDX_Text(pDX, IDC_TOMORROW_TO, v_TomorrowTo);
	DDX_Check(pDX, IDC_URNO_VISIBLE, v_UrnoVisible);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GridObjectDefinitionDlg, CDialog)
	//{{AFX_MSG_MAP(GridObjectDefinitionDlg)
	ON_BN_CLICKED(IDC_ASCHOICELIST, OnAschoicelist)
	ON_BN_CLICKED(IDC_SET_DEFAULT_TIMEFRAM, OnSetDefaultTimefram)
	ON_BN_CLICKED(IDC_SHOW_TIMEFRAME_DIALOG, OnShowTimeframeDialog)
	ON_BN_CLICKED(IDC_TODAY, OnToday)
	ON_BN_CLICKED(IDC_TOMORROW, OnTomorrow)
	ON_CBN_SELCHANGE(IDC_DATASOURCE_CB, OnSelchangeDatasourceCb)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GridObjectDefinitionDlg message handlers

BOOL GridObjectDefinitionDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	for(int i = 0; i < ogQueries.GetSize(); i++)
	{
		CString olQ = ogQueries[i].omName;
		m_DataSource.AddString(ogQueries[i].omName);
		m_Filter.AddString(ogQueries[i].omName);
		m_GroupBy.AddString(ogQueries[i].omName);
		//m_ReturnField
	}
	bool blFound = false;
	for(i = 0; ((i < ogQueries.GetSize()) && (blFound == false)); i++)
	{
		if(ogQueries[i].omName == pomGridDef->DataSource)
		{
			blFound = true;
			//UQuery o;
			//UQueryDetail p;p.Field
			for(int j = 0; j < ogQueries[i].omQueryDetails.GetSize(); j++)
			{
				m_ReturnField.AddString(ogQueries[i].omQueryDetails[j].Field);
			}
		}
		
	}
	if(v_AsChoiceList == TRUE)
	{
		m_ReturnField.ShowWindow(SW_SHOWNORMAL);
		m_ReturnLabel.ShowWindow(SW_SHOWNORMAL);
	}
	else
	{
		m_ReturnField.ShowWindow(SW_HIDE);
		m_ReturnLabel.ShowWindow(SW_HIDE);
		m_ReturnField.SetWindowText("");
	}
	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void GridObjectDefinitionDlg::OnAschoicelist() 
{
	int ilCheck = m_AsChoiceList.GetCheck();
	if(ilCheck == 1)
	{
		m_ReturnField.ShowWindow(SW_SHOWNORMAL);
		m_ReturnLabel.ShowWindow(SW_SHOWNORMAL);
	}
	else
	{
		m_ReturnField.ShowWindow(SW_HIDE);
		m_ReturnLabel.ShowWindow(SW_HIDE);
		m_ReturnField.SetWindowText("");
	}
}

void GridObjectDefinitionDlg::OnSetDefaultTimefram() 
{
	// TODO: Add your control notification handler code here
	
}

void GridObjectDefinitionDlg::OnShowTimeframeDialog() 
{
	// TODO: Add your control notification handler code here
	
}

void GridObjectDefinitionDlg::OnToday() 
{
	// TODO: Add your control notification handler code here
	
}

void GridObjectDefinitionDlg::OnTomorrow() 
{
	// TODO: Add your control notification handler code here
	
}

void GridObjectDefinitionDlg::OnOK() 
{
	if(UpdateData() == FALSE)
		return;

	if(v_GridName.IsEmpty())
	{
		MessageBox(LoadStg(IDS_STRING61452), LoadStg(IDS_STRING61453), MB_OK);
		m_GridName.SetFocus();	
		return;
	}


 	pomGridDef->AsFrameWindow      = v_AsFrameWindow         ;
	pomGridDef->AsChoiceList       = v_AsChoiceList          ;
	pomGridDef->DataSource         = v_DataSource            ;
	pomGridDef->Filter             = v_Filter                ;
	pomGridDef->GridName           = v_GridName              ;
//	pomGridDef->GroupBy            = v_GroupBy               ;
	pomGridDef->HasViewButton      = v_HasViewButton         ;
	pomGridDef->HeaderSize         = v_HeaderSize            ;
	pomGridDef->IsEditable         = v_IsEditable            ;
	pomGridDef->MultiSelect        = v_MultiSelect           ;
	pomGridDef->Printable          = v_Printable             ;
	pomGridDef->ReturnField        = v_ReturnField           ;
//	pomGridDef->SaveUserLayout     = v_SaveUserLayout        ;
	pomGridDef->SensitiveForChanges= v_SensitiveForChanges   ;
	pomGridDef->SetDefaultTimeFrame= v_SetDefaultTimeFrame   ;
	pomGridDef->ShowTimeFrameDialog= v_ShowTimeFrameDialog   ;
	pomGridDef->Today              = v_Today                 ;
	pomGridDef->TodayFrom          = v_TodayFrom             ;
	pomGridDef->TodayTo            = v_TodayTo               ;
	pomGridDef->TomorrowFrom       = v_TomorrowFrom          ;
	pomGridDef->TomorrowTo         = v_TomorrowTo            ;
//	pomGridDef->UrnoVisible        = v_UrnoVisible           ;
	
	CDialog::OnOK();
}

void GridObjectDefinitionDlg::OnSelchangeDatasourceCb() 
{
	CString olText;
	int ilIdx = m_DataSource.GetCurSel();
	m_DataSource.GetLBText(ilIdx, olText);
	bool blFound = false;
	for(int i = 0; ((i < ogQueries.GetSize()) && (blFound == false)); i++)
	{
		if(ogQueries[i].omName == olText)
		{
			blFound = true;
			m_ReturnField.ResetContent();
			//UQuery o;
			//UQueryDetail p;p.Field
			for(int j = 0; j < ogQueries[i].omQueryDetails.GetSize(); j++)
			{
				m_ReturnField.AddString(ogQueries[i].omQueryDetails[j].Field);
			}
		}
		
	}
}
