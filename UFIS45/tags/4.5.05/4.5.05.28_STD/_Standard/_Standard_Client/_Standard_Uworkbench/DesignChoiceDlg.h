#if !defined(AFX_DESIGNCHOICEDLG_H__B475BCA1_7D51_11D3_A202_00500437F607__INCLUDED_)
#define AFX_DESIGNCHOICEDLG_H__B475BCA1_7D51_11D3_A202_00500437F607__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DesignChoiceDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DesignChoiceDlg dialog

class DesignChoiceDlg : public CDialog
{
// Construction
public:
	DesignChoiceDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DesignChoiceDlg)
	enum { IDD = IDD_DESIGN_CHOICE };
	int		m_Choice;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DesignChoiceDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DesignChoiceDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DESIGNCHOICEDLG_H__B475BCA1_7D51_11D3_A202_00500437F607__INCLUDED_)
