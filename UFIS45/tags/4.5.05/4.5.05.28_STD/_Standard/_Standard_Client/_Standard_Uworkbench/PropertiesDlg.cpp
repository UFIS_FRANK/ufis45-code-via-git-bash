// PropertiesDlg.cpp : implementation file
//
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *	UWorkBench - Project
 *	
 *		Property dialog class  
 *		
 *
 *	ABB Airport Technologies GmbH 2000
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *
 *		Date		Author			Comment
 *		-------------------------------------------------------------
 *
 *		??/??/??	mwo AAT/ID		Initial version
 *		05/11/00	cla AAT/ID		Adaptions for the Workbench Step made
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "stdafx.h"
#include "UWorkBench.h"
#include "PropertiesDlg.h"
#include "FormDesignerView.h"
#include "CCSGlobl.h"
#include "Tools.h"
#include "DetailGridWizard.h"
#include "FormDesignerDoc.h"
#include "CntlEvent.h"
#include <iterator>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PropertiesDlg dialog


PropertiesDlg::PropertiesDlg(CWnd* pParent, 
							 UCtrlProperties *popProperties, 
							 UFormControlObj *popCtrlObj, 
							 CString opTable, 
							 FormDesignerView* popView)
	: CDialog(PropertiesDlg::IDD, pParent)
{
	pomCtrlObj = popCtrlObj;
	pomProperties = popProperties;
	pomView = popView;
	imCurrentPage = PROP_PROPERTIES;
	if(pomProperties != NULL)
	{
		omCurrentControl = pomProperties->omCtrlType;
	}
	omCurrentTable = opTable;
	//{{AFX_DATA_INIT(PropertiesDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	CDialog::Create(PropertiesDlg::IDD, pParent);
	ShowWindow(SW_SHOWNORMAL);
}

PropertiesDlg::~PropertiesDlg()
{
	if(pomPropTab != NULL)
	{
		delete pomPropTab;
	}

}

void PropertiesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PropertiesDlg)
	DDX_Control(pDX, IDC_BUTTON1, omOnOkButton);
	DDX_Control(pDX, IDC_TAB1, m_Tab);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(PropertiesDlg, CDialog)
	//{{AFX_MSG_MAP(PropertiesDlg)
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_MESSAGE(GX_ROWCOL_CHANGED, OnRowColChanged)
	ON_MESSAGE(GX_CELL_BUTTONCLICKED, OnGridButtonClicked)
	ON_MESSAGE(WM_PROPERTY_UPDATE, OnPropertyUpdate)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB1, OnSelchangeTab1)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PropertiesDlg message handlers

BOOL PropertiesDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	if(ogPropertiesDlgRect != CRect(0,0,0,0))
	{
		MoveWindow(&ogPropertiesDlgRect);
	}
	TC_ITEM TabCtrlItem;
	TabCtrlItem.mask = TCIF_TEXT;
/*	TabCtrlItem.pszText = "Format";
	m_Tab.InsertItem( 0, &TabCtrlItem );
	TabCtrlItem.pszText = "Data";
	m_Tab.InsertItem( 1, &TabCtrlItem );
	TabCtrlItem.pszText = "Event";
	m_Tab.InsertItem( 2, &TabCtrlItem );
	TabCtrlItem.pszText = "Others";
	m_Tab.InsertItem( 3, &TabCtrlItem );
	TabCtrlItem.pszText = "All";
	m_Tab.InsertItem( 4, &TabCtrlItem );
*/	
	TabCtrlItem.pszText = "Properties";
	m_Tab.InsertItem( 0, &TabCtrlItem );
	TabCtrlItem.pszText = "Event";
	m_Tab.InsertItem( 1, &TabCtrlItem );

	pomPropTab = new GxGridTab(this);

	pomPropTab->EnableAutoGrow(false);
	if(pomProperties != NULL)
	{

		pomPropTab->SubclassDlgItem(IDC_PROPERTIES, this);
		pomPropTab->Initialize();
		CGXPushbutton *polButton = new CGXPushbutton(pomPropTab);
		//polButton->Create(WS_VSCROLL | CBS_DROPDOWN | CBS_AUTOHSCROLL | CBS_SORT, 0);
		//pWnd->m_bFillWithChoiceList = TRUE;
		//pWnd->m_bWantArrowKeys = TRUE;
		pomPropTab->RegisterControl(GX_IDS_CTRL_PUSHBTN, polButton, TRUE);

		CGXComboBoxWnd *pWnd = new CGXComboBoxWnd(pomPropTab);
		pWnd->Create(WS_VSCROLL | CBS_DROPDOWN | CBS_AUTOHSCROLL | CBS_SORT, 0);
		pWnd->m_bFillWithChoiceList = TRUE;
		pWnd->m_bWantArrowKeys = TRUE;
		pomPropTab->RegisterControl(GX_IDS_CTRL_CBS_DROPDOWN, pWnd);

		pomPropTab->GetParam()->EnableUndo(FALSE);
		pomPropTab->LockUpdate(TRUE);
		pomPropTab->SetRowCount(100); 
		pomPropTab->SetColCount(2);
		pomPropTab->SetColWidth(0,1,169);
		pomPropTab->SetColWidth(1,1,170);
		pomPropTab->SetColWidth(2,2,17);

		pomPropTab->SetRowHeight(0, 0, 0);

		pomPropTab->LockUpdate(FALSE);
		pomPropTab->GetParam()->EnableUndo(TRUE);

		CGXStyle olStyle;
		for( int i = 1; i < 100; i++)
		{
			if(i-1 < pomProperties->omPropLabels.GetSize())
			{
				pomPropTab->SetStyleRange(CGXRange(i, 0, i, 0), olStyle.SetValue(pomProperties->omPropLabels[i-1]));
				pomPropTab->SetStyleRange(CGXRange(i, 2, i, 2), CGXStyle().SetChoiceList("...")
										.SetControl(GX_IDS_CTRL_PUSHBTN));
				pomPropTab->SetControlTextRowCol(i, 2, CString("..."));
				if(i == 3)
				{
					CString olChoiceList;
					if(omCurrentControl == "Form")
					{
						POSITION pos;
						for( pos = ogTanaMap.GetStartPosition(); pos != NULL; )
						{
							CString olKey;
							void *pNul;
							ogTanaMap.GetNextAssoc( pos, olKey, pNul);
							olChoiceList+=olKey + CString("\n");
						}
						pomPropTab->SetStyleRange(CGXRange(i,1,i, 1), CGXStyle().SetHorizontalAlignment(DT_LEFT)
																.SetVerticalAlignment(DT_BOTTOM)
																.SetControl(GX_IDS_CTRL_COMBOBOX)
																.SetChoiceList(olChoiceList));
					}
					else
					{
						if(!omCurrentTable.IsEmpty())
						{
							CCSPtrArray<RecordSet> olRecords;

							ogBCD.GetRecords("SYS", "TANA", omCurrentTable, &olRecords);
							for(int j = 0; j < olRecords.GetSize(); j++)
							{
								olChoiceList+=olRecords[j].Values[ogBCD.GetFieldIndex("SYS", "FINA")] + CString("\n");
							}
							olRecords.DeleteAll();
							pomPropTab->SetStyleRange(CGXRange(i,1,i, 1), CGXStyle().SetHorizontalAlignment(DT_LEFT)
																	.SetVerticalAlignment(DT_BOTTOM)
																	.SetControl(GX_IDS_CTRL_COMBOBOX)
																	.SetChoiceList(olChoiceList));
						}
					}
				}//end if(i == 3)
				if(i == 4  && omCurrentControl == "edit")
				{
					CString olChoiceList;
					CString olFieldTypes = "DATE,TIME,DTIM,TRIM,LONG,BOOL,URNO,MIN2HHMM,MIN2HH,HHMM2MIN,HHMM2HH,HH2MIN,HH2HHMM";
					CString olDescription = "dd.mm.YYYY,HH:MM,Date and time,trimed string,Number,TRUE/FALSE,URNO,DB->90min == GUI->01:30 or 0130,DB->90min == GUI->1.5h,DB->01:30 or 01300 == GUI 90min,DB->01:30 or 01300 == GUI->1.5h,DB->1.5h == GUI->90min,DB->1.5h == 01:30 or 0130";



					CStringArray olFtypeArr;
					CStringArray olDescArr;
					ExtractItemList(olDescription, &olDescArr, ',');
					int ilC2 = ExtractItemList(olFieldTypes, &olFtypeArr, ',');
					for(int j = 0; j < ilC2; j++)
					{
						olChoiceList += olFtypeArr[j] + CString("\t") + olDescArr[j] + CString("\n");
					}
					pomPropTab->SetStyleRange(CGXRange(i,1,i, 1), CGXStyle().SetHorizontalAlignment(DT_LEFT)
															.SetVerticalAlignment(DT_BOTTOM)
															.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
															.SetChoiceList(olChoiceList));
				}//end if(i == 4  && omCurrentControl == "edit")
			}
			else
			{
				pomPropTab->SetStyleRange(CGXRange(i, 0,  i, 0), olStyle.SetValue(""));
			}
		}// end for
		CString olText;
		olText = "Properties: " + pomProperties->omCtrlType;
		SetWindowText(olText);
//		pomPropTab->LockUpdate(TRUE);

		if(omCurrentControl == "Form")
		{
			pomPropTab->SetValueRange(CGXRange(1,1),pomProperties->omLabel);
			pomPropTab->SetValueRange(CGXRange(2,1),pomProperties->omName);
			pomPropTab->SetValueRange(CGXRange(3,1),((UFormProperties*)pomProperties)->omTable);
			pomPropTab->SetValueRange(CGXRange(4,1),pomProperties->omOrientation);
			pomPropTab->SetValueRange(CGXRange(5,1),(LONG)pomProperties->omRect.left);
			pomPropTab->SetValueRange(CGXRange(6,1),(LONG)pomProperties->omRect.top);
			pomPropTab->SetValueRange(CGXRange(7,1),(LONG)(pomProperties->omRect.right - pomProperties->omRect.left));
			pomPropTab->SetValueRange(CGXRange(8,1),(LONG)(pomProperties->omRect.bottom - pomProperties->omRect.top));
			pomPropTab->SetValueRange(CGXRange(9,1),(LONG)pomProperties->omBackColor);
			pomPropTab->SetStyleRange ( CGXRange(9,1), CGXStyle().SetInterior(pomProperties->omBackColor) ); 

			pomPropTab->SetValueRange(CGXRange(10,1),(LONG)pomProperties->omFontColor);
			pomPropTab->SetStyleRange ( CGXRange(10,1), CGXStyle().SetInterior(pomProperties->omFontColor) ); 
			pomPropTab->SetValueRange(CGXRange(11,1),(LONG)pomProperties->omBorderColor);
			pomPropTab->SetStyleRange ( CGXRange(11,1), CGXStyle().SetInterior(pomProperties->omBorderColor) ); 
			if(pomProperties->bmEnabled == TRUE)
				pomPropTab->SetValueRange(CGXRange(12,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(12,1),"FALSE");
			if(pomProperties->bmEnabled == TRUE)
				pomPropTab->SetValueRange(CGXRange(13,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(13,1),"FALSE");
			pomPropTab->SetValueRange(CGXRange(14,1),(LONG)pomProperties->imBorderWidth);
			if(pomProperties->bmModal == TRUE)
				pomPropTab->SetValueRange(CGXRange(15,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(15,1),"FALSE");
		}
		if(omCurrentControl == "button")
		{
			pomPropTab->SetValueRange(CGXRange(1,1),pomProperties->omLabel);
			pomPropTab->SetValueRange(CGXRange(2,1),pomProperties->omName);
			pomPropTab->SetValueRange(CGXRange(3,1),pomProperties->omDBField);
			pomPropTab->SetValueRange(CGXRange(4,1),pomProperties->omOrientation);
			pomPropTab->SetValueRange(CGXRange(5,1),(LONG)pomProperties->omRect.left);
			pomPropTab->SetValueRange(CGXRange(6,1),(LONG)pomProperties->omRect.top);
			pomPropTab->SetValueRange(CGXRange(7,1),(LONG)(pomProperties->omRect.right - pomProperties->omRect.left));
			pomPropTab->SetValueRange(CGXRange(8,1),(LONG)(pomProperties->omRect.bottom - pomProperties->omRect.top));
			pomPropTab->SetValueRange(CGXRange(9,1),(LONG)pomProperties->omBackColor);
			pomPropTab->SetStyleRange ( CGXRange(9,1), CGXStyle().SetInterior(pomProperties->omBackColor) ); 

			pomPropTab->SetValueRange(CGXRange(10,1),(LONG)pomProperties->omFontColor);
			pomPropTab->SetStyleRange ( CGXRange(10,1), CGXStyle().SetInterior(pomProperties->omFontColor) ); 
			pomPropTab->SetValueRange(CGXRange(11,1),(LONG)pomProperties->omBorderColor);
			pomPropTab->SetStyleRange ( CGXRange(11,1), CGXStyle().SetInterior(pomProperties->omBorderColor) ); 
			if(pomProperties->bmEnabled == TRUE)
				pomPropTab->SetValueRange(CGXRange(12,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(12,1),"FALSE");
			if(pomProperties->bmEnabled == TRUE)
				pomPropTab->SetValueRange(CGXRange(13,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(13,1),"FALSE");
			pomPropTab->SetValueRange(CGXRange(14,1),(LONG)pomProperties->imBorderWidth);
		}
		if(omCurrentControl == "edit")
		{
			pomPropTab->SetValueRange(CGXRange(1,1),pomProperties->omLabel);
			pomPropTab->SetValueRange(CGXRange(2,1),pomProperties->omName);
			pomPropTab->SetValueRange(CGXRange(3,1),pomProperties->omDBField);
			pomPropTab->SetValueRange(CGXRange(4,1),pomProperties->omFieldType);
			pomPropTab->SetValueRange(CGXRange(5,1),pomProperties->omFormatString);
			pomPropTab->SetValueRange(CGXRange(6,1),pomProperties->omOrientation);


			pomPropTab->SetValueRange(CGXRange(7,1),(LONG)pomProperties->omRect.left);
			pomPropTab->SetValueRange(CGXRange(8,1),(LONG)pomProperties->omRect.top);
			pomPropTab->SetValueRange(CGXRange(9,1),(LONG)(pomProperties->omRect.right - pomProperties->omRect.left));
			pomPropTab->SetValueRange(CGXRange(10,1),(LONG)(pomProperties->omRect.bottom - pomProperties->omRect.top));
			pomPropTab->SetValueRange(CGXRange(11,1),(LONG)pomProperties->omBackColor);
			pomPropTab->SetStyleRange ( CGXRange(11,1), CGXStyle().SetInterior(pomProperties->omBackColor) ); 

			pomPropTab->SetValueRange(CGXRange(12,1),(LONG)pomProperties->omFontColor);
			pomPropTab->SetStyleRange ( CGXRange(12,1), CGXStyle().SetInterior(pomProperties->omFontColor) ); 
			pomPropTab->SetValueRange(CGXRange(13,1),(LONG)pomProperties->omBorderColor);
			pomPropTab->SetStyleRange ( CGXRange(13,1), CGXStyle().SetInterior(pomProperties->omBorderColor) ); 
			if(pomProperties->bmEnabled == TRUE)
				pomPropTab->SetValueRange(CGXRange(14,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(14,1),"FALSE");
			if(pomProperties->bmVisible == TRUE)
				pomPropTab->SetValueRange(CGXRange(15,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(15,1),"FALSE");
			pomPropTab->SetValueRange(CGXRange(16,1),(LONG)pomProperties->imBorderWidth);
			pomPropTab->SetValueRange(CGXRange(17,1),(LONG)pomProperties->imTextLimit);
			if(pomProperties->bmMandatory == TRUE)
				pomPropTab->SetValueRange(CGXRange(18,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(18,1),"FALSE");
			if(pomProperties->bmMultiLine == TRUE)
				pomPropTab->SetValueRange(CGXRange(19,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(19,1),"FALSE");
			pomPropTab->SetValueRange(CGXRange(20,1), pomProperties->omMasterName);
		}
		if(omCurrentControl == "checkbox")
		{
			pomPropTab->SetValueRange(CGXRange(1,1),pomProperties->omLabel);
			pomPropTab->SetValueRange(CGXRange(2,1),pomProperties->omName);
			pomPropTab->SetValueRange(CGXRange(3,1),pomProperties->omDBField);
			pomPropTab->SetValueRange(CGXRange(4,1),pomProperties->omOrientation);
			pomPropTab->SetValueRange(CGXRange(5,1),(LONG)pomProperties->omRect.left);
			pomPropTab->SetValueRange(CGXRange(6,1),(LONG)pomProperties->omRect.top);
			pomPropTab->SetValueRange(CGXRange(7,1),(LONG)(pomProperties->omRect.right - pomProperties->omRect.left));
			pomPropTab->SetValueRange(CGXRange(8,1),(LONG)(pomProperties->omRect.bottom - pomProperties->omRect.top));
			pomPropTab->SetValueRange(CGXRange(9,1),(LONG)pomProperties->omBackColor);
			pomPropTab->SetStyleRange ( CGXRange(9,1), CGXStyle().SetInterior(pomProperties->omBackColor) ); 

			pomPropTab->SetValueRange(CGXRange(10,1),(LONG)pomProperties->omFontColor);
			pomPropTab->SetStyleRange ( CGXRange(10,1), CGXStyle().SetInterior(pomProperties->omFontColor) ); 
			pomPropTab->SetValueRange(CGXRange(11,1),(LONG)pomProperties->omBorderColor);
			pomPropTab->SetStyleRange ( CGXRange(11,1), CGXStyle().SetInterior(pomProperties->omBorderColor) ); 
			if(pomProperties->bmEnabled == TRUE)
				pomPropTab->SetValueRange(CGXRange(12,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(12,1),"FALSE");
			if(pomProperties->bmEnabled == TRUE)
				pomPropTab->SetValueRange(CGXRange(13,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(13,1),"FALSE");
			pomPropTab->SetValueRange(CGXRange(14,1),(LONG)pomProperties->imBorderWidth);
			pomPropTab->SetValueRange(CGXRange(15,1), pomProperties->omCheckedValue);
		}
		if(omCurrentControl == "radio")
		{
			pomPropTab->SetValueRange(CGXRange(1,1),pomProperties->omLabel);
			pomPropTab->SetValueRange(CGXRange(2,1),pomProperties->omName);
			pomPropTab->SetValueRange(CGXRange(3,1),pomProperties->omDBField);
			pomPropTab->SetValueRange(CGXRange(4,1),pomProperties->omOrientation);
			pomPropTab->SetValueRange(CGXRange(5,1),(LONG)pomProperties->omRect.left);
			pomPropTab->SetValueRange(CGXRange(6,1),(LONG)pomProperties->omRect.top);
			pomPropTab->SetValueRange(CGXRange(7,1),(LONG)(pomProperties->omRect.right - pomProperties->omRect.left));
			pomPropTab->SetValueRange(CGXRange(8,1),(LONG)(pomProperties->omRect.bottom - pomProperties->omRect.top));
			pomPropTab->SetValueRange(CGXRange(9,1),(LONG)pomProperties->omBackColor);
			pomPropTab->SetStyleRange ( CGXRange(9,1), CGXStyle().SetInterior(pomProperties->omBackColor) ); 

			pomPropTab->SetValueRange(CGXRange(10,1),(LONG)pomProperties->omFontColor);
			pomPropTab->SetStyleRange ( CGXRange(10,1), CGXStyle().SetInterior(pomProperties->omFontColor) ); 
			pomPropTab->SetValueRange(CGXRange(11,1),(LONG)pomProperties->omBorderColor);
			pomPropTab->SetStyleRange ( CGXRange(11,1), CGXStyle().SetInterior(pomProperties->omBorderColor) ); 
			if(pomProperties->bmEnabled == TRUE)
				pomPropTab->SetValueRange(CGXRange(12,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(12,1),"FALSE");
			if(pomProperties->bmVisible == TRUE)
				pomPropTab->SetValueRange(CGXRange(13,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(13,1),"FALSE");

			if(pomProperties->bmIsMaster == TRUE)
				pomPropTab->SetValueRange(CGXRange(14,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(14,1),"FALSE");

			pomPropTab->SetValueRange(CGXRange(15,1), pomProperties->omMasterName);
			pomPropTab->SetValueRange(CGXRange(16,1), pomProperties->omCheckedValue);
		}
		if(omCurrentControl == "combobox")
		{
			pomPropTab->SetValueRange(CGXRange(1,1),pomProperties->omLabel);
			pomPropTab->SetValueRange(CGXRange(2,1),pomProperties->omName);
			pomPropTab->SetValueRange(CGXRange(3,1),pomProperties->omDBField);
			pomPropTab->SetValueRange(CGXRange(4,1),pomProperties->omOrientation);
			pomPropTab->SetValueRange(CGXRange(5,1),(LONG)pomProperties->omRect.left);
			pomPropTab->SetValueRange(CGXRange(6,1),(LONG)pomProperties->omRect.top);
			pomPropTab->SetValueRange(CGXRange(7,1),(LONG)(pomProperties->omRect.right - pomProperties->omRect.left));
			pomPropTab->SetValueRange(CGXRange(8,1),(LONG)(pomProperties->omRect.bottom - pomProperties->omRect.top));
			pomPropTab->SetValueRange(CGXRange(9,1),(LONG)pomProperties->omBackColor);
			pomPropTab->SetStyleRange ( CGXRange(9,1), CGXStyle().SetInterior(pomProperties->omBackColor) ); 

			pomPropTab->SetValueRange(CGXRange(10,1),(LONG)pomProperties->omFontColor);
			pomPropTab->SetStyleRange ( CGXRange(10,1), CGXStyle().SetInterior(pomProperties->omFontColor) ); 
			pomPropTab->SetValueRange(CGXRange(11,1),(LONG)pomProperties->omBorderColor);
			pomPropTab->SetStyleRange ( CGXRange(11,1), CGXStyle().SetInterior(pomProperties->omBorderColor) ); 
			if(pomProperties->bmEnabled == TRUE)
				pomPropTab->SetValueRange(CGXRange(12,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(12,1),"FALSE");
			if(pomProperties->bmEnabled == TRUE)
				pomPropTab->SetValueRange(CGXRange(13,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(13,1),"FALSE");
			pomPropTab->SetValueRange(CGXRange(14,1),(LONG)pomProperties->imBorderWidth);
			pomPropTab->SetValueRange(CGXRange(15,1),(LONG)pomProperties->imTextLimit);
			if(pomProperties->bmMandatory == TRUE)
				pomPropTab->SetValueRange(CGXRange(16,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(16,1),"FALSE");
			CString olDefaults;
			for(i = 0; i < pomProperties->omDefaultValues.GetSize(); i++)
			{
				if((i+1) ==  pomProperties->omDefaultValues.GetSize())
				{
					olDefaults+=pomProperties->omDefaultValues[i];
				}
				else
				{
					olDefaults+=pomProperties->omDefaultValues[i] + CString(";");
				}
			}
			pomPropTab->SetValueRange(CGXRange(17,1),olDefaults);
			pomPropTab->SetValueRange(CGXRange(18,1),pomProperties->Refe);
			
			/*
			 * <<< cla Convencience for converting old to new
			 */
			if((pomProperties->HiddenRefe.IsEmpty() == TRUE) && (pomProperties->Refe.IsEmpty() == FALSE))
			{
//			   pomProperties->HiddenRefe = pomProperties->Refe;
			}
			pomPropTab->SetValueRange(CGXRange(19,1),pomProperties->HiddenRefe);

		}
		if(omCurrentControl == "static")
		{
			pomPropTab->SetValueRange(CGXRange(1,1),pomProperties->omLabel);
			pomPropTab->SetValueRange(CGXRange(2,1),pomProperties->omName);
			pomPropTab->SetValueRange(CGXRange(3,1),pomProperties->omDBField);
			pomPropTab->SetValueRange(CGXRange(4,1),pomProperties->omOrientation);
			pomPropTab->SetValueRange(CGXRange(5,1),(LONG)pomProperties->omRect.left);
			pomPropTab->SetValueRange(CGXRange(6,1),(LONG)pomProperties->omRect.top);
			pomPropTab->SetValueRange(CGXRange(7,1),(LONG)(pomProperties->omRect.right - pomProperties->omRect.left));
			pomPropTab->SetValueRange(CGXRange(8,1),(LONG)(pomProperties->omRect.bottom - pomProperties->omRect.top));
			pomPropTab->SetValueRange(CGXRange(9,1),(LONG)pomProperties->omBackColor);
			pomPropTab->SetStyleRange ( CGXRange(9,1), CGXStyle().SetInterior(pomProperties->omBackColor) ); 

			pomPropTab->SetValueRange(CGXRange(10,1),(LONG)pomProperties->omFontColor);
			pomPropTab->SetStyleRange ( CGXRange(10,1), CGXStyle().SetInterior(pomProperties->omFontColor) ); 
			pomPropTab->SetValueRange(CGXRange(11,1),(LONG)pomProperties->omBorderColor);
			pomPropTab->SetStyleRange ( CGXRange(11,1), CGXStyle().SetInterior(pomProperties->omBorderColor) ); 
			if(pomProperties->bmVisible == TRUE)
				pomPropTab->SetValueRange(CGXRange(12,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(12,1),"FALSE");
			pomPropTab->SetValueRange(CGXRange(13,1),(LONG)pomProperties->imBorderWidth);

		}
		if(omCurrentControl == "line")
		{
		}
		if(omCurrentControl == "rectangle")
		{
			pomPropTab->SetValueRange(CGXRange(1,1),pomProperties->omName);
			pomPropTab->SetValueRange(CGXRange(2,1),(LONG)pomProperties->omRect.left);
			pomPropTab->SetValueRange(CGXRange(3,1),(LONG)pomProperties->omRect.top);
			pomPropTab->SetValueRange(CGXRange(4,1),(LONG)(pomProperties->omRect.right - pomProperties->omRect.left));
			pomPropTab->SetValueRange(CGXRange(5,1),(LONG)(pomProperties->omRect.bottom - pomProperties->omRect.top));
			pomPropTab->SetValueRange(CGXRange(6,1),(LONG)pomProperties->omBackColor);
			pomPropTab->SetStyleRange ( CGXRange(6,1), CGXStyle().SetInterior(pomProperties->omBackColor) ); 

			pomPropTab->SetValueRange(CGXRange(7,1),(LONG)pomProperties->omFontColor);
			pomPropTab->SetStyleRange ( CGXRange(7,1), CGXStyle().SetInterior(pomProperties->omFontColor) ); 
			pomPropTab->SetValueRange(CGXRange(8,1),(LONG)pomProperties->omBorderColor);
			pomPropTab->SetStyleRange ( CGXRange(8,1), CGXStyle().SetInterior(pomProperties->omBorderColor) ); 
			if(pomProperties->bmVisible == TRUE)
				pomPropTab->SetValueRange(CGXRange(9,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(9,1),"FALSE");
			pomPropTab->SetValueRange(CGXRange(10,1),(LONG)pomProperties->imBorderWidth);

		}
		if(omCurrentControl == "grid")
		{
			UGridCtrlProperties *polGridProperties = (UGridCtrlProperties *)pomProperties;
			pomPropTab->SetValueRange(CGXRange(1,1),polGridProperties->omName);//Name
			pomPropTab->SetValueRange(CGXRange(2,1),polGridProperties->omGridTable);//DB-Table
			pomPropTab->SetValueRange(CGXRange(3,1),polGridProperties->omMasterDetail/*omMasterName*/);//Masterreference
			//omFieldlist
			CString olList;
			for(i = 0; i < polGridProperties->omFieldlist.GetSize(); i++)
			{
				if((i+1) ==  polGridProperties->omFieldlist.GetSize())
				{
					olList+=polGridProperties->omFieldlist[i];
				}
				else
				{
					olList+=polGridProperties->omFieldlist[i] + CString(";");
				}
			}
			pomPropTab->SetValueRange(CGXRange(4,1),olList);//Fieldlist
			//omTypelist
			olList = "";
			for(i = 0; i < polGridProperties->omTypelist.GetSize(); i++)
			{
				if((i+1) ==  polGridProperties->omTypelist.GetSize())
				{
					olList+=polGridProperties->omTypelist[i];
				}
				else
				{
					olList+=polGridProperties->omTypelist[i] + CString(";");
				}
			}
			pomPropTab->SetValueRange(CGXRange(5,1),olList);//Typelist
			//Haederlist
			olList = "";
			for(i = 0; i < polGridProperties->omHaederlist.GetSize(); i++)
			{
				if((i+1) ==  polGridProperties->omHaederlist.GetSize())
				{
					olList+=polGridProperties->omHaederlist[i];
				}
				else
				{
					olList+=polGridProperties->omHaederlist[i] + CString(";");
				}
			}
			pomPropTab->SetValueRange(CGXRange(6,1),olList);//Typelist
			//Visiblelist
			olList = "";
			for(i = 0; i < polGridProperties->omVisiblelist.GetSize(); i++)
			{
				if((i+1) ==  polGridProperties->omVisiblelist.GetSize())
				{
					olList+=polGridProperties->omVisiblelist[i];
				}
				else
				{
					olList+=polGridProperties->omVisiblelist[i] + CString(";");
				}
			}
			pomPropTab->SetValueRange(CGXRange(7,1),olList);//Typelist
			//omLookupfieldList
			olList = "";
			for(i = 0; i < polGridProperties->omLookupfieldList.GetSize(); i++)
			{
				if((i+1) ==  polGridProperties->omLookupfieldList.GetSize())
				{
					olList+=polGridProperties->omLookupfieldList[i];
				}
				else
				{
					olList+=polGridProperties->omLookupfieldList[i] + CString(";");
				}
			}
			pomPropTab->SetValueRange(CGXRange(8,1),olList);//Typelist
			//omMandatorylist
			olList = "";
			for(i = 0; i < polGridProperties->omMandatorylist.GetSize(); i++)
			{
				if((i+1) ==  polGridProperties->omMandatorylist.GetSize())
				{
					olList+=polGridProperties->omMandatorylist[i];
				}
				else
				{
					olList+=polGridProperties->omMandatorylist[i] + CString(";");
				}
			}
			pomPropTab->SetValueRange(CGXRange(9,1),olList);//Typelist
			//omHeadersizes
			olList = "";
			for(i = 0; i < polGridProperties->omHeadersizes.GetSize(); i++)
			{
				if((i+1) ==  polGridProperties->omHeadersizes.GetSize())
				{
					olList+=polGridProperties->omHeadersizes[i];
				}
				else
				{
					olList+=polGridProperties->omHeadersizes[i] + CString(";");
				}
			}
			pomPropTab->SetValueRange(CGXRange(10,1),olList);//Typelist
			pomPropTab->SetValueRange(CGXRange(11,1),(LONG)polGridProperties->omRect.left);
			pomPropTab->SetValueRange(CGXRange(12,1),(LONG)polGridProperties->omRect.top);
			pomPropTab->SetValueRange(CGXRange(13,1),(LONG)(polGridProperties->omRect.right - polGridProperties->omRect.left));
			pomPropTab->SetValueRange(CGXRange(14,1),(LONG)(polGridProperties->omRect.bottom - polGridProperties->omRect.top));
			if(polGridProperties->bmEnabled == TRUE)
				pomPropTab->SetValueRange(CGXRange(15,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(15,1),"FALSE");
			if(polGridProperties->bmVisible == TRUE)
				pomPropTab->SetValueRange(CGXRange(16,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(16,1),"FALSE");
		}
		if(omCurrentControl == "group")
		{
			pomPropTab->SetValueRange(CGXRange(1,1),pomProperties->omLabel);
			pomPropTab->SetValueRange(CGXRange(2,1),pomProperties->omName);
			pomPropTab->SetValueRange(CGXRange(3,1),pomProperties->omDBField);
			pomPropTab->SetValueRange(CGXRange(4,1),pomProperties->omOrientation);
			pomPropTab->SetValueRange(CGXRange(5,1),(LONG)pomProperties->omRect.left);
			pomPropTab->SetValueRange(CGXRange(6,1),(LONG)pomProperties->omRect.top);
			pomPropTab->SetValueRange(CGXRange(7,1),(LONG)(pomProperties->omRect.right - pomProperties->omRect.left));
			pomPropTab->SetValueRange(CGXRange(8,1),(LONG)(pomProperties->omRect.bottom - pomProperties->omRect.top));
			pomPropTab->SetValueRange(CGXRange(9,1),(LONG)pomProperties->omBackColor);
			pomPropTab->SetStyleRange ( CGXRange(9,1), CGXStyle().SetInterior(pomProperties->omBackColor) ); 

			pomPropTab->SetValueRange(CGXRange(10,1),(LONG)pomProperties->omFontColor);
			pomPropTab->SetStyleRange ( CGXRange(10,1), CGXStyle().SetInterior(pomProperties->omFontColor) ); 
			pomPropTab->SetValueRange(CGXRange(11,1),(LONG)pomProperties->omBorderColor);
			pomPropTab->SetStyleRange ( CGXRange(11,1), CGXStyle().SetInterior(pomProperties->omBorderColor) ); 
			if(pomProperties->bmEnabled == TRUE)
				pomPropTab->SetValueRange(CGXRange(12,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(12,1),"FALSE");
			if(pomProperties->bmEnabled == TRUE)
				pomPropTab->SetValueRange(CGXRange(13,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(13,1),"FALSE");
			pomPropTab->SetValueRange(CGXRange(14,1),(LONG)pomProperties->imBorderWidth);
		}
		if(omCurrentControl == "listbox")
		{
		}

		pomPropTab->Invalidate();
		//pomPropTab->GetParam()->EnableUndo(TRUE);
	}
	else
	{
		SetWindowText("Properties (No control)...");
	}
	omOnOkButton.SetWindowText(_T("Edit OnOK"));
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void PropertiesDlg::PostNcDestroy() 
{
	delete this;
	pogPropertiesDlg = NULL;
	CDialog::PostNcDestroy();
}


void PropertiesDlg::SetObjectProperties(UCtrlProperties *popProperties, UFormControlObj *popCtrlObj, CString opTable, FormDesignerView* popView )
{
	if(imCurrentPage == PROP_PROPERTIES)
	{
		SetProperties(popProperties, popCtrlObj, opTable, popView);
	}
	if(imCurrentPage == PROP_EVENTS)
	{
		SetEvents(popProperties, popCtrlObj, opTable, popView);
	}
}
void PropertiesDlg::SetProperties(UCtrlProperties *popProperties, 
								  UFormControlObj *popCtrlObj, 
								  CString opTable, 
						          FormDesignerView* popView)
{
	BOOL bOld = pomPropTab->LockUpdate();
	pomProperties = popProperties;
	pomCtrlObj = popCtrlObj;
	omCurrentTable = opTable;
	pomView = popView;

	if(popProperties != NULL)
	{
		omCurrentControl = popProperties->omCtrlType;
		CGXRangeList olList;
		olList.AddTail(new CGXRange(0, 0, 100, 1));
		pomPropTab->SetRowCount(0);
		pomPropTab->SetRowCount(100);
		pomPropTab->ClearCells(olList);
		CGXStyle olStyle;
		for( int i = 1; i < 100; i++)
		{
			if(i-1 < pomProperties->omPropLabels.GetSize())
			{
				pomPropTab->SetStyleRange(CGXRange(i, 0,  i, 0), olStyle.SetValue(pomProperties->omPropLabels[i-1]));
				pomPropTab->SetStyleRange(CGXRange(i, 2, i, 2), CGXStyle().SetChoiceList("...")
										.SetControl(GX_IDS_CTRL_PUSHBTN));
				pomPropTab->SetControlTextRowCol(i, 2, CString("..."));
				if(i == 3)
				{
					CString olChoiceList;
					if(omCurrentControl == "Form")
					{
						POSITION pos;
						for( pos = ogTanaMap.GetStartPosition(); pos != NULL; )
						{
							CString olKey;
							void *pNul;
							ogTanaMap.GetNextAssoc( pos, olKey, pNul);
							olChoiceList+=olKey + CString("\n");
						}
						pomPropTab->SetStyleRange(CGXRange(i,1,i, 1), CGXStyle().SetHorizontalAlignment(DT_LEFT)
																.SetVerticalAlignment(DT_BOTTOM)
																.SetControl(GX_IDS_CTRL_COMBOBOX)
																.SetChoiceList(olChoiceList));
					}
					else
					{
						if(!omCurrentTable.IsEmpty())
						{
							CCSPtrArray<RecordSet> olRecords;

							ogBCD.GetRecords("SYS", "TANA", omCurrentTable, &olRecords);
							for(int j = 0; j < olRecords.GetSize(); j++)
							{
								olChoiceList+=olRecords[j].Values[ogBCD.GetFieldIndex("SYS", "FINA")] + CString("\n");
							}
							olRecords.DeleteAll();
							pomPropTab->SetStyleRange(CGXRange(i,1,i, 1), CGXStyle().SetHorizontalAlignment(DT_LEFT)
																	.SetVerticalAlignment(DT_BOTTOM)
																	.SetControl(GX_IDS_CTRL_COMBOBOX)
																	.SetChoiceList(olChoiceList));
						}
					}
				}// end if i == 3
				if(i == 4  && omCurrentControl == "edit")
				{
					CString olChoiceList;
					CString olFieldTypes = "DATE,TIME,DTIM,TRIM,LONG,BOOL,URNO,MIN2HHMM,MIN2HH,HHMM2MIN,HHMM2HH,HH2MIN,HH2HHMM";
					CString olDescription = "dd.mm.YYYY,HH:MM,Date and time,trimed string,Number,TRUE/FALSE,URNO,DB->90min == GUI->01:30 or 0130,DB->90min == GUI->1.5h,DB->01:30 or 01300 == GUI 90min,DB->01:30 or 01300 == GUI->1.5h,DB->1.5h == GUI->90min,DB->1.5h == 01:30 or 0130";



					CStringArray olFtypeArr;
					CStringArray olDescArr;
					ExtractItemList(olDescription, &olDescArr, ',');
					int ilC2 = ExtractItemList(olFieldTypes, &olFtypeArr, ',');
					for(int j = 0; j < ilC2; j++)
					{
						olChoiceList += olFtypeArr[j] + CString("\t") + olDescArr[j] + CString("\n");
					}
					pomPropTab->SetStyleRange(CGXRange(i,1,i, 1), CGXStyle().SetHorizontalAlignment(DT_LEFT)
															.SetVerticalAlignment(DT_BOTTOM)
															.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
															.SetChoiceList(olChoiceList));
				}//end if(i == 4  && omCurrentControl == "edit")

			}
			else
			{
				pomPropTab->SetStyleRange(CGXRange(i, 0,  i, 0), olStyle.SetValue(""));
			}
		}
		CString olText;
		olText = "Properties: " + pomProperties->omCtrlType;
		SetWindowText(olText);
		if(omCurrentControl == "Form")
		{
			pomPropTab->SetValueRange(CGXRange(1,1),pomProperties->omLabel);
			pomPropTab->SetValueRange(CGXRange(2,1),pomProperties->omName);
			pomPropTab->SetValueRange(CGXRange(3,1),((UFormProperties*)pomProperties)->omTable);
			pomPropTab->SetValueRange(CGXRange(4,1),pomProperties->omOrientation);
			pomPropTab->SetValueRange(CGXRange(5,1),(LONG)pomProperties->omRect.left);
			pomPropTab->SetValueRange(CGXRange(6,1),(LONG)pomProperties->omRect.top);
			pomPropTab->SetValueRange(CGXRange(7,1),(LONG)(pomProperties->omRect.right - pomProperties->omRect.left));
			pomPropTab->SetValueRange(CGXRange(8,1),(LONG)(pomProperties->omRect.bottom - pomProperties->omRect.top));
			pomPropTab->SetValueRange(CGXRange(9,1),(LONG)pomProperties->omBackColor);
			pomPropTab->SetStyleRange ( CGXRange(9,1), CGXStyle().SetInterior(pomProperties->omBackColor) ); 

			pomPropTab->SetValueRange(CGXRange(10,1),(LONG)pomProperties->omFontColor);
			pomPropTab->SetStyleRange ( CGXRange(10,1), CGXStyle().SetInterior(pomProperties->omFontColor) ); 
			pomPropTab->SetValueRange(CGXRange(11,1),(LONG)pomProperties->omBorderColor);
			pomPropTab->SetStyleRange ( CGXRange(11,1), CGXStyle().SetInterior(pomProperties->omBorderColor) ); 
			if(pomProperties->bmEnabled == TRUE)
				pomPropTab->SetValueRange(CGXRange(12,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(12,1),"FALSE");
			if(pomProperties->bmEnabled == TRUE)
				pomPropTab->SetValueRange(CGXRange(13,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(13,1),"FALSE");
			pomPropTab->SetValueRange(CGXRange(14,1),(LONG)pomProperties->imBorderWidth);
			if(pomProperties->bmModal == TRUE)
				pomPropTab->SetValueRange(CGXRange(15,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(15,1),"FALSE");
		}
		if(omCurrentControl == "button")
		{
			pomPropTab->SetValueRange(CGXRange(1,1),pomProperties->omLabel);
			pomPropTab->SetValueRange(CGXRange(2,1),pomProperties->omName);
			pomPropTab->SetValueRange(CGXRange(3,1),pomProperties->omDBField);
			pomPropTab->SetValueRange(CGXRange(4,1),pomProperties->omOrientation);
			pomPropTab->SetValueRange(CGXRange(5,1),(LONG)pomProperties->omRect.left);
			pomPropTab->SetValueRange(CGXRange(6,1),(LONG)pomProperties->omRect.top);
			pomPropTab->SetValueRange(CGXRange(7,1),(LONG)(pomProperties->omRect.right - pomProperties->omRect.left));
			pomPropTab->SetValueRange(CGXRange(8,1),(LONG)(pomProperties->omRect.bottom - pomProperties->omRect.top));
			pomPropTab->SetValueRange(CGXRange(9,1),(LONG)pomProperties->omBackColor);
			pomPropTab->SetStyleRange ( CGXRange(9,1), CGXStyle().SetInterior(pomProperties->omBackColor) ); 

			pomPropTab->SetValueRange(CGXRange(10,1),(LONG)pomProperties->omFontColor);
			pomPropTab->SetStyleRange ( CGXRange(10,1), CGXStyle().SetInterior(pomProperties->omFontColor) ); 
			pomPropTab->SetValueRange(CGXRange(11,1),(LONG)pomProperties->omBorderColor);
			pomPropTab->SetStyleRange ( CGXRange(11,1), CGXStyle().SetInterior(pomProperties->omBorderColor) ); 
			if(pomProperties->bmEnabled == TRUE)
				pomPropTab->SetValueRange(CGXRange(12,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(12,1),"FALSE");
			if(pomProperties->bmEnabled == TRUE)
				pomPropTab->SetValueRange(CGXRange(13,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(13,1),"FALSE");
			pomPropTab->SetValueRange(CGXRange(14,1),(LONG)pomProperties->imBorderWidth);
		}
		if(omCurrentControl == "edit")
		{
			pomPropTab->SetValueRange(CGXRange(1,1),pomProperties->omLabel);
			pomPropTab->SetValueRange(CGXRange(2,1),pomProperties->omName);
			pomPropTab->SetValueRange(CGXRange(3,1),pomProperties->omDBField);
			pomPropTab->SetValueRange(CGXRange(4,1),pomProperties->omFieldType);
			pomPropTab->SetValueRange(CGXRange(5,1),pomProperties->omFormatString);
			pomPropTab->SetValueRange(CGXRange(6,1),pomProperties->omOrientation);


			pomPropTab->SetValueRange(CGXRange(7,1),(LONG)pomProperties->omRect.left);
			pomPropTab->SetValueRange(CGXRange(8,1),(LONG)pomProperties->omRect.top);
			pomPropTab->SetValueRange(CGXRange(9,1),(LONG)(pomProperties->omRect.right - pomProperties->omRect.left));
			pomPropTab->SetValueRange(CGXRange(10,1),(LONG)(pomProperties->omRect.bottom - pomProperties->omRect.top));
			pomPropTab->SetValueRange(CGXRange(11,1),(LONG)pomProperties->omBackColor);
			pomPropTab->SetStyleRange ( CGXRange(11,1), CGXStyle().SetInterior(pomProperties->omBackColor) ); 

			pomPropTab->SetValueRange(CGXRange(12,1),(LONG)pomProperties->omFontColor);
			pomPropTab->SetStyleRange ( CGXRange(12,1), CGXStyle().SetInterior(pomProperties->omFontColor) ); 
			pomPropTab->SetValueRange(CGXRange(13,1),(LONG)pomProperties->omBorderColor);
			pomPropTab->SetStyleRange ( CGXRange(13,1), CGXStyle().SetInterior(pomProperties->omBorderColor) ); 
			if(pomProperties->bmEnabled == TRUE)
				pomPropTab->SetValueRange(CGXRange(14,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(14,1),"FALSE");
			if(pomProperties->bmVisible == TRUE)
				pomPropTab->SetValueRange(CGXRange(15,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(15,1),"FALSE");
			pomPropTab->SetValueRange(CGXRange(16,1),(LONG)pomProperties->imBorderWidth);
			pomPropTab->SetValueRange(CGXRange(17,1),(LONG)pomProperties->imTextLimit);
			if(pomProperties->bmMandatory == TRUE)
				pomPropTab->SetValueRange(CGXRange(18,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(18,1),"FALSE");
			if(pomProperties->bmMultiLine == TRUE)
				pomPropTab->SetValueRange(CGXRange(19,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(19,1),"FALSE");
			pomPropTab->SetValueRange(CGXRange(20,1), pomProperties->omMasterName);
		}
		if(omCurrentControl == "checkbox")
		{
			pomPropTab->SetValueRange(CGXRange(1,1),pomProperties->omLabel);
			pomPropTab->SetValueRange(CGXRange(2,1),pomProperties->omName);
			pomPropTab->SetValueRange(CGXRange(3,1),pomProperties->omDBField);
			pomPropTab->SetValueRange(CGXRange(4,1),pomProperties->omOrientation);
			pomPropTab->SetValueRange(CGXRange(5,1),(LONG)pomProperties->omRect.left);
			pomPropTab->SetValueRange(CGXRange(6,1),(LONG)pomProperties->omRect.top);
			pomPropTab->SetValueRange(CGXRange(7,1),(LONG)(pomProperties->omRect.right - pomProperties->omRect.left));
			pomPropTab->SetValueRange(CGXRange(8,1),(LONG)(pomProperties->omRect.bottom - pomProperties->omRect.top));
			pomPropTab->SetValueRange(CGXRange(9,1),(LONG)pomProperties->omBackColor);
			pomPropTab->SetStyleRange ( CGXRange(9,1), CGXStyle().SetInterior(pomProperties->omBackColor) ); 

			pomPropTab->SetValueRange(CGXRange(10,1),(LONG)pomProperties->omFontColor);
			pomPropTab->SetStyleRange ( CGXRange(10,1), CGXStyle().SetInterior(pomProperties->omFontColor) ); 
			pomPropTab->SetValueRange(CGXRange(11,1),(LONG)pomProperties->omBorderColor);
			pomPropTab->SetStyleRange ( CGXRange(11,1), CGXStyle().SetInterior(pomProperties->omBorderColor) ); 
			if(pomProperties->bmEnabled == TRUE)
				pomPropTab->SetValueRange(CGXRange(12,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(12,1),"FALSE");
			if(pomProperties->bmEnabled == TRUE)
				pomPropTab->SetValueRange(CGXRange(13,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(13,1),"FALSE");
			pomPropTab->SetValueRange(CGXRange(14,1),(LONG)pomProperties->imBorderWidth);
			pomPropTab->SetValueRange(CGXRange(15,1), pomProperties->omCheckedValue);
		}
		if(omCurrentControl == "radio")
		{
			pomPropTab->SetValueRange(CGXRange(1,1),pomProperties->omLabel);
			pomPropTab->SetValueRange(CGXRange(2,1),pomProperties->omName);
			pomPropTab->SetValueRange(CGXRange(3,1),pomProperties->omDBField);
			pomPropTab->SetValueRange(CGXRange(4,1),pomProperties->omOrientation);
			pomPropTab->SetValueRange(CGXRange(5,1),(LONG)pomProperties->omRect.left);
			pomPropTab->SetValueRange(CGXRange(6,1),(LONG)pomProperties->omRect.top);
			pomPropTab->SetValueRange(CGXRange(7,1),(LONG)(pomProperties->omRect.right - pomProperties->omRect.left));
			pomPropTab->SetValueRange(CGXRange(8,1),(LONG)(pomProperties->omRect.bottom - pomProperties->omRect.top));
			pomPropTab->SetValueRange(CGXRange(9,1),(LONG)pomProperties->omBackColor);
			pomPropTab->SetStyleRange ( CGXRange(9,1), CGXStyle().SetInterior(pomProperties->omBackColor) ); 

			pomPropTab->SetValueRange(CGXRange(10,1),(LONG)pomProperties->omFontColor);
			pomPropTab->SetStyleRange ( CGXRange(10,1), CGXStyle().SetInterior(pomProperties->omFontColor) ); 
			pomPropTab->SetValueRange(CGXRange(11,1),(LONG)pomProperties->omBorderColor);
			pomPropTab->SetStyleRange ( CGXRange(11,1), CGXStyle().SetInterior(pomProperties->omBorderColor) ); 
			if(pomProperties->bmEnabled == TRUE)
				pomPropTab->SetValueRange(CGXRange(12,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(12,1),"FALSE");
			if(pomProperties->bmVisible == TRUE)
				pomPropTab->SetValueRange(CGXRange(13,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(13,1),"FALSE");

			if(pomProperties->bmIsMaster == TRUE)
				pomPropTab->SetValueRange(CGXRange(14,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(14,1),"FALSE");

			pomPropTab->SetValueRange(CGXRange(15,1), pomProperties->omMasterName);
			pomPropTab->SetValueRange(CGXRange(16,1), pomProperties->omCheckedValue);
		}
		if(omCurrentControl == "combobox")
		{
			pomPropTab->SetValueRange(CGXRange(1,1),pomProperties->omLabel);
			pomPropTab->SetValueRange(CGXRange(2,1),pomProperties->omName);
			pomPropTab->SetValueRange(CGXRange(3,1),pomProperties->omDBField);
			pomPropTab->SetValueRange(CGXRange(4,1),pomProperties->omOrientation);
			pomPropTab->SetValueRange(CGXRange(5,1),(LONG)pomProperties->omRect.left);
			pomPropTab->SetValueRange(CGXRange(6,1),(LONG)pomProperties->omRect.top);
			pomPropTab->SetValueRange(CGXRange(7,1),(LONG)(pomProperties->omRect.right - pomProperties->omRect.left));
			pomPropTab->SetValueRange(CGXRange(8,1),(LONG)(pomProperties->omRect.bottom - pomProperties->omRect.top));
			pomPropTab->SetValueRange(CGXRange(9,1),(LONG)pomProperties->omBackColor);
			pomPropTab->SetStyleRange ( CGXRange(9,1), CGXStyle().SetInterior(pomProperties->omBackColor) ); 

			pomPropTab->SetValueRange(CGXRange(10,1),(LONG)pomProperties->omFontColor);
			pomPropTab->SetStyleRange ( CGXRange(10,1), CGXStyle().SetInterior(pomProperties->omFontColor) ); 
			pomPropTab->SetValueRange(CGXRange(11,1),(LONG)pomProperties->omBorderColor);
			pomPropTab->SetStyleRange ( CGXRange(11,1), CGXStyle().SetInterior(pomProperties->omBorderColor) ); 
			if(pomProperties->bmEnabled == TRUE)
				pomPropTab->SetValueRange(CGXRange(12,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(12,1),"FALSE");
			if(pomProperties->bmEnabled == TRUE)
				pomPropTab->SetValueRange(CGXRange(13,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(13,1),"FALSE");
			pomPropTab->SetValueRange(CGXRange(14,1),(LONG)pomProperties->imBorderWidth);
			pomPropTab->SetValueRange(CGXRange(15,1),(LONG)pomProperties->imTextLimit);
			if(pomProperties->bmMandatory == TRUE)
				pomPropTab->SetValueRange(CGXRange(16,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(16,1),"FALSE");
			CString olDefaults;
			for(i = 0; i < pomProperties->omDefaultValues.GetSize(); i++)
			{
				if((i+1) ==  pomProperties->omDefaultValues.GetSize())
				{
					olDefaults+=pomProperties->omDefaultValues[i];
				}
				else
				{
					olDefaults+=pomProperties->omDefaultValues[i] + CString(";");
				}
			}
			pomPropTab->SetValueRange(CGXRange(17,1),olDefaults);
			pomPropTab->SetValueRange(CGXRange(18,1),popProperties->Refe);
			pomPropTab->SetValueRange(CGXRange(19,1),popProperties->HiddenRefe);
		}
		if(omCurrentControl == "static")
		{
			pomPropTab->SetValueRange(CGXRange(1,1),pomProperties->omLabel);
			pomPropTab->SetValueRange(CGXRange(2,1),pomProperties->omName);
			pomPropTab->SetValueRange(CGXRange(3,1),pomProperties->omDBField);
			pomPropTab->SetValueRange(CGXRange(4,1),pomProperties->omOrientation);
			pomPropTab->SetValueRange(CGXRange(5,1),(LONG)pomProperties->omRect.left);
			pomPropTab->SetValueRange(CGXRange(6,1),(LONG)pomProperties->omRect.top);
			pomPropTab->SetValueRange(CGXRange(7,1),(LONG)(pomProperties->omRect.right - pomProperties->omRect.left));
			pomPropTab->SetValueRange(CGXRange(8,1),(LONG)(pomProperties->omRect.bottom - pomProperties->omRect.top));
			pomPropTab->SetValueRange(CGXRange(9,1),(LONG)pomProperties->omBackColor);
			pomPropTab->SetStyleRange ( CGXRange(9,1), CGXStyle().SetInterior(pomProperties->omBackColor) ); 

			pomPropTab->SetValueRange(CGXRange(10,1),(LONG)pomProperties->omFontColor);
			pomPropTab->SetStyleRange ( CGXRange(10,1), CGXStyle().SetInterior(pomProperties->omFontColor) ); 
			pomPropTab->SetValueRange(CGXRange(11,1),(LONG)pomProperties->omBorderColor);
			pomPropTab->SetStyleRange ( CGXRange(11,1), CGXStyle().SetInterior(pomProperties->omBorderColor) ); 
			if(pomProperties->bmVisible == TRUE)
				pomPropTab->SetValueRange(CGXRange(12,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(12,1),"FALSE");
			pomPropTab->SetValueRange(CGXRange(13,1),(LONG)pomProperties->imBorderWidth);
		}
		if(omCurrentControl == "line")
		{
		}
		if(omCurrentControl == "rectangle")
		{
			//pomPropTab->SetValueRange(CGXRange(1,1), const CString& str, 
			//pomPropTab->SetValueRange(CGXRange(2,1),pomProperties->omLabel
			pomPropTab->SetValueRange(CGXRange(1,1),pomProperties->omName);
			//pomPropTab->SetValueRange(CGXRange(4,1),pomProperties->omOrientation
			pomPropTab->SetValueRange(CGXRange(2,1),(LONG)pomProperties->omRect.left);
			pomPropTab->SetValueRange(CGXRange(3,1),(LONG)pomProperties->omRect.top);
			pomPropTab->SetValueRange(CGXRange(4,1),(LONG)(pomProperties->omRect.right - pomProperties->omRect.left));
			pomPropTab->SetValueRange(CGXRange(5,1),(LONG)(pomProperties->omRect.bottom - pomProperties->omRect.top));
			pomPropTab->SetValueRange(CGXRange(6,1),(LONG)pomProperties->omBackColor);
			pomPropTab->SetStyleRange ( CGXRange(6,1), CGXStyle().SetInterior(pomProperties->omBackColor) ); 

			pomPropTab->SetValueRange(CGXRange(7,1),(LONG)pomProperties->omFontColor);
			pomPropTab->SetStyleRange ( CGXRange(7,1), CGXStyle().SetInterior(pomProperties->omFontColor) ); 
			pomPropTab->SetValueRange(CGXRange(8,1),(LONG)pomProperties->omBorderColor);
			pomPropTab->SetStyleRange ( CGXRange(8,1), CGXStyle().SetInterior(pomProperties->omBorderColor) ); 
			//SetValueRange(CGXRange(9,1),pomProperties->bmEnabled
			if(pomProperties->bmVisible == TRUE)
				pomPropTab->SetValueRange(CGXRange(9,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(9,1),"FALSE");
			pomPropTab->SetValueRange(CGXRange(10,1),(LONG)pomProperties->imBorderWidth);

		}
		if(omCurrentControl == "grid")
		{
			UGridCtrlProperties *polGridProperties = (UGridCtrlProperties *)pomProperties;
			pomPropTab->SetValueRange(CGXRange(1,1),polGridProperties->omName);//Name
			pomPropTab->SetValueRange(CGXRange(2,1),polGridProperties->omGridTable);//DB-Table
			pomPropTab->SetValueRange(CGXRange(3,1),polGridProperties->omMasterDetail/*omMasterName*/);//Masterreference
			//omFieldlist
			CString olList;
			for(i = 0; i < polGridProperties->omFieldlist.GetSize(); i++)
			{
				if((i+1) ==  polGridProperties->omFieldlist.GetSize())
				{
					olList+=polGridProperties->omFieldlist[i];
				}
				else
				{
					olList+=polGridProperties->omFieldlist[i] + CString(";");
				}
			}
			pomPropTab->SetValueRange(CGXRange(4,1),olList);//Fieldlist
			//omTypelist
			olList = "";
			for(i = 0; i < polGridProperties->omTypelist.GetSize(); i++)
			{
				if((i+1) ==  polGridProperties->omTypelist.GetSize())
				{
					olList+=polGridProperties->omTypelist[i];
				}
				else
				{
					olList+=polGridProperties->omTypelist[i] + CString(";");
				}
			}
			pomPropTab->SetValueRange(CGXRange(5,1),olList);//Typelist
			//Haederlist
			olList = "";
			for(i = 0; i < polGridProperties->omHaederlist.GetSize(); i++)
			{
				if((i+1) ==  polGridProperties->omHaederlist.GetSize())
				{
					olList+=polGridProperties->omHaederlist[i];
				}
				else
				{
					olList+=polGridProperties->omHaederlist[i] + CString(";");
				}
			}
			pomPropTab->SetValueRange(CGXRange(6,1),olList);//Typelist
			//Visiblelist
			olList = "";
			for(i = 0; i < polGridProperties->omVisiblelist.GetSize(); i++)
			{
				if((i+1) ==  polGridProperties->omVisiblelist.GetSize())
				{
					olList+=polGridProperties->omVisiblelist[i];
				}
				else
				{
					olList+=polGridProperties->omVisiblelist[i] + CString(";");
				}
			}
			pomPropTab->SetValueRange(CGXRange(7,1),olList);//Typelist
			//omLookupfieldList
			olList = "";
			for(i = 0; i < polGridProperties->omLookupfieldList.GetSize(); i++)
			{
				if((i+1) ==  polGridProperties->omLookupfieldList.GetSize())
				{
					olList+=polGridProperties->omLookupfieldList[i];
				}
				else
				{
					olList+=polGridProperties->omLookupfieldList[i] + CString(";");
				}
			}
			pomPropTab->SetValueRange(CGXRange(8,1),olList);//Typelist
			//omMandatorylist
			olList = "";
			for(i = 0; i < polGridProperties->omMandatorylist.GetSize(); i++)
			{
				if((i+1) ==  polGridProperties->omMandatorylist.GetSize())
				{
					olList+=polGridProperties->omMandatorylist[i];
				}
				else
				{
					olList+=polGridProperties->omMandatorylist[i] + CString(";");
				}
			}
			pomPropTab->SetValueRange(CGXRange(9,1),olList);//Typelist
			//omHeadersizes
			olList = "";
			for(i = 0; i < polGridProperties->omHeadersizes.GetSize(); i++)
			{
				if((i+1) ==  polGridProperties->omHeadersizes.GetSize())
				{
					olList+=polGridProperties->omHeadersizes[i];
				}
				else
				{
					olList+=polGridProperties->omHeadersizes[i] + CString(";");
				}
			}
			pomPropTab->SetValueRange(CGXRange(10,1),olList);//Typelist
			pomPropTab->SetValueRange(CGXRange(11,1),(LONG)polGridProperties->omRect.left);
			pomPropTab->SetValueRange(CGXRange(12,1),(LONG)polGridProperties->omRect.top);
			pomPropTab->SetValueRange(CGXRange(13,1),(LONG)(polGridProperties->omRect.right - polGridProperties->omRect.left));
			pomPropTab->SetValueRange(CGXRange(14,1),(LONG)(polGridProperties->omRect.bottom - polGridProperties->omRect.top));
			if(polGridProperties->bmEnabled == TRUE)
				pomPropTab->SetValueRange(CGXRange(15,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(15,1),"FALSE");
			if(polGridProperties->bmVisible == TRUE)
				pomPropTab->SetValueRange(CGXRange(16,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(16,1),"FALSE");
		}
		if(omCurrentControl == "group")
		{
			pomPropTab->SetValueRange(CGXRange(1,1),pomProperties->omLabel);
			pomPropTab->SetValueRange(CGXRange(2,1),pomProperties->omName);
			pomPropTab->SetValueRange(CGXRange(3,1),pomProperties->omDBField);
			pomPropTab->SetValueRange(CGXRange(4,1),pomProperties->omOrientation);
			pomPropTab->SetValueRange(CGXRange(5,1),(LONG)pomProperties->omRect.left);
			pomPropTab->SetValueRange(CGXRange(6,1),(LONG)pomProperties->omRect.top);
			pomPropTab->SetValueRange(CGXRange(7,1),(LONG)(pomProperties->omRect.right - pomProperties->omRect.left));
			pomPropTab->SetValueRange(CGXRange(8,1),(LONG)(pomProperties->omRect.bottom - pomProperties->omRect.top));
			pomPropTab->SetValueRange(CGXRange(9,1),(LONG)pomProperties->omBackColor);
			pomPropTab->SetStyleRange ( CGXRange(9,1), CGXStyle().SetInterior(pomProperties->omBackColor) ); 

			pomPropTab->SetValueRange(CGXRange(10,1),(LONG)pomProperties->omFontColor);
			pomPropTab->SetStyleRange ( CGXRange(10,1), CGXStyle().SetInterior(pomProperties->omFontColor) ); 
			pomPropTab->SetValueRange(CGXRange(11,1),(LONG)pomProperties->omBorderColor);
			pomPropTab->SetStyleRange ( CGXRange(11,1), CGXStyle().SetInterior(pomProperties->omBorderColor) ); 
			if(pomProperties->bmEnabled == TRUE)
				pomPropTab->SetValueRange(CGXRange(12,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(12,1),"FALSE");
			if(pomProperties->bmEnabled == TRUE)
				pomPropTab->SetValueRange(CGXRange(13,1),"TRUE");
			else
				pomPropTab->SetValueRange(CGXRange(13,1),"FALSE");
			pomPropTab->SetValueRange(CGXRange(14,1),(LONG)pomProperties->imBorderWidth);
		}
		if(omCurrentControl == "listbox")
		{
		}
	}
	else
	{
		pomPropTab->SetRowCount(0);//Clear();//RemoveRows(0, 100);
		SetWindowText("Properties (No control)...");
	}
	pomPropTab->LockUpdate(bOld);
	pomPropTab->Invalidate();

}
// ==================================================================
// 
// FUNCTION :  PropertiesDlg::SetEvents()
// 
// * Description : Provides information for the Event tab
// 
// 
// * Author : [cla AAT/IR], Created : [07.11.00 14:57:11]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// [*popProperties] - The Properties of the Control
// [*popCtrlObj] - Form Control object
// [opTable] - ?
// [popView] - The Designer View
// 
// ==================================================================
void PropertiesDlg::SetEvents(UCtrlProperties *popProperties, 
							  UFormControlObj *popCtrlObj, 
							  CString opTable, 
							  FormDesignerView* popView)
{

	BOOL bOld = pomPropTab->LockUpdate();
	pomProperties = popProperties;
	pomCtrlObj = popCtrlObj;
	omCurrentTable = opTable;
	pomView = popView;
	std::vector<CCntlEvent>::iterator olPos;

	CGXRangeList olList;

	olList.AddTail(new CGXRange(0, 0, 100, 1));
	pomPropTab->SetRowCount(0);
	pomPropTab->SetRowCount(100);
	pomPropTab->ClearCells(olList);

	if(popProperties != NULL)
	{
		omCurrentControl = popProperties->omCtrlType;
		CGXStyle olStyle;
/*
 * <<<cla: Old code removed
		for( int i = 1; i < 100; i++)
		{
			if(i-1 < pomProperties->omEventLabels.GetSize())
			{
				pomPropTab->SetStyleRange(CGXRange(i, 0,  i, 0), olStyle.SetValue(pomProperties->omEventLabels[i-1]));
				pomPropTab->SetStyleRange(CGXRange(i, 2, i, 2), CGXStyle().SetChoiceList("...")
										.SetControl(GX_IDS_CTRL_PUSHBTN));
				pomPropTab->SetControlTextRowCol(i, 2, CString("..."));
			}
		}
		if(omCurrentControl == "button")
		{
		}
*/
		/*
		 * <<<cla: New code introduced
		 */
		for(int indx = 0;indx < popProperties->imNumEvents;indx++)
		{
			pomPropTab->SetStyleRange(CGXRange(indx+1, 0,  indx+1, 0), olStyle.SetValue(pomProperties->omEvents[indx].EventName));
			pomPropTab->SetStyleRange(CGXRange(indx+1, 2, indx+1, 2), CGXStyle().SetChoiceList("...")
										.SetControl(GX_IDS_CTRL_PUSHBTN));
			pomPropTab->SetControlTextRowCol(indx+1, 2, CString("..."));
			/*
			 * Iterate through the Event list and display the
			 * the actual content when matching the DISPID
			*/
			for(olPos = pomProperties->omEventList.begin();olPos != pomProperties->omEventList.end();++olPos)
			{
				if((*olPos) == (pomProperties->omEvents[indx].EventId))
				{
					pomPropTab->SetStyleRange(CGXRange(indx+1, 1, indx+1, 1),
						                      olStyle.SetValue(olPos->m_vbMethodName));
					break;
				}

			}
				
		}

	}
	pomPropTab->LockUpdate(bOld);
	pomPropTab->Invalidate();
}

void PropertiesDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	delete this;
	pogPropertiesDlg = NULL;
	
}



void PropertiesDlg::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	GetWindowRect(&ogPropertiesDlgRect);
	CDialog::OnClose();
	DestroyWindow();
}

LONG PropertiesDlg::OnRowColChanged(UINT wParam, LONG lParam)
{
	if(imCurrentPage == PROP_PROPERTIES)
	{
		RowColChanged_Properties(wParam, lParam);
	}
	if(imCurrentPage == PROP_EVENTS)
	{
		RowColChanged_Event(wParam, lParam);
	}
	return 0L;
}

// Cell of the Grid changed
void PropertiesDlg::RowColChanged_Properties(UINT wParam, LONG lParam)
{
	GxMiniGridInfo *polGrid = (GxMiniGridInfo *)(long)wParam;
	wParam = polGrid->Row;
	if(omCurrentControl == "Form")
	{
		if(wParam == 1) // Label
		{
			pomProperties->omLabel = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				((FormDesignerView*)pomCtrlObj)->Invalidate();
			}
		}
		if(wParam == 2) // Name
		{
			pomProperties->omName = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				((FormDesignerView*)pomCtrlObj)->Invalidate();
				CMultiDocTemplate* pTpl;
				pTpl = (CMultiDocTemplate*)((CUWorkBenchApp*)AfxGetApp())->pDesignerTpl;
				if(pTpl != NULL)
				{
					POSITION rlPos = pTpl->GetFirstDocPosition();
					if(rlPos != NULL)
					{
						while(rlPos != NULL)
						{
							CDocument *polDoc = pTpl->GetNextDoc( rlPos );
							if(polDoc != NULL)
							{
								polDoc->UpdateAllViews(NULL);
							}
						}
					}
				}
				pTpl = (CMultiDocTemplate*)((CUWorkBenchApp*)AfxGetApp())->pFormDesignerTpl;
				if(pTpl != NULL)
				{
					POSITION rlPos = pTpl->GetFirstDocPosition();
					if(rlPos != NULL)
					{
						while(rlPos != NULL)
						{
							CDocument *polDoc = pTpl->GetNextDoc( rlPos );
							if(polDoc != NULL)
							{
								polDoc->SetTitle(pomProperties->omName);
							}
						}
					}
				}
				
			}
		}
		if(wParam == 3) // Main Table
		{
			((UFormProperties*)pomProperties)->omTable = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				((FormDesignerView*)pomCtrlObj)->Invalidate();
			}
		}
		if(wParam == 4) // Orientation
		{
			pomProperties->omOrientation = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				((FormDesignerView*)pomCtrlObj)->Invalidate();
			}
		}
		if(wParam == 5 || wParam == 6 || wParam == 7 || wParam == 8) // Left, top, width, height
		{
			CString olLeft, olTop, olWidth, olHeight;
			olLeft =	pomPropTab->GetValueRowCol(5, 1);
			olTop	=	pomPropTab->GetValueRowCol(6, 1);
			olWidth	=	pomPropTab->GetValueRowCol(7, 1);
			olHeight=	pomPropTab->GetValueRowCol(8, 1);
			int ilLeft, ilTop, ilRight, ilBottom;
			ilLeft		= atoi(olLeft.GetBuffer(0));
			ilTop		= atoi(olTop.GetBuffer(0));
			ilRight		= ilLeft + atoi(olWidth.GetBuffer(0));
			ilBottom	= ilTop + atoi(olHeight.GetBuffer(0));
			CRect olRect = CRect(ilLeft, ilTop, ilRight, ilBottom);
			pomProperties->omRect = olRect;
			if(pomCtrlObj != NULL)
			{
				((FormDesignerView*)pomCtrlObj)->Invalidate();
			}

		}
		if(wParam == 14) // BorderWidth
		{
			CString olWidth = pomPropTab->GetValueRowCol(wParam, 1);
			int		ilWidth = atoi(olWidth.GetBuffer(0));
			pomProperties->imBorderWidth = ilWidth;
			if(pomCtrlObj != NULL)
			{
				((FormDesignerView*)pomCtrlObj)->Invalidate();
			}
			pomPropTab->Invalidate();

		}
		if(wParam == 15) // Modal or Modless
		{
			CString olModal = pomPropTab->GetValueRowCol(wParam, 1);
			if(olModal == "TRUE")
				pomProperties->bmModal = TRUE;
			else
				pomProperties->bmModal = FALSE;
			if(pomCtrlObj != NULL)
			{
				((FormDesignerView*)pomCtrlObj)->Invalidate();
			}
			pomPropTab->Invalidate();

		}
	}
	if(omCurrentControl == "button")
	{
		if(wParam == 1) // Label
		{
			pomProperties->omLabel = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 2) // Name
		{
			pomProperties->omName = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 3) // DBField
		{
			pomProperties->omDBField = pomPropTab->GetValueRowCol(wParam, 1);
			if(!omCurrentTable.IsEmpty())
			{
				if(pomView != NULL)
				{
					pomView->GetDocument()->pomForm->SetFieldInfo(pomPropTab->GetValueRowCol(wParam, 1), omCurrentTable);
				}
			}
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 4) // Orientation
		{
			pomProperties->omOrientation = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 5 || wParam == 6 || wParam == 7 || wParam == 8) // Left, top, width, height
		{
			CString olLeft, olTop, olWidth, olHeight;
			olLeft =	pomPropTab->GetValueRowCol(5, 1);
			olTop	=	pomPropTab->GetValueRowCol(6, 1);
			olWidth	=	pomPropTab->GetValueRowCol(7, 1);
			olHeight=	pomPropTab->GetValueRowCol(8, 1);
			int ilLeft, ilTop, ilRight, ilBottom;
			ilLeft		= atoi(olLeft.GetBuffer(0));
			ilTop		= atoi(olTop.GetBuffer(0));
			ilRight		= ilLeft + atoi(olWidth.GetBuffer(0));
			ilBottom	= ilTop + atoi(olHeight.GetBuffer(0));
			CRect olRect = CRect(ilLeft, ilTop, ilRight, ilBottom);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->MoveTo(olRect);
			}
		}
		if(wParam == 14) // BorderWidth
		{
			CString olWidth = pomPropTab->GetValueRowCol(wParam, 1);
			int		ilWidth = atoi(olWidth.GetBuffer(0));
			pomProperties->imBorderWidth = ilWidth;
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
			pomPropTab->Invalidate();

		}
	}
	if(omCurrentControl == "group")
	{
		if(wParam == 1) // Label
		{
			pomProperties->omLabel = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 2) // Name
		{
			pomProperties->omName = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 3) // DBField
		{
			pomProperties->omDBField = pomPropTab->GetValueRowCol(wParam, 1);
			if(!omCurrentTable.IsEmpty())
			{
				if(pomView != NULL)
				{
					pomView->GetDocument()->pomForm->SetFieldInfo(pomPropTab->GetValueRowCol(wParam, 1), omCurrentTable);
				}
			}
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 4) // Orientation
		{
			pomProperties->omOrientation = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 5 || wParam == 6 || wParam == 7 || wParam == 8) // Left, top, width, height
		{
			CString olLeft, olTop, olWidth, olHeight;
			olLeft =	pomPropTab->GetValueRowCol(5, 1);
			olTop	=	pomPropTab->GetValueRowCol(6, 1);
			olWidth	=	pomPropTab->GetValueRowCol(7, 1);
			olHeight=	pomPropTab->GetValueRowCol(8, 1);
			int ilLeft, ilTop, ilRight, ilBottom;
			ilLeft		= atoi(olLeft.GetBuffer(0));
			ilTop		= atoi(olTop.GetBuffer(0));
			ilRight		= ilLeft + atoi(olWidth.GetBuffer(0));
			ilBottom	= ilTop + atoi(olHeight.GetBuffer(0));
			CRect olRect = CRect(ilLeft, ilTop, ilRight, ilBottom);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->MoveTo(olRect);
			}
		}
		if(wParam == 14) // BorderWidth
		{
			CString olWidth = pomPropTab->GetValueRowCol(wParam, 1);
			int		ilWidth = atoi(olWidth.GetBuffer(0));
			pomProperties->imBorderWidth = ilWidth;
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
			pomPropTab->Invalidate();

		}
	}
	if(omCurrentControl == "edit")
	{
		if(wParam == 1) // Label
		{
			pomProperties->omLabel = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 2) // Name
		{
			pomProperties->omName = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 3) // DBField
		{
			pomProperties->omDBField = pomPropTab->GetValueRowCol(wParam, 1);
			if(!omCurrentTable.IsEmpty())
			{
				if(pomView != NULL)
				{
					pomView->GetDocument()->pomForm->SetFieldInfo(pomPropTab->GetValueRowCol(wParam, 1), omCurrentTable);
				}
			}
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 4) // FieldType
		{
			pomProperties->omFieldType = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 5) // FormatString
		{
			pomProperties->omFormatString = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 6) // Orientation
		{
			pomProperties->omOrientation = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 7 || wParam == 8 || wParam == 9 || wParam == 10) // Left, top, width, height
		{
			CString olLeft, olTop, olWidth, olHeight;
			olLeft =	pomPropTab->GetValueRowCol(7, 1);
			olTop	=	pomPropTab->GetValueRowCol(8, 1);
			olWidth	=	pomPropTab->GetValueRowCol(9, 1);
			olHeight=	pomPropTab->GetValueRowCol(10, 1);
			int ilLeft, ilTop, ilRight, ilBottom;
			ilLeft		= atoi(olLeft.GetBuffer(0));
			ilTop		= atoi(olTop.GetBuffer(0));
			ilRight		= ilLeft + atoi(olWidth.GetBuffer(0));
			ilBottom	= ilTop + atoi(olHeight.GetBuffer(0));
			CRect olRect = CRect(ilLeft, ilTop, ilRight, ilBottom);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->MoveTo(olRect);
			}
		}
		if(wParam == 14) // Enabled
		{
			CString olEnabled = pomPropTab->GetValueRowCol(wParam, 1);
			if(olEnabled != "TRUE")
				pomProperties->bmEnabled = FALSE;
			else
				pomProperties->bmEnabled = TRUE;
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 15) // Visible
		{
			CString olVisible = pomPropTab->GetValueRowCol(wParam, 1);
			if(olVisible != "TRUE")
				pomProperties->bmVisible = FALSE;
			else
				pomProperties->bmVisible = TRUE;
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 16) // BorderWidth
		{
			CString olWidth = pomPropTab->GetValueRowCol(wParam, 1);
			int		ilWidth = atoi(olWidth.GetBuffer(0));
			pomProperties->imBorderWidth = ilWidth;
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
			pomPropTab->Invalidate();

		}
//		pomPropTab->Invalidate();
		if(wParam == 17) // Textlimit
		{
			CString olLimit = pomPropTab->GetValueRowCol(wParam, 1);
			int		ilLimit = atoi(olLimit.GetBuffer(0));
			pomProperties->imTextLimit = ilLimit;
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
			pomPropTab->Invalidate();

		}
		if(wParam == 18) // Mandatory
		{
			CString olMandatory = pomPropTab->GetValueRowCol(wParam, 1);
			if(olMandatory == "TRUE")
				pomProperties->bmMandatory = TRUE;
			else
				pomProperties->bmMandatory = FALSE;
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
			pomPropTab->Invalidate();
		}
		if(wParam == 19) // Multiline
		{
			CString olMultiline = pomPropTab->GetValueRowCol(wParam, 1);
			if(olMultiline == "TRUE")
				pomProperties->bmMultiLine = TRUE;
			else
				pomProperties->bmMultiLine = FALSE;
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
			pomPropTab->Invalidate();
		}
		if(wParam == 20) // MasterName
		{
			CString olMasterName = pomPropTab->GetValueRowCol(wParam, 1);
			pomProperties->omMasterName = olMasterName;
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
			pomPropTab->Invalidate();
		}
	}
	if(omCurrentControl == "checkbox")
	{
		if(wParam == 1) // Label
		{
			pomProperties->omLabel = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 2) // Name
		{
			pomProperties->omName = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 3) // DBField
		{
			pomProperties->omDBField = pomPropTab->GetValueRowCol(wParam, 1);
			if(!omCurrentTable.IsEmpty())
			{
				if(pomView != NULL)
				{
					pomView->GetDocument()->pomForm->SetFieldInfo(pomPropTab->GetValueRowCol(wParam, 1), omCurrentTable);
				}
			}
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 4) // Orientation
		{
			pomProperties->omOrientation = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 5 || wParam == 6 || wParam == 7 || wParam == 8) // Left, top, width, height
		{
			CString olLeft, olTop, olWidth, olHeight;
			olLeft =	pomPropTab->GetValueRowCol(5, 1);
			olTop	=	pomPropTab->GetValueRowCol(6, 1);
			olWidth	=	pomPropTab->GetValueRowCol(7, 1);
			olHeight=	pomPropTab->GetValueRowCol(8, 1);
			int ilLeft, ilTop, ilRight, ilBottom;
			ilLeft		= atoi(olLeft.GetBuffer(0));
			ilTop		= atoi(olTop.GetBuffer(0));
			ilRight		= ilLeft + atoi(olWidth.GetBuffer(0));
			ilBottom	= ilTop + atoi(olHeight.GetBuffer(0));
			CRect olRect = CRect(ilLeft, ilTop, ilRight, ilBottom);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->MoveTo(olRect);
			}
		}
		if(wParam == 14) // BorderWidth
		{
			CString olWidth = pomPropTab->GetValueRowCol(wParam, 1);
			int		ilWidth = atoi(olWidth.GetBuffer(0));
			pomProperties->imBorderWidth = ilWidth;
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
			pomPropTab->Invalidate();

		}
		if(wParam == 15) // True Value
		{
			CString olValue = pomPropTab->GetValueRowCol(wParam, 1);
			pomProperties->omCheckedValue = olValue;
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
			pomPropTab->Invalidate();

		}

	}
	if(omCurrentControl == "radio")
	{
		if(wParam == 1) // Label
		{
			pomProperties->omLabel = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 2) // Name
		{
			pomProperties->omName = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 3) // DBField
		{
			pomProperties->omDBField = pomPropTab->GetValueRowCol(wParam, 1);
			if(!omCurrentTable.IsEmpty())
			{
				if(pomView != NULL)
				{
					pomView->GetDocument()->pomForm->SetFieldInfo(pomPropTab->GetValueRowCol(wParam, 1), omCurrentTable);
				}
			}
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 4) // Orientation
		{
			pomProperties->omOrientation = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 5 || wParam == 6 || wParam == 7 || wParam == 8) // Left, top, width, height
		{
			CString olLeft, olTop, olWidth, olHeight;
			olLeft =	pomPropTab->GetValueRowCol(5, 1);
			olTop	=	pomPropTab->GetValueRowCol(6, 1);
			olWidth	=	pomPropTab->GetValueRowCol(7, 1);
			olHeight=	pomPropTab->GetValueRowCol(8, 1);
			int ilLeft, ilTop, ilRight, ilBottom;
			ilLeft		= atoi(olLeft.GetBuffer(0));
			ilTop		= atoi(olTop.GetBuffer(0));
			ilRight		= ilLeft + atoi(olWidth.GetBuffer(0));
			ilBottom	= ilTop + atoi(olHeight.GetBuffer(0));
			CRect olRect = CRect(ilLeft, ilTop, ilRight, ilBottom);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->MoveTo(olRect);
			}
		}
		if(wParam == 14) // IsMaster
		{
			CString olValue = pomPropTab->GetValueRowCol(wParam, 1);
			if(olValue == "TRUE")
				pomProperties->bmIsMaster = TRUE;
			else
				pomProperties->bmIsMaster = FALSE;
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
			pomPropTab->Invalidate();
		}
		if(wParam == 15) // Corresponding Master
		{
			CString olValue = pomPropTab->GetValueRowCol(wParam, 1);
			pomProperties->omMasterName = olValue;
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
			pomPropTab->Invalidate();
		}
		if(wParam == 16) // Checked Value
		{
			CString olValue = pomPropTab->GetValueRowCol(wParam, 1);
			pomProperties->omCheckedValue = olValue;
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
			pomPropTab->Invalidate();
		}
	}
	if(omCurrentControl == "combobox")
	{
		if(wParam == 1) // Label
		{
			pomProperties->omLabel = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 2) // Name
		{
			pomProperties->omName = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 3) // DBField
		{
			pomProperties->omDBField = pomPropTab->GetValueRowCol(wParam, 1);
			if(!omCurrentTable.IsEmpty())
			{
				if(pomView != NULL)
				{
					pomView->GetDocument()->pomForm->SetFieldInfo(pomPropTab->GetValueRowCol(wParam, 1), omCurrentTable);
				}
			}
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 4) // Orientation
		{
			pomProperties->omOrientation = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 5 || wParam == 6 || wParam == 7 || wParam == 8) // Left, top, width, height
		{
			CString olLeft, olTop, olWidth, olHeight;
			olLeft =	pomPropTab->GetValueRowCol(5, 1);
			olTop	=	pomPropTab->GetValueRowCol(6, 1);
			olWidth	=	pomPropTab->GetValueRowCol(7, 1);
			olHeight=	pomPropTab->GetValueRowCol(8, 1);
			int ilLeft, ilTop, ilRight, ilBottom;
			ilLeft		= atoi(olLeft.GetBuffer(0));
			ilTop		= atoi(olTop.GetBuffer(0));
			ilRight		= ilLeft + atoi(olWidth.GetBuffer(0));
			ilBottom	= ilTop + atoi(olHeight.GetBuffer(0));
			CRect olRect = CRect(ilLeft, ilTop, ilRight, ilBottom);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->MoveTo(olRect);
			}
		}
		if(wParam == 14) // BorderWidth
		{
			CString olWidth = pomPropTab->GetValueRowCol(wParam, 1);
			int		ilWidth = atoi(olWidth.GetBuffer(0));
			pomProperties->imBorderWidth = ilWidth;
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
			pomPropTab->Invalidate();

		}
		if(wParam == 15) // Textlimit
		{
			CString olLimit = pomPropTab->GetValueRowCol(wParam, 1);
			int		ilLimit = atoi(olLimit.GetBuffer(0));
			pomProperties->imTextLimit = ilLimit;
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
			pomPropTab->Invalidate();

		}
		if(wParam == 16) // Mandatory
		{
			CString olMandatory = pomPropTab->GetValueRowCol(wParam, 1);
			if(olMandatory == "TRUE")
				pomProperties->bmMandatory = TRUE;
			else
				pomProperties->bmMandatory = FALSE;
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
			pomPropTab->Invalidate();
		}
		if(wParam == 17) // Default Values
		{
			pomProperties->omDefaultValues.RemoveAll();
			CString olDefaultValues = pomPropTab->GetValueRowCol(wParam, 1);
			CStringArray olArr;
			ExtractItemList(olDefaultValues, &olArr, ';');
			for(int ilc = 0; ilc < olArr.GetSize(); ilc++)
			{
				pomProperties->omDefaultValues.Add(olArr[ilc]);
			}
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
			pomPropTab->Invalidate();
		}

		if(wParam == 18) // Refe
		{
			pomProperties->Refe = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				UFormField * polFormField;
				CString olTab, olFld;
				if(pomCtrlObj != NULL)
				{
					olTab = pomCtrlObj->pomDocument->pomForm->omTable;
					olFld = pomProperties->omDBField;
					polFormField = pomCtrlObj->pomDocument->pomForm->GetFieldInfo(olFld, olTab);
					if(polFormField != NULL)
					{
						polFormField->Refe = pomProperties->Refe;
					}
					pomCtrlObj->Invalidate();
				}
			}
		}
		if(wParam == 19) // HiddenRefe
		{
			pomProperties->HiddenRefe = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				UFormField * polFormField;
				CString olTab, olFld;
				if(pomCtrlObj != NULL)
				{
					olTab = pomCtrlObj->pomDocument->pomForm->omTable;
					olFld = pomProperties->omDBField;
					polFormField = pomCtrlObj->pomDocument->pomForm->GetFieldInfo(olFld, olTab);
					if(polFormField != NULL)
					{
						polFormField->HiddenRefe = pomProperties->HiddenRefe;
					}
					pomCtrlObj->Invalidate();
				}
			}
		}
	}
	if(omCurrentControl == "line")
	{
	}
	if(omCurrentControl == "static")
	{
		if(wParam == 1) // Label
		{
			pomProperties->omLabel = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 2) // Name
		{
			pomProperties->omName = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 3) // DBField
		{
			pomProperties->omDBField = pomPropTab->GetValueRowCol(wParam, 1);
			if(!omCurrentTable.IsEmpty())
			{
				if(pomView != NULL)
				{
					pomView->GetDocument()->pomForm->SetFieldInfo(pomPropTab->GetValueRowCol(wParam, 1), omCurrentTable);
				}
			}
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 4) // Orientation
		{
			pomProperties->omOrientation = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 5 || wParam == 6 || wParam == 7 || wParam == 8) // Left, top, width, height
		{
			CString olLeft, olTop, olWidth, olHeight;
			olLeft =	pomPropTab->GetValueRowCol(5, 1);
			olTop	=	pomPropTab->GetValueRowCol(6, 1);
			olWidth	=	pomPropTab->GetValueRowCol(7, 1);
			olHeight=	pomPropTab->GetValueRowCol(8, 1);
			int ilLeft, ilTop, ilRight, ilBottom;
			ilLeft		= atoi(olLeft.GetBuffer(0));
			ilTop		= atoi(olTop.GetBuffer(0));
			ilRight		= ilLeft + atoi(olWidth.GetBuffer(0));
			ilBottom	= ilTop + atoi(olHeight.GetBuffer(0));
			CRect olRect = CRect(ilLeft, ilTop, ilRight, ilBottom);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->MoveTo(olRect);
			}
		}
		if(wParam == 12) // Visible
		{
			CString olStr = pomPropTab->GetValueRowCol(wParam, 1);
			if(olStr == "TRUE")
				pomProperties->bmVisible = TRUE;
			else
				pomProperties->bmVisible = FALSE;
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 13) // BorderWidth
		{
			CString olWidth = pomPropTab->GetValueRowCol(wParam, 1);
			int		ilWidth = atoi(olWidth.GetBuffer(0));
			pomProperties->imBorderWidth = ilWidth;
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
			pomPropTab->Invalidate();

		}
	}
	if(omCurrentControl == "rectangle")
	{
		if(wParam == 10) // BorderWidth
		{
			CString olWidth = pomPropTab->GetValueRowCol(wParam, 1);
			int		ilWidth = atoi(olWidth.GetBuffer(0));
			pomProperties->imBorderWidth = ilWidth;
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
			pomPropTab->Invalidate();

		}
		if(wParam == 1) // Name
		{
			pomProperties->omName = pomPropTab->GetValueRowCol(wParam, 1);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 2 || wParam == 3 || wParam == 4 || wParam == 5) // Left, top, width, height
		{
			CString olLeft, olTop, olWidth, olHeight;
			olLeft =	pomPropTab->GetValueRowCol(2, 1);
			olTop	=	pomPropTab->GetValueRowCol(3, 1);
			olWidth	=	pomPropTab->GetValueRowCol(4, 1);
			olHeight=	pomPropTab->GetValueRowCol(5, 1);
			int ilLeft, ilTop, ilRight, ilBottom;
			ilLeft		= atoi(olLeft.GetBuffer(0));
			ilTop		= atoi(olTop.GetBuffer(0));
			ilRight		= ilLeft + atoi(olWidth.GetBuffer(0));
			ilBottom	= ilTop + atoi(olHeight.GetBuffer(0));
			CRect olRect = CRect(ilLeft, ilTop, ilRight, ilBottom);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->MoveTo(olRect);
			}
		}
	}
	if(omCurrentControl == "listbox")
	{
	}
	if(omCurrentControl == "grid")
	{
/*
*/		UGridCtrlProperties *polGridProperties = (UGridCtrlProperties *)pomProperties;
		CStringArray olList;
		if( wParam == 1)//Gridname
		{
			CString olValue;
			olValue = pomPropTab->GetValueRowCol(wParam, 1);
			polGridProperties->omName = olValue;
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
			
		}
		if( wParam == 2)//DB-Table
		{
			CString olValue;
			olValue = pomPropTab->GetValueRowCol(wParam, 1);
			polGridProperties->omGridTable = olValue;
		}
		if( wParam == 3)//Masterreference (omMasterDetail)
		{
			CString olValue;
			olValue = pomPropTab->GetValueRowCol(wParam, 1);
			polGridProperties->omMasterDetail = olValue;
		}
		if( wParam == 4)//Fieldlist
		{
			CString olValue;
			olValue = pomPropTab->GetValueRowCol(wParam, 1);
			olList.RemoveAll();
			ExtractItemList(olValue, &olList, ';');
			polGridProperties->omFieldlist.RemoveAll();
			for(int i = 0; i < olList.GetSize(); i++)
			{
				polGridProperties->omFieldlist.Add(olList[i]);
			}
		}
		if( wParam == 5)//Typelist
		{
			CString olValue;
			olValue = pomPropTab->GetValueRowCol(wParam, 1);
			olList.RemoveAll();
			ExtractItemList(olValue, &olList, ';');
			polGridProperties->omTypelist.RemoveAll();
			for(int i = 0; i < olList.GetSize(); i++)
			{
				polGridProperties->omTypelist.Add(olList[i]);
			}
		}
		if( wParam == 6)//Headerlist
		{
			CString olValue;
			olValue = pomPropTab->GetValueRowCol(wParam, 1);
			olList.RemoveAll();
			ExtractItemList(olValue, &olList, ';');
			polGridProperties->omHaederlist.RemoveAll();
			for(int i = 0; i < olList.GetSize(); i++)
			{
				polGridProperties->omHaederlist.Add(olList[i]);
			}
		}
		if( wParam == 7)//Visiblelist (of columns)
		{
			CString olValue;
			olValue = pomPropTab->GetValueRowCol(wParam, 1);
			olList.RemoveAll();
			ExtractItemList(olValue, &olList, ';');
			polGridProperties->omVisiblelist.RemoveAll();
			for(int i = 0; i < olList.GetSize(); i++)
			{
				polGridProperties->omVisiblelist.Add(olList[i]);
			}
		}
		if( wParam == 8)//Refe or LookupList
		{
			CString olValue;
			olValue = pomPropTab->GetValueRowCol(wParam, 1);
			olList.RemoveAll();
			ExtractItemList(olValue, &olList, ';');
			polGridProperties->omLookupfieldList.RemoveAll();
			for(int i = 0; i < olList.GetSize(); i++)
			{
				polGridProperties->omLookupfieldList.Add(olList[i]);
			}
		}
		if( wParam == 9)//Mandatorylist
		{
			CString olValue;
			olValue = pomPropTab->GetValueRowCol(wParam, 1);
			olList.RemoveAll();
			ExtractItemList(olValue, &olList, ';');
			polGridProperties->omMandatorylist.RemoveAll();
			for(int i = 0; i < olList.GetSize(); i++)
			{
				polGridProperties->omMandatorylist.Add(olList[i]);
			}
		}
		if( wParam == 10)//Columnsizelist
		{
			CString olValue;
			olValue = pomPropTab->GetValueRowCol(wParam, 1);
			olList.RemoveAll();
			ExtractItemList(olValue, &olList, ';');
			polGridProperties->omHeadersizes.RemoveAll();
			for(int i = 0; i < olList.GetSize(); i++)
			{
				polGridProperties->omHeadersizes.Add(olList[i]);
			}
		}
		if( wParam == 11 || wParam == 12 || wParam == 13 || wParam == 14)//left, top, width, height
		{
			CString olLeft, olTop, olWidth, olHeight;
			olLeft =	pomPropTab->GetValueRowCol(11, 1);
			olTop	=	pomPropTab->GetValueRowCol(12, 1);
			olWidth	=	pomPropTab->GetValueRowCol(13, 1);
			olHeight=	pomPropTab->GetValueRowCol(14, 1);
			int ilLeft, ilTop, ilRight, ilBottom;
			ilLeft		= atoi(olLeft.GetBuffer(0));
			ilTop		= atoi(olTop.GetBuffer(0));
			ilRight		= ilLeft + atoi(olWidth.GetBuffer(0));
			ilBottom	= ilTop + atoi(olHeight.GetBuffer(0));
			CRect olRect = CRect(ilLeft, ilTop, ilRight, ilBottom);
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->MoveTo(olRect);
			}
		}
		if(wParam == 15) // Enabled
		{
			CString olEnabled = pomPropTab->GetValueRowCol(wParam, 1);
			if(olEnabled != "TRUE")
				polGridProperties->bmEnabled = FALSE;
			else
				polGridProperties->bmEnabled = TRUE;
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
		if(wParam == 16) //Visible of the grid itself
		{
			CString olVisible = pomPropTab->GetValueRowCol(wParam, 1);
			if(olVisible != "TRUE")
				polGridProperties->bmVisible = FALSE;
			else
				polGridProperties->bmVisible = TRUE;
			if(pomCtrlObj != NULL)
			{
				pomCtrlObj->Invalidate();
			}
		}
	}
}

void PropertiesDlg::RowColChanged_Event(UINT wParam, LONG lParam)
{
}

LONG PropertiesDlg::OnPropertyUpdate(UINT wParam, LONG lParam)
{
	SetObjectProperties((UCtrlProperties *)wParam, (UFormControlObj *)lParam);
	return 0L;
}

LONG PropertiesDlg::OnGridButtonClicked(UINT wParam, LONG lParam)
{
	if(imCurrentPage == PROP_PROPERTIES)
	{
		GridButtonClicked_Properties(wParam, lParam);
	}
	if(imCurrentPage == PROP_EVENTS)
	{
		GridButtonClicked_Event(wParam, lParam);
	}
	return 0L;
}

//Button of the grid was clicked
void PropertiesDlg::GridButtonClicked_Properties(UINT wParam, LONG lParam)
{
	if( omCurrentControl == "Form")
	{
		if(wParam == 9)
		{
			CColorDialog polDlg(0,0,this);
			if(polDlg.DoModal() == IDOK)
			{
				COLORREF olNewColor;
				olNewColor = polDlg.GetColor();
				CString olStr; olStr.Format("%ld", (long)olNewColor);
				pomPropTab->SetValueRange(CGXRange(wParam, 1), olStr); 
				pomPropTab->SetStyleRange ( CGXRange(wParam,1), CGXStyle().SetInterior(olNewColor) ); 
				pomProperties->omBackColor = olNewColor;
				if(pomCtrlObj != NULL)
				{
					((FormDesignerView*)pomCtrlObj)->Invalidate();
				}
			}
		}
		if(wParam == 10)
		{
			CColorDialog polDlg(0,0,this);
			if(polDlg.DoModal() == IDOK)
			{
				COLORREF olNewColor;
				olNewColor = polDlg.GetColor();
				CString olStr; olStr.Format("%ld", (long)olNewColor);
				pomPropTab->SetValueRange(CGXRange(wParam, 1), olStr); 
				pomPropTab->SetStyleRange ( CGXRange(wParam,1), CGXStyle().SetInterior(olNewColor) ); 
				pomProperties->omFontColor = olNewColor;
				if(pomCtrlObj != NULL)
				{
					((FormDesignerView*)pomCtrlObj)->Invalidate();
				}
			}
		}
		if(wParam == 11)
		{
			CColorDialog polDlg(0,0,this);
			if(polDlg.DoModal() == IDOK)
			{
				COLORREF olNewColor;
				olNewColor = polDlg.GetColor();
				CString olStr; olStr.Format("%ld", (long)olNewColor);
				pomPropTab->SetValueRange(CGXRange(wParam, 1), olStr); 
				pomPropTab->SetStyleRange ( CGXRange(wParam,1), CGXStyle().SetInterior(olNewColor) ); 
				pomProperties->omBorderColor = olNewColor;
				if(pomCtrlObj != NULL)
				{
					((FormDesignerView*)pomCtrlObj)->Invalidate();
				}
			}
		}
	}
	if( omCurrentControl == "button")
	{
		if(wParam == 9)
		{
			CColorDialog polDlg(0,0,this);
			if(polDlg.DoModal() == IDOK)
			{
				COLORREF olNewColor;
				olNewColor = polDlg.GetColor();
				CString olStr; olStr.Format("%ld", (long)olNewColor);
				pomPropTab->SetValueRange(CGXRange(wParam, 1), olStr); 
				pomPropTab->SetStyleRange ( CGXRange(wParam,1), CGXStyle().SetInterior(olNewColor) ); 
				pomProperties->omBackColor = olNewColor;
				if(pomCtrlObj != NULL)
				{
					pomCtrlObj->Invalidate();
				}
			}
		}
		if(wParam == 10)
		{
			CColorDialog polDlg(0,0,this);
			if(polDlg.DoModal() == IDOK)
			{
				COLORREF olNewColor;
				olNewColor = polDlg.GetColor();
				CString olStr; olStr.Format("%ld", (long)olNewColor);
				pomPropTab->SetValueRange(CGXRange(wParam, 1), olStr); 
				pomPropTab->SetStyleRange ( CGXRange(wParam,1), CGXStyle().SetInterior(olNewColor) ); 
				pomProperties->omFontColor = olNewColor;
				if(pomCtrlObj != NULL)
				{
					pomCtrlObj->Invalidate();
				}
			}
		}
		if(wParam == 11)
		{
			CColorDialog polDlg(0,0,this);
			if(polDlg.DoModal() == IDOK)
			{
				COLORREF olNewColor;
				olNewColor = polDlg.GetColor();
				CString olStr; olStr.Format("%ld", (long)olNewColor);
				pomPropTab->SetValueRange(CGXRange(wParam, 1), olStr); 
				pomPropTab->SetStyleRange ( CGXRange(wParam,1), CGXStyle().SetInterior(olNewColor) ); 
				pomProperties->omBorderColor = olNewColor;
				if(pomCtrlObj != NULL)
				{
					pomCtrlObj->Invalidate();
				}
			}
		}
	}
	if(omCurrentControl == "edit")
	{
		if(wParam == 11)
		{
			CColorDialog polDlg(0,0,this);
			if(polDlg.DoModal() == IDOK)
			{
				COLORREF olNewColor;
				olNewColor = polDlg.GetColor();
				CString olStr; olStr.Format("%ld", (long)olNewColor);
				pomPropTab->SetValueRange(CGXRange(wParam, 1), olStr); 
				pomPropTab->SetStyleRange ( CGXRange(wParam,1), CGXStyle().SetInterior(olNewColor) ); 
				pomProperties->omBackColor = olNewColor;
				if(pomCtrlObj != NULL)
				{
					pomCtrlObj->Invalidate();
				}
			}
		}
		if(wParam == 12)
		{
			CColorDialog polDlg(0,0,this);
			if(polDlg.DoModal() == IDOK)
			{
				COLORREF olNewColor;
				olNewColor = polDlg.GetColor();
				CString olStr; olStr.Format("%ld", (long)olNewColor);
				pomPropTab->SetValueRange(CGXRange(wParam, 1), olStr); 
				pomPropTab->SetStyleRange ( CGXRange(wParam,1), CGXStyle().SetInterior(olNewColor) ); 
				pomProperties->omFontColor = olNewColor;
				if(pomCtrlObj != NULL)
				{
					pomCtrlObj->Invalidate();
				}
			}
		}
		if(wParam == 13)
		{
			CColorDialog polDlg(0,0,this);
			if(polDlg.DoModal() == IDOK)
			{
				COLORREF olNewColor;
				olNewColor = polDlg.GetColor();
				CString olStr; olStr.Format("%ld", (long)olNewColor);
				pomPropTab->SetValueRange(CGXRange(wParam, 1), olStr); 
				pomPropTab->SetStyleRange ( CGXRange(wParam,1), CGXStyle().SetInterior(olNewColor) ); 
				pomProperties->omBorderColor = olNewColor;
				if(pomCtrlObj != NULL)
				{
					pomCtrlObj->Invalidate();
				}
			}
		}
		pomPropTab->Invalidate();
	}
	if(omCurrentControl == "checkbox" || omCurrentControl == "radio")
	{
		if(wParam == 9)
		{
			CColorDialog polDlg(0,0,this);
			if(polDlg.DoModal() == IDOK)
			{
				COLORREF olNewColor;
				olNewColor = polDlg.GetColor();
				CString olStr; olStr.Format("%ld", (long)olNewColor);
				pomPropTab->SetValueRange(CGXRange(wParam, 1), olStr); 
				pomPropTab->SetStyleRange ( CGXRange(wParam,1), CGXStyle().SetInterior(olNewColor) ); 
				pomProperties->omBackColor = olNewColor;
				if(pomCtrlObj != NULL)
				{
					pomCtrlObj->Invalidate();
				}
			}
		}
		if(wParam == 10)
		{
			CColorDialog polDlg(0,0,this);
			if(polDlg.DoModal() == IDOK)
			{
				COLORREF olNewColor;
				olNewColor = polDlg.GetColor();
				CString olStr; olStr.Format("%ld", (long)olNewColor);
				pomPropTab->SetValueRange(CGXRange(wParam, 1), olStr); 
				pomPropTab->SetStyleRange ( CGXRange(wParam,1), CGXStyle().SetInterior(olNewColor) ); 
				pomProperties->omFontColor = olNewColor;
				if(pomCtrlObj != NULL)
				{
					pomCtrlObj->Invalidate();
				}
			}
		}
		if(wParam == 11)
		{
			CColorDialog polDlg(0,0,this);
			if(polDlg.DoModal() == IDOK)
			{
				COLORREF olNewColor;
				olNewColor = polDlg.GetColor();
				CString olStr; olStr.Format("%ld", (long)olNewColor);
				pomPropTab->SetValueRange(CGXRange(wParam, 1), olStr); 
				pomPropTab->SetStyleRange ( CGXRange(wParam,1), CGXStyle().SetInterior(olNewColor) ); 
				pomProperties->omBorderColor = olNewColor;
				if(pomCtrlObj != NULL)
				{
					pomCtrlObj->Invalidate();
				}
			}
		}
	}
	if(omCurrentControl == "combobox")
	{
		if(wParam == 9)
		{
			CColorDialog polDlg(0,0,this);
			if(polDlg.DoModal() == IDOK)
			{
				COLORREF olNewColor;
				olNewColor = polDlg.GetColor();
				CString olStr; olStr.Format("%ld", (long)olNewColor);
				pomPropTab->SetValueRange(CGXRange(wParam, 1), olStr); 
				pomPropTab->SetStyleRange ( CGXRange(wParam,1), CGXStyle().SetInterior(olNewColor) ); 
				pomProperties->omBackColor = olNewColor;
				if(pomCtrlObj != NULL)
				{
					pomCtrlObj->Invalidate();
				}
			}
		}
		if(wParam == 10)
		{
			CColorDialog polDlg(0,0,this);
			if(polDlg.DoModal() == IDOK)
			{
				COLORREF olNewColor;
				olNewColor = polDlg.GetColor();
				CString olStr; olStr.Format("%ld", (long)olNewColor);
				pomPropTab->SetValueRange(CGXRange(wParam, 1), olStr); 
				pomPropTab->SetStyleRange ( CGXRange(wParam,1), CGXStyle().SetInterior(olNewColor) ); 
				pomProperties->omFontColor = olNewColor;
				if(pomCtrlObj != NULL)
				{
					pomCtrlObj->Invalidate();
				}
			}
		}
		if(wParam == 11)
		{
			CColorDialog polDlg(0,0,this);
			if(polDlg.DoModal() == IDOK)
			{
				COLORREF olNewColor;
				olNewColor = polDlg.GetColor();
				CString olStr; olStr.Format("%ld", (long)olNewColor);
				pomPropTab->SetValueRange(CGXRange(wParam, 1), olStr); 
				pomPropTab->SetStyleRange ( CGXRange(wParam,1), CGXStyle().SetInterior(olNewColor) ); 
				pomProperties->omBorderColor = olNewColor;
				if(pomCtrlObj != NULL)
				{
					pomCtrlObj->Invalidate();
				}
			}
		}
	}
	if(omCurrentControl == "static")
	{
		if(wParam == 9)
		{
			CColorDialog polDlg(0,0,this);
			if(polDlg.DoModal() == IDOK)
			{
				COLORREF olNewColor;
				olNewColor = polDlg.GetColor();
				CString olStr; olStr.Format("%ld", (long)olNewColor);
				pomPropTab->SetValueRange(CGXRange(wParam, 1), olStr); 
				pomPropTab->SetStyleRange ( CGXRange(wParam,1), CGXStyle().SetInterior(olNewColor) ); 
				pomProperties->omBackColor = olNewColor;
				if(pomCtrlObj != NULL)
				{
					pomCtrlObj->Invalidate();
				}
			}
		}
		if(wParam == 10)
		{
			CColorDialog polDlg(0,0,this);
			if(polDlg.DoModal() == IDOK)
			{
				COLORREF olNewColor;
				olNewColor = polDlg.GetColor();
				CString olStr; olStr.Format("%ld", (long)olNewColor);
				pomPropTab->SetValueRange(CGXRange(wParam, 1), olStr); 
				pomPropTab->SetStyleRange ( CGXRange(wParam,1), CGXStyle().SetInterior(olNewColor) ); 
				pomProperties->omFontColor = olNewColor;
				if(pomCtrlObj != NULL)
				{
					pomCtrlObj->Invalidate();
				}
			}
		}
		if(wParam == 11)
		{
			CColorDialog polDlg(0,0,this);
			if(polDlg.DoModal() == IDOK)
			{
				COLORREF olNewColor;
				olNewColor = polDlg.GetColor();
				CString olStr; olStr.Format("%ld", (long)olNewColor);
				pomPropTab->SetValueRange(CGXRange(wParam, 1), olStr); 
				pomPropTab->SetStyleRange ( CGXRange(wParam,1), CGXStyle().SetInterior(olNewColor) ); 
				pomProperties->omBorderColor = olNewColor;
				if(pomCtrlObj != NULL)
				{
					pomCtrlObj->Invalidate();
				}
			}
		}
	}
	if(omCurrentControl == "line")
	{
	}
	if(omCurrentControl == "rectangle")
	{
		if(wParam == 6)
		{
			CColorDialog polDlg(0,0,this);
			if(polDlg.DoModal() == IDOK)
			{
				COLORREF olNewColor;
				olNewColor = polDlg.GetColor();
				CString olStr; olStr.Format("%ld", (long)olNewColor);
				pomPropTab->SetValueRange(CGXRange(wParam, 1), olStr); 
				pomPropTab->SetStyleRange ( CGXRange(wParam,1), CGXStyle().SetInterior(olNewColor) ); 
				pomProperties->omBackColor = olNewColor;
				if(pomCtrlObj != NULL)
				{
					pomCtrlObj->Invalidate();
				}
			}
		}
		if(wParam == 7)
		{
			CColorDialog polDlg(0,0,this);
			if(polDlg.DoModal() == IDOK)
			{
				COLORREF olNewColor;
				olNewColor = polDlg.GetColor();
				CString olStr; olStr.Format("%ld", (long)olNewColor);
				pomPropTab->SetValueRange(CGXRange(wParam, 1), olStr); 
				pomPropTab->SetStyleRange ( CGXRange(wParam,1), CGXStyle().SetInterior(olNewColor) ); 
				pomProperties->omFontColor = olNewColor;
				if(pomCtrlObj != NULL)
				{
					pomCtrlObj->Invalidate();
				}
			}
		}
		if(wParam == 8)
		{
			CColorDialog polDlg(0,0,this);
			if(polDlg.DoModal() == IDOK)
			{
				COLORREF olNewColor;
				olNewColor = polDlg.GetColor();
				CString olStr; olStr.Format("%ld", (long)olNewColor);
				pomPropTab->SetValueRange(CGXRange(wParam, 1), olStr); 
				pomPropTab->SetStyleRange ( CGXRange(wParam,1), CGXStyle().SetInterior(olNewColor) ); 
				pomProperties->omBorderColor = olNewColor;
				if(pomCtrlObj != NULL)
				{
					pomCtrlObj->Invalidate();
				}
			}
		}
	}
	if( omCurrentControl == "group")
	{
		if(wParam == 9)
		{
			CColorDialog polDlg(0,0,this);
			if(polDlg.DoModal() == IDOK)
			{
				COLORREF olNewColor;
				olNewColor = polDlg.GetColor();
				CString olStr; olStr.Format("%ld", (long)olNewColor);
				pomPropTab->SetValueRange(CGXRange(wParam, 1), olStr); 
				pomPropTab->SetStyleRange ( CGXRange(wParam,1), CGXStyle().SetInterior(olNewColor) ); 
				pomProperties->omBackColor = olNewColor;
				if(pomCtrlObj != NULL)
				{
					pomCtrlObj->Invalidate();
				}
			}
		}
		if(wParam == 10)
		{
			CColorDialog polDlg(0,0,this);
			if(polDlg.DoModal() == IDOK)
			{
				COLORREF olNewColor;
				olNewColor = polDlg.GetColor();
				CString olStr; olStr.Format("%ld", (long)olNewColor);
				pomPropTab->SetValueRange(CGXRange(wParam, 1), olStr); 
				pomPropTab->SetStyleRange ( CGXRange(wParam,1), CGXStyle().SetInterior(olNewColor) ); 
				pomProperties->omFontColor = olNewColor;
				if(pomCtrlObj != NULL)
				{
					pomCtrlObj->Invalidate();
				}
			}
		}
		if(wParam == 11)
		{
			CColorDialog polDlg(0,0,this);
			if(polDlg.DoModal() == IDOK)
			{
				COLORREF olNewColor;
				olNewColor = polDlg.GetColor();
				CString olStr; olStr.Format("%ld", (long)olNewColor);
				pomPropTab->SetValueRange(CGXRange(wParam, 1), olStr); 
				pomPropTab->SetStyleRange ( CGXRange(wParam,1), CGXStyle().SetInterior(olNewColor) ); 
				pomProperties->omBorderColor = olNewColor;
				if(pomCtrlObj != NULL)
				{
					pomCtrlObj->Invalidate();
				}
			}
		}
	}
	if(omCurrentControl == "listbox")
	{
	}
	if(omCurrentControl == "grid")
	{
		if(wParam >= 2 && wParam <= 10)
		{
			DetailGridWizard *polDlg = new DetailGridWizard(this, (UGridCtrlProperties *)pomProperties);
			if(polDlg->DoModal() == IDOK)
			{
				SetObjectProperties(pomProperties, pomCtrlObj, omCurrentTable );
			}
			delete polDlg;
		}
	}
}

// ==================================================================
// 
// FUNCTION :  PropertiesDlg::GridButtonClicked_Event()
// 
// * Description : Creats an entry in the Event Tab and popup
//				   Vbw Code view 
// 
// 
// * Author : [cla AAT/IR], changed : [07.11.00 14:06:21]
// 
// * Returns : [void] -
// 
// * Function parameters : 
// [wParam] - Window parameter
// [lParam] - Left parameter
// 
// ==================================================================
void PropertiesDlg::GridButtonClicked_Event(UINT wParam, LONG lParam)
{
	CString olVbEventName;

	if((omCurrentControl == "button") || (omCurrentControl == "edit"))
	{
//		if(wParam == 1)
//		{
			UFormProperties *polForm = NULL;
			polForm = pomView->GetDocument()->pomForm;
			if(polForm != NULL)
			{
				CString olFileName;
				CString olFunctionName;
				//olFunctionName = pomProperties->omName + CString("_Clicked");
				olFileName = polForm->omName + ".vbw";
				/*
				 * Temporarily commented out
				 */
				if(omCurrentControl == "edit")
				{
					olVbEventName = pomProperties->omName.Right(((pomProperties->omName.GetLength())-
						                                       (pomProperties->omName.Find('.')))-1);
				}
				else
				{
					olVbEventName = pomProperties->omName;
				}
				olVbEventName += _T("_");
				olVbEventName += (pomProperties->omEvents[wParam-1].EventName);
				//AfxMessageBox(LPCTSTR(olVbEventName));
				pomPropTab->SetValueRange(CGXRange(wParam, 1), olVbEventName);
				/*
				 * Add it to List
				 */
				CCntlEvent olEvent(pomProperties->omEvents[wParam-1].EventId,
					               pomProperties->omEvents[wParam-1].EventName,
								   olVbEventName);
				pomProperties->omEventList.push_back(olEvent);
				OpenVBScriptForFunction(olFileName, olVbEventName);
			}
//		}
	}
}


//---------------------------------------------------------------------------------
// Open the script for the Form & Control and jump to the specified function
//---------------------------------------------------------------------------------
void PropertiesDlg::OpenVBScriptForFunction(CString opFileName, CString opFunction)
{
	CVBScriptEditorDoc *polDoc;
	CVBScriptEditorView *polView;
	bool blMustCreateNew = true;
	bool blDocFound = false;
	POSITION pos = ((CUWorkBenchApp*)AfxGetApp())->pVBScriptTpl->GetFirstDocPosition();
	while (pos != NULL)
	{
		polDoc = (CVBScriptEditorDoc*)((CUWorkBenchApp*)AfxGetApp())->pUfisGridMDI->GetNextDoc(pos );
		if(polDoc != NULL)
		{
			if(polDoc->omFileName == opFileName)
			{
				POSITION pos2 = polDoc->GetFirstViewPosition();
				while (pos2 != NULL)
				{
					//polDoc->G
					polView = (CVBScriptEditorView*)polDoc->GetNextView(pos2);
					if(polView != NULL)
					{
						//((CUWorkBenchApp*)AfxGetApp())->pMainFrame->GetActiveFrame()->MDIActivate();
						//CMainFrame* polMainFrm;
						//polMainFrm = (CMainFrame*)AfxGetMainWnd();
						((CUWorkBenchApp*)AfxGetApp())->pMainFrame->ActivateScriptMDI(opFileName);
						//((VBScriptEditorMDI*)polMainFrm->GetActiveFrame())->MDIActivate();
						JumpToFunction(polView, opFunction);
						blMustCreateNew = false;
						blDocFound = true;
					}
				}
			}
		}
	}
	if( blMustCreateNew == true )
	{
		(CVBScriptEditorDoc*)((CMultiDocTemplate*)((CUWorkBenchApp*)AfxGetApp())->pVBScriptTpl->OpenDocumentFile(opFileName.GetBuffer(0)));
		VBScriptEditorMDI* polMDI = (VBScriptEditorMDI*)(CMDIChildWnd*)((CUWorkBenchApp*)AfxGetApp())->pMainFrame->GetActiveFrame( );
		if(polMDI != NULL)
		{
			if(polMDI->IsKindOf( RUNTIME_CLASS(VBScriptEditorMDI)))
			{
				polMDI->SetScriptName(opFileName);
				((CUWorkBenchApp*)AfxGetApp())->pMainFrame->AddScriptMDI(opFileName, polMDI);
			}
		}

		POSITION pos = ((CUWorkBenchApp*)AfxGetApp())->pVBScriptTpl->GetFirstDocPosition();
		while (pos != NULL)
		{
			polDoc = (CVBScriptEditorDoc*)((CUWorkBenchApp*)AfxGetApp())->pUfisGridMDI->GetNextDoc(pos );
			if(polDoc != NULL)
			{
				if(polDoc->omFileName == opFileName)
				{
					POSITION pos2 = polDoc->GetFirstViewPosition();
					while (pos2 != NULL)
					{
						//polDoc->G
						polView = (CVBScriptEditorView*)polDoc->GetNextView(pos2);
						if(polView != NULL)
						{
							JumpToFunction(polView, opFunction);
						}
					}
				}
			}
		}
	}
}

//-------------------------------------------------------------------------------
// Trys to locate the function. If the function can be found ==> jump to the 
//								function
//								else
//								Append the function and jump to it
//-------------------------------------------------------------------------------
void PropertiesDlg::JumpToFunction(CVBScriptEditorView* popView, CString opFunction, CString opParameters)
{
	BOOL blFktFound = false;
	CString olSearchText;
	CString olModuleText;
	CString olAppendText;
	olSearchText = "Public Sub " + opFunction + "(" + opParameters + ")";
	
	blFktFound = popView->FindText(olSearchText.GetBuffer(0));
	if(blFktFound == FALSE)
	{
		popView->GetRichEditCtrl().GetWindowText(olModuleText);
		olAppendText = "\n\nPublic Sub " + opFunction + "(" + opParameters + ")\n\nEnd Sub";
		olModuleText += olAppendText;
		popView->GetRichEditCtrl().SetWindowText(olModuleText);
		popView->FindText(olSearchText.GetBuffer(0));
	}
}

void PropertiesDlg::OnOK()
{
}

void PropertiesDlg::OnSelchangeTab1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	int ilSelectedPage = m_Tab.GetCurSel();
	if(ilSelectedPage == 0)
	{
		imCurrentPage = PROP_PROPERTIES;
		SetProperties(pomProperties, pomCtrlObj, omCurrentTable, pomView);
	}
	if(ilSelectedPage == 1)
	{
		imCurrentPage = PROP_EVENTS;
		SetEvents(pomProperties, pomCtrlObj, omCurrentTable, pomView);
	}

	*pResult = 0;
}

// ==================================================================
// 
// FUNCTION :  PropertiesDlg::OnButton1()
// 
// * Description : Intermediate solution for editing of OnOK
// 
// 
// * Author : [cla AAT/ID], Created : [19.03.01 10:29:11]
// 
// * Returns : [void] - 
// 
// * Function parameters : - 
// 
// ==================================================================
void PropertiesDlg::OnButton1() 
{
	// TODO: Add your control notification handler code here
	CString olFileName;
	CString olFunctionName;
				
	UFormProperties *polForm = NULL;
	polForm = pomView->GetDocument()->pomForm;

	olFileName = polForm->omName + ".vbw";
	olFunctionName = "OnOK";
	OpenVBScriptForFunction(olFileName, olFunctionName);

}
