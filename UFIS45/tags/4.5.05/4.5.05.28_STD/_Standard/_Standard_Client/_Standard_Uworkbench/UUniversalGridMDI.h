#if !defined(AFX_UUNIVERSALGRIDMDI_H__EE6B3C54_47E4_11D3_B093_00001C019205__INCLUDED_)
#define AFX_UUNIVERSALGRIDMDI_H__EE6B3C54_47E4_11D3_B093_00001C019205__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UUniversalGridMDI.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// UUniversalGridMDI frame

class UUniversalGridMDI : public CMDIChildWnd
{
	DECLARE_DYNCREATE(UUniversalGridMDI)
protected:
	UUniversalGridMDI();           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:

	void OnGridTimeFrame();
	void OnGridFilter();
	CToolBar    m_wndToolBar; //IDR_UNIVERSAL_GRID_TOOLBAR
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(UUniversalGridMDI)
	protected:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~UUniversalGridMDI();

	// Generated message map functions
	//{{AFX_MSG(UUniversalGridMDI)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UUNIVERSALGRIDMDI_H__EE6B3C54_47E4_11D3_B093_00001C019205__INCLUDED_)
