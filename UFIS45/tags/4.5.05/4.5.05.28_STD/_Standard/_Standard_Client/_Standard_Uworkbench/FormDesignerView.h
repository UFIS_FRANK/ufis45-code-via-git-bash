#if !defined(AFX_FORMDESIGNERVIEW_H__99507044_6CE0_11D3_A1EA_0000B4984BBE__INCLUDED_)
#define AFX_FORMDESIGNERVIEW_H__99507044_6CE0_11D3_A1EA_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FormDesignerView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// FormDesignerView view
#define HINT_UPDATE_WINDOW      0
#define HINT_UPDATE_DRAWOBJ     1
#define HINT_UPDATE_SELECTION   2
#define HINT_DELETE_SELECTION   3

#include "UFormControlObj.h"
//class UFormControlObj;
class FormDesignerDoc;

class FormDesignerView : public CScrollView
{
protected:
	FormDesignerView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(FormDesignerView)

// Attributes
public:
	FormDesignerDoc* GetDocument()
		{ return (FormDesignerDoc*)m_pDocument; }
	void SetPageSize(CSize size);
	CRect GetInitialPosition();

// Operations
public:

	void DocToClient(CRect& ropRect);
	void DocToClient(CPoint& ropPoint);
	void ClientToDoc(CPoint& ropPoint);
	void ClientToDoc(CRect& ropRect);

	void Select(UFormControlObj* pObj, BOOL bAdd = FALSE);
	void SelectWithinRect(CRect rect, BOOL bAdd = FALSE);
	void Deselect(UFormControlObj* pObj);
	void CloneSelection();
	void UpdateActiveItem();
	void InvalObj(UFormControlObj* popObj);
	void Remove(UFormControlObj* popObj);
	void PasteNative(COleDataObject& dataObject);
	void PasteEmbedded(COleDataObject& dataObject, CPoint point );


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FormDesignerView)
	protected:
//	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
//	virtual void OnInitialUpdate();     // first time after construct
	//}}AFX_VIRTUAL

// Implementation
protected:
	COleDropTarget m_dropTarget;        // for drop target functionality
	CPoint m_dragPoint;                 // current position
	CSize m_dragSize;                   // size of dragged object
	CSize m_dragOffset;                 // offset between pt and drag object corner
	DROPEFFECT m_prevDropEffect;
	BOOL m_bDragDataAcceptable;

	BOOL GetObjectInfo(COleDataObject* pDataObject,
		CSize* pSize, CSize* pOffset);

public:
	virtual ~FormDesignerView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);  // overriden to record time/date
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual void OnActivateView(BOOL bActivate, CView* pActiveView, CView* pDeactiveView);
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo);
	virtual BOOL OnScrollBy(CSize sizeScroll, BOOL bDoScroll);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	void DrawGrid(CDC* pDC);

	// added for drop-target functionality
/*	virtual BOOL OnDrop(COleDataObject* pDataObject,
		DROPEFFECT dropEffect, CPoint point);
	virtual DROPEFFECT OnDragEnter(COleDataObject* pDataObject,
		DWORD grfKeyState, CPoint point);
	virtual DROPEFFECT OnDragOver(COleDataObject* pDataObject,
		DWORD grfKeyState, CPoint point);
	virtual void OnDragLeave();
*/
	static CLIPFORMAT m_cfObjectDescriptor;
	// end of drop-target additions

	static CLIPFORMAT m_cfDraw; // custom clipboard format

	UFormControlObjList omSelection;
	BOOL				bmGrid;
	COLORREF			omGridColor;
	BOOL				bmActive; // is the view active?

protected:
	virtual void OnInitialUpdate(); // called first time after construct

	// Printing support
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

	// OLE Container support
public:
	virtual BOOL IsSelected(const CObject* pDocItem) const;

	// Generated message map functions
	//{{AFX_MSG(FormDesignerView)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnDrawSelect();
	afx_msg void OnDrawRect();
	afx_msg void OnDrawLine();
	afx_msg void OnDrawEditCtrl();
	afx_msg void OnDrawGrid();
	afx_msg void OnUpdateDrawGridCtrl(CCmdUI* pCmdUI);
	afx_msg void OnDrawCheckboxCtrl();
	afx_msg void OnDrawRadiobuttonCtrl();
	afx_msg void OnUpdateDrawCheckboxCtrl(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDrawRadiobuttonCtrl(CCmdUI* pCmdUI);
	afx_msg void OnDrawComboboxCtrl();
	afx_msg void OnDrawButtonCtrl();
	afx_msg void OnUpdateDrawButtonCtrl(CCmdUI* pCmdUI);
	afx_msg void OnDrawGroupCtrl();
	afx_msg void OnUpdateDrawGroupCtrl(CCmdUI* pCmdUI);
	afx_msg void OnDrawStaticCtrl();
	afx_msg void OnUpdateDrawStatic(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDrawComboboxCtrl(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDrawLine(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDrawEdit(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDrawRect(CCmdUI* pCmdUI);
	afx_msg void OnUpdateDrawSelect(CCmdUI* pCmdUI);
	afx_msg void OnEditSelectAll();
	afx_msg void OnEditClear();
	afx_msg void OnKeyLeft();
	afx_msg void OnKeyRight();
	afx_msg void OnKeyArrowUp();
	afx_msg void OnKeyDown();
	afx_msg void OnUpdateAnySelect(CCmdUI* pCmdUI);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnEditCopy();
	afx_msg void OnUpdateEditCopy(CCmdUI* pCmdUI);
	afx_msg void OnEditCut();
	afx_msg void OnUpdateEditCut(CCmdUI* pCmdUI);
	afx_msg void OnEditPaste();
	afx_msg void OnUpdateEditPaste(CCmdUI* pCmdUI);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnDestroy();
	afx_msg void OnUpdateEditSelectAll(CCmdUI* pCmdUI);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnKeyUp( UINT nChar, UINT nRepCnt, UINT nFlags );
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FORMDESIGNERVIEW_H__99507044_6CE0_11D3_A1EA_0000B4984BBE__INCLUDED_)
