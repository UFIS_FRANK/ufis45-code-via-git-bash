// ScriptHost.cpp: implementation of the CScriptHost class.
//
//////////////////////////////////////////////////////////////////////

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *	UWorkBench - Project
 *	
 *		ScriptHost Wrapper class to get a single instance of 
 *		the script host.
 *
 *	ABB Airport Technologies GmbH 2000
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *
 *		Date		Author			Comment
 *		-------------------------------------------------------------
 *
 *		17/10/00	cla AAT/IR		Initial version
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "stdafx.h"
#include "ScriptHost.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

/*
 * Initialization of the static member
 */

CScriptHost* CScriptHost::s_pScriptHost = 0;

/* ----------------------------------------------------------------- */

CScriptHost::CScriptHost()
{
	::CoInitialize(NULL);

	try
	{
		pScriptControl = IScriptControlPtr(CLSID_ScriptControl);
	}
	catch(_com_error & error)
	{
		AfxMessageBox(error.ErrorMessage());
	}
	/*
	 * Set several properties
	 */
	try
	{
		pScriptControl->PutLanguage("VBScript");
	}
	catch(_com_error & error)
	{
		AfxMessageBox(error.ErrorMessage());
	}

	try
	{
		pScriptControl->PutAllowUI(TRUE);
	}
	catch(_com_error & error)
	{
		AfxMessageBox(error.ErrorMessage());
	}
}

CScriptHost::~CScriptHost()
{
	pScriptControl = NULL;
}

// ==================================================================
// 
// FUNCTION :  CScriptHost::CreateInstance()
// 
// * Description : Creation of Singleton
// 
// 
// * Author : [cla AAT/IR], Created : [18.10.00 09:27:13]
// 
// * Returns : [CScriptHost*] -
// 
// * Function parameters : 
// 
// ==================================================================
CScriptHost* CScriptHost::CreateInstance()
{
	if(s_pScriptHost == 0)
	{
		s_pScriptHost = new CScriptHost;
	}
	return s_pScriptHost;

}