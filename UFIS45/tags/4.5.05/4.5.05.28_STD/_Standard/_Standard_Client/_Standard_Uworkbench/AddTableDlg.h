#if !defined(AFX_ADDTABLEDLG_H__F091C682_1FD1_11D3_A1C3_0000B4984BBE__INCLUDED_)
#define AFX_ADDTABLEDLG_H__F091C682_1FD1_11D3_A1C3_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AddTableDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// AddTableDlg dialog

class AddTableDlg : public CDialog
{
// Construction
public:
	AddTableDlg(CWnd* pParent, CStringArray *popList);   // standard constructor

// Dialog Data
	//{{AFX_DATA(AddTableDlg)
	enum { IDD = IDD_ADDTABLE };
	CListBox	m_List;
	//}}AFX_DATA

	CStringArray *pomList;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(AddTableDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(AddTableDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnDblclkTableList();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADDTABLEDLG_H__F091C682_1FD1_11D3_A1C3_0000B4984BBE__INCLUDED_)
