@echo off
REM -- First make map file from Microsoft Visual C++ generated resource.h
echo // MAKEHELP.BAT generated Help Map file.  Used by UWORKBENCH.HPJ. >"hlp\UWorkBench.hm"
echo. >>"hlp\UWorkBench.hm"
echo // Commands (ID_* and IDM_*) >>"hlp\UWorkBench.hm"
makehm ID_,HID_,0x10000 IDM_,HIDM_,0x10000 resource.h >>"hlp\UWorkBench.hm"
echo. >>"hlp\UWorkBench.hm"
echo // Prompts (IDP_*) >>"hlp\UWorkBench.hm"
makehm IDP_,HIDP_,0x30000 resource.h >>"hlp\UWorkBench.hm"
echo. >>"hlp\UWorkBench.hm"
echo // Resources (IDR_*) >>"hlp\UWorkBench.hm"
makehm IDR_,HIDR_,0x20000 resource.h >>"hlp\UWorkBench.hm"
echo. >>"hlp\UWorkBench.hm"
echo // Dialogs (IDD_*) >>"hlp\UWorkBench.hm"
makehm IDD_,HIDD_,0x20000 resource.h >>"hlp\UWorkBench.hm"
echo. >>"hlp\UWorkBench.hm"
echo // Frame Controls (IDW_*) >>"hlp\UWorkBench.hm"
makehm IDW_,HIDW_,0x50000 resource.h >>"hlp\UWorkBench.hm"
REM -- Make help for Project UWORKBENCH


echo Building Win32 Help files
start /wait hcw /C /E /M "hlp\UWorkBench.hpj"
if errorlevel 1 goto :Error
if not exist "hlp\UWorkBench.hlp" goto :Error
if not exist "hlp\UWorkBench.cnt" goto :Error
echo.
if exist Debug\nul copy "hlp\UWorkBench.hlp" Debug
if exist Debug\nul copy "hlp\UWorkBench.cnt" Debug
if exist Release\nul copy "hlp\UWorkBench.hlp" Release
if exist Release\nul copy "hlp\UWorkBench.cnt" Release
echo.
goto :done

:Error
echo hlp\UWorkBench.hpj(1) : error: Problem encountered creating help file

:done
echo.
