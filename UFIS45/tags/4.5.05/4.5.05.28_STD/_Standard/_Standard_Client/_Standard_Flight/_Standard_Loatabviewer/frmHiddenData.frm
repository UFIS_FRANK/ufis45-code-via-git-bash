VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmHiddenData 
   Caption         =   "Hidden Data and Configuration"
   ClientHeight    =   10050
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   14175
   Icon            =   "frmHiddenData.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   10050
   ScaleWidth      =   14175
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox chkEdit 
      Caption         =   "Edit"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   22
      Top             =   30
      Width           =   795
   End
   Begin VB.CheckBox chkSave 
      Caption         =   "Save"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   870
      Style           =   1  'Graphical
      TabIndex        =   21
      Top             =   30
      Width           =   795
   End
   Begin VB.CheckBox chkClose 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1680
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   30
      Width           =   795
   End
   Begin VB.Frame Frame2 
      Caption         =   "Folder Categories"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   2910
      TabIndex        =   17
      Top             =   450
      Width           =   8265
      Begin VB.OptionButton Option2 
         Caption         =   "A"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   0
         Left            =   120
         TabIndex        =   18
         Top             =   270
         Width           =   645
      End
      Begin VB.Label Label2 
         Caption         =   "Passenger"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   840
         TabIndex        =   19
         Top             =   270
         Width           =   1275
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Information Priority Levels"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   60
      TabIndex        =   8
      Top             =   450
      Width           =   2715
      Begin VB.OptionButton Option1 
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   3
         Left            =   180
         TabIndex        =   12
         Top             =   990
         Width           =   645
      End
      Begin VB.OptionButton Option1 
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   2
         Left            =   180
         TabIndex        =   11
         Top             =   750
         Width           =   645
      End
      Begin VB.OptionButton Option1 
         Caption         =   "2"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   1
         Left            =   180
         TabIndex        =   10
         Top             =   510
         Width           =   645
      End
      Begin VB.OptionButton Option1 
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   0
         Left            =   180
         TabIndex        =   9
         Top             =   270
         Width           =   645
      End
      Begin VB.Label Label1 
         Caption         =   "Level 4"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   3
         Left            =   840
         TabIndex        =   16
         Top             =   990
         Width           =   1755
      End
      Begin VB.Label Label1 
         Caption         =   "Level 3"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   840
         TabIndex        =   15
         Top             =   750
         Width           =   1755
      End
      Begin VB.Label Label1 
         Caption         =   "Level 2"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   840
         TabIndex        =   14
         Top             =   510
         Width           =   1755
      End
      Begin VB.Label Label1 
         Caption         =   "Level 1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   840
         TabIndex        =   13
         Top             =   270
         Width           =   1755
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   7
      Top             =   9735
      Width           =   14175
      _ExtentX        =   25003
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   24474
         EndProperty
      EndProperty
   End
   Begin TABLib.TAB tabData 
      Height          =   975
      Index           =   0
      Left            =   60
      TabIndex        =   1
      Tag             =   "AFTTAB;URNO,FLNO,ADID,REGN,ACT3,ACT5,STOA,STOD,ETAI,ETDI,ORG3,DES3"
      Top             =   4980
      Width           =   13515
      _Version        =   65536
      _ExtentX        =   23839
      _ExtentY        =   1720
      _StockProps     =   64
   End
   Begin TABLib.TAB tabContentCfg 
      Height          =   2715
      Left            =   60
      TabIndex        =   0
      Top             =   1890
      Width           =   13695
      _Version        =   65536
      _ExtentX        =   24156
      _ExtentY        =   4789
      _StockProps     =   64
   End
   Begin TABLib.TAB tabData 
      Height          =   3255
      Index           =   1
      Left            =   60
      TabIndex        =   4
      Tag             =   "LOATAB;URNO,FLNU,RURN,DSSN,TYPE,STYP,SSTP,SSST,FLNO,APC3,VALU,RMRK,UNIT,TIME,ULDP,ULDT,ADDI"
      Top             =   6180
      Width           =   13515
      _Version        =   65536
      _ExtentX        =   23839
      _ExtentY        =   5741
      _StockProps     =   64
   End
   Begin VB.Label lblCount 
      Caption         =   "Loaded Records: 0"
      Height          =   315
      Index           =   1
      Left            =   900
      TabIndex        =   6
      Top             =   5940
      Width           =   3375
   End
   Begin VB.Label lblTab 
      Caption         =   "LOATAB:"
      Height          =   255
      Index           =   1
      Left            =   60
      TabIndex        =   5
      Top             =   5940
      Width           =   795
   End
   Begin VB.Label lblCount 
      Caption         =   "Loaded Records: 0"
      Height          =   315
      Index           =   0
      Left            =   900
      TabIndex        =   3
      Top             =   4740
      Width           =   3375
   End
   Begin VB.Label lblTab 
      Caption         =   "AFTTAB:"
      Height          =   255
      Index           =   0
      Left            =   60
      TabIndex        =   2
      Top             =   4740
      Width           =   795
   End
End
Attribute VB_Name = "frmHiddenData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public strCfgTab As String
Public IsConfigMode As Boolean

Private Sub chkClose_Click()
    If chkClose.Value = 1 Then
        Me.Hide
        chkClose.Value = 0
    End If
End Sub

Private Sub chkEdit_Click()
    Dim LineNo As Long
    If chkEdit.Value = 1 Then
        chkEdit.BackColor = LightGreen
        tabContentCfg.EnableInlineEdit True
        tabContentCfg.InplaceEditUpperCase = False
    Else
        chkEdit.BackColor = vbButtonFace
        tabContentCfg.EnableInlineEdit False
        LineNo = tabContentCfg.GetCurrentSelected
        If LineNo >= 0 Then BuildUpdateList LineNo
    End If
End Sub

Private Sub chkSave_Click()
    Dim iniFile As String
    If chkSave.Value = 1 Then
        'iniFile = GetIniEntry("", "LOATABVIEWER", "", "INIFILE", "c:\ufis\system\LoaTabCfg.cfg")
        iniFile = GetIniEntry("", "LOATABVIEWER", "", "INIFILE", UFIS_SYSTEM & "\LoaTabCfg.cfg")
        tabContentCfg.WriteToFile iniFile, False
        chkSave.Value = 0
    End If
End Sub

Private Sub Form_Load()
    Frame1.Visible = False
    Frame2.Visible = False
    tabContentCfg.ResetContent
    tabContentCfg.EnableHeaderSizing True
    tabContentCfg.ShowHorzScroller True
    tabContentCfg.AutoSizeByHeader = True
    tabContentCfg.HeaderString = "CODE,ID,DSSN,TYPE,STYP,SSTP,SSST,FLNO,APC3,ULDP,ULDT,ADDI,Remark"
    tabContentCfg.LogicalFieldList = "CODE,NAME,DSSN,TYPE,STYP,SSTP,SSST,FLNO,APC3,ULDP,ULDT,ADDI,IDNT"
    tabContentCfg.HeaderLengthString = "30,120,50,30,30,30,30,80,50,100,100,100,400"
    tabContentCfg.CreateDecorationObject "CellDiff", "R,T,B,L", "2,2,2,2", Str(vbBlack) & "," & Str(LightGray) & "," & Str(vbBlack) & "," & Str(LightGray)
    tabContentCfg.SetFieldSeparator strSep
    'tabContentCfg.ReadFromFile GetIniEntry("", "LOATABVIEWER", "", "INIFILE", "c:\ufis\system\LoaTabCfg.cfg")
    tabContentCfg.ReadFromFile GetIniEntry("", "LOATABVIEWER", "", "INIFILE", UFIS_SYSTEM & "\LoaTabCfg.cfg")
    CleanEmptyCodes tabContentCfg, ",,", ",#,"
    tabContentCfg.AutoSizeColumns
    tabContentCfg.Top = Frame1.Top
    tabContentCfg.height = lblTab(0).Top - Frame1.Top - 90
    ResizeCfgTab
End Sub

Private Sub CleanEmptyCodes(CurTab As TABLib.TAB, StrLookUp As String, StrReplace As String)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim OldCodes As String
    Dim NewCodes As String
    MaxLine = CurTab.GetLineCount - 1
  
    For CurLine = 0 To MaxLine
       
        OldCodes = CurTab.GetFieldValues(CurLine, "DSSN,STYP,SSTP,SSST,TYPE")
        OldCodes = Replace(OldCodes, " ", "", 1, -1, vbBinaryCompare)
        NewCodes = OldCodes
        OldCodes = "START"
      
        While OldCodes <> NewCodes
            OldCodes = NewCodes
            NewCodes = Replace(OldCodes, StrLookUp, StrReplace, 1, 1, vbBinaryCompare)
        Wend
        CurTab.SetFieldValues CurLine, "DSSN,STYP,SSTP,SSST,TYPE", NewCodes
      
    Next
  
End Sub
Private Sub MergeTypeMeaning()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim LineCnt As Long
    Dim LineNo As Long
    Dim UseLineNo As Long
    Dim CfgValues As String
    Dim CfgMean As String
    Dim CfgCode As String
    Dim CurApc3 As String
    Dim CurFlno As String
    Dim ItemNbr As Long
    Dim FldName As String
    Dim FldValu As String
    Dim CfgDynaList As String
    Dim CurDynaList As String
    Dim CfgDynaComp As String
    Dim CurDynaComp As String
    Dim ColNo As Long
    CfgDynaList = "SSTP,FLNO,APC3,ULDP,ULDT"
    CurDynaList = "SSTP,FLNO,APC3,ULDP,ULDT"
    tabData(idxLOATAB).Sort "3,4,5,7", True, True
    tabContentCfg.Sort "2,3,4,6", True, True
    tabContentCfg.SetInternalLineBuffer True
    MaxLine = tabData(idxLOATAB).GetLineCount - 1
    
    For CurLine = 0 To MaxLine
        CfgMean = ""
        CfgValues = tabData(idxLOATAB).GetFieldValues(CurLine, "DSSN,TYPE,STYP,SSST")
        LineCnt = Val(tabContentCfg.GetLinesByMultipleColumnValue("2,3,4,6", CfgValues, 0))
        LineNo = LineCnt
        
        ' MsgBox tabContentCfg.GetLinesByMultipleColumnValue("2,3,4,6", "KRI,PXJ,E,#", 0)
        UseLineNo = -1
        While ((LineNo >= 0) And (UseLineNo < 0))
            LineNo = tabContentCfg.GetNextResultLine
            If LineNo >= 0 Then
                If LineCnt > 1 Then
                    CfgDynaComp = tabContentCfg.GetFieldValues(LineNo, CfgDynaList)
                    CurDynaComp = tabData(idxLOATAB).GetFieldValues(CurLine, CurDynaList)
                    If CompareDynamicList(CfgDynaComp, CurDynaComp) = True Then
                        UseLineNo = LineNo
                    Else
                        UseLineNo = -1
                    End If
                    'If CurDynaComp = CfgDynaComp Then UseLineNo = LineNo
                ElseIf LineCnt > 0 Then
                    UseLineNo = LineNo
                End If
            End If
        Wend
        If UseLineNo >= 0 Then
            CfgMean = tabContentCfg.GetFieldValue(UseLineNo, "IDNT")
            CfgCode = tabContentCfg.GetFieldValue(UseLineNo, "CODE")
            ItemNbr = 0
            FldName = GetRealItem(CfgDynaList, ItemNbr, ",")
            While FldName <> ""
                FldValu = tabData(idxLOATAB).GetFieldValue(CurLine, FldName)
                FldName = "<" & FldName & ">"
                CfgMean = Replace(CfgMean, FldName, FldValu, 1, 1, vbBinaryCompare)
                ItemNbr = ItemNbr + 1
                FldName = GetRealItem(CfgDynaList, ItemNbr, ",")
            Wend
        Else
            CurApc3 = tabData(idxLOATAB).GetFieldValue(CurLine, "APC3")
            CurFlno = tabData(idxLOATAB).GetFieldValue(CurLine, "FLNO")
            If CurApc3 <> "" Then CurApc3 = "[" & CurApc3 & "]"
            If CurFlno <> "" Then CurFlno = "[" & CurFlno & "]"
            CfgMean = "Missing Config for [" & CfgValues & "]"
            CfgMean = CfgMean & CurApc3 & " "
            CfgMean = CfgMean & CurFlno
            CfgCode = "??"
        End If
        CfgMean = Replace(CfgMean, ",", ";", 1, -1, vbBinaryCompare)
        CfgCode = Right(CfgCode, 1) & Left(CfgCode, Len(CfgCode) - 1)
        tabData(idxLOATAB).SetFieldValues CurLine, "IDNT,RFLD", CfgMean & "," & CfgCode
       
    Next
  
    tabContentCfg.SetInternalLineBuffer False
    tabData(idxLOATAB).AutoSizeColumns
    ColNo = CLng(GetRealItemNo(UsedLoaTabFields, "RFLD"))
    tabData(idxLOATAB).IndexCreate "FolderData", ColNo
End Sub
Private Function CompareDynamicList(CfgValueList As String, TabValueList As String) As Boolean
    Dim IsOk As Boolean
    Dim CfgResult As String
    Dim TabResult As String
    Dim CfgValue As String
    Dim TabValue As String
    Dim CurItem As Long
    Dim MaxItem As Long
    CfgResult = ""
    TabResult = ""
    MaxItem = ItemCount(CfgValueList, ",")
    For CurItem = 0 To MaxItem
        CfgValue = GetRealItem(CfgValueList, CurItem, ",")
        TabValue = GetRealItem(TabValueList, CurItem, ",")
        If (Left(CfgValue, 1) = "<") And (TabValue <> "") Then CfgValue = TabValue
        CfgResult = CfgResult & CfgValue & ","
        TabResult = TabResult & TabValue & ","
    Next
    If CfgResult = TabResult Then IsOk = True Else IsOk = False
    CompareDynamicList = IsOk
End Function
Public Sub LoadAll()
    Dim myTab As TABLib.TAB
    Dim ilTabCnt As Integer
    Dim arr() As String
    Dim arrFields() As String
    Dim tmpList As String
    Dim ilCount As Integer
    Dim strWhere As String
    Dim i As Integer
    Dim RetVal As Boolean
    
    ilTabCnt = 0
    For Each myTab In tabData
        arr = Split(myTab.Tag, ";")
        arrFields = Split(arr(1), ",")
        strWhere = ""
        myTab.ResetContent
        myTab.RedrawTab
        DoEvents
        myTab.AutoSizeByHeader = True
        myTab.HeaderString = ""
        myTab.HeaderLengthString = ""
        myTab.HeaderString = arr(1)
        myTab.EnableInlineEdit True
        For i = 0 To UBound(arrFields)
            If i = 0 Then
                myTab.HeaderLengthString = "80,"
            Else
                myTab.HeaderLengthString = myTab.HeaderLengthString + "80,"
            End If
        Next i
        myTab.CedaServerName = strServer
        myTab.CedaPort = "3357"
        myTab.CedaHopo = strHOPO
        myTab.CedaCurrentApplication = "LoaTabViewer" & "," & UfisTools.GetApplVersion(True)
        myTab.CedaTabext = "TAB"
        myTab.CedaUser = GetWindowsUserName()
        myTab.CedaWorkstation = GetWorkStationName()
        myTab.CedaSendTimeout = "3"
        myTab.CedaReceiveTimeout = "240"
        myTab.CedaRecordSeparator = Chr(10)
        myTab.CedaIdentifier = "LoaTab"
        myTab.ShowHorzScroller True
        myTab.EnableHeaderSizing True
        'frmMain.MainStatusBar.Panels(1).Text = "Loading: " + arr(0) + " ..."
        If arr(0) = "AFTTAB" Then
            strWhere = "WHERE URNO=" + strAFT_URNO
            strAFTFIELDS = arr(1)
        End If
        If arr(0) = "LOATAB" Then
            tmpList = frmMain.BuildLengthListFromFields(arr(1))
            myTab.ColumnWidthString = tmpList
            strWhere = "WHERE FLNU=" + strAFT_URNO
            '====================
            'strWhere = " "
            '====================
        End If
        If strServer = "LOCAL" Then strWhere = ""
        If strWhere <> "" Then
        If CedaIsConnected Then   '-------#####This If condition Added by Sai for performance when ceda is not connected
            myTab.LogicalFieldList = arr(1)
            RetVal = myTab.CedaAction("RT", arr(0), arr(1), "", strWhere)
        Else
            RetVal = False
        End If '-----------######If condition ends here--by Sai for Performance when ceda not connected-------------
            If Not RetVal Then
                'MsgBox myTab.GetLastCedaError
            End If
        End If
        myTab.RedrawTab
        lblCount(ilTabCnt) = "Table: " + arr(0) + "  Records: " + CStr(myTab.GetLineCount)
        
        ilTabCnt = ilTabCnt + 1
        myTab.AutoSizeColumns
    Next
    CleanEmptyCodes tabData(idxLOATAB), ",,", ",#,"
    MergeTypeMeaning
    
    tabData(idxLOATAB).Refresh
    tabContentCfg.Refresh
End Sub

Private Sub Form_Resize()
    Dim NewSize As Long
    NewSize = Me.ScaleWidth - (tabContentCfg.Left * 2)
    If NewSize > 300 Then
        tabContentCfg.Width = NewSize
        tabData(0).Width = NewSize
        tabData(1).Width = NewSize
        If IsConfigMode Then
            NewSize = NewSize - Frame2.Left
            Frame2.Width = NewSize
        End If
    End If
    If IsConfigMode Then
        NewSize = Me.ScaleHeight - tabContentCfg.Top - StatusBar1.height - 30
        If NewSize > 300 Then
            tabContentCfg.height = NewSize
        End If
    End If
End Sub

Private Sub Option1_Click(Index As Integer)
    Frame1.Tag = Option1(Index).Caption
End Sub

Private Sub Option2_Click(Index As Integer)
    Frame2.Tag = Option2(Index).Caption
End Sub

Private Sub tabContentCfg_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    If chkEdit.Value = 1 Then
        If (LineNo >= 0) And (Not Selected) Then BuildUpdateList LineNo
    End If
End Sub

Private Sub tabContentCfg_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim strCode As String
    If LineNo < 0 Then
        tabContentCfg.Sort CStr(ColNo), True, True
        tabContentCfg.Refresh
    Else
        If chkEdit.Value = 0 Then
            strCode = Frame2.Tag & Frame1.Tag
            If Len(strCode) >= 2 Then
                tabContentCfg.SetColumnValue LineNo, 0, strCode
                BuildUpdateList LineNo
            End If
        End If
    End If
End Sub

Private Sub tabContentCfg_SendRButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim tmpMsgText As String
    If Not IsConfigMode Then
        ResizeCfgTab
    Else
'        If LineNo >= 0 Then
'            tmpMsgText = BuildUpdateList(LineNo)
'            If tmpMsgText = "" Then tmpMsgText = "No Updates."
'            MsgBox tmpMsgText
'        End If
    End If
End Sub
Private Function BuildUpdateList(LineNo As Long) As String
    Dim Result As String
    Dim FldLst As String
    Dim FldNam As String
    Dim FldIdx As Long
    Dim LineTag As String
    Dim LineData As String
    Dim FldData As String
    Dim TagData As String
    Result = ""
    tabContentCfg.ResetLineDecorations LineNo
    tabContentCfg.SetLineColor LineNo, vbBlack, vbWhite
    LineData = tabContentCfg.GetLineValues(LineNo)
    LineTag = tabContentCfg.GetLineTag(LineNo)
    FldLst = tabContentCfg.LogicalFieldList
    FldIdx = 0
    FldNam = GetRealItem(FldLst, FldIdx, ",")
    While FldNam <> ""
        FldData = GetRealItem(LineData, FldIdx, ",")
        TagData = GetRealItem(LineTag, FldIdx, ",")
        If FldData <> TagData Then
            Result = Result & FldNam & ": " & FldData & " (" & TagData & ")" & vbNewLine
            tabContentCfg.SetDecorationObject LineNo, FldIdx, "CellDiff"
            tabContentCfg.SetLineColor LineNo, vbBlack, vbYellow
        End If
        FldIdx = FldIdx + 1
        FldNam = GetRealItem(FldLst, FldIdx, ",")
    Wend
    BuildUpdateList = Result
End Function

Private Sub ResizeCfgTab()
    Dim OldPosSize As String
    Dim NewPosSize As String
    OldPosSize = tabContentCfg.myTag
    If OldPosSize = "" Then
        NewPosSize = ""
        NewPosSize = NewPosSize & CStr(tabContentCfg.Left) & ","
        NewPosSize = NewPosSize & CStr(tabContentCfg.Top) & ","
        NewPosSize = NewPosSize & CStr(tabContentCfg.Width) & ","
        NewPosSize = NewPosSize & CStr(tabContentCfg.height)
        tabContentCfg.myTag = NewPosSize
        If IsConfigMode Then
            tabContentCfg.Top = Frame1.Top + Frame1.height + 90
        Else
            tabContentCfg.Top = Frame1.Top
        End If
        tabContentCfg.Width = Me.ScaleWidth - (tabContentCfg.Left * 2)
        tabContentCfg.height = Me.ScaleHeight - tabContentCfg.Top - StatusBar1.height - 30
    Else
        tabContentCfg.Left = Val(GetRealItem(OldPosSize, 0, ","))
        tabContentCfg.Top = Val(GetRealItem(OldPosSize, 1, ","))
        tabContentCfg.height = Val(GetRealItem(OldPosSize, 3, ","))
        tabContentCfg.myTag = ""
    End If
    tabContentCfg.ZOrder
End Sub

Private Sub tabData_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo < 0 Then
        tabData(Index).Sort CStr(ColNo), True, True
        tabData(Index).Refresh
    End If
End Sub

Private Sub tabData_SendRButtonClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim OldPosSize As String
    Dim NewPosSize As String
    OldPosSize = tabData(Index).myTag
    If OldPosSize = "" Then
        NewPosSize = ""
        NewPosSize = NewPosSize & CStr(tabData(Index).Left) & ","
        NewPosSize = NewPosSize & CStr(tabData(Index).Top) & ","
        NewPosSize = NewPosSize & CStr(tabData(Index).Width) & ","
        NewPosSize = NewPosSize & CStr(tabData(Index).height)
        tabData(Index).myTag = NewPosSize
        tabData(Index).Left = tabContentCfg.Left
        tabData(Index).Top = Frame1.Top
        tabData(Index).Width = Me.ScaleWidth - tabData(Index).Left * 2
        tabData(Index).height = Me.ScaleHeight - tabData(Index).Top - 150
    Else
        tabData(Index).Left = Val(GetRealItem(OldPosSize, 0, ","))
        tabData(Index).Top = Val(GetRealItem(OldPosSize, 1, ","))
        'tabData(Index).Width = Val(GetRealItem(OldPosSize, 2, ","))
        tabData(Index).height = Val(GetRealItem(OldPosSize, 3, ","))
        tabData(Index).myTag = ""
    End If
    tabData(Index).ZOrder
End Sub
Public Sub ArrangeConfigMode(SetMode As Boolean)
    IsConfigMode = SetMode
    If IsConfigMode Then
        InitFolderOptions
        StoreLineValues
        Frame1.Visible = True
        Frame2.Visible = True
        tabContentCfg.myTag = ""
        ResizeCfgTab
        Form_Resize
        chkEdit.Enabled = True
        chkSave.Enabled = True
    Else
        Frame1.Visible = False
        Frame2.Visible = False
        chkEdit.Enabled = False
        chkSave.Enabled = False
        ResizeCfgTab
    End If
End Sub
Private Sub StoreLineValues()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim LineData As String
    MaxLine = tabContentCfg.GetLineCount - 1
    For CurLine = 0 To MaxLine
        LineData = tabContentCfg.GetLineValues(CurLine)
        tabContentCfg.SetLineTag CurLine, LineData
    Next
End Sub
Private Sub InitFolderOptions()
    Dim i As Integer
    Dim j As Integer
    Dim NewLeft As Long
    Dim NewTop As Long
    Dim cnt As Integer
    Dim CurCapt As String
    cnt = 0
    i = -1
    NewTop = Option1(cnt).Top
    NewLeft = Option2(0).Left
    For j = 0 To frmMain.fraFolder.UBound
        CurCapt = Left(frmMain.fraBlend(j).Tag, 1)
        If (CurCapt <> "?") And (CurCapt <> "*") Then
            i = i + 1
            If i > Option2.UBound Then
                Load Option2(i)
                Load Label2(i)
            End If
            Option2(i).Left = NewLeft
            Option2(i).Top = NewTop
            Option2(i).Caption = frmMain.fraBlend(j).Tag
            Label2(i).Left = NewLeft + (Label2(0).Left - Option2(0).Left)
            Label2(i).Top = NewTop
            Label2(i).Caption = frmMain.chkFolder(j).Caption
            Set Option2(i).Container = Frame2
            Set Label2(i).Container = Frame2
            Option2(i).Visible = True
            Label2(i).Visible = True
            Label2(i).ZOrder
            cnt = cnt + 1
            If cnt > 3 Then
                NewLeft = Label2(i).Left + Label2(i).Width + 60
                cnt = 0
            End If
            NewTop = Option1(cnt).Top
        End If
    Next
End Sub
