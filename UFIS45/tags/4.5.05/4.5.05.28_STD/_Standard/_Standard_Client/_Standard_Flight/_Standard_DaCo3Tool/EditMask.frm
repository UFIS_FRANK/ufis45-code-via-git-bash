VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.ocx"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form EditMask 
   Caption         =   "Editor"
   ClientHeight    =   5400
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7755
   Icon            =   "EditMask.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5400
   ScaleWidth      =   7755
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1 
      Left            =   1830
      Top             =   4110
   End
   Begin VB.PictureBox WorkArea 
      AutoRedraw      =   -1  'True
      Height          =   3885
      Left            =   0
      ScaleHeight     =   3825
      ScaleWidth      =   6015
      TabIndex        =   5
      Top             =   0
      Width           =   6075
      Begin VB.PictureBox BottomPanel 
         Height          =   525
         Index           =   0
         Left            =   60
         ScaleHeight     =   465
         ScaleWidth      =   4875
         TabIndex        =   15
         Top             =   3210
         Width           =   4935
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "on:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   2610
            TabIndex        =   24
            Top             =   240
            Width           =   285
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "on:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   2610
            TabIndex        =   23
            Top             =   30
            Width           =   285
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Entered by:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   60
            TabIndex        =   21
            Top             =   30
            Width           =   990
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "Changed by:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   60
            TabIndex        =   20
            Top             =   240
            Width           =   1080
         End
         Begin VB.Label lblDisplay 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   2460
            TabIndex        =   19
            Top             =   30
            Width           =   75
         End
         Begin VB.Label lblDisplay 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   2460
            TabIndex        =   18
            Top             =   240
            Width           =   75
         End
         Begin VB.Label lblDisplay 
            AutoSize        =   -1  'True
            Caption         =   "."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   2940
            TabIndex        =   17
            Top             =   30
            Width           =   75
         End
         Begin VB.Label lblDisplay 
            AutoSize        =   -1  'True
            Caption         =   "."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   2940
            TabIndex        =   16
            Top             =   240
            Width           =   75
         End
      End
      Begin VB.Frame fraEditFrame 
         Caption         =   "Field Values"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1065
         Index           =   1
         Left            =   2910
         TabIndex        =   12
         Top             =   60
         Width           =   3375
         Begin VB.CheckBox chkCheck 
            Alignment       =   1  'Right Justify
            Caption         =   "Is Read"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Index           =   0
            Left            =   120
            TabIndex        =   25
            Top             =   600
            Width           =   1185
         End
         Begin VB.TextBox txtInput 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   630
            MaxLength       =   5
            TabIndex        =   13
            Top             =   270
            Width           =   675
         End
         Begin VB.Label Label1 
            Caption         =   "Time"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   150
            TabIndex        =   14
            Top             =   300
            Width           =   435
         End
      End
      Begin VB.TextBox txtContext 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1935
         Left            =   60
         MaxLength       =   3000
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   11
         Top             =   1200
         Width           =   4575
      End
      Begin VB.Frame fraEditFrame 
         Caption         =   "Flight Relation"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1065
         Index           =   0
         Left            =   60
         TabIndex        =   6
         Top             =   60
         Width           =   2775
         Begin VB.CheckBox chkAftCheck 
            Caption         =   "Departure"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   150
            TabIndex        =   8
            Top             =   630
            Width           =   1215
         End
         Begin VB.CheckBox chkAftCheck 
            Caption         =   "Arrival"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   150
            TabIndex        =   7
            Top             =   300
            Width           =   945
         End
         Begin VB.Label lblFlno 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   1410
            TabIndex        =   10
            Top             =   270
            Width           =   1200
         End
         Begin VB.Label lblFlno 
            Alignment       =   2  'Center
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   1410
            TabIndex        =   9
            Top             =   600
            Width           =   1200
         End
      End
   End
   Begin VB.PictureBox RightPanel 
      Align           =   4  'Align Right
      AutoRedraw      =   -1  'True
      Height          =   5115
      Left            =   6360
      ScaleHeight     =   5055
      ScaleWidth      =   1335
      TabIndex        =   1
      Top             =   0
      Width           =   1395
      Begin VB.CheckBox chkTool 
         Caption         =   "User Info"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   150
         Style           =   1  'Graphical
         TabIndex        =   22
         Tag             =   "ABOUT"
         Top             =   435
         Width           =   1035
      End
      Begin VB.CommandButton btnClose 
         Cancel          =   -1  'True
         Caption         =   "Cancel"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   150
         TabIndex        =   3
         Top             =   765
         Width           =   1035
      End
      Begin VB.CommandButton btnOk 
         Caption         =   "OK"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   150
         TabIndex        =   2
         Top             =   105
         Width           =   1035
      End
      Begin TABLib.TAB TAB1 
         Height          =   1935
         Left            =   30
         TabIndex        =   4
         Top             =   2760
         Width           =   1275
         _Version        =   65536
         _ExtentX        =   2249
         _ExtentY        =   3413
         _StockProps     =   64
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   5115
      Width           =   7755
      _ExtentX        =   13679
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13176
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "EditMask"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim GiveAnswer As Boolean

Public Function GetEditorValues(UseForm As Form, MyCaption As String, UseLayout As String, FlightRelation As String, MappFields As String, UseData As String, DispFields As String, DispData As String) As String
    Dim Result As String
    
    ArrangeLayout UseLayout, FlightRelation, MappFields & "," & DispFields, UseData & "," & DispData
    Me.Caption = MyCaption
    EditMaskIsOpen = True
    Me.Left = UseForm.Left + ((UseForm.Width - Me.Width) / 2)
    Me.Top = UseForm.Top + ((UseForm.Height - Me.Height) / 2)
    CheckMonitorArea Me
    Timer1.Interval = 20
    Timer1.Enabled = True
    Me.Show vbModal, UseForm
    If GiveAnswer Then
        Result = CollectResultData(UseLayout, MappFields)
    Else
        Result = ""
    End If
    EditMaskIsOpen = False
    GetEditorValues = Result
End Function

Public Sub UpdateFieldValues(BcFld As String, BcDat As String)
    Dim tmpFldNam As String
    Dim tmpFldVal As String
    Dim ItemNo As Long
    ItemNo = 0
    tmpFldNam = GetRealItem(BcFld, ItemNo, ",")
    While tmpFldNam <> ""
        tmpFldVal = GetRealItem(BcDat, ItemNo, ",")
        Select Case tmpFldNam
            Case "TIME"
                txtInput(0).Text = tmpFldVal
                txtInput(0).Tag = tmpFldVal
            Case "TEXT"
                txtContext.Text = CleanString(tmpFldVal, FOR_CLIENT, False)
                txtContext.Tag = txtContext.Text
                txtContext.Locked = False
            Case "READ"
                If tmpFldVal = "X" Then chkCheck(0).Value = 0 Else chkCheck(0).Value = 1
                chkCheck(0).Visible = True
            Case "USEC"
                lblDisplay(0).Caption = tmpFldVal
            Case "USEU"
                lblDisplay(1).Caption = tmpFldVal
            Case "CDAT"
                lblDisplay(2).Caption = MyDateFormat(tmpFldVal, True)
            Case "LSTU"
                lblDisplay(3).Caption = MyDateFormat(tmpFldVal, True)
        End Select
        ItemNo = ItemNo + 1
        tmpFldNam = GetRealItem(BcFld, ItemNo, ",")
    Wend
    If txtContext.Text = "" Then chkCheck(0).Value = 0
End Sub
Private Sub ArrangeLayout(UseLayout As String, FlightRelation As String, MappFields As String, UseData As String)
    Dim tmpData As String
    Dim i As Integer
    TAB1.Visible = False
    txtContext.Locked = True
    chkCheck(0).Visible = False
    chkAftCheck(0).Value = 0
    chkAftCheck(1).Value = 0
    chkAftCheck(0).Tag = GetRealItem(FlightRelation, 2, ",")
    chkAftCheck(1).Tag = GetRealItem(FlightRelation, 3, ",")
    Me.Tag = GetRealItem(FlightRelation, 4, ",")
    
    For i = 0 To lblDisplay.UBound
        lblDisplay(i).Caption = ""
    Next
    tmpData = GetRealItem(FlightRelation, 0, ",")
    If tmpData = "SET" Then
        fraEditFrame(0).Enabled = True
    Else
        fraEditFrame(0).Enabled = False
    End If
    tmpData = GetRealItem(FlightRelation, 1, ",")
    Select Case tmpData
        Case "ARR"
            chkAftCheck(0).Value = 1
        Case "DEP"
            chkAftCheck(1).Value = 1
        Case Else
            chkAftCheck(1).Value = 1
    End Select
    Select Case UseLayout
        Case "DLLG"
            ArrangeDllgLayout MappFields, UseData
        Case "DLYS"
            ArrangeDlysLayout MappFields, UseData
        Case "MVLG"
            ArrangeMvlgLayout MappFields, UseData
        Case "REQU"
            ArrangeRequLayout MappFields, UseData
        Case Else
    End Select
End Sub
Private Sub ArrangeDllgLayout(MappFields As String, UseData As String)
    Dim tmpFldNam As String
    Dim tmpFldVal As String
    Dim tmpTab1Val As String
    Dim tmpTab2Val As String
    Dim tmpTabLine As String
    Dim ItemNo As Long
    tmpTab1Val = ""
    tmpTab2Val = ""
    ItemNo = 0
    tmpFldNam = GetRealItem(MappFields, ItemNo, ",")
    While tmpFldNam <> ""
        tmpFldVal = GetRealItem(UseData, ItemNo, ",")
        Select Case tmpFldNam
            Case "TXT1"
                txtInput(0).Text = tmpFldVal
            Case "TEXT"
                txtContext.Text = CleanString(tmpFldVal, FOR_CLIENT, False)
                txtContext.Locked = False
            Case "TAB1"
                tmpTab1Val = tmpFldVal
            Case "TAB2"
                tmpTab2Val = tmpFldVal
            Case "USEC"
                lblDisplay(0).Caption = tmpFldVal
            Case "USEU"
                lblDisplay(1).Caption = tmpFldVal
            Case "CDAT"
                lblDisplay(2).Caption = MyDateFormat(tmpFldVal, True)
            Case "LSTU"
                lblDisplay(3).Caption = MyDateFormat(tmpFldVal, True)
        End Select
        ItemNo = ItemNo + 1
        tmpFldNam = GetRealItem(MappFields, ItemNo, ",")
    Wend
    tmpTabLine = tmpTab1Val & "," & tmpTab2Val
    InitTabSelection "TAB1,TAB2", tmpTabLine
End Sub
Private Sub ArrangeDlysLayout(MappFields As String, UseData As String)
    Dim tmpFldNam As String
    Dim tmpFldVal As String
    Dim tmpTab1Val As String
    Dim tmpTab2Val As String
    Dim tmpTabLine As String
    Dim ItemNo As Long
    tmpTab1Val = ""
    tmpTab2Val = ""
    ItemNo = 0
    tmpFldNam = GetRealItem(MappFields, ItemNo, ",")
    While tmpFldNam <> ""
        tmpFldVal = GetRealItem(UseData, ItemNo, ",")
        Select Case tmpFldNam
            Case "TXT1"
                txtInput(0).Text = tmpFldVal
            Case "TEXT"
                txtContext.Text = CleanString(tmpFldVal, FOR_CLIENT, False)
                txtContext.Locked = False
            Case "TAB1"
                tmpTab1Val = tmpFldVal
            Case "TAB2"
                tmpTab2Val = tmpFldVal
            Case "USEC"
                lblDisplay(0).Caption = tmpFldVal
            Case "USEU"
                lblDisplay(1).Caption = tmpFldVal
            Case "CDAT"
                lblDisplay(2).Caption = MyDateFormat(tmpFldVal, True)
            Case "LSTU"
                lblDisplay(3).Caption = MyDateFormat(tmpFldVal, True)
        End Select
        ItemNo = ItemNo + 1
        tmpFldNam = GetRealItem(MappFields, ItemNo, ",")
    Wend
    tmpTabLine = tmpTab1Val & "," & tmpTab2Val
    InitTabSelection "TAB1,TAB2", tmpTabLine
End Sub
Private Sub ArrangeMvlgLayout(MappFields As String, UseData As String)
    Dim tmpFldNam As String
    Dim tmpFldVal As String
    Dim ItemNo As Long
    ItemNo = 0
    tmpFldNam = GetRealItem(MappFields, ItemNo, ",")
    While tmpFldNam <> ""
        tmpFldVal = GetRealItem(UseData, ItemNo, ",")
        Select Case tmpFldNam
            Case "TXT1"
                txtInput(0).Text = tmpFldVal
            Case "TEXT"
                txtContext.Text = CleanString(tmpFldVal, FOR_CLIENT, False)
                txtContext.Locked = False
            Case "USEC"
                lblDisplay(0).Caption = tmpFldVal
            Case "USEU"
                lblDisplay(1).Caption = tmpFldVal
            Case "CDAT"
                lblDisplay(2).Caption = MyDateFormat(tmpFldVal, True)
            Case "LSTU"
                lblDisplay(3).Caption = MyDateFormat(tmpFldVal, True)
        End Select
        ItemNo = ItemNo + 1
        tmpFldNam = GetRealItem(MappFields, ItemNo, ",")
    Wend
End Sub
Private Sub ArrangeRequLayout(MappFields As String, UseData As String)
    Dim tmpFldNam As String
    Dim tmpFldVal As String
    Dim ItemNo As Long
    ItemNo = 0
    tmpFldNam = GetRealItem(MappFields, ItemNo, ",")
    While tmpFldNam <> ""
        tmpFldVal = GetRealItem(UseData, ItemNo, ",")
        Select Case tmpFldNam
            Case "TXT1"
                txtInput(0).Text = tmpFldVal
                txtInput(0).Tag = tmpFldVal
            Case "TEXT"
                txtContext.Text = CleanString(tmpFldVal, FOR_CLIENT, False)
                txtContext.Tag = txtContext.Text
                txtContext.Locked = False
            Case "CHK1"
                If tmpFldVal = "X" Then chkCheck(0).Value = 0 Else chkCheck(0).Value = 1
                chkCheck(0).Visible = True
            Case "USEC"
                lblDisplay(0).Caption = tmpFldVal
            Case "USEU"
                lblDisplay(1).Caption = tmpFldVal
            Case "CDAT"
                lblDisplay(2).Caption = MyDateFormat(tmpFldVal, True)
            Case "LSTU"
                lblDisplay(3).Caption = MyDateFormat(tmpFldVal, True)
        End Select
        ItemNo = ItemNo + 1
        tmpFldNam = GetRealItem(MappFields, ItemNo, ",")
    Wend
    If txtContext.Text = "" Then chkCheck(0).Value = 0
End Sub
Private Function CollectResultData(UseLayout As String, MappFields As String) As String
    Dim Result As String
    Select Case UseLayout
        Case "DLLG"
            Result = CollectDllgData(MappFields)
        Case "DLYS"
            Result = CollectDlysData(MappFields)
        Case "MVLG"
            Result = CollectMvlgData(MappFields)
        Case "REQU"
            Result = CollectRequData(MappFields)
        Case Else
    End Select
    CollectResultData = Result
End Function
Private Function CollectDllgData(MappFields As String) As String
    Dim Result As String
    Dim tmpFldNam As String
    Dim tmpFldVal As String
    Dim LineNo As Long
    Dim ItemNo As Long
    Result = ""
    ItemNo = 0
    tmpFldNam = GetRealItem(MappFields, ItemNo, ",")
    While tmpFldNam <> ""
        Select Case tmpFldNam
            Case "TXT1"
                tmpFldVal = txtInput(0).Text
                Result = Result & tmpFldVal & ","
            Case "TAB1"
                LineNo = TAB1.GetCurrentSelected
                If LineNo < 0 Then LineNo = 0
                tmpFldVal = TAB1.GetFieldValue(LineNo, tmpFldNam)
                Result = Result & tmpFldVal & ","
            Case "TAB2"
                LineNo = TAB1.GetCurrentSelected
                If LineNo < 0 Then LineNo = 0
                tmpFldVal = TAB1.GetFieldValue(LineNo, tmpFldNam)
                Result = Result & tmpFldVal & ","
            Case "TEXT"
                tmpFldVal = txtContext.Text
                tmpFldVal = CleanString(tmpFldVal, FOR_SERVER, False)
                Result = Result & tmpFldVal & ","
            Case Else
                Result = Result & ","
        End Select
        ItemNo = ItemNo + 1
        tmpFldNam = GetRealItem(MappFields, ItemNo, ",")
    Wend
    Result = Left(Result, Len(Result) - 1)
    Result = Result & vbLf
    If chkAftCheck(0).Value = 1 Then Result = Result & 0
    If chkAftCheck(1).Value = 1 Then Result = Result & 1
    CollectDllgData = Result
End Function
Private Function CollectDlysData(MappFields As String) As String
    Dim Result As String
    Dim tmpFldNam As String
    Dim tmpFldVal As String
    Dim LineNo As Long
    Dim ItemNo As Long
    Result = ""
    ItemNo = 0
    tmpFldNam = GetRealItem(MappFields, ItemNo, ",")
    While tmpFldNam <> ""
        Select Case tmpFldNam
            Case "TXT1"
                tmpFldVal = txtInput(0).Text
                Result = Result & tmpFldVal & ","
            Case "TEXT"
                tmpFldVal = txtContext.Text
                tmpFldVal = CleanString(tmpFldVal, FOR_SERVER, False)
                Result = Result & tmpFldVal & ","
            Case "TAB1"
                LineNo = TAB1.GetCurrentSelected
                If LineNo < 0 Then LineNo = 0
                tmpFldVal = TAB1.GetFieldValue(LineNo, tmpFldNam)
                Result = Result & tmpFldVal & ","
            Case "TAB2"
                LineNo = TAB1.GetCurrentSelected
                If LineNo < 0 Then LineNo = 0
                tmpFldVal = TAB1.GetFieldValue(LineNo, tmpFldNam)
                Result = Result & tmpFldVal & ","
            Case Else
                Result = Result & ","
        End Select
        ItemNo = ItemNo + 1
        tmpFldNam = GetRealItem(MappFields, ItemNo, ",")
    Wend
    Result = Left(Result, Len(Result) - 1)
    Result = Result & vbLf
    If chkAftCheck(0).Value = 1 Then Result = Result & 0
    If chkAftCheck(1).Value = 1 Then Result = Result & 1
    CollectDlysData = Result
End Function

Private Function CollectMvlgData(MappFields As String) As String
    Dim Result As String
    Dim tmpFldNam As String
    Dim tmpFldVal As String
    Dim ItemNo As Long
    Result = ""
    ItemNo = 0
    tmpFldNam = GetRealItem(MappFields, ItemNo, ",")
    While tmpFldNam <> ""
        Select Case tmpFldNam
            Case "TXT1"
                tmpFldVal = txtInput(0).Text
                Result = Result & tmpFldVal & ","
            Case "TEXT"
                tmpFldVal = txtContext.Text
                tmpFldVal = CleanString(tmpFldVal, FOR_SERVER, False)
                Result = Result & tmpFldVal & ","
            Case Else
                Result = Result & ","
        End Select
        ItemNo = ItemNo + 1
        tmpFldNam = GetRealItem(MappFields, ItemNo, ",")
    Wend
    Result = Left(Result, Len(Result) - 1)
    Result = Result & vbLf
    If chkAftCheck(0).Value = 1 Then Result = Result & 0
    If chkAftCheck(1).Value = 1 Then Result = Result & 1
    CollectMvlgData = Result
End Function
Private Function CollectRequData(MappFields As String) As String
    Dim Result As String
    Dim tmpFldNam As String
    Dim tmpFldVal As String
    Dim ItemNo As Long
    Dim DataChanged As Boolean
    Result = ""
    DataChanged = False
    If txtContext.Text <> txtContext.Tag Then DataChanged = True
    If txtInput(0).Text <> txtInput(0).Tag Then DataChanged = True
    If DataChanged Then chkCheck(0).Value = 0
    ItemNo = 0
    tmpFldNam = GetRealItem(MappFields, ItemNo, ",")
    While tmpFldNam <> ""
        Select Case tmpFldNam
            Case "TXT1"
                tmpFldVal = txtInput(0).Text
                Result = Result & tmpFldVal & ","
            Case "TEXT"
                tmpFldVal = txtContext.Text
                tmpFldVal = CleanString(tmpFldVal, FOR_SERVER, False)
                Result = Result & tmpFldVal & ","
            Case "CHK1"
                tmpFldVal = "X"
                If chkCheck(0).Value = 1 Then tmpFldVal = ""
                Result = Result & tmpFldVal & ","
            Case Else
                Result = Result & ","
        End Select
        ItemNo = ItemNo + 1
        tmpFldNam = GetRealItem(MappFields, ItemNo, ",")
    Wend
    Result = Left(Result, Len(Result) - 1)
    Result = Result & vbLf
    If chkAftCheck(0).Value = 1 Then Result = Result & 0
    If chkAftCheck(1).Value = 1 Then Result = Result & 1
    CollectRequData = Result
End Function

Private Sub btnClose_Click()
    GiveAnswer = False
    Me.Hide
End Sub

Private Sub btnOk_Click()
    GiveAnswer = True
    Me.Hide
End Sub

Private Sub chkAftCheck_Click(Index As Integer)
    If chkAftCheck(Index).Value = 1 Then
        lblFlno(Index).Caption = chkAftCheck(Index).Tag
        lblFlno(Index).BackColor = vbWhite
        If Me.Visible Then
            If Index = 1 Then
                chkAftCheck(0).Value = 0
            Else
                chkAftCheck(1).Value = 0
            End If
        End If
    Else
        lblFlno(Index).Caption = ""
        lblFlno(Index).BackColor = vbButtonFace
        If Me.Visible Then
            If Index = 1 Then
                chkAftCheck(0).Value = 1
            Else
                chkAftCheck(1).Value = 1
            End If
        End If
    End If
End Sub

Private Sub chkTool_Click(Index As Integer)
    If chkTool(Index).Value = 1 Then
        chkTool(Index).BackColor = LightGreen
        BottomPanel(0).Visible = True
    Else
        chkTool(Index).BackColor = vbButtonFace
        BottomPanel(0).Visible = False
    End If
End Sub

Private Sub Form_Load()
    BottomPanel(0).ZOrder
    StatusBar1.ZOrder
    RightPanel.ZOrder
    BottomPanel(0).Visible = False
    SetButtonFaceStyle Me
    InitDataTab
End Sub

Private Sub Form_Resize()
    Dim NewSize As Long
    NewSize = Me.ScaleWidth - RightPanel.Width - 15
    If NewSize > 300 Then
        WorkArea.Width = NewSize
        NewSize = NewSize - (txtContext.Left * 2) - 90
        txtContext.Width = NewSize
        BottomPanel(0).Left = ((NewSize - BottomPanel(0).Width) / 2) + txtContext.Left
        NewSize = WorkArea.Width - fraEditFrame(1).Left - 135
        If NewSize > 300 Then fraEditFrame(1).Width = NewSize
    End If
    NewSize = Me.ScaleHeight - StatusBar1.Height
    If NewSize > 300 Then
        BottomPanel(0).Top = NewSize - BottomPanel(0).Height - 60
        WorkArea.Height = NewSize
        NewSize = NewSize - txtContext.Top - 150
        If NewSize > 300 Then
            txtContext.Height = NewSize
            TAB1.Height = NewSize + 15
        End If
    End If
    If RightPanel.ScaleHeight > 90 Then
        If MainLifeStyle Then DrawBackGround RightPanel, 7, True, True
        If MainLifeStyle Then DrawBackGround WorkArea, 7, True, True
    End If
End Sub

Private Sub TAB1_SendRButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    InitTabSelection "TAB1,TAB2", TAB1.Tag
End Sub

Private Sub Timer1_Timer()
    Timer1.Enabled = False
    SetFormOnTop Me, True
End Sub

Private Sub txtContext_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub
Private Sub InitDataTab()
    Dim i As Integer
    Dim tmpStrg As String
    TAB1.ResetContent
    TAB1.Top = txtContext.Top
    TAB1.LogicalFieldList = "TAB1,TAB2"
    TAB1.HeaderString = "Context,   ID"
    TAB1.HeaderLengthString = "10,10"
    TAB1.ColumnWidthString = "10,2"
    TAB1.ColumnAlignmentString = "L,R"
    TAB1.HeaderFontSize = 17
    TAB1.FontSize = 17
    TAB1.LineHeight = 17
    TAB1.SetTabFontBold True
    TAB1.SetMainHeaderValues "2", "Reason", ""
    TAB1.SetMainHeaderFont 17, False, False, True, 0, "Courier New"
    TAB1.MainHeader = True
    If GridLifeStyle Then TAB1.LifeStyle = True
    If GridLifeStyle Then TAB1.CursorLifeStyle = True
    TAB1.InsertTextLine "-.-,00", False
    For i = 0 To MainDialog.DlyEditPanel.UBound
        If MainDialog.DlyEditPanel(i).Visible Then
            If Val(MainDialog.DlyEditPanel(i).Tag) = 0 Then
                tmpStrg = MainDialog.lblDlyReason(i).Caption & ","
                tmpStrg = tmpStrg & MainDialog.lblDlyReason(i).Tag
                TAB1.InsertTextLine tmpStrg, False
            End If
        End If
    Next
    TAB1.AutoSizeByHeader = True
    TAB1.AutoSizeColumns
End Sub
Private Sub InitTabSelection(FieldList As String, DataList As String)
    Dim LineNo As Long
    Dim ColNo As Long
    Dim ItemNo As Long
    Dim HitList As String
    Dim tmpFldNam As String
    Dim tmpFldVal As String
    LineNo = -1
    ItemNo = 0
    tmpFldNam = GetRealItem(FieldList, ItemNo, ",")
    While tmpFldNam <> ""
        tmpFldVal = GetRealItem(DataList, ItemNo, ",")
        ColNo = Val(TranslateFieldItems(tmpFldNam, TAB1.LogicalFieldList))
        HitList = TAB1.GetLinesByColumnValue(ColNo, tmpFldVal, 0)
        If HitList <> "" Then
            LineNo = Val(HitList)
            TAB1.SetCurrentSelection LineNo
        End If
        ItemNo = ItemNo + 1
        tmpFldNam = GetRealItem(FieldList, ItemNo, ",")
    Wend
    If LineNo < 0 Then
        TAB1.InsertTextLine DataList, False
        TAB1.SetCurrentSelection 0
    End If
    TAB1.Tag = DataList
    TAB1.Visible = True
End Sub

Private Sub txtInput_Change(Index As Integer)
    Dim CheckIt As Boolean
    Dim tmpTxt As String
    tmpTxt = txtInput(Index).Text
    CheckIt = CheckTimeValues(tmpTxt, False, False)
    If CheckIt Then
        txtInput(Index).BackColor = vbWhite
        txtInput(Index).ForeColor = vbBlack
    Else
        txtInput(Index).BackColor = vbRed
        txtInput(Index).ForeColor = vbWhite
    End If
End Sub

Private Sub txtInput_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Private Sub txtInput_KeyPress(Index As Integer, KeyAscii As Integer)
    If Not ((KeyAscii >= 48 And KeyAscii <= 57) Or (KeyAscii = 8) Or (Chr(KeyAscii) = ":")) Then
        KeyAscii = 0
    End If
End Sub
