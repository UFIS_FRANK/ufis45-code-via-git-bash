VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form FilterDialog 
   Caption         =   "Record Filter Preset"
   ClientHeight    =   4830
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   13425
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   11.25
      Charset         =   161
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FilterDialog.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4830
   ScaleWidth      =   13425
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox RightPanel 
      AutoRedraw      =   -1  'True
      BackColor       =   &H000040C0&
      Height          =   3075
      Left            =   3060
      ScaleHeight     =   3015
      ScaleWidth      =   915
      TabIndex        =   35
      TabStop         =   0   'False
      Top             =   1320
      Visible         =   0   'False
      Width           =   975
      Begin VB.CheckBox chkFilterIsVisible 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   0
         Left            =   45
         MaskColor       =   &H8000000F&
         TabIndex        =   51
         Tag             =   "-1"
         Top             =   2070
         Visible         =   0   'False
         Width           =   210
      End
      Begin VB.CheckBox chkFullSize 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   60
         TabIndex        =   48
         Top             =   1560
         Width           =   210
      End
      Begin VB.CheckBox chkToggle 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   60
         TabIndex        =   47
         Top             =   1800
         Width           =   210
      End
      Begin VB.CheckBox chkOnTop 
         Caption         =   "Top"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   45
         Top             =   30
         Width           =   540
      End
      Begin VB.CheckBox Check2 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   570
         Style           =   1  'Graphical
         TabIndex        =   44
         ToolTipText     =   "Bypass the Clear Function"
         Top             =   30
         Width           =   315
      End
      Begin VB.CheckBox chkAppl 
         Caption         =   "Adjust"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   43
         Tag             =   "AODBREFRESH"
         Top             =   1050
         Width           =   855
      End
      Begin VB.CheckBox chkAppl 
         Caption         =   "Load"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   42
         Tag             =   "AODB_FILTER"
         Top             =   390
         Width           =   855
      End
      Begin VB.CheckBox chkAppl 
         Caption         =   "Close"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   30
         Style           =   1  'Graphical
         TabIndex        =   36
         Tag             =   "CLOSE"
         Top             =   720
         Width           =   855
      End
      Begin VB.HScrollBar FilterScroll 
         Height          =   315
         Left            =   30
         TabIndex        =   46
         Top             =   30
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.Label lblToggle 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Toggle"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   0
         Left            =   300
         TabIndex        =   54
         Top             =   1770
         Width           =   600
      End
      Begin VB.Label lblToggle 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Toggle"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Index           =   1
         Left            =   315
         TabIndex        =   55
         Top             =   1845
         Width           =   600
      End
      Begin VB.Label lblFilterShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Shadow"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Index           =   0
         Left            =   90
         TabIndex        =   53
         Top             =   2310
         Visible         =   0   'False
         Width           =   690
      End
      Begin VB.Label lblFilterName 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   0
         Left            =   300
         TabIndex        =   52
         Top             =   2100
         Visible         =   0   'False
         Width           =   585
      End
      Begin VB.Label lblFullSize 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Full"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   195
         Index           =   0
         Left            =   300
         TabIndex        =   49
         Top             =   1530
         Width           =   315
      End
      Begin VB.Label lblFullSize 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Full"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Index           =   1
         Left            =   465
         TabIndex        =   50
         Top             =   1605
         Width           =   315
      End
   End
   Begin VB.PictureBox TopPanel 
      AutoRedraw      =   -1  'True
      BackColor       =   &H000040C0&
      Height          =   1125
      Left            =   60
      ScaleHeight     =   1065
      ScaleWidth      =   7785
      TabIndex        =   34
      TabStop         =   0   'False
      Top             =   60
      Visible         =   0   'False
      Width           =   7845
      Begin VB.TextBox txtVpfr 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   690
         MaxLength       =   4
         TabIndex        =   0
         Top             =   420
         Width           =   585
      End
      Begin VB.TextBox txtVpfr 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   1290
         MaxLength       =   2
         TabIndex        =   1
         Top             =   420
         Width           =   375
      End
      Begin VB.TextBox txtVpfr 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   1680
         MaxLength       =   2
         TabIndex        =   2
         Top             =   420
         Width           =   375
      End
      Begin VB.TextBox txtVpto 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   3540
         MaxLength       =   2
         TabIndex        =   5
         Top             =   420
         Width           =   375
      End
      Begin VB.TextBox txtVpto 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   3150
         MaxLength       =   2
         TabIndex        =   4
         Top             =   420
         Width           =   375
      End
      Begin VB.TextBox txtVpto 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   2550
         MaxLength       =   4
         TabIndex        =   3
         Top             =   420
         Width           =   585
      End
      Begin VB.Label TopLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Label"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   240
         Index           =   0
         Left            =   720
         TabIndex        =   41
         Top             =   0
         Width           =   600
      End
      Begin VB.Label TopShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Shadow"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000015&
         Height          =   240
         Index           =   0
         Left            =   1500
         TabIndex        =   40
         Top             =   0
         Width           =   840
      End
   End
   Begin VB.PictureBox WorkArea 
      AutoRedraw      =   -1  'True
      BackColor       =   &H000040C0&
      Height          =   2235
      Left            =   60
      ScaleHeight     =   2175
      ScaleWidth      =   2685
      TabIndex        =   28
      TabStop         =   0   'False
      Top             =   1650
      Visible         =   0   'False
      Width           =   2745
      Begin VB.PictureBox FilterPanel 
         Height          =   1845
         Index           =   0
         Left            =   90
         ScaleHeight     =   1785
         ScaleWidth      =   2295
         TabIndex        =   29
         TabStop         =   0   'False
         Top             =   60
         Visible         =   0   'False
         Width           =   2355
         Begin VB.PictureBox ButtonPanel 
            AutoRedraw      =   -1  'True
            Height          =   405
            Index           =   0
            Left            =   0
            ScaleHeight     =   345
            ScaleWidth      =   2115
            TabIndex        =   37
            TabStop         =   0   'False
            Top             =   1350
            Width           =   2175
            Begin VB.CheckBox chkRightFilterSet 
               Caption         =   "0"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   0
               Left            =   1215
               Style           =   1  'Graphical
               TabIndex        =   39
               Top             =   30
               Width           =   765
            End
            Begin VB.CheckBox chkLeftFilterSet 
               Caption         =   "0"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   161
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   0
               Left            =   180
               Style           =   1  'Graphical
               TabIndex        =   38
               Top             =   30
               Width           =   765
            End
         End
         Begin VB.TextBox txtFilterVal 
            Alignment       =   2  'Center
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   870
            TabIndex        =   31
            Top             =   15
            Width           =   855
         End
         Begin VB.CheckBox chkWork 
            Caption         =   "Filter"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   15
            Style           =   1  'Graphical
            TabIndex        =   30
            ToolTipText     =   "Activates/De-Activates the filter "
            Top             =   15
            Width           =   855
         End
         Begin TABLib.TAB LeftFilterTab 
            Height          =   915
            Index           =   0
            Left            =   15
            TabIndex        =   32
            Top             =   345
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   1614
            _StockProps     =   64
            Columns         =   10
         End
         Begin TABLib.TAB RightFilterTab 
            Height          =   915
            Index           =   0
            Left            =   870
            TabIndex        =   33
            Top             =   345
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   1614
            _StockProps     =   64
            Columns         =   10
         End
      End
   End
   Begin VB.Frame fraDispoButtonPanel 
      BorderStyle     =   0  'None
      Height          =   405
      Left            =   4590
      TabIndex        =   23
      Top             =   2100
      Visible         =   0   'False
      Width           =   7005
      Begin VB.CheckBox Check1 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3870
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   0
         Width           =   855
      End
      Begin TABLib.TAB NatTabCombo 
         Height          =   315
         Index           =   0
         Left            =   0
         TabIndex        =   26
         Top             =   0
         Visible         =   0   'False
         Width           =   3855
         _Version        =   65536
         _ExtentX        =   6800
         _ExtentY        =   556
         _StockProps     =   64
      End
      Begin VB.CheckBox chkSetDispo 
         Caption         =   "Perform"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4740
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkClose 
         Caption         =   "Close"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5610
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   0
         Width           =   855
      End
   End
   Begin VB.Frame fraFilterButtonPanel 
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   4620
      TabIndex        =   17
      Top             =   1740
      Visible         =   0   'False
      Width           =   5175
      Begin VB.CheckBox chkTool 
         Caption         =   "Adjust"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   2610
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Close"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   3480
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Refresh"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1740
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Load"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Compare"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   870
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   0
         Width           =   855
      End
   End
   Begin VB.Frame fraFredButtonPanel 
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   4620
      TabIndex        =   7
      Top             =   1350
      Visible         =   0   'False
      Width           =   3585
      Begin VB.CheckBox chkWkDay 
         Caption         =   "All"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   8
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   16
         ToolTipText     =   "Select all frequency days"
         Top             =   0
         Width           =   765
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "7"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   7
         Left            =   2400
         Style           =   1  'Graphical
         TabIndex        =   15
         ToolTipText     =   "Frequency filter for Sunday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "6"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   6
         Left            =   2130
         Style           =   1  'Graphical
         TabIndex        =   14
         ToolTipText     =   "Frequency filter for Saturday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "5"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   5
         Left            =   1860
         Style           =   1  'Graphical
         TabIndex        =   13
         ToolTipText     =   "Frequency filter for Friday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   1590
         Style           =   1  'Graphical
         TabIndex        =   12
         ToolTipText     =   "Frequency filter for Thursday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   1320
         Style           =   1  'Graphical
         TabIndex        =   11
         ToolTipText     =   "Frequency filter for Wednesday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "2"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   1050
         Style           =   1  'Graphical
         TabIndex        =   10
         ToolTipText     =   "Frequency filter for Thuesday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   780
         Style           =   1  'Graphical
         TabIndex        =   9
         ToolTipText     =   "Frequency filter for Monday"
         Top             =   0
         Width           =   255
      End
      Begin VB.CheckBox chkWkDay 
         Caption         =   "None"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   2670
         Style           =   1  'Graphical
         TabIndex        =   8
         Tag             =   "......."
         ToolTipText     =   "Unselect all frequency days"
         Top             =   0
         Width           =   795
      End
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   6
      Top             =   4545
      Width           =   13425
      _ExtentX        =   23680
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   1
            Object.Width           =   23142
            MinWidth        =   1764
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "FilterDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim InternalResize As Boolean
Dim LayoutChanged As Boolean
Dim MyGlobalResult As String
Dim MyFilterIniSection As String
Dim MyCurParent As Form

Public Function InitMyLayout(MyParent As Form, ShowModal As Boolean, LayoutStyle As String) As String
    Dim MyTop As Long
    Dim MyLeft As Long
    Set MyCurParent = MyParent
    If LayoutStyle <> Me.Tag Then LayoutChanged = True Else LayoutChanged = False
    MyFilterIniSection = GetIniEntry(myIniFullName, myIniSection, "", "AODB_FILTER_SECTION", myIniSection)
    
    Me.Tag = ""
    InitTopPanel LayoutStyle
    InitWorkArea LayoutStyle
    Me.Tag = LayoutStyle
    MyTop = MyCurParent.Top + ((MyCurParent.Height - Me.Height) / 2)
    MyLeft = MyCurParent.Left + ((MyCurParent.Width - Me.Width) / 2)
    If MyTop > Screen.Height - Me.Height Then MyTop = Screen.Height - Me.Height
    If MyTop < 0 Then MyTop = 0
    If MyLeft > Screen.Width - Me.Width Then MyLeft = Screen.Width - Me.Width
    If MyLeft < 0 Then MyLeft = 0
    Me.Top = MyTop
    Me.Left = MyLeft
    Me.Caption = GetFilterConfig(LayoutStyle, "FILTER_CAPTION", -1)
    If ShowModal Then
        Me.Show vbModal, MyParent
    Else
        Me.Show , MyParent
    End If
    InitMyLayout = MyGlobalResult
End Function

Private Sub InitTopPanel(LayoutType As String)
    Dim i As Integer
    Dim NewLeft As Long
    Dim tmpData As String
    TopPanel.Top = 0
    TopPanel.Left = 0
    If TopLabel.UBound = 0 Then
        For i = 1 To 2
            Load TopLabel(i)
            Load TopShadow(i)
        Next
    End If
    TopLabel(0).Caption = "Timeframe Filter"
    TopLabel(0).Top = 120
    TopLabel(0).Left = 120
    NewLeft = 120
    For i = 0 To 2
        txtVpfr(i).Top = 420
        txtVpfr(i).Left = NewLeft
        NewLeft = NewLeft + txtVpfr(i).Width
    Next
    NewLeft = NewLeft + 90
    TopLabel(1).Caption = "-"
    TopLabel(1).Top = 450
    TopLabel(1).Left = NewLeft
    NewLeft = NewLeft + TopLabel(1).Width + 60
    For i = 0 To 2
        txtVpto(i).Top = 420
        txtVpto(i).Left = NewLeft
        NewLeft = NewLeft + txtVpto(i).Width
    Next
    For i = 0 To 1
        TopLabel(i).ZOrder
        TopShadow(i).Caption = TopLabel(i).Caption
        TopShadow(i).Top = TopLabel(i).Top + 15
        TopShadow(i).Left = TopLabel(i).Left + 15
        TopLabel(i).Visible = True
        TopShadow(i).Visible = True
    Next
    tmpData = Format(Now, "yyyymmdd")
    If txtVpfr(0).Tag = "" Then
        txtVpfr(0).Text = Mid(tmpData, 1, 4)
        txtVpfr(1).Text = Mid(tmpData, 5, 2)
        txtVpfr(2).Text = Mid(tmpData, 7, 2)
    End If
    If txtVpto(0).Tag = "" Then
        txtVpto(0).Text = Mid(tmpData, 1, 4)
        txtVpto(1).Text = Mid(tmpData, 5, 2)
        txtVpto(2).Text = Mid(tmpData, 7, 2)
    End If
    TopPanel.Height = 900
    TopPanel.Width = Screen.Width + 300
    DrawBackGround TopPanel, 7, True, False
End Sub
Private Sub InitWorkArea(LayoutStyle As String)
    Dim UsedPanels As Integer
    Dim NewLeft As Long
    Dim NewTop As Long
    Dim TabLeft As Long
    Dim TabWidth As Long
    Dim WidthList As String
    Dim TabCfgLine As String
    Dim ItemNo As Long
    Dim NewSection As Boolean
    Dim FilterCount As Integer
    Dim CurFilter As Integer
    Dim PanelLayout As String
    Dim tmpHeader As String
    
    
    For UsedPanels = 0 To FilterPanel.UBound
        FilterPanel(UsedPanels).Visible = False
        chkFilterIsVisible(UsedPanels).Visible = False
        lblFilterName(UsedPanels).Visible = False
        lblFilterShadow(UsedPanels).Visible = False
        RightFilterTab(UsedPanels).myTag = ""
        HiddenData.UpdateTabConfig LeftFilterTab(UsedPanels).myName, "STAT", "H"
    Next
    WorkArea.Top = TopPanel.Height
    FilterCount = Val(GetFilterConfig(LayoutStyle, "FILTER_PANELS", -1))
    
    NewLeft = 120
    NewTop = chkToggle.Top + chkToggle.Height + 90
    UsedPanels = -1
    ItemNo = 0
    For CurFilter = 1 To FilterCount
        PanelLayout = GetFilterConfig(LayoutStyle, "PANEL_LAYOUT", CurFilter)
        UsedPanels = UsedPanels + 1
        NewSection = False
        If UsedPanels > FilterPanel.UBound Then
            Load FilterPanel(UsedPanels)
            Load txtFilterVal(UsedPanels)
            Load chkWork(UsedPanels)
            Load LeftFilterTab(UsedPanels)
            Load RightFilterTab(UsedPanels)
            Load ButtonPanel(UsedPanels)
            Load chkFilterIsVisible(UsedPanels)
            Load lblFilterName(UsedPanels)
            Load lblFilterShadow(UsedPanels)
            Set txtFilterVal(UsedPanels).Container = FilterPanel(UsedPanels)
            Set chkWork(UsedPanels).Container = FilterPanel(UsedPanels)
            Set LeftFilterTab(UsedPanels).Container = FilterPanel(UsedPanels)
            Set RightFilterTab(UsedPanels).Container = FilterPanel(UsedPanels)
            Set ButtonPanel(UsedPanels).Container = FilterPanel(UsedPanels)
            Load chkLeftFilterSet(UsedPanels)
            Load chkRightFilterSet(UsedPanels)
            Set chkLeftFilterSet(UsedPanels).Container = ButtonPanel(UsedPanels)
            Set chkRightFilterSet(UsedPanels).Container = ButtonPanel(UsedPanels)
            NewSection = True
        End If
        
        FilterPanel(UsedPanels).Top = 120
        FilterPanel(UsedPanels).Left = NewLeft
        FilterPanel(UsedPanels).Visible = True
        txtFilterVal(UsedPanels).Visible = True
        If (LayoutChanged) Or (NewSection) Then LeftFilterTab(UsedPanels).ResetContent
        tmpHeader = GetRealItem(PanelLayout, 4, "|")
        tmpHeader = BuildDummyHeader(tmpHeader)
        LeftFilterTab(UsedPanels).HeaderString = tmpHeader
        LeftFilterTab(UsedPanels).LogicalFieldList = GetRealItem(PanelLayout, 3, "|")
        LeftFilterTab(UsedPanels).HeaderLengthString = "10,10"
        LeftFilterTab(UsedPanels).SelectBackColor = DarkestYellow
        LeftFilterTab(UsedPanels).SelectTextColor = vbWhite
        LeftFilterTab(UsedPanels).EmptyAreaBackColor = LightGray
        LeftFilterTab(UsedPanels).EmptyAreaRightColor = LightGray
        LeftFilterTab(UsedPanels).MainHeaderOnly = True
        LeftFilterTab(UsedPanels).FontSize = 17
        LeftFilterTab(UsedPanels).HeaderFontSize = 17
        LeftFilterTab(UsedPanels).LineHeight = 17
        LeftFilterTab(UsedPanels).LeftTextOffset = 1
        LeftFilterTab(UsedPanels).SetTabFontBold True
        LeftFilterTab(UsedPanels).AutoSizeByHeader = True
        LeftFilterTab(UsedPanels).AutoSizeColumns
        WidthList = LeftFilterTab(UsedPanels).HeaderLengthString
        TabWidth = Val(GetRealItem(WidthList, 0, ",")) * 15
        If GetRealItem(PanelLayout, 2, "|") = "Y" Then TabWidth = TabWidth + Val(GetRealItem(WidthList, 1, ",")) * 15
        TabWidth = TabWidth + 16 * 15 'ScrollBar
        If TabWidth < 855 Then TabWidth = 855
        LeftFilterTab(UsedPanels).Width = TabWidth
        LeftFilterTab(UsedPanels).Visible = True
        
        If (LayoutChanged) Or (NewSection) Then RightFilterTab(UsedPanels).ResetContent
        RightFilterTab(UsedPanels).Width = TabWidth
        RightFilterTab(UsedPanels).HeaderString = tmpHeader
        RightFilterTab(UsedPanels).LogicalFieldList = GetRealItem(PanelLayout, 3, "|")
        RightFilterTab(UsedPanels).HeaderLengthString = "10,10"
        RightFilterTab(UsedPanels).Left = LeftFilterTab(UsedPanels).Left + TabWidth + 15
        RightFilterTab(UsedPanels).SelectBackColor = DarkBlue
        RightFilterTab(UsedPanels).SelectTextColor = vbWhite
        RightFilterTab(UsedPanels).EmptyAreaBackColor = LightGray
        RightFilterTab(UsedPanels).EmptyAreaRightColor = LightGray
        RightFilterTab(UsedPanels).MainHeaderOnly = True
        RightFilterTab(UsedPanels).FontSize = 17
        RightFilterTab(UsedPanels).HeaderFontSize = 17
        RightFilterTab(UsedPanels).LineHeight = 17
        RightFilterTab(UsedPanels).LeftTextOffset = 1
        RightFilterTab(UsedPanels).SetTabFontBold True
        RightFilterTab(UsedPanels).AutoSizeByHeader = True
        RightFilterTab(UsedPanels).AutoSizeColumns
        RightFilterTab(UsedPanels).Visible = True
        
        chkWork(UsedPanels).Width = TabWidth
        txtFilterVal(UsedPanels).Width = TabWidth
        txtFilterVal(UsedPanels).Left = RightFilterTab(UsedPanels).Left
        FilterPanel(UsedPanels).Width = RightFilterTab(UsedPanels).Left + RightFilterTab(UsedPanels).Width + 60
        
        chkLeftFilterSet(UsedPanels).Width = TabWidth - 90
        chkRightFilterSet(UsedPanels).Width = TabWidth - 90
        chkLeftFilterSet(UsedPanels).Left = LeftFilterTab(UsedPanels).Left + ((TabWidth - chkLeftFilterSet(UsedPanels).Width) / 2)
        chkRightFilterSet(UsedPanels).Left = RightFilterTab(UsedPanels).Left + ((TabWidth - chkRightFilterSet(UsedPanels).Width) / 2) - 15
        chkLeftFilterSet(UsedPanels).Visible = True
        chkRightFilterSet(UsedPanels).Visible = True
        ButtonPanel(UsedPanels).Left = -30
        ButtonPanel(UsedPanels).Width = FilterPanel(UsedPanels).Width + 30
        DrawBackGround ButtonPanel(UsedPanels), 7, True, True
        ButtonPanel(UsedPanels).Visible = True
        
        chkWork(UsedPanels).Caption = GetRealItem(PanelLayout, 0, "|")
        If (LayoutChanged) Or (NewSection) Then
            chkWork(UsedPanels).Value = 0
            chkWork(UsedPanels).Value = 1
            If GetRealItem(PanelLayout, 1, "|") <> "Y" Then chkWork(UsedPanels).Value = 0
            chkWork(UsedPanels).Tag = ""
        End If
        chkWork(UsedPanels).Visible = True
        
        LeftFilterTab(UsedPanels).myName = "FILTER_LEFT_" & CStr(UsedPanels)
        DefineDataConfig LeftFilterTab(UsedPanels), LayoutStyle, PanelLayout
        RightFilterTab(UsedPanels).myTag = GetRealItem(RightFilterTab(UsedPanels).LogicalFieldList, 0, ",")
        
        chkFilterIsVisible(UsedPanels).Top = NewTop
        lblFilterName(UsedPanels).Top = NewTop + 15
        lblFilterShadow(UsedPanels).Top = NewTop + 30
        lblFilterName(UsedPanels).Left = chkFilterIsVisible(UsedPanels).Left + chkFilterIsVisible(UsedPanels).Width + 15
        lblFilterShadow(UsedPanels).Left = lblFilterName(UsedPanels).Left + 15
        lblFilterName(UsedPanels).Caption = GetRealItem(PanelLayout, 0, "|")
        lblFilterShadow(UsedPanels).Caption = GetRealItem(PanelLayout, 0, "|")
        lblFilterName(UsedPanels).ZOrder
        chkFilterIsVisible(UsedPanels).Value = 1
        chkFilterIsVisible(UsedPanels).Visible = True
        lblFilterName(UsedPanels).Visible = True
        lblFilterShadow(UsedPanels).Visible = True
        
        NewTop = NewTop + chkFilterIsVisible(UsedPanels).Height + 15
        NewLeft = FilterPanel(UsedPanels).Left + FilterPanel(UsedPanels).Width + 120
        ItemNo = ItemNo + 1
    Next
    RightPanel.Align = 0
    RightPanel.Height = Screen.Height
    DrawBackGround RightPanel, 7, True, True
    RightPanel.Align = 4
    RightPanel.Visible = True
    TopPanel.Visible = True
    Me.Width = NewLeft + RightPanel.Width + 180
End Sub
Private Function BuildDummyHeader(SizeList As String) As String
    Dim Result As String
    Dim ItemNo As Long
    Dim ItemVal As String
    Dim ColWidth As Long
    ItemNo = 0
    ItemVal = GetRealItem(SizeList, ItemNo, ",")
    While ItemVal <> ""
        ColWidth = Val(ItemVal)
        Result = Result & String(ColWidth, "-") & ","
        ItemNo = ItemNo + 1
        ItemVal = GetRealItem(SizeList, ItemNo, ",")
    Wend
    If Result <> "" Then Result = Left(Result, Len(Result) - 1)
    BuildDummyHeader = Result
End Function
Private Sub DefineDataConfig(CurTab As TABLib.Tab, LayoutStyle As String, PanelLayout As String)
    Dim TabCfgLine As String
    Dim TabCfgFields As String
    Dim TabCfgData As String
    Dim HiddenTab As String
    Dim tmpFldLst As String
    Dim tmpSqlKey As String
    Dim ItemNo As Long
    Dim ItemVal As String
    TabCfgLine = CurTab.myName & Chr(15)
    TabCfgLine = TabCfgLine & "0" & Chr(15)
    TabCfgLine = TabCfgLine & "V" & Chr(15)
    TabCfgLine = TabCfgLine & "SEL" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & CurTab.LogicalFieldList & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    HiddenData.CheckTabConfig TabCfgLine
    HiddenTab = HiddenData.CreateFilterTab(CurTab.Index)
    HiddenData.UpdateTabConfig CurTab.myName, "DSRC", HiddenTab
    
    TabCfgLine = GetFilterConfig(LayoutStyle, "READAODB," & CStr(CurTab.Index), -1)
    TabCfgFields = "DTAB,LFLD,DFLD,DSQL"
    TabCfgData = ""
    TabCfgData = TabCfgData & GetRealItem(PanelLayout, 5, "|") & Chr(15)
    TabCfgData = TabCfgData & GetRealItem(PanelLayout, 3, "|") & Chr(15)
    ItemNo = 6
    ItemVal = GetRealItem(PanelLayout, ItemNo, "|")
    While ItemVal <> ""
        tmpFldLst = tmpFldLst & ItemVal & Chr(14)
        ItemNo = ItemNo + 1
        ItemVal = GetRealItem(PanelLayout, ItemNo, "|")
        tmpSqlKey = tmpSqlKey & ItemVal & Chr(14)
        ItemNo = ItemNo + 1
        ItemVal = GetRealItem(PanelLayout, ItemNo, "|")
    Wend
    If tmpFldLst <> "" Then tmpFldLst = Left(tmpFldLst, Len(tmpFldLst) - 1)
    If tmpSqlKey <> "" Then tmpSqlKey = Left(tmpSqlKey, Len(tmpSqlKey) - 1)
    TabCfgData = TabCfgData & tmpFldLst & Chr(15) & tmpSqlKey
    
    HiddenData.UpdateTabConfig HiddenTab, TabCfgFields, TabCfgData
End Sub
Private Function GetFilterConfig(LayoutType As String, ForWhat As String, Index As Integer) As String
    Dim Result As String
    Dim TypeCode As String
    Dim WhatCode As String
    Dim UseIniKey As String
    Result = ""
    TypeCode = GetRealItem(LayoutType, 0, ",")
    WhatCode = GetRealItem(ForWhat, 0, ",")
    Select Case TypeCode
        Case "AODB_FILTER"
            Select Case WhatCode
                Case "FILTER_PANELS"
                    Result = GetIniEntry(myIniFullName, MyFilterIniSection, "", "AODB_FILTER_PANELS", "0")
                Case "FILTER_CAPTION"
                    Result = GetIniEntry(myIniFullName, MyFilterIniSection, "", "AODB_FILTER_CAPTION", "AODB Flight Data Filter")
                Case "PANEL_LAYOUT"
                    UseIniKey = "AODB_FILTER_PANEL_" & CStr(Index)
                    Result = GetIniEntry(myIniFullName, MyFilterIniSection, "", UseIniKey, "??|??,??")
                Case Else
            End Select
        Case Else
    End Select
    GetFilterConfig = Result
End Function
Private Sub chkAppl_Click(Index As Integer)
    If chkAppl(Index).Value = 1 Then
        chkAppl(Index).BackColor = LightGreen
        chkAppl(Index).Refresh
        Select Case chkAppl(Index).Tag
            Case "CLOSE"
                HandleClose chkAppl(Index)
            Case "AODB_FILTER"
                HandleAodbFilter chkAppl(Index), True
            Case "AODBREFRESH"
                HandleAodbRefresh chkAppl(Index)
            Case Else
                chkAppl(Index).Value = 0
        End Select
    Else
        chkAppl(Index).BackColor = MyOwnButtonFace
    End If
End Sub
Private Sub HandleToggleFilter(CurButton As CheckBox)
    Dim i As Integer
    For i = 0 To FilterPanel.UBound
        If FilterPanel(i).Visible Then
            If chkWork(i).Value = 1 Then
                ToggleTabColumns LeftFilterTab(i)
                ToggleTabColumns RightFilterTab(i)
            End If
        End If
    Next
End Sub
Private Sub ToggleTabColumns(CurTab As TABLib.Tab)
    Dim tmpLogFields As String
    Dim tmpNewFields As String
    Dim tmpBuff As String
    Dim LineNo As Long
    tmpLogFields = CurTab.LogicalFieldList
    tmpNewFields = GetRealItem(tmpLogFields, 1, ",") & "," & GetRealItem(tmpLogFields, 0, ",")
    LineNo = CurTab.GetLineCount - 1
    If LineNo >= 0 Then
        tmpBuff = CurTab.GetBufferByFieldList(0, LineNo, tmpNewFields, vbLf)
        CurTab.ResetContent
        CurTab.InsertBuffer tmpBuff, vbLf
        CurTab.Sort "0", True, True
    End If
    CurTab.LogicalFieldList = tmpNewFields
    tmpLogFields = CurTab.HeaderString
    tmpNewFields = GetRealItem(tmpLogFields, 1, ",") & "," & GetRealItem(tmpLogFields, 0, ",")
    CurTab.HeaderString = tmpNewFields
    CurTab.AutoSizeColumns
    CurTab.Refresh
End Sub
Private Sub HandleAodbRefresh(CurButton As CheckBox)
    Dim i As Integer
    For i = 0 To FilterPanel.UBound
        If FilterPanel(i).Visible Then
            If chkWork(i).Value = 1 Then
                chkWork(i).Value = 0
                chkWork(i).Value = 1
            End If
        End If
    Next
    CurButton.Value = 0
    CurButton.Enabled = False
End Sub
Private Sub HandleAodbFilter(CurButton As CheckBox, CloseWin As Boolean)
    Dim tmpMsg As String
    Dim tmpFields As String
    Dim tmpData As String
    Dim tmpBuff As String
    Dim tmpFld As String
    Dim i As Integer
    Dim j As Long
    Dim LineNo As Long
    tmpMsg = ""
    If CloseWin Then
        If txtVpfr(0).Tag = "" Then tmpMsg = tmpMsg & "Period Begin" & vbNewLine
        If txtVpto(0).Tag = "" Then tmpMsg = tmpMsg & "Period End" & vbNewLine
    End If
    If tmpMsg = "" Then
        MyGlobalResult = ""
        tmpFields = "VPFR" & Chr(15) & "VPTO" & Chr(15) & "HOPO"
        tmpData = txtVpfr(0).Tag & Chr(15) & txtVpto(0).Tag & Chr(15) & UfisServer.HOPO
        For i = 0 To FilterPanel.UBound
            If FilterPanel(i).Visible Then
                If chkWork(i).Value = 1 Then
                    If LeftFilterTab(i).GetLineCount > 0 Then
                        LineNo = RightFilterTab(i).GetLineCount - 1
                        If LineNo >= 0 Then
                            tmpFld = RightFilterTab(i).myTag
                            tmpFields = tmpFields & Chr(15) & tmpFld
                            tmpBuff = RightFilterTab(i).GetBufferByFieldList(0, LineNo, tmpFld, ",")
                            tmpBuff = "'" & Replace(tmpBuff, ",", "','", 1, -1, vbBinaryCompare) & "'"
                            tmpData = tmpData & Chr(15) & tmpBuff
                        End If
                    End If
                End If
            End If
        Next
        MyGlobalResult = tmpFields & Chr(16) & tmpData
        If CloseWin Then Me.Hide
    Else
        tmpMsg = "Missing or wrong input for:" & vbNewLine & tmpMsg
        If MyMsgBox.CallAskUser(0, 0, 0, "Action Control", tmpMsg, "stop2", "", UserAnswer) >= 0 Then DoNothing
    End If
    If CloseWin Then CurButton.Value = 0
End Sub
Private Sub HandleClose(CurButton As CheckBox)
    MyGlobalResult = ""
    Me.Hide
    CurButton.Value = 0
End Sub

Private Sub chkFilterIsVisible_Click(Index As Integer)
    Dim MyOldWidth As Long
    Dim MyNewWidth As Long
    Dim NewLeft As Long
    Dim TabWidth As Long
    Dim WidthList As String
    Dim i As Integer
    MyOldWidth = Me.Width
    NewLeft = FilterPanel(0).Left
    For i = 0 To LeftFilterTab.UBound
        If RightFilterTab(i).myTag <> "" Then
            If chkFilterIsVisible(i).Value = 1 Then
                FilterPanel(i).Left = NewLeft
                FilterPanel(i).Visible = True
                NewLeft = FilterPanel(i).Left + FilterPanel(i).Width + 120
            Else
                FilterPanel(i).Visible = False
            End If
        End If
    Next
    InternalResize = True
    MyNewWidth = NewLeft + RightPanel.Width + 180
    NewLeft = Me.Left + MyOldWidth - MyNewWidth
    Me.Move NewLeft, Me.Top, MyNewWidth, Me.Height
    InternalResize = False
    Form_Resize
End Sub

Private Sub chkFullSize_Click()
    Dim MyOldWidth As Long
    Dim MyNewWidth As Long
    Dim NewLeft As Long
    Dim TabWidth As Long
    Dim WidthList As String
    Dim i As Integer
    MyOldWidth = Me.Width
    NewLeft = FilterPanel(0).Left
    For i = 0 To LeftFilterTab.UBound
        If RightFilterTab(i).myTag <> "" Then
            WidthList = LeftFilterTab(i).HeaderLengthString
            TabWidth = Val(GetRealItem(WidthList, 0, ",")) * 15
            If chkFullSize.Value = 1 Then TabWidth = TabWidth + Val(GetRealItem(WidthList, 1, ",")) * 15
            TabWidth = TabWidth + 16 * 15 'ScrollBar
            If TabWidth < 855 Then TabWidth = 855
            LeftFilterTab(i).Width = TabWidth
            RightFilterTab(i).Width = TabWidth
            RightFilterTab(i).Left = LeftFilterTab(i).Left + TabWidth + 15
            FilterPanel(i).Left = NewLeft
            FilterPanel(i).Width = RightFilterTab(i).Left + RightFilterTab(i).Width + 60
            chkWork(i).Width = TabWidth
            txtFilterVal(i).Width = TabWidth
            txtFilterVal(i).Left = RightFilterTab(i).Left
            FilterPanel(i).Width = RightFilterTab(i).Left + RightFilterTab(i).Width + 60
            
            chkLeftFilterSet(i).Width = TabWidth - 90
            chkRightFilterSet(i).Width = TabWidth - 90
            chkLeftFilterSet(i).Left = LeftFilterTab(i).Left + ((TabWidth - chkLeftFilterSet(i).Width) / 2)
            chkRightFilterSet(i).Left = RightFilterTab(i).Left + ((TabWidth - chkRightFilterSet(i).Width) / 2) - 15
            
            ButtonPanel(i).Left = -30
            ButtonPanel(i).Width = FilterPanel(i).Width + 30
            DrawBackGround ButtonPanel(i), 7, True, True
            
            NewLeft = FilterPanel(i).Left + FilterPanel(i).Width + 120
        End If
    Next
    InternalResize = True
    MyNewWidth = NewLeft + RightPanel.Width + 180
    NewLeft = Me.Left + MyOldWidth - MyNewWidth
    Me.Move NewLeft, Me.Top, MyNewWidth, Me.Height
    InternalResize = False
    Form_Resize
End Sub

Private Sub chkLeftFilterSet_Click(Index As Integer)
    Dim tmpBuff As String
    Dim LineNo As Long
    If chkLeftFilterSet(Index).Value = 1 Then
        LineNo = LeftFilterTab(Index).GetLineCount - 1
        If LineNo >= 0 Then
            tmpBuff = LeftFilterTab(Index).GetBuffer(0, LineNo, vbLf)
            RightFilterTab(Index).InsertBuffer tmpBuff, vbLf
            LeftFilterTab(Index).ResetContent
            LeftFilterTab(Index).Refresh
            RightFilterTab(Index).Sort "0", True, True
            RightFilterTab(Index).Refresh
            chkLeftFilterSet(Index).Caption = "0"
            chkRightFilterSet(Index).Caption = CStr(RightFilterTab(Index).GetLineCount)
        End If
        chkLeftFilterSet(Index).Value = 0
    Else
    End If
End Sub

Private Sub chkRightFilterSet_Click(Index As Integer)
    Dim tmpBuff As String
    Dim LineNo As Long
    If chkRightFilterSet(Index).Value = 1 Then
        LineNo = RightFilterTab(Index).GetLineCount - 1
        If LineNo >= 0 Then
            tmpBuff = RightFilterTab(Index).GetBuffer(0, LineNo, vbLf)
            LeftFilterTab(Index).InsertBuffer tmpBuff, vbLf
            RightFilterTab(Index).ResetContent
            RightFilterTab(Index).Refresh
            LeftFilterTab(Index).Sort "0", True, True
            LeftFilterTab(Index).Refresh
            chkRightFilterSet(Index).Caption = "0"
            chkLeftFilterSet(Index).Caption = CStr(LeftFilterTab(Index).GetLineCount)
        End If
        chkRightFilterSet(Index).Value = 0
    Else
    End If
End Sub

Private Sub chkToggle_Click()
    HandleToggleFilter chkToggle
End Sub

Private Sub chkWork_Click(Index As Integer)
    If chkWork(Index).Value = 1 Then
        chkWork(Index).BackColor = LightGreen
        CheckFilterTask Index, chkWork(Index), LeftFilterTab(Index)
        txtFilterVal(Index).Enabled = True
        txtFilterVal(Index).BackColor = vbWhite
        LeftFilterTab(Index).DisplayBackColor = LightYellow
        LeftFilterTab(Index).DisplayTextColor = vbBlack
        LeftFilterTab(Index).Enabled = True
        LeftFilterTab(Index).AutoSizeColumns
        LeftFilterTab(Index).Refresh
        RightFilterTab(Index).DisplayBackColor = NormalBlue
        RightFilterTab(Index).DisplayTextColor = vbWhite
        RightFilterTab(Index).Enabled = True
        RightFilterTab(Index).AutoSizeColumns
        RightFilterTab(Index).Refresh
        chkLeftFilterSet(Index).Enabled = True
        chkRightFilterSet(Index).Enabled = True
    Else
        chkWork(Index).BackColor = MyOwnButtonFace
        txtFilterVal(Index).BackColor = LightGray
        txtFilterVal(Index).Enabled = False
        LeftFilterTab(Index).DisplayBackColor = LightGray
        LeftFilterTab(Index).DisplayTextColor = vbBlack
        LeftFilterTab(Index).Refresh
        LeftFilterTab(Index).Enabled = False
        RightFilterTab(Index).DisplayBackColor = LightGray
        RightFilterTab(Index).DisplayTextColor = vbBlack
        RightFilterTab(Index).Refresh
        RightFilterTab(Index).Enabled = False
        chkLeftFilterSet(Index).Enabled = False
        chkRightFilterSet(Index).Enabled = False
    End If
End Sub

Private Function CheckFilterTask(Index As Integer, CurButton As CheckBox, CurTab As TABLib.Tab) As Boolean
    Dim NewData As Boolean
    Dim tmpTag As String
    Dim tmpLayout As String
    Dim tmpTimeFrame As String
    Dim AodbResult As String
    Dim PatchCodes As String
    Dim PatchValues As String
    Dim FilterList As String
    NewData = False
    If WorkArea.Visible Then
        tmpTag = Me.Tag
        tmpLayout = GetRealItem(tmpTag, 0, ",")
        Select Case tmpLayout
            Case "AODB_FILTER"
                HandleAodbFilter CurButton, False
                FilterList = MyGlobalResult
                tmpTimeFrame = txtVpfr(0).Tag & "/" & txtVpto(0).Tag
                If Len(tmpTimeFrame) = 17 Then
                    tmpTag = CurButton.Tag
                    If tmpTag <> FilterList Then
                        If RightFilterTab(Index).GetLineCount = 0 Then
                            CurTab.ResetContent
                            CurTab.Refresh
                            RightFilterTab(Index).ResetContent
                            RightFilterTab(Index).Refresh
                            chkRightFilterSet(Index).Caption = "0"
                            PatchCodes = GetRealItem(FilterList, 0, Chr(16))
                            PatchValues = GetRealItem(FilterList, 1, Chr(16))
                            AodbResult = HiddenData.GetFilterData(CurTab.myName, CurTab.LogicalFieldList, PatchCodes, PatchValues)
                            CurTab.InsertBuffer AodbResult, vbLf
                            CurButton.Tag = FilterList
                            CurTab.Sort "0", True, True
                            chkLeftFilterSet(Index).Caption = CStr(CurTab.GetLineCount)
                            NewData = True
                        End If
                    End If
                End If
            Case Else
        End Select
    End If
    CheckFilterTask = NewData
End Function
'Dim FilterItems(4) As String
'Dim CheckItems(4) As String
'Dim LastLoadFilter As String
'Dim MyFilterCaption As String
'
'Public Sub ArrangeFormLayout()
'    If FilterCalledAsDispo Then
'        fraFilterButtonPanel.Visible = False
'        fraDispoButtonPanel.Left = fraFilterButtonPanel.Left
'        fraDispoButtonPanel.Visible = True
'        fraFredButtonPanel.Visible = False
'        InitNatureCombo
'        Me.Caption = "Flight Nature Disposition"
'        Me.Left = MainDialog.Left + MainDialog.Width / 2 - Me.Width / 2
'        Me.Top = MainDialog.Top + MainDialog.Height / 2 - Me.Height / 2
'    Else
'        fraFilterButtonPanel.Visible = True
'        fraDispoButtonPanel.Visible = False
'        fraFredButtonPanel.Visible = True
'        Me.Caption = MyFilterCaption
'        Me.Top = FlightExtract.Top + FlightExtract.Height - FlightExtract.StatusBar.Height - Me.Height - 22 * Screen.TwipsPerPixelY
'        Me.Left = FlightExtract.Left + FlightExtract.Width / 2 - Me.Width / 2
'    End If
'End Sub
'
'Private Sub InitNatureCombo()
'    Static ComboFilled As Boolean
'    Dim LineNo As Long
'    Dim MaxLine As Long
'    Dim tmpTest As String
'    If Not ComboFilled Then
'        NatTabCombo(0).ResetContent
'        NatTabCombo(0).ShowVertScroller False
'        NatTabCombo(0).ShowHorzScroller False
'        NatTabCombo(0).VScrollMaster = False
'        NatTabCombo(0).FontName = "Courier New"
'        NatTabCombo(0).SetTabFontBold True
'        NatTabCombo(0).HeaderFontSize = 17
'        NatTabCombo(0).FontSize = 17
'        NatTabCombo(0).LineHeight = 19
'        NatTabCombo(0).GridLineColor = vbBlack
'        NatTabCombo(0).HeaderLengthString = Str(NatTabCombo(0).Width / Screen.TwipsPerPixelX)
'        NatTabCombo(0).HeaderString = "TYP"
'        NatTabCombo(0).ColumnAlignmentString = "L"
'        NatTabCombo(0).ColumnWidthString = "500"
'        NatTabCombo(0).Height = 1 * NatTabCombo(0).LineHeight * Screen.TwipsPerPixelY
'        NatTabCombo(0).MainHeaderOnly = True
'        NatTabCombo(0).InsertTextLine ",,", False
'        NatTabCombo(0).SetCurrentSelection 0
'        NatTabCombo(0).CreateComboObject "NatureCodes", 0, 2, "EDIT", 400
'        NatTabCombo(0).SetComboColumn "NatureCodes", 0
'        NatTabCombo(0).ComboMinWidth = 30
'        NatTabCombo(0).EnableInlineEdit True
'        NatTabCombo(0).NoFocusColumns = "0"
'        NatTabCombo(0).ComboSetColumnLengthString "NatureCodes", "32,350"
'        tmpTest = ",,"
'        NatTabCombo(0).ComboAddTextLines "NatureCodes", tmpTest, vbLf
'        NatTabCombo(0).Visible = True
'        NatTabCombo(0).RedrawTab
'        NatTabCombo(0).ComboResetContent "NatureCodes"
'        MaxLine = UfisServer.BasicData(1).GetLineCount - 1
'        For LineNo = 0 To MaxLine
'            tmpTest = UfisServer.BasicData(1).GetLineValues(LineNo)
'            NatTabCombo(0).ComboAddTextLines "NatureCodes", tmpTest, vbLf
'        Next
'        NatTabCombo(0).SetCurrentSelection 0
'        NatTabCombo(0).RedrawTab
'        ComboFilled = True
'    End If
'End Sub
'Private Sub InitFilterConfig()
'    Dim i As Integer
'    Dim KeyCode As String
'    Dim FilterCfg As String
'    Dim CurTab As Integer
'    Dim ItemList As String
'    For i = 0 To 4
'        If DataSystemType <> "FREE" Then
'            KeyCode = "FILTER" & Trim(Str(i + 1))
'            FilterCfg = GetIniEntry(myIniFullName, myIniSection, "", KeyCode, "")
'            If NewLayoutType Then FilterCfg = TranslateFilter(FilterCfg)
'        Else
'            FilterCfg = ""
'            Select Case i
'                Case 0  'ALC
'                    FilterCfg = MainDialog.BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(19), 2, 25, ",")
'                    FilterCfg = Replace(FilterCfg, ":,", ":", 1, -1, vbBinaryCompare)
'                    FilterCfg = Replace(FilterCfg, ",", ":", 1, -1, vbBinaryCompare)
'                Case 1  'FLNO
'                    FilterCfg = MainDialog.BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(20), 2, 25, ",")
'                    FilterCfg = Replace(FilterCfg, ":,", ":", 1, -1, vbBinaryCompare)
'                    FilterCfg = Replace(FilterCfg, ",:", ":", 1, -1, vbBinaryCompare)
'                    FilterCfg = Replace(FilterCfg, "+,", "+", 1, -1, vbBinaryCompare)
'                Case 2  'APC
'                    FilterCfg = MainDialog.BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(21), 2, 25, ",")
'                    FilterCfg = Replace(FilterCfg, ":,", ":", 1, -1, vbBinaryCompare)
'                    FilterCfg = Replace(FilterCfg, ",:", ":", 1, -1, vbBinaryCompare)
'                Case 3 'ACT
'                    FilterCfg = MainDialog.BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(22), 2, 25, ",")
'                    FilterCfg = Replace(FilterCfg, ":,", ":", 1, -1, vbBinaryCompare)
'                    FilterCfg = Replace(FilterCfg, ",", ":", 1, -1, vbBinaryCompare)
'                Case 4
'                Case Else
'            End Select
'        End If
'        chkWork(i).Caption = GetItem(FilterCfg, 1, ":")
'        CurTab = i * 2
'        FilterTab(CurTab).SetHeaderText GetItem(FilterCfg, 2, ":")
'        FilterTab(CurTab).Tag = GetItem(FilterCfg, 2, ":")
'        FilterTab(CurTab).ResetContent
'        FilterTab(CurTab + 1).SetHeaderText "Filter"
'        FilterTab(CurTab + 1).ResetContent
'        FilterTab(CurTab + 1).Tag = GetItem(FilterCfg, 2, ":")
'        'we must increase all coloumn numbers by 2
'        ItemList = GetItem(FilterCfg, 3, ":")
'        FilterItems(i) = ShiftItemValues(ItemList, 2)
'        ItemList = GetItem(FilterCfg, 4, ":")
'        If ItemList <> "" Then
'            CheckItems(i) = ShiftItemValues(ItemList, 2)
'        Else
'            CheckItems(i) = FilterItems(i)
'        End If
'    Next
'End Sub
'Private Function TranslateFilter(CurFilterCfg As String) As String
'    Dim Result As String
'    Dim CurItemList As String
'    Dim MainItems As String
'    Dim ViewItems As String
'    Result = ""
'    If CurFilterCfg <> "" Then
'        Result = Result & GetItem(CurFilterCfg, 1, ":") & ":"
'        Result = Result & GetItem(CurFilterCfg, 2, ":") & ":"
'        CurItemList = GetItem(CurFilterCfg, 3, ":")
'        MainItems = TranslateItemList(CurItemList, MainDialog.MainFieldList, False)
'        CurItemList = GetItem(CurFilterCfg, 4, ":")
'        If CurItemList = "" Then
'            ViewItems = MainItems
'        Else
'            ViewItems = TranslateItemList(CurItemList, myViewFieldList, False)
'        End If
'        Result = Result & MainItems & ":" & ViewItems
'    End If
'    TranslateFilter = Result
'End Function
'Private Function ShiftItemValues(ItemList As String, ShiftVal As Integer) As String
'    Dim Result As String
'    Dim CurItm As Integer
'    Dim ColItm As Integer
'    Dim ColNbr As Integer
'    Dim tmpItm As String
'    Dim tmpCol As String
'    Dim NewItm As String
'    Dim tmpLen As Integer
'    Result = ""
'    CurItm = 1
'    Do
'        'separate items
'        tmpItm = GetItem(ItemList, CurItm, ",")
'        If tmpItm <> "" Then
'            NewItm = ""
'            ColItm = 1
'            Do
'                'separate coloumns
'                tmpCol = GetItem(tmpItm, ColItm, "+")
'                If tmpCol <> "" Then
'                    ColNbr = Val(tmpCol) + ShiftVal
'                    NewItm = NewItm & Trim(Str(ColNbr)) & "+"
'                End If
'                ColItm = ColItm + 1
'            Loop While tmpCol <> ""
'            If ColItm > 2 Then NewItm = Left(NewItm, Len(NewItm) - 1)
'            Result = Result & NewItm & ","
'        End If
'        CurItm = CurItm + 1
'    Loop While tmpItm <> ""
'    If CurItm > 2 Then Result = Left(Result, Len(Result) - 1)
'    ShiftItemValues = Result
'End Function
'
'Private Sub chkClose_Click()
'    If chkClose.Value = 1 Then
'        Me.Hide
'        chkClose.Value = 0
'    End If
'End Sub
'
'Private Sub chkFilterSet_Click(Index As Integer)
'Dim FromCol As Integer
'Dim ToCol As Integer
'    If chkFilterSet(Index).Value = 1 Then
'        FromCol = Index
'        If FromCol Mod 2 = 0 Then
'            ToCol = FromCol + 1
'        Else
'            ToCol = FromCol - 1
'        End If
'        ShiftAllData FromCol, ToCol
'        chkFilterSet(Index).Value = 0
'    End If
'End Sub
'
'Private Sub ShiftAllData(FromCol As Integer, ToCol As Integer)
'    Dim ColTxt As String
'    MousePointer = 11
'    ColTxt = FilterTab(FromCol).SelectDistinct("0", "", "", vbLf, False)
'    FilterTab(ToCol).InsertBuffer ColTxt, vbLf
'    FilterTab(ToCol).Sort "0", True, True
'    FilterTab(ToCol).RedrawTab
'    FilterTab(FromCol).ResetContent
'    FilterTab(FromCol).RedrawTab
'    SetFilterCount FromCol
'    SetFilterCount ToCol
'    MousePointer = 0
'    CheckRecLoadFilter False
'End Sub
'Private Sub SetFilterCount(ColIdx As Integer)
'    Dim tmpCount As Long
'    Dim MaxLin As Long
'    tmpCount = FilterTab(ColIdx).GetLineCount
'    chkFilterSet(ColIdx).Caption = Trim(Str(tmpCount))
'    If FilterTab(ColIdx).GetVScrollPos = 0 Then
'        MaxLin = FilterTab(ColIdx).Height / (FilterTab(ColIdx).LineHeight * Screen.TwipsPerPixelY) - 1
'        If tmpCount > MaxLin Then
'            FilterTab(ColIdx).ShowVertScroller True
'        Else
'            FilterTab(ColIdx).ShowVertScroller False
'        End If
'    End If
'End Sub
'Private Sub ShiftData(FromCol As Integer, ToCol As Integer, ActSelLine As Long)
'Dim SelLine As Long
'Dim TgtLine As Long
'Dim Linedata As String
'Dim NewData As String
'Dim ItmDat As String
'    SelLine = ActSelLine
'    If SelLine < 0 Then SelLine = FilterTab(FromCol).GetCurrentSelected
'    If SelLine >= 0 Then
'        Linedata = FilterTab(FromCol).GetLineValues(SelLine)
'        ItmDat = GetItem(Linedata, 1, ",")
'        If ItmDat <> "" Then
'            InsertFilterValue Linedata, ToCol
'            FilterTab(FromCol).DeleteLine SelLine
'        End If
'        SetFilterCount FromCol
'        SetFilterCount ToCol
'        FilterTab(FromCol).RedrawTab
'        FilterTab(ToCol).RedrawTab
'    Else
'        MyMsgBox.InfoApi 0, "Click on the desired (valid) line and try it again.", "More info"
'        If MyMsgBox.CallAskUser(0, 0, 0, "Action Control", "No data selected", "hand", "", UserAnswer) >= 0 Then DoNothing
'    End If
'End Sub
'
'Private Sub chkSetDispo_Click()
'    If chkSetDispo.Value = 1 Then
'        chkSetDispo.BackColor = LightGreen
'        Screen.MousePointer = 11
'        PerformNatureDispo
'        chkSetDispo.Value = 0
'        Screen.MousePointer = 0
'    Else
'        chkSetDispo.BackColor = MyOwnButtonFace
'    End If
'End Sub
'Private Sub PerformNatureDispo()
'    Dim CurLine As Long
'    Dim MaxLine As Long
'    Dim TtypColFound As Boolean
'    Dim TtypColNo As Long
'    Dim NataColNo As Long
'    Dim NatdColNo As Long
'    Dim LookTwice As Boolean
'    Dim IsValidLine As Boolean
'    Dim tmpTtyp As String
'    Dim FredFilter As String
'    Dim tmpFldList As String
'    Dim tmpRec As String
'    Dim tmpFltRec As String
'    tmpTtyp = NatTabCombo(0).GetColumnValue(0, 0)
'    TtypColFound = False
'    TtypColNo = CLng(GetRealItemNo(MainDialog.MainFieldList, "TTYP") + 3)
'    If TtypColNo >= 3 Then
'        LookTwice = False
'        TtypColFound = True
'    Else
'        NataColNo = CLng(GetRealItemNo(MainDialog.MainFieldList, "NATA") + 3)
'        NatdColNo = CLng(GetRealItemNo(MainDialog.MainFieldList, "NATD") + 3)
'        If (NataColNo >= 3) And (NatdColNo >= 3) Then
'            LookTwice = True
'            TtypColFound = True
'        End If
'    End If
'    If (TtypColFound) And (tmpTtyp <> "") Then
'        FredFilter = chkWkDay(0).Tag
'        If FredFilter = "......." Then FredFilter = ""
'        MaxLine = MainDialog.FileData.GetLineCount - 1
'        For CurLine = 0 To MaxLine
'            tmpRec = MainDialog.FileData.GetLineValues(CurLine)
'            If Not LookTwice Then
'                IsValidLine = True
'                IsValidLine = CheckFilterKeys(tmpRec, IsValidLine)
'                'IsValidLine = MainDialog.CheckFlightPeriod(Spfr, Spto, RecData, IsValidLine, True, True, FredFilter)
'                If IsValidLine Then MainDialog.FileData.SetColumnValue CurLine, TtypColNo, tmpTtyp
'            Else
'                tmpFldList = MainDialog.MainFieldList
'                tmpFldList = Replace(tmpFldList, "FLCD", "....", 1, -1, vbBinaryCompare)
'                tmpFldList = Replace(tmpFldList, "FLTD", "....", 1, -1, vbBinaryCompare)
'                tmpFltRec = TranslateRecordData(tmpRec, MainDialog.MainFieldList, tmpFldList)
'                IsValidLine = True
'                IsValidLine = CheckFilterKeys(tmpFltRec, IsValidLine)
'                If IsValidLine Then MainDialog.FileData.SetColumnValue CurLine, NataColNo, tmpTtyp
'                tmpFldList = MainDialog.MainFieldList
'                tmpFldList = Replace(tmpFldList, "FLCA", "....", 1, -1, vbBinaryCompare)
'                tmpFldList = Replace(tmpFldList, "FLTA", "....", 1, -1, vbBinaryCompare)
'                tmpFltRec = TranslateRecordData(tmpRec, MainDialog.MainFieldList, tmpFldList)
'                IsValidLine = True
'                IsValidLine = CheckFilterKeys(tmpFltRec, IsValidLine)
'                If IsValidLine Then MainDialog.FileData.SetColumnValue CurLine, NatdColNo, tmpTtyp
'            End If
'        Next
'        MainDialog.FileData.Refresh
'    Else
'    End If
'End Sub
'Private Sub chkTool_Click(Index As Integer)
'    If chkTool(Index).Value = 0 Then chkTool(Index).BackColor = MyOwnButtonFace
'    Select Case Index
'        Case 0
'            'Close
'            If chkTool(Index).Value = 1 Then
'                Me.Tag = ""
'                Me.Hide
'                If Not FilterCalledAsDispo Then FlightExtract.Show
'                chkTool(Index).Value = 0
'            End If
'        Case 1
'            'Refresh
'            If chkTool(Index).Value = 1 Then
'                InitLists
'                chkTool(Index).Value = 0
'            End If
'        Case 2
'            'load
'            If chkTool(Index).Value = 1 Then
'                If Not FilterCalledAsDispo Then
'                    CheckRecLoadFilter True
'                    FlightExtract.chkWork(4).Value = 1
'                    If Me.Visible = True Then Me.Refresh
'                End If
'            End If
'        Case 3
'            'Adjust
'        Case 4
'            'Compare NOOP
'            If Not FilterCalledAsDispo Then
'                If chkTool(Index).Value = 1 Then
'                    FlightExtract.chkWork(13).Value = 1
'                Else
'                    FlightExtract.chkWork(13).Value = 0
'                End If
'            End If
'        Case Else
'    End Select
'    If chkTool(Index).Value = 1 Then chkTool(Index).BackColor = LightestGreen
'End Sub
'
'Private Sub BuildFilters()
'    Dim tmpType As String
'    Dim ColTxt As String
'    Dim TgtCol As Integer
'    Dim ItmNbr As Integer
'    Dim CurCol As String
'    Dim i As Integer
'    MousePointer = 11
'    For i = 0 To 4
'        UfisTools.BlackBox(0).ResetContent
'        UfisTools.BlackBox(0).ColumnWidthString = "100"
'        TgtCol = i * 2
'        FilterTab(TgtCol).ResetContent
'        FilterTab(TgtCol + 1).ResetContent
'        ItmNbr = 1
'        Do
'            CurCol = GetItem(FilterItems(i), ItmNbr, ",")
'            If CurCol <> "" Then
'                CurCol = Replace(CurCol, "+", ",", 1, -1, vbBinaryCompare)
'                ColTxt = MainDialog.FileData.SelectDistinct(CurCol, "", "", vbLf, True)
'                UfisTools.BlackBox(0).InsertBuffer ColTxt, vbLf
'            End If
'            ItmNbr = ItmNbr + 1
'        Loop While CurCol <> ""
'        ColTxt = UfisTools.BlackBox(0).SelectDistinct("0", "", "", vbLf, True)
'        UfisTools.BlackBox(0).ResetContent
'        ColTxt = Replace(ColTxt, (vbLf & vbLf), vbLf, 1, -1, vbBinaryCompare)
'        If Left(ColTxt, 1) = vbLf Then ColTxt = Mid(ColTxt, 2)
'        If Right(ColTxt, 1) = vbLf Then
'            ColTxt = Left(ColTxt, Len(ColTxt) - 1)
'        End If
'        tmpType = FilterTab(TgtCol).Tag
'        Select Case tmpType
'            Case "ROUTE"
'                ExtractMultiItems ColTxt
'            Case Else
'        End Select
'        FilterTab(TgtCol).InsertBuffer ColTxt, vbLf
'        FilterTab(TgtCol).Sort "0", True, True
'        FilterTab(TgtCol).RedrawTab
'        SetFilterCount TgtCol
'    Next
'    MousePointer = 0
'End Sub
'Private Sub ExtractMultiItems(ItemList As String)
'    Dim ItemText As String
'    Dim CurLine As Long
'    Dim MaxLine As Long
'    CrossFilter(0).ResetContent
'    CrossFilter(1).ResetContent
'    CrossFilter(0).ColumnWidthString = "100"
'    CrossFilter(1).ColumnWidthString = "100"
'    CrossFilter(1).InsertBuffer ItemList, vbLf
'    MaxLine = CrossFilter(1).GetLineCount - 1
'    For CurLine = 0 To MaxLine
'        ItemText = CrossFilter(1).GetColumnValue(CurLine, 0)
'        CrossFilter(0).InsertBuffer ItemText, "|"
'    Next
'    If InStr(ItemList, ":") > 0 Then
'        MaxLine = CrossFilter(0).GetLineCount - 1
'        For CurLine = 0 To MaxLine
'            ItemText = CrossFilter(0).GetColumnValue(CurLine, 0)
'            ItemText = GetItem(ItemText, 1, ":")
'            CrossFilter(0).SetColumnValue CurLine, 0, ItemText
'        Next
'    End If
'    ItemList = CrossFilter(0).SelectDistinct("0", "", "", vbLf, True)
'    ItemList = Replace(ItemList, (vbLf & vbLf), vbLf, 1, -1, vbBinaryCompare)
'    If Left(ItemList, 1) = vbLf Then ItemList = Mid(ItemList, 2)
'    If Right(ItemList, 1) = vbLf Then ItemList = Left(ItemList, Len(ItemList) - 1)
'End Sub
'
'Private Sub chkWkDay_Click(Index As Integer)
'    Dim i As Integer
'    Dim tmpTag As String
'    If chkWkDay(Index).Value = 1 Then chkWkDay(Index).BackColor = LightGreen Else chkWkDay(Index).BackColor = MyOwnButtonFace
'    Select Case Index
'        Case 8
'            If chkWkDay(Index).Value = 1 Then
'                For i = 1 To 7
'                    chkWkDay(i).Value = 1
'                Next
'                chkWkDay(8).Value = 0
'            End If
'        Case 0
'            If chkWkDay(Index).Value = 1 Then
'                For i = 1 To 7
'                    chkWkDay(i).Value = 0
'                Next
'                chkWkDay(Index).Tag = "......."
'                chkWkDay(0).Value = 0
'            End If
'        Case Else
'            tmpTag = chkWkDay(0).Tag
'            If chkWkDay(Index).Value = 1 Then
'                Mid(tmpTag, Index, 1) = Trim(Str(Index))
'                chkWkDay(0).Tag = tmpTag
'            Else
'                Mid(tmpTag, Index, 1) = "."
'                chkWkDay(0).Tag = tmpTag
'                If tmpTag = "......." Then chkWkDay(0).Value = 1
'            End If
'    End Select
'End Sub
'
'Private Sub chkWork_Click(Index As Integer)
'Dim FirstButton As Integer
'Dim LastButton As Integer
'Dim CurButton As Integer
'Dim ColIdx As Integer
'    If chkWork(Index).Value = 0 Then chkWork(Index).BackColor = MyOwnButtonFace
'    If chkWork(Index).Value = 1 Then
'        FilterPanel(Index).Enabled = True
'    Else
'        FilterPanel(Index).Enabled = False
'    End If
'    FirstButton = Index * 2
'    LastButton = FirstButton + 1
'    For CurButton = FirstButton To LastButton
'        chkFilterSet(CurButton).Enabled = FilterPanel(Index).Enabled
'        If FilterPanel(Index).Enabled Then
'            If CurButton Mod 2 = 0 Then
'                FilterTab(CurButton).DisplayBackColor = LightYellow
'                FilterTab(CurButton).DisplayTextColor = vbBlack
'                FilterTab(CurButton).SelectBackColor = DarkestYellow
'                FilterTab(CurButton).SelectTextColor = vbWhite
'                txtFilterVal(CurButton).BackColor = vbWhite
'            Else
'                FilterTab(CurButton).DisplayBackColor = NormalBlue
'                FilterTab(CurButton).DisplayTextColor = vbWhite
'                FilterTab(CurButton).SelectBackColor = DarkBlue
'                FilterTab(CurButton).SelectTextColor = vbWhite
'            End If
'        Else
'            FilterTab(CurButton).DisplayBackColor = LightGray
'            FilterTab(CurButton).DisplayTextColor = vbBlack
'            FilterTab(CurButton).SelectBackColor = LightGray
'            FilterTab(CurButton).SelectTextColor = vbBlack
'            If CurButton Mod 2 = 0 Then txtFilterVal(CurButton).BackColor = LightGray
'        End If
'        FilterTab(CurButton).RedrawTab
'    Next
'    CheckRecLoadFilter False
'    If chkWork(Index).Value = 1 Then chkWork(Index).BackColor = LightestGreen
'End Sub
'
'Private Sub FilterTab_GotFocus(Index As Integer)
'    FilterTab(Index).SelectTextColor = vbWhite
'    FilterTab(Index).RedrawTab
'    StatusBar.Panels(5).Text = FilterTab(Index).Tag
'End Sub
'Private Sub FilterTab_LostFocus(Index As Integer)
'    FilterTab(Index).SelectTextColor = NormalGray
'    FilterTab(Index).RedrawTab
'    StatusBar.Panels(5).Text = ""
'End Sub
'
'Private Sub FilterTab_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
'    If Trim(FilterTab(Index).GetColumnValue(LineNo, 0)) <> "" Then
'        SetStatusInfo Index, FilterTab(Index).GetColumnValue(LineNo, 0), "HighLighted"
'    End If
'End Sub
'
'Private Sub FilterTab_SendLButtonClick(Index As Integer, ByVal Line As Long, ByVal Col As Long)
'    If Line < 0 Then
'        FilterTab(Index).SetFocus
'    Else
'        'ToggleFilterValues Index, Line, Col
'    End If
'End Sub
'
'Private Sub FilterTab_SendLButtonDblClick(Index As Integer, ByVal Line As Long, ByVal Col As Long)
'    If Line < 0 Then
'        FilterTab(Index).SetFocus
'    Else
'        ToggleFilterValues Index, Line, Col
'    End If
'End Sub
'
'Private Sub ToggleFilterValues(Index As Integer, Line As Long, Col As Long)
'    Dim FromCol As Integer
'    Dim ToCol As Integer
'    Dim SelCol As Integer
'    If Line >= 0 Then
'        If Trim(FilterTab(Index).GetColumnValue(Line, 0)) <> "" Then
'            FromCol = Index
'            If Index Mod 2 = 0 Then
'                ToCol = FromCol + 1
'                SelCol = ToCol
'            Else
'                ToCol = FromCol - 1
'                SelCol = FromCol
'            End If
'            SetStatusInfo Index, FilterTab(Index).GetColumnValue(Line, 0), "Moved"
'            ShiftData FromCol, ToCol, Line
'            FilterTab(Index).SetCurrentSelection Line
'            FilterTab(ToCol).RedrawTab
'            Me.Refresh
'            If FromCol = 0 Then
'                If FilterTab(ToCol).GetLineCount = 1 Then
'                    'chkTool(2).Value = 1
'                End If
'            ElseIf FromCol = 1 Then
'                If FilterTab(FromCol).GetLineCount = 0 Then
'                    If Not FilterCalledAsDispo Then FlightExtract.ResetList
'                End If
'                If FilterTab(FromCol).GetLineCount = 1 Then
'                    'chkTool(2).Value = 1
'                End If
'            End If
'            CheckRecLoadFilter False
'        End If
'    End If
'End Sub
'
'Private Sub SetStatusInfo(ColIdx As Integer, ColTxt As String, ActTxt As String)
'    Dim tmpTxt As String
'    Dim tmpWork As Integer
'    tmpWork = ((ColIdx + 2) \ 2) - 1
'    StatusBar.Panels(1).Text = chkWork(tmpWork).Caption
'    StatusBar.Panels(2).Text = FilterTab(ColIdx).GetHeaderText
'    StatusBar.Panels(3).Text = Replace(ColTxt, ",", "", 1, -1, vbBinaryCompare) & ": "
'    StatusBar.Panels(4).Text = ActTxt
'End Sub
'
'Private Sub FilterTab_SendRButtonClick(Index As Integer, ByVal Line As Long, ByVal Col As Long)
'    Dim tmpText As String
'    Dim tmpType As String
'    tmpText = Trim(FilterTab(Index).GetColumnValue(Line, Col))
'    tmpType = FilterTab(Index).Tag
'    BasicDetails tmpType, tmpText
'End Sub
'
'Public Sub BasicDetails(UseType As String, tmpText As String)
'    Dim tmpSqlKey As String
'    Dim tmpTable As String
'    Dim tmpFields As String
'    Dim RetVal As Integer
'    Dim CurItm As Integer
'    Dim tmpFldNam As String
'    Dim tmpFldVal As String
'    Dim MsgTxt As String
'    Dim tmpFlno As String
'    Dim tmpFlca As String
'    Dim tmpFltn As String
'    Dim tmpFlns As String
'    Dim ReqButtons As String
'
'    MousePointer = 11
'    If tmpText <> "" Then
'        ReqButtons = ""
'        tmpTable = ""
'        Select Case UseType
'            Case "ALC"
'                tmpTable = "ALTTAB"
'                tmpFields = "ALC2,ALC3,ALFN,CTRY,CASH,ADD1,ADD2,ADD3,ADD4,PHON,TFAX,SITA,TELX,WEBS,BASE"
'                If Len(tmpText) = 2 Then
'                    tmpSqlKey = "WHERE ALC2='" & tmpText & "'"
'                End If
'                If Len(tmpText) = 3 Then
'                    tmpSqlKey = "WHERE ALC3='" & tmpText & "'"
'                End If
'                'ReqButtons = "OK,Flights"
'            Case "FLNO"
'                If Not FilterCalledAsDispo Then
'                    tmpFlno = StripAftFlno(tmpText, tmpFlca, tmpFltn, tmpFlns)
'                    ShowFlights.Show , MainDialog
'                    ShowFlights.LoadFlightData FlightExtract.txtVpfr(3).Tag, FlightExtract.txtVpto(3).Tag, tmpFlca, tmpFltn, tmpFlns
'                End If
'            Case "APT"
'                tmpTable = "APTTAB"
'                tmpFields = "APC3,APC4,APSN,APFN,APN2,APN3,APN4,LAND,APTT"
'                If Len(tmpText) = 3 Then
'                    tmpSqlKey = "WHERE APC3='" & tmpText & "'"
'                End If
'                If Len(tmpText) = 4 Then
'                    tmpSqlKey = "WHERE APC4='" & tmpText & "'"
'                End If
'            Case "ACT"
'                tmpTable = "ACTTAB"
'                tmpFields = "ACT3,ACT5,ACTI,ACFN,SEAF,SEAB,SEAE,SEAT,ENTY"
'                If Len(tmpText) = 3 Then
'                    tmpSqlKey = "WHERE ACT3='" & tmpText & "'"
'                Else
'                    tmpSqlKey = "WHERE ACT5='" & tmpText & "'"
'                End If
'            Case Else
'        End Select
'        If tmpTable <> "" Then
'            RetVal = UfisServer.CallCeda(UserAnswer, "RT", tmpTable, tmpFields, "", tmpSqlKey, "", 0, True, False)
'            MsgTxt = ""
'            CurItm = 0
'            Do
'                CurItm = CurItm + 1
'                tmpFldNam = GetItem(tmpFields, CurItm, ",")
'                If tmpFldNam <> "" Then
'                    tmpFldVal = GetItem(UserAnswer, CurItm, ",")
'                    If tmpFldVal <> "" Then
'                        MsgTxt = MsgTxt & tmpFldNam & ": " & tmpFldVal & vbNewLine
'                    End If
'                End If
'            Loop While tmpFldNam <> ""
'            If MsgTxt <> "" Then
'                MsgTxt = Left(MsgTxt, Len(MsgTxt) - 2)
'            Else
'                MsgTxt = "'" & tmpText & "' is not in your basic data."
'            End If
'            If MyMsgBox.CallAskUser(0, 0, 0, "Basic Data Information", MsgTxt, "infomsg", ReqButtons, UserAnswer) = 2 Then
'                Me.Refresh
'                UserAnswer = Replace(UserAnswer, "&", "", 1, -1, vbBinaryCompare)
'                Select Case UserAnswer
'                    Case "Flights"
'                        If Not FilterCalledAsDispo Then
'                            ShowFlights.Show , MainDialog
'                            ShowFlights.LoadFlightData FlightExtract.txtVpfr(3).Tag, FlightExtract.txtVpto(3).Tag, tmpText, tmpFltn, tmpFlns
'                        End If
'                    Case Else
'                End Select
'            End If
'        End If
'    End If
'    MousePointer = 0
'End Sub
'
'Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = vbKeyF1 Then DisplayHtmHelpFile "", Me.Name, Me.ActiveControl.Name
'End Sub
'
'
'Private Sub InitLists()
'Dim myHeaderList As String
'Dim HdrLenLst As String
'Dim i As Integer
'Dim j As Integer
'Dim tmpLen As Integer
'    For i = 0 To 9
'        FilterTab(i).ShowHorzScroller False
'        FilterTab(i).ResetContent
'        FilterTab(i).FontName = "Courier New"
'        FilterTab(i).SetTabFontBold True
'        FilterTab(i).HeaderFontSize = 17
'        FilterTab(i).FontSize = 17
'        FilterTab(i).LineHeight = 18
'        FilterTab(i).HeaderLengthString = "200"
'        FilterTab(i).HeaderString = " "
'        FilterTab(i).MainHeaderOnly = True
'        FilterTab(i).Tag = ""
'        If i Mod 2 = 0 Then
'            FilterTab(i).DisplayBackColor = LightYellow
'            FilterTab(i).DisplayTextColor = vbBlack
'            FilterTab(i).SelectBackColor = DarkestYellow
'            FilterTab(i).SelectTextColor = vbBlack
'        Else
'            FilterTab(i).DisplayBackColor = NormalBlue
'            FilterTab(i).DisplayTextColor = vbWhite
'            FilterTab(i).SelectBackColor = DarkestBlue
'            FilterTab(i).SelectTextColor = vbWhite
'        End If
'        FilterTab(i).EmptyAreaBackColor = LightGray
'        FilterTab(i).GridLineColor = DarkGray
'    Next
'    Me.Refresh
'    InitFilterConfig
'    BuildFilters
'    For i = 0 To 4
'        chkWork(i).Value = 1
'        If Val(chkFilterSet(i * 2).Caption) = 0 Then chkWork(i).Value = 0
'    Next
'    If UpdateMode Then
'        For i = 2 To 4
'            chkWork(i).Value = 0
'            chkWork(i).Enabled = False
'        Next
'    End If
'End Sub
'
'Public Function CheckFilterKeys(RecData As String, IsValidLine As Boolean) As Boolean
'    Dim RetVal As Boolean
'    Dim i As Integer
'    Dim CurTab As Integer
'    Dim CurColLst As String
'    Dim tmpType As String
'    Dim tmpWidthList As String
'    Dim ItmNbr As Integer
'    Dim ColNbr As Integer
'    Dim tmpCol As String
'    Dim ColDat As String
'    Dim ChkVal As String
'    Dim HitLst As String
'    Dim ColLen As Integer
'    Dim CurCol As Integer
'    Dim SelCnt As Integer
'    Dim HitCnt As Integer
'    RetVal = IsValidLine
'    If RetVal = True Then
'        If FilterCalledAsDispo Then
'            tmpWidthList = MainDialog.FileData.ColumnWidthString
'        Else
'            tmpWidthList = FlightExtract.ExtractData.ColumnWidthString
'        End If
'        SelCnt = 0
'        HitCnt = 0
'        i = 0
'        Do
'            If FilterPanel(i).Enabled = True Then
'                If chkWork(i).Value = 1 Then
'                    CurTab = i * 2 + 1
'                    If FilterTab(CurTab).GetLineCount > 0 Then
'                        tmpType = FilterTab(CurTab).Tag
'                        SelCnt = SelCnt + 1
'                        'Run through Activated FilterValues
'                        ItmNbr = 1
'                        Do
'                            CurColLst = GetItem(CheckItems(i), ItmNbr, ",")
'                            If CurColLst <> "" Then
'                                CurColLst = Replace(CurColLst, "+", ",", 1, -1, vbBinaryCompare)
'                                ChkVal = ""
'                                ColNbr = 1
'                                Do
'                                    tmpCol = GetItem(CurColLst, ColNbr, ",")
'                                    If tmpCol <> "" Then
'                                        CurCol = Val(tmpCol) + 1
'                                        ColLen = Val(GetItem(tmpWidthList, CurCol, ","))
'                                        ColDat = GetItem(RecData, CurCol, ",")
'                                        ChkVal = ChkVal & Left(ColDat & Space(ColLen), ColLen)
'                                    End If
'                                    ColNbr = ColNbr + 1
'                                Loop While tmpCol <> ""
'                                ChkVal = RTrim(ChkVal)
'                                If ChkVal <> "" Then
'                                    Select Case tmpType
'                                        Case "ROUTE"
'                                            HitLst = CheckCrossFilter(CurTab, ChkVal)
'                                        Case Else
'                                            HitLst = FilterTab(CurTab).GetLinesByColumnValue(0, ChkVal, 0)
'                                    End Select
'                                    If HitLst <> "" Then HitCnt = HitCnt + 1
'                                End If
'                            End If
'                            ItmNbr = ItmNbr + 1
'                        Loop While (CurColLst <> "") And (HitCnt < SelCnt)
'                    End If
'                End If
'            End If
'            i = i + 1
'        Loop While (i <= 4) And (SelCnt = HitCnt)
'        If SelCnt <> HitCnt Then RetVal = False
'    End If
'    CheckFilterKeys = RetVal
'End Function
'Private Function CheckCrossFilter(CurTab As Integer, ItemList As String) As String
'    Dim CurLine As Long
'    Dim MaxLine As Long
'    Dim ChkVal As String
'    Dim HitList As String
'    CrossFilter(0).ResetContent
'    CrossFilter(0).InsertBuffer ItemList, "|"
'    HitList = ""
'    CurLine = 0
'    MaxLine = CrossFilter(0).GetLineCount - 1
'    While (HitList = "") And (CurLine <= MaxLine)
'        ChkVal = CrossFilter(0).GetColumnValue(CurLine, 0)
'        ChkVal = GetItem(ChkVal, 1, ":")
'        HitList = FilterTab(CurTab).GetLinesByColumnValue(0, ChkVal, 0)
'        CurLine = CurLine + 1
'    Wend
'    CheckCrossFilter = HitList
'End Function
'Public Sub InsertFilterValue(FilterData As String, ColIdx As Integer)
'Dim MaxLin As Long
'Dim CurLin As Long
'Dim FndLin As Long
'Dim tmpData As String
'    MaxLin = FilterTab(ColIdx).GetLineCount - 1
'    FndLin = -1
'    CurLin = 0
'    While (CurLin <= MaxLin) And (FndLin < 0)
'        tmpData = FilterTab(ColIdx).GetLineValues(CurLin)
'        If FilterData <= tmpData Then FndLin = CurLin
'        CurLin = CurLin + 1
'    Wend
'    If FndLin < 0 Then
'        If CurLin > MaxLin Then FndLin = CurLin Else FndLin = 0
'    End If
'    FilterTab(ColIdx).InsertTextLineAt FndLin, FilterData, True
'End Sub
'
Private Sub Form_Load()
    SetButtonFaceStyle Me
    WorkArea.Top = 0
    WorkArea.Left = 0
    lblFullSize(0).Top = chkFullSize.Top + 15
    lblFullSize(0).Left = chkFullSize.Left + chkFullSize.Width + 15
    lblFullSize(1).Left = chkFullSize.Left + chkFullSize.Width + 30
    lblFullSize(1).Top = lblFullSize(0).Top + 15
    lblToggle(0).Top = chkToggle.Top + 15
    lblToggle(0).Left = chkToggle.Left + chkToggle.Width + 15
    lblToggle(1).Left = chkToggle.Left + chkToggle.Width + 30
    lblToggle(1).Top = lblToggle(0).Top + 15
    StatusBar.ZOrder
End Sub
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Me.Tag = ""
        Me.Hide
        Cancel = True
    End If
End Sub
'
Private Sub Form_Resize()
    Dim i As Integer
    Dim NewHeight As Long
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim MaxLin As Long
    If Not InternalResize Then
        WorkArea.Visible = False
        TopPanel.Width = Me.ScaleWidth - RightPanel.Width
        If RightPanel.Visible Then
            WorkArea.Width = Me.ScaleWidth - RightPanel.Width
        Else
            WorkArea.Width = Me.ScaleWidth
        End If
        NewHeight = Me.ScaleHeight - StatusBar.Height - WorkArea.Top
        If NewHeight > 1200 Then
            WorkArea.Height = NewHeight
            NewHeight = NewHeight - 300
            NewTop = NewHeight - ButtonPanel(0).Height - 30
            For i = 0 To FilterPanel.UBound
                FilterPanel(i).Height = NewHeight
                ButtonPanel(i).Top = NewTop
            Next
            NewHeight = ButtonPanel(0).Top - LeftFilterTab(0).Top - 15
            For i = 0 To LeftFilterTab.UBound
                LeftFilterTab(i).Height = NewHeight
                RightFilterTab(i).Height = NewHeight
            Next
        End If
        DrawBackGround WorkArea, 7, True, True
        WorkArea.Visible = True
        NewLeft = 0
        For i = FilterPanel.UBound To 0 Step -1
            If FilterPanel(i).Visible Then
                NewLeft = FilterPanel(i).Left + (FilterPanel(i).Width / 2)
                Exit For
            End If
        Next
        If WorkArea.Width >= NewLeft Then
            FilterScroll.Visible = False
        Else
            FilterScroll.Visible = True
            FilterScroll.ZOrder
        End If
    End If
End Sub

Private Sub lblFilterName_Click(Index As Integer)
    If chkFilterIsVisible(Index).Value = 0 Then chkFilterIsVisible(Index).Value = 1 Else chkFilterIsVisible(Index).Value = 0
End Sub

Private Sub lblFullSize_Click(Index As Integer)
    If chkFullSize.Value = 0 Then chkFullSize.Value = 1 Else chkFullSize.Value = 0
End Sub

Private Sub lblToggle_Click(Index As Integer)
    If chkToggle.Value = 0 Then chkToggle.Value = 1 Else chkToggle.Value = 0
End Sub

Private Sub LeftFilterTab_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim tmpBuff As String
    If LineNo >= 0 Then
        tmpBuff = LeftFilterTab(Index).GetLineValues(LineNo)
        RightFilterTab(Index).InsertTextLine tmpBuff, False
        LeftFilterTab(Index).DeleteLine LineNo
        RightFilterTab(Index).Sort "0", True, True
        LeftFilterTab(Index).Refresh
        RightFilterTab(Index).Refresh
        chkLeftFilterSet(Index).Caption = CStr(LeftFilterTab(Index).GetLineCount)
        chkRightFilterSet(Index).Caption = CStr(RightFilterTab(Index).GetLineCount)
        CheckRefreshSettings
    End If
End Sub

Private Sub RightFilterTab_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim tmpBuff As String
    If LineNo >= 0 Then
        tmpBuff = RightFilterTab(Index).GetLineValues(LineNo)
        LeftFilterTab(Index).InsertTextLine tmpBuff, False
        RightFilterTab(Index).DeleteLine LineNo
        LeftFilterTab(Index).Sort "0", True, True
        LeftFilterTab(Index).Refresh
        RightFilterTab(Index).Refresh
        chkLeftFilterSet(Index).Caption = CStr(LeftFilterTab(Index).GetLineCount)
        chkRightFilterSet(Index).Caption = CStr(RightFilterTab(Index).GetLineCount)
        CheckRefreshSettings
    End If
End Sub

Private Sub txtFilterVal_Change(Index As Integer)
    SearchInList Index, 1
End Sub

Private Sub txtFilterVal_KeyPress(Index As Integer, KeyAscii As Integer)
    Dim LineNo As Long
    Dim HitText As String
    If KeyAscii = 13 Then
        LineNo = LeftFilterTab(Index).GetCurrentSelected
        If LineNo >= 0 Then
            HitText = LeftFilterTab(Index).GetColumnValue(LineNo, 0)
            If InStr(HitText, txtFilterVal(Index).Text) > 0 Then
                LeftFilterTab_SendLButtonDblClick Index, LineNo, 0
            End If
        End If
        KeyAscii = 0
    Else
        KeyAscii = Asc(UCase(Chr(KeyAscii)))
    End If
End Sub

'Private Sub CheckRecLoadFilter(ForLoad As Boolean)
'    Dim Result As String
'    Dim i As Integer
'    Dim MaxLine As Long
'    Result = ""
'    For i = 1 To 9 Step 2
'        If chkFilterSet(i).Enabled = True Then
'            MaxLine = FilterTab(i).GetLineCount - 1
'            If MaxLine >= 0 Then
'                Result = Result & FilterTab(i).GetBuffer(0, MaxLine, ",")
'            End If
'        End If
'    Next
'    If ForLoad = True Then LastLoadFilter = Result
'    If LastLoadFilter = Result Then
'        chkTool(2).ForeColor = vbBlack
'        chkTool(2).BackColor = MyOwnButtonFace
'        chkTool(4).Enabled = True
'        chkTool(2).Tag = "OK"
'    Else
'        chkTool(2).ForeColor = vbWhite
'        chkTool(2).BackColor = vbRed
'        chkTool(4).Enabled = False
'        chkTool(2).Tag = "NOTOK"
'    End If
'    If Not FilterCalledAsDispo Then
'        If Not ForLoad Then FlightExtract.CheckLoadFilter False
'    End If
'End Sub
'
'Public Function CheckFullAirlineFilter() As Boolean
'    Dim Result As Boolean
'    Dim i As Integer
'    Dim FilterCnt As Integer
'    Result = True
'    FilterCnt = 0
'    For i = 3 To 9 Step 2
'        If chkFilterSet(i).Enabled = True Then FilterCnt = FilterCnt + Val(chkFilterSet(i).Caption)
'    Next
'    If FilterCnt > 0 Then Result = False
'    CheckFullAirlineFilter = Result
'End Function
'
'Public Sub PrepareNoopFlights(SetEnable As Boolean)
'    Dim i As Integer
'    For i = 1 To 4
'        If SetEnable = True Then chkWork(i) = 1 Else chkWork(i) = 0
'        chkWork(i).Enabled = SetEnable
'    Next
'    If UpdateMode Then
'        For i = 2 To 4
'            chkWork(i).Value = 0
'            chkWork(i).Enabled = False
'        Next
'    End If
'    If SetEnable = True Then
'        Caption = "Record Filter Preset" & " (" & MySetUp.CurFileInfo(5).Caption & " Mode) " & GetItem(MySetUp.MyFileInfo.Caption, 1, " ")
'    Else
'        Caption = "Airline Compare Flights Filter" & " (" & MySetUp.CurFileInfo(5).Caption & " Mode) " & GetItem(MySetUp.MyFileInfo.Caption, 1, " ")
'    End If
'End Sub
'
'Private Sub txtFilterVal_Change(Index As Integer)
'    If Index = 2 Then
'        SearchInList Index, 1
'    Else
'        SearchInList Index, 2
'    End If
'End Sub
'
'Private Sub txtFilterVal_KeyPress(Index As Integer, KeyAscii As Integer)
'    Dim LineNo As Long
'    Dim HitText As String
'    If KeyAscii = 13 Then
'        LineNo = FilterTab(Index).GetCurrentSelected
'        If LineNo >= 0 Then
'            HitText = FilterTab(Index).GetColumnValue(LineNo, 0)
'            If InStr(HitText, txtFilterVal(Index).Text) > 0 Then
'                FilterTab_SendLButtonDblClick Index, LineNo, 0
'                chkTool(2).Value = 1
'            End If
'        End If
'        KeyAscii = 0
'    Else
'        KeyAscii = Asc(UCase(Chr(KeyAscii)))
'    End If
'End Sub
'
'Private Sub SearchInList(UseIndex As Integer, Method As Integer)
'    Static LastIndex As Integer
'    Dim HitLst As String
'    Dim HitRow As Long
'    Dim NxtRow As Long
'    Dim NewScroll As Long
'    Dim LoopCount As Integer
'    Dim tmpText As String
'    Dim ColNbr As String
'    Dim Index As Integer
'    Dim UseCol As Long
'    Index = UseIndex
'    If Index < 0 Then Index = LastIndex
'    LastIndex = Index
'    tmpText = Trim(txtFilterVal(Index).Text)
'    UseCol = 0
'    If tmpText <> "" Then
'        LoopCount = 0
'        Do
'            HitLst = FilterTab(Index).GetNextLineByColumnValue(UseCol, tmpText, Method)
'            HitRow = Val(HitLst)
'            If HitLst <> "" Then
'                If HitRow > 5 Then NewScroll = HitRow - 5 Else NewScroll = 0
'                FilterTab(Index).OnVScrollTo NewScroll
'            Else
'                FilterTab(Index).SetCurrentSelection -1
'                FilterTab(Index).OnVScrollTo 0
'            End If
'            LoopCount = LoopCount + 1
'        Loop While LoopCount < 2 And HitLst = ""
'        FilterTab(Index).SetCurrentSelection HitRow
'    End If
'    txtFilterVal(Index).SetFocus
'End Sub
'
'
Private Sub txtVpfr_Change(Index As Integer)
    Dim tmpCedaDate As String
    Dim i As Integer
    tmpCedaDate = ""
    tmpCedaDate = tmpCedaDate & Trim(txtVpfr(0).Text)
    tmpCedaDate = tmpCedaDate & Trim(txtVpfr(1).Text)
    tmpCedaDate = tmpCedaDate & Trim(txtVpfr(2).Text)
    If CheckValidDate(tmpCedaDate) = True Then
        txtVpfr(0).Tag = tmpCedaDate
        For i = 0 To 2
            txtVpfr(i).BackColor = vbYellow
            txtVpfr(i).ForeColor = vbBlack
        Next
        AdjustFilterButtons True
    Else
        txtVpfr(0).Tag = ""
        txtVpfr(Index).BackColor = vbRed
        txtVpfr(Index).ForeColor = vbWhite
        AdjustFilterButtons False
    End If
End Sub
Private Sub txtVpfr_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Private Sub txtVpto_Change(Index As Integer)
    Dim tmpCedaDate As String
    Dim i As Integer
    tmpCedaDate = ""
    tmpCedaDate = tmpCedaDate & Trim(txtVpto(0).Text)
    tmpCedaDate = tmpCedaDate & Trim(txtVpto(1).Text)
    tmpCedaDate = tmpCedaDate & Trim(txtVpto(2).Text)
    If CheckValidDate(tmpCedaDate) = True Then
        txtVpto(0).Tag = tmpCedaDate
        For i = 0 To 2
            txtVpto(i).BackColor = vbYellow
            txtVpto(i).ForeColor = vbBlack
        Next
        AdjustFilterButtons True
    Else
        txtVpto(0).Tag = ""
        txtVpto(Index).BackColor = vbRed
        txtVpto(Index).ForeColor = vbWhite
        AdjustFilterButtons False
    End If
End Sub

Private Sub txtVpto_GotFocus(Index As Integer)
    SetTextSelected
End Sub
Private Sub AdjustFilterButtons(SetEnable As Boolean)
    Dim i As Integer
    For i = 0 To FilterPanel.UBound
        If FilterPanel(i).Visible Then
            chkWork(i).Enabled = SetEnable
        End If
    Next
    AdjustApplButton "AODB_FILTER", SetEnable
    AdjustApplButton "FILE_FILTER", SetEnable
    AdjustApplButton "TOGGLE", SetEnable
    If SetEnable = True Then
        CheckRefreshSettings
    Else
        AdjustApplButton "AODBREFRESH", SetEnable
        AdjustApplButton "FILEREFRESH", SetEnable
    End If
End Sub
Private Sub AdjustApplButton(KeyCode As String, SetEnable As Boolean)
    Dim i As Integer
    For i = 0 To chkAppl.UBound
        If chkAppl(i).Tag = KeyCode Then
            chkAppl(i).Enabled = SetEnable
            Exit For
        End If
    Next
End Sub
Private Sub CheckRefreshSettings()
    'Dim tmpTimeFrame As String
    Dim tmpTag As String
    Dim FilterList As String
    Dim ChkValue As Boolean
    Dim i As Integer
    ChkValue = False
    'tmpTimeFrame = txtVpfr(0).Tag & "/" & txtVpto(0).Tag
    HandleAodbFilter chkWork(0), False
    FilterList = MyGlobalResult
    For i = 0 To FilterPanel.UBound
        If FilterPanel(i).Visible Then
            If chkWork(i).Value = 1 Then
                tmpTag = chkWork(i).Tag
                If tmpTag <> FilterList Then
                    ChkValue = True
                    Exit For
                End If
            End If
        End If
    Next
    AdjustApplButton "AODBREFRESH", ChkValue
    AdjustApplButton "FILEREFRESH", ChkValue
End Sub

Private Sub SearchInList(UseIndex As Integer, Method As Integer)
    Static LastIndex As Integer
    Dim HitLst As String
    Dim HitRow As Long
    Dim NxtRow As Long
    Dim NewScroll As Long
    Dim LoopCount As Integer
    Dim tmpText As String
    Dim ColNbr As String
    Dim Index As Integer
    Dim UseCol As Long
    Index = UseIndex
    If Index < 0 Then Index = LastIndex
    LastIndex = Index
    tmpText = Trim(txtFilterVal(Index).Text)
    UseCol = 0
    If tmpText <> "" Then
        LoopCount = 0
        Do
            HitLst = LeftFilterTab(Index).GetNextLineByColumnValue(UseCol, tmpText, Method)
            HitRow = Val(HitLst)
            If HitLst <> "" Then
                If HitRow > 5 Then NewScroll = HitRow - 5 Else NewScroll = 0
                LeftFilterTab(Index).OnVScrollTo NewScroll
            Else
                LeftFilterTab(Index).SetCurrentSelection -1
                LeftFilterTab(Index).OnVScrollTo 0
            End If
            LoopCount = LoopCount + 1
        Loop While LoopCount < 2 And HitLst = ""
        LeftFilterTab(Index).SetCurrentSelection HitRow
    End If
    txtFilterVal(Index).SetFocus
End Sub


