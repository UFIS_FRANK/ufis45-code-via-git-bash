VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form Deicing 
   Caption         =   "De-icing"
   ClientHeight    =   5865
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   6285
   Icon            =   "Deicing.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5865
   ScaleWidth      =   6285
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer tmrReload 
      Enabled         =   0   'False
      Interval        =   60000
      Left            =   5640
      Tag             =   "0"
      Top             =   3840
   End
   Begin VB.Timer tmrRefresh 
      Enabled         =   0   'False
      Interval        =   60000
      Left            =   5640
      Tag             =   "0"
      Top             =   3360
   End
   Begin VB.Timer tmrTimer 
      Enabled         =   0   'False
      Interval        =   100
      Left            =   5760
      Top             =   5280
   End
   Begin VB.PictureBox picMainButtons 
      Height          =   435
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   6015
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      Width           =   6075
   End
   Begin TABLib.TAB tabFlightList 
      Height          =   5295
      Left            =   0
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   480
      Width           =   4455
      _Version        =   65536
      _ExtentX        =   7858
      _ExtentY        =   9340
      _StockProps     =   64
   End
End
Attribute VB_Name = "Deicing"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private PDFExport As ActiveReportsPDFExport.ARExportPDF
Private ExcelExport As ActiveReportsExcelExport.ARExportExcel

Private rsAft As ADODB.Recordset
Private rsDeicing As ADODB.Recordset
Private rsView As ADODB.Recordset
Private rsAirlines As ADODB.Recordset
Private rsHandlingRels As ADODB.Recordset
Private rsAircraftPos As ADODB.Recordset
Private rsAircraftPosGroups As ADODB.Recordset
Private rsAircraftPosGroupRels As ADODB.Recordset
Private rsSeason As ADODB.Recordset

Private rsFieldUsed As ADODB.Recordset
Private colFieldUsed As CollectionExtended

Private strDataFormat As String
Private strBoolColumns As String

Private lngAftUrnoColNo As Long
Private lngIceUrnoColNo As Long
Private strAftUrnoCol As String
Private strIceUrnoCol As String

Private strEditedAftUrno As String

Private colViewParams As CollectionExtended
Private colSearchParams As CollectionExtended

Private IsOnEdit As Boolean
Private lngCurrLine As Long
Private lngCurrCol As Long
Private OldValue As Variant
    
Private blnOpenFileAfterExporting  As Boolean
Private blnAlwaysSort As Boolean
Private intRelPrdBuffer As Integer
Private intRelRefreshIntv As Integer
Private intRelReloadIntv As Integer

Public Sub LoadData(ByVal LoadParam As Variant)
    Dim vntDataSources As Variant
    Dim vntDataSource As Variant
    Dim strWhere As String
    Dim colLocalParams As New CollectionExtended
    
    Screen.MousePointer = vbHourglass
    
    If IsObject(LoadParam) Then
        If TypeOf LoadParam Is CollectionExtended Then
            Set colViewParams = LoadParam
        Else
            Set colViewParams = GetDefaultParamCollection()
        End If
    Else
        Set colViewParams = GetDefaultParamCollection()
    End If
    
    If pstrDataSource = "" Then
        pstrDataSource = GetConfigEntry(colConfigs, MyMainForm.AppType, "DATA_SOURCE", "AFTTAB")
    End If
    If InStr(pstrDataSource & ",", "AFTTAB,") > 0 Then
        strWhere = BuildWhereClause(colViewParams)
                
        colLocalParams.Add "URNO", "OrderByClause"
        colLocalParams.Add strWhere, "WhereClause"
        
        pstrAftUrnoList = AddDataSource(MyMainForm.AppType, colDataSources, "AFTTAB", colLocalParams, "URNO")
        
        Set rsAft = colDataSources.Item("AFTTAB")
        If pstrAftUrnoList = "" Then
            pstrAftUrnoList = "0"
        End If
        
        Call ReadDataSource(pstrAftUrnoList)
        
        If colDataSources.Exists("ICETAB") Then
            Set rsDeicing = colDataSources.Item("ICETAB")
        End If
        
        'activate/deactivate timer
        tmrRefresh.Tag = 0
        tmrReload.Tag = (intRelPrdBuffer * 60) - intRelReloadIntv
            
        If colViewParams.Item("PERIODTYPE") = 0 Then 'absolute period
            tmrRefresh.Enabled = False
            tmrReload.Enabled = False
        Else 'relative period
            tmrRefresh.Enabled = (intRelRefreshIntv > 0)
            tmrReload.Enabled = (intRelReloadIntv > 0)
        End If
    End If
    
    Call DisplayFlightList(colViewParams)
    
    Screen.MousePointer = vbDefault
End Sub

Public Sub ChangeRowSelection(ByVal Key As String)
    Dim strRow As String
    
    strRow = tabFlightList.GetLinesByColumnValue(lngAftUrnoColNo, Key, 0)
    If strRow <> "" Then
        tabFlightList.SetCurrentSelection CLng(strRow)
        tabFlightList.OnVScrollTo CLng(strRow)
    End If
End Sub

Private Sub ChangeTimeZone(ByVal UTCTime As Boolean)
    Dim blnProceed As Boolean
    Dim i As Long, j As Integer
    Dim vntActFields As Variant
    Dim vntDataFormat As Variant
    Dim vntActFieldOptions As Variant
    Dim vntActFieldOption As Variant
    Dim vntTableField As Variant
    Dim strTable As String
    Dim strField As String
    Dim vntValue As Variant
    Dim blnUpdate As Boolean
        
    blnProceed = (IsUTCTimeZone And (Not UTCTime)) Or (Not (IsUTCTimeZone) And UTCTime)
    
    If blnProceed Then
        IsUTCTimeZone = UTCTime
    
        vntActFields = Split(tabFlightList.Tag, ",")
        vntDataFormat = Split(strDataFormat, ",")
    
        For i = 0 To tabFlightList.GetLineCount - 1
            rsAft.Find "URNO = " & tabFlightList.GetColumnValue(i, lngAftUrnoColNo), , , 1
            If Not rsAft.EOF Then
                rsDeicing.Find "URNO = " & Val(tabFlightList.GetColumnValue(i, lngIceUrnoColNo)), , , 1
                
                For j = 0 To tabFlightList.GetColumnCount - 1
                    blnUpdate = False
                    vntActFieldOptions = Split(vntActFields(j), "|")
                    For Each vntActFieldOption In vntActFieldOptions
                        vntTableField = Split(vntActFieldOption, ".")
                        strTable = vntTableField(0)
                        strField = vntTableField(1)
                
                        vntValue = ""
                        With colDataSources.Item(strTable)
                            If Not .EOF Then
                                If Not IsNull(.Fields(strField).Value) Then
                                    If .Fields(strField).Type = adDate Then
                                        vntValue = .Fields(strField).Value
                                        If Not IsUTCTimeZone Then
                                            vntValue = UTCDateToLocal(vntValue, colUTCTimeDiff, rsSeason)
                                        End If
                                        blnUpdate = True
                                    End If
                                    
                                    If blnUpdate Then
                                        vntValue = GetFormattedData(vntValue, vntDataFormat(j))
                                    End If
                                End If
                            End If
                        End With
                        
                        If vntValue <> "" Then Exit For
                    Next vntActFieldOption
                
                    If blnUpdate Then
                        tabFlightList.SetColumnValue i, j, vntValue
                    End If
                Next j
            End If
        Next i
        tabFlightList.Refresh
        
        IsUTCTimeZone = UTCTime
    End If
End Sub

Private Function BuildWhereClause(ViewParamCollection As CollectionExtended) As String
    Dim dtmNow As Date
    Dim dtmStartDate As Date
    Dim dtmEndDate As Date
    Dim strStartDate As String
    Dim strEndDate As String
    Dim strStartTime As String
    Dim strEndTime As String
    Dim strRelStart As String
    Dim strRelEnd As String
    Dim vntDates As Variant
    Dim strParamVal As String
    Dim strWhere As String
    Dim strTemp As String
    Dim vntParamVal As Variant
    Dim i As Integer
    Dim strDays As String
    
    dtmNow = Now
    If IsUTCTimeZone Then
        dtmNow = LocalDateToUTC(dtmNow, colUTCTimeDiff, rsSeason)
    End If
    strRelStart = ViewParamCollection.Item("RELPRDSTART")
    strRelEnd = ViewParamCollection.Item("RELPRDEND")
        
    strStartDate = ViewParamCollection.Item("STARTDATE")
    strStartTime = ViewParamCollection.Item("STARTTIME")
    strEndDate = ViewParamCollection.Item("ENDDATE")
    strEndTime = ViewParamCollection.Item("ENDTIME")
    
    vntDates = GetParamDateArray(strStartDate, strStartTime, strEndDate, strEndTime, strRelStart, strRelEnd, dtmNow)
    dtmStartDate = vntDates(0)
    dtmEndDate = vntDates(1)
    
    'check DAYS
    If IsUTCTimeZone Then
        strParamVal = Trim(ViewParamCollection.Item("DAYS"))
        If strParamVal <> "" And Len(strParamVal) < 7 Then
            strDays = ""
            For i = 1 To Len(strParamVal)
                strDays = strDays & ",'" & Mid(strParamVal, i, 1) & "'"
            Next i
            If strDays <> "" Then strDays = Mid(strDays, 2)
            strWhere = strWhere & "AND DOOD IN (" & strDays & ")"
        End If
    End If
    
    'check ALC2
    strTemp = BuildSimpleWhereClause(ViewParamCollection, "ALC2", "ALC2")
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
    
    'check FLTN
    strTemp = BuildSimpleWhereClause(ViewParamCollection, "FLTN", "FLTN")
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
    
    'check FLNS
    strTemp = BuildSimpleWhereClause(ViewParamCollection, "FLNS", "FLNS")
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
    
    'check AIRLINE
    strTemp = BuildExtendedWhereClause(ViewParamCollection, "AIRLINE", _
        Array("ALC2", "ALC3"), Array(2, 3), True)
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
        
    'check CSGN
    strTemp = BuildSimpleWhereClause(ViewParamCollection, "CSGN", "CSGN")
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
    
    'check FLTI
    strTemp = BuildSimpleWhereClause(ViewParamCollection, "FLTI", "FLTI")
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
        
    'check REGN
    strTemp = BuildSimpleWhereClause(ViewParamCollection, "REGN", "REGN", True)
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
    
    'check ACTY
    strTemp = BuildExtendedWhereClause(ViewParamCollection, "ACTY", _
        Array("ACT3", "ACT5"), Array(3, 5), True)
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
    
    'check AIRP
    strTemp = BuildExtendedWhereClause(ViewParamCollection, "AIRP", _
        Array("DES3", "DES4"), Array(3, 4), True)
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
    
    'check VIAP
    strTemp = BuildExtendedWhereClause(ViewParamCollection, "VIAP", _
        Array("VIA3", "VIA4"), Array(3, 4), True)
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
    
    'check NATURE
    strTemp = BuildSimpleWhereClause(ViewParamCollection, "NATURE", "TTYP", True)
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
    
    'check STEV
    strTemp = BuildSimpleWhereClause(ViewParamCollection, "STEV", "STEV", True)
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
    
    'check HDLAGENT
    strParamVal = ViewParamCollection.Item("HDLAGENT")
    If Trim(strParamVal) <> "" Then
        vntParamVal = Split(strParamVal, ",")
        strParamVal = ""
        For i = 0 To UBound(vntParamVal)
            rsHandlingRels.Filter = "HSNA = '" & vntParamVal(i) & "'"
            Do While Not rsHandlingRels.EOF
                rsAirlines.Find "URNO = " & rsHandlingRels.Fields("ALTU").Value, , , 1
                If Not rsAirlines.EOF Then
                    strParamVal = strParamVal & ",'" & rsAirlines.Fields("ALC2").Value & "'"
                End If
                
                rsHandlingRels.MoveNext
            Loop
            rsHandlingRels.Filter = adFilterNone
        Next i
        If strParamVal <> "" Then
            strParamVal = Mid(strParamVal, 2)
            strWhere = strWhere & " AND ALC2 IN (" & strParamVal & ")"
        End If
    End If
    
    'check HTYP
    strTemp = BuildSimpleWhereClause(ViewParamCollection, "HTYP", "HTYP", True)
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
    
    'check PSTN/PSGR
    'check PSTN
    strParamVal = ViewParamCollection.Item("PSTN")
    If strParamVal <> "" Then
        strParamVal = ",'" & Replace(strParamVal, ",", "','") & "'"
    End If
    'check PSGR
    strTemp = ViewParamCollection.Item("PSGR")
    If strTemp <> "" Then
        vntParamVal = Split(strTemp, ",")
        For i = 0 To UBound(vntParamVal)
            rsAircraftPosGroups.Find "GRPN = '" & vntParamVal(i) & "'", , , 1
            If Not rsAircraftPosGroups.EOF Then
                rsAircraftPosGroupRels.Filter = "GURN = " & rsAircraftPosGroups.Fields("URNO").Value
                Do While Not rsAircraftPosGroupRels.EOF
                    rsAircraftPos.Find "URNO = " & rsAircraftPosGroupRels.Fields("VALU").Value, , , 1
                    If Not rsAircraftPos.EOF Then
                        strParamVal = strParamVal & ",'" & rsAircraftPos.Fields("PNAM").Value & "'"
                    End If
                    
                    rsAircraftPosGroupRels.MoveNext
                Loop
                rsAircraftPosGroupRels.Filter = adFilterNone
            End If
        Next i
    End If
    If strParamVal <> "" Then
        strParamVal = Mid(strParamVal, 2)
        strWhere = strWhere & " AND PSTD IN (" & strParamVal & ")"
    End If
    
    strTemp = BuildSimpleWhereClause(ViewParamCollection, "PSTN", "PSTD", True)
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
    
    'check FTYP
    strParamVal = GetParamValList(ViewParamCollection, Array("O", "S", "X", "N", "D", "R", "P", "G", "T"), _
        Array("O,Z,B", "S", "X", "N", "D", "R", "G", " ", "T"))
    If strParamVal <> "" Then
        strParamVal = "'" & Replace(strParamVal, ",", "','") & "'"
        strWhere = strWhere & " AND FTYP IN (" & strParamVal & ")"
    End If
    
    If Not IsUTCTimeZone Then
        dtmStartDate = LocalDateToUTC(dtmStartDate, colUTCTimeDiff, rsSeason)
        dtmEndDate = LocalDateToUTC(dtmEndDate, colUTCTimeDiff, rsSeason)
    End If
    
    'add to paramcollection -> UTC START/END Date
    ViewParamCollection.Add dtmStartDate, "UTCSTARTDATE"
    ViewParamCollection.Add dtmEndDate, "UTCENDDATE"
    ViewParamCollection.Add vntDates(2), "PERIODTYPE"

    If vntDates(2) = 1 Then 'Use relative period
        'add buffer
        dtmEndDate = DateAdd("h", intRelPrdBuffer, dtmEndDate)
    End If
    
    ViewParamCollection.Add dtmEndDate, "BUFFERENDDATE"
        
    strStartDate = Format(dtmStartDate, "YYYYMMDDhhmmss")
    strEndDate = Format(dtmEndDate, "YYYYMMDDhhmmss")
    
    BuildWhereClause = "((STOD BETWEEN '[:UTCSTARTDATE]' AND '[:BUFFERENDDATE]') OR " & _
        "(TIFD BETWEEN '[:UTCSTARTDATE]' AND '[:BUFFERENDDATE]')) AND " & _
        "AIRB = ' ' AND ADID = 'D' " & strWhere
        
    ViewParamCollection.Add BuildWhereClause, "SQLWHERECLAUSE"
        
    BuildWhereClause = Replace(BuildWhereClause, "[:UTCSTARTDATE]", strStartDate)
    BuildWhereClause = Replace(BuildWhereClause, "[:BUFFERENDDATE]", strEndDate)
End Function

Private Function GetDefaultParamCollection() As CollectionExtended
    Dim strRelBefore As String
    Dim strRelAfter As String
    
    rsView.Find "URNO = -1", , , 1
    If rsView.EOF Then
        strRelBefore = GetConfigEntry(colConfigs, MyMainForm.AppType, "LOAD_REL_BEFORE", "2")
        strRelAfter = GetConfigEntry(colConfigs, MyMainForm.AppType, "LOAD_REL_AFTER", "2")
            
        Set GetDefaultParamCollection = New CollectionExtended
        GetDefaultParamCollection.Add "<Default>", "VIEW"
        GetDefaultParamCollection.Add strRelBefore, "RELPRDSTART"
        GetDefaultParamCollection.Add strRelAfter, "RELPRDEND"
        GetDefaultParamCollection.Add "1", "DEP"
        GetDefaultParamCollection.Add "1", "O"
        GetDefaultParamCollection.Add "1", "X"
    Else
        Set GetDefaultParamCollection = GetViewParamCollection(rsView.Fields("TEXT").Value)
    End If
End Function

Private Sub AddDefaultView()
    Dim AppName As String
    Dim strRelBefore As String
    Dim strRelAfter As String
    
    AppName = Mid(colParameters.Item("AppName"), 2)
    AppName = Left(AppName, Len(AppName) - 1)
        
    strRelBefore = GetConfigEntry(colConfigs, MyMainForm.AppType, "LOAD_REL_BEFORE", "2")
    strRelAfter = GetConfigEntry(colConfigs, MyMainForm.AppType, "LOAD_REL_AFTER", "2")
    
    rsView.AddNew
    rsView.Fields("URNO").Value = -1
    rsView.Fields("APPN").Value = AppName
    rsView.Fields("TEXT").Value = "VIEW=<Default>;RELPRDSTART=" & strRelBefore & ";" & _
        "RELPRDEND=" & strRelAfter & ";DEP=1;O=1;X=1"
    rsView.Fields("PKNO").Value = LoginUserName
    rsView.Update
End Sub

Private Sub ShowFlightViewForm(ByVal Search As Boolean)
    Dim oViewForm As FlightView
    Dim strView As String
    Dim chk As CheckBox
    
    If Search Then
        If colDataSources.Exists("AFTTAB") Then
            If SearchViewForm Is Nothing Then
                Set SearchViewForm = New FlightView
            End If
            Set oViewForm = SearchViewForm
        End If
    Else
        Set oViewForm = FlightView
    End If
    
    If Not (oViewForm Is Nothing) Then
        oViewForm.IsSearchForm = Search
        '--Disable some controls
        For Each chk In oViewForm.chkADID
            chk.Enabled = (chk.Tag = "DEP")
        Next chk
        '---
        If Search Then
            oViewForm.chkADID(1).Value = vbChecked
        Else
            oViewForm.cboView.ListIndex = MainButtons.cboView.ListIndex
        End If
        If IsUTCTimeZone Then
            oViewForm.lblUTC_LT(0).Caption = "(UTC)"
            oViewForm.lblUTC_LT(1).Caption = "(Calculated in UTC times)"
        Else
            oViewForm.lblUTC_LT(0).Caption = "(LT)"
            oViewForm.lblUTC_LT(1).Caption = "(Calculated in local times)"
        End If
        oViewForm.Show vbModal
        strView = oViewForm.ViewString
        
        If strView <> "" Then
            If Search Then
                Set colSearchParams = GetViewParamCollection(strView, Search)
                Call ShowSearchList(colSearchParams)
            Else
                Set colViewParams = GetViewParamCollection(strView, Search)
                Call LoadData(colViewParams)
            End If
        End If
    End If
End Sub

Private Function ShowSearchList(colParams As CollectionExtended)
    Static oSearchListForm As Form
    Dim i As Integer
    Dim intColCount As Long
    Dim strColIndex As String
    Dim rsView As ADODB.Recordset
    
    Screen.MousePointer = vbHourglass
    
    strColIndex = ""
    intColCount = Split(tabFlightList.GetMainHeaderRanges, ",")(0)
    For i = 0 To intColCount - 1
        strColIndex = strColIndex & "," & CStr(i)
    Next i
    If strColIndex <> "" Then strColIndex = Mid(strColIndex, 2)
        
    If (oSearchListForm Is Nothing) Then
        Set oSearchListForm = SearchList
        oSearchListForm.SetParentForm Me
        oSearchListForm.SetMasterTab tabFlightList, strColIndex, lngAftUrnoColNo
    End If
    
    Set rsView = GetSearchViewRecordset(strColIndex, colParams)
    
    oSearchListForm.PopulateList rsView
    
    Screen.MousePointer = vbDefault
    
    oSearchListForm.Show vbModal
End Function

Private Sub SortRecordByColumnNo(ByVal ColNo As Long, Optional SortAscending As Variant)
    Dim lngCurrentSortCol As Long
    
    If IsMissing(SortAscending) Then
        lngCurrentSortCol = Val(tabFlightList.CurrentSortColumn)
        If ColNo = lngCurrentSortCol Then
            SortAscending = Not tabFlightList.SortOrderASC
        Else
            SortAscending = True
        End If
    End If
    
    Screen.MousePointer = vbHourglass
    tabFlightList.Sort CStr(ColNo), SortAscending, True
    Screen.MousePointer = vbDefault
End Sub

Private Function GetSearchViewRecordset(ByVal ColIndexList As String, colParams As CollectionExtended) As ADODB.Recordset
    Dim i As Integer
    
    Set GetSearchViewRecordset = New ADODB.Recordset
    GetSearchViewRecordset.Fields.Append "DATA", adVarChar, 400, adFldIsNullable
    GetSearchViewRecordset.Open
    
    For i = 0 To tabFlightList.GetLineCount - 1
        rsAft.Find "URNO = " & tabFlightList.GetColumnValue(i, lngAftUrnoColNo), , , 1
        If Not rsAft.EOF Then
            If ValidateDataWithCurrentView(rsAft, colParams) Then
                GetSearchViewRecordset.AddNew
                GetSearchViewRecordset.Fields("DATA").Value = tabFlightList.GetColumnValues(i, ColIndexList)
                GetSearchViewRecordset.Update
            End If
        End If
    Next i
End Function

Private Function GetViewParamCollection(ByVal ViewParams As String, Optional ByVal Search As Boolean = False) As CollectionExtended
    Dim vntParams As Variant
    Dim vntParam As Variant
    Dim vntParamItems As Variant
    Dim strParamKey As String
    Dim strParamValue As String
    Dim dtmDefStartDate As Date
    Dim dtmDefEndDate As Date
    Dim dtmNow As Date
    
    Set GetViewParamCollection = New CollectionExtended
    
    vntParams = Split(ViewParams, ";")
    For Each vntParam In vntParams
        vntParamItems = Split(vntParam, "=")
        strParamKey = vntParamItems(0)
        strParamValue = vntParamItems(1)
        
        GetViewParamCollection.Add strParamValue, strParamKey
    Next vntParam
    
    If Search Then
        dtmDefStartDate = colViewParams.Item("UTCSTARTDATE")
        dtmDefEndDate = colViewParams.Item("UTCENDDATE")
        If Not IsUTCTimeZone Then
            dtmDefStartDate = UTCDateToLocal(dtmDefStartDate, colUTCTimeDiff, rsSeason)
            dtmDefEndDate = UTCDateToLocal(dtmDefEndDate, colUTCTimeDiff, rsSeason)
        End If
    
        dtmNow = Now
        If IsUTCTimeZone Then
            dtmNow = LocalDateToUTC(dtmNow, colUTCTimeDiff, rsSeason)
        End If
        
        vntParams = GetParamDateArray( _
            GetViewParamCollection.Item("STARTDATE"), GetViewParamCollection.Item("STARTTIME"), _
            GetViewParamCollection.Item("ENDDATE"), GetViewParamCollection.Item("ENDTIME"), _
            GetViewParamCollection.Item("RELPRDSTART"), GetViewParamCollection.Item("RELPRDEND"), _
            dtmNow, dtmDefStartDate, dtmDefEndDate)
        
        If Not IsUTCTimeZone Then
            vntParams(0) = LocalDateToUTC(vntParams(0), colUTCTimeDiff, rsSeason)
            vntParams(1) = LocalDateToUTC(vntParams(1), colUTCTimeDiff, rsSeason)
        End If
        
        GetViewParamCollection.Add vntParams(0), "UTCSTARTDATE"
        GetViewParamCollection.Add vntParams(1), "UTCENDDATE"
    End If
End Function

Private Sub PrintDeicing(ByVal Media As String)
    Dim rpt As New PrintDeicing
    Dim strFileName As String
    Dim strDefPath As String
    Dim strDefFile As String
    
    Load rpt
    rpt.InitReport tabFlightList, MainButtons.optTimeZone(0).Value, Media
    If Media = "print" Then
        rpt.Show , Me
        'rpt.Refresh
    Else
        rpt.Visible = False
        If pblnIsOnTop Then
            'SetFormOnTop MyMainForm, False
            SetFormOnTop Me, False
        End If

        strDefPath = GetConfigEntry(colConfigs, MyMainForm.AppType, "EXPORT_" & UCase(Media) & "_PATH", App.Path)
        If Dir(strDefPath, vbDirectory) = "" Then
            If Not CreateFolder(strDefPath) Then
                MsgBox "Folder " & strDefPath & " could not be created!", vbCritical, Me.Caption
                Exit Sub
            End If
        End If
        
        strDefFile = "De-icing_" & Format(Now, "YYYYMMDDhhmmss")
        strFileName = GetFileName(MyMainForm.cdgSave, strDefPath, strDefFile, Media)
        If strFileName <> "" Then
            If Dir(strFileName) <> "" Then
                On Error Resume Next
                Err.Number = 0
                Kill strFileName
                If Err.Number <> 0 Then
                    MsgBox "Could not overwrite file " & strFileName & vbNewLine & Err.Description, vbCritical, Me.Caption
                    Err.Number = 0
                    Exit Sub
                End If
                On Error GoTo 0
            End If
        
            rpt.Run False

            Select Case Media
                Case "excel"
                    Set ExcelExport = New ActiveReportsExcelExport.ARExportExcel
                    ExcelExport.FileName = strFileName
                    ExcelExport.Export rpt.Pages

                    Set ExcelExport = Nothing
                Case "pdf"
                    Set PDFExport = New ActiveReportsPDFExport.ARExportPDF
                    PDFExport.FileName = strFileName
                    PDFExport.Export rpt.Pages

                    Set PDFExport = Nothing
            End Select
            
            If blnOpenFileAfterExporting Then
                OpenFileUsingDefaultApp Me, strFileName
            End If
        End If

        If pblnIsOnTop Then
            'SetFormOnTop MyMainForm, pblnIsOnTop
            SetFormOnTop Me, pblnIsOnTop
        End If

        Unload rpt
    End If
    
    Set rpt = Nothing
End Sub

Public Sub MainControlsExecuted(ByVal Key As String, Optional ByVal Value As Variant)
    Select Case Key
        Case "view"
            Call ShowFlightViewForm(False)
        Case "viewselect"
            Call ChangeView(Value(0), Value(1))
        Case "search"
            Call ShowFlightViewForm(True)
        Case "print", "excel"
            Call PrintDeicing(Key)
        Case "timezone"
            Call ChangeTimeZone(Value)
        Case "ontop"
            Call ChangeWindowOnTop(Value)
        Case "fipsreport"
            Call OpenFIPSReport
    End Select
End Sub

Private Sub OpenFIPSReport()
    Dim strFIPSReportPath As String
    Dim strParameter As String
    
    strFIPSReportPath = GetConfigEntry(colConfigs, MyMainForm.AppType, "FIPS_REPORT_PATH", "")
    If strFIPSReportPath = "" Then
        MsgBox "FIPSReport path has not been configured!", vbInformation, Me.Caption
        Exit Sub
    End If
    
    If Dir(strFIPSReportPath) = "" Then
        MsgBox "Could not find " & strFIPSReportPath, vbInformation, Me.Caption
        Exit Sub
    End If
    
    strParameter = EvaluateParameters(colParameters, GetConfigEntry(colConfigs, MyMainForm.AppType, "FIPS_REPORT_PARAMETER", ""))
    If strParameter <> "" Then
        strFIPSReportPath = strFIPSReportPath & " " & strParameter
    End If
    
    Shell strFIPSReportPath, vbNormalFocus
End Sub

Private Sub ReadDataSource(ByVal FlightUrno As String)
    Dim intUrnoCountInWhereClause As Integer
    Dim vntFlightUrno As Variant
    Dim intUrnoCount As Integer
    Dim intArrayCount As Integer
    Dim i As Integer, j As Integer
    Dim strUrno As String
    Dim intStart As Integer, intEnd As Integer
    Dim strWhere As String
    Dim strDataSource As String
    Dim vntDataSources As Variant
    Dim vntDataSourceItem As Variant
    
    intUrnoCountInWhereClause = Val(GetConfigEntry(colConfigs, "MAIN", "URNO_COUNT_IN_WHERE_CLAUSE", "300"))
    
    vntFlightUrno = Split(FlightUrno, ",")
    intUrnoCount = UBound(vntFlightUrno) + 1
    If intUrnoCount > intUrnoCountInWhereClause Then
        FlightUrno = ""
        intArrayCount = Int(intUrnoCount / intUrnoCountInWhereClause) + 1
        For i = 1 To intArrayCount
            intStart = intUrnoCountInWhereClause * (i - 1)
            intEnd = intStart + (intUrnoCountInWhereClause - 1)
            If intEnd > intUrnoCount - 1 Then
                intEnd = intUrnoCount - 1
            End If
        
            strUrno = ""
            For j = intStart To intEnd
                strUrno = strUrno & "," & vntFlightUrno(j)
            Next j
            strUrno = Mid(strUrno, 2)
            
            FlightUrno = FlightUrno & "|" & strUrno
        Next i
        If FlightUrno <> "" Then FlightUrno = Mid(FlightUrno, 2)
    End If
    
   If colParameters.Exists("AftUrnoList") Then
        colParameters.Remove "AftUrnoList"
    End If
    colParameters.Add FlightUrno, "AftUrnoList"
    
    If pstrDataSource = "" Then
        pstrDataSource = GetConfigEntry(colConfigs, MyMainForm.AppType, "DATA_SOURCE", "")
    End If
    vntDataSources = Split(pstrDataSource, ",")
    For Each vntDataSourceItem In vntDataSources
        If vntDataSourceItem <> "AFTTAB" Then
            Call AddDataSource(MyMainForm.AppType, colDataSources, vntDataSourceItem, colParameters, , True, True, "|")
        End If
    Next vntDataSourceItem
End Sub

Private Sub ValidateAndSaveUserEntry(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim NewValue As Variant
    Dim strField As String
    Dim strValue As String
    Dim blnValid As Boolean
    Dim dtmSTDT As Date
    Dim dtmDate As Date
    Dim dtmBeginDate As Date
    Dim dtmEndDate As Date
    Dim ConvertedValue As Variant
    
    tabFlightList.EnableInlineEdit False
    IsOnEdit = False
    
    NewValue = tabFlightList.GetColumnValue(LineNo, ColNo)
    ConvertedValue = Null
    
    blnValid = False
    strField = GetRealItem(tabFlightList.LogicalFieldList, ColNo, ",")
    If NewValue = "" Then
        NewValue = " "
        blnValid = True
    Else
        Select Case strField
            Case "DITM", "DIHO"
                If IsNumeric(NewValue) Then
                    If Len(NewValue) <= rsDeicing.Fields(strField).DefinedSize Then
                        If CStr(Val(NewValue)) = NewValue Then
                            blnValid = True
                        End If
                    End If
                End If
            Case "DIAB", "DIAE"
                NewValue = CheckValidTimeExt(NewValue)
                If Not (NewValue = "ERROR" Or NewValue = "NULL") Then
                    rsAft.Find "URNO = " & tabFlightList.GetColumnValue(LineNo, lngAftUrnoColNo), , , 1
                    If Not rsAft.EOF Then
                        NewValue = Replace(NewValue, ";", "")
                    
                        dtmSTDT = rsAft.Fields("STOD").Value
                        If Not IsUTCTimeZone Then
                            dtmSTDT = UTCDateToLocal(dtmSTDT, colUTCTimeDiff, rsSeason)
                        End If
                        dtmSTDT = DateTimeToDate(dtmSTDT)
                        dtmDate = ShortTimeToFullDateTime(dtmSTDT, NewValue)
                        If Not IsUTCTimeZone Then
                            dtmDate = LocalDateToUTC(dtmDate, colUTCTimeDiff, rsSeason)
                        End If
                        ConvertedValue = Format(dtmDate, "YYYYMMDDhhmmss")
                        
                        If strField = "DIAB" Then
                            dtmBeginDate = dtmDate
                            If Trim(tabFlightList.GetFieldValue(LineNo, "DIAE")) <> "" Then
                                dtmEndDate = ShortTimeToFullDateTime(dtmSTDT, tabFlightList.GetFieldValue(LineNo, "DIAE"))
                                If Not IsUTCTimeZone Then
                                    dtmEndDate = LocalDateToUTC(dtmEndDate, colUTCTimeDiff, rsSeason)
                                End If
                            Else
                                dtmEndDate = dtmDate
                            End If
                        Else
                            If Trim(tabFlightList.GetFieldValue(LineNo, "DIAB")) <> "" Then
                                dtmBeginDate = ShortTimeToFullDateTime(dtmSTDT, tabFlightList.GetFieldValue(LineNo, "DIAB"))
                                If Not IsUTCTimeZone Then
                                    dtmBeginDate = LocalDateToUTC(dtmBeginDate, colUTCTimeDiff, rsSeason)
                                End If
                            Else
                                dtmBeginDate = dtmDate + 1
                            End If
                            dtmEndDate = dtmDate
                        End If
                        
                        If dtmEndDate >= dtmBeginDate Then
                            tabFlightList.SetColumnValue LineNo, ColNo, NewValue
                            blnValid = True
                        Else
                            blnValid = False
                        End If
                    Else
                        blnValid = False
                    End If
                End If
        End Select
    End If
    
    If blnValid Then
        If Trim(OldValue) <> Trim(NewValue) Then
            If IsNull(ConvertedValue) Then
                ConvertedValue = NewValue
            End If
            Call UpdateDeicing(LineNo, strField, ConvertedValue)
            
            SetAndSortTabFieldValue LineNo, ColNo, NewValue, OldValue, blnAlwaysSort, True
        End If
    Else
        tabFlightList.SetColumnValue LineNo, ColNo, OldValue
    End If
End Sub

Private Sub UpdateDeicing(ByVal LineNo As Long, ByVal FieldNames As String, ByVal fieldValues As String)
    Dim strFlightUrno As String
    Dim strDeicingUrno As String
    Dim strTable As String
    Dim strCommand As String
    Dim strFields As String
    Dim strWhere As String
    Dim strData As String
    Dim strResult As String
    
    strFlightUrno = tabFlightList.GetFieldValue(LineNo, strAftUrnoCol)
    strDeicingUrno = tabFlightList.GetFieldValue(LineNo, strIceUrnoCol)
    
    strTable = "ICETAB"
    
    If Trim(strDeicingUrno) = "" Then
        rsAft.Find "URNO = " & strFlightUrno, , , 1
        If Not rsAft.EOF Then
            strDeicingUrno = GetNewUrno
            
            strCommand = "IRT"
            strFields = "URNO,RURN,FTYP,FLNO,ADID,STDT," & FieldNames
            strData = strDeicingUrno & "," & strFlightUrno & "," & _
                rsAft.Fields("FTYP").Value & "," & _
                rsAft.Fields("FLNO").Value & "," & _
                rsAft.Fields("ADID").Value & "," & _
                Format(rsAft.Fields("STOD").Value, "YYYYMMDDhhmmss") & "," & _
                fieldValues
            
            'update memory table
            Call UpdateRecord(rsDeicing, strCommand, strFields, strData, strDeicingUrno)
            
            oUfisServer.CallCeda strResult, strCommand, strTable, strFields, strData, strWhere, "", 0, False, False
            Call UpdateRecord(rsDeicing, strCommand, strFields, strData, strDeicingUrno)
            
            'update the tab
            tabFlightList.SetFieldValues LineNo, strIceUrnoCol, strDeicingUrno
            tabFlightList.Refresh
        End If
    Else
        strCommand = "URT"
        strFields = FieldNames
        strData = fieldValues
        strWhere = "WHERE URNO = " & strDeicingUrno
        
        'update memory table
        Call UpdateRecord(rsDeicing, strCommand, strFields, strData, strDeicingUrno)
        
        'update DB
        oUfisServer.CallCeda strResult, strCommand, strTable, strFields, strData, strWhere, "", 0, False, False
        Call UpdateRecord(rsDeicing, strCommand, strFields, strData, strDeicingUrno)
    End If
End Sub

Private Function GetTabFieldValue(ByVal LineNo As Long, ByVal Index As Variant) As String
    If IsNumeric(Index) Then
        GetTabFieldValue = tabFlightList.GetColumnValue(LineNo, Index)
    Else
        GetTabFieldValue = tabFlightList.GetFieldValue(LineNo, Index)
    End If
End Function

Private Sub SetTabFieldValue(ByVal LineNo As Long, ByVal Index As Variant, ByVal Value As Variant)
    If IsNumeric(Index) Then
        tabFlightList.SetColumnValue LineNo, Index, Value
    Else
        tabFlightList.SetFieldValues LineNo, Index, Value
    End If
End Sub

Private Sub SetAndSortTabFieldValue(ByVal LineNo As Long, ByVal Index As Variant, ByVal Value As Variant, _
Optional ByVal CurrentValue As Variant, Optional ByVal AlwaysSort As Boolean = False, _
Optional ByVal SortOnly As Boolean = False, Optional ByVal MoveCursor As Boolean = True)
    Dim lngCurrentSortCol As Long
    Dim strCurrentSortCol As String
    Dim blnCurrentSortAsc As Boolean
    Dim blnSameColumn As Boolean
    Dim blnSortNeeded As Boolean
    Dim vntBeforeValue As Variant
    Dim vntAfterValue As Variant
    Dim strAftUrno As String
    
    blnSortNeeded = False
    
    If AlwaysSort Then
        lngCurrentSortCol = Val(tabFlightList.CurrentSortColumn)
        strCurrentSortCol = GetRealItem(tabFlightList.LogicalFieldList, lngCurrentSortCol, ",")
        If IsNumeric(Index) Then
            blnSameColumn = (Index = lngCurrentSortCol)
        Else
            blnSameColumn = (Index = strCurrentSortCol)
        End If
        
        If blnSameColumn Then
            If IsMissing(CurrentValue) Then
                CurrentValue = GetTabFieldValue(LineNo, Index)
            End If
            If Value <> CurrentValue Then
                blnCurrentSortAsc = tabFlightList.SortOrderASC
                
                If LineNo > 0 Then
                    vntBeforeValue = GetTabFieldValue(LineNo - 1, Index)
                End If
                If LineNo < tabFlightList.GetLineCount - 1 Then
                    vntAfterValue = GetTabFieldValue(LineNo + 1, Index)
                End If
                
                'convert to number for DITM, DIHO
                Select Case strCurrentSortCol
                    Case "DITM", "DIHO"
                        Value = Val(Value)
                        vntBeforeValue = Val(vntBeforeValue)
                        vntAfterValue = Val(vntAfterValue)
                End Select
                
                If blnCurrentSortAsc Then
                    If LineNo = 0 Then
                        blnSortNeeded = Value > vntAfterValue
                    ElseIf LineNo = tabFlightList.GetLineCount - 1 Then
                        blnSortNeeded = Value < vntBeforeValue
                    Else
                        blnSortNeeded = Value < vntBeforeValue Or Value > vntAfterValue
                    End If
                Else
                    If LineNo = 0 Then
                        blnSortNeeded = Value < vntAfterValue
                    ElseIf LineNo = tabFlightList.GetLineCount - 1 Then
                        blnSortNeeded = Value > vntBeforeValue
                    Else
                        blnSortNeeded = Value > vntBeforeValue Or Value < vntAfterValue
                    End If
                End If
            End If
        End If
    End If
    
    If Not SortOnly Then
        Call SetTabFieldValue(LineNo, Index, Value)
    End If
    If blnSortNeeded Then
        If MoveCursor Then
            strAftUrno = tabFlightList.GetColumnValue(LineNo, lngAftUrnoColNo)
        Else
            strAftUrno = tabFlightList.GetColumnValue(tabFlightList.GetCurrentSelected, lngAftUrnoColNo)
        End If
        
        If IsOnEdit Then
            Call ValidateAndSaveUserEntry(lngCurrLine, lngCurrCol)
        End If
        
        tabFlightList.Sort lngCurrentSortCol, blnCurrentSortAsc, True
        
        If strAftUrno <> "" Then
            LineNo = tabFlightList.GetLinesByColumnValue(lngAftUrnoColNo, strAftUrno, 0)
            tabFlightList.SetCurrentSelection LineNo
        End If
    End If
End Sub

Private Sub SetLineColor(ByVal LineNo As Integer, Optional ByVal FlightType)
    Dim strReadOnlyColumns As String
    Dim i As Integer
    Dim blnHasData As Boolean
    Dim intACDC As Integer
    Dim ForeColor As Long
    Dim BackColor As Long
    
    If IsMissing(FlightType) Then
        tabFlightList.GetLineColor LineNo, ForeColor, BackColor
    Else
        Select Case FlightType
            Case "X" 'Cancelled
                ForeColor = vbRed
            Case "N" 'No-op
                ForeColor = RGB(255, 165, 0) 'Orange
            Case Else
                ForeColor = vbBlack
        End Select
    End If
    
    intACDC = GetRealItemNo(tabFlightList.LogicalFieldList, "ACDC")
    strReadOnlyColumns = tabFlightList.NoFocusColumns & "," & CStr(intACDC)
    blnHasData = False
    For i = 0 To tabFlightList.GetColumnCount - 1
        If InStr("," & strReadOnlyColumns & ",", "," & CStr(i) & ",") <= 0 Then
            If InStr("," & strBoolColumns & ",", "," & CStr(i) & ",") > 0 Then
                blnHasData = Not ((Trim(tabFlightList.GetColumnValue(LineNo, i)) = "") Or _
                    (Trim(tabFlightList.GetColumnValue(LineNo, i)) = "N"))
            Else
                blnHasData = Not (Trim(tabFlightList.GetColumnValue(LineNo, i)) = "")
            End If
            If blnHasData Then Exit For
        End If
    Next i
    
    If blnHasData Then
        tabFlightList.SetLineColor LineNo, ForeColor, RGB(211, 229, 250)
    Else
        tabFlightList.SetLineColor LineNo, ForeColor, vbWhite
    End If
End Sub

Private Sub PrepareTab()
    Dim i As Integer
    Dim strActualFieldList As String
    Dim strLogicalFieldList As String
    Dim strHeaderString As String
    Dim strHeaderLengthString As String
    Dim strHeaderAlignmentString As String
    Dim strColumnAlignmentString As String
    Dim strMainHeaderLengthString As String
    Dim strMainHeaderCaption As String
    Dim strReadOnlyColumns As String
    Dim vntBoolColumns As Variant
    Dim strPrintColumns As String
    Dim blnReadOnly As Boolean
    
    strActualFieldList = "AFTTAB.URNO,AFTTAB.FLNO,AFTTAB.CSGN,AFTTAB.DES3,AFTTAB.STOD,AFTTAB.ETDI," & _
        "AFTTAB.OFBL,AFTTAB.AIRB,PSTD,AFTTAB.ACT3,AFTTAB.REGN,ICETAB.DICE,ICETAB.ACDC,ICETAB.DITM," & _
        "ICETAB.DIHO, ICETAB.DIAB, ICETAB.DIAE"
    strLogicalFieldList = "URNO,FLNO,CSGN,DES3,STOD,ETDI,OFBL,AIRB,PSTD,ACT3,REGN,DICE,ACDC,DITM,DIHO,DIAB,DIAE"
    strHeaderString = "URNO,FLIGHT,C/S,DES,STD,ETD,OFB,ATD,POS,A/C,REG,REQ.,DOOR CLOSED,TIME (MIN),HOLD OVER (MIN),START,END"
    strHeaderLengthString = "0,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30"
    strHeaderAlignmentString = "C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C"
    strColumnAlignmentString = strHeaderAlignmentString
    strMainHeaderLengthString = "11,6"
    strMainHeaderCaption = "Flight Information,De-icing Information"
    strReadOnlyColumns = "0,1,2,3,4,5,6,7,8,9,10,11"
    strBoolColumns = "12,13"
    strPrintColumns = "0,1,2,3,4,5"
    strDataFormat = "YYYYMMDDhhmmss,,,,,,DD,hh:mm,hh:mm,hh:mm,hh:mm,,,,,,,,,"
    
    strActualFieldList = GetConfigEntry(colConfigs, MyMainForm.AppType, "TAB_ACTFLDLIST", strActualFieldList)
    strLogicalFieldList = GetConfigEntry(colConfigs, MyMainForm.AppType, "TAB_LOGFLDLIST", strLogicalFieldList)
    strHeaderString = GetConfigEntry(colConfigs, MyMainForm.AppType, "TAB_HDRSTRING", strHeaderString)
    strHeaderLengthString = GetConfigEntry(colConfigs, MyMainForm.AppType, "TAB_HDRLENGTH", strHeaderLengthString)
    strHeaderAlignmentString = GetConfigEntry(colConfigs, MyMainForm.AppType, "TAB_HDRALIGNMENT", strHeaderAlignmentString)
    strColumnAlignmentString = GetConfigEntry(colConfigs, MyMainForm.AppType, "TAB_COLALIGNMENT", strColumnAlignmentString)
    strMainHeaderLengthString = GetConfigEntry(colConfigs, MyMainForm.AppType, "TAB_MAINHEADERLENGTH", strMainHeaderLengthString)
    strMainHeaderCaption = GetConfigEntry(colConfigs, MyMainForm.AppType, "TAB_MAINHEADERCAPTION", strMainHeaderCaption)
    strReadOnlyColumns = GetConfigEntry(colConfigs, MyMainForm.AppType, "TAB_READONLYCOLUMNS", strReadOnlyColumns)
    strBoolColumns = GetConfigEntry(colConfigs, MyMainForm.AppType, "TAB_BOOLCOLUMNS", strBoolColumns)
    strPrintColumns = GetConfigEntry(colConfigs, MyMainForm.AppType, "TAB_PRINTCOLUMNS", strPrintColumns)
    strDataFormat = GetConfigEntry(colConfigs, MyMainForm.AppType, "TAB_DATAFORMAT", strDataFormat)
    
    With tabFlightList
        .ResetContent
        .Tag = strActualFieldList
        .myTag = strPrintColumns
        .LogicalFieldList = strLogicalFieldList
        .HeaderString = strHeaderString
        .HeaderLengthString = strHeaderLengthString
        .HeaderAlignmentString = strHeaderAlignmentString
        .ColumnAlignmentString = strColumnAlignmentString
        .SetMainHeaderValues strMainHeaderLengthString, strMainHeaderCaption, ""
        .NoFocusColumns = strReadOnlyColumns
        .SetMainHeaderFont .FontSize, False, False, True, 0, .FontName
        .MainHeader = True
        .PostEnterBehavior = 3
        .InplaceEditSendKeyEvents = True
            
        vntBoolColumns = Split(strBoolColumns, ",")
        For i = 0 To UBound(vntBoolColumns)
            .SetColumnBoolProperty vntBoolColumns(i), "Y", "N"
            
            blnReadOnly = (InStr("," & strReadOnlyColumns & ",", "," & vntBoolColumns(i) & ",") > 0)
            .SetBoolPropertyReadOnly vntBoolColumns(i), blnReadOnly
        Next i

        .ShowHorzScroller True
        .ShowVertScroller True
    End With
    
    'Add to collection of fields used
    Set rsFieldUsed = AddFieldsUsedToRecordset(strActualFieldList)
    Set colFieldUsed = AddFieldsUsedToCollection(rsFieldUsed)
    
    'save urno col no
    lngAftUrnoColNo = GetRealItemNo(tabFlightList.Tag, "AFTTAB.URNO")
    lngIceUrnoColNo = GetRealItemNo(tabFlightList.Tag, "ICETAB.URNO")
    strAftUrnoCol = GetRealItem(tabFlightList.LogicalFieldList, lngAftUrnoColNo, ",")
    strIceUrnoCol = GetRealItem(tabFlightList.LogicalFieldList, lngIceUrnoColNo, ",")
End Sub

Private Function AddFieldsUsedToRecordset(ByVal ActFields As String) As ADODB.Recordset
    Dim vntActFields As Variant
    Dim vntLogFields As Variant
    Dim vntActField As Variant
    Dim vntActFieldOptions As Variant
    Dim vntActFieldOption As Variant
    Dim vntTableField As Variant
    Dim strTable As String
    Dim strActFieldList As String
    Dim strLogFieldList As String
    Dim i As Integer, j As Integer
    Dim intOptionsCount As Integer
    Dim vntDataFormat As Variant
    
    Set AddFieldsUsedToRecordset = New ADODB.Recordset
    AddFieldsUsedToRecordset.Fields.Append "TableName", adVarChar, 6, adFldIsNullable
    AddFieldsUsedToRecordset.Fields.Append "ActField", adVarChar, 4, adFldIsNullable
    AddFieldsUsedToRecordset.Fields.Append "LogField", adVarChar, 4, adFldIsNullable
    AddFieldsUsedToRecordset.Fields.Append "DataFormat", adVarChar, 20, adFldIsNullable
    AddFieldsUsedToRecordset.Fields.Append "Index", adTinyInt, , adFldIsNullable
    AddFieldsUsedToRecordset.Open
    AddFieldsUsedToRecordset.Sort = "TableName"
    
    vntActFields = Split(tabFlightList.Tag, ",")
    vntLogFields = Split(tabFlightList.LogicalFieldList, ",")
    vntDataFormat = Split(strDataFormat, ",")
    i = 0
    For Each vntActField In vntActFields
        vntActFieldOptions = Split(vntActField, "|")
        intOptionsCount = UBound(vntActFieldOptions)
        j = 0
        For Each vntActFieldOption In vntActFieldOptions
            vntTableField = Split(vntActFieldOption, ".")
            
            strTable = vntTableField(0)
            strActFieldList = vntTableField(1)
            strLogFieldList = vntLogFields(i)
            
            AddFieldsUsedToRecordset.AddNew
            AddFieldsUsedToRecordset.Fields("TableName").Value = strTable
            AddFieldsUsedToRecordset.Fields("ActField").Value = strActFieldList
            AddFieldsUsedToRecordset.Fields("LogField").Value = strLogFieldList
            AddFieldsUsedToRecordset.Fields("DataFormat").Value = vntDataFormat(i)
            AddFieldsUsedToRecordset.Fields("Index").Value = intOptionsCount - j
            AddFieldsUsedToRecordset.Update
            
            j = j + 1
        Next vntActFieldOption
        
        i = i + 1
    Next vntActField
End Function

Private Function AddFieldsUsedToCollection(rsFieldList As ADODB.Recordset) As CollectionExtended
    Dim strTable As String
    Dim strField As String
    Dim strFieldList As String
    
    Set AddFieldsUsedToCollection = New CollectionExtended
    
    If Not (rsFieldList.BOF And rsFieldList.EOF) Then
        rsFieldList.MoveFirst
    End If
    
    strFieldList = ""
    strTable = ""
    Do While Not rsFieldList.EOF
        If strTable <> rsFieldList.Fields("TableName").Value Then
            If strTable <> "" Then
                If strFieldList <> "" Then strFieldList = Mid(strFieldList, 2)
                AddFieldsUsedToCollection.Add strFieldList, strTable
            End If
            
            strTable = rsFieldList.Fields("TableName").Value
            strFieldList = ""
        End If
        
        strField = rsFieldList.Fields("ActField").Value
        strFieldList = strFieldList & "," & strField
        
        rsFieldList.MoveNext
    Loop
    
    If strTable <> "" Then
        If strFieldList <> "" Then strFieldList = Mid(strFieldList, 2)
        AddFieldsUsedToCollection.Add strFieldList, strTable
    End If
End Function

Public Sub RefreshDisplay(ByVal CedaCmd As String, ByVal TableName As String, ByVal Urno As String, Optional ByVal ChangedFields As String)
    Dim strADID As String
    Dim strLine As String

    Select Case TableName
        Case "AFTTAB"
            strLine = tabFlightList.GetLinesByColumnValue(lngAftUrnoColNo, rsAft.Fields("URNO").Value, 0)
            Select Case CedaCmd
                Case "IFR", "IRT"
                    If strLine = "" Then 'Not Found
                        If ValidateDataWithCurrentView(rsAft, colViewParams) Then ' match the criteria, insert to tab
                            Call UpdateTabValues(-1, TableName, CedaCmd, , rsAft.Fields("FTYP").Value)
                        End If
                    Else 'Found
                        If ValidateDataWithCurrentView(rsAft, colViewParams) Then ' match the criteria, update
                            Call UpdateTabValues(CLng(strLine), TableName, CedaCmd, , rsAft.Fields("FTYP").Value)
                        Else ' not match, remove from display
                            Call UpdateTabValues(CLng(strLine), TableName, CedaCmd, True)
                        End If
                    End If
                Case "UFR", "URT"
                    Call AddTextToLogForm(BcLogForm, _
                            "AFTTAB Update Broadcast Found!!" & vbNewLine & _
                            "TableName : " & TableName & vbNewLine & _
                            "Urno : " & Urno & vbNewLine & _
                            "Fields : " & ChangedFields & vbNewLine)
'                    If IsRefreshNeeded(TableName, ChangedFields) Then
                        If strLine <> "" Then 'Found
                            If ValidateDataWithCurrentView(rsAft, colViewParams) Then ' match the criteria, update
                                Call AddTextToLogForm(BcLogForm, _
                                    "FLIGHT MATCHES THE VIEW FILTER. REFRESH IT NOW!" & vbNewLine & _
                                    vbNewLine)
                            
                                Call UpdateTabValues(CLng(strLine), TableName, CedaCmd, , rsAft.Fields("FTYP").Value)
                            Else ' not match, remove from display
                                Call AddTextToLogForm(BcLogForm, _
                                    "FLIGHT DOESN'T MATCH THE VIEW FILTER. REMOVE IT FROM DISPLAY!" & vbNewLine & _
                                    vbNewLine)
                            
                                Call UpdateTabValues(CLng(strLine), TableName, CedaCmd, True)
                            End If
                        Else
                            If ValidateDataWithCurrentView(rsAft, colViewParams) Then ' match the criteria, update
                                Call AddTextToLogForm(BcLogForm, _
                                    "URNO NOT FOUND BUT MATCHES THE VIEW FILTER. INSERT IT NOW!" & vbNewLine & _
                                    vbNewLine)
                                
                                Call UpdateTabValues(-1, TableName, CedaCmd, , rsAft.Fields("FTYP").Value)
                                
                                'simulate the IRT of ICETAB
                                rsDeicing.Find "RURN = " & rsAft.Fields("URNO").Value, , , 1
                                If Not rsDeicing.EOF Then
                                    Call RefreshDisplay("IRT", "ICETAB", rsDeicing.Fields("URNO").Value)
                                End If
                            Else ' not match, do nothing
                                Call AddTextToLogForm(BcLogForm, _
                                    "URNO NOT FOUND AND DOESN'T MATCH THE VIEW FILTER. NO REFRESH NEEDED!" & vbNewLine & _
                                    vbNewLine)
                            End If
                        End If
'                    Else
'                        Call AddTextToLogForm(BcLogForm, _
'                            "UPDATED FIELDS NOT DISPLAYED IN APP. NO REFRESH NEEDED!" & vbNewLine & _
'                            vbNewLine)
'                    End If
                Case "DRT"
                    If strLine <> "" Then 'Found
                        'remove from display
                        Call UpdateTabValues(CLng(strLine), TableName, CedaCmd, True)
                    End If
            End Select
        Case "ICETAB"
            Call AddTextToLogForm(BcLogForm, _
                "ICETAB Broadcast Found!!" & vbNewLine & _
                "TableName : " & TableName & vbNewLine & _
                "CedaCmd : " & CedaCmd & vbNewLine & _
                "Urno : " & Urno & vbNewLine & _
                "Fields : " & ChangedFields & vbNewLine)
            
            If rsDeicing.BOF Or rsDeicing.EOF Then
                Call AddTextToLogForm(BcLogForm, _
                    "ICETAB URNO Not Found (BOF/EOF)!!" & vbNewLine & _
                    vbNewLine)
            Else
                strLine = tabFlightList.GetLinesByColumnValue( _
                          GetRealItemNo(tabFlightList.Tag, "ICETAB.URNO"), rsDeicing.Fields("URNO").Value, 0)
                If CedaCmd = "IRT" Then
                    If strLine = "" Then 'Not Found, find the AftUrno
                        Call AddTextToLogForm(BcLogForm, _
                            "ICETAB URNO Not Found!!" & vbNewLine)
                    
                        strLine = tabFlightList.GetLinesByColumnValue(lngAftUrnoColNo, rsDeicing.Fields("RURN").Value, 0)
                        If strLine <> "" Then 'Found
                            Call AddTextToLogForm(BcLogForm, _
                                "AFTTAB URNO Found!! REFRESH IT NOW!!" & vbNewLine & _
                                vbNewLine)
                        
                            Call UpdateTabValues(CLng(strLine), TableName, CedaCmd)
                        Else
                            Call AddTextToLogForm(BcLogForm, _
                                "AFTTAB URNO Not Found!! NO REFRESH NEEDED!!" & vbNewLine & _
                                vbNewLine)
                        End If
                    Else 'Found, update
                        Call AddTextToLogForm(BcLogForm, _
                            "ICETAB URNO Found!! REFRESH THE DISPLAY NOW!!" & vbNewLine & _
                            vbNewLine)
                    
                        Call UpdateTabValues(CLng(strLine), TableName, CedaCmd)
                    End If
                Else
                    If strLine <> "" Then
                        If CedaCmd = "DRT" Then
                            Call AddTextToLogForm(BcLogForm, _
                                "ICETAB URNO Found!! REMOVE IT FROM DISPLAY!!" & vbNewLine & _
                                vbNewLine)
                        
                            Call UpdateTabValues(CLng(strLine), TableName, CedaCmd, True)
                        Else
                            If IsRefreshNeeded(TableName, ChangedFields) Then
                                Call AddTextToLogForm(BcLogForm, _
                                    "ICETAB URNO Found!! REFRESH THE DISPLAY NOW!!!" & vbNewLine & _
                                    vbNewLine)
                            
                                Call UpdateTabValues(CLng(strLine), TableName, CedaCmd)
                            Else
                                Call AddTextToLogForm(BcLogForm, _
                                    "UPDATED FIELDS NOT DISPLAYED IN APP. NO REFRESH NEEDED!" & vbNewLine & _
                                    vbNewLine)
                            End If
                        End If
                    Else
                        Call AddTextToLogForm(BcLogForm, _
                            "ICETAB URNO Not Found!! NO REFRESH NEEDED!!" & vbNewLine & _
                            vbNewLine)
                    End If
                End If
            End If
    End Select
End Sub

Private Sub ChangeView(ByVal ViewIndex As Long, ByVal Urno As Long)
    rsView.Find "URNO = " & CStr(Urno), , , 1
    If rsView.EOF Then 'Not found, use default
        Set colViewParams = GetDefaultParamCollection()
    Else
        Set colViewParams = GetViewParamCollection(rsView.Fields("TEXT").Value)
    End If
    
    Call LoadData(colViewParams)
    
    If FormIsLoaded("FlightView") Then
        FlightView.cboView.ListIndex = ViewIndex
    End If
End Sub

Private Function ValidateDataWithCurrentView(rsAft As ADODB.Recordset, ViewParamCollection As CollectionExtended) As Boolean
    Dim dtmStartDate As Date
    Dim dtmEndDate As Date
    Dim intDayOfWeek As Integer
    Dim strParamVal As String
    Dim strTemp As String
    Dim vntParamVal As Variant
    Dim i As Integer

    ValidateDataWithCurrentView = True
    
    'Default: ADID = 'D'
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, "D", "ADID")
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'Default: AIRB = ' '
    ValidateDataWithCurrentView = IsNull(rsAft.Fields("AIRB").Value)
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'Validate against the view filter
    'check TIFD/STOD not Null
    ValidateDataWithCurrentView = Not IsNull(rsAft.Fields("TIFD").Value)
    If Not ValidateDataWithCurrentView Then Exit Function
    
    ValidateDataWithCurrentView = Not IsNull(rsAft.Fields("STOD").Value)
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check the start/end date
    dtmStartDate = ViewParamCollection.Item("UTCSTARTDATE")
    If ViewParamCollection.Item("PERIODTYPE") = 0 Then 'Absolute period
        dtmEndDate = ViewParamCollection.Item("UTCENDDATE")
    Else 'relative period
        dtmEndDate = LocalDateToUTC(Now, colUTCTimeDiff, rsSeason)
        dtmEndDate = DateAdd("h", Val(ViewParamCollection.Item("RELPRDEND")), dtmEndDate)
    End If
    'check TIFD/STOD >= start date and <= end date
    If dtmStartDate <> CDate(0) And dtmEndDate <> CDate(0) Then
        ValidateDataWithCurrentView = ((rsAft.Fields("TIFD").Value >= dtmStartDate) And (rsAft.Fields("TIFD").Value <= dtmEndDate)) Or _
            ((rsAft.Fields("STOD").Value >= dtmStartDate) And (rsAft.Fields("STOD").Value <= dtmEndDate))
        If Not ValidateDataWithCurrentView Then Exit Function
    End If
    
    'check the day of week
    strParamVal = ViewParamCollection.Item("DAYS")
    If Trim(strParamVal) <> "" Then
        If IsUTCTimeZone Then
            intDayOfWeek = Weekday(rsAft.Fields("TIFD").Value, vbMonday)
        Else
            intDayOfWeek = Weekday(UTCDateToLocal(rsAft.Fields("TIFD").Value, colUTCTimeDiff, rsSeason), vbMonday)
        End If
        ValidateDataWithCurrentView = (InStr(strParamVal, CStr(intDayOfWeek)) > 0)
        If Not ValidateDataWithCurrentView Then Exit Function
    End If
    
    'check ALC2
    strParamVal = ViewParamCollection.Item("ALC2")
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "ALC2")
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check FLTN
    strParamVal = ViewParamCollection.Item("FLTN")
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "FLTN")
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check FLNS
    strParamVal = ViewParamCollection.Item("FLNS")
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "FLNS")
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check AIRLINE
    strParamVal = ViewParamCollection.Item("AIRLINE")
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "ALC2", "ALC3")
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check CSGN
    strParamVal = ViewParamCollection.Item("CSGN")
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "CSGN")
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check FLTI
    strParamVal = ViewParamCollection.Item("FLTI")
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "FLTI")
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check REGN
    strParamVal = ViewParamCollection.Item("REGN")
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "REGN")
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check ACTY
    strParamVal = ViewParamCollection.Item("ACTY")
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "ACT3", "ACT5")
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check AIRP
    strParamVal = ViewParamCollection.Item("AIRP")
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "DES3", "DES4")
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check VIAP
    strParamVal = ViewParamCollection.Item("VIAP")
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "VIA3", "VIA4")
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check NATURE
    strParamVal = ViewParamCollection.Item("NATURE")
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "TTYP")
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check STEV
    strParamVal = ViewParamCollection.Item("STEV")
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "STEV")
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check HDLAGENT
    strParamVal = ViewParamCollection.Item("HDLAGENT")
    If Trim(strParamVal) <> "" Then
        ValidateDataWithCurrentView = False
        
        rsAirlines.Find "ALC2 = '" & Trim(rsAft.Fields("ALC2").Value) & "'", , , 1
        If rsAirlines.EOF Then
            rsAirlines.Find "ALC3 = '" & Trim(rsAft.Fields("ALC3").Value) & "'", , , 1
        End If
        If Not rsAirlines.EOF Then
            rsHandlingRels.Filter = "ALTU = " & rsAirlines.Fields("URNO").Value
            Do While Not rsHandlingRels.EOF
                ValidateDataWithCurrentView = IsDataInViewCriteria(rsHandlingRels, strParamVal, "HSNA")
                If ValidateDataWithCurrentView Then Exit Do
                
                rsHandlingRels.MoveNext
            Loop
            rsHandlingRels.Filter = adFilterNone
        End If
        
        If Not ValidateDataWithCurrentView Then Exit Function
    End If
    
    'check HTYP
    strParamVal = ViewParamCollection.Item("HTYP")
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "HTYP")
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check PSTN/PSGR
    'check PSTN
    strParamVal = ViewParamCollection.Item("PSTN")
    If strParamVal <> "" Then
        strParamVal = "," & strParamVal
    End If
    'check PSGR
    strTemp = ViewParamCollection.Item("PSGR")
    If strTemp <> "" Then
        vntParamVal = Split(strTemp, ",")
        For i = 0 To UBound(vntParamVal)
            rsAircraftPosGroups.Find "GRPN = '" & vntParamVal(i) & "'", , , 1
            If Not rsAircraftPosGroups.EOF Then
                rsAircraftPosGroupRels.Filter = "GURN = " & rsAircraftPosGroups.Fields("URNO").Value
                Do While Not rsAircraftPosGroupRels.EOF
                    rsAircraftPos.Find "URNO = " & rsAircraftPosGroupRels.Fields("VALU").Value, , , 1
                    If Not rsAircraftPos.EOF Then
                        strParamVal = strParamVal & "," & rsAircraftPos.Fields("PNAM").Value
                    End If
                    
                    rsAircraftPosGroupRels.MoveNext
                Loop
                rsAircraftPosGroupRels.Filter = adFilterNone
            End If
        Next i
    End If
    If strParamVal <> "" Then
        strParamVal = Mid(strParamVal, 2)
        ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "PSTD")
        If Not ValidateDataWithCurrentView Then Exit Function
    End If
    
    'check FTYP
    strParamVal = GetParamValList(ViewParamCollection, Array("O", "S", "X", "N", "D", "R", "P", "G", "T"), _
        Array("O,Z,B", "S", "X", "N", "D", "R", "G", " ", "T"))
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "FTYP")
    If Not ValidateDataWithCurrentView Then Exit Function
End Function

Private Function IsColumnEditable(ByVal ColNo As Long) As Boolean
    Dim blnReturn As Boolean
    
    blnReturn = (InStr("," & tabFlightList.NoFocusColumns & ",", "," & CStr(ColNo) & ",") <= 0)
    If blnReturn Then
        blnReturn = (InStr("," & strBoolColumns & ",", "," & CStr(ColNo) & ",") <= 0)
    End If
    
    IsColumnEditable = blnReturn
End Function

Private Function IsRefreshNeeded(ByVal TableName As String, ByVal ChangedFields As String) As Boolean
    Dim strFieldList As String
    
    strFieldList = colFieldUsed.Item(TableName)
    IsRefreshNeeded = IsInList(ChangedFields, strFieldList)
End Function

Private Sub UpdateTabValues(ByVal LineNo As Integer, ByVal TableName As String, ByVal CedaCmd As String, _
Optional ByVal Deleted As Boolean = False, Optional ByVal IsCancelled)
    Dim intIndex As Integer
    Dim vntData As Variant
    Dim i As Integer
    Dim lngCurrentSortCol As Long
    Dim strCurrentSortCol As String
    Dim NewValue As Variant
    Dim OldValue As Variant
    
    If LineNo = -1 Then 'insert
        tabFlightList.InsertTextLine "", False
        LineNo = tabFlightList.GetLineCount - 1
    End If
    
    lngCurrentSortCol = tabFlightList.CurrentSortColumn
    strCurrentSortCol = GetRealItem(tabFlightList.LogicalFieldList, lngCurrentSortCol, ",")

    rsFieldUsed.Find "TableName = '" & TableName & "'", , , 1
    If Deleted Then
        If TableName = "AFTTAB" Then
            tabFlightList.DeleteLine LineNo
        Else
            Do While Not rsFieldUsed.EOF
                If rsFieldUsed.Fields("TableName").Value <> TableName Then
                    Exit Do
                End If
                
                If strCurrentSortCol = rsFieldUsed.Fields("LogField").Value Then
                    OldValue = tabFlightList.GetFieldValue(LineNo, strCurrentSortCol)
                    NewValue = ""
                End If
                
                'set the value, but don't sort first
                tabFlightList.SetFieldValues LineNo, rsFieldUsed.Fields("LogField").Value, ""
                
                rsFieldUsed.MoveNext
            Loop
            
            'sort the record if neccessary
            Call SetLineColor(LineNo, IsCancelled)
            SetAndSortTabFieldValue LineNo, lngCurrentSortCol, NewValue, OldValue, blnAlwaysSort, True, False
        End If
    Else
        If TableName = "ICETAB" Then
            rsAft.Find "URNO = " & rsDeicing.Fields("RURN").Value, , , 1
        End If
    
        Do While Not rsFieldUsed.EOF
            If rsFieldUsed.Fields("TableName").Value <> TableName Then
                Exit Do
            End If
            
            intIndex = rsFieldUsed.Fields("Index").Value
            vntData = ""
            For i = 0 To intIndex
                If vntData = "" Then
                    vntData = colDataSources.Item(TableName).Fields(rsFieldUsed.Fields("ActField").Value).Value
                    If IsNull(vntData) Then
                        vntData = ""
                    Else
                        If colDataSources.Item(TableName).Fields(rsFieldUsed.Fields("ActField").Value).Type = adDate Then
                            If Not IsUTCTimeZone Then
                                vntData = UTCDateToLocal(vntData, colUTCTimeDiff, rsSeason)
                            End If
                        End If
                        
                        vntData = GetFormattedData(vntData, rsFieldUsed.Fields("DataFormat").Value)
                    End If
                End If
                
                If i <> intIndex Then rsFieldUsed.MoveNext
            Next i
            
            If strCurrentSortCol = rsFieldUsed.Fields("LogField").Value Then
                OldValue = tabFlightList.GetFieldValue(LineNo, strCurrentSortCol)
                NewValue = vntData
            End If
            'set the value, but don't sort first
            tabFlightList.SetFieldValues LineNo, rsFieldUsed.Fields("LogField").Value, vntData
            
            rsFieldUsed.MoveNext
        Loop
        
        'sort the record if neccessary
        Call SetLineColor(LineNo, IsCancelled)
        SetAndSortTabFieldValue LineNo, lngCurrentSortCol, NewValue, OldValue, blnAlwaysSort, True, False
    End If
    
    tabFlightList.Refresh
End Sub

Private Function GetFormattedData(ByVal ActValue As Variant, ByVal DataFormat As String) As String
    Dim strValue As String
    Dim dtmBaseDate As Date
    Dim vntDataFormat As Variant
    Dim strDataFormat As String
    Dim vntTableField As Variant
    Dim strTable As String
    Dim strField As String
    
    strValue = Trim(NVL(ActValue, ""))
    If Trim(DataFormat) <> "" Then
        If InStr(DataFormat, "|") > 0 Then
            vntDataFormat = Split(DataFormat, "|")
            strDataFormat = vntDataFormat(0)
            vntTableField = Split(vntDataFormat(1), ".")
            strTable = vntTableField(0)
            strField = vntTableField(1)
            
            If colDataSources.Exists(strTable) Then
                If colDataSources.Item(strTable).Fields(strField).Type = adDate Then
                    dtmBaseDate = colDataSources.Item(strTable).Fields(strField).Value
                    If Not IsUTCTimeZone Then
                        dtmBaseDate = UTCDateToLocal(dtmBaseDate, colUTCTimeDiff, rsSeason)
                    End If
                    
                    strValue = FullDateTimeToShortTime(dtmBaseDate, NVL(ActValue, CDate(0)))
                End If
            End If
        Else
            strValue = Format(strValue, DataFormat)
        End If
    End If
    
    GetFormattedData = strValue
End Function

Private Sub ChangeWindowOnTop(ByVal Value As Integer)
    'MyMainForm.chkRightButtons(0).Value = Value
    pblnIsOnTop = (Value = vbChecked)
    SetFormOnTop Me, pblnIsOnTop
End Sub

Private Sub ConfigureMainButtons()
    Dim blnShowFIPSReportButton As Boolean
    
    blnShowFIPSReportButton = (GetConfigEntry(colConfigs, MyMainForm.AppType, "SHOW_FIPS_REPORT_BUTTON", "NO") = "YES")
    If blnShowFIPSReportButton Then
        MainButtons.LoadExtraButton "FIPS Report", "fipsreport"
    End If
End Sub

Public Sub DisplayFlightList(ViewParamCollection As CollectionExtended)
    Dim strValue As String
    Dim intColIndex As Integer
    
    tabFlightList.ResetContent
    
    If Not (rsAft Is Nothing) Then
        Do While Not rsAft.EOF
            If ValidateDataWithCurrentView(rsAft, ViewParamCollection) Then
                rsDeicing.Find "RURN = " & rsAft.Fields("URNO").Value, , , 1
                
                strValue = GetTextLine(colDataSources, tabFlightList.Tag, strDataFormat)
            
                tabFlightList.InsertTextLine strValue, False
                Call SetLineColor(tabFlightList.GetLineCount - 1, rsAft.Fields("FTYP").Value)
            End If
            
            rsAft.MoveNext
        Loop
    End If
    
    intColIndex = GetRealItemNo(tabFlightList.LogicalFieldList, "SORT")
    If intColIndex >= 0 Then
        Call SortRecordByColumnNo(intColIndex, True)
    End If
    tabFlightList.Refresh
End Sub

Private Function GetTextLine(colDataSources As CollectionExtended, ByVal ActFields As String, _
ByVal DataFormat As String) As String   ', _
ByVal StdtTable As String, ByVal StdtField As String) As String
    Dim vntActFields As Variant
    Dim vntActField As Variant
    Dim vntActFieldOptions As Variant
    Dim vntActFieldOption As Variant
    Dim vntDataFormat As Variant
    Dim vntTableField As Variant
    Dim strTable As String
    Dim strField As String
    Dim vntValue As Variant
    Dim strReturn As String
    Dim i As Integer
    Dim dtmDate As Date
    
    strReturn = ""
    vntActFields = Split(ActFields, ",")
    vntDataFormat = Split(DataFormat, ",")
    i = 0
    For Each vntActField In vntActFields
        vntActFieldOptions = Split(vntActField, "|")
        For Each vntActFieldOption In vntActFieldOptions
            vntTableField = Split(vntActFieldOption, ".")
            strTable = vntTableField(0)
            strField = vntTableField(1)
            
            vntValue = ""
            With colDataSources.Item(strTable)
                If Not (.BOF Or .EOF) Then
                    If Not IsNull(.Fields(strField).Value) Then
                        If .Fields(strField).Type = adDate Then
                            vntValue = .Fields(strField).Value
                            If Not IsUTCTimeZone Then
                                vntValue = UTCDateToLocal(vntValue, colUTCTimeDiff, rsSeason)
                            End If
                        Else
                            vntValue = NVL(.Fields(strField).Value, "")
                        End If
                    
                        vntValue = GetFormattedData(vntValue, vntDataFormat(i))
                    End If
                End If
            End With
            
            If vntValue <> "" Then Exit For
        Next vntActFieldOption
        
        strReturn = strReturn & "," & vntValue
        i = i + 1
    Next vntActField
    strReturn = Mid(strReturn, 2)
    
    GetTextLine = strReturn
End Function

Private Sub Form_Activate()
    Static IsActivated As Boolean
    
    If Not IsActivated Then
        SetFormOnTop Me, pblnIsOnTop
        IsActivated = True
    End If
End Sub

Private Sub Form_Load()
    Dim FormToShow As Form
    
    MyMainForm.Icon = Me.Icon
    
    Set rsAirlines = colDataSources.Item("ALTTAB")
    Set rsHandlingRels = colDataSources.Item("HAITAB")
    Set rsView = colDataSources.Item("VCDTAB")
    Set rsAircraftPos = colDataSources.Item("PSTTAB")
    Set rsAircraftPosGroups = colDataSources.Item("GRNTAB")
    Set rsAircraftPosGroupRels = colDataSources.Item("GRMTAB")
    If colDataSources.Exists("SEATAB") Then
        Set rsSeason = colDataSources.Item("SEATAB")
    End If
    
    Call AddDefaultView
    
    blnOpenFileAfterExporting = (GetConfigEntry(colConfigs, MyMainForm.AppType, "OPEN_FILE_AFTER_EXPORTING", "YES") = "YES")
    blnAlwaysSort = (GetConfigEntry(colConfigs, MyMainForm.AppType, "ALWAYS_SORT", "NO") = "YES")
    intRelPrdBuffer = Val(GetConfigEntry(colConfigs, MyMainForm.AppType, "LOAD_REL_BUFFER", "0"))
    intRelRefreshIntv = Val(GetConfigEntry(colConfigs, MyMainForm.AppType, "LOAD_REL_REFRESH_MIN", "0"))
    intRelReloadIntv = Val(GetConfigEntry(colConfigs, MyMainForm.AppType, "LOAD_REL_RELOAD_MIN", "0"))
    
    If pblnFlightListMainButtons Then
        Load MainButtons
        
        Call ConfigureMainButtons
        
        Set FormToShow = MainButtons
        Set MainButtons.ContainerForm = Me
        Call ChangeFormBorderStyle(FormToShow, vbBSNone)
        FormWithinForm picMainButtons, FormToShow
        'FormToShow.Move 0, 0, picMainButtons.Width, picMainButtons.Height
        FormToShow.Show
        
        picMainButtons.Visible = True
    Else
        picMainButtons.Visible = False
    End If

    Call PrepareTab
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    On Error Resume Next

    Unload MyMainForm
    If pblnCanExitApp Then
        If UnloadMode = 0 Then
            If pblnFlightListMainButtons Then
                Unload MainButtons
            End If
        End If
    Else
        Cancel = True
    End If
End Sub

Private Sub Form_Resize()
    Dim sngTop As Single
    Dim intAdjustment As Integer

    On Error Resume Next
    
    If Me.WindowState <> vbMinimized Then
        sngTop = 0
        If picMainButtons.Visible Then sngTop = sngTop + picMainButtons.Height
        
        intAdjustment = 0
        If pblnIsStandAlone And Not pblnFlightListWindow Then
            intAdjustment = 50
        End If
        
        picMainButtons.Width = Me.ScaleWidth
        tabFlightList.Move 0, sngTop, Me.ScaleWidth - intAdjustment, Me.ScaleHeight - sngTop - intAdjustment
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set ExcelExport = Nothing
    Set PDFExport = Nothing
End Sub

Private Sub picMainButtons_Resize()
    If pblnFlightListMainButtons Then
        MainButtons.Move 0, 0, picMainButtons.Width, picMainButtons.Height
    End If
End Sub

Private Sub tabFlightList_BoolPropertyChanged(ByVal LineNo As Long, ByVal ColNo As Long, ByVal NewValue As Boolean)
    Dim strField As String
    Dim strValue As String
    Dim strOldValue As String
    
    strField = GetRealItem(tabFlightList.LogicalFieldList, ColNo, ",")
    strValue = IIf(NewValue, "Y", "N")
    strOldValue = IIf(NewValue, "N", "Y")
        
    Call UpdateDeicing(LineNo, strField, strValue)
    If strField <> "ACDC" Then
        Call SetLineColor(LineNo)
        Call SetAndSortTabFieldValue(LineNo, strField, strValue, strOldValue, blnAlwaysSort, True)
    End If
End Sub

Private Sub tabFlightList_HitKeyOnLine(ByVal Key As Integer, ByVal LineNo As Long)
    Dim lngLineNo As Long
    Dim lngColNo As Long
    Dim blnEditingDone As Boolean
    Dim blnMoveEdit As Boolean

    Select Case Key
        Case vbKeyReturn, vbKeyTab, vbKeyUp, vbKeyDown
            If IsOnEdit Then
                blnEditingDone = True
                blnMoveEdit = False
            
                lngLineNo = lngCurrLine
                lngColNo = lngCurrCol
                Select Case Key
                    Case vbKeyUp
                        lngLineNo = lngLineNo - 1
                        blnEditingDone = (lngLineNo >= 0)
                        blnMoveEdit = True
                    Case vbKeyDown
                        lngLineNo = lngLineNo + 1
                        blnEditingDone = (lngLineNo < tabFlightList.GetLineCount)
                        blnMoveEdit = True
                    Case vbKeyTab
                        Do
                            lngColNo = lngColNo + 1
                            If lngColNo >= tabFlightList.GetColumnCount Then
                                lngColNo = 0
                                lngLineNo = lngLineNo + 1
                                If lngLineNo >= tabFlightList.GetLineCount Then
                                    Exit Do
                                End If
                            End If
                        Loop Until IsColumnEditable(lngColNo)
                        blnEditingDone = True
                        blnMoveEdit = (lngLineNo < tabFlightList.GetLineCount) And _
                            (lngColNo < tabFlightList.GetColumnCount)
                End Select
                
                If blnEditingDone Then
                    Call ValidateAndSaveUserEntry(lngCurrLine, lngCurrCol)
                    
                    If tabFlightList.GetColumnValue(lngCurrLine, lngAftUrnoColNo) <> strEditedAftUrno Then
                        lngLineNo = tabFlightList.GetLinesByColumnValue(lngAftUrnoColNo, strEditedAftUrno, 0)
                        Select Case Key
                            Case vbKeyUp
                                lngLineNo = lngLineNo - 1
                                blnMoveEdit = (lngLineNo >= 0)
                            Case vbKeyDown
                                lngLineNo = lngLineNo + 1
                                blnMoveEdit = (lngLineNo < tabFlightList.GetLineCount)
                            Case vbKeyTab
                                lngColNo = lngCurrCol
                                Do
                                    lngColNo = lngColNo + 1
                                    If lngColNo >= tabFlightList.GetColumnCount Then
                                        lngColNo = 0
                                        lngLineNo = lngLineNo + 1
                                        If lngLineNo >= tabFlightList.GetLineCount Then
                                            Exit Do
                                        End If
                                    End If
                                Loop Until IsColumnEditable(lngColNo)
                                blnMoveEdit = (lngLineNo < tabFlightList.GetLineCount) And _
                                    (lngColNo < tabFlightList.GetColumnCount)
                        End Select
                    End If
                    
                    If blnMoveEdit Then
                        tmrTimer.Tag = CStr(lngLineNo) & "," & CStr(lngColNo)
                        tmrTimer.Enabled = True
                    End If
                End If
            End If
    End Select
End Sub

Private Sub tabFlightList_LostFocus()
    If IsOnEdit Then
        Call ValidateAndSaveUserEntry(lngCurrLine, lngCurrCol)
    End If
End Sub

Private Sub tabFlightList_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Select Case LineNo
        Case -2
            ColNo = GetRealItemNo(tabFlightList.LogicalFieldList, "SORT")
            If ColNo >= 0 Then
                Call SortRecordByColumnNo(ColNo)
            End If
        Case -1
            Call SortRecordByColumnNo(ColNo)
        Case Else
            If (LineNo <> lngCurrLine Or ColNo <> lngCurrCol) And IsOnEdit Then
                Call ValidateAndSaveUserEntry(lngCurrLine, lngCurrCol)
            End If
            tmrTimer.Tag = LineNo & "," & ColNo
            tmrTimer.Enabled = True
    End Select
End Sub

Private Sub StartEditMode(ByVal LineNo As Long, ByVal ColNo As Long)
    If IsColumnEditable(ColNo) Then
        lngCurrLine = LineNo
        lngCurrCol = ColNo

        'Save the old value and Aft Urno
        OldValue = tabFlightList.GetColumnValue(LineNo, ColNo)
        strEditedAftUrno = tabFlightList.GetColumnValue(LineNo, lngAftUrnoColNo)
        
        'select the line
        If tabFlightList.GetCurrentSelected <> LineNo Then
            tabFlightList.SetCurrentSelection LineNo
        End If

        tabFlightList.EnableInlineEdit True
        tabFlightList.SetInplaceEdit lngCurrLine, lngCurrCol, False
        IsOnEdit = True
    End If
End Sub

Private Sub tabFlightList_SendRButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim strField As String
    Dim strValue As String
    Dim strSavedValue As String
    Dim dtmNow As Date
    Dim dtmSTDT As Date
    
    strField = GetRealItem(tabFlightList.LogicalFieldList, ColNo, ",")
    Select Case strField
        Case "DIAB", "DIAE"
            If IsOnEdit Then
                Call ValidateAndSaveUserEntry(lngCurrLine, lngCurrCol)
            End If
        
            dtmNow = Now
            If IsUTCTimeZone Then
                dtmNow = LocalDateToUTC(dtmNow, colUTCTimeDiff, rsSeason)
            End If
            
            'simulate user entry
            OldValue = tabFlightList.GetFieldValue(LineNo, strField)
            
            rsAft.Find "URNO = " & tabFlightList.GetColumnValue(LineNo, lngAftUrnoColNo), , , 1
            If Not rsAft.EOF Then
                dtmSTDT = rsAft.Fields("STOD").Value
                If Not IsUTCTimeZone Then
                    dtmSTDT = UTCDateToLocal(dtmSTDT, colUTCTimeDiff, rsSeason)
                End If
                dtmSTDT = DateTimeToDate(dtmSTDT)
                strValue = FullDateTimeToShortTime(dtmSTDT, dtmNow)
                
                tabFlightList.SetFieldValues LineNo, strField, strValue
                
                Call ValidateAndSaveUserEntry(LineNo, ColNo)
            End If
    End Select
End Sub

Private Sub tmrRefresh_Timer()
    Dim intMin As Integer
    Dim dtmPrevStartDate As Date
    Dim dtmStartDate As Date
    Dim dtmPrevEndDate As Date
    Dim dtmEndDate As Date
    Dim strLine As String
    
    intMin = Val(tmrRefresh.Tag) + 1
    tmrRefresh.Tag = intMin
    If intMin = intRelRefreshIntv Then
        intMin = 0
        tmrRefresh.Tag = intMin
        
        dtmPrevStartDate = colViewParams.Item("UTCSTARTDATE")
        dtmStartDate = DateAdd("n", intRelRefreshIntv, dtmPrevStartDate)
        
        dtmPrevEndDate = colViewParams.Item("UTCENDDATE")
        dtmEndDate = DateAdd("n", intRelRefreshIntv, dtmPrevEndDate)
        
        'If dtmEndDate >= dtmPrevEndDate Then
            If Not (rsAft.BOF And rsAft.EOF) Then
                rsAft.MoveFirst
            End If
            
            Do While Not rsAft.EOF
                If (rsAft.Fields("TIFD").Value < dtmStartDate) Or _
                (rsAft.Fields("STOD").Value < dtmStartDate) Then

                    strLine = tabFlightList.GetLinesByColumnValue(lngAftUrnoColNo, rsAft.Fields("URNO").Value, 0)
                    If strLine <> "" Then 'Found
                        'Delete the line
                        tabFlightList.DeleteLine Val(strLine)
                    End If

                    'Delete the record
                    rsAft.Delete

                ElseIf (rsAft.Fields("TIFD").Value >= dtmPrevEndDate And rsAft.Fields("TIFD").Value <= dtmEndDate) Or _
                (rsAft.Fields("STOD").Value >= dtmPrevEndDate And rsAft.Fields("STOD").Value <= dtmEndDate) Then

                    If ValidateDataWithCurrentView(rsAft, colViewParams) Then
                        strLine = tabFlightList.GetLinesByColumnValue(lngAftUrnoColNo, rsAft.Fields("URNO").Value, 0)
                        If strLine = "" Then 'Not found
                            'simulate broadcast
                            Call RefreshDisplay("URT", "AFTTAB", rsAft.Fields("URNO").Value)
                        End If
                    End If

                End If
                
                rsAft.MoveNext
            Loop
            
'            rsAft.Filter = "(TIFD >= #" & Format(dtmStartDate, "YYYY/MM/DD hh:mm:ss") & "# AND TIFD <= #" & Format(dtmEndDate, "YYYY/MM/DD hh:mm:ss") & "#) OR " & _
'                "(STOD >= #" & Format(dtmStartDate, "YYYY/MM/DD hh:mm:ss") & "# AND STOD <= #" & Format(dtmEndDate, "YYYY/MM/DD hh:mm:ss") & "#)"
'
'            Do While Not rsAft.EOF
'                If ValidateDataWithCurrentView(rsAft, colViewParams) Then
'                    strLine = tabFlightList.GetLinesByColumnValue(lngAftUrnoColNo, rsAft.Fields("URNO").Value, 0)
'                    If strLine = "" Then 'Not found
'                        'simulate broadcast
'                        Call RefreshDisplay("URT", "AFTTAB", rsAft.Fields("URNO").Value)
'                    End If
'                End If
'
'                rsAft.MoveNext
'            Loop
'
'            rsAft.Filter = adFilterNone
            
            colViewParams.Remove "UTCSTARTDATE"
            colViewParams.Add dtmStartDate, "UTCSTARTDATE"
            
            colViewParams.Remove "UTCENDDATE"
            colViewParams.Add dtmEndDate, "UTCENDDATE"
        'End If
    End If
End Sub

Private Sub tmrReload_Timer()
    Dim intMin As Integer
    Dim dtmStartDate As Date
    Dim dtmEndDate As Date
    Dim strStartDate As String
    Dim strEndDate As String
    Dim strWhere As String
    Dim colLocalParams As New CollectionExtended
    
    intMin = Val(tmrReload.Tag) + 1
    tmrReload.Tag = intMin
    If intMin = (intRelPrdBuffer * 60) Then
        intMin = 0
        tmrReload.Tag = intMin
    
        dtmStartDate = DateAdd("s", 1, colViewParams.Item("BUFFERENDDATE"))
        dtmEndDate = DateAdd("h", intRelPrdBuffer, dtmStartDate)
        
        strStartDate = Format(dtmStartDate, "YYYYMMDDhhmmss")
        strEndDate = Format(dtmEndDate, "YYYYMMDDhhmmss")
        
        strWhere = colViewParams.Item("SQLWHERECLAUSE")
        strWhere = Replace(strWhere, "[:UTCSTARTDATE]", strStartDate)
        strWhere = Replace(strWhere, "[:BUFFERENDDATE]", strEndDate)
        
        colLocalParams.Add "URNO", "OrderByClause"
        colLocalParams.Add strWhere, "WhereClause"
        
        pstrAftUrnoList = AddDataSource(MyMainForm.AppType, colDataSources, "AFTTAB", colLocalParams, "URNO", , , , False)
            
        Set rsAft = colDataSources.Item("AFTTAB")
        If pstrAftUrnoList = "" Then
            pstrAftUrnoList = "0"
        End If
        
        Call ReadDataSource(pstrAftUrnoList)
            
        If colDataSources.Exists("ICETAB") Then
            Set rsDeicing = colDataSources.Item("ICETAB")
        End If
        
        colViewParams.Remove "BUFFERENDDATE"
        colViewParams.Add dtmEndDate, "BUFFERENDDATE"
    End If
End Sub

Private Sub tmrTimer_Timer()
    Dim vntTag As Variant
    Dim LineNo As Long
    Dim ColNo As Long
    
    tmrTimer.Enabled = False
    
    vntTag = Split(tmrTimer.Tag, ",")
    LineNo = vntTag(0)
    If LineNo = -1 Then LineNo = tabFlightList.GetCurrentSelected
    ColNo = vntTag(1)
    If ColNo = -1 Then ColNo = lngCurrCol
    
    Call StartEditMode(LineNo, ColNo)
End Sub
