VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form Deicing 
   Caption         =   "De-icing"
   ClientHeight    =   5865
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12780
   Icon            =   "Deicing.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5865
   ScaleWidth      =   12780
   StartUpPosition =   3  'Windows Default
   Begin MSComDlg.CommonDialog cdgSave 
      Left            =   6360
      Top             =   5280
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.PictureBox picMainButtons 
      Height          =   435
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   10455
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      Width           =   10515
      Begin VB.CommandButton cmdMainButtons 
         Caption         =   "View"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   0
         Left            =   0
         TabIndex        =   10
         Top             =   0
         Width           =   855
      End
      Begin VB.ComboBox cboView 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   960
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   30
         Width           =   2535
      End
      Begin VB.CommandButton cmdMainButtons 
         Caption         =   "Search"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   1
         Left            =   3600
         TabIndex        =   8
         Top             =   0
         Width           =   975
      End
      Begin VB.CommandButton cmdMainButtons 
         Caption         =   "Print"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   2
         Left            =   4680
         TabIndex        =   7
         Top             =   0
         Width           =   975
      End
      Begin VB.CommandButton cmdMainButtons 
         Caption         =   "Excel"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   3
         Left            =   5640
         TabIndex        =   6
         Top             =   0
         Width           =   975
      End
      Begin VB.Frame fraTimeZone 
         Height          =   375
         Left            =   6720
         TabIndex        =   3
         Top             =   0
         Width           =   1455
         Begin VB.OptionButton optTimeZone 
            Caption         =   "UTC"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   5
            Top             =   0
            Value           =   -1  'True
            Width           =   735
         End
         Begin VB.OptionButton optTimeZone 
            Caption         =   "LOCAL"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   1
            Left            =   720
            Style           =   1  'Graphical
            TabIndex        =   4
            Top             =   0
            Width           =   735
         End
      End
      Begin VB.CheckBox chkOnTop 
         Caption         =   "On Top"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   8280
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   0
         Width           =   975
      End
   End
   Begin TABLib.TAB tabFlightList 
      Height          =   5295
      Left            =   0
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   480
      Width           =   4455
      _Version        =   65536
      _ExtentX        =   7858
      _ExtentY        =   9340
      _StockProps     =   64
   End
End
Attribute VB_Name = "Deicing"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private PDFExport As ActiveReportsPDFExport.ARExportPDF
Private ExcelExport As ActiveReportsExcelExport.ARExportExcel

Private rsAft As ADODB.Recordset
Private rsDeicing As ADODB.Recordset
Private rsView As ADODB.Recordset
Private rsAirlines As ADODB.Recordset
Private rsHandlingRels As ADODB.Recordset
Private rsAircraftPos As ADODB.Recordset
Private rsAircraftPosGroups As ADODB.Recordset
Private rsAircraftPosGroupRels As ADODB.Recordset
Private rsSeason As ADODB.Recordset

Private rsFieldUsed As ADODB.Recordset
Private colFieldUsed As CollectionExtended

Private strDataFormat As String
Private strBoolColumns As String

Private lngAftUrnoColNo As Long
Private lngIceUrnoColNo As Long
Private strAftUrnoCol As String
Private strIceUrnoCol As String

Private strEditedAftUrno As String

Private colViewParams As CollectionExtended
Private colSearchParams As CollectionExtended

Private lngCurrLine As Long
Private lngCurrCol As Long
    
Private blnOpenFileAfterExporting  As Boolean

'-------------------
Private m_strRealReadOnlyColumns As String
'-------------------

Public Sub UpdateView(ByVal CedaCmd As String, ByVal Urno As String, Optional ByVal ChangedFields As String)
'-------------------
    Call UpdateViewCombo(cboView, CedaCmd, Urno, ChangedFields)
'-------------------
End Sub

Public Sub LoadData(ByVal LoadParam As Variant)
    Dim vntDataSources As Variant
    Dim vntDataSource As Variant
    Dim strWhere As String
    Dim colLocalParams As New CollectionExtended
    
    Screen.MousePointer = vbHourglass
    
    If IsObject(LoadParam) Then
        If TypeOf LoadParam Is CollectionExtended Then
            Set colViewParams = LoadParam
        Else
            Set colViewParams = GetDefaultParamCollection()
        End If
    Else
        Set colViewParams = GetDefaultParamCollection()
    End If
    
    If pstrDataSource = "" Then
        pstrDataSource = GetConfigEntry(colConfigs, "DE-ICING", "DATA_SOURCE", "AFTTAB")
    End If
    If InStr(pstrDataSource & ",", "AFTTAB,") > 0 Then
        strWhere = BuildWhereClause(colViewParams)
                
        colLocalParams.Add "URNO", "OrderByClause"
        colLocalParams.Add strWhere, "WhereClause"
        
        pstrAftUrnoList = AddDataSource("DE-ICING", colDataSources, "AFTTAB", colLocalParams, "URNO")
        
        Set rsAft = colDataSources.Item("AFTTAB")
        If Not (rsAft Is Nothing) Then
            If Not (rsAft.BOF And rsAft.EOF) Then
                Call ReadDataSource(pstrAftUrnoList)
            End If
        End If
        
        If colDataSources.Exists("ICETAB") Then
            Set rsDeicing = colDataSources.Item("ICETAB")
        End If
    End If
    
    Call DisplayFlightList(colViewParams)
    
    Screen.MousePointer = vbDefault
End Sub

Public Sub ChangeRowSelection(ByVal Key As String)
    Dim strRow As String
    
    strRow = tabFlightList.GetLinesByColumnValue(lngAftUrnoColNo, Key, 0)
    If strRow <> "" Then
        tabFlightList.SetCurrentSelection CLng(strRow)
        tabFlightList.OnVScrollTo CLng(strRow)
    End If
End Sub

Private Sub ChangeTimeZone(ByVal UTCTime As Boolean)
    Dim blnProceed As Boolean
    Dim i As Long, j As Integer
    Dim vntActFields As Variant
    Dim vntDataFormat As Variant
    Dim vntActFieldOptions As Variant
    Dim vntActFieldOption As Variant
    Dim vntTableField As Variant
    Dim strTable As String
    Dim strField As String
    Dim vntValue As Variant
    Dim blnUpdate As Boolean
        
    blnProceed = (IsUTCTimeZone And (Not UTCTime)) Or (Not (IsUTCTimeZone) And UTCTime)
    
    If blnProceed Then
        IsUTCTimeZone = UTCTime
    
        vntActFields = Split(tabFlightList.Tag, ",")
        vntDataFormat = Split(strDataFormat, ",")
    
        For i = 0 To tabFlightList.GetLineCount - 1
            rsAft.Find "URNO = " & tabFlightList.GetColumnValue(i, lngAftUrnoColNo), , , 1
            If Not rsAft.EOF Then
                rsDeicing.Find "URNO = " & Val(tabFlightList.GetColumnValue(i, lngIceUrnoColNo)), , , 1
                
                For j = 0 To tabFlightList.GetColumnCount - 1
                    blnUpdate = False
                    vntActFieldOptions = Split(vntActFields(j), "|")
                    For Each vntActFieldOption In vntActFieldOptions
                        vntTableField = Split(vntActFieldOption, ".")
                        strTable = vntTableField(0)
                        strField = vntTableField(1)
                
                        vntValue = ""
                        With colDataSources.Item(strTable)
                            If Not .EOF Then
                                If Not IsNull(.Fields(strField).Value) Then
                                    If .Fields(strField).Type = adDate Then
                                        vntValue = .Fields(strField).Value
                                        If Not IsUTCTimeZone Then
                                            vntValue = UTCDateToLocal(vntValue, colUTCTimeDiff, rsSeason)
                                        End If
                                        blnUpdate = True
                                    End If
                                    
                                    If blnUpdate Then
                                        vntValue = GetFormattedData(vntValue, vntDataFormat(j))
                                    End If
                                End If
                            End If
                        End With
                        
                        If vntValue <> "" Then Exit For
                    Next vntActFieldOption
                
                    If blnUpdate Then
                        tabFlightList.SetColumnValue i, j, vntValue
                    End If
                Next j
            End If
        Next i
        tabFlightList.Refresh
        
        IsUTCTimeZone = UTCTime
    End If
End Sub

Private Function BuildWhereClause(ViewParamCollection As CollectionExtended) As String
    Dim dtmNow As Date
    Dim dtmStartDate As Date
    Dim dtmEndDate As Date
    Dim strStartDate As String
    Dim strEndDate As String
    Dim strStartTime As String
    Dim strEndTime As String
    Dim strRelStart As String
    Dim strRelEnd As String
    Dim vntDates As Variant
    Dim strParamVal As String
    Dim strWhere As String
    Dim strTemp As String
    Dim vntParamVal As Variant
    Dim i As Integer
    Dim strDays As String
    
    dtmNow = Now
    If IsUTCTimeZone Then
        dtmNow = LocalDateToUTC(dtmNow, colUTCTimeDiff, rsSeason)
    End If
    strRelStart = ViewParamCollection.Item("RELPRDSTART")
    strRelEnd = ViewParamCollection.Item("RELPRDEND")
        
    strStartDate = ViewParamCollection.Item("STARTDATE")
    strStartTime = ViewParamCollection.Item("STARTTIME")
    strEndDate = ViewParamCollection.Item("ENDDATE")
    strEndTime = ViewParamCollection.Item("ENDTIME")
    
    vntDates = GetParamDateArray(strStartDate, strStartTime, strEndDate, strEndTime, strRelStart, strRelEnd, dtmNow)
    dtmStartDate = vntDates(0)
    dtmEndDate = vntDates(1)
    
    'check DAYS
    If IsUTCTimeZone Then
        strParamVal = Trim(ViewParamCollection.Item("DAYS"))
        If strParamVal <> "" And Len(strParamVal) < 7 Then
            strDays = ""
            For i = 1 To Len(strParamVal)
                strDays = strDays & ",'" & Mid(strParamVal, i, 1) & "'"
            Next i
            If strDays <> "" Then strDays = Mid(strDays, 2)
            strWhere = strWhere & "AND DOOD IN (" & strDays & ")"
        End If
    End If
    
    'check ALC2
    strTemp = BuildSimpleWhereClause(ViewParamCollection, "ALC2", "ALC2")
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
    
    'check FLTN
    strTemp = BuildSimpleWhereClause(ViewParamCollection, "FLTN", "FLTN")
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
    
    'check FLNS
    strTemp = BuildSimpleWhereClause(ViewParamCollection, "FLNS", "FLNS")
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
    
    'check AIRLINE
    strTemp = BuildExtendedWhereClause(ViewParamCollection, "AIRLINE", _
        Array("ALC2", "ALC3"), Array(2, 3), True)
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
        
    'check CSGN
    strTemp = BuildSimpleWhereClause(ViewParamCollection, "CSGN", "CSGN")
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
    
    'check FLTI
    strTemp = BuildSimpleWhereClause(ViewParamCollection, "FLTI", "FLTI")
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
        
    'check REGN
    strTemp = BuildSimpleWhereClause(ViewParamCollection, "REGN", "REGN", True)
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
    
    'check ACTY
    strTemp = BuildExtendedWhereClause(ViewParamCollection, "ACTY", _
        Array("ACT3", "ACT5"), Array(3, 5), True)
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
    
    'check AIRP
    strTemp = BuildExtendedWhereClause(ViewParamCollection, "AIRP", _
        Array("DES3", "DES4"), Array(3, 4), True)
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
    
    'check VIAP
    strTemp = BuildExtendedWhereClause(ViewParamCollection, "VIAP", _
        Array("VIA3", "VIA4"), Array(3, 4), True)
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
    
    'check NATURE
    strTemp = BuildSimpleWhereClause(ViewParamCollection, "NATURE", "TTYP", True)
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
    
    'check STEV
    strTemp = BuildSimpleWhereClause(ViewParamCollection, "STEV", "STEV", True)
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
    
    'check HDLAGENT
    strParamVal = ViewParamCollection.Item("HDLAGENT")
    If Trim(strParamVal) <> "" Then
        vntParamVal = Split(strParamVal, ",")
        strParamVal = ""
        For i = 0 To UBound(vntParamVal)
            rsHandlingRels.Filter = "HSNA = '" & vntParamVal(i) & "'"
            Do While Not rsHandlingRels.EOF
                rsAirlines.Find "URNO = " & rsHandlingRels.Fields("ALTU").Value, , , 1
                If Not rsAirlines.EOF Then
                    strParamVal = strParamVal & ",'" & rsAirlines.Fields("ALC2").Value & "'"
                End If
                
                rsHandlingRels.MoveNext
            Loop
            rsHandlingRels.Filter = adFilterNone
        Next i
        If strParamVal <> "" Then
            strParamVal = Mid(strParamVal, 2)
            strWhere = strWhere & " AND ALC2 IN (" & strParamVal & ")"
        End If
    End If
    
    'check HTYP
    strTemp = BuildSimpleWhereClause(ViewParamCollection, "HTYP", "HTYP", True)
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
    
    'check PSTN/PSGR
    'check PSTN
    strParamVal = ViewParamCollection.Item("PSTN")
    If strParamVal <> "" Then
        strParamVal = ",'" & Replace(strParamVal, ",", "','") & "'"
    End If
    'check PSGR
    strTemp = ViewParamCollection.Item("PSGR")
    If strTemp <> "" Then
        vntParamVal = Split(strTemp, ",")
        For i = 0 To UBound(vntParamVal)
            rsAircraftPosGroups.Find "GRPN = '" & vntParamVal(i) & "'", , , 1
            If Not rsAircraftPosGroups.EOF Then
                rsAircraftPosGroupRels.Filter = "GURN = " & rsAircraftPosGroups.Fields("URNO").Value
                Do While Not rsAircraftPosGroupRels.EOF
                    rsAircraftPos.Find "URNO = " & rsAircraftPosGroupRels.Fields("VALU").Value, , , 1
                    If Not rsAircraftPos.EOF Then
                        strParamVal = strParamVal & ",'" & rsAircraftPos.Fields("PNAM").Value & "'"
                    End If
                    
                    rsAircraftPosGroupRels.MoveNext
                Loop
                rsAircraftPosGroupRels.Filter = adFilterNone
            End If
        Next i
    End If
    If strParamVal <> "" Then
        strParamVal = Mid(strParamVal, 2)
        strWhere = strWhere & " AND PSTD IN (" & strParamVal & ")"
    End If
    
    strTemp = BuildSimpleWhereClause(ViewParamCollection, "PSTN", "PSTD", True)
    If strTemp <> "" Then strWhere = strWhere & " " & strTemp
    
    'check FTYP
    strParamVal = GetParamValList(ViewParamCollection, Array("O", "S", "X", "N", "D", "R", "P", "G", "T"), _
        Array("O,Z,B", "S", "X", "N", "D", "R", "G", " ", "T"))
    If strParamVal <> "" Then
        strParamVal = "'" & Replace(strParamVal, ",", "','") & "'"
        strWhere = strWhere & " AND FTYP IN (" & strParamVal & ")"
    End If
    
    If Not IsUTCTimeZone Then
        dtmStartDate = LocalDateToUTC(dtmStartDate, colUTCTimeDiff, rsSeason)
        dtmEndDate = LocalDateToUTC(dtmEndDate, colUTCTimeDiff, rsSeason)
    End If
    strStartDate = Format(dtmStartDate, "YYYYMMDDhhmmss")
    strEndDate = Format(dtmEndDate, "YYYYMMDDhhmmss")
    
    'add to paramcollection -> UTC START/END Date
    ViewParamCollection.Add dtmStartDate, "UTCSTARTDATE"
    ViewParamCollection.Add dtmEndDate, "UTCENDDATE"
            
    BuildWhereClause = "((TIFD BETWEEN '" & strStartDate & "' AND '" & strEndDate & "') OR " & _
        "(STOD BETWEEN '" & strStartDate & "' AND '" & strEndDate & "')) AND " & _
        "ADID = 'D' " & strWhere
        '"AIRB = ' ' AND ADID = 'D' " & strWhere
End Function

Private Function GetDefaultParamCollection() As CollectionExtended
    Dim strRelBefore As String
    Dim strRelAfter As String
    
    rsView.Find "URNO = -1", , , 1
    If Not rsView.EOF Then
        strRelBefore = GetConfigEntry(colConfigs, "DE-ICING", "LOAD_REL_BEFORE", "2")
        strRelAfter = GetConfigEntry(colConfigs, "DE-ICING", "LOAD_REL_AFTER", "2")
            
        Set GetDefaultParamCollection = New CollectionExtended
        GetDefaultParamCollection.Add "<Default>", "VIEW"
        GetDefaultParamCollection.Add strRelBefore, "RELPRDSTART"
        GetDefaultParamCollection.Add strRelAfter, "RELPRDEND"
        GetDefaultParamCollection.Add "1", "DEP"
        GetDefaultParamCollection.Add "1", "O"
        GetDefaultParamCollection.Add "1", "X"
    Else
        Set GetDefaultParamCollection = GetViewParamCollection(rsView.Fields("TEXT").Value)
    End If
End Function

Private Sub AddDefaultView()
    Dim AppName As String
    Dim strRelBefore As String
    Dim strRelAfter As String
    Dim blnAdd As Boolean
    
    AppName = Mid(colParameters.Item("AppName"), 2)
    AppName = Left(AppName, Len(AppName) - 1)
        
    strRelBefore = GetConfigEntry(colConfigs, "DE-ICING", "LOAD_REL_BEFORE", "2")
    strRelAfter = GetConfigEntry(colConfigs, "DE-ICING", "LOAD_REL_AFTER", "2")
    
    If (rsView.BOF And rsView.EOF) Then
        blnAdd = True
    Else
        rsView.Find "URNO = -1", , , 1
        blnAdd = rsView.EOF
    End If
    
    If blnAdd Then
        rsView.AddNew
        rsView.Fields("URNO").Value = -1
        rsView.Fields("APPN").Value = AppName
        rsView.Fields("TEXT").Value = "VIEW=<Default>;RELPRDSTART=" & strRelBefore & ";" & _
            "RELPRDEND=" & strRelAfter & ";DEP=1;O=1;X=1"
        rsView.Fields("PKNO").Value = LoginUserName
        rsView.Update
    End If
End Sub

Private Sub ShowFlightViewForm(ByVal Search As Boolean)
    Dim oViewForm As FlightView
    Dim strView As String
    Dim chk As CheckBox
    
    If Search Then
        If colDataSources.Exists("AFTTAB") Then
            If SearchViewForm Is Nothing Then
                Set SearchViewForm = New FlightView
            End If
            Set oViewForm = SearchViewForm
        End If
    Else
        Set oViewForm = FlightView
    End If
    
    If Not (oViewForm Is Nothing) Then
        oViewForm.IsSearchForm = Search
        '--Disable some controls
        For Each chk In oViewForm.chkADID
            chk.Enabled = (chk.Tag = "DEP")
        Next chk
        '---
        If Search Then
            oViewForm.chkADID(1).Value = vbChecked
        Else
            If FormIsLoaded("MainButtons") Then
            '---------------
                oViewForm.cboView.ListIndex = MainButtons.cboView.ListIndex
            '---------------
            Else
                oViewForm.cboView.ListIndex = Me.cboView.ListIndex
            End If
        End If
        If IsUTCTimeZone Then
            oViewForm.lblUTC_LT(0).Caption = "(UTC)"
            oViewForm.lblUTC_LT(1).Caption = "(Calculated in UTC times)"
        Else
            oViewForm.lblUTC_LT(0).Caption = "(LT)"
            oViewForm.lblUTC_LT(1).Caption = "(Calculated in local times)"
        End If
        oViewForm.Show vbModal
        strView = oViewForm.ViewString
        
        If strView <> "" Then
            If Search Then
                Set colSearchParams = GetViewParamCollection(strView, Search)
                Call ShowSearchList(colSearchParams)
            Else
                Set colViewParams = GetViewParamCollection(strView, Search)
                Call LoadData(colViewParams)
            End If
        End If
    End If
End Sub

Private Function ShowSearchList(colParams As CollectionExtended)
    Static oSearchListForm As Form
    Dim i As Integer
    Dim intColCount As Long
    Dim strColIndex As String
    Dim rsView As ADODB.Recordset
    
    Screen.MousePointer = vbHourglass
    
    strColIndex = ""
    intColCount = Split(tabFlightList.GetMainHeaderRanges, ",")(0)
    For i = 0 To intColCount - 1
        strColIndex = strColIndex & "," & CStr(i)
    Next i
    If strColIndex <> "" Then strColIndex = Mid(strColIndex, 2)
        
    If (oSearchListForm Is Nothing) Then
        Set oSearchListForm = SearchList
        oSearchListForm.SetParentForm Me
        oSearchListForm.SetMasterTab tabFlightList, strColIndex, lngAftUrnoColNo
    End If
    
    Set rsView = GetSearchViewRecordset(strColIndex, colParams)
    
    oSearchListForm.PopulateList rsView
    
    Screen.MousePointer = vbDefault
    
    oSearchListForm.Show vbModal
End Function

Private Sub SortRecordByColumnNo(ByVal ColNo As Long, Optional SortAscending As Variant)
    Dim lngCurrentSortCol As Long
    
    If IsMissing(SortAscending) Then
        lngCurrentSortCol = Val(tabFlightList.CurrentSortColumn)
        If ColNo = lngCurrentSortCol Then
            SortAscending = Not tabFlightList.SortOrderASC
        Else
            SortAscending = True
        End If
    End If
    
    Screen.MousePointer = vbHourglass
    tabFlightList.Sort CStr(ColNo), SortAscending, True
    Screen.MousePointer = vbDefault
End Sub

Private Function GetSearchViewRecordset(ByVal ColIndexList As String, colParams As CollectionExtended) As ADODB.Recordset
    Dim i As Integer
    
    Set GetSearchViewRecordset = New ADODB.Recordset
    GetSearchViewRecordset.Fields.Append "DATA", adVarChar, 400, adFldIsNullable
    GetSearchViewRecordset.Open
    
    For i = 0 To tabFlightList.GetLineCount - 1
        rsAft.Find "URNO = " & tabFlightList.GetColumnValue(i, lngAftUrnoColNo), , , 1
        If Not rsAft.EOF Then
            If ValidateDataWithCurrentView(rsAft, colParams) Then
                GetSearchViewRecordset.AddNew
                GetSearchViewRecordset.Fields("DATA").Value = tabFlightList.GetColumnValues(i, ColIndexList)
                GetSearchViewRecordset.Update
            End If
        End If
    Next i
End Function

Private Function GetViewParamCollection(ByVal ViewParams As String, Optional ByVal Search As Boolean = False) As CollectionExtended
    Dim vntParams As Variant
    Dim vntParam As Variant
    Dim vntParamItems As Variant
    Dim strParamKey As String
    Dim strParamValue As String
    Dim dtmDefStartDate As Date
    Dim dtmDefEndDate As Date
    Dim dtmNow As Date
    
    Set GetViewParamCollection = New CollectionExtended
    
    vntParams = Split(ViewParams, ";")
    For Each vntParam In vntParams
        vntParamItems = Split(vntParam, "=")
        strParamKey = vntParamItems(0)
        strParamValue = vntParamItems(1)
        
        GetViewParamCollection.Add strParamValue, strParamKey
    Next vntParam
    
    If Search Then
        dtmDefStartDate = colViewParams.Item("UTCSTARTDATE")
        dtmDefEndDate = colViewParams.Item("UTCENDDATE")
        If Not IsUTCTimeZone Then
            dtmDefStartDate = UTCDateToLocal(dtmDefStartDate, colUTCTimeDiff, rsSeason)
            dtmDefEndDate = UTCDateToLocal(dtmDefEndDate, colUTCTimeDiff, rsSeason)
        End If
    
        dtmNow = Now
        If IsUTCTimeZone Then
            dtmNow = LocalDateToUTC(dtmNow, colUTCTimeDiff, rsSeason)
        End If
        
        vntParams = GetParamDateArray( _
            GetViewParamCollection.Item("STARTDATE"), GetViewParamCollection.Item("STARTTIME"), _
            GetViewParamCollection.Item("ENDDATE"), GetViewParamCollection.Item("ENDTIME"), _
            GetViewParamCollection.Item("RELPRDSTART"), GetViewParamCollection.Item("RELPRDEND"), _
            dtmNow, dtmDefStartDate, dtmDefEndDate)
        
        If Not IsUTCTimeZone Then
            vntParams(0) = LocalDateToUTC(vntParams(0), colUTCTimeDiff, rsSeason)
            vntParams(1) = LocalDateToUTC(vntParams(1), colUTCTimeDiff, rsSeason)
        End If
        
        GetViewParamCollection.Add vntParams(0), "UTCSTARTDATE"
        GetViewParamCollection.Add vntParams(1), "UTCENDDATE"
    End If
End Function

Private Sub PrintDeicing(ByVal Media As String)
    Dim rpt As New PrintDeicing
    Dim strFileName As String
    Dim strDefPath As String
    Dim strDefFile As String
    
    Load rpt
    If FormIsLoaded("MainButtons") Then
        rpt.InitReport tabFlightList, MainButtons.optTimeZone(0).Value, Media
    End If
    If Media = "print" Then
        rpt.Show vbModal, Me
        'rpt.Refresh
    Else
        rpt.Visible = False
        If pblnIsOnTop Then
            SetFormOnTop MyMainForm, False
        End If

        strDefPath = GetConfigEntry(colConfigs, "DE-ICING", "EXPORT_" & UCase(Media) & "_PATH", App.Path)
        If Dir(strDefPath, vbDirectory) = "" Then
            If Not CreateFolder(strDefPath) Then
                MsgBox "Folder " & strDefPath & " could not be created!", vbCritical, Me.Caption
                Exit Sub
            End If
        End If
        
        strDefFile = "De-icing_" & Format(Now, "YYYYMMDDhhmmss")
        strFileName = GetFileName(MyMainForm.cdgSave, strDefPath, strDefFile, Media)
        If strFileName <> "" Then
            If Dir(strFileName) <> "" Then
                On Error Resume Next
                Err.Number = 0
                Kill strFileName
                If Err.Number <> 0 Then
                    MsgBox "Could not overwrite file " & strFileName & vbNewLine & Err.Description, vbCritical, Me.Caption
                    Err.Number = 0
                    Exit Sub
                End If
                On Error GoTo 0
            End If
        
            rpt.Run False

            Select Case Media
                Case "excel"
                    Set ExcelExport = New ActiveReportsExcelExport.ARExportExcel
                    ExcelExport.FileName = strFileName
                    ExcelExport.Export rpt.Pages

                    Set ExcelExport = Nothing
                Case "pdf"
                    Set PDFExport = New ActiveReportsPDFExport.ARExportPDF
                    PDFExport.FileName = strFileName
                    PDFExport.Export rpt.Pages

                    Set PDFExport = Nothing
            End Select
            
            If blnOpenFileAfterExporting Then
                OpenFileUsingDefaultApp Me, strFileName
            End If
        End If

        If pblnIsOnTop Then
            SetFormOnTop MyMainForm, pblnIsOnTop
        End If

        Unload rpt
    End If
    
    Set rpt = Nothing
End Sub

Public Sub MainControlsExecuted(ByVal Key As String, Optional ByVal Value As Variant)
    Select Case Key
        Case "view"
            Call ShowFlightViewForm(False)
        Case "viewselect"
            Call ChangeView(Value(0), Value(1))
        Case "search"
            Call ShowFlightViewForm(True)
        Case "print", "excel"
            Call PrintDeicing(Key)
        Case "timezone"
            Call ChangeTimeZone(Value)
        Case "ontop"
            Call ChangeWindowOnTop(Value)
        Case "fipsreport"
            Call OpenFIPSReport
    End Select
End Sub

Private Sub OpenFIPSReport()
    Dim strFIPSReportPath As String
    Dim strParameter As String
    
    strFIPSReportPath = GetConfigEntry(colConfigs, "DE-ICING", "FIPS_REPORT_PATH", "")
    If strFIPSReportPath = "" Then
        MsgBox "FIPSReport path has not been configured!", vbInformation, Me.Caption
        Exit Sub
    End If
    
    If Dir(strFIPSReportPath) = "" Then
        MsgBox "Could not find " & strFIPSReportPath, vbInformation, Me.Caption
        Exit Sub
    End If
    
    strParameter = GetConfigEntry(colConfigs, "DE-ICING", "FIPS_REPORT_PARAMETER", "")
    If strParameter <> "" Then
        strFIPSReportPath = strFIPSReportPath & " " & strParameter
    End If
    
    Shell strFIPSReportPath, vbNormalFocus
End Sub

Private Sub ReadDataSource(ByVal FlightUrno As String)
    Dim intUrnoCountInWhereClause As Integer
    Dim vntFlightUrno As Variant
    Dim intUrnoCount As Integer
    Dim intArrayCount As Integer
    Dim i As Integer, j As Integer
    Dim strUrno As String
    Dim intStart As Integer, intEnd As Integer
    Dim strWhere As String
    Dim strDataSource As String
    Dim vntDataSources As Variant
    Dim vntDataSourceItem As Variant
    
    intUrnoCountInWhereClause = Val(GetConfigEntry(colConfigs, "MAIN", "URNO_COUNT_IN_WHERE_CLAUSE", "300"))
    
    vntFlightUrno = Split(FlightUrno, ",")
    intUrnoCount = UBound(vntFlightUrno) + 1
    If intUrnoCount > intUrnoCountInWhereClause Then
        FlightUrno = ""
        intArrayCount = Int(intUrnoCount / intUrnoCountInWhereClause) + 1
        For i = 1 To intArrayCount
            intStart = intUrnoCountInWhereClause * (i - 1)
            intEnd = intStart + (intUrnoCountInWhereClause - 1)
            If intEnd > intUrnoCount - 1 Then
                intEnd = intUrnoCount - 1
            End If
        
            strUrno = ""
            For j = intStart To intEnd
                strUrno = strUrno & "," & vntFlightUrno(j)
            Next j
            strUrno = Mid(strUrno, 2)
            
            FlightUrno = FlightUrno & "|" & strUrno
        Next i
        If FlightUrno <> "" Then FlightUrno = Mid(FlightUrno, 2)
    End If
    
   If colParameters.Exists("AftUrnoList") Then
        colParameters.Remove "AftUrnoList"
    End If
    colParameters.Add FlightUrno, "AftUrnoList"
    
    If pstrDataSource = "" Then
        pstrDataSource = GetConfigEntry(colConfigs, "DE-ICING", "DATA_SOURCE", "")
    End If
    vntDataSources = Split(pstrDataSource, ",")
    For Each vntDataSourceItem In vntDataSources
        If vntDataSourceItem <> "AFTTAB" Then
            Call AddDataSource("DE-ICING", colDataSources, vntDataSourceItem, colParameters, , True, True, "|")
        End If
    Next vntDataSourceItem
End Sub

Private Sub UpdateDeicing(ByVal LineNo As Long, ByVal FieldNames As String, ByVal fieldValues As String)
    Dim strFlightUrno As String
    Dim strDeicingUrno As String
    Dim strTable As String
    Dim strCommand As String
    Dim strFields As String
    Dim strWhere As String
    Dim strData As String
    Dim strResult As String
    
    strFlightUrno = tabFlightList.GetFieldValue(LineNo, strAftUrnoCol)
    strDeicingUrno = tabFlightList.GetFieldValue(LineNo, strIceUrnoCol)
    
    strTable = "ICETAB"
    
    If Trim(strDeicingUrno) = "" Then
        rsAft.Find "URNO = " & strFlightUrno, , , 1
        If Not rsAft.EOF Then
            strDeicingUrno = GetNewUrno
            
            strCommand = "IRT"
            strFields = "URNO,RURN,FTYP,FLNO,ADID,STDT," & FieldNames
            strData = strDeicingUrno & "," & strFlightUrno & "," & _
                rsAft.Fields("FTYP").Value & "," & _
                rsAft.Fields("FLNO").Value & "," & _
                rsAft.Fields("ADID").Value & "," & _
                Format(rsAft.Fields("STOD").Value, "YYYYMMDDhhmmss") & "," & _
                fieldValues
            
            'update memory table
            Call UpdateRecord(rsDeicing, strCommand, strFields, strData, strDeicingUrno)
            
            oUfisServer.CallCeda strResult, strCommand, strTable, strFields, strData, strWhere, "", 0, False, False
            Call UpdateRecord(rsDeicing, strCommand, strFields, strData, strDeicingUrno)
            
            'update the tab
            tabFlightList.SetFieldValues LineNo, strIceUrnoCol, strDeicingUrno
            tabFlightList.Refresh
        End If
    Else
        strCommand = "URT"
        strFields = FieldNames
        strData = fieldValues
        strWhere = "WHERE URNO = " & strDeicingUrno
        
        'update memory table
        Call UpdateRecord(rsDeicing, strCommand, strFields, strData, strDeicingUrno)
        
        'update DB
        oUfisServer.CallCeda strResult, strCommand, strTable, strFields, strData, strWhere, "", 0, False, False
        Call UpdateRecord(rsDeicing, strCommand, strFields, strData, strDeicingUrno)
    End If
End Sub

Private Function GetTabFieldValue(ByVal LineNo As Long, ByVal Index As Variant) As String
    If IsNumeric(Index) Then
        GetTabFieldValue = tabFlightList.GetColumnValue(LineNo, Index)
    Else
        GetTabFieldValue = tabFlightList.GetFieldValue(LineNo, Index)
    End If
End Function

Private Sub SetTabFieldValue(ByVal LineNo As Long, ByVal Index As Variant, ByVal Value As Variant)
    If IsNumeric(Index) Then
        tabFlightList.SetColumnValue LineNo, Index, Value
    Else
        tabFlightList.SetFieldValues LineNo, Index, Value
    End If
End Sub

Private Sub SetLineColor(ByVal LineNo As Integer, Optional ByVal FlightType)
    Dim strReadOnlyColumns As String
    Dim i As Integer
    Dim blnHasData As Boolean
    Dim intACDC As Integer
    Dim ForeColor As Long
    Dim BackColor As Long
    
    If IsMissing(FlightType) Then
        tabFlightList.GetLineColor LineNo, ForeColor, BackColor
    Else
        Select Case FlightType
            Case "X" 'Cancelled
                ForeColor = vbRed
            Case "N" 'No-op
                ForeColor = RGB(255, 165, 0) 'Orange
            Case Else
                ForeColor = vbBlack
        End Select
    End If
    
    intACDC = GetRealItemNo(tabFlightList.LogicalFieldList, "ACDC")
    '---------------
    'strReadOnlyColumns = tabFlightList.NoFocusColumns & "," & CStr(intACDC)
    strReadOnlyColumns = m_strRealReadOnlyColumns & "," & CStr(intACDC)
    '---------------
    blnHasData = False
    For i = 0 To tabFlightList.GetColumnCount - 1
        If InStr("," & strReadOnlyColumns & ",", "," & CStr(i) & ",") <= 0 Then
            If InStr("," & strBoolColumns & ",", "," & CStr(i) & ",") > 0 Then
                blnHasData = Not ((Trim(tabFlightList.GetColumnValue(LineNo, i)) = "") Or _
                    (Trim(tabFlightList.GetColumnValue(LineNo, i)) = "N"))
            Else
                blnHasData = Not (Trim(tabFlightList.GetColumnValue(LineNo, i)) = "")
            End If
            If blnHasData Then Exit For
        End If
    Next i
    
    If blnHasData Then
        tabFlightList.SetLineColor LineNo, ForeColor, RGB(211, 229, 250)
    Else
        tabFlightList.SetLineColor LineNo, ForeColor, vbWhite
    End If
End Sub

Private Sub PrepareTab()
    Dim i As Integer
    Dim strActualFieldList As String
    Dim strLogicalFieldList As String
    Dim strHeaderString As String
    Dim strHeaderLengthString As String
    Dim strHeaderAlignmentString As String
    Dim strColumnAlignmentString As String
    Dim strMainHeaderLengthString As String
    Dim strMainHeaderCaption As String
    Dim strReadOnlyColumns As String
    Dim vntBoolColumns As Variant
    Dim strPrintColumns As String
    Dim blnReadOnly As Boolean
    
    strActualFieldList = "AFTTAB.URNO,AFTTAB.FLNO,AFTTAB.CSGN,AFTTAB.DES3,AFTTAB.STOD,AFTTAB.ETDI," & _
        "AFTTAB.OFBL,AFTTAB.AIRB,PSTD,AFTTAB.ACT3,AFTTAB.REGN,ICETAB.DICE,ICETAB.ACDC,ICETAB.DITM," & _
        "ICETAB.DIHO, ICETAB.DIAB, ICETAB.DIAE"
    strLogicalFieldList = "URNO,FLNO,CSGN,DES3,STOD,ETDI,OFBL,AIRB,PSTD,ACT3,REGN,DICE,ACDC,DITM,DIHO,DIAB,DIAE"
    strHeaderString = "URNO,FLIGHT,C/S,DES,STD,ETD,OFB,ATD,POS,A/C,REG,REQ.,DOOR CLOSED,TIME (MIN),HOLD OVER (MIN),START,END"
    strHeaderLengthString = "0,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30"
    strHeaderAlignmentString = "C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C,C"
    strColumnAlignmentString = strHeaderAlignmentString
    strMainHeaderLengthString = "11,6"
    strMainHeaderCaption = "Flight Information,De-icing Information"
    strReadOnlyColumns = "0,1,2,3,4,5,6,7,8,9,10,11"
    strBoolColumns = "12,13"
    strPrintColumns = "0,1,2,3,4,5"
    strDataFormat = "YYYYMMDDhhmmss,,,,,,DD,hh:mm,hh:mm,hh:mm,hh:mm,,,,,,,,,"
    
    strActualFieldList = GetConfigEntry(colConfigs, "DE-ICING", "TAB_ACTFLDLIST", strActualFieldList)
    strLogicalFieldList = GetConfigEntry(colConfigs, "DE-ICING", "TAB_LOGFLDLIST", strLogicalFieldList)
    strHeaderString = GetConfigEntry(colConfigs, "DE-ICING", "TAB_HDRSTRING", strHeaderString)
    strHeaderLengthString = GetConfigEntry(colConfigs, "DE-ICING", "TAB_HDRLENGTH", strHeaderLengthString)
    strHeaderAlignmentString = GetConfigEntry(colConfigs, "DE-ICING", "TAB_HDRALIGNMENT", strHeaderAlignmentString)
    strColumnAlignmentString = GetConfigEntry(colConfigs, "DE-ICING", "TAB_COLALIGNMENT", strColumnAlignmentString)
    strMainHeaderLengthString = GetConfigEntry(colConfigs, "DE-ICING", "TAB_MAINHEADERLENGTH", strMainHeaderLengthString)
    strMainHeaderCaption = GetConfigEntry(colConfigs, "DE-ICING", "TAB_MAINHEADERCAPTION", strMainHeaderCaption)
    '----------------------
    'strReadOnlyColumns = GetConfigEntry(colConfigs, "DE-ICING", "TAB_READONLYCOLUMNS", strReadOnlyColumns)
    m_strRealReadOnlyColumns = GetConfigEntry(colConfigs, "DE-ICING", "TAB_READONLYCOLUMNS", strReadOnlyColumns)
    strReadOnlyColumns = ""
    For i = 0 To UBound(Split(strLogicalFieldList, ","))
        strReadOnlyColumns = strReadOnlyColumns & "," & CStr(i)
    Next i
    If strReadOnlyColumns <> "" Then strReadOnlyColumns = Mid(strReadOnlyColumns, 2)
    '----------------------
    strBoolColumns = GetConfigEntry(colConfigs, "DE-ICING", "TAB_BOOLCOLUMNS", strBoolColumns)
    strPrintColumns = GetConfigEntry(colConfigs, "DE-ICING", "TAB_PRINTCOLUMNS", strPrintColumns)
    strDataFormat = GetConfigEntry(colConfigs, "DE-ICING", "TAB_DATAFORMAT", strDataFormat)
    
    With tabFlightList
        .ResetContent
        .Tag = strActualFieldList
        .myTag = strPrintColumns
        .LogicalFieldList = strLogicalFieldList
        .HeaderString = strHeaderString
        .HeaderLengthString = strHeaderLengthString
        .HeaderAlignmentString = strHeaderAlignmentString
        .ColumnAlignmentString = strColumnAlignmentString
        .SetMainHeaderValues strMainHeaderLengthString, strMainHeaderCaption, ""
        .NoFocusColumns = strReadOnlyColumns
        .SetMainHeaderFont .FontSize, False, False, True, 0, .FontName
        .MainHeader = True
        .PostEnterBehavior = 3
        .InplaceEditSendKeyEvents = True
            
        vntBoolColumns = Split(strBoolColumns, ",")
        For i = 0 To UBound(vntBoolColumns)
            .SetColumnBoolProperty vntBoolColumns(i), "Y", "N"
            
            blnReadOnly = (InStr("," & strReadOnlyColumns & ",", "," & vntBoolColumns(i) & ",") > 0)
            .SetBoolPropertyReadOnly vntBoolColumns(i), blnReadOnly
        Next i

        .ShowHorzScroller True
        .ShowVertScroller True
    End With
    
    'Add to collection of fields used
    Set rsFieldUsed = AddFieldsUsedToRecordset(strActualFieldList)
    Set colFieldUsed = AddFieldsUsedToCollection(rsFieldUsed)
    
    'save urno col no
    lngAftUrnoColNo = GetRealItemNo(tabFlightList.Tag, "AFTTAB.URNO")
    lngIceUrnoColNo = GetRealItemNo(tabFlightList.Tag, "ICETAB.URNO")
    strAftUrnoCol = GetRealItem(tabFlightList.LogicalFieldList, lngAftUrnoColNo, ",")
    strIceUrnoCol = GetRealItem(tabFlightList.LogicalFieldList, lngIceUrnoColNo, ",")
End Sub

Private Function AddFieldsUsedToRecordset(ByVal ActFields As String) As ADODB.Recordset
    Dim vntActFields As Variant
    Dim vntLogFields As Variant
    Dim vntActField As Variant
    Dim vntActFieldOptions As Variant
    Dim vntActFieldOption As Variant
    Dim vntTableField As Variant
    Dim strTable As String
    Dim strActFieldList As String
    Dim strLogFieldList As String
    Dim i As Integer, j As Integer
    Dim intOptionsCount As Integer
    Dim vntDataFormat As Variant
    
    Set AddFieldsUsedToRecordset = New ADODB.Recordset
    AddFieldsUsedToRecordset.Fields.Append "TableName", adVarChar, 6, adFldIsNullable
    AddFieldsUsedToRecordset.Fields.Append "ActField", adVarChar, 4, adFldIsNullable
    AddFieldsUsedToRecordset.Fields.Append "LogField", adVarChar, 4, adFldIsNullable
    AddFieldsUsedToRecordset.Fields.Append "DataFormat", adVarChar, 20, adFldIsNullable
    AddFieldsUsedToRecordset.Fields.Append "Index", adTinyInt, , adFldIsNullable
    AddFieldsUsedToRecordset.Open
    AddFieldsUsedToRecordset.Sort = "TableName"
    
    vntActFields = Split(tabFlightList.Tag, ",")
    vntLogFields = Split(tabFlightList.LogicalFieldList, ",")
    vntDataFormat = Split(strDataFormat, ",")
    i = 0
    For Each vntActField In vntActFields
        vntActFieldOptions = Split(vntActField, "|")
        intOptionsCount = UBound(vntActFieldOptions)
        j = 0
        For Each vntActFieldOption In vntActFieldOptions
            vntTableField = Split(vntActFieldOption, ".")
            
            strTable = vntTableField(0)
            strActFieldList = vntTableField(1)
            strLogFieldList = vntLogFields(i)
            
            AddFieldsUsedToRecordset.AddNew
            AddFieldsUsedToRecordset.Fields("TableName").Value = strTable
            AddFieldsUsedToRecordset.Fields("ActField").Value = strActFieldList
            AddFieldsUsedToRecordset.Fields("LogField").Value = strLogFieldList
            AddFieldsUsedToRecordset.Fields("DataFormat").Value = vntDataFormat(i)
            AddFieldsUsedToRecordset.Fields("Index").Value = intOptionsCount - j
            AddFieldsUsedToRecordset.Update
            
            j = j + 1
        Next vntActFieldOption
        
        i = i + 1
    Next vntActField
End Function

Private Function AddFieldsUsedToCollection(rsFieldList As ADODB.Recordset) As CollectionExtended
    Dim strTable As String
    Dim strField As String
    Dim strFieldList As String
    
    Set AddFieldsUsedToCollection = New CollectionExtended
    
    If Not (rsFieldList.BOF And rsFieldList.EOF) Then
        rsFieldList.MoveFirst
    End If
    
    strFieldList = ""
    strTable = ""
    Do While Not rsFieldList.EOF
        If strTable <> rsFieldList.Fields("TableName").Value Then
            If strTable <> "" Then
                If strFieldList <> "" Then strFieldList = Mid(strFieldList, 2)
                AddFieldsUsedToCollection.Add strFieldList, strTable
            End If
            
            strTable = rsFieldList.Fields("TableName").Value
            strFieldList = ""
        End If
        
        strField = rsFieldList.Fields("ActField").Value
        strFieldList = strFieldList & "," & strField
        
        rsFieldList.MoveNext
    Loop
    
    If strTable <> "" Then
        If strFieldList <> "" Then strFieldList = Mid(strFieldList, 2)
        AddFieldsUsedToCollection.Add strFieldList, strTable
    End If
End Function

Private Sub ChangeView(ByVal ViewIndex As Long, ByVal Urno As Long)
    rsView.Find "URNO = " & CStr(Urno), , , 1
    If rsView.EOF Then 'Not found, use default
        Set colViewParams = GetDefaultParamCollection()
    Else
        Set colViewParams = GetViewParamCollection(rsView.Fields("TEXT").Value)
    End If
    
    Call LoadData(colViewParams)
    
    If FormIsLoaded("FlightView") Then
        FlightView.cboView.ListIndex = ViewIndex
    End If
End Sub

Private Function ValidateDataWithCurrentView(rsAft As ADODB.Recordset, ViewParamCollection As CollectionExtended) As Boolean
    Dim dtmStartDate As Date
    Dim dtmEndDate As Date
    Dim intDayOfWeek As Integer
    Dim strParamVal As String
    Dim strTemp As String
    Dim vntParamVal As Variant
    Dim i As Integer

    ValidateDataWithCurrentView = True
    
    'Default: ADID = 'D'
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, "D", "ADID")
    If Not ValidateDataWithCurrentView Then Exit Function
    
'    'Default: AIRB = ' '
'    ValidateDataWithCurrentView = IsNull(rsAft.Fields("AIRB").Value)
'    If Not ValidateDataWithCurrentView Then Exit Function
    
    'Validate against the view filter
    'check TIFD/STOD not Null
    ValidateDataWithCurrentView = Not IsNull(rsAft.Fields("TIFD").Value)
    If Not ValidateDataWithCurrentView Then Exit Function
    
    ValidateDataWithCurrentView = Not IsNull(rsAft.Fields("STOD").Value)
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check the start/end date
    dtmStartDate = ViewParamCollection.Item("UTCSTARTDATE")
    dtmEndDate = ViewParamCollection.Item("UTCENDDATE")
    'check TIFD/STOD >= start date and <= end date
    If dtmStartDate <> CDate(0) And dtmEndDate <> CDate(0) Then
        ValidateDataWithCurrentView = ((rsAft.Fields("TIFD").Value >= dtmStartDate) And (rsAft.Fields("TIFD").Value <= dtmEndDate)) Or _
            ((rsAft.Fields("STOD").Value >= dtmStartDate) And (rsAft.Fields("STOD").Value <= dtmEndDate))
        If Not ValidateDataWithCurrentView Then Exit Function
    End If
    
    'check the day of week
    strParamVal = ViewParamCollection.Item("DAYS")
    If Trim(strParamVal) <> "" Then
        If IsUTCTimeZone Then
            intDayOfWeek = Weekday(rsAft.Fields("TIFD").Value, vbMonday)
        Else
            intDayOfWeek = Weekday(UTCDateToLocal(rsAft.Fields("TIFD").Value, colUTCTimeDiff, rsSeason), vbMonday)
        End If
        ValidateDataWithCurrentView = (InStr(strParamVal, CStr(intDayOfWeek)) > 0)
        If Not ValidateDataWithCurrentView Then Exit Function
    End If
    
    'check ALC2
    strParamVal = ViewParamCollection.Item("ALC2")
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "ALC2")
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check FLTN
    strParamVal = ViewParamCollection.Item("FLTN")
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "FLTN")
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check FLNS
    strParamVal = ViewParamCollection.Item("FLNS")
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "FLNS")
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check AIRLINE
    strParamVal = ViewParamCollection.Item("AIRLINE")
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "ALC2", "ALC3")
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check CSGN
    strParamVal = ViewParamCollection.Item("CSGN")
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "CSGN")
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check FLTI
    strParamVal = ViewParamCollection.Item("FLTI")
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "FLTI")
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check REGN
    strParamVal = ViewParamCollection.Item("REGN")
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "REGN")
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check ACTY
    strParamVal = ViewParamCollection.Item("ACTY")
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "ACT3", "ACT5")
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check AIRP
    strParamVal = ViewParamCollection.Item("AIRP")
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "DES3", "DES4")
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check VIAP
    strParamVal = ViewParamCollection.Item("VIAP")
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "VIA3", "VIA4")
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check NATURE
    strParamVal = ViewParamCollection.Item("NATURE")
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "TTYP")
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check STEV
    strParamVal = ViewParamCollection.Item("STEV")
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "STEV")
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check HDLAGENT
    strParamVal = ViewParamCollection.Item("HDLAGENT")
    If Trim(strParamVal) <> "" Then
        ValidateDataWithCurrentView = False
        
        rsAirlines.Find "ALC2 = '" & Trim(rsAft.Fields("ALC2").Value) & "'", , , 1
        If rsAirlines.EOF Then
            rsAirlines.Find "ALC3 = '" & Trim(rsAft.Fields("ALC3").Value) & "'", , , 1
        End If
        If Not rsAirlines.EOF Then
            rsHandlingRels.Filter = "ALTU = " & rsAirlines.Fields("URNO").Value
            Do While Not rsHandlingRels.EOF
                ValidateDataWithCurrentView = IsDataInViewCriteria(rsHandlingRels, strParamVal, "HSNA")
                If ValidateDataWithCurrentView Then Exit Do
                
                rsHandlingRels.MoveNext
            Loop
            rsHandlingRels.Filter = adFilterNone
        End If
        
        If Not ValidateDataWithCurrentView Then Exit Function
    End If
    
    'check HTYP
    strParamVal = ViewParamCollection.Item("HTYP")
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "HTYP")
    If Not ValidateDataWithCurrentView Then Exit Function
    
    'check PSTN/PSGR
    'check PSTN
    strParamVal = ViewParamCollection.Item("PSTN")
    If strParamVal <> "" Then
        strParamVal = "," & strParamVal
    End If
    'check PSGR
    strTemp = ViewParamCollection.Item("PSGR")
    If strTemp <> "" Then
        vntParamVal = Split(strTemp, ",")
        For i = 0 To UBound(vntParamVal)
            rsAircraftPosGroups.Find "GRPN = '" & vntParamVal(i) & "'", , , 1
            If Not rsAircraftPosGroups.EOF Then
                rsAircraftPosGroupRels.Filter = "GURN = " & rsAircraftPosGroups.Fields("URNO").Value
                Do While Not rsAircraftPosGroupRels.EOF
                    rsAircraftPos.Find "URNO = " & rsAircraftPosGroupRels.Fields("VALU").Value, , , 1
                    If Not rsAircraftPos.EOF Then
                        strParamVal = strParamVal & "," & rsAircraftPos.Fields("PNAM").Value
                    End If
                    
                    rsAircraftPosGroupRels.MoveNext
                Loop
                rsAircraftPosGroupRels.Filter = adFilterNone
            End If
        Next i
    End If
    If strParamVal <> "" Then
        strParamVal = Mid(strParamVal, 2)
        ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "PSTD")
        If Not ValidateDataWithCurrentView Then Exit Function
    End If
    
    'check FTYP
    strParamVal = GetParamValList(ViewParamCollection, Array("O", "S", "X", "N", "D", "R", "P", "G", "T"), _
        Array("O,Z,B", "S", "X", "N", "D", "R", "G", " ", "T"))
    ValidateDataWithCurrentView = IsDataInViewCriteria(rsAft, strParamVal, "FTYP")
    If Not ValidateDataWithCurrentView Then Exit Function
End Function

Private Function IsColumnEditable(ByVal ColNo As Long) As Boolean
    Dim blnReturn As Boolean
    
    blnReturn = (InStr("," & tabFlightList.NoFocusColumns & ",", "," & CStr(ColNo) & ",") <= 0)
    If blnReturn Then
        blnReturn = (InStr("," & strBoolColumns & ",", "," & CStr(ColNo) & ",") <= 0)
    End If
    
    IsColumnEditable = blnReturn
End Function

Private Function GetFormattedData(ByVal ActValue As Variant, ByVal DataFormat As String) As String
    Dim strValue As String
    Dim dtmBaseDate As Date
    Dim vntDataFormat As Variant
    Dim strDataFormat As String
    Dim vntTableField As Variant
    Dim strTable As String
    Dim strField As String
    
    strValue = Trim(NVL(ActValue, ""))
    If Trim(DataFormat) <> "" Then
        If InStr(DataFormat, "|") > 0 Then
            vntDataFormat = Split(DataFormat, "|")
            strDataFormat = vntDataFormat(0)
            vntTableField = Split(vntDataFormat(1), ".")
            strTable = vntTableField(0)
            strField = vntTableField(1)
            
            If colDataSources.Exists(strTable) Then
                If colDataSources.Item(strTable).Fields(strField).Type = adDate Then
                    dtmBaseDate = colDataSources.Item(strTable).Fields(strField).Value
                    If Not IsUTCTimeZone Then
                        dtmBaseDate = UTCDateToLocal(dtmBaseDate, colUTCTimeDiff, rsSeason)
                    End If
                    
                    strValue = FullDateTimeToShortTime(dtmBaseDate, NVL(ActValue, CDate(0)))
                End If
            End If
        Else
            strValue = Format(strValue, DataFormat)
        End If
    End If
    
    GetFormattedData = strValue
End Function

Private Sub ChangeWindowOnTop(ByVal Value As Integer)
    pblnIsOnTop = (Value = vbChecked)
    SetFormOnTop Me, pblnIsOnTop
End Sub

Public Sub DisplayFlightList(ViewParamCollection As CollectionExtended)
    Dim strValue As String
    Dim intColIndex As Integer
    
    tabFlightList.ResetContent
    
    If Not (rsAft Is Nothing) Then
        Do While Not rsAft.EOF
            If ValidateDataWithCurrentView(rsAft, ViewParamCollection) Then
                rsDeicing.Find "RURN = " & rsAft.Fields("URNO").Value, , , 1
                
                strValue = GetTextLine(colDataSources, tabFlightList.Tag, strDataFormat)
            
                tabFlightList.InsertTextLine strValue, False
                Call SetLineColor(tabFlightList.GetLineCount - 1, rsAft.Fields("FTYP").Value)
            End If
            
            rsAft.MoveNext
        Loop
    End If
    
    intColIndex = GetRealItemNo(tabFlightList.LogicalFieldList, "SORT")
    If intColIndex >= 0 Then
        Call SortRecordByColumnNo(intColIndex, True)
    End If
    tabFlightList.Refresh
End Sub

Private Function GetTextLine(colDataSources As CollectionExtended, ByVal ActFields As String, _
ByVal DataFormat As String) As String   ', _
ByVal StdtTable As String, ByVal StdtField As String) As String
    Dim vntActFields As Variant
    Dim vntActField As Variant
    Dim vntActFieldOptions As Variant
    Dim vntActFieldOption As Variant
    Dim vntDataFormat As Variant
    Dim vntTableField As Variant
    Dim strTable As String
    Dim strField As String
    Dim vntValue As Variant
    Dim strReturn As String
    Dim i As Integer
    Dim dtmDate As Date
    
    strReturn = ""
    vntActFields = Split(ActFields, ",")
    vntDataFormat = Split(DataFormat, ",")
    i = 0
    For Each vntActField In vntActFields
        vntActFieldOptions = Split(vntActField, "|")
        For Each vntActFieldOption In vntActFieldOptions
            vntTableField = Split(vntActFieldOption, ".")
            strTable = vntTableField(0)
            strField = vntTableField(1)
            
            vntValue = ""
            With colDataSources.Item(strTable)
                If Not (.BOF Or .EOF) Then
                    If Not IsNull(.Fields(strField).Value) Then
                        If .Fields(strField).Type = adDate Then
                            vntValue = .Fields(strField).Value
                            If Not IsUTCTimeZone Then
                                vntValue = UTCDateToLocal(vntValue, colUTCTimeDiff, rsSeason)
                            End If
                        Else
                            vntValue = NVL(.Fields(strField).Value, "")
                        End If
                    
                        vntValue = GetFormattedData(vntValue, vntDataFormat(i))
                    End If
                End If
            End With
            
            If vntValue <> "" Then Exit For
        Next vntActFieldOption
        
        strReturn = strReturn & "," & vntValue
        i = i + 1
    Next vntActField
    strReturn = Mid(strReturn, 2)
    
    GetTextLine = strReturn
End Function

Private Sub cboView_Click()
    Dim lngUrno As Long
    
    lngUrno = cboView.ItemData(cboView.ListIndex)
    Me.MainControlsExecuted "viewselect", Array(cboView.ListIndex, lngUrno)
End Sub

Private Sub chkOnTop_Click()
    If chkOnTop.Value = vbChecked Then
        chkOnTop.BackColor = vbGreen
    Else
        chkOnTop.BackColor = vbButtonFace
    End If
    Me.MainControlsExecuted "ontop", chkOnTop.Value
End Sub

Private Sub cmdMainButtons_Click(Index As Integer)
    Select Case Index
        Case 0 'View
            Me.MainControlsExecuted "view"
        Case 1 'Search
            Me.MainControlsExecuted "search"
        Case 2 'Print
            Me.MainControlsExecuted "print"
        Case 3 'Excel
            Me.MainControlsExecuted "excel"
        Case Else 'For other purposes
            Me.MainControlsExecuted cmdMainButtons(Index).Tag
    End Select
End Sub

Private Sub PrepareEnv()
    Dim AppType As String
    
    AppType = "DE-ICING"

    chkOnTop.Value = IIf(pblnIsOnTop, vbChecked, vbUnchecked)
    
    Me.Caption = GetConfigEntry(colConfigs, AppType, "WINDOW_TITLE", "Common Data Input Dialog")
    
    Call SetWindowSizePos(Me, colConfigs, AppType)
End Sub

Private Sub Form_Load()
    Dim FormToShow As Form
    
    Call PrepareEnv
    
    Set rsAirlines = colDataSources.Item("ALTTAB")
    Set rsHandlingRels = colDataSources.Item("HAITAB")
    Set rsView = colDataSources.Item("VCDTAB")
    Set rsAircraftPos = colDataSources.Item("PSTTAB")
    Set rsAircraftPosGroups = colDataSources.Item("GRNTAB")
    Set rsAircraftPosGroupRels = colDataSources.Item("GRMTAB")
    If colDataSources.Exists("SEATAB") Then
        Set rsSeason = colDataSources.Item("SEATAB")
    End If
    
    Call AddDefaultView
    
    blnOpenFileAfterExporting = (GetConfigEntry(colConfigs, "DE-ICING", "OPEN_FILE_AFTER_EXPORTING", "YES") = "YES")
    If pblnFlightListMainButtons Then
   
        Call FillViewComboBox(rsView, cboView)
        cboView.ListIndex = 0
        
        optTimeZone(0).Value = IsUTCTimeZone
        optTimeZone(1).Value = Not IsUTCTimeZone
        Call optTimeZone_Click(0)
        
        chkOnTop.Value = IIf(pblnIsOnTop, vbChecked, vbUnchecked)
        
        picMainButtons.Visible = True
    Else
        picMainButtons.Visible = False
    End If

    Call PrepareTab
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    On Error Resume Next

    If FormIsLoaded("FlightView") Then
        Unload FlightView
    End If
End Sub

Private Sub Form_Resize()
    Dim sngTop As Single
    Dim intAdjustment As Integer

    On Error Resume Next
    
    If Me.WindowState <> vbMinimized Then
        sngTop = 0
        If picMainButtons.Visible Then sngTop = sngTop + picMainButtons.Height
        
        intAdjustment = 0
        If pblnIsStandAlone And Not pblnFlightListWindow Then
            intAdjustment = 50
        End If
        
        picMainButtons.Width = Me.ScaleWidth
        tabFlightList.Move 0, sngTop, Me.ScaleWidth - intAdjustment, Me.ScaleHeight - sngTop - intAdjustment
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set ExcelExport = Nothing
    Set PDFExport = Nothing
End Sub

Private Sub optTimeZone_Click(Index As Integer)
    Dim opt As OptionButton
    
    For Each opt In Me.optTimeZone
        If opt.Value Then
            opt.BackColor = vbGreen
        Else
            opt.BackColor = vbButtonFace
        End If
    Next opt
    
    Me.MainControlsExecuted "timezone", optTimeZone(0).Value
End Sub

Private Sub tabFlightList_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Select Case LineNo
        Case -2
            ColNo = GetRealItemNo(tabFlightList.LogicalFieldList, "SORT")
            If ColNo >= 0 Then
                Call SortRecordByColumnNo(ColNo)
            End If
        Case -1
            Call SortRecordByColumnNo(ColNo)
    End Select
End Sub

Public Sub EvaluateBc(ByVal CedaCmd As String, ByVal ObjName As String, ByVal CedaSqlKey As String, Optional ByVal ChangedFields As String)
    'This is for report
    'No BC will be handled
    
    'Do nothing
End Sub

