VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FlightView 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Flight View"
   ClientHeight    =   9495
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7455
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FlightView.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9495
   ScaleWidth      =   7455
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdOK 
      Cancel          =   -1  'True
      Caption         =   "&OK"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4800
      TabIndex        =   71
      Top             =   9000
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton cmdApply 
      Caption         =   "Apply"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5400
      TabIndex        =   70
      Top             =   8400
      Width           =   1215
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "Delete"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4080
      TabIndex        =   69
      Top             =   8400
      Width           =   1215
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Save"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2760
      TabIndex        =   68
      Top             =   8400
      Width           =   1215
   End
   Begin VB.ComboBox cboView 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   120
      TabIndex        =   67
      Top             =   8430
      Width           =   2535
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Cancel"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   6120
      TabIndex        =   72
      Top             =   9000
      Width           =   1215
   End
   Begin TabDlg.SSTab sstFlightView 
      Height          =   8175
      Left            =   120
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   120
      Width           =   7215
      _ExtentX        =   12726
      _ExtentY        =   14420
      _Version        =   393216
      Tabs            =   1
      TabsPerRow      =   4
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Search for flights"
      TabPicture(0)   =   "FlightView.frx":058A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblLabels(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblLabels(4)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblLabels(1)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "lblLabels(2)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "lblLabels(5)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "lblLabels(6)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "lblLabels(7)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "lblLabels(8)"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "lblLabels(9)"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "lblLabels(10)"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "lblLabels(11)"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "lblLabels(12)"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "lblLabels(13)"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "lblLabels(14)"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "lblLabels(15)"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "lblLabels(16)"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "lblLabels(18)"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "lblUTC_LT(1)"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "lblUTC_LT(0)"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "lblLabels(3)"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "cmdLookups(1)"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "chkDaily"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).Control(22)=   "txtDays"
      Tab(0).Control(22).Enabled=   0   'False
      Tab(0).Control(23)=   "txtFlno(0)"
      Tab(0).Control(23).Enabled=   0   'False
      Tab(0).Control(24)=   "txtDate(0)"
      Tab(0).Control(24).Enabled=   0   'False
      Tab(0).Control(25)=   "txtTime(0)"
      Tab(0).Control(25).Enabled=   0   'False
      Tab(0).Control(26)=   "txtDate(1)"
      Tab(0).Control(26).Enabled=   0   'False
      Tab(0).Control(27)=   "txtTime(1)"
      Tab(0).Control(27).Enabled=   0   'False
      Tab(0).Control(28)=   "txtFlno(1)"
      Tab(0).Control(28).Enabled=   0   'False
      Tab(0).Control(29)=   "txtFlno(2)"
      Tab(0).Control(29).Enabled=   0   'False
      Tab(0).Control(30)=   "txtNature"
      Tab(0).Control(30).Enabled=   0   'False
      Tab(0).Control(31)=   "chkADID(0)"
      Tab(0).Control(31).Enabled=   0   'False
      Tab(0).Control(32)=   "chkADID(1)"
      Tab(0).Control(32).Enabled=   0   'False
      Tab(0).Control(33)=   "chkADID(2)"
      Tab(0).Control(33).Enabled=   0   'False
      Tab(0).Control(34)=   "chkFTYP(0)"
      Tab(0).Control(34).Enabled=   0   'False
      Tab(0).Control(35)=   "chkFTYP(1)"
      Tab(0).Control(35).Enabled=   0   'False
      Tab(0).Control(36)=   "chkFTYP(2)"
      Tab(0).Control(36).Enabled=   0   'False
      Tab(0).Control(37)=   "chkFTYP(3)"
      Tab(0).Control(37).Enabled=   0   'False
      Tab(0).Control(38)=   "cmdLookups(2)"
      Tab(0).Control(38).Enabled=   0   'False
      Tab(0).Control(39)=   "txtAirline"
      Tab(0).Control(39).Enabled=   0   'False
      Tab(0).Control(40)=   "cmdLookups(0)"
      Tab(0).Control(40).Enabled=   0   'False
      Tab(0).Control(41)=   "txtRelPeriod(0)"
      Tab(0).Control(41).Enabled=   0   'False
      Tab(0).Control(42)=   "txtRelPeriod(1)"
      Tab(0).Control(42).Enabled=   0   'False
      Tab(0).Control(43)=   "txtCallSign"
      Tab(0).Control(43).Enabled=   0   'False
      Tab(0).Control(44)=   "txtFlightID"
      Tab(0).Control(44).Enabled=   0   'False
      Tab(0).Control(45)=   "txtKeyCode"
      Tab(0).Control(45).Enabled=   0   'False
      Tab(0).Control(46)=   "txtRegNo"
      Tab(0).Control(46).Enabled=   0   'False
      Tab(0).Control(47)=   "txtAircraftType"
      Tab(0).Control(47).Enabled=   0   'False
      Tab(0).Control(48)=   "txtAirport"
      Tab(0).Control(48).Enabled=   0   'False
      Tab(0).Control(49)=   "txtVia"
      Tab(0).Control(49).Enabled=   0   'False
      Tab(0).Control(50)=   "cmdLookups(3)"
      Tab(0).Control(50).Enabled=   0   'False
      Tab(0).Control(51)=   "cmdLookups(4)"
      Tab(0).Control(51).Enabled=   0   'False
      Tab(0).Control(52)=   "cmdLookups(5)"
      Tab(0).Control(52).Enabled=   0   'False
      Tab(0).Control(53)=   "cmdLookups(6)"
      Tab(0).Control(53).Enabled=   0   'False
      Tab(0).Control(54)=   "txtHandlingType"
      Tab(0).Control(54).Enabled=   0   'False
      Tab(0).Control(55)=   "txtHandlingAgent"
      Tab(0).Control(55).Enabled=   0   'False
      Tab(0).Control(56)=   "txtPosition"
      Tab(0).Control(56).Enabled=   0   'False
      Tab(0).Control(57)=   "cmdLookups(7)"
      Tab(0).Control(57).Enabled=   0   'False
      Tab(0).Control(58)=   "cmdLookups(8)"
      Tab(0).Control(58).Enabled=   0   'False
      Tab(0).Control(59)=   "chkFTYP(4)"
      Tab(0).Control(59).Enabled=   0   'False
      Tab(0).Control(60)=   "chkFTYP(5)"
      Tab(0).Control(60).Enabled=   0   'False
      Tab(0).Control(61)=   "chkFTYP(6)"
      Tab(0).Control(61).Enabled=   0   'False
      Tab(0).Control(62)=   "chkFTYP(7)"
      Tab(0).Control(62).Enabled=   0   'False
      Tab(0).Control(63)=   "chkFTYP(8)"
      Tab(0).Control(63).Enabled=   0   'False
      Tab(0).Control(64)=   "cmdLookups(9)"
      Tab(0).Control(64).Enabled=   0   'False
      Tab(0).Control(65)=   "txtPosGroups"
      Tab(0).Control(65).Enabled=   0   'False
      Tab(0).ControlCount=   66
      Begin VB.TextBox txtPosGroups 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   5160
         TabIndex        =   53
         Tag             =   "PSGR"
         Top             =   5520
         Width           =   1455
      End
      Begin VB.CommandButton cmdLookups 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   9
         Left            =   6600
         TabIndex        =   54
         TabStop         =   0   'False
         Top             =   5520
         Width           =   375
      End
      Begin VB.CheckBox chkFTYP 
         Caption         =   "Prognosis/GatPos"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   8
         Left            =   2640
         TabIndex        =   60
         Tag             =   "P"
         Top             =   6840
         Width           =   1815
      End
      Begin VB.CheckBox chkFTYP 
         Caption         =   "Rerouted"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   7
         Left            =   4920
         TabIndex        =   64
         Tag             =   "R"
         Top             =   7200
         Width           =   1215
      End
      Begin VB.CheckBox chkFTYP 
         Caption         =   "Diverted"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   6
         Left            =   4920
         TabIndex        =   63
         Tag             =   "D"
         Top             =   6840
         Width           =   1215
      End
      Begin VB.CheckBox chkFTYP 
         Caption         =   "Towing"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   360
         TabIndex        =   66
         Tag             =   "T"
         Top             =   7680
         Width           =   975
      End
      Begin VB.CheckBox chkFTYP 
         Caption         =   "Ground Movement"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   360
         TabIndex        =   65
         Tag             =   "G"
         Top             =   7320
         Width           =   1935
      End
      Begin VB.CommandButton cmdLookups 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   8
         Left            =   3240
         TabIndex        =   51
         TabStop         =   0   'False
         Top             =   5520
         Width           =   375
      End
      Begin VB.CommandButton cmdLookups 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   7
         Left            =   6600
         TabIndex        =   48
         TabStop         =   0   'False
         Top             =   5040
         Width           =   375
      End
      Begin VB.TextBox txtPosition 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1800
         TabIndex        =   50
         Tag             =   "PSTN"
         Top             =   5520
         Width           =   1455
      End
      Begin VB.TextBox txtHandlingAgent 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1800
         TabIndex        =   44
         Tag             =   "HDLAGENT"
         Top             =   5040
         Width           =   1455
      End
      Begin VB.TextBox txtHandlingType 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   5160
         TabIndex        =   47
         Tag             =   "HTYP"
         Top             =   5040
         Width           =   1455
      End
      Begin VB.CommandButton cmdLookups 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   6
         Left            =   3240
         TabIndex        =   45
         TabStop         =   0   'False
         Top             =   5040
         Width           =   375
      End
      Begin VB.CommandButton cmdLookups 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   5
         Left            =   3240
         TabIndex        =   40
         TabStop         =   0   'False
         Top             =   4560
         Width           =   375
      End
      Begin VB.CommandButton cmdLookups 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   4
         Left            =   6600
         TabIndex        =   37
         TabStop         =   0   'False
         Top             =   4080
         Width           =   375
      End
      Begin VB.CommandButton cmdLookups 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   3
         Left            =   3240
         TabIndex        =   34
         TabStop         =   0   'False
         Top             =   4080
         Width           =   375
      End
      Begin VB.TextBox txtVia 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   5160
         TabIndex        =   36
         Tag             =   "VIAP"
         Top             =   4080
         Width           =   1455
      End
      Begin VB.TextBox txtAirport 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1800
         TabIndex        =   33
         Tag             =   "AIRP"
         Top             =   4080
         Width           =   1455
      End
      Begin VB.TextBox txtAircraftType 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3720
         TabIndex        =   30
         Tag             =   "ACTY"
         Top             =   3600
         Width           =   735
      End
      Begin VB.TextBox txtRegNo 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1800
         TabIndex        =   28
         Tag             =   "REGN"
         Top             =   3600
         Width           =   1455
      End
      Begin VB.TextBox txtKeyCode 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   5160
         TabIndex        =   42
         Tag             =   "STEV"
         Top             =   4560
         Width           =   1455
      End
      Begin VB.TextBox txtFlightID 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   5160
         MaxLength       =   1
         TabIndex        =   26
         Tag             =   "FLTI"
         Top             =   3120
         Width           =   375
      End
      Begin VB.TextBox txtCallSign 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1800
         TabIndex        =   24
         Tag             =   "CSGN"
         Top             =   3120
         Width           =   1455
      End
      Begin VB.TextBox txtRelPeriod 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   1
         Left            =   4200
         TabIndex        =   14
         Tag             =   "RELPRDEND"
         Top             =   1680
         Width           =   615
      End
      Begin VB.TextBox txtRelPeriod 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   0
         Left            =   1800
         TabIndex        =   12
         Tag             =   "RELPRDSTART"
         Top             =   1680
         Width           =   615
      End
      Begin VB.CommandButton cmdLookups 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   0
         Left            =   3240
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   2640
         Width           =   375
      End
      Begin VB.TextBox txtAirline 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1800
         TabIndex        =   21
         Tag             =   "AIRLINE"
         Top             =   2640
         Width           =   1455
      End
      Begin VB.CommandButton cmdLookups 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   2
         Left            =   4440
         TabIndex        =   31
         TabStop         =   0   'False
         Top             =   3600
         Width           =   375
      End
      Begin VB.CheckBox chkFTYP 
         Caption         =   "Cancelled"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   4920
         TabIndex        =   61
         Tag             =   "X"
         Top             =   6120
         Width           =   1215
      End
      Begin VB.CheckBox chkFTYP 
         Caption         =   "Noop"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   4920
         TabIndex        =   62
         Tag             =   "N"
         Top             =   6480
         Width           =   975
      End
      Begin VB.CheckBox chkFTYP 
         Caption         =   "Planning"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   2640
         TabIndex        =   59
         Tag             =   "S"
         Top             =   6480
         Width           =   1215
      End
      Begin VB.CheckBox chkFTYP 
         Caption         =   "Operation"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   2640
         TabIndex        =   58
         Tag             =   "O"
         Top             =   6120
         Width           =   1335
      End
      Begin VB.CheckBox chkADID 
         Caption         =   "Rotation"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   57
         Tag             =   "ROT"
         Top             =   6840
         Width           =   1095
      End
      Begin VB.CheckBox chkADID 
         Caption         =   "Departure"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   56
         Tag             =   "DEP"
         Top             =   6480
         Width           =   1215
      End
      Begin VB.CheckBox chkADID 
         Caption         =   "Arrival"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   55
         Tag             =   "ARR"
         Top             =   6120
         Width           =   975
      End
      Begin VB.TextBox txtNature 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1800
         TabIndex        =   39
         Tag             =   "NATURE"
         Top             =   4560
         Width           =   1455
      End
      Begin VB.TextBox txtFlno 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   2
         Left            =   3240
         TabIndex        =   19
         Tag             =   "FLNS"
         Top             =   2160
         Width           =   495
      End
      Begin VB.TextBox txtFlno 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   1
         Left            =   2280
         TabIndex        =   18
         Tag             =   "FLTN"
         Top             =   2160
         Width           =   975
      End
      Begin VB.TextBox txtTime 
         DataField       =   "NULL"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   1
         Left            =   5400
         MaxLength       =   5
         TabIndex        =   5
         Tag             =   "ENDTIME"
         Top             =   720
         Width           =   1095
      End
      Begin VB.TextBox txtDate 
         DataField       =   "NULL"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   1
         Left            =   4200
         MaxLength       =   10
         TabIndex        =   4
         Tag             =   "ENDDATE"
         Top             =   720
         Width           =   1215
      End
      Begin VB.TextBox txtTime 
         DataField       =   "NULL"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   0
         Left            =   3000
         MaxLength       =   5
         TabIndex        =   3
         Tag             =   "STARTTIME"
         Top             =   720
         Width           =   1095
      End
      Begin VB.TextBox txtDate 
         DataField       =   "NULL"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   0
         Left            =   1800
         MaxLength       =   10
         TabIndex        =   2
         Tag             =   "STARTDATE"
         Top             =   720
         Width           =   1215
      End
      Begin VB.TextBox txtFlno 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   0
         Left            =   1800
         TabIndex        =   17
         Tag             =   "ALC2"
         Top             =   2160
         Width           =   495
      End
      Begin VB.TextBox txtDays 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1800
         MaxLength       =   7
         TabIndex        =   8
         Tag             =   "DAYS"
         Top             =   1200
         Width           =   1215
      End
      Begin VB.CheckBox chkDaily 
         Caption         =   "Daily"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3120
         TabIndex        =   9
         Tag             =   "DAILY"
         Top             =   1200
         Width           =   735
      End
      Begin VB.CommandButton cmdLookups 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   1
         Left            =   3240
         TabIndex        =   29
         TabStop         =   0   'False
         Top             =   3600
         Width           =   375
      End
      Begin VB.Label lblLabels 
         BackStyle       =   0  'Transparent
         Caption         =   "Pos Groups:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   3840
         TabIndex        =   52
         Top             =   5520
         Width           =   1215
      End
      Begin VB.Label lblUTC_LT 
         Caption         =   "(UTC)"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   255
         Index           =   0
         Left            =   6600
         TabIndex        =   6
         Top             =   765
         Width           =   495
      End
      Begin VB.Label lblUTC_LT 
         Caption         =   "(Calculated in UTC times)"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   255
         Index           =   1
         Left            =   4080
         TabIndex        =   10
         Top             =   1245
         Width           =   2415
      End
      Begin VB.Label lblLabels 
         BackStyle       =   0  'Transparent
         Caption         =   "Position:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   18
         Left            =   240
         TabIndex        =   49
         Top             =   5520
         Width           =   1215
      End
      Begin VB.Label lblLabels 
         BackStyle       =   0  'Transparent
         Caption         =   "Handling Agent:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   16
         Left            =   240
         TabIndex        =   43
         Top             =   5040
         Width           =   1455
      End
      Begin VB.Label lblLabels 
         BackStyle       =   0  'Transparent
         Caption         =   "Handling Type:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   15
         Left            =   3840
         TabIndex        =   46
         Top             =   5040
         Width           =   1215
      End
      Begin VB.Label lblLabels 
         BackStyle       =   0  'Transparent
         Caption         =   "Via:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   14
         Left            =   3840
         TabIndex        =   35
         Top             =   4080
         Width           =   735
      End
      Begin VB.Label lblLabels 
         BackStyle       =   0  'Transparent
         Caption         =   "Airport:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   13
         Left            =   240
         TabIndex        =   32
         Top             =   4080
         Width           =   1215
      End
      Begin VB.Label lblLabels 
         BackStyle       =   0  'Transparent
         Caption         =   "Regist. / A/C:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   12
         Left            =   240
         TabIndex        =   27
         Top             =   3600
         Width           =   1215
      End
      Begin VB.Label lblLabels 
         BackStyle       =   0  'Transparent
         Caption         =   "Keycode:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   11
         Left            =   3840
         TabIndex        =   41
         Top             =   4560
         Width           =   975
      End
      Begin VB.Label lblLabels 
         BackStyle       =   0  'Transparent
         Caption         =   "Flight ID:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   10
         Left            =   3840
         TabIndex        =   25
         Top             =   3120
         Width           =   855
      End
      Begin VB.Label lblLabels 
         BackStyle       =   0  'Transparent
         Caption         =   "Callsign:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   9
         Left            =   240
         TabIndex        =   23
         Top             =   3120
         Width           =   1215
      End
      Begin VB.Label lblLabels 
         Caption         =   "hour(s) after actual time."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   8
         Left            =   4920
         TabIndex        =   15
         Top             =   1680
         Width           =   2055
      End
      Begin VB.Label lblLabels 
         Caption         =   "hour(s) before and "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   7
         Left            =   2520
         TabIndex        =   13
         Top             =   1680
         Width           =   1575
      End
      Begin VB.Label lblLabels 
         Caption         =   "Relative period:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   6
         Left            =   240
         TabIndex        =   11
         Top             =   1680
         Width           =   1575
      End
      Begin VB.Label lblLabels 
         BackStyle       =   0  'Transparent
         Caption         =   "Airline:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   240
         TabIndex        =   20
         Top             =   2640
         Width           =   1215
      End
      Begin VB.Label lblLabels 
         BackStyle       =   0  'Transparent
         Caption         =   "Nature:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   38
         Top             =   4560
         Width           =   1215
      End
      Begin VB.Label lblLabels 
         BackStyle       =   0  'Transparent
         Caption         =   "Flight number:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   16
         Top             =   2160
         Width           =   1455
      End
      Begin VB.Label lblLabels 
         BackStyle       =   0  'Transparent
         Caption         =   "Days:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   240
         TabIndex        =   7
         Top             =   1200
         Width           =   1215
      End
      Begin VB.Label lblLabels 
         Caption         =   "Absolute period:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   1
         Top             =   720
         Width           =   1575
      End
   End
   Begin VB.Line linSep 
      BorderColor     =   &H80000005&
      Index           =   0
      X1              =   120
      X2              =   7320
      Y1              =   8880
      Y2              =   8880
   End
   Begin VB.Line linSep 
      BorderColor     =   &H80000010&
      BorderWidth     =   2
      Index           =   1
      X1              =   120
      X2              =   7320
      Y1              =   8880
      Y2              =   8880
   End
End
Attribute VB_Name = "FlightView"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public IsSearchForm As Boolean
Public ViewString As String

Private IsFormActivated As Boolean
Private IsUserClickOnDailyCheckBox As Boolean

Private rsView As ADODB.Recordset
Private rsAirlines As ADODB.Recordset
Private rsFlightNatures As ADODB.Recordset
Private rsAirportCodes As ADODB.Recordset
Private rsAircraftPos As ADODB.Recordset
Private rsAircraftPosGroups As ADODB.Recordset
Private rsAircraftTypes As ADODB.Recordset
Private rsAircraftRegNums As ADODB.Recordset
Private rsHandlingAgents As ADODB.Recordset
Private rsHandlingRels As ADODB.Recordset
Private rsHandlingTypes As ADODB.Recordset

Public Sub UpdateView(ByVal CedaCmd As String, ByVal Urno As String, Optional ByVal ChangedFields As String)
    Call UpdateViewCombo(cboView, CedaCmd, Urno, ChangedFields)
End Sub

Private Function GetNewMemUrno() As Long
    Static NewUrno As Long
    
    If NewUrno = 0 Then
        NewUrno = -10000
    Else
        NewUrno = NewUrno - 1
    End If
    
    GetNewMemUrno = NewUrno
End Function

Private Sub ClearAll()
    Dim i As Integer
    
    For i = 0 To 1
        txtDate(i).Text = ""
        txtTime(i).Text = ""
    Next i
    txtDays.Text = ""
    chkDaily.Value = vbUnchecked
    For i = 0 To 1
        txtRelPeriod(i).Text = ""
    Next i
    For i = 0 To 2
        txtFlno(i).Text = ""
    Next i
    txtAirline.Text = ""
    txtCallSign.Text = ""
    txtFlightID.Text = ""
    txtRegNo.Text = ""
    txtAircraftType.Text = ""
    txtAirport.Text = ""
    txtVia.Text = ""
    txtNature.Text = ""
    txtKeyCode.Text = ""
    txtHandlingAgent.Text = ""
    txtHandlingType.Text = ""
    txtPosition.Text = ""
    txtPosGroups.Text = ""
    For i = 0 To chkADID.UBound
        chkADID(i).Value = vbUnchecked
    Next i
    For i = 0 To chkFTYP.UBound
        chkFTYP(i).Value = vbUnchecked
    Next i
End Sub

Private Function GetViewText() As String
    Dim strText As String
    Dim ctl As Control

    strText = "VIEW=" & cboView.Text
    For Each ctl In Me.Controls
        If TypeOf ctl Is TextBox Or TypeOf ctl Is CheckBox Then
            If ctl.Visible Then
                If ctl.Tag <> "" Then
                    If TypeOf ctl Is TextBox Then
                        strText = strText & ";" & ctl.Tag & "=" & ctl.Text
                    Else
                        strText = strText & ";" & ctl.Tag & "=" & CStr(ctl.Value)
                    End If
                End If
            End If
        End If
    Next ctl
    
    GetViewText = strText
End Function

Private Function IsViewValid() As Boolean
    Dim blnValid As Boolean
    Dim blnValidUserDates As Boolean
    Dim i As Integer
    Dim strMessage As String
    Dim dtmDate(1) As Date

    blnValid = False
    
    'check the view name
    If Not IsSearchForm Then
        If Trim(cboView.Text) = "" Then
            MsgBoxEx "Please specify view name!", vbInformation, Me.Caption, , , eCentreDialog
            cboView.SetFocus
            Exit Function
        End If
        
        If Trim(cboView.Text) = "<Default>" Then
            cboView.SetFocus
            Exit Function
        End If
        
        If IsInList(cboView.Text, ";,=") Then
            MsgBoxEx "View name is not valid!", vbInformation, Me.Caption, , , eCentreDialog
            cboView.SetFocus
            Exit Function
        End If
    End If
    
    blnValidUserDates = True
    'Check the timeframe
    For i = 0 To 1
        Select Case txtDate(i).DataField
            Case "ERROR"
                MsgBoxEx "Invalid date!", vbInformation, Me.Caption, , , eCentreDialog
                txtDate(i).SetFocus
                blnValidUserDates = False
                Exit Function
            Case "NULL"
                dtmDate(i) = CDate(0)
                blnValidUserDates = False
            Case Else
                dtmDate(i) = DateSerial(CInt(Right(txtDate(i).DataField, 4)), _
                                        CInt(Mid(txtDate(i).DataField, 4, 2)), _
                                        CInt(Left(txtDate(i).DataField, 2)))
        End Select
        
        Select Case txtTime(i).DataField
            Case "ERROR"
                MsgBoxEx "Invalid time!", vbInformation, Me.Caption, , , eCentreDialog
                txtTime(i).SetFocus
                Exit Function
            Case "NULL"
                If i = 1 Then
                    dtmDate(i) = dtmDate(i) + _
                                 TimeSerial(23, 59, 0)
                End If
            Case Else
                dtmDate(i) = dtmDate(i) + _
                             TimeSerial(CInt(Left(txtTime(i).DataField, 2)), _
                                        CInt(Right(txtTime(i).DataField, 2)), 0)
        End Select
        
        'Convert to UTC
        dtmDate(i) = LocalDateToUTC(dtmDate(i), UtcTimeDiff)
    Next i
    
    If blnValidUserDates Then
        If dtmDate(0) > dtmDate(1) Then
            strMessage = "The begin for the timeframe is after the end of the timeframe!" & vbNewLine & _
                "Begin (UTC): " & Format(dtmDate(0), "dd.mm.yy hh:mm") & " > " & _
                "End (UTC): " & Format(dtmDate(1), "dd.mm.yy hh:mm")
                
            MsgBoxEx strMessage, vbInformation, Me.Caption, , , eCentreDialog
            txtDate(0).SetFocus
            Exit Function
        End If
    End If

    blnValid = True
    
    IsViewValid = blnValid
End Function

Private Sub ReadView(ByVal Urno As Long)
    Dim strText As String
    Dim aText As Variant
    Dim rsObject As ADODB.Recordset
    Dim aObject As Variant
    Dim i As Integer, j As Integer
    Dim ctl As Control

    rsView.Find "URNO = " & CStr(Urno), , , 1
    If Not rsView.EOF Then
        Set rsObject = New ADODB.Recordset
        rsObject.Fields.Append "TAG", adVarChar, 100, adFldIsNullable
        rsObject.Fields.Append "VAL", adVarChar, 100, adFldIsNullable
        rsObject.Open
    
        strText = rsView.Fields("TEXT").Value
        aText = Split(strText, ";")
        
        For i = 0 To UBound(aText)
            aObject = Split(aText(i), "=")
            
            rsObject.AddNew
            For j = 0 To UBound(aObject)
                If j <= rsObject.Fields.Count - 1 Then
                    rsObject.Fields(j).Value = aObject(j)
                End If
            Next j
            rsObject.Update
        Next i
        
        For Each ctl In Me.Controls
            If TypeOf ctl Is TextBox Or TypeOf ctl Is CheckBox Then
                If ctl.Tag <> "" Then
                    rsObject.Find "TAG = '" & ctl.Tag & "'", , , 1
                    If Not rsObject.EOF Then
                        If TypeOf ctl Is TextBox Then
                            ctl.Text = rsObject.Fields("VAL").Value
                        Else
                            ctl.Value = Val(rsObject.Fields("VAL").Value)
                        End If
                    End If
                End If
            End If
        Next ctl
    End If
End Sub

Private Sub cboView_Change()
    Dim lngIndex As Long
    Dim lngUrno As Long
    
    lngIndex = cboView.ListIndex
    If lngIndex = -1 Then
        lngIndex = FindInCombobox(cboView, cboView.Text)
    End If
    Select Case lngIndex
        Case -1
            'Do nothing
        Case Else
            lngUrno = cboView.ItemData(lngIndex)
    
            Call ClearAll
            Call ReadView(lngUrno)
    End Select
End Sub

Private Sub cboView_Click()
    Call cboView_Change
End Sub

Private Sub chkADID_Click(Index As Integer)
    Dim i As Integer
    Dim blnIsChecked As Boolean

    If chkADID(Index).Value = vbUnchecked Then
        blnIsChecked = False
        For i = 0 To 1
            If i <> Index Then
                If chkADID(i).Value = vbChecked Then
                    blnIsChecked = True
                    Exit For
                End If
            End If
        Next i
        If Not blnIsChecked Then
            chkADID(Index).Value = vbChecked
        End If
    End If
End Sub

Private Sub chkDaily_Click()
    If IsUserClickOnDailyCheckBox Then
        If chkDaily.Value = vbChecked Then
            txtDays.Text = "1234567"
        Else
            txtDays.Text = ""
            txtDays.SetFocus
        End If
    End If
End Sub

Private Sub chkFTYP_Click(Index As Integer)
    Dim i As Integer
    Dim blnIsChecked As Boolean

    If chkFTYP(Index).Value = vbUnchecked Then
        blnIsChecked = False
        For i = 0 To chkFTYP.UBound
            If i <> Index Then
                If chkFTYP(i).Value = vbChecked Then
                    blnIsChecked = True
                    Exit For
                End If
            End If
        Next i
        If Not blnIsChecked Then
            chkFTYP(Index).Value = vbChecked
        End If
    End If
End Sub

Private Sub cmdApply_Click()
    Dim lngIndex As Long
    Dim lngUrno As Long
    Dim strViewText As String
    Dim AppName As String
    
    Call txtFlno_Validate(1, False)
    If IsViewValid() Then
        strViewText = GetViewText()
            
        lngIndex = cboView.ListIndex
        If lngIndex = -1 Then
            lngIndex = FindInCombobox(cboView, cboView.Text)
        Else
            If cboView.Text <> cboView.List(lngIndex) Then
                lngIndex = FindInCombobox(cboView, cboView.Text)
            End If
        End If
        If lngIndex > -1 Then 'Edit
            lngUrno = cboView.ItemData(lngIndex)
        
            rsView.Find "URNO = " & CStr(lngUrno), , , 1
            If Not rsView.EOF Then
                rsView.Fields("TEXT").Value = strViewText
                rsView.Update
            End If
            
            If FormIsLoaded("MainButtons") Then
                MainButtons.cboView.ListIndex = lngIndex
            End If
        Else 'Add New
            lngUrno = GetNewMemUrno()
            AppName = Mid(colParameters.Item("AppName"), 2)
            AppName = Left(AppName, Len(AppName) - 1)
        
            rsView.AddNew
            rsView.Fields("URNO").Value = lngUrno
            rsView.Fields("APPN").Value = AppName
            rsView.Fields("CKEY").Value = "GENERAL"
            rsView.Fields("CTYP").Value = "VIEW-DATA"
            rsView.Fields("TEXT").Value = strViewText
            rsView.Update
            
            cboView.AddItem cboView.Text
            cboView.ItemData(cboView.NewIndex) = rsView.Fields("URNO").Value
            If FormIsLoaded("MainButtons") Then
                MainButtons.cboView.AddItem cboView.Text
                MainButtons.cboView.ItemData(MainButtons.cboView.NewIndex) = rsView.Fields("URNO").Value
                MainButtons.cboView.ListIndex = MainButtons.cboView.NewIndex
            End If
        End If
        
        ViewString = strViewText
        
        Me.Hide
    End If
End Sub

Private Sub cmdCancel_Click()
    ViewString = ""
    Me.Hide
End Sub

Private Sub cmdDelete_Click()
    Dim lngIndex As Long
    Dim strUrno As String
    Dim strResult As String
    Dim strCommand As String
    Dim strTable As String
    Dim strFields As String
    Dim strData As String
    Dim strWhere As String
    
    lngIndex = cboView.ListIndex
    If lngIndex = -1 Then
        lngIndex = FindInCombobox(cboView, cboView.Text)
    Else
        If cboView.Text <> cboView.List(lngIndex) Then
            lngIndex = FindInCombobox(cboView, cboView.Text)
        End If
    End If
    If lngIndex > 0 Then
        strUrno = CStr(cboView.ItemData(lngIndex))
    
        strTable = "VCDTAB"
        strCommand = "DRT"
        strFields = ""
        strData = ""
        strWhere = "WHERE URNO = " & strUrno
        oUfisServer.CallCeda strResult, strCommand, strTable, strFields, strData, strWhere, "", 0, False, False
        
        rsView.Find "URNO = " & strUrno, , , 1
        If Not rsView.EOF Then
            rsView.Delete
            rsView.MoveNext
        End If
        
        cboView.RemoveItem lngIndex
        cboView.ListIndex = 0
        
        If FormIsLoaded("MainButtons") Then
            MainButtons.cboView.RemoveItem lngIndex
        End If
    End If
End Sub

Private Sub cmdLookups_Click(Index As Integer)
    Dim strSelectedData As String
    Dim oText As TextBox

    Select Case Index
        Case 0 'Airline
            Set LookupForm.DataSource = rsAirlines
            LookupForm.Columns = "ALC2,ALC3,ALFN"
            LookupForm.ColumnHeaders = "ALC2,ALC3,Description"
            LookupForm.ColumnWidths = "600,800,6000"
            LookupForm.KeyColumns = "ALC2"
            LookupForm.Caption = "Airlines"
            LookupForm.AllowMultiSelection = True
            LookupForm.IsOnTop = pblnIsOnTop
            LookupForm.Show vbModal, Me
        
            Set oText = txtAirline
        Case 1 'Registration
            Set LookupForm.DataSource = rsAircraftRegNums
            LookupForm.Columns = "REGN,ACT3,ACT5"
            LookupForm.ColumnHeaders = "Registration Number,ACT3,ACT5"
            LookupForm.ColumnWidths = "2000,1200,1500"
            LookupForm.KeyColumns = "REGN"
            LookupForm.Caption = "Aircraft Registration Numbers"
            LookupForm.AllowMultiSelection = True
            LookupForm.IsOnTop = pblnIsOnTop
            LookupForm.Show vbModal, Me
        
            Set oText = txtRegNo
        Case 2 'Aircraft Type
            Set LookupForm.DataSource = rsAircraftTypes
            LookupForm.Columns = "ACT3,ACT5"
            LookupForm.ColumnHeaders = "ACT3,ACT5"
            LookupForm.ColumnWidths = "1200,1500"
            LookupForm.KeyColumns = "ACT3"
            LookupForm.Caption = "Aircraft Types"
            LookupForm.AllowMultiSelection = True
            LookupForm.IsOnTop = pblnIsOnTop
            LookupForm.Show vbModal, Me
        
            Set oText = txtAircraftType
        Case 3, 4 'Airport, Via
            Set LookupForm.DataSource = rsAirportCodes
            LookupForm.Columns = "APC3,APC4,APFN"
            LookupForm.ColumnHeaders = "APC3,APC4,Airport Name"
            LookupForm.ColumnWidths = "600,800,6000"
            LookupForm.KeyColumns = "APC3"
            LookupForm.Caption = "Airports"
            LookupForm.AllowMultiSelection = True
            LookupForm.IsOnTop = pblnIsOnTop
            LookupForm.Show vbModal, Me
        
            If Index = 3 Then
                Set oText = txtAirport
            Else
                Set oText = txtVia
            End If
        Case 5 'Nature
            Set LookupForm.DataSource = rsFlightNatures
            LookupForm.Columns = "TTYP,TNAM"
            LookupForm.ColumnHeaders = "Code,Description"
            LookupForm.ColumnWidths = "600,6000"
            LookupForm.KeyColumns = "TTYP"
            LookupForm.Caption = "Flight Natures"
            LookupForm.AllowMultiSelection = True
            LookupForm.IsOnTop = pblnIsOnTop
            LookupForm.Show vbModal, Me
        
            Set oText = txtNature
        Case 6 'Handling Agent
            Set LookupForm.DataSource = rsHandlingAgents
            LookupForm.Columns = "HSNA,HNAM"
            LookupForm.ColumnHeaders = "Code,Description"
            LookupForm.ColumnWidths = "600,6000"
            LookupForm.KeyColumns = "HSNA"
            LookupForm.Caption = "Handling Agents"
            LookupForm.AllowMultiSelection = True
            LookupForm.IsOnTop = pblnIsOnTop
            LookupForm.Show vbModal, Me
        
            Set oText = txtHandlingAgent
        Case 7 'Handling Type
            Set LookupForm.DataSource = rsHandlingTypes
            LookupForm.Columns = "HTYP,HNAM"
            LookupForm.ColumnHeaders = "Code,Description"
            LookupForm.ColumnWidths = "600,6000"
            LookupForm.KeyColumns = "HTYP"
            LookupForm.Caption = "Handling Types"
            LookupForm.AllowMultiSelection = True
            LookupForm.IsOnTop = pblnIsOnTop
            LookupForm.Show vbModal, Me
        
            Set oText = txtHandlingType
        Case 8 'Position
            Set LookupForm.DataSource = rsAircraftPos
            LookupForm.Columns = "PNAM"
            LookupForm.ColumnHeaders = "Name"
            LookupForm.ColumnWidths = "6000"
            LookupForm.KeyColumns = "PNAM"
            LookupForm.Caption = "Aircraft Position"
            LookupForm.AllowMultiSelection = True
            LookupForm.IsOnTop = pblnIsOnTop
            LookupForm.Show vbModal, Me
        
            Set oText = txtPosition
        Case 9 'Position Groups
            Set LookupForm.DataSource = rsAircraftPosGroups
            LookupForm.Columns = "GRPN"
            LookupForm.ColumnHeaders = "Name"
            LookupForm.ColumnWidths = "6000"
            LookupForm.KeyColumns = "GRPN"
            LookupForm.Caption = "Aircraft Position Groups"
            LookupForm.AllowMultiSelection = True
            LookupForm.IsOnTop = pblnIsOnTop
            LookupForm.Show vbModal, Me
        
            Set oText = txtPosGroups
    End Select
    
    strSelectedData = LookupForm.SelectedData
    If strSelectedData <> "" Then
        oText.Text = RTrim(strSelectedData)
    End If
    oText.SetFocus
End Sub

Private Sub cmdOK_Click()
    Dim strViewText As String
    
    Call txtDate_Validate(0, False)
    Call txtDate_Validate(1, False)
    Call txtFlno_Validate(1, False)
    If IsViewValid() Then
        strViewText = GetViewText()
            
        ViewString = strViewText
        
        Me.Hide
    End If
End Sub

Private Sub cmdSave_Click()
    Dim lngIndex As Long
    Dim lngUrno As Long
    Dim strResult As String
    Dim strCommand As String
    Dim strTable As String
    Dim strFields As String
    Dim strData As String
    Dim strWhere As String
    Dim strViewText As String
    Dim AppName As String
    
    Call txtFlno_Validate(1, False)
    If IsViewValid() Then
        strTable = "VCDTAB"
        strViewText = GetViewText()
            
        lngIndex = cboView.ListIndex
        If lngIndex = -1 Then
            lngIndex = FindInCombobox(cboView, cboView.Text)
        Else
            If cboView.Text <> cboView.List(lngIndex) Then
                lngIndex = FindInCombobox(cboView, cboView.Text)
            End If
        End If
        If lngIndex > -1 Then 'Edit
            lngUrno = cboView.ItemData(lngIndex)
            
            strCommand = "URT"
            strFields = "TEXT"
            strData = strViewText
            strWhere = "WHERE URNO = " & CStr(lngUrno)
            oUfisServer.CallCeda strResult, strCommand, strTable, strFields, strData, strWhere, "", 0, False, False
            
            rsView.Find "URNO = " & CStr(lngUrno), , , 1
            If Not rsView.EOF Then
                rsView.Fields(strFields).Value = strViewText
                rsView.Update
            End If
            
            If FormIsLoaded("MainButtons") Then
                MainButtons.cboView.ListIndex = lngIndex
            End If
        Else 'Add New
            lngUrno = CLng(GetNewUrno())
            AppName = Mid(colParameters.Item("AppName"), 2)
            AppName = Left(AppName, Len(AppName) - 1)
        
            strCommand = "IRT"
            strFields = "URNO,APPN,CKEY,CTYP,PKNO,TEXT,VAFR"
            strData = CStr(lngUrno) & "," & AppName & ",GENERAL,VIEW-DATA," & LoginUserName & "," & _
                UfisLib.CleanString(strViewText, CLIENT_TO_SERVER, False) & "," & Format(Now, "yyyyMMddhhmmss")
            strWhere = ""
            oUfisServer.CallCeda strResult, strCommand, strTable, strFields, strData, strWhere, "", 0, False, False
            
            rsView.AddNew
            rsView.Fields("URNO").Value = lngUrno
            rsView.Fields("APPN").Value = AppName
            rsView.Fields("CKEY").Value = "GENERAL"
            rsView.Fields("CTYP").Value = "VIEW-DATA"
            rsView.Fields("TEXT").Value = strViewText
            rsView.Update
            
            cboView.AddItem cboView.Text
            cboView.ItemData(cboView.NewIndex) = rsView.Fields("URNO").Value
            If FormIsLoaded("MainButtons") Then
                MainButtons.cboView.AddItem cboView.Text
                MainButtons.cboView.ItemData(MainButtons.cboView.NewIndex) = rsView.Fields("URNO").Value
                MainButtons.cboView.ListIndex = MainButtons.cboView.NewIndex
            End If
        End If
        
        ViewString = strViewText
        
        Me.Hide
    End If
End Sub

Private Sub Form_Activate()
    If Not IsFormActivated Then
        If pblnIsOnTop Then
            SetFormOnTop Me, pblnIsOnTop
        End If
        IsFormActivated = True
    End If
    
    txtDate(0).SetFocus
End Sub

Private Sub Form_Load()
    Dim i As Integer
    
    Set rsView = colDataSources.Item("VCDTAB")
    Set rsAirlines = colDataSources.Item("ALTTAB")
    Set rsFlightNatures = colDataSources.Item("NATTAB")
    Set rsAirportCodes = colDataSources.Item("APTTAB")
    Set rsAircraftPos = colDataSources.Item("PSTTAB")
    Set rsAircraftPosGroups = colDataSources.Item("GRNTAB")
    Set rsAircraftTypes = colDataSources.Item("ACTTAB")
    Set rsAircraftRegNums = colDataSources.Item("ACRTAB")
    Set rsHandlingAgents = colDataSources.Item("HAGTAB")
    Set rsHandlingRels = colDataSources.Item("HAITAB")
    Set rsHandlingTypes = colDataSources.Item("HTYTAB")

    IsFormActivated = False
    IsUserClickOnDailyCheckBox = True
    
'    For i = 0 To chkADID.UBound
'        chkADID(i).Value = vbChecked
'    Next i
'
'    chkFTYP(0).Value = vbChecked
    
    If IsSearchForm Then
        Me.Caption = "Search Flights in Current View"
        chkADID(2).Visible = False
        cboView.Visible = False
        cmdSave.Visible = False
        cmdDelete.Visible = False
        cmdApply.Visible = False
        cmdOK.Visible = True
        cmdOK.Default = True
    Else
        Call FillViewComboBox(rsView, cboView)
    End If
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Call cmdCancel_Click
        Cancel = True
    End If
End Sub

Private Sub txtAircraftType_KeyPress(KeyAscii As Integer)
    KeyAscii = UCaseAsc(KeyAscii)
End Sub

Private Sub txtAirline_KeyPress(KeyAscii As Integer)
    KeyAscii = UCaseAsc(KeyAscii)
End Sub

Private Sub txtAirport_KeyPress(KeyAscii As Integer)
    KeyAscii = UCaseAsc(KeyAscii)
End Sub

Private Sub txtCallSign_KeyPress(KeyAscii As Integer)
    KeyAscii = UCaseAsc(KeyAscii)
End Sub

Private Sub txtDate_Change(Index As Integer)
    Dim strDate As String
    
    strDate = CheckValidDateExt(txtDate(Index).Text)
    If strDate = "ERROR" Then
        txtDate(Index).BackColor = &HC0C0FF
    Else
        txtDate(Index).BackColor = vbWindowBackground
    End If
    
    txtDate(Index).DataField = strDate
End Sub

Private Sub txtDate_KeyPress(Index As Integer, KeyAscii As Integer)
    Const VALID_CHARS = "0123456789-./ "
    
    Select Case KeyAscii
        Case vbKeyBack
        Case Else
            If InStr(VALID_CHARS, Chr(KeyAscii)) <= 0 Then
                KeyAscii = 0
            End If
    End Select
End Sub

Private Sub txtDate_Validate(Index As Integer, Cancel As Boolean)
    If txtDate(Index).DataField <> "ERROR" And txtDate(Index).DataField <> "NULL" Then
        txtDate(Index).Text = txtDate(Index).DataField
    End If
End Sub

Private Sub txtDays_Change()
    IsUserClickOnDailyCheckBox = False
    If Len(Trim(txtDays.Text)) = 7 Then
        chkDaily.Value = vbChecked
    Else
        chkDaily.Value = vbUnchecked
    End If
    IsUserClickOnDailyCheckBox = True
End Sub

Private Sub txtDays_KeyPress(KeyAscii As Integer)
    Const VALID_NUMBERS = "1234567"
    
    Select Case KeyAscii
        Case vbKeyBack
        Case Else
            If InStr(VALID_NUMBERS, Chr(KeyAscii)) <= 0 Then
                KeyAscii = 0
            Else
                If InStr(txtDays.Text, Chr(KeyAscii)) > 0 Then
                    If InStr(txtDays.SelText, Chr(KeyAscii)) <= 0 Then
                        KeyAscii = 0
                    End If
                End If
            End If
    End Select
End Sub

Private Sub txtFlightID_KeyPress(KeyAscii As Integer)
    KeyAscii = UCaseAsc(KeyAscii)
End Sub

Private Sub txtFlno_KeyPress(Index As Integer, KeyAscii As Integer)
    KeyAscii = UCaseAsc(KeyAscii)
End Sub

Private Sub txtFlno_Validate(Index As Integer, Cancel As Boolean)
    Dim strValue As String
    
    If Index = 1 Then 'FLTN
        strValue = Trim(txtFlno(Index).Text)
        If Len(strValue) < 3 Then
            txtFlno(Index).Text = Format(strValue, "000")
        End If
    End If
End Sub

Private Sub txtHandlingAgent_KeyPress(KeyAscii As Integer)
    KeyAscii = UCaseAsc(KeyAscii)
End Sub

Private Sub txtHandlingType_KeyPress(KeyAscii As Integer)
    KeyAscii = UCaseAsc(KeyAscii)
End Sub

Private Sub txtKeyCode_KeyPress(KeyAscii As Integer)
    KeyAscii = UCaseAsc(KeyAscii)
End Sub

Private Sub txtNature_KeyPress(KeyAscii As Integer)
    KeyAscii = UCaseAsc(KeyAscii)
End Sub

Private Sub txtPosGroups_KeyPress(KeyAscii As Integer)
    KeyAscii = UCaseAsc(KeyAscii)
End Sub

Private Sub txtPosition_KeyPress(KeyAscii As Integer)
    KeyAscii = UCaseAsc(KeyAscii)
End Sub

Private Sub txtRegNo_KeyPress(KeyAscii As Integer)
    KeyAscii = UCaseAsc(KeyAscii)
End Sub

Private Sub txtRelPeriod_KeyPress(Index As Integer, KeyAscii As Integer)
    Const VALID_CHARS = "0123456789"
    
    Select Case KeyAscii
        Case vbKeyBack
        Case Else
            If InStr(VALID_CHARS, Chr(KeyAscii)) <= 0 Then
                KeyAscii = 0
            End If
    End Select
End Sub

Private Sub txtTime_Change(Index As Integer)
    Dim strTime As String
    
    strTime = CheckValidTimeExt(txtTime(Index).Text)
    If strTime = "ERROR" Then
        txtTime(Index).BackColor = &HC0C0FF
    Else
        txtTime(Index).BackColor = vbWindowBackground
    End If
    
    txtTime(Index).DataField = strTime
End Sub

Private Sub txtTime_KeyPress(Index As Integer, KeyAscii As Integer)
    Const VALID_CHARS = "0123456789:. "
    
    Select Case KeyAscii
        Case vbKeyBack
        Case Else
            If InStr(VALID_CHARS, Chr(KeyAscii)) <= 0 Then
                KeyAscii = 0
            End If
    End Select
End Sub

Private Sub txtTime_Validate(Index As Integer, Cancel As Boolean)
    If txtTime(Index).DataField <> "ERROR" And txtTime(Index).DataField <> "NULL" Then
        txtTime(Index).Text = txtTime(Index).DataField
    End If
End Sub

Private Sub txtVia_KeyPress(KeyAscii As Integer)
    KeyAscii = UCaseAsc(KeyAscii)
End Sub
