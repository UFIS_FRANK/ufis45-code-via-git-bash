VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DeicingDialog"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Enum TimeZoneEnum
    UTC = 0
    LocalTime = 1
End Enum

Private m_IniFilePath As String
Private m_UserName As String
Private m_AppName As String
Private m_AppVersion As String
Private m_TimeZone As TimeZoneEnum
Private m_HandleBc As Boolean
'Private m_ShowSplashScreen As Boolean

Public Property Get AppName() As String
    AppName = m_AppName
End Property

Public Property Let AppName(ByVal sAppName As String)
    m_AppName = sAppName
    p_AppName = m_AppName
End Property

Public Property Get AppVersion() As String
    AppVersion = m_AppVersion
End Property

Public Property Let AppVersion(ByVal sAppVersion As String)
    m_AppVersion = sAppVersion
    p_AppVersion = m_AppVersion
End Property

Public Property Get UserName() As String
    UserName = m_UserName
End Property

Public Property Let UserName(ByVal sUserName As String)
    m_UserName = sUserName
    LoginUserName = m_UserName
End Property

Public Property Get IniFilePath() As String
    IniFilePath = m_IniFilePath
End Property

Public Property Let IniFilePath(ByVal sIniFilePath As String)
    m_IniFilePath = sIniFilePath
    myIniFullName = m_IniFilePath
End Property

Public Property Get TimeZone() As TimeZoneEnum
    TimeZone = m_TimeZone
End Property

Public Property Let TimeZone(eTimeZone As TimeZoneEnum)
    m_TimeZone = eTimeZone
    IsUTCTimeZone = (m_TimeZone = UTC)
End Property

Public Property Get HandleBc() As Boolean
    HandleBc = m_HandleBc
End Property

Public Property Let HandleBc(ByVal bHandleBc As Boolean)
    m_HandleBc = bHandleBc
    pblnHandleBc = m_HandleBc
End Property

'Public Property Get ShowSplashScreen() As Boolean
'    ShowSplashScreen = m_ShowSplashScreen
'End Property
'
'Public Property Let ShowSplashScreen(ByVal bShowSplashScreen As Boolean)
'    m_ShowSplashScreen = bShowSplashScreen
'    pblnShowSplashScreen = m_ShowSplashScreen
'End Property

Public Function LoadBasicData()
    LoadBasicData = StartApp(True)
End Function

Public Function ShowDialog(Optional ByVal IsOnTop As Boolean = False) As Boolean
    pblnIsOnTop = IsOnTop
    ShowDialog = StartApp(False)
End Function

Public Function ShowVersion()
    MsgBox "Ver 1.0"
End Function
