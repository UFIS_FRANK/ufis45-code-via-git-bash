#if !defined(AFX_CHADIAGRAM_H__DAC99481_8801_11D1_B43F_0000B45A33F5__INCLUDED_)
#define AFX_CHADIAGRAM_H__DAC99481_8801_11D1_B43F_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ChaDiagram.h : header file
//

#include <ChaDiaViewer.h>
#include <CCSClientWnd.h>
#include <CCS3dStatic.h>
#include <CCSButtonCtrl.h>
#include <CCSTimeScale.h>
#include <CViewer.h>
#include <ChaChart.h>
#include <CCSEdit.h>
//#include <vector>
/////////////////////////////////////////////////////////////////////////////
// ChaDiagram frame

enum 
{
	CHA_ZEITRAUM_PAGE,
	CHA_FLUGSUCHEN_PAGE,
	CHA_GEOMETRIE_PAGE
};


class ChaDiagram : public CFrameWnd
{
	DECLARE_DYNCREATE(ChaDiagram)
public:
	ChaDiagram();           // protected constructor used by dynamic creation
	~ChaDiagram();

	bool ShowFlight(long lpUrno);

	CDialogBar omDialogBar;

	void GetCurView( char * pcpView);
	void ViewSelChange(char *pcpView);

	void SetWndPos();
    void PositionChild();
    void SetTSStartTime(CTime opTSStartTime);
	void UpdateComboBox();
	void ChangeViewTo(const char *pcpViewName,bool RememberPositions = true);
	void PrePlanMode(BOOL bpToSet,CTime opPrePlanTime);
	bool IsPassFlightFilter(CCAFLIGHTDATA *prpFlight);
	bool HasAllocatedChas(CCSPtrArray<CHADATA> &ropChaList);
	void ShowChaDuration(CTime opCkbs, CTime opCkes);
	void ShowTime(const CTime &ropTime);

	void ActivateTables();
	bool UpdateDia();


	bool GetLoadedAllocations(CMapStringToString& opCicUrnoMap);
	bool GetLoadedCha(CCSPtrArray<CHADATA> &opCha, CMapStringToString& opTypMap, bool bpOnlySel, bool bpMustBeInOneDay);
	void SaveToReg();
	void SetFocusToDiagram();	 //PRF 8363
// Attributes
public:

	ChaDiagramViewer omViewer;

	CString omOldWhere;
	void LoadFlights(char *pspView = NULL);

    CCS3DStatic omTime;
    CCS3DStatic omDate;
    CCS3DStatic omTSDate;
	bool bmAlreadyAlloc;

    CButton m_KlebeFkt;
    CCSTimeScale omTimeScale;
    CBitmapButton omBB1, omBB2;
    
    CCSClientWnd omClientWnd;
    CStatusBar omStatusBar;
    
    CTime omStartTime;
    CTimeSpan omDuration;
    CTime omTSStartTime;
    CTimeSpan omTSDuration;
    CTimeSpan omTSInterval;
    
	ChaChart *pomChart;

    int imFirstVisibleChart;
    int imStartTimeScalePos;
    
	CString omCaptionText;
	bool bmIsViewOpen;


	int imWhichMonitor; 
	int imMonitorCount;


// Redisplay all methods for DDX call back function
public:
	BOOL bmNoUpdatesNow;
	bool bmRepaintAll;

	void RedisplayAll();
	bool bmReadAll;
// Time Band data, use the TIMENULL value for hiding the marker
public:
	void SetTimeBand(CTime opStartTime, CTime opEndTime);
	void UpdateTimeBand();
 	void SetMarkTime(CTime opStartTime, CTime opEndTime);
private:
	CTime omTimeBandStartTime;
	CTime omTimeBandEndTime;

	CString m_key;

private:
    CPoint omMaxTrackSize;
    CPoint omMinTrackSize;
    COLORREF lmBkColor;
    COLORREF lmTextColor;
    COLORREF lmHilightColor;
	bool bmTimeLine;


// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ChaDiagram)
	virtual BOOL DestroyWindow();
	//}}AFX_VIRTUAL

// Implementation

	// Generated message map functions
	//{{AFX_MSG(ChaDiagram)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnBeenden();
	afx_msg void OnPrintGantt();
	afx_msg void OnDestroy();
	afx_msg void OnAnsicht();
	afx_msg void OnClose();
    afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
    afx_msg void OnPaint();
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg LONG OnPositionChild(WPARAM wParam, LPARAM lParam);
    afx_msg LONG OnUpdateDiagram(WPARAM wParam, LPARAM lParam);
	afx_msg void OnViewSelChange();
	afx_msg void OnCloseupView();
	afx_msg void OnZeit();
	afx_msg void OnSearch();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg LONG RepaintAll(WPARAM wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.


#endif // !defined(AFX_CHADIAGRAM_H__DAC99481_8801_11D1_B43F_0000B45A33F5__INCLUDED_)
