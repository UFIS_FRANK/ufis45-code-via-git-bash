// ReportReasonForChangeViewer.h: interface for the ReportReasonForChangeViewer class.

//////////////////////////////////////////////////////////////////////

#if !defined(AFX_REPORTREASONFORCHANGEVIEWER_H__DC6C9D70_F63F_434B_920F_598BEB4EE05B__INCLUDED_)
#define AFX_REPORTREASONFORCHANGEVIEWER_H__DC6C9D70_F63F_434B_920F_598BEB4EE05B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CVIEWER.H"
#include "ReportReasonForChange.h"
#include <CCSTable.h>
#include <CCSDefines.h>
#include <CViewer.h>
#include <CCSPrint.h>
 
#define REASON_FOR_CHANGE_COLCOUNT 10

struct REPORTREASONFORCHANGE_LINEDATA
{ 
	long Urno;
	char Flno[12];
	long Furn;
	char Ftyp[3];
	CTime Scdt;
	CTime Cdat;
	char Aloc[13];
	char Code[7];
	char Rema[130];
	char Usec[35];
	char OVal[12];
	char NVal[12];

	REPORTREASONFORCHANGE_LINEDATA()
	{
		memset(this,'\0',sizeof(*this));
	}
};

class ReportReasonForChangeViewer : public CViewer  
{

public:
	ReportReasonForChangeViewer();
	virtual ~ReportReasonForChangeViewer();

	// Connects the viewer with a table
    void Attach(CCSTable *popTable);
	// Load the intern line data from the given data and displays the table
    void ChangeViewTo(const CCSPtrArray<REASON_FOR_CHANGE> &ropData, char *popDateStr);
 
	// Rebuild the table from the intern data
 	void UpdateDisplay(void);
	// Print table to paper
	void PrintTableView(void);
	// Print table to file
	bool PrintPlanToFile(char *popFilePath);
	
	int GetFlightCount(void) const;

	CString omTableName;
	CString omPrintName;
	CString omFileName;
 	
private:

	// columns width of the table in char
	static int imTableColCharWidths[REASON_FOR_CHANGE_COLCOUNT];
	// columns width of the table in pixels
	int imTableColWidths[REASON_FOR_CHANGE_COLCOUNT];
	int imPrintColWidths[REASON_FOR_CHANGE_COLCOUNT];
	// table header strings
	CString omTableHeadlines[REASON_FOR_CHANGE_COLCOUNT];


	const int imOrientation;

	// Table fonts and widths for displaying
	CFont &romTableHeaderFont; 
	CFont &romTableLinesFont;
	const float fmTableHeaderFontWidth; 
	const float fmTableLinesFontWidth;

	// Table fonts and widths for printing
	CFont *pomPrintHeaderFont; 
	CFont *pomPrintLinesFont;
	float fmPrintHeaderFontWidth; 
	float fmPrintLinesFontWidth;

	
	void DrawTableHeader(void);
	// Transfer the data of the database to the intern data structures
	void MakeLines(const CCSPtrArray<REASON_FOR_CHANGE> &ropData);
	// Copy the data from the db-record to the table-record
	bool MakeLineData(REPORTREASONFORCHANGE_LINEDATA &rrpLineData, const REASON_FOR_CHANGE &rrpFlight);
	// Create a intern table data record
	int  CreateLine(const REPORTREASONFORCHANGE_LINEDATA &rrpLine);
	// Fills one row of the table
	bool MakeColList(const REPORTREASONFORCHANGE_LINEDATA &rrpLine, CCSPtrArray<TABLE_COLUMN> &ropColList) const;
	// Compares two lines of the table
	int CompareLines(const REPORTREASONFORCHANGE_LINEDATA &rrpLine1, const REPORTREASONFORCHANGE_LINEDATA &rrpLine2) const;

	bool PrintTableHeader(CCSPrint &ropPrint);

 	bool PrintTableLine(CCSPrint &ropPrint, int ipLineNo);
	// delete intern table lines
    void DeleteAll(void);
	// Date string
	char *pomDateStr;
	char *pomAirline;
	char *pomDelayCode;
	int ipmDelayTime;
	int imNoArr;
	int imNoDep;
	CString omFooterName; 

 	// Table
 	CCSTable *pomTable;

	// Table Data
	CCSPtrArray<REPORTREASONFORCHANGE_LINEDATA> omLines;

};

#endif // !defined(AFX_REPORTREASONFORCHANGEVIEWER_H__DC6C9D70_F63F_434B_920F_598BEB4EE05B__INCLUDED_)
