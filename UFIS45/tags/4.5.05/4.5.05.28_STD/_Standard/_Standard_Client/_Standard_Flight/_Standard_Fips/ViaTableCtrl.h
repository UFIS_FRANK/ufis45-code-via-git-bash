#if !defined(AFX_VIATABLECTRL_H__FCB37FA1_2C47_11D2_858C_0000C04D916B__INCLUDED_)
#define AFX_VIATABLECTRL_H__FCB37FA1_2C47_11D2_858C_0000C04D916B__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ViaTableCtrl.h : header file
//

 
#include <CCSTable.h>
#include <CCSGlobl.h>
#include <CCSButtonCtrl.h>

/////////////////////////////////////////////////////////////////////////////
// ViaTableCtrl window

class ViaTableCtrl : public CDialog
{
// Construction
public:
	ViaTableCtrl(CWnd *popParent = NULL, CString opFields = "");

	void SetSecStat(char clStat);

	char cmSecStat;

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ViaTableCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~ViaTableCtrl();


	void Enable( BOOL bpEnable );

	CCSTable *pomTable;

	//CButton *pomButton;
	CCSButtonCtrl *pomButton;

	CStatic *pomBorder;

	CString omViaList;

	CStringArray omFields;	

	CTime omRefDat;

	bool bmLocal;

	bool bmEdit;

	CWnd *pomParent;

	bool bmShowButton;

//	void SetViaList(CString opList, CTime opRefDat);
	void SetViaList(CString opList, CTime opRefDat, bool bpLocalTime = false);

	CString GetViaList();

	void GetViaList(char *pcpViaList);

	void InitTable();

	void SetPosition(CRect opRect);

	CString GetField(VIADATA *prpVia, CString opField);

	void GetAttrib(CCSEDIT_ATTRIB &ropAttrib, CString opField);

	CString GetLabel(CString opField);

	bool GetStatus(CString &opMessage);
	CString  GetStatus();

	void ShowTable();

//	void SetField(VIADATA *prpVia, CString opField, CString opValue);
	void SetField(VIADATA *prpVia, CString opField, CString opValue, CTime opRefDat = TIMENULL);

	void SetSelectMode(bool bpMode);

	void ShowButton(bool bpShow){bmShowButton =   bpShow; };

	CString CreateViaList(CCSPtrArray<VIADATA> *popVias, CTime opRefDat = TIMENULL);


	CString GetViaList(CString opViaList, CTime opRefDat);

	void SelectColumn(int ipColumn, bool bpSelect);

	int GetSelColumns();

	// Generated message map functions
protected:
	//{{AFX_MSG(ViaTableCtrl)
	afx_msg void OnShowTable();
	afx_msg LONG OnEditChanged( UINT wParam, LPARAM lParam);
	afx_msg LONG OnTableLButtonDown( UINT wParam, LPARAM lParam);
	afx_msg LONG OnTableIPEditKillfocus( UINT wParam, LPARAM lParam);
	afx_msg LONG OnTableMenuSelect( UINT wParam, LPARAM lParam);
	afx_msg LONG OnTableSelChanged( UINT wParam, LPARAM lParam);
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VIATABLECTRL_H__FCB37FA1_2C47_11D2_858C_0000C04D916B__INCLUDED_)
