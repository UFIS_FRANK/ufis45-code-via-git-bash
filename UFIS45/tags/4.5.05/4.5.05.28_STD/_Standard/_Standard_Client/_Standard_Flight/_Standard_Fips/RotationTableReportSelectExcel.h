#if !defined(AFX_ROTATIONTABLEREPORTSELECTEXCEL_H__8A798B4E_5329_40DA_A80D_4DC0F802143B__INCLUDED_)
#define AFX_ROTATIONTABLEREPORTSELECTEXCEL_H__8A798B4E_5329_40DA_A80D_4DC0F802143B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// rotationtablereportselectexcel.h : header file
//
#include <resrc1.h>

/////////////////////////////////////////////////////////////////////////////
// RotationTableReportSelectExcel dialog

class RotationTableReportSelectExcel : public CDialog
{
// Construction
public:
	RotationTableReportSelectExcel(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(RotationTableReportSelectExcel)
	enum { IDD = IDD_ROTTABLESEL_EXCEL };
	BOOL	m_bDeparture;
	BOOL	m_bArrival;
	BOOL	m_bAirborn;
	BOOL	m_bLanded;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(RotationTableReportSelectExcel)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(RotationTableReportSelectExcel)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROTATIONTABLEREPORTSELECTEXCEL_H__8A798B4E_5329_40DA_A80D_4DC0F802143B__INCLUDED_)
