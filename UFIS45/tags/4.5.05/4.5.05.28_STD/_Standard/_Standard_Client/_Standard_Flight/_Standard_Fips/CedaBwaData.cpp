
// CPP-FILE 

#include <stdafx.h>
#include <afxwin.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <RotationCedaFlightData.h>
#include <ccsddx.h>
#include <CedaBwaData.h>
#include <BasicData.h>
#include <CedaBasicData.h>

CedaBwaData ogBwaData;

static void ProcessBwaCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);


CedaBwaData::CedaBwaData()
{
	// Create an array of CEDARECINFO for BWADATA
	BEGIN_CEDARECINFO(BWADATA,GrmDataRecInfo)
		CCS_FIELD_LONG(Rurn,"RURN", "", 1)
		CCS_FIELD_LONG(Urno,"URNO", "Eindeutige Datensatz-Nr.", 1)
		CCS_FIELD_DATE(Vato,"VATO", "", 1)
		CCS_FIELD_DATE(Vafr,"VAFR", "", 1)
		CCS_FIELD_CHAR_TRIM(Stat,"STAT", "", 1)
		CCS_FIELD_CHAR_TRIM(Dele,"DELE", "", 1)
		CCS_FIELD_CHAR_TRIM(Tabn,"TABN", "", 1)
	END_CEDARECINFO //(BWADATA)

	// Copy the record structure
	for (int i=0; i< sizeof(GrmDataRecInfo)/sizeof(GrmDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&GrmDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"BWA");
	strcpy(pcmFList,"RURN,URNO,VATO,VAFR,STAT,DELE,TABN");
	pcmFieldList = pcmFList;


}; // end Constructor



CedaBwaData::~CedaBwaData()
{
	omRecInfo.DeleteAll();
	omData.DeleteAll();
}


void CedaBwaData::Register()
{
	ogDdx.Register((void *)this, BC_BWA_CHANGE, CString("BWA"), CString("BWA"),ProcessBwaCf);
	ogDdx.Register((void *)this, BC_BWA_DELETE, CString("BWA"), CString("BWA"), ProcessBwaCf);
	ogDdx.Register((void *)this, BC_BWA_NEW, CString("BWA"), CString("BWA"), ProcessBwaCf);
}



void CedaBwaData::UnRegister()
{
	ogDdx.UnRegister(this,NOTUSED);
}




void  ProcessBwaCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	CedaBwaData *polInst = (CedaBwaData*)vpInstance;

	polInst->ProcessBwaBc(ipDDXType,vpDataPointer,ropInstanceName);
}





void CedaBwaData::Clear()
{
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
	UnRegister();
}

void CedaBwaData::ReadAll()
{
	Clear();
	Register();

	if (CedaAction("RT") == false)
	{
		return;
	}

	bool ilRc = true;

    for (int ilLc = 0; ilRc == true; ilLc++)
    {
		BWADATA *prlBwa = new BWADATA;
		if ((ilRc = GetBufferRecord(ilLc,prlBwa)) == true)
		{
			InsertInternal(prlBwa);
		}
		else
		{
			delete prlBwa;
		}
	}

}



bool CedaBwaData::ResolveBwa(BWADATA *prpBwa  )
{
	char buffer[11];

	ltoa(prpBwa->Rurn , buffer, 10);

	CString olTmp1;
	CString olTmp2;
	bool blFound1;
	bool blFound2;

	if( (strcmp(prpBwa->Tabn, "ACT") == 0))
	{
		blFound1 = ogBCD.GetField("ACT", "URNO", CString(buffer), "ACT3", prpBwa->Act3 );
		blFound2 = ogBCD.GetField("ACT", "URNO", CString(buffer), "ACT5", prpBwa->Act5 );

		if(blFound1 && blFound2)
		{
			prpBwa->IsAct = true;
			return true;
		}
		return false;
	}

	if( (strcmp(prpBwa->Tabn, "ALT") == 0))
	{
		blFound1 = ogBCD.GetField("ALT", "URNO", CString(buffer), "ALC2", prpBwa->Alc2 );
		blFound2 = ogBCD.GetField("ALT", "URNO", CString(buffer), "ALC3", prpBwa->Alc3 );

		if(blFound1 && blFound2)
		{
			prpBwa->IsAlt = true;
			return true;
		}
		return false;
	}

	if( (strcmp(prpBwa->Tabn, "ACR") == 0))
	{
		CString olWhere;
		olWhere.Format("WHERE URNO = '%s'", CString(buffer));
		ogBCD.Read( "ACR", olWhere);

		blFound1 = ogBCD.GetField("ACR", "URNO", CString(buffer), "REGN", prpBwa->Regn );

		if(blFound1 )
		{
			prpBwa->IsAcr = true;
			return true;
		}
		return false;
	}

	if( (strcmp(prpBwa->Tabn, "APT") == 0))
	{
		
		blFound1 = ogBCD.GetField("APT", "URNO", CString(buffer), "APC3", prpBwa->Apc3 );
		blFound2 = ogBCD.GetField("APT", "URNO", CString(buffer), "APC4", prpBwa->Apc4 );

		if(blFound1 && blFound2)
		{
			prpBwa->IsApt = true;
			return true;
		}
		return false;
	}




/*
	int ilCount = omData.GetSize();
	BWADATA *prlBwa;	
	for (int i = 0; i < ilCount; i++)
	{
		prlBwa = &omData[i];
		if ( CString(omData[i].Type) == opType)
		{
			return &omData[i];
		}
	}
*/
	return false;
}



char CedaBwaData::CheckRegn(CTime opSto, CString olRegn )
{
	int ilCount = omData.GetSize();
	BWADATA *prlBwa;	
	for (int i = 0; i < ilCount; i++)
	{
		prlBwa = &omData[i];
		if(prlBwa->IsAcr) 
		{
			if(prlBwa->Regn == olRegn) 
			{
				if ( (opSto >= prlBwa->Vafr) &&
					 ((prlBwa->Vato== TIMENULL) ||
					  (opSto <= prlBwa->Vato)
					 )
					)
				{
					return prlBwa->Stat[0];
				}
				/*
				if(opSto <= prlBwa->Vato && opSto >= prlBwa->Vafr)
				{
					return prlBwa->Stat[0];
				}
				*/
			}
		}
	}

	return ' ';
}


char CedaBwaData::CheckAlc(CTime opSto, CString olAlc2, CString olAlc3 )
{
	int ilCount = omData.GetSize();
	BWADATA *prlBwa;	
	for (int i = 0; i < ilCount; i++)
	{
		prlBwa = &omData[i];
		if(prlBwa->IsAlt) 
		{
			if(prlBwa->Alc2 == olAlc2 && prlBwa->Alc3 == olAlc3) 
			{
				if ( (opSto >= prlBwa->Vafr) &&
					 ((prlBwa->Vato== TIMENULL) ||
					  (opSto <= prlBwa->Vato)
					 )
					)
				{
					return prlBwa->Stat[0];
				}
				/*
				if(opSto <= prlBwa->Vato && opSto >= prlBwa->Vafr)
				{
					return prlBwa->Stat[0];
				}
				*/
			}
		}
	}

	return ' ';
}


char CedaBwaData::CheckAct(CTime opSto, CString olAct3, CString olAct5 )
{
	int ilCount = omData.GetSize();
	BWADATA *prlBwa;	
	for (int i = 0; i < ilCount; i++)
	{
		prlBwa = &omData[i];
		if(prlBwa->IsAct) 
		{
			if(prlBwa->Act3 == olAct3 && prlBwa->Act5 == olAct5) 
			{
				if ( (opSto >= prlBwa->Vafr) &&
					 ((prlBwa->Vato== TIMENULL) ||
					  (opSto <= prlBwa->Vato)
					 )
					)
				{
					return prlBwa->Stat[0];
				}
				/*
				if(opSto <= prlBwa->Vato && opSto >= prlBwa->Vafr)
				{
					return prlBwa->Stat[0];
				}
				*/
			}
		}
	}

	return ' ';
}

char CedaBwaData::CheckApt(CTime opSto, CString olApc3, CString olApc4 )
{
	int ilCount = omData.GetSize();
	BWADATA *prlBwa;	
	for (int i = 0; i < ilCount; i++)
	{
		prlBwa = &omData[i];
		if(prlBwa->IsApt) 
		{
			if(prlBwa->Apc3 == olApc3 && prlBwa->Apc4 == olApc4) 
			{
				if ( (opSto >= prlBwa->Vafr) &&
					 ((prlBwa->Vato== TIMENULL) ||
					  (opSto <= prlBwa->Vato)
					 )
					)
				{
					return prlBwa->Stat[0];
				}
				/*
				if(opSto <= prlBwa->Vato && opSto >= prlBwa->Vafr)
				{
					return prlBwa->Stat[0];
				}
				*/
			}
		}
	}

	return ' ';
}













void CedaBwaData::InsertInternal(BWADATA *prpBwa)
{
	if (CString(prpBwa->Dele[0])== CString("X"))
	{
		DeleteInternal(prpBwa);
		return;
	}

	omData.Add(prpBwa);
	omUrnoMap.SetAt((void *)prpBwa->Urno,prpBwa);

	ResolveBwa(prpBwa);

}




///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////


bool CedaBwaData::UpdateInternal(BWADATA *prpBwa)
{	
	if (CString(prpBwa->Dele[0])== CString("X"))
	{
		DeleteInternal(prpBwa);
		return true;
	}

	int ilCount = omData.GetSize();
	for (int i = ilCount-1; i >=0 ; i--)
	{
		if (omData[i].Urno == prpBwa->Urno)
		{
			omData[i] = *prpBwa;
			ResolveBwa(prpBwa);
			return true;
		}
	}
	return false;
}






void CedaBwaData::DeleteInternal(BWADATA *prpBwa)
{
	omUrnoMap.RemoveKey((void *)prpBwa->Urno);
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpBwa->Urno)
		{
			omData.DeleteAt(i);//Update omData
			break;
		}
	}
}


BWADATA *CedaBwaData::GetBwaByUrno(long lpUrno)
{
	BWADATA  *prlBwa;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlBwa) == TRUE)
	{
		return prlBwa;
	}
	return NULL;
}



void  CedaBwaData::ProcessBwaBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlBWADATA;
	long llUrno;
	prlBWADATA = (struct BcStruct *) vpDataPointer;
	CString olSelection = (CString)prlBWADATA->Selection;
	if (olSelection.Find('\'') != -1)
	{
		llUrno = GetUrnoFromSelection(prlBWADATA->Selection);
	}
	else
	{
		int ilFirst = olSelection.Find("=")+2;
		int ilLast  = olSelection.GetLength();
		if (ilLast<=ilFirst) llUrno = 0;
		else llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
	}

	BWADATA *prlBwa;
	if(ipDDXType == BC_BWA_CHANGE || ipDDXType == BC_BWA_NEW)
	{
		if((prlBwa = GetBwaByUrno(llUrno)) != NULL)
		{
			GetRecordFromItemList(prlBwa,prlBWADATA->Fields,prlBWADATA->Data);
			UpdateInternal(prlBwa);
		}
		else
		{
			prlBwa = new BWADATA;
			GetRecordFromItemList(prlBwa,prlBWADATA->Fields,prlBWADATA->Data);
			InsertInternal(prlBwa);
		}
	}
	if(ipDDXType == BC_BWA_DELETE)
	{
		prlBwa = GetBwaByUrno(llUrno);
		if (prlBwa != NULL)
		{
			DeleteInternal(prlBwa);
		}
	}

}


