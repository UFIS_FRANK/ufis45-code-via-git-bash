#ifndef __FlightSearchTableViewer_H__

#include <stdafx.h>
#include <Fpms.h>
#include <CCSGlobl.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <DiaCedaFlightData.h>

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

struct FLIGHTSEARCHTABLE_LINEDATA
{
	long AUrno;
	long DUrno;
	CString		AFtyp;
	CString		AFlno;
	CTime		AStoa;
	CString		AOrg3;
	CString		Regn;
	CString		ACType;

	CString		DFtyp;
	CString		DFlno;
	CTime		DStod;
	CString		DDes3;

  
	FLIGHTSEARCHTABLE_LINEDATA(void)
	{ 
		AUrno = 0;
		DUrno = 0;
		
		AFtyp.Empty();
		AFlno.Empty();
		AStoa = TIMENULL;
		AOrg3.Empty();
		
		Regn.Empty();
		ACType.Empty();

		DFtyp.Empty();
		DFlno.Empty();
		DStod = TIMENULL;
		DDes3.Empty();

	}
};

 

/////////////////////////////////////////////////////////////////////////////
// FlightSearchTableViewer

/////////////////////////////////////////////////////////////////////////////
// Class declaration of FlightSearchTableViewer

 
#define FLIGHTSEARCHTABLE_COLCOUNT 12
 
class FlightSearchTableViewer : public CViewer
{
	DECLARE_DYNAMIC(FlightSearchTableViewer);

public:
    //@ManMemo: Default constructor
    FlightSearchTableViewer();
    //@ManMemo: Default destructor
    ~FlightSearchTableViewer();
	
	void ProcessFlightChange(const DIAFLIGHTDATA &rrpFlight);
	void ProcessFlightDelete(const DIAFLIGHTDATA &rrpFlight);

	int GetFlightLegCount();

	void UnRegister();

  	void Attach(CCSTable *popAttachWnd);
    void ChangeViewTo(DiaCedaFlightData *popFlightData = NULL);
	void SetLocal(bool bpLocal);
	BOOL CheckPostFlight(const long lpBarUrno, CWnd* opCWnd);

// Internal data processing routines
private:
	static int imTableColCharWidths[FLIGHTSEARCHTABLE_COLCOUNT];
	CString omTableHeadlines[FLIGHTSEARCHTABLE_COLCOUNT];
	int imTableColWidths[FLIGHTSEARCHTABLE_COLCOUNT];

	// Table fonts and widths
	CFont &romTableHeaderFont; 
	CFont &romTableLinesFont;
	const float fmTableHeaderFontWidth; 
	const float fmTableLinesFontWidth;

	bool bmLocal;

 	void DeleteAll();
 	void DeleteLine(int ipLineno);
 	bool DeleteFlightLine(long lpUrno, char cpFPart);

 
  	void UpdateDisplay();

	void DrawHeader();

 	int CompareLines(const FLIGHTSEARCHTABLE_LINEDATA &rrpLine1, const FLIGHTSEARCHTABLE_LINEDATA &rrpLine2) const;

    void MakeLines(void);

	void MakeLineFlight(const DIAFLIGHTDATA *prpFlight, int &ripLineNo1, int &ripLineNo2);

	int  MakeLine(const DIAFLIGHTDATA *prpAFlight, const DIAFLIGHTDATA *prpDFlight);
 
	void MakeLineData(const DIAFLIGHTDATA *prpAFlight, const DIAFLIGHTDATA *prpDFlight, FLIGHTSEARCHTABLE_LINEDATA &rrpLine);
 
	void MakeColList(const FLIGHTSEARCHTABLE_LINEDATA &rrpLine, CCSPtrArray<TABLE_COLUMN> &ropColList) const;
	int  CreateLine(const FLIGHTSEARCHTABLE_LINEDATA &rrpLine);

	void InsertFlight(const DIAFLIGHTDATA &rrpFlight);

	bool FindLine(long lpUrno, char cpFPart, int &ripLineno) const;
	
	void InsertDisplayLine(int ipLineNo);

	bool UtcToLocal(FLIGHTSEARCHTABLE_LINEDATA &rrpLine);

    CCSPtrArray<FLIGHTSEARCHTABLE_LINEDATA> omLines;

	CCSTable *pomTable;
	DiaCedaFlightData *pomFlightData;

};

#endif //__FlightSearchTableViewer_H__
