// 

#include <stdafx.h>
#include <ccsglobl.h>
#include <BasicData.h>
#include <ReportActCedaData.h>




////////////////////////////////////////////////////////////////////////////
// Construktor   
//
ReportActCedaData::ReportActCedaData() : CCSCedaData(&ogCommHandler)
{

	BEGIN_CEDARECINFO(REPORTACTDATA,AftDataRecInfo)
	// Referenz AFT.Alc2 <--> ALT.Alc2
	CCS_FIELD_CHAR_TRIM(Act3,"ACT3","3-Letter Code", 1)
	CCS_FIELD_CHAR_TRIM(Acfn,"ACFN","Flugzeug-Bezeichnung", 1)

	END_CEDARECINFO //(AFTDATA)

	// Copy the record structure
	for (int i=0; i< sizeof(AftDataRecInfo)/sizeof(AftDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&AftDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for


	// set Tablename
    strcpy(pcmTableName,"ACT");
	// initialize field names
	strcpy(pcmAftFieldList,"ACT3,ACFN");
	pcmFieldList = pcmAftFieldList;

	int ilrst = sizeof(REPORTACTDATA);

	lmBaseID = IDM_ROTATION;


}




////////////////////////////////////////////////////////////////////////////
// Destruktor   
//
ReportActCedaData::~ReportActCedaData(void)
{
	TRACE("ReportActCedaData::~ReportActCedaData called\n");
	ogDdx.UnRegister(this,NOTUSED);
	omRecInfo.DeleteAll();

}




CString ReportActCedaData::ReadAcfn(CString opTtyp)
{

	CCS_TRY

	//select ttyp,tnam from nattab where ttyp='43';
	CString olSelection("WHERE ACT3 ='");
	
	// Where Klausel vervollstaendigen
	olSelection += opTtyp;
	olSelection += "'";


	                 // ReadTable                                       // Ergebnisbuffer
	bool blRet = CedaAction("RT", pcmTableName, pcmFieldList, olSelection.GetBuffer(0), "", 
		                    pcgDataBuf, "BUF1");
	if (blRet)
	{
		REPORTACTDATA prTtyp;
		if (GetFirstBufferRecord(&prTtyp))
			return prTtyp.Acfn;
	}


	CCS_CATCH_ALL

	return "";

}
