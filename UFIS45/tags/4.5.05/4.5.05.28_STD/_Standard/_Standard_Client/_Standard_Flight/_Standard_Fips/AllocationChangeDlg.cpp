// AllocationChangeDlg.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <resrc1.h>
#include <AllocationChangeDlg.h>
#include <DiaCedaFlightData.h>
#include <CedaBasicData.h>
#include <CCSGlobl.h>
#include <SpotAllocation.h>
#include <SeasonDlgCedaFlightData.h>
#include <CcaCedaFlightData.h>
#include <DataSet.h>
#include <CedaGrmData.h>
#include <PrivList.h>
#include "GridFenster.h"
#include <CedaCraData.h> 

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static TCHAR BASED_CODE szComboStyle[] = _T("Combo Style");
#define IsReallyOverlapped(start1, end1, start2, end2)	((start1) < (end2) && (start2) < (end1))


int AllocationChangeDlg::CompareCcas(const CCADATA **e1, const CCADATA **e2)
{
/*	if (strlen((**e1).Ckic) == 0)
		return 1;
	if (strlen((**e2).Ckic) == 0)
		return -1;
*/
	if (strcmp((**e1).Ckic, (**e2).Ckic) > 0)
		return 1;
	if (strcmp((**e1).Ckic, (**e2).Ckic) < 0)
		return -1;

	return 0;
}

/////////////////////////////////////////////////////////////////////////////
// AllocationChangeDlg dialog


AllocationChangeDlg::AllocationChangeDlg(DIAFLIGHTDATA *popFlight,const CString& ropResType,const CString& ropTarget,CWnd* pParent /*=NULL*/)
	: CDialog(AllocationChangeDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(AllocationChangeDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pomFlight= popFlight;
	omType	 = ropResType;
	omTarget = ropTarget;	
	bmInit = false;

	static bool blFirst = true;
	if (blFirst)
	{
		blFirst = false;
		GXInit();
	}

	pomCheckinCounter = new CGridFenster(this);
}

AllocationChangeDlg::~AllocationChangeDlg()
{
	//YYYY
	omCcaData.omData.DeleteAll();
	omCcaData.ClearAll();
	omDemandCcaData.omData.DeleteAll();
	omDemandCcaData.ClearAll();

	delete pomCheckinCounter;

	CFPMSApp::SetTopmostWnds();
}

void AllocationChangeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(AllocationChangeDlg)
	DDX_Control(pDX, IDC_BLTREASON, m_BltReason);
	DDX_Control(pDX, IDC_CHECK_BAGGAGE_BELT, m_CheckBaggageBelt);
	DDX_Control(pDX, IDC_CHECK_POSITION, m_CheckPosition);
	DDX_Control(pDX, IDC_CHECK_LOUNGE, m_CheckLounge);
	DDX_Control(pDX, IDC_CHECK_GATE, m_CheckGate);
	DDX_Control(pDX, IDC_CHECK_CIC_3, m_CheckCiC_3);
	DDX_Control(pDX, IDC_CHECK_CIC_2, m_CheckCiC_2);
	DDX_Control(pDX, IDC_CHECK_CIC_1, m_CheckCiC_1);
	DDX_Control(pDX, IDC_BITMAP_ARR, m_BitmapArr);
	DDX_Control(pDX, IDC_BITMAP_DEP, m_BitmapDep);
	DDX_Control(pDX, IDC_TO_POSITION, m_ToPosition);
	DDX_Control(pDX, IDC_TO_LOUNGE, m_ToLounge);
	DDX_Control(pDX, IDC_TO_GATE, m_ToGate);
	DDX_Control(pDX, IDC_TO_CIC_3, m_ToCiC_3);
	DDX_Control(pDX, IDC_TO_CIC_2, m_ToCiC_2);
	DDX_Control(pDX, IDC_TO_CIC_1, m_ToCiC_1);
	DDX_Control(pDX, IDC_TO_BAGGAGE_BELT, m_ToBaggageBelt);
	DDX_Control(pDX, IDC_FROM_POSITION, m_FromPosition);
	DDX_Control(pDX, IDC_FROM_LOUNGE, m_FromLounge);
	DDX_Control(pDX, IDC_FROM_GATE, m_FromGate);
	DDX_Control(pDX, IDC_FROM_CIC_3, m_FromCiC_3);
	DDX_Control(pDX, IDC_FROM_CIC_2, m_FromCiC_2);
	DDX_Control(pDX, IDC_FROM_CIC_1, m_FromCiC_1);
	DDX_Control(pDX, IDC_FROM_BAGGAGE_BELT, m_FromBaggageBelt);
	DDX_Control(pDX, IDC_CHECK_EMPTY, m_CheckEmpty);
	DDX_Control(pDX, IDC_CHECK_RECALC, m_Recalc);
	DDX_Control(pDX, IDC_POSREASON, m_PosReason); 
	DDX_Control(pDX, IDC_GATREASON, m_GatReason); 
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(AllocationChangeDlg, CDialog)
	//{{AFX_MSG_MAP(AllocationChangeDlg)
	ON_BN_CLICKED(IDC_CHECK_BAGGAGE_BELT, OnCheckBaggageBelt)
	ON_BN_CLICKED(IDC_CHECK_CIC_1, OnCheckCic1)
	ON_BN_CLICKED(IDC_CHECK_CIC_2, OnCheckCic2)
	ON_BN_CLICKED(IDC_CHECK_CIC_3, OnCheckCic3)
	ON_BN_CLICKED(IDC_CHECK_GATE, OnCheckGate)
	ON_BN_CLICKED(IDC_CHECK_LOUNGE, OnCheckLounge)
	ON_BN_CLICKED(IDC_CHECK_POSITION, OnCheckPosition)
	ON_CBN_SELENDOK(IDC_TO_BAGGAGE_BELT, OnSelendokToBaggageBelt)
	ON_CBN_SELENDOK(IDC_TO_CIC_1, OnSelendokToCic1)
	ON_CBN_SELENDOK(IDC_TO_CIC_2, OnSelendokToCic2)
	ON_CBN_SELENDOK(IDC_TO_CIC_3, OnSelendokToCic3)
	ON_CBN_SELENDOK(IDC_TO_GATE, OnSelendokToGate)
	ON_CBN_SELENDOK(IDC_TO_LOUNGE, OnSelendokToLounge)
	ON_CBN_SELENDOK(IDC_TO_POSITION, OnSelendokToPosition)
	ON_MESSAGE(GRID_MESSAGE_CELLCLICK,OnGridMessageCellClick)
	ON_MESSAGE(GRID_MESSAGE_ENDEDITING,OnGridMessageEndEditing)
	ON_BN_CLICKED(IDC_CHECK_EMPTY, OnCheckEmpty)
	ON_BN_CLICKED(IDC_CHECK_RECALC, OnRecalc)
	ON_BN_CLICKED(IDC_POSREASON, OnPosReason) 
	ON_BN_CLICKED(IDC_GATREASON, OnGatReason) 	
	ON_BN_CLICKED(IDC_BLTREASON, OnBltreason)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// AllocationChangeDlg message handlers

void AllocationChangeDlg::OnCheckBaggageBelt() 
{
	// TODO: Add your control notification handler code here
	UpdateBaggageBeltList();
	
}

void AllocationChangeDlg::OnRecalc() 
{
	if (bgNewRecalculateResChain && !m_Recalc.GetCheck() && !m_bFirstRecalc)
		pomCheckinCounter->SetColWidth(5,5,0);
	else
		pomCheckinCounter->SetColWidth(5,5,14);

	m_bFirstRecalc = true;
	if (m_Recalc.GetCheck())
		UpdateCkeckInCounter();
	// TODO: Add your control notification handler code here
	
}

//PRF 8379 
void AllocationChangeDlg::OnPosReason()
{
	// Get all the reasons related to position //PRF 8379 
	if(bgReasonFlag && ((omReasons.blPstResf == true) || (!omReasons.olPstUrno.IsEmpty())))
	{
		omReasons.olPstUrno = CFPMSApp::GetSelectedReason(this,"POSF",omReasons.olPstUrno);
		if(omReasons.olPstUrno.GetLength()>0)
		{
			omReasons.blPstResf = false;
			m_PosReason.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		}
	}
}

void AllocationChangeDlg::OnGatReason()
{
	// Get all the reasons related to position //PRF 8379 
	if(bgReasonFlag && ((omReasons.blGatResf == true) || (!omReasons.olGatUrno.IsEmpty())))
	{
		omReasons.olGatUrno = CFPMSApp::GetSelectedReason(this,"GATF",omReasons.olGatUrno);
		if(omReasons.olGatUrno.GetLength()>0)
		{
			omReasons.blGatResf = false;
			m_GatReason.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		}
	}
}

//PRF 8379 

void AllocationChangeDlg::OnCheckCic1() 
{
	// TODO: Add your control notification handler code here
	
}

void AllocationChangeDlg::OnCheckCic2() 
{
	// TODO: Add your control notification handler code here
	
}

void AllocationChangeDlg::OnCheckCic3() 
{
	// TODO: Add your control notification handler code here
	
}

void AllocationChangeDlg::OnCheckEmpty() 
{
	// TODO: Add your control notification handler code here
	if (m_CheckEmpty.GetCheck())
	{
		if (m_CheckPosition.IsWindowEnabled())
		{
			if (m_CheckPosition.GetCheck())
			{
				m_CheckPosition.SetCheck(0);
				OnCheckPosition();
			}
		}
		m_ToPosition.EnableWindow(FALSE);
		m_CheckPosition.EnableWindow(FALSE);

		if (m_CheckGate.IsWindowEnabled())
		{
			if (m_CheckGate.GetCheck())
				m_CheckGate.SetCheck(0);
			OnCheckGate();
		}
		m_ToGate.EnableWindow(FALSE);
		m_CheckGate.EnableWindow(FALSE);

		if (m_CheckBaggageBelt.IsWindowEnabled())
		{
			if (m_CheckBaggageBelt.GetCheck())
				m_CheckBaggageBelt.SetCheck(0);

			OnCheckBaggageBelt();
		}
		m_ToBaggageBelt.EnableWindow(FALSE);
		m_CheckBaggageBelt.EnableWindow(FALSE);

		if (m_CheckLounge.IsWindowEnabled())
		{
			if (m_CheckLounge.GetCheck())
				m_CheckLounge.SetCheck(0);
			OnCheckLounge();
		}
		m_ToLounge.EnableWindow(FALSE);
		m_CheckLounge.EnableWindow(FALSE);

	}
	else
	{
		m_ToPosition.EnableWindow(TRUE);
		m_CheckPosition.EnableWindow(TRUE);
		m_ToGate.EnableWindow(TRUE);
		m_CheckGate.EnableWindow(TRUE);
		m_ToBaggageBelt.EnableWindow(TRUE);
		m_CheckBaggageBelt.EnableWindow(TRUE);
		m_ToLounge.EnableWindow(TRUE);
		m_CheckLounge.EnableWindow(TRUE);
//		UpdateCkeckInCounter();
/*		int i = pomCheckinCounter->GetRowCount();
		CString olVal = "0";
		CGXStyle olStyle;
		olStyle.SetEnabled(TRUE);
		olStyle.SetReadOnly(FALSE);
		pomCheckinCounter->SetStyleRange(CGXRange(0,5,i,5),olStyle.SetControl(GX_IDS_CTRL_CHECKBOX3D)
																  .SetChoiceList("1\n0\n")
																  .SetValue(olVal)
																  );
		pomCheckinCounter->SetStyleRange(CGXRange(0,4,i,4),olStyle.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
																  .SetUserAttribute(GX_IDS_UA_TABLIST_KEYCOL, _T("0"))
																  .SetUserAttribute(GX_IDS_UA_TABLIST_TEXTCOL, _T("2"))
																  .SetUserAttribute(GX_IDS_UA_TABLIST_COLWIDTHS, _T("0,16,50"))
																  .SetUserAttribute(GX_IDS_UA_TABLIST_SHOWALLCOLS, _T("1"))
																  .SetUserAttribute(GX_IDS_UA_TABLIST_SORTCOL, _T("0"))
//																  .SetValue(""));
																  );


		// redraw whole grid
		pomCheckinCounter->Redraw();
*/
	}

	if (pomFlight->Adid[0] == 'A')	// arrival
	{
		m_ToLounge.EnableWindow(FALSE);
		m_CheckLounge.EnableWindow(FALSE);
	}
	else if (pomFlight->Adid[0] == 'D')
	{
		m_ToBaggageBelt.EnableWindow(FALSE);
		m_CheckBaggageBelt.EnableWindow(FALSE);
	}

	if (omType == "PST")
	{
		m_ToPosition.EnableWindow(FALSE);
		m_CheckPosition.EnableWindow(FALSE);
	}
	else if (omType == "GAT")
	{
		m_ToGate.EnableWindow(FALSE);
		m_CheckGate.EnableWindow(FALSE);
	}
	else if (omType == "BLT")
	{
		m_ToBaggageBelt.EnableWindow(FALSE);
		m_CheckBaggageBelt.EnableWindow(FALSE);
	}
	else if (omType == "WRO")
	{
		m_ToLounge.EnableWindow(FALSE);
		m_CheckLounge.EnableWindow(FALSE);
	}
/*
	if (!m_CheckEmpty.GetCheck())
	{
		if (m_CheckPosition.IsWindowEnabled())
		{
			if (!m_CheckPosition.GetCheck())
			{
				m_CheckPosition.SetCheck(1);
				OnCheckPosition();
			}
		}
		if (m_CheckGate.IsWindowEnabled())
		{
			if (m_CheckGate.GetCheck())
			{
				m_CheckGate.SetCheck(0);
				OnCheckGate();
			}
		}
		if (m_CheckBaggageBelt.IsWindowEnabled())
		{
			if (m_CheckBaggageBelt.GetCheck())
			{
				m_CheckBaggageBelt.SetCheck(0);
				OnCheckBaggageBelt();
			}
		}
		if (m_CheckLounge.IsWindowEnabled())
		{
			if (m_CheckLounge.GetCheck())
			{
				m_CheckLounge.SetCheck(0);
				OnCheckLounge();
			}
		}
	}
*/
	SetSecSate();

}

void AllocationChangeDlg::OnCheckGate() 
{
	// TODO: Add your control notification handler code here
	if (omType == "WRO")
	{
		UpdateGateListFromLounge();
		UpdatePositionListFromGate();
		UpdateCkeckInCounter();
	}
	else if (omType == "BLT")
	{
		UpdateGateListFromBelt();
		UpdatePositionListFromGate();
	}
	else if (omType == "PST")
	{
		UpdateGateList();
		UpdateLoungeList();
		UpdateBaggageBeltList();
		UpdateCkeckInCounter();
	}

//	UpdateGateList();
//	UpdateCkeckInCounter();
}

void AllocationChangeDlg::OnCheckLounge() 
{
	// TODO: Add your control notification handler code here
	UpdateLoungeList();
	UpdateCkeckInCounter();
}

void AllocationChangeDlg::OnCheckPosition() 
{
	// TODO: Add your control notification handler code here
	UpdatePositionListFromGate();
	UpdateCkeckInCounter();
}

void AllocationChangeDlg::OnSelendokToBaggageBelt() 
{
	// TODO: Add your control notification handler code here
	
}

void AllocationChangeDlg::OnSelendokToCic1() 
{
	// TODO: Add your control notification handler code here
	
}

void AllocationChangeDlg::OnSelendokToCic2() 
{
	// TODO: Add your control notification handler code here
	
}

void AllocationChangeDlg::OnSelendokToCic3() 
{
	// TODO: Add your control notification handler code here
	
}

void AllocationChangeDlg::OnSelendokToGate() 
{
	// TODO: Add your control notification handler code here
	if (omType == "PST")
	{
		if (pomFlight->Adid[0] == 'D')
		{
			if (this->omType != "WRO")
				UpdateLoungeList();
		}
		else
		{
			if (this->omType != "BLT")
				UpdateBaggageBeltList();
		}
	}
	if (omType == "WRO" || omType == "BLT")
	{
		UpdatePositionListFromGate();
	}

	// For Getting the Reason for change flag	//PRF 8379 
	if(bgReasonFlag)
	{
		CString olGate;
		this->m_ToGate.GetWindowText(olGate);

		if(this->pomFlight->Adid[0] == 'A')
		{
			if((olGate.Compare(this->pomFlight->Gta1) != 0) && (strlen(pomFlight->Gta1)>0))
			{
				CheckReasonForChange("GAT");
			}
			else if((olGate.Compare(this->pomFlight->Gta1) == 0) && (omReasons.blGatResf == true))
			{
				omReasons.blGatResf = false;
				m_GatReason.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			}
		}
		else if(this->pomFlight->Adid[0] == 'D')
		{
			if((olGate.Compare(this->pomFlight->Gtd1) != 0) && (strlen(pomFlight->Gtd1)>0))
			{
				CheckReasonForChange("GAT");
			}
			else if((olGate.Compare(this->pomFlight->Gta1) == 0)  && (omReasons.blGatResf == true))
			{
				omReasons.blGatResf = false;
				m_GatReason.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			}
		}
	}

	if(this->pomFlight->Adid[0] == 'A')
	{
		CString olGate;
		this->m_ToGate.GetWindowText(olGate);
	}
	

	UpdateCkeckInCounter();
}

void AllocationChangeDlg::UpdateCkeckInCounter() 
{
	if (!m_Recalc.GetCheck() && !bmInit)
		return;

	DIAFLIGHTDATA prlFlightTmp = *this->pomFlight;

	CString olCurPos;
	this->m_ToPosition.GetWindowText(olCurPos);
	strcpy(prlFlightTmp.Pstd, olCurPos);

	CString olCurGate;
	this->m_ToGate.GetWindowText(olCurGate);
	strcpy(prlFlightTmp.Gtd1, olCurGate);

	CString olCurWro;
	this->m_ToLounge.GetWindowText(olCurWro);
	strcpy(prlFlightTmp.Wro1, olCurWro);

	CString olCurBlt;
	this->m_ToBaggageBelt.GetWindowText(olCurBlt);
	strcpy(prlFlightTmp.Blt1, olCurBlt);

	ReadCcaData(&prlFlightTmp);
	UpdateCheckinCounterGrid();
}

void AllocationChangeDlg::OnSelendokToLounge() 
{
	// TODO: Add your control notification handler code here
	UpdateCkeckInCounter();
	
}

void AllocationChangeDlg::OnSelendokToPosition() 
{
	// For Getting the Reason for change flag	//PRF 8379 
	if(bgReasonFlag)
	{
		CString olPosition;
		this->m_ToPosition.GetWindowText(olPosition);

		if(this->pomFlight->Adid[0] == 'A')
		{
			 if((olPosition.Compare(this->pomFlight->Psta) != 0)  && (strlen(pomFlight->Psta)>0))
			 {
				 CheckReasonForChange("PST");
			 }
			 else if((olPosition.Compare(this->pomFlight->Psta) == 0) && (omReasons.blPstResf == true))
			 {
				 omReasons.blPstResf = false;
			     m_PosReason.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			 }
		}
		else if(this->pomFlight->Adid[0] == 'D')
		{
			if((olPosition.Compare(this->pomFlight->Pstd) != 0)  && (strlen(pomFlight->Pstd)>0))
			{
				CheckReasonForChange("PST");
			}
			else if((olPosition.Compare(this->pomFlight->Pstd) == 0) && omReasons.blGatResf == true) 
			{	
				omReasons.blGatResf = false;
				m_GatReason.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			}
		}
	}

	UpdateCkeckInCounter();
	
}

void AllocationChangeDlg::OnOK() 
{
	// TODO: Add extra validation here
	CString olCurPos;
	CString olCurGat;
	CString olCurBlt;
	CString olCurWro;

	this->m_ToPosition.GetWindowText(olCurPos);
	this->m_ToGate.GetWindowText(olCurGat);
	this->m_ToBaggageBelt.GetWindowText(olCurBlt);
	this->m_ToLounge.GetWindowText(olCurWro);

//	ogPosDiaFlightData.DeleteAllCcaResourses(*pomFlight);
	DIAFLIGHTDATA rlFlightSave;
	DIAFLIGHTDATA rlFlight;
	rlFlightSave = *pomFlight;
	rlFlight = *pomFlight;

	bool blUpdate = false;

	//Inserting the reason records into the CRATAB  //PRF8379  
	if(bgReasonFlag)
	{
		if (this->pomFlight->Adid[0] == 'A')
		{

			if(ogPosDiaFlightData.bmOffLine) 				
			{

				if(strcmp(pomFlight->Psta,olCurPos) != 0)
				{
					ogPosDiaFlightData.bmPosReason = true;
					this->pomFlight->PstaReason = true;
					if(ogPosDiaFlightData.lmPosReasonUrno == 0)
					{
							ogPosDiaFlightData.lmPosReasonUrno = atof(omReasons.olPstUrno);
					}
				
				}

				if(strcmp(pomFlight->Gta1,olCurGat) != 0)
				{

					ogPosDiaFlightData.bmGatReason = true;
					this->pomFlight->Gta1Reason = true;
					if(ogPosDiaFlightData.lmBltReasonUrno == 0)
					{
							ogPosDiaFlightData.lmBltReasonUrno = atof(omReasons.olBltUrno);
					}
				}

				if(strcmp(pomFlight->Blt1,olCurBlt) != 0)
				{

					ogPosDiaFlightData.bmBltReason = true;
					this->pomFlight->Blt1Reason = true;
					if(ogPosDiaFlightData.lmBltReasonUrno == 0)
					{
							ogPosDiaFlightData.lmBltReasonUrno = atof(omReasons.olBltUrno);
					}
				}
			
			}
			else
			{

				if(omReasons.blPstResf || omReasons.blGatResf || omReasons.blBltResf)
				{
					CString olReasonSelect ="";

					//(strcmp(prmAFlight->Fbl1, "X") == 0) 
				
					if(omReasons.blPstResf)

						olReasonSelect = GetString(IDS_STRING877) +", ";
					
					if(omReasons.blGatResf)
						olReasonSelect = GetString(IDS_STRING878) +", ";
					if(omReasons.blBltResf)
						olReasonSelect = GetString(IDS_STRING1278) +", ";
					

					olReasonSelect.TrimRight(", ");
					olReasonSelect += " Reasons not selected";
					MessageBox(olReasonSelect, GetString(IDS_WARNING), MB_ICONWARNING|MB_OK);
					return;
				}

				if((omReasons.olPstUrno.GetLength() > 0 ) || (omReasons.olGatUrno.GetLength() > 0 ) 
					|| (strcmp(pomFlight->Psta,olCurPos) != 0) || (strcmp(pomFlight->Gta1,olCurGat) != 0)
					|| (strcmp(pomFlight->Blt1,olCurBlt) != 0))
				{
					CRADATA *prlCRADATA = new CRADATA();

					if(bgGatPosLocal)			
						prlCRADATA->Cdat = CTime::GetCurrentTime();
					else
						GetCurrentUtcTime(prlCRADATA->Cdat);

					prlCRADATA->Furn = this->pomFlight->Urno;
					strcpy(prlCRADATA->Hopo, pcgHome);
					strcpy(prlCRADATA->Usec, ogBasicData.omUserID);
					prlCRADATA->IsChanged = true;

					if((omReasons.olPstUrno.GetLength() > 0 ) && (strcmp(pomFlight->Psta,olCurPos) != 0))
					{
						prlCRADATA->Urno = ogBasicData.GetNextUrno();
						prlCRADATA->Curn = atof(omReasons.olPstUrno);
						strcpy(prlCRADATA->Oval,this->pomFlight->Psta);
						strcpy(prlCRADATA->Nval,olCurPos);
						strcpy(prlCRADATA->Aloc,"PSTA");
						ogCraData.InsertDB(prlCRADATA);
					}
					if((omReasons.olGatUrno.GetLength() > 0 ) && (strcmp(pomFlight->Gta1,olCurGat) != 0))
					{
						prlCRADATA->Urno = ogBasicData.GetNextUrno();
						prlCRADATA->Curn = atof(omReasons.olGatUrno);
						strcpy(prlCRADATA->Oval,this->pomFlight->Gta1);
						strcpy(prlCRADATA->Nval,olCurGat);
						strcpy(prlCRADATA->Aloc,"GTA1");
						ogCraData.InsertDB(prlCRADATA);
					}

					if((omReasons.olBltUrno.GetLength() > 0 ) && (strcmp(pomFlight->Blt1,olCurBlt) != 0))
					{
						prlCRADATA->Urno = ogBasicData.GetNextUrno();
						prlCRADATA->Curn = atof(omReasons.olBltUrno);
						strcpy(prlCRADATA->Oval,this->pomFlight->Blt1);
						strcpy(prlCRADATA->Nval,olCurBlt);
						strcpy(prlCRADATA->Aloc,"BLT1");
						ogCraData.InsertDB(prlCRADATA);
					}
				}
			}
		}
		else if (this->pomFlight->Adid[0] == 'D')
		{
			if(ogPosDiaFlightData.bmOffLine) 				
			{

				if(strcmp(pomFlight->Pstd,olCurPos) != 0)
				{
					ogPosDiaFlightData.bmPosReason = true;
					this->pomFlight->PstdReason = true;
					if(ogPosDiaFlightData.lmPosReasonUrno == 0)
					{
							ogPosDiaFlightData.lmPosReasonUrno = atof(omReasons.olPstUrno);
					}
				
				}

				if(strcmp(pomFlight->Gtd1,olCurGat) != 0)
				{

					ogPosDiaFlightData.bmGatReason = true;
					this->pomFlight->Gtd1Reason = true;
					if(ogPosDiaFlightData.lmGatReasonUrno == 0)
					{
							ogPosDiaFlightData.lmGatReasonUrno = atof(omReasons.olGatUrno);
					}
				}
				if(strcmp(pomFlight->Blt1,olCurBlt) != 0)
				{

					ogPosDiaFlightData.bmBltReason = true;
					this->pomFlight->Blt1Reason = true;
					if(ogPosDiaFlightData.lmBltReasonUrno == 0)
					{
							ogPosDiaFlightData.lmBltReasonUrno = atof(omReasons.olBltUrno);
					}
				}
			
			}
			else
			{
				if(omReasons.blPstResf || omReasons.blGatResf)
				{
					CString olReasonSelect ="";
					if(omReasons.blPstResf)
						olReasonSelect = GetString(IDS_STRING891) +", ";
					if(omReasons.blGatResf)
						olReasonSelect = GetString(IDS_STRING892) +", ";

					olReasonSelect.TrimRight(", ");
					olReasonSelect += " Reasons not selected";
					MessageBox(olReasonSelect, GetString(IDS_WARNING), MB_ICONWARNING|MB_OK);
					return;
				}

				if((omReasons.olPstUrno.GetLength() > 0 ) || (omReasons.olGatUrno.GetLength() > 0 )
					|| (strcmp(pomFlight->Pstd,olCurPos) != 0) || (strcmp(pomFlight->Gtd1,olCurGat) != 0))
				{
					CRADATA *prlCRADATA = new CRADATA();

					if(bgGatPosLocal)			
						prlCRADATA->Cdat = CTime::GetCurrentTime();
					else
						GetCurrentUtcTime(prlCRADATA->Cdat);
					
					prlCRADATA->Furn = this->pomFlight->Urno;
					strcpy(prlCRADATA->Hopo, pcgHome);
					strcpy(prlCRADATA->Usec, ogBasicData.omUserID);
					prlCRADATA->IsChanged = true;

					if((omReasons.olPstUrno.GetLength() > 0 )  && (strcmp(pomFlight->Pstd,olCurPos) != 0))
					{
						prlCRADATA->Urno = ogBasicData.GetNextUrno();
						prlCRADATA->Curn = atof(omReasons.olPstUrno);
						strcpy(prlCRADATA->Oval,this->pomFlight->Pstd);
						strcpy(prlCRADATA->Nval,olCurPos);
						strcpy(prlCRADATA->Aloc,"PSTD");
						ogCraData.InsertDB(prlCRADATA);
					}
					if((omReasons.olGatUrno.GetLength() > 0 )  && (strcmp(pomFlight->Gtd1,olCurGat) != 0))
					{
						prlCRADATA->Urno = ogBasicData.GetNextUrno();
						prlCRADATA->Curn = atof(omReasons.olGatUrno);
						strcpy(prlCRADATA->Oval,this->pomFlight->Gtd1);
						strcpy(prlCRADATA->Nval,olCurGat);
						strcpy(prlCRADATA->Aloc,"GTD1");
						ogCraData.InsertDB(prlCRADATA);
					}
					if((omReasons.olBltUrno.GetLength() > 0 ) && (strcmp(pomFlight->Blt1,olCurBlt) != 0))
					{
						prlCRADATA->Urno = ogBasicData.GetNextUrno();
						prlCRADATA->Curn = atof(omReasons.olBltUrno);
						strcpy(prlCRADATA->Oval,this->pomFlight->Blt1);
						strcpy(prlCRADATA->Nval,olCurBlt);
						strcpy(prlCRADATA->Aloc,"BLT1");
						ogCraData.InsertDB(prlCRADATA);
					}
				}
			}
		}
	}




	if(ogPrivList.GetStat("RESCHAIN_CB_Pos") != '-' )
	{
		ogPosDiaFlightData.PreChangeFlightPos(rlFlight, pomFlight->Adid[0], olCurPos);
		blUpdate = true;
	}

	if(ogPrivList.GetStat("RESCHAIN_CB_Gat") != '-' )
	{
		ogPosDiaFlightData.PreChangeFlightGat(rlFlight, pomFlight->Adid[0], olCurGat, 1);
		blUpdate = true;
	}

	if (pomFlight->Adid[0] == 'A' && 	ogPrivList.GetStat("RESCHAIN_CB_Blt") != '-' )
	{
		{
			ogPosDiaFlightData.PreChangeFlightBlt(rlFlight, olCurBlt, "", 1, true);
			blUpdate = true;
		}
	}

	if (pomFlight->Adid[0] == 'D'	&& ogPrivList.GetStat("RESCHAIN_CB_Wro") != '-' )
	{
		{
			ogPosDiaFlightData.PreChangeFlightWro(rlFlight, olCurWro, 1);
			blUpdate = true;
		}
	}



	if (this->pomFlight->Adid[0] == 'D' && 	ogPrivList.GetStat("RESCHAIN_CB_Cic") != '-' )
	{
		CMapStringToString ropMapBLUE;
		CMapStringToString ropMapRED;
		if (omDemandCcaData.omData.GetSize() > 0)
		{
			CCADATA* prlDemCca = &omDemandCcaData.omData[0];
			CStringArray olBLUEArray;
			ogDataSet.GetCcaRes(prlDemCca, ropMapBLUE, ropMapRED, olBLUEArray);
		}


	    CCSPtrArray<CCADATA> opNewData;
	    CCSPtrArray<CCADATA> opOldData;
		for(int i=0; i<omCcaData.omData.GetSize(); i++)
		{
			if (i+1 <= pomCheckinCounter->GetRowCount())
			{
				CString olCnam = pomCheckinCounter->GetValueRowCol(i+1,4);
				CString olCnamFrom = pomCheckinCounter->GetValueRowCol(i+1,1);

				CString olTmp;
				bool blResChain = false;
				if (ropMapBLUE.Lookup(olCnam, olTmp))
					blResChain = true;


				if (/*!olCnam.IsEmpty() && */olCnam != "Demand")
				{
					CCADATA* prlCcaTmp = new CCADATA;
					opOldData.Add(prlCcaTmp);

					//opNewData.New(omCcaData.omData[i]);
					//CCADATA* prlCca = &opNewData[opNewData.GetSize()-1];
					CCADATA* prlCca = NULL;

					if (i<omDemandCcaData.omData.GetSize()) 
					{
						opNewData.New(omDemandCcaData.omData[i]);
						prlCca = &opNewData[opNewData.GetSize()-1];
						CCADATA* prlCcaDem = &omDemandCcaData.omData[i];
//						strcpy(prlCca->Stat, "0000000000");
//						strcpy(prlCca->Ckic, olCnam); 
					}
					else
					{
						opNewData.New(omCcaData.omData[i]);
						prlCca = &opNewData[opNewData.GetSize()-1];
						prlCca->Ghpu = 0;
						prlCca->Gpmu = 0;
						prlCca->Ghsu = 0;
						strcpy(prlCca->Cgru, ""); 
					}

					if (prlCca)
					{
						strcpy(prlCca->Ckic, olCnam); 
						if (olCnam.IsEmpty())
						{
							prlCca->Ckbs = TIMENULL;
							prlCca->Ckes = TIMENULL;
							prlCca->Flnu = 0;
							prlCca->Ghpu = 0;
							prlCca->Gpmu = 0;
							prlCca->Ghsu = 0;
							strcpy(prlCca->Cgru, olCnam); 
						}
						else
						{
							CString olTerm;
							if(ogBCD.GetField("CIC", "CNAM", olCnam, "TERM", olTerm))
								strcpy(prlCca->Ckit, olTerm); 
						}
//						strcpy(prlCca->Stat, "0000000000");
					}
				}
			}
		}


		if (opOldData.GetSize()>0 && opNewData.GetSize()>0 && opOldData.GetSize()== opNewData.GetSize())
			omCcaData.UpdateSpecial(opNewData, opOldData, pomFlight->Urno);

		//YYYY
		opNewData.DeleteAll();
		opOldData.DeleteAll();
	}

	if (blUpdate)
		ogPosDiaFlightData.UpdateFlight(&rlFlight, &rlFlightSave);


	CDialog::OnOK();
}

BOOL AllocationChangeDlg::OnInitDialog() 
{
	if (!pomFlight)
		return FALSE;

	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	pomCheckinCounter->SubclassDlgItem(IDC_CHECKIN_COUNTER, this);

	// initialize selection grid
	pomCheckinCounter->Initialize();
	pomCheckinCounter->SetAllowMultiSelect(false);
	pomCheckinCounter->SetSortingEnabled(false);

/***
	if (pomCheckinCounter->GetParam()->GetStylesMap() == NULL)
	{
		// create a stylesmap and connect it with the parameter-object
		CGXStylesMap* pStyMap;

		pomCheckinCounter->GetParam()->SetStylesMap(pStyMap = new CGXStylesMap);

		// create standard styles
		pStyMap->CreateStandardStyles();

		// Add some base styles

		// "Combo Style" - A style with a combo box and choice list
		pStyMap->RegisterStyle(szComboStyle,
			CGXStyle()
				.SetControl(GX_IDS_CTRL_CBS_DROPDOWNLIST)
				.SetChoiceList(_T("one\r\ntwo\r\nthree\r\nfour\r\nfive\r\nsix\r\n")),
			TRUE    // system-style (non removable)
		);
	}	
**/

	// disable immediate update
	pomCheckinCounter->LockUpdate(TRUE);
	pomCheckinCounter->GetParam()->EnableUndo(FALSE);
	// column width is static
	pomCheckinCounter->GetParam()->EnableTrackColWidth(FALSE);
	// row height is static
	pomCheckinCounter->GetParam()->EnableTrackRowHeight(FALSE);
	// column headers aren't numbered
	pomCheckinCounter->GetParam()->SetNumberedColHeaders(FALSE);

	pomCheckinCounter->SetColCount(6);

	// set the size of the row number
	pomCheckinCounter->SetColWidth(0,0,20);

	// set the size of the "from" CIC
	pomCheckinCounter->SetColWidth(1,1,60);

	// set the size of the "->" text
	pomCheckinCounter->SetColWidth(2,2,35);

	// set the size of the "to" CIC bitmap
	pomCheckinCounter->SetColWidth(3,3,14);

	// set the size of the "to" CIC
	pomCheckinCounter->SetColWidth(4,4,65);

	// set the size of the "valid only" flag
	if (bgNewRecalculateResChain && !m_Recalc.GetCheck())
		pomCheckinCounter->SetColWidth(5,5,0);
	else
		pomCheckinCounter->SetColWidth(5,5,14);

	// set the size of the "ptr" (hidden) column
	pomCheckinCounter->SetColWidth(6,6,0);

	pomCheckinCounter->HideCols(6,6);

	pomCheckinCounter->SetRowHeight(0, 0, 14);

	pomCheckinCounter->SetScrollBarMode(SB_BOTH,gxnAutomatic|gxnEnhanced,TRUE);

	// enable immediate update
	pomCheckinCounter->LockUpdate(FALSE);

	if (bgNewRecalculateResChain)
	{
		if (bgKeepExisting)
			m_CheckEmpty.SetCheck(1);
		else
			m_CheckEmpty.SetCheck(0);

		if (bgKeepExistingCki)
			m_Recalc.SetCheck(1);
		else
			m_Recalc.SetCheck(0);
	}
	else
	{
		if (bgKeepExisting)
		{
			m_CheckEmpty.SetCheck(1);
			m_Recalc.SetCheck(0);
		}
		else
		{
			m_CheckEmpty.SetCheck(0);
			m_Recalc.SetCheck(1);
		}
	}

	m_bFirstRecalc = false;

	m_CheckPosition.SetCheck(1);
	m_CheckGate.SetCheck(1);
	m_CheckBaggageBelt.SetCheck(1);
	m_CheckLounge.SetCheck(1);
	m_CheckCiC_1.SetCheck(1);
	m_CheckCiC_2.SetCheck(1);
	m_CheckCiC_3.SetCheck(1);
	
	m_FromPosition.SetTypeToString("X(6)",6,1);
	m_FromGate.SetTypeToString("X(6)",6,1);
	m_FromBaggageBelt.SetTypeToString("X(6)",6,1);
	m_FromLounge.SetTypeToString("X(6)",6,1);
	m_FromCiC_1.SetTypeToString("X(6)",6,1);
	m_FromCiC_2.SetTypeToString("X(6)",6,1);
	m_FromCiC_3.SetTypeToString("X(6)",6,1);

	if (pomFlight->Adid[0] == 'A')	// arrival
	{
		m_FromPosition.SetInitText(pomFlight->Psta);
		m_FromGate.SetInitText(pomFlight->Gta1);
		m_FromBaggageBelt.SetInitText(pomFlight->Blt1);
		m_FromLounge.SetInitText("");
		m_ToLounge.EnableWindow(FALSE);
		m_CheckLounge.EnableWindow(FALSE);
	}
	else if (pomFlight->Adid[0] == 'D')
	{
		m_FromPosition.SetInitText(pomFlight->Pstd);
		m_FromGate.SetInitText(pomFlight->Gtd1);
		m_FromBaggageBelt.SetInitText("");
		m_ToBaggageBelt.EnableWindow(FALSE);
		m_CheckBaggageBelt.EnableWindow(FALSE);
		m_FromLounge.SetInitText(pomFlight->Wro1);
	}
	else
		return FALSE;

	bmInit = true;
	CString olCaption;
	if (omType == "PST")
	{
		SetPosition();
		m_ToPosition.EnableWindow(FALSE); 
		m_CheckPosition.EnableWindow(FALSE);
		if (this->pomFlight->Adid[0] == 'A')
		{
			olCaption.Format("Allocation change - %s - %s %s -> %s %s",pomFlight->Flno,"Position",pomFlight->Psta,"Position",this->omTarget);
			UpdateGateList();
			UpdateBaggageBeltList();
		}
		else
		{
			olCaption.Format("Allocation change - %s - %s %s -> %s %s",pomFlight->Flno,"Position",pomFlight->Pstd,"Position",this->omTarget);
			UpdateGateList();
			UpdateLoungeList();
			UpdateCkeckInCounter();
		}

		//Check for position change - PRF 8379 
		if(bgReasonFlag)
		{
			CheckReasonForChange(omType);
		}
	}
	else if (omType == "GAT")
	{
		SetGate();
		m_ToGate.EnableWindow(FALSE);
		m_CheckGate.EnableWindow(FALSE);
		if (this->pomFlight->Adid[0] == 'A')
		{
			olCaption.Format("Allocation change - %s - %s %s -> %s %s",pomFlight->Flno,"Gate",pomFlight->Gta1,"Gate",this->omTarget);
			UpdatePositionListFromGate();
			UpdateBaggageBeltList();
		}
		else
		{
			olCaption.Format("Allocation change - %s - %s %s -> %s %s",pomFlight->Flno,"Gate",pomFlight->Gtd1,"Gate",this->omTarget);
			UpdatePositionListFromGate();
			UpdateLoungeList();
			UpdateCkeckInCounter();
		}
		
		//Check for the position change - PRF 8379 
		if(bgReasonFlag)
		{
			CheckReasonForChange(omType);
		}
	}
	else if (omType == "BLT")
	{
		SetBaggageBelt();
		m_ToBaggageBelt.EnableWindow(FALSE);
		m_CheckBaggageBelt.EnableWindow(FALSE);
		olCaption.Format("Allocation change - %s - %s %s -> %s %s",pomFlight->Flno,"Baggage belt",pomFlight->Blt1,"Baggage belt",this->omTarget);
		UpdateGateListFromBelt();
		UpdatePositionListFromGate();

		if(bgReasonFlag)
		{
			CheckReasonForChange(omType);
		}

	}
	else if (omType == "WRO")
	{
		SetLounge();
		m_ToLounge.EnableWindow(FALSE);
		m_CheckLounge.EnableWindow(FALSE);
		olCaption.Format("Allocation change - %s - %s %s -> %s %s",pomFlight->Flno,"Lounge",pomFlight->Wro1,"Lounge",this->omTarget);
		UpdateGateListFromLounge();
		UpdatePositionListFromGate();
		UpdateCkeckInCounter();
	}
	else if (omType == "CIC")
	{
		m_ToCiC_1.EnableWindow(FALSE);
		m_ToCiC_2.EnableWindow(FALSE);
		m_ToCiC_3.EnableWindow(FALSE);
	}
	
	this->SetWindowText(olCaption);

	if (this->pomFlight->Adid[0] == 'A')
	{
		m_BitmapArr.ShowWindow(SW_SHOW);
		m_BitmapDep.ShowWindow(SW_HIDE);
	}
	else if (this->pomFlight->Adid[0] == 'D')
	{
		m_BitmapArr.ShowWindow(SW_HIDE);
		m_BitmapDep.ShowWindow(SW_SHOW);
	}
	else
	{
		m_BitmapArr.ShowWindow(SW_HIDE);
		m_BitmapDep.ShowWindow(SW_HIDE);
	}

	bmInit = false;

	SetOpenAllocationState();
	OnCheckEmpty();

	CFPMSApp::UnsetTopmostWnds();


	//security
	if(bgNewRecalculateResChain)
	{
	}

	if(!bgReasonFlag)
	{
			m_PosReason.ShowWindow(SW_HIDE);
			m_GatReason.ShowWindow(SW_HIDE);
			m_BltReason.ShowWindow(SW_HIDE);
	}


	SetSecSate();



	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void AllocationChangeDlg::SetOpenAllocationState()
{
	if (this->pomFlight->Adid[0] == 'A')
	{
		if (strlen(this->pomFlight->Psta) != 0 && this->pomFlight->Paba != TIMENULL)
		{
			m_ToPosition.EnableWindow(FALSE);
			m_CheckPosition.EnableWindow(FALSE);
			CWnd* wnd = GetDlgItem(IDC_OPEN_PST);
			if (wnd)
				wnd->ShowWindow(SW_SHOW);

		}
		if (strlen(this->pomFlight->Gta1) != 0 && this->pomFlight->Ga1x != TIMENULL)
		{
			m_ToGate.EnableWindow(FALSE);
			m_CheckGate.EnableWindow(FALSE);
			CWnd* wnd = GetDlgItem(IDC_OPEN_GAT);
			if (wnd)
				wnd->ShowWindow(SW_SHOW);
		}
		if (strlen(this->pomFlight->Blt1) != 0 && this->pomFlight->B1ba != TIMENULL)
		{
			m_ToBaggageBelt.EnableWindow(FALSE);
			m_CheckBaggageBelt.EnableWindow(FALSE);
			CWnd* wnd = GetDlgItem(IDC_OPEN_BLT);
			if (wnd)
				wnd->ShowWindow(SW_SHOW);
		}
	}
	else if (this->pomFlight->Adid[0] == 'D')
	{
		if (strlen(this->pomFlight->Pstd) != 0 && this->pomFlight->Pdba != TIMENULL)
		{
			m_ToPosition.EnableWindow(FALSE);
			m_CheckPosition.EnableWindow(FALSE);
			CWnd* wnd = GetDlgItem(IDC_OPEN_PST);
			if (wnd)
				wnd->ShowWindow(SW_SHOW);
		}
		if (strlen(this->pomFlight->Gtd1) != 0 && this->pomFlight->Gd1x != TIMENULL)
		{
			m_ToGate.EnableWindow(FALSE);
			m_CheckGate.EnableWindow(FALSE);
			CWnd* wnd = GetDlgItem(IDC_OPEN_GAT);
			if (wnd)
				wnd->ShowWindow(SW_SHOW);
		}
		if (strlen(this->pomFlight->Wro1) != 0 && this->pomFlight->W1ba != TIMENULL)
		{
			m_ToLounge.EnableWindow(FALSE);
			m_CheckLounge.EnableWindow(FALSE);
			CWnd* wnd = GetDlgItem(IDC_OPEN_WRO);
			if (wnd)
				wnd->ShowWindow(SW_SHOW);
		}
	}
	SetSecSate();


}

void AllocationChangeDlg::SetPosition()
{
	InitTarget();
	SetPositionSelection();
}

void AllocationChangeDlg::SetBaggageBelt()
{
	InitTarget();
	SetBaggageBeltSelection();
}

void AllocationChangeDlg::SetGate()
{
	InitTarget();
	SetGateSelection();
}

void AllocationChangeDlg::SetLounge()
{
	InitTarget();
	SetLoungeSelection();
}

void AllocationChangeDlg::InitTarget()
{
	if (!pomFlight)
		return;

	bool blIsAvailable = ogSpotAllocation.ResIsAvailable(pomFlight, omType, omTarget, false, true);

	CBitmap olBitmap;
	if (blIsAvailable)
		olBitmap.LoadBitmap(IDB_LIGHTBLUE);
	else
		olBitmap.LoadBitmap(IDB_LIGHTBLUEBLOCKED);

	if (omType == "PST")
		int ilIndex = m_ToPosition.AddBitmap(olBitmap,omTarget);
	if (omType == "BLT")
		int ilIndex = m_ToBaggageBelt.AddBitmap(olBitmap,omTarget);
	if (omType == "GAT")
		int ilIndex = m_ToGate.AddBitmap(olBitmap,omTarget);
	if (omType == "WRO")
		int ilIndex = m_ToLounge.AddBitmap(olBitmap,omTarget);

	olBitmap.Detach();
}

void AllocationChangeDlg::UpdatePositionList()
{
/*
	CTime olStartAlloc;
	CTime olEndAlloc;

	if (pomFlight->Adid[0] == 'A')
	{
		if (!ogPosDiaFlightData.GetPstAllocTimes(pomFlight,NULL,this->pomFlight->Psta,olStartAlloc, olEndAlloc))
		{
			return;
		}
	}
	else if (pomFlight->Adid[0] == 'D')
	{
		if (!ogPosDiaFlightData.GetPstAllocTimes(NULL,pomFlight,this->pomFlight->Pstd,olStartAlloc, olEndAlloc))
		{
			return;
		}
	}
	else
		return;

	m_ToPosition.ResetContent();

	if (!m_CheckPosition.GetCheck())	// all positions	
	{
		int ilPnam = ogBCD.GetFieldIndex("PST", "PNAM");
		
		for(int i = 0; i < ogBCD.GetDataCount("PST"); i++)
		{
			RecordSet rlRec;
			ogBCD.GetRecord("PST", i, rlRec);

			CString olPnam = rlRec[ilPnam];
			bool blIsAvailable = ogSpotAllocation.IsAvailable("PST",olPnam,olStartAlloc,olEndAlloc);
			CBitmap olBitmap;
			if (blIsAvailable)
				olBitmap.LoadBitmap(IDB_LIGHTBLUE);
			else
				olBitmap.LoadBitmap(IDB_ORANGE);
			int ilIndex = m_ToPosition.AddBitmap(olBitmap,olPnam);
			olBitmap.Detach();
		}

		int ilCurSel = 0;
		if (this->omType == "PST")
		{
			ilCurSel = m_ToPosition.FindStringExact(-1,this->omTarget);
		}
		else
		{
			if (this->pomFlight->Adid[0] == 'A')
				ilCurSel = m_ToPosition.FindStringExact(-1,this->pomFlight->Psta);
			else
				ilCurSel = m_ToPosition.FindStringExact(-1,this->pomFlight->Pstd);

		}

		m_ToPosition.SetCurSel(ilCurSel);
	}
	else	// resource chain only
	{
		CString olPnam;

		if (this->omType == "PST")
		{
			olPnam = this->omTarget;							
		}
		else
		{
			if (pomFlight->Adid[0] == 'A')
				olPnam = pomFlight->Psta;
			else
				olPnam = pomFlight->Pstd;
		}

		bool blIsAvailable = ogSpotAllocation.IsAvailable("PST",olPnam,olStartAlloc,olEndAlloc);
		CBitmap olBitmap;
		if (blIsAvailable)
			olBitmap.LoadBitmap(IDB_LIGHTBLUE);
		else
			olBitmap.LoadBitmap(IDB_ORANGE);
		int ilIndex = m_ToPosition.AddBitmap(olBitmap,olPnam);
		olBitmap.Detach();

		int ilCurSel = 0;
		if (this->omType == "PST")
		{
			ilCurSel = m_ToPosition.FindStringExact(-1,this->omTarget);
		}
		else
		{
			if (this->pomFlight->Adid[0] == 'A')
				ilCurSel = m_ToPosition.FindStringExact(-1,this->pomFlight->Psta);
			else
				ilCurSel = m_ToPosition.FindStringExact(-1,this->pomFlight->Pstd);

		}

		m_ToPosition.SetCurSel(ilCurSel);


	}
*/
}

void AllocationChangeDlg::SetPositionSelection(CString olDefault)
{
	int ilCurSel = 0;
	if (this->omType == "PST")
	{
		ilCurSel = m_ToPosition.FindStringExact(-1,this->omTarget);
	}
	else
	{
		if (this->pomFlight->Adid[0] == 'A')
			ilCurSel = m_ToPosition.FindStringExact(-1,this->pomFlight->Psta);
		else if (this->pomFlight->Adid[0] == 'D')
			ilCurSel = m_ToPosition.FindStringExact(-1,this->pomFlight->Pstd);

//		if (!olDefault.IsEmpty() && ilCurSel < 0)
		if (!olDefault.IsEmpty())
			ilCurSel = m_ToPosition.FindStringExact(-1,olDefault);
	}
	m_ToPosition.SetCurSel(ilCurSel);
}

void AllocationChangeDlg::SetGateSelection(CString olDefault)
{
	int ilCurSel = 0;
	if (this->omType == "GAT")
	{
		ilCurSel = m_ToGate.FindStringExact(-1,this->omTarget);
	}
	else
	{
		if (this->pomFlight->Adid[0] == 'A')
			ilCurSel = m_ToGate.FindStringExact(-1,this->pomFlight->Gta1);
		else if (this->pomFlight->Adid[0] == 'D')
			ilCurSel = m_ToGate.FindStringExact(-1,this->pomFlight->Gtd1);

//		if (!olDefault.IsEmpty() && ilCurSel < 0)
		if (!olDefault.IsEmpty())
			ilCurSel = m_ToGate.FindStringExact(-1,olDefault);
	}
	m_ToGate.SetCurSel(ilCurSel);
}

void AllocationChangeDlg::SetLoungeSelection(CString olDefault)
{
	int ilCurSel = 0;
	if (this->omType == "WRO")
	{
		ilCurSel = m_ToLounge.FindStringExact(-1,this->omTarget);
	}
	else
	{
		if (this->pomFlight->Adid[0] == 'D')
			ilCurSel = m_ToLounge.FindStringExact(-1,this->pomFlight->Wro1);

//		if (!olDefault.IsEmpty() && ilCurSel < 0)
		if (!olDefault.IsEmpty())
			ilCurSel = m_ToLounge.FindStringExact(-1,olDefault);
	}
	m_ToLounge.SetCurSel(ilCurSel);
}

void AllocationChangeDlg::SetBaggageBeltSelection(CString olDefault)
{
	int ilCurSel = 0;
	if (this->omType == "BLT")
	{
		ilCurSel = m_ToBaggageBelt.FindStringExact(-1,this->omTarget);
	}
	else
	{
		if (this->pomFlight->Adid[0] == 'A')
			ilCurSel = m_ToBaggageBelt.FindStringExact(-1,this->pomFlight->Blt1);

//		if (!olDefault.IsEmpty() && ilCurSel < 0)
		if (!olDefault.IsEmpty())
			ilCurSel = m_ToBaggageBelt.FindStringExact(-1,olDefault);
	}
	m_ToBaggageBelt.SetCurSel(ilCurSel);
}

void AllocationChangeDlg::UpdatePositionListFromGate()
{
	// resouce chain
	CString olCurGat;
	this->m_ToGate.GetWindowText(olCurGat);

	CMapStringToString olPosMapRED;
	CMapStringToString olPosMapBLUE;
	CStringArray olPosBLUE;
	CStringArray olPosRED;
	if (!ogSpotAllocation.GetResBy("PST", "GAT", olCurGat, olPosBLUE, olPosMapBLUE, olPosRED, olPosMapRED, this->pomFlight))
		return;

	CString olDefault;
	this->m_ToPosition.GetWindowText(olDefault); 

	if (m_CheckEmpty.GetCheck())
		this->m_FromPosition.GetWindowText(olDefault);

	m_ToPosition.ResetContent();//CComboBox

	if (!m_CheckPosition.GetCheck())	// all Pos
	{
		EmptyLineAatCombo (m_ToPosition);
		FillAatCombo (m_ToPosition, olPosBLUE, olPosMapBLUE);
		FillAatCombo (m_ToPosition, olPosRED, olPosMapRED);
	}
	else	// resouce chain only
	{
		EmptyLineAatCombo (m_ToPosition);
		FillAatCombo (m_ToPosition, olPosBLUE, olPosMapBLUE);
	}

	// set selection
	if (olPosBLUE.GetSize() > 0 && olDefault.IsEmpty())
	{
		for (int i=0; i<olPosBLUE.GetSize(); i++)
		{
			CString olTmp;
			olPosMapBLUE.Lookup(olPosBLUE.GetAt(i), olTmp);
			if (olTmp == "IDB_LIGHTBLUE")
			{
				olDefault = olPosBLUE.GetAt(i);
				break;
			}
		}
	}

	SetPositionSelection(olDefault);
}

void AllocationChangeDlg::UpdateGateListFromLounge()
{
	// resouce chain
	CString olCurWro;
	this->m_ToLounge.GetWindowText(olCurWro); 

	CMapStringToString olGatesMapRED;
	CMapStringToString olGatesMapBLUE;
	CStringArray olGatesBLUE;
	CStringArray olGatesRED;
	if (!ogSpotAllocation.GetResBy("GAT", "WRO", olCurWro, olGatesBLUE, olGatesMapBLUE, olGatesRED, olGatesMapRED, this->pomFlight))
		return;

	CString olDefault;
	this->m_ToGate.GetWindowText(olDefault);

	if (m_CheckEmpty.GetCheck())
		this->m_FromGate.GetWindowText(olDefault);

	m_ToGate.ResetContent();//CComboBox

	if (!m_CheckGate.GetCheck())	// all gates
	{
		EmptyLineAatCombo (m_ToGate);
		FillAatCombo (m_ToGate, olGatesBLUE, olGatesMapBLUE);
		FillAatCombo (m_ToGate, olGatesRED, olGatesMapRED);
	}
	else	// resouce chain only
	{
		EmptyLineAatCombo (m_ToGate);
		FillAatCombo (m_ToGate, olGatesBLUE, olGatesMapBLUE);
	}

	// set selection
	if (olGatesBLUE.GetSize() > 0 && olDefault.IsEmpty())
	{
		for (int i=0; i<olGatesBLUE.GetSize(); i++)
		{
			CString olTmp;
			olGatesMapBLUE.Lookup(olGatesBLUE.GetAt(i), olTmp);
			if (olTmp == "IDB_LIGHTBLUE")
			{
				olDefault = olGatesBLUE.GetAt(i);
				break;
			}
		}
	}

	SetGateSelection(olDefault);
}

void AllocationChangeDlg::UpdateGateListFromBelt()
{
	// resouce chain
	CString olCurBlt;
	this->m_ToBaggageBelt.GetWindowText(olCurBlt);

	CMapStringToString olGatesMapRED;
	CMapStringToString olGatesMapBLUE;
	CStringArray olGatesBLUE;
	CStringArray olGatesRED;
	if (!ogSpotAllocation.GetResBy("GAT", "BLT", olCurBlt, olGatesBLUE, olGatesMapBLUE, olGatesRED, olGatesMapRED, this->pomFlight))
		return;

	CString olDefault;
	this->m_ToGate.GetWindowText(olDefault);

	if (m_CheckEmpty.GetCheck())
		this->m_FromGate.GetWindowText(olDefault);

	m_ToGate.ResetContent();//CComboBox

	if (!m_CheckGate.GetCheck())	// all gates
	{
		EmptyLineAatCombo (m_ToGate);
		FillAatCombo (m_ToGate, olGatesBLUE, olGatesMapBLUE);
		FillAatCombo (m_ToGate, olGatesRED, olGatesMapRED);
	}
	else	// resouce chain only
	{
		EmptyLineAatCombo (m_ToGate);
		FillAatCombo (m_ToGate, olGatesBLUE, olGatesMapBLUE);
	}

	// set selection
	if (olGatesBLUE.GetSize() > 0 && olDefault.IsEmpty())
	{
		for (int i=0; i<olGatesBLUE.GetSize(); i++)
		{
			CString olTmp;
			olGatesMapBLUE.Lookup(olGatesBLUE.GetAt(i), olTmp);
			if (olTmp == "IDB_LIGHTBLUE")
			{
				olDefault = olGatesBLUE.GetAt(i);
				break;
			}
		}
	}

	SetGateSelection(olDefault);
}


void AllocationChangeDlg::UpdateGateList()
{
	//
	CString olCurPos;
	this->m_ToPosition.GetWindowText(olCurPos);

	CMapStringToString olGatesMapRED;
	CMapStringToString olGatesMapBLUE;
	CStringArray olGatesBLUE;
	CStringArray olGatesRED;
	if (!ogSpotAllocation.GetResBy("GAT", "PST", olCurPos, olGatesBLUE, olGatesMapBLUE, olGatesRED, olGatesMapRED, this->pomFlight, false, false, true))
		return;

	CString olDefault;
	this->m_ToGate.GetWindowText(olDefault);

	if (m_CheckEmpty.GetCheck())
		this->m_FromGate.GetWindowText(olDefault);

	m_ToGate.ResetContent();//CComboBox

	if (!m_CheckGate.GetCheck())	// all gates
	{
		EmptyLineAatCombo (m_ToGate);
		FillAatCombo (m_ToGate, olGatesBLUE, olGatesMapBLUE);
		FillAatCombo (m_ToGate, olGatesRED, olGatesMapRED);
	}
	else	// resouce chain only
	{
		EmptyLineAatCombo (m_ToGate);
		FillAatCombo (m_ToGate, olGatesBLUE, olGatesMapBLUE);
	}

	// set selection
//	if (olGatesBLUE.GetSize() > 0)
	if (olGatesBLUE.GetSize() > 0 && !m_CheckEmpty.GetCheck())
	{
		for (int i=0; i<olGatesBLUE.GetSize(); i++)
		{
			CString olTmp;
			olGatesMapBLUE.Lookup(olGatesBLUE.GetAt(i), olTmp);
			if (olTmp == "IDB_LIGHTBLUE")
			{
				olDefault = olGatesBLUE.GetAt(i);
				break;
			}
		}
	}

	SetGateSelection(olDefault);
}



void AllocationChangeDlg::UpdateLoungeList()
{
	CString olCurGat;
	this->m_ToGate.GetWindowText(olCurGat);

	CMapStringToString olWroMapRED;
	CMapStringToString olWroMapBLUE;
	CStringArray olWroBLUE;
	CStringArray olWroRED;
	if (!ogSpotAllocation.GetResBy("WRO", "GAT", olCurGat, olWroBLUE, olWroMapBLUE, olWroRED, olWroMapRED, this->pomFlight, false, false, true))
		return;

	CString olDefault;
	this->m_ToLounge.GetWindowText(olDefault);
	if (m_CheckEmpty.GetCheck())
		this->m_FromLounge.GetWindowText(olDefault);

	m_ToLounge.ResetContent();//CComboBox

	if (!m_CheckLounge.GetCheck())	// all gates
	{
		EmptyLineAatCombo (m_ToLounge);
		FillAatCombo (m_ToLounge, olWroBLUE, olWroMapBLUE);
		FillAatCombo (m_ToLounge, olWroRED, olWroMapRED);
	}
	else	// resouce chain only
	{
		EmptyLineAatCombo (m_ToLounge);
		FillAatCombo (m_ToLounge, olWroBLUE, olWroMapBLUE);
	}

	// set selection
//	if (olWroBLUE.GetSize() > 0 && olDefault.IsEmpty())
	if (olWroBLUE.GetSize() > 0 && !m_CheckEmpty.GetCheck())
	{
		for (int i=0; i<olWroBLUE.GetSize(); i++)
		{
			CString olTmp;
			olWroMapBLUE.Lookup(olWroBLUE.GetAt(i), olTmp);
			if (olTmp == "IDB_LIGHTBLUE")
			{
				olDefault = olWroBLUE.GetAt(i);
				break;
			}
		}
	}

	// set selection
	SetLoungeSelection(olDefault);
}


void AllocationChangeDlg::UpdateBaggageBeltList()
{
	CString olCurGat;
	this->m_ToGate.GetWindowText(olCurGat);

	CMapStringToString olBltMapRED;
	CMapStringToString olBltMapBLUE;
	CStringArray olBltBLUE;
	CStringArray olBltRED;
	if (!ogSpotAllocation.GetResBy("BLT", "GAT", olCurGat, olBltBLUE, olBltMapBLUE, olBltRED, olBltMapRED, this->pomFlight, false, false, true))
		return;

	CString olDefault;
	this->m_ToBaggageBelt.GetWindowText(olDefault);
	if (m_CheckEmpty.GetCheck())
		this->m_FromBaggageBelt.GetWindowText(olDefault);

	m_ToBaggageBelt.ResetContent();//CComboBox

	if (!m_CheckBaggageBelt.GetCheck())	// all gates
	{
		EmptyLineAatCombo (m_ToBaggageBelt);
		FillAatCombo (m_ToBaggageBelt, olBltBLUE, olBltMapBLUE);
		FillAatCombo (m_ToBaggageBelt, olBltRED, olBltMapRED);
	}
	else	// resouce chain only
	{
		EmptyLineAatCombo (m_ToBaggageBelt);
		FillAatCombo (m_ToBaggageBelt, olBltBLUE, olBltMapBLUE);
	}

	// set selection
//	if (olBltBLUE.GetSize() > 0 && olDefault.IsEmpty())
	if (olBltBLUE.GetSize() > 0 && !m_CheckEmpty.GetCheck())
	{
		for (int i=0; i<olBltBLUE.GetSize(); i++)
		{
			CString olTmp;
			olBltMapBLUE.Lookup(olBltBLUE.GetAt(i), olTmp);
			if (olTmp == "IDB_LIGHTBLUE")
			{
				olDefault = olBltBLUE.GetAt(i);
				break;
			}
		}
	}

	SetBaggageBeltSelection(olDefault);
}

void AllocationChangeDlg::UpdateExitList()
{
/*
	CTime olStartAlloc;
	CTime olEndAlloc;

	if (!ogPosDiaFlightData.GetBltAllocTimes(*pomFlight,this->pomFlight->Blt1, 1,olStartAlloc, olEndAlloc))
	{
		return;
	}

	m_ToExit.ResetContent();

	// get resouce chain only
	CStringArray olExts;
	CString olCurBlt;
	CMapStringToString olExtsMap;
	this->m_ToBaggageBelt.GetWindowText(olCurBlt);
	this->GetExitsByBaggageBelt(olCurBlt, olExts, olExtsMap);

	if (!m_ToExit.GetCheck())	// all lounges
	{
		int ilEnam = ogBCD.GetFieldIndex("EXT", "ENAM");
		
		for(int i = 0; i < ogBCD.GetDataCount("EXT"); i++)
		{
			RecordSet rlRec;
			ogBCD.GetRecord("EXT", i, rlRec);

			CString olEnam = rlRec[ilWnam];

			bool blIsAvailable = this->IsAvailable("EXT",olEnam,olStartAlloc,olEndAlloc);
			CBitmap olBitmap;
			CString olTmp;
			if (olExtsMap.Lookup(olEnam, olTmp))// is in resource chain
			{
				if (blIsAvailable)
					olBitmap.LoadBitmap(IDB_LIGHTBLUE);
				else
					olBitmap.LoadBitmap(IDB_ORANGE);
			}
			else // is not in resource chain
				olBitmap.LoadBitmap(IDB_RED);

			int ilIndex = m_ToExit.AddBitmap(olBitmap,olEnam);
			olBitmap.Detach();
		}

		int ilCurSel = 0;
		if (this->omType == "EXT")
		{
			ilCurSel = m_ToExit.FindStringExact(-1,this->omTarget);
		}
		else
		{
			if (this->pomFlight->Adid[0] == 'A')
				ilCurSel = m_ToExit.FindStringExact(-1,this->pomFlight->Ext1);

		}

		m_ToExit.SetCurSel(ilCurSel);
	}
	else	// resouce chain only
	{
		for(int i = 0; i < olExts.GetSize(); i++)
		{
			CString olEnam = olExts[i];

			bool blIsAvailable = this->IsAvailable("EXT",olEnam,olStartAlloc,olEndAlloc);
			CBitmap olBitmap;
			if (blIsAvailable)
				olBitmap.LoadBitmap(IDB_LIGHTBLUE);
			else
				olBitmap.LoadBitmap(IDB_ORANGE);

			int ilIndex = m_ToExit.AddBitmap(olBitmap,olEnam);
			olBitmap.Detach();
		}

		int ilCurSel = 0;
		if (this->omType == "EXT")
		{
			ilCurSel = m_ToExit.FindStringExact(-1,this->omTarget);
		}
		else
		{
			if (this->pomFlight->Adid[0] == 'A')
				ilCurSel = m_ToExit.FindStringExact(-1,this->pomFlight->Ext1);

		}

		m_ToExit.SetCurSel(ilCurSel);
	}
*/
}

bool AllocationChangeDlg::FillAatCombo (AatBitmapComboBox& ropAatCombo, CStringArray& ropArray, CMapStringToString& ropMap)
{
	for(int i = 0; i < ropArray.GetSize(); i++)
	{
		CString olTmp;
		if (ropMap.Lookup(ropArray.GetAt(i), olTmp))// is in resource chain
		{
			CBitmap olBitmap;
			if (olTmp == "IDB_RED")
				olBitmap.LoadBitmap(IDB_RED);
			else if (olTmp == "IDB_REDBLOCKED")
				olBitmap.LoadBitmap(IDB_REDBLOCKED);
			else if (olTmp == "IDB_LIGHTBLUE")
				olBitmap.LoadBitmap(IDB_LIGHTBLUE);
			else if (olTmp == "IDB_LIGHTBLUEBLOCKED")
				olBitmap.LoadBitmap(IDB_LIGHTBLUEBLOCKED);
			else
				olBitmap.LoadBitmap(IDB_ORANGE);

			int ilIndex = ropAatCombo.AddBitmap(olBitmap, ropArray.GetAt(i));
			olBitmap.Detach();
		}
	}
	return true;
}


bool AllocationChangeDlg::EmptyLineAatCombo (AatBitmapComboBox& ropAatCombo)
{
	CBitmap olBitmap;
	olBitmap.LoadBitmap(IDB_ORANGE);

	int ilIndex = ropAatCombo.AddBitmap(olBitmap, "");
	olBitmap.Detach();

	return true;
}

bool AllocationChangeDlg::GetCcaRes(CCADATA* prpCca, CMapStringToString& ropMapBLUE, CMapStringToString& ropMapRED)
{
	ASSERT(FALSE);

	if (!prpCca)
		return false;

	CString olCgru = CString(prpCca->Cgru);
	CStringArray olStrArray;
	ExtractItemList(olCgru, &olStrArray, ';');
	int ilItemCount = olStrArray.GetSize();
	for(int i = 0; i < ilItemCount; i++)
	{
		long llUrno = atol(olStrArray[i].GetBuffer(0));
		CCSPtrArray<GRMDATA> olGrmList;
		ogGrmData.GetGrmDataByGrnUrno(olGrmList, llUrno);
		int ilGrmCount = olGrmList.GetSize();
		CString olCounterNo;
		for(int j = 0; j < ilGrmCount; j++)
		{
			CString olCkic;
			CString olTmpValu = olGrmList[j].Valu;
			ogBCD.GetField("CIC", "URNO", olGrmList[j].Valu, "CNAM", olCkic);
			if(!olCkic.IsEmpty())
			{
				ropMapBLUE.SetAt(olCkic, "IDB_LIGHTBLUE");
				olCounterNo += "," + olCkic;
			}
			else
			{
				olCounterNo += ",";
			}
		}
	}


	int ilCnam = ogBCD.GetFieldIndex("CIC", "CNAM");
	for(int l = 0; l < ogBCD.GetDataCount("CIC"); l++)
	{
		RecordSet rlRec;
		ogBCD.GetRecord("CIC", l, rlRec);

		CString olCnam = rlRec[ilCnam];
		bool blIsAvailable = ogSpotAllocation.IsAvailable("CIC", olCnam, prpCca->Ckbs, prpCca->Ckes);

		if (blIsAvailable)
			blIsAvailable = ogCcaDiaFlightData.omCcaData.CkeckOverlapping(olCnam, prpCca->Ckbs, prpCca->Ckes);

		CString olTmp;
		if (ropMapBLUE.Lookup(olCnam, olTmp))
		{
			if (blIsAvailable)
				ropMapBLUE.SetAt(olCnam, "IDB_LIGHTBLUE");
			else
				ropMapBLUE.SetAt(olCnam, "IDB_LIGHTBLUEBLOCKED");
		}
		else
		{
			if (blIsAvailable)
				ropMapRED.SetAt(olCnam, "IDB_RED");
			else
				ropMapRED.SetAt(olCnam, "IDB_REDBLOCKED");
		}
	}

	return true;
}


void AllocationChangeDlg::ReadCcaData(DIAFLIGHTDATA* prpFlight)
{
	if (!prpFlight || prpFlight->Adid[0] != 'D' || prpFlight->Urno == 0)
		return;

	if(ogPrivList.GetStat("RESCHAIN_CB_Cic") == '-' )
		return;

	omCcaData.omData.RemoveAll();
	omCcaData.ClearAll();
	omDemandCcaData.omData.RemoveAll();
	omDemandCcaData.ClearAll();

	SEASONDLGFLIGHTDATA rlSeason (*prpFlight);
	CCAFLIGHTDATA rlCcaFlight;
	rlCcaFlight = rlSeason;

	CString olWhere;
	olWhere.Format("FLNU = %d AND CKIC != ' '", prpFlight->Urno);
	omCcaData.ReadSpecial(olWhere);

	if (bgNewRecalculateResChain && !m_Recalc.GetCheck())
		return;

	int ilCountCtr = omCcaData.omData.GetSize();
	for (int i=0; i<omCcaData.omData.GetSize(); i++)
	{
		CCADATA* prlCca = &omCcaData.omData[i];
		int wait = 0;
	}

	CCSPtrArray<CCADATA> olFlightCcaDemList;
	// check ccas with rules CMapPtrToPtr
	CCSPtrArray<CCADATA> olFlightCcaTmpList1; // must be empty, because need all demands!!
//	bool blCheckDem = ogDataSet.CheckDemands(&rlCcaFlight, omCcaData.omData, olFlightCcaDemList);
	bool blCheckDem = ogDataSet.CheckDemands(&rlCcaFlight, olFlightCcaTmpList1, olFlightCcaDemList);



	for (int k=0; k<olFlightCcaDemList.GetSize(); k++)
	{
		CCADATA* prlCca = &olFlightCcaDemList[k];
		omCcaData.omData.Add(prlCca);
	}

	//create hole demandlist for the choice side
	CCSPtrArray<CCADATA> olFlightCcaTmpList; // must be empty, because need all demands!!
	blCheckDem = ogDataSet.CheckDemands(&rlCcaFlight, olFlightCcaTmpList, omDemandCcaData.omData);
/*
	for (int k=0; k<omDemandCcaData.omData.GetSize(); k++)
	{
		CCADATA* prlCca = &omDemandCcaData.omData[k];
		omCcaData.omData.Add(prlCca);
	}
*/

	omCcaData.omData.Sort(CompareCcas); 

//YYYY
//	olFlightCcaDemList.DeleteAll();
//	olFlightCcaTmpList.DeleteAll();

	return;
}

void AllocationChangeDlg::CreateChoiceList(CCADATA* prpCca, CMapStringToString& ropMapBLUE, CMapStringToString& ropMapRED, CString& opChoiceList)
{
	CString olEmpty;
	int iltmp = 213;
	olEmpty.Format("%s\t#BMP(%d)\t%s\n",/* ilSort,*/ "", iltmp, "");
	opChoiceList += olEmpty;


	POSITION pos;
	CString olKey;
	CString olValue;
	for( pos = ropMapBLUE.GetStartPosition(); pos != NULL; )
	{
		ropMapBLUE.GetNextAssoc( pos, olKey , olValue );
		int ilBitmap = 0;
		int ilSort = 0;
		if (olValue == "IDB_LIGHTBLUE")
		{
			ilBitmap = 214;
			ilSort = 11;
		}
		else
		{
			ilBitmap = 219;
			ilSort = 22;
		}

		CString olFormat;
		olFormat.Format("%s\t#BMP(%d)\t%s\n",/* ilSort,*/ olKey, ilBitmap, olKey);
		opChoiceList += olFormat;
	}

	for( pos = ropMapRED.GetStartPosition(); pos != NULL; )
	{
		ropMapRED.GetNextAssoc( pos, olKey , olValue );
		int ilBitmap = 0;
		int ilSort = 0;
		if (olValue == "IDB_RED")
		{
			ilBitmap = 212;
			ilSort = 88;
		}
		else
		{
			ilBitmap = 220;
			ilSort = 99;
		}

		CString olFormat;
		olFormat.Format("%s\t#BMP(%d)\t%s\n",/* ilSort,*/ olKey, ilBitmap, olKey);
		opChoiceList += olFormat;
	}

}

void AllocationChangeDlg::UpdateCheckinCounterGrid()
{
	if (bgNewRecalculateResChain && !m_Recalc.GetCheck())
	{
		UpdateCheckinCounterGrid1();
		return;
	}

//	WORD wStyleCombo = pomCheckinCounter->GetParam()->GetStylesMap()->GetBaseStyleId(szComboStyle);

	// enable update
	pomCheckinCounter->GetParam()->SetLockReadOnly(FALSE);

	// enable immediate update
	pomCheckinCounter->LockUpdate(FALSE);

	// delete all rows
	if (pomCheckinCounter->GetRowCount() > 0)
	{
		pomCheckinCounter->RemoveRows(1,pomCheckinCounter->GetRowCount());
	}

	// write all checkin counters
	int ilCountCtr = omCcaData.omData.GetSize();
	int ilNotAllocated = 0;
	bool blInBlueMap = false;
	CMapStringToString olCnamDefault;
	for (int k=0; k<omCcaData.omData.GetSize(); k++)
	{
		CCADATA* prlCca = &omCcaData.omData[k];

		//YYYY
//		int il = sizeof(*prlCca); 560

		CString olCnam;
		CString olCnamDefaultForDemand;
		CString olChoiceList;
		int ilBitmap = 212;
		CMapStringToString ropMapBLUE;
		CMapStringToString ropMapRED;
		CMapStringToString ropMapTMP;
		if (prlCca)
		{
			olCnam = CString(prlCca->Ckic);
			blInBlueMap = false;

			ilNotAllocated = omDemandCcaData.omData.GetSize();
			if (k < omDemandCcaData.omData.GetSize())
			{
				CCADATA* prlDemCca = &omDemandCcaData.omData[k];

				CStringArray olBLUEArray;
				if (ogDataSet.GetCcaRes(prlDemCca, ropMapBLUE, ropMapRED, olBLUEArray)) 
				{
					CreateChoiceList(prlDemCca, ropMapBLUE, ropMapTMP, olChoiceList);

					if (ilNotAllocated > 0)
					{
						if (olBLUEArray.GetSize() > 0)
						{
							for(int i = 0; i < olBLUEArray.GetSize(); i++)
							{
								CString olTmp;
								if (!olCnamDefault.Lookup(olBLUEArray.GetAt(i), olTmp))// is in resource chain
								{
									olCnamDefaultForDemand = olBLUEArray.GetAt(i);
									olCnamDefault.SetAt(olCnamDefaultForDemand, "1");
									blInBlueMap = true;
									break;
								}
							}
						}

						if (!olCnam.IsEmpty())
						{
						}
						else
							olCnam = "Demand";

						ilBitmap = 214;
					}

					CString olTmp;
					if (ropMapBLUE.Lookup(olCnamDefaultForDemand, olTmp) || blInBlueMap)
					{
						if (olTmp == "IDB_LIGHTBLUE")
							ilBitmap = 214;
						else
							ilBitmap = 219;
					}
					else if (ropMapRED.Lookup(olCnamDefaultForDemand, olTmp))
					{
						if (olTmp == "IDB_RED")
							ilBitmap = 212;
						else
							ilBitmap = 220;
					}
				}
			}
		}
		else
			continue;

		if (pomCheckinCounter->InsertRows(pomCheckinCounter->GetRowCount()+1,1))
		{
			CGXStyle olStyle;
			olStyle.SetEnabled(FALSE);
			olStyle.SetReadOnly(TRUE);

			int i = pomCheckinCounter->GetRowCount();
//			pomCheckinCounter->SetRowHeight(i, i, 14);

			// 1 column = "From" value
//			pomCheckinCounter->SetStyleRange(CGXRange(i,1,i,1),olStyle.SetValue("CIC 01"));
			pomCheckinCounter->SetStyleRange(CGXRange(i,1,i,1),olStyle.SetValue(olCnam));

			// 2 column = "->" value
			pomCheckinCounter->SetStyleRange(CGXRange(i,2,i,2),olStyle.SetValue("   ->   "));
 

			// 3 column = "To" bitmap 
			CString olFormat;
			if (ilBitmap != 0)
				olFormat.Format("#BMP(%d)", ilBitmap);
			pomCheckinCounter->SetStyleRange(CGXRange(i,3,i,3),olStyle.SetControl(GX_IDS_CTRL_STATIC)
																	  .SetValue(olFormat));
//																	  .SetValue("#BMP(219)"));
			// 4 column = "To" value
			if (prlCca && strlen(prlCca->Ckic) != 0 && prlCca->Ckba != TIMENULL)
			{
				olStyle.SetEnabled(FALSE);
				olStyle.SetReadOnly(TRUE);
				CWnd* wnd = GetDlgItem(IDC_OPEN_CIC);
				if (wnd)
					wnd->ShowWindow(SW_SHOW);
				}
			else
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(FALSE);
			}

			// 5 column = "valid only" flag
			CString olVal = "1";
//			olStyle.SetEnabled(TRUE);
//			olStyle.SetReadOnly(FALSE);

			if (m_CheckEmpty.GetCheck())
			{
				olVal = "0";
//				olStyle.SetEnabled(FALSE);
//				olStyle.SetReadOnly(TRUE);
			}

			pomCheckinCounter->SetStyleRange(CGXRange(i,5,i,5),olStyle.SetControl(GX_IDS_CTRL_CHECKBOX3D)
																	  .SetChoiceList("1\n0\n")
	//																  .SetInterior(RGB(255,255,255))
																	  .SetValue(olVal)
//																	  .SetValue("1")
																	  );


			if (olCnam == "Demand" || blInBlueMap)
			{
				if (m_CheckEmpty.GetCheck())
					olCnamDefaultForDemand = "";
				pomCheckinCounter->SetStyleRange(CGXRange(i,4,i,4),olStyle.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
	//																	  .SetChoiceList("CIC 01\t#BMP(212)\tCIC 01\nCIC 02\t#BMP(213)\tCIC02\nCIC 05\t#BMP(219)\tCIC 05\n")
																		  .SetChoiceList(olChoiceList)
																		  .SetUserAttribute(GX_IDS_UA_TABLIST_KEYCOL, _T("0"))
																		  .SetUserAttribute(GX_IDS_UA_TABLIST_TEXTCOL, _T("2"))
																		  .SetUserAttribute(GX_IDS_UA_TABLIST_COLWIDTHS, _T("0,16,50"))
																		  .SetUserAttribute(GX_IDS_UA_TABLIST_SHOWALLCOLS, _T("1"))
																		  .SetUserAttribute(GX_IDS_UA_TABLIST_SORTCOL, _T("0"))
																		  .SetValue(olCnamDefaultForDemand));
			}
			else
			{
				if (!m_CheckEmpty.GetCheck())
					olCnam = "";
				pomCheckinCounter->SetStyleRange(CGXRange(i,4,i,4),olStyle.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
	//																	  .SetChoiceList("CIC 01\t#BMP(212)\tCIC 01\nCIC 02\t#BMP(213)\tCIC02\nCIC 05\t#BMP(219)\tCIC 05\n")
																		  .SetChoiceList(olChoiceList)
																		  .SetUserAttribute(GX_IDS_UA_TABLIST_KEYCOL, _T("0"))
																		  .SetUserAttribute(GX_IDS_UA_TABLIST_TEXTCOL, _T("2"))
																		  .SetUserAttribute(GX_IDS_UA_TABLIST_COLWIDTHS, _T("0,16,50"))
																		  .SetUserAttribute(GX_IDS_UA_TABLIST_SHOWALLCOLS, _T("1"))
																		  .SetUserAttribute(GX_IDS_UA_TABLIST_SORTCOL, _T("0"))
																		  .SetValue(olCnam));
//																		  .SetValue(""));
			}

			UpdateChoiceList(olVal, i);

/*
			// 5 column = "valid only" flag
			CString olVal = "1";
			olStyle.SetEnabled(TRUE);
			olStyle.SetReadOnly(FALSE);

			if (m_CheckEmpty.GetCheck())
			{
				olVal = "0";
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(TRUE);
			}
			pomCheckinCounter->SetStyleRange(CGXRange(i,5,i,5),olStyle.SetControl(GX_IDS_CTRL_CHECKBOX3D)
																	  .SetChoiceList("1\n0\n")
	//																  .SetInterior(RGB(255,255,255))
																	  .SetValue(olVal)
//																	  .SetValue("1")
																	  );
*/
			// 6 column = "ptr" (hidden)value
//			char *ptr = new char[80];
			olStyle.SetEnabled(FALSE);
			olStyle.SetReadOnly(TRUE);
//			pomCheckinCounter->SetStyleRange(CGXRange(i,6,i,6),olStyle.SetValue((DWORD)ptr));
			pomCheckinCounter->SetStyleRange(CGXRange(i,6,i,6),olStyle.SetValue((DWORD)prlCca));
			
		}
	}

	// disable update
	pomCheckinCounter->GetParam()->SetLockReadOnly(TRUE);

	// redraw whole grid
	pomCheckinCounter->Redraw();

}

void AllocationChangeDlg::UpdateCheckinCounterGrid1()
{
	CMapStringToString ropMapBLUE;
	CMapStringToString ropMapRED;
	CMapStringToString ropMapTMP;
	CString olChoiceList;
	// enable update
	pomCheckinCounter->GetParam()->SetLockReadOnly(FALSE);

	// enable immediate update
	pomCheckinCounter->LockUpdate(FALSE);

	// delete all rows
	if (pomCheckinCounter->GetRowCount() > 0)
	{
		pomCheckinCounter->RemoveRows(1,pomCheckinCounter->GetRowCount());
	}

	// write all checkin counters
	for (int k=0; k<omCcaData.omData.GetSize(); k++)
	{
		CCADATA* prlCca = &omCcaData.omData[k];
		CString olCnam("");

		if (prlCca)
		{
			olCnam = CString(prlCca->Ckic);
			CreateChoiceList(prlCca, ropMapBLUE, ropMapTMP, olChoiceList);
		}
		else
			continue;

		int l = pomCheckinCounter->GetRowCount();
		if (pomCheckinCounter->InsertRows(pomCheckinCounter->GetRowCount()+1,1))
		{
			CGXStyle olStyle;
			olStyle.SetEnabled(FALSE);
			olStyle.SetReadOnly(TRUE);

			int i = pomCheckinCounter->GetRowCount();

			// 1 column = "From" value
			pomCheckinCounter->SetStyleRange(CGXRange(i,1,i,1),olStyle.SetValue(olCnam));

			// 2 column = "->" value
			pomCheckinCounter->SetStyleRange(CGXRange(i,2,i,2),olStyle.SetValue("   ->   "));
 

			// 3 column = "To" bitmap 
			CString olFormat;
			int ilBitmap = 289;
			olFormat.Format("#BMP(%d)", ilBitmap);
			pomCheckinCounter->SetStyleRange(CGXRange(i,3,i,3),olStyle.SetControl(GX_IDS_CTRL_STATIC)
																	  .SetValue(olFormat));
																	  //.SetValue("#BMP(214)"));
			// 4 column = "To" value
			if (prlCca && strlen(prlCca->Ckic) != 0 && prlCca->Ckba != TIMENULL)
			{
				CWnd* wnd = GetDlgItem(IDC_OPEN_CIC);
				if (wnd)
					wnd->ShowWindow(SW_SHOW);
			}


			pomCheckinCounter->SetStyleRange(CGXRange(i,4,i,4),olStyle.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
	//																	  .SetChoiceList("CIC 01\t#BMP(212)\tCIC 01\nCIC 02\t#BMP(213)\tCIC02\nCIC 05\t#BMP(219)\tCIC 05\n")
																		  .SetChoiceList(olChoiceList)
																		  .SetUserAttribute(GX_IDS_UA_TABLIST_KEYCOL, _T("0"))
																		  .SetUserAttribute(GX_IDS_UA_TABLIST_TEXTCOL, _T("2"))
																		  .SetUserAttribute(GX_IDS_UA_TABLIST_COLWIDTHS, _T("0,16,50"))
																		  .SetUserAttribute(GX_IDS_UA_TABLIST_SHOWALLCOLS, _T("1"))
																		  .SetUserAttribute(GX_IDS_UA_TABLIST_SORTCOL, _T("0"))
																		  .SetValue(olCnam));
//																		  .SetValue(""));

			UpdateChoiceList("0", i);

			// 6 column = "ptr" (hidden)value
			pomCheckinCounter->SetStyleRange(CGXRange(i,6,i,6),olStyle.SetValue((DWORD)prlCca));
			
		}
	}

	// disable update
	pomCheckinCounter->GetParam()->SetLockReadOnly(TRUE);

	// redraw whole grid
	pomCheckinCounter->Redraw();

}


/*
void AllocationChangeDlg::UpdateCheckinCounterGrid()
{
//	WORD wStyleCombo = pomCheckinCounter->GetParam()->GetStylesMap()->GetBaseStyleId(szComboStyle);

	// enable update
	pomCheckinCounter->GetParam()->SetLockReadOnly(FALSE);

	// enable immediate update
	pomCheckinCounter->LockUpdate(FALSE);

	// delete all rows
	if (pomCheckinCounter->GetRowCount() > 0)
	{
		pomCheckinCounter->RemoveRows(1,pomCheckinCounter->GetRowCount());
	}

	// write all checkin counters
	int ilCountCtr = omCcaData.omData.GetSize();
	int ilNotAllocated = 0;
	bool blInBlueMap = false;
	int ilChoice = 0;
	for (int k=0; k<omCcaData.omData.GetSize(); k++)
	{
		CCADATA* prlCca = &omCcaData.omData[k];

		//YYYY
//		int il = sizeof(*prlCca); 560

		CString olCnam;
		CString olCnamDefaultForDemand;
		CString olChoiceList;
		int ilBitmap = 212;
		CMapStringToString ropMapBLUE;
		CMapStringToString ropMapRED;
		CMapStringToString ropMapTMP;
		if (prlCca)
		{
			olCnam = CString(prlCca->Ckic);
			blInBlueMap = false;

			ilNotAllocated = omDemandCcaData.omData.GetSize();
			if (k < omDemandCcaData.omData.GetSize())
			{
				CCADATA* prlDemCca = &omDemandCcaData.omData[k];

				if (k>0)
				{
					CCADATA* prlDemBefore = &omDemandCcaData.omData[k-1];
					if (prlDemCca && prlDemBefore && CString(prlDemCca->Cgru) != CString(prlDemBefore->Cgru))
						ilChoice = 0;
				}

				CStringArray olBLUEArray;
				if (ogDataSet.GetCcaRes(prlDemCca, ropMapBLUE, ropMapRED, olBLUEArray)) 
				{
					CreateChoiceList(prlDemCca, ropMapBLUE, ropMapTMP, olChoiceList);

//					if (olBLUEArray.GetSize() >= ilNotAllocated && ilNotAllocated > 0)
					if (ilNotAllocated > 0)
					{
						if (ilChoice >= olBLUEArray.GetSize())
							ilChoice = olBLUEArray.GetSize()-1;

						olCnamDefaultForDemand = olBLUEArray.GetAt(ilChoice);
//						olCnamDefaultForDemand = olBLUEArray.GetAt(k);
						if (!olCnam.IsEmpty())
						{
							CString olTmp;
							if (ropMapBLUE.Lookup(olCnam, olTmp))
							{
								olCnamDefaultForDemand = olCnam;
								blInBlueMap = true;
							}
							else
							{
//								olCnamDefaultForDemand = olBLUEArray.GetAt(k);
								olCnamDefaultForDemand = olBLUEArray.GetAt(ilChoice);
								blInBlueMap = true;
							}
						}
						else
							olCnam = "Demand";

						ilBitmap = 214;

						ilChoice++;
					}

					CString olTmp;
					if (ropMapBLUE.Lookup(olCnam, olTmp) || blInBlueMap)
					{
						if (olTmp == "IDB_LIGHTBLUE")
							ilBitmap = 214;
						else
							ilBitmap = 219;
					}
					else if (ropMapRED.Lookup(olCnam, olTmp))
					{
						if (olTmp == "IDB_RED")
							ilBitmap = 212;
						else
							ilBitmap = 220;
					}
				}
			}
		}
		else
			continue;

		if (pomCheckinCounter->InsertRows(pomCheckinCounter->GetRowCount()+1,1))
		{
			CGXStyle olStyle;
			olStyle.SetEnabled(FALSE);
			olStyle.SetReadOnly(TRUE);

			int i = pomCheckinCounter->GetRowCount();
//			pomCheckinCounter->SetRowHeight(i, i, 14);

			// 1 column = "From" value
//			pomCheckinCounter->SetStyleRange(CGXRange(i,1,i,1),olStyle.SetValue("CIC 01"));
			pomCheckinCounter->SetStyleRange(CGXRange(i,1,i,1),olStyle.SetValue(olCnam));

			// 2 column = "->" value
			pomCheckinCounter->SetStyleRange(CGXRange(i,2,i,2),olStyle.SetValue("   ->   "));
 

			// 3 column = "To" bitmap 
			CString olFormat;
			if (ilBitmap != 0)
				olFormat.Format("#BMP(%d)", ilBitmap);
			pomCheckinCounter->SetStyleRange(CGXRange(i,3,i,3),olStyle.SetControl(GX_IDS_CTRL_STATIC)
																	  .SetValue(olFormat));
//																	  .SetValue("#BMP(219)"));

			// 4 column = "To" value
			if (prlCca && strlen(prlCca->Ckic) != 0 && prlCca->Ckba != TIMENULL)
			{
				olStyle.SetEnabled(FALSE);
				olStyle.SetReadOnly(TRUE);
				CWnd* wnd = GetDlgItem(IDC_OPEN_CIC);
				if (wnd)
					wnd->ShowWindow(SW_SHOW);
				}
			else
			{
				olStyle.SetEnabled(TRUE);
				olStyle.SetReadOnly(FALSE);
			}

			if (olCnam == "Demand" || blInBlueMap)
			{
				pomCheckinCounter->SetStyleRange(CGXRange(i,4,i,4),olStyle.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
	//																	  .SetChoiceList("CIC 01\t#BMP(212)\tCIC 01\nCIC 02\t#BMP(213)\tCIC02\nCIC 05\t#BMP(219)\tCIC 05\n")
																		  .SetChoiceList(olChoiceList)
																		  .SetUserAttribute(GX_IDS_UA_TABLIST_KEYCOL, _T("0"))
																		  .SetUserAttribute(GX_IDS_UA_TABLIST_TEXTCOL, _T("2"))
																		  .SetUserAttribute(GX_IDS_UA_TABLIST_COLWIDTHS, _T("0,16,50"))
																		  .SetUserAttribute(GX_IDS_UA_TABLIST_SHOWALLCOLS, _T("1"))
																		  .SetUserAttribute(GX_IDS_UA_TABLIST_SORTCOL, _T("0"))
																		  .SetValue(olCnamDefaultForDemand));
			}
			else
			{
				pomCheckinCounter->SetStyleRange(CGXRange(i,4,i,4),olStyle.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
	//																	  .SetChoiceList("CIC 01\t#BMP(212)\tCIC 01\nCIC 02\t#BMP(213)\tCIC02\nCIC 05\t#BMP(219)\tCIC 05\n")
																		  .SetChoiceList(olChoiceList)
																		  .SetUserAttribute(GX_IDS_UA_TABLIST_KEYCOL, _T("0"))
																		  .SetUserAttribute(GX_IDS_UA_TABLIST_TEXTCOL, _T("2"))
																		  .SetUserAttribute(GX_IDS_UA_TABLIST_COLWIDTHS, _T("0,16,50"))
																		  .SetUserAttribute(GX_IDS_UA_TABLIST_SHOWALLCOLS, _T("1"))
																		  .SetUserAttribute(GX_IDS_UA_TABLIST_SORTCOL, _T("0"))
//																		  .SetValue(olCnam));
																		  .SetValue(""));
			}


			// 5 column = "valid only" flag
			olStyle.SetEnabled(TRUE);
			olStyle.SetReadOnly(FALSE);
			pomCheckinCounter->SetStyleRange(CGXRange(i,5,i,5),olStyle.SetControl(GX_IDS_CTRL_CHECKBOX3D)
																	  .SetChoiceList("1\n0\n")
	//																  .SetInterior(RGB(255,255,255))
																	  .SetValue("1")
																	  );

			// 6 column = "ptr" (hidden)value
//			char *ptr = new char[80];
			olStyle.SetEnabled(FALSE);
			olStyle.SetReadOnly(TRUE);
//			pomCheckinCounter->SetStyleRange(CGXRange(i,6,i,6),olStyle.SetValue((DWORD)ptr));
			pomCheckinCounter->SetStyleRange(CGXRange(i,6,i,6),olStyle.SetValue((DWORD)prlCca));
			
		}
	}

	// disable update
	pomCheckinCounter->GetParam()->SetLockReadOnly(TRUE);

	// redraw whole grid
	pomCheckinCounter->Redraw();

}
*/
void AllocationChangeDlg::UpdateChoiceList(CString opCheck, int ipRow)
{
	if (ipRow < 1 || ipRow > omCcaData.omData.GetSize())
		return;

	bool blDelete = false;
	CCADATA* prlCca;
	if (ipRow <= omDemandCcaData.omData.GetSize())
		prlCca = &omDemandCcaData.omData[ipRow-1];
	else
	{
		prlCca = new CCADATA;
		blDelete = true;
	}

	CString olCnam;
	CString olChoiceList;
	CMapStringToString ropMapBLUE;
	CMapStringToString ropMapRED;
	CMapStringToString ropMapTMP;
	if (prlCca)
	{
		olCnam = CString(prlCca->Ckic);
		CStringArray olTmp;
		if (ogDataSet.GetCcaRes(prlCca, ropMapBLUE, ropMapRED, olTmp))
		{
			if (opCheck == "0")
				CreateChoiceList(prlCca, ropMapBLUE, ropMapRED, olChoiceList);
			else
				CreateChoiceList(prlCca, ropMapBLUE, ropMapTMP, olChoiceList);
		}
		CGXStyle olStyle;
		pomCheckinCounter->SetStyleRange(CGXRange(ipRow,4,ipRow,4),olStyle.SetChoiceList(olChoiceList));

		pomCheckinCounter->Redraw();

		if (blDelete)
			delete prlCca;
	}
}



LONG AllocationChangeDlg::OnGridMessageCellClick(WPARAM wParam,LPARAM lParam)
{
//	if (m_CheckEmpty.GetCheck())
//		return 1;

	CELLPOS *prlPos = (CELLPOS *)lParam;
	CString olValue;
	if (prlPos)
	{
		if (prlPos->Row > 0 && prlPos->Col > 3)
		{
			// retrieve CCATAB records pointer at first
			char* ptr = (char *)atol(pomCheckinCounter->GetValueRowCol(prlPos->Row,6));

			CGXStyle olStyle;
			
			olStyle.SetEnabled(TRUE);
			olStyle.SetReadOnly(TRUE);
			
			pomCheckinCounter->GetParam()->SetLockReadOnly(FALSE);
//			pomConflictSetup->LockUpdate(FALSE);
			switch(prlPos->Col)
			{
			case 5 :	// "valid only" flag
				if (pomCheckinCounter->GetValueRowCol(prlPos->Row,prlPos->Col) == "1")
				{
					pomCheckinCounter->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),
															   olStyle.SetControl(GX_IDS_CTRL_CHECKBOX3D)
																	  .SetChoiceList("1\n0\n")
																	  .SetValue("0")
														);
				
				}
				else
				{
					pomCheckinCounter->SetStyleRange(CGXRange(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col),
															   olStyle.SetControl(GX_IDS_CTRL_CHECKBOX3D)
																	  .SetChoiceList("1\n0\n")
																	  .SetValue("1")
														);
				}

				UpdateChoiceList(pomCheckinCounter->GetValueRowCol(prlPos->Row,prlPos->Col), prlPos->Row);

				CString olSelected = pomCheckinCounter->GetValueRowCol(prlPos->Row,4);
				int ilBitmap = 0;

				bool blTmp = GetCcaBitmap(ilBitmap, prlPos->Row, olSelected);
				CString olValue;
				
				if (ilBitmap != 0)
					olValue.Format("#BMP(%d)", ilBitmap);

				pomCheckinCounter->SetStyleRange(CGXRange(prlPos->Row,3,prlPos->Row,3),olStyle.SetControl(GX_IDS_CTRL_STATIC)
																	  .SetValue(olValue));
				
				pomCheckinCounter->GetParam()->SetLockReadOnly(TRUE);
				pomCheckinCounter->RedrawRowCol(prlPos->Row,prlPos->Col-2,prlPos->Row,prlPos->Col-2);

			break;
			}

			pomCheckinCounter->GetParam()->SetLockReadOnly(TRUE);
			pomCheckinCounter->RedrawRowCol(prlPos->Row,prlPos->Col,prlPos->Row,prlPos->Col);
		}
	}

	return 1;
}

LONG AllocationChangeDlg::OnGridMessageEndEditing(WPARAM wParam,LPARAM lParam)
{
	CELLPOS *prlPos = (CELLPOS *)lParam;
	
	if (prlPos)
	{
		if (prlPos->Row > 0 && prlPos->Col == 4)	// "TO" field changed
		{
			pomCheckinCounter->GetParam()->SetLockReadOnly(FALSE);
			// retrieve CCATAB records pointer at first
//			CCADATA* prlCca = (CCADATA *)atol(pomCheckinCounter->GetValueRowCol(prlPos->Row,5));
			CString olSelected = pomCheckinCounter->GetValueRowCol(prlPos->Row,prlPos->Col);
			int ilBitmap = 0;

			bool blTmp = GetCcaBitmap(ilBitmap, prlPos->Row, olSelected);

			CString olValue;
			CGXStyle olStyle;
			
			olStyle.SetEnabled(FALSE);
			olStyle.SetReadOnly(TRUE);

			if (ilBitmap != 0)
				olValue.Format("#BMP(%d)", ilBitmap);

			pomCheckinCounter->SetStyleRange(CGXRange(prlPos->Row,3,prlPos->Row,3),olStyle.SetControl(GX_IDS_CTRL_STATIC)
																  .SetValue(olValue));
			

			pomCheckinCounter->GetParam()->SetLockReadOnly(TRUE);
			pomCheckinCounter->RedrawRowCol(prlPos->Row,prlPos->Col-1,prlPos->Row,prlPos->Col-1);
		}
	}

	return 1;
}


bool AllocationChangeDlg::GetCcaBitmap(int& ipBitmap, int& ipRow, CString& opSelected)
{
	ipBitmap = 0;

	if (ipRow < 1 || ipRow > omCcaData.omData.GetSize())
		return false;

	bool blDelete = false;
	CCADATA* prlCca;
	if (ipRow <= omDemandCcaData.omData.GetSize())
		prlCca = &omDemandCcaData.omData[ipRow-1];
	else
	{
		prlCca = new CCADATA;
		blDelete = true;
	}
	
	if (prlCca)
	{
		CMapStringToString ropMapBLUE;
		CMapStringToString ropMapRED;
		CStringArray olTmp;
		if (ogDataSet.GetCcaRes(prlCca, ropMapBLUE, ropMapRED, olTmp))
		{
			CString olTmp;
			if (ropMapBLUE.Lookup(opSelected, olTmp))
			{
				if (olTmp == "IDB_LIGHTBLUE")
					ipBitmap = 214;
				else
					ipBitmap = 219;
			}
			else if (ropMapRED.Lookup(opSelected, olTmp))
			{
				if (olTmp == "IDB_RED")
					ipBitmap = 212;
				else
					ipBitmap = 220;
			}
		}

		if (blDelete)
			delete prlCca;
	}
	else
		return false;

	return true;
}

void AllocationChangeDlg::CheckReasonForChange(const CString& ropType)
{
	// Check the difference in position selection
	CTime olbesttime; 
	if (this->pomFlight->Adid[0] == 'A')
	{
		if(pomFlight->Tifa == NULL)
		{
			olbesttime = pomFlight->Stoa;
		}
		else
		{
			olbesttime = pomFlight->Tifa;
		}
	}
	else if (this->pomFlight->Adid[0] == 'D')
	{			
		if(pomFlight->Tifd == NULL)
		{
			olbesttime = pomFlight->Stod;
		}
		else
		{
			olbesttime = pomFlight->Tifd;
		}
	}

	CTime olCurrentTime ;
	
	if(bgGatPosLocal)			
		olCurrentTime = CTime::GetCurrentTime();						
	else
		GetCurrentUtcTime(olCurrentTime);

	CTime olDiffTime = olbesttime - ogXminutesBufferTime;

	if(olDiffTime < olCurrentTime)
	{
		if(ropType.Compare("PST") == 0)
		{
			m_PosReason.SetColors(RGB(255,0,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			omReasons.blPstResf = true;
		}
		else if(ropType.Compare("GAT") == 0)
		{
			m_GatReason.SetColors(RGB(255,0,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			omReasons.blGatResf = true;
		}
		else if(ropType.Compare("BLT") == 0)
		{
			m_BltReason.SetColors(RGB(255,0,0), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
			omReasons.blBltResf = true;
		}

	}
}


void AllocationChangeDlg::SetSecSate()
{


		if(ogPrivList.GetStat("RESCHAIN_CB_Keep") == '0' )
			m_CheckEmpty.EnableWindow(FALSE);

		if(ogPrivList.GetStat("RESCHAIN_CB_Keep") == '-')
			m_CheckEmpty.ShowWindow(SW_HIDE);


		// Position section
		if(ogPrivList.GetStat("RESCHAIN_CB_Pos") == '0' )
		{
			m_ToPosition.EnableWindow(FALSE);
			m_FromPosition.EnableWindow(FALSE);
			m_CheckPosition.EnableWindow(FALSE);
		}
		if(ogPrivList.GetStat("RESCHAIN_CB_Pos") == '-' )
		{
			m_ToPosition.ShowWindow(SW_HIDE);
			m_FromPosition.ShowWindow(SW_HIDE);
			m_CheckPosition.ShowWindow(SW_HIDE);
		}


		// Gate section
		if(ogPrivList.GetStat("RESCHAIN_CB_Gat") == '0' )
		{
			m_ToGate.EnableWindow(FALSE);
			m_FromGate.EnableWindow(FALSE);
			m_CheckGate.EnableWindow(FALSE);
		}
		if(ogPrivList.GetStat("RESCHAIN_CB_Gat") == '-' )
		{
			m_ToGate.ShowWindow(SW_HIDE);
			m_FromGate.ShowWindow(SW_HIDE);
			m_CheckGate.ShowWindow(SW_HIDE);
		}


		// Belt section
		if(ogPrivList.GetStat("RESCHAIN_CB_Blt") == '0' )
		{
			m_ToBaggageBelt.EnableWindow(FALSE);
			m_FromBaggageBelt.EnableWindow(FALSE);
			m_CheckBaggageBelt.EnableWindow(FALSE);
		}
		if(ogPrivList.GetStat("RESCHAIN_CB_Blt") == '-' )
		{
			m_ToBaggageBelt.ShowWindow(SW_HIDE);
			m_FromBaggageBelt.ShowWindow(SW_HIDE);
			m_CheckBaggageBelt.ShowWindow(SW_HIDE);
		}



		// WRO section
		if(ogPrivList.GetStat("RESCHAIN_CB_Wro") == '0' )
		{
			m_ToLounge.EnableWindow(FALSE);
			m_FromLounge.EnableWindow(FALSE);
			m_CheckLounge.EnableWindow(FALSE);
		}
		if(ogPrivList.GetStat("RESCHAIN_CB_Wro") == '-' )
		{
			m_ToLounge.ShowWindow(SW_HIDE);
			m_FromLounge.ShowWindow(SW_HIDE);
			m_CheckLounge.ShowWindow(SW_HIDE);
		}

		// CKI



		if(ogPrivList.GetStat("RESCHAIN_CB_Cic") == '0' )
		{
			pomCheckinCounter->EnableWindow(FALSE);

		}
		if(ogPrivList.GetStat("RESCHAIN_CB_Cic") == '-' )
		{
			pomCheckinCounter->ShowWindow(SW_HIDE);

		}


		if(ogPrivList.GetStat("RESCHAIN_CB_Recalc") == '0' )
			m_Recalc.EnableWindow(FALSE);
		if(ogPrivList.GetStat("RESCHAIN_CB_Recalc") == '-' )
			m_Recalc.ShowWindow(SW_HIDE);


}




void AllocationChangeDlg::OnBltreason() 
{
	// TODO: Add your control notification handler code here
		// Get all the reasons related to position //PRF 8379 
	if((omReasons.blBltResf == true) || (!omReasons.olBltUrno.IsEmpty()))
	{
		omReasons.olBltUrno = CFPMSApp::GetSelectedReason(this,"BELT",omReasons.olBltUrno);
		if(omReasons.olBltUrno.GetLength()>0)
		{
			omReasons.blBltResf = false;
			m_BltReason.SetColors(::GetSysColor(COLOR_BTNFACE), ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
		}
	}
}
