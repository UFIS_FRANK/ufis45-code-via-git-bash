#ifndef __ALLOCATE_CCA__
#define __ALLOCATE_CCA__

#include <CCSGlobl.h>
#include <CedaBasicData.h>
#include <CcaCedaFlightData.h>

/*
struct FLIGHTS
{
	CCSPtrArray<CCAFLIGHTDATA> *Flights;
};
*/

struct CIC_ALLOCTIMES
{
	CTime From;
	CTime To;
};


struct CIC_ALLOCATEINFO
{
	long Urno;
	CString Cnam;      // Name of Counter
	CString Term;	   // Name of Termina or Hall
	CString SortText;  // Combination of Hall/Term and Countername
	CString Cicr;	   // Island or Group
	CCSPtrArray<CIC_ALLOCTIMES> AllocTimes;
};

struct CIC_INFO
{
	long Urno;			//Urno CIC
	int  CounterNo;		//Counter number
	long	GrNo;
	long	CicIndex;

	CIC_INFO(void)
	{
		Urno = 0;
		GrNo = 0;
		CounterNo = 0;
		CicIndex = -1;
	}
};


struct FLT_CCA_FLIGHT
{
	long Urno;				//Flighturno or CCA Urno if Common-Checkin
	CString Type;			// "F" for Flight or "C" for Common-Checkin Counter
	CString Flno;			//For Debuggin, to see the Flight
	CTime MinDemandTime;	//Begin of the first CCA-Demand
	CTime MaxDemandTime;	//End of the last CCA-Demand
	int	  NeedLines;		//Amount of Demands
	bool  IsAllocated;
	int   StartLine;		// If Demands are allocated, it contains the line of the first cca
	int   EndLine;			// If Demands are allocated, it contains the line of the last cca

	CRect AllocRect;		// If is allocated, it contains the following below
							// Left   = MinDemandTime.GetTotalMinutes
							// Top    = StartLine
							// Right  = MaxDemandTime.GetTotalMinutes
							// Bottom = EndLine
	CCSPtrArray<long> DemandsUrnos; //All CCA-Demands for this Flight
	CCSPtrArray<CIC_INFO> CicInfo;
	int OrdDigit;			//Ordnungszahl f�r Sortierung der Bedarfe:
							//wenn Anzahl der Bedarfe und Schaltergruppen sehr eng dann zuerst
							//die restriktivsten
	FLT_CCA_FLIGHT(void)
	{
		IsAllocated = false;
		Flno = CString("");
		NeedLines = 0;
		Urno = 0;
		MaxDemandTime = TIMENULL;
		MinDemandTime = TIMENULL;
	}
};

class AllocateCca
{
public:
	AllocateCca(int ipMode, CTime opFrom, CTime opTo);
	~AllocateCca();

	int GetFirstValidLine( FLT_CCA_FLIGHT *prlCcaFlight, int ipStartLine );

	int imAlloRun;

	CTime omFrom;
	CTime omTo;

	int imMode; // 0 ==> alle einteilen 1 ==> Nut offene
	CCSPtrArray<CIC_ALLOCATEINFO> omCicData;
	CCSPtrArray<FLT_CCA_FLIGHT>   omFlightDem;
//	CMapPtrToPtr omFlightDemMap;
//	CMapPtrToPtr omCicDataMap;
	void Allocate(CTime opFrom = TIMENULL, CTime opTo = TIMENULL);
	void AllocateWithVacancy(CTime opFrom = TIMENULL, CTime opTo = TIMENULL);
	void InitAllocationData();
	void MakeCicList();
	void MakeCicEntry(CIC_ALLOCATEINFO *prpAllocInf, RecordSet &ropRec);
	void ClearAll();
	void GetFlightsAndDemands();
	void MakeGroups(FLT_CCA_FLIGHT *prpCcaFlight);
	bool GetAndCalcDemands( CCAFLIGHTDATA *prpFlight);//Flight-Urno
	bool GetAndCalcCommonDemands(FLT_CCA_FLIGHT *prpCcaFlight, long lpUrno);//Cca-Urno
	bool AreFollowingLinesValid(FLT_CCA_FLIGHT *prpCcaFlight, int ipCkiIndex, int ilNeededLines);
	bool AreFollowingLinesValid(FLT_CCA_FLIGHT *prpCcaFlight, int ipCkiIndex, int ilNeededLines, int& ipAllocLines);
	bool IsRectOverlapped(FLT_CCA_FLIGHT *prpCcaFlight);

    CMapPtrToPtr omUrnoMap;


	void ResetAll();

};

#endif //__ALLOCATE_CCA__