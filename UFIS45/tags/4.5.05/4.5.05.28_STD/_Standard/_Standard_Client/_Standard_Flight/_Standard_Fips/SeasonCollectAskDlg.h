#ifndef AFX_SEASONCOLLECTASKDLG_H__CF2D7561_AB83_11D1_A3D2_0000B45A33F5__INCLUDED_
#define AFX_SEASONCOLLECTASKDLG_H__CF2D7561_AB83_11D1_A3D2_0000B45A33F5__INCLUDED_

// SeasonCollectAskDlg.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld SeasonCollectAskDlg 

#include <CCSEdit.h>


class SeasonCollectAskDlg : public CDialog
{
// Konstruktion
public:
	SeasonCollectAskDlg(CWnd* pParent = NULL);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(SeasonCollectAskDlg)
	enum { IDD = IDD_SEASONCOLLECTASK };
	CButton	m_CR_OrgOrDes;
	CButton	m_CR_OrgOrDes2;
	CCSEdit	m_CE_Flns;
	CCSEdit	m_CE_Fltn;
	CCSEdit	m_CE_From;
	CCSEdit	m_CE_To;
	CCSEdit	m_CE_Seas;
	CButton	m_CB_AlcList;
	CButton	m_CB_SeaList;
	CButton	m_CB_FlnoList;
	CCSEdit	m_CE_Alc;
	CCSEdit	m_CE_Tfrom;
	CCSEdit	m_CE_Tto;
	CString	m_Alc;
	CString	m_Seas;
	CString	m_To;
	CString	m_From;
	CString	m_Fltn;
	CString	m_Flns;
	CString	m_Tfrom;
	CString	m_Tto;
	//}}AFX_DATA

	CString omAdid;
	CString omAlc3;

// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(SeasonCollectAskDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(SeasonCollectAskDlg)
	virtual void OnOK();
	afx_msg void OnAlclist();
	virtual BOOL OnInitDialog();
	afx_msg LONG OnEditKillfocus( UINT wParam, LPARAM lParam);
	afx_msg void OnSealist();
	afx_msg void OnFlnolist();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_SEASONCOLLECTASKDLG_H__CF2D7561_AB83_11D1_A3D2_0000B45A33F5__INCLUDED_
