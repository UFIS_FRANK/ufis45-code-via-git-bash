#ifndef AFX_AIRPORTINFORMATIONDLG_H__B5887372_A6B0_11D1_BF9D_004095434A85__INCLUDED_
#define AFX_AIRPORTINFORMATIONDLG_H__B5887372_A6B0_11D1_BF9D_004095434A85__INCLUDED_

// AirportInformationDlg.h : Header-Datei
//

#include <resource.h>
#include <CCSButtonCtrl.h>
#include <CedaInfData.h>
#include <CCSEdit.h>
#include <DlgResizeHelper.h>

enum
{
	NO_INFO_STATUS,SET_INFO_STATUS,INSERT_INFO,CHANGE_INFO,CREATE_INFO,NEW_INFO
};

struct LISTBOXDATA
{
	Apc3Info *pomApc3;
	CString Apc3Name;

	ApplInfo *pomAppl;
	CString ApplName;

	FunkInfo *pomFunk;
	CString FunkName;

	AreaInfo *pomArea;
	CString AreaName;

	INFDATA *pomInfo;
	long InfoUrno;
};

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld AirportInformationDlg 

class AirportInformationDlg : public CDialog
{
// Konstruktion
public:
	AirportInformationDlg(CWnd* pParent = NULL);   // Standardkonstruktor
	~AirportInformationDlg(void);
// Dialogfelddaten
	//{{AFX_DATA(AirportInformationDlg)
	enum { IDD = IDD_AIRPORTINFORMATION_DLG };
	CListBox	m_FunkList;
	CListBox	m_DateList;
	CListBox	m_AreaList;
	CListBox	m_ApplList;
	CListBox	m_Apc3List;
	CCSEdit	m_Apc3Edit;
	CCSEdit	m_ApplEdit;
	CCSEdit	m_AreaEdit;
	CCSEdit	m_FunkEdit;
	CEdit   m_TextField;
	CCSButtonCtrl	m_NewInfoButton;
	CButton	m_ResetButton;
	CButton	m_PrintButton;
	CButton	m_SendButton;
	CButton	m_InsertButton;
	CButton	m_ChangeButton;
	CButton	m_CreateButton;
	CButton	m_DeletePathButton;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(AirportInformationDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(AirportInformationDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnChange();
	afx_msg void OnInsert();
	afx_msg void OnPrint();
	afx_msg void OnSend();
	afx_msg void OnSelchangeAreaList();
	afx_msg void OnSelchangeApplList();
	afx_msg void OnSelchangeApc3List();
	afx_msg void OnSelchangeFunkList();
	afx_msg void OnSelchangeDateList();
	afx_msg void OnCreate();
	afx_msg void OnReset();
	afx_msg void OnNewinfo();
	afx_msg void OnPaint();
	afx_msg void OnDeletePath();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	DlgResizeHelper m_resizeHelper;
	CString m_key;

public:
//Funktionen

	void FillList(UINT ipListBoxID,bool bpSetLBD = true);
	void SaveToReg();

//Variablen

	CRect omTextFieldRecUp;
	CRect omTextFieldRecDown;
	CString omErrorTxt;
	int imInfoStatus;
	int imNewInfoNo;
	int imTotalNewInfoNo;
	LISTBOXDATA omListBoxData;

	CString omReadErrTxt;
	CString omInsertErrTxt;
	CString omUpdateErrTxt;
	CString omNoDataErrTxt;
	CString omBadFormatErrTxt;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_AIRPORTINFORMATIONDLG_H__B5887372_A6B0_11D1_BF9D_004095434A85__INCLUDED_
