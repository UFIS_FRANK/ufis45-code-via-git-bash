#if !defined(AFX_MODEOFPAY_H__44D117FF_896F_4192_951C_B913B65D2F9B__INCLUDED_)
#define AFX_MODEOFPAY_H__44D117FF_896F_4192_951C_B913B65D2F9B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ModeOfPay.h : header file
//

#include <Resource.h>
#include <resrc1.h>
#include <CCSGlobl.h>
#include <FPMS.h>
#include <CedaCfgData.h>
#include <CCSEdit.h>
#include <CCSButtonCtrl.h>
#include <RotationDlgCedaFlightData.h>

#include <CedaMOPData.h>
/////////////////////////////////////////////////////////////////////////////
// CModeOfPay dialog

class CModeOfPay : public CDialog
{
// Construction
public:
	CModeOfPay(CWnd* pParent = NULL,long olFlightUrno=0,CString strFlightType="");   // standard constructor
	//ModeOfPay(CWnd* pParent = NULL);   // standard constructor
	
	CString LoadAFTRelatedData();
	void LoadMOPData(CString olRreq);
	void LoadCollectedBy(CString strToSelect);
	void LoadHandlingAgents(CString strToSelect);
	void LoadRequiredControls(long olUrno,CString strADID);
	void LoadExistingControls(long olUrno,CString strADID,CString olRreq);
	BOOL HaveRecordForCFG(long olUrno,CString strADID);
	void SetFlightNo(long lFlightUrno);
	void SetFlightType(CString FlightType);
	void DrawControls(CString itemText, DWORD dwStyle1,CRect rect,int controlID,BOOL CHECKED,BOOL ENABLED);
	void SetInitialScreen(BOOL isNew);
	void LoadConfigForResources(CString strADID);

	int GetConfigCount();
	
	char* GetFieldList();
	char* GetDataList();



	void SetAllSection(bool bEnable);
	void SetPaymentSection(bool bEnable) ;
	void SetResourceSection(bool bEnable);

	void SetMode(CString ScreenMode);
	void Save();

	long olFlightUrno;
	CString strFlightType;
	CString strMode;

	char m_pclFieldList[2048];// "COBY,HDAG,BLTO,PRMK,RREQ,RRMK,MOPA";
	char m_pclData[3072];

	CString		m_Caption;



// Dialog Data
	//{{AFX_DATA(CModeOfPay)
	enum { IDD = IDD_MODE_OF_PAY };
	CCSEdit	m_strBillTo;
	CStatic	m_PayHeader;
	CEdit	m_strPRMK;
	CEdit	m_strRRMK;
	CButton	m_rdCash;
	CButton	m_rdBank;
	CButton	m_rdRequired;
	CButton	m_rdNotRequired;
	CComboBox	m_cbCollectedBy;
	CComboBox	m_cbHandlingAgents;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CModeOfPay)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CModeOfPay)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnRequired();
	afx_msg void OnNotRequired();
	afx_msg void OnBank();
	afx_msg void OnCash();
	afx_msg void OnSelchangeCollectedBy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MODEOFPAY_H__44D117FF_896F_4192_951C_B913B65D2F9B__INCLUDED_)
