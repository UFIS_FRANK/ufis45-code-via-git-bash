// CedaInfData.h

#ifndef __CEDAINFDATA__
#define __CEDAINFDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------

struct INFDATA 
{
	char 	 Apc3[5];
	char 	 Appl[34];		// Applikation
	char 	 Area[34];		// Arbeitsbereich
	CTime 	 Cdat;			// Erstellungsdatum
	char 	 Funk[34];		// Funktionsbereich
	CTime 	 Lstu;			// Datum letzte �nderung
	long 	 Urno;			// Eindeutige Datensatz-Nr.
	char 	 Usec[34];		// Anwender (Ersteller)
	char 	 Useu[34]; 		// Anwender (letzte �nderung)
	char 	 Text[2002]; 	// Infotext

	//DataCreated by this class
	int		 IsChanged;	// Check whether Data has Changed f�r Relaese

	INFDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged	=	DATA_UNCHANGED;
		Cdat		=	TIMENULL;
		Lstu		=	TIMENULL;
	}

};

struct NEWINFDATA
{
	long Urno;
};
//---------------------------------------------------------------------------------------------------------

class AreaInfo
{
public:

	AreaInfo();
	~AreaInfo();
	void AddInf(INFDATA *prpInf);
	INFDATA *FindInf(long Urno);
	
	CString omArea;		// Arbeitsbereich

    CMapPtrToPtr omUrnoMap;
};

class FunkInfo
{
public:

	FunkInfo();
	~FunkInfo();
	void AddInf(INFDATA *prpInf);
	AreaInfo *FindInf(CString opArea);
	
	CString omFunk;		// Funktionsbereich

	CMapStringToPtr omAreaMap;
};

class ApplInfo
{
public:

	ApplInfo();
	~ApplInfo();
	void AddInf(INFDATA *prpInf);
	FunkInfo *FindInf(CString opFunk);
	
	CString omAppl;		// Applikation

	CMapStringToPtr omFunkMap;
};

class Apc3Info
{
public:

	Apc3Info();
	~Apc3Info();
	void AddInf(INFDATA *prpInf);
	ApplInfo *FindInf(CString opAppl);

	CString omApc3;		// Airport 3-Letter Code

	CMapStringToPtr omApplMap;
};

//---------------------------------------------------------------------------------------------------------
// Class declaration

class CedaInfData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr	omUrnoMap;
	CMapStringToPtr omApc3Map;

    CCSPtrArray<INFDATA> omData;
    CCSPtrArray<NEWINFDATA> omNewInfoData;

	char pcmListOfFields[2048];
	int imTextLength;
// Operations
public:
    CedaInfData();
	~CedaInfData();

	
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspFieldList = NULL,char *pspWhere = NULL);
	bool Insert(INFDATA *prpInf);
	bool InsertInternal(INFDATA *prpInf);
	bool Update(INFDATA *prpInf);
	bool UpdateInternal(INFDATA *prpInf);
	bool Delete(long lpUrno);
	bool DeleteInternal(INFDATA *prpInf);
	bool ReadSpecialData(CCSPtrArray<INFDATA> *popInf,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(INFDATA *prpInf);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	INFDATA  *GetInfByUrno(long lpUrno);
	LPCTSTR TranslateText(INFDATA *prpInfRec, CString &ropText, bool blFromDb);

	void AddInf(INFDATA *prpInf);
	Apc3Info *FindInf(CString opApc3);
	bool Delete(char *pspWhere = NULL);

	// Private methods
private:
    void PrepareInfData(INFDATA *prpInfData);

};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDAINFDATA__
