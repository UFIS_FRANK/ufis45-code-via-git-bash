// WroDiagram.cpp : implementation file
// Modification History:
// 19-feb-01	rkr	PRF 1305 Dubai: Gantchart-Anzeige unter local unterstützt
//

#include <stdafx.h>
#include <WoResTableDlg.h>
#include <WroDiagram.h>
#include <WroDiaPropertySheet.h>
#include <DiaCedaFlightData.h>
#include <WroChart.h>
#include <WroGantt.h>
#include <TimePacket.h>
#include <RotationISFDlg.h>
#include <PrivList.h>
#include <FlightSearchTableDlg.h>
#include <Utils.h>
#include <PosDiagram.h>
#include <CCAExpandDlg.h>
#include <SpotAllocation.h>
#include <Utils.h>
#include <BltOverviewTableDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static void WroDiagramCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// WroDiagram
extern int igAnsichtPageIndex;

IMPLEMENT_DYNCREATE(WroDiagram, CFrameWnd)

WroDiagram::WroDiagram()
{
	igAnsichtPageIndex = -1;
	omMaxTrackSize = CPoint(4000/*1024*/, 1500);
	omMinTrackSize = CPoint(1024 / 4, 768 / 4);
	omViewer.SetViewerKey("POSDIA");
	omViewer.SelectView("<Default>");
    imStartTimeScalePos = 104;
    imStartTimeScalePos++;              // plus one for left border of chart
    imFirstVisibleChart = -1;
	lmBkColor = lgBkColor;
	pogWroDiagram = this;
	bmIsViewOpen = false;
	bgKlebefunktion = false;
	bmRepaintAll = false;
	pomBltOverviewTableDlg = NULL;
	m_key = "DialogPosition\\LoungeDiagram";

    lmBkColor = COLORREF(SILVER);;
    lmTextColor = COLORREF(BLACK);;
    lmHilightColor = COLORREF(BLACK);;


}

WroDiagram::~WroDiagram()
{
	pogWroDiagram = NULL;
}


BEGIN_MESSAGE_MAP(WroDiagram, CFrameWnd)
	//{{AFX_MSG_MAP(WroDiagram)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_WORES_WRO, OnWoRes)
	ON_BN_CLICKED(IDC_BEENDEN, OnBeenden)
	ON_BN_CLICKED(IDC_ANSICHT, OnAnsicht)
	ON_BN_CLICKED(IDC_INSERT, OnInsert)
    ON_WM_GETMINMAXINFO()
    ON_WM_PAINT()
    ON_WM_ERASEBKGND()
    ON_WM_SIZE()
    ON_WM_HSCROLL()
    ON_WM_TIMER()
    ON_MESSAGE(WM_POSITIONCHILD, OnPositionChild)
    ON_MESSAGE(WM_UPDATEDIAGRAM, OnUpdateDiagram)
    ON_CBN_SELCHANGE(IDC_VIEW, OnViewSelChange)
	ON_CBN_CLOSEUP(IDC_VIEW, OnCloseupView)
	ON_BN_CLICKED(IDC_ZEIT, OnZeit)
	ON_BN_CLICKED(IDC_SEARCH, OnSearch)
	ON_BN_CLICKED(IDC_OFFLINE, OnOffline)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_WM_CLOSE()
	ON_MESSAGE(WM_REPAINT_ALL, RepaintAll)
	ON_WM_ACTIVATE()
	ON_BN_CLICKED(IDC_EXPAND, OnExpand )
	ON_BN_CLICKED(IDC_PRINT, OnPrint)
	ON_BN_CLICKED(IDC_CHANGES, OnChanges)
	ON_BN_CLICKED(IDC_ALLOCATE, OnAllocate)
	ON_BN_CLICKED(IDC_WROOVERVIEW, OnOverview)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


static void WroDiagramCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
	WroDiagram *polDiagram = (WroDiagram *)popInstance;

	TIMEPACKET *polTimePacket;

	switch (ipDDXType)
	{
	case REDISPLAY_ALL:
		polDiagram->RedisplayAll();
		break;
	case STAFFDIAGRAM_UPDATETIMEBAND:
		polTimePacket = (TIMEPACKET *)vpDataPointer;
		polDiagram->SetTimeBand(polTimePacket->StartTime, polTimePacket->EndTime);
		polDiagram->UpdateTimeBand();
		break;
	case SAS_VIEW_CHANGED:
        polDiagram->ViewSelChange( (char *) vpDataPointer);
		break;
	case SAS_GET_CUR_VIEW:
		polDiagram->GetCurView((char *) vpDataPointer);
		break;
	case D_FLIGHT_CHANGE_WORES:
		polDiagram->UpdateWoResButton();
		break;
	}
}

void WroDiagram::GetCurView( char * pcpView)
{
	strcpy(pcpView, omViewer.GetViewName());
}


void WroDiagram::ViewSelChange(char *pcpView)
{

	omViewer.SelectView(pcpView);

	
	CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
	polCB->SetCurSel(polCB->FindString( 0, pcpView ) );


	UpdateDia();
	SetFocusToDiagram(); // PRF 8363

	/*
	if(IsIconic())  //RST
	{
		return;
	}
		
	
	omViewer.MakeMasstab();
	CTime olT = omViewer.GetGeometrieStartTime();
	CString olS = olT.Format("%d.%m.%Y-%H:%M");
	SetTSStartTime(olT);
	omTSDuration = omViewer.GetGeometryTimeSpan();

    
	long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
	SetScrollPos(SB_HORZ, int(nPos), TRUE);
	
	
	
//rkr
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);
    
	omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
//	omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
    omTimeScale.Invalidate(TRUE);
	SetTSStartTime(omTSStartTime);
	ChangeViewTo(clText, false);
	*/
}



/////////////////////////////////////////////////////////////////////////////
// WroDiagram message handlers
void WroDiagram::PositionChild()
{
    CRect olRect;
    CRect olChartRect;
    
    int ilLastY = 0;
    omClientWnd.GetClientRect(&olRect);
         
    pomChart->GetClientRect(&olChartRect);
    olChartRect.right = olRect.right;

    olChartRect.top = ilLastY;

    ilLastY += pomChart->GetHeight();
    olChartRect.bottom = ilLastY;
        
    // check
    if ((pomChart->GetState() != Minimized) &&
		(olChartRect.top < olRect.bottom) && (olChartRect.bottom > olRect.bottom))
    {
        olChartRect.bottom = olRect.bottom;
        pomChart->SetState(Normal);
    }
    //
        
    pomChart->MoveWindow(&olChartRect, FALSE);
	pomChart->ShowWindow(SW_SHOW);

    
	omClientWnd.Invalidate(TRUE);
	//SetAllStaffAreaButtonsColor();
 	UpdateTimeBand();
}


void WroDiagram::SetTSStartTime(CTime opTSStartTime)
{
    omTSStartTime = opTSStartTime;
	/*
    if ((omTSStartTime < omStartTime) ||
		(omTSStartTime > omStartTime + omDuration))
    {
        omStartTime = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
            0, 0, 0) - CTimeSpan(1, 0, 0, 0);
    }
      */
	

	CTime olTSStartTime(omTSStartTime);
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);


    char clBuf[8];
    sprintf(clBuf, "%02d%02d%02d",
        olTSStartTime.GetDay(), olTSStartTime.GetMonth(), olTSStartTime.GetYear() % 100);
    omTSDate.SetWindowText(clBuf);
    omTSDate.Invalidate(FALSE);
}

void WroDiagram::ActivateTimer()
{
	SetTimer(1, (UINT) 60 * 1000 * ogPosDiaFlightData.cimBartimesUpdateMinute, NULL);  // Update bar times
}

void WroDiagram::DeActivateTimer()
{
	KillTimer(1);
}

int WroDiagram::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

    SetTimer(0, (UINT) 60 * 1000, NULL);
	ActivateTimer();
//    SetTimer(1, (UINT) 60 * 1000 * ogPosDiaFlightData.cimBartimesUpdateMinute, NULL);  // Update bar times

	omDialogBar.Create( this, IDD_WRODIAGRAM, CBRS_TOP, IDD_WRODIAGRAM );
	SetWindowPos(&wndTop/*Most*/, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);

    omViewer.Attach(this);
	UpdateComboBox();


    CRect olRect; GetClientRect(&olRect);
	CTime olCurrentTime;
	olCurrentTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olCurrentTime);

	// Create 'Offline'-Button
	CButton *polButton = (CButton *)omDialogBar.GetDlgItem(IDC_OFFLINE2);	
 	WINDOWPLACEMENT olWndPlace;
	polButton->GetWindowPlacement( &olWndPlace ); 
	m_CB_Offline.Create(GetString(IDS_STRING1258), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_OFFLINE);
    m_CB_Offline.SetFont(polButton->GetFont());
	m_CB_Offline.EnableWindow(true);
	//m_CB_Offline.ShowWindow(SW_HIDE);

	#ifdef _FIPSVIEWER
	{
		m_CB_Offline.EnableWindow(false);
	}
	#else
	{
	}
	#endif

	// Create 'Wores'-Button
	polButton = (CButton *)omDialogBar.GetDlgItem(IDC_WORES_WRO2);	
	polButton->GetWindowPlacement( &olWndPlace ); 
	CString olTmp;
	olTmp.Format("%s (%d)", GetString(IDS_STRING1914),0);
	m_CB_WoRes.Create(olTmp, WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_WORES_WRO);
    m_CB_WoRes.SetFont(polButton->GetFont());
	m_CB_WoRes.EnableWindow(true);
	m_CB_WoRes.SetSecState(ogPrivList.GetStat("WRODIAGRAMM_CB_WORes"));	

	// Create 'Changes'-Button
	polButton = (CButton *)omDialogBar.GetDlgItem(IDC_CHANGES);	
	polButton->GetWindowPlacement( &olWndPlace ); 
	m_CB_Changes.Create(GetString(IDS_CHANGES), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW, olWndPlace.rcNormalPosition, &omDialogBar, IDC_CHANGES);
    m_CB_Changes.SetFont(polButton->GetFont());
	m_CB_Changes.EnableWindow(true);
	m_CB_Changes.SetSecState(ogPrivList.GetStat("DESKTOP_CB_History"));


//	m_KlebeFkt.Create( "&Klebefunktion", SS_CENTER | WS_CHILD | WS_VISIBLE, 
//						CRect(olRect.left + 400, 6,olRect.left + 480, 23), this, IDC_KLEBEFKT);

    omTSStartTime = olCurrentTime;// - CTimeSpan(0, 1, 0, 0);
    omStartTime = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
        0, 0, 0);// - CTimeSpan(1, 0, 0, 0);

    omDuration = CTimeSpan(3, 0, 0, 0);
    omTSDuration = CTimeSpan(0, 6, 0, 0);
    omTSInterval = CTimeSpan(0, 0, 10, 0);

	//Die wollen local
    CTime olUCT = CTime::GetCurrentTime();//olCurrentTime;
    CTime olRealUTC = olCurrentTime;
    char olBuf[16];
    sprintf(olBuf, "%02d%02d/%02d%02dz",
        olUCT.GetHour(), olUCT.GetMinute(),        
        olRealUTC.GetHour(), olRealUTC.GetMinute()
    );

    
	
	CTime olLocalTime = CTime::GetCurrentTime();
	CTime olUtcTime = CTime::GetCurrentTime();
	ogBasicData.LocalToUtc(olUtcTime);

	char pclTimes[36];
	char pclDate[36];
	
	CTime olUtcTimeTmp = CTime::GetCurrentTime();
	olUtcTimeTmp -= ogUtcDiff;
	
	if(ogUtcDiff != 0)
	{
		sprintf(pclDate, "%02d.%02d.%02d z", olUtcTimeTmp.GetDay(), olUtcTimeTmp.GetMonth(), olUtcTimeTmp.GetYear() % 100);
		sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTimeTmp.GetHour(), olUtcTimeTmp.GetMinute());
	}
	else
	{
		sprintf(pclDate, "%02d.%02d.%02d z", olUtcTime.GetDay(), olUtcTime.GetMonth(), olUtcTime.GetYear() % 100);
		sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTime.GetHour(), olUtcTime.GetMinute());
	}
    
	
	omTime.Create(pclTimes, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 192, 3, olRect.right - 90, 22), this);

    omDate.Create(pclDate, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 88, 3, olRect.right - 8, 22), this);
        
    omDate.SetTextColor(RGB(255,0,0));
	
	omTime.ShowWindow(SW_HIDE);
	omDate.ShowWindow(SW_HIDE);
	

    sprintf(olBuf, "%02d%02d%02d",  omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 100);
    int ilPos = (imStartTimeScalePos - olRect.left - 80) / 2;
    omTSDate.Create(olBuf, SS_CENTER | WS_CHILD | WS_VISIBLE,
        CRect(ilPos, 35, ilPos + 80, 52), this);

    // CBitmapButton
    omBB1.Create("PREV", BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 36, 21 + 5, olRect.right - 19, (21 + 5) + 25), this, IDC_PREV);
    omBB1.LoadBitmaps("PREVU", "PREVD", NULL, "NEXTX");

    omBB2.Create("NEXT", BS_OWNERDRAW | WS_CHILD | WS_VISIBLE,
        CRect(olRect.right - 19, 21 + 5, olRect.right - 2, (21 + 5) + 25), this, IDC_NEXT);
    omBB2.LoadBitmaps("NEXTU", "NEXTD", NULL, "NEXTX");

	omBB1.ShowWindow(SW_HIDE);
	omBB2.ShowWindow(SW_HIDE);

	omTimeScale.lmBkColor = lgBkColor;
    omTimeScale.Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE,
        CRect(imStartTimeScalePos, 28, olRect.right - (36 + 2), 62), this, 0, NULL);
//rkr
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);
    
	omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);
//	omTimeScale.SetDisplayTimeFrame(omTSStartTime, omTSDuration, omTSInterval);
	if(bgGatPosLocal)
		omTimeScale.UpdateCurrentTimeLine(olLocalTime);
	else
		omTimeScale.UpdateCurrentTimeLine(olRealUTC);
     
    // must be static
    static UINT ilIndicators[] = { ID_SEPARATOR };
    omStatusBar.Create(this, CBRS_BOTTOM | WS_CHILD | WS_VISIBLE,0);
    omStatusBar.SetIndicators(ilIndicators, sizeof(ilIndicators) / sizeof(UINT));

    UINT nID, nStyle; int cxWidth;
    omStatusBar.GetPaneInfo(0, nID, nStyle, cxWidth);
    omStatusBar.SetPaneInfo(0, nID, SBPS_STRETCH | SBPS_NORMAL, cxWidth);

    olRect.top += 64;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    omClientWnd.Create(NULL, "ClientWnd", WS_CHILD /*| WS_HSCROLL */ | WS_VISIBLE, olRect, this,
        0 /* IDD_CLIENTWND */, NULL);
        
	// This will fix the bug for the horizontal scroll bar in the preplan mode
    long llTSMin = (omTSStartTime - omStartTime).GetTotalMinutes();
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
    SetScrollRange(SB_HORZ, 0, 1000, FALSE);
    SetScrollPos(SB_HORZ, (int) (1000 * llTSMin / llTotalMin), FALSE);

    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);
    
    imFirstVisibleChart = 0;
    int ilLastY = 0;

	olRect.SetRect(olRect.left, ilLastY, olRect.right, ilLastY);
        
    pomChart = new WroChart;
    pomChart->SetTimeScale(&omTimeScale);
    pomChart->SetViewer(&omViewer);
    pomChart->SetStatusBar(&omStatusBar);
    pomChart->SetStartTime(omTSStartTime);
    pomChart->SetInterval(omTimeScale.GetDisplayDuration());
    pomChart->Create(NULL, "WroChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE,
        olRect, &omClientWnd,
        0 /* IDD_CHART */, NULL);
    
    OnTimer(0);

	// Register DDX call back function
	//TRACE("WroDiagram: DDX Registration\n");
	ogDdx.Register(this, REDISPLAY_ALL, CString("STAFFDIAGRAM"),
		CString("Redisplay all from What-If"), WroDiagramCf);	// for what-if changes
	ogDdx.Register(this, STAFFDIAGRAM_UPDATETIMEBAND, CString("STAFFDIAGRAM"),
		CString("Update Time Band"), WroDiagramCf);	// for updating the yellow lines

	 
  	ogDdx.Register(this, SAS_VIEW_CHANGED,CString(" "), CString("DATA_RELOAD"),WroDiagramCf);
 	ogDdx.Register(this, SAS_GET_CUR_VIEW,CString(" "), CString("DATA_RELOAD"),WroDiagramCf);
	ogDdx.Register(this, D_FLIGHT_CHANGE_WORES, CString("FLIGHTDIAVIEWER"), CString("Flight Changed"), WroDiagramCf);

	// Create OverviewTableDlg
	if (pomBltOverviewTableDlg == NULL && bgAllocationOverview)
	{
		pomBltOverviewTableDlg = new BltOverviewTableDlg(this);
		pomBltOverviewTableDlg->SetWindowPos(&wndTop,100,200,850,400, SWP_HIDEWINDOW);
		pomBltOverviewTableDlg->SetResource("WRO");
	}

	//overviewbutton	
	polButton = (CButton *)omDialogBar.GetDlgItem(IDC_WROOVERVIEW);	
	if (polButton && !bgAllocationOverview)
	{
		polButton->EnableWindow(FALSE);
		polButton->ShowWindow(SW_HIDE);
	}



	// Connect to the GatPosFlightData-Class
	ogPosDiaFlightData.Connect();
	// Connect to the FlightsWithoutResource - Table
	pogWoResTableDlg->Connect();
	
	CButton *polCB; 

	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_ALLOCATE);
	polCB->ShowWindow(SW_SHOW);
	SetpWndStatAll(ogPrivList.GetStat("WRODIAGRAMM_CB_Allocate"),polCB);
	#ifdef _FIPSVIEWER
	{
		SetpWndStatAll('0',polCB);
	}
	#else
	{
	}
	#endif

	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_SEARCH);
	SetpWndStatAll(ogPrivList.GetStat("WRODIAGRAMM_CB_Search"),polCB);

	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_ANSICHT);
	SetpWndStatAll(ogPrivList.GetStat("WRODIAGRAMM_CB_Ansicht"),polCB);

	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_EXPAND);
	SetpWndStatAll(ogPrivList.GetStat("WRODIAGRAMM_CB_Expand"),polCB);
	#ifdef _FIPSVIEWER
	{
		SetpWndStatAll('0',polCB);
	}
	#else
	{
	}
	#endif

	polCB = (CButton *) omDialogBar.GetDlgItem(IDC_PRINT);
	SetpWndStatAll(ogPrivList.GetStat("WRODIAGRAMM_CB_Print"),polCB);

	if (!bgOffRelFuncGatpos)
	{
		m_CB_Offline.ShowWindow(SW_HIDE);
	}


	olRect = CRect(0, 95, ::GetSystemMetrics(SM_CXSCREEN), ::GetSystemMetrics(SM_CYSCREEN));
    CRect olTSRect(imStartTimeScalePos, 28, olRect.right - (36 + 2), 62);
    omTimeScale.MoveWindow(&olTSRect, TRUE);


	if (bgNewGantDefault)
	{
		char pclView[100];
		strcpy(pclView, "<Default>");
		ogDdx.DataChanged((void *)this, SAS_GET_CUR_VIEW,(void *)pclView );
		omViewer.SelectView(pclView);
		ViewSelChange(pclView);
	}
	else
	{
		char pclView[100];
		strcpy(pclView, "<Default>");
		ogDdx.DataChanged((void *)this, SAS_GET_CUR_VIEW,(void *)pclView );
		omViewer.SelectView(pclView);

		CTime olFrom = TIMENULL;
		CTime olTo = TIMENULL;
		CString olFtyps;
		omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);
		if (olFrom != TIMENULL && olTo != TIMENULL)
		{
			omDuration = (olTo - olFrom) + CTimeSpan(1, 0, 0, 0);
			omStartTime = olFrom - CTimeSpan(1, 0, 0, 0);
		}
		ViewSelChange(pclView);
	}

	// set caption
	CString olTimes;
	if (bgGatPosLocal)
		olTimes = GetString(IDS_STRING1921);
	else
		olTimes = GetString(IDS_STRING1920);
	CString olCaption = GetString(IDS_STRING823);
	olCaption += " ("+olTimes+")";
	SetWindowText(olCaption);

	olRect = CRect(0, 95, ::GetSystemMetrics(SM_CXSCREEN), ::GetSystemMetrics(SM_CYSCREEN));
	GetDialogFromReg(olRect, m_key);
	MoveWindow(olRect);

	return 0;
}

void WroDiagram::SaveToReg() 
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}

void WroDiagram::OnOffline()
{	

	if(ogPosDiaFlightData.bmOffLine)
	{
		if (MessageBox(GetString(IDS_STRING1772), GetString(IMFK_RELEASE), MB_YESNO | MB_ICONQUESTION ) == IDNO)
			return;
	}


	if(bgReasonFlag)
	{
	
		if(ogPosDiaFlightData.bmOffLine && ogPosDiaFlightData.lmPosReasonUrno == 0 && ogPosDiaFlightData.bmPosReason == true) 				
		{
			CString olReason = CFPMSApp::GetSelectedReason(this);
			ogPosDiaFlightData.lmPosReasonUrno = atof(olReason);
			if(ogPosDiaFlightData.lmPosReasonUrno == 0)
				return;
		}

		if(ogPosDiaFlightData.bmOffLine && ogPosDiaFlightData.lmGatReasonUrno == 0 && ogPosDiaFlightData.bmGatReason == true) 				
		{
			CString olReason = CFPMSApp::GetSelectedReason(this, CString("GATF"), CString(""));
			ogPosDiaFlightData.lmGatReasonUrno = atof(olReason);
			if(ogPosDiaFlightData.lmGatReasonUrno == 0)
				return;
		}
	}

	ogPosDiaFlightData.ToggleOffline();


	ogPosDiaFlightData.bmGatReason = false;
	ogPosDiaFlightData.lmGatReasonUrno = 0;
	ogPosDiaFlightData.bmPosReason = false;
	ogPosDiaFlightData.lmPosReasonUrno = 0;



}

void WroDiagram::OnAllocate()
{
	if (!bgOffRelFuncGatpos)
	{
		ogPosDiaFlightData.ToggleOffline();
	}

	if (bgOffRelFuncGatpos && !ogPosDiaFlightData.bmOffLine)
	{
		MessageBox(GetString(IDS_STRING2003), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
		return;
	}

	SpotAllocateDlg *polDlg = new SpotAllocateDlg(this);
	if(polDlg->DoModal() == IDOK)
	{
		omStatusBar.SetPaneText(0, GetString(IDS_STRING1493));
		omStatusBar.UpdateWindow();
	}
	omStatusBar.SetPaneText(0, "");
	omStatusBar.UpdateWindow();
	delete polDlg;

	if (!bgOffRelFuncGatpos)
	{
		ogPosDiaFlightData.ToggleOffline();
	}

}


void WroDiagram::OnBeenden() 
{
	//ogPosDiaFlightData.UnRegister();	 
	//ogPosDiaFlightData.ClearAll();	 
	DestroyWindow();	
}


void WroDiagram::OnInsert()
{
	RotationISFDlg dlg;
	dlg.DoModal();
}


void WroDiagram::OnSearch()
{
	pogFlightSearchTableDlg->SetUrnoFilter( &ogPosDiaFlightData.omUrnoMap);
	pogFlightSearchTableDlg->Activate(omViewer.GetBaseViewName(), omViewer.GetViewName(), WRODIA);
}


void WroDiagram::OnAnsicht()
{
//	WroDiaPropertySheet olDlg("POSDIA", this, &omViewer, 0, GetString(IDS_STRING1649));
	WroDiaPropertySheet olDlg("WRODIA", this, &omViewer, 0, GetString(IDS_STRING1649));
	bmIsViewOpen = true;
	int ilDlgRet = olDlg.DoModal();
	bmIsViewOpen = false;

	if (ilDlgRet != IDCANCEL)
	{
		ogSpotAllocation.InitializePositionMatrix();

		if (olDlg.FilterChanged())
			LoadFlights();
		else
		{
			// upate status bar
			omStatusBar.SetPaneText(0, GetString(IDS_SAS_CHECK));
			omStatusBar.UpdateWindow();

			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			ogKonflikte.SASCheckAll();
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

			// upate status bar
			omStatusBar.SetPaneText(0, "");
			omStatusBar.UpdateWindow();
		}

//		UpdateDia();

		char pclView[100];
		strcpy(pclView, omViewer.GetViewName());
		ogDdx.DataChanged((void *)this, SAS_VIEW_CHANGED,(void *)pclView );

	}

	UpdateComboBox();
	SetFocusToDiagram(); // PRF 8386
}



void WroDiagram::ResetStateVariables()
{
	CTime olUtcTime;
	GetCurrentUtcTime(olUtcTime);

    omTSStartTime = olUtcTime;// - CTimeSpan(0, 1, 0, 0);
    omStartTime = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
        0, 0, 0);// - CTimeSpan(1, 0, 0, 0);

    omDuration = CTimeSpan(3, 0, 0, 0);
    omTSDuration = CTimeSpan(0, 6, 0, 0);
    omTSInterval = CTimeSpan(0, 0, 10, 0);

}



bool WroDiagram::UpdateDia()
{
	CString olViewName = omViewer.GetViewName();
	if (olViewName == "<Default>")
	{
		ogPosDiaFlightData.ClearAll();
		ResetStateVariables();
	}

	
	if(IsIconic())  //RST
	{
		return false;
	}
	


	// set internal variables
	CTime olFrom = TIMENULL;
	CTime olTo = TIMENULL;
	CString olFtyps;
	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);
	if (olFrom != TIMENULL && olTo != TIMENULL)
	{
		omDuration = (olTo - olFrom) + CTimeSpan(1, 0, 0, 0);
		omStartTime = olFrom - CTimeSpan(1, 0, 0, 0);
	}


	omViewer.MakeMasstab();

	CTime olT = omViewer.GetGeometrieStartTime();
//	ogBasicData.UtcToLocal(olT); //RSTL
	SetTSStartTime(olT);

	omTSDuration = omViewer.GetGeometryTimeSpan();
//rkr
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if (!bgNewGantDefault)
	{
		if(bgGatPosLocal)
			ogBasicData.UtcToLocal(olTSStartTime);
	}

	if (bgNewGantDefault)
	{
		if (olViewName != "<Default>")
		{
			if(bgGatPosLocal)
				ogBasicData.UtcToLocal(olTSStartTime);
		}
		else
		{
			if(!bgGatPosLocal)
				ogBasicData.LocalToUtc(olTSStartTime);
		}
	}

	omTimeScale.SetDisplayTimeFrame(olTSStartTime, omTSDuration, omTSInterval);

	if (bgNewGantDefault)
		omTimeScale.SetDisplayStartTime(olTSStartTime);

    omTimeScale.Invalidate(TRUE);
	ChangeViewTo(omViewer.GetViewName(), false);

	long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	if (llTotalMin > 0)
		nPos = nPos * 1000L / llTotalMin;
	SetScrollPos(SB_HORZ, int(nPos), TRUE);

	/*
	char pclView[100];
	strcpy(pclView, omViewer.GetViewName());
	ogDdx.DataChanged((void *)this, SAS_VIEW_CHANGED,(void *)pclView );
	*/

	pogWoResTableDlg->Rebuild();

	return true;
}



bool WroDiagram::LoadFlights(const char *pspView)
{
	if(pspView == NULL)
	{
		CString olView = omViewer.GetViewName();
		pspView = olView.GetBuffer(0);
	}
	

	omViewer.SelectView(pspView);
	if (strcmp(pspView, "<Default>") == 0)
	{
		return false;
	}


	// get Where-Clause
	CString olWhere;
	bool blRotation;
	CString olFOGTAB = "";

	bool blWithAcOnGround = bgShowOnGround;
	if (pogPosDiagram)
		blWithAcOnGround = pogPosDiagram->WithAcOnGround();

	if (!omViewer.GetZeitraumWhereString(olWhere, blRotation, olFOGTAB, blWithAcOnGround))
		return false;

	// PRF 8382
	////Forms the query for the Genral filter page.
	CString  olstring = omViewer.GetUnifilterWhereString();
	olstring.TrimLeft();
	olstring.TrimRight();
	if(!olstring.IsEmpty())
	{
		olWhere = olWhere + olstring;	
	}

	olWhere.TrimRight();
	if (olWhere.IsEmpty())
		return false;

	if (bgMustHaveFLNO)
		olWhere += CString(" AND FLNO <> ' '");



	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));

	// Update Status Bar
	omStatusBar.SetPaneText(0, GetString(IDS_STRING1205));
	omStatusBar.UpdateWindow();

		
	ogPosDiaFlightData.Register();	 


	// get timeframe
	CTime olFrom;
	CTime olTo;
	CString olFtyps;
	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);

	//check timelimit for on gound (at moment we don#t know if we should use global or local)
	if (bgShowOnGround && pogPosDiagram)
	{
		if (olFrom != TIMENULL && olTo != TIMENULL)
		{
			CTimeSpan olDuration = olTo - olFrom;
			int ilTotalMinutes = olDuration.GetTotalMinutes();
			if (ogOnGroundTimelimit.GetTotalMinutes() >= ilTotalMinutes /*&& !bmMessageDone*/)
			{
				CString olMess;
				CString olTimeLimit;
				int ilHours = ogOnGroundTimelimit.GetTotalHours();
				int ilMinutes = ogOnGroundTimelimit.GetTotalMinutes() - ilHours*60;
				olTimeLimit.Format("%d.%d",ilHours,ilMinutes);
				olMess.Format(GetString(IDS_ONGROUND_TIMELIMIT), olTimeLimit );

				if (CFPMSApp::MyTopmostMessageBox(NULL, olMess, GetString(ST_WARNING), MB_OKCANCEL | MB_ICONWARNING) == IDOK)
					bool blok = true;
				else
					return false;
			}
		}
	}

	/*
	// enable/disable now button
	CTime olCurrUtc;
	GetCurrentUtcTime(olCurrUtc);

	CButton *polCB = (CButton *) omDialogBar.GetDlgItem(IDC_ZEIT);

	if(olFrom < olCurrUtc && olTo > olCurrUtc)
	{
		polCB->EnableWindow(TRUE);
	}
	else
	{
		polCB->EnableWindow(FALSE);
	}
	*/



	// set pre-filter
	ogPosDiaFlightData.SetPreSelection(olFrom, olTo, olFtyps);
	
	// read flights
	ogPosDiaFlightData.ReadAllFlights(olWhere, blRotation, olFOGTAB);

	// set internal variables
	omDuration = (olTo - olFrom) + CTimeSpan(1, 0, 0, 0);
	omStartTime = olFrom - CTimeSpan(1, 0, 0, 0);
	
	// Update Status Bar
	omStatusBar.SetPaneText(0, "");
	omStatusBar.UpdateWindow();

	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));

	return true;
}

void WroDiagram::OnDestroy() 
{
	SaveToReg();
//	if (bgModal == TRUE)
//		return;
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	ogDdx.UnRegister(&omViewer, NOTUSED);

	// Unregister DDX call back function
	TRACE("WroDiagram: DDX Unregistration %s\n",
		::IsWindow(GetSafeHwnd())? "": "(window is already destroyed)");
    ogDdx.UnRegister(this, NOTUSED);

	// Disconnect from the GatPosFlightData-Class
 	ogPosDiaFlightData.Disconnect();
	// Disconnect from the FlightsWithoutResource - Table
	if (pogWoResTableDlg)
		pogWoResTableDlg->Disconnect();

	if (pomBltOverviewTableDlg)
	{
		delete pomBltOverviewTableDlg;
		pomBltOverviewTableDlg = NULL;
	}


	CFrameWnd::OnDestroy();
}

void WroDiagram::OnClose() 
{
    // Ignore close -- This makes Alt-F4 keys no effect
	CFrameWnd::OnClose();
}

void WroDiagram::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
    // TODO: Add your message handler code here and/or call default
    CFrameWnd::OnGetMinMaxInfo(lpMMI);

    lpMMI->ptMaxTrackSize = omMaxTrackSize;
    lpMMI->ptMinTrackSize = omMinTrackSize;
}

BOOL WroDiagram::OnEraseBkgnd(CDC* pDC) 
{
    // TODO: Add your message handler code here and/or call default
    CRect olClipRect;
    pDC->GetClipBox(&olClipRect);
    //GetClientRect(&olClipRect);

    CBrush olBrush(lmBkColor);
    CBrush *polOldBrush = pDC->SelectObject(&olBrush);
    pDC->PatBlt(olClipRect.left, olClipRect.top,
        olClipRect.Width(), olClipRect.Height(),
        PATCOPY);
    pDC->SelectObject(polOldBrush);
    
    return TRUE;
    
    //return CFrameWnd::OnEraseBkgnd(pDC);
}

void WroDiagram::OnPaint() 
{
    CPaintDC dc(this); // device context for painting
    
    // TODO: Add your message handler code here
    CRect olRect;
    GetClientRect(&olRect);
    
    // draw horizontal line
    CPen olHPen(PS_SOLID, 1, lmHilightColor);
    CPen *polOldPen = dc.SelectObject(&olHPen);
    dc.MoveTo(olRect.left, 27); dc.LineTo(olRect.right, 27);
    dc.MoveTo(olRect.left, 62); dc.LineTo(olRect.right, 62);

    CPen olTPen(PS_SOLID, 1, lmTextColor);
    dc.SelectObject(&olTPen);
    dc.MoveTo(olRect.left, 63); dc.LineTo(olRect.right, 63);
    
    
    // draw vertical line
    dc.SelectObject(&olTPen);
    dc.MoveTo(imStartTimeScalePos - 2, 27);
    dc.LineTo(imStartTimeScalePos - 2, 63);

    dc.SelectObject(&olHPen);
    dc.MoveTo(imStartTimeScalePos - 1, 27);
    dc.LineTo(imStartTimeScalePos - 1, 63);

    dc.SelectObject(polOldPen);
    // Do not call CFrameWnd::OnPaint() for painting messages
}


void WroDiagram::OnSize(UINT nType, int cx, int cy) 
{
    CFrameWnd::OnSize(nType, cx, cy);
    
    // TODO: Add your message handler code here
    CRect olRect; GetClientRect(&olRect);

    CRect olBB1Rect(olRect.right - 36, 27 + 5, olRect.right - 19, (27 + 5) + 25);
    omBB1.MoveWindow(&olBB1Rect, TRUE);
    
    CRect olBB2Rect(olRect.right - 19, 27 + 5, olRect.right - 2, (27 + 5) + 25);
    omBB2.MoveWindow(&olBB2Rect, TRUE);

    CRect olTSRect(imStartTimeScalePos, 28, olRect.right - (36 + 2), 62);
    omTimeScale.MoveWindow(&olTSRect, TRUE);

    olRect.top += 64;                   // for Dialog Bar & TimeScale
    olRect.bottom -= 20;                // for Horizontal Scroll Bar
    // LeftTop, RightBottom
    omClientWnd.MoveWindow(&olRect, TRUE);

	GetClientRect(&olRect);
	omTime.MoveWindow( CRect(olRect.right - 192, 3, olRect.right - 90, 22));
    omDate.MoveWindow( CRect(olRect.right - 88, 3, olRect.right - 8, 22));

	PositionChild();
}

void WroDiagram::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
    // TODO: Add your message handler code here and/or call default
    
    //CFrameWnd::OnHScroll(nSBCode, nPos, pScrollBar);

	CTime olTSStartTime = omTSStartTime;
 	int ilCurrLine = pomChart->omGantt.GetTopIndex();

    long llTotalMin;
    int ilPos;
    
    switch (nSBCode)
    {
        case SB_LINEUP :
            //OutputDebugString("LineUp\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ) - int (60 * 1000L / llTotalMin);
            if (ilPos <= 0)
            {
                ilPos = 0;
                ShowTime(omStartTime);
            }
            else
                ShowTime(omTSStartTime - CTimeSpan(0, 0, 60, 0));

			omViewer.UpdateLineHeights();
        break;
        
        case SB_LINEDOWN :
            //OutputDebugString("LineDown\n\r");

            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ) + int (60 * 1000L / llTotalMin);
            if (ilPos >= 1000)
            {
                ilPos = 1000;
                ShowTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin+1), 0));
            }
            else
                ShowTime(omTSStartTime + CTimeSpan(0, 0, 60, 0));

			omViewer.UpdateLineHeights();
        break;
        
        case SB_PAGEUP :
            //OutputDebugString("PageUp\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ)
                - int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
            if (ilPos <= 0)
            {
                ilPos = 0;
                ShowTime(omStartTime);
            }
            else
                ShowTime(omTSStartTime - CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));

			omViewer.UpdateLineHeights();
        break;
        
        case SB_PAGEDOWN :
            //OutputDebugString("PageDown\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
            ilPos = GetScrollPos(SB_HORZ)
                + int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);

            if (ilPos >= 1000)
            {
                ilPos = 1000;
                ShowTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin+1), 0));
            }
            else
                ShowTime(omTSStartTime + CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));

			omViewer.UpdateLineHeights();
        break;
        
        case SB_THUMBTRACK /* pressed, any drag time */:
            //OutputDebugString("ThumbTrack\n\r");

            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
            
            SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin * nPos / 1000), 0));

			olTSStartTime = omTSStartTime;
			if(bgGatPosLocal)
				ogBasicData.UtcToLocal(olTSStartTime);

            omTimeScale.SetDisplayStartTime(olTSStartTime);
            omTimeScale.Invalidate(TRUE);
            SetScrollPos(SB_HORZ, nPos, TRUE);

            omClientWnd.Invalidate(FALSE);
        break;
        case SB_THUMBPOSITION:	// the thumb was just released?
            omClientWnd.Invalidate(FALSE);


        case SB_TOP :
        //break;
        case SB_BOTTOM :
            //OutputDebugString("TopBottom\n\r");
        break;
        
        case SB_ENDSCROLL :
            //OutputDebugString("EndScroll\n\r");
            //OutputDebugString("\n\r");
        break;
    }

 	pomChart->omGantt.SetTopIndex(ilCurrLine);

}


void WroDiagram::OnTimer(UINT nIDEvent)
{

	if (nIDEvent == 1 && !bgConfCheckRuns)
	{
		DeActivateTimers();
		ogPosDiaFlightData.UpdateBarTimes();
		ActivateTimers();
	}

	//// Update time line
	if(bmNoUpdatesNow == FALSE)
	{
		CTime olLocalTime = CTime::GetCurrentTime();
		CTime olUtcTime = CTime::GetCurrentTime();
		ogBasicData.LocalToUtc(olUtcTime);

		char pclTimes[100];
		char pclDate[100];
		
		CTime olUtcTimeTmp = CTime::GetCurrentTime();
		olUtcTimeTmp -= ogUtcDiff;
		
		if(ogUtcDiff != 0)
		{
			sprintf(pclDate, "%02d.%02d.%02d z", olUtcTimeTmp.GetDay(), olUtcTimeTmp.GetMonth(), olUtcTimeTmp.GetYear() % 100);
			sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTimeTmp.GetHour(), olUtcTimeTmp.GetMinute());
		}
		else
		{
			sprintf(pclDate, "%02d.%02d.%02d z", olUtcTime.GetDay(), olUtcTime.GetMonth(), olUtcTime.GetYear() % 100);
			sprintf(pclTimes, "%02d:%02d / %02d:%02d z", olLocalTime.GetHour(), olLocalTime.GetMinute(), olUtcTime.GetHour(), olUtcTime.GetMinute());
		}
		if(bgGatPosLocal)
			omTimeScale.UpdateCurrentTimeLine(olLocalTime);
		else
			omTimeScale.UpdateCurrentTimeLine(olUtcTime);

		omTime.SetWindowText(pclTimes);
		omTime.Invalidate(FALSE);

		omDate.SetWindowText(pclDate);
		omDate.Invalidate(FALSE);


		if(bgGatPosLocal)
			pomChart->GetGanttPtr()->SetCurrentTime(olLocalTime);
		else
			pomChart->GetGanttPtr()->SetCurrentTime(olUtcTime);


		CFrameWnd::OnTimer(nIDEvent);
	}
	///// Update time line! (END)

}

void WroDiagram::SetMarkTime(CTime opStartTime, CTime opEndTime)
{
	pomChart->SetMarkTime(opStartTime, opEndTime);
}


LONG WroDiagram::OnPositionChild(WPARAM wParam, LPARAM lParam)
{
    PositionChild();
    return 0L;
}

LONG WroDiagram::OnUpdateDiagram(WPARAM wParam, LPARAM lParam)
{
//	UpdateWoResButton();

    CString olStr;
    int ilGroupNo = HIWORD(lParam);
    int ilLineNo = LOWORD(lParam);

	int ilItemID = omViewer.GetItemID(ilGroupNo, ilLineNo);

	CTime olTime = CTime((time_t) lParam);

	char clBuf[255];
	int ilCount;

    WroGantt *polWroGantt = pomChart -> GetGanttPtr();
    switch (wParam)
    {
        // group message

        case UD_UPDATEGROUP :
            olStr = omViewer.GetGroupText(ilGroupNo);
            pomChart->GetChartButtonPtr()->SetWindowText(olStr);
            pomChart->GetChartButtonPtr()->Invalidate(FALSE);

            olStr = omViewer.GetGroupTopScaleText(ilGroupNo);
            pomChart->GetTopScaleTextPtr()->SetWindowText(olStr);
            pomChart->GetTopScaleTextPtr()->Invalidate(TRUE);
			//SetStaffAreaButtonColor(ilGroupNo);
        break;
        
        // line message
        case UD_INSERTLINE :
            polWroGantt->InsertString(ilItemID, "");
			polWroGantt->RepaintItemHeight(ilItemID);
			
			ilCount = omViewer.GetLineCount(ilGroupNo);
			sprintf(clBuf, "%d", ilCount);
			pomChart->GetCountTextPtr()->SetWindowText(clBuf);
			pomChart->GetCountTextPtr()->Invalidate(TRUE);

			//SetStaffAreaButtonColor(ilGroupNo);
            PositionChild();
        break;
        
        case UD_UPDATELINE :
            polWroGantt->RepaintVerticalScale(ilItemID);
            polWroGantt->RepaintGanttChart(ilItemID);
            polWroGantt->RepaintItemHeight(ilItemID);
        break;

        case UD_DELETELINE :
            polWroGantt->DeleteString(ilItemID);
            
			ilCount = omViewer.GetLineCount(ilGroupNo);
			sprintf(clBuf, "%d", ilCount);
			pomChart->GetCountTextPtr()->SetWindowText(clBuf);
			pomChart->GetCountTextPtr()->Invalidate(TRUE);

			//SetStaffAreaButtonColor(ilGroupNo);
			PositionChild();
        break;

        case UD_UPDATELINEHEIGHT :
            polWroGantt->RepaintItemHeight(ilItemID);
			//SetStaffAreaButtonColor(ilGroupNo);
            PositionChild();
		break;
    }

    return 0L;
}


///////////////////////////////////////////////////////////////////////////////
// Damkerng and Pichet: Date: 5 July 1995

void WroDiagram::UpdateComboBox()
{
	CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
	polCB->ResetContent();
	CStringArray olStrArr;
	omViewer.GetViews(olStrArr);
	for (int ilIndex = 0; ilIndex < olStrArr.GetSize(); ilIndex++)
	{
		polCB->AddString(olStrArr[ilIndex]);
	}
	CString olViewName = omViewer.GetViewName();
	if (olViewName.IsEmpty() == TRUE)
	{

		olViewName = omViewer.SelectView();//ogCfgData.rmUserSetup.STCV;
	}

	ilIndex = polCB->FindString(-1,olViewName);
		
	if (ilIndex != CB_ERR)
	{
		polCB->SetCurSel(ilIndex);
	}
}

void WroDiagram::ChangeViewTo(const char *pcpViewName,bool RememberPositions)
{
	if (bgNoScroll == TRUE)
		return;


	if (pomChart != NULL)
	{
		if (!pomChart->omGantt.bmRepaint)
		{
			bmRepaintAll = true;
			return;
		}
    }

	int ilDwRef = m_dwRef;
	AfxGetApp()->DoWaitCursor(1);
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);

	CCSPtrArray <int> olChartStates;
	olChartStates.RemoveAll();

	CRect olRect; omTimeScale.GetClientRect(&olRect);

	/*
//rkr25042001
	//Set begin to local if local
	CTime olTSStartTime = omTSStartTime;
	if(bgGatPosLocal)
		ogBasicData.UtcToLocal(olTSStartTime);
    omViewer.ChangeViewTo(pcpViewName, olTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));
	*/
	// viewer always in UTC
    omViewer.ChangeViewTo(pcpViewName, omTSStartTime, omTimeScale.GetTimeFromX(olRect.Width() + 38));

	int ilChartState;
	int ilTopIndex;
 	if (RememberPositions == TRUE && pomChart != NULL)
	{
		ilChartState = pomChart->GetState();
		if (pomChart->GetGanttPtr() != NULL)
			ilTopIndex = pomChart->GetGanttPtr()->GetTopIndex();
	}


    CRect olPrevRect;
    omClientWnd.GetClientRect(&olRect);
    
	if (RememberPositions == FALSE)
	{
	    imFirstVisibleChart = 0;
	}
    int ilLastY = 0;
	pomChart->DestroyWindow();
    omClientWnd.GetClientRect(&olRect);
        
	olRect.SetRect(olRect.left, 0, olRect.right, 0);
        
    pomChart = new WroChart;
    pomChart->SetTimeScale(&omTimeScale);
    pomChart->SetViewer(&omViewer);
    pomChart->SetStatusBar(&omStatusBar);
    pomChart->SetStartTime(omTSStartTime);
    pomChart->SetInterval(omTimeScale.GetDisplayDuration());
 	if (RememberPositions == TRUE)
	{
		pomChart->SetState(ilChartState);
	}
	else
	{
		pomChart->SetState(Maximized);
	}

    pomChart->Create(NULL, "WroChart", WS_OVERLAPPED | WS_BORDER | WS_CHILD | WS_VISIBLE,
            olRect, &omClientWnd, 0 /* IDD_CHART */, NULL);


 	if (RememberPositions == TRUE)
	{
		if (pomChart->GetGanttPtr() != NULL)
 			pomChart->GetGanttPtr()->SetTopIndex(ilTopIndex);
	}


	PositionChild();
	AfxGetApp()->DoWaitCursor(-1);
	olChartStates.DeleteAll();
	bmNoUpdatesNow = FALSE;
	omViewer.AllowUpdates(bmNoUpdatesNow);

	if (pomBltOverviewTableDlg)
		pomBltOverviewTableDlg->Update();

	UpdateWoResButton();

	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();
}

/*
void WroDiagram::UpdateWoResButton()
{
	int ilCount = pogWoResTableDlg->GetCount(WRODIA);

	if(ilCount >= 0)
	{
		CString olTmp;
		olTmp.Format("%s (%d)", GetString(IDS_STRING1914),ilCount);

		CButton *polSave = (CButton *)omDialogBar.GetDlgItem(IDC_WORES);	
		if(polSave != NULL)
			polSave->SetWindowText(olTmp);
	}

}
*/

void WroDiagram::OnOverview()
{
	if (pomBltOverviewTableDlg != NULL)
 		pomBltOverviewTableDlg->Activate();
}


void WroDiagram::UpdateWoResButton()
{
	CString olTmp;
	COLORREF olButtonColor;

	int ilCount = pogWoResTableDlg->GetCount(WRODIA);
	if(ilCount > 0)
	{
		if(bgFwrWro)
			olButtonColor = ogColors[IDX_ORANGE];
		else
			olButtonColor = ::GetSysColor(COLOR_BTNFACE);

		olTmp.Format("%s (%d)", GetString(IDS_STRING2624),ilCount);
	}
	else
	{
		olTmp.Format("%s (%d)", GetString(IDS_STRING2624),ilCount);
		olButtonColor = ::GetSysColor(COLOR_BTNFACE);
	}

	m_CB_WoRes.SetWindowText(olTmp);
	m_CB_WoRes.SetColors(olButtonColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_WoRes.UpdateWindow();
}

void WroDiagram::UpdateChangesButton(CString& opText, COLORREF opColor)
{
	CString olTmp;
	olTmp.Format("%s (%s)", GetString(IDS_CHANGES),opText);

	m_CB_Changes.SetWindowText(olTmp);
	m_CB_Changes.SetColors(opColor, ::GetSysColor(COLOR_BTNSHADOW), ::GetSysColor(COLOR_BTNHIGHLIGHT));
	m_CB_Changes.UpdateWindow();
}

void WroDiagram::OnViewSelChange()
{
	//PRF 8363, Change the focus to gantt chart 
	SetFocusToDiagram(); 
    char clText[64];
    CComboBox *polCB = (CComboBox *) omDialogBar.GetDlgItem(IDC_VIEW);
    polCB->GetLBText(polCB->GetCurSel(), clText);
	if(strcmp(clText, "<Default>") == 0)
		return;

	ogSpotAllocation.InitializePositionMatrix();
	if (!LoadFlights(clText))
		return;

//	UpdateDia();

	ogDdx.DataChanged((void *)this, SAS_VIEW_CHANGED,(void *)clText );
}


void WroDiagram::OnCloseupView() 
{
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();
}

void WroDiagram::OnZeit()
{
	CTime olTime;
	GetCurrentUtcTime(olTime);
	olTime -= CTimeSpan(0, 1, 0, 0);

	ShowTime(olTime);

	/*
	// Id 25-Sep-96
	// This will set the keyboard focus back to the bottom most list-box after this button was clicked.
	//SetFocus();

	// TODO: Add your command handler code here
	CTime olTime = CTime::GetCurrentTime();
	
	if(!bgGatPosLocal)	
		ogBasicData.LocalToUtc(olTime);


	//Die wollen local
	olTime -= CTimeSpan(0, 1, 0, 0);
	SetTSStartTime(olTime);
	//TRACE("Time: [%s]\n", olTime.Format("%H:%M"));

	omTimeScale.SetDisplayStartTime(omTSStartTime);
    omTimeScale.Invalidate(TRUE);
		
	// update scroll bar position
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
    SetScrollPos(SB_HORZ, int(nPos), TRUE);

	ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
    omClientWnd.Invalidate(FALSE);
	*/
}


BOOL WroDiagram::DestroyWindow() 
{

	if ((bmIsViewOpen))
	{
		MessageBeep((UINT)-1);
		return FALSE;    // don't destroy window while view property sheet is still open
	}
	pogWroDiagram = NULL; 

	ogDdx.UnRegister(&omViewer, NOTUSED);
	bmNoUpdatesNow = TRUE;
	omViewer.AllowUpdates(bmNoUpdatesNow);
	BOOL blRc = CWnd::DestroyWindow();
	return blRc;
}

/////////XXXXX
////////////////////////////////////////////////////////////////////////
// WroDiagram keyboard handling

void WroDiagram::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// check if the control key is pressed
    BOOL blIsControl = ::GetKeyState(VK_CONTROL) & 0x8080;
		// This statement has to be fixed for using both in Windows 3.11 and NT.
		// In Windows 3.11 the bit 0x80 will be turned on if control key is pressed.
		// In Windows NT, the bit 0x8000 will be turned on if control key is pressed.

	switch (nChar)
	{
	case VK_LEFT:
		OnHScroll(blIsControl? SB_PAGEUP: SB_LINEUP, 0, NULL);
		break;
	case VK_RIGHT:
		OnHScroll(blIsControl? SB_PAGEDOWN: SB_LINEDOWN, 0, NULL);
		break;
	case VK_HOME:
		SetScrollPos(SB_HORZ, 0, FALSE);
		OnHScroll(SB_LINEUP, 0, NULL);
		break;
	case VK_END:
		SetScrollPos(SB_HORZ, 1000, FALSE);
		OnHScroll(SB_LINEDOWN, 0, NULL);
		break;
	default:
		omDialogBar.SendMessage(WM_KEYDOWN, nChar, MAKELONG(LOWORD(nRepCnt), LOWORD(nFlags)));
		CFrameWnd::OnKeyDown(nChar, nRepCnt, nFlags);
		break;
	}
}

void WroDiagram::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	CFrameWnd::OnKeyUp(nChar, nRepCnt, nFlags);
}

////////////////////////////////////////////////////////////////////////
// WroDiagram -- implementation of DDX call back function


void WroDiagram::RedisplayAll()
{
	ChangeViewTo(omViewer.SelectView());//ogCfgData.rmUserSetup.STCV);
}

void WroDiagram::SetTimeBand(CTime opStartTime, CTime opEndTime)
{
	omTimeBandStartTime = opStartTime;
	omTimeBandEndTime = opEndTime;
}

void WroDiagram::UpdateTimeBand()
{
	pomChart->omGantt.SetMarkTime(omTimeBandStartTime, omTimeBandEndTime);
}

LONG WroDiagram::RepaintAll(WPARAM wParam, LPARAM lParam)
{
	ChangeViewTo(omViewer.GetViewName(), true);
	return 0L;
}


void WroDiagram::OnWoRes()
{
	pogWoResTableDlg->Activate(WRODIA);
}

void WroDiagram::OnChanges()
{
	CallHistory(CString("WROLOUNGES"));
}


void WroDiagram::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized) 
{
	CFrameWnd::OnActivate(nState, pWndOther, bMinimized);
	
	pogWoResTableDlg->Activate(WRODIA, false);


}


void WroDiagram::ShowTime(const CTime &ropTime)
{

	if(omTSStartTime == ropTime)
		return;

	
	SetTSStartTime(ropTime);
	//TRACE("Time: [%s]\n", olTime.Format("%H:%M"));

	CTime olTSStartTime(omTSStartTime);
	if(bgGatPosLocal)	
		ogBasicData.UtcToLocal(olTSStartTime);


	omTimeScale.SetDisplayStartTime(olTSStartTime);
    omTimeScale.Invalidate(TRUE);
		
	// update scroll bar position
    long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
	long nPos = (omTSStartTime - omStartTime).GetTotalMinutes();
	nPos = nPos * 1000L / llTotalMin;
    SetScrollPos(SB_HORZ, int(nPos), TRUE);

    omClientWnd.Invalidate(FALSE);
}



bool WroDiagram::ShowFlight(long lpUrno)
{
	DIAFLIGHTDATA *prlFlight = ogPosDiaFlightData.GetFlightByUrno(lpUrno);
	
	if (prlFlight == NULL)
		return false;

	int index = omViewer.AdjustBar(prlFlight, NULL);
	if (index != -1)
		pomChart->omGantt.SetTopIndex(index);
	else
	{
		CTime olStart;
		CTime olEnd;
 		if (omViewer.CalculateTimes(*prlFlight, olStart, olEnd))
		{	
			CTime olTime = olStart - CTimeSpan(0, 1, 0, 0);
			ShowTime(olTime);
			if(bgGatPosLocal) ogBasicData.UtcToLocal(olStart);
			if(bgGatPosLocal) ogBasicData.UtcToLocal(olEnd);
			SetMarkTime(olStart, olEnd);
		}	
	}

	return true;
}

void WroDiagram::OnExpand()
{

	CString olStev;
	omViewer.GetStev(olStev);


	long llMinutes = omDuration.GetTotalMinutes();

	CTime olRefDate = omStartTime + CTimeSpan(0,0, llMinutes / 2  ,0);

	CTime olFrom = TIMENULL;
	CTime olTo = TIMENULL;
	CString olFtyps;
	omViewer.GetTimeSpanAndFtyp(olFrom, olTo, olFtyps);
 

	CCAExpandDlg olDlg (this, SUB_MOD_WRO, olStev, olRefDate, olFrom, olTo);
	olDlg.DoModal();

}


void WroDiagram::OnPrint() 
{
	omViewer.PrintGantt(pomChart);
	
}

bool WroDiagram::GetLoadedFlights(CCSPtrArray<DIAFLIGHTDATA> &opFlights)
{
	return omViewer.GetLoadedFlights(opFlights);
}

void WroDiagram::SetFocusToDiagram()
{
	//PRF 8363, Change the focus to gantt chart sisl//
    WroGantt *polWroGantt = pomChart -> GetGanttPtr();
	CWnd* polTopWindow = CWnd::GetActiveWindow();
	if(pomChart->GetParent() != NULL)
	{
	CWnd* polParent = pomChart->GetParent()->GetParent();
	if(polTopWindow != NULL && polParent != NULL)
	{		
		if(polParent->m_hWnd != NULL && polTopWindow->m_hWnd == polParent->m_hWnd)
		polWroGantt->SetFocus();
	}
	}
}
