// CicNoDemandTableDlg.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <CicNoDemandTableDlg.h>
#include <SeasonDlg.h>
#include <RotationDlg.h>
#include <CcaCedaFlightData.h>
#include <DataSet.h>
#include <utils.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CicNoDemandTableDlg dialog


CicNoDemandTableDlg::CicNoDemandTableDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CicNoDemandTableDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CicNoDemandTableDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pomTable = NULL;
	isCreated = false;

    CDialog::Create(CicNoDemandTableDlg::IDD, pParent);
	isCreated = true;

	m_key = "DialogPosition\\WithoutDemands";
}

CicNoDemandTableDlg::~CicNoDemandTableDlg()
{
	delete pomTable;
}

void CicNoDemandTableDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CicNoDemandTableDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CicNoDemandTableDlg, CDialog)
	//{{AFX_MSG_MAP(CicNoDemandTableDlg)
	ON_WM_SIZE()
	ON_WM_CLOSE()
	ON_MESSAGE(WM_DRAGOVER, OnDragOver)
	ON_MESSAGE(WM_DROP, OnDrop)  
    ON_MESSAGE(WM_TABLE_DRAGBEGIN, OnTableDragBegin)
    ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK, OnTableLButtonDblclk)
    ON_MESSAGE(WM_TABLE_LBUTTONDOWN, OnTableLButtonDown)
    ON_MESSAGE(WM_TABLE_RETURN_PRESSED, OnTableReturnPressed)
	ON_MESSAGE(WM_TABLE_SELCHANGE, OnTableSelChanged)
 	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CicNoDemandTableDlg message handlers



void CicNoDemandTableDlg::Activate()
{


	if(!omViewer.bmInit)
		omViewer.ChangeViewTo("");

}


void CicNoDemandTableDlg::Reset()
{
	omViewer.ClearAll();
	CheckPostFlightCcaDia(0,this);
}

void CicNoDemandTableDlg::SaveToReg()
{
	BOOL ok = WriteDialogToReg((CWnd*) this, m_key);
}


void CicNoDemandTableDlg::OnDestroy() 
{
	SaveToReg();
	CDialog::OnDestroy();
}



BOOL CicNoDemandTableDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	omDragDropObject.RegisterTarget(this, this);

	m_key = "DialogPosition\\WithoutDemands";

	CRect olRect;
	this->GetWindowRect(&olRect);
	GetDialogFromReg(olRect, m_key);
	SetWindowPos(&wndTop, olRect.left, olRect.top, olRect.Width(), olRect.Height(), SWP_HIDEWINDOW);// | SWP_NOMOVE );

	pomTable = new CCSTable;
	pomTable->SetSelectMode(LBS_MULTIPLESEL | LBS_EXTENDEDSEL);
	pomTable->SetHeaderSpacing(0);

	pomTable->bmMultiDrag = true;


    GetClientRect(&olRect);


	//olRect.top = olRect.top + imDialogBarHeight;
	//olRect.bottom = olRect.bottom;// - imDialogBarHeight;

    olRect.InflateRect(1, 1);     // hiding the CTable window border
    
	pomTable->SetTableData(this, olRect.left, olRect.right, olRect.top, olRect.bottom);

	omViewer.Attach(pomTable);

	omViewer.ChangeViewTo("");

	SetWindowPos(&wndTop, 0,0,0,0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CicNoDemandTableDlg::OnClose() 
{
	ShowWindow(SW_HIDE);	

//	omViewer.UnRegister();

	//CDialog::OnClose();
}


////////////////////////////////////////////////////////////////////////////
// Dragbeginn einer Drag&Drop Aktion
//
LONG CicNoDemandTableDlg::OnTableDragBegin(UINT wParam, LONG lParam)
{
	return -1L;
/*
	int ilCol;
	int ilLine;

	CCSPtrArray<DIACCADATA> olData;

	CICNODEMANDTABLE_LINEDATA *prlLine = (CICNODEMANDTABLE_LINEDATA*)pomTable->GetTextLineData(wParam);
	CPoint point;
    ::GetCursorPos(&point);
    pomTable->pomListBox->ScreenToClient(&point);    
    //ScreenToClient(&point);    

	if (prlLine != NULL)
	{
		ilLine = pomTable->GetLinenoFromPoint(point);
		ilCol = pomTable->GetColumnnoFromPoint(point);
	}
	else
		return 0L;


	int ilItems[300];
	int ilAnz = 300;
	int ilAnzSel = 0;

	ilAnz = (pomTable->GetCTableListBox())->GetSelItems(ilAnz, ilItems);
	if(ilAnz <= 0)
	{
		return 0L;
	}


	omDragDropObject.CreateDWordData(DIT_CCA_DEMAND, ilAnz);
	
	for(int i = 0; i < ilAnz; i++)
	{
		prlLine = (CICNODEMANDTABLE_LINEDATA *)pomTable->GetTextLineData(ilItems[i]);
		if(prlLine->Urno != 0)
		{
			DIACCADATA *prlCca = ogCcaDiaFlightData.omCcaData.GetCcaByUrno(prlLine->Urno);

			if(prlCca == NULL)
			{
				return 0L;
			}
			else
			{
				if(strcmp(prlCca->Ctyp, "E") != 0)
				{
					omDragDropObject.AddDWord((long)prlLine->Urno);
				}
			}
		}
	}

	ogBcHandle.BufferBc();
	omDragDropObject.BeginDrag();
	ogBcHandle.ReleaseBuffer();
*/
	return 0L;
}


////////////////////////////////////////////////////////////////////////////
// linke Maustaste auf der Flighttable gedr�ckt
//
LONG CicNoDemandTableDlg::OnTableLButtonDblclk( UINT wParam, LPARAM lParam)
{
	UINT ipItem = wParam;
	CICNODEMANDTABLE_LINEDATA *prlTableLine = NULL;
	prlTableLine = (CICNODEMANDTABLE_LINEDATA *)pomTable->GetTextLineData(ipItem);
	if (prlTableLine != NULL)
	{
		CCAFLIGHTDATA *prlFlight = ogCcaDiaFlightData.GetFlightByUrno(prlTableLine->Urno);;

		if(prlFlight != NULL)
		{
 			if (prlTableLine->Sto - CTimeSpan(2,0,0,0) < CTime::GetCurrentTime())
			{
				pogRotationDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, "D", bgGatPosLocal);
			}
			else
			{
				pogSeasonDlg->NewData(this, prlFlight->Rkey, prlFlight->Urno, DLG_CHANGE, bgGatPosLocal);
			}
		}
	}
	return 0L;
}




LONG CicNoDemandTableDlg::OnTableLButtonDown(UINT wParam, LONG lParam)
{

	UINT ipItem = wParam;
	CICNODEMANDTABLE_LINEDATA *prlTableLine = NULL;
	prlTableLine = (CICNODEMANDTABLE_LINEDATA *)pomTable->GetTextLineData(ipItem);
	if (prlTableLine != NULL)
	{
		CCAFLIGHTDATA *prlFlight = ogCcaDiaFlightData.GetFlightByUrno(prlTableLine->Urno);;

		if(prlFlight != NULL)
			ogDdx.DataChanged((void *)this, SHOW_FLIGHT, (void*)&(prlFlight->Urno));
	}


	return 0L;

}

void CicNoDemandTableDlg::OnSize(UINT nType, int cx, int cy) 
{
	if(isCreated != false)
	{
		CDialog::OnSize(nType, cx, cy);

		if(pomTable != NULL)
		{
			CRect rect;	
			GetClientRect(&rect);

			rect.InflateRect(1, 1);     // hiding the CTable window border
    
			pomTable->SetPosition(rect.left, rect.right, rect.top, rect.bottom);
			pomTable->DisplayTable();
		}
	}
/*	if(isCreated != false)
	{
		CDialog::OnSize(nType, cx, cy);
	
		if (nType != SIZE_MINIMIZED)
		{
			pomTable->SetPosition(1, cx+1, 1, cy+1);
		}
	}
*/
}
LONG CicNoDemandTableDlg::OnDragOver(UINT wParam, LONG lParam)
{
	int ilLine = -1;
	int ilCol = -1;
	int ilLogicLine = -1;
	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);

   int ilClass = omDragDropObject.GetDataClass(); 
	if (ilClass == DIT_CCA_GANTT)
	{
		//long ilUrno = m_DragDropTarget.GetDataDWord(0);
		//GHDDATA *prlGhd = ogGhdData.GetJobByUrno(ilUrno);
		//if(prlGhd!= NULL)
		//{
		//	if(strcmp(prlGhd->Dtyp, "D") == 0)
		//	{
		//		return 0;
		//	}
		//	else
		//	{
		//		return -1L;
		//	}
		//}
		//TO DO: Pr�fen, ob DTY OK
		return 0;	// cannot interpret this object
	}

	return -1L;
}

LONG CicNoDemandTableDlg::OnDrop(UINT wParam, LONG lParam)
{
	CCSDragDropCtrl *pomDragDropCtrl = &omDragDropObject;

	DROPEFFECT lpDropEffect = wParam;

	switch (pomDragDropCtrl->GetDataClass())
	{
	case DIT_CCA_GANTT: // Drop from this gantt to another point
		return ProcessDropCcaDuty(pomDragDropCtrl, lpDropEffect);
	}
    return -1L;

}

LONG CicNoDemandTableDlg::ProcessDropCcaDuty(CCSDragDropCtrl *popDragDropCtrl, DROPEFFECT lpDropEffect)
{
	long ilUrno = popDragDropCtrl->GetDataDWord(0);
	DIACCADATA *prlCca = ogCcaDiaFlightData.omCcaData.GetCcaByUrno(ilUrno);
	if(prlCca != NULL)
	{
		ogDataSet.DeAssignCca(prlCca->Urno);
	}
	return 0L;
}



LONG CicNoDemandTableDlg::OnTableReturnPressed( UINT wParam, LPARAM lParam)
{
	OnTableLButtonDblclk(wParam, lParam); 
	return 0L;
}

LONG CicNoDemandTableDlg::OnTableSelChanged( UINT wParam, LPARAM lParam)
{
    int ilLineNo = pomTable->pomListBox->GetCurSel();    
	
	CICNODEMANDTABLE_LINEDATA *prlTableLine = NULL;
	prlTableLine = (CICNODEMANDTABLE_LINEDATA *)pomTable->GetTextLineData(ilLineNo);
	if (prlTableLine->Urno != 0)
		CheckPostFlightCcaDia(prlTableLine->Urno,this);
	else
		CheckPostFlightCcaDia(0,this);
	
	return 0L;
}

int CicNoDemandTableDlg::GetCount()
{
	return omViewer.GetCount();
}
