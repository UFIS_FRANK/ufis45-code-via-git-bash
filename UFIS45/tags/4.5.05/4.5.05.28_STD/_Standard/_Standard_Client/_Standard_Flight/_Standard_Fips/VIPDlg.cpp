// VeryImpPers.cpp : implementation file
//

#include <stdafx.h>
#include <VIPDlg.h>
#include <resrc1.h>
#include <CCSGlobl.h>
#include <CedaBasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);



/////////////////////////////////////////////////////////////////////////////
// VIPDlg dialog
//---------------------------------------------------------------------------

VIPDlg::VIPDlg(CWnd* pParent, long lpFlnu, VIPDATA *prpVip)
	   :CDialog(VIPDlg::IDD, pParent)
{

	//{{AFX_DATA_INIT(VIPDlg)

	//}}AFX_DATA_INIT

	omFlnu = lpFlnu;

	pomVip = prpVip;

	emStatus = RotationVipDlg::Status::UNKNOWN;

	ogDdx.Register(this, BCD_VIP_UPDATE, CString("ROTATIONDLG_VIP"), CString("Vip Update"), FlightTableCf);
	ogDdx.Register(this, BCD_VIP_DELETE, CString("ROTATIONDLG_VIP"), CString("Vip Delete"), FlightTableCf);
	ogDdx.Register(this, BCD_VIP_INSERT, CString("ROTATIONDLG_VIP"), CString("Vip Insert"), FlightTableCf);

}



VIPDlg::~VIPDlg()
{

	ogDdx.UnRegister(this, NOTUSED);


}

void VIPDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(VIPDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CdatD);
	DDX_Control(pDX, IDC_CDAT_T, m_CdatT);
	DDX_Control(pDX, IDC_LSTU_D, m_LstuD);
	DDX_Control(pDX, IDC_LSTU_T, m_LstuT);
	DDX_Control(pDX, IDC_PAXN, m_Paxn);
	DDX_Control(pDX, IDC_PAXT, m_Paxt);
	DDX_Control(pDX, IDC_SEAT, m_Seat);
	DDX_Control(pDX, IDC_REFR, m_Refr);
	DDX_Control(pDX, IDC_REFE, m_Refe);
	DDX_Control(pDX, IDC_PAXR, m_Paxr);
	DDX_Control(pDX, IDC_USEC, m_Usec);
	DDX_Control(pDX, IDC_USEU, m_Useu);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(VIPDlg, CDialog)
	//{{AFX_MSG_MAP(VIPDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// VIPDlg message handlers
//---------------------------------------------------------------------------

BOOL VIPDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CString olTemp;

	m_CdatD.SetBKColor(SILVER);
	m_CdatT.SetBKColor(SILVER);
	m_LstuD.SetBKColor(SILVER);
	m_LstuT.SetBKColor(SILVER);
	m_Usec.SetBKColor(SILVER);
	m_Useu.SetBKColor(SILVER);

	m_Paxn.SetTypeToString("X(64)",64,1);
	m_Paxn.SetTextErrColor(RED);
	m_Paxn.SetBKColor(YELLOW);

	m_Paxr.SetTypeToString("X(64)",64,0);
	m_Paxr.SetTextErrColor(RED);
	
	m_Refe.SetTypeToString("X(64)",64,0);
	m_Refe.SetTextErrColor(RED);

	m_Refr.SetTypeToString("X(64)",64,0);
	m_Refr.SetTextErrColor(RED);

	m_Paxt.SetTypeToString("X(64)",64,0);
	m_Paxt.SetTextErrColor(RED);

	m_Seat.SetFormat("x|#x|#x|#");
	m_Seat.SetTextLimit(0,3);
	m_Seat.SetTextErrColor(RED);


	UpdateData(false);
	InitDialog();
	InitialStatus();

	return TRUE;

}

//---------------------------------------------------------------------------

void VIPDlg::OnOK() 
{

	UpdateData(true);

	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_Paxt.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += GetString(ST_EINGABEFELD) + "Title\" " + GetString(ST_BADFORMAT);
	}
	if(m_Paxr.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += GetString(ST_EINGABEFELD) + "Remark\" " + GetString(ST_BADFORMAT);
	}

	if(m_Paxn.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += GetString(ST_EINGABEFELD) + "Name\" " + GetString(ST_BADFORMAT);
	}


	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		
		CString olPaxr;
		CString olPaxt;
		CString olPaxn;
		CString olSeat;
		CString olRefe;
		CString olRefr;
		CString olUser = CString(pcgUser);
		CTime olTime = CTime::GetCurrentTime();

		m_Paxr.GetWindowText(olPaxr);
		m_Paxt.GetWindowText(olPaxt);
		m_Paxn.GetWindowText(olPaxn);
		m_Refe.GetWindowText(olRefe);
		m_Refr.GetWindowText(olRefr);
		m_Seat.GetWindowText(olSeat);


		if(pomVip == NULL)
		{
			pomVip = new VIPDATA;
			pomVip->Flnu = omFlnu;
			pomVip->Cdat = olTime;
			strcpy(pomVip->Usec, olUser);
			strcpy(pomVip->Paxt, olPaxt);
			strcpy(pomVip->Paxn, olPaxn);
			strcpy(pomVip->Paxr, olPaxr);
			strcpy(pomVip->Refe, olRefe);
			strcpy(pomVip->Refr, olRefr);
			strcpy(pomVip->Seat, olSeat);
			pomVip->IsChanged = DATA_NEW;
			ogVipData.Save(pomVip);

		}
		else
		{
			pomVip->Flnu = omFlnu;
			pomVip->Lstu = olTime;
			strcpy(pomVip->Useu, olUser);
			strcpy(pomVip->Paxt, olPaxt);
			strcpy(pomVip->Paxn, olPaxn);
			strcpy(pomVip->Paxr, olPaxr);
			strcpy(pomVip->Refe, olRefe);
			strcpy(pomVip->Refr, olRefr);
			strcpy(pomVip->Seat, olSeat);
			pomVip->IsChanged = DATA_CHANGED;
			ogVipData.Save(pomVip);
		}

		if(bgDailyShowVIP)
		{
			int llCountA = 0;
			llCountA = ogVipData.GetCount(omFlnu);

			char alCount[4] = "";		
			ltoa(llCountA, alCount,3);
			ogRotationDlgFlights.UpdateFlight(omFlnu, "VCNT", alCount);
		}

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText, GetString(ST_HINWEIS), MB_ICONEXCLAMATION);
	}
}


void VIPDlg::OnCancel() 
{
	CDialog::OnCancel();
}


void VIPDlg::InitDialog()
{

	if (pomVip != NULL)
	{
		m_CdatD.SetInitText(pomVip->Cdat.Format("%d.%m.%Y"));
		m_CdatT.SetInitText(pomVip->Cdat.Format("%H:%M"));
		m_LstuD.SetInitText(pomVip->Lstu.Format("%d.%m.%Y"));
		m_LstuT.SetInitText(pomVip->Lstu.Format("%H:%M"));
		m_Usec.SetInitText(pomVip->Usec);
		m_Useu.SetInitText(pomVip->Useu);

		m_Paxr.SetInitText(pomVip->Paxr);
		m_Paxt.SetInitText(pomVip->Paxt);
		m_Refe.SetInitText(pomVip->Refe);
		m_Refr.SetInitText(pomVip->Refr);
		m_Paxn.SetInitText(pomVip->Paxn);
		m_Seat.SetInitText(pomVip->Seat);
	}

	InitialStatus();
}



static void FlightTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{

    VIPDlg *polDlg = (VIPDlg*)popInstance;

	if (ipDDXType == BCD_VIP_UPDATE || ipDDXType == BCD_VIP_DELETE || ipDDXType == BCD_VIP_INSERT)
		polDlg->InitDialog();

}

void VIPDlg::InitialStatus()
{
	CWnd* plWnd = NULL;
	switch (emStatus)
	{
		case RotationVipDlg::Status::UNKNOWN:
			InitialPostFlight(TRUE);
			break;
		case RotationVipDlg::Status::POSTFLIGHT:
			InitialPostFlight(FALSE);
			break;
		default:
			InitialPostFlight(TRUE);
			break;
	}
}

void VIPDlg::setStatus(RotationVipDlg::Status epStatus)
{
	emStatus = epStatus;
}

RotationVipDlg::Status VIPDlg::getStatus() const
{
	return emStatus;
}

void VIPDlg::InitialPostFlight(BOOL bpPostFlight)
{
	CWnd* plWnd = NULL;
	plWnd = GetDlgItem(IDC_PAXT);
	if (plWnd) 
		plWnd->EnableWindow(bpPostFlight);

	plWnd = GetDlgItem(IDC_PAXR);
	if (plWnd) 
		plWnd->EnableWindow(bpPostFlight);

	plWnd = GetDlgItem(IDC_PAXN);
	if (plWnd) 
		plWnd->EnableWindow(bpPostFlight);

	plWnd = GetDlgItem(IDC_SEAT);
	if (plWnd) 
		plWnd->EnableWindow(bpPostFlight);

	plWnd = GetDlgItem(IDC_REFE);
	if (plWnd) 
		plWnd->EnableWindow(bpPostFlight);

	plWnd = GetDlgItem(IDC_REFR);
	if (plWnd) 
		plWnd->EnableWindow(bpPostFlight);
}
