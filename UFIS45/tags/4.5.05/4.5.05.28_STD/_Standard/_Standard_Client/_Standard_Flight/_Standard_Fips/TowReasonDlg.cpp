// TowReasonDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <TowReasonDlg.h>
#include <PrivList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CTowReasonDlg 


CTowReasonDlg::CTowReasonDlg(CWnd* pParent /*=NULL*/) : CDialog(CTowReasonDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTowReasonDlg)
	//}}AFX_DATA_INIT
	m_oSetReasonFlag = "";
}


void CTowReasonDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTowReasonDlg)
	DDX_Control(pDX, IDC_RADIO_POSA,  m_CRB_Posa);
	DDX_Control(pDX, IDC_RADIO_POSD,  m_CRB_Posd);
	DDX_Control(pDX, IDC_RADIO_POSAD, m_CRB_Posad);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTowReasonDlg, CDialog)
	//{{AFX_MSG_MAP(CTowReasonDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CTowReasonDlg 

BOOL CTowReasonDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_CRB_Posa.EnableWindow(true);
	m_CRB_Posd.EnableWindow(true);
	m_CRB_Posad.EnableWindow(true);
	m_CRB_Posad.SetCheck(1);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}


void CTowReasonDlg::OnOK() 
{
	if(m_CRB_Posad.GetCheck() == 1 ) 
	{
		m_oSetReasonFlag = "TOWAD";
	}
	else if(m_CRB_Posd.GetCheck() == 1)
	{
		m_oSetReasonFlag = "TOWD";
	}
	else if(m_CRB_Posa.GetCheck() == 1)
	{
		m_oSetReasonFlag = "TOWA";
	}
	CDialog::OnOK();
}

void CTowReasonDlg::OnCancel() 
{
	// TODO: Zus�tzlichen Bereinigungscode hier einf�gen
	
	CDialog::OnCancel();
}

