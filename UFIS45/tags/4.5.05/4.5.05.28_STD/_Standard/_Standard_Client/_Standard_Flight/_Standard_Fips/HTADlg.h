#if !defined(AFX_HTADlg_H__89A8CE34_48BB_11D3_AB92_00001C019D0B__INCLUDED_)
#define AFX_HTADlg_H__89A8CE34_48BB_11D3_AB92_00001C019D0B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HTADlg.h : header file
//

#include <CCSTable.h>
#include <CCSEdit.h>
#include <resrc1.h>
#include <RotationDlgCedaFlightData.h>
#include <CedaHtaData.h>


/////////////////////////////////////////////////////////////////////////////
// HTADlg dialog

class HTADlg : public CDialog
{
// Construction
public:
	HTADlg(CWnd* pParent = NULL, HTADATA *prpHta = NULL, ROTATIONDLGFLIGHTDATA *prpFlight = NULL);
	~HTADlg();

	void InitDialog();

// Dialog Data
	//{{AFX_DATA(HTADlg)
	enum { IDD = IDD_HTA };
	CButton	m_OK;
	CCSEdit	m_CdatD;
	CCSEdit	m_CdatT;
	CCSEdit	m_LstuD;
	CCSEdit	m_LstuT;
	CCSEdit	m_Usec;
	CCSEdit	m_Useu;
	CCSEdit	m_HSNA;
	CCSEdit	m_HTYP;
	CCSEdit	m_REMA;
	CCSEdit	m_Agent_Name;
	CCSEdit	m_Type_Name;
	CCSEdit m_CE_SVC_Stdt;
	CCSEdit m_CE_SVC_Endt;
	CCSButtonCtrl m_CB_HSNA;
	CCSButtonCtrl m_CB_HTYP;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(HTADlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(HTADlg)
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnHsnalist();
	afx_msg void OnHtyplist();
	afx_msg LONG OnEditKillfocus( UINT wParam, LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	bool    bmInsert;

	ROTATIONDLGFLIGHTDATA *prmFlight;
	HTADATA *prmHta;

	CString m_SVC_Stdt;
	CString m_SVC_Endt;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HTADlg_H__89A8CE34_48BB_11D3_AB92_00001C019D0B__INCLUDED_)
