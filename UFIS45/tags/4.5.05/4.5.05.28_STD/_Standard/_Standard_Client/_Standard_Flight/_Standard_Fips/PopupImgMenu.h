// PopupImgMenu.h: interface for the CPopupImgMenu class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_POPUPIMGMENU_H__D85F5E92_5224_406E_B3C4_99B4484CB577__INCLUDED_)
#define AFX_POPUPIMGMENU_H__D85F5E92_5224_406E_B3C4_99B4484CB577__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <resrc1.h>
#include "ImageButton.h"
#include <CCSGlobl.h>
//#include <resource.h>
#include <resrc1.h>
#include <fpms.h>
#include <CedaBasicData.h>
#include <Utils.h>
#include <process.h>
#include <BasicData.h>
#include <PrivList.h>
/////////////////////////////////////////////////////////////////////////////
// CPopupImgMenu dialog

#define	SHOW_ARR_ONLY			0
#define SHOW_DEP_ONLY			1
#define SHOW_ARR_DEP			2


struct PIM_BTNDATA
{
	int		iconMsgMapId;
	char	pcrImgFileName[256];
	CImageButton iconBmpButton;
	PIM_BTNDATA(void)
	{
		iconMsgMapId = 0;
		pcrImgFileName[0] = 0;
	}
};

struct PIM_IMGDATA
{
	char	iconChar;
	char    name[30];
	
	PIM_BTNDATA	rmBtnData[1];

	PIM_IMGDATA(void)
	{ 
		iconChar		= ' ';
		name[0] = '\0';
	}

};
	

class CPopupImgMenu : public CDialog
{
// Construction 
public:
	CPopupImgMenu(CWnd* pParent = NULL);   // standard constructor
	~CPopupImgMenu();
	void SetImagePath( const char* pcpImgPath );
	void PostNcDestroy() ;

	bool ShowMenu( const int ipLeft, const int ipTop, const long lpArrUrno, const char* pspArrIconToShow, const long lpDepUrno, const char* pspDepIconToShow, const int ipMaxWidth );
	bool ShowMenu( const int ipLeft, const int ipTop, const char* lpArrUrno, const char* pspArrIconToShow, const char* lpDepUrno, const char* pspDepIconToShow, const int ipMaxWidth );
	bool HideMenu();
// Dialog Data
	//{{AFX_DATA(CPopupImgMenu)
	enum { IDD = IDD_POPUPIMGMENU };
	CString	m_FLINFO;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPopupImgMenu)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CPopupImgMenu)
	afx_msg void OnCheckIn();
	afx_msg void OnMeal();
	afx_msg void OnGate();
	afx_msg void OnSorting();
	afx_msg void OnCleaning();
	afx_msg void OnMeal_Arr();
	afx_msg void OnMeal_ArrHalf();
	afx_msg void OnMeal_Dep();
	afx_msg void OnMeal_DepHalf();	
	afx_msg LRESULT OnRightButtonDown( UINT wParam, LPARAM lParam);
	
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private :
	void InitAButton( PIM_IMGDATA* prIMGDATA, const char cpCodeChar, const char* pcpBtnName, 
								 const char* pcpImgFileName,
								 const int ipMsgMapId);
	//void ShowIcon( PIM_BTNDATA& rpBtnData, int &left, const int top,  bool bpShowHalf);
	void ShowIcon( PIM_IMGDATA& prImgData, int &left, const int top, int ipShowInd );
	void HideIcon(CImageButton& omBmpBtn, const int ipMsgMapId);
	void CreateIcon( CImageButton* omBmpBtn, int &left, const int top,  const char* pcpBitMapName, const char* pspName, const int ipMsgMapId, bool bpShowHalf );
	void CreateAllIcons();
	void LaunchFlightDataTool(const CString spMsg);

	PIM_IMGDATA omIMGDATA[20];
	char psmArrIconToShow[50];
	char psmDepIconToShow[50];
	int  imLeft,imTop,imMaxWidth;

	CWnd* omParent;
	//long omArrUrno,omDepUrno;
	char pcmArrUrno[32],pcmDepUrno[32];
	bool bmIsIconCreated;
	char pcmImgPath[512];
	int	 pmMaxIconsAvailable;
};

#endif // !defined(AFX_POPUPIMGMENU_H__D85F5E92_5224_406E_B3C4_99B4484CB577__INCLUDED_)
