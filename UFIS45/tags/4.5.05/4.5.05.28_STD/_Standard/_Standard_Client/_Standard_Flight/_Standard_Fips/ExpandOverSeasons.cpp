// ExpandOverSeasons.cpp : implementation file
//

#include "stdafx.h"
#include "ExpandOverSeasons.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ExpandOverSeasons

ExpandOverSeasons *ExpandOverSeasons::pomThis = NULL;

ExpandOverSeasons::ExpandOverSeasons()
{
	pomThis = this;
	long result = GetTimeZoneInformation(&this->omTZInfo);
}

ExpandOverSeasons::~ExpandOverSeasons()
{
	pomThis = NULL;
}

void ExpandOverSeasons::ExitApp()
{
	delete pomThis;
}

bool ExpandOverSeasons::Expand(CTime from,CTime to)
{
	if (pomThis == NULL)
	{
		pomThis = new ExpandOverSeasons();
	}


	int ilTZFrom = pomThis->GetTimeZone(from);
	int ilTZTo	 = pomThis->GetTimeZone(to);

	if (ilTZFrom == ilTZTo)
	{
		return true;
	}
	else
	{
		return false;
	}
}

int ExpandOverSeasons::GetTimeZone(CTime from)
{
	if (pomThis == NULL)
	{
		pomThis = new ExpandOverSeasons();
	}

	// check on season data
	if (pomThis->omTZInfo.StandardDate.wMonth == 0 || pomThis->omTZInfo.DaylightDate.wMonth == 0)
	{
		return TIME_ZONE_ID_STANDARD;
	}

	CTime olStandard;
	// check if day in month format (means wDayOfWeek = 0 (Sunday) 1 (Monday)..) and wDay = 1 (first day of month) .. wDay = 5 (last Day of Month)
	if (pomThis->omTZInfo.StandardDate.wYear == 0)
	{
		olStandard = pomThis->CalculateDayInMonth(from.GetYear(),pomThis->omTZInfo.StandardDate);
	}
	else
	{
		olStandard = CTime(pomThis->omTZInfo.StandardDate.wYear,pomThis->omTZInfo.StandardDate.wMonth,pomThis->omTZInfo.StandardDate.wDay,pomThis->omTZInfo.StandardDate.wHour,pomThis->omTZInfo.StandardDate.wMinute,pomThis->omTZInfo.StandardDate.wSecond,0);
	}


	CTime olDaylight;
	// check if day in month format (means wDayOfWeek = 0 (Sunday) 1 (Monday)..) and wDay = 1 (first day of month) .. wDay = 5 (last Day of Month)
	if (pomThis->omTZInfo.StandardDate.wYear == 0)
	{
		olDaylight = pomThis->CalculateDayInMonth(from.GetYear(),pomThis->omTZInfo.DaylightDate);
	}
	else
	{
		olDaylight = CTime(pomThis->omTZInfo.DaylightDate.wYear,pomThis->omTZInfo.DaylightDate.wMonth,pomThis->omTZInfo.DaylightDate.wDay,pomThis->omTZInfo.DaylightDate.wHour,pomThis->omTZInfo.DaylightDate.wMinute,pomThis->omTZInfo.DaylightDate.wSecond,0);
	}

	if (from >= olDaylight && from < olStandard)
	{
		return TIME_ZONE_ID_DAYLIGHT;
	}
	else
	{
		return TIME_ZONE_ID_STANDARD;
	}
}


CTime	ExpandOverSeasons::CalculateDayInMonth(int year,const SYSTEMTIME &time)
{
	int match = 0;
	CTime olLast = 0;

	for (int i = 1; i <= 31; i++)
	{
		CTime olCurrent = CTime(year,time.wMonth,i,time.wHour,time.wMinute,time.wSecond,0);

		if (olCurrent <= 0)
			break;

		if (olCurrent.GetDayOfWeek() == time.wDayOfWeek + 1)
		{
			++match;
			if (match == time.wDay)
			{
				return olCurrent;
			}
			else
			{
				olLast = olCurrent;
			}
		}
	}

	return olLast;
}

