// CicAgentPeriod.h: interface for the CCicAgentPeriod class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CICAGENTPERIOD_H__FC0435A2_1063_4E78_A492_626BE44596B9__INCLUDED_)
#define AFX_CICAGENTPERIOD_H__FC0435A2_1063_4E78_A492_626BE44596B9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TimeRange.h"

// 050315 MVy: containts an occupation period of a checkin counter regarding to a handling agent

class CCicAgentPeriod : public CTimeRange
{
public:
	CCicAgentPeriod();
	CCicAgentPeriod( CTimeRange &range );
	virtual ~CCicAgentPeriod();

protected:
	long m_lAgentUrno ;
	char Days[8];
	long m_lCounterUrno ;
	CString m_sBarTitle ;		// the text you see in the bar

public:
	void SetPeriod( CTime start, CTime end );
	void SetPeriod( CTimeRange &range );
	void SetDays( CString days );
	void SetTitle( CString sTitle );
	CString* GetTitle();
	void SetAgentUrno( long lUrno );
	long GetAgentUrno();
	bool IsAgentUrno( long lUrno );
	void SetCounterUrno( long lUrno );
	long GetCounterUrno();
	bool IsCounterUrno( long lUrno );

	CString Dump();		// give some infos for debugging

};	// CCicAgentPeriod

#endif // !defined(AFX_CICAGENTPERIOD_H__FC0435A2_1063_4E78_A492_626BE44596B9__INCLUDED_)
