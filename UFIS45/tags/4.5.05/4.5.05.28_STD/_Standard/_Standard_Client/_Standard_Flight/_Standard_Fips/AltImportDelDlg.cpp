// AltImportDelDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <fpms.h>
#include <AltImportDelDlg.h>
#include <CCSTime.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CAltImportDelDlg 


CAltImportDelDlg::CAltImportDelDlg(CWnd* pParent /*=NULL*/, CTime* poMinDate, CTime* poMaxDate, int *piRet)
	: CDialog(CAltImportDelDlg::IDD, pParent)
{

	pomMinDate = poMinDate;
	pomMaxDate = poMaxDate;
	pimRet = piRet;

	CString oMinString = poMinDate->Format( "%d.%m.%Y" );
	CString oMaxString = poMaxDate->Format( "%d.%m.%Y" ); 

	// (oMinDate);
	//(oMaxDate);

	//{{AFX_DATA_INIT(CAltImportDelDlg)
	m_Datumbis = _T("");
	m_Datumvon = _T("");
	//}}AFX_DATA_INIT

	m_Datumvon = oMinString;
	m_Datumbis = oMaxString; 

}
		


void CAltImportDelDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAltImportDelDlg)
	DDX_Control(pDX, IDC_DATUMVON, m_CE_Datumvon);
	DDX_Control(pDX, IDC_DATUMBIS, m_CE_Datumbis);
	DDX_Text(pDX, IDC_DATUMBIS, m_Datumbis);
	DDX_Text(pDX, IDC_DATUMVON, m_Datumvon);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAltImportDelDlg, CDialog)
	//{{AFX_MSG_MAP(CAltImportDelDlg)
	ON_BN_CLICKED(IDONLYGEN, OnOnlygen)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CAltImportDelDlg 

void CAltImportDelDlg::OnOK() 
{
	m_CE_Datumvon.GetWindowText(m_Datumvon);
	m_CE_Datumbis.GetWindowText(m_Datumbis);
	*pomMinDate = DateStringToDate(m_Datumvon);
	*pomMaxDate = DateStringToDate(m_Datumbis); 
	*pimRet = 2;
	// Zus�tzliche Pr�fung
	if ((*pomMinDate != -1) && (*pomMaxDate != -1))
		CDialog::OnOK();

}

void CAltImportDelDlg::OnCancel() 
{
	// TODO: Zus�tzlichen Bereinigungscode hier einf�gen
	*pimRet = 0;
	CDialog::OnCancel();
}

BOOL CAltImportDelDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Zus�tzliche Initialisierung hier einf�gen
	m_CE_Datumvon.SetWindowText(m_Datumvon);	//oMinString
	m_CE_Datumbis.SetWindowText(m_Datumbis);	//oMaxString
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void CAltImportDelDlg::OnOnlygen() 
{
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	m_CE_Datumvon.GetWindowText(m_Datumvon);
	m_CE_Datumbis.GetWindowText(m_Datumbis);
	*pomMinDate = DateStringToDate(m_Datumvon);
	*pomMaxDate = DateStringToDate(m_Datumbis); 
	*pimRet = 1;
	// Zus�tzliche Pr�fung
	if ((*pomMinDate != -1) && (*pomMaxDate != -1))
		CDialog::OnOK();
	
}
