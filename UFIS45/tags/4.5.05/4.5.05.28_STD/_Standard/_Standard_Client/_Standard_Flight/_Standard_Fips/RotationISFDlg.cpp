// RotationISFDlg.cpp : implementation file
//

#include <stdafx.h>
#include <RotationISFDlg.h>
#include <PrivList.h>
#include <RotationRegnDlg.h>
#include <RotationDlgCedaFlightData.h>
#include <RotationAltDlg.h>
#include <RotationActDlg.h>
#include <RotationAptDlg.h>
#include <Fpms.h>
#include <Utils.h>

#include <CedaBasicData.h>
#include <AwBasicDataDlg.h>

#include <resource.h>
#include <resrc1.h>



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void IsfCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName);



/////////////////////////////////////////////////////////////////////////////
// RotationISFDlg dialog


RotationISFDlg::RotationISFDlg(CWnd* pParent /*=NULL*/)
	: CDialog(RotationISFDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(RotationISFDlg)
	m_Act5 = _T("");
	m_OrgDes = _T("");
	m_Regn = _T("");
	m_Sto = _T("");
	m_Alc = _T("");
	m_Flns = _T("");
	m_Fltn = _T("");
	m_Act3 = _T("");
	m_StoDate = _T("");
	m_Ttyp = _T("");
	m_Stev = _T("");
	m_Pos = _T("");
	m_Csgn = _T("");
	m_check1 = FALSE;
	m_Styp = _T("");
	//}}AFX_DATA_INIT
	bmCsgnUser = false;


    ogDdx.Register(this, APP_EXIT, CString("ISFDLG"), CString("Appli. exit"), IsfCf);

}


RotationISFDlg::~RotationISFDlg()
{
	ogDdx.UnRegister(this,NOTUSED);
}


static void IsfCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    RotationISFDlg *polDlg = (RotationISFDlg *)popInstance;

    if (ipDDXType == APP_EXIT)
        polDlg->AppExit();

}


void RotationISFDlg::AppExit()
{
	EndDialog(IDCANCEL);
}


void RotationISFDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(RotationISFDlg)
	DDX_Control(pDX, IDC_CSGN, m_CE_Csgn);
	DDX_Control(pDX, IDC_TTYP, m_CE_Ttyp);
	DDX_Control(pDX, IDC_STO_DATE, m_CE_StoDate);
	DDX_Control(pDX, IDC_ACT3, m_CE_Act3);
	DDX_Control(pDX, IDC_ORGORDES, m_CR_OrgOrDes);
	DDX_Control(pDX, IDC_ORGORDES2, m_CR_OrgOrDes2);
	DDX_Control(pDX, IDC_FLTN, m_CE_Fltn);
	DDX_Control(pDX, IDC_FLNS, m_CE_Flns);
	DDX_Control(pDX, IDC_ALC, m_CE_Alc);
	DDX_Control(pDX, IDC_STO, m_CE_Sto);
	DDX_Control(pDX, IDC_REGN, m_CE_Regn);
	DDX_Control(pDX, IDC_ORGDES, m_CE_OrgDes);
	DDX_Control(pDX, IDC_ACT5, m_CE_Act5);
	DDX_Control(pDX, IDC_STEV, m_CE_Stev);
	DDX_Text(pDX, IDC_STEV, m_Stev);
	DDX_Text(pDX, IDC_ACT5, m_Act5);
	DDX_Text(pDX, IDC_ORGDES, m_OrgDes);
	DDX_Text(pDX, IDC_REGN, m_Regn);
	DDX_Text(pDX, IDC_STO, m_Sto);
	DDX_Text(pDX, IDC_ALC, m_Alc);
	DDX_Text(pDX, IDC_FLNS, m_Flns);
	DDX_Text(pDX, IDC_FLTN, m_Fltn);
	DDX_Text(pDX, IDC_ACT3, m_Act3);
	DDX_Text(pDX, IDC_STO_DATE, m_StoDate);
	DDX_Text(pDX, IDC_TTYP, m_Ttyp);
	DDX_Control(pDX, IDC_RADIO_VFR, m_CR_VFR);
	DDX_Control(pDX, IDC_RADIO_IFR, m_CR_IFR);
	DDX_Control(pDX, IDC_POS, m_CE_Pos);
	DDX_Text(pDX, IDC_POS, m_Pos);
	DDX_Text(pDX, IDC_CSGN, m_Csgn);
	DDX_Check(pDX, IDC_CHECK_VFR, m_check1);
	DDX_Control(pDX, IDC_STYP, m_CE_Styp);
	DDX_Text(pDX, IDC_STYP, m_Styp);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(RotationISFDlg, CDialog)
	//{{AFX_MSG_MAP(RotationISFDlg)
	ON_BN_CLICKED(IDC_ALC3LIST, OnAlc3list)
	ON_BN_CLICKED(IDC_ACT35LIST, OnAct35list)
	ON_BN_CLICKED(IDC_ORGDESLIST, OnOrgdeslist)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnEditKillfocus)
	ON_BN_CLICKED(IDC_POSLIST, OnPosList)
	ON_MESSAGE(WM_EDIT_CHANGED, OnEditChanged)
	ON_BN_CLICKED(IDC_TTYPLIST, OnTtyplist)
	ON_BN_CLICKED(IDC_STYPLIST, OnStyplist)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// RotationISFDlg message handlers

void RotationISFDlg::OnOK() 
{
	
	UpdateData(TRUE);

	CWnd *polButtonOK = (CWnd *)GetDlgItem(IDOK);
	if (polButtonOK)
		polButtonOK->SetFocus();

	ROTATIONDLGFLIGHTDATA *prlFlight = new ROTATIONDLGFLIGHTDATA;

	CCSPtrArray<CCADATA> olCins;

	CString olMess;
	
	CTime	olSto = DateHourStringToDate(m_StoDate, m_Sto);

	if(bgDailyLocal)
		ogBasicData.LocalToUtc(olSto);

	if((m_CR_OrgOrDes.GetCheck() == 0) && (m_CR_OrgOrDes2.GetCheck() == 0))
		olMess += GetString(IDS_STRING1250);//"Selektion Ankunft/Abflug fehlt!\n"; 
		

//	if(!m_CE_Ttyp.GetStatus() || m_Ttyp.IsEmpty())
//		olMess += GetString(IDS_STRING360) + CString("\n");//"Verkehrsart!\n"; 

	if(olSto == TIMENULL)
		olMess += GetString(IDS_STRING1243) + CString("\n");//"Ungültiges Datum/Zeit\n"; 

	CString olSeason;
	if (!ogBCD.GetFieldBetween("SEA", "VPFR", "VPTO", CTimeToDBString(olSto, TIMENULL), "SEAS", olSeason ))
	{
		olMess += GetString(IDS_STRING2175) + CString("\n");//"Saison\n"; 
	}

	m_Act5.TrimRight();
	m_Act3.TrimRight();
	if((!m_CE_Act5.GetStatus() || m_Act5.IsEmpty()) || (!m_CE_Act3.GetStatus()))// || m_Act3.IsEmpty()))
		olMess += GetString(IDS_STRING1244) + CString("\n");//"A/C Typ nicht in StammDaten erfasst!\n"; 

	m_Alc.TrimRight();
	if(!m_CE_Alc.GetStatus() && !m_Alc.IsEmpty())
		olMess += GetString(IDS_STRING1245) + CString("\n");//"Fluggesellschaft nicht in Stammdaten erfasst!\n"; 

	m_OrgDes.TrimRight();
	if(!m_CE_OrgDes.GetStatus() || m_OrgDes.IsEmpty())
		olMess += GetString(IDS_STRING1251) + CString("\n");//"Flughafen nicht in Stammdaten erfasst!\n"; 

	m_Regn.TrimLeft();
	m_Regn.TrimRight();
	if(!m_CE_Regn.GetStatus() && !m_Regn.IsEmpty())
		olMess += GetString(IDS_STRING1252) + CString("\n");//"LFZ-Kennzeichen nicht in Stammdaten erfasst!\n"; 

	CString olFlno = ogRotationDlgFlights.CreateFlno(m_Alc, m_Fltn, m_Flns); 
	if (olFlno.IsEmpty() && m_Csgn.IsEmpty())
		olMess += GetString(IDS_FLNO_OR_CSGN) + CString("\n");//"Flightnumber or Callsign must be filled!\n"; 


	
		if(bgNatureAutoCalc)
		{
			if(!m_CE_Styp.GetStatus() && !m_Styp.IsEmpty() )
			olMess += GetString(IDS_STRING1008) + CString("\n");//"Verkehrsart!\n"; 
		}
		else
		{
			if(!m_CE_Ttyp.GetStatus() || m_Ttyp.IsEmpty() )
			olMess += GetString(IDS_STRING2274) + CString("\n");//"Verkehrsart!\n"; 

		}



	if(!m_CE_Pos.GetStatus())
		olMess += GetString(IDS_STRING361) + CString("\n");//"Postionsbezeichnung\n";

//	if(!m_CE_Styp.GetStatus() || m_Styp.IsEmpty())
	if(!m_CE_Styp.GetStatus())
		olMess += GetString(IDS_STRING2610) + CString("\n");//"Service Type!\n"; 

	if(!olMess.IsEmpty())
	{
		MessageBox(olMess, GetString(ST_FEHLER)); 
		delete prlFlight;
		return;
	}

	char pclDay[3];
	GetDayOfWeek(olSto, pclDay);

	strcpy(prlFlight->Flno, olFlno);

	strcpy(prlFlight->Styp, m_Styp);
	strcpy(prlFlight->Ttyp, m_Ttyp);
	strcpy(prlFlight->Stev, m_Stev);
	strcpy(prlFlight->Act3, m_Act3);
	strcpy(prlFlight->Act5, m_Act5);
	strcpy(prlFlight->Fltn, m_Fltn);
	strcpy(prlFlight->Flns, m_Flns);
	strcpy(prlFlight->Ftyp, "O");
	strcpy(prlFlight->Vian, "0");

	strcpy(prlFlight->Alc3, "");
	strcpy(prlFlight->Alc2, "");

	strcpy(prlFlight->Csgn, m_Csgn);

	if(m_Alc.GetLength() > 1)
	{
		CString olTmp;
		bool blRet = false;
		
		CString olAAlc3;
		CString olAAlc2;

		CString olStoStr = 	CTimeToDBString( olSto, olSto);
		
		blRet = ogBCD.GetField("ALT", "ALC2", "ALC3", m_Alc, olAAlc2, olAAlc3, olStoStr );

		strcpy(prlFlight->Alc3, olAAlc3);
		strcpy(prlFlight->Alc2, olAAlc2);
	}


	strcpy(prlFlight->Regn, m_Regn);

	//anflugverfahren : false == IFR (" "), true==VFR ("V")
	CString olFR = " ";
	if(m_check1)
	{
		olFR = "V";
	}


	if(m_CR_OrgOrDes.GetCheck() == TRUE)
	{
		strcpy(prlFlight->Ifra, olFR);
		strcpy(prlFlight->Psta, m_Pos);

		prlFlight->Stoa = olSto;

		if(omOrgDes3 == CString(pcgHome))
			prlFlight->Stod = olSto;

		strcpy(prlFlight->Org4, omOrgDes4);
		strcpy(prlFlight->Org3, omOrgDes3);
		strcpy(prlFlight->Des3, pcgHome);
		strcpy(prlFlight->Des4, pcgHome4);

		CTime olTime = prlFlight->Stoa;
		if (bgDailyLocal)
			UtcToLocal(olTime);

		CString olText = IsDupFlight(0, "A", prlFlight->Alc3, prlFlight->Fltn, prlFlight->Flns, olTime, bgDailyLocal, true);
		//olText = ogRotationDlgFlights.CheckDupFlights(olSto, olSto, CString(pclDay), CString(pclDay), CString("1"), olSto, olSto, prlFlight, NULL, olCins);

		if( !olText.IsEmpty())
		{
			if(strcmp(pcgHome, "DXB") == 0)
			{
				MessageBox(olText, GetString(IDS_WARNING), MB_OK); 
				if (prlFlight)
					delete prlFlight;
				return;
			}
			else
			{
				if(MessageBox(olText, GetString(IDS_WARNING), MB_YESNO) == IDYES)
					ogRotationDlgFlights.AddSeasonFlights(olSto, olSto, CString(pclDay),CString(pclDay), CString("1"), olSto, olSto, prlFlight, NULL, olCins);
				else
				{
					if (prlFlight)
						delete prlFlight;
					return;
				}
			}
		}
		else
			ogRotationDlgFlights.AddSeasonFlights(olSto, olSto, CString(pclDay),CString(pclDay), CString("1"), olSto, olSto, prlFlight, NULL, olCins);
	}
	else
	{
		strcpy(prlFlight->Ifrd, olFR);
		strcpy(prlFlight->Pstd, m_Pos);

		prlFlight->Stod = olSto;

		if(omOrgDes3 == CString(pcgHome))
			prlFlight->Stoa = olSto;
		
		strcpy(prlFlight->Des4, omOrgDes4);
		strcpy(prlFlight->Des3, omOrgDes3);
		strcpy(prlFlight->Org3, pcgHome);
		strcpy(prlFlight->Org4, pcgHome4);

		CTime olTime = prlFlight->Stod;
		if (bgDailyLocal)
			UtcToLocal(olTime);

		CString olText = IsDupFlight(0, "D", prlFlight->Alc3, prlFlight->Fltn, prlFlight->Flns, olTime, bgDailyLocal, true);
//		olText = ogRotationDlgFlights.CheckDupFlights(olSto, olSto, CString(pclDay), CString(pclDay), CString("1"), olSto, olSto, NULL, prlFlight, olCins);

		if( !olText.IsEmpty())
		{
			if(strcmp(pcgHome, "DXB") == 0)
			{
				MessageBox(olText, GetString(IDS_WARNING), MB_OK); 
				if (prlFlight)
					delete prlFlight;
				return;
			}
			else
			{
				if(MessageBox(olText, GetString(IDS_WARNING), MB_YESNO) == IDYES)
					ogRotationDlgFlights.AddSeasonFlights(olSto, olSto, CString(pclDay),CString(pclDay), CString("1"), olSto, olSto, NULL, prlFlight, olCins);
				else
				{
					if (prlFlight)
						delete prlFlight;
					return;
				}
			}
		}
		else
			ogRotationDlgFlights.AddSeasonFlights(olSto, olSto, CString(pclDay),CString(pclDay), CString("1"), olSto, olSto, NULL, prlFlight, olCins);
	}

	if (prlFlight)
		delete prlFlight;

	CDialog::OnOK();
}

void RotationISFDlg::OnCancel() 
{
	CDialog::OnCancel();
}

BOOL RotationISFDlg::OnInitDialog() 
{
	bmActSelect = true;
	bmCheckRegn = true;
	bmCsgnUser = false;
	CDialog::OnInitDialog();
//	m_CR_VFR.SetCheck(TRUE);	
//	m_CR_IFR.SetCheck(FALSE);	

	//anflugverfahren : false == IFR , true==VFR
	m_check1 = FALSE;	
	
	m_CR_OrgOrDes.SetCheck(FALSE);	
	m_CR_OrgOrDes2.SetCheck(FALSE);	

	m_CE_Sto.SetBKColor(YELLOW);
	/* //UFIS-1097 - Commented Out - Start
	if(bgNatureAutoCalc)
		m_CE_Styp.SetBKColor(YELLOW);
	else
		m_CE_Ttyp.SetBKColor(YELLOW);
	*/ //UFIS-1097 - Commented Out - End

	if(bgNatureServiceMand)
	{	
		m_CE_Styp.SetBKColor(YELLOW);
		m_CE_Ttyp.SetBKColor(YELLOW);
	}
	else if(bgNatureAutoCalc)//UFIS-1097 - Start
	{
		m_CE_Styp.SetBKColor(YELLOW);
	}
	else
	{
		if (bgServiceMandatory)
		{
			m_CE_Styp.SetBKColor(YELLOW);
		}
		if (bgNatureMandatory)
		{
			m_CE_Ttyp.SetBKColor(YELLOW);
		}		
	}//UFIS-1097 - End

	m_CE_StoDate.SetBKColor(YELLOW);
	m_CE_Sto.SetTypeToTime();
	m_CE_StoDate.SetTypeToDate();
	m_CE_Act5.SetTypeToString("XXXXX",5,0);
	m_CE_Act5.SetBKColor(YELLOW);
	m_CE_Act3.SetTypeToString("XXX",3,0);
//	m_CE_Act3.SetBKColor(YELLOW);
	m_CE_Regn.SetTypeToString("XXXXXXXXXXXX",12,0);
	m_CE_OrgDes.SetTypeToString("XXXX",4,0);
	m_CE_OrgDes.SetBKColor(YELLOW);

	m_CE_Alc.SetTypeToString("X|#X|#X|#",3,2);
	m_CE_Fltn.SetTypeToString("#####",5,2);
	m_CE_Flns.SetTypeToString("X",1,0);

	m_CE_Stev.SetTypeToString("X",1,0);
	m_CE_Pos.SetTypeToString("x|#x|#x|#x|#x|#",5,0);

	/*  //UFIS-1097 - Commented out - Start
	if(bgNatureAutoCalc)
	{
		m_CE_Ttyp.SetTypeToString("X|#X|#X|#X|#X|#",5,0);
		m_CE_Styp.SetTypeToString("X|#X|#X|#X|#X|#",5,1);
	}
	else
	{
		m_CE_Ttyp.SetTypeToString("X|#X|#X|#X|#X|#",5,1);
		m_CE_Styp.SetTypeToString("X|#X|#X|#X|#X|#",5,0);
	}
	*/  //UFIS-1097 - Commented out - End

	if(bgNatureServiceMand)
	{	
		m_CE_Styp.SetTypeToString("X|#X|#X|#X|#X|#",5,1);
		m_CE_Ttyp.SetTypeToString("X|#X|#X|#X|#X|#",5,1);
	}
	else if(bgNatureAutoCalc)//UFIS-1097 - Start
	{
		m_CE_Ttyp.SetTypeToString("X|#X|#X|#X|#X|#",5,0);
		m_CE_Styp.SetTypeToString("X|#X|#X|#X|#X|#",5,1);
	}
	else
	{
		m_CE_Ttyp.SetTypeToString("X|#X|#X|#X|#X|#",5,0);
		m_CE_Styp.SetTypeToString("X|#X|#X|#X|#X|#",5,0);
		if (bgServiceMandatory)
		{
			m_CE_Styp.SetTypeToString("X|#X|#X|#X|#X|#",5,1);
		}
		if (bgNatureMandatory)
		{
			m_CE_Ttyp.SetTypeToString("X|#X|#X|#X|#X|#",5,1);
		}		
	}//UFIS-1097 - End


	CTime olCurUtc = CTime::GetCurrentTime();
	
	if(!bgDailyLocal)
		ogBasicData.LocalToUtc(olCurUtc);

	m_CE_Sto.SetInitText(olCurUtc.Format("%H:%M"));
	m_CE_StoDate.SetInitText(olCurUtc.Format("%d.%m.%Y"));
	
	m_CE_Csgn.SetTypeToString("X|#X|#X|#X|#X|#X|#X|#X|#",8,0);

	CPoint point;
	::GetCursorPos(&point);
	this->SetWindowPos(&wndTop, point.x, point.y,0,0, SWP_SHOWWINDOW | SWP_NOSIZE);// | SWP_NOMOVE );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void RotationISFDlg::OnAlc3list() 
{
	CString olText;
	m_CE_Alc.GetWindowText(olText);

	CString olField = "ALC2,ALC3,ALFN";
	CString olSort  = "ALC2+,ALC3+,ALFN+";

	int ilLen = olText.GetLength();
	if (ilLen > 2)
	{
		olField = "ALC3,ALC2,ALFN";
		olSort  = "ALC3+,ALC2+,ALFN+";
	}


	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT",olField, olSort, olText);
//	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALFN+,ALC2+");
	if(polDlg->DoModal() == IDOK)
	{
		if(polDlg->GetField("ALC2").IsEmpty())
			m_CE_Alc.SetInitText(polDlg->GetField("ALC3"), true);	
		else
			m_CE_Alc.SetInitText(polDlg->GetField("ALC2"), true);	
	}
	delete polDlg;
}


void RotationISFDlg::OnTtyplist() 
{
	CString olText;
	m_CE_Ttyp.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "NAT","TTYP,TNAM", "TNAM+", olText);
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_Ttyp.SetInitText(polDlg->GetField("TTYP"), true);	
	}
	delete polDlg;
}

void RotationISFDlg::OnStyplist() 
{
	CString olText;
	m_CE_Styp.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "STY","STYP,SNAM", "SNAM+", olText);
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_Styp.SetInitText(polDlg->GetField("STYP"), true);	
	}
	delete polDlg;
}
/*
void RotationISFDlg::OnAct35list() 
{
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ACT","ACT3,ACT5,ACFN", "ACFN+,ACT3+");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_Act3.SetInitText(polDlg->GetField("ACT3"), true);	
		m_CE_Act5.SetInitText(polDlg->GetField("ACT5"), true);	
	}
	delete polDlg;
}
*/

void RotationISFDlg::OnOrgdeslist() 
{
	CString olText;
	m_CE_OrgDes.GetWindowText(olText);

	CString olField = "APC3,APC4,APFN";
	CString olSort  = "APC3+,APC4+,APFN+";

	int ilLen = olText.GetLength();
	if (ilLen > 3)
	{
		olField = "APC4,APC3,APFN";
		olSort  = "APC4+,APC3+,APFN+";
	}


	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "APT",olField, olSort, olText);
//	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "APT","APC3,APC4,APFN", "APFN+");
	if(polDlg->DoModal() == IDOK)
	{
		omOrgDes4 = polDlg->GetField("APC4");
		omOrgDes3 = polDlg->GetField("APC3");
		m_CE_OrgDes.SetInitText(omOrgDes4 , true);	
	}
	delete polDlg;
}

void RotationISFDlg::OnPosList() 
{

	CCS_TRY

	CString olText;
	m_CE_Pos.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "PST","PNAM", "PNAM+", olText);
//	polDlg->SetSecState("SEASONDLG_CE_APsta");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_Pos.SetInitText(polDlg->GetField("PNAM"), true);	
		m_CE_Pos.SetFocus();
	}
	delete polDlg;

	CCS_CATCH_ALL
}

LONG RotationISFDlg::OnEditKillfocus( UINT wParam, LPARAM lParam)
{
	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*)lParam;
	prlNotify->Text.TrimRight();
//	if(prlNotify->Text.IsEmpty())
//		return 0L;

	if(!(UINT)m_CE_Act5.imID == wParam && !(UINT)m_CE_Act5.imID == wParam)
	{
		if(prlNotify->Text.IsEmpty())
			return 0L;
	}

	CString olTmp;

	if((UINT)m_CE_OrgDes.imID == wParam)
	{
		CString olTmp;
		prlNotify->UserStatus = true;
		omOrgDes3 = "";
		omOrgDes4 = "";

		if(prlNotify->Text.GetLength() == 4)
		{
			if(prlNotify->Status = ogBCD.GetField("APT", "APC4", prlNotify->Text, "APC3", olTmp ))
			{
				omOrgDes3 = olTmp;
				omOrgDes4 = prlNotify->Text;
			}
		}
		else
		{
			if(prlNotify->Status = ogBCD.GetField("APT", "APC3", prlNotify->Text, "APC4", olTmp ))
			{
				omOrgDes4 = olTmp;
				omOrgDes3 = prlNotify->Text;
			}
		}

		if(!prlNotify->Status)
		{
			RotationAptDlg olDlg(this, prlNotify->Text);
			if(olDlg.DoModal() == IDOK)
			{
				omOrgDes4 = olDlg.m_Apc4;
				omOrgDes3 = olDlg.m_Apc3;
				prlNotify->Status = true;
			}

		}
		m_CE_OrgDes.SetInitText(omOrgDes4, true);
		
		return 0L;
	}

	if((UINT)m_CE_Alc.imID == wParam)
	{
		CString olTmp;
		prlNotify->UserStatus = true;
		if(prlNotify->Text.GetLength() == 3)
			prlNotify->Status = ogBCD.GetField("ALT", "ALC3", prlNotify->Text, "ALC2", olTmp );
		else
			prlNotify->Status = ogBCD.GetField("ALT", "ALC2", prlNotify->Text, "ALC3", olTmp );

		if(!prlNotify->Status)
		{

			RotationAltDlg olDlg(this, prlNotify->Text);
			if(olDlg.DoModal() == IDOK)
			{
				prlNotify->Status = true;
				if(olDlg.m_Alc2.IsEmpty())
					m_CE_Alc.SetInitText(olDlg.m_Alc3, true);
				else
					m_CE_Alc.SetInitText(olDlg.m_Alc2, true);
			}
		}

		if (!bmCsgnUser)
			m_CE_Csgn.SetInitText("", false);

		return 0L;
	}

	if((UINT)m_CE_Fltn.imID == wParam)
	{
		if (!bmCsgnUser)
			m_CE_Csgn.SetInitText("", false);

		return 0L;
	}

/*
	CString olRegn;
	m_CE_Regn.GetWindowText(olRegn);
	
	if((UINT)m_CE_Act5.imID == wParam)
	{
		CString olTmp;
		prlNotify->Status = ogBCD.GetField("ACT", "ACT5", prlNotify->Text, "ACT3", olTmp );
		prlNotify->UserStatus = true;

		if(prlNotify->Status == true && !prlNotify->Text.IsEmpty())
		{
			m_CE_Act3.SetInitText(olTmp, true);
		}

		if(!olRegn.IsEmpty())
		{

			
			CString olWhere;
			olRegn.TrimLeft();
			if(!olRegn.IsEmpty())
			{
				olWhere.Format("WHERE REGN = '%s'", olRegn);
				ogBCD.Read( "ACR", olWhere);
			}
			
			
			
			ogBCD.GetField("ACR", "REGN", olRegn, "ACT5", olTmp );
			if(olTmp != prlNotify->Text)
			{
				m_CE_Regn.SetStatus(false);
			}
		}
		
		return 0L;
	}

	if((UINT)m_CE_Act3.imID == wParam)
	{
		CString olTmp;
		prlNotify->Status = ogBCD.GetField("ACT", "ACT3", prlNotify->Text, "ACT5", olTmp );
		prlNotify->UserStatus = true;

		if(prlNotify->Status == true && !prlNotify->Text.IsEmpty())
		{
			m_CE_Act5.SetInitText(olTmp, true);
		}

		if(!olRegn.IsEmpty())
		{

			
			CString olWhere;
			olRegn.TrimLeft();
			if(!olRegn.IsEmpty())
			{
				olWhere.Format("WHERE REGN = '%s'", olRegn);
				ogBCD.Read( "ACR", olWhere);
			}
			
			
			ogBCD.GetField("ACR", "REGN", olRegn, "ACT3", olTmp );
			if(olTmp != prlNotify->Text)
			{
				m_CE_Regn.SetStatus(false);
			}
		}
		
		return 0L;
	}
*/
	if((UINT)m_CE_Pos.imID == wParam && !prlNotify->Text.IsEmpty())
	{
		CString olTmp;
		prlNotify->Status = ogBCD.GetField("PST", "PNAM", prlNotify->Text, "URNO", olTmp );
		prlNotify->UserStatus = true;

		if(prlNotify->Status == true && !prlNotify->Text.IsEmpty())
		{
			m_CE_Pos.SetInitText(prlNotify->Text, true);
		}
		
		return 0L;
	}
/*	
	if((UINT)m_CE_Regn.imID == wParam)
	{
		CString olAct3;
		CString olAct5;
		bool blRet = false;

		
		CString olWhere;
		prlNotify->Text.TrimLeft();
		if(!prlNotify->Text.IsEmpty())
		{
			olWhere.Format("WHERE REGN = '%s'", prlNotify->Text);
			ogBCD.Read( "ACR", olWhere);
		}
		
		
		
		blRet = ogBCD.GetField("ACR", "REGN", prlNotify->Text, "ACT3", olAct3 );
		blRet = ogBCD.GetField("ACR", "REGN", prlNotify->Text, "ACT5", olAct5 );

		if(blRet)
		{
			m_CE_Act5.SetInitText(olAct5, true);
			m_CE_Act3.SetInitText(olAct3, true);
		}
		else
		{
			CString	olRegn;
			m_CE_Regn.GetWindowText(olRegn);
			m_CE_Act3.GetWindowText(olAct3);
			m_CE_Act5.GetWindowText(olAct5);

			RotationRegnDlg olDlg(this, olRegn, olAct3, olAct5);
			if(olDlg.DoModal() == IDOK)
			{
				m_CE_Regn.SetInitText(olDlg.m_Regn, true);
				m_CE_Act5.SetInitText(olDlg.m_Act5, true);
				m_CE_Act3.SetInitText(olDlg.m_Act3, true);
				blRet = true;
			}
			else
				m_CE_Regn.SetInitText("", true);
		}

		//
		m_CE_Regn.GetWindowText(olRegn);
		m_CE_Alc.GetWindowText(m_Alc);
		m_CE_Fltn.GetWindowText(m_Fltn);
		m_CE_Flns.GetWindowText(m_Flns);
		CString olFlno = ogRotationDlgFlights.CreateFlno(m_Alc, m_Fltn, m_Flns); 
		CString olCsgn;
		m_CE_Csgn.GetWindowText(olCsgn);

		if (!olRegn.IsEmpty() && olCsgn.IsEmpty() && olFlno.IsEmpty() && !bmCsgnUser)
			m_CE_Csgn.SetInitText(olRegn, false);

		prlNotify->UserStatus = true;
		prlNotify->Status = blRet;
		return 0L;
	}
*/

	if(((UINT)m_CE_Act5.imID == wParam))
	{
		if (!bmActSelect)
		{
			prlNotify->UserStatus = true;

			CString olWhere;
			if (prlNotify->Text.IsEmpty())
				prlNotify->Text = " ";

			CString olTmp5 = prlNotify->Text;
			CString olTmp3;
			m_CE_Act3.GetWindowText(olTmp3);
			if (olTmp3.IsEmpty())
				olTmp3 = " ";

			CCSPtrArray<RecordSet> prlRecords;
			int ilCount = ogBCD.GetRecordsExt("ACT","ACT3","ACT5",olTmp3,olTmp5, &prlRecords);
			prlRecords.DeleteAll();

			if (ilCount != 1)
			{
				ilCount = ogBCD.GetRecordsExt("ACT","ACT5","HOPO",prlNotify->Text,CString(pcgHome), &prlRecords);
				prlRecords.DeleteAll();

				if(prlNotify->Status = ogBCD.GetField("ACT", "ACT5", prlNotify->Text, "ACT3", olTmp ))
				{
					m_CE_Act3.SetInitText(olTmp);
				}

				if (ilCount > 1)
					OnAct35list();
			}
		}
	}

	if(((UINT)m_CE_Act3.imID == wParam))
	{
		if (!bmActSelect)
		{
			prlNotify->UserStatus = true;

			CString olWhere;
			if (prlNotify->Text.IsEmpty())
				prlNotify->Text = " ";

			CString olTmp3 = prlNotify->Text;
			CString olTmp5;
			m_CE_Act5.GetWindowText(olTmp5);
			if (olTmp5.IsEmpty())
				olTmp5 = " ";

			CCSPtrArray<RecordSet> prlRecords;
			int ilCount = ogBCD.GetRecordsExt("ACT","ACT3","ACT5",olTmp3,olTmp5, &prlRecords);
			prlRecords.DeleteAll();

			if (ilCount != 1)
			{
				ilCount = ogBCD.GetRecordsExt("ACT","ACT3","HOPO",prlNotify->Text,CString(pcgHome), &prlRecords);
				prlRecords.DeleteAll();

				if(prlNotify->Status = ogBCD.GetField("ACT", "ACT3", prlNotify->Text, "ACT5", olTmp ))
				{
					m_CE_Act5.SetInitText(olTmp);
				}

				if (ilCount > 1)
				{
					OnAct3list();
				}
			}
		}
	}


	CString olRegn;
	m_CE_Regn.GetWindowText(olRegn);

	if( !olRegn.IsEmpty() && ( ((UINT)m_CE_Act5.imID == wParam) || ((UINT)m_CE_Act3.imID == wParam) ) && prlNotify->Status)
	{
		if (!bmCheckRegn)
			return 0L;

		bool blRet = false;
		CString olAct3;
		CString olAct5;

		CString olWhere;
		olRegn.TrimLeft();
		olRegn.TrimRight();
		if(!olRegn.IsEmpty())
		{
			olWhere.Format("WHERE REGN = '%s'", olRegn);
			ogBCD.Read( "ACR", olWhere);
		}
		else
			return 0L;
		

		if(ogBCD.GetFields("ACR", "REGN", olRegn, "ACT3", "ACT5", olAct3, olAct5))
		{

			CString olTmp3;
			CString olTmp5;
			m_CE_Act3.GetWindowText(olTmp3);
			m_CE_Act5.GetWindowText(olTmp5);

			if (olTmp3 == " ") olTmp3.Empty();
			if (olTmp5 == " ") olTmp5.Empty();
			if ( (olTmp3 != olAct3) || (olTmp5 != olAct5) )
			{
				bool blIsIn = true;

				if (olTmp3.IsEmpty() && olTmp5.IsEmpty())
					blIsIn = false;

				CString olWhere;
				if (olTmp3.IsEmpty())
					olTmp3 = " ";
				if (olTmp5.IsEmpty())
					olTmp5 = " ";

					CCSPtrArray<RecordSet> prlRecords;
					int ilCount = ogBCD.GetRecordsExt("ACT","ACT3","ACT5",olTmp3,olTmp5, &prlRecords);
					prlRecords.DeleteAll();

				if(!blIsIn || ilCount == 0)
				{
					CFPMSApp::MyTopmostMessageBox(this,GetString(IDS_STRING342), GetString(ST_FEHLER), MB_ICONWARNING);
					return 0L;
				}

				bmCheckRegn = false;

				if(ogPrivList.GetStat("REGN_AC_CHANGE") == '1' || ogPrivList.GetStat("REGN_AC_CHANGE") == ' ')
				{

					if(MessageBox(GetString(IDS_STRING945), GetString(ST_FRAGE), MB_YESNO) == IDYES)				
					{
						m_CE_Act3.GetWindowText(olAct3);
						m_CE_Act5.GetWindowText(olAct5);

						ogBCD.GetField("ACT", "ACT3","ACT5", prlNotify->Text, olAct3, olAct5);

						ogBCD.SetField("ACR", "REGN", olRegn, "ACT3", olAct3, false);
						ogBCD.SetField("ACR", "REGN", olRegn, "ACT5", olAct5, false);
						ogBCD.Save("ACR");
					}
					else
					{
						m_CE_Act3.SetInitText( olAct3, true);
						m_CE_Act5.SetInitText( olAct5, true);
					}
					bmActSelect = true;
				}
				m_CE_Act3.SetInitText( olAct3, true);
				m_CE_Act5.SetInitText( olAct5, true);

			}
		}
	}

	if(((UINT)m_CE_Regn.imID == wParam) && (prlNotify->Text.IsEmpty()))
	{

			m_CE_Act5.EnableWindow(TRUE);
			m_CE_Act3.EnableWindow(TRUE);
			GetDlgItem(IDC_ACT35LIST)->EnableWindow(TRUE);

	}

	if(((UINT)m_CE_Regn.imID == wParam) && (!prlNotify->Text.IsEmpty()))
	{
		CString olAct3;
		CString olAct5;
		prlNotify->Status = true;

		if((ogPrivList.GetStat("REGN_AC_CHANGE") != '1' && ogPrivList.GetStat("REGN_AC_CHANGE") != ' '))
		{
		
			m_CE_Act5.EnableWindow(FALSE);
			m_CE_Act3.EnableWindow(FALSE);
			GetDlgItem(IDC_ACT35LIST)->EnableWindow(FALSE);
		}
		else
		{
			m_CE_Act5.EnableWindow(TRUE);
			m_CE_Act3.EnableWindow(TRUE);
			GetDlgItem(IDC_ACT35LIST)->EnableWindow(TRUE);
		}


		CString olWhere;
		prlNotify->Text.TrimLeft();
		prlNotify->Text.TrimRight();
		if(!prlNotify->Text.IsEmpty())
		{
			olWhere.Format("WHERE REGN = '%s'", prlNotify->Text);
			ogBCD.Read( "ACR", olWhere);
		}
		else
			return 0L;
		
		if(prlNotify->Status = ogBCD.GetFields("ACR", "REGN", prlNotify->Text, "ACT3", "ACT5", olAct3, olAct5))
		{
			m_CE_Act3.SetInitText(olAct3, true);
			m_CE_Act5.SetInitText(olAct5, true);
		}
		else
		{
			m_CE_Act3.GetWindowText(olAct3);
			m_CE_Act5.GetWindowText(olAct5);

			if(ogPrivList.GetStat("REGN_INSERT") == '1' || ogPrivList.GetStat("REGN_INSERT") == ' ')
			{

				RotationRegnDlg olDlg(this, prlNotify->Text, olAct3, olAct5);
				if(olDlg.DoModal() == IDOK)
				{
					m_CE_Act5.SetInitText(olDlg.m_Act5, true);
					m_CE_Act3.SetInitText(olDlg.m_Act3, true);
					prlNotify->Status = true;
				}
				else
					m_CE_Regn.SetInitText("", true);
			}
			prlNotify->UserStatus = true;
			prlNotify->Status = false;
		}
		bmActSelect = true;
		return 0L;
	}

	if((UINT)m_CE_Styp.imID == wParam && !prlNotify->Text.IsEmpty()) 
	{
		CString olTmp;
		prlNotify->Status  = ogBCD.GetField("STY", "STYP", prlNotify->Text, "STYP", olTmp );
		prlNotify->UserStatus = true;

		if(prlNotify->Status == true && !prlNotify->Text.IsEmpty())
		{
			m_CE_Styp.SetInitText(prlNotify->Text, true);
		}
		
		return 0L;
	}	

	if((UINT)m_CE_Ttyp.imID == wParam)
	{
		CString olTmp;
		prlNotify->Status  = ogBCD.GetField("NAT", "TTYP", prlNotify->Text, "TTYP", olTmp );
		prlNotify->UserStatus = true;

		if(prlNotify->Status == true && !prlNotify->Text.IsEmpty())
		{
			m_CE_Ttyp.SetInitText(prlNotify->Text, true);
		}
		
		return 0L;
	}	

	if((UINT)m_CE_Csgn.imID == wParam)
	{
		prlNotify->UserStatus = true;
	}	
	return 0L;

}


BOOL RotationISFDlg::DestroyWindow() 
{
	BOOL blRet =  CDialog::DestroyWindow();
	return blRet;
}

LONG RotationISFDlg::OnEditChanged( UINT wParam, LPARAM lParam)
{
	if((UINT)m_CE_Act3.imID == wParam || (UINT)m_CE_Act5.imID == wParam)
	{
		bmActSelect = false;
		bmCheckRegn = true;
	}


	CString olTmp;

//	arr-gate1
	if((UINT)m_CE_Csgn.imID == wParam)
	{
		bmCsgnUser = true;
/*		m_CE_AGta1.GetWindowText(olTmp);

		CString olGate = prmAFlightSave->Gta1;

		if (prmAFlightSave->Ga1x != TIMENULL && !olGate.IsEmpty())
		{
			if (olTmp != olGate)
			{
				m_CE_AGta1.SetInitText(olGate);
				CFPMSApp::MyTopmostMessageBox(this, GetString(IDS_STRING_GATE_ISOPEN), GetString(ST_FEHLER), MB_OK | MB_ICONERROR);
			}
		}
*/
	}
	return 0L;
}

void RotationISFDlg::OnAct35list() 
{
	CString olText3;
	m_CE_Act3.GetWindowText(olText3);
	CString olText5;
	m_CE_Act5.GetWindowText(olText5);

	CCS_TRY
	
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ACT","ACT5,ACT3,ACFN", "ACT5+,ACT3,+ACFN+", olText5+","+olText3);

	if(polDlg->DoModal() == IDOK)
	{
		m_CE_Act3.SetInitText(polDlg->GetField("ACT3"), true);	
		m_CE_Act5.SetInitText(polDlg->GetField("ACT5"), true);	

		bmActSelect = true;
		bmCheckRegn = true;
	}
	delete polDlg;

	m_CE_Act3.SetFocus();

	CCS_CATCH_ALL	
}

void RotationISFDlg::OnAct3list() 
{
	CString olText3;
	m_CE_Act3.GetWindowText(olText3);
	CString olText5;
	m_CE_Act5.GetWindowText(olText5);

	CCS_TRY

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ACT","ACT3,ACT5,ACFN", "ACT3+,ACT5+,ACFN+", olText3+","+olText5);

	if(polDlg->DoModal() == IDOK)
	{
		m_CE_Act3.SetInitText(polDlg->GetField("ACT3"), true);	
		m_CE_Act5.SetInitText(polDlg->GetField("ACT5"), true);	

		bmActSelect = true;
		bmCheckRegn = true;
	}
	delete polDlg;

	CCS_CATCH_ALL	
}
