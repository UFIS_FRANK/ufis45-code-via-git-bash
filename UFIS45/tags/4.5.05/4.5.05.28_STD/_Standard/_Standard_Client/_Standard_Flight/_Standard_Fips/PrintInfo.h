#ifndef _PRINTINFO_H_
#define _PRINTINFO_H_

#include <CCSPtrArray.h>
#include <CedaInfData.h>

enum egPrintInfo
{
	PRINTINFO_LANDSCAPE,PRINTINFO_PORTRAIT,
	PRINTINFO_LEFT,PRINTINFO_RIGHT,PRINTINFO_CENTER,
	PRINTINFO_NOFRAME,PRINTINFO_FRAMETHIN,PRINTINFO_FRAMEMEDIUM,PRINTINFO_FRAMETHICK,
	PRINTINFO_SMALL,PRINTINFO_MEDIUM,PRINTINFO_LARGE,
	PRINTINFO_SMALLBOLD,PRINTINFO_MEDIUMBOLD,PRINTINFO_LARGEBOLD
};

//-----------------------------------------------------------------------------

class PrintInfo : public CObject
{
public:

// Functions
	PrintInfo(CString opInfoName,CWnd *opParent = NULL);
	~PrintInfo();
	void SetBitmaps(CBitmap *popBitmap = NULL);
	bool InitializePrinter();
	bool Print(INFDATA opInfo);

// Variables

private:
// Functions
	void PrintHeader();
	void PrintFooter();
	void PrintText();

// Variables
	CWnd *pomParent;

	CDC	 omCdc;
	CDC	 omMemDc;
	CDC	 omCcsMemDc;

	CRgn omRgn;

	CFont omCourierNew_Regular_8;
	CFont omCourierNew_Bold_8;
	CFont omArial_Regular_8;
	CFont omArial_Regular_10;
	CFont omArial_Regular_12;
	CFont omArial_Regular_18;
	CFont omArial_Bold_8;
	CFont omArial_Bold_10;
	CFont omArial_Bold_11;
	CFont omArial_Bold_12;
	CFont omArial_Bold_18;

    CPen omThinPen;
    CPen omMediumPen;
    CPen omThickPen;
	CPen omDottedPen;

	CString	omInfoName;

	CBitmap *pomBitmap;
	CBitmap omBitmap;

	INFDATA omInfo;
	CString omInfoText;

	double dmPages;
	int imLineHeight;
	int imFirstPos;
	int imActualPos;
	int imMaxLines;
	int	imActualLine;
	int imActualPage;
	int imLeftOffset;
	int imRightEnd;
	int	imOrientation;
	int	imLogPixelsY;
	int	imLogPixelsX;
	bool bmIsInitialized;


private:
	BYTE lmCharSet;
};

#endif

//-----------------------------------------------------------------------------


