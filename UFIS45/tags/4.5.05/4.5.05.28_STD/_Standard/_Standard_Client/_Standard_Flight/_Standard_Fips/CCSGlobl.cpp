// CCSGlobl.cpp: implementation of the CCSGlobl .
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <resource.h>
#include <CCSGlobl.h>
#include <CCSCedaData.h>
#include <CCSCedaCom.h>
#include <CCSDdx.h>
#include <CCSLog.h>
#include <CCSBcHandle.h>
#include <SeasonCollectCedaFlightData.h>
#include <SeasonDlgCedaFlightData.h>
#include <RotationCedaFlightData.h>
#include <CedaFpeData.h>
#include <RotationDlgCedaFlightData.h>
#include <RotGDlgCedaFlightData.h>
#include <BasicData.h>
#include <PrivList.h>
#include <CedaBasicData.h>
#include <CCSBasic.h>
#include <CedaInfData.h>
#include <Konflikte.h>
#include <DiaCedaFlightData.h>
#include <CcaCedaFlightData.h>
#include <DataSet.h>
#include <SeasonCedaFlightData.h>
#include <SpotAllocation.h>
#include <CedaResGroupData.h>
#include <DailyCedaFlightData.h>
#include <UFISAmSink.h>
#include <CedaFlightUtilsData.h>
#include <CedaParData.h>
#include <CedaValData.h>
#include <CedaFlnoData.h>


CString GetString(UINT ID)
{
	CString olRet;
	olRet.LoadString(ID);
	return olRet;
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Global Variable Section

IUFISAmPtr pConnect; // for com-interface

CString ogAppName = "FIPS"; 
CString ogCustomer = "SHA"; 
CString ogLkeyFlight = "ENG"; 
CString ogFactor_CCA = ""; 

char pcgUser[33];
char pcgPasswd[33];

char pcgHome[4] = "HAJ";
char pcgHome4[5] = "EDDV";
char pcgTableExt[10] = "HAJ"; 

char pcgVersion[12] = "4.5.5.29";

char pcgInternalBuild[12] = "003"; 

CTimeSpan ogTimelineBuffer(0, 0, 0, 0);
bool bgMulitpleDelayCodes = false;
extern bool bgTroyanHorse = false;

bool bgConfCheckRuns = false;

extern bool bgCheckPRFL = true;


CTimeSpan ogWingoverBuffer(0, 0, 0, 0);
bool bgWingoverBuffer = false;


CTimeSpan ogPosDefAllocDur(0, 0, 30, 0);
CTimeSpan ogGatDefAllocDur(0, 0, 30, 0);
CTimeSpan ogBltDefAllocDur(0, 0, 30, 0);
CTimeSpan ogWroDefAllocDur(0, 0, 30, 0);


CTimeSpan	ogDefDurPsta(0,0,30,0);
CTimeSpan	ogDefDurPstd(0,0,30,0);
CTimeSpan	ogDefDurGata(0,0,30,0);
CTimeSpan	ogDefDurGatd(0,0,30,0);

// timspan for postfilghts
CTimeSpan ogTimeSpanPostFlight(0, 0, 0, 0);		//days in the past
CTimeSpan ogTimeSpanPostFlightInFuture(0, 0, 0, 0);		//days in the future
CTimeSpan ogXDays(7, 0, 0, 0);		//days for warning in selection
BOOL ogArchivePostFlightTodayOnly = FALSE ;		// 050309 MVy: today only or period, global variable; users sets the global variable in SetupDlg::UpdatePostFlightTodayOnly()
unsigned long g_ulHandlingAgent ;		// 050311 MVy: PRF6903: Check-In Counter Restrictions for Handling Agents; users sets global variable in SetupDlg::UpdateHandlingAgent()
COLORREF g_rgbHandlingAgentOccBkBar ;		// 050317 MVy: color of occupation backbars via in setup dialog
unsigned long g_ulHandlingTask ;		// 050317 MVy: handling task, normally "CheckIn"
CStringArray g_strarrHandlingAgentAirlinesCodes ;		// 050318 MVy: contains the airline codes regarding the current handling agent is allowed to handle; these are needed for enabling flighs which are not in his occupation time ranges

CTimeSpan ogLocalDiff;
CTimeSpan ogUtcDiff;
CTime ogUtcStartTime;

CStringArray ogDssfFields;
int ogDssfFltiId = -1;
int ogDssfTtypId = -1;

ofstream of_catch;
ofstream of_debug;
ofstream *pogBCLog;

bool bgDebug;

bool bgComOk = false;

CCSLog          ogLog(NULL);
CCSDdx			ogDdx;
CCSCedaCom      ogCommHandler(&ogLog, NULL);  
CCSBcHandle     ogBcHandle(&ogDdx, &ogCommHandler, &ogLog);
CedaBasicData	ogBCD(pcgHome, &ogDdx, &ogBcHandle, &ogCommHandler, &ogCCSBasic);

bool bgUseBccom = false;


CCSBasic ogCCSBasic;

DailyCedaFlightData  ogDailyCedaFlightData;

CBasicData      ogBasicData;
CedaCfgData		ogCfgData;
PrivList		ogPrivList;
CedaParData		ogParData;
CedaValData		ogValData;

CedaResGroupData ogResGroupData;
CButtonListDlg	*pogButtonList;
SeasonAskDlg	*pogSeasonAskDlg;
RotationTables  *pogRotationTables;
CSeasonTableDlg *pogSeasonTableDlg;
CSeasonDlg		*pogSeasonDlg;
RotationDlg		*pogRotationDlg;
RotGroundDlg	*pogRotGroundDlg;
FlightDiagram	*pogFlightDiagram;
BltDiagram		*pogBltDiagram;
GatDiagram		*pogGatDiagram;
PosDiagram		*pogPosDiagram;
WroDiagram		*pogWroDiagram;
CcaDiagram		*pogCcaDiagram;
ChaDiagram		*pogChaDiagram;
DailyScheduleTableDlg* pogDailyScheduleTableDlg;

CReportSelectDlg *pogReportSelectDlg;
FlightSearchTableDlg *pogFlightSearchTableDlg;

WoResTableDlg	*pogWoResTableDlg;

CicDemandTableDlg *pogCicDemandTableDlg;
CicNoDemandTableDlg *pogCicNoDemandTableDlg;
CicConfTableDlg *pogCicConfTableDlg;

SpecialConflictDlg	*pogSpecialConflictDlg;
KonflikteDlg	*pogKonflikteDlg;
Konflikte		ogKonflikte;


DataSet			ogDataSet;


SeasonCollectDlg *pogSeasonCollectDlg;

SeasonDlgCedaFlightData  ogSeasonDlgFlights;
SeasonCollectCedaFlightData  ogSeasonCollectFlights;
CedaFlightUtilsData ogFlightUtilsData;
SeasonCedaFlightData  ogSeasonFlights;
RotationCedaFlightData ogRotationFlights;
CedaFpeData ogFpeData;
RotationDlgCedaFlightData ogRotationDlgFlights;
//RotGDlgCedaFlightData ogRotGDlgFlights;
CcaCedaFlightData ogCcaDiaFlightData;
DiaCedaFlightData ogDiaFlightData;

//DiaCedaFlightData ogBltDiaFlightData;
//DiaCedaFlightData ogGatDiaFlightData;
DiaCedaFlightData ogPosDiaFlightData;
//DiaCedaFlightData ogWroDiaFlightData;


SpotAllocation ogSpotAllocation;

CCSPtrArray<EXPANDDATA> ogExandData;


CedaInfData	ogInfData;

CBackGround *pogBackGround;
CCcaCommonTableDlg *pogCommonCcaTable;
CCcaCommonDlg *pogCommonCcaDlg;
CCicAgentPeriods AgentPeriods ;		// 050315 MVy: time periods when handling agents occupying this checkin counter		// 050321 MVy: code moved from CcaDiagramViewer



// For Com-Interface to the new TelexPool
CUFISAmSink m_UFISAmSink;
//
char ogConfigPath[256];
char ogExcelPath[256];
char ogExcelTrenner[64];

bool bgReinstadedConflicts = true;
bool bgNewRecalculateResChain = false;
bool bgAutoAllocGateWithoutGlobalNature = false;
bool bgAutoAllocGateFetchRotation = false;
bool bgNewFwRGates = false;
bool bgNatureAutoCalc = false;
bool bgServiceMandatory = false;//UFIS-1097
bool bgNatureMandatory = false;//UFIS-1097
bool bgHAAllowChangeAgent;//UFIS-1070
bool bgAutoAllocVacancy = false;
bool bgDailyRotChainDlg = false;
bool bgcheckingRemarkEdit = false;
bool bgTowingPopup = false;
bool bgConfFromCfltab = false;
bool bgPreviousDaycheckIn = false;
bool bgAllowCcaOverlap = false;
// RFC8784 DXB
bool bgPositionTerminalConflict = false;


bool bgNewGantDefault = true;
bool bgUTCCONVERTIONS = true;
bool bgFipsReports = false;
bool bgAllocationOverview = false;

bool bgTermRestrForAl = false;

bool bgConflictsForAirb = true;
bool bgNewGoodCount = false;
double dgFactorPrio = 1.0;
double dgFactorSequ = 0.1;
double dgFactorGoodCount = 1.8;

bool bgDisplayPaid = true;
bool bgDisplayCcaGroups = false;
bool bgDisplayChaGroups = false;
bool bgDiffColoursID = false;
bool bgConflictBeltFlti = false;
bool bgConflictNewFlight = false;
CString ogFltiPrio = "";
bool bgRuleforAcrRegn = false;		// 050301 MVy: Aircraft Registration restrictions
bool bgCheckInAirline= false;//Airline Grouping
bool bgCheckInAlGroup= false;
bool bgRotationmask=false;//For Rotation Dialog
bool bgReports=false;//for  reports(DXB)
bool bgCicConflictCheck=true;
CString ogPrefixReports="";
CString ogKeyCodeFilterDefault = ""; 

bool bgPosCutPaste = false; // ADR RFC 001 Cut & Paste 
bool bgExcelFLBag = false; // DXB 9173
bool bgRotationDlgLock = true;
CString ogRotationDlgAutoLockFields = "RACT3,RACT5,RREGN,DREM1,AREM1,DSTOD,ASTOA";


bool bgCnamAtr = false;
bool blDatPosDura = false;
bool blDatGatDura = false;

bool blIncomApPos = false;
bool blIncomAlPos = false;
bool blIncomApGat = false;
bool blIncomAlGat = false;
bool bgSplitJoinLocal = false;
bool bgReasonFlag = false;  
bool bgConflictUtctoLocal = false;

bool bgAutoAllocRemovePosIfDifferent = true;
bool bgLoacationChangesStarted = false;
bool bgUseCcaReload = false;
bool blNoCheckInConflictAC = false;
bool bgFWRSortPOSByType = false;
bool bgBeltAlocEqualDistribution = false;
bool bgBeltAlocWithoutGates = false;
bool bgBeltAllocReferExactParameters = true;
bool bgShowOrgDes = false;
bool bgShowRegn = false;
bool bgShowMXAC = false;
bool bgUNICODERemarks = false;


bool bgKeepExisting = false;
bool bgKeepExistingCki = false;
bool bgMustHaveFLNO = false;

bool bgRealLocal   = false;
bool bgLocal = true;
bool bgRelatedGatePosition = true;	 //PRF 8494
bool bgSecondGateDemandFlag = false; // Lisbon Terminal 2

bool bgGATPOS = false;
bool bgShowGATPOS = true;
bool bgDelaycodeNumeric = true;

bool bgViewBigBobs = false;
bool bmViewBigBobs = false;

bool bgStevFilterForBatchandExpand = false;
bool bgChuteDisplay = false;
bool bgMergeBlk = false;
bool bgAdditionalRemark = false;

bool bgUseDepBelts = false;
bool bgUseEmptyBatch = false;
bool bgEnableFlnoSelect = false;
bool bgUseAddKeyfields = false;
bool bgUseRegInSeasonSearch = false;
bool bgEnableRegInsert = false;
bool bgAddKeyCodesDR = false;
bool bgAddKeyCodesDS = false;
bool bgAddKeyCodesST = false;
bool bgConflictPriority = false;
int igWarningThreshold = 0;
bool bgShowWingspan = false;
bool bgUseAptActNatAlcList = false;
bool bgUseBeltAllocTime = false;
bool bgDiaGrouping = false;

bool bgSeasonLocal   = true;
bool bgGatPosLocal   = true;
bool bgReportLocal   = true;
bool bgDailyLocal = false;
bool bgPrintDlgOpen = false;
bool bgDailyWithAlt = false;
bool bgCreateDaily_OrgAtdBlue = false;

bool bgViewEditFilter = false;
bool bgViewEditSort = false;
bool bgNatureServiceMand = false;

bool bgPrintArrivalGate = false;

bool bgAdjacentGatRestriction = false;

// ADR_LM RFC 8963
bool bgPositionConfirmation = false;

CString ogDeicingApplName;

CTimeSpan ogAdjacentGatBufferTime(0,0,0,0);

bool bgBlackList = false;


bool bgPosRulePax = false;
bool bgBltRuleBag = false;
bool bgPstRuleFlno = false;

bool bgAutoAllocWithScore = false;
bool bgTowingDemand = false;

bool bgAutoAllocTimeFrame = false;
bool bgCommonGate = false;

CTimeSpan ogCommonGateBeginBuffer(0,0,10,0);


CString ogExtConflicts = "";

bool bgShowOnGround = true;
bool bgCheckAllConflicts = true;
CTimeSpan ogOnGroundTimelimit (0,0,0,0);

CString ogSeatsFrom = "ACT";

CString ogFIDRemarkField = "BEMD";
bool bgAirport4LC   = true;
CTimeSpan ogPosAllocBufferTime(0,0,0,0);
CTimeSpan ogGateAllocBufferTime(0,0,0,0);
CTimeSpan ogXminutesBufferTime(0,0,0,0); 

bool bgFastTowing = false;
CTimeSpan ogTowingSpan (0,0,-10,0);
int igMaxTowings = 10;

// flags for customer functionality configuration
bool bgOffRelFuncGatpos = true;
bool bgOffRelFuncCheckin = true;

bool bgExtBltCheck = false;
bool bgFlightIntYelBar = false;
bool bgFlightIDEditable = false;

bool bgShowCSTAD = false;

bool bgShowDetailAfterSearch = true;


bool bgVIPFlag = false;
bool bgFixedRes = false;
bool bgDailyFixedRes = false;
bool bgSeasonFixedRes = false;
int	igPaxPerCounter = 0;



bool bgFLBag = false;
bool bgUseMultiDelayTool = false;					
CMapStringToString ogConfigMap;//AM:20100915 - CEDA.INI Config Map					


CString ogTimeFrameStringSBC = "";
CString ogInsertStringSBC = "";
CString ogUpdateStringSBC = "";
CString ogDeleteStringSBC = "";
CString ogFlightUrnoStringSBC = "";
CString ogReleaseCCAStringSBC = "";
CString ogInsertString = "";
CString ogUpdateString = "";
CString ogDeleteString = "";

bool	bgInsert = false;
bool	bgDelete = false;
bool	bgUpdate = false;
bool	bgInsertSBC = false;
bool	bgDeleteSBC = false;
bool	bgUpdateSBC = false;

bool bgShowFlightDataToolButton = false;
bool bgShowPopupImgMenu = false;
bool bgHasGanttChartHLColor = false;
unsigned long ulgGanttChartHLColor = 0x999999;
CBrush *pogHLBrush=NULL;
CString ogPositionText = "";

int ogResetAllocParameter = 0;

bool bgBltHeight = true;
bool bgCcaHeight = true;
bool bgChaHeight = true;
bool bgGatHeight = true;
bool bgPosHeight = true;
bool bgWroHeight = true;
double dgOverlapHeight = 0.05;

bool bgFwrWro = false;
bool bgFwrPos = false;
bool bgFwrBlt = false;
bool bgFwrGat = false;
bool bgFwrCic = false;

bool bgAutoAllocWithGroundTime = false;
bool bgPrintAutoAlloc = false;
bool bgCreateAutoAllocFiles = false;
bool bgAutoAllocFirstRule = false;
bool bgAutoAllocWithExactParameter = false; 

bool bgGateFIDSRemark = false;
bool bgFIDSLogo = false;
bool bgEnhancedHandlingAgentAssignment = false;

bool bgCxxReason = false;
bool bgNightFlightSlot = false;
bool bgBltShowMaxPax = false;
bool bgColorDisabledFields  = false;

bool bgCDMPhase1 = false;

bool bgNewVIPDlg = false;

bool bgUseCenterline = false;

CTimeSpan ogPosBarSlotMinus(0,0,0,0);


int  igLengthStev = 1;


bool bgConfCheck = true;
CMapPtrToPtr ogUrnoMapConfCheck;
CMapPtrToPtr ogUrnoMapConfCheckTimer;
CMapStringToString ogMapLblUrnoToKonftype;

IApplication* UIFappl = NULL;
/////
STRUCT_BCFIELDS egStructBcfields = EXT_CHANGED_NO;


CMapPtrToPtr ogBrushMap;



bool bgKlebefunktion;

long  igWhatIfUrno;
CString ogWhatIfKey;

bool bgNoScroll;
bool bgOnline = true;
bool bgIsButtonListMovable = false;
CInitialLoadDlg *pogInitialLoad;

bool bgPosDiaAutoAllocateInWork = false;

//Added by Christine
bool bgShowClassInBatchDlg = false;
bool bgShowCashComment = false;
bool bgShowDelayArrival = false;
bool bgShowVIPNearBridge = false;
bool bgShowPayDetail = false;
bool bgShowCashButtonInBothFlight = false;
bool bgCopyCashToOther  = false;
bool bgShowChock  = false;
bool bgUseRequestedForPrognosis = false;
bool bgDailyHightligh = false;
bool bgDailyHideInsert  = false;
bool bgDailyShowVIP = false;
bool bgDailyShowActivities=false;
bool bgBarColorNature=false;
bool bgReadyForTowing=false;//UFIS-987	
bool bgCreateTowing=false;//Ufis-985
bool bgPosShadowbars =false;//UFIS-988
bool bgCanSearchConflict=false;//Ufis-989

bool bgOngroundInFuture =false;
bool bgNumTabKeys = false;

//TSC 090
bool bgscenario_autoallocate = false;
bool bgscenario_checkincounter = false;
CString oguseralloctepath="<Default>";


CBrush *pogFirstREQBrush;
CBrush *pogSecondREQBrush;
CBrush *pogGatBrush;
CBrush *pogPosBrush;
CBrush *pogBltBrush;
CBrush *pogCcaBrush;
CBrush *pogWroBrush;
CBrush *pogNotAvailBrush;
CBrush *pogNotValidBrush;



CFont ogSmallFonts_Regular_6;
CFont ogSmallFonts_Regular_7;
CFont ogMSSansSerif_Regular_6;
CFont ogMSSansSerif_Regular_8;
CFont ogMSSansSerif_Regular_12;
CFont ogMSSansSerif_Regular_16;
CFont ogMSSansSerif_Bold_8;
CFont ogMSSansSerif_Bold_7;
CFont ogCourier_Bold_10;
CFont ogCourier_Bold_8;
CFont ogCourier_Regular_10;
CFont ogCourier_Regular_8;
CFont ogCourier_Regular_9;
CFont ogMS_Sans_Serif_8;

CFont ogTimesNewRoman_9;
CFont ogTimesNewRoman_12;
CFont ogTimesNewRoman_16;
CFont ogTimesNewRoman_30;

CFont ogSetupFont;


CFont ogScalingFonts[30];
FONT_INDEXES ogStaffIndex;
FONT_INDEXES ogFlightIndex;
FONT_INDEXES ogHwIndex;
FONT_INDEXES ogRotIndex;

BOOL bgIsInitialized = FALSE;

CBrush *ogBrushs[MAXCOLORS+1];
COLORREF ogColors[MAXCOLORS+1];

COLORREF lgBkColor = SILVER;
//COLORREF lgBkColor = ::GetSysColor(COLOR_BTNFACE);
COLORREF lgTextColor = BLACK;
COLORREF lgHilightColor = WHITE;


CPoint ogMaxTrackSize = CPoint(1024, 768);
CPoint ogMinTrackSize = CPoint(1024 / 4, 768 / 4);

int   igDaysToRead;
int igFontIndex1;
int igFontIndex2;

CBitmap ogBMNotAvail;
CBitmap ogBMNotValid;


/////////////////////////////////////////////////////////////////////////////
void CreateBrushes()
{
	 int ilLc;

	for( ilLc = 0; ilLc < MAXCOLORS; ilLc++)
		ogColors[ilLc] = RED;
	ogColors[2] = GRAY;
	ogColors[3] = GREEN;
	ogColors[4] = RED;
	ogColors[5] = BLUE;
	ogColors[6] = SILVER;
	ogColors[7] = MAROON;
	ogColors[8] = OLIVE;
	ogColors[9] = NAVY;
	ogColors[10] = PURPLE;
	ogColors[11] = TEAL;
	ogColors[12] = LIME;
	ogColors[13] = YELLOW;
	ogColors[14] = FUCHSIA;
	ogColors[15] = AQUA;
	ogColors[16] = WHITE;
	ogColors[17] = BLACK;
	ogColors[18] = ORANGE;
	ogColors[19] = TOWING;
	ogColors[20] = CREAM;
	ogColors[21] = LLGREEN;
	ogColors[22] = LLVELVET;
	ogColors[23] = LIGHTLIME;
	ogColors[24] = PINK;
	ogColors[25] = VIOLET;


// don't use index 21 it's used for Break jobs already

	for(ilLc = 0; ilLc < MAXCOLORS; ilLc++)
	{
		ogBrushs[ilLc] = new CBrush(ogColors[ilLc]);
	}

	// create a break job brush pattern
	/*
    delete ogBrushs[21];
	CBitmap omBitmap;
	omBitmap.LoadBitmap(IDB_BREAK);
	ogBrushs[21] = new CBrush(&omBitmap);
    */

	pogGatBrush = new CBrush(TEAL);
	pogPosBrush = new CBrush(NAVY);
	pogBltBrush = new CBrush(OLIVE);
	pogCcaBrush = new CBrush(MAROON);
	pogWroBrush = new CBrush(PURPLE);
	pogFirstREQBrush = new CBrush(ORANGE);
	pogSecondREQBrush = new CBrush(WHITE);

	ogBMNotAvail.LoadBitmap(IDB_NOTAVAIL);
	pogNotAvailBrush = new CBrush(&ogBMNotAvail);


	ogBMNotValid.LoadBitmap(IDB_NOTVALID);
	pogNotValidBrush = new CBrush(&ogBMNotValid);


}

void DeleteBrushes()
{
	for( int ilLc = 0; ilLc < MAXCOLORS; ilLc++)
	{
		CBrush *olBrush = ogBrushs[ilLc];
		delete olBrush;
	}





	POSITION pos;
	void *pVoid;
	CBrush *polBrush;

	for( pos = ogBrushMap.GetStartPosition(); pos != NULL; )
	{
		ogBrushMap.GetNextAssoc( pos, pVoid , (void *&)polBrush );
		delete polBrush;
	}
	ogBrushMap.RemoveAll();


	delete pogFirstREQBrush;
	delete pogSecondREQBrush;
	delete pogGatBrush;
	delete pogPosBrush;
	delete pogBltBrush;
	delete pogCcaBrush;
	delete pogWroBrush;
	delete pogNotAvailBrush;
	delete pogNotValidBrush;
	try
	{
		if (pogHLBrush!=NULL) delete pogHLBrush;
	}catch(...){}

}


void InitFont() 
{
    CDC dc;
    dc.CreateCompatibleDC(NULL);

    LOGFONT logFont;
    memset(&logFont, 0, sizeof(LOGFONT));

	char pclTmpText[512], pclConfigPath[512];
	BYTE lmCharSet = DEFAULT_CHARSET;
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "CHARSET", "DEFAULT", pclTmpText, sizeof pclTmpText, pclConfigPath);
	if(!strcmp(pclTmpText, "ARABIC"))
		lmCharSet = ARABIC_CHARSET;

////////////////////

	for(int i = 0; i < 8; i++)
	{
		logFont.lfCharSet= lmCharSet;
		logFont.lfHeight = - MulDiv(i, dc.GetDeviceCaps(LOGPIXELSY), 72);
		logFont.lfWeight = FW_BOLD;
		logFont.lfQuality = PROOF_QUALITY;
		logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
		lstrcpy(logFont.lfFaceName, "Small Fonts"); 
		ogScalingFonts[i].CreateFontIndirect(&logFont);
	}
	for( i = 8; i < 20; i++)
	{ 
		logFont.lfCharSet= lmCharSet;
		logFont.lfHeight = - MulDiv(i, dc.GetDeviceCaps(LOGPIXELSY), 72);
		logFont.lfWeight = FW_BOLD;
		logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
		logFont.lfQuality = PROOF_QUALITY;

		lstrcpy(logFont.lfFaceName, "MS Sans Serif"); /*"MS Sans Serif""Arial"*/
		ogScalingFonts[i/*GANTT_S_FONT*/].CreateFontIndirect(&logFont);
	}


    // ogMS_Sans_Serif_8.CreateFont(15, 0, 0, 0, 400, FALSE, FALSE, 0, ANSI_CHARSET, 
	//							 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, "MS Sans Serif");



	logFont.lfCharSet= lmCharSet;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = DEFAULT_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ogMS_Sans_Serif_8.CreateFontIndirect(&logFont);


////////////////////


	logFont.lfCharSet= lmCharSet;
    logFont.lfHeight = - MulDiv(6, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts");
    ogSmallFonts_Regular_6.CreateFontIndirect(&logFont);
        
	logFont.lfCharSet= lmCharSet;
    logFont.lfHeight = - MulDiv(7, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts");
    ogSmallFonts_Regular_7.CreateFontIndirect(&logFont);

	logFont.lfCharSet= lmCharSet;
    logFont.lfHeight = - MulDiv(10, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ogCourier_Bold_10.CreateFontIndirect(&logFont);

	logFont.lfCharSet= lmCharSet;
	logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ogCourier_Bold_8.CreateFontIndirect(&logFont);

	logFont.lfCharSet= lmCharSet;
    logFont.lfHeight = - MulDiv(10, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ogCourier_Regular_10.CreateFontIndirect(&logFont);

	logFont.lfCharSet= lmCharSet;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ogCourier_Regular_8.CreateFontIndirect(&logFont);


	logFont.lfCharSet= lmCharSet;
	logFont.lfHeight = - MulDiv(9, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ogCourier_Regular_9.CreateFontIndirect(&logFont);


/*
	logFont.lfHeight = - MulDiv(9, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = 200;
    logFont.lfQuality = PROOF_QUALITY;
    logFont.lfOutPrecision = OUT_CHARACTER_PRECIS;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Fixedsys");
    ASSERT(ogCourier_Regular_9.CreateFontIndirect(&logFont));
*/
/*
    logFont.lfHeight = - MulDiv(6, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Small Fonts"); /
    ASSERT(ogScalingFonts[MS_SANS6].CreateFontIndirect(&logFont));

    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogScalingFonts[MS_SANS8].CreateFontIndirect(&logFont));

    logFont.lfHeight = - MulDiv(12, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogScalingFonts[MS_SANS12].CreateFontIndirect(&logFont));

	// logFont.lfHeight = - MulDiv(16, dc.GetDeviceCaps(LOGPIXELSY), 72);
	logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ASSERT(ogScalingFonts[MS_SANS16].CreateFontIndirect(&logFont));
*/ 

	ogStaffIndex.VerticalScale = MS_SANS8;
	ogStaffIndex.Chart = MS_SANS6;
	ogFlightIndex.VerticalScale = MS_SANS8;
	ogFlightIndex.Chart = MS_SANS6;
	ogHwIndex.VerticalScale = MS_SANS8;
	ogHwIndex.Chart = MS_SANS6;

	logFont.lfCharSet= lmCharSet;
    logFont.lfHeight = - MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "MS Sans Serif");
    ogMSSansSerif_Bold_8.CreateFontIndirect(&logFont);



	logFont.lfCharSet= lmCharSet;
    logFont.lfHeight = - MulDiv(7, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
    lstrcpy(logFont.lfFaceName, "Verdana");
    ogMSSansSerif_Bold_7.CreateFontIndirect(&logFont);


	// 9 - 16 - 12 - 30

	logFont.lfCharSet= lmCharSet;
    logFont.lfHeight = - MulDiv(30, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_BOLD;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	//logFont.lfItalic = TRUE;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    ogTimesNewRoman_30.CreateFontIndirect(&logFont);

	logFont.lfCharSet= lmCharSet;
    logFont.lfHeight = - MulDiv(16, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	//logFont.lfItalic = TRUE;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    ogTimesNewRoman_16.CreateFontIndirect(&logFont);

	logFont.lfCharSet= lmCharSet;
    logFont.lfHeight = - MulDiv(12, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	//logFont.lfItalic = TRUE;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    ogTimesNewRoman_12.CreateFontIndirect(&logFont);

	logFont.lfCharSet= lmCharSet;
    logFont.lfHeight = - MulDiv(9, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_ROMAN;
	//logFont.lfItalic = TRUE;
	logFont.lfQuality = PROOF_QUALITY;
    lstrcpy(logFont.lfFaceName, "Times New Roman");
    ogTimesNewRoman_9.CreateFontIndirect(&logFont);


	logFont.lfCharSet= lmCharSet;
	logFont.lfHeight = - MulDiv(9, dc.GetDeviceCaps(LOGPIXELSY), 72);
    logFont.lfWeight = FW_NORMAL;
    logFont.lfPitchAndFamily = VARIABLE_PITCH | FF_MODERN;
    lstrcpy(logFont.lfFaceName, "Courier New");
    ogSetupFont.CreateFontIndirect(&logFont);


    dc.DeleteDC();
}




