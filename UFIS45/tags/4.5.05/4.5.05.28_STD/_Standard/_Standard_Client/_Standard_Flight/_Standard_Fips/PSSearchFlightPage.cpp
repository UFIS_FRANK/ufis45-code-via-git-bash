// searchflightpage.cpp : implementation file
//

#include <stdafx.h>
#include <PSsearchflightpage.h>
#include <BasicData.h>
#include <CCSGlobl.h>
#include <CedaBasicData.h>
#include <CedaCcaData.h>
#include <AwBasicDataDlg.h>
#include <resrc1.h>
#include <PSSearchPosDlg.h>
#include <PrivList.h>

#include <FPMS.h>
#include "CCSCedadata.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSearchFlightPage property page

//IMPLEMENT_DYNCREATE(CSearchFlightPage, CPropertyPage)

CSearchFlightPage::CSearchFlightPage(bool bpLocalTime) : 
	CPropertyPage(CSearchFlightPage::IDD), bmLocalTime(bpLocalTime)
{
	bmChanged = false;
/*	omFieldList.strgSetValue("TIFA From || TIFD From;Date,TIFA From || TIFD From; Time," 
		                "TIFA To || TIFD To; Date,TIFA To || TIFD To; Time," 
						"SearchHours, SearchDay,DOOA || DOOD," 
		                "RelHBefore,RelHAfter,LSTU; Date,LSTU; Time," 
						"FDAT; Date,FDAT; Time," 
						"ALC2,FLTN,FLNS,REGN,ACT3,ORG3 || ORG4 || DES3 || DES4,"
						"TIFA Flugzeit || TIFD Flugzeit; Date,"
						"TIFA Flugzeit || TIFD Flugzeit; Time,TTYP,STEV");
						*/

}

CSearchFlightPage::~CSearchFlightPage()
{
/*	pomPageBuffer = NULL;
	for (int ilIdx = omData.GetSize()-1; ilIdx >=0; ilIdx--)
	{
		DISPStrukt *polField;
		polField = &omData[ilIdx];
		delete polField;
		omData.RemoveAt(ilIdx);
	} // end for
*/
}


BOOL CSearchFlightPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

	if(bgUseRequestedForPrognosis)	
	{
		m_Check5.SetWindowText (GetString(IDS_STRING2937));
	}

	InitPage();

	return TRUE;
}


void CSearchFlightPage::SetCalledFrom(CString opCalledFrom)
{
	omCalledFrom = opCalledFrom;
	if (opCalledFrom == "SEASON")
		bmLocalTime = bgSeasonLocal;
	else if (opCalledFrom == "DAILY" || opCalledFrom == "ROTATION")
		bmLocalTime = bgDailyLocal;

}

void CSearchFlightPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchFlightPage)
	DDX_Control(pDX, IDC_CHECK1, m_Check1);
	DDX_Control(pDX, IDC_CHECK2, m_Check2);
	DDX_Control(pDX, IDC_CHECK3, m_Check3);
	DDX_Control(pDX, IDC_CHECK4, m_Check4);
	DDX_Control(pDX, IDC_CHECK5, m_Check5);
	DDX_Control(pDX, IDC_CHECK6, m_Check6);
	DDX_Control(pDX, IDC_CHECK7, m_Check7);
	DDX_Control(pDX, IDC_CHECK8, m_Check8);
	DDX_Control(pDX, IDC_CHECK9, m_Check9);
	DDX_Control(pDX, IDC_CHECK10, m_Check10);
	DDX_Control(pDX, IDC_CHECK11, m_Check11);
	DDX_Control(pDX, IDC_CHECK12, m_Check12);
	DDX_Control(pDX, IDC_KOMPL_ROT, m_Rotation);

	DDX_Control(pDX, IDC_EDIT1, m_Edit1);
	DDX_Control(pDX, IDC_EDIT10, m_Edit10);
	DDX_Control(pDX, IDC_EDIT11, m_Edit11);
	DDX_Control(pDX, IDC_EDIT12, m_Edit12);
	DDX_Control(pDX, IDC_EDIT13, m_Edit13);
	DDX_Control(pDX, IDC_EDIT14, m_Edit14);
	DDX_Control(pDX, IDC_EDIT15, m_Edit15);
	DDX_Control(pDX, IDC_EDIT16, m_Edit16);
	DDX_Control(pDX, IDC_EDIT17, m_Edit17);
	DDX_Control(pDX, IDC_EDIT18, m_Edit18);
	DDX_Control(pDX, IDC_EDIT19, m_Edit19);
	DDX_Control(pDX, IDC_EDIT2, m_Edit2);
	DDX_Control(pDX, IDC_EDIT20, m_Edit20);
	DDX_Control(pDX, IDC_EDIT21, m_Edit21);
	DDX_Control(pDX, IDC_EDIT22, m_Edit22);
	DDX_Control(pDX, IDC_EDIT23, m_Edit23);
DDX_Control(pDX, IDC_STE2, m_CE_Ste2);
DDX_Control(pDX, IDC_STE3, m_CE_Ste3);
DDX_Control(pDX, IDC_STE4, m_CE_Ste4);
	DDX_Control(pDX, IDC_EDIT3, m_Edit3);
	DDX_Control(pDX, IDC_EDIT4, m_Edit4);
	DDX_Control(pDX, IDC_EDIT5, m_Edit5);
	DDX_Control(pDX, IDC_EDIT6, m_Edit6);
	DDX_Control(pDX, IDC_EDIT7, m_Edit7);
	DDX_Control(pDX, IDC_EDIT8, m_Edit8);
	DDX_Control(pDX, IDC_EDIT9, m_Edit9);
	DDX_Control(pDX, IDC_FLIGHT_ID, m_FlightID);
	DDX_Control(pDX, IDC_VIA, m_Via);
	DDX_Control(pDX, IDC_BUTTON_HANDL_CODE, m_CB_HandlCode);
	DDX_Control(pDX, IDC_BUTTON_POS, m_CB_Pos);// PRF 8382
	DDX_Control(pDX, IDC_BUTTON_APT, m_CB_Apt);
	DDX_Control(pDX, IDC_BUTTON_ALT, m_CB_Alt);
	DDX_Control(pDX, IDC_BUTTON_ACT, m_CB_Act);
	DDX_Control(pDX, IDC_BUTTON_NAT, m_CB_Nat);
	DDX_Control(pDX, IDC_BUT_POSIGRP, m_CB_PosGrp);// PRF 8382
	DDX_Control(pDX, IDC_BUTTON_HANDL_AGENT, m_CB_HandlAgent);
	DDX_Text(pDX, IDC_HANDL_CODE, m_HandlCode);
	DDX_Control(pDX, IDC_HANDL_CODE, m_CE_HandlCode);
	DDX_Text(pDX, IDC_POS, m_Pos);// PRF 8382
	DDX_Control(pDX, IDC_POS, m_CE_Pos);// PRF 8382
	DDX_Text(pDX, IDC_POSIGRP, m_PosGrp);// PRF 8382
	DDX_Control(pDX, IDC_POSIGRP, m_CE_PosGrp);// PRF 8382
	DDX_Text(pDX, IDC_HANDL_AGENT, m_HandlAgent);
	DDX_Control(pDX, IDC_HANDL_AGENT, m_CE_HandlAgent);
	DDX_Control(pDX, IDC_CODESHARE, m_CodeShare);
	DDX_Control(pDX, IDC_CALLSIGN, m_Callsign);
	DDX_Control(pDX, IDC_DGD2D, m_DGd2d);
	//}}AFX_DATA_MAP

 	if (pDX->m_bSaveAndValidate == FALSE)
	{
		SetData();
	}
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		GetData();
	}
	
	/*    
	m_Edit1.GetWindowText(omData[0].omData);// TIFA From || TIFD From; Date
    m_Edit2.GetWindowText(omData[1].omData);// TIFA From || TIFD From; Time
    m_Edit3.GetWindowText(omData[2].omData);// TIFA To || TIFD To; Date
    m_Edit4.GetWindowText(omData[3].omData);// TIFA To || TIFD To; Time
    m_Edit5.GetWindowText(omData[4].omData);// SearchHours
    m_Edit6.GetWindowText(omData[5].omData);// SearchDay
    m_Edit7.GetWindowText(omData[6].omData);// DOOA || DOOD
    m_Edit8.GetWindowText(omData[7].omData);// RelHBefore
    m_Edit9.GetWindowText(omData[8].omData);// RelHAfter
    m_Edit10.GetWindowText(omData[9].omData);// LSTU; Date
    m_Edit11.GetWindowText(omData[10].omData);// LSTU; Time
    m_Edit12.GetWindowText(omData[11].omData);// FDAT; Date
    m_Edit13.GetWindowText(omData[12].omData);// FDAT; Time
    m_Edit14.GetWindowText(omData[13].omData);// ACL2
    m_Edit15.GetWindowText(omData[14].omData);// FLTN
    m_Edit16.GetWindowText(omData[15].omData);// FLNS
    m_Edit17.GetWindowText(omData[16].omData);// REGN
    m_Edit18.GetWindowText(omData[17].omData);// ACT3
    m_Edit19.GetWindowText(omData[18].omData);// ORG3 || ORG4 || DES3 || DES4
    m_Edit20.GetWindowText(omData[19].omData);// TIFA Flugzeit || TIFD Flugzeit; Date
    m_Edit21.GetWindowText(omData[20].omData);// TIFA Flugzeit || TIFD Flugzeit; Time
    m_Edit22.GetWindowText(omData[21].omData);// TTYP
    m_Edit23.GetWindowText(omData[22].omData);// STEV
*/
}


BEGIN_MESSAGE_MAP(CSearchFlightPage, CPropertyPage)
	//{{AFX_MSG_MAP(CSearchFlightPage)
		// NOTE: the ClassWizard will add message map macros here
	ON_BN_CLICKED(IDC_CHECK1, OnClickedAnkunft)
	ON_BN_CLICKED(IDC_CHECK2, OnClickedAbflug)
	ON_BN_CLICKED(IDC_CHECK3, OnChange)
	ON_BN_CLICKED(IDC_CHECK4, OnChange)
	ON_BN_CLICKED(IDC_CHECK5, OnChange)
	ON_BN_CLICKED(IDC_CHECK6, OnChange)
	ON_BN_CLICKED(IDC_CHECK7, OnChange)
	ON_BN_CLICKED(IDC_CHECK8, OnChange)
	ON_BN_CLICKED(IDC_CHECK9, OnChange)
	ON_BN_CLICKED(IDC_CHECK10, OnChange)
	ON_BN_CLICKED(IDC_CHECK11, OnChange)
	ON_BN_CLICKED(IDC_CHECK12, OnChange)
	ON_BN_CLICKED(IDC_KOMPL_ROT, OnChange)
	ON_MESSAGE(WM_EDIT_KILLFOCUS, OnEditKillfocus)
	ON_MESSAGE(WM_EDIT_CHANGED, OnChange2)
	ON_BN_CLICKED(IDC_BUTTON_HANDL_CODE, OnHandlCode)
	ON_BN_CLICKED(IDC_BUTTON_HANDL_AGENT, OnHandlAgent)
	ON_BN_CLICKED(IDC_CODESHARE, OnChange)
	ON_BN_CLICKED(IDC_BUTTON_POS, OnPosition)// PRF 8382
	ON_BN_CLICKED(IDC_BUTTON_ALT, OnAirline)
	ON_BN_CLICKED(IDC_BUTTON_NAT, OnNature)
	ON_BN_CLICKED(IDC_BUTTON_APT, OnAirport)
	ON_BN_CLICKED(IDC_BUTTON_ACT, OnAircraftType)
	ON_BN_CLICKED(IDC_BUT_POSIGRP,OnPositionGrp)// PRF 8382
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CSearchFlightPage::GetData()
{
	omValues.RemoveAll();
	CStringArray olTmpValues;
	GetData(olTmpValues);
	for(int i = 0; i < olTmpValues.GetSize(); i++)
	{
		CString olTmp = olTmpValues[i];
		omValues.Add(olTmpValues[i]);
	}
}

void CSearchFlightPage::GetData(CStringArray &ropValues)
{
	CString olT1, olT2, olT3, olT4, olT5, olT6;
	CString olResult;
	CTime olDate, olTime;
	m_Edit1.GetWindowText(olT1);// TIFA From || TIFD From; Date
	m_Edit2.GetWindowText(olT2);
	m_Edit3.GetWindowText(olT3);
	m_Edit4.GetWindowText(olT4);
	m_Edit8.GetWindowText(olT5);
	m_Edit9.GetWindowText(olT6);

	bool blSetRelative = false;

	//Erst mal einen precheck und was leer ist wird 
	//entsprechend gesetzt

	if(olT1.IsEmpty() && olT2.IsEmpty() && olT3.IsEmpty() && olT4.IsEmpty() && (!olT5.IsEmpty() || !olT6.IsEmpty()) )
	{
		blSetRelative = true;
	}
	else
	{

		if(olT2.IsEmpty())
		{
			olT2 = CString("00:00");
			m_Edit2.SetWindowText(olT2);
			bmChanged = true;
		}
		if(olT4.IsEmpty())
		{
			olT4 = CString("23:59");
			m_Edit4.SetWindowText(olT4);
			bmChanged = true;
		}
		if(olT1.IsEmpty() && olT3.IsEmpty())
		{
			olT1 = CTime::GetCurrentTime().Format("%d.%m.%Y");
			olT3 = olT1;
			m_Edit1.SetWindowText(olT1);
			m_Edit3.SetWindowText(olT3);
			bmChanged = true;
		}
		if(!olT1.IsEmpty() && olT3.IsEmpty())
		{
			olT3 = olT1;
			m_Edit3.SetWindowText(olT3);
			bmChanged = true;
		}
		if(olT1.IsEmpty() && !olT3.IsEmpty())
		{
			olT1 = olT3;
			m_Edit1.SetWindowText(olT1);
			bmChanged = true;
		}
	}

	// remove seconds if present
	if (olT2.GetLength() > 5)
	{
		if (olT2.GetLength() > 7)
			olT2 = olT2.Left(olT2.GetLength() - 3);
		else
			olT2 = olT2.Left(olT2.GetLength() - 2);
	}

	// Processing From-Date and From-Time
	if(olT1 != "" && olT2 != "")
	{
		olDate = DateHourStringToDate(olT1, olT2);
		if (olDate != TIMENULL)
		{
			if (bmLocalTime) 
			{
				ogBasicData.LocalToUtc(olDate);
			}
			olT1 = olDate.Format("%Y%m%d%H%M%S");
		}
		else
		{
			olT1 = " ";
		}
	}
	else
	{
 		olT1 = " ";
	}
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");

	
	// Processing To-Date and To-Time
    m_Edit3.GetWindowText(olT1);// TIFA To || TIFD To; Date
	m_Edit4.GetWindowText(olT2);
	// remove seconds if present
	if (olT2.GetLength() > 5)
	{
		if (olT2.GetLength() > 7)
			olT2 = olT2.Left(olT2.GetLength() - 3);
		else
			olT2 = olT2.Left(olT2.GetLength() - 2);
	}
	
	if(olT1 != "" && olT2 != "")
	{
		olDate = DateHourStringToDate(olT1, olT2);
		if (olDate != TIMENULL)
		{
			if (bmLocalTime) 
			{
				ogBasicData.LocalToUtc(olDate);
			}
			olT1 = olDate.Format("%Y%m%d%H%M%S");
		}
		else
		{
			olT1 = " ";
		}
	}
	else
	{
 		olT1 = " ";
	}		
 	ropValues.Add(olT1);
	olResult += olT1 + CString("|");

	m_Edit5.GetWindowText(olT1);// SearchHours
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_Edit6.GetWindowText(olT1);// SearchDay
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_Edit7.GetWindowText(olT1);// DOOA || DOOD
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");



    m_Edit8.GetWindowText(olT1);// RelHBefore
	if (olT1 == "")
	{
		if (blSetRelative)
		{
			olT1 = "0";
			olDate = CTime::GetCurrentTime();
			CTime olDateU(olDate);
			ogBasicData.LocalToUtc(olDateU);

			CTimeSpan olSpan = olDate - olDateU;
			olT2.Format("%d", atoi(olT1) + olSpan.GetHours());
			olT1 = olT2;
		}
		else
			olT1 = " ";
	}
	else// if (!bmLocalTime)
	{
		// Include Local-Utc-Hourdiff in RelHBefore
		olDate = CTime::GetCurrentTime();
		CTime olDateU(olDate);
		ogBasicData.LocalToUtc(olDateU);

		CTimeSpan olSpan = olDate - olDateU;
		olT2.Format("%d", atoi(olT1) + olSpan.GetHours());
		olT1 = olT2;
	}
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");

    m_Edit9.GetWindowText(olT1);// RelHAfter
	if (olT1 == "")
	{
		if (blSetRelative)
		{
			olT1 = "0";
			olDate = CTime::GetCurrentTime();
			CTime olDateU1(olDate);
			ogBasicData.LocalToUtc(olDateU1);

			CTimeSpan olSpan1 = olDate - olDateU1; 
			olT2.Format("%d", atoi(olT1) - olSpan1.GetHours());
			olT1 = olT2;
		}
		else
			olT1 = " ";
	}
	else// if (!bmLocalTime)
	{
		// Include Local-Utc-Hourdiff in RelHAfter
		olDate = CTime::GetCurrentTime();
		CTime olDateU1(olDate);
		ogBasicData.LocalToUtc(olDateU1);

		CTimeSpan olSpan1 = olDate - olDateU1; 
		olT2.Format("%d", atoi(olT1) - olSpan1.GetHours());
		olT1 = olT2;
	}
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");


    m_Edit10.GetWindowText(olT1);// LSTU
	m_Edit11.GetWindowText(olT2);
	if(olT1 != "" && olT2 != "")
	{
		olDate = DateHourStringToDate(olT1, olT2);
		if (olDate != TIMENULL)
		{
 			olT1 = olDate.Format("%Y%m%d%H%M%S");
		}
		else
		{
			olT1 = " ";
		}
	}
	else
	{
 		olT1 = " ";
	}		
 	ropValues.Add(olT1);
	olResult += olT1 + CString("|");	

    m_Edit12.GetWindowText(olT1);// FDAT
	m_Edit13.GetWindowText(olT2);
	if(olT1 != "" && olT2 != "")
	{
		olDate = DateHourStringToDate(olT1, olT2);
		if (olDate != TIMENULL)
		{
 			olT1 = olDate.Format("%Y%m%d%H%M%S");
		}
		else
		{
			olT1 = " ";
		}
	}
	else
	{
 		olT1 = " ";
	}		
 	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
	
	
    m_Edit14.GetWindowText(olT1);// ACL2
	olT1.TrimLeft();
	olT1.TrimRight();
	if(olT1 == "")
		olT1 = " ";
	olResult += olT1 + CString("|");
	ropValues.Add(olT1);
    m_Edit15.GetWindowText(olT1);// FLTN
	olT1.TrimLeft();
	olT1.TrimRight();
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_Edit16.GetWindowText(olT1);// FLNS
	olT1.TrimLeft();
	olT1.TrimRight();
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_Edit17.GetWindowText(olT1);// REGN
	olT1.TrimLeft();
	olT1.TrimRight();
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_Edit18.GetWindowText(olT1);// ACT3
	olT1.TrimLeft();
	olT1.TrimRight();
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_Edit19.GetWindowText(olT1);// ORG3 || ORG4 || DES3 || DES4
	olT1.TrimLeft();
	olT1.TrimRight();
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");

    m_Edit20.GetWindowText(olT1);// TIFA || TIFD
	m_Edit21.GetWindowText(olT2);
	if(olT1 != "" && olT2 != "")
	{
		olDate = DateHourStringToDate(olT1, olT2);
		if (olDate != TIMENULL)
		{
			if (bmLocalTime) 
			{
				ogBasicData.LocalToUtc(olDate);
			}
			olT1 = olDate.Format("%Y%m%d%H%M%S");
		}
		else
		{
			olT1 = " ";
		}
	}
	else
	{
 		olT1 = " ";
	}		
 	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
	
	
    m_Edit22.GetWindowText(olT1);// TTYP
	olT1.TrimLeft();
	olT1.TrimRight();
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_Edit23.GetWindowText(olT1);// STEV
	olT1.TrimLeft();
	olT1.TrimRight();
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");

	if(m_Check1.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	if(m_Check2.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	if(m_Check3.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	if(m_Check4.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	if(m_Check5.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	if(m_Check6.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	if(m_Check7.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	if(m_Check8.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	if(m_Rotation.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}

	if (strcmp(pcgHome, "PVG") != 0)
	{
		//YYY
		if (m_Rotation.GetCheck() == 1 && omCalledFrom == "POSDIA")
		{
			m_Check9.SetCheck(1);
			m_Check10.SetCheck(1);
		}
		else if (m_Rotation.GetCheck() == 0 && omCalledFrom == "POSDIA")
		{
			m_Check9.SetCheck(0);
			m_Check10.SetCheck(0);
		}


		if(omCalledFrom == "POSDIA")
		{
			if (bgShowOnGround == 1)
			{
				m_Rotation.SetCheck(1);
				m_Check9.SetCheck(1);
				m_Check10.SetCheck(1);
			}
		}
		//YYY
	}




	if(m_Check9.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}

	if(m_Check10.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}


	if(m_Check11.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}

	if(m_Check12.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}

    m_FlightID.GetWindowText(olT1);// FlighID
	olT1.TrimLeft();
	olT1.TrimRight();
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");

    m_Via.GetWindowText(olT1);// Via
	olT1.TrimLeft();
	olT1.TrimRight();
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");

    m_CE_HandlCode.GetWindowText(olT1);
	olT1.TrimLeft();
	olT1.TrimRight();
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");

    m_CE_HandlAgent.GetWindowText(olT1);
	olT1.TrimLeft();
	olT1.TrimRight();
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");

    m_Callsign.GetWindowText(olT1);
	olT1.TrimLeft();
	olT1.TrimRight();
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");

	// PRF 8382
	//Position
	m_CE_Pos.GetWindowText(olT1);
	olT1.TrimLeft();
	olT1.TrimRight();
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");

	//Position Groups
	m_CE_PosGrp.GetWindowText(olT1);
	olT1.TrimLeft();
	olT1.TrimRight();
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");

	if(m_CodeShare.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}


	if(m_DGd2d.GetCheck() == 1)
	{
		olT1 = "X";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}


	//Keycode 
	m_CE_Ste2.GetWindowText(olT1);
	olT1.TrimLeft();
	olT1.TrimRight();
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");

	//Keycode 
	m_CE_Ste3.GetWindowText(olT1);
	olT1.TrimLeft();
	olT1.TrimRight();
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
	//Keycode 
	m_CE_Ste4.GetWindowText(olT1);
	olT1.TrimLeft();
	olT1.TrimRight();
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");


	//ropValues.Add(olResult);

}

void CSearchFlightPage::SetData()
{
	CStringArray olTmpValues;
//	for(int i = omValues.GetSize()-1; i < 26; i++)
//	{
//		omValues.Add(CString(""));
//	}
	SetData(omValues);


}

void CSearchFlightPage::SetData(CStringArray &ropValues)
{
	
	/*
	m_Edit1.SetBKColor(YELLOW);
	m_Edit2.SetBKColor(YELLOW);
	m_Edit3.SetBKColor(YELLOW);
	m_Edit4.SetBKColor(YELLOW);
	m_Edit8.SetBKColor(YELLOW);
	m_Edit9.SetBKColor(YELLOW);
	*/

	bool blDefault = true;

	m_Edit1.SetWindowText("");// TIFA From || TIFD From; Date
    m_Edit2.SetWindowText("");// TIFA From || TIFD From; Time
    m_Edit3.SetWindowText("");// TIFA To || TIFD To; Date
    m_Edit4.SetWindowText("");// TIFA To || TIFD To; Time
    m_Edit5.SetWindowText("");// SearchHours
    m_Edit6.SetWindowText("");// SearchDay
    m_Edit7.SetWindowText("");// DOOA || DOOD
    m_Edit8.SetWindowText("");// RelHBefore
    m_Edit9.SetWindowText("");// RelHAfter
    m_Edit10.SetWindowText("");// LSTU; Date
    m_Edit11.SetWindowText("");// LSTU; Time
    m_Edit12.SetWindowText("");// FDAT; Date
    m_Edit13.SetWindowText("");// FDAT; Time
    m_Edit14.SetWindowText("");// ACL2
    m_Edit15.SetWindowText("");// FLTN
    m_Edit16.SetWindowText("");// FLNS
    m_Edit17.SetWindowText("");// REGN
    m_Edit18.SetWindowText("");// ACT3
    m_Edit19.SetWindowText("");// ORG3 || ORG4 || DES3 || DES4
    m_Edit20.SetWindowText("");// TIFA Flugzeit || TIFD Flugzeit; Date
    m_Edit21.SetWindowText("");// TIFA Flugzeit || TIFD Flugzeit; Time
    m_Edit22.SetWindowText("");// TTYP
    m_Edit23.SetWindowText(ogKeyCodeFilterDefault);// STEV
    m_CE_Ste2.SetWindowText(ogKeyCodeFilterDefault);// STE2
   m_CE_Ste3.SetWindowText(ogKeyCodeFilterDefault);// STE3
   m_CE_Ste4.SetWindowText(ogKeyCodeFilterDefault);// STE4
    m_FlightID.SetWindowText("");// ID
    m_Via.SetWindowText("");// VIA
	m_FlightID.SetTypeToString("X",1,0);
	m_Via.SetTypeToString("XXXX",4,0);
    m_CE_HandlCode.SetWindowText("");
    m_CE_HandlAgent.SetWindowText("");
    m_Callsign.SetWindowText("");
	m_CE_Pos.SetWindowText("");// PRF 8382
	m_CE_PosGrp.SetWindowText("");// PRF 8382
	m_Check1.SetCheck(0);
	m_Check2.SetCheck(0);
	m_Check3.SetCheck(0);
	m_Check4.SetCheck(0);
	m_Check5.SetCheck(0);
	m_Check6.SetCheck(0);
	m_Check7.SetCheck(0);
	m_Check8.SetCheck(0);
	m_Check9.SetCheck(0);
	m_Check10.SetCheck(0);
	m_Check11.SetCheck(0);
	m_Check12.SetCheck(0);
	m_DGd2d.SetCheck(0);
	m_Rotation.SetCheck(0);
	m_CodeShare.SetCheck(0);

	for(int i = 0; i < ropValues.GetSize(); i++)
	{
		CTime olDate = TIMENULL;
		CString olV = ropValues[i];
		switch(i)
		{
		case 0:
			olDate = DBStringToDateTime(ropValues[i]);
			if(olDate != TIMENULL)
			{
				if (bmLocalTime) 
				{
					ogBasicData.UtcToLocal(olDate);
				}
 				m_Edit1.SetWindowText(olDate.Format("%d.%m.%Y"));
				m_Edit2.SetWindowText(olDate.Format("%H:%M"));
			}
			else
			{
				m_Edit1.SetWindowText(CString(""));
				m_Edit2.SetWindowText(CString(""));
			}
			break;
		case 1:
			olDate = DBStringToDateTime(ropValues[i]);
			if(olDate != TIMENULL)
			{
				if (bmLocalTime) 
				{
					ogBasicData.UtcToLocal(olDate);
				}
				m_Edit3.SetWindowText(olDate.Format("%d.%m.%Y"));
				m_Edit4.SetWindowText(olDate.Format("%H:%M"));
			}
			else
			{
				m_Edit3.SetWindowText(CString(""));
				m_Edit4.SetWindowText(CString(""));
			}
			break;
		case 2:
			if(ropValues[i] != CString(" "))
			{
				m_Edit5.SetWindowText(ropValues[i]);
			}
			break;
		case 3:
			if(ropValues[i] != CString(" "))
				m_Edit6.SetWindowText(ropValues[i]);
			break;
		case 4:
			if(ropValues[i] != CString(" "))
				m_Edit7.SetWindowText(ropValues[i]);
			break;
		case 5:
			if(ropValues[i] != CString(" "))
			{
				if (true)//!bmLocalTime)
				{
					// Include Local-Utc-Hourdiff in RelHBefore
					olDate = CTime::GetCurrentTime();
					CTime olDateU(olDate);
					ogBasicData.LocalToUtc(olDateU);

					CTimeSpan olSpan = olDate - olDateU;
					CString olNewVal;
					olNewVal.Format("%d", atoi(ropValues[i]) - olSpan.GetHours());
					m_Edit8.SetWindowText(olNewVal);
					
				}
				else
				{
					m_Edit8.SetWindowText(ropValues[i]);
				}
			}
			break;
		case 6:
			if(ropValues[i] != CString(" "))
			{
				if (true)//!bmLocalTime)
				{
					// Include Local-Utc-Hourdiff in RelHAfter
					olDate = CTime::GetCurrentTime();
					CTime olDateU(olDate);
					ogBasicData.LocalToUtc(olDateU);

					CTimeSpan olSpan = olDate - olDateU;
					CString olNewVal;
					olNewVal.Format("%d", atoi(ropValues[i]) + olSpan.GetHours());
					m_Edit9.SetWindowText(olNewVal);
					
				}
				else
				{				
					m_Edit9.SetWindowText(ropValues[i]);
				}
			}
			break;
		case 7:
			olDate = DBStringToDateTime(ropValues[i]);
			if(olDate != TIMENULL)
			{
 				m_Edit10.SetWindowText(olDate.Format("%d.%m.%Y"));
				m_Edit11.SetWindowText(olDate.Format("%H:%M"));
			}
			else
			{
				m_Edit10.SetWindowText(CString(""));
				m_Edit11.SetWindowText(CString(""));
			}
			break;
		case 8:
			olDate = DBStringToDateTime(ropValues[i]);
			if(olDate != TIMENULL)
			{
 				m_Edit12.SetWindowText(olDate.Format("%d.%m.%Y"));
				m_Edit13.SetWindowText(olDate.Format("%H:%M"));
			}
			else
			{
				m_Edit12.SetWindowText(CString(""));
				m_Edit13.SetWindowText(CString(""));
			}
			break;
		case 9:
			if(ropValues[i] != CString(" "))
				m_Edit14.SetWindowText(ropValues[i]);
			break;
		case 10:
			if(ropValues[i] != CString(" "))
				m_Edit15.SetWindowText(ropValues[i]);
			break;
		case 11:
			if(ropValues[i] != CString(" "))
				m_Edit16.SetWindowText(ropValues[i]);
			break;
		case 12:
			if(ropValues[i] != CString(" "))
				m_Edit17.SetWindowText(ropValues[i]);
			break;
		case 13:
			if(ropValues[i] != CString(" "))
				m_Edit18.SetWindowText(ropValues[i]);
			break;
		case 14:
			if(ropValues[i] != CString(" "))
				m_Edit19.SetWindowText(ropValues[i]);
			break;
		case 15:
			olDate = DBStringToDateTime(ropValues[i]);
			if(olDate != TIMENULL)
			{
				if (bmLocalTime) 
				{
					ogBasicData.UtcToLocal(olDate);
				}
				m_Edit20.SetWindowText(olDate.Format("%d.%m.%Y"));
				m_Edit21.SetWindowText(olDate.Format("%H:%M"));
			}
			else
			{
				m_Edit20.SetWindowText(CString(""));
				m_Edit21.SetWindowText(CString(""));
			}
			break;
		case 16:
			if(ropValues[i] != CString(" "))
				m_Edit22.SetWindowText(ropValues[i]);
			break;
		case 17:
			//if(ropValues[i] != CString(" "))
				m_Edit23.SetWindowText(ropValues[i]);
			break;
		case 18: //Ab hier CheckBoxen
			if(ropValues[i] == "J")
			{
				m_Check1.SetCheck(1);
				blDefault = false;
			} 
			else
			{
				m_Check1.SetCheck(0);
			}
			break;
		case 19:
			if(ropValues[i] == "J")
			{
				m_Check2.SetCheck(1);
				blDefault = false;
			}
			else
			{
				m_Check2.SetCheck(0);
			}
			break;
		case 20:
			if(ropValues[i] == "J")
			{
				m_Check3.SetCheck(1);
				blDefault = false;
			}
			else
			{
				m_Check3.SetCheck(0);
			}
			break;
		case 21:
			if(ropValues[i] == "J")
			{
				m_Check4.SetCheck(1);
				blDefault = false;
			}
			else
			{
				m_Check4.SetCheck(0);
			}
			break;
		case 22:
			if(ropValues[i] == "J")
			{
				m_Check5.SetCheck(1);
				blDefault = false;
			}
			else
			{
				m_Check5.SetCheck(0);
			}
			break;
		case 23:
			if(ropValues[i] == "J")
			{
				m_Check6.SetCheck(1);
				blDefault = false;
			}
			else
			{
				m_Check6.SetCheck(0);
			}
			break;
		case 24:
			if(ropValues[i] == "J")
			{
				m_Check7.SetCheck(1);
				blDefault = false;
			}
			else
			{
				m_Check7.SetCheck(0);
			}
			break;
		case 25:
			if(ropValues[i] == "J")
			{
				m_Check8.SetCheck(1);
				blDefault = false;
			}
			else
			{
				m_Check8.SetCheck(0);
			}
			break;
		case 26:
			if(omCalledFrom == "POSDIA")
			{
				if (strcmp(pcgHome, "PVG") != 0)
				{
					if (bgShowOnGround == 1)
					{
						ropValues[26] = "J";
					}
				}
			}

			if(ropValues[i] == "J")
			{
				m_Rotation.SetCheck(1);
			}
			else
			{
				m_Rotation.SetCheck(0);
			}
			break;
		case 27:
			
			if (strcmp(pcgHome, "PVG") != 0)
			{
				if(omCalledFrom == "POSDIA")
				{
					if (bgShowOnGround == 1 || m_Rotation.GetCheck() == 1)
					{
						ropValues[27] = "J";
						ropValues[28] = "J";
					}
				}
			}

			if(ropValues[i] == "J")
			{
				m_Check9.SetCheck(1);
			}
			else
			{
				m_Check9.SetCheck(0);
			}
			break;
		case 28:
			if(ropValues[i] == "J")
			{
				m_Check10.SetCheck(1);
			}
			else
			{
				m_Check10.SetCheck(0);
			}
			break;
		case 29:
			if(ropValues[i] == "J")
			{
				m_Check11.SetCheck(1);
			}
			else
			{
				m_Check11.SetCheck(0);
			}
			break;
		case 30:
			if(ropValues[i] == "J")
			{
				m_Check12.SetCheck(1);
			}
			else
			{
				m_Check12.SetCheck(0);
			}
			break;
		case 31://FlightId
			if(ropValues[i] != CString(" "))
				m_FlightID.SetWindowText(ropValues[i]);
			break;
		case 32://Via
			if(ropValues[i] != CString(" "))
				m_Via.SetWindowText(ropValues[i]);
			break;
		case 33:
			if(ropValues[i] != CString(" "))
				m_CE_HandlCode.SetWindowText(ropValues[i]);
			break;
		case 34:
			if(ropValues[i] != CString(" "))
				m_CE_HandlAgent.SetWindowText(ropValues[i]);
			break;
		case 35:
			if(ropValues[i] != CString(" "))
				m_Callsign.SetWindowText(ropValues[i]);
			break;
		case 36: // Position  PRF 8382 
			if(ropValues[i] != CString(" "))
				m_CE_Pos.SetWindowText(ropValues[i]);
			break;  
		case 37:// Position Groups  PRF 8382 
			if(ropValues[i] != CString(" "))
				m_CE_PosGrp.SetWindowText(ropValues[i]);
			break;  
		case 38:
			if(ropValues[i] == "J")
			{
				m_CodeShare.SetCheck(1);
			}
			else
			{
				m_CodeShare.SetCheck(0);
			}
			break;
		case 39:
			if(ropValues[i] == "X")
			{
				m_DGd2d.SetCheck(1);
			}
			else
			{
				m_DGd2d.SetCheck(0);
			}
			break;
		case 40:
				m_CE_Ste2.SetWindowText(ropValues[i]);
			break;
		case 41:
				m_CE_Ste3.SetWindowText(ropValues[i]);
			break;
		case 42:
				m_CE_Ste4.SetWindowText(ropValues[i]);
			break;

		}
	}

	if(m_Check1.GetCheck() == 0 && m_Check2.GetCheck() == 0)
	{
		m_Check1.SetCheck(1);
		m_Check2.SetCheck(1);
	}

	if(blDefault)
	{
		if(omCalledFrom == "SEASON")
		{
			m_Check4.SetCheck(1);
			m_Check3.SetCheck(1);
			m_Check6.SetCheck(1);
			m_Rotation.SetCheck(1);
		}
		if(omCalledFrom == "ROTATION")
		{
			m_Check3.SetCheck(1);
			m_Check6.SetCheck(1);
			m_Rotation.SetCheck(1);
		}
		if(omCalledFrom == "DAILY")
		{
			m_Check3.SetCheck(1);
			m_Check6.SetCheck(1);
			m_Rotation.SetCheck(1);
		}
		if(omCalledFrom == "POSDIA" || omCalledFrom == "FLIGHTDIA")
		{
			m_Check4.SetCheck(1);
			m_Check3.SetCheck(1);
			m_Rotation.SetCheck(1);
		}
		if(omCalledFrom == "CCADIA" || omCalledFrom == "CHADIA")
		{
			m_Check1.SetCheck(0);
			m_Check4.SetCheck(1);
			m_Check3.SetCheck(1);
		}

		if (strcmp(pcgHome, "PVG") != 0)
		{
			if (m_Rotation.GetCheck() == 1 && omCalledFrom == "POSDIA")
			{
				m_Check9.SetCheck(1);
				m_Check10.SetCheck(1);
			}
		}

		if(omCalledFrom == "FLIGHTSEARCH")
		{
			m_Check7.SetCheck(0);
			m_Check6.SetCheck(0);
			m_Check4.SetCheck(1);
			m_Check3.SetCheck(1);
			m_Check2.SetCheck(1);
			m_Check1.SetCheck(1);
			m_Rotation.SetCheck(1);
		}
	}

/*
	if(blDefault)
	{
		if(omCalledFrom == "SEASON")
		{
			m_Check4.SetCheck(1);
			m_Check3.SetCheck(1);
			m_Check6.SetCheck(1);
			m_Check7.SetCheck(1);
		}
		if(omCalledFrom == "ROTATION")
		{
			m_Check3.SetCheck(1);
			m_Check6.SetCheck(1);
			m_Check7.SetCheck(1);
		}
	}
*/
}


/////////////////////////////////////////////////////////////////////////////
// CSearchFlightPage message handlers
void CSearchFlightPage::InitPage()
{
	if (!bgUseAptActNatAlcList)
	{
		m_CB_Apt.ShowWindow(SW_HIDE);
		m_CB_Alt.ShowWindow(SW_HIDE);
		m_CB_Act.ShowWindow(SW_HIDE);
		m_CB_Nat.ShowWindow(SW_HIDE);
	}
	if(ogPrivList.GetStat("VIEW_CB_PosGroup") != ' ')
	{
		m_CE_PosGrp.SetSecState(ogPrivList.GetStat("VIEW_CB_PosGroup"));
		m_CB_PosGrp.SetSecState(ogPrivList.GetStat("VIEW_CB_PosGroup"));
	}
	if (bgUseAddKeyfields)
	{
		m_CE_Ste2.ShowWindow(SW_SHOW);
		m_CE_Ste3.ShowWindow(SW_SHOW);
		m_CE_Ste4.ShowWindow(SW_SHOW);
	} else
	{
		m_CE_Ste2.ShowWindow(SW_HIDE);
		m_CE_Ste3.ShowWindow(SW_HIDE);
		m_CE_Ste4.ShowWindow(SW_HIDE);
	}

	if(!bgSecondGateDemandFlag)
		m_DGd2d.ShowWindow(FALSE);



	
} // end InitPage

 
//---------------------------------------------------------------------------------
CString CSearchFlightPage::GetPrivList()
{
	//char aclInitFieldList[] = "APPL,SUBD,FUNC,FUAL,TYPE,STAT";
	CString olInitPageData; 
	/*ogAppName +
 		CString("SearchFlightPage,TIFA From || TIFD From;Date,TimeFrameDateFrom,E,1,") +
		CString("SearchFlightPage,TIFA From || TIFD From;Time,TimeFrameTimeFrom,E,1,") +
		CString("SearchFlightPage,TIFA To || TIFD To;Date,TimeFrameDateTo,E,1,") +
		CString("SearchFlightPage,TIFA To || TIFD To;Time,TimeFrameTimeTo,E,1,") +
		CString("SearchFlightPage,SearchHours,SearchHours,E,1,") +
		CString("SearchFlightPage,SearchDay,SearchDay,E,1,") +
		CString("SearchFlightPage,DOOA || DOOD,DayOfOperation,E,1,") +
		CString("SearchFlightPage,RelHBefore,RelHBefore,E,1,") +
		CString("SearchFlightPage,RelHAfter,RelHAfter,E,1,") +
		CString("SearchFlightPage,LSTU;Date,LSTUDate,E,1,") +
		CString("SearchFlightPage,LSTU;Time,LSTUTime,E,1,") +
		CString("SearchFlightPage,FDAT;Date,FDATDate,E,1,") +
		CString("SearchFlightPage,FDAT;Time,FDATTime,E,1,") +
		CString("SearchFlightPage,ALC2,ALC2,E,1,") +
		CString("SearchFlightPage,FLTN,FLTN,E,1,") +
		CString("SearchFlightPage,FLNS,FLNS,E,1,") +
		CString("SearchFlightPage,REGN,REGN,E,1,") + 
		CString("SearchFlightPage,ACT3,ACT3,E,1,") +
		CString("SearchFlightPage,ORG3 || ORG4 || DES3 || DES4,OrgDest,E,1,") +
		CString("SearchFlightPage,TIFA Flugzeit || TIFD Flugzeit;Date,FlugZeitDate,E,1,") +
		CString("SearchFlightPage,TIFA Flugzeit || TIFD Flugzeit;Time,FlugZeitTime,E,1,") +
		CString("SearchFlightPage,TTYP,TTYP,E,1,") +
		CString("SearchFlightPage,STEV,STEV,B,1,") +
		CString("SearchFlightPage,Ankunft,AnkunftButton,B,1,")+
		CString("SearchFlightPage,Abflug,AbflugButton,B,1,")+
		CString("SearchFlightPage,Betrieb,BetriebButton,B,1,")+
		CString("SearchFlightPage,Planung,PlanungButton,B,1,")+
		CString("SearchFlightPage,Prognose,PrognoseButton,B,1,")+
		CString("SearchFlightPage,Cancelled,CancelledButton,B,1,")+
		CString("SearchFlightPage,Noop,NoopButton,B,1,")+
		CString("SearchFlightPage,gelöscht,gelöschtButton,B,1,");
		*/
	return(olInitPageData);
}

void CSearchFlightPage::OnClickedAbflug()
{
	bmChanged = true;
	if(m_Check2.GetCheck() == 0)
	{
		if(m_Check1.GetCheck() == 0)
		{
			m_Check2.SetCheck(1);
		}
	}
}

void CSearchFlightPage::OnClickedAnkunft()
{
	bmChanged = true;
	if(m_Check1.GetCheck() == 0)
	{
		if(m_Check2.GetCheck() == 0)
		{
			m_Check1.SetCheck(1);
		}
	}
}


LRESULT CSearchFlightPage::OnChange2(UINT wParam, LPARAM lParam)
{

	OnChange();

	return 0L;

}


void CSearchFlightPage::OnChange()
{
	bmChanged = true;

	if (strcmp(pcgHome, "PVG") != 0)
	{
		if (m_Rotation.GetCheck() == 1 && omCalledFrom == "POSDIA")
		{
			m_Check9.SetCheck(1);
			m_Check10.SetCheck(1);
		}
		else if (m_Rotation.GetCheck() == 0 && omCalledFrom == "POSDIA")
		{
			m_Check9.SetCheck(0);
			m_Check10.SetCheck(0);
		}

		if(omCalledFrom == "POSDIA")
		{
			if (bgShowOnGround == 1)
			{
				m_Rotation.SetCheck(1);
				m_Check9.SetCheck(1);
				m_Check10.SetCheck(1);
			}
		}
	}

	if(	m_Check3.GetCheck() == 0 &&
		m_Check4.GetCheck() == 0 &&
		m_Check5.GetCheck() == 0 &&
		m_Check6.GetCheck() == 0 &&
		m_Check7.GetCheck() == 0 &&
		m_Check9.GetCheck() == 0 &&
		m_Check10.GetCheck() == 0 &&
		m_Check11.GetCheck() == 0 &&
		m_Check12.GetCheck() == 0
	)
		m_Check3.SetCheck(1);

	return ;

}


void CSearchFlightPage::OnHandlCode()
{
	CCS_TRY

	CString olText;
	m_CE_HandlCode.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "HAG","HSNA,HNAM","HSNA+", olText);
//	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "HAI","HSNA,TASK","HSNA+", olText);
//	polDlg->SetSecState("SEASONDLG_CE_APsta");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_HandlCode.SetInitText(polDlg->GetField("HSNA"), true);	
		CString olHaitabHsna = polDlg->GetField("HSNA");
		m_CE_HandlCode.SetFocus();
	}
	delete polDlg;

	CCS_CATCH_ALL
}

void CSearchFlightPage::OnHandlAgent()
{
	CCS_TRY

	CString olText;
	m_CE_HandlAgent.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "HTY","HTYP,HNAM","HTYP+", olText);
//	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "HAG","HNAM","HNAM+", olText);
//	polDlg->SetSecState("SEASONDLG_CE_APsta");
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_HandlAgent.SetInitText(polDlg->GetField("HTYP"), true);	
		CString olHaitabTask = polDlg->GetField("HNAM");
		m_CE_HandlAgent.SetFocus();
	}
	delete polDlg;

	CCS_CATCH_ALL
}


LONG CSearchFlightPage::OnEditKillfocus( UINT wParam, LPARAM lParam)
{
 	/*

	CCSEDITNOTIFY *prlNotify = (CCSEDITNOTIFY*)lParam;

	if(((UINT)m_Edit1.imID == wParam) || ((UINT)m_Edit2.imID == wParam)  || ((UINT)m_Edit3.imID == wParam) || ((UINT)m_Edit4.imID == wParam))
	{
		m_Edit1.SetBKColor(YELLOW);
		m_Edit2.SetBKColor(YELLOW);
		m_Edit3.SetBKColor(YELLOW);
		m_Edit4.SetBKColor(YELLOW);
		m_Edit8.SetBKColor(SILVER);
		m_Edit9.SetBKColor(SILVER);
		m_Edit8.SetWindowText("");// RelHBefore
		m_Edit9.SetWindowText("");// RelHAfter
	
	
	}

	if(((UINT)m_Edit8.imID == wParam) || ((UINT)m_Edit9.imID == wParam))
	{
		m_Edit1.SetBKColor(SILVER);
		m_Edit2.SetBKColor(SILVER);
		m_Edit3.SetBKColor(SILVER);
		m_Edit4.SetBKColor(SILVER);
		m_Edit8.SetBKColor(YELLOW);
		m_Edit9.SetBKColor(YELLOW);
		m_Edit1.SetWindowText("");// TIFA From || TIFD From; Date
		m_Edit2.SetWindowText("");// TIFA From || TIFD From; Time
		m_Edit3.SetWindowText("");// TIFA To || TIFD To; Date
		m_Edit4.SetWindowText("");// TIFA To || TIFD To; Time
	}
	
	*/
	/*
	if((UINT)m_CE_PosGrp.imID == wParam)
	{
		bool blflag=false;
		CString olTxt; 
		if(omPosGrp.GetSize()==0)
		{
			bmIsPosGrpFocuss = true;
			OnPositionGrp();
		}

		m_CE_PosGrp.GetWindowText(olTxt);
		if(!olTxt.IsEmpty())
		{
				bool blfound = false;
*/

/*			
			CStringArray olArray;
						
			ExtractItemList(olTxt, &olArray, ' ');
			for(int j = 0 ;j< olArray.GetSize();j++)
			{

				olTxt = olArray.GetAt(j);
				
				if(olTxt.IsEmpty())
					continue;
*/
	/*			
				for(int i = 0 ;i< omPosGrp.GetSize();i++)
				{
					if(olTxt == omPosGrp.GetAt(i))
					{
						blfound = true;
						break;
					}
				}

				if(!blfound)
				{
					AfxMessageBox("Position Group Does not Exist");
					m_CE_PosGrp.SetWindowText("");
					m_CE_PosGrp.SetFocus();	
					//break;
				}
		}
	}
*/
	return 0L;
}

// Airline button click invokes this function. shows the avilable Airlines in a list box
void CSearchFlightPage::OnAirline()
{
	CString olText;
	m_Edit14.GetWindowText(olText);
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ALT","ALC2,ALC3,ALFN", "ALC3+", olText);
	if(polDlg->DoModal() == IDOK)
	{
		CString olAirline;
		olAirline = polDlg->GetField("ALC2");
		if (olAirline.IsEmpty())
			olAirline = polDlg->GetField("ALC3");
		m_Edit14.SetInitText(olAirline, true);	
		//m_Edit14.SetInitText(polDlg->GetField("ALC2"), true);	
		m_Edit14.SetFocus();
		}
	delete polDlg;
}
// Nature button click invokes this function. shows the avilable Natures in a list box
void CSearchFlightPage::OnNature()
{
	CString olText;
	m_Edit22.GetWindowText(olText);

	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "NAT","TTYP,TNAM", "TNAM+", olText);
//	polDlg->SetSecState("ROTATIONDLG_CE_ATtyp");
	if(polDlg->DoModal() == IDOK)
	{
		m_Edit22.SetInitText(polDlg->GetField("TTYP"), true);
		m_Edit22.SetFocus();
	}
	delete polDlg;
}
// AircraftType button click invokes this function. shows the avilable AircraftType in a list box
void CSearchFlightPage::OnAircraftType()
{
	CString olText;
	m_Edit18.GetWindowText(olText);
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "ACT","ACT3,ACT5,ACFN", "ACT3+,ACT5+,ACFN+", olText);
	if(polDlg->DoModal() == IDOK)
	{
		m_Edit18.SetInitText(polDlg->GetField("ACT3"), true);	
		m_Edit18.SetFocus();
	}
	delete polDlg;
}


// Airport button click invokes this function. shows the avilable airport in a list box
void CSearchFlightPage::OnAirport()
{
	CString olText;
	m_Edit19.GetWindowText(olText);
	AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "APT","APC3,APC4,APFN", "APFN5+", olText);
	if(polDlg->DoModal() == IDOK)
	{
		m_Edit19.SetInitText(polDlg->GetField("APC3"), true);	
		m_Edit19.SetFocus();
	}
	delete polDlg;
}


// Position button click invokes this function. shows the avilable postions in a list box
void CSearchFlightPage::OnPosition()
{
	CString olText;
	m_CE_Pos.GetWindowText(olText);
	PSSearchPosDlg *polDlg = new PSSearchPosDlg(this, "PST","PNAM", "PNAM+", olText);
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_Pos.SetInitText(polDlg->GetField(), true);	
		m_CE_Pos.SetFocus();
	}
	delete polDlg;
}

// Position Group button click invokes this function. shows the avilable postions Groups in a list box
void CSearchFlightPage::OnPositionGrp()
{
	
	CString olPosList;
	olPosList  = ogBCD.GetValueList("GRN","TABN","PSTTAB", "GRPN", ";");

	::ExtractItemList(olPosList,&omPosGrp,';');


	CString olTxt;
	m_CE_PosGrp.GetWindowText(olTxt);



	PSSearchPosDlg *polDlg = new PSSearchPosDlg(this,"","","",olTxt,omPosGrp,true);
	if(polDlg->DoModal() == IDOK)
	{
		m_CE_PosGrp.SetStatus(true);
		m_CE_PosGrp.SetInitText(polDlg->GetField(), true);	
		m_CE_PosGrp.SetFocus();
	}
	
	delete polDlg;

	//bmIsPosGrpFocuss = false;
}

