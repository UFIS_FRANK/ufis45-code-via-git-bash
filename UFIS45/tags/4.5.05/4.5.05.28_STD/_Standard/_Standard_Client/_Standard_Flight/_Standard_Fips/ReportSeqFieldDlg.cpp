// ReportSeqFieldDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <fpms.h>
#include <ReportSeqFieldDlg.h>
#include <CcsGlobl.h>
#include <CedaBasicData.h>
#include <resrc1.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CReportSeqFieldDlg 


CReportSeqFieldDlg::CReportSeqFieldDlg(CWnd* pParent, CString opHeadline, CString opSelectfield, 
									   CTime* poMinDate, CTime* poMaxDate, char *opSelect, 
									   CString opSetFormat, int ipMin, int ipMax)
	: CDialog(CReportSeqFieldDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CReportSeqFieldDlg)
	m_Datumbis = _T("");
	m_Datumvon = _T("");
	m_Select = _T("");
	m_Selectfield = _T("");
	//}}AFX_DATA_INIT
	omHeadline = opHeadline;	//Dialogbox-�berschrift
	m_Selectfield = opSelectfield;
	pomMinDate = poMinDate;
	pomMaxDate = poMaxDate;
	pcmSelect = opSelect;
	omSetFormat = opSetFormat;

	imMax = ipMax;
	imMin = ipMin;

}


void CReportSeqFieldDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CReportSeqFieldDlg)
	DDX_Control(pDX, IDC_SELECTFIELD, m_CS_Selectfield);
	DDX_Control(pDX, IDC_SELECT, m_CE_Select);
	DDX_Control(pDX, IDC_DATUMVON, m_CE_Datumvon);
	DDX_Control(pDX, IDC_DATUMBIS, m_CE_Datumbis);
	DDX_Text(pDX, IDC_DATUMBIS, m_Datumbis);
	DDX_Text(pDX, IDC_DATUMVON, m_Datumvon);
	DDX_Text(pDX, IDC_SELECT, m_Select);
	DDX_Text(pDX, IDC_SELECTFIELD, m_Selectfield);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CReportSeqFieldDlg, CDialog)
	//{{AFX_MSG_MAP(CReportSeqFieldDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CReportSeqFieldDlg 

void CReportSeqFieldDlg::OnCancel() 
{
	// TODO: Zus�tzlichen Bereinigungscode hier einf�gen
	
	CDialog::OnCancel();
}

void CReportSeqFieldDlg::OnOK() 
{
	m_CE_Datumvon.GetWindowText(m_Datumvon);
	m_CE_Datumbis.GetWindowText(m_Datumbis);
	// ist nur ein Time Feld gef�llt, �bernimmt das andere dessen Inhalt
	if(m_Datumvon.IsEmpty())
		m_CE_Datumbis.GetWindowText(m_Datumvon);
	if(m_Datumbis.IsEmpty())
		m_CE_Datumvon.GetWindowText(m_Datumbis);

	*pomMinDate = DateStringToDate(m_Datumvon);
	*pomMaxDate = DateStringToDate(m_Datumbis); 
	m_CE_Select.GetWindowText(m_Select);
	strcpy(pcmSelect, m_Select);

	if (*pomMinDate > *pomMaxDate)
	{
		// To-Time before From-Time
		MessageBox(GetString(IDS_TIMERANGE), GetString(IMFK_REPORT), MB_OK);
		return;
	}

	if(m_Selectfield == "Airline") // Airline ist kein Pflichtfeld
	{
		if( m_Datumbis.IsEmpty() || m_Datumvon.IsEmpty() )
		{
			MessageBox(GetString(IDS_STRING918), GetString(IMFK_REPORT), MB_OK);
			return;
		}
	}
	else
	{		
		if(!bgReports)
		{
			if((!strlen(pcmSelect)) || ((m_Datumbis.IsEmpty()) || (m_Datumvon.IsEmpty())) )
			{
			MessageBox(GetString(IDS_STRING918), GetString(IMFK_REPORT), MB_OK);
			return;
			}
		}
	    else
        {	
		if(((m_Datumbis.IsEmpty()) || (m_Datumvon.IsEmpty())) )
			{
			MessageBox(GetString(IDS_STRING918), GetString(IMFK_REPORT), MB_OK);
			return;
			}
		
		}
	
	
	}
	



	{

		if(strcmp(m_Selectfield,GetString(IDS_STRING1046)) == 0)	// Airline
		{
			CString olTmp;
			CString olTmp2;
			bool blRet = ogBCD.GetField("ALT", "ALC2", "ALC3", pcmSelect, olTmp2, olTmp);
			if(!blRet)
			{
//				MessageBox(GetString(IDS_STRING1042), GetString(IDS_WARNING), MB_ICONWARNING );
//				return;
			}

/*			if(strlen(pcmSelect) < 2)
			{
				MessageBox("2Ltr/3Ltr-Code only!", GetString(IDS_WARNING), MB_ICONWARNING );
				return;
			}
*/
		}

		if(strcmp(m_Selectfield,GetString(IDS_STRING1416)) == 0)	// Day of Week
		{
			int ilTrafficDay = atoi(pcmSelect);

			if(ilTrafficDay == 0 || ilTrafficDay >7)
			{
				MessageBox(GetString(IDS_STRING1417), GetString(IDS_WARNING), MB_ICONWARNING );
				return;
			}
			if((m_CE_Datumvon.GetStatus()) && (m_CE_Datumbis.GetStatus()))
			{
				int i = 0;
				bool blInRange = false;
				CTimeSpan olOneDay (1, 0, 0, 0);
				CTime olActTime = *pomMinDate;

				while (olActTime <= *pomMaxDate)
				{
//					int ilActDay = olActTime.GetDayOfWeek(); don�t call MFC, there is day 1 sunday!!
					int ilActDay = GetDayOfWeek(olActTime);
					if (ilActDay == ilTrafficDay)
					{
						blInRange = true;
						break;
					}

					olActTime += olOneDay;

					if (i++ > 7)
						break;
				}

				if(!blInRange)
				{
					MessageBox(GetString(IDS_STRING_TRAFFIC_RANGE), GetString(IDS_WARNING), MB_ICONWARNING );
					return;
				}
			}
		}

		if((m_CE_Datumvon.GetStatus()) && (m_CE_Datumbis.GetStatus()))
			CDialog::OnOK();
		else
			MessageBox(GetString(IDS_STRING934), GetString(IMFK_REPORT), MB_OK);
	}
}

BOOL CReportSeqFieldDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

/*rkr
	if (ogCustomer == "SHA" && ogAppName == "FIPS")
	{
		CStatic* polSelectHeadline = (CStatic*)GetDlgItem(IDC_SELECTHEADLINE);
		CStatic* polTill = (CStatic*)GetDlgItem(IDC_ST_INTERVAL_TILL);
		polSelectHeadline->ShowWindow(SW_HIDE);
		polTill->SetWindowText(GetString(IDS_STRING1604));
	}
*/

	// MB Test Anfang
	if (omSetFormat.IsEmpty());
//		m_CE_Select.SetFormat("A|#A|#A|#A|#A");
	else
	{
		m_CE_Select.SetFormat(omSetFormat);
		if (imMax != 0)
			m_CE_Select.SetTypeToString(omSetFormat, imMax, imMin);
	}
	// MB Test Ende

	CDialog::SetWindowText(omHeadline);	//Dialogbox-�berschrift
	m_CE_Datumvon.SetTypeToDate();
	m_CE_Datumvon.SetBKColor(YELLOW);
	m_Datumvon = pomMinDate->Format("%d.%m.%Y" ) ;
	m_CE_Datumvon.SetWindowText( m_Datumvon );

	m_CE_Datumbis.SetTypeToDate();
	m_CE_Datumbis.SetBKColor(YELLOW);
	m_Datumbis = pomMaxDate->Format("%d.%m.%Y" ) ;
	m_CE_Datumbis.SetWindowText( m_Datumbis );

	m_Select = pcmSelect ;
	m_CE_Select.SetWindowText(  m_Select );

	CString olCompare(GetString(IDS_STRING1036));
//	if( m_Selectfield.Compare(GetString(IDS_STRING1037)) ) // Messe ist kein Pflichtfeld
	if(m_Selectfield != "Airline") // Airline ist kein Pflichtfeld
		m_CE_Select.SetBKColor(YELLOW);
	if(bgReports)
	{
		if(m_Selectfield != "Airline") // Airline ist kein Pflichtfeld
		m_CE_Select.SetBKColor(WHITE);
	}
	if( !(m_Selectfield.Compare(olCompare)) )	
	{
		m_CE_Select.SetTypeToInt();
		m_CE_Select.SetWindowText(m_Select);
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}
