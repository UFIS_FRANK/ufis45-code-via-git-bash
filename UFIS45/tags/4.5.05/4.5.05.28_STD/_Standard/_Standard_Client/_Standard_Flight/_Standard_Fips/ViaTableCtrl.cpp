// ViaTableCtrl.cpp : implementation file
//

#include <stdafx.h>
#include <fpms.h>
#include <ViaTableCtrl.h>
#include <BasicData.h>
#include <CedaBasicData.h>
#include <dataset.h>
#include <resrc1.h>
#include <AwBasicDataDlg.h>
#include <CedaAptLocalUtc.h>



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ViaTableCtrl

ViaTableCtrl::ViaTableCtrl(CWnd *popParent, CString opFields)
{

	ExtractItemList(opFields, &omFields);

	Create( IDD_VIATABLE, popParent );

	bmShowButton = true;

	pomParent = popParent;	

	pomTable = new CCSTable;

	pomTable->SetTableData(this, 0, 100, 0, 100);

	InitTable();

	pomBorder = new CStatic;

	pomBorder->Create( NULL,   WS_DISABLED | WS_CHILD, CRect(0,0,0,0), this);

	pomBorder->ModifyStyleEx(0, WS_EX_CLIENTEDGE);

	//pomButton = new CButton;
	pomButton = new CCSButtonCtrl;

	pomButton->Create( GetString(IDS_STRING299), SW_HIDE | WS_CHILD, CRect(0,0,0,0), this, IDC_CHARTBUTTON ); 


	//SetWindowPos( NULL, 0, 0, 268, 35,  SWP_NOZORDER );
	SetWindowPos( NULL, 0, 0, 223, 51,  SWP_NOZORDER );

	omRefDat = CTime::GetCurrentTime();

	bmLocal = false;
	
	bmEdit = true;

}



void ViaTableCtrl::Enable( BOOL bpEnable )
{
	pomButton->EnableWindow(bpEnable);
	pomButton->UpdateWindow();

	if (pomTable) 
		pomTable->SetTableEditable(bpEnable);
}



ViaTableCtrl::~ViaTableCtrl()
{
	delete pomTable;
	delete pomBorder;
	delete pomButton;
}


BEGIN_MESSAGE_MAP(ViaTableCtrl, CDialog)
	//{{AFX_MSG_MAP(ViaTableCtrl)
	ON_BN_CLICKED(IDC_CHARTBUTTON, OnShowTable)
	ON_MESSAGE(WM_EDIT_CHANGED, OnEditChanged)
    ON_MESSAGE(WM_TABLE_LBUTTONDOWN, OnTableLButtonDown)
	ON_MESSAGE(WM_TABLE_IPEDIT_KILLFOCUS, OnTableIPEditKillfocus)
	ON_MESSAGE(WM_TABLE_SELCHANGE, OnTableSelChanged)
	ON_MESSAGE(WM_TABLE_MENU_SELECT, OnTableMenuSelect)
	ON_WM_SYSDEADCHAR()
	ON_WM_SYSKEYDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// ViaTableCtrl message handlers



void ViaTableCtrl::SetPosition(CRect opRect)
{
	if(opRect.bottom == 0 && opRect.right == 0)
	{
		SetWindowPos( NULL, opRect.left, opRect.top, 0, 0,  SWP_NOSIZE | SWP_NOZORDER | SWP_SHOWWINDOW);

	}
	else
	{
		MoveWindow(opRect);
	}


	GetClientRect(opRect);
	
	pomBorder->MoveWindow(opRect);

	GetClientRect(opRect);
	opRect.DeflateRect(2,2);

	pomTable->SetPosition(opRect.left, opRect.right, opRect.top, opRect.bottom);

	//opRect.DeflateRect(60,20);


	int ilDx = opRect.left + ((opRect.right - opRect.left) / 2);
	int ilDy = opRect.top  + ((opRect.bottom - opRect.top) / 2);
	
	opRect.left   = ilDx - 45;
	opRect.right  = ilDx + 45;
	opRect.top    = ilDy - 11;
	opRect.bottom = ilDy + 11;
	
	pomButton->MoveWindow(opRect);
}



void ViaTableCtrl::SetViaList(CString opList, CTime opRefDat, bool bpLocalTime)
{
	bmLocal = bpLocalTime;
	omViaList = opList;
	omRefDat = opRefDat;

	if(omRefDat == TIMENULL)
		omRefDat = CTime::GetCurrentTime();

	ShowTable();

	pomTable->SetTableEditable(bmEdit);

	if(!omViaList.IsEmpty() || !bmShowButton)
	{
		pomButton->ShowWindow(SW_HIDE);
		pomBorder->ShowWindow(SW_SHOW);
		pomTable->SetWindowPos( pomBorder, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE | SWP_SHOWWINDOW);
	}
	else
	{
		pomTable->ShowWindow(SW_HIDE);
		pomBorder->ShowWindow(SW_HIDE);
		pomButton->ShowWindow(SW_SHOW);
	}

}


void ViaTableCtrl::OnShowTable()
{
	pomButton->ShowWindow(SW_HIDE);
	pomBorder->ShowWindow(SW_SHOW);
	pomTable->SetWindowPos( pomBorder, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE | SWP_SHOWWINDOW );
	pomTable->MakeInplaceEdit(0, 1);
}


// override the OnCnacel function to catch the ESC-key (Ctrl close) MBR, 08.04.99 
void ViaTableCtrl::OnCancel()
{

	// Hide the table
	pomTable->ShowWindow(SW_HIDE);
	pomBorder->ShowWindow(SW_HIDE);

	// show the button
	pomButton->ShowWindow(SW_SHOW);

}

void ViaTableCtrl::SetSecStat(char clStat)
{
	cmSecStat = clStat;
	if (clStat == '-') // because button must be aktivated also when table is deaktivated
		pomButton->SetSecState(clStat); 
	ShowTable();
}



void ViaTableCtrl::SetSelectMode(bool bpMode)
{
	pomTable->SetSelectMode(LBS_MULTIPLESEL);
	pomTable->SetShowSelection(bpMode);
}


void ViaTableCtrl::InitTable()
{
	CCSPtrArray <TABLE_HEADER_COLUMN> olHeaderDataArray;
	TABLE_HEADER_COLUMN rlHeader;


	pomTable->SetHeaderSpacing(0);
	pomTable->SetMiniTable();
	pomTable->SetTableEditable(true);

	//rkr04042001
	pomTable->SetIPEditModus(true);

	pomTable->SetSelectMode(0);


	//RST!!!
	pomTable->SetShowSelection(false);
	
	rlHeader.Length = 35; 
	olHeaderDataArray.New(rlHeader);

	rlHeader.Alignment = COLALIGN_CENTER;
	rlHeader.Length = 50; 
	rlHeader.Font = &ogCourier_Regular_10;
	rlHeader.Text = CString("");

	for(int i = 0; i < 8; i++)	
		olHeaderDataArray.New(rlHeader);
	
	pomTable->SetHeaderFields(olHeaderDataArray);
	pomTable->SetDefaultSeparator();
	olHeaderDataArray.DeleteAll();
	pomTable->DisplayTable();

}







CString ViaTableCtrl::GetField(VIADATA *prpVia, CString opField)
{
	CString olRet;
	CTime olTime;

	if(prpVia == NULL)
		return olRet;


	if(opField == "APC3")
		olRet = CString(prpVia->Apc3);
	else 
	if(opField == "APC4")
		olRet = CString(prpVia->Apc4);
	else 
	if(opField == "STOA")
	{
		olTime = prpVia->Stoa;
//		if(bmLocal) ogBasicData.UtcToLocal(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptUtcToLocal (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.UtcToLocal(olTime);
		}
		olRet = DateToHourDivString(olTime, omRefDat);
	}
	else 
	if(opField == "STOD")
	{
		olTime = prpVia->Stod;
//		if(bmLocal) ogBasicData.UtcToLocal(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptUtcToLocal (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.UtcToLocal(olTime);
		}
		olRet = DateToHourDivString(olTime, omRefDat);
	}
	else 
	if(opField == "ETA")
	{
		olTime = prpVia->Etoa;
//		if(bmLocal) ogBasicData.UtcToLocal(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptUtcToLocal (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.UtcToLocal(olTime);
		}
		olRet = DateToHourDivString(olTime, omRefDat);
	}
	else 
	if(opField == "ATA")
	{
		olTime = prpVia->Land;
//		if(bmLocal) ogBasicData.UtcToLocal(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptUtcToLocal (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.UtcToLocal(olTime);
		}
		olRet = DateToHourDivString(olTime, omRefDat);
	}
	else 
	if(opField == "ETD")
	{
		olTime = prpVia->Etod;
//		if(bmLocal) ogBasicData.UtcToLocal(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptUtcToLocal (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.UtcToLocal(olTime);
		}
		olRet = DateToHourDivString(olTime, omRefDat);
	}
	else 
	if(opField == "ATD")
	{
		olTime = prpVia->Airb;
//		if(bmLocal) ogBasicData.UtcToLocal(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptUtcToLocal (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.UtcToLocal(olTime);
		}
		olRet = DateToHourDivString(olTime, omRefDat);
	}
	else 
	if(opField == "FIDS")
		olRet = CString(prpVia->Fids);





	return olRet;

/*
	char	Fids[2];
	char	Apc3[4];
	char	Apc4[5];
	CTime	Stoa;
	CTime	Etoa;
	CTime	Land;
	CTime	Onbl;
	CTime	Stod;
	CTime	Etod;
	CTime	Ofbl;
	CTime	Airb;
*/


}


void ViaTableCtrl::SetField(VIADATA *prpVia, CString opField, CString opValue, CTime opRefDat)
{

	CString olApc4;
	CString olApc3;
	CTime olTime;

	if (opRefDat == TIMENULL)
		opRefDat = omRefDat;

	if(prpVia == NULL)
		return; 


	if((opField == "APC3") || (opField == "APC4"))
	{
		if(ogBCD.GetField("APT", "APC4", "APC3", opValue, olApc4, olApc3 ))
		{
			strcpy(prpVia->Apc3, olApc3);	
			strcpy(prpVia->Apc4, olApc4);	
		}
		else
		{
			strcpy(prpVia->Apc3, "");	
			strcpy(prpVia->Apc4, "");	
		}
	}	
	else 
	if(opField == "STOA")
	{
		olTime = HourStringToDate(opValue, opRefDat); 
//		if(bmLocal) ogBasicData.LocalToUtc(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptLocalToUtc (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.LocalToUtc(olTime);
		}
		prpVia->Stoa = olTime;
	}
	else 
	if(opField == "STOD")
	{
		olTime = HourStringToDate(opValue, opRefDat); 
//		if(bmLocal) ogBasicData.LocalToUtc(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptLocalToUtc (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.LocalToUtc(olTime);
		}
		prpVia->Stod = olTime;
	}
	else 
	if(opField == "ETA")
	{
		olTime = HourStringToDate(opValue, opRefDat); 
//		if(bmLocal) ogBasicData.LocalToUtc(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptLocalToUtc (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.LocalToUtc(olTime);
		}
		prpVia->Etoa = olTime;
	}
	else 
	if(opField == "ATA")
	{
		olTime = HourStringToDate(opValue, opRefDat); 
//		if(bmLocal) ogBasicData.LocalToUtc(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptLocalToUtc (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.LocalToUtc(olTime);
		}
		prpVia->Land = olTime;
	}
	else 
	if(opField == "ETD")
	{
		olTime = HourStringToDate(opValue, opRefDat); 
//		if(bmLocal) ogBasicData.LocalToUtc(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptLocalToUtc (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.LocalToUtc(olTime);
		}
		prpVia->Etod = olTime;
	}
	else 
	if(opField == "ATD")
	{
		olTime = HourStringToDate(opValue, opRefDat); 
//		if(bmLocal) ogBasicData.LocalToUtc(olTime);
		if(bmLocal)
		{
			if (bgRealLocal)
				CedaAptLocalUtc::AptLocalToUtc (olTime, CString(prpVia->Apc4));
			else
				ogBasicData.LocalToUtc(olTime);
		}
		prpVia->Airb = olTime;
	}
	else 
	if(opField == "FIDS")
		strcpy(prpVia->Fids, opValue);	


}





CString ViaTableCtrl::GetLabel(CString opField)
{
	CString olRet;

	if(opField == "APC3")
		olRet = GetString(IDS_STRING299);
	else 
	if(opField == "APC4")
		olRet = GetString(IDS_STRING299);
	else 
	if(opField == "STOA")
	{
		olRet = GetString(IDS_STRING323);
	}
	else 
	if(opField == "STOD")
	{
		olRet = GetString(IDS_STRING316);
	}
	else 
	if(opField == "ETA")
	{
		olRet = GetString(IDS_STRING302);
	}
	else 
	if(opField == "ATA")
	{
		olRet = GetString(IDS_STRING304);
	}
	else 
	if(opField == "ETD")
	{
		olRet = GetString(IDS_STRING317);
	}
	else 
	if(opField == "ATD")
	{
		olRet = GetString(IDS_STRING321);
	}
	else 
	if(opField == "FIDS")
	{
		olRet = GetString(IDS_STRING1452);
	}

	return olRet;
}



void ViaTableCtrl::GetAttrib(CCSEDIT_ATTRIB &ropAttrib, CString opField)
{
	

	if(opField == "APC4")
	{
		ropAttrib.Type = KT_STRING;
		ropAttrib.Format = "XXXX";
		ropAttrib.TextMinLenght = 0;
		ropAttrib.TextMaxLenght = 4;
		ropAttrib.Style = ES_UPPERCASE;
	}
	else 
	if(opField == "APC3")
	{
		ropAttrib.Type = KT_STRING;
		ropAttrib.Format = "XXX";
		ropAttrib.TextMinLenght = 0;
		ropAttrib.TextMaxLenght = 3;
		ropAttrib.Style = ES_UPPERCASE;
	}
	else 
	if(opField == "FIDS")
	{
		ropAttrib.Type = KT_STRING;
		ropAttrib.Format = "X";
		ropAttrib.TextMinLenght = 0;
		ropAttrib.TextMaxLenght = 1;
	}
	else 
	{
		ropAttrib.Type = KT_TIME;
		ropAttrib.ChangeDay	= true;
		ropAttrib.TextMaxLenght = 7;
	}

}



void ViaTableCtrl::ShowTable()
{

	if(cmSecStat == '-')
		pomTable->ShowWindow(SW_HIDE);

	CCSEDIT_ATTRIB rlAttrib;
	CCSPtrArray<TABLE_COLUMN> olColList;
	TABLE_COLUMN rlColumnData;
	CCSPtrArray<VIADATA> olVias;
	char buffer[64];


	ogBasicData.GetViaArray(&olVias, omViaList);

	pomTable->ResetContent();

	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogMS_Sans_Serif_8;
	rlColumnData.VerticalSeparator = SEPA_NORMAL;
	rlColumnData.HorizontalSeparator = SEPA_NONE;
	rlColumnData.Alignment = COLALIGN_CENTER;



	if(cmSecStat != '1' || !bmEdit)
		rlColumnData.BkColor = RGB(192,192,192);


	int ilCount = omFields.GetSize();

	for(int i = 0; i < ilCount; i++)
	{

		if(omFields[i] == "HEAD")
		{
			rlColumnData.blIsEditable = false;	
			rlColumnData.Text = "";
			rlColumnData.BkColor = RGB(192,192,192);
			olColList.New(rlColumnData);

			for (int  ilLc = 0; ilLc < 8 ; ilLc++)
			{
				itoa(ilLc+1, buffer, 10);
				rlColumnData.Text = CString(buffer);
				olColList.New(rlColumnData);
			}

		}
		else
		{
			rlColumnData.blIsEditable = false;	
			rlColumnData.Text = GetLabel(omFields[i]);
			olColList.New(rlColumnData);
		
			if(cmSecStat != '1' || !bmEdit)
			{
				rlColumnData.blIsEditable = false;	
				rlColumnData.BkColor = RGB(192,192,192);
			}
			else
			{
				rlColumnData.blIsEditable = true;
				rlColumnData.BkColor = RGB(255,255,255);
			}


			for (int  ilLc = 0; ilLc < 8 ; ilLc++)
			{
				if(ilLc >= olVias.GetSize())
					rlColumnData.Text = CString("");
				else
				{
					if(cmSecStat != '-')
						rlColumnData.Text = GetField(&olVias[ilLc], omFields[i]);
					else
						rlColumnData.Text = CString("");
				}

				GetAttrib(rlAttrib, omFields[i]);
				rlColumnData.EditAttrib = rlAttrib;
				olColList.New(rlColumnData);
			}
			
			if(omFields[i] == "APC3")
			{
				pomTable->AddMenuItem( i, -1, 0, 101, GetString(IDS_STRING1711));
			}
			if(omFields[i] == "APC4")
			{
				pomTable->AddMenuItem( i, -1, 0, 102, GetString(IDS_STRING1711));
			}
		}
		pomTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();

	}
	olVias.DeleteAll();
	pomTable->DisplayTable();
}


bool ViaTableCtrl::GetStatus(CString &opMessage)
{
	opMessage = GetStatus();
	
	if(opMessage.IsEmpty())
		return true;
	else
		return false;
}


CString  ViaTableCtrl::GetStatus()
{
	CString olRet;
	CString olTmp;


	int ilLines = pomTable->GetLinesCount();

	bool blRet;
	int ilColumn = 0;


	for(int  i = 0; i < ilLines; i++)
	{
		blRet = true;
		ilColumn = 0;

		while(blRet)
		{
			if((blRet = pomTable->GetTextFieldValue(i, ilColumn, olTmp)))
			{
				if(!pomTable->GetCellStatus(i, ilColumn))
				{
					olRet = GetString(IDS_STRING299); // "VIA"
					return olRet;
				}
			}
			ilColumn++;
		}
	}

	return olRet;
}



void ViaTableCtrl::GetViaList(char *pcpViaList)
{
	CString olRet = GetViaList();

	strcpy(pcpViaList, olRet);
}


CString ViaTableCtrl::GetViaList()
{

	CCSPtrArray<VIADATA> olNewVias;
	CCSPtrArray<VIADATA> olVias;

	VIADATA *prlVia;

	CString olTmp;

	int ilLines = pomTable->GetLinesCount();

	bool olRet;
	int ilColumn = 0;

	CString olViaList;

	for(int  i = 0; i < ilLines; i++)
	{
		olRet = true;
		ilColumn = 1;

		while(pomTable->GetTextFieldValue(i, ilColumn, olTmp))
		{
			if(ilColumn > olNewVias.GetSize())
			{
				prlVia = new VIADATA;
				olNewVias.Add(prlVia);
			}
			else
			{
				prlVia = &olNewVias[ilColumn-1];
			}
			
			SetField(prlVia , omFields[i], olTmp);

			ilColumn++;
		}

	}


	//ogBasicData.GetViaArray(&olVias, omViaList);



	olViaList = CreateViaList(&olNewVias);


	olNewVias.DeleteAll();
	olVias.DeleteAll();


/*

	CString olRet;
	CString olApc3;
	CString olApc4;
	CString olFids;
	CTime olStoa;
	CTime olStod;
	CString olTmp;
	CString olBlank14 = "              ";
	int ilVian = 0;
	CString olVial;

	CStringArray olVias;

	
	for( i = ilLines - 1; i >= 0 ; i--)
	{
		pomTable->GetTextFieldValue(i, 0, olApc3);
		pomTable->GetTextFieldValue(i, 1, olTmp);
		olStoa = HourStringToDate(olTmp, omRefDat); 
		if(bmLocal) ogBasicData.LocalToUtc(olStoa);
		pomTable->GetTextFieldValue(i, 2, olTmp);
		pomTable->GetTextFieldValue(i, 3, olFids);
		olStod = HourStringToDate(olTmp, omRefDat); 
		if(bmLocal) ogBasicData.LocalToUtc(olStod);

		if(olFids.IsEmpty())
			olFids = " ";

		if(!olApc3.IsEmpty() || olStoa != TIMENULL || olStod != TIMENULL)
		{
			if(olApc4.IsEmpty())
			{
				if(!ogBCD.GetField("APT", "APC4", "APC3", olApc3, olApc4, olApc3 ))
				{
					olApc3 = "   ";
					olApc4 = "    ";
				}
			}
			else
			{
				//	olApc3 = "   ";
				if(!olApc3.IsEmpty())
				{
					if(!ogBCD.GetField("APT", "APC4", "APC3", olApc3, olApc4, olApc3 ))
					{
						olApc3 = "   ";
						olApc4 = "    ";
					}
				}
				else
				{
					if(!ogBCD.GetField("APT", "APC3", "APC4", olApc4, olApc3, olApc4 ))
					{
						olApc3 = "   ";
						olApc4 = "    ";
					}
				}
			}

			olVias.Add(olApc3);
			if(olApc3.GetLength() != 3)
				olApc3 = "   ";

			if(olApc4.GetLength() != 4)
				olApc4 = "   ";

			olTmp  =	olFids  +
						olApc3  +
						olApc4  +
						CTimeToDBString(olStoa, omRefDat) +  
						olBlank14 +  
						olBlank14 +  
						olBlank14 +  
						CTimeToDBString(olStod, omRefDat) + 
						olBlank14 +  
						olBlank14 +  
						olBlank14; 

			for(int ilLc = omAVias.GetSize() - 1; ilLc >= 0; ilLc--)
			{
				if(strcmp(omAVias[ilLc].Apc3, olApc3) == 0)
				{
					olTmp  =	olFids  +
								olApc3  +
								olApc4  +
								CTimeToDBString(olStoa, omRefDat) +  
								CTimeToDBString(omAVias[ilLc].Etoa, omRefDat) +  
								CTimeToDBString(omAVias[ilLc].Land, omRefDat) +  
								CTimeToDBString(omAVias[ilLc].Onbl, omRefDat) +  
								CTimeToDBString(olStod, omRefDat) + 
								CTimeToDBString(omAVias[ilLc].Etod, omRefDat) +  
								CTimeToDBString(omAVias[ilLc].Airb, omRefDat) +  
								CTimeToDBString(omAVias[ilLc].Ofbl, omRefDat); 
					break;
				}
			}
			olVial += olTmp;
			ilVian++;
		}
	}

	int ilCViaA = olVias.GetSize();
	if(ilCViaA > 0)
	{
		CString oO = olVias[ilCViaA-1];
		strcpy(prmAFlight->Via3, olVias[ilCViaA-1]);
	}

	
	olVial.TrimRight();

*/




	return olViaList;
}




CString ViaTableCtrl::CreateViaList(CCSPtrArray<VIADATA> *popVias, CTime opRefDat)
{
	CString olVia;
	CString olFids;
	CString olApc3;
	CString olApc4;
	CString	olTmp;
	CString	olTmp2;

	VIADATA *prlVia;

	for(int i = 0; i < popVias->GetSize(); i++)
	{
		prlVia = &(*popVias)[i];


		olApc3 = CString(prlVia->Apc3);
		olApc4 = CString(prlVia->Apc4);
		olFids = CString(prlVia->Fids);


		if(olApc3.GetLength() != 3)
			olApc3 = "   ";

		if(olApc4.GetLength() != 4)
			olApc4 = "    ";

		if(olFids.GetLength() != 1)
			olFids = " ";


		olTmp  =	olFids  +
					olApc3  +
					olApc4  +
					CTimeToDBString(prlVia->Stoa, TIMENULL) +  
					CTimeToDBString(prlVia->Etoa, TIMENULL) +  
					CTimeToDBString(prlVia->Land, TIMENULL) +  
					CTimeToDBString(prlVia->Onbl, TIMENULL) +  
					CTimeToDBString(prlVia->Stod, TIMENULL) + 
					CTimeToDBString(prlVia->Etod, TIMENULL) +  
					CTimeToDBString(prlVia->Ofbl, TIMENULL) +  
					CTimeToDBString(prlVia->Airb, TIMENULL); 
//					CTimeToDBString(prlVia->Airb, TIMENULL) +  
//					CTimeToDBString(prlVia->Ofbl, TIMENULL); 

		olTmp2 = olTmp;
		olTmp2.TrimRight();
		if(!olTmp2.IsEmpty())
			olVia += olTmp;

	}

	return olVia;
}

LONG ViaTableCtrl::OnEditChanged( UINT wParam, LPARAM lParam)
{
	return pomParent->SendMessage(WM_EDIT_CHANGED, wParam, lParam);	
}

LONG ViaTableCtrl::OnTableSelChanged( UINT wParam, LPARAM lParam)
{
	// Only send message to parent, if a column will be selected
	// That's if the clicked line number is 0 and the clicked column number is greater than 0!
	CPoint point;
    ::GetCursorPos(&point);
    pomTable->pomListBox->ScreenToClient(&point);    
  	int ilColNo = pomTable->GetColumnnoFromPoint(point);
	int ilLineNo = pomTable->GetLinenoFromPoint(point);
 
	if (ilLineNo == 0 && ilColNo > 0)
		return pomParent->SendMessage(WM_TABLE_SELCHANGE, wParam, lParam);
	else
		return 0L;
}


LONG ViaTableCtrl::OnTableIPEditKillfocus( UINT wParam, LPARAM lParam)
{
	CCSIPEDITNOTIFY *prlNotify = (CCSIPEDITNOTIFY*)lParam;

	if(prlNotify->Text.IsEmpty())
		return 0L;
	CString olTmp;
	CString olTmp2;


	if(omFields[prlNotify->Line] == "APC3" )
	{
		prlNotify->UserStatus = true;
		if(prlNotify->Status = ogBCD.GetField("APT", "APC3", "APC4", prlNotify->Text, olTmp, olTmp2 ))
			prlNotify->Text = olTmp;

	}	
	if(omFields[prlNotify->Line] == "APC4" )
	{
		prlNotify->UserStatus = true;
		if(prlNotify->Status = ogBCD.GetField("APT", "APC3", "APC4", prlNotify->Text, olTmp, olTmp2 ))
			prlNotify->Text = olTmp2;

	}	
	return 0L;
}

LONG ViaTableCtrl::OnTableMenuSelect( UINT wParam, LPARAM lParam)
{

	CCSTABLENOTIFY *prlNotify = (CCSTABLENOTIFY *) lParam;

	if(!bmEdit)
		return 0L;

	if(wParam == 101)
	{
		AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "APT","APC3,APC4,APFN", "APFN+");
		if(polDlg->DoModal() == IDOK)
		{
			pomTable->SetIPValue(prlNotify->Line, prlNotify->Column, polDlg->GetField("APC3"));
		}
		delete polDlg;
	}


	if(wParam == 102)
	{
		AwBasicDataDlg *polDlg = new AwBasicDataDlg(this, "APT","APC3,APC4,APFN", "APFN+");
		if(polDlg->DoModal() == IDOK)
		{
			pomTable->SetIPValue(prlNotify->Line, prlNotify->Column, polDlg->GetField("APC4"));
		}
		delete polDlg;
	}


	return TRUE  ;
}


LONG ViaTableCtrl::OnTableLButtonDown( UINT wParam, LPARAM lParam)
{
	CCSTABLENOTIFY *prlNotify = (CCSTABLENOTIFY *) lParam;

	int ilColumn = 	prlNotify->Column;
	COLORREF olColor;

	if(wParam < (UINT)omFields.GetSize())
	{
		if((omFields[wParam] == "HEAD") && (ilColumn > 0))
		{
 			// Select or deselect table column
			if(pomTable->GetLineColumnBkColor(wParam,  ilColumn, olColor))
			{

				if(olColor != RGB(0,0,255))
				{
					pomTable->ChangeColumnColor(ilColumn, RGB(0,0,255), RGB(255,255,255));
				}
				else
				{
					pomTable->ChangeColumnColor(ilColumn, RGB(255,255,255), RGB(0,0,0));
					pomTable->ChangeColumnColor(wParam, ilColumn, RGB(192,192,192), RGB(0,0,0));
				}
			}

		}
	}
	return 0L;
}





CString ViaTableCtrl::GetViaList(CString opViaList, CTime opRefDat)
{

	CCSPtrArray<VIADATA> olNewVias;
	CCSPtrArray<VIADATA> olVias;
	COLORREF olColor;

	VIADATA *prlVia;
	VIADATA *prlVia2;

	CString olTmp;

	ogBasicData.GetViaArray(&olVias, opViaList);

	int ilLines = pomTable->GetLinesCount();

	int ilColumn = 1;

	CString olViaList;

	for(int  i = 0; i < ilLines; i++)
	{
		ilColumn = 1;

		while(pomTable->GetTextFieldValue(i, ilColumn, olTmp))
		{
			if(ilColumn > olNewVias.GetSize())
			{
				prlVia = new VIADATA;
				olNewVias.Add(prlVia);

				if(pomTable->GetLineColumnBkColor(i,  ilColumn, olColor))
				{
					if(olColor == RGB(0,0,255))
					{
						prlVia->InUse = true;
					}
					else
					{
						prlVia->InUse = false;
					}
				}
			}
			else
			{
				prlVia = &olNewVias[ilColumn - 1];
			}
			
			SetField(prlVia , omFields[i], olTmp, opRefDat);

			ilColumn++;
		}

	}


	
	int ilCount = max(olNewVias.GetSize(), olVias.GetSize());
	

	
	for(i = olNewVias.GetSize(); i < ilCount; i++)
	{
		olNewVias.Add(new VIADATA);
	}

	for(i = olVias.GetSize(); i < ilCount; i++)
	{
		olVias.Add(new VIADATA);
	}


	for(i = 0; i < ilCount; i++)
	{
		prlVia = &olNewVias[i];

		if(prlVia->InUse)
		{
			prlVia2 = &olVias[i];
			*prlVia2 = *prlVia;
		}
	}
	
	
	olViaList = CreateViaList(&olVias, opRefDat);


	olNewVias.DeleteAll();
	olVias.DeleteAll();



	return olViaList;
}



int ViaTableCtrl::GetSelColumns()
{
	int ilRet = 0;

	bool olRet = true;
	int	ilColumn = 1;
	COLORREF olColor;
	CString olTmp;

	while(pomTable->GetTextFieldValue(0, ilColumn, olTmp))
	{
		if(pomTable->GetLineColumnBkColor(0,  ilColumn, olColor))
		{
			if(olColor == RGB(0,0,255))
			{
				ilRet++;
			}
		}
		ilColumn++;
	}

	return ilRet;
}


void ViaTableCtrl::SelectColumn(int ipColumn, bool bpSelect)
{

	bool olRet = true;
	int	ilColumn = 1;
	CString olTmp;

	if(ipColumn < 0)
	{
		while(pomTable->GetTextFieldValue(0, ilColumn, olTmp))
		{
			pomTable->ChangeColumnColor(ilColumn, RGB(255,255,255), RGB(0,0,0));
			pomTable->ChangeColumnColor(0, ilColumn, RGB(192,192,192), RGB(0,0,0));
			ilColumn++;
		}
	}
	else
	{
		//to do
	}

}

