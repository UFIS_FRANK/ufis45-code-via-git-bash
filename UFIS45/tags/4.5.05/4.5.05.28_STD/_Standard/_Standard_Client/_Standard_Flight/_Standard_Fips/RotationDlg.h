#if !defined(AFX_ROTATIONDLG_H__8ECFE3F1_5ABB_11D1_B3FC_0000B45A33F5__INCLUDED_)
#define AFX_ROTATIONDLG_H__8ECFE3F1_5ABB_11D1_B3FC_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// RotationDlg.h : header file
//
// Modification History:
// 22-nov-00	rkr		HandlePostFlight() and CheckPostFlight() 
//						for handling postflights added;

//#define ID_OK    WM_USER + 17171

 
#include <RotationDlgCedaFlightData.h>
#include <CCSEdit.h>
#include <CCSButtonCtrl.h>
#include <CCSTable.h>
#include <CCSGlobl.h>
#include <CedaFlzData.h>
#include <CedaCraData.h>
#include <resrc1.h>
#include <CedaCcaData.h>
#include <CedaChaData.h>
#include <RotationViaDlg.h>
#include <DlgResizeHelper.h>
#include <tab.h>
#include <MultiDelayDlg.h>
#include <RotationLoadDlgWAW.h>


#include <CCSGlobl.h>
#include <CCSPtrArray.h>
#include <CCSCedadata.h>
#include <CCSddx.h>
#include <ModeOfPay.h>



//class ViaTableCtrl;

/////////////////////////////////////////////////////////////////////////////
// RotationDlg dialog

class RotationDlg : public CDialog
{
// Construction
public:
	RotationDlg(CWnd* pParent = NULL);   // standard constructor
	~RotationDlg();

	void FillFlightData();
	CToolTipCtrl* m_pToolTip;
	

// Dialog Data
	//{{AFX_DATA(RotationDlg)
	enum { IDD = IDD_ROTATION };
	CStatic	m_LB_CAROSEL;
	CStatic	m_LB_KEY2;
	CStatic	m_LB_KEY1;
	CCSButtonCtrl	m_CB_VipADlg;
	CCSButtonCtrl	m_CB_VipDDlg;
	CComboBox	m_CL_ACxxReason;
	CComboBox	m_CL_DCxxReason;
	CCSButtonCtrl	m_CB_ANfes;
	CCSButtonCtrl	m_CB_DNfes;
	CCSEdit	m_CE_ANfes;
	CCSEdit	m_CE_DNfes;
	CCSButtonCtrl	m_CB_AVip;
	CCSButtonCtrl	m_CB_DVip;
	CCSButtonCtrl	m_CB_DFpsd;
	CCSButtonCtrl	m_CB_AFpsa;
	CCSButtonCtrl	m_CB_ARep;
	CCSButtonCtrl	m_CB_DRep;
	CCSButtonCtrl	m_CB_DDelay;
	CCSButtonCtrl	m_CB_AGpu;
	CCSButtonCtrl	m_CB_DGpu;
	CCSButtonCtrl	m_CB_DLoad;
	CCSButtonCtrl	m_CB_ALoad;
	CCSButtonCtrl	m_CB_ALog;
	CCSButtonCtrl	m_CB_DLog;
	CCSButtonCtrl	m_CB_DCicRem4;
	CCSButtonCtrl	m_CB_DCicRem3;
	CCSButtonCtrl	m_CB_DCicRem2;
	CCSButtonCtrl	m_CB_DCicRem1;
	CCSButtonCtrl	m_CB_DCicCodeShare4;
	CCSButtonCtrl	m_CB_DCicCodeShare3;
	CCSButtonCtrl	m_CB_DCicCodeShare2;
	CCSButtonCtrl	m_CB_DCicCodeShare1;
	CCSButtonCtrl	m_CB_Towing;
	CCSButtonCtrl	m_CB_REQ;
	CCSEdit	m_CE_ADivr;
	CCSEdit	m_CE_DDivr;
	CCSButtonCtrl	m_CB_DRetTaxi;
	CCSButtonCtrl	m_CB_DRetFlight;
	CCSButtonCtrl	m_CB_ARetTaxi;
	CCSButtonCtrl	m_CB_ARetFlight;
	CCSButtonCtrl	m_CB_DGd2d;
	CCSEdit	m_CE_DBaz4;
	CCSEdit	m_CE_DBaz1;
	CCSEdit	m_CE_DBao1;
	CCSEdit	m_CE_DBac1;
	CCSEdit	m_CE_DBao4;
	CCSEdit	m_CE_DBac4;
	CCSEdit	m_CE_ABas1;
	CCSEdit	m_CE_ABae1;
	CCSEdit	m_CE_ABas2;
	CCSEdit	m_CE_ABae2;
 	CCSEdit	m_CE_DBbfa;
	CCSEdit	m_CE_ABbaa;
	CCSButtonCtrl	m_CB_SeasonMask;
	CCSEdit	m_CE_DCsgn;
	CCSEdit	m_CE_ACsgn;
	CCSButtonCtrl m_CB_Paid;
	CCSButtonCtrl	m_CB_DTtypList;
	CCSButtonCtrl	m_CB_DDcd1List;
	CCSButtonCtrl	m_CB_ATtypList;
	CComboBox	m_CL_DRemp;
	CComboBox	m_CL_ARemp;
	CComboBox	m_CL_DGatLogo;
	CComboBox	m_CL_DGatLogo2;
	CCSButtonCtrl	m_CB_Ok;
	CCSButtonCtrl	m_CB_Lock;
	CCSButtonCtrl	m_CB_AExt2List;
	CCSButtonCtrl	m_CB_AExt1List;
	CCSButtonCtrl	m_CB_Act35List;
	CCSButtonCtrl	m_CB_DPstdList;
	CCSButtonCtrl	m_CB_DCinsList4;
	CCSButtonCtrl	m_CB_DCinsList3;
	CCSButtonCtrl	m_CB_APstaList;
	CCSButtonCtrl	m_CB_DWro1List;
	CCSButtonCtrl	m_CB_DCinsList2;
	CCSButtonCtrl	m_CB_DCinsList1;
	CCSButtonCtrl	m_CB_DCicLogo1;
	CCSButtonCtrl	m_CB_DCicLogo2;
	CCSButtonCtrl	m_CB_DCicLogo3;
	CCSButtonCtrl	m_CB_DCicLogo4;
	CCSEdit	m_CE_DCinsBorder;
	CCSEdit	m_CE_DChuBorder;
	CCSButtonCtrl	m_CB_DAlc3List3;
	CCSButtonCtrl	m_CB_DAlc3List2;
	CCSButtonCtrl	m_CB_AAlc3List3;
	CCSButtonCtrl	m_CB_AAlc3List2;
	CCSButtonCtrl	m_CS_Ankunft;
	CCSButtonCtrl	m_CS_Abflug;
	CCSEdit	m_CE_DGd2x;
	CCSEdit	m_CE_ARwya;
	CCSEdit	m_CE_Ming;
	CCSEdit	m_CE_DFltn;
	CCSEdit	m_CE_DFlti;
	CCSEdit	m_CE_AAlc3;
	CCSEdit	m_CE_DOrg3;
	CCSButtonCtrl	m_CB_Join;
	CCSEdit	m_CE_DW1ea;
	CCSEdit	m_CE_DW1ba;
	CCSEdit	m_CE_ATet2;
	CCSEdit	m_CE_ATet1;
	CCSEdit	m_CE_AOrg3;
	CCSEdit	m_CE_AOrg3L;
	CCSEdit	m_CE_ADes3;
	CCSEdit	m_CE_DTobt;
	CCSEdit	m_CE_DTsat;
	CCSButtonCtrl	m_CB_CkeckList_Arr;
	CCSButtonCtrl	m_CB_Split;
	CCSButtonCtrl	m_CB_CkeckList_Dep;
	CCSButtonCtrl	m_CB_Object;
	CCSEdit	m_CE_DWro1;
	CCSEdit	m_CE_DTwr1;
	CCSEdit	m_CE_DTtyp;
	CCSEdit	m_CE_DTgd2;
	CCSEdit	m_CE_DTgd1;
	CCSButtonCtrl	m_CB_DTelex;
	CCSButtonCtrl	m_CB_DStypList;
	CCSEdit	m_CE_DStyp;
	CCSEdit	m_CE_DStod;
	CCSEdit	m_CE_DStoa;
	CCSEdit	m_CE_DStev;
	CCSEdit	m_CE_DSte2;
	CCSEdit	m_CE_DSte3;
	CCSEdit	m_CE_DSte4;
	CCSEdit	m_CE_DSlot;
	CCSButtonCtrl	m_CB_DShowJfno;
	CCSEdit	m_CE_DRwyd;
	CCSEdit	m_CE_DRem1;
	CCSEdit	m_CE_DRem3;
	CCSEdit	m_CE_ARem3;
	CCSEdit	m_CE_DPstd;
	CCSEdit	m_CE_DOfbl;
	CCSEdit	m_CE_DNxti;
	CCSEdit	m_CE_DLastChange;
	CCSEdit	m_CE_DLastChangeTime;
	CCSEdit	m_CE_DJfnoBorder;
	CCSEdit	m_CE_DIskd;
	CCSEdit	m_CE_DIfrd;
	CCSButtonCtrl	m_CB_DHtypList;
	CCSEdit	m_CE_DHtyp;
	CCSEdit	m_CE_DGtd2;
	CCSEdit	m_CE_DGtd1;
	CCSButtonCtrl	m_CB_DGta1List;
	CCSButtonCtrl	m_CB_DGta2List;
	CCSEdit	m_CE_DGd2y;
	CCSEdit	m_CE_DGd1y;
	CCSEdit	m_CE_DGd1x;
	CCSEdit	m_CE_DFlns;
	CCSEdit	m_CE_DEtod;
	CCSEdit	m_CE_DEtdi;
	CCSEdit	m_CE_DEtdc;
	CCSEdit	m_CE_DDtd2;
	CCSEdit	m_CE_DDtd1;
	CCSButtonCtrl	m_CB_DDes3List;
	CCSEdit	m_CE_DDes3;
	CCSEdit	m_CE_DDes4;
	CCSButtonCtrl	m_CB_DDcd2List;
	CCSEdit	m_CE_DDcd2;
	CCSEdit	m_CE_DDcd1;
	CCSButtonCtrl	m_CB_DCxx;
	CCSButtonCtrl	m_CB_DRerouted;
	CCSButtonCtrl	m_CB_DDiverted;
	CCSButtonCtrl	m_CB_DAlc3List;
	CCSEdit	m_CE_DAlc3;
	CCSEdit	m_CE_DAirb;
	CCSEdit	m_CE_ATtyp;
	CCSEdit	m_CE_ATmoa;
	CCSEdit	m_CE_ATmb2;
	CCSEdit	m_CE_ATmb1;
	CCSEdit	m_CE_ATga2;
	CCSEdit	m_CE_ATga1;
	CCSButtonCtrl	m_CB_ATelex;
	CCSButtonCtrl	m_CB_AStypList;
	CCSEdit	m_CE_AStyp;
	CCSEdit	m_CE_AStod;
	CCSEdit	m_CE_AStoa;
	CCSEdit	m_CE_AStev;
	CCSEdit	m_CE_ASte2;
	CCSEdit	m_CE_ASte3;
	CCSEdit	m_CE_ASte4;
	CCSButtonCtrl	m_CB_AShowJfno;
	CCSButtonCtrl	m_CB_ARerouted;
	CCSEdit	m_CE_ARem1;
	CCSEdit	m_CE_APsta;
	CCSButtonCtrl	m_CB_AOrg3List;
	CCSEdit	m_CE_AOnbl;
	CCSEdit	m_CE_AOnbe;
	CCSEdit	m_CE_AOfbl;
	CCSEdit	m_CE_ANxti;
	CCSEdit	m_CE_AMail;
	CCSEdit	m_CE_ALastChange;
	CCSEdit	m_CE_ALastChangeTime;
	CCSEdit	m_CE_ALand;
	CCSEdit	m_CE_AJfnoBorder;
	CCSEdit	m_CE_AIfra;
	CCSButtonCtrl	m_CB_AHtypList;
	CCSEdit	m_CE_AHtyp;
	CCSButtonCtrl	m_CB_AGta2List;
	CCSButtonCtrl	m_CB_AGta1List;
	CCSEdit	m_CE_AGta2;
	CCSEdit	m_CE_AGta1;
	CCSButtonCtrl	m_CB_Agents;
	CCSEdit	m_CE_AGa2y;
	CCSEdit	m_CE_AGa1y;
	CCSEdit	m_CE_AGa2x;
	CCSEdit	m_CE_AGa1x;
	CCSEdit	m_CE_AExt2;
	CCSEdit	m_CE_AExt1;
	CCSEdit	m_CE_AEtoa;
	CCSEdit	m_CE_AEtai;
	CCSEdit	m_CE_ADtd2;
	CCSEdit	m_CE_ADtd1;
	CCSButtonCtrl	m_CB_ADiverted;
	CCSButtonCtrl	m_CB_ADcd2List;
	CCSEdit	m_CE_ADcd2;
	CCSButtonCtrl	m_CB_ADcd1List;
	CCSEdit	m_CE_ADcd1;
	CCSButtonCtrl	m_CB_ACxx;
	CCSButtonCtrl	m_CB_ABlt2List;
	CCSEdit	m_CE_ABlt2;
	CCSButtonCtrl	m_CB_ABlt1List;
	CCSEdit	m_CE_ABlt1;
	CCSEdit	m_CE_AB2ea;
	CCSEdit	m_CE_AB2ba;
	CCSEdit	m_CE_AB1ba;
	CCSEdit	m_CE_AB1ea;
	CCSEdit	m_CE_AFlns;
	CCSEdit	m_CE_AFltn;
	CCSEdit	m_CE_DBlt1;
	CCSButtonCtrl	m_CB_DBlt1List;
	CCSEdit	m_CE_DB1ea;
	CCSEdit	m_CE_DB1ba;
	CCSEdit	m_CE_DTmb1;
	CCSEdit	m_CE_AFlti;
	CCSButtonCtrl	m_CB_AAlc3List;
	CCSEdit	m_CE_AAirb;
	CCSEdit	m_CE_Act5;
	CCSEdit	m_CE_Act3;
	CCSEdit	m_CE_Regn;
	CCSEdit	m_CE_Acws;
	CCSEdit	m_CE_Acle;
	CString	m_Regn;
	CString	m_Act3;
	CString	m_Act5;
	CString	m_Ming;
	CString m_Acws;
	CString m_Acle;
	BOOL	m_Paid;
	CString	m_AAirb;
	CString	m_AFltn;
	CString	m_AFlti;
	CString	m_AFlns;
	CString	m_AB1ba;
	CString	m_DB1ba;
	CString	m_AB1ea;
	CString	m_DB1ea;
	CString	m_AB2ba;
	CString	m_AB2ea;
	CString	m_ABlt1;
	CString	m_ABlt2;
	CString	m_DBlt1;
	CString	m_DTmb1;
	BOOL	m_ACxx;
	CString	m_ADcd1;
	CString	m_ADcd2;
	BOOL	m_ADiverted;
	BOOL	m_DDiverted;
	CString	m_ADtd1;
	CString	m_ADtd2;
	CString	m_AEtai;
	CString	m_AEtoa;
	CString	m_AExt1;
	CString	m_AExt2;
	CString	m_AGa1x;
	CString	m_AGa2x;
	CString	m_AGa1y;
	CString	m_AGa2y;
	CString	m_AGta1;
	CString	m_AGta2;
	CString	m_AHtyp;
	CString	m_AIfra;
	CString	m_ALand;
	CString	m_ALastChange;
	CString	m_ALastChangeTime;
	CString	m_AMail;
	CString	m_ANxti;
	CString	m_AOfbl;
	CString	m_AOnbe;
	CString	m_AOnbl;
	CString	m_APsta;
	CString	m_ARem1;
	CString	m_ARem3;
	CString	m_DRem3;
	CString	m_ARemp;
	CString	m_ABas1;
	CString	m_ABae1;
	CString	m_ABas2;
	CString	m_ABae2;
	BOOL	m_AVip;
	BOOL	m_DVip;
	BOOL	m_AFpsa;
	BOOL	m_DFpsd;
	BOOL	m_ARerouted;
	BOOL	m_DRerouted;
	BOOL	m_DGd2d;
	CString	m_ARwya;
	CString	m_AStev;
	CString	m_ASte2;
	CString	m_ASte3;
	CString	m_ASte4;
	CString	m_AStoa;
	CString	m_AStod;
	CString	m_AStyp;
	CString	m_ATga1;
	CString	m_ATga2;
	CString	m_ATmb1;
	CString	m_ATmb2;
	CString	m_ATmoa;
	CString	m_ATtyp;
	CString	m_DAirb;
	CString	m_DAlc3;
	BOOL	m_DCxx;
	CString	m_DDcd1;
	CString	m_DDcd2;
	CString	m_DDes3;
	CString	m_DDes4;
	CString	m_DDtd1;
	CString	m_DDtd2;
	CString	m_DEtdc;
	CString	m_DEtdi;
	CString	m_DEtod;
	CString	m_DFlns;
	CString	m_DGd1x;
	CString	m_DGd1y;
	CString	m_DGd2x;
	CString	m_DGd2y;
	CString	m_DGtd1;
	CString	m_DGtd2;
	CString	m_DHtyp;
	CString	m_DIfrd;
	CString	m_DIskd;
	CString	m_DLastChange;
	CString	m_DLastChangeTime;
	CString	m_DNxti;
	CString	m_DOfbl;
	CString	m_DPstd;
	CString	m_DRem1;
	CString	m_DRemp;
	CString	m_DGatLogo;
	CString	m_DGatLogo2;
	CString	m_DRwyd;
	CString	m_DSlot;
	CString	m_DStev;
	CString	m_DSte2;
	CString	m_DSte3;
	CString	m_DSte4;
	CString	m_DStoa;
	CString	m_DStod;
	CString	m_DStyp;
	CString	m_DTgd1;
	CString	m_DTgd2;
	CString	m_DTtyp;
	CString	m_DTwr1;
	CString	m_DWro1;
	CString	m_ADes3;
	CString	m_AOrg3;
	CString	m_AOrg3L;
	CString	m_ATet1;
	CString	m_ATet2;
	CString	m_DW1ba;
	CString	m_DW1ea;
	CString	m_DOrg3;
	CString	m_AAlc3;
	CString	m_DFltn;
	CString	m_DFlti;
	CString	m_ACsgn;
	CString	m_DCsgn;
	CString	m_ABbaa;
	CString	m_DBbfa;
	CString	m_DBaz1;
	CString	m_DBaz4;
	CString	m_DBao1;
	CString	m_DBac1;
	CString	m_DBao4;
	CString	m_DBac4;
	CString	m_ADivr;
	CString	m_DDivr;
	CCSEdit	m_CE_AEtdi;
	CString	m_DTobt;
	CString	m_DTsat;
	CString	m_AEtdi;
	CCSButtonCtrl	m_CB_DWro3List;
	CCSEdit	m_CE_DWro3;
	CString	m_DWro3;
	CCSEdit	m_CE_DTwr3;
	CString	m_DTwr3;
	CCSEdit	m_CE_DW3ba;
	CString	m_DW3ba;
	CCSEdit	m_CE_DW3ea;
	CString	m_DW3ea;
	CString	m_DCiFr;
	CCSEdit	m_CE_DCiFr;
	CCSEdit	m_CE_ChuFrTo;
	CCSButtonCtrl	m_CB_Load_View_Arr;
	CCSButtonCtrl	m_CB_Load_View_Dep;
	CCSButtonCtrl	m_CB_APca;
	CCSButtonCtrl	m_CB_DPca;
	CCSButtonCtrl	m_CB_APlb;
	CCSButtonCtrl	m_CB_DPlb;
	CStatic	m_CE_DCinsBorderExt;
	CComboBox	m_ComboAHistory;
	CComboBox	m_ComboDHistory;
	CComboBox	m_ComboDcins;
	CCSButtonCtrl *pomBTCRCFPSA; 
	CCSButtonCtrl *pomBTCRCAGTA1;
	CCSButtonCtrl *pomBTCRCAGTA2;
	CCSButtonCtrl *pomBTCRCRESOA;
	CCSButtonCtrl *pomBTCRCFPSD; 
	CCSButtonCtrl *pomBTCRCDGTD1;
	CCSButtonCtrl *pomBTCRCDGTD2;
	CCSButtonCtrl *pomBTCRCRESOD;
	CCSButtonCtrl *pomBTCRCABLT1;
	CCSButtonCtrl *pomBTCRCABLT2;

	CComboBox	  *pomDRgd1;
	CComboBox	  *pomDRgd2;
	CCSButtonCtrl *pomBTFlightDataTool;//Button to launch "Flight Data Tool"
	CStatic		  *pomLblPosArr;//Label 'Position" for Arrival
	CStatic       *pomLblPosDep;//Label 'Position" for Departure
	CStatic		m_CS_DISKD;
	CCSButtonCtrl	m_CB_GoAround;
	BOOL	m_GoAround;
	//}}AFX_DATA

//MWO: 13.04.05 Multi Delay codes
	CCSButtonCtrl m_DelayArrBtn;
	CCSButtonCtrl m_DelayDepBtn;
	MultiDelayDlg *pomMultiDelayDlg;
	CString GetToolTipForDelaysArrival();
	CString GetToolTipForDelaysDepature();
//END MWO: 13.04.05 Multi Delay codes

	CTAB tabDelayArrival,
		 tabDelayDeparture;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(RotationDlg)
	public:
	virtual BOOL DestroyWindow();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	RotationLoadDlgWAW*	polRotationALoadDlg;
	RotationLoadDlgWAW*	polRotationDLoadDlg;

	static int CompareCcas(const CCADATA **e1, const CCADATA **e2);
	static int CompareRotationFlight(const ROTATIONDLGFLIGHTDATA **e1, const ROTATIONDLGFLIGHTDATA **e2);

	long lmCalledBy;
	long lmRkey;

	CString omArrFlightPermitInfo, omDepFlightPermitInfo;
	void SetArrPermitsButton();
	void SetDepPermitsButton();
	CTime ConvertFlightPermitTime(CTime opTime);

	ROTATIONDLGFLIGHTDATA *prmAFlight;
	ROTATIONDLGFLIGHTDATA *prmDFlight;
	ROTATIONDLGFLIGHTDATA *prmAFlightSave;
	ROTATIONDLGFLIGHTDATA *prmDFlightSave;

	void InitTables();
	//MWO: 13.04.05 Muli Delay codes
	void InitTabs();

	void SetSecState();

	bool InitHistoryCombos();
	bool SetHistoryNamesForEdit();
	void CallHistory(CString opField);

	bool bmInit;
	CWnd *pomParent;
	CCSTable *pomFlightTable;
	bool bmIsJfnoShown;
	int imAJfnoColumnPos;
	bool bmIsCheckInNew;
	bool bmDlgEnabled;
	bool bmChanged;

	CString	omAAlc2;
	CString omAAlc3;
	CString	omDAlc2;
	CString omDAlc3;

	CCSPtrArray<JFNODATA> omAJfno;
	CCSPtrArray<JFNODATA> omDJfno;
	CCSPtrArray<CCADATA> omDCins;
	CCSPtrArray<CCADATA> omDCinsSave;

	void Settestdefault();
	void InitDialog(bool bpArrival = true, bool bpDeparture = true);
	void ClearAll();
	bool CheckAll(CString &opGMess, CString &opAMess, CString &opDMess);
	bool CheckAll2(CString &opGMess, CString &opAMess, CString &opDMess);
	void ResetDatesForFlight();

	void UpdateCashBtnStatus(CCSButtonCtrl* ctrl,CString olCash,char cpPaid);

	bool AddBltCheck();

	void EnableGlobal();
	void EnableArrival();
	void EnableDeparture();

	void AShowJfnoButton();
	void DShowJfnoButton();

	void GetCcas();	
	void DShowCinsTable();
	void DShowChaTable();

	void UpdateCashButton(const CString &ropAlc3, char cpPaid);

	//functions for handling postflights
	BOOL HandlePostFlight();
	BOOL CheckPostFlight();

	CCSTable *pomAJfnoTable;
	CCSTable *pomDJfnoTable;
	//CCSTable *pomAViaTable;
	//CCSTable *pomDViaTable;
	CCSTable *pomDCinsTable;
	CCSTable *pomDChuTable;

//	ViaTableCtrl *pomAViaCtrl;
//	ViaTableCtrl *pomDViaCtrl;

	bool bmAutoSetBaz1;
	bool bmAutoSetBaz4;

	bool bmArrival;
	bool bmDeparture;

	bool bmIsAFfnoShown;
	bool bmIsDFfnoShown;

	bool bmLocked;

	char pcmAOrg4[5];
	char pcmDOrg4[5];
	char pcmADes4[5];
	char pcmDDes4[5];

	char pcmAOrg3[4];
	char pcmDDes3[4];

	CString omAdid; // Zeigt an ob Rundflug links oder rechts dargestellt wird

	bool bmLocalTime; // Zeiten in LocalTime oder UTCTime anzeigen?
	bool bmRotationReadOnly;
	void OnAct3list();

	CStringArray omCicLogoArray;

	CCSEdit* pomBTCRCom;//Christine :to show the comment for payment
	CStatic* pomLBLCCom;//Christine :to show the comment for payment
	
	
	CCSButtonCtrl m_CashArrBtn;
	CCSButtonCtrl m_CashDepBtn;

	CCSButtonCtrl* pomBTActivity;
	void ArrangeTabsOrder();

	
private:
	void CreateDynamicControls();  
	CString GetSelectedReason(CString olField,CString olSelStr); 
	void CheckForChangeReasons(CCSButtonCtrl* ropButtonCtrl,const CString& ropValue,
						  const CString& ropChangedValue,bool* bpReasonFlag, char cpAdid); 

	struct REASONS
	{
		CString olPstaUrno;
		CString olAGta1Urno;
		CString olAGta2Urno;
		CString olPstdUrno;
		CString olDGtd1Urno;
		CString olDGtd2Urno;
		CString olAGtd2Urno;
		CString olABlt1Urno;
		CString olABlt2Urno;
		bool blPstaResf;
		bool blAGta1Resf;
		bool blAGta2Resf;
		bool blPstdResf;
		bool blDGtd1Resf;
		bool blDGtd2Resf;
		bool blDGta1Resf;
		bool blDGta2Resf;
		bool blABlt1Resf;
		bool blABlt2Resf;

		REASONS(void)
		{
			olPstaUrno="";  olPstdUrno="";
			olAGta1Urno=""; olDGtd1Urno="";	
			olAGta2Urno=""; olDGtd2Urno	="";
			olABlt1Urno="";olABlt2Urno="";
			blPstaResf = false;	 blPstdResf = false;	
			blAGta1Resf = false; blDGtd1Resf = false;
			blAGta2Resf = false; blDGtd2Resf = false;
			blABlt1Resf = false; blABlt2Resf = false;
		}			
	}omReasons;


	// Store the original values for Resason of change

	CString omPsta;
	CString omPstd;
	CString omGta1;
	CString omGta2;
	CString omGtd1;
	CString omGtd2;
	CString omBlt1;
	CString omBlt2;
	CString omPcom; //Added by Christines

	CString omPositionText;

	void LaunchFlightDataTool(CString opPermit);//Launch "Flight Data tool"
public:
	void ResetTabs();
	CString GetDelayCount(CTAB* tab);
	void ProcessFlightChange(ROTATIONDLGFLIGHTDATA *prpFlight);
	void ProcessRotationChange(ROTATIONDLGFLIGHTDATA *prpFlight);
	bool ProcessRotationChange();
	void ProcessCCA(CCADATA *prpCca, int ipDDXType);
	void ProcessFpeChange();
	void ProcessDCF(void *data, int ipDDXType);
	void ProcessChaChange(CHADATA *prpCha);


//	bool NewData(CWnd* pParent, long lpRkey, long lpUrno, CString opAdid, bool bpLocalTime);
	bool NewData(CWnd* pParent, long lpRkey, long lpUrno, CString opAdid, bool bpLocalTime, bool bpRotationReadOnly = false);
	void SetWndPos();
	void ShowAwi( CString olApc) ;
	void CheckTowingButton(void);
	void CheckREQButton(void);
	void CheckFlightReportButton(void);
	void SetModus(bool bpRotationReadOnly);
	void ReloadCCA();
	void SaveToReg();
	void UpdateVipButton(bool blRead = true) ;
	void SetFixedControls();

	void UpdateCashButton2(const CString &ropAlc3, char cpPaid) ;
	
	CString GetMopa(long flighturno);


	void SetModeOfPayControls();

// Implementation
private:
	bool ConvertUtcTOLocal (ROTATIONDLGFLIGHTDATA *prpFlight, bool bpArr, bool bpHopo = false);
	bool ConvertLocalTOUtc (ROTATIONDLGFLIGHTDATA *prpFlight, bool bpArr, bool bpHopo = false);
	bool ConvertStructUtcTOLocal(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight);
	bool ConvertStructLocalTOUtc(ROTATIONDLGFLIGHTDATA *prpAFlight, ROTATIONDLGFLIGHTDATA *prpDFlight);

	// checkin-counter has actual openingtime (-1L), otherwise (0L); ipLineNo = line from table
	LONG CcaHasNoOpentime(int ipLineNo);
	void ArrivalTifd(CTime&); 
	void ArrivalTifa(CTime&); 
	void DepartureTifd(CTime&); 
	void DepartureTifa(CTime&); 

	void ArrivalSchedTifd(CTime&); 
	void ArrivalSchedTifa(CTime&); 
	void DepartureSchedTifd(CTime&); 
	void DepartureSchedTifa(CTime&); 

	void ArrivalAktTifd(CTime&); 
	void ArrivalAktTifa(CTime&); 
	void DepartureAktTifd(CTime&); 
	void DepartureAktTifa(CTime&); 

	void ManageDelayCode(int ID, const CString &ropSecStr);
	void ManageBasicData(int ID, CString opTab, CString opFields, CString opSort, CString opShow, const CString &ropSecStr);

	bool ShowCodeShareListBox(int &ripSel);
	CUIntArray olCicCoIxs;

	void ReadCcaData();
	void ReadChaData();

	void Register();
	void AutoLock();

	CString GetGatFlgu();

	void ReadFlzData();
	void SaveLogo();

	CedaCcaData omCcaData;
	CedaChaData omChaData;
	CedaFlzData omFlzData;
	CCSButtonCtrl* pomAVia;
	CCSButtonCtrl* pomDVia;
	CCSButtonCtrl* pomArrPermitsButton;
	CCSButtonCtrl* pomDepPermitsButton;
	RotationViaDlg*	polRotationAViaDlg;
	RotationViaDlg*	polRotationDViaDlg;
	void CheckViaButton(void);
	void CheckCodeShareButton(void);
	void EnableDepBelts(boolean enable);
	DlgResizeHelper m_resizeHelper;

	bool bmActSelect;
	bool bmCheckRegn;

	bool InitDcinsCombo();
	bool CallDcins(int ipFactor);
	void SetCashCommentDisplay();
	void SetCashButtonInBothFlights();
	void SetChocksOnOffControls();
	
	CCSEdit* pomCEChocksOn;
	CStatic* pomLBLCEChocksOn;

	CCSEdit* pomCEChocksOf;
	CStatic* pomLBLCEChocksOf;

	//For fixed resources
	CButton *pomCBFPSA; 
	CButton *pomCBFGA1;
	CButton *pomCBFGA2;
	CButton *pomCBFBL1;
	CButton *pomCBFBL2;
	CButton *pomCBFPSD;
	CButton *pomCBFGD1;
	CButton *pomCBFGD2;

	//Mode of payment buttons
	CModeOfPay*	polAModeOfPay;
	CModeOfPay*	polDModeOfPay;
	CCSButtonCtrl* pomAModeOfPay;
	CCSButtonCtrl* pomDModeOfPay;


	CString m_key; 
/*
	CButton* pomAPca;
	CButton* pomDPca;
	CButton* pomAPlb;
	CButton* pomDPlb;
*/
protected:

	// Generated message map functions
	//{{AFX_MSG(RotationDlg)

	
	afx_msg void OnDVip();
	afx_msg void OnAVip();
	afx_msg void OnAFpsa();
	afx_msg void OnDFpsd();
	virtual void OnCancel();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnARep();
	afx_msg void OnDRep();
	afx_msg void onArrPermitsButton();
	afx_msg void onDepPermitsButton();
	afx_msg void OnDDelay();
	afx_msg void OnVipADlg();
	afx_msg void OnVipDDlg();
	afx_msg void OnLock();
	afx_msg void OnSeasonMask();
	afx_msg void OnAshowjfno();
	afx_msg void OnDshowjfno();
	afx_msg void OnSplit();
	afx_msg void OnJoin();
	afx_msg void OnPaid();
	afx_msg LONG OnEditKillfocus( UINT wParam, LPARAM lParam);
	afx_msg LONG OnEditChanged( UINT wParam, LPARAM lParam);
	afx_msg LONG OnTableIPEditKillfocus( UINT wParam, LPARAM lParam);
	afx_msg void OnAAlc3LIST();
	afx_msg void OnAAlc3LIST2();
	afx_msg void OnAAlc3LIST3();
	afx_msg void OnAblt1list();
	afx_msg void OnDblt1list();
	afx_msg void OnAblt2list();
	afx_msg void OnAdcd1list();
	afx_msg void OnAdcd2list();
	afx_msg void OnAgta1list();
	afx_msg void OnAgta2list();
	afx_msg void OnAhtyplist();
	afx_msg void OnAorg3list();
	afx_msg void OnAstyplist();
	afx_msg void OnAttyplist();
	afx_msg void OnDalc3list();
	afx_msg void OnDalc3list2();
	afx_msg void OnDalc3list3();
	afx_msg void OnDcinslist1();
	afx_msg void OnDcinslist2();
	afx_msg void OnDcicLogo1();
	afx_msg void OnDcicLogo2();
	afx_msg void OnDcicLogo3();
	afx_msg void OnDcicLogo4();
	afx_msg void OnDdcd1list();
	afx_msg void OnDdcd2list();
	afx_msg void OnDdes3list();
	afx_msg void OnDgta1list();
	afx_msg void OnDgta2list();
	afx_msg void OnDhtyplist();
	afx_msg void OnDstyplist();
	afx_msg void OnDttyplist();
	afx_msg void OnDwro1list();
	afx_msg void OnDpstdlist();
	afx_msg void OnApstalist();
	afx_msg void OnDcinslist3();
	afx_msg void OnDcinslist4();
	afx_msg void OnAext1list();
	afx_msg void OnAext2list();
	afx_msg void OnAct35list();
	afx_msg void OnDcxx();
	afx_msg void OnAcxx();
	afx_msg void OnDgd2d();
	afx_msg void OnADiverted();
	afx_msg void OnDDiverted();
	afx_msg void OnARerouted();
	afx_msg void OnDRerouted();
	afx_msg void OnSelchangeDGatLogo();
	afx_msg void OnSelchangeDremp();
	afx_msg void OnSelchangeAremp();
	afx_msg void OnSelchangeACxxReason();
	afx_msg void OnSelchangeDCxxReason();
	afx_msg void OnCkeckList_Dep();
	afx_msg void OnANfes();
	afx_msg void OnDNfes();
	afx_msg void OnDTelex();
	afx_msg void OnATelex();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnCkeckList_Arr();
	afx_msg void OnAgent();
	afx_msg void OnObject();
	afx_msg void OnTowing();
	afx_msg void OnREQ();
	afx_msg void OnAretflight();
	afx_msg void OnArettaxi();
	afx_msg void OnDretflight();
	afx_msg void OnDrettaxi();
	afx_msg void OnDcicrem1();
	afx_msg void OnDcicrem2();
	afx_msg void OnDcicrem3();
	afx_msg void OnDcicrem4();
	afx_msg void OnDcicCodeShare1();
	afx_msg void OnDcicCodeShare2();
	afx_msg void OnDcicCodeShare3();
	afx_msg void OnDcicCodeShare4();
	afx_msg void OnAgpu();
	afx_msg void OnDgpu();
	afx_msg void OnAload();
	afx_msg void OnDload();
	afx_msg void OnALog();
	afx_msg void OnDLog();
	afx_msg LRESULT OnEditRButtonDown(UINT wParam, LPARAM lParam);
	afx_msg LONG OnTableIPEdit( UINT wParam, LPARAM lParam);
	afx_msg void OnDwro3list();
	afx_msg void OnAViewLoad();
	afx_msg void OnDViewLoad();
	afx_msg LRESULT OnViaChanged(WPARAM wParam,LPARAM lParam);
	afx_msg void OnAVia();
	afx_msg void OnDVia();
	afx_msg int OnToolHitTest( CPoint point, TOOLINFO* pTI ) const;
	afx_msg BOOL OnToolTipNotify( UINT id, NMHDR * pNMHDR, LRESULT * pResult );
	afx_msg void OnSize(UINT nType, int cx, int cy); 
	afx_msg void OnAPca();
	afx_msg void OnDPca();
	afx_msg void OnAPlb();
	afx_msg void OnDPlb();
	afx_msg LRESULT OnEditDbClk( UINT wParam, LPARAM lParam);
	
	afx_msg void OnDelaybtnArrival();
	afx_msg void OnDelaybtnDeparture();
	afx_msg void OnSelchangeAHistory();
	afx_msg void OnSelchangeDHistory();
	afx_msg void OnSelchangeDcins();
	afx_msg void OnClickCrcPsta() ;  
	afx_msg void OnClickCrcAGta1();
	afx_msg void OnClickCrcAGta2();
	afx_msg void OnClickCrcResoA();
	afx_msg void OnClickCrcPstd() ;
	afx_msg void OnClickCrcDGtd1();
	afx_msg void OnClickCrcDGtd2();
	afx_msg void OnClickCrcResoD();
	afx_msg void OnClickCrcDataTool();
	afx_msg void OnClickDelayArrival();
	afx_msg void OnGoAround();
	afx_msg void OnCashArr();
	afx_msg void OnCashDep();
	afx_msg void OnFixedAllocaton(); 
	afx_msg void OnClickActivities(); 
	afx_msg void OnAPay();
	afx_msg void OnDPay();
	afx_msg void OnClickCrcABlt1();
	afx_msg void OnClickCrcABlt2();
	afx_msg void OnDcinsmal4();

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};



//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROTATIONDLG_H__8ECFE3F1_5ABB_11D1_B3FC_0000B45A33F5__INCLUDED_)
