//This Class is created by sisl for the support of multiple selection of 
//Positions and Positions Groups in the Flight Search Page.

#if !defined(AFX_PSSEARCHPOSDLG_H__825A1800_F5AF_4E74_88C9_BCC1A1415B57__INCLUDED_)
#define AFX_PSSEARCHPOSDLG_H__825A1800_F5AF_4E74_88C9_BCC1A1415B57__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PSSearchPosDlg.h : header file
//
#include "resrc1.h"
#include "AwBasicDataDlg.h"
/////////////////////////////////////////////////////////////////////////////
// PSSearchPosDlg dialog

class PSSearchPosDlg : public AwBasicDataDlg
{
// Construction
public:
	PSSearchPosDlg(CWnd* pParent = NULL, CString opObject = "" , CString opFields = "", CString opSortFields = "", CString opSelStrings = "");   // standard constructor
	~PSSearchPosDlg()
	{

	}
	PSSearchPosDlg(CWnd* pParent , CString opObject  , CString opFields , CString opSortFields , CString opSelStrings ,CStringArray &olPosGrpstr,bool bpPosGrp);
	CString GetField();
private:
// Dialog Data
	//{{AFX_DATA(PSSearchPosDlg)
	enum { IDD = IDD_PSSEARCH_POSITION };
			// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA
	
	CStringArray *omPosGrparr;
	bool bmPosGrp;
	CString omObject;
	CString omFields;
	CString omSortFields;
	CString omSelStrings;
	int imLastSortIndex;
	CString omPostionStr;
	char cmSecState;
	RecordSet *pomRecord;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(PSSearchPosDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(PSSearchPosDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PSSEARCHPOSDLG_H__825A1800_F5AF_4E74_88C9_BCC1A1415B57__INCLUDED_)
