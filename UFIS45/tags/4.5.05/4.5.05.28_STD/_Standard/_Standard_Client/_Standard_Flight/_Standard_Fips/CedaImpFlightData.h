// CedaImpFlightData.h
#ifndef __CedaImpFlightData__

#define __CedaImpFlightData__
 
#include <CCSGlobl.h>
#include <CCSPtrArray.h>
#include <CCSCedadata.h>
#include <CCSddx.h>
#include <ProgressDlg.h>


//---------------------------------------------------------------------------------------------------------

struct IMPFLIGHTDATA 
{
	char 	 Stox[5];	// Planm��ige An-Abflugzeit alternativ
	char 	 Flno[11];	// komplette Flugnummer (Airline, Nummer, Suffix)
    char	 ArDe[2];	// Arr - Dep kein DB Feld
    char	 Empt[12];	// Unbekanntes leeres Feld
	char 	 Orde[5];	// Ausgangsflughafen 3-Lettercode    alternativ
	char 	 Ttyp[4];	// Verkehrsart
	char 	 Flnj[11];	// komplette Flugnummer            Partner
	char 	 Stoj[5];	// Planm��ige Ankunfts- Abflugzeit Partner
	char 	 Ardj[4];
	char 	 Act5[6];	// Ausgangsflughafen               Partner
	char 	 X001[2];	// Leeres Feld
	char 	 X002[2];	// Leeres Feld
	char 	 X003[4];	// Leeres Feld
	char 	 X004[2];	// Leeres Feld
	char 	 X005[4];	// Leeres Feld
	char 	 X006[2];	// Leeres Feld
	char 	 X007[2];	// Leeres Feld
	char 	 X008[4];	// Leeres Feld
	char 	 X009[2];	// Leeres Feld
	char 	 X010[4];	// Leeres Feld
	char 	 X011[4];	// Leeres Feld
	char 	 X012[2];	// Leeres Feld
	char 	 X013[2];	// Leeres Feld
	char 	 X014[4];	// Leeres Feld
	char 	 X015[2];	// Leeres Feld
	char 	 X016[4];	// Leeres Feld
	char 	 X017[2];	// Leeres Feld
	char 	 X018[2];	// Leeres Feld
	char 	 X019[4];	// Leeres Feld
	char 	 X020[2];	// Leeres Feld
	char 	 X021[3];	// Leeres Feld
	char 	 Gate[3];	// Gate Arr. oder Gate Dep. (- A/D)
	char 	 Pstx[3];	// Postion PSTA/PSTD (- A/D)
	char 	 X023[3];	// Leeres Feld
	char 	 Viax[4];	// Vial 
	char 	 X024[2];	// Leeres Feld
	char 	 X025[4];	// Leeres Feld
	//char 	 X026[5];	// Leeres Feld
	char 	 Cicf[5];	// Checkin-Schalterzeit
	char 	 Twr1[2];	// Warteraum Terminal
	char 	 X027[2];	// Leeres Feld
	char 	 X028[4];	// Leeres Feld
	char 	 X029[5];	// Leeres Feld
	char 	 Sto2[7];	// Planm��ige An-Abflugzeit alternativ - Datum (s. Feld 1)
	char 	 Line[200];	// Ganze Zeile (nicht in Rec-List)


	IMPFLIGHTDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
	}

}; // end IMPFLIGHTDATAStrukt

struct JOINLIST
{
	SEASONDLGFLIGHTDATA *prlFlight;
	char	Flnj[11];
	char	Stoj[5];
	char	ArDe[2];
	char 	Cicf[5];	

}; // end JOINLIST


struct FLNOKEYLIST
{
	CCSPtrArray<JOINLIST> JoinData;

}; // end FLNOKEYLIST


struct ALTSTRUCT
{
	char 	 Alc2[4]; 	// Fluggesellschaft 2-Letter Code
	char 	 Alc3[5]; 	// Fluggesellschaft 3-Letter Code
}; 

struct ACTSTRUCT
{
	char	Act3[5]; 	// Flugzeug-Typ 3-Letter Code (IATA)
	char	Act5[7]; 	// Flugzeug-Typ 5-Letter Code (ICAO)
}; 

struct APTSTRUCT
{
	char	Apc3[5]; 	// Flughafen 3-Letter Code
	char	Apc4[6]; 	// Flughafen 4-Letter Code

};








//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaImpFlightData: public CCSCedaData
{
// Attributes
public:
	CMapStringToPtr omFlnoKeyMap;

	CMapStringToPtr omAlc2KeyMap;
	CMapStringToPtr omAlc3KeyMap;
	CMapStringToPtr omApc3KeyMap;
	CMapStringToPtr omApc4KeyMap;
	CMapStringToPtr omPnamKeyMap;
	CMapStringToPtr omWnamKeyMap;
	CMapStringToPtr omAct3KeyMap;
	CMapStringToPtr omAct5KeyMap;
	CMapStringToPtr omTtypKeyMap;
	CMapStringToPtr omGnamKeyMap;


    //CCSPtrArray<IMPFLIGHTDATA> omData;
    CCSPtrArray<SEASONDLGFLIGHTDATA> omFlights;
	char pcmListOfFields[2048];
	CProgressDlg *polProgress;
// Operations
public:
    CedaImpFlightData();
	~CedaImpFlightData();
	void Register(void);
	void DeleteFlights(void);
	//bool Stammdaten(void);
	int MinMaxDate(char *pcMin ,char * pcMax, CStdioFile *popLogfile);
	//bool DaysinPeriod(CTime olMinDate, CTime olMaxDate, CString &olDaynumbers);
	void CreateFlights(CStdioFile *popLogfile);
	void OneFlight(IMPFLIGHTDATA *prlImp, int ilLinecnt, CStdioFile *popLogfile);
	bool Import(char *pcgPath);
	void CreateFlno(char* pcpFlno, CString &olFlno, CString &opAlc, CString &opFltn, CString &opFlns, bool *blOk);
	void ClearAll(bool bpWithRegistration = true);



private:
	CCSPtrArray<CCADATA> omDCins;
	CCSPtrArray<CCADATA> omDCinsSave;

};

//---------------------------------------------------------------------------------------------------------


#endif //__CedaImpFlightData__
