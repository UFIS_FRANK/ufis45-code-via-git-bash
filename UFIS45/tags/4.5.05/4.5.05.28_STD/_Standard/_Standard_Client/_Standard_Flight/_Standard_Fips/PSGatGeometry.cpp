// PSGatGeometry.cpp : implementation file
//

#include <stdafx.h>
#include <CCSEdit.h>
#include <CCSGlobl.h>
#include <PSGatGeometry.h>
#include <UniEingabe.h>
#include <BasicData.h>
#include <CedaBasicData.h>
#include <Utils.h>
#include <CCSTime.h>
#include <StringConst.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PSGatGeometry property page

IMPLEMENT_DYNCREATE(PSGatGeometry, CPropertyPage)


PSGatGeometry::PSGatGeometry() : CPropertyPage(PSGatGeometry::IDD)
{
	//{{AFX_DATA_INIT(PSGatGeometry)
	m_GroupValue = -1;
	//}}AFX_DATA_INIT
    bmNotMoreThan12 = FALSE;
	pomCurrentGroup = NULL;
	blIsInit = false;
	pomGroupTable = new CCSTable;
    pomGroupTable->tempFlag = 2;
	bmMaxDefault = true;
	bmLinesDefault = true;
	bmTimeDefault = true;
	bmVertDefault = true;
	bmDisplayBeginDefault = true;
}

PSGatGeometry::~PSGatGeometry()
{
	if(pomGroupTable != NULL)
	{
		delete pomGroupTable;
	}
	for(int i = omGroupData.GetSize()-1; i >= 0; i-- )
	{
		omGroupData[i].Codes.DeleteAll();
	}
	omGroupData.DeleteAll();  
}
void PSGatGeometry::SetCalledFrom(CString opCalledFrom)
{
	omCalledFrom = opCalledFrom;
}
void PSGatGeometry::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PSGatGeometry)
	DDX_Control(pDX, IDC_TEST, m_TestText);
	DDX_Control(pDX, IDC_FONT, m_Font);
	DDX_Control(pDX, IDC_STARTDATE, m_Date);
	DDX_Control(pDX, IDC_RB_MIN, m_Min);
	DDX_Control(pDX, IDC_RB_MAX, m_Max);
	DDX_Control(pDX, IDC_NEW, m_New);
	DDX_Control(pDX, IDC_LIST1, m_ResList);
	DDX_Control(pDX, IDC_HEIGHT, m_Height);
	DDX_Control(pDX, IDC_FLT_TIMELINES, m_TimeLines);
	DDX_Control(pDX, IDC_DELETE, m_Delete);
	DDX_Control(pDX, IDC_24H, m_24h);
	DDX_Control(pDX, IDC_12H, m_12h);
	DDX_Control(pDX, IDC_10H, m_10h);
	DDX_Control(pDX, IDC_8H, m_8h);
	DDX_Control(pDX, IDC_6H, m_6h);
	DDX_Control(pDX, IDC_4H, m_4h);
	DDX_Control(pDX, IDC_100PERCENT, m_100Proz);
	DDX_Control(pDX, IDC_75PERCENT, m_75Proz);
	DDX_Control(pDX, IDC_50PERCENT, m_50Proz);
	DDX_Control(pDX, IDC_GRUPPE, m_GRUPPE);
	DDX_Control(pDX, IDC_STARTTIME, m_Time);
	DDX_Radio(pDX, IDC_GRP1, m_GroupValue);
	DDX_Control(pDX, IDC_GRP1, m_GroupBy1);
	DDX_Control(pDX, IDC_GRP2, m_GroupBy2);
	DDX_Control(pDX, IDC_GRP3, m_GroupBy3);
	DDX_Control(pDX, IDC_GRP4, m_GroupBy4);
	DDX_Control(pDX, IDC_GRP5, m_GroupBy5);

	//}}AFX_DATA_MAP
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		SetData();
		if(omCurrentGroupBy == "P")
		{
			m_GroupBy1.SetCheck(1);			
			m_GroupBy2.SetCheck(0);
			m_GroupBy3.SetCheck(0);
			m_GroupBy4.SetCheck(0);
			m_GroupBy5.SetCheck(0);
			OnGrp1();
		}
		else if(omCurrentGroupBy == "G")
		{
			m_GroupBy1.SetCheck(0);			
			m_GroupBy2.SetCheck(1);
			m_GroupBy3.SetCheck(0);
			m_GroupBy4.SetCheck(0);
			m_GroupBy5.SetCheck(0);
			OnGrp2();
		}
		else if(omCurrentGroupBy == "C")
		{
			m_GroupBy1.SetCheck(0);			
			m_GroupBy2.SetCheck(0);
			m_GroupBy3.SetCheck(1);
			m_GroupBy4.SetCheck(0);
			m_GroupBy5.SetCheck(0);
			OnGrp3();
		}
		else if(omCurrentGroupBy == "B")
		{
			m_GroupBy1.SetCheck(0);			
			m_GroupBy2.SetCheck(0);
			m_GroupBy3.SetCheck(0);
			m_GroupBy4.SetCheck(1);
			m_GroupBy5.SetCheck(0);
			OnGrp4();
		}
		else if(omCurrentGroupBy == "W")
		{
			m_GroupBy1.SetCheck(0);			
			m_GroupBy2.SetCheck(0);
			m_GroupBy3.SetCheck(0);
			m_GroupBy4.SetCheck(0);
			m_GroupBy5.SetCheck(1);
			OnGrp5();
		}
	}
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		GetData();
	}

}


BEGIN_MESSAGE_MAP(PSGatGeometry, CPropertyPage)
	//{{AFX_MSG_MAP(PSGatGeometry)
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDC_NEW, OnNew)
	ON_BN_CLICKED(IDC_GEG, OnGeg)
	ON_BN_CLICKED(IDC_PFC, OnPfc)
	ON_BN_CLICKED(IDC_10H, On10h)
	ON_BN_CLICKED(IDC_12H, On12h)
	ON_BN_CLICKED(IDC_24H, On24h)
	ON_BN_CLICKED(IDC_4H, On4h)
	ON_BN_CLICKED(IDC_6H, On6h)
	ON_BN_CLICKED(IDC_8H, On8h)
	ON_LBN_SELCHANGE(IDC_LIST1, OnSelchangeList1)
    ON_MESSAGE(WM_TABLE_DRAGBEGIN, OnTableDragBegin)
	ON_MESSAGE(WM_DRAGOVER, OnDragOver)
	ON_MESSAGE(WM_DROP, OnDrop)
	ON_MESSAGE(WM_TABLE_LBUTTONDOWN, OnTableLButtonDown)
	ON_BN_CLICKED(IDC_DELETE, OnDelete)
	ON_BN_CLICKED(IDC_GRP1, OnGrp1)
	ON_BN_CLICKED(IDC_GRP2, OnGrp2)
	ON_BN_CLICKED(IDC_GRP3, OnGrp3)
	ON_BN_CLICKED(IDC_GRP4, OnGrp4)
	ON_BN_CLICKED(IDC_GRP5, OnGrp5)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PSGatGeometry message handlers

BOOL PSGatGeometry::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

	m_DragDropTarget.RegisterTarget(this, this);

	CRect rect;
	m_GRUPPE.GetWindowRect(&rect);
    rect.InflateRect(1, 1);     // hiding the CTable window border
	ScreenToClient(rect);
    pomGroupTable->SetTableData(this, rect.left, rect.right, rect.top, rect.bottom, ::GetSysColor(COLOR_WINDOWTEXT), ::GetSysColor(COLOR_WINDOW), ::GetSysColor(COLOR_HIGHLIGHTTEXT), ::GetSysColor(COLOR_HIGHLIGHT), &ogMSSansSerif_Regular_8);

    if (bmNotMoreThan12)
         GetDlgItem(IDC_24H) -> EnableWindow(FALSE);

    
    m_Height.SetRange(0, 120);
	m_Height.SetTicFreq(10);
    m_Height.SetPos(m_Hour * ONE_HOUR_POINT);

	m_Font.SetRange(0,19);
	m_Font.SetTicFreq(1);
    m_Font.SetPos(m_FontHeight);

	m_100Proz.ShowWindow(SW_HIDE);
	m_75Proz.ShowWindow(SW_HIDE);
	m_50Proz.ShowWindow(SW_HIDE);

//Fill the content of GroupBy Listbox

	/*
	m_GroupBy1.SetWindowText(LoadStg(IDS_STRING1378));
	m_GroupBy2.SetWindowText(LoadStg(IDS_STRING1379));
	m_GroupBy3.SetWindowText(LoadStg(IDS_STRING1380));
	m_GroupBy4.SetWindowText(LoadStg(IDS_STRING1381));
	m_GroupBy5.SetWindowText(LoadStg(IDS_STRING1382));
*/
	
	m_GroupBy1.SetCheck(1);
	m_GroupBy2.SetCheck(0);
	m_GroupBy3.SetCheck(0);
	m_GroupBy4.SetCheck(0);
	m_GroupBy5.SetCheck(0);

	ShowTable();
	if(bmMaxDefault == true)
	{
		m_Max.SetCheck(1);
		m_Min.SetCheck(0);
	}
	if(bmVertDefault == true)
	{
		m_FontHeight = 8;
		m_Font.SetPos(m_FontHeight);
		m_TestText.SetFont(&ogScalingFonts[m_FontHeight]);
	}
	if(bmTimeDefault == true)
	{
		m_10h.SetCheck(1);
		m_Height.SetPos(10 * ONE_HOUR_POINT);
	}
	if(bmDisplayBeginDefault == true)
	{
		CTime olTime = CTime::GetCurrentTime();
		m_Date.SetWindowText(olTime.Format("%d.%m.%Y"));
		m_Time.SetWindowText(olTime.Format("%H:%M"));
	}

	if(omCalledFrom == "COVERAGEDIA")
	{
		pomGroupTable->ShowWindow(SW_HIDE);
		m_ResList.ShowWindow(SW_HIDE);
		m_New.ShowWindow(SW_HIDE);
		m_Delete.ShowWindow(SW_HIDE);
		m_GRUPPE.ShowWindow(SW_HIDE);
	}
	m_GroupBy1.SetCheck(1);			
	m_GroupBy2.SetCheck(0);
	m_GroupBy3.SetCheck(0);
	m_GroupBy4.SetCheck(0);
	m_GroupBy5.SetCheck(0);
	OnGrp1();
	blIsInit = true;
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
void PSGatGeometry::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// TODO: Add your message handler code here and/or call default

    //CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
    //UpdateData();

    m_Hour = m_Height.GetPos() / ONE_HOUR_POINT;
    //m_24h = -1;
    if (m_Hour == 4)
	{
        m_24h.SetCheck(0);
        m_12h.SetCheck(0);
        m_10h.SetCheck(0);
        m_8h.SetCheck(0);
        m_6h.SetCheck(0);
        m_4h.SetCheck(1);
	}
    else if (m_Hour == 6)
	{
        m_24h.SetCheck(0);
        m_12h.SetCheck(0);
        m_10h.SetCheck(0);
        m_8h.SetCheck(0);
        m_6h.SetCheck(1);
        m_4h.SetCheck(0);
	}
    else if (m_Hour == 8)
	{
        m_24h.SetCheck(0);
        m_12h.SetCheck(0);
        m_10h.SetCheck(0);
        m_8h.SetCheck(1);
        m_6h.SetCheck(0);
        m_4h.SetCheck(0);
	}
    else if (m_Hour == 10)
	{
        m_24h.SetCheck(0);
        m_12h.SetCheck(0);
        m_10h.SetCheck(1);
        m_8h.SetCheck(0);
        m_6h.SetCheck(0);
        m_4h.SetCheck(0);
	}
    else if (m_Hour == 12)
	{
        m_24h.SetCheck(0);
        m_12h.SetCheck(1);
        m_10h.SetCheck(0);
        m_8h.SetCheck(0);
        m_6h.SetCheck(0);
        m_4h.SetCheck(0);
	}
    else if (m_Hour == 24)
	{
        m_24h.SetCheck(1);
        m_12h.SetCheck(0);
        m_10h.SetCheck(0);
        m_8h.SetCheck(0);
        m_6h.SetCheck(0);
        m_4h.SetCheck(0);
	}
    else if (m_Hour == 0)
    {
        m_Hour = 2;
    }

    
    if (bmNotMoreThan12 && (m_Hour > 12))
        m_Hour = 12;
    m_Height.SetPos(m_Hour * ONE_HOUR_POINT);


	int ilFontPos = m_Font.GetPos();
	m_TestText.SetFont(&ogScalingFonts[ilFontPos]);
	m_TestText.UpdateWindow();
    //UpdateData(FALSE);


/*    m_TimeScale = CTime(m_startDate.GetYear(), m_startDate.GetMonth(), m_startDate.GetDay(),
                atoi(m_startTime), 0, 0);

    if (pomTS != NULL)
    {
        pomTS->SetDisplayTimeFrame(m_TimeScale, CTimeSpan(0, m_Hour, 0, 0), omTSI);
        pomTS->Invalidate(TRUE);
    }

    if (pomTS1 != NULL)
    {
        pomTS1->SetDisplayTimeFrame(m_TimeScale, CTimeSpan(0, m_Hour, 0, 0), omTSI);
        pomTS1->Invalidate(TRUE);
    }
*/
}

void PSGatGeometry::OnNew() 
{
	CString olNewGroup;
	UniEingabe *polDlg = new UniEingabe(this, GetString(IDS_STRING1209), GetString(IDS_STRING1210), 12);
	if(polDlg->DoModal() != IDCANCEL)
	{
		olNewGroup = polDlg->m_Eingabe;
		if(!olNewGroup.IsEmpty())
		{
			CCSPtrArray<TABLE_COLUMN> olColList;

			TABLE_COLUMN rlColumnData;
			for(int i = 0; i < omGroupData.GetSize(); i++)
			{
				if(omGroupData[i].Name == olNewGroup)
				{
					char pclMsg[150]="";
					sprintf(pclMsg, GetString(IDS_STRING1211), olNewGroup);
					MessageBox(pclMsg, GetString(IDS_WARNING), MB_OK);
					return;
				}
			}

			rlColumnData.Text = olNewGroup;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			pomGroupTable->AddTextLine(olColList, (void*)NULL);
			PS_GAT_GEO_GROUP_DATA *prlGroup = new PS_GAT_GEO_GROUP_DATA;
			pomCurrentGroup = prlGroup;
			prlGroup->Name = olNewGroup;
			prlGroup->Typ = omCurrentGroupBy;
			omGroupData.Add(prlGroup);
			pomGroupTable->DisplayTable();
			olColList.DeleteAll();
		}
	}
	delete polDlg;

}

void PSGatGeometry::OnGeg() 
{
/*	int ilCount = omGegData.GetSize();
	m_ResList.ResetContent();
	for(int i = 0; i < ilCount; i++)
	{
		m_ResList.AddString(omGegData[i].Gcde);
	}
	if(pomCurrentGroup != NULL)
	{
		pomCurrentGroup->Typ = CString("G");
	}
*/
}

void PSGatGeometry::OnPfc() 
{
/*	int ilCount = omPfcData.GetSize();
	m_ResList.ResetContent();
	for(int i = 0; i < ilCount; i++)
	{
		m_ResList.AddString(omPfcData[i].Fctc);
	}
	if(pomCurrentGroup != NULL)
	{
		pomCurrentGroup->Typ = CString("F");
	}
*/
}

void PSGatGeometry::On10h() 
{
    m_Height.SetPos(10 * ONE_HOUR_POINT);
}

void PSGatGeometry::On12h() 
{
    m_Height.SetPos(12 * ONE_HOUR_POINT);
}

void PSGatGeometry::On24h() 
{
    m_Height.SetPos(24 * ONE_HOUR_POINT);
}

void PSGatGeometry::On4h() 
{
    m_Height.SetPos(4 * ONE_HOUR_POINT);
}

void PSGatGeometry::On6h() 
{
    m_Height.SetPos(6 * ONE_HOUR_POINT);
}

void PSGatGeometry::On8h() 
{
    m_Height.SetPos(8 * ONE_HOUR_POINT);
}

void PSGatGeometry::OnSelchangeList1() 
{
	if(pomCurrentGroup != NULL)
	{
		pomCurrentGroup->Codes.DeleteAll();
		int *pilItems = new int[m_ResList.GetCount()];
		
		int ilC = MyGetSelItems(m_ResList, m_ResList.GetCount(),  pilItems);
		for(int k = 0; k < ilC; k++)
		{
			CString olCode;
			m_ResList.GetText( pilItems[k], olCode);
			pomCurrentGroup->Codes.NewAt(pomCurrentGroup->Codes.GetSize(), olCode);
		}
		delete pilItems;
	}
}

LONG PSGatGeometry::OnTableDragBegin(UINT wParam, LONG lParam)
{
//	CCSTABLENOTIFY *polNotify;
//	polNotify = (CCSTABLENOTIFY*)lParam;
//	TABLE_COLUMN *prlColumn = (TABLE_COLUMN*)polNotify->Data;
//	CString olText = prlColumn->Text;
	m_DragDropSource.CreateDWordData(DIT_ANSICHT_GRP, 1);
	m_DragDropSource.AddDWord(wParam);
	m_DragDropSource.BeginDrag();

	return 0L;
}
LONG PSGatGeometry::OnDragOver(UINT, LONG)
{
	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);
	CRect olRect;
	pomGroupTable->GetWindowRect(olRect);
	int ilClass = m_DragDropTarget.GetDataClass(); 
	if (ilClass == DIT_ANSICHT_GRP)
	{
		if(olRect.PtInRect(olDropPosition) == TRUE)
			return 0;
	}
	return -1;
}

LONG PSGatGeometry::OnTableLButtonDown(UINT wParam, LONG lParam)
{
	int ilItem = wParam;
	if(ilItem != -1)
	{
		if(ilItem < omGroupData.GetSize())
		{
			pomCurrentGroup = &omGroupData[ilItem];
			ShowResList();
		}
	}
	return 0L;
}

LONG PSGatGeometry::OnDrop(UINT wParam, LONG lParam)
{
	CPoint olDropPosition;
    ::GetCursorPos(&olDropPosition);
	//ClientToScreen(&olDropPosition);
	pomGroupTable->GetCTableListBox()->ScreenToClient(&olDropPosition);
	int ilLine = pomGroupTable->GetLinenoFromPoint(olDropPosition);
	int ilItemID = m_DragDropTarget.GetDataDWord(0);
	if(ilItemID != -1 && ilLine != -1 && (ilItemID != ilLine ))
	{
		char pclMsg[50]="";

		sprintf(pclMsg, GetString(IDS_STRING1212), ilItemID, ilLine);
		PS_GAT_GEO_GROUP_DATA *polOld = new PS_GAT_GEO_GROUP_DATA;
		PS_GAT_GEO_GROUP_DATA *polNew = new PS_GAT_GEO_GROUP_DATA;
		if((ilItemID < omGroupData.GetSize()) && (ilLine < omGroupData.GetSize()))
		{
			int ilOldPos = ilItemID;
			int ilNewPos = ilLine;
			*polOld = omGroupData[ilItemID];
			*polNew = omGroupData[ilLine];
			omGroupData.DeleteAt(ilLine);
			omGroupData.InsertAt(ilNewPos, polOld);
			omGroupData.DeleteAt(ilItemID);
			pomGroupTable->DeleteTextLine(ilItemID);
			omGroupData.InsertAt(ilOldPos, polNew);
			pomCurrentGroup = &omGroupData[ilNewPos];
			ShowTable();
			pomGroupTable->SelectLine(ilNewPos);
			ShowResList();
		}

	}
	return 0L; 
}

void PSGatGeometry::ShowTable()
{
	CRect rect;
	m_GRUPPE.GetWindowRect(&rect);
    rect.InflateRect(1, 1);     // hiding the CTable window border
	ScreenToClient(rect);
	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;

	TABLE_HEADER_COLUMN rlHeader;
	pomGroupTable->SetShowSelection(TRUE);
	pomGroupTable->SetSelectMode(0);
	pomGroupTable->ResetContent();
	rlHeader.Text = CString("Gruppen:");
	rlHeader.Font = &ogMSSansSerif_Regular_8;
	rlHeader.Length = rect.right - rect.left-10; 
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	pomGroupTable->SetHeaderFields(omHeaderDataArray);
	omHeaderDataArray.DeleteAll();

	CCSPtrArray<TABLE_COLUMN> olColList;
	pomGroupTable->ResetContent();

	TABLE_COLUMN rlColumnData;
	for(int i = 0; i < omGroupData.GetSize(); i++)
	{
		CString olTmp = omGroupData[i].Typ;
		if(omGroupData[i].Typ == omCurrentGroupBy)
		{
			if(omGroupData[i].Name != CString(""))
			{
				rlColumnData.Text = omGroupData[i].Name;
				olColList.NewAt(olColList.GetSize(), rlColumnData);
				pomGroupTable->AddTextLine(olColList, (void*)NULL);
				olColList.DeleteAll();
			}
		}
	}

    
	pomGroupTable->DisplayTable();
}

void PSGatGeometry::ShowResList()
{
	m_ResList.SetSel(-1, FALSE);
	if(pomCurrentGroup != NULL)
	{
		for(int i = 0; i < pomCurrentGroup->Codes.GetSize(); i++)
		{
			int ilIdx = m_ResList.FindString(-1, pomCurrentGroup->Codes[i]);
			if(ilIdx != -1)
			{
				m_ResList.SetSel(ilIdx);
			}
		}
	}
	else
	{
		OnGrp1();
	}
}

void PSGatGeometry::OnDelete() 
{
	int ilIdx = pomGroupTable->GetCTableListBox()->GetCurSel();	
	if(ilIdx != -1)
	{
		if(ilIdx < omGroupData.GetSize())
		{
			omGroupData.DeleteAt(ilIdx);
			pomCurrentGroup = NULL;
			ShowTable();
			ShowResList();
		}
	}
}

void PSGatGeometry::SetData()
{
	omGroupData.DeleteAll();
	CStringArray olGroups;
	CString olTotalString;
	for(int i = 0; i < omValues.GetSize(); i++)
	{
		olTotalString += omValues[i];
	}
	//Ma�stab
	int ilMassFrom = olTotalString.Find('<');
	int ilMassTo = olTotalString.Find('>');
	if(ilMassFrom != -1 && ilMassTo != -1)
	{
		CString olMassString = olTotalString.Mid(ilMassFrom+1, ilMassTo-1);
		if(!olMassString.IsEmpty())
		{
			int ilMinPos = olMassString.Find('M');
			int ilLinePos = olMassString.Find('F');
			int ilTimePos = olMassString.Find('Z');
			int ilVertPos = olMassString.Find('V');
			int ilShowTimePos = olMassString.Find('T');
			if(ilMinPos != -1)
			{
				CString T = olMassString.Mid(ilMinPos+2, 1);
				if(!T.IsEmpty())
				{
					if(T == "A")
					{
						m_Max.SetCheck(1);
						m_Min.SetCheck(0);
					}
					else
					{
						m_Max.SetCheck(0);
						m_Min.SetCheck(1);
					}
					bmMaxDefault = false;
				}
			}
			if(ilLinePos != -1)
			{
				CString T = olMassString.Mid(ilLinePos+2, 1);
				if(!T.IsEmpty())
				{
					if(T == "J")
					{
						m_TimeLines.SetCheck(1);
					}
					else
					{
						m_TimeLines.SetCheck(0);
					}
					bmLinesDefault = false;
				}
			}
			if(ilTimePos != -1)
			{
				CString T = olMassString.Mid(ilTimePos+2, 2);
				if(!T.IsEmpty())
				{
					m_Hour = atoi(T);
				    m_Height.SetPos(m_Hour * ONE_HOUR_POINT);
					if (m_Hour == 4)
					{
						m_24h.SetCheck(0);
						m_12h.SetCheck(0);
						m_10h.SetCheck(0);
						m_8h.SetCheck(0);
						m_6h.SetCheck(0);
						m_4h.SetCheck(1);
					}
					else if (m_Hour == 6)
					{
						m_24h.SetCheck(0);
						m_12h.SetCheck(0);
						m_10h.SetCheck(0);
						m_8h.SetCheck(0);
						m_6h.SetCheck(1);
						m_4h.SetCheck(0);
					}
					else if (m_Hour == 8)
					{
						m_24h.SetCheck(0);
						m_12h.SetCheck(0);
						m_10h.SetCheck(0);
						m_8h.SetCheck(1);
						m_6h.SetCheck(0);
						m_4h.SetCheck(0);
					}
					else if (m_Hour == 10)
					{
						m_24h.SetCheck(0);
						m_12h.SetCheck(0);
						m_10h.SetCheck(1);
						m_8h.SetCheck(0);
						m_6h.SetCheck(0);
						m_4h.SetCheck(0);
					}
					else if (m_Hour == 12)
					{
						m_24h.SetCheck(0);
						m_12h.SetCheck(1);
						m_10h.SetCheck(0);
						m_8h.SetCheck(0);
						m_6h.SetCheck(0);
						m_4h.SetCheck(0);
					}
					else if (m_Hour == 24)
					{
						m_24h.SetCheck(1);
						m_12h.SetCheck(0);
						m_10h.SetCheck(0);
						m_8h.SetCheck(0);
						m_6h.SetCheck(0);
						m_4h.SetCheck(0);
					}
					else if (m_Hour == 0)
					{
						m_24h.SetCheck(0);
						m_12h.SetCheck(0);
						m_10h.SetCheck(0);
						m_8h.SetCheck(0);
						m_6h.SetCheck(0);
						m_4h.SetCheck(0);
						m_Hour = 2;
					}
					else
					{
						m_24h.SetCheck(0);
						m_12h.SetCheck(0);
						m_10h.SetCheck(0);
						m_8h.SetCheck(0);
						m_6h.SetCheck(0);
						m_4h.SetCheck(0);
					}
					bmTimeDefault = false;
				}
			}
			if(ilVertPos != -1)
			{
				CString T = olMassString.Mid(ilVertPos+2, 2);
				if(!T.IsEmpty())
				{
					int M = atoi(T);
					m_Font.SetPos(M);
					m_FontHeight = M;
					m_TestText.SetFont(&ogScalingFonts[m_FontHeight]);
					bmVertDefault = false;
				}
			}
			if(ilShowTimePos != -1)
			{
				CString T = olMassString.Mid(ilShowTimePos+2, 14);
				if(!T.IsEmpty())
				{
					CTime olDate;
					olDate = DBStringToDateTime(T);
					if(olDate != TIMENULL)
					{
						CString olDateStr, olTimeStr;
						olDateStr = olDate.Format("%d.%m.%Y");
						olTimeStr = olDate.Format("%H:%M");
						m_Date.SetWindowText(olDateStr);
						m_Time.SetWindowText(olTimeStr);
						bmDisplayBeginDefault = false;
					}
					else
					{
						CString olDateStr, olTimeStr;
						olDate = CTime::GetCurrentTime();
						olDateStr = olDate.Format("%d.%m.%Y");
						olTimeStr = olDate.Format("%H:%M");
						m_Date.SetWindowText(olDateStr);
						m_Time.SetWindowText(olTimeStr);
					}
				}
				else
				{
					CString olDateStr, olTimeStr;
					CTime olDate = CTime::GetCurrentTime();
					olDateStr = olDate.Format("%d.%m.%Y");
					olTimeStr = olDate.Format("%H:%M");
					m_Date.SetWindowText(olDateStr);
					m_Time.SetWindowText(olTimeStr);
				}
			}
			else
			{
				CString olDateStr, olTimeStr;
				CTime olDate = CTime::GetCurrentTime();
				olDateStr = olDate.Format("%d.%m.%Y");
				olTimeStr = olDate.Format("%H:%M");
				m_Date.SetWindowText(olDateStr);
				m_Time.SetWindowText(olTimeStr);
			}
		}
	}
	//Rest
	CString olSubString = olTotalString;
	if(!olSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		bool blEnd = false;
		CString olText;
		while(blEnd == false)
		{
			pos = olSubString.Find(';');
			if(pos == -1)
			{
				blEnd = true;
				olText = olSubString;
			}
			else
			{
				olText = olSubString.Mid(0, olSubString.Find(';'));
				olSubString = olSubString.Mid(olSubString.Find(';')+1, olSubString.GetLength( )-olSubString.Find(';')+1);
			}
			olGroups.Add(olText);
		}
	}
	for(i = 0; i < olGroups.GetSize(); i++)
	{
		PS_GAT_GEO_GROUP_DATA rlGrp;// = new PS_GAT_GEO_GROUP_DATA;
		CString olRest;
		CString olName;
		CString olPart;
		int ilPos1 = olGroups[i].Find('=');
		if(ilPos1 != -1)
		{
			olPart = olGroups[i].Mid(0, olGroups[i].Find('='));
			olRest = olGroups[i].Mid(olGroups[i].Find('=')+1, olGroups[i].GetLength());
			ilPos1 = olPart.Find('|');

			rlGrp.Name = olPart.Mid(0, ilPos1);
			olPart = olPart.Mid(olPart.Find('|')+1, olPart.GetLength());
			rlGrp.Typ = olPart;
			omCurrentGroupBy = olPart;
		}
		if(!rlGrp.Name.IsEmpty())
		{
			olSubString = olRest;
			if(!olSubString.IsEmpty())
			{
				int pos;
				int olPos = 0;
				bool blEnd = false;
				CString olText;
				while(blEnd == false)
				{
					pos = olSubString.Find('|');
					if(pos == -1)
					{
						blEnd = true;
						olText = olSubString;
					}
					else
					{
						olText = olSubString.Mid(0, olSubString.Find('|'));
						olSubString = olSubString.Mid(olSubString.Find('|')+1, olSubString.GetLength( )-olSubString.Find('|')+1);
					}
					rlGrp.Codes.NewAt(rlGrp.Codes.GetSize(), olText);
				}
			}
			omGroupData.NewAt(omGroupData.GetSize(), rlGrp);
		}
	}
	if(blIsInit == true)
	{
		ShowTable();
	}
}

void PSGatGeometry::GetData()
{
	omValues.RemoveAll();
	//Erst mal die Ma�stab-Elemente abfragen
	CString olMass;
	if(m_Min.GetCheck() == 1)
	{
		olMass += "M=I|";
	}
	else
	{
		olMass += "M=A|";
	}
	if(m_TimeLines.GetCheck() == 1)
	{
		olMass += "F=J|";
	}
	else
	{
		olMass += "F=N|";
	}
	m_Hour = m_Height.GetPos() / ONE_HOUR_POINT;
	char pclHour[100]="";
	char pclFont[5]="";
	m_FontHeight = m_Font.GetPos();
	sprintf(pclFont, "%02d", m_FontHeight);
	sprintf(pclHour, "%02ld", m_Hour);
	olMass += "Z=" + CString(pclHour) + "|";
	olMass += "V=" + CString(pclFont) + "|";
/*	if(m_100Proz.GetCheck() == 1)
	{
		olMass += "V=0|";
	}
	else if((m_75Proz.GetCheck() == 1))
	{
		olMass += "V=1|";
	}
	else if((m_50Proz.GetCheck() == 1))
	{
		olMass += "V=2|";
	}
*/
	CString olStrDate, olStrTime;
	m_Date.GetWindowText(olStrDate);
	m_Time.GetWindowText(olStrTime);
	CTime olDate, olTime;
	olDate = DateStringToDate(olStrDate);
	if(olDate != TIMENULL)
	{
		olTime = HourStringToDate(olStrTime, olDate);
		olMass += "T=" + olTime.Format("%Y%m%d%H%M00");
	}
	olMass = "<" + olMass + ">;";
	omValues.Add(olMass);
	for(int i = 0; i < omGroupData.GetSize(); i++)
	{
		if(omGroupData[i].Name != CString(""))
		{
			CString olValue;
			olValue += omGroupData[i].Name + "|" + omGroupData[i].Typ + "=";
			for(int j = 0; j < omGroupData[i].Codes.GetSize(); j++)
			{
				olValue += omGroupData[i].Codes[j] + "|";
			}
			olValue += ";";
			omValues.Add(olValue);
		}
	}
}


void PSGatGeometry::OnGrp1() 
{
	//Positions
	m_ResList.ResetContent();
	ogBCD.ShowAllFields("PST", "PNAM", &m_ResList);
	omCurrentGroupBy = "P";
	ShowTable();
//	ShowResList();
}

void PSGatGeometry::OnGrp2() 
{
	//gates
	m_ResList.ResetContent();
	ogBCD.ShowAllFields("GAT", "GNAM", &m_ResList);
	omCurrentGroupBy = "G";
	ShowTable();
//	ShowResList();
}

void PSGatGeometry::OnGrp3() 
{
	//Checkin
	m_ResList.ResetContent();
	ogBCD.ShowAllFields("CIC", "CNAM", &m_ResList);
	omCurrentGroupBy = "C";
	ShowTable();
//	ShowResList();
}

void PSGatGeometry::OnGrp4() 
{
	//BaggageBelts
	m_ResList.ResetContent();
	ogBCD.ShowAllFields("BLT", "BNAM", &m_ResList);
	omCurrentGroupBy = "B";
	ShowTable();
//	ShowResList();
}

void PSGatGeometry::OnGrp5() 
{
	//Waitingrooms
	m_ResList.ResetContent();
	ogBCD.ShowAllFields("WRO", "WNAM", &m_ResList);
	omCurrentGroupBy = "W";
	ShowTable();
//	ShowResList();
}
