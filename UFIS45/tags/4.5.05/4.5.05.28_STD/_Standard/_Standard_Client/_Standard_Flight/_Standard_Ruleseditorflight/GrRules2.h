#if !defined(AFX_GRRULES2_H__FB4A7142_F271_11D2_A1A5_0000B4984BBE__INCLUDED_)
#define AFX_GRRULES2_H__FB4A7142_F271_11D2_A1A5_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GrRules2.h : header file
//
#include "CCSTable.h"
#include "Gr2Viewer.h"

/////////////////////////////////////////////////////////////////////////////
// GrRules2 dialog

class GrRules2 : public CPropertyPage
{
	DECLARE_DYNCREATE(GrRules2)

// Construction
public:
	GrRules2();
	~GrRules2();

// Dialog Data
	//{{AFX_DATA(GrRules2)
	enum { IDD = IDD_GR_RULES2 };
	CStatic	m_GatPos2;
	//}}AFX_DATA

	CCSTable *pomNatList;

	Gr2Viewer omNatRulesViewer;

	CUIntArray omNatUrnos;
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(GrRules2)
	public:
	virtual BOOL OnSetActive();
	virtual BOOL OnKillActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	void UpdateView();
// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(GrRules2)
	virtual BOOL OnInitDialog();
	afx_msg LONG OnGridDblClk(UINT wParam, LONG lParam);
	void OnPrintNature();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRRULES2_H__FB4A7142_F271_11D2_A1A5_0000B4984BBE__INCLUDED_)
