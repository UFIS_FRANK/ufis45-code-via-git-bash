// Rules.h : main header file for the RULES application
//

#if !defined(AFX_CCSPRJ_H__FC3BA6F7_1AED_11D1_82C3_0080AD1DC701__INCLUDED_)
#define AFX_CCSPRJ_H__FC3BA6F7_1AED_11D1_82C3_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
#include "BackGround.h"		

/////////////////////////////////////////////////////////////////////////////
// CRulesApp:
// See Rules.cpp for the implementation of this class
//

class CRulesApp : public CWinApp
{

// 050228 MVy: handle Aircraft Registrations only when configuration key POSRULE_WITH_REGN is set
protected: BOOL m_bHandleAcrRegn ;
public: BOOL CanHandleAcrRegn();
		 BOOL CanHandleFlno();

protected: BOOL m_bBltFlti ;
		   BOOL m_bBltAloc ;
		   BOOL m_bPrintRules;
		   BOOL m_bCenterLine;
		   BOOL m_bPosMinMaxPax;
		   BOOL m_bBeltBag;
		   BOOL m_bHandleFlno;
public: BOOL CanHandleBltFlti();
		BOOL CanHandleBltAloc();
		BOOL CanHandlePrintRules();
		BOOL CanHandleCenterLine();
		BOOL CanHandlePosMinMaxPax();
		BOOL CanHandleBeltBag();

public:
	CRulesApp();
	~CRulesApp();

    void InitialLoad(CWnd *pParent);

	CBackGround *pomBackGround;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRulesApp)
	public:
	virtual BOOL InitInstance();
	virtual void WinHelp(DWORD dwData, UINT nCmd = HELP_CONTEXT);
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CRulesApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	afx_msg void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CCSPRJ_H__FC3BA6F7_1AED_11D1_82C3_0080AD1DC701__INCLUDED_)
