// CedaBLTData.cpp
 
#include "stdafx.h"
#include "CedaBLTData.h"

// Local function prototype
static void ProcessBLTCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaBLTData ogBltData;

CedaBLTData::CedaBLTData()
{
    // Create an array of CEDARECINFO for BLTDATA
	BEGIN_CEDARECINFO(BLTDATA,BLTDataRecInfo)
		FIELD_CHAR_TRIM	(Bnam,"BNAM")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_DATE		(Nafr,"NAFR")
		FIELD_DATE		(Nato,"NATO")
		FIELD_CHAR_TRIM	(Resn,"RESN")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Tele,"TELE")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_DATE		(Vafr,"VAFR")
		FIELD_DATE		(Vato,"VATO")
		FIELD_CHAR_TRIM	(Term, "TERM")
		FIELD_CHAR_TRIM	(Bltr, "BLTR")
	END_CEDARECINFO //(BLTDataAStruct)

	// Copy the record structure
	for (int i=0; i< sizeof(BLTDataRecInfo)/sizeof(BLTDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&BLTDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"BLT");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmBLTFieldList,"BNAM,CDAT,LSTU,NAFR,NATO,RESN,PRFL,TELE,URNO,USEC,USEU,VAFR,VATO,TERM,BLTR");
	pcmFieldList = pcmBLTFieldList;

	omData.SetSize(0,1000);
	//omUrnoMap.InitHashTable(2001);
}

void CedaBLTData::Register(void)
{
	ogDdx.Register((void *)this,BC_BLT_CHANGE,CString("BLTDATA"), CString("BLT-changed"),ProcessBLTCf);
	ogDdx.Register((void *)this,BC_BLT_DELETE,CString("BLTDATA"), CString("BLT-deleted"),ProcessBLTCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaBLTData::~CedaBLTData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaBLTData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
	omBnamMap.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaBLTData::ReadAllBLTs()
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
	omBnamMap.RemoveAll();
    omData.DeleteAll();
	ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	if (ilRc != true)
	{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		BLTDATA *prpBLT = new BLTDATA;
		if ((ilRc = GetBufferRecord(ilLc,prpBLT)) == true)
		{
			prpBLT->IsChanged = DATA_UNCHANGED;
			omData.Add(prpBLT);//Update omData
			omUrnoMap.SetAt((void *)prpBLT->Urno,prpBLT);
			omBnamMap.SetAt(prpBLT->Bnam,prpBLT);
		}
		else
		{
			delete prpBLT;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaBLTData::InsertBLT(BLTDATA *prpBLT,BOOL bpSendDdx)
{
	BLTDATA *prlBLT = new BLTDATA;
	memcpy(prlBLT,prpBLT,sizeof(BLTDATA));
	prlBLT->IsChanged = DATA_NEW;
	SaveBLT(prlBLT); //Update Database
	InsertBLTInternal(prlBLT);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaBLTData::InsertBLTInternal(BLTDATA *prpBLT)
{
	//PrepareBLTData(prpBLT);
	ogDdx.DataChanged((void *)this, BLT_CHANGE,(void *)prpBLT ); //Update Viewer
	omData.Add(prpBLT);//Update omData
	omUrnoMap.SetAt((void *)prpBLT->Urno,prpBLT);
	omBnamMap.SetAt(prpBLT->Bnam,prpBLT);
	return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaBLTData::DeleteBLT(long lpUrno)
{
	bool olRc = true;
	BLTDATA *prlBLT = GetBLTByUrno(lpUrno);
	if (prlBLT != NULL)
	{
		prlBLT->IsChanged = DATA_DELETED;
		olRc = SaveBLT(prlBLT); //Update Database
		DeleteBLTInternal(prlBLT);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaBLTData::DeleteBLTInternal(BLTDATA *prpBLT)
{
	ogDdx.DataChanged((void *)this,BLT_DELETE,(void *)prpBLT); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpBLT->Urno);
	omBnamMap.RemoveKey(prpBLT->Bnam);
	int ilBLTCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilBLTCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpBLT->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaBLTData::PrepareBLTData(BLTDATA *prpBLT)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaBLTData::UpdateBLT(BLTDATA *prpBLT,BOOL bpSendDdx)
{
	if (GetBLTByUrno(prpBLT->Urno) != NULL)
	{
		if (prpBLT->IsChanged == DATA_UNCHANGED)
		{
			prpBLT->IsChanged = DATA_CHANGED;
		}
		SaveBLT(prpBLT); //Update Database
		UpdateBLTInternal(prpBLT);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaBLTData::UpdateBLTInternal(BLTDATA *prpBLT)
{
	BLTDATA *prlBLT = GetBLTByUrno(prpBLT->Urno);
	if (prlBLT != NULL)
	{
		omBnamMap.RemoveKey(prlBLT->Bnam);
		*prlBLT = *prpBLT; //Update omData
		omBnamMap.SetAt(prlBLT->Bnam,prlBLT);
		ogDdx.DataChanged((void *)this,BLT_CHANGE,(void *)prlBLT); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

BLTDATA *CedaBLTData::GetBLTByUrno(long lpUrno)
{
	BLTDATA  *prlBLT;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlBLT) == TRUE)
	{
		return prlBLT;
	}
	return NULL;
}

//--READSPECIALBLTDATA-------------------------------------------------------------------------------------

bool CedaBLTData::ReadSpecial(CCSPtrArray<BLTDATA> &ropBlt,char *pspWhere)
{
	bool ilRc = true;
	if (pspWhere == NULL)
	{
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT",pspWhere);
	}
	if (ilRc != true)
	{
			if(!omLastErrorMessage.IsEmpty())
			{
				if(omLastErrorMessage.Find("ORA") != -1)
				{
					char pclMsg[2048]="";
					sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
					::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
					return false;
				}
			}
	}
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		BLTDATA *prpBlt = new BLTDATA;
		if ((ilRc = GetBufferRecord(ilLc,prpBlt)) == true)
		{
			ropBlt.Add(prpBlt);
		}
		else
		{
			delete prpBlt;
		}
	}
    return true;
}

long CedaBLTData::GetUrnoByBlt(char *pspPst)
{
	BLTDATA  *prlPST;
	if(strcmp(pspPst, "") == 0)
	{
		return 0;
	}

	if (omBnamMap.Lookup((LPCSTR)pspPst,(void *&)prlPST) == TRUE)
	{
		return prlPST->Urno;
	}
	return 0;
}

BLTDATA * CedaBLTData::GetBltByBnam(char *pspBnam)
{
	BLTDATA  *prlBLT;
	if (omBnamMap.Lookup(pspBnam,(void *& )prlBLT) == TRUE)
	{
		return prlBLT;
	}
	return NULL;
}

//--EXIST-I------------------------------------------------------------------------------------------------

CString CedaBLTData::BLTExists(BLTDATA *prpBLT)
{
	CString olField = "";
	BLTDATA  *prlBLT;

	if (omBnamMap.Lookup((LPCSTR)prpBLT->Bnam,(void *&)prlBLT) == TRUE)
	{
		if(prlBLT->Urno != prpBLT->Urno)
		{
			olField += GetString(IDS_STRING195);
		}
	}

	return olField;
}

//--EXIST-II----------------------------------------------------------------------------------------------

bool CedaBLTData::ExistBnam(char* popBnam)
{
	bool ilRc = true;
	char pclSelection[512];
	sprintf(pclSelection,"WHERE BNAM='%s'",popBnam);
	ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"),pcmTableName,"BNAM",pclSelection,"","");
	return ilRc;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaBLTData::SaveBLT(BLTDATA *prpBLT)
{
	bool olRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpBLT->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpBLT->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpBLT);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpBLT->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpBLT->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpBLT);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpBLT->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpBLT->Urno);
		olRc = CedaAction("DRT",pclSelection);
		//prpBLT->IsChanged = DATA_UNCHANGED;
		break;
	}
	if(!omLastErrorMessage.IsEmpty())
	{
		if(omLastErrorMessage.Find("ORA") != -1)
		{
			char pclMsg[2048]="";
			sprintf(pclMsg, "%s", omLastErrorMessage.GetBuffer(0));
			::MessageBox(NULL,pclMsg,GetString(IDS_STRING188),MB_OK);
			return false;
		}
	}

    return true;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessBLTCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_BLT_CHANGE :
	case BC_BLT_DELETE :
		((CedaBLTData *)popInstance)->ProcessBLTBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaBLTData::ProcessBLTBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlBLTData;
	long llUrno;
	prlBLTData = (struct BcStruct *) vpDataPointer;
	CString olSelection = (CString)prlBLTData->Selection;
	if (olSelection.Find('\'') != -1)
	{
		llUrno = GetUrnoFromSelection(prlBLTData->Selection);
	}
	else
	{
		int ilFirst = olSelection.Find("=")+2;
		int ilLast  = olSelection.GetLength();
		llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
	}

	BLTDATA *prlBLT;
	if(ipDDXType == BC_BLT_CHANGE)
	{
		if((prlBLT = GetBLTByUrno(llUrno)) != NULL)
		{
			GetRecordFromItemList(prlBLT,prlBLTData->Fields,prlBLTData->Data);
			UpdateBLTInternal(prlBLT);
		}
		else
		{
			prlBLT = new BLTDATA;
			GetRecordFromItemList(prlBLT,prlBLTData->Fields,prlBLTData->Data);
			InsertBLTInternal(prlBLT);
		}
	}
	if(ipDDXType == BC_BLT_DELETE)
	{
		prlBLT = GetBLTByUrno(llUrno);
		if (prlBLT != NULL)
		{
			DeleteBLTInternal(prlBLT);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
