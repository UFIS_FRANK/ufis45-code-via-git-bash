// AirCraftGroupSelDlg.cpp : implementation file
//

#include "stdafx.h"
#include "rules.h"
#include "AirCraftGroupSelDlg.h"
#include "GroupNamesPage.h"
#include "PstRuleDlg.h"
#include "BltRuleDlg.h"
#include "GatRuleDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAirCraftGroupSelDlg dialog


CAirCraftGroupSelDlg::CAirCraftGroupSelDlg(CWnd* pParent, CString& opGrpStr/*=NULL*/)
	: CDialog(CAirCraftGroupSelDlg::IDD, pParent)
{
	omGrpStr = opGrpStr;
	//{{AFX_DATA_INIT(CAirCraftGroupSelDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CAirCraftGroupSelDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAirCraftGroupSelDlg)
		DDX_Control(pDX, IDC_LIST1, m_GroupNameList);
		DDX_Control(pDX, IDC_LIST2, m_SelGroupNameList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAirCraftGroupSelDlg, CDialog)
	//{{AFX_MSG_MAP(CAirCraftGroupSelDlg)
	ON_BN_CLICKED(IDC_BUTTON1,OnIncludeGroup)
	ON_BN_CLICKED(IDC_BUTTON2,OnExcludeGroup)
	ON_BN_CLICKED(IDC_BUTTON3,OnRemomeGroup)
	ON_BN_CLICKED(IDC_BUTTON4,OnOk)
	ON_BN_CLICKED(IDC_BUTTON6,OnWithoutPrefix)
	ON_BN_CLICKED(IDC_BUTTON5,OnCancel)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAirCraftGroupSelDlg message handlers

BOOL CAirCraftGroupSelDlg::OnInitDialog() 
{
	CedaGrnData groupPage;
	CString str ;
	CDialog::OnInitDialog();
	
	char pclWhere[100]="";
	CCSPtrArray<GRNDATA> olGrnList;
	char pclTableName[16] = "ACTTAB";
	strcpy(pclTableName,omGrpStr);
	ogGrnData.GetGrnsByTabn(olGrnList, pclTableName, "POPS");
	int ilCount = olGrnList.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		m_GroupNameList.AddString(olGrnList[i].Grpn);
	}
	
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CAirCraftGroupSelDlg::OnIncludeGroup()
{
	
	CString str ;
	int i = 0;
	
	while(i < m_GroupNameList.GetCount())
	{
		if(m_GroupNameList.GetSel(i) != 0)
		{
			m_GroupNameList.GetText(i,str);
			m_GroupNameList.DeleteString(i);
			str = "=" + str;

			m_SelGroupNameList.AddString(str);

		}
		else
		  i++;
	}

}

void CAirCraftGroupSelDlg::OnExcludeGroup()
{
	
	CString str ;
	int i = 0;
	
	while(i < m_GroupNameList.GetCount())
	{
		if(m_GroupNameList.GetSel(i) != 0)
		{
			m_GroupNameList.GetText(i,str);
			m_GroupNameList.DeleteString(i);
			str = "!" + str;

			m_SelGroupNameList.AddString(str);

		}
		else
		 i++;
	}

}

void CAirCraftGroupSelDlg::OnRemomeGroup()
{
	
	CString str ;
	int length;
	int i = 0;

	while(i < m_SelGroupNameList.GetCount())
	{
		if(m_SelGroupNameList.GetSel(i) != 0)
		{
			m_SelGroupNameList.GetText(i,str);
			m_SelGroupNameList.DeleteString(i);
			length = str.GetLength();
			length--;
			str.Remove('!');
			str.Remove('=');
			m_GroupNameList.AddString(str);

		}
		else
		 i++;
	}

}

void CAirCraftGroupSelDlg::OnOk()
{
	CString str ;
	CString FinalStr;
	int li_count = m_SelGroupNameList.GetCount();
	if(li_count != 0)
	{
		m_SelGroupNameList.GetText(0,FinalStr);
		for(int i=1;i<li_count;i++)
		{
			m_SelGroupNameList.GetText(i,str);
			FinalStr = FinalStr + " " + str;

		}

		omGrpStr = FinalStr;

/*		
		CWnd *ptr = GetParent();
		if(ptr->IsKindOf(RUNTIME_CLASS(PstRuleDlg)))
		{

			PstRuleDlg	*dlg = (PstRuleDlg*)ptr;
			if (omGrpStr == "ACTTAB")
				dlg->m_ACGroup = FinalStr;

			dlg->UpdateData(FALSE);

		}

		if(ptr->IsKindOf(RUNTIME_CLASS(BltRuleDlg)))
		{
			BltRuleDlg *dlc = (BltRuleDlg*)ptr;
			if (omGrpStr == "ACTTAB")
				dlc->m_ACGroup = FinalStr;

			dlc->UpdateData(FALSE);

		}

		if(ptr->IsKindOf(RUNTIME_CLASS(GatRuleDlg)))
		{
			GatRuleDlg *dlc = (GatRuleDlg*)ptr;
			if (omGrpStr == "ACTTAB")
				dlc->m_ACGroup = FinalStr;

			dlc->UpdateData(FALSE);

		}
*/
	}

	CDialog::OnOK();


}

void CAirCraftGroupSelDlg::OnWithoutPrefix()
{
	
	CString str ;
	int i = 0;

	while(i < m_GroupNameList.GetCount())
	{
		if(m_GroupNameList.GetSel(i) != 0)
		{
			m_GroupNameList.GetText(i,str);
			m_GroupNameList.DeleteString(i);
			m_SelGroupNameList.AddString(str);

		}
		else
			i++;
	}


}

void CAirCraftGroupSelDlg::OnCancel()
{

	CDialog::OnCancel();
}
