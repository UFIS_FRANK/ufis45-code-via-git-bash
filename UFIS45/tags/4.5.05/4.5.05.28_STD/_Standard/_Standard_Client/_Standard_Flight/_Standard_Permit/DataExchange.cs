using System;
using System.Windows.Forms;
using Ufis.Data;
using Ufis.Utils;

namespace FlightPermits
{
	/// <summary>
	/// Summary description for DataExchange.
	/// The class DE is supposed to be the one and only central point for
	/// application's delegates. It contains all necessary delegate definitions
	/// for this application. Moreover is has worker methods, which must be
	/// called to initiate a delegate event to update classes, which are
	/// registered for the respective delegate. The worker methods can call
	/// a number of delegates depending on the context (application's logic/requirements).
	/// This class may not be is not instantiated.
	/// All Members are static and can be called directly by e.g. "DE.DoSomthing()"
	/// 
	/// IMPORTANT: 
	/// ==========
	/// When the application starts, the static Method
	///  --- InitDBObjects(Ufis.Utils.BCBlinker blinker) --- must be called to 
	///  trigger the BCBlinker and to init the ITable, IDatabase objects for furhter usage
	/// </summary>
	public class  DE
	{
		#region --- MyMembers

		static IDatabase myDB = null;
		static ITable    FpeTab = null;
		static BCBlinker myBCBlinker = null;

		public static AxAATLOGINLib.AxAatLogin LoginControl = null;

		/// <summary>
		/// Sends a delegate that "CLO" message was sent by the server
		/// </summary>
		public delegate void CLO_Message();
		public static event CLO_Message OnCLO_Message;

		/// <summary>
		/// Sends a delegate that flight was updated.
		/// </summary>
		public delegate void UpdateFpeData(object sender, string Urno, State state);
		public static event UpdateFpeData OnUpdateFpeData;

		#endregion -- MyMembers



		#region MyMethods

		public static string SetPrivs(Control opControl, string opKey)
		{
			string prv = "0";
			if(opControl != null)
			{
				prv = LoginControl.GetPrivileges(opKey);
				if(prv == "1")
				{
					opControl.Visible = true;
					opControl.Enabled = true;
				}
				else if(prv == "-")
				{
					opControl.Visible = false;
				}
				else
				{
					opControl.Visible = true;
					opControl.Enabled = false;
				}
			}

			return prv;
		}

		public static string SetPrivs(ToolBarButton opControl, string opKey)
		{
			string prv = "0";
			if(opControl != null)
			{
				prv = LoginControl.GetPrivileges(opKey);
				if(prv == "1")
				{
					opControl.Visible = true;
					opControl.Enabled = true;
				}
				else if(prv == "-")
				{
					opControl.Visible = false;
				}
				else
				{
					opControl.Visible = true;
					opControl.Enabled = false;
				}
			}

			return prv;
		}

		#endregion MyMethods



		#region --- BroadCast Methods

		/// <summary>
		/// Initialise the data exchange class
		/// </summary>
		/// <param name="blinker"></param>
		static public void InitDBObjects(Ufis.Utils.BCBlinker blinker)
		{
			myBCBlinker = blinker;
			myDB = UT.GetMemDB();
			FpeTab = myDB["FPE"];

			FpeTab.OnUpdateRequestHandler = new UpdateRequestHandler(FpeOnUpdateRequestHandler);
			myDB.OnEventHandler += new DatabaseEventHandler(DatabaseEventHandler);
			myDB.OnTableEventHandler += new DatabaseTableEventHandler(TableEventHandler);

		}

		/// <summary>
		/// Broadcast event received that is not directly related to a table e.g. the close event
		/// </summary>
		/// <param name="obj">The sender of the event</param>
		/// <param name="eventArgs">The broadcast data</param>
		static void DatabaseEventHandler(object obj,DatabaseEventArgs eventArgs)
		{
			myBCBlinker.Blink();
			if(eventArgs.command == "CLO")
			{
				//CLO_Message 
				if(OnCLO_Message != null)
				{
					OnCLO_Message();
				}
			}
		}

		/// <summary>
		/// Broadcast for a table received - this is a method called before the broadcast has been
		/// accepted - allows filtering of the broadcast
		/// </summary>
		/// <param name="pReqId"></param>
		/// <param name="pDest1"></param>
		/// <param name="pDest2"></param>
		/// <param name="pCmd"></param>
		/// <param name="pObject"></param>
		/// <param name="pSeq"></param>
		/// <param name="pTws"></param>
		/// <param name="pTwe"></param>
		/// <param name="pSelection"></param>
		/// <param name="pFields"></param>
		/// <param name="pData"></param>
		/// <param name="pBcNum"></param>
		/// <returns></returns>
		static bool FpeOnUpdateRequestHandler(string pReqId, string pDest1, string pDest2,string pCmd, string pObject, string pSeq,string pTws, string pTwe, string pSelection, string pFields, string pData, string pBcNum)
		{
			bool blRet = false;
			DateTime olVafr;
			DateTime olVato;
			int idxVafr = UT.GetItemNo(pFields, "VAFR");
			int idxVato = UT.GetItemNo(pFields, "VATO");
			
			if(idxVafr != -1 && idxVato != -1)
			{
				olVafr = UT.CedaFullDateToDateTime(UT.GetItem(pData, idxVafr, ","));
				olVato = UT.CedaFullDateToDateTime(UT.GetItem(pData, idxVato, ","));

				if(olVafr <= UT.TimeFrameTo && olVato >= UT.TimeFrameFrom)
					blRet = true; // bc within timeframe so accept it
			}

			return blRet;
		}

		/// <summary>
		/// Broadcast for a table received - this is a method called after the broadcast has been
		/// accepted so that tables and views can be updated
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="eventArgs"></param>
		static void TableEventHandler(object obj,DatabaseTableEventArgs eventArgs)
		{
			myBCBlinker.Blink();						
			ITable myTable = myDB[eventArgs.table];
			if (myTable == FpeTab)
			{
				HandleFpeEvent(obj,eventArgs);
			}
		}

		/// <summary>
		/// Handles broadcasts for FPETAB
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="eventArgs"></param>
		static void HandleFpeEvent(object obj, DatabaseTableEventArgs eventArgs)
		{
			if (FpeTab.Command("insert").IndexOf(","+eventArgs.command+",") >= 0)
			{
				if(OnUpdateFpeData != null)
				{
					OnUpdateFpeData(FpeTab, eventArgs.row["URNO"], State.Created); 
				}
			}
			else if (FpeTab.Command("update").IndexOf(","+eventArgs.command+",") >= 0)
			{
				if(OnUpdateFpeData != null)
				{
					OnUpdateFpeData(FpeTab, eventArgs.row["URNO"], State.Modified); 
				}
			}
			else if (FpeTab.Command("delete").IndexOf(","+eventArgs.command+",") >= 0)
			{
				if(OnUpdateFpeData != null)
				{
					OnUpdateFpeData(FpeTab, eventArgs.row["URNO"], State.Deleted); 
				}
			}
		}
		#endregion  BroadCast Methods
	}
}
