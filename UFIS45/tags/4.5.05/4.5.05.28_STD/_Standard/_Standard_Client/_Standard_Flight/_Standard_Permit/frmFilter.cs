using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Ufis.Utils;

namespace FlightPermits
{
	/// <summary>
	/// Summary description for frmFilter.
	/// </summary>
	public class frmFilter : System.Windows.Forms.Form
	{
		#region PublicProperties
		public string omFrom;
		public string omTo;
		public bool bmDaily;
		public bool bmMon;
		public bool bmTue;
		public bool bmWed;
		public bool bmThu;
		public bool bmFri;
		public bool bmSat;
		public bool bmSun;
		public string omAlc3;
		public string omFlno;
		public string omFlns;
        public string omATDNumber; 
        public string omRegistration; 

		public string omPermitNumber;
		public ArrayList omAirlineCodes = null;
		#endregion PublicProperties

		private System.Windows.Forms.Label FromLabel;
		private System.Windows.Forms.Label ToLabel;
		private System.Windows.Forms.DateTimePicker FromField;
		private System.Windows.Forms.DateTimePicker ToField;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.CheckBox checkBox2;
		private System.Windows.Forms.CheckBox checkBox3;
		private System.Windows.Forms.CheckBox checkBox4;
		private System.Windows.Forms.CheckBox checkBox5;
		private System.Windows.Forms.CheckBox checkBox6;
		private System.Windows.Forms.CheckBox checkBox7;
		private System.Windows.Forms.CheckBox checkBoxDaily;
		private System.Windows.Forms.Label Alc3Label;
		private System.Windows.Forms.Label FlnoLabel;
		private System.Windows.Forms.Label FlnsLabel;
		private System.Windows.Forms.TextBox FlnoField;
		private System.Windows.Forms.TextBox FlnsField;
		private System.Windows.Forms.Label PermitNumLabel;
		private System.Windows.Forms.Button Cancel;
		private System.Windows.Forms.Button OK;
		private System.Windows.Forms.ComboBox AlcComboBox;
		private System.Windows.Forms.TextBox PermitNumField;
		private System.Windows.Forms.TextBox ATDNfield;
		private System.Windows.Forms.TextBox REGNField;
		private System.Windows.Forms.Label ATDNumberLable;
		private System.Windows.Forms.Label RegistrationLable;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmFilter()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.FromLabel = new System.Windows.Forms.Label();
			this.ToLabel = new System.Windows.Forms.Label();
			this.FromField = new System.Windows.Forms.DateTimePicker();
			this.ToField = new System.Windows.Forms.DateTimePicker();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.checkBox2 = new System.Windows.Forms.CheckBox();
			this.checkBox3 = new System.Windows.Forms.CheckBox();
			this.checkBox4 = new System.Windows.Forms.CheckBox();
			this.checkBox5 = new System.Windows.Forms.CheckBox();
			this.checkBox6 = new System.Windows.Forms.CheckBox();
			this.checkBox7 = new System.Windows.Forms.CheckBox();
			this.checkBoxDaily = new System.Windows.Forms.CheckBox();
			this.Alc3Label = new System.Windows.Forms.Label();
			this.FlnoLabel = new System.Windows.Forms.Label();
			this.FlnsLabel = new System.Windows.Forms.Label();
			this.FlnoField = new System.Windows.Forms.TextBox();
			this.FlnsField = new System.Windows.Forms.TextBox();
			this.PermitNumLabel = new System.Windows.Forms.Label();
			this.PermitNumField = new System.Windows.Forms.TextBox();
			this.Cancel = new System.Windows.Forms.Button();
			this.OK = new System.Windows.Forms.Button();
			this.AlcComboBox = new System.Windows.Forms.ComboBox();
			this.ATDNfield = new System.Windows.Forms.TextBox();
			this.ATDNumberLable = new System.Windows.Forms.Label();
			this.REGNField = new System.Windows.Forms.TextBox();
			this.RegistrationLable = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// FromLabel
			// 
			this.FromLabel.Location = new System.Drawing.Point(8, 16);
			this.FromLabel.Name = "FromLabel";
			this.FromLabel.Size = new System.Drawing.Size(100, 16);
			this.FromLabel.TabIndex = 0;
			this.FromLabel.Text = "From";
			// 
			// ToLabel
			// 
			this.ToLabel.Location = new System.Drawing.Point(144, 16);
			this.ToLabel.Name = "ToLabel";
			this.ToLabel.Size = new System.Drawing.Size(100, 16);
			this.ToLabel.TabIndex = 1;
			this.ToLabel.Text = "To";
			// 
			// FromField
			// 
			this.FromField.CustomFormat = "dd.MM.yyyyy - HH:mm";
			this.FromField.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.FromField.Location = new System.Drawing.Point(8, 32);
			this.FromField.Name = "FromField";
			this.FromField.Size = new System.Drawing.Size(128, 20);
			this.FromField.TabIndex = 3;
			// 
			// ToField
			// 
			this.ToField.CustomFormat = "dd.MM.yyyy - HH:mm";
			this.ToField.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.ToField.Location = new System.Drawing.Point(144, 32);
			this.ToField.Name = "ToField";
			this.ToField.Size = new System.Drawing.Size(128, 20);
			this.ToField.TabIndex = 4;
			// 
			// checkBox1
			// 
			this.checkBox1.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.checkBox1.Location = new System.Drawing.Point(8, 64);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(16, 40);
			this.checkBox1.TabIndex = 5;
			this.checkBox1.Text = "1";
			this.checkBox1.CheckedChanged += new System.EventHandler(this.OnCheckChanged);
			// 
			// checkBox2
			// 
			this.checkBox2.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.checkBox2.Location = new System.Drawing.Point(32, 64);
			this.checkBox2.Name = "checkBox2";
			this.checkBox2.Size = new System.Drawing.Size(16, 40);
			this.checkBox2.TabIndex = 6;
			this.checkBox2.Text = "2";
			this.checkBox2.CheckedChanged += new System.EventHandler(this.OnCheckChanged);
			// 
			// checkBox3
			// 
			this.checkBox3.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.checkBox3.Location = new System.Drawing.Point(56, 64);
			this.checkBox3.Name = "checkBox3";
			this.checkBox3.Size = new System.Drawing.Size(16, 40);
			this.checkBox3.TabIndex = 7;
			this.checkBox3.Text = "3";
			this.checkBox3.CheckedChanged += new System.EventHandler(this.OnCheckChanged);
			// 
			// checkBox4
			// 
			this.checkBox4.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.checkBox4.Location = new System.Drawing.Point(80, 64);
			this.checkBox4.Name = "checkBox4";
			this.checkBox4.Size = new System.Drawing.Size(16, 40);
			this.checkBox4.TabIndex = 8;
			this.checkBox4.Text = "4";
			this.checkBox4.CheckedChanged += new System.EventHandler(this.OnCheckChanged);
			// 
			// checkBox5
			// 
			this.checkBox5.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.checkBox5.Location = new System.Drawing.Point(104, 64);
			this.checkBox5.Name = "checkBox5";
			this.checkBox5.Size = new System.Drawing.Size(16, 40);
			this.checkBox5.TabIndex = 9;
			this.checkBox5.Text = "5";
			this.checkBox5.CheckedChanged += new System.EventHandler(this.OnCheckChanged);
			// 
			// checkBox6
			// 
			this.checkBox6.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.checkBox6.Location = new System.Drawing.Point(128, 64);
			this.checkBox6.Name = "checkBox6";
			this.checkBox6.Size = new System.Drawing.Size(16, 40);
			this.checkBox6.TabIndex = 10;
			this.checkBox6.Text = "6";
			this.checkBox6.CheckedChanged += new System.EventHandler(this.OnCheckChanged);
			// 
			// checkBox7
			// 
			this.checkBox7.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.checkBox7.Location = new System.Drawing.Point(152, 64);
			this.checkBox7.Name = "checkBox7";
			this.checkBox7.Size = new System.Drawing.Size(16, 40);
			this.checkBox7.TabIndex = 11;
			this.checkBox7.Text = "7";
			this.checkBox7.CheckedChanged += new System.EventHandler(this.OnCheckChanged);
			// 
			// checkBoxDaily
			// 
			this.checkBoxDaily.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.checkBoxDaily.Location = new System.Drawing.Point(184, 64);
			this.checkBoxDaily.Name = "checkBoxDaily";
			this.checkBoxDaily.Size = new System.Drawing.Size(40, 40);
			this.checkBoxDaily.TabIndex = 12;
			this.checkBoxDaily.Text = "Daily";
			this.checkBoxDaily.CheckedChanged += new System.EventHandler(this.OnDailyCheckChanged);
			// 
			// Alc3Label
			// 
			this.Alc3Label.Location = new System.Drawing.Point(8, 120);
			this.Alc3Label.Name = "Alc3Label";
			this.Alc3Label.Size = new System.Drawing.Size(40, 16);
			this.Alc3Label.TabIndex = 13;
			this.Alc3Label.Text = "AL";
			// 
			// FlnoLabel
			// 
			this.FlnoLabel.Location = new System.Drawing.Point(112, 120);
			this.FlnoLabel.Name = "FlnoLabel";
			this.FlnoLabel.Size = new System.Drawing.Size(72, 16);
			this.FlnoLabel.TabIndex = 14;
			this.FlnoLabel.Text = "Flno";
			// 
			// FlnsLabel
			// 
			this.FlnsLabel.Location = new System.Drawing.Point(200, 120);
			this.FlnsLabel.Name = "FlnsLabel";
			this.FlnsLabel.Size = new System.Drawing.Size(32, 16);
			this.FlnsLabel.TabIndex = 15;
			this.FlnsLabel.Text = "S";
			// 
			// FlnoField
			// 
			this.FlnoField.Location = new System.Drawing.Point(104, 136);
			this.FlnoField.Name = "FlnoField";
			this.FlnoField.Size = new System.Drawing.Size(80, 20);
			this.FlnoField.TabIndex = 17;
			this.FlnoField.Text = "textBox2";
			// 
			// FlnsField
			// 
			this.FlnsField.Location = new System.Drawing.Point(192, 136);
			this.FlnsField.Name = "FlnsField";
			this.FlnsField.Size = new System.Drawing.Size(24, 20);
			this.FlnsField.TabIndex = 18;
			this.FlnsField.Text = "textBox3";
			// 
			// PermitNumLabel
			// 
			this.PermitNumLabel.Location = new System.Drawing.Point(8, 176);
			this.PermitNumLabel.Name = "PermitNumLabel";
			this.PermitNumLabel.Size = new System.Drawing.Size(112, 16);
			this.PermitNumLabel.TabIndex = 19;
			this.PermitNumLabel.Text = "Permit Nr";
			// 
			// PermitNumField
			// 
			this.PermitNumField.Location = new System.Drawing.Point(8, 192);
			this.PermitNumField.Name = "PermitNumField";
			this.PermitNumField.Size = new System.Drawing.Size(136, 20);
			this.PermitNumField.TabIndex = 20;
			this.PermitNumField.Text = "textBox1";
			// 
			// Cancel
			// 
			this.Cancel.Location = new System.Drawing.Point(184, 288);
			this.Cancel.Name = "Cancel";
			this.Cancel.TabIndex = 21;
			this.Cancel.Text = "&Cancel";
			this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
			// 
			// OK
			// 
			this.OK.Location = new System.Drawing.Point(104, 288);
			this.OK.Name = "OK";
			this.OK.TabIndex = 22;
			this.OK.Text = "&OK";
			this.OK.Click += new System.EventHandler(this.OK_Click);
			// 
			// AlcComboBox
			// 
			this.AlcComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.AlcComboBox.Location = new System.Drawing.Point(8, 136);
			this.AlcComboBox.Name = "AlcComboBox";
			this.AlcComboBox.Size = new System.Drawing.Size(88, 21);
			this.AlcComboBox.TabIndex = 23;
			// 
			// ATDNfield
			// 
			this.ATDNfield.Location = new System.Drawing.Point(160, 192);
			this.ATDNfield.MaxLength = 25;
			this.ATDNfield.Name = "ATDNfield";
			this.ATDNfield.Size = new System.Drawing.Size(136, 20);
			this.ATDNfield.TabIndex = 25;
			this.ATDNfield.Tag = "";
			this.ATDNfield.Text = "";
			// 
			// ATDNumberLable
			// 
			this.ATDNumberLable.Location = new System.Drawing.Point(160, 176);
			this.ATDNumberLable.Name = "ATDNumberLable";
			this.ATDNumberLable.Size = new System.Drawing.Size(112, 16);
			this.ATDNumberLable.TabIndex = 24;
			this.ATDNumberLable.Text = "ATD Number";
			// 
			// REGNField
			// 
			this.REGNField.Location = new System.Drawing.Point(8, 248);
			this.REGNField.MaxLength = 12;
			this.REGNField.Name = "REGNField";
			this.REGNField.Size = new System.Drawing.Size(136, 20);
			this.REGNField.TabIndex = 27;
			this.REGNField.Tag = "";
			this.REGNField.Text = "";
			// 
			// RegistrationLable
			// 
			this.RegistrationLable.Location = new System.Drawing.Point(8, 232);
			this.RegistrationLable.Name = "RegistrationLable";
			this.RegistrationLable.Size = new System.Drawing.Size(112, 16);
			this.RegistrationLable.TabIndex = 26;
			this.RegistrationLable.Text = "Registration";
			// 
			// frmFilter
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(314, 328);
			this.Controls.Add(this.REGNField);
			this.Controls.Add(this.RegistrationLable);
			this.Controls.Add(this.ATDNfield);
			this.Controls.Add(this.ATDNumberLable);
			this.Controls.Add(this.AlcComboBox);
			this.Controls.Add(this.OK);
			this.Controls.Add(this.Cancel);
			this.Controls.Add(this.PermitNumField);
			this.Controls.Add(this.FlnsField);
			this.Controls.Add(this.FlnoField);
			this.Controls.Add(this.PermitNumLabel);
			this.Controls.Add(this.FlnsLabel);
			this.Controls.Add(this.FlnoLabel);
			this.Controls.Add(this.Alc3Label);
			this.Controls.Add(this.checkBoxDaily);
			this.Controls.Add(this.checkBox7);
			this.Controls.Add(this.checkBox6);
			this.Controls.Add(this.checkBox5);
			this.Controls.Add(this.checkBox4);
			this.Controls.Add(this.checkBox3);
			this.Controls.Add(this.checkBox2);
			this.Controls.Add(this.checkBox1);
			this.Controls.Add(this.ToField);
			this.Controls.Add(this.FromField);
			this.Controls.Add(this.ToLabel);
			this.Controls.Add(this.FromLabel);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmFilter";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Filter Flight Permits";
			this.Load += new System.EventHandler(this.frmFilter_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void frmFilter_Load(object sender, System.EventArgs e)
		{
			FromField.Value = UT.CedaFullDateToDateTime(omFrom);
			ToField.Value = UT.CedaFullDateToDateTime(omTo);
			checkBoxDaily.Checked = bmDaily;
			checkBox1.Checked = bmMon;
			checkBox2.Checked = bmTue;
			checkBox3.Checked = bmWed;
			checkBox4.Checked = bmThu;
			checkBox5.Checked = bmFri;
			checkBox6.Checked = bmSat;
			checkBox7.Checked = bmSun;
			checkBoxDaily.Checked = bmDaily;
			FlnoField.Text = omFlno;
			FlnsField.Text = omFlns;
			PermitNumField.Text = omPermitNumber;
            ATDNfield.Text = omATDNumber; 
            REGNField.Text = omRegistration; 

			AlcComboBox.DataSource = omAirlineCodes;
			AlcComboBox.DisplayMember = "Code" ;
			AlcComboBox.ValueMember = "Alc3";
			AlcComboBox.SelectedValue = omAlc3;
		}

		private void OnCheckChanged(object sender, System.EventArgs e)
		{
			if(checkBox1.Checked && checkBox2.Checked && checkBox3.Checked  && checkBox4.Checked && checkBox5.Checked && checkBox6.Checked && checkBox7.Checked)
				checkBoxDaily.Checked = true;
			else
				checkBoxDaily.Checked = false;
		}

		private void OnDailyCheckChanged(object sender, System.EventArgs e)
		{
			if(checkBoxDaily.Checked)
			{
				checkBox1.Checked = true;
				checkBox2.Checked = true;
				checkBox3.Checked = true;
				checkBox4.Checked = true;
				checkBox5.Checked = true;
				checkBox6.Checked = true;
				checkBox7.Checked = true;
			}
		}

		private void OK_Click(object sender, System.EventArgs e)
		{
			bool blChanged = false;

			string olFrom = UT.DateTimeToCeda(FromField.Value);
			string olTo = UT.DateTimeToCeda(ToField.Value);
			if(bmMon != checkBox1.Checked)
			{
				bmMon = checkBox1.Checked;
				blChanged = true;
			}
			if(bmTue != checkBox2.Checked)
			{
				bmTue = checkBox2.Checked;
				blChanged = true;
			}
			if(bmWed != checkBox3.Checked)
			{
				bmWed = checkBox3.Checked;
				blChanged = true;
			}
			if(bmThu != checkBox4.Checked)
			{
				bmThu = checkBox4.Checked;
				blChanged = true;
			}
			if(bmFri != checkBox5.Checked)
			{
				bmFri = checkBox5.Checked;
				blChanged = true;
			}
			if(bmSat != checkBox6.Checked)
			{
				bmSat = checkBox6.Checked;
				blChanged = true;
			}
			if(bmSun != checkBox7.Checked)
			{
				bmSun = checkBox7.Checked;
				blChanged = true;
			}
			if(bmDaily != checkBoxDaily.Checked)
			{
				bmDaily = checkBoxDaily.Checked;
				blChanged = true;
			}
			//string olAlc3 = Alc3Field.Text;
			string olFlno = FlnoField.Text;
			string olFlns = FlnsField.Text;
			string olPermitNumber = PermitNumField.Text;
            string olATDNumber = ATDNfield.Text; 
            string olRegistration = REGNField.Text; 
			
			if(omFrom != olFrom)
			{
				omFrom = olFrom;
				blChanged = true;
			}
			if(omTo != olTo)
			{
				omTo = olTo;
				blChanged = true;
			}
//			if(omAlc3 != olAlc3)
//			{
//				omAlc3 = olAlc3;
//				blChanged = true;
//			}
			if(omFlno != olFlno)
			{
				omFlno = olFlno;
				blChanged = true;
			}
			if(omFlns != olFlns)
			{
				omFlns = olFlns;
				blChanged = true;
			}
			if(omPermitNumber != olPermitNumber)
			{
				omPermitNumber = olPermitNumber;
				blChanged = true;
			}
            if (omATDNumber != olATDNumber)
            {
                omATDNumber = olATDNumber;
                blChanged = true;
            }
            if (omRegistration != olRegistration)
            {
                omRegistration = olRegistration;
				blChanged = true;
			}
			string olAlc3 = (string) AlcComboBox.SelectedValue;
			if(olAlc3 != omAlc3)
			{
				omAlc3 = olAlc3;
				blChanged = true;
			}

			if(blChanged)
				DialogResult = DialogResult.OK;
			else
				DialogResult = DialogResult.Cancel;
			this.Close();
		}

		private void Cancel_Click(object sender, System.EventArgs e)
		{
			DialogResult = DialogResult.Cancel;
			this.Close();
		}
	}
}
