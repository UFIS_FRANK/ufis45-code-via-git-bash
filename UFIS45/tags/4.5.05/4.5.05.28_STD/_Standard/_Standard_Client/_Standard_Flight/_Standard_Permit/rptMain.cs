using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using Ufis.Data;
using Ufis.Utils;
using System.Drawing;

namespace FlightPermits
{
	public class rptMain : ActiveReport3
	{
		#region MyData
		private AxTABLib.AxTAB omTable = null;
		private IDatabase omDB = null;
		private ITable omFpeTab = null;
		private int imCurrLine = 0;
		private int imMaxLines = 0;
		#endregion MyData

		public rptMain(AxTABLib.AxTAB opTable)
		{
			omTable = opTable;
			InitializeReport();
		}

		private void Detail_Format(object sender, System.EventArgs eArgs)
		{
			
		}

		#region ActiveReports Designer generated code
		private PageHeader PageHeader = null;
		private Shape Shape1 = null;
		private Label PERNheader = null;
		private Label VAFRheader = null;
		private Label VATOheader = null;
		private Label DOOPheader = null;
		private Label ALCOheader = null;
		private Label FLNOheader = null;
		private Label SUFXheader = null;
		private Label REGNheader = null;
		private Label ADIDheader = null;
		private Label ATDNheader = null;
		private Label DTGNheader = null;
		private Label USRRheader = null;
		private Label Label1 = null;
		private Picture Logo = null;
		private Label FromTitle = null;
		private Label From = null;
		private Label ToTitle = null;
		private Label To = null;
		private Detail Detail = null;
		private Line Line2 = null;
		private Line Line4 = null;
		private Line Line5 = null;
		private Line Line19 = null;
		private Line Line20 = null;
		private Line Line21 = null;
		private Line Line22 = null;
		private Line Line23 = null;
		private Line Line26 = null;
		private Line Line27 = null;
		private Line Line30 = null;
		private Label DOOP = null;
		private Label ALCO = null;
		private Label FLNO = null;
		private Label ATDN = null;
		private Label DTGN = null;
		private Label USRR = null;
		private Label SUFX = null;
		private Label ADID = null;
		private Line Line32 = null;
		private Label REGN = null;
		private Line Line34 = null;
		private Line Line35 = null;
		private TextBox PERN = null;
		private TextBox VAFR = null;
		private TextBox VATO = null;
		private PageFooter PageFooter = null;
		private TextBox PageTotal = null;
		private Label OfTitle = null;
		private TextBox TextBox1 = null;
		private Label PageTitle = null;
		private TextBox CreationDateTitle = null;
		private TextBox CreationDate = null;
		private TextBox CreatedByTitle = null;
		private TextBox CreatedBy = null;
		public void InitializeReport()
		{
			this.LoadLayout(this.GetType(), "FlightPermits.rptMain.rpx");
			this.PageHeader = ((DataDynamics.ActiveReports.PageHeader)(this.Sections["PageHeader"]));
			this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
			this.PageFooter = ((DataDynamics.ActiveReports.PageFooter)(this.Sections["PageFooter"]));
			this.Shape1 = ((DataDynamics.ActiveReports.Shape)(this.PageHeader.Controls[0]));
			this.PERNheader = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[1]));
			this.VAFRheader = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[2]));
			this.VATOheader = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[3]));
			this.DOOPheader = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[4]));
			this.ALCOheader = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[5]));
			this.FLNOheader = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[6]));
			this.SUFXheader = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[7]));
			this.REGNheader = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[8]));
			this.ADIDheader = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[9]));
			this.ATDNheader = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[10]));
			this.DTGNheader = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[11]));
			this.USRRheader = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[12]));
			this.Label1 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[13]));
			this.Logo = ((DataDynamics.ActiveReports.Picture)(this.PageHeader.Controls[14]));
			this.FromTitle = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[15]));
			this.From = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[16]));
			this.ToTitle = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[17]));
			this.To = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[18]));
			this.Line2 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[0]));
			this.Line4 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[1]));
			this.Line5 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[2]));
			this.Line19 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[3]));
			this.Line20 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[4]));
			this.Line21 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[5]));
			this.Line22 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[6]));
			this.Line23 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[7]));
			this.Line26 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[8]));
			this.Line27 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[9]));
			this.Line30 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[10]));
			this.DOOP = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[11]));
			this.ALCO = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[12]));
			this.FLNO = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[13]));
			this.ATDN = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[14]));
			this.DTGN = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[15]));
			this.USRR = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[16]));
			this.SUFX = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[17]));
			this.ADID = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[18]));
			this.Line32 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[19]));
			this.REGN = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[20]));
			this.Line34 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[21]));
			this.Line35 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[22]));
			this.PERN = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[23]));
			this.VAFR = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[24]));
			this.VATO = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[25]));
			this.PageTotal = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[0]));
			this.OfTitle = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[1]));
			this.TextBox1 = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[2]));
			this.PageTitle = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[3]));
			this.CreationDateTitle = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[4]));
			this.CreationDate = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[5]));
			this.CreatedByTitle = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[6]));
			this.CreatedBy = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[7]));
			// Attach Report Events
			this.ReportStart += new System.EventHandler(this.rptMain_ReportStart);
			this.FetchData += new DataDynamics.ActiveReports.ActiveReport3.FetchEventHandler(this.rptMain_FetchData);
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
		}

		#endregion

		private void rptMain_ReportStart(object sender, EventArgs e)
		{
			omDB = UT.GetMemDB();
			omFpeTab = omDB["FPE"];
			imMaxLines = omTable.GetLineCount();
			DateTime olNow = DateTime.Now;
			CreationDate.Text = olNow.ToString("dd.MM.yyyy HH:mm");
			CreatedBy.Text = UT.UserName;
			From.Text = UT.TimeFrameFrom.ToString("dd.MM.yyyy HH:mm");
			To.Text = UT.TimeFrameTo.ToString("dd.MM.yyyy HH:mm");

			// load the customer bitmap
			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strPrintLogoPath = myIni.IniReadValue("FLIGHTPERMITS", "PRINTLOGO");
			Image myImage = null;
			try
			{
				if(strPrintLogoPath != "")
				{
					myImage = new Bitmap(strPrintLogoPath);
				}
			}
			catch(Exception)
			{
			}
			if(myImage != null)
				Logo.Image = myImage;
		}

		private void rptMain_FetchData(object sender, DataDynamics.ActiveReports.ActiveReport3.FetchEventArgs eArgs)
		{
			string olUrno = omTable.GetFieldValue(imCurrLine, "URNO");
			IRow [] olRows = omFpeTab.RowsByIndexValue("URNO", olUrno);
			if(olRows.Length > 0)
			{
				PERN.Text = olRows[0]["PERN"];
				DateTime olVafr = UT.CedaFullDateToDateTime(olRows[0]["VAFR"]);
				DateTime olVato   = UT.CedaFullDateToDateTime(olRows[0]["VATO"]);
				VAFR.Text = olVafr.ToString("dd.MM.yyyy HH:mm");
				VATO.Text = olVato.ToString("dd.MM.yyyy HH:mm");
				DOOP.Text = olRows[0]["DOOP"];
				ALCO.Text = olRows[0]["ALCO"];
				FLNO.Text = olRows[0]["FLNO"];
				SUFX.Text = olRows[0]["SUFX"];
				REGN.Text = olRows[0]["REGN"];
				ADID.Text = olRows[0]["ADID"];
				ATDN.Text = olRows[0]["ATDN"];
				DTGN.Text = olRows[0]["DTGN"];
				USRR.Text = olRows[0]["USRR"];
			}

			if(imCurrLine == imMaxLines)
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
				imCurrLine++;
			}
		}
	}
}
