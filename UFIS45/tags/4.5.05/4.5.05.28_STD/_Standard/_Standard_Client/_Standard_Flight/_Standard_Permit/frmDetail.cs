using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Ufis.Utils;
using Ufis.Data;

namespace FlightPermits
{
    /// <summary>
    /// Summary description for frmDetail.
    /// </summary>
    public class frmDetail : System.Windows.Forms.Form
    {
        #region myProperties
        private string[] omFields = "PERN,FLNO,SUFX,REGN,ATDN,DTGN,REPN,MTOW,PERR,OBLR,USRR,USEC,USEU".Split(',');
        private string[] omFieldDescriptions = "Permit Nr,Flight Number,Flight Suffix,Registration,ATD Number,DTGN,REPN,MTOW,Permit Remark,Obligation Remark,User Remark,USEC,USEU".Split(',');
        private IRow omRow = null;
        private ArrayList omAirlineCodes = null;
        private bool bmInsert = false;
        private FlightPermits.DE.UpdateFpeData omDelegateUpdateFpeData = null;
        private bool bmAlc2 = true;
        private bool bmInitialized = false;
        private bool bmCopy = false; // Copy Flag
        private bool isPremitValidation = false;


        public class SortAsc : IComparer
        {
            int IComparer.Compare(Object x, Object y)
            {
                return ((new CaseInsensitiveComparer()).Compare(x, y));
            }
        }

        public class SortDesc : IComparer
        {
            int IComparer.Compare(Object x, Object y)
            {
                return ((new CaseInsensitiveComparer()).Compare(y, x));
            }
        }
        #endregion myProperties

        #region Controls

        private System.Windows.Forms.ToolBar toolBar1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label PERNlabel;
        private System.Windows.Forms.TextBox PERNfield;
        private System.Windows.Forms.Label ALCOlabel;
        private System.Windows.Forms.Label FLNOlabel;
        private System.Windows.Forms.Label SUFXlabel;
        private System.Windows.Forms.Label REGNlabel;
        private System.Windows.Forms.Label VAFRlabel;
        private System.Windows.Forms.Label VATOlabel;
        private System.Windows.Forms.ComboBox AlcComboBox;
        private System.Windows.Forms.TextBox REGNfield;
        private System.Windows.Forms.DateTimePicker ToField;
        private System.Windows.Forms.DateTimePicker FromField;
        private System.Windows.Forms.CheckBox checkBoxDaily;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox AHFPcheckBox;
        private System.Windows.Forms.TextBox DTGNfield;
        private System.Windows.Forms.TextBox ATDNfield;
        private System.Windows.Forms.Label DTGNlabel;
        private System.Windows.Forms.Label ATDNlabel;
        private System.Windows.Forms.Label line1;
        private System.Windows.Forms.TextBox MTOWfield;
        private System.Windows.Forms.Label MTOWlabel;
        private System.Windows.Forms.Label line2;
        private System.Windows.Forms.Label KGlabel;
        private System.Windows.Forms.Label PERRlabel;
        private System.Windows.Forms.TextBox PERRfield;
        private System.Windows.Forms.TextBox OBLRfield;
        private System.Windows.Forms.Label OBLRlabel;
        private System.Windows.Forms.TextBox USRRfield;
        private System.Windows.Forms.Label USRRlabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox USECfield;
        private System.Windows.Forms.Label USEClabel;
        private System.Windows.Forms.Label CDATlabel;
        private System.Windows.Forms.Label LSTUlabel;
        private System.Windows.Forms.TextBox USEUfield;
        private System.Windows.Forms.Label USEUlabel;
        private System.Windows.Forms.ToolBarButton SaveButton;
        private System.Windows.Forms.ToolBarButton PrintButton;
        private System.Windows.Forms.ToolBarButton CloseButton;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton DepRadioButton;
        private System.Windows.Forms.RadioButton ArrRadioButton;
        private System.Windows.Forms.DateTimePicker CDATfield;
        private System.Windows.Forms.DateTimePicker LSTUfield;

        #endregion Controls
        private System.Windows.Forms.TextBox SUFXfield;
        private System.Windows.Forms.TextBox FLNOfield;
        private System.Windows.Forms.TextBox ALCOfield;
        private System.Windows.Forms.Button SelAlcButton;
        private System.Windows.Forms.Button SortAlcButton;
        private System.Windows.Forms.ToolBarButton HelpButton;
        private System.Windows.Forms.ToolBarButton Sep1;
        private System.Windows.Forms.CheckBox PRFFCheckBox;
        private System.Windows.Forms.TextBox REPNfield;
        private System.Windows.Forms.Label ReceiptNlabel;



        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        public frmDetail()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            //
            // TODO: Add any constructor code after InitializeComponent call
            //


            IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
            if (!string.IsNullOrEmpty(myIni.IniReadValue("FIPS", "FLIGHTPERMITS_VALIDITY")))
                isPremitValidation = bool.Parse(myIni.IniReadValue("FIPS", "FLIGHTPERMITS_VALIDITY"));
            if (!string.IsNullOrEmpty(myIni.IniReadValue("FIPS", "FLIGHTPERMITS_LABEL")))
                ReceiptNlabel.Text = myIni.IniReadValue("FIPS", "FLIGHTPERMITS_LABEL");
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDetail));
            this.toolBar1 = new System.Windows.Forms.ToolBar();
            this.SaveButton = new System.Windows.Forms.ToolBarButton();
            this.PrintButton = new System.Windows.Forms.ToolBarButton();
            this.CloseButton = new System.Windows.Forms.ToolBarButton();
            this.Sep1 = new System.Windows.Forms.ToolBarButton();
            this.HelpButton = new System.Windows.Forms.ToolBarButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.REPNfield = new System.Windows.Forms.TextBox();
            this.ReceiptNlabel = new System.Windows.Forms.Label();
            this.PRFFCheckBox = new System.Windows.Forms.CheckBox();
            this.SortAlcButton = new System.Windows.Forms.Button();
            this.SelAlcButton = new System.Windows.Forms.Button();
            this.ALCOfield = new System.Windows.Forms.TextBox();
            this.LSTUfield = new System.Windows.Forms.DateTimePicker();
            this.CDATfield = new System.Windows.Forms.DateTimePicker();
            this.panel2 = new System.Windows.Forms.Panel();
            this.DepRadioButton = new System.Windows.Forms.RadioButton();
            this.ArrRadioButton = new System.Windows.Forms.RadioButton();
            this.LSTUlabel = new System.Windows.Forms.Label();
            this.USEUfield = new System.Windows.Forms.TextBox();
            this.USEUlabel = new System.Windows.Forms.Label();
            this.CDATlabel = new System.Windows.Forms.Label();
            this.USECfield = new System.Windows.Forms.TextBox();
            this.USEClabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.USRRfield = new System.Windows.Forms.TextBox();
            this.USRRlabel = new System.Windows.Forms.Label();
            this.OBLRfield = new System.Windows.Forms.TextBox();
            this.OBLRlabel = new System.Windows.Forms.Label();
            this.PERRfield = new System.Windows.Forms.TextBox();
            this.PERRlabel = new System.Windows.Forms.Label();
            this.KGlabel = new System.Windows.Forms.Label();
            this.line2 = new System.Windows.Forms.Label();
            this.line1 = new System.Windows.Forms.Label();
            this.MTOWfield = new System.Windows.Forms.TextBox();
            this.MTOWlabel = new System.Windows.Forms.Label();
            this.DTGNfield = new System.Windows.Forms.TextBox();
            this.ATDNfield = new System.Windows.Forms.TextBox();
            this.DTGNlabel = new System.Windows.Forms.Label();
            this.ATDNlabel = new System.Windows.Forms.Label();
            this.AHFPcheckBox = new System.Windows.Forms.CheckBox();
            this.checkBoxDaily = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.ToField = new System.Windows.Forms.DateTimePicker();
            this.FromField = new System.Windows.Forms.DateTimePicker();
            this.REGNfield = new System.Windows.Forms.TextBox();
            this.AlcComboBox = new System.Windows.Forms.ComboBox();
            this.SUFXfield = new System.Windows.Forms.TextBox();
            this.FLNOfield = new System.Windows.Forms.TextBox();
            this.VATOlabel = new System.Windows.Forms.Label();
            this.VAFRlabel = new System.Windows.Forms.Label();
            this.REGNlabel = new System.Windows.Forms.Label();
            this.SUFXlabel = new System.Windows.Forms.Label();
            this.FLNOlabel = new System.Windows.Forms.Label();
            this.ALCOlabel = new System.Windows.Forms.Label();
            this.PERNfield = new System.Windows.Forms.TextBox();
            this.PERNlabel = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolBar1
            // 
            this.toolBar1.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.SaveButton,
            this.PrintButton,
            this.CloseButton,
            this.Sep1,
            this.HelpButton});
            this.toolBar1.ButtonSize = new System.Drawing.Size(75, 20);
            this.toolBar1.DropDownArrows = true;
            this.toolBar1.Location = new System.Drawing.Point(0, 0);
            this.toolBar1.Name = "toolBar1";
            this.toolBar1.ShowToolTips = true;
            this.toolBar1.Size = new System.Drawing.Size(840, 28);
            this.toolBar1.TabIndex = 0;
            this.toolBar1.TextAlign = System.Windows.Forms.ToolBarTextAlign.Right;
            this.toolBar1.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.toolBar1_ButtonClick);
            // 
            // SaveButton
            // 
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Tag = "SAVE";
            this.SaveButton.Text = "&Save";
            // 
            // PrintButton
            // 
            this.PrintButton.Name = "PrintButton";
            this.PrintButton.Tag = "PRINT";
            this.PrintButton.Text = "&Print";
            // 
            // CloseButton
            // 
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Tag = "CLOSE";
            this.CloseButton.Text = "&Close";
            // 
            // Sep1
            // 
            this.Sep1.Name = "Sep1";
            this.Sep1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // HelpButton
            // 
            this.HelpButton.Name = "HelpButton";
            this.HelpButton.Tag = "HELP";
            this.HelpButton.Text = "&Help";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.REPNfield);
            this.panel1.Controls.Add(this.ReceiptNlabel);
            this.panel1.Controls.Add(this.PRFFCheckBox);
            this.panel1.Controls.Add(this.SortAlcButton);
            this.panel1.Controls.Add(this.SelAlcButton);
            this.panel1.Controls.Add(this.ALCOfield);
            this.panel1.Controls.Add(this.LSTUfield);
            this.panel1.Controls.Add(this.CDATfield);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.LSTUlabel);
            this.panel1.Controls.Add(this.USEUfield);
            this.panel1.Controls.Add(this.USEUlabel);
            this.panel1.Controls.Add(this.CDATlabel);
            this.panel1.Controls.Add(this.USECfield);
            this.panel1.Controls.Add(this.USEClabel);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.USRRfield);
            this.panel1.Controls.Add(this.USRRlabel);
            this.panel1.Controls.Add(this.OBLRfield);
            this.panel1.Controls.Add(this.OBLRlabel);
            this.panel1.Controls.Add(this.PERRfield);
            this.panel1.Controls.Add(this.PERRlabel);
            this.panel1.Controls.Add(this.KGlabel);
            this.panel1.Controls.Add(this.line2);
            this.panel1.Controls.Add(this.line1);
            this.panel1.Controls.Add(this.MTOWfield);
            this.panel1.Controls.Add(this.MTOWlabel);
            this.panel1.Controls.Add(this.DTGNfield);
            this.panel1.Controls.Add(this.ATDNfield);
            this.panel1.Controls.Add(this.DTGNlabel);
            this.panel1.Controls.Add(this.ATDNlabel);
            this.panel1.Controls.Add(this.AHFPcheckBox);
            this.panel1.Controls.Add(this.checkBoxDaily);
            this.panel1.Controls.Add(this.checkBox7);
            this.panel1.Controls.Add(this.checkBox6);
            this.panel1.Controls.Add(this.checkBox5);
            this.panel1.Controls.Add(this.checkBox4);
            this.panel1.Controls.Add(this.checkBox3);
            this.panel1.Controls.Add(this.checkBox2);
            this.panel1.Controls.Add(this.checkBox1);
            this.panel1.Controls.Add(this.ToField);
            this.panel1.Controls.Add(this.FromField);
            this.panel1.Controls.Add(this.REGNfield);
            this.panel1.Controls.Add(this.AlcComboBox);
            this.panel1.Controls.Add(this.SUFXfield);
            this.panel1.Controls.Add(this.FLNOfield);
            this.panel1.Controls.Add(this.VATOlabel);
            this.panel1.Controls.Add(this.VAFRlabel);
            this.panel1.Controls.Add(this.REGNlabel);
            this.panel1.Controls.Add(this.SUFXlabel);
            this.panel1.Controls.Add(this.FLNOlabel);
            this.panel1.Controls.Add(this.ALCOlabel);
            this.panel1.Controls.Add(this.PERNfield);
            this.panel1.Controls.Add(this.PERNlabel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(840, 554);
            this.panel1.TabIndex = 1;
            // 
            // REPNfield
            // 
            this.REPNfield.Location = new System.Drawing.Point(552, 188);
            this.REPNfield.MaxLength = 12;
            this.REPNfield.Name = "REPNfield";
            this.REPNfield.Size = new System.Drawing.Size(120, 20);
            this.REPNfield.TabIndex = 71;
            this.REPNfield.Tag = "REPN";
            // 
            // ReceiptNlabel
            // 
            this.ReceiptNlabel.Location = new System.Drawing.Point(552, 172);
            this.ReceiptNlabel.Name = "ReceiptNlabel";
            this.ReceiptNlabel.Size = new System.Drawing.Size(172, 16);
            this.ReceiptNlabel.TabIndex = 70;
            this.ReceiptNlabel.Text = "REF Permit Nr. or ATD Number";
            // 
            // PRFFCheckBox
            // 
            this.PRFFCheckBox.Location = new System.Drawing.Point(16, 124);
            this.PRFFCheckBox.Name = "PRFFCheckBox";
            this.PRFFCheckBox.Size = new System.Drawing.Size(136, 24);
            this.PRFFCheckBox.TabIndex = 69;
            this.PRFFCheckBox.Text = "Private Flight";
            this.PRFFCheckBox.CheckedChanged += new System.EventHandler(this.OnPrivateFlightCheckChanged);
            // 
            // SortAlcButton
            // 
            this.SortAlcButton.Location = new System.Drawing.Point(44, 68);
            this.SortAlcButton.Name = "SortAlcButton";
            this.SortAlcButton.Size = new System.Drawing.Size(40, 20);
            this.SortAlcButton.TabIndex = 68;
            this.SortAlcButton.Text = "button1";
            this.SortAlcButton.Visible = false;
            this.SortAlcButton.Click += new System.EventHandler(this.SortAlcButton_Click);
            // 
            // SelAlcButton
            // 
            this.SelAlcButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SelAlcButton.Location = new System.Drawing.Point(772, 244);
            this.SelAlcButton.Name = "SelAlcButton";
            this.SelAlcButton.Size = new System.Drawing.Size(24, 20);
            this.SelAlcButton.TabIndex = 67;
            this.SelAlcButton.Text = "...";
            this.SelAlcButton.Visible = false;
            this.SelAlcButton.Click += new System.EventHandler(this.SelAlcButton_Click);
            // 
            // ALCOfield
            // 
            this.ALCOfield.BackColor = System.Drawing.Color.White;
            this.ALCOfield.ForeColor = System.Drawing.Color.Black;
            this.ALCOfield.Location = new System.Drawing.Point(688, 244);
            this.ALCOfield.Name = "ALCOfield";
            this.ALCOfield.ReadOnly = true;
            this.ALCOfield.Size = new System.Drawing.Size(84, 20);
            this.ALCOfield.TabIndex = 66;
            this.ALCOfield.Visible = false;
            // 
            // LSTUfield
            // 
            this.LSTUfield.CustomFormat = "dd.MM.yyyyy - HH:mm";
            this.LSTUfield.Enabled = false;
            this.LSTUfield.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.LSTUfield.Location = new System.Drawing.Point(576, 516);
            this.LSTUfield.Name = "LSTUfield";
            this.LSTUfield.Size = new System.Drawing.Size(120, 20);
            this.LSTUfield.TabIndex = 65;
            // 
            // CDATfield
            // 
            this.CDATfield.CustomFormat = "dd.MM.yyyyy - HH:mm";
            this.CDATfield.Enabled = false;
            this.CDATfield.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.CDATfield.Location = new System.Drawing.Point(576, 488);
            this.CDATfield.Name = "CDATfield";
            this.CDATfield.Size = new System.Drawing.Size(120, 20);
            this.CDATfield.TabIndex = 64;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.DepRadioButton);
            this.panel2.Controls.Add(this.ArrRadioButton);
            this.panel2.Location = new System.Drawing.Point(236, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(188, 52);
            this.panel2.TabIndex = 63;
            // 
            // DepRadioButton
            // 
            this.DepRadioButton.Location = new System.Drawing.Point(38, 28);
            this.DepRadioButton.Name = "DepRadioButton";
            this.DepRadioButton.Size = new System.Drawing.Size(124, 20);
            this.DepRadioButton.TabIndex = 66;
            this.DepRadioButton.Text = "Departure";
            // 
            // ArrRadioButton
            // 
            this.ArrRadioButton.Location = new System.Drawing.Point(38, 4);
            this.ArrRadioButton.Name = "ArrRadioButton";
            this.ArrRadioButton.Size = new System.Drawing.Size(124, 20);
            this.ArrRadioButton.TabIndex = 65;
            this.ArrRadioButton.Text = "Arrival";
            // 
            // LSTUlabel
            // 
            this.LSTUlabel.Location = new System.Drawing.Point(412, 520);
            this.LSTUlabel.Name = "LSTUlabel";
            this.LSTUlabel.Size = new System.Drawing.Size(148, 16);
            this.LSTUlabel.TabIndex = 61;
            this.LSTUlabel.Tag = "";
            this.LSTUlabel.Text = "Date/Time of last change:";
            // 
            // USEUfield
            // 
            this.USEUfield.Location = new System.Drawing.Point(156, 520);
            this.USEUfield.Name = "USEUfield";
            this.USEUfield.ReadOnly = true;
            this.USEUfield.Size = new System.Drawing.Size(184, 20);
            this.USEUfield.TabIndex = 60;
            this.USEUfield.Tag = "USEU";
            // 
            // USEUlabel
            // 
            this.USEUlabel.Location = new System.Drawing.Point(28, 520);
            this.USEUlabel.Name = "USEUlabel";
            this.USEUlabel.Size = new System.Drawing.Size(120, 16);
            this.USEUlabel.TabIndex = 59;
            this.USEUlabel.Tag = "";
            this.USEUlabel.Text = "Changed by:";
            // 
            // CDATlabel
            // 
            this.CDATlabel.Location = new System.Drawing.Point(412, 488);
            this.CDATlabel.Name = "CDATlabel";
            this.CDATlabel.Size = new System.Drawing.Size(148, 16);
            this.CDATlabel.TabIndex = 57;
            this.CDATlabel.Tag = "";
            this.CDATlabel.Text = "Date/Time of entry:";
            // 
            // USECfield
            // 
            this.USECfield.Location = new System.Drawing.Point(156, 488);
            this.USECfield.Name = "USECfield";
            this.USECfield.ReadOnly = true;
            this.USECfield.Size = new System.Drawing.Size(184, 20);
            this.USECfield.TabIndex = 56;
            this.USECfield.Tag = "USEC";
            // 
            // USEClabel
            // 
            this.USEClabel.Location = new System.Drawing.Point(28, 488);
            this.USEClabel.Name = "USEClabel";
            this.USEClabel.Size = new System.Drawing.Size(120, 16);
            this.USEClabel.TabIndex = 55;
            this.USEClabel.Tag = "";
            this.USEClabel.Text = "Entered by:";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(14, 460);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(772, 2);
            this.label1.TabIndex = 54;
            // 
            // USRRfield
            // 
            this.USRRfield.AcceptsReturn = true;
            this.USRRfield.Location = new System.Drawing.Point(552, 320);
            this.USRRfield.MaxLength = 2000;
            this.USRRfield.Multiline = true;
            this.USRRfield.Name = "USRRfield";
            this.USRRfield.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.USRRfield.Size = new System.Drawing.Size(236, 112);
            this.USRRfield.TabIndex = 53;
            this.USRRfield.Tag = "USRR";
            // 
            // USRRlabel
            // 
            this.USRRlabel.Location = new System.Drawing.Point(556, 300);
            this.USRRlabel.Name = "USRRlabel";
            this.USRRlabel.Size = new System.Drawing.Size(232, 16);
            this.USRRlabel.TabIndex = 52;
            this.USRRlabel.Text = "User Defined Remark:";
            // 
            // OBLRfield
            // 
            this.OBLRfield.AcceptsReturn = true;
            this.OBLRfield.Location = new System.Drawing.Point(282, 320);
            this.OBLRfield.MaxLength = 2000;
            this.OBLRfield.Multiline = true;
            this.OBLRfield.Name = "OBLRfield";
            this.OBLRfield.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.OBLRfield.Size = new System.Drawing.Size(236, 112);
            this.OBLRfield.TabIndex = 51;
            this.OBLRfield.Tag = "OBLR";
            // 
            // OBLRlabel
            // 
            this.OBLRlabel.Location = new System.Drawing.Point(286, 300);
            this.OBLRlabel.Name = "OBLRlabel";
            this.OBLRlabel.Size = new System.Drawing.Size(232, 16);
            this.OBLRlabel.TabIndex = 50;
            this.OBLRlabel.Text = "Obligation Remark:";
            // 
            // PERRfield
            // 
            this.PERRfield.AcceptsReturn = true;
            this.PERRfield.Location = new System.Drawing.Point(12, 320);
            this.PERRfield.MaxLength = 2000;
            this.PERRfield.Multiline = true;
            this.PERRfield.Name = "PERRfield";
            this.PERRfield.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.PERRfield.Size = new System.Drawing.Size(236, 112);
            this.PERRfield.TabIndex = 49;
            this.PERRfield.Tag = "PERR";
            // 
            // PERRlabel
            // 
            this.PERRlabel.Location = new System.Drawing.Point(16, 300);
            this.PERRlabel.Name = "PERRlabel";
            this.PERRlabel.Size = new System.Drawing.Size(232, 16);
            this.PERRlabel.TabIndex = 48;
            this.PERRlabel.Text = "Permit Remark:";
            // 
            // KGlabel
            // 
            this.KGlabel.Location = new System.Drawing.Point(140, 268);
            this.KGlabel.Name = "KGlabel";
            this.KGlabel.Size = new System.Drawing.Size(44, 16);
            this.KGlabel.TabIndex = 47;
            this.KGlabel.Text = "ton";
            // 
            // line2
            // 
            this.line2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.line2.Location = new System.Drawing.Point(14, 228);
            this.line2.Name = "line2";
            this.line2.Size = new System.Drawing.Size(772, 2);
            this.line2.TabIndex = 46;
            // 
            // line1
            // 
            this.line1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.line1.Location = new System.Drawing.Point(12, 152);
            this.line1.Name = "line1";
            this.line1.Size = new System.Drawing.Size(772, 2);
            this.line1.TabIndex = 45;
            // 
            // MTOWfield
            // 
            this.MTOWfield.Location = new System.Drawing.Point(16, 264);
            this.MTOWfield.MaxLength = 10;
            this.MTOWfield.Name = "MTOWfield";
            this.MTOWfield.Size = new System.Drawing.Size(120, 20);
            this.MTOWfield.TabIndex = 44;
            this.MTOWfield.Tag = "MTOW";
            // 
            // MTOWlabel
            // 
            this.MTOWlabel.Location = new System.Drawing.Point(16, 248);
            this.MTOWlabel.Name = "MTOWlabel";
            this.MTOWlabel.Size = new System.Drawing.Size(112, 16);
            this.MTOWlabel.TabIndex = 43;
            this.MTOWlabel.Text = "MTOW";
            // 
            // DTGNfield
            // 
            this.DTGNfield.Location = new System.Drawing.Point(416, 188);
            this.DTGNfield.MaxLength = 6;
            this.DTGNfield.Name = "DTGNfield";
            this.DTGNfield.Size = new System.Drawing.Size(120, 20);
            this.DTGNfield.TabIndex = 42;
            this.DTGNfield.Tag = "DTGN";
            // 
            // ATDNfield
            // 
            this.ATDNfield.Location = new System.Drawing.Point(192, 188);
            this.ATDNfield.MaxLength = 12;
            this.ATDNfield.Name = "ATDNfield";
            this.ATDNfield.Size = new System.Drawing.Size(184, 20);
            this.ATDNfield.TabIndex = 41;
            this.ATDNfield.Tag = "ATDN";
            // 
            // DTGNlabel
            // 
            this.DTGNlabel.Location = new System.Drawing.Point(416, 172);
            this.DTGNlabel.Name = "DTGNlabel";
            this.DTGNlabel.Size = new System.Drawing.Size(112, 16);
            this.DTGNlabel.TabIndex = 40;
            this.DTGNlabel.Text = "DTGN";
            // 
            // ATDNlabel
            // 
            this.ATDNlabel.Location = new System.Drawing.Point(192, 172);
            this.ATDNlabel.Name = "ATDNlabel";
            this.ATDNlabel.Size = new System.Drawing.Size(120, 16);
            this.ATDNlabel.TabIndex = 39;
            this.ATDNlabel.Tag = "";
            this.ATDNlabel.Text = "ATD Number";
            // 
            // AHFPcheckBox
            // 
            this.AHFPcheckBox.Location = new System.Drawing.Point(16, 184);
            this.AHFPcheckBox.Name = "AHFPcheckBox";
            this.AHFPcheckBox.Size = new System.Drawing.Size(136, 24);
            this.AHFPcheckBox.TabIndex = 38;
            this.AHFPcheckBox.Text = "Adhoc Flight Permit";
            this.AHFPcheckBox.CheckedChanged += new System.EventHandler(this.AHFPcheckBox_CheckedChanged);
            // 
            // checkBoxDaily
            // 
            this.checkBoxDaily.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.checkBoxDaily.Location = new System.Drawing.Point(752, 64);
            this.checkBoxDaily.Name = "checkBoxDaily";
            this.checkBoxDaily.Size = new System.Drawing.Size(40, 40);
            this.checkBoxDaily.TabIndex = 37;
            this.checkBoxDaily.Text = "Daily";
            this.checkBoxDaily.CheckedChanged += new System.EventHandler(this.OnDailyCheckedChanged);
            // 
            // checkBox7
            // 
            this.checkBox7.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.checkBox7.Location = new System.Drawing.Point(720, 64);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(16, 40);
            this.checkBox7.TabIndex = 36;
            this.checkBox7.Text = "7";
            this.checkBox7.CheckedChanged += new System.EventHandler(this.OnCheckedChanged);
            // 
            // checkBox6
            // 
            this.checkBox6.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.checkBox6.Location = new System.Drawing.Point(696, 64);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(16, 40);
            this.checkBox6.TabIndex = 35;
            this.checkBox6.Text = "6";
            this.checkBox6.CheckedChanged += new System.EventHandler(this.OnCheckedChanged);
            // 
            // checkBox5
            // 
            this.checkBox5.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.checkBox5.Location = new System.Drawing.Point(672, 64);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(16, 40);
            this.checkBox5.TabIndex = 34;
            this.checkBox5.Text = "5";
            this.checkBox5.CheckedChanged += new System.EventHandler(this.OnCheckedChanged);
            // 
            // checkBox4
            // 
            this.checkBox4.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.checkBox4.Location = new System.Drawing.Point(648, 64);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(16, 40);
            this.checkBox4.TabIndex = 33;
            this.checkBox4.Text = "4";
            this.checkBox4.CheckedChanged += new System.EventHandler(this.OnCheckedChanged);
            // 
            // checkBox3
            // 
            this.checkBox3.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.checkBox3.Location = new System.Drawing.Point(624, 64);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(16, 40);
            this.checkBox3.TabIndex = 32;
            this.checkBox3.Text = "3";
            this.checkBox3.CheckedChanged += new System.EventHandler(this.OnCheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.checkBox2.Location = new System.Drawing.Point(600, 64);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(16, 40);
            this.checkBox2.TabIndex = 31;
            this.checkBox2.Text = "2";
            this.checkBox2.CheckedChanged += new System.EventHandler(this.OnCheckedChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.checkBox1.Location = new System.Drawing.Point(576, 64);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(16, 40);
            this.checkBox1.TabIndex = 30;
            this.checkBox1.Text = "1";
            this.checkBox1.CheckedChanged += new System.EventHandler(this.OnCheckedChanged);
            // 
            // ToField
            // 
            this.ToField.CustomFormat = "dd.MM.yyyy - HH:mm";
            this.ToField.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.ToField.Location = new System.Drawing.Point(440, 88);
            this.ToField.Name = "ToField";
            this.ToField.Size = new System.Drawing.Size(120, 20);
            this.ToField.TabIndex = 29;
            this.ToField.ValueChanged += new System.EventHandler(this.ToField_ValueChanged);
            // 
            // FromField
            // 
            this.FromField.CustomFormat = "dd.MM.yyyyy - HH:mm";
            this.FromField.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FromField.Location = new System.Drawing.Point(312, 88);
            this.FromField.Name = "FromField";
            this.FromField.Size = new System.Drawing.Size(120, 20);
            this.FromField.TabIndex = 28;
            // 
            // REGNfield
            // 
            this.REGNfield.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.REGNfield.Location = new System.Drawing.Point(216, 88);
            this.REGNfield.MaxLength = 12;
            this.REGNfield.Name = "REGNfield";
            this.REGNfield.Size = new System.Drawing.Size(80, 20);
            this.REGNfield.TabIndex = 27;
            this.REGNfield.Tag = "REGN";
            // 
            // AlcComboBox
            // 
            this.AlcComboBox.BackColor = System.Drawing.Color.Yellow;
            this.AlcComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AlcComboBox.Location = new System.Drawing.Point(16, 88);
            this.AlcComboBox.Name = "AlcComboBox";
            this.AlcComboBox.Size = new System.Drawing.Size(84, 21);
            this.AlcComboBox.TabIndex = 26;
            // 
            // SUFXfield
            // 
            this.SUFXfield.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.SUFXfield.Location = new System.Drawing.Point(172, 88);
            this.SUFXfield.MaxLength = 1;
            this.SUFXfield.Name = "SUFXfield";
            this.SUFXfield.Size = new System.Drawing.Size(24, 20);
            this.SUFXfield.TabIndex = 25;
            this.SUFXfield.Tag = "SUFX";
            // 
            // FLNOfield
            // 
            this.FLNOfield.Location = new System.Drawing.Point(104, 88);
            this.FLNOfield.MaxLength = 10;
            this.FLNOfield.Name = "FLNOfield";
            this.FLNOfield.Size = new System.Drawing.Size(64, 20);
            this.FLNOfield.TabIndex = 24;
            this.FLNOfield.Tag = "FLNO";
            // 
            // VATOlabel
            // 
            this.VATOlabel.Location = new System.Drawing.Point(440, 72);
            this.VATOlabel.Name = "VATOlabel";
            this.VATOlabel.Size = new System.Drawing.Size(88, 16);
            this.VATOlabel.TabIndex = 9;
            this.VATOlabel.Text = "Valid Until";
            // 
            // VAFRlabel
            // 
            this.VAFRlabel.Location = new System.Drawing.Point(320, 72);
            this.VAFRlabel.Name = "VAFRlabel";
            this.VAFRlabel.Size = new System.Drawing.Size(88, 16);
            this.VAFRlabel.TabIndex = 8;
            this.VAFRlabel.Text = "Valid From";
            // 
            // REGNlabel
            // 
            this.REGNlabel.Location = new System.Drawing.Point(216, 72);
            this.REGNlabel.Name = "REGNlabel";
            this.REGNlabel.Size = new System.Drawing.Size(72, 16);
            this.REGNlabel.TabIndex = 7;
            this.REGNlabel.Text = "Registration";
            // 
            // SUFXlabel
            // 
            this.SUFXlabel.Location = new System.Drawing.Point(172, 72);
            this.SUFXlabel.Name = "SUFXlabel";
            this.SUFXlabel.Size = new System.Drawing.Size(24, 16);
            this.SUFXlabel.TabIndex = 6;
            this.SUFXlabel.Text = "S";
            // 
            // FLNOlabel
            // 
            this.FLNOlabel.Location = new System.Drawing.Point(104, 72);
            this.FLNOlabel.Name = "FLNOlabel";
            this.FLNOlabel.Size = new System.Drawing.Size(40, 16);
            this.FLNOlabel.TabIndex = 5;
            this.FLNOlabel.Text = "Flno";
            // 
            // ALCOlabel
            // 
            this.ALCOlabel.Location = new System.Drawing.Point(16, 72);
            this.ALCOlabel.Name = "ALCOlabel";
            this.ALCOlabel.Size = new System.Drawing.Size(24, 16);
            this.ALCOlabel.TabIndex = 4;
            this.ALCOlabel.Text = "AL";
            // 
            // PERNfield
            // 
            this.PERNfield.BackColor = System.Drawing.Color.Yellow;
            this.PERNfield.Location = new System.Drawing.Point(16, 32);
            this.PERNfield.MaxLength = 32;
            this.PERNfield.Name = "PERNfield";
            this.PERNfield.Size = new System.Drawing.Size(200, 20);
            this.PERNfield.TabIndex = 1;
            this.PERNfield.Tag = "PERN";
            // 
            // PERNlabel
            // 
            this.PERNlabel.Location = new System.Drawing.Point(16, 16);
            this.PERNlabel.Name = "PERNlabel";
            this.PERNlabel.Size = new System.Drawing.Size(144, 16);
            this.PERNlabel.TabIndex = 0;
            this.PERNlabel.Text = "Permit Number";
            // 
            // frmDetail
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(840, 582);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolBar1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmDetail";
            this.Text = "Flight Permits Rule";
            this.Load += new System.EventHandler(this.frmDetail_Load);
            this.Closed += new System.EventHandler(this.frmDetail_Closed);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.frmDetail_HelpRequested);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        /// <summary>
        /// Pass data to the form before it is displayed
        /// </summary>
        /// <param name="opRow">The row of data to be changed or an empty row for inserting</param>
        /// <param name="opAirlineCodes">List of airline codes in format "ALC2/ALC3"</param>
        public void SetData(ref IRow opRow, ArrayList opAirlineCodes)
        {
            omRow = opRow;
            omAirlineCodes = opAirlineCodes;

            if (omRow["URNO"] == "")
                bmInsert = true;
            else
                bmInsert = false;
        }
        /// <summary>
        /// Sets the bmCopy Flag
        /// </summary>
        public void SetCopyFlag()
        {
            this.bmCopy = true;
        }


        /// <summary>
        /// Form is being displayed so load the data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmDetail_Load(object sender, System.EventArgs e)
        {

            omDelegateUpdateFpeData = new FlightPermits.DE.UpdateFpeData(OnUpdateFpeData);
            DE.OnUpdateFpeData += omDelegateUpdateFpeData;

            LoadData();
        }

        private void LoadData()
        {
            if (omRow != null)
            {
                FromField.Value = UT.CedaFullDateToDateTime(omRow["VAFR"]);
                ToField.Value = UT.CedaFullDateToDateTime(omRow["VATO"]);
                if (omRow["CDAT"].Length > 0)
                    CDATfield.Value = UT.CedaFullDateToDateTime(omRow["CDAT"]);
                else
                    CDATfield.Value = DateTime.Now;
                if (omRow["LSTU"].Length > 0)
                    LSTUfield.Value = UT.CedaFullDateToDateTime(omRow["LSTU"]);
                else
                    LSTUfield.Value = CDATfield.Value;

                string olDoop = omRow["DOOP"];
                checkBox1.Checked = olDoop.IndexOf("1") > -1;
                checkBox2.Checked = olDoop.IndexOf("2") > -1;
                checkBox3.Checked = olDoop.IndexOf("3") > -1;
                checkBox4.Checked = olDoop.IndexOf("4") > -1;
                checkBox5.Checked = olDoop.IndexOf("5") > -1;
                checkBox6.Checked = olDoop.IndexOf("6") > -1;
                checkBox7.Checked = olDoop.IndexOf("7") > -1;
                SetDailyFlag();

                string olAdid = omRow["ADID"];
                if (olAdid == "A")
                    ArrRadioButton.Checked = true;
                else
                    DepRadioButton.Checked = true;

                string olAdhocFlightPermit = omRow["ADFP"];
                if (olAdhocFlightPermit == "X")
                    AHFPcheckBox.Checked = true;
                else
                    AHFPcheckBox.Checked = false;

                SetAlcComboBox();

                // set all other fields
                for (int i = 0; i < omFields.Length; i++)
                {
                    string olFieldName = omFields[i];
                    SetFieldValue(olFieldName, omRow[olFieldName]);
                }

                // Set the Private Flight Check Box State. Check for the Alirline Code Value
                // If the Airline code is Empty then set the Private Flight to 'Checked' state.
                if (USECfield.Text.Length > 0)
                {
                    if (AlcComboBox.Text.Length == 0)
                        PRFFCheckBox.Checked = true;
                    else
                        PRFFCheckBox.Checked = false;
                }
                else
                {
                    if (REGNfield.Text.Length == 0)
                        PRFFCheckBox.Checked = false;
                    else
                        PRFFCheckBox.Checked = true;
                }

                DE.SetPrivs(SaveButton, "DetailDialogSave");
                DE.SetPrivs(PrintButton, "DetailDialogPrint");
                DE.SetPrivs(PERNfield, "DetailDialogPern");
                DE.SetPrivs(PERNlabel, "DetailDialogPern");
                DE.SetPrivs(ArrRadioButton, "DetailDialogAdid");
                DE.SetPrivs(DepRadioButton, "DetailDialogAdid");
                DE.SetPrivs(AlcComboBox, "DetailDialogAlco");
                DE.SetPrivs(ALCOlabel, "DetailDialogAlco");
                DE.SetPrivs(FLNOfield, "DetailDialogFlno");
                DE.SetPrivs(FLNOlabel, "DetailDialogFlno");
                DE.SetPrivs(SUFXfield, "DetailDialogSufx");
                DE.SetPrivs(SUFXlabel, "DetailDialogSufx");
                DE.SetPrivs(REGNfield, "DetailDialogRegn");
                DE.SetPrivs(REGNlabel, "DetailDialogRegn");
                DE.SetPrivs(FromField, "DetailDialogVafr");
                DE.SetPrivs(VAFRlabel, "DetailDialogVafr");
                DE.SetPrivs(ToField, "DetailDialogVato");
                DE.SetPrivs(VATOlabel, "DetailDialogVato");
                DE.SetPrivs(checkBox1, "DetailDialogDoop");
                DE.SetPrivs(checkBox2, "DetailDialogDoop");
                DE.SetPrivs(checkBox3, "DetailDialogDoop");
                DE.SetPrivs(checkBox4, "DetailDialogDoop");
                DE.SetPrivs(checkBox5, "DetailDialogDoop");
                DE.SetPrivs(checkBox6, "DetailDialogDoop");
                DE.SetPrivs(checkBox7, "DetailDialogDoop");
                DE.SetPrivs(checkBoxDaily, "DetailDialogDoop");
                DE.SetPrivs(AHFPcheckBox, "DetailDialogAhfp");
                DE.SetPrivs(ATDNfield, "DetailDialogAtdn");
                DE.SetPrivs(ATDNlabel, "DetailDialogAtdn");
                DE.SetPrivs(DTGNfield, "DetailDialogDtgn");
                DE.SetPrivs(DTGNlabel, "DetailDialogDtgn");
                DE.SetPrivs(MTOWfield, "DetailDialogMtow");
                DE.SetPrivs(MTOWlabel, "DetailDialogMtow");
                DE.SetPrivs(KGlabel, "DetailDialogMtow");
                DE.SetPrivs(PERRfield, "DetailDialogPerr");
                DE.SetPrivs(PERRlabel, "DetailDialogPerr");
                DE.SetPrivs(OBLRfield, "DetailDialogOblr");
                DE.SetPrivs(OBLRlabel, "DetailDialogOblr");
                DE.SetPrivs(USRRfield, "DetailDialogUsrr");
                DE.SetPrivs(USRRlabel, "DetailDialogUsrr");

                // If Airline code doesn't contain text
                if (AlcComboBox.Text.Length == 0)
                {
                    PRFFCheckBox.Enabled = true;

                    // Check the PrivateFlight checkbox state.
                    if (PRFFCheckBox.Checked)
                    {
                        this.AlcComboBox.BackColor = System.Drawing.Color.LightGray;
                        this.AlcComboBox.Enabled = false;
                        this.FLNOfield.Enabled = false;
                        this.SUFXfield.Enabled = false;
                        this.REGNfield.BackColor = System.Drawing.Color.Yellow;
                    }
                }
                else
                    PRFFCheckBox.Enabled = false;

                bmInitialized = true;
            }
        }

        private void SetAlcComboBox()
        {
            AlcComboBox.DataSource = omAirlineCodes;
            AlcComboBox.DisplayMember = bmAlc2 ? "Code1" : "Code2";
            AlcComboBox.ValueMember = "Alc3";
            AlcComboBox.SelectedValue = omRow["ALCO"];
            //AlcComboBox.Sorted = true;
            AlcComboBox.Refresh();
        }

        /// <summary>
        /// Broadcast received for FPETAB
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="Urno"></param>
        /// <param name="state"></param>
        private void OnUpdateFpeData(object sender, string Urno, State state)
        {
            string olMyUrno = omRow["URNO"];
            if (Urno == olMyUrno)
            {
                if (state == State.Modified)
                {
                    DialogResult olResult = MessageBox.Show("This flight permit has been changed by another user. Do you want to update this form with the changes made?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (olResult == DialogResult.Yes)
                    {
                        IDatabase omDB = UT.GetMemDB();
                        if (omDB != null)
                        {
                            ITable omFpeTab = omDB["FPE"];
                            if (omFpeTab != null)
                            {
                                IRow[] olRows = omFpeTab.RowsByIndexValue("URNO", Urno);
                                if (olRows.Length > 0)
                                {
                                    omRow = olRows[0];
                                    LoadData();
                                }
                            }
                        }
                    }
                }
                else if (state == State.Deleted)
                {
                    MessageBox.Show("This flight permit has been deleted by another user. The form will be closed.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    DialogResult = DialogResult.Abort;
                }
            }
        }

        /// <summary>
        /// Set the control referenced by opFieldName to opValue
        /// </summary>
        /// <param name="opFieldName">Name of the field to set</param>
        /// <param name="opValue">Value to set the field to</param>
        /// <returns>Returns true if the field was found</returns>
        private bool SetFieldValue(string opFieldName, string opValue)
        {
            bool blWasSet = false;
            Control ctl = null;
            ctl = GetControlByTag(opFieldName, this, ref ctl);
            if (ctl != null)
            {
                //Replace the Special Character('\x0001') with with ','char
                if (opValue.IndexOf('\x0001') != -1)
                    opValue = opValue.Replace('\x0001', ',');

                //Replace the End of Line('\r\n') string in the place of '\x0002'
                if ((opFieldName == "PERR" || opFieldName == "OBLR" || opFieldName == "USRR") && (opValue.IndexOf('\x0002') != -1))
                    opValue = opValue.Replace('\x0002'.ToString(), "\r\n");

                ctl.Text = opValue;
                blWasSet = true;
            }

            return blWasSet;
        }

        /// <summary>
        /// Returns the value of the field reference by opFieldName
        /// </summary>
        /// <param name="opFieldName">Name of the field</param>
        /// <returns>Returns the value of the field</returns>
        private bool GetAndValidateFieldValue(string opFieldName, ref string opFieldValue, ref string opErrMess)
        {
            bool blOk = true;
            Control ctl = null;
            ctl = GetControlByTag(opFieldName, this, ref ctl);
            if (ctl != null)
            {
                opFieldValue = ctl.Text;
                if (ctl.BackColor == System.Drawing.Color.Yellow && opFieldValue.Length <= 0)
                {
                    blOk = false;
                    opErrMess = "Field must contain a value.";
                }
                else
                {
                    // Replace the End of Line('\r\n') string with '\x0002'
                    if ((opFieldName == "PERR" || opFieldName == "OBLR" || opFieldName == "USRR") && (opFieldValue.IndexOf("\r\n") != -1))
                    {
                        opFieldValue = opFieldValue.Replace("\r\n", '\x0002'.ToString());
                    }
                    else if (opFieldName == "REGN" && opFieldValue.Length > 0) // Regn is Unique
                    {
                        if (omRow["REGN"] != opFieldValue) // Is the Regn Value has been changed.?
                            if (checkRegn_Uniqueness(opFieldValue))
                            {
                                opErrMess = "Registration Value is already exists";
                                MessageBox.Show(opErrMess, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                    }
                    //Validate ATD Number
                    else if ((opFieldName == "ATDN") && opFieldValue.Length > 0)
                    {
                        if (omRow["ATDN"] != opFieldValue) // ATDN Value has changed
                        {
                            if (!ValidateATDNumber(opFieldValue))
                            {
                                opErrMess = "Valid ATD Number value Format is XXXXX/MM/YY";
                                blOk = false;
                            }
                        }
                    }
                    else if ((opFieldName == "DTGN") && opFieldValue.Length > 0)
                    {
                        if (omRow["DTGN"] != opFieldValue) // DTGN Value has changed
                        {
                            try
                            {
                                int k = Convert.ToInt32(opFieldValue);
                                // Validate the DTGN Number
                                if (k == 0 || k > 999999)
                                {
                                    opErrMess = "Valid DTGN Value is > 0 and <= 999999";
                                    blOk = false;
                                }
                            }
                            catch
                            {
                                blOk = false;
                                opErrMess = "Valid DTGN Value is > 0 and <= 999999";
                            }
                        }
                    }
                }
            }
            else
            {
                blOk = false;
                opErrMess = "Internal Error. Field: " + opFieldName + " not found.";
            }
            return blOk;
        }

        /// <summary>
        /// Searches through all controls on this form for the control whose tag property matches the parameter tag
        /// </summary>
        /// <param name="tag">Name of the control</param>
        /// <param name="currCtl"></param>
        /// <param name="myControl"></param>
        /// <returns>Returns the control referenced by tag</returns>
        private Control GetControlByTag(string tag, Control currCtl, ref Control myControl)
        {
            if (myControl == null)
            {
                foreach (System.Windows.Forms.Control olC in currCtl.Controls)
                {
                    string lType = olC.GetType().ToString();
                    if (lType == "System.Windows.Forms.PictureBox" || lType == "System.Windows.Forms.Panel")
                    {
                        if (olC.Tag != null)
                            if (olC.Tag.ToString() != tag)
                                myControl = GetControlByTag(tag.ToString(), olC, ref myControl);
                            else
                                myControl = olC;
                        else
                            myControl = GetControlByTag(tag.ToString(), olC, ref myControl);
                    }
                    else
                    {
                        if (olC.Tag != null)
                        {
                            if (olC.Tag.ToString() == tag)
                                myControl = olC;
                        }
                    }
                }
            }
            return myControl;
        }

        /// <summary>
        /// Called when one of the Day Of Operation check boxes was changed so that the Daily checkbox will be set correctly
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnCheckedChanged(object sender, System.EventArgs e)
        {
            SetDailyFlag();
        }

        /// <summary>
        /// If all Day Of Operation checkBoxes are set then set the Daily CheckBox else clear it
        /// </summary>
        private void SetDailyFlag()
        {
            if (checkBox1.Checked && checkBox2.Checked && checkBox3.Checked && checkBox4.Checked && checkBox5.Checked && checkBox6.Checked && checkBox7.Checked)
                checkBoxDaily.Checked = true;
            else
                checkBoxDaily.Checked = false;
        }

        /// <summary>
        /// If the Daily checkBox is set then set all the Day Of Operation checkBoxes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDailyCheckedChanged(object sender, System.EventArgs e)
        {
            TimeSpan diffDate = ToField.Value.Date.Subtract(FromField.Value.Date);

            string dayofWeek = FromField.Value.DayOfWeek.ToString();

            if (checkBoxDaily.Checked && diffDate.Days >= 6)
            {
                checkBox1.Checked = true;
                checkBox2.Checked = true;
                checkBox3.Checked = true;
                checkBox4.Checked = true;
                checkBox5.Checked = true;
                checkBox6.Checked = true;
                checkBox7.Checked = true;
            }

        }

        /// <summary>
        /// One of the buttons in the toolbar was clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void toolBar1_ButtonClick(object sender, System.Windows.Forms.ToolBarButtonClickEventArgs e)
        {
            try
            {
                if ((string)e.Button.Tag == "SAVE")
                {
                    if (GetAndValidateChanges())
                    {
                        this.Close();
                    }
                }
                else if ((string)e.Button.Tag == "PRINT")
                {
                    string olErrMess = "";
                    IRow olTmpRow = omRow.CopyRaw();
                    GetChanges(ref olTmpRow, ref olErrMess);
                    rptDetail olRptDetail = new rptDetail(olTmpRow);
                    Ufis.Utils.frmPrintPreview olPrintPreview = new Ufis.Utils.frmPrintPreview(olRptDetail);
                    olPrintPreview.Show();
                }
                else if ((string)e.Button.Tag == "CLOSE")
                {
                    DialogResult = DialogResult.Cancel;
                    this.Close();
                }
                else if ((string)e.Button.Tag == "HELP")
                {
                    ShowHelp();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Get any values changed and check them
        /// </summary>
        /// <returns>Returns true if all changes are valid</returns>
        private bool GetAndValidateChanges()
        {
            bool blOk = true;
            string olErrMess = "";
            IRow olTmpRow = omRow.CopyRaw();
            GetChanges(ref olTmpRow, ref olErrMess);

            if (olErrMess.Length > 0)
            {
                string olCaption = "Error in " + Application.ProductName;
                MessageBox.Show(olErrMess, olCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                blOk = false;
            }
            else
            {
                // copy changes to the row
                bool blChanged = false;
                for (int i = 0; i < olTmpRow.Count; i++)
                {
                    if (omRow[i] != olTmpRow[i])
                    {
                        blChanged = true;
                        omRow[i] = olTmpRow[i];
                    }
                }

                if (blChanged)
                {
                    //Place to encript. Replace the comma(',') charecter with '\x0001' Special char.
                    for (int i = 0; i < omRow.Count; i++)
                    {
                        if (omRow[i].IndexOf(",") != -1)
                            omRow[i] = omRow[i].Replace(',', '\x0001');
                    }

                    //DateTime olCurrTime = UT.LocalToUtc(DateTime.Now);
                    DateTime olCurrTime = DateTime.Now;
                    if (bmInsert || bmCopy)
                    {
                        omRow["CDAT"] = UT.DateTimeToCeda(olCurrTime);
                        omRow["USEC"] = UT.UserName;
                        omRow.Status = State.Created;
                    }
                    else
                    {
                        omRow["LSTU"] = UT.DateTimeToCeda(olCurrTime);
                        omRow["USEU"] = UT.UserName;
                        omRow.Status = State.Modified;
                    }

                    DialogResult = DialogResult.OK;
                }
                else
                {
                    DialogResult = DialogResult.Cancel;
                }
            }
            return blOk;
        }

        private void GetChanges(ref IRow opTmpRow, ref string opErrMess)
        {
            //string olFrom = UT.DateTimeToCeda(UT.LocalToUtc(FromField.Value));
            //string olTo = UT.DateTimeToCeda(UT.LocalToUtc(ToField.Value));
            string olFrom = UT.DateTimeToCeda(FromField.Value);
            string olTo = UT.DateTimeToCeda(ToField.Value);

            if (olFrom.CompareTo(olTo) >= 0)
            {
                opErrMess += "Valid From must be an earlier date than Valid To.\n";
            }
            else
            {
                opTmpRow["VAFR"] = olFrom;
                opTmpRow["VATO"] = olTo;
            }

            string olDoop = "";
            if (checkBox1.Checked)
                olDoop += "1";
            if (checkBox2.Checked)
                olDoop += "2";
            if (checkBox3.Checked)
                olDoop += "3";
            if (checkBox4.Checked)
                olDoop += "4";
            if (checkBox5.Checked)
                olDoop += "5";
            if (checkBox6.Checked)
                olDoop += "6";
            if (checkBox7.Checked)
                olDoop += "7";

            if (olDoop.Length <= 0)
            {
                opErrMess += "At least one day of operation must be selected.\n";
            }
            else
            {
                opTmpRow["DOOP"] = olDoop;
            }

            string olAlc = AlcComboBox.SelectedValue.ToString();
            if (AlcComboBox.BackColor == System.Drawing.Color.Yellow && olAlc.Length <= 0)
                opErrMess += "Error in field: AL - Field must contain a value.\n";
            else
                opTmpRow["ALCO"] = olAlc;
            opTmpRow["ADID"] = (ArrRadioButton.Checked) ? "A" : "D";
            opTmpRow["ADFP"] = (AHFPcheckBox.Checked) ? "X" : "";

            for (int i = 0; i < omFields.Length; i++)
            {
                string olFieldName = omFields[i];
                string opErrMess2 = "";
                string olValue = "";
                if (!GetAndValidateFieldValue(olFieldName, ref olValue, ref opErrMess2))
                    opErrMess += "Error in field: " + omFieldDescriptions[i] + " - " + opErrMess2 + "\n";
                else
                    opTmpRow[olFieldName] = olValue;
            }
        }

        private void AHFPcheckBox_CheckedChanged(object sender, System.EventArgs e)
        {
            if (AHFPcheckBox.Checked)
            {
                if (isPremitValidation)
                {
                    if (!PRFFCheckBox.Checked)
                    {
                        FLNOfield.BackColor = System.Drawing.Color.Yellow;
                    }
                    ATDNfield.BackColor = System.Drawing.Color.White;
                    DTGNfield.BackColor = System.Drawing.Color.White;
                    PERNfield.BackColor = System.Drawing.Color.Yellow;
                }
                else
                {
                    if (!PRFFCheckBox.Checked)
                    {
                        FLNOfield.BackColor = System.Drawing.Color.Yellow;
                    }
                    ATDNfield.BackColor = System.Drawing.Color.Yellow;
                    DTGNfield.BackColor = System.Drawing.Color.Yellow;
                    PERNfield.BackColor = System.Drawing.Color.White;
                }

                if (bmInitialized && bmInsert)
                {
                    DateTime olStart = DateTime.Now;
                    olStart = olStart.Subtract(new TimeSpan(0, olStart.Hour, olStart.Minute, olStart.Second));
                    DateTime olEnd = olStart.Add(new TimeSpan(0, 23, 59, 59));
                    FromField.Value = olStart;
                    ToField.Value = olEnd;

                    DayOfWeek olDay = olStart.DayOfWeek;
                    checkBox1.Checked = (olDay == DayOfWeek.Monday) ? true : false;
                    checkBox2.Checked = (olDay == DayOfWeek.Tuesday) ? true : false;
                    checkBox3.Checked = (olDay == DayOfWeek.Wednesday) ? true : false;
                    checkBox4.Checked = (olDay == DayOfWeek.Thursday) ? true : false;
                    checkBox5.Checked = (olDay == DayOfWeek.Friday) ? true : false;
                    checkBox6.Checked = (olDay == DayOfWeek.Saturday) ? true : false;
                    checkBox7.Checked = (olDay == DayOfWeek.Sunday) ? true : false;
                }
            }
            else
            {
                FLNOfield.BackColor = System.Drawing.Color.White;
                ATDNfield.BackColor = System.Drawing.Color.White;
                DTGNfield.BackColor = System.Drawing.Color.White;
                PERNfield.BackColor = System.Drawing.Color.Yellow;

                if (bmInitialized && bmInsert)
                {
                    DateTime olStart = DateTime.Now;
                    olStart = olStart.Subtract(new TimeSpan(0, olStart.Hour, olStart.Minute, olStart.Second));
                    DateTime olEnd = olStart.Add(new TimeSpan(364, 23, 59, 59));
                    FromField.Value = olStart;
                    ToField.Value = olEnd;
                }
            }
        }

        private void frmDetail_Closed(object sender, System.EventArgs e)
        {
            DE.OnUpdateFpeData -= omDelegateUpdateFpeData;
        }

        private void SelAlcButton_Click(object sender, System.EventArgs e)
        {
            frmSelAlc olSelAlcForm = new frmSelAlc();
            olSelAlcForm.ShowDialog();
            if (olSelAlcForm.DialogResult == DialogResult.OK)
            {
            }
        }

        private void SortAlcButton_Click(object sender, System.EventArgs e)
        {
            bmAlc2 = !bmAlc2;
            IComparer myComparer = null;
            if (bmAlc2)
                myComparer = new SortAsc();
            else
                myComparer = new SortDesc();
            omAirlineCodes.Sort(myComparer);
            SetAlcComboBox();
        }

        private void frmDetail_HelpRequested(object sender, System.Windows.Forms.HelpEventArgs hlpevent)
        {
            ShowHelp();
        }

        private void ShowHelp()
        {
            IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");

            string HelpFilePath = myIni.IniReadValue("GLOBAL", "HelpDirectory");
            if (HelpFilePath.Length <= 0)
                HelpFilePath = @"C:\Ufis\Help";

            string HelpFileName = myIni.IniReadValue("FLIGHTPERMITS", "HELPFILE");
            if (HelpFileName.Length <= 0)
                HelpFileName = @"FlightPermits.chm";

            string HelpFile = HelpFilePath + @"\" + HelpFileName;

            Help.ShowHelp(this, HelpFile);
        }
        /// <summary>
        /// Check the State of PrivateFlight check Box, If it is checked then make 
        /// Registration filed Mandatory instead of Airline code and disable the 
        /// fields Airline code, Flight Number and Suffix and vice versa.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnPrivateFlightCheckChanged(object sender, EventArgs e)
        {
            // If the Private field is cheked.?
            if (PRFFCheckBox.Checked)
            {
                this.AlcComboBox.BackColor = System.Drawing.Color.LightGray;
                this.AlcComboBox.Enabled = false;
                this.FLNOfield.Text = "";
                this.FLNOfield.BackColor = System.Drawing.Color.LightGray;
                this.FLNOfield.Enabled = false;
                this.SUFXfield.Enabled = false;
                this.REGNfield.BackColor = System.Drawing.Color.Yellow;
            }
            else
            {
                this.AlcComboBox.BackColor = System.Drawing.Color.Yellow;
                if (AHFPcheckBox.Checked)
                    this.FLNOfield.BackColor = System.Drawing.Color.Yellow;
                else
                    this.FLNOfield.BackColor = System.Drawing.Color.White;
                this.AlcComboBox.Enabled = true;
                this.FLNOfield.Enabled = true;
                this.SUFXfield.Enabled = true;
                this.REGNfield.BackColor = System.Drawing.Color.White;
            }

        }

        private void OnAlcComboBox_SelectionChanged(object sender, EventArgs e)
        {
            if (AlcComboBox.Text.Length != 0)
                PRFFCheckBox.Enabled = false;
            else
                PRFFCheckBox.Enabled = true;
        }
        /// <summary>
        /// Regestration value should be unique
        /// </summary>
        /// <param name="RegnValue">Registration Value</param>
        /// <returns></returns>
        private bool checkRegn_Uniqueness(string RegnValue)
        {
            IDatabase omDB = UT.GetMemDB();
            ITable omFpeTab = omDB["FPE"];
            omFpeTab.Load("WHERE REGN = " + RegnValue);
            omFpeTab.CreateIndex("REGN", "REGN");

            if (omFpeTab != null)
            {
                IRow[] olRows = omFpeTab.RowsByIndexValue("REGN", RegnValue);
                if (olRows.Length > 0)
                    return true;
            }
            return false;
        }
        /// <summary>
        /// Validate ATD Number Format <99999/12/99>
        /// </summary>
        /// <param name="ATDNumber">ATD Number Value</param>
        /// <returns></returns>
        private bool ValidateATDNumber(string ATDNumber)
        {
            string[] strArray = ATDNumber.Split('/');

            if (strArray.Length > 2)
            {
                try
                {
                    // Extract the Number, Month and year from the String
                    int iNum = Convert.ToInt32(strArray[0]);
                    int iMonth = Convert.ToInt32(strArray[1]);
                    int iYear = Convert.ToInt32(strArray[2]);

                    // Validate the ATD Num
                    if ((iNum != 0 && iNum <= 99999) && (iMonth != 0 && iMonth <= 12))
                    {
                        return true;
                    }
                }
                catch
                {
                    return false;
                }
            }

            return false;
        }

        private void ToField_ValueChanged(object sender, EventArgs e)
        {
            CheckDate();
        }

        private void CheckDate()
        {
            Checked(false);
            Enabled(false);

            TimeSpan diffDate = ToField.Value.Date.Subtract(FromField.Value.Date);

            if (diffDate.Days >= 0)
            {
                int caseSwitch = (Int32)((DayOfWeek)FromField.Value.DayOfWeek);

                if (diffDate.Days >= 6)
                {
                    Checked(true);
                    Enabled(true);
                }
                else
                {
                    switch (caseSwitch)
                    {
                        case 1:
                            checkBox1.Enabled = true;
                            checkBox1.Checked = true;
                            if (diffDate.Days == 5)
                            {
                                checkBox2.Enabled = true;
                                checkBox3.Enabled = true;
                                checkBox4.Enabled = true;
                                checkBox5.Enabled = true;
                                checkBox6.Enabled = true;

                                checkBox2.Checked = true;
                                checkBox3.Checked = true;
                                checkBox4.Checked = true;
                                checkBox5.Checked = true;
                                checkBox6.Checked = true;
                            }
                            else if (diffDate.Days == 4)
                            {
                                checkBox2.Enabled = true;
                                checkBox3.Enabled = true;
                                checkBox4.Enabled = true;
                                checkBox5.Enabled = true;

                                checkBox2.Checked = true;
                                checkBox3.Checked = true;
                                checkBox4.Checked = true;
                                checkBox5.Checked = true;
                            }
                            else if (diffDate.Days == 3)
                            {
                                checkBox2.Enabled = true;
                                checkBox3.Enabled = true;
                                checkBox4.Enabled = true;

                                checkBox2.Checked = true;
                                checkBox3.Checked = true;
                                checkBox4.Checked = true;

                            }
                            else if (diffDate.Days == 2)
                            {
                                checkBox2.Enabled = true;
                                checkBox3.Enabled = true;

                                checkBox2.Checked = true;
                                checkBox3.Checked = true;
                            }
                            else if (diffDate.Days == 1)
                            {
                                checkBox2.Enabled = true;
                                checkBox2.Checked = true;
                            }

                            break;
                        case 2:
                            checkBox2.Enabled = true;
                            checkBox2.Checked = true;
                            if (diffDate.Days == 5)
                            {
                                checkBox3.Enabled = true;
                                checkBox4.Enabled = true;
                                checkBox5.Enabled = true;
                                checkBox6.Enabled = true;
                                checkBox7.Enabled = true;

                                checkBox3.Checked = true;
                                checkBox4.Checked = true;
                                checkBox5.Checked = true;
                                checkBox6.Checked = true;
                                checkBox7.Checked = true;
                            }
                            else if (diffDate.Days == 4)
                            {
                                checkBox3.Enabled = true;
                                checkBox4.Enabled = true;
                                checkBox5.Enabled = true;
                                checkBox6.Enabled = true; 

                                checkBox3.Checked = true;
                                checkBox4.Checked = true;
                                checkBox5.Checked = true;
                                checkBox6.Checked = true;
                            }
                            else if (diffDate.Days == 3)
                            {
                                checkBox3.Enabled = true;
                                checkBox4.Enabled = true;
                                checkBox5.Enabled = true; 

                                checkBox3.Checked = true;
                                checkBox4.Checked = true;
                                checkBox5.Checked = true;
                            }
                            else if (diffDate.Days == 2)
                            {
                                checkBox3.Enabled = true;
                                checkBox4.Enabled = true; 

                                checkBox3.Checked = true;
                                checkBox4.Checked = true;
                            }
                            else if (diffDate.Days == 1)
                            {
                                checkBox3.Enabled = true; 

                                checkBox3.Checked = true;
                            }
                            break;
                        case 3:
                            checkBox3.Enabled = true;
                            checkBox3.Checked = true;
                            if (diffDate.Days == 5)
                            {
                                checkBox4.Enabled = true;
                                checkBox5.Enabled = true;
                                checkBox6.Enabled = true;
                                checkBox7.Enabled = true;
                                checkBox1.Enabled = true;

                                checkBox4.Checked = true;
                                checkBox5.Checked = true;
                                checkBox6.Checked = true;
                                checkBox7.Checked = true;
                                checkBox1.Checked = true;
                            }
                            else if (diffDate.Days == 4)
                            {
                                checkBox4.Enabled = true;
                                checkBox5.Enabled = true;
                                checkBox6.Enabled = true;
                                checkBox7.Enabled = true; 

                                checkBox4.Checked = true;
                                checkBox5.Checked = true;
                                checkBox6.Checked = true;
                                checkBox7.Checked = true;
                            }
                            else if (diffDate.Days == 3)
                            {
                                checkBox4.Enabled = true;
                                checkBox5.Enabled = true;
                                checkBox6.Enabled = true; 

                                checkBox4.Checked = true;
                                checkBox5.Checked = true;
                                checkBox6.Checked = true;
                            }
                            else if (diffDate.Days == 2)
                            {
                                checkBox4.Enabled = true;
                                checkBox5.Enabled = true; 

                                checkBox4.Checked = true;
                                checkBox5.Checked = true;
                            }
                            else if (diffDate.Days == 1)
                            {
                                checkBox4.Enabled = true; 

                                checkBox4.Checked = true;
                            }
                            break;
                        case 4:
                            checkBox4.Enabled = true;
                            checkBox4.Checked = true;
                            if (diffDate.Days == 5)
                            {
                                checkBox5.Enabled = true;
                                checkBox6.Enabled = true;
                                checkBox7.Enabled = true;
                                checkBox1.Enabled = true;
                                checkBox2.Enabled = true;

                                checkBox5.Checked = true;
                                checkBox6.Checked = true;
                                checkBox7.Checked = true;
                                checkBox1.Checked = true;
                                checkBox2.Checked = true;
                            }
                            else if (diffDate.Days == 4)
                            {
                                checkBox5.Enabled = true;
                                checkBox6.Enabled = true;
                                checkBox7.Enabled = true;
                                checkBox1.Enabled = true; 

                                checkBox5.Checked = true;
                                checkBox6.Checked = true;
                                checkBox7.Checked = true;
                                checkBox1.Checked = true;
                            }
                            else if (diffDate.Days == 3)
                            {
                                checkBox5.Enabled = true;
                                checkBox6.Enabled = true;
                                checkBox7.Enabled = true; 

                                checkBox5.Checked = true;
                                checkBox6.Checked = true;
                                checkBox7.Checked = true;
                            }
                            else if (diffDate.Days == 2)
                            {
                                checkBox5.Enabled = true;
                                checkBox6.Enabled = true; 

                                checkBox5.Checked = true;
                                checkBox6.Checked = true;
                            }
                            else if (diffDate.Days == 1)
                            {
                                checkBox5.Enabled = true; 

                                checkBox5.Checked = true;
                            }
                            break;
                        case 5:
                            checkBox5.Enabled = true;
                            checkBox5.Checked = true;
                            if (diffDate.Days == 5)
                            {
                                checkBox6.Enabled = true;
                                checkBox7.Enabled = true;
                                checkBox1.Enabled = true;
                                checkBox2.Enabled = true;
                                checkBox3.Enabled = true;

                                checkBox6.Checked = true;
                                checkBox7.Checked = true;
                                checkBox1.Checked = true;
                                checkBox2.Checked = true;
                                checkBox3.Checked = true;
                            }
                            else if (diffDate.Days == 4)
                            {
                                checkBox6.Enabled = true;
                                checkBox7.Enabled = true;
                                checkBox1.Enabled = true;
                                checkBox2.Enabled = true; 

                                checkBox6.Checked = true;
                                checkBox7.Checked = true;
                                checkBox1.Checked = true;
                                checkBox2.Checked = true;
                            }
                            else if (diffDate.Days == 3)
                            {
                                checkBox6.Enabled = true;
                                checkBox7.Enabled = true;
                                checkBox1.Enabled = true; 

                                checkBox6.Checked = true;
                                checkBox7.Checked = true;
                                checkBox1.Checked = true;
                            }
                            else if (diffDate.Days == 2)
                            {
                                checkBox6.Enabled = true;
                                checkBox7.Enabled = true; 

                                checkBox6.Checked = true;
                                checkBox7.Checked = true;
                            }
                            else if (diffDate.Days == 1)
                            {
                                checkBox6.Enabled = true; 

                                checkBox6.Checked = true;
                            }
                            break;
                        case 6:
                            checkBox6.Enabled = true;
                            checkBox6.Checked = true;
                            if (diffDate.Days == 5)
                            {
                                checkBox7.Enabled = true;
                                checkBox1.Enabled = true;
                                checkBox2.Enabled = true;
                                checkBox3.Enabled = true;
                                checkBox4.Enabled = true;

                                checkBox7.Checked = true;
                                checkBox1.Checked = true;
                                checkBox2.Checked = true;
                                checkBox3.Checked = true;
                                checkBox4.Checked = true;
                            }
                            else if (diffDate.Days == 4)
                            {
                                checkBox7.Enabled = true;
                                checkBox1.Enabled = true;
                                checkBox2.Enabled = true;
                                checkBox3.Enabled = true; 

                                checkBox7.Checked = true;
                                checkBox1.Checked = true;
                                checkBox2.Checked = true;
                                checkBox3.Checked = true;
                            }
                            else if (diffDate.Days == 3)
                            {
                                checkBox7.Enabled = true;
                                checkBox1.Enabled = true;
                                checkBox2.Enabled = true; 

                                checkBox7.Checked = true;
                                checkBox1.Checked = true;
                                checkBox2.Checked = true;
                            }
                            else if (diffDate.Days == 2)
                            {
                                checkBox7.Enabled = true;
                                checkBox1.Enabled = true;

                                checkBox7.Checked = true;
                                checkBox1.Checked = true;
                            }
                            else if (diffDate.Days == 1)
                            {
                                checkBox7.Enabled = true;

                                checkBox7.Checked = true;
                            }
                            break;
                        default:
                            checkBox7.Enabled = true;
                            checkBox7.Checked = true;
                            if (diffDate.Days == 5)
                            {
                                checkBox1.Enabled = true;
                                checkBox2.Enabled = true;
                                checkBox3.Enabled = true;
                                checkBox4.Enabled = true;
                                checkBox5.Enabled = true;

                                checkBox1.Checked = true;
                                checkBox2.Checked = true;
                                checkBox3.Checked = true;
                                checkBox4.Checked = true;
                                checkBox5.Checked = true;
                            }
                            else if (diffDate.Days == 4)
                            {
                                checkBox1.Enabled = true;
                                checkBox2.Enabled = true;
                                checkBox3.Enabled = true;
                                checkBox4.Enabled  = true;

                                checkBox1.Checked = true;
                                checkBox2.Checked = true;
                                checkBox3.Checked = true;
                                checkBox4.Checked = true;
                            }
                            else if (diffDate.Days == 3)
                            {
                                checkBox1.Enabled = true;
                                checkBox2.Enabled = true;
                                checkBox3.Enabled = true;

                                checkBox1.Checked = true;
                                checkBox2.Checked = true;
                                checkBox3.Checked = true;
                            }
                            else if (diffDate.Days == 2)
                            {
                                checkBox1.Enabled = true;
                                checkBox2.Enabled = true;

                                checkBox1.Checked = true;
                                checkBox2.Checked = true;
                            }
                            else if (diffDate.Days == 1)
                            {
                                checkBox1.Enabled = true;

                                checkBox1.Checked = true;
                            }

                            break;
                    }
                }
            }
            else
            {
                MessageBox.Show("Valid To must be greater than Valid From", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }

        }



        private void Checked(bool value)
        {
            checkBox1.Checked = value;
            checkBox2.Checked = value;
            checkBox3.Checked = value;
            checkBox4.Checked = value;
            checkBox5.Checked = value;
            checkBox6.Checked = value;
            checkBox7.Checked = value;
            checkBoxDaily.Checked = value;
        }


        private void Enabled(bool value)
        {
            checkBox1.Enabled = value;
            checkBox2.Enabled = value;
            checkBox3.Enabled = value;
            checkBox4.Enabled = value;
            checkBox5.Enabled = value;
            checkBox6.Enabled = value;
            checkBox7.Enabled = value;
            checkBoxDaily.Enabled = value;
        }

    }
}
