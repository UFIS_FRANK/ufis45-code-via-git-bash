using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using Ufis.Data;
using Ufis.Utils;

namespace FlightPermits
{
	public class rptDetail : ActiveReport3
	{
		private IRow omRow;
		public rptDetail(IRow opRow)
		{
			omRow = opRow;
			InitializeReport();
		}

		private void Detail_Format(object sender, System.EventArgs eArgs)
		{
			
		}

		#region ActiveReports Designer generated code
		private PageHeader PageHeader = null;
		private Label Title = null;
		private Detail Detail = null;
		private Label DOOP1 = null;
		private Shape Shape1 = null;
		private Label AHFP = null;
		private Shape Shape8 = null;
		private Label DOOP7 = null;
		private Label DOOP6 = null;
		private Label DOOP5 = null;
		private Label DOOP4 = null;
		private Label DOOP3 = null;
		private Label DOOP2 = null;
		private Shape Shape7 = null;
		private Shape Shape6 = null;
		private Shape Shape5 = null;
		private Shape Shape4 = null;
		private Shape Shape3 = null;
		private Shape Shape2 = null;
		private Label PERNtitle = null;
		private Label PERN = null;
		private Label ADID = null;
		private Label ALCOtitle = null;
		private Label FLNOtitle = null;
		private Label FLNO = null;
		private Label SUFXtitle = null;
		private Label SUFX = null;
		private Label REGNtitle = null;
		private Label REGN = null;
		private Label VAFRtitle = null;
		private Label VAFR = null;
		private Label VATOtitle = null;
		private Label VATO = null;
		private Label DOOPtitle = null;
		private Line Line1 = null;
		private Label ATDNtitle = null;
		private Label ATDN = null;
		private Label DTGN = null;
		private Label DTGNtitle = null;
		private Line Line2 = null;
		private Label MTOWtitle = null;
		private Label MTOW = null;
		private Label MTOWunits = null;
		private Label PERRtitle = null;
		private Label OBLRtitle = null;
		private Label USRRtitle = null;
		private Label USECtitle = null;
		private Label CDATtitle = null;
		private Label USEUtitle = null;
		private Label LSTUtitle = null;
		private Label ALCO = null;
		private Label DOOP1title = null;
		private Label DOOPtitle2 = null;
		private Label DOOPtitle3 = null;
		private Label DOOPtitle4 = null;
		private Label DOOPtitle5 = null;
		private Label DOOPtitle6 = null;
		private Label DOOPtitle7 = null;
		private Label USEC = null;
		private Label USEU = null;
		private Label CDAT = null;
		private Label LSTU = null;
		private Label AHFPtitle = null;
		private TextBox PERR2 = null;
		private TextBox OBLR2 = null;
		private TextBox USRR2 = null;
		private Line Line3 = null;
		private PageFooter PageFooter = null;
		private TextBox CreationDate = null;
		private TextBox CreationDateTitle = null;
		private TextBox CreatedBy = null;
		private TextBox CreatedByTitle = null;
		public void InitializeReport()
		{
			this.LoadLayout(this.GetType(), "FlightPermits.rptDetail.rpx");
			this.PageHeader = ((DataDynamics.ActiveReports.PageHeader)(this.Sections["PageHeader"]));
			this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
			this.PageFooter = ((DataDynamics.ActiveReports.PageFooter)(this.Sections["PageFooter"]));
			this.Title = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[0]));
			this.DOOP1 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[0]));
			this.Shape1 = ((DataDynamics.ActiveReports.Shape)(this.Detail.Controls[1]));
			this.AHFP = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[2]));
			this.Shape8 = ((DataDynamics.ActiveReports.Shape)(this.Detail.Controls[3]));
			this.DOOP7 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[4]));
			this.DOOP6 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[5]));
			this.DOOP5 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[6]));
			this.DOOP4 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[7]));
			this.DOOP3 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[8]));
			this.DOOP2 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[9]));
			this.Shape7 = ((DataDynamics.ActiveReports.Shape)(this.Detail.Controls[10]));
			this.Shape6 = ((DataDynamics.ActiveReports.Shape)(this.Detail.Controls[11]));
			this.Shape5 = ((DataDynamics.ActiveReports.Shape)(this.Detail.Controls[12]));
			this.Shape4 = ((DataDynamics.ActiveReports.Shape)(this.Detail.Controls[13]));
			this.Shape3 = ((DataDynamics.ActiveReports.Shape)(this.Detail.Controls[14]));
			this.Shape2 = ((DataDynamics.ActiveReports.Shape)(this.Detail.Controls[15]));
			this.PERNtitle = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[16]));
			this.PERN = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[17]));
			this.ADID = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[18]));
			this.ALCOtitle = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[19]));
			this.FLNOtitle = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[20]));
			this.FLNO = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[21]));
			this.SUFXtitle = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[22]));
			this.SUFX = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[23]));
			this.REGNtitle = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[24]));
			this.REGN = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[25]));
			this.VAFRtitle = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[26]));
			this.VAFR = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[27]));
			this.VATOtitle = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[28]));
			this.VATO = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[29]));
			this.DOOPtitle = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[30]));
			this.Line1 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[31]));
			this.ATDNtitle = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[32]));
			this.ATDN = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[33]));
			this.DTGN = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[34]));
			this.DTGNtitle = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[35]));
			this.Line2 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[36]));
			this.MTOWtitle = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[37]));
			this.MTOW = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[38]));
			this.MTOWunits = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[39]));
			this.PERRtitle = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[40]));
			this.OBLRtitle = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[41]));
			this.USRRtitle = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[42]));
			this.USECtitle = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[43]));
			this.CDATtitle = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[44]));
			this.USEUtitle = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[45]));
			this.LSTUtitle = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[46]));
			this.ALCO = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[47]));
			this.DOOP1title = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[48]));
			this.DOOPtitle2 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[49]));
			this.DOOPtitle3 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[50]));
			this.DOOPtitle4 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[51]));
			this.DOOPtitle5 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[52]));
			this.DOOPtitle6 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[53]));
			this.DOOPtitle7 = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[54]));
			this.USEC = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[55]));
			this.USEU = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[56]));
			this.CDAT = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[57]));
			this.LSTU = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[58]));
			this.AHFPtitle = ((DataDynamics.ActiveReports.Label)(this.Detail.Controls[59]));
			this.PERR2 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[60]));
			this.OBLR2 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[61]));
			this.USRR2 = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[62]));
			this.Line3 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[63]));
			this.CreationDate = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[0]));
			this.CreationDateTitle = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[1]));
			this.CreatedBy = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[2]));
			this.CreatedByTitle = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[3]));
			// Attach Report Events
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.ReportStart += new System.EventHandler(this.rptDetail_ReportStart);
		}

		#endregion

		private void rptDetail_ReportStart(object sender, EventArgs e)
		{
			if(omRow == null)
				return;

			DateTime olNow = DateTime.Now;
			CreationDate.Text = olNow.ToString("dd.MM.yyyy HH:mm");
			CreatedBy.Text = UT.UserName;

			AHFP.Text = omRow["AHFP"];
			DOOP7.Text = omRow["DOOP"].IndexOf("7") > -1 ? "X" : "";
			DOOP6.Text = omRow["DOOP"].IndexOf("6") > -1 ? "X" : "";
			DOOP5.Text = omRow["DOOP"].IndexOf("5") > -1 ? "X" : "";
			DOOP4.Text = omRow["DOOP"].IndexOf("4") > -1 ? "X" : "";
			DOOP3.Text = omRow["DOOP"].IndexOf("3") > -1 ? "X" : "";
			DOOP2.Text = omRow["DOOP"].IndexOf("2") > -1 ? "X" : "";
			DOOP1.Text = omRow["DOOP"].IndexOf("1") > -1 ? "X" : "";
			PERN.Text =  omRow["PERN"];
			ADID.Text =  (omRow["ADID"] == "A") ? "Arrival" : "Departure";
			FLNO.Text = omRow["FLNO"];
			SUFX.Text = omRow["SUFX"];
			REGN.Text =  omRow["REGN"];
			DateTime olVafr = UT.CedaFullDateToDateTime(omRow["VAFR"]);
			VAFR.Text =  olVafr.ToString("dd.MM.yyyy HH:mm");
			DateTime olVato = UT.CedaFullDateToDateTime(omRow["VATO"]);
			VATO.Text =  olVato.ToString("dd.MM.yyyy HH:mm");
			ATDN.Text =  omRow["ATDN"];
			DTGN.Text =  omRow["DTGN"];
			MTOW.Text =  omRow["MTOW"];
			PERR2.Text =  omRow["PERR"];
			OBLR2.Text =  omRow["OBLR"];
			USRR2.Text =  omRow["USRR"];
			ALCO.Text =  omRow["ALCO"];
			USEC.Text =  omRow["USEC"];
			USEU.Text =  omRow["USEU"];
			DateTime olCdat = UT.CedaFullDateToDateTime(omRow["CDAT"]);
			CDAT.Text =  olCdat.ToString("dd.MM.yyyy HH:mm");
			DateTime olLstu = UT.CedaFullDateToDateTime(omRow["LSTU"]);
			LSTU.Text =  olLstu.ToString("dd.MM.yyyy HH:mm");
		}
	}
}
