using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Utils;
using Ufis.Data;
namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmStatDelayReasonsForPVG.This class is created for the support of the Delay Reason for the PVG airport
	/// </summary>
	public class frmStatDelayReasonsForPVG : System.Windows.Forms.Form
	{
		#region _MyMembers 
		/// <summary>
		/// Summary description for Statistics of Delay Reasons.
		/// </summary>
		private string strWhereRawA =  "((STOA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))) OR (TIFA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))))" ;

		private string strWhereRawD =  "((STOD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('D','B'))) OR (TIFD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('D','B'))))" ;

		private string strWhereRawB = string.Format("({0} OR {1})","((STOA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))) OR (TIFA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))))","((STOD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('D','B'))) OR (TIFD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('D','B'))))");
		private string strLogicalFields = "DELCODE,DELRES,ALC,NOARRFL,NODEPFL";
		private string strWhereRawDEN = "WHERE (DECA = '@@DELCODE' OR DECN = '@@DELCODE')";
		/// <summary>
		/// The length of each column
		/// </summary>
		private string strTabHeaderLens = "80,150,60,130,130"; 
		private string strTabHeader = "Delay Code,Delay Reason,Airlines,No. of Arriv Flights,No. of Dept Flights";
		private IDatabase myDB;
		private ITable myAFT;
		private ITable myDEN;
		#endregion _MyMembers

		private UserControls.ucView ucView1;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Button buttonPrintPView;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Panel panelTab;
		private AxTABLib.AxTAB tabResult;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		/// <summary>
		/// 
		/// The constructor 
		/// </summary>
		public frmStatDelayReasonsForPVG()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			myDB = UT.GetMemDB();
			this.ucView1.Report = "StatisticsDelayReasons";
			this.ucView1.viewEvent +=new UserControls.ucView.ViewChangedEvent(ucView1_viewEvent);

			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmStatDelayReasonsForPVG));
			this.ucView1 = new UserControls.ucView();
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.buttonPrintPView = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.panelBody = new System.Windows.Forms.Panel();
			this.panelTab = new System.Windows.Forms.Panel();
			this.tabResult = new AxTABLib.AxTAB();
			this.lblResults = new System.Windows.Forms.Label();
			this.panelBody.SuspendLayout();
			this.panelTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			this.SuspendLayout();
			// 
			// ucView1
			// 
			this.ucView1.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.ucView1.Location = new System.Drawing.Point(8, 8);
			this.ucView1.Name = "ucView1";
			this.ucView1.Report = "";
			this.ucView1.Size = new System.Drawing.Size(304, 40);
			this.ucView1.TabIndex = 19;
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(336, 56);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 28;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(336, 79);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 27;
			this.progressBar1.Visible = false;
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(244, 80);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 26;
			this.btnCancel.Text = "&Close";
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(167, 80);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 25;
			this.btnLoadPrint.Text = "Loa&d + Print";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// buttonPrintPView
			// 
			this.buttonPrintPView.BackColor = System.Drawing.SystemColors.Control;
			this.buttonPrintPView.Location = new System.Drawing.Point(85, 80);
			this.buttonPrintPView.Name = "buttonPrintPView";
			this.buttonPrintPView.Size = new System.Drawing.Size(80, 23);
			this.buttonPrintPView.TabIndex = 24;
			this.buttonPrintPView.Text = "&Print Preview";
			this.buttonPrintPView.Click += new System.EventHandler(this.buttonPrintPView_Click);
			// 
			// btnOK
			// 
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnOK.Location = new System.Drawing.Point(8, 80);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 23;
			this.btnOK.Text = "&Load Data";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// panelBody
			// 
			this.panelBody.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.panelBody.Controls.Add(this.panelTab);
			this.panelBody.Controls.Add(this.lblResults);
			this.panelBody.Location = new System.Drawing.Point(0, 120);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(752, 400);
			this.panelBody.TabIndex = 29;
			// 
			// panelTab
			// 
			this.panelTab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.panelTab.Controls.Add(this.tabResult);
			this.panelTab.Location = new System.Drawing.Point(0, 16);
			this.panelTab.Name = "panelTab";
			this.panelTab.Size = new System.Drawing.Size(752, 384);
			this.panelTab.TabIndex = 4;
			// 
			// tabResult
			// 
			this.tabResult.ContainingControl = this;
			this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabResult.Location = new System.Drawing.Point(0, 0);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(752, 384);
			this.tabResult.TabIndex = 2;
			this.tabResult.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabResult_SendLButtonDblClick);
			// 
			// lblResults
			// 
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 0);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(752, 16);
			this.lblResults.TabIndex = 3;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// frmStatDelayReasonsForPVG
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.ClientSize = new System.Drawing.Size(752, 520);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.lblProgress);
			this.Controls.Add(this.progressBar1);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnLoadPrint);
			this.Controls.Add(this.buttonPrintPView);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.ucView1);
			this.Name = "frmStatDelayReasonsForPVG";
			this.Text = "Statistics of Delay Reasons...";
			this.Load += new System.EventHandler(this.frmStatDelayReasonsForPVG_Load);
			this.panelBody.ResumeLayout(false);
			this.panelTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void ucView1_viewEvent(string view)
		{
			btnOK_Click(this,null);
		}
		/// <summary>
		/// Event handler for the Load button.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnOK_Click(object sender, System.EventArgs e)
		{
			if(validEntry())
			{
				this.Cursor = Cursors.WaitCursor;
				this.InitTab();
				LoadReportData();
				PrepareReportData();
				this.Cursor = Cursors.Arrow;
			}
		}
		/// <summary>
		/// Validates the parameters that are set in the View page.
		/// </summary>
		/// <returns></returns>
		private bool validEntry() 
		{
			string strRet = "";
			int ilErrorCount = 1;

			if (this.ucView1.Viewer.CurrentView == "")
			{
				strRet += ilErrorCount.ToString() +  ". A view must be selected!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.PeriodFrom >= this.ucView1.Viewer.PeriodTo)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}
			if (this.ucView1.Viewer.Arrival == false && this.ucView1.Viewer.Departure == false)
			{
				strRet += ilErrorCount.ToString() +  ". Either Arrival or Departure must be used!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.Arrival == true && this.ucView1.Viewer.Departure == true)
			{
				strRet += ilErrorCount.ToString() +  ". Either Arrival or Departure must be used!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.Rotation == true)
			{
				strRet += ilErrorCount.ToString() +  ". Rotation must not be used!\n";
				ilErrorCount++;
			}
			string strQueryDelayCode = this.ucView1.Viewer.GeneralFilter;
			if((strQueryDelayCode.Length == 0) || ( strQueryDelayCode.IndexOf("DCD") == -1))
			{
				strRet += ilErrorCount.ToString() +  ". Delay Code must be selected in the Filter!\n";
				ilErrorCount++;
			}
			if (strRet.Length > 0)
			{
				MessageBox.Show(this, strRet);
				return false;
			}
			else
			{
				return true;
			}
		}
		/// <summary>
		/// Initializes the tabel to be displayed..
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;
			tabResult.HeaderString = strTabHeader;
			tabResult.LogicalFieldList = strLogicalFields;
			tabResult.HeaderLengthString = strTabHeaderLens;
			PrepareReportData();
		}

		private void frmStatDelayReasonsForPVG_Load(object sender, System.EventArgs e)
		{
			InitTab();
		}
		/// <summary>
		/// Reads all the necessary data from the table to be diplayed in the report.
		/// </summary>
		private void LoadReportData()
		{
			string strTmpQry;
			tabResult.ResetContent();
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			string strFtyp = this.ucView1.Viewer.FlightTypes;
			datFrom = this.ucView1.Viewer.PeriodFrom;
			datTo   = this.ucView1.Viewer.PeriodTo;
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(this.ucView1.Viewer.PeriodFrom);
				datTo   = UT.LocalToUtc(this.ucView1.Viewer.PeriodTo);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(this.ucView1.Viewer.PeriodFrom);
				strDateTo = UT.DateTimeToCeda(this.ucView1.Viewer.PeriodTo);

			}
			myDB.Unbind("AFT");
			myAFT = myDB.Bind("AFT", "AFT", "URNO,RKEY,FLNO,ADID,ORG3,DES3,STOA,STOD,TIFA,TIFD,ACT3,TTYP,ONBL,OFBL,REGN,NOSE,ALC2,ALC3,DCD1", 
				"10,10,12,1,3,3,14,14,14,14,3,5,14,14,12,3,2,3,2", 
				"URNO,RKEY,FLNO,ADID,ORG3,DES3,STOA,STOD,TIFA,TIFD,ACT3,TTYP,ONBL,OFBL,REGN,NOSE,ALC2,ALC3,DCD1");
			myAFT.Clear();
			myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,ONBL,OFBL";
			myAFT.Command("read",",GFR,");
			ArrayList myUniques = new ArrayList();
			myUniques.Add("URNO");
			myAFT.UniqueFields = myUniques;
			myAFT.TimeFieldsInitiallyInUtc = true;

			DateTime datReadFrom = datFrom;
			DateTime datReadTo ;

			TimeSpan tsDays = (datTo - datFrom);
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) 
				ilTotal = 1;
			int loopCnt = 1;
			progressBar1.Value = 0;
			lblProgress.Text = "Loading Flight Data";
			lblProgress.Refresh();
			progressBar1.Show();
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) 
					datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);

				strTmpQry = "WHERE ";

				// check on code share enabled
				if (this.ucView1.Viewer.IncludeCodeShare)
				{
					string strTmpWhere1 = "";
					//patch the where statement according to the user's entry
					if (this.ucView1.Viewer.Arrival && this.ucView1.Viewer.Departure)
						strTmpWhere1 = strWhereRawB;
					else if (this.ucView1.Viewer.Arrival)
						strTmpWhere1 = strWhereRawA;
					else if (this.ucView1.Viewer.Departure)
						strTmpWhere1 = strWhereRawD;
					strTmpWhere1+= this.ucView1.Viewer.AdditionalSelection(0);
					strTmpWhere1+= this.ucView1.Viewer.GeneralFilter;

					string strTmpWhere2 = "";
					//patch the where statement according to the user's entry
					strTmpWhere2+= this.ucView1.Viewer.AdditionalSelection(1);

					strTmpQry = string.Format("WHERE ({0}) AND ({1})",strTmpWhere1,strTmpWhere2);
				}
				else
				{
					//patch the where statement according to the user's entry
					if (this.ucView1.Viewer.Arrival && this.ucView1.Viewer.Departure)
						strTmpQry += strWhereRawB;
					else if (this.ucView1.Viewer.Arrival)
						strTmpQry += strWhereRawA;
					else if (this.ucView1.Viewer.Departure)
						strTmpQry += strWhereRawD;
					strTmpQry+= this.ucView1.Viewer.AdditionalSelection(0);
					strTmpQry+= this.ucView1.Viewer.GeneralFilter;

				}
				strTmpQry = strTmpQry.Replace("@@FROM",strDateFrom);
				strTmpQry = strTmpQry.Replace("@@TO",strDateTo);
				strTmpQry = strTmpQry.Replace("@@ORIG",UT.Hopo);
				strTmpQry = strTmpQry.Replace("@@DEST",UT.Hopo);
				if (this.ucView1.Viewer.Rotation)
				{
					strTmpQry += "[ROTATIONS]";
					if (strFtyp.Length > 0)
						strTmpQry += string.Format("[FILTER=FTYP IN ({0})]",strFtyp);
				}

				myAFT.Load(strTmpQry);
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			} while(datReadFrom <= datReadTo);
			lblProgress.Text = "";
			progressBar1.Hide();
			//Read the Delay Code reasons from DEN TAB
			string strQeryDelayCode = ucView1.Viewer.GeneralFilter;
			if(strQeryDelayCode.Length > 0)
			{
				string strDelayCode = "";
				int index = strQeryDelayCode.IndexOf("DCD1");
				if(index > -1)
					strDelayCode = strQeryDelayCode.Substring(index+10,2);
				strTmpQry = strWhereRawDEN ;
				strTmpQry = strTmpQry.Replace("@@DELCODE",strDelayCode);
				myDB.Unbind("DEN");
				myDEN = myDB.Bind("DEN","DEN","DECA,DECN,DENA","2,2,75","DECA,DECN,DENA");
				myDEN.Clear();
				myDEN.Load(strTmpQry);
			}
		}
		/// <summary>
		/// Prepares the Report Data for the display..
		/// </summary>
		private void PrepareReportData()
		{
			if (myAFT == null)
				return;
			string strDelayReason = "";
			string strOldAlc2Value = "";
			string strNewAlc2Value = "";
			int ilC=0;
			int ilArrivFlight = 0;
			int ilDeptFlight = 0;
			StringBuilder sb = new StringBuilder(10000);
			myAFT.Sort("ALC2",true);
			if(myDEN != null)
			{
				if(myDEN.Count > 0)
					strDelayReason = myDEN[0]["DENA"];
			}
			for(int i = 0; i < myAFT.Count; i++)
			{
				string strValues = ""; 
				ilC = i+1;
				strOldAlc2Value = myAFT[i]["ALC2"];
				if(ilC < myAFT.Count )
				{
					strNewAlc2Value = myAFT[ilC]["ALC2"];
				}
				if(strNewAlc2Value == strOldAlc2Value)
				{
					if(myAFT[i]["ADID"] == "A")
						ilArrivFlight++;
					else
						ilDeptFlight++;
				}
				else
				{
					strValues += myAFT[i]["DCD1"] + "," ;
					strValues += strDelayReason + "," ;
					strValues += strOldAlc2Value + ",";
					if(myAFT[i]["ADID"] == "A")
						ilArrivFlight++;
					else
						ilDeptFlight++;
					strValues += ilArrivFlight.ToString() + ",";
					strValues += ilDeptFlight.ToString() + "\n";
					ilArrivFlight = 0;
					ilDeptFlight = 0;
					sb.Append(strValues);
				}
			}
			tabResult.InsertBuffer(sb.ToString(), "\n");
			tabResult.Refresh();
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
		}
		/// <summary>
		/// Event handler for the support Print Preview button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void buttonPrintPView_Click(object sender, System.EventArgs e)
		{
			RunReport();
		}
		/// <summary>
		/// Initializes the Preview page with the header and footer ..
		/// </summary>
		private void RunReport() 
		{
			StringBuilder strSubHeader = new StringBuilder();

			//strSubHeader.Append("Delay Reasons Report with ");
			if (this.ucView1.Viewer.Arrival)
				strSubHeader.Append("Arrival ");
			else
				strSubHeader.Append("Departure ");

			strSubHeader.Append("From: ")
				.Append(this.ucView1.Viewer.PeriodFrom.ToString("dd.MM.yy'/'HH:mm"))
				.Append(" To: ")
				.Append(this.ucView1.Viewer.PeriodTo.ToString("dd.MM.yy'/'HH:mm"));
			strSubHeader.Append("View: ");
			strSubHeader.Append(this.ucView1.Viewer.CurrentView);
			strSubHeader.Append(" (Delay Reasons) ");

			//rptA3_Landscape rpt = new rptA3_Landscape(tabResult, "Statistics - Arrival / Departure", strSubHeader.ToString(), "", 8);
			prtFIPS_Landscape rpt = new prtFIPS_Landscape(tabResult,"Statistics - Delay Reasons",strSubHeader.ToString(),"",8);
			rpt.TextBoxCanGrow = false;
			frmPrintPreview frm = new frmPrintPreview(rpt);
			frm.Show();
		}
		/// <summary>
		/// Event Handler for the Load and Preview button.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			if(validEntry())
			{
				this.Cursor = Cursors.WaitCursor;
				this.InitTab();
				LoadReportData();
				PrepareReportData();
				RunReport();
				this.Cursor = Cursors.Arrow;
			}
		}
		/// <summary>
		/// Event handler for the double click of the LButton on the column header.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tabResult_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if( tabResult.SortOrderASC == true)
					{
						tabResult.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						tabResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabResult.Sort( e.colNo.ToString(), true, true);
				}
				tabResult.Refresh();
			}
		}
	}
}
