using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Ufis.Data;
using Ufis.Utils;
using System.Text;

namespace FIPS_Reports
{	
	public class frmCancelled_FlightDetail : System.Windows.Forms.Form
	{
		#region _My Members

		/// <summary>
		/// The where statement: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		private string strWhere = "WHERE (((STOA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('A','B'))) OR (STOD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('D','B')))) OR " +
			"((TIFA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('A','B'))) OR (TIFD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('D','B'))))) AND FTYP='X'" ;
		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes
		/// </summary>
		private string strLogicalFields = "DATE,ADID,ARRIVAL,DEPARTURE";
		private string stractLogicalFields = "DATE,STOA,STOD";
		/// <summary>
		/// The header columns, which must be used for the report output
		/// </summary>
		private string strTabHeader =     "Flight Date  ,A/D  ,Arrival  ,Departure  ";
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// </summary>
		private string stractTabHeader =     "Date  ,No.of Arr Flights  ,No.of Dep Flights ";
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// </summary>
		private string stractTabHeaderLens = "100,120,120";
		private string strTabHeaderLens = "70,30,80,70";
		/// <summary>
		/// String for empty line generation in the tab control.
		/// </summary>
		private string strEmptyLine = ",,,,,";
		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
		/// <summary>
		/// Total of arrival flights in the report
		/// </summary>
		/// private int iTotalcountArr = 0;
		private int iprevcountArr = 0;
		private int icurrcountArr = 0;
		private int iarrv= 0;
		private int idep = 0;
		private int testarrv=0;
		private int testdep=0;
		/// <summary>
		/// Total of dep flights in the report on a day
		/// </summary>
		private int iTotalcountDep = 0;
		private int iprevcountDep = 0;
		private int icurrcountDep = 0;
		private int imArrivals = 0;
		private double [] NumberofFlights;
		
		private string strValues;
		private string currDate;
		private string prevDate;
		/// <summary>
		/// Total of departure flights in the report
		/// </summary>
		private int imDepartures = 0;
		/// <summary>
		/// Total of rotationein the report
		/// </summary>
		private int imRotations = 0;
		/// <summary>
		/// Total of all flights in the report
		/// </summary>
		private double  imTotalFlights = 0;
		/// <summary>
		/// Boolean for mouse status with respect to tabResult control
		/// </summary>
		private bool bmMouseInsideTabControl = false;
		/// <summary>
		/// Report Header
		/// </summary>
		private string strReportHeader = "";
		/// <summary>
		/// Report Sub Header
		/// </summary>
		private string strReportSubHeader = "";
		/// <summary>
		/// Report Object
		/// </summary>
		private DataDynamics.ActiveReports.ActiveReport3 activeReport = null;

		#endregion _My Members
		/// <summary>
		/// Summary description for frmCancelled_FlightDetail.
		/// </summary>

		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Button btnPrintPreview;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.DateTimePicker dtTo;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label lblResults;
		private AxTABLib.AxTAB tabResult;
		private AxTABLib.AxTAB actualtabResult;
		private System.Windows.Forms.Button buttonHelp;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmCancelled_FlightDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeMouseEvents();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmCancelled_FlightDetail));
			this.panelTop = new System.Windows.Forms.Panel();
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.btnPrintPreview = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.dtTo = new System.Windows.Forms.DateTimePicker();
			this.label2 = new System.Windows.Forms.Label();
			this.dtFrom = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.lblResults = new System.Windows.Forms.Label();
			this.tabResult = new AxTABLib.AxTAB();
			this.actualtabResult = new AxTABLib.AxTAB();
			this.buttonHelp = new System.Windows.Forms.Button();
			this.panelTop.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.actualtabResult)).BeginInit();
			this.SuspendLayout();
			// 
			// panelTop
			// 
			this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelTop.Controls.Add(this.buttonHelp);
			this.panelTop.Controls.Add(this.lblProgress);
			this.panelTop.Controls.Add(this.progressBar1);
			this.panelTop.Controls.Add(this.btnLoadPrint);
			this.panelTop.Controls.Add(this.btnPrintPreview);
			this.panelTop.Controls.Add(this.btnCancel);
			this.panelTop.Controls.Add(this.btnOK);
			this.panelTop.Controls.Add(this.dtTo);
			this.panelTop.Controls.Add(this.label2);
			this.panelTop.Controls.Add(this.dtFrom);
			this.panelTop.Controls.Add(this.label1);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(736, 112);
			this.panelTop.TabIndex = 1;
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(496, 56);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 22;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(496, 72);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 6;
			this.progressBar1.Visible = false;
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(244, 72);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 4;
			this.btnLoadPrint.Text = "Loa&d + Print";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// btnPrintPreview
			// 
			this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPrintPreview.Location = new System.Drawing.Point(168, 72);
			this.btnPrintPreview.Name = "btnPrintPreview";
			this.btnPrintPreview.TabIndex = 3;
			this.btnPrintPreview.Text = "&Print Preview";
			this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(320, 72);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 5;
			this.btnCancel.Text = "&Close";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnOK
			// 
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnOK.Location = new System.Drawing.Point(92, 72);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 2;
			this.btnOK.Text = "&Load Data";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// dtTo
			// 
			this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtTo.Location = new System.Drawing.Point(268, 4);
			this.dtTo.Name = "dtTo";
			this.dtTo.Size = new System.Drawing.Size(128, 20);
			this.dtTo.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(232, 8);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(24, 16);
			this.label2.TabIndex = 2;
			this.label2.Text = "to:";
			// 
			// dtFrom
			// 
			this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtFrom.Location = new System.Drawing.Point(92, 4);
			this.dtFrom.Name = "dtFrom";
			this.dtFrom.Size = new System.Drawing.Size(128, 20);
			this.dtFrom.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Date from:";
			// 
			// lblResults
			// 
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 112);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(736, 16);
			this.lblResults.TabIndex = 2;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// tabResult
			// 
			this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabResult.Location = new System.Drawing.Point(0, 128);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(736, 253);
			this.tabResult.TabIndex = 3;
			// 
			// actualtabResult
			// 
			this.actualtabResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.actualtabResult.Location = new System.Drawing.Point(0, 128);
			this.actualtabResult.Name = "actualtabResult";
			this.actualtabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("actualtabResult.OcxState")));
			this.actualtabResult.Size = new System.Drawing.Size(736, 253);
			this.actualtabResult.TabIndex = 4;
			this.actualtabResult.SendLButtonClick += new AxTABLib._DTABEvents_SendLButtonClickEventHandler(this.actualtabResult_SendLButtonClick);
			// 
			// buttonHelp
			// 
			this.buttonHelp.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonHelp.Location = new System.Drawing.Point(396, 72);
			this.buttonHelp.Name = "buttonHelp";
			this.buttonHelp.TabIndex = 30;
			this.buttonHelp.Text = "Help";
			this.buttonHelp.Click += new System.EventHandler(this.buttonHelp_Click);
			// 
			// frmCancelled_FlightDetail
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(736, 381);
			this.Controls.Add(this.actualtabResult);
			this.Controls.Add(this.tabResult);
			this.Controls.Add(this.lblResults);
			this.Controls.Add(this.panelTop);
			this.Name = "frmCancelled_FlightDetail";
			this.Text = "Cancelled Flights (Summary)";
			this.Load += new System.EventHandler(this.frmCancelled_FlightDetail_Load);
			this.HelpRequested += new HelpEventHandler(frmCancelled_FlightDetail_HelpRequested);
			this.panelTop.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.actualtabResult)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void frmCancelled_FlightDetail_Load(object sender, System.EventArgs e)
		{
			myDB = UT.GetMemDB();
			InitTimePickers();
			InitTab();
			PrepareFilterData();
			NumberofFlights= new double[4];
			NumberofFlights.Initialize();
		}
		/// <summary>
		/// initializes the From/to time control with the current day in
		/// the custom format "dd.MM.yyyy - HH:mm".
		/// </summary>
		private void InitTimePickers()
		{
			DateTime olFrom;
			DateTime olTo;
			
			olFrom = DateTime.Now;
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olTo   = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23,59,0);
			dtFrom.Value = olFrom;
			dtTo.Value = olTo;
			dtFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtTo.CustomFormat = "dd.MM.yyyy - HH:mm";
		}
		/// <summary>
		/// Initializes the tab controll.
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;
			tabResult.HeaderString = strTabHeader;
			tabResult.LogicalFieldList = strLogicalFields;
			tabResult.HeaderLengthString = strTabHeaderLens;

			actualtabResult.ResetContent();
			actualtabResult.ShowHorzScroller(true);
			actualtabResult.EnableHeaderSizing(true);
			actualtabResult.SetTabFontBold(true);
			actualtabResult.LifeStyle = true;
			actualtabResult.LineHeight = 16;
			actualtabResult.FontName = "Arial";
			actualtabResult.FontSize = 14;
			actualtabResult.HeaderFontSize = 12;
			actualtabResult.AutoSizeByHeader = true;
			actualtabResult.HeaderString = stractTabHeader;
			actualtabResult.LogicalFieldList = stractLogicalFields;
			actualtabResult.HeaderLengthString = stractTabHeaderLens;
		}
		/// <summary>
		/// Loads and prepare the data necessary for the filter. 
		/// In this case loads the nature codes and puts the
		/// data into the combobox.
		/// </summary>
		private void PrepareFilterData()
		{
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			tabResult.ResetContent ();
			actualtabResult.ResetContent ();
			LoadReportData();
			PrepareReportCount();
			PrepareFillReportData();
		//	tabResult.ResetContent ();
		//	actualtabResult.ResetContent ();
		//	PrepareReportData();
			//RunReport();
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// Fills Report
		/// </summary>
		private void PrepareFillReportData()
		{
			
		//	actualtabResult.InsertBuffer(sb.ToString(), "\n");
			actualtabResult.Sort ("0",true,true);
		//	insertTotal();
			lblProgress.Text = "";
			progressBar1.Visible = false;
			actualtabResult.Refresh();
			tabResult.Visible =false;
			actualtabResult.Visible =true;
			lblResults.Text = "Report Results: (" + actualtabResult.GetLineCount().ToString() + ")";
		}
		/// <summary>
		/// Loads all necessary tables with filter criteria from the 
		/// database ==> to memoryDB.
		/// </summary>
		private void LoadReportData()
		{
			string strTmpAFTWhere = strWhere;
			//Clear the tab
			tabResult.ResetContent();

			// In the first step change the times to UTC if this is
			// necessary.
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			datFrom = dtFrom.Value;
			datTo   = dtTo.Value;
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(dtFrom.Value);
				datTo   = UT.LocalToUtc(dtTo.Value);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
				strDateTo = UT.DateTimeToCeda(dtTo.Value);
			}
			//Load the data from AFTTAB
			myDB.Unbind("AFT");
			myAFT = myDB.Bind("AFT", "AFT", "URNO,FLNO,ADID,ORG3,DES3,STOA,STOD,TIFA,TIFD,ACT3", "10,12,2,3,3,14,14,14,14,3", "URNO,FLNO,ADID,ORG3,DES3,STOA,STOD,TIFA,TIFD,ACT3");
			myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,ONBL,OFBL";
			myAFT.Clear();
			ArrayList myUniques = new ArrayList();
			myUniques.Add("URNO");
			myAFT.UniqueFields = myUniques;
			myAFT.TimeFieldsInitiallyInUtc = true;
			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);
			progressBar1.Value = 0;
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			lblProgress.Text = "Loading Flight Data";
			lblProgress.Refresh();
			progressBar1.Show();
			int loopCnt = 1;
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				//patch the where statement according to the user's entry
				strTmpAFTWhere = strWhere;
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@FROM", strDateFrom);
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@TO", strDateTo );

				//Load the data from AFTTAB
				myAFT.Load(strTmpAFTWhere);

				// Added to use new logic -------------------------------------
				//NumberofFlights= new double[4];
				myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
				PrepareReportData();
				//	PrepareReportCount();
				tabResult.Visible = false;
				myAFT.Clear();
				// Ends here ----------------------------------
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
			lblProgress.Text = "";
			progressBar1.Hide();
	//		myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
		}
		/// <summary>
		/// Prepare the loaded data according to the report's needs.
		/// Prepare line by line the data a logical records and put
		/// the data into the Tab control.
		/// </summary>
		private void PrepareReportCount()
		{
			StringBuilder sb = new StringBuilder(10000);
			for (int k=0; k < tabResult .GetLineCount (); k++)
			{
				currDate = tabResult .GetColumnValue(k,0);
				if (k==0 )
				{
					//	prevAirline = currAirline ;
					prevDate = currDate;
				}
				if (prevDate == currDate)
				{
					//Start with the count of Arrival flights
					if (tabResult .GetColumnValue(k,1) == "A")
					{
						NumberofFlights[0]++;
						NumberofFlights[2]++;
						prevDate = currDate;
						if (k ==tabResult .GetLineCount() -1)
						{
							strValues += currDate + "," + NumberofFlights[0].ToString() +"," +NumberofFlights[1].ToString() +" \n" ;
							sb.Append(strValues);
							strValues = "";
						}
					}
					if (tabResult .GetColumnValue(k,1) == "D")
					{
						NumberofFlights[1]++;
						NumberofFlights[3]++;
						prevDate = currDate;
						if (k ==tabResult .GetLineCount() -1)
						{
							strValues += currDate + "," +NumberofFlights[0].ToString() +"," +NumberofFlights[1].ToString()+" \n" ;
							sb.Append(strValues);
							strValues = "";
						}
					}
				}
				if (prevDate != currDate)
				{
					strValues = "";
					strValues += prevDate + "," +NumberofFlights[0].ToString() +"," +NumberofFlights[1].ToString()+" \n" ;
					sb.Append(strValues);
					NumberofFlights[0]=0;
					NumberofFlights[1]=0;
					strValues = "";
					//Start with the count of Arrival flights
					if (tabResult .GetColumnValue(k,1) == "A")
					{
						NumberofFlights[0]++;
						NumberofFlights[2]++;
						prevDate = currDate;

						if (k ==tabResult .GetLineCount() -1)
						{
							strValues += currDate + "," + NumberofFlights[0].ToString() +"," +NumberofFlights[1] +" \n" ;
							sb.Append(strValues);
							strValues = "";
						}
					}
					if (tabResult .GetColumnValue(k,1) == "D")
					{
						NumberofFlights[1]++;
						NumberofFlights[3]++;
						prevDate = currDate;

						if (k ==tabResult .GetLineCount() -1)
						{
							strValues += currDate + "," +NumberofFlights[0].ToString() +"," +NumberofFlights[1].ToString()+" \n" ;
							sb.Append(strValues);
							strValues = "";
						}
					}
				}
			}
			actualtabResult.InsertBuffer(sb.ToString(), "\n");
		}
		/// <summary>
		/// Prepare the loaded data according to the report's needs.
		/// Prepare line by line the data a logical records and put
		/// the data into the Tab control.
		/// </summary>
		private void PrepareReportData()
		{
			int ilLine = 0;
			//"FLNO,ADID,ORGDES,SCHEDULE,ACTUAL,ACTYPE"
			if(myAFT == null) 
				return;
			string strSchedule = "";
			lblProgress.Text = "Preparing Data";
			lblProgress.Refresh();
			progressBar1.Visible = true;
			progressBar1.Value = 0;
			int ilTotal = myAFT.Count;
			if(ilTotal == 0) ilTotal = 1;
			imArrivals = 0;
			imDepartures =0 ;
			imTotalFlights = 0;
		//	StringBuilder sb = new StringBuilder(10000);
			
			for(int i = 0; i < myAFT.Count; i++)
			{
				string strValues = "";
				string strAdid = myAFT[i]["ADID"];
				
				if( i % 100 == 0)
				{
					int percent = Convert.ToInt32((i * 100)/ilTotal);
					if (percent > 100) 
						percent = 100;
					progressBar1.Value = percent;
				}
				if(strAdid == "A")
				{
					//imTotalFlights++;
					string strSTOA_showdate = Helper.DateString(myAFT[i]["STOA"],"yyyyMMdd");
					tabResult.InsertTextLine(strEmptyLine, false);
					ilLine = tabResult.GetLineCount() - 1;
					tabResult.SetFieldValues(ilLine, "DATE", strSTOA_showdate);
					tabResult.SetFieldValues(ilLine, "ADID", strAdid);
					tabResult.SetFieldValues(ilLine, "ARRIVAL", "1");
					tabResult.SetFieldValues(ilLine, "DEPARTURE", "0");
				}
				if(strAdid == "D")
				{
					//imTotalFlights++;
					string strSTOD_showdate = Helper.DateString(myAFT[i]["STOD"],"yyyyMMdd");
					imTotalFlights++;
					tabResult.InsertTextLine(strEmptyLine, false);
					ilLine = tabResult.GetLineCount() - 1;
					tabResult.SetFieldValues(ilLine, "DATE", strSTOD_showdate);
					tabResult.SetFieldValues(ilLine, "ADID", strAdid);
					tabResult.SetFieldValues(ilLine, "ARRIVAL", "0");
					tabResult.SetFieldValues(ilLine, "DEPARTURE", "1");
				}
			}
			tabResult.Refresh();
			tabResult.Visible =false;
			tabResult.Sort("1",true,true);
			tabResult.Sort("0",true,true);
			actualtabResult.Visible =true;
			actualtabResult.Refresh();
	//		prepareActualReportdata();
		}
		/// <summary>
		/// Counts the Arrival flights
		/// </summary>
		private void prepareActualReportdata()
		{
			string currDateVal="";
			string prevDateVal = "";
			string currArrcellValue="";
			string currDepValue= "";
			bool firsttime = false;
			StringBuilder sb1 = new StringBuilder(10000);			
			int Linecount = tabResult.GetLineCount();
			for (int k=0; k <Linecount ; k++)
			{
				string strValuesact = "";
				currDateVal = tabResult.GetColumnValue(k,0);
				currArrcellValue =tabResult.GetColumnValue(k,2);
				currDepValue =tabResult.GetColumnValue(k,3);
				if (k==0)
				{
					prevDateVal=currDateVal;
					firsttime = true;
				}
				if ((prevDateVal==currDateVal))
				{
					if((currArrcellValue =="0") && (currDepValue =="1"))
					{
						imDepartures++;
					}
					
					if ( (currArrcellValue =="1") && (currDepValue =="0"))
					{
						imArrivals++;
					}
					if(k==Linecount-1)
					{
						strValuesact="";
						testarrv = imArrivals;
						testdep = imDepartures;
						idep=imDepartures-iprevcountDep;
						iarrv=imArrivals-iprevcountArr;
						iprevcountDep= testdep;
						iprevcountArr = testarrv;
						strValuesact += currDateVal + ",";
						strValuesact += iarrv.ToString() + ",";
						strValuesact += idep.ToString() + "\n";
						sb1.Append(strValuesact);
					}
				}
				if ((prevDateVal!=currDateVal))
				{
					if (firsttime)
					{
						iprevcountDep=imDepartures;
						iprevcountArr=imArrivals;
						iarrv=iprevcountArr;
						idep = iprevcountDep;
						firsttime = false;		
					}
					else
					{
						testarrv = imArrivals;
						testdep = imDepartures;
						idep=imDepartures-iprevcountDep;
						iarrv=imArrivals-iprevcountArr;
						iprevcountDep= testdep;
						iprevcountArr = testarrv;						
					}					
					strValuesact += prevDateVal + ",";
					strValuesact += iarrv.ToString() + ",";
					strValuesact += idep.ToString() + "\n";
					sb1.Append(strValuesact);
					prevDateVal="";									
					
					if((currArrcellValue =="0") && (currDepValue =="1"))
					{
						imDepartures++;
						prevDateVal=currDateVal;
					}					
					if ( (currArrcellValue =="1") && (currDepValue =="0"))
					{
						imArrivals++;
						prevDateVal=currDateVal;
					}
					if(k==Linecount-1)
					{
						strValuesact="";
						testarrv = imArrivals;
						testdep = imDepartures;
						idep=imDepartures-iprevcountDep;
						iarrv=imArrivals-iprevcountArr;
						iprevcountDep= testdep;
						iprevcountArr = testarrv;
						strValuesact += currDateVal + ",";
						strValuesact += iarrv.ToString() + ",";
						strValuesact += idep.ToString() + "\n";
						sb1.Append(strValuesact);
					}
				}				
			}
			actualtabResult .InsertBuffer(sb1.ToString(), "\n");
			lblProgress.Text = "";
			progressBar1.Visible = false;
			actualtabResult.Refresh();
			tabResult.Visible =true;
			actualtabResult.Visible =false;
			lblResults.Text = "Report Results: (" + actualtabResult.GetLineCount().ToString() + ")";
		}
		/// <summary>
		/// Validates the user entry for nonsense.
		/// </summary>
		/// <returns></returns>
		private string ValidateUserEntry()
		{
			string strRet = "";
			int ilErrorCount = 1;
			if(dtFrom.Value >= dtTo.Value)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}
			return strRet;
		}

		private void actualtabResult_SendLButtonClick(object sender, AxTABLib._DTABEvents_SendLButtonClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == actualtabResult.CurrentSortColumn)
				{
					if( actualtabResult.SortOrderASC == true)
					{
						actualtabResult.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						actualtabResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					actualtabResult.Sort( e.colNo.ToString(), true, true);
				}
				actualtabResult.Refresh();
			}
		}

		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			tabResult.ResetContent ();
			actualtabResult.ResetContent ();
			LoadReportData();
			PrepareFillReportData();
			//	PrepareReportData();
			RunReport();
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// Calls the generic report. The contructor must have the Tab 
		/// control and the reports headers as well as the types for
		/// each column to enable the reports to transform data, e.g.
		/// the data/time formatting. The lengths of the columns are
		/// dynamically read out of the tab control's header.
		/// </summary>
		private void RunReport()
		{
			string strSubHeader = "";
			imTotalFlights = NumberofFlights[2] + NumberofFlights[3];
			strSubHeader = "From: " + dtFrom.Value.ToString("dd.MM.yy'/'HH:mm") + " to " + 
				dtTo.Value.ToString("dd.MM.yy'/'HH:mm");
			strSubHeader += "       (Flights: " + imTotalFlights.ToString();
			strSubHeader += " "+" ARR: " + NumberofFlights[2].ToString();
			strSubHeader += " "+" DEP: " + NumberofFlights[3].ToString() + ")";
			strReportHeader = "Cancelled Flights (Summary): ";
			strReportSubHeader = strSubHeader;
			rptFIPS rpt = new rptFIPS(actualtabResult , strReportHeader, strSubHeader, "", 10);
			frmPrintPreview frm = new frmPrintPreview(rpt);
			if(Helper.UseSpreadBuilder() == true)
			{
				activeReport = rpt;
				frm.UseSpreadBuilder(true);
				frm.GetBtnExcelExport().Click += new EventHandler(frmCancelled_FlightDetail_Click);
			}
			else
			{
				frm.UseSpreadBuilder(false);
			}
			frm.Show();
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			myDB.Unbind("AFT");
			this.Close ();
		}

		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			RunReport();
		}

		private void InitializeMouseEvents()
		{
			((System.Windows.Forms.Control)actualtabResult).MouseWheel += new MouseEventHandler(frmCancelled_FlightDetail_MouseWheel);
			((System.Windows.Forms.Control)actualtabResult).MouseEnter += new EventHandler(frmCancelled_FlightDetail_MouseEnter);
			((System.Windows.Forms.Control)actualtabResult).MouseLeave += new EventHandler(frmCancelled_FlightDetail_MouseLeave);
		}

		private void frmCancelled_FlightDetail_MouseWheel(object sender, MouseEventArgs e)
		{
			if(bmMouseInsideTabControl == false || Helper.CheckScrollingStatus(actualtabResult) == false)
			{
				return;
			}
			if(e.Delta > 0 && actualtabResult.GetVScrollPos() > 0)
			{				
				actualtabResult.OnVScrollTo(actualtabResult.GetVScrollPos() - 1);
			}
			else if(e.Delta < 0 && (actualtabResult.GetVScrollPos() + 1) < actualtabResult.GetLineCount())
			{
				actualtabResult.OnVScrollTo(actualtabResult.GetVScrollPos() + 1);
			}
		}

		private void frmCancelled_FlightDetail_MouseEnter(object sender, EventArgs e)
		{			
			bmMouseInsideTabControl = true;
		}

		private void frmCancelled_FlightDetail_MouseLeave(object sender, EventArgs e)
		{
			bmMouseInsideTabControl = false;
		}

		private void frmCancelled_FlightDetail_Click(object sender, EventArgs e)
		{
			Helper.ExportToExcel(actualtabResult,strReportHeader,strReportSubHeader,stractTabHeaderLens,10,0,activeReport);
		}

		private void buttonHelp_Click(object sender, System.EventArgs e)
		{
			Helper.ShowHelpFile(this);
		}

		private void frmCancelled_FlightDetail_HelpRequested(object sender, HelpEventArgs hlpevent)
		{
			Helper.ShowHelpFile(this);
		}
	}
}
