using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Ufis.Utils;
using Ufis.Data;
using System.Text;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmSeat. This class is created for supporting the Seat Statistics 
	/// for the PVG airport.  This is created during solving the PRF 8523.
	/// </summary>
	public class frmSeatStatisticsAccDomIntForPVG : System.Windows.Forms.Form
	{
		#region _My Members
		private string strWhereRawA =  "((STOA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))) OR (TIFA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))))" ;
		private string strWhereRawD =  "((STOD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('D','B'))) OR (TIFD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('D','B'))))" ;
		private string strWhereRawB = string.Format("({0} OR {1})","((STOA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))) OR (TIFA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))))","((STOD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('D','B'))) OR (TIFD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('D','B'))))");
		//private string strWhereRaw =	"WHERE((TIFD BETWEEN '@@FROM' AND '@@TO') AND (ORG3 = '@@HOPO') AND (FTYP = 'O' OR FTYP = 'S'))";
		private string strLogicalFieldsAirlines = "ALC2,NOSE,FLTPERALT";
		private string strTabHeaderAirLines =     "Airline  ,Seats  ,Flights per Airline";
		private string strTabHeaderLensAirLines = "80,80,140";
		string strLogicalFieldsDest = "DES3,NOSE,FLTPERDST";
		private string strTabHeaderDestination = "Destination ,Seats ,Flights per Destination";
		string strLogicalFieldsAlcDest = "ALC2,DES3,NOSE,FLTPERDST,FLTPERALT";
		private string strTabHeaderAirLineAndDest = "AirLine ,Destination ,Seats ,Flights per Destination ,Flights per Airline";
		private string strTabHeaderLensAirLineDest = "80,80,80,130,160";
		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;

		#endregion _My Members

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.RadioButton radioButtonAirLineDest;
		private System.Windows.Forms.RadioButton radioButtonDest;
		private System.Windows.Forms.RadioButton radioButtonAirLine;
		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Panel panel1;
		private AxTABLib.AxTAB tabResult;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Button buttonPrintPView;
		private System.Windows.Forms.Button btnLoad;
		private UserControls.ucView ucView1;
		private System.Windows.Forms.Button btnCancel;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		/// <summary>
		/// constructor 
		/// </summary>
		public frmSeatStatisticsAccDomIntForPVG()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			radioButtonAirLine.Checked = true;
			//
			myDB = UT.GetMemDB();
			this.ucView1.Report = "Seats Statistics";
			this.ucView1.viewEvent +=new UserControls.ucView.ViewChangedEvent(ucView1_viewEvent);
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmSeatStatisticsAccDomIntForPVG));
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.radioButtonAirLineDest = new System.Windows.Forms.RadioButton();
			this.radioButtonDest = new System.Windows.Forms.RadioButton();
			this.radioButtonAirLine = new System.Windows.Forms.RadioButton();
			this.panelBody = new System.Windows.Forms.Panel();
			this.panel1 = new System.Windows.Forms.Panel();
			this.tabResult = new AxTABLib.AxTAB();
			this.lblResults = new System.Windows.Forms.Label();
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.buttonPrintPView = new System.Windows.Forms.Button();
			this.btnLoad = new System.Windows.Forms.Button();
			this.ucView1 = new UserControls.ucView();
			this.btnCancel = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			this.panelBody.SuspendLayout();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.BackColor = System.Drawing.Color.Transparent;
			this.groupBox1.Controls.Add(this.radioButtonAirLineDest);
			this.groupBox1.Controls.Add(this.radioButtonDest);
			this.groupBox1.Controls.Add(this.radioButtonAirLine);
			this.groupBox1.Location = new System.Drawing.Point(16, 56);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(544, 48);
			this.groupBox1.TabIndex = 20;
			this.groupBox1.TabStop = false;
			// 
			// radioButtonAirLineDest
			// 
			this.radioButtonAirLineDest.Location = new System.Drawing.Point(300, 16);
			this.radioButtonAirLineDest.Name = "radioButtonAirLineDest";
			this.radioButtonAirLineDest.Size = new System.Drawing.Size(184, 24);
			this.radioButtonAirLineDest.TabIndex = 9;
			this.radioButtonAirLineDest.Text = "AirLine and Destination";
			this.radioButtonAirLineDest.CheckedChanged += new System.EventHandler(this.radioButtonAirLineDest_CheckedChanged_1);
			// 
			// radioButtonDest
			// 
			this.radioButtonDest.Location = new System.Drawing.Point(144, 16);
			this.radioButtonDest.Name = "radioButtonDest";
			this.radioButtonDest.TabIndex = 8;
			this.radioButtonDest.Text = "Destination";
			this.radioButtonDest.CheckedChanged += new System.EventHandler(this.radioButtonDest_CheckedChanged_1);
			// 
			// radioButtonAirLine
			// 
			this.radioButtonAirLine.Location = new System.Drawing.Point(16, 16);
			this.radioButtonAirLine.Name = "radioButtonAirLine";
			this.radioButtonAirLine.Size = new System.Drawing.Size(92, 24);
			this.radioButtonAirLine.TabIndex = 7;
			this.radioButtonAirLine.Text = "AirLine";
			this.radioButtonAirLine.CheckedChanged += new System.EventHandler(this.radioButtonAirLine_CheckedChanged_1);
			// 
			// panelBody
			// 
			this.panelBody.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.panelBody.Controls.Add(this.panel1);
			this.panelBody.Controls.Add(this.lblResults);
			this.panelBody.Location = new System.Drawing.Point(0, 168);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(944, 352);
			this.panelBody.TabIndex = 21;
			// 
			// panel1
			// 
			this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.panel1.Controls.Add(this.tabResult);
			this.panel1.Location = new System.Drawing.Point(0, 24);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(944, 328);
			this.panel1.TabIndex = 5;
			// 
			// tabResult
			// 
			this.tabResult.ContainingControl = this;
			this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabResult.Location = new System.Drawing.Point(0, 0);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(944, 328);
			this.tabResult.TabIndex = 2;
			this.tabResult.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabResult_SendLButtonDblClick);
			// 
			// lblResults
			// 
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 0);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(944, 16);
			this.lblResults.TabIndex = 4;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(360, 112);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 29;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(368, 136);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 28;
			this.progressBar1.Visible = false;
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(175, 136);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 26;
			this.btnLoadPrint.Text = "Loa&d + Print";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// buttonPrintPView
			// 
			this.buttonPrintPView.BackColor = System.Drawing.SystemColors.Control;
			this.buttonPrintPView.Location = new System.Drawing.Point(93, 136);
			this.buttonPrintPView.Name = "buttonPrintPView";
			this.buttonPrintPView.Size = new System.Drawing.Size(80, 23);
			this.buttonPrintPView.TabIndex = 25;
			this.buttonPrintPView.Text = "&Print Preview";
			this.buttonPrintPView.Click += new System.EventHandler(this.buttonPrintPView_Click);
			// 
			// btnLoad
			// 
			this.btnLoad.BackColor = System.Drawing.SystemColors.Control;
			this.btnLoad.Location = new System.Drawing.Point(16, 136);
			this.btnLoad.Name = "btnLoad";
			this.btnLoad.TabIndex = 24;
			this.btnLoad.Text = "Load";
			this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
			// 
			// ucView1
			// 
			this.ucView1.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.ucView1.Location = new System.Drawing.Point(24, 8);
			this.ucView1.Name = "ucView1";
			this.ucView1.Report = "";
			this.ucView1.Size = new System.Drawing.Size(304, 40);
			this.ucView1.TabIndex = 30;
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(252, 136);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 34;
			this.btnCancel.Text = "&Close";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click_1);
			// 
			// frmSeatStatisticsAccDomIntForPVG
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.ClientSize = new System.Drawing.Size(947, 520);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.ucView1);
			this.Controls.Add(this.lblProgress);
			this.Controls.Add(this.progressBar1);
			this.Controls.Add(this.btnLoadPrint);
			this.Controls.Add(this.buttonPrintPView);
			this.Controls.Add(this.btnLoad);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.groupBox1);
			this.Name = "frmSeatStatisticsAccDomIntForPVG";
			this.Text = "Seats Statistics...";
			this.Load += new System.EventHandler(this.frmSeatStatisticsAccDomIntForPVG_Load);
			this.groupBox1.ResumeLayout(false);
			this.panelBody.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void btnLoad_Click(object sender, System.EventArgs e)
		{
			if(validEntry())
			{
				this.Cursor = Cursors.WaitCursor;
				this.InitTab();
				LoadReportData();
				PrepareReportData();
				this.Cursor = Cursors.Arrow;
			}
		}
		/// <summary>
		/// Validates the From and To dates
		/// </summary>
		/// <returns></returns>
		private bool validEntry() 
		{
			string strRet = "";
			int ilErrorCount = 1;

			if (this.ucView1.Viewer.CurrentView == "")
			{
				strRet += ilErrorCount.ToString() +  ". A view must be selected!\n";
				ilErrorCount++;
			}
			if (this.ucView1.Viewer.PeriodFrom >= this.ucView1.Viewer.PeriodTo)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}
			if(this.ucView1.Viewer.Arrival == true )
			{
				strRet += ilErrorCount.ToString() +  ". Arrival must not be used!\n";
				ilErrorCount++;
			}
			if (this.ucView1.Viewer.Departure == false)
			{
				strRet += ilErrorCount.ToString() +  ".Departure must be used!\n";
				ilErrorCount++;
			}
			if (this.ucView1.Viewer.Rotation == true)
			{
				strRet += ilErrorCount.ToString() +  ". Rotation must not be used!\n";
				ilErrorCount++;
			}
			if (strRet.Length > 0)
			{
				MessageBox.Show(this, strRet);
				return false;
			}
			else
			{
				return true;
			}
		}
		/// <summary>
		/// Reads the necessary data for the display from the table.
		/// </summary>
		private void LoadReportData()
		{
			string strTmpQry;
			//strTmpWhere = strWhereRaw;
			
			tabResult.ResetContent();
			DateTime datFrom;
			DateTime datTo;

			string strDateFrom = "";
			string strDateTo = "";
			string strFtyp = this.ucView1.Viewer.FlightTypes;
			datFrom = this.ucView1.Viewer.PeriodFrom;
			datTo   = this.ucView1.Viewer.PeriodTo;

			
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(this.ucView1.Viewer.PeriodFrom);
				datTo   = UT.LocalToUtc(this.ucView1.Viewer.PeriodTo);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(this.ucView1.Viewer.PeriodFrom);
				strDateTo = UT.DateTimeToCeda(this.ucView1.Viewer.PeriodTo);

			}
			myDB.Unbind("AFT");
			myAFT = myDB.Bind("AFT", "AFT", "URNO,RKEY,FLNO,ADID,ORG3,DES3,STOA,STOD,TIFA,TIFD,ACT3,TTYP,ONBL,OFBL,REGN,NOSE,ALC2,ALC3", 
				"10,10,12,1,3,3,14,14,14,14,3,5,14,14,12,3,2,3", 
				"URNO,RKEY,FLNO,ADID,ORG3,DES3,STOA,STOD,TIFA,TIFD,ACT3,TTYP,ONBL,OFBL,REGN,NOSE,ALC2,ALC3");

			myAFT.Clear();
			myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,ONBL,OFBL";
			myAFT.Command("read",",GFR,");
			ArrayList myUniques = new ArrayList();
			myUniques.Add("URNO");
			myAFT.UniqueFields = myUniques;
			myAFT.TimeFieldsInitiallyInUtc = true;

			DateTime datReadFrom = datFrom;
			DateTime datReadTo ;

			TimeSpan tsDays = (datTo - datFrom);
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) 
				ilTotal = 1;
			int loopCnt = 1;
			progressBar1.Value = 0;
			lblProgress.Text = "Loading Seats Data";
			lblProgress.Refresh();
			progressBar1.Show();
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) 
					datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);

				strTmpQry = "WHERE ";

				// check on code share enabled
				if (this.ucView1.Viewer.IncludeCodeShare)
				{
					string strTmpWhere1 = "";
					//patch the where statement according to the user's entry
					if (this.ucView1.Viewer.Arrival && this.ucView1.Viewer.Departure)
						strTmpWhere1 = strWhereRawB;
					else if (this.ucView1.Viewer.Arrival)
						strTmpWhere1 = strWhereRawA;
					else if (this.ucView1.Viewer.Departure)
						strTmpWhere1 = strWhereRawD;
					strTmpWhere1+= this.ucView1.Viewer.AdditionalSelection(0);
					strTmpWhere1+= this.ucView1.Viewer.GeneralFilter;

					string strTmpWhere2 = "";
					//patch the where statement according to the user's entry
					strTmpWhere2+= this.ucView1.Viewer.AdditionalSelection(1);

					strTmpQry = string.Format("WHERE ({0}) AND ({1})",strTmpWhere1,strTmpWhere2);
				}
				else
				{
					//patch the where statement according to the user's entry
					if (this.ucView1.Viewer.Arrival && this.ucView1.Viewer.Departure)
						strTmpQry += strWhereRawB;
					else if (this.ucView1.Viewer.Arrival)
						strTmpQry += strWhereRawA;
					else if (this.ucView1.Viewer.Departure)
						strTmpQry += strWhereRawD;
					strTmpQry+= this.ucView1.Viewer.AdditionalSelection(0);
					strTmpQry+= this.ucView1.Viewer.GeneralFilter;

				}


				strTmpQry = strTmpQry.Replace("@@FROM",strDateFrom);
				strTmpQry = strTmpQry.Replace("@@TO",strDateTo);
				strTmpQry = strTmpQry.Replace("@@ORIG",UT.Hopo);
				strTmpQry = strTmpQry.Replace("@@DEST",UT.Hopo);

				if (this.ucView1.Viewer.Rotation)
				{
					strTmpQry += "[ROTATIONS]";
					if (strFtyp.Length > 0)
						strTmpQry += string.Format("[FILTER=FTYP IN ({0})]",strFtyp);
				}

				myAFT.Load(strTmpQry);
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			} while(datReadFrom <= datReadTo);
			lblProgress.Text = "";
			progressBar1.Hide();
		}
		
		private void frmSeatStatisticsAccDomIntForPVG_Load(object sender, System.EventArgs e)
		{
			InitTab();
		}
		/// <summary>
		/// Initializes the table
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;
			if(radioButtonAirLine.Checked)
			{
				tabResult.HeaderString = strTabHeaderAirLines;
				tabResult.LogicalFieldList = strLogicalFieldsAirlines;
				tabResult.HeaderLengthString = strTabHeaderLensAirLines;
				PrepareReportAirline();
			}
			else if(radioButtonDest.Checked)
			{
				
				tabResult.HeaderString = strTabHeaderDestination;
				tabResult.LogicalFieldList = strLogicalFieldsDest;
				tabResult.HeaderLengthString = strTabHeaderLensAirLines;
				PrepareReportDestination();
			}
			else 
			{
				tabResult.HeaderString = strTabHeaderAirLineAndDest;
				tabResult.LogicalFieldList = strLogicalFieldsAlcDest;
				tabResult.HeaderLengthString = strTabHeaderLensAirLineDest;
				PrepareReportAirLineDestination();
			}
		}
		/// <summary>
		/// Prepares the report data for the display. 
		/// </summary>
		private void PrepareReportData()
		{
			if(myAFT == null) 
				return;
			if(radioButtonAirLine.Checked)
				PrepareReportAirline();
			else if(radioButtonDest.Checked)
				PrepareReportDestination();
			else 
				PrepareReportAirLineDestination();
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
		}
		/// <summary>
		/// Prepares the report data for the display when Airline radio button is selected
		/// </summary>
		private void PrepareReportAirline()
		{
			if(myAFT == null) 
				return;
			string strSchedule = "";
			DateTime tmpDate;
			int ilTotalFlights;
			int ilTotal = myAFT.Count;
			if(ilTotal == 0) ilTotal = 1;
			StringBuilder sb = new StringBuilder(10000);
			myAFT.Sort("ALC2",true);
			string  olNewAlc2,olOldAlc2,olNewAlc3,olOldAlc3,olTmpSeats;
			olNewAlc2 = "";
			olOldAlc2 = "";
			olNewAlc3 = "";
			olOldAlc3 = "";
			olTmpSeats= "";
			int ilTotalSeats = 0,ilTmpSeats = 0,ilTotalFlight = 0;
			for (int ilLc = 0; ilLc < myAFT.Count; ilLc++)
			{
				string strValues = ""; 
				int i = ilLc;
				//olOldAlc2 = myAFT[i]["ALC2"].;
				if(myAFT[i]["ALC2"].Length >0)
					olOldAlc2 = myAFT[i]["ALC2"];
				else if(myAFT[i]["ALC3"].Length > 0)
					olOldAlc3 = myAFT[i]["ALC3"];
				if((olOldAlc2.Length == 0) &&(olOldAlc3.Length == 0))
					continue ;

				string olStrADID = myAFT[i]["ADID"];
				olTmpSeats = myAFT[i]["NOSE"];
				if(olTmpSeats.Length > 0)
					ilTmpSeats = int.Parse(olTmpSeats);
				else
					ilTmpSeats = 0;

				if((i+1) < myAFT.Count)
				{
					if(myAFT[i+1]["ALC2"].Length >0 )
						olNewAlc2 = myAFT[i+1]["ALC2"];
					else
						olNewAlc3 = myAFT[i+1]["ALC3"];
				}
				
				if((olOldAlc2 == olNewAlc2) &&(olOldAlc3 == olNewAlc3)) 
				{
					olNewAlc3 = "";
					olNewAlc2 = "";
					ilTotalFlight++;
					ilTotalSeats += ilTmpSeats;
					continue; 
				}
				else
				{
					ilTotalFlight++;
					ilTotalSeats += ilTmpSeats;
					
					if(olOldAlc2.Length > 0)
						strValues += olOldAlc2 + ",";
					else
						strValues += olOldAlc3 + ",";
					strValues += ilTotalSeats.ToString() + ",";
					strValues += ilTotalFlight.ToString() + "\n";
					ilTotalSeats = 0;
					ilTotalFlight = 0;
					sb.Append(strValues);

				}
			}
			tabResult.InsertBuffer(sb.ToString(), "\n");
			tabResult.Sort("0", true, true);
			tabResult.Refresh();
		}
		/// <summary>
		/// Prepares the report data for the display when Destination radio button is selected
		/// </summary>
		private void PrepareReportDestination()
		{
			if(myAFT == null) 
				return;
			StringBuilder sb = new StringBuilder(10000);
			string olNewDestStr = "";
			string olOldDestStr = "";
			string olSeatStr = "";
			int ilTmpSeat = 0;
			int ilTotalSeats = 0 ;
			int ilTotlFlights = 0;
			myAFT.Sort("DES3",true);
			for (int ilLc = 0; ilLc < myAFT.Count; ilLc++)
			{
				int i = ilLc;
				string strValues = ""; 
				olOldDestStr = myAFT[ilLc]["DES3"];
				olSeatStr = myAFT[ilLc]["NOSE"];
				if(olSeatStr.Length > 0)
					ilTmpSeat = int.Parse(olSeatStr);
				else
					ilTmpSeat = 0;
				
				if((i+1) < myAFT.Count)
				{
					olNewDestStr = myAFT[i+1]["DES3"];
				}
				if(olNewDestStr == olOldDestStr) 	// neue Destination
				{
					olNewDestStr = "";
					ilTotalSeats += ilTmpSeat;
					ilTotlFlights++;
				}
				else
				{
					ilTotlFlights++;
					ilTotalSeats += ilTmpSeat;
					strValues = olOldDestStr + ",";
					strValues += ilTotalSeats.ToString() + ",";
					strValues += ilTotlFlights.ToString() + "\n";

					ilTotalSeats = 0;
					ilTotlFlights = 0;
					sb.Append(strValues);
				}
			}
			tabResult.InsertBuffer(sb.ToString(), "\n");
			tabResult.Sort("0", true, true);
			tabResult.Refresh();
		}
		/// <summary>
		/// Prepares the report for display when the Airline and Destination radio button is selected.
		/// </summary>
		private void PrepareReportAirLineDestination()
		{
			if(myAFT == null) 
				return;
			StringBuilder sb = new StringBuilder(10000);
			string olNewDestStr = "";
			string olOldDestStr = "";
			string olOldAlc2Str = "";
			string olNewAlc2Str = "";

			string olSeatStr = "";
			int ilTmpSeats = 0;
			int ilTotalSeats = 0 ;
			int ilTotlFlightAirLine = 0;
			int ilTotalFlightDest = 0;
			myAFT.Sort("ALC2,DES3",true);
			for (int ilLc = 0; ilLc < myAFT.Count; ilLc++)
			{
				string strValues = ""; 
				int i = ilLc;
				if(myAFT[i]["ALC2"].Length >0)
					olOldAlc2Str = myAFT[i]["ALC2"];
				else if(myAFT[i]["ALC3"].Length > 0)
					olOldAlc2Str = myAFT[i]["ALC3"];

				olOldDestStr = myAFT[i]["DES3"];
				olSeatStr = myAFT[i]["NOSE"];
				if(olSeatStr.Length > 0)
					ilTmpSeats = int.Parse(olSeatStr);
				else
					ilTmpSeats = 0;

				if((i+1) < myAFT.Count)
				{
					if(myAFT[i+1]["ALC2"].Length >0 )
						olNewAlc2Str = myAFT[i+1]["ALC2"];
					else
						olNewAlc2Str = myAFT[i+1]["ALC3"];
					olNewDestStr = myAFT[i+1]["DES3"];
				}

				if(olOldAlc2Str != olNewAlc2Str)
				{
					ilTotalSeats += ilTmpSeats;
					ilTotalFlightDest++;
					ilTotlFlightAirLine++;
					strValues = olOldAlc2Str + "," + olOldDestStr + "," + ilTotalSeats.ToString()+  "," + ilTotalFlightDest.ToString()+ "," + "\n" ;
					sb.Append(strValues);
					
					strValues = olOldAlc2Str + ",";
					strValues += ",,,";
					//strValues += ilTotalSeats.ToString() + ",";
					//strValues += ilTotalFlightDest.ToString() + ",";
					strValues += ilTotlFlightAirLine.ToString() + "\n";
					ilTotlFlightAirLine = 0;
					ilTotalFlightDest = 0;
					ilTotalSeats = 0;
					sb.Append(strValues);
					strValues = ",,,,\n";
					if((ilLc+1) < myAFT.Count)
					{
					   sb.Append(strValues);
					}
					
				}
				else if(olOldDestStr != olNewDestStr)
				{
					ilTotalFlightDest++;
					ilTotlFlightAirLine++;
					ilTotalSeats += ilTmpSeats;

					strValues = olOldAlc2Str + "," + olOldDestStr + ",";
					strValues += ilTotalSeats.ToString() + ",";
					strValues += ilTotalFlightDest.ToString() + "," + "\n";
					ilTotalFlightDest = 0;
					ilTotalSeats = 0;
					sb.Append(strValues);
					
				}
				else
				{
					ilTotalFlightDest++;
					ilTotalSeats += ilTmpSeats;
					ilTotlFlightAirLine++;
					olNewDestStr = "";
					olNewAlc2Str = "";
				}

			}
			tabResult.InsertBuffer(sb.ToString(), "\n");
			//tabResult.Sort("0", true, true);
			tabResult.Refresh();
		}

		private void ucView1_viewEvent(string view)
		{
			btnLoad_Click(this,null);
		}
		/// <summary>
		/// Event handler for the selection change in the Airline radio button. 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void radioButtonAirLine_CheckedChanged_1(object sender, System.EventArgs e)
		{
			if(radioButtonAirLine.Checked)
			{
				tabResult.ResetContent();
				tabResult.HeaderString = strTabHeaderAirLines;
				tabResult.LogicalFieldList = strLogicalFieldsAirlines;
				tabResult.HeaderLengthString = strTabHeaderLensAirLines;
				PrepareReportAirline();
			}
		}
		/// <summary>
		/// Event handler for the selection change in the Destination radio button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void radioButtonDest_CheckedChanged_1(object sender, System.EventArgs e)
		{
			if(radioButtonDest.Checked)
			{
				tabResult.ResetContent();
				tabResult.HeaderString = strTabHeaderDestination;
				tabResult.LogicalFieldList = strLogicalFieldsDest;
				tabResult.HeaderLengthString = strTabHeaderLensAirLines;
				PrepareReportDestination();
			}
		}
		/// <summary>
		/// Event handler for the selection change of the Airline and Destination radio button..
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void radioButtonAirLineDest_CheckedChanged_1(object sender, System.EventArgs e)
		{
			if(radioButtonAirLineDest.Checked)
			{
				tabResult.ResetContent();
				tabResult.HeaderString = strTabHeaderAirLineAndDest;
				tabResult.LogicalFieldList = strLogicalFieldsAlcDest;
				tabResult.HeaderLengthString = strTabHeaderLensAirLineDest;
				PrepareReportAirLineDestination();
			}
		}
		/// <summary>
		/// Event handler for the Print Preview 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void buttonPrintPView_Click(object sender, System.EventArgs e)
		{
			RunReport();
		}
		/// <summary>
		/// Initializes the Preview page  with the header and the footer
		/// </summary>
		private void RunReport() 
		{
			StringBuilder strSubHeader = new StringBuilder();
			if (this.ucView1.Viewer.Arrival)
				strSubHeader.Append("Arrival ");
			else
				strSubHeader.Append("Departure ");
			strSubHeader.Append("From: ")
				.Append(this.ucView1.Viewer.PeriodFrom.ToString("dd.MM.yy'/'HH:mm"))
				.Append(" To: ")
				.Append((this.ucView1.Viewer.PeriodTo.ToString("dd.MM.yy'/'HH:mm")));
			strSubHeader.Append("View: ");
			strSubHeader.Append(this.ucView1.Viewer.CurrentView);
			strSubHeader.Append(" (Statistics of Seats) ");
			//rptA3_Landscape rpt = new rptA3_Landscape(tabResult, "Statistics - Arrival / Departure", strSubHeader.ToString(), "", 8);
			prtFIPS_Landscape rpt = new prtFIPS_Landscape(tabResult,"Statistics of Seats",strSubHeader.ToString(),"",8);
			rpt.TextBoxCanGrow = false;
			frmPrintPreview frm = new frmPrintPreview(rpt);
			frm.Show();

		}
		/// <summary>
		/// Event handler for the Laod and Preview button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			if(validEntry())
			{
				this.Cursor = Cursors.WaitCursor;
				this.InitTab();
				LoadReportData();
				PrepareReportData();
				RunReport();
				this.Cursor = Cursors.Arrow;
			}
		}
		private void btnCancel_Click_1(object sender, System.EventArgs e)
		{
			this.Close();
		}
		/// <summary>
		/// Event handler for the double click of the LButton mouse click on the column header of the table.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tabResult_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(!radioButtonAirLineDest.Checked)
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if( tabResult.SortOrderASC == true)
					{
						tabResult.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						tabResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabResult.Sort( e.colNo.ToString(), true, true);
				}
				tabResult.Refresh();
			}
		}
	}
}
