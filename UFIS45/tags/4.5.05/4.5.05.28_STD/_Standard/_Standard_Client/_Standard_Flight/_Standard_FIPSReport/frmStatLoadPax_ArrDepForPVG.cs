using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Ufis.Utils;
using Ufis.Data;
using System.Text;

namespace FIPS_Reports
{
	/// <summary>
	/// This class is created for the support of the Statistics of the Load and Pax for the PVG airport.This is done 
	/// while solving the PRF 8523.
	/// </summary>
	public class frmStatLoadPax_ArrDepForPVG : System.Windows.Forms.Form
	{
		#region _My Members

		/// <summary>
		/// Summary description for frmStatLoadPax_ArrDepForPVG. This class is created for the Load Pax support for the PVG airport .
		/// Created while solving the 
		/// </summary>
		private string strWhereRawA =  "((STOA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))) OR (TIFA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))))" ;

		private string strWhereRawD =  "((STOD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('D','B'))) OR (TIFD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('D','B'))))" ;

		private string strWhereRawB = string.Format("({0} OR {1})","((STOA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))) OR (TIFA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))))","((STOD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('D','B'))) OR (TIFD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('D','B'))))");

		private string strTabHeaderArrival = "Flight ,ORG ,STA ,ATA ,A/C ,REG ,NA ,ALC ,POS ,Gate ,Belt ,F ,C ,Y ,Inf ,Total ,ID ,TR ,TRF ,BAG ,CGO ,MAIL ,Total";
		private string strAPXWhereRaw = "where FLNU in (@@FLNU)";
		private string strLOAWhereRaw = "where FLNU in (@@FLNU) "+
			"AND DSSN IN ('USR','LDM','MVT','KRI') "+
			"AND TYPE IN ('PAX','PAD','LOA') "+
			"AND APC3=' '";
		private string strCCAWhereRaw = "where FLNU in (@@FLNU)";
		/// <summary>
		/// The logical mapping of the columns
		/// </summary>
		private string strLogicalFieldsArrival = "FLNO,ORG,STOA,ATA,AC_TYP,REG,NA,ALC,POS,GATE,BELT,PAX_F,PAX_C,PAX_Y,PAX_INF,P_TTL,ID,TR,TRF,BAG,CGO,MAIL,TTL";

		/// <summary>
		/// The length of each column
		/// </summary>
		private string strTabHeaderLensArrival = "60,30,50,50,40,50,40,40,40,40,80,40,40,40,40,40,40,40,40,40,40,40,40";

		/// <summary>
		/// Heading of the columns as it needs to be in the report output - 21 Columns
		/// </summary>
		private string strTabHeaderDeparture = "Flight ,DES ,STD ,ATD ,A/C ,REG ,NA ,ALC ,Cki ,Gate ,POS ,F ,C ,Y ,Inf ,Total ,ID ,TR ,TRF ,BAG ,CGO ,MAIL ,Total";

		/// <summary>
		/// The logical mapping of the columns
		/// </summary>
		private string strLogicalFieldsDeparture = "FLNO,DES,STOD,ATD,AC_TYP,REG,NA,ALC,CKIN,GATE,POS,PAX_F,PAX_C,PAX_Y,PAX_INF,P_TTL,ID,TR,TRF,BAG,CGO,MAIL,TTL";
		
		/// <summary>
		/// The length of each column
		/// </summary>
		private String strTabHeaderLensDeparture = "60,30,50,50,40,40,40,40,80,40,40,40,40,40,40,40,40,40,40,40,40,40,40";

		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
		private ITable myCCA;
		private ITable myAPX;
		private ITable myLOA;
	
		private int iTotalFlights = 0;
		#endregion _My Members	

		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Button buttonPrintPView;
		private System.Windows.Forms.CheckBox cbLoadInfo;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Panel panelTab;
		private AxTABLib.AxTAB tabResult;
		private System.Windows.Forms.Label lblResults;
		private UserControls.ucView ucView1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		/// <summary>
		/// The constructor where all the members are initiallized 
		/// </summary>
		public frmStatLoadPax_ArrDepForPVG()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			myDB = UT.GetMemDB();
			this.ucView1.Report = "ArrivalDepartureLoadPaxStatistics";
			this.ucView1.viewEvent +=new UserControls.ucView.ViewChangedEvent(ucView1_viewEvent);

			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmStatLoadPax_ArrDepForPVG));
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.buttonPrintPView = new System.Windows.Forms.Button();
			this.cbLoadInfo = new System.Windows.Forms.CheckBox();
			this.button1 = new System.Windows.Forms.Button();
			this.panelBody = new System.Windows.Forms.Panel();
			this.panelTab = new System.Windows.Forms.Panel();
			this.tabResult = new AxTABLib.AxTAB();
			this.lblResults = new System.Windows.Forms.Label();
			this.ucView1 = new UserControls.ucView();
			this.panelBody.SuspendLayout();
			this.panelTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			this.SuspendLayout();
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(352, 64);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 35;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(352, 88);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 34;
			this.progressBar1.Visible = false;
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(252, 88);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 33;
			this.btnCancel.Text = "&Close";
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(175, 88);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 32;
			this.btnLoadPrint.Text = "Loa&d + Print";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// buttonPrintPView
			// 
			this.buttonPrintPView.BackColor = System.Drawing.SystemColors.Control;
			this.buttonPrintPView.Location = new System.Drawing.Point(93, 88);
			this.buttonPrintPView.Name = "buttonPrintPView";
			this.buttonPrintPView.Size = new System.Drawing.Size(80, 23);
			this.buttonPrintPView.TabIndex = 31;
			this.buttonPrintPView.Text = "&Print Preview";
			this.buttonPrintPView.Click += new System.EventHandler(this.buttonPrintPView_Click);
			// 
			// cbLoadInfo
			// 
			this.cbLoadInfo.Location = new System.Drawing.Point(344, 24);
			this.cbLoadInfo.Name = "cbLoadInfo";
			this.cbLoadInfo.Size = new System.Drawing.Size(168, 24);
			this.cbLoadInfo.TabIndex = 30;
			this.cbLoadInfo.Text = "Use Loading Information";
			this.cbLoadInfo.CheckedChanged += new System.EventHandler(this.cbLoadInfo_CheckedChanged);
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.SystemColors.Control;
			this.button1.Location = new System.Drawing.Point(16, 88);
			this.button1.Name = "button1";
			this.button1.TabIndex = 29;
			this.button1.Text = "Load";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// panelBody
			// 
			this.panelBody.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.panelBody.Controls.Add(this.panelTab);
			this.panelBody.Controls.Add(this.lblResults);
			this.panelBody.Location = new System.Drawing.Point(0, 136);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(944, 392);
			this.panelBody.TabIndex = 36;
			// 
			// panelTab
			// 
			this.panelTab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.panelTab.Controls.Add(this.tabResult);
			this.panelTab.Location = new System.Drawing.Point(0, 16);
			this.panelTab.Name = "panelTab";
			this.panelTab.Size = new System.Drawing.Size(944, 376);
			this.panelTab.TabIndex = 3;
			// 
			// tabResult
			// 
			this.tabResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.tabResult.ContainingControl = this;
			this.tabResult.Location = new System.Drawing.Point(0, 0);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(944, 376);
			this.tabResult.TabIndex = 0;
			this.tabResult.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabResult_SendLButtonDblClick);
			// 
			// lblResults
			// 
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 0);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(944, 16);
			this.lblResults.TabIndex = 2;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// ucView1
			// 
			this.ucView1.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.ucView1.Location = new System.Drawing.Point(24, 24);
			this.ucView1.Name = "ucView1";
			this.ucView1.Report = "";
			this.ucView1.Size = new System.Drawing.Size(304, 40);
			this.ucView1.TabIndex = 18;
			// 
			// frmStatLoadPax_ArrDepForPVG
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.ClientSize = new System.Drawing.Size(944, 525);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.lblProgress);
			this.Controls.Add(this.progressBar1);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnLoadPrint);
			this.Controls.Add(this.buttonPrintPView);
			this.Controls.Add(this.cbLoadInfo);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.ucView1);
			this.Name = "frmStatLoadPax_ArrDepForPVG";
			this.Text = "frmStatLoadPax_ArrDepForPVG...";
			this.Load += new System.EventHandler(this.frmStatLoadPax_ArrDepForPVG_Load);
			this.panelBody.ResumeLayout(false);
			this.panelTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void frmStatLoadPax_ArrDepForPVG_Load(object sender, System.EventArgs e)
		{
			InitTab();
		}
		/// <summary>
		/// This function initializes the table to be displayed..
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;
			if(this.ucView1.Viewer.Arrival == true)
			{
				tabResult.HeaderString = strTabHeaderArrival;
				tabResult.LogicalFieldList = strLogicalFieldsArrival;
				tabResult.HeaderLengthString = strTabHeaderLensArrival;
				PrepareReportDataArrival();
			}
			else
			{
				tabResult.HeaderString = strTabHeaderDeparture;
				tabResult.LogicalFieldList = strLogicalFieldsDeparture;
				tabResult.HeaderLengthString = strTabHeaderLensDeparture;
				PrepareReportDataDeparture();
			}
		}
		/// <summary>
		/// LoadReportData reads all the data required to display,from the table.
		/// </summary>
		private void LoadReportData()
		{
			string strTmpQry;
			tabResult.ResetContent();
			DateTime datFrom;
			DateTime datTo;

			string strDateFrom = "";
			string strDateTo = "";
			string strFtyp = this.ucView1.Viewer.FlightTypes;
			datFrom = this.ucView1.Viewer.PeriodFrom;
			datTo   = this.ucView1.Viewer.PeriodTo;

			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(this.ucView1.Viewer.PeriodFrom);
				datTo   = UT.LocalToUtc(this.ucView1.Viewer.PeriodTo);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(this.ucView1.Viewer.PeriodFrom);
				strDateTo = UT.DateTimeToCeda(this.ucView1.Viewer.PeriodTo);

			}
			myDB.Unbind("AFT");
			myAFT = myDB.Bind("AFT", "AFT", "URNO,FLNO,ORG3,STOA,LAND,DES3,STOD,AIRB,ACT3,REGN,PSTD,PSTA,GTA1,GTA2,GTD1,GTD2,BLT1,BLT2,ADID,PAX1,PAX2,PAX3,PAXF,PAXI,PAXT,BAGN,BAGW,MAIL,CGOT,TTYP,ALC3"
				, "10,9,3,14,14,3,14,14,3,12,5,5,5,5,5,5,5,5,1,3,3,3,3,3,3,6,7,6,6,5,3"
				, "URNO,FLNO,ORG3,STOA,LAND,DES3,STOD,AIRB,ACT3,REGN,PSTD,PSTA,GTA1,GTA2,GTD1,GTD2,BLT1,BLT2,ADID,PAX1,PAX2,PAX3,PAXF,PAXI,PAXT,BAGN,BAGW,MAIL,CGOT,TTYP,ALC3");

			myAFT.Clear();
			myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,ONBL,OFBL";
			myAFT.Command("read",",GFR,");
			ArrayList myUniques = new ArrayList();
			myUniques.Add("URNO");
			myAFT.UniqueFields = myUniques;
			myAFT.TimeFieldsInitiallyInUtc = true;

			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo ;
			TimeSpan tsDays = (datTo - datFrom);

			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			int loopCnt = 1;
			progressBar1.Value = 0;
			lblProgress.Text = "Loading Flight Data";
			lblProgress.Refresh();
			progressBar1.Show();
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;

				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				strTmpQry = "WHERE ";
				// check on code share enabled
				if (this.ucView1.Viewer.IncludeCodeShare)
				{
					string strTmpWhere1 = "";
					//patch the where statement according to the user's entry
					if (this.ucView1.Viewer.Arrival && this.ucView1.Viewer.Departure)
						strTmpWhere1 = strWhereRawB;
					else if (this.ucView1.Viewer.Arrival)
						strTmpWhere1 = strWhereRawA;
					else if (this.ucView1.Viewer.Departure)
						strTmpWhere1 = strWhereRawD;
					strTmpWhere1+= this.ucView1.Viewer.AdditionalSelection(0);
					strTmpWhere1+= this.ucView1.Viewer.GeneralFilter;

					string strTmpWhere2 = "";
					//patch the where statement according to the user's entry
					strTmpWhere2+= this.ucView1.Viewer.AdditionalSelection(1);

					strTmpQry = string.Format("WHERE ({0}) AND ({1})",strTmpWhere1,strTmpWhere2);
				}
				else
				{
					//patch the where statement according to the user's entry
					if (this.ucView1.Viewer.Arrival && this.ucView1.Viewer.Departure)
						strTmpQry += strWhereRawB;
					else if (this.ucView1.Viewer.Arrival)
						strTmpQry += strWhereRawA;
					else if (this.ucView1.Viewer.Departure)
						strTmpQry += strWhereRawD;
					strTmpQry+= this.ucView1.Viewer.AdditionalSelection(0);
					strTmpQry+= this.ucView1.Viewer.GeneralFilter;

				}

				strTmpQry = strTmpQry.Replace("@@FROM", strDateFrom);
				strTmpQry = strTmpQry.Replace("@@TO", strDateTo );
				strTmpQry = strTmpQry.Replace("@@ORIG",UT.Hopo);
				strTmpQry = strTmpQry.Replace("@@DEST",UT.Hopo);
				strTmpQry = strTmpQry.Replace("@@HOPO", UT.Hopo);
				if (this.ucView1.Viewer.Rotation)
				{
					strTmpQry += "[ROTATIONS]";
					if (strFtyp.Length > 0)
						strTmpQry += string.Format("[FILTER=FTYP IN ({0})]",strFtyp);
				}
				myAFT.Load(strTmpQry);
				datReadFrom = datReadFrom.AddDays(1);//+=  new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
			lblProgress.Text = "";
			progressBar1.Hide();

			for(int i = myAFT.Count-1; i >= 0; i--)
			{
				if(myAFT[i]["FTYP"] == "T" || myAFT[i]["FTYP"] == "G" || 
					myAFT[i]["FTYP"] == "N" || myAFT[i]["FTYP"] == "B" ||  myAFT[i]["FTYP"] == "Z" )
				{
					myAFT.Remove(i);
				}
			}

			LoadCheckInCounterData();
			LoadPaxAndLoadData();
			myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
		}

		/// <summary>
		/// Loads the Check-In counter data corresponding to the flights 
		/// selected from the AFT table.
		/// </summary>
		private void LoadCheckInCounterData() 
		{
			int ilTotal = 0;
			int loopCnt = 0;
			int percent = 0;
			StringBuilder strFlnus = new StringBuilder();
			string strTmpQry;

			progressBar1.Value = 0;
			lblProgress.Text = "Loading Counter Data";
			lblProgress.Refresh();
			progressBar1.Show();
			myDB.Unbind("CCA");
			myCCA = myDB.Bind("CCA", "CCA"
				, "FLNU,CKIC"
				, "10,5"
				, "FLNU,CKIC");
			myCCA.Clear();

			ilTotal = (int)(myAFT.Count/300);
			if(ilTotal == 0) 
				ilTotal = 1;
			loopCnt = 0;
			for(int i = 0; i < myAFT.Count; i++)
			{
				strFlnus.Append(myAFT[i]["URNO"]).Append(",");
				if((i % 300) == 0 && i > 0)
				{
					loopCnt++;
					percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
					if (percent > 100) 
						percent = 100;
					progressBar1.Value = percent;

					strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
					strTmpQry = strCCAWhereRaw;
					strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
					myCCA.Load(strTmpQry);
					strFlnus.Remove(0,strFlnus.Length);
				}
			}
			if(strFlnus.Length > 0)
			{
				strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
				strTmpQry = strCCAWhereRaw;
				strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
				myCCA.Load(strTmpQry);
				strFlnus.Remove(0,strFlnus.Length);
			}
			myCCA.CreateIndex("FLNU", "FLNU");
			lblProgress.Text = "";
			progressBar1.Hide();
		}
		/// <summary>
		/// Loads the Load and Pax data from the respective table..
		/// </summary>
		private void LoadPaxAndLoadData()
		{
			int ilTotal = 0;
			int loopCnt = 0;
			int percent = 0;
			string strTmpQry;
			StringBuilder strFlnus = new StringBuilder();

			progressBar1.Value = 0;
			lblProgress.Text = "Loading Load Data";
			lblProgress.Refresh();
			progressBar1.Show();

			if (cbLoadInfo.Checked) 
			{
				myDB.Unbind("LOA");
				myLOA = myDB.Bind("LOA","LOA"
					,"FLNU,DSSN,TYPE,STYP,SSTP,SSST,VALU"
					,"10,10,14,40,1,3,3,3,3,6"
					,"FLNU,DSSN,TYPE,STYP,SSTP,SSST,VALU");
				myLOA.Clear();

				ilTotal = (int)(myAFT.Count/300);
				if(ilTotal == 0) 
					ilTotal = 1;
				loopCnt = 0;
				for(int i = 0; i < myAFT.Count; i++)
				{
					strFlnus.Append(myAFT[i]["URNO"]).Append(",");
					if((i % 300) == 0 && i > 0)
					{
						loopCnt++;
						percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
						if (percent > 100) 
							percent = 100;
						progressBar1.Value = percent;

						strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
						strTmpQry = strLOAWhereRaw;
						strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
						myLOA.Load(strTmpQry);
						strFlnus.Remove(0,strFlnus.Length);
					}
				}
				if(strFlnus.Length > 0)
				{
					strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
					strTmpQry = strLOAWhereRaw;
					strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
					myLOA.Load(strTmpQry);
					strFlnus.Remove(0,strFlnus.Length);
				}
				myLOA.CreateIndex("FLNU", "FLNU");
				lblProgress.Text = "";
				progressBar1.Hide();
			}
			else
			{
				myDB.Unbind("APX");
				myAPX = myDB.Bind("APX","APX"
					,"FLNU,URNO,TYPE,PAXC"
					,"10,10,3,6"
					,"FLNU,URNO,TYPE,PAXC");
				myAPX.Clear();

				ilTotal = (int)(myAFT.Count/300);
				if(ilTotal == 0) 
					ilTotal = 1;
				loopCnt = 0;
				for(int i = 0; i < myAFT.Count; i++)
				{
					strFlnus.Append(myAFT[i]["URNO"]).Append(",");
					if((i % 300) == 0 && i > 0)
					{
						loopCnt++;
						percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
						if (percent > 100) 
							percent = 100;
						progressBar1.Value = percent;

						strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
						strTmpQry = strAPXWhereRaw;
						strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
						myAPX.Load(strTmpQry);
						strFlnus.Remove(0,strFlnus.Length);
					}
				}
				if(strFlnus.Length > 0)
				{
					strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
					strTmpQry = strAPXWhereRaw;
					strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
					myAPX.Load(strTmpQry);
					strFlnus.Remove(0,strFlnus.Length);
				}
				myAPX.CreateIndex("FLNU", "FLNU");
				lblProgress.Text = "";
				progressBar1.Hide();
			}
		}
		/// <summary>
		/// Prepares the Load data for the display on the table.
		/// </summary>
		/// <param name="dType"></param>
		/// <param name="strAftUrno"></param>
		/// <param name="styp"></param>
		/// <param name="sstp"></param>
		/// <param name="ssst"></param>
		/// <returns></returns>
		private string GetLoadData(string dType, string strAftUrno, string styp, string sstp, string ssst)
		{
			string strLDM = "";
			string strMVT = "";
			string strKRI = "";
			
			IRow [] rows = myLOA.RowsByIndexValue("FLNU", strAftUrno);
			for(int i = 0; i < rows.Length; i++)
			{
				if (rows[i]["TYPE"].Equals(dType))
				{
					if ( rows[i]["STYP"].Equals(styp) 
						&& rows[i]["SSTP"].Equals(sstp) 
						&& rows[i]["SSST"].Equals(ssst))
					{
						if(rows[i]["DSSN"]=="USR")
							return rows[i]["VALU"];
						if(rows[i]["DSSN"] == "LDM")
							strLDM = rows[i]["VALU"];
						if(rows[i]["DSSN"] == "MVT")
							strMVT = rows[i]["VALU"];
						if(rows[i]["DSSN"] == "KRI")
							strKRI = rows[i]["VALU"];
					}
				}
			}
			if(strLDM != "") return strLDM;
			if(strMVT != "") return strMVT;
			if(strKRI != "") return strKRI;
			return "";
		}
		/// <summary>
		/// Prepares the data to be dipalyed on the table ( Departure data)
		/// </summary>
		private void PrepareReportDataDeparture()
		{
			if (myAFT == null)
				return;
			StringBuilder strData = new StringBuilder(200);
			StringBuilder sb = new StringBuilder(100000);
			string strCknCtr;
			string strId = "";
			string strInf = "";
			iTotalFlights = 0;
			IRow [] rows;

			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strDateDisplayFormat = myIni.IniReadValue("FIPSREPORT", "DATEDISPLAYFORMAT");
			if(strDateDisplayFormat.Length == 0)
			{
				strDateDisplayFormat = "dd'/'HH:mm";
			}

			for (int i = 0; i<myAFT.Count ; i++ )
			{
				if (myAFT[i]["ADID"] == "D") 
				{
					strData.Append(myAFT[i]["FLNO"]).Append(",");
					strData.Append(myAFT[i]["DES3"]).Append(",");
					strData.Append(Helper.DateString(myAFT[i]["STOD"],strDateDisplayFormat)).Append(",");
					strData.Append(Helper.DateString(myAFT[i]["AIRB"],strDateDisplayFormat)).Append(",");
					strData.Append(myAFT[i]["ACT3"]).Append(",");
					strData.Append(myAFT[i]["REGN"]).Append(",");
					strData.Append(myAFT[i]["TTYP"]).Append(",");
					strData.Append(myAFT[i]["ALC3"]).Append(",");
					rows = myCCA.RowsByIndexValue("FLNU", myAFT[i]["URNO"]);
					strCknCtr = "";
					for(int j = 0; j < rows.Length; j++)
						strCknCtr += rows[j]["CKIC"] + "/ ";

					if(rows.Length == 0)
						strData.Append(",");
					else
						strData.Append(strCknCtr.Substring(0,strCknCtr.Length-2)).Append(",");
					if ( !myAFT[i]["GTD2"].Equals(""))
						strData.Append(myAFT[i]["GTD1"]).Append("/").Append(myAFT[i]["GTD2"]).Append(",");
					else
						strData.Append(myAFT[i]["GTD1"]).Append(",");
					strData.Append(myAFT[i]["PSTD"]).Append(",");
					if (cbLoadInfo.Checked) 
					{
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"F","","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"B","","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"E","","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"","","I")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"T","","")).Append(",");
						strData.Append(GetLoadData("PAD",myAFT[i]["URNO"],"E","","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"T","R","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"T","T","")).Append(",");
						strData.Append(GetLoadData("LOA",myAFT[i]["URNO"],"B","","")).Append(",");
						strData.Append(GetLoadData("LOA",myAFT[i]["URNO"],"C","","")).Append(",");
						strData.Append(GetLoadData("LOA",myAFT[i]["URNO"],"M","","")).Append(",");
						strData.Append(GetLoadData("LOA",myAFT[i]["URNO"],"T","",""));
					} 
					else
					{
						strData.Append(myAFT[i]["PAX1"]).Append(",");
						strData.Append(myAFT[i]["PAX2"]).Append(",");
						strData.Append(myAFT[i]["PAX3"]).Append(",");

						rows = myAPX.RowsByIndexValue("FLNU", myAFT[i]["URNO"]);
						for(int j = 0; j < rows.Length; j++)
						{
							if (rows[j]["TYPE"].Equals("INF"))
								strInf = rows[j]["PAXC"];

							if (rows[j]["TYPE"].Equals("ID"))
								strId = rows[j]["PAXC"];
						}
						strData.Append(strInf).Append(",");
						strData.Append(myAFT[i]["PAXT"]).Append(",");
						strData.Append(strId).Append(",");
						strData.Append(myAFT[i]["PAXI"]).Append(",");
						strData.Append(myAFT[i]["PAXF"]).Append(",");
						strData.Append(myAFT[i]["BAGN"]).Append(",");
						strData.Append(myAFT[i]["CGOT"]).Append(",");
						strData.Append(myAFT[i]["MAIL"]).Append(",");
						strData.Append(myAFT[i]["BAGW"]);
					}
					strData.Append("\n");
					sb.Append(strData.ToString());
					strData.Remove(0,strData.Length);
					iTotalFlights++;
				}
				
			}
			tabResult.InsertBuffer(sb.ToString(), "\n");
			tabResult.Sort("2", true, true);
			tabResult.Refresh();
		}
		/// <summary>
		/// Prepares the data to be displayed on the table (Arrival data)
		/// </summary>
		private void PrepareReportDataArrival()
		{
			if (myAFT == null)
				return;
			StringBuilder strData = new StringBuilder(200);
			StringBuilder sb = new StringBuilder(100000);
			string strId = "";
			string strInf = "";
			iTotalFlights = 0;
			IRow [] rows;

			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strDateDisplayFormat = myIni.IniReadValue("FIPSREPORT", "DATEDISPLAYFORMAT");
			if(strDateDisplayFormat.Length == 0)
			{
				strDateDisplayFormat = "dd'/'HH:mm";
			}

			for (int i = 0; i<myAFT.Count ; i++ )
			{
				if (myAFT[i]["ADID"] == "A") 
					// || myAFT[i]["ADID"].Equals("B") 
				{
					strData.Append(myAFT[i]["FLNO"]).Append(",");
					strData.Append(myAFT[i]["ORG3"]).Append(",");
					strData.Append(Helper.DateString(myAFT[i]["STOA"],strDateDisplayFormat)).Append(",");
					strData.Append(Helper.DateString(myAFT[i]["ATA"],strDateDisplayFormat)).Append(",");
					strData.Append(myAFT[i]["ACT3"]).Append(",");
					strData.Append(myAFT[i]["REGN"]).Append(",");
					strData.Append(myAFT[i]["TTYP"]).Append(",");
					strData.Append(myAFT[i]["ALC3"]).Append(",");
					strData.Append(myAFT[i]["PSTA"]).Append(",");
					if ( !myAFT[i]["GTA2"].Equals(""))
						strData.Append(myAFT[i]["GTA1"]).Append("/").Append(myAFT[i]["GTA2"]).Append(",");
					else
						strData.Append(myAFT[i]["GTA1"]).Append(",");
					if (!myAFT[i]["BLT2"].Equals(""))
						strData.Append(myAFT[i]["BLT1"]).Append("-").Append(myAFT[i]["BLT2"]).Append(",");
					else
						strData.Append(myAFT[i]["BLT1"]).Append(",");
					if (cbLoadInfo.Checked) 
					{
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"F","","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"B","","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"E","","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"","","I")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"T","","")).Append(",");
						strData.Append(GetLoadData("PAD",myAFT[i]["URNO"],"E","","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"T","R","")).Append(",");
						strData.Append(GetLoadData("PAX",myAFT[i]["URNO"],"T","T","")).Append(",");
						strData.Append(GetLoadData("LOA",myAFT[i]["URNO"],"B","","")).Append(",");
						strData.Append(GetLoadData("LOA",myAFT[i]["URNO"],"C","","")).Append(",");
						strData.Append(GetLoadData("LOA",myAFT[i]["URNO"],"M","","")).Append(",");
						strData.Append(GetLoadData("LOA",myAFT[i]["URNO"],"T","",""));
					}
					else
					{
						strData.Append(myAFT[i]["PAX1"]).Append(",");
						strData.Append(myAFT[i]["PAX2"]).Append(",");
						strData.Append(myAFT[i]["PAX3"]).Append(",");

						rows = myAPX.RowsByIndexValue("FLNU", myAFT[i]["URNO"]);
						for(int j = 0; j < rows.Length; j++)
						{
							if (rows[j]["TYPE"].Equals("INF"))
								strInf = rows[j]["PAXC"];

							if (rows[j]["TYPE"].Equals("ID"))
								strId = rows[j]["PAXC"];
						}
						strData.Append(strInf).Append(",");
						strData.Append(myAFT[i]["PAXT"]).Append(",");
						strData.Append(strId).Append(",");
						strData.Append(myAFT[i]["PAXI"]).Append(",");
						strData.Append(myAFT[i]["PAXF"]).Append(",");
						strData.Append(myAFT[i]["BAGN"]).Append(",");
						strData.Append(myAFT[i]["CGOT"]).Append(",");
						strData.Append(myAFT[i]["MAIL"]).Append(",");
						strData.Append(myAFT[i]["BAGW"]);
					}
					strData.Append("\n");
					sb.Append(strData.ToString());
					strData.Remove(0,strData.Length);
					iTotalFlights++;
				}
			}
			tabResult.InsertBuffer(sb.ToString(),"\n");
			tabResult.Sort("2", true, true);
			tabResult.Refresh();
			lblResults.Text = "";
			lblResults.Text = "Report Results: ("  + tabResult.GetLineCount().ToString() + ")";
		}
		/// <summary>
		/// Runreport initializes the Preview page with header and footer ..
		/// 
		/// </summary>
		private void RunReport() 
		{
			StringBuilder strSubHeader = new StringBuilder();
			if (this.ucView1.Viewer.Arrival)
				strSubHeader.Append("Arrival ");
			else
				strSubHeader.Append("Departure ");
			strSubHeader.Append("From: ")
				.Append(this.ucView1.Viewer.PeriodFrom.ToString("dd.MM.yy'/'HH:mm"))
				.Append(" To: ")
				.Append((this.ucView1.Viewer.PeriodTo.ToString("dd.MM.yy'/'HH:mm")));
			strSubHeader.Append("View: ");
			strSubHeader.Append(this.ucView1.Viewer.CurrentView);
			strSubHeader.Append(" (Flights: ").Append(iTotalFlights.ToString());
			if (this.ucView1.Viewer.Arrival)
				strSubHeader.Append("/ARR:").Append(iTotalFlights.ToString())
					.Append(" /DEP:0)"); 
			else
				strSubHeader.Append("/ARR:0")
					.Append(" /DEP:").Append(iTotalFlights.ToString()).Append(")"); 

			//rptA3_Landscape rpt = new rptA3_Landscape(tabResult, "Statistics - Arrival / Departure", strSubHeader.ToString(), "", 8);
			prtFIPS_Landscape rpt = new prtFIPS_Landscape(tabResult,frmMain.strPrefixVal + "Statistics - Load Pax Arrival/Departure",strSubHeader.ToString(),"",8);
			rpt.TextBoxCanGrow = false;
			frmPrintPreview frm = new frmPrintPreview(rpt);
			frm.Show();

		}

		/// <summary>
		/// Validates the user entry for the following fields:
		/// 1. FROM and TO fields
		/// 
		/// Displays the error message.
		/// </summary>
		/// <returns>TRUE - If entries are correct, else FALSE</returns>
		private bool validEntry() 
		{
			string strRet = "";
			int ilErrorCount = 1;

			if (this.ucView1.Viewer.CurrentView == "")
			{
				strRet += ilErrorCount.ToString() +  ". A view must be selected!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.PeriodFrom >= this.ucView1.Viewer.PeriodTo)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.Arrival == false && this.ucView1.Viewer.Departure == false)
			{
				strRet += ilErrorCount.ToString() +  ". Either Arrival or Departure must be used!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.Arrival == true && this.ucView1.Viewer.Departure == true)
			{
				strRet += ilErrorCount.ToString() +  ". Either Arrival or Departure must be used!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.Rotation == true)
			{
				strRet += ilErrorCount.ToString() +  ". Rotation must not be used!\n";
				ilErrorCount++;
			}

			if (strRet.Length > 0)
			{
				MessageBox.Show(this, strRet);
				return false;
			}
			else
			{
				return true;
			}

		}
		/// <summary>
		/// Function handler for the radio button selection change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
	
		private void radioButtonArrival_CheckedChanged(object sender, System.EventArgs e)
		{
			tabResult.HeaderString = strTabHeaderArrival;
			tabResult.LogicalFieldList = strLogicalFieldsArrival;
			tabResult.HeaderLengthString = strTabHeaderLensArrival;
			if(tabResult.GetLineCount() != 0)
			{
				tabResult.ResetContent();
				PrepareReportDataArrival();
			}
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
		}
		/// <summary>
		/// function handler for the radio button selection change
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void radioButtonDeparture_CheckedChanged(object sender, System.EventArgs e)
		{
			tabResult.HeaderString = strTabHeaderDeparture;
			tabResult.LogicalFieldList = strLogicalFieldsDeparture;
			tabResult.HeaderLengthString = strTabHeaderLensDeparture;
			if(tabResult.GetLineCount() != 0)
			{
				tabResult.ResetContent();
				PrepareReportDataDeparture();
			}
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
		}
		/// <summary>
		/// Function handler for the Load button 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void button1_Click(object sender, System.EventArgs e)
		{
			if(validEntry())
			{
				this.Cursor = Cursors.WaitCursor;
				this.InitTab();
				LoadReportData();
				if(this.ucView1.Viewer.Arrival == false)
					PrepareReportDataDeparture();
				else
					PrepareReportDataArrival();
				this.Cursor = Cursors.Arrow;
			}
		}
		/// <summary>
		/// Function handler for the Print Preview button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void buttonPrintPView_Click(object sender, System.EventArgs e)
		{
			RunReport();
		}
		/// <summary>
		/// function handler for the Load and Preview button
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			if(validEntry())
			{
				this.Cursor = Cursors.WaitCursor;
				this.InitTab();
				LoadReportData();
				if(this.ucView1.Viewer.Arrival == false)
					PrepareReportDataDeparture();
				else
					PrepareReportDataArrival();
				RunReport();
				this.Cursor = Cursors.Arrow;
			}
		}
		/// <summary>
		/// Event Handler for the View usercontrol button
		/// </summary>
		/// <param name="view"></param>
		private void ucView1_viewEvent(string view)
		{
			button1_Click(this,null);
		}
		/// <summary>
		/// function handler for the double click of the LButton mouse click on the column header of the table.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tabResult_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if( tabResult.SortOrderASC == true)
					{
						tabResult.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						tabResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabResult.Sort( e.colNo.ToString(), true, true);
				}
				tabResult.Refresh();
			}		
		}

		private void cbLoadInfo_CheckedChanged(object sender, System.EventArgs e)
		{
			tabResult.ResetContent();
			tabResult.Refresh();
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
		}
	}
}
