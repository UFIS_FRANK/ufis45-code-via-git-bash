using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Ufis.Data;
using Ufis.Utils;
using UserControls;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmCancelled_Flights.
	/// </summary>
	public class frmAbnormal_Flights : System.Windows.Forms.Form
	{
		#region _My Members

		/// <summary>
		/// The where statement: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		private string strWhereRawA =  "((STOA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))) OR (TIFA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))))";

		private string strWhereRawD =  "((STOD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('D','B'))) OR (TIFD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('D','B'))))" ;

		private string strWhereRawB = string.Format("({0} OR {1})","((STOA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))) OR (TIFA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))))","((STOD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('D','B'))) OR (TIFD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('D','B'))))");
		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes
		/// </summary>
		private string strLogicalFields = "24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0";
		/// <summary>
		/// The header columns, which must be used for the report output
		/// </summary>
		private string strTabHeader =     "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y";
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// </summary>
		private string strTabHeaderLens = "26,26,26,26,26,26,26,26,26,26,26,26,26,26,26,26,26,26,26,26,26,26,26,26,26";
		/// <summary>
		/// String for empty line generation in the tab control.
		/// </summary>
		private string strEmptyLine = ",,,,,,,,,,,,,,,,,,,,,,,,";
		/// <summary>
		/// The one and only IDatabase object.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
		/// <summary>
		/// ITable object for the DENTAB (Delay codes)
		/// </summary>
		private ITable myDEN;
		/// <summary>
		/// ITable object for the VIPTAB (Very important persons)
		/// </summary>
		private ITable myVIP;
		/// <summary>
		/// Total counters
		/// </summary>
		private int[] imTotalCounter = new int[25];
		/// <summary>
		/// perDay counters
		/// </summary>
		private int[] imPerDayCounter = new int[25];

		#endregion _My Members

		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Panel panelTab;
		private AxTABLib.AxTAB tabResult;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Button btnPrintPreview;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private UserControls.ucAbnormalFlights ucAbnormalFlights1;
		private System.Windows.Forms.Panel panelHeader;
		private System.Windows.Forms.NumericUpDown txtDelay;
		private System.Windows.Forms.Label label5;
		private UserControls.ucView ucView1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmAbnormal_Flights()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			myDB = UT.GetMemDB();
			this.ucView1.Report = "AbnormalFlights";
			this.ucView1.viewEvent +=new UserControls.ucView.ViewChangedEvent(ucView1_viewEvent);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmAbnormal_Flights));
			this.panelTop = new System.Windows.Forms.Panel();
			this.lblProgress = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.btnPrintPreview = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.txtDelay = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.ucAbnormalFlights1 = new UserControls.ucAbnormalFlights();
			this.panelBody = new System.Windows.Forms.Panel();
			this.panelTab = new System.Windows.Forms.Panel();
			this.tabResult = new AxTABLib.AxTAB();
			this.lblResults = new System.Windows.Forms.Label();
			this.panelHeader = new System.Windows.Forms.Panel();
			this.ucView1 = new UserControls.ucView();
			this.panelTop.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtDelay)).BeginInit();
			this.panelBody.SuspendLayout();
			this.panelTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			this.SuspendLayout();
			// 
			// panelTop
			// 
			this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelTop.Controls.Add(this.ucView1);
			this.panelTop.Controls.Add(this.lblProgress);
			this.panelTop.Controls.Add(this.progressBar1);
			this.panelTop.Controls.Add(this.btnLoadPrint);
			this.panelTop.Controls.Add(this.btnPrintPreview);
			this.panelTop.Controls.Add(this.btnCancel);
			this.panelTop.Controls.Add(this.btnOK);
			this.panelTop.Controls.Add(this.txtDelay);
			this.panelTop.Controls.Add(this.label5);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(636, 120);
			this.panelTop.TabIndex = 0;
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(412, 72);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 22;
			// 
			// progressBar1
			// 
			this.progressBar1.Location = new System.Drawing.Point(412, 88);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 6;
			this.progressBar1.Visible = false;
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(244, 88);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 4;
			this.btnLoadPrint.Text = "Loa&d + Print";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// btnPrintPreview
			// 
			this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPrintPreview.Location = new System.Drawing.Point(168, 88);
			this.btnPrintPreview.Name = "btnPrintPreview";
			this.btnPrintPreview.TabIndex = 3;
			this.btnPrintPreview.Text = "&Print Preview";
			this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(320, 88);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 5;
			this.btnCancel.Text = "&Close";
			// 
			// btnOK
			// 
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnOK.Location = new System.Drawing.Point(92, 88);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 2;
			this.btnOK.Text = "&Load Data";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// txtDelay
			// 
			this.txtDelay.Location = new System.Drawing.Point(232, 52);
			this.txtDelay.Maximum = new System.Decimal(new int[] {
																	 1000,
																	 0,
																	 0,
																	 0});
			this.txtDelay.Name = "txtDelay";
			this.txtDelay.Size = new System.Drawing.Size(100, 20);
			this.txtDelay.TabIndex = 18;
			this.txtDelay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.txtDelay.Value = new System.Decimal(new int[] {
																   10,
																   0,
																   0,
																   0});
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(148, 52);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(80, 16);
			this.label5.TabIndex = 19;
			this.label5.Text = "Delay Time >=";
			// 
			// ucAbnormalFlights1
			// 
			this.ucAbnormalFlights1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.ucAbnormalFlights1.Location = new System.Drawing.Point(0, 16);
			this.ucAbnormalFlights1.Name = "ucAbnormalFlights1";
			this.ucAbnormalFlights1.Size = new System.Drawing.Size(636, 464);
			this.ucAbnormalFlights1.TabIndex = 23;
			// 
			// panelBody
			// 
			this.panelBody.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelBody.Controls.Add(this.panelTab);
			this.panelBody.Controls.Add(this.ucAbnormalFlights1);
			this.panelBody.Controls.Add(this.lblResults);
			this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelBody.Location = new System.Drawing.Point(0, 120);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(636, 630);
			this.panelBody.TabIndex = 1;
			// 
			// panelTab
			// 
			this.panelTab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.panelTab.Controls.Add(this.tabResult);
			this.panelTab.Location = new System.Drawing.Point(0, 270);
			this.panelTab.Name = "panelTab";
			this.panelTab.Size = new System.Drawing.Size(636, 398);
			this.panelTab.TabIndex = 2;
			// 
			// tabResult
			// 
			this.tabResult.ContainingControl = this;
			this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabResult.Location = new System.Drawing.Point(0, 0);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(636, 398);
			this.tabResult.TabIndex = 0;
			this.tabResult.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabResult_SendLButtonDblClick);
			// 
			// lblResults
			// 
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 0);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(636, 16);
			this.lblResults.TabIndex = 1;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panelHeader
			// 
			this.panelHeader.Location = new System.Drawing.Point(0, 0);
			this.panelHeader.Name = "panelHeader";
			this.panelHeader.TabIndex = 0;
			// 
			// ucView1
			// 
			this.ucView1.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.ucView1.Location = new System.Drawing.Point(24, 4);
			this.ucView1.Name = "ucView1";
			this.ucView1.Report = "";
			this.ucView1.Size = new System.Drawing.Size(304, 40);
			this.ucView1.TabIndex = 23;
			// 
			// frmAbnormal_Flights
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(636, 750);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.panelTop);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmAbnormal_Flights";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Statistics: Abnormal flights at Pudong Airport ...";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.frmAbnormal_Flights_Closing);
			this.SizeChanged += new System.EventHandler(this.frmAbnormal_Flights_SizeChanged);
			this.Load += new System.EventHandler(this.frmFlightByNatureCode_Load);
			this.panelTop.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.txtDelay)).EndInit();
			this.panelBody.ResumeLayout(false);
			this.panelTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
		
		/// <summary>
		/// Load the form. Prepare and init all controls.
		/// </summary>
		/// <param name="sender">Form.</param>
		/// <param name="e">Event arguments.</param>
		private void frmFlightByNatureCode_Load(object sender, System.EventArgs e)
		{
			InitTimePickers();
			InitTab();
			PrepareFilterData();
		}
		/// <summary>
		/// initializes the From/to time control with the current day in
		/// the custom format "dd.MM.yyyy - HH:mm".
		/// </summary>
		private void InitTimePickers()
		{
		}
		/// <summary>
		/// Initializes the tab controll.
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(false);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;

			tabResult.HeaderString = strTabHeader;
			tabResult.LogicalFieldList = strLogicalFields;
			tabResult.HeaderLengthString = strTabHeaderLens;
			tabResult.MainHeaderOnly = true;
			tabResult.MainHeader = false;

			frmAbnormal_Flights_SizeChanged(this,null);

		}
		/// <summary>
		/// Loads and prepare the data necessary for the filter. 
		/// In this case loads the nature codes and puts the
		/// data into the combobox.
		/// </summary>
		private void PrepareFilterData()
		{
			// load delay codes
			myDB.Unbind("DEN");
			myDEN = myDB.Bind("DEN", "DEN", "ALC3,DECA,DECN,DENA,URNO,VAFR,VATO,RCOL", "3,2,2,75,10,14,14,2", "ALC3,DECA,DECN,DENA,URNO,VAFR,VATO,RCOL");
			myDEN.Clear();
			myDEN.Load("");
			myDEN.CreateIndex("DECA","DECA");
			myDEN.CreateIndex("DECN","DECN");
			myDEN.CreateIndex("URNO","URNO");

			// load very important person data
			myDB.Unbind("VIP");
			myVIP = myDB.Bind("VIP","VIP","FLNU,NOGR,URNO","10,3,10","FLNU,NOGR,URNO");
			myVIP.Clear();
			myVIP.Load("");
			myVIP.CreateIndex("FLNU","FLNU");
			myVIP.CreateIndex("URNO","URNO");

		}
		/// <summary>
		/// Runs the data queries according to the reports needs and puts
		///	Calls the data prepare method and puts the prepared data into
		///	the tab control. After this is finished it calls the reports
		///	by sending the tab and the headers to the generic report.
		/// </summary>
		/// <param name="sender">Originator object of the call</param>
		/// <param name="e">Event arguments</param>
		private void btnOK_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			PrepareReportData();
			//RunReport();
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// Validates the user entry for nonsense.
		/// </summary>
		/// <returns></returns>
		private string ValidateUserEntry()
		{
			string strRet = "";
			int ilErrorCount = 1;

			if (this.ucView1.Viewer.CurrentView == "")
			{
				strRet += ilErrorCount.ToString() +  ". A view must be selected!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.PeriodFrom >= this.ucView1.Viewer.PeriodTo)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}

			if (this.ucView1.Viewer.Rotation == true)
			{
				strRet += ilErrorCount.ToString() +  ". Rotation must not be selected!\n";
				ilErrorCount++;
			}

			return strRet;
		}
		/// <summary>
		/// Loads all necessary tables with filter criteria from the 
		/// database ==> to memoryDB.
		/// </summary>
		private void LoadReportData()
		{
			string strTmpAFTWhere;
			//Clear the tab
			tabResult.ResetContent();

			// In the first step change the times to UTC if this is
			// necessary.
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			string strFtyp = this.ucView1.Viewer.FlightTypes;
			datFrom = this.ucView1.Viewer.PeriodFrom;
			datTo   = this.ucView1.Viewer.PeriodTo;
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(this.ucView1.Viewer.PeriodFrom);
				datTo   = UT.LocalToUtc(this.ucView1.Viewer.PeriodTo);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(this.ucView1.Viewer.PeriodFrom);
				strDateTo = UT.DateTimeToCeda(this.ucView1.Viewer.PeriodTo);
			}

			//Load the data from AFTTAB
			myDB.Unbind("AFT");
			myAFT = myDB.Bind("AFT", "AFT", "URNO,FLNO,ADID,ORG3,DES3,STOA,STOD,TIFA,TIFD,ONBL,OFBL,ACT3,DCD1,DCD2,DTD1,DTD2,TTYP,SDAY", "10,12,2,3,3,14,14,14,14,14,14,3,2,2,4,4,5,14", "URNO,FLNO,ADID,ORG3,DES3,STOA,STOD,TIFA,TIFD,ONBL,OFBL,ACT3,DCD1,DCD2,DTD1,DTD2,TTYP");
			myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,ONBL,OFBL,SDAY";
			myAFT.Clear();
			myAFT.TimeFieldsInitiallyInUtc = true;
			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);
			progressBar1.Value = 0;
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			lblProgress.Text = "Loading Flight Data";
			lblProgress.Refresh();
			progressBar1.Show();
			int loopCnt = 1;
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				//patch the where statement according to the user's entry
				strTmpAFTWhere = "WHERE ";
				// check on code share enabled
				if (this.ucView1.Viewer.IncludeCodeShare)
				{
					string strTmpWhere1 = "";
					//patch the where statement according to the user's entry
					if (this.ucView1.Viewer.Arrival && this.ucView1.Viewer.Departure)
						strTmpWhere1 = strWhereRawB;
					else if (this.ucView1.Viewer.Arrival)
						strTmpWhere1 = strWhereRawA;
					else if (this.ucView1.Viewer.Departure)
						strTmpWhere1 = strWhereRawD;
					strTmpWhere1+= this.ucView1.Viewer.AdditionalSelection(0);
					strTmpWhere1+= this.ucView1.Viewer.GeneralFilter;

					string strTmpWhere2 = "";
					//patch the where statement according to the user's entry
					strTmpWhere2+= this.ucView1.Viewer.AdditionalSelection(1);

					strTmpAFTWhere = string.Format("WHERE ({0}) AND ({1})",strTmpWhere1,strTmpWhere2);
				}
				else
				{
					//patch the where statement according to the user's entry
					if (this.ucView1.Viewer.Arrival && this.ucView1.Viewer.Departure)
						strTmpAFTWhere += strWhereRawB;
					else if (this.ucView1.Viewer.Arrival)
						strTmpAFTWhere += strWhereRawA;
					else if (this.ucView1.Viewer.Departure)
						strTmpAFTWhere += strWhereRawD;
					strTmpAFTWhere+= this.ucView1.Viewer.AdditionalSelection(0);
					strTmpAFTWhere+= this.ucView1.Viewer.GeneralFilter;

				}

				strTmpAFTWhere = strTmpAFTWhere.Replace("@@FROM", strDateFrom);
				strTmpAFTWhere = strTmpAFTWhere.Replace("@@TO", strDateTo );

				if (this.ucView1.Viewer.Rotation)
				{
					strTmpAFTWhere += "[ROTATIONS]";
					if (strFtyp.Length > 0)
						strTmpAFTWhere += string.Format("[FILTER=FTYP IN ({0})]",strFtyp);
				}

				//Load the data from AFTTAB
				myAFT.Load(strTmpAFTWhere);
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);
			lblProgress.Text = "";
			progressBar1.Hide();

			for (int i = 0; i < myAFT.Count; i++)
			{
				switch(myAFT[i]["ADID"])
				{
					case "A":
						myAFT[i]["SDAY"] = myAFT[i]["ONBL"];
						break;
					case "D":
						myAFT[i]["SDAY"] = myAFT[i]["OFBL"];
						break;
					case "B":
						myAFT[i]["SDAY"] = myAFT[i]["ONBL"];
						break;
				}
			}

			myAFT.Sort("SDAY",true);

			myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;

		}
		/// <summary>
		/// Prepare the loaded data according to the report's needs.
		/// Prepare line by line the data a logical records and put
		/// the data into the Tab control.
		/// </summary>
		private void PrepareReportData()
		{
			int ilLine = 0;
			for (int i = 0; i < 25; i++)
			{
				this.imTotalCounter[i]  = 0;
				this.imPerDayCounter[i] = 0;
			}
			
			DateTime lastDate = new DateTime(0);

			for(int i = 0; i < myAFT.Count; i++)
			{
				DateTime currDate = UT.CedaFullDateToDateTime(myAFT[i]["SDAY"]);

				// initialize per day counters
				if (i != 0 && lastDate.Date != currDate.Date)
				{
					tabResult.InsertTextLine(strEmptyLine, false);
					ilLine = tabResult.GetLineCount() - 1;
					for (int j = 0; j < 24; j++)
					{
						if (j == 17 || imPerDayCounter[j] != 0)
						{
							if (j == 17)	// nominal flights
							{
								if (imPerDayCounter[21] > 0)
								{
									float nominal = 1.0F - (float)imPerDayCounter[20]  / imPerDayCounter[21];
									tabResult.SetFieldValues(ilLine,j.ToString(),nominal.ToString("F2"));				
								}
							}
							else
							{
								tabResult.SetFieldValues(ilLine,j.ToString(),imPerDayCounter[j].ToString());
							}
						}

						imPerDayCounter[j] = 0;
					}

					tabResult.SetFieldValues(ilLine,24.ToString(),lastDate.ToString("dd'/'MM"));
				}

				lastDate = currDate;

				// check on very important pesons
				IRow[] rows = myVIP.RowsByIndexValue("FLNU",myAFT[i]["URNO"]);
				if (rows != null && rows.Length > 0)
				{
					for (int j = 0; j < rows.Length; j++)
					{
						int count = int.Parse(rows[j]["NOGR"]);
						imPerDayCounter[0] += count;
						imTotalCounter[0] += count;
					}
				}

				if (this.FlightIsArrivalOrBoth(myAFT[i]))
				{
					CheckArrivalFlight(myAFT[i]);
				}

				if (this.FlightIsDepartureOrBoth(myAFT[i]))
				{

					CheckDepartureFlight(myAFT[i]);

				}
			}

			// add total count
			tabResult.InsertTextLine(strEmptyLine, false);
			ilLine = tabResult.GetLineCount() - 1;
			for (int j = 0; j < 24; j++)
			{
				if (j == 17 || imTotalCounter[j] != 0)
				{
					if (j == 17)	// nominal flight count
					{
						if (imTotalCounter[21] > 0)
						{
							float nominal = 1.0F - (float)imTotalCounter[20]  / imTotalCounter[21];
							tabResult.SetFieldValues(ilLine,j.ToString(),nominal.ToString("F2"));				
						}
					}
					else
					{
						tabResult.SetFieldValues(ilLine,j.ToString(),imTotalCounter[j].ToString());
					}
				}
			}

			tabResult.SetFieldValues(ilLine,24.ToString(),"Total");
			
			tabResult.Refresh();
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
		}
		/// <summary>
		/// Calls the generic report. The contructor must have the Tab 
		/// control and the reports headers as well as the types for
		/// each column to enable the reports to transform data, e.g.
		/// the data/time formatting. The lengths of the columns are
		/// dynamically read out of the tab control's header.
		/// </summary>
		private void RunReport()
		{
			string strSubHeader = "";
			strSubHeader = "From: " + this.ucView1.Viewer.PeriodFrom.ToString("dd.MM.yy'/'HH:mm") + " to " + this.ucView1.Viewer.PeriodTo.ToString("dd.MM.yy'/'HH:mm");
			strSubHeader += " View: " + this.ucView1.Viewer.CurrentView;
			strSubHeader += "       (Flights: " + imTotalCounter[17].ToString();
			strSubHeader += " ARR: " + imTotalCounter[23].ToString();
			strSubHeader += " DEP: " + imTotalCounter[22].ToString() + ")";
			prtFIPS_AbnormalFlights rpt = new prtFIPS_AbnormalFlights(tabResult, "Report of the abnormal flights at Pudong Airport", strSubHeader, "", 10);
			//rpt.TextBoxCanGrow = false;
			frmPrintPreview frm = new frmPrintPreview(rpt);
			frm.Show();
		}
		/// <summary>
		/// Start the print preview without data selection.
		/// This can be used to adjust the column widths and
		/// the sorting of the tab before printing.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			RunReport();
		}
		/// <summary>
		/// Loads data and dirctly calls the print preview.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			PrepareReportData();
			RunReport();
			this.Cursor = Cursors.Arrow;
		}
		/// <summary>
		/// Sent by the tab control when the user double click.
		/// This is necessary for sorting.
		/// </summary>
		/// <param name="sender">The Tab control</param>
		/// <param name="e">Double click event parameters.</param>
		private void tabResult_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if( tabResult.SortOrderASC == true)
					{
						tabResult.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						tabResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabResult.Sort( e.colNo.ToString(), true, true);
				}
				tabResult.Refresh();
			}
		}

		private void frmAbnormal_Flights_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			myDB.Unbind("AFT");
		}

		private void label1_Click(object sender, System.EventArgs e)
		{
		
		}

		private void frmAbnormal_Flights_SizeChanged(object sender, System.EventArgs e)
		{
			int size = this.ClientRectangle.Width / 25;
			string olHeaderLength = "";
			for (int i = 0; i < 25; i++)
			{
				if (olHeaderLength.Length == 0)
					olHeaderLength += size.ToString();
				else
				{
					olHeaderLength += ",";
					olHeaderLength += size.ToString();
				}
			}
			tabResult.HeaderLengthString = olHeaderLength;
			tabResult.RedrawTab();
		
		}

		private int GetReportColumnFromDelayCode(string code,string alc3)
		{
			int column = -1;
			try
			{
				IRow[]	rows = null;
				IRow	alc3Dependend = null;
				IRow	alc3Independend = null;

				// check on valid delay code
				if (code.Length > 0)
				{
					// check for special delay code for airline
					if (Char.IsNumber(code,1))
					{
						rows = myDEN.RowsByIndexValue("DECN",code);						
					}
					else
					{
						rows = myDEN.RowsByIndexValue("DECA",code);						
					}
						
					for (int j = 0; rows != null && j < rows.Length; j++)
					{
						if (rows[j]["ALC3"] == alc3)
							alc3Dependend = rows[j];
						else if (rows[j]["ALC3"] == "")
							alc3Independend = rows[j];
					}
					
					if (alc3Dependend != null)
					{
						column = int.Parse(alc3Dependend["RCOL"]);
					}
					else if (alc3Independend != null)
					{
						column = int.Parse(alc3Independend["RCOL"]);
					}
				}
			}
			catch(Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.Message);
			}

			return column;
		}

		private bool ArrivalFlightIsDelayed(IRow row)
		{
			if (row["STOA"] != "" && row["ONBL"] != "" && row["STOA"] != row["ONBL"])
			{
				DateTime olStoa = UT.CedaFullDateToDateTime(row["STOA"]);
				DateTime olOnbl = UT.CedaFullDateToDateTime(row["ONBL"]);
				TimeSpan olDelay = olOnbl - olStoa;
				long llTxtDelay = Convert.ToInt64(this.txtDelay.Text);
				if (olDelay.TotalMinutes >= llTxtDelay)
				{
					return true;
				}
			}

			return false;
		}

		private bool DepartureFlightIsDelayed(IRow row)
		{
			if (row["STOD"] != "" && row["OFBL"] != "" && row["STOD"] != row["OFBL"])
			{
				DateTime olStod = UT.CedaFullDateToDateTime(row["STOD"]);
				DateTime olOfbl = UT.CedaFullDateToDateTime(row["OFBL"]);
				TimeSpan olDelay = olOfbl - olStod;
				long llTxtDelay = Convert.ToInt64(this.txtDelay.Text);
				if (olDelay.TotalMinutes >= llTxtDelay)
				{
					return true;
				}
			}

			return false;
		}

		private bool FlightIsArrivalOrBoth(IRow row)
		{
			return row["ADID"] == "A" || row["ADID"] == "B";
		}

		private bool FlightIsArrival(IRow row)
		{
			return row["ADID"] == "A";
		}

		private bool FlightIsDepartureOrBoth(IRow row)
		{
			return row["ADID"] == "D" || row["ADID"] == "B";
		}

		private bool FlightIsDeparture(IRow row)
		{
			return row["ADID"] == "D";
		}

		private bool FlightIsCancelled(IRow row)
		{
			return row["FTYP"] == "X" && (row["ADID"] == "D" || row["ADID"] == "B");
		}

		private bool FlightIsTraining(IRow row)
		{
			return row["TTYP"] == "K/L";
		}

		private bool FlightIsSpecial(IRow row)
		{
			return row["TTYP"] == "B/W";
		}

		private void CheckArrivalFlight(IRow row)
		{
			// check on training flight
			if (this.FlightIsTraining(row))
			{
				++imPerDayCounter[19];
				++imTotalCounter[19];
			}

			// check on special flight
			if (this.FlightIsSpecial(row))
			{
				++imPerDayCounter[1];
				++imTotalCounter[1];
			}

			// arrival flight counters
			++imPerDayCounter[23];
			++imTotalCounter[23];

		}

		private void CheckDepartureFlight(IRow row)
		{
			// check on W/Z nature departure flights
			if (row["TTYP"] == "W/Z")
			{
				++imPerDayCounter[21];
				++imTotalCounter[21];
			}

			// check on training flight
			if (this.FlightIsTraining(row))
			{
				++imPerDayCounter[19];
				++imTotalCounter[19];
			}

			// check on special flight
			if (this.FlightIsSpecial(row))
			{
				++imPerDayCounter[1];
				++imTotalCounter[1];
			}

			// check on cancelled flight
			if (this.FlightIsCancelled(row))
			{
				++imPerDayCounter[18];
				++imTotalCounter[18];
			}
			// check if flight is delayed or not
			else if (this.DepartureFlightIsDelayed(row))
			{
				if (row["TTYP"] == "W/Z")
				{
					++imPerDayCounter[20];
					++imTotalCounter[20];
				}

				bool assignedToColumn = false;
				// check on valid delay code
				int ind = this.GetReportColumnFromDelayCode(row["DCD1"],row["ALC3"]);
				if (ind >= 0)
				{
					assignedToColumn = true;
					++imPerDayCounter[ind];
					++imTotalCounter[ind];
				}

				// check on valid delay code
				ind = this.GetReportColumnFromDelayCode(row["DCD2"],row["ALC3"]);
				if (ind >= 0)
				{
					assignedToColumn = true;
					++imPerDayCounter[ind];
					++imTotalCounter[ind];
				}

				// delay codes not found or invalid -> "Due to other reasons" section
				if (assignedToColumn == false)
				{
					++imPerDayCounter[2];
					++imTotalCounter[2];
				}
			}

			// departure flight counter
			++imPerDayCounter[22];
			++imTotalCounter[22];
		}
		private void ucView1_viewEvent(string view)
		{
			btnOK_Click(this,null);
		}
	}
}
