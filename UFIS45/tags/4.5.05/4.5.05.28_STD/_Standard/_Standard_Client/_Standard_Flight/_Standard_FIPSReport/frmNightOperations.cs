using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Utils;
using Ufis.Data;
using System.Text.RegularExpressions;

namespace FIPS_Reports
{
    	/// <summary>
	/// Summary description for frmNightOperations.
	/// </summary>
	public class frmNightOperations : System.Windows.Forms.Form
	{
		#region _My Members
		/// <summary>
		/// Filter condition for AFT load. 
		/// The current implementation doesn't display the rotational flights. To include the rotational 
		/// flight add FTYP and also include ADID type as part of the Arrival / Departure display
		/// 
		/// </summary>

        private string strWhereRaw = "WHERE ((STOA BETWEEN '@@FROM' AND '@@TO' AND DES3='@@HOPO') OR " +
                                     "(STOD BETWEEN '@@FROM' AND '@@TO' AND ORG3='@@HOPO')) AND " +
                                     "(FTYP IN (@@FTYP))";

        private string strWhereONBLOFBLRaw = "WHERE ((ONBL BETWEEN '@@FROM' AND '@@TO' AND DES3='@@HOPO') " +
                                                "OR  (OFBL BETWEEN '@@FROM' AND '@@TO' AND ORG3='@@HOPO')) AND " +
                                                 "(FTYP IN (@@FTYP))";

        private string strWhereLANDAIRBRaw = "WHERE ((LAND BETWEEN '@@FROM' AND '@@TO' AND DES3='@@HOPO') " +
                                                "OR  (AIRB BETWEEN '@@FROM' AND '@@TO' AND ORG3='@@HOPO')) AND " +
                                                 "(FTYP IN (@@FTYP))";

        private string strNatWhere = " AND (TTYP='@@NAT')";
        private string strORGDESWhere = " AND (ORG3='@@APT' OR DES3='@@APT')";

		/// <summary>
		/// Filter condition for CCA load
		/// </summary>
		private string strCCAWhereRaw = "where FLNU in (@@FLNU)";
		/// <summary>
		/// Filter condition for APX load
		/// </summary>
		private string strAPXWhereRaw = "where FLNU in (@@FLNU)";
		/// <summary>
		/// Filter condition for LOA load
		/// </summary>
		private string strLOAWhereRaw = "where FLNU in (@@FLNU) "+
										"AND DSSN IN ('USR','LDM','MVT','KRI') "+
										"AND TYPE IN ('PAX','PAD','LOA') "+
										"AND APC3=' '";

		/// <summary>
		/// Filter condition for STEV
		/// </summary>
		private string strTerminalWhere = " AND (STEV = '@@STEV')";

		/// <summary>
		/// The where statement for the dedicated chutes.
		/// </summary>
		private string strDedicatedCHAWhere = "WHERE RURN IN (@@RURN) AND LNAM<>' ' AND DES3 <> '@@@'";
		
		/// <summary>
		/// Single object of the internal memory object.
		/// </summary>
		private IDatabase myDB;

		/// <summary>
		/// Heading of the columns as it needs to be in the report output - 21 Columns
		/// </summary>
		//private string strTabHeaderArrival = "Flight ,ORG ,STA ,ATA ,A/C ,REG ,POS ,Gate ,Belt ,F ,C ,Y ,Inf ,Total ,ID ,TR ,TRF ,BAG ,CGO ,MAIL ,Total";
		private string strTabHeaderArrival = "Flight ,ID ,J , STA, A, Na ,S ,ORG ,VIA ,POS ,T ,Land/Onbl ,Belt , I ,Type ,Regn ,NFES ";
		/// <summary>
		/// The logical mapping of the columns
		/// </summary>
		//private string strLogicalFieldsArrival = "FLNO,ORG,STOA,ATA,AC_TYP,REG,POS,GATE,BELT,PAX_F,PAX_C,PAX_Y,PAX_INF,P_TTL,ID,TR,TRF,BAG,CGO,MAIL,TTL";
        //OLDprivate string strLogicalFieldsArrival = "FLNO,ORG,STOA,ATA,AC_TYP,REG,POS,GATE,BELT,TERM";
        private string strLogicalFieldsArrival = "FLNO,FLTI,JFNO,STOA,DAY,TTYP,STYP,ORG,VIA,POS,TERM,�LAND_ONBL,BLT,ISRE,ACT3,REGN,NFES";

		/// <summary>
		/// The length of each column
		/// </summary>
        //private string strTabHeaderLensArrival = "60,30,50,50,40,70,40,50,80,20";
        private string strTabHeaderLensArrival = "60,20,20,50,20,40,20,40,20,40,20,110,30,20,40,70,50";
		
		/// <summary>
		/// The length of each column for excel export
		/// </summary>
        private string strExcelTabHeaderLensArrival = "60,20,20,50,20,40,20,30,20,30,20,110,30,20,40,70,50";

		/// <summary>
		/// Heading of the columns as it needs to be in the report output - 21 Columns
		/// </summary>
       
        private string strTabHeaderDeparture = "Flight ,ID ,J ,STD ,A ,Na ,S ,DES ,VIA ,POS ,T ,Gate ,Ofbl/Airb ,CKI ,Chute ,I ,Type ,Regn ,NFES";

		/// <summary>
		/// The logical mapping of the columns
		/// </summary>
        //private string strLogicalFieldsDeparture = "FLNO,DES,STOD,ATD,AC_TYP,REG,CKIN,GATE,POS,TERM,CHUTES_FROM_TO";
        private string strLogicalFieldsDeparture = "FLNO,FLTI,JFNO,STD,DAY,TTYP,STYP,DES,VIA,POS,TERM,GAT,OFBL_AIRB,CKIN,CHUTES_FROM_TO,ISRE,ACT3,REGN,NFES";
		
		/// <summary>
		/// The length of each column
		/// </summary>
        private String strTabHeaderLensDeparture = "60,20,20,50,20,40,20,40,20,40,20,30,110,90,90,20,40,70,50";
		
		/// <summary>
		/// The length of each column for excel export 
		/// </summary>
        private String strExcelTabHeaderLensDeparture = "60,20,20,50,20,40,20,30,20,30,20,30,90,90,110,20,40,70,50";

		/// <summary>
		/// Instance of the table
		/// </summary>
        ITable myAFT;
        ITable myNAT;
        ITable myCCA;
		ITable myAPX;
		ITable myLOA;
		/// <summary>
        /// 
        private string strNatureCode = "";
        /// <summary>
        /// Airline 2-3 letter code
        /// </summary>
        private string strAirportCode = "";
        /// <summary>
        /// 
        /// 
		/// ITable object for the chutes table CHATAB
		/// </summary>
		private ITable myCHA;
		/// <summary>
		/// Count of total flights displayed on the list
		/// </summary>
		private int iTotalFlights = 0;
		/// <summary>
		/// Datetime format for excel export
		/// </summary>
		private string strExcelDateTimeFormat = "";
		/// <summary>
		/// Datetime format for report
		/// </summary>
		private string strReportDateTimeFormat = "dd'/'HH:mm";
		/// <summary>
		/// Datetime format for table
		/// </summary>
		private string strDateDisplayFormat = "";
		/// <summary>
		/// Boolean for mouse status with respect to tabResult control
		/// </summary>
		private bool bmMouseInsideTabControl = false;
		/// <summary>
		/// Report Header
		/// </summary>
		private string strReportHeader = "";
		/// <summary>
		/// Report Sub Header
		/// </summary>
		private string strReportSubHeader = "";
		/// <summary>
		/// Report Object
		/// </summary>
		private DataDynamics.ActiveReports.ActiveReport3 activeReport = null;
		/// <summary>
		/// Boolean to suggest if "Chutes from/to" column should be visible or not
		/// </summary>
		private bool bmChuteDisplay = false;
		/// <summary>
		/// Boolean for showing stev
		/// </summary>
		private bool bmShowStev = false;

        private bool TimeFrameIsWithinOneDay = false;


		#endregion _My Members

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		
		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Panel panelTab;
		private AxTABLib.AxTAB tabResult;
		private AxTABLib.AxTAB tabExcelResult;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Button btnPrintPreview;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.DateTimePicker dtTo;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.RadioButton rbArrival;
		private System.Windows.Forms.Button buttonHelp;
		private System.Windows.Forms.TextBox txtTerminal;
		private System.Windows.Forms.Label lblTerminal;
        private RadioButton radioButtonLANDAIRB;
        private RadioButton radioButtonONBLOFBL;
        private RadioButton radioButtonSTASTD;
        private TextBox txtAirport;
        private ComboBox cbNatures;
        private Label label4;
        private Label label3;
        private GroupBox groupBox2;
        private GroupBox groupBox1;
        private RadioButton radioButton1;
        private RadioButton radioButton3;
        private RadioButton radioButton2;
        private CheckBox checkBoxFtypR;
        private CheckBox checkBoxFtypD;
        private CheckBox checkBoxFtypZ;
        private CheckBox checkBoxFtypO;
        private GroupBox groupBox3;
        private DateTimePicker timeTo;
        private Label label7;
        private Label label6;
        private DateTimePicker timeFrom;
		private System.Windows.Forms.RadioButton rbDeparture;

		public frmNightOperations()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeMouseEvents();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNightOperations));
            this.panelBody = new System.Windows.Forms.Panel();
            this.panelTab = new System.Windows.Forms.Panel();
            this.tabResult = new AxTABLib.AxTAB();
            this.tabExcelResult = new AxTABLib.AxTAB();
            this.lblResults = new System.Windows.Forms.Label();
            this.panelTop = new System.Windows.Forms.Panel();
            this.checkBoxFtypR = new System.Windows.Forms.CheckBox();
            this.checkBoxFtypD = new System.Windows.Forms.CheckBox();
            this.checkBoxFtypZ = new System.Windows.Forms.CheckBox();
            this.checkBoxFtypO = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtAirport = new System.Windows.Forms.TextBox();
            this.cbNatures = new System.Windows.Forms.ComboBox();
            this.lblTerminal = new System.Windows.Forms.Label();
            this.buttonHelp = new System.Windows.Forms.Button();
            this.lblProgress = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btnLoadPrint = new System.Windows.Forms.Button();
            this.btnPrintPreview = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtFrom = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTerminal = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbDeparture = new System.Windows.Forms.RadioButton();
            this.rbArrival = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radioButtonSTASTD = new System.Windows.Forms.RadioButton();
            this.radioButtonONBLOFBL = new System.Windows.Forms.RadioButton();
            this.radioButtonLANDAIRB = new System.Windows.Forms.RadioButton();
            this.timeTo = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.timeFrom = new System.Windows.Forms.DateTimePicker();
            this.panelBody.SuspendLayout();
            this.panelTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabExcelResult)).BeginInit();
            this.panelTop.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelBody
            // 
            this.panelBody.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panelBody.Controls.Add(this.panelTab);
            this.panelBody.Controls.Add(this.lblResults);
            this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBody.Location = new System.Drawing.Point(0, 220);
            this.panelBody.Name = "panelBody";
            this.panelBody.Size = new System.Drawing.Size(944, 305);
            this.panelBody.TabIndex = 3;
            // 
            // panelTab
            // 
            this.panelTab.Controls.Add(this.tabResult);
            this.panelTab.Controls.Add(this.tabExcelResult);
            this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTab.Location = new System.Drawing.Point(0, 16);
            this.panelTab.Name = "panelTab";
            this.panelTab.Size = new System.Drawing.Size(944, 289);
            this.panelTab.TabIndex = 2;
            // 
            // tabResult
            // 
            this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabResult.Location = new System.Drawing.Point(0, 0);
            this.tabResult.Name = "tabResult";
            this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
            this.tabResult.Size = new System.Drawing.Size(944, 289);
            this.tabResult.TabIndex = 0;
            this.tabResult.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabResult_SendLButtonDblClick);
            // 
            // tabExcelResult
            // 
            this.tabExcelResult.Location = new System.Drawing.Point(0, 0);
            this.tabExcelResult.Name = "tabExcelResult";
            this.tabExcelResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabExcelResult.OcxState")));
            this.tabExcelResult.Size = new System.Drawing.Size(100, 50);
            this.tabExcelResult.TabIndex = 1;
            // 
            // lblResults
            // 
            this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.lblResults.Location = new System.Drawing.Point(0, 0);
            this.lblResults.Name = "lblResults";
            this.lblResults.Size = new System.Drawing.Size(944, 16);
            this.lblResults.TabIndex = 1;
            this.lblResults.Text = "Report Results";
            this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelTop
            // 
            this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panelTop.Controls.Add(this.timeTo);
            this.panelTop.Controls.Add(this.label7);
            this.panelTop.Controls.Add(this.label6);
            this.panelTop.Controls.Add(this.timeFrom);
            this.panelTop.Controls.Add(this.rbDeparture);
            this.panelTop.Controls.Add(this.rbArrival);
            this.panelTop.Controls.Add(this.checkBoxFtypR);
            this.panelTop.Controls.Add(this.checkBoxFtypD);
            this.panelTop.Controls.Add(this.checkBoxFtypZ);
            this.panelTop.Controls.Add(this.checkBoxFtypO);
            this.panelTop.Controls.Add(this.groupBox1);
            this.panelTop.Controls.Add(this.label4);
            this.panelTop.Controls.Add(this.label3);
            this.panelTop.Controls.Add(this.txtAirport);
            this.panelTop.Controls.Add(this.cbNatures);
            this.panelTop.Controls.Add(this.lblTerminal);
            this.panelTop.Controls.Add(this.buttonHelp);
            this.panelTop.Controls.Add(this.lblProgress);
            this.panelTop.Controls.Add(this.progressBar1);
            this.panelTop.Controls.Add(this.btnLoadPrint);
            this.panelTop.Controls.Add(this.btnPrintPreview);
            this.panelTop.Controls.Add(this.btnCancel);
            this.panelTop.Controls.Add(this.btnOK);
            this.panelTop.Controls.Add(this.dtTo);
            this.panelTop.Controls.Add(this.label2);
            this.panelTop.Controls.Add(this.dtFrom);
            this.panelTop.Controls.Add(this.label1);
            this.panelTop.Controls.Add(this.txtTerminal);
            this.panelTop.Controls.Add(this.groupBox2);
            this.panelTop.Controls.Add(this.groupBox3);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(944, 220);
            this.panelTop.TabIndex = 2;
            // 
            // checkBoxFtypR
            // 
            this.checkBoxFtypR.Location = new System.Drawing.Point(536, 137);
            this.checkBoxFtypR.Name = "checkBoxFtypR";
            this.checkBoxFtypR.Size = new System.Drawing.Size(87, 24);
            this.checkBoxFtypR.TabIndex = 41;
            this.checkBoxFtypR.Text = "Rerouted";
            // 
            // checkBoxFtypD
            // 
            this.checkBoxFtypD.Location = new System.Drawing.Point(536, 112);
            this.checkBoxFtypD.Name = "checkBoxFtypD";
            this.checkBoxFtypD.Size = new System.Drawing.Size(87, 24);
            this.checkBoxFtypD.TabIndex = 40;
            this.checkBoxFtypD.Text = "Diverted";
            // 
            // checkBoxFtypZ
            // 
            this.checkBoxFtypZ.Location = new System.Drawing.Point(536, 87);
            this.checkBoxFtypZ.Name = "checkBoxFtypZ";
            this.checkBoxFtypZ.Size = new System.Drawing.Size(98, 24);
            this.checkBoxFtypZ.TabIndex = 39;
            this.checkBoxFtypZ.Text = "Return Flight";
            // 
            // checkBoxFtypO
            // 
            this.checkBoxFtypO.Checked = true;
            this.checkBoxFtypO.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxFtypO.Location = new System.Drawing.Point(536, 62);
            this.checkBoxFtypO.Name = "checkBoxFtypO";
            this.checkBoxFtypO.Size = new System.Drawing.Size(87, 24);
            this.checkBoxFtypO.TabIndex = 38;
            this.checkBoxFtypO.Text = "Operation";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Controls.Add(this.radioButton3);
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Location = new System.Drawing.Point(375, 44);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(136, 123);
            this.groupBox1.TabIndex = 37;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Time to be considered";
            // 
            // radioButton1
            // 
            this.radioButton1.Location = new System.Drawing.Point(12, 68);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(103, 24);
            this.radioButton1.TabIndex = 40;
            this.radioButton1.Text = "&LAND / AIRB";
            // 
            // radioButton3
            // 
            this.radioButton3.Checked = true;
            this.radioButton3.Location = new System.Drawing.Point(12, 20);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(103, 20);
            this.radioButton3.TabIndex = 38;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "&STA / STD";
            // 
            // radioButton2
            // 
            this.radioButton2.Location = new System.Drawing.Point(12, 44);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(103, 20);
            this.radioButton2.TabIndex = 39;
            this.radioButton2.Text = "&ONBL / OFBL";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(12, 119);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 16);
            this.label4.TabIndex = 36;
            this.label4.Text = "Airport:";
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.label3.Location = new System.Drawing.Point(12, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 16);
            this.label3.TabIndex = 35;
            this.label3.Text = "Nature:";
            // 
            // txtAirport
            // 
            this.txtAirport.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAirport.Location = new System.Drawing.Point(74, 115);
            this.txtAirport.Name = "txtAirport";
            this.txtAirport.Size = new System.Drawing.Size(128, 20);
            this.txtAirport.TabIndex = 34;
            // 
            // cbNatures
            // 
            this.cbNatures.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNatures.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbNatures.ItemHeight = 14;
            this.cbNatures.Location = new System.Drawing.Point(74, 84);
            this.cbNatures.MaxDropDownItems = 16;
            this.cbNatures.Name = "cbNatures";
            this.cbNatures.Size = new System.Drawing.Size(245, 22);
            this.cbNatures.TabIndex = 33;
            // 
            // lblTerminal
            // 
            this.lblTerminal.Location = new System.Drawing.Point(12, 149);
            this.lblTerminal.Name = "lblTerminal";
            this.lblTerminal.Size = new System.Drawing.Size(52, 23);
            this.lblTerminal.TabIndex = 6;
            this.lblTerminal.Text = "Terminal:";
            // 
            // buttonHelp
            // 
            this.buttonHelp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonHelp.Location = new System.Drawing.Point(378, 180);
            this.buttonHelp.Name = "buttonHelp";
            this.buttonHelp.Size = new System.Drawing.Size(75, 23);
            this.buttonHelp.TabIndex = 12;
            this.buttonHelp.Text = "Help";
            this.buttonHelp.Click += new System.EventHandler(this.buttonHelp_Click);
            // 
            // lblProgress
            // 
            this.lblProgress.Location = new System.Drawing.Point(659, 149);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(212, 16);
            this.lblProgress.TabIndex = 12;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(462, 180);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(216, 23);
            this.progressBar1.TabIndex = 13;
            this.progressBar1.Visible = false;
            // 
            // btnLoadPrint
            // 
            this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLoadPrint.Location = new System.Drawing.Point(226, 180);
            this.btnLoadPrint.Name = "btnLoadPrint";
            this.btnLoadPrint.Size = new System.Drawing.Size(75, 23);
            this.btnLoadPrint.TabIndex = 10;
            this.btnLoadPrint.Text = "Loa&d + Print";
            this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
            // 
            // btnPrintPreview
            // 
            this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnPrintPreview.Location = new System.Drawing.Point(150, 180);
            this.btnPrintPreview.Name = "btnPrintPreview";
            this.btnPrintPreview.Size = new System.Drawing.Size(75, 23);
            this.btnPrintPreview.TabIndex = 9;
            this.btnPrintPreview.Text = "&Print Preview";
            this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCancel.Location = new System.Drawing.Point(302, 180);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "&Close";
            // 
            // btnOK
            // 
            this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnOK.Location = new System.Drawing.Point(74, 180);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 8;
            this.btnOK.Text = "&Load Data";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // dtTo
            // 
            this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtTo.Location = new System.Drawing.Point(219, 12);
            this.dtTo.Name = "dtTo";
            this.dtTo.Size = new System.Drawing.Size(100, 20);
            this.dtTo.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(186, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "to:";
            // 
            // dtFrom
            // 
            this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFrom.Location = new System.Drawing.Point(74, 12);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Size = new System.Drawing.Size(100, 20);
            this.dtFrom.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Date from:";
            // 
            // txtTerminal
            // 
            this.txtTerminal.Location = new System.Drawing.Point(74, 145);
            this.txtTerminal.Name = "txtTerminal";
            this.txtTerminal.Size = new System.Drawing.Size(128, 20);
            this.txtTerminal.TabIndex = 7;
            // 
            // groupBox2
            // 
            this.groupBox2.Location = new System.Drawing.Point(375, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(267, 33);
            this.groupBox2.TabIndex = 33;
            this.groupBox2.TabStop = false;
            // 
            // rbDeparture
            // 
            this.rbDeparture.Location = new System.Drawing.Point(461, 12);
            this.rbDeparture.Name = "rbDeparture";
            this.rbDeparture.Size = new System.Drawing.Size(104, 19);
            this.rbDeparture.TabIndex = 5;
            this.rbDeparture.Text = "&Departure";
            this.rbDeparture.CheckedChanged += new System.EventHandler(this.rbDeparture_CheckedChanged);
            // 
            // rbArrival
            // 
            this.rbArrival.Checked = true;
            this.rbArrival.Location = new System.Drawing.Point(387, 12);
            this.rbArrival.Name = "rbArrival";
            this.rbArrival.Size = new System.Drawing.Size(68, 19);
            this.rbArrival.TabIndex = 4;
            this.rbArrival.TabStop = true;
            this.rbArrival.Text = "&Arrival";
            this.rbArrival.CheckedChanged += new System.EventHandler(this.rbArrival_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Location = new System.Drawing.Point(526, 44);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(116, 123);
            this.groupBox3.TabIndex = 34;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Flight Status";
            // 
            // radioButtonSTASTD
            // 
            this.radioButtonSTASTD.Location = new System.Drawing.Point(0, 0);
            this.radioButtonSTASTD.Name = "radioButtonSTASTD";
            this.radioButtonSTASTD.Size = new System.Drawing.Size(104, 24);
            this.radioButtonSTASTD.TabIndex = 0;
            // 
            // radioButtonONBLOFBL
            // 
            this.radioButtonONBLOFBL.Location = new System.Drawing.Point(0, 0);
            this.radioButtonONBLOFBL.Name = "radioButtonONBLOFBL";
            this.radioButtonONBLOFBL.Size = new System.Drawing.Size(104, 24);
            this.radioButtonONBLOFBL.TabIndex = 0;
            // 
            // radioButtonLANDAIRB
            // 
            this.radioButtonLANDAIRB.Location = new System.Drawing.Point(0, 0);
            this.radioButtonLANDAIRB.Name = "radioButtonLANDAIRB";
            this.radioButtonLANDAIRB.Size = new System.Drawing.Size(104, 24);
            this.radioButtonLANDAIRB.TabIndex = 0;
            // 
            // timeTo
            // 
            this.timeTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeTo.Location = new System.Drawing.Point(219, 43);
            this.timeTo.Name = "timeTo";
            this.timeTo.ShowUpDown = true;
            this.timeTo.Size = new System.Drawing.Size(100, 20);
            this.timeTo.TabIndex = 43;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(12, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 16);
            this.label7.TabIndex = 45;
            this.label7.Text = "Time from:";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(186, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 16);
            this.label6.TabIndex = 44;
            this.label6.Text = "to:";
            // 
            // timeFrom
            // 
            this.timeFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeFrom.Location = new System.Drawing.Point(74, 43);
            this.timeFrom.Name = "timeFrom";
            this.timeFrom.ShowUpDown = true;
            this.timeFrom.Size = new System.Drawing.Size(100, 20);
            this.timeFrom.TabIndex = 42;
            // 
            // frmNightOperations
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(944, 525);
            this.Controls.Add(this.panelBody);
            this.Controls.Add(this.panelTop);
            this.Name = "frmNightOperations";
            this.Text = "Night Operations";
            this.Load += new System.EventHandler(this.frmNightOperations_Load);
            this.HelpRequested += new System.Windows.Forms.HelpEventHandler(this.frmNightOperations_HelpRequested);
            this.panelBody.ResumeLayout(false);
            this.panelTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabExcelResult)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		private void frmNightOperations_Load(object sender, System.EventArgs e)
		{	
			myDB = UT.GetMemDB();
			InitTimePickers();
			SetupReportTabHeader();
			InitTab();
            PrepareFilterData();
		}

		/// <summary>
		/// Initializes the From/to time control with the current day in
		/// the custom format "dd.MM.yyyy - HH:mm".
		/// </summary>
		private void InitTimePickers()
		{
			DateTime olFrom;
			DateTime olTo;

            TimeSpan olOneDay = new TimeSpan(1, 0, 0, 0);

            olFrom = DateTime.Now;
            olTo = DateTime.Now;
            olTo += olOneDay;

			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 22,0,0);
            olTo = new DateTime(olTo.Year, olTo.Month, olTo.Day + 1, 6, 0, 0);
			dtFrom.Value = olFrom;
			dtTo.Value = olTo;
			dtFrom.CustomFormat = "dd.MM.yyyy";
			dtTo.CustomFormat = "dd.MM.yyyy";

            timeFrom.CustomFormat = "HH:mm";
            timeTo.CustomFormat = "HH:mm";
            
            timeFrom.Value = olFrom;
            timeTo.Value = olTo;

		}

		/// <summary>
		/// Initializes the tab control
		/// </summary>
		private void InitTab()
		{
			tabExcelResult.ResetContent();
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;
			
			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			strDateDisplayFormat = myIni.IniReadValue("FIPSREPORT", "DATEDISPLAYFORMAT");
			strExcelDateTimeFormat = myIni.IniReadValue("FIPSREPORT", "DATEEXCELFORMAT");
			if(strDateDisplayFormat.Length == 0)
			{
				strDateDisplayFormat = strReportDateTimeFormat;
			}
			else
			{
				strReportDateTimeFormat = strDateDisplayFormat;
			}
			if(strExcelDateTimeFormat.Length == 0)
			{
				strExcelDateTimeFormat = strReportDateTimeFormat;
			}

			if (rbArrival.Checked == true) 
			{
				tabResult.HeaderString = strTabHeaderArrival;
				tabResult.LogicalFieldList = strLogicalFieldsArrival;
				tabResult.HeaderLengthString = strTabHeaderLensArrival;
				PrepareReportDataArrival("Report");

				tabExcelResult.HeaderString = strTabHeaderArrival;
				tabExcelResult.LogicalFieldList = strLogicalFieldsArrival;
				tabExcelResult.HeaderLengthString = strTabHeaderLensArrival;
				PrepareReportDataArrival("Excel");
			} 
			else 
			{
				tabResult.HeaderString = strTabHeaderDeparture;
				tabResult.LogicalFieldList = strLogicalFieldsDeparture;
				tabResult.HeaderLengthString = strTabHeaderLensDeparture;
				PrepareReportDataDeparture("Report");

				tabExcelResult.HeaderString = strTabHeaderDeparture;
				tabExcelResult.LogicalFieldList = strLogicalFieldsDeparture;
				tabExcelResult.HeaderLengthString = strTabHeaderLensDeparture;
				PrepareReportDataDeparture("Excel");
			}		
		}

		/// <summary>
		/// Initializes the tab control header data.
		/// </summary>
		private void SetupReportTabHeader()
		{
			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			if(myIni.IniReadValue("FIPS","CHUTE_DISPLAY").CompareTo("TRUE") != 0)
			{
				Helper.RemoveOrReplaceString(10,ref strTabHeaderDeparture,',',"",true);
				Helper.RemoveOrReplaceString(10,ref strLogicalFieldsDeparture,',',"",true);
				Helper.RemoveOrReplaceString(10,ref strTabHeaderLensDeparture,',',"",true);
				Helper.RemoveOrReplaceString(10,ref strExcelTabHeaderLensDeparture,',',"",true);
			}
			else
			{
				bmChuteDisplay = true;
			}
			string[] strLabelHeaderEntry = new string[2];
			string strTerminalLabel = "";
			string strTerminalHeader = "";
			if(Helper.IsSTEVConfigured(this,ref strLabelHeaderEntry,true,true) == true)
			{
				bmShowStev = true;
				strTerminalLabel  = strLabelHeaderEntry[0];
				strTerminalHeader = strLabelHeaderEntry[1];				
				lblTerminal.Text = strTerminalLabel + ":";										
				Helper.RemoveOrReplaceString(10,ref strTabHeaderArrival,',',strTerminalHeader,false);					
				Helper.RemoveOrReplaceString(10,ref strTabHeaderDeparture,',',strTerminalHeader,false);				
			}
			else
			{
				Helper.RemoveOrReplaceString(10,ref strLogicalFieldsArrival,',',"",true);
				Helper.RemoveOrReplaceString(10,ref strTabHeaderArrival,',',"",true);
				Helper.RemoveOrReplaceString(10,ref strTabHeaderLensArrival,',',"",true);
				Helper.RemoveOrReplaceString(10,ref strExcelTabHeaderLensArrival,',',"",true);
					
				Helper.RemoveOrReplaceString(10,ref strLogicalFieldsDeparture,',',"",true);
				Helper.RemoveOrReplaceString(10,ref strTabHeaderDeparture,',',"",true);
				Helper.RemoveOrReplaceString(10,ref strTabHeaderLensDeparture,',',"",true);
				Helper.RemoveOrReplaceString(10,ref strExcelTabHeaderLensDeparture,',',"",true);

				lblTerminal.Hide();
				txtTerminal.Hide();
			}
		}

		/// <summary>
		/// Loads the data for the list from the data store in the following 
		/// sequence:
		/// 1. Loads the flight records from AFT (day wise)
		/// 2. Loads the Checkin Counter information corresponding to the 
		///		flight selected in step 1.
		///	3. Loads the Pax and Load information corresponding to the flights
		///		selected in step 1.
		/// 
		/// </summary>
		private void LoadReportData()
		{
			tabResult.ResetContent();
			tabExcelResult.ResetContent();
			string strTmpQry = "";
			int ilTotal;
			int percent;
			StringBuilder strFlnus = new StringBuilder(10000);


            // Extract the true Nature code from the selected entry of the combobox
            strNatureCode = "";
            string[] strArr = cbNatures.Text.Split('/');
            strNatureCode = strArr[0].Trim();
            strAirportCode = txtAirport.Text;



			// Reading flight information
			myDB.Unbind("AFT");
			myAFT = myDB.Bind("AFT", "AFT"
							, "URNO,FLNO,ORG3,STOA,LAND,DES3,STOD,AIRB,ACT3,REGN,PSTD,PSTA,GTA1,GTA2,GTD1,GTD2,BLT1,BLT2,ADID,PAX1,PAX2,PAX3,PAXF,PAXI,PAXT,BAGN,BAGW,MAIL,CGOT,STEV,CKIF,CKIT,FLTI,NFES,JCNT,TTYP,STYP,VIAN,ONBL,OFBL,STEV,ISRE"
                            , "10,9,3,14,14,3,14,14,3,12,5,5,5,5,5,5,5,5,1,3,3,3,3,3,3,6,7,6,6,1,5,5,1,14,1,5,2,2,14,14,1,1"
							, "URNO,FLNO,ORG3,STOA,LAND,DES3,STOD,AIRB,ACT3,REGN,PSTD,PSTA,GTA1,GTA2,GTD1,GTD2,BLT1,BLT2,ADID,PAX1,PAX2,PAX3,PAXF,PAXI,PAXT,BAGN,BAGW,MAIL,CGOT,STEV,CKIF,CKIT,FLTI,NFES,JCNT,TTYP,STYP,VIAN,ONBL,OFBL,STEV,ISRE");
			
			myAFT.Clear();
			myAFT.TimeFields = "STOA,LAND,STOD,AIRB,ONBL,OFBL,NFES";
			myAFT.Command("read",",GFR,");
			myAFT.TimeFieldsInitiallyInUtc = true;


			progressBar1.Value = 0;
			lblProgress.Text = "Loading Flight Data";
			lblProgress.Refresh();
			progressBar1.Show();

			string strTerminal = "";
			if(bmShowStev == true)
			{
				strTerminal = txtTerminal.Text.Trim();
			}

            DateTime datFrom;
            DateTime datTo;
            string strDateFrom = "";
            string strDateTo = "";
            datFrom = dtFrom.Value;
            datTo = dtTo.Value;


            datFrom = new DateTime(datFrom.Year, datFrom.Month, datFrom.Day, timeFrom.Value.Hour, timeFrom.Value.Minute, 0);
            datTo = new DateTime(datTo.Year, datTo.Month, datTo.Day, timeFrom.Value.Hour, timeFrom.Value.Minute, 0);

            TimeSpan tsDays = (datTo - datFrom);
            ilTotal = Convert.ToInt32(tsDays.TotalDays);
            if (ilTotal == 0)
                ilTotal = 1;


            TimeSpan NightDuration = datTo - datFrom;

            int Days = NightDuration.Days;


            if (timeFrom.Value.Hour > timeTo.Value.Hour)
            {

                TimeSpan olOneDay = new TimeSpan(1, 0, 0, 0);

                DateTime olTmp = datFrom+ olOneDay;


                DateTime datToTmp = new DateTime(olTmp.Year, olTmp.Month, olTmp.Day , timeTo.Value.Hour, timeTo.Value.Minute, 0);
                NightDuration = datToTmp - datFrom;
                TimeFrameIsWithinOneDay = false;
            }
            else
            {
                Days += 1;
                DateTime datToTmp = new DateTime(datFrom.Year, datFrom.Month, datFrom.Day, timeTo.Value.Hour, timeTo.Value.Minute, 0);
                NightDuration = datToTmp - datFrom;
                TimeFrameIsWithinOneDay = true;
            }



            if (UT.IsTimeInUtc == false)
            {
                datFrom = UT.LocalToUtc(datFrom);
                datTo = UT.LocalToUtc(datTo);
                strDateFrom = UT.DateTimeToCeda(datFrom);
                strDateTo = UT.DateTimeToCeda(datTo);
            }
            else
            {
                strDateFrom = UT.DateTimeToCeda(datFrom);
                strDateTo = UT.DateTimeToCeda(datTo);
            }


            //Prepare loop reading day by day
            DateTime datReadFrom = datFrom;
            DateTime datReadTo;


            for (int loopCnt = 0; loopCnt < Days; loopCnt++)
            {
                percent = Convert.ToInt32((loopCnt * 100) / ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
                datReadTo = datReadFrom + NightDuration;
                strDateFrom = UT.DateTimeToCeda(datReadFrom);
                strDateTo = UT.DateTimeToCeda(datReadTo);

				strTmpQry = strWhereRaw;

                if (this.radioButton3.Checked == true)
                {
                    strTmpQry = strWhereRaw;
                }
                else if (this.radioButton2.Checked == true)
                {
                    strTmpQry = strWhereONBLOFBLRaw;
                }
                else if (this.radioButton1.Checked == true)
                {
                    strTmpQry = strWhereLANDAIRBRaw;
                }
            

				if(strTerminal != "")
				{
					strTmpQry += strTerminalWhere;
				}

                if (strNatureCode != "<None>")
                    strTmpQry += strNatWhere;
                if (strAirportCode != "")
                    strTmpQry += strORGDESWhere;


                string strFtyp = "";

                if(checkBoxFtypO.Checked)
                {
                    if (strFtyp == "")
                        strFtyp = "'O'";
                }
                if (checkBoxFtypD.Checked)
                {
                    if (strFtyp == "")
                        strFtyp = "'D'";
                    else
                        strFtyp += ",'D'";
                }
                if (checkBoxFtypZ.Checked)
                {
                    if (strFtyp == "")
                        strFtyp = "'Z'";
                    else
                        strFtyp += ",'Z'";
                }
                if (checkBoxFtypR.Checked)
                {
                    if (strFtyp == "")
                        strFtyp = "'R'";
                    else
                        strFtyp += ",'R'";
                }

                strTmpQry = strTmpQry.Replace("@@FROM", strDateFrom);
                strTmpQry = strTmpQry.Replace("@@FTYP", strFtyp);
                strTmpQry = strTmpQry.Replace("@@TO", strDateTo);
                strTmpQry = strTmpQry.Replace("@@ORIG", UT.Hopo);
                strTmpQry = strTmpQry.Replace("@@DEST", UT.Hopo);
				strTmpQry = strTmpQry.Replace("@@STEV",strTerminal);
                strTmpQry = strTmpQry.Replace("@@NAT", strNatureCode);
                strTmpQry = strTmpQry.Replace("@@APT", strAirportCode);
                strTmpQry = strTmpQry.Replace("@@HOPO", UT.Hopo);				

				myAFT.Load(strTmpQry);
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
			} ;

			if(bmChuteDisplay == true)
			{
				//-------------------------------------------------------------------
				//Load chutes according to the AFT.URNO->CHA.RURN
				myDB.Unbind("DED_CHA");
				myCHA = myDB.Bind("DED_CHA", "CHA", "RURN,LNAM,STOB,STOE,TIFB,TIFE,LTYP,CHUF,CHUT", "10,5,14,14,14,14,3,5,5,3", "RURN,LNAM,STOB,STOE,TIFB,TIFE,LTYP");
				myCHA.Clear();
				lblProgress.Text = "Loading Chute Data";
				lblProgress.Refresh();
				int loops = 0;
				progressBar1.Value = 0;
				progressBar1.Refresh();
				ilTotal = (int)(myAFT.Count/300);
				if(ilTotal == 0) ilTotal = 1;
				string strRurns = "";
				string strTmpCHAWhere = strDedicatedCHAWhere;
				for(int i = 0; i < myAFT.Count; i++)
				{
					strRurns += myAFT[i]["URNO"] + ",";
					if((i % 300) == 0 && i > 0)
					{
						loops++;
						percent = Convert.ToInt32((loops * 100)/ilTotal);
						if (percent > 100) 
							percent = 100;
						progressBar1.Value = percent;
						strRurns = strRurns.Remove(strRurns.Length-1, 1);
						strTmpCHAWhere = strDedicatedCHAWhere;
						strTmpCHAWhere = strTmpCHAWhere.Replace("@@RURN", strRurns);
						myCHA.Load(strTmpCHAWhere);
						strRurns = "";
					}
				}
				//Load the rest of the dedicated Chutes.
				if(strRurns.Length > 0)
				{
					strRurns = strRurns.Remove(strRurns.Length-1, 1);
					strTmpCHAWhere = strDedicatedCHAWhere;
					strTmpCHAWhere = strTmpCHAWhere.Replace("@@RURN", strRurns);
					myCHA.Load(strTmpCHAWhere);
					strRurns = "";
				}
				myCHA.Sort("STOB,TIFB,STOE,TIFE", true);
				myCHA.CreateIndex("RURN", "RURN");
			}

			lblProgress.Text = "";
			progressBar1.Hide();

			LoadCheckInCounterData();
			//LoadPaxAndLoadData();
			myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;
		}
		
		/// <summary>
		/// Loads the Check-In counter data corresponding to the flights 
		/// selected from the AFT table.
		/// </summary>
		private void LoadCheckInCounterData() 
		{
			int ilTotal = 0;
			int loopCnt = 0;
			int percent = 0;
			StringBuilder strFlnus = new StringBuilder();
			string strTmpQry;

			progressBar1.Value = 0;
			lblProgress.Text = "Loading Counter Data";
			lblProgress.Refresh();
			progressBar1.Show();
			myDB.Unbind("CCA");
			myCCA = myDB.Bind("CCA", "CCA"
								, "FLNU,CKIC"
								, "10,5"
								, "FLNU,CKIC");
			myCCA.Clear();

			ilTotal = (int)(myAFT.Count/300);
			if(ilTotal == 0) 
				ilTotal = 1;
			loopCnt = 0;
			for(int i = 0; i < myAFT.Count; i++)
			{
				strFlnus.Append(myAFT[i]["URNO"]).Append(",");
				if((i % 300) == 0 && i > 0)
				{
					loopCnt++;
					percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
					if (percent > 100) 
						percent = 100;
					progressBar1.Value = percent;

					strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
					strTmpQry = strCCAWhereRaw;
					strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
					myCCA.Load(strTmpQry);
					strFlnus.Remove(0,strFlnus.Length);
				}
			}
			if(strFlnus.Length > 0)
			{
				strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
				strTmpQry = strCCAWhereRaw;
				strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
				myCCA.Load(strTmpQry);
				strFlnus.Remove(0,strFlnus.Length);
			}
			myCCA.CreateIndex("FLNU", "FLNU");
			lblProgress.Text = "";
			progressBar1.Hide();
		}

		/// <summary>
		/// Loads the Pax Count and Load information for the flights 
		/// selected from AFT table.
		/// </summary>
		/*private void LoadPaxAndLoadData()
		{
			int ilTotal = 0;
			int loopCnt = 0;
			int percent = 0;
			string strTmpQry;
			StringBuilder strFlnus = new StringBuilder();

			progressBar1.Value = 0;
			lblProgress.Text = "Loading Load Data";
			lblProgress.Refresh();
			progressBar1.Show();

			if (cbLoadInfo.Checked) 
			{
				myDB.Unbind("LOA");
				myLOA = myDB.Bind("LOA","LOA"
					,"FLNU,DSSN,TYPE,STYP,SSTP,SSST,VALU"
					,"10,10,14,40,1,3,3,3,3,6"
					,"FLNU,DSSN,TYPE,STYP,SSTP,SSST,VALU");
				myLOA.Clear();

				ilTotal = (int)(myAFT.Count/300);
				if(ilTotal == 0) 
					ilTotal = 1;
				loopCnt = 0;
				for(int i = 0; i < myAFT.Count; i++)
				{
					strFlnus.Append(myAFT[i]["URNO"]).Append(",");
					if((i % 300) == 0 && i > 0)
					{
						loopCnt++;
						percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
						if (percent > 100) 
							percent = 100;
						progressBar1.Value = percent;

						strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
						strTmpQry = strLOAWhereRaw;
						strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
						myLOA.Load(strTmpQry);
						strFlnus.Remove(0,strFlnus.Length);
					}
				}
				if(strFlnus.Length > 0)
				{
					strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
					strTmpQry = strLOAWhereRaw;
					strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
					myLOA.Load(strTmpQry);
					strFlnus.Remove(0,strFlnus.Length);
				}
				myLOA.CreateIndex("FLNU", "FLNU");
				lblProgress.Text = "";
				progressBar1.Hide();
			}
			else
			{
				myDB.Unbind("APX");
				myAPX = myDB.Bind("APX","APX"
					,"FLNU,URNO,TYPE,PAXC"
					,"10,10,3,6"
					,"FLNU,URNO,TYPE,PAXC");
				myAPX.Clear();

				ilTotal = (int)(myAFT.Count/300);
				if(ilTotal == 0) 
					ilTotal = 1;
				loopCnt = 0;
				for(int i = 0; i < myAFT.Count; i++)
				{
					strFlnus.Append(myAFT[i]["URNO"]).Append(",");
					if((i % 300) == 0 && i > 0)
					{
						loopCnt++;
						percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
						if (percent > 100) 
							percent = 100;
						progressBar1.Value = percent;

						strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
						strTmpQry = strAPXWhereRaw;
						strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
						myAPX.Load(strTmpQry);
						strFlnus.Remove(0,strFlnus.Length);
					}
				}
				if(strFlnus.Length > 0)
				{
					strFlnus = strFlnus.Remove(strFlnus.Length-1, 1);
					strTmpQry = strAPXWhereRaw;
					strTmpQry = strTmpQry.Replace("@@FLNU", strFlnus.ToString());
					myAPX.Load(strTmpQry);
					strFlnus.Remove(0,strFlnus.Length);
				}
				myAPX.CreateIndex("FLNU", "FLNU");
				lblProgress.Text = "";
				progressBar1.Hide();
			}
		}*/
		/// <summary>
		/// Returns the correct load value for type, styp, sstp,ssst
		/// according to the priority.
		/// </summary>
		/// <param name="dType">The TYPE of LOATAB</param>
		/// <param name="strAftUrno">Urno of AFT</param>
		/// <param name="styp">STYP of LOATAB</param>
		/// <param name="sstp">SSTP of LOATAB</param>
		/// <param name="ssst">SSST of LOATAB</param>
		/// <returns></returns>
		private string GetLoadData(string dType, string strAftUrno, string styp, string sstp, string ssst)
		{
			string strLDM = "";
			string strMVT = "";
			string strKRI = "";
			
			IRow [] rows = myLOA.RowsByIndexValue("FLNU", strAftUrno);
			for(int i = 0; i < rows.Length; i++)
			{
				if (rows[i]["TYPE"].Equals(dType))
				{
					if ( rows[i]["STYP"].Equals(styp) 
							&& rows[i]["SSTP"].Equals(sstp) 
							&& rows[i]["SSST"].Equals(ssst))
					{
						if(rows[i]["DSSN"]=="USR")
							return rows[i]["VALU"];
						if(rows[i]["DSSN"] == "LDM")
							strLDM = rows[i]["VALU"];
						if(rows[i]["DSSN"] == "MVT")
							strMVT = rows[i]["VALU"];
						if(rows[i]["DSSN"] == "KRI")
							strKRI = rows[i]["VALU"];
					}
				}
			}
			if(strLDM != "") return strLDM;
			if(strMVT != "") return strMVT;
			if(strKRI != "") return strKRI;
			return "";
		}

		/// <summary>
		/// Formats the Departure data for display in the list.
		/// </summary>
		private void PrepareReportDataDeparture(string strReportOrExcel)
		{
			if (myAFT == null)
				return;
			StringBuilder strData = new StringBuilder(200);
			StringBuilder sb = new StringBuilder(100000);
			string strCknCtr;
			string strId = "";
			string strInf = "";
			iTotalFlights = 0;
			IRow [] rows;		


			string strDateTimeFormat = strReportDateTimeFormat;
			if(strReportOrExcel.Equals("Excel") == true)
			{
				strDateTimeFormat = strExcelDateTimeFormat;
			}

			for (int i = 0; i<myAFT.Count ; i++ )
			{
				if (myAFT[i]["ADID"].Equals("D"))
				{
                    strData.Append(myAFT[i]["FLNO"]).Append(",");
                    strData.Append(myAFT[i]["FLTI"]).Append(",");
                    strData.Append(myAFT[i]["JCNT"]).Append(",");
                    strData.Append(Helper.DateString(myAFT[i]["STOD"], strDateTimeFormat)).Append(",");
                    // Weekday
                    DateTime tmpDate = UT.CedaFullDateToDateTime(myAFT[i]["STOD"]);
                    strData.Append( (int)tmpDate.DayOfWeek);
                    strData.Append(",");

                    strData.Append(myAFT[i]["TTYP"]).Append(",");
                    strData.Append(myAFT[i]["STYP"]).Append(",");
                    strData.Append(myAFT[i]["DES3"]).Append(",");
                    strData.Append(myAFT[i]["VIAN"]).Append(",");
                    strData.Append(myAFT[i]["PSTD"]).Append(",");
                    strData.Append(myAFT[i]["STEV"]).Append(",");
                    strData.Append(myAFT[i]["GTD1"]).Append(",");
                    strData.Append(Helper.DateString(myAFT[i]["OFBL"], strDateTimeFormat)).Append(" / ");
                    strData.Append(Helper.DateString(myAFT[i]["AIRB"], strDateTimeFormat)).Append(",");
                    // CKI
                    strCknCtr = "";
                    if (myAFT[i]["CKIF"] != "" || myAFT[i]["CKIT"] != "")
                    {
                        strCknCtr = myAFT[i]["CKIF"] + " - " + myAFT[i]["CKIT"];
                    }
                    strData.Append(strCknCtr).Append(",");

                    //CKI

					if(bmChuteDisplay == true)
					{
						rows = myCHA.RowsByIndexValue("RURN", myAFT[i]["URNO"]);
						if(rows.Length == 0)
						{
							strData.Append(",");
						}
						else
						{
                            string olTmp = "";
                            olTmp = this.AppendChuteFromTo(rows);
                            strData.Append(olTmp);
                            strData.Append(",");
                        }
					}

                    strData.Append(myAFT[i]["ISRE"]).Append(",");
                    strData.Append(myAFT[i]["ACT3"]).Append(",");
                    strData.Append(myAFT[i]["REGN"]).Append(",");
                    strData.Append(Helper.DateString(myAFT[i]["NFES"], strDateTimeFormat));


					strData.Append("\n");
					sb.Append(strData.ToString());
					strData.Remove(0,strData.Length);					
					iTotalFlights++;
				}
			}
			if(strReportOrExcel.Equals("Report") == true)
			{
				tabResult.InsertBuffer(sb.ToString(),"\n");
				tabResult.Sort("3", true, true);
				tabResult.Refresh();
				lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
			}
			else if(strReportOrExcel.Equals("Excel") == true)
			{
				tabExcelResult.InsertBuffer(sb.ToString(),"\n");
				tabExcelResult.Sort("3", true, true);			
			}
		}

		/// <summary>
		/// Formats the Arrival data for display in the list.
		/// </summary>
		private void PrepareReportDataArrival(string strReportOrExcel)
		{
			if (myAFT == null)
				return;
			StringBuilder strData = new StringBuilder(200);
			StringBuilder sb = new StringBuilder(100000);
			string strId = "";
			string strInf = "";
			iTotalFlights = 0;
			IRow [] rows;

			string strDateTimeFormat = strReportDateTimeFormat;
			if(strReportOrExcel.Equals("Excel") == true)
			{
				strDateTimeFormat = strExcelDateTimeFormat;
			}

			for (int i = 0; i<myAFT.Count ; i++ )
			{
				if (myAFT[i]["ADID"] == "A") 
				{
                    strData.Append(myAFT[i]["FLNO"]).Append(",");
                    strData.Append(myAFT[i]["FLTI"]).Append(",");
                    strData.Append(myAFT[i]["JCNT"]).Append(",");
                    strData.Append(Helper.DateString(myAFT[i]["STOA"], strDateTimeFormat)).Append(",");
                    // Weekday
                    // Weekday
                    DateTime tmpDate = UT.CedaFullDateToDateTime(myAFT[i]["STOA"]);
                    strData.Append((int)tmpDate.DayOfWeek);
                    strData.Append(",");
                    strData.Append(myAFT[i]["TTYP"]).Append(",");
                    strData.Append(myAFT[i]["STYP"]).Append(",");
                    strData.Append(myAFT[i]["ORG3"]).Append(",");
                    strData.Append(myAFT[i]["VIAN"]).Append(",");
                    strData.Append(myAFT[i]["PSTA"]).Append(",");
                    strData.Append(myAFT[i]["STEV"]).Append(",");
                    strData.Append(Helper.DateString(myAFT[i]["LAND"], strDateTimeFormat)).Append(" / ");
                    strData.Append(Helper.DateString(myAFT[i]["ONBL"], strDateTimeFormat)).Append(",");
                    strData.Append(myAFT[i]["BLT1"]).Append(",");
                    strData.Append(myAFT[i]["ISRE"]).Append(",");
                    strData.Append(myAFT[i]["ACT3"]).Append(",");
                    strData.Append(myAFT[i]["REGN"]).Append(",");
                    strData.Append(Helper.DateString(myAFT[i]["NFES"], strDateTimeFormat));
					strData.Append("\n");

					sb.Append(strData.ToString());
					strData.Remove(0,strData.Length);
					iTotalFlights++;
				}
			}
			if(strReportOrExcel.Equals("Report") == true)
			{
				tabResult.InsertBuffer(sb.ToString(),"\n");
				tabResult.Sort("3", true, true);
				tabResult.Refresh();
				lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
			}
			else if(strReportOrExcel.Equals("Excel") == true)
			{
				tabExcelResult.InsertBuffer(sb.ToString(),"\n");
				tabExcelResult.Sort("3", true, true);			
			}
		}

		private string AppendChuteFromTo(IRow[] rows)
		{
			// copy members to array list
			ArrayList myRows = new ArrayList(rows);
			// initialize members			
			for (int i = 0; i < myRows.Count; i++)
			{				
				((IRow)myRows[i])["CHUF"]=	((IRow)myRows[i])["LNAM"];
				((IRow)myRows[i])["CHUT"]=	((IRow)myRows[i])["LNAM"];
			}

			for (int i = 0; i < myRows.Count; i++)
			{				
				int ilCHUF = CalculateIntValueOfString(((IRow)myRows[i])["CHUF"]);
				int ilCHUT = CalculateIntValueOfString(((IRow)myRows[i])["CHUT"]);
				for (int j = 0; j < myRows.Count; j++)
				{					
					if (((IRow)myRows[i])["LTYP"] == ((IRow)myRows[j])["LTYP"] && 
						((IRow)myRows[i])["STOB"] == ((IRow)myRows[j])["STOB"] &&
						((IRow)myRows[i])["STOE"] == ((IRow)myRows[j])["STOE"])
					{
						if (CalculateIntValueOfString(((IRow)myRows[j])["CHUT"]) + 1 == ilCHUF ||
							CalculateIntValueOfString(((IRow)myRows[j])["CHUF"]) - 1 == ilCHUT )
						{
							if (CalculateIntValueOfString(((IRow)myRows[j])["CHUT"]) + 1 == ilCHUF )
							{								
								((IRow)myRows[i])["CHUF"] = ((IRow)myRows[j])["CHUF"];
							}
							else
							{								
								((IRow)myRows[i])["CHUT"] = ((IRow)myRows[j])["CHUT"];
							}
							
							myRows.RemoveAt(j);
							if ( j < i ) 
								i--;
							i--;
							break;
						}
					}
				}
			}


            if (myRows.Count > 0)
                return ((IRow)myRows[0])["CHUF"] + " - " + ((IRow)myRows[0])["CHUT"];
            else
                return "";


		}

		/// <summary>
		/// Sent by the tab control when the user double click.
		/// This is necessary for sorting.
		/// </summary>
		/// <param name="sender">The Tab control</param>
		/// <param name="e">Double click event parameters.</param>
		private void tabResult_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)		
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if( tabResult.SortOrderASC == true)
					{
						tabResult.Sort(e.colNo.ToString(), false, true);
						tabExcelResult.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						tabResult.Sort(e.colNo.ToString(), true, true);
						tabExcelResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabResult.Sort( e.colNo.ToString(), true, true);
					tabExcelResult.Sort( e.colNo.ToString(), true, true);
				}
				tabResult.Refresh();
			}
		}

		/// <summary>
		/// This function returns the int value of a string.
		/// </summary>
		/// <param name="strFlightValues"></param>
		/// <returns></returns>
		private int CalculateIntValueOfString(string strFlightValues)
		{
			string strNew = "";
			int total = 0;
			if(IsAlpha(strFlightValues))
			{
				char ab;
				IEnumerator strFltValEnum = strFlightValues.GetEnumerator();
				int CharCount = 0;
				while( strFltValEnum.MoveNext( ) )
				{
					CharCount++;
					ab = (char) strFltValEnum.Current;
					if(Char.IsNumber(ab))
						strNew += ab;
				}
				total = int.Parse(strNew);	
			}
			else
			{
				total = int.Parse(strFlightValues);
			}
			return total ;
		}
		/// <summary>
		/// Checks if a character is Alphabet or Numeric.
		/// </summary>
		/// <param name="strToCheck"></param>
		/// <returns></returns>
		public bool IsAlpha(string strToCheck)
		{
			Regex objAlphaPattern=new Regex("[A-Z]");

			return objAlphaPattern.IsMatch(strToCheck);
		}

		/// <summary>
		/// Generates the report in a popup window
		/// 
		/// Format - A4 Landscape
		/// </summary>
		private void RunReport() 
		{
			StringBuilder strSubHeader = new StringBuilder();

			//strSubHeader.Append("Load and Pax Report with ");
			if (rbArrival.Checked)
				strSubHeader.Append("Arrival ");
			else
				strSubHeader.Append("Departure ");

			strSubHeader.Append("From: ")
						.Append(dtFrom.Value.ToString("dd.MM.yy'/'HH:mm"))
						.Append(" To: ")
						.Append(dtTo.Value.ToString("dd.MM.yy'/'HH:mm"));
            strSubHeader.Append(" (Flights: ").Append(iTotalFlights.ToString());
            strSubHeader.Append(" )");

			strReportSubHeader = strSubHeader.ToString();
			strReportHeader = "Night Flight Operation ";
			//rptA3_Landscape rpt = new rptA3_Landscape(tabResult, "Statistics - Arrival / Departure", strSubHeader.ToString(), "", 8);
            //prtFIPS_Landscape rpt = new prtFIPS_Landscape(tabResult,"Statistics - Arrival/Departure",strSubHeader.ToString(),"",8);

            prtFIPS_Landscape rpt = new prtFIPS_Landscape(tabResult, strReportHeader, strReportSubHeader.ToString(), "", 8);
            //rptFIPS rpt = new rptFIPS(tabResult, strReportHeader, strReportSubHeader.ToString(), "", 8);
			if (rbArrival.Checked == true) 
			{
				rpt.SetExcelExportHeaderLenghtString(strExcelTabHeaderLensArrival);
			}
			else
			{
				rpt.SetExcelExportHeaderLenghtString(strExcelTabHeaderLensDeparture);
			}
			rpt.SetExcelExportTabResult(tabExcelResult);
			//rpt.TextBoxCanGrow = false;
			frmPrintPreview frm = new frmPrintPreview(rpt);

			if(Helper.UseSpreadBuilder() == true)
			{
				activeReport = rpt;
				frm.UseSpreadBuilder(true);
				frm.GetBtnExcelExport().Click += new EventHandler(frmNightOperations_Click);
			}
			else
			{
				frm.UseSpreadBuilder(false);
			}
			frm.Show();
		}

		/// <summary>
		/// Validates the user entry for the following fields:
		/// 1. FROM and TO fields
		/// 
		/// Displays the error message.
		/// </summary>
		/// <returns>TRUE - If entries are correct, else FALSE</returns>
		private bool validEntry() 
		{
			string strErr = "";
//			if (dtFrom.Value > dtTo.Value)
//				strErr = "Date From is later than Date To!\n";

			if (strErr.Equals(""))
				return true;
			else 
			{
				MessageBox.Show(this, strErr
					,"FIPS Reports" ,MessageBoxButtons.OK ,MessageBoxIcon.Error);
				return false;
			}
		}
		
		private void rbArrival_CheckedChanged(object sender, System.EventArgs e)
		{
			InitTab();
		}

		private void rbDeparture_CheckedChanged(object sender, System.EventArgs e)
		{
			InitTab();
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			if(validEntry())
			{
				this.Cursor = Cursors.WaitCursor;
				LoadReportData();
				if(this.rbArrival.Checked == false)
				{
					PrepareReportDataDeparture("Report");
					PrepareReportDataDeparture("Excel");
				}
				else
				{
					PrepareReportDataArrival("Report");
					PrepareReportDataArrival("Excel");
				}
				this.Cursor = Cursors.Arrow;
			}
		}

		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			RunReport();
		}

		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			if(validEntry())
			{
				this.Cursor = Cursors.WaitCursor;
				LoadReportData();
				if(this.rbArrival.Checked == false)
				{
					PrepareReportDataDeparture("Report");
					PrepareReportDataDeparture("Excel");
				}
				else
				{
					PrepareReportDataArrival("Report");
					PrepareReportDataArrival("Excel");
				}
				RunReport();
				this.Cursor = Cursors.Arrow;
			}
		}

		private void InitializeMouseEvents()
		{			
			((System.Windows.Forms.Control)tabResult).MouseWheel += new MouseEventHandler(frmNightOperations_MouseWheel);
			((System.Windows.Forms.Control)tabResult).MouseEnter += new EventHandler(frmNightOperations_MouseEnter);
			((System.Windows.Forms.Control)tabResult).MouseLeave += new EventHandler(frmNightOperations_MouseLeave);
		}

		private void frmNightOperations_MouseWheel(object sender, MouseEventArgs e)
		{
			if(bmMouseInsideTabControl == false || Helper.CheckScrollingStatus(tabResult) == false)
			{
				return;
			}
			if(e.Delta > 0 && tabResult.GetVScrollPos() > 0)
			{				
				tabResult.OnVScrollTo(tabResult.GetVScrollPos() - 1);
			}
			else if(e.Delta < 0 && (tabResult.GetVScrollPos() + 1) < tabResult.GetLineCount())
			{
				tabResult.OnVScrollTo(tabResult.GetVScrollPos() + 1);
			}
		}

		private void frmNightOperations_MouseEnter(object sender, EventArgs e)
		{			
			bmMouseInsideTabControl = true;
		}

		private void frmNightOperations_MouseLeave(object sender, EventArgs e)
		{
			bmMouseInsideTabControl = false;
		}

		private void frmNightOperations_Click(object sender, EventArgs e)
		{
			if (rbArrival.Checked == true) 
			{
				Helper.ExportToExcel(tabExcelResult,strReportHeader,strReportSubHeader,strExcelTabHeaderLensArrival,8,0,activeReport);
			}
			else
			{
				Helper.ExportToExcel(tabExcelResult,strReportHeader,strReportSubHeader,strExcelTabHeaderLensDeparture,8,0,activeReport);
			}
		}

		private void buttonHelp_Click(object sender, System.EventArgs e)
		{
			Helper.ShowHelpFile(this);
		}

		private void frmNightOperations_HelpRequested(object sender, HelpEventArgs hlpevent)
		{
			Helper.ShowHelpFile(this);
		}



        private void PrepareFilterData()
        {
            myNAT = myDB["NAT"];
            cbNatures.Items.Add("<None>");
            if (myNAT == null)
            {
                myNAT = myDB.Bind("NAT", "NAT", "TTYP,TNAM", "5,30", "TTYP,TNAM");
                myNAT.Load("ORDER BY TTYP");
            }
            else if (myNAT.Count == 0)
            {
                myNAT = myDB.Bind("NAT", "NAT", "TTYP,TNAM", "5,30", "TTYP,TNAM");
                myNAT.Load("ORDER BY TTYP");
            }
            for (int i = 0; i < myNAT.Count; i++)
            {
                string strValue = myNAT[i]["TTYP"] + " / " + myNAT[i]["TNAM"];
                cbNatures.Items.Add(strValue);
            }
            if (cbNatures.Items.Count > 0)
                cbNatures.SelectedIndex = 0;
        }




    }
}
