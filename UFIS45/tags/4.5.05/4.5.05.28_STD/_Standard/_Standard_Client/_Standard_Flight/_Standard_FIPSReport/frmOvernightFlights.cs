using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Utils;
using Ufis.Data;
using System.Text.RegularExpressions;

namespace FIPS_Reports
{
	/// <summary>
	/// Summary description for frmOvernightFlights.
	/// </summary>
	public class frmOvernightFlights : System.Windows.Forms.Form
	{
		#region _My Members

		/// <summary>
		/// The where statements: all "@@xx" fields will be replaced
		/// with the true values according to the user's entry.
		/// </summary>
		
		//		private string strWhereRaw = "WHERE (((STOA BETWEEN '@@FROM' AND '@@TO' AND DES3='@@HOPO' ) OR ( STOD BETWEEN '@@FROM' AND '@@TO' AND ORG3='@@HOPO')) AND (FTYP='O' OR FTYP='S' OR FTYP='X')) AND STAT<>'DEL'";
		//		private string strWhereRaw = "WHERE (((STOA BETWEEN '@@FROM' AND '@@TO') AND (STOD BETWEEN '@@FROM' AND '@@TO'))) AND (FTYP='O' OR FTYP='S' OR FTYP='X') AND STAT<>'DEL' " ;
		private string strWhereRaw = "WHERE (((STOA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))) OR (STOD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('D','B')))) OR " +
			"((TIFA BETWEEN '@@FROM' AND '@@TO' AND (ADID IN('A','B'))) OR (TIFD BETWEEN '@@FROM' AND '@@TO' AND (ADID IN ('D','B'))))) AND (FTYP='O' OR FTYP='S' OR FTYP='X') AND STAT<>'DEL' " ;
		private string strWhereSingleArrivals = "WHERE ((STOA BETWEEN '@@FROM' AND '@@TIMEFROM') OR (TIFA BETWEEN '@@FROM' AND '@@TIMEFROM')) AND (ADID='A' AND RTYP='S' AND ONBL != ' ')";
		private string strNatWhere			= " AND (TTYP='@@NAT')";
		private string strORGDESWhere	= " AND (ORG3='@@APT' OR DES3='@@APT')";
		/// <summary>
		/// The where statement for the dedicated chutes.
		/// </summary>
		private string strDedicatedCHAWhere = "WHERE RURN IN (@@RURN) AND LNAM<>' ' AND DES3 <> '@@@'";
		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes
		/// </summary>
		private string strLogicalFields = "FLNO,ADID,ORGDES,SCHEDULE,ACTUAL,ACTYPE,ONOFBL,TTYP,REGN";
		/// <summary>
		/// The logical field names of the tab control for internal 
		/// development purposes ==> for Rotation
		/// </summary>
		
		//DB-Fields
		// 10 , 10 , 9  ,  8 , 1  , 1  , 14 , 1  ,  5 , 1  , 2  , 3  , 4  , 3  , 4  , 2  , 5  , 5  , 5  , 5  , 5  , 1  , 3  , 14 , 1  , 3  , 4  , 5  ,  5 , 5  , 5  , 5
		//RKEY,URNO,FLNO,CSGN,FLTI,JCNT,STOA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT"
		//private string strLogicalFieldsRotation = "Arrival_A,ID_A,J_A,Sched_A,A_A,Na_A,K_A,S_A,ORG_A,VIA_A,POS_A,TERM_A,Gate_A,Land_Onbl_A,Belt_A,I_A,Type_A,Departure_D,ID_D,J_D,STD_D,A_D,Na_D,K_D,S_D,VIA_D,DES_D,POS_D,TERM_D,Gate_D,Ofbl_Airb_D,CKI_D,Ch_F_T,I_D";
		private string strLogicalFieldsRotation = "Arrival_A,ID_A,J_A,Sched_A,A_A,Na_A,S_A,ORG_A,VIA_A,POS_A,TERM_A,Gate_A,Land_Onbl_A,Belt_A,I_A,Type_A,Departure_D,ID_D,J_D,STD_D,A_D,Na_D,S_D,VIA_D,DES_D,POS_D,TERM_D,Gate_D,Ofbl_Airb_D,CKI_D,Ch_F_T,I_D";
		/// <summary>
		/// The header columns, which must be used for the report output
		/// </summary>
		/// <summary>
		private string strTabHeader =     "Flight  ,A/D  ,Org/Des  ,STD  ,Actual  ,A/C  , On/Ofbl  ,Nat.  ,Reg";
		/// The header columns, which must be used for the report output
		/// for rotation
		/// </summary>
		private string strTabHeaderRotation = "Arrival,ID,J,STA,A,Na,S,ORG,VIA,POS,Term,Gate,Land/Onbl,Belt,I,Type,Departure,ID,J,STD,A,Na,S,VIA,DES,POS,Term,Gate,Ofbl/Airb,Cki,Chute  From/To,I";
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// </summary>
		private string strTabHeaderLens = "60,30,50,70,70,40,60,40,70";
		/// <summary>
		/// The lengths, which must be used for the report's column widths
		/// for rotation
		/// </summary>
		private string strTabHeaderLensRotation = "43,16,15,45,15,27,20,32,35,35,20,36,90,26,14,32,43,18,13,45,13,29,20,35,31,35,20,36,90,55,100,15";
		/// <summary>
		/// The lengths, which must be used for the report's excel export column widths
		/// for rotation
		/// </summary>
		private string strExcelTabHeaderLensRotation = "54,16,15,105,15,27,24,32,43,27,20,50,220,26,14,32,54,18,13,105,13,29,22,41,31,27,20,50,220,70,105,15";
		/// <summary>
		/// The one and only IDatabase obejct.
		/// </summary>
		private IDatabase myDB;
		/// <summary>
		/// ITable object for the AFTTAB
		/// </summary>
		private ITable myAFT;
		/// <summary>
		/// ITable object for the nature codes of NATTAB
		/// </summary>
		private ITable myNAT;
		/// <summary>
		/// ITable object for the chutes table CHATAB
		/// </summary>
		private ITable myCHA;
		/// <summary>
		/// The nature code as global valid member.
		/// </summary>
		private string strNatureCode = "";
		/// <summary>
		/// Airline 2-3 letter code
		/// </summary>
		private string strAirportCode = "";
		/// <summary>
		/// Total of arrival flights in the report
		/// </summary>
		private int imArrivals = 0;
		/// <summary>
		/// Total of departure flights in the report
		/// </summary>
		private int imDepartures = 0;
		/// <summary>
		/// Total of rotationein the report
		/// </summary>
		private int imRotations = 0;
		/// <summary>
		/// Total of all flights in the report
		/// </summary>
		/// <summary>
		/// variable to store the arrival and departure flights
		/// </summary>
		private int[] NumberofFlights;

		private int imTotalFlights = 0;
		/// <summary>
		/// Datetime format for excel export
		/// </summary>
		private string strExcelDateTimeFormat = "";
		/// <summary>
		/// Datetime format for report
		/// </summary>
		private string strReportDateTimeFormat = "dd'/'HH:mm";
		/// <summary>
		/// Datetime format for table
		/// </summary>
		private string strDateDisplayFormat = "";
		/// <summary>
		/// Datetime separator 
		/// </summary>
		private string strDateTimeSeparator = " / ";
		/// <summary>
		/// Boolean for mouse status with respect to tabResult control
		/// </summary>
		private bool bmMouseInsideTabControl = false;
		/// <summary>
		/// Report Header
		/// </summary>
		private string strReportHeader = "";
		/// <summary>
		/// Report Sub Header
		/// </summary>
		private string strReportSubHeader = "";
		/// <summary>
		/// Report Object
		/// </summary>
		private DataDynamics.ActiveReports.ActiveReport3 activeReport = null;
		/// <summary>
		/// Boolean to suggest if "Chutes from/to" column should be visible or not
		/// </summary>
		private bool bmChuteDisplay = false;
		/// <summary>
		/// Boolean for showing stev
		/// </summary>
		private bool bmShowStev = false;
		#endregion _My Members

		private System.Windows.Forms.Panel panelBody;
		private System.Windows.Forms.Panel panelTab;
		private AxTABLib.AxTAB tabResult;
		private AxTABLib.AxTAB tabExcelResult;
		private System.Windows.Forms.Label lblResults;
		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.TextBox txtAirport;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label lblProgress;
		private System.Windows.Forms.Button btnLoadPrint;
		private System.Windows.Forms.Button btnPrintPreview;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.DateTimePicker dtTo;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dtFrom;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox cbNatures;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.DateTimePicker timeFrom;
		private System.Windows.Forms.DateTimePicker timeTo;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.CheckBox checkBoxSTASTD;
		private System.Windows.Forms.CheckBox checkBoxTIFATIFD;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.TextBox txtLens;
		private System.Windows.Forms.DateTimePicker dtDuration;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.CheckBox checkBoxSingleArrivals;
		private System.Windows.Forms.Button buttonHelp;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmOvernightFlights()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeMouseEvents();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmOvernightFlights));
			this.panelBody = new System.Windows.Forms.Panel();
			this.panelTab = new System.Windows.Forms.Panel();
			this.tabResult = new AxTABLib.AxTAB();
			this.tabExcelResult = new AxTABLib.AxTAB();
			this.lblResults = new System.Windows.Forms.Label();
			this.panelTop = new System.Windows.Forms.Panel();
			this.checkBoxTIFATIFD = new System.Windows.Forms.CheckBox();
			this.checkBoxSTASTD = new System.Windows.Forms.CheckBox();
			this.label7 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.timeTo = new System.Windows.Forms.DateTimePicker();
			this.timeFrom = new System.Windows.Forms.DateTimePicker();
			this.button1 = new System.Windows.Forms.Button();
			this.dtDuration = new System.Windows.Forms.DateTimePicker();
			this.label5 = new System.Windows.Forms.Label();
			this.txtAirport = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.btnLoadPrint = new System.Windows.Forms.Button();
			this.btnPrintPreview = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.cbNatures = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.dtTo = new System.Windows.Forms.DateTimePicker();
			this.label2 = new System.Windows.Forms.Label();
			this.dtFrom = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.lblProgress = new System.Windows.Forms.Label();
			this.txtLens = new System.Windows.Forms.TextBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.checkBoxSingleArrivals = new System.Windows.Forms.CheckBox();
			this.buttonHelp = new System.Windows.Forms.Button();
			this.panelBody.SuspendLayout();
			this.panelTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabExcelResult)).BeginInit();
			this.panelTop.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelBody
			// 
			this.panelBody.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelBody.Controls.Add(this.panelTab);
			this.panelBody.Controls.Add(this.lblResults);
			this.panelBody.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelBody.Location = new System.Drawing.Point(0, 240);
			this.panelBody.Name = "panelBody";
			this.panelBody.Size = new System.Drawing.Size(780, 282);
			this.panelBody.TabIndex = 3;
			// 
			// panelTab
			// 
			this.panelTab.Controls.Add(this.tabResult);
			this.panelTab.Controls.Add(this.tabExcelResult);
			this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelTab.Location = new System.Drawing.Point(0, 16);
			this.panelTab.Name = "panelTab";
			this.panelTab.Size = new System.Drawing.Size(780, 266);
			this.panelTab.TabIndex = 2;
			// 
			// tabResult
			// 
			this.tabResult.ContainingControl = this;
			this.tabResult.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabResult.Location = new System.Drawing.Point(0, 0);
			this.tabResult.Name = "tabResult";
			this.tabResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabResult.OcxState")));
			this.tabResult.Size = new System.Drawing.Size(780, 266);
			this.tabResult.TabIndex = 0;
			this.tabResult.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabResult_SendLButtonDblClick);
			// 
			// tabExcelResult
			// 
			this.tabExcelResult.ContainingControl = this;
			this.tabExcelResult.Location = new System.Drawing.Point(0, 0);
			this.tabExcelResult.Name = "tabExcelResult";
			this.tabExcelResult.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabExcelResult.OcxState")));
			this.tabExcelResult.Size = new System.Drawing.Size(100, 50);
			this.tabExcelResult.TabIndex = 1;
			// 
			// lblResults
			// 
			this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblResults.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblResults.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblResults.Location = new System.Drawing.Point(0, 0);
			this.lblResults.Name = "lblResults";
			this.lblResults.Size = new System.Drawing.Size(780, 16);
			this.lblResults.TabIndex = 1;
			this.lblResults.Text = "Report Results";
			this.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panelTop
			// 
			this.panelTop.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.panelTop.Controls.Add(this.buttonHelp);
			this.panelTop.Controls.Add(this.checkBoxTIFATIFD);
			this.panelTop.Controls.Add(this.checkBoxSTASTD);
			this.panelTop.Controls.Add(this.label7);
			this.panelTop.Controls.Add(this.label6);
			this.panelTop.Controls.Add(this.timeTo);
			this.panelTop.Controls.Add(this.timeFrom);
			this.panelTop.Controls.Add(this.button1);
			this.panelTop.Controls.Add(this.dtDuration);
			this.panelTop.Controls.Add(this.label5);
			this.panelTop.Controls.Add(this.txtAirport);
			this.panelTop.Controls.Add(this.label4);
			this.panelTop.Controls.Add(this.progressBar1);
			this.panelTop.Controls.Add(this.btnLoadPrint);
			this.panelTop.Controls.Add(this.btnPrintPreview);
			this.panelTop.Controls.Add(this.btnCancel);
			this.panelTop.Controls.Add(this.btnOK);
			this.panelTop.Controls.Add(this.cbNatures);
			this.panelTop.Controls.Add(this.label3);
			this.panelTop.Controls.Add(this.dtTo);
			this.panelTop.Controls.Add(this.label2);
			this.panelTop.Controls.Add(this.dtFrom);
			this.panelTop.Controls.Add(this.label1);
			this.panelTop.Controls.Add(this.groupBox1);
			this.panelTop.Controls.Add(this.groupBox2);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(780, 240);
			this.panelTop.TabIndex = 2;
			// 
			// checkBoxTIFATIFD
			// 
			this.checkBoxTIFATIFD.Location = new System.Drawing.Point(480, 156);
			this.checkBoxTIFATIFD.Name = "checkBoxTIFATIFD";
			this.checkBoxTIFATIFD.Size = new System.Drawing.Size(72, 24);
			this.checkBoxTIFATIFD.TabIndex = 26;
			this.checkBoxTIFATIFD.Text = "ATA/ATD";
			// 
			// checkBoxSTASTD
			// 
			this.checkBoxSTASTD.Checked = true;
			this.checkBoxSTASTD.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxSTASTD.Location = new System.Drawing.Point(400, 156);
			this.checkBoxSTASTD.Name = "checkBoxSTASTD";
			this.checkBoxSTASTD.Size = new System.Drawing.Size(72, 24);
			this.checkBoxSTASTD.TabIndex = 25;
			this.checkBoxSTASTD.Text = "STA/STD";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(28, 160);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(64, 16);
			this.label7.TabIndex = 24;
			this.label7.Text = "Time from:";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(232, 160);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(24, 16);
			this.label6.TabIndex = 23;
			this.label6.Text = "to:";
			// 
			// timeTo
			// 
			this.timeTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.timeTo.Location = new System.Drawing.Point(260, 156);
			this.timeTo.Name = "timeTo";
			this.timeTo.ShowUpDown = true;
			this.timeTo.Size = new System.Drawing.Size(128, 20);
			this.timeTo.TabIndex = 22;
			// 
			// timeFrom
			// 
			this.timeFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.timeFrom.Location = new System.Drawing.Point(100, 156);
			this.timeFrom.Name = "timeFrom";
			this.timeFrom.ShowUpDown = true;
			this.timeFrom.Size = new System.Drawing.Size(128, 20);
			this.timeFrom.TabIndex = 21;
			this.timeFrom.ValueChanged += new System.EventHandler(this.timeFrom_ValueChanged);
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(224)), ((System.Byte)(192)));
			this.button1.Location = new System.Drawing.Point(456, 72);
			this.button1.Name = "button1";
			this.button1.TabIndex = 20;
			this.button1.Text = "button1";
			this.button1.Visible = false;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// dtDuration
			// 
			this.dtDuration.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtDuration.Location = new System.Drawing.Point(608, 72);
			this.dtDuration.Name = "dtDuration";
			this.dtDuration.ShowUpDown = true;
			this.dtDuration.Size = new System.Drawing.Size(128, 20);
			this.dtDuration.TabIndex = 2;
			this.dtDuration.Value = new System.DateTime(2008, 5, 8, 11, 3, 0, 984);
			this.dtDuration.Visible = false;
			// 
			// label5
			// 
			this.label5.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.label5.Location = new System.Drawing.Point(540, 76);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(64, 16);
			this.label5.TabIndex = 18;
			this.label5.Text = "Duration >=";
			this.label5.Visible = false;
			// 
			// txtAirport
			// 
			this.txtAirport.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtAirport.Location = new System.Drawing.Point(100, 96);
			this.txtAirport.Name = "txtAirport";
			this.txtAirport.TabIndex = 5;
			this.txtAirport.Text = "";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(28, 100);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(64, 16);
			this.label4.TabIndex = 17;
			this.label4.Text = "Airport:";
			// 
			// progressBar1
			// 
			this.progressBar1.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.progressBar1.Location = new System.Drawing.Point(508, 200);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(216, 23);
			this.progressBar1.TabIndex = 10;
			this.progressBar1.Visible = false;
			// 
			// btnLoadPrint
			// 
			this.btnLoadPrint.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnLoadPrint.Location = new System.Drawing.Point(252, 200);
			this.btnLoadPrint.Name = "btnLoadPrint";
			this.btnLoadPrint.TabIndex = 8;
			this.btnLoadPrint.Text = "Loa&d + Print";
			this.btnLoadPrint.Click += new System.EventHandler(this.btnLoadPrint_Click);
			// 
			// btnPrintPreview
			// 
			this.btnPrintPreview.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnPrintPreview.Location = new System.Drawing.Point(176, 200);
			this.btnPrintPreview.Name = "btnPrintPreview";
			this.btnPrintPreview.TabIndex = 7;
			this.btnPrintPreview.Text = "&Print Preview";
			this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreview_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(328, 200);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 9;
			this.btnCancel.Text = "&Close";
			// 
			// btnOK
			// 
			this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnOK.Location = new System.Drawing.Point(100, 200);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 6;
			this.btnOK.Text = "&Load Data";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// cbNatures
			// 
			this.cbNatures.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbNatures.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cbNatures.ItemHeight = 14;
			this.cbNatures.Location = new System.Drawing.Point(100, 64);
			this.cbNatures.MaxDropDownItems = 16;
			this.cbNatures.Name = "cbNatures";
			this.cbNatures.Size = new System.Drawing.Size(288, 22);
			this.cbNatures.TabIndex = 4;
			// 
			// label3
			// 
			this.label3.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.label3.Location = new System.Drawing.Point(28, 68);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(64, 16);
			this.label3.TabIndex = 4;
			this.label3.Text = "Nature:";
			// 
			// dtTo
			// 
			this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtTo.Location = new System.Drawing.Point(260, 32);
			this.dtTo.Name = "dtTo";
			this.dtTo.Size = new System.Drawing.Size(128, 20);
			this.dtTo.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(232, 36);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(24, 16);
			this.label2.TabIndex = 2;
			this.label2.Text = "to:";
			// 
			// dtFrom
			// 
			this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtFrom.Location = new System.Drawing.Point(100, 32);
			this.dtFrom.Name = "dtFrom";
			this.dtFrom.Size = new System.Drawing.Size(128, 20);
			this.dtFrom.TabIndex = 0;
			this.dtFrom.ValueChanged += new System.EventHandler(this.dtFrom_ValueChanged);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(28, 36);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Date from:";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.lblProgress);
			this.groupBox1.Controls.Add(this.txtLens);
			this.groupBox1.Location = new System.Drawing.Point(16, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(744, 116);
			this.groupBox1.TabIndex = 27;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Search Flights:";
			// 
			// lblProgress
			// 
			this.lblProgress.Location = new System.Drawing.Point(460, 28);
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.Size = new System.Drawing.Size(212, 16);
			this.lblProgress.TabIndex = 12;
			// 
			// txtLens
			// 
			this.txtLens.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(224)), ((System.Byte)(192)));
			this.txtLens.Location = new System.Drawing.Point(468, 80);
			this.txtLens.Name = "txtLens";
			this.txtLens.Size = new System.Drawing.Size(200, 20);
			this.txtLens.TabIndex = 19;
			this.txtLens.Text = "textBox1";
			this.txtLens.Visible = false;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.checkBoxSingleArrivals);
			this.groupBox2.Location = new System.Drawing.Point(16, 132);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(744, 56);
			this.groupBox2.TabIndex = 28;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Timeframe that define an overnight flight:";
			// 
			// checkBoxSingleArrivals
			// 
			this.checkBoxSingleArrivals.Location = new System.Drawing.Point(548, 24);
			this.checkBoxSingleArrivals.Name = "checkBoxSingleArrivals";
			this.checkBoxSingleArrivals.Size = new System.Drawing.Size(124, 24);
			this.checkBoxSingleArrivals.TabIndex = 0;
			this.checkBoxSingleArrivals.Text = "With Single Arrivals";
			this.checkBoxSingleArrivals.CheckedChanged += new System.EventHandler(this.checkBoxSingleArrivals_CheckedChanged);
			// 
			// buttonHelp
			// 
			this.buttonHelp.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.buttonHelp.Location = new System.Drawing.Point(404, 200);
			this.buttonHelp.Name = "buttonHelp";
			this.buttonHelp.TabIndex = 30;
			this.buttonHelp.Text = "Help";
			this.buttonHelp.Click += new System.EventHandler(this.buttonHelp_Click);
			// 
			// frmOvernightFlights
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(780, 522);
			this.Controls.Add(this.panelBody);
			this.Controls.Add(this.panelTop);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmOvernightFlights";
			this.Text = "Overnight Flights";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.frmOvernightFlights_Closing);
			this.Load += new System.EventHandler(this.frmOvernightFlights_Load);
			this.HelpRequested += new HelpEventHandler(frmOvernightFlights_HelpRequested);
			this.panelBody.ResumeLayout(false);
			this.panelTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabExcelResult)).EndInit();
			this.panelTop.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion
		
		/// <summary>
		/// Loads the from and inits the necessary things.
		/// </summary>
		/// <param name="sender">Originator of the method call</param>
		/// <param name="e">Event args</param>
		private void frmOvernightFlights_Load(object sender, System.EventArgs e)
		{
			myDB = UT.GetMemDB();
			InitTimePickers();
			SetupReportTabHeader();
			InitTab();
			PrepareFilterData();
		}
		/// <summary>
		/// initializes the From/to time control with the current day in
		/// the custom format "dd.MM.yyyy - HH:mm".
		/// </summary>
		private void InitTimePickers()
		{
			DateTime olFrom;
			DateTime olTo;
			DateTime olDura;
			
			olFrom = DateTime.Now;
			olTo = DateTime.Now;
			olFrom = olFrom.AddDays(-1);

			olTo   = new DateTime(olTo.Year, olTo.Month, olTo.Day, 23,59,0);
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 0,0,0);
			olDura = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 3,0,0);
			dtFrom.Value = olFrom;
			dtTo.Value = olTo;
			dtDuration.Value = olDura;
			dtFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtTo.CustomFormat = "dd.MM.yyyy - HH:mm";
			dtDuration.CustomFormat = "HH:mm";
			timeFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
			timeTo.CustomFormat = "dd.MM.yyyy - HH:mm";

			olTo   = new DateTime(olTo.Year, olTo.Month, olTo.Day, 02,00,0);
			olFrom = new DateTime(olFrom.Year, olFrom.Month, olFrom.Day, 23,0,0);

			timeFrom.Value = olFrom;
			timeTo.Value = olTo;
		
		}
		/// <summary>
		/// Initializes the tab control.
		/// </summary>
		private void InitTab()
		{
			tabResult.ResetContent();
			tabResult.ShowHorzScroller(true);
			tabResult.EnableHeaderSizing(true);
			tabResult.SetTabFontBold(true);
			tabResult.LifeStyle = true;
			tabResult.LineHeight = 16;
			tabResult.FontName = "Arial";
			tabResult.FontSize = 14;
			tabResult.HeaderFontSize = 14;
			tabResult.AutoSizeByHeader = true;
			tabResult.HeaderString = strTabHeaderRotation;
			tabResult.LogicalFieldList = strLogicalFieldsRotation;
			tabResult.HeaderLengthString = strTabHeaderLensRotation;

			tabExcelResult.ResetContent();			
			tabExcelResult.HeaderString = strTabHeaderRotation;
			tabExcelResult.LogicalFieldList = strLogicalFieldsRotation;
			tabExcelResult.HeaderLengthString = strTabHeaderLensRotation;
			
			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			strDateDisplayFormat = myIni.IniReadValue("FIPSREPORT", "DATEDISPLAYFORMAT");
			strExcelDateTimeFormat = myIni.IniReadValue("FIPSREPORT", "DATEEXCELFORMAT");
			if(strDateDisplayFormat.Length == 0)
			{
				strDateDisplayFormat = strReportDateTimeFormat;
			}
			else
			{
				strReportDateTimeFormat = strDateDisplayFormat;
			}
			if(strExcelDateTimeFormat.Length == 0)
			{
				strExcelDateTimeFormat = strReportDateTimeFormat;
			}

			PrepareReportDataRotations("Report");
			PrepareReportDataRotations("Excel");
		}

		/// <summary>
		/// Initializes the tab control header data.
		/// </summary>
		private void SetupReportTabHeader()
		{
			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			if(myIni.IniReadValue("FIPS","CHUTE_DISPLAY").CompareTo("TRUE") == 0)
			{
				bmChuteDisplay = true;
			}
			
			string[] strLabelHeaderEntry = new string[2];
			string strTerminalLabel = "";
			string strTerminalHeader = "";
			if(Helper.IsSTEVConfigured(this,ref strLabelHeaderEntry,true,true) == true)
			{
				bmShowStev = true;
				strTerminalLabel  = strLabelHeaderEntry[0];
				strTerminalHeader = strLabelHeaderEntry[1];				
				Helper.RemoveOrReplaceString(10,ref strTabHeaderRotation,',',strTerminalHeader,false);
				Helper.RemoveOrReplaceString(26,ref strTabHeaderRotation,',',strTerminalHeader,false);				
			}

			if(bmChuteDisplay == false)
			{
				Helper.RemoveOrReplaceString(0,ref strTabHeaderLensRotation,',',"50",false);				
				Helper.RemoveOrReplaceString(8,ref strTabHeaderLensRotation,',',"43",false);
				Helper.RemoveOrReplaceString(16,ref strTabHeaderLensRotation,',',"50",false);				
				Helper.RemoveOrReplaceString(23,ref strTabHeaderLensRotation,',',"43",false);
				Helper.RemoveOrReplaceString(29,ref strTabHeaderLensRotation,',',"65",false);

				Helper.RemoveOrReplaceString(30,ref strTabHeaderRotation,',',"",true);
				Helper.RemoveOrReplaceString(30,ref strLogicalFieldsRotation,',',"",true);
				Helper.RemoveOrReplaceString(30,ref strTabHeaderLensRotation,',',"",true);
				Helper.RemoveOrReplaceString(30,ref strExcelTabHeaderLensRotation,',',"",true);
			}

			if(bmShowStev == false)
			{
				Helper.RemoveOrReplaceString(10,ref strLogicalFieldsRotation,',',"",true);
				Helper.RemoveOrReplaceString(10,ref strTabHeaderRotation,',',"",true);
				Helper.RemoveOrReplaceString(10,ref strTabHeaderLensRotation,',',"",true);
				Helper.RemoveOrReplaceString(10,ref strExcelTabHeaderLensRotation,',',"",true);

				Helper.RemoveOrReplaceString(25,ref strLogicalFieldsRotation,',',"",true);
				Helper.RemoveOrReplaceString(25,ref strTabHeaderRotation,',',"",true);
				Helper.RemoveOrReplaceString(25,ref strTabHeaderLensRotation,',',"",true);
				Helper.RemoveOrReplaceString(25,ref strExcelTabHeaderLensRotation,',',"",true);
			}
		}
		/// <summary>
		/// Loads and prepare the data necessary for the filter. 
		/// In this case loads the nature codes and puts the
		/// data into the combobox.
		/// </summary>
		private void PrepareFilterData()
		{
			myNAT = myDB["NAT"];
			cbNatures.Items.Add("<None>");
			if(myNAT == null)
			{
				myNAT = myDB.Bind("NAT", "NAT", "TTYP,TNAM", "5,30", "TTYP,TNAM");
				myNAT.Load("ORDER BY TTYP");
			}
			else if(myNAT.Count == 0)
			{
				myNAT = myDB.Bind("NAT", "NAT", "TTYP,TNAM", "5,30", "TTYP,TNAM");
				myNAT.Load("ORDER BY TTYP");
			}
			for(int i = 0; i < myNAT.Count; i++)
			{
				string strValue = myNAT[i]["TTYP"] + " / " + myNAT[i]["TNAM"];
				cbNatures.Items.Add(strValue);
			}
			if(cbNatures.Items.Count > 0)
				cbNatures.SelectedIndex = 0;
		}

		/// <summary>
		/// Runs the data queries according to the reports needs and puts
		///	Calls the data prepare method and puts the prepared data into
		///	the tab control. After this is finished it calls the reports
		///	by sending the tab and the headers to the generic report.
		/// </summary>
		/// <param name="sender">Originator object of the call</param>
		/// <param name="e">Event arguments</param>
		private void btnOK_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			PrepareReportDataRotations("Report");
			PrepareReportDataRotations("Excel");
			//RunReport();
			this.Cursor = Cursors.Arrow;
			txtLens.Text = tabResult.HeaderLengthString;
		}
		/// <summary>
		/// Validates the user entry for nonsense.
		/// </summary>
		/// <returns></returns>
		private string ValidateUserEntry()
		{
			string strRet = "";
			int ilErrorCount = 1;

			//			TimeSpan span = dtTo.Value - dtFrom.Value;
			//			if(span.TotalDays > 10)
			//			{
			//				strRet += ilErrorCount.ToString() +  ". Please do not load more the 10 days!\n";
			//				ilErrorCount++;
			//			}
			if(dtFrom.Value >= dtTo.Value)
			{
				strRet += ilErrorCount.ToString() +  ". Date From is later than Date To!\n";
				ilErrorCount++;
			}
			if(cbNatures.Text == "")
			{
				strRet += ilErrorCount.ToString() +  ". Please select a nature code!\n";
				ilErrorCount++;
			}
			return strRet;
		}
		/// <summary>
		/// Loads all necessary tables with filter criteria from the 
		/// database ==> to memoryDB.
		/// </summary>
		private void LoadReportData()
		{
			string strTmpWhere = strWhereRaw;
			//Clear the tab
			tabResult.ResetContent();
			tabExcelResult.ResetContent();

			// In the first step change the times to UTC if this is
			// necessary.
			DateTime datFrom;
			DateTime datTo;
			string strDateFrom = "";
			string strDateTo = "";
			datFrom = dtFrom.Value;
			datTo   = dtTo.Value;
			if(UT.IsTimeInUtc == false)
			{
				datFrom = UT.LocalToUtc(dtFrom.Value);
				datTo   = UT.LocalToUtc(dtTo.Value);
				strDateFrom = UT.DateTimeToCeda( datFrom );
				strDateTo = UT.DateTimeToCeda( datTo );
			}
			else
			{
				strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
				strDateTo = UT.DateTimeToCeda(dtTo.Value);
			}
			// Extract the true Nature code from the selected entry of the combobox
			strNatureCode = "";
			string [] strArr = cbNatures.Text.Split('/');
			strNatureCode = strArr[0].Trim();
			strAirportCode = txtAirport.Text;

			//Now reading day by day the flight records in order to 
			//not stress the database too much
			myDB.Unbind("AFT");
			// 10 , 10 , 9  ,  8 , 1  , 1  , 14 , 1  ,  5 , 1  , 2  , 3  , 4  , 3  , 4  , 2  , 5  , 5  , 5  , 5  , 5  , 1  , 3  , 14 , 1  , 3  , 4  , 5  ,  5 , 5  , 5  , 5
			//RKEY,URNO,FLNO,CSGN,FLTI,JCNT,STOA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT
			myAFT = myDB.Bind("AFT", "AFT", 
				"RKEY,URNO,ADID,FLNO,CSGN,FLTI,JCNT,STOA,TIFA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,TIFD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT,REGN,LAND,ONBL,OFBL,AIRB,RTYP", 
				"10,10,1,9,8,1,1,14,14,1,5,1,2,3,4,3,4,2,5,5,5,5,5,1,3,14,14,1,3,4,5,5,5,5,5,12,14,14,14,14,1",
				"RKEY,URNO,ADID,FLNO,CSGN,FLTI,JCNT,STOA,TIFA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,TIFD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT,REGN,LAND,ONBL,OFBL,AIRB,RTYP");
			myAFT.Clear();
			myAFT.TimeFields = "STOA,STOD,TIFA,TIFD,ONBL,OFBL,LAND,AIRB";
			myAFT.Command("read",",GFR,");
			ArrayList myUniques = new ArrayList();
			myUniques.Add("URNO");
			myAFT.UniqueFields = myUniques;
			myAFT.TimeFieldsInitiallyInUtc = true;

			//Prepare loop reading day by day
			DateTime datReadFrom = datFrom;
			DateTime datReadTo;
			TimeSpan tsDays = (datTo - datFrom);
			progressBar1.Value = 0;
			int ilTotal = Convert.ToInt32(tsDays.TotalDays);
			if(ilTotal == 0) ilTotal = 1;
			lblProgress.Text = "Loading Data";
			lblProgress.Refresh();
			progressBar1.Show();
			int loopCnt = 1;
			do
			{
				int percent = Convert.ToInt32((loopCnt * 100)/ilTotal);
				if (percent > 100) 
					percent = 100;
				progressBar1.Value = percent;
				datReadTo = datReadFrom + new TimeSpan(1, 0, 0, 0, 0);
				if( datReadTo > datTo) datReadTo = datTo;
				strDateFrom = UT.DateTimeToCeda(datReadFrom);
				strDateTo = UT.DateTimeToCeda(datReadTo);
				//patch the where statement according to the user's entry
				strTmpWhere = strWhereRaw;
				if(strNatureCode != "<None>")
					strTmpWhere += strNatWhere;
				if(strAirportCode != "" )
					strTmpWhere += strORGDESWhere;
				strTmpWhere = strTmpWhere.Replace("@@FROM", strDateFrom);
				strTmpWhere = strTmpWhere.Replace("@@TO", strDateTo );
				strTmpWhere = strTmpWhere.Replace("@@NAT", strNatureCode );
				strTmpWhere = strTmpWhere.Replace("@@APT", strAirportCode);
				strTmpWhere += "[ROTATIONS]";
				//Load the data from AFTTAB
				myAFT.Load(strTmpWhere);
				datReadFrom += new TimeSpan(1, 0, 0, 0, 0);
				loopCnt++;
			}while(datReadFrom <= datReadTo);

			if(this.checkBoxSingleArrivals.Checked == true)
			{				
				string strtimeFromDate = "";
				if(UT.IsTimeInUtc == false)
				{					
					strtimeFromDate = UT.DateTimeToCeda(UT.LocalToUtc(timeFrom.Value));
					strDateFrom = UT.DateTimeToCeda(UT.LocalToUtc(dtFrom.Value));
				}
				else
				{
					strtimeFromDate = UT.DateTimeToCeda(timeFrom.Value);
					strDateFrom = UT.DateTimeToCeda(dtFrom.Value);
				}				
				strTmpWhere = strWhereSingleArrivals;
				if(strNatureCode != "<None>")
					strTmpWhere += strNatWhere;
				if(strAirportCode != "" )
					strTmpWhere += strORGDESWhere;
				strTmpWhere = strTmpWhere.Replace("@@FROM", strDateFrom);
				strTmpWhere = strTmpWhere.Replace("@@TIMEFROM", strtimeFromDate );
				strTmpWhere = strTmpWhere.Replace("@@NAT", strNatureCode );
				strTmpWhere = strTmpWhere.Replace("@@APT", strAirportCode);				
				//Load the data from AFTTAB
				myAFT.Load(strTmpWhere);
			}			
			
			//Filter out the FTYPs which are selected due to rotations
			for(int i = myAFT.Count-1; i >= 0; i--)
			{
				switch(myAFT[i]["FTYP"])
				{
					case "T":
					case "G":
					case "N":
					case "X":
						myAFT.Remove(i);
						break;
				}
			}

			myAFT.TimeFieldsCurrentlyInUtc = UT.IsTimeInUtc;

			if(bmChuteDisplay == true)
			{
				//-------------------------------------------------------------------
				//Load chutes according to the AFT.URNO->CHA.RURN
				myDB.Unbind("DED_CHA");
				myCHA = myDB.Bind("DED_CHA", "CHA", "RURN,LNAM,STOB,STOE,TIFB,TIFE,LTYP,CHUF,CHUT", "10,5,14,14,14,14,3,5,5,3", "RURN,LNAM,STOB,STOE,TIFB,TIFE,LTYP");
				myCHA.Clear();
				lblProgress.Text = "Loading Chute Data";
				lblProgress.Refresh();
				int loops = 0;
				progressBar1.Value = 0;
				progressBar1.Refresh();
				ilTotal = (int)(myAFT.Count/300);
				if(ilTotal == 0) ilTotal = 1;
				string strRurns = "";
				string strTmpCHAWhere = strDedicatedCHAWhere;
				for(int i = 0; i < myAFT.Count; i++)
				{
					strRurns += myAFT[i]["URNO"] + ",";
					if((i % 300) == 0 && i > 0)
					{
						loops++;
						int percent = Convert.ToInt32((loops * 100)/ilTotal);
						if (percent > 100) 
							percent = 100;
						progressBar1.Value = percent;
						strRurns = strRurns.Remove(strRurns.Length-1, 1);
						strTmpCHAWhere = strDedicatedCHAWhere;
						strTmpCHAWhere = strTmpCHAWhere.Replace("@@RURN", strRurns);
						myCHA.Load(strTmpCHAWhere);
						strRurns = "";
					}
				}
				//Load the rest of the dedicated Chutes.
				if(strRurns.Length > 0)
				{
					strRurns = strRurns.Remove(strRurns.Length-1, 1);
					strTmpCHAWhere = strDedicatedCHAWhere;
					strTmpCHAWhere = strTmpCHAWhere.Replace("@@RURN", strRurns);
					myCHA.Load(strTmpCHAWhere);
					strRurns = "";
				}
				myCHA.Sort("STOB,TIFB,STOE,TIFE", true);
				myCHA.CreateIndex("RURN", "RURN");
			}

			lblProgress.Text = "";
			progressBar1.Hide();
		}

		/// <summary>
		/// Prepare the loaded data according to the report's needs.
		/// First organize the rotations and then put the result
		/// into the tabResult
		/// </summary>
		private void PrepareReportDataRotations(string strReportOrExcel)
		{
			NumberofFlights = new int [4];
			if(myAFT == null)
				return;

			myAFT.Sort("RKEY", true);
			int llCurr = 0;
			int ilStep = 1;
			string strCurrAdid = "";
			string strNextAdid = "";
			string strCurrRkey = "";
			string strNextRkey = "";
			string strTime = "";
			DateTime tmpDate;
			imArrivals = 0;
			imDepartures =0 ;
			imRotations = 0;
			imTotalFlights = 0;
			StringBuilder sb = new StringBuilder(10000);

			string strDateTimeFormat = strReportDateTimeFormat;
			if(strReportOrExcel.Equals("Excel") == true)
			{
				strDateTimeFormat = strExcelDateTimeFormat;
			}

			while(llCurr < myAFT.Count)
			{
				string strValues = "";
				strCurrRkey = myAFT[llCurr]["RKEY"];
				strCurrAdid = myAFT[llCurr]["ADID"];
				if ((llCurr + 1) < myAFT.Count)
				{
					strNextRkey = myAFT[llCurr+1]["RKEY"];
					strNextAdid = myAFT[llCurr+1]["ADID"];
				}
				else
				{
					strNextRkey = "";
					strNextAdid = "";
				}

				if(strCurrRkey == strNextRkey)
				{//Is a rotation
					ilStep = 2;
					if( strCurrAdid == "A" && strNextAdid == "D")
					{
						DateTime timeSTOA = UT.CedaFullDateToDateTime(myAFT[llCurr]["STOA"]);
						DateTime timeSTOD = UT.CedaFullDateToDateTime(myAFT[llCurr+1]["STOD"]);

						DateTime timeARR = new DateTime(1980,1,1,0,0,0) ;
						DateTime timeDEP = new DateTime(1980,1,1,0,0,0) ;
						
						strTime = myAFT[llCurr]["TIFA"];
						if(strTime != "")
							timeARR = UT.CedaFullDateToDateTime(strTime);
						strTime = myAFT[llCurr+1]["TIFD"];
						if(strTime != "")
							timeDEP = UT.CedaFullDateToDateTime(strTime);
						int ilDura = (dtDuration.Value.Hour * 60) + dtDuration.Value.Minute;


						DateTime timeFromDate = new DateTime(timeFrom.Value.Year, timeFrom.Value.Month, timeFrom.Value.Day, timeFrom.Value.Hour, timeFrom.Value.Minute, 0 );
						DateTime timeToDate = new DateTime(timeTo.Value.Year, timeTo.Value.Month, timeTo.Value.Day, timeTo.Value.Hour, timeTo.Value.Minute, 0 );

						/*
						if(UT.IsTimeInUtc == false)
						{
							timeFromDate = UT.LocalToUtc(timeFromDate);
							timeToDate = UT.LocalToUtc(timeToDate);
						}
						*/
						TimeSpan ts = timeDEP - timeARR;
						//if( (ts.TotalMinutes >= ilDura) && timeARR.Day != timeDEP.Day )
						
						bool show = false;
						
						if(checkBoxSTASTD.Checked)
						{
							if(timeSTOA < timeFromDate && timeSTOD > timeToDate)
								show= true;				
						}
						
						if(checkBoxTIFATIFD.Checked)
						{
							if(timeARR < timeFromDate && timeDEP > timeToDate)
								show= true;				
						}

						if(show)
						{
							NumberofFlights[0]++;
							NumberofFlights[1]++;
							//The arrival part of rotation
							//RKEY,URNO,FLNO,CSGN,FLTI,JCNT,STOA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT
							//Arrival,ID,Sched,A,Na,K,S,ORG,VIA,POS,Gate1,Gate2,Belt,I,Type,Departure,ID,J,STD,A,Na,K,S,VIA,DES,POS,Gate1,Gate2,I
							//ARR:,,,,,,,,,,,,,,,
							//DEP:,,,,,,,,,,,,,
							string strFLNO = myAFT[llCurr]["FLNO"];
							if(strFLNO == "") strFLNO = myAFT[llCurr]["CSGN"];
							strValues += strFLNO + ",";
							strValues += myAFT[llCurr]["FLTI"] + ",";
							strValues += myAFT[llCurr]["JCNT"] + ",";
							strValues += Helper.DateString(myAFT[llCurr]["STOA"], strDateTimeFormat) + ",";
							strValues += myAFT[llCurr]["DOOA"] + ",";
							strValues += myAFT[llCurr]["TTYP"] + ",";							
							strValues += myAFT[llCurr]["STYP"] + ",";
							strValues += myAFT[llCurr]["ORG3"] + ",";
							string strVian = myAFT[llCurr]["VIAN"];
							if(strVian != "" && strVian != "0") strVian = "/" + strVian;
							strValues += myAFT[llCurr]["VIA3"] + strVian + ",";
							strValues += myAFT[llCurr]["PSTA"] + ",";
							if(bmShowStev == true)
							{
								strValues += myAFT[llCurr]["STEV"] + ",";
							}
							if(myAFT[llCurr]["GTA1"].Trim().Length > 0 && myAFT[llCurr]["GTA2"].Trim().Length > 0)
							{
								strValues += myAFT[llCurr]["GTA1"] + "/" + myAFT[llCurr]["GTA2"] + ",";
							}
							else if(myAFT[llCurr]["GTA1"].Trim().Length > 0)
							{
								strValues += myAFT[llCurr]["GTA1"] + ",";
							}
							else if(myAFT[llCurr]["GTA2"].Trim().Length > 0)
							{
								strValues += "/" + myAFT[llCurr]["GTA2"] + ",";
							}
							else
							{
								strValues += ",";
							}
							if(myAFT[llCurr]["LAND"].Trim().Length > 0 && myAFT[llCurr]["ONBL"].Trim().Length > 0)
							{
								strValues += Helper.DateString(myAFT[llCurr]["LAND"], strDateTimeFormat) + strDateTimeSeparator + Helper.DateString(myAFT[llCurr]["ONBL"], strDateTimeFormat) + ",";
							}
							else if(myAFT[llCurr]["LAND"].Trim().Length > 0)
							{
								strValues += Helper.DateString(myAFT[llCurr]["LAND"],strDateTimeFormat) + ",";
							}
							else if(myAFT[llCurr]["ONBL"].Trim().Length > 0)
							{
								strValues += strDateTimeSeparator + Helper.DateString(myAFT[llCurr]["ONBL"],strDateTimeFormat) + ",";
							}
							else
							{
								strValues += "," ;
							}							
							strValues += myAFT[llCurr]["BLT1"] + " " + myAFT[llCurr]["BLT2"] + ",";
							strValues += myAFT[llCurr]["ISRE"] + ",";
							strValues += myAFT[llCurr]["ACT3"] + ",";
							//-----------------------------------The departure part of rotation
							strFLNO = myAFT[llCurr+1]["FLNO"];
							if(strFLNO == "") strFLNO = myAFT[llCurr+1]["CSGN"];
							strValues += strFLNO + ",";
							strValues += myAFT[llCurr+1]["FLTI"] + ",";
							strValues += myAFT[llCurr+1]["JCNT"] + ",";
							strValues += Helper.DateString(myAFT[llCurr+1]["STOD"], strDateTimeFormat) + ",";
							strValues += myAFT[llCurr+1]["DOOD"] + ",";
							strValues += myAFT[llCurr+1]["TTYP"] + ",";							
							strValues += myAFT[llCurr+1]["STYP"] + ",";
							strVian = myAFT[llCurr]["VIAN"];
							if(strVian != "" && strVian != "0") strVian = "/" + strVian;
							strValues += myAFT[llCurr+1]["VIA3"] + strVian + ",";
							strValues += myAFT[llCurr+1]["DES3"] + ",";
							strValues += myAFT[llCurr+1]["PSTD"] + ",";
							if(bmShowStev == true)
							{
								strValues += myAFT[llCurr+1]["STEV"] + ",";
							}
							if(myAFT[llCurr+1]["GTD1"].Trim().Length > 0 && myAFT[llCurr+1]["GTD2"].Length > 0)
							{
								strValues += myAFT[llCurr+1]["GTD1"] + "/" + myAFT[llCurr+1]["GTD2"] + ",";
							}
							else if(myAFT[llCurr+1]["GTD1"].Trim().Length > 0)
							{
								strValues += myAFT[llCurr+1]["GTD1"] + ",";
							}
							else if(myAFT[llCurr+1]["GTD2"].Trim().Length > 0)
							{
								strValues += "/" + myAFT[llCurr+1]["GTD2"] + ",";
							}
							else
							{
								strValues += ",";
							}
							if(myAFT[llCurr+1]["OFBL"].Trim().Length > 0 && myAFT[llCurr+1]["AIRB"].Trim().Length > 0)
							{
								strValues += Helper.DateString(myAFT[llCurr+1]["OFBL"], strDateTimeFormat) + strDateTimeSeparator + Helper.DateString(myAFT[llCurr+1]["AIRB"], strDateTimeFormat) + ",";
							}
							else if(myAFT[llCurr+1]["OFBL"].Trim().Length > 0)
							{
								strValues += Helper.DateString(myAFT[llCurr+1]["OFBL"], strDateTimeFormat) + ",";
							}
							else if(myAFT[llCurr+1]["AIRB"].Trim().Length > 0)
							{
								strValues += strDateTimeSeparator + Helper.DateString(myAFT[llCurr+1]["AIRB"], strDateTimeFormat) + ",";
							}
							else
							{
								strValues += ",";
							}
							strValues += myAFT[llCurr+1]["CKIF"] + " " + myAFT[llCurr+1]["CKIT"] + ",";

							bool blDataAppended = false;
							if(bmChuteDisplay == true)
							{
								IRow [] rows = myCHA.RowsByIndexValue("RURN", myAFT[llCurr+1]["URNO"]);
								if(rows.Length == 0)
								{
									strValues += ",";								
								}
								else
								{
									string[] chuteData = GetChuteData(rows);									
									for(int i = 0 ; i < chuteData.Length; i++)
									{
										string strRowValue = strValues;
										strRowValue += chuteData[i] + ",";
										strRowValue += myAFT[llCurr+1]["ISRE"] + "\n";
										sb.Append(strRowValue);
									}
									blDataAppended = true;
									//this.AppendChuteFromTo(ref strValues,rows,sb);
								}
							}
							if(blDataAppended == false)
							{
								strValues += myAFT[llCurr+1]["ISRE"] + "\n";
								sb.Append(strValues);
							}
						}
					}
					if( strCurrAdid == "D" && strNextAdid == "A")
					{
						DateTime timeSTOA = UT.CedaFullDateToDateTime(myAFT[llCurr+1]["STOA"]);
						DateTime timeSTOD = UT.CedaFullDateToDateTime(myAFT[llCurr]["STOD"]);
/*
						strTime = myAFT[llCurr+1]["TIFA"];
						if(strTime != "")
							timeARR = UT.CedaFullDateToDateTime(strTime);
						strTime = myAFT[llCurr]["TIFD"];
						if(strTime != "")
							timeDEP = UT.CedaFullDateToDateTime(strTime);
						int ilDura = (dtDuration.Value.Hour * 60) + dtDuration.Value.Minute;
						TimeSpan ts = timeDEP - timeARR;
						
						
						if( (ts.TotalMinutes >= ilDura) && timeARR.Day != timeDEP.Day )
							*/

						DateTime timeARR = new DateTime(1980,1,1,0,0,0) ;
						DateTime timeDEP = new DateTime(1980,1,1,0,0,0) ;
						
						strTime = myAFT[llCurr+1]["TIFA"];
						if(strTime != "")
							timeARR = UT.CedaFullDateToDateTime(strTime);
						strTime = myAFT[llCurr]["TIFD"];
						if(strTime != "")
							timeDEP = UT.CedaFullDateToDateTime(strTime);
						int ilDura = (dtDuration.Value.Hour * 60) + dtDuration.Value.Minute;


						DateTime timeFromDate = new DateTime(timeFrom.Value.Year, timeFrom.Value.Month, timeFrom.Value.Day, timeFrom.Value.Hour, timeFrom.Value.Minute, 0 );
						DateTime timeToDate = new DateTime(timeTo.Value.Year, timeTo.Value.Month, timeTo.Value.Day, timeTo.Value.Hour, timeTo.Value.Minute, 0 );

						/*
						if(UT.IsTimeInUtc == false)
						{
							timeFromDate = UT.LocalToUtc(timeFromDate);
							timeToDate = UT.LocalToUtc(timeToDate);
						}
						*/
						TimeSpan ts = timeDEP - timeARR;
						//if( (ts.TotalMinutes >= ilDura) && timeARR.Day != timeDEP.Day )
						
						bool show = false;
						
						if(checkBoxSTASTD.Checked)
						{
							if(timeSTOA < timeFromDate && timeSTOD > timeToDate)
								show= true;				
						}
						
						if(checkBoxTIFATIFD.Checked)
						{
							if(timeARR < timeFromDate && timeDEP > timeToDate)
								show= true;				
						}

						if(show)

						{
							NumberofFlights[2]++;
							NumberofFlights[3]++;
							//The arrival part of rotation
							string strFLNO = myAFT[llCurr+1]["FLNO"];
							if(strFLNO == "") strFLNO = myAFT[llCurr+1]["CSGN"];
							strValues += strFLNO + ",";
							strValues += myAFT[llCurr+1]["FLTI"] + ",";
							strValues += myAFT[llCurr+1]["JCNT"] + ",";
							strValues += Helper.DateString(myAFT[llCurr+1]["STOA"], strDateTimeFormat) + ",";
							strValues += myAFT[llCurr+1]["DOOA"] + ",";
							strValues += myAFT[llCurr+1]["TTYP"] + ",";							
							strValues += myAFT[llCurr+1]["STYP"] + ",";
							strValues += myAFT[llCurr+1]["ORG3"] + ",";
							string strVian = myAFT[llCurr+1]["VIAN"];
							if(strVian != "" && strVian != "0") strVian = "/" + strVian;
							strValues += myAFT[llCurr+1]["VIA3"] +  strVian + ",";
							strValues += myAFT[llCurr+1]["PSTA"] + ",";
							if(bmShowStev == true)
							{
								strValues += myAFT[llCurr+1]["STEV"] + ",";
							}
							if(myAFT[llCurr+1]["GTA1"].Trim().Length > 0 && myAFT[llCurr+1]["GTA2"].Trim().Length > 0)
							{
								strValues += myAFT[llCurr+1]["GTA1"] + "/" + myAFT[llCurr+1]["GTA2"] + ",";
							}
							else if(myAFT[llCurr+1]["GTA1"].Trim().Length > 0)
							{
								strValues += myAFT[llCurr+1]["GTA1"] + ",";
							}
							else if(myAFT[llCurr+1]["GTA2"].Trim().Length > 0)
							{
								strValues += "/" + myAFT[llCurr+1]["GTA2"] + ",";
							}	
							else
							{
								strValues += ",";
							}

							if(myAFT[llCurr+1]["LAND"].Trim().Length > 0 && myAFT[llCurr+1]["ONBL"].Trim().Length > 0)
							{
								strValues += Helper.DateString(myAFT[llCurr+1]["LAND"], strDateTimeFormat) + strDateTimeSeparator + Helper.DateString(myAFT[llCurr+1]["ONBL"], strDateTimeFormat) + ",";
							}
							else if(myAFT[llCurr+1]["LAND"].Trim().Length > 0)
							{
								strValues += Helper.DateString(myAFT[llCurr+1]["LAND"], strDateTimeFormat) + ",";
							}
							else if(myAFT[llCurr+1]["ONBL"].Trim().Length > 0)
							{
								strValues += strDateTimeSeparator + Helper.DateString(myAFT[llCurr+1]["ONBL"], strDateTimeFormat) + ",";
							}
							else
							{
								strValues += ",";
							}
							strValues += myAFT[llCurr+1]["BLT1"] + " " + myAFT[llCurr+1]["BLT2"] + ",";
							strValues += myAFT[llCurr+1]["ISRE"] + ",";
							strValues += myAFT[llCurr+1]["ACT3"] + ",";
							//-----------------------------------The departure part of rotation
							strFLNO = myAFT[llCurr]["FLNO"];
							if(strFLNO == "") strFLNO = myAFT[llCurr]["CSGN"];
							strValues += strFLNO + ",";
							strValues += myAFT[llCurr]["FLTI"] + ",";
							strValues += myAFT[llCurr]["JCNT"] + ",";
							strValues += Helper.DateString(myAFT[llCurr]["STOD"], strDateTimeFormat) + ",";
							strValues += myAFT[llCurr]["DOOD"] + ",";
							strValues += myAFT[llCurr]["TTYP"] + ",";							
							strValues += myAFT[llCurr]["STYP"] + ",";
							strVian = myAFT[llCurr+1]["VIAN"];
							if(strVian != "" && strVian != "0") strVian = "/" + strVian;
							strValues += myAFT[llCurr]["VIA3"] + strVian + ",";
							strValues += myAFT[llCurr]["DES3"] + ",";
							strValues += myAFT[llCurr]["PSTD"] + ",";
							if(bmShowStev == true)
							{
								strValues += myAFT[llCurr]["STEV"] + ",";
							}
							if(myAFT[llCurr]["GTD1"].Trim().Length > 0 && myAFT[llCurr]["GTD2"].Trim().Length > 0)
							{
								strValues += myAFT[llCurr]["GTD1"] + "/" + myAFT[llCurr]["GTD2"] + ",";
							}
							else if(myAFT[llCurr]["GTD1"].Trim().Length > 0)
							{
								strValues += myAFT[llCurr]["GTD1"] + ",";
							}
							else if(myAFT[llCurr]["GTD2"].Trim().Length > 0)
							{
								strValues += "/" + myAFT[llCurr]["GTD2"] + ",";
							}
							else
							{
								strValues += ",";
							}
							if(myAFT[llCurr]["OFBL"].Trim().Length > 0 && myAFT[llCurr]["AIRB"].Trim().Length > 0)
							{
								strValues += Helper.DateString(myAFT[llCurr]["OFBL"], strDateTimeFormat) + strDateTimeSeparator + Helper.DateString(myAFT[llCurr]["AIRB"], strDateTimeFormat) + ",";
							}
							else if(myAFT[llCurr]["OFBL"].Trim().Length > 0)
							{
								strValues += Helper.DateString(myAFT[llCurr]["OFBL"], strDateTimeFormat) + ",";
							}
							else if(myAFT[llCurr]["AIRB"].Trim().Length > 0)
							{
								strValues += strDateTimeSeparator + Helper.DateString(myAFT[llCurr]["AIRB"], strDateTimeFormat) + ",";
							}
							else
							{
								strValues += ",";
							}
							strValues += myAFT[llCurr]["CKIF"] + " " + myAFT[llCurr]["CKIT"] + ",";
							
							bool blDataAppended = false;
							if(bmChuteDisplay == true)
							{
								IRow [] rows = myCHA.RowsByIndexValue("RURN", myAFT[llCurr]["URNO"]);
								if(rows.Length == 0)
								{
									strValues +=",";								
								}
								else
								{
									string[] chuteData = GetChuteData(rows);									
									for(int i = 0 ; i < chuteData.Length; i++)
									{
										string strRowValue = strValues;
										strRowValue += chuteData[i] + ",";
										strRowValue += myAFT[llCurr]["ISRE"] + "\n";
										sb.Append(strRowValue);
									}
									blDataAppended = true;
									//this.AppendChuteFromTo(ref strValues,rows,sb);
								}
							}
							if(blDataAppended == false)
							{
								strValues += myAFT[llCurr]["ISRE"] + "\n";
								sb.Append(strValues);
							}
						}
					}

				}//if(strCurrRkey == strNextRkey)
				else
				{	
					if(this.checkBoxSingleArrivals.Checked == true)
					{
						DateTime timeSTOA = UT.CedaFullDateToDateTime(myAFT[llCurr]["STOA"]);
						DateTime timeARR = new DateTime(1980,1,1,0,0,0) ;
							
						strTime = myAFT[llCurr]["TIFA"];
						if(strTime != "")
							timeARR = UT.CedaFullDateToDateTime(strTime);

						DateTime timeFromDate = new DateTime(timeFrom.Value.Year, timeFrom.Value.Month, timeFrom.Value.Day, timeFrom.Value.Hour, timeFrom.Value.Minute, 0 );
					
						bool show = false;
						if(checkBoxSTASTD.Checked)
						{
							if(timeSTOA < timeFromDate)
								show= true;				
						}
							
						if(checkBoxTIFATIFD.Checked)
						{
							if(timeARR < timeFromDate)
								show= true;				
						}

						if(myAFT[llCurr]["ADID"] != "A" || myAFT[llCurr]["RTYP"] != "S" || myAFT[llCurr]["ONBL"].Trim().Length == 0)
						{
							show = false;
						}

						if(show)
						{
							NumberofFlights[0]++;
							//The arrival part of rotation
							//RKEY,URNO,FLNO,CSGN,FLTI,JCNT,STOA,DOOA,TTYP,STEV,STYP,ORG3,ORG4,VIA3,VIA4,VIAN,PSTA,GTA1,GTA2,BLT1,BLT2,ISRE,ACT3,STOD,DOOD,DES3,DES4,PSTD,GTD1,GTD2,CKIF,CKIT
							//Arrival,ID,Sched,A,Na,K,S,ORG,VIA,POS,Gate1,Gate2,Belt,I,Type,Departure,ID,J,STD,A,Na,K,S,VIA,DES,POS,Gate1,Gate2,I
							//ARR:,,,,,,,,,,,,,,,
							//DEP:,,,,,,,,,,,,,
							string strFLNO = myAFT[llCurr]["FLNO"];
							if(strFLNO == "") strFLNO = myAFT[llCurr]["CSGN"];
							strValues += strFLNO + ",";
							strValues += myAFT[llCurr]["FLTI"] + ",";
							strValues += myAFT[llCurr]["JCNT"] + ",";
							strValues += Helper.DateString(myAFT[llCurr]["STOA"], strDateTimeFormat) + ",";
							strValues += myAFT[llCurr]["DOOA"] + ",";
							strValues += myAFT[llCurr]["TTYP"] + ",";							
							strValues += myAFT[llCurr]["STYP"] + ",";
							strValues += myAFT[llCurr]["ORG3"] + ",";
							string strVian = myAFT[llCurr]["VIAN"];
							if(strVian != "" && strVian != "0") strVian = "/" + strVian;
							strValues += myAFT[llCurr]["VIA3"] + strVian + ",";
							strValues += myAFT[llCurr]["PSTA"] + ",";
							if(bmShowStev == true)
							{
								strValues += myAFT[llCurr]["STEV"] + ",";
							}
							if(myAFT[llCurr]["GTA1"].Trim().Length > 0 && myAFT[llCurr]["GTA2"].Trim().Length > 0)
							{
								strValues += myAFT[llCurr]["GTA1"] + "/" + myAFT[llCurr]["GTA2"] + ",";
							}
							else if(myAFT[llCurr]["GTA1"].Trim().Length > 0)
							{
								strValues += myAFT[llCurr]["GTA1"] + ",";
							}
							else if(myAFT[llCurr]["GTA2"].Trim().Length > 0)
							{
								strValues += "/" + myAFT[llCurr]["GTA2"] + ",";
							}
							else
							{
								strValues += ",";
							}
							if(myAFT[llCurr]["LAND"].Trim().Length > 0 && myAFT[llCurr]["ONBL"].Trim().Length > 0)
							{
								strValues += Helper.DateString(myAFT[llCurr]["LAND"], strDateTimeFormat) + strDateTimeSeparator + Helper.DateString(myAFT[llCurr]["ONBL"], strDateTimeFormat) + ",";
							}
							else if(myAFT[llCurr]["LAND"].Trim().Length > 0)
							{
								strValues += Helper.DateString(myAFT[llCurr]["LAND"],strDateTimeFormat) + ",";
							}
							else if(myAFT[llCurr]["ONBL"].Trim().Length > 0)
							{
								strValues += strDateTimeSeparator + Helper.DateString(myAFT[llCurr]["ONBL"],strDateTimeFormat) + ",";
							}
							else
							{
								strValues += "," ;
							}
							strValues += myAFT[llCurr]["BLT1"] + " " + myAFT[llCurr]["BLT2"] + ",";
							strValues += myAFT[llCurr]["ISRE"] + ",";
							strValues += myAFT[llCurr]["ACT3"] + ",";
							//-----------------------------------The departure part of rotation
							strValues += ",,,,,,,,,,,,,," + "\n";
							sb.Append(strValues);
						}
					}
					ilStep = 1;
				}
				llCurr = llCurr + ilStep;
			}//while(llCurr < myAFT.Count)
			if(strReportOrExcel.Equals("Report") == true)
			{
				tabResult.InsertBuffer(sb.ToString(), "\n");
				lblProgress.Text = "";
				progressBar1.Visible = false;
				tabResult.Sort("3,19", true, true);
				//tabResult.AutoSizeColumns();
				tabResult.Refresh();
				lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
			}
			else
			{
				tabExcelResult.InsertBuffer(sb.ToString(), "\n");
				tabExcelResult.Sort("3,19", true, true);
			}
		}

		private string[] GetChuteData(IRow[] rows)
		{
			// copy members to array list
			ArrayList myRows = new ArrayList(rows);
			// initialize members			
			for (int i = 0; i < myRows.Count; i++)
			{				
				((IRow)myRows[i])["CHUF"]=	((IRow)myRows[i])["LNAM"];
				((IRow)myRows[i])["CHUT"]=	((IRow)myRows[i])["LNAM"];
			}

			for (int i = 0; i < myRows.Count; i++)
			{				
				int ilCHUF = CalculateIntValueOfString(((IRow)myRows[i])["CHUF"]);
				int ilCHUT = CalculateIntValueOfString(((IRow)myRows[i])["CHUT"]);
				for (int j = 0; j < myRows.Count; j++)
				{					
					if (((IRow)myRows[i])["LTYP"] == ((IRow)myRows[j])["LTYP"] && 
						((IRow)myRows[i])["STOB"] == ((IRow)myRows[j])["STOB"] &&
						((IRow)myRows[i])["STOE"] == ((IRow)myRows[j])["STOE"])
					{
						if (CalculateIntValueOfString(((IRow)myRows[j])["CHUT"]) + 1 == ilCHUF ||
							CalculateIntValueOfString(((IRow)myRows[j])["CHUF"]) - 1 == ilCHUT )
						{
							if (CalculateIntValueOfString(((IRow)myRows[j])["CHUT"]) + 1 == ilCHUF )
							{								
								((IRow)myRows[i])["CHUF"] = ((IRow)myRows[j])["CHUF"];
							}
							else
							{								
								((IRow)myRows[i])["CHUT"] = ((IRow)myRows[j])["CHUT"];
							}
							
							myRows.RemoveAt(j);
							if ( j < i ) 
								i--;
							i--;
							break;
						}
					}
				}
			}
			
			string[] ChuteData = new string[myRows.Count];
			for (int i = 0; i < myRows.Count; i++)
			{				
				ChuteData[i] = ((IRow)myRows[i])["CHUF"] + "-" + ((IRow)myRows[i])["CHUT"];
			}
			return ChuteData;
		}


		/// <summary>
		/// Prepare the loaded data according to the report's needs.
		/// Prepare line by line the data a logical records and put
		/// the data into the Tab control.
		/// </summary>
		private void PrepareReportData()
		{
			//"FLNO,ADID,ORGDES,SCHEDULE,ACTUAL,ACTYPE"
			if(myAFT == null) 
				return;
			string strSchedule = "";
			DateTime tmpDate;
			lblProgress.Text = "Preparing Data";
			lblProgress.Refresh();
			progressBar1.Visible = true;
			progressBar1.Value = 0;
			int ilTotal = myAFT.Count;
			if(ilTotal == 0) ilTotal = 1;
			imArrivals = 0;
			imDepartures =0 ;
			imRotations = 0;
			imTotalFlights = 0;
			StringBuilder sb = new StringBuilder(10000);

			for(int i = 0; i < myAFT.Count; i++)
			{
				string strValues = "";
				string strAdid = myAFT[i]["ADID"];
				if( i % 100 == 0)
				{
					int percent = Convert.ToInt32((i * 100)/ilTotal);
					if (percent > 100) 
						percent = 100;
					progressBar1.Value = percent;
				}
				if(strAdid == "A")
				{
					imArrivals++;
					imTotalFlights++;
					strValues += myAFT[i]["FLNO"] + ",";
					strValues += strAdid + ",";
					strValues += myAFT[i]["ORG3"] + ",";
					strValues += Helper.DateString(myAFT[i]["STOA"], strDateDisplayFormat) + ",";
					strValues += Helper.DateString(myAFT[i]["TIFA"], strDateDisplayFormat) + ",";
					strValues += myAFT[i]["ACT3"] + ",";
					strValues += Helper.DateString(myAFT[i]["ONBL"], strDateDisplayFormat) + ",";
					strValues += myAFT[i]["TTYP"] + ",";
					strValues += myAFT[i]["REGN"] + "\n";
					sb.Append(strValues);
				}
				if(strAdid == "D")
				{
					imDepartures++;
					imTotalFlights++;
					strValues += myAFT[i]["FLNO"] + ",";
					strValues += strAdid + ",";
					strValues += myAFT[i]["DES3"] + ",";
					strValues += Helper.DateString(myAFT[i]["STOD"], strDateDisplayFormat) + ",";
					strValues += Helper.DateString(myAFT[i]["TIFD"], strDateDisplayFormat) + ",";
					strValues += myAFT[i]["ACT3"] + ",";
					strValues += Helper.DateString(myAFT[i]["OFBL"], strDateDisplayFormat) + ",";
					strValues += myAFT[i]["TTYP"] + ",";
					strValues += myAFT[i]["REGN"] + "\n";
					sb.Append(strValues);
				}
			}
			tabResult.InsertBuffer(sb.ToString(), "\n");
			lblProgress.Text = "";
			progressBar1.Visible = false;
			tabResult.Sort("3,19", true, true);
			tabResult.Refresh();
			lblResults.Text = "Report Results: (" + tabResult.GetLineCount().ToString() + ")";
		}
		/// <summary>
		/// Calls the generic report. The contructor must have the Tab 
		/// control and the reports headers as well as the types for
		/// each column to enable the reports to transform data, e.g.
		/// the data/time formatting. The lengths of the columns are
		/// dynamically read out of the tab control's header.
		/// </summary>
		private void RunReport()
		{
			string strSubHeader = "";
			imTotalFlights = NumberofFlights[0] +NumberofFlights[2] + NumberofFlights[1] +NumberofFlights[3];
			strSubHeader = "From: " + dtFrom.Value.ToString("dd.MM.yy'/'HH:mm") + " to " + 
				dtTo.Value.ToString("dd.MM.yy'/'HH:mm");
			strSubHeader += "       (Flights: " + imTotalFlights.ToString();
			strSubHeader += " "+" ARR: " + (NumberofFlights[0] +NumberofFlights[2]).ToString();
			strSubHeader += " "+" DEP: " + (NumberofFlights[1] +NumberofFlights[3]).ToString() + ")";
			strReportHeader = frmMain.strPrefixVal +  
				"Overnight Flights: NAT: " + strNatureCode +  " "+" APT: " + strAirportCode + " "+ " Min G/T: " + timeFrom.Value.ToString("HH:mm") + " to " + timeTo.Value.ToString("HH:mm");
			strReportSubHeader = strSubHeader;
			rptA3_Landscape rpt = new rptA3_Landscape(tabResult,strReportHeader, strSubHeader, "", 8);
			//prtFIPS_Landscape rpt = new prtFIPS_Landscape(tabResult,strReportHeader, strSubHeader, "", 8);
			rpt.SetExcelExportHeaderLenghtString(strExcelTabHeaderLensRotation);
			rpt.SetExcelExportTabResult(tabExcelResult);
			frmPrintPreview frm = new frmPrintPreview(rpt);
			if(Helper.UseSpreadBuilder() == true)
			{				
				activeReport = rpt;
				frm.UseSpreadBuilder(true);
				frm.GetBtnExcelExport().Click += new EventHandler(frmOvernightFlights_Click);
			}
			else
			{
				frm.UseSpreadBuilder(false);
			}
			frm.Show();
		}

		/// <summary>
		/// Start the print preview without data selection.
		/// This can be used to adjust the column widths and
		/// the sorting of the tab before printing.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnPrintPreview_Click(object sender, System.EventArgs e)
		{
			RunReport();
		}

		/// <summary>
		/// Loads data and dirctly calls the print preview.
		/// </summary>
		/// <param name="sender">Originator is the form</param>
		/// <param name="e">Event arguments.</param>
		private void btnLoadPrint_Click(object sender, System.EventArgs e)
		{
			string strError = ValidateUserEntry();
			if(strError != "")
			{
				MessageBox.Show(this, strError);
				return; // Do nothing due to errors
			}
			this.Cursor = Cursors.WaitCursor;
			LoadReportData();
			PrepareReportDataRotations("Report");
			PrepareReportDataRotations("Excel");
			RunReport();
			this.Cursor = Cursors.Arrow;
		}

		/// <summary>
		/// Sent by the tab control when the user double click.
		/// This is necessary for sorting.
		/// </summary>
		/// <param name="sender">The Tab control</param>
		/// <param name="e">Double click event parameters.</param>
		private void tabResult_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if( e.colNo == tabResult.CurrentSortColumn)
				{
					if( tabResult.SortOrderASC == true)
					{
						tabResult.Sort(e.colNo.ToString(), false, true);
						tabExcelResult.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						tabResult.Sort(e.colNo.ToString(), true, true);
						tabExcelResult.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabResult.Sort( e.colNo.ToString(), true, true);
					tabExcelResult.Sort( e.colNo.ToString(), true, true);
				}
				tabResult.Refresh();
			}
		}

		private void frmOvernightFlights_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			myDB.Unbind("NAT");
			myDB.Unbind("AFT");
			myDB.Unbind("CHA");
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			txtLens.Text = tabResult.HeaderLengthString;
		}

		private void dtFrom_ValueChanged(object sender, System.EventArgs e)
		{
		
		}

		private void timeFrom_ValueChanged(object sender, System.EventArgs e)
		{
		
		}

		private void label8_Click(object sender, System.EventArgs e)
		{
		
		}

		private void checkBoxSingleArrivals_CheckedChanged(object sender, System.EventArgs e)
		{
			if(tabResult.GetLineCount() > 0)
			{
				LoadReportData();
				PrepareReportDataRotations("Report");
				PrepareReportDataRotations("Excel");
			}
		}

		/// <summary>
		/// This function returns the int value of a string.
		/// </summary>
		/// <param name="strFlightValues"></param>
		/// <returns></returns>
		private int CalculateIntValueOfString(string strFlightValues)
		{
			string strNew = "";
			int total = 0;
			if(IsAlpha(strFlightValues))
			{
				char ab;
				IEnumerator strFltValEnum = strFlightValues.GetEnumerator();
				int CharCount = 0;
				while( strFltValEnum.MoveNext( ) )
				{
					CharCount++;
					ab = (char) strFltValEnum.Current;
					if(Char.IsNumber(ab))
						strNew += ab;
				}
				total = int.Parse(strNew);	
			}
			else
			{
				total = int.Parse(strFlightValues);
			}
			return total ;
		}
		/// <summary>
		/// Checks if a character is Alphabet or Numeric.
		/// </summary>
		/// <param name="strToCheck"></param>
		/// <returns></returns>
		public bool IsAlpha(string strToCheck)
		{
			Regex objAlphaPattern=new Regex("[A-Z]");

			return objAlphaPattern.IsMatch(strToCheck);
		}

		private void InitializeMouseEvents()
		{
			((System.Windows.Forms.Control)tabResult).MouseWheel += new MouseEventHandler(frmOvernightFlights_MouseWheel);
			((System.Windows.Forms.Control)tabResult).MouseEnter += new EventHandler(frmOvernightFlights_MouseEnter);
			((System.Windows.Forms.Control)tabResult).MouseLeave += new EventHandler(frmOvernightFlights_MouseLeave);
		}

		private void frmOvernightFlights_MouseWheel(object sender, MouseEventArgs e)
		{
			if(bmMouseInsideTabControl == false || Helper.CheckScrollingStatus(tabResult) == false)
			{
				return;
			}
			if(e.Delta > 0 && tabResult.GetVScrollPos() > 0)
			{				
				tabResult.OnVScrollTo(tabResult.GetVScrollPos() - 1);
			}
			else if(e.Delta < 0 && (tabResult.GetVScrollPos() + 1) < tabResult.GetLineCount())
			{
				tabResult.OnVScrollTo(tabResult.GetVScrollPos() + 1);
			}
		}

		private void frmOvernightFlights_MouseEnter(object sender, EventArgs e)
		{			
			bmMouseInsideTabControl = true;
		}

		private void frmOvernightFlights_MouseLeave(object sender, EventArgs e)
		{
			bmMouseInsideTabControl = false;
		}

		private void frmOvernightFlights_Click(object sender, EventArgs e)
		{
			Helper.ExportToExcel(tabExcelResult,strReportHeader,strReportSubHeader,strExcelTabHeaderLensRotation,8,0,activeReport);
		}

		private void buttonHelp_Click(object sender, System.EventArgs e)
		{
			Helper.ShowHelpFile(this);
		}

		private void frmOvernightFlights_HelpRequested(object sender, HelpEventArgs hlpevent)
		{
			Helper.ShowHelpFile(this);
		}
	}
}
