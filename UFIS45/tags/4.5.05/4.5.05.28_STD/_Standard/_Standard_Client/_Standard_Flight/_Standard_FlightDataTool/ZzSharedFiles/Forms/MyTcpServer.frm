VERSION 5.00
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form MyTcpServer 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ICU2 TCP/IP Server"
   ClientHeight    =   7305
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   11325
   Icon            =   "MyTcpServer.frx":0000
   LinkMode        =   1  'Source
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7305
   ScaleWidth      =   11325
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox FuncPanel 
      Height          =   6435
      Index           =   0
      Left            =   660
      ScaleHeight     =   6375
      ScaleWidth      =   9375
      TabIndex        =   0
      Top             =   420
      Visible         =   0   'False
      Width           =   9435
      Begin VB.TextBox txtServer 
         Height          =   285
         Left            =   690
         TabIndex        =   26
         Top             =   60
         Width           =   975
      End
      Begin VB.TextBox txtTot 
         Height          =   285
         Left            =   690
         TabIndex        =   25
         Top             =   720
         Width           =   975
      End
      Begin VB.TextBox txtCmd 
         Height          =   285
         Left            =   2400
         TabIndex        =   24
         Text            =   "GBCH"
         Top             =   720
         Width           =   975
      End
      Begin VB.TextBox txtTbl 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   4170
         TabIndex        =   23
         Text            =   "AFTTAB"
         Top             =   720
         Width           =   975
      End
      Begin VB.TextBox txtBcNum 
         Height          =   285
         Left            =   690
         TabIndex        =   22
         Top             =   1050
         Width           =   975
      End
      Begin VB.TextBox txtFld 
         Height          =   285
         Left            =   2400
         TabIndex        =   21
         Top             =   1050
         Width           =   8955
      End
      Begin VB.TextBox txtWhe 
         Height          =   285
         Left            =   2400
         TabIndex        =   20
         Text            =   "[CMD=GET_HIST][CNT=1000]"
         Top             =   1380
         Width           =   8955
      End
      Begin VB.TextBox txtUsr 
         Height          =   285
         Left            =   690
         TabIndex        =   19
         Top             =   1380
         Width           =   975
      End
      Begin VB.TextBox txtWks 
         Height          =   285
         Left            =   690
         TabIndex        =   18
         Top             =   1710
         Width           =   975
      End
      Begin VB.TextBox txtTwe 
         Height          =   285
         Left            =   8100
         TabIndex        =   17
         Top             =   720
         Width           =   1455
      End
      Begin VB.TextBox txtTws 
         Height          =   285
         Left            =   5880
         TabIndex        =   16
         Top             =   720
         Width           =   1485
      End
      Begin VB.TextBox txtPort 
         Height          =   285
         Left            =   2400
         TabIndex        =   15
         Text            =   "3357"
         Top             =   60
         Width           =   975
      End
      Begin VB.TextBox txtQueue 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   4185
         TabIndex        =   14
         Top             =   60
         Width           =   975
      End
      Begin VB.TextBox txtError 
         Height          =   285
         Left            =   2400
         TabIndex        =   13
         Top             =   1710
         Width           =   8955
      End
      Begin VB.CheckBox chkConnect 
         Caption         =   "Connect"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5280
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   60
         Width           =   1185
      End
      Begin VB.CheckBox chkDisconnect 
         Caption         =   "Disconnect"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7680
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   60
         Width           =   1185
      End
      Begin VB.TextBox txtPack 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   10380
         TabIndex        =   10
         Top             =   720
         Width           =   975
      End
      Begin VB.TextBox HOPO 
         Height          =   285
         Left            =   60
         TabIndex        =   9
         Text            =   "SIN"
         Top             =   3150
         Visible         =   0   'False
         Width           =   555
      End
      Begin VB.TextBox TblExt 
         Height          =   285
         Left            =   60
         TabIndex        =   8
         Text            =   "TAB"
         Top             =   3480
         Visible         =   0   'False
         Width           =   555
      End
      Begin VB.TextBox HostName 
         Height          =   285
         Left            =   60
         TabIndex        =   7
         Text            =   "SIN1"
         Top             =   3810
         Visible         =   0   'False
         Width           =   555
      End
      Begin VB.TextBox txtMsg 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Left            =   690
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   6
         Top             =   2910
         Width           =   10665
      End
      Begin VB.CheckBox chkSendMsg 
         Caption         =   "Send MSG"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6480
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   60
         Width           =   1185
      End
      Begin VB.TextBox Text1 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   825
         Left            =   690
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   4
         Top             =   2070
         Width           =   10665
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Save"
         Height          =   285
         Left            =   2400
         TabIndex        =   3
         Top             =   420
         Width           =   690
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Load"
         Height          =   285
         Left            =   3120
         TabIndex        =   2
         Top             =   420
         Width           =   690
      End
      Begin VB.CommandButton Command3 
         Caption         =   "Run"
         Height          =   285
         Left            =   3840
         TabIndex        =   1
         Top             =   420
         Width           =   690
      End
      Begin TABLib.TAB BcSpoolerTab 
         Height          =   1935
         Left            =   690
         TabIndex        =   27
         Top             =   3780
         Width           =   12375
         _Version        =   65536
         _ExtentX        =   21828
         _ExtentY        =   3413
         _StockProps     =   64
      End
      Begin VB.Label Label1 
         Caption         =   "Server:"
         Height          =   255
         Left            =   60
         TabIndex        =   42
         Top             =   90
         Width           =   585
      End
      Begin VB.Label Label2 
         Caption         =   "COUNT"
         Height          =   255
         Left            =   60
         TabIndex        =   41
         Top             =   750
         Width           =   585
      End
      Begin VB.Label Label3 
         Caption         =   "CMD:"
         Height          =   255
         Left            =   1770
         TabIndex        =   40
         Top             =   750
         Width           =   585
      End
      Begin VB.Label Label5 
         Caption         =   "TBL:"
         Height          =   255
         Left            =   3540
         TabIndex        =   39
         Top             =   750
         Width           =   585
      End
      Begin VB.Label Label7 
         Caption         =   "BYTES"
         Height          =   255
         Left            =   60
         TabIndex        =   38
         Top             =   1080
         Width           =   585
      End
      Begin VB.Label Label8 
         Caption         =   "FLD:"
         Height          =   255
         Left            =   1770
         TabIndex        =   37
         Top             =   1080
         Width           =   585
      End
      Begin VB.Label Label9 
         Caption         =   "WHE:"
         Height          =   255
         Left            =   1770
         TabIndex        =   36
         Top             =   1410
         Width           =   585
      End
      Begin VB.Label Label10 
         Caption         =   "USR:"
         Height          =   255
         Left            =   60
         TabIndex        =   35
         Top             =   1410
         Width           =   585
      End
      Begin VB.Label Label11 
         Caption         =   "WKS:"
         Height          =   255
         Left            =   60
         TabIndex        =   34
         Top             =   1740
         Width           =   585
      End
      Begin VB.Label Label12 
         Caption         =   "TWE:"
         Height          =   255
         Left            =   7530
         TabIndex        =   33
         Top             =   750
         Width           =   585
      End
      Begin VB.Label Label14 
         Caption         =   "TWS:"
         Height          =   255
         Left            =   5310
         TabIndex        =   32
         Top             =   750
         Width           =   585
      End
      Begin VB.Label Label15 
         Caption         =   "Port:"
         Height          =   255
         Left            =   1770
         TabIndex        =   31
         Top             =   90
         Width           =   585
      End
      Begin VB.Label Label16 
         Caption         =   "Socket:"
         Height          =   255
         Left            =   3540
         TabIndex        =   30
         Top             =   90
         Width           =   555
      End
      Begin VB.Label Label4 
         Caption         =   "ERR:"
         Height          =   255
         Left            =   1770
         TabIndex        =   29
         Top             =   1740
         Width           =   585
      End
      Begin VB.Label Label6 
         Caption         =   "PACK:"
         Height          =   255
         Left            =   9750
         TabIndex        =   28
         Top             =   750
         Width           =   585
      End
   End
   Begin MSWinsockLib.Winsock WsFds 
      Index           =   0
      Left            =   60
      Top             =   60
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin VB.Timer TcpTimer 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   60
      Top             =   1020
   End
   Begin MSWinsockLib.Winsock WS1 
      Index           =   0
      Left            =   60
      Top             =   540
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin VB.PictureBox FuncPanel 
      BackColor       =   &H00E0E0E0&
      Height          =   7305
      Index           =   1
      Left            =   0
      ScaleHeight     =   7245
      ScaleWidth      =   11265
      TabIndex        =   43
      Top             =   0
      Width           =   11325
      Begin VB.Frame FdsAppl 
         BackColor       =   &H00E0E0E0&
         Caption         =   "MODULE FDS-LOAD"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   7050
         Index           =   2
         Left            =   7620
         TabIndex        =   116
         Top             =   90
         Width           =   3525
         Begin VB.Frame FdsModul 
            BackColor       =   &H00E0E0E0&
            Caption         =   "DUPLEX LINE TO MODULE  FDS-FILE"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3135
            Index           =   5
            Left            =   90
            TabIndex        =   136
            Top             =   600
            Width           =   3345
            Begin VB.Frame FdsTcpCfg 
               BackColor       =   &H00E0E0E0&
               Caption         =   "WS(01): MAIN-LOAD-RECV"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   1185
               Index           =   11
               Left            =   90
               TabIndex        =   146
               Top             =   600
               Width           =   3165
               Begin VB.TextBox txtRcv 
                  Height          =   285
                  Index           =   11
                  Left            =   690
                  TabIndex        =   175
                  Top             =   -30
                  Width           =   2475
               End
               Begin VB.CheckBox chkTest 
                  Caption         =   "Test 11"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   11
                  Left            =   0
                  Style           =   1  'Graphical
                  TabIndex        =   163
                  Top             =   0
                  Width           =   675
               End
               Begin VB.CheckBox chkTask 
                  Caption         =   "Listen"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   11
                  Left            =   90
                  Style           =   1  'Graphical
                  TabIndex        =   149
                  Tag             =   "LISTEN"
                  Top             =   270
                  Width           =   885
               End
               Begin VB.TextBox MainPort 
                  Alignment       =   2  'Center
                  Height          =   285
                  Index           =   11
                  Left            =   2580
                  TabIndex        =   148
                  Text            =   "4394"
                  Top             =   270
                  Width           =   495
               End
               Begin VB.TextBox MainTcpIp 
                  Alignment       =   2  'Center
                  Height          =   285
                  Index           =   11
                  Left            =   990
                  TabIndex        =   147
                  Text            =   "255.255.255.255"
                  Top             =   270
                  Width           =   1335
               End
               Begin VB.Image PicStat 
                  Appearance      =   0  'Flat
                  Height          =   195
                  Index           =   11
                  Left            =   2355
                  Picture         =   "MyTcpServer.frx":08CA
                  Top             =   315
                  Width           =   195
               End
               Begin VB.Label MainStatus 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackStyle       =   0  'Transparent
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Socket ready for use"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   11
                  Left            =   90
                  TabIndex        =   151
                  Top             =   585
                  Width           =   2985
               End
               Begin VB.Label SockStatus 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackStyle       =   0  'Transparent
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Socket ready for use"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   11
                  Left            =   90
                  TabIndex        =   150
                  Top             =   855
                  Width           =   2985
               End
            End
            Begin VB.CheckBox chkLine 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   17
               Left            =   90
               Style           =   1  'Graphical
               TabIndex        =   145
               Top             =   270
               Width           =   885
            End
            Begin VB.CheckBox chkLine 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   16
               Left            =   990
               Style           =   1  'Graphical
               TabIndex        =   144
               Top             =   270
               Width           =   885
            End
            Begin VB.CheckBox chkLine 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   15
               Left            =   1890
               Style           =   1  'Graphical
               TabIndex        =   143
               Top             =   270
               Width           =   885
            End
            Begin VB.Frame FdsTcpCfg 
               BackColor       =   &H00E0E0E0&
               Caption         =   "WS(01): LOAD-MAIN-SEND"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   1185
               Index           =   10
               Left            =   90
               TabIndex        =   137
               Top             =   1860
               Width           =   3165
               Begin VB.TextBox txtRcv 
                  Height          =   285
                  Index           =   10
                  Left            =   690
                  TabIndex        =   174
                  Top             =   -30
                  Width           =   2475
               End
               Begin VB.CheckBox chkTest 
                  Caption         =   "Test 10"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   10
                  Left            =   0
                  Style           =   1  'Graphical
                  TabIndex        =   162
                  Top             =   0
                  Width           =   675
               End
               Begin VB.TextBox MainTcpIp 
                  Alignment       =   2  'Center
                  Height          =   285
                  Index           =   10
                  Left            =   990
                  TabIndex        =   140
                  Text            =   "255.255.255.255"
                  Top             =   270
                  Width           =   1335
               End
               Begin VB.TextBox MainPort 
                  Alignment       =   2  'Center
                  Height          =   285
                  Index           =   10
                  Left            =   2580
                  TabIndex        =   139
                  Text            =   "4391"
                  Top             =   270
                  Width           =   495
               End
               Begin VB.CheckBox chkTask 
                  Caption         =   "Connect"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   10
                  Left            =   90
                  Style           =   1  'Graphical
                  TabIndex        =   138
                  Tag             =   "CONNECT"
                  Top             =   270
                  Width           =   885
               End
               Begin VB.Label SockStatus 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackStyle       =   0  'Transparent
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Socket ready for use"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   10
                  Left            =   90
                  TabIndex        =   142
                  Top             =   855
                  Width           =   2985
               End
               Begin VB.Label MainStatus 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackStyle       =   0  'Transparent
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Socket ready for use"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   10
                  Left            =   90
                  TabIndex        =   141
                  Top             =   585
                  Width           =   2985
               End
               Begin VB.Image PicStat 
                  Appearance      =   0  'Flat
                  Height          =   195
                  Index           =   10
                  Left            =   2355
                  Picture         =   "MyTcpServer.frx":0B14
                  Top             =   315
                  Width           =   195
               End
            End
         End
         Begin VB.CheckBox chkMain 
            Caption         =   "FILE"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   8
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   135
            Tag             =   "FILE"
            Top             =   270
            Width           =   885
         End
         Begin VB.CheckBox chkMain 
            Caption         =   "MAIN"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   7
            Left            =   990
            Style           =   1  'Graphical
            TabIndex        =   134
            Tag             =   "MAIN"
            Top             =   270
            Width           =   885
         End
         Begin VB.CheckBox chkMain 
            Caption         =   "LOAD"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   6
            Left            =   1890
            Style           =   1  'Graphical
            TabIndex        =   133
            Tag             =   "LOAD"
            Top             =   270
            Width           =   885
         End
         Begin VB.Frame FdsModul 
            BackColor       =   &H00E0E0E0&
            Caption         =   "DUPLEX LINE TO MODULE  FDS-MAIN"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3135
            Index           =   4
            Left            =   90
            TabIndex        =   117
            Top             =   3810
            Width           =   3345
            Begin VB.Frame FdsTcpCfg 
               BackColor       =   &H00E0E0E0&
               Caption         =   "WS(01): LOAD-COMP-SEND"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   1185
               Index           =   9
               Left            =   90
               TabIndex        =   127
               Top             =   1860
               Width           =   3165
               Begin VB.TextBox txtRcv 
                  Height          =   285
                  Index           =   9
                  Left            =   690
                  TabIndex        =   173
                  Top             =   -30
                  Width           =   2475
               End
               Begin VB.CheckBox chkTest 
                  Caption         =   "Test 9"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   9
                  Left            =   0
                  Style           =   1  'Graphical
                  TabIndex        =   161
                  Top             =   0
                  Width           =   675
               End
               Begin VB.CheckBox chkTask 
                  Caption         =   "Connect"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   9
                  Left            =   90
                  Style           =   1  'Graphical
                  TabIndex        =   130
                  Tag             =   "CONNECT"
                  Top             =   270
                  Width           =   885
               End
               Begin VB.TextBox MainPort 
                  Alignment       =   2  'Center
                  Height          =   285
                  Index           =   9
                  Left            =   2580
                  TabIndex        =   129
                  Text            =   "4393"
                  Top             =   270
                  Width           =   495
               End
               Begin VB.TextBox MainTcpIp 
                  Alignment       =   2  'Center
                  Height          =   285
                  Index           =   9
                  Left            =   990
                  TabIndex        =   128
                  Text            =   "255.255.255.255"
                  Top             =   270
                  Width           =   1335
               End
               Begin VB.Image PicStat 
                  Appearance      =   0  'Flat
                  Height          =   195
                  Index           =   9
                  Left            =   2355
                  Picture         =   "MyTcpServer.frx":0D5E
                  Top             =   315
                  Width           =   195
               End
               Begin VB.Label MainStatus 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackStyle       =   0  'Transparent
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Socket ready for use"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   9
                  Left            =   90
                  TabIndex        =   132
                  Top             =   585
                  Width           =   2985
               End
               Begin VB.Label SockStatus 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackStyle       =   0  'Transparent
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Socket ready for use"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   9
                  Left            =   90
                  TabIndex        =   131
                  Top             =   855
                  Width           =   2985
               End
            End
            Begin VB.CheckBox chkLine 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   14
               Left            =   1890
               Style           =   1  'Graphical
               TabIndex        =   126
               Top             =   270
               Width           =   885
            End
            Begin VB.CheckBox chkLine 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   13
               Left            =   990
               Style           =   1  'Graphical
               TabIndex        =   125
               Top             =   270
               Width           =   885
            End
            Begin VB.CheckBox chkLine 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   12
               Left            =   90
               Style           =   1  'Graphical
               TabIndex        =   124
               Top             =   270
               Width           =   885
            End
            Begin VB.Frame FdsTcpCfg 
               BackColor       =   &H00E0E0E0&
               Caption         =   "WS(01): COMP-LOAD-RECV"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   1185
               Index           =   8
               Left            =   90
               TabIndex        =   118
               Top             =   600
               Width           =   3165
               Begin VB.TextBox txtRcv 
                  Height          =   285
                  Index           =   8
                  Left            =   690
                  TabIndex        =   172
                  Top             =   -30
                  Width           =   2475
               End
               Begin VB.CheckBox chkTest 
                  Caption         =   "Test 8"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   8
                  Left            =   0
                  Style           =   1  'Graphical
                  TabIndex        =   160
                  Top             =   0
                  Width           =   675
               End
               Begin VB.TextBox MainTcpIp 
                  Alignment       =   2  'Center
                  Height          =   285
                  Index           =   8
                  Left            =   990
                  TabIndex        =   121
                  Text            =   "255.255.255.255"
                  Top             =   270
                  Width           =   1335
               End
               Begin VB.TextBox MainPort 
                  Alignment       =   2  'Center
                  Height          =   285
                  Index           =   8
                  Left            =   2580
                  TabIndex        =   120
                  Text            =   "4395"
                  Top             =   270
                  Width           =   495
               End
               Begin VB.CheckBox chkTask 
                  Caption         =   "Listen"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   8
                  Left            =   90
                  Style           =   1  'Graphical
                  TabIndex        =   119
                  Tag             =   "LISTEN"
                  Top             =   270
                  Width           =   885
               End
               Begin VB.Label SockStatus 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackStyle       =   0  'Transparent
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Socket ready for use"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   8
                  Left            =   90
                  TabIndex        =   123
                  Top             =   855
                  Width           =   2985
               End
               Begin VB.Label MainStatus 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackStyle       =   0  'Transparent
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Socket ready for use"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   8
                  Left            =   90
                  TabIndex        =   122
                  Top             =   585
                  Width           =   2985
               End
               Begin VB.Image PicStat 
                  Appearance      =   0  'Flat
                  Height          =   195
                  Index           =   8
                  Left            =   2355
                  Picture         =   "MyTcpServer.frx":0FA8
                  Top             =   315
                  Width           =   195
               End
            End
         End
      End
      Begin VB.Frame FdsAppl 
         BackColor       =   &H00E0E0E0&
         Caption         =   "MODULE FDS-MAIN"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   7050
         Index           =   1
         Left            =   3870
         TabIndex        =   80
         Top             =   90
         Width           =   3525
         Begin VB.Frame FdsModul 
            BackColor       =   &H00E0E0E0&
            Caption         =   "DUPLEX LINE TO MODULE  FDS-FILE"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3135
            Index           =   3
            Left            =   90
            TabIndex        =   100
            Top             =   600
            Width           =   3345
            Begin VB.Frame FdsTcpCfg 
               BackColor       =   &H00E0E0E0&
               Caption         =   "WS(01): MAIN-COMP-RECV"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   1185
               Index           =   7
               Left            =   90
               TabIndex        =   110
               Top             =   600
               Width           =   3165
               Begin VB.TextBox txtRcv 
                  Height          =   285
                  Index           =   7
                  Left            =   690
                  TabIndex        =   171
                  Top             =   -30
                  Width           =   2475
               End
               Begin VB.CheckBox chkTest 
                  Caption         =   "Test 7"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   7
                  Left            =   0
                  Style           =   1  'Graphical
                  TabIndex        =   159
                  Top             =   0
                  Width           =   675
               End
               Begin VB.CheckBox chkTask 
                  Caption         =   "Listen"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   7
                  Left            =   90
                  Style           =   1  'Graphical
                  TabIndex        =   113
                  Tag             =   "LISTEN"
                  Top             =   270
                  Width           =   885
               End
               Begin VB.TextBox MainPort 
                  Alignment       =   2  'Center
                  Height          =   285
                  Index           =   7
                  Left            =   2580
                  TabIndex        =   112
                  Text            =   "4392"
                  Top             =   270
                  Width           =   495
               End
               Begin VB.TextBox MainTcpIp 
                  Alignment       =   2  'Center
                  Height          =   285
                  Index           =   7
                  Left            =   990
                  TabIndex        =   111
                  Text            =   "255.255.255.255"
                  Top             =   270
                  Width           =   1335
               End
               Begin VB.Image PicStat 
                  Appearance      =   0  'Flat
                  Height          =   195
                  Index           =   7
                  Left            =   2355
                  Picture         =   "MyTcpServer.frx":11F2
                  Top             =   315
                  Width           =   195
               End
               Begin VB.Label MainStatus 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackStyle       =   0  'Transparent
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Socket ready for use"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   7
                  Left            =   90
                  TabIndex        =   115
                  Top             =   585
                  Width           =   2985
               End
               Begin VB.Label SockStatus 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackStyle       =   0  'Transparent
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Socket ready for use"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   7
                  Left            =   90
                  TabIndex        =   114
                  Top             =   855
                  Width           =   2985
               End
            End
            Begin VB.CheckBox chkLine 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   11
               Left            =   90
               Style           =   1  'Graphical
               TabIndex        =   109
               Top             =   270
               Width           =   885
            End
            Begin VB.CheckBox chkLine 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   10
               Left            =   990
               Style           =   1  'Graphical
               TabIndex        =   108
               Top             =   270
               Width           =   885
            End
            Begin VB.CheckBox chkLine 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   9
               Left            =   1890
               Style           =   1  'Graphical
               TabIndex        =   107
               Top             =   270
               Width           =   885
            End
            Begin VB.Frame FdsTcpCfg 
               BackColor       =   &H00E0E0E0&
               Caption         =   "WS(01): COMP-MAIN-SEND"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   1185
               Index           =   6
               Left            =   90
               TabIndex        =   101
               Top             =   1860
               Width           =   3165
               Begin VB.TextBox txtRcv 
                  Height          =   285
                  Index           =   6
                  Left            =   690
                  TabIndex        =   170
                  Top             =   -30
                  Width           =   2475
               End
               Begin VB.CheckBox chkTest 
                  Caption         =   "Test 6"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   6
                  Left            =   0
                  Style           =   1  'Graphical
                  TabIndex        =   158
                  Top             =   0
                  Width           =   675
               End
               Begin VB.TextBox MainTcpIp 
                  Alignment       =   2  'Center
                  Height          =   285
                  Index           =   6
                  Left            =   990
                  TabIndex        =   104
                  Text            =   "255.255.255.255"
                  Top             =   270
                  Width           =   1335
               End
               Begin VB.TextBox MainPort 
                  Alignment       =   2  'Center
                  Height          =   285
                  Index           =   6
                  Left            =   2580
                  TabIndex        =   103
                  Text            =   "4390"
                  Top             =   270
                  Width           =   495
               End
               Begin VB.CheckBox chkTask 
                  Caption         =   "Connect"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   6
                  Left            =   90
                  Style           =   1  'Graphical
                  TabIndex        =   102
                  Tag             =   "CONNECT"
                  Top             =   270
                  Width           =   885
               End
               Begin VB.Label SockStatus 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackStyle       =   0  'Transparent
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Socket ready for use"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   6
                  Left            =   90
                  TabIndex        =   106
                  Top             =   855
                  Width           =   2985
               End
               Begin VB.Label MainStatus 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackStyle       =   0  'Transparent
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Socket ready for use"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   6
                  Left            =   90
                  TabIndex        =   105
                  Top             =   585
                  Width           =   2985
               End
               Begin VB.Image PicStat 
                  Appearance      =   0  'Flat
                  Height          =   195
                  Index           =   6
                  Left            =   2355
                  Picture         =   "MyTcpServer.frx":143C
                  Top             =   315
                  Width           =   195
               End
            End
         End
         Begin VB.CheckBox chkMain 
            Caption         =   "FILE"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   5
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   99
            Tag             =   "FILE"
            Top             =   270
            Width           =   885
         End
         Begin VB.CheckBox chkMain 
            Caption         =   "MAIN"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   4
            Left            =   990
            Style           =   1  'Graphical
            TabIndex        =   98
            Tag             =   "MAIN"
            Top             =   270
            Width           =   885
         End
         Begin VB.CheckBox chkMain 
            Caption         =   "LOAD"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   3
            Left            =   1890
            Style           =   1  'Graphical
            TabIndex        =   97
            Tag             =   "LOAD"
            Top             =   270
            Width           =   885
         End
         Begin VB.Frame FdsModul 
            BackColor       =   &H00E0E0E0&
            Caption         =   "DUPLEX LINE TO MODULE  FDS-LOAD"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3135
            Index           =   2
            Left            =   90
            TabIndex        =   81
            Top             =   3810
            Width           =   3345
            Begin VB.Frame FdsTcpCfg 
               BackColor       =   &H00E0E0E0&
               Caption         =   "WS(01): COMP-LOAD-SEND"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   1185
               Index           =   5
               Left            =   90
               TabIndex        =   91
               Top             =   1860
               Width           =   3165
               Begin VB.TextBox txtRcv 
                  Height          =   285
                  Index           =   5
                  Left            =   690
                  TabIndex        =   169
                  Top             =   -30
                  Width           =   2475
               End
               Begin VB.CheckBox chkTest 
                  Caption         =   "Test 5"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   5
                  Left            =   0
                  Style           =   1  'Graphical
                  TabIndex        =   157
                  Top             =   0
                  Width           =   675
               End
               Begin VB.CheckBox chkTask 
                  Caption         =   "Connect"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   5
                  Left            =   90
                  Style           =   1  'Graphical
                  TabIndex        =   94
                  Tag             =   "CONNECT"
                  Top             =   270
                  Width           =   885
               End
               Begin VB.TextBox MainPort 
                  Alignment       =   2  'Center
                  Height          =   285
                  Index           =   5
                  Left            =   2580
                  TabIndex        =   93
                  Text            =   "4395"
                  Top             =   270
                  Width           =   495
               End
               Begin VB.TextBox MainTcpIp 
                  Alignment       =   2  'Center
                  Height          =   285
                  Index           =   5
                  Left            =   990
                  TabIndex        =   92
                  Text            =   "255.255.255.255"
                  Top             =   270
                  Width           =   1335
               End
               Begin VB.Image PicStat 
                  Appearance      =   0  'Flat
                  Height          =   195
                  Index           =   5
                  Left            =   2355
                  Picture         =   "MyTcpServer.frx":1686
                  Top             =   315
                  Width           =   195
               End
               Begin VB.Label MainStatus 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackStyle       =   0  'Transparent
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Socket ready for use"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   5
                  Left            =   90
                  TabIndex        =   96
                  Top             =   585
                  Width           =   2985
               End
               Begin VB.Label SockStatus 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackStyle       =   0  'Transparent
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Socket ready for use"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   5
                  Left            =   90
                  TabIndex        =   95
                  Top             =   855
                  Width           =   2985
               End
            End
            Begin VB.CheckBox chkLine 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   8
               Left            =   1890
               Style           =   1  'Graphical
               TabIndex        =   90
               Top             =   270
               Width           =   885
            End
            Begin VB.CheckBox chkLine 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   7
               Left            =   990
               Style           =   1  'Graphical
               TabIndex        =   89
               Top             =   270
               Width           =   885
            End
            Begin VB.CheckBox chkLine 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   6
               Left            =   90
               Style           =   1  'Graphical
               TabIndex        =   88
               Top             =   270
               Width           =   885
            End
            Begin VB.Frame FdsTcpCfg 
               BackColor       =   &H00E0E0E0&
               Caption         =   "WS(01): LOAD-COMP-RECV"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   1185
               Index           =   4
               Left            =   90
               TabIndex        =   82
               Top             =   600
               Width           =   3165
               Begin VB.TextBox txtRcv 
                  Height          =   285
                  Index           =   4
                  Left            =   690
                  TabIndex        =   168
                  Top             =   -30
                  Width           =   2475
               End
               Begin VB.CheckBox chkTest 
                  Caption         =   "Test 4"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   4
                  Left            =   0
                  Style           =   1  'Graphical
                  TabIndex        =   156
                  Top             =   0
                  Width           =   675
               End
               Begin VB.TextBox MainTcpIp 
                  Alignment       =   2  'Center
                  Height          =   285
                  Index           =   4
                  Left            =   990
                  TabIndex        =   85
                  Text            =   "255.255.255.255"
                  Top             =   270
                  Width           =   1335
               End
               Begin VB.TextBox MainPort 
                  Alignment       =   2  'Center
                  Height          =   285
                  Index           =   4
                  Left            =   2580
                  TabIndex        =   84
                  Text            =   "4393"
                  Top             =   270
                  Width           =   495
               End
               Begin VB.CheckBox chkTask 
                  Caption         =   "Listen"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   4
                  Left            =   90
                  Style           =   1  'Graphical
                  TabIndex        =   83
                  Tag             =   "LISTEN"
                  Top             =   270
                  Width           =   885
               End
               Begin VB.Label SockStatus 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackStyle       =   0  'Transparent
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Socket ready for use"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   4
                  Left            =   90
                  TabIndex        =   87
                  Top             =   855
                  Width           =   2985
               End
               Begin VB.Label MainStatus 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackStyle       =   0  'Transparent
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Socket ready for use"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   4
                  Left            =   90
                  TabIndex        =   86
                  Top             =   585
                  Width           =   2985
               End
               Begin VB.Image PicStat 
                  Appearance      =   0  'Flat
                  Height          =   195
                  Index           =   4
                  Left            =   2355
                  Picture         =   "MyTcpServer.frx":18D0
                  Top             =   315
                  Width           =   195
               End
            End
         End
      End
      Begin VB.Frame FdsAppl 
         BackColor       =   &H00E0E0E0&
         Caption         =   "MODULE FDS-FILE"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   7050
         Index           =   0
         Left            =   120
         TabIndex        =   44
         Top             =   90
         Width           =   3525
         Begin VB.Frame FdsModul 
            BackColor       =   &H00E0E0E0&
            Caption         =   "DUPLEX LINE TO MODULE  FDS-LOAD"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3135
            Index           =   1
            Left            =   90
            TabIndex        =   64
            Top             =   3810
            Width           =   3345
            Begin VB.Frame FdsTcpCfg 
               BackColor       =   &H00E0E0E0&
               Caption         =   "WS(01): LOAD-MAIN-RECV"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   1185
               Index           =   3
               Left            =   90
               TabIndex        =   74
               Top             =   600
               Width           =   3165
               Begin VB.TextBox txtRcv 
                  Height          =   285
                  Index           =   3
                  Left            =   690
                  TabIndex        =   167
                  Top             =   -30
                  Width           =   2475
               End
               Begin VB.CheckBox chkTest 
                  Caption         =   "Test 3"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   3
                  Left            =   0
                  Style           =   1  'Graphical
                  TabIndex        =   155
                  Top             =   0
                  Width           =   675
               End
               Begin VB.CheckBox chkTask 
                  Caption         =   "Listen"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   3
                  Left            =   90
                  Style           =   1  'Graphical
                  TabIndex        =   77
                  Tag             =   "LISTEN"
                  Top             =   270
                  Width           =   885
               End
               Begin VB.TextBox MainPort 
                  Alignment       =   2  'Center
                  Height          =   285
                  Index           =   3
                  Left            =   2580
                  TabIndex        =   76
                  Text            =   "4391"
                  Top             =   270
                  Width           =   495
               End
               Begin VB.TextBox MainTcpIp 
                  Alignment       =   2  'Center
                  Height          =   285
                  Index           =   3
                  Left            =   990
                  TabIndex        =   75
                  Text            =   "255.255.255.255"
                  Top             =   270
                  Width           =   1335
               End
               Begin VB.Image PicStat 
                  Appearance      =   0  'Flat
                  Height          =   195
                  Index           =   3
                  Left            =   2355
                  Picture         =   "MyTcpServer.frx":1B1A
                  Top             =   315
                  Width           =   195
               End
               Begin VB.Label MainStatus 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackStyle       =   0  'Transparent
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Socket ready for use"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   3
                  Left            =   90
                  TabIndex        =   79
                  Top             =   585
                  Width           =   2985
               End
               Begin VB.Label SockStatus 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackStyle       =   0  'Transparent
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Socket ready for use"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   3
                  Left            =   90
                  TabIndex        =   78
                  Top             =   855
                  Width           =   2985
               End
            End
            Begin VB.CheckBox chkLine 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   5
               Left            =   90
               Style           =   1  'Graphical
               TabIndex        =   73
               Top             =   270
               Width           =   885
            End
            Begin VB.CheckBox chkLine 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   4
               Left            =   990
               Style           =   1  'Graphical
               TabIndex        =   72
               Top             =   270
               Width           =   885
            End
            Begin VB.CheckBox chkLine 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   3
               Left            =   1890
               Style           =   1  'Graphical
               TabIndex        =   71
               Top             =   270
               Width           =   885
            End
            Begin VB.Frame FdsTcpCfg 
               BackColor       =   &H00E0E0E0&
               Caption         =   "WS(01): MAIN-LOAD-SEND"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   1185
               Index           =   2
               Left            =   90
               TabIndex        =   65
               Top             =   1860
               Width           =   3165
               Begin VB.TextBox txtRcv 
                  Height          =   285
                  Index           =   2
                  Left            =   690
                  TabIndex        =   166
                  Top             =   -30
                  Width           =   2475
               End
               Begin VB.CheckBox chkTest 
                  Caption         =   "Test 2"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   2
                  Left            =   0
                  Style           =   1  'Graphical
                  TabIndex        =   154
                  Top             =   0
                  Width           =   675
               End
               Begin VB.TextBox MainTcpIp 
                  Alignment       =   2  'Center
                  Height          =   285
                  Index           =   2
                  Left            =   990
                  TabIndex        =   68
                  Text            =   "255.255.255.255"
                  Top             =   270
                  Width           =   1335
               End
               Begin VB.TextBox MainPort 
                  Alignment       =   2  'Center
                  Height          =   285
                  Index           =   2
                  Left            =   2580
                  TabIndex        =   67
                  Text            =   "4394"
                  Top             =   270
                  Width           =   495
               End
               Begin VB.CheckBox chkTask 
                  Caption         =   "Connect"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   2
                  Left            =   90
                  Style           =   1  'Graphical
                  TabIndex        =   66
                  Tag             =   "CONNECT"
                  Top             =   270
                  Width           =   885
               End
               Begin VB.Label SockStatus 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackStyle       =   0  'Transparent
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Socket ready for use"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   2
                  Left            =   90
                  TabIndex        =   70
                  Top             =   855
                  Width           =   2985
               End
               Begin VB.Label MainStatus 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackStyle       =   0  'Transparent
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Socket ready for use"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   2
                  Left            =   90
                  TabIndex        =   69
                  Top             =   585
                  Width           =   2985
               End
               Begin VB.Image PicStat 
                  Appearance      =   0  'Flat
                  Height          =   195
                  Index           =   2
                  Left            =   2355
                  Picture         =   "MyTcpServer.frx":1D64
                  Top             =   315
                  Width           =   195
               End
            End
         End
         Begin VB.CheckBox chkMain 
            Caption         =   "LOAD"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   2
            Left            =   2310
            Style           =   1  'Graphical
            TabIndex        =   54
            Tag             =   "LOAD"
            Top             =   240
            Width           =   1095
         End
         Begin VB.CheckBox chkMain 
            Caption         =   "MAIN"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   1
            Left            =   1200
            Style           =   1  'Graphical
            TabIndex        =   53
            Tag             =   "MAIN"
            Top             =   240
            Width           =   1095
         End
         Begin VB.CheckBox chkMain 
            Caption         =   "FILE"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Index           =   0
            Left            =   90
            Style           =   1  'Graphical
            TabIndex        =   52
            Tag             =   "FILE"
            Top             =   240
            Width           =   1095
         End
         Begin VB.Frame FdsModul 
            BackColor       =   &H00E0E0E0&
            Caption         =   "DUPLEX LINE TO MODULE  FDS-MAIN"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3135
            Index           =   0
            Left            =   90
            TabIndex        =   45
            Top             =   600
            Width           =   3345
            Begin VB.Frame FdsTcpCfg 
               BackColor       =   &H00E0E0E0&
               Caption         =   "WS(01): MAIN-COMP-SEND"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   1185
               Index           =   1
               Left            =   90
               TabIndex        =   58
               Top             =   1860
               Width           =   3165
               Begin VB.TextBox txtRcv 
                  Height          =   285
                  Index           =   1
                  Left            =   690
                  TabIndex        =   165
                  Top             =   -30
                  Width           =   2475
               End
               Begin VB.CheckBox chkTest 
                  Caption         =   "Test 1"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   1
                  Left            =   0
                  Style           =   1  'Graphical
                  TabIndex        =   153
                  Top             =   0
                  Width           =   675
               End
               Begin VB.CheckBox chkTask 
                  Caption         =   "Connect"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   1
                  Left            =   90
                  Style           =   1  'Graphical
                  TabIndex        =   61
                  Tag             =   "CONNECT"
                  Top             =   270
                  Width           =   885
               End
               Begin VB.TextBox MainPort 
                  Alignment       =   2  'Center
                  Height          =   285
                  Index           =   1
                  Left            =   2580
                  TabIndex        =   60
                  Text            =   "4392"
                  Top             =   270
                  Width           =   495
               End
               Begin VB.TextBox MainTcpIp 
                  Alignment       =   2  'Center
                  Height          =   285
                  Index           =   1
                  Left            =   990
                  TabIndex        =   59
                  Text            =   "255.255.255.255"
                  Top             =   270
                  Width           =   1335
               End
               Begin VB.Image PicStat 
                  Appearance      =   0  'Flat
                  Height          =   195
                  Index           =   1
                  Left            =   2355
                  Picture         =   "MyTcpServer.frx":1FAE
                  Top             =   315
                  Width           =   195
               End
               Begin VB.Label MainStatus 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackStyle       =   0  'Transparent
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Socket ready for use"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   1
                  Left            =   90
                  TabIndex        =   63
                  Top             =   585
                  Width           =   2985
               End
               Begin VB.Label SockStatus 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackStyle       =   0  'Transparent
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Socket ready for use"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   1
                  Left            =   90
                  TabIndex        =   62
                  Top             =   855
                  Width           =   2985
               End
            End
            Begin VB.CheckBox chkLine 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   2
               Left            =   1890
               Style           =   1  'Graphical
               TabIndex        =   57
               Top             =   270
               Width           =   885
            End
            Begin VB.CheckBox chkLine 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   1
               Left            =   990
               Style           =   1  'Graphical
               TabIndex        =   56
               Top             =   270
               Width           =   885
            End
            Begin VB.CheckBox chkLine 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   0
               Left            =   90
               Style           =   1  'Graphical
               TabIndex        =   55
               Top             =   270
               Width           =   885
            End
            Begin VB.Frame FdsTcpCfg 
               BackColor       =   &H00E0E0E0&
               Caption         =   "WS(01): COMP-MAIN-RECV"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   1185
               Index           =   0
               Left            =   90
               TabIndex        =   46
               Top             =   600
               Width           =   3165
               Begin VB.TextBox txtRcv 
                  Height          =   285
                  Index           =   0
                  Left            =   690
                  TabIndex        =   164
                  Top             =   -30
                  Width           =   2475
               End
               Begin VB.CheckBox chkTest 
                  Caption         =   "Test 0"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   0
                  Left            =   0
                  Style           =   1  'Graphical
                  TabIndex        =   152
                  Top             =   0
                  Width           =   675
               End
               Begin VB.TextBox MainTcpIp 
                  Alignment       =   2  'Center
                  Height          =   285
                  Index           =   0
                  Left            =   990
                  TabIndex        =   49
                  Text            =   "255.255.255.255"
                  Top             =   270
                  Width           =   1335
               End
               Begin VB.TextBox MainPort 
                  Alignment       =   2  'Center
                  Height          =   285
                  Index           =   0
                  Left            =   2580
                  TabIndex        =   48
                  Text            =   "4390"
                  Top             =   270
                  Width           =   495
               End
               Begin VB.CheckBox chkTask 
                  Caption         =   "Listen"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   0
                  Left            =   90
                  Style           =   1  'Graphical
                  TabIndex        =   47
                  Tag             =   "LISTEN"
                  Top             =   270
                  Width           =   885
               End
               Begin VB.Label SockStatus 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackStyle       =   0  'Transparent
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Socket ready for use"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   0
                  Left            =   90
                  TabIndex        =   51
                  Top             =   855
                  Width           =   2985
               End
               Begin VB.Label MainStatus 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackStyle       =   0  'Transparent
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Socket ready for use"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   240
                  Index           =   0
                  Left            =   90
                  TabIndex        =   50
                  Top             =   585
                  Width           =   2985
               End
               Begin VB.Image PicStat 
                  Appearance      =   0  'Flat
                  Height          =   195
                  Index           =   0
                  Left            =   2355
                  Picture         =   "MyTcpServer.frx":21F8
                  Top             =   315
                  Width           =   195
               End
            End
         End
      End
   End
End
Attribute VB_Name = "MyTcpServer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim TotalBuffer As String
Dim LastError As String
Dim LastBcNum As Long
Dim CloseAllSent As Boolean
Dim StartedAsChild
Dim OutMsg As String
Dim TotPacks As Long
Dim TotBytes As Long
Dim CdrOldStyle As Boolean
Dim DataComplete As Boolean
Dim GetBcHistory As Boolean
Dim DontRefresh As Boolean
Dim DontTrigger As Boolean

Public Sub GetServerData()
    GetBcHistory = True
    'Me.Show
    BcSpooler.chkWork(1).Value = 1
    chkConnect.Value = 1
End Sub
Private Sub chkConnect_Click()
    Dim TcpAdr As String
    Dim TcpPort As String
    If chkConnect.Value = 1 Then
        chkConnect.BackColor = LightGreen
        txtError.Text = ""
        txtMsg.Text = ""
        Text1.Text = ""
        BcSpoolerTab.ResetContent
        BcSpoolerTab.Refresh
        chkDisconnect.Value = 0
        If WS1(0).State <> sckClosed Then WS1(0).Close
        TotalBuffer = ""
        TcpAdr = txtServer.Text
        TcpPort = txtPort.Text
        WS1(0).RemoteHost = TcpAdr
        WS1(0).RemotePort = TcpPort
        WS1(0).Connect TcpAdr, TcpPort
    Else
        chkDisconnect.Value = 1
        chkConnect.Caption = "Connect"
        chkConnect.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkDisconnect_Click()
    If chkDisconnect.Value = 1 Then
        chkDisconnect.BackColor = LightGreen
        On Error Resume Next
        WS1(0).SendData "{=TOT=}000000028{=CMD=}CLOSE"
        txtQueue.Text = "CLOSING"
        'If WS1(0).State <> sckClosed Then WS1(0).Close
        'txtQueue.Text = "CLOSED"
        'chkConnect.Value = 0
        'chkDisconnect.Value = 0
    Else
        chkDisconnect.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkMain_Click(Index As Integer)
    Dim tmpTag As String
    If chkMain(Index).Value = 1 Then
        tmpTag = chkMain(Index).Tag
        If (tmpTag <> ApplFuncCode) And (DontTrigger = False) Then
            'Start the other Module
            chkMain(Index).BackColor = vbYellow
            StartFdsModule tmpTag
        Else
            chkMain(Index).BackColor = LightGreen
        End If
    Else
        chkMain(Index).BackColor = vbButtonFace
    End If
End Sub
Private Sub StartFdsModule(ModuleCode As String)
    Dim tmpCmdTxt As String
    tmpCmdTxt = ""
    tmpCmdTxt = tmpCmdTxt & App.Path & "\" & App.EXEName
    tmpCmdTxt = tmpCmdTxt & " [FC]" & ModuleCode & "[TCP]000.000.000.000"
    Shell tmpCmdTxt, vbNormalNoFocus
End Sub
Private Sub chkSendMsg_Click()
    Dim tmpMsg As String
    Dim tmpData As String
    Dim tmpCfgData As String
    Dim MsgLen As Integer
    If chkSendMsg.Value = 1 Then
        txtMsg.Text = ""
        Text1.Text = ""
        txtMsg.Refresh
        BcSpoolerTab.ResetContent
        BcSpoolerTab.Refresh
        tmpData = Trim(txtFld.Text)
        tmpData = GetIniEntry(myIniFullName, ButtonSectionName, "", "HISTORY_FIELDS", tmpData)
        If tmpData = "" Then
            tmpCfgData = GetIniEntry(myIniFullName, ButtonSectionName, "", "REGN_HISTORY_FIELDS", "")
            If tmpCfgData <> "" Then
                tmpData = tmpData & "," & tmpCfgData
            End If
            tmpCfgData = GetIniEntry(myIniFullName, ButtonSectionName, "", "BAYS_HISTORY_FIELDS", "")
            If tmpCfgData <> "" Then
                tmpData = tmpData & "," & tmpCfgData
            End If
            tmpCfgData = GetIniEntry(myIniFullName, ButtonSectionName, "", "FTYP_HISTORY_FIELDS", "")
            If tmpCfgData <> "" Then
                tmpData = tmpData & "," & tmpCfgData
            End If
            If tmpData <> "" Then
                tmpData = Mid(tmpData, 2)
            End If
        End If
        txtFld.Text = tmpData
        tmpMsg = ""
        tmpData = Trim(txtCmd.Text)
        If tmpData <> "" Then
            tmpMsg = tmpMsg & "{=TOT=}" & Space(9)
            tmpMsg = tmpMsg & "{=CMD=}" & tmpData
        End If
        If tmpMsg <> "" Then
            BcSpooler.chkWork(1).Value = 1
            'msgLen = Len(tmpMsg)
            'Mid(tmpMsg, 8) = CStr(msgLen)
            BuildOutMsg
            tmpMsg = OutMsg
            WS1(0).SendData tmpMsg
        Else
        End If
        chkSendMsg.Value = 0
    Else
    End If
End Sub

Private Sub chkTask_Click(Index As Integer)
    Dim tmpTag As String
    Dim tmpCode As String
    Dim WsState As Long
    If chkTask(Index).Value = 1 Then
        TcpTimer.Enabled = False
        chkTask(Index).BackColor = LightGreen
        tmpTag = chkTask(Index).Tag
        tmpCode = GetItem(tmpTag, 1, ",")
        Select Case tmpCode
            Case "LISTEN"
                If WsFds(Index).State <> sckClosed Then WsFds(Index).Close
                WsFds(Index).LocalPort = MainPort(Index).Text
                WsFds(Index).Listen
                PicStat(Index).Picture = ImagePool.SmallPlain(4).Picture
                PicStat(Index).Tag = "YEL"
                WsState = WsFds(Index).State
                SockStatus(Index).Caption = TranslateSocketStatus(WsState)
            Case "CONNECT"
                If WsFds(Index).State <> sckClosed Then WsFds(Index).Close
                WsFds(Index).RemoteHost = MainTcpIp(Index).Text
                WsFds(Index).RemotePort = MainPort(Index).Text
                PicStat(Index).Picture = ImagePool.SmallPlain(4).Picture
                PicStat(Index).Tag = "YEL"
                WsFds(Index).Connect MainTcpIp(Index).Text, MainPort(Index).Text
                WsState = WsFds(Index).State
                SockStatus(Index).Caption = TranslateSocketStatus(WsState)
            Case Else
                chkTask(Index).Value = 0
        End Select
        TcpTimer.Enabled = True
    Else
        tmpTag = chkTask(Index).Tag
        tmpCode = GetItem(tmpTag, 1, ",")
        Select Case tmpCode
            Case "LISTEN"
                If WsFds(Index).State <> sckClosed Then WsFds(Index).Close
                If DontRefresh = False Then
                    PicStat(Index).Picture = ImagePool.SmallPlain(1).Picture
                    PicStat(Index).Tag = "GRY"
                    MainStatus(Index).Caption = "Port disabled"
                End If
                chkTask(Index).Caption = "Listen"
            Case "CONNECT"
                If WsFds(Index).State <> sckClosed Then WsFds(Index).Close
                If DontRefresh = False Then
                    PicStat(Index).Picture = ImagePool.SmallPlain(1).Picture
                    PicStat(Index).Tag = "GRY"
                    MainStatus(Index).Caption = "Disconnected"
                End If
                chkTask(Index).Caption = "Connect"
            Case Else
                chkTask(Index).Value = 0
        End Select
        chkTask(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub chkTest_Click(Index As Integer)
    Dim tmpData As String
    Dim tmpMsg As String
    If chkTest(Index).Value = 1 Then
        tmpData = "LINE" & CStr(Index)
        tmpMsg = BuildFdsMsg("MS", tmpData)
        WsFds(Index).SendData tmpMsg
        chkTest(Index).Value = 0
    End If
End Sub

Private Sub Command1_Click()
    Dim tmpFileName As String
    Dim tmpPath As String
    Dim tmpName As String
    If BcSpoolerTab.GetLineCount > 0 Then
        tmpPath = UFIS_TMP
        tmpName = "CDRTEST" & "_BcSpool.txt"
        tmpFileName = tmpPath & "\" & tmpName
        BcSpoolerTab.WriteToFile tmpFileName, False
    End If
End Sub

Private Sub Command2_Click()
    Dim tmpFileName As String
    Dim tmpPath As String
    Dim tmpName As String
    On Error Resume Next
    BcSpoolerTab.ResetContent
    BcSpoolerTab.Refresh
    tmpPath = UFIS_TMP
    tmpName = "CDRTEST" & "_BcSpool.txt"
    tmpFileName = tmpPath & "\" & tmpName
    BcSpoolerTab.ReadFromFile tmpFileName
    BcSpoolerTab.AutoSizeColumns
End Sub

Private Sub Command3_Click()
    TmpFreeStrg1 = "Retrieving Update History From The Server."
    TmpFreeStrg2 = "This will take a few seconds. Please wait ..."
    MainDialog.MsgPanelShow 0, TmpFreeStrg1, TmpFreeStrg2, -1, -1, False
    SendBcHistToSpooler
End Sub

Private Sub Form_Activate()
    Static IsActive As Boolean
    Dim tmpCheck As String
    Dim tmpData As String
    Dim i As Integer
    If Not IsActive Then
        IsActive = True
        Me.Refresh
        StartedAsChild = False
    End If
End Sub
Private Sub SetPanelStatus(Index As Integer, SetEnable As Boolean)
    FdsTcpCfg(Index).Enabled = SetEnable
    MainTcpIp(Index).Enabled = SetEnable
    MainPort(Index).Enabled = SetEnable
    SockStatus(Index).Enabled = SetEnable
    chkTask(Index).Enabled = SetEnable
    If SetEnable = True Then
        PicStat(Index).Picture = ImagePool.SmallPlain(1).Picture
        PicStat(Index).Tag = "GRY"
        MainTcpIp(Index).Text = WsFds(Index).LocalIP
        MainTcpIp(Index).BackColor = vbWhite
        MainPort(Index).BackColor = vbWhite
        SockStatus(Index).Caption = "Socket ready for use"
    Else
        PicStat(Index).Picture = ImagePool.SmallPlain(1).Picture
        PicStat(Index).Tag = "GRY"
        MainTcpIp(Index).Text = ""
        MainTcpIp(Index).BackColor = vbButtonFace
        MainPort(Index).BackColor = vbButtonFace
        SockStatus(Index).Caption = "Objects available"
    End If
End Sub
Private Sub Form_Load()
    Dim BcFields As String
    Dim tmpData As String
    Dim tmpStrg As String
    Dim i As Integer
    
    HOPO.Text = GetIniEntry("", "GLOBAL", "", "HOMEAIRPORT", "???")
    TblExt.Text = GetIniEntry("", "GLOBAL", "", "TABLEEXTENSION", "TAB")
    txtServer.Text = GetIniEntry("", "GLOBAL", "", "HOSTNAME", "")
    BcSpoolerTab.ResetContent
    BcFields = "BSEQ,BNUM,BRSQ,BCMD,BTBL,BSEL,BDAT,BFLD,BTWS,BTWE,BWKS,BUSR,REMA"
    BcSpoolerTab.HeaderString = BcFields
    BcSpoolerTab.LogicalFieldList = BcFields
    BcSpoolerTab.HeaderLengthString = "10,10,10,10,10,10,10,10,10,10,10,10,10,10"
    BcSpoolerTab.FontName = "Courier New"
    BcSpoolerTab.SetFieldSeparator (Chr(16))
    'BcSpoolerTab.ShowHorzScroller True
    BcSpoolerTab.AutoSizeByHeader = True
    BcSpoolerTab.AutoSizeColumns
    
    
    Select Case ApplMainCode
        Case "FDT"
        Case "FDC", "OCM"
        Case "FDS"
            FuncPanel(1).Top = 0
            FuncPanel(1).Left = 0
            FuncPanel(1).Width = FdsAppl(0).Width + 240 + 60
            FuncPanel(1).ZOrder
            Me.Width = FuncPanel(1).Width + (Me.Width - Me.ScaleWidth)
            Me.Height = FuncPanel(1).Height + (Me.Height - Me.ScaleHeight)
            tmpStrg = "[" & ApplFuncCode & "] " & ApplMainCode & "-"
            tmpData = Me.Caption
            tmpData = Replace(tmpData, tmpStrg, "", 1, -1, vbBinaryCompare)
            tmpData = Trim(tmpData)
            tmpData = tmpStrg & tmpData
            Me.Caption = tmpData
            For i = 0 To FdsAppl.UBound
                FdsAppl(i).Enabled = False
                FdsAppl(i).Visible = False
            Next
            For i = 0 To FdsModul.UBound
                FdsModul(i).Enabled = False
            Next
            For i = 0 To FdsTcpCfg.UBound
                If i > 0 Then Load WsFds(i)
                tmpData = FdsTcpCfg(i).Caption
                If InStr(tmpData, ":") > 0 Then tmpData = GetItem(tmpData, 2, ":")
                tmpStrg = Right("00" & CStr(i), 2)
                FdsTcpCfg(i).Caption = "WS(" & tmpStrg & "): " & tmpData
                SetPanelStatus i, False
            Next
            If ApplIsFdsFile Then
                FdsAppl(0).Left = 120
                FdsAppl(0).Visible = True
                FdsAppl(0).Enabled = True
                FdsModul(0).Enabled = True
                FdsModul(1).Enabled = True
                SetPanelStatus 0, True
                SetPanelStatus 1, True
                SetPanelStatus 2, True
                SetPanelStatus 3, True
                chkMain(0).Value = 1
            End If
            If ApplIsFdsMain Then
                FdsAppl(1).Left = 120
                FdsAppl(1).Visible = True
                FdsAppl(1).Enabled = True
                FdsModul(2).Enabled = True
                FdsModul(3).Enabled = True
                SetPanelStatus 4, True
                SetPanelStatus 5, True
                SetPanelStatus 6, True
                SetPanelStatus 7, True
                chkMain(4).Value = 1
            End If
            If ApplIsFdsLoad Then
                FdsAppl(2).Left = 120
                FdsAppl(2).Visible = True
                FdsAppl(2).Enabled = True
                FdsModul(4).Enabled = True
                FdsModul(5).Enabled = True
                SetPanelStatus 8, True
                SetPanelStatus 9, True
                SetPanelStatus 10, True
                SetPanelStatus 11, True
                chkMain(6).Value = 1
            End If
        Case Else
    End Select
    
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Cancel = True
    Me.Hide
End Sub

Private Sub Form_Resize()
    Dim NewSize As Long
    NewSize = Me.ScaleWidth - txtMsg.Left - 150
    If NewSize > 300 Then
        Text1.Width = NewSize
        txtMsg.Width = NewSize
        BcSpoolerTab.Width = NewSize
    End If
    'newSize = Me.ScaleHeight - txtMsg.Top - 150
    'If newSize > 300 Then txtMsg.Height = newSize
    NewSize = Me.ScaleHeight - BcSpoolerTab.Top - 150
    If NewSize > 300 Then BcSpoolerTab.Height = NewSize
End Sub

Private Sub TcpTimer_Timer()
    Dim WsState As Long
    Dim RestartTimer As Boolean
    Dim i As Integer
    RestartTimer = False
    TcpTimer.Enabled = RestartTimer
    For i = 0 To WsFds.UBound
        WsState = WsFds(i).State
        SockStatus(i).Caption = TranslateSocketStatus(WsState)
        If WsState = sckConnected Then
            If PicStat(i).Tag <> "GRN" Then
                PicStat(i).Picture = ImagePool.SmallPlain(3).Picture
                PicStat(i).Tag = "GRN"
                chkTask(i).Caption = "Line Valid"
            End If
        End If
        If chkTask(i).Value = 1 Then RestartTimer = True
    Next
    TcpTimer.Enabled = RestartTimer
End Sub

Private Sub txtMsg_Change()
    Dim tmpText As String
    tmpText = txtMsg.Text
    'txtTot.Text = CStr(Len(tmpText))
    tmpText = Replace(tmpText, vbCr, "", 1, -1, vbBinaryCompare)
    txtBcNum.Text = CStr(Len(tmpText))
End Sub

Private Sub WS1_Close(Index As Integer)
    If WS1(Index).State <> sckClosed Then WS1(Index).Close
    txtQueue.Text = "CLOSED"
    chkConnect.Value = 0
End Sub

Private Sub WS1_Connect(Index As Integer)
    Dim tmpTxt As String
    txtQueue.Text = "OPEN"
    TotalBuffer = ""
    chkConnect.Caption = "Connected"
    If GetBcHistory Then
        chkSendMsg.Value = 1
    End If
End Sub

Private Sub WS1_DataArrival(Index As Integer, ByVal bytesTotal As Long)
    Dim GotMessage As Boolean
    Dim Data As String
    Dim tmpData As String
    Dim tmpPack As String
    Dim tmpRecord As String
    Dim tmpErrMsg As String
    Dim tmpResult As String
    Dim DataLen As Integer
    Dim TotalLen As Long
    Dim Tot As String

    DataComplete = False
    WS1(Index).GetData Data, vbString
    TotalBuffer = TotalBuffer & Data
    'TotalBuffer = Replace(TotalBuffer, vbLf, vbNewLine, 1, -1, vbBinaryCompare)
    GotMessage = True
    While GotMessage
        GotMessage = False
        If Len(TotalBuffer) >= 16 Then
            GetKeyItem Tot, TotalBuffer, "{=TOT=}", "{="
            TotalLen = Val(Tot)
            If Len(TotalBuffer) >= TotalLen Then
                GotMessage = True
                Data = Left(TotalBuffer, TotalLen)
                TotalBuffer = Mid(TotalBuffer, TotalLen + 1)
                GetKeyItem tmpErrMsg, Data, "{=ERR=}", "{="
                GetKeyItem tmpPack, Data, "{=PACK=}", "{="
                GetKeyItem tmpRecord, Data, "{=BCREC=}", "{=/BCREC=}"
                If tmpRecord <> "" Then StoreBcRecord tmpRecord
                If tmpPack = "0" Then
                    SendAckToCdrhdl
                    BcSpoolerTab.AutoSizeColumns
                    BcSpoolerTab.OnVScrollTo 0
                    BcSpoolerTab.Refresh
                    txtMsg.Text = Data
                    SendBcHistToSpooler
                End If
                If tmpErrMsg <> "" Then
                    SendAckToCdrhdl
                    txtMsg.Text = Data
                    ShowErrorMessage "The server doesn't support this feature.", tmpErrMsg
                End If
            End If
        End If
    Wend
End Sub
Private Sub StoreBcRecord(CurBcRec As String)
    Dim BcRec As String
    Dim BcFldLst As String
    Dim BcXmlTag As String
    Dim tmpData As String
    Dim FldSep As String
    Dim CurTag As String
    Dim BgnTag As String
    Dim EndTag As String
    Dim MaxLine As Long
    Dim LineNo As Long
    Dim Itm As Integer
    FldSep = Chr(16)
    EndTag = "{="
    BcFldLst = "BSEQ,BNUM,BRSQ,BCMD,BTBL,BSEL,BDAT,BFLD,BTWS,BTWE,BWKS,BUSR,REMA"
    BcXmlTag = "BCSEQ,BCNUM,BCRSQ,BCCMD,BCTBL,BCSEL,BCDAT,BCFLD,BCTWS,BCTWE,BCWKS,BCUSR,REMA"
    BcRec = ""
    Itm = 0
    CurTag = "START"
    While CurTag <> ""
        Itm = Itm + 1
        CurTag = GetItem(BcXmlTag, Itm, ",")
        If CurTag <> "" Then
            BgnTag = "{=" & CurTag & "=}"
            GetKeyItem tmpData, CurBcRec, BgnTag, EndTag
            BcRec = BcRec & tmpData & FldSep
        End If
    Wend
    BcSpoolerTab.InsertTextLine BcRec, False
    'Maxline = BcSpoolerTab.GetLineCount - 1
    'BcSpoolerTab.OnVScrollTo Maxline
    
        'BcSpooler.BcSpoolerTab.InsertTextLine BcRec, False
        ''LineNo = BcSpooler.BcSpoolerTab.GetLineCount
        ''HandleBroadCastSpooler LineNo, BcRec
        'BcSpooler.BcSpoolTimer.Enabled = True
    
End Sub
Private Sub SendBcHistToSpooler()
    Dim BcRec As String
    Dim CurLine As Long
    Dim MaxLine As Long
    BcSpoolReleased = True
    MaxLine = BcSpoolerTab.GetLineCount - 1
    For CurLine = MaxLine To 0 Step -1
        DoEvents
        'BcRec = BcSpoolerTab.GetLineValues(CurLine)
        'BcSpooler.BcSpoolerTab.InsertTextLine BcRec, False
        ''LineNo = BcSpooler.BcSpoolerTab.GetLineCount
        ''HandleBroadCastSpooler LineNo, BcRec
        'BcSpooler.BcSpoolTimer.Enabled = True
        GetBcFromSpooler CurLine
    Next
    MainDialog.MsgPanelHide 0
    If MainDialog.SidePanel(1).Visible Then MainDialog.ReleaseInfoPanel
    BcSpooler.chkWork(1).Value = 0
    GetBcHistory = False
End Sub
Public Sub GetBcFromSpooler(LineNo As Long)
    Dim ReqId As String
    Dim DestName As String
    Dim RecvName As String
    Dim CedaCmd As String
    Dim ObjName As String
    Dim Seq As String
    Dim tws As String
    Dim twe As String
    Dim CedaSqlKey As String
    Dim Fields As String
    Dim Data As String
    Dim BcNum As String
    'If Not Spooling Then
        'If BcSpoolerTab.GetLineCount > 0 Then
            'BcFields = "BSEQ,BNUM,BRSQ,BCMD,BTBL,BSEL,BDAT,BFLD,BTWS,BTWE,BWKS,BUSR,REMA"
            'TabColNo = "0   ,1   ,2   ,3   ,4   ,5   ,6   ,7   ,8   ,9   ,10  ,11  ,12
            Seq = BcSpoolerTab.GetColumnValue(LineNo, 0)
            BcNum = BcSpoolerTab.GetColumnValue(LineNo, 1)
            ReqId = BcSpoolerTab.GetColumnValue(LineNo, 2)
            CedaCmd = BcSpoolerTab.GetColumnValue(LineNo, 3)
            ObjName = BcSpoolerTab.GetColumnValue(LineNo, 4)
            CedaSqlKey = BcSpoolerTab.GetColumnValue(LineNo, 5)
            Data = BcSpoolerTab.GetColumnValue(LineNo, 6)
            Fields = BcSpoolerTab.GetColumnValue(LineNo, 7)
            tws = BcSpoolerTab.GetColumnValue(LineNo, 8)
            twe = BcSpoolerTab.GetColumnValue(LineNo, 9)
            DestName = BcSpoolerTab.GetColumnValue(LineNo, 10)
            RecvName = BcSpoolerTab.GetColumnValue(LineNo, 11)
            'BcRema = BcSpoolerTab.GetColumnValue(LineNo, 12)
            'BcSpoolerTab.DeleteLine 0
            HandleBroadcast ReqId, DestName, RecvName, CedaCmd, ObjName, Seq, tws, twe, CedaSqlKey, Fields, Data, BcNum
            DoEvents
        'End If
    'End If
    'BcSpoolTimer.Enabled = Not Spooling
    'If BcSpoolerTab.GetLineCount = 0 Then
    '    BcSpoolTimer.Enabled = False
    'ElseIf Scrolling Then
    '    BcSpoolerTab.OnVScrollTo BcSpoolerTab.GetLineCount
    'End If
End Sub

Private Sub SendAckToCdrhdl()
    WS1(0).SendData "{=TOT=}000000027{=ACK=}NEXT"
    'If (DataComplete) And (CdrOldStyle) Then chkConnect.Value = 0
End Sub

Private Function GetKeyItem(ByRef rResult, ByRef rString, ByVal vStartSep, ByVal vEndSep) As Boolean
    Dim llPosStart As Long
    Dim llPosEnd As Long
    GetKeyItem = False
    rResult = ""
    llPosStart = InStr(1, rString, vStartSep, vbTextCompare)
    If llPosStart > 0 Then
        GetKeyItem = True
        llPosStart = llPosStart + Len(vStartSep)
        llPosEnd = InStr(llPosStart, rString, vEndSep, vbTextCompare)
        If llPosEnd > 0 Then
            rResult = Mid(rString, llPosStart, (llPosEnd - llPosStart))
        Else
            rResult = Right(rString, Len(rString) - llPosStart + 1)
        End If
    End If
End Function
                    
Private Sub WS1_Error(Index As Integer, ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
    Dim tmpMsg As String
    tmpMsg = "WS" & CStr(Index) & " (ERR" & CStr(Number) & "): " & Description
    txtError.Text = tmpMsg
    If WS1(Index).State <> sckClosed Then WS1(Index).Close
    DoEvents
    TmpFreeStrg1 = "Remote Procedure Call Failed:"
    TmpFreeStrg2 = tmpMsg
    ShowErrorMessage TmpFreeStrg1, TmpFreeStrg2
    chkConnect.Value = 0
End Sub
Private Sub ShowErrorMessage(ErrMsg1 As String, ErrMsg2 As String)
    MainDialog.MsgPanelShow 0, ErrMsg1, ErrMsg2, -1, -1, False
    DoEvents
    Sleep 6
    If GetBcHistory = True Then
        BcSpooler.chkWork(1).Value = 0
        GetBcHistory = False
    End If
End Sub
Private Sub SendDataToBcOut(Index As Integer, BcCmd As String, Data As String)
    Dim tmpStr As String
    Dim tmpLen As Long
    'tmpStr = Space(16)
    'tmpStr = tmpStr + "{=CMD=}" + BcCmd
    'If Data <> "" Then tmpStr = tmpStr + "{=DAT=}" + Data
    'tmpLen = Len(tmpStr)
    'Mid(tmpStr, 1, 16) = Left("{=TOT=}" & CStr(tmpLen) & "                  ", 16)
    'WS1(Index).SendData tmpStr
End Sub

Private Function GetHexTcpIp() As String
    Dim MyTcpIpAdr As String
    Dim HexTcpAdr As String
    Dim tmpDat As String
    Dim tmpVal As Integer
    Dim ItmNbr As Integer
    
    MyTcpIpAdr = WS1(0).LocalIP
    HexTcpAdr = ""
    For ItmNbr = 1 To 4
        tmpVal = Val(GetItem(MyTcpIpAdr, ItmNbr, "."))
        tmpDat = Hex(tmpVal)
        HexTcpAdr = HexTcpAdr + Right("00" + tmpDat, 2)
    Next
    GetHexTcpIp = HexTcpAdr
End Function

Private Sub InitFromTo()
    Dim FrameDay
    Dim Days As Integer
    'Days = Abs(Val(txtBgnOff.Text))
    'FrameDay = DateAdd("d", -Days, Now)
    'txtVpfr.Text = Format(FrameDay, "yyyymmdd")
    'Days = Abs(Val(txtEndOff.Text))
    'FrameDay = DateAdd("d", Days, Now)
    'txtVpto.Text = Format(FrameDay, "yyyymmdd")
End Sub

Private Sub BuildOutMsg()
    Dim tmpCdrBuf As String
    Dim tmpLen As Integer
    Dim tmpTot As String
    tmpCdrBuf = ""
    tmpCdrBuf = tmpCdrBuf & "{=CMD=}" & txtCmd.Text
    tmpCdrBuf = tmpCdrBuf & "{=WKS=}" & txtWks.Text
    tmpCdrBuf = tmpCdrBuf & "{=USR=}" & txtUsr.Text
    tmpCdrBuf = tmpCdrBuf & "{=PACK=}" & txtPack.Text
    tmpCdrBuf = tmpCdrBuf & "{=TBL=}" & txtTbl.Text
    tmpCdrBuf = tmpCdrBuf & "{=FLD=}" & txtFld.Text
    tmpCdrBuf = tmpCdrBuf & "{=WHE=}WHERE " & txtWhe.Text
    tmpLen = Len(tmpCdrBuf) + 16
    tmpTot = "{=TOT=}" & Right("000000000" & CStr(tmpLen), 9)
    OutMsg = tmpTot & tmpCdrBuf
    txtPack.Tag = txtPack.Text
End Sub

Private Sub WS1_SendComplete(Index As Integer)
    If txtQueue.Text = "CLOSING" Then
        'Sleep 2
        If WS1(0).State <> sckClosed Then WS1(0).Close
        txtQueue.Text = "CLOSED"
        chkConnect.Value = 0
        chkDisconnect.Value = 0
    End If
End Sub

Private Sub WsFds_Close(Index As Integer)
    MainStatus(Index).Caption = "Closed"
    PicStat(Index).Picture = ImagePool.SmallPlain(1).Picture
    PicStat(Index).Tag = "GRY"
    chkTask(Index).Value = 0
End Sub

Private Sub WsFds_Connect(Index As Integer)
    MainStatus(Index).Caption = "Connect"
End Sub

Private Sub WsFds_ConnectionRequest(Index As Integer, ByVal requestID As Long)
    MainStatus(Index).Caption = "Connection Request"
    If WsFds(Index).State <> sckClosed Then WsFds(Index).Close
    WsFds(Index).Accept requestID
    PicStat(Index).Picture = ImagePool.SmallPlain(3).Picture
    PicStat(Index).Tag = "GRN"
    chkTask(Index).Caption = "Line Valid"
End Sub

Private Sub WsFds_DataArrival(Index As Integer, ByVal bytesTotal As Long)
    Dim GotMessage As Boolean
    Dim MsgBuffer As String
    Dim MsgData As String
    Dim TotalLen As Long
    Dim tmpItem As String
    Dim tmpCode As String
    Dim tmpData As String

    MainStatus(Index).Caption = "Data Arrival"
    DataComplete = False
    WsFds(Index).GetData MsgData, vbString
    MsgBuffer = WsFds(Index).Tag & MsgData
    GotMessage = True
    While GotMessage
        GotMessage = False
        If Len(MsgBuffer) >= 8 Then
            GetKeyItem tmpItem, MsgBuffer, "[SZ]", "["
            TotalLen = Val(tmpItem)
            If Len(MsgBuffer) >= TotalLen Then
                GotMessage = True
                MsgData = Left(MsgBuffer, TotalLen)
                MsgBuffer = Mid(MsgBuffer, TotalLen + 1)
                GetKeyItem tmpCode, MsgData, "[CD]", "["
                GetKeyItem tmpData, MsgData, "[MD]", "["
                txtRcv(Index).Text = MsgData
            End If
        End If
    Wend
    WsFds(Index).Tag = MsgBuffer
End Sub

Private Function BuildFdsMsg(MsgCode As String, MsgData As String) As String
    Dim tmpBuf As String
    Dim tmpTot As String
    Dim MsgLen As Long
    tmpBuf = ""
    tmpBuf = tmpBuf & "[CD]" & MsgCode
    tmpBuf = tmpBuf & "[MD]" & MsgData
    tmpTot = "[SZ]0000"
    MsgLen = Len(tmpBuf) + Len(tmpTot)
    tmpTot = "[SZ]" & Right("0000" & CStr(MsgLen), 4)
    tmpBuf = tmpTot & tmpBuf
    BuildFdsMsg = tmpBuf
End Function

Private Sub WsFds_Error(Index As Integer, ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
    DontRefresh = True
    MainStatus(Index).Caption = Description
    PicStat(Index).Picture = ImagePool.SmallPlain(2).Picture
    PicStat(Index).Tag = "RED"
    chkTask(Index).Value = 0
    DontRefresh = False
End Sub

Private Sub WsFds_SendComplete(Index As Integer)
    MainStatus(Index).Caption = "Send Complete"
End Sub

Private Sub WsFds_SendProgress(Index As Integer, ByVal bytesSent As Long, ByVal bytesRemaining As Long)
    MainStatus(Index).Caption = "Send Progress"
End Sub

Private Function TranslateSocketStatus(SockStatus As Long) As String
    Dim Result As String
    Select Case SockStatus
        Case sckAddressInUse
            Result = "Address In Use"
        Case sckAddressNotAvailable
            Result = "Address Not Available"
        Case sckAlreadyComplete
            Result = "Already Complete"
        Case sckAlreadyConnected
            Result = "Already Connected"
        Case sckBadState
            Result = "Bad State"
        Case sckClosed
            Result = "Closed"
        Case sckClosing
            Result = "Closing"
        Case sckConnectAborted
            Result = "Connect Aborted"
        Case sckConnected
            Result = "Connected"
        Case sckConnecting
            Result = "Connecting"
        Case sckConnectionPending
            Result = "Connection Pending"
        Case sckConnectionRefused
            Result = "Connection Refused"
        Case sckConnectionReset
            Result = "Connection Reset"
        Case sckError
            Result = "Error"
        Case sckGetNotSupported
            Result = "Get Not Supported"
        Case sckHostNotFound
            Result = "Host Not Found"
        Case sckHostNotFoundTryAgain
            Result = "Host Not Found Try Again"
        Case sckHostResolved
            Result = "Host Resolved"
        Case sckInProgress
            Result = "In Progress"
        Case sckInvalidArg
            Result = "Invalid Arg (?)"
        Case sckInvalidArgument
            Result = "Invalid Argument"
        Case sckInvalidOp
            Result = "Invalid Operation"
        Case sckInvalidPropertyValue
            Result = "Invalid Property Value"
        Case sckListening
            Result = "Listening"
        Case sckMsgTooBig
            Result = "Message Too Big"
        Case sckNetReset
            Result = "Net Reset"
        Case sckNetworkSubsystemFailed
            Result = "Network Subsystem Failed"
        Case sckNetworkUnreachable
            Result = "NetworkUnreachable"
        Case sckNoBufferSpace
            Result = "No Buffer Space"
        Case sckNoData
            Result = "No Data"
        Case sckNonRecoverableError
            Result = "Non Recoverable Error"
        Case sckNotConnected
            Result = "Not Connected"
        Case sckNotInitialized
            Result = "Not Initialized"
        Case sckNotSocket
            Result = "Not Socket"
        Case sckOpCanceled
            Result = "Operation Canceled"
        Case sckOpen
            Result = "Open"
        Case sckOutOfMemory
            Result = "Out Of Memory"
        Case sckOutOfRange
            Result = "Out Of Range"
        Case sckPortNotSupported
            Result = "Port Not Supported"
        Case sckResolvingHost
            Result = "Resolving Host"
        Case sckSetNotSupported
            Result = "Set Not Supported"
        Case sckSocketShutdown
            Result = "Socket Shutdown"
        Case sckSuccess
            Result = "Success"
        Case sckTCPProtocol
            Result = "TCP Protocol"
        Case sckTimedout
            Result = "Timed Out"
        Case sckUDPProtocol
            Result = "UDP Protocol"
        Case sckUnsupported
            Result = "Unsupported"
        Case sckWouldBlock
            Result = "WouldBlock"
        Case sckWrongProtocol
            Result = "Wrong Protocol"
        Case Else
            Result = "Unknown"
    End Select
    TranslateSocketStatus = Result
End Function
