VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form InfoConfig 
   Caption         =   "Configuration of Info Messages"
   ClientHeight    =   3465
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10725
   Icon            =   "InfoConfig.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3465
   ScaleWidth      =   10725
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame1 
      Caption         =   "Variables"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2445
      Left            =   7620
      TabIndex        =   13
      Top             =   210
      Width           =   2775
      Begin VB.ListBox List1 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1950
         ItemData        =   "InfoConfig.frx":0442
         Left            =   120
         List            =   "InfoConfig.frx":045B
         TabIndex        =   14
         Top             =   300
         Width           =   2505
      End
   End
   Begin MSComDlg.CommonDialog CommonDialog 
      Left            =   210
      Top             =   2640
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame InfoFrame 
      Caption         =   "Band 3: System && Communication"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   765
      Index           =   2
      Left            =   90
      TabIndex        =   9
      Top             =   1890
      Width           =   7395
      Begin VB.TextBox InfoText 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   150
         TabIndex        =   12
         Top             =   300
         Width           =   6345
      End
      Begin VB.PictureBox InfoTextColor 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Index           =   2
         Left            =   6570
         ScaleHeight     =   255
         ScaleWidth      =   255
         TabIndex        =   11
         Top             =   300
         Width           =   315
      End
      Begin VB.PictureBox InfoBackColor 
         BackColor       =   &H00FF8080&
         Height          =   315
         Index           =   2
         Left            =   6930
         ScaleHeight     =   255
         ScaleWidth      =   255
         TabIndex        =   10
         Top             =   300
         Width           =   315
      End
   End
   Begin VB.Frame InfoFrame 
      Caption         =   "Band 2: Flight Related Information"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   765
      Index           =   1
      Left            =   90
      TabIndex        =   5
      Top             =   1050
      Width           =   7395
      Begin VB.TextBox InfoText 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   150
         TabIndex        =   8
         Top             =   300
         Width           =   6345
      End
      Begin VB.PictureBox InfoTextColor 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Index           =   1
         Left            =   6570
         ScaleHeight     =   255
         ScaleWidth      =   255
         TabIndex        =   7
         Top             =   300
         Width           =   315
      End
      Begin VB.PictureBox InfoBackColor 
         BackColor       =   &H00FF8080&
         Height          =   315
         Index           =   1
         Left            =   6930
         ScaleHeight     =   255
         ScaleWidth      =   255
         TabIndex        =   6
         Top             =   300
         Width           =   315
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Done"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4650
      TabIndex        =   4
      Top             =   2850
      Width           =   885
   End
   Begin VB.Frame InfoFrame 
      Caption         =   "Band 1: Global Messages"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   765
      Index           =   0
      Left            =   90
      TabIndex        =   0
      Top             =   210
      Width           =   7395
      Begin VB.PictureBox InfoBackColor 
         BackColor       =   &H00FF8080&
         Height          =   315
         Index           =   0
         Left            =   6930
         ScaleHeight     =   255
         ScaleWidth      =   255
         TabIndex        =   3
         Top             =   300
         Width           =   315
      End
      Begin VB.PictureBox InfoTextColor 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Index           =   0
         Left            =   6570
         ScaleHeight     =   255
         ScaleWidth      =   255
         TabIndex        =   2
         Top             =   300
         Width           =   315
      End
      Begin VB.TextBox InfoText 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   150
         TabIndex        =   1
         Top             =   300
         Width           =   6345
      End
   End
End
Attribute VB_Name = "InfoConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim CurTxtIdx As Integer
Dim AppIsUp As Boolean

Private Sub Command1_Click()
    GetOrSetInfoCfg "SET"
    FlightRotation.MainButton(11).Value = 0
    Me.Hide
End Sub
Private Sub GetOrSetInfoCfg(ForWhat As String)
    Dim SavePath As String
    Dim fn As Integer
    Dim i As Integer
    Dim idx As Integer
    Dim OutStrg As String
    Dim KeyItem As String
    Dim TxtColor As Long
    On Error GoTo ErrExit
    SavePath = UFIS_SYSTEM & "\FDTInfoConfig.txt"
    fn = FreeFile()
    If ForWhat = "SET" Then
        Open SavePath For Output As #fn
            For i = 0 To InfoText.UBound
                OutStrg = "{=IDX=}" & CStr(i)
                OutStrg = OutStrg & "{=TXT=}" & InfoText(i).Text
                OutStrg = OutStrg & "{=FOR=}" & CStr(InfoTextColor(i).BackColor)
                OutStrg = OutStrg & "{=BCK=}" & CStr(InfoBackColor(i).BackColor)
                Print #fn, OutStrg
            Next
        Close #fn
    End If
    If ForWhat = "GET" Then
        Open SavePath For Input As #fn
            For i = 0 To InfoText.UBound
                Line Input #fn, OutStrg
                GetKeyItem KeyItem, OutStrg, "{=IDX=}", "{="
                GetKeyItem KeyItem, OutStrg, "{=TXT=}", "{="
                InfoText(i).Text = KeyItem
                FlightRotation.RotFlag(i).Tag = KeyItem
                GetKeyItem KeyItem, OutStrg, "{=FOR=}", "{="
                TxtColor = val(KeyItem)
                InfoText(i).ForeColor = TxtColor
                InfoTextColor(i).BackColor = TxtColor
                FlightRotation.RotFlag(i).ForeColor = TxtColor
                GetKeyItem KeyItem, OutStrg, "{=BCK=}", "{="
                TxtColor = val(KeyItem)
                InfoText(i).BackColor = TxtColor
                InfoBackColor(i).BackColor = TxtColor
                FlightRotation.RotFlag(i).BackColor = TxtColor
            Next
        Close #fn
    End If
ErrExit:
    Close #fn
End Sub
Private Sub Form_Load()
    Dim i As Integer
    AppIsUp = False
    GetOrSetInfoCfg "GET"
    AppIsUp = True
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Me.Hide
        If FormIsVisible("FlightRotation") Then FlightRotation.MainButton(11).Value = 0
        Cancel = True
    End If
End Sub

Private Sub InfoBackColor_Click(Index As Integer)
    InfoBackColor(Index).BackColor = GetObjectColor(InfoBackColor(Index).BackColor)
    InfoText(Index).BackColor = InfoBackColor(Index).BackColor
    FlightRotation.RotFlag(Index).BackColor = InfoText(Index).BackColor
End Sub

Private Sub InfoText_Change(Index As Integer)
    If AppIsUp Then
        FlightRotation.RotFlag(Index).Caption = InfoText(Index).Text
        FlightRotation.RotFlag(Index).Tag = InfoText(Index).Text
        FlightRotation.PatchInfoText
    End If
End Sub

Private Function GetObjectColor(CurColor As Long) As Long
    Dim UseColor As Long
    On Error Resume Next
    UseColor = CurColor
    CommonDialog.Flags = cdlCCRGBInit
    CommonDialog.Color = UseColor
    CommonDialog.ShowColor
    UseColor = CommonDialog.Color
    GetObjectColor = UseColor
End Function

Private Sub InfoText_GotFocus(Index As Integer)
    CurTxtIdx = Index
End Sub

Private Sub InfoTextColor_DblClick(Index As Integer)
    InfoTextColor(Index).BackColor = GetObjectColor(InfoTextColor(Index).BackColor)
    InfoText(Index).ForeColor = InfoTextColor(Index).BackColor
    FlightRotation.RotFlag(Index).ForeColor = InfoText(Index).ForeColor
End Sub

Private Sub List1_Click()
    Dim tmpListText As String
    Dim tmpListItem As String
    Dim CodeList As String
    Dim KeyCode As String
    Dim TxtStrg As String
    Dim BgnStrg As String
    Dim EndStrg As String
    Dim ItmIdx As Integer
    Dim ListIdx As Integer
    Dim TextPos As Integer
    If CurTxtIdx >= 0 Then
        CodeList = "REGN,FLNA,FLND,ORIG,DEST,VIAA,VIAD"
        ListIdx = List1.ListIndex
        tmpListText = List1.List(ListIdx)
        tmpListItem = GetItem(tmpListText, 1, ":")
        If tmpListItem <> "" Then
            ItmIdx = val(tmpListItem)
            ItmIdx = ItmIdx
            KeyCode = GetItem(CodeList, ItmIdx, ",")
            KeyCode = "{" & KeyCode & "}"
            TxtStrg = InfoText(CurTxtIdx).Text
            TextPos = InfoText(CurTxtIdx).SelStart
            If TextPos > 0 Then BgnStrg = Left(TxtStrg, TextPos)
            TextPos = TextPos + 1
            EndStrg = Mid(TxtStrg, TextPos)
            TxtStrg = BgnStrg & KeyCode & EndStrg
            InfoText(CurTxtIdx).Text = TxtStrg
            InfoText(CurTxtIdx).SelStart = TextPos + Len(KeyCode) - 1
        End If
        FlightRotation.PatchInfoText
    End If
End Sub
