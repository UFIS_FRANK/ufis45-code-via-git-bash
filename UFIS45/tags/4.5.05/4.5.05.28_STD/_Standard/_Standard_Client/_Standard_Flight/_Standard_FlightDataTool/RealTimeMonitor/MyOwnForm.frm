VERSION 5.00
Begin VB.Form MyOwnForm 
   Caption         =   "Form1"
   ClientHeight    =   8310
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   8985
   Icon            =   "MyOwnForm.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   8310
   ScaleWidth      =   8985
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox StaffPanel 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   660
      Left            =   4680
      ScaleHeight     =   600
      ScaleWidth      =   3750
      TabIndex        =   1
      Top             =   450
      Visible         =   0   'False
      Width           =   3810
      Begin VB.CheckBox chkTask 
         Caption         =   "Start"
         Height          =   300
         Index           =   32
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   12
         Tag             =   "TRLG"
         Top             =   300
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.CheckBox chkTask 
         Caption         =   "Stop"
         Height          =   300
         Index           =   8
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   11
         Tag             =   "TRLR"
         Top             =   0
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.CheckBox chkTask 
         Height          =   600
         Index           =   40
         Left            =   3000
         Picture         =   "MyOwnForm.frx":030A
         Style           =   1  'Graphical
         TabIndex        =   10
         Tag             =   "ABS.FUNC:3"
         Top             =   0
         Visible         =   0   'False
         Width           =   600
      End
      Begin VB.TextBox txtVptoDD 
         Alignment       =   2  'Center
         Height          =   300
         Index           =   4
         Left            =   1560
         MaxLength       =   2
         TabIndex        =   9
         Top             =   315
         Visible         =   0   'False
         Width           =   345
      End
      Begin VB.TextBox txtVptoMM 
         Alignment       =   2  'Center
         Height          =   300
         Index           =   4
         Left            =   1200
         MaxLength       =   2
         TabIndex        =   8
         Top             =   315
         Visible         =   0   'False
         Width           =   345
      End
      Begin VB.TextBox txtVptoYY 
         Alignment       =   2  'Center
         Height          =   300
         Index           =   4
         Left            =   630
         MaxLength       =   4
         TabIndex        =   7
         Top             =   315
         Visible         =   0   'False
         Width           =   555
      End
      Begin VB.TextBox txtVpfrDD 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         Height          =   300
         Index           =   4
         Left            =   1560
         MaxLength       =   2
         TabIndex        =   6
         Top             =   0
         Visible         =   0   'False
         Width           =   345
      End
      Begin VB.TextBox txtVpfrMM 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         Height          =   300
         Index           =   4
         Left            =   1200
         MaxLength       =   2
         TabIndex        =   5
         Top             =   0
         Visible         =   0   'False
         Width           =   345
      End
      Begin VB.TextBox txtVpfrYY 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         Height          =   300
         Index           =   4
         Left            =   630
         MaxLength       =   4
         TabIndex        =   4
         Top             =   0
         Visible         =   0   'False
         Width           =   555
      End
      Begin VB.CheckBox chkTask 
         Caption         =   "Load Staff"
         Height          =   300
         Index           =   11
         Left            =   1920
         Style           =   1  'Graphical
         TabIndex        =   3
         Tag             =   "ABS.STFTAB"
         Top             =   300
         Visible         =   0   'False
         Width           =   1065
      End
      Begin VB.CheckBox chkTask 
         Caption         =   "Load Abs."
         Height          =   300
         Index           =   6
         Left            =   1920
         Style           =   1  'Graphical
         TabIndex        =   2
         Tag             =   "ABS.HRVIEW"
         Top             =   0
         Visible         =   0   'False
         Width           =   1065
      End
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Left            =   30
      MultiLine       =   -1  'True
      TabIndex        =   0
      Text            =   "MyOwnForm.frx":0BD4
      Top             =   60
      Width           =   8475
   End
   Begin VB.Image MyLogos 
      Height          =   510
      Index           =   1
      Left            =   7110
      Picture         =   "MyOwnForm.frx":0C49
      Top             =   2520
      Width           =   1200
   End
   Begin VB.Image MyLogos 
      Height          =   510
      Index           =   0
      Left            =   7110
      Picture         =   "MyOwnForm.frx":2C6B
      Top             =   1920
      Width           =   1200
   End
   Begin VB.Image MyPictures 
      Height          =   3450
      Index           =   2
      Left            =   510
      Picture         =   "MyOwnForm.frx":3B4D
      Top             =   4650
      Width           =   6105
   End
   Begin VB.Image MyPictures 
      Height          =   3450
      Index           =   1
      Left            =   510
      Picture         =   "MyOwnForm.frx":4873F
      Top             =   1170
      Width           =   6105
   End
   Begin VB.Image MyPictures 
      Height          =   480
      Index           =   0
      Left            =   0
      Picture         =   "MyOwnForm.frx":8D331
      Top             =   1200
      Width           =   480
   End
   Begin VB.Image MyIcons 
      Height          =   480
      Index           =   6
      Left            =   3360
      Picture         =   "MyOwnForm.frx":8DF73
      Top             =   600
      Width           =   480
   End
   Begin VB.Image MyIcons 
      Height          =   480
      Index           =   5
      Left            =   2820
      Picture         =   "MyOwnForm.frx":8E27D
      Top             =   600
      Width           =   480
   End
   Begin VB.Image MyIcons 
      Height          =   480
      Index           =   4
      Left            =   2280
      Picture         =   "MyOwnForm.frx":8E587
      Top             =   600
      Width           =   480
   End
   Begin VB.Image MyIcons 
      Height          =   480
      Index           =   3
      Left            =   1740
      Picture         =   "MyOwnForm.frx":8E891
      Top             =   600
      Width           =   480
   End
   Begin VB.Image MyIcons 
      Height          =   480
      Index           =   2
      Left            =   1200
      Picture         =   "MyOwnForm.frx":8EB9B
      Top             =   600
      Width           =   480
   End
   Begin VB.Image MyIcons 
      Height          =   480
      Index           =   1
      Left            =   660
      Picture         =   "MyOwnForm.frx":8EEA5
      Top             =   600
      Width           =   480
   End
   Begin VB.Image MyIcons 
      Height          =   480
      Index           =   0
      Left            =   120
      Picture         =   "MyOwnForm.frx":8F1AF
      Top             =   600
      Width           =   480
   End
End
Attribute VB_Name = "MyOwnForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
