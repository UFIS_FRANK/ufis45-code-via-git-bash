VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{0E59F1D2-1FBE-11D0-8FF2-00A0D10038BC}#1.0#0"; "MSSCRIPT.OCX"
Begin VB.Form MyMaskCfg 
   Caption         =   "Dialog Configuration"
   ClientHeight    =   9045
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10260
   Icon            =   "MyMaskCfg.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   9045
   ScaleWidth      =   10260
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "Reset"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Index           =   2
      Left            =   2340
      TabIndex        =   6
      Top             =   30
      Width           =   1125
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Load"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Index           =   1
      Left            =   1200
      TabIndex        =   5
      Top             =   30
      Width           =   1125
   End
   Begin VB.TextBox DbsSqlKey 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   120
      TabIndex        =   4
      Text            =   "Text1"
      Top             =   2970
      Visible         =   0   'False
      Width           =   10005
   End
   Begin MSComctlLib.StatusBar MyStatusBar 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   3
      Top             =   8730
      Width           =   10260
      _ExtentX        =   18098
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   15028
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin TABLib.TAB CfgTab 
      Height          =   5055
      Index           =   0
      Left            =   60
      TabIndex        =   0
      Top             =   3450
      Width           =   10125
      _Version        =   65536
      _ExtentX        =   17859
      _ExtentY        =   8916
      _StockProps     =   0
   End
   Begin MSScriptControlCtl.ScriptControl MyScrCtrl 
      Left            =   90
      Top             =   450
      _ExtentX        =   1005
      _ExtentY        =   1005
      AllowUI         =   -1  'True
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Refresh"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Index           =   0
      Left            =   60
      TabIndex        =   1
      Top             =   30
      Width           =   1125
   End
   Begin MSComctlLib.TabStrip DbsTblStrip 
      Height          =   2955
      Left            =   60
      TabIndex        =   2
      Top             =   420
      Width           =   10185
      _ExtentX        =   17965
      _ExtentY        =   5212
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   1
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "MyMaskCfg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim DlgForm As Form
Dim CurDataTab As Integer
Dim LinCol As Long
Dim IdcCol As Long
Dim UpcCol As Long
Dim FmtCol As Long
Dim TypCol As Long
Dim BckCol As Long
Dim ForCol As Long
Dim RngCol As Long
Dim DefCol As Long
Dim StsCol As Long
Dim FldCol As Long
Dim TblCol As Long
Dim IdxCol As Long
Dim DbsCol As Long
Dim NewCol As Long
Dim TmpCol As Long
Dim InpCol As Long
Dim RegCol As Long
Dim VisCol As Long
Dim DatCol As Long

Private Sub CfgTab_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim CurCtrl As Control
    Dim ContCtrl As Control
    Dim ObjTag As String
    Dim CtrlFound As Boolean
    Dim LineInfo As String
    Dim CurItm As Integer
    For CurItm = 1 To DlgForm.ShowObj.Count - 1
        DlgForm.ShowObj(CurItm).Visible = False
    Next
    If Index = 0 Then LineInfo = Trim(Str(LineNo)) Else LineInfo = CfgTab(Index).GetColumnValue(4, ColNo)
    CurItm = 0
    ObjTag = "START"
    While ObjTag <> "##"
        CurItm = CurItm + 1
        If CurItm >= DlgForm.ShowObj.Count Then Load DlgForm.ShowObj(CurItm)
        ObjTag = "##" & GetItem(LineInfo, CurItm, ".")
        CtrlFound = IdentifyObject(DlgForm, ObjTag, CurCtrl)
        If CtrlFound Then
            SetObjContainer DlgForm.ShowObj(CurItm), CurCtrl
            On Error GoTo ErrHdl
            DlgForm.ShowObj(CurItm).Top = CurCtrl.Top
            DlgForm.ShowObj(CurItm).Left = CurCtrl.Left
            DlgForm.ShowObj(CurItm).Height = CurCtrl.Height
            DlgForm.ShowObj(CurItm).Width = CurCtrl.Width
            DlgForm.ShowObj(CurItm).Tag = ObjTag
            DlgForm.ShowObj(CurItm).Value = 0
            DlgForm.ShowObj(CurItm).Visible = True
            DlgForm.ShowObj(CurItm).ZOrder
        End If
    Wend
ErrHdl:
End Sub
Private Sub SetObjContainer(MyShowCtrl As Control, MyIdcCtrl As Control)
    On Error Resume Next
    Set MyShowCtrl.Container = MyIdcCtrl.Container
End Sub
Private Sub Command1_Click(Index As Integer)
    Select Case Index
        Case 0
            RefreshCfgTabs
        Case 1
            ReadRecordData DlgForm
        Case 2
            'MyScrCtrl.Reset
            'RestoreTagValues DlgForm
            'SetMaskConfig DlgForm
Case Else
    End Select
End Sub

Private Sub DbsTblStrip_Click()
    Dim CurTab As Integer
    CurTab = DbsTblStrip.SelectedItem.Index
    CfgTab(CurTab).Width = ScaleWidth - 2 * CfgTab(CurTab).Left
    CfgTab(CurTab).Visible = True
    CfgTab(CurTab).ZOrder
    DbsSqlKey(CurTab).Width = ScaleWidth - 2 * DbsSqlKey(CurTab).Left
    DbsSqlKey(CurTab).Visible = True
    DbsSqlKey(CurTab).ZOrder
    If (CurDataTab > 0) And (CurTab <> CurDataTab) Then
        CfgTab(CurDataTab).Visible = False
        DbsSqlKey(CurDataTab).Visible = False
    End If
    CurDataTab = CurTab
End Sub

Private Sub Form_Activate()
    RefreshCfgTabs
End Sub

Private Sub Form_Load()
    CurDataTab = -1
    LinCol = 0  'Line NBR
    IdcCol = 1  'IDC_CODE
    TypCol = 2  'SetTypeTo...
    UpcCol = 3  'SetToUpperCase Y/N
    FmtCol = 4  'SetFormat
    RegCol = 5  'Validation Format (like Regular Expression)
    RngCol = 6  'Range or Size etc.
    StsCol = 7  'Status, Disabled etc
    DefCol = 8  'Default Caption or Text Value
    ForCol = 9  'SetTextColor
    BckCol = 10 'SetBackColor
    TblCol = 11 'DBS Table Index
    IdxCol = 12 'DBS Field Index
    FldCol = 13 'DBS Field Name
    DbsCol = 14 'DBS Original Data
    InpCol = 15 'New Data Input (User Format)
    NewCol = 16 'New Data for Update/Insert (Ceda Format)
    TmpCol = 17 'Temporary Data Storage
    DatCol = 18 'Data Value (i.e. for CheckBoxes etc.)
    VisCol = 19 'Visible Y/N
    InitCfgTabs
End Sub
Private Sub InitCfgTabs()
    CfgTab(0).ResetContent
    CfgTab(0).HeaderString = "NBR,IDC,Type,U UpperCase,Format,Reg Expr,Control Flow,Status,Default Caption or Text,ForeColor,BackColor,FldIdx,TAB.FLD,DBS Data,INP Data,NEW Data,TMP Data,DEF Data,Visible"
    CfgTab(0).HeaderLengthString = "200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200,200"
    CfgTab(0).SetFieldSeparator Chr(15)
    CfgTab(0).ShowHorzScroller True
    CfgTab(0).ResetContent
End Sub
Public Sub SetMaskConfig(MyForm As Form)
    CfgTab(0).ResetContent
    Set DlgForm = MyForm
    PrepareMaskConfig MyForm
    StoreMaskConfig 'Must be available in a global module (Like ApplConfig.bas)
    ReadApplCfgFile
    FinishMaskConfig MyForm
    ReadApplChgFile
    SetControlFonts MyForm, "", -1, False
    ReadApplCusFile
End Sub
Private Sub RefreshCfgTabs()
    Dim i As Integer
    For i = 0 To CfgTab.Count - 1
        CfgTab(i).AutoSizeColumns
        CfgTab(i).RedrawTab
    Next
End Sub
Public Sub PrepareMaskConfig(MyForm As Form)
    Dim CurCtrl As Control
    Dim LookupCode As String
    Dim LineNo As Long
    LineNo = InitIdcLine("#EmptyLine#")
    LineNo = InitIdcLine(MyForm.Name + ".Form")
    MyForm.Tag = "##" + Trim(Str(LineNo))
    For Each CurCtrl In MyForm.Controls
        If CurCtrl.Tag <> "" Then LookupCode = CurCtrl.Tag Else LookupCode = CurCtrl.Name
        InitCurCtrl CurCtrl, LookupCode
    Next
End Sub
Private Sub InitCurCtrl(CurCtrl As Control, LookupCode As String)
    Dim LineNo As Long
    LineNo = InitIdcLine(LookupCode)
    CurCtrl.Tag = "##" + Trim(Str(LineNo))
    Select Case TypeName(CurCtrl)
        Case "TextBox"
            SetInitText CurCtrl.Tag, CurCtrl.Text
            SetToUpper CurCtrl.Tag, True
        Case "Label", "Frame", "CheckBox", "OptionButton"
            SetInitText CurCtrl.Tag, CurCtrl.Caption
        Case "ComboBox"
            SetInitText CurCtrl.Tag, CurCtrl.Text
        Case "StatusBar"
        Case "TAB"
        Case Else
    End Select
    MyScrCtrl.AddObject GetIdcCode(CurCtrl.Tag), CurCtrl
End Sub
Public Sub FinishMaskConfig(MyForm As Form)
    Dim CurCtrl As Control
    Dim LookupCode As String
    Dim Value1 As Long
    Dim Value2 As Long
    Dim Strg1 As String
    Dim TmpBool As Boolean
    On Error Resume Next
    For Each CurCtrl In MyForm.Controls
        Select Case TypeName(CurCtrl)
            Case "TextBox"
                CurCtrl.Text = GetInitText(CurCtrl.Tag)
                CurCtrl.Locked = GetReadOnly(CurCtrl.Tag)
            Case "Label", "Frame", "CheckBox"
                CurCtrl.Caption = GetInitText(CurCtrl.Tag)
            Case "OptionButton"
                CurCtrl.Caption = GetInitText(CurCtrl.Tag)
                If CurCtrl.Value = True Then
                    MyMaskCfg.SetTmpData CurCtrl.Container.Tag, Trim(Str(CurCtrl.Index))
                End If
            Case "ComboBox"
                CurCtrl.Text = GetInitText(CurCtrl.Tag)
            Case "StatusBar"
            Case "TAB"
            Case Else
                'MsgBox TypeName(CurCtrl)
        End Select
        TmpBool = CheckVisibleStatus(CurCtrl.Tag, Strg1)
        If Strg1 <> "" Then CurCtrl.Visible = TmpBool
        Value1 = CurCtrl.ForeColor
        Value2 = CurCtrl.BackColor
        GetObjColors CurCtrl.Tag, Value1, Value2
        CurCtrl.ForeColor = Value1
        CurCtrl.BackColor = Value2
    Next
End Sub
Public Function InitIdcLine(IdcLookup As String) As Long
    Dim LineNbrs As String
    Dim LookupCode As String
    Dim TabLine As String
    Dim LineNo As Long
    LookupCode = IdcLookup
    LineNo = CfgTab(0).GetLineCount
    LineNbrs = CfgTab(0).GetLinesByColumnValue(IdcCol, LookupCode, 0)
    If LineNbrs <> "" Then LookupCode = LookupCode + "_NU" + Trim(Str(LineNo))
    TabLine = Trim(Str(LineNo)) + Chr(15) + LookupCode
    CfgTab(0).InsertTextLine TabLine, False
    InitIdcLine = LineNo
End Function
'--------------------------------
'List of available Set Routines
'--------------------------------
Public Sub SetTextColor(IdcCode As String, ColorCode As String)
    UpdateCfgTabs IdcCode, ColorCode, ForCol
End Sub
Public Sub SetForeColor(IdcCode As String, ColorCode As String)
    UpdateCfgTabs IdcCode, ColorCode, ForCol
End Sub
Public Sub SetBKColor(IdcCode As String, ColorCode As String)
    UpdateCfgTabs IdcCode, ColorCode, BckCol
End Sub
Public Sub SetFormat(IdcCode As String, UsedFormat As String)
    UpdateCfgTabs IdcCode, UsedFormat, FmtCol
End Sub
Public Sub SetTypeToInt(IdcCode As String, FromValue As Integer, ToValue As Integer)
    Dim tmpValue As String
    UpdateCfgTabs IdcCode, "INTG", TypCol
    tmpValue = Trim(Str(FromValue)) + "," + Trim(Str(ToValue))
    UpdateCfgTabs IdcCode, tmpValue, RngCol
End Sub
Public Sub SetTypeToLong(IdcCode As String, FromValue As Long, ToValue As Long)
    Dim tmpValue As String
    UpdateCfgTabs IdcCode, "LONG", TypCol
    tmpValue = Trim(Str(FromValue)) + "," + Trim(Str(ToValue))
    UpdateCfgTabs IdcCode, tmpValue, RngCol
End Sub
Public Sub SetTypeToDouble(IdcCode As String, PrePoint As Integer, AfterPoint As Integer, FromValue As String, ToValue As String)
    Dim tmpValue As String
    UpdateCfgTabs IdcCode, "DOUB", TypCol
    tmpValue = FromValue + "," + ToValue
    UpdateCfgTabs IdcCode, tmpValue, RngCol
    tmpValue = Trim(Str(PrePoint)) + "," + Trim(Str(AfterPoint))
    UpdateCfgTabs IdcCode, tmpValue, FmtCol
End Sub
Public Sub SetTypeToMoney(IdcCode As String, PrePoint As Integer, AfterPoint As Integer, FromValue As String, ToValue As String)
    Dim tmpValue As String
    UpdateCfgTabs IdcCode, "CURR", TypCol
    tmpValue = FromValue + "," + ToValue
    UpdateCfgTabs IdcCode, tmpValue, RngCol
    tmpValue = Trim(Str(PrePoint)) + "," + Trim(Str(AfterPoint))
    UpdateCfgTabs IdcCode, tmpValue, FmtCol
End Sub
Public Sub SetTypeToTime(IdcCode As String, Required As Boolean, ChangeDay As Boolean)
    UpdateCfgTabs IdcCode, "TIME", TypCol
    If Required Then UpdateCfgTabs IdcCode, "MA", StsCol
End Sub
Public Sub SetTypeToDate(IdcCode As String, Required As Boolean)
    UpdateCfgTabs IdcCode, "DATE", TypCol
    If Required Then UpdateCfgTabs IdcCode, "MA", StsCol
End Sub
Public Sub SetTypeToString(IdcCode As String, Format As String, MinLength As Long, MaxLength As Long)
    Dim tmpValue As String
    UpdateCfgTabs IdcCode, "CHAR", TypCol
    tmpValue = Trim(Str(MinLength)) + "," + Trim(Str(MaxLength)) + ","
    UpdateCfgTabs IdcCode, tmpValue, RngCol
    UpdateCfgTabs IdcCode, Format, FmtCol
End Sub
Public Sub SetTextLimit(IdcCode As String, MinLength As Long, MaxLength As Long, EmptyIsValid As Boolean)
    Dim tmpValue As String
    tmpValue = Trim(Str(MinLength)) + "," + Trim(Str(MaxLength)) + ","
    UpdateCfgTabs IdcCode, tmpValue, RngCol
End Sub
Public Sub SetInitText(IdcCode As String, DefText As String)
    UpdateCfgTabs IdcCode, DefText, DefCol
End Sub
Public Sub SetReadOnly(IdcCode As String, SetValue As Boolean)
    Dim tmpValue As String
    If SetValue = True Then tmpValue = "RO" Else tmpValue = ""
    UpdateCfgTabs IdcCode, tmpValue, StsCol
End Sub
Public Sub SetPrecision(IdcCode As String, PrePoint As Integer, AfterPoint As Integer)
    Dim tmpValue As String
    tmpValue = Trim(Str(PrePoint)) + "," + Trim(Str(AfterPoint))
    UpdateCfgTabs IdcCode, tmpValue, FmtCol
End Sub
Public Sub SetToUpper(IdcCode As String, SetValue As Boolean)
    If SetValue Then UpdateCfgTabs IdcCode, "Y", UpcCol Else UpdateCfgTabs IdcCode, "N", UpcCol
End Sub
Public Sub SetRange(IdcCode As String, FromValue As Long, ToValue As Long)
    Dim tmpValue As String
    tmpValue = Trim(Str(FromValue)) + "," + Trim(Str(ToValue)) + ","
    UpdateCfgTabs IdcCode, tmpValue, RngCol
End Sub
Public Sub SetVisibleStatus(IdcCode As String, SetValue As Boolean)
    If SetValue Then UpdateCfgTabs IdcCode, "Y", VisCol Else UpdateCfgTabs IdcCode, "N", VisCol
End Sub
Public Sub SetDbField(IdcCode As String, TanaFina As String)
    UpdateCfgTabs IdcCode, TanaFina, FldCol
End Sub
Public Sub SetTmpData(IdcCode As String, tmpData As String)
    UpdateCfgTabs IdcCode, tmpData, TmpCol
End Sub
Public Sub SetExitButton(IdcCode As String, AskBack As Boolean)
    UpdateCfgTabs IdcCode, "EXIT", TypCol
    If AskBack Then UpdateCfgTabs IdcCode, "ASK", RngCol
End Sub
Public Sub SetSaveButton(IdcCode As String, AskBack As Boolean)
    UpdateCfgTabs IdcCode, "SAVE", TypCol
    If AskBack Then UpdateCfgTabs IdcCode, "ASK", RngCol
End Sub
Public Sub SetOpenButton(IdcCode As String, AskBack As Boolean)
    UpdateCfgTabs IdcCode, "OPEN", TypCol
End Sub
Public Sub SetCheckMaster(IdcCode As String, Members As String)
    Dim MasterLineNo As Long
    Dim MemberLineNo As Long
    Dim GrpMembers As String
    Dim GrpIdcName As String
    Dim GrpMemberNo As Integer
    MasterLineNo = GetIdcLine(IdcCode)
    If MasterLineNo > 0 Then
        CfgTab(0).SetColumnValue MasterLineNo, TypCol, "GRPM"
        GrpMembers = ""
        GrpMemberNo = 0
        GrpIdcName = "START"
        While GrpIdcName <> ""
            GrpMemberNo = GrpMemberNo + 1
            GrpIdcName = GetItem(Members, GrpMemberNo, ",")
            If GrpIdcName <> "" Then
                MemberLineNo = GetIdcLine(GrpIdcName)
                If MemberLineNo > 0 Then
                    GrpMembers = GrpMembers + Trim(Str(MemberLineNo)) + ","
                    CfgTab(0).SetColumnValue MemberLineNo, TypCol, ("GRM" + Trim(Str(GrpMemberNo)))
                    CfgTab(0).SetColumnValue MemberLineNo, RngCol, Trim(Str(MasterLineNo))
                End If
            End If
        Wend
        If GrpMembers <> "" Then GrpMembers = Left(GrpMembers, Len(GrpMembers) - 1)
        CfgTab(0).SetColumnValue MasterLineNo, RngCol, GrpMembers
    End If
End Sub
'--------------------------------
'List of available Get Functions
'--------------------------------
Public Function GetTextColor(IdcCode As String, DefaultColor As Long) As Long
    GetTextColor = TranslateColorCode(CfgTab(0).GetColumnValue(GetIdcLine(IdcCode), ForCol), DefaultColor)
End Function
Public Function GetForeColor(IdcCode As String, DefaultColor As Long) As Long
    GetForeColor = TranslateColorCode(CfgTab(0).GetColumnValue(GetIdcLine(IdcCode), ForCol), DefaultColor)
End Function
Public Function GetBackColor(IdcCode As String, DefaultColor As Long) As Long
    GetBackColor = TranslateColorCode(CfgTab(0).GetColumnValue(GetIdcLine(IdcCode), BckCol), DefaultColor)
End Function
Public Function GetObjColors(IdcCode As String, ForeColor As Long, BackColor As Long) As Long
    ForeColor = GetForeColor(IdcCode, ForeColor)
    BackColor = GetBackColor(IdcCode, BackColor)
    GetObjColors = RC_SUCCESS
End Function
Public Function GetObjType(IdcCode As String) As String
    GetObjType = CfgTab(0).GetColumnValue(GetIdcLine(IdcCode), TypCol)
End Function
Public Function GetInitText(IdcCode As String) As String
    GetInitText = CfgTab(0).GetColumnValue(GetIdcLine(IdcCode), DefCol)
End Function
Public Function GetReadOnly(IdcCode As String) As Boolean
    GetReadOnly = (CfgTab(0).GetColumnValue(GetIdcLine(IdcCode), StsCol) = "RO")
End Function
Public Function GetObjStatus(IdcCode As String) As String
    GetObjStatus = CfgTab(0).GetColumnValue(GetIdcLine(IdcCode), StsCol)
End Function
Public Function GetRange(IdcCode As String) As String
    GetRange = CfgTab(0).GetColumnValue(GetIdcLine(IdcCode), RngCol)
End Function
Public Function GetToUpper(IdcCode As String) As Boolean
    GetToUpper = (CfgTab(0).GetColumnValue(GetIdcLine(IdcCode), UpcCol) = "Y")
End Function
Public Function GetVisibleStatus(IdcCode As String) As Boolean
    GetVisibleStatus = (CfgTab(0).GetColumnValue(GetIdcLine(IdcCode), VisCol) <> "N")
End Function
Public Function GetDbField(IdcCode As String) As String
    GetDbField = CfgTab(0).GetColumnValue(GetIdcLine(IdcCode), FldCol)
End Function
Public Function GetTmpData(IdcCode As String) As String
    GetTmpData = CfgTab(0).GetColumnValue(GetIdcLine(IdcCode), TmpCol)
End Function
Public Function GetIdcCode(IdcCode As String) As String
    GetIdcCode = CfgTab(0).GetColumnValue(GetIdcLine(IdcCode), IdcCol)
End Function
'---------------------------------
'List of available Check Functions
'---------------------------------
Public Function CheckGrpMaster(IdcCode As String) As Boolean
    If CfgTab(0).GetColumnValue(GetIdcLine(IdcCode), TypCol) = "GRPM" Then CheckGrpMaster = True Else CheckGrpMaster = False
End Function
Public Function CheckGrpMember(IdcCode As String) As Boolean
    If Left(CfgTab(0).GetColumnValue(GetIdcLine(IdcCode), TypCol), 3) = "GRM" Then CheckGrpMember = True Else CheckGrpMember = False
End Function
Public Function CheckVisibleStatus(IdcCode As String, CfgEntry As String) As Boolean
    CfgEntry = CfgTab(0).GetColumnValue(GetIdcLine(IdcCode), VisCol)
    CheckVisibleStatus = (CfgEntry = "Y")
End Function
Public Function CheckTextLimit(IdcCode As String, myText As String)
    Dim MaxLength As Long
    MaxLength = Val(GetItem(CfgTab(0).GetColumnValue(GetIdcLine(IdcCode), RngCol), 2, ","))
    If (MaxLength < 1) Or (MaxLength > Len(myText)) Then CheckTextLimit = True Else CheckTextLimit = False
End Function
'----------------------------------
'List of available Handle Functions
'----------------------------------
Public Function HandleButtonAction(MyForm As Form, IdcCode As String, ChkValue As Integer) As Boolean
    Dim CurCtrl As Control
    Dim CtrlFound As Boolean
    Dim ActionDone As Boolean
    Dim ObjType As String
    Dim RngInfo As String
    ActionDone = HandleChkGroup(MyForm, IdcCode, ChkValue)  'Control used as graphical CheckBox ?
    If Not ActionDone Then
        'Control is used as Button
        CtrlFound = IdentifyObject(MyForm, IdcCode, CurCtrl)
        If CtrlFound Then
            ObjType = GetObjType(IdcCode)
            If ChkValue = 1 Then
                CurCtrl.BackColor = LightGreen
                Select Case ObjType
                    Case "EXIT"
                        If GetRange(IdcCode) = "ASK" Then ShutDownApplication True Else ShutDownApplication False
                        ActionDone = True
                        CurCtrl.Value = 0
                    Case "SAVE"
                        MsgBox "Here we'll save the changes and exit ..."
                        If GetRange(IdcCode) = "ASK" Then ShutDownApplication True Else ShutDownApplication False
                        ActionDone = True
                        CurCtrl.Value = 0
                    Case "OPEN"
                        MsgBox "Here we must open something ..."
                        ActionDone = True
                    Case Else
                        ActionDone = False
                End Select
            Else
                CurCtrl.BackColor = vbButtonFace
            End If
        End If
    End If
    HandleButtonAction = ActionDone
End Function
Public Function HandleChkGroup(MyForm As Form, IdcCode As String, ChkValue As Integer) As Boolean
    Dim CurChkBox As Control
    Dim ChkGrpMaster As Boolean
    Dim CtrlFound As Boolean
    Dim RangeInfo As String
    Dim LineNbr As String
    Dim Curline As Integer
    StopNestedCalls = True
    CtrlFound = IdentifyObject(MyForm, IdcCode, CurChkBox)
    If CtrlFound Then
        If CheckGrpMaster(IdcCode) = True Then
            CurChkBox.Value = ChkValue
            If CurChkBox.Name = "MyVbButton" Then
                If CurChkBox.Value = 1 Then CurChkBox.BackColor = LightGreen Else CurChkBox.BackColor = vbButtonFace
            End If
            RangeInfo = CfgTab(0).GetColumnValue(GetIdcLine(IdcCode), RngCol)
            Curline = 0
            LineNbr = "START"
            While LineNbr <> ""
                Curline = Curline + 1
                LineNbr = GetItem(RangeInfo, Curline, ",")
                If LineNbr <> "" Then
                    LineNbr = "##" + LineNbr
                    CtrlFound = IdentifyObject(MyForm, LineNbr, CurChkBox)
                    If CtrlFound Then
                        If ChkValue = 1 Then
                            'Set Member to Checked
                            CurChkBox.Value = 1
                        Else
                            'Restore Value of Members
                            CurChkBox.Value = Val(GetTmpData(LineNbr))
                        End If
                        If CurChkBox.Name = "MyVbButton" Then
                            If CurChkBox.Value = 1 Then CurChkBox.BackColor = LightGreen Else CurChkBox.BackColor = vbButtonFace
                        End If
                    End If
                End If
            Wend
            HandleChkGroup = True
        ElseIf CheckGrpMember(IdcCode) = True Then
            UpdateCfgTabs IdcCode, Str(CurChkBox.Value), TmpCol
            If CurChkBox.Name = "MyVbButton" Then
                If CurChkBox.Value = 1 Then CurChkBox.BackColor = LightGreen Else CurChkBox.BackColor = vbButtonFace
            End If
            'Set Master to Unchecked
            RangeInfo = "##" + CfgTab(0).GetColumnValue(GetIdcLine(IdcCode), RngCol)
            CtrlFound = IdentifyObject(MyForm, RangeInfo, CurChkBox)
            If CtrlFound Then
                CurChkBox.Value = 0
                If CurChkBox.Name = "MyVbButton" Then
                    If CurChkBox.Value = 1 Then CurChkBox.BackColor = LightGreen Else CurChkBox.BackColor = vbButtonFace
                End If
            End If
            HandleChkGroup = True
        Else
            HandleChkGroup = False
        End If
    Else
        HandleChkGroup = False
    End If
    StopNestedCalls = False
End Function
'--------------------------------
'List of available Add Functions
'--------------------------------
Public Sub AddMaskObject(MyVbType As String, IdcCode As String, MyTop As Long, MyLeft As Long, MyHeight As Long, MyWidth As Long)
    Dim CurCtrl As Control
    Dim ObjIdx As Integer
    Select Case MyVbType
        Case "MyVbText"
            ObjIdx = MyMainForm.MyVbText.Count
            Load MyMainForm.MyVbText(ObjIdx)
            Set CurCtrl = MyMainForm.MyVbText(ObjIdx)
        Case "MyVbButton"
            ObjIdx = MyMainForm.MyVbButton.Count
            Load MyMainForm.MyVbButton(ObjIdx)
            Set CurCtrl = MyMainForm.MyVbButton(ObjIdx)
        Case Else
    End Select
    CurCtrl.Visible = True
    CurCtrl.Top = MyTop * Screen.TwipsPerPixelY
    CurCtrl.Left = MyLeft * Screen.TwipsPerPixelX
    CurCtrl.Height = MyHeight * Screen.TwipsPerPixelY
    CurCtrl.Width = MyWidth * Screen.TwipsPerPixelX
    CurCtrl.ZOrder
    InitCurCtrl CurCtrl, IdcCode
End Sub
Private Function GetIdcLine(IdcCode As String) As Long
    If Left(IdcCode, 2) = "##" Then GetIdcLine = Val(Mid(IdcCode, 3)) Else GetIdcLine = Val(CfgTab(0).GetLinesByColumnValue(IdcCol, IdcCode, 0))
End Function
Private Sub UpdateCfgTabs(IdcCode As String, IdcValue As String, ColIdx As Long)
    Dim LineNo As Long
    LineNo = GetIdcLine(IdcCode)
    If LineNo > 0 Then CfgTab(0).SetColumnValue LineNo, ColIdx, IdcValue
End Sub
Public Function IdentifyObject(MyForm As Form, TagValue As String, UseCtrl As Control) As Boolean
    Dim CurCtrl As Control
    IdentifyObject = False
    For Each CurCtrl In MyForm.Controls
        If CurCtrl.Tag <> "" Then
            If CurCtrl.Tag = TagValue Then
                Set UseCtrl = CurCtrl
                IdentifyObject = True
                Exit For
            End If
        End If
    Next
End Function
Public Function TranslateColorCode(ColorCode As String, DefaultColor As Long) As Long
    Dim ColVal As Long
    Select Case ColorCode
        Case "SILVER", "LIGHTGRAY"
            ColVal = LightGray
        Case "BLACK"
            ColVal = vbBlack
        Case "YELLOW", "NORMALYELLOW"
            ColVal = NormalYellow
        Case Else
            ColVal = DefaultColor
    End Select
    TranslateColorCode = ColVal
End Function
Public Sub IdentifyDbAccess(MyForm As Form)
    Dim Curline As Long
    Dim MaxLine As Long
    Dim UseTab As Integer
    Dim UseCol As Long
    Dim DbsCfg As String
    Dim CurTbl As String
    Dim CurFld As String
    Dim FldInfo As String
    MaxLine = CfgTab(0).GetLineCount - 1
    For Curline = 0 To MaxLine
        DbsCfg = CfgTab(0).GetColumnValue(Curline, FldCol)
        If DbsCfg <> "" Then
            CurTbl = GetItem(DbsCfg, 1, ".")
            CurFld = GetItem(DbsCfg, 2, ".")
            UseTab = IdentifyTableTab(CurTbl)
            UseCol = IdentifyField(CurTbl, CurFld)
            CfgTab(0).SetColumnValue Curline, TblCol, Trim(Str(UseTab))
            CfgTab(0).SetColumnValue Curline, IdxCol, Trim(Str(UseCol))
            FldInfo = CfgTab(UseTab).GetColumnValue(4, UseCol)
            If FldInfo <> "" Then FldInfo = FldInfo + "."
            FldInfo = FldInfo + Trim(Str(Curline))
            CfgTab(UseTab).SetColumnValue 4, UseCol, FldInfo
        End If
    Next
    If CfgTab.Count > 1 Then
        CurDataTab = 1
        CfgTab(CurDataTab).Visible = True
        CfgTab(CurDataTab).ZOrder
        DbsSqlKey(CurDataTab).Visible = True
        DbsSqlKey(CurDataTab).ZOrder
    End If
    ReadRecordData MyForm
End Sub
Private Function IdentifyTableTab(TableName As String) As Integer
    Dim CurTab As Integer
    Dim MaxTab As Integer
    Dim UseTab As Integer
    Dim SystabData As String
    UseTab = -1
    MaxTab = CfgTab.Count - 1
    For CurTab = 1 To MaxTab
        If CfgTab(CurTab).Tag = TableName Then
            UseTab = CurTab
            Exit For
        End If
    Next
    If UseTab < 0 Then
        UseTab = CfgTab.Count
        If DbsTblStrip.Tabs.Count < UseTab Then DbsTblStrip.Tabs.Add UseTab
        'If DbsTblStrip.Tabs.Count < UseTab + 1 Then DbsTblStrip.Tabs.Add UseTab + 1
        DbsTblStrip.Tabs(UseTab).Caption = TableName
        Load CfgTab(UseTab)
        Load DbsSqlKey(UseTab)
        DbsSqlKey(UseTab).Visible = False
        DbsSqlKey(UseTab).Text = ExtractWhereClose(TableName, Command)
        CfgTab(UseTab).Top = DbsTblStrip.ClientTop + 60
        CfgTab(UseTab).Left = DbsTblStrip.Left + 60
        CfgTab(UseTab).Height = DbsTblStrip.ClientHeight - DbsSqlKey(0).Height - 180
        CfgTab(UseTab).Tag = TableName
        CfgTab(UseTab).Visible = False
        SystabData = UfisServer.GetSystabData(TableName, True)
        CfgTab(UseTab).HeaderString = GetItem(SystabData, 1, vbLf)
        CfgTab(UseTab).HeaderLengthString = GetItem(SystabData, 2, vbLf)
        CfgTab(UseTab).ShowHorzScroller True
        CfgTab(UseTab).ResetContent
        CfgTab(UseTab).InsertBuffer SystabData, vbLf
        CfgTab(UseTab).InsertTextLine "", False
    End If
    IdentifyTableTab = UseTab
End Function
Private Function ExtractWhereClose(TableCode As String, CmdLine As String)
    Dim Result As String
    Dim LookUp As String
    Dim tmpText As String
    Result = "No SqlKey"
    LookUp = TableCode + "."
    tmpText = GetItem(CmdLine, 2, LookUp)
    If tmpText <> "" Then
        Result = GetItem(tmpText, 1, " ")
    End If
    ExtractWhereClose = Result
End Function
Private Function IdentifyField(TableCode As String, FieldName As String) As Long
    Dim UseTab As Integer
    Dim FldItm As Integer
    Dim TabLine As String
    UseTab = IdentifyTableTab(TableCode)
    TabLine = CfgTab(UseTab).GetLineValues(0)
    FldItm = GetItemNo(TabLine, FieldName) - 1
    IdentifyField = CLng(FldItm)
End Function
Private Sub ReadRecordData(MyForm As Form)
    Dim FieldList As String
    Dim TableName As String
    Dim UrnoValue As String
    Dim UseTab As Integer
    Dim GotData As Boolean
    For UseTab = 1 To CfgTab.Count - 1
        UrnoValue = GetItem(DbsSqlKey(UseTab).Text, 2, "=")
        If UrnoValue <> "" Then
            TableName = DbsTblStrip.Tabs(UseTab).Caption + "TAB"
            FieldList = CfgTab(UseTab).GetLineValues(0)
            'Due to bug in Tab.ocx
            While Right(FieldList, 1) = ","
                FieldList = Left(FieldList, Len(FieldList) - 1)
            Wend
            GotData = UfisServer.GetOneRecord(TableName, FieldList, UrnoValue, CedaDataAnswer, True)
            CfgTab(UseTab).InsertTextLineAt 5, CedaDataAnswer, False
        End If
    Next
    PublishRecordData MyForm
    MyForm.Refresh
    If InStr(Command, "TestMode") > 0 Then Me.Show
End Sub
Private Sub PublishRecordData(MyForm As Form)
    Dim UseTab As Integer
    Dim UseCol As Long
    Dim UseLin As Long
    Dim CurCtrl As Control
    Dim TypInfo As String
    Dim RngInfo As String
    Dim IdxInfo As String
    Dim CedaData As String
    Dim Flddata As String
    InitialMaskDisplay = True
    On Error Resume Next
    For Each CurCtrl In MyForm.Controls
        If CurCtrl.Tag <> "" Then
            UseLin = GetIdcLine(CurCtrl.Tag)
            If UseLin > 0 Then
                IdxInfo = CfgTab(0).GetColumnValue(UseLin, TblCol)
                If IdxInfo <> "" Then
                    UseTab = Val(IdxInfo)
                    IdxInfo = CfgTab(0).GetColumnValue(UseLin, IdxCol)
                    UseCol = Val(IdxInfo)
                    CedaData = RTrim(CfgTab(UseTab).GetColumnValue(5, UseCol))
                    TypInfo = CfgTab(0).GetColumnValue(UseLin, TypCol)
                    'MsgBox CedaData
                    Flddata = CedaData
                    CurCtrl.Text = Flddata
                    CfgTab(0).SetColumnValue UseLin, DbsCol, CedaData
                End If
            End If
        End If
    Next
    InitialMaskDisplay = False
End Sub
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Select Case UnloadMode
        Case 0
            Me.Hide
            Cancel = True
        Case Else
            Unload Me
    End Select
End Sub

Private Sub Form_Resize()
    Dim i As Integer
    Dim NewHeight As Long
    DbsTblStrip.Width = ScaleWidth - 2 * DbsTblStrip.Left
    CfgTab(0).Width = ScaleWidth - 2 * CfgTab(0).Left
    NewHeight = ScaleHeight - CfgTab(0).Top - MyStatusBar.Height - 3 * Screen.TwipsPerPixelY
    If NewHeight > 150 Then CfgTab(0).Height = NewHeight
    If CurDataTab > 0 Then
        CfgTab(CurDataTab).Width = DbsTblStrip.Width - 2 * (CfgTab(CurDataTab).Left - DbsTblStrip.Left)
        DbsSqlKey(CurDataTab).Width = ScaleWidth - 2 * DbsSqlKey(CurDataTab).Left
    End If
End Sub
Private Sub ReadApplCfgFile()
    Dim MyFile As String
    Dim FileNbr As Integer
    Dim IniLine As String
    Dim IdcCode As String
    Dim FctCode As String
    Dim IdcItem As String
    Dim CfgCode As String
    Dim MyModuleName As String
    Dim MyCfgFunction As String
    Dim RetVal As Variant, m As Variant
    On Error GoTo ErrHdl
    MyFile = App.Path + "\" + App.EXEName + ".cfg"
    FileNbr = FreeFile
    Open MyFile For Input As #FileNbr
    MyModuleName = "MyConfigModule"
    MyCfgFunction = "ExtCfgData"
    MyScrCtrl.Modules.Add MyModuleName
    MyScrCtrl.AddObject "Cfg", Me
    CfgCode = "Sub " + MyCfgFunction + "()" + vbNewLine
    While Not EOF(FileNbr)
        Line Input #FileNbr, IniLine
        CfgCode = CfgCode + IniLine + vbNewLine
    Wend
    Close #FileNbr
    CfgCode = CfgCode + "End Sub" + vbNewLine
    MyScrCtrl.Modules(MyModuleName).AddCode CfgCode
    Set m = MyScrCtrl.Modules(MyModuleName)
    With m.Procedures(MyCfgFunction)
        RetVal = m.Run(MyCfgFunction)
    End With
    Exit Sub
ErrHdl:
    'MsgBox Err.Description
End Sub
Private Sub ReadApplChgFile()
    Dim MyFile As String
    Dim FileNbr As Integer
    Dim IniLine As String
    Dim IdcCode As String
    Dim FctCode As String
    Dim IdcItem As String
    Dim CfgCode As String
    Dim MyModuleName As String
    Dim MyCfgFunction As String
    Dim RetVal As Variant, m As Variant
    On Error GoTo ErrHdl
    MyFile = App.Path + "\" + App.EXEName + ".chg"
    FileNbr = FreeFile
    Open MyFile For Input As #FileNbr
    MyModuleName = "MyRuntimeModule"
    MyCfgFunction = "RunCfgData"
    MyScrCtrl.Modules.Add MyModuleName
    'MyScrCtrl.AddObject "Cfg", Me
    CfgCode = "Sub " + MyCfgFunction + "()" + vbNewLine
    While Not EOF(FileNbr)
        Line Input #FileNbr, IniLine
        CfgCode = CfgCode + IniLine + vbNewLine
    Wend
    Close #FileNbr
    CfgCode = CfgCode + "End Sub" + vbNewLine
    MyScrCtrl.Modules(MyModuleName).AddCode CfgCode
    Set m = MyScrCtrl.Modules(MyModuleName)
    With m.Procedures(MyCfgFunction)
        RetVal = m.Run(MyCfgFunction)
    End With
    Exit Sub
ErrHdl:
    'MsgBox Err.Description
End Sub

Private Sub ReadApplCusFile()
    Dim MyFile As String
    Dim FileNbr As Integer
    Dim IniLine As String
    Dim IdcCode As String
    Dim FctCode As String
    Dim IdcItem As String
    Dim CfgCode As String
    Dim MyModuleName As String
    Dim MyCfgFunction As String
    Dim RetVal As Variant, m As Variant
    On Error GoTo ErrHdl
    MyFile = App.Path + "\" + App.EXEName + ".cus"
    FileNbr = FreeFile
    Open MyFile For Input As #FileNbr
    MyModuleName = "MyCustModule"
    MyScrCtrl.Modules.Add MyModuleName
    CfgCode = ""
    While Not EOF(FileNbr)
        Line Input #FileNbr, IniLine
        CfgCode = CfgCode + IniLine + vbNewLine
    Wend
    Close #FileNbr
    MyScrCtrl.Modules(MyModuleName).AddCode CfgCode
    'Set m = MyScrCtrl.Modules(MyModuleName)
    'With m.Procedures("Hello")
    '    RetVal = m.Run("Hello")
    'End With
    Exit Sub
ErrHdl:
    'MsgBox "Reading Cus File: " & Err.Description
End Sub

Public Sub CheckControlHandling(ActControl As Control, CalledFrom As String)
    Dim RetVal As Variant, m As Variant
    Dim UseSubName As String
    Dim IdcCode As String
    On Error GoTo ErrHdl
    If InitialMaskDisplay = True Then
        'ActControl.BackColor = vbRed
    Else
        IdcCode = GetIdcCode(ActControl.Tag)
        Select Case CalledFrom
            Case "GotFocus"
                'ActControl.BackColor = vbGreen
                UseSubName = IdcCode + "_" + "Test1"
            Case "TextChanged"
                'ActControl.BackColor = vbWhite
                UseSubName = IdcCode + "_" + "Changed"
            Case "LostFocus"
                'ActControl.BackColor = vbYellow
                UseSubName = IdcCode + "_" + "Test2"
            Case Else
                MsgBox CalledFrom
        End Select
        Set m = MyScrCtrl.Modules("MyCustModule")
        With m.Procedures(UseSubName)
            RetVal = m.Run(UseSubName)
        End With
    End If
    Exit Sub
ErrHdl:
    'MsgBox "Call Function: " & Err.Description
End Sub

Public Function CallCfgSheet(IdcCode As String) As Boolean
    Dim RetVal As Boolean
    RetVal = True
    MsgBox "CfgSheet still under construction!"
    RetVal = False
    CallCfgSheet = RetVal
End Function
