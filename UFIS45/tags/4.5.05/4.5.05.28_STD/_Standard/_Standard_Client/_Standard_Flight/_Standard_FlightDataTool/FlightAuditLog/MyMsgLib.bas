Attribute VB_Name = "MyMsgLib"
Option Explicit

Public Function CallAskUser(MsgType As Integer, PosX As Long, PosY As Long, _
                MsgTitle As String, MsgText As String, _
                IconKey As String, ButtonList As String, UserAnswer As String) As Integer
    
    SetAllFormsOnTop False 'must be defined in StartModule
    CallAskUser = MyMsgBox.AskUser(MsgType, PosX, PosY, MsgTitle, MsgText, IconKey, ButtonList, UserAnswer)
    SetAllFormsOnTop True
End Function

Public Sub UnderConstruction(tmpText As String)
    Dim MsgText As String
    MsgText = "Still under construction." & vbNewLine
    If tmpText <> "" Then MsgText = MsgText & "Ready until " & tmpText
    If CallAskUser(0, 0, 0, "Version Control", MsgText, "hand", "", UserAnswer) <> 0 Then DoNothing
End Sub

Public Function CheckPendingEvent(TaskCode As Integer) As Integer
    CheckPendingEvent = RC_NONE
    If PendingEvent.IsActive Then
        CheckPendingEvent = RC_PENDING
        If TaskCode = RELEASE Then
            Select Case PendingEvent.Type
                Case 1
                    If CallAskUser(0, 0, 0, "Format Error", "Wrong Date Format", "stop", "Edit,Ignore", UserAnswer) = 1 Then
                        PendingEvent.Object.SetFocus
                        CheckPendingEvent = RC_NEWFOCUS
                    Else
                        CheckPendingEvent = RC_IGNORED
                    End If
                Case Else
            End Select
            PendingEvent.IsActive = False
        End If
    End If
End Function


