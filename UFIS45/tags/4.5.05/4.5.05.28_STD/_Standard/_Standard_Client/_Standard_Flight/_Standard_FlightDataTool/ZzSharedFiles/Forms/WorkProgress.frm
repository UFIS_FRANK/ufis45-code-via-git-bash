VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form WorkProgress 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Task Progress Information"
   ClientHeight    =   2955
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7155
   Icon            =   "WorkProgress.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2955
   ScaleWidth      =   7155
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox UfisIntroFrame 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      ForeColor       =   &H80000008&
      Height          =   585
      Left            =   120
      ScaleHeight     =   555
      ScaleWidth      =   1290
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   3090
      Visible         =   0   'False
      Width           =   1320
      Begin VB.Image UfisIntroPicture 
         Height          =   345
         Left            =   0
         Top             =   0
         Width           =   930
      End
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Height          =   1245
      Left            =   60
      TabIndex        =   12
      Top             =   3030
      Visible         =   0   'False
      Width           =   3555
      Begin VB.Timer UfisIntroTimer 
         Enabled         =   0   'False
         Index           =   1
         Interval        =   250
         Left            =   540
         Top             =   720
      End
      Begin VB.FileListBox MyFileList 
         Height          =   1065
         Index           =   0
         Left            =   1470
         TabIndex        =   13
         Top             =   90
         Visible         =   0   'False
         Width           =   1965
      End
      Begin VB.Timer UfisIntroTimer 
         Enabled         =   0   'False
         Index           =   0
         Interval        =   1000
         Left            =   90
         Top             =   720
      End
      Begin VB.Timer UfisIntroTimer 
         Enabled         =   0   'False
         Index           =   2
         Interval        =   100
         Left            =   990
         Top             =   720
      End
   End
   Begin VB.Frame fraOkButton 
      BorderStyle     =   0  'None
      Height          =   645
      Left            =   5820
      TabIndex        =   9
      Top             =   2850
      Width           =   1035
      Begin VB.CheckBox chkWork 
         Caption         =   "Cancel"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   4
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   330
         Width           =   1035
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Start"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   5
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   0
         Width           =   1035
      End
   End
   Begin VB.Frame fraDisplay 
      BackColor       =   &H0000C000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   30
      TabIndex        =   6
      Top             =   60
      Width           =   7005
      Begin VB.Label lblInfo 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Index           =   1
         Left            =   4260
         TabIndex        =   17
         Top             =   45
         Visible         =   0   'False
         Width           =   1500
      End
      Begin VB.Label lblInfo 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Index           =   0
         Left            =   2700
         TabIndex        =   16
         Top             =   45
         Visible         =   0   'False
         Width           =   1515
      End
      Begin VB.Label lblContext 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Selected Functions"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Index           =   1
         Left            =   5280
         TabIndex        =   8
         Top             =   90
         Visible         =   0   'False
         Width           =   1620
      End
      Begin VB.Label lblContext 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Progress Report"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Index           =   0
         Left            =   120
         TabIndex        =   7
         Top             =   60
         Width           =   1410
      End
      Begin VB.Label lblConBack 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "No progress report available"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   225
         Index           =   0
         Left            =   135
         TabIndex        =   18
         Top             =   75
         Width           =   2415
      End
   End
   Begin VB.Timer Timer2 
      Enabled         =   0   'False
      Interval        =   5000
      Left            =   6330
      Top             =   3630
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   5000
      Left            =   5880
      Top             =   3630
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   1
      Top             =   2670
      Width           =   7155
      _ExtentX        =   12621
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   10001
         EndProperty
      EndProperty
   End
   Begin VB.Frame ButtonPanel 
      Enabled         =   0   'False
      Height          =   1995
      Left            =   5760
      TabIndex        =   2
      Top             =   510
      Width           =   1155
      Begin VB.CheckBox chkWork 
         Caption         =   "Marker"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   1140
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "User Log"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   810
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "Flight Log"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   480
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.CheckBox chkWork 
         Caption         =   "All Grids"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   150
         Width           =   1035
      End
   End
   Begin TABLib.TAB ProgTab 
      Height          =   2055
      Left            =   30
      TabIndex        =   0
      Top             =   540
      Width           =   5355
      _Version        =   65536
      _ExtentX        =   9446
      _ExtentY        =   3625
      _StockProps     =   64
   End
End
Attribute VB_Name = "WorkProgress"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim GlobalResult As String
Dim CurProgLine As Long
Dim MyStartTime
Dim ModalBusy As Boolean
Dim UseMultiSel As Boolean
Dim CurCallButton As CheckBox

Public Function ShowProgress(CurForm As Form, CurButton As CheckBox, SetModal As Boolean) As Boolean
    Set CurCallButton = CurButton
    If SetModal Then
        Me.Show vbModal, CurForm
        CurButton.Value = 0
    Else
        Me.Show , CurForm
    End If
    ShowProgress = False
End Function

Public Function SelectFeature(SetStyle As Integer, DispTitle As String, B1 As Integer, B2 As Integer, B3 As Integer, MultiSel As Boolean, ShowModal As Boolean, DispColor As Long) As String
    Dim i As Integer
    On Error Resume Next
    fraDisplay.BackColor = DispColor
    CurCallButton.Value = 0
    ModalBusy = True
    UseMultiSel = True
    ProgTab.ResetContent
    lblContext(0) = DispTitle
    lblConBack(0) = DispTitle
    lblContext(0).Top = 90
    lblContext(0).Left = 120
    lblConBack(0).Top = 105
    lblConBack(0).Left = 135
    Select Case SetStyle
        Case 1
            lblContext(0).ZOrder
        Case 2
            lblConBack(0).ZOrder
        Case Else
    End Select
    lblContext(1) = "Select the desired data function please:"
    GlobalResult = ""
    ButtonPanel.Enabled = False
    chkWork(4).Enabled = False
    chkWork(5).Enabled = False
    chkWork(3).Value = 0
    If BookMarkTotal > 0 Then
        chkWork(3).Visible = True
    Else
        chkWork(3).Visible = False
    End If
    If B1 >= 0 Then
        chkWork(0).Value = B1
        chkWork(0).Enabled = True
    Else
        chkWork(0).Value = 0
        chkWork(0).Enabled = False
    End If
    If B2 >= 0 Then
        chkWork(1).Value = B2
        chkWork(1).Enabled = True
    Else
        chkWork(1).Value = 0
        chkWork(1).Enabled = False
    End If
    If B3 >= 0 Then
        chkWork(2).Value = B3
        chkWork(2).Enabled = True
    Else
        chkWork(2).Value = 0
        chkWork(2).Enabled = False
    End If
    UseMultiSel = MultiSel
    If ShowModal Then
        'ProgTab.Visible = False
        ButtonPanel.Enabled = True
        chkWork(4).Enabled = True
        chkWork(5).Enabled = True
        UfisIntroTimer(2).Enabled = True
        Me.Show vbModal
        ProgTab.Visible = True
        UfisIntroFrame.Visible = False
        UfisIntroTimer(0).Enabled = False
        UfisIntroTimer(1).Enabled = False
        UfisIntroTimer(2).Enabled = False
        SelectFeature = GlobalResult
    End If
    ModalBusy = False
    ButtonPanel.Enabled = False
    chkWork(4).Enabled = True
    chkWork(5).Enabled = False
    lblContext(1) = "Selected Data Functions:"
End Function
Public Sub ResetDataGrid()
    CurProgLine = -1
    ProgTab.ResetContent
    ProgTab.Refresh
    lblInfo(0).Visible = False
    lblInfo(1).Visible = False
    'Me.Refresh
    DoEvents
End Sub
Public Function CreateNewEntry(CurArea As String, CurSection As String, CurTask As String, ResetTab As Boolean) As Long
    Dim LineNo As Long
    Dim VisLines As Long
    Dim tmpLine As String
    Dim tmpResult As String
    Dim ExpVal As Integer
    Dim DotPos As Integer
    Dim tmpLeft As String
    Dim tmpRight As String
    Dim tmpVal2 As String
    If ResetTab = True Then
        ProgTab.ResetContent
        CurProgLine = -1
    End If
    If CurProgLine >= 0 Then
        tmpResult = CStr(Timer - MyStartTime)
        If InStr(tmpResult, "E-") > 0 Then
            ExpVal = Abs(Val(GetItem(tmpResult, 2, "E")))
            tmpResult = "0000" & tmpResult
            DotPos = InStr(tmpResult, ".") - ExpVal
            If DotPos > 0 Then
                tmpResult = Replace(tmpResult, ".", "", 1, -1, vbBinaryCompare)
                DotPos = DotPos - 1
                tmpResult = Left(tmpResult, DotPos) & "." & Mid(tmpResult, DotPos + 1)
                While Left(tmpResult, 1) = "0"
                    tmpResult = Mid(tmpResult, 2)
                Wend
            End If
        End If
        If Left(tmpResult, 1) = "." Then tmpResult = "0" & tmpResult
        tmpResult = GetItem(tmpResult, 1, ".") & "." & Left(GetItem(tmpResult, 2, "."), 3)
        tmpVal2 = Trim(ProgTab.GetFieldValue(CurProgLine, "VAL2"))
        If tmpVal2 <> "" Then ProgTab.SetFieldValues CurProgLine, "DURA", tmpResult
    End If
    MyStartTime = Timer
    tmpLine = CreateEmptyLine(ProgTab.LogicalFieldList)
    ProgTab.InsertTextLine tmpLine, False
    LineNo = ProgTab.GetLineCount - 1
    ProgTab.SetFieldValues LineNo, "AREA,NAME,TASK", CurArea & "," & CurSection & "," & CurTask
    VisLines = VisibleTabLines(ProgTab)
    ProgTab.OnVScrollTo LineNo - VisLines
    CurProgLine = LineNo
    ProgTab.SetCurrentSelection LineNo
    ProgTab.Refresh
    'Me.Refresh
    DoEvents
    CreateNewEntry = LineNo
End Function
Public Sub UpdateProgress(CurValue As Long, MaxValue As Long, CurCount As Long, CurTotal As Long)
    Dim tmpData As String
    Dim CurPerc As Integer
    If CurCount > 0 Then ProgTab.SetFieldValues CurProgLine, "VAL1", CStr(CurCount)
    If CurTotal > 0 Then ProgTab.SetFieldValues CurProgLine, "VAL2", CStr(CurTotal)
    If MaxValue > 0 Then CurPerc = CInt((CurValue * 100) \ MaxValue)
    tmpData = CStr(CurPerc) & "%"
    ProgTab.SetFieldValues CurProgLine, "VALU", tmpData
    tmpData = "Prog" & CStr(CurPerc)
    ProgTab.SetDecorationObject CurProgLine, 3, tmpData
    ProgTab.Refresh
    'Me.Refresh
    DoEvents
End Sub
Public Sub SetTaskText(CurTask As String)
    ProgTab.SetFieldValues CurProgLine, "TASK", CurTask
    ProgTab.Refresh
    'Me.Refresh
    DoEvents
End Sub
Public Function GetProgressValue(FieldName As String) As Long
    Dim tmpValue As String
    tmpValue = ProgTab.GetFieldValue(CurProgLine, FieldName)
    If FieldName = "VALU" Then tmpValue = Replace(tmpValue, "%", "", 1, -1, vbBinaryCompare)
    GetProgressValue = Val(tmpValue)
End Function
Public Sub RemoveEntry()
    If CurProgLine >= 0 Then ProgTab.DeleteLine CurProgLine
    CurProgLine = -1
    ProgTab.Refresh
    'Me.Refresh
    DoEvents
End Sub
Public Sub SetContextInfo(InfoText1 As String, InfoText2 As String)
    If InfoText1 <> "" Then
        lblInfo(0).Caption = InfoText1
        lblInfo(0).Visible = True
        lblInfo(0).Refresh
    Else
        lblInfo(0).Visible = False
    End If
    If InfoText2 <> "" Then
        lblInfo(1).Caption = InfoText2
        lblInfo(1).Visible = True
        lblInfo(1).Refresh
    Else
        lblInfo(1).Visible = False
    End If
End Sub
Public Sub SetPeriodInfo(CedaDate1 As String, CedaDate2 As String)
    Dim tmpTime As String
    Dim VbTime
    If CedaDate1 <> "" Then
        VbTime = CedaFullDateToVb(CedaDate1)
        VbTime = DateAdd("n", UtcTimeDisp, VbTime)
        tmpTime = Format(VbTime, "yyyy.mm.dd / hh:mm")
        lblInfo(0).Caption = tmpTime
        lblInfo(0).Visible = True
        lblInfo(0).Refresh
    Else
        lblInfo(0).Visible = False
    End If
    If CedaDate2 <> "" Then
        VbTime = CedaFullDateToVb(CedaDate2)
        VbTime = DateAdd("n", UtcTimeDisp, VbTime)
        tmpTime = Format(VbTime, "yyyy.mm.dd / hh:mm")
        lblInfo(1).Caption = tmpTime
        lblInfo(1).Visible = True
        lblInfo(1).Refresh
    Else
        lblInfo(1).Visible = False
    End If
End Sub

Private Sub chkWork_Click(Index As Integer)
    Dim i As Integer
    On Error Resume Next
    If chkWork(Index).Value = 1 Then
        chkWork(Index).BackColor = LightGreen
        If UseMultiSel = False Then
            If Index < 4 Then
                For i = 0 To 3
                    If i <> Index Then chkWork(i).Value = 0
                Next
            End If
        End If
        If ModalBusy = True Then
            If Index = 4 Then
                GlobalResult = "-1"
                chkWork(Index).Value = 0
                ModalBusy = False
                Me.Hide
            End If
            If Index = 5 Then
                GlobalResult = ""
                GlobalResult = GlobalResult & CStr(chkWork(0).Value) & ","
                GlobalResult = GlobalResult & CStr(chkWork(1).Value) & ","
                GlobalResult = GlobalResult & CStr(chkWork(2).Value) & ","
                GlobalResult = GlobalResult & CStr(chkWork(3).Value) & ","
                chkWork(Index).Value = 0
                ModalBusy = False
                Me.Hide
            End If
        Else
            If (Index = 4) And (AodbCallInProgress = True) Then
                StopAllLoops = True
            ElseIf Index = 4 Then
                chkWork(Index).Value = 0
                Me.Hide
                CurCallButton.Value = 0
            End If
        End If
    Else
        chkWork(Index).BackColor = vbButtonFace
        If (Index = 4) And (StopAllLoops = True) Then
            chkWork(Index).BackColor = vbRed
            chkWork(Index).ForeColor = vbWhite
            Me.Refresh
            DoEvents
        End If
    End If
End Sub

Private Sub chkWork_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim tmpData As String
    If (Index = 4) And (AodbCallInProgress = True) Then
        tmpData = "Do you want to stop now?" & vbNewLine
        If (MyMsgBox.CallAskUser(0, 0, 0, "Application Control", tmpData, "ask", "Yes,No", UserAnswer) = 1) Then StopAllLoops = True
    End If
End Sub

Private Sub Form_Activate()
    Dim MyTop As Long
    If MainDialog.WindowState = vbMinimized Then
        MyTop = Screen.Height + 600
        Me.Top = MyTop
    ElseIf ApplIsHidden Then
        MyTop = Screen.Height + 600
        Me.Top = MyTop
    Else
        MyTop = MainDialog.Top + ((MainDialog.Height - Me.Height) / 2) + 900
        Me.Top = MyTop
    End If
End Sub

Private Sub Form_Load()
    Dim MyTop As Long
    Dim i As Integer
    Me.Width = 733 * 15
    MyTop = MainDialog.Top + ((MainDialog.Height - Me.Height) / 2) + 900
    If MainDialog.WindowState = vbMinimized Then MyTop = Screen.Height + 600
    If ApplIsHidden Then MyTop = Screen.Height + 600
    Me.Top = MyTop
    Me.Left = MainDialog.Left + MainDialog.WorkArea.Left + ((MainDialog.WorkArea.Width - Me.Width) / 2)
    ProgTab.ResetContent
    ProgTab.LogicalFieldList = "AREA,NAME,TASK,VALU,VAL1,VAL2,DURA,REMA"
    ProgTab.HeaderString = "Section,Data Area,Task,Progress,Count ,Total ,Seconds ,"
    ProgTab.HeaderLengthString = "70,70,140,200,50,50,65,500"
    ProgTab.ColumnWidthString = "10,10,10,10,10,10,10,10"
    ProgTab.HeaderAlignmentString = "L,L,L,C,R,R,R,L"
    ProgTab.ColumnAlignmentString = "L,L,L,C,R,R,R,L"
    ProgTab.FontName = "Arial"
    ProgTab.SetTabFontBold True
    ProgTab.FontSize = 15
    ProgTab.LineHeight = 15
    ProgTab.HeaderFontSize = 15
    ProgTab.LifeStyle = True
    ProgTab.CursorLifeStyle = True
    ProgTab.ShowVertScroller False
    For i = 1 To 100
        ProgTab.CreateDecorationObject "Prog" & CStr(i), "L,L,T,B,R", CStr(i * 2) & ",2,2,2,2", Str(vbGreen) & "," & Str(LightGray) & "," & Str(LightGray) & "," & Str(vbBlack) & "," & Str(vbBlack)
    Next
    If BookMarkTotal > 0 Then chkWork(3).Visible = True Else chkWork(3).Visible = False
    fraDisplay.ZOrder
    StatusBar1.ZOrder
End Sub

Private Sub Form_Resize()
    Dim NewVal As Long
    fraDisplay.Top = 0
    fraDisplay.Left = 0
    NewVal = Me.ScaleWidth
    If NewVal > 300 Then
        fraDisplay.Width = NewVal
        lblContext(1).Left = NewVal - lblContext(1).Width - 90
        NewVal = (NewVal \ 2) + 255
        lblInfo(0).Left = NewVal - lblInfo(0).Width - 15
        lblInfo(1).Left = NewVal + 15
    End If
    NewVal = Me.ScaleWidth
    If ButtonPanel.Visible = True Then NewVal = NewVal - ButtonPanel.Width
    ButtonPanel.Left = NewVal
    fraOkButton.Left = NewVal + 60
    ProgTab.Left = fraDisplay.Left
    ButtonPanel.Top = fraDisplay.Top + fraDisplay.Height - 75
    ProgTab.Top = ButtonPanel.Top + 90
    NewVal = ButtonPanel.Left - ProgTab.Left - 15
    If NewVal > 300 Then ProgTab.Width = NewVal
    NewVal = Me.ScaleHeight - ButtonPanel.Top - StatusBar1.Height '- 15
    If NewVal > 300 Then ButtonPanel.Height = NewVal
    NewVal = ButtonPanel.Top + ButtonPanel.Height - fraOkButton.Height - 60
    fraOkButton.Top = NewVal
    NewVal = Me.ScaleHeight - ProgTab.Top - StatusBar1.Height - 15
    If NewVal > 300 Then ProgTab.Height = NewVal
    If ProgTab.Visible = False Then UfisIntroTimer(1).Enabled = True
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error Resume Next
    CurCallButton.Value = 0
End Sub

Private Sub ProgTab_TimerExpired(ByVal LineNo As Long, ByVal LineStatus As Long)
    Dim tmpVal As String
    tmpVal = Trim(ProgTab.GetColumnValue(LineNo, 5))
    tmpVal = Replace(tmpVal, "%", "", 1, -1, vbBinaryCompare)
    If Val(tmpVal) >= 100 Then
        ProgTab.SetLineStatusValue LineNo, 1
    Else
        ProgTab.TimerSetValue LineNo, 12
    End If
End Sub

Private Sub Timer1_Timer()
    Timer1.Enabled = False
    ProgTab.TimerCheck
    Timer2.Enabled = True
End Sub

Private Sub Timer2_Timer()
    Dim MaxLine As Long
    Dim CurLine As Long
    Timer2.Enabled = False
    ProgTab.IndexDestroy "LOOK"
    MaxLine = ProgTab.GetLineCount - 1
    CurLine = 0
    While CurLine <= MaxLine
        If ProgTab.GetLineStatusValue(CurLine) = 1 Then
            ProgTab.DeleteLine CurLine
            CurLine = CurLine - 1
            MaxLine = MaxLine - 1
        End If
        CurLine = CurLine + 1
    Wend
    ProgTab.IndexCreate "LOOK", 0
    ProgTab.RedrawTab
    If ProgTab.GetLineCount = 0 Then chkWork(0).Value = 1
    Timer1.Enabled = True
End Sub
Private Sub UfisIntroTimer_Timer(Index As Integer)
    Select Case Index
        Case 0
            CheckUfisIntroPicture False, 0
        Case 1
            CheckUfisIntroPicture True, 0
            UfisIntroTimer(Index).Enabled = False
        Case 2
            ProgTab.Visible = False
            CheckUfisIntroPicture False, 0
            UfisIntroTimer(Index).Enabled = False
        Case Else
    End Select
End Sub
Private Sub CheckUfisIntroPicture(RefreshPicture As Boolean, IncIdx As Integer)
    Static FileIndex As Integer
    Static FileCount As Integer
    Static CountDownLimit As Long
    Static CountDownValue As Long
    Static PathName As String
    Dim FilePattern As String
    Dim FileName As String
    Dim NewSize As Long
    Dim TimeStep As Long
    Dim ActPicX
    Dim ActPicY
    Dim ActZoom
    On Error Resume Next
    If ProgTab.Visible = False Then
        If PathName = "" Then
            CountDownValue = 1
            CountDownLimit = 1
            PathName = GetIniEntry(myIniFullName, "MAIN", "", "APPL_PICTURE_PATH", UFIS_SYSTEM)
            TimeStep = Val(GetIniEntry(myIniFullName, "MAIN", "", "APPL_SLIDE_INTERVAL", "5"))
            If TimeStep < 1 Then TimeStep = 5
            If TimeStep > 60 Then
                CountDownLimit = TimeStep
                TimeStep = 1
            End If
            TimeStep = TimeStep * 1000
            UfisIntroTimer(0).Interval = TimeStep
        End If
        FileIndex = FileIndex + IncIdx
        If RefreshPicture = True Then
            FileIndex = FileIndex - 1
            CountDownValue = CountDownValue + 1
        End If
        CountDownValue = CountDownValue - 1
        If (CountDownValue <= 0) Or (RefreshPicture = True) Then
            If PathName = UFIS_SYSTEM Then
                FilePattern = "UfisFal*.jpg;UfisFal*.bmp"
            Else
                FilePattern = "*.jpg;*.bmp"
            End If
            FileName = GetNextFile(PathName, FilePattern, FileIndex, FileCount, True)
            If FileName <> "" Then
                UfisIntroPicture.Visible = False
                UfisIntroPicture.Stretch = False
                UfisIntroPicture.Picture = LoadPicture(PathName & "\" & FileName)
                ActPicX = UfisIntroPicture.Width
                ActPicY = UfisIntroPicture.Height
                NewSize = ProgTab.Width
                If ActPicX > NewSize Then
                    UfisIntroPicture.Stretch = True
                    ActZoom = ActPicX / NewSize
                    UfisIntroPicture.Width = ActPicX / ActZoom
                    UfisIntroPicture.Height = ActPicY / ActZoom
                End If
                NewSize = ProgTab.Height
                ActPicX = UfisIntroPicture.Width
                ActPicY = UfisIntroPicture.Height
                If ActPicY > NewSize Then
                    UfisIntroPicture.Stretch = True
                    ActZoom = ActPicY / NewSize
                    UfisIntroPicture.Width = ActPicX / ActZoom
                    UfisIntroPicture.Height = ActPicY / ActZoom
                End If
                UfisIntroFrame.Width = UfisIntroPicture.Width
                UfisIntroFrame.Height = UfisIntroPicture.Height
                UfisIntroPicture.Visible = True
                UfisIntroFrame.Visible = True
                
                If FileCount > 1 Then UfisIntroTimer(0).Enabled = True Else UfisIntroTimer(0).Enabled = False
            Else
                UfisIntroFrame.Visible = False
            End If
            UfisIntroFrame.Top = ((ProgTab.Height - UfisIntroPicture.Height) \ 2) + ProgTab.Top
            UfisIntroFrame.Left = ((ProgTab.Width - UfisIntroPicture.Width) \ 2) + ProgTab.Left
            If RefreshPicture = False Then CountDownValue = CountDownLimit
        End If
    End If
End Sub

Private Function GetNextFile(PathName As String, FileName As String, FileIndex As Integer, FileCount As Integer, SwapAround As Boolean) As String
    Dim Result As String
    On Error Resume Next
    Result = ""
    MyFileList(0).Path = PathName
    MyFileList(0).FileName = FileName
    If FileIndex >= MyFileList(0).ListCount Then
        If SwapAround = True Then FileIndex = 0 Else FileIndex = -1
    End If
    If FileIndex >= 0 Then Result = MyFileList(0).List(FileIndex)
    FileIndex = FileIndex + 1
    FileCount = MyFileList(0).ListCount
    GetNextFile = Result
End Function


