VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.ocx"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form SlideControls 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "UFIS Scenario Controls"
   ClientHeight    =   3720
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   13710
   Icon            =   "SlideControls.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3720
   ScaleWidth      =   13710
   ShowInTaskbar   =   0   'False
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   14
      Top             =   3435
      Width           =   13710
      _ExtentX        =   24183
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   2646
            MinWidth        =   2646
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   21458
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Height          =   1245
      Left            =   0
      TabIndex        =   11
      Top             =   390
      Visible         =   0   'False
      Width           =   3555
      Begin VB.Timer UfisIntroTimer 
         Enabled         =   0   'False
         Index           =   1
         Interval        =   250
         Left            =   540
         Top             =   720
      End
      Begin VB.FileListBox MyFileList 
         Height          =   870
         Index           =   0
         Left            =   1470
         TabIndex        =   12
         Top             =   90
         Visible         =   0   'False
         Width           =   1965
      End
      Begin VB.Timer UfisIntroTimer 
         Enabled         =   0   'False
         Index           =   0
         Interval        =   500
         Left            =   90
         Top             =   720
      End
      Begin VB.Timer UfisIntroTimer 
         Enabled         =   0   'False
         Index           =   2
         Interval        =   2000
         Left            =   990
         Top             =   720
      End
      Begin TABLib.TAB tabSlides 
         Height          =   585
         Left            =   90
         TabIndex        =   13
         Top             =   90
         Visible         =   0   'False
         Width           =   1335
         _Version        =   65536
         _ExtentX        =   2355
         _ExtentY        =   1032
         _StockProps     =   64
      End
   End
   Begin VB.Frame fraFuncPanel 
      BorderStyle     =   0  'None
      Height          =   300
      Index           =   2
      Left            =   30
      TabIndex        =   0
      Top             =   30
      Width           =   13875
      Begin VB.CheckBox chkTool 
         Caption         =   "Close"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   9
         Left            =   12600
         Style           =   1  'Graphical
         TabIndex        =   16
         Tag             =   "CLOSE"
         Top             =   0
         Width           =   1050
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Details"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   8
         Left            =   11550
         Style           =   1  'Graphical
         TabIndex        =   15
         Tag             =   "SLIDE.DETAILS"
         Top             =   0
         Width           =   1050
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Stop All"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   2
         Left            =   2100
         Style           =   1  'Graphical
         TabIndex        =   8
         Tag             =   "SLIDE.STOP"
         Top             =   0
         Width           =   1050
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Collected"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   7
         Left            =   7350
         Style           =   1  'Graphical
         TabIndex        =   7
         Tag             =   "SLIDE.COLLECTED"
         Top             =   0
         Width           =   1050
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Hold All"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   6
         Left            =   6300
         Style           =   1  'Graphical
         TabIndex        =   6
         Tag             =   "SLIDE.HOLD.ALL"
         Top             =   0
         Width           =   1050
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Synchron"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   4
         Left            =   4200
         Style           =   1  'Graphical
         TabIndex        =   5
         Tag             =   "SLIDE.DOUBLE"
         Top             =   0
         Width           =   1050
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Show List"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   1
         Left            =   1050
         Style           =   1  'Graphical
         TabIndex        =   4
         Tag             =   "SLIDE.SELECT"
         Top             =   0
         Width           =   1050
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Hold Right"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   5
         Left            =   5250
         Style           =   1  'Graphical
         TabIndex        =   3
         Tag             =   "SLIDE.HOLD.RIGHT"
         Top             =   0
         Width           =   1050
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Hold Left"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   3
         Left            =   3150
         Style           =   1  'Graphical
         TabIndex        =   2
         Tag             =   "SLIDE.HOLD.LEFT"
         Top             =   0
         Width           =   1050
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Open Files"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   1
         Tag             =   "SLIDES"
         Top             =   0
         Width           =   1050
      End
      Begin MSComctlLib.Slider Slider1 
         Height          =   315
         Index           =   1
         Left            =   10500
         TabIndex        =   9
         ToolTipText     =   "Steps Per Time Unit (1)"
         Top             =   0
         Width           =   1050
         _ExtentX        =   1852
         _ExtentY        =   556
         _Version        =   393216
         LargeChange     =   1
         Min             =   1
         Max             =   5
         SelStart        =   1
         TickStyle       =   3
         Value           =   1
      End
      Begin MSComctlLib.Slider Slider1 
         Height          =   315
         Index           =   0
         Left            =   8400
         TabIndex        =   10
         ToolTipText     =   "Scenario Unit Timer (5)"
         Top             =   0
         Width           =   2100
         _ExtentX        =   3704
         _ExtentY        =   556
         _Version        =   393216
         LargeChange     =   1
         SelStart        =   5
         TickStyle       =   3
         Value           =   5
      End
   End
End
Attribute VB_Name = "SlideControls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    Me.Height = 975
    Me.Width = 13800
    Me.Top = UfisPicForm.Top + UfisPicArea.Top
    Me.Left = UfisPicForm.Left + (UfisPicArea.Left + (UfisPicArea.Width - Me.Width) / 2) + 60
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode < 2 Then
        If ApplicationIsReadyForUse Then
            If SlideActive Then
                SetUfisIntroFrames False
                SetUfisIntroFrames True
            End If
        End If
    End If
End Sub

Private Sub UfisIntroTimer_Timer(Index As Integer)
    Dim tmpCount As Long
    Dim tmpTime As Long
    Dim tmpRest As Long
    Dim tmpHour As Long
    Dim tmpMinute   As Long
    Dim tmpSec As Long
    Dim tmpSlide As String
    Dim tmpPath As String
    Dim tmpText As String
    If ApplicationIsReadyForUse Then
        Select Case Index
            Case 0  'Timer for IntroPicture(s)
                CheckUfisIntroPicture 0, False, 0, "", ""
            Case 1  'Timer for Picture Resizing
                CheckUfisIntroPicture 1, True, 0, "", ""
                UfisIntroTimer(Index).Enabled = False
            Case 2  'Timer for Slides
                UfisIntroTimer(0).Enabled = False
                UfisIntroTimer(Index).Enabled = False
                tmpCount = FileDialog.GetNextResultLine(SlideIndex, tmpPath, tmpSlide)
                chkTool(7).Value = SlideIndex
                If tmpCount > 0 Then
                    CheckUfisIntroPicture 2, False, 0, tmpSlide, tmpPath
                    If Slider1(1).Value > 0 Then
                        tmpTime = tmpCount * CLng(Slider1(0).Value) \ CLng(Slider1(1).Value)
                    Else
                        tmpTime = tmpCount * CLng(Slider1(0).Value)
                    End If
                    tmpHour = tmpTime \ 3600
                    tmpRest = tmpTime - tmpHour * 3600
                    tmpMinute = tmpRest \ 60
                    tmpSec = tmpRest - tmpMinute * 60
                    tmpText = CStr(tmpHour) & "h:" & CStr(tmpMinute) & "m:" & CStr(tmpSec) & "s"
                    tmpText = CStr(tmpCount) & " / " & tmpText
                    StatusBar1.Panels(1).Text = tmpText
                    StatusBar1.Panels(2).Text = tmpPath & "\" & tmpSlide
                Else
                    StatusBar1.Panels(1).Text = ""
                    StatusBar1.Panels(2).Text = ""
                End If
                UfisIntroTimer(Index).Enabled = SlideShow
            Case Else
        End Select
    End If
End Sub

Public Function GetNextFile(PathName As String, FileName As String, FileIndex As Integer, FileCount As Integer, SwapAround As Boolean) As String
    Dim Result As String
    On Error Resume Next
    Result = ""
    MyFileList(0).Path = PathName
    MyFileList(0).FileName = FileName
    If FileIndex >= MyFileList(0).ListCount Then
        If SwapAround = True Then FileIndex = 0 Else FileIndex = -1
    End If
    If FileIndex >= 0 Then Result = MyFileList(0).List(FileIndex)
    FileIndex = FileIndex + 1
    FileCount = MyFileList(0).ListCount
    GetNextFile = Result
End Function

Private Sub chkTool_Click(Index As Integer)
    Dim tmpValue As Long
    Dim tmpTag As String
    tmpTag = chkTool(Index).Tag
    If chkTool(Index).Value = 1 Then
        chkTool(Index).BackColor = LightGreen
        Select Case tmpTag
            Case "SLIDES"
                HandleSlides
                chkTool(Index).Value = 0
            Case "CLOSE"
                Unload Me
            Case "SLIDE.COLLECTED"
                SlideIndex = 1
                If SlideShow = False Then chkTool(Index).Value = 0
            Case "SLIDE.HOLD.LEFT"
                SlideHoldLeft = True
            Case "SLIDE.HOLD.MID"
                SlideHoldMid = True
            Case "SLIDE.HOLD.RIGHT"
                SlideHoldRight = True
            Case "SLIDE.HOLD.ALL"
                SlideHoldAll = True
                If SlideShow = True Then
                    UfisIntroTimer(2).Enabled = False
                Else
                    UfisIntroTimer(0).Enabled = False
                End If
            Case "SLIDE.SELECT"
                'If SlideShow Then
                    SlideShow = True
                    FileDialog.SelectSlides True
                    FileDialog.Show , Me
                'End If
                'chkTool(Index).Value = 0
            Case "SLIDE.DOUBLE"
                If SlideShow = False Then
                    UfisPicForm.UfisIntroFrame(0).Visible = True
                    UfisPicForm.UfisIntroFrame(1).Visible = True
                    UfisPicForm.UfisIntroFrame(2).Visible = False
                    UfisPicArea.Refresh
                End If
                SlideDouble = True
            Case "SLIDE.STOP"
                ReorgSlideShow False
                ReorgSlideShow False    'Call twice!
                InitSlideShowTimer
                chkTool(Index).Value = 0
            Case "SLIDE.DETAILS"
                If SlideShow Then
                    SlideFullSize.Show
                    SlideFullSize.Refresh
                    AdjustUfisIntroDetails
                End If
                chkTool(Index).Value = 0
            Case Else
                chkTool(Index).Value = 0
        End Select
    Else
        chkTool(Index).BackColor = vbButtonFace
        Select Case tmpTag
            Case "SLIDE.COLLECTED"
                SlideIndex = 0
            Case "SLIDE.HOLD.LEFT"
                SlideHoldLeft = False
            Case "SLIDE.HOLD.MID"
                SlideHoldMid = False
            Case "SLIDE.HOLD.RIGHT"
                SlideHoldRight = False
            Case "SLIDE.HOLD.ALL"
                If SlideShow = True Then
                    UfisIntroTimer(2).Enabled = True
                Else
                    UfisIntroTimer(0).Enabled = True
                End If
                SlideHoldAll = False
            Case "SLIDE.SELECT"
                FileDialog.SelectSlides False
                tmpValue = FileDialog.tabFoundFiles(0).GetLineCount + FileDialog.tabFoundFiles(1).GetLineCount
                If tmpValue > 0 Then
                    InitSlideShowTimer
                Else
                    SlideShow = False
                End If
            Case "SLIDE.DOUBLE"
                If SlideShow = False Then
                    UfisPicForm.UfisIntroFrame(0).Visible = False
                    UfisPicForm.UfisIntroFrame(1).Visible = False
                    UfisPicForm.UfisIntroFrame(2).Visible = True
                    UfisPicArea.Refresh
                End If
                SlideDouble = False
            Case Else
        End Select
    End If
End Sub


Private Sub HandleSlides()
    Static tmpFilePath As String
    Dim tmpFileFilter As String
    Dim tmpFileResult As String
    SlideShow = True
    tmpFileFilter = "{Slide Files}{*.jpg;*.bmp}{M}|{All Files}{*.*}{M}"
    If tmpFilePath = "" Then tmpFilePath = UFIS_UFIS
    tmpFileResult = FileDialog.GetFileOpenDialog(0, tmpFilePath, tmpFileFilter, 0, 0, "SLIDE.SHOW")
    If tmpFileResult = "OK" Then
        InitSlideShowTimer
    Else
        UfisIntroTimer(2).Enabled = False
        SlideShow = False
    End If
End Sub

Private Sub ReorgSlideShow(SetEnable As Boolean)
    Dim tmpTag As String
    Dim tmpSlideTag As String
    Dim tmpFileName As String
    Dim tmpPathName As String
    Dim i As Integer
    On Error Resume Next
    If SetEnable = True Then
        tmpTag = UfisPicForm.UfisIntroFrame(2).Tag
        If tmpTag <> "" Then
            SlideShow = Val(GetItem(tmpTag, 1, ","))
            If SlideShow = True Then
                For i = 0 To 2
                    UfisPicForm.UfisIntroFrame(i).Visible = True
                    tmpSlideTag = GetItem(tmpTag, i + 8, ",")
                    tmpFileName = GetItem(tmpSlideTag, 2, ";")
                    tmpPathName = GetItem(tmpSlideTag, 3, ";")
                    UfisPicForm.UfisIntroPicture(i).Picture = LoadPicture(tmpPathName & "\" & tmpFileName)
                Next
            End If
            chkTool(8).Value = Val(GetItem(tmpTag, 2, ","))
            chkTool(12).Value = Val(GetItem(tmpTag, 3, ","))
            chkTool(9).Value = Val(GetItem(tmpTag, 4, ","))
            chkTool(6).Value = Val(GetItem(tmpTag, 5, ","))
            chkTool(7).Value = Val(GetItem(tmpTag, 6, ","))
            chkTool(11).Value = Val(GetItem(tmpTag, 7, ","))
            If SlideShow = True Then
                CheckUfisIntroPicture 1, True, 0, "", ""
                UfisPicArea.Refresh
                If SlideHoldAll = False Then UfisIntroTimer(2).Enabled = True
            End If
        End If
    Else
        tmpTag = CStr(CInt(SlideShow)) & ","
        tmpTag = tmpTag & CStr(chkTool(8).Value) & ","
        tmpTag = tmpTag & CStr(chkTool(12).Value) & ","
        tmpTag = tmpTag & CStr(chkTool(9).Value) & ","
        tmpTag = tmpTag & CStr(chkTool(6).Value) & ","
        tmpTag = tmpTag & CStr(chkTool(7).Value) & ","
        tmpTag = tmpTag & CStr(chkTool(11).Value) & ","
        For i = 0 To 2
            tmpSlideTag = UfisPicForm.UfisIntroPicture(i).Tag
            tmpSlideTag = Replace(tmpSlideTag, ",", ";", 1, -1, vbBinaryCompare)
            tmpTag = tmpTag & tmpSlideTag & ","
        Next
        UfisPicForm.UfisIntroFrame(2).Tag = tmpTag
        SlideShow = False
        UfisIntroTimer(2).Enabled = False
        chkTool(8).Value = 0
        chkTool(12).Value = 0
        chkTool(9).Value = 0
        chkTool(6).Value = 0
        chkTool(7).Value = 0
        chkTool(11).Value = 0
        UfisPicForm.UfisIntroFrame(0).Visible = False
        UfisPicForm.UfisIntroFrame(1).Visible = False
        StatusBar1.Panels(1).Text = ""
        StatusBar1.Panels(2).Text = ""
        UfisIntroTimer(0).Enabled = True
    End If
End Sub

Private Sub Slider1_Change(Index As Integer)
    Static OldValue(2) As Integer
    If Slider1(Index).Value <> OldValue(Index) Then
        InitSlideShowTimer
        OldValue(Index) = Slider1(Index).Value
        If Index = 0 Then Slider1(Index).ToolTipText = "Scenario Unit Timer (" & CStr(Slider1(Index).Value) & ")"
        If Index = 1 Then Slider1(Index).ToolTipText = "Steps Per Time Unit (" & CStr(Slider1(Index).Value) & ")"
    End If
End Sub

Private Sub InitSlideShowTimer()
    Dim Index As Integer
    If SlideShow = True Then Index = 2 Else Index = 0
    If Slider1(1).Value > 0 Then
        UfisIntroTimer(Index).Interval = Slider1(0).Value * 1000 \ Slider1(1).Value
    Else
        UfisIntroTimer(Index).Interval = Slider1(0).Value * 1000
    End If
    UfisIntroTimer(2).Enabled = False
    If ((SlideShow = True) And (Slider1(0).Value > 0) And (SlideHoldAll = False)) Or (Index = 0) Then
        UfisIntroTimer_Timer Index
    End If
End Sub


