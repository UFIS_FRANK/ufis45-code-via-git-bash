VERSION 5.00
Begin VB.Form GanttDecoObjCfg 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "FormDecorationObject"
   ClientHeight    =   4095
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   7770
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4095
   ScaleWidth      =   7770
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame3 
      Caption         =   "AutoScroll"
      Height          =   1365
      Left            =   4725
      TabIndex        =   32
      Top             =   1890
      Width           =   2940
      Begin VB.TextBox txtScrollInterval 
         Height          =   285
         Index           =   0
         Left            =   1755
         TabIndex        =   38
         Text            =   "100"
         ToolTipText     =   "ms"
         Top             =   900
         Width           =   840
      End
      Begin VB.CommandButton btnScrollInterval 
         Caption         =   "ScrollInterval"
         Height          =   285
         Left            =   135
         TabIndex        =   37
         ToolTipText     =   "Assigns a decoration object to a background-bar"
         Top             =   900
         Width           =   1515
      End
      Begin VB.TextBox txtScrollInset 
         Height          =   285
         Index           =   0
         Left            =   1755
         TabIndex        =   36
         Text            =   "20"
         ToolTipText     =   "ms"
         Top             =   585
         Width           =   840
      End
      Begin VB.CommandButton btnScrollInset 
         Caption         =   "ScrollInset"
         Height          =   285
         Left            =   135
         TabIndex        =   35
         ToolTipText     =   "Assigns a decoration object to a bar"
         Top             =   585
         Width           =   1515
      End
      Begin VB.TextBox txtScrollDelay 
         Height          =   285
         Index           =   0
         Left            =   1755
         TabIndex        =   34
         Text            =   "200"
         ToolTipText     =   "ms to wait till starting autoscroll"
         Top             =   270
         Width           =   840
      End
      Begin VB.CommandButton btnScrollDelay 
         Caption         =   "ScrollDelay"
         Height          =   285
         Left            =   135
         TabIndex        =   33
         Top             =   270
         Width           =   1515
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Time Marker"
      Height          =   1995
      Left            =   45
      TabIndex        =   22
      Top             =   1890
      Width           =   4515
      Begin VB.CheckBox chkEnableDrawTimeMarkerOfActiveBar 
         Caption         =   "EnableDrawTimeMarkerOfActiveBar"
         Enabled         =   0   'False
         Height          =   240
         Left            =   180
         TabIndex        =   31
         Top             =   1575
         Width           =   3030
      End
      Begin VB.CommandButton btnDeleteAllTimeMarkers 
         Caption         =   "DeleteAllTMs"
         Height          =   285
         Left            =   135
         TabIndex        =   30
         Top             =   1215
         Width           =   1425
      End
      Begin VB.TextBox txtTimeMarkerTime 
         Height          =   285
         Index           =   2
         Left            =   1665
         TabIndex        =   29
         Text            =   "29.09.00 22:00:00"
         ToolTipText     =   "Time of the time-marker, e.g. 01.01.2000 10:15"
         Top             =   900
         Width           =   1440
      End
      Begin VB.CommandButton btnGetTimeMarker 
         Caption         =   "GetTimeMarker"
         Height          =   285
         Left            =   135
         TabIndex        =   28
         Top             =   900
         Width           =   1425
      End
      Begin VB.TextBox txtTimeMarkerTime 
         Height          =   285
         Index           =   1
         Left            =   1665
         TabIndex        =   27
         Text            =   "29.09.00 22:00:00"
         ToolTipText     =   "Time of the time-marker, e.g. 01.01.2000 10:15"
         Top             =   585
         Width           =   1440
      End
      Begin VB.CommandButton btnDeleteTimeMarker 
         Caption         =   "DeleteTimeMarker"
         Height          =   285
         Left            =   135
         TabIndex        =   26
         Top             =   585
         Width           =   1425
      End
      Begin VB.TextBox txtTimeMarkerColor 
         Height          =   285
         Left            =   3195
         TabIndex        =   25
         Text            =   "255"
         ToolTipText     =   "Color of the time-marker"
         Top             =   270
         Width           =   1155
      End
      Begin VB.TextBox txtTimeMarkerTime 
         Height          =   285
         Index           =   0
         Left            =   1665
         TabIndex        =   24
         Text            =   "29.09.00 22:00:00"
         ToolTipText     =   "Time of the time-marker, e.g. 01.01.2000 10:15"
         Top             =   270
         Width           =   1440
      End
      Begin VB.CommandButton btnSetTimeMarker 
         Caption         =   "SetTimeMarker"
         Height          =   285
         Left            =   135
         TabIndex        =   23
         Top             =   270
         Width           =   1425
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Decoration Object"
      Height          =   1635
      Left            =   45
      TabIndex        =   0
      Top             =   90
      Width           =   7665
      Begin VB.CommandButton btnCreate 
         Caption         =   "Create"
         Height          =   285
         Left            =   90
         TabIndex        =   21
         Top             =   270
         Width           =   1515
      End
      Begin VB.TextBox txtLocationList 
         Height          =   285
         Left            =   2610
         TabIndex        =   20
         Text            =   "TL,TR"
         ToolTipText     =   "Locationlist: e.g. ""TL,BL,BR,BL,L,R,BTL,BTR,T,B"""
         Top             =   270
         Width           =   1440
      End
      Begin VB.TextBox txtID 
         Height          =   285
         Index           =   0
         Left            =   1710
         TabIndex        =   19
         Text            =   "ID"
         ToolTipText     =   "ID for the decoration object"
         Top             =   270
         Width           =   840
      End
      Begin VB.TextBox txtPixelList 
         Height          =   285
         Left            =   4125
         TabIndex        =   18
         Text            =   "15,10"
         ToolTipText     =   "PixelList: comma separated list, leg of triangle or width of the bar in pixels"
         Top             =   270
         Width           =   1485
      End
      Begin VB.TextBox txtColorList 
         Height          =   285
         Left            =   5670
         TabIndex        =   17
         Text            =   "255,123456"
         ToolTipText     =   "ColorList: Comma separated colors"
         Top             =   270
         Width           =   1830
      End
      Begin VB.CommandButton btnSetToBar 
         Caption         =   "SetToBar"
         Height          =   285
         Left            =   90
         TabIndex        =   16
         ToolTipText     =   "Assigns a decoration object to a bar"
         Top             =   585
         Width           =   1515
      End
      Begin VB.TextBox txtBarKey 
         Height          =   285
         Index           =   0
         Left            =   2610
         TabIndex        =   15
         Text            =   "Key"
         ToolTipText     =   "Key of the bar you want to assign the decoration object"
         Top             =   585
         Width           =   840
      End
      Begin VB.TextBox txtID 
         Height          =   285
         Index           =   1
         Left            =   1710
         TabIndex        =   14
         Text            =   "ID"
         ToolTipText     =   "ID of the decoration object"
         Top             =   585
         Width           =   840
      End
      Begin VB.CommandButton btnSetToBkBar 
         Caption         =   "SetToBkBar"
         Height          =   285
         Left            =   90
         TabIndex        =   13
         ToolTipText     =   "Assigns a decoration object to a background-bar"
         Top             =   900
         Width           =   1515
      End
      Begin VB.TextBox txtBarKey 
         Height          =   285
         Index           =   1
         Left            =   2610
         TabIndex        =   12
         Text            =   "Key"
         ToolTipText     =   "Key of the background bar you want to assign the decoration object"
         Top             =   900
         Width           =   840
      End
      Begin VB.TextBox txtID 
         Height          =   285
         Index           =   2
         Left            =   1710
         TabIndex        =   11
         Text            =   "ID"
         ToolTipText     =   "ID of the decoration object"
         Top             =   900
         Width           =   840
      End
      Begin VB.TextBox txtID 
         Height          =   285
         Index           =   3
         Left            =   5760
         TabIndex        =   10
         Text            =   "ID"
         ToolTipText     =   "ID of the decoration object"
         Top             =   585
         Width           =   840
      End
      Begin VB.TextBox txtBarKey 
         Height          =   285
         Index           =   2
         Left            =   6660
         TabIndex        =   9
         Text            =   "Key"
         ToolTipText     =   "Key of the bar you want to reset the decoration object"
         Top             =   585
         Width           =   840
      End
      Begin VB.CommandButton btnResetBar 
         Caption         =   "ResetBar"
         Height          =   285
         Left            =   4140
         TabIndex        =   8
         ToolTipText     =   "Assigns a decoration object to a bar"
         Top             =   585
         Width           =   1515
      End
      Begin VB.TextBox txtID 
         Height          =   285
         Index           =   4
         Left            =   5760
         TabIndex        =   7
         Text            =   "ID"
         ToolTipText     =   "ID of the decoration object"
         Top             =   900
         Width           =   840
      End
      Begin VB.TextBox txtBarKey 
         Height          =   285
         Index           =   3
         Left            =   6660
         TabIndex        =   6
         Text            =   "Key"
         ToolTipText     =   "Key of the background bar you want to reset the decoration object"
         Top             =   900
         Width           =   840
      End
      Begin VB.CommandButton btnResetBkBar 
         Caption         =   "ResetBkBar"
         Height          =   285
         Left            =   4140
         TabIndex        =   5
         ToolTipText     =   "Assigns a decoration object to a bar"
         Top             =   900
         Width           =   1515
      End
      Begin VB.TextBox txtBarKey 
         Height          =   285
         Index           =   4
         Left            =   1710
         TabIndex        =   4
         Text            =   "Key"
         ToolTipText     =   "Key of the bar you want to get the assigned decoration objects"
         Top             =   1215
         Width           =   840
      End
      Begin VB.CommandButton btnGetByBarKey 
         Caption         =   "GetByBarKey"
         Height          =   285
         Left            =   90
         TabIndex        =   3
         ToolTipText     =   "Assigns a decoration object to a background-bar"
         Top             =   1215
         Width           =   1515
      End
      Begin VB.TextBox txtBarKey 
         Height          =   285
         Index           =   5
         Left            =   5760
         TabIndex        =   2
         Text            =   "Key"
         ToolTipText     =   "Key of the background bar you want to get the assigned decoration objects"
         Top             =   1215
         Width           =   840
      End
      Begin VB.CommandButton btnGetByBkBarKey 
         Caption         =   "GetBkByBarKey"
         Height          =   285
         Left            =   4140
         TabIndex        =   1
         ToolTipText     =   "Assigns a decoration object to a background-bar"
         Top             =   1215
         Width           =   1515
      End
   End
End
Attribute VB_Name = "GanttDecoObjCfg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'[id(98)] BSTR DecorationObjectsGetByBarKey(BSTR BarKey);
'[id(99)] BSTR DecorationObjectsGetByBkBarKey(BSTR BkBarKey);

Private Sub btnCreate_Click()
    GanttConfig.Gantt.DecorationObjectCreate txtID(0).Text, txtLocationList.Text, txtPixelList.Text, txtColorList.Text
End Sub

Private Sub btnGetByBarKey_Click()
    Dim strTmp As String
    strTmp = GanttConfig.Gantt.DecorationObjectsGetByBarKey(txtBarKey(4).Text)
    MsgBox "Decoration-objects assigned to the bar with the key " & txtBarKey(4).Text & _
        vbCrLf & strTmp, vbInformation, "DecorationObjectGetByBarKey"
End Sub

Private Sub btnGetByBkBarKey_Click()
    Dim strTmp As String
    strTmp = GanttConfig.Gantt.DecorationObjectsGetByBkBarKey(txtBarKey(5).Text)
    MsgBox "Decoration-objects assigned to the bar with the key " & txtBarKey(5).Text & _
        vbCrLf & strTmp, vbInformation, "DecorationObjectGetByBkBarKey"
End Sub

Private Sub btnResetBar_Click()
    GanttConfig.Gantt.DecorationObjectResetBar txtID(3).Text, txtBarKey(2).Text
End Sub

Private Sub btnResetBkBar_Click()
    GanttConfig.Gantt.DecorationObjectResetBkBar txtID(4).Text, txtBarKey(3).Text
End Sub

Private Sub btnSetToBar_Click()
    GanttConfig.Gantt.DecorationObjectSetToBar txtID(1).Text, txtBarKey(0).Text
End Sub

Private Sub btnSetToBkBar_Click()
    GanttConfig.Gantt.DecorationObjectSetToBkBar txtID(2).Text, txtBarKey(1).Text
End Sub

Private Sub btnSetTimeMarker_Click()
    Dim olDate As Date

    olDate = CDate(txtTimeMarkerTime(0).Text)
    GanttConfig.Gantt.SetTimeMarker olDate, CLng(txtTimeMarkerColor.Text)
End Sub

Private Sub btnDeleteTimeMarker_Click()
    Dim olDate As Date

    olDate = CDate(txtTimeMarkerTime(1).Text)
    GanttConfig.Gantt.DeleteTimeMarker olDate
End Sub

Private Sub btnGetTimeMarker_Click()
    Dim olDate As Date

    olDate = CDate(txtTimeMarkerTime(2).Text)
    MsgBox CStr(GanttConfig.Gantt.GetTimeMarker(olDate)), vbInformation, "GetTimeMarker"
End Sub

Private Sub btnDeleteAllTimeMarkers_Click()
    GanttConfig.Gantt.DeleteAllTimeMarkers
End Sub

Private Sub chkEnableDrawTimeMarkerOfActiveBar_Click()
    If chkEnableDrawTimeMarkerOfActiveBar.Value = 1 Then
        GanttConfig.Gantt.EnableDrawTimeMarkerOfActiveBar = True
    Else
        GanttConfig.Gantt.EnableDrawTimeMarkerOfActiveBar = False
    End If
End Sub
'    dispidScrollDelay = 8L,
'    dispidScrollInset = 9L,
'    dispidScrollInterval = 10L,
'            [id(8)] long ScrollDelay;
'            [id(9)] long ScrollInset;
'            [id(10)] long ScrollInterval;
