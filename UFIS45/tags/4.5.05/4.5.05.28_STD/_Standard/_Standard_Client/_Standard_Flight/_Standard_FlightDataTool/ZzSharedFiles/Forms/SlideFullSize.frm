VERSION 5.00
Begin VB.Form SlideFullSize 
   BackColor       =   &H00000000&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   3090
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox UfisIntroFrame 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      ForeColor       =   &H80000008&
      Height          =   585
      Index           =   2
      Left            =   405
      ScaleHeight     =   555
      ScaleWidth      =   1290
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   330
      Visible         =   0   'False
      Width           =   1320
      Begin VB.Image UfisIntroPicture 
         Height          =   285
         Index           =   2
         Left            =   0
         Top             =   0
         Width           =   915
      End
   End
   Begin VB.PictureBox UfisIntroFrame 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      ForeColor       =   &H80000008&
      Height          =   585
      Index           =   1
      Left            =   195
      ScaleHeight     =   555
      ScaleWidth      =   1290
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   150
      Visible         =   0   'False
      Width           =   1320
      Begin VB.Image UfisIntroPicture 
         Height          =   285
         Index           =   1
         Left            =   0
         Top             =   0
         Width           =   915
      End
   End
   Begin VB.PictureBox UfisIntroFrame 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      ForeColor       =   &H80000008&
      Height          =   585
      Index           =   0
      Left            =   0
      ScaleHeight     =   555
      ScaleWidth      =   1290
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      Width           =   1320
      Begin VB.Image UfisIntroPicture 
         Height          =   285
         Index           =   0
         Left            =   0
         Top             =   0
         Width           =   915
      End
   End
End
Attribute VB_Name = "SlideFullSize"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Activate()
    MyDetailsIsOpen = True
End Sub

Private Sub Form_Click()
    MyDetailsIsOpen = False
    Me.Hide
End Sub

Private Sub Form_Load()
    Me.Top = 0
    Me.Left = 0
    Me.Width = MaxScreenWidth
    Me.Height = Screen.Height
    MyDetailsIsOpen = True
End Sub

Private Sub Form_Paint()
    MyDetailsIsOpen = True
End Sub

Private Sub UfisIntroFrame_Click(Index As Integer)
    MyDetailsIsOpen = False
    Me.Hide
End Sub

Private Sub UfisIntroPicture_Click(Index As Integer)
    MyDetailsIsOpen = False
    Me.Hide
End Sub
