VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.ocx"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form HiddenData 
   Caption         =   "Hidden Data and Configuration"
   ClientHeight    =   7515
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   12285
   Icon            =   "HiddenData.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7515
   ScaleWidth      =   12285
   StartUpPosition =   3  'Windows Default
   Begin TABLib.TAB TableDataTab 
      Height          =   1575
      Index           =   0
      Left            =   60
      TabIndex        =   5
      Top             =   3690
      Width           =   3015
      _Version        =   65536
      _ExtentX        =   5318
      _ExtentY        =   2778
      _StockProps     =   64
   End
   Begin TABLib.TAB FormCfgTab 
      Height          =   1575
      Left            =   60
      TabIndex        =   4
      Top             =   420
      Width           =   3465
      _Version        =   65536
      _ExtentX        =   6112
      _ExtentY        =   2778
      _StockProps     =   64
   End
   Begin TABLib.TAB BasicDataTab 
      Height          =   1545
      Index           =   0
      Left            =   60
      TabIndex        =   3
      Top             =   2070
      Width           =   3015
      _Version        =   65536
      _ExtentX        =   5318
      _ExtentY        =   2725
      _StockProps     =   64
   End
   Begin TABLib.TAB TabCfgTab 
      Height          =   1575
      Left            =   3600
      TabIndex        =   2
      Top             =   420
      Width           =   4305
      _Version        =   65536
      _ExtentX        =   7594
      _ExtentY        =   2778
      _StockProps     =   64
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   1
      Top             =   7200
      Width           =   12285
      _ExtentX        =   21669
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   21140
         EndProperty
      EndProperty
   End
   Begin VB.CheckBox chkClose 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   30
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   30
      Width           =   855
   End
   Begin TABLib.TAB FilterTab 
      Height          =   1575
      Index           =   0
      Left            =   60
      TabIndex        =   6
      Top             =   5340
      Width           =   3015
      _Version        =   65536
      _ExtentX        =   5318
      _ExtentY        =   2778
      _StockProps     =   64
   End
End
Attribute VB_Name = "HiddenData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim TabMaximized As Boolean
Dim CurMaxTab As TABLib.Tab

Public Function CheckTabConfig(TabCfgLine As String) As String
    Dim Result As String
    Dim KeyValue As String
    Dim LineNo As Long
    Result = ""
    KeyValue = GetRealItem(TabCfgLine, 0, Chr(15))
    If KeyValue <> "" Then
        TabCfgTab.IndexCreate "KEYCODE", 0
        TabCfgTab.SetInternalLineBuffer True
        LineNo = Val(TabCfgTab.GetLinesByIndexValue("KEYCODE", KeyValue, 0))
        If LineNo > 0 Then
            If LineNo = 1 Then
                LineNo = TabCfgTab.GetNextResultLine
                TabCfgTab.UpdateTextLine LineNo, TabCfgLine, False
            Else
                MsgBox "TAB_KEY [" & KeyValue & "] not unique !!"
            End If
        Else
            TabCfgTab.InsertTextLine TabCfgLine, False
        End If
        'TabCfgTab.AutoSizeColumns
        'TabCfgTab.Refresh
        TabCfgTab.SetInternalLineBuffer False
    End If
    CheckTabConfig = Result
End Function

Public Function UpdateTabConfig(TabCode As String, FieldList As String, ValueList As String) As String
    Dim Result As String
    Dim LineNo As Long
    Dim ColNo As Long
    Dim tmpLine As String
    Dim tmpFld As String
    Dim tmpVal As String
    Dim tmpList As String
    Dim i As Integer
    Result = ""
    If TabCode <> "" Then
        TabCfgTab.IndexCreate "KEYCODE", 0
        TabCfgTab.SetInternalLineBuffer True
        LineNo = Val(TabCfgTab.GetLinesByIndexValue("KEYCODE", TabCode, 0))
        If LineNo > 0 Then
            If LineNo = 1 Then
                LineNo = TabCfgTab.GetNextResultLine
                TabCfgTab.SetFieldValues LineNo, FieldList, ValueList
            Else
                MsgBox "TAB_KEY [" & TabCode & "] Found=" & CStr(LineNo) & " !!"
            End If
            TabCfgTab.SetInternalLineBuffer False
        Else
            TabCfgTab.SetInternalLineBuffer False
            tmpLine = CreateEmptyLine(TabCfgTab.LogicalFieldList)
            tmpLine = Replace(tmpLine, ",", Chr(15), 1, -1, vbBinaryCompare)
            TabCfgTab.InsertTextLine tmpLine, False
            LineNo = TabCfgTab.GetLineCount - 1
            'Due to Macke in tab.ocx
            tmpFld = "START"
            i = 0
            While tmpFld <> ""
                i = i + 1
                tmpFld = GetItem(FieldList, i, Chr(15))
                tmpVal = GetItem(ValueList, i, Chr(15))
                If tmpFld <> "" Then
                    ColNo = CLng(GetRealItemNo(TabCfgTab.LogicalFieldList, tmpFld))
                    If ColNo >= 0 Then TabCfgTab.SetColumnValue LineNo, ColNo, tmpVal
                End If
            Wend
            'tmpList = Replace(FieldList, Chr(15), ",", 1, -1, vbBinaryCompare)
            'TabCfgTab.SetFieldValues LineNo, tmpList, ValueList
        End If
        'TabCfgTab.AutoSizeColumns
        'TabCfgTab.Refresh
    End If
    UpdateTabConfig = Result
End Function

Public Sub RemoveImpConfig()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpType As String
    MaxLine = TabCfgTab.GetLineCount - 1
    While CurLine <= MaxLine
        tmpType = TabCfgTab.GetFieldValue(CurLine, "TYPE")
        If InStr("IMP,SEL,DAT", tmpType) > 0 Then
            TabCfgTab.DeleteLine CurLine
            MaxLine = MaxLine - 1
            CurLine = CurLine - 1
        End If
        CurLine = CurLine + 1
    Wend
End Sub

Public Function CreateFilterTab(Index As Integer) As String
    Dim TabCfgLine As String
    If Index > FilterTab.UBound Then
        Load FilterTab(Index)
        FilterTab(Index).Visible = True
        FilterTab(Index).Left = (FilterTab(Index).Width + 60) * Index + 120
    End If
    FilterTab(Index).ResetContent
    FilterTab(Index).LogicalFieldList = ""
    FilterTab(Index).HeaderString = "Data" & Space(200)
    FilterTab(Index).HeaderLengthString = "10"
    FilterTab(Index).ColumnWidthString = "10"
    FilterTab(Index).SetMainHeaderValues "1", "Filter TAB Data", ""
    FilterTab(Index).MainHeader = True
    FilterTab(Index).EnableHeaderSizing True
    FilterTab(Index).AutoSizeByHeader = True
    FilterTab(Index).AutoSizeColumns
    FilterTab(Index).myName = "HIDDEN_FILTER_" & CStr(Index)
    TabCfgLine = FilterTab(Index).myName & Chr(15)
    TabCfgLine = TabCfgLine & "0" & Chr(15)
    TabCfgLine = TabCfgLine & "V" & Chr(15)
    TabCfgLine = TabCfgLine & "DAT" & Chr(15)
    TabCfgLine = TabCfgLine & "AODB" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & FilterTab(Index).LogicalFieldList & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    CheckTabConfig TabCfgLine
    CreateFilterTab = FilterTab(Index).myName
End Function

Public Function CreateBasicDataTab(Index As Integer, TableName As String, TableFields As String, SqlKey) As String
    Dim TabCfgLine As String
    If Index > BasicDataTab.UBound Then
        Load BasicDataTab(Index)
        BasicDataTab(Index).Visible = True
        BasicDataTab(Index).Left = (BasicDataTab(Index).Width + 60) * Index + 120
    End If
    BasicDataTab(Index).ResetContent
    BasicDataTab(Index).LogicalFieldList = TableFields
    BasicDataTab(Index).HeaderString = TableFields
    BasicDataTab(Index).HeaderLengthString = "10,10,10"
    BasicDataTab(Index).ColumnWidthString = "10,10,10"
    BasicDataTab(Index).SetMainHeaderValues "3", TableName & " Basic Data", ""
    BasicDataTab(Index).MainHeader = True
    BasicDataTab(Index).EnableHeaderSizing True
    BasicDataTab(Index).AutoSizeByHeader = True
    BasicDataTab(Index).AutoSizeColumns
    BasicDataTab(Index).myName = "BASIC_" & TableName
    TabCfgLine = BasicDataTab(Index).myName & Chr(15)
    TabCfgLine = TabCfgLine & "0" & Chr(15)
    TabCfgLine = TabCfgLine & "V" & Chr(15)
    TabCfgLine = TabCfgLine & "BAS" & Chr(15)
    TabCfgLine = TabCfgLine & "AODB" & Chr(15)
    TabCfgLine = TabCfgLine & TableName & Chr(15)
    TabCfgLine = TabCfgLine & TableFields & Chr(15)
    TabCfgLine = TabCfgLine & SqlKey & Chr(15)
    TabCfgLine = TabCfgLine & BasicDataTab(Index).LogicalFieldList & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    CheckTabConfig TabCfgLine
    CreateBasicDataTab = BasicDataTab(Index).myName
End Function
Public Function GetConfigValues(FilterTabCode As String, CfgFields As String) As String
    Dim Result As String
    Dim TabNo As Long
    Result = ""
    TabNo = GetCfgTabLineNo(FilterTabCode)
    If TabNo >= 0 Then
        Result = TabCfgTab.GetFieldValues(TabNo, CfgFields)
    End If
    GetConfigValues = Result
End Function
Public Function GetFilterData(FilterTabCode As String, BufferFields As String, PatchCodes As String, PatchValues As String) As String
    Dim Result As String
    Dim DataTab As String
    Dim TableName As String
    Dim FldLst As String
    Dim LookField As String
    Dim GetField As String
    Dim LookValue As String
    Dim GetValue As String
    Dim TabNo As Long
    Dim LineNo As Long
    Dim CurLine As Long
    Dim i As Integer
    Result = ""
    TabNo = GetCfgTabLineNo(FilterTabCode)
    If TabNo >= 0 Then
        DataTab = TabCfgTab.GetFieldValue(TabNo, "DSRC")
        For i = 0 To FilterTab.UBound
            If FilterTab(i).myName = DataTab Then
                ReadAodbData FilterTab(i), True, "0", PatchCodes, PatchValues, True
                LineNo = FilterTab(i).GetLineCount - 1
                If LineNo >= 0 Then
                    FldLst = FilterTab(i).LogicalFieldList
                    LookField = GetRealItem(FldLst, 0, ",")
                    GetField = GetRealItem(FldLst, 1, ",")
                    Select Case LookField
                        Case "ALC3"
                            TableName = "ALTTAB"
                        Case "APC3"
                            TableName = "APTTAB"
                        Case "ACT3"
                            TableName = "ACTTAB"
                        Case Else
                            TableName = ""
                    End Select
                    If TableName <> "" Then
                        For CurLine = 0 To LineNo
                            LookValue = FilterTab(i).GetColumnValue(CurLine, 0)
                            GetValue = BasicDataLookUp(TableName, LookField, LookValue, GetField, True)
                            FilterTab(i).SetColumnValue CurLine, 1, GetValue
                        Next
                    Else
                        If LookField = "FLNO" Then
                            For CurLine = 0 To LineNo
                                LookValue = FilterTab(i).GetColumnValue(CurLine, 0)
                                LookValue = Trim(Left(LookValue, 3))
                                If Len(LookValue) = 2 Then
                                    GetValue = BasicDataLookUp("ALTTAB", "ALC2", LookValue, "ALC3", True)
                                Else
                                    GetValue = BasicDataLookUp("ALTTAB", "ALC3", LookValue, "ALC2", True)
                                End If
                                FilterTab(i).SetColumnValue CurLine, 1, GetValue
                            Next
                        End If
                    End If
                    Result = FilterTab(i).GetBufferByFieldList(0, LineNo, BufferFields, vbLf)
                End If
                Exit For
            End If
        Next
    End If
    GetFilterData = Result
End Function

Public Sub ReadAodbData(CurTab As TABLib.Tab, SetHeader As Boolean, UniqueColumns As String, PatchCodes As String, PatchValues As String, AppendUnknown As Boolean)
    Dim TblNam As String
    Dim CurFldLst As String
    Dim FldLst As String
    Dim LogFld As String
    Dim CurSqlKey As String
    Dim AddSqlKey As String
    Dim UseSqlKey As String
    Dim SqlKey As String
    Dim PatchName As String
    Dim CheckPatch As String
    Dim PatchData As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim tmpZone As String
    Dim tmpTifr As String
    Dim tmpTito As String
    Dim LineNo As Long
    Dim ItemNo As Long
    Dim SqlItemNo As Long
    Dim ReadDays As Long
    Dim SigIdx As Integer
    Dim FirstDay
    Dim LastDay
    Dim ReadFrom
    Dim ReadTo

    CurTab.ResetContent
    LineNo = GetCfgTabLineNo(CurTab.myName)
    If LineNo >= 0 Then
        TblNam = TabCfgTab.GetFieldValue(LineNo, "DTAB")
        FldLst = TabCfgTab.GetFieldValue(LineNo, "DFLD")
        LogFld = TabCfgTab.GetFieldValue(LineNo, "LFLD")
        If LogFld = "" Then LogFld = FldLst
        If FldLst <> "" Then
            SigIdx = 1
            Screen.MousePointer = 11
            CurTab.ResetContent
            If SetHeader Then
                CurTab.HeaderString = LogFld
                CurTab.LogicalFieldList = LogFld
            End If
            'CurTab.Refresh
            CurTab.CedaCurrentApplication = UfisServer.ModName & "," & UfisServer.GetApplVersion(True)
            CurTab.CedaHopo = UfisServer.HOPO
            CurTab.CedaIdentifier = "IDX"
            CurTab.CedaPort = "3357"
            CurTab.CedaReceiveTimeout = "250"
            CurTab.CedaRecordSeparator = vbLf
            CurTab.CedaSendTimeout = "250"
            CurTab.CedaServerName = UfisServer.HostName
            CurTab.CedaTabext = UfisServer.TblExt
            CurTab.CedaUser = UfisServer.ModName
            CurTab.CedaWorkstation = UfisServer.GetMyWorkStationName
            CurTab.CedaPacketSize = 500
            CurTab.SetUniqueFields UniqueColumns
            
            If ServerIsAvailable Then
                SqlKey = TabCfgTab.GetFieldValue(LineNo, "DSQL")
                AddSqlKey = ""
                SqlItemNo = 0
                CurFldLst = GetRealItem(FldLst, SqlItemNo, Chr(14))
                While CurFldLst <> ""
                    CurSqlKey = GetRealItem(SqlKey, SqlItemNo, Chr(14))
                    tmpVpfr = ""
                    tmpVpto = ""
                    ItemNo = 0
                    PatchName = GetRealItem(PatchCodes, ItemNo, Chr(15))
                    While PatchName <> ""
                        CheckPatch = "[" & PatchName & "]"
                        PatchData = GetRealItem(PatchValues, ItemNo, Chr(15))
                        Select Case PatchName
                            Case "ZONE"
                                tmpZone = PatchData
                            Case "VPFR"
                                tmpVpfr = PatchData
                            Case "VPTO"
                                tmpVpto = PatchData
                            'Case "TIFR"
                            '    tmpTifr = PatchData
                            'Case "TITO"
                            '    tmpTito = PatchData
                            Case Else
                                If AppendUnknown Then
                                    If InStr(CurSqlKey, CheckPatch) = 0 Then
                                        Select Case PatchName
                                            Case "APC3"
                                                AddSqlKey = AddSqlKey & " AND (ORG3 IN (" & PatchData & ") OR DES3 IN (" & PatchData & "))"
                                            Case "HOPO", "TIFR", "TITO"
                                                'Do not append these
                                            Case Else
                                                AddSqlKey = AddSqlKey & " AND " & PatchName & " IN (" & PatchData & ")"
                                        End Select
                                    End If
                                End If
                                CurSqlKey = Replace(CurSqlKey, CheckPatch, PatchData, 1, -1, vbBinaryCompare)
                        End Select
                        ItemNo = ItemNo + 1
                        PatchName = GetRealItem(PatchCodes, ItemNo, Chr(15))
                    Wend
                    CheckPatch = "[FILTER]"
                    If InStr(CurSqlKey, CheckPatch) > 0 Then
                        CurSqlKey = Replace(CurSqlKey, CheckPatch, AddSqlKey, 1, -1, vbBinaryCompare)
                        AddSqlKey = ""
                    End If
                    If CurSqlKey <> "" Then CurSqlKey = "WHERE " & CurSqlKey
                    If AddSqlKey <> "" Then
                        CurSqlKey = CurSqlKey & AddSqlKey
                        AddSqlKey = ""
                    End If
                    If tmpVpfr = "" Then
                        MainDialog.AdjustServerSignals True, SigIdx
                        CurTab.CedaAction "RTA", TblNam, CurFldLst, "", CurSqlKey
                        MainDialog.AdjustServerSignals False, SigIdx
                    Else
                        FirstDay = CedaDateToVb(tmpVpfr)
                        LastDay = CedaDateToVb(tmpVpto)
                        ReadDays = 4
                        ReadFrom = FirstDay
                        Do
                            ReadTo = DateAdd("d", (ReadDays - 1), ReadFrom)
                            If ReadTo > LastDay Then ReadTo = LastDay
                            Mid(tmpVpfr, 1, 8) = Format(ReadFrom, "yyyymmdd")
                            Mid(tmpVpto, 1, 8) = Format(ReadTo, "yyyymmdd")
                            UseSqlKey = Replace(CurSqlKey, "[VPFR]", tmpVpfr, 1, -1, vbBinaryCompare)
                            UseSqlKey = Replace(UseSqlKey, "[VPTO]", tmpVpto, 1, -1, vbBinaryCompare)
                            MainDialog.AdjustServerSignals True, SigIdx
                            DoEvents
                            CurTab.CedaAction "RTA", TblNam, CurFldLst, "", UseSqlKey
                            MainDialog.AdjustServerSignals False, SigIdx
                            DoEvents
                            SigIdx = SigIdx + 1
                            If SigIdx > 2 Then SigIdx = 1
                            ReadFrom = DateAdd("d", 1, ReadTo)
                            If StopAllLoops Then ReadFrom = DateAdd("d", 1, LastDay)
                        Loop While (ReadFrom <= LastDay) ' And (Not CedaError)
                    End If
                    SqlItemNo = SqlItemNo + 1
                    CurFldLst = GetRealItem(FldLst, SqlItemNo, Chr(14))
                    CurSqlKey = GetRealItem(SqlKey, SqlItemNo, Chr(14))
                    If StopAllLoops Then CurFldLst = ""
                    DoEvents
                Wend
            Else
                'If HostName = "LOCAL" Then CurTab.ReadFromFile "c:\ufis\basicdata" & Trim(Str(Index)) & ".txt"
            End If
            'CurTab.AutoSizeColumns
            'CurTab.Refresh
            Screen.MousePointer = 0
        End If
    End If
End Sub

Private Function GetCfgTabLineNo(TabCode As String) As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim LineNo As Long
    Dim tmpCode As String
    LineNo = -1
    'MaxLine = TabCfgTab.GetLineCount - 1
    'CurLine = 0
    'While (CurLine <= MaxLine) And (LineNo < 0)
    '    tmpCode = TabCfgTab.GetColumnValue(CurLine, 0)
    '    If tmpCode = TabCode Then LineNo = CurLine
    '    CurLine = CurLine + 1
    'Wend
    
    If TabCode <> "" Then
        'Just to be safe
        TabCfgTab.IndexCreate "KEYCODE", 0
        TabCfgTab.SetInternalLineBuffer True
        LineNo = Val(TabCfgTab.GetLinesByIndexValue("KEYCODE", TabCode, 0))
        If LineNo = 1 Then
            LineNo = TabCfgTab.GetNextResultLine
        Else
            LineNo = -1
        End If
        TabCfgTab.SetInternalLineBuffer False
    End If
    
    GetCfgTabLineNo = LineNo
End Function
Private Sub InitMyForm()
    Dim TabCfgLine As String
    TabCfgTab.ResetContent
    TabCfgTab.LogicalFieldList = "CODE,FIDX,STAT,TYPE,DSRC,DTAB,DFLD,DSQL,LFLD,CAPT,CODE,CALL,MASK,EDIT,MAPP,NAME,SORT,SYNC,REMA"
    'TabCfgTab.HeaderString = "Unique Key,FX,S,Typ,Data Source,Table,Table Fields,SQL Selection,Logical Fields,Caption,Action,Called,Edit Mask,Edit Fields,Mapped Fields,Name,Synch.Cursor,Remark"
    TabCfgTab.HeaderString = "CODE,FIDX,STAT,TYPE,DSRC,DTAB,DFLD,DSQL,LFLD,CAPT,CODE,CALL,MASK,EDIT,MAPP,NAME,SORT,SYNC,REMA"
    TabCfgTab.HeaderLengthString = "10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10"
    TabCfgTab.ColumnWidthString = "20,2,1,3,20,6,250,250,250,20,10,10,40,20,20,10,10,10,10"
    TabCfgTab.SetMainHeaderValues "18", "Data TAB Configuration", ""
    TabCfgTab.MainHeader = True
    TabCfgTab.SetFieldSeparator Chr(15)
    TabCfgTab.ShowHorzScroller True
    TabCfgTab.EnableHeaderSizing True
    TabCfgTab.AutoSizeByHeader = True
    TabCfgTab.AutoSizeColumns
    TabCfgTab.myName = "HIDDEN_TABCFG"
    TabCfgLine = TabCfgTab.myName & Chr(15)
    TabCfgLine = TabCfgLine & "0" & Chr(15)
    TabCfgLine = TabCfgLine & "V" & Chr(15)
    TabCfgLine = TabCfgLine & "CFG" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & TabCfgTab.LogicalFieldList & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    CheckTabConfig TabCfgLine
    
    CreateFilterTab 0
    StatusBar1.ZOrder
End Sub

Private Sub chkClose_Click()
    If chkClose.Value = 1 Then
        Me.Hide
        chkClose.Value = 0
    End If
End Sub

Private Sub Form_Load()
    InitMyForm
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Me.Hide
        Cancel = True
    End If
End Sub

Private Sub Form_Resize()
    If TabMaximized Then
        CurMaxTab.Width = Me.ScaleWidth
        If Me.ScaleHeight > 1500 Then
            CurMaxTab.Height = Me.ScaleHeight - StatusBar1.Height - CurMaxTab.Top
        End If
    End If
End Sub

Private Sub TabCfgTab_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim tmpTag As String
    If LineNo >= 0 Then
    ElseIf LineNo = -1 Then
        TabCfgTab.Sort CStr(ColNo), True, True
        TabCfgTab.Refresh
    Else
        If TabMaximized Then
            tmpTag = TabCfgTab.Tag
            TabCfgTab.Left = Val(GetRealItem(tmpTag, 0, ","))
            TabCfgTab.Top = Val(GetRealItem(tmpTag, 1, ","))
            TabCfgTab.Width = Val(GetRealItem(tmpTag, 2, ","))
            TabCfgTab.Height = Val(GetRealItem(tmpTag, 3, ","))
            TabMaximized = False
        Else
            tmpTag = ""
            tmpTag = tmpTag & CStr(TabCfgTab.Left) & ","
            tmpTag = tmpTag & CStr(TabCfgTab.Top) & ","
            tmpTag = tmpTag & CStr(TabCfgTab.Width) & ","
            tmpTag = tmpTag & CStr(TabCfgTab.Height)
            TabCfgTab.Tag = tmpTag
            TabCfgTab.Top = chkClose.Top + chkClose.Height + 60
            TabCfgTab.Left = 0
            TabCfgTab.ZOrder
            TabCfgTab.AutoSizeColumns
            TabCfgTab.Refresh
            Set CurMaxTab = TabCfgTab
            TabMaximized = True
            Form_Resize
        End If
    End If
End Sub

Public Sub InitBasicData(Index As Integer, TableName As String, TableFields As String, SqlKey As String)
    Dim HiddenTab As String
    Dim tmpFldLst As String
    Dim tmpIdxName As String
    Dim i As Long
    HiddenTab = CreateBasicDataTab(Index, TableName, TableFields, SqlKey)
    ReadAodbData BasicDataTab(Index), True, "", "", "", False
    tmpFldLst = BasicDataTab(Index).LogicalFieldList
    
    For i = 0 To 1
        tmpIdxName = GetRealItem(tmpFldLst, i, ",")
        BasicDataTab(Index).IndexCreate tmpIdxName, i
    Next
End Sub
Public Function BasicDataLookUp(TableName As String, LookField As String, LookValue As String, GetField As String, FirstHit As Boolean) As String
    Dim Result As String
    Dim TabName As String
    Dim LineNo As Long
    Dim i As Integer
    Result = ""
    If LookValue <> "" Then
        TabName = "BASIC_" & TableName
        For i = 0 To BasicDataTab.UBound
            If BasicDataTab(i).myName = TabName Then
                BasicDataTab(i).SetInternalLineBuffer True
                LineNo = Val(BasicDataTab(i).GetLinesByIndexValue(LookField, LookValue, 0))
                While LineNo >= 0
                    LineNo = BasicDataTab(i).GetNextResultLine
                    If LineNo >= 0 Then Result = Result & BasicDataTab(i).GetFieldValues(LineNo, GetField) & vbLf
                Wend
                BasicDataTab(i).SetInternalLineBuffer False
                Exit For
            End If
        Next
        If Len(Result) > 0 Then Result = Left(Result, Len(Result) - 1)
        If FirstHit Then Result = GetRealItem(Result, 0, vbLf)
    End If
    BasicDataLookUp = Result
End Function

Public Sub CreateConfigEntry(Fields As String, Values As String)
    Dim tmpLine As String
    Dim LineNo As Long
    Dim ColNo As Long
    Dim i As Integer
    Dim itmField As String
    Dim itmValue As String
    tmpLine = CreateEmptyLine(TabCfgTab.LogicalFieldList)
    tmpLine = Replace(tmpLine, ",", Chr(15), 1, -1, vbBinaryCompare)
    TabCfgTab.InsertTextLine tmpLine, False
    LineNo = TabCfgTab.GetLineCount - 1
    i = 0
    itmField = "START"
    While itmField <> ""
        i = i + 1
        itmField = GetItem(Fields, i, Chr(15))
        If itmField <> "" Then
            itmValue = GetItem(Values, i, Chr(15))
            ColNo = CLng(GetItemNo(TabCfgTab.LogicalFieldList, itmField) - 1)
            TabCfgTab.SetColumnValue LineNo, ColNo, itmValue
        End If
    Wend
    TabCfgTab.RedrawTab
End Sub
