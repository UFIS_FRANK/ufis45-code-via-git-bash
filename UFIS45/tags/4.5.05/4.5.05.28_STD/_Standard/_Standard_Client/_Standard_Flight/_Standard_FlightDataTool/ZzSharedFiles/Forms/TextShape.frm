VERSION 5.00
Begin VB.Form TextShape 
   AutoRedraw      =   -1  'True
   BackColor       =   &H00FFFFFF&
   ClientHeight    =   840
   ClientLeft      =   60
   ClientTop       =   60
   ClientWidth     =   12510
   ControlBox      =   0   'False
   FillColor       =   &H0080FFFF&
   FillStyle       =   0  'Solid
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   36
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H0080FF80&
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MouseIcon       =   "TextShape.frx":0000
   MousePointer    =   99  'Custom
   Picture         =   "TextShape.frx":0152
   ScaleHeight     =   840
   ScaleWidth      =   12510
   ShowInTaskbar   =   0   'False
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   780
      Top             =   360
   End
   Begin VB.Image Image1 
      Height          =   705
      Left            =   30
      Top             =   30
      Width           =   765
   End
End
Attribute VB_Name = "TextShape"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function BeginPath Lib "gdi32" (ByVal hDC As Long) As Long
Private Declare Function EndPath Lib "gdi32" (ByVal hDC As Long) As Long
Private Declare Function PathToRegion Lib "gdi32" (ByVal hDC As Long) As Long
Private Declare Function SetWindowRgn Lib "user32" (ByVal hWnd As Long, ByVal hRgn As Long, ByVal bRedraw As Boolean) As Long
Private Declare Function CreateEllipticRgn Lib "gdi32" (ByVal x1 As Long, ByVal y1 As Long, ByVal X2 As Long, ByVal Y2 As Long) As Long
Private Declare Function CombineRgn Lib "gdi32" (ByVal hDestRgn As Long, ByVal hSrcRgn1 As Long, ByVal hSrcRgn2 As Long, ByVal nCombineMode As Long) As Long

Private Declare Function CreateFont Lib "gdi32" Alias "CreateFontA" (ByVal h As Long, ByVal W As Long, ByVal e As Long, ByVal O As Long, ByVal W As Long, ByVal i As Long, ByVal u As Long, ByVal s As Long, ByVal c As Long, ByVal OP As Long, ByVal CP As Long, ByVal Q As Long, ByVal PAF As Long, ByVal f As String) As Long
Private Declare Function SelectObject Lib "gdi32" (ByVal hDC As Long, ByVal hObject As Long) As Long
Private Declare Function DeleteObject Lib "gdi32" (ByVal hObject As Long) As Long

Private Const FW_BOLD = 700
Dim NotifyText As String
Dim NotifyTip As String
Dim TxtColor1 As Long
Dim TxtColor2 As Long

Public Sub SetNotificationText(UseText As String, UseToolTip As String, UseColor1 As Long, UseColor2 As Long)
    NotifyText = UseText
    NotifyTip = UseToolTip
    TxtColor1 = UseColor1
    TxtColor2 = UseColor2
End Sub
Private Sub ShapePicture()
Const RGN_OR = 2
Dim Text1 As String
If NotifyText = "" Then NotifyText = "A/C NOTIFICATION"
Text1 = NotifyText

Dim new_font As Long
Dim old_font As Long
Dim hRgn As Long
Dim new_rgn As Long

    ' Prepare the form.
    AutoRedraw = True
    BorderStyle = vbBSNone
    ScaleMode = vbPixels
    Me.BackColor = TxtColor1
    
    
    ' Make a big font.
    new_font = CustomFont(36, 20, 0, 0, FW_BOLD, False, True, False, "Arial")
    Me.Font = new_font
    Me.Width = (TextWidth(Text1) + 10) * 15
    old_font = SelectObject(Me.hDC, new_font)
    ' Make the region.
    SelectObject Me.hDC, new_font
    Me.ForeColor = vbBlack
    BeginPath Me.hDC
    Me.CurrentX = 60
    Me.CurrentY = 6
    Me.Print Text1
    EndPath Me.hDC
    hRgn = PathToRegion(Me.hDC)
    
    'Me.BackColor = vbBlack
    'old_font = SelectObject(Me.hDC, new_font)
    'BeginPath Me.hDC
    'Me.CurrentX = 59
    'Me.CurrentY = 8
    'Me.Print Text1
    'EndPath Me.hDC
    'new_rgn = PathToRegion(Me.hDC)
    'CombineRgn hRgn, hRgn, new_rgn, RGN_OR
    'DeleteObject new_rgn
    
    new_rgn = CreateEllipticRgn(6, 4, 56, 54)
    CombineRgn hRgn, hRgn, new_rgn, RGN_OR
    DeleteObject new_rgn
    
    
    
    ' Constrain the form to the region.
    SetWindowRgn Me.hWnd, hRgn, False

    ' Restore the original font.
    SelectObject hDC, old_font

    ' Free font resources (important!)
    DeleteObject new_font

    ' Draw text in the PictureBox.
    'With Me.Font
    '    .Name = "Arial"
    '    .Size = 8
    '    .Bold = True
    'End With
End Sub
' Make a customized font and return its handle.
Private Function CustomFont(ByVal hgt As Long, ByVal wid As Long, ByVal escapement As Long, ByVal orientation As Long, ByVal wgt As Long, ByVal is_italic As Long, ByVal is_underscored As Long, ByVal is_striken_out As Long, ByVal face As String) As Long
    Const CLIP_LH_ANGLES = 16   ' Needed for tilted fonts.
    CustomFont = CreateFont(hgt, wid, escapement, orientation, wgt, is_italic, is_underscored, is_striken_out, 0, 0, CLIP_LH_ANGLES, 0, 0, face)
End Function


Private Sub Form_Load()
    ' Shape the picture.
    ShapePicture
    Me.Top = Screen.Height - Me.Height - 300
    Me.Left = MaxScreenWidth - Me.Width - 210
    Image1.ToolTipText = NotifyTip
    Timer1.Tag = "SHOW"
    Timer1.Enabled = True
End Sub

Private Sub Form_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    GetCapUserMenu
End Sub
Private Sub GetCapUserMenu()
    Dim RECT As POINTAPI
    Dim retval As Integer
    Dim Msg As String
    
    If ApplShortCode <> "CAP" Then
        MainDialog.Show
        MainDialog.WindowState = vbNormal
    Else
        Msg = Image1.ToolTipText & vbNewLine & vbNewLine
        Msg = Msg & "Please select ..."
        retval = MyMsgBox.CallAskUser(0, 0, 0, "Network Error Handling", Msg, "netfail", "Login,Exit,Reboot", UserAnswer)
        Select Case retval
            Case 1
                If AllowOpenCappuccino Then
                    MainDialog.HandleMyApplReLogin "INIT"
                Else
                    GetCursorPos RECT
                    MyMsgPosX = CLng(RECT.X) * 15
                    If MyMsgBox.CallAskUser(0, MyMsgPosX, 0, "FDT Capture", "Access denied.", "stop", "", UserAnswer) > 0 Then DoNothing
                    MyMsgPosX = 0
                End If
            Case 2
                GetCursorPos RECT
                MyMsgPosX = CLng(RECT.X) * 15
                MainDialog.MainTimer(3).Tag = "EXIT"
                MainDialog.MainTimer(3).Enabled = True
            Case 3
                GetCursorPos RECT
                MyMsgPosX = CLng(RECT.X) * 15
                If MyMsgBox.CallAskUser(0, MyMsgPosX, 0, "Reboot WKS", "Access denied.", "stop", "", UserAnswer) > 0 Then DoNothing
                MyMsgPosX = 0
            Case Else
        End Select
    End If
End Sub
Private Sub Form_Unload(Cancel As Integer)
    Dim tmpData As String
    'UfisServer.CallCeda tmpData, "SBC", "PDETAB/TRG", "F", "D", "W", "", 0, False, False
End Sub

Private Sub Image1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    GetCapUserMenu
End Sub

Private Sub Timer1_Timer()
    Dim tmpTag As String
    tmpTag = Timer1.Tag
    Select Case tmpTag
        Case "SHOW"
            Me.Show
            Timer1.Tag = "TOP"
        Case "TOP"
            SetFormOnTop Me, True
            If Me.BackColor = TxtColor1 Then
                Me.BackColor = TxtColor2
            Else
                Me.BackColor = TxtColor1
            End If
            If ApplShortCode = "CAP" Then
                Timer1.Enabled = False
            End If
        Case Else
    End Select
End Sub

Public Sub ShapeMyForm(ForWhat As String, CurForm As Form, CurImage As Image)
    Dim TopDiff As Long
    Dim lftDiff As Long
    Dim rgn As Long
    Dim Top As Single
    Dim lft As Single
    Dim wid As Single
    Dim hgt As Single
    Dim UseTop As Long
    Dim UseLeft As Long
    Dim UseHeight As Long
    Dim UseWidth As Long
    Select Case ForWhat
        Case "INTRO"
            lftDiff = (CurForm.Width - CurForm.ScaleWidth) / 2
            TopDiff = (CurForm.Height - CurForm.ScaleHeight) - lftDiff
            UseTop = CurImage.Top + TopDiff - 15
            UseLeft = CurImage.Left + lftDiff - 15
            UseLeft = UseLeft
            UseTop = UseTop
            UseWidth = CurImage.Width
            UseHeight = CurImage.Height
            Top = (UseTop / 15) + 2
            lft = UseLeft / 15
            wid = UseWidth / 15
            hgt = (UseHeight / 15) - 3
            rgn = CreateEllipticRgn(lft, Top, lft + wid, Top + hgt)
            SetWindowRgn CurForm.hWnd, rgn, True
            DeleteObject rgn
        Case Else
    End Select
End Sub

