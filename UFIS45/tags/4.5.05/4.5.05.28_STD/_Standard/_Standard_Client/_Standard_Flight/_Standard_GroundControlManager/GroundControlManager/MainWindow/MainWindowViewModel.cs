﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.MVVM.ViewModel;
using Ufis.Utilities;
using Ufis.MVVM.Command;
using Ufis.Data;
using Ufis.Entities;
using System.Collections.ObjectModel;
using System.Windows.Data;
using DevExpress.Utils;
using DevExpress.Xpf.Editors.Settings;
using DevExpress.Xpf.Core;
using System.ComponentModel;
using Ufis.Flight.Filter;
using Ufis.Data.Ceda;
using GroundControlManager.DataAccess;
using GroundControlManager.Helpers;

namespace GroundControlManager.MainWindow
{
    /// <summary>
    /// The ViewModel for the application's main window.
    /// </summary>
    public class MainWindowViewModel : MultiWorkspaceViewModel
    {
        private EntityCollectionBase<EntDbFlight> _flights;
        private ICollectionView _itemsSource;
        private FlightFilterViewModel _flightFilterViewModel;
        private readonly FlightFilter _flightFilter;
        private bool _isLoading;

        #region Public Properties

        public EntityCollectionBase<EntDbFlight> Flights
        {
            get
            {
                return _flights;
            }
            set
            {
                if (_flights == value)
                    return;

                if (_flights != null)
                    _flights.Close();

                _flights = value;
                OnPropertyChanged("Flights");

                ItemsSource = new ListCollectionView(_flights)
                {
                    Filter = _flightFilter.FilterItem
                };
            }
        }

        public ICollectionView ItemsSource { 
            get
            {
                return _itemsSource;
            }
            set
            {
                if (_itemsSource == value)
                    return;

                _itemsSource = value;
                OnPropertyChanged("ItemsSource");
            }
        }

        public ObservableCollection<UfisGridColumn> Columns { get; set; }

        public FlightFilterViewModel FlightFilter
        {
            get
            {
                if (_flightFilterViewModel == null)
                {
                    _flightFilterViewModel = new FlightFilterViewModel("FloatHost", _flightFilter)
                        { 
                            CanFilterByFlightTypes = true,
                            FlightTypes = FlightType.Towing | FlightType.GroundMovement
                        };
                    _flightFilterViewModel.RequestFilter += (o, e) =>
                        {
                            IsLoading = true;
                            using (BackgroundWorker worker = new BackgroundWorker())
                            {
                                worker.DoWork += (ob, ev) =>
                                    {
                                        LoadFlightData();
                                    };
                                worker.RunWorkerCompleted += (ob, ev) =>
                                    {
                                        IsLoading = false;
                                    };
                                worker.RunWorkerAsync();
                            }
                        };
                }

                return _flightFilterViewModel;
            }
        }

        public bool IsLoading
        {
            get
            {
                return _isLoading;
            }
            set
            {
                if (_isLoading == value)
                    return;

                _isLoading = value;
                OnPropertyChanged("IsLoading");
            }
        }

        #endregion

        #region Public Methods
        
        public string LoadFlightData()
        {
            string strAttrList = "[Urno],[ReadyForTowing],[FullFlightNumber],[RegistrationNumber],[AircraftIATACode]," +
                "[PositionOfDeparture],[PositionOfArrival],[StandardTimeOfDeparture],[StandardTimeOfArrival]," +
                "[OffblockTime],[OnblockTime],[OperationalType]";
            string strWhere = "WHERE (([StandardTimeOfArrival] BETWEEN '{0}' AND '{1}') OR ([TimeframeOfArrival] BETWEEN '{0}' AND '{1}'))" +
                " AND (([StandardTimeOfDeparture] BETWEEN '{0}' AND '{1}') OR ([TimeframeOfDeparture] BETWEEN '{0}' AND '{1}'))" +
                " AND FTYP IN ({2})";
            DateTime dtmUTCStartDate = TimeZoneInfo.ConvertTime(_flightFilter.AbsoluteStartDate, 
                DlUfisData.Current.DataContext.DefaultTimeZoneInfo);
            string strUTCStartDate = CedaUtils.DateTimeToCedaDateTime(dtmUTCStartDate);
            DateTime dtmUTCEndDate = TimeZoneInfo.ConvertTime(_flightFilter.AbsoluteEndDate, 
                DlUfisData.Current.DataContext.DefaultTimeZoneInfo);
            string strUTCEndDate = CedaUtils.DateTimeToCedaDateTime(dtmUTCEndDate);            
            strWhere = string.Format(strWhere, strUTCStartDate, strUTCEndDate, "'T','G'");
            
            string strErrorMsg = null;

            try
            {
                if (_flights != null)
                    _flights.Close();

                Flights = DlGroundControlManager.LoadFlights(strAttrList, strWhere);
            }
            catch (Exception e)
            {
                strErrorMsg = e.Message;
                if (e is DataException)
                    strErrorMsg += "\n" + ((DataException)e).Message;                
            }

            return strErrorMsg;
        }

        public void UpdateDb(EntDbFlight flight)
        {
            if (flight != null)
            {
                try
                {
                    flight.SetEntityState(Entity.EntityState.Modified);
                    _flights.Save(flight);
                }
                catch (Exception ex)
                {
                    string strErrMsg = ex.Message;
                    if (ex is DataException)
                        strErrMsg += "\n" + ((DataException)ex).Message;

                    DXMessageBox.Show("Failed to update the database.\nFollwoing error(s) occured:\n" + strErrMsg);
                }
            }
        }

        #endregion

        #region Constructor

        public MainWindowViewModel()
        {
            DisplayName = HpAppInfo.Current.ProductTitle;

            Columns = new ObservableCollection<UfisGridColumn>()
            {
                new UfisGridColumn() { FieldName = "ReadyForTowing", Header = "Ready", Width = 60, AllowEditing = DefaultBoolean.True, Settings = UfisGridColumnSettingsType.Check, HorizontalContentAlignment = EditSettingsHorizontalAlignment.Center },
                new UfisGridColumn() { FieldName = "FullFlightNumber", Header = "Flight No", Width = 80, AllowEditing = DefaultBoolean.False },
                new UfisGridColumn() { FieldName = "RegistrationNumber", Header = "Reg. No", Width = 80, AllowEditing = DefaultBoolean.False },
                new UfisGridColumn() { FieldName = "AircraftIATACode", Header = "A/C Type", Width = 80, AllowEditing = DefaultBoolean.False, HorizontalContentAlignment = EditSettingsHorizontalAlignment.Center },
                new UfisGridColumn() { FieldName = "PositionOfDeparture", Header = "From Bay", Width = 80, AllowEditing = DefaultBoolean.False },
                new UfisGridColumn() { FieldName = "PositionOfArrival", Header = "To Bay", Width = 80, AllowEditing = DefaultBoolean.False },
                new UfisGridColumn() { FieldName = "StandardTimeOfDeparture", Header = "SOBT", Width = 80, Settings = UfisGridColumnSettingsType.DateTime, DisplayFormat = "HH:mm/dd", AllowEditing = DefaultBoolean.False, HorizontalContentAlignment = EditSettingsHorizontalAlignment.Center },
                new UfisGridColumn() { FieldName = "StandardTimeOfArrival", Header = "SIBT", Width = 80, Settings = UfisGridColumnSettingsType.DateTime, DisplayFormat = "HH:mm/dd", AllowEditing = DefaultBoolean.False, HorizontalContentAlignment = EditSettingsHorizontalAlignment.Center },
                new UfisGridColumn() { FieldName = "OffblockTime", Header = "AOBT", Width = 80, Settings = UfisGridColumnSettingsType.DateTime, DisplayFormat = "HH:mm/dd", AllowEditing = DefaultBoolean.False, HorizontalContentAlignment = EditSettingsHorizontalAlignment.Center },
                new UfisGridColumn() { FieldName = "OnblockTime", Header = "AIBT", Width = 80, Settings = UfisGridColumnSettingsType.DateTime, DisplayFormat = "HH:mm/dd", AllowEditing = DefaultBoolean.False, HorizontalContentAlignment = EditSettingsHorizontalAlignment.Center }
            };

            DateTime dtmNow = DateTime.Now;
            _flightFilter = new FlightFilter()
            {
                DateFilterType = FlightDateFilterType.AbsoluteDate,
                AbsoluteStartDate = dtmNow.AddHours(-2),
                AbsoluteEndDate = dtmNow.AddHours(8),
                RelativeStartRange = -2,
                RelativeEndRange = 8,
                FlightTypes = FlightTypesBuilder.GetFlightTypeList(FlightType.Towing | FlightType.GroundMovement)
            };
        }
        #endregion // Constructor

        #region Commands

        protected override List<CommandViewModel> CreateCommands()
        {
            return new List<CommandViewModel>()            
            {
                new CommandViewModel("Filter Flights", new RelayCommand(RunCommand))
                {
                    Glyph = ImageGetter.GetImage("filter_16x16.png"),
                    LargeGlyph = ImageGetter.GetImage("filter_32x32.png")
                }
            };
        }

        private void RunCommand(object parameter)
        {
            string strParameter = (parameter == null ? string.Empty : parameter.ToString());
            switch (strParameter)
            {
                case "Filter Flights":
                    OpenWorkspace(FlightFilter);
                    break;
                default:
                    break;
            }
        }

        #endregion // Commands

    }
}