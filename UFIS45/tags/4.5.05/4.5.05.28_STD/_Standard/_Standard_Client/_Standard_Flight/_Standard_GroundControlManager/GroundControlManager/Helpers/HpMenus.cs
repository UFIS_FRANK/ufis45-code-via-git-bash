﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Utilities;
using System.Windows.Media.Imaging;

namespace GroundControlManager.Helpers
{
    public class HpMenus
    {
        private static List<AppMenu> _lstAppMenus;

        private static List<AppMenu> CreateMenus()
        {
            const string SMALL_IMAGE_URI = "pack://application:,,,/Ufis.Resources.ImageLibrary;v4.5.0.0;8f51493079604dd3;component/application_16x16.png";
            const string LARGE_IMAGE_URI = "pack://application:,,,/Ufis.Resources.ImageLibrary;v4.5.0.0;8f51493079604dd3;component/application_32x32.png";

            BitmapImage smallImage = new BitmapImage(new Uri(SMALL_IMAGE_URI));
            smallImage.Freeze();
            BitmapImage largeImage = new BitmapImage(new Uri(LARGE_IMAGE_URI));
            largeImage.Freeze();

            IList<AppMenu> lstAppMenus = new List<AppMenu>();

            lstAppMenus.Add(
                new AppMenu() 
                { Name = "Aircraft family", ImageSmall = smallImage, ImageLarge = largeImage } );
            lstAppMenus.Add(new AppMenu() 
                { Name = "Aircraft types", ImageSmall = smallImage, ImageLarge = largeImage } );
            lstAppMenus.Add(new AppMenu() 
                { Name = "Airlines", ImageSmall = smallImage, ImageLarge = largeImage } );
            lstAppMenus.Add(new AppMenu() 
                { Name = "Airports", ImageSmall = smallImage, ImageLarge = largeImage } );
            lstAppMenus.Add(new AppMenu() 
                { Name = "Baggage belts (arrival)", ImageSmall = smallImage, ImageLarge = largeImage } );
            lstAppMenus.Add(new AppMenu() 
                { Name = "Change Reason Code", Caption = "Change reason codes", ImageSmall = smallImage, ImageLarge = largeImage } );
            lstAppMenus.Add(new AppMenu() 
                { Name = "Check-in counters", ImageSmall = smallImage, ImageLarge = largeImage } );
            lstAppMenus.Add(new AppMenu() 
                { Name = "Continents", ImageSmall = smallImage, ImageLarge = largeImage });
            lstAppMenus.Add(new AppMenu() 
                { Name = "Countries", ImageSmall = smallImage, ImageLarge = largeImage });
            lstAppMenus.Add(new AppMenu() 
                { Name = "Delay codes", ImageSmall = smallImage, ImageLarge = largeImage } );
            lstAppMenus.Add(new AppMenu() 
                { Name = "Engine types", ImageSmall = smallImage, ImageLarge = largeImage } );
            lstAppMenus.Add(new AppMenu() 
                { Name = "Exits", ImageSmall = smallImage, ImageLarge = largeImage } );
            lstAppMenus.Add(new AppMenu() 
                { Name = "Gates", ImageSmall = smallImage, ImageLarge = largeImage } );
            lstAppMenus.Add(new AppMenu() 
                { Name = "Handling agents", ImageSmall = smallImage, ImageLarge = largeImage } );
            lstAppMenus.Add(new AppMenu() 
                { Name = "Handling types", ImageSmall = smallImage, ImageLarge = largeImage } );
            lstAppMenus.Add(new AppMenu() 
                { Name = "Lounges", ImageSmall = smallImage, ImageLarge = largeImage } );
            lstAppMenus.Add(new AppMenu() 
                { Name = "Nature codes", ImageSmall = smallImage, ImageLarge = largeImage } );
            lstAppMenus.Add(new AppMenu() 
                { Name = "Parking stands", ImageSmall = smallImage, ImageLarge = largeImage } );
            lstAppMenus.Add(new AppMenu() 
                { Name = "Registrations", ImageSmall = smallImage, ImageLarge = largeImage } );
            lstAppMenus.Add(new AppMenu() 
                { Name = "Runways", ImageSmall = smallImage, ImageLarge = largeImage } );
            lstAppMenus.Add(new AppMenu() 
                { Name = "Service types", ImageSmall = smallImage, ImageLarge = largeImage } );
            lstAppMenus.Add(new AppMenu() 
                { Name = "Parameter", Caption = "Parameters", ImageSmall = smallImage, ImageLarge = largeImage } );
            lstAppMenus.Add(new AppMenu() 
                { Name = "Taxiways", ImageSmall = smallImage, ImageLarge = largeImage } );
            lstAppMenus.Add(new AppMenu() 
                { Name = "Telex addresses", ImageSmall = smallImage, ImageLarge = largeImage } );

            return lstAppMenus.OrderBy(x => x.ToString()).ToList();
        }

        public static List<AppMenu> MenuList
        {
            get 
            { 
                if (_lstAppMenus == null)
                    _lstAppMenus = CreateMenus();

                return _lstAppMenus; 
            }
        }
    }
}
