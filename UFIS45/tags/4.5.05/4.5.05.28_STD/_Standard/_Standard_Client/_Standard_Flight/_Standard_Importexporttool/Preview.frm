VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form PreView 
   Caption         =   "Import Updates Preview"
   ClientHeight    =   3585
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10590
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   8.25
      Charset         =   161
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Preview.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3585
   ScaleWidth      =   10590
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox Text1 
      Height          =   825
      Left            =   60
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   7
      Top             =   420
      Width           =   10155
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Flights"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   1800
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   60
      Width           =   855
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   5
      Top             =   3300
      Width           =   10590
      _ExtentX        =   18680
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   15584
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin TABLib.TAB PreviewList 
      Height          =   1785
      Left            =   60
      TabIndex        =   4
      Top             =   1380
      Width           =   3405
      _Version        =   65536
      _ExtentX        =   6006
      _ExtentY        =   3149
      _StockProps     =   64
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Save As"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   930
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   60
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Compare"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   60
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   2670
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   60
      Width           =   855
   End
   Begin VB.Label lblCount 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3570
      TabIndex        =   1
      Top             =   60
      Width           =   945
   End
End
Attribute VB_Name = "PreView"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private CurHeadLength As String
Private IsInitialized As Boolean

Public Sub InitPreviewList(ipLines As Integer, ipFirstCall As Boolean)
Dim i As Integer
Dim tmpLen
Dim HdrLenLst As String
Dim tmpHeader As String
Dim ActColLst As String
Dim FldTypLst As String

    Me.FontSize = MySetUp.FontSlider.Value
    tmpHeader = "TRKY,RURN,S,FKYD,FKYN,RFLD,VALO,VALN,WKSN,CDAT,URNO,REMARK"
    ActColLst = "10  ,10  ,1,8   ,10  ,4   ,14  ,14  ,10  ,14  ,10 "
    HdrLenLst = ""
    FldTypLst = ""
    i = 0
    Do
        i = i + 1
        tmpLen = GetItem(ActColLst, i, ",")
        If tmpLen <> "" Then
            If tmpLen > 0 Then
                HdrLenLst = HdrLenLst & TextWidth(Space(tmpLen)) / Screen.TwipsPerPixelX + 6 & ","
            Else
                HdrLenLst = HdrLenLst & "1,"
            End If
            FldTypLst = FldTypLst & "L,"
        End If
    Loop While tmpLen <> ""
    HdrLenLst = HdrLenLst & "1000"
    FldTypLst = FldTypLst & "L"
    If ipFirstCall Then PreviewList.ResetContent
    PreviewList.FontName = "Courier New"
    PreviewList.SetTabFontBold True
    PreviewList.ShowHorzScroller False
    PreviewList.HeaderFontSize = MySetUp.FontSlider.Value + 6
    PreviewList.FontSize = MySetUp.FontSlider.Value + 6
    PreviewList.LineHeight = MySetUp.FontSlider.Value + 7
    PreviewList.HeaderLengthString = HdrLenLst
    PreviewList.HeaderString = tmpHeader
    
    If ipFirstCall And Not IsInitialized Then
        PreviewList.Height = ipLines * PreviewList.LineHeight * Screen.TwipsPerPixelY
        Me.Height = PreviewList.Top + PreviewList.Height + 30 * Screen.TwipsPerPixelY
        IsInitialized = True
    End If
    CurHeadLength = ActColLst
    Me.Refresh
End Sub

Public Sub LoadPreviewData(ForWhat As String)
    Dim RetVal As Integer
    Dim tmpFields As String
    Dim tmpSqlKey As String
    Dim TrkyList  As String
    Dim LineNo As Long
    Screen.MousePointer = 11
    PreviewList.ResetContent
    tmpSqlKey = ""
    TrkyList = ""
    If ForWhat = "LINEMODE" Then
        LineNo = FlightExtract.ExtractData.GetCurrentSelected
        If LineNo >= 0 Then
            TrkyList = FlightExtract.ExtractData.GetColumnValue(LineNo, 24)
            If TrkyList <> "" Then
                tmpSqlKey = "WHERE TRKY='" & TrkyList & "'"
            End If
        End If
    ElseIf ForWhat = "FULLMODE" Then
        TrkyList = FlightExtract.ExtractData.SelectDistinct("24", "'", "'", ",", True)
        TrkyList = Replace(TrkyList, "''", "", 1, -1, vbBinaryCompare)
        TrkyList = Replace(TrkyList, ",,", "", 1, -1, vbBinaryCompare)
        If Left(TrkyList, 1) = "," Then TrkyList = Mid(TrkyList, 2)
        If Right(TrkyList, 1) = "," Then TrkyList = Left(TrkyList, Len(TrkyList) - 1)
        If TrkyList <> "" Then
            tmpSqlKey = "WHERE TRKY IN (" & TrkyList & ")"
        End If
    End If
    If (TrkyList <> "") Or (ForWhat = "READALL") Then
        tmpFields = "TRKY,RURN,STAT,FKYD,FKYN,RFLD,VALO,VALN,WKSN,CDAT,URNO"
        RetVal = UfisServer.CallCeda(UserAnswer, "RTA", "ICHTAB", tmpFields, "", tmpSqlKey, "FKYN,FKYD,CDAT", 0, True, False)
        PreviewList.InsertBuffer UserAnswer, vbLf
        PreviewList.RedrawTab
        tmpFields = "URNO,CDAT,USEC,WKSN,TRKY,FKYA,FKYD,CALH,CARH,CDEH,CIAR,CUAR,CNAR,CIDE,CUDE,CNDE,STAT"
        RetVal = UfisServer.CallCeda(UserAnswer, "RTA", "IMCTAB", tmpFields, "", tmpSqlKey, "FKYA,FKYD,CDAT", 0, True, False)
        Text1.Text = UserAnswer
    Else
        
    End If
    lblCount.Caption = Str(PreviewList.GetLineCount)
    Screen.MousePointer = 0
    Me.Refresh
End Sub

Private Sub chkWork_Click(Index As Integer)
    Select Case Index
        Case 0
            'Close
            If chkWork(Index) = 1 Then Unload Me
        Case 1
            If chkWork(Index) = 1 Then
                CompareFlights
                chkWork(Index) = 0
            End If
        Case 2
            'Save As
            If chkWork(Index).Value = 1 Then
                MainDialog.SaveLoadedFile Index, PreView.PreviewList
                chkWork(Index).Value = 0
            End If
        Case 3
            'Flights
            If chkWork(Index).Value = 1 Then
                DataDiff.Show , Me
                DataDiff.LoadDiffData
            Else
                Unload DataDiff
            End If
    End Select
End Sub

Private Sub CompareFlights()
    FlightExtract.ExtractData.SetCurrentSelection 0
            FlightDetails.GetFlightCalendar
            If MySetUp.MonitorSetting(3).Value = 1 Then
                FlightDetails.Show , Me
            Else
                FlightDetails.Show
            End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    'If KeyCode = vbKeyF1 Then DisplayHtmHelpFile "", Me.Name, Me.ActiveControl.Name
    If KeyCode = vbKeyF1 Then ShowHtmlHelp Me, "", ""
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    FlightExtract.chkWork(11).Value = 0
    FlightExtract.chkWork(12).Value = 0
End Sub

Private Sub PreviewList_SendRButtonClick(ByVal Line As Long, ByVal Col As Long)
    If Col >= 0 Then
        'PreviewList.Sort str(Col), True, True
        'PreviewList.RedrawTab
    End If
End Sub

Private Sub Form_Load()
    Me.Left = 0
    Me.Top = 1000
    Me.Width = MainDialog.Width
    InitPreviewList 16, True
End Sub
Private Sub Form_Resize()
Dim NewVal As Long
    NewVal = Me.ScaleHeight - PreviewList.Top - StatusBar.Height - 3 * Screen.TwipsPerPixelY
    If NewVal > 50 Then
        PreviewList.Height = NewVal
    End If
    NewVal = Me.ScaleWidth - 2 * PreviewList.Left
    If NewVal > 50 Then PreviewList.Width = NewVal
    Text1.Width = PreviewList.Width
End Sub

