VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form FlightExtract 
   Caption         =   "Flight Data Extract"
   ClientHeight    =   2460
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   14970
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   8.25
      Charset         =   161
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FlightExtract.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   2460
   ScaleWidth      =   14970
   ShowInTaskbar   =   0   'False
   Begin TABLib.TAB HelperTab 
      Height          =   915
      Left            =   5310
      TabIndex        =   49
      Top             =   1020
      Visible         =   0   'False
      Width           =   1155
      _Version        =   65536
      _ExtentX        =   2037
      _ExtentY        =   1614
      _StockProps     =   64
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Ack Err"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   29
      Left            =   5280
      Style           =   1  'Graphical
      TabIndex        =   48
      Top             =   360
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Rotation"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   28
      Left            =   4410
      Style           =   1  'Graphical
      TabIndex        =   47
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Shrink"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   27
      Left            =   3540
      Style           =   1  'Graphical
      TabIndex        =   45
      Top             =   -120
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Check"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   21
      Left            =   2670
      Style           =   1  'Graphical
      TabIndex        =   44
      Top             =   -120
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   18
      Left            =   9630
      Style           =   1  'Graphical
      TabIndex        =   43
      Top             =   360
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Errors"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   2670
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Batch"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   26
      Left            =   13110
      Style           =   1  'Graphical
      TabIndex        =   42
      Top             =   360
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Update"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   25
      Left            =   11370
      Style           =   1  'Graphical
      TabIndex        =   41
      Top             =   360
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Insert"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   24
      Left            =   10500
      Style           =   1  'Graphical
      TabIndex        =   40
      Top             =   360
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Ignored"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   2670
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   210
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Delete"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   23
      Left            =   12240
      Style           =   1  'Graphical
      TabIndex        =   39
      Top             =   360
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   14
      Left            =   6150
      Style           =   1  'Graphical
      TabIndex        =   24
      Top             =   360
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Import"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   9
      Left            =   8760
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Compare"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   13
      Left            =   5280
      Style           =   1  'Graphical
      TabIndex        =   23
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Report"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   10
      Left            =   7890
      Style           =   1  'Graphical
      TabIndex        =   36
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Full Mode"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   11
      Left            =   8325
      Style           =   1  'Graphical
      TabIndex        =   21
      Top             =   360
      Width           =   1290
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Preview"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   8
      Left            =   7020
      Style           =   1  'Graphical
      TabIndex        =   19
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Booklet"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   19
      Left            =   13110
      Style           =   1  'Graphical
      TabIndex        =   37
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Print"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   17
      Left            =   9630
      Style           =   1  'Graphical
      TabIndex        =   35
      ToolTipText     =   "Spare Part"
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Save As"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   16
      Left            =   10500
      Style           =   1  'Graphical
      TabIndex        =   34
      Top             =   30
      Width           =   855
   End
   Begin VB.Frame Frame2 
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   315
      Left            =   4605
      TabIndex        =   31
      Top             =   360
      Width           =   675
      Begin VB.TextBox txtVpto 
         Alignment       =   2  'Center
         BackColor       =   &H0080FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   0
         TabIndex        =   33
         Top             =   0
         Width           =   315
      End
      Begin VB.TextBox txtVpto 
         Alignment       =   2  'Center
         BackColor       =   &H0080FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   330
         TabIndex        =   32
         Top             =   0
         Width           =   315
      End
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   315
      Left            =   2295
      TabIndex        =   28
      Top             =   360
      Width           =   705
      Begin VB.TextBox txtVpfr 
         Alignment       =   2  'Center
         BackColor       =   &H0080FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   0
         TabIndex        =   30
         Top             =   0
         Width           =   315
      End
      Begin VB.TextBox txtVpfr 
         Alignment       =   2  'Center
         BackColor       =   &H0080FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   330
         TabIndex        =   29
         Top             =   0
         Width           =   315
      End
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Statistic"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   15
      Left            =   12240
      Style           =   1  'Graphical
      TabIndex        =   25
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Line Mode"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   12
      Left            =   7020
      Style           =   1  'Graphical
      TabIndex        =   22
      Top             =   360
      Width           =   1290
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Daily"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   7
      Left            =   11370
      Style           =   1  'Graphical
      TabIndex        =   18
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Merge"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   6
      Left            =   3540
      Style           =   1  'Graphical
      TabIndex        =   15
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Manual"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   5
      Left            =   3540
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   210
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   20
      Left            =   13980
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Load"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   4
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   30
      Width           =   855
   End
   Begin VB.TextBox txtVpto 
      Alignment       =   2  'Center
      BackColor       =   &H0080FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   4125
      TabIndex        =   8
      Top             =   360
      Width           =   315
   End
   Begin VB.TextBox txtVpto 
      Alignment       =   2  'Center
      BackColor       =   &H0080FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   3795
      TabIndex        =   7
      Top             =   360
      Width           =   315
   End
   Begin VB.TextBox txtVpfr 
      Alignment       =   2  'Center
      BackColor       =   &H0080FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   1815
      TabIndex        =   5
      Top             =   360
      Width           =   315
   End
   Begin VB.TextBox txtVpfr 
      Alignment       =   2  'Center
      BackColor       =   &H0080FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   1485
      TabIndex        =   4
      Top             =   360
      Width           =   315
   End
   Begin VB.TextBox txtVpto 
      Alignment       =   2  'Center
      BackColor       =   &H0080FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   3225
      TabIndex        =   6
      Top             =   360
      Width           =   555
   End
   Begin VB.TextBox txtVpfr 
      Alignment       =   2  'Center
      BackColor       =   &H0080FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   915
      TabIndex        =   3
      Top             =   360
      Width           =   555
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Filter"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   930
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "All"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   1800
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   30
      Width           =   855
   End
   Begin TABLib.TAB ExtractData 
      Height          =   1155
      Left            =   30
      TabIndex        =   0
      Top             =   750
      Width           =   4875
      _Version        =   65536
      _ExtentX        =   8599
      _ExtentY        =   2037
      _StockProps     =   64
      Columns         =   10
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   11
      Top             =   2175
      Width           =   14970
      _ExtentX        =   26405
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   7
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1323
            MinWidth        =   1323
            Object.ToolTipText     =   "Total Lines"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1323
            MinWidth        =   1323
            Object.ToolTipText     =   "Valid Lines (Filtered)"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1323
            MinWidth        =   1323
            Object.ToolTipText     =   "Total Flights (Lines)"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1323
            MinWidth        =   1323
            Object.ToolTipText     =   "Arrivals (Lines)"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1323
            MinWidth        =   1323
            Object.ToolTipText     =   "Departures (Lines)"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1323
            MinWidth        =   1323
            Object.ToolTipText     =   "Rotations (Lines)"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   8819
            MinWidth        =   8819
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Activate"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   22
      Left            =   6150
      Style           =   1  'Graphical
      TabIndex        =   38
      Top             =   30
      Width           =   855
   End
   Begin VB.Label ErrCnt 
      Alignment       =   2  'Center
      BackColor       =   &H000000FF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   13980
      TabIndex        =   46
      ToolTipText     =   "Error Counter"
      Top             =   345
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00000000&
      X1              =   0
      X2              =   16030
      Y1              =   690
      Y2              =   690
   End
   Begin VB.Line Line2 
      BorderColor     =   &H80000014&
      X1              =   0
      X2              =   16030
      Y1              =   705
      Y2              =   705
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Caption         =   "/"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   4455
      TabIndex        =   27
      Top             =   405
      Width           =   135
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "to"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   3000
      TabIndex        =   26
      Top             =   405
      Width           =   195
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "/"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   2145
      TabIndex        =   17
      Top             =   405
      Width           =   135
   End
   Begin VB.Label PeriodLabel 
      Caption         =   "Period:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   165
      TabIndex        =   16
      Top             =   390
      Width           =   675
   End
End
Attribute VB_Name = "FlightExtract"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private IsInitialized As Boolean
Private StopRecursion As Boolean
Dim myRemaTxtCol As Long
Dim myMainIdxCol As Long
Dim myHeaderWidth As String
Dim myHeaderTitle As String
Dim myHeaderColor As String
Dim myExpandHeader As String
Dim myFldWidLst As String
Dim myHdrLenLst As String
Dim myDateFields As String
Dim myTimeFields As String
Dim WorkPeriodCols As String
Dim MsgCalled As Boolean

Public Sub InitDetailList(ipLines As Integer, ipFirstCall As Boolean)
    Dim i As Integer
    Dim CurDay
    Dim FirstDate As String
    Dim LastDate As String
    Dim tmpDate As String
    Dim FirstTime As String
    Dim LastTime As String
    Me.Caption = DataSystemType & " " & MainDialog.ActSeason(0).Caption & " Flight Data Extract" & " (" & MySetUp.CurFileInfo(5).Caption & " Mode) " & GetItem(MySetUp.MyFileInfo.Caption, 1, " ")
    If ipFirstCall Then ExtractData.ResetContent
    Me.FontSize = MySetUp.FontSlider.Value
    ExtractData.FontName = "Courier New"
    ExtractData.SetTabFontBold True
    ExtractData.ShowHorzScroller True
    ExtractData.HeaderFontSize = MySetUp.FontSlider.Value + 6
    ExtractData.FontSize = MySetUp.FontSlider.Value + 6
    ExtractData.LineHeight = MySetUp.FontSlider.Value + 7
    If ipFirstCall And Not IsInitialized Then
        ExtractData.Height = ipLines * ExtractData.LineHeight * Screen.TwipsPerPixelY
        Me.Height = ExtractData.Top + ExtractData.Height + StatusBar.Height + 30 * Screen.TwipsPerPixelY
        IsInitialized = True
    End If
    If Not NewLayoutType Then
        myExpandHeader = GetIniEntry(myIniFullName, "EXTRACT", "", "FINAL_HEAD", "")
        myFldWidLst = GetIniEntry(myIniFullName, "EXTRACT", "", "FINAL_LEN", "")
        WorkPeriodCols = GetIniEntry(myIniFullName, "EXTRACT", "", "WORK_PERIOD", "0,0,0,0,0,0")
        If myExpandHeader = "" Or myFldWidLst = "" Then
            ExtractData.HeaderLengthString = MainDialog.FileData(CurFdIdx).HeaderLengthString
            ExtractData.HeaderString = MainDialog.FileData(CurFdIdx).HeaderString
            ExtractData.ColumnWidthString = MainDialog.FileData(CurFdIdx).ColumnWidthString
            ExtractData.ColumnAlignmentString = MainDialog.FileData(CurFdIdx).ColumnAlignmentString
        Else
            InitGridLayout
        End If
        'HardCoded
        ExtractData.DateTimeSetColumn 7
        ExtractData.DateTimeSetInputFormatString 7, "YYYYMMDD"
        ExtractData.DateTimeSetOutputFormatString 7, "DDMMMYY"
        ExtractData.DateTimeSetColumn 8
        ExtractData.DateTimeSetInputFormatString 8, "YYYYMMDD"
        ExtractData.DateTimeSetOutputFormatString 8, "DDMMMYY"
        ExtractData.DateTimeSetColumn 14
        ExtractData.DateTimeSetInputFormatString 14, "YYYYMMDDhhmm"
        ExtractData.DateTimeSetOutputFormatString 14, "DDMMMYY'/'hhmm"
        ExtractData.DateTimeSetColumn 15
        ExtractData.DateTimeSetInputFormatString 15, "YYYYMMDDhhmm"
        ExtractData.DateTimeSetOutputFormatString 15, "DDMMMYY'/'hhmm"
    Else
        InitNewLayout
    End If
    CheckMyMainHeader
    If UpdateMode Then
        For i = 0 To 4
            txtVpfr(i).Enabled = False
            txtVpto(i).Enabled = False
        Next
        FirstDate = MainDialog.ActSeason(1).Tag & "0000"
        LastDate = MainDialog.ActSeason(2).Tag & "2359"
        txtVpfr(0).Text = ""
    Else
        LastDate = MainDialog.ActSeason(2).Tag & "2359"
        If LastDate = "2359" Then LastDate = GetItem(MainDialog.ActSeason(0).Tag, 6, ",") & "2359"
        tmpDate = GetItem(MainDialog.ActSeason(0).Tag, 6, ",") & "2359"
        If tmpDate = "2359" Then tmpDate = LastDate
        If tmpDate < LastDate Then LastDate = tmpDate
        If DataSystemType = "OAFOS" Then
            CurDay = DateAdd("h", 2, Now)
            FirstDate = Format(CurDay, "yyyymmddhhmm")
        Else
            FirstDate = MySetUp.ServerTime.Tag
            CurDay = CedaFullDateToVb(FirstDate)
            CurDay = DateAdd("d", Val(MySetUp.AutoClear.Caption), CurDay)
            FirstDate = Format(CurDay, "yyyymmdd") & "0000"
            tmpDate = GetItem(MainDialog.ActSeason(0).Tag, 5, ",") & "0000"
            If tmpDate = "0000" Then tmpDate = MainDialog.ActSeason(1).Tag & "0000"
            If tmpDate > FirstDate Then FirstDate = tmpDate
        End If
    End If
    If Trim(txtVpfr(0).Text = "") Then
        txtVpfr(0).Tag = Left(FirstDate, 8)
        txtVpfr(0).Text = Left(FirstDate, 4)
        txtVpfr(1).Text = Mid(FirstDate, 5, 2)
        txtVpfr(2).Text = Mid(FirstDate, 7, 2)
        txtVpfr(3).Text = Mid(FirstDate, 9, 2)
        txtVpfr(4).Text = Right(FirstDate, 2)
        txtVpto(0).Tag = Left(LastDate, 8)
        txtVpto(0).Text = Left(LastDate, 4)
        txtVpto(1).Text = Mid(LastDate, 5, 2)
        txtVpto(2).Text = Mid(LastDate, 7, 2)
        txtVpto(3).Text = Mid(LastDate, 9, 2)
        txtVpto(4).Text = Right(LastDate, 2)
        CheckLoadFilter True
    End If
    ExtractData.EnableInlineEdit False
End Sub
Private Sub InitNewLayout()
    Dim tmpData As String
    Dim useSection As String
    tmpData = GetIniEntry(myIniFullName, myIniSection, "", "VIEW_LAYOUT", "SECTION")
    If tmpData = "SECTION" Then useSection = myIniSection Else useSection = tmpData & "_LAYOUT"
    myViewFieldList = GetIniEntry(myIniFullName, useSection, "", "VIEW_FIELDS", "MAIN")
    If myViewFieldList = "MAIN" Then myViewFieldList = MainFieldList
    myExpandHeader = GetIniEntry(myIniFullName, useSection, "", "VIEW_FINAL_HEAD", "MAIN")
    If myExpandHeader = "MAIN" Then myExpandHeader = MainExpandHeader
    myFldWidLst = GetIniEntry(myIniFullName, useSection, "", "VIEW_FINAL_LEN", "MAIN")
    If myFldWidLst = "MAIN" Then myFldWidLst = MainFldWidLst
    WorkPeriodCols = GetIniEntry(myIniFullName, useSection, "", "WORK_PERIOD", "VPFR,VPTO,FREQ,FREW,STOA,STOD,DAOF,OVNI")
    WorkPeriodCols = TranslateItemList(WorkPeriodCols, myViewFieldList, True)
    If Left(WorkPeriodCols, 1) = "-" Then
        WorkPeriodCols = TranslateItemList("SKED,SKED", myViewFieldList, True)
    End If
    myHeaderTitle = GetIniEntry(myIniFullName, useSection, "", "VIEW_HEADER_TITLE", "MAIN")
    myHeaderWidth = GetIniEntry(myIniFullName, useSection, "", "VIEW_HEADER_WIDTH", "")
    myHeaderColor = GetIniEntry(myIniFullName, useSection, "", "VIEW_HEADER_COLOR", "")
    If myHeaderTitle <> "MAIN" Then
        myHeaderWidth = "3," & myHeaderWidth & ",3"
        myHeaderTitle = "Status," & myHeaderTitle & ",System"
        If myHeaderColor <> "" Then myHeaderColor = "12632256," & myHeaderColor & ",12632256"
    Else
        myHeaderWidth = MainHeaderWidth
        myHeaderTitle = MainHeaderTitle
        myHeaderColor = MainHeaderColor
    End If
    InitGridLayout
    CreateTypeList myDateFields, myViewFieldList, "DATE", 2
    ToggleMyDateFields myDateFields
    CreateTypeList myTimeFields, myViewFieldList, "DATI", 2
    ToggleMyTimeFields myTimeFields
End Sub
Private Sub ToggleMyTimeFields(LineList As String)
    Dim ColNbr As Long
    Dim CurItm As Integer
    Dim ItmTxt As String
    CurItm = 1
    ItmTxt = GetItem(LineList, CurItm, ",")
    While ItmTxt <> ""
        ColNbr = Val(ItmTxt)
        ExtractData.DateTimeSetColumn ColNbr
        ExtractData.DateTimeSetInputFormatString ColNbr, "YYYYMMDDhhmm"
        ExtractData.DateTimeSetOutputFormatString ColNbr, "DDMMMYY'/'hhmm"
        CurItm = CurItm + 1
        ItmTxt = GetItem(LineList, CurItm, ",")
    Wend
End Sub
Private Sub ToggleMyDateFields(LineList As String)
    Dim ColNbr As Integer
    Dim CurItm As Integer
    Dim ItmTxt As String
    Dim newLen As String
    CurItm = 1
    ItmTxt = GetItem(LineList, CurItm, ",")
    While ItmTxt <> ""
        ColNbr = Val(ItmTxt)
        ExtractData.DateTimeSetColumn ColNbr
        ExtractData.DateTimeSetInputFormatString ColNbr, "YYYYMMDD"
        ExtractData.DateTimeSetOutputFormatString ColNbr, "DDMMMYY"
        ColNbr = ColNbr + 1
        newLen = Trim(Str(TextWidth("01OCT00") / Screen.TwipsPerPixelX + 6))
        SetItem myHdrLenLst, ColNbr, ",", newLen
        CurItm = CurItm + 1
        ItmTxt = GetItem(LineList, CurItm, ",")
    Wend
    ExtractData.HeaderLengthString = myHdrLenLst
End Sub

Private Sub InitGridLayout()
    Dim i As Integer
    Dim tmpLen
    Dim ColWidLst As String
    Dim tmpHeader As String
    Dim ActColLst As String
    Dim ColAlign As String
    myRemaTxtCol = CLng(ItemCount(myExpandHeader, ",")) + 3
    myMainIdxCol = myRemaTxtCol + 1
    tmpHeader = "LINE,I,A," & myExpandHeader & ",Remark,MainIndex"
    myHdrLenLst = ""
    myHdrLenLst = myHdrLenLst & TextWidth("9999") / Screen.TwipsPerPixelX + 6 & ","
    myHdrLenLst = myHdrLenLst & TextWidth("A") / Screen.TwipsPerPixelX + 6 & ","
    myHdrLenLst = myHdrLenLst & TextWidth("U") / Screen.TwipsPerPixelX + 6 & ","
    ColWidLst = "4,1,1,"
    ColAlign = "R,L,L,"
    i = 0
    Do
        i = i + 1
        tmpLen = GetItem(myFldWidLst, i, ",")
        If tmpLen <> "" Then
            myHdrLenLst = myHdrLenLst & TextWidth(Space(tmpLen)) / Screen.TwipsPerPixelX + 6 & ","
            ColWidLst = ColWidLst & tmpLen & ","
            ColAlign = ColAlign & "L,"
        End If
    Loop While tmpLen <> ""
    myHdrLenLst = myHdrLenLst & "500,100"
    ColWidLst = ColWidLst & "18,32"
    ColAlign = ColAlign & "L,L"
    ExtractData.HeaderLengthString = myHdrLenLst
    ExtractData.HeaderString = tmpHeader
    ExtractData.ColumnWidthString = ColWidLst
    ExtractData.ColumnAlignmentString = ColAlign
End Sub
Public Sub ChangeMyDataFont(myFontSize As Integer)
Dim i As Integer
Dim tmpLen
Dim CurLenLst As String
Dim myHdrLenLst As String

    FontSize = myFontSize
    myHdrLenLst = ""
    CurLenLst = "4,1,1," & MainDialog.ActColLst
    i = 0
    Do
        i = i + 1
        tmpLen = GetItem(CurLenLst, i, ",")
        If tmpLen <> "" Then
            myHdrLenLst = myHdrLenLst & TextWidth(Space(tmpLen)) / Screen.TwipsPerPixelX + 6 & ","
        End If
    Loop While tmpLen <> ""
    myHdrLenLst = myHdrLenLst & "1000"
    ExtractData.HeaderLengthString = myHdrLenLst
    ExtractData.HeaderFontSize = myFontSize + 6
    ExtractData.FontSize = myFontSize + 6
    ExtractData.LineHeight = myFontSize + 7
    Refresh
End Sub

Private Sub chkWork_Click(Index As Integer)
    Dim LineNo As Long
    Dim b As Boolean
    Dim i As Integer
    Dim k As Integer
    Dim l As Integer
    Dim MsgTxt As String
    Dim tmpLst As String
    Dim RetVal As Integer
    If chkWork(Index).Value = 0 Then chkWork(Index).BackColor = vbButtonFace
    If chkWork(Index).Value = 1 Then chkWork(Index).BackColor = LightestGreen
    Select Case Index
        Case 0
            'Filter
            If chkWork(Index).Value = 1 Then
                chkWork(1).Value = 0
                MousePointer = 11
                FilterCalledAsDispo = False
                If MySetUp.MonitorSetting(3).Value = 1 Then
                    RecFilter.Show , Me
                Else
                    RecFilter.Show
                End If
                RecFilter.ArrangeFormLayout
                RecFilter.Tag = ""
                MousePointer = 0
            Else
                RecFilter.Tag = "AUTOCLOSE"
                RecFilter.Hide
            End If
            CheckLoadFilter False
        Case 1
            'Get All
            If chkWork(Index).Value = 1 Then
                FilterCalledAsDispo = False
                If Not StopRecursion Then chkWork(0).Value = 0
                If Not StopRecursion Then CheckLoadFilter False
                If Not StopRecursion Then chkWork(4).Value = 1
                RecFilter.Hide
                RecFilter.ArrangeFormLayout
            End If
        Case 2
            'Errors
            CheckErrorFilter
            CheckLoadFilter False
            chkWork(3).Value = chkWork(Index).Value
        Case 3
            'Ignores
            CheckErrorFilter
            CheckLoadFilter False
        Case 4
            'Load
            If chkWork(Index).Value = 1 Then
                MsgCalled = False
                If MainDialog.chkTimes(0).Enabled Then MainDialog.chkTimes(0).Value = 1
                If chkWork(0).Value = 0 And chkWork(1).Value = 0 Then
                    StopRecursion = True
                    chkWork(1).Value = 1
                    StopRecursion = False
                End If
                RecFilter.chkTool(2).Value = 1
                Me.Refresh
                CopyAllFlights 0, -1
                CheckLoadFilter True
                chkWork(Index).Value = 0
                RecFilter.chkTool(2).Value = 0
                If UpdateMode Then
                    chkWork(21).Value = 0
                    chkWork(27).Value = 0
                    chkWork(29).Value = 0
                    chkWork(29).Enabled = False
                    'If DataSystemType <> "FHK" Then
                    '    chkWork(21).Enabled = False
                    '    chkWork(27).Enabled = False
                    'End If
                End If
            End If
        Case 5
            'Manual
            If chkWork(Index).Value = 1 Then
                chkWork(23).Enabled = True
            Else
                chkWork(23).Enabled = False
            End If
        Case 6
            'Merge
            If chkWork(Index).Value = 1 Then
                ResetList
                ExtractData.SetUniqueFields "0"
            Else
                ExtractData.SetUniqueFields ""
            End If
            chkWork(5).Value = chkWork(Index).Value
        Case 7
            'Daily
            DisplayFlightCalendar
        Case 8
            'Preview
            If chkWork(Index).Value = 1 Then
                chkWork(10).Value = 0
                chkWork(9).Value = 0
                chkWork(11).Enabled = True
                chkWork(12).Enabled = True
            Else
                chkWork(11).Enabled = False
                chkWork(12).Enabled = False
            End If
        Case 9
            'Import
            If chkWork(Index).Value = 1 Then
                chkWork(8).Value = 0
                chkWork(10).Value = 0
                chkWork(11).Enabled = True
                chkWork(12).Enabled = True
            Else
                chkWork(11).Enabled = False
                chkWork(12).Enabled = False
            End If
        Case 10
            'Report
            If chkWork(Index).Value = 1 Then
                chkWork(8).Value = 0
                chkWork(9).Value = 0
                chkWork(11).Enabled = True
                chkWork(12).Enabled = True
            Else
                chkWork(11).Value = 0
                chkWork(12).Value = 0
                chkWork(11).Enabled = False
                chkWork(12).Enabled = False
            End If
        Case 11
            'Full Mode
            If chkWork(Index).Value = 1 Then
                chkWork(12).Value = 0
                If chkWork(8).Value = 1 Then
                    'Preview
                    PrepareCedaEvent Index, 12, "PREVIEW"
                    chkWork(Index).Value = 0
                ElseIf chkWork(9).Value = 1 Then
                    'Import
                    PrepareCedaEvent Index, 12, "IMPORT"
                    chkWork(Index).Value = 0
                ElseIf chkWork(10).Value = 1 Then
                    'Report
                    If MySetUp.MonitorSetting(3).Value = 1 Then
                        PreView.Show , Me
                    Else
                        PreView.Show
                    End If
                    PreView.LoadPreviewData "FULLMODE"
                End If
            End If
        Case 12
            'Line Mode
            If chkWork(Index).Value = 1 Then
                chkWork(11).Value = 0
                If chkWork(8).Value = 1 Then
                    'Preview
                    PrepareCedaEvent Index, 12, "PREVIEW"
                    chkWork(Index).Value = 0
                ElseIf chkWork(9).Value = 1 Then
                    'Import
                    PrepareCedaEvent Index, 12, "IMPORT"
                    chkWork(Index).Value = 0
                ElseIf chkWork(10).Value = 1 Then
                    'Report
                    If MySetUp.MonitorSetting(3).Value = 1 Then
                        PreView.Show , Me
                    Else
                        PreView.Show
                    End If
                    PreView.LoadPreviewData "LINEMODE"
                End If
            End If
        Case 13
            'Compare NOOP Flights
            NoopFullMode = FullMode
            If AutoUpdateMode Then NoopFullMode = False
            MsgCalled = False
            If chkWork(Index).Value = 1 Then
                RetVal = -1
                If (chkWork(1).Value = 1) Or (RecFilter.CheckFullAirlineFilter = True) Or (UpdateMode) Then
                    RetVal = 1
                    If (Not MsgCalled) And (AlcErrorList <> "") Then
                        MsgCalled = True
                        MsgTxt = "Attention:" & vbNewLine
                        MsgTxt = MsgTxt + "You selected airlines with erroneous records!" + vbNewLine
                        MsgTxt = MsgTxt + "Thus the Compare Function cannot work properly" + vbNewLine
                        MsgTxt = MsgTxt + "concerning the flight deletion list!" + vbNewLine
                        MsgTxt = MsgTxt + "List of mentioned airlines:" + vbNewLine + vbNewLine
                        HelperTab.ResetContent
                        HelperTab.HeaderString = "X"
                        HelperTab.HeaderLengthString = "100"
                        HelperTab.ResetContent
                        tmpLst = AlcErrorList
                        tmpLst = Replace(tmpLst, " ", "", 1, -1, vbBinaryCompare)
                        HelperTab.InsertBuffer tmpLst, ","
                        HelperTab.Sort "0", True, True
                        tmpLst = HelperTab.GetBuffer(0, (HelperTab.GetLineCount - 1), ",")
                        tmpLst = Replace(tmpLst, ",", ", ", 1, -1, vbBinaryCompare)
                        l = Len(tmpLst)
                        k = 0
                        For i = 1 To l
                            If Mid(tmpLst, i, 1) = "," Then k = k + 1
                            If k = 10 Then
                                Mid(tmpLst, i, 2) = vbNewLine
                                k = 0
                            End If
                        Next
                        MsgTxt = MsgTxt + tmpLst + vbNewLine + vbNewLine
                        If NoopFullMode Then
                            MsgTxt = MsgTxt + "Do you want to proceed in Update Mode?"
                            RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Action Control", MsgTxt, "hand", "Yes,No,Stop", UserAnswer)
                            If RetVal = 3 Then RetVal = -1
                        Else
                            MsgTxt = MsgTxt + "Do you want to proceed?"
                            RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Action Control", MsgTxt, "hand", "Yes,No", UserAnswer)
                            If RetVal = 2 Then RetVal = -1
                        End If
                        If RetVal = 1 Then NoopFullMode = False
                    End If
                    If UpdateMode Then chkWork(27).Value = 1
                    If Val(ErrCnt.Caption) > 0 Then
                        MsgTxt = "The loaded data contain " & ErrCnt.Caption & " error(s)." & vbNewLine
                        MsgTxt = MsgTxt & "Do you want to proceed ?"
                        RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Action Control", MsgTxt, "stop2", "Yes,No", UserAnswer)
                        If RetVal = 2 Then RetVal = -1
                    End If
                    If RetVal >= 1 Then
                        RecFilter.PrepareNoopFlights False
                        RecFilter.chkTool(4).Value = 1
                        Me.Refresh
                        NoopFlights.Show
                        RetVal = 1
                    End If
                Else
                    If (FullMode) And (Not AutoUpdateMode) Then
                        MsgTxt = "Attention:" & vbNewLine
                        MsgTxt = MsgTxt + "The Compare function will work properly in Full Mode only" + vbNewLine
                        MsgTxt = MsgTxt + "when you choose a (full) airline filter and not partial flights!" + vbNewLine
                        MsgTxt = MsgTxt + "Do you want to proceed in Update Mode?"
                        MsgCalled = True
                        RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Action Control", MsgTxt, "stop2", "Yes,No", UserAnswer)
                    Else
                        RetVal = 1
                    End If
                    If RetVal = 1 Then
                        If Val(ErrCnt.Caption) > 0 Then
                            MsgTxt = "The loaded data contain " & ErrCnt.Caption & " error(s) in the Main View." & vbNewLine
                            MsgTxt = MsgTxt & "Do you want to proceed anyhow ?"
                            RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Action Control", MsgTxt, "stop2", "Yes,No", UserAnswer)
                        End If
                        If RetVal = 1 Then
                            NoopFullMode = False
                            RecFilter.chkTool(4).Value = 1
                            Me.Refresh
                            NoopFlights.Show
                        End If
                    End If
                End If
                If RetVal <> 1 Then chkWork(Index).Value = 0
            Else
                RecFilter.PrepareNoopFlights True
                RecFilter.chkTool(4).Value = 0
                Unload NoopFlights
            End If
        Case 14
            '???
            'If chkWork(Index).Value = 1 Then chkWork(9).Value = 1
        Case 15
            'Statistic
            If chkWork(Index).Value = 1 Then
                FlightStatistic.Show , Me
            Else
                Unload FlightStatistic
            End If
        Case 16
            'Save As
            If chkWork(Index).Value = 1 Then
                MainDialog.SaveLoadedFile Index, FlightExtract.ExtractData
                chkWork(Index).Value = 0
            End If
        Case 17
            'Print
        Case 19
            'Booklet
            If chkWork(Index).Value = 1 Then
                MyMsgBox.UnderConstruction ""
                chkWork(Index).Value = 0
            End If
        Case 20
            'Close
            If chkWork(Index).Value = 1 Then
                chkWork(Index).Value = 0
                MainDialog.chkExtract.Value = 0
                Unload Me
            End If
        Case 21
            'Check (FHK Updates) and (OAFOS Updates)
            If chkWork(Index).Value = 1 Then
                If UpdateMode Then
                    CheckUpdateActions
                    MsgCalled = True
                    chkWork(Index).Enabled = False
                End If
            End If
        Case 22
            'Activate
            If chkWork(Index).Value = 1 Then b = True Else b = False
            k = 0
            For i = 8 To 12
                chkWork(i).Enabled = b
                k = k + chkWork(i).Value
            Next
            If k = 0 Then
                chkWork(11).Enabled = False
                chkWork(12).Enabled = False
            End If
        Case 23
            'Delete
            If chkWork(Index).Value = 1 Then
                LineNo = ExtractData.GetCurrentSelected
                If LineNo >= 0 Then
                    ExtractData.DeleteLine LineNo
                    ExtractData.SetCurrentSelection LineNo
                    ExtractData.RedrawTab
                End If
                chkWork(Index).Value = 0
            End If
        Case 27
            'Shrink
            If chkWork(Index).Value = 1 Then
                If UpdateMode Then
                    chkWork(21).Value = 1
                    ShrinkUpdateList
                    chkWork(Index).Enabled = False
                End If
            End If
        Case 28
            'Rotations
            If chkWork(Index).Value = 1 Then
                If UpdateMode Then chkWork(27).Value = 1
                RetVal = 1
                If Val(ErrCnt.Caption) > 0 Then
                    MsgTxt = "The loaded data contain " & ErrCnt.Caption & " error(s)." & vbNewLine
                    MsgTxt = MsgTxt & "Do you want to proceed ?"
                    RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Action Control", MsgTxt, "stop2", "Yes,No", UserAnswer)
                End If
                If RetVal = 1 Then
                    Screen.MousePointer = 11
                    StrTabCaller = "EXTRACT"
                    If Not FormIsLoaded("StandardRotations") Then
                        ShowStrTabStyle = 1
                        AutoSwitch = True
                    End If
                    StandardRotations.Show
                    'StandardRotations.SetExtractTimeframe
                    StandardRotations.InitMyLayout "INIT"
                    Screen.MousePointer = 0
                End If
                chkWork(Index).Value = 0
            End If
        Case Else
    End Select
End Sub

Private Sub ShrinkUpdateList()
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim NxtLine As Long
    Dim ForeColor As Long
    Dim BackColor As Long
    Dim CurRegn As String
    Dim NxtRegn As String
    Dim CurFlca As String
    Dim NxtFlcd As String
    Dim CurFtyp As String
    Dim NxtFtyp As String
    MaxLine = ExtractData.GetLineCount - 1
    If MaxLine >= 0 Then
        Select Case DataSystemType
            Case "FHK"
                While MaxLine >= 0
                    ExtractData.GetLineColor MaxLine, ForeColor, BackColor
                    If BackColor = LightGray Then ExtractData.DeleteLine MaxLine
                    MaxLine = MaxLine - 1
                Wend
            Case "OAFOS"
                CurLine = 0
                While CurLine < MaxLine
                    NxtLine = CurLine + 1
                    CurRegn = Trim(ExtractData.GetColumnValue(CurLine, 22))
                    NxtRegn = Trim(ExtractData.GetColumnValue(NxtLine, 22))
                    If (CurRegn <> "") And (CurRegn = NxtRegn) Then
                        CurFtyp = Trim(ExtractData.GetColumnValue(CurLine, 23))
                        NxtFtyp = Trim(ExtractData.GetColumnValue(NxtLine, 23))
                        If (CurFtyp = "") And (NxtFtyp = "") Then
                            CurFlca = Trim(ExtractData.GetColumnValue(CurLine, 3))
                            NxtFlcd = Trim(ExtractData.GetColumnValue(NxtLine, 5))
                            If (CurFlca <> "") And (NxtFlcd <> "") Then
                                ExtractData.SetColumnValue CurLine, 1, " "
                                ExtractData.SetColumnValue CurLine, 5, ExtractData.GetColumnValue(NxtLine, 5)
                                ExtractData.SetColumnValue CurLine, 6, ExtractData.GetColumnValue(NxtLine, 6)
                                ExtractData.SetColumnValue CurLine, 15, ExtractData.GetColumnValue(NxtLine, 15)
                                ExtractData.SetColumnValue CurLine, 17, ExtractData.GetColumnValue(NxtLine, 17)
                                ExtractData.SetColumnValue CurLine, 18, ExtractData.GetColumnValue(NxtLine, 18)
                                ExtractData.SetColumnValue CurLine, 20, ExtractData.GetColumnValue(NxtLine, 20)
                                ExtractData.SetLineColor CurLine, vbBlack, vbWhite
                                ExtractData.DeleteLine NxtLine
                                MaxLine = MaxLine - 1
                            End If
                        End If
                    End If
                    CurLine = CurLine + 1
                Wend
            Case Else
        End Select
    End If
    ExtractData.RedrawTab
End Sub

Private Sub CheckUpdateActions()
    Dim MsgTxt As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim FromLine As Long
    Dim ToLine As Long
    Dim PosX As Long
    Dim PosY As Long
    Dim CodeCol As Long
    Dim FlcaCol As Long
    Dim VptoCol As Long
    Dim CurFlight As String
    Dim NxtFlight As String
    Dim tmpData As String
    MaxLine = ExtractData.GetLineCount
    If MaxLine > 0 Then
        If (AlcErrorList <> "") And (Not MsgCalled) Then
            MsgTxt = "Attention:" & vbNewLine
            MsgTxt = MsgTxt + "You selected airlines with erroneous records!" + vbNewLine
            MsgTxt = MsgTxt + "Thus the Check function cannot work properly!" + vbNewLine
            MsgTxt = MsgTxt + "List of concerned airlines:" + vbNewLine
            MsgTxt = MsgTxt + AlcErrorList
            MsgCalled = True
            If MyMsgBox.CallAskUser(0, 0, 0, "Action Control", MsgTxt, "hand", "", UserAnswer) >= 0 Then DoNothing
        End If
        Screen.MousePointer = 11
        Select Case DataSystemType
            Case "FHK"
                ExpandFhkDailyFlights
                CodeCol = 2
                FlcaCol = 3
                VptoCol = 8
                CurFlight = ""
                FromLine = -1
                StatusBar.Panels(7).Text = "Evaluating Daily Deletes & Updates ..."
                MaxLine = ExtractData.GetLineCount
                For CurLine = 0 To MaxLine
                    NxtFlight = TabGetColumnValues(CurLine, FlcaCol, VptoCol)
                    If NxtFlight <> CurFlight Then
                        ToLine = CurLine - 1
                        If CurFlight <> "" Then
                            CheckUpdatePairs FromLine, ToLine
                        End If
                        FromLine = CurLine
                        CurFlight = NxtFlight
                    End If
                Next
                StatusBar.Panels(7).Text = ""
            Case "OAFOS"
                MaxLine = MaxLine - 1
                For CurLine = 0 To MaxLine
                    CurFlight = Trim(ExtractData.GetColumnValue(CurLine, 22))   'REGN
                    If CurFlight <> "" Then
                        tmpData = Trim(ExtractData.GetColumnValue(CurLine, 23))   'FTYP
                        If tmpData = "" Then
                            tmpData = Trim(ExtractData.GetColumnValue(CurLine, 14))   'STOA
                            If tmpData = "" Then tmpData = Trim(ExtractData.GetColumnValue(CurLine, 15)) 'STOD
                            CurFlight = CurFlight + tmpData
                        Else
                            CurFlight = ""
                        End If
                    Else
                    End If
                    ExtractData.SetColumnValue CurLine, 24, CurFlight
                Next
                ExtractData.Sort "24", True, True
                For CurLine = 0 To MaxLine
                    ExtractData.SetColumnValue CurLine, 24, ""
                Next
            Case Else
        End Select
        If Val(ErrCnt.Caption) > 0 Then
            chkWork(29).Enabled = True
            ErrCnt.Visible = True
        Else
            chkWork(29).Enabled = False
        End If
        ExtractData.RedrawTab
        Screen.MousePointer = 0
    Else
        PosX = Me.Left + chkWork(21).Left + 100 * Screen.TwipsPerPixelX
        PosY = Me.Top + chkWork(21).Top + 100 * Screen.TwipsPerPixelY
        MsgTxt = "There's nothing to check!"
        If MyMsgBox.CallAskUser(0, PosX, PosY, "Action Control", MsgTxt, "infomsg", "", UserAnswer) >= 0 Then DoNothing
        chkWork(21).Value = 0
    End If
End Sub
Private Sub ExpandFhkDailyFlights()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim NewLine As Long
    Dim ForeColor As Long
    Dim BackColor As Long
    Dim IsValidWeek As Boolean
    Dim IsValidFreq As Boolean
    Dim CurFhkRec As String
    Dim CurVpfr As String
    Dim CurVpto As String
    Dim CurFrqd As String
    Dim CurStoa As String
    Dim CurStod As String
    Dim CurWkdy As String
    Dim CurDate As String
    Dim CurSort As String
    Dim tmpEmpty As String
    Dim FirstDay
    Dim CurrDay
    Dim LastDay
    StatusBar.Panels(7).Text = "Expanding Period Timeframes ..."
    tmpEmpty = Space(18)
    MaxLine = ExtractData.GetLineCount - 1
    NewLine = MaxLine
    For CurLine = 0 To MaxLine
        CurFhkRec = ExtractData.GetLineValues(0)
        ExtractData.GetLineColor 0, ForeColor, BackColor
        CurVpfr = ExtractData.GetColumnValue(0, 7)
        CurVpto = ExtractData.GetColumnValue(0, 8)
        CurFrqd = ExtractData.GetColumnValue(0, 9)
        CurStoa = Trim(ExtractData.GetColumnValue(0, 14))
        CurStod = Trim(ExtractData.GetColumnValue(0, 15))
        CurSort = tmpEmpty
        If CurStoa <> "" Then
            Mid(CurSort, 1) = ExtractData.GetColumnValue(0, 3)
            Mid(CurSort, 4) = ExtractData.GetColumnValue(0, 4)
            Mid(CurSort, 10) = "A"
        End If
        If CurStod <> "" Then
            Mid(CurSort, 1) = ExtractData.GetColumnValue(0, 5)
            Mid(CurSort, 4) = ExtractData.GetColumnValue(0, 6)
            Mid(CurSort, 10) = "D"
        End If
        ExtractData.DeleteLine 0
        NewLine = NewLine - 1
        FirstDay = CedaDateToVb(CurVpfr)
        LastDay = CedaDateToVb(CurVpto)
        CurrDay = FirstDay
        IsValidWeek = True
        While CurrDay <= LastDay
            CurWkdy = Trim(Str(Weekday(CurrDay, vbMonday)))
            If IsValidWeek And (InStr(CurFrqd, CurWkdy) > 0) Then
                NewLine = NewLine + 1
                ExtractData.InsertTextLine CurFhkRec, False
                ExtractData.SetLineColor NewLine, ForeColor, BackColor
                CurDate = Format(CurrDay, "yyyymmdd")
                ExtractData.SetColumnValue NewLine, 7, CurDate
                ExtractData.SetColumnValue NewLine, 8, CurDate
                ExtractData.SetColumnValue NewLine, 9, FormatFrequency(CurWkdy, "123", IsValidFreq)
                If CurStoa <> "" Then
                    Mid(CurStoa, 1, 8) = CurDate
                    ExtractData.SetColumnValue NewLine, 14, CurStoa
                    Mid(CurSort, 10) = "A"
                End If
                If CurStod <> "" Then
                    Mid(CurStod, 1, 8) = CurDate
                    ExtractData.SetColumnValue NewLine, 15, CurStod
                    Mid(CurSort, 10) = "D"
                End If
                Mid(CurSort, 11) = CurDate
                ExtractData.SetColumnValue NewLine, 24, CurSort
            End If
            CurrDay = DateAdd("d", 1, CurrDay)
        Wend
    Next
    ExtractData.Sort "24", True, True
    ExtractData.RedrawTab
    StatusBar.Panels(7).Text = ""
End Sub
Private Sub CheckUpdatePairs(FromLine As Long, ToLine As Long)
    Dim CurLineD As Long
    Dim CurLineN As Long
    Dim LinePosD As Long
    Dim LinePosN As Long
    Dim ActCodeD As String
    Dim ActCodeN As String
    Dim ValuesD As String
    Dim ValuesN As String
    Dim CountD As Long
    Dim CountN As Long
    'Look for identical pairs of New/Delete
    For CurLineD = FromLine To ToLine
        ActCodeD = ExtractData.GetColumnValue(CurLineD, 2)
        If ActCodeD = "D" Then
            ValuesD = TabGetColumnValues(CurLineD, 9, 20)
            For CurLineN = FromLine To ToLine
                If ExtractData.GetColumnValue(CurLineN, 1) <> "-" Then
                    ActCodeN = ExtractData.GetColumnValue(CurLineN, 2)
                    If ActCodeN = "N" Then
                        ValuesN = TabGetColumnValues(CurLineN, 9, 20)
                        If ValuesD = ValuesN Then
                            ExtractData.SetColumnValue CurLineD, 1, "-"
                            ExtractData.SetColumnValue CurLineD, 24, "N/D Pair: " & Trim(ExtractData.GetColumnValue(CurLineN, 0))
                            ExtractData.SetLineColor CurLineD, vbBlack, LightGray
                            ExtractData.SetColumnValue CurLineN, 1, "-"
                            ExtractData.SetColumnValue CurLineN, 24, "N/D Pair: " & Trim(ExtractData.GetColumnValue(CurLineD, 0))
                            ExtractData.SetLineColor CurLineN, vbBlack, LightGray
                            Exit For
                        End If
                    End If
                End If
            Next
        End If
    Next
    'Look for identical Deletes
    For CurLineD = FromLine To ToLine - 1
        If ExtractData.GetColumnValue(CurLineD, 1) <> "-" Then
            ActCodeD = ExtractData.GetColumnValue(CurLineD, 2)
            If ActCodeD = "D" Then
                ValuesD = TabGetColumnValues(CurLineD, 9, 20)
                For CurLineN = CurLineD + 1 To ToLine
                    If ExtractData.GetColumnValue(CurLineN, 1) <> "-" Then
                        ActCodeN = ExtractData.GetColumnValue(CurLineN, 2)
                        If ActCodeN = "D" Then
                            ValuesN = TabGetColumnValues(CurLineN, 9, 20)
                            If ValuesD = ValuesN Then
                                ExtractData.SetColumnValue CurLineN, 1, "-"
                                ExtractData.SetColumnValue CurLineN, 24, "D/D Identical: " & Trim(ExtractData.GetColumnValue(CurLineD, 0))
                                ExtractData.SetLineColor CurLineN, vbBlack, LightGray
                            End If
                        End If
                    End If
                Next
            End If
        End If
    Next
    'Look for identical New Values
    For CurLineD = FromLine To ToLine - 1
        If ExtractData.GetColumnValue(CurLineD, 1) <> "-" Then
            ActCodeD = ExtractData.GetColumnValue(CurLineD, 2)
            If ActCodeD = "N" Then
                ValuesD = TabGetColumnValues(CurLineD, 9, 20)
                For CurLineN = CurLineD + 1 To ToLine
                    If ExtractData.GetColumnValue(CurLineN, 1) <> "-" Then
                        ActCodeN = ExtractData.GetColumnValue(CurLineN, 2)
                        If ActCodeN = "N" Then
                            ValuesN = TabGetColumnValues(CurLineN, 9, 20)
                            If ValuesD = ValuesN Then
                                ExtractData.SetColumnValue CurLineD, 1, "-"
                                ExtractData.SetColumnValue CurLineD, 24, "N/N Identical: " & Trim(ExtractData.GetColumnValue(CurLineN, 0))
                                ExtractData.SetLineColor CurLineD, vbBlack, LightGray
                            End If
                        End If
                    End If
                Next
            End If
        End If
    Next
    'Count New and deletes (should not be more than one)
    CountD = 0
    CountN = 0
    LinePosD = -1
    LinePosN = -1
    For CurLineD = FromLine To ToLine
        If ExtractData.GetColumnValue(CurLineD, 1) <> "-" Then
            ActCodeD = ExtractData.GetColumnValue(CurLineD, 2)
            If ActCodeD = "N" Then
                CountN = CountN + 1
                LinePosN = CurLineD
            End If
            If ActCodeD = "D" Then
                CountD = CountD + 1
                LinePosD = CurLineD
            End If
        End If
    Next
    If (CountD > 1) Or (CountN > 1) Then
        'something must be wrong
        For CurLineD = FromLine To ToLine
            If ExtractData.GetColumnValue(CurLineD, 1) <> "-" Then
                ExtractData.SetColumnValue CurLineD, 1, "E"
                ExtractData.SetLineColor CurLineD, vbWhite, vbRed
                ErrCnt.Caption = Trim(Str(Val(ErrCnt) + 1))
            End If
        Next
    Else
        'Ignore the Delete if a New line exists
        If (LinePosD >= 0) And (LinePosN >= 0) Then
            If LinePosN >= 0 Then
                ExtractData.SetColumnValue LinePosD, 1, "-"
                ExtractData.SetColumnValue LinePosD, 24, "Valid N: " & Trim(ExtractData.GetColumnValue(LinePosN, 0))
                ExtractData.SetLineColor LinePosD, vbBlack, LightGray
            End If
        End If
    End If
End Sub

Private Function TabGetColumnValues(LineNo As Long, FirstCol As Long, LastCol As Long) As String
    Dim Result As String
    Dim CurCol As Long
    Result = ""
    For CurCol = FirstCol To LastCol
        Result = Result & ExtractData.GetColumnValue(LineNo, CurCol) & ","
    Next
    TabGetColumnValues = Result
End Function
Public Sub CopyAllFlights(LoopStart As Long, LoopEnd As Long)
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim FlightLine As Long
    Dim LinIdx As Long
    Dim LineNo As Long
    Dim FlcaCol As Integer
    Dim FlcdCol As Integer
    Dim FlnaCol As Integer
    Dim FlndCol As Integer
    Dim MyBackColor As Long
    Dim MyForeColor As Long
    Dim IsValidLine As Boolean
    Dim RecData As String
    Dim ColData As String
    Dim ErrFlg1 As String
    Dim ErrFlg2 As String
    Dim AlcItem As String
    Dim ChkItem As String
    Dim Spfr As String
    Dim Spto As String
    Dim Flna As String
    Dim Flnd As String
    Dim ArrCnt As Long
    Dim DepCnt As Long
    Dim RotCnt As Long
    Dim ValidLines As Long
    Dim ErrorFilter As Long
    Dim LoadFilter As Long
    Dim FredFilter As String
    Dim i As Integer
    ErrCnt.Caption = "0"
    ErrCnt.Visible = False
    If (LoopStart <> LoopEnd) And (chkWork(6).Value = 0) Then
        ResetList
        'ExtractData.ResetContent
        'For i = 1 To 7
        '    StatusBar.Panels(i).Text = ""
        'Next
    End If
    ErrorFilter = chkWork(2).Value + chkWork(3).Value
    LoadFilter = chkWork(0).Value + chkWork(1).Value + chkWork(5).Value + ErrorFilter
    If (LoadFilter > 0) And (CheckDateInput(True) = True) Then
        StatusBar.Panels(7).Text = "Reading data from main viewer ..."
        Refresh
        Screen.MousePointer = 11
        MainDialog.InitPeriodCheck WorkPeriodCols, False
        Spfr = ""
        Spfr = Spfr & Trim(txtVpfr(0).Text)
        Spfr = Spfr & Trim(txtVpfr(1).Text)
        Spfr = Spfr & Trim(txtVpfr(2).Text)
        Spto = ""
        Spto = Spto & Trim(txtVpto(0).Text)
        Spto = Spto & Trim(txtVpto(1).Text)
        Spto = Spto & Trim(txtVpto(2).Text)
        FredFilter = RecFilter.chkWkDay(0).Tag
        If FredFilter = "......." Then FredFilter = ""
        LinIdx = ExtractData.GetLineCount - 1
        ValidLines = Val(StatusBar.Panels(1).Text)
        ArrCnt = Val(StatusBar.Panels(4).Text)
        DepCnt = Val(StatusBar.Panels(5).Text)
        RotCnt = Val(StatusBar.Panels(6).Text)
        If NewLayoutType Then
            FlcaCol = GetItemNo(myViewFieldList, "FLCA") + 3
            FlcdCol = GetItemNo(myViewFieldList, "FLCA") + 3
            FlnaCol = GetItemNo(myViewFieldList, "FLTA") + 3
            FlndCol = GetItemNo(myViewFieldList, "FLTD") + 3
            If FlcaCol < 4 Then
                FlcaCol = GetItemNo(myViewFieldList, "FLCB") + 3
                FlcdCol = GetItemNo(myViewFieldList, "FLCB") + 3
                FlnaCol = GetItemNo(myViewFieldList, "FLTB") + 3
                FlndCol = GetItemNo(myViewFieldList, "FLTB") + 3
            End If
        Else
            FlcaCol = 4
            FlcdCol = 6
            FlnaCol = 5
            FlndCol = 7
        End If
        CurLine = LoopStart
        If LoopEnd >= 0 Then
            MaxLine = LoopEnd
        Else
            MaxLine = MainDialog.FileData(CurFdIdx).GetLineCount - 1
        End If
        AlcErrorList = ","
        While CurLine <= MaxLine
            RecData = GetFlightRecord(CurLine, FlightLine)
            IsValidLine = True
            If ErrorFilter = 0 Then
                If IsValidLine Then
                    If chkWork(0).Value = 1 Then IsValidLine = RecFilter.CheckFilterKeys(RecData, IsValidLine)
                    If IsValidLine Then
                        ErrFlg1 = GetItem(RecData, 2, ",")
                        If (ErrFlg1 = "E") Or (ErrFlg1 = "-") Then IsValidLine = False
                        ErrFlg2 = GetItem(RecData, 3, ",")
                        If (ErrFlg2 = "E") Or (ErrFlg2 = "-") Then IsValidLine = False
                        If Not IsValidLine Then
                            If (ErrFlg1 = "E") Or (ErrFlg2 = "E") Then
                                AlcItem = GetItem(RecData, FlcaCol, ",")
                                If AlcItem <> "" Then
                                    ChkItem = "," + AlcItem + ","
                                    If InStr(AlcErrorList, ChkItem) = 0 Then AlcErrorList = AlcErrorList + AlcItem + ","
                                End If
                                AlcItem = GetItem(RecData, FlcdCol, ",")
                                If AlcItem <> "" Then
                                    ChkItem = "," + AlcItem + ","
                                    If InStr(AlcErrorList, ChkItem) = 0 Then AlcErrorList = AlcErrorList + AlcItem + ","
                                End If
                            End If
                        End If
                    End If
                    If IsValidLine Then ValidLines = ValidLines + 1
                    IsValidLine = MainDialog.CheckFlightPeriod(Spfr, Spto, RecData, IsValidLine, True, True, FredFilter)
                End If
            Else
                IsValidLine = False
                If chkWork(2).Value = 1 Then
                    ColData = GetItem(RecData, 2, ",")
                    If ColData = "E" Then IsValidLine = True
                End If
                If chkWork(3).Value = 1 Then
                    ColData = GetItem(RecData, 3, ",")
                    If ColData = "-" Then IsValidLine = True
                End If
                If IsValidLine Then ValidLines = ValidLines + 1
                IsValidLine = RecFilter.CheckFilterKeys(RecData, IsValidLine)
            End If
            If IsValidLine Then
                RecData = RecData & ",,,,,,,,,,,,,,,,,,,,,,"
                'Due to UniqueColumns ...
                LinIdx = ExtractData.GetLineCount - 1
                ExtractData.InsertTextLine RecData, False
                LineNo = ExtractData.GetLineCount - 1
                If LineNo > LinIdx Then
                    If Trim(ExtractData.GetColumnValue(LineNo, 0)) <> "" Then
                        ExtractData.SetColumnValue LinIdx, myMainIdxCol, Str(FlightLine)
                        If chkWork(2).Value = 0 Then
                            MainDialog.FileData(CurFdIdx).GetLineColor FlightLine, MyForeColor, MyBackColor
                            ExtractData.SetLineColor LineNo, MyForeColor, MyBackColor
                        End If
                        Flna = GetItem(RecData, FlnaCol, ",")
                        Flnd = GetItem(RecData, FlndCol, ",")
                        If Flna <> "" Then ArrCnt = ArrCnt + 1
                        If Flnd <> "" Then DepCnt = DepCnt + 1
                        If (Flna <> "") And (Flnd <> "") Then RotCnt = RotCnt + 1
                    Else
                        ExtractData.DeleteLine LineNo
                    End If
                End If
                If (LineNo Mod 100) = 0 Then
                    ExtractData.OnVScrollTo LineNo
                    ExtractData.Refresh
                End If
            End If
        Wend
        ChkErrorList = AlcErrorList
        AlcErrorList = Mid(AlcErrorList, 2)
        If AlcErrorList <> "" Then
            AlcErrorList = Left(AlcErrorList, Len(AlcErrorList) - 1)
            AlcErrorList = Replace(AlcErrorList, ",", ", ", 1, -1, vbBinaryCompare)
        End If
        StatusBar.Panels(1).Text = CStr(ValidLines)
        StatusBar.Panels(2).Text = CStr(ExtractData.GetLineCount)
        StatusBar.Panels(3).Text = CStr(ArrCnt + DepCnt)
        StatusBar.Panels(4).Text = CStr(ArrCnt)
        StatusBar.Panels(5).Text = CStr(DepCnt)
        StatusBar.Panels(6).Text = CStr(RotCnt)
        StatusBar.Panels(7).Text = ""
        ExtractData.OnVScrollTo 0
        ExtractData.AutoSizeByHeader = True
        ExtractData.AutoSizeColumns
        ExtractData.Refresh
        If Not CheckDateInput(False) Then DoNothing
        Me.Refresh
        Screen.MousePointer = 0
    End If
End Sub

Private Function GetFlightRecord(CurLine As Long, FlightLine As Long) As String
Dim RecData As String
    Select Case MainDialog.ExtractSystemType
        Case 1  'Rotation Model (SLOT)
            RecData = MainDialog.FileData(CurFdIdx).GetLineValues(CurLine)
            MainDialog.GetSlotRecInfo RecData, CurLine, -1, FlightLine, "X", True
        Case 2  'Leg Model (OAFOS/SSIM)
            RecData = MainDialog.BuildSlotRecord(CurLine, FlightLine)
            CurLine = CurLine - 1
        Case 3  'Linked Flight Model (FHK)
            RecData = MainDialog.FileData(CurFdIdx).GetLineValues(CurLine)
            MainDialog.GetSlotRecInfo RecData, CurLine, -1, FlightLine, "F", True
        Case Else
            'Undefined. Take what you see.
            RecData = MainDialog.FileData(CurFdIdx).GetLineValues(CurLine)
            If NewLayoutType Then
                RecData = TranslateRecordData(RecData, MainFieldList, myViewFieldList)
            End If
            FlightLine = CurLine
    End Select
    CurLine = CurLine + 1
    GetFlightRecord = RecData
End Function

Private Sub ExtractData_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim TopLine As Long
    Dim VisLine As Long
    If Selected And LineNo >= 0 Then
        TopLine = ExtractData.GetVScrollPos
        VisLine = TopLine + ((ExtractData.Height / Screen.TwipsPerPixelY) / ExtractData.LineHeight) - 2
        If (LineNo < TopLine) Or (LineNo > VisLine) Then ExtractData.OnVScrollTo LineNo - 1
        VisLine = Val(ExtractData.GetColumnValue(LineNo, 25))
        If MainDialog.FileData(CurFdIdx).GetCurrentSelected <> VisLine Then
            MainDialog.FileData(CurFdIdx).SetCurrentSelection VisLine
            'ExtractData.SetFocus
        End If
    Else
        If LineNo < 0 Then
            If MainDialog.FileData(CurFdIdx).GetCurrentSelected <> -1 Then
                MainDialog.FileData(CurFdIdx).SetCurrentSelection -1
            End If
        End If
    End If
End Sub

Private Sub ExtractData_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim ArrFltn As String
    Dim DepFltn As String
    If LineNo >= 0 Then
        If chkWork(29).Value = 1 Then
            If ErrCnt.Visible = True Then
                If Trim(ExtractData.GetColumnValue(LineNo, 1)) = "E" Then
                    ExtractData.SetColumnValue LineNo, 1, ""
                    ArrFltn = ExtractData.GetColumnValue(LineNo, 4)
                    DepFltn = ExtractData.GetColumnValue(LineNo, 6)
                    If (ArrFltn <> "") And (DepFltn <> "") Then
                        ExtractData.SetLineColor LineNo, vbBlack, vbWhite
                    ElseIf ArrFltn <> "" Then
                        ExtractData.SetLineColor LineNo, vbBlack, vbYellow
                    Else
                        ExtractData.SetLineColor LineNo, vbBlack, vbGreen
                    End If
                    ErrCnt.Caption = Trim(Str(Val(ErrCnt.Caption) - 1))
                    If Val(ErrCnt.Caption) <= 0 Then
                        ErrCnt.Visible = False
                        chkWork(29).Value = 0
                        chkWork(29).Enabled = False
                    End If
                    ExtractData.RedrawTab
                End If
            End If
        End If
    Else
        'ExtractData.Sort Str(ColNo), True, True
        'ExtractData.RedrawTab
    End If
End Sub

Private Sub Form_Activate()
    Static AllSet As Boolean
    If (UpdateMode) And (Not AllSet) Then
        chkWork(1).Value = 1
        AllSet = True
    Else
        If chkWork(0).Value = 1 Then
            If RecFilter.Tag = "AUTOCLOSE" Then
                RecFilter.Tag = ""
                FilterCalledAsDispo = False
                If MySetUp.MonitorSetting(3).Value = 1 Then
                    RecFilter.Show , Me
                Else
                    RecFilter.Show
                End If
                RecFilter.ArrangeFormLayout
            End If
        End If
    End If
End Sub

Private Sub ExtractData_GotFocus()
    ExtractData.SelectTextColor = vbWhite
    ExtractData.RedrawTab
End Sub
Private Sub ExtractData_LostFocus()
    ExtractData.SelectTextColor = NormalGray
    ExtractData.RedrawTab
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    'If KeyCode = vbKeyF1 Then DisplayHtmHelpFile "", Me.Name, Me.ActiveControl.Name
    If KeyCode = vbKeyF1 Then ShowHtmlHelp Me, "", ""
End Sub

Private Sub Form_Load()
    Me.Top = MainDialog.Top + MainDialog.FileData(CurFdIdx).Top + (MainDialog.Height - MainDialog.ScaleHeight) - 3 * Screen.TwipsPerPixelY
    Me.Left = MainDialog.Left
    Me.Width = MainDialog.Width
    InitDetailList 10, True
    ExtractData.LifeStyle = True
    Me.Height = MainDialog.Height - MainDialog.FileData(CurFdIdx).Top - MainDialog.StatusBar.Height - 5 * Screen.TwipsPerPixelY
    If UpdateMode Then
        chkWork(6).Enabled = False
        chkWork(7).Enabled = False
        chkWork(15).Enabled = False
        chkWork(22).Enabled = False
        chkWork(21).Top = chkWork(6).Top
        chkWork(21).Visible = True
        chkWork(27).Top = chkWork(6).Top
        chkWork(27).Visible = True
        chkWork(6).Visible = False
        chkWork(2).Visible = False
    Else
        'Implicits FullMode or RotationMode
        chkWork(6).Visible = True
        chkWork(2).Visible = True
        chkWork(21).Visible = False
        chkWork(27).Visible = False
        If RotationMode Then
            chkWork(7).Enabled = False
            chkWork(15).Enabled = False
            chkWork(22).Enabled = False
        End If
        chkWork(0).Value = 1
    End If
    Me.Visible = True
    Me.Refresh
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Me.Hide
    MainDialog.chkExtract.Value = 0
    MainDialog.Show
End Sub

Private Sub Form_Resize()
    Dim NewVal As Long
    NewVal = Me.ScaleHeight - StatusBar.Height - ExtractData.Top - 2 * Screen.TwipsPerPixelY
    If NewVal > 50 Then
        ExtractData.Height = NewVal
    End If
    NewVal = Me.ScaleWidth - 2 * ExtractData.Left '- 5 * Screen.TwipsPerPixelX
    If NewVal > 50 Then ExtractData.Width = NewVal
End Sub

Private Sub txtVpfr_Change(Index As Integer)
    If Not CheckDateInput(False) Then DoNothing
End Sub

Private Sub txtVpfr_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Private Sub txtVpto_Change(Index As Integer)
    If Not CheckDateInput(False) Then DoNothing
End Sub

Private Function CheckDateInput(FullCheck As Boolean) As Boolean
    Dim RetVal As Boolean
    Dim Spfr As String
    Dim Spto As String
    Dim chkSpfr As String
    Dim chkSpto As String
    Dim ReqTxt As String
    RetVal = True
    Spfr = ""
    Spfr = Spfr & Trim(txtVpfr(0).Text)
    Spfr = Spfr & Trim(txtVpfr(1).Text)
    Spfr = Spfr & Trim(txtVpfr(2).Text)
    Spto = ""
    Spto = Spto & Trim(txtVpto(0).Text)
    Spto = Spto & Trim(txtVpto(1).Text)
    Spto = Spto & Trim(txtVpto(2).Text)
    If FullCheck Then
        chkSpfr = DateInputFormatToCeda(Spfr, "yyyymmdd")
        chkSpto = DateInputFormatToCeda(Spto, "yyyymmdd")
        If (chkSpfr = Spfr) And (chkSpto = Spto) Then
            txtVpfr(0).Tag = Spfr
            txtVpfr(3).Tag = Spfr & Trim(txtVpfr(3).Text) & Trim(txtVpfr(4).Text)
            txtVpto(0).Tag = Spto
            txtVpto(3).Tag = Spto & Trim(txtVpto(3).Text) & Trim(txtVpto(4).Text)
        Else
            If chkSpfr = "" Then chkSpfr = "???"
            If chkSpto = "" Then chkSpto = "???"
            ReqTxt = ""
            ReqTxt = ReqTxt & "Invalid period data:" & vbNewLine & vbNewLine
            ReqTxt = ReqTxt & "P.FR: " & Spfr & " converted to " & chkSpfr & vbNewLine
            ReqTxt = ReqTxt & "P.TO: " & Spto & " converted to " & chkSpto & vbNewLine & vbNewLine
            ReqTxt = ReqTxt & "Using format 'yyyymmdd'"
            If MyMsgBox.CallAskUser(0, 0, 0, "Action Control", ReqTxt, "stop", "", UserAnswer) >= 0 Then DoNothing
            RetVal = False
        End If
    Else
        If ExtractData.GetLineCount <= 0 Then
            txtVpfr(0).Tag = Spfr
            txtVpfr(3).Tag = Spfr & Trim(txtVpfr(3).Text) & Trim(txtVpfr(4).Text)
            txtVpto(0).Tag = Spto
            txtVpto(3).Tag = Spto & Trim(txtVpto(3).Text) & Trim(txtVpto(4).Text)
        End If
    End If
    CheckLoadFilter False
    CheckDateInput = RetVal
End Function

Public Sub CheckLoadFilter(ForLoad As Boolean)
Dim Result As String
Dim i As Integer
    Result = ""
    For i = 0 To 3
        Result = Result & Str(chkWork(i).Value) & ","
    Next
    Result = Result & txtVpfr(0).Tag & ","
    Result = Result & txtVpto(0).Tag & ","
    If chkWork(0).Value = 1 Then
        Result = Result & RecFilter.chkTool(2).Tag
    Else
        Result = Result & "ALL"
    End If
    If ForLoad = True Then chkWork(4).Tag = Result
    If chkWork(4).Tag <> Result Then
        'MsgBox "NE" & vbNewLine & chkWork(4).Tag & vbNewLine & Result & vbNewLine & RecFilter.chkTool(2).Tag
        chkWork(4).BackColor = vbRed
        chkWork(4).ForeColor = vbWhite
        chkWork(13).Enabled = False
        chkWork(21).Enabled = False
        chkWork(27).Enabled = False
        chkWork(28).Enabled = False
    Else
        'MsgBox "EQ" & vbNewLine & chkWork(4).Tag & vbNewLine & Result & vbNewLine & RecFilter.chkTool(2).Tag
        chkWork(4).BackColor = vbButtonFace
        chkWork(4).ForeColor = vbBlack
        chkWork(13).Enabled = True
        chkWork(21).Enabled = True
        chkWork(27).Enabled = True
        chkWork(28).Enabled = False
        'if FullMode Then chkWork(28).Enabled = True
        'If DataSystemType <> "OAFOS" Then chkWork(28).Enabled = True
    End If
End Sub

Private Sub CheckErrorFilter()
Dim ErrorFilter As Integer
Dim EditEnable As Boolean
Dim i As Integer
    ErrorFilter = chkWork(2).Value + chkWork(3).Value
    If ErrorFilter = 0 Then EditEnable = True Else EditEnable = False
    If FullMode Then
        PeriodLabel.Enabled = EditEnable
        For i = 0 To 2
            txtVpfr(i).Enabled = EditEnable
            txtVpto(i).Enabled = EditEnable
        Next
        For i = 7 To 9
            chkWork(i).Enabled = EditEnable
        Next
    Else
    End If
End Sub

Private Sub DisplayFlightCalendar()
Dim SelLine As Long
Dim SelData As String
Dim ColData As String
Dim MyErrMsg As String
    If chkWork(7).Value = 1 Then
        SelLine = ExtractData.GetCurrentSelected
        If SelLine >= 0 Then SelData = ExtractData.GetLineValues(SelLine) Else SelData = ""
        MyErrMsg = ""
        If SelData <> "" Then
            ColData = ExtractData.GetColumnValue(SelLine, 1)
            If ColData = "E" Then
                MyErrMsg = "Won't display 'Error' line."
            End If
            If ColData = "-" Then
                MyErrMsg = "Won't display 'Ignore Overnight' line."
            End If
            ColData = ExtractData.GetColumnValue(SelLine, 2)
            If ColData = "-" Then
                MyErrMsg = "Won't display 'Ignore' line."
            End If
        Else
            MyErrMsg = "Won't display empty line."
        End If
        If (SelLine >= 0) And (MyErrMsg = "") Then
            FlightDetails.GetFlightCalendar
            If MySetUp.MonitorSetting(3).Value = 1 Then
                FlightDetails.Show , Me
            Else
                FlightDetails.Show
            End If
            chkWork(7).BackColor = LightestGreen
        Else
            MyMsgBox.InfoApi 0, "Click on the desired (valid) line and try it again.", "More info"
            If MyMsgBox.CallAskUser(0, 0, 0, "Action Control", MyErrMsg, "hand", "", UserAnswer) >= 0 Then DoNothing
            chkWork(7).Value = 0
        End If
    Else
        FlightDetails.Hide
        chkWork(7).BackColor = vbButtonFace
    End If
End Sub

Private Sub PrepareCedaEvent(Index As Integer, LineCheck As Integer, ActionType As String)
Dim SelLine As Long
Dim MaxLine As Long
Dim ErrLvl As Integer
Dim UsrAns As Integer
Dim SelData As String
Dim ColData As String
Dim MyErrMsg As String
Dim ButtonLable As String
Dim CurDay
Dim IpfrDate As String
Dim VpfrDate As String
Dim ListOfRecs As String

    If chkWork(Index).Value = 1 Then
        ErrLvl = 0
        MyErrMsg = ""
        MaxLine = ExtractData.GetLineCount
        If MaxLine > 0 Then
            SelLine = ExtractData.GetCurrentSelected
            If SelLine < 0 Then
                If chkWork(LineCheck).Value = 0 Then SelLine = 0
            End If
            If SelLine >= 0 Then SelData = ExtractData.GetLineValues(SelLine) Else SelData = ""
            If SelData <> "" Then
                ColData = ExtractData.GetColumnValue(SelLine, 1)
                If ColData = "E" Then
                    MyErrMsg = "Won't import 'Error' line."
                    ErrLvl = 1
                End If
                If ColData = "-" Then
                    MyErrMsg = "Won't import 'Ignore Overnight' line."
                    ErrLvl = 1
                End If
                ColData = ExtractData.GetColumnValue(SelLine, 2)
                If ColData = "-" Then
                    MyErrMsg = "Won't import 'Ignore' line."
                    ErrLvl = 1
                End If
            Else
                MyErrMsg = "Won't import empty line."
                ErrLvl = 1
            End If
        Else
            MyErrMsg = "Won't import empty list."
            ErrLvl = 1
        End If
        If ErrLvl = 0 Then
            'If CedaIsConnected Then
                'MsgBox "CedaIsOn"
            'Else
                CurDay = DateAdd("d", Val(MySetUp.AutoClear.Caption), Now)
                IpfrDate = Format(CurDay, "yyyymmdd")
            'End If
            VpfrDate = txtVpfr(0).Tag
            Select Case DataSystemType
                Case "OAFOS"
                    CurDay = DateAdd("h", 1, Now)
                    IpfrDate = Format(CurDay, "yyyymmddhhmm")
                    VpfrDate = txtVpfr(3).Tag
                Case "SLOT", "FHK", "FREE"
                Case "SSIM"
                Case "SSIM45"
                Case Else
                    VpfrDate = "SYSTEM: " & DataSystemType
                    IpfrDate = "UNKNOWN"
                    ErrLvl = 3
            End Select
            
            If VpfrDate < IpfrDate Then
                MyErrMsg = ActionType & vbNewLine
                MyErrMsg = MyErrMsg & "Won't import data of the past." & vbNewLine & vbNewLine
                MyErrMsg = MyErrMsg & "Next import period begin: " & IpfrDate & vbNewLine
                MyErrMsg = MyErrMsg & "Selected period begin: " & VpfrDate & vbNewLine & vbNewLine
                MyErrMsg = MyErrMsg & "Do you want to continue ?"
                If ErrLvl = 0 Then ErrLvl = 2
            End If
        End If
        If ErrLvl > 0 Then
            Select Case ErrLvl
                Case 1
                    ButtonLable = "OK"
                Case 2
                    ButtonLable = "YES,NO;F"
                Case 3
                    ButtonLable = "YES,NO;F"
                Case Else
            End Select
            UsrAns = MyMsgBox.CallAskUser(0, 0, 0, ActionType & " Action Control", MyErrMsg, "hand", ButtonLable, UserAnswer)
            If (UsrAns = 1) And ((ErrLvl = 3) Or ((ErrLvl = 2) And (ActionType = "IMPORT"))) Then
                
                UsrAns = MyMsgBox.CallAskUser(0, 0, 0, ActionType & " Action Control", "Sorry.", "stop2", "", UserAnswer)
'BERNI
'ErrLvl = 0
            ElseIf (UsrAns = 1) And (ErrLvl = 2) And (ActionType = "PREVIEW") Then
                If ErrLvl < 3 Then ErrLvl = 0
            End If
        End If
        If (ErrLvl = 0) And (SelLine >= 0) Then
            'Here we call the import function
            If chkWork(LineCheck).Value = 1 Then
                ListOfRecs = Str(SelLine)
            Else
                ListOfRecs = "SENDALL"
            End If
            'MsgBox ListOfRecs
            BuildCedaEvent ActionType, ListOfRecs
        End If
        chkWork(Index).Value = 0
    End If
End Sub

Private Sub BuildCedaEvent(ActionType As String, ListOfRecs As String)
Dim ArrFldLst As String
Dim DepFldLst As String
Dim ArrColLst As String
Dim DepColLst As String
Dim tmpMsg As String
Dim tmpCap As String
    'MsgBox ActionType & " / " & DataSystemType
    'MsgBox RecData & vbNewLine & FirstLine & " / " & LastLine & " / " & FlightLine
    Me.MousePointer = 11
    Me.Refresh
    'HardCoded
    Select Case DataSystemType
        Case "OAFOS"
            If ActionType = "PREVIEW" Then MainDialog.OaFosPreview
            If ActionType = "IMPORT" Then MainDialog.OaFosImport
        Case "SLOT", "FHK", "FREE"
            'ATTENTION: the last 4 fields always must be: FLNO,AIRL,FLTN,FLNS !!!
            ArrFldLst = "VPFR,VPTO,FRQD,FRQW,ORG3,VIAL,STOA,ACT3,STYP,FLNO,AIRL,FLTN,FLNS"
            ArrColLst = "8   ,9   ,10  ,22  ,13  ,14  ,15  ,11  ,20  ,-1  ,4   ,5   ,-1  "
            DepFldLst = "VPFR,VPTO,FRQD,FRQW,DES3,VIAL,STOD,ACT3,STYP,FLNO,AIRL,FLTN,FLNS"
            DepColLst = "8   ,9   ,10  ,22  ,19  ,18  ,16  ,11  ,21  ,-1  ,6   ,7   ,-1  "
            ArrFldLst = Replace(ArrFldLst, "STYP", UseStypField, 1, -1, vbBinaryCompare)
            DepFldLst = Replace(DepFldLst, "STYP", UseStypField, 1, -1, vbBinaryCompare)
            SendSlotRecsToCeda 1, ActionType, ListOfRecs, ArrFldLst, ArrColLst, DepFldLst, DepColLst
        Case "SCORE"
            'ATTENTION: the last 4 fields always must be: FLNO,AIRL,FLTN,FLNS !!!
            ArrFldLst = "VPFR,VPTO,FRQD,FRQW,ORG3,VIAL,STOA,ACT3,STYP,FLNO,AIRL,FLTN,FLNS"
            ArrColLst = "8   ,9   ,10  ,22  ,13  ,14  ,15  ,11  ,20  ,-1  ,4   ,5   ,-1  "
            DepFldLst = "VPFR,VPTO,FRQD,FRQW,DES3,VIAL,STOD,ACT3,STYP,FLNO,AIRL,FLTN,FLNS"
            DepColLst = "8   ,9   ,10  ,22  ,19  ,18  ,16  ,11  ,21  ,-1  ,6   ,7   ,-1  "
            ArrFldLst = Replace(ArrFldLst, "STYP", UseStypField, 1, -1, vbBinaryCompare)
            DepFldLst = Replace(DepFldLst, "STYP", UseStypField, 1, -1, vbBinaryCompare)
            SendSlotRecsToCeda 1, ActionType, ListOfRecs, ArrFldLst, ArrColLst, DepFldLst, DepColLst
        Case "SSIM"
            'ATTENTION: the last 4 fields always must be: FLNO,AIRL,FLTN,FLNS !!!
            ArrFldLst = "VPFR,VPTO,FRQD,FRQW,ORG3,VIAL,STOA,ACT3,STYP,FLNO,AIRL,FLTN,FLNS"
            ArrColLst = "8   ,9   ,10  ,22  ,13  ,14  ,15  ,11  ,20  ,-1  ,4   ,5   ,-1  "
            DepFldLst = "VPFR,VPTO,FRQD,FRQW,DES3,VIAL,STOD,ACT3,STYP,FLNO,AIRL,FLTN,FLNS"
            DepColLst = "8   ,9   ,10  ,22  ,19  ,18  ,16  ,11  ,21  ,-1  ,6   ,7   ,-1  "
            ArrFldLst = Replace(ArrFldLst, "STYP", UseStypField, 1, -1, vbBinaryCompare)
            DepFldLst = Replace(DepFldLst, "STYP", UseStypField, 1, -1, vbBinaryCompare)
            SendSlotRecsToCeda 2, ActionType, ListOfRecs, ArrFldLst, ArrColLst, DepFldLst, DepColLst
        Case "SSIM45"
            'ATTENTION: the last 4 fields always must be: FLNO,AIRL,FLTN,FLNS !!!
            ArrFldLst = "VPFR,VPTO,FRQD,FRQW,ORG3,VIAL,STOA,ACT3,STYP,FLNO,AIRL,FLTN,FLNS"
            ArrColLst = "8   ,9   ,10  ,22  ,13  ,14  ,15  ,11  ,20  ,-1  ,4   ,5   ,-1  "
            DepFldLst = "VPFR,VPTO,FRQD,FRQW,DES3,VIAL,STOD,ACT3,STYP,FLNO,AIRL,FLTN,FLNS"
            DepColLst = "8   ,9   ,10  ,22  ,19  ,18  ,16  ,11  ,21  ,-1  ,6   ,7   ,-1  "
            ArrFldLst = Replace(ArrFldLst, "STYP", UseStypField, 1, -1, vbBinaryCompare)
            DepFldLst = Replace(DepFldLst, "STYP", UseStypField, 1, -1, vbBinaryCompare)
            SendSlotRecsToCeda 2, ActionType, ListOfRecs, ArrFldLst, ArrColLst, DepFldLst, DepColLst
        Case Else
    End Select
    Me.MousePointer = 0
End Sub

Private Sub SendSlotRecsToCeda(UsedList As Integer, ActionType As String, ListOfRecs As String, OutArrFldLst As String, ArrColLst As String, OutDepFldLst As String, DepColLst As String)
Dim SendAll As Boolean
Dim MaxPackets As Integer
Dim RecData As String
Dim LinCnt As Integer
Dim ItmCnt As Integer
Dim CurLinNbr As String
Dim CurLine As Long
Dim MaxLine As Long
Dim FirstLine As Long
Dim LastLine As Long
Dim FlightLine As Long
Dim FltLinItm As String
Dim LinLstMain As String
Dim LinLstExtr As String
Dim ArrFldLst As String
Dim DepFldLst As String
Dim ArrDatLst As String
Dim DepDatLst As String
Dim ItmNbr As Integer
Dim ColNbr As String
Dim ColIdx As Integer
Dim tmpData As String
Dim CedaFields As String
Dim CedaData As String
Dim CedaCmd As String
Dim CedaTable As String
Dim CedaSelKey As String
Dim SrvRetVal As Integer
Dim SrvAnsw As String
Dim AftAirl As String
Dim AftFltn As String
Dim AftFlns As String
Dim ActionCode As String
Dim CurTrky As String

Dim tmpItem As Integer
Dim tmpLine As Long

'Meaning of UsedList Indicator
'1 = Original SLOT File
'2 = Converted SSIM File (to SLOT)

    ActionCode = Left(ActionType, 1)
    SrvRetVal = UfisServer.CallCeda(SrvAnsw, "GNU", CedaTable, CedaFields, CedaData, CedaSelKey, "", 0, True, False)
    CurTrky = GetItem(SrvAnsw, 1, ",")
    UfisServer.aCeda.Tws = CurTrky
    
    MousePointer = 11

    MaxPackets = 4
    
    ArrFldLst = OutArrFldLst & ",DES3,FTYP"
    DepFldLst = OutDepFldLst & ",ORG3,FTYP"
    
    CedaCmd = "EXCO"
    CedaTable = "AFTTAB"
    CedaSelKey = "IMP," & ActionType
    CedaFields = ArrFldLst & vbLf & DepFldLst & vbLf
    
    If ListOfRecs = "SENDALL" Then SendAll = True Else SendAll = False
    MaxLine = ExtractData.GetLineCount - 1
    
    LinLstMain = ""
    LinLstExtr = ""
    CedaData = ""
    LinCnt = 0
    CurLine = -1
    Do
        ItmCnt = ItmCnt + 1
        If SendAll Then
            CurLine = CurLine + 1
            If CurLine > MaxLine Then CurLine = -1
        Else
            CurLinNbr = GetItem(ListOfRecs, ItmCnt, ",")
            CurLine = Val(CurLinNbr)
            If CurLinNbr = "" Then CurLine = -1
        End If
        If CurLine >= 0 Then
            LinCnt = LinCnt + 1
            RecData = ExtractData.GetLineValues(CurLine)
            'FirstLine = Val(GetItem(RecData, 26, ","))
            'LastLine = Val(GetItem(RecData, 27, ","))
            FltLinItm = GetItem(RecData, 28, ",")
            FlightLine = Val(FltLinItm)
            LinLstExtr = LinLstExtr & Trim(Str(CurLine)) & ","
            LinLstMain = LinLstMain & Trim(Str(FlightLine)) & ","
            
            ArrDatLst = BuildFltDataLine("A", OutArrFldLst, ArrColLst, RecData)
            ArrDatLst = ArrDatLst & "," & HomeAirport & "," & DefaultFtyp
            
            DepDatLst = BuildFltDataLine("D", OutDepFldLst, DepColLst, RecData)
            DepDatLst = DepDatLst & "," & HomeAirport & "," & DefaultFtyp
            
            CedaData = CedaData & ArrDatLst & vbLf & DepDatLst & vbLf
            
            If LinCnt >= MaxPackets Then
                
                'MsgBox CedaFields & vbNewLine & CedaData
                SrvRetVal = UfisServer.CallCeda(SrvAnsw, CedaCmd, CedaTable, CedaFields, CedaData, CedaSelKey, "", 0, False, False)
                LinLstMain = LinLstMain & "-1"
                LinLstExtr = LinLstExtr & "-1"
                tmpItem = 0
                Do
                    tmpItem = tmpItem + 1
                    tmpLine = Val(GetItem(LinLstMain, tmpItem, ","))
                    If tmpLine >= 0 Then
                        MainDialog.FileData(CurFdIdx).SetColumnValue tmpLine, 2, ActionCode
                        MainDialog.FileData(CurFdIdx).SetColumnValue tmpLine, MainDialog.ExtFldCnt, CurTrky
                    End If
                    tmpLine = Val(GetItem(LinLstExtr, tmpItem, ","))
                    If tmpLine >= 0 Then
                        ExtractData.SetColumnValue tmpLine, 2, ActionCode
                        ExtractData.SetColumnValue tmpLine, 24, CurTrky
                    End If
                Loop While tmpLine >= 0
                Me.ExtractData.RedrawTab
                MainDialog.FileData(CurFdIdx).RedrawTab
                LinCnt = 0
                CedaData = ""
                LinLstMain = ""
                LinLstExtr = ""
            End If
        End If
    Loop While CurLine >= 0
    If LinCnt > 0 Then
        'MsgBox CedaFields & vbNewLine & CedaData
        SrvRetVal = UfisServer.CallCeda(SrvAnsw, CedaCmd, CedaTable, CedaFields, CedaData, CedaSelKey, "", 0, False, False)
        LinLstMain = LinLstMain & "-1"
        LinLstExtr = LinLstExtr & "-1"
        tmpItem = 0
        Do
            tmpItem = tmpItem + 1
            tmpLine = Val(GetItem(LinLstMain, tmpItem, ","))
            If tmpLine >= 0 Then
                MainDialog.FileData(CurFdIdx).SetColumnValue tmpLine, 2, ActionCode
                MainDialog.FileData(CurFdIdx).SetColumnValue tmpLine, MainDialog.ExtFldCnt, CurTrky
            End If
            tmpLine = Val(GetItem(LinLstExtr, tmpItem, ","))
            If tmpLine >= 0 Then
                ExtractData.SetColumnValue tmpLine, 2, ActionCode
                ExtractData.SetColumnValue tmpLine, 24, CurTrky
            End If
        Loop While tmpLine >= 0
        ExtractData.RedrawTab
        MainDialog.FileData(CurFdIdx).RedrawTab
    End If
    MousePointer = 0
End Sub

Private Function BuildFltDataLine(ForWhat As String, FldLst As String, ItmLst As String, RecDat As String) As String
    Dim RecDatLst As String
    Dim OldFldNam As String
    Dim NewFldNam As String
    Dim ItmNbr As Integer
    Dim ColNbr As String
    Dim ColIdx As Integer
    Dim tmpData As String
    Dim tmpLen As Integer
    Dim tmpFlno As String
    Dim tmpAirl As String
    Dim tmpFltn As String
    Dim tmpFlns As String
    Dim tmpRout As String
    Dim tmpSffx As String
    Dim tmpFldLst As String
    Dim tmpColLst As String
    Dim ValidField As Boolean
    RecDatLst = ""
    ItmNbr = 0
    Do
        ItmNbr = ItmNbr + 1
        OldFldNam = GetItem(FldLst, ItmNbr, ",")
        ColNbr = GetItem(ItmLst, ItmNbr, ",")
        If ColNbr <> "" Then
            Select Case OldFldNam
                Case "ORG3"
                    If InStr(myViewFieldList, "ORG3") = 0 Then
                        tmpData = ""
                        ColNbr = TranslateItemList("ROUT", myViewFieldList, True)
                        ColIdx = Val(ColNbr) + 3 'Header Fields
                        If ColIdx >= 3 Then
                            ValidField = True
                            tmpRout = GetItem(RecDat, ColIdx, ",")
                            Select Case ForWhat
                                Case "A"
                                    tmpData = GetDataFromRoute(ForWhat, "ORG", tmpRout)
                                Case "D"
                                    tmpData = GetDataFromRoute(ForWhat, "ORG", tmpRout)
                                Case Else
                            End Select
                        End If
                        If ValidField Then RecDatLst = RecDatLst & tmpData & ","
                        ValidField = False
                        NewFldNam = ""
                    End If
                Case "DES3"
                    If InStr(myViewFieldList, "DES3") = 0 Then
                        tmpData = ""
                        ColNbr = TranslateItemList("ROUT", myViewFieldList, True)
                        ColIdx = Val(ColNbr) + 3
                        If ColIdx >= 3 Then
                            ValidField = True
                            tmpRout = GetItem(RecDat, ColIdx, ",")
                            Select Case ForWhat
                                Case "A"
                                    tmpData = GetDataFromRoute(ForWhat, "DST", tmpRout)
                                Case "D"
                                    tmpData = GetDataFromRoute(ForWhat, "DST", tmpRout)
                                Case Else
                            End Select
                        End If
                        If ValidField Then RecDatLst = RecDatLst & tmpData & ","
                        ValidField = False
                        NewFldNam = ""
                    End If
                Case "VIAL"
                    If InStr(myViewFieldList, "ROUT") > 0 Then
                        tmpData = ""
                        ColNbr = TranslateItemList("ROUT", myViewFieldList, True)
                        ColIdx = Val(ColNbr) + 3
                        If ColIdx >= 3 Then
                            ValidField = True
                            tmpRout = GetItem(RecDat, ColIdx, ",")
                            Select Case ForWhat
                                Case "A"
                                    tmpData = GetDataFromRoute(ForWhat, "PRV", tmpRout)
                                Case "D"
                                    tmpData = GetDataFromRoute(ForWhat, "NXT", tmpRout)
                                Case Else
                            End Select
                            If (tmpData <> "") And (MainDialog.UseVialFlag = "YES") Then
                                tmpData = "1" & tmpData
                            Else
                                tmpData = " " & tmpData
                            End If
                        End If
                        If ValidField Then RecDatLst = RecDatLst & tmpData & ","
                        ValidField = False
                        NewFldNam = ""
                    End If
                Case "FRQD"
                    NewFldNam = "FREQ"
                Case "FRQW"
                    NewFldNam = "FREW"
                Case "STYP"
                    Select Case ForWhat
                        Case "A"
                            NewFldNam = "NATA"
                        Case "D"
                            NewFldNam = "NATD"
                        Case Else
                    End Select
                Case "FLNO"
                    tmpFlno = ""
                    Select Case ForWhat
                        Case "A"
                            NewFldNam = ""
                        Case "D"
                            NewFldNam = ""
                        Case Else
                    End Select
                    NewFldNam = ""
                Case "AIRL"
                    tmpAirl = ""
                    Select Case ForWhat
                        Case "A"
                            NewFldNam = "FLCA"
                        Case "D"
                            NewFldNam = "FLCD"
                        Case Else
                    End Select
                    ColNbr = TranslateItemList(NewFldNam, myViewFieldList, True)
                    ColIdx = Val(ColNbr) + 3 'Header Fields
                    If ColIdx >= 3 Then
                        ValidField = True
                        tmpData = GetItem(RecDat, ColIdx, ",")
                        tmpAirl = tmpData
                    End If
                    NewFldNam = ""
                Case "FLTN"
                    tmpFltn = ""
                    Select Case ForWhat
                        Case "A"
                            NewFldNam = "FLTA"
                        Case "D"
                            NewFldNam = "FLTD"
                        Case Else
                    End Select
                    ColNbr = TranslateItemList(NewFldNam, myViewFieldList, True)
                    ColIdx = Val(ColNbr) + 3 'Header Fields
                    If ColIdx >= 3 Then
                        ValidField = True
                        tmpData = GetItem(RecDat, ColIdx, ",")
                        If tmpData <> "" Then
                            tmpSffx = Right(tmpData, 1)
                            If InStr("0123456789", tmpSffx) = 0 Then
                                tmpData = Left(tmpData, Len(tmpData) - 1)
                            End If
                        End If
                        tmpFltn = tmpData
                    End If
                    NewFldNam = ""
                Case "FLNS"
                    tmpFlns = ""
                    Select Case ForWhat
                        Case "A"
                            'NewFldNam = "FLSA"
                            NewFldNam = "FLTA"
                        Case "D"
                            'NewFldNam = "FLSD"
                            NewFldNam = "FLTD"
                        Case Else
                    End Select
                    ColNbr = TranslateItemList(NewFldNam, myViewFieldList, True)
                    ColIdx = Val(ColNbr) + 3 'Header Fields
                    If ColIdx >= 3 Then
                        ValidField = True
                        tmpData = GetItem(RecDat, ColIdx, ",")
                        If tmpData <> "" Then
                            tmpSffx = Right(tmpData, 1)
                            tmpData = ""
                            If InStr("0123456789", tmpSffx) = 0 Then
                                tmpData = tmpSffx
                            End If
                        End If
                        tmpFlns = tmpData
                    End If
                    NewFldNam = ""
                Case Else
                NewFldNam = OldFldNam
            End Select
        End If
        If NewFldNam <> "" Then
            tmpColLst = TranslateItemList(NewFldNam, myViewFieldList, True)
            ColIdx = Val(tmpColLst) + 3 'Header Fields
            ColNbr = CStr(ColIdx)
            If tmpColLst = "-1" Then
                tmpData = OldFldNam
            End If
            
            If ColNbr <> "" Then
                ColIdx = Val(ColNbr)
                If ColIdx >= 0 Then
                    tmpData = GetItem(RecDat, ColIdx, ",")
                    ValidField = True
                    'HardCoded
                    Select Case NewFldNam
                        Case "STOA"
                            tmpLen = Len(tmpData)
                            If tmpLen > 0 Then tmpData = tmpData & "00"
                        Case "STOD"
                            tmpLen = Len(tmpData)
                            If tmpLen > 0 Then tmpData = tmpData & "00"
                        Case "VIAL"
                            If (tmpData <> "") And (MainDialog.UseVialFlag = "YES") Then
                                tmpData = "1" & tmpData
                            Else
                                tmpData = " " & tmpData
                            End If
                        Case "FLNO"
                            tmpFlno = tmpData
                            ValidField = False
                        Case "AIRL"
                            tmpAirl = tmpData
                            ValidField = False
                        Case "FLTN"
                            tmpFltn = tmpData
                            ValidField = False
                        Case "FLNS"
                            tmpFlns = tmpData
                            ValidField = False
                        Case Else
                    End Select
                    If ValidField Then RecDatLst = RecDatLst & tmpData & ","
                End If
            End If
        End If
    Loop While ColNbr <> ""
    tmpFlno = BuildProperAftFlno(tmpAirl, tmpFltn, tmpFlns)
    RecDatLst = RecDatLst & tmpFlno & ","
    RecDatLst = RecDatLst & tmpAirl & ","
    RecDatLst = RecDatLst & tmpFltn & ","
    RecDatLst = RecDatLst & tmpFlns
    BuildFltDataLine = RecDatLst
End Function
Public Function GetDataFromRoute(ForAdid As String, ForWhat As String, UseRoute As String) As String
    Dim Result As String
    Dim AptGrp As String
    Dim tmpApc3 As String
    Dim itm As Integer
    Result = ""
    itm = 0
    AptGrp = "START"
    While AptGrp <> ""
        itm = itm + 1
        AptGrp = GetItem(UseRoute, itm, "|")
        If AptGrp <> "" Then
            tmpApc3 = GetItem(AptGrp, 1, ":")
            If tmpApc3 <> HomeAirport Then
                Select Case ForWhat
                    Case "ORG"
                        If itm = 1 Then Result = tmpApc3
                        AptGrp = ""
                    Case "PRV"
                        If itm > 1 Then Result = tmpApc3
                    Case "NXT"
                        If itm > 1 Then Result = tmpApc3
                        AptGrp = ""
                    Case "DST"
                        If itm > 1 Then Result = tmpApc3
                    Case Else
                End Select
            Else
            End If
        End If
    Wend
    GetDataFromRoute = Result
End Function
Public Sub ResetList()
    Dim i As Integer
    ExtractData.ResetContent
    ExtractData.Refresh
    For i = 1 To 7
        StatusBar.Panels(i).Text = ""
    Next
End Sub

Public Sub ClearExtractList()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim ArrFltn As String
    Dim DepFltn As String
    MaxLine = ExtractData.GetLineCount - 1
    CurLine = 0
    While CurLine <= MaxLine
        ArrFltn = Trim(ExtractData.GetColumnValue(CurLine, 4))
        DepFltn = Trim(ExtractData.GetColumnValue(CurLine, 6))
        If (ArrFltn = "") Or (DepFltn = "") Then
            ExtractData.DeleteLine CurLine
            MaxLine = MaxLine - 1
            CurLine = CurLine - 1
        End If
        CurLine = CurLine + 1
    Wend
End Sub

Private Sub CheckMyMainHeader()
    If myHeaderTitle <> "" Then
        ExtractData.SetMainHeaderValues myHeaderWidth, myHeaderTitle, myHeaderColor
        ExtractData.SetMainHeaderFont MySetUp.FontSlider.Value + 6, False, False, True, 0, "Courier New"
        ExtractData.MainHeader = True
    Else
        ExtractData.MainHeader = False
    End If
End Sub


Private Sub txtVpto_GotFocus(Index As Integer)
    SetTextSelected
End Sub
