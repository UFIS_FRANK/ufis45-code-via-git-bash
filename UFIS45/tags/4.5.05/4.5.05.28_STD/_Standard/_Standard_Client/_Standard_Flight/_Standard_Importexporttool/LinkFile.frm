VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form LinkFile 
   Caption         =   "Rotation Link File"
   ClientHeight    =   9000
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9210
   Icon            =   "LinkFile.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   9000
   ScaleWidth      =   9210
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox chkLoad 
      Caption         =   "Transmit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   900
      Style           =   1  'Graphical
      TabIndex        =   24
      Top             =   30
      Width           =   855
   End
   Begin VB.TextBox txtVpfr 
      Alignment       =   2  'Center
      BackColor       =   &H0080FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   1770
      TabIndex        =   18
      Top             =   30
      Width           =   585
   End
   Begin VB.TextBox txtVpto 
      Alignment       =   2  'Center
      BackColor       =   &H0080FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   3270
      TabIndex        =   21
      Top             =   30
      Width           =   585
   End
   Begin VB.TextBox txtVpfr 
      Alignment       =   2  'Center
      BackColor       =   &H0080FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   2370
      TabIndex        =   19
      Top             =   30
      Width           =   345
   End
   Begin VB.TextBox txtVpfr 
      Alignment       =   2  'Center
      BackColor       =   &H0080FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   2730
      TabIndex        =   20
      Top             =   30
      Width           =   345
   End
   Begin VB.TextBox txtVpto 
      Alignment       =   2  'Center
      BackColor       =   &H0080FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   3870
      TabIndex        =   22
      Top             =   30
      Width           =   345
   End
   Begin VB.TextBox txtVpto 
      Alignment       =   2  'Center
      BackColor       =   &H0080FFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   4230
      TabIndex        =   23
      Top             =   30
      Width           =   345
   End
   Begin VB.TextBox txtActUtcDiff 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5040
      TabIndex        =   17
      Text            =   "0"
      Top             =   2640
      Width           =   510
   End
   Begin VB.CheckBox chkLoc 
      Caption         =   "LOC"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5580
      Style           =   1  'Graphical
      TabIndex        =   16
      Top             =   2640
      Width           =   615
   End
   Begin VB.CheckBox chkUtc 
      Caption         =   "UTC"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4410
      Style           =   1  'Graphical
      TabIndex        =   15
      Top             =   2640
      Width           =   615
   End
   Begin VB.TextBox txtCurFile 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   60
      TabIndex        =   5
      Top             =   390
      Width           =   5325
   End
   Begin VB.CheckBox chkClose 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4590
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   30
      Width           =   855
   End
   Begin VB.CheckBox chkRequest 
      Caption         =   "Select"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   30
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   30
      Width           =   855
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   2
      Top             =   8685
      Width           =   9210
      _ExtentX        =   16245
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   15716
         EndProperty
      EndProperty
   End
   Begin VB.TextBox FileNote 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1845
      Left            =   60
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   1
      Top             =   750
      Width           =   4425
   End
   Begin TABLib.TAB FileData 
      Height          =   1695
      Left            =   60
      TabIndex        =   0
      Top             =   2970
      Width           =   4305
      _Version        =   65536
      _ExtentX        =   7594
      _ExtentY        =   2990
      _StockProps     =   64
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "->"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3075
      TabIndex        =   25
      Top             =   60
      Width           =   210
   End
   Begin VB.Label lblRemark 
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6225
      TabIndex        =   14
      ToolTipText     =   "Total Lines"
      Top             =   2640
      Width           =   525
   End
   Begin VB.Label lblTotalErr 
      Alignment       =   1  'Right Justify
      BackColor       =   &H000000FF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   3840
      MousePointer    =   10  'Up Arrow
      TabIndex        =   13
      ToolTipText     =   "Errors"
      Top             =   2640
      Width           =   525
   End
   Begin VB.Label lblTotalWarn 
      Alignment       =   1  'Right Justify
      BackColor       =   &H008080FF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3300
      MousePointer    =   10  'Up Arrow
      TabIndex        =   12
      ToolTipText     =   "Warnings"
      Top             =   2640
      Width           =   525
   End
   Begin VB.Label lblTotalRot 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FF0000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   2760
      MousePointer    =   10  'Up Arrow
      TabIndex        =   11
      ToolTipText     =   "Total Rotations"
      Top             =   2640
      Width           =   525
   End
   Begin VB.Label lblTotalOvn 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2220
      MousePointer    =   10  'Up Arrow
      TabIndex        =   10
      ToolTipText     =   "Overnight Rotations"
      Top             =   2640
      Width           =   525
   End
   Begin VB.Label lblTotalDay 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1680
      MousePointer    =   10  'Up Arrow
      TabIndex        =   9
      ToolTipText     =   "Daylight Rotations"
      Top             =   2640
      Width           =   525
   End
   Begin VB.Label lblTotalDep 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0000FF00&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1140
      MousePointer    =   10  'Up Arrow
      TabIndex        =   8
      ToolTipText     =   "Single Departures"
      Top             =   2640
      Width           =   525
   End
   Begin VB.Label lblTotalArr 
      Alignment       =   1  'Right Justify
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   600
      MousePointer    =   10  'Up Arrow
      TabIndex        =   7
      ToolTipText     =   "Single Arrivals"
      Top             =   2640
      Width           =   525
   End
   Begin VB.Label lblTotalLines 
      Alignment       =   1  'Right Justify
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   60
      TabIndex        =   6
      ToolTipText     =   "Total Lines"
      Top             =   2640
      Width           =   525
   End
End
Attribute VB_Name = "LinkFile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public NoEcho As Boolean
Public DontCallTwice As Boolean
Public IsActivated As Boolean

Private Sub chkClose_Click()
    If chkClose.Value = 1 Then
        chkClose.Value = 0
        If StandardRotations.Visible Then StandardRotations.SetFocus
        Me.Hide
    End If
End Sub

Private Sub chkLoad_Click()
    If chkLoad.Value = 1 Then
        chkUtc.Value = 1
        'StandardRotations.chkWork(0).Value = 1
        StandardRotations.CheckStrTabLoader "FOSDAT"
        chkLoad.Value = 0
        Me.Refresh
    End If
End Sub

Private Sub chkLoc_Click()
    If chkLoc.Value = 1 Then
        chkLoc.BackColor = LightGreen
        chkUtc.Value = 0
        ToggleUtcLocal Val(txtActUtcDiff.Text)
    Else
        chkLoc.BackColor = vbButtonFace
        chkUtc.Value = 1
    End If
End Sub

Private Sub chkRequest_Click()
    Dim LinkFilter As String
    Dim LinkPath As String
    Dim LinkName As String
    Dim UseFile As String
    Dim FileCount As Integer
    UseFile = ""
    If chkRequest.Value = 1 Then
        chkRequest.BackColor = LightestGreen
        LinkFilter = GetIniEntry(myIniFullName, myIniSection, "", "LINK_FILTER", "Link Files|*.not|ASCII Files|*.txt|All|*.*")
        If chkRequest.Tag = "" Then chkRequest.Tag = CurLoadPath & "\" & "*.*"
        FileCount = GetFileAndPath(chkRequest.Tag, LinkName, LinkPath)
        UfisTools.CommonDialog.DialogTitle = "Open link file of: " & DataSystemType
        UfisTools.CommonDialog.InitDir = LinkPath
        UfisTools.CommonDialog.FileName = ""
        UfisTools.CommonDialog.Filter = LinkFilter
        UfisTools.CommonDialog.FilterIndex = 1
        UfisTools.CommonDialog.Flags = (cdlOFNHideReadOnly)
        UfisTools.CommonDialog.CancelError = True
        On Error GoTo ErrorHandle
        UfisTools.CommonDialog.ShowOpen
        If UfisTools.CommonDialog.FileName <> "" Then
            UseFile = UfisTools.CommonDialog.FileName
            txtCurFile.Text = UseFile
            chkRequest.Tag = UseFile
            ReadLinkFile
        End If
        chkRequest.Value = 0
    Else
        chkRequest.BackColor = vbButtonFace
    End If
    Exit Sub
ErrorHandle:
    'myLoadName = ""
    'myLoadPath = ""
    chkRequest.Value = 0
    Exit Sub
End Sub
Private Sub ReadLinkFile()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpFile As String
    Dim CurFileName As String
    Dim TxtFileName As String
    Dim NotFileName As String
    Dim CurFilePath As String
    Dim CurFileExt As String
    Dim ReadLine As String
    Dim TabLine As String
    Dim tmpBuff As String
    Dim tmpCode As String
    Dim tmpSeas As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim tmpFlta As String
    Dim tmpFltd As String
    Dim tmpStoa As String
    Dim tmpStod As String
    Dim tmpHome As String
    Dim tmpOrg3 As String
    Dim tmpDes3 As String
    Dim tmpDay As String
    Dim ErrText As String
    Dim FileHdl As Integer
    Dim TurnType As Integer
    Dim LineStatus As Long
    tmpFile = chkRequest.Tag
    If tmpFile <> "" Then
        ErrText = ""
        TxtFileName = ""
        GetFileAndPath tmpFile, CurFileName, CurFilePath
        CurFileExt = UCase(GetItem(CurFileName, 2, "."))
        CurFileName = GetItem(CurFileName, 1, ".")
        If (CurFileExt = "NOT") Or (CurFileExt = "TXT") Then
            NotFileName = CurFilePath & "\" & CurFileName & ".not"
            TxtFileName = CurFilePath & "\" & CurFileName & ".txt"
        Else
            'ErrText = "That's not a valid Rotation Link File !!"
        End If
        If TxtFileName <> "" Then
            FileNote.Text = ""
            FileData.ResetContent
            FileNote.Refresh
            FileData.Refresh
            chkLoc.Value = 1
            tmpDay = Format(Now, "yyyymmdd")
            tmpSeas = SeasonData.GetValidSeason(tmpDay)
            If Left(tmpSeas, 1) = "W" Then txtActUtcDiff.Text = "120" Else txtActUtcDiff.Text = "180"
            If ExistFile(NotFileName) Then
                FileHdl = FreeFile
                Open NotFileName For Input As #FileHdl
                While Not EOF(FileHdl)
                    Line Input #FileHdl, ReadLine
                    tmpBuff = tmpBuff & ReadLine & vbNewLine
                Wend
                Close FileHdl
                FileNote.Text = tmpBuff
                tmpCode = GetItem(tmpBuff, 1, ":")
                tmpBuff = GetItem(tmpBuff, 2, ":")
                If Left(tmpCode, 1) = "W" Then txtActUtcDiff.Text = "120"
                If Left(tmpCode, 1) = "S" Then txtActUtcDiff.Text = "180"
            Else
                ErrText = ErrText & vbNewLine & "File not found: " & NotFileName
            End If
            tmpBuff = ""
            If ExistFile(TxtFileName) Then
                FileHdl = FreeFile
                Open TxtFileName For Input As #FileHdl
                While Not EOF(FileHdl)
                    Line Input #FileHdl, ReadLine
                    TabLine = ""
                    TabLine = TabLine & Mid(ReadLine, 1, 3) & ","
                    TabLine = TabLine & Mid(ReadLine, 4, 3) & ","
                    TabLine = TabLine & Mid(ReadLine, 7, 3) & ","
                    TabLine = TabLine & Mid(ReadLine, 10, 3) & ","
                    TabLine = TabLine & Mid(ReadLine, 13, 4) & ","
                    TabLine = TabLine & Mid(ReadLine, 17, 4) & ","
                    TabLine = TabLine & Mid(ReadLine, 21, 4) & ","
                    TabLine = TabLine & Mid(ReadLine, 25, 4) & ","
                    TabLine = TabLine & Mid(ReadLine, 29, 3) & ","
                    TabLine = TabLine & Mid(ReadLine, 32, 7) & ","
                    TabLine = TabLine & Mid(ReadLine, 39, 1) & ","
                    TabLine = TabLine & Mid(ReadLine, 40, 1) & ","
                    FileData.InsertTextLine TabLine, False
                Wend
                Close FileHdl
                FileData.AutoSizeColumns
                FileData.Refresh
                MaxLine = FileData.GetLineCount - 1
                For CurLine = 0 To MaxLine
                    LineStatus = 0
                    TurnType = 0
                    If LineStatus = 0 Then
                        tmpHome = FileData.GetColumnValue(CurLine, 2)
                        If tmpHome <> HomeAirport Then
                            FileData.SetDecorationObject CurLine, 2, "CellError"
                            LineStatus = 99
                        End If
                    End If
                    If LineStatus = 0 Then
                        tmpOrg3 = FileData.GetColumnValue(CurLine, 1)
                        If tmpOrg3 = HomeAirport Then
                            FileData.SetDecorationObject CurLine, 1, "CellError"
                            LineStatus = 98
                        End If
                        tmpDes3 = FileData.GetColumnValue(CurLine, 3)
                        If tmpDes3 = HomeAirport Then
                            FileData.SetDecorationObject CurLine, 3, "CellError"
                            LineStatus = 98
                        End If
                        tmpFlta = Trim(FileData.GetColumnValue(CurLine, 6))
                        If Len(tmpFlta) = 4 Then
                            TurnType = TurnType + 1
                            If Left(tmpFlta, 1) > "9" Then
                                FileData.SetDecorationObject CurLine, 6, "CellError"
                                If LineStatus = 0 Then LineStatus = 97
                            End If
                        End If
                        tmpFltd = Trim(FileData.GetColumnValue(CurLine, 7))
                        If Len(tmpFltd) = 4 Then
                            TurnType = TurnType + 2
                            If Left(tmpFltd, 1) > "9" Then
                                FileData.SetDecorationObject CurLine, 7, "CellError"
                                If LineStatus = 0 Then LineStatus = 97
                            End If
                        End If
                        tmpStoa = Trim(FileData.GetColumnValue(CurLine, 4))
                        If Len(tmpStoa) = 4 Then
                        End If
                    End If
                    If LineStatus = 0 Then
                        tmpStoa = Trim(FileData.GetColumnValue(CurLine, 4))
                        If Len(tmpStoa) <> 4 Then
                        End If
                        tmpStod = Trim(FileData.GetColumnValue(CurLine, 5))
                        If Len(tmpStod) <> 4 Then
                        End If
                        Select Case TurnType
                            Case 0
                                FileData.SetDecorationObject CurLine, 6, "CellError"
                                FileData.SetDecorationObject CurLine, 7, "CellError"
                                LineStatus = 96
                            Case 1
                                LineStatus = 1
                            Case 2
                                LineStatus = 2
                            Case 3
                                If tmpStod < tmpStoa Then
                                    FileData.SetLineColor CurLine, vbBlack, LightGray
                                    FileData.SetColumnValue CurLine, 12, "DEP +1"
                                Else
                                    FileData.SetLineColor CurLine, vbBlack, vbWhite
                                End If
                        End Select
                    End If
                    Select Case LineStatus
                        Case 0
                        Case 1
                            FileData.SetLineColor CurLine, vbBlack, vbYellow
                        Case 2
                            FileData.SetLineColor CurLine, vbBlack, vbGreen
                        Case 98
                            FileData.SetLineColor CurLine, vbBlack, LightRed
                        Case Else
                            FileData.SetLineColor CurLine, vbWhite, vbRed
                    End Select
                    FileData.SetLineStatusValue CurLine, LineStatus
                Next
                FileData.Refresh
            Else
                ErrText = ErrText & vbNewLine & "File not found: " & TxtFileName
            End If
            If ErrText <> "" Then
                FileNote.Text = FileNote.Text & ErrText
            End If
        End If
        PublishCounters
        chkLoad.Value = 1
    End If
End Sub
Private Sub ToggleUtcLocal(UseUtcDiff As Integer)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpDay As String
    Dim tmpStoa As String
    Dim tmpStod As String
    Dim tmpFreq As String
    Dim newFreq As String
    Dim tmpStoaTxt As String
    Dim tmpStodTxt As String
    Dim tmpChr As String
    Dim DayDiff As Integer
    Dim i As Integer
    Dim j As Integer
    
    Dim varStoa
    Dim varStod
    Dim varSkdA
    Dim varSkdD
    
    tmpDay = Format(Now, "yyyymmdd")
    MaxLine = FileData.GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpStoaTxt = Trim(FileData.GetColumnValue(CurLine, 4))
        tmpStodTxt = Trim(FileData.GetColumnValue(CurLine, 5))
        tmpFreq = FileData.GetColumnValue(CurLine, 9)
        newFreq = tmpFreq
        tmpStoa = ""
        tmpStod = ""
        If Len(tmpStoaTxt) = 4 Then
            tmpStoa = tmpDay & tmpStoaTxt
            varStoa = CedaFullDateToVb(tmpStoa)
            varSkdA = varStoa
        End If
        If Len(tmpStodTxt) = 4 Then
            tmpStod = tmpDay & tmpStodTxt
            varStod = CedaFullDateToVb(tmpStod)
            varSkdD = varStod
            If (tmpStoa <> "") And (varStod < varStoa) Then varStod = DateAdd("d", 1, varStod)
        End If
        If tmpStod <> "" Then
            varStod = DateAdd("n", UseUtcDiff, varStod)
            DayDiff = DateDiff("d", varSkdD, varStod)
        End If
        If tmpStoa <> "" Then
            varStoa = DateAdd("n", UseUtcDiff, varStoa)
            DayDiff = DateDiff("d", varSkdA, varStoa)
        End If
        If DayDiff <> 0 Then
            newFreq = "0000000"
            For i = 1 To 7
                tmpChr = Mid(tmpFreq, i, 1)
                If tmpChr = "1" Then
                    j = i + DayDiff
                    If j > 7 Then j = 1
                    If j < 1 Then j = 7
                    Mid(newFreq, j, 1) = "1"
                End If
            Next
        End If
        If tmpStoa <> "" Then
            tmpStoa = Format(varStoa, "hhmm")
            FileData.SetColumnValue CurLine, 4, tmpStoa
        End If
        If tmpStod <> "" Then
            tmpStod = Format(varStod, "hhmm")
            FileData.SetColumnValue CurLine, 5, tmpStod
        End If
        FileData.SetColumnValue CurLine, 9, newFreq
        If FileData.GetLineStatusValue(CurLine) = 0 Then
            DayDiff = DateDiff("d", varStoa, varStod)
            If DayDiff = 0 Then
                FileData.SetLineColor CurLine, vbBlack, vbWhite
                FileData.SetColumnValue CurLine, 12, ""
            Else
                FileData.SetLineColor CurLine, vbBlack, LightGray
                FileData.SetColumnValue CurLine, 12, "DEP +" & CStr(DayDiff)
            End If
        End If
    Next
    FileData.Refresh
    PublishCounters
End Sub
Private Sub PublishCounters()
    Dim tmpList As String
    FileData.SetInternalLineBuffer True
    lblTotalLines.Caption = CStr(FileData.GetLineCount)
    tmpList = FileData.GetLinesByBackColor(vbYellow)
    lblTotalArr.Caption = CStr(Val(tmpList))
    tmpList = FileData.GetLinesByBackColor(vbGreen)
    lblTotalDep.Caption = CStr(Val(tmpList))
    tmpList = FileData.GetLinesByBackColor(vbWhite)
    lblTotalDay.Caption = CStr(Val(tmpList))
    tmpList = FileData.GetLinesByBackColor(LightGray)
    lblTotalOvn.Caption = CStr(Val(tmpList))
    tmpList = FileData.GetLinesByBackColor(vbRed)
    lblTotalErr.Caption = CStr(Val(tmpList))
    tmpList = FileData.GetLinesByBackColor(LightRed)
    lblTotalWarn.Caption = CStr(Val(tmpList))
    FileData.SetInternalLineBuffer False
    lblTotalRot.Caption = CStr(Val(lblTotalDay.Caption) + Val(lblTotalOvn.Caption))
End Sub

Private Sub chkUtc_Click()
    If chkUtc.Value = 1 Then
        chkUtc.BackColor = LightGreen
        chkLoc.Value = 0
        ToggleUtcLocal -Val(txtActUtcDiff.Text)
    Else
        chkUtc.BackColor = vbButtonFace
        chkLoc.Value = 1
    End If
End Sub

Private Sub FileData_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim tmpUrno As String
    Dim tmpHit As String
    Dim CurLine As Long
    If (Selected) And (LineNo >= 0) And (Not DontCallTwice) Then
        If Not NoEcho Then
            tmpUrno = CStr(LineNo + StandardRotations.CurRotaUrno)
            tmpHit = StandardRotations.StrTabData(0).GetLinesByColumnValue(StandardRotations.StrUrnoCol, tmpUrno, 0)
            If tmpHit <> "" Then
                CurLine = Val(tmpHit)
                StandardRotations.NoEcho = True
                StandardRotations.StrTabData(0).OnVScrollTo CurLine - 3
                StandardRotations.StrTabData(0).SetCurrentSelection CurLine
                StandardRotations.NoEcho = False
                DontCallTwice = True
                FileData.SetFocus
                DontCallTwice = False
            End If
        End If
    End If
End Sub

Private Sub Form_Activate()
    Dim i As Integer
    For i = 0 To 2
        txtVpfr(i).Text = StandardRotations.txtVpfr(i).Text
        txtVpto(i).Text = StandardRotations.txtVpto(i).Text
    Next
    If Not IsActivated Then
        Me.Refresh
        chkRequest.Value = 1
        IsActivated = True
    End If
End Sub

Private Sub Form_Load()
    Dim NewTop As Long
    Dim NewLeft As Long
    NewTop = Screen.ActiveForm.Top + (Screen.ActiveForm.Height - Me.Height) / 2
    Me.Top = NewTop
    NewLeft = Screen.ActiveForm.Left + (Screen.ActiveForm.Width - Me.Width) / 2
    Me.Left = NewLeft
    InitMyForm
End Sub

Private Sub InitMyForm()
    Dim i As Integer
    FileData.ResetContent
    FileData.HeaderString = "ALC,ORG,HOM,DES,STOA,STOD,FLTA,FLTD,TYP,DAYS,O,T,Remark" & Space(100)
    FileData.HeaderLengthString = "30,30,30,30,30,30,30,30,30,30,30,30,30"
    FileData.ColumnWidthString = "3,3,3,3,4,4,4,4,3,7,1,1,40"
    FileData.ColumnAlignmentString = "L,L,L,L,L,L,L,L,L,L,L,L,L"
    FileData.SetTabFontBold True
    FileData.FontName = "Courier New"
    FileData.HeaderFontSize = 17
    FileData.FontSize = 17
    FileData.LineHeight = 17
    FileData.SetMainHeaderValues "1,3,2,2,1,1,2,1", "CAR,ROUTE,TIMES,FLIGHTS,A/C,FREQ,FL,SYSTEM", ""
    FileData.MainHeader = True
    FileData.LifeStyle = True
    'FileData.CursorLifeStyle = True
    FileData.CreateDecorationObject "CellError", "R,T,B,L", "2,2,2,2", Str(vbBlack) & "," & Str(LightGray) & "," & Str(vbBlack) & "," & Str(LightGray)
    FileData.AutoSizeByHeader = True
    FileData.AutoSizeColumns
    FileData.Refresh
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        If StandardRotations.Visible Then StandardRotations.SetFocus
        Me.Hide
        Cancel = True
    End If
End Sub

Private Sub Form_Resize()
    Dim NewWidth As Long
    Dim NewHeight As Long
    NewHeight = Me.ScaleHeight - FileData.Top - StatusBar1.Height - 45
    If NewHeight > 600 Then
        FileData.Height = NewHeight
    End If
    NewWidth = Me.ScaleWidth - (FileData.Left * 2)
    If NewWidth > 600 Then
        txtCurFile.Width = NewWidth
        FileNote.Width = NewWidth
        FileData.Width = NewWidth
        NewWidth = NewWidth - lblRemark.Left + FileData.Left
        If NewWidth > 120 Then lblRemark.Width = NewWidth
    End If
End Sub

Private Sub lblTotalArr_Click()
    JumpToColoredLine FileData, vbYellow, -1
End Sub

Private Sub lblTotalDay_Click()
    JumpToColoredLine FileData, vbWhite, -1
End Sub

Private Sub lblTotalDep_Click()
    JumpToColoredLine FileData, vbGreen, -1
End Sub

Private Sub lblTotalErr_Click()
    JumpToColoredLine FileData, vbRed, -1
End Sub

Private Sub lblTotalOvn_Click()
    JumpToColoredLine FileData, LightGray, -1
End Sub

Private Sub lblTotalRot_Click()
    JumpToColoredLine FileData, vbWhite, LightGray
End Sub

Private Sub lblTotalWarn_Click()
    JumpToColoredLine FileData, LightRed, -1
End Sub

Private Sub txtActUtcDiff_GotFocus()
    'chkUtc.Value = 1
    chkLoc.Value = 1
    SetTextSelected
End Sub
Private Sub txtVpfr_Change(Index As Integer)
    Dim tmpCedaDate As String
    StandardRotations.txtVpfr(Index).Text = txtVpfr(Index).Text
    tmpCedaDate = ""
    tmpCedaDate = tmpCedaDate & Trim(txtVpfr(0))
    tmpCedaDate = tmpCedaDate & Trim(txtVpfr(1))
    tmpCedaDate = tmpCedaDate & Trim(txtVpfr(2))
    txtVpfr(0).Tag = tmpCedaDate
End Sub

Private Sub txtVpfr_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Private Sub txtVpfr_LostFocus(Index As Integer)
    CheckLoadPeriod
End Sub

Private Sub txtVpto_GotFocus(Index As Integer)
    SetTextSelected
End Sub

Private Sub txtVpto_LostFocus(Index As Integer)
    CheckLoadPeriod
End Sub

Private Sub txtVpto_Change(Index As Integer)
    Dim tmpCedaDate As String
    StandardRotations.txtVpto(Index).Text = txtVpto(Index).Text
    tmpCedaDate = ""
    tmpCedaDate = tmpCedaDate & Trim(txtVpto(0))
    tmpCedaDate = tmpCedaDate & Trim(txtVpto(1))
    tmpCedaDate = tmpCedaDate & Trim(txtVpto(2))
    txtVpto(0).Tag = tmpCedaDate
End Sub

Private Sub txtVpfr_KeyPress(Index As Integer, KeyAscii As Integer)
    Select Case KeyAscii
        Case 8
        Case 48 To 57
        Case Else
            'MsgBox KeyAscii
            KeyAscii = 0
    End Select
End Sub

Private Sub txtVpto_KeyPress(Index As Integer, KeyAscii As Integer)
    Select Case KeyAscii
        Case 8
        Case 48 To 57
        Case Else
            'MsgBox KeyAscii
            KeyAscii = 0
    End Select
End Sub

Private Function CheckLoadPeriod() As Boolean
    Dim Result As Boolean
    Dim IsOk As Boolean
    
    Result = True
    IsOk = CheckValidDate(txtVpfr(0).Tag)
    If Not IsOk Then
        txtVpfr(0).BackColor = vbRed
        txtVpfr(0).ForeColor = vbWhite
        txtVpfr(1).BackColor = vbRed
        txtVpfr(1).ForeColor = vbWhite
        txtVpfr(2).BackColor = vbRed
        txtVpfr(2).ForeColor = vbWhite
        Result = False
    Else
        txtVpfr(0).BackColor = vbYellow
        txtVpfr(0).ForeColor = vbBlack
        txtVpfr(1).BackColor = vbYellow
        txtVpfr(1).ForeColor = vbBlack
        txtVpfr(2).BackColor = vbYellow
        txtVpfr(2).ForeColor = vbBlack
    End If
    IsOk = CheckValidDate(txtVpto(0).Tag)
    If Not IsOk Then
        txtVpto(0).BackColor = vbRed
        txtVpto(0).ForeColor = vbWhite
        txtVpto(1).BackColor = vbRed
        txtVpto(1).ForeColor = vbWhite
        txtVpto(2).BackColor = vbRed
        txtVpto(2).ForeColor = vbWhite
        Result = False
    Else
        txtVpto(0).BackColor = vbYellow
        txtVpto(0).ForeColor = vbBlack
        txtVpto(1).BackColor = vbYellow
        txtVpto(1).ForeColor = vbBlack
        txtVpto(2).BackColor = vbYellow
        txtVpto(2).ForeColor = vbBlack
    End If
    CheckLoadPeriod = Result
End Function

Private Sub JumpToColoredLine(CurTab, JmpColor1 As Long, JmpColor2 As Long)
    Dim CurScroll As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim UseLine As Long
    Dim ForeColor As Long
    Dim BackColor As Long
    Dim LineFound As Boolean
    Dim LoopCount As Integer
    CurLine = CurTab.GetCurrentSelected
    If CurLine < 0 Then CurLine = -1
    MaxLine = CurTab.GetLineCount - 1
    LineFound = False
    LoopCount = 0
    Do
        While CurLine <= MaxLine
            CurLine = CurLine + 1
            CurTab.GetLineColor CurLine, ForeColor, BackColor
            If (BackColor = JmpColor1) Or (BackColor = JmpColor2) Then
                UseLine = CurLine
                LineFound = True
                CurLine = MaxLine + 1
            End If
        Wend
        LoopCount = LoopCount + 1
        CurLine = -1
    Loop While (LineFound = False) And (LoopCount < 2)
    If LineFound Then
        If UseLine > 5 Then CurScroll = UseLine - 5 Else CurScroll = 0
        CurTab.OnVScrollTo CurScroll
        CurTab.SetCurrentSelection UseLine
    End If
End Sub


