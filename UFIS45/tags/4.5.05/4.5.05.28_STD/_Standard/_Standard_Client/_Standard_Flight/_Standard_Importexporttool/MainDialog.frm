VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form MainDialog 
   Caption         =   "Flight Data Import Tool"
   ClientHeight    =   7665
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   16065
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   8.25
      Charset         =   161
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "MainDialog.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MouseIcon       =   "MainDialog.frx":030A
   OLEDropMode     =   1  'Manual
   ScaleHeight     =   7665
   ScaleWidth      =   16065
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   315
      Left            =   480
      TabIndex        =   81
      Top             =   1170
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.Timer TcpRcvTimer 
      Enabled         =   0   'False
      Index           =   0
      Interval        =   20
      Left            =   2970
      Top             =   3960
   End
   Begin VB.Timer TcpMsgTimer 
      Enabled         =   0   'False
      Index           =   0
      Interval        =   20
      Left            =   1320
      Top             =   3960
   End
   Begin TABLib.TAB TcpMsgSpooler 
      Height          =   2115
      Index           =   0
      Left            =   120
      TabIndex        =   78
      Top             =   3960
      Visible         =   0   'False
      Width           =   1155
      _Version        =   65536
      _ExtentX        =   2037
      _ExtentY        =   3731
      _StockProps     =   64
   End
   Begin VB.PictureBox SlidePanel 
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   0  'None
      Height          =   2985
      Index           =   0
      Left            =   6840
      ScaleHeight     =   2985
      ScaleWidth      =   3090
      TabIndex        =   71
      Top             =   3120
      Visible         =   0   'False
      Width           =   3090
      Begin VB.PictureBox ToolPanel 
         Height          =   2115
         Index           =   0
         Left            =   60
         ScaleHeight     =   2055
         ScaleWidth      =   2400
         TabIndex        =   72
         Top             =   60
         Width           =   2460
         Begin VB.TextBox Text1 
            Height          =   315
            Left            =   150
            TabIndex        =   82
            Text            =   "Text1"
            Top             =   1800
            Visible         =   0   'False
            Width           =   1035
         End
         Begin VB.CheckBox chkWebUrl 
            Caption         =   "Close"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   4
            Left            =   1200
            Style           =   1  'Graphical
            TabIndex        =   77
            TabStop         =   0   'False
            Top             =   0
            Width           =   1200
         End
         Begin VB.CheckBox chkWebUrl 
            Caption         =   "Ready"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   3
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   76
            TabStop         =   0   'False
            Top             =   0
            Width           =   1200
         End
         Begin VB.CheckBox chkWebUrl 
            Caption         =   "ACDM Import Manager"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   75
            TabStop         =   0   'False
            Top             =   900
            Width           =   2400
         End
         Begin VB.CheckBox chkWebUrl 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   1
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   74
            TabStop         =   0   'False
            Top             =   1200
            Width           =   2400
         End
         Begin VB.CheckBox chkWebUrl 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   2
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   73
            TabStop         =   0   'False
            Top             =   1500
            Width           =   2400
         End
         Begin VB.Label WebComLabel 
            Alignment       =   2  'Center
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   525
            Index           =   0
            Left            =   30
            TabIndex        =   80
            Top             =   360
            Width           =   2355
         End
      End
      Begin VB.Timer SlideTimer 
         Enabled         =   0   'False
         Index           =   0
         Interval        =   20
         Left            =   450
         Top             =   2520
      End
   End
   Begin VB.PictureBox ButtonPanel 
      Height          =   345
      Index           =   2
      Left            =   30
      ScaleHeight     =   285
      ScaleWidth      =   15195
      TabIndex        =   51
      TabStop         =   0   'False
      Top             =   780
      Width           =   15255
      Begin VB.CheckBox chkLineWork 
         Caption         =   "Batch"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   6
         Left            =   12180
         Style           =   1  'Graphical
         TabIndex        =   64
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkLineWork 
         Caption         =   "Delete"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   11310
         Style           =   1  'Graphical
         TabIndex        =   63
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkLineWork 
         Caption         =   "Update"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   10440
         Style           =   1  'Graphical
         TabIndex        =   62
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkLineWork 
         Caption         =   "Insert"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   9570
         Style           =   1  'Graphical
         TabIndex        =   61
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkLineWork 
         Caption         =   "Join"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   5
         Left            =   8700
         Style           =   1  'Graphical
         TabIndex        =   60
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkLineWork 
         Caption         =   "Split"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   7830
         Style           =   1  'Graphical
         TabIndex        =   59
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkLineWork 
         Caption         =   "Ignore"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   6960
         Style           =   1  'Graphical
         TabIndex        =   58
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkAll 
         Caption         =   "All"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6525
         Style           =   1  'Graphical
         TabIndex        =   57
         ToolTipText     =   "Global update (permanent for this file)"
         Top             =   0
         Width           =   420
      End
      Begin VB.CheckBox chkReplace 
         Caption         =   "Upd"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6090
         Style           =   1  'Graphical
         TabIndex        =   56
         ToolTipText     =   "Single update "
         Top             =   0
         Width           =   450
      End
      Begin VB.TextBox txtEdit 
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         Height          =   285
         Left            =   4350
         Locked          =   -1  'True
         TabIndex        =   55
         Top             =   0
         Width           =   1725
      End
      Begin VB.CheckBox chkEdit 
         Caption         =   "Edit"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3900
         Style           =   1  'Graphical
         TabIndex        =   54
         Top             =   0
         Width           =   435
      End
      Begin VB.CheckBox chkInline 
         Caption         =   "Cell"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3480
         Style           =   1  'Graphical
         TabIndex        =   53
         Top             =   0
         Width           =   435
      End
      Begin VB.Frame Frame1 
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   0
         TabIndex        =   52
         Top             =   0
         Width           =   2625
         Begin VB.Label ActSeason 
            Alignment       =   2  'Center
            BackColor       =   &H00E0E0E0&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   2
            Left            =   1740
            TabIndex        =   69
            Top             =   0
            Width           =   855
         End
         Begin VB.Label ActSeason 
            Alignment       =   2  'Center
            BackColor       =   &H00E0E0E0&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   870
            MouseIcon       =   "MainDialog.frx":045C
            MousePointer    =   99  'Custom
            TabIndex        =   68
            Top             =   0
            Width           =   855
         End
         Begin VB.Label ActSeason 
            Alignment       =   2  'Center
            BackColor       =   &H00E0E0E0&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   0
            TabIndex        =   67
            Top             =   0
            Width           =   855
         End
      End
      Begin VB.Label ActSeason 
         Alignment       =   2  'Center
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   2610
         TabIndex        =   70
         Top             =   0
         Width           =   855
      End
      Begin VB.Label WarnCnt 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   13920
         TabIndex        =   66
         ToolTipText     =   "Warning Counter"
         Top             =   0
         Width           =   855
      End
      Begin VB.Label ErrCnt 
         Alignment       =   2  'Center
         BackColor       =   &H00FFC0FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   13050
         TabIndex        =   65
         ToolTipText     =   "Error Counter"
         Top             =   0
         Width           =   855
      End
   End
   Begin VB.PictureBox ButtonPanel 
      BorderStyle     =   0  'None
      Height          =   345
      Index           =   1
      Left            =   30
      ScaleHeight     =   345
      ScaleWidth      =   15255
      TabIndex        =   29
      TabStop         =   0   'False
      Top             =   390
      Width           =   15255
      Begin VB.CheckBox chkAppl 
         Caption         =   "File"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   10
         Left            =   11970
         Style           =   1  'Graphical
         TabIndex        =   83
         Tag             =   "READER"
         ToolTipText     =   "Open/Close File Reader"
         Top             =   0
         Visible         =   0   'False
         Width           =   405
      End
      Begin VB.CheckBox chkAppl 
         Caption         =   "Exit"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   6
         Left            =   14220
         Style           =   1  'Graphical
         TabIndex        =   50
         Tag             =   "EXIT"
         Top             =   0
         Width           =   555
      End
      Begin VB.CheckBox chkAppl 
         Caption         =   "Help"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   9
         Left            =   13710
         Style           =   1  'Graphical
         TabIndex        =   49
         Tag             =   "HELP"
         Top             =   0
         Width           =   495
      End
      Begin VB.CheckBox chkAppl 
         Caption         =   "About"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   5
         Left            =   13050
         Style           =   1  'Graphical
         TabIndex        =   48
         Tag             =   "ABOUT"
         Top             =   0
         Width           =   645
      End
      Begin VB.CheckBox chkAppl 
         Caption         =   "File"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   11
         Left            =   11970
         Style           =   1  'Graphical
         TabIndex        =   47
         Tag             =   "READER"
         ToolTipText     =   "Open/Close File Reader"
         Top             =   0
         Width           =   405
      End
      Begin VB.CheckBox chkAppl 
         Caption         =   "Setup"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   11310
         Style           =   1  'Graphical
         TabIndex        =   46
         Tag             =   "SETUP"
         Top             =   0
         Width           =   645
      End
      Begin VB.CheckBox chkAppl 
         Caption         =   "iMgr"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   7
         Left            =   12390
         Style           =   1  'Graphical
         TabIndex        =   45
         Tag             =   "ACDM"
         Top             =   0
         Width           =   645
      End
      Begin VB.CheckBox chkSpare 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   10440
         Style           =   1  'Graphical
         TabIndex        =   44
         ToolTipText     =   "A Spare Part Only (Don't touch)"
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkSpare 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   9570
         Style           =   1  'Graphical
         TabIndex        =   43
         ToolTipText     =   "A Spare Part Only (Don't touch)"
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkAppl 
         Caption         =   "Save As"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   8700
         Style           =   1  'Graphical
         TabIndex        =   42
         Tag             =   "SAVEAS"
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkAppl 
         Caption         =   "Refresh"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   7830
         Style           =   1  'Graphical
         TabIndex        =   41
         Tag             =   "REFRESH"
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkExtract 
         Caption         =   "Extract"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6960
         Style           =   1  'Graphical
         TabIndex        =   40
         ToolTipText     =   "Opens the 'import workbench' for data filtering"
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkAppl 
         Caption         =   "Doubles"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   6090
         Style           =   1  'Graphical
         TabIndex        =   39
         Tag             =   "CHECKDUP"
         ToolTipText     =   "Pre-check double flights"
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkIdentify 
         Caption         =   "Identify"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5220
         Style           =   1  'Graphical
         TabIndex        =   38
         ToolTipText     =   "Determine single arrivals, departures or complete rotations"
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkCheck 
         Caption         =   "Check"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4350
         Style           =   1  'Graphical
         TabIndex        =   37
         ToolTipText     =   "Full check and basic data validation"
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkDispo 
         Caption         =   "Dispo"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3480
         Style           =   1  'Graphical
         TabIndex        =   36
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkDontClear 
         Caption         =   ">"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3270
         Style           =   1  'Graphical
         TabIndex        =   35
         ToolTipText     =   "Bypass the Clear Function"
         Top             =   0
         Width           =   195
      End
      Begin VB.CheckBox chkClear 
         Caption         =   "Clear"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2610
         Style           =   1  'Graphical
         TabIndex        =   34
         ToolTipText     =   "Clear out all flights of the past"
         Top             =   0
         Width           =   645
      End
      Begin VB.CheckBox chkShrink 
         Caption         =   "Shrink"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1740
         Style           =   1  'Graphical
         TabIndex        =   33
         ToolTipText     =   "Determine overnight rotations"
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkSort 
         Caption         =   "Sort"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1740
         Style           =   1  'Graphical
         TabIndex        =   32
         ToolTipText     =   "Single update "
         Top             =   0
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CheckBox chkRestore 
         Caption         =   "Restore"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   870
         Style           =   1  'Graphical
         TabIndex        =   31
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkBackup 
         Caption         =   "Backup"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   30
         Top             =   0
         Width           =   855
      End
   End
   Begin VB.PictureBox ButtonPanel 
      BorderStyle     =   0  'None
      Height          =   345
      Index           =   0
      Left            =   30
      ScaleHeight     =   345
      ScaleWidth      =   15255
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   0
      Width           =   15255
      Begin VB.HScrollBar ImpScroll 
         Height          =   285
         Left            =   5220
         TabIndex        =   28
         TabStop         =   0   'False
         Top             =   0
         Width           =   855
      End
      Begin VB.CheckBox chkImpTyp 
         Caption         =   "Import"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   4350
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   0
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CheckBox chkTimes 
         Caption         =   "LT"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   3960
         Style           =   1  'Graphical
         TabIndex        =   26
         ToolTipText     =   "Show Local Times"
         Top             =   0
         Width           =   375
      End
      Begin VB.CheckBox chkTimes 
         Caption         =   "UTC"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   3480
         Style           =   1  'Graphical
         TabIndex        =   25
         ToolTipText     =   "Show UTC Times"
         Top             =   0
         Width           =   465
      End
      Begin VB.CheckBox chkGrid 
         Caption         =   "S"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   3120
         Style           =   1  'Graphical
         TabIndex        =   24
         ToolTipText     =   "Switch to Second Data Grid View"
         Top             =   0
         Width           =   345
      End
      Begin VB.CheckBox chkGrid 
         Caption         =   "B"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   2760
         Style           =   1  'Graphical
         TabIndex        =   23
         ToolTipText     =   "Show both data grids"
         Top             =   0
         Width           =   345
      End
      Begin VB.CheckBox chkGrid 
         Caption         =   "M"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   2400
         Style           =   1  'Graphical
         TabIndex        =   22
         ToolTipText     =   "Switch to Main Data Grid View"
         Top             =   0
         Width           =   345
      End
      Begin VB.CheckBox chkRequest 
         Caption         =   "Select"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1740
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   0
         Width           =   645
      End
      Begin VB.CheckBox chkExpand 
         Caption         =   "Open"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1740
         Style           =   1  'Graphical
         TabIndex        =   20
         ToolTipText     =   "Load file in pre-interpretation mode"
         Top             =   0
         Width           =   645
      End
      Begin VB.CheckBox chkRotation 
         Caption         =   "Rotation Planner"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   0
         Width           =   1725
      End
   End
   Begin VB.Timer Timer2 
      Enabled         =   0   'False
      Interval        =   50
      Left            =   10530
      Top             =   2220
   End
   Begin TABLib.TAB HelperTab 
      Height          =   2475
      Index           =   0
      Left            =   11640
      TabIndex        =   16
      Top             =   1560
      Visible         =   0   'False
      Width           =   1485
      _Version        =   65536
      _ExtentX        =   2619
      _ExtentY        =   4366
      _StockProps     =   64
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   10
      Left            =   10530
      Top             =   1770
   End
   Begin VB.CheckBox chkAppl 
      Caption         =   "?"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   8
      Left            =   7950
      Style           =   1  'Graphical
      TabIndex        =   13
      Tag             =   "ASK"
      ToolTipText     =   "Asks for Drop confirmation"
      Top             =   2670
      Visible         =   0   'False
      Width           =   225
   End
   Begin VB.Frame CedaBusyFlag 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1275
      Left            =   8940
      TabIndex        =   8
      Top             =   1440
      Width           =   1215
      Begin VB.TextBox CedaStatus 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   1
         Left            =   30
         Locked          =   -1  'True
         TabIndex        =   12
         Text            =   "LOG ON"
         Top             =   300
         Visible         =   0   'False
         Width           =   1125
      End
      Begin VB.TextBox CedaStatus 
         Alignment       =   2  'Center
         BackColor       =   &H00004000&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   0
         Left            =   30
         Locked          =   -1  'True
         TabIndex        =   11
         Text            =   "LINE BUSY"
         Top             =   0
         Visible         =   0   'False
         Width           =   1125
      End
      Begin VB.TextBox CedaStatus 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   2
         Left            =   30
         Locked          =   -1  'True
         TabIndex        =   10
         Text            =   "LOG OFF"
         Top             =   600
         Visible         =   0   'False
         Width           =   1125
      End
      Begin VB.TextBox CedaStatus 
         Alignment       =   2  'Center
         BackColor       =   &H00C00000&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   3
         Left            =   30
         Locked          =   -1  'True
         TabIndex        =   9
         Text            =   "OFFLINE"
         ToolTipText     =   "Server:"
         Top             =   900
         Visible         =   0   'False
         Width           =   1125
      End
      Begin VB.Line Line10 
         BorderColor     =   &H00FFFFFF&
         X1              =   1140
         X2              =   1140
         Y1              =   0
         Y2              =   1590
      End
      Begin VB.Line Line9 
         BorderColor     =   &H00FFFFFF&
         X1              =   45
         X2              =   1155
         Y1              =   255
         Y2              =   255
      End
      Begin VB.Line Line8 
         BorderColor     =   &H00FFFFFF&
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   1590
      End
      Begin VB.Line Line7 
         BorderColor     =   &H00808080&
         X1              =   45
         X2              =   45
         Y1              =   0
         Y2              =   1590
      End
      Begin VB.Line Line6 
         BorderColor     =   &H00808080&
         X1              =   60
         X2              =   1155
         Y1              =   0
         Y2              =   0
      End
   End
   Begin VB.CheckBox chkDetails 
      Caption         =   "Daily"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   7770
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   1560
      Visible         =   0   'False
      Width           =   855
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   300
      Left            =   0
      TabIndex        =   4
      Top             =   7365
      Width           =   16065
      _ExtentX        =   28337
      _ExtentY        =   529
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   7
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1323
            MinWidth        =   1323
            Object.ToolTipText     =   "Total Lines"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   1323
            MinWidth        =   1323
            Object.ToolTipText     =   "Valid Lines (Filtered)"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1323
            MinWidth        =   1323
            Object.ToolTipText     =   "Total Flights"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1323
            MinWidth        =   1323
            Object.ToolTipText     =   "Arrivals"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1323
            MinWidth        =   1323
            Object.ToolTipText     =   "Departures"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1323
            MinWidth        =   1323
            Object.ToolTipText     =   "Rotations"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   19720
            MinWidth        =   8819
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CheckBox chkAppl 
      Caption         =   "Drag"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   7320
      Style           =   1  'Graphical
      TabIndex        =   6
      Tag             =   "DRAG"
      ToolTipText     =   "Enables manual Drag&Drop"
      Top             =   2670
      Visible         =   0   'False
      Width           =   645
   End
   Begin VB.CheckBox chkAction 
      Caption         =   "Preview"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   7770
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   1860
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CheckBox chkImport 
      Caption         =   "Import"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   7770
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   2160
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CheckBox chkSplit 
      Caption         =   "Split"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6810
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   2160
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CheckBox chkFilter 
      Caption         =   "Filter"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6810
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   1860
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CheckBox chkLoad 
      Caption         =   "Load"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6810
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   1560
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Frame TabFrame 
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   2115
      Left            =   60
      TabIndex        =   14
      Top             =   1590
      Width           =   6645
      Begin TABLib.TAB FileData 
         Height          =   1815
         Index           =   0
         Left            =   210
         TabIndex        =   15
         Top             =   90
         Width           =   3525
         _Version        =   65536
         _ExtentX        =   6218
         _ExtentY        =   3201
         _StockProps     =   64
         Columns         =   10
         FontName        =   "Courier New"
      End
      Begin TABLib.TAB TAB2 
         Height          =   1815
         Left            =   3930
         TabIndex        =   17
         Top             =   90
         Visible         =   0   'False
         Width           =   2505
         _Version        =   65536
         _ExtentX        =   4419
         _ExtentY        =   3201
         _StockProps     =   64
      End
   End
   Begin MSWinsockLib.Winsock WebSock 
      Index           =   0
      Left            =   1320
      Top             =   4410
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin TABLib.TAB TcpRcvSpooler 
      Height          =   2115
      Index           =   0
      Left            =   1770
      TabIndex        =   79
      Top             =   3960
      Visible         =   0   'False
      Width           =   1155
      _Version        =   65536
      _ExtentX        =   2037
      _ExtentY        =   3731
      _StockProps     =   64
   End
End
Attribute VB_Name = "MainDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim WebToolId As Long
Dim SrvMsgRcv As String
Dim PathToUfisWebBrowser As String
Dim WebToolIsConnected As Boolean
Dim DoNotReSync As Boolean
Dim AcdmManagerIsUsed As Boolean

Dim UseImpType As String
Dim UseImpConf As String
Dim UseImpFile As String
Dim UseOutFile As String

Dim IsResetApplication As Boolean
Dim IsResetMainScreen As Boolean
Dim SectionPushed As Boolean
Dim PushedFilePath As String
Dim AutoPushIndex As Integer
Public PushedImpButton As Integer
Public HardCodeType As Boolean
Public ExtractSystemType As Integer
Public ActColLst As String
Public TotalReset As Boolean
Public ExtFldCnt As Long
Public UseVialFlag As String
Public UsedTimeType As String
'Public MainFieldList As String
'Public MainExpandHeader As String
'Public MainFldWidLst As String
'Public MainHeaderTitle As String
'Public MainHeaderWidth As String
'Public MainHeaderColor As String
Dim UniqueLineKey As String
Dim UniqueKeyFormat As String
Dim UniqueKeyPos As Integer
Dim UniqueKeyLen As Integer

Dim MainDoIt As Boolean
Dim DbfReaderIsChecked As Boolean
Dim TranslateIcao As String
Dim TranslateApcList As String
Dim PreCheckDoubles As String
'Dim UseDefaultSeason As Boolean
'Dim DefaultSeasonRec As String
'Dim SeasonActYear As String
'Dim SeasonNxtYear As String
'Dim SeasonBegin As String
'Dim SeasonEnd As String
'Dim SeasonFirstMonth As Integer
'Dim SeasonLastMonth As Integer
Dim ErrorColor As Long
Dim ShowMaxButtons As Integer
Dim MainPeriodCols As String
Dim MainVpfrCol As Long
Dim MainVptoCol As Long
Dim MainFredCol As Long
Dim MainFrewCol As Long
Dim MainStoaCol As Long
Dim MainStodCol As Long
Dim MainDaofCol As Long
Dim DepFreqCol As Long
Dim DepOvniCol As Long
Dim DepDpfrCol As Long
Dim ImpOvnFlag As Integer
Dim MainChkFred As String
Dim CurHeaderLength As String
Dim MainTitle As String
Dim FormatType As String
Dim ItemSep As String
Dim GridPrepared As Boolean
Dim AllLinCnt As Integer
Dim GrpLinCnt As Integer
Dim DatLinCnt As Integer
Dim ArrFltCnt As Integer
Dim DepFltCnt As Integer
'Dim myLoadPath As String
'Dim myLoadName As String
'Public CurLoadPath As String
'Dim CurLoadName As String
Dim CurFileFilter As String
Dim CurMultiFiles As String
Dim CurFilterIndex As Integer
Dim NormalHeader As String
'Dim CurrentHeader As String
Dim FldPosLst As String
'Dim ExpPosLst As String
'Dim CurFldPosLst As String
'Dim FldLenLst As String
Dim FldFrmLst As String
Dim FldAddLst As String
Dim LineFilterPos As Integer
Dim LineFilterLen As Integer
Dim LineFilterTyp As String
Dim LineFilterText As String
Dim LineWrap As Boolean
Dim IgnoreEmptyLine As Boolean
Dim SetColSelection As Boolean
Dim ColSelectList As String
Dim ChkBasDat As String
Dim AutoCheck As String
Dim LegColNbr As Long
Dim OrgColNbr As Long
Dim DesColNbr As Long
Dim DateFields As String
Dim TimeFields As String
'Dim DateFieldFormat As String
Dim ClearZeros As String
Dim ClearValues As String
Dim ClearByValue As String
Dim TimeChainInfo As String
Dim CheckViaList As String
Dim IgnoreEmpty As String
Dim ShrinkList As String
Dim AftFldLst As String
Dim ExtFldLst As String
Dim OutputType As String
Dim ActionType As String
Dim CheckRotation As String
Dim ExtractDepartures As String
Dim ExtractArrivals As String
Dim ExtractRotationArr As String
Dim ExtractRotationDep As String
Dim ExtractSlotLine As String
Dim AutoReplaceAll As String
Dim DragLine As Long
Dim DropLine As Long
Dim MoveLine As Long
Dim DragForeColor As Long
Dim DragBackcolor As Long
Dim TestMode As Boolean
Dim ActiveTimeFields As String
Dim FlnoField As Integer
Dim WinZipPath As String
Dim WinZipId 'as variant

Private Sub ActSeason_Click(Index As Integer)
    If Index = 0 Then
        If MySetUp.MonitorSetting(3).Value = 1 Then
            SeasonData.Show , Me
        Else
            SeasonData.Show
        End If
    End If
End Sub

Private Sub chkAction_Click()
    If Not RestoreAction Then
        If Not TotalReset Then
            If chkAction.Value = 1 Then
                OaFosPreview
            Else
                chkAction.BackColor = vbButtonFace
                chkImport.Enabled = False
            End If
        End If
    End If
End Sub

Public Sub OaFosPreview()
Dim CurLine As Long
Dim MaxLine As Long
Dim Alc3Row As Long
Dim FltnRow As Long
Dim FlnsRow As Long
Dim DateRow As Long
Dim AdidRow As Long
Dim tmpAlc3 As String
Dim tmpFltn As String
Dim tmpFlns As String
Dim tmpDate As String
Dim tmpAdid As String
Dim AftFkey As String
Dim AftFkeyList As String
Dim tmpSqlKey As String
Dim tmpResult As String
Dim i As Long
Dim llCount As Long
Dim RetCode As Integer
Dim PacketCount As Integer
Dim ChkMainLine As String
Dim ChkExtrLine As String
Dim ExtrMaxLine As Long
Dim ListOfOaFosLines As String
    ListOfOaFosLines = ""
    'hardcode
    Alc3Row = 3
    FltnRow = 4
    FlnsRow = -1
    DateRow = 18
    AdidRow = 1
    ClickButtonChain 9
    Me.MousePointer = 11
    AftFkeyList = ""
    MaxLine = FileData(CurFdIdx).GetLineCount - 1
    ExtrMaxLine = FlightExtract.ExtractData.GetLineCount - 1
    PacketCount = 0
    For CurLine = 0 To MaxLine
        tmpAlc3 = DataSystemAlc3
        tmpFltn = FileData(CurFdIdx).GetColumnValue(CurLine, FltnRow)
        tmpFlns = " "
        tmpAdid = Trim(FileData(CurFdIdx).GetColumnValue(CurLine, AdidRow))
        If tmpAdid = "A" Then
            tmpDate = FileData(CurFdIdx).GetColumnValue(CurLine, 18)
        ElseIf tmpAdid = "D" Then
            tmpDate = FileData(CurFdIdx).GetColumnValue(CurLine, 16)
        ElseIf tmpAdid = "B" Then
            tmpDate = FileData(CurFdIdx).GetColumnValue(CurLine, 16)
        Else
            tmpDate = ""
        End If
        If tmpDate <> "" Then
            ChkMainLine = FileData(CurFdIdx).GetColumnValue(CurLine, 0)
            ChkExtrLine = FlightExtract.ExtractData.GetLinesByColumnValue(0, ChkMainLine, 0)
            'MsgBox ChkMainLine & " / " & ChkExtrLine
            If Val(ChkExtrLine) > ExtrMaxLine Then ChkExtrLine = ""
            If ChkExtrLine = "DONE" Then ChkExtrLine = ""
            If ChkExtrLine = "OK" Then ChkExtrLine = ""
            If ChkExtrLine <> "" Then
                AftFkey = BuildAftFkey(tmpAlc3, tmpFltn, tmpFlns, tmpDate, tmpAdid)
                FileData(CurFdIdx).SetColumnValue CurLine, ExtFldCnt, AftFkey
                ListOfOaFosLines = ListOfOaFosLines & Str(CurLine) & ","
                AftFkey = "'" & AftFkey & "',"
                AftFkeyList = AftFkeyList & AftFkey
                PacketCount = PacketCount + 1
                If PacketCount >= 30 Then
                    AftFkeyList = Left(AftFkeyList, Len(AftFkeyList) - 1)
                    CheckExistingFlights AftFkeyList
                    AftFkeyList = ""
                    PacketCount = 0
                End If
            End If
        End If
    Next
    If AftFkeyList <> "" Then
        AftFkeyList = Left(AftFkeyList, Len(AftFkeyList) - 1)
        CheckExistingFlights AftFkeyList
    End If
    CheckInsertFlights
    chkImport.Enabled = True
    PrecheckCedaAction 1, ErrorColor
    PrecheckCedaAction 2, ErrorColor
    Me.MousePointer = 0
    'MsgBox ListOfOaFosLines
End Sub
Private Sub CheckExistingFlights(AftFkeyList As String)
Dim tmpSqlKey As String
Dim tmpResult As String
Dim i As Long
Dim llCount As Long
Dim RetCode As Integer
Dim tmpUrno As String
Dim tmpFkey As String
Dim tmpColLst As String
Dim tmpLine As Long
Dim tmpData As String
    If AftFkeyList <> "" Then
        tmpSqlKey = "WHERE FKEY IN (" & AftFkeyList & ")"
        RetCode = UfisServer.CallCeda(tmpResult, "RTA", "AFTTAB", "URNO,FKEY", " ", tmpSqlKey, "", 1, False, False)
        llCount = UfisServer.DataBuffer(1).GetLineCount - 1
        For i = 0 To llCount
            tmpUrno = UfisServer.DataBuffer(1).GetColumnValue(i, 0)
            tmpFkey = UfisServer.DataBuffer(1).GetColumnValue(i, 1)
            tmpColLst = FileData(CurFdIdx).GetLinesByColumnValue(ExtFldCnt, tmpFkey, 1)
            If tmpColLst <> "" Then
                tmpLine = Val(GetItem(tmpColLst, 1, ","))
                If tmpLine >= 0 Then
                    FileData(CurFdIdx).SetColumnValue tmpLine, ExtFldCnt, tmpUrno
                    FileData(CurFdIdx).SetColumnValue tmpLine, 2, "U"
                End If
            End If
        Next
        FileData(CurFdIdx).RedrawTab
    End If
End Sub
Private Sub CheckInsertFlights()
Dim MaxLine As Long
Dim CurLine As Long
Dim tmpAdid As String
Dim tmpAction As String
Dim ChkMainLine As String
Dim ChkExtrLine As String
Dim ExtrMaxLine As Long

    MaxLine = FileData(CurFdIdx).GetLineCount - 1
    ExtrMaxLine = FlightExtract.ExtractData.GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpAdid = FileData(CurFdIdx).GetColumnValue(CurLine, 1)
        tmpAction = Trim(FileData(CurFdIdx).GetColumnValue(CurLine, 2))
        If tmpAction = "" Then
            Select Case tmpAdid
                Case "A", "D", "B"
                    ChkMainLine = FileData(CurFdIdx).GetColumnValue(CurLine, 0)
                    ChkExtrLine = FlightExtract.ExtractData.GetLinesByColumnValue(0, ChkMainLine, 0)
                    'MsgBox ChkMainLine & " / " & ChkExtrLine
                    If Val(ChkExtrLine) > ExtrMaxLine Then ChkExtrLine = ""
                    If ChkExtrLine <> "" Then
                        FileData(CurFdIdx).SetColumnValue CurLine, 2, "I"
                    End If
                Case Else
            End Select
        End If
    Next
End Sub

Private Sub chkAll_Click()
    If chkAll.Value = 1 Then
        chkAll.BackColor = LightestGreen
        chkReplace.Value = 1
    Else
        chkAll.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkAppl_Click(Index As Integer)
    On Error Resume Next
    If Index <> 4 Then
        If chkAppl(Index).Value = 1 Then chkAppl(Index).BackColor = LightestGreen Else chkAppl(Index).BackColor = vbButtonFace
    End If
    Select Case chkAppl(Index).Tag
        Case "EXIT"
            If chkAppl(Index).Value = 1 Then
                ShutDownApplication
                chkAppl(Index).Value = 0
                If TabFrame.Enabled = True Then FileData(CurFdIdx).SetFocus
            End If
        Case "RESET"
            If chkAppl(Index).Value = 1 Then
                TotalReset = True
                ResetMain 0, False
                TotalReset = False
                MySetUp.InitForm True
                InitApplication
                chkAppl(Index).Value = 0
            End If
        Case "SETUP"
            If chkAppl(Index).Value = 1 Then
                If MySetUp.MonitorSetting(3).Value = 1 Then
                    MySetUp.Show , Me
                Else
                    MySetUp.Show
                End If
            Else
                MySetUp.Hide
                If TabFrame.Enabled = True Then FileData(CurFdIdx).SetFocus
            End If
        Case "ABOUT"
            If chkAppl(Index).Value = 1 Then
                ShowAbout
                chkAppl(Index).Value = 0
            Else
                'Unload MyAboutBox
                'If TabFrame.Enabled = True Then FileData(CurFdIdx).SetFocus
            End If
        Case "SAVEAS"
            If chkAppl(Index).Value = 1 Then
                chkAppl(Index).BackColor = LightestGreen
                SaveLoadedFile Index, MainDialog.FileData
                chkAppl(Index).Value = 0
            Else
                chkAppl(Index).BackColor = vbButtonFace
            End If
            If TabFrame.Enabled = True Then FileData(CurFdIdx).SetFocus
        'Case "SPARE"
        '    DontPlay
        '    If TabFrame.Enabled = True Then FileData(CurFdIdx).SetFocus
        Case "DRAG"
            If chkAppl(Index).Value = 0 Then chkAppl(8).Value = 0
        Case "ASK"
            If chkAppl(Index).Value = 1 Then chkAppl(3).Value = 1
        Case "HELP"
            'If chkAppl(Index).Value = 1 Then DisplayHtmHelpFile "", Me.Name, Me.ActiveControl.Name
            If chkAppl(Index).Value = 1 Then ShowHtmlHelp Me, "", ""
            chkAppl(Index).Value = 0
        Case "CONFIG"
            If chkAppl(Index).Value = 1 Then
                CfgTool.Show , Me
                chkAppl(Index).Value = 0
            End If
        Case "ACDM"
            If chkAppl(Index).Value = 1 Then
                'CfgTool.Show , Me
                If WebToolId > 0 Then
                    OpenWebTool 0, "SHOW", ""
                End If
                chkAppl(Index).Value = 0
            End If
        Case "CHECKDUP"
            If chkAppl(Index).Enabled Then
                If chkAppl(Index).Value = 1 Then
                    'If IsDoubleGridView Then chkGrid(0).Value = 1
                    NoopFullMode = True
                    NoopCheckDoubles = True
                    NoopFlights.Show , Me
                    NoopFlights.chkWork(5).Value = 1
                    If NoopFlights.NoopList(0).GetLineCount < 1 Then
                        Unload NoopFlights
                        If PreCheckDoubles = "YES" Then
                            Me.SetFocus
                            If MyMsgBox.CallAskUser(0, 0, 0, "Pre-Check Double Flights", "No double flights detected.", "infomsg", "", UserAnswer) = 2 Then DoNothing
                        End If
                    Else
                        If PreCheckDoubles = "AUTO" Then
                            Me.SetFocus
                            If MyMsgBox.CallAskUser(0, 0, 0, "Pre-Check Double Flights", "Double flights detected !", "stop", "", UserAnswer) = 2 Then DoNothing
                        End If
                    End If
                Else
                    Unload NoopFlights
                End If
            End If
        Case "REFRESH"
            If chkAppl(Index).Value = 1 Then
                FileData(0).Sort "0", True, True
                FileData(0).AutoSizeByHeader = True
                FileData(0).AutoSizeColumns
                FileData(0).Refresh
                If IsDoubleGridView Then
                    FileData(1).Sort "0", True, True
                    FileData(1).AutoSizeByHeader = True
                    FileData(1).AutoSizeColumns
                    FileData(1).Refresh
                End If
                chkAppl(Index).Value = 0
            End If
        Case "READER"
            If chkAppl(Index).Value = 1 Then
                DbfReader.Show
            Else
                DbfReader.Hide
            End If
            DoEvents
        Case "ACDM"
            If WebToolId > 0 Then
                OpenWebTool 0, "SHOW", ""
            End If
            chkAppl(Index).Value = 0
        Case Else
            If TabFrame.Enabled = True Then FileData(CurFdIdx).SetFocus
    End Select
End Sub

Private Sub ShowAbout()
    Dim myControls() As Control
    Dim CompLine As String
    Dim CompList As String
    CompLine = "BcProxy.exe,"
    CompLine = CompLine & UfisServer.GetComponentVersion("BcProxy")
    CompList = CompList & CompLine & vbLf
    CompLine = "UfisAppMng.exe,"
    CompLine = CompLine & UfisServer.GetComponentVersion("ufisappmng")
    CompList = CompList & CompLine & vbLf
    MyAboutBox.SetComponents myControls(), CompList
    MyAboutBox.SetLifeStyle = True
    If MySetUp.MonitorSetting(3).Value = 1 Then
        MyAboutBox.Show , Me
    Else
        MyAboutBox.Show
    End If
End Sub

Private Sub DontPlay(Index As Integer)
    If chkSpare(Index).Value = 1 Then
        If MyMsgBox.CallAskUser(0, 0, 0, "Function Button Control", "Don't play with" & vbNewLine & "my spare parts!", "grobi", "", UserAnswer) = 2 Then
            '
        End If
        chkSpare(Index).Value = 0
    End If
End Sub

Public Sub SaveLoadedFile(Index As Integer, myObject As Object)
    Dim FileCount As Integer
    Dim ActFile As String
    Dim ActPath As String
    Dim tmpAnsw As Integer
    Dim FileNo As Integer
    Dim tmpRec As String
    Dim CurLine As Long
    Dim MaxLine As Long
    If (chkAppl(0).Value = 1) Or (Index >= 0) Then
        If myObject.GetLineCount > 0 Then
            FileCount = GetFileAndPath(chkRequest.Tag, myLoadName, myLoadPath)
            ActFile = GetItem(myLoadName, 1, ".") & "_sve.txt"
            UfisTools.CommonDialog.DialogTitle = "Save data as: " & DataDisplayType
            UfisTools.CommonDialog.InitDir = myLoadPath
            UfisTools.CommonDialog.FileName = ActFile
            UfisTools.CommonDialog.Filter = "Saved Files|*_sve.txt|Excel Export|*.csv|ASCII Files|*.txt|All|*.*"
            UfisTools.CommonDialog.CancelError = True
            On Error GoTo ErrorHandle
            UfisTools.CommonDialog.ShowSave
            If UfisTools.CommonDialog.FileName <> "" Then
                tmpAnsw = 1
                FileNo = FreeFile
                Open UfisTools.CommonDialog.FileName For Output As #FileNo
                MaxLine = myObject.GetLineCount - 1
                For CurLine = 0 To MaxLine
                    tmpRec = myObject.GetLineValues(CurLine)
                    Print #FileNo, tmpRec
                Next
                Close #FileNo
            End If
        Else
            If MyMsgBox.CallAskUser(0, 0, 0, "Save Data Control", "There's nothing to save!", "export", "", UserAnswer) >= 0 Then DoNothing
        End If
    End If
    Exit Sub
ErrorHandle:
    myLoadName = ""
    myLoadPath = ""
    chkAppl(0).Value = 0
    Exit Sub
End Sub

Private Sub chkBackup_Click()
    Dim UsePath As String
    Dim UseName As String
    Dim PathAndFile As String
    If Not RestoreAction Then
        If chkBackup.Value = 1 Then
            UsePath = App.Path
            UseName = Replace(chkImpTyp(PushedImpButton).Caption, " ", "_", 1, -1, vbBinaryCompare) & "_"
            If FullMode Then
                UseName = UseName & "FULL_" & Format(Now, "yyyymmdd") & ".ite"
            Else
                UseName = UseName & "UPDT_" & Format(Now, "yyyymmdd") & ".ite"
            End If
            If UsePath <> "" Then UsePath = UsePath & "\"
            PathAndFile = UsePath & UseName
            Screen.MousePointer = 11
            MySetUp.Show
            MySetUp.Refresh
            SaveForm PathAndFile, MySetUp, True
            MySetUp.Hide
            Me.Refresh
            SetIniFileEntry Me.Name, Me.Name & "_IMP_BUTTON", Str(PushedImpButton), PathAndFile
            SetIniFileEntry Me.Name, Me.Name & "_COL_COUNT", Str(ExtFldCnt), PathAndFile
            SetIniFileEntry Me.Name, Me.Name & "_UPDT_MODE", Str(CInt(UpdateMode)), PathAndFile
            SetIniFileEntry Me.Name, Me.Name & "_FULL_MODE", Str(CInt(FullMode)), PathAndFile
            StoreLineStatus
            SaveForm PathAndFile, Me, True
            chkBackup.Value = 0
            Screen.MousePointer = 0
        End If
    Else
        chkBackup.Value = 0
    End If
End Sub
Private Sub WriteObjProps(curCtrl)
    Dim ObjType As String
    Print #1, curCtrl.Name
    Print #1, curCtrl.Visible
    Print #1, curCtrl.Tag
    ObjType = TypeName(curCtrl)
    Select Case ObjType
        Case "CheckBox"
            Print #1, curCtrl.Value
        Case "TextBox"
            Print #1, curCtrl.Value
        Case Else
            MsgBox "Undefined Type: " & ObjType
    End Select
End Sub

Private Sub chkDispo_Click()
    If chkDispo.Value = 1 Then
        chkDispo.BackColor = LightestGreen
        'If IsDoubleGridView Then chkGrid(0).Value = 1
        chkClear.Value = 1
        If chkDispo.Tag <> "" Then
            FilterCalledAsDispo = True
            RecFilter.Show , Me
            RecFilter.ArrangeFormLayout
        End If
    Else
        chkDispo.BackColor = vbButtonFace
        If chkDispo.Tag <> "" Then
            RecFilter.Hide
        End If
    End If
End Sub

Private Sub chkGrid_Click(Index As Integer)
    Dim iCnt As Integer
    If chkGrid(Index).Value = 1 Then
        'If chkGrid(Index).Enabled Then
            If FileData.Count > 1 Then iCnt = 1 Else iCnt = 0
            Select Case Index
                Case 0 'Main Grid
                    If Not IsDoubleGridView Then CurFdIdx = 0
                    chkGrid(Index).BackColor = vbYellow
                    chkGrid(2).Value = 0
                    chkGrid(1).Value = 0
                Case 1 'Second Grid
                    If Not IsDoubleGridView Then CurFdIdx = iCnt
                    chkGrid(Index).BackColor = LightGreen
                    chkGrid(2).Value = 0
                    chkGrid(0).Value = 0
                Case 2 'Both Grids
                    chkGrid(Index).BackColor = vbCyan
                    chkGrid(0).Value = 0
                    chkGrid(1).Value = 0
            End Select
            ArrangeMainScreen "INIT"
        'End If
    Else
        chkGrid(Index).BackColor = vbButtonFace
        If chkGrid(2).Enabled Then
            Select Case Index
                Case 0 'Main Grid
                    If chkGrid(2).Value = 0 Then chkGrid(1).Value = 1
                Case 1 'Second Grid
                    If chkGrid(2).Value = 0 Then chkGrid(0).Value = 1
                Case 2 'Both Grids
            End Select
        End If
    End If
End Sub
Private Sub ArrangeMainScreen(ForWhat As String)
    Dim FullHeight As Long
    Dim NewTopHeight As Long
    Dim NewBotHeight As Long
    Dim NewTopTop As Long
    Dim NewBotTop As Long
    Dim TopIdx As Integer
    Dim BotIdx As Integer
    On Error Resume Next
    FullHeight = TabFrame.Height
    If Tab2IsUsed Then
        If (chkGrid(2).Value = 1) Or (ForWhat = "BOTH") Then
            BotIdx = 0
            Select Case ForWhat
                Case "INIT", "BOTH"
                    NewTopHeight = FullHeight / 2
                    NewBotHeight = FullHeight - NewTopHeight
                    NewTopTop = 0
                    NewBotTop = NewTopTop + NewTopHeight
                    TAB2.Top = NewTopTop
                    TAB2.Height = NewTopHeight
                    TAB2.Visible = True
                    FileData(BotIdx).Top = NewBotTop
                    FileData(BotIdx).Height = NewBotHeight
                    FileData(BotIdx).Visible = True
                Case "SIZE"
                Case Else
            End Select
        ElseIf chkGrid(0).Value = 1 Then
            BotIdx = 0
            Select Case ForWhat
                Case "INIT"
                    NewTopHeight = FullHeight
                    NewBotHeight = FullHeight
                    NewTopTop = 0
                    NewBotTop = 0
                    TAB2.Top = NewTopTop
                    TAB2.Height = NewTopHeight
                    TAB2.Visible = False
                    FileData(BotIdx).Top = NewBotTop
                    FileData(BotIdx).Height = NewBotHeight
                    FileData(BotIdx).Visible = True
                Case "SIZE"
                Case Else
            End Select
        ElseIf chkGrid(1).Value = 1 Then
            BotIdx = 0
            Select Case ForWhat
                Case "INIT"
                    NewTopHeight = FullHeight
                    NewBotHeight = FullHeight
                    NewTopTop = 0
                    NewBotTop = 0
                    TAB2.Top = NewTopTop
                    TAB2.Height = NewTopHeight
                    TAB2.Visible = True
                    FileData(BotIdx).Top = NewBotTop
                    FileData(BotIdx).Height = NewBotHeight
                    FileData(BotIdx).Visible = False
                Case "SIZE"
                Case Else
            End Select
        ElseIf chkGrid(2).Value = 0 Then
            BotIdx = 0
            Select Case ForWhat
                Case "INIT"
                    NewTopHeight = FullHeight
                    NewBotHeight = FullHeight
                    NewTopTop = 0
                    NewBotTop = 0
                    TAB2.Top = NewTopTop
                    TAB2.Height = NewTopHeight
                    TAB2.Visible = False
                    FileData(BotIdx).Top = NewBotTop
                    FileData(BotIdx).Height = NewBotHeight
                    FileData(BotIdx).Visible = True
                Case "SIZE"
                Case Else
            End Select
        End If
    Else
        If (chkGrid(2).Value = 1) Or (ForWhat = "BOTH") Then
            TopIdx = 1
            BotIdx = 0
            If FileData.Count = 1 Then TopIdx = -1
            Select Case ForWhat
                Case "INIT", "BOTH"
                    NewTopHeight = FullHeight / 2
                    NewBotHeight = FullHeight - NewTopHeight
                    NewTopTop = 0
                    NewBotTop = NewTopTop + NewTopHeight
                    If TopIdx >= 0 Then
                        FileData(TopIdx).Top = NewTopTop
                        FileData(TopIdx).Height = NewTopHeight
                        FileData(TopIdx).Visible = True
                    End If
                    FileData(BotIdx).Top = NewBotTop
                    FileData(BotIdx).Height = NewBotHeight
                    FileData(BotIdx).Visible = True
                Case "SIZE"
                Case Else
            End Select
        ElseIf chkGrid(0).Value = 1 Then
            TopIdx = 0
            BotIdx = 1
            If FileData.Count = 1 Then BotIdx = -1
            Select Case ForWhat
                Case "INIT"
                    NewTopHeight = FullHeight
                    NewBotHeight = FullHeight
                    NewTopTop = 0
                    NewBotTop = 0
                    FileData(TopIdx).Top = NewTopTop
                    FileData(TopIdx).Height = NewTopHeight
                    FileData(TopIdx).Visible = True
                    If BotIdx >= 0 Then
                        FileData(BotIdx).Top = NewBotTop
                        FileData(BotIdx).Height = NewBotHeight
                        FileData(BotIdx).Visible = False
                    End If
                Case "SIZE"
                Case Else
            End Select
        ElseIf chkGrid(1).Value = 1 Then
            TopIdx = 1
            BotIdx = 0
            If FileData.Count = 1 Then TopIdx = -1
            Select Case ForWhat
                Case "INIT"
                    NewTopHeight = FullHeight
                    NewBotHeight = FullHeight
                    NewTopTop = 0
                    NewBotTop = 0
                    If TopIdx >= 0 Then
                        FileData(TopIdx).Top = NewTopTop
                        FileData(TopIdx).Height = NewTopHeight
                        FileData(TopIdx).Visible = True
                    End If
                    FileData(BotIdx).Top = NewBotTop
                    FileData(BotIdx).Height = NewBotHeight
                    FileData(BotIdx).Visible = False
                Case "SIZE"
                Case Else
            End Select
        ElseIf chkGrid(2).Value = 0 Then
            TopIdx = 0
            BotIdx = 1
            If FileData.Count = 1 Then BotIdx = -1
            Select Case ForWhat
                Case "INIT"
                    NewTopHeight = FullHeight
                    NewBotHeight = FullHeight
                    NewTopTop = 0
                    NewBotTop = 0
                    FileData(TopIdx).Top = NewTopTop
                    FileData(TopIdx).Height = NewTopHeight
                    FileData(TopIdx).Visible = True
                    If BotIdx >= 0 Then
                        FileData(BotIdx).Top = NewBotTop
                        FileData(BotIdx).Height = NewBotHeight
                        FileData(BotIdx).Visible = False
                    End If
                Case "SIZE"
                Case Else
            End Select
        End If
    End If
End Sub
Private Sub chkRestore_Click()
    Dim UsePath As String
    Dim UseName As String
    Dim PathAndFile As String
    Dim CurCfgTag As String
    Dim CurCfgText As String
    If chkRestore.Value = 1 Then
        If PushedImpButton >= 0 Then chkImpTyp(PushedImpButton).Value = 0
        UsePath = App.Path
        UseName = ""
        PathAndFile = UsePath & UseName
        UfisTools.CommonDialog.DialogTitle = "Open Backup file"
        UfisTools.CommonDialog.InitDir = UsePath
        UfisTools.CommonDialog.FileName = "*.ite"
        UfisTools.CommonDialog.Filter = "Import Tool Backup Files|*.ite"
        UfisTools.CommonDialog.FilterIndex = 1
        UfisTools.CommonDialog.Flags = (cdlOFNHideReadOnly)
        UfisTools.CommonDialog.CancelError = True
        On Error GoTo ErrorHandle
        UfisTools.CommonDialog.ShowOpen
        If UfisTools.CommonDialog.FileName <> "" Then
            Screen.MousePointer = 11
            RestoreAction = True
            Initializing = True
            PathAndFile = UfisTools.CommonDialog.FileName
            CurCfgTag = MySetUp.ServerTime.Tag
            CurCfgText = MySetUp.ServerTime.Text
            ReadForm PathAndFile, MySetUp, True
            MySetUp.ServerTime.Tag = CurCfgTag
            MySetUp.ServerTime.Text = CurCfgText
            RestoreAction = True
            PushedImpButton = Val(GetIniFileEntry(Me.Name, Me.Name & "_IMP_BUTTON", "-1", PathAndFile))
            If PushedImpButton >= 0 Then
                chkImpTyp(PushedImpButton).Value = 1
            End If
            Initializing = False
            ReadForm PathAndFile, Me, True
            RestoreAction = True
            ExtFldCnt = Val(GetIniFileEntry(Me.Name, Me.Name & "_COL_COUNT", Str(ExtFldCnt), PathAndFile))
            UpdateMode = Val(GetIniFileEntry(Me.Name, Me.Name & "_UPDT_MODE", Str(CInt(UpdateMode)), PathAndFile))
            FullMode = Val(GetIniFileEntry(Me.Name, Me.Name & "_FULL_MODE", Str(CInt(FullMode)), PathAndFile))
            GridPrepared = True
            InitDataList 10, False
            If MySetUp.MonitorSetting(1).Value = 1 Then ToggleDateFormat True Else ToggleDateFormat False
            RestoreLineStatus
            RestoreCellObjectInfo
            RestoreAction = False
            chkRestore.Value = 0
            Screen.MousePointer = 0
        End If
        Exit Sub
    End If
ErrorHandle:
    chkRestore.Value = 0
End Sub


Private Sub chkCheck_Click()
    Dim tmpTag As String
    On Error Resume Next
    If Not RestoreAction Then
        If Not TotalReset Then
            If chkCheck.Value = 1 Then
                'If IsDoubleGridView Then chkGrid(0).Value = 1
                chkClear.Value = 1
                tmpTag = chkDispo.Tag
                chkDispo.Tag = ""
                chkDispo.Value = 1
                chkDispo.Tag = tmpTag
                PerformChecks 0, -1
                CheckRouteInfo
                chkCheck.BackColor = LightestGreen
            Else
                MainDialog.MousePointer = 11
                chkIdentify.Value = 0
                ClearErrorStatus 0, -1
                chkCheck.BackColor = vbButtonFace
                MainDialog.MousePointer = 0
            End If
        End If
        If TabFrame.Enabled = True Then FileData(CurFdIdx).SetFocus
    End If
End Sub

Private Sub PerformChecks(LoopStart As Long, LoopEnd As Long)
Dim tmpRowNum As Long
Dim tmpFldNam As String
Dim tmpTblNam As String
Dim DontCheck As String
Dim FieldList As String
Dim ErrLinLst As String
Dim ErrLinNbr As Integer
Dim Columns As String
Dim i As Integer
Dim DataLen As Integer
    MainDialog.MousePointer = 11
    'must be checked again in case of manual changes
    CheckVpfrVptoFrame LoopStart, LoopEnd
    CheckEmptyLines LoopStart, LoopEnd
    If CedaIsConnected Then
        If AutoCheck <> "" Then
            i = 0
            Do
                tmpFldNam = GetItem(AutoCheck, i + 1, ",")
                'cfg rule syntax will be changed soon
                DataLen = Val(GetItem(tmpFldNam, 2, "/"))
                tmpFldNam = GetItem(tmpFldNam, 1, "/")
                DontCheck = GetItem(AutoCheck, i + 2, ",")
                tmpTblNam = GetItem(AutoCheck, i + 3, ",")
                tmpRowNum = Val(GetItem(AutoCheck, i + 4, ",")) + 2
                FieldList = GetItem(AutoCheck, i + 5, ",")
                Columns = GetItem(AutoCheck, i + 6, ",")
                If tmpFldNam <> "" Then CheckBasicData LoopStart, LoopEnd, tmpRowNum, tmpFldNam, tmpTblNam, DataLen, FieldList, Columns, DontCheck
                i = i + 6
            Loop While tmpFldNam <> ""
        End If
        If ChkBasDat <> "" Then
            i = 0
            Do
                tmpFldNam = GetItem(ChkBasDat, i + 1, ",")
                'cfg rule syntax will be changed soon
                DataLen = Val(GetItem(tmpFldNam, 2, "/"))
                tmpFldNam = GetItem(tmpFldNam, 1, "/")
                DontCheck = GetItem(ChkBasDat, i + 2, ",")
                tmpTblNam = GetItem(ChkBasDat, i + 3, ",")
                tmpRowNum = Val(GetItem(ChkBasDat, i + 4, ",")) + 2
                If tmpFldNam <> "" Then CheckBasicData LoopStart, LoopEnd, tmpRowNum, tmpFldNam, tmpTblNam, DataLen, "", "", DontCheck
                i = i + 4
            Loop While tmpFldNam <> ""
        End If
    Else
        'If MyMsgBox.CallAskUser(0, 0, 0, "Check Basic Data", "You are not connected to a server.", "netfail", "", UserAnswer) >= 0 Then DoNothing
    End If
    FileData(CurFdIdx).AutoSizeColumns
    FileData(CurFdIdx).RedrawTab
    ResetErrorCounter
    MainDialog.MousePointer = 0
End Sub

Public Sub ClearErrorStatus(LoopStart As Long, LoopEnd As Long)
    ClearColumnValue LoopStart, LoopEnd, ExtFldCnt, 0, True
    ClearColumnValue LoopStart, LoopEnd, 1, 0, True
    ClearColumnValue LoopStart, LoopEnd, 2, 0, True
End Sub

Private Sub CheckEmptyLines(LoopBegin As Long, LoopEnd As Long)
Dim CurLine As Long
Dim MaxLine As Long
Dim tmpFldVal As String
Dim ColIdx As Long
Dim ItmNbr As Integer
Dim InvalidLine As Boolean
    If IgnoreEmpty <> "" Then
        If LoopEnd >= 0 Then
            MaxLine = LoopEnd
        Else
            MaxLine = FileData(CurFdIdx).GetLineCount - 1
        End If
        For CurLine = LoopBegin To MaxLine
            InvalidLine = True
            ItmNbr = 0
            tmpFldVal = FileData(CurFdIdx).GetColumnValue(CurLine, 1)
            If tmpFldVal = "" Then
                Do
                    ItmNbr = ItmNbr + 1
                    ColIdx = Val(GetItem(IgnoreEmpty, ItmNbr, ",")) + 2
                    If ColIdx > 2 Then tmpFldVal = FileData(CurFdIdx).GetColumnValue(CurLine, ColIdx)
                    If tmpFldVal <> "" Then
                        InvalidLine = False
                        ColIdx = 0
                    End If
                Loop While ColIdx > 2
                If InvalidLine Then
                    FileData(CurFdIdx).SetColumnValue CurLine, 1, "-"
                    FileData(CurFdIdx).SetColumnValue CurLine, 2, "-"
                    FileData(CurFdIdx).SetColumnValue CurLine, ExtFldCnt, "IGNORED"
                    FileData(CurFdIdx).SetLineColor CurLine, vbBlack, LightGray
                End If
            End If
        Next
    End If
End Sub

Private Sub ShowEditGroup(bpFlag)
    txtEdit.Enabled = bpFlag
    If bpFlag = False Then
        chkEdit.Tag = ""
        txtEdit.Text = ""
        txtEdit.Tag = ""
        chkReplace.ToolTipText = ""
        chkAll.ToolTipText = ""
        chkAll.Tag = ""
        txtEdit.BackColor = vbButtonFace
        txtEdit.Locked = True
    Else
        txtEdit.BackColor = vbWhite
        txtEdit.Locked = False
    End If
    chkReplace.Enabled = bpFlag
    chkAll.Enabled = False
    FileData(CurFdIdx).EnableHeaderSelection bpFlag
End Sub

Private Sub ClearColumnValue(LoopStart As Long, LoopEnd As Long, lpCol As Long, lpLen As Long, ClearErrorLines As Boolean)
    Dim MyForeColor As Long
    Dim MyBackColor As Long
    Dim llCount As Long
    Dim i As Long
    Dim StatCol As Long
    Dim clData As String
    Dim tmpData As String
    MousePointer = 11
    If lpLen > 0 Then clData = Space(lpLen) Else clData = ""
    If LoopEnd >= 0 Then
        llCount = LoopEnd
    Else
        llCount = FileData(CurFdIdx).GetLineCount - 1
    End If
    StatCol = ExtFldCnt + 1
    For i = LoopStart To llCount
        If ClearErrorLines Then
            FileData(CurFdIdx).SetColumnValue i, lpCol, clData
        Else
            If lpCol = 1 Then
                tmpData = FileData(CurFdIdx).GetColumnValue(i, lpCol)
                tmpData = Trim(tmpData)
                If tmpData <> "E" Then
                    FileData(CurFdIdx).SetColumnValue i, lpCol, clData
                End If
            End If
        End If
        If ClearErrorLines Then
            FileData(CurFdIdx).SetLineColor i, vbBlack, vbWhite
            FileData(CurFdIdx).ResetLineDecorations i
            FileData(CurFdIdx).SetColumnValue i, StatCol, ""
        Else
            FileData(CurFdIdx).GetLineColor i, MyForeColor, MyBackColor
            If MyBackColor <> ErrorColor Then FileData(CurFdIdx).SetLineColor i, vbBlack, vbWhite
        End If
    Next
    MousePointer = 0
End Sub
Private Sub CheckBasicData(LoopStart As Long, LoopEnd As Long, tmpCol As Long, tmpFldNam As String, tmpTblNam As String, DataLen As Integer, FieldList As String, Columns As String, DontCheck As String)
Dim ChkList As String
Dim llCount As Long
Dim i As Long
Dim ItmNbr As Integer
Dim tmpData As String
Dim chkData As String
Dim RetCod
Dim tmpSqlKey As String
Dim tmpResult As String
Dim tmpColLst As String
Dim tmpFldLst As String
Dim tmpFldVal As String
Dim llLine As Long
Dim CurColIdx As Long
Dim clLineIdx As String
Dim LinItm As Integer
Dim Col0Val As String
Dim Col1Val As String
Dim Col2Val As String
Dim CedaData As String
    If CedaIsConnected Then
        StatusBar.Panels(7).Text = "Check Basic Data: " & tmpFldNam
        If LoopEnd >= 0 Then
            llCount = LoopEnd
        Else
            llCount = FileData(CurFdIdx).GetLineCount - 1
        End If
        ChkList = ""
        For i = LoopStart To llCount
            Col0Val = FileData(CurFdIdx).GetColumnValue(i, 0)
            Col1Val = FileData(CurFdIdx).GetColumnValue(i, 1)
            Col2Val = FileData(CurFdIdx).GetColumnValue(i, 2)
            If Not ((Col1Val = "-") And (Col2Val = "-")) Then
                tmpData = FileData(CurFdIdx).GetColumnValue(i, tmpCol)
                If DataLen > 0 Then
                    If Len(tmpData) <> DataLen Then tmpData = ""
                End If
                If (DontCheck <> "") And (tmpData <> "") Then
                    If InStr(DontCheck, tmpData) > 0 Then
                        tmpData = ""
                    End If
                End If
                If tmpData <> "" Then
                    tmpData = "'" & tmpData & "'"
                    If InStr(ChkList, tmpData) = 0 Then ChkList = ChkList & tmpData & ","
                End If
            End If
        Next
        If ChkList <> "" Then
            If Len(ChkList) > 0 Then ChkList = Left(ChkList, Len(ChkList) - 1)
            tmpSqlKey = "WHERE " & tmpFldNam & " IN (" & ChkList & ")"
            tmpFldLst = tmpFldNam
            If FieldList <> "" Then
                tmpFldLst = tmpFldLst & "," & Replace(FieldList, ";", ",", 1, -1, vbBinaryCompare)
            End If
            RetCod = UfisServer.CallCeda(tmpResult, "RTA", tmpTblNam, tmpFldLst, "", tmpSqlKey, "", 1, False, False)
            If DontCheck <> "" Then
                ChkList = ChkList & "," & Replace(DontCheck, ";", ",", 1, -1, vbBinaryCompare)
            End If
            llCount = UfisServer.DataBuffer(1).GetLineCount - 1
            For i = 0 To llCount
                CedaData = UfisServer.DataBuffer(1).GetLineValues(i)
                tmpFldVal = GetItem(CedaData, 1, ",")
                tmpData = "'" & tmpFldVal & "'"
                ChkList = Replace(ChkList, tmpData, "", 1, -1, vbBinaryCompare)
                If FieldList <> "" Then
                    tmpColLst = FileData(CurFdIdx).GetLinesByColumnValue(tmpCol, tmpFldVal, 0)
                    clLineIdx = "START"
                    LinItm = 0
                    While clLineIdx <> ""
                        LinItm = LinItm + 1
                        clLineIdx = GetItem(tmpColLst, LinItm, ",")
                        If clLineIdx <> "" Then
                            llLine = Val(clLineIdx)
                            Col1Val = FileData(CurFdIdx).GetColumnValue(llLine, 1)
                            Col2Val = FileData(CurFdIdx).GetColumnValue(llLine, 2)
                            If Not ((Col1Val = "-") And (Col2Val = "-")) Then
                                tmpResult = tmpFldNam & ":" & tmpData
                                tmpFldVal = GetItem(CedaData, 2, ",")
                                CurColIdx = Val(GetItem(Columns, 1, ";")) + 2
                                If CurColIdx > 2 Then
                                    FileData(CurFdIdx).SetColumnValue llLine, CurColIdx, tmpFldVal
                                End If
                            End If
                        End If
                    Wend
                End If
            Next
            ChkList = Replace(ChkList, "'", "", 1, -1, vbBinaryCompare)
            ChkList = ChkList & ",*END*"
            ItmNbr = 0
            tmpData = "START"
            While ItmNbr >= 0
                ItmNbr = ItmNbr + 1
                tmpData = GetItem(ChkList, ItmNbr, ",")
                If (tmpData <> "") Then
                    If (tmpData <> "*END*") Then
                        tmpColLst = FileData(CurFdIdx).GetLinesByColumnValue(tmpCol, tmpData, 0)
                        clLineIdx = "START"
                        LinItm = 0
                        While clLineIdx <> ""
                            LinItm = LinItm + 1
                            clLineIdx = GetItem(tmpColLst, LinItm, ",")
                            If clLineIdx <> "" Then
                                llLine = Val(clLineIdx)
                                Col1Val = FileData(CurFdIdx).GetColumnValue(llLine, 1)
                                Col2Val = FileData(CurFdIdx).GetColumnValue(llLine, 2)
                                If Not ((Col1Val = "-") And (Col2Val = "-")) Then
                                    tmpResult = tmpFldNam & ":" & tmpData
                                    SetErrorText Str(tmpCol), llLine, ExtFldCnt, tmpResult, True, 0
                                End If
                            End If
                        Wend
                    Else
                        ItmNbr = -1
                    End If
                End If
            Wend
        End If
        StatusBar.Panels(7).Text = "Ready"
        Refresh
    Else
        If MyMsgBox.CallAskUser(0, 0, 0, "Check Basic Data", "You are not connected to a server.", "stop", "", UserAnswer) >= 0 Then DoNothing
    End If
End Sub

Private Sub SetErrorRange(ColIdx As Long, FirstLine As Long, LastLine As Long, ErrTxt As String)
    Dim CurLine As Long
    For CurLine = FirstLine To LastLine
        SetErrorText Str(ColIdx), CurLine, ExtFldCnt, ErrTxt, True, 1
    Next
End Sub
Private Sub SetErrorText(ErrColList As String, tmpLine As Long, tmpCol As Long, tmpText As String, Markit As Boolean, ErrPrio As Integer)
Dim ErrorCol As Long
Dim ItmNbr As Integer
Dim ItmTxt As String
Dim tmpResult As String
Dim tmpDat As String
    Select Case ErrPrio
        Case 1
            FileData(CurFdIdx).SetLineColor tmpLine, vbBlack, LightestRed
            If Markit Then FileData(CurFdIdx).SetColumnValue tmpLine, 1, "W"
        Case Else
            FileData(CurFdIdx).SetLineColor tmpLine, vbWhite, ErrorColor
            If Markit Then FileData(CurFdIdx).SetColumnValue tmpLine, 1, "E"
    End Select
    tmpResult = FileData(CurFdIdx).GetColumnValue(tmpLine, tmpCol)
    If InStr(tmpResult, tmpText) = 0 Then tmpResult = tmpResult & tmpText & " "
    FileData(CurFdIdx).SetColumnValue tmpLine, tmpCol, tmpResult
    If Markit Then FileData(CurFdIdx).SetColumnValue tmpLine, 2, "-"
    If ErrColList <> "" Then
        ItmTxt = "START"
        ItmNbr = 0
        While ItmTxt <> ""
            ItmNbr = ItmNbr + 1
            ItmTxt = GetItem(ErrColList, ItmNbr, ",")
            If ItmTxt <> "" Then
                ErrorCol = Val(ItmTxt)
                If ErrorCol > 2 Then
                    FileData(CurFdIdx).SetDecorationObject tmpLine, ErrorCol, "CellError"
                    StoreCellObjectInfo tmpLine, ErrorCol
                End If
            End If
        Wend
    End If
End Sub

Private Sub StoreCellObjectInfo(LineNo As Long, ColNo As Long)
    Dim StatCol As Long
    Dim CurList As String
    Dim ChkList As String
    Dim AddVal As String
    Dim ChkCol As String
    StatCol = ExtFldCnt + 1
    AddVal = Trim(Str(ColNo))
    CurList = FileData(CurFdIdx).GetColumnValue(LineNo, StatCol)
    If CurList <> "" Then
        ChkList = "/" & CurList & "/"
        ChkCol = "/" & AddVal & "/"
        If InStr(ChkList, ChkCol) = 0 Then CurList = CurList & "/" & AddVal
    Else
        CurList = AddVal
    End If
    FileData(CurFdIdx).SetColumnValue LineNo, StatCol, CurList
End Sub
Private Sub RestoreCellObjectInfo()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim ColNo As Long
    Dim ItmNbr As Integer
    Dim StatCol As Long
    Dim CurList As String
    Dim ChkCol As String
    StatCol = ExtFldCnt + 1
    MaxLine = FileData(CurFdIdx).GetLineCount - 1
    For CurLine = 0 To MaxLine
        CurList = FileData(CurFdIdx).GetColumnValue(CurLine, StatCol)
        ChkCol = CurList
        ItmNbr = 0
        While ChkCol <> ""
            ItmNbr = ItmNbr + 1
            ChkCol = GetItem(CurList, ItmNbr, "/")
            If ChkCol <> "" Then
                ColNo = Val(ChkCol)
                FileData(CurFdIdx).SetDecorationObject CurLine, ColNo, "CellError"
            End If
        Wend
    Next
End Sub

Private Sub ResetErrorCounter()
    Dim ErrLinLst As String
    Dim ErrLinNbr As Integer
    ErrLinLst = FileData(CurFdIdx).GetLinesByBackColor(ErrorColor)
    ErrLinNbr = ItemCount(ErrLinLst, ",")
    If ErrLinNbr > 0 Then
        ErrCnt.Caption = Str(Trim(ErrLinNbr))
        ErrCnt.BackColor = vbRed
        ErrCnt.ForeColor = vbWhite
    Else
        ErrCnt.Caption = ""
        ErrCnt.BackColor = vbButtonFace
        ErrCnt.ForeColor = vbBlack
    End If
    ErrCnt.Refresh
    ErrLinLst = FileData(CurFdIdx).GetLinesByBackColor(LightestRed)
    ErrLinNbr = ItemCount(ErrLinLst, ",")
    If ErrLinNbr > 0 Then
        WarnCnt.Caption = Str(Trim(ErrLinNbr))
        WarnCnt.BackColor = LightestRed
        WarnCnt.ForeColor = vbBlack
    Else
        WarnCnt.Caption = ""
        WarnCnt.BackColor = vbButtonFace
        WarnCnt.ForeColor = vbBlack
    End If
    WarnCnt.Refresh
End Sub

Private Sub chkCheck_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim MsgTxt As String
    Dim LineNo As Long
    If (Button = 2) And (chkCheck.Value = 1) Then
        LineNo = FileData(CurFdIdx).GetCurrentSelected
        If LineNo >= 0 Then
            MsgTxt = ""
            MsgTxt = MsgTxt & "Do you want to check" & vbNewLine
            MsgTxt = MsgTxt & "the highlighted line?"
            If MyMsgBox.CallAskUser(0, 0, 0, "Data Validation", MsgTxt, "ask", "Yes,No", UserAnswer) = 1 Then
                ClearErrorStatus LineNo, LineNo
                PerformChecks LineNo, LineNo
                PerformIdentify
            End If
        End If
    End If
End Sub

Private Sub chkClear_Click()
    Dim FilterVpfr As String
    Dim FilterVpto As String
    Dim IsValidLine As Boolean
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim RecData As String
    Dim OldRecData As String
    Dim Col1Txt As String
    Dim Col2Txt As String
    Dim MsgTxt As String
    On Error Resume Next
    If Not RestoreAction Then
        If chkClear.Value = 1 Then
            'If IsDoubleGridView Then chkGrid(0).Value = 1
            chkShrink.Value = 1
            If (MySetUp.chkWork(1).Value = 1) And (chkDontClear.Value = 0) Then
                chkClear.BackColor = LightestGreen
                StatusBar.Panels(7).Text = "Clearing out data of the past ..."
                Refresh
                If Not ((DataSystemType = "SSIM") Or (DataSystemType = "OAFOS")) Then
                    Screen.MousePointer = 11
                    InitPeriodCheck MainPeriodCols, False
                    FilterVpfr = ActSeason(3).Tag
                    FilterVpto = ActSeason(2).Tag
                    MaxLine = FileData(CurFdIdx).GetLineCount - 1
                    CurLine = 0
                    While CurLine <= MaxLine
                        IsValidLine = True
                        RecData = FileData(CurFdIdx).GetLineValues(CurLine)
                        Col1Txt = GetItem(RecData, 2, ",")
                        Col2Txt = GetItem(RecData, 3, ",")
                        If (Col1Txt <> "E" And Col1Txt <> "-") And Col2Txt <> "-" Then
                            OldRecData = RecData
                            IsValidLine = CheckFlightPeriod(FilterVpfr, FilterVpto, RecData, IsValidLine, True, False, "")
                            If IsValidLine Then
                                If OldRecData <> RecData Then
                                    FileData(CurFdIdx).UpdateTextLine CurLine, RecData, False
                                End If
                            Else
                                FileData(CurFdIdx).DeleteLine CurLine
                                CurLine = CurLine - 1
                                MaxLine = MaxLine - 1
                            End If
                        End If
                        CurLine = CurLine + 1
                    Wend
                    FileData(CurFdIdx).RedrawTab
                    MaxLine = FileData(CurFdIdx).GetLineCount
                    StatusBar.Panels(2).Text = Str(MaxLine)
                    chkClear.Enabled = False
                    StatusBar.Panels(7).Text = "Ready"
                    Refresh
                    Screen.MousePointer = 0
                    ResetErrorCounter
                Else
                    MsgTxt = ""
                    MsgTxt = MsgTxt & "The function 'Clear Out' is" & vbNewLine
                    MsgTxt = MsgTxt & "not yet available for '" & DataDisplayType & "'"
                    If MyMsgBox.CallAskUser(0, 0, 0, "Configuration Control", MsgTxt, "hand", "", UserAnswer) >= 0 Then DoNothing
                    StatusBar.Panels(7).Text = ""
                    Refresh
                End If
            Else
                '
            End If
            If chkDontClear.Value = 0 Then
                chkDontClear.Value = 1
                chkDontClear.BackColor = LightestGreen
                chkDontClear.Caption = ""
                chkDontClear.Enabled = False
            End If
        Else
            chkClear.BackColor = vbButtonFace
        End If
        If TabFrame.Enabled = True Then FileData(CurFdIdx).SetFocus
    End If
End Sub

Private Sub chkDontClear_Click()
    On Error Resume Next
    If Not RestoreAction Then
        If chkDontClear.Value = 1 Then
            'If IsDoubleGridView Then chkGrid(0).Value = 1
            chkDontClear.BackColor = vbRed
            chkDontClear.ForeColor = vbWhite
            chkDontClear.Caption = "X"
            chkClear.Value = 1
        Else
            chkDontClear.BackColor = vbButtonFace
            chkDontClear.ForeColor = vbBlack
            chkDontClear.Caption = ">"
        End If
        If TabFrame.Enabled = True Then FileData(CurFdIdx).SetFocus
    End If
End Sub

Private Sub chkEdit_Click()
    On Error Resume Next
    If chkEdit.Value = 1 Then
        ShowEditGroup True
        chkEdit.BackColor = LightestGreen
        chkInline.Value = 0
    Else
        chkEdit.BackColor = vbButtonFace
        ShowEditGroup False
    End If
    If TabFrame.Enabled = True Then FileData(CurFdIdx).SetFocus
End Sub

Private Sub chkExpand_Click()
    On Error Resume Next
    If Not RestoreAction Then
        If (Not TotalReset) Then
            IsDoubleGridView = False
            ResetTimeFields
            If chkExpand.Value = 1 Then
                StopMyApplEvents = True
                chkRequest.Visible = False
                chkSplit.Value = 1
                StopMyApplEvents = False
                CurrentHeader = MainExpandHeader
                CurFldPosLst = ExpPosLst
                chkExpand.Enabled = True
            Else
                chkCheck.Value = 0
                chkShrink.Value = 0
                chkClear.Value = 0
                chkDontClear.Value = 0
                chkClear.Enabled = True
                chkDontClear.Enabled = True
                chkExpand.BackColor = vbButtonFace
                CurrentHeader = NormalHeader
                CurFldPosLst = FldPosLst
            End If
            If chkLoad.Value = 1 Then
                If Not StopMyApplEvents Then
                    LoadFileData "(for preparation)"
                End If
            Else
                chkLoad.Value = 1
            End If
            FileData(CurFdIdx).RedrawTab
            TabFrame.Enabled = True
            If chkExpand.Value = 1 Then
                chkExpand.BackColor = LightGreen
                chkExpand.Enabled = True
                If (DoubleGridInit) Then
                    chkImpTyp(PushedImpButton).BackColor = vbYellow
                    chkImpTyp(PushedImpButton).Enabled = False
                    ReleaseMain -1, True
                    IsDoubleGridView = True
                    chkGrid(2).Value = 1
                End If
                MySetUp.MonitorSetting(0).Enabled = True
                MySetUp.MonitorSetting(1).Enabled = True
            Else
                MySetUp.MonitorSetting(0).Enabled = False
                MySetUp.MonitorSetting(1).Enabled = False
            End If
            If TabFrame.Enabled = True Then FileData(CurFdIdx).SetFocus
        End If
    End If
End Sub

Private Sub chkExtract_Click()
    Dim Answ As Integer
    Dim MsgTxt As String
    Dim tmpBool As Boolean
    If Not RestoreAction Then
        If chkExtract.Value = 1 Then
            'If IsDoubleGridView Then chkGrid(0).Value = 1
            chkEdit.Value = 0
            chkInline.Value = 0
            FileData(CurFdIdx).Sort "0", True, True
            FileData(CurFdIdx).Refresh
            chkIdentify.Value = 1
            If chkTimes(0).Enabled Then chkTimes(0).Value = 1
            tmpBool = chkAppl(2).Enabled
            chkAppl(2).Enabled = False
            chkAppl(2).Value = 1
            chkAppl(2).Enabled = tmpBool
            Answ = 1
            MsgTxt = ""
            If Val(ErrCnt.Caption) > 0 Then
                MsgTxt = MsgTxt & ErrCnt.Caption & " errors"
            End If
            If Val(WarnCnt.Caption) > 0 Then
                If MsgTxt <> "" Then MsgTxt = MsgTxt & " and "
                MsgTxt = MsgTxt & WarnCnt.Caption & " warnings"
            End If
            If MsgTxt <> "" Then
                MsgTxt = "This file contains still" & MsgTxt & vbNewLine & "Do you want to proceed?"
                Answ = MyMsgBox.CallAskUser(0, 0, 0, "File Import Control", MsgTxt, "query", "Yes,No;F", UserAnswer)
            End If
            If Answ = 1 Then
                chkSort.Value = 1
                MousePointer = 11
                chkSort.Enabled = False
                If MySetUp.MonitorSetting(3).Value = 1 Then
                    FlightExtract.Show , Me
                Else
                    FlightExtract.Show
                End If
                MousePointer = 0
                chkExtract.BackColor = LightestGreen
            Else
                chkExtract.Value = 0
            End If
        Else
            If FormIsVisible("RecFilter") Then RecFilter.Hide
            If FormIsVisible("FlightExtract") Then FlightExtract.Hide
            If DataSystemType = "FHK" Then chkSort.Enabled = True
            chkExtract.BackColor = vbButtonFace
            If TabFrame.Enabled = True Then FileData(CurFdIdx).SetFocus
        End If
    End If
End Sub

Private Sub chkFilter_Click()
    If Not RestoreAction Then
        If Not TotalReset Then
            MySetUp.chkFilter.Value = chkFilter.Value
            If chkFilter.Value = 0 Then
                chkFilter.BackColor = vbButtonFace
                StopMyApplEvents = True
                chkSplit.Value = 0
                StopMyApplEvents = False
            End If
            If chkLoad.Value = 1 Then LoadFileData "(For Filter)" Else chkLoad.Value = 1
            If chkFilter.Value = 1 Then chkFilter.BackColor = LightestGreen
        End If
    End If
End Sub

Private Sub chkIdentify_Click()
    On Error Resume Next
    If Not RestoreAction Then
        If Not TotalReset Then
            ArrFltCnt = 0
            DepFltCnt = 0
            If chkIdentify.Value = 1 Then
                'If IsDoubleGridView Then chkGrid(0).Value = 1
                chkCheck.Value = 1
                If PreCheckDoubles = "AUTO" Then chkAppl(2).Value = 1
                PerformIdentify
                chkIdentify.BackColor = LightestGreen
            Else
                chkExtract.Value = 0
                Unload FlightExtract
                chkIdentify.BackColor = vbButtonFace
                ClearColumnValue 0, -1, 1, 0, False
            End If
            FileData(CurFdIdx).RedrawTab
        End If
        If TabFrame.Enabled = True Then FileData(CurFdIdx).SetFocus
    End If
End Sub
Private Sub PerformIdentify()
    If chkIdentify.Value = 1 Then
        If LegColNbr > 2 And OrgColNbr > 2 And DesColNbr > 2 Then
            IdentifyFlights 0, LegColNbr, OrgColNbr, DesColNbr
        ElseIf CheckRotation <> "" Then
            IdentifyRotations CheckRotation
        End If
        ResetErrorCounter
    End If
End Sub
Private Sub IdentifyRotations(FltnList As String)
Dim CurLine As Long
Dim MaxLine As Long
Dim FlnaCol As Long
Dim FlndCol As Long
Dim AdidCol As Long
Dim tmpFlna As String
Dim tmpFlnd As String
Dim tmpAdid As String
Dim Col1Val As String
Dim Col2Val As String
    If FltnList <> "" Then
        MousePointer = 11
        If InStr(FltnList, "ADID") > 0 Then
            AdidCol = Val(GetItem(FltnList, 2, ",")) + 2
        Else
            FlnaCol = Val(GetItem(FltnList, 1, ",")) + 2
            FlndCol = Val(GetItem(FltnList, 2, ",")) + 2
        End If
        If ((FlnaCol > 2) And (FlndCol > 2)) Or (AdidCol > 2) Then
            MaxLine = FileData(CurFdIdx).GetLineCount - 1
            For CurLine = 0 To MaxLine
                Col1Val = FileData(CurFdIdx).GetColumnValue(CurLine, 1)
                Col2Val = FileData(CurFdIdx).GetColumnValue(CurLine, 2)
                If (Col1Val <> "E") And (Col2Val <> "-") Then
                    If AdidCol > 2 Then
                        tmpAdid = FileData(CurFdIdx).GetColumnValue(CurLine, AdidCol)
                        If tmpAdid = "A" Then
                            FileData(CurFdIdx).SetLineColor CurLine, vbBlack, vbYellow
                        ElseIf tmpAdid = "D" Then
                            FileData(CurFdIdx).SetLineColor CurLine, vbBlack, vbGreen
                        Else
                            'empty line ??
                            SetErrorText Str(AdidCol), CurLine, ExtFldCnt, "ADID:???", True, 0
                        End If
                    Else
                        tmpFlna = FileData(CurFdIdx).GetColumnValue(CurLine, FlnaCol)
                        tmpFlnd = FileData(CurFdIdx).GetColumnValue(CurLine, FlndCol)
                        If (tmpFlna <> "") And (tmpFlnd <> "") Then
                            'rotation
                        ElseIf tmpFlna <> "" Then
                            FileData(CurFdIdx).SetLineColor CurLine, vbBlack, vbYellow
                        ElseIf tmpFlnd <> "" Then
                            FileData(CurFdIdx).SetLineColor CurLine, vbBlack, vbGreen
                        Else
                            'empty line ??
                            SetErrorText "", CurLine, ExtFldCnt, "FLTN:???", True, 0
                        End If
                    End If
                End If
            Next
        End If
        MousePointer = 0
    End If
End Sub
Private Sub chkImport_Click()
    If Not RestoreAction Then
        If chkImport.Value = 1 Then
            OaFosImport
        Else
            chkImport.BackColor = vbButtonFace
        End If
    End If
End Sub

Public Sub OaFosImport()
Dim ErrMsgTxt As String
        ErrMsgTxt = ""
        If AftFldLst = "" Then
            ErrMsgTxt = ErrMsgTxt & "Missing AFT field list for output" & vbNewLine
        End If
        If ExtFldLst = "" Then
            ErrMsgTxt = ErrMsgTxt & "Missing EXT field list for output" & vbNewLine
        End If
        If ErrMsgTxt = "" Then
            Select Case ActionType
                Case "CEDA_CMD"
                    PrecheckCedaAction 3, LightGray
                Case Else
            End Select
            chkImport.BackColor = LightestGreen
        Else
            MyMsgBox.InfoApi 0, "The keywords AFT_FIELDS or EXT_FIELDS" & vbNewLine & "are not included in ImportTool.ini for this session.", "More info"
            If MyMsgBox.CallAskUser(0, 0, 0, "Configuration Control", ErrMsgTxt, "stop", "", UserAnswer) >= 0 Then DoNothing
        End If
End Sub

Private Sub PrecheckCedaAction(doRelease As Integer, CheckColor As Long)
Dim pclFullName As String
Dim CurLine As Long
Dim MaxLine As Long
Dim FirstLine As Long
Dim LastLine As Long
Dim tmpCol1Val As String
Dim tmpCol2Val As String
Dim tmpFldNam As String
Dim tmpFldVal As String
Dim CurFlno As String
Dim OldFlno As String
Dim CurAdid As String
Dim OldAdid As String
Dim tmpFlcaVal As String
Dim tmpFltnVal As String
Dim ErrorTrip As Boolean
Dim ArrivalTrip As Boolean
Dim DepartureTrip As Boolean
Dim ViaTrip As Boolean
Dim FldColIdx As Long
Dim FlcaCol As Long
Dim FltnCol As Long
    Me.MousePointer = 11
    'MsgBox "Precheck " & doRelease
    If doRelease = 1 Then WarnCnt.Caption = 0
    If OutputType = "LOCAL_FILE" Then
        pclFullName = myLoadPath & "\CMD_" & myLoadName
        Open pclFullName For Output As #2
    End If
    'hardcode
    FlcaCol = 1 + 2
    FltnCol = 2 + 2
    OldFlno = ""
    CurAdid = "?"
    OldAdid = "?"
    FirstLine = -1
    LastLine = -1
    ErrorTrip = False
    ArrivalTrip = False
    DepartureTrip = False
    ViaTrip = False
    MaxLine = FileData(CurFdIdx).GetLineCount
    For CurLine = 0 To MaxLine
        tmpFlcaVal = FileData(CurFdIdx).GetColumnValue(CurLine, FlcaCol)
        tmpFltnVal = FileData(CurFdIdx).GetColumnValue(CurLine, FltnCol)
        CurFlno = tmpFlcaVal & tmpFltnVal
        tmpCol1Val = FileData(CurFdIdx).GetColumnValue(CurLine, 1)
        tmpCol2Val = FileData(CurFdIdx).GetColumnValue(CurLine, 2)
        If CurFlno <> OldFlno Then
            If OldFlno <> "" Then
                'Run through the flight lines
                'MsgBox OldFlno & " / " & FirstLine + 1 & " / " & LastLine + 1 & " / " & ErrorTrip
                If ErrorTrip Then
                    SetFlightLineErrors FirstLine, LastLine, CheckColor
                Else
                    GetFlightRecInfo FirstLine, LastLine, CurAdid, doRelease
                End If
            End If
            FirstLine = CurLine
            ErrorTrip = False
            ArrivalTrip = False
            DepartureTrip = False
            CurAdid = "?"
            OldFlno = CurFlno
        End If
        If (tmpCol1Val = "A") Or (CurAdid = "?" And tmpCol1Val = "1") Then
            CurAdid = "A"
            ArrivalTrip = True
        ElseIf tmpCol1Val = "D" Then
            CurAdid = tmpCol1Val
            DepartureTrip = True
        Else
        End If
        If tmpCol2Val = "-" Then ErrorTrip = True
        OldFlno = CurFlno
        OldAdid = CurAdid
        LastLine = CurLine
    Next
    If OutputType = "LOCAL_FILE" Then
        Close #2
    End If
    Me.MousePointer = 0
End Sub

Private Sub SetFlightLineErrors(FirstLine As Long, LastLine As Long, BackColor As Long)
    Dim CurLine As Long
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    For CurLine = FirstLine To LastLine
        FileData(CurFdIdx).SetColumnValue CurLine, 2, "-"
        FileData(CurFdIdx).GetLineColor CurLine, CurForeColor, CurBackColor
        If CurBackColor <> ErrorColor Then
            FileData(CurFdIdx).SetLineColor CurLine, vbBlack, BackColor
        End If
    Next
    FileData(CurFdIdx).RedrawTab
End Sub

Private Sub GetFlightRecInfo(FirstLine As Long, LastLine As Long, CurAdid As String, doRelease As Integer)
Dim ColStatus As String
Dim tmpTblNam As String
Dim CurLine As Long
Dim ItmNbr As Integer
Dim FldColIdx As Long
Dim tmpFldNam As String
Dim tmpFldVal As String
Dim TblFldVal As String
Dim tmpOutFldLst As String
Dim tmpOutDatLst As String
Dim ActOutFldLst As String
Dim ActOutDatLst As String
Dim tmpCmdVal As String
Dim tmpActCmd As String
Dim tmpSqlKey As String
Dim tmpUrno As String
Dim RetVal As Integer
Dim ResultBuffer As String
Dim ActRem As String
Dim tmpAlc2 As String
Dim tmpFlno As String
Dim tmpStoaDay As String
Dim tmpStodDay As String
Dim tmpViaList As String
Dim ActViaList As String
Dim FltCnt

    'this function is called 3 times
    'by value of doRelease:
    '1 = step through the list and increase the total number of ceda transactions (flights)
    '2 = for update transactions: read the existing data,
    '    decrease the total number and set the flight line to OK when nothing has to be changed
    '3 = perform the inserts and remaining updates,
    '    decrease the total number and set the flight line to DONE
    
    FltCnt = WarnCnt.Caption
    If doRelease = 1 Then FltCnt = FltCnt + 1
    
    'The first line always contains data of the departure airport (left part)
    'The last line always contains data of the destination airport (right part)
    'All the lines contain data of the aircraft
    'if there is more than one line, the via information must be handled separately
    
    Select Case CurAdid
        Case "A"
            tmpCmdVal = FileData(CurFdIdx).GetColumnValue(LastLine, 2)
            tmpUrno = FileData(CurFdIdx).GetColumnValue(LastLine, ExtFldCnt)
            ColStatus = FileData(CurFdIdx).GetColumnValue(LastLine, 0)
        Case "B"
            tmpCmdVal = FileData(CurFdIdx).GetColumnValue(LastLine, 2)
            tmpUrno = FileData(CurFdIdx).GetColumnValue(LastLine, ExtFldCnt)
            ColStatus = FileData(CurFdIdx).GetColumnValue(LastLine, 0)
        Case "D"
            tmpCmdVal = FileData(CurFdIdx).GetColumnValue(FirstLine, 2)
            tmpUrno = FileData(CurFdIdx).GetColumnValue(FirstLine, ExtFldCnt)
            ColStatus = FileData(CurFdIdx).GetColumnValue(FirstLine, 0)
        Case Else
    End Select
    
    If ColStatus <> "OK" Then
        tmpOutFldLst = ""
        tmpOutDatLst = ""
        
        '1.Get Departure Part
        ItmNbr = 0
        Do
            ItmNbr = ItmNbr + 1
            tmpFldNam = GetItem(AftFldLst, ItmNbr, ",")
            If tmpFldNam <> "" Then
                If InStr(("FLDA,ORG3,STOD,DTD1,ACT3,REGN,FTYP,STYP," & UseStypField), tmpFldNam) > 0 Then
                    FldColIdx = Val(GetItem(ExtFldLst, ItmNbr, ",")) + 2
                    If FldColIdx > 2 Then
                        tmpFldVal = FileData(CurFdIdx).GetColumnValue(FirstLine, FldColIdx)
                        If tmpFldNam = "FTYP" And tmpFldVal = "" Then
                            Select Case tmpFldVal
                                Case "C"
                                    tmpFldVal = "X"
                                Case "N"
                                    tmpFldVal = "N"
                                Case Else
                                    tmpFldVal = "O"
                            End Select
                        End If
                        If tmpFldVal <> "" Then
                            If tmpFldNam = "STOD" Then
                                tmpFldVal = tmpFldVal & "00"
                                tmpStoaDay = Left(tmpFldVal, 8)
                            End If
                            tmpOutFldLst = tmpOutFldLst & tmpFldNam & ","
                            tmpOutDatLst = tmpOutDatLst & tmpFldVal & ","
                        End If
                    End If
                End If
            End If
        Loop While tmpFldNam <> ""
        
        '2.Get Destination Part
        ItmNbr = 0
        Do
            ItmNbr = ItmNbr + 1
            tmpFldNam = GetItem(AftFldLst, ItmNbr, ",")
            If tmpFldNam <> "" Then
                If InStr("DES3,STOA", tmpFldNam) > 0 Then
                    FldColIdx = Val(GetItem(ExtFldLst, ItmNbr, ",")) + 2
                    If FldColIdx > 2 Then
                        tmpFldVal = FileData(CurFdIdx).GetColumnValue(LastLine, FldColIdx)
                        If tmpFldVal <> "" Then
                            If tmpFldNam = "STOA" Then
                                tmpFldVal = tmpFldVal & "00"
                                tmpStodDay = Left(tmpFldVal, 8)
                            End If
                            tmpOutFldLst = tmpOutFldLst & tmpFldNam & ","
                            tmpOutDatLst = tmpOutDatLst & tmpFldVal & ","
                        End If
                    End If
                End If
            End If
        Loop While tmpFldNam <> ""
      
        tmpOutFldLst = tmpOutFldLst & "VIAL,"
        tmpOutDatLst = tmpOutDatLst & ","
        tmpOutFldLst = Left(tmpOutFldLst, Len(tmpOutFldLst) - 1)
        tmpOutDatLst = Left(tmpOutDatLst, Len(tmpOutDatLst) - 1)
        
        Select Case tmpCmdVal
            Case "I"
                'Extract of aftdata.doc ('flight' documentation)
                'Command ISF: Insert Seasonal Flight
                'Selection bei einem Flug
                '1. Date (YYYYMMDD) Begin of the period
                '2. Date (YYYYMMDD) End of the period
                '3. Operatioanl days (1-7) of the flight
                '4. Weekly frequency (1..2..) of the flight
                tmpActCmd = "ISF"
                Select Case CurAdid
                    Case "A"
                        tmpSqlKey = tmpStoaDay & "," & tmpStoaDay & ",1234567,1"
                    Case "B"
                        tmpSqlKey = tmpStodDay & "," & tmpStodDay & ",1234567,1"
                    Case "D"
                        tmpSqlKey = tmpStodDay & "," & tmpStodDay & ",1234567,1"
                    Case Else
                End Select
            Case "U"
                tmpActCmd = "UFR"
                tmpSqlKey = "WHERE URNO=" & tmpUrno
            Case "D"
                'Actually there is no Delete Statement in Data Import Files
            Case Else
        End Select
        
        tmpTblNam = "AFTTAB"
        
        ActRem = ""
        If tmpActCmd = "UFR" Then
            If doRelease > 1 Then
                RetVal = UfisServer.CallCeda(ResultBuffer, "GFR", tmpTblNam, tmpOutFldLst, " ", tmpSqlKey, "", 0, True, False)
                'MsgBox ResultBuffer
                ActOutFldLst = ""
                ActOutDatLst = ""
                ItmNbr = 0
                Do
                    ItmNbr = ItmNbr + 1
                    tmpFldNam = GetItem(tmpOutFldLst, ItmNbr, ",")
                    If tmpFldNam <> "" Then
                        tmpFldVal = GetItem(tmpOutDatLst, ItmNbr, ",")
                        TblFldVal = GetItem(ResultBuffer, ItmNbr, ",")
                        If tmpFldNam = "VIAL" Then
                            tmpFldVal = CheckViaInformation(FirstLine, LastLine, TblFldVal, CurAdid)
                            'MsgBox "VIAL:" & vbNewLine & "DBS <" & TblFldVal & ">" & vbNewLine & "NEW <" & tmpFldVal & ">"
                        End If
                        If tmpFldVal <> TblFldVal Then
                            ActOutFldLst = ActOutFldLst & tmpFldNam & ","
                            ActOutDatLst = ActOutDatLst & tmpFldVal & ","
                        End If
                    End If
                Loop While tmpFldNam <> ""
                If ActOutFldLst <> "" Then
                    ActOutFldLst = Left(ActOutFldLst, Len(ActOutFldLst) - 1)
                    ActOutDatLst = Left(ActOutDatLst, Len(ActOutDatLst) - 1)
                    If doRelease = 3 Then
                        'MsgBox "FOR UPDATE" & vbNewLine & tmpSqlKey & vbNewLine & ActOutFldLst & vbNewLine & ActOutDatLst
                        If OutputType = "LOCAL_FILE" Then
                            Print #2, "CMD:" & tmpActCmd
                            Print #2, "TBL:" & tmpTblNam
                            Print #2, "KEY:" & tmpSqlKey
                            Print #2, "FLD:" & ActOutFldLst
                            Print #2, "DAT:" & ActOutDatLst
                            Print #2, "----------------------------------------------"
                        End If
                        RetVal = UfisServer.CallCeda(ResultBuffer, tmpActCmd, tmpTblNam, ActOutFldLst, ActOutDatLst, tmpSqlKey, "", 0, False, False)
                        ActRem = "DONE"
                    End If
                Else
                    ActRem = "OK"
                End If
            End If
        End If
        If tmpActCmd = "ISF" Then
            If doRelease = 3 Then
                tmpOutFldLst = "FLNO,FLTN,ALC2," & tmpOutFldLst
                FldColIdx = 1 + 2
                tmpAlc2 = FileData(CurFdIdx).GetColumnValue(FirstLine, FldColIdx)
                tmpOutDatLst = tmpAlc2 & "," & tmpOutDatLst
                FldColIdx = 2 + 2
                tmpFldVal = FileData(CurFdIdx).GetColumnValue(FirstLine, FldColIdx)
                tmpOutDatLst = tmpFldVal & "," & tmpOutDatLst 'FLTN
                tmpFlno = Left(tmpAlc2 & Space(3), 3) & tmpFldVal
                tmpOutDatLst = tmpFlno & "," & tmpOutDatLst
                tmpViaList = CheckViaInformation(FirstLine, LastLine, "", CurAdid)
                If tmpViaList <> "" Then
                    tmpOutDatLst = tmpOutDatLst & tmpViaList
                End If
                'MsgBox tmpActCmd & vbNewLine & tmpSqlKey & vbNewLine & tmpOutFldLst & vbNewLine & tmpOutDatLst
                If OutputType = "LOCAL_FILE" Then
                    Print #2, "CMD:" & tmpActCmd
                    Print #2, "TBL:" & tmpTblNam
                    Print #2, "KEY:" & tmpSqlKey
                    Print #2, "FLD:" & tmpOutFldLst
                    Print #2, "DAT:" & tmpOutDatLst
                    Print #2, "----------------------------------------------"
                End If
                RetVal = UfisServer.CallCeda(ResultBuffer, tmpActCmd, tmpTblNam, tmpOutFldLst, tmpOutDatLst, tmpSqlKey, "", 0, False, False)
                ActRem = "DONE"
            End If
        End If
            
        If ActRem <> "" Then
            For CurLine = FirstLine To LastLine
                FileData(CurFdIdx).SetColumnValue CurLine, 0, ActRem
            Next
            FltCnt = FltCnt - 1
        End If
   
    End If
    WarnCnt.Caption = FltCnt
    WarnCnt.Refresh
End Sub

Private Function CheckViaInformation(FirstLine As Long, LastLine As Long, ActViaList As String, FltAdid As String)
Dim Result As String
Dim CurLine As Long
Dim PrvLine As Long
Dim FldColIdx As Long
Dim tmpVal As String
Dim ViaData As String
Dim NewViaList As String

'Extract of 'Flight' Documentation aftdata.doc
'Field VIAL
'Position  2.Via  Bedeutung   Inhalt
'1,      121,     ... Flughafen wird auf den Anzeigen dargestellt Blank, 1 oder 2
'2-4,    122-124, *** 3-Lettercode des Flughafens                 FRA, MUC, ...
'5-8,    125-128, ... 4-Lettercode des Flughafens                 EDDF, EDDM, ...
'9-22,   129-142, *** Geplante Ankunftzeit am Flughafen (STOA)    19971010120000
'23-36,  143-156, ... Erwartete Ankunftzeit am Flughafen (ETOA)   19971010120000
'37-50,  157-170, ... Landezeit am Flughafen (LAND)               19971010120000
'51-64,  171-184, ... onBlock-Zeit am Flughafen (ONBL)            19971010120000
'65-78,  185-198, *** Geplante Abflugzeit am Flughafen (STOD)     19971010123000
'79-92,  199-212, ... Erwartete Abflugzeit am Flughafen (ETOD)    19971010123000
'93-106, 213-226, ... offBlock-Zeit am Flughafen (OFBL)           19971010123000
'107-120,227-240, ... Startzeit am Flughafen (AIRB)               19971010123000

'the Import Data file contains only the *** marked information
'we must take care not to overwrite already existing data of the other parts
'so it is necessary to read VIAL from the database and mix it with the imported data
'than first we can compare old/new and decide not to update if it is equal
    
    Result = ""
    If LastLine > FirstLine Then
        For CurLine = FirstLine + 1 To LastLine
            FldColIdx = Val(GetFieldValue("DES3", ExtFldLst, AftFldLst)) + 2
            If FldColIdx > 2 Then
                tmpVal = FileData(CurFdIdx).GetColumnValue(CurLine - 1, FldColIdx)
                ViaData = StripViaFromList(tmpVal, ActViaList)
                'here we have a valid Via record for this airport.
                'the string might be empty, so we patch it
                Mid(ViaData, 2, 3) = tmpVal
            End If
            FldColIdx = Val(GetFieldValue("STOA", ExtFldLst, AftFldLst)) + 2
            If FldColIdx > 2 Then
                tmpVal = FileData(CurFdIdx).GetColumnValue(CurLine - 1, FldColIdx) & "00"
                Mid(ViaData, 9, 14) = tmpVal
            End If
            FldColIdx = Val(GetFieldValue("STOD", ExtFldLst, AftFldLst)) + 2
            If FldColIdx > 2 Then
                tmpVal = FileData(CurFdIdx).GetColumnValue(CurLine, FldColIdx) & "00"
                Mid(ViaData, 65, 14) = tmpVal
            End If
            Result = Result & ViaData
        Next
    Else
        'No vias, so return empty VIAL
        Result = ""
    End If
    If (Result <> "") And (UseVialFlag = "YES") Then
        Mid(Result, 1) = "1"
    End If
    CheckViaInformation = RTrim(Result)
End Function
Private Function StripViaFromList(Apc3Code As String, ActViaList As String)
Dim Result As String
Dim ilLen As Integer
Dim i As Integer
    Result = ""
    ilLen = Len(ActViaList)
    For i = 1 To ilLen Step 120
        If Mid(ActViaList, i + 1, 3) = Apc3Code Then
            Result = Mid(ActViaList, i, 120)
            Exit For
        End If
    Next
    Result = Left(Result & Space(120), 120)
    StripViaFromList = Result
End Function

Private Sub chkInline_Click()
    If chkInline.Value = 1 Then
        chkEdit.Value = 0
        FileData(CurFdIdx).EnableInlineEdit True
        chkInline.BackColor = LightestGreen
    Else
        FileData(CurFdIdx).EnableInlineEdit False
        chkInline.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkLoad_Click()
    If Not RestoreAction Then
        If Not TotalReset Then
            MySetUp.chkLoad.Value = chkLoad.Value
            If chkLoad.Value = 1 Then
                If chkExpand.Value = 0 Then LoadFileData "(For Load)"
            Else
                ResetMain 0, False
                chkLoad.Enabled = True
                MySetUp.chkLoad.Enabled = True
            End If
        End If
    End If
End Sub

Public Sub ResetMain(SetValue As Integer, SetEnable As Boolean)
    Dim i As Integer
    Dim tmpData As String
    Dim tmpItmDat As String
    If Not RestoreAction Then
        IsResetMainScreen = True
        TotalReset = True
        
        InitGrids True
        
        chkAppl(0).BackColor = vbButtonFace
        chkAppl(0).Value = SetValue
        chkAppl(0).Enabled = SetEnable
        
        chkAppl(1).BackColor = vbButtonFace
        chkAppl(1).Value = SetValue
        chkAppl(1).Enabled = SetEnable
        
        chkImport.BackColor = vbButtonFace
        chkImport.Value = SetValue
        chkImport.Enabled = SetEnable
        
        chkAction.BackColor = vbButtonFace
        chkAction.Value = SetValue
        chkAction.Enabled = SetEnable
        
        chkExtract.BackColor = vbButtonFace
        chkExtract.Value = SetValue
        chkExtract.Enabled = SetEnable
        
        chkIdentify.BackColor = vbButtonFace
        chkIdentify.Value = SetValue
        chkIdentify.Enabled = SetEnable
        
        chkSort.BackColor = vbButtonFace
        chkSort.Value = SetValue
        chkSort.Enabled = SetEnable
        
        chkShrink.BackColor = vbButtonFace
        chkShrink.Value = SetValue
        chkShrink.Enabled = SetEnable
        
        chkClear.BackColor = vbButtonFace
        chkClear.Value = SetValue
        chkClear.Enabled = SetEnable
        
        chkDispo.BackColor = vbButtonFace
        chkDispo.Value = SetValue
        chkDispo.Enabled = SetEnable
        If chkDispo.Tag = "" Then chkDispo.Enabled = False
        
        chkInline.BackColor = vbButtonFace
        chkInline.Value = SetValue
        chkInline.Enabled = SetEnable
        
        chkEdit.BackColor = vbButtonFace
        chkEdit.Value = SetValue
        chkEdit.Enabled = SetEnable
        
        chkCheck.BackColor = vbButtonFace
        chkCheck.Value = SetValue
        chkCheck.Enabled = SetEnable
        
        chkDontClear.BackColor = vbButtonFace
        chkDontClear = SetValue
        chkDontClear.Enabled = SetEnable
        
        chkExpand.BackColor = vbButtonFace
        chkExpand.Value = SetValue
        chkExpand.Enabled = SetEnable
        
        chkSplit.BackColor = vbButtonFace
        chkSplit.Value = SetValue
        chkSplit.Enabled = True
        'MySetUp.chkSplit.BackColor = vbButtonFace
        'MySetUp.chkSplit.Value = SetValue
        
        chkFilter.BackColor = vbButtonFace
        chkFilter.Value = SetValue
        chkFilter.Enabled = True
        'MySetUp.chkFilter.BackColor = vbButtonFace
        'MySetUp.chkFilter.Value = SetValue
        
        chkLoad.BackColor = vbButtonFace
        chkLoad.Value = SetValue
        chkLoad.Enabled = True
        
        chkRequest.BackColor = vbButtonFace
        chkRequest.Value = SetValue
        chkRequest.Enabled = SetEnable
        
        'MySetUp.chkLoad.BackColor = vbButtonFace
        'MySetUp.chkLoad.Value = SetValue
        
        Select Case PreCheckDoubles
            Case "YES", "AUTO"
                chkAppl(2).BackColor = vbButtonFace
                chkAppl(2).Value = SetValue
                chkAppl(2).Enabled = SetEnable
            Case Else
                chkAppl(2).BackColor = vbButtonFace
                chkAppl(2).Value = 0
                chkAppl(2).Enabled = False
        End Select
        
        For i = 0 To 3
            ActSeason(i).Caption = ""
            ActSeason(i).Tag = ""
            ActSeason(i).ToolTipText = ""
            ActSeason(i).ForeColor = vbBlack
            ActSeason(i).BackColor = LightGrey
        Next
            
        If SetEnable = True Then
            MySetUp.SetAutoClearLimits myIniSection
            If Not UpdateMode Then MySetUp.chkWork(1).Enabled = True
        End If
        
        chkTimes(0).Tag = ""
        chkTimes(1).Tag = ""
        chkTimes(0).Value = 0
        chkTimes(1).Value = 0
        chkTimes(0).Enabled = False
        chkTimes(1).Enabled = False
        
        GetDefaultSeason True
        
        Refresh
        
        TotalReset = False
        IsResetMainScreen = False
    End If
End Sub

Public Sub ReleaseMain(SetValue As Integer, SetEnable As Boolean)
    Dim i As Integer
    Dim tmpData As String
    Dim tmpItmDat As String
    If Not RestoreAction Then
        TotalReset = True
        
        chkAppl(0).Enabled = SetEnable
        chkAppl(1).Enabled = SetEnable
        chkImport.Enabled = SetEnable
        chkAction.Enabled = SetEnable
        chkExtract.Enabled = SetEnable
        chkIdentify.Enabled = SetEnable
        chkSort.Enabled = SetEnable
        chkShrink.Enabled = SetEnable
        chkClear.Enabled = SetEnable
        If chkDispo.Tag = "" Then chkDispo.Enabled = False
        chkInline.Enabled = SetEnable
        chkEdit.Enabled = SetEnable
        chkCheck.Enabled = SetEnable
        chkDontClear.Enabled = SetEnable
        chkSplit.Enabled = True
        chkFilter.Enabled = True
        chkLoad.Enabled = True
        chkRequest.Enabled = SetEnable
        Me.Refresh
        
        TotalReset = False
    End If
End Sub

Private Sub ClickButtonChain(myButton As Integer)
    Dim SetValue As Integer
    If Not RestoreAction Then
        'If IsDoubleGridView Then chkGrid(0).Value = 1
        SetValue = 1
        If myButton > 2 Then chkLoad.Value = SetValue: Refresh
        If myButton > 3 Then chkFilter.Value = SetValue: Refresh
        If myButton > 4 Then chkSplit.Value = SetValue: Refresh
        If myButton > 5 Then chkExpand.Value = SetValue: Refresh
        If myButton > 6 Then chkShrink.Value = SetValue: Refresh
        If myButton > 7 Then chkCheck.Value = SetValue: Refresh
        If myButton > 8 Then chkIdentify.Value = SetValue: Refresh
        If myButton > 9 Then chkAction.Value = SetValue: Refresh
        If myButton > 10 Then chkImport.Value = SetValue: Refresh
    End If
End Sub

Private Sub LoadFileData(tmpCaption As String)
    LoadImportFile tmpCaption
End Sub
Private Sub LoadImportFile(tmpCaption As String)
    Dim CurFileName As String
    Dim FullFileName As String
    Dim InpLine As String
    Dim wrapLine As String
    Dim SetUpAnsw As String
    Dim SetUpCmd As String
    Dim pclText As String
    Dim ErrMsg As String
    Dim tmpData As String
    Dim tmpFlag As String
    Dim FileCount As Integer
    Dim DoIt As Boolean
    Dim Proceed As Boolean
    Dim ReadLines As Boolean
    Dim NxtFile As Integer
    Dim LoopLinCnt As Long
    Dim ActLine As Long
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim AutoChkCol As Long
    Dim AutoChkDat As String
    Dim SkippedLines As Integer
    Dim i As Integer
    Dim IsFirstLine As Boolean
    Dim StartTime
    On Error GoTo ErrorHandle
    If (Not DoubleGridInit) Then
        AutoUpdateMode = False
        CheckAutoUpdateMode = False
        If InStr(CheckFilePeriod, "YES") > 0 Then
            AutoChkCol = 5
            AutoChkDat = "I"
            CheckAutoUpdateMode = True
        End If
        FileCount = GetFileAndPath(chkRequest.Tag, myLoadName, myLoadPath)
    End If
    MainDialog.MousePointer = 11
    If (Not DoubleGridInit) Then
        If DataSystemType = "SCORE" Then
            FullFileName = myLoadPath & "\" & myLoadName
            DbfReader.PreCheckAnyFile FullFileName
            If AddOnDataFound And (Not AddOnDataMerge) Then ModifyTabLayout 1
        End If
    End If
    InitGrids True
    FileData(CurFdIdx).ShowVertScroller False
    FileData(CurFdIdx).ShowHorzScroller False
    Me.Refresh
    
    
    ErrCnt.Caption = ""
    WarnCnt.Caption = ""
    AllLinCnt = 0
    DatLinCnt = 0
    DbfReaderIsChecked = False
    If (Not DoubleGridInit) Then
        MySetUp.DetermineFileType "SESSION"
        If ImportActionType = "AODB" Then
            Unload DbfReader
            DbfReader.ReadAodbData
            DbfReader.Show
            DbfReader.Refresh
            DoEvents
            MaxLine = DbfReader.DataTab.GetLineCount - 1
            IsFirstLine = True
            For CurLine = 0 To MaxLine
                If CurLine Mod 50 = 0 Then
                    DbfReader.DataTab.OnVScrollTo CurLine
                    DbfReader.DataTab.Refresh
                    DoEvents
                End If
                InpLine = DbfReader.DataTab.GetColumnValue(CurLine, 1)
                If InsertDataLine(InpLine) Then
                    LoopLinCnt = LoopLinCnt + 1
                    If CheckAutoUpdateMode Then
                        ActLine = FileData(CurFdIdx).GetLineCount - 1
                        If FileData(CurFdIdx).GetColumnValue(ActLine, AutoChkCol) = AutoChkDat Then
                            CheckAutoUpdateMode = False
                        End If
                    End If
                    If DatLinCnt > 0 Then
                        If DatLinCnt Mod 50 = 0 Then
                            FileData(CurFdIdx).OnVScrollTo DatLinCnt - 50
                            FileData(CurFdIdx).Refresh
                        End If
                    End If
                End If
            Next
            If MaxLine >= 0 Then
                FileData(CurFdIdx).Sort 3, True, True
                DoIt = True
                myLoadName = ImportActionType
                MySetUp.CurFileInfo(5).Caption = "Update"
                DbfReader.DataTab.OnVScrollTo 0
                DoEvents
                FileCount = 0
            End If
        End If
        For NxtFile = 1 To FileCount
            Unload DbfReader
            Me.Refresh
            DoEvents
            RoutWithTimes = False
            MySetUp.DetermineFileType "RESET," & Trim(Str(GrpLinCnt)) & "," & Chr(30) & FullFileName
            CurFileName = GetItem(myLoadName, NxtFile, " ")
            FullFileName = myLoadPath & "\" & CurFileName
            StatusBar.Panels(1).Text = AllLinCnt
            StatusBar.Panels(2).Text = ""
            StatusBar.Panels(7).Text = "Reading file data " & tmpCaption & " - " & CurFileName & " ..."
            StatusBar.Refresh
            DefaultTtyp = DetermineTtypFromFile(CurFileName)
            GrpLinCnt = 0
            MySetUp.DetermineFileType "INIT," & Trim(Str(GrpLinCnt)) & "," & Chr(30) & FullFileName
            Select Case DataSystemType
                Case "RMSIMP"
                    ReadLines = False
                    RoutWithTimes = True
                    ReadRmsImpFile FullFileName
                    DoIt = True
                Case "CSKED"
                    ReadLines = False
                    RoutWithTimes = True
                    ReadCskedImpFile FullFileName
                    DoIt = True
                Case "SSIM45"
                    ReadLines = False
                    RoutWithTimes = True
                    ReadSsimImpFile FullFileName
                    DoIt = True
                Case Else
                    ReadLines = True
            End Select
            FileData(CurFdIdx).Refresh
            If ReadLines Then
                If (UseFileReader) Or (UCase(Right(FullFileName, 4)) = ".DBF") Then
                    MySetUp.chkReadDbf.Enabled = True
                    DoIt = True
                    If UCase(Right(FullFileName, 4)) = ".DBF" Then
                        DbfReader.OpenDbfFile FullFileName, Me, True
                    Else
                        DbfReader.OpenAnyFile FullFileName, Me, True
                    End If
                    Me.Refresh
                    DoEvents
                    MaxLine = DbfReader.DataTab.GetLineCount - 1
                    IsFirstLine = True
                    For CurLine = 0 To MaxLine
                        If CurLine Mod 50 = 0 Then
                            DbfReader.DataTab.OnVScrollTo CurLine
                            DbfReader.DataTab.Refresh
                            DoEvents
                        End If
                        InpLine = DbfReader.DataTab.GetLineValues(CurLine)
                        Proceed = True
                        Select Case DataSystemType
                            Case "RMSLNK"
                                Proceed = InsertRmsLinkLine(InpLine, IsFirstLine)
                                If Proceed Then LoopLinCnt = LoopLinCnt + 1
                            Case Else
                                If InsertDataLine(InpLine) = True Then
                                    LoopLinCnt = LoopLinCnt + 1
                                    If CheckAutoUpdateMode Then
                                        ActLine = FileData(CurFdIdx).GetLineCount - 1
                                        If FileData(CurFdIdx).GetColumnValue(ActLine, AutoChkCol) = AutoChkDat Then
                                            CheckAutoUpdateMode = False
                                        End If
                                    End If
                                End If
                        End Select
                        If Proceed Then
                            If IsFirstLine Then IsFirstLine = False
                            If (FileCount > 1) And (LoopLinCnt = 1) Then FileData(CurFdIdx).SetColumnValue DatLinCnt - 1, ExtFldCnt, CurFileName
                            If DatLinCnt > 0 Then
                                If DatLinCnt Mod 50 = 0 Then
                                    FileData(CurFdIdx).OnVScrollTo DatLinCnt - 50
                                    FileData(CurFdIdx).Refresh
                                End If
                            End If
                        End If
                    Next
                    DbfReader.Hide
                    Me.Refresh
                    DoEvents
                Else
                    MySetUp.chkReadDbf.Enabled = False
                    Open FullFileName For Input As #1
                        DoIt = True
                        LoopLinCnt = 0
                        IsFirstLine = True
                        While (Not EOF(1)) And (DoIt)
                            Line Input #1, InpLine
                            Proceed = True
                            Select Case DataSystemType
                                Case "RMSLNK"
                                    Proceed = InsertRmsLinkLine(InpLine, IsFirstLine)
                                    If Proceed Then LoopLinCnt = LoopLinCnt + 1
                                Case Else
                                    If InsertDataLine(InpLine) Then LoopLinCnt = LoopLinCnt + 1
                            End Select
                            If Proceed Then
                                If IsFirstLine Then IsFirstLine = False
                                If (FileCount > 1) And (LoopLinCnt = 1) Then FileData(CurFdIdx).SetColumnValue DatLinCnt - 1, ExtFldCnt, CurFileName
                                If DatLinCnt > 0 Then
                                    If DatLinCnt Mod 50 = 0 Then
                                        FileData(CurFdIdx).OnVScrollTo DatLinCnt - 50
                                        FileData(CurFdIdx).Refresh
                                    End If
                                End If
                            End If
                        Wend
                    Close #1
                End If
            End If
            StatusBar.Panels(2).Text = Str(LoopLinCnt)
            StatusBar.Refresh
        Next
    End If
    'Fields RMRK,CELL,SYST not to be synchronized. Thus using fake names.
    If HeadFieldList = "" Then HeadFieldList = MainFieldList
    If MainFieldList = "" Then MainFieldList = HeadFieldList
    LogicalMainFieldList = "LINE,IDX1,IDX2," & MainFieldList & ",RMRK,CELL,SYST,UKEY"
    LogicalHeadFieldList = "LINE,IDX1,IDX2," & HeadFieldList & ",MRKR,ELLC,TSYS,UKEY"
    FileData(0).LogicalFieldList = LogicalMainFieldList
    FileData(1).LogicalFieldList = LogicalHeadFieldList
    
    If (Not DoubleGridInit) Then
        DbfReader.Hide
        Me.Refresh
        DoEvents
        If Not DbfReaderIsChecked Then MainDoIt = DbfReader.CheckErrorsModal(Me)
        Me.Refresh
        DoEvents
    Else
        GetRecordsFromGrid2
        AllLinCnt = FileData(CurFdIdx).GetLineCount
        DatLinCnt = AllLinCnt
        StatusBar.Panels(1).Text = CStr(AllLinCnt)
        StatusBar.Panels(2).Text = CStr(DatLinCnt)
        MainDoIt = True
        DoIt = True
    End If
    FileData(CurFdIdx).Refresh
    If MainDoIt Then
        MainDoIt = DoIt
        If (Not DoubleGridInit) Then
            SetUpAnsw = MySetUp.DetermineFileType("FINISH")
            FileData(CurFdIdx).Refresh
            If SetUpAnsw <> "OK" Then
                chkAppl(4).Value = 1
                If MyMsgBox.CallAskUser(0, 0, 0, "File Data Control", SetUpAnsw, "stop", "", UserAnswer) >= 0 Then DoNothing
            End If
        Else
            SetUpAnsw = "OK"
        End If
        If (UpdateMode) And (DataSystemType = "FHK") Then
            chkSort.Top = chkShrink.Top
            chkSort.Visible = True
            chkShrink.Visible = False
            MySetUp.chkWork(1).Value = 0
        Else
            chkShrink.Visible = True
            chkSort.Visible = False
        End If
        StatusBar.Panels(1).Text = AllLinCnt
        StatusBar.Panels(2).Text = DatLinCnt
        StatusBar.Panels(7).Text = "Ready"
        FileData(CurFdIdx).OnVScrollTo 0
        FileData(CurFdIdx).ShowVertScroller True
        FileData(CurFdIdx).ShowHorzScroller True
        FileData(CurFdIdx).Refresh
        Me.Refresh
        If DoIt And chkExpand.Enabled = True And chkExpand.Value = 1 Then
            GetTimeChains
            CheckVpfrVptoFrame 0, -1
            ClearDoubleVias
            ClearFieldsByFieldValue
            PrepareOverNightShrink
            CheckAutoReplace
            ToggleDateFormat True
        End If
        Me.Caption = chkImpTyp(PushedImpButton).Caption & " " & MainTitle & " (" & MySetUp.CurFileInfo(5).Caption & " Mode) " & GetItem(MySetUp.MyFileInfo.Caption, 1, " ")
        FileData(CurFdIdx).AutoSizeByHeader = True
        FileData(CurFdIdx).AutoSizeColumns
        FileData(CurFdIdx).Refresh
        Me.Refresh
        If (DoIt) And (TranslateIcao <> "") Then TranslateIcaoCodes -1, ""
        If (DoIt) And (TranslateApcList <> "") Then TranslateAirportCodes -1, ""
        If (DoIt) Then CheckMandatoryColumns
        MainDialog.MousePointer = 0
        ErrMsg = ""
        tmpData = ActSeason(1).Tag
        tmpFlag = SeasonData.GetValidSeason(tmpData)
        If tmpFlag = "" Then
            pclText = ActSeason(1).Caption
            ErrMsg = ErrMsg & pclText & " (" & tmpData & ")" & vbNewLine
        End If
        tmpData = ActSeason(0).Tag
        tmpFlag = GetItem(tmpData, 1, ",")
        If tmpFlag = "ERROR" Then
            tmpData = GetItem(tmpData, 2, ",")
            pclText = DecodeSsimDayFormat(tmpData, "CEDA", "SSIM2")
            ErrMsg = ErrMsg & pclText & " (" & tmpData & ")" & vbNewLine
        End If
        tmpData = ActSeason(2).Tag
        tmpFlag = SeasonData.GetValidSeason(tmpData)
        If tmpFlag = "" Then
            pclText = ActSeason(2).Caption
            ErrMsg = ErrMsg & pclText & " (" & tmpData & ")" & vbNewLine
        End If
        'tmpData = ActSeason(3).Tag
        'tmpFlag = SeasonData.GetValidSeason(tmpData)
        'If tmpFlag = "" Then
        '    pclText = ActSeason(3).Caption
        '    ErrMsg = ErrMsg & pclText & " (" & tmpData & ")" & vbNewLine
        'End If
        If ErrMsg <> "" Then
            pclText = "Can't determine the related season" & vbNewLine
            pclText = pclText & "of the following dates:" & vbNewLine & vbNewLine
            ErrMsg = pclText & ErrMsg
            If MyMsgBox.CallAskUser(0, 0, 0, "Seasonal Period Check", ErrMsg, "infomsg", "", UserAnswer) > 0 Then DoNothing
        End If
        StatusBar.Panels(7).Text = ""
        ResetErrorCounter
        Me.Refresh
    End If
    Me.Refresh
    MainDialog.MousePointer = 0
Exit Sub
    
ErrorHandle:
    StatusBar.Panels(7).Text = ""
    MainDialog.MousePointer = 0
    pclText = Err.Description & vbNewLine & FullFileName
    i = Err.Number
    Select Case Err.Number   ' Evaluate error number.
        Case 53   ' "File does not exist" error.
            MyMsgBox.InfoApi 0, " " & vbNewLine & " ", "Hint"
            If MyMsgBox.CallAskUser(0, 0, 0, "File Control", pclText, "stop", "", UserAnswer) >= 0 Then DoNothing
            'Exit Sub
        Case 55   ' "File already open" error.
            Close #1   ' Close open file.
            Resume 'try again
        Case 76     'Bad File Name or Number
            MyMsgBox.InfoApi 0, " " & vbNewLine & " ", "Hint"
            If MyMsgBox.CallAskUser(0, 0, 0, "File Control", pclText, "stop", "", UserAnswer) >= 0 Then DoNothing
            Exit Sub
        Case Else
            ' Handle other situations here...
            'MsgBox CStr(Err.Number) & vbNewLine & pclText
            Resume Next
        MainDialog.MousePointer = 0
    End Select
    MySetUp.DetermineFileType "INIT"
End Sub
Private Sub GetRecordsFromGrid2()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim NewRec As String
    Dim CurName As String
    Dim CurData As String
    Dim ItmCnt As Integer
    Dim CurItm As Integer
    Dim MaxCol As Integer
    Dim i As Integer
    FileData(0).ResetContent
    MaxCol = CInt(FileData(0).GetColumnCount)
    ItmCnt = CInt(ItemCount(LogicalMainFieldList, ","))
    MaxLine = FileData(1).GetLineCount - 1
    For CurLine = 0 To MaxLine
        NewRec = ""
        For CurItm = 1 To ItmCnt
            CurName = GetItem(LogicalMainFieldList, CurItm, ",")
            Select Case CurName
                Case "IDX1"
                    CurData = ""
                Case "IDX2"
                    CurData = ""
                Case "ORG4"
                    CurData = ""
                Case "VSA4"
                    CurData = ""
                Case "VSD4"
                    CurData = ""
                Case "DES4"
                    CurData = ""
                Case Else
                CurData = FileData(1).GetFieldValue(CurLine, CurName)
            End Select
            NewRec = NewRec & CurData & ","
        Next
        For i = CurItm To MaxCol - 1
            'CurData = CStr(i)
            CurData = ""
            NewRec = NewRec & CurData & ","
        Next
        'CurData = CStr(i)
        CurData = ""
        NewRec = NewRec & CurData
        FileData(0).InsertTextLine NewRec, False
    Next
    FileData(0).Refresh
    DoEvents
End Sub
Private Function DetermineTtypFromFile(CurFileName As String) As String
    Dim CurName As String
    Dim UseTtypValue As String
    Dim CurItemNo As Long
    Dim CurItemGrp As String
    Dim CurPattern As String
    UseTtypValue = ""
    If AutoFileTtyp <> "" Then
        CurName = UCase(CurFileName)
        CurItemNo = 0
        CurItemGrp = UCase(GetRealItem(AutoFileTtyp, CurItemNo, "|"))
        While (UseTtypValue = "") And (CurItemGrp <> "")
            CurPattern = GetRealItem(CurItemGrp, 0, ",")
            If InStr(CurName, CurPattern) > 0 Then UseTtypValue = GetRealItem(CurItemGrp, 1, ",")
            CurItemNo = CurItemNo + 1
            CurItemGrp = UCase(GetRealItem(AutoFileTtyp, CurItemNo, "|"))
        Wend
        If (UseTtypValue = "") And (CurPattern <> "") Then
            Select Case CurPattern
                Case ".CHOOSE."
                    '
                Case ".ASK."
                    UseTtypValue = AskForBasicData(1, "2700,1600", "Flight Natures (Traffic Types)", "NAT,Meaning", "Select Natures of File '" & CurFileName & "'")
                Case Else
                    UseTtypValue = CurPattern
            End Select
        End If
    End If
    DetermineTtypFromFile = UseTtypValue
End Function
Private Sub ReadCskedImpFile(FullFileName As String)
    Dim CurLine As Long
    MySetUp.chkReadDbf.Enabled = True
    DbfReader.OpenAnyFile FullFileName, Me, True
    AllLinCnt = 0
    GrpLinCnt = 0
    DatLinCnt = 0
    ArrFltCnt = 0
    DepFltCnt = 0
    If chkExpand.Value = 1 Then
        ShowCskedImpExpand
    ElseIf chkSplit.Value = 1 Then
        ShowCskedImpSplit
    ElseIf chkFilter.Value = 1 Then
        ShowCskedImpFilter
    ElseIf chkLoad.Value = 1 Then
        ShowCskedImpLoad
    End If
    FileData(CurFdIdx).AutoSizeByHeader = True
    FileData(CurFdIdx).AutoSizeColumns
    FileData(CurFdIdx).RedrawTab
    AllLinCnt = FileData(CurFdIdx).GetLineCount
    DatLinCnt = FileData(CurFdIdx).GetLineCount
    'DbfReader.Hide
End Sub
Private Sub ShowCskedImpExpand()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim FirstLineNo As Long
    Dim DataLineNo As Long
    Dim ItmCnt As Integer
    Dim tmpData As String
    Dim tmpLine As String
    Dim tmpBuff As String
    Dim FirstChar As String
    Screen.MousePointer = 11
    DbfReader.PreCheckCskedImpFile
    DbfReader.Hide
    Screen.MousePointer = 0
    MainDoIt = DbfReader.CheckErrorsModal(Me)
    If MainDoIt Then
        Screen.MousePointer = 11
        ShrinkCskedImpFile True
        Screen.MousePointer = 0
    End If
    DbfReaderIsChecked = True
End Sub
Private Sub ShowCskedImpLoad()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim FirstLineNo As Long
    Dim DataLineNo As Long
    Dim ItmCnt As Integer
    Dim tmpData As String
    Dim tmpLine As String
    Dim tmpBuff As String
    FirstLineNo = FileData(CurFdIdx).GetLineCount
    DataLineNo = FirstLineNo
    MaxLine = DbfReader.DataTab.GetLineCount - 1
    tmpBuff = ""
    For CurLine = 0 To MaxLine
        tmpData = DbfReader.DataTab.GetColumnValue(CurLine, 0)
        DataLineNo = DataLineNo + 1
        tmpLine = CStr(DataLineNo)
        tmpBuff = tmpBuff & tmpLine & "," & tmpData & vbLf
        If DataLineNo Mod 200 = 0 Then
            FileData(CurFdIdx).InsertBuffer tmpBuff, vbLf
            DbfReader.DataTab.OnVScrollTo CurLine
            DbfReader.DataTab.Refresh
            FileData(CurFdIdx).OnVScrollTo DataLineNo
            FileData(CurFdIdx).Refresh
            tmpBuff = ""
        End If
    Next
    FileData(CurFdIdx).InsertBuffer tmpBuff, vbLf
    FileData(CurFdIdx).OnVScrollTo 0
    DbfReader.DataTab.OnVScrollTo 0
    DbfReader.DataTab.RedrawTab
    FileData(CurFdIdx).RedrawTab
End Sub

Private Sub ShowCskedImpFilter()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim FirstLineNo As Long
    Dim DataLineNo As Long
    Dim ItmCnt As Integer
    Dim tmpData As String
    Dim tmpLine As String
    Dim tmpBuff As String
    FirstLineNo = FileData(CurFdIdx).GetLineCount
    DataLineNo = FirstLineNo
    MaxLine = DbfReader.DataTab.GetLineCount - 1
    tmpBuff = ""
    For CurLine = 0 To MaxLine
        tmpData = DbfReader.DataTab.GetColumnValue(CurLine, 0)
        DataLineNo = DataLineNo + 1
        tmpLine = CStr(DataLineNo)
        tmpBuff = tmpBuff & tmpLine & "," & tmpData & vbLf
        If DataLineNo Mod 200 = 0 Then
            FileData(CurFdIdx).InsertBuffer tmpBuff, vbLf
            DbfReader.DataTab.OnVScrollTo CurLine
            DbfReader.DataTab.Refresh
            FileData(CurFdIdx).OnVScrollTo DataLineNo
            FileData(CurFdIdx).Refresh
            tmpBuff = ""
        End If
    Next
    FileData(CurFdIdx).InsertBuffer tmpBuff, vbLf
    tmpData = FileData(CurFdIdx).GetColumnValue(0, 1)
    MySetUp.CurFileInfo(1).Caption = tmpData
    If Left(tmpData, 1) = "0" Then
        FileData(CurFdIdx).SetLineColor 0, vbBlack, vbYellow
    Else
        FileData(CurFdIdx).SetLineColor 0, vbWhite, vbRed
    End If
    MaxLine = FileData(CurFdIdx).GetLineCount - 1
    tmpData = FileData(CurFdIdx).GetColumnValue(MaxLine, 1)
    MySetUp.CurFileInfo(2).Caption = tmpData
    If Left(tmpData, 1) = "9" Then
        FileData(CurFdIdx).SetLineColor MaxLine, vbBlack, vbGreen
    Else
        FileData(CurFdIdx).SetLineColor MaxLine, vbWhite, vbRed
    End If
    DbfReader.DataTab.OnVScrollTo 0
    DbfReader.DataTab.RedrawTab
    FileData(CurFdIdx).OnVScrollTo 0
    FileData(CurFdIdx).RedrawTab
End Sub

Private Sub ShowCskedImpSplit()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim FirstLineNo As Long
    Dim DataLineNo As Long
    Dim ItmCnt As Integer
    Dim tmpData As String
    Dim tmpLine As String
    Dim tmpBuff As String
    Dim DoIt As Boolean
    DbfReader.PreCheckCskedImpFile
    DbfReader.Hide
    DoIt = DbfReader.CheckErrorsModal(Me)
    DbfReaderIsChecked = True
    If DoIt Then
        ShrinkCskedImpFile False
        FirstLineNo = FileData(CurFdIdx).GetLineCount
        DataLineNo = FirstLineNo
        MaxLine = DbfReader.DataTab.GetLineCount - 1
        tmpBuff = ""
        For CurLine = 0 To MaxLine
            tmpData = DbfReader.DataTab.GetColumnValue(CurLine, 0)
            If Left(tmpData, 1) = "1" Then
                DataLineNo = DataLineNo + 1
                tmpLine = CStr(DataLineNo)
                tmpBuff = tmpBuff & tmpLine & ",,," & tmpData & vbLf
                If DataLineNo Mod 200 = 0 Then
                    FileData(CurFdIdx).InsertBuffer tmpBuff, vbLf
                    DbfReader.DataTab.OnVScrollTo CurLine
                    DbfReader.DataTab.Refresh
                    FileData(CurFdIdx).OnVScrollTo DataLineNo
                    FileData(CurFdIdx).Refresh
                    tmpBuff = ""
                End If
            End If
        Next
        FileData(CurFdIdx).InsertBuffer tmpBuff, vbLf
        DbfReader.DataTab.OnVScrollTo 0
        DbfReader.DataTab.RedrawTab
        FileData(CurFdIdx).OnVScrollTo 0
        FileData(CurFdIdx).RedrawTab
    End If
End Sub

'===========================================================================
Private Sub ReadSsimImpFile(FullFileName As String)
    Dim CurLine As Long
    MySetUp.chkReadDbf.Enabled = True
    DbfReader.OpenSsimFile FullFileName, Me, True
    AllLinCnt = 0
    GrpLinCnt = 0
    DatLinCnt = 0
    ArrFltCnt = 0
    DepFltCnt = 0
    If chkExpand.Value = 1 Then
        ShowSsimImpExpand
    ElseIf chkSplit.Value = 1 Then
        'ShowRmsImpSplit
    ElseIf chkFilter.Value = 1 Then
        'ShowRmsImpFilter
    ElseIf chkLoad.Value = 1 Then
        'ShowRmsImpLoad
    End If
    FileData(CurFdIdx).AutoSizeByHeader = True
    FileData(CurFdIdx).AutoSizeColumns
    FileData(CurFdIdx).RedrawTab
    AllLinCnt = FileData(CurFdIdx).GetLineCount
    DatLinCnt = FileData(CurFdIdx).GetLineCount
End Sub
Private Sub ShowSsimImpExpand()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim FirstLineNo As Long
    Dim DataLineNo As Long
    Dim ItmCnt As Integer
    Dim tmpData As String
    Dim tmpLine As String
    Dim tmpBuff As String
    Dim FirstChar As String
    Screen.MousePointer = 11
    DbfReader.PreCheckSsimImpFile
    DbfReader.Hide
    Me.Refresh
    Screen.MousePointer = 0
    MainDoIt = DbfReader.CheckErrorsModal(Me)
    If MainDoIt Then
        Screen.MousePointer = 11
        ShrinkSsimImpFile True
        Screen.MousePointer = 0
    End If
    DbfReaderIsChecked = True
End Sub

Private Sub ShrinkSsimImpFile(ForImport As Boolean)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim NewLine As Long
    Dim CurTextColor As Long
    Dim CurBackColor As Long
    Dim LineTag As String
    Dim FltRec As String
    Dim ItemName As String
    Dim ItemData As String
    Dim itm As Integer
    
    SSIM7FieldGroups = GetIniEntry(myIniFullName, myIniSection, "", "SSIM_GROUPS", "DAYS,AFLT,DFLT,ACTP,ROUT,APDX")
    Screen.MousePointer = 11
    FileData(CurFdIdx).ResetContent
    Me.Refresh
    DbfReader.Show
    DbfReader.chkWork(7).Value = 1
    DbfReader.Refresh
    MaxLine = DbfReader.FlightData.GetLineCount - 1
    NewLine = 0
    For CurLine = 0 To MaxLine
        If (CurLine > 0) And (CurLine < MaxLine) Then
            If (CurLine Mod 200) = 0 Then
                DbfReader.FlightData.OnVScrollTo CurLine
                DbfReader.FlightData.Refresh
                DoEvents
            End If
        End If
        DbfReader.FlightData.GetLineColor CurLine, CurTextColor, CurBackColor
        Select Case CurBackColor
            Case DbfReader.lblImpCnt(13).BackColor    '(vbGreen)  Departure Flight
                NewLine = NewLine + 1
                LineTag = DbfReader.FlightData.GetLineTag(CurLine)
                FltRec = CStr(NewLine) & ",,,"  'Line Header
                itm = 0
                ItemName = "START"
                While ItemName <> ""
                    itm = itm + 1
                    ItemName = GetItem(SSIM7FieldGroups, itm, ",")
                    If ItemName <> "" Then
                        Select Case ItemName
                            Case "AFLT"
                                ItemData = ",,,"
                            Case "DFLT"
                                ItemData = GetItem(LineTag, 3, ";")
                            Case "DAYS"
                                ItemData = GetItem(LineTag, 2, ";")
                            Case "ACTP"
                                ItemData = GetItem(LineTag, 4, ";")
                            Case "ROUT"
                                ItemData = GetItem(LineTag, 5, ";")
                            Case "APDX"
                                ItemData = GetItem(LineTag, 6, ";")
                            Case Else
                                ItemData = "?"
                        End Select
                        FltRec = FltRec & ItemData & ","
                    End If
                Wend
                FltRec = FltRec & ",,," & DbfReader.FlightData.GetFieldValue(CurLine, "RSNO")
                FileData(CurFdIdx).InsertTextLine FltRec, False
            Case DbfReader.lblImpCnt(11).BackColor    '(vbYellow) Arrival Flight
                NewLine = NewLine + 1
                LineTag = DbfReader.FlightData.GetLineTag(CurLine)
                FltRec = CStr(NewLine) & ",,,"  'Line Header
                itm = 0
                ItemName = "START"
                While ItemName <> ""
                    itm = itm + 1
                    ItemName = GetItem(SSIM7FieldGroups, itm, ",")
                    If ItemName <> "" Then
                        Select Case ItemName
                            Case "AFLT"
                                ItemData = GetItem(LineTag, 3, ";")
                            Case "DFLT"
                                ItemData = ",,,,,"
                            Case "DAYS"
                                ItemData = GetItem(LineTag, 2, ";")
                            Case "ACTP"
                                ItemData = GetItem(LineTag, 4, ";")
                            Case "ROUT"
                                ItemData = GetItem(LineTag, 5, ";")
                            Case "APDX"
                                ItemData = GetItem(LineTag, 6, ";")
                            Case Else
                                ItemData = "?"
                        End Select
                        FltRec = FltRec & ItemData & ","
                    End If
                Wend
                FltRec = FltRec & ",,," & DbfReader.FlightData.GetFieldValue(CurLine, "RSNO")
                FileData(CurFdIdx).InsertTextLine FltRec, False
            Case Else
        End Select
        If (NewLine > 0) And (NewLine < MaxLine) And (NewLine Mod 200 = 0) Then
            FileData(CurFdIdx).OnVScrollTo NewLine
            FileData(CurFdIdx).Refresh
            DoEvents
        End If
    Next
    FileData(CurFdIdx).OnVScrollTo 0
    FileData(CurFdIdx).AutoSizeColumns
    FileData(CurFdIdx).Refresh
    DbfReader.FlightData.OnVScrollTo 0
    DbfReader.chkWork(7).Value = 0
    DbfReader.Hide
    Screen.MousePointer = 0
    Me.Refresh
    DoEvents
End Sub

'===========================================================================
Private Sub ReadRmsImpFile(FullFileName As String)
    Dim CurLine As Long
    MySetUp.chkReadDbf.Enabled = True
    DbfReader.OpenAnyFile FullFileName, Me, True
    AllLinCnt = 0
    GrpLinCnt = 0
    DatLinCnt = 0
    ArrFltCnt = 0
    DepFltCnt = 0
    If chkExpand.Value = 1 Then
        ShowRmsImpExpand
    ElseIf chkSplit.Value = 1 Then
        ShowRmsImpSplit
    ElseIf chkFilter.Value = 1 Then
        ShowRmsImpFilter
    ElseIf chkLoad.Value = 1 Then
        ShowRmsImpLoad
    End If
    FileData(CurFdIdx).AutoSizeByHeader = True
    FileData(CurFdIdx).AutoSizeColumns
    FileData(CurFdIdx).RedrawTab
    AllLinCnt = FileData(CurFdIdx).GetLineCount
    DatLinCnt = FileData(CurFdIdx).GetLineCount
    'DbfReader.Hide
End Sub
Private Sub ShowRmsImpLoad()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim FirstLineNo As Long
    Dim DataLineNo As Long
    Dim ItmCnt As Integer
    Dim tmpData As String
    Dim tmpLine As String
    FirstLineNo = FileData(CurFdIdx).GetLineCount
    DataLineNo = FirstLineNo
    MaxLine = DbfReader.DataTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpData = DbfReader.DataTab.GetColumnValue(CurLine, 0)
        tmpData = Replace(tmpData, ",", "|", 1, -1, vbBinaryCompare)
        DataLineNo = DataLineNo + 1
        tmpLine = Trim(Str(DataLineNo))
        tmpLine = Right("    " & tmpLine, 4)
        tmpData = tmpLine & "," & tmpData
        FileData(CurFdIdx).InsertTextLine tmpData, False
        If DataLineNo Mod 100 = 0 Then
            DbfReader.DataTab.OnVScrollTo CurLine
            DbfReader.DataTab.Refresh
            FileData(CurFdIdx).OnVScrollTo DataLineNo
            FileData(CurFdIdx).Refresh
        End If
    Next
    FileData(CurFdIdx).OnVScrollTo 0
    DbfReader.DataTab.OnVScrollTo 0
    DbfReader.DataTab.RedrawTab
    FileData(CurFdIdx).RedrawTab
End Sub
Private Sub ShowRmsImpFilter()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim FirstLineNo As Long
    Dim DataLineNo As Long
    Dim ItmCnt As Integer
    Dim tmpData As String
    Dim tmpLine As String
    FirstLineNo = FileData(CurFdIdx).GetLineCount
    DataLineNo = FirstLineNo
    MaxLine = DbfReader.DataTab.GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpData = DbfReader.DataTab.GetColumnValue(CurLine, 0)
        tmpData = Replace(tmpData, ",", "|", 1, -1, vbBinaryCompare)
        DataLineNo = DataLineNo + 1
        tmpLine = Trim(Str(DataLineNo))
        tmpLine = Right("    " & tmpLine, 4)
        tmpData = tmpLine & "," & tmpData
        FileData(CurFdIdx).InsertTextLine tmpData, False
        If DataLineNo Mod 100 = 0 Then
            DbfReader.DataTab.OnVScrollTo CurLine
            DbfReader.DataTab.Refresh
            FileData(CurFdIdx).OnVScrollTo DataLineNo
            FileData(CurFdIdx).Refresh
        End If
    Next
    FileData(CurFdIdx).OnVScrollTo 0
    DbfReader.DataTab.OnVScrollTo 0
    DbfReader.DataTab.RedrawTab
    tmpData = Trim(DbfReader.DataTab.GetColumnValue(0, 0))
    ItmCnt = ItemCount(tmpData, ",")
    If (ItmCnt <> 1) Or (tmpData = "") Then
        FileData(CurFdIdx).SetLineColor FirstLineNo, vbWhite, vbRed
    Else
        FileData(CurFdIdx).SetLineColor FirstLineNo, vbWhite, vbMagenta
    End If
    FileData(CurFdIdx).RedrawTab
End Sub

Private Sub ShowRmsImpSplit()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim FirstLineNo As Long
    Dim DataLineNo As Long
    Dim IsValid As Boolean
    Dim ItmCnt As Integer
    Dim tmpData As String
    Dim tmpLine As String
    Dim tmpFreq As String
    ShrinkRmsImpFile
    FirstLineNo = FileData(CurFdIdx).GetLineCount
    DataLineNo = FirstLineNo
    MaxLine = DbfReader.DataTab.GetLineCount - 1
    For CurLine = 1 To MaxLine
        tmpData = DbfReader.DataTab.GetColumnValue(CurLine, 0)
        tmpLine = Trim(Str(DataLineNo + 1))
        tmpLine = Right("    " & tmpLine, 4) & ",,"
        tmpData = tmpLine & "," & tmpData
        FileData(CurFdIdx).InsertTextLine tmpData, False
        tmpFreq = FormatFrequency(FileData(CurFdIdx).GetColumnValue(DataLineNo, 13), "123", IsValid)
        FileData(CurFdIdx).SetColumnValue DataLineNo, 13, tmpFreq
        DataLineNo = DataLineNo + 1
        
        If DataLineNo Mod 100 = 0 Then
            DbfReader.DataTab.OnVScrollTo CurLine
            DbfReader.DataTab.Refresh
            FileData(CurFdIdx).OnVScrollTo DataLineNo
            FileData(CurFdIdx).Refresh
        End If
    Next
    FileData(CurFdIdx).OnVScrollTo 0
    DbfReader.DataTab.OnVScrollTo 0
    DbfReader.DataTab.RedrawTab
    FileData(CurFdIdx).RedrawTab
End Sub
Private Sub ShowRmsImpExpand()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim DataLineNo As Long
    Dim IsValidDep As Boolean
    Dim IsValidArr As Boolean
    Dim ItmCnt As Integer
    Dim tmpData As String
    Dim tmpLine As String
    Dim tmpFreq As String
    Dim tmpVsno As String
    Dim tmpRout As String
    Dim NewRec As String
    Dim ErrNumber As Integer
    ShrinkRmsImpFile
    DataLineNo = FileData(CurFdIdx).GetLineCount
    MaxLine = DbfReader.DataTab.GetLineCount - 1
    For CurLine = 1 To MaxLine
        tmpData = DbfReader.DataTab.GetColumnValue(CurLine, 0)
        IsValidDep = BuildRmsImpLine(NewRec, tmpData, True, ErrNumber)
        'IsValidDep = False
        If IsValidDep Then
            tmpLine = Trim(Str(DataLineNo + 1))
            tmpLine = Right("    " & tmpLine, 4) & ",,"
            NewRec = tmpLine & "," & NewRec
            FileData(CurFdIdx).InsertTextLine NewRec, False
            tmpVsno = FileData(CurFdIdx).GetColumnValue(DataLineNo, 20)
            tmpRout = FileData(CurFdIdx).GetColumnValue(DataLineNo, 21)
            CheckNumberOfVias DataLineNo, 20, ExtFldCnt, tmpVsno, tmpRout
            Select Case ErrNumber
                Case -1
                    SetErrorText "11", DataLineNo, ExtFldCnt, "ALC?", True, 0
                Case -2
                    SetErrorText "12", DataLineNo, ExtFldCnt, "FLT?", True, 0
                Case -3
                    SetErrorText "3,22", DataLineNo, ExtFldCnt, "P.FROM?", True, 0
                Case -4
                    SetErrorText "4", DataLineNo, ExtFldCnt, "P.TO?", True, 0
                Case Else
            End Select
            DataLineNo = DataLineNo + 1
        End If
        IsValidArr = BuildRmsImpLine(NewRec, tmpData, False, ErrNumber)
        'IsValidArr = False
        If IsValidArr Then
            tmpLine = Trim(Str(DataLineNo + 1))
            tmpLine = Right("    " & tmpLine, 4) & ",,"
            NewRec = tmpLine & "," & NewRec
            FileData(CurFdIdx).InsertTextLine NewRec, False
            tmpVsno = FileData(CurFdIdx).GetColumnValue(DataLineNo, 20)
            tmpRout = FileData(CurFdIdx).GetColumnValue(DataLineNo, 21)
            CheckNumberOfVias DataLineNo, 20, ExtFldCnt, tmpVsno, tmpRout
            Select Case ErrNumber
                Case -1
                    SetErrorText "8", DataLineNo, ExtFldCnt, "ALC?", True, 0
                Case -2
                    SetErrorText "9", DataLineNo, ExtFldCnt, "FLT?", True, 0
                Case -3
                    SetErrorText "3", DataLineNo, ExtFldCnt, "P.FROM?", True, 0
                Case -4
                    SetErrorText "4", DataLineNo, ExtFldCnt, "P.TO?", True, 0
                Case Else
            End Select
            DataLineNo = DataLineNo + 1
        End If
        'If IsValidArr And IsValidDep Then
            'FileData(CurFdIdx).SetColumnValue DataLineNo - 1, 0, "I"
            'SetErrorText 21, DataLineNo - 1, ExtFldCnt, "IMPL.ARR", False, 1
        'End If
        If (Not IsValidArr) And (Not IsValidDep) Then
            NewRec = ""
            NewRec = NewRec & ConvertRmsImpDate(GetItem(tmpData, 5, ",")) & ","
            NewRec = NewRec & ConvertRmsImpDate(GetItem(tmpData, 6, ",")) & ","
            NewRec = NewRec & FormatFrequency(GetItem(tmpData, 11, ","), "123", IsValidArr) & ","
            NewRec = NewRec & ","
            NewRec = NewRec & GetItem(tmpData, 1, ",") & ","
            NewRec = NewRec & GetItem(tmpData, 2, ",") & ","
            NewRec = NewRec & GetItem(tmpData, 3, ",") & ","
            NewRec = NewRec & ",,"
            NewRec = NewRec & GetItem(tmpData, 2, ",") & ","
            NewRec = NewRec & GetItem(tmpData, 3, ",") & ","
            NewRec = NewRec & ","
            NewRec = NewRec & ",,"
            NewRec = NewRec & GetItem(tmpData, 7, ",") & ","    'ACT
            NewRec = NewRec & ","
            NewRec = NewRec & ","
            NewRec = NewRec & GetItem(tmpData, 4, ",") & ","
            NewRec = NewRec & GetItem(tmpData, 12, ",") & ","
            NewRec = NewRec & ",,"
            NewRec = NewRec & GetItem(tmpData, 8, ",") & "P/"
            NewRec = NewRec & GetItem(tmpData, 9, ",") & "J/"
            NewRec = NewRec & GetItem(tmpData, 10, ",") & "Y"
            tmpLine = Trim(Str(DataLineNo + 1))
            tmpLine = Right("    " & tmpLine, 4) & ",,"
            NewRec = tmpLine & "," & NewRec
            FileData(CurFdIdx).InsertTextLine NewRec, False
            SetErrorText 20, DataLineNo, ExtFldCnt, HomeAirport & "?", True, 0
            DataLineNo = DataLineNo + 1
        End If
        If DataLineNo Mod 100 = 0 Then
            DbfReader.DataTab.OnVScrollTo CurLine
            FileData(CurFdIdx).OnVScrollTo DataLineNo
            DbfReader.DataTab.Refresh
            FileData(CurFdIdx).Refresh
        End If
    Next
    FileData(CurFdIdx).OnVScrollTo 0
    DbfReader.DataTab.OnVScrollTo 0
    DbfReader.DataTab.Refresh
End Sub
Private Sub CheckNumberOfVias(LineNo As Long, ColNo As Long, RemColNo As Long, ViaCount As String, ViaList As String)
    If ItemCount(ViaList, "|") <> Val(ViaCount) Then SetErrorText Str(ColNo), LineNo, RemColNo, "VS.NO?", True, 0
End Sub
Private Sub ShrinkRmsImpFile()
    Dim CurLine As Long
    Dim NxtLine As Long
    Dim MaxLine As Long
    Dim tmpData As String
    Dim tmpLine As String
    Dim FirstChar As String
    Dim FirstItem As String
    Dim BreakOut As Boolean
    Dim RouteFound As Boolean
    DbfReader.DataTab.HeaderString = "Shrinked File Content"
    MaxLine = DbfReader.DataTab.GetLineCount - 1
    CurLine = 1
    While CurLine <= MaxLine
        tmpLine = DbfReader.DataTab.GetColumnValue(CurLine, 0)
        FirstItem = GetItem(tmpLine, 1, ",")
        FirstChar = Left(tmpLine, 1)
        If FirstItem = FirstChar Then
            'Flight Line detected
            NxtLine = CurLine + 1
            RouteFound = False
            BreakOut = False
            While Not BreakOut
                If NxtLine <= MaxLine Then
                    tmpData = DbfReader.DataTab.GetColumnValue(NxtLine, 0)
                    FirstItem = GetItem(tmpData, 1, ",")
                    FirstChar = Left(tmpData, 1)
                    If FirstItem = FirstChar Then
                        BreakOut = True
                    Else
                        RouteFound = True
                        tmpData = Trim(tmpData)
                        tmpData = FormatRouteInfo(tmpData)
                        tmpLine = tmpLine & tmpData & "|"
                        DbfReader.DataTab.DeleteLine NxtLine
                        MaxLine = MaxLine - 1
                    End If
                Else
                    BreakOut = True
                End If
            Wend
            If RouteFound Then
                tmpLine = Left(tmpLine, Len(tmpLine) - 1)
                DbfReader.DataTab.SetColumnValue CurLine, 0, tmpLine
            Else
                'Must be an Error
                MsgBox "File Error Type 1"
            End If
        Else
            'Must be an Error
            MsgBox "File Error Type 2"
        End If
        CurLine = CurLine + 1
    Wend
    DbfReader.DataTab.Refresh

End Sub

Private Sub ShrinkCskedImpFile(ForImport As Boolean)
    Dim DataLineNo As Long
    Dim CurLine As Long
    Dim NxtLine As Long
    Dim MaxLine As Long
    Dim tmpData As String
    Dim tmpLineNbr As String
    Dim tmpLine As String
    Dim NewLine As String
    Dim tmpRout As String
    Dim FirstChar As String
    Dim tmpDate As String
    Dim tmpStoa As String
    Dim tmpStod As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim tmpFred As String
    Dim BreakOut As Boolean
    Dim RouteFound As Boolean
    Dim IsValidArr As Boolean
    Dim IsValidDep As Boolean
    DbfReader.DataTab.HeaderString = "Shrinked File Content"
    MaxLine = DbfReader.DataTab.GetLineCount - 1
    DataLineNo = FileData(CurFdIdx).GetLineCount
    CurLine = 0
    While CurLine <= MaxLine
        tmpLine = DbfReader.DataTab.GetColumnValue(CurLine, 0) & ";"
        FirstChar = Left(tmpLine, 1)
        If FirstChar = "1" Then
            'Flight Line detected
            NxtLine = CurLine + 1
            RouteFound = False
            BreakOut = False
            tmpRout = ""
            While Not BreakOut
                If NxtLine <= MaxLine Then
                    tmpData = DbfReader.DataTab.GetColumnValue(NxtLine, 0)
                    FirstChar = Left(tmpData, 1)
                    If FirstChar = "2" Then
                        tmpData = Trim(tmpData)
                        tmpData = FormatRouteCskedInfo(tmpData, ForImport, RouteFound)
                        tmpRout = tmpRout & tmpData
                        DbfReader.DataTab.DeleteLine NxtLine
                        MaxLine = MaxLine - 1
                        RouteFound = True
                    Else
                        BreakOut = True
                    End If
                Else
                    BreakOut = True
                End If
            Wend
            If RouteFound Then
                tmpRout = Left(tmpRout, Len(tmpRout) - 1)
                If ForImport Then
                    IsValidArr = IsRmsImpArr(tmpRout, tmpVpfr, tmpVpto, tmpFred, tmpStoa, True)
                    IsValidDep = IsRmsImpDep(tmpRout, tmpDate, tmpStod, True)
                    If (IsValidArr) And (IsValidDep) Then
                        NewLine = FormatCskedImpLine(tmpLine, "A", tmpStoa)
                        If ItemCount(tmpRout, "|") < 3 Then
                            NewLine = FormatCskedImpLine(tmpLine, "B", tmpStod)
                            DataLineNo = DataLineNo + 1
                            tmpLineNbr = CStr(DataLineNo)
                            NewLine = tmpLineNbr & ",,," & NewLine & tmpRout & ",,,,,"
                            FileData(CurFdIdx).InsertTextLine NewLine, False
                            IsValidArr = False
                            IsValidDep = False
                        End If
                    End If
                    If IsValidArr Then
                        NewLine = FormatCskedImpLine(tmpLine, "A", tmpStoa)
                        DataLineNo = DataLineNo + 1
                        tmpLineNbr = CStr(DataLineNo)
                        NewLine = tmpLineNbr & ",,," & NewLine & tmpRout & ",,,,,"
                        FileData(CurFdIdx).InsertTextLine NewLine, False
                    End If
                    If IsValidDep Then
                        NewLine = FormatCskedImpLine(tmpLine, "D", tmpStod)
                        DataLineNo = DataLineNo + 1
                        tmpLineNbr = CStr(DataLineNo)
                        NewLine = tmpLineNbr & ",,," & NewLine & tmpRout & ",,,,,"
                        FileData(CurFdIdx).InsertTextLine NewLine, False
                    End If
                Else
                    tmpLine = FormatCskedSplitLine(tmpLine)
                    tmpLine = tmpLine & tmpRout
                    DbfReader.DataTab.SetColumnValue CurLine, 0, tmpLine
                End If
            Else
                'Must be an Error
                MsgBox "File Error Type 1"
            End If
        Else
            If (FirstChar <> "0") And (FirstChar <> "9") Then
                'Must be an Error
                MsgBox "File Error Type 2"
            End If
        End If
        CurLine = CurLine + 1
    Wend
    DbfReader.DataTab.RedrawTab
End Sub
Private Function FormatCskedImpLine(InpLine As String, UseAdid As String, UseSked As String) As String
    Dim Result As String
    Result = UseAdid & ","
    'Result = Result & "TTYP" & ","
    Result = Result & Trim(Mid(InpLine, 32, 3)) & ","
    Result = Result & Trim(Mid(InpLine, 2, 3)) & ","
    Result = Result & Trim(Mid(InpLine, 5, 5)) & ","
    Result = Result & UseSked & ","
    Result = Result & Trim(Mid(InpLine, 27, 5)) & ","
    Result = Result & Trim(Mid(InpLine, 22, 5)) & ",,"
    'Result = Result & Mid(InpLine, 10, 12) & ","
    FormatCskedImpLine = Result
End Function
Private Function FormatCskedSplitLine(InpLine As String) As String
    Dim Result As String
    Result = ""
    Result = Result & Trim(Mid(InpLine, 1, 1)) & ","
    Result = Result & Trim(Mid(InpLine, 2, 3)) & ","
    Result = Result & Trim(Mid(InpLine, 5, 5)) & ","
    Result = Result & Mid(InpLine, 10, 8) & ","
    Result = Result & Mid(InpLine, 18, 4) & ","
    Result = Result & Trim(Mid(InpLine, 22, 5)) & ","
    Result = Result & Trim(Mid(InpLine, 27, 5)) & ","
    'Result = Result & Trim(Mid(InpLine, 32, 5)) & ","
    FormatCskedSplitLine = Result
End Function
Private Function FormatRouteCskedInfo(RouteData As String, ForImport As Boolean, IsNextLine As Boolean) As String
    Dim Result As String
    Result = ""
    If ForImport Then
        If Not IsNextLine Then Result = Result & Trim(Mid(RouteData, 23, 5)) & ":-"
        Result = Result & Trim(Mid(RouteData, 33, 12)) & "|"
        Result = Result & Trim(Mid(RouteData, 28, 5)) & ":"
        Result = Result & Trim(Mid(RouteData, 45, 12)) & "-"
    Else
        Result = Result & Trim(Mid(RouteData, 23, 5)) & "-"
        Result = Result & Trim(Mid(RouteData, 28, 5)) & ":"
        Result = Result & Trim(Mid(RouteData, 33, 12)) & "-"
        Result = Result & Trim(Mid(RouteData, 45, 12)) & "|"
    End If
    FormatRouteCskedInfo = Result
End Function


Private Function FormatRouteInfo(RouteData As String) As String
    Dim Result As String
    Result = ""
    Result = Result & GetItem(RouteData, 1, ",") & ":"
    'Result = Result & "{ST}" & Left(GetItem(RouteData, 2, ",") & "    ", 4) & "-"
    'Result = Result & Left(GetItem(RouteData, 2, ",") & "    ", 4) & "-"
    'Result = Result & Left(GetItem(RouteData, 3, ",") & "    ", 4)
    Result = Result & GetItem(RouteData, 2, ",") & "-"
    Result = Result & GetItem(RouteData, 3, ",")
    FormatRouteInfo = Result
End Function
Private Function BuildRmsImpLine(NewRec As String, InpLine As String, AsDepFlight As Boolean, ErrNumber As Integer) As Boolean
    Dim LineIsValid As Boolean
    Dim IsValid As Boolean
    Dim tmpRout As String
    Dim tmpAlc2 As String
    Dim tmpFltn As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim tmpFreq As String
    Dim tmpStoa As String
    Dim tmpStod As String
    Dim tmpData As String
    Dim DepVpfr As String
    Dim DepVpto As String
    Dim DepFreq As String
    LineIsValid = False
    NewRec = ""
    ErrNumber = 0
    tmpRout = GetItem(InpLine, 12, ",")
    tmpVpfr = ConvertRmsImpDate(GetItem(InpLine, 5, ","))
    tmpVpto = ConvertRmsImpDate(GetItem(InpLine, 6, ","))
    tmpFreq = FormatFrequency(GetItem(InpLine, 11, ","), "123", IsValid)
    DepVpfr = tmpVpfr
    DepVpto = tmpVpto
    DepFreq = tmpFreq
    tmpAlc2 = Trim(GetItem(InpLine, 2, ","))
    tmpFltn = GetItem(InpLine, 3, ",")
    If AsDepFlight Then
        If IsRmsImpDep(tmpRout, tmpVpfr, tmpStod, False) Then
            LineIsValid = True
            NewRec = NewRec & tmpVpfr & ","
            NewRec = NewRec & tmpVpto & ","
            NewRec = NewRec & tmpFreq & ",,"
            'NewRec = NewRec & GetItem(InpLine, 1, ",") & ","
            NewRec = NewRec & "" & ","
            NewRec = NewRec & ",,,"
            NewRec = NewRec & DefaultTtyp & ","
            
            NewRec = NewRec & tmpAlc2 & ","
            NewRec = NewRec & tmpFltn & ","
            NewRec = NewRec & tmpStod & ","
            NewRec = NewRec & ",,"
            NewRec = NewRec & Trim(GetItem(InpLine, 7, ",")) & ","    'ACT
            NewRec = NewRec & ","
            NewRec = NewRec & ","
            NewRec = NewRec & GetItem(InpLine, 4, ",") & ","    'VSNO
            NewRec = NewRec & tmpRout & ","
            NewRec = NewRec & DepVpfr & ","
            NewRec = NewRec & DepFreq & ","
            NewRec = NewRec & GetItem(InpLine, 8, ",") & "P/"
            NewRec = NewRec & GetItem(InpLine, 9, ",") & "J/"
            NewRec = NewRec & GetItem(InpLine, 10, ",") & "Y"
        End If
    Else
        If IsRmsImpArr(tmpRout, tmpVpfr, tmpVpto, tmpFreq, tmpStoa, False) Then
            LineIsValid = True
            NewRec = NewRec & tmpVpfr & ","
            NewRec = NewRec & tmpVpto & ","
            NewRec = NewRec & tmpFreq & ",,"
            'NewRec = NewRec & GetItem(InpLine, 1, ",") & ","
            NewRec = NewRec & DefaultTtyp & ","
            NewRec = NewRec & tmpAlc2 & ","
            NewRec = NewRec & tmpFltn & ","
            NewRec = NewRec & tmpStoa & ","
            NewRec = NewRec & ",,,,,,"
            NewRec = NewRec & Trim(GetItem(InpLine, 7, ",")) & ","    'ACT
            NewRec = NewRec & ","
            NewRec = NewRec & ","
            NewRec = NewRec & GetItem(InpLine, 4, ",") & ","    'VSNO
            NewRec = NewRec & tmpRout & ","
            NewRec = NewRec & DepVpfr & ","
            NewRec = NewRec & DepFreq & ","
            NewRec = NewRec & GetItem(InpLine, 8, ",") & "P/"
            NewRec = NewRec & GetItem(InpLine, 9, ",") & "J/"
            NewRec = NewRec & GetItem(InpLine, 10, ",") & "Y"
        End If
    End If
    If Trim(tmpAlc2) = "" Then ErrNumber = -1
    If Trim(tmpFltn) = "" Then ErrNumber = -2
    If Trim(tmpVpfr) = "" Then ErrNumber = -3
    If Trim(tmpVpto) = "" Then ErrNumber = -4
    BuildRmsImpLine = LineIsValid
End Function
Private Function ConvertRmsImpDate(RmsDate As String) As String
    Dim Result As String
    Result = Trim(RmsDate)
    If Result <> "" Then
        Result = "20" & Mid(RmsDate, 5, 2)
        Result = Result & Mid(RmsDate, 3, 2)
        Result = Result & Mid(RmsDate, 1, 2)
    End If
    ConvertRmsImpDate = Result
End Function
Private Function IsRmsImpDep(FlightRoute As String, VpfrDate As String, FltStod As String, FullDate As Boolean) As Boolean
    Dim tmpApc As String
    Dim tmpLeg As String
    Dim tmpSked As String
    Dim tmpStoa As String
    Dim tmpStod As String
    tmpLeg = GetItem(FlightRoute, 1, "|")
    tmpApc = GetItem(tmpLeg, 1, ":")
    IsRmsImpDep = False
    If tmpApc = HomeAirport Then
        tmpSked = GetItem(tmpLeg, 2, ":")
        'GetKeyItem tmpSked, tmpLeg, "{ST}", "{"
        tmpStoa = GetItem(tmpSked, 1, "-")
        tmpStod = GetItem(tmpSked, 2, "-")
        If (tmpStoa = "") And (tmpStod <> "") Then
            If FullDate Then
                FltStod = tmpStod
            Else
                FltStod = tmpStod
            End If
            IsRmsImpDep = True
        End If
    End If
End Function
Private Function IsRmsImpArr(DepRoute As String, DepVpfr As String, DepVpto As String, DepFreq As String, ArrStoa As String, FullDate As Boolean) As Boolean
    Dim ItmCnt As Integer
    Dim LegNbr As Integer
    Dim tmpApc As String
    Dim tmpLeg As String
    Dim tmpSked As String
    Dim tmpStoa As String
    Dim tmpStod As String
    Dim LegDate As String
    Dim LegTime As String
    Dim NxtTime As String
    ItmCnt = ItemCount(DepRoute, "|")
    tmpLeg = GetItem(DepRoute, ItmCnt, "|")
    tmpApc = GetItem(tmpLeg, 1, ":")
    If tmpApc = HomeAirport Then
        If Not FullDate Then
            tmpLeg = GetItem(DepRoute, 1, "|")
            tmpSked = GetItem(tmpLeg, 2, ":")
            'GetKeyItem tmpSked, tmpLeg, "{ST}", "{"
            LegDate = DepVpfr
            tmpStod = GetItem(tmpSked, 2, "-")
            LegTime = LegDate & tmpStod
            For LegNbr = 2 To ItmCnt - 1
                tmpLeg = GetItem(DepRoute, LegNbr, "|")
                tmpSked = GetItem(tmpLeg, 2, ":")
                'GetKeyItem tmpSked, tmpLeg, "{ST}", "{"
                tmpStoa = GetItem(tmpSked, 1, "-")
                NxtTime = LegDate & tmpStoa
                If NxtTime < LegTime Then
                    MovePeriodChain DepVpfr, DepVpto, DepFreq, 1
                    LegDate = DepVpfr
                    NxtTime = LegDate & tmpStoa
                End If
                LegTime = NxtTime
                tmpStod = GetItem(tmpSked, 2, "-")
                NxtTime = LegDate & tmpStod
                If NxtTime < LegTime Then
                    MovePeriodChain DepVpfr, DepVpto, DepFreq, 1
                    LegDate = DepVpfr
                    NxtTime = LegDate & tmpStod
                End If
                LegTime = NxtTime
            Next
        End If
        tmpLeg = GetItem(DepRoute, ItmCnt, "|")
        tmpSked = GetItem(tmpLeg, 2, ":")
        'GetKeyItem tmpSked, tmpLeg, "{ST}", "{"
        tmpStoa = GetItem(tmpSked, 1, "-")
        If FullDate Then
            ArrStoa = tmpStoa
        Else
            NxtTime = LegDate & tmpStoa
            If NxtTime < LegTime Then
                MovePeriodChain DepVpfr, DepVpto, DepFreq, 1
                LegDate = DepVpfr
                NxtTime = LegDate & tmpStoa
            End If
            ArrStoa = tmpStoa
        End If
        IsRmsImpArr = True
    Else
        IsRmsImpArr = False
    End If
End Function
Private Sub MovePeriodChain(CurVpfr As String, CurVpto As String, CurFreq As String, MoveDays As Integer)
    If CurVpfr <> "" Then CurVpfr = CedaDateAdd(CurVpfr, MoveDays)
    If CurVpto <> "" Then CurVpto = CedaDateAdd(CurVpto, MoveDays)
    If CurFreq <> "" Then CurFreq = ShiftFrqd(CurFreq, MoveDays)
End Sub

Private Function InsertRmsLinkLine(InpLine As String, IsFirstLine As Boolean) As Boolean
    Dim Proceed As Boolean
    Dim LineIsValid As Boolean
    Dim FilterIsValid As Boolean
    Dim RouteError As Boolean
    Dim NewLine As String
    Dim tmpData As String
    Dim FirstChar As String
    Dim RouteInfo As String
    Dim ErrNumber As Integer
    ErrNumber = 0
    Proceed = True
    LineIsValid = True
    FilterIsValid = True
    IsFirstLine = False
    NewLine = InpLine
    AllLinCnt = AllLinCnt + 1
    FirstChar = Left(InpLine, 1)
    RouteError = False
    If chkExpand.Value = 1 Then
        Select Case FirstChar
            Case "1"    'Valid line
                NewLine = BuildRmsLinkLine(InpLine, ErrNumber)
                RouteInfo = GetItem(NewLine, 18, ",")
                If InStr(RouteInfo, HomeAirport) = 0 Then RouteError = True
                NewLine = Right("    " & Str(DatLinCnt + 1), 4) & ",,," & NewLine & ",,"
                GrpLinCnt = GrpLinCnt + 1
            Case "0"    'Header
                MySetUp.DetermineFileType "HEADER," & Trim(Str(GrpLinCnt)) & "," & Chr(30) & InpLine
                GrpLinCnt = 0
                'IsFirstLine = True
                IsFirstLine = False
                LineIsValid = False
                Proceed = False
            Case "9"    'Footer
                MySetUp.DetermineFileType "FOOTER," & Trim(Str(GrpLinCnt)) & "," & Chr(30) & InpLine
                GrpLinCnt = 0
                LineIsValid = False
            Case "*"    'Remark
                Proceed = False
                LineIsValid = False
            Case Else
        End Select
    ElseIf chkSplit.Value = 1 Then
        Select Case FirstChar
            Case "1"    'Valid line
                NewLine = Right("    " & Str(DatLinCnt + 1), 4) & ",,," & InpLine & ",,"
            Case "0"    'Header
                LineIsValid = False
                IsFirstLine = True
            Case "9"    'Footer
                LineIsValid = False
            Case "*"    'Remark
                Proceed = False
                LineIsValid = False
            Case Else
        End Select
    ElseIf chkFilter.Value = 1 Then
        NewLine = Right("    " & Str(DatLinCnt + 1), 4) & "," & Replace(InpLine, ",", "|", 1, -1, vbBinaryCompare) & ",,"
        If FirstChar <> "1" Then FilterIsValid = False
    ElseIf chkLoad.Value = 1 Then
        NewLine = Right("    " & Str(DatLinCnt + 1), 4) & "," & Replace(InpLine, ",", "|", 1, -1, vbBinaryCompare) & ",,"
    Else
        Proceed = False
    End If
    If LineIsValid Then
        FileData(CurFdIdx).InsertTextLine NewLine, False
        If Not FilterIsValid Then FileData(CurFdIdx).SetLineColor DatLinCnt, vbWhite, vbMagenta
        Select Case FirstChar
            Case "1"    'Valid line
                If RouteError Then
                    SetErrorText "20", CLng(DatLinCnt), ExtFldCnt, HomeAirport & "?", True, 0
                End If
                Select Case ErrNumber
                    Case -1
                        SetErrorText "8,12", CLng(DatLinCnt), ExtFldCnt, "ALC?", True, 0
                    Case -2
                        SetErrorText "9", CLng(DatLinCnt), ExtFldCnt, "ARR?", True, 0
                    Case -3
                        SetErrorText "13", CLng(DatLinCnt), ExtFldCnt, "DEP?", True, 0
                    Case -4
                        SetErrorText "3", CLng(DatLinCnt), ExtFldCnt, "P.FROM?", True, 0
                    Case -5
                        SetErrorText "4", CLng(DatLinCnt), ExtFldCnt, "P.TO?", True, 0
                    Case Else
                End Select
            Case "0"    'Header
                FileData(CurFdIdx).SetLineColor DatLinCnt, vbBlack, vbYellow
                IsFirstLine = True
            Case "9"    'Footer
                FileData(CurFdIdx).SetLineColor DatLinCnt, vbBlack, vbGreen
            Case "*"    'Remark
            Case Else
        End Select
        DatLinCnt = DatLinCnt + 1
    End If
    InsertRmsLinkLine = Proceed
End Function
Private Function BuildRmsLinkLine(InpLine As String, ErrNumber As Integer) As String
    Dim IsValidArr As Boolean
    Dim IsValidDep As Boolean
    Dim NewLine As String
    Dim tmpData As String
    Dim tmpFlca As String
    Dim tmpFlna As String
    Dim tmpFlnd As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim tmpDaof As String
    Dim tmpFrea As String
    Dim tmpFred As String
    NewLine = ""
    ErrNumber = 0
    tmpData = GetItem(InpLine, 5, ",")
    tmpFlna = GetItem(tmpData, 1, "/")
    tmpFlnd = GetItem(tmpData, 2, "/")
    tmpFlca = Left(tmpFlna, 2)
    tmpFlna = Mid(tmpFlna, 3)
    tmpData = GetItem(InpLine, 12, ",")
    tmpVpfr = Trim(GetItem(tmpData, 1, "-"))
    tmpVpto = Trim(GetItem(tmpData, 2, "-"))
    tmpVpfr = Right(tmpVpfr, 4) & Mid(tmpVpfr, 4, 2) & Left(tmpVpfr, 2)
    tmpVpto = Right(tmpVpto, 4) & Mid(tmpVpto, 4, 2) & Left(tmpVpto, 2)
    If Trim(tmpVpfr) = "" Then ErrNumber = -4
    If Trim(tmpVpto) = "" Then ErrNumber = -5
    If Trim(tmpFlnd) = "" Then ErrNumber = -3
    If Trim(tmpFlna) = "" Then ErrNumber = -2
    If Trim(tmpFlca) = "" Then ErrNumber = -1
    
    NewLine = NewLine & tmpVpfr & ","
    NewLine = NewLine & tmpVpto & ","
    tmpFrea = GetItem(InpLine, 8, ",")
    NewLine = NewLine & tmpFrea & ","
    NewLine = NewLine & " " & ","
    NewLine = NewLine & DefaultTtyp & ","
    NewLine = NewLine & tmpFlca & ","
    NewLine = NewLine & tmpFlna & ","
    NewLine = NewLine & GetItem(InpLine, 9, ",") & ","
    NewLine = NewLine & DefaultTtyp & ","
    NewLine = NewLine & tmpFlca & ","
    NewLine = NewLine & tmpFlnd & ","
    NewLine = NewLine & GetItem(InpLine, 11, ",") & ","
    tmpData = GetItem(InpLine, 10, ",") & "        "
    tmpFred = Left(tmpData, 7)
    tmpData = Mid(tmpData, 8, 1)
    If tmpData = "*" Then
        tmpDaof = CreateDaofFromFreq(tmpFrea, tmpFred, True, IsValidArr, IsValidDep)
    Else
        tmpDaof = tmpData
    End If
    NewLine = NewLine & tmpDaof & ","
    tmpData = GetItem(InpLine, 10, ",") & "        "
    NewLine = NewLine & Mid(tmpData, 8, 1) & ","
    NewLine = NewLine & "" & ","
    NewLine = NewLine & MySetUp.CurFileInfo(8).Caption & ","
    NewLine = NewLine & "" & ","
    tmpData = GetItem(InpLine, 4, ",")
    tmpData = Replace(tmpData, "/", "|", 1, -1, vbBinaryCompare)
    NewLine = NewLine & tmpData & ","
    NewLine = NewLine & GetItem(InpLine, 7, ",") & ","
    NewLine = NewLine & tmpFred & ","
    NewLine = NewLine & GetItem(InpLine, 3, ",")
    BuildRmsLinkLine = NewLine
End Function

Private Function CreateDaofFromFreq(ArrFreq As String, DepFreq As String, NotOnSameDay As Boolean, IsValidArr As Boolean, IsValidDep As Boolean) As String
    Dim Result As String
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim n As Integer
    ArrFreq = FormatFrequency(ArrFreq, "123", IsValidArr)
    DepFreq = FormatFrequency(DepFreq, "123", IsValidDep)
    If (ArrFreq <> DepFreq) Or (NotOnSameDay) Then
        n = 0
        k = 0
        i = 0
        While i < 7
            i = i + 1
            If Mid(ArrFreq, i, 1) <> "." Then
                j = i
                While k < 7
                    k = k + 1
                    j = j + 1
                    If j > 7 Then j = 1
                    n = n + 1
                    If Mid(DepFreq, j, 1) <> "." Then k = 8
                Wend
                i = 8
            End If
        Wend
        Result = Trim(Str(n))
    Else
        Result = "?"
    End If
    CreateDaofFromFreq = Result
End Function
Private Function InsertDataLine(CurLine As String) As Boolean
    Dim tmpLine As String
    Dim datLine As String
    Dim UseLine As String
    Dim LineIsValid As Boolean
    Dim tabCols As Integer
    Dim datCols As Integer
    Dim itm As Integer
    tabCols = CInt(FileData(CurFdIdx).GetColumnCount)
    UseLine = Replace(CurLine, ",", ";", 1, -1, vbBinaryCompare)
    LineIsValid = CheckValidLines(UseLine)
    If LineIsValid And LineWrap Then
        Line Input #1, tmpLine
        tmpLine = Replace(tmpLine, ",", ";", 1, -1, vbBinaryCompare)
        UseLine = UseLine & tmpLine
    End If
    AllLinCnt = AllLinCnt + 1
    If chkFilter.Value = 1 Then
        If IgnoreEmptyLine Then
            If Trim(UseLine) = "" Then LineIsValid = False
        End If
        If LineIsValid Then
            DatLinCnt = DatLinCnt + 1
            datLine = SplitLineData(UseLine)
            tmpLine = CStr(DatLinCnt) & "," & datLine & ",,,"
            tmpLine = tmpLine & GetUniqueLineKey(AllLinCnt, UseLine)
            datCols = CInt(ItemCount(tmpLine, ","))
            If datCols <> tabCols Then
                itm = tabCols - datCols
            End If
            FileData(CurFdIdx).InsertTextLine tmpLine, False
        End If
    Else
        DatLinCnt = DatLinCnt + 1
        datLine = SplitLineData(UseLine)
        tmpLine = CStr(DatLinCnt) & "," & datLine & ",,,"
        datCols = CInt(ItemCount(tmpLine, ","))
        If datCols <> tabCols Then
            itm = tabCols - datCols
        End If
        FileData(CurFdIdx).InsertTextLine tmpLine, False
        If LineIsValid = False Then
            FileData(CurFdIdx).SetLineColor DatLinCnt - 1, vbWhite, vbMagenta
        End If
    End If
    InsertDataLine = LineIsValid
End Function
Private Function GetUniqueLineKey(UseLine As Integer, UseData As String) As String
    Dim LineKey As String
    LineKey = ""
    Select Case UniqueKeyFormat
        Case "AUTO"
        Case "FIXED"
            If (UniqueKeyPos > 0) And (UniqueKeyLen > 0) Then
                LineKey = Mid(UseData, UniqueKeyPos, UniqueKeyLen)
            End If
        Case "LINE"
            LineKey = Right("000000" & CStr(UseLine), 6)
        Case Else
    End Select
    GetUniqueLineKey = LineKey
End Function
Private Function SplitLineData(tmpLine As String) As String
Dim Result As String
Dim tmpItem As String
Dim tmpFldVal As String
Dim tmpAddVal As String
Dim tmpData As String
Dim FldNbr As Integer
Dim FldPos As Integer
Dim FldLen As Integer

    If chkSplit.Value = 1 Then
        FldNbr = 0
        ExtFldCnt = 0
        Result = ",,"
        'MsgBox FormatType
        Do
            FldNbr = FldNbr + 1
            ExtFldCnt = ExtFldCnt + 1
            tmpItem = GetItem(CurFldPosLst, FldNbr, ",")
            tmpFldVal = GetCombiValue(FldNbr, tmpLine, tmpItem)
            FldPos = Val(tmpItem)
            FldLen = Val(GetItem(FldLenLst, FldNbr, ","))
            If (FldPos = 0) And (FormatType = "ITEMS") Then
                If CurFldPosLst = "ALL" Then
                    tmpData = GetItem(CurrentHeader, FldNbr, ",")
                    If tmpData <> "" Then FldPos = FldNbr
                End If
            End If
            If (FldPos > 0) Or (tmpItem = "-") Then
                If (tmpFldVal = "") And (tmpItem <> "-") Then
                    If FormatType = "FIXED" Then
                        tmpFldVal = Trim(Mid(tmpLine, FldPos, FldLen))
                    Else
                        tmpFldVal = GetItem(tmpLine, FldPos, ItemSep)
                    End If
                End If
                If (tmpFldVal <> "") Or (tmpItem = "-") Then
                    If chkExpand.Value = 1 Then
                        tmpAddVal = GetItem(FldAddLst, FldNbr, ",")
                        If tmpAddVal <> "" Then Result = Result & Replace(tmpAddVal, "#", " ", 1, -1, vbBinaryCompare)
                        tmpFldVal = CheckDateFieldFormat(FldNbr, tmpFldVal, DateFields, TimeFields, True)
                        tmpFldVal = ClearZeroFieldFiller(FldNbr, tmpFldVal)
                        tmpFldVal = ClearFieldValues(FldNbr, tmpFldVal)
                        tmpFldVal = CheckFrequencyFormat(FldNbr, tmpFldVal)
                    End If
                End If
                Result = Result & tmpFldVal & ","
            End If
        Loop While (FldPos > 0) Or (tmpItem = "-")

        ExtFldCnt = ExtFldCnt + 2
    Else
        Result = tmpLine
    End If
    SplitLineData = Result
End Function
Private Function GetCombiValue(tmpFldNbr As Integer, tmpLine As String, tmpList As String) As String
Dim Result As String
Dim FldItm As Integer
Dim ItmNbr As Integer
Dim ItmLen As Integer
Dim ItmPos As Integer
Dim FldFrm As String
Dim ItmDat As String
Dim tmpFlno As String
Dim FltAlc As String
Dim FltNum As String
Dim FltSfx As String
Dim ItmText As String
    Result = ""
    If Left(tmpList, 1) = "#" Then
        If FlnoField > 0 Then
            tmpFlno = GetItem(tmpLine, FlnoField, ItemSep)
            StripAftFlno tmpFlno, FltAlc, FltNum, FltSfx
        End If
        Select Case tmpList
            Case "#FLC"
                Result = FltAlc
            Case "#FLN"
                Result = FltNum
            Case "#FLS"
                Result = FltSfx
            Case "#FLT"
                Result = RTrim(Left(FltNum + "     ", 5) + FltSfx)
            Case Else
        End Select
        tmpList = "-"
    ElseIf InStr(tmpList, "+") > 0 Then
        ItmNbr = 0
        Do
            ItmNbr = ItmNbr + 1
            FldItm = Val(GetItem(tmpList, ItmNbr, "+"))
            If FldItm > 0 Then
                ItmDat = GetItem(tmpLine, FldItm, ItemSep)
                FldFrm = GetItem(FldFrmLst, FldItm, ",")
                ItmDat = CheckFieldFormat(ItmDat, FldFrm)
                Result = Result & ItmDat
            End If
        Loop While FldItm > 0
    ElseIf InStr(tmpList, "L") > 0 Then
        FldItm = Val(GetItem(tmpList, 1, "L"))
        ItmLen = Val(GetItem(tmpList, 2, "L"))
        If (FldItm > 0) And (ItmLen > 0) Then Result = Result & Left(GetItem(tmpLine, FldItm, ItemSep), ItmLen)
    ElseIf InStr(tmpList, "R") > 0 Then
        FldItm = Val(GetItem(tmpList, 1, "R"))
        ItmLen = Val(GetItem(tmpList, 2, "R"))
        If (FldItm > 0) And (ItmLen > 0) Then Result = Result & Right(GetItem(tmpLine, FldItm, ItemSep), ItmLen)
    ElseIf InStr(tmpList, "M") > 0 Then
        FldItm = Val(GetItem(tmpList, 1, "M"))
        ItmPos = Val(GetItem(tmpList, 2, "M"))
        ItmLen = Val(GetItem(tmpList, 2, "/"))
        If (FldItm > 0) And (ItmLen > 0) And (ItmPos > 0) Then Result = Result & Mid(GetItem(tmpLine, FldItm, ItemSep), ItmPos, ItmLen)
    End If
    GetCombiValue = Result
End Function
Private Function CheckFieldFormat(FieldData As String, FieldFormat As String)
    Dim Result As String
    Dim FrmCod As String
    Dim FrmCtl As String
    Dim tmpVal As Integer
    Result = FieldData
    FrmCod = Left(FieldFormat, 1)
    FrmCtl = Mid(FieldFormat, 2)
    Select Case FrmCod
        Case "N"
            'Numerical fixed length leading zeros
            If Trim(FieldData) <> "" Then
                tmpVal = Val(FrmCtl)
                Result = Right(String(tmpVal, "0") & Trim(FieldData), tmpVal)
            End If
        Case Else
    End Select
    'MsgBox FieldData & " / " & FieldFormat & " / " & Result
    CheckFieldFormat = Result
End Function

Private Sub GetTimeChains()
Dim CurLine As Long
Dim MaxLine As Long
Dim tmpStodDay As String
Dim tmpStod As String
Dim tmpStoaDay As String
Dim tmpStoa As String
Dim FldaColIdx As Long
Dim StodColIdx As Long
Dim StoaColIdx As Long
    If TimeChainInfo <> "" Then
        StatusBar.Panels(7).Text = "Formatting Time Fields ..."
        FldaColIdx = Val(GetItem(TimeChainInfo, 1, ",")) + 2
        StodColIdx = Val(GetItem(TimeChainInfo, 2, ",")) + 2
        StoaColIdx = Val(GetItem(TimeChainInfo, 3, ",")) + 2
        If (FldaColIdx > 2) And (StodColIdx > 2) And (StoaColIdx > 2) Then
            MaxLine = FileData(CurFdIdx).GetLineCount - 1
            For CurLine = 0 To MaxLine
                tmpStodDay = FileData(CurFdIdx).GetColumnValue(CurLine, FldaColIdx)
                tmpStoaDay = tmpStodDay
                tmpStod = FileData(CurFdIdx).GetColumnValue(CurLine, StodColIdx)
                tmpStoa = FileData(CurFdIdx).GetColumnValue(CurLine, StoaColIdx)
                If tmpStoa < tmpStod Then
                    tmpStoaDay = ShiftDate(tmpStoaDay, 1)
                End If
                If tmpStod <> "" Then
                    tmpStod = tmpStodDay & tmpStod
                    FileData(CurFdIdx).SetColumnValue CurLine, StodColIdx, tmpStod
                End If
                If tmpStoa <> "" Then
                    tmpStoa = tmpStoaDay & tmpStoa
                    FileData(CurFdIdx).SetColumnValue CurLine, StoaColIdx, tmpStoa
                End If
            Next
            FileData(CurFdIdx).RedrawTab
            StatusBar.Panels(7).Text = "Ready"
            Refresh
        End If
    End If
End Sub

Private Sub ClearDoubleVias()
    Dim i As Integer
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpVia1 As String
    Dim tmpVia2 As String
    Dim Via1ColIdx As Long
    Dim Via2ColIdx As Long
    Dim ClrColIdx As Long
    Dim ItmCnt As Integer
    Dim tmpFldNam As String
    If CheckViaList <> "" Then
        StatusBar.Panels(7).Text = "Clearing Duplicated VIA's ..."
        ItmCnt = ItemCount(CheckViaList, ",") - 1
        For i = 0 To ItmCnt Step 3
            Via1ColIdx = Val(GetItem(CheckViaList, i + 1, ",")) + 2
            Via2ColIdx = Val(GetItem(CheckViaList, i + 2, ",")) + 2
            ClrColIdx = Val(GetItem(CheckViaList, i + 3, ",")) + 2
            If (Via1ColIdx > 2) And (Via2ColIdx > 2) And (ClrColIdx > 2) Then
                MaxLine = FileData(CurFdIdx).GetLineCount - 1
                For CurLine = 0 To MaxLine
                    'Attention: The meaning of Via1 and Via2 is different to old versions of import tool!!!
                    tmpVia1 = Trim(FileData(CurFdIdx).GetColumnValue(CurLine, Via1ColIdx))  'Now Always ORIG or DEST
                    tmpVia2 = Trim(FileData(CurFdIdx).GetColumnValue(CurLine, Via2ColIdx))  'Now Always VIA
                    If (tmpVia1 <> "") And (tmpVia2 <> "") Then
                        If tmpVia1 = tmpVia2 Then
                            FileData(CurFdIdx).SetColumnValue CurLine, ClrColIdx, ""
                        End If
                    End If
                    If (tmpVia1 = "") And (tmpVia2 <> "") Then
                        'Set VIA into missing ORG/DES ?
                        'FileData(CurFdIdx).SetColumnValue CurLine, Via1ColIdx, tmpVia2
                        'FileData(CurFdIdx).SetColumnValue CurLine, Via2ColIdx, ""
                        tmpFldNam = GetRealItem(FileData(CurFdIdx).HeaderString, Via1ColIdx, ",")
                        SetErrorText CStr(Via1ColIdx), CurLine, ExtFldCnt, tmpFldNam & "?", True, 0
                    End If
                Next
                FileData(CurFdIdx).RedrawTab
                Refresh
            End If
        Next
        StatusBar.Panels(7).Text = "Ready"
    End If
End Sub
Private Sub ClearFieldsByFieldValue()
Dim LstItm As Integer
Dim ClrItm As Integer
Dim CurLine As Long
Dim MaxLine As Long
Dim tmpFldVal As String
Dim tmpChkVal As String
Dim ChkColIdx As Long
Dim ClrColIdx As Long
Dim ClrList As String
    If ClearByValue <> "" Then
        StatusBar.Panels(7).Text = "Erasing Unneeded Data ..."
        FileData(CurFdIdx).RedrawTab
        MaxLine = FileData(CurFdIdx).GetLineCount - 1
        For CurLine = 0 To MaxLine
            LstItm = 0
            Do
                LstItm = LstItm + 1
                ClrList = GetItem(ClearByValue, LstItm, "|")
                ChkColIdx = Val(GetItem(ClrList, 1, ",")) + 2
                If ChkColIdx > 2 Then
                    tmpFldVal = FileData(CurFdIdx).GetColumnValue(CurLine, ChkColIdx)
                    tmpChkVal = GetItem(ClrList, 2, ",")
                    If Trim(tmpFldVal) = Trim(tmpChkVal) Then
                        ClrItm = 2
                        Do
                            ClrItm = ClrItm + 1
                            ClrColIdx = Val(GetItem(ClrList, ClrItm, ",")) + 2
                            If ClrColIdx > 2 Then FileData(CurFdIdx).SetColumnValue CurLine, ClrColIdx, ""
                        Loop While ClrColIdx > 2
                    End If
                End If
            Loop While ChkColIdx > 2
        Next
        FileData(CurFdIdx).RedrawTab
        StatusBar.Panels(7).Text = "Ready"
        Refresh
    End If
End Sub
Private Function ShiftDate(tmpDay As String, tmpVal As Integer) As String
Dim Result As String
Dim tmpVbDate
    'Result = tmpDay
    tmpVbDate = UfisLib.CedaDateToVb(tmpDay)
    tmpVbDate = DateAdd("d", tmpVal, tmpVbDate)
    Result = Format(tmpVbDate, "yyyymmdd")
    ShiftDate = Result
End Function

Private Function ClearZeroFieldFiller(tmpFldNbr As Integer, tmpDate As String) As String
Dim Result As String
Dim tmpVal As String
    Result = tmpDate
    tmpVal = "," & Trim(Str(tmpFldNbr)) & ","
    If InStr(ClearZeros, tmpVal) > 0 Then
        If tmpDate = Left("00000000000000000000", Len(tmpDate)) Then Result = ""
    End If
    ClearZeroFieldFiller = Result
End Function
Private Function ClearFieldValues(tmpFldNbr As Integer, tmpValue As String) As String
Dim Result As String
Dim tmpChkFld As String
Dim tmpClrVal As String
Dim tmpClrLst As String
Dim LstItm As Integer
    Result = tmpValue
    LstItm = 0
    tmpChkFld = "," & Trim(Str(tmpFldNbr)) & ","
    If InStr(2, ClearValues, tmpChkFld) > 0 Then
        Do
            LstItm = LstItm + 1
            tmpClrLst = GetItem(ClearValues, LstItm, "|")
            tmpClrVal = GetItem(tmpClrLst, 2, ",")
            If tmpClrVal = tmpValue Then Result = ""
        Loop While tmpClrLst <> ""
    End If
    ClearFieldValues = Result
End Function

Private Function CheckFrequencyFormat(tmpFldNbr As Integer, tmpFreq As String) As String
Dim Result As String
Dim tmpPos As Integer
Dim tmpChr As String
Dim tmpVal As String
Dim i As Integer
Dim l As Integer
    Result = tmpFreq
    tmpVal = "," & Trim(Str(tmpFldNbr)) & ","
    If InStr(MainChkFred, tmpVal) > 0 Then
        Result = "......."
        l = Len(tmpFreq)
        For i = 1 To l
            tmpChr = Mid(tmpFreq, i, 1)
            tmpPos = Val(tmpChr)
            If tmpPos > 0 Then Mid(Result, tmpPos, 1) = tmpChr
        Next
    End If
    CheckFrequencyFormat = Result
End Function

Private Function CheckValidLines(tmpLine As String) As Boolean
Dim tmpData As String
    tmpData = "..."
    CheckValidLines = True
    If LineFilterPos > 0 Then
        If FormatType = "FIXED" Then
            tmpData = Mid(tmpLine, LineFilterPos, LineFilterLen)
        ElseIf FormatType = "ITEMS" Then
            tmpData = GetItem(tmpLine, LineFilterPos, ItemSep)
        End If
    End If
    Select Case LineFilterTyp
        Case "TEXT"
            If tmpData <> LineFilterText Then CheckValidLines = False
        Case "TIME"
            CheckValidLines = CheckValidTime(tmpData, "2400")
        Case "NUMBER"
            CheckValidLines = IsNumeric(tmpData)
        Case "MONTH3LC"
            If tmpData = "" Then tmpData = "..."
            If InStr(MonthList, tmpData) = 0 Then CheckValidLines = False
        Case "NOT"
            If tmpData = LineFilterText Then CheckValidLines = False
        Case Else
    End Select
End Function

Private Sub chkImpTyp_Click(Index As Integer)
    Static StopMyEvent As Boolean
    Dim tmpData As String
    Dim tmpStrg As String
    Dim tmpItmDat As String
    Dim tmpCurDate As String
    Dim DefTimeChain As String
    Dim DefViaCheck As String
    Dim DefTimePeriod As String
    Dim DefAdidInfo As String
    Dim i As Integer
    Dim ImpCnt As Integer
    If (Not StopMyApplEvents) And (Not IsResetApplication) And (Not IsResetMainScreen) Then
        ImpCnt = 0
        For i = 0 To chkImpTyp.UBound
            If chkImpTyp(i).Value = 1 Then ImpCnt = ImpCnt + 1
        Next
        If chkImpTyp(Index).Value = 1 Then
            If Index = AutoPushIndex Then
                'This is the expected button
                'of an automated session switch
                If (DoubleGridInit) Or (IsDoubleGridView) Then
                    PushSectionDataGrid False
                End If
                i = 1
            Else
                PushSectionDataGrid False
            End If
            AutoPushIndex = -1
        End If
        'Variable "i" now used for break points
        If ImpCnt = 0 Then
            If chkImpTyp(Index).Value = 0 Then
                'The previously pushed button goes up.
                If Index = PushedImpButton Then
                    'The user stopped the current session.
                    i = 1
                    ResetApplication Index
                Else
                    'Something went wrong
                    i = 1
                    ResetApplication Index
                End If
            Else
                'Cannot be
                i = 1
            End If
            PushSectionDataGrid False
            AutoPushIndex = -1
        ElseIf ImpCnt = 1 Then
            If chkImpTyp(Index).Value = 1 Then
                'Only one button goes down.
                'The user starts a session.
                'Or Timer2 came too late.
                If (Not DoubleGridInit) Then
                    'Starting a new session
                    ResetApplication Index
                    AutoPushIndex = -1
                    i = 1
                Else
                    'Timer problem
                    'Shouldn't be
                    i = 1
                End If
            Else
                'One button is down and one comes up
                'This happens when the user switched to another session
                'or stopps a DoubleGrid View
                If IsDoubleGridView Then
                    'Stopping the DoubleGrid View
                    i = 1
                    ResetApplication Index
                    AutoPushIndex = -1
                Else
                    'Switching the session
                    'Let it go thru
                    i = 1
                End If
            End If
        ElseIf ImpCnt = 2 Then
            'Two buttons are down now.
            'This can only be when ...
            '... the user switches to another session
            '... timer2 started a DoubleGrid session
            If chkImpTyp(Index).Value = 1 Then
                If (Not DoubleGridInit) Then
                    'Switching to a new session
                    If SectionPushed Then
                        PushedFilePath = chkRequest.Tag
                    End If
                    i = 1
                    ResetApplication Index
                Else
                    'Starting a DoubleGrid View
                    'Let it go thru
                    i = 1
                End If
            Else
                'Cannot be
                i = 1
            End If
        ElseIf ImpCnt = 3 Then
            'This happens when a user
            'switches from a DoubleGrid View
            'to another session.
            If chkImpTyp(Index).Value = 1 Then
                'Switching the session
                i = 1
                ResetApplication Index
            Else
                'Cannot be
                i = 1
                ResetApplication Index
            End If
            PushSectionDataGrid False
        Else 'Cannot be, but just to be sure ...
            If chkImpTyp(Index).Value = 1 Then
                i = 1
                ResetApplication Index
            Else
                i = 1
                ResetApplication Index
            End If
        End If
        CheckAutoUpdateMode = False
        chkAppl(10).Visible = False
        chkAppl(7).Enabled = False
        If chkImpTyp(Index).Value = 1 Then
            If chkImpTyp(Index).Tag = "DUMMY" Then
                chkImpTyp(Index).Value = 0
                Exit Sub
            End If
            DoEvents
            PushedImpButton = Index
            If Not DoubleGridInit Then
                CurFdIdx = 0
                FileData(0).ResetContent
                myIniSection = chkImpTyp(Index).Tag
                tmpData = UCase(GetIniEntry(myIniFullName, myIniSection, "", "PRE_PROCESS", "NO"))
                If tmpData = "YES" Then
                    If FileData.Count > 1 Then
                        Unload FileData(1)
                    End If
                    If FileData.Count = 1 Then
                        Load FileData(1)
                    End If
                    CurFdIdx = 1
                    SetGridButtons True, 1
                    'chkGrid(1).Value = 1
                    'chkGrid(0).Enabled = True
                    'chkGrid(1).Enabled = True
                    'chkGrid(2).Enabled = True
                Else
                    If FileData.Count > 1 Then
                        Unload FileData(1)
                    End If
                    CurFdIdx = 0
                    SetGridButtons False, -1
                    'chkGrid(0).Enabled = False
                    'chkGrid(1).Enabled = False
                    'chkGrid(2).Enabled = False
                    'chkGrid(0).Value = 0
                    'chkGrid(1).Value = 0
                    'chkGrid(2).Value = 0
                    ArrangeMainScreen "INIT"
                End If
            Else
                'Invoked by Timer2 or what?
                If FileData.Count = 1 Then
                    'Would be a fatal error
                    Load FileData(1)
                End If
                SetGridButtons True, 1
                'chkGrid(0).Value = 1
                'chkGrid(0).Enabled = True
                'chkGrid(1).Enabled = True
                'chkGrid(2).Enabled = True
                CurFdIdx = 0
            End If
            ImportActionType = ""
            AddOnDataFound = False
            AddOnDataMerge = False
            If Not RestoreAction Then MySetUp.DetermineFileType "SESSION"
            ResetTimeFields
            ResetChkImpTyp Index
            If (Initializing) Or (Not RestoreAction) Then
                TabFrame.Enabled = False
                ResetMain 0, False
            End If
            Unload FlightDetails
            Unload RecFilter
            Unload FlightExtract
            myIniSection = chkImpTyp(Index).Tag
            tmpData = UCase(GetIniEntry(myIniFullName, myIniSection, "", "ACTION_TYPE", ""))
            If tmpData <> "" Then
                ImportActionType = GetItem(tmpData, 1, ",")
                tmpStrg = GetItem(tmpData, 2, ",")
                If tmpStrg <> "" Then myIniSection = tmpStrg
            End If
            CheckFilePeriod = UCase(GetIniEntry(myIniFullName, myIniSection, "", "CHECK_FILE_PERIOD", "NO"))
            DataSystemMode = UCase(GetIniEntry(myIniFullName, myIniSection, "", "SYSTEM_MODE", ""))
            DataSystemType = UCase(GetIniEntry(myIniFullName, myIniSection, "", "SYSTEM_TYPE", ""))
            DataDisplayType = DataSystemType
            Select Case DataDisplayType
                Case "OAFOS"
                    DataSystemAlc3 = UCase(GetIniEntry(myIniFullName, myIniSection, "", "SYSTEM_ALC3", "OAL"))
                Case "A3FOS"
                    DataSystemAlc3 = UCase(GetIniEntry(myIniFullName, myIniSection, "", "SYSTEM_ALC3", "AEE"))
                    DataSystemType = "OAFOS"
                Case Else
            End Select
            'If DataSystemType <> "FREE" Then
            '    chkAppl(7).Enabled = False
            'Else
            '    chkAppl(7).Enabled = True
            'End If
            tmpData = GetIniEntry(myIniFullName, myIniSection, "", "IMPORT_BOTH_ACT", "NO")
            If tmpData = "YES" Then ImportAct3Act5 = True Else ImportAct3Act5 = False
            tmpData = GetIniEntry(myIniFullName, myIniSection, "", "USE_FKEY_TIME", "YES")
            If tmpData = "YES" Then UseFkeyTime = True Else UseFkeyTime = False
            tmpData = GetIniEntry(myIniFullName, myIniSection, "", "SHRINK_NEW_DEL", "YES")
            If tmpData = "YES" Then ShrinkNewDel = True Else ShrinkNewDel = False
            tmpData = GetIniEntry(myIniFullName, myIniSection, "", "FILE_READER", "NO")
            If tmpData = "YES" Then UseFileReader = True Else UseFileReader = False
            PreCheckDoubles = GetIniEntry(myIniFullName, myIniSection, "", "PRE_CHECK_DOUBLES", "NO")
            TranslateIcao = GetIniEntry(myIniFullName, myIniSection, "", "TRANSLATE_ICAO", "")
            TranslateApcList = GetIniEntry(myIniFullName, myIniSection, "", "TRANSLATE_AIRP", "")
            tmpData = GetIniEntry(myIniFullName, myIniSection, "", "LAYOUT_TYPE", "HARDCODE")
            If (tmpData = "SATS") Or (tmpData = "UFIS45") Then
                If tmpData = "SATS" Then
                    UseRoutField = True
                    UseFldaField = False
                Else
                    UseRoutField = False
                    UseFldaField = False
                End If
                NewLayoutType = True
                tmpData = GetIniEntry(myIniFullName, myIniSection, "", "DATA_MODEL", "")
                Select Case tmpData
                    Case "DAILY_FLIGHT"
                        DefTimePeriod = "SKED,SKED"
                        DefTimeChain = ""
                        DefAdidInfo = "ADID"
                    Case "FLIGHT"
                        DefTimePeriod = "VPFR,VPTO,FREQ,FREW,STOD,STOA,DAOF,OVNI"
                        DefTimeChain = "VPFR,STOD,STOA,DAOF,OVNI"
                        DefAdidInfo = ""
                    Case "ROTATION"
                        DefTimePeriod = "VPFR,VPTO,FREQ,FREW,STOA,STOD,DAOF,OVNI"
                        DefTimeChain = "VPFR,STOA,STOD,DAOF,OVNI"
                        DefAdidInfo = "FLTA,FLTD"
                        DefViaCheck = "ORG3,VSA3,VSA3,DES3,VSD3,VSD3,ORG4,VSA4,VSA4,DES4,VSD4,VSD4"
                    Case Else
                        DefTimePeriod = ""
                        DefTimeChain = ""
                        DefAdidInfo = ""
                        DefViaCheck = ""
                End Select
            Else
                NewLayoutType = False
            End If
            
            tmpData = GetIniEntry(myIniFullName, myIniSection, "", "IMPLICIT_OVN", "YES")
            Select Case tmpData
                Case "NO"
                    ImpOvnFlag = 1
                Case "WARN"
                    ImpOvnFlag = 2
                Case Else
                    ImpOvnFlag = 0
            End Select
            
            MainStypField = UCase(GetIniEntry(myIniFullName, "MAIN", "", "SERVICE_TYPE", "STYP"))
            UseStypField = GetIniEntry(myIniFullName, myIniSection, "", "FILE_SERVICE_TYPE", MainStypField)
            AutoFileTtyp = GetIniEntry(myIniFullName, myIniSection, "", "AUTO_FILE_TTYP", "")
            tmpData = GetIniEntry(myIniFullName, myIniSection, "", "DISPO_FILE_TTYP", "")
            If tmpData <> "" Then
                chkDispo.Caption = tmpData
                chkDispo.Tag = tmpData
                chkDispo.Enabled = True
            Else
                chkDispo.Caption = "Dispo"
                chkDispo.Enabled = False
                chkDispo.Tag = tmpData
            End If
            WinZipPath = GetIniEntry(myIniFullName, "MAIN", "", "WINZIP", "")
            myLoadPath = GetIniEntry(myIniFullName, myIniSection, "", "FILE_PATH", "")
            myLoadName = GetIniEntry(myIniFullName, myIniSection, "", "FILE_NAME", "")
            UsedTimeType = Left(GetIniEntry(myIniFullName, myIniSection, "", "TIME_TYPE", "UTC"), 3)
            MainHeaderTitle = GetIniEntry(myIniFullName, myIniSection, "", "MAIN_HEADER_TITLE", "")
            MainHeaderWidth = GetIniEntry(myIniFullName, myIniSection, "", "MAIN_HEADER_WIDTH", "")
            MainHeaderColor = GetIniEntry(myIniFullName, myIniSection, "", "MAIN_HEADER_COLOR", "")
            If MainHeaderTitle <> "" Then
                MainHeaderWidth = "3," & MainHeaderWidth & ",4"
                MainHeaderTitle = "Status," & MainHeaderTitle & ",System"
                If MainHeaderColor <> "" Then MainHeaderColor = "12632256," & MainHeaderColor & ",12632256"
            End If
            If (Not RestoreAction) And (Not SectionPushed) Then
                chkRequest.Tag = myLoadPath & "\" & myLoadName
                chkRequest.ToolTipText = chkRequest.Tag
            End If
            If DataSystemType = "FREE" Then
                Load CfgTool
                'CfgTool.Show
                'CfgTool.Refresh
                Screen.MousePointer = 11
                tmpData = GetIniEntry(myIniFullName, myIniSection, "", "CONFIG_FILE", "???????")
                CfgTool.InitFreeConfig tmpData
                InitFreeConfigData
                Screen.MousePointer = 0
            Else
                CurFileFilter = GetIniEntry(myIniFullName, myIniSection, "", "FILE_FILTER", "ASCII Files|*.txt|Excel Export|*.csv|All|*.*")
                tmpItmDat = GetIniEntry(myIniFullName, myIniSection, "", "FILTER_INDEX", "1,1")
                'tmpCurDate = Format(Now, "yyyymmdd")
                GetDefaultSeason True
                tmpData = CurSeasData
                'tmpData = SeasonData.GetValidSeason(tmpCurDate)
                'CurSeasData = tmpData
                'CurSeasName = GetItem(tmpData, 1, ",")
                'CurSeasVpfr = GetItem(tmpData, 2, ",")
                'CurSeasVpto = GetItem(tmpData, 3, ",")
                'ActSeason(0).Caption = CurSeasName
                'ActSeason(1).Caption = CurSeasVpfr
                'ActSeason(2).Caption = CurSeasVpto
                CurFilterIndex = 0
                If Left(tmpData, 1) = "W" Then CurFilterIndex = Val(GetItem(tmpItmDat, 1, ","))
                If Left(tmpData, 1) = "S" Then CurFilterIndex = Val(GetItem(tmpItmDat, 2, ","))
                If CurFilterIndex < 1 Then CurFilterIndex = 1
                CurMultiFiles = GetIniEntry(myIniFullName, myIniSection, "", "MULTI_FILES", "")
                tmpData = GetIniEntry(myIniFullName, myIniSection, "", "FORMAT_TITLE", MainTitle)
                If (Not RestoreAction) And (tmpData <> "") Then MainTitle = tmpData
                FormatType = GetIniEntry(myIniFullName, myIniSection, "", "FORMAT_TYPE", "FIXED")
                ItemSep = GetItem(FormatType, 2, ",")
                If ItemSep = "SEMICOLON" Then ItemSep = ";" 'we don't use commas inside this appl
                If ItemSep = "COMMA" Then ItemSep = ";" 'we don't use commas inside this appl
                FormatType = GetItem(FormatType, 1, ",")
                If Not RestoreAction Then Me.Caption = chkImpTyp(Index).Caption & " " & MainTitle
                MainFieldList = GetIniEntry(myIniFullName, myIniSection, "", "MAIN_FIELDS", "")
                If Not DoubleGridInit Then
                    HeadFieldList = GetIniEntry(myIniFullName, myIniSection, "", "FIELD_LIST", "")
                End If
                NormalHeader = GetIniEntry(myIniFullName, myIniSection, "", "HEAD_LINE", "")
                MainExpandHeader = GetIniEntry(myIniFullName, myIniSection, "", "FINAL_HEAD", NormalHeader)
                FldPosLst = GetIniEntry(myIniFullName, myIniSection, "", "FIELD_POS", "ALL")
                ExpPosLst = GetIniEntry(myIniFullName, myIniSection, "", "FINAL_POS", FldPosLst)
                FldLenLst = GetIniEntry(myIniFullName, myIniSection, "", "FIELD_LEN", "")
                MainFldWidLst = GetIniEntry(myIniFullName, myIniSection, "", "FINAL_LEN", FldLenLst)
                
                UniqueLineKey = GetIniEntry(myIniFullName, myIniSection, "", "UNIQUE_LINE_KEY", "AUTO")
                UniqueKeyFormat = GetItem(UniqueLineKey, 1, ",")
                UniqueKeyPos = Val(GetItem(UniqueLineKey, 2, ","))
                UniqueKeyLen = Val(GetItem(UniqueLineKey, 3, ","))
                
                FldFrmLst = GetIniEntry(myIniFullName, myIniSection, "", "FIELD_FRM", "")
                FldAddLst = GetIniEntry(myIniFullName, myIniSection, "", "FIELD_ADD", "")
                FlnoField = Val(GetIniEntry(myIniFullName, myIniSection, "", "FLNO_FIELD", "0"))
                tmpData = GetIniEntry(myIniFullName, myIniSection, "", "LINE_FILTER", "0,0,,NONE")
                tmpData = Replace(tmpData, "[HOPO]", HomeAirport, 1, -1, vbBinaryCompare)
                LineFilterTyp = GetItem(tmpData, 1, ",")
                LineFilterPos = Val(GetItem(tmpData, 2, ","))
                LineFilterLen = Val(GetItem(tmpData, 3, ","))
                LineFilterText = GetItem(tmpData, 4, ",")
                tmpData = GetIniEntry(myIniFullName, myIniSection, "", "LINE_WRAP", "")
                If tmpData = "YES" Then LineWrap = True Else LineWrap = False
                ChkBasDat = GetIniEntry(myIniFullName, myIniSection, "", "CHECK_BAS", "")
                ChkBasDat = Replace(ChkBasDat, "[HOPO]", HomeAirport, 1, -1, vbBinaryCompare)
                AutoCheck = GetIniEntry(myIniFullName, myIniSection, "", "AUTO_CHECK", "")
                AutoReplaceAll = GetIniEntry(myIniFullName, myIniSection, "", "REPLACE_ALL", "")
                tmpData = GetIniEntry(myIniFullName, myIniSection, "", "CHECK_LEG", "0,0,0")
                LegColNbr = Val(GetItem(tmpData, 1, ",")) + 2
                OrgColNbr = Val(GetItem(tmpData, 2, ",")) + 2
                DesColNbr = Val(GetItem(tmpData, 3, ",")) + 2
                DefineDateAndTimeFields
                ClearZeros = "," & GetIniEntry(myIniFullName, myIniSection, "", "CLEAR_ZEROS", "") & ","
                ClearZeros = Replace(ClearZeros, " ", "", 1, -1, vbBinaryCompare)
                ClearValues = "," & GetIniEntry(myIniFullName, myIniSection, "", "CLEAR_VALUE", "") & ","
                ClearValues = Replace(ClearValues, " ", "", 1, -1, vbBinaryCompare)
                ClearValues = Replace(ClearValues, "|", ",|,", 1, -1, vbBinaryCompare)
                ClearByValue = GetIniEntry(myIniFullName, myIniSection, "", "CLEAR_BYVAL", "")
                ClearByValue = Replace(ClearByValue, " ", "", 1, -1, vbBinaryCompare)
                If NewLayoutType Then
                    TimeChainInfo = GetIniEntry(myIniFullName, myIniSection, "", "TIME_CHAIN", DefTimeChain)
                    TimeChainInfo = TranslateItemList(TimeChainInfo, MainFieldList, True)
                    MainPeriodCols = GetIniEntry(myIniFullName, myIniSection, "", "TIME_PERIOD", DefTimePeriod)
                    MainPeriodCols = TranslateItemList(MainPeriodCols, MainFieldList, True)
                    CheckViaList = GetIniEntry(myIniFullName, myIniSection, "", "CHECK_VIAS", DefViaCheck)
                    CheckViaList = TranslateItemList(CheckViaList, MainFieldList, True)
                Else
                    TimeChainInfo = GetIniEntry(myIniFullName, myIniSection, "", "TIME_CHAIN", "")
                    MainPeriodCols = GetIniEntry(myIniFullName, myIniSection, "", "TIME_PERIOD", "0,0,0,0")
                    CheckViaList = GetIniEntry(myIniFullName, myIniSection, "", "CHECK_VIAS", "")
                End If
                InitPeriodCheck MainPeriodCols, True
                IgnoreEmpty = GetIniEntry(myIniFullName, myIniSection, "", "IGNORE_EMP", "")
                ShrinkList = GetIniEntry(myIniFullName, myIniSection, "", "LINE_SHRINK", "")
                AftFldLst = GetIniEntry(myIniFullName, myIniSection, "", "AFT_FIELDS", "")
                ExtFldLst = GetIniEntry(myIniFullName, myIniSection, "", "EXT_FIELDS", "")
                DefaultFtyp = GetIniEntry(myIniFullName, myIniSection, "", "DEFAULT_FTYP", "S")
                UseVialFlag = GetIniEntry(myIniFullName, myIniSection, "", "SET_VIA_FLAG", "NO")
                OutputType = GetIniEntry(myIniFullName, myIniSection, "", "OUTPUT_TYPE", "LOCAL_FILE")
                ActionType = GetIniEntry(myIniFullName, myIniSection, "", "ACTION_TYPE", "CEDA_IMP")
                CheckRotation = GetIniEntry(myIniFullName, myIniSection, "", "ROTATIONS", DefAdidInfo)
                If NewLayoutType Then
                    If CheckRotation = "ADID" Then
                        CheckRotation = CheckRotation & "," & CStr(GetItemNo(MainFieldList, "ADID"))
                    Else
                        CheckRotation = TranslateItemList(CheckRotation, MainFieldList, True)
                    End If
                End If
                If InStr(MainFieldList, "ROUT") > 0 Then
                    UseRoutField = True
                    UseFldaField = False
                End If
                tmpData = GetIniEntry(myIniFullName, myIniSection, "", "IGNORE_EMPTY", "")
                If tmpData = "YES" Then IgnoreEmptyLine = True Else IgnoreEmptyLine = False
                tmpData = GetIniEntry(myIniFullName, myIniSection, "", "COL_SELECT", "")
                If tmpData = "YES" Then SetColSelection = True Else SetColSelection = False
                ColSelectList = GetIniEntry(myIniFullName, myIniSection, "", "SELECT_COL", "")
                If ColSelectList <> "" Then SetColSelection = True
                ExtractRotationArr = GetIniEntry(myIniFullName, myIniSection, "", "EXTRACT_RFA", "")
                ExtractRotationDep = GetIniEntry(myIniFullName, myIniSection, "", "EXTRACT_RFD", "")
                ExtractArrivals = GetIniEntry(myIniFullName, myIniSection, "", "EXTRACT_ARR", "")
                ExtractDepartures = GetIniEntry(myIniFullName, myIniSection, "", "EXTRACT_DEP", "")
                ExtractSlotLine = GetIniEntry(myIniFullName, myIniSection, "", "EXTRACT_DAT", "")
                ExtractSystemType = 0
                If ExtractSlotLine <> "" Then ExtractSystemType = 1
                If ExtractArrivals <> "" Then ExtractSystemType = 2
                If ExtractRotationArr <> "" Then ExtractSystemType = 3
            End If
            If (Not RestoreAction) And (Not SectionPushed) Then
                If (Not DoubleGridInit) Then
                    tmpData = GetIniEntry(myIniFullName, myIniSection, "", "AUTO_OPEN", "NO")
                    If tmpData = "NO" Then
                        If (ImportActionType = "") Or (InStr("OAFOS,A3FOS", DataSystemType) > 0) Then
                            chkRequest.Enabled = True
                            chkRequest.Value = 1
                        Else
                            myLoadName = ImportActionType
                            ResetMain 0, False
                            ResetMain 0, True
                            chkExpand.Value = 1
                            MainDoIt = True
                        End If
                    Else
                        ResetMain 0, True
                        chkRequest.Enabled = False
                        chkExpand.Value = 1
                    End If
                Else
                End If
                'We must jump out here when ACDM Manager is in use
                If Not AcdmManagerIsUsed Then
                    DoTheRestOfIt Index
                End If
'                If myLoadName <> "" Then
'                    chkBackup.Enabled = True
'                    chkImpTyp(Index).BackColor = LightGreen
'                    chkTimes(0).Tag = ""
'                    chkTimes(1).Tag = ""
'                    chkTimes(0).Value = 0
'                    chkTimes(1).Value = 0
'                    chkTimes(0).Enabled = False
'                    chkTimes(1).Enabled = False
'                    Select Case UsedTimeType
'                        Case "UTC"
'                            'chkTimes(0).Value = 1
'                            'chkTimes(1).Value = 0
'                        Case "LOC"
'                            chkTimes(0).Value = 0
'                            chkTimes(1).Value = 1
'                            chkTimes(0).Enabled = True
'                            chkTimes(1).Enabled = True
'                            chkTimes(0).Tag = "UT"
'                            chkTimes(1).Tag = "LC"
'                        Case Else
'                    End Select
'                Else
'                    If Not DoubleGridInit Then
'                        chkImpTyp(Index).Value = 0
'                    Else
'                        'chkExpand.Value = 1
'                        MainDoIt = True
'                    End If
'                End If
'                If Not MainDoIt Then
'                    chkImpTyp(Index).Value = 0
'                    PushSectionDataGrid False
'                Else
'                    If (Not DoubleGridInit) Then
'                        If Not SectionPushed Then
'                            tmpData = ""
'                            If tmpData = "" Then
'                                tmpData = GetIniEntry(myIniFullName, myIniSection, "", "TRANSFER_TO", "")
'                                If tmpData <> "" Then
'                                    PushSectionDataGrid False
'                                    chkIdentify.Value = 1
'                                    i = GetImportSection(tmpData)
'                                    If i >= 0 Then
'                                        AutoPushIndex = i
'                                        tmpData = CStr(i)
'                                        Timer2.Tag = "DBL," & tmpData
'                                        Timer2.Enabled = True
'                                    End If
'                                End If
'                            End If
'                            If tmpData = "" Then
'                                tmpData = GetIniEntry(myIniFullName, myIniSection, "", "PUSH_SECTION", "")
'                                If tmpData <> "" Then
'                                    chkIdentify.Value = 1
'                                    'PushedFilePath = myLoadName
'                                    i = GetImportSection(tmpData)
'                                    If i >= 0 Then
'                                        AutoPushIndex = i
'                                        PushSectionDataGrid True
'                                        tmpData = "PSH," & CStr(i)
'                                        Timer2.Tag = tmpData
'                                        Timer2.Enabled = True
'                                    End If
'                                End If
'                            End If
'                            If tmpData = "" Then
'                                'Just a normal session activated
'                                AutoPushIndex = -1
'                                PushSectionDataGrid False
'                            End If
'                        Else
'                            'We never come here
'                            'chkRequest.Tag = PushedFilePath
'                            'myLoadName = PushedFilePath
'                            'chkExpand.Value = 1
'                        End If
'                    Else
'                        'Here we open the DoubleGrid import file again
'                        chkExpand.Value = 1
'                    End If
'                End If
            ElseIf SectionPushed Then
                SetGridButtons True, 2
                chkExpand.Value = 1
                ReleaseMain 0, True
                chkImpTyp(Index).BackColor = LightGreen
                CreateIndexes True
            End If
            'Finally all data grids are published now
            '----------------------------------------
            'CreateIndexes True
            '----------------------------------------
        Else
            chkImpTyp(Index).BackColor = vbButtonFace
            chkRequest.Visible = True
        End If
        Me.Refresh
    End If
End Sub
Private Sub DoTheRestOfIt(Index As Integer)
    Dim tmpData As String
    Dim i As Integer
                If myLoadName <> "" Then
                    chkBackup.Enabled = True
                    chkImpTyp(Index).BackColor = LightGreen
                    chkTimes(0).Tag = ""
                    chkTimes(1).Tag = ""
                    chkTimes(0).Value = 0
                    chkTimes(1).Value = 0
                    chkTimes(0).Enabled = False
                    chkTimes(1).Enabled = False
                    Select Case UsedTimeType
                        Case "UTC"
                            'chkTimes(0).Value = 1
                            'chkTimes(1).Value = 0
                        Case "LOC"
                            chkTimes(0).Value = 0
                            chkTimes(1).Value = 1
                            chkTimes(0).Enabled = True
                            chkTimes(1).Enabled = True
                            chkTimes(0).Tag = "UT"
                            chkTimes(1).Tag = "LC"
                        Case Else
                    End Select
                Else
                    If Not DoubleGridInit Then
                        chkImpTyp(Index).Value = 0
                    Else
                        'chkExpand.Value = 1
                        MainDoIt = True
                    End If
                End If
                If Not MainDoIt Then
                    chkImpTyp(Index).Value = 0
                    PushSectionDataGrid False
                Else
                    If (Not DoubleGridInit) Then
                        If Not SectionPushed Then
                            tmpData = ""
                            If tmpData = "" Then
                                tmpData = GetIniEntry(myIniFullName, myIniSection, "", "TRANSFER_TO", "")
                                If tmpData <> "" Then
                                    PushSectionDataGrid False
                                    chkIdentify.Value = 1
                                    i = GetImportSection(tmpData)
                                    If i >= 0 Then
                                        AutoPushIndex = i
                                        tmpData = CStr(i)
                                        Timer2.Tag = "DBL," & tmpData
                                        Timer2.Enabled = True
                                    End If
                                End If
                            End If
                            If tmpData = "" Then
                                tmpData = GetIniEntry(myIniFullName, myIniSection, "", "PUSH_SECTION", "")
                                If tmpData <> "" Then
                                    chkIdentify.Value = 1
                                    'PushedFilePath = myLoadName
                                    i = GetImportSection(tmpData)
                                    If i >= 0 Then
                                        AutoPushIndex = i
                                        PushSectionDataGrid True
                                        tmpData = "PSH," & CStr(i)
                                        Timer2.Tag = tmpData
                                        Timer2.Enabled = True
                                    End If
                                End If
                            End If
                            If tmpData = "" Then
                                'Just a normal session activated
                                AutoPushIndex = -1
                                PushSectionDataGrid False
                            End If
                        Else
                            'We never come here
                            'chkRequest.Tag = PushedFilePath
                            'myLoadName = PushedFilePath
                            'chkExpand.Value = 1
                        End If
                    Else
                        'Here we open the DoubleGrid import file again
                        chkExpand.Value = 1
                    End If
                End If

            'Finally all data grids are published now
            '----------------------------------------
            CreateIndexes True
            '----------------------------------------
            If AcdmManagerIsUsed Then
                chkAppl(7).Enabled = True
            Else
                chkAppl(7).Enabled = False
            End If
            
        Me.Refresh
End Sub
Public Sub CreateIndexes(DoIt As Boolean)
    Dim KeyCol As Long
    If DoIt Then
        KeyCol = FileData(0).GetColumnCount - 1
        FileData(0).IndexCreate "LKEY", KeyCol
        If FileData.Count > 1 Then
            KeyCol = FileData(1).GetColumnCount - 1
            FileData(1).IndexCreate "LKEY", KeyCol
        End If
        If Tab2IsUsed Then
            KeyCol = TAB2.GetColumnCount - 1
            TAB2.IndexCreate "LKEY", KeyCol
        End If
    Else
        FileData(0).IndexDestroy "LKEY"
        If FileData.Count > 1 Then
            FileData(1).IndexDestroy "LKEY"
        End If
        If Tab2IsUsed Then
            TAB2.IndexDestroy "LKEY"
        End If
    End If
End Sub
Private Sub PushSectionDataGrid(PushIt As Boolean)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim LineData As String
    Dim SysDat As String
    Dim CurCell As String
    Dim BackColor As Long
    Dim ForeColor As Long
    Dim SysCol As Long
    Dim CurCol As Long
    Dim tabCols As Integer
    Dim datCols As Integer
    Dim itm As Integer
    If PushIt Then
        SysCol = CLng(GetRealItemNo(FileData(0).HeaderString, "System Cells"))
        Tab2IsUsed = True
        CloneGridLayout TAB2, FileData(0)
        LineData = TAB2.HeaderString
        Mid(LineData, 1, 1) = "T"
        TAB2.HeaderString = LineData
        TAB2.CreateDecorationObject "CellError", "R,T,B,L", "2,2,2,2", Str(vbBlack) & "," & Str(LightGray) & "," & Str(vbBlack) & "," & Str(LightGray)
        tabCols = CInt(TAB2.GetColumnCount)
        MaxLine = FileData(0).GetLineCount - 1
        For CurLine = 0 To MaxLine
            LineData = FileData(0).GetLineValues(CurLine)
            datCols = CInt(ItemCount(LineData, ","))
            If datCols <> tabCols Then
                itm = 1
            End If
            TAB2.InsertTextLine LineData, False
            FileData(0).GetLineColor CurLine, ForeColor, BackColor
            TAB2.SetLineColor CurLine, ForeColor, BackColor
            If SysCol >= 0 Then
                SysDat = Trim(FileData(0).GetColumnValue(CurLine, SysCol))
                If SysDat <> "" Then
                    itm = 0
                    CurCell = "START"
                    While CurCell <> ""
                        itm = itm + 1
                        CurCell = GetItem(SysDat, itm, "/")
                        If CurCell <> "" Then
                            CurCol = Val(CurCell)
                            TAB2.SetDecorationObject CurLine, CurCol, "CellError"
                        End If
                    Wend
                End If
            End If
        Next
        TAB2.RedrawTab
        TAB2.AutoSizeColumns
        TAB2.Refresh
        TAB2.Visible = True
        TAB2.ZOrder
        SetGridButtons True, 2
    Else
        TAB2.ResetContent
        TAB2.Visible = False
        Tab2IsUsed = False
        SetGridButtons False, -1
    End If
End Sub
Private Sub DefineDateAndTimeFields()
    DateFieldFormat = GetIniEntry(myIniFullName, myIniSection, "", "DATE_FORMAT", "")
    If MainFieldList = "" Then
        DateFields = "," & GetIniEntry(myIniFullName, myIniSection, "", "DATE_FIELDS", "") & ","
        DateFields = Replace(DateFields, " ", "", 1, -1, vbBinaryCompare)
        TimeFields = "," & GetIniEntry(myIniFullName, myIniSection, "", "TIME_FIELDS", "") & ","
        TimeFields = Replace(TimeFields, " ", "", 1, -1, vbBinaryCompare)
    Else
        CreateTypeList DateFields, MainFieldList, "DATE", 0
        DateFields = "," & DateFields & ","
        CreateTypeList TimeFields, MainFieldList, "DATI", 0
        TimeFields = "," & TimeFields & ","
    End If
End Sub
Private Sub ResetChkImpTyp(WithOut As Integer)
    Dim i As Integer
    Dim n As Integer
    If Not DoubleGridInit Then
        n = chkImpTyp.UBound
        For i = 0 To n
            If i <> WithOut Then chkImpTyp(i).Value = 0
        Next
    End If
End Sub

Private Sub chkReplace_Click()
Dim tmpLine
Dim tmpColNbr As String
Dim tmpColNo As Long
Dim RefCol As Long
Dim RefTag As String
Dim RefTxt As String
Dim CodeTxt As String
Dim ColList As String
Dim DataList As String
Dim tmpColLst As String
Dim clLineIdx As String
Dim llLine As Long
Dim LinItm As Integer
Dim tmpData As String
Dim tmpOldData As String
Dim tmpLval As String
Dim tmpRval As String
Dim ReqMsg As String
Dim UsrAnsw As Integer
Dim RefAnsw As Integer
Dim RetVal As Integer
Dim CedaData As String
Dim tmpResult As String
Dim tmpSqlKey As String
Dim RefContext As String
Dim ColFld As String

    If chkReplace.Value = 1 Then
        chkReplace.BackColor = LightestGreen
        chkReplace.Refresh
        Me.Refresh
        MainDialog.MousePointer = 11
        'MsgBox chkEdit.Tag
        tmpLine = GetItem(chkEdit.Tag, 1, ",")
        tmpColNbr = GetItem(chkEdit.Tag, 2, ",")
        tmpColNo = Val(tmpColNbr)
        If tmpLine <> "" And tmpColNbr <> "" And Trim(txtEdit.Text) <> Trim(txtEdit.Tag) Then
            llLine = Val(tmpLine)
            FileData(CurFdIdx).SetColumnValue llLine, tmpColNo, Trim(txtEdit.Text)
            ColFld = GetRealItem(MainFieldList, (tmpColNo - 3), ",")
            If ColFld <> "" Then
                If InStr(TranslateIcao, ColFld) > 0 Then TranslateIcaoCodes llLine, ColFld
                If InStr(TranslateApcList, ColFld) > 0 Then TranslateAirportCodes llLine, ColFld
            End If
            ClearErrorStatus llLine, llLine
            PerformChecks llLine, llLine
            If chkAll.Value = 1 Then
                RefTag = chkAll.Tag
                CodeTxt = GetItem(RefTag, 1, "/")
                RefAnsw = 0
                If InStr(RefTag, "/") > 0 Then
                    RefCol = Val(GetItem(RefTag, 2, "/")) + 2
                    ColList = Trim(Str(RefCol)) & "," & tmpColNbr
                    RefTxt = Trim(FileData(CurFdIdx).GetColumnValue(tmpLine, RefCol))
                    DataList = RefTxt & "," & txtEdit.Tag
                    RefContext = GetItem(RefTag, 3, "/")
                    If RefContext = "" Then RefContext = "Airline"
                    ReqMsg = ""
                    ReqMsg = ReqMsg & "Do you want to perform this " & vbNewLine
                    ReqMsg = ReqMsg & "global change based on" & vbNewLine & vbNewLine
                    ReqMsg = ReqMsg & RefContext & " " & RefTxt & " only ?" & vbNewLine
                    RefAnsw = MyMsgBox.CallAskUser(0, 0, 0, "Perform Auto Replace All", ReqMsg, "ask", "Yes;F,No", UserAnswer)
                End If
                If RefAnsw = 1 Then
                    tmpColLst = FileData(CurFdIdx).GetLinesByMultipleColumnValue(ColList, DataList, 0)
                Else
                    tmpColLst = FileData(CurFdIdx).GetLinesByColumnValue(tmpColNo, txtEdit.Tag, 0)
                End If
                'MsgBox tmpColLst
                clLineIdx = "START"
                LinItm = 0
                While clLineIdx <> ""
                    LinItm = LinItm + 1
                    clLineIdx = GetItem(tmpColLst, LinItm, ",")
                    If clLineIdx <> "" Then
                        llLine = Val(clLineIdx)
                        FileData(CurFdIdx).SetColumnValue llLine, tmpColNo, Trim(txtEdit.Text)
                        'ClearErrorStatus llLine, llLine
                        'PerformChecks llLine, llLine
                    End If
                Wend
                ReqMsg = ""
                ReqMsg = ReqMsg & "Do you want to make this " & vbNewLine
                ReqMsg = ReqMsg & "global change permanent ?" & vbNewLine & vbNewLine
                ReqMsg = ReqMsg & "TYPE:" & vbTab & DataSystemType & vbNewLine
                ReqMsg = ReqMsg & "CODE:" & vbTab & CodeTxt & vbNewLine & vbNewLine
                ReqMsg = ReqMsg & "TEXT:" & vbTab & txtEdit.Tag & vbNewLine
                ReqMsg = ReqMsg & "REPL:" & vbTab & txtEdit.Text & vbNewLine
                If RefAnsw = 1 Then ReqMsg = ReqMsg & vbNewLine & "(" & RefContext & " " & RefTxt & " only.)" & vbNewLine
                If MyMsgBox.CallAskUser(0, 0, 0, "Check Auto Replace All", ReqMsg, "ask", "Yes;F,No", UserAnswer) = 1 Then
                    If RefAnsw = 1 Then
                        tmpLval = Trim(txtEdit.Tag) & "/" & RefTxt
                        tmpRval = Trim(txtEdit.Text) & "/" & RefTxt
                    Else
                        tmpLval = Trim(txtEdit.Tag)
                        tmpRval = Trim(txtEdit.Text)
                    End If
                    tmpSqlKey = "WHERE TYPE='" & DataSystemType & "' AND LKEY='" & CodeTxt & "' AND (LVAL='" & tmpLval & "' OR RVAL='" & tmpRval & "')"
                    RetVal = UfisServer.CallCeda(tmpResult, "DRT", "GDLTAB", "", "", tmpSqlKey, "", 1, False, False)
                    tmpSqlKey = "            "
                    CedaData = ""
                    CedaData = CedaData & DataSystemType & "," & CodeTxt
                    CedaData = CedaData & "," & tmpLval & "," & tmpRval
                    RetVal = UfisServer.CallCeda(tmpResult, "IBT", "GDLTAB", "TYPE,LKEY,LVAL,RVAL", CedaData, tmpSqlKey, "", 1, False, False)
                    If MyMsgBox.CallAskUser(0, 0, 0, "Check Auto Replace All", "Made permanent!", "hand", "", UserAnswer) = 1 Then DoNothing
                End If
            End If
            tmpOldData = "'" & txtEdit.Tag & "'"
            txtEdit.Tag = Trim(txtEdit.Text)
            txtEdit.BackColor = vbWhite
            tmpData = "'" & txtEdit.Tag & "'"
            chkAll.ToolTipText = Replace(chkAll.ToolTipText, tmpOldData, tmpData, 1, 1, vbBinaryCompare)
            PerformIdentify
        End If
        FileData(CurFdIdx).RedrawTab
        MainDialog.MousePointer = 0
        chkAll.Value = 0
        chkReplace.Value = 0
    Else
        chkReplace.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkRequest_Click()
    Dim ListOfFiles As String
    Dim ActFile As String
    Dim ActPath As String
    Dim tmpAnsw As Integer
    Dim SelFilterItem As Integer
    Dim SelFilterIndex As Integer
    Dim SelFilterText As String
    Dim FileCount As Integer
    Dim UseAcdm As Boolean
    If (Not Initializing) And (Not RestoreAction) Then
        If chkRequest.Value = 1 Then
            TabFrame.Enabled = False
            chkRequest.BackColor = LightestGreen
            If chkRequest.Tag = "" Then chkRequest.Tag = CurLoadPath & "\" & CurLoadName
            FileCount = GetFileAndPath(chkRequest.Tag, myLoadName, myLoadPath)
            UfisTools.CommonDialog.DialogTitle = "Open import file of: " & DataDisplayType
            UfisTools.CommonDialog.InitDir = myLoadPath
            UfisTools.CommonDialog.FileName = myLoadName
            UfisTools.CommonDialog.Filter = CurFileFilter
            UfisTools.CommonDialog.FilterIndex = CurFilterIndex
            If InStr(CurMultiFiles, "Y") > 0 Then
                UfisTools.CommonDialog.Flags = (cdlOFNAllowMultiselect Or cdlOFNHideReadOnly)
            Else
                UfisTools.CommonDialog.Flags = (cdlOFNHideReadOnly)
            End If
            UfisTools.CommonDialog.CancelError = True
            On Error GoTo ErrorHandle
            UfisTools.CommonDialog.ShowOpen
            If UfisTools.CommonDialog.FileName <> "" Then
                ListOfFiles = Replace(UfisTools.CommonDialog.FileName, Chr(34), "", 1, -1, vbBinaryCompare)
                SelFilterIndex = UfisTools.CommonDialog.FilterIndex
                SelFilterItem = ((SelFilterIndex - 1) * 2) + 1
                SelFilterText = GetItem(CurFileFilter, SelFilterItem, "|")
                tmpAnsw = 1
                If (ListOfFiles = chkRequest.Tag) And (chkExpand.Value = 1) Then
                    tmpAnsw = MyMsgBox.CallAskUser(0, 0, 0, "Import File Control", "Do you want to reopen the file?", "ask", "Yes,No;F", UserAnswer)
                End If
                FileCount = GetFileAndPath(ListOfFiles, ActFile, ActPath)
                If FileCount > 1 Then
                    If GetItem(CurMultiFiles, SelFilterIndex, ",") <> "Y" Then
                        tmpAnsw = MyMsgBox.CallAskUser(0, 0, 0, (SelFilterText & " File Control"), "Too many files! Please select one file only!", "stop", "", UserAnswer)
                        tmpAnsw = -1
                    End If
                End If
                If tmpAnsw = 1 Then
                    chkRequest.Tag = ListOfFiles
                    chkRequest.ToolTipText = chkRequest.Tag
                    If UCase(GetItem(ActFile, 2, ".")) = "ZIP" Then
                        If WinZipPath <> "" Then
                            chkRequest.Value = 0
                            WinZipId = Shell(WinZipPath & " " & ListOfFiles, vbNormalFocus)
                            AppActivate "WinZip", True
                        End If
                    Else
                        chkRequest.Value = 0
                        TabFrame.Enabled = True
                        Refresh
                        ResetMain 0, False
                        ResetMain 0, True
                        If Not TestMode Then
                            UseAcdm = CheckAcdmManagerUse
                            If Not UseAcdm Then
                                AcdmManagerIsUsed = False
                                chkExpand.Value = 1
                            Else
                                AcdmManagerIsUsed = True
                                ConnectToAcdmManager
                            End If
                        Else
                            MySetUp.Show , Me
                            chkLoad.Value = 1
                        End If
                    End If
                Else
                    chkRequest.Value = 0
                    chkRequest.Tag = ""
                    chkRequest.ToolTipText = "No file selected."
                End If
            End If
        Else
            chkRequest.BackColor = vbButtonFace
        End If
    End If
    Exit Sub
ErrorHandle:
    myLoadName = ""
    myLoadPath = ""
    chkRequest.Value = 0
    chkRequest.Enabled = False
    Exit Sub
End Sub
Private Function CheckAcdmManagerUse() As Boolean
    Dim IsUsed As Boolean
    Dim CfgData As String
    Dim FileSuffix As String
    Dim FilePrefix As String
    
    Dim TypeCheck As String
    Dim ConfCheck As String
    Dim FileCheck As String
    Dim SaveCheck As String
    Dim PathCheck As String
    
    Dim TypePath As String
    Dim ConfPath As String
    Dim DataPath As String
    Dim SavePath As String
    
    Dim TypeName As String
    Dim ConfName As String
    Dim DataName As String
    Dim SaveName As String
    
    IsUsed = False
    UseImpType = ""
    UseImpConf = ""
    UseImpFile = ""
    UseOutFile = ""
    
    CfgData = Trim(GetIniEntry(myIniFullName, chkImpTyp(PushedImpButton).Tag, "", "ACDM_IMPORT_MANAGER", "NO"))
    If CfgData = "YES" Then
        IsUsed = True
        UseImpType = Trim(GetIniEntry(myIniFullName, chkImpTyp(PushedImpButton).Tag, "", "SCORE_TYPE_TEMPLATE", ""))
        UseImpConf = Trim(GetIniEntry(myIniFullName, chkImpTyp(PushedImpButton).Tag, "", "COHOR_CONF_TEMPLATE", ""))
        FilePrefix = Trim(GetIniEntry(myIniFullName, chkImpTyp(PushedImpButton).Tag, "", "USE_FILENAME_PREFIX", "SCORE"))
        FileSuffix = Trim(GetIniEntry(myIniFullName, chkImpTyp(PushedImpButton).Tag, "", "USE_FILENAME_SUFFIX", "TXT"))
        UseImpFile = chkRequest.Tag
        GetFileAndPath UseImpType, TypeName, TypePath
        GetFileAndPath UseImpConf, ConfName, ConfPath
        GetFileAndPath UseImpFile, DataName, DataPath
        SavePath = Trim(GetIniEntry(myIniFullName, chkImpTyp(PushedImpButton).Tag, "", "USE_RESULT_SAVEPATH", DataPath))
        SaveName = GetItem(DataName, 1, ".")
        UseOutFile = SavePath & "\" & FilePrefix & "_" & SaveName & "." & FileSuffix
    End If
    CheckAcdmManagerUse = IsUsed
End Function
Private Sub ConnectToAcdmManager()
    Dim NewLeft As Long
    'NewLeft = chkImpTyp(0).Left - SlidePanel(0).Width
    'NewLeft = -1
    ShowSlidePanel 0, -1, "OPN", "ACDM"
End Sub
Private Sub chkRotation_Click()
    If chkRotation.Value = 1 Then
        chkRotation.BackColor = LightGreen
        If PushedImpButton >= 0 Then
            StrTabCaller = "IMPTYP_" & CStr(PushedImpButton)
        Else
            StrTabCaller = "MAIN"
        End If
        StartedAsImportTool = True
        ShowStrTabStyle = 0
        Load StandardRotations
        StandardRotations.Show
        'StandardRotations.SetExtractTimeframe
        StandardRotations.InitMyLayout "INIT"

        'StandardRotations.Refresh
    Else
        chkRotation.BackColor = vbButtonFace
    End If
End Sub

Private Sub chkShrink_Click()
    On Error Resume Next
    If Not RestoreAction Then
        If chkShrink.Value = 1 Then
            'If IsDoubleGridView Then chkGrid(0).Value = 1
            ClickButtonChain 6
            If (UpdateMode) And (DataSystemType = "FHK") Then
                chkSort.Value = 1
            Else
                ShrinkOverNightLines
            End If
            ResetErrorCounter
            chkShrink.BackColor = LightestGreen
            chkShrink.Enabled = False
        Else
            chkShrink.Enabled = True
            chkShrink.BackColor = vbButtonFace
        End If
        If TabFrame.Enabled = True Then FileData(CurFdIdx).SetFocus
    End If
End Sub
Private Sub PrepareOverNightShrink()
Dim ShrinkRecsFound
Dim CurLine As Long
Dim MaxLine As Long
Dim ArrLine As Long
Dim DepLine As Long
'Dim tmpline As Long
Dim StoaColIdx As Long
Dim StodColIdx As Long
Dim DaofColIdx As Long
Dim FltnArrCol As Long
Dim FltnDepCol As Long
Dim DaofVal As Integer
Dim GroundDays As Integer
Dim tmpDaof As String
Dim tmpStoa As String
Dim tmpStod As String
Dim CurFlna As String
Dim CurFlnd As String
Dim tmpFlna As String
Dim tmpFlnd As String
Dim tmpData As String
Dim varDepDay As Variant
Dim strDepDay As String
Dim tmpColStat As String
    ShrinkRecsFound = False
    chkShrink.Enabled = ShrinkRecsFound
    chkShrink.Value = 0
    FileData(CurFdIdx).RedrawTab
    If ShrinkList <> "" Then
        StatusBar.Panels(7).Text = "Searching Overnight Rotations ..."
        'hardcoded
        FltnArrCol = 2 + 2
        FltnDepCol = 4 + 2
        StoaColIdx = 12 + 2
        StodColIdx = 13 + 2
        DaofColIdx = 14 + 2
        GroundDays = -1
        DepLine = -1
        MaxLine = FileData(CurFdIdx).GetLineCount - 1
        For CurLine = 0 To MaxLine
            tmpColStat = FileData(CurFdIdx).GetColumnValue(CurLine, 1)
            'If tmpColStat <> "E" Then
                tmpDaof = FileData(CurFdIdx).GetColumnValue(CurLine, DaofColIdx)
                DaofVal = Val(tmpDaof)
                tmpStoa = GetTimePart(FileData(CurFdIdx).GetColumnValue(CurLine, StoaColIdx))
                tmpStod = GetTimePart(FileData(CurFdIdx).GetColumnValue(CurLine, StodColIdx))
                If DaofVal > 0 Then
                    'found a new nightstop arrival
                    If GroundDays >= 0 Then
                        'can't be. Here we have an error
                        SetErrorRange DaofColIdx, ArrLine, CurLine - 1, "OVN?.ALL"
                    End If
                    GroundDays = DaofVal
                    tmpData = Left(FileData(CurFdIdx).GetColumnValue(CurLine, StoaColIdx), 8)
                    varDepDay = CedaDateToVb(tmpData)
                    varDepDay = DateAdd("d", DaofVal, varDepDay)
                    strDepDay = Format(varDepDay, "yyyymmdd")
                    CurFlna = FileData(CurFdIdx).GetColumnValue(CurLine, FltnArrCol)
                    CurFlnd = FileData(CurFdIdx).GetColumnValue(CurLine, FltnDepCol)
                    DepLine = -1
                Else
                    'Here only special values are allowed
                    If Trim(tmpDaof) <> "" Then
                        If (tmpDaof = "X") And (DepLine >= 0) Then
                            SetErrorText Str(DaofColIdx), CurLine, ExtFldCnt, "OVN?.ARR", True, 0
                        End If
                    End If
                End If
                If (GroundDays = DaofVal) And (GroundDays > 0) Then
                    'this is the arrival record
                    FileData(CurFdIdx).SetColumnValue CurLine, 1, "A"
                    FileData(CurFdIdx).SetColumnValue CurLine, 2, "N"
                    'FileData(CurFdIdx).SetColumnValue CurLine, StodColIdx, ""
                    FileData(CurFdIdx).SetLineColor CurLine, LightYellow, vbBlack
                    ArrLine = CurLine
                    ShrinkRecsFound = True
                ElseIf GroundDays > 0 Then
                    'these records will be ignored
                    tmpFlna = FileData(CurFdIdx).GetColumnValue(CurLine, FltnArrCol)
                    tmpFlnd = FileData(CurFdIdx).GetColumnValue(CurLine, FltnDepCol)
                    If (tmpFlna = CurFlna) And (tmpFlnd = CurFlnd) Then
                        FileData(CurFdIdx).SetColumnValue CurLine, 1, "-"
                        FileData(CurFdIdx).SetColumnValue CurLine, 2, "N"
                        FileData(CurFdIdx).SetLineColor CurLine, vbBlack, LightGray
                    Else
                        SetErrorRange DaofColIdx, ArrLine, CurLine - 1, "OVN?.NXT"
                    End If
                ElseIf GroundDays = 0 Then
                    'must be nightstop departure record
                    tmpFlna = FileData(CurFdIdx).GetColumnValue(CurLine, FltnArrCol)
                    tmpFlnd = FileData(CurFdIdx).GetColumnValue(CurLine, FltnDepCol)
                    If (tmpFlna = CurFlna) And (tmpFlnd = CurFlnd) Then
                        FileData(CurFdIdx).SetColumnValue CurLine, 1, "D"
                        FileData(CurFdIdx).SetColumnValue CurLine, 2, "N"
                        tmpData = Left(FileData(CurFdIdx).GetColumnValue(CurLine, StodColIdx), 8)
                        If tmpData = strDepDay Then
                            FileData(CurFdIdx).SetLineColor CurLine, LightGreen, vbBlack
                        Else
                            tmpData = strDepDay & tmpStod
                            FileData(CurFdIdx).SetColumnValue CurLine, StodColIdx, tmpData
                            SetErrorText "7", CurLine, ExtFldCnt, "P.BEGIN", False, 1
                        End If
                        DepLine = CurLine
                        ShrinkRecsFound = True
                    Else
                        SetErrorRange DaofColIdx, ArrLine, CurLine - 1, "OVN?.DEP"
                    End If
                End If
            'End If
            GroundDays = GroundDays - 1
        Next
        FileData(CurFdIdx).RedrawTab
        StatusBar.Panels(7).Text = "Ready"
        Refresh
    End If
    chkShrink.Enabled = ShrinkRecsFound
End Sub
Private Function GetTimePart(tmpDate As String) As String
    Dim Result As String
    Result = Right(tmpDate, 4)
    GetTimePart = Result
End Function
Private Sub ShrinkOverNightLines()
Dim CurLine As Long
Dim MaxLine As Long
Dim Col1Val As String
Dim Col2Val As String
Dim ArrLine As Long
Dim ColNbr As Long
Dim ItmNbr As Integer
Dim ItmVal As String
Dim DelLine As Boolean
Dim ForeColor As Long
Dim BackColor As Long
    If ShrinkList <> "" Then
        StatusBar.Panels(2).Text = ""
        StatusBar.Panels(7).Text = "Shrinking Overnight Rotations ..."
        Refresh
        MousePointer = 11
        MaxLine = FileData(CurFdIdx).GetLineCount
        CurLine = 0
        ArrLine = -1
        Do
            Col1Val = FileData(CurFdIdx).GetColumnValue(CurLine, 1)
            Col2Val = FileData(CurFdIdx).GetColumnValue(CurLine, 2)
            DelLine = False
            If (Col1Val = "-") And (Col2Val = "-") Then
                DelLine = True
            ElseIf (Col1Val = "-") And (Col2Val = "N") Then
                DelLine = True
            ElseIf (Col1Val = "A") And (Col2Val = "N") Then
                If ArrLine >= 0 Then
                    'missing departure
                    SetErrorText "", ArrLine, ExtFldCnt, "DEP?", False, 0
                End If
                ArrLine = CurLine
            ElseIf (Col1Val = "D") And (Col2Val = "N") Then
                If ArrLine >= 0 Then
                    ItmNbr = 0
                    Do
                        ItmNbr = ItmNbr + 1
                        ColNbr = Val(GetItem(ShrinkList, ItmNbr, ",")) + 2
                        If ColNbr > 2 Then
                            ItmVal = FileData(CurFdIdx).GetColumnValue(CurLine, ColNbr)
                            FileData(CurFdIdx).SetColumnValue ArrLine, ColNbr, ItmVal
                        End If
                    Loop While ColNbr > 2
                    FileData(CurFdIdx).SetColumnValue ArrLine, 1, ""
                    FileData(CurFdIdx).SetColumnValue ArrLine, 2, ""
                    FileData(CurFdIdx).SetLineColor ArrLine, vbBlack, LightGray
                    DelLine = True
                    ArrLine = -1
                Else
                    'missing arrival
                    SetErrorText "", ArrLine, ExtFldCnt, "ARR?", False, 0
                End If
            End If
            If DelLine Then
                FileData(CurFdIdx).GetLineColor CurLine, ForeColor, BackColor
                'If BackColor = LightestRed Then ResetErrorCounter
                FileData(CurFdIdx).DeleteLine CurLine
                CurLine = CurLine - 1
                MaxLine = MaxLine - 1
                DelLine = False
            End If
            CurLine = CurLine + 1
        Loop While CurLine < MaxLine
        MaxLine = FileData(CurFdIdx).GetLineCount
        StatusBar.Panels(2).Text = Trim(Str(MaxLine))
        StatusBar.Panels(7).Text = "Ready"
        FileData(CurFdIdx).RedrawTab
        Me.Refresh
        MousePointer = 0
    End If
End Sub

Private Sub chkSort_Click()
    'For FHK Update Format only
    On Error Resume Next
    If Not RestoreAction Then
        If (UpdateMode) And (DataSystemType = "FHK") Then
            Screen.MousePointer = 11
            If chkSort.Value = 1 Then
                chkSort.BackColor = LightestGreen
                FileData(CurFdIdx).Sort "10", True, True
                FileData(CurFdIdx).Sort "9", True, True
                FileData(CurFdIdx).Sort "7", True, True
                FileData(CurFdIdx).Sort "6", True, True
                FileData(CurFdIdx).Sort "5", True, True
            Else
                FileData(CurFdIdx).Sort "0", True, True
                chkSort.BackColor = vbButtonFace
            End If
            FileData(CurFdIdx).RedrawTab
            If TabFrame.Enabled = True Then FileData(CurFdIdx).SetFocus
            Screen.MousePointer = 0
        End If
    End If
End Sub

Private Sub chkSpare_Click(Index As Integer)
    If chkSpare(Index).Value = 1 Then
        chkSpare(Index).BackColor = LightestGreen
        DontPlay Index
    Else
        chkSpare(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub chkSplit_Click()
    If Not RestoreAction Then
        If Not TotalReset Then
            MySetUp.chkSplit.Value = chkSplit.Value
            If chkSplit.Value = 1 Then
                CurrentHeader = NormalHeader
                CurFldPosLst = FldPosLst
                If chkFilter.Value = 0 Then
                    chkFilter.Value = 1
                Else
                    If Not StopMyApplEvents Then LoadFileData "(For Split)"
                End If
                chkSplit.BackColor = LightestGreen
                chkExpand.Enabled = True
            Else
                chkSplit.BackColor = vbButtonFace
                StopMyApplEvents = True
                chkExpand.Value = 0
                StopMyApplEvents = False
                If Not StopMyApplEvents Then LoadFileData "(For Filter)"
            End If
        End If
    End If
End Sub

Private Sub chkTimes_Click(Index As Integer)
    If chkTimes(Index).Value = 1 Then chkTimes(Index).BackColor = LightestGreen Else chkTimes(Index).BackColor = vbButtonFace
    If chkTimes(Index).Tag <> "" Then
        If chkTimes(Index).Value = 1 Then
            If Index = 0 Then
                chkTimes(1).Value = 0
                Me.Refresh
                ToggleLocalToUtc True
            Else
                chkTimes(0).Value = 0
                Me.Refresh
                ToggleLocalToUtc False
            End If
        Else
            If Index = 0 Then
                chkTimes(1).Value = 1
            Else
                chkTimes(0).Value = 1
            End If
        End If
    End If
End Sub

Private Sub CheckRouteInfo()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim LineNo As Long
    Dim RoutCol As Long
    Dim tmpRout As String
    Dim tmpSect As String
    Dim tmpApc3 As String
    Dim tmpTimes As String
    Dim tmpStoa As String
    Dim tmpStod As String
    Dim NewRec As String
    Dim Apc3List As String
    Dim SqlKey As String
    Dim CedaResult As String
    Dim LineNbr As String
    Dim ItemNbr As Integer
    Dim ItemCnt As Integer
    If CedaIsConnected Then
        RoutCol = GetItemNo(MainFieldList, "ROUT") + 2
        If RoutCol > 2 Then
            StatusBar.Panels(7).Text = "Check Basic Data: Route Info (APC and Times)"
            Screen.MousePointer = 11
            HelperTab(0).ResetContent
            MaxLine = FileData(CurFdIdx).GetLineCount - 1
            For CurLine = 0 To MaxLine
                LineNbr = CStr(CurLine)
                tmpRout = Trim(FileData(CurFdIdx).GetColumnValue(CurLine, RoutCol))
                If tmpRout <> "" Then
                    ItemNbr = 1
                    ItemCnt = CInt(ItemCount(tmpRout, "|"))
                    For ItemNbr = 1 To ItemCnt
                        tmpSect = GetItem(tmpRout, ItemNbr, "|")
                        If tmpSect <> "" Then
                            tmpApc3 = Trim(GetItem(tmpSect, 1, ":"))
                            NewRec = tmpApc3 & "," & LineNbr
                            HelperTab(0).InsertTextLine NewRec, False
                            If InStr(tmpRout, ":") > 0 Then
                                tmpTimes = Trim(GetItem(tmpSect, 2, ":"))
                                tmpStoa = Trim(GetItem(tmpTimes, 1, "-"))
                                tmpStod = Trim(GetItem(tmpTimes, 2, "-"))
                                If Len(tmpStoa) > 4 Then
                                    If Not CheckValidCedaTime(tmpStoa) Then SetErrorText CStr(RoutCol), CurLine, ExtFldCnt, "STA(" & tmpApc3 & ")", True, 0
                                ElseIf Len(tmpStoa) > 0 Then
                                    If Not CheckValidTime(tmpStoa, "") Then SetErrorText CStr(RoutCol), CurLine, ExtFldCnt, "STA(" & tmpApc3 & ")", True, 0
                                End If
                                If Len(tmpStod) > 4 Then
                                    If Not CheckValidCedaTime(tmpStod) Then SetErrorText CStr(RoutCol), CurLine, ExtFldCnt, "STD(" & tmpApc3 & ")", True, 0
                                ElseIf Len(tmpStod) > 0 Then
                                    If Not CheckValidTime(tmpStod, "") Then SetErrorText CStr(RoutCol), CurLine, ExtFldCnt, "STD(" & tmpApc3 & ")", True, 0
                                End If
                                If ItemNbr = 1 Then
                                    If Len(tmpStoa) > 0 Then SetErrorText CStr(RoutCol), CurLine, ExtFldCnt, "STA?(" & tmpApc3 & ")", True, 0
                                    If Len(tmpStod) = 0 Then SetErrorText CStr(RoutCol), CurLine, ExtFldCnt, "STD?(" & tmpApc3 & ")", True, 0
                                ElseIf ItemNbr = ItemCnt Then
                                    If Len(tmpStoa) = 0 Then SetErrorText CStr(RoutCol), CurLine, ExtFldCnt, "STA?(" & tmpApc3 & ")", True, 0
                                    If Len(tmpStod) > 0 Then SetErrorText CStr(RoutCol), CurLine, ExtFldCnt, "STD?(" & tmpApc3 & ")", True, 0
                                Else
                                    If Len(tmpStoa) = 0 Then SetErrorText CStr(RoutCol), CurLine, ExtFldCnt, "STA?(" & tmpApc3 & ")", True, 0
                                    If Len(tmpStod) = 0 Then SetErrorText CStr(RoutCol), CurLine, ExtFldCnt, "STD?(" & tmpApc3 & ")", True, 0
                                End If
                            End If
                        Else
                            SetErrorText CStr(RoutCol), CurLine, ExtFldCnt, "VIA?(" & CStr(ItemNbr) & ")", True, 0
                        End If
                    Next
                Else
                    SetErrorText CStr(RoutCol), CurLine, ExtFldCnt, "ROUTE?", True, 0
                End If
            Next
            HelperTab(0).RedrawTab
            Apc3List = HelperTab(0).SelectDistinct(0, "'", "'", ",", True)
            SqlKey = "WHERE APC3 IN (" & Apc3List & ")"
            UfisServer.CallCeda CedaResult, "RTA", "APTTAB", "APC3", "", SqlKey, "", 0, True, False
            Refresh
            CedaResult = Replace(CedaResult, vbNewLine, ",", 1, -1, vbBinaryCompare)
            ItemNbr = 1
            MaxLine = HelperTab(0).GetLineCount - 1
            tmpApc3 = GetItem(CedaResult, ItemNbr, ",")
            While tmpApc3 <> ""
                CurLine = MaxLine
                While CurLine >= 0
                    If tmpApc3 = HelperTab(0).GetColumnValue(CurLine, 0) Then
                        HelperTab(0).DeleteLine CurLine
                        MaxLine = MaxLine - 1
                        CurLine = CurLine + 1
                    End If
                    CurLine = CurLine - 1
                Wend
                ItemNbr = ItemNbr + 1
                tmpApc3 = GetItem(CedaResult, ItemNbr, ",")
            Wend
            HelperTab(0).RedrawTab
            tmpRout = CStr(RoutCol)
            MaxLine = HelperTab(0).GetLineCount - 1
            For CurLine = 0 To MaxLine
                tmpApc3 = HelperTab(0).GetColumnValue(CurLine, 0)
                LineNbr = HelperTab(0).GetColumnValue(CurLine, 1)
                LineNo = Val(LineNbr)
                SetErrorText tmpRout, LineNo, ExtFldCnt, "APC3:" & tmpApc3, True, 0
            Next
            FileData(CurFdIdx).AutoSizeColumns
            FileData(CurFdIdx).RedrawTab
            HelperTab(0).ResetContent
            HelperTab(0).RedrawTab
            ResetErrorCounter
            Screen.MousePointer = 0
            StatusBar.Panels(7).Text = "Ready"
        End If
    End If
End Sub

Private Function GetImportSection(SecName As String) As Integer
    Dim i As Integer
    Dim j As Integer
    Dim tmpTag As String
    j = -1
    For i = 0 To chkImpTyp.UBound
        tmpTag = chkImpTyp(i).Tag
        If tmpTag = SecName Then
            j = i
            Exit For
        End If
    Next
    GetImportSection = j
End Function

Private Sub Command1_Click()
    ShowSlidePanel 0, -1, "", ""
End Sub

Private Sub ErrCnt_Click()
    If Val(ErrCnt.Caption) > 0 Then JumpToErrorLine ErrorColor
End Sub

Private Sub ErrCnt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Val(ErrCnt.Caption) > 0 Then ErrCnt.MousePointer = 14 Else ErrCnt.MousePointer = 0
End Sub

Private Sub FileData_InplaceEditCell(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal NewValue As String, ByVal OldValue As String)
    Dim ColFld As String
    ColFld = GetRealItem(MainFieldList, (ColNo - 3), ",")
    If ColFld <> "" Then
        If InStr(TranslateIcao, ColFld) > 0 Then TranslateIcaoCodes LineNo, ColFld
        If InStr(TranslateApcList, ColFld) > 0 Then TranslateAirportCodes LineNo, ColFld
    End If
End Sub

Private Sub FileData_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    If Selected Then
        If Not StopMyApplEvents Then
            Select Case Index
                Case 0
                    SynchronizeMyTabs "FILE", "CURSOR", LineNo
                Case 1
                    SynchronizeMyTabs "COPY", "CURSOR", LineNo
                Case Else
            End Select
        End If
    End If
End Sub
Private Sub FileData_SendMouseMove(Index As Integer, ByVal Line As Long, ByVal Col As Long, ByVal Flags As String)
    'Dim RecData As String
    'If Flags = "LBUTTON" Then
    '    MousePointer = 99
    '    If DragLine < 0 Then
    '        DragLine = Line
    '        MoveLine = Line
    '        FileData(CurFdIdx).GetLineColor Line, DragForeColor, DragBackcolor
    '        FileData(CurFdIdx).SetLineColor Line, vbBlack, vbCyan
    '        FileData(CurFdIdx).SelectBackColor = vbCyan
    '        FileData(CurFdIdx).SelectTextColor = vbBlack
    '    Else
    '        If MoveLine <> Line Then
    '            MoveLine = Line
    '            FileData(CurFdIdx).SetCurrentSelection Line
    '        End If
    '    End If
    'Else
    '    If DragLine >= 0 Then
    '        If DragLine <> Line Then
    '            RecData = FileData(CurFdIdx).GetLineValues(DragLine)
    '            If DragLine > Line Then
    '                FileData(CurFdIdx).DeleteLine DragLine
    '                FileData(CurFdIdx).InsertTextLineAt Line, RecData, False
    '                FileData(CurFdIdx).SetLineColor Line, DragForeColor, DragBackcolor
    '                FileData(CurFdIdx).SetCurrentSelection Line
    '            Else
    '                FileData(CurFdIdx).InsertTextLineAt Line, RecData, False
    '                FileData(CurFdIdx).SetLineColor Line, DragForeColor, DragBackcolor
    '                FileData(CurFdIdx).SetCurrentSelection Line
    '                FileData(CurFdIdx).DeleteLine DragLine
    '            End If
    '        Else
    '            FileData(CurFdIdx).SetLineColor DragLine, DragForeColor, DragBackcolor
    '        End If
    '        FileData(CurFdIdx).SelectBackColor = NormalBlue
    '        FileData(CurFdIdx).SelectTextColor = vbWhite
    '        FileData(CurFdIdx).RedrawTab
    '        MousePointer = 0
    '        DragLine = -1
    '        DropLine = -1
    '        MoveLine = -1
    '    End If
    'End If
End Sub

Private Sub FileData_SendRButtonClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim tmpText As String
    Dim tmpType As String
    Dim tmpFldName As String
    tmpFldName = GetRealItem(FileData(Index).LogicalFieldList, ColNo, ",")
    tmpText = Trim(FileData(Index).GetColumnValue(LineNo, ColNo))
    Select Case tmpFldName
        Case "FLCA", "FLCD", "ALCD"
            tmpType = "ALC"
        Case "FLTA"
            tmpText = Left((FileData(Index).GetFieldValue(LineNo, "FLCA") & "   "), 3) & tmpText
            tmpType = "FLIGHT"
        Case "FLTD"
            tmpText = Left((FileData(Index).GetFieldValue(LineNo, "FLCD") & "   "), 3) & tmpText
            tmpType = "FLIGHT"
        Case "FLTN"
            tmpText = Left((FileData(Index).GetFieldValue(LineNo, "ALCD") & "   "), 3) & tmpText
            tmpType = "FLIGHT"
        Case "ACT3", "ACT5" 'AirCraft Type
            tmpType = "ACT"
        Case "ORG3", "DES3", "VSA3", "VSD3", "ORG4", "DES4", "VSA4", "VSD4" 'AirportCode
            tmpType = "APT"
        Case Else
    End Select
    BasicDetails tmpType, tmpText
End Sub

Private Sub Form_Activate()
    Static IsActive As Boolean
    If Not IsActive Then
        IsActive = True
        GetDefaultSeason True
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    'If KeyCode = vbKeyF1 Then DisplayHtmHelpFile "", Me.Name, Me.ActiveControl.Name
    If KeyCode = vbKeyF1 Then ShowHtmlHelp Me, "", ""
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    'If DragLine >= 0 Then
    '    MousePointer = 12
    'End If
End Sub

Private Sub Form_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    'If DragLine >= 0 Then
    '    FileData(CurFdIdx).SetLineColor DragLine, DragForeColor, DragBackcolor
    '    FileData(CurFdIdx).SelectBackColor = NormalBlue
    '    FileData(CurFdIdx).SelectTextColor = vbWhite
    '    FileData(CurFdIdx).RedrawTab
    '    DragLine = -1
    '    DropLine = -1
    '    MousePointer = 0
    'End If
End Sub

Private Sub Form_OLEDragDrop(Data As DataObject, Effect As Long, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim tmpFile As String
    Dim RetVal As Integer
    On Error GoTo ErrHdl
    If PushedImpButton >= 0 Then
        If (Effect And vbDropEffectCopy) = vbDropEffectCopy Then
            If Data.Files.Count > 0 Then
                tmpFile = Data.Files(1)
                RetVal = 1
                If chkExpand.Value = 1 Then
                    RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Import File Control", "Do you want to replace the current file?", "export", "Yes,No", UserAnswer)
                End If
                If RetVal = 1 Then
                    chkRequest.Tag = tmpFile
                    TabFrame.Enabled = True
                    ResetMain 0, False
                    ResetMain 0, True
                    StopMyApplEvents = True
                    chkExpand.Value = 0
                    StopMyApplEvents = False
                    chkExpand.Value = 1
                End If
            End If
        End If
    End If
ErrHdl:
End Sub

Private Sub Form_OLEDragOver(Data As DataObject, Effect As Long, Button As Integer, Shift As Integer, X As Single, Y As Single, State As Integer)
    If PushedImpButton >= 0 Then
        If State = 1 Then TabFrame.Enabled = True Else TabFrame.Enabled = False
        Me.SetFocus
    Else
        '
    End If
End Sub

Private Sub ImpScroll_Change()
    HandleImpScroll
End Sub
Private Sub ImpScroll_Scroll()
    HandleImpScroll
End Sub
Private Sub HandleImpScroll()
    Dim idx As Integer
    Dim i As Integer
    Dim LeftPos As Long
    idx = ImpScroll.Value
    LeftPos = chkTimes(1).Left + chkTimes(1).Width + Screen.TwipsPerPixelX
    For i = 0 To ShowMaxButtons
        If idx <= chkImpTyp.UBound Then
            chkImpTyp(idx).Left = LeftPos
            chkImpTyp(idx).Visible = True
            chkImpTyp(idx).ZOrder
            LeftPos = LeftPos + chkImpTyp(idx).Width + Screen.TwipsPerPixelX
            idx = idx + 1
        End If
    Next
End Sub

Private Sub TAB2_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    If Selected Then
        If Not StopMyApplEvents Then
            SynchronizeMyTabs "PUSH", "CURSOR", LineNo
        End If
    End If
End Sub

Private Sub TcpMsgTimer_Timer(Index As Integer)
    Dim MsgCmd As String
    Dim MsgData As String
    Dim TcpMsg As String
    Dim LineNo As Long
    TcpMsgTimer(Index).Enabled = False
    LineNo = TcpMsgSpooler(Index).GetLineCount
    If WebToolIsConnected Then
        If LineNo > 0 Then
            MsgCmd = TcpMsgSpooler(Index).GetColumnValue(0, 0)
            MsgData = TcpMsgSpooler(Index).GetColumnValue(0, 1)
            TcpMsg = CreateOutMessage(MsgCmd, MsgData)
            WebSock(Index).SendData TcpMsg
            TcpMsgSpooler(Index).DeleteLine 0
        End If
        LineNo = TcpMsgSpooler(Index).GetLineCount
    End If
    If LineNo > 0 Then
        TcpMsgTimer(Index).Enabled = True
    End If
End Sub

Private Sub Timer1_Timer()
    Timer1.Enabled = False
    CheckCmdLineParams
End Sub

Private Sub CheckCmdLineParams()
    Dim FileCount As Integer
    Dim CmdLine As String
    Dim ListOfFiles As String
    Dim ActFile As String
    Dim ActPath As String
    CmdLine = Command
    ListOfFiles = GetItem(CmdLine, 1, "{=")
    If ListOfFiles <> "" Then
        'MsgBox ListOfFiles
        'FileCount = GetFileAndPath(ListOfFiles, ActFile, ActPath)
        'MsgBox ActFile
    End If
End Sub

Private Sub Timer2_Timer()
    Dim tmpTag As String
    Dim tmpCmd As String
    Dim tmpIdx As String
    Dim i As Integer
    Timer2.Enabled = False
    tmpTag = Timer2.Tag
    Timer2.Tag = ""
    If tmpTag <> "" Then
        tmpCmd = GetItem(tmpTag, 1, ",")
        tmpIdx = GetItem(tmpTag, 2, ",")
        i = Val(tmpIdx)
        Select Case tmpCmd
            Case "DBL"
                DoubleGridInit = True
                chkImpTyp(i).Value = 1
                DoubleGridInit = False
            Case "PSH"
                SectionPushed = True
                chkImpTyp(i).Value = 1
                SectionPushed = False
            Case Else
        End Select
    End If
End Sub

Private Sub txtEdit_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub WarnCnt_Click()
    If Val(WarnCnt.Caption) > 0 Then JumpToErrorLine LightestRed
End Sub

Private Sub JumpToErrorLine(ErrColor As Long)
    Dim CurScroll As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim ForeColor As Long
    Dim BackColor As Long
    CurLine = FileData(CurFdIdx).GetCurrentSelected
    If CurLine < 0 Then CurLine = -1
    MaxLine = FileData(CurFdIdx).GetLineCount - 1
    While CurLine <= MaxLine
        CurLine = CurLine + 1
        FileData(CurFdIdx).GetLineColor CurLine, ForeColor, BackColor
        If BackColor = ErrColor Then
            If CurLine > 10 Then CurScroll = CurLine - 10 Else CurScroll = 0
            FileData(CurFdIdx).OnVScrollTo CurScroll
            FileData(CurFdIdx).SetCurrentSelection CurLine
            CurLine = MaxLine + 1
        End If
    Wend
End Sub

Private Sub FileData_SendLButtonDblClick(Index As Integer, ByVal Line As Long, ByVal Col As Long)
    Dim tmpData As String
    Dim tmpLinNbr As String
    Dim tmpHead As String
    Dim LineNo As Long
    On Error Resume Next
    If Line >= 0 Then
        If chkEdit.Value = 1 Then
            tmpData = FileData(Index).GetColumnValue(Line, Col)
            txtEdit.Tag = tmpData
            txtEdit.Text = tmpData
            chkEdit.Tag = Line & "," & Col
            tmpLinNbr = FileData(Index).GetColumnValue(Line, 0)
            tmpHead = FileData(Index).GetLineValues(-1)
            tmpData = GetItem(tmpHead, Col + 1, ",")
            chkReplace.ToolTipText = "Replace '" & Trim(tmpData) & "' in line " & Trim(tmpLinNbr) & " "
            chkAll.ToolTipText = "Replace all '" & Trim(txtEdit.Tag) & "' in column '" & Trim(tmpData) & "' "
            chkAll.Tag = GetItem(AutoReplaceAll, Col - 2, ",")
            FileData(Index).ColSelectionRemoveAll
            FileData(Index).SelectColumnBackColor = DarkBlue
            FileData(Index).SelectColumnTextColor = vbWhite
            FileData(Index).ColSelectionAdd Col
            txtEdit.SetFocus
        ElseIf chkExtract.Value = 1 Then
            If (chkInline.Value = 0) And (FlightExtract.chkWork(5).Value = 1) Then
                LineNo = FileData(Index).GetCurrentSelected
                FlightExtract.CopyAllFlights LineNo, LineNo
                If TabFrame.Enabled = True Then FileData(Index).SetFocus
            End If
        End If
    Else
        If Line = -1 Then
            'Sort
            If Not UpdateMode Then
                MousePointer = 11
                chkEdit.Value = 0
                chkExtract.Value = 0
                FileData(Index).Sort Str(Col), True, True
                FileData(Index).Refresh
                'If IsDoubleGridView Then
                '    FileData(1).Sort Str(Col), True, True
                '    FileData(1).Refresh
                'End If
                MousePointer = 0
            End If
        End If
    End If
End Sub

Private Sub Form_Load()
    InitApplication
    PushedImpButton = -1
    'ErrorColor = &HFF80FF
    ErrorColor = vbRed
    If Command <> "" Then Timer1.Enabled = True
    InitSlidePanels -1
    InitTcpMsgSpooler 0
    InitRcvMsgSpooler 0
End Sub

Public Sub InitApplication()
    Dim tmpStr As String
    If FileData.UBound > 0 Then
        Unload FileData(1)
    End If
    'Load FileData(1)
    WinZipId = -1
    DragLine = -1
    DropLine = -1
    FileData(0).Left = 0
    'FileData(1).Left = 0
    TAB2.Left = 0
    GetUfisDir
    PathToUfisWebBrowser = UFIS_APPL & "\UfisWebTool.exe"
    WebToolIsConnected = False
    
    ButtonPanel(0).Left = 30
    ButtonPanel(1).Left = 30
    ButtonPanel(2).Left = 30
    ButtonPanel(0).Height = 300
    ButtonPanel(1).Height = 300
    ButtonPanel(0).Top = 0
    ButtonPanel(1).Top = ButtonPanel(0).Top + ButtonPanel(0).Height
    ButtonPanel(2).Top = ButtonPanel(1).Top + ButtonPanel(1).Height
    TabFrame.Left = 30
    TabFrame.Top = ButtonPanel(2).Top + ButtonPanel(2).Height
    
    'myIniPath = "c:\ufis\system"
    myIniPath = UFIS_SYSTEM
    myIniFile = "ImportTool" & UfisServer.HOPO & ".ini"
    myIniFullName = myIniPath & "\" & myIniFile
    tmpStr = Dir(myIniFullName)
    If UCase(tmpStr) <> UCase(myIniFile) Then
        myIniFile = "ImportTool.ini"
        myIniFullName = myIniPath & "\" & myIniFile
    End If
    MainTitle = GetIniEntry(myIniFullName, "MAIN", "", "IMPORT_TITLE", "Flight Data Import Tool")
    Me.Caption = MainTitle
    UseGfrCmd = GetIniEntry(myIniFullName, "MAIN", "", "ROUTER_CMD", "GFR")
    Load MySetUp
    InitGrids True
    ShowMaxButtons = Abs(Val(GetIniEntry(myIniFullName, "MAIN", "", "MAX_BUTTONS", "12"))) - 1
    If ShowMaxButtons < 1 Then ShowMaxButtons = 1
    If ShowMaxButtons > 12 Then ShowMaxButtons = 12
    InitValidSections ""
    MySetUp.DetermineFileType "SESSION"
    Load SeasonData
    UfisServer.ConnectToCeda
    UfisServer.ModName.Tag = gsUserName
    UfisServer.BasicDataInit 1, 0, "NATTAB", "TTYP,TNAM", ""
End Sub

Public Sub InitValidSections(CfgSections As String)
    Dim ValidSections As String
    Dim ItmNbr As Integer
    Dim tmpData As String
    Dim tmpType As String
    Dim ButtonIndex As Integer
    Dim BreakOut As Boolean
    Dim LeftPos As Long
    Dim TopPos As Long
    ValidSections = CfgSections
    LeftPos = chkTimes(1).Left + chkTimes(1).Width + Screen.TwipsPerPixelX
    TopPos = chkTimes(1).Top
    chkLoad.Value = 0
    For ButtonIndex = 0 To chkImpTyp.UBound
        chkImpTyp(ButtonIndex).Visible = False
        chkImpTyp(ButtonIndex).Value = 0
    Next
    tmpData = GetIniEntry(myIniFullName, "MAIN", "", "TESTMODE", "NO")
    If tmpData = "YES" Then TestMode = True Else TestMode = False
    If ValidSections = "" Then ValidSections = GetIniEntry(myIniFullName, "MAIN", "", "IMPORT_TYPES", "")
    ButtonIndex = -1
    ItmNbr = 0
    BreakOut = False
    Do
        ItmNbr = ItmNbr + 1
        tmpData = Trim(GetItem(ValidSections, ItmNbr, ","))
        If tmpData <> "" Then
            ButtonIndex = ButtonIndex + 1
            If ButtonIndex > chkImpTyp.UBound Then Load chkImpTyp(ButtonIndex)
            If ButtonIndex <= chkImpTyp.UBound Then
                chkImpTyp(ButtonIndex).Top = TopPos
                chkImpTyp(ButtonIndex).Left = LeftPos
                chkImpTyp(ButtonIndex).Tag = tmpData
                tmpType = Trim(GetIniEntry(myIniFullName, chkImpTyp(ButtonIndex).Tag, "", "SYSTEM_TYPE", ""))
                If tmpType = "DUMMY" Then
                    chkImpTyp(ButtonIndex).Width = 405
                End If
                If ButtonIndex <= ShowMaxButtons Then
                    chkImpTyp(ButtonIndex).Visible = True
                    LeftPos = LeftPos + chkImpTyp(ButtonIndex).Width + Screen.TwipsPerPixelX
                End If
                chkImpTyp(ButtonIndex).Caption = ""
                tmpData = Trim(GetIniEntry(myIniFullName, chkImpTyp(ButtonIndex).Tag, "", "FORMAT_NAME", ""))
                If tmpData <> "" Then
                    chkImpTyp(ButtonIndex).Caption = tmpData
                    chkImpTyp(ButtonIndex).ToolTipText = LTrim(Trim(GetIniEntry(myIniFullName, chkImpTyp(ButtonIndex).Tag, "", "TOOLTIP_TEXT", "")) & " ")
                ElseIf tmpType <> "DUMMY" Then
                    chkImpTyp(ButtonIndex).Caption = "???" & Str(ItmNbr)
                    chkImpTyp(ButtonIndex).ToolTipText = "Missing configuration key: 'FORMAT_NAME' "
                    chkImpTyp(ButtonIndex).Tag = "NOTHING"
                Else
                    chkImpTyp(ButtonIndex).ToolTipText = LTrim(Trim(GetIniEntry(myIniFullName, chkImpTyp(ButtonIndex).Tag, "", "TOOLTIP_TEXT", "")) & " ")
                    chkImpTyp(ButtonIndex).Tag = tmpType
                End If
            End If
        Else
            BreakOut = True
        End If
    Loop While Not BreakOut
    ImpScroll.Visible = False
    If ButtonIndex < 0 Then
        MsgBox "No valid configuration found."
    Else
        If ButtonIndex > ShowMaxButtons Then
            ImpScroll.Tag = Trim(Str(ShowMaxButtons))
            ImpScroll.Max = ButtonIndex - ShowMaxButtons
            ImpScroll.Left = LeftPos - Screen.TwipsPerPixelX
            ImpScroll.Visible = True
            LeftPos = LeftPos + ImpScroll.Width + Screen.TwipsPerPixelX - Screen.TwipsPerPixelX
        End If
    End If
End Sub

Public Sub InitGrids(ipFirstCall As Boolean)
    ResetLabels
    If WindowState = vbNormal Then
        Me.Top = 975
        'Me.Left = 0
        'Me.Width = Screen.Width / Val(MySetUp.MaxMon.Caption)
        Me.Width = chkAppl(6).Left + chkAppl(6).Width + 180
        'Me.Left = (Screen.Width - Me.Width) / 2
        Me.Left = ((1024 * 15) - Me.Width) / 2
    End If
    FileData(CurFdIdx).LifeStyle = True
    If chkSplit.Value = 0 Or chkSplit.Enabled = False Then
        PrepareFileData
    Else
        InitDataList 8, ipFirstCall
    End If
    If WindowState = vbNormal Then
        Me.Height = Screen.Height - 1380
    End If
    Me.Refresh
End Sub

Private Sub PrepareFileData()
    Dim HdrLenLst As String
    Dim i As Integer
    Me.FontSize = MySetUp.FontSlider.Value
    HdrLenLst = ""
    HdrLenLst = HdrLenLst & TextWidth("9999") / Screen.TwipsPerPixelX + 6 & ","
    HdrLenLst = HdrLenLst & "1000"
    For i = 0 To FileData.UBound
        FileData(i).ResetContent
        FileData(i).LogicalFieldList = "FLD1,FLD2"
        FileData(i).FontName = "Courier New"
        FileData(i).SetTabFontBold True
        FileData(i).ShowHorzScroller False
        FileData(i).HeaderFontSize = MySetUp.FontSlider.Value + 6
        FileData(i).FontSize = MySetUp.FontSlider.Value + 6
        FileData(i).LineHeight = MySetUp.FontSlider.Value + 6
        FileData(i).CreateDecorationObject "CellError", "R,T,B,L", "2,2,2,2", Str(vbBlack) & "," & Str(LightGray) & "," & Str(vbBlack) & "," & Str(LightGray)
        FileData(i).HeaderLengthString = HdrLenLst
        FileData(i).HeaderString = "Line,Data Lines (" & CStr(i) & ")"
        FileData(i).MainHeader = False
        CheckMainHeader i
    Next
    GridPrepared = True
End Sub
Private Sub CheckMainHeader(Index As Integer)
    If chkExpand.Value = 1 Then
        If Index = 0 Then
            If MainHeaderTitle <> "" Then
                FileData(Index).SetMainHeaderValues MainHeaderWidth, MainHeaderTitle, MainHeaderColor
                FileData(Index).SetMainHeaderFont MySetUp.FontSlider.Value + 6, False, False, True, 0, "Courier New"
                FileData(Index).MainHeader = True
            End If
        Else
            FileData(Index).MainHeader = False
        End If
    Else
        FileData(Index).MainHeader = False
    End If
End Sub
Private Sub InitDataList(ipLines As Integer, ipFirstCall As Boolean)
    Dim i As Integer
    Dim tmpLen
    Dim HdrLenLst As String
    Dim tmpHeader As String
    Dim tmpAlign As String
    Dim ColWid As String
    Dim InitBoth As Boolean
    Me.FontSize = MySetUp.FontSlider.Value
    FileData(CurFdIdx).ShowHorzScroller False
    If chkExpand.Value = 1 Then
        ActColLst = MainFldWidLst
        tmpHeader = "LINE,I,A," & MainExpandHeader & ",Remark,System Cells,System Status,Unique ID"
        InitBoth = False
    Else
        ActColLst = FldLenLst
        tmpHeader = "LINE,I,A," & NormalHeader & ",Remark,System Cells,System Status,Unique ID"
        InitBoth = True
    End If
    HdrLenLst = ""
    ColWid = ""
    HdrLenLst = HdrLenLst & TextWidth("999999") / Screen.TwipsPerPixelX + 6 & ","
    HdrLenLst = HdrLenLst & TextWidth("A") / Screen.TwipsPerPixelX + 6 & ","
    HdrLenLst = HdrLenLst & TextWidth("U") / Screen.TwipsPerPixelX + 6 & ","
    tmpAlign = "R,L,L,"
    ColWid = "6,1,1,"
    i = 0
    Do
        i = i + 1
        tmpLen = GetItem(ActColLst, i, ",")
        If tmpLen <> "" Then
            ColWid = ColWid & tmpLen & ","
            HdrLenLst = HdrLenLst & TextWidth(Space(tmpLen)) / Screen.TwipsPerPixelX + 6 & ","
            tmpAlign = tmpAlign & "L,"
        End If
    Loop While tmpLen <> ""
    ExtFldCnt = ItemCount(HdrLenLst, ",") - 1
    HdrLenLst = HdrLenLst & "500,500,500,500"
    ColWid = ColWid & "160,160,160,160"
    
    tmpAlign = tmpAlign & "L,L,L,L"
    If ipFirstCall Then FileData(CurFdIdx).ResetContent
    FileData(CurFdIdx).MainHeader = False
    FileData(CurFdIdx).HeaderString = CStr(CurFdIdx) & ":" & tmpHeader
    FileData(CurFdIdx).HeaderLengthString = HdrLenLst
    FileData(CurFdIdx).ColumnAlignmentString = tmpAlign
    FileData(CurFdIdx).ColumnWidthString = ColWid
    FileData(CurFdIdx).FontName = "Courier New"
    FileData(CurFdIdx).SetTabFontBold True
    FileData(CurFdIdx).ShowHorzScroller True
    FileData(CurFdIdx).HeaderFontSize = MySetUp.FontSlider.Value + 6
    FileData(CurFdIdx).FontSize = MySetUp.FontSlider.Value + 6
    FileData(CurFdIdx).LineHeight = MySetUp.FontSlider.Value + 6
    FileData(CurFdIdx).CreateDecorationObject "CellError", "R,T,B,L", "2,2,2,2", Str(vbBlack) & "," & Str(LightGray) & "," & Str(vbBlack) & "," & Str(LightGray)
    FileData(CurFdIdx).AutoSizeByHeader = True
    CurHeaderLength = HdrLenLst
    CheckMainHeader CurFdIdx
    If SetColSelection Then FileData(CurFdIdx).EnableHeaderSelection True
    'FileData(CurFdIdx).EnableHeaderSizing True
    FileData(CurFdIdx).Refresh
End Sub
Private Sub ResetLabels()
Dim i As Integer
    ErrCnt.Caption = ""
    ErrCnt.BackColor = vbButtonFace
    ErrCnt.ForeColor = vbBlack
    WarnCnt.Caption = ""
    WarnCnt.BackColor = vbButtonFace
    WarnCnt.ForeColor = vbBlack
    For i = 1 To StatusBar.Panels.Count
        StatusBar.Panels(i).Text = ""
    Next
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'MsgBox UnloadMode
    Select Case UnloadMode
        Case 0
            chkAppl(6).Value = 1
            chkAppl(6).Value = 0
            Cancel = True
        Case Else
            Unload Me
    End Select
End Sub

Private Sub Form_Resize()
    Dim NewVal As Long
    On Error Resume Next
    NewVal = Me.ScaleHeight - TabFrame.Top - StatusBar.Height - 2 * Screen.TwipsPerPixelY
    If NewVal > 300 Then
        TabFrame.Height = NewVal
        'FileData(0).Height = NewVal
    End If
    NewVal = Me.ScaleWidth - 2 * TabFrame.Left '- 5 * Screen.TwipsPerPixelX
    If NewVal > 300 Then
        TabFrame.Width = NewVal
        FileData(0).Width = NewVal '- 1 * Screen.TwipsPerPixelY
        If FileData.Count > 1 Then FileData(1).Width = FileData(0).Width
        TAB2.Width = FileData(0).Width
    End If
    CedaBusyFlag.Top = MainDialog.ScaleHeight - StatusBar.Height + 2 * Screen.TwipsPerPixelY
    CedaBusyFlag.Left = MainDialog.ScaleWidth - CedaBusyFlag.Width - 14 * Screen.TwipsPerPixelX
    'lblSize.Caption = CStr(Me.Width / 15) & " / " & CStr(Me.Height / 15)
    ArrangeMainScreen "INIT"
 End Sub

Private Sub txtEdit_Change()
    If txtEdit.Text <> txtEdit.Tag Then
        txtEdit.BackColor = vbYellow
        chkReplace.Enabled = True
        If chkAll.Tag <> "" Then chkAll.Enabled = True Else chkAll.Enabled = False
    Else
        txtEdit.BackColor = vbWhite
        chkReplace.Enabled = False
        chkAll.Enabled = False
    End If
End Sub

Private Sub IdentifyFlights(lpFlno As Long, lpSector As Long, lpOrig As Long, lpDest As Long)
Dim llCount As Long
Dim i As Long
Dim ilSectorNbr As Integer
Dim ilLastSectorNbr As Integer
Dim clFlno As String
Dim clOrg3 As String
Dim clDes3 As String
Dim clLastOrg3 As String
Dim clLastDes3 As String
Dim StodColIdx As Long
Dim StoaColIdx As Long
Dim clStod As String
Dim clStoa As String
Dim clLastStod As String
Dim clLastStoa As String
Dim MyForeColor As Long
Dim MyBackColor As Long
Dim FirstLine As Long
Dim LastLine As Long
Dim clAdid As String
Dim clCntVal As String
Dim FlightClosed As Boolean
Dim ApproachCnt As Integer
Dim DepartureCnt As Integer
    ArrFltCnt = 0
    DepFltCnt = 0
    If LegColNbr > 2 And OrgColNbr > 2 And DesColNbr > 2 Then
        MousePointer = 11
        
        If TimeChainInfo <> "" Then
            StodColIdx = Val(GetItem(TimeChainInfo, 2, ",")) + 2
            StoaColIdx = Val(GetItem(TimeChainInfo, 3, ",")) + 2
        End If
        
        llCount = FileData(CurFdIdx).GetLineCount - 1
        For i = 0 To llCount
            FileData(CurFdIdx).GetLineColor i, MyForeColor, MyBackColor
            If MyBackColor <> ErrorColor Then
                ilSectorNbr = Val(FileData(CurFdIdx).GetColumnValue(i, lpSector))
                clOrg3 = FileData(CurFdIdx).GetColumnValue(i, lpOrig)
                clDes3 = FileData(CurFdIdx).GetColumnValue(i, lpDest)
                If (StodColIdx > 2) And (StoaColIdx > 2) Then
                    clStod = FileData(CurFdIdx).GetColumnValue(i, StodColIdx)
                    clStoa = FileData(CurFdIdx).GetColumnValue(i, StoaColIdx)
                End If
                If ilSectorNbr = 1 Then
                    If (clAdid = "?") And (Not FlightClosed) Then
                        'BADFLT doesn't connect HomeAirport
                        SetErrorText Str(lpSector) & "," & Str(lpOrig) & "," & Str(lpDest), LastLine, ExtFldCnt, HomeAirport & "?", True, 1
                    End If
                    'Begin of a new flight (First Sector)
                    If clOrg3 = HomeAirport And clDes3 = HomeAirport Then
                        'Circulation Flight
                        clAdid = "B"
                        FlightClosed = True
                        ApproachCnt = 0
                        FileData(CurFdIdx).SetLineColor i, vbBlack, vbCyan
                        FileData(CurFdIdx).SetColumnValue i, 1, clAdid
                        ArrFltCnt = ArrFltCnt + 1
                        DepFltCnt = DepFltCnt + 1
                    ElseIf clOrg3 = HomeAirport Then
                        'Departure Flight
                        clAdid = "D"
                        ApproachCnt = 0
                        DepartureCnt = 1
                        FlightClosed = False
                        FileData(CurFdIdx).SetLineColor i, vbBlack, vbGreen
                        FileData(CurFdIdx).SetColumnValue i, 1, clAdid
                        DepFltCnt = DepFltCnt + 1
                    ElseIf clDes3 = HomeAirport Then
                        'Arrival Flight
                        clAdid = "A"
                        FlightClosed = True
                        ApproachCnt = 0
                        DepartureCnt = 0
                        FileData(CurFdIdx).SetLineColor i, vbBlack, vbYellow
                        FileData(CurFdIdx).SetColumnValue i, 1, clAdid
                        ArrFltCnt = ArrFltCnt + 1
                    Else
                        'Must be an approach to us
                        clAdid = "?"
                        DepartureCnt = 0
                        FlightClosed = False
                        FileData(CurFdIdx).SetLineColor i, vbBlack, LightestYellow
                        ApproachCnt = ApproachCnt + 1
                        clCntVal = Trim(Str(ApproachCnt))
                        FileData(CurFdIdx).SetColumnValue i, 1, clCntVal
                    End If
                    FirstLine = i
                Else
                    ilLastSectorNbr = ilLastSectorNbr + 1
                    If ilSectorNbr = ilLastSectorNbr Then
                        If clOrg3 = HomeAirport And clDes3 = HomeAirport Then
                            'Circulation Flight must be an Error
                            SetErrorText "", i, ExtFldCnt, "CIRCUL", True, 0
                        ElseIf clOrg3 = HomeAirport Then
                            'Departure Flight is an error
                            SetErrorText "", i, ExtFldCnt, "BADDEP", True, 0
                        ElseIf clDes3 = HomeAirport Then
                            'Arrival Flight
                            clAdid = "A"
                            ApproachCnt = 0
                            If FlightClosed = False Then
                                FileData(CurFdIdx).SetLineColor i, vbBlack, vbYellow
                                FlightClosed = True
                                FileData(CurFdIdx).SetColumnValue i, 1, clAdid
                                If clLastDes3 <> clOrg3 Then
                                    SetErrorText "", i, ExtFldCnt, "ORIGIN", True, 0
                                End If
                                If (StodColIdx > 2) And (StoaColIdx > 2) Then
                                    If clLastStoa > clStod Then
                                        SetErrorText "", i, ExtFldCnt, "STOD", True, 0
                                    End If
                                End If
                                ArrFltCnt = ArrFltCnt + 1
                            Else
                                SetErrorText "", i, ExtFldCnt, "BADARR", True, 0
                            End If
                        Else
                            If FlightClosed Then
                                SetErrorText "", i, ExtFldCnt, "CLOSED", True, 0
                            Else
                                If clAdid = "?" Then
                                    ApproachCnt = ApproachCnt + 1
                                    clCntVal = Trim(Str(ApproachCnt))
                                    FileData(CurFdIdx).SetColumnValue i, 1, clCntVal
                                    FileData(CurFdIdx).SetLineColor i, vbBlack, LightestYellow
                                Else
                                    DepartureCnt = DepartureCnt + 1
                                    clCntVal = Trim(Str(DepartureCnt))
                                    FileData(CurFdIdx).SetColumnValue i, 1, clCntVal
                                    FileData(CurFdIdx).SetLineColor i, vbBlack, LightestGreen
                                End If
                                If clLastDes3 <> clOrg3 Then
                                    SetErrorText "", i, ExtFldCnt, "ORIGIN", True, 0
                                End If
                                If (StodColIdx > 2) And (StoaColIdx > 2) Then
                                    If clLastStoa > clStod Then
                                        SetErrorText "", i, ExtFldCnt, "STOD", True, 0
                                    End If
                                End If
                            End If
                        End If
                    Else
                        SetErrorText "", i, ExtFldCnt, "SECTOR", True, 0
                    End If
                End If
                ilLastSectorNbr = ilSectorNbr
                clLastOrg3 = clOrg3
                clLastDes3 = clDes3
                clLastStod = clStod
                clLastStoa = clStoa
                LastLine = i
            End If
        Next
        If (clAdid = "?") And (Not FlightClosed) Then
            'BADFLT doesn't connect HomeAirport
            SetErrorText Str(lpSector) & "," & Str(lpOrig) & "," & Str(lpDest), LastLine, ExtFldCnt, HomeAirport & "?", True, 1
        End If
        MousePointer = 0
    End If
End Sub

Private Sub CheckVpfrVptoFrame(LoopBegin As Long, LoopEnd As Long)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim FldaColIdx As Long
    Dim StoaColIdx As Long
    Dim StodColIdx As Long
    Dim DaofColIdx As Long
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim tmpFred As String
    Dim tmpFrew As String
    Dim tmpStoa As String
    Dim tmpStod As String
    Dim tmpStoaDay As String
    Dim tmpStodDay As String
    Dim tmpDaof As String
    Dim tmpData As String
    Dim DayOffset As Integer
    Dim RetVal As Integer
    Dim FirstDay As String
    Dim MinVpfrDay As String
    Dim MaxVptoDay As String
    Dim tmpSeasBegin As String
    Dim tmpSeasEnd As String
    Dim tmpSeasData As String
    Dim MinLinNbr As String
    Dim MaxLinNbr As String
    Dim tmpErrFlag As String
    Dim tmpMsg As String
    Dim IsValidFreq As Boolean
    Dim CheckAgain As Boolean
    '===============================================================
    'Attention:
    'The use of STOD and STOA depends on the Import File Data Model.
    'The variables of this function have been designed as FLIGHT
    '(STOD=STOD / STOA=STOA)
    'But the configuration might define the model as ROTATION (at HOME)
    '(STOD=STOA / STOA=STOD) !!!
    'Thus don't be confused while reading this code !
    '===============================================================
    If (MainVpfrCol > 2) And (MainVptoCol > 2) Then
        MinVpfrDay = "99999999"
        MaxVptoDay = "00000000"
        If TimeChainInfo <> "" Then
            FldaColIdx = Val(GetItem(TimeChainInfo, 1, ",")) + 2
            StodColIdx = Val(GetItem(TimeChainInfo, 2, ",")) + 2
            StoaColIdx = Val(GetItem(TimeChainInfo, 3, ",")) + 2
            DaofColIdx = Val(GetItem(TimeChainInfo, 4, ",")) + 2
        End If
        StatusBar.Panels(7).Text = "Checking period time frames and frequency"
        If LoopEnd >= 0 Then
            MaxLine = LoopEnd
        Else
            MaxLine = FileData(CurFdIdx).GetLineCount - 1
        End If
        If NewLayoutType Then
            DepFreqCol = CLng(GetItemNo(MainFieldList, "FRED")) + 2
            DepOvniCol = CLng(GetItemNo(MainFieldList, "OVNI")) + 2
            DepDpfrCol = CLng(GetItemNo(MainFieldList, "DPFR")) + 2
        Else
            DepFreqCol = -1
            DepOvniCol = -1
            DepDpfrCol = -1
        End If
        For CurLine = LoopBegin To MaxLine
            If DepDpfrCol > 2 Then
                tmpVpfr = Left(FileData(CurFdIdx).GetColumnValue(CurLine, DepDpfrCol), 8)
                If Not CheckValidDate(tmpVpfr) Then SetErrorText Str(DepDpfrCol), CurLine, ExtFldCnt, "DEP.DATE", True, 0
            End If
            tmpVpfr = Left(FileData(CurFdIdx).GetColumnValue(CurLine, MainVpfrCol), 8)
            tmpVpto = Left(FileData(CurFdIdx).GetColumnValue(CurLine, MainVptoCol), 8)
            If (MainFredCol > 2) Then
                tmpFred = FileData(CurFdIdx).GetColumnValue(CurLine, MainFredCol)
                If MainFrewCol > 2 Then tmpFrew = FileData(CurFdIdx).GetColumnValue(CurLine, MainFrewCol) Else tmpFrew = "1"
                FirstDay = ""
                RetVal = Validation.CheckPeriodData(tmpVpfr, tmpVpto, tmpFred, tmpFrew, FirstDay, False)
                If DepFreqCol > 2 Then
                    tmpData = FileData(CurFdIdx).GetColumnValue(CurLine, DepFreqCol)
                    tmpData = FormatFrequency(tmpData, "123", IsValidFreq)
                    If Not IsValidFreq Then SetErrorText Str(DepFreqCol), CurLine, ExtFldCnt, "FRQDEP", True, 0
                End If
            Else
                RetVal = 0
                If Not CheckValidDate(tmpVpfr) Then SetErrorText Str(MainVpfrCol), CurLine, ExtFldCnt, "FROM.DATE", True, 0
                If Not CheckValidDate(tmpVpto) Then SetErrorText Str(MainVptoCol), CurLine, ExtFldCnt, "TO.DATE", True, 0
                FirstDay = tmpVpfr
            End If
            CheckAgain = True
            If RetVal < 0 Then
                CheckAgain = False
                Select Case RetVal
                    Case -1
                        SetErrorText Str(MainFredCol), CurLine, ExtFldCnt, "NODAYS", True, 0
                    Case -2
                        SetErrorText Str(MainVpfrCol), CurLine, ExtFldCnt, "P.DATES", True, 0
                    Case -3
                        If DataSystemType = "SSIM45" Then
                            SetErrorText Str(MainFredCol), CurLine, ExtFldCnt, "OPDAYS", False, 1
                            CheckAgain = True
                        Else
                            SetErrorText Str(MainFredCol), CurLine, ExtFldCnt, "OPDAYS", True, 0
                        End If
                    Case Else
                        SetErrorText Str(MainVptoCol), CurLine, ExtFldCnt, "PERIOD", True, 0
                End Select
            End If
            If CheckAgain Then
                If (StodColIdx > 2) And (StoaColIdx > 2) Then
                    If FirstDay = "" Then
                        SetErrorText Str(MainVpfrCol), CurLine, ExtFldCnt, "DAY1", True, 0
                        FirstDay = tmpVpfr
                    End If
                    tmpStodDay = FirstDay
                    tmpStoaDay = tmpStodDay
                    tmpStod = FileData(CurFdIdx).GetColumnValue(CurLine, StodColIdx)
                    tmpStod = Right(tmpStod, 4)
                    tmpStoa = FileData(CurFdIdx).GetColumnValue(CurLine, StoaColIdx)
                    tmpStoa = Right(tmpStoa, 4)
                    DayOffset = 0
                    If DaofColIdx > 2 Then
                        tmpDaof = FileData(CurFdIdx).GetColumnValue(CurLine, DaofColIdx)
                        DayOffset = Val(tmpDaof)
                    End If
                    If (tmpStoa <> "") And (tmpStod <> "") Then
                        If (tmpStoa < tmpStod) Or (DayOffset > 0) Then
                            If DayOffset <= 0 Then
                                'Internally identified OverNight Rotation (OVN)
                                DayOffset = 1
                                If DepOvniCol > 2 Then
                                    Select Case ImpOvnFlag
                                        Case 0  'Allowded
                                            FileData(CurFdIdx).SetColumnValue CurLine, DepOvniCol, "V"
                                        Case 1  'Error
                                            FileData(CurFdIdx).SetColumnValue CurLine, DepOvniCol, "!"
                                            SetErrorText Str(DepOvniCol), CurLine, ExtFldCnt, "OVERNIGHT", True, 0
                                        Case 2  'Warn
                                            FileData(CurFdIdx).SetColumnValue CurLine, DepOvniCol, "?"
                                            SetErrorText Str(DepOvniCol), CurLine, ExtFldCnt, "OVERNIGHT", False, 1
                                        Case Else
                                    End Select
                                End If
                            End If
                            tmpStoaDay = ShiftDate(tmpStoaDay, DayOffset)
                            tmpErrFlag = Trim(FileData(CurFdIdx).GetColumnValue(CurLine, 1))
                            If tmpErrFlag = "" Then FileData(CurFdIdx).SetLineColor CurLine, vbBlack, LightGray
                        End If
                    End If
                    If tmpStod <> "" Then
                        tmpStod = tmpStodDay & tmpStod
                        FileData(CurFdIdx).SetColumnValue CurLine, StodColIdx, tmpStod
                        If Not CheckValidCedaTime(tmpStod) Then SetErrorText Str(StodColIdx), CurLine, ExtFldCnt, "DATE/TIME", True, 0
                        If DataSystemType = "OAFOS" Then tmpVpfr = Left(tmpStod, 8)
                    End If
                    If tmpStoa <> "" Then
                        tmpStoa = tmpStoaDay & tmpStoa
                        FileData(CurFdIdx).SetColumnValue CurLine, StoaColIdx, tmpStoa
                        If Not CheckValidCedaTime(tmpStoa) Then SetErrorText Str(StoaColIdx), CurLine, ExtFldCnt, "DATE/TIME", True, 0
                        If DataSystemType = "OAFOS" Then tmpVpto = Left(tmpStoa, 8)
                    End If
                Else
                    tmpStod = FileData(CurFdIdx).GetColumnValue(CurLine, MainVpfrCol)
                    If Not CheckValidCedaTime(tmpStod) Then SetErrorText Str(MainVpfrCol), CurLine, ExtFldCnt, "DATE/TIME", True, 0
                    If MainVpfrCol <> MainVptoCol Then
                        tmpStoa = FileData(CurFdIdx).GetColumnValue(CurLine, MainVptoCol)
                        If Not CheckValidCedaTime(tmpStoa) Then SetErrorText Str(MainVptoCol), CurLine, ExtFldCnt, "DATE/TIME", True, 0
                    End If
                End If
                If tmpVpto > MaxVptoDay Then
                    MaxVptoDay = tmpVpto
                    MaxLinNbr = FileData(CurFdIdx).GetColumnValue(CurLine, 0)
                End If
                If tmpVpfr < MinVpfrDay Then
                    MinVpfrDay = tmpVpfr
                    MinLinNbr = FileData(CurFdIdx).GetColumnValue(CurLine, 0)
                End If
            End If
        Next
        FileData(CurFdIdx).RedrawTab
        If (Trim(ActSeason(0).Tag) = "NEW") Or (Trim(ActSeason(0).Caption) = "") Then
            AutoUpdateMode = CheckAutoUpdateMode
            DetermineValidSeason MinVpfrDay, MaxVptoDay
            ActSeason(1).ToolTipText = "In line " & MinLinNbr
            ActSeason(2).ToolTipText = "In line " & MaxLinNbr
            If InStr(CheckFilePeriod, "YES") > 0 Then
                If ActSeason(0).Caption <> "???" Then
                    tmpSeasData = ActSeason(0).Tag
                    tmpSeasBegin = GetItem(tmpSeasData, 5, ",")
                    tmpSeasEnd = GetItem(tmpSeasData, 6, ",")
                    If (tmpSeasBegin < MinVpfrDay) Or (tmpSeasEnd > MaxVptoDay) Then
                        If InStr(CheckFilePeriod, "ASK") > 0 Then
                            tmpMsg = "The import file's timeframe doesn't" & vbNewLine
                            tmpMsg = tmpMsg & "cover the full " & ActSeason(0).Caption & " seasonal period." & vbNewLine
                            tmpMsg = tmpMsg & "Do you want to proceed in Update mode?"
                            RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Import File " & ActSeason(0).Caption & " Time Period", tmpMsg, "hand", "Yes,No", UserAnswer)
                        Else
                            RetVal = 1
                        End If
                        If RetVal = 1 Then AutoUpdateMode = True
                    Else
                        AutoUpdateMode = False
                        MySetUp.CurFileInfo(5).Caption = "Full"
                    End If
                End If
            End If
            If AutoUpdateMode = True Then MySetUp.CurFileInfo(5).Caption = "Update"
        End If
        
        StatusBar.Panels(7).Text = "Ready"
        Me.Refresh
    End If
End Sub

Public Sub DetermineValidSeason(Vpfr As String, Vpto As String)
    Dim varVpfr
    Dim varVpto
    Dim varNxtDay
    Dim varDiff
    Dim varMidDay
    Dim MidDay As String
    Dim NxtDay As String
    Dim tmpSeaDat As String
    Dim tmpDat As String
    On Error Resume Next
    'MsgBox "Entered 'DetermineValidSeason'"
    ActSeason(1).Caption = DecodeSsimDayFormat(Vpfr, "CEDA", "SSIM2")
    ActSeason(1).Tag = Vpfr
    ActSeason(1).BackColor = LightYellow
    ActSeason(2).Caption = DecodeSsimDayFormat(Vpto, "CEDA", "SSIM2")
    ActSeason(2).Tag = Vpto
    ActSeason(2).BackColor = LightYellow
    varVpfr = CedaDateToVb(Vpfr)
    varVpto = CedaDateToVb(Vpto)
    varDiff = Round(DateDiff("d", varVpto, varVpfr) / 2)
    varMidDay = DateAdd("d", varDiff, varVpto)
    MidDay = Format(varMidDay, "yyyymmdd")
    tmpSeaDat = SeasonData.GetValidSeason(MidDay)
    If tmpSeaDat <> "" Then
        ActSeason(0).Caption = GetItem(tmpSeaDat, 1, ",")
        ActSeason(0).Tag = tmpSeaDat
        tmpDat = GetItem(tmpSeaDat, 4, ",")
        If tmpDat = "" Then tmpDat = GetItem(tmpSeaDat, 1, ",")
        ActSeason(0).ToolTipText = "Season: " & tmpDat
        ActSeason(0).ForeColor = vbBlack
        ActSeason(0).BackColor = LightYellow
    Else
        'ActSeason(0).Caption = DecodeSsimDayFormat(MidDay, "CEDA", "SSIM2")
        ActSeason(0).Caption = "???"
        ActSeason(0).Tag = "ERROR," & MidDay
        ActSeason(0).ToolTipText = "No valid season found."
        ActSeason(0).ForeColor = vbWhite
        ActSeason(0).BackColor = vbRed
    End If
    tmpSeaDat = MySetUp.ServerTime.Tag
    varNxtDay = CedaFullDateToVb(tmpSeaDat)
    varNxtDay = DateAdd("d", Val(MySetUp.AutoClear.Caption), varNxtDay)
    NxtDay = Format(varNxtDay, "yyyymmdd")
    ActSeason(3).Caption = DecodeSsimDayFormat(NxtDay, "CEDA", "SSIM2")
    ActSeason(3).Tag = NxtDay
    ActSeason(3).ForeColor = vbBlack
    ActSeason(3).BackColor = LightestGreen
    chkClear.ToolTipText = "Clear out all flights prior " & ActSeason(3).Caption
End Sub

Public Function BuildSlotRecord(CurLine As Long, FlightLine As Long) As String
Dim ResultRecord As String
Dim StopSearch As Boolean
Dim LegRec As String
Dim pclFullName As String
Dim MaxLine As Long
Dim FirstLine As Long
Dim LastLine As Long
Dim ErrorLine As Long
Dim tmpCol1Val As String
Dim tmpCol2Val As String
Dim tmpFldNam As String
Dim tmpFldVal As String
Dim tmpOrig As String
Dim tmpDest As String
Dim errAdid As String
Dim CurFlno As String
Dim OldFlno As String
Dim CurAdid As String
Dim OldAdid As String
Dim tmpFlcaVal As String
Dim tmpFltnVal As String
Dim tmpFltvVal As String
Dim ErrorTrip As Boolean
Dim ArrivalTrip As Boolean
Dim DepartureTrip As Boolean
Dim ViaTrip As Boolean
Dim FldColIdx As Long
Dim FlcaCol As Long
Dim FltnCol As Long
Dim FltvCol As Long
    Select Case DataSystemType
        Case "SSIM"
            FlcaCol = 2 + 2
            FltnCol = 3 + 2
            FltvCol = 4 + 2
        Case "OAFOS"
            FlcaCol = 1 + 2
            FltnCol = 2 + 2
            FltvCol = 3 + 2
        Case Else
            'hardcode for SSIM
            FlcaCol = 2 + 2
            FltnCol = 3 + 2
            FltvCol = 4 + 2
    End Select
    OldFlno = ""
    CurAdid = "?"
    OldAdid = "?"
    FirstLine = -1
    LastLine = -1
    ErrorLine = -1
    ErrorTrip = False
    ArrivalTrip = False
    DepartureTrip = False
    ViaTrip = False
    MaxLine = FileData(CurFdIdx).GetLineCount + 1
    StopSearch = False
    ResultRecord = ""
    While (CurLine <= MaxLine) And (StopSearch = False)
        tmpFlcaVal = FileData(CurFdIdx).GetColumnValue(CurLine, FlcaCol)
        tmpFltnVal = FileData(CurFdIdx).GetColumnValue(CurLine, FltnCol)
        tmpFltvVal = FileData(CurFdIdx).GetColumnValue(CurLine, FltvCol)
        CurFlno = tmpFlcaVal & tmpFltnVal & "V" & tmpFltvVal
        tmpCol1Val = FileData(CurFdIdx).GetColumnValue(CurLine, 1)
        tmpCol2Val = FileData(CurFdIdx).GetColumnValue(CurLine, 2)
        If (tmpCol1Val = "E") And (ErrorLine < 0) Then
            tmpOrig = FileData(CurFdIdx).GetColumnValue(CurLine, OrgColNbr)
            tmpDest = FileData(CurFdIdx).GetColumnValue(CurLine, DesColNbr)
            If tmpOrig = HomeAirport Then
                'MsgBox CurLine & "|" & tmpOrig & "/" & tmpDest
                errAdid = "D"
                ErrorLine = CurLine
            ElseIf tmpDest = HomeAirport Then
                'MsgBox CurLine & "/" & tmpOrig & "/" & tmpDest
                errAdid = "A"
                ErrorLine = CurLine
            End If
        End If
        If CurFlno <> OldFlno Then
            If OldFlno <> "" Then
                'Run through the flight lines
                'MsgBox OldFlno & " / " & FirstLine + 1 & " / " & LastLine + 1 & " / " & ErrorTrip
                If ErrorTrip Then
                    'MsgBox OldFlno & "/" & FirstLine + 1 & "/" & LastLine + 1 & "/ERROR"
                    SetFlightLineErrors FirstLine, LastLine, LightestRed
                    GetSlotRecInfo ResultRecord, FirstLine, LastLine, FlightLine, CurAdid, False
                    StopSearch = True
                Else
                    GetSlotRecInfo ResultRecord, FirstLine, LastLine, FlightLine, CurAdid, True
                    StopSearch = True
                End If
            End If
            FirstLine = CurLine
            ErrorTrip = False
            ArrivalTrip = False
            DepartureTrip = False
            CurAdid = "?"
            OldFlno = CurFlno
        End If
        If (tmpCol1Val = "A") Or (CurAdid = "?" And tmpCol1Val = "1") Then
            CurAdid = "A"
            ArrivalTrip = True
        ElseIf tmpCol1Val = "D" Then
            CurAdid = tmpCol1Val
            DepartureTrip = True
        ElseIf tmpCol1Val = "B" Then
            'Adid=B
        Else
        End If
        If (tmpCol1Val = "E") Or (tmpCol2Val = "-") Then ErrorTrip = True
        OldFlno = CurFlno
        OldAdid = CurAdid
        LastLine = CurLine
        CurLine = CurLine + 1
    Wend
    If ErrorLine >= 0 Then
        If ErrorLine < FirstLine Then
            'MsgBox "ERROR FROM " & FirstLine & " to " & LastLine & " (" & ErrorLine & ")"
            GetSlotRecInfo ResultRecord, ErrorLine, ErrorLine, ErrorLine, errAdid, True
        End If
    End If
    CurLine = CurLine - 1
    BuildSlotRecord = ResultRecord
End Function

Public Sub GetSlotRecInfo(ResultRecord As String, FirstLine As Long, LastLine As Long, FlightLine As Long, CurAdid As String, ValidFlight As Boolean)
Dim BreakOut As Boolean
Dim FirstRec As String
Dim LastRec As String
Dim FormatList As String
Dim ItmTxt As String
Dim ItmIdx As String
Dim LineCtrl As String
Dim TaskCtrl As String
Dim ColTxt As String
Dim CurItm As Integer
Dim ColIdx As Integer

    'MsgBox FirstLine + 1 & " / " & LastLine + 1 & " / " & CurAdid
    BreakOut = False
    Select Case CurAdid
        Case "A"
            'ExtractArrivals = "LL1,LL2,LL3,LL5,LL6,'','',LL10,LL11,LL12,'',LL21,FL13,F?17,LL18,'','','','',LL9,'','1',FL22"
            FormatList = ExtractArrivals
            FlightLine = LastLine
        Case "D"
            'ExtractDepartures = "FL1,FL2,FL3,'','',FL5,FL6,FL10,FL11,FL12,'',FL21,'','','',FL14,'',L?13,LL17,'',FL9,'1',FL22"
            FormatList = ExtractDepartures
            FlightLine = FirstLine
        Case "B"
            BreakOut = True
            FlightLine = FirstLine
        Case "X"
            'Slot Coord line
            FormatList = ExtractSlotLine
            FlightLine = FirstLine
        Case "F"
            'FHK Format line
            'HardCode
            If GetItem(ResultRecord, 8, ",") = "A" Then
                FormatList = ExtractRotationArr
            Else
                FormatList = ExtractRotationDep
            End If
            FlightLine = FirstLine
        Case Else
            'MsgBox "ADID:" & CurAdid
            BreakOut = True
            FlightLine = FirstLine
    End Select
    ResultRecord = ""
    If Not BreakOut Then
        FirstRec = FileData(CurFdIdx).GetLineValues(FirstLine)
        If LastLine >= 0 Then LastRec = FileData(CurFdIdx).GetLineValues(LastLine)
        CurItm = 0
        Do
            CurItm = CurItm + 1
            ItmTxt = GetItem(FormatList, CurItm, ",")
            If ItmTxt <> "" Then
                LineCtrl = Left(ItmTxt, 1)
                TaskCtrl = Mid(ItmTxt, 2, 1)
                If LineCtrl <> "'" Then
                    ColIdx = Val(Mid(ItmTxt, 3))
                    If ColIdx <= 0 Then
                        MsgBox "Syntax Error 'ColIdx': " & ItmTxt
                    End If
                    ColTxt = ""
                    Select Case TaskCtrl
                        Case "L"
                            'Line Data required
                        Case "?"
                            If FirstLine = LastLine Then LineCtrl = "X"
                        Case Else
                            MsgBox "Syntax Error 'TaskCtrl': " & ItmTxt
                    End Select
                    Select Case LineCtrl
                        Case "F"
                            'First Line
                            ColTxt = GetItem(FirstRec, ColIdx, ",")
                        Case "L"
                            'Last Line
                            ColTxt = GetItem(LastRec, ColIdx, ",")
                        Case "X"
                            'No Line Data required. Use item 2 in 'ItmTxt' as text.
                            ColTxt = GetItem(ItmTxt, 2, ";")
                            ColTxt = Replace(ColTxt, "'", "", 1, -1, vbBinaryCompare)
                        Case "D"
                            'Slot Coord
                            ColTxt = GetItem(FirstRec, ColIdx, ",")
                        Case Else
                            MsgBox "Syntax Error 'LineCtrl': " & ItmTxt
                    End Select
                Else
                    'Direct Text Input
                    ColTxt = Replace(ItmTxt, "'", "", 1, -1, vbBinaryCompare)
                End If
                ResultRecord = ResultRecord & ColTxt & ","
            End If
        Loop While ItmTxt <> ""
        ResultRecord = ResultRecord & Trim(Str(FirstLine)) & ","
        ResultRecord = ResultRecord & Trim(Str(LastLine)) & ","
        ResultRecord = ResultRecord & Trim(Str(FlightLine)) & ","
        If CurAdid = "A" Then
            Select Case DataSystemType
                Case "SSIM"
                    CheckSsimArrFrqd LastRec, ResultRecord
                Case "OAFOS"
                    CheckOaFosArrFrqd LastRec, ResultRecord
                Case Else
            End Select
        End If
    End If
End Sub
Private Sub CheckSsimArrFrqd(SsimRec As String, SlotRec As String)
    Dim tmpStodDay As String
    Dim tmpStoaDay As String
    Dim tmpFrqd As String
    tmpStodDay = Left(GetItem(SsimRec, 14, ","), 8)
    tmpStoaDay = Left(GetItem(SsimRec, 18, ","), 8)
    If tmpStoaDay > tmpStodDay Then
        tmpFrqd = GetItem(SlotRec, 10, ",")
        tmpFrqd = ShiftFrqd(tmpFrqd, 1)
        SetItem SlotRec, 10, ",", tmpFrqd
        tmpStodDay = GetItem(SlotRec, 8, ",")
        tmpStodDay = CedaDateAdd(tmpStodDay, 1)
        SetItem SlotRec, 8, ",", tmpStodDay
        tmpStoaDay = GetItem(SlotRec, 9, ",")
        tmpStoaDay = CedaDateAdd(tmpStoaDay, 1)
        SetItem SlotRec, 9, ",", tmpStoaDay
    End If
End Sub
Private Sub CheckOaFosArrFrqd(OaFosRec As String, SlotRec As String)
    Dim tmpStodDay As String
    Dim tmpStoaDay As String
    Dim tmpFrqd As String
    tmpStodDay = Left(GetItem(OaFosRec, 17, ","), 8)
    tmpStoaDay = Left(GetItem(OaFosRec, 19, ","), 8)
    If tmpStoaDay > tmpStodDay Then
        SetItem SlotRec, 8, ",", tmpStoaDay
        SetItem SlotRec, 9, ",", tmpStoaDay
    End If
End Sub
Private Sub CheckAutoReplace()
    Dim CurCol As Long
    Dim RefCol As Long
    Dim CurItm As Integer
    Dim ColCode As String
    Dim OldTxt As String
    Dim NewTxt As String
    Dim RefTxt As String
    Dim tmpResult As String
    Dim tmpSqlKey As String
    Dim CedaData As String
    Dim llCount As Long
    Dim i As Long
    Dim RetVal As Integer
    Dim tmpColLst As String
    Dim clLineIdx As String
    Dim LinItm As Integer
    Dim llLine As Long
    Dim UsedKeys As String
    Dim ColList As String
    Dim DataList As String
    MySetUp.UsedGldTabKeys = ""
    If (MySetUp.chkWork(2).Value = 1) And (AutoReplaceAll <> "") And (CedaIsConnected) Then
        CurItm = 0
        For CurItm = 1 To ExtFldCnt
            ColCode = GetItem(AutoReplaceAll, CurItm, ",")
            If ColCode <> "" Then
                If InStr(ColCode, "/") > 0 Then
                    RefCol = Val(GetItem(ColCode, 2, "/")) + 2
                    ColCode = GetItem(ColCode, 1, "/")
                Else
                    RefCol = -1
                End If
                tmpSqlKey = "WHERE TYPE='" & DataSystemType & "' AND LKEY='" & ColCode & "' ORDER BY LVAL DESC"
                RetVal = UfisServer.CallCeda(tmpResult, "RTA", "GDLTAB", "LVAL,RVAL", "", tmpSqlKey, "", 1, False, False)
                CurCol = CurItm + 2
                'MsgBox CurCol & vbNewLine & CedaData
                llCount = UfisServer.DataBuffer(1).GetLineCount - 1
                For i = 0 To llCount
                    CedaData = UfisServer.DataBuffer(1).GetLineValues(i)
                    OldTxt = GetItem(CedaData, 1, ",")
                    OldTxt = GetItem(OldTxt, 1, "/")
                    NewTxt = GetItem(CedaData, 2, ",")
                    RefTxt = GetItem(NewTxt, 2, "/")
                    NewTxt = GetItem(NewTxt, 1, "/")
                    If OldTxt <> NewTxt Then
                        If RefTxt = "" Then
                            tmpColLst = FileData(CurFdIdx).GetLinesByColumnValue(CurCol, OldTxt, 0)
                        Else
                            ColList = Trim(Str(RefCol)) & "," & Trim(Str(CurCol))
                            DataList = RefTxt & "," & OldTxt
                            tmpColLst = FileData(CurFdIdx).GetLinesByMultipleColumnValue(ColList, DataList, 0)
                        End If
                        If tmpColLst <> "" Then
                            clLineIdx = "START"
                            LinItm = 0
                            While clLineIdx <> ""
                                LinItm = LinItm + 1
                                clLineIdx = GetItem(tmpColLst, LinItm, ",")
                                If clLineIdx <> "" Then
                                    llLine = Val(clLineIdx)
                                    FileData(CurFdIdx).SetColumnValue llLine, CurCol, NewTxt
                                End If
                            Wend
                            UsedKeys = DataSystemType & "," & ColCode & "," & GetItem(CedaData, 1, ",") & "," & GetItem(CedaData, 2, ",") & ",CNT: " & Trim(Str(LinItm - 1))
                            MySetUp.UsedGldTabKeys = MySetUp.UsedGldTabKeys & UsedKeys & vbLf
                        End If
                    End If
                Next
            End If
        Next
    End If
    MySetUp.chkWork(4).Value = 0
    MySetUp.chkWork(4).Value = 1
End Sub

Public Sub ToggleDateFormat(bpWhat As Boolean)
    Dim CurItm As Integer
    Dim ColNbr As Integer
    Dim ItmTxt As String
    Dim newLen As String
    On Error Resume Next
    If chkExpand.Value = 1 Then
        CurItm = 2
        Do
            ItmTxt = GetItem(DateFields, CurItm, ",")
            If ItmTxt <> "" Then
                ColNbr = Val(ItmTxt) + 2
                If bpWhat = True Then   'User Format
                    FileData(CurFdIdx).DateTimeSetColumn ColNbr
                    FileData(CurFdIdx).DateTimeSetInputFormatString ColNbr, "YYYYMMDD"
                    FileData(CurFdIdx).DateTimeSetOutputFormatString ColNbr, "DDMMMYY"
                    ItmTxt = Right("000" + Trim(Str(ColNbr)), 3)
                    If InStr(ActiveTimeFields, ItmTxt) = 0 Then ActiveTimeFields = ActiveTimeFields + ItmTxt + ","
                    ColNbr = ColNbr + 1
                    newLen = TextWidth("01OCT00") / Screen.TwipsPerPixelX + 6
                    SetItem CurHeaderLength, ColNbr, ",", newLen
                Else
                    FileData(CurFdIdx).DateTimeResetColumn ColNbr
                    ColNbr = ColNbr + 1
                    newLen = TextWidth("20001001") / Screen.TwipsPerPixelX + 6
                    SetItem CurHeaderLength, ColNbr, ",", newLen
                End If
            End If
            CurItm = CurItm + 1
        Loop While ItmTxt <> ""
        CurItm = 2
        Do
            ItmTxt = GetItem(TimeFields, CurItm, ",")
            If ItmTxt <> "" Then
                ColNbr = Val(ItmTxt) + 2
                If bpWhat = True Then
                    FileData(CurFdIdx).DateTimeSetColumn ColNbr
                    FileData(CurFdIdx).DateTimeSetInputFormatString ColNbr, "YYYYMMDDhhmm"
                    FileData(CurFdIdx).DateTimeSetOutputFormatString ColNbr, "DDMMMYY'/'hhmm"
                    ItmTxt = Right("000" + Trim(Str(ColNbr)), 3)
                    If InStr(ActiveTimeFields, ItmTxt) = 0 Then ActiveTimeFields = ActiveTimeFields + ItmTxt + ","
                    ColNbr = ColNbr + 1
                    newLen = TextWidth("01OCT00/1200") / Screen.TwipsPerPixelX + 6
                    SetItem CurHeaderLength, ColNbr, ",", newLen
                Else
                    FileData(CurFdIdx).DateTimeResetColumn ColNbr
                    ColNbr = ColNbr + 1
                    newLen = TextWidth("200010011200") / Screen.TwipsPerPixelX + 6
                    SetItem CurHeaderLength, ColNbr, ",", newLen
                End If
            End If
            CurItm = CurItm + 1
        Loop While ItmTxt <> ""
        'FileData(CurFdIdx).HeaderLengthString = CurHeaderLength
        FileData(CurFdIdx).AutoSizeColumns
        CheckMainHeader CurFdIdx
        FileData(CurFdIdx).Refresh
    End If
End Sub

Private Sub ResetTimeFields()
    Dim CurCol As Long
    Dim ItmTxt As String
    Dim ItmNbr As Integer
    ItmNbr = 0
    ItmTxt = "START"
    While ItmTxt <> ""
        ItmNbr = ItmNbr + 1
        ItmTxt = GetItem(ActiveTimeFields, ItmNbr, ",")
        If ItmTxt <> "" Then
            CurCol = Val(ItmTxt)
            FileData(CurFdIdx).DateTimeResetColumn CurCol
        End If
    Wend
    ActiveTimeFields = ""
End Sub

Public Sub InitPeriodCheck(TimePeriodColumns As String, InitMain As Boolean)
    If InitMain Then
        MainVpfrCol = Val(GetItem(TimePeriodColumns, 1, ",")) + 2
        MainVptoCol = Val(GetItem(TimePeriodColumns, 2, ",")) + 2
        MainFredCol = Val(GetItem(TimePeriodColumns, 3, ",")) + 2
        MainFrewCol = Val(GetItem(TimePeriodColumns, 4, ",")) + 2
        MainStoaCol = Val(GetItem(TimePeriodColumns, 5, ",")) + 2
        MainStodCol = Val(GetItem(TimePeriodColumns, 6, ",")) + 2
        MainDaofCol = Val(GetItem(TimePeriodColumns, 7, ",")) + 2
        MainChkFred = "," & GetItem(MainPeriodCols, 3, ",") & ","
    End If
    CheckFlightPeriod TimePeriodColumns, "", "", True, False, False, ""
End Sub

Public Function CheckFlightPeriod(Spfr As String, Spto As String, FltRecord As String, RetVal As Boolean, ForCheck As Boolean, CutOut As Boolean, FredFilter As String) As Boolean
    Static VpfrItm As Integer
    Static VptoItm As Integer
    Static FrqdItm As Integer
    Static FrqwItm As Integer
    Static StoaItm As Integer
    Static StodItm As Integer
    Static DaofItm As Integer
    Dim Result As Boolean
    Dim Vpfr As String
    Dim Vpto As String
    Dim Frqd As String
    Dim OldFrqd As String
    Dim Frqw As String
    Dim FirstDay As String
    Dim Stoa As String
    Dim Stod As String
    Result = RetVal
    If ForCheck = True Then
        If Result = True Then
            FirstDay = ""
            Vpfr = Left(GetItem(FltRecord, VpfrItm, ","), 8)
            Vpto = Left(GetItem(FltRecord, VptoItm, ","), 8)
            Frqd = GetItem(FltRecord, FrqdItm, ",")
            OldFrqd = Frqd
            Frqw = GetItem(FltRecord, FrqwItm, ",")
            If Frqw = "" Then Frqw = "1"
            If (Vpfr > Spto) Or (Vpto < Spfr) Then
                Result = False
            Else
                If VpfrItm <> VptoItm Then
                    If (CutOut = True) And (Vpfr < Spfr) Then
                        Vpfr = Spfr
                        SetItem FltRecord, VpfrItm, ",", Vpfr
                    End If
                    If (CutOut = True) And (Vpto > Spto) Then
                        Vpto = Spto
                        SetItem FltRecord, VptoItm, ",", Vpto
                    End If
                    If FredFilter <> "" Then Frqd = PatchFrqDays(Frqd, FredFilter)
                    If Validation.CheckPeriodData(Vpfr, Vpto, Frqd, Frqw, FirstDay, True) <> 0 Then Result = False
                    If Frqd <> OldFrqd Then SetItem FltRecord, FrqdItm, ",", Frqd
                    If Result = True Then
                        If FirstDay = "" Then
                            'Here we have a problem
                            'MsgBox "No FirstDay found!" & vbNewLine & FltRecord
                            FirstDay = Vpfr
                        End If
                        If StoaItm > 3 Then Stoa = GetItem(FltRecord, StoaItm, ",") Else Stoa = ""
                        If StodItm > 3 Then Stod = GetItem(FltRecord, StodItm, ",") Else Stod = ""
                        PatchTimeChain FirstDay, Stoa, Stod
                        If StoaItm > 3 Then SetItem FltRecord, StoaItm, ",", Stoa
                        If StodItm > 3 Then SetItem FltRecord, StodItm, ",", Stod
                        If DataSystemType = "OAFOS" Then
                            If Trim(Stoa) <> "" Then
                                If Stoa < FlightExtract.txtVpfr(3).Tag Then Result = False
                            End If
                            If Trim(Stod) <> "" Then
                                If Stod < FlightExtract.txtVpfr(3).Tag Then Result = False
                            End If
                        End If
                    End If
                Else
                    If FredFilter <> "" Then
                        Frqd = GetCedaWeekDay(Vpfr)
                        If InStr(FredFilter, Frqd) = 0 Then Result = False
                    End If
                End If
            End If
        End If
    Else
        'For Init
        VpfrItm = Val(GetItem(Spfr, 1, ",")) + 3
        VptoItm = Val(GetItem(Spfr, 2, ",")) + 3
        FrqdItm = Val(GetItem(Spfr, 3, ",")) + 3
        FrqwItm = Val(GetItem(Spfr, 4, ",")) + 3
        StoaItm = Val(GetItem(Spfr, 5, ",")) + 3
        StodItm = Val(GetItem(Spfr, 6, ",")) + 3
        DaofItm = Val(GetItem(Spfr, 7, ",")) + 3
    End If
    CheckFlightPeriod = Result
End Function

Private Function PatchFrqDays(CurFred As String, FilterFred As String) As String
    Dim Result As String
    Dim i As Integer
    Dim tmpChr As String
    Result = CurFred
    If FilterFred <> "......." Then
        For i = 1 To 7
            tmpChr = Mid(FilterFred, i, 1)
            If tmpChr = "." Then Mid(Result, i, 1) = "."
        Next
    End If
    PatchFrqDays = Result
End Function
Private Sub PatchTimeChain(FirstDay As String, Stoa As String, Stod As String)
    Dim StoaDay As String
    Dim StodDay As String
    Dim varFirstDay
    Dim varStoaDay
    Dim varStodDay
    Dim DayOffset As Long
    If Len(RTrim(Stoa)) = 4 Then Stoa = FirstDay + Stoa
    If Len(RTrim(Stod)) = 4 Then Stod = FirstDay + Stod
    If (RTrim(Stoa) <> "") And (RTrim(Stod) <> "") Then
        StoaDay = Left(Stoa, 8)
        StodDay = Left(Stod, 8)
        varFirstDay = CedaDateToVb(FirstDay)
        varStoaDay = CedaDateToVb(StoaDay)
        varStodDay = CedaDateToVb(StodDay)
        DayOffset = DateDiff("d", varStoaDay, varStodDay)
        varStodDay = DateAdd("d", DayOffset, varFirstDay)
        StodDay = Format(varStodDay, "yyyymmdd")
        StoaDay = Format(varFirstDay, "yyyymmdd")
        Mid(Stoa, 1, 8) = StoaDay
        Mid(Stod, 1, 8) = StodDay
    ElseIf RTrim(Stoa) <> "" Then
        Mid(Stoa, 1, 8) = FirstDay
    ElseIf RTrim(Stod) <> "" Then
        Mid(Stod, 1, 8) = FirstDay
    End If
End Sub

Private Sub WarnCnt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Val(WarnCnt.Caption) > 0 Then WarnCnt.MousePointer = 14 Else WarnCnt.MousePointer = 0
End Sub

Private Sub StoreLineStatus()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Dim StatCol As Long
    Dim CurStat As String
    StatCol = ExtFldCnt + 2
    MaxLine = FileData(CurFdIdx).GetLineCount - 1
    For CurLine = 0 To MaxLine
        FileData(CurFdIdx).GetLineColor CurLine, CurForeColor, CurBackColor
        If CurBackColor <> vbWhite Then
            CurStat = Trim(Str(CurForeColor)) & "/" & Trim(Str(CurBackColor))
            FileData(CurFdIdx).SetColumnValue CurLine, StatCol, CurStat
        End If
    Next
End Sub
Private Sub RestoreLineStatus()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Dim StatCol As Long
    Dim CurStat As String
    StatCol = ExtFldCnt + 2
    MaxLine = FileData(CurFdIdx).GetLineCount - 1
    For CurLine = 0 To MaxLine
        CurStat = FileData(CurFdIdx).GetColumnValue(CurLine, StatCol)
        If CurStat <> "" Then
            CurForeColor = Val(GetItem(CurStat, 1, "/"))
            CurBackColor = Val(GetItem(CurStat, 2, "/"))
            FileData(CurFdIdx).SetLineColor CurLine, CurForeColor, CurBackColor
            FileData(CurFdIdx).SetColumnValue CurLine, StatCol, ""
        End If
    Next
End Sub

Private Sub InitFreeConfigData()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim MaxItem As Long
    Dim LineData As String
    FormatType = CfgTool.FileFormatType.Text
    ItemSep = ";"
    MainExpandHeader = BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(4), 2, -1, ",")
    MaxItem = ItemCount(MainExpandHeader, ",") + 2
    ExpPosLst = BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(5), 2, -1, ",")
    MainFldWidLst = BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(6), 2, -1, ",")
    NormalHeader = BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(1), 2, -1, ",")
    FldPosLst = BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(2), 2, -1, ",")
    FldLenLst = BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(3), 2, -1, ",")
    DateFields = "," & BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(10), 2, MaxItem, ",") & ","
    TimeFields = "," & BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(11), 2, MaxItem, ",") & ","
    TimeChainInfo = BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(13), 2, -1, ",")
    MainPeriodCols = BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(12), 2, 7, ",")
    CheckViaList = BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(14), 2, 7, ",")
    AutoReplaceAll = BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(8), 2, MaxItem, ",")
    AutoReplaceAll = Replace(AutoReplaceAll, "-99", "", 1, -1, vbBinaryCompare)
    CheckRotation = BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(15), 2, 3, ",")
    ChkBasDat = BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(9), 2, MaxItem, ",")
    ChkBasDat = Replace(ChkBasDat, "|", ",", 1, -1, vbBinaryCompare)
    ExtractSlotLine = BuildCleanItems(CfgTool.CfgTab(1).GetLineValues(25), 2, 25, ",")
    ExtractSlotLine = Replace(ExtractSlotLine, "-99", "''", 1, -1, vbBinaryCompare)
    InitPeriodCheck MainPeriodCols, True
    
    'STILL HARD CODED PARTS
    '======================
    DateFieldFormat = "SSIM2"
    ExtractSystemType = 1
    DefaultFtyp = "S"
End Sub
' For Config Tool Purposes
Public Function BuildCleanItems(RecData As String, FirstItem As Long, LastItem As Long, RecDeli As String) As String
    Dim Result As String
    Dim CurItm As Long
    Dim ItmData As String
    Result = ""
    CurItm = FirstItem
    ItmData = GetRealItem(RecData, CurItm, RecDeli)
    While ((ItmData <> "") And (ItmData <> "-99")) Or (CurItm <= LastItem)
        If ItmData <> "" Then Result = Result & RecDeli & ItmData
        CurItm = CurItm + 1
        ItmData = GetRealItem(RecData, CurItm, RecDeli)
    Wend
    Result = Mid(Result, 2)
    BuildCleanItems = Result
End Function

Private Sub ToggleLocalToUtc(LocalToUtc As Boolean)
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpData As String
    Dim VpfrCol As Long
    Dim VptoCol As Long
    Dim FreqCol As Long
    Dim FredCol As Long
    Dim Time1Col As Long
    Dim Time2Col As Long
    Dim DaofCol As Long
    Dim CurDateDiff As Long
    Dim CurForeColor As Long
    Dim CurBackColor As Long
    Dim CurVpfr As String
    Dim CurVpto As String
    Dim CurFreq As String
    Dim CurTime1 As String
    Dim CurTime2 As String
    Dim CurDaof As String
    Dim NewVpfr As String
    Dim NewVpto As String
    Dim newFreq As String
    Dim NewTime1 As String
    Dim NewTime2 As String
    Dim NewDaof As String
    
    Screen.MousePointer = 11
    VpfrCol = MainVpfrCol
    VptoCol = MainVptoCol
    FreqCol = MainFredCol
    Time1Col = MainStoaCol
    Time2Col = MainStodCol
    DaofCol = MainDaofCol
    MaxLine = FileData(CurFdIdx).GetLineCount - 1
    For CurLine = 0 To MaxLine
        If Time1Col > 2 Then
            CurTime1 = FileData(CurFdIdx).GetColumnValue(CurLine, Time1Col)
            If LocalToUtc Then NewTime1 = ApcLocalToUtc(HomeAirport, CurTime1) Else NewTime1 = ApcUtcToLocal(HomeAirport, CurTime1)
            FileData(CurFdIdx).SetColumnValue CurLine, Time1Col, NewTime1
            CurDateDiff = CedaDateDiff(CurTime1, NewTime1)
            If CurDateDiff <> 0 Then
                If VpfrCol > 2 Then
                    CurVpfr = FileData(CurFdIdx).GetColumnValue(CurLine, VpfrCol)
                    NewVpfr = CedaDateAdd(CurVpfr, CInt(CurDateDiff))
                    FileData(CurFdIdx).SetColumnValue CurLine, VpfrCol, NewVpfr
                End If
                If VptoCol > 2 Then
                    CurVpto = FileData(CurFdIdx).GetColumnValue(CurLine, VptoCol)
                    NewVpto = CedaDateAdd(CurVpto, CInt(CurDateDiff))
                    FileData(CurFdIdx).SetColumnValue CurLine, VptoCol, NewVpto
                End If
                If FreqCol > 2 Then
                    CurFreq = FileData(CurFdIdx).GetColumnValue(CurLine, FreqCol)
                    newFreq = ShiftFrqd(CurFreq, CInt(CurDateDiff))
                    FileData(CurFdIdx).SetColumnValue CurLine, FreqCol, newFreq
                End If
            End If
        End If
        If Time2Col > 2 Then
            CurTime2 = FileData(CurFdIdx).GetColumnValue(CurLine, Time2Col)
            If LocalToUtc Then NewTime2 = ApcLocalToUtc(HomeAirport, CurTime2) Else NewTime2 = ApcUtcToLocal(HomeAirport, CurTime2)
            FileData(CurFdIdx).SetColumnValue CurLine, Time2Col, NewTime2
            If DepFreqCol > 2 Then
                CurDateDiff = CedaDateDiff(CurTime2, NewTime2)
                If CurDateDiff <> 0 Then
                    CurFreq = FileData(CurFdIdx).GetColumnValue(CurLine, DepFreqCol)
                    newFreq = ShiftFrqd(CurFreq, CInt(CurDateDiff))
                    FileData(CurFdIdx).SetColumnValue CurLine, DepFreqCol, newFreq
                End If
            End If
        End If
        If DaofCol > 2 Then
            If (Time1Col > 2) And (Time2Col > 2) Then
                CurDaof = Trim(FileData(CurFdIdx).GetColumnValue(CurLine, DaofCol))
                CurDateDiff = CedaDateDiff(NewTime1, NewTime2)
                NewDaof = Trim(Str(CurDateDiff))
                If CurDateDiff <> 0 Then
                    NewDaof = Trim(Str(CurDateDiff))
                Else
                    NewDaof = ""
                End If
                FileData(CurFdIdx).SetColumnValue CurLine, DaofCol, NewDaof
                If NewDaof <> CurDaof Then
                    FileData(CurFdIdx).GetLineColor CurLine, CurForeColor, CurBackColor
                    If (CurBackColor = vbWhite) Or (CurBackColor = LightGray) Then
                        If CurDateDiff <> 0 Then
                            FileData(CurFdIdx).SetLineColor CurLine, vbBlack, LightGray
                        Else
                            FileData(CurFdIdx).SetLineColor CurLine, vbBlack, vbWhite
                        End If
                    End If
                End If
            End If
        End If
    Next
    FileData(CurFdIdx).Refresh
    Screen.MousePointer = 0
End Sub

Private Sub TranslateIcaoCodes(UseLine As Long, UseField As String)
    Dim LookList As String
    Dim ActLineList As String
    Dim ImpLineList As String
    Dim UseFieldList As String
    Dim LookType As String
    Dim CurAct5 As String
    Dim CurAct3 As String
    Dim CurActi As String
    Dim CurLine As String
    Dim CurField As String
    Dim CurValue As String
    Dim ActItemNo As Long
    Dim ImpItemNo As Long
    Dim ActLineNo As Long
    Dim ImpLineNo As Long
    Dim ActLineCount As Integer
    Dim LookCol As Long
    Dim SetCol As Long
    Dim Act3Col As Long
    Dim Act5Col As Long
    Dim ActiCol As Long
    Dim SortCol As Long
    Dim CurItm As Integer
    Dim ItmCnt As Integer
    Dim CheckEmpty As Boolean
    UseFieldList = ""
    If UseField = "" Then
        UseFieldList = TranslateIcao
        CheckEmpty = True
    Else
        If InStr(TranslateIcao, UseField) > 0 Then
            UseFieldList = UseField
            CheckEmpty = False
        End If
    End If
    If UseFieldList <> "" Then
        StatusBar.Panels(7).Text = "Translate Aircraft Codes"
        Act5Col = CLng(GetItemNo(MainFieldList, "ACT5")) + 2
        If Act5Col > 2 Then
            Act3Col = CLng(GetItemNo(MainFieldList, "ACT3")) + 2
            Act5Col = CLng(GetItemNo(MainFieldList, "ACT5")) + 2
            ActiCol = CLng(GetItemNo(MainFieldList, "ACTI")) + 2
            ItmCnt = ItemCount(UseFieldList, ",")
            For CurItm = 1 To ItmCnt
                CurField = GetItem(UseFieldList, CurItm, ",")
                If CurField <> "" Then
                    SortCol = -1
                    If CurField = "ACT5" Then
                        LookCol = Act5Col
                        SetCol = Act3Col
                        SortCol = 1
                    End If
                    If CurField = "ACT3" Then
                        LookCol = Act3Col
                        SetCol = Act5Col
                        SortCol = 0
                    End If
                    If SortCol >= 0 Then
                        StatusBar.Panels(7).Text = "Translate Aircraft Codes (" & CurField & ") ..."
                        If UfisServer.BasicData(6).GetLineCount <= 0 Then
                            UfisServer.BasicDataInit 6, SortCol, "ACTTAB", "ACT3,ACT5,ACTI", ""
                        Else
                            UfisServer.BasicData(6).Sort SortCol, True, True
                        End If
                        LookList = FileData(CurFdIdx).SelectDistinct(Str(LookCol), "", "", ",", True)
                        LookList = Replace(LookList, ",,", ",", 1, -1, vbBinaryCompare)
                        If Left(LookList, 1) = "," Then LookList = Mid(LookList, 2)
                        FileData(CurFdIdx).IndexCreate "ACTYPE", LookCol
                        ActItemNo = 0
                        LookType = GetRealItem(LookList, ActItemNo, ",")
                        While LookType <> ""
                            ActLineList = UfisServer.BasicData(6).GetLinesByColumnValue(SortCol, LookType, 0)
                            If ActLineList <> "" Then
                                ActLineCount = ItemCount(ActLineList, ",")
                                ActLineNo = Val(GetRealItem(ActLineList, 0, ","))
                                CurAct3 = UfisServer.BasicData(6).GetColumnValue(ActLineNo, 0)
                                CurAct5 = UfisServer.BasicData(6).GetColumnValue(ActLineNo, 1)
                                CurActi = UfisServer.BasicData(6).GetColumnValue(ActLineNo, 2)
                                ImpLineList = FileData(CurFdIdx).GetLinesByIndexValue("ACTYPE", LookType, 0)
                                ImpItemNo = 0
                                CurLine = GetRealItem(ImpLineList, ImpItemNo, ",")
                                While CurLine <> ""
                                    ImpLineNo = Val(CurLine)
                                    If (UseLine >= 0) And (ImpLineNo <> UseLine) Then ImpLineNo = -1
                                    If ImpLineNo >= 0 Then
                                        CurValue = Trim(FileData(CurFdIdx).GetColumnValue(CurLine, SetCol))
                                        If (CurValue = "") Or (Not CheckEmpty) Then
                                            If Act3Col > 2 Then FileData(CurFdIdx).SetColumnValue ImpLineNo, Act3Col, CurAct3
                                            If Act5Col > 2 Then FileData(CurFdIdx).SetColumnValue ImpLineNo, Act5Col, CurAct5
                                            If ActiCol > 2 Then FileData(CurFdIdx).SetColumnValue ImpLineNo, ActiCol, CurActi
                                            If ActLineCount > 1 Then SetErrorText Str(LookCol), ImpLineNo, ExtFldCnt, LookType & " NOT UNIQUE", False, 1
                                        End If
                                    End If
                                    ImpItemNo = ImpItemNo + 1
                                    CurLine = GetRealItem(ImpLineList, ImpItemNo, ",")
                                Wend
                            End If
                            ActItemNo = ActItemNo + 1
                            LookType = GetRealItem(LookList, ActItemNo, ",")
                        Wend
                        FileData(CurFdIdx).AutoSizeColumns
                        FileData(CurFdIdx).Refresh
                    End If
                End If
            Next
        End If
        FileData(CurFdIdx).Refresh
        StatusBar.Panels(7).Text = ""
    End If
End Sub

Private Sub TranslateAirportCodes(UseLine As Long, UseField As String)
    Dim LookList As String
    Dim ApcLineList As String
    Dim ImpLineList As String
    Dim UseFieldList As String
    Dim LookType As String
    Dim CurApc4 As String
    Dim CurApc3 As String
    Dim CurLine As String
    Dim CurField As String
    Dim CurValue As String
    Dim ApcItemNo As Long
    Dim ImpItemNo As Long
    Dim ApcLineNo As Long
    Dim ImpLineNo As Long
    Dim ApcLineCount As Integer
    Dim LookCol As Long
    Dim SetCol As Long
    Dim Apc3Col As Long
    Dim Apc4Col As Long
    Dim SortCol As Long
    Dim CurItm As Integer
    Dim CheckEmpty As Boolean
    UseFieldList = ""
    If UseField = "" Then
        UseFieldList = TranslateApcList
        CheckEmpty = True
    Else
        If InStr(TranslateApcList, UseField) > 0 Then
            UseFieldList = UseField
            CheckEmpty = False
        End If
    End If
    If UseFieldList <> "" Then
        StatusBar.Panels(7).Text = "Translate Airport Codes ..."
        For CurItm = 1 To 10
            CurField = GetItem(TranslateApcList, CurItm, ",")
            If CurField <> "" Then
                SortCol = -1
                If CurField = "ORG3" Then
                    Apc3Col = CLng(GetItemNo(MainFieldList, "ORG3")) + 2
                    Apc4Col = CLng(GetItemNo(MainFieldList, "ORG4")) + 2
                    LookCol = Apc3Col
                    SetCol = Apc4Col
                    SortCol = 0
                End If
                If CurField = "ORG4" Then
                    Apc3Col = CLng(GetItemNo(MainFieldList, "ORG3")) + 2
                    Apc4Col = CLng(GetItemNo(MainFieldList, "ORG4")) + 2
                    LookCol = Apc4Col
                    SetCol = Apc3Col
                    SortCol = 1
                End If
                If CurField = "VIA3" Then
                    Apc3Col = CLng(GetItemNo(MainFieldList, "VIA3")) + 2
                    Apc4Col = CLng(GetItemNo(MainFieldList, "VIA4")) + 2
                    LookCol = Apc3Col
                    SetCol = Apc4Col
                    SortCol = 0
                End If
                If CurField = "VIA4" Then
                    Apc3Col = CLng(GetItemNo(MainFieldList, "VIA3")) + 2
                    Apc4Col = CLng(GetItemNo(MainFieldList, "VIA4")) + 2
                    LookCol = Apc4Col
                    SetCol = Apc3Col
                    SortCol = 1
                End If
                If CurField = "VSA3" Then
                    Apc3Col = CLng(GetItemNo(MainFieldList, "VSA3")) + 2
                    Apc4Col = CLng(GetItemNo(MainFieldList, "VSA4")) + 2
                    LookCol = Apc3Col
                    SetCol = Apc4Col
                    SortCol = 0
                End If
                If CurField = "VSA4" Then
                    Apc3Col = CLng(GetItemNo(MainFieldList, "VSA3")) + 2
                    Apc4Col = CLng(GetItemNo(MainFieldList, "VSA4")) + 2
                    LookCol = Apc4Col
                    SetCol = Apc3Col
                    SortCol = 1
                End If
                If CurField = "VSD3" Then
                    Apc3Col = CLng(GetItemNo(MainFieldList, "VSD3")) + 2
                    Apc4Col = CLng(GetItemNo(MainFieldList, "VSD4")) + 2
                    LookCol = Apc3Col
                    SetCol = Apc4Col
                    SortCol = 0
                End If
                If CurField = "VSD4" Then
                    Apc3Col = CLng(GetItemNo(MainFieldList, "VSD3")) + 2
                    Apc4Col = CLng(GetItemNo(MainFieldList, "VSD4")) + 2
                    LookCol = Apc4Col
                    SetCol = Apc3Col
                    SortCol = 1
                End If
                If CurField = "DES3" Then
                    Apc3Col = CLng(GetItemNo(MainFieldList, "DES3")) + 2
                    Apc4Col = CLng(GetItemNo(MainFieldList, "DES4")) + 2
                    LookCol = Apc3Col
                    SetCol = Apc4Col
                    SortCol = 0
                End If
                If CurField = "DES4" Then
                    Apc3Col = CLng(GetItemNo(MainFieldList, "DES3")) + 2
                    Apc4Col = CLng(GetItemNo(MainFieldList, "DES4")) + 2
                    LookCol = Apc4Col
                    SetCol = Apc3Col
                    SortCol = 1
                End If
                If SortCol >= 0 Then
                    If UseLine >= 0 Then
                        CurValue = Trim(FileData(CurFdIdx).GetColumnValue(UseLine, LookCol))
                        If (CurValue = "") Then
                            FileData(CurFdIdx).SetColumnValue UseLine, SetCol, ""
                            SortCol = -1
                        End If
                    End If
                End If
                If SortCol >= 0 Then
                    StatusBar.Panels(7).Text = "Translate Airport Codes (" & CurField & ") ..."
                    If UfisServer.BasicData(7).GetLineCount <= 0 Then
                        UfisServer.BasicDataInit 7, SortCol, "APTTAB", "APC3,APC4", ""
                    Else
                        UfisServer.BasicData(7).Sort SortCol, True, True
                    End If
                    LookList = FileData(CurFdIdx).SelectDistinct(Str(LookCol), "", "", ",", True)
                    LookList = Replace(LookList, ",,", ",", 1, -1, vbBinaryCompare)
                    If Left(LookList, 1) = "," Then LookList = Mid(LookList, 2)
                    FileData(CurFdIdx).IndexCreate "APCODE", LookCol
                    ApcItemNo = 0
                    LookType = GetRealItem(LookList, ApcItemNo, ",")
                    While LookType <> ""
                        ApcLineList = UfisServer.BasicData(7).GetLinesByColumnValue(SortCol, LookType, 0)
                        If ApcLineList <> "" Then
                            ApcLineCount = ItemCount(ApcLineList, ",")
                            ApcLineNo = Val(GetRealItem(ApcLineList, 0, ","))
                            CurApc3 = UfisServer.BasicData(7).GetColumnValue(ApcLineNo, 0)
                            CurApc4 = UfisServer.BasicData(7).GetColumnValue(ApcLineNo, 1)
                            ImpLineList = FileData(CurFdIdx).GetLinesByIndexValue("APCODE", LookType, 0)
                            ImpItemNo = 0
                            CurLine = GetRealItem(ImpLineList, ImpItemNo, ",")
                            While CurLine <> ""
                                ImpLineNo = Val(CurLine)
                                If (UseLine >= 0) And (ImpLineNo <> UseLine) Then ImpLineNo = -1
                                If ImpLineNo >= 0 Then
                                    CurValue = Trim(FileData(CurFdIdx).GetColumnValue(CurLine, SetCol))
                                    If (CurValue = "") Or (Not CheckEmpty) Then
                                        If Apc3Col > 2 Then FileData(CurFdIdx).SetColumnValue ImpLineNo, Apc3Col, CurApc3
                                        If Apc4Col > 2 Then FileData(CurFdIdx).SetColumnValue ImpLineNo, Apc4Col, CurApc4
                                        If ApcLineCount > 1 Then SetErrorText Str(LookCol), ImpLineNo, ExtFldCnt, LookType & " NOT UNIQUE", False, 1
                                    End If
                                End If
                                ImpItemNo = ImpItemNo + 1
                                CurLine = GetRealItem(ImpLineList, ImpItemNo, ",")
                            Wend
                        End If
                        ApcItemNo = ApcItemNo + 1
                        LookType = GetRealItem(LookList, ApcItemNo, ",")
                    Wend
                    FileData(CurFdIdx).AutoSizeColumns
                    FileData(CurFdIdx).Refresh
                End If
            End If
        Next
        FileData(CurFdIdx).Refresh
        StatusBar.Panels(7).Text = ""
    End If
End Sub

Private Sub CheckMandatoryColumns()
    Dim MandFields As String
    Dim LineNo As Long
    Dim CurFld As Long
    Dim CurCol As Long
    Dim CurFldNam As String
    'MandFields = "ACT3,ORG3,DES3"
    MandFields = "ACT3"
    FileData(CurFdIdx).IndexDestroy "MAND"
    CurFldNam = "START"
    CurFld = 0
    While CurFldNam <> ""
        CurFldNam = GetRealItem(MandFields, CurFld, ",")
        If CurFldNam <> "" Then
            CurCol = CLng(GetItemNo(MainFieldList, CurFldNam)) + 2
            If CurCol > 2 Then
                FileData(CurFdIdx).IndexCreate "MAND", CurCol
                FileData(CurFdIdx).SetInternalLineBuffer True
                LineNo = Val(FileData(CurFdIdx).GetLinesByIndexValue("MAND", "", 0))
                While LineNo >= 0
                    LineNo = FileData(CurFdIdx).GetNextResultLine
                    If LineNo >= 0 Then SetErrorText CStr(CurCol), LineNo, ExtFldCnt, CurFldNam & "?", True, 0
                Wend
                FileData(CurFdIdx).SetInternalLineBuffer False
                FileData(CurFdIdx).IndexDestroy "MAND"
            End If
        End If
        CurFld = CurFld + 1
    Wend
End Sub
Public Function AskForBasicData(Index As Integer, TabBounds As String, MsgCaption As String, TabHeader As String, MsgLabel As String) As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim ReqList As String
    Dim AskText As String
    Dim lblText As String
    lblText = MsgLabel
    ReqList = ""
    MaxLine = UfisServer.BasicData(Index).GetLineCount - 1
    For CurLine = 0 To MaxLine
        ReqList = ReqList & UfisServer.BasicData(Index).GetLineValues(CurLine) & vbLf
    Next
    AskText = "{=LABEL=}" & lblText
    AskText = AskText & "{=DATA=}" & ReqList
    AskText = AskText & "{=HEAD=}" & TabHeader
    AskText = AskText & "{=SIZE=}" & TabBounds
    MyMsgBox.CallAskUser 1, 0, 0, MsgCaption, AskText, "hand", "", UserAnswer
    AskForBasicData = UserAnswer
End Function

Private Sub ResetApplication(Index As Integer)
    Dim i As Integer
    On Error Resume Next
    IsResetApplication = True
    Timer2.Enabled = False
    Timer2.Tag = ""
    IsDoubleGridView = False
    DoubleGridInit = False
    If Not SectionPushed Then
        SetGridButtons False, -1
    End If
    For i = 0 To chkImpTyp.UBound
        If (i <> Index) Then
            chkImpTyp(i).Value = 0
            chkImpTyp(i).BackColor = vbButtonFace
            chkImpTyp(i).Enabled = True
        ElseIf chkImpTyp(i).Value = 0 Then
            chkImpTyp(i).BackColor = vbButtonFace
            chkImpTyp(i).Enabled = True
        End If
    Next
    PushedImpButton = -1
    CurFdIdx = 0
    If FileData.Count > 1 Then Unload FileData(1)
    PrepareFileData
    ResetMain 0, False
    ArrangeMainScreen "INIT"
    IsResetApplication = False
    Me.Caption = MainTitle
    DoEvents
End Sub
Private Sub SetGridButtons(SetEnable As Boolean, PushIdx As Integer)
    Dim i As Integer
    chkGrid(2).Enabled = SetEnable
    For i = 0 To chkGrid.UBound
        chkGrid(i).Enabled = SetEnable
        chkGrid(i).Value = 0
        chkGrid(i).BackColor = vbButtonFace
    Next
    If PushIdx >= 0 Then chkGrid(PushIdx).Value = 1
End Sub

Private Sub GetDefaultSeason(ShowIt As Boolean)
    Dim tmpCurDate As String
    Dim tmpData As String
    Dim tmpNxtDay As String
    Dim varNxtDay
    tmpCurDate = Format(Now, "yyyymmdd")
    tmpData = SeasonData.GetValidSeason(tmpCurDate)
    CurSeasData = tmpData
    CurSeasName = GetItem(tmpData, 1, ",")
    CurSeasVpfr = GetItem(tmpData, 5, ",")
    CurSeasVpto = GetItem(tmpData, 6, ",")
    If ShowIt Then
        ActSeason(1).Caption = DecodeSsimDayFormat(CurSeasVpfr, "CEDA", "SSIM2")
        ActSeason(1).Tag = CurSeasVpfr
        ActSeason(2).Caption = DecodeSsimDayFormat(CurSeasVpto, "CEDA", "SSIM2")
        ActSeason(2).Tag = CurSeasVpto
        ActSeason(0).Caption = CurSeasName
        ActSeason(0).Tag = "NEW"
        varNxtDay = DateAdd("d", 2, Now)
        tmpNxtDay = Format(varNxtDay, "yyyymmdd")
        ActSeason(3).Caption = DecodeSsimDayFormat(tmpNxtDay, "CEDA", "SSIM2")
        ActSeason(3).Tag = tmpNxtDay
    End If
End Sub

Private Sub chkWebUrl_Click(Index As Integer)
    Dim MsgData As String
    Dim UrlName As String
    Dim idx As Integer
    'UseImpType = "D:\Ufis\System\SCORE_ACL_CFG.csv"
    'UseImpConf = "D:\Ufis\Data\ImportTool\LYS\COHOR_LYS_CFG.csv"
    'UseImpFile = "D:\Ufis\Data\ImportTool\LYS\CohorS110725.TXT"
    'UseOutFile = "D:\Ufis\Data\ImportTool\LYS\ScoreS110725.TXT"
    If chkWebUrl(Index).Value = 1 Then
        chkWebUrl(Index).BackColor = LightGreen
        chkWebUrl(Index).Refresh
        For idx = 0 To chkWebUrl.UBound
            If idx <> Index Then chkWebUrl(idx).Value = 0
        Next
        Select Case Index
            Case 0
                'MsgData = CStr(Me.Left + (SlidePanel(0).Left + SlidePanel(0).Width))
                OpenWebTool 0, "SHOW", MsgData
                MsgData = ""
                MsgData = MsgData & "[TYPE]" & UseImpType & "[/TYPE]"
                MsgData = MsgData & "[CONF]" & UseImpConf & "[/CONF]"
                MsgData = MsgData & "[FILE]" & UseImpFile & "[/FILE]"
                MsgData = MsgData & "[SAVE]" & UseOutFile & "[/SAVE]"
                OpenWebTool 0, "IMP", MsgData
            Case 1
                MsgData = ""
                chkWebUrl(Index).Value = 0
            Case 2
                MsgData = ""
                OpenWebTool 0, Trim(Text1.Text), MsgData
                chkWebUrl(Index).Value = 0
            Case 3
                If WebToolId > 0 Then
                    OpenWebTool 0, "CLS", MsgData
                    ArrangeWebToolPanel "INIT", ""
                End If
                ShowSlidePanel 0, -1, "CLS", ""
                chkWebUrl(Index).Value = 0
            Case 4
                ShowSlidePanel 0, -1, "CLS", ""
                ArrangeWebToolPanel "INIT", ""
                chkWebUrl(Index).Value = 0
            Case Else
                chkWebUrl(Index).Value = 0
        End Select
    Else
        chkWebUrl(Index).BackColor = vbButtonFace
        chkWebUrl(Index).Refresh
        Select Case Index
            Case Else
        End Select
    End If
End Sub

Private Sub OpenWebTool(Index As Integer, MsgCmd As String, MsgData As String)
    Dim tmpData As String
    Dim tmpArgu As String
    On Error Resume Next
    If Dir(PathToUfisWebBrowser) <> "" Then
        SpoolOutMessage Index, MsgCmd, MsgData
        If WebToolId <= 0 Then
            tmpArgu = "POS=" & CStr(Me.Left + chkImpTyp(0).Left)
            tmpData = PathToUfisWebBrowser & " " & tmpArgu
            WebToolId = Shell(tmpData, vbNormalFocus)
            If WebSock(Index).State <> sckClosed Then
                WebSock(Index).Close
                WebToolIsConnected = False
            End If
            WebSock(Index).RemoteHost = "localhost"
            WebSock(Index).RemotePort = "4438"
            WebSock(Index).Connect
        End If
    Else
        MsgBox "WebTool not found"
    End If
End Sub
Private Function SpoolOutMessage(Index As Integer, SendCmd As String, SendMsg As String) As Long
    Dim NewRec As String
    Dim LineNo As Long
    NewRec = SendCmd & Chr(16) & SendMsg
    TcpMsgSpooler(Index).InsertTextLine NewRec, False
    LineNo = TcpMsgSpooler(Index).GetLineCount
    TcpMsgTimer(Index).Enabled = True
    SpoolOutMessage = LineNo
End Function
Private Function CreateOutMessage(SendCmd As String, SendMsg As String) As String
    Dim MsgOut As String
    MsgOut = ""
    MsgOut = MsgOut & "[CMD=]" & SendCmd & "[/CMD]"
    MsgOut = MsgOut & "[DAT=]" & SendMsg & "[/DAT]"
    CreateOutMessage = MsgOut
End Function
Private Sub WebSock_Close(Index As Integer)
    Select Case Index
        Case 0
            If WebSock(Index).State <> sckClosed Then WebSock(Index).Close
            WebToolIsConnected = False
            If WebToolId > 0 Then
                chkWebUrl(4).Value = 1
            End If
            WebToolId = 0
        Case Else
    End Select
End Sub

Private Sub WebSock_Connect(Index As Integer)
    Dim tmpTag As String
    WebToolIsConnected = True
    tmpTag = WebSock(Index).Tag
    If tmpTag <> "" Then
        WebSock(Index).Tag = ""
    End If
End Sub

Private Sub SetMyZOrder()
    ButtonPanel(2).ZOrder
    ButtonPanel(1).ZOrder
    ButtonPanel(0).ZOrder
End Sub

Private Sub InitSlidePanels(Index As Integer)
    Dim NewHeight As Long
    Dim NewWidth As Long
    Dim NewLeft As Long
    Dim NewTop As Long
    Dim i As Integer
    Dim I1 As Integer
    Dim I2 As Integer
    If Index >= 0 Then
        I1 = Index
        I2 = Index
    Else
        I1 = 0
        I2 = SlidePanel.UBound
    End If
    For i = I1 To I2
        NewLeft = ToolPanel(i).Left
        NewWidth = ToolPanel(i).Width + (NewLeft * 2) + 15
        SlidePanel(i).Width = NewWidth
        NewTop = ToolPanel(i).Top
        NewHeight = ToolPanel(i).Height + (NewTop * 2) + 15
        SlidePanel(i).Height = NewHeight
        SlidePanel(i).ZOrder
    Next
    SlidePanel(0).Left = chkImpTyp(0).Left - SlidePanel(0).Width
End Sub
 
Private Sub InitTcpMsgSpooler(Index)
    TcpMsgSpooler(Index).ResetContent
    TcpMsgSpooler(Index).HeaderString = "CMD,DATA"
    TcpMsgSpooler(Index).HeaderLengthString = "50,1000"
    TcpMsgSpooler(Index).SetFieldSeparator Chr(16)
End Sub

Private Sub ShowSlidePanel(Index As Integer, SetLeft As Long, UseTagCmd As String, UseBtnCmd As String)
    Dim tmpTag As String
    Dim i As Integer
    SlideTimer(Index).Enabled = False
    tmpTag = SlideTimer(Index).Tag
    If tmpTag = "" Then
        tmpTag = "CLS"
    End If
    Select Case UseTagCmd
        Case "OPN"
            tmpTag = "CLS"
        Case "CLS"
            tmpTag = "OPN"
        Case Else
    End Select
    If (tmpTag = "CLS") Or (tmpTag = "MVU") Then SlideTimer(Index).Tag = "OPN"
    If (tmpTag = "OPN") Or (tmpTag = "MVD") Then SlideTimer(Index).Tag = "CLS"
    If SetLeft >= 0 Then SlidePanel(Index).Left = SetLeft
    SlidePanel(Index).Tag = UseBtnCmd
    ButtonPanel(2).ZOrder
    SlidePanel(Index).ZOrder
    ButtonPanel(1).ZOrder
    ButtonPanel(0).ZOrder
    SlideTimer(Index).Enabled = True
End Sub

Private Sub SlideTimer_Timer(Index As Integer)
    Dim tmpTag As String
    Dim CurTop As Long
    Dim NewTop As Long
    Dim MinTop As Long
    Dim MaxTop As Long
    SlideTimer(Index).Enabled = False
    tmpTag = SlideTimer(Index).Tag
    Select Case tmpTag
        Case "OPN"
            If Not SlidePanel(Index).Visible Then
                SlidePanel(Index).Top = ButtonPanel(1).Top + ButtonPanel(1).Height - SlidePanel(Index).Height
                SlidePanel(Index).Visible = True
            End If
            SlideTimer(Index).Tag = "MVD"
            SlideTimer(Index).Enabled = True
        Case "MVD"
            CurTop = SlidePanel(Index).Top
            MaxTop = ButtonPanel(1).Top + ButtonPanel(1).Height
            NewTop = CurTop + 150
            If NewTop < MaxTop Then
                SlidePanel(Index).Top = NewTop
                SlideTimer(Index).Enabled = True
            Else
                SlidePanel(Index).Top = MaxTop
                SlideTimer(Index).Tag = "OPN"
                SlideTimer(Index).Enabled = False
                CallFinalSlideAction Index, SlideTimer(Index).Tag, SlidePanel(Index).Tag
            End If
        Case "MVU"
            CurTop = SlidePanel(Index).Top
            MinTop = ButtonPanel(1).Top + ButtonPanel(1).Height - SlidePanel(Index).Height
            NewTop = CurTop - 150
            If NewTop > MinTop Then
                SlidePanel(Index).Top = NewTop
                SlideTimer(Index).Enabled = True
            Else
                SlidePanel(Index).Top = MinTop
                SlideTimer(Index).Tag = "CLS"
                SlidePanel(Index).Visible = False
                SlideTimer(Index).Enabled = False
            End If
        Case "CLS"
            SlideTimer(Index).Tag = "MVU"
            SlideTimer(Index).Enabled = True
        Case Else
            SlideTimer(Index).Enabled = False
    End Select
End Sub

Private Sub CallFinalSlideAction(Index As Integer, SlideStatus As String, SlideCmd As String)
    Select Case SlideStatus
        Case "OPN"
            Select Case SlideCmd
                Case "ACDM"
                    chkWebUrl(0).Value = 1
                Case Else
            End Select
        Case Else
    End Select
End Sub
Private Sub WebSock_DataArrival(Index As Integer, ByVal bytesTotal As Long)
    Dim tmpText As String
    Dim CurMsgRcv As String
    Dim CurMsgCmd As String
    Dim CurMsgDat As String
    Dim SrvMsgLen As Long
    Dim CurMsgLen As Long
    tmpText = ""
    WebSock(Index).GetData tmpText, vbString
    SrvMsgRcv = SrvMsgRcv & tmpText
    CurMsgLen = InStr(SrvMsgRcv, "[/DAT]")
    If CurMsgLen > 0 Then
        CurMsgLen = CurMsgLen + 5
        CurMsgRcv = Left(SrvMsgRcv, CurMsgLen)
        GetKeyItem CurMsgCmd, CurMsgRcv, "[CMD=]", "[/CMD]"
        GetKeyItem CurMsgDat, CurMsgRcv, "[DAT=]", "[/DAT]"
        SrvMsgRcv = Mid(SrvMsgRcv, CurMsgLen + 1)
        SpoolRcvMessage Index, CurMsgCmd, CurMsgDat
    End If
End Sub

Private Sub InitRcvMsgSpooler(Index)
    TcpRcvSpooler(Index).ResetContent
    TcpRcvSpooler(Index).HeaderString = "CMD,DATA"
    TcpRcvSpooler(Index).HeaderLengthString = "50,1000"
    TcpRcvSpooler(Index).SetFieldSeparator Chr(16)
End Sub

Private Function SpoolRcvMessage(Index As Integer, SendCmd As String, SendMsg As String) As Long
    Dim NewRec As String
    Dim LineNo As Long
    NewRec = SendCmd & Chr(16) & SendMsg
    TcpRcvSpooler(Index).InsertTextLine NewRec, False
    LineNo = TcpRcvSpooler(Index).GetLineCount
    TcpRcvTimer(Index).Enabled = True
    SpoolRcvMessage = LineNo
End Function

Private Sub TcpRcvTimer_Timer(Index As Integer)
    Dim MsgCmd As String
    Dim MsgData As String
    Dim TcpMsg As String
    Dim LineNo As Long
    TcpRcvTimer(Index).Enabled = False
    LineNo = TcpRcvSpooler(Index).GetLineCount
    If WebToolIsConnected Then
        If LineNo > 0 Then
            MsgCmd = TcpRcvSpooler(Index).GetColumnValue(0, 0)
            MsgData = TcpRcvSpooler(Index).GetColumnValue(0, 1)
            HandleRcvCommand MsgCmd, MsgData
            TcpRcvSpooler(Index).DeleteLine 0
        End If
        LineNo = TcpRcvSpooler(Index).GetLineCount
    End If
    If LineNo > 0 Then
        TcpRcvTimer(Index).Enabled = True
    End If
End Sub

Private Sub HandleRcvCommand(GotCmd As String, GotMsg As String)
    Dim tmpData As String
    Select Case GotCmd
        Case "IMP"
            'chkImp(2).Value = 0
        Case "POS"
            'tmpData = GotMsg
            'Me.Left = Val(tmpData)
        Case "CLS"
            'Unload Me
        Case "GPS"
            ArrangeWebToolPanel "GPS", GotMsg
        Case "DONE"
            chkRequest.Tag = GotMsg
            chkWebUrl(4).Value = 1
            OpenWebTool 0, "HIDE", ""
            chkExpand.Value = 1
            DoTheRestOfIt PushedImpButton
        Case "INFO"
            ArrangeWebToolPanel "INFO", GotMsg
        Case Else
    End Select
End Sub

Private Sub ArrangeWebToolPanel(ForWhat As String, Context As String)
    Dim NewTop As Long
    WebComLabel(0).Caption = Context
    Select Case ForWhat
        Case "INIT"
            NewTop = chkWebUrl(4).Top
            NewTop = NewTop + chkWebUrl(4).Height
            WebComLabel(0).Top = NewTop
            NewTop = NewTop + WebComLabel(0).Height
            chkWebUrl(0).Top = NewTop
            NewTop = NewTop + chkWebUrl(0).Height
            chkWebUrl(1).Top = NewTop
            NewTop = NewTop + chkWebUrl(1).Height
            chkWebUrl(2).Top = NewTop
            SlidePanel(0).BackColor = LightestBlue
        Case "GPS"
            NewTop = chkWebUrl(4).Top
            NewTop = NewTop + chkWebUrl(4).Height
            chkWebUrl(0).Top = NewTop
            NewTop = NewTop + chkWebUrl(0).Height
            WebComLabel(0).Top = NewTop
            SlidePanel(0).BackColor = vbBlue
        Case Else
    End Select
    ToolPanel(0).Refresh
End Sub

