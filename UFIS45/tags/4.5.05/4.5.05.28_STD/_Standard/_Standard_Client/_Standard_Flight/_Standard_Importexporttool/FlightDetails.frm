VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form FlightDetails 
   Caption         =   "Flight Details (Home Rotation View)"
   ClientHeight    =   7455
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   13725
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   8.25
      Charset         =   161
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "FlightDetails.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   7455
   ScaleWidth      =   13725
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CheckBox chkWork 
      Caption         =   "Save As"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   1740
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Shrink"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   870
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox chkWork 
      Caption         =   "Season"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   0
      Width           =   855
   End
   Begin TABLib.TAB DetailData 
      Height          =   2055
      Left            =   30
      TabIndex        =   0
      Top             =   600
      Width           =   4875
      _Version        =   65536
      _ExtentX        =   8599
      _ExtentY        =   3625
      _StockProps     =   64
      Columns         =   10
   End
End
Attribute VB_Name = "FlightDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private CurHeadLength As String
Private IsInitialized As Boolean

Public Sub InitDetailList(ipLines As Integer, ipFirstCall As Boolean)
Dim i As Integer
Dim tmpLen
Dim HdrLenLst As String
Dim tmpHeader As String
Dim ActColLst As String
    Me.FontSize = MySetUp.FontSlider.Value
    'DetailData.ShowHorzScroller False
    tmpHeader = "Date,W,.,LINE,A,S,ALC,FLTN,S,ORG,STOD,V,VIA,DES,STOA,N,.,ACT,CAP,.,LINE,A,S,ALC,FLTN,S,ORG,STOD,V,VIA,DES,STOA,N,.,Remark"
    ActColLst = "7,1,0,4,1,1,3,4,1,3,4,2,3,3,4,1,0,3,3,0,4,1,1,3,4,1,3,4,2,3,3,4,1,0"
    HdrLenLst = ""
    i = 0
    Do
        i = i + 1
        tmpLen = GetItem(ActColLst, i, ",")
        If tmpLen <> "" Then
            If tmpLen > 0 Then
                HdrLenLst = HdrLenLst & TextWidth(Space(tmpLen)) / Screen.TwipsPerPixelX + 6 & ","
            Else
                HdrLenLst = HdrLenLst & "1,"
            End If
        End If
    Loop While tmpLen <> ""
    HdrLenLst = HdrLenLst & "1000"
    If ipFirstCall Then DetailData.ResetContent
    DetailData.FontName = "Courier New"
    DetailData.SetTabFontBold True
    DetailData.ShowHorzScroller True
    DetailData.HeaderFontSize = MySetUp.FontSlider.Value + 6
    DetailData.FontSize = MySetUp.FontSlider.Value + 6
    DetailData.LineHeight = MySetUp.FontSlider.Value + 7
    DetailData.HeaderLengthString = HdrLenLst
    DetailData.HeaderString = tmpHeader
    DetailData.DateTimeSetColumn 0
    DetailData.DateTimeSetInputFormatString 0, "YYYYMMDD"
    DetailData.DateTimeSetOutputFormatString 0, "DDMMMYY"
    If ipFirstCall And Not IsInitialized Then
        DetailData.Height = ipLines * DetailData.LineHeight * Screen.TwipsPerPixelY
        Me.Height = DetailData.Top + DetailData.Height + 30 * Screen.TwipsPerPixelY
        IsInitialized = True
    End If
    CurHeadLength = ActColLst
End Sub

Public Sub ChangeMyDataFont(myFontSize As Integer)
Dim i As Integer
Dim tmpLen
Dim HdrLenLst As String
    FontSize = myFontSize
    HdrLenLst = ""
    i = 0
    Do
        i = i + 1
        tmpLen = GetItem(CurHeadLength, i, ",")
        If tmpLen <> "" Then
            HdrLenLst = HdrLenLst & TextWidth(Space(tmpLen)) / Screen.TwipsPerPixelX + 6 & ","
        End If
    Loop While tmpLen <> ""
    HdrLenLst = HdrLenLst & "1000"
    DetailData.HeaderLengthString = HdrLenLst
    DetailData.HeaderFontSize = myFontSize + 6
    DetailData.FontSize = myFontSize + 6
    DetailData.LineHeight = myFontSize + 7
    Refresh
End Sub

Public Sub GetFlightCalendar()
Dim SelLine As Long
Dim SelData As String
Dim FldLst As String
    Me.MousePointer = 11
    SelData = ""
    SelLine = FlightExtract.ExtractData.GetCurrentSelected
    If SelLine >= 0 Then SelData = FlightExtract.ExtractData.GetLineValues(SelLine)
    If (SelLine >= 0) And (SelData <> "") Then
        FldLst = "LINE,CODI,CODA,FLCA,FLNA,FLCD,FLND,VPFR,VPTO,FRQD,ACT3,NOSE,ORGA,VIA3,STOA,STOD,DAOF,VID3,DESD,NATA,NATD,FRQW"
        ShowPeriodData FldLst, SelData
    Else
        MyMsgBox.InfoApi 0, "Click on the desired line and try it again.", "More info"
        If MyMsgBox.CallAskUser(0, 0, 0, "Action Control", "No data selected.", "hand", "", UserAnswer) >= 0 Then DoNothing
    End If
    Me.MousePointer = 0
End Sub

Public Sub ShowPeriodData(Fields As String, Data As String)
Dim varVpfr As Variant
Dim varVpto As Variant
Dim varFday As Variant
Dim strWkdy As Integer
Dim WkDayCnt As Integer
Dim LinCnt As Long
Dim tmpData As String
Dim tmpFrqd As String
Dim tmpFrqw As String
Dim tmpLine As String
Dim LineNbr As String
Dim CurLine As Long
Dim CalDay As String
Dim DataLine As String
Dim tmpRecord As String
Dim ArrFlt As Boolean
Dim DepFlt As Boolean
Dim IsValidWeek As Boolean
Dim WeekFreq As Integer
Dim InvalidDays As Integer
Dim DatLinNbr As String
Dim DataRecord As String

    If chkWork(0).Value = 0 Then InitDetailList 30, True
    MousePointer = 11

    tmpLine = "|,,,,,,,,,,,,,,|,,,|,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,"
    DataLine = ""
'tmpHeader = "Date,W,.,LINE,A,S,ALC,FLTN,S,ORG,STOD,V,VIA,DES,STOA,N,.,ACT,NOS,.,LINE,A,S,ALC,FLTN,S,ORG,STOD,V,VIA,DES,STOA,N"
'FldLst = "LINE,CODI,CODA,FLCA,FLNA,FLCD,FLND,VPFR,VPTO,FRQD,ACT3,NOSE,ORGA,VIA3,STOA,STOD,DAOF,VID3,DESD,NATA,NATD,FRQW"
    DatLinNbr = GetFieldValue("LINE", Data, Fields)
    tmpData = GetFieldValue("FLCA", Data, Fields)
    If tmpData <> "" Then
        DataLine = DataLine & DatLinNbr & ",,,"
        ArrFlt = True
    Else
        DataLine = DataLine & ",,,"
        ArrFlt = False
    End If
    DataLine = DataLine & tmpData & ","
    DataLine = DataLine & GetFieldValue("FLNA", Data, Fields) & ","
    DataLine = DataLine & GetFieldValue("FLSA", Data, Fields) & ","
    DataLine = DataLine & GetFieldValue("ORGA", Data, Fields) & ","
    DataLine = DataLine & Right(GetFieldValue("STDO", Data, Fields), 4) & ","
    DataLine = DataLine & GetFieldValue("VAOF", Data, Fields) & ","
    DataLine = DataLine & GetFieldValue("VIA3", Data, Fields) & ","
    tmpData = GetFieldValue("DESA", Data, Fields)
    If tmpData = "" And ArrFlt Then tmpData = HomeAirport
    DataLine = DataLine & tmpData & ","
    DataLine = DataLine & Right(GetFieldValue("STOA", Data, Fields), 4) & ","
    DataLine = DataLine & GetFieldValue("NATA", Data, Fields) & ",|,"
    DataLine = DataLine & GetFieldValue("ACT3", Data, Fields) & ","
    DataLine = DataLine & GetFieldValue("NOSE", Data, Fields) & ",|,"
    tmpData = GetFieldValue("FLCD", Data, Fields)
    If tmpData <> "" Then
        DepFlt = True
        DataLine = DataLine & DatLinNbr & ",,,"
    Else
        DepFlt = False
        DataLine = DataLine & ",,,,"
    End If
    DataLine = DataLine & tmpData & ","
    DataLine = DataLine & GetFieldValue("FLND", Data, Fields) & ","
    DataLine = DataLine & GetFieldValue("FLSD", Data, Fields) & ","
    tmpData = GetFieldValue("ORGD", Data, Fields)
    If tmpData = "" And DepFlt Then tmpData = HomeAirport
    DataLine = DataLine & tmpData & ","
    DataLine = DataLine & Right(GetFieldValue("STOD", Data, Fields), 4) & ","
    tmpData = GetFieldValue("DAOF", Data, Fields)
    If tmpData <> "" Then tmpData = "+" & tmpData
    DataLine = DataLine & tmpData & ","
    DataLine = DataLine & GetFieldValue("VID3", Data, Fields) & ","
    DataLine = DataLine & GetFieldValue("DESD", Data, Fields) & ","
    DataLine = DataLine & Right(GetFieldValue("STAD", Data, Fields), 4) & ","
    DataLine = DataLine & GetFieldValue("NATD", Data, Fields) & ",,,,,"
    'DetailData.ResetContent
    tmpFrqd = GetFieldValue("FRQD", Data, Fields)
    tmpFrqw = GetFieldValue("FRQW", Data, Fields)
    WeekFreq = Val(tmpFrqw)
    
    tmpData = GetFieldValue("VPFR", Data, Fields)
    LineNbr = DetailData.GetLinesByColumnValue(0, tmpData, 1)
    If LineNbr <> "" Then
        CurLine = Val(LineNbr)
    Else
        'Date not found
        LinCnt = DetailData.GetLineCount
        If LinCnt > 0 Then
            MyMsgBox.InfoApi 0, "Check and correct the period begin date in the mainview", "More info"
            If MyMsgBox.CallAskUser(0, 0, 0, "Action Control", "Date not valid.", "stop", "", UserAnswer) >= 0 Then DoNothing
            CurLine = -2
        Else
            'MsgBox "No Lines. So go on"
        End If
    End If
    
    If CurLine >= 0 Then
        varVpfr = CedaDateToVb(tmpData)
        tmpData = GetFieldValue("VPTO", Data, Fields)
        varVpto = CedaDateToVb(tmpData)
        WkDayCnt = 0
        LinCnt = 0
        IsValidWeek = True
        InvalidDays = 0
        
        varFday = varVpfr
        Do
            strWkdy = Trim(Str(Weekday(varFday, vbMonday)))
            tmpData = Format(varFday, "yyyymmdd")
            'CalDay = tmpData
            tmpRecord = ""
            If IsValidWeek And (InStr(tmpFrqd, strWkdy) > 0) Then
                tmpRecord = tmpData & "," & strWkdy & ",|," & DataLine
            Else
                If (chkWork(1).Value = 0) And (chkWork(0).Value = 0) Then
                    tmpRecord = tmpData & "," & strWkdy & "," & tmpLine
                End If
            End If
            If tmpRecord <> "" Then
                If chkWork(0).Value = 0 Then
                    DetailData.InsertTextLine tmpRecord, False
                    If InStr("67", strWkdy) > 0 Then DetailData.SetLineColor LinCnt, vbBlack, LightestYellow
                    'If InStr("12345", strWkdy) > 0 Then DetailData.SetLineColor LinCnt, vbBlack, LightGray
                    LinCnt = LinCnt + 1
                Else
                    'LineNbr = DetailData.GetLinesByColumnValue(0, CalDay, 1)
                    If ArrFlt <> DepFlt Then
                        DataRecord = DetailData.GetLineValues(CurLine)
                        If DataRecord <> "" Then
                            If ArrFlt Then
                                tmpRecord = MixItemLists(DataRecord, tmpRecord, 2)
                            End If
                            If DepFlt Then
                                tmpRecord = MixItemLists(DataRecord, tmpRecord, 1)
                            End If
                        End If
                    End If
                    DetailData.UpdateTextLine CurLine, tmpRecord, False
                End If
            End If
            varFday = DateAdd("d", 1, varFday)
            CurLine = CurLine + 1
            If Not IsValidWeek Then InvalidDays = InvalidDays - 1
            WkDayCnt = WkDayCnt + 1
            If WkDayCnt = 7 Then
                If IsValidWeek Then
                    InvalidDays = (WeekFreq - 1) * 7
                    If InvalidDays > 0 Then IsValidWeek = False
                Else
                    If InvalidDays = 0 Then
                        IsValidWeek = True
                    End If
                End If
                WkDayCnt = 0
            End If
        Loop While varFday <= varVpto
        DetailData.RedrawTab
        Refresh
    End If
    MousePointer = 0
End Sub
Private Function MixItemLists(List1 As String, List2 As String, ipHow As Integer) As String
    Dim Result As String
    Dim MidItm1 As String
    Dim MidItm2 As String
    Result = ""
    MidItm1 = Result & GetItem(List1, 3, "|") & "|"
    MidItm2 = Result & GetItem(List2, 3, "|") & "|"
    If MidItm1 = ",,,|" Then MidItm1 = MidItm2
    If MidItm1 = MidItm2 Then
        Result = Result & GetItem(List1, 1, "|") & "|"
        Select Case ipHow
            Case 1
                Result = Result & GetItem(List1, 2, "|") & "|"
                Result = Result & MidItm1
                Result = Result & GetItem(List2, 4, "|")
            Case 2
                Result = Result & GetItem(List2, 2, "|") & "|"
                Result = Result & MidItm2
                Result = Result & GetItem(List1, 4, "|")
            Case Else
                MsgBox "Please check MixItemList"
        End Select
    Else
        MsgBox "das geht nicht"
    End If
    MixItemLists = Result
End Function
Private Sub chkWork_Click(Index As Integer)
    Dim Vpfr As String
    Dim Vpto As String
    If chkWork(Index).Value = 1 Then chkWork(Index).BackColor = LightestGreen Else chkWork(Index).BackColor = vbButtonFace
    Select Case Index
        Case 0
            'Season
            If chkWork(0).Value = 1 Then
                chkWork(1).Enabled = False
                chkWork(1).Value = 0
                Vpfr = FlightExtract.txtVpfr(0).Tag
                Vpto = FlightExtract.txtVpto(0).Tag
                BuildSeasonView Vpfr, Vpto
                GetFlightCalendar
            Else
                chkWork(1).Enabled = True
            End If
        Case 1
            'Shrink
            If chkWork(1).Enabled Then
                GetFlightCalendar
            End If
        Case 2
            'Save As
            If chkWork(Index).Value = 1 Then
                MainDialog.SaveLoadedFile Index, FlightDetails.DetailData
                chkWork(Index).Value = 0
            End If
        Case Else
    End Select
End Sub

Private Sub BuildSeasonView(Vpfr As String, Vpto As String)
Dim varVpfr As Variant
Dim varVpto As Variant
Dim varFday As Variant
Dim strWkdy As Integer
Dim intDayCnt As Integer
Dim tmpData As String
Dim tmpLine As String
Dim DataLine As String
Dim tmpRecord As String
Dim LinCnt As Long
    If Vpfr = "" Or Vpto = "" Then
        If MyMsgBox.CallAskUser(0, 0, 0, "Date Check", "Can't determine the valid season.", "stop", "", UserAnswer) >= 0 Then DoNothing
    Else
        InitDetailList 30, True
        tmpLine = "|,,,,,,,,,,,,,,|,,,|,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,"
        varVpfr = CedaDateToVb(Vpfr)
        varVpto = CedaDateToVb(Vpto)
        intDayCnt = 1
        varFday = varVpfr
        LinCnt = 0
        Do
            strWkdy = Trim(Str(Weekday(varFday, vbMonday)))
            tmpData = Format(varFday, "yyyymmdd")
            tmpRecord = tmpData & "," & strWkdy & "," & tmpLine
            DetailData.InsertTextLine tmpRecord, False
            If InStr("67", strWkdy) > 0 Then DetailData.SetLineColor LinCnt, vbBlack, LightestYellow
            LinCnt = LinCnt + 1
            intDayCnt = intDayCnt + 1
            varFday = DateAdd("d", 1, varFday)
        Loop While varFday <= varVpto
        Refresh
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    'If KeyCode = vbKeyF1 Then DisplayHtmHelpFile "", Me.Name, Me.ActiveControl.Name
    If KeyCode = vbKeyF1 Then ShowHtmlHelp Me, "", ""
End Sub

Private Sub Form_Resize()
Dim NewVal As Long
    NewVal = Me.ScaleHeight - DetailData.Top '- StatusBar.Height - 4 * Screen.TwipsPerPixelY
    If NewVal > 50 Then
        DetailData.Height = NewVal
    End If
    NewVal = Me.ScaleWidth - 2 * DetailData.Left '- 5 * Screen.TwipsPerPixelX
    If NewVal > 50 Then DetailData.Width = NewVal
End Sub

Private Sub Form_Unload(Cancel As Integer)
    FlightExtract.chkWork(7).Value = 0
End Sub
