VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmData 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "frmData"
   ClientHeight    =   10890
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   12255
   LinkTopic       =   "Form1"
   ScaleHeight     =   10890
   ScaleWidth      =   12255
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin TABLib.TAB tab_GATTAB 
      Height          =   2295
      Left            =   9960
      TabIndex        =   10
      Tag             =   "{=TABLE=}GATTAB{=FIELDS=}TERM"
      Top             =   6600
      Width           =   1335
      _Version        =   65536
      _ExtentX        =   2355
      _ExtentY        =   4048
      _StockProps     =   64
   End
   Begin VB.Timer TimerReloadAft 
      Interval        =   60000
      Left            =   10080
      Top             =   120
   End
   Begin TABLib.TAB tab_CONFIG 
      Height          =   975
      Left            =   6420
      TabIndex        =   2
      Top             =   480
      Width           =   3285
      _Version        =   65536
      _ExtentX        =   5794
      _ExtentY        =   1720
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_ACTTAB 
      Height          =   1215
      Left            =   120
      TabIndex        =   0
      Tag             =   "{=TABLE=}ACTTAB{=FIELDS=}URNO,ACT3,ACT5"
      Top             =   480
      Width           =   5865
      _Version        =   65536
      _ExtentX        =   10345
      _ExtentY        =   2143
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_APTTAB 
      Height          =   2235
      Left            =   240
      TabIndex        =   4
      Tag             =   "{=TABLE=}APTTAB{=FIELDS=}APC3,APC4,VATO,URNO"
      Top             =   6600
      Width           =   1605
      _Version        =   65536
      _ExtentX        =   2831
      _ExtentY        =   3942
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_NATTAB 
      Height          =   2235
      Left            =   2040
      TabIndex        =   6
      Tag             =   "{=TABLE=}NATTAB{=FIELDS=}TTYP,VATO,URNO"
      Top             =   6600
      Width           =   3765
      _Version        =   65536
      _ExtentX        =   6641
      _ExtentY        =   3942
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_ALTTAB 
      Height          =   2235
      Left            =   6120
      TabIndex        =   8
      Tag             =   "{=TABLE=}ALTTAB{=FIELDS=}ALC2,ALC3,URNO"
      Top             =   6600
      Width           =   1605
      _Version        =   65536
      _ExtentX        =   2831
      _ExtentY        =   3942
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_SEATAB 
      Height          =   2235
      Left            =   7920
      TabIndex        =   9
      Tag             =   "{=TABLE=}SEATAB{=FIELDS=}SEAS"
      Top             =   6600
      Width           =   1605
      _Version        =   65536
      _ExtentX        =   2831
      _ExtentY        =   3942
      _StockProps     =   64
   End
   Begin VB.Label Label5 
      Caption         =   "GATTAB"
      Height          =   360
      Left            =   9960
      TabIndex        =   13
      Top             =   6360
      Width           =   1575
   End
   Begin VB.Label Label2 
      Caption         =   "SEATAB"
      Height          =   360
      Left            =   7920
      TabIndex        =   12
      Top             =   6360
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "ALTTAB"
      Height          =   360
      Left            =   6120
      TabIndex        =   11
      Top             =   6360
      Width           =   1575
   End
   Begin VB.Label Label8 
      Caption         =   "NATTAB"
      Height          =   360
      Left            =   2040
      TabIndex        =   7
      Top             =   6360
      Width           =   3150
   End
   Begin VB.Label Label7 
      Caption         =   "APTTAB"
      Height          =   360
      Left            =   240
      TabIndex        =   5
      Top             =   6360
      Width           =   3150
   End
   Begin VB.Label Label4 
      Caption         =   "Config-File:"
      Height          =   240
      Left            =   6390
      TabIndex        =   3
      Top             =   120
      Width           =   2580
   End
   Begin VB.Label Label3 
      Caption         =   "ACTTAB:"
      Height          =   195
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   765
   End
End
Attribute VB_Name = "frmData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private strFieldSeparator As String
Private strConfigSepa As String
Private colURNOs As New Collection
Private strHOPO As String
Private strTableExt As String
Private strServer As String
Private strAccessMethod As String
Private sglUTCOffsetHours As Single
Private strConfigFile As String
Private ilCntMinutes As Integer
Private ilReloadMinutes As Integer
Private strTemplatePath As String
Private ilOffset As Integer

'#######################################################################
'# INITIAL LOAD
'#######################################################################
Public Sub InitialLoadData()
    Me.MousePointer = vbHourglass
    Dim strAftWhere As String

    '### Load Basic Data
      
    InitTabGeneral tab_ALTTAB
    InitTabForCedaConnection tab_ALTTAB
    LoadData tab_ALTTAB, "where VATO = '' order by ALC2"
    tab_ALTTAB.AutoSizeColumns
    tab_ALTTAB.Sort 0, True, False
    tab_ALTTAB.Refresh
    
    InitTabGeneral tab_SEATAB
    InitTabForCedaConnection tab_SEATAB
    
    InitTabGeneral tab_GATTAB
    InitTabForCedaConnection tab_GATTAB
    LoadData tab_GATTAB, ""
    tab_GATTAB.AutoSizeColumns
    tab_GATTAB.Sort 0, True, False
    tab_GATTAB.Refresh
        
        
    Me.MousePointer = vbDefault
    

    'starting the timer to reload every 60 minutes the AFT-data
    ilCntMinutes = 0
    TimerReloadAft.Enabled = True
    Me.MousePointer = vbDefault
End Sub

Public Sub InitTabGeneral(ByRef rTab As TABLib.Tab)
    rTab.ResetContent
    rTab.FontName = "Courier New"
    rTab.HeaderFontSize = "18"
    rTab.FontSize = "16"
    rTab.EnableHeaderSizing True
    rTab.ShowHorzScroller True
    rTab.AutoSizeByHeader = True
    rTab.SetFieldSeparator strFieldSeparator

    Dim ilCnt As Integer
    Dim i As Integer
    Dim strFields As String
    Dim strHeaderLengthString As String
    GetKeyItem strFields, rTab.Tag, "{=FIELDS=}", "{="
    ilCnt = ItemCount(strFields, ",")
    For i = 1 To ilCnt Step 1
        strHeaderLengthString = strHeaderLengthString & "10,"
    Next i
    strHeaderLengthString = Left(strHeaderLengthString, Len(strHeaderLengthString) - 1)
    rTab.HeaderLengthString = strHeaderLengthString

    strFields = Replace(strFields, ",", strFieldSeparator)
    rTab.HeaderString = strFields
End Sub

Public Sub InitTabForCedaConnection(ByRef rTab As TABLib.Tab)
    'do the initializing for the CEDA-connection
    rTab.CedaServerName = strServer
    rTab.CedaPort = "3357"
    rTab.CedaHopo = strHOPO
    rTab.CedaCurrentApplication = gsAppName
    rTab.CedaTabext = strTableExt
    rTab.CedaUser = gsUserName
    rTab.CedaWorkstation = frmMain.AATLoginControl1.GetWorkStationName
    rTab.CedaSendTimeout = "120"
    rTab.CedaReceiveTimeout = "240"
    rTab.CedaRecordSeparator = Chr(10)
    rTab.CedaIdentifier = gsAppName
End Sub

Public Sub LoadData(ByRef rTab As TABLib.Tab, ByRef pWhere As String)
    Dim strCommand As String
    Dim strTable As String
    Dim strFieldList As String
    Dim strData As String
    Dim strWhere As String

    strCommand = "RT"
    
    GetKeyItem strTable, rTab.Tag, "{=TABLE=}", "{="
    GetKeyItem strFieldList, rTab.Tag, "{=FIELDS=}", "{="
    strData = ""
    strWhere = pWhere
    If Len(strWhere) = 0 Then
        GetKeyItem strWhere, rTab.Tag, "{=WHERE=}", "{="
    End If
    rTab.CedaAction strCommand, strTable, strFieldList, strData, strWhere
End Sub

Private Sub ConnectUfisCom()
    frmMain.UfisCom.SetCedaPerameters frmMain.AATLoginControl1.GetUserName, strHOPO, strTableExt
    frmMain.UfisCom.InitCom strServer, strAccessMethod
End Sub

Private Sub DisconnetUfisCom()
    frmMain.UfisCom.CleanupCom
End Sub

Private Sub Form_Load()
    ilOffset = 42
    strFieldSeparator = ","
    strConfigSepa = Chr(15)
    strServer = GetIniEntry("", gsAppName, "GLOBAL", "HOSTNAME", "XXX")
    strHOPO = GetIniEntry("", gsAppName, "GLOBAL", "HOMEAIRPORT", "XXX")
    strTableExt = GetIniEntry("", gsAppName, "GLOBAL", "TABLEEXTENSION", "TAB")
    strAccessMethod = "CEDA"
    strConfigFile = GetIniEntry("", gsAppName, "GLOBAL", "CONFIG_FILE", "XXX")
    sglUTCOffsetHours = GetUTCOffset
End Sub

Private Sub WriteToDB(ByRef rTab As TABLib.Tab, ByRef strSearchLineTag As String, ByRef strCMD As String)
    Dim strLineNo As String
    Dim strLine As String
    Dim strFields As String
    Dim strTable As String
    Dim strWhere As String
    Dim strData As String
    Dim i As Integer

    GetKeyItem strFields, rTab.Tag, "{=FIELDS=}", "{="
    GetKeyItem strTable, rTab.Tag, "{=TABLE=}", "{="

    For i = rTab.GetLineCount - 1 To 0 Step -1
        If rTab.GetLineTag(CLng(i)) = strSearchLineTag Then
            strData = rTab.GetLineValues(CLng(i))
            strWhere = "WHERE URNO = " & rTab.GetColumnValue(CLng(i), 0)
            While InStr(1, strData, ",,") > 0
                strData = Replace(strData, ",,", ", ,")
            Wend
            If Right(strData, 1) = "," Then
                strData = strData & " "
            End If
            If frmMain.UfisCom.CallServer(strCMD, strTable, strFields, strData, strWhere, "360") <> 0 Then
                MsgBox frmMain.UfisCom.LastErrorMessage, vbCritical
                Exit Sub
            End If

            If strSearchLineTag = "DELETE" Then
                rTab.DeleteLine CLng(i)
            Else
                rTab.SetLineTag CLng(i), ""
            End If
        End If
    Next i
End Sub

Private Function GetUTCOffset() As Single
    Dim strUtcArr() As String
    Dim strArr() As String
    Dim CurrStr As String
    Dim tmpstr As String

    Dim istrRet As Integer
    Dim count As Integer
    Dim i As Integer

    GetUTCOffset = 0

    ConnectUfisCom

    If frmMain.UfisCom.CallServer("GFR", "", "", "", "[CONFIG]", "360") = 0 Then
        tmpstr = frmMain.UfisCom.GetDataBuffer(True)
    End If
    strArr = Split(tmpstr, Chr(10))
    count = UBound(strArr)
    For i = 0 To count
        CurrStr = strArr(i)
        istrRet = InStr(1, CurrStr, "UTCD", 0)
        If istrRet <> 0 Then
            strUtcArr = Split(strArr(i), ",")
            GetUTCOffset = CSng(strUtcArr(1) / 60)
            Exit For
        End If
    Next i
    DisconnetUfisCom
End Function

Private Sub TimerReloadAft_Timer()
    ilCntMinutes = ilCntMinutes + 1
    If ilCntMinutes = ilReloadMinutes Then
        Me.MousePointer = vbHourglass
        TimerReloadAft.Enabled = False
        frmMain.StatusBar1.Panels(1).Text = "Reloading MAPTAB data (AFTTAB) ..."
        'Dim strAftWhere As String
        'strAftWhere = GenerateAftWhere()
        'tab_AFTTAB.ResetContent
        'LoadData tab_AFTTAB, strAftWhere
        'LoadData tab_SEVTAB, "WHERE UAFT IN (" & tab_AFTTAB.SelectDistinct("0", "", "", ",", False) & ") AND STAT IN ('ACTIVE','CANCELLED')"
'        If gbDebug = True Then
'            tab_AFTTAB.AutoSizeColumns
'        End If
        'frmMain.RefreshManualInputTab
        
        frmData.InitTabGeneral frmMain.tab_MAPTAB
        frmData.InitTabForCedaConnection frmMain.tab_MAPTAB
        frmData.LoadData frmMain.tab_MAPTAB, "order by URNO"
        frmMain.tab_MAPTAB.AutoSizeColumns
    
        ilCntMinutes = 0
        TimerReloadAft.Enabled = True
        Me.MousePointer = vbDefault
        frmMain.StatusBar1.Panels(1).Text = "Ready."
    End If
End Sub

Private Sub ReadConfigFile()
    Dim strLineNo As String

    tab_CONFIG.ResetContent
    tab_CONFIG.HeaderLengthString = "100,1000"
    tab_CONFIG.EnableInlineEdit True
    tab_CONFIG.ShowHorzScroller True
    tab_CONFIG.SetFieldSeparator strConfigSepa
    tab_CONFIG.HeaderString = "Parameter" & strConfigSepa & "Setting"
    tab_CONFIG.ReadFromFile strConfigFile

    Dim i As Integer
    Dim strLine As String
    For i = 0 To tab_CONFIG.GetLineCount - 1 Step 1
        strLine = tab_CONFIG.GetLineValues(i)
        strLine = Replace(strLine, "=", Chr(15), 1, 1)
        tab_CONFIG.UpdateTextLine i, strLine, False
    Next i
    tab_CONFIG.RedrawTab

    ' Reading TEMPLATE_PATH
    strLineNo = tab_CONFIG.GetLinesByColumnValue(0, "TEMPLATE_PATH", 0)
    If IsNumeric(strLineNo) = True Then
        strTemplatePath = tab_CONFIG.GetColumnValue(CLng(strLineNo), 1)
        If Right(strTemplatePath, 1) <> "\" Then
            strTemplatePath = strTemplatePath & "\"
        End If
    Else
        strTemplatePath = "d:\ufis\system\templates\" 'default
    End If

    ' Reading RELOAD_INTERVAL
    strLineNo = tab_CONFIG.GetLinesByColumnValue(0, "RELOAD_INTERVAL", 0)
    If IsNumeric(strLineNo) = True Then
        ilReloadMinutes = CInt(tab_CONFIG.GetColumnValue(CLng(strLineNo), 1))
    Else
        ilReloadMinutes = 60 'default
    End If
End Sub


Public Sub LoadTemplateFile(ByRef rTextBox As TextBox, ByRef strFileName As String)
    If ExistFile(strFileName) = True Then
        Dim ilFileNo As Integer
        Dim strTextLine As String
        Dim strFile As String
        Dim strScreen As String

        ilFileNo = FreeFile ' Get unused file number.
        Open strFileName For Input As #ilFileNo
        Do While Not EOF(ilFileNo)
           Line Input #ilFileNo, strTextLine
           strFile = strFile & strTextLine & vbCrLf
        Loop
        Close ilFileNo
        GetKeyItem strScreen, strFile, "<=SCREENCONTENT=>", "<="
        If Len(strScreen) > 0 Then
            While Left(strScreen, 2) = vbCrLf
                strScreen = Right(strScreen, Len(strScreen) - 2)
            Wend
            While Right(strScreen, 2) = vbCrLf
                strScreen = Left(strScreen, Len(strScreen) - 2)
            Wend
        Else
            strScreen = strFile
        End If

        rTextBox.Text = strScreen
    Else
        MsgBox "The file " & strFileName & " does not exist!" & vbCrLf & _
               "Please check the configuration-file '" & strConfigFile & _
               "'.", vbCritical, "No template file"
    End If
End Sub

Public Sub SendToServer(ByRef rScreenText As String, ByRef rCommand As String, ByRef rFlightUrno As String)
    Dim strCMD As String
    Dim strData As String
    Dim strSelection As String
    Dim strScreenContent As String

    If Len(rScreenText) = 0 Then
        strCMD = "EXRQ"
        strSelection = rFlightUrno
        strData = rCommand
    Else
        strCMD = "FDI"
        strSelection = "TELEX,7"
        strScreenContent = Replace(rScreenText, vbCrLf, vbCr)

        strData = "<=SOURCE=>KRISCOM<=/SOURCE=>" & vbCr & "<=COMMAND=>"
        strData = strData & rCommand & "<=/COMMAND=>" & vbCr & "<=URNO=>"
        strData = strData & rFlightUrno & "<=/URNO=>" & vbCr & "<=SCREENCONTENT=>" & vbCr
        strData = strData & strScreenContent & vbCr & "<=/SCREENCONTENT=>"
    End If

    ConnectUfisCom
    frmMain.UfisCom.CallServer strCMD, "", "", strData, strSelection, "1"
    DisconnetUfisCom
End Sub

Private Function DecryptString(ByRef rString As String)
    Dim i As Integer
    Dim ilLen As Integer
    Dim strRet As String
    Dim ilNewCharCode As Integer

    ilLen = Len(rString)

    For i = 1 To ilLen Step 1
        ilNewCharCode = Asc(Mid(rString, i, 1)) - ilOffset
        If ilNewCharCode < 0 Then
            ilNewCharCode = ilNewCharCode + 255
        End If
        strRet = strRet & Chr(ilNewCharCode)
    Next i
    DecryptString = strRet
End Function

Private Function EncryptString(ByRef rString As String)
    Dim i As Integer
    Dim ilLen As Integer
    Dim strRet As String
    Dim ilNewCharCode As Integer

    ilLen = Len(rString)

    For i = 1 To ilLen Step 1
        ilNewCharCode = Asc(Mid(rString, i, 1)) + ilOffset
        If ilNewCharCode > 255 Then
            ilNewCharCode = ilNewCharCode - 255
        End If
        strRet = strRet & Chr(ilNewCharCode)
    Next i
    EncryptString = strRet
End Function

