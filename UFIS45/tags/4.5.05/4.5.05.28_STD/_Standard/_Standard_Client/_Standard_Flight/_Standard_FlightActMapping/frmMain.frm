VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{A2F31E92-C74F-11D3-A251-00500437F607}#1.0#0"; "UfisCom.ocx"
Object = "{7BDE1363-DF9A-4D96-A2A0-4E85E0191F0F}#1.0#0"; "AatLogin.ocx"
Begin VB.Form frmMain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mapping"
   ClientHeight    =   10410
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   12735
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   10410
   ScaleWidth      =   12735
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtHopo 
      Enabled         =   0   'False
      Height          =   330
      Left            =   3480
      TabIndex        =   2
      Text            =   "SIN"
      Top             =   10440
      Width           =   945
   End
   Begin VB.TextBox txtServer 
      Enabled         =   0   'False
      Height          =   330
      Left            =   1440
      TabIndex        =   1
      Text            =   "green"
      Top             =   10440
      Width           =   945
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   10080
      Width           =   12735
      _ExtentX        =   22463
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   19173
            MinWidth        =   15522
            Text            =   "Ready."
            TextSave        =   "Ready."
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Object.Width           =   1764
            MinWidth        =   1764
            TextSave        =   "12-Jun-08"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Object.Width           =   1411
            MinWidth        =   1411
            TextSave        =   "10:24 AM"
         EndProperty
      EndProperty
   End
   Begin UFISCOMLib.UfisCom UfisCom 
      Left            =   5160
      Top             =   10320
      _Version        =   65536
      _ExtentX        =   1164
      _ExtentY        =   1032
      _StockProps     =   0
   End
   Begin VB.Frame Frame1 
      Height          =   735
      Left            =   600
      TabIndex        =   3
      Top             =   10200
      Width           =   4335
      Begin VB.Label lblHOPO 
         Caption         =   "HOPO"
         Height          =   255
         Left            =   2160
         TabIndex        =   5
         Top             =   360
         Width           =   855
      End
      Begin VB.Label lblServer 
         Caption         =   "Server"
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   360
         Width           =   975
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "MAPPING for UFIS DB Fields and External Interfaces Info"
      Height          =   9855
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   12495
      Begin VB.CommandButton cmdExit 
         Caption         =   "Exit"
         Height          =   375
         Left            =   10920
         TabIndex        =   22
         Top             =   9000
         Width           =   1095
      End
      Begin VB.Frame Frame3 
         Caption         =   "Mapping Details"
         Height          =   2535
         Left            =   480
         TabIndex        =   8
         Top             =   6360
         Width           =   11535
         Begin VB.CommandButton cmdDelete 
            Caption         =   "Delete"
            Height          =   375
            Left            =   9840
            TabIndex        =   20
            Top             =   1440
            Width           =   1095
         End
         Begin VB.CommandButton cmdSave 
            Caption         =   "Save"
            Height          =   375
            Left            =   9840
            TabIndex        =   19
            Top             =   960
            Width           =   1095
         End
         Begin VB.TextBox txtLVAL 
            BackColor       =   &H0000FFFF&
            Height          =   375
            Left            =   2520
            TabIndex        =   18
            Top             =   1920
            Width           =   1815
         End
         Begin VB.ComboBox cmbRVAL 
            Height          =   315
            ItemData        =   "frmMain.frx":030A
            Left            =   2520
            List            =   "frmMain.frx":030C
            TabIndex        =   16
            Text            =   "cmbRVAL"
            Top             =   1440
            Width           =   1815
         End
         Begin VB.CommandButton cmdCancel 
            Caption         =   "Cancel"
            Height          =   375
            Left            =   9840
            TabIndex        =   14
            Top             =   480
            Width           =   1095
         End
         Begin VB.CommandButton cmdNew 
            Caption         =   "New"
            Height          =   375
            Left            =   9840
            TabIndex        =   13
            Top             =   1920
            Width           =   1095
         End
         Begin VB.ComboBox cmbTYPE 
            Height          =   315
            Left            =   2520
            Locked          =   -1  'True
            TabIndex        =   12
            Text            =   "CSK"
            Top             =   960
            Width           =   1815
         End
         Begin VB.ComboBox cmbLKEY 
            Height          =   315
            Left            =   2520
            Locked          =   -1  'True
            TabIndex        =   10
            Text            =   "ACT3"
            Top             =   480
            Width           =   1815
         End
         Begin VB.Label lblLVAL 
            Caption         =   "LVAL (External Info):"
            Height          =   375
            Left            =   360
            TabIndex        =   17
            Top             =   1920
            Width           =   1575
         End
         Begin VB.Label lblRVAL 
            Caption         =   "RVAL (UFIS Ref.) :"
            Height          =   375
            Left            =   360
            TabIndex        =   15
            Top             =   1440
            Width           =   1695
         End
         Begin VB.Label lblTYPE 
            Caption         =   "TYPE :"
            Height          =   375
            Left            =   360
            TabIndex        =   11
            Top             =   960
            Width           =   855
         End
         Begin VB.Label lblLKEY 
            Caption         =   "LKEY :"
            Height          =   375
            Left            =   360
            TabIndex        =   9
            Top             =   480
            Width           =   855
         End
      End
      Begin TABLib.TAB tab_MAPTAB 
         Height          =   5130
         Left            =   480
         TabIndex        =   21
         Tag             =   "{=TABLE=}MAPTAB{=FIELDS=}LKEY,TYPE,RVAL,LVAL,URNO,LSTU,USEC,USEU"
         Top             =   960
         Width           =   11505
         _Version        =   65536
         _ExtentX        =   20294
         _ExtentY        =   9049
         _StockProps     =   64
      End
      Begin VB.Label lblDesc 
         Caption         =   "This application is use to Mantain Mapping info in  MAPTAB"
         Height          =   375
         Left            =   480
         TabIndex        =   7
         Top             =   480
         Width           =   4335
      End
   End
   Begin AATLOGINLib.AatLogin AATLoginControl1 
      Height          =   495
      Left            =   6240
      TabIndex        =   23
      Top             =   10320
      Width           =   1335
      _Version        =   65536
      _ExtentX        =   2355
      _ExtentY        =   873
      _StockProps     =   0
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim strHOPO As String
Dim strServer As String
Dim globURNOlist As String

Dim globTestOnly As Boolean
Dim insertAction As Boolean
Private strTableExt As String
Public frmConnTimesDirty As Boolean
Public lmCurrCol As Long
Private gNewRecord As Boolean
Private gMAPLKEY As String
Private gMAPTYPE As String
Private gMAPRVAL As String
Private gMAPLVAL As String
Private gMAPURNO As String
Private gSaved As Boolean


Public Sub LoadData(ByRef rTab As TABLib.Tab, ByRef pWhere As String)
    Dim strCommand As String
    Dim strTable As String
    Dim strFieldList As String
    Dim strData As String
    Dim strWhere As String

    MousePointer = vbHourglass
    strCommand = "RT"
    rTab.ResetContent

    GetKeyItem strTable, rTab.Tag, "{=TABLE=}", "{="
    GetKeyItem strFieldList, rTab.Tag, "{=FIELDS=}", "{="
    strData = ""
    strWhere = pWhere
    If Len(strWhere) = 0 Then
        GetKeyItem strWhere, rTab.Tag, "{=WHERE=}", "{="
    End If
    If rTab.CedaAction(strCommand, strTable, strFieldList, strData, strWhere) = False Then
        MsgBox "Error loading " & strTable & "!" & vbCrLf & _
            "Field list: " & strFieldList & vbCrLf & _
            "Where statement: " & strWhere, vbCritical, "Error reading data (RT)"
            rTab.GetLastCedaError
    End If
    MousePointer = vbDefault
End Sub
Private Sub cmdDelete_Click()
    Dim strWhere As String
    Dim strTemp As String

    strTemp = "Delete this record?"
    Dim ilRet As Integer
    ilRet = MsgBox(strTemp, vbYesNo, "Delete Record")
    If ilRet = vbNo Then
        Exit Sub
    End If

    strWhere = "WHERE URNO='" & gMAPURNO & "'"
        
    Me.MousePointer = vbHourglass
    If frmMain.UfisCom.CallServer("DRT", "MAPTAB", "", "", strWhere, "360") <> 0 Then
        MsgBox frmMain.UfisCom.LastErrorMessage, vbCritical
        Exit Sub
    End If
    
    tab_MAPTAB.ResetContent
    LoadData tab_MAPTAB, "order by URNO"
    
    InitTabs
    gNewRecord = True
    gSaved = True
    Me.MousePointer = vbNormal
End Sub

Private Sub cmdExit_Click()
End
    Dim ilRet
    ilRet = MsgBox("Do you want to exit ?", vbYesNo, "EXIT")
    If ilRet = vbYes Then
        Unload frmData
        End
    End If
End Sub

Private Sub cmdNew_Click()
    'cmbRVAL.Clear
    InitTabs
    gNewRecord = True
    gSaved = True
End Sub


Private Sub cmdCancel_Click()
    'cmbRVAL.Clear
    InitTabs
    gNewRecord = True
    gSaved = True
End Sub
Private Sub cmdSave_Click()

    'Dim strTemp As String
    'Dim strCMD As String
    Dim strData As String
    Dim strFields As String
    Dim strWhere As String
    Dim i As Integer
    Dim IRTConfirmation As Boolean
    Dim ilRet As Integer
    
    'gMAPLKEY = cmbLKEY.Text
    'gMAPTYPE = cmbTYPE.Text
    'gMAPRVAL = cmbRVAL.Text
    'gMAPLVAL = txtLVAL.Text
    strFields = "LKEY,TYPE,RVAL,LVAL"
    
    '### REPLACE , WITH | FOR CEDA DB
    'strTemp = Replace(txtTADR.Text, ",", "|")
    strData = cmbLKEY.Text & "," & cmbTYPE.Text & "," & cmbRVAL.Text & "," & txtLVAL.Text

    '### CHECK MANDATORY FIELDS
    If Trim(txtLVAL.Text) = "" Then
        MsgBox "LVAL not enter.", vbCritical
        txtLVAL.SetFocus
        Exit Sub
    End If
    'StatusBar1.Panels(1).Text = gNewRecord
    If (gMAPLVAL <> txtLVAL.Text) And (gMAPRVAL <> cmbRVAL.Text) Then
        gNewRecord = True
    End If
    
    Me.MousePointer = vbHourglass
    If gNewRecord = True Then
'        tab_MAPTAB.ResetContent
'        strWhere = "WHERE LVAL = '" & UCase(gMAPLVAL) & "'"
'        frmData.LoadData tab_MAPTAB, strWhere
        Debug.Print tab_MAPTAB.GetLineCount
        For i = 0 To tab_MAPTAB.GetLineCount - 1
            Debug.Print tab_MAPTAB.GetColumnValues(i, 3)
            If tab_MAPTAB.GetColumnValues(i, 3) = UCase(txtLVAL.Text) Then
               MsgBox ("LVAL :" + UCase(txtLVAL.Text) + " Record Exists.")
                MousePointer = vbDefault
                'cmdCancel_Click
                Exit Sub
            End If
        Next i
        
        For i = 0 To frmData.tab_ACTTAB.GetLineCount - 1 Step 1
            If cmbRVAL.Text = frmData.tab_ACTTAB.GetColumnValues(i, 1) Then
                IRTConfirmation = True
                Exit For
            Else
                Debug.Print frmData.tab_ACTTAB.GetColumnValues(i, 1)
                IRTConfirmation = False
            End If
        Next i
        
        If IRTConfirmation = False Then
            'Debug.Print frmData.tab_ACTTAB.GetColumnValues(i, 1)
            MsgBox "RVAL " & cmbRVAL.Text & " is NOT UFIS Basic Data, mapping NOT allow"
'            ilRet = MsgBox("The RVAL is NOT in Basic Data, continue create?", vbYesNo, "Create Record")
'            If ilRet = vbNo Then
                Me.MousePointer = vbNormal
                Exit Sub
'            End If
        End If
        
        If frmMain.UfisCom.CallServer("IRT", "MAPTAB", strFields, strData, "", "360") <> 0 Then
            MsgBox frmMain.UfisCom.LastErrorMessage, vbCritical
            Exit Sub
        End If
    Else
        
        If gMAPRVAL = cmbRVAL.Text Then
            For i = 0 To tab_MAPTAB.GetLineCount - 1
                'checking for prevent duplicate entry of LVAL in MAPTAB
                If tab_MAPTAB.GetColumnValues(i, 3) = UCase(txtLVAL.Text) Then
                    MsgBox ("LVAL :" + UCase(txtLVAL.Text) + " Already Exists.")
                    MousePointer = vbDefault
                    'cmdCancel_Click
                    Exit Sub
                End If
            Next i
        Else
            For i = 0 To tab_MAPTAB.GetLineCount - 1
                'checking for prevent duplicate entry of LVAL in MAPTAB
                If tab_MAPTAB.GetColumnValues(i, 3) = UCase(txtLVAL.Text) And tab_MAPTAB.GetColumnValues(i, 2) = cmbRVAL.Text Then
                    MsgBox ("LVAL :" + UCase(txtLVAL.Text) + " Record Exists.")
                    MousePointer = vbDefault
                    'cmdCancel_Click
                    Exit Sub
                End If
            Next i
'
'            If gMAPLVAL <> UCase(txtLVAL.Text) Then
'                MsgBox ("LVAL :" + UCase(txtLVAL.Text) + " Halo Already Exists.")
'                MousePointer = vbDefault
'                'cmdCancel_Click
'                Exit Sub
'            End If
        End If
        
        'Updating existing record
        strWhere = "WHERE URNO='" & gMAPURNO & "'"
        If frmMain.UfisCom.CallServer("URT", "MAPTAB", strFields, strData, strWhere, "360") <> 0 Then
            MsgBox frmMain.UfisCom.LastErrorMessage, vbCritical
            Exit Sub
        End If
    End If
    
    InitTabs
    Me.MousePointer = vbNormal
End Sub

Private Sub Form_Load()
    InitTabs
End Sub
Sub InitTabs()

    Dim cnt As Long
    Dim i As Long
     'Initialise ACTTAB
    cmbRVAL.Clear
    frmData.InitTabGeneral frmData.tab_ACTTAB
    frmData.InitTabForCedaConnection frmData.tab_ACTTAB
    frmData.LoadData frmData.tab_ACTTAB, "where ACT3 <> ' ' order by ACT3"
    For i = 0 To frmData.tab_ACTTAB.GetLineCount - 1 Step 1
        cmbRVAL.AddItem frmData.tab_ACTTAB.GetColumnValues(i, 1), i
    Next i
    cmdSave.Enabled = False
    cmdDelete.Enabled = False
        
    txtLVAL.Text = ""
    RefreshView
    
'     'Initialise MAPTAB
'    frmData.InitTabGeneral tab_MAPTAB
'    frmData.InitTabForCedaConnection tab_MAPTAB
'    frmData.LoadData tab_MAPTAB, "order by URNO"
'    Debug.Print tab_MAPTAB.GetLineCount
'    For i = 0 To tab_MAPTAB.GetLineCount - 1 Step 1
'        Debug.Print "step in"
'        Debug.Print tab_MAPTAB.GetColumnValue(i, 2)
'    Next i

End Sub

Public Sub RefreshView()
    'Initialise MAPTAB
    frmData.InitTabGeneral tab_MAPTAB
    frmData.InitTabForCedaConnection tab_MAPTAB
    frmData.LoadData tab_MAPTAB, "order by URNO"
    tab_MAPTAB.AutoSizeColumns
    
    tab_MAPTAB.HeaderLengthString = "60,60,126,126,105,120,84,84"
    tab_MAPTAB.HeaderString = "LKEY,TYPE,RVAL(UFIS Ref),LVAL(Ext Info),URNO,LSTU,USEC,USEU"
    tab_MAPTAB.HeaderFontSize = 17
    gMAPLKEY = ""
    gMAPTYPE = ""
    gMAPRVAL = ""
    gMAPLVAL = ""
    gMAPURNO = ""
    
    cmdCancel.Refresh
    cmdSave.Refresh
    cmdDelete.Refresh
    cmdNew.Refresh
    
    StatusBar1.Panels(1).Text = "Ready."
    
    gSaved = True
    gNewRecord = True
End Sub

Private Sub tab_MAPTAB_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim strkey1 As String
    Dim strWhere As String
    Dim strTemp As String
    Dim strFields As String
    Dim strValues As String
    Dim l As Long
    Dim i As Long
    Dim cmbRVALCnt As Long
    Dim llLineCount As Long
    Dim INlist As String
    
    l = tab_MAPTAB.GetCurrentSelected
    
    If gSaved = True Then
        If l <> -1 Then
            MousePointer = vbHourglass
            
            gMAPLKEY = tab_MAPTAB.GetColumnValues(l, 0)
                        
            gMAPTYPE = tab_MAPTAB.GetColumnValues(l, 1)
            
            gMAPRVAL = tab_MAPTAB.GetColumnValues(l, 2)
            
            cmbRVAL.Clear
            For i = 0 To frmData.tab_ACTTAB.GetLineCount - 1 Step 1
                cmbRVAL.AddItem frmData.tab_ACTTAB.GetColumnValues(i, 1), i
            Next i
            For i = 0 To cmbRVAL.ListCount - 1
                If cmbRVAL.List(i) = gMAPRVAL Then
                    cmbRVAL.SelText = cmbRVAL.List(i)
                    'cmbRVAL.Text.List (i)
                End If
            Next i
            
            gMAPLVAL = tab_MAPTAB.GetColumnValues(l, 3)
            txtLVAL = gMAPLVAL
            
            gMAPURNO = tab_MAPTAB.GetColumnValues(l, 4)
           
            gNewRecord = False
            cmdSave.Enabled = False
            cmdDelete.Enabled = True
            MousePointer = vbDefault
        End If
    Else
        MsgBox "Changes not saved.", vbCritical
    End If
    
    If LineNo = -1 Then
        tab_MAPTAB.Sort ColNo, True, False
        tab_MAPTAB.Refresh
    End If
End Sub

Private Sub txtLVAL_Change()
    If (txtLVAL.Text <> "" And gMAPLVAL <> txtLVAL.Text) Or (gMAPRVAL <> cmbRVAL.Text) Then
        cmdSave.Enabled = True
    Else
        cmdSave.Enabled = False
    End If
    
    'cmbRVAL_Click
End Sub

Private Sub cmbRVAL_Click()
    If (gMAPRVAL <> cmbRVAL.Text) Or (gMAPLVAL <> txtLVAL.Text) Then
        cmdSave.Enabled = True
    Else
        cmdSave.Enabled = False
    End If
End Sub
Private Sub Form_Unload(Cancel As Integer)
    cmdExit_Click
End Sub
'Public Sub SetPrivStat()
'    Dim strPrivRet As String
'    strPrivRet = AATLoginControl1.GetPrivileges("m_TAB_Configuration")
'
'    If strPrivRet = "0" Or strPrivRet = "-" Then
'        SSTab1.TabVisible(0) = False
'    End If
'End Sub
