VERSION 5.00
Begin VB.Form EditTemplates 
   Caption         =   "Edit Telex Template"
   ClientHeight    =   7410
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10320
   LinkTopic       =   "Form1"
   ScaleHeight     =   7410
   ScaleWidth      =   10320
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtTplOwner 
      Height          =   285
      Left            =   7560
      TabIndex        =   11
      Top             =   240
      Width           =   2655
   End
   Begin VB.TextBox txtTplType 
      Height          =   285
      Left            =   5640
      TabIndex        =   9
      Top             =   240
      Width           =   975
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   9480
      TabIndex        =   7
      Top             =   6480
      Width           =   735
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   9480
      TabIndex        =   6
      Top             =   6960
      Width           =   735
   End
   Begin VB.TextBox txtTplText 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6015
      Left            =   1560
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   5
      Top             =   1200
      Width           =   7575
   End
   Begin VB.TextBox txtTplDesc 
      Height          =   285
      Left            =   1560
      TabIndex        =   4
      Top             =   720
      Width           =   7575
   End
   Begin VB.TextBox txtTplName 
      Height          =   285
      Left            =   1560
      TabIndex        =   3
      Top             =   240
      Width           =   3255
   End
   Begin VB.Label Label5 
      Caption         =   "Owner:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6840
      TabIndex        =   10
      Top             =   240
      Width           =   615
   End
   Begin VB.Label Label4 
      Caption         =   "Type :"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5040
      TabIndex        =   8
      Top             =   240
      Width           =   615
   End
   Begin VB.Label Label3 
      Caption         =   "Text:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   1200
      Width           =   975
   End
   Begin VB.Label Label2 
      Caption         =   "Description :"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   720
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "Name :"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   1215
   End
End
Attribute VB_Name = "EditTemplates"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCancel_Click()

    EditTemplates.Visible = False
End Sub

Private Sub cmdOK_Click()
    Dim RetVal As Integer
    Dim ActResult As String
    Dim ActCmd As String
    Dim ActTable As String
    Dim ActFldLst As String
    Dim ActDatLst As String
    Dim ActCondition As String
    Dim ActOrder As String
    Dim i As Integer
    Dim found As Boolean
    Dim NewLine As String
    Dim ErrorMsg As String
    Dim tmpText As String
    Dim tmpText1 As String
    Dim tmpText2 As String

    ErrorMsg = ""
    If Len(txtTplName.Text) = 0 Then
        ErrorMsg = "Missing Template Name!"
    End If
    If Len(txtTplText.Text) = 0 Then
        ErrorMsg = "Missing Template Text!"
    End If
    If Len(txtTplName.Text) = 0 And Len(txtTplText.Text) = 0 Then
        ErrorMsg = "Missing Template Name and Text!"
    End If
    If Len(ErrorMsg) > 0 Then
        MyMsgBox.CallAskUser 0, 0, 0, "Edit Templates", ErrorMsg, "hand", "", UserAnswer
    Else
        If Len(txtTplDesc.Text) = 0 Then
            txtTplDesc.Text = " "
        End If
        If Len(txtTplType) = 0 Then
            txtTplType.Text = "FREE"
        Else
            found = False
            For i = 0 To EditTelex.TlxTypeTab.GetLineCount - 1
                If txtTplType.Text = EditTelex.TlxTypeTab.GetColumnValue(i, 0) Then
                    found = True
                End If
            Next
            If found = False Then
                NewLine = txtTplType.Text & ",,"
                EditTelex.TlxTypeTab.InsertTextLine NewLine, False
            End If
        End If
        tmpText = CleanString(txtTplText.Text, FOR_SERVER, False)
        If Len(tmpText) <= 4000 Then
            tmpText1 = tmpText
            tmpText2 = " "
        Else
            i = 4000
            While i > 1 And Mid(tmpText, i, 1) = " "
                i = i - 1
            Wend
            tmpText1 = Mid(tmpText, 1, i)
            tmpText2 = Mid(tmpText, i + 1, 4000)
        End If
        If txtTplName.Tag = "INS" Then
            ActResult = ""
            ActCmd = "IRT"
            ActTable = "TTPTAB"
            ActFldLst = "URNO,HOPO,TYPE,TPLN,TPDE,TPTX,TPT2,TPTP,TPOW"
            ActCondition = ""
            ActOrder = ""
            EditTelex.chkSelAddr(2).Tag = UfisServer.UrnoPoolGetNext
            ActDatLst = EditTelex.chkSelAddr(2).Tag & "," & _
                        UfisServer.HOPO & ",T," & _
                        txtTplName.Text & "," & _
                        txtTplDesc.Text & "," & _
                        tmpText1 & "," & _
                        tmpText2 & "," & _
                        txtTplType.Text & "," & _
                        txtTplOwner.Text
            RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
        Else
            ActResult = ""
            ActCmd = "URT"
            ActTable = "TTPTAB"
            ActFldLst = "TPLN,TPDE,TPTX,TPT2,TPTP"
            ActCondition = "WHERE URNO = " & txtTplName.Tag
            ActOrder = ""
            ActDatLst = txtTplName.Text & "," & _
                        txtTplDesc.Text & "," & _
                        tmpText1 & "," & _
                        tmpText2 & "," & _
                        txtTplType.Text
            RetVal = UfisServer.CallCeda(ActResult, ActCmd, ActTable, ActFldLst, ActDatLst, ActCondition, ActOrder, 0, True, False)
        End If
        EditTelex.LoadTemplateData
        EditTemplates.Visible = False
    End If
End Sub

Private Sub txtTplText_KeyPress(KeyAscii As Integer)
    If EditTelex.chkLcase.Value = 0 Then KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub


