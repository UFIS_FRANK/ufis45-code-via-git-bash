VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form AirportEvents 
   Caption         =   "Airport Events"
   ClientHeight    =   7065
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11550
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7065
   ScaleWidth      =   11550
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   8220
      Top             =   2790
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   178
      ImageHeight     =   110
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "AirportEvents.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "AirportEvents.frx":E6A2
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "AirportEvents.frx":186CC
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox MainPanel 
      BackColor       =   &H00008080&
      Height          =   5835
      Index           =   0
      Left            =   150
      ScaleHeight     =   5775
      ScaleWidth      =   7650
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   480
      Width           =   7710
      Begin VB.PictureBox SubPanelM 
         BackColor       =   &H00FF8080&
         BorderStyle     =   0  'None
         Height          =   5415
         Index           =   0
         Left            =   0
         ScaleHeight     =   5415
         ScaleWidth      =   7290
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   0
         Width           =   7290
         Begin VB.PictureBox GrpPanelM 
            BackColor       =   &H00C00000&
            BorderStyle     =   0  'None
            Height          =   4965
            Index           =   0
            Left            =   0
            ScaleHeight     =   4965
            ScaleWidth      =   6840
            TabIndex        =   7
            TabStop         =   0   'False
            Top             =   0
            Width           =   6840
         End
      End
   End
   Begin VB.PictureBox ButtonRow 
      BackColor       =   &H00C0C0FF&
      BorderStyle     =   0  'None
      Height          =   315
      Index           =   1
      Left            =   120
      ScaleHeight     =   315
      ScaleWidth      =   8295
      TabIndex        =   4
      Top             =   6720
      Width           =   8295
   End
   Begin VB.PictureBox VPanel 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   161
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6210
      Index           =   0
      Left            =   9480
      ScaleHeight     =   6210
      ScaleWidth      =   1950
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   450
      Width           =   1950
      Begin VB.PictureBox PicPanel 
         BackColor       =   &H008080FF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   660
         Index           =   1
         Left            =   780
         ScaleHeight     =   660
         ScaleWidth      =   1110
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   60
         Width           =   1110
         Begin VB.Image AnyPic 
            Height          =   480
            Index           =   1
            Left            =   0
            Picture         =   "AirportEvents.frx":1CF4E
            Stretch         =   -1  'True
            Top             =   0
            Width           =   1110
         End
      End
      Begin VB.PictureBox PicPanel 
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   161
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   690
         Index           =   0
         Left            =   60
         ScaleHeight     =   690
         ScaleWidth      =   1125
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   150
         Width           =   1125
         Begin VB.Image AnyPic 
            Height          =   480
            Index           =   0
            Left            =   0
            Picture         =   "AirportEvents.frx":1EB90
            Stretch         =   -1  'True
            Top             =   0
            Width           =   1125
         End
      End
   End
   Begin VB.PictureBox ButtonRow 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      Height          =   315
      Index           =   0
      Left            =   180
      ScaleHeight     =   315
      ScaleWidth      =   8295
      TabIndex        =   0
      Top             =   90
      Width           =   8295
   End
End
Attribute VB_Name = "AirportEvents"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    InitMyForm
End Sub

Private Sub InitMyForm()
    Dim NewHeight As Long
    Dim NewWidth As Long
    Dim NewTop As Long
    Dim i As Integer
    ButtonRow(1).Visible = False
    
    'ButtonRow(0).BackColor = LightGray
    'ButtonRow(1).BackColor = LightGray
    
    NewHeight = Screen.Height
    VPanel(0).Top = 0
    VPanel(0).Height = NewHeight
    i = 0
    PicPanel(i).Left = 0
    PicPanel(i).Top = 0
    PicPanel(i).Height = NewHeight
    AnyPic(i).Left = 0
    AnyPic(i).Top = 0
    AnyPic(i).Height = PicPanel(i).ScaleHeight
    i = 1
    PicPanel(i).Width = 1020
    PicPanel(i).Left = VPanel(0).ScaleWidth - PicPanel(i).Width
    PicPanel(i).Top = 0
    PicPanel(i).Height = NewHeight
    AnyPic(i).Left = PicPanel(i).ScaleWidth - AnyPic(i).Width
    AnyPic(i).Top = 0
    AnyPic(i).Height = PicPanel(i).ScaleHeight
    
    NewWidth = Me.ScaleWidth - VPanel(0).Width
    ButtonRow(0).Top = 0
    ButtonRow(0).Left = 0
    ButtonRow(1).Left = 0
    
    InitMainPanel 0
    
    VPanel(0).ZOrder
End Sub
Private Sub InitMainPanel(idx As Integer)
    Dim NewTop As Long
    NewTop = ButtonRow(0).Top + ButtonRow(0).Height
    MainPanel(idx).Top = NewTop
    MainPanel(idx).Left = 0
    InitSubPanel 0
End Sub
Private Sub InitSubPanel(idx As Integer)
    Dim NewTop As Long
    NewTop = 0
    SubPanelM(idx).Top = NewTop
    SubPanelM(idx).Left = 0
End Sub

Private Sub Form_Resize()
    Dim NewLeft As Long
    Dim NewWidth As Long
    Dim SubWidth As Long
    Dim GrpWidth As Long
    Dim NewTop As Long
    Dim NewHeight As Long
    Dim SubHeight As Long
    Dim GrpHeight As Long
    Dim i As Integer
    NewLeft = Me.ScaleWidth - VPanel(0).Width
    VPanel(0).Left = NewLeft
    NewWidth = NewLeft
    If NewWidth > 90 Then
        ButtonRow(0).Width = NewWidth
        ButtonRow(1).Width = NewWidth
        MainPanel(0).Width = NewWidth
        SubWidth = MainPanel(0).ScaleWidth
        For i = 0 To SubPanelM.UBound
            SubPanelM(i).Width = SubWidth
        Next
    End If
    NewTop = Me.ScaleHeight - ButtonRow(1).Height
    ButtonRow(1).Top = NewTop
    NewHeight = NewTop
    If NewHeight > 90 Then
        MainPanel(0).Height = NewHeight
        SubHeight = MainPanel(0).ScaleHeight
        For i = 0 To SubPanelM.UBound
            SubPanelM(i).Height = SubHeight
        Next
        GrpHeight = SubHeight
        For i = 0 To SubPanelM.UBound
            GrpPanelM(i).Height = GrpHeight
        Next
    End If
End Sub
