VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form RcvTelex 
   Caption         =   "Received Telexes"
   ClientHeight    =   2370
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4380
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "RcvTelex.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   2370
   ScaleWidth      =   4380
   StartUpPosition =   3  'Windows Default
   Tag             =   "Received Telexes"
   Begin VB.PictureBox ListPanel 
      Height          =   2265
      Left            =   0
      ScaleHeight     =   2205
      ScaleWidth      =   4125
      TabIndex        =   4
      Top             =   300
      Width           =   4185
      Begin TABLib.TAB tabTelexList 
         Height          =   1170
         Index           =   1
         Left            =   150
         TabIndex        =   5
         Top             =   840
         Visible         =   0   'False
         Width           =   3750
         _Version        =   65536
         _ExtentX        =   6615
         _ExtentY        =   2064
         _StockProps     =   64
         Columns         =   9
         Lines           =   10
      End
      Begin TABLib.TAB tabTelexList 
         Height          =   1710
         Index           =   0
         Left            =   330
         TabIndex        =   6
         Top             =   30
         Width           =   3750
         _Version        =   65536
         _ExtentX        =   6615
         _ExtentY        =   3016
         _StockProps     =   64
         Columns         =   9
         Lines           =   10
      End
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   3570
      Top             =   30
   End
   Begin VB.CheckBox chkLock 
      Caption         =   "Lock"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1740
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox OpenPool 
      Caption         =   "Main"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox chkLoad 
      Caption         =   "Refresh"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   870
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox OnTop 
      Caption         =   "On Top"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2610
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   0
      Width           =   855
   End
End
Attribute VB_Name = "RcvTelex"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim PrevWindowState As Integer
Private m_Snap As CSnapDialog

Private Sub chkLock_Click()
    If chkLock.Value = 1 Then
        tabTelexList(0).LockScroll True
        TelexPoolHead.tabTelexList(2).LockScroll True
    Else
        tabTelexList(0).LockScroll False
        TelexPoolHead.tabTelexList(2).LockScroll False
    End If
End Sub

Private Sub Form_Activate()
    Static AlreadyDone As Boolean
    If Not AlreadyDone Then
        AlreadyDone = True
        tabTelexList(0).Refresh
        Me.Refresh
        DoEvents
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    CheckKeyboard Shift
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    CheckKeyboard Shift
End Sub

Private Sub Form_Load()
        If UseSnapFeature Then
            If Not ApplIsInDesignMode Then
                Set m_Snap = New CSnapDialog
                m_Snap.hwnd = Me.hwnd
            End If
        End If
    InitForm 6
    InitPosSize
    If Not AutoRefreshOnline Then
        chkLoad.BackColor = DarkGreen
        chkLoad.ForeColor = vbWhite
    End If
End Sub

Private Sub InitPosSize()
    Dim MainTop As Long
    Dim MainHight As Long
    Dim NewHeight As Long
    Dim NewTop As Long
    Dim MyPlace As Long
    MainHight = MaxScreenHeight
    MainTop = (Screen.Height - MainHight) / 2
    MyPlace = GetMyStartupWindowPlace(Me.Name)
    Me.Width = 3600
    If OnlineWindows > 0 Then
        NewHeight = MainHight / OnlineWindows
        NewTop = MainTop + (NewHeight * (MyPlace - 1))
        Me.Top = NewTop
        Me.Left = MaxScreenWidth - Me.Width
        Me.Height = NewHeight
        If Not MainIsOnline Then Me.Show
    Else
        Me.Top = 3675
        Me.Left = MaxScreenWidth - Me.Width
        Me.Height = 6000
    End If
End Sub

Private Sub InitForm(ipLines As Integer)
    Dim RcvIdx As Integer
    RcvTelex.Tag = GetIniEntry(myIniFullName, "MAIN_WINDOW", "", "RECV_CAPTION", "Received Telexes")
    RcvTelex.Caption = RcvTelex.Tag
    For RcvIdx = 0 To 1
        tabTelexList(RcvIdx).ResetContent
        tabTelexList(RcvIdx).FontName = "Courier New"
        tabTelexList(RcvIdx).HeaderFontSize = MyFontSize
        tabTelexList(RcvIdx).FontSize = MyFontSize
        tabTelexList(RcvIdx).lineHeight = MyFontSize
        tabTelexList(RcvIdx).SetTabFontBold MyFontBold
        tabTelexList(RcvIdx).Top = 0
        tabTelexList(RcvIdx).Left = 0
        tabTelexList(RcvIdx).Width = ListPanel.ScaleWidth
        tabTelexList(RcvIdx).Height = ipLines * tabTelexList(0).lineHeight * Screen.TwipsPerPixelY
        tabTelexList(RcvIdx).HeaderString = "S,W,Type,ST,Time,No,Text Extract,URNO,Index,FLNU," & Space(500)
        tabTelexList(RcvIdx).HeaderLengthString = "12,12,32,22,40,31,819,50,50,50,500"
        tabTelexList(RcvIdx).SetUniqueFields "7"
        tabTelexList(RcvIdx).LifeStyle = True
        tabTelexList(RcvIdx).ShowHorzScroller True
        tabTelexList(RcvIdx).DateTimeSetColumn 4
        tabTelexList(RcvIdx).DateTimeSetInputFormatString 4, "YYYYMMDDhhmmss"
        tabTelexList(RcvIdx).DateTimeSetOutputFormatString 4, "hh':'mm"
        tabTelexList(RcvIdx).AutoSizeByHeader = True
        tabTelexList(RcvIdx).AutoSizeColumns
    Next
    tabTelexList(1).Left = 1200
    ListPanel.Height = tabTelexList(0).Height + 2 * Screen.TwipsPerPixelY
    Me.Height = ListPanel.Top + ListPanel.Height + (Me.Height - Me.ScaleHeight)
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If Not ShutDownRequested Then
        If UnloadMode = 0 Then
            Cancel = CheckCancelExit(Me, 1)
        End If
    End If
End Sub

Private Sub Form_Resize()
    If (PrevWindowState = vbNormal) And (Me.WindowState = vbNormal) And (Not SystemResizing) Then LastResizeName = Me.Name
    If Me.WindowState <> vbMinimized Then
        ListPanel.Width = Me.ScaleWidth - 2 * ListPanel.Left
        tabTelexList(0).Width = ListPanel.ScaleWidth
        tabTelexList(1).Width = ListPanel.ScaleWidth
        If Me.ScaleHeight > 500 Then
            ListPanel.Height = Me.ScaleHeight - ListPanel.Top - ListPanel.Left
            tabTelexList(0).Height = ListPanel.ScaleHeight
            tabTelexList(1).Height = ListPanel.ScaleHeight
        End If
        SyncWindowSize Me.Left, Me.Width
    End If
End Sub

Private Sub chkLoad_Click()
    If chkLoad.Value = 1 Then
        chkLoad.BackColor = LightGreen
        chkLoad.ForeColor = vbBlack
        chkLoad.Refresh
        TelexPoolHead.RefreshOnlineWindows "RCV"
        chkLoad.Value = 0
    Else
        chkLoad.BackColor = vbButtonFace
    End If
End Sub

Private Sub OnTop_Click()
    If OnTop.Value = 1 Then OnTop.BackColor = LightGreen Else OnTop.BackColor = vbButtonFace
    If OnTop.Value <> 1 Then SetFormOnTop Me, False, False
    SetAllFormsOnTop True
End Sub

Private Sub OpenPool_Click()
    On Error Resume Next
    If OpenPool.Value = 1 Then
        SwitchMainWindows "MAIN"
        OpenPool.Value = 0
    End If
End Sub

Private Sub tabTelexList_SendLButtonClick(Index As Integer, ByVal Line As Long, ByVal Col As Long)
    Dim tmpUrno As String
    Dim tmpWsta As String
    Dim clSqlKey As String
    Dim RetCode As Integer
    Dim retval As String
    tmpWsta = Trim(tabTelexList(Index).GetColumnValue(Line, 1))
    tmpUrno = Trim(tabTelexList(Index).GetColumnValue(Line, 7))
    If tmpUrno <> "" Then
        SwitchMainWindows "MAIN"
        clSqlKey = "WHERE URNO=" & tmpUrno
        'If (UpdateWstaOnClick = True) And (tmpWsta = "") Then
            UfisServer.TwsCode.Text = ".NW."
            RetCode = UfisServer.CallCeda(retval, "URT", DataPool.TableName, "WSTA", "V", clSqlKey, "", 0, True, False)
            UfisServer.TwsCode.Text = ""
            tabTelexList(Index).SetColumnValue Line, 1, "V"
            tabTelexList(Index).Refresh
        'End If
        TelexPoolHead.LoadTelexData 2, tmpUrno, clSqlKey, "", "ALL", "RCV"
        tabTelexList(Index).SetFocus
    End If
End Sub

Private Sub tabTelexList_SendMouseMove(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal Flags As String)
    Dim tmpCode As String
    tmpCode = "RCV.TABTLX." & CStr(Index)
    If MouseOverObjCode <> tmpCode Then Set MouseOverTabOcx = Me.tabTelexList(Index)
    SetMouseWheelObjectCode tmpCode
End Sub

Private Sub tabTelexList_TimerExpired(Index As Integer, ByVal LineNo As Long, ByVal LineStatus As Long)
    Dim tmpUrno As String
    If Index = 1 Then
        If MainIsOnline Then
            tmpUrno = Trim(tabTelexList(1).GetColumnValue(LineNo, 7))
            DataPool.ClearOnlineTelex tmpUrno
        End If
        tabTelexList(Index).DeleteLine LineNo
        If tabTelexList(Index).GetLineCount < 1 Then Timer1.Enabled = False
    End If
End Sub

Private Sub Timer1_Timer()
    tabTelexList(1).TimerCheck
End Sub

Public Sub ShowHiddenTelex(LineIdx As Long)
    Dim tmpData As String
    Dim tmpSere As String
    Dim tmpTtyp As String
    Dim tmpText As String
    Dim clFields As String
    Dim clData As String
    Dim clBcNum As String
    Dim FilterLookUp As String
    Dim RcvTlxLine As String
    Dim LineNo As Long
    Dim MaxLinCnt As Long
    Dim myTextColor As Long
    Dim myBackColor As Long
    Dim ShowTelex As Boolean
    Dim tmpUrno As String
    ShowTelex = True
    tmpTtyp = Trim(tabTelexList(1).GetColumnValue(LineIdx, 2))
    tmpText = Trim(tabTelexList(1).GetColumnValue(LineIdx, 6))
    If MySetUp.AddrFilter.Value = 1 Then ShowTelex = FilterTelexAddress(tmpText, MySetUp.HomeTlxAddr.Text, AddrNoCheck, tmpTtyp, True, "R")
    ShowTelex = FilterTelexText(tmpText, IgnoreAllTlxText, ShowTelex)
    ShowTelex = FilterTelexTypeText(tmpTtyp, tmpText, TextFilterList, ShowTelex)
    If (tmpText = "") And (IgnoreEmptyText) Then ShowTelex = False
    If ShowTelex Then
        ShowTelex = False
        FilterLookUp = MySetUp.CheckOnlineFilter("LOOKUP_RCV", tmpTtyp)
        If FilterLookUp = "OK" Then
            ShowTelex = True
            LineNo = -1
            If MainIsOnline Then
                tmpUrno = Trim(tabTelexList(1).GetColumnValue(LineIdx, 7))
                LineNo = DataPool.ReleaseOnlineTelex(tmpUrno)
            End If
            tmpSere = "R"
            tmpData = TelexPoolHead.GetTelexExtract(tmpSere, tmpTtyp, tmpText, 80)
            tabTelexList(1).SetColumnValue LineIdx, 6, tmpData
            RcvTlxLine = tabTelexList(1).GetLineValues(LineIdx)
            tabTelexList(0).InsertTextLineAt 0, RcvTlxLine, True
            tabTelexList(1).GetLineColor LineIdx, myTextColor, myBackColor
            tabTelexList(0).SetLineColor 0, myTextColor, myBackColor
            If MainIsOnline Then
                'Here we don't have a BcNum. We'll use TIME instead
                'tabTelexList(RcvIdx).HeaderString = "S,W,Type,ST,Time,No,Text Extract,URNO,Index,FLNU," & Space(500)
                clBcNum = Trim(tabTelexList(1).GetColumnValue(LineIdx, 4))
                clBcNum = Mid(clBcNum, 9, 4)
                clData = ""
                clFields = "SERE,STAT,WSTA,TTYP,URNO"
                tmpData = "R" 'SERE
                clData = clData & tmpData & ","
                tmpData = Trim(tabTelexList(1).GetColumnValue(LineIdx, 0)) 'STAT
                clData = clData & tmpData & ","
                tmpData = Trim(tabTelexList(1).GetColumnValue(LineIdx, 1)) 'WSTA
                clData = clData & tmpData & ","
                tmpData = Trim(tabTelexList(1).GetColumnValue(LineIdx, 2)) 'TTYP
                clData = clData & tmpData & ","
                tmpData = Trim(tabTelexList(1).GetColumnValue(LineIdx, 7)) 'URNO
                clData = clData & tmpData & ","
                TelexPoolHead.EvaluateBc 2, 0, clBcNum, "", "", "", "", "", clFields, clData, -1
                
                DoNotRefreshTelexList = True 'igu on 5 Aug 2010
                TelexPoolHead.tabTelexList(2).InsertTextLineAt 0, RcvTlxLine, True
                TelexPoolHead.tabTelexList(2).SetLineColor 0, myTextColor, myBackColor
                TelexPoolHead.tabTelexList(2).SetColumnValue 0, 8, CStr(LineNo)
                TelexPoolHead.tabTelexList(2).AutoSizeColumns
                DoNotRefreshTelexList = False 'igu on 5 Aug 2010
            End If
            MaxLinCnt = tabTelexList(0).GetLineCount
            If Not MainIsOnline Then
                If MaxLinCnt > MaxOnlineCount Then
                    MaxLinCnt = MaxLinCnt - 1
                    tabTelexList(0).DeleteLine MaxLinCnt
                End If
            End If
            Me.Caption = CStr(MaxLinCnt) & " " & Me.Tag
            tabTelexList(0).Refresh
        End If
    End If
    If (MainIsOnline) And (Not ShowTelex) Then
        tmpUrno = Trim(tabTelexList(1).GetColumnValue(LineIdx, 7))
        DataPool.ClearOnlineTelex tmpUrno
    End If
    tabTelexList(1).DeleteLine LineIdx
    If tabTelexList(1).GetLineCount < 1 Then Timer1.Enabled = False
End Sub

