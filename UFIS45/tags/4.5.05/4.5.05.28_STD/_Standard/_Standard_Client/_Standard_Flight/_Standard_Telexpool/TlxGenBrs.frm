VERSION 5.00
Begin VB.Form TlxGenBrs 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Create BRS Telexes Setup"
   ClientHeight    =   1575
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6510
   ControlBox      =   0   'False
   Icon            =   "TlxGenBrs.frx":0000
   LinkTopic       =   "Form3"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1575
   ScaleWidth      =   6510
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox chkReset 
      Caption         =   "Reset"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5610
      Style           =   1  'Graphical
      TabIndex        =   17
      Top             =   960
      Width           =   855
   End
   Begin VB.CheckBox chkTransmit 
      Caption         =   "Transmit"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5610
      Style           =   1  'Graphical
      TabIndex        =   16
      Top             =   660
      Width           =   855
   End
   Begin VB.TextBox Text1 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3600
      TabIndex        =   15
      ToolTipText     =   "Future Use"
      Top             =   390
      Width           =   615
   End
   Begin VB.Timer DayOffTimer 
      Enabled         =   0   'False
      Interval        =   30000
      Left            =   4440
      Top             =   -90
   End
   Begin VB.Timer CountDownTimer 
      Interval        =   1000
      Left            =   5070
      Top             =   -90
   End
   Begin VB.TextBox CountDown 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4920
      Locked          =   -1  'True
      TabIndex        =   14
      ToolTipText     =   "Remaining time until the next transmission (hh:mm)"
      Top             =   60
      Width           =   615
   End
   Begin VB.TextBox NextTransmit 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2940
      TabIndex        =   10
      ToolTipText     =   "Date of the next transmission"
      Top             =   60
      Width           =   1275
   End
   Begin VB.TextBox SndFileExt 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1770
      TabIndex        =   9
      Text            =   "SND"
      ToolTipText     =   "Filename Extension"
      Top             =   390
      Width           =   525
   End
   Begin VB.TextBox NextTime 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4260
      TabIndex        =   8
      ToolTipText     =   "Time of the next transmission"
      Top             =   60
      Width           =   615
   End
   Begin VB.TextBox NextDay 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4260
      TabIndex        =   7
      ToolTipText     =   "Next daily flight data transmission of date ..."
      Top             =   390
      Width           =   1275
   End
   Begin VB.TextBox Text2 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1200
      TabIndex        =   6
      ToolTipText     =   "(Future use)"
      Top             =   390
      Width           =   525
   End
   Begin VB.TextBox BrsPreInfo 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2940
      TabIndex        =   5
      ToolTipText     =   "Data Offset for the next transmission (Days)"
      Top             =   390
      Width           =   615
   End
   Begin VB.TextBox maxTlxPerFile 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2340
      TabIndex        =   4
      ToolTipText     =   "Max. Telexes per File"
      Top             =   390
      Width           =   525
   End
   Begin VB.TextBox tlxDblSig 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   630
      TabIndex        =   3
      ToolTipText     =   "DBLSIG (who pays?)"
      Top             =   390
      Width           =   525
   End
   Begin VB.TextBox tlxPrio 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   60
      TabIndex        =   2
      ToolTipText     =   "Priority QU/QD"
      Top             =   390
      Width           =   525
   End
   Begin VB.TextBox tlxAddr 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   60
      TabIndex        =   1
      ToolTipText     =   "BRS Destination Type B Address"
      Top             =   60
      Width           =   1665
   End
   Begin VB.CommandButton btnClose 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5610
      TabIndex        =   0
      Top             =   1260
      Width           =   855
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Height          =   645
      Left            =   5610
      TabIndex        =   11
      Top             =   60
      Width           =   915
      Begin VB.OptionButton StopTimer 
         Caption         =   "Stop"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   300
         Width           =   855
      End
      Begin VB.OptionButton StartTimer 
         Caption         =   "Start"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   0
         Width           =   855
      End
   End
End
Attribute VB_Name = "TlxGenBrs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Sub ReorgTimers()
    InitBrsConfig
End Sub
Private Sub BrsPreInfo_Change()
    If Trim(BrsPreInfo.Text) <> "" Then SetPreInfoDate
End Sub

Private Sub BrsPreInfo_GotFocus()
    ToggleDayTimer False
End Sub

Private Sub BrsPreInfo_LostFocus()
    If Trim(BrsPreInfo.Text) = "" Then BrsPreInfo.Text = "1"
    SetPreInfoDate
End Sub

Private Sub btnClose_Click()
    Me.Hide
End Sub

Private Sub chkTransmit_Click()
    If chkTransmit.Value = 1 Then
        ToggleDayTimer False
        StartTimer.Enabled = False
        TlxGenBrs.Show
        CreateTlxGenBrses
        NextTransmit.Text = Format(CedaDateToVb(NextTransmit.Tag) + 1, MySetUp.DefDateFormat)
        StartTimer.Enabled = True
        ToggleDayTimer True
        chkTransmit.Value = 0
    End If
End Sub

Private Sub CreateTlxGenBrses()
Dim retStr As String
Dim clData As String
Dim tmpData As String
Dim clRecDat As String
Dim clVia As String
Dim count As Integer
Dim ilViaCount As Integer
Dim ilVialLen As Integer
Dim ilVialPos As Integer
Dim i As Integer
Dim clResult As String
Dim clOutText As String
Dim TlxCount As Integer
Dim tlxHeader As String
Dim tlxFields As String
Dim tlxSqlKey As String

tlxFields = "FLNO,STOD,TISD,TIFD,VIAL,DES3,FTYP,ADID"
tlxSqlKey = "WHERE TIFD BETWEEN '" & TlxGenBrs.NextDay.Tag & "000000' AND '" & TlxGenBrs.NextDay.Tag & "235959' AND ORG3='" & UfisServer.HOPO.Text & "' AND FTYP IN ('O','S','X')"
'MsgBox tlxSqlKey

    tlxHeader = ""
    If Trim(tlxPrio.Text) <> "" Then tlxHeader = tlxHeader & "=PRIORITY" & vbNewLine & Trim(tlxPrio.Text) & vbNewLine
    If Trim(tlxAddr.Text) <> "" Then tlxHeader = tlxHeader & "=DESTINATION TYPE B" & vbNewLine & Trim(tlxAddr.Text) & vbNewLine
    If Trim(tlxDblSig.Text) <> "" Then tlxHeader = tlxHeader & "=DBLSIG" & vbNewLine & Trim(tlxDblSig.Text) & vbNewLine
    tlxHeader = tlxHeader & "=TEXT" & vbNewLine
    
    clData = ""
    retStr = UfisServer.aCeda.Ufis("GFRC", "", tlxFields, tlxSqlKey, clData)
    count = UfisServer.aCeda.GetBufferCount
    clOutText = ""
    TlxCount = 0
    For i = 0 To count
        clRecDat = UfisServer.aCeda.GetBufferLine(i)
        If (Trim(clRecDat) <> "") Then
            clResult = ""
            clResult = clResult & ".AAD" & vbNewLine
            clData = GetItem(clRecDat, 1, ",") 'FLNO
            clResult = clResult & ".F/" & clData & vbNewLine
            clData = GetItem(clRecDat, 2, ",") 'STOD
            clData = UCase(Format(CedaDateToVb(clData), "ddmmm"))
            clResult = clResult & ".S/" & clData & vbNewLine
            tmpData = GetItem(clRecDat, 7, ",") 'FTYP
            If tmpData = "X" Then
                clData = tmpData
            Else
                tmpData = GetItem(clRecDat, 3, ",") 'TISD
                clData = tmpData
            End If
            clResult = clResult & ".C/" & clData & vbNewLine
            clData = GetItem(clRecDat, 4, ",") 'TIFD Date
            clData = UCase(Format(CedaDateToVb(clData), "ddmmm"))
            clResult = clResult & ".D/" & clData & vbNewLine
            clData = GetItem(clRecDat, 4, ",") 'TIFD Time
            clResult = clResult & ".T/" & Mid(clData, 9, 4) & vbNewLine
            'clData = GetItem(clRecDat, 4, ",") 'HandlingType
            clData = " "
            clResult = clResult & ".H/" & clData & vbNewLine
            tmpData = GetItem(clRecDat, 5, ",") 'VIAL
            clData = ""
            ilVialLen = Len(tmpData)
            ilVialPos = 2
            ilViaCount = 0
            While (((ilVialPos + 2) < ilVialLen) And (ilViaCount < 4))
                clVia = Mid(tmpData, ilVialPos, 3)
                If clVia <> "" Then clData = clData & clVia
                ilVialPos = ilVialPos + 120
                ilViaCount = ilViaCount + 1
            Wend
            clData = clData & GetItem(clRecDat, 6, ",") 'DES3
            clResult = clResult & ".R/" & clData & vbNewLine
            'MsgBox clRecDat & vbNewLine & clResult
            If clResult <> "" Then
                If clOutText = "" Then clOutText = tlxHeader
                clOutText = clOutText & clResult
                clResult = ""
                TlxCount = TlxCount + 1
                If TlxCount >= maxTlxPerFile Then
                    MySetUp.WriteTelexToFile (clOutText)
                    clOutText = ""
                    TlxCount = 0
                End If
            End If
        End If
    Next i
    If clOutText <> "" Then MySetUp.WriteTelexToFile (clOutText)
End Sub

Private Sub chkReset_Click()
    Me.MousePointer = 11
    ToggleDayTimer False
    InitBrsConfig
    ToggleDayTimer True
    Me.MousePointer = 0
End Sub


Private Sub CountDownTimer_Timer()
    SetCountDown
End Sub

Private Sub DayOffTimer_Timer()
Dim NowDate As String
Dim StartDate As String
    NowDate = Format(Now, "yyyymmddhhmmss")
    StartDate = NextTransmit.Tag & NextTime.Tag
    If NowDate >= StartDate Then chkTransmit.Value = 1
End Sub
Private Sub Form_Load()
    InitBrsConfig
End Sub
Private Sub InitBrsConfig()
    SetTransmitDate
    tlxAddr.Text = GetIniEntry("", "TELEXPOOL", "", "BRS_TLXADDR", "")
    tlxPrio.Text = GetIniEntry("", "TELEXPOOL", "", "BRS_PRIORITY", "")
    tlxDblSig.Text = GetIniEntry("", "TELEXPOOL", "", "BRS_DBLSIG", "")
    maxTlxPerFile.Text = GetIniEntry("", "TELEXPOOL", "", "BRS_MAX_TLX", "1")
    BrsPreInfo.Text = GetIniEntry("", "TELEXPOOL", "", "BRS_DAY_OFFSET", "7")
    NextTime.Text = GetIniEntry("", "TELEXPOOL", "", "BRS_INFO_TIME", "23:59:00")
    SetPreInfoDate
    ToggleDayTimer True
End Sub
Private Sub SetPreInfoDate()
Dim MyDate
    MyDate = CedaDateToVb(NextTransmit.Tag) + Val(BrsPreInfo.Text)
    NextDay.Text = Format(MyDate, MySetUp.DefDateFormat)
    NextDay.Tag = Format(MyDate, "yyyymmdd")
End Sub
Private Sub SetTransmitDate()
    If Format(Time, "hhmmss") >= NextTime.Tag Then
        NextTransmit.Tag = Format(DateValue(Now) + 1, "yyyymmdd")
    Else
        NextTransmit.Tag = Format(DateValue(Now), "yyyymmdd")
    End If
    NextTransmit.Text = Format(CedaDateToVb(NextTransmit.Tag), MySetUp.DefDateFormat)
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Cancel = CheckCancelExit(Me, 3)
    End If
End Sub

Private Sub NextDay_Change()
Dim MyDate As String
Dim myDiff As Integer
    MyDate = DateInputFormatToCeda(NextDay.Text, MySetUp.DefDateFormat)
    If MyDate <> "" Then
        NextDay.Tag = MyDate
        myDiff = CedaDateToVb(MyDate) - CedaDateToVb(NextTransmit.Tag)
        BrsPreInfo.Text = Str(myDiff)
    End If
End Sub

Private Sub NextDay_GotFocus()
    ToggleDayTimer False
End Sub

Private Sub NextTime_Change()
    NextTime.Tag = Format(CedaTimeToVb(NextTime.Text), "hhmmss")
End Sub

Private Sub NextTime_GotFocus()
    ToggleDayTimer False
End Sub

Private Sub ToggleDayTimer(bpVal)
    If bpVal = True Then
        StartTimer.Value = True
    Else
        StopTimer.Value = True
    End If
End Sub

Private Sub NextTransmit_Change()
Dim MyDate As String
    MyDate = DateInputFormatToCeda(NextTransmit.Text, MySetUp.DefDateFormat)
    If MyDate <> "" Then
        NextTransmit.Tag = MyDate
        SetPreInfoDate
    End If
End Sub

Private Sub NextTransmit_GotFocus()
    ToggleDayTimer False
End Sub

Private Sub StartTimer_Click()
    If StartTimer.Value = True Then
        StartTimer.BackColor = vbGreen
        StartTimer.ForeColor = vbBlack
        StartTimer.Caption = "OK"
        StopTimer.BackColor = vbButtonFace
        StopTimer.Caption = "Stop"
        DayOffTimer.Enabled = True
    End If
End Sub

Private Sub StopTimer_Click()
    If StopTimer.Value = True Then
        StopTimer.BackColor = vbRed
        StopTimer.Caption = "Halted"
        StartTimer.BackColor = vbRed
        StartTimer.ForeColor = vbWhite
        StartTimer.Caption = "Start"
        DayOffTimer.Enabled = False
    End If
End Sub

Private Sub SetCountDown()
Dim StartDate As String
Dim MinDiff
    StartDate = NextTransmit.Tag & NextTime.Tag
    MinDiff = CedaFullDateToVb(StartDate) - Now
    If MinDiff > 1 Then
        CountDown.Text = "Days"
    ElseIf MinDiff < 0 Then
        CountDown.Text = "gone"
    Else
        CountDown.Text = Format(MinDiff, "hh:mm")
    End If
End Sub

