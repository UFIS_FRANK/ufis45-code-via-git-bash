VERSION 5.00
Begin VB.Form HiddenMain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Telex Pool Status"
   ClientHeight    =   7545
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7170
   Icon            =   "HiddenMain.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7545
   ScaleWidth      =   7170
   Begin VB.Timer Timer2 
      Enabled         =   0   'False
      Interval        =   50
      Left            =   3450
      Top             =   0
   End
   Begin VB.Timer Timer1 
      Left            =   3000
      Top             =   0
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   315
      Left            =   0
      TabIndex        =   0
      Top             =   4980
      Width           =   7605
      Begin VB.TextBox CedaStatus 
         Alignment       =   2  'Center
         BackColor       =   &H00008000&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   1
         Left            =   0
         Locked          =   -1  'True
         TabIndex        =   1
         Text            =   "CONNECTING"
         Top             =   60
         Width           =   7160
      End
   End
   Begin VB.Frame Frame2 
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   345
      Left            =   0
      TabIndex        =   2
      Top             =   5250
      Width           =   7605
      Begin VB.TextBox CedaStatus 
         Alignment       =   2  'Center
         BackColor       =   &H0000C0C0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Index           =   2
         Left            =   0
         Locked          =   -1  'True
         TabIndex        =   3
         Text            =   "CONNECTING"
         Top             =   60
         Width           =   7160
      End
   End
   Begin VB.Image Image5 
      BorderStyle     =   1  'Fixed Single
      Height          =   6375
      Left            =   420
      Top             =   435
      Visible         =   0   'False
      Width           =   6375
   End
   Begin VB.Image Image2 
      BorderStyle     =   1  'Fixed Single
      Height          =   7155
      Left            =   0
      MouseIcon       =   "HiddenMain.frx":030A
      MousePointer    =   99  'Custom
      Picture         =   "HiddenMain.frx":045C
      Top             =   30
      Width           =   7155
   End
   Begin VB.Image Image1 
      BorderStyle     =   1  'Fixed Single
      Height          =   960
      Left            =   2295
      Picture         =   "HiddenMain.frx":2AEE9
      Top             =   30
      Visible         =   0   'False
      Width           =   4860
   End
   Begin VB.Image Image4 
      BorderStyle     =   1  'Fixed Single
      Height          =   960
      Left            =   0
      Picture         =   "HiddenMain.frx":2D120
      Stretch         =   -1  'True
      Top             =   30
      Visible         =   0   'False
      Width           =   2250
   End
   Begin VB.Image Image3 
      Height          =   855
      Left            =   30
      Picture         =   "HiddenMain.frx":309DA
      Stretch         =   -1  'True
      Top             =   1050
      Visible         =   0   'False
      Width           =   885
   End
End
Attribute VB_Name = "HiddenMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim CurCaller As Form
Public Sub SetShutDownTimer(MyCaller As Form)
    Set CurCaller = MyCaller
    Timer2.Enabled = True
End Sub
Public Sub ShowHiddenMain(OnTop As Boolean)
    Top = MaxScreenHeight / 2 - Height / 2
    Left = Top
    Me.Show
    PutFormOnTop Me, False, True, True
    DoEvents
End Sub
Public Sub HideHiddenMain()
    'Top = Screen.Height + 60
    Me.Hide
    Me.Refresh
    DoEvents
End Sub
Public Sub SetStatusText(Index As Integer, SetText As String)
    If Me.Visible Then
        CedaStatus(Index).Text = SetText
        PutFormOnTop Me, False, True, True
        DoEvents
    End If
End Sub
Private Sub Form_Load()
    ApplIsInDesignMode = False
    Debug.Assert CheckIDE
    Top = MaxScreenHeight / 2 - Height / 2
    Left = Top
End Sub

Private Sub Image2_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Me.Move Me.Left, -Me.Height - 1000
End Sub
Private Function CheckIDE() As Boolean
    'This function will never be executed in an EXE
    ApplIsInDesignMode = True  'set global flag
    'Set CheckIDE or the Debug.Assert will Break
    CheckIDE = True
End Function

Private Sub Timer1_Timer()
    'Future Animation
End Sub

Private Sub Timer2_Timer()
    Dim CurName As String
    On Error Resume Next
    Timer2.Enabled = False
    ShutDownApplication True
    If Not ShutDownRequested Then
        'if we come back ....
        CurName = CurCaller.Name
        Select Case CurName
            Case "TelexPoolHead"
                CurCaller.chkExit.Value = 0
                CurCaller.chkClose.Value = 0
            Case "CreateTelex"
                CurCaller.chkExit.Value = 0
                CurCaller.chkClose.Value = 0
            Case Else
        End Select
    End If
End Sub
