VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form OutTelex 
   Caption         =   "Sent Telexes"
   ClientHeight    =   2370
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4380
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "OutTelex.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   2370
   ScaleWidth      =   4380
   StartUpPosition =   3  'Windows Default
   Tag             =   "Sent Telexes"
   Begin VB.PictureBox ListPanel 
      Height          =   1245
      Left            =   0
      ScaleHeight     =   1185
      ScaleWidth      =   3375
      TabIndex        =   4
      Top             =   300
      Width           =   3435
      Begin TABLib.TAB tabTelexList 
         Height          =   1170
         Left            =   0
         TabIndex        =   5
         Top             =   0
         Width           =   3030
         _Version        =   65536
         _ExtentX        =   5345
         _ExtentY        =   2064
         _StockProps     =   64
         Columns         =   9
         Lines           =   10
      End
   End
   Begin VB.CheckBox chkLock 
      Caption         =   "Lock"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1740
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox OpenPool 
      Caption         =   "Main"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox chkLoad 
      Caption         =   "Refresh"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   870
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   0
      Width           =   855
   End
   Begin VB.CheckBox OnTop 
      Caption         =   "On Top"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2610
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   0
      Width           =   855
   End
End
Attribute VB_Name = "OutTelex"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim PrevWindowState As Integer
Private m_Snap As CSnapDialog

Private Sub chkLock_Click()
    If chkLock.Value = 1 Then
        tabTelexList.LockScroll True
    Else
        tabTelexList.LockScroll False
    End If
End Sub

Private Sub Form_Activate()
    Static AlreadyDone As Boolean
    If Not AlreadyDone Then
        AlreadyDone = True
'        If UseSnapFeature Then
'            If Not ApplIsInDesignMode Then
'                Set m_Snap = New CSnapDialog
'                m_Snap.hWnd = Me.hWnd
'            End If
'        End If
        tabTelexList.Refresh
        Me.Refresh
        DoEvents
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    CheckKeyboard Shift
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    CheckKeyboard Shift
End Sub

Private Sub Form_Load()
    If UseSnapFeature Then
        If Not ApplIsInDesignMode Then
            Set m_Snap = New CSnapDialog
            m_Snap.hwnd = Me.hwnd
        End If
    End If
    InitForm 6
    InitPosSize
    If Not AutoRefreshOnline Then
        chkLoad.BackColor = DarkGreen
        chkLoad.ForeColor = vbWhite
    End If
End Sub

Private Sub InitPosSize()
    Dim MainTop As Long
    Dim MainHight As Long
    Dim NewHeight As Long
    Dim NewTop As Long
    Dim MyPlace As Long
    MainHight = MaxScreenHeight
    MainTop = (Screen.Height - MainHight) / 2
    MyPlace = GetMyStartupWindowPlace(Me.Name)
    Me.Width = 3600
    If OnlineWindows > 0 Then
        NewHeight = MainHight / OnlineWindows
        NewTop = MainTop + (NewHeight * (MyPlace - 1))
        Me.Top = NewTop
        Me.Left = MaxScreenWidth - Me.Width
        Me.Height = NewHeight
        If Not MainIsOnline Then Me.Show
    Else
        Me.Top = 3675
        Me.Left = MaxScreenWidth - Me.Width
        Me.Height = 6000
    End If
End Sub

Private Sub InitForm(ipLines As Integer)
    tabTelexList.ResetContent
    tabTelexList.FontName = "Courier New"
    tabTelexList.HeaderFontSize = MyFontSize
    tabTelexList.FontSize = MyFontSize
    tabTelexList.lineHeight = MyFontSize
    tabTelexList.SetTabFontBold MyFontBold
    tabTelexList.Top = 0
    tabTelexList.Left = 0
    tabTelexList.Width = ListPanel.ScaleWidth
    tabTelexList.Height = ipLines * tabTelexList.lineHeight * Screen.TwipsPerPixelY
    ListPanel.Height = tabTelexList.Height + 60
    tabTelexList.HeaderString = "S,W,Type,ST,Time,No,Text Extract,URNO,Index,FLNU," & Space(500)
    tabTelexList.HeaderLengthString = "12,12,32,22,40,31,819,50,50,50,1000"
    tabTelexList.SetUniqueFields "7"
    tabTelexList.LifeStyle = True
    tabTelexList.ShowHorzScroller True
    tabTelexList.DateTimeSetColumn 4
    tabTelexList.DateTimeSetInputFormatString 4, "YYYYMMDDhhmmss"
    tabTelexList.DateTimeSetOutputFormatString 4, "hh':'mm"
    tabTelexList.AutoSizeByHeader = True
    tabTelexList.AutoSizeColumns
    Me.Height = ListPanel.Top + ListPanel.Height + (Me.Height - Me.ScaleHeight)
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If Not ShutDownRequested Then
        If UnloadMode = 0 Then
            Cancel = CheckCancelExit(Me, 1)
        End If
    End If
End Sub

Private Sub Form_Resize()
    If (PrevWindowState = vbNormal) And (Me.WindowState = vbNormal) And (Not SystemResizing) Then LastResizeName = Me.Name
    If Me.WindowState <> vbMinimized Then
        ListPanel.Width = Me.ScaleWidth - 2 * ListPanel.Left
        tabTelexList.Width = ListPanel.ScaleWidth
        If Me.ScaleHeight > 500 Then
            ListPanel.Height = Me.ScaleHeight - ListPanel.Top - ListPanel.Left
            tabTelexList.Height = ListPanel.ScaleHeight
        End If
        SyncWindowSize Me.Left, Me.Width
    End If
End Sub

Private Sub chkLoad_Click()
    If chkLoad.Value = 1 Then
        chkLoad.BackColor = LightGreen
        chkLoad.ForeColor = vbBlack
        chkLoad.Refresh
        TelexPoolHead.RefreshOnlineWindows "OUT"
        chkLoad.Value = 0
    Else
        chkLoad.BackColor = vbButtonFace
    End If
End Sub

Private Sub OnTop_Click()
    If OnTop.Value = 1 Then OnTop.BackColor = LightGreen Else OnTop.BackColor = vbButtonFace
    If OnTop.Value <> 1 Then SetFormOnTop Me, False, False
    SetAllFormsOnTop True
End Sub

Private Sub OpenPool_Click()
    If OpenPool.Value = 1 Then
        SwitchMainWindows "MAIN"
        OpenPool.Value = 0
    End If
End Sub

Private Sub tabTelexList_SendLButtonClick(ByVal Line As Long, ByVal Col As Long)
Dim tmpUrno As String
Dim tmpWsta As String
Dim clSqlKey As String
Dim RetCode As Integer
Dim RetVal As String
    tmpWsta = Trim(tabTelexList.GetColumnValue(Line, 1))
    tmpUrno = Trim(tabTelexList.GetColumnValue(Line, 7))
    If tmpUrno <> "" Then
        SwitchMainWindows "MAIN"
        clSqlKey = "WHERE URNO=" & tmpUrno
        If (UpdateWstaOnClick = True) And (tmpWsta = "") Then
            RetCode = UfisServer.CallCeda(RetVal, "URT", DataPool.TableName, "WSTA", "V", clSqlKey, "", 0, True, False)
            tabTelexList.SetColumnValue Line, 1, "V"
        End If
        TelexPoolHead.LoadTelexData 3, "", clSqlKey, "", "ALL", "SND"
    End If
End Sub

Private Sub tabTelexList_SendMouseMove(ByVal LineNo As Long, ByVal ColNo As Long, ByVal Flags As String)
    Dim tmpCode As String
    tmpCode = "OUT.TABTLX.X"
    If MouseOverObjCode <> tmpCode Then Set MouseOverTabOcx = Me.tabTelexList
    SetMouseWheelObjectCode tmpCode
End Sub

Public Function GetMvtCorCount(AftUrno As String, TlxMainTypes As String, TlxSubType As String) As Integer
    Dim TlxFlnu As String
    Dim CurMainType As String
    Dim CurSubType As String
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim TlxCnt As Integer
    TlxCnt = 0
    MaxLine = tabTelexList.GetLineCount - 1
    For CurLine = 0 To MaxLine
        TlxFlnu = tabTelexList.GetColumnValue(CurLine, 9)
        If TlxFlnu = AftUrno Then
            CurSubType = tabTelexList.GetColumnValue(CurLine, 3)
            If CurSubType = TlxSubType Then
                CurMainType = tabTelexList.GetColumnValue(CurLine, 2)
                If (InStr(TlxMainTypes, CurMainType) > 0) Then
                    TlxCnt = TlxCnt + 1
                End If
            End If
        End If
    Next
    GetMvtCorCount = TlxCnt
End Function
