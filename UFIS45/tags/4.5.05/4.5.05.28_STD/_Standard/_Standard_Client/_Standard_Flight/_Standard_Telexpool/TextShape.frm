VERSION 5.00
Begin VB.Form TextShape 
   AutoRedraw      =   -1  'True
   ClientHeight    =   1170
   ClientLeft      =   60
   ClientTop       =   60
   ClientWidth     =   12510
   ControlBox      =   0   'False
   FillColor       =   &H0080FFFF&
   FillStyle       =   0  'Solid
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   36
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H0080FF80&
   Icon            =   "TextShape.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   Picture         =   "TextShape.frx":000C
   ScaleHeight     =   1170
   ScaleWidth      =   12510
   ShowInTaskbar   =   0   'False
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   780
      Top             =   360
   End
   Begin VB.Image Image1 
      Height          =   765
      Left            =   0
      MouseIcon       =   "TextShape.frx":1F62
      MousePointer    =   99  'Custom
      Picture         =   "TextShape.frx":20B4
      ToolTipText     =   "Open the Application Window"
      Top             =   0
      Width           =   780
   End
End
Attribute VB_Name = "TextShape"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const HTCAPTION = 2
Const WM_NCLBUTTONDOWN = &HA1

Private Declare Function ReleaseCapture Lib "user32" () As Long
Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lparam As Long) As Long

Private Declare Function BeginPath Lib "gdi32" (ByVal hDC As Long) As Long
Private Declare Function EndPath Lib "gdi32" (ByVal hDC As Long) As Long
Private Declare Function PathToRegion Lib "gdi32" (ByVal hDC As Long) As Long
Private Declare Function SetWindowRgn Lib "user32" (ByVal hwnd As Long, ByVal hRgn As Long, ByVal bRedraw As Boolean) As Long
Private Declare Function CreateEllipticRgn Lib "gdi32" (ByVal x1 As Long, ByVal y1 As Long, ByVal X2 As Long, ByVal Y2 As Long) As Long
Private Declare Function CombineRgn Lib "gdi32" (ByVal hDestRgn As Long, ByVal hSrcRgn1 As Long, ByVal hSrcRgn2 As Long, ByVal nCombineMode As Long) As Long

Private Declare Function CreateFont Lib "gdi32" Alias "CreateFontA" (ByVal h As Long, ByVal W As Long, ByVal e As Long, ByVal O As Long, ByVal W As Long, ByVal i As Long, ByVal u As Long, ByVal s As Long, ByVal c As Long, ByVal OP As Long, ByVal CP As Long, ByVal Q As Long, ByVal PAF As Long, ByVal f As String) As Long
Private Declare Function SelectObject Lib "gdi32" (ByVal hDC As Long, ByVal hObject As Long) As Long
Private Declare Function DeleteObject Lib "gdi32" (ByVal hObject As Long) As Long

Private Const FW_BOLD = 700
Dim NotifyText As String
Dim TxtColor1 As Long
Dim TxtColor2 As Long

Public Sub SetNotificationText(UseText As String, UseColor1 As Long, UseColor2 As Long)
    NotifyText = UseText
    TxtColor1 = UseColor1
    TxtColor2 = UseColor2
End Sub

Private Sub ShapePicture()
    Const RGN_OR = 2
    Dim Text1 As String
    If NotifyText = "" Then NotifyText = "NEW AUTO MVT CREATED"
    Text1 = NotifyText
    'Exit Sub
    Dim new_font As Long
    Dim old_font As Long
    Dim hRgn As Long
    Dim new_rgn As Long

    ' Prepare the form.
    AutoRedraw = True
    BorderStyle = vbBSNone
    ScaleMode = vbPixels
    Me.BackColor = TxtColor1
    
    ' Make a big font.
    new_font = CustomFont(36, 20, 0, 0, FW_BOLD, False, True, False, "Arial")
    Me.Font = new_font
    old_font = SelectObject(Me.hDC, new_font)
    ' Make the region.
    SelectObject Me.hDC, new_font
    Me.ForeColor = vbBlack
    BeginPath Me.hDC
    Me.CurrentX = 60
    Me.CurrentY = 6
    Me.Print Text1
    EndPath Me.hDC
    hRgn = PathToRegion(Me.hDC)
    
    Me.Width = (60 * 15) + (TextWidth(Text1) * 15) + ((Me.Width / 15 - Me.ScaleWidth) * 15)
    Me.Left = Screen.Width - Me.Width
    
    new_rgn = CreateEllipticRgn(6, 4, 56, 54)
    CombineRgn hRgn, hRgn, new_rgn, RGN_OR
    DeleteObject new_rgn
    
    ' Constrain the form to the region.
    SetWindowRgn Me.hwnd, hRgn, False

    ' Restore the original font.
    SelectObject hDC, old_font

    ' Free font resources (important!)
    DeleteObject new_font

End Sub
' Make a customized font and return its handle.
Private Function CustomFont(ByVal hgt As Long, ByVal wid As Long, ByVal escapement As Long, ByVal orientation As Long, ByVal wgt As Long, ByVal is_italic As Long, ByVal is_underscored As Long, ByVal is_striken_out As Long, ByVal face As String) As Long
    Const CLIP_LH_ANGLES = 16   ' Needed for tilted fonts.
    CustomFont = CreateFont(hgt, wid, escapement, orientation, wgt, is_italic, is_underscored, is_striken_out, 0, 0, CLIP_LH_ANGLES, 0, 0, face)
End Function


Private Sub Form_Load()
    TxtColor2 = vbBlack
    TxtColor1 = vbRed
    ' Shape the picture.
    ShapePicture
    Me.Top = Screen.Height - Me.Height - 210
    Me.Height = Image1.Height + (Me.Height - Me.ScaleHeight)
    Timer1.Tag = "SHOW"
    Timer1.Enabled = True
End Sub

Private Sub Form_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    'MainDialog.Show
    'MainDialog.WindowState = vbNormal
End Sub

Private Sub Image1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim ReturnVal As Long
    'If Button = 1 Then
    '    x = ReleaseCapture()
    '    ReturnVal = SendMessage(Me.hwnd, WM_NCLBUTTONDOWN, HTCAPTION, 0)
    'End If
    SwitchMainWindows "CREATE"
End Sub

Private Sub Timer1_Timer()
    Static ticCnt As Integer
    Dim tmpTag As String
    tmpTag = Timer1.Tag
    Select Case tmpTag
        Case "SHOW"
            ticCnt = 0
            Me.Show
            Timer1.Tag = "TOP"
        Case "TOP"
            PutFormOnTop Me, True, True, False
            ticCnt = ticCnt + 1
            If ticCnt > 10 Then
                If Me.BackColor = TxtColor1 Then
                    Me.BackColor = TxtColor2
                Else
                    Me.BackColor = TxtColor1
                End If
                If ticCnt > 20 Then ticCnt = 0
            Else
                Me.BackColor = TxtColor1
            End If
        Case Else
    End Select
End Sub
