VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form MyMsgBox 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Message"
   ClientHeight    =   1080
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6120
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   161
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "MyMsgBox.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1080
   ScaleWidth      =   6120
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin TABLib.TAB TAB1 
      Height          =   915
      Left            =   2820
      TabIndex        =   10
      Top             =   90
      Width           =   2805
      _Version        =   65536
      _ExtentX        =   4948
      _ExtentY        =   1614
      _StockProps     =   64
   End
   Begin VB.Timer CountDown 
      Enabled         =   0   'False
      Interval        =   10000
      Left            =   60
      Top             =   600
   End
   Begin VB.Frame ButtonFrame 
      BorderStyle     =   0  'None
      Height          =   345
      Left            =   660
      TabIndex        =   4
      Top             =   570
      Width           =   5175
      Begin VB.CheckBox btnList 
         Height          =   285
         Index           =   4
         Left            =   3480
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   30
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CheckBox btnList 
         Height          =   285
         Index           =   3
         Left            =   2610
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   30
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CheckBox btnList 
         Height          =   285
         Index           =   2
         Left            =   1740
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   30
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CheckBox btnList 
         Height          =   285
         Index           =   1
         Left            =   870
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   30
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CheckBox btnList 
         Height          =   285
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   30
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CommandButton CxxButton 
         Cancel          =   -1  'True
         Caption         =   "OK"
         Default         =   -1  'True
         Height          =   285
         Left            =   0
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   30
         Width           =   855
      End
   End
   Begin MSComctlLib.ImageList MyIcons 
      Left            =   2070
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   47
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":014A
            Key             =   "airbus"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":0464
            Key             =   "ask"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":077E
            Key             =   "bulb"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":0A98
            Key             =   "hand"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":0DB2
            Key             =   "mobile"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":10CC
            Key             =   "netfail"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":13E6
            Key             =   "note"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":1700
            Key             =   "phone"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":1A1A
            Key             =   "runway"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":1D34
            Key             =   "stop"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":204E
            Key             =   "timer"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":2368
            Key             =   "ufis32"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":2682
            Key             =   "ufis44"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":299C
            Key             =   "write"
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":2CB6
            Key             =   "export"
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":3108
            Key             =   "postit"
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":3422
            Key             =   "ask2"
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":373C
            Key             =   "days"
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":3A56
            Key             =   "tools"
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":3D70
            Key             =   "edit"
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":3ECA
            Key             =   "edit2"
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":41E4
            Key             =   "keys"
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":4636
            Key             =   "zoom"
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":4950
            Key             =   "mail"
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":4C6A
            Key             =   "props"
         EndProperty
         BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":4DC4
            Key             =   "aid"
         EndProperty
         BeginProperty ListImage27 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":50DE
            Key             =   "setup"
         EndProperty
         BeginProperty ListImage28 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":5530
            Key             =   "stop2"
         EndProperty
         BeginProperty ListImage29 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":584A
            Key             =   "calc"
         EndProperty
         BeginProperty ListImage30 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":5B64
            Key             =   "clock1"
         EndProperty
         BeginProperty ListImage31 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":5E7E
            Key             =   "clock2"
         EndProperty
         BeginProperty ListImage32 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":6198
            Key             =   "warn1"
         EndProperty
         BeginProperty ListImage33 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":64B2
            Key             =   "warn2"
         EndProperty
         BeginProperty ListImage34 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":67CC
            Key             =   "snap1"
         EndProperty
         BeginProperty ListImage35 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":6926
            Key             =   "snap2"
         EndProperty
         BeginProperty ListImage36 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":6C40
            Key             =   "work"
         EndProperty
         BeginProperty ListImage37 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":6F5A
            Key             =   "check"
         EndProperty
         BeginProperty ListImage38 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":7274
            Key             =   "green"
         EndProperty
         BeginProperty ListImage39 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":758E
            Key             =   "screen"
         EndProperty
         BeginProperty ListImage40 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":78A8
            Key             =   "oops1"
         EndProperty
         BeginProperty ListImage41 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":7BC2
            Key             =   "oops2"
         EndProperty
         BeginProperty ListImage42 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":7EDC
            Key             =   "oops3"
         EndProperty
         BeginProperty ListImage43 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":81F6
            Key             =   "lock"
         EndProperty
         BeginProperty ListImage44 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":8510
            Key             =   "grobi"
         EndProperty
         BeginProperty ListImage45 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":A062
            Key             =   "infomsg"
         EndProperty
         BeginProperty ListImage46 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":A14E
            Key             =   "query"
         EndProperty
         BeginProperty ListImage47 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MyMsgBox.frx":A273
            Key             =   "stopit"
         EndProperty
      EndProperty
   End
   Begin VB.Frame BackFrame 
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   405
      Left            =   660
      TabIndex        =   0
      Top             =   90
      Width           =   1275
      Begin VB.TextBox MsgField 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         Height          =   285
         Left            =   0
         MultiLine       =   -1  'True
         TabIndex        =   2
         TabStop         =   0   'False
         Text            =   "MyMsgBox.frx":A392
         Top             =   0
         Width           =   705
      End
   End
   Begin VB.PictureBox IconBox 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   60
      Picture         =   "MyMsgBox.frx":A398
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   60
      Width           =   480
   End
End
Attribute VB_Name = "MyMsgBox"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private FormIsUp As Boolean
Private AskRetVal As Integer
Private FocusButton As Integer
Private GlobalMessage As String
Private SetMeOnTop As Boolean
Private ActCursor As Integer

Public Sub InfoApi(ApiType As Integer, ApiValue As String, TipValue As String)
    Load Me
    Select Case ApiType
        Case 0
            GlobalMessage = ApiValue
            IconBox.ToolTipText = TipValue
        Case 1
            SetMeOnTop = True
        Case Else
    End Select
End Sub

Public Function AskUser(MsgType As Integer, LeftPosMid As Long, TopPosMid As Long, _
                MsgTitle As String, MsgText As String, _
                IconKey As String, ButtonList As String, UserAnswer As String) As Integer
    Static TaskIsBusy As Boolean
    Dim lastCapPos As Long
    Dim lastTxtPos As Long
    Dim lastBtnPos As Long
    Dim PosX As Long
    Dim PosY As Long
    Dim NewHeight As Long
    Dim tmpText As String
    Dim tmpList As String
    Dim tmpHead As String
    Dim TabSize As String
    Dim TabWidth As Long
    Dim TabHeight As Long
    Dim MsgBoxHeight As Long
    Dim MsgBoxWidth As Long
    Dim LineNo As Long
    If TaskIsBusy = True Then
        AskUser = -1
        'Exit Function
    End If
    If Me.Visible = True Then Exit Function

    TaskIsBusy = True
    FocusButton = -1
    PosX = LeftPosMid
    PosY = TopPosMid
    With Screen.ActiveForm
        If PosX = 0 Then PosX = .Left + .Width / 2
        If PosY = 0 Then PosY = .Top + .Height / 2
    End With
    If MsgTitle <> "" Then Caption = MsgTitle Else Caption = "Message"
    lastCapPos = TextWidth(MsgTitle) + 50 * Screen.TwipsPerPixelX
    IconBox.Picture = MyIcons.ListImages(GetIconIndex(IconKey)).Picture
    Select Case MsgType
        Case 0
            tmpText = MsgText
            TAB1.Visible = False
        Case 1
            GetKeyItem tmpText, MsgText, "{=LABEL=}", "{="
            GetKeyItem tmpList, MsgText, "{=DATA=}", "{="
            GetKeyItem tmpHead, MsgText, "{=HEAD=}", "{="
            GetKeyItem TabSize, MsgText, "{=SIZE=}", "{="
            TabWidth = Val(GetItem(TabSize, 1, ","))
            TabHeight = Val(GetItem(TabSize, 2, ","))
            If TabWidth < 2805 Then TabWidth = 2805
            If TabHeight < 915 Then TabHeight = 915
            TAB1.Visible = True
            TAB1.ResetContent
            TAB1.HeaderString = tmpHead
            TAB1.HeaderLengthString = "100,100,100,100"
            TAB1.EmptyAreaRightColor = vbWhite
            TAB1.InsertBuffer tmpList, vbLf
            TAB1.AutoSizeByHeader = True
            TAB1.ShowVertScroller True
            TAB1.AutoSizeColumns
            TAB1.Width = TabWidth
            TAB1.Height = TabHeight
            TAB1.SetCurrentSelection 0
        Case Else
            tmpText = MsgText
    End Select
    MsgField.Height = TextHeight(tmpText)
    MsgField.Width = TextWidth((tmpText & "W"))
    MsgField.Text = tmpText
    BackFrame.Left = IconBox.Left + IconBox.Width + 8 * Screen.TwipsPerPixelX
    BackFrame.Width = MsgField.Width
    BackFrame.Height = MsgField.Height
    ButtonFrame.Left = BackFrame.Left
    ButtonFrame.Top = BackFrame.Top + BackFrame.Height + 6 * Screen.TwipsPerPixelY
    TAB1.Left = ButtonFrame.Left
    TAB1.Top = ButtonFrame.Top
    If MsgType = 1 Then ButtonFrame.Top = ButtonFrame.Top + TAB1.Height + 60
    Me.Height = ButtonFrame.Top + ButtonFrame.Height + 30 * Screen.TwipsPerPixelY
    NewHeight = Me.ScaleHeight - (IconBox.Height + 7 * Screen.TwipsPerPixelY)
    If NewHeight < 0 Then
        Me.Height = Me.Height - NewHeight
        ButtonFrame.Top = Me.ScaleHeight - ButtonFrame.Height - 1 * Screen.TwipsPerPixelY
        If MsgType = 1 Then ButtonFrame.Top = ButtonFrame.Top + TAB1.Height + 60
    End If
    NewHeight = Me.ScaleHeight - (ButtonFrame.Top + ButtonFrame.Height)
    If NewHeight < 60 Then
        Me.Height = Me.Height - NewHeight + 60
    End If
    lastTxtPos = BackFrame.Left + BackFrame.Width + 12 * Screen.TwipsPerPixelX
    If lastTxtPos < lastCapPos Then lastTxtPos = lastCapPos
    lastBtnPos = DefineButtons(ButtonList)
    If lastTxtPos > lastBtnPos Then
        MsgBoxWidth = lastTxtPos
    Else
        MsgBoxWidth = lastBtnPos
    End If
    If MsgType = 1 Then
        TabWidth = TAB1.Left + TAB1.Width + 450
        If TabWidth > MsgBoxWidth Then MsgBoxWidth = TabWidth
    End If
    Me.Width = MsgBoxWidth
    Left = PosX - (Width / 2)
    Top = PosY - (Height / 2)
    If (Left + Width) > Screen.Width Then Left = Screen.Width - Width - 30 * Screen.TwipsPerPixelX
    If Left < (30 * Screen.TwipsPerPixelX) Then Left = 30 * Screen.TwipsPerPixelX
    If (Top + Height) > Screen.Height Then Top = Screen.Height - Height - 30 * Screen.TwipsPerPixelY
    If Top < (30 * Screen.TwipsPerPixelY) Then Top = 30 * Screen.TwipsPerPixelY
    'CountDown.Enabled = True
    ModalMsgIsOpen = True
    Me.MousePointer = 0
    Me.Show vbModal
    Select Case AskRetVal
        Case -2
            UserAnswer = "Close"
        Case -1
            UserAnswer = "Cancel"
        Case 0
            UserAnswer = "Cancel/OK"
        Case Else
            UserAnswer = btnList(AskRetVal - 1).Caption
    End Select
    If MsgType = 1 Then
        LineNo = TAB1.GetCurrentSelected
        UserAnswer = TAB1.GetColumnValue(LineNo, 0)
    End If
    TaskIsBusy = False
    AskUser = AskRetVal
    Unload Me
End Function
Private Function DefineButtons(KeyList As String) As Long
Dim lastBtnPos As Long
Dim i As Integer
Dim BtnKey As String
    lastBtnPos = ButtonFrame.Left
    BtnKey = "START"
    i = 0
    While (BtnKey <> "") And (i < 5)
        BtnKey = GetItem(KeyList, i + 1, ",")
        If BtnKey <> "" Then
            btnList(i).Caption = "&" & GetItem(BtnKey, 1, ";")
            btnList(i).Visible = True
            If GetItem(BtnKey, 2, ";") = "F" Then FocusButton = i
            lastBtnPos = lastBtnPos + btnList(i).Width
        End If
        i = i + 1
    Wend
    If lastBtnPos = ButtonFrame.Left Then
        lastBtnPos = lastBtnPos + CxxButton.Width
    End If
    DefineButtons = lastBtnPos + 12 * Screen.TwipsPerPixelY
End Function

Private Function GetIconIndex(IconKey As String) As Integer
    On Error GoTo ErrorHandler
    GetIconIndex = MyIcons.ListImages(IconKey).Index
    Exit Function
ErrorHandler:
    GetIconIndex = 4
    Resume Next
End Function

Private Sub btnList_Click(Index As Integer)
    AskRetVal = Index + 1
    If SetMeOnTop = True Then SetFormOnTop Me, False, False
    Me.Hide
End Sub

Private Sub btnList_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = Asc(vbCr) Then btnList_Click Index
End Sub

Private Sub CountDown_Timer()
    Select Case CountDown.Tag
        Case ""
            IconBox.Picture = MyIcons.ListImages(GetIconIndex("timer")).Picture
            CountDown.Tag = "COUNT"
        Case "COUNT"
            CountDown.Tag = "CANCEL"
        Case "CANCEL"
            CxxButton_Click
        Case Else
            CxxButton_Click
    End Select
End Sub

Private Sub CxxButton_Click()
    If btnList(0).Visible = True Then AskRetVal = -1 Else AskRetVal = 0
    If SetMeOnTop = True Then SetFormOnTop Me, False, False
    Me.Hide
End Sub

Private Sub Form_Activate()
    On Error Resume Next
    If Not FormIsUp Then
        If FocusButton >= 0 Then btnList(FocusButton).SetFocus
        If SetMeOnTop Then SetFormOnTop Me, True, True
        ActCursor = Screen.MousePointer
        Screen.MousePointer = 0
        SetFormOnTop Me, True, True
        FormIsUp = True
    End If
End Sub

Private Sub Form_Load()
    'ModalMsgIsOpen = True
    FormIsUp = False
    GlobalMessage = "No further information available."
    SetMeOnTop = False
    AskRetVal = -2
End Sub

Private Sub Form_Unload(Cancel As Integer)
    ModalMsgIsOpen = False
    Screen.MousePointer = ActCursor
End Sub

Private Sub IconBox_Click()
Dim tmpTitle As String
    On Error Resume Next
    If InStr(GlobalMessage, App.EXEName) = 0 Then tmpTitle = App.EXEName & ": " Else tmpTitle = ""
    tmpTitle = tmpTitle & Me.Caption & " (More Info)"
    MsgBox GlobalMessage, , tmpTitle
    If FocusButton >= 0 Then btnList(FocusButton).SetFocus
End Sub

Public Function CallAskUser(MsgType As Integer, PosX As Long, PosY As Long, _
                MsgTitle As String, MsgText As String, _
                IconKey As String, ButtonList As String, UserAnswer As String) As Integer
    
    'SetAllFormsOnTop False 'must be defined in StartModule
    CallAskUser = AskUser(MsgType, PosX, PosY, MsgTitle, MsgText, IconKey, ButtonList, UserAnswer)
    'SetAllFormsOnTop True
End Function

Public Sub UnderConstruction(tmpText As String)
    Dim MsgText As String
    MsgText = "Still under construction." & vbNewLine
    If tmpText <> "" Then MsgText = MsgText & "Ready until " & tmpText
    If CallAskUser(0, 0, 0, "Version Control", MsgText, "hand", "", UserAnswer) <> 0 Then DoNothing
End Sub

Public Function CheckPendingEvent(TaskCode As Integer) As Integer
    On Error Resume Next
    CheckPendingEvent = RC_NONE
    If PendingEvent.IsActive Then
        CheckPendingEvent = RC_PENDING
        If TaskCode = RELEASE Then
            Select Case PendingEvent.Type
                Case 1
                    If CallAskUser(0, 0, 0, "Format Error", "Wrong Date Format", "stop", "Edit,Ignore", UserAnswer) = 1 Then
                        PendingEvent.Object.SetFocus
                        CheckPendingEvent = RC_NEWFOCUS
                    Else
                        CheckPendingEvent = RC_IGNORED
                    End If
                Case Else
            End Select
            PendingEvent.IsActive = False
        End If
    End If
End Function


Private Sub TAB1_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo >= 0 Then Me.Hide
End Sub
