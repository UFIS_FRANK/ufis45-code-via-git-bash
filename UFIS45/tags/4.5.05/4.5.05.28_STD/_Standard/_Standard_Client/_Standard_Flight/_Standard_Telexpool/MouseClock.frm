VERSION 5.00
Begin VB.Form MouseClock 
   AutoRedraw      =   -1  'True
   ClientHeight    =   5895
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7335
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5895
   ScaleWidth      =   7335
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Index           =   5
      Interval        =   40
      Left            =   2250
      Top             =   0
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Index           =   4
      Interval        =   40
      Left            =   1800
      Top             =   0
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Index           =   3
      Interval        =   40
      Left            =   1350
      Top             =   0
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Index           =   2
      Interval        =   40
      Left            =   900
      Top             =   0
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Index           =   1
      Interval        =   40
      Left            =   450
      Top             =   0
   End
   Begin VB.PictureBox Picture1 
      Height          =   5055
      Left            =   240
      ScaleHeight     =   4995
      ScaleWidth      =   6735
      TabIndex        =   0
      Top             =   480
      Width           =   6795
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Index           =   0
      Interval        =   40
      Left            =   0
      Top             =   0
   End
End
Attribute VB_Name = "MouseClock"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim CurPicBoxObj(5) As PictureBox
Dim Ymouse(5)
Dim Xmouse(5)
Dim dy(0 To 5, 0 To 30)
Dim dx(0 To 5, 0 To 30)
Dim Da(30)
Dim Mo(30)
Dim Split1, Day1, Year1, Todaysdate, h, m, D, s, face, Speed, n, scrll
Dim Dsplit, HandHeight, Handwidth, HandX, HandY, Step
Dim currStep(5)
Dim Test, ClockHeight, ClockWidth, ClockFromMouseY, ClockFromMouseX
Dim Fcol, Mcol, Scol, Hcol, Dcol
Private Type FL
    T(30) As Long
    Le(30) As Long
End Type
Dim FL(5) As FL
Private Type HL
    T(30) As Long
    Le(30) As Long
End Type
Dim HL(5) As HL
Private Type SL
    T(30) As Long
    Le(30) As Long
End Type
Dim SL(5) As SL
Private Type ML
    T(30) As Long
    Le(30) As Long
End Type
Dim ML(5) As ML
Private Type DL
    T(30) As Long
    Le(30) As Long
End Type
Dim DL(5) As DL
Const PI = 3.1415

Public Sub StartMouseClock(Index As Integer, UsePicBox As PictureBox)
    Set CurPicBoxObj(Index) = UsePicBox
    CurPicBoxObj(Index).Font = "Arial"
    CurPicBoxObj(Index).FontSize = 8
    Timer1(Index).Enabled = True
End Sub
Public Sub StopMouseClock(Index As Integer)
    Dim idx As Integer
    Dim i1 As Integer
    Dim i2 As Integer
    If Index < 0 Then
        i1 = 0
        i2 = Timer1.UBound
    Else
        i1 = Index
        i2 = Index
    End If
    For idx = i1 To i2
        Timer1(idx).Enabled = False
    Next
End Sub
Private Sub Form_Activate()
    StartMouseClock 1, Picture1
End Sub

Private Sub Form_Click()
    Me.Hide
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dcol = 150   '//date colour.
    Fcol = vbBlue  '//face colour.
    Scol = vbRed    '//seconds colour.
    Mcol = 0   '//minutes colour.
    Hcol = 0   '//hours colour.
    ClockHeight = 600
    ClockWidth = 600
    ClockFromMouseY = 1200
    ClockFromMouseX = 600
    '//Alter nothing below! Alignments will be lost!
    Da(1) = "SUNDAY": Da(2) = "MONDAY": Da(3) = "TUESDAY": Da(4) = "WEDNESDAY"
    Da(5) = "THURSDAY": Da(6) = "FRIDAY": Da(7) = "SATURDAY"
    Mo(1) = "JANUARY": Mo(2) = "FEBRUARY": Mo(3) = "MARCH"
    Mo(4) = "APRIL": Mo(5) = "MAY": Mo(6) = "JUNE": Mo(7) = "JULY"
    Mo(8) = "AUGUST": Mo(9) = "SEPTEMBER": Mo(10) = "OCTOBER"
    Mo(11) = "NOVEMBER": Mo(12) = "DECEMBER"
    Day1 = Day(Now)
    Year1 = Year(Now)
    If (Year1 < 2000) Then Year1 = Year1 + 1900
    Todaysdate = " " + Da(Weekday(Now)) + " " + Str(Day1) + " " + Mo(Month(Now)) + " " + Str(Year1)
    D = Todaysdate
    h = "..."
    m = "....."
    s = "....."
    face = "1 2 3 4 5 6 7 8 9 101112  "
    'CurPicBox.Font = "Arial"
    'CurPicBox.FontSize = 8
    Speed = 0.6
    n = Len(face) - 2
    For i = 0 To 5
        Ymouse(i) = (64 * 15)
        Xmouse(i) = (70 * 15)
    Next
    scrll = 0
    Split1 = 360 / n
    Dsplit = 360 / Len(D)
    HandHeight = ClockHeight / 4.5
    Handwidth = ClockWidth / 4.5
    HandY = -7
    HandX = -2.5
    scrll = 0 '2 * ClockHeight
    Step = 0.06
    'currStep(Index) = 0
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
'Ymouse = Y + ClockFromMouseY ':event.y+ClockFromMouseY;
'Xmouse = X + ClockFromMouseX ':event.x+ClockFromMouseX;
End Sub

Public Sub MouseMoves(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
    Ymouse(Index) = y + ClockFromMouseY ':event.y+ClockFromMouseY;
    Xmouse(Index) = x + ClockFromMouseX ':event.x+ClockFromMouseX;
End Sub

Private Sub Picture1_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    MouseMoves 1, Button, Shift, x, y
End Sub

Private Sub Timer1_Timer(Index As Integer)
    Static TimerIsBusy As Boolean
    Dim i As Integer
    Dim secs, sec, mins, min, hrs, hr
    If Not TimerIsBusy Then
        'TimerIsBusy = True
        'Timer1(Index).Enabled = False
        dy(Index, 0) = dy(Index, 0) + (Ymouse(Index) - dy(Index, 0)) * Speed
        dy(Index, 0) = Fix(dy(Index, 0))
        dx(Index, 0) = dx(Index, 0) + (Xmouse(Index) - dx(Index, 0)) * Speed
        dx(Index, 0) = Fix(dx(Index, 0))
        For i = 1 To Len(D) - 1
            dy(Index, i) = dy(Index, i) + (dy(Index, i - 1) - dy(Index, i)) * Speed
            dy(Index, i) = Fix(dy(Index, i))
            dx(Index, i) = dx(Index, i) + (dx(Index, i - 1) - dx(Index, i)) * Speed
            dx(Index, i) = Fix(dx(Index, i))
        Next i
        secs = Second(Now)
        sec = -1.57 + PI * secs / 30
        mins = Minute(Now)
        min = -1.57 + PI * mins / 30
        hr = Hour(Now)
        hrs = -1.575 + PI * hr / 6 + PI * Int(Minute(Now)) / 360
        For i = 0 To n - 2
            FL(Index).T(i) = dy(Index, i) + ClockHeight * Sin(-1.0471 + i * Split1 * PI / 180) + scrll
            FL(Index).Le(i) = dx(Index, i) + ClockWidth * Cos(-1.0471 + i * Split1 * PI / 180)
        Next i
        For i = 0 To Len(h) - 1
            HL(Index).T(i) = dy(Index, i) + HandY + (i * HandHeight) * Sin(hrs) + scrll
            HL(Index).Le(i) = dx(Index, i) + HandX + (i * Handwidth) * Cos(hrs)
        Next i
        For i = 0 To Len(m) - 1
            ML(Index).T(i) = dy(Index, i) + HandY + (i * HandHeight) * Sin(min) + scrll
            ML(Index).Le(i) = dx(Index, i) + HandX + (i * Handwidth) * Cos(min)
        Next i
        For i = 0 To Len(s) - 1
            SL(Index).T(i) = dy(Index, i) + HandY + (i * HandHeight) * Sin(sec) + scrll
            SL(Index).Le(i) = dx(Index, i) + HandX + (i * Handwidth) * Cos(sec)
        Next i
        For i = 0 To Len(D) - 1
            DL(Index).T(i) = dy(Index, i) + ClockHeight * 1.5 * Sin(currStep(Index) + i * Dsplit * PI / 180) + scrll
            DL(Index).Le(i) = dx(Index, i) + ClockWidth * 1.5 * Cos(currStep(Index) + i * Dsplit * PI / 180)
        Next i
        currStep(Index) = currStep(Index) - Step
        P Index
        'Timer1(Index).Enabled = True
        TimerIsBusy = False
    End If
End Sub

Private Function SP(ByVal ST As String, ByVal Nu As Integer, Optional K As Byte = 1) As String
    SP = Mid(ST, Nu + 1, K)
End Function
Private Sub P(Index As Integer)
    Dim i As Integer
    CurPicBoxObj(Index).Cls
    With CurPicBoxObj(Index)
        '.FontBold = False
        .FontBold = True
        .ForeColor = Dcol
        For i = 0 To Len(D) - 1
            .CurrentY = DL(Index).T(i)
            .CurrentX = DL(Index).Le(i)
            CurPicBoxObj(Index).Print SP(D, i)
        Next i
        .ForeColor = Fcol
        For i = 0 To n - 1
            .CurrentY = FL(Index).T(i)
            .CurrentX = FL(Index).Le(i)
            If (i = 18 Or i = 20 Or i = 22) Then
                CurPicBoxObj(Index).Print SP(face, i, 2)
                i = i + 1
            Else
                CurPicBoxObj(Index).Print SP(face, i, 1)
            End If
        Next i
        .FontBold = True
        .ForeColor = Scol
        For i = 0 To Len(s) - 1
            .CurrentY = SL(Index).T(i)
            .CurrentX = SL(Index).Le(i)
            'Second Handle
            CurPicBoxObj(Index).Print SP(s, i)
        Next i
        'Minute Handle
        .ForeColor = Mcol
        For i = 0 To Len(m) - 1
            .CurrentY = ML(Index).T(i)
            .CurrentX = ML(Index).Le(i)
            CurPicBoxObj(Index).Print SP(m, i)
        Next i
        'Hour Handle
        .ForeColor = Hcol
        For i = 0 To Len(h) - 1
            .CurrentY = HL(Index).T(i)
            .CurrentX = HL(Index).Le(i)
            CurPicBoxObj(Index).Print SP(h, i)
        Next i
    End With
End Sub

