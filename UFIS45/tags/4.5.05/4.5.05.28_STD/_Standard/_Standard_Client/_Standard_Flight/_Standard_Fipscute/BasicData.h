// basicdat.h CCSBasicData class for providing general used methods

#ifndef _BASICDATA
#define _BASICDATA

//#include "InitialLoadDlg.h"
#include "ccsglobl.h"
#include "ccsptrarray.h"
#include "ccscedaData.h"
#include "CcsLog.h"
//#include "CedaCfgData.h"
#include "CCSTime.h"
#include "CCSDdx.h"
#include "CCSCedaCom.h"
#include "CCSBcHandle.h"
#include "CCSMessageBox.h"


int GetItemCount(CString olList, char cpTrenner  = ',' );
int ExtractItemList(CString opSubString, CStringArray *popStrArray, char cpTrenner = ',');

CString GetListItem(CString &opList, int ipPos, bool bpCut, char cpTrenner  = ',');

bool GetListItemByField(CString &opDataList, CString &opFieldList, CString &opField, CString &opData);
bool GetListItemByField(CString &opDataList, CString &opFieldList, CString &opField, CTime &opData);


CString DeleteListItem(CString &opList, CString olItem);

int SplitItemList(CString opString, CStringArray *popStrArray, int ipMaxItem = 10, char cpTrenner = ',');

void TraceFieldAndData(CString &opFieldList,CString &opListOfData, CString opMessage);

// String rechts auff�llen
int FillRightByChar( CString &opStr, char cpSign, int ipLength ) ; 
int FillLeftByChar( CString &opStr, char cpSign, int ipLength ) ; 

// Linken Anteil von String besorgen   
int CutLeft(CString & ropLeft, CString & ropLeftover, const char * OneOfSep) ;
int CutLeft(CString & ropLeft, CString & ropLeftover, int ipNo) ;

// Zeitraumangabe in Kurzform aufbereiten (POPS) 
const int cimFullRangeLen = 21 ;	//	L�nge eines Range Strings "dd.mm.yyyy-dd.mm.yyyy"
void ShortRangeString( CString &ropStr ) ;

/////////////////////////////////////////////////////////////////////////////
// class BasicData

class CBasicData : public CCSCedaData
{

public:

	CBasicData(void);
	~CBasicData(void);
	
	int imScreenResolutionX;
	int imScreenResolutionY;
	int imMonitorCount;
	long ConfirmTime;
	CString omUserID;

	long GetNextUrno(void);
	int imNextOrder;
	bool GetManyUrnos(int ipAnz, CUIntArray &opUrnos);


	int GetNextOrderNo();

	char *GetCedaCommand(CString opCmdType);
	void GetDiagramStartTime(CTime &opStart, CTime &opEnd);
	void SetDiagramStartTime(CTime opDiagramStartTime, CTime opEndTime);
	void SetWorkstationName(CString opWsName);
	CString GetWorkstationName();
	bool GetWindowPosition(CRect& rlPos,CString olMonitor);

	// Timeroutines

	void SetLocalDiff();

	CTimeSpan GetLocalDiff(CTime opDate)
	{
		if(opDate < omTich)
		{
			return omLocalDiff1;
		}
		else
		{
			return omLocalDiff2;
		}

		return omLocalDiff2;
	};


	CTimeSpan GetLocalDiff1() { return omLocalDiff1; };
	CTimeSpan GetLocalDiff2() { return omLocalDiff2; };
	CTime     GetTich() { return omTich; };


	void LocalToUtc(CTime &opTime);
	void UtcToLocal(CTime &opTime);

	// end time..



public:
	static void TrimLeft(char *s);
	static void TrimRight(char *s);

private:

	CUIntArray omNewUrnos;

	CTimeSpan omLocalDiff1;
	CTimeSpan omLocalDiff2;
	CTime	  omTich;

	char pcmCedaCmd[12];
	CTime omDiaStartTime;
	CTime omDiaEndTime;

	CString omDefaultComands;
	CString omActualComands;
	char pcmCedaComand[24];
	CString omWorkstationName; 
};


#endif
