//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by CuteIF.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_CUTEIF_DIALOG               102
#define IDS_STRING102                   102
#define IDD_CUTEIF                      102
#define IDS_STRING103                   103
#define IDS_STRING104                   104
#define IDS_STRING105                   105
#define IDS_STRING106                   106
#define IDS_STRING107                   107
#define IDS_STRING108                   108
#define IDS_STRING109                   109
#define IDS_STRING110                   110
#define IDS_ERROR                       111
#define IDS_STRING112                   112
#define IDS_STRING113                   113
#define IDS_STRING114                   114
#define IDR_MAINFRAME                   128
#define IDB_NOLOGO                      129
#define IDB_LOGO                        130
#define IDD_BLT_FREETEXT                132
#define IDC_CKICGAT_STATE               1001
#define IDC_LOGO_SEL                    1004
#define IDC_FIDS_REMARKS                1007
#define IDC_FREE1                       1008
#define IDC_FREE2                       1009
#define IDC_TRANSMIT                    1010
#define IDC_CKICGATE_CLOSE              1011
#define IDC_DUMMYLOGO                   1016
#define IDC_CHECKIN_SEL                 1022
#define IDC_GATE_SEL                    1026
#define IDC_EXTERN                      1027
#define IDC_CKIC_STATIC                 1028
#define IDC_GATE_STATIC                 1029
#define IDC_FLT_BOR                     1031
#define IDC_BLT_NAME                    1032
#define IDC_FREETEXT1                   1033
#define IDC_FREETEXT2                   1034
#define IDC_SIGN                        1036

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1037
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
