Attribute VB_Name = "mdlMain"
Public fMainForm As frmMain

Global gsAppName As String
Global gsUserName As String
Global gbDebug As Boolean
Global gbOffline As Boolean
Global sglUTCOffsetHours As Single
Global sExcelSeparator As String
Global sConfigFile As String

Public Declare Sub GetLocalTime Lib "kernel32" (lpSystemTime As SYSTEMTIME)

Public Type SYSTEMTIME
   wYear           As Integer
   wMonth          As Integer
   wDayOfWeek      As Integer
   wDay            As Integer
   wHour           As Integer
   wMinute         As Integer
   wSecond         As Integer
   wMilliseconds   As Integer
End Type


Sub Main()
    Dim strCommand As String

    ' handling of command arguments
    strCommand = Command()
    If GetItem(strCommand, 1, ",") = "DEBUG" Then
        gbDebug = True
    Else
        gbDebug = False
    End If
    If GetItem(strCommand, 2, ",") = "OFFLINE" Then
        gbOffline = True
    Else
        gbOffline = False
    End If

    ' setting the application data
    gsAppName = "FlightDataGenerator"

    ' setting the config file
    If ExistFile("c:\ufis\system\" & gsAppName & ".cfg") = True Then
        sConfigFile = "c:\ufis\system\" & gsAppName & ".cfg"
    Else
        sConfigFile = DEFAULT_CEDA_INI
    End If

    ' Load the main form and all included controls on that form
'    Set frmMainForm = New frmMain
'    Load frmMainForm
    Load frmMain
    frmMain.Visible = False

    'do the login
    InitLoginControl
    If gbDebug = False Then
        strRetLogin = frmMain.LoginCtrl.ShowLoginDialog
    Else
        strRetLogin = "OK"
    End If

    'look after the login result
    If strRetLogin = "CANCEL" Or strRetLogin = "ERROR" Then
        frmMain.UfisComCtrl.CleanupCom
        End
    Else
        frmMain.UfisComCtrl.CleanupCom
        gsUserName = frmMain.LoginCtrl.GetUserName()
'        frmMain.SetPrivStat
'        frmData.InitialLoadData

        If gbDebug = True Then
            frmData.Visible = True
        End If

        'frmMain.RefreshView
        frmMain.sbStatusBar.Panels(1).Text = "Ready."
        frmMain.Show 'vbModal
    End If
End Sub

Private Sub InitLoginControl()
    Dim strRegister As String
    ' giving the login control an UfisCom to do the login
    Set frmMain.LoginCtrl.UfisComCtrl = frmMain.UfisComCtrl

    ' setting some information to be displayed on the login control
    frmMain.LoginCtrl.ApplicationName = gsAppName
    frmMain.LoginCtrl.VersionString = App.Major & "." & App.Minor & ".0." & App.Revision
    frmMain.LoginCtrl.InfoCaption = "Info about Flight Data Generator"
    frmMain.LoginCtrl.InfoButtonVisible = True
    frmMain.LoginCtrl.InfoUfisVersion = "UFIS Version 4.5"
    frmMain.LoginCtrl.InfoAppVersion = CStr("Flight Data Generator ") & frmMain.LoginCtrl.VersionString
    frmMain.LoginCtrl.InfoCopyright = "� 2005-2007 UFIS Airport Solutions GmbH"
    frmMain.LoginCtrl.InfoAAT = "UFIS Airport Solutions GmbH"
    frmMain.LoginCtrl.UserNameLCase = False 'not automatic upper case letters
    frmMain.LoginCtrl.LoginAttempts = 3

    strRegister = gsAppName & ",InitModu,InitModu,Initialize (InitModu),B,-"
    frmMain.LoginCtrl.RegisterApplicationString = strRegister
End Sub

