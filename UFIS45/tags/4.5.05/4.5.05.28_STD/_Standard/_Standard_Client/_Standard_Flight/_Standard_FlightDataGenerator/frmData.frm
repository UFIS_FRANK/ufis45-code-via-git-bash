VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmData 
   Caption         =   "Data Tabs"
   ClientHeight    =   9255
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   14130
   LinkTopic       =   "Form1"
   ScaleHeight     =   9255
   ScaleWidth      =   14130
   StartUpPosition =   3  'Windows Default
   Begin TABLib.TAB TabGAT 
      Height          =   3735
      Left            =   6120
      TabIndex        =   8
      Tag             =   "{=TABLE=}GATTAB{=FIELDS=}URNO,GNAM"
      Top             =   4560
      Width           =   1335
      _Version        =   65536
      _ExtentX        =   2355
      _ExtentY        =   6588
      _StockProps     =   64
   End
   Begin TABLib.TAB TabAPT 
      Height          =   3735
      Left            =   240
      TabIndex        =   4
      Tag             =   "{=TABLE=}APTTAB{=FIELDS=}URNO,APC3,APFN"
      Top             =   4560
      Width           =   1935
      _Version        =   65536
      _ExtentX        =   3413
      _ExtentY        =   6588
      _StockProps     =   64
   End
   Begin VB.CommandButton cmdReadFromFiles 
      Caption         =   "ReadFromFiles"
      Height          =   375
      Left            =   2520
      TabIndex        =   2
      Top             =   8760
      Width           =   2175
   End
   Begin VB.CommandButton cmdSaveToFiles 
      Caption         =   "SaveToFiles"
      Height          =   375
      Left            =   240
      TabIndex        =   1
      Top             =   8760
      Width           =   2175
   End
   Begin TABLib.TAB TabAFT 
      Height          =   3615
      Left            =   240
      TabIndex        =   0
      Tag             =   $"frmData.frx":0000
      Top             =   480
      Width           =   11535
      _Version        =   65536
      _ExtentX        =   20346
      _ExtentY        =   6376
      _StockProps     =   64
   End
   Begin TABLib.TAB TabACR 
      Height          =   3735
      Left            =   2520
      TabIndex        =   6
      Tag             =   "{=TABLE=}ACRTAB{=FIELDS=}URNO,ACT3,REGN,ACT5"
      Top             =   4560
      Width           =   3375
      _Version        =   65536
      _ExtentX        =   5953
      _ExtentY        =   6588
      _StockProps     =   64
   End
   Begin TABLib.TAB TabCIC 
      Height          =   3735
      Left            =   7560
      TabIndex        =   10
      Tag             =   "{=TABLE=}CICTAB{=FIELDS=}URNO,CNAM"
      Top             =   4560
      Width           =   1335
      _Version        =   65536
      _ExtentX        =   2355
      _ExtentY        =   6588
      _StockProps     =   64
   End
   Begin TABLib.TAB TabPST 
      Height          =   3735
      Left            =   9000
      TabIndex        =   12
      Tag             =   "{=TABLE=}PSTTAB{=FIELDS=}URNO,PNAM"
      Top             =   4560
      Width           =   1335
      _Version        =   65536
      _ExtentX        =   2355
      _ExtentY        =   6588
      _StockProps     =   64
   End
   Begin TABLib.TAB TabBLT 
      Height          =   3735
      Left            =   10440
      TabIndex        =   14
      Tag             =   "{=TABLE=}BLTTAB{=FIELDS=}URNO,BNAM"
      Top             =   4560
      Width           =   1335
      _Version        =   65536
      _ExtentX        =   2355
      _ExtentY        =   6588
      _StockProps     =   64
   End
   Begin TABLib.TAB TabALT 
      Height          =   3735
      Left            =   11880
      TabIndex        =   16
      Tag             =   "{=TABLE=}ALTTAB{=FIELDS=}URNO,ALC2,ALC3"
      Top             =   4560
      Width           =   1935
      _Version        =   65536
      _ExtentX        =   3413
      _ExtentY        =   6588
      _StockProps     =   64
   End
   Begin VB.Label Label8 
      Caption         =   "ALTTAB:"
      Height          =   255
      Left            =   11880
      TabIndex        =   17
      Top             =   4320
      Width           =   855
   End
   Begin VB.Label Label7 
      Caption         =   "BLTTAB:"
      Height          =   255
      Left            =   10440
      TabIndex        =   15
      Top             =   4320
      Width           =   855
   End
   Begin VB.Label Label6 
      Caption         =   "PSTTAB:"
      Height          =   255
      Left            =   9000
      TabIndex        =   13
      Top             =   4320
      Width           =   855
   End
   Begin VB.Label Label5 
      Caption         =   "CICTAB:"
      Height          =   255
      Left            =   7560
      TabIndex        =   11
      Top             =   4320
      Width           =   855
   End
   Begin VB.Label Label4 
      Caption         =   "GATTAB:"
      Height          =   255
      Left            =   6120
      TabIndex        =   9
      Top             =   4320
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "ACRTAB:"
      Height          =   255
      Left            =   2520
      TabIndex        =   7
      Top             =   4320
      Width           =   1575
   End
   Begin VB.Label Label2 
      Caption         =   "APTTAB:"
      Height          =   255
      Left            =   240
      TabIndex        =   5
      Top             =   4320
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "AFTTAB:"
      Height          =   255
      Left            =   240
      TabIndex        =   3
      Top             =   120
      Width           =   1695
   End
End
Attribute VB_Name = "frmData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public strFieldSeparatorTabDatabase As String
Public strFieldSeparatorTabConfig As String
Public strServer As String
Public strHOPO As String
Public strTableExt As String
Public strAccessMethod As String
Private bmLoadBasicData As String

Public Sub cmdReadFromFiles_Click()
    Dim ctl As Control
    Dim sCtlType As String
    Dim sFileName As String
    For Each ctl In frmData.Controls
        sCtlType = TypeName(ctl)
        If sCtlType = "TAB" Then
            sFileName = "c:\tmp\" & ctl.Name & ".txt"
            If ExistFile(sFileName) = True Then
                InitTabGeneral ctl
                ctl.ReadFromFile sFileName
                ctl.AutoSizeColumns
            End If
        End If
    Next
End Sub

Private Sub cmdSaveToFiles_Click()
    Dim ctl As Control
    Dim sCtlType As String
    For Each ctl In frmData.Controls
        sCtlType = TypeName(ctl)
        If sCtlType = "TAB" Then
            ctl.WriteToFile "c:\tmp\" & ctl.Name & ".txt", False
        End If
    Next
End Sub

Private Sub Form_Load()
    Dim strTmp As String
    
    ' set the field separators
    strFieldSeparatorTabDatabase = ","
    strFieldSeparatorTabConfig = "="

    ' read entries from ceda.ini
    strAccessMethod = "CEDA"
    strHOPO = GetIniEntry("", gsAppName, "GLOBAL", "HOMEAIRPORT", "XXX")
    strServer = GetIniEntry("", gsAppName, "GLOBAL", "HOSTNAME", "XXX")
    strTableExt = GetIniEntry("", gsAppName, "GLOBAL", "TABLEEXTENSION", "TAB")

    bmLoadBasicData = True

    'Init the Tabs
    InitTabs
    
    ' get the UTC-difference
    sglUTCOffsetHours = GetUTCOffset
End Sub

Private Function GetUTCOffset() As Single
    Dim strUtcArr() As String
    Dim strArr() As String
    Dim CurrStr As String
    Dim tmpStr As String

    Dim istrRet As Integer
    Dim count As Integer
    Dim i As Integer

    GetUTCOffset = 0

    If gbOffline = False Then
        ConnectUfisCom
        If frmMain.UfisComCtrl.CallServer("GFR", "", "", "", "[CONFIG]", "360") = 0 Then
            tmpStr = frmMain.UfisComCtrl.GetDataBuffer(True)
        End If
        strArr = Split(tmpStr, Chr(10))
        count = UBound(strArr)
        For i = 0 To count
            CurrStr = strArr(i)
            istrRet = InStr(1, CurrStr, "UTCD", 0)
            If istrRet <> 0 Then
                strUtcArr = Split(strArr(i), ",")
                GetUTCOffset = CSng(strUtcArr(1) / 60)
                Exit For
            End If
        Next i
        DisconnetUfisCom
    Else
        GetUTCOffset = 7
    End If
End Function

Public Sub ConnectUfisCom()
    If gbOffline = False Then
        frmMain.UfisComCtrl.SetCedaPerameters frmMain.LoginCtrl.GetUserName, strHOPO, strTableExt
        frmMain.UfisComCtrl.InitCom strServer, strAccessMethod
        frmMain.UfisComCtrl.Module = "FDG " & App.Major & "." & App.Minor & "." & App.Revision
    End If
End Sub

Public Sub DisconnetUfisCom()
    If gbOffline = False Then
        frmMain.UfisComCtrl.CleanupCom
    End If
End Sub

Private Sub InitTabs()
    InitTabGeneral TabAFT
    InitTabForCedaConnection TabAFT
End Sub

Private Sub InitTabGeneral(ByRef rTab As TABLib.Tab)
    rTab.ResetContent
    rTab.FontName = "Courier New"
    rTab.HeaderFontSize = "18"
    rTab.FontSize = "16"
    rTab.EnableHeaderSizing True
    rTab.ShowHorzScroller True
    rTab.AutoSizeByHeader = True
    rTab.SetFieldSeparator strFieldSeparatorTabDatabase

    Dim ilCnt As Integer
    Dim i As Integer
    Dim strFields As String
    Dim strHeaderLengthString As String
    GetKeyItem strFields, rTab.Tag, "{=FIELDS=}", "{="
    ilCnt = ItemCount(strFields, ",")
    For i = 1 To ilCnt Step 1
        strHeaderLengthString = strHeaderLengthString & "10,"
    Next i
    strHeaderLengthString = Left(strHeaderLengthString, Len(strHeaderLengthString) - 1)
    rTab.HeaderLengthString = strHeaderLengthString

    strFields = Replace(strFields, ",", strFieldSeparatorTabDatabase)
    rTab.HeaderString = strFields
    rTab.LogicalFieldList = strFields
End Sub

Private Sub InitTabForCedaConnection(ByRef rTab As TABLib.Tab)
    'do the initializing for the CEDA-connection
    rTab.CedaServerName = strServer
    rTab.CedaPort = "3357"
    rTab.CedaHopo = strHOPO
    rTab.CedaCurrentApplication = gsAppName
    rTab.CedaTabext = strTableExt
    rTab.CedaUser = gsUserName
    rTab.CedaWorkstation = frmMain.LoginCtrl.GetWorkStationName
    rTab.CedaSendTimeout = "120"
    rTab.CedaReceiveTimeout = "240"
    rTab.CedaRecordSeparator = Chr(10)
    rTab.CedaIdentifier = gsAppName
End Sub

Public Sub LoadAFT(oFrom As Date, oTo As Date, oLIKE As String)
    Dim strWhere As String
    Dim strFrom As String
    Dim strTo As String
    Dim strTmp As String
    Dim ilIdx As Integer
    Dim dateFrom As Date
    Dim dateTo As Date

    LoadBasicData
    
    ' Init the TAB
    InitTabGeneral TabAFT
    InitTabForCedaConnection TabAFT

    ' loading AFTTAB data
    frmMain.sbStatusBar.Panels(1).Text = "Loading AFTTAB (flight schedule)..."
    
    dateFrom = DateAdd("h", sglUTCOffsetHours * -1, oFrom)
    dateTo = DateAdd("h", sglUTCOffsetHours * -1, oTo)

    strFrom = Format(dateFrom, "YYYYMMDDhhmmss")
    strTo = Format(dateTo, "YYYYMMDDhhmmss")

    strWhere = "WHERE ((STOA BETWEEN '" & strFrom & "' AND '" & strTo & "' AND ADID='A') OR (" & _
                "STOD BETWEEN '" & strFrom & "' AND '" & strTo & "' AND ADID='D'))"
    If Len(Trim(oLIKE)) > 0 Then
        strWhere = strWhere & " AND " & oLIKE
    End If
    LoadData TabAFT, strWhere
    TabAFT.IndexCreate "URNO", 0
    TabAFT.IndexCreate "RKEY", 1
    TabAFT.AutoSizeColumns
    If TabAFT.GetLineCount = 0 Then
        MsgBox "No flights found.", vbInformation, "No flights"
    End If
    frmMain.sbStatusBar.Panels(1).Text = "Ready."
End Sub

Public Sub InitialLoad(bpLoadBasicdata As Boolean, sFrom As String, sTo As String)

End Sub

Public Sub LoadData(ByRef rTab As TABLib.Tab, ByRef pWhere As String, Optional bReset As Boolean = True)
    Dim strCommand As String
    Dim strTable As String
    Dim strFieldList As String
    Dim strData As String
    Dim strWhere As String

    strCommand = "RT"
    If (bReset = True) Then
        rTab.ResetContent
    End If
    GetKeyItem strTable, rTab.Tag, "{=TABLE=}", "{="
    GetKeyItem strFieldList, rTab.Tag, "{=FIELDS=}", "{="
    strData = ""
    strWhere = pWhere
    If Len(strWhere) = 0 Then
        GetKeyItem strWhere, rTab.Tag, "{=WHERE=}", "{="
    End If
    If rTab.CedaAction(strCommand, strTable, strFieldList, strData, strWhere) = False Then
        MsgBox "Error loading " & strTable & "!" & vbCrLf & _
            "Field list: " & strFieldList & vbCrLf & _
            "Where statement: " & strWhere, vbCritical, "Error reading data (RT)"
            rTab.GetLastCedaError
    End If
End Sub

Public Sub LoadBasicData()
    Dim strWhere As String
    Dim strFrom As String
    Dim strTo As String
    Dim strTmp As String
    Dim ilIdx As Integer
    Dim dateFrom As Date
    Dim dateTo As Date

    If bmLoadBasicData = True And gbOffline = False Then
        bmLoadBasicData = False
    Else
        Exit Sub
    End If

    ' loading APTTAB data
    InitTabGeneral TabAPT
    InitTabForCedaConnection TabAPT
    frmMain.sbStatusBar.Panels(1).Text = "Loading APTTAB (airport data)..."
    strWhere = "WHERE LAND = 'TH'"
    LoadData TabAPT, strWhere
    TabAPT.AutoSizeColumns
    frmMain.sbStatusBar.Panels(1).Text = "Ready."

    ' loading ACTTAB
    InitTabGeneral TabACR
    InitTabForCedaConnection TabACR
    frmMain.sbStatusBar.Panels(1).Text = "Loading ACRTAB (aircraft registrations)..."
    strWhere = ""
    LoadData TabACR, strWhere
    TabACR.AutoSizeColumns
    TabACR.Sort 2, True, True
    frmMain.sbStatusBar.Panels(1).Text = "Ready."

    InitTabGeneral TabGAT
    InitTabForCedaConnection TabGAT
    frmMain.sbStatusBar.Panels(1).Text = "Loading GATTAB (gates)..."
    strWhere = ""
    LoadData TabGAT, strWhere
    TabGAT.AutoSizeColumns
    TabGAT.IndexCreate "GNAM", 1
    frmMain.sbStatusBar.Panels(1).Text = "Ready."

    InitTabGeneral TabCIC
    InitTabForCedaConnection TabCIC
    frmMain.sbStatusBar.Panels(1).Text = "Loading CICTAB (check-in counter)..."
    strWhere = ""
    LoadData TabCIC, strWhere
    TabCIC.AutoSizeColumns
    TabCIC.IndexCreate "CNAM", 1
    frmMain.sbStatusBar.Panels(1).Text = "Ready."

    InitTabGeneral TabPST
    InitTabForCedaConnection TabPST
    frmMain.sbStatusBar.Panels(1).Text = "Loading PSTTAB (positions)..."
    strWhere = ""
    LoadData TabPST, strWhere
    TabPST.AutoSizeColumns
    TabPST.IndexCreate "PNAM", 1
    frmMain.sbStatusBar.Panels(1).Text = "Ready."

    InitTabGeneral TabBLT
    InitTabForCedaConnection TabBLT
    frmMain.sbStatusBar.Panels(1).Text = "Loading BLTTAB (baggage belts)..."
    strWhere = ""
    LoadData TabBLT, strWhere
    TabBLT.AutoSizeColumns
    TabBLT.IndexCreate "BNAM", 1
    frmMain.sbStatusBar.Panels(1).Text = "Ready."

    InitTabGeneral TabALT
    InitTabForCedaConnection TabALT
    frmMain.sbStatusBar.Panels(1).Text = "Loading ALTTAB (airlines)..."
    strWhere = ""
    LoadData TabALT, strWhere
    TabALT.AutoSizeColumns
    TabALT.IndexCreate "ALC3", 1
    frmMain.sbStatusBar.Panels(1).Text = "Ready."
End Sub
