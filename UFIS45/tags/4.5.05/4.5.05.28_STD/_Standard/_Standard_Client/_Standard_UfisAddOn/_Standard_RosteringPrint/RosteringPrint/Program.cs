﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Ufis.MVVM.ViewModel;
using Ufis.Data;
using Ufis.LoginWindow;
using Ufis.NotificationWindow;
using Ufis.Utilities;
using RosteringPrint.DataAccess;
using RosteringPrint.Helpers;
using RosteringPrint.Shell;
using Ufis.Security;
using System.Windows.Media.Imaging;

namespace RosteringPrint
{
    class App : Application
    {
        public App()
        {
            InitializeComponent();
        }

        public event EventHandler BeforeMainWindowClosed;

        private void InitializeComponent()
        {
            //Set current application infomation
            AppInfo appInfo = HpAppInfo.Current;
            appInfo.ProductCode = "BDPS-UIF";
            appInfo.ProductTitle = "Rostering Print";
            appInfo.Description = "To print the staff roster report";
            appInfo.Image = new BitmapImage(new Uri("../Images/BDPSUIF_128x128.png", UriKind.Relative));

            //Create shell window
            ShellView shell = new ShellView();
            this.MainWindow = shell;
            shell.BeforeClosed += OnMainWindowBeforeClosed;

            // Create the ViewModel to which 
            // the shell window binds.
            var viewModel = new ShellViewModel();

            // When the ViewModel asks to be closed, 
            // close the window.
            EventHandler handler = null;
            handler = (sender, e) =>
            {
                viewModel.RequestClose -= handler;
                shell.Close();
            };
            viewModel.RequestClose += handler;

            //check the prequisite requirements, if all ok
            //shows the login dialog
            string connectionError = CheckConnection();
            WorkspaceViewModel workspace = null;
            if (string.IsNullOrEmpty(connectionError))
            {
                IUfisUserAuthentication userAuthentication = DlUfisData.Current.UserAuthenticationManager;
                LoginViewModel loginViewModel;
#if !XABPP
                loginViewModel = new LoginViewModel(appInfo, userAuthentication, LoginViewModel.GetLastUser()) { AutoSaveLastUser = true };
#else
                loginViewModel = new LoginViewModel(appInfo, userAuthentication);
#endif
                loginViewModel.ProjectInfo = DlUfisData.Current.ProjectInfo;
                workspace = loginViewModel;
            }
            //otherwise shows ErrorScreen
            else
            {
                workspace = new NotificationViewModel()
                {
                    NotificationInfo = new MessageInfo()
                    {
                        InfoText = connectionError,
                        Severity = MessageInfo.MessageInfoSeverity.Error
                    },
                    CanContinue = false
                };
            }
            viewModel.Workspace = workspace;

            // Allow all controls in the window to 
            // bind to the ViewModel by setting the 
            // DataContext, which propagates down 
            // the element tree.
            shell.DataContext = viewModel;

            shell.Show();
        }

        private string CheckConnection()
        {
            const string CONN_ERROR = "We could not connect to the application server(s).";
            const string CONN_ERROR_UNABLE_TO_CONNECT = "Could not connect to following server(s):";
            const string CONN_ERROR_NO_CONFIG = "Please make sure that you have the Ceda.ini configuration file in your system.";

            string strConnErr = null;

            string strAppName = HpAppInfo.Current.ProductCode;
            EntityDataContextBase dataContext = DlUfisData.CreateInstance(strAppName).DataContext;
            if (dataContext == null)
            {
                strConnErr = String.Format("{0}\n{1}", CONN_ERROR, CONN_ERROR_NO_CONFIG);
            }
            else
            {
                ConnectionBase connection = dataContext.Connection;
                if (connection == null)
                    strConnErr = String.Format("{0}\n{1}\n{2}", CONN_ERROR, CONN_ERROR_UNABLE_TO_CONNECT, connection.ServerList);
            }

            return strConnErr;
        }

        void OnMainWindowBeforeClosed(object sender, EventArgs e)
        {
            if (BeforeMainWindowClosed != null)
                BeforeMainWindowClosed(this, EventArgs.Empty);
        }
    }

    static class Program
    {
        static App _currentApp;

        [STAThread]
        static void Main(string[] args)
        {
            App app = new App();
            _currentApp = app;
            app.BeforeMainWindowClosed += OnBeforeMainWindowClosed;
            app.DispatcherUnhandledException += OnDispatcherUnhandledException;
            app.Run();            
        }

        static void OnBeforeMainWindowClosed(object sender, EventArgs e)
        {
            DlUfisData dlUfisData = DlUfisData.Current;
            if (dlUfisData != null)
            {
                EntityDataContextBase dataContext = dlUfisData.DataContext;
                if (dataContext != null)
                {
                    ConnectionBase connection = dataContext.Connection;
                    if (connection != null)
                    {
                        if (connection.State == ConnectionState.Open) connection.Close();
                        connection = null;
                    }
                    dataContext.Dispose();
                }
            }
        }

        static void OnDispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            string strException = "Application is going to close.\nException occured:\n";
            Exception CurrentException = e.Exception;
            while (CurrentException != null)
            {
                strException += CurrentException.Message + "\n";
                CurrentException = CurrentException.InnerException;
            }

            App app = _currentApp;
            ShellView shell = (ShellView)app.MainWindow;
            ShellViewModel viewModel = (ShellViewModel)shell.DataContext;
            WorkspaceViewModel workspace = new NotificationViewModel()
            {
                NotificationInfo = new MessageInfo()
                {
                    InfoText = strException,
                    Severity = MessageInfo.MessageInfoSeverity.Error
                },
                CanContinue = false
            };
            viewModel.Workspace = workspace;

            e.Handled = true;
        }
    }
}
