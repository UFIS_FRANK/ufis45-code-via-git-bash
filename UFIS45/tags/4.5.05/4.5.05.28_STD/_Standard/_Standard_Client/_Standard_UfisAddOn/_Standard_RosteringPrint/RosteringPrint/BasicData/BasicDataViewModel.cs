﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.MVVM.ViewModel;
using Ufis.Entities;
using System.Windows.Input;
using Ufis.MVVM.Command;
using RosteringPrint.Helpers;
using DevExpress.Xpf.Grid;
using System.Windows;
using Ufis.Data;
using System.Reflection;

namespace RosteringPrint.BasicData
{
    public abstract class BasicDataViewModel : WorkspaceViewModel
    {
        private const string DELETE_WARNING_TEXT =
            "Note: Other data depending on selected record may be damaged when deleting this record!\n" +
            "Do you really want to delete the selected record?";
        private const string DELETE_WARNING_CAPTION = "Warning";
        private const string INVALID_ENTRY_MANDATORY_TEXT = "Please enter the value(s) of the following field(s):";
        private const string INVALID_ENTRY_UNIQUE_TEXT = "The value(s) of the following field(s) must be unique:";
        private const string INVALID_ENTRY_VALIDITY_TEXT = "Valid to must be after valid from.";
        private const string INVALID_ENTRY_WARNING_CAPTION = "Invalid Entry";

        private bool _showAutoFilterRow;
        private bool _isEditing;
        private object _itemsSource;
        private ICollection<EntDbUserDefinedLayout> _userDefinedLayouts;
        RelayCommand _insertCommand;
        RelayCommand _updateCommand;
        RelayCommand _deleteCommand;
        RelayCommand _copyCommand;
        RelayCommand _saveUpdateCommand;
        RelayCommand _cancelUpdateCommand;

        public bool ShowAutoFilterRow
        {
            get
            {
                return _showAutoFilterRow;
            }
            set
            {
                if (_showAutoFilterRow == value)
                    return;

                _showAutoFilterRow = value;

                OnPropertyChanged("ShowAutoFilterRow");
            }
        }

        public bool IsEditing
        {
            get
            {
                return _isEditing;
            }
            set
            {
                if (_isEditing == value)
                    return;

                _isEditing = value;

                OnPropertyChanged("IsEditing");
            }
        }

        public object ItemsSource 
        { 
            get
            {
                return _itemsSource;
            }
            set
            {
                if (_itemsSource == value)
                    return;

                _itemsSource = value;
                OnPropertyChanged("ItemsSource");
            }
        }

        public ICollection<EntDbUserDefinedLayout> UserDefinedLayouts 
        { 
            get
            {
                return _userDefinedLayouts;
            }
            set
            {
                if (_userDefinedLayouts == value)
                    return;

                _userDefinedLayouts = value;
                OnPropertyChanged("UserDefinedLayouts");
            } 
        }

        public ICommand InsertCommand
        {
            get
            {
                if (_insertCommand == null)
                    _insertCommand = new RelayCommand(Insert);

                return _insertCommand;
            }
        }

        public ICommand UpdateCommand
        {
            get
            {
                if (_updateCommand == null)
                    _updateCommand = new RelayCommand(Update);

                return _updateCommand;
            }
        }

        public ICommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                    _deleteCommand = new RelayCommand(Delete);

                return _deleteCommand;
            }
        }

        public ICommand CopyCommand
        {
            get
            {
                if (_copyCommand == null)
                    _copyCommand = new RelayCommand(Copy);

                return _copyCommand;
            }
        }

        public ICommand SaveUpdateCommand
        {
            get
            {
                if (_saveUpdateCommand == null)
                    _saveUpdateCommand = new RelayCommand(SaveUpdate);

                return _saveUpdateCommand;
            }
        }

        public ICommand CancelUpdateCommand
        {
            get
            {
                if (_cancelUpdateCommand == null)
                    _cancelUpdateCommand = new RelayCommand(CancelUpdate);

                return _cancelUpdateCommand;
            }
        }

        public Type EntityType { get; protected set; }
        public EntityDataContextBase DataContext { get; protected set; }
        public bool LoadAllData { get; set; }
        public bool IsAllDataLoaded { get; set; }

        public abstract object CurrentRow { get; set; }
        public abstract void LoadLayout(EntDbUserDefinedLayout userLayout);

        public void ChangeActiveEntity(Entity oldEntity)
        {
            if (IsEditing)
                CancelUpdate(oldEntity);
        }

        public void SaveUpdate(object param)
        {
            if (param == null) return;

            BaseEntity newEntity = (BaseEntity)CurrentRow;
            string strInvalidEntryMessage = ValidateUserEntry(newEntity);
            if (string.IsNullOrEmpty(strInvalidEntryMessage))
            {
                GridControl grid = (GridControl)param;
                BaseEntity currentEntity = (BaseEntity)grid.GetFocusedRow();
                SaveEntity(currentEntity, newEntity);

                IsEditing = false;
            }
            else
            {
                MessageBox.Show(strInvalidEntryMessage, INVALID_ENTRY_WARNING_CAPTION,
                    MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private string ValidateUserEntry(BaseEntity newEntity)
        {
            string strMessage = null;
            string strInvalidEntryMandatory = string.Empty;
            string strInvalidEntryUnique = string.Empty;
            string strInvalidEntryValidity = string.Empty;
            string strInvalidEntryExtended = string.Empty;

            //Get the entity information
            EntityTableMapping tableMap = DataContext.EntityTableMappings[EntityType];
            if (tableMap != null)
            {                
                PropertyInfo[] arrInfo = EntityType.GetProperties();
                foreach (PropertyInfo info in arrInfo)
                {
                    object objValue = info.GetValue(newEntity, null);
                        
                    EntityColumnMapping columnMap = tableMap.ColumnMappings[info];
                    if (columnMap.IsMandatory) //check mandatory fields
                    {
                        if (objValue == null || string.IsNullOrEmpty(objValue.ToString()))
                        {
                            if (string.IsNullOrEmpty(strInvalidEntryMandatory)) 
                                strInvalidEntryMandatory = INVALID_ENTRY_MANDATORY_TEXT + "\n";
                            strInvalidEntryMandatory += info.Name + "\n";
                        }
                    }
                    if (columnMap.IsUnique) //check unique fields
                    {
                        if (!IsEntityUnique(newEntity, info))
                        {
                            if (string.IsNullOrEmpty(strInvalidEntryUnique))
                                strInvalidEntryMandatory = INVALID_ENTRY_UNIQUE_TEXT + "\n";
                            strInvalidEntryUnique += info.Name + "\n";
                        }
                    }
                }                
            }

            //check validity period
            if (typeof(ValidityBaseEntity).IsAssignableFrom(newEntity.GetType()))
            {
                ValidityBaseEntity entity = (ValidityBaseEntity)newEntity;
                if (entity.ValidFrom.HasValue && entity.ValidTo.HasValue)
                {
                    if (entity.ValidFrom.Value >= entity.ValidTo.Value)
                        strInvalidEntryValidity = INVALID_ENTRY_VALIDITY_TEXT + "\n"; 
                }
            }

            strMessage = strInvalidEntryMandatory + strInvalidEntryUnique + strInvalidEntryValidity;
            strInvalidEntryExtended = ExtendedEntityValidation(newEntity);
            if (!string.IsNullOrEmpty(strInvalidEntryExtended))
            {
                strMessage += strInvalidEntryExtended;
            }

            return strMessage;
        }

        protected abstract bool IsEntityUnique(BaseEntity entity, PropertyInfo propertyInfo);

        protected virtual string ExtendedEntityValidation(BaseEntity newEntity)
        {
            return string.Empty;
        }

        protected abstract void SaveEntity(BaseEntity currentEntity, BaseEntity newEntity);

        public void CancelUpdate(object param)
        {
            if (param == null) return;

            BaseEntity baseEntity;
            if (param is GridControl)
            {
                GridControl grid = (GridControl)param;
                baseEntity = (BaseEntity)grid.GetFocusedRow();
            }
            else
            {
                baseEntity = (BaseEntity)param;
            }

            if (baseEntity.Urno <= 0) //if it is a new row, delete it
                RemoveEntity(baseEntity);

            IsEditing = false;
        }

        protected abstract void RemoveEntity(BaseEntity entity);

        public void Insert(object param)
        {
            InsertEntity(param);

            int intNewRow = LocateNewRow(HpDxGrid.CurrentGrid);
            if (intNewRow >= 0)
                HpDxGrid.CurrentGrid.View.MoveFocusedRow(intNewRow);

            CurrentRow = HpDxGrid.CurrentGrid.GetFocusedRow();
            IsEditing = true;
        }

        protected abstract void InsertEntity(object param);

        public void Update(object param)
        {
            BaseEntity baseEntity = (BaseEntity)HpDxGrid.CurrentGrid.GetFocusedRow();
            if (baseEntity == null) return;

            CurrentRow = baseEntity;
            IsEditing = true;
        }

        public void Delete(object param)
        {
            BaseEntity baseEntity = (BaseEntity)HpDxGrid.CurrentGrid.GetFocusedRow();
            if (baseEntity == null) return;

            MessageBoxResult result = MessageBox.Show(DELETE_WARNING_TEXT, DELETE_WARNING_CAPTION,
                MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No);
            if (result == MessageBoxResult.Yes)
            {
                DeleteEntity(baseEntity);
            }
        }

        protected abstract void DeleteEntity(BaseEntity entity);

        public void Copy(object param)
        {
            BaseEntity baseEntity = (BaseEntity)HpDxGrid.CurrentGrid.GetFocusedRow();
            if (baseEntity == null) return;

            Insert(baseEntity);
        }

        private int LocateNewRow(GridControl grid)
        {
            //check first row
            int intRow = 0;
            if (!IsNewRow(grid, intRow))
            {
                //check last row
                intRow = grid.VisibleRowCount - 1;
                if (!IsNewRow(grid, intRow))
                {
                    //check other rows
                    intRow = -1;
                    for (int i = 1; i < grid.VisibleRowCount - 1; i++)
                    {
                        if (IsNewRow(grid, i))
                        {
                            intRow = i;
                            break;
                        }
                    }
                }
            }

            return intRow;
        }

        private bool IsNewRow(GridControl grid, int rowIndex)
        {
            bool bNewRow = false;

            int intRowHandle = grid.GetRowHandleByVisibleIndex(rowIndex);
            if (!grid.IsGroupRowHandle(intRowHandle))
            {
                BaseEntity baseEntity = (BaseEntity)grid.GetRow(intRowHandle);
                if (baseEntity.Urno <= 0)
                    bNewRow = true;
            }

            return bNewRow;
        }
    }
}
