﻿#region ===== Source Code Information =====
/*
Project Name    : Ufis.Entities
Project Type    : Class Library
Platform        : Microsoft Visual C#.NET
File Name       : entlcAllocationPlan.cs

Version         : 1.0.0
Created Date    : 16 - Feb - 2012
Complete Date   : 16 - Feb - 2012
Created By      : Phyoe Khaing Min

Date Reason Updated By
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------

Copyright (C) All rights reserved.
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Entities;

namespace Ufis.AllocationPlan.Entities
{
    public class entlcAllocationPlan : Entity
    {
        private string _Arrival;
        public string Arrival
        {
            get { return _Arrival; }
            set
            {
                if (_Arrival != value)
                {
                    _Arrival = value;
                    OnPropertyChanged("Arrival");
                }
            }
        }
        private string _From;
        public string From
        {
            get { return _From; }
            set
            {
                if (_From != value)
                {
                    _From = value;
                    OnPropertyChanged("From");
                }
            }
        }
        private string _STA;
        public string STA
        {
            get { return _STA; }
            set
            {
                if (_STA != value)
                {
                    _STA = value;
                    OnPropertyChanged("STA");
                }
            }
        }
        private string _Departure;
        public string Departure
        {
            get { return _Departure; }
            set
            {
                if (_Departure != value)
                {
                    _Departure = value;
                    OnPropertyChanged("Departure");
                }
            }
        }
        private string _STD;
        public string STD
        {
            get { return _STD; }
            set
            {
                if (_STD != value)
                {
                    _STD = value;
                    OnPropertyChanged("STD");
                }
            }
        }
        private string _To;
        public string TO
        {
            get { return _To; }
            set
            {
                if (_To != value)
                {
                    _To = value;
                    OnPropertyChanged("TO");
                }
            }
        }
        private string _Type;
        public string Type
        {
            get { return _Type; }
            set
            {
                if (_Type != value)
                {
                    _Type = value;
                    OnPropertyChanged("Type");
                }
            }
        }
        private string _Reg;
        public string Reg
        {
            get { return _Reg; }
            set
            {
                if (_Reg != value)
                {
                    _Reg = value;
                    OnPropertyChanged("Reg");
                }
            }
        }
        private string _Stand;
        public string Stand
        {
            get { return _Stand; }
            set
            {
                if (_Stand != value)
                {
                    _Stand = value;
                    OnPropertyChanged("Stand");
                }
            }
        }
        private string _Counter;
        public string Counter
        {
            get { return _Counter; }
            set
            {
                if (_Counter != value)
                {
                    _Counter = value;
                    OnPropertyChanged("Counter");
                }
            }
        }
        private string _Gate;
        public string Gate
        {
            get { return _Gate; }
            set
            {
                if (_Gate != value)
                {
                    _Gate = value;
                    OnPropertyChanged("Gate");
                }
            }
        }
        private string _Belt;
        public string Belt
        {
            get { return _Belt; }
            set
            {
                if (_Belt != value)
                {
                    _Belt = value;
                    OnPropertyChanged("Belt");
                }
            }
        }
        private string _APax;        
        public string APax
        {
            get { return _APax; }
            set
            {
                if (_APax != value)
                {
                    _APax = value;
                    OnPropertyChanged("APax");
                }
            }
        }
        private string _DPax;
        public string DPax
        {
            get { return _DPax; }
            set
            {
                if (_DPax != value)
                {
                    _DPax = value;
                    OnPropertyChanged("DPax");
                }
            }
        }

    }
}
