﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Xpf.Core;
using System.Windows.Controls;

namespace Ufis.AllocationPlan.Helpers
{
    public class HpRowDetailContainerControl : ContentControl
    {
        public HpRowDetailContainerControl()
        {
            this.SetDefaultStyleKey(typeof(HpRowDetailContainerControl));
        }
    }
}
