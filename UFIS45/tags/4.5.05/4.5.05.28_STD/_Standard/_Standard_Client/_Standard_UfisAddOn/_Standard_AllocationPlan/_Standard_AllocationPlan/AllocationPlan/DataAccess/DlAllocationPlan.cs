﻿using System;
using Ufis.Data;
using System.Linq;
using Ufis.Entities;
using Ufis.Data.Ceda;
using System.Collections.Generic;

namespace Ufis.AllocationPlan.DataAccess
{
    public class DlAllocationPlan
    {
        /// <summary>
        /// Load Flight List by date Range for Arrival
        /// </summary>
        /// <param name="sFDate">System.String containing sFDate</param>
        /// <param name="sTdate">System.String containing sTdate</param>
        /// <returns>EntityCollection for Flight</returns>
        public static EntityCollectionBase<EntDbFlight> LoadArrivalFlightListByDateRange(string sFDate, string sTdate)
        {
            const string FIELD_LIST = "[RelationKey],[Urno],[FullFlightNumber],[OriginAirportIATACode],[StandardTimeOfArrival],[AircraftICAOCodeModified],[AircraftIATACode]," +
                                        "[RegistrationNumber],[PositionOfArrival],[GateOfArrival1],[Belt1],[TotalNumberOfPassengers]";
            string WHERE_CLAUSE = String.Format("WHERE [OperationalType] NOT IN ('T','G') AND [StandardTimeOfArrival] BETWEEN '{0}' AND '{1}'", sFDate, sTdate);

            EntityCollectionBase<EntDbFlight> objFlightList = null;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbFlight),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
            };

            objFlightList = dataContext.OpenEntityCollection<EntDbFlight>(command);

            return objFlightList;
        }
        /// <summary>
        /// Get Specific Flight Info by Relation Key (RKEY)
        /// </summary>
        /// <param name="lRKEY">system.long containing lRKEY</param>
        /// <returns>EntityCollection for Flight</returns>
        public static EntityCollectionBase<EntDbFlight> GetDepartureFlightByRKEY(int lRKEY, int lUrno)
        {
            const string FIELD_LIST = "[RelationKey],[Urno],[FullFlightNumber],[DestinationAirportIATACode],[StandardTimeOfDeparture],[AircraftICAOCodeModified],[AircraftIATACode]," +
                                        "[RegistrationNumber],[PositionOfDeparture],[GateOfDeparture1],[Belt1],[TotalNumberOfPassengers]";
            string WHERE_CLAUSE = String.Format("WHERE [RelationKey] = {0} AND [Urno] <> {1}", lRKEY, lUrno);

            EntityCollectionBase<EntDbFlight> objRKeyFlightList = null;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbFlight),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
            };

            objRKeyFlightList = dataContext.OpenEntityCollection<EntDbFlight>(command);

            return objRKeyFlightList;
        }

        /// <summary>
        /// Load Flight List by date Range for Departure
        /// </summary>
        /// <param name="sFDate">System.String containing sFDate</param>
        /// <param name="sTdate">System.String containing sTdate</param>
        /// <returns>EntityCollection for Flight</returns>
        public static EntityCollectionBase<EntDbFlight> LoadDepartureFlightListByDateRange(string sFDate, string sTdate)
        {
            const string FIELD_LIST = "[RelationKey],[Urno],[FullFlightNumber],[DestinationAirportIATACode],[StandardTimeOfDeparture],[AircraftICAOCodeModified],[AircraftIATACode],"+
                                        "[RegistrationNumber],[PositionOfDeparture],[GateOfArrival1],[Belt1],[TotalNumberOfPassengers]";
            string WHERE_CLAUSE = String.Format("WHERE [OperationalType] NOT IN ('T','G') AND [StandardTimeOfDeparture] BETWEEN '{0}' AND '{1}' ", sFDate, sTdate);

            EntityCollectionBase<EntDbFlight> objDepFlightList = null;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbFlight),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
            };

            objDepFlightList = dataContext.OpenEntityCollection<EntDbFlight>(command);

            return objDepFlightList;
        }
        /// <summary>
        /// Get Check In Counter Allocation
        /// </summary>
        /// <param name="sFlightNo">System.string containing sFlightNo</param>
        /// <param name="lFlightUrno">System.string containing sFlightUrno</param>
        /// <returns>Return Entity Collection object of EntDbCheckInCounterAllocation</returns>
        public static EntityCollectionBase<EntDbCheckInCounterAllocation> GetCheckInCounter(string sFlightNo, int lFlightUrno)
        {
            const string FIELD_LIST = "[FLightNo],[FLightURNo],[CheckInCounter],[FixCheckInCounter],[TerminalCheckInCounter]";
            string WHERE_CLAUSE = String.Format("WHERE [FLightNo] = '{0}' AND [FLightURNo] <> {1} ", sFlightNo, lFlightUrno);

            EntityCollectionBase<EntDbCheckInCounterAllocation> objCheckInCounter = null;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbCheckInCounterAllocation),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
            };

            objCheckInCounter = dataContext.OpenEntityCollection<EntDbCheckInCounterAllocation>(command);

            return objCheckInCounter;
        }
        /// <summary>
        /// Get All Check In Counter Allocation
        /// </summary>
        /// <returns>Return Entity Collection object of EntDbCheckInCounterAllocation</returns>
        public static EntityCollectionBase<EntDbCheckInCounterAllocation> GetCheckInCounters()
        {
            const string FIELD_LIST = "[FLightNo],[FLightURNo],[CheckInCounter],[FixCheckInCounter],[TerminalCheckInCounter]";           

            EntityCollectionBase<EntDbCheckInCounterAllocation> objCheckInCounters = null;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbCheckInCounterAllocation),
                AttributeList = FIELD_LIST
            };

            objCheckInCounters = dataContext.OpenEntityCollection<EntDbCheckInCounterAllocation>(command);

            return objCheckInCounters;
        }
    }
}