﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ufis.Status_Manager.Util
{
    public class UtilString
    {
        public static List<string> SplitBySize(string stToBeSplit, int maxSize)
        {
            List<string> lsSt = new List<string>();
            int stLen = stToBeSplit.Length;
            int cnt = stLen / maxSize;
            if ((cnt * maxSize) < stLen) cnt++;

            int startPos = 0;
            int lenToCopy = maxSize;
            for (int i = 0; i < cnt; i++)
            {
                if ((i+1==cnt) && (startPos + lenToCopy > stLen)) lenToCopy = stLen - startPos;
                lsSt.Add(stToBeSplit.Substring(startPos, lenToCopy));
                startPos += maxSize;
            }

            return lsSt;
        }
    }
}
