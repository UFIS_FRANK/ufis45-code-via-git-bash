using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;
using System.Threading;

using Ufis.Data;
using Ufis.Utils;

using Ufis.Status_Manager.DB;
using Ufis.Status_Manager.DS;
using Ufis.Status_Manager.UI;

namespace Ufis.Status_Manager.Ctrl
{
    public class CtrlOst
    {
        private static CtrlOst _this = null;

        private CtrlOst() { }

        public static CtrlOst GetInstance()
        {
            if (_this == null) _this = new CtrlOst();
            return _this;
        }

        public void LoadOstData()
        {
            ArrayList arr = CtrlFlight.GetUAftArrStForSelection();
            int cnt = arr.Count;
            IDatabase myDB = UT.GetMemDB();
            ITable myOstTable = myDB["OST"];

            for (int i = 0; i < cnt; i++)
            {
                myOstTable.Load(" WHERE UAFT IN (" + arr[i].ToString() + ")");
            }
        }
    }
}
