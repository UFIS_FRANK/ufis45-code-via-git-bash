﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ufis.Status_Manager.Ent
{
    public class EntSymbol
    {
        public enum EntSymbolShapeEnum
        {
            Circle,
            Square
        }

        public int Radius { get; set; }
        public EntSymbolShapeEnum Shape { get; set; }

        public EntSymbol(int radius, EntSymbolShapeEnum shape)
        {
            this.Radius = radius;
            this.Shape = shape;
        }

        public EntSymbol(): this(0, EntSymbolShapeEnum.Circle)
        {

        }
    }
}

