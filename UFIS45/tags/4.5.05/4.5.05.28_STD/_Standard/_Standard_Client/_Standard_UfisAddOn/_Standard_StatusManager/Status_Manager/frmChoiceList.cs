using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using Ufis.Utils;
using Ufis.Data;
using Microsoft.Win32;

namespace Ufis.Status_Manager.UI
{
	/// <summary>
	/// Summary description for frmChoiceList.
	/// </summary>
	public class frmChoiceList : System.Windows.Forms.Form
	{
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Button btnOK;
		public System.Windows.Forms.Label lblCaption;
		public AxTABLib.AxTAB tabList;
		private System.Windows.Forms.Button btnClose;
		public string currTable = "";
		public string myHeaderString = "";
		public bool isGroup = false;
		public bool isAddEmptyLine = true;
		private System.Windows.Forms.Panel panelBottom;
		private System.Windows.Forms.Panel panelFill;
		private System.Windows.Forms.Button btnPrint;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.ComponentModel.IContainer components;
		private string myCaption = "";
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.ContextMenu mnuContext;
		private System.Windows.Forms.MenuItem menuItemReactivate;
		private System.Windows.Forms.CheckBox cbShowDeleted;
		private string myHeaderLenString = "";

		public frmChoiceList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmChoiceList));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblCaption = new System.Windows.Forms.Label();
            this.tabList = new AxTABLib.AxTAB();
            this.btnOK = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnClose = new System.Windows.Forms.Button();
            this.panelBottom = new System.Windows.Forms.Panel();
            this.cbShowDeleted = new System.Windows.Forms.CheckBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.panelFill = new System.Windows.Forms.Panel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.mnuContext = new System.Windows.Forms.ContextMenu();
            this.menuItemReactivate = new System.Windows.Forms.MenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabList)).BeginInit();
            this.panelBottom.SuspendLayout();
            this.panelFill.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(402, 52);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            // 
            // lblCaption
            // 
            this.lblCaption.BackColor = System.Drawing.Color.Black;
            this.lblCaption.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCaption.ForeColor = System.Drawing.Color.Lime;
            this.lblCaption.Location = new System.Drawing.Point(0, 0);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Size = new System.Drawing.Size(402, 23);
            this.lblCaption.TabIndex = 2;
            this.lblCaption.Text = "<Choice list for what>";
            // 
            // tabList
            // 
            this.tabList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabList.Location = new System.Drawing.Point(0, 0);
            this.tabList.Name = "tabList";
            this.tabList.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabList.OcxState")));
            this.tabList.Size = new System.Drawing.Size(402, 485);
            this.tabList.TabIndex = 3;
            this.tabList.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabList_SendLButtonDblClick);
            this.tabList.SendMouseMove += new AxTABLib._DTABEvents_SendMouseMoveEventHandler(this.tabList_SendMouseMove);
            this.tabList.SendRButtonClick += new AxTABLib._DTABEvents_SendRButtonClickEventHandler(this.tabList_SendRButtonClick);
            // 
            // btnOK
            // 
            this.btnOK.BackColor = System.Drawing.Color.Transparent;
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOK.ImageIndex = 1;
            this.btnOK.ImageList = this.imageList1;
            this.btnOK.Location = new System.Drawing.Point(124, 8);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 28);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = false;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.White;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            this.imageList1.Images.SetKeyName(2, "");
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.Transparent;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.ImageIndex = 0;
            this.btnClose.ImageList = this.imageList1;
            this.btnClose.Location = new System.Drawing.Point(204, 8);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 28);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "   &Close";
            this.btnClose.UseVisualStyleBackColor = false;
            // 
            // panelBottom
            // 
            this.panelBottom.Controls.Add(this.cbShowDeleted);
            this.panelBottom.Controls.Add(this.btnPrint);
            this.panelBottom.Controls.Add(this.btnClose);
            this.panelBottom.Controls.Add(this.btnOK);
            this.panelBottom.Controls.Add(this.pictureBox1);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBottom.Location = new System.Drawing.Point(0, 508);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(402, 52);
            this.panelBottom.TabIndex = 6;
            // 
            // cbShowDeleted
            // 
            this.cbShowDeleted.BackColor = System.Drawing.Color.Transparent;
            this.cbShowDeleted.Checked = true;
            this.cbShowDeleted.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShowDeleted.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbShowDeleted.Location = new System.Drawing.Point(8, 12);
            this.cbShowDeleted.Name = "cbShowDeleted";
            this.cbShowDeleted.Size = new System.Drawing.Size(104, 24);
            this.cbShowDeleted.TabIndex = 7;
            this.cbShowDeleted.Text = "&Show Deleted";
            this.cbShowDeleted.UseVisualStyleBackColor = false;
            this.cbShowDeleted.CheckedChanged += new System.EventHandler(this.cbShowDeleted_CheckedChanged);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.Transparent;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrint.ImageIndex = 2;
            this.btnPrint.ImageList = this.imageList1;
            this.btnPrint.Location = new System.Drawing.Point(312, 8);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 27);
            this.btnPrint.TabIndex = 6;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // panelFill
            // 
            this.panelFill.Controls.Add(this.tabList);
            this.panelFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFill.Location = new System.Drawing.Point(0, 23);
            this.panelFill.Name = "panelFill";
            this.panelFill.Size = new System.Drawing.Size(402, 485);
            this.panelFill.TabIndex = 7;
            // 
            // mnuContext
            // 
            this.mnuContext.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemReactivate});
            // 
            // menuItemReactivate
            // 
            this.menuItemReactivate.Index = 0;
            this.menuItemReactivate.Text = "&Reactivate Rule";
            this.menuItemReactivate.Click += new System.EventHandler(this.menuItemReactivate_Click);
            // 
            // frmChoiceList
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(402, 560);
            this.ContextMenu = this.mnuContext;
            this.Controls.Add(this.panelFill);
            this.Controls.Add(this.panelBottom);
            this.Controls.Add(this.lblCaption);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmChoiceList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Choice List ...";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmChoiceList_Closing);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmChoiceList_MouseMove);
            this.Load += new System.EventHandler(this.frmChoiceList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabList)).EndInit();
            this.panelBottom.ResumeLayout(false);
            this.panelFill.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		private void frmChoiceList_Load(object sender, System.EventArgs e)
		{
			int myX=-1, myY=-1, myW=-1, myH=-1;
			string regVal = "";
			myCaption = lblCaption.Text;
			btnOK.Parent = pictureBox1;
			btnClose.Parent = pictureBox1;
			btnPrint.Parent = pictureBox1;
			cbShowDeleted.Parent = pictureBox1;
			cbShowDeleted.Visible = false;

			string theKey = "Software\\StatusManager\\STAT_MGR_CHOICELIST_" + myCaption;
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			if(rk != null)
			{
				regVal = rk.GetValue("X", "-1").ToString();
				myX = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Y", "-1").ToString();
				myY = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Width", "-1").ToString();
				myW = Convert.ToInt32(regVal);
				regVal = rk.GetValue("Height", "-1").ToString();
				myH = Convert.ToInt32(regVal);
				myHeaderLenString = rk.GetValue("HeaderLenString", "").ToString();
			}
			if((myW != -1 && myH != -1) && (myX < Screen.PrimaryScreen.Bounds.Width))
			{
				this.Left = myX;
				this.Top = myY;
				this.Width = myW;
				this.Height = myH;
			}
			LoadData();
		}
		private void LoadData()
		{
			tabList.LifeStyle = true;
			tabList.CursorLifeStyle = true;
			tabList.ShowHorzScroller(true);
			tabList.EnableHeaderSizing(true);
			IDatabase myDB = UT.GetMemDB();
			btnPrint.Visible = false;
			if(isGroup == true)
			{
				ITable myTab = myDB["GRN"];
				tabList.ResetContent();
				tabList.HeaderString = "URNO,GROUPNAME,TABN";
				tabList.LogicalFieldList = tabList.HeaderString;
				tabList.HeaderLengthString = "0,100,100";
				StringBuilder strBuffer = new StringBuilder(10000);				
				string strTab="";
				for(int i = 0; i < myTab.Count; i++)
				{
					IRow row = myTab[i];
					strTab = row["TABN"];
					if(strTab != "")
					{
						strTab = strTab.Substring(0, 3);
						if(strTab == currTable)
						{
							strBuffer.Append(row["URNO"] + "," + row["GRPN"] + "," + row["TABN"] + "\n");
						}
					}
				}
				tabList.InsertBuffer(strBuffer.ToString(), "\n");
				if(isAddEmptyLine == true)
				{
					tabList.InsertTextLineAt(0, ",,", false);
				}
				tabList.AutoSizeByHeader = true;
				tabList.AutoSizeColumns();
			}
			else
			{
				ITable myTab = myDB[currTable];
				lblCaption.Refresh();
				if (myTab != null)
				{
					//tabList.ColumnAlignmentString = myTab.FieldLenString;
					myTab.FillVisualizer(tabList);
					tabList.LogicalFieldList = tabList.HeaderString;
					if(isAddEmptyLine == true)
					{
						string strEmpty = "";
						for(int i = 0 ; i < myTab.FieldList.Count-1; i++)
						{
							strEmpty += ",";
						}
						tabList.InsertTextLineAt(0, strEmpty, false);
					}
					tabList.AutoSizeByHeader = true;
					tabList.AutoSizeColumns();
				}
				//Handle LSTU and CDAT
				int idxLSTU = UT.GetItemNo(tabList.LogicalFieldList, "LSTU");
				int idxCDAT = UT.GetItemNo(tabList.LogicalFieldList, "CDAT");
				if(idxCDAT > -1)
				{
					tabList.DateTimeSetColumnFormat(idxCDAT, "YYYYMMDDhhmmss", "DD'.'MM'.'YY'-'hh':'mm");
				}
				if(idxLSTU > -1)
				{
					tabList.DateTimeSetColumnFormat(idxLSTU, "YYYYMMDDhhmmss", "DD'.'MM'.'YY'-'hh':'mm");
				}
				if(currTable == "SRH")
				{
					cbShowDeleted.Visible = true;
					ReorgSRH();
				}
				if(currTable == "HSS")
				{
					tabList.Sort("1", true, true);
				}
			}
			string [] arrHeaderLen = tabList.HeaderLengthString.Split(',');
			string [] arrFields = tabList.LogicalFieldList.Split(',');
			string strNewLenString="";
			for(int rc = 0; rc < arrHeaderLen.Length && rc < arrFields.Length; rc++)
			{
				string strFld = arrHeaderLen[rc];
				if(arrFields[rc] == "URNO")
				{
					strFld = "0";
				}
				if( (rc+1) == arrHeaderLen.Length)
				{
					strNewLenString += strFld;
				}
				else
				{
					strNewLenString += strFld + ",";
				}
			}
			if(strNewLenString != "")
				tabList.HeaderLengthString = strNewLenString;

			if(myHeaderLenString != "")
			{
				//tabList.HeaderLengthString = myHeaderLenString;
			}
			lblCaption.Text = myCaption + " --- " + tabList.GetLineCount().ToString() + " Records";
			mnuContext.MenuItems[0].Visible = false;
		}

		/// <summary>
		/// If it's the choice list for Rule header, then 
		/// resolve the URNOs to readable values
		/// </summary>
		void ReorgSRH()
		{
			btnPrint.Visible = true;
			ITable myTab = null;
			int cnt = tabList.GetLineCount();
			IDatabase myDB = UT.GetMemDB();
			string strColor = UT.colBlue + "," + UT.colBlue;
			tabList.CursorDecoration(tabList.LogicalFieldList, "B,T", "2,2", strColor);
			tabList.DefaultCursor = false;
			tabList.CursorLifeStyle = false;

			//UHSS,ACTG,ACTU,ALTG,ALTU,APTG,APTU,NATG,NATU
								//" URNO,NAME,ADID,UHSS,          ACTG,   ACTU,ALTG,     ALTU,  APTG,      APTU,  NATG,      NATU,CDAT,LSTU,USEC,USEU", 
			tabList.HeaderString = "Urno,Name,Flt,Status-Section,A/C-Grp.,A/C,Airl.-Grp.,Airl.,Airp.-Grp.,Airport,Nature-Grp,Nature,Creation,Lstu,Creator,User Lstu,DelFlag,Max-Ground";
			int i = 0;
			for( i = 0; i < cnt; i++)
			{
				myTab = myDB["HSS"];
				IRow [] rows = myTab.RowsByIndexValue("URNO", tabList.GetFieldValue(i, "UHSS"));
				if(rows.Length > 0)
				{
					tabList.SetFieldValues(i, "UHSS", rows[0]["NAME"]);
				}
				myTab = myDB["GRN"];
				rows = myTab.RowsByIndexValue("URNO", tabList.GetFieldValue(i, "ACTG"));
				if(rows.Length > 0)
				{
					tabList.SetFieldValues(i, "ACTG", rows[0]["GRPN"]);
				}
				myTab = myDB["ACT"];
				rows = myTab.RowsByIndexValue("URNO", tabList.GetFieldValue(i, "ACTU"));
				if(rows.Length > 0)
				{
					tabList.SetFieldValues(i, "ACTU", rows[0]["ACT3"]+"/"+rows[0]["ACT5"]);
				}
				myTab = myDB["ALT"];
				rows = myTab.RowsByIndexValue("URNO", tabList.GetFieldValue(i, "ALTU"));
				if(rows.Length > 0)
				{
					tabList.SetFieldValues(i, "ALTU", rows[0]["ALC2"]+"/"+rows[0]["ALC3"]);
				}
				myTab = myDB["GRN"];
				rows = myTab.RowsByIndexValue("URNO", tabList.GetFieldValue(i, "ALTG"));
				if(rows.Length > 0)
				{
					tabList.SetFieldValues(i, "ALTG", rows[0]["GRPN"]);
				}
				//
				myTab = myDB["APT"];
				rows = myTab.RowsByIndexValue("URNO", tabList.GetFieldValue(i, "APTU"));
				if(rows.Length > 0)
				{
					tabList.SetFieldValues(i, "APTU", rows[0]["APC3"]+"/"+rows[0]["APC4"]);
				}
				myTab = myDB["GRN"];
				rows = myTab.RowsByIndexValue("URNO", tabList.GetFieldValue(i, "APTG"));
				if(rows.Length > 0)
				{
					tabList.SetFieldValues(i, "APTG", rows[0]["GRPN"]);
				}
				//
				myTab = myDB["NAT"];
				rows = myTab.RowsByIndexValue("URNO", tabList.GetFieldValue(i, "NATU"));
				if(rows.Length > 0)
				{
					tabList.SetFieldValues(i, "NATU", rows[0]["TNAM"]);
				}
				myTab = myDB["GRN"];
				rows = myTab.RowsByIndexValue("URNO", tabList.GetFieldValue(i, "NATG"));
				if(rows.Length > 0)
				{
					tabList.SetFieldValues(i, "NATG", rows[0]["GRPN"]);
				}
				if(tabList.GetFieldValue(i, "DELF") == "X")
				{
					tabList.SetLineColor(i, UT.colBlack, UT.colOrange);
				}
			}
			tabList.AutoSizeColumns();
//			int lastW = this.Width;
//			this.Left -= (int)lastW/2;
//			this.Width = this.Width*2;
			if(cbShowDeleted.Checked == false)
			{
				string strDELF="";
				//Remove all deleted rows
				for( i = cnt-1; i >= 0; i--)
				{
					strDELF = tabList.GetFieldValue(i, "DELF");
					if(strDELF == "X")
					{
						tabList.DeleteLine(i);
					}
				}
				tabList.Refresh();
			}
		}
		private void pictureBox1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			UT.DrawGradient(e.Graphics, 90f, pictureBox1, Color.WhiteSmoke, Color.Gray);
		}

		private void tabList_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			if(e.lineNo == -1)
			{
				if (e.colNo == tabList.CurrentSortColumn)
				{
					if( tabList.SortOrderASC == true)
					{
						tabList.Sort(e.colNo.ToString(), false, true);
					}
					else
					{
						tabList.Sort(e.colNo.ToString(), true, true);
					}
				}
				else
				{
					tabList.Sort(e.colNo.ToString(), true, true);
				}
				tabList.AutoSizeColumns();
				tabList.Refresh();
			}
			else
			{
				int txtColor = -1;
				int backColor = -1;
				tabList.GetLineColor(e.lineNo, ref txtColor, ref backColor);
				if(backColor == UT.colOrange)
				{
					MessageBox.Show(this, "This record is marked as deleted and can't be selected", "Attention",
									MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				}
				else
				{
					this.DialogResult = DialogResult.OK;
					this.Close();
				}
			}
		}

		private void btnPrint_Click(object sender, System.EventArgs e)
		{
            //if(currTable == "SRH")
            //{
            //    //tabList.InsertBuffer(tabList.GetBuffer(0, tabList.GetLineCount()-1, "\n"), "\n");
            //    rptRules r = new rptRules(tabList);
            //    frmPreview p = new frmPreview(r);
            //    p.Show();
            //}
		}

		private void tabList_SendMouseMove(object sender, AxTABLib._DTABEvents_SendMouseMoveEvent e)
		{
			if(isGroup == true)
			{ 
				if(e.lineNo >= 0)
				{
					string strTABN = tabList.GetColumnValue(e.lineNo, 2);
					string strUSGR = tabList.GetColumnValue(e.lineNo, 0);
					if(strTABN != "")
					{
						IDatabase myDB = UT.GetMemDB();
						if(strTABN == "ACT")
						{
							if(strUSGR != "")
							{
								ITable myTab = myDB["SGM"];
								IRow [] rows = myTab.RowsByIndexValue("USGR", strUSGR);
								if(rows.Length > 0)
								{
									string strTip="";
									ITable myACT = myDB["ACT"];
									int cnt = 0;
									for(int i = 0; i < rows.Length; i++)
									{
										IRow [] rowsACT = myACT.RowsByIndexValue("URNO", rows[i]["UVAL"]);
										if(rowsACT.Length > 0)
										{
											if((cnt % 25 == 0) && cnt != 0)
											{
												strTip += "\n";
											}
											strTip += rowsACT[0]["ACT3"]+",";
											cnt++;
										}
									}
									if(strTip != "")
										toolTip1.SetToolTip(tabList, strTip);
									else
										toolTip1.SetToolTip(tabList, "<None>");

								}
								else 
									toolTip1.SetToolTip(tabList, "<None>");
							}
						}
						if(strTABN == "APT")
						{
							if(strUSGR != "")
							{
								ITable myTab = myDB["SGM"];
								IRow [] rows = myTab.RowsByIndexValue("USGR", strUSGR);
								if(rows.Length > 0)
								{
									string strTip="";
									ITable myRefTab = myDB["APT"];
									int cnt = 0;
									for(int i = 0; i < rows.Length; i++)
									{
										IRow [] rowsACT = myRefTab.RowsByIndexValue("URNO", rows[i]["UVAL"]);
										if(rowsACT.Length > 0)
										{
											if((cnt % 25 == 0) && cnt != 0)
											{
												strTip += "\n";
											}
											strTip += rowsACT[0]["APC3"]+",";
											cnt++;
										}
									}
									if(strTip != "")
										toolTip1.SetToolTip(tabList, strTip);
									else
										toolTip1.SetToolTip(tabList, "<None>");
								}
								else 
									toolTip1.SetToolTip(tabList, "<None>");

							}
						}
						if(strTABN == "ALT")
						{
							if(strUSGR != "")
							{
								ITable myTab = myDB["SGM"];
								IRow [] rows = myTab.RowsByIndexValue("USGR", strUSGR);
								if(rows.Length > 0)
								{
									string strTip="";
									ITable myRefTab = myDB["ALT"];
									int cnt = 0;
									for(int i = 0; i < rows.Length; i++)
									{
										IRow [] rowsACT = myRefTab.RowsByIndexValue("URNO", rows[i]["UVAL"]);
										if(rowsACT.Length > 0)
										{
											if((cnt % 25 == 0) && cnt != 0)
											{
												strTip += "\n";
											}
											strTip += rowsACT[0]["ALC2"]+"-"+rowsACT[0]["ALC3"]+",";
											cnt++;
										}
									}
									if(strTip != "")
										toolTip1.SetToolTip(tabList, strTip);
									else
										toolTip1.SetToolTip(tabList, "<None>");
								}
								else 
									toolTip1.SetToolTip(tabList, "<None>");
							}
						}
						if(strTABN == "NAT")
						{
							ITable myTab = myDB["SGM"];
							IRow [] rows = myTab.RowsByIndexValue("USGR", strUSGR);
							if(rows.Length > 0)
							{
								string strTip="";
								ITable myRefTab = myDB["NAT"];
								int cnt = 0;
								for(int i = 0; i < rows.Length; i++)
								{
									IRow [] rowsACT = myRefTab.RowsByIndexValue("URNO", rows[i]["UVAL"]);
									if(rowsACT.Length > 0)
									{
										if((cnt % 25 == 0) && cnt != 0)
										{
											strTip += "\n";
										}
										strTip += rowsACT[0]["TNAM"]+",";
										cnt++;
									}
								}
								if(strTip != "")
									toolTip1.SetToolTip(tabList, strTip);
								else
									toolTip1.SetToolTip(tabList, "<None>");
							}
							else 
								toolTip1.SetToolTip(tabList, "<None>");
						}
					}
				}
			}
		}

		private void frmChoiceList_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string theKey = "Software\\StatusManager\\STAT_MGR_CHOICELIST_" + myCaption;
			RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			myHeaderLenString = tabList.HeaderLengthString;

			if(rk == null)
			{
				rk = Registry.CurrentUser.OpenSubKey("Software\\StatusManager", true);
				theKey = "STAT_MGR_CHOICELIST_" + myCaption;
				rk.CreateSubKey(theKey);
				rk.Close();
				theKey = "Software\\StatusManager\\STAT_MGR_CHOICELIST_" + myCaption;
				rk = Registry.CurrentUser.OpenSubKey(theKey, true);
			}

			rk.SetValue("X", this.Left.ToString());
			rk.SetValue("Y", this.Top.ToString());
			rk.SetValue("Width", this.Width.ToString());
			rk.SetValue("Height", this.Height.ToString());
			rk.SetValue("HeaderLenString", myHeaderLenString);
		}

		private void menuItemReactivate_Click(object sender, System.EventArgs e)
		{
			int idx = tabList.GetCurrentSelected();
			if(idx > -1)
			{
				ITable srhTAB = null;
				IDatabase myDB = UT.GetMemDB();
				srhTAB = myDB["SRH"];
				string strUrno = "";
				strUrno = tabList.GetFieldValue(idx, "URNO");
				IRow [] rows = srhTAB.RowsByIndexValue("URNO", strUrno);
				if(rows.Length > 0)
				{
					rows[0]["DELF"] = "1";
					rows[0].Status = State.Modified;
					srhTAB.Save();
					tabList.SetLineColor(idx, UT.colBlack, UT.colWhite);
					tabList.Refresh();
					mnuContext.MenuItems[0].Visible = false;
				}
			}
		}

		private void tabList_SendRButtonClick(object sender, AxTABLib._DTABEvents_SendRButtonClickEvent e)
		{
			mnuContext.MenuItems[0].Visible = false;
			if(e.lineNo > -1)
			{
				if(currTable == "SRH")
				{
					int txtCol = -1;
					int bkCol = -1;
					tabList.GetLineColor(e.lineNo, ref txtCol, ref bkCol);
					if(bkCol == UT.colOrange)
					{
						tabList.SetCurrentSelection(e.lineNo);
						mnuContext.MenuItems[0].Visible = true;
					}
				}
			}
		}

		private void frmChoiceList_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			mnuContext.MenuItems[0].Visible = false;
		}

		private void pictureBox1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			mnuContext.MenuItems[0].Visible = false;
		}

		private void cbShowDeleted_CheckedChanged(object sender, System.EventArgs e)
		{
			LoadData();
		}



	}
}
