﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Ufis.Status_Manager.Ent
{
    public class EntPosition
    {
        public string Name { get; set; }
        public Point CenterPoint { get; set; }
        public int Radius { get; set; }
        public double Degrees { get; set; }
    }

    public class EntPositions : List<EntPosition>
    {
        public EntPosition FindByName(string name)
        {
            foreach (EntPosition pos in this)
            {
                if (pos.Name == name)
                    return pos;
            }
            return null;
        }

        public EntPositions(): this(false, null)
        {
        }

        public EntPositions(bool autoGenerate, AxTABLib.AxTAB tab): this(autoGenerate, tab, 60)
        {
        }

        public EntPositions(bool autoGenerate, AxTABLib.AxTAB tab, int radius)
        {
            if (autoGenerate)
            {
                for (int i = 0; i < tab.GetLineCount(); i++)
                {
                    String olPos = tab.GetFieldValue(i, "POS");
                    int x = -1;
                    int y = -1;
                    String strX = tab.GetFieldValue(i, "X");
                    String strY = tab.GetFieldValue(i, "Y");
                    String strAlign = tab.GetFieldValue(i, "ALIGN");
                    if (strX != "" && strY != "")
                    {
                        x = Convert.ToInt32(strX);
                        y = Convert.ToInt32(strY);

                        EntPosition pos = new EntPosition();
                        pos.Name = olPos;
                        pos.CenterPoint = new Point(x, y);
                        pos.Radius = radius;
                        pos.Degrees = GetDegrees(strAlign);

                        this.Add(pos);
                    }
                }
            }
        }

        public static double GetDegrees(string align)
        {
            double degrees = 0;
            
            switch (align)
            {
                case "0"://225°
                    degrees = 225;
                    break;
                case "1"://270°  |
                    //           \/
                    degrees = 270;
                    break;
                case "2":// 315°
                    degrees = 315;
                    break;
                case "3"://0° <---
                    degrees = 0;
                    break;
                case "4": //45°
                    degrees = 45;
                    break;
                case "5"://90°
                    degrees = 90;
                    break;
                case "6"://135°
                    degrees = 135;
                    break;
                case "7"://180°  --->
                    degrees = 180;
                    break;
                default:
                    break;
            }

            return degrees;
        }
    }
}
