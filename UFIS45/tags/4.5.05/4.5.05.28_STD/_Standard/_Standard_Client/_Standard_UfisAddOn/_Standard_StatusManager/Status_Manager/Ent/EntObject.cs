﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Ufis.Status_Manager.Ent
{
    public class EntObject
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FuncCode { get; set; }
        public string WorkgroupCode { get; set; }
        public EntObjectGroup Group { get; set; }
        public Point Location { get; set; }
        public EntPosition Position { get; set; }

        public void GenerateLocation()
        {
            Random rnd = new Random();
            int xDist = rnd.Next(-this.Position.Radius, this.Position.Radius);
            int yDist = rnd.Next(-this.Position.Radius, this.Position.Radius);

            GenerateLocation(xDist, yDist);
        }

        public void GenerateLocation(int xDist, int yDist)
        {
            if (this.Position != null)
            {
                int x = this.Position.CenterPoint.X + xDist;
                int y = this.Position.CenterPoint.Y + yDist;
                this.Location = new Point(x, y);
            }
        }

        public override string ToString()
        {
            return base.ToString() + ";" + this.Id.ToString();
        }
    }

    public class EntObjectCollection : List<EntObject>
    {
        private string[] staffNames =
            new string[]
            {
                "Lim Hing Jau",
                "Law Teo Hock",
                "Loh Ngiap Pheng",
                "Junita Ortega Nee Sammy",
                "Mohamed Mansoor M Kassim",
                "Mohd Fadzil B Mohd Abd Kader",
                "Chia Ting Siong",
                "Tamil Mani s/o Perumal",
                "Ong Adler , Ong Ah Ong",
                "Mun Mun",
                "Jasman B Togi",
                "Mak Wing Fatt",
                "Yeo Poh Seng",
                "Liew Siew Phing",
                "Azmi Bin Abu Masod",
                "Goh Chwee Siong",
                "Sanwari B Ali",
                "Ma Tze Khiong",
                "Fung Chee Seng",
                "Mohd Zainis",
                "Sahroni Bin Tasumi",
                "Surjit Singh",
                "Chan Soo How",
                "Mohd Noorshah B Hamila Maricar",
                "Chung Siew Yin Anthony",
                "Misman B Mahat",
                "Hardev Singh",
                "Yazid Bin Ibrahim",
                "Yong",
                "Fook Ann",
                "Shahfudin Bin Ahmad",
                "De Rozario Dennis Dominic",
                "Lim Peng Koon",
                "LEE",
                "Abdul Rahim B Wahab",
                "Tey Kang Ngen",
                "Jumat B Ismail",
                "Mohamed Noor Bin Ali",
                "Lim Tse Kwang",
                "Jumari Bin Madari",
                "Teo Kim Kiat",
                "Hardial Singh",
                "MOHD",
                "Lim Hong Kim",
                "Ho Beng Chye",
                "Ong Chuan Kiat",
                "Ganesan A/L Kaliaperumal",
                "Tan Kian Hoe",
                "Yee Lee Peng",
                "R Shankar A/L Ramalingam",
                "Thiah Kok Keong",
                "R Veerappan",
                "Riduwan Bin Ismail",
                "Rifin Bin Ahmad",
                "Mahful Bin Abdullah",
                "Mohammad Ali Bin Abdul Fattar",
                "Chandrasegaran S/O Marges",
                "Nooraznan B Mohamed",
                "Zailani Bin Patni",
                "Lim Wah Bin",
                "Tan Lee Seng",
                "Pong Sin Jong",
                "Mohamed Kamari",
                "Albert Vincent A/L Joseph V",
                "Jasni B Shariff",
                "Chee Kuat Fah",
                "Tan Kai Soon",
                "Mohamad Moktar B Abdullah",
                "Zainuddin",
                "Azman Bin Sarbini",
                "Chua Choon Wan",
                "Sargunan A/L P Muthusamy",
                "P Segar",
                "Cheng Liang Soon",
                "Nasim Ahmad A/L Ali Hassan",
                "Jibril Allan Cicero Sammy@Allan Cicero S",
                "GANAPATHY",
                "Selvakumar A/L Ramayam",
                "Latiff Bin Hassan",
                "Imran Bin Senin",
                "Ku Wei Cheng",
                "Chea Ah Huat",
                "Yong Kem Hoe",
                "Mohamad Anuar B Sapadi",
                "Md Nasir Maricar s/o Md Ishak",
                "Mohammad",
                "Md Shah Bin Hj Kadir",
                "Kwa Cheng Siong",
                "Yichun",
                "Chai Fook Soon",
                "Hassan Bin Moksen",
                "Imran Bin Salleh",
                "Amir Khan",
                "Idris Bin Salleh",
                "Lee Chee Wai",
                "Lim Weng Kee",
                "Lim Kim Foo",
                "Jmial B Aripin ,Jamal B Aripin",
                "Voon Nyuk Lin",
                "Idris Bin Abdullah"
            };

        string[] equNames =
            new string[]
            {
                "LR211 *scrapped",
                "LR212",
                "LR213",
                "WT007",
                "WT008",
                "WT009",
                "WT010",
                "WT011",
                "WT012",
                "SL141",
                "SL142",
                "SL143",
                "SL144",
                "SL145",
                "SL150",
                "SL146",
                "SL147",
                "SL148",
                "SL149",
                "SL151",
                "SL152",
                "SL153",
                "SL154",
                "SL240",
                "SL241",
                "SL242",
                "SL243",
                "MDL02",
                "LR214",
                "LR215",
                "LR216",
                "LR217",
                "LR218",
                "LR219",
                "LR220",
                "LR221",
                "LR222",
                "LR223",
                "LR224",
                "LR228",
                "LR229",
                "LR230",
                "LR231",
                "LR232",
                "LR233",
                "LR234",
                "LR235",
                "LR236",
                "LR237",
                "LR238",
                "LR241",
                "LR242",
                "LR243",
                "LR244",
                "LR245",
                "LR246",
                "LR247",
                "LR248",
                "LR249",
                "LR250",
                "LR251",
                "LR252",
                "LR253",
                "MDL04 *scrapped",
                "MDL05  ¿sch_Laid-Up¿",
                "MDL06",
                "MDL07",
                "MDL08",
                "MDL09",
                "MDL10",
                "MDL12",
                "MDL13",
                "MDL11",
                "PS22",
                "PS23  vvip",
                "PS24",
                "PS25 *scrapped",
                "PS26 *scrapped",
                "PS61",
                "PS62 vvip",
                "PS63",
                "PS64",
                "PS65",
                "PS66",
                "PS67",
                "PS68",
                "PS69",
                "PS70",
                "PS71",
                "PS72",
                "SL105 *scrapped",
                "SL106 *scrapped",
                "SL107 *scrapped",
                "SL108 *scrapped",
                "SL112 *scrapped",
                "SL114 *scrapped",
                "SL212",
                "SL213 *scrapped",
                "SL215",
                "SL216"
            };

        public EntObject FindById(int id)
        {
            foreach (EntObject obj in this)
            {
                if (obj.Id == id)
                    return obj;
            }
            return null;
        }

        public void GenerateObjects(EntPositions positions, EntObjectGroupCollection groups)
        {
            Random rnd = new Random();
            int staffCount = staffNames.Length;
            int equCount = equNames.Length;
            int posCount = positions.Count;
            
            int objId = 0, staffIdx = 0, equIdx = 0;                    
            foreach (EntObjectGroup group in groups)
            {
                for (int objNo = 0; objNo < group.OptNumbers; objNo++)
                {
                    string name;

                    EntObject obj = new EntObject();
                    obj.Id = objId;
                    if (group.Type.Id == 0)
                    {//staff
                        name = staffNames[staffIdx];
                        staffIdx++;                        
                    }
                    else
                    {//equipment
                        name = equNames[equIdx];
                        equIdx++;                        
                    }
                    obj.Name = name;
                    obj.Position = positions[rnd.Next(0, posCount - 1)];
                    obj.Group = group;
                    obj.GenerateLocation();

                    this.Add(obj);

                    objId++;
                }
            }
        }

        public EntObjectCollection(): this(false, null, null)
        {
        }

        public EntObjectCollection(bool autoGenerate, EntPositions positions): this(autoGenerate, positions, null)
        {
        }

        public EntObjectCollection(bool autoGenerate, EntPositions positions, EntObjectGroupCollection groups)
        {
            if (autoGenerate)
            {
                if (groups == null)
                {
                    groups = new EntObjectGroupCollection(true, null);
                }

                GenerateObjects(positions, groups);
            }
        }
    }
}
