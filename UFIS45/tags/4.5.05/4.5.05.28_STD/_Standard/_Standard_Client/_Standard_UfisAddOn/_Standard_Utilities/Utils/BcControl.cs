using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace Ufis.Data
{
	/// <summary>
	/// Summary description for BcControl.
	/// </summary>
	public class BcControl : System.Windows.Forms.UserControl
	{
		public delegate void BroadcastHandler(string pReqId, string pDest1, string pDest2,string pCmd, string pObject, string pSeq,string pTws, string pTwe, string pSelection, string pFields, string pData, string pBcNum);

		private	BcHandle bcHandle = null;
		private System.Windows.Forms.ListView listViewBroadcasts;
		private const int WM_BCADD = 1040;
		private string		application;
		private	string		homeAirport;
		private string		tableExtension; 

		struct BcTableHandler
		{
			public	BroadcastHandler	onBroadcast;
			public	string				table;
			public	string				command;
			public	string				application;
			public	bool				useOwnBc;
			public	bool				onlyOwnBc;
		};
		
		private	SortedList	tableMap = new SortedList();


		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public	BroadcastHandler	OnBroadcast;

		public string Application
		{
			set
			{
				this.application = value;
			}
		}

		public string HomeAirport
		{
			set
			{
				this.homeAirport = value;
			}
		}

		public string TableExtension
		{
			set
			{
				this.tableExtension = value;
			}
		}

		public void RegisterObject(BroadcastHandler broadcast,string table,string command,bool ownBc,string appl)
		{
			BcTableHandler handler = new BcTableHandler();
			handler.onBroadcast  = broadcast;
			handler.table		 = table;
			handler.command		 = command;
			handler.useOwnBc	 = ownBc;
			handler.application	 = application;

			AddHandlerToMap(handler);
		}

		public void UnregisterObject(string table,string command)
		{
			BcTableHandler handler = new BcTableHandler();
			handler.table		 = table;
			handler.command		 = command;

			RemoveHandlerFromMap(handler);
		}

		public void SetFilterRange(string table,string sFromField,string sToField,string sFromValue,string sToValue)
		{

		}

		public BcControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				this.bcHandle.UnRegisterBcForm(this);
				this.bcHandle.CleanUpCom();

				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.listViewBroadcasts = new System.Windows.Forms.ListView();
			this.SuspendLayout();
			// 
			// listViewBroadcasts
			// 
			this.listViewBroadcasts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.listViewBroadcasts.FullRowSelect = true;
			this.listViewBroadcasts.GridLines = true;
			this.listViewBroadcasts.HoverSelection = true;
			this.listViewBroadcasts.Location = new System.Drawing.Point(8, 8);
			this.listViewBroadcasts.Name = "listViewBroadcasts";
			this.listViewBroadcasts.Size = new System.Drawing.Size(976, 696);
			this.listViewBroadcasts.TabIndex = 0;
			this.listViewBroadcasts.View = System.Windows.Forms.View.Details;
			// 
			// BcControl
			// 
			this.Controls.Add(this.listViewBroadcasts);
			this.Name = "BcControl";
			this.Size = new System.Drawing.Size(992, 712);
			this.Load += new System.EventHandler(this.BcControl_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void AddHandlerToMap(BcTableHandler handler)
		{
			ArrayList tableHandler = null;

			int ind = this.tableMap.IndexOfKey(handler.table);
			if (ind < 0)
			{
				tableHandler = new ArrayList();
				this.tableMap[handler.table] = tableHandler;
			}
			else
			{
				tableHandler = (ArrayList)this.tableMap.GetByIndex(ind);
			}

			tableHandler.Add(handler);
		}

		private void RemoveHandlerFromMap(BcTableHandler handler)
		{
			ArrayList tableHandler = null;

			int ind = this.tableMap.IndexOfKey(handler.table);
			if (ind >= 0)
			{
				tableHandler = (ArrayList)this.tableMap.GetByIndex(ind);
				for (int i = 0; i < tableHandler.Count; i++)
				{
					BcTableHandler current = (BcTableHandler)tableHandler[i];
					if (current.command == handler.command)
					{
						tableHandler.RemoveAt(i);
						if (tableHandler.Count == 0)
						{
							this.tableMap.RemoveAt(ind);
						}
						return;
					}
				}
			}

			tableHandler.Add(handler);
		}

		private void BcControl_Load(object sender, System.EventArgs e)
		{
			this.bcHandle = new BcHandle(this.application);
			this.bcHandle.SetHomeAirport(this.homeAirport);
			this.bcHandle.SetTableExtension(this.tableExtension);

			if (!this.bcHandle.Initialize(null,null))
			{
				System.Windows.Forms.MessageBox.Show(this.bcHandle.LastError());
			}
			this.bcHandle.RegisterBcForm(this);	
		
			this.listViewBroadcasts.Columns.Add("BcNum",50,HorizontalAlignment.Left);
			this.listViewBroadcasts.Columns.Add("Req",50,HorizontalAlignment.Left);
			this.listViewBroadcasts.Columns.Add("Dest1",100,HorizontalAlignment.Left);
			this.listViewBroadcasts.Columns.Add("Dest2",100,HorizontalAlignment.Left);
			this.listViewBroadcasts.Columns.Add("Cmd",100,HorizontalAlignment.Left);
			this.listViewBroadcasts.Columns.Add("Object",100,HorizontalAlignment.Left);
			this.listViewBroadcasts.Columns.Add("Seq",100,HorizontalAlignment.Left);
			this.listViewBroadcasts.Columns.Add("Tws",100,HorizontalAlignment.Left);
			this.listViewBroadcasts.Columns.Add("Twe",100,HorizontalAlignment.Left);
			this.listViewBroadcasts.Columns.Add("Selection",100,HorizontalAlignment.Left);
			this.listViewBroadcasts.Columns.Add("Fields",100,HorizontalAlignment.Left);
			this.listViewBroadcasts.Columns.Add("Data",100,HorizontalAlignment.Left);
			this.listViewBroadcasts.Refresh();
		
		}

		[System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name="FullTrust")]
		protected override void WndProc(ref Message m) 
		{
			// Listen for operating system messages.
			switch (m.Msg)
			{
					// The WM_ACTIVATEAPP message occurs when the application
					// becomes the active application or becomes inactive.
				case WM_BCADD:
					ArrayList olBcArray = new ArrayList();
					this.bcHandle.GetBc((int)m.WParam,olBcArray);
					if (this.listViewBroadcasts.Items.Count >= 500)
						this.listViewBroadcasts.Items.Clear();

					ArrayList tableHandler = null;

					for (int i = 0; i < olBcArray.Count; i++)
					{
						BcHandle.BCDATA data = (BcHandle.BCDATA)olBcArray[i];
						ListViewItem item = this.listViewBroadcasts.Items.Insert(0,new BroadcastItem(data));
						item.SubItems.Add(data.ReqId);
						item.SubItems.Add(data.Dest1);
						item.SubItems.Add(data.Dest2);
						item.SubItems.Add(data.Cmd);
						item.SubItems.Add(data.Object);
						item.SubItems.Add(data.Seq);
						item.SubItems.Add(data.Tws);
						item.SubItems.Add(data.Twe);
						item.SubItems.Add(data.Selection);
						item.SubItems.Add(data.Fields);
						item.SubItems.Add(data.Data);


						int ind = this.tableMap.IndexOfKey(data.Object);
						if (ind >= 0)
						{
							tableHandler = (ArrayList)this.tableMap.GetByIndex(ind);
							for (int j = 0; j < tableHandler.Count; j++)
							{
								BcTableHandler current = (BcTableHandler)tableHandler[j];
								if (current.command == data.Cmd)
								{
									current.onBroadcast(data.ReqId,data.Dest1,data.Dest2,data.Cmd,data.Object,data.Seq,data.Tws,data.Twe,data.Selection,data.Fields,data.Data,data.BcNum.ToString());
									break;
								}
							}
						}

						if (this.OnBroadcast != null)
						{
							this.OnBroadcast(data.ReqId,data.Dest1,data.Dest2,data.Cmd,data.Object,data.Seq,data.Tws,data.Twe,data.Selection,data.Fields,data.Data,data.BcNum.ToString());
						}
					}
					break;                
			}
			base.WndProc(ref m);
		}

	}

	public class BroadcastItem : System.Windows.Forms.ListViewItem
	{
		public readonly short	localBcNum;
		public BroadcastItem(BcHandle.BCDATA data) : base(data.BcNum.ToString(),-1)
		{
			localBcNum = data.BcNum;	
		}
	}
}
