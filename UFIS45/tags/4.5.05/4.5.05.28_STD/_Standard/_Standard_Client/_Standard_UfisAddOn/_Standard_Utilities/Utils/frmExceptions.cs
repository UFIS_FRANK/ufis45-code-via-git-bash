using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Ufis.Utils
{
	/// <summary>
	/// frmException is a windows form's dialog. This dialog can be used to show
	/// all exceptions, which occured during runtime of an application. Ufis.Utils provides
	/// an array, where all Ufis.Utils internal exceptions are stored. The application
	/// programmer can use the <see cref="UT.AddException"/> method to add his own 
	/// application exceptions to the storage array, which is displayed by this class in
	/// a grid. 
	/// </summary>
	/// <example>
	///	<code>
	///	[C#]
	///	Ufis.Utils.frmException exDlg = new Ufis.Utils.frmException();
	///	exDlg.ShowDialog(this);
	/// </code>
	/// </example>
	/// <remarks>
	/// This class should be used for debugging purposes in order to get further information
	/// about runtime onsite at the customer. As a suggestion the application should read
	/// a specific entry from the application's configuration file to show/hide the functionality
	/// of this dialog. To add exceptions to the inner list, which is used by the
	/// <B>frmExceptions</B> class pleas refer to the description of <see cref="UT.AddException"/>
	/// method.
	/// </remarks>
	public class frmExceptions : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button btnReset;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private AxTABLib.AxTAB tabExceptions;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// Default constructor.
		/// </summary>
		/// <remarks>none.</remarks>
		public frmExceptions()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		/// <summary>
		/// Cleanup any resources being used.
		/// </summary>
		/// <param name="disposing">Threaded indicator.</param>
		/// <remarks>none.</remarks>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmExceptions));
			this.btnReset = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.tabExceptions = new AxTABLib.AxTAB();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabExceptions)).BeginInit();
			this.SuspendLayout();
			// 
			// btnReset
			// 
			this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnReset.Location = new System.Drawing.Point(248, 16);
			this.btnReset.Name = "btnReset";
			this.btnReset.Size = new System.Drawing.Size(80, 24);
			this.btnReset.TabIndex = 2;
			this.btnReset.Text = "&Reset";
			this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
			// 
			// btnClose
			// 
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnClose.Location = new System.Drawing.Point(336, 16);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(80, 24);
			this.btnClose.TabIndex = 3;
			this.btnClose.Text = "&Close";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnReset);
			this.panel1.Controls.Add(this.btnClose);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 334);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(664, 48);
			this.panel1.TabIndex = 4;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.tabExceptions);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(664, 334);
			this.panel2.TabIndex = 5;
			// 
			// tabExceptions
			// 
			this.tabExceptions.ContainingControl = this;
			this.tabExceptions.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabExceptions.Location = new System.Drawing.Point(0, 0);
			this.tabExceptions.Name = "tabExceptions";
			this.tabExceptions.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabExceptions.OcxState")));
			this.tabExceptions.Size = new System.Drawing.Size(664, 334);
			this.tabExceptions.TabIndex = 0;
			// 
			// frmExceptions
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(664, 382);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Name = "frmExceptions";
			this.Text = "Application Exception ...";
			this.Load += new System.EventHandler(this.frmExceptions_Load);
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabExceptions)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void frmExceptions_Load(object sender, System.EventArgs e)
		{
			char c = (char)15;
			tabExceptions.ResetContent();
			tabExceptions.HeaderString = "Module,Method,Description,Parameter";
			tabExceptions.HeaderLengthString = "150,150,150,300";
			tabExceptions.LogicalFieldList = tabExceptions.HeaderString;
			tabExceptions.SetFieldSeparator(c.ToString());
			tabExceptions.ShowHorzScroller(true);
			tabExceptions.EnableHeaderSizing(true);
			foreach(string myS in UT.arrExceptions)
			{
				tabExceptions.InsertTextLine(myS, true);
			}
			tabExceptions.AutoSizeByHeader = true;
			tabExceptions.AutoSizeColumns();
		}

		private void btnReset_Click(object sender, System.EventArgs e)
		{
			UT.arrExceptions.Clear();
			tabExceptions.ResetContent();
			tabExceptions.Refresh();
		}
	}
}
