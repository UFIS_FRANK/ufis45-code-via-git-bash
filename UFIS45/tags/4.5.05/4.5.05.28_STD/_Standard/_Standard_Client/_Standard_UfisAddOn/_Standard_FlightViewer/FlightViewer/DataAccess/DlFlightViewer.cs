﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Data;
using Ufis.Entities;
using Ufis.Data.Ceda;

namespace FlightViewer.DataAccess
{
    public class DlFlightViewer
    {
        /// <summary>
        /// Filtered Flight viewer list
        /// </summary>
        /// <param name="sSeason">System.String containing sSeason. </param>
        /// <param name="sflightList">System.String containing sFlightList</param>
        /// <param name="sAirportlist">System.String containing sAirportList</param>
        /// <returns>Return EntityCollectionBase(EntDbSeasonalFlightSchedule)</returns>
        public static EntityCollectionBase<EntDbSeasonalFlightSchedule> GetFlightViewerList(string sSeasonName, string sValidFrom, string sValidTo,
                                string sAirportInfo, string sAirportEI, string sFlightInfo, string sFlightEI, char cAirport, char cFlight)
        {
            const string FIELD_LIST = "[SeasonName],[FullFlightNumberOfArrival],[FullFlightNumberOfDeparture]," +
                                      "[ValidFrom],[ValidTo],[DaysOfOperation],[Frequency],[AircraftIATACode]," +
                                      "[OriginAirportIATACode],[ViaAirportIATACodeOfArrival],[StandardTimeOfArrival]," +
                                      "[StandardTimeOfDeparture],[NightsStop],[ViaAirportIATACodeOfDeparture]," +
                                      "[DestinationAirportIATACode],[NatureCodeOfArrival],[NatureCodeOfDeparture]," +
                                      "[RemarkOfArrival],[Urno],[ServiceTypeOfArrival],[ServiceTypeOfDeparture]";
            string WHERE_CLAUSE = "";
            const string ORDER_BY = "  ORDER BY [FullFlightNumberOfArrival],[ValidFrom],[FullFlightNumberOfDeparture],[ValidTo]";
            //Filter by all criteria
            if ((sSeasonName != null || sValidFrom != null || sValidTo != null) && (sFlightInfo != "" && sAirportInfo != ""))
            {
                switch (cAirport)
                {
                    case 'O':
                        WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND [ValidFrom] BETWEEN '{1}' AND '{2}' AND [ValidTo] BETWEEN '{1}' AND '{2}' AND [OriginAirportIATACode] {3} {4}) "
                        , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sAirportEI, sAirportInfo);
                        break;
                    case 'D':
                        WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND [ValidFrom] BETWEEN '{1}' AND '{2}' AND [ValidTo] BETWEEN '{1}' AND '{2}' AND [DestinationAirportIATACode] {3} {4})"
                        , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sAirportEI, sAirportInfo);
                        break;
                    default:
                        if (sAirportEI == "IN(")
                        {
                            WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND [ValidFrom] BETWEEN '{1}' AND '{2}' AND [ValidTo] BETWEEN '{1}' AND '{2}' AND ([OriginAirportIATACode] {3} {4}) OR [DestinationAirportIATACode] {3} {4}))"
                            , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sAirportEI, sAirportInfo);
                            break;
                        }
                        else
                        {
                            WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND [ValidFrom] BETWEEN '{1}' AND '{2}' AND [ValidTo] BETWEEN '{1}' AND '{2}' AND ([OriginAirportIATACode] {3} {4}) AND [DestinationAirportIATACode] {3} {4}))"
                            , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sAirportEI, sAirportInfo);
                            break;
                        }
                        
                }
                switch (cFlight)
                {
                    case 'C':
                        if (sFlightEI == "IN(")
                        {
                            WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND [ValidFrom] BETWEEN '{1}' AND '{2}' AND [ValidTo] BETWEEN '{1}' AND '{2}' AND ([AirlineIATACodeOfDeparture] {3} {4}) OR [AirlineIATACodeOfArrival] {3} {4})) "
                                                        , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sFlightEI, sFlightInfo);
                            break;
                        }
                        else
                        {
                            WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND [ValidFrom] BETWEEN '{1}' AND '{2}' AND [ValidTo] BETWEEN '{1}' AND '{2}' AND ([AirlineIATACodeOfDeparture] {3} {4}) AND [AirlineIATACodeOfArrival] {3} {4})) "
                                                        , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sFlightEI, sFlightInfo);
                            break; 
                        }
                    case 'D':
                        WHERE_CLAUSE = WHERE_CLAUSE + string.Format(" AND [AirlineIATACodeOfDeparture] {0} {1}) ", sFlightEI, sFlightInfo);
                        break;
                    default:
                        WHERE_CLAUSE = WHERE_CLAUSE + string.Format(" AND [AirlineIATACodeOfArrival] {0} {1}) ", sFlightEI, sFlightInfo);
                        break;
                }
                WHERE_CLAUSE = WHERE_CLAUSE + ORDER_BY;

            }
            else if ((sSeasonName != null || sValidFrom != null || sValidTo != null) && (sFlightInfo == "" && sAirportInfo != ""))
            {
                switch (cAirport)
                {
                    case 'O':
                        WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND [ValidFrom] BETWEEN '{1}' AND '{2}' AND [ValidTo] BETWEEN '{1}' AND '{2}' AND [OriginAirportIATACode] {3} {4}) "
                        , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sAirportEI, sAirportInfo);
                        break;
                    case 'D':
                        WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND [ValidFrom] BETWEEN '{1}' AND '{2}' AND [ValidTo] BETWEEN '{1}' AND '{2}' AND [DestinationAirportIATACode] {3} {4})"
                        , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sAirportEI, sAirportInfo);
                        break;
                    default:
                        if (sAirportEI == "IN(")
                        {
                            WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND [ValidFrom] BETWEEN '{1}' AND '{2}' AND [ValidTo] BETWEEN '{1}' AND '{2}' AND ([OriginAirportIATACode] {3} {4}) OR [DestinationAirportIATACode] {3} {4}))"
                            , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sAirportEI, sAirportInfo);
                            break;
                        }
                        else
                        {
                            WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND [ValidFrom] BETWEEN '{1}' AND '{2}' AND [ValidTo] BETWEEN '{1}' AND '{2}' AND ([OriginAirportIATACode] {3} {4}) AND [DestinationAirportIATACode] {3} {4}))"
                            , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sAirportEI, sAirportInfo);
                            break;
                        }
                }
                WHERE_CLAUSE = WHERE_CLAUSE + ORDER_BY;
            }
            else if ((sSeasonName != null || sValidFrom != null || sValidTo != null) && (sFlightInfo != "" && sAirportInfo == ""))
            {
                switch (cFlight)
                {
                    case 'C':
                        if (sFlightEI == "IN(")
                        {
                            WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND [ValidFrom] BETWEEN '{1}' AND '{2}' AND [ValidTo] BETWEEN '{1}' AND '{2}' AND ([AirlineIATACodeOfDeparture] {3} {4}) OR [AirlineIATACodeOfArrival] {3} {4})) "
                                                        , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sFlightEI, sFlightInfo);
                            break;
                        }
                        else
                        {
                            WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND [ValidFrom] BETWEEN '{1}' AND '{2}' AND [ValidTo] BETWEEN '{1}' AND '{2}' AND ([AirlineIATACodeOfDeparture] {3} {4}) AND [AirlineIATACodeOfArrival] {3} {4})) "
                                                        , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sFlightEI, sFlightInfo);
                            break;
                        }
                    case 'D':
                        WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND [ValidFrom] BETWEEN '{1}' AND '{2}' AND [ValidTo] BETWEEN '{1}' AND '{2}' AND [AirlineIATACodeOfDeparture] {3} {4}) "
                                                    , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sFlightEI, sFlightInfo);
                        break;
                    default:
                        WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND [ValidFrom] BETWEEN '{1}' AND '{2}' AND [ValidTo] BETWEEN '{1}' AND '{2}' AND [AirlineIATACodeOfArrival] {3} {4}) "
                                                    , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sFlightEI, sFlightInfo);
                        break;
                }
                WHERE_CLAUSE = WHERE_CLAUSE + ORDER_BY;
            }
            else if ((sSeasonName != null || sValidFrom != null || sValidTo != null) && (sFlightInfo == "" && sAirportInfo == ""))
            {
                WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND [ValidFrom] BETWEEN '{1}' AND '{2}' AND [ValidTo] BETWEEN '{1}' AND '{2}' "
                    , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8));

                WHERE_CLAUSE = WHERE_CLAUSE + ORDER_BY;
            }
            else
            {
                WHERE_CLAUSE = "WHERE [Urno] IS NULL";
            }

            EntityCollectionBase<EntDbSeasonalFlightSchedule> FlightViewerList = null;
            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbSeasonalFlightSchedule),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
            };

            FlightViewerList = dataContext.OpenEntityCollection<EntDbSeasonalFlightSchedule>(command);

            return FlightViewerList;
        }

        public static EntityCollectionBase<EntDbSeason> GetSeason()
        {
            //SEAS,VPFR,VPTO
            const string FIELD_LIST = "[Urno],[Name],[ValidFrom],[ValidTo]";

            const string WHERE_CLAUSE = "ORDER BY [ValidFrom] DESC ";

            EntityCollectionBase<EntDbSeason> Season = null;
            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbSeason),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
            };

            Season = dataContext.OpenEntityCollection<EntDbSeason>(command);

            return Season;
        }
    }
}
