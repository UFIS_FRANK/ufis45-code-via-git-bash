Attribute VB_Name = "mdlMain"
Option Explicit

Global gsAppName As String
Global gsUserName As String
Global gsTplUrno As String
' KKH - on 27/12/2006
Global gsTplUrno2 As String
' igu - on 23/06/2009 -> PRM Info Report
Global gsTplUrno3 As String

Global gbDebug As Boolean
Global gbTeamLeaderGroup As Boolean
Global gsDebugToday As String

Global sglUTCOffsetHours As Single

Sub Main()
    Dim strRetLogin As String
    Dim strCommand As String
    
    gsAppName = "InfoPC"
    strCommand = Command()
    If UCase(GetItem(strCommand, 1, ",")) = "DEBUG" Then
        gbDebug = True
        Dim strTime As String
        strTime = GetItem(strCommand, 3, ",")
        If Len(strTime) > 0 And IsDate(strTime) = True Then
             Date = strTime
        End If
    Else
        gbDebug = False
    End If

    'do the login
    InitLoginControl
    If gbDebug = False Then
        strRetLogin = frmSplash.AATLoginControl1.ShowLoginDialog
    Else
        strRetLogin = "OK"
    End If

    ' close connection to server
    frmSplash.UfisCom1.CleanupCom

    'look after the login result
    If strRetLogin = "CANCEL" Or strRetLogin = "ERROR" Then
        End
    Else
        ' save the user name
        gsUserName = frmSplash.AATLoginControl1.GetUserName()

        ' load the data form (not visible)
        Load frmData

        ' check if we are running in debug mode
        If gbDebug = True Then
            gsUserName = GetItem(strCommand, 2, ",")
            frmData.Visible = True
        End If

        ' load the data and create the views
        frmData.InitialLoad True, True

        ' show the main dialog
        frmMain.Show
    End If
End Sub

Private Sub InitLoginControl()
    Dim AppFullVersion As String 'igu on 11/09/2009

    Dim strRegister As String
    ' giving the login control an UfisCom to do the login
    Set frmSplash.AATLoginControl1.UfisComCtrl = frmSplash.UfisCom1

    AppFullVersion = App.Major & "." & App.Minor & ".0." & App.Revision 'igu on 11/09/2009

    ' setting some information to be displayed on the login control
    frmSplash.AATLoginControl1.ApplicationName = gsAppName
    frmSplash.AATLoginControl1.VersionString = AppFullVersion 'igu on 11/09/2009
    'frmSplash.AATLoginControl1.VersionString = "4.5.0.26" 'igu on 11/09/2009
    frmSplash.AATLoginControl1.InfoCaption = "Info about InfoPC"
    frmSplash.AATLoginControl1.InfoButtonVisible = True
    frmSplash.AATLoginControl1.InfoUfisVersion = "UFIS Version 4.5"
    frmSplash.AATLoginControl1.InfoAppVersion = App.Title & " " & AppFullVersion   'igu on 11/09/2009
    'frmSplash.AATLoginControl1.InfoAppVersion = CStr("InfoPC 4.5.0.26") 'igu on 11/09/2009
    frmSplash.AATLoginControl1.InfoCopyright = "� 2001-2005 UFIS Airport Solutions GmbH"
    frmSplash.AATLoginControl1.InfoAAT = "UFIS Airport Solutions GmbH"
    frmSplash.AATLoginControl1.UserNameLCase = False 'not automatic upper case letters
    frmSplash.AATLoginControl1.ApplicationName = "InfoPC"
    frmSplash.AATLoginControl1.LoginAttempts = 3

    ' build the register string for BDPS-SEC
    strRegister = "InfoPC,InitModu,InitModu,Initialize (InitModu),B,-"
    strRegister = strRegister & ",General,Z_JOBS,Display Job Tab,Z,1"
    strRegister = strRegister & ",General,Z_SHIFTS,Display Shift Tab,Z,1"
    strRegister = strRegister & ",General,Z_JOBDETAIL,Open Job Details,Z,1"
    strRegister = strRegister & ",Button,B_STATUS,Status Button,B,1"
    strRegister = strRegister & ",Button,B_TELEX,Telex Viewer Button,B,1"
    strRegister = strRegister & ",Button,B_PRINT,Print Button,B,1"
    strRegister = strRegister & ",Button,B_INFO,Flight Info Button,B,1" 'igu on 01 Oct 2010
    strRegister = strRegister & ",Button,B_TRACK,Track Sheet Button,B,1" 'igu on 15 Dec 2011
    frmSplash.AATLoginControl1.RegisterApplicationString = strRegister
End Sub

Public Sub ExitApplication()
    End
End Sub

'igu on 21/04/2011
'take call sign when FLNO is empty
Public Function GetFlnoOrCsgn(TabObject As TABLib.Tab, ByVal LineNo As Long)
    Dim strRet As String
    
    strRet = TabObject.GetFieldValue(LineNo, "FLNO")
    If strRet = "" Then
        strRet = TabObject.GetFieldValue(LineNo, "CSGN")
    End If
    
    GetFlnoOrCsgn = strRet
End Function

