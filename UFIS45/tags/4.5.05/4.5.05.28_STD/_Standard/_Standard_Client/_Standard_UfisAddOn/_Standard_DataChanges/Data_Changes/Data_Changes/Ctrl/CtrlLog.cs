﻿using System;
using System.Collections.Generic;
using System.Text;

using Ufis.Data;
using Ufis.Utils;

namespace Data_Changes.Ctrl
{
    public class CtrlLog
    {
        static IDatabase _myDB = null;
        static CtrlLog _this = null;

        private CtrlLog() { }

        public static CtrlLog GetInstance()
        {
            if (_this == null) _this = new CtrlLog();
            return _this;
        }


        public const string LOGTAB_LOGICAL_FIELDS = 
            "Flight,Location,Tran,Old Value,New Value,Time of Change,Changed by User,Urno,Keyf,STOX,TANA";
        public const string LOGTAB_FIELD_LENGTHS = 
            "10,10,10,10,10,14,10,10,10,14,10";
        public const string LOGTAB_DB_FIELDS = 
            "UREF,FINA,TRAN,OVAL,VALU,TIME,USEC,URNO,KEYF,STOX,TANA";
        public const string LOGTAB_EXCLUDE_FIELDS = ",URNO,USEC,CDAT,USEU,LSTU,";

        public static bool IsFieldToExcludeForLog(string fldName)
        {
            return LOGTAB_EXCLUDE_FIELDS.Contains("," + fldName + ",");
        }

        public List<string> GetLogFieldsWithoutDefaultFields(List<string> lsFieldList)
        {
            List<string> lsNew = new List<string>();
            foreach (string fld in lsFieldList)
            {
                if (IsFieldToExcludeForLog(fld)) continue;
                lsNew.Add(fld);
            }
            return lsNew;
        }

        /// <summary>
        /// Load Log Data for AFT
        /// </summary>
        /// <param name="stWhereTimeframe"></param>
        /// <param name="logTab"></param>
        /// <param name="lsAdditionalTables"></param>
        /// <returns></returns>
        public bool LoadAFTRelatedLog(string stWhereTimeframe, ITable logTab, out List<string> lsAdditionalTables)
        {
         
            
            bool result = true;
            IDatabase myDB = UT.GetMemDB();
            ITable myAft = myDB["AFT"];
            lsAdditionalTables = null;

            List<string> lsAftUrnosForSelection = CtrlData.GetInstance().GetIDListForSelection("AFT", "URNO", 300, ",", "'");

            CtrlConfig ctrlConfig = CtrlConfig.GetInstance();
            ctrlConfig.LoadConfigInfo();
            LinkInfoToATAble aftLinkInfo = ctrlConfig.GetLinkInfoFor("AFT");
            List<string> lsTables = aftLinkInfo.GetLinkTableList();
            lsAdditionalTables = lsTables;
            Dictionary<string, MapRelation> mapTableAndKeyMap = new Dictionary<string, MapRelation>();
            

            foreach (string tableName in lsTables)
            {
                LinkInfo lInfo = aftLinkInfo.GetLinkInfo(tableName);


                List<string> lsUrnosForSelection = null;
                MapRelation map = GetMap(lsAftUrnosForSelection, lInfo.LinkColumn, tableName, "URNO", out lsUrnosForSelection);

                if (map != null && map.Count > 0)
                {
                    mapTableAndKeyMap.Add(tableName, map);
                    string logTableName = "";
                    List<string> lsLogFields;
                    

                    CtrlSYSTabData.GetInstance().GetLogInfoFor(tableName, out logTableName, out lsLogFields);

                    List<string> lsFieldsToCheck = CtrlData.GetIDsForSelection(lInfo.ListColumnToCheck, 200, ",", "'");
                    if (lsFieldsToCheck == null || lsFieldsToCheck.Count == 0)
                    {
                        lsFieldsToCheck = CtrlData.GetIDsForSelection(lsLogFields, 200, ",", "'");
                    }


                    if (lsFieldsToCheck != null && lsFieldsToCheck.Count > 0)
                    {
                        foreach (string columnsToCheck in lsFieldsToCheck)
                        {
                            string filter="";
                            if (tableName == "FLZ" && columnsToCheck=="FLGU") 
                            {
                                filter = " AND UTYP='G' ";
                            }
                            LoadLinkData(
                                stWhereTimeframe + filter,
                                lsUrnosForSelection, logTab, logTableName, tableName, "Loading Log for " + tableName, columnsToCheck, mapTableAndKeyMap);
                        }
                    }
                }

                
               
            }

            //int cntLog = logTab.Count;
            //for (int i = 0; i < cntLog; i++)
            //{
            //    IRow row = logTab[i];

            //    string table = row["TANA"];
            //    if (mapTableAndKeyMap.ContainsKey(table))
            //    {
            //        try
            //        {
            //            string id = row["Flight"];
            //            row["KeyF"] = mapTableAndKeyMap[table][id];
            //        }
            //        catch (Exception ex)
            //        {
            //            row["KeyF"] = " ";
            //        }
            //    }
            //}


            

            return result;
        }
        
        /// <summary>
        /// Load Log Data for CCA
        /// </summary>
        /// <param name="stWhereTimeframe"></param>
        /// <param name="logTab"></param>
        /// <param name="lsAdditionalTables"></param>
        /// <returns></returns>
        public bool LoadCCARelatedLog(string stWhereTimeframe, ITable logTab, out List<string> lsAdditionalTables,string strAFTURNO)
        {
            bool result = true;
            ITable tmpTab=null;
            
            //tmpTab = myDB["L0G"];
            //tmpTab = myDB.Bind("L0G", "TEMP", "FINA,TRAN,OVAL,VALU,TIME,USEC,URNO,KEYF,STOX,UREF", "10,10,10,10,14,10,10,10,14,10", "FINA,TRAN,OVAL,VALU,TIME,USEC,URNO,KEYF,STOX,UREF");
            

            //SELECT URNO,FLNO,CNAM,FLNU,STOD FROM CCATAB where FLNU in AND CTYP=' '
            IDatabase myDB = UT.GetMemDB();
            myDB.Unbind("CCAR");
            ITable tabCCA = myDB.Bind("CCAR", "CCA", "URNO,FLNO,CNAM,FLNU,STOD", "30,30,30,30,100", "URNO,FLNO,CKIC,FLNU,STOD");
            lsAdditionalTables = null;

            ////List<string> lsAftUrnosForSelection = CtrlData.GetInstance().GetIDListForSelection("AFT", "URNO", 300, ",", "'");
            ////Load CCA tab according to the flight selection
            //foreach (string strURNOAft in lsAftUrnosForSelection)
            //{
            //    tabCCA.Load(String.Format("WHERE {0} IN ({1}) AND CTYP=' '", "FLNU", strURNOAft));
            //}

            if (strAFTURNO.Length > 0)
            {
                tabCCA.Load(String.Format("WHERE {0} IN ({1}) AND CTYP=' '", "FLNU", strAFTURNO));
            }
            List<string> lsUrnosForMapCCA = null;
            Dictionary<string, MapRelation> mapTableAndKeyMap = new Dictionary<string, MapRelation>();

            //map CCA and FlightID
            MapRelation mapCA = CtrlData.GetInstance().GetMap("CCAR", "URNO", "CNAM", 300, ",", "'", out lsUrnosForMapCCA);
            mapTableAndKeyMap.Add("CCAR", mapCA);

            //get the CCAID to load the FLZtab changes
            List<string> lsCCAUrnos = CtrlData.GetInstance().GetIDListForSelection("CCAR", "URNO", 300, ",", "'");

            //getting FLZTAB
            CtrlConfig ctrlConfig = CtrlConfig.GetInstance();
            ctrlConfig.LoadConfigInfo();
            LinkInfoToATAble aftLinkInfo = ctrlConfig.GetLinkInfoFor("CCA");
            List<string> lsTables = aftLinkInfo.GetLinkTableList();
            lsAdditionalTables = lsTables;
            
            List<string> lsUrnosForCCA = null;
            //MapRelation mapAft = GetMap(lsAftUrnosForSelection, "FLNU", "CCA", "URNO", out lsUrnosForCCA);
            List<string> tmpAFtUrnos = new List<string>();
            tmpAFtUrnos.Add(strAFTURNO);

            MapRelation mapAft = GetMap(tmpAFtUrnos, "FLNU", "CCA", "URNO", out lsUrnosForCCA);
            mapTableAndKeyMap.Add("AFT", mapAft);

            foreach (string tableName in lsTables)
            {
                LinkInfo lInfo = aftLinkInfo.GetLinkInfo(tableName);
                List<string> lsUrnosForSelection = null;
                MapRelation map = GetMap(lsCCAUrnos, lInfo.LinkColumn, tableName, "URNO", out lsUrnosForSelection);

                if (map != null && map.Count > 0)
                {
                    mapTableAndKeyMap.Add(tableName, map);
                    List<string> lsTempUrnos = CtrlData.GetInstance().GetIDListForSelection(tableName, "URNO", 300, ",", "'");

                    string logTableName = "";
                    List<string> lsLogFields;
                    CtrlSYSTabData.GetInstance().GetLogInfoFor(tableName, out logTableName, out lsLogFields);
                    List<string> lsFieldsToCheck = CtrlData.GetIDsForSelection(lInfo.ListColumnToCheck, 200, ",", "'");

                    if (lsFieldsToCheck == null || lsFieldsToCheck.Count == 0)
                    {
                        lsFieldsToCheck = CtrlData.GetIDsForSelection(lsLogFields, 200, ",", "'");
                    }

                    if (lsFieldsToCheck != null && lsFieldsToCheck.Count > 0)
                    {
                        foreach (string columnsToCheck in lsFieldsToCheck)
                        {
                            string filter = "";
                            if (tableName == "FLZ" && columnsToCheck == "FLGU")
                            {
                                filter = " AND UTYP='C' ";
                            }
                            /*LoadLinkData(
                                stWhereTimeframe + filter,
                                lsCCAUrnos, logTab, logTableName, tableName, "Loading Log for " + tableName, columnsToCheck, mapTableAndKeyMap);*/

                            LoadLinkData(
                                stWhereTimeframe + filter,
                                lsUrnosForSelection, logTab, logTableName, tableName, "Loading Log for " + tableName, columnsToCheck, mapTableAndKeyMap);
                        }
                    }
                }
            }

            if (logTab.Count > 0)
            {
                logTab = ReplaceUrnoWithDescForCCA(logTab, "CCA", mapTableAndKeyMap, "AFT", true);
            }
            return result;
        }
        /// <summary>
        /// Load Log Data for CCA
        /// </summary>
        /// <param name="stWhereTimeframe"></param>
        /// <param name="logTab"></param>
        /// <param name="lsAdditionalTables"></param>
        /// <returns></returns>
        public ITable LoadCKIRelatedLog(string stWhereTimeframe, ITable logTab, List<string> lstCkiUrno,string logicalfields,string fieldlength,string dbfields)
        {
            bool result = true;
            ITable tmpTab = null;

            Dictionary<string, MapRelation> mapTableAndKeyMap = new Dictionary<string, MapRelation>();


            //getting FLZTAB
            CtrlConfig ctrlConfig = CtrlConfig.GetInstance();
            ctrlConfig.LoadConfigInfo();
            LinkInfoToATAble aftLinkInfo = ctrlConfig.GetLinkInfoFor("CCA");
            List<string> lsTables = aftLinkInfo.GetLinkTableList();
            //lsAdditionalTables = lsTables;


            foreach (string tableName in lsTables)
            {
                LinkInfo lInfo = aftLinkInfo.GetLinkInfo(tableName);
                List<string> lsUrnosForSelection = null;
                //MapRelation map = GetMap(lstCkiUrno, lInfo.LinkColumn, tableName, "URNO", out lsUrnosForSelection);
                MapRelation map = new MapRelation();

                
                if (tableName == "FLZ")
                {

                    map = GetMap(lstCkiUrno, lInfo.LinkColumn, tableName, "URNO", out lsUrnosForSelection);
                }
                //else if (tableName == "ALT")
                //{
                //    //GetMap(tmpAFtUrnos, "FLNU", "CCA", "URNO", out lsUrnosForCCA);
                    
                //    map = GetMap(lstCkiUrno, "FLNU", "CCA", "URNO", out lsUrnosForSelection);
                //}
                //else if (tableName == "GRN")
                //{

                //    map = GetMap(lstCkiUrno, "GURN", "CCA", lInfo.LinkColumn, out lsUrnosForSelection);
                //}
                else
                {
                    map = GetMap(lstCkiUrno, "URNO", "CCA", lInfo.LinkColumn, out lsUrnosForSelection);
                }
               

                if (map != null && map.Count > 0)
                {
                    mapTableAndKeyMap.Add(tableName, map);
                    List<string> lsTempUrnos = CtrlData.GetInstance().GetIDListForSelection(tableName, "URNO", 300, ",", "'");

                    string logTableName = "";
                    List<string> lsLogFields;
                    CtrlSYSTabData.GetInstance().GetLogInfoFor(tableName, out logTableName, out lsLogFields);
                    List<string> lsFieldsToCheck = CtrlData.GetIDsForSelection(lInfo.ListColumnToCheck, 200, ",", "'");

                    if (lsFieldsToCheck == null || lsFieldsToCheck.Count == 0)
                    {
                        lsFieldsToCheck = CtrlData.GetIDsForSelection(lsLogFields, 200, ",", "'");
                    }

                    if (lsFieldsToCheck != null && lsFieldsToCheck.Count > 0)
                    {
                        foreach (string columnsToCheck in lsFieldsToCheck)
                        {
                            string filter = "";
                            if (tableName == "FLZ" && columnsToCheck == "FLGU")
                            {
                                filter = " AND UTYP='C' ";
                            }

                            if (tableName == "ALT" && columnsToCheck == "FLNU")
                            {
                                filter = " AND VALU<>'0' ";
                            }

                            if (tableName == "GRN" && columnsToCheck == "GURN")
                            {
                                filter = " AND VALU<>'0' ";
                            }
                            /*LoadLinkData(
                                stWhereTimeframe + filter,
                                lsCCAUrnos, logTab, logTableName, tableName, "Loading Log for " + tableName, columnsToCheck, mapTableAndKeyMap);*/

                            LoadLinkData(
                                stWhereTimeframe + filter,
                                lsUrnosForSelection, logTab, logTableName, tableName, "Loading Log for " + tableName, columnsToCheck, mapTableAndKeyMap, logicalfields, fieldlength, dbfields);

                            if (tableName == "ALT")
                            {
                                List<string> lstmpUrnos = CtrlData.GetInstance().GetIDListForSelection("CKI", "FLNU", 300, ",", "'");
                                
                                logTab = ReplaceUrnoWithDesc(logTab, "CCA", "FLNU", lstmpUrnos, "ALT", "URNO", "ALC2",false);
                            }

                            else if (tableName == "GRN")
                            {
                                List<string> lstmpUrnos = CtrlData.GetInstance().GetIDListForSelection("CKI", "GURN", 300, ",", "'");

                                logTab = ReplaceUrnoWithDesc(logTab, "CCA", "GURN", lstmpUrnos, "GRN", "URNO", "GRPN", false);
                            }

                            
                        }
                    }
                }
            }

            logTab = ReplaceUrnoWithDescForCCA(logTab, "CCA", mapTableAndKeyMap, "FLZ", false);

            return logTab;
        }
        private Dictionary<string, MapInfo> LoadMapConfigForATable(string strTable)
        {
            CtrlConfig ctrlConfig = CtrlConfig.GetInstance();

            ctrlConfig.LoadConfigInfo();
            LinkInfoToATAble aftLinkInfo = ctrlConfig.GetLinkInfoFor(strTable);
            List<string> lsTables = aftLinkInfo.GetLinkTableList();

            CtrlMapConfig mapConfig = new CtrlMapConfig();
            foreach (string tbName in lsTables)
            {
                LinkInfo lInfo = aftLinkInfo.GetLinkInfo(tbName);
                mapConfig.LoadAllSectionForLink(tbName, lInfo.ListColumnToCheck);
            }

            Dictionary<string, MapInfo> maps = new Dictionary<string, MapInfo>();
            if (mapConfig != null)
            {
                maps = mapConfig.GetAllMapInfo();
            }

            return maps;

        }

        private List<string> GetKeysInLogTable(ITable logTab, string strMainTable,string strlColMain)
        {

            List<string> strUrnos = new List<string>();
            if (logTab != null)
            {
                int cntLog = logTab.Count;
                for (int i = 0; i < cntLog; i++)
                {
                    IRow tempRow = logTab[i];
                    string table = tempRow["TANA"];
                    string field = tempRow["Location"];
                    if (string.IsNullOrEmpty(field))
                    {
                        field = tempRow["FINA"];
                    }

                    if (strMainTable == table && strlColMain == field)
                    {
                        string oldvalue = tempRow["Old Value"];
                        if (string.IsNullOrEmpty(oldvalue))
                        {
                            oldvalue = tempRow["OVAL"];
                        }
                        string newvalue = tempRow["New Value"];
                        if (string.IsNullOrEmpty(newvalue))
                        {
                            oldvalue = tempRow["VALU"];
                        }

                        if (!string.IsNullOrEmpty(oldvalue))
                        {
                            if (!strUrnos.Contains(oldvalue))
                            {
                                strUrnos.Add(oldvalue);
                            }
                        }

                        if (!string.IsNullOrEmpty(newvalue))
                        {
                            if (!strUrnos.Contains(newvalue))
                            {
                                strUrnos.Add(newvalue);
                            }
                        }
                    }
                }
            }
            return strUrnos;
        }


        public ITable ReplaceUrnoWithDescForCCA(ITable logTab, string strTable, Dictionary<string, MapRelation> mapTableAndKeyMap,string mainTable,bool isCounterInclude)
        {
           
            Dictionary<string, MapInfo> maps = LoadMapConfigForATable(strTable);
            if (maps.Count > 0)
            {
                foreach (string mapName in maps.Keys)
                {
                    MapInfo mapInfo = maps[mapName];
                    if (mapInfo != null)
                    {
                        string strlColMain = mapInfo.ColMain;
                        string strMainTable = mapInfo.MainTable;
                        string strLinkTable = mapInfo.Link;
                        string strLinkKey = mapInfo.LinkKey;
                        string strLinkDescCol = mapInfo.LinkDesCol;

                        List<string> strUrnos = GetKeysInLogTable(logTab, strMainTable, strlColMain);

                        //Get the Descrption Table
                        IDatabase myDB = UT.GetMemDB();
                        ITable temp1 = myDB.Bind(strLinkTable, strLinkTable, strLinkKey + "," + strLinkDescCol, "10,128", strLinkKey + "," + strLinkDescCol);
                        foreach (string strURNODesc in strUrnos)
                        {
                            temp1.Load(String.Format("WHERE {0} IN {1}", strLinkKey, strURNODesc));
                        }

                        Dictionary<string, string> strKeyValue = new Dictionary<string, string>();
                        int cntDesc = temp1.Count;
                        for (int i = 0; i < cntDesc; i++)
                        {
                            IRow tempRow = temp1[i];
                            string key = tempRow[strLinkKey];
                            string value = tempRow[strLinkDescCol];
                            if (!strKeyValue.ContainsKey(key))
                            {
                                strKeyValue.Add(key, value);
                            }
                        }

                        int cntLog = logTab.Count;
                        string desc = CtrlSYSTabData.GetInstance().GetFieldDesc(strLinkTable, strLinkDescCol);
                       
                        
                        for (int i = 0; i < cntLog; i++)
                        {
                            try
                            {
                                IRow tempLRow = logTab[i];
                                
                                string table = tempLRow["TANA"];
                                string field = tempLRow["Location"];

                                if (isCounterInclude)
                                {
                                    field = tempLRow["Location"];
                                }
                                else
                                {
                                    field = tempLRow["FINA"];
                                }
                               


                                if (strMainTable == table && strlColMain == field)
                                {
                                    string counterid = tempLRow["Keyf"];

                                    if (string.IsNullOrEmpty(counterid))
                                    {
                                        counterid = tempLRow["KEYF"];
                                    }
                                    
                                    string flight = "";
                                    try
                                    {
                                        //flight = mapTableAndKeyMap["AFT"][counterid];
                                        flight = mapTableAndKeyMap[mainTable][counterid];
                                    }
                                    catch (Exception)
                                    {
                                    }
                                    string oldvalue = tempLRow["Old Value"];
                                    string newvalue = tempLRow["New Value"];
                                    string oldvalueDesc = string.Empty;
                                    if (isCounterInclude)
                                    {
                                        oldvalue = tempLRow["Old Value"];
                                        newvalue = tempLRow["New Value"];
                                        if (!string.IsNullOrEmpty(desc))
                                        {
                                            tempLRow["Field"] = desc;
                                        }
                                    }
                                    else
                                    {
                                        oldvalue = tempLRow["OVAL"];
                                        newvalue = tempLRow["VALU"];
                                        if (!string.IsNullOrEmpty(desc))
                                        {
                                            tempLRow["FINA"] = strLinkDescCol;
                                        }
                                    }
                                    

                                    if (!string.IsNullOrEmpty(oldvalue))
                                    {
                                        try
                                        {
                                            if (strTable == "CCA")
                                            {
                                                oldvalueDesc = strKeyValue[oldvalue];
                                                //Get Counter ID
                                                if (isCounterInclude)
                                                {
                                                    string countername = mapTableAndKeyMap["CCAR"][counterid];
                                                    tempLRow["Old Value"] = countername + "/" + oldvalueDesc;
                                                }
                                                else
                                                {
                                                    tempLRow["OVAL"] = oldvalueDesc;
                                                }
                                            }
                                            else
                                            {
                                                tempLRow["Old Value"] = strKeyValue[oldvalue];
                                            }
                                        }
                                        catch { }
                                    }

                                    if (!string.IsNullOrEmpty(newvalue))
                                    {
                                        try
                                        {
                                            if (strTable == "CCA")
                                            {
                                                oldvalueDesc = strKeyValue[newvalue];
                                                //Get Counter ID
                                                if (isCounterInclude)
                                                {
                                                    string countername = mapTableAndKeyMap["CCAR"][counterid];
                                                    tempLRow["New Value"] = countername + "/" + oldvalueDesc;
                                                }
                                                else
                                                {
                                                    tempLRow["VALU"] =oldvalueDesc;
                                                }
                                            }
                                            else
                                            {
                                                tempLRow["New Value"] = strKeyValue[newvalue];
                                            }
                                        }
                                        catch { }
                                    }

                                    if (strTable == "CCA")
                                    {
                                        if (!string.IsNullOrEmpty(flight))
                                        {
                                            tempLRow["Keyf"] = flight;
                                        }
                                        else
                                        {
                                            logTab.Remove(i);
                                        }

                                        //if (isCounterInclude)
                                        //{
                                        //    string tmpLoc = tempLRow["Location"];
                                        //    if (!string.IsNullOrEmpty(tmpLoc))
                                        //    {
                                        //        logTab.Remove(i);
                                        //    }
                                        //}


                                    }

                                }
                                //logTab.Save(tempLRow);
                            
                            }
                            catch (Exception)
                            {
                                continue;
                            }
                            
                        }
                    }
                }
            }
            return logTab;
        }   

        public ITable LoadOtherDescriptions(ITable logTab,string strTable)
        {
            CtrlConfig ctrlConfig = CtrlConfig.GetInstance();

            ctrlConfig.LoadConfigInfo();
            LinkInfoToATAble aftLinkInfo = ctrlConfig.GetLinkInfoFor(strTable);
            List<string> lsTables = aftLinkInfo.GetLinkTableList();

            CtrlMapConfig mapConfig = new CtrlMapConfig();
            foreach (string tbName in lsTables)
            {
                LinkInfo lInfo = aftLinkInfo.GetLinkInfo(tbName);
                mapConfig.LoadAllSectionForLink(tbName, lInfo.ListColumnToCheck);
            }

            if (mapConfig != null)
            {
                Dictionary<string,MapInfo> maps = mapConfig.GetAllMapInfo();

                if (maps.Count > 0)
                {
                    foreach (string mapName in maps.Keys)
                    {
                        MapInfo mapInfo = maps[mapName];
                        List<string> strUrnos = new List<string>();
                        if (mapInfo != null)
                        {
                            
                            string strlColMain = mapInfo.ColMain;
                            string strMainTable = mapInfo.MainTable;
                            string strLinkTable = mapInfo.Link;
                            string strLinkKey= mapInfo.LinkKey;
                            string strLinkDescCol = mapInfo.LinkDesCol;

                            int cntLog = logTab.Count;
                            for (int i = 0; i < cntLog; i++)
                            {
                                IRow tempRow = logTab[i];
                                string table = tempRow["TANA"];
                                string field = tempRow["Location"];

                                if (strMainTable == table && strlColMain == field)
                                {
                                    string oldvalue = tempRow["Old Value"];
                                    string newvalue = tempRow["New Value"];

                                    if (!string.IsNullOrEmpty(oldvalue))
                                    {
                                        if (!strUrnos.Contains(oldvalue))
                                        {
                                            strUrnos.Add(oldvalue);
                                        }
                                    }

                                    if (!string.IsNullOrEmpty(newvalue))
                                    {
                                        if (!strUrnos.Contains(newvalue))
                                        {
                                            strUrnos.Add(newvalue);
                                        }
                                    }
                                }
                            }
                            logTab = ReplaceUrnoWithDesc(logTab, strMainTable, strlColMain, strUrnos, strLinkTable, strLinkKey, strLinkDescCol,true);
                        }
                    }
                }
            }
            return logTab;
           
        }

        public ITable ReplaceUrnoWithDesc(ITable logTab, string strMainTable, string strlColMain, List<string> strUrnos, string strLinkTable, string strLinkKey, string strLinkDescCol, bool isCounterInclude)
        {
            if (strUrnos.Count > 0)
            { 
                //get the description table
                IDatabase myDB = UT.GetMemDB();
                myDB.Unbind(strLinkTable);
                ITable temp1 = myDB.Bind(strLinkTable, strLinkTable, strLinkKey + "," + strLinkDescCol, "10,128", strLinkKey + "," + strLinkDescCol);
                foreach (string strURNODesc in strUrnos)
                {
                    temp1.Load(String.Format("WHERE {0} IN {1}", strLinkKey, strURNODesc));
                }


                Dictionary<string, string> strKeyValue = new Dictionary<string, string>();
                int cntDesc = temp1.Count;
                for (int i = 0; i < cntDesc; i++)
                {
                    IRow tempRow = temp1[i];
                    string key = tempRow[strLinkKey];
                    string value = tempRow[strLinkDescCol];
                    if (!strKeyValue.ContainsKey(key))
                    {
                        strKeyValue.Add(key, value);
                    }
                }

                
                 int cntLog = logTab.Count;
                 string desc = CtrlSYSTabData.GetInstance().GetFieldDesc(strLinkTable, strLinkDescCol);
                 for (int i = 0; i < cntLog; i++)
                 {
                     IRow tempLRow = logTab[i];
                     string table = tempLRow["TANA"];
                     string field = "";  //FINA

                     if (isCounterInclude)
                     {
                         field = tempLRow["Location"];
                     }
                     else
                     {
                         field = tempLRow["FINA"];
                     }
                     

                     if (strMainTable == table && strlColMain == field)
                     {
                         string oldvalue = "";
                         string newvalue = "";

                         if (isCounterInclude)
                         {
                             oldvalue = tempLRow["Old Value"];
                             newvalue = tempLRow["New Value"];
                         }
                         else
                         {
                             oldvalue = tempLRow["OVAL"];
                             newvalue = tempLRow["VALU"];
                         }

                         if (!string.IsNullOrEmpty(desc))
                         {
                             tempLRow["Field"] = desc;
                         }

                         if (!string.IsNullOrEmpty(oldvalue))
                         {
                             try
                             {
                                 if (isCounterInclude)
                                 {
                                     tempLRow["Old Value"] = strKeyValue[oldvalue];
                                 }
                                 else
                                 {
                                     tempLRow["OVAL"] = strKeyValue[oldvalue];
                                 }
                             }
                             catch { }
                         }

                         if (!string.IsNullOrEmpty(newvalue))
                         {
                             try
                             {
                                 //if ((table == "ALT" && field == "FLNU") || (table == "GRN" && field == "GURN"))
                                 //{
                                 //    if (newvalue == "0")
                                 //    {
                                 //        logTab.Remove(i);
                                 //    }
                                 //}
                                 if (isCounterInclude)
                                 {
                                     tempLRow["New Value"] = strKeyValue[newvalue];
                                 }
                                 else
                                 {
                                     tempLRow["VALU"] = strKeyValue[newvalue];
                                 }
                             }
                             catch { }
                         }                        
                     }
                 }
            }
            return logTab;
        }

        /// <summary>
        /// Load Link Data
        /// </summary>
        /// <param name="stWhereTimeframe">where clause with time frame</param>
        /// <param name="lsUrnos">List of Urnos</param>
        /// <param name="logTable">Log table</param>
        /// <param name="logTableName">Log Table Name</param>
        /// <param name="tableName">Table name (from which the data is logged)</param>
        /// <param name="tableLoadingMsg">Loading Message to show to the user while loading</param>
        /// <param name="fieldNamesForLog">FieldNames for Log</param>
        /// <param name="mapTableAndKeyMap">Map of Table and Key</param>
        public void LoadLinkData(
            string stWhereTimeframe,
            List<string> lsUrnos,
            ITable logTable, string logTableName,
            string tableName, string tableLoadingMsg,
            string fieldNamesForLog,
            Dictionary<string, MapRelation> mapTableAndKeyMap
             )
        {
            string fieldFilter = "";
            if (string.IsNullOrEmpty(fieldNamesForLog))
            {
            }
            else
            {
                if (fieldNamesForLog.Trim() != "")
                {
                    fieldFilter = " AND FINA IN (" + fieldNamesForLog + ")";
                }
            }

            IDatabase myDB = UT.GetMemDB();
            myDB.Unbind("TEMP");

            ITable tempLogTable = myDB.Bind("TEMP",
                logTableName,
                CtrlLog.LOGTAB_LOGICAL_FIELDS,
                CtrlLog.LOGTAB_FIELD_LENGTHS,
                CtrlLog.LOGTAB_DB_FIELDS);

           

            foreach (string urnos in lsUrnos) 
            {
                if (string.IsNullOrEmpty(urnos) || urnos.Trim() == "") continue;
                string st = string.Format("{0} AND TANA='{1}' {2} AND UREF IN ({3})", stWhereTimeframe, tableName, fieldFilter, urnos);
                tempLogTable.Load(st);
            }

            int cntLog = tempLogTable.Count;
            for (int i = 0; i < cntLog; i++)
            {
                IRow tempRow = tempLogTable[i];

                string table = tempRow["TANA"];
                string field = tempRow["FINA"];
                if (mapTableAndKeyMap.ContainsKey(table))
                {
                    string id = "";
                    id = tempRow["Flight"];
                    try
                    {
                        tempRow["Keyf"] = mapTableAndKeyMap[table][id];
                        
                    }
                    catch (Exception)
                    {
                        tempRow["Keyf"] = " ";
                    }

                    try
                    {
                        
                        tempRow["UREF"] = mapTableAndKeyMap[table][id];
                    }
                    catch (Exception)
                    {
                        tempRow["UREF"] = " ";
                    }

                    try
                    {
                        tempRow["Flight"] = mapTableAndKeyMap[table][id];
                    }
                    catch (Exception)
                    {
                    }
                }
                
                    IRow rowNew = logTable.CreateEmptyRow();
                    rowNew.SetFieldValues(tempRow.FieldValues());
                    logTable.Add(rowNew);
               
            }
        }
        /// <summary>
        /// Load Link Data
        /// </summary>
        /// <param name="stWhereTimeframe">where clause with time frame</param>
        /// <param name="lsUrnos">List of Urnos</param>
        /// <param name="logTable">Log table</param>
        /// <param name="logTableName">Log Table Name</param>
        /// <param name="tableName">Table name (from which the data is logged)</param>
        /// <param name="tableLoadingMsg">Loading Message to show to the user while loading</param>
        /// <param name="fieldNamesForLog">FieldNames for Log</param>
        /// <param name="mapTableAndKeyMap">Map of Table and Key</param>
        public void LoadLinkData(
            string stWhereTimeframe,
            List<string> lsUrnos,
            ITable logTable, string logTableName,
            string tableName, string tableLoadingMsg,
            string fieldNamesForLog,
            Dictionary<string, MapRelation> mapTableAndKeyMap,
            string logicalfields,
            string fieldlength,
            string dbfields
             )
        {
            string fieldFilter = "";
            if (string.IsNullOrEmpty(fieldNamesForLog))
            {
            }
            else
            {
                if (fieldNamesForLog.Trim() != "")
                {
                    fieldFilter = " AND FINA IN (" + fieldNamesForLog + ")";
                }
            }

            IDatabase myDB = UT.GetMemDB();
            myDB.Unbind("TEMP");

            ITable tempLogTable = myDB.Bind("TEMP",
                logTableName,
                logicalfields,
                fieldlength,
                dbfields);

            foreach (string urnos in lsUrnos)
            {
                if (string.IsNullOrEmpty(urnos) || urnos.Trim() == "") continue;
                string st = string.Format("{0} AND TANA='{1}' {2} AND UREF IN ({3})", stWhereTimeframe, tableName, fieldFilter, urnos);
                tempLogTable.Load(st);
            }

            int cntLog = tempLogTable.Count;
            for (int i = 0; i < cntLog; i++)
            {
                IRow tempRow = tempLogTable[i];

                string table = tempRow["TANA"];
                string field = tempRow["FINA"];
                if (mapTableAndKeyMap.ContainsKey(table))
                {
                    string id = "";
                    id = tempRow["UREF"];
                    try
                    {

                        tempRow["UREF"] = mapTableAndKeyMap[table][id];
                    }
                    catch (Exception)
                    {
                        tempRow["UREF"] = " ";
                    }

                    try
                    {

                        tempRow["Keyf"] = mapTableAndKeyMap[table][id];
                    }
                    catch (Exception)
                    {
                        tempRow["Keyf"] = " ";
                    }
                    
                }

                IRow rowNew = logTable.CreateEmptyRow();
                rowNew.SetFieldValues(tempRow.FieldValues());
                logTable.Add(rowNew);

            }
        }
        /// <summary>
        /// Get Map of of the Key and value for
        ///   from given Table 'TableName' and select with URNO in given list of urnos
        ///   and return the Map of key, data from refFieldName
        /// </summary>
        /// <param name="lsRefTableUrno"></param>
        /// <param name="refFieldName"></param>
        /// <param name="tableName"></param>
        /// <param name="urnoFieldName"></param>
        /// <param name="lsUrnos"></param>
        /// <returns></returns>
        public MapRelation GetMap(List<string> lsRefTableUrno, string refFieldName,
            string tableName, string urnoFieldName,
            out List<string> lsUrnos)
        {
            MapRelation map = new MapRelation();
            lsUrnos = new List<string>();
            IDatabase myDB = UT.GetMemDB();
            string fieldNames = string.Format("{0},{1}", urnoFieldName, refFieldName);
            myDB.Unbind("TEMP");

            ITable myTable = myDB.Bind("TEMP",
                tableName,
                fieldNames,
                "14,14",
                fieldNames);
            myTable.Clear();

            foreach (string urnos in lsRefTableUrno)
            {
                string st = string.Format("WHERE {0} IN ({1})", refFieldName, urnos);
                myTable.Load(string.Format("WHERE {0} IN ({1})", refFieldName, urnos));
            }

            return CtrlData.GetInstance().GetMap("TEMP", urnoFieldName, refFieldName, 300, ",", "'", out lsUrnos);
         }
        
      
     }
}
