using System;
using Ufis.Data;
using Ufis.Utils;
using Microsoft.Win32;

namespace Data_Changes
{
	/// <summary>
	/// Summary description for DataExchange.
	/// </summary>
	public class DE
	{
		#region --- MyMembers

		static IDatabase myDB = null;
		static ITable    actTAB = null;
		static BCBlinker myBCBlinker = null;

		#endregion -- MyMembers

		static public void InitDBObjects(Ufis.Utils.BCBlinker blinker)
		{
			myBCBlinker = blinker;
			myDB = UT.GetMemDB();
			actTAB = myDB["ACT"];
		}
	}
}
