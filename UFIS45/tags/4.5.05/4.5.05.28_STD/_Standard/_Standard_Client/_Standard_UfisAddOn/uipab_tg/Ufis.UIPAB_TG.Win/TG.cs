using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Ufis.UIPAB_TG.BL.Common;
using Ufis.UIPAB_TG.BL.Entities;

namespace Ufis.UIPAB_TG.Win
{
	//public class TG : System.Windows.Forms.Form
	public class TG : Microsoft.ApplicationBlocks.UIProcess.WindowsFormView
	{
		private DataView dv = null;
		private System.Windows.Forms.DataGrid dataGrid1;
		private System.Windows.Forms.Label lblMy;
		private AxTABLib.AxTAB tabFlights;
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.DataGrid gridResult;
		private System.Windows.Forms.Button btnDoFilter;
		private System.Windows.Forms.TextBox txtFilter;
		private System.Windows.Forms.Label lblCnt;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Button btnInsertRow;
		private System.Windows.Forms.Button btnDelete;
		private DataSet	  ds;


		public TG()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			MyController.DisposeApplication();

			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(TG));
			this.dataGrid1 = new System.Windows.Forms.DataGrid();
			this.tabFlights = new AxTABLib.AxTAB();
			this.lblMy = new System.Windows.Forms.Label();
			this.gridResult = new System.Windows.Forms.DataGrid();
			this.btnDoFilter = new System.Windows.Forms.Button();
			this.txtFilter = new System.Windows.Forms.TextBox();
			this.lblCnt = new System.Windows.Forms.Label();
			this.btnSave = new System.Windows.Forms.Button();
			this.btnInsertRow = new System.Windows.Forms.Button();
			this.btnDelete = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabFlights)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridResult)).BeginInit();
			this.SuspendLayout();
			// 
			// dataGrid1
			// 
			this.dataGrid1.DataMember = "";
			this.dataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGrid1.Location = new System.Drawing.Point(8, 384);
			this.dataGrid1.Name = "dataGrid1";
			this.dataGrid1.Size = new System.Drawing.Size(248, 184);
			this.dataGrid1.TabIndex = 0;
			// 
			// tabFlights
			// 
			this.tabFlights.Location = new System.Drawing.Point(120, 48);
			this.tabFlights.Name = "tabFlights";
			this.tabFlights.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabFlights.OcxState")));
			this.tabFlights.Size = new System.Drawing.Size(624, 312);
			this.tabFlights.TabIndex = 1;
			this.tabFlights.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabFlights_SendLButtonDblClick);
			// 
			// lblMy
			// 
			this.lblMy.BackColor = System.Drawing.Color.Black;
			this.lblMy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblMy.ForeColor = System.Drawing.Color.Lime;
			this.lblMy.Location = new System.Drawing.Point(120, 24);
			this.lblMy.Name = "lblMy";
			this.lblMy.Size = new System.Drawing.Size(624, 23);
			this.lblMy.TabIndex = 2;
			this.lblMy.Text = "Hallo";
			// 
			// gridResult
			// 
			this.gridResult.DataMember = "";
			this.gridResult.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.gridResult.Location = new System.Drawing.Point(272, 384);
			this.gridResult.Name = "gridResult";
			this.gridResult.Size = new System.Drawing.Size(472, 224);
			this.gridResult.TabIndex = 3;
			// 
			// btnDoFilter
			// 
			this.btnDoFilter.Location = new System.Drawing.Point(752, 368);
			this.btnDoFilter.Name = "btnDoFilter";
			this.btnDoFilter.TabIndex = 4;
			this.btnDoFilter.Text = "Do Filter";
			this.btnDoFilter.Click += new System.EventHandler(this.btnDoFilter_Click);
			// 
			// txtFilter
			// 
			this.txtFilter.Location = new System.Drawing.Point(752, 408);
			this.txtFilter.Multiline = true;
			this.txtFilter.Name = "txtFilter";
			this.txtFilter.Size = new System.Drawing.Size(144, 112);
			this.txtFilter.TabIndex = 5;
			this.txtFilter.Text = "textBox1";
			// 
			// lblCnt
			// 
			this.lblCnt.BackColor = System.Drawing.Color.Black;
			this.lblCnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblCnt.ForeColor = System.Drawing.Color.Lime;
			this.lblCnt.Location = new System.Drawing.Point(272, 368);
			this.lblCnt.Name = "lblCnt";
			this.lblCnt.Size = new System.Drawing.Size(464, 16);
			this.lblCnt.TabIndex = 6;
			// 
			// btnSave
			// 
			this.btnSave.Location = new System.Drawing.Point(832, 368);
			this.btnSave.Name = "btnSave";
			this.btnSave.TabIndex = 7;
			this.btnSave.Text = "Save";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// btnInsertRow
			// 
			this.btnInsertRow.Location = new System.Drawing.Point(768, 48);
			this.btnInsertRow.Name = "btnInsertRow";
			this.btnInsertRow.TabIndex = 8;
			this.btnInsertRow.Text = "Insert";
			this.btnInsertRow.Click += new System.EventHandler(this.btnInsertRow_Click);
			// 
			// btnDelete
			// 
			this.btnDelete.Location = new System.Drawing.Point(768, 80);
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.TabIndex = 9;
			this.btnDelete.Text = "Delete";
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			// 
			// TG
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(944, 622);
			this.Controls.Add(this.btnDelete);
			this.Controls.Add(this.btnInsertRow);
			this.Controls.Add(this.btnSave);
			this.Controls.Add(this.lblCnt);
			this.Controls.Add(this.txtFilter);
			this.Controls.Add(this.btnDoFilter);
			this.Controls.Add(this.gridResult);
			this.Controls.Add(this.lblMy);
			this.Controls.Add(this.tabFlights);
			this.Controls.Add(this.dataGrid1);
			this.Name = "TG";
			this.Text = "Training Guide Form 1";
			this.Load += new System.EventHandler(this.TG_Load);
			((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabFlights)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridResult)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void TG_Load(object sender, EventArgs e)
		{
			string header = "";
			string lenstring = "";

			if (!MyController.LoadDatabase())
				return;

			tabFlights.ResetContent();

			
			if (MyController.GetBusinessCaseData("LocationManagement",out this.ds))
			{
				this.ds.AcceptChanges();
				this.dataGrid1.DataSource = this.ds;	
			}

			for(int i = 0; i < ds.Tables["Flight"].Columns.Count; i++)
			{
				if( i+1 < ds.Tables["Flight"].Columns.Count)
				{
					header += ds.Tables["Flight"].Columns[i].Caption + ",";
					lenstring += "120,";
				}
				else
				{
					header += ds.Tables["Flight"].Columns[i].Caption;
					lenstring += "120";
				}
			}
			tabFlights.HeaderString = header;
			tabFlights.LogicalFieldList = header;
			tabFlights.HeaderLengthString = lenstring;
			tabFlights.LineHeight = 18;

			lblMy.Text = "Flights";
			for(int i = 0; i < ds.Tables["Flight"].Rows.Count; i++)
			{
				string str="";
				for(int j = 0; j < ds.Tables["Flight"].Columns.Count; j++)
				{
					switch (ds.Tables["Flight"].Columns[j].DataType.FullName)
					{
						case "System.Int32":
							if( j+1 < ds.Tables["Flight"].Columns.Count)
							{
								if (str.Length > 0)
									str += ',';
								str += ((int)ds.Tables["Flight"].Rows[i][ds.Tables["Flight"].Columns[j]]).ToString();
							}
							break;
						case "System.String":
							if( j+1 < ds.Tables["Flight"].Columns.Count)
							{
								if (str.Length > 0)
									str += ',';
								str += ds.Tables["Flight"].Rows[i][ds.Tables["Flight"].Columns[j]];
							}
							break;
						case "System.DateTime":
							if( j+1 < ds.Tables["Flight"].Columns.Count)
							{
								if (str.Length > 0)
									str += ',';
								str += ds.Tables["Flight"].Rows[i][ds.Tables["Flight"].Columns[j]];
							}
							break;
						default:
//							if (ds.Tables["Flight"].Columns[j].DataType is typeof(Enum))
							if (ds.Tables["Flight"].Rows[i][ds.Tables["Flight"].Columns[j]] != System.DBNull.Value ) {
								byte valu = (byte)ds.Tables["Flight"].Rows[i][ds.Tables["Flight"].Columns[j]];
								if (str.Length > 0)
									str += ',';
								str += Enum.GetName(ds.Tables["Flight"].Columns[j].DataType,valu);
							}
							break;
					}
				}
				tabFlights.InsertTextLine(str, false);
			}
			tabFlights.Refresh();
			lblMy.Text = "Flights: (" + tabFlights.GetLineCount() + ")";

//			if (MyController.GetFlightData(out flight))
//			{
//				this.dataGrid1.DataSource = flight;	
//			}
//			for(int i = 0; i < flight.Flight.Columns.Count; i++)
//			{
//				if( i+1 < flight.Flight.Columns.Count)
//				{
//					header += flight.Flight.Columns[i].Caption + ",";
//					lenstring += "120,";
//					}
//				else
//				{
//					header += flight.Flight.Columns[i].Caption;
//					lenstring += "120";
//				}
//			}
//			tabFlights.HeaderString = header;
//			tabFlights.LogicalFieldList = header;
//			tabFlights.HeaderLengthString = lenstring;
//			tabFlights.LineHeight = 18;
//
//			lblMy.Text = "Flights";
//			for(int i = 0; i < flight.Flight.Rows.Count; i++)
//			{
//				string str="";
//				for(int j = 0; j < flight.Flight.Columns.Count; j++)
//				{
//					if( j+1 < flight.Flight.Columns.Count)
//					{
//						str += flight.Flight.Rows[i][flight.Flight.Columns[j].Caption] + ",";
//					}
//					else
//					{
//						str += flight.Flight.Rows[i][flight.Flight.Columns[j].Caption];
//					}
//				}
//				tabFlights.InsertTextLine(str, false);
//			}
//			tabFlights.Refresh();
//			lblMy.Text = "Flights: (" + tabFlights.GetLineCount() + ")";
		}

		private void tabFlights_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
		{
			string flightID = "";

			if(e.lineNo > -1)
			{
				flightID = tabFlights.GetFieldValue(e.lineNo, "Id");
				DataRow row = null;
//				for(int i = 0; i < flight.Flight.Rows.Count; i++)
//				{
//					string str = (string)flight.Flight.Rows[i]["Id"];
//					if(str == flightID)
//					{
//						row = flight.Flight.Rows[i];
//						i = flight.Flight.Rows.Count;
//					}
//				}
				object find;
				find = (object)flightID;
				bool blF = this.ds.Tables["Flight"].Rows.Contains(find);
				row =  this.ds.Tables["Flight"].Rows.Find(find);
			
				if (row != null)
				{
					MyController.CurrentFlight = row;
					MyController.SelectService("Next");
				}

			}
		}

		private void btnDoFilter_Click(object sender, System.EventArgs e)
		{
			try
			{
				string strFilter = txtFilter.Text;
				dv = new DataView(this.ds.Tables["Flight"], strFilter, "", DataViewRowState.CurrentRows);
				gridResult.DataSource = dv;
				int ilC = dv.Table.Rows.Count;
				for(int i = 0; i < dv.Count; i++)
				{
					DataRowView rv = dv[i];
					DataRow dr = rv.Row;
				}
				lblCnt.Text = dv.Count.ToString();
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			ArrayList errorMessages;
			if (!MyController.Save(this.ds,out errorMessages))
			{
				string msg = "";
				foreach(string errMsg in errorMessages)
				{
					if (msg.Length > 0)
					{
						msg += "\n";
					}

					msg += errMsg;
				}

				MessageBox.Show(this,msg);
			}
		}

		private void btnDelete_Click(object sender, System.EventArgs e)
		{
			ArrayList errorMessages;
			int line = this.tabFlights.GetCurrentSelected();
			if (line < 0)
				return;


			this.tabFlights.SetLineColor(line,000000,654321);
			
			if (!MyController.DeleteRow(this.ds.Tables["Flight"].Rows[line],out errorMessages))
			{
				string msg = "";
				foreach(string errMsg in errorMessages)
				{
					if (msg.Length > 0)
					{
						msg += "\n";
					}

					msg += errMsg;
				}

				MessageBox.Show(this,msg);
				
			}
			else
			{

			}

		}


		private void btnInsertRow_Click(object sender, System.EventArgs e)
		{
			ArrayList errorMessages;
			if (!MyController.InsertRow(this.ds.Tables["Flight"],out errorMessages))
			{
				string msg = "";
				foreach(string errMsg in errorMessages)
				{
					if (msg.Length > 0)
					{
						msg += "\n";
					}

					msg += errMsg;
				}

				MessageBox.Show(this,msg);
				
			}
			else
			{
				DataRow row = MyController.CurrentFlight;
				string str="";
				for(int j = 0; j < ds.Tables["Flight"].Columns.Count; j++)
				{
					if( j+1 < ds.Tables["Flight"].Columns.Count)
					{
						str += row[ds.Tables["Flight"].Columns[j].Caption] + ",";
					}
					else
					{
						str += row[ds.Tables["Flight"].Columns[j].Caption];
					}
				}
				tabFlights.InsertTextLine(str, true);
			}
		}

		private Controller MyController
		{
			get
			{
				return (Controller)base.Controller;
			}
		}
	}
}
