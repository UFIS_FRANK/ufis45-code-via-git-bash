using System;
using System.Collections;
using System.Diagnostics;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Reflection.Emit;

using Ufis.Data;
using Ufis.Utils;

namespace Ufis.UIPAB_TG.BL.Services
{
	/// <summary>
	/// Summary description for Basic.
	/// </summary>
	public class Basic
	{
		private IDatabase		database = null;
		private	bool			authenticate = true;	
		private	string			wksName = string.Empty;
		private	string			usrName	= string.Empty;
		private bool			isLoggedIn = false;
	
		private	static TimeSpan	localDiff1;
		private static TimeSpan	localDiff2;
		private static DateTime	tich;	

		private	SortedList	privileges = new SortedList();
		private	ArrayList	dbSchema = new ArrayList();

		private	AppDomain		currentDomain = null;
		private	AssemblyName	myAssemblyName = null;

		// Define a dynamic assembly in the 'currentDomain'.
		private	AssemblyBuilder myAssemblyBuilder = null;

		// Define a dynamic module in "TempAssembly" assembly.
		private	ModuleBuilder myModuleBuilder = null;



		public delegate string WhereClause(string logicalTableName);
		public delegate bool   RowInserting(DataTable dt,DataRow row,ArrayList errorMessages);
		public delegate void   RowInserted(DataTable dt,DataRow row,ArrayList errorMessages);
		public delegate bool   RowDeleting(DataRow row,ArrayList errorMessages);
		public delegate bool   RowDeleted(DataRow row,ArrayList errorMessages);

		public WhereClause	OnWhereClause;
		public RowInserting	OnRowInserting;
		public RowInserted	OnRowInserted;	
		public RowDeleting	OnRowDeleting;
		public RowDeleted	OnRowDeleted;	

		public Basic(bool authenticate)
		{
			//
			// TODO: Add constructor logic here
			//
			this.authenticate = authenticate;
			this.database	  = UT.GetMemDB();
			this.database.Multithreaded = true;
			this.database.IndexUpdateImmediately = true;
			if (!this.database.IsSyncronized)
			{
				this.database = (IDatabase)this.database.SyncRoot;
			}
		}


		public ArrayList GetValuesFromIniFile()
		{
			ArrayList values = new ArrayList();
			// read configuration information from ceda.ini file
			IniFile iniFile = new IniFile("c:\\ufis\\system\\ceda.ini");
			string database		= iniFile.IniReadValue("Global","HostName");
			string hopo		= iniFile.IniReadValue("Global","HomeAirport");
			string tabExt	= iniFile.IniReadValue("Global","TableExtension");
			values.Add(database);
			values.Add(hopo);
			values.Add(tabExt);
			return values;
		}

		/// <summary>
		/// Sorgt f�r das Login auf dem Server mit den �bergebenen Parametern
		/// </summary>
		/// <param name="pUser"></param>
		/// <param name="pPassword"></param>
		/// <returns></returns>
		public bool LoginToServer(string database, string hopo, string tabExt, string username, string password,string appName,out string errorMessage)
		{


			errorMessage = string.Empty;

			// lokale Variablen
			bool loginSuccess = true;
			IUfisComWriter writer = null;

			if (!this.database.Connected)
			{
				loginSuccess = this.database.Connect(database,username,hopo,tabExt,appName);
				if (!loginSuccess)
				{
					errorMessage = "Connection to database failed";
					return loginSuccess;
				}

				writer  = this.database.Writer;	// create new writer for multithreading support
				wksName = writer.GetWorkstationName();
			}
			else
			{
				writer = this.database.Writer;	// create new writer for multithreading support
				wksName = writer.GetWorkstationName();
			}

			if (this.authenticate)
			{
				string data = username + "," + password + "," + appName + "," + wksName;
				if (writer.CallServer("GPR",hopo,"USID,PASS,APPL,WKST",data,"","360") == 0)
				{
					if (writer.GetBufferCount() > 0 && writer.GetBufferLine(0) == "[GPR]")
					{
						for (int i = 1; i < writer.GetBufferCount(); i++)
						{
							string tmp = writer.GetBufferLine(i);
							ArrayList itemList = new ArrayList(tmp.Split(','));
							if (itemList.Count == 3)
							{
								string key = (string)itemList[1];
								string valu= (string)itemList[2];
								this.AddPrivileg(key,valu);
							}
						}
					}
				}
				else
				{
					errorMessage = writer.LastErrorMessage;
					loginSuccess = false;
				}
			}

			this.isLoggedIn = loginSuccess;
			this.usrName	= username;
			return loginSuccess;
		}


		public bool Register(string hopo, string registerApp)
		{
			bool loginSuccess = false;
			IUfisComWriter writer = null;

			if (!this.database.Connected)
				return false;

			writer = this.database.Writer;	// create new writer for multithreading support

			loginSuccess = writer.CallServer("SMI",hopo,"APPL,SUBD,FUNC,FUAL,TYPE,STAT",registerApp,"","360") == 0;
			if (loginSuccess)
			{
				for (int i = 0; i < writer.GetBufferCount(); i++)
				{
					string tmp = writer.GetBufferLine(i);
					ArrayList itemList = new ArrayList(tmp.Split(','));
					string key = (string)itemList[1];
					string valu= (string)itemList[2];
					this.AddPrivileg(key,valu);
				}
			}

			return loginSuccess;
		}

		private void AddPrivileg(string key,string value)
		{
			this.privileges.Add(key,value);
		}

		public bool	RegisterApplication()
		{
			if (this.privileges.Count == 0)
				return true;
			else if (this.Privileg("InitModu") == "1")
				return true;
			else
				return false;
		}

		public string	Privileg(string value)
		{
			if (this.privileges.Count == 0)
				return null;
			else
				return (string)this.privileges[value];
		}

		private static bool GetLocalDiff(IUfisComWriter writer,ArrayList para)
		{
			string where = string.Format("where apc3 = '{0}'",writer.HomeAirport);
			writer.CallServer("RT","APTTAB","TICH,TDI1,TDI2","",where,"240");
			if (writer.GetBufferCount() == 1)
			{
				string[] values = writer.GetBufferLine(0).Split(',');	
				para.AddRange(values);
				return true;
			}
			else
			{
				return false;
			}
		}

		protected static bool SetLocalDiff(ArrayList para)
		{
			string ufisTime = para[0] + "00";
			tich = UT.CedaFullDateToDateTime(ufisTime);
			if (((string)para[1]).Length > 0)
			{
				int tdi1 = int.Parse((string)para[1]);
				int hour = tdi1 / 60;
				int minute = tdi1 % 60;
				localDiff1 = new TimeSpan(0,hour,minute,0);
			}

			if (((string)para[2]).Length > 0)
			{
				int tdi2 = int.Parse((string)para[2]);
				int hour = tdi2 / 60;
				int minute = tdi2 % 60;
				localDiff2 = new TimeSpan(0,hour,minute,0);
			}
			return true;
		}

		public bool IsLoggedIn
		{
			get
			{
				return this.isLoggedIn;
			}
		}

		public string UserName
		{
			get
			{
				return this.usrName;
			}
		}

		public static bool GetUtcToLocalDifference(IDatabase database)
		{
			IUfisComWriter writer  = database.Writer;	// create new writer for multithreading support

			ArrayList	para = new ArrayList();
			if (GetLocalDiff(writer,para))
			{
				SetLocalDiff(para);
				return true;
			}
			else
				return false;															
		}

		public static DateTime	Tich
		{
			get
			{
				return Basic.tich;
			}
		}

		public static TimeSpan LocalDiff1
		{
			get
			{
				return Basic.localDiff1;
			}
		}

		public static TimeSpan LocalDiff2
		{
			get
			{
				return Basic.localDiff2;
			}
		}

		internal IDatabase Database
		{
			get
			{
				if (this.database.IsSyncronized)
					return this.database;
				else
					return (IDatabase)this.database.SyncRoot;
			}
		}

		internal string GetSingleValueFromTableIndex(string table,string index,string value,string field)
		{
			ITable iTable = (ITable) Database[table];
			if (iTable != null)
			{
				try
				{
					iTable.Lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
					int[] lines	= iTable.LinesByIndexValue(index,value);
					if (lines != null && lines.Length == 1)
					{
						IRow row = (IRow)iTable[lines[0]];
						return row[field]; 
					}
				}
				catch(Exception exc)
				{
					System.Diagnostics.Debug.WriteLine(exc.Message);
				}
				finally
				{
					iTable.Lock.ReleaseReaderLock();
				}
			}

			return String.Empty;
		}

		internal string[] GetMultipleValuesFromTableIndex(string table,string index,string value,string field)
		{
			ITable iTable = (ITable) Database[table];
			if (iTable != null)
			{
				try
				{
					iTable.Lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
					int[] lines	= iTable.LinesByIndexValue(index,value);
					if (lines != null && lines.Length > 0)
					{
						string[] values = new string[lines.Length];
						for (int i = 0; i < lines.Length; i++)
						{
							IRow row = (IRow)iTable[lines[i]];
							values[i] = row[field];
						}

						return values;
					}
				}
				catch(Exception exc)
				{
					System.Diagnostics.Debug.WriteLine(exc.Message);
				}
				finally
				{
					iTable.Lock.ReleaseReaderLock();
				}
			}

			return null;
		}

		internal IRow GetSingleRowFromTableIndex(string table,string index,string value)
		{
			ITable iTable = (ITable) Database[table];
			if (iTable != null)
			{
				try
				{
					iTable.Lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
					int[] lines	= iTable.LinesByIndexValue(index,value);
					if (lines != null && lines.Length == 1)
					{
						IRow row = (IRow)iTable[lines[0]];
						return row;
					}
				}
				catch(Exception exc)
				{
					System.Diagnostics.Debug.WriteLine(exc.Message);
				}
				finally
				{
					iTable.Lock.ReleaseReaderLock();
				}
			}

			return null;
		}

		internal int[] GetMultipleLinesFromTableIndex(string table,string index,string value)
		{
			ITable iTable = (ITable) Database[table].SyncRoot;
			if (iTable != null)
			{
				int[] lines	= iTable.LinesByIndexValue(index,value);
				return lines;
			}

			return null;
		}

		internal IRow[] GetMultipleRowsFromTableIndex(string table,string index,string value)
		{
			ITable iTable = (ITable) Database[table].SyncRoot;
			if (iTable != null)
			{
				IRow[] rows	= iTable.RowsByIndexValue(index,value);
				return rows;
			}

			return null;
		}

		internal bool IsTotallyInside(DateTime start1, DateTime end1, DateTime start2, DateTime end2)
		{
			return ((start1) >= (start2) && (end1) <= (end2));
		}

		internal bool IsOverlapped(DateTime Start1, DateTime end1,DateTime  Start2, DateTime end2)
		{
			return ((Start1) <= (end2) && (Start2) <= (end1));
		}

		internal bool IsBetween(DateTime val, DateTime start, DateTime end)  
		{
			return (start) <= (val) && (val) <= (end);
		}

		internal bool IsReallyOverlapped(DateTime start1,DateTime end1,DateTime start2,DateTime end2)
		{
			return ((start1) < (end2) && (start2) < (end1));
		}

		internal bool IsWithIn(DateTime start1,DateTime  end1,DateTime  start2,DateTime  end2)
		{
			return ((start1) >= (start2) && (end1) <= (end2));
		}

		internal string CedaFullDateToString(IRow row,string field,string format)
		{
			string cedaDate = row[field];
			if (cedaDate.Length == 0)
				return string.Empty;
			if (format == null || format.Length == 0)
			{
				return UT.CedaFullDateToDateTime(cedaDate).ToString();				
			}
			else
			{
				return UT.CedaFullDateToDateTime(cedaDate).ToString(format);				
			}
		}

		internal DateTime CedaFullDateToDateTime(string date)
		{
			if (date.Length == 0)
				return DateTime.MinValue;
			else
				return UT.CedaFullDateToDateTime(date);
		}

		internal bool AddDatabaseSchema(string xmlSchemaFilePath,string xmlFilePath)
		{
			DataSet newSchema = new DataSet(xmlFilePath);
			newSchema.ReadXmlSchema(xmlSchemaFilePath);
			newSchema.ReadXml(xmlFilePath,XmlReadMode.Auto);
			this.dbSchema.Add(newSchema);
			return true;
		}

		internal bool CreateDatabaseBindings()
		{
			foreach (DataSet schema in this.dbSchema)
			{
				if (!CreateDatabaseBindings(schema))
					return false;	
			}

			return true;
		}

		private bool CreateDatabaseBindings(DataSet schema)
		{
			DataTable table = schema.Tables["TABLE"];
			if (table != null)
			{
				foreach(DataRow row in table.Rows)
				{
					if (!CreateDatabaseBindings(row))
						return false;

				}
			}

			return true;
		}

		private bool CreateDatabaseBindings(DataRow row)
		{
			string name	  = (string)row["name"];
			string dbname = (string)row["dbname"];
			int id		  = (int)row["TABLE_Id"];
			
			DataTable table = row.Table.DataSet.Tables["FIELD"];
			string filter = "TABLE_Id =" + id.ToString();
			DataRow[] rows  = table.Select(filter);

			StringBuilder fields = new StringBuilder();
			StringBuilder length = new StringBuilder();
			StringBuilder timefields = new StringBuilder();

			ArrayList indexes = new ArrayList();

			foreach(DataRow r in rows)
			{
				if (fields.Length > 0)
					fields.Append(",");
				fields.Append(r["dbname"]);

				if (length.Length > 0)
					length.Append(",");
				length.Append(r["dblength"]);

				if (r["logicaltype"] != System.DBNull.Value && (string)r["logicaltype"] == "datetime")
				{
					if (timefields.Length > 0)
						timefields.Append(",");
					timefields.Append(r["dbname"]);
				}

				if (r["index"] != System.DBNull.Value && (string)r["index"] == "true")
				{
					indexes.Add(r["dbname"]);
				}
			}
			
			ITable iTable = this.Database.Bind(name,dbname,fields.ToString(),length.ToString(),fields.ToString());

			if (row["readcmd"] != System.DBNull.Value)
			{
				iTable.Command("read",(string)row["readcmd"]);
			}

			if (row["updatecmd"] != System.DBNull.Value)
			{
				iTable.Command("update",(string)row["updatecmd"]);
			}

			if (row["insertcmd"] != System.DBNull.Value)
			{
				iTable.Command("insert",(string)row["insertcmd"]);
			}
			if (row["deletecmd"] != System.DBNull.Value)
			{
				iTable.Command("delete",(string)row["deletecmd"]);
			}

			iTable.TimeFields = timefields.ToString();
			if (row["timesinutc"] != System.DBNull.Value)
			{
				iTable.TimeFieldsInitiallyInUtc = bool.Parse((string)row["timesinutc"]);
			}
			else
			{
				iTable.TimeFieldsInitiallyInUtc = true;
			}
			iTable.TimeFieldsCurrentlyInUtc = true;

			foreach(string indexname in indexes)
			{
				iTable.CreateIndex(indexname,indexname);
			}
			
			return true;
		}

		internal bool LoadDatabase()
		{
			foreach (DataSet schema in this.dbSchema)
			{
				if (!LoadDatabase(schema))
					return false;	
			}

			return true;
		}

		private bool LoadDatabase(DataSet schema)
		{
			DataTable table = schema.Tables["TABLE"];
			if (table != null)
			{
				DataView dv = new DataView(table,"","loadsequence ASC",DataViewRowState.CurrentRows);
				dv.Sort = "loadsequence ASC";
				foreach(DataRow row in dv.Table.Rows)
				{
					if (!LoadDatabase(row))
						return false;

				}
			}

			return true;
		}

		private bool LoadDatabase(DataRow row)
		{
			string name	  = (string)row["name"];
			string dbname = (string)row["dbname"];

			ITable iTable = this.Database[name];

			if (row["whereclause"] != System.DBNull.Value && (string)row["whereclause"] == "true")
			{
				string where = this.OnWhereClause(name);
				if (where != "")
				{
					try
					{
						iTable.Lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
						if (!iTable.Loaded)
						{
							iTable.Load(where);
						}
					}
					catch(Exception exc)
					{
						System.Diagnostics.Debug.WriteLine(exc.Message);
						return false;
					}
					finally
					{
						iTable.Lock.ReleaseWriterLock();
					}
				}
			}
			else
			{
				try
				{
					iTable.Lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
					if (!iTable.Loaded)
					{
						iTable.Load("");
					}
				}
				catch(Exception exc)
				{
					System.Diagnostics.Debug.WriteLine(exc.Message);
					return false;
				}
				finally
				{
					iTable.Lock.ReleaseWriterLock();
				}
			}

			return true;
		}

		internal bool FillInitialDataSet(string businessCase,out DataSet ds)
		{
			ds = new DataSet(businessCase);

			foreach (DataSet schema in this.dbSchema)
			{
				if (!FillInitialDataSet(schema,businessCase,ds))
					return false;	
			}
						
			return true;
		}

		internal bool FillInitialDataSet(DataSet schema,string businessCase,DataSet ds)
		{
			DataTable table = schema.Tables["CASE"];
			if (table == null)
				return true;
			DataRow[] rows = table.Select("name = \'" + businessCase + "\'");
			if (rows == null || rows.Length == 0)
				return true;
			else if (rows.Length > 1)
				return false;

			int caseId = (int)rows[0]["CASE_Id"];
						
			DataTable tableview = schema.Tables["TABLEVIEW"];
			if (tableview == null)
				return false;

			rows = tableview.Select("CASE_Id = \'" + caseId.ToString() + "\'");
			foreach(DataRow row in rows)
			{
				CreateDataSetSchemaTable(schema,row,ds);
			}
				
			// create relationships between the tables
			DataTable relationtable = schema.Tables["RELATION"];
			if (tableview != null)
			{
				foreach(DataRow row in relationtable.Rows)
				{
					CreateDataSetSchemaRelation(schema,row,ds);
				}
				
			}						

			// fill dataset tables
			FillDataSet(schema,ds);
			return true;
		}


		internal bool CreateDataSetSchemaRelation(DataSet schema,DataRow row,DataSet ds)
		{
			int	 relationId		 = (int)row["RELATION_Id"];
			string relationName  = (string)row["name"];
			bool	constraint   = (string)row["constraint"] == "true" ? true : false;


			DataTable parent = schema.Tables["PARENT"];
			if (parent == null)
				return false;

			DataRow[] rows = parent.Select("RELATION_Id = \'" + relationId.ToString() + "\'");
			if (rows == null || rows.Length != 1)
				return false;

			string parentTable = (string)rows[0]["name"];
			string parentField = (string)rows[0]["field"];

			DataColumn parentColumn = ds.Tables[parentTable].Columns[parentField];

			DataTable child = schema.Tables["CHILD"];
			if (child == null)
				return false;

			rows = child.Select("RELATION_Id = \'" + relationId.ToString() + "\'");
			if (rows == null || rows.Length != 1)
				return false;

			string childTable = (string)rows[0]["name"];
			string childField = (string)rows[0]["field"];

			DataColumn childColumn = ds.Tables[childTable].Columns[childField];

			DataRelation dr = new DataRelation(relationName,parentColumn,childColumn,constraint);
			ds.Relations.Add(dr);

			return true;
		}

		internal bool CreateDataSetSchemaTable(DataSet schema,DataRow row,DataSet ds)
		{
			int	 tableViewId	 = (int)row["TABLEVIEW_Id"];
			string tableViewName = (string)row["name"];	
			DataTable table = ds.Tables.Add(tableViewName);
						
			DataTable tablefield = schema.Tables["TABLEFIELD"];
			if (tablefield == null)
				return false;

			int tableId = FindSchemaName(schema,"TABLE","name",tableViewName,"TABLE_Id");

			ArrayList primaryKeys = new ArrayList();
			DataRow[] rows = tablefield.Select("TABLEVIEW_Id = \'" + tableViewId.ToString() + "\'");
			foreach(DataRow tableviewrow in rows)
			{
				CreateDataSetSchemaTableFields(schema,tableviewrow,table,tableId,primaryKeys);
			}
			
			if (primaryKeys.Count > 0)
			{
				table.PrimaryKey = (System.Data.DataColumn[])primaryKeys.ToArray(typeof(System.Data.DataColumn));
			}
			return true;
		}

		internal System.Type UserDefinedDataType(DataSet schema,string logicaltype,string logicalname)
		{
			if (logicaltype == "ENUM")
			{
				int ENUM_Id = FindSchemaName(schema,"ENUM","name",logicalname,"ENUM_Id");
				if (ENUM_Id < 0)
				{
					return typeof(string);
				}

				// Get the current application domain for the current thread.
				if (currentDomain == null)
				{
					currentDomain = AppDomain.CurrentDomain;

					// Create assembly in current currentDomain
					myAssemblyName = new AssemblyName();
					myAssemblyName.Name = "TempAssembly";

					// Define a dynamic assembly in the 'currentDomain'.
					myAssemblyBuilder = currentDomain.DefineDynamicAssembly(myAssemblyName, AssemblyBuilderAccess.Run);

					// Define a dynamic module in "TempAssembly" assembly.
					myModuleBuilder = myAssemblyBuilder.DefineDynamicModule("TempModule",true);
				}

				// Define a enumeration type with name 'logicalname' in the 'TempModule'.
				EnumBuilder myEnumBuilder = myModuleBuilder.DefineEnum(logicalname,TypeAttributes.Public,typeof(byte));

				DataRow[] rows = schema.Tables["VALUE"].Select("ENUM_Id = '" + ENUM_Id.ToString() + "'");
				foreach (DataRow row in rows)
				{
					// Define the named static fields in the enumeration type 'MyEnum'.
					string name = (string)row["name"];
					char   valu = ((string)row["dbname"]).ToCharArray()[0];
					byte[] valuAsByte = BitConverter.GetBytes((char)valu);

					myEnumBuilder.DefineLiteral(name,valuAsByte[0]);
				}

				return myEnumBuilder.CreateType();
			}
			else
			{
				return typeof(string);
			}
		}

		internal bool CreateDataSetSchemaTableFields(DataSet schema,DataRow row,DataTable dt,int tableId,ArrayList primaryKeys)
		{
			string tableFieldName = (string)row["name"];	
			DataColumn column = dt.Columns.Add(tableFieldName);

			DataRow dbRow = FindSchemaName(schema,"FIELD","TABLE_Id",tableId,"name",tableFieldName);
			if (dbRow != null)
			{
				switch((string)dbRow["logicaltype"])
				{
					case "id":
						column.DataType = typeof(string);
						break;
					case "string":
						column.DataType = typeof(string);
						break;
					case "datetime":
						//						column.DataType = typeof(System.DateTime);
						column.DataType = typeof(string);
						break;
					default:
						column.DataType = UserDefinedDataType(schema,(string)dbRow["logicaltype"],(string)dbRow["logicalname"]);
						break;
				}

				if (dbRow["index"] != System.DBNull.Value && (string)dbRow["index"] == "true")
				{
					primaryKeys.Add(column);
				}
			}
						
			return true;
								
		}

		//		internal DataTable FindTable(DataSet ds,string search,string id)
		//		{
		//			DataTable dt = null;
		//
		//			return dt;
		//		}
		internal DataRow [] FindRows(DataTable dt, string search, string id)
		{
			DataRow [] rows = null;

			return rows;
		}

		internal bool FillDataSet(DataSet schema,DataSet ds)
		{
			foreach(DataTable dt in ds.Tables)
			{
				FillDataTable(schema,dt);
			}

			return true;
		}

		internal bool FillDataTable(DataSet schema,DataTable dt)
		{
			int tableId = FindSchemaName(schema,"TABLE","name",dt.TableName,"TABLE_Id");
			if (tableId < 0)
				return false;

			ITable iTable = this.Database[dt.TableName];
			if (iTable == null)
				return false;
			for (int i = 0; i < iTable.Count; i++)
			{
				DataRow row = dt.NewRow();

				foreach(DataColumn column in dt.Columns)
				{
					DataRow  dbRow = FindSchemaName(schema,"FIELD","TABLE_Id",tableId,"name",column.ColumnName);
					if (dbRow != null)
					{
						switch(column.DataType.FullName)
						{
							case "System.Int32":
								row[column.ColumnName] = int.Parse(iTable[i][(string)dbRow["dbname"]]);
								break;
							case "System.String":
								row[column.ColumnName] = iTable[i][(string)dbRow["dbname"]];
								break;
							case "System.DateTime":
								row[column.ColumnName] = UT.CedaFullDateToDateTime(iTable[i][(string)dbRow["dbname"]]);
								break;
							default :	// Userdefined enumeration
								char valu = (iTable[i][(string)dbRow["dbname"]]).ToCharArray()[0];
								Type type = this.myModuleBuilder.GetType((string)dbRow["logicalname"]);
								Object obj = Activator.CreateInstance(type);
								Enum e = (Enum)obj;
								byte[] values = (byte[])Enum.GetValues(type);
								string[]  names  = Enum.GetNames(type);
								for (int j = 0; j < values.Length; j++)
								{
									if (values[j] == valu)
									{										
										Enum e2 = (Enum)Enum.Parse(type,names[j]);
										row[column.ColumnName] = e2;
										break;
									}
								}
								break;
						}
					}
				}

				dt.Rows.Add(row);
			}

			return true;
		}

		internal int	 FindSchemaName(DataSet schema,string tableName,string attribName,string attribute,string resultName)
		{
			DataTable table = schema.Tables[tableName];	
			if (table != null)
			{
				DataRow[] rows = table.Select(attribName + "= \'" + attribute +"\'");
				if (rows.Length == 1)
					return (int)rows[0][resultName];
			}
			
			return -1;
		}


		internal DataRow FindSchemaName(DataSet schema,string tableName,string idName,int id,string attribName,string attribute)
		{
			DataTable table = schema.Tables[tableName];	
			if (table != null)
			{
				DataRow[] rows = table.Select(idName + " = \'" + id.ToString() + "\' AND " + attribName + "= \'" + attribute +"\'");
				if (rows.Length == 1)
					return rows[0];
			}
			
			return null;
		}

		internal DataRow FindSchemaName(DataSet schema,string tableName,string attribName,string attribute)
		{
			DataTable table = schema.Tables[tableName];	
			if (table != null)
			{
				DataRow[] rows = table.Select(attribName + "= \'" + attribute +"\'");
				if (rows.Length == 1)
					return rows[0];
			}
			
			return null;
		}

		internal DataSet FindSchema(string businessCase,out int caseId)
		{
			caseId = -1;
			foreach(DataSet schema in this.dbSchema)
			{
				DataRow[] rows = schema.Tables["CASE"].Select("name ='"+businessCase+"'");
				if (rows != null && rows.Length == 1)
				{
					caseId = (int)rows[0]["CASE_Id"];
					return schema;
				}
			}

			return null;
		}

		public bool Validate(DataRow row,out ArrayList errorMessages)
		{
			errorMessages = new ArrayList();

			int caseId;
			DataSet schema = FindSchema(row.Table.DataSet.DataSetName,out caseId);
			if (schema == null)
			{
				errorMessages.Add("Business case '" + row.Table.DataSet.DataSetName + "' not found in any schema");
				return false;
			}

			int table_id = FindSchemaName(schema,"TABLE","name",row.Table.TableName,"TABLE_Id");
			if (table_id >= 0)
			{
				foreach(DataColumn column in row.Table.Columns)
				{
					string item = (string)row[column,DataRowVersion.Current];

					DataRow dbRow = FindSchemaName(schema,"FIELD","TABLE_Id",table_id,"name",column.ColumnName);
					if (dbRow == null)
					{
						errorMessages.Add("Field '" + column.ColumnName + "' not found in any schema");
						return false;
					}

					int dblength	 = int.Parse((string)dbRow["dblength"]);
					if (dbRow["dbpattern"] != System.DBNull.Value)
					{
						string dbpattern = (string)dbRow["dbpattern"];
						Regex r = new Regex(dbpattern);
						Match match = r.Match(item);
						if (!match.Success)
						{
							errorMessages.Add("Value '" + item + "' for Field '" + column.ColumnName + "' doesn't match the pattern '" + dbpattern + "'");
						}
					}

					switch((string)dbRow["logicaltype"])
					{
						case "Id":
							break;
						case "string":
							break;
						case "datetime":
							break;
						default:
							break;

					}
				}
			}

			return true;
		}

		public bool Save(DataSet ds,out ArrayList errorMessages)
		{
			errorMessages = new ArrayList();

			int caseId;
			DataSet schema = FindSchema(ds.DataSetName,out caseId);
			if (schema == null)
			{
				errorMessages.Add("Business case '" + ds.DataSetName + "' not found in any schema");
				return false;
			}
						
			DataSet updates = ds.GetChanges(DataRowState.Modified);
			if (updates != null)
			{
				foreach (DataTable dt in updates.Tables)
				{
					if (!SaveDataTable(schema,caseId,dt,DataRowState.Modified,errorMessages))
						return false;
				}
			}

			updates = ds.GetChanges(DataRowState.Added);
			if (updates != null)
			{
				foreach (DataTable dt in updates.Tables)
				{
					if (!SaveDataTable(schema,caseId,dt,DataRowState.Added,errorMessages))
						return false;
				}
			}

			updates = ds.GetChanges(DataRowState.Deleted);
			if (updates != null)
			{
				foreach (DataTable dt in updates.Tables)
				{
					if (!SaveDataTable(schema,caseId,dt,DataRowState.Deleted,errorMessages))
						return false;
				}
			}

			return this.Database.Save();
		}

		public bool SaveDataTable(DataSet schema,int caseId,DataTable dt,DataRowState state,ArrayList errorMessages)
		{
			DataRow dbRow = FindSchemaName(schema,"TABLE","name",dt.TableName);
			if (dbRow == null)
			{
				errorMessages.Add("Table '" + dt.TableName + "' not found in any schema");
				return false;
			}

			int table_id = (int)dbRow["TABLE_Id"];

			// lookup for internal table
			ITable iTable = this.Database[dt.TableName];
			if (iTable == null)
			{
				errorMessages.Add("Table '" + dt.TableName + "' not found in internal data storage");
				return false;
			}
			iTable = (ITable)iTable.SyncRoot;
			
			bool result = false;
			switch(state)
			{
				case DataRowState.Modified:
					result = UpdateRows(schema,table_id,dt,iTable,errorMessages);
					break;
				case DataRowState.Added:
					result = InsertRows(schema,table_id,dt,iTable,errorMessages);
					break;
				case DataRowState.Deleted:
					result = DeleteRows(schema,table_id,dt,iTable,errorMessages);
					break;
			}
			return result;

		}		

		private bool UpdateRows(DataSet schema,int table_id,DataTable dt,ITable iTable,ArrayList errorMessages)
		{
			// find this row in internal storage
			DataRow dbRow = FindSchemaName(schema,"FIELD","TABLE_Id",table_id,"dbname","URNO");
			if (dbRow == null)
			{
				errorMessages.Add("Field '" + "URNO" + "' not found in any schema");
				return false;
			}
							
			string name = (string)dbRow["name"];

			foreach(DataColumn column in dt.Columns)
			{
				dbRow = FindSchemaName(schema,"FIELD","TABLE_Id",table_id,"name",column.ColumnName);
				if (dbRow == null)
				{
					errorMessages.Add("Field '" + column.ColumnName + "' not found in any schema");
					return false;
				}

				string dbname = (string)dbRow["dbname"];

				for (int i = 0; i < dt.Rows.Count; i++)
				{
					string urno = (string)dt.Rows[i][name];
					IRow iRow = this.GetSingleRowFromTableIndex(dt.TableName,"URNO",urno);
					if (iRow == null)
					{
						errorMessages.Add("Row with URNO = '" + urno + "' not found in internal storage");
						return false;
					}
					else
					{
						iRow.Status = State.Modified;
					}

					string item = (string)dt.Rows[i][column];

					if (dbname == "URNO")
					{
						;
					}
					else if (dbname == "LSTU")
					{
						iRow["LSTU"] = UT.DateTimeToCeda(DateTime.UtcNow);
					}
					else if (dbname == "USEU")
					{
						iRow["USEU"] = this.usrName;
					}
					else
					{
						iRow[dbname] = item;
					}

				}
			}

			return true;
		}

		private bool InsertRows(DataSet schema,int table_id,DataTable dt,ITable iTable,ArrayList errorMessages)
		{
			IRow[] iRows = iTable.CreateEmptyRows(dt.Rows.Count);

			DataRow dbRow = null;
			foreach(DataColumn column in dt.Columns)
			{

				dbRow = FindSchemaName(schema,"FIELD","TABLE_Id",table_id,"name",column.ColumnName);
				if (dbRow == null)
				{
					errorMessages.Add("Field '" + column.ColumnName + "' not found in any schema");
					return false;
				}

				string dbname = (string)dbRow["dbname"];

				for (int i = 0; i < dt.Rows.Count; i++)
				{
					
					if (dbname == "URNO")
					{
						;
					}
					else if (dbname == "CDAT")
					{
						iRows[i]["CDAT"] = UT.DateTimeToCeda(DateTime.UtcNow);
					}
					else if (dbname == "USEC")
					{
						iRows[i]["USEC"] = this.usrName;
					}
					else
					{
						string item = dt.Rows[i][column].ToString();
						iRows[i][dbname] = item;
					}
				}
			}

			return true;
		}

		private bool DeleteRows(DataSet ds,int table_id,DataTable dt,ITable iTable,ArrayList errorMessages)
		{
			int caseId;
			DataSet schema = FindSchema(ds.DataSetName,out caseId);
			if (schema == null)
			{
				errorMessages.Add("Business case :'" + ds.DataSetName +"' not found in any schema");
				return false;
			}
			
			// find this row in internal storage
			DataRow dbRow = FindSchemaName(schema,"FIELD","TABLE_Id",table_id,"dbname","URNO");
			if (dbRow == null)
			{
				errorMessages.Add("Field '" + "URNO" + "' not found in any schema");
				return false;
			}
							
			string name = (string)dbRow["name"];

			foreach(DataColumn column in dt.Columns)
			{
				dbRow = FindSchemaName(schema,"FIELD","TABLE_Id",table_id,"name",column.ColumnName);
				if (dbRow == null)
				{
					errorMessages.Add("Field '" + column.ColumnName + "' not found in any schema");
					return false;
				}

				foreach(DataRow row in dt.Rows)
				{
					string urno = (string)row[name];

					IRow iRow = this.GetSingleRowFromTableIndex(dt.TableName,"URNO",urno);
					if (iRow == null)
					{
						errorMessages.Add("Row with URNO = '" + urno + "' not found in internal storage");
						return false;
					}
					else
					{
						iRow.Status = State.Deleted;
					}

					string item = (string)row[column];

					string dbname = (string)dbRow["dbname"];

					if (dbname == "URNO")
					{
						;
					}
					else if (dbname == "LSTU")
					{
						iRow["LSTU"] = UT.DateTimeToCeda(DateTime.UtcNow);
					}
					else if (dbname == "USEU")
					{
						iRow["USEU"] = this.usrName;
					}
					else
					{
						iRow[dbname] = item;
					}
				}
			}

			return true;
		}

		public bool GetBusinessDataPattern(DataColumn column,out string pattern)
		{
			pattern = string.Empty;

			int caseId;
			DataSet schema = FindSchema(column.Table.DataSet.DataSetName,out caseId);
			if (schema == null)
			{
				return false;
			}

			int tableId = FindSchemaName(schema,"TABLE","name",column.Table.TableName,"TABLE_Id");
			if (tableId < 0)
			{
				return false;
			}

			DataRow row = FindSchemaName(schema,"FIELD","TABLE_Id",tableId,"name",column.ColumnName);
			if (row == null)
			{
				return false;
			}
			
			if (row["dbpattern"] == System.DBNull.Value)
				return false;
			else
			{
				pattern = (string)row["dbpattern"];
				return true;
			}
		}





		public bool InsertRow(DataTable dt,out ArrayList errorMessages)
		{
			errorMessages = new ArrayList();

			int caseId;
			DataSet schema = FindSchema(dt.DataSet.DataSetName,out caseId);
			if (schema == null)
			{
				errorMessages.Add("Business case :'" + dt.DataSet.DataSetName +"' not found in any schema");
				return false;
			}

			int tableId = FindSchemaName(schema,"TABLE","name",dt.TableName,"TABLE_Id");
			if (tableId < 0)
			{
				errorMessages.Add("Table name :'" + dt.TableName +"' not found in any schema");
				return false;
			}

			DataRow dbRow = FindSchemaName(schema,"FIELD","TABLE_Id",tableId,"dbname","URNO");
			if (dbRow == null)
			{
				return false;
			}

			DataRow row = dt.NewRow();

			foreach(DataColumn column in dt.Columns)
			{
				DataRow  dbRowColumn = FindSchemaName(schema,"FIELD","TABLE_Id",tableId,"name",column.ColumnName);
				if (dbRowColumn != null)
				{
					switch(column.DataType.FullName)
					{
						case "System.Int32":
							row[column.ColumnName] = 0;
							break;
						case "System.String":
							row[column.ColumnName] = "";
							break;
						case "System.DateTime":
							row[column.ColumnName] = 0;
							break;
						default :	// Userdefined enumeration
							Type type = this.myModuleBuilder.GetType((string)dbRowColumn["logicalname"]);
							Object obj = Activator.CreateInstance(type);
							Enum e = (Enum)obj;
							byte[] values = (byte[])Enum.GetValues(type);
							string[]  names  = Enum.GetNames(type);
							Enum e2 = (Enum)Enum.Parse(type,names[0]);
							row[column.ColumnName] = e2;
							break;
					}
				}
			}

			row[(string)dbRow["name"]] = this.Database.GetNextUrno();	

			if (this.OnRowInserting != null)
			{
				if (!this.OnRowInserting(dt,row,errorMessages))
					return false;
			}

			dt.Rows.Add(row);

			if (this.OnRowInserted != null)
			{
				this.OnRowInserted(dt,row,errorMessages);
			}
			return true;
		}





		public bool DeleteRow(DataRow dr,out ArrayList errorMessages)
		{
			errorMessages = new ArrayList();

			if (this.OnRowDeleting != null)
			{
				if (!this.OnRowDeleting(dr,errorMessages))
					return false;
			}

			int caseId;
			DataSet schema = FindSchema(dr.Table.DataSet.DataSetName,out caseId);
			if (schema == null)
			{
				errorMessages.Add("Business case :'" + dr.Table.DataSet.DataSetName +"' not found in any schema");
				return false;
			}

			int table_id = FindSchemaName(schema,"TABLE","name",dr.Table.TableName,"TABLE_Id");
			
			// lookup for internal table
			ITable iTable = this.Database[dr.Table.TableName];
			if (iTable == null)
			{
				errorMessages.Add("Table '" + dr.Table.TableName + "' not found in internal data storage");
				return false;
			}
			iTable = (ITable)iTable.SyncRoot;

			
			// find this row in internal storage
			DataRow dbRow = FindSchemaName(schema,"FIELD","TABLE_Id",table_id,"dbname","URNO");
			if (dbRow == null)
			{
				errorMessages.Add("Field '" + "URNO" + "' not found in any schema");
				return false;
			}
							
			string name = (string)dbRow["name"];

			string urno = (string)dr[name];

			IRow iRow = this.GetSingleRowFromTableIndex(dr.Table.TableName,"URNO",urno);
			if (iRow == null)
			{
				errorMessages.Add("Row with URNO = '" + urno + "' not found in internal storage");
				return false;
			}
			else
			{
				iRow.Status = State.Deleted;
				
			}

			if (this.OnRowDeleted != null)
			{
				this.OnRowDeleted(dr,errorMessages);
			}

			return true;
		}

		public bool DisposeApplication() 
		{
			UT.DisposeMemDB();
			return true;
		}
	}
}
