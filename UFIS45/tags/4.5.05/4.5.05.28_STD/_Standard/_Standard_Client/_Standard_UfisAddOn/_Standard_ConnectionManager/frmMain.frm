VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{6782E133-3223-11D4-996A-0000863DE95C}#1.1#0"; "UGantt.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form frmMain 
   Caption         =   "Connection Manager"
   ClientHeight    =   8805
   ClientLeft      =   165
   ClientTop       =   855
   ClientWidth     =   14550
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8805
   ScaleWidth      =   14550
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox cmdExcel 
      Caption         =   "&Excel"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   10440
      Style           =   1  'Graphical
      TabIndex        =   46
      Top             =   5160
      Width           =   645
   End
   Begin MSComDlg.CommonDialog dlgSave 
      Left            =   12600
      Top             =   1920
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdSendTelex 
      Caption         =   "SendTelex"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   9240
      TabIndex        =   45
      Top             =   5160
      Width           =   1215
   End
   Begin VB.Timer timerFlnuBcs 
      Interval        =   30000
      Left            =   12285
      Top             =   3465
   End
   Begin TABLib.TAB tabFlnuBcs 
      Height          =   735
      Left            =   10395
      TabIndex        =   42
      Top             =   8010
      Visible         =   0   'False
      Width           =   2760
      _Version        =   65536
      _ExtentX        =   4868
      _ExtentY        =   1296
      _StockProps     =   64
   End
   Begin TABLib.TAB tabResultLines 
      Height          =   735
      Left            =   7470
      TabIndex        =   40
      Top             =   8055
      Visible         =   0   'False
      Width           =   2760
      _Version        =   65536
      _ExtentX        =   4868
      _ExtentY        =   1296
      _StockProps     =   64
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "&Delete"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8415
      TabIndex        =   39
      Top             =   5160
      Width           =   705
   End
   Begin VB.CommandButton cmdInsert 
      Caption         =   "&Insert"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   7695
      TabIndex        =   38
      Top             =   5160
      Width           =   660
   End
   Begin VB.Timer timerUpdateTimefields 
      Enabled         =   0   'False
      Interval        =   60000
      Left            =   12300
      Top             =   4050
   End
   Begin VB.Timer timerRepaint 
      Enabled         =   0   'False
      Interval        =   10
      Left            =   12270
      Top             =   6270
   End
   Begin TABLib.TAB tabTab 
      Height          =   2115
      Index           =   1
      Left            =   6240
      TabIndex        =   29
      Top             =   5520
      Width           =   5595
      _Version        =   65536
      _ExtentX        =   9869
      _ExtentY        =   3731
      _StockProps     =   64
   End
   Begin VB.PictureBox lblSize 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFF00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   3555
      Index           =   1
      Left            =   6060
      MousePointer    =   9  'Size W E
      ScaleHeight     =   3555
      ScaleWidth      =   135
      TabIndex        =   27
      Tag             =   "-1"
      Top             =   4140
      Width           =   135
   End
   Begin VB.PictureBox lblSize 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H0080FF80&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   135
      Index           =   0
      Left            =   0
      MousePointer    =   7  'Size N S
      ScaleHeight     =   135
      ScaleWidth      =   11895
      TabIndex        =   26
      Tag             =   "-1"
      Top             =   4020
      Width           =   11895
      Begin VB.PictureBox lblSize 
         Appearance      =   0  'Flat
         AutoRedraw      =   -1  'True
         BackColor       =   &H00FF00FF&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   135
         Index           =   2
         Left            =   6060
         MousePointer    =   15  'Size All
         ScaleHeight     =   135
         ScaleWidth      =   135
         TabIndex        =   28
         Top             =   0
         Width           =   135
      End
   End
   Begin VB.PictureBox picToolBar 
      AutoRedraw      =   -1  'True
      Height          =   975
      Left            =   12390
      ScaleHeight     =   915
      ScaleWidth      =   1155
      TabIndex        =   25
      Top             =   0
      Width           =   1215
   End
   Begin VB.CheckBox cmdTelex 
      Caption         =   "&Telex"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   6300
      Style           =   1  'Graphical
      TabIndex        =   24
      Top             =   5160
      Width           =   645
   End
   Begin VB.Timer timerConflictCheck 
      Interval        =   30000
      Left            =   12240
      Top             =   4560
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   6990
      TabIndex        =   21
      Top             =   5160
      Width           =   660
   End
   Begin VB.Timer TimerRefresh 
      Left            =   12240
      Top             =   5160
   End
   Begin TABLib.TAB tabTmpFlights 
      Height          =   855
      Left            =   4260
      TabIndex        =   18
      Top             =   7980
      Visible         =   0   'False
      Width           =   2355
      _Version        =   65536
      _ExtentX        =   4154
      _ExtentY        =   1508
      _StockProps     =   64
   End
   Begin VB.Timer Timer1 
      Interval        =   100
      Left            =   12240
      Top             =   5700
   End
   Begin TABLib.TAB tabTab 
      Height          =   915
      Index           =   0
      Left            =   6240
      TabIndex        =   12
      Top             =   4200
      Width           =   5595
      _Version        =   65536
      _ExtentX        =   9869
      _ExtentY        =   1614
      _StockProps     =   64
   End
   Begin UGANTTLib.UGantt gantt 
      Height          =   3435
      Index           =   1
      Left            =   60
      TabIndex        =   10
      Top             =   4200
      Width           =   5955
      _Version        =   65536
      _ExtentX        =   10504
      _ExtentY        =   6059
      _StockProps     =   64
   End
   Begin UGANTTLib.UGantt gantt 
      Height          =   2895
      Index           =   0
      Left            =   60
      TabIndex        =   8
      Top             =   1080
      Width           =   11775
      _Version        =   65536
      _ExtentX        =   20770
      _ExtentY        =   5106
      _StockProps     =   64
   End
   Begin MSComctlLib.Slider slFontSize 
      Height          =   255
      Left            =   30
      TabIndex        =   1
      ToolTipText     =   "Change font and bar size"
      Top             =   7980
      Width           =   1875
      _ExtentX        =   3307
      _ExtentY        =   450
      _Version        =   393216
      Min             =   5
      Max             =   72
      SelStart        =   5
      TickFrequency   =   10
      Value           =   5
   End
   Begin MSComctlLib.Slider slDuration 
      Height          =   255
      Left            =   1920
      TabIndex        =   2
      ToolTipText     =   "Change visible time frame"
      Top             =   7980
      Width           =   1875
      _ExtentX        =   3307
      _ExtentY        =   450
      _Version        =   393216
      Min             =   2
      Max             =   24
      SelStart        =   5
      TickFrequency   =   2
      Value           =   5
   End
   Begin MSComctlLib.StatusBar MainStatusBar 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   3
      Top             =   8535
      Width           =   14550
      _ExtentX        =   25665
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   20003
            Text            =   "Status"
            TextSave        =   "Status"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            AutoSize        =   2
            TextSave        =   "20/01/2012"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            AutoSize        =   2
            TextSave        =   "16:23"
         EndProperty
      EndProperty
   End
   Begin VB.Frame fraToolbar 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   735
      Index           =   0
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Visible         =   0   'False
      Width           =   12795
      Begin VB.CheckBox cbToolButton 
         Caption         =   "View Log"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   675
         Index           =   11
         Left            =   8220
         Style           =   1  'Graphical
         TabIndex        =   47
         Top             =   0
         Width           =   615
      End
      Begin VB.CheckBox cbToolButton 
         Caption         =   "Gate"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   10
         Left            =   1845
         Style           =   1  'Graphical
         TabIndex        =   44
         Top             =   360
         Width           =   570
      End
      Begin VB.CheckBox cbToolButton 
         Caption         =   "&Filter"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   9
         Left            =   1260
         Style           =   1  'Graphical
         TabIndex        =   37
         Top             =   360
         Width           =   585
      End
      Begin VB.OptionButton rbArea 
         Caption         =   "&All"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   675
         Index           =   3
         Left            =   7800
         Style           =   1  'Graphical
         TabIndex        =   36
         ToolTipText     =   "Show all areas"
         Top             =   0
         Width           =   375
      End
      Begin VB.OptionButton rbArea 
         Caption         =   "&3"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   7440
         Style           =   1  'Graphical
         TabIndex        =   35
         ToolTipText     =   "Show connection tables full screen"
         Top             =   360
         Width           =   315
      End
      Begin VB.OptionButton rbArea 
         Caption         =   "&2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   7080
         Style           =   1  'Graphical
         TabIndex        =   34
         ToolTipText     =   "Show connection Gantt full screen"
         Top             =   360
         Width           =   315
      End
      Begin VB.OptionButton rbArea 
         Caption         =   "&1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   7080
         Style           =   1  'Graphical
         TabIndex        =   33
         ToolTipText     =   "Show position Gantt full screen"
         Top             =   0
         Width           =   675
      End
      Begin VB.CheckBox cbToolButton 
         Caption         =   "Plain &Gantt"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   8
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   30
         Top             =   360
         Width           =   1155
      End
      Begin VB.CheckBox cbToolButton 
         Caption         =   "Hourly &PAX"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   7
         Left            =   4440
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   360
         Width           =   1395
      End
      Begin VB.CheckBox cbToolButton 
         Caption         =   "Connection &Times"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   6
         Left            =   2460
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   360
         Width           =   1935
      End
      Begin VB.CheckBox cbToolButton 
         Caption         =   "&Mail/Cargo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   5
         Left            =   5880
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   0
         Width           =   1155
      End
      Begin VB.CheckBox cbToolButton 
         Caption         =   "&Setup"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   4
         Left            =   5880
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   360
         Width           =   1155
      End
      Begin VB.CheckBox cbToolButton 
         Caption         =   "&Hourly Flights"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   4440
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   0
         Width           =   1395
      End
      Begin VB.CheckBox cbToolButton 
         Caption         =   "&Compressed Flights"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   2460
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   0
         Width           =   1935
      End
      Begin VB.CheckBox cbToolButton 
         Caption         =   "&Now <|>>"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   1260
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   0
         Width           =   1155
      End
      Begin VB.CheckBox cbToolButton 
         Caption         =   "&Load"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   60
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   0
         Width           =   1155
      End
      Begin VB.Label lblTime 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "27.07.2003 -10:23"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   255
         Index           =   1
         Left            =   10740
         TabIndex        =   32
         Top             =   420
         Width           =   1935
      End
      Begin VB.Label lblTime 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "27.07.2003 -10:23z"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   0
         Left            =   8610
         TabIndex        =   31
         Top             =   420
         Width           =   2025
      End
      Begin VB.Shape ServerSignal 
         FillColor       =   &H0000FF00&
         Height          =   195
         Index           =   1
         Left            =   10140
         Shape           =   3  'Circle
         Top             =   150
         Width           =   195
      End
      Begin VB.Shape ServerSignal 
         FillColor       =   &H0000FF00&
         Height          =   195
         Index           =   0
         Left            =   9900
         Shape           =   3  'Circle
         Top             =   150
         Width           =   195
      End
   End
   Begin VB.Label Label3 
      Caption         =   "tmp TLX-FLNUs"
      Height          =   240
      Left            =   10395
      TabIndex        =   43
      Top             =   7785
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label Label2 
      Caption         =   "tmp for result lines"
      Height          =   240
      Left            =   7470
      TabIndex        =   41
      Top             =   7830
      Visible         =   0   'False
      Width           =   2760
   End
   Begin VB.Label lblTip 
      BackColor       =   &H80000007&
      Caption         =   "Press right mouse button below to get alternative flights"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FF00&
      Height          =   195
      Index           =   0
      Left            =   11160
      TabIndex        =   22
      Top             =   5220
      Width           =   585
   End
   Begin VB.Label Label1 
      Caption         =   "Tmp Fligts to find next dep."
      Height          =   195
      Left            =   4260
      TabIndex        =   19
      Top             =   7800
      Visible         =   0   'False
      Width           =   2355
   End
   Begin VB.Label lblTabContainer 
      Height          =   3555
      Left            =   6180
      TabIndex        =   11
      Top             =   4140
      Width           =   5715
   End
   Begin VB.Label lblDetailContainer 
      Height          =   3555
      Left            =   0
      TabIndex        =   9
      Top             =   4140
      Width           =   6075
   End
   Begin VB.Label lblMainContainer 
      BorderStyle     =   1  'Fixed Single
      Height          =   3015
      Left            =   0
      TabIndex        =   7
      Top             =   1020
      Width           =   11895
   End
   Begin VB.Label lblAboveText 
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "lblAboveText(0)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   255
      Index           =   0
      Left            =   0
      TabIndex        =   6
      Top             =   720
      Width           =   2055
   End
   Begin VB.Label lblAboveText 
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "lblAboveText(1)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   2100
      TabIndex        =   0
      Top             =   720
      Width           =   9795
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuExit 
         Caption         =   "&Exit"
         Index           =   0
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuAbout 
         Caption         =   "&Info/About ..."
      End
   End
   Begin VB.Menu mnuDetailTabContext 
      Caption         =   "&Edit"
      Visible         =   0   'False
      Begin VB.Menu mnuAlternateConnections 
         Caption         =   "&Search alternative connections"
      End
      Begin VB.Menu mnuFeedingFlights 
         Caption         =   "&Show feeding flights"
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public igCurrentPercentY As Integer 'where is the horizontal splitter of total height
Public igCurrentPercentX As Integer 'where is the vertical splitter of total width
Public mnCount As Integer
Public bgSimulation As Boolean
Public lgDragSource As Integer
Public bgfrmLoadIsActive As Boolean
Public bgHourlyIsActive As Boolean
Public bgMailCargoIsActive As Boolean
Public bmIsInitialized As Boolean
Public omCurrentSelectedUrno As String
Public omWKS As String
Public bmConnTimesDirty As Boolean 'will be set when connecton times have been changed to
                                   'optimize the drawings if not changed, otherwise we have
                                   'to recheck all conflicts
Public bmPositionActive As Boolean    'indicator True => POSGantt False => GatGantt

Private Declare Function GetPrivateProfileString Lib "kernel32" _
           Alias "GetPrivateProfileStringA" _
                 (ByVal sSectionName As String, _
                  ByVal sKeyName As String, _
                  ByVal sDefault As String, _
                  ByVal sReturnedString As String, _
                  ByVal lSize As Long, _
                  ByVal sFileName As String) As Long

Private WithEvents MyBC As BcProxyLib.BcPrxy
Attribute MyBC.VB_VarHelpID = -1
Public WithEvents MsExcel As Excel.Application
Attribute MsExcel.VB_VarHelpID = -1

Public Sub RefreshAllControls()
    TimerRefresh.Enabled = True
    TimerRefresh.interval = 5
End Sub

Private Sub cbToolButton_Click(Index As Integer)
    Dim strTmp As String
    Dim strLine As String
    Dim myHourlyPax As frmTransferPAXChart
    Dim d As Date
    Dim myControls() As Control
    Dim strCtlNames As String
    Dim strCtlVersions As String
    Dim llLine As Long
    Dim strStoa As String
    Dim datStoa As Date
    
    Select Case (Index)
        Case 0 '***View Button pressed
            If bgfrmLoadIsActive = False Then
                'Reset frmMain DECISION dropdown
                bgfrmMainOpen = True
                bgfrmLoadIsActive = True
                cbToolButton(Index).backColor = vbGreen
                frmLoad.Show vbModal
                cbToolButton(Index).Value = 0
                cbToolButton(Index).backColor = vbButtonFace
                bgfrmLoadIsActive = False
                gantt(0).Refresh
                gantt(1).Refresh
            End If
        Case 1 '***Now Button pressed
            cbToolButton(Index).Value = 0
            d = Now
            If TimesInUTC = True Then
                d = DateAdd("h", -(UTCOffset + 1), d)
            Else
                d = DateAdd("h", -1, d)
            End If
            lblAboveText(0) = Format(d, "dd.mm.YY")
            gantt(0).ScrollTo d
            gantt(0).Refresh
            gantt(1).Refresh
        Case 2 '***Compressed Flights View
            If cbToolButton(Index).Value = 1 Then
                If bgfrmCompressedFlightsOpen = False Then
                    frmCompressedFligths.Show
                    bgfrmCompressedFlightsOpen = True
                Else
                    frmCompressedFligths.ZOrder
                    frmCompressedFligths.gantt.Refresh
                End If
                cbToolButton(Index).Value = 0
                gantt(0).Refresh
                gantt(1).Refresh
            Else
                cbToolButton(Index).backColor = vbButtonFace
            End If
        Case 3 '***Hourly Distribution pressed
            If cbToolButton(Index).Value = 1 Then
                If bgHourlyIsActive = False Then
                    bgHourlyIsActive = True
                    frmHourlyDistribution.Show
                    frmHourlyDistribution.ZOrder
                Else
                    frmHourlyDistribution.ZOrder
                End If
                cbToolButton(Index).Value = 0
            End If
        Case 4 '***Setup pressed
            If cbToolButton(Index).Value = 1 Then
                cbToolButton(Index).backColor = vbGreen
                frmSetup.Show vbModal
                gantt(0).Refresh
                gantt(1).Refresh
            Else
                cbToolButton(Index).backColor = vbButtonFace
            End If
            cbToolButton(Index).Value = 0
        Case 5 '***Mail/Cargo was pressed
            If cbToolButton(Index).Value = 1 Then
                If bgMailCargoIsActive = False Then
                    cbToolButton(Index).backColor = vbGreen
                    bgMailCargoIsActive = True
                    frmMailCargo.Show , Me
                    frmMailCargo.ZOrder
                Else
                    frmMailCargo.ZOrder
                    cbToolButton(Index).backColor = vbButtonFace
                End If
            Else
                Unload frmMailCargo
                bgMailCargoIsActive = False
                cbToolButton(Index).backColor = vbButtonFace
            End If
        Case 6: '*** FrmConnection Times
            If cbToolButton(Index).Value = 1 Then
                cbToolButton(Index).backColor = vbGreen
                frmConnectionTimes.Show vbModal
                'If bmConnTimesDirty = True Then
                    'Recheck all and reselect the current bar
                    'because we could have a new conflict situation
                    CheckAllArrivalsForConflicts
                    If omCurrentSelectedUrno <> "" Then
                        SelectMainGanttBar omCurrentSelectedUrno, 0
                    End If
                'End If
                cbToolButton(Index).backColor = vbButtonFace
                cbToolButton(Index).Value = 0
                gantt(0).Refresh
                gantt(1).Refresh
            Else
                cbToolButton(Index).backColor = vbButtonFace
            End If
        Case 7 '***Hourly Distribution pressed
            If cbToolButton(Index).Value = 1 Then
                Set myHourlyPax = New frmTransferPAXChart
                myHourlyPax.Show
                cbToolButton(Index).Value = 0
            End If
        Case 8 '***Plain Gantt pressed
            If cbToolButton(Index).Value = 1 Then
                cbToolButton(Index).backColor = vbGreen
                gantt(0).LifeStyle = False
                gantt(0).Refresh
                gantt(1).LifeStyle = False
                gantt(1).Refresh
                If bgfrmCompressedFlightsOpen = True Then
                    frmCompressedFligths.gantt.LifeStyle = False
                    frmCompressedFligths.gantt.Refresh
                End If
            Else
                cbToolButton(Index).backColor = vbButtonFace
                gantt(0).LifeStyle = True
                gantt(0).Refresh
                gantt(1).LifeStyle = True
                gantt(1).Refresh
                If bgfrmCompressedFlightsOpen = True Then
                    frmCompressedFligths.gantt.LifeStyle = True
                    frmCompressedFligths.gantt.Refresh
                End If
            End If
        Case 9 'Call filter dialog
            If cbToolButton(Index).Value = 1 Then
                cbToolButton(Index).Value = 0
                frmFilter.strMode = ""
                frmFilter.Show , Me
            End If
        Case 10
            If cbToolButton(Index).Value = 1 Then
                cbToolButton(Index).backColor = vbGreen
                cbToolButton(Index).Refresh
                bmPositionActive = False
                cbToolButton(Index).Caption = "Pos."
            Else
                cbToolButton(Index).backColor = vbButtonFace
                cbToolButton(Index).Refresh
                bmPositionActive = True
                cbToolButton(Index).Caption = "Gate"
            End If
            MakeMainGanttLines
            MakeMainGanttBars False, False
            gantt(0).Refresh
            gantt(1).Refresh
         Case 11 'kkh on 25/07/2008 P2 View Decision Log
            If cbToolButton(Index).Value = 1 Then
                'cbToolButton(Index).Value = 0
                strTmp = tabTab(1).GetCurrentSelected
                frmViewLog.TabReadLog tabTab(1).GetFieldValue(strTmp, "CFI_URNO")
                cbToolButton(Index).backColor = vbGreen
                cbToolButton(Index).Value = 1
                frmViewLog.Show , Me
            End If
        Case Else
            If cbToolButton(Index).Value = 1 Then
                cbToolButton(Index).backColor = vbGreen
            Else
                cbToolButton(Index).backColor = vbButtonFace
            End If
    End Select
End Sub

Public Function GetBarCountOfLine(IDXGantt As Integer, line As Long) As Long
    Dim lineBars() As UGANTTLib.BAR_RECORD
    Dim cnt As Integer
    lineBars = gantt(IDXGantt).GetBarRecordsByLine(line)
    GetBarCountOfLine = UBound(lineBars) + 1
End Function

Private Sub cmdDelete_Click()
    Dim curSel As Long
    Dim strUrnoList As String
    Dim strUrno As String
    Dim strRurn As String
    Dim arrUrnos() As String
    Dim i As Integer
    Dim llLine As Long
    Dim strWhere As String
    Dim strTifa As String
    Dim datTifa As Date
    Dim tmpUrno As String
'"AFTURNO,LOA_RURN,ACT,FLNO,DATE,STD,EST,ATD,DES,T-PAX,F-PAX,B-PAX,E-PAX,NA,Hold,DUMMY,L_URNOS"
    
    curSel = tabTab(1).GetCurrentSelected
    If curSel > -1 Then
        strRurn = tabTab(1).GetFieldValue(curSel, "LOA_RURN")
        If strRurn = "0" Then
            strUrnoList = tabTab(1).GetFieldValue(curSel, "L_URNOS")
            arrUrnos = Split(strUrnoList, ";")
            For i = 0 To UBound(arrUrnos)
                strUrno = arrUrnos(i)
                strWhere = "WHERE URNO = '" & strUrno & "'"
                frmData.tabData(3).GetLinesByIndexValue "URNO", strUrno, 0
                llLine = frmData.tabData(3).GetNextResultLine
                If llLine > -1 Then
                    frmData.tabData(3).DeleteLine (llLine)
                    'kkh on 28/11/2008 Display KRI as higher priority then PTM message for all flights
                    'frmData.tabData(3).Sort "1,2,3", True, True  ' This reorgs the indexes as well!
                    frmData.tabData(3).Sort "3,2", True, True
                End If
                frmData.SetServerParameters
                If frmData.aUfis.CallServer("DRT", "LOATAB", "", "", strWhere, "230") <> 0 Then
                    MsgBox "Delete Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
                Else
                    frmData.SetServerParameters
                    frmData.aUfis.CallServer "SBC", "LOA:DRT", "", omCurrentSelectedUrno, strUrno, "230"
                End If
            Next i
            
            frmData.tabData(0).GetLinesByIndexValue "URNO", omCurrentSelectedUrno, 0
            llLine = frmData.tabData(0).GetNextResultLine
            If llLine > -1 Then
                strTifa = frmData.tabData(0).GetFieldValue(llLine, "TIFA")
                If Trim(strTifa) <> "" Then
                    datTifa = CedaFullDateToVb(strTifa)
                End If
            End If
            
            If HasArrivalConflict(omCurrentSelectedUrno, datTifa, False) = True Then
                If frmData.tabCurrentBar.GetLineCount > 0 Then
                    frmData.tabCurrentBar.SetColumnValue 0, 1, CStr(vbRed)
                Else
                    frmData.tabCurrentBar.SetColumnValue 0, 1, CStr(colorArrivalBar)
                End If
            Else
                If frmData.tabCurrentBar.GetLineCount > 0 Then
                    frmData.tabCurrentBar.SetColumnValue 0, 1, CStr(colorArrivalBar)
                End If
                If bgfrmCompressedFlightsOpen = True Then
                    If frmCompressedFligths.cbToolButton(2).Value = 1 Then
                        frmCompressedFligths.gantt.DeleteBarByKey omCurrentSelectedUrno
                    End If
                End If
            End If
            SelectMainGanttBar omCurrentSelectedUrno, 0
        End If
    End If
End Sub
'kkh on 15/07/2008 P2 Excel Button RFC
Private Sub cmdExcel_Click()
    frmExportExcel.Show
    cmdExcel.backColor = vbButtonFace
End Sub
'kkh on 15/07/2008 P2 Excel Button RFC
Public Function expExcel(strTYPE As String)
    Dim i As Integer
    Dim llLine As Long
    Dim ExcelApp As Object
    Dim ExcelWorkbook As Object
    Dim ExcelWorkBook1 As Object
    Dim ExcelSheet As Excel.Worksheet
    Dim ExcelRange As Excel.Range
    Dim MyFileName As String
    Dim tmpPath As String
    Dim k As Integer
    Dim rowData() As String
    Dim KeyVal As String
    Dim arrRemoveNo() As String
    
    Dim colSort1, colSort2, colSort3 As String
    Dim objrange As Object
    Dim objRangeSort1, objRangeSort2, objRangeSort3 As Object
    
    If cmdExcel.Value = 1 Then 'pressed
        'retrieve the data to frmTelex
        llLine = tabTab(1).GetCurrentSelected
        
        If llLine < 0 Then
            MsgBox "Please select a flight for telex."
            cmdExcel.Value = 0
            Exit Function
        Else
            llLine = tabTab(1).GetCurrentSelected
            ShowTelexForLine llLine
            cmdTelex.backColor = vbButtonFace
        End If

        'If UBound(arr1) > 0 And InStr(frmTelex.txtTelex.Text, "Telex not found") = 0 Then
        If InStr(frmTelex.txtTelex.Text, "Telex not found") = 0 Then
            MousePointer = vbHourglass
'            dlgSave.DialogTitle = "Export to Excel"
'            dlgSave.Filter = "Excel (xls)|*.xls"
'            Select Case (strType)
'                Case "PN":
'                    dlgSave.FileName = Replace(Mid(arr1(0), 1, 14), "  ", "") + " FN"
'                Case "FLT":
'                    dlgSave.FileName = Replace(Mid(arr1(0), 1, 14), "  ", "") + " FLT"
'            End Select
'            dlgSave.ShowSave
             
            'If dlgSave.FileName <> "" Then

            'Set ExcelApp = New Excel.Application
            Set ExcelApp = CreateObject("Excel.application")
            Set ExcelWorkbook = ExcelApp.Workbooks.Add
        
            Set ExcelSheet = ExcelWorkbook.Worksheets(1)
            Set ExcelRange = ExcelSheet.Cells
            
            For i = 1 To Len(frmTelex.txtTelex.Text)
                KeyVal = KeyVal + Hex(Asc(Mid(frmTelex.txtTelex.Text, i, 1)))   ' Build Value Char. By Char.
            Next i
            rowData = Split(KeyVal, "DA")
                          
            For i = 1 To UBound(rowData)
                KeyVal = ""
                For k = 1 To Len(rowData(i))                               ' Convert Each Bit
                    KeyVal = KeyVal + ChrW("&h" & Mid(rowData(i), k, 2))   ' Build Value Char. By Char.
                    k = k + 1
                Next
                ExcelSheet.Columns("A:N").NumberFormat = "@"
                
                arrRemoveNo = Split(KeyVal, ".")
                If UBound(arrRemoveNo) > 0 Then
                    ExcelSheet.Cells(i + 1, 1) = CStr(i) + "."
                    If Len(KeyVal) = 61 Then
                        ExcelSheet.Cells(i + 1, 2) = Mid(arrRemoveNo(1), 1, 8)
                        ExcelSheet.Cells(i + 1, 3) = Mid(arrRemoveNo(1), 9, 1)
                        ExcelSheet.Cells(i + 1, 4) = Mid(arrRemoveNo(1), 11, 3)
                        ExcelSheet.Cells(i + 1, 5) = Mid(arrRemoveNo(1), 15, 1)
                        ExcelSheet.Cells(i + 1, 6) = Mid(arrRemoveNo(1), 17, 1)
                        ExcelSheet.Cells(i + 1, 7) = Mid(arrRemoveNo(1), 19, 4)
                        ExcelSheet.Cells(i + 1, 8) = Mid(arrRemoveNo(1), 23, 5)
                        ExcelSheet.Cells(i + 1, 9) = Mid(arrRemoveNo(1), 29, 8)
                        ExcelSheet.Cells(i + 1, 10) = Mid(arrRemoveNo(1), 38, 8)
                        ExcelSheet.Cells(i + 1, 11) = Mid(arrRemoveNo(1), 47, 5)
                        ExcelSheet.Cells(i + 1, 12) = Mid(arrRemoveNo(1), 53, 3)
                        ExcelSheet.Cells(i + 1, 13) = Mid(arrRemoveNo(1), 57, 1)
                        ExcelSheet.Cells(i + 1, 14) = " "
                    Else
                        ExcelSheet.Cells(i + 1, 2) = Mid(arrRemoveNo(1), 1, 8)
                        ExcelSheet.Cells(i + 1, 3) = Mid(arrRemoveNo(1), 9, 1)
                        ExcelSheet.Cells(i + 1, 4) = Mid(arrRemoveNo(1), 11, 3)
                        ExcelSheet.Cells(i + 1, 5) = Mid(arrRemoveNo(1), 15, 1)
                        ExcelSheet.Cells(i + 1, 6) = Mid(arrRemoveNo(1), 17, 1)
                        ExcelSheet.Cells(i + 1, 7) = Mid(arrRemoveNo(1), 19, 4)
                        ExcelSheet.Cells(i + 1, 8) = Mid(arrRemoveNo(1), 23, 5)
                        ExcelSheet.Cells(i + 1, 9) = Mid(arrRemoveNo(1), 29, 8)
                        ExcelSheet.Cells(i + 1, 10) = Mid(arrRemoveNo(1), 38, 8)
                        ExcelSheet.Cells(i + 1, 11) = Mid(arrRemoveNo(1), 47, 5)
                        ExcelSheet.Cells(i + 1, 12) = Mid(arrRemoveNo(1), 53, 3)
                        ExcelSheet.Cells(i + 1, 13) = Mid(arrRemoveNo(1), 57, 1)
                        ExcelSheet.Cells(i + 1, 14) = Mid(arrRemoveNo(1), 59, 2)
                    End If
                End If
                ReDim arrRemoveNo(1)
            Next i
            ExcelSheet.Columns.AutoFit
            ExcelSheet.Cells(1, 1) = LTrim(RTrim(Mid(frmTelex.txtTelex.Text, 1, 50)))
            ExcelSheet.Cells(1, 11) = " " 'to make sure the sorting will not affect this header

            Select Case (strTYPE)
                Case "PN":
                    'no sorting needed
                Case "FLT":
                    'sorting base on A:Number|B:Name|C:Initial|D:dunno|E:Class|F:Check in/Board info|G:Gate|H:Seat|I:Bag/Weight|J:FLNO|K:STD|L:DES3|M:dunno|N:dunno
                    colSort1 = "K2" + "," + "K" + CStr(UBound(rowData))
                    colSort2 = "J2" + "," + "J" + CStr(UBound(rowData))
                    colSort3 = "B2" + "," + "B" + CStr(UBound(rowData))
        
                    Set objrange = ExcelSheet.Range("A:N") ' assuming columns A:N for data
                    Set objRangeSort1 = ExcelSheet.Range(colSort1)
                    Set objRangeSort2 = ExcelSheet.Range(colSort2)
                    Set objRangeSort3 = ExcelSheet.Range(colSort3)
        
                    'To sort by row change the number one at the end to 2
                    'example:
'                    objrange.Sort objRange2,2,,,,,,1 (1=column)
'                    objrange.Sort objRange2,2,,,,,,2 (2=row)
'                    objrange.Sort objRange2, 2, , , , , , 1 'for descending, or
'                    objrange.Sort objRange2, 1, , , , , , 1 'for ascending
'
                    objrange.Sort Key1:=objRangeSort1, Key2:=objRangeSort2, Key3:=objRangeSort3
'
'                    ExcelSheet.Range("A:N").Select
'                    Selection.Sort Key1:=Range("N1"), Order1:=xlAscending, Key2:=Range("B1") _
'                    , Order2:=xlAscending, Key3:=Range("J1"), Order3:=xlAscending, Header:= _
'                    xlGuess, OrderCustom:=1, MatchCase:=False, Orientation:=xlTopToBottom
            End Select
                
            tmpPath = GetIniEntry(myIniFullName, "HUBMANAGER", "GLOBAL", "EXCEL_WKBOOKS", "")
            If Dir$(tmpPath, vbDirectory) = "" Then
                'Directory doesnt exist, create automatically
                MkDir tmpPath
            End If
            
            Select Case (strTYPE)
                Case "PN":
                    'MyFileName = tmpPath & "\" & Replace(Mid(arr1(0), 1, 14), "  ", "") & " PN" & ".xls"
                    'MyFileName = tmpPath & "\" & Replace(Mid(frmTelex.txtTelex.Text, 1, 50), "  ", "") & " PN" & ".xls"
                    MyFileName = tmpPath & "\" & Replace(Mid(frmTelex.txtTelex.Text, 1, 15), "  ", "") & " PN" & ".xls"
                    'Mid(arr1(i), 1, 50)
                Case "FLT":
                    'MyFileName = tmpPath & "\" & Replace(Mid(arr1(0), 1, 14), "  ", "") & " FLT" & ".xls"
                    'MyFileName = tmpPath & "\" & Replace(Mid(frmTelex.txtTelex.Text, 1, 50), "  ", "") & " FLT" & ".xls"
                    MyFileName = tmpPath & "\" & Replace(Mid(frmTelex.txtTelex.Text, 1, 15), "  ", "") & " FLT" & ".xls"
            End Select

                         
            ExcelWorkbook.SaveAs MyFileName
            ExcelWorkbook.Close
           
            MsgBox "File Location : " & tmpPath
            
            Set ExcelWorkBook1 = ExcelApp.Workbooks.Add(MyFileName)
            ExcelApp.Visible = True
        Else
            MsgBox "Telex not found."
            cmdExcel.Value = 0
            Exit Function
        End If
    Else
        cmdExcel.backColor = vbButtonFace
    End If
    
    MousePointer = vbDefault
    cmdExcel.Value = 0

End Function
Private Function checkLen(sVal As String) As String

    
    'MsgBox sVal
    'If Len(Trim(sVal)) <= 1 Then
    If Len(Trim(sVal)) <= 1 Then
        checkLen = "  "
    Else
        checkLen = sVal
    End If
End Function
Private Sub cmdInsert_Click()
    If omCurrentSelectedUrno <> "" Then
        frmFilter.strMode = "CHOICE"
        frmFilter.Show , Me
    End If
End Sub
Private Sub cmdPrint_Click()
   'kkh on 20/07/2008 P2 Print Button RFC
    Dim rpt As New rptConnections
    Dim rpt_BKK As New rptConnections_BKK
   ' CEDA.INI file path. The path of the BMP file is taken from the parameter 'PRINTLOGO' under 'HUBMANAGER'
   ' Const sFile As String = "D:\Ufis\System\Ceda.ini"
    Dim sString As String
    Dim lSize As Long
    Dim lReturn As Long
    sString = String$(255, " ")
    lSize = Len(sString)
    
    lReturn = GetPrivateProfileString("HUBMANAGER", "PRINTLOGO", "", sString, lSize, DEFAULT_CEDA_INI)
     'lReturn = GetPrivateProfileString("HUBMANAGER", "PRINTLOGO", "", sString, lSize, sFile)
    sString = Trim(sString)
    sString = Left$(sString, Len(sString) - 1)
    
    Dim fso As FileSystemObject
    Set fso = New FileSystemObject
    
    If lReturn > 0 Then
        If fso.FileExists(sString) Then
            rpt.PrintLogo.Picture = LoadPicture(sString)
        Else
            MsgBox "Invalid print Logo bitmap file Path in CEDA.INI", vbExclamation + vbOKOnly, "Error"
        End If
    End If
    'kkh on 20/07/2008 P2 Print Button RFC
    If strHopo = "BKK" Then
        rpt_BKK.Show
    Else
        rpt.Show
    End If
    
End Sub
'kkh on 11/07/2008 P2 Send Telex RFC
Private Sub CmdSendTelex_Click()
    Dim RetVal As String
    Dim MyPath As String
    Dim opened As Boolean
      
    Dim tabLine As Integer
    Dim strRecordIndividual As String
    Dim strRecordAll As String
    Dim strDecision As String
    Dim strRecordDep() As String
    Dim strRecordArr As String
    Dim datTifd As Date
    Dim datEtdi As Date
    Dim i As Integer
    
    Dim strAftUrnoLineCnt As String
    Dim strLine() As String
    Dim strFigure() As String
    Dim strFclassFigure As Integer
    Dim strCclassFigure As Integer
    Dim strYclassFigure As Integer
    Dim strBWFigure() As String
    Dim strBagFigure As Integer
    Dim strWeightFigure As Integer
    Dim k As Integer
    'Dim lineSkip As Integer
    Dim strCurAFTURNO As String
    Dim strPreAFTURNO As String
    
    '---
    'igu on 28/06/2011
    'variables for special requirements
    Dim strArrFlno As String
    Dim strDepUrno As String
    Dim colDepFlights As Collection
    
    If pblnCopyToSpecialRequirements Then
        Set colDepFlights = New Collection
    End If
    '---
     
    opened = False
    tabTab(1).Sort 22, True, True
    
    For tabLine = 0 To tabTab(1).GetLineCount - 1
    
        strDecision = tabTab(1).GetFieldValue(tabLine, "Hold")
        ReDim strRecordDep(tabLine)
        strCurAFTURNO = tabTab(1).GetFieldValue(tabLine, "AFTURNO")
        If strCurAFTURNO <> strPreAFTURNO Then
            If strDecision <> "" Then
                strAftUrnoLineCnt = tabTab(1).GetLinesByColumnValues(0, tabTab(1).GetFieldValue(tabLine, "AFTURNO"), 0)

                If Len(strAftUrnoLineCnt) > 1 Then
                    strLine = Split(strAftUrnoLineCnt, ",")
                    
                    '--------------
                    'Date: May 13th 2009
                    'Desc: Reset all Figure Variables to 0
                    'By  : igu
                    '--------------
                    strFclassFigure = 0
                    strCclassFigure = 0
                    strYclassFigure = 0
                    strBagFigure = 0
                    strWeightFigure = 0
                    '--------------
                    For i = 0 To UBound(strLine)
                        strFigure = Split(tabTab(1).GetFieldValues(strLine(i), "F-PAX,C-PAX,Y-PAX,T-PC/WT"), ",")
                        For k = 0 To UBound(strFigure)
                            Select Case (k)
                                Case 0: strFclassFigure = Val(strFigure(k)) + strFclassFigure
                                Case 1: strCclassFigure = Val(strFigure(k)) + strCclassFigure
                                Case 2: strYclassFigure = Val(strFigure(k)) + strYclassFigure
                                Case 3: strBWFigure = Split(strFigure(k), "/")
                                        strBagFigure = strBWFigure(0) + strBagFigure
                                        strWeightFigure = strBWFigure(1) + strWeightFigure
                            End Select
                        Next k
                    Next i
                    
                    strDepUrno = tabTab(1).GetFieldValue(tabLine, "AFTURNO")
                    strRecordDep = Split(tabTab(1).GetFieldValues(tabLine, "FLNO,STD,EST,NA,VIAnDES"), ",")
                    strRecordIndividual = ""
                    
                    For i = 0 To UBound(strRecordDep)
                        Select Case (i)
                            Case 0:
                                strRecordDep(i) = Replace(strRecordDep(i), " ", "")
                                strRecordIndividual = strRecordDep(i)
                            Case 1:
                                If strRecordDep(i) <> "" Then
                                    datTifd = CedaFullDateToVb(strRecordDep(i))
                                    strRecordDep(i) = "STD" + " " + Format(datTifd, "hhmm") + "/" + Format(datTifd, "DD")
                                Else
                                    strRecordDep(i) = ""
                                End If
                                strRecordIndividual = strRecordIndividual + "," + strRecordDep(i)
                            Case 2:
                                If strRecordDep(i) <> "" Then
                                    datEtdi = CedaFullDateToVb(strRecordDep(i))
                                    strRecordDep(i) = "ETD" + " " + Format(datEtdi, "hhmm") + "/" + Format(datEtdi, "DD")
                                Else
                                    strRecordDep(i) = ""
                                End If
                                strRecordIndividual = strRecordIndividual + "," + strRecordDep(i)
                            Case 3:
                                If strRecordDep(i) <> "" Then
                                    strRecordDep(i) = strRecordDep(i)
                                End If
                                strRecordIndividual = strRecordIndividual + "," + strRecordDep(i)
                                'append pax figures
                                strRecordIndividual = strRecordIndividual + "," + CStr(strFclassFigure) + "P"
                                strRecordIndividual = strRecordIndividual + "/" + CStr(strCclassFigure) + "J"
                                strRecordIndividual = strRecordIndividual + "/" + CStr(strYclassFigure) + "Y"
                            Case 4:
                                If strRecordDep(i) <> "" Then
                                    strRecordDep(i) = strRecordDep(i)
                                End If
                                strRecordIndividual = strRecordIndividual + "," + strRecordDep(i)
                                strRecordIndividual = strRecordIndividual + "," + CStr(strBagFigure) + "PCS/" + CStr(strWeightFigure) + "KG"
                        End Select
                    Next i
                Else
                    'standard content for creating a telex
                    strDepUrno = tabTab(1).GetFieldValue(tabLine, "AFTURNO")
                    strRecordDep = Split(tabTab(1).GetFieldValues(tabLine, "FLNO,STD,EST,NA,F-PAX,C-PAX,Y-PAX,DES,T-PC/WT"), ",")
                    strRecordIndividual = ""
        
                    For i = 0 To UBound(strRecordDep)
                        Select Case (i)
                            Case 0:
                                'strRecordDep(i) = Trim$(strRecordDep(i))
                                strRecordDep(i) = Replace(strRecordDep(i), " ", "")
                                'Debug.Print strRecordDep(i)
                                strRecordIndividual = strRecordDep(i)
                            Case 1:
                                If strRecordDep(i) <> "" Then
                                    datTifd = CedaFullDateToVb(strRecordDep(i))
                                    strRecordDep(i) = "STD" + " " + Format(datTifd, "hhmm") + "/" + Format(datTifd, "DD")
                                Else
                                    strRecordDep(i) = ""
                                End If
                                strRecordIndividual = strRecordIndividual + "," + strRecordDep(i)
                            Case 2:
                                If strRecordDep(i) <> "" Then
                                    datEtdi = CedaFullDateToVb(strRecordDep(i))
                                    strRecordDep(i) = "ETD" + " " + Format(datEtdi, "hhmm") + "/" + Format(datEtdi, "DD")
                                Else
                                    strRecordDep(i) = ""
                                End If
                                strRecordIndividual = strRecordIndividual + "," + strRecordDep(i)
                            Case 3:
                                If strRecordDep(i) <> "" Then
                                    strRecordDep(i) = strRecordDep(i)
                                End If
                                strRecordIndividual = strRecordIndividual + "," + strRecordDep(i)
                            Case 4:
                                If strRecordDep(i) <> "" Then
                                    strRecordDep(i) = strRecordDep(i) + "P"
                                End If
                                strRecordIndividual = strRecordIndividual + "," + strRecordDep(i)
                            Case 5:
                                If strRecordDep(i) <> "" Then
                                    strRecordDep(i) = strRecordDep(i) + "J"
                                End If
                                strRecordIndividual = strRecordIndividual + "/" + strRecordDep(i)
                            Case 6:
                                If strRecordDep(i) <> "" Then
                                    strRecordDep(i) = strRecordDep(i) + "Y"
                                End If
                                strRecordIndividual = strRecordIndividual + "/" + strRecordDep(i)
                            Case 7:
                                If strRecordDep(i) <> "" Then
                                    strRecordDep(i) = strRecordDep(i)
                                End If
                                strRecordIndividual = strRecordIndividual + "," + strRecordDep(i)
                            Case 8:
                                If strRecordDep(i) <> "" Then
                                    strRecordDep(i) = Replace(strRecordDep(i), "/", "PCS/")
                                End If
                                strRecordIndividual = strRecordIndividual + "," + strRecordDep(i) + "KG"
                        End Select
                    Next i
                End If
            End If
           
            If strRecordAll = "" Then
                '/////DELAY ARRIVAL ADVICE/////
    '            strRecordArr = "INVIEW OF " + Replace(tabTab(0).GetFieldValue(0, "FLNO_A"), " ", "") + "/" + Mid(CedaFullDateToVb(tabTab(0).GetFieldValue(0, "DATE_A")), 8, 2) + UCase(Mid(CedaFullDateToVb(tabTab(0).GetFieldValue(0, "DATE_A")), 4, 3)) + " " & _
    '            tabTab(0).GetFieldValue(0, "REGN_A") + " " + tabTab(0).GetFieldValue(0, "ORG") + "/" + tabTab(0).GetFieldValue(0, "VIA_A") + "/SIN DLYD ARR"
                'strRecordArr = "INVIEW OF " + Replace(tabTab(0).GetFieldValue(0, "FLNO_A"), " ", "") + "/" + Mid(CedaFullDateToVb(tabTab(0).GetFieldValue(0, "DATE_A")), 8, 2) + UCase(Mid(CedaFullDateToVb(tabTab(0).GetFieldValue(0, "DATE_A")), 4, 3)) + " "
                'strRecordArr = "INVIEW OF " + Replace(tabTab(0).GetFieldValue(0, "FLNO_A"), " ", "") + "/" + Mid(tabTab(0).GetFieldValue(0, "DATE_A"), 1, 2) + Mid(tabTab(0).GetFieldValue(0, "DATE_A"), 4, 2) + " "
                strRecordArr = "INVIEW OF " + Replace(tabTab(0).GetFieldValue(0, "FLNO_A"), " ", "") + "/" + Mid(tabTab(0).GetFieldValue(0, "DATE_A"), 1, 2) + UCase(Mid(CedaFullDateToVb(tabTab(0).GetFieldValue(0, "FLIGHT_DATE")), 4, 3)) + " "
                If tabTab(0).GetFieldValue(0, "REGN_A") <> "" Then
                    strRecordArr = strRecordArr + tabTab(0).GetFieldValue(0, "REGN_A") + " "
                End If
                If tabTab(0).GetFieldValue(0, "ORG") <> "" Then
                    strRecordArr = strRecordArr + tabTab(0).GetFieldValue(0, "ORG") + "/"
                End If
                If tabTab(0).GetFieldValue(0, "VIA_A") <> "" Then
                    strRecordArr = strRecordArr + tabTab(0).GetFieldValue(0, "VIA_A") + "/"
                End If
                strRecordArr = strRecordArr + "SIN DLYD ARR"
                strRecordArr = strRecordArr & vbNewLine
                strRecordArr = strRecordArr + "STA/" + Replace(tabTab(0).GetFieldValue(0, "STA"), ":", "") + " ETA/" + Replace(tabTab(0).GetFieldValue(0, "ETA"), ":", "") + " PSE NOTE THE FLWG"
            
        '        strRecordAll = "QD CGKKDSQ KULKDSQ LHRKDSQ PVGKDSQ SINKDSQ SINKASQ SINOFSQ"
        '        strRecordAll = strRecordAll & vbNewLine & ".SINKDSQ 230301"
                strRecordAll = "ATTN ALL CONCERNED FRM SINKDSQ/ FOR DTM/"
                strRecordAll = strRecordAll & vbNewLine & "/////DELAY ARRIVAL ADVICE/////"
                strRecordAll = strRecordAll & vbNewLine & strRecordArr
            End If
            
            If strRecordIndividual <> "" Then
                If strDecision = "Not Waiting" Then
                    If InStr(strRecordAll, "/////FLIGHT NOT WAITING/////") = 0 Then
                        strRecordAll = strRecordAll & vbNewLine & "/"
                        strRecordAll = strRecordAll & vbNewLine & "/////FLIGHT NOT WAITING/////"
                    End If
                    strRecordAll = strRecordAll & vbNewLine & strRecordIndividual
                    
                    'igu on 28/06/2011
                    'add to dep flights special requirements collection
                    If pblnCopyToSpecialRequirements Then
                        colDepFlights.Add Array(strDepUrno, "NWTG")
                    End If
                End If
                
                If strDecision = "Waiting" Then
                    If InStr(strRecordAll, "/////FLIGHT WAITING/////") = 0 Then
                        strRecordAll = strRecordAll & vbNewLine & "/"
                        strRecordAll = strRecordAll & vbNewLine & "/////FLIGHT WAITING/////"
                    End If
                    strRecordAll = strRecordAll & vbNewLine & strRecordIndividual
                    
                    'igu on 28/06/2011
                    'add to dep flights special requirements collection
                    If pblnCopyToSpecialRequirements Then
                        colDepFlights.Add Array(strDepUrno, "WTG")
                    End If
                End If
            End If
        End If
        strPreAFTURNO = tabTab(1).GetFieldValue(tabLine, "AFTURNO")
    Next tabLine
    If strRecordAll <> "" Then
        'finishing telex content
        strRecordAll = strRecordAll & vbNewLine & "/"
        'strRecordAll = strRecordAll & vbNewLine & "SINRR/RS - PSE RBKD MISCON ON FIRAV FLT IF ANY"
'        strRecordAll = strRecordAll & vbNewLine & "SINLL/LK/LR/LO- DAPO TRANSFER BAGGAGES"
'        strRecordAll = strRecordAll & vbNewLine & "SINKY-PSE ADVISE ALO"
'        strRecordAll = strRecordAll & vbNewLine & "SINKN-PSE UPDATE SYSTEM"
'        strRecordAll = strRecordAll & vbNewLine & "SINKK/KJ - PSE MAAS PAX AND ENSURE ESCORTED FOR DEP IF ANY"
        strRecordAll = strRecordAll & vbNewLine & "RGDS"
        
        'igu on 28/06/2011
        'feed the special requirements variables
        If pblnCopyToSpecialRequirements Then
            strArrFlno = Replace(tabTab(0).GetFieldValue(0, "FLNO_A"), " ", "")
            frmEditTelex.SetSpecialReqVars strArrFlno, colDepFlights
        End If
                    
        frmEditTelex.txtTelexText.Text = Replace(strRecordAll, ",", " ")
        frmEditTelex.Show
        opened = True
    Else
        strRecordAll = "No flight selected/no decision made for departure flight(s)"
        frmEditTelex.txtTelexText.Text = Replace(strRecordAll, ",", " ")
        frmEditTelex.Show
        opened = True
'        frmSendTelex.txtSendTelex.Text = Replace(strRecordAll, ",", " ")
'        frmSendTelex.Show
'        If MsgBox("Do you still want to launch Telexpool ?", vbYesNo, "Note") = vbYes Then
'            opened = True
'        End If
    End If
    'Call telexpool
'    If opened = True Then
'        MyPath = "D:\Ufis\Appl\TelexPool.exe /CONNECTED ULDManager"
'        RetVal = Shell(MyPath, vbNormalFocus)
'    End If
    
    tabTab(1).Sort "6,23,7", True, True
End Sub

Private Sub cmdTelex_Click()
    Dim llLine As Long
    Dim strLoaUrno As String
    
    If cmdTelex.Value = 1 Then 'pressed
        llLine = tabTab(1).GetCurrentSelected
        ShowTelexForLine llLine
    Else
        cmdTelex.backColor = vbButtonFace
        frmTelex.Hide
    End If
End Sub

Sub ShowTelexForLine(tabLine As Long)
    Dim strLoaUrno As String
    If tabLine > -1 Then
        cmdTelex.backColor = vbGreen
        strLoaUrno = tabTab(1).GetFieldValue(tabLine, "LOA_RURN")
        If Trim(strLoaUrno) <> "" Then
            frmTelex.TabReadTelex strLoaUrno
            'kkh on 15/07/2008 P2 Excel Button RFC
            If cmdExcel.Value = 0 Then
                frmTelex.Show , Me
            Else
                frmTelex.Hide
            End If
            tabTab(1).SetFocus
        End If
    End If
End Sub

Private Sub Form_Load()
    Dim strCode As String
    Dim sngLeft As Single
    Dim sngTop As Single
    Dim sngWidth As Single
    Dim sngHeight As Single
    Dim regPercentX As Integer
    Dim regPercentY As Integer
    
    bgfrmLoadIsActive = False
    'Resize / adjust the width
    igCurrentPercentX = 4
    igCurrentPercentY = 50
    bmPositionActive = True
    bmIsInitialized = False
    Set MyBC = New BcPrxy
'Read position from registry and move the window
    sngLeft = Val(GetSetting(appName:="HUB_Manager_Main", _
                        section:="Startup", _
                        Key:="myLeft", _
                        Default:="-1"))
    sngTop = Val(GetSetting(appName:="HUB_Manager_Main", _
                        section:="Startup", _
                        Key:="myTop", _
                        Default:="-1"))
    sngWidth = Val(GetSetting(appName:="HUB_Manager_Main", _
                        section:="Startup", _
                        Key:="myWidth", _
                        Default:="-1"))
    sngHeight = Val(GetSetting(appName:="HUB_Manager_Main", _
                        section:="Startup", _
                        Key:="myHeight", _
                        Default:="-1"))
'    regPercentX = GetSetting(AppName:="HUB_Manager_Main", _
'                        section:="Startup", _
'                        Key:="myPercentX", _
'                        Default:=-1)
'    regPercentY = GetSetting(AppName:="HUB_Manager_Main", _
'                        section:="Startup", _
'                        Key:="myPercentY", _
'                        Default:=-1)
    If sngTop = -1 Or sngLeft = -1 Or sngWidth = -1 Or sngHeight = -1 Then
    Else
        sngLeft = Max(sngLeft, 0)
        sngTop = Max(sngTop, 0)
        sngWidth = Min(sngWidth, Screen.Width)
        sngHeight = Min(sngHeight, Screen.Height)
    
        Me.Move sngLeft, sngTop, sngWidth, sngHeight
    End If
'    If regPercentX = -1 Or regPercentY = -1 Then
'        igCurrentPercentX = 50
'        igCurrentPercentY = 70
'    Else
'        igCurrentPercentX = regPercentX
'        igCurrentPercentY = regPercentY
'    End If
    igCurrentPercentX = 42
    igCurrentPercentY = 50
'END: Read position from registry and move the window

    '************************
    'TO DO: Remove the next line
    bgSimulation = True
    frmData.Show
    lblSize(0).backColor = vbButtonFace 'vbButtonShadow
    lblSize(1).backColor = vbButtonFace 'vbButtonShadow
    lblSize(2).backColor = vbButtonFace 'vbButtonShadow

    Dim fromDat As Date
    Dim toDat As Date
    Dim strFrom As String
    Dim strTo As String
    
    lblAboveText(0).Caption = ""
    lblAboveText(1).Caption = ""
    gantt(0).ResetContent
    gantt(1).ResetContent
    
    fromDat = Now
    strFrom = Format(fromDat, "YYYYMMDD") & "000000"
    toDat = DateAdd("d", 1, fromDat)
    strTo = Format(toDat, "YYYYMMDD") & "235900"
    fromDat = CedaFullDateToVb(strFrom)
    toDat = CedaFullDateToVb(strTo)
    
    UTCOffset = frmData.GetUTCOffset
    omWKS = frmData.aUfis.GetWorkStationName
    
    gantt(0).UTCOffsetInHours = -UTCOffset
    gantt(1).UTCOffsetInHours = -UTCOffset
    gantt(0).TimeFrameFrom = CedaFullDateToVb(strTimeFrameFrom)  'dateFrom
    gantt(0).TimeFrameTo = CedaFullDateToVb(strTimeFrameTo)
    gantt(0).TimeScaleDuration = 4                         '10 hours are on screen
    gantt(0).AutoScroll = True
    gantt(1).TimeFrameFrom = CedaFullDateToVb(strTimeFrameFrom)  'dateFrom
    gantt(1).TimeFrameTo = CedaFullDateToVb(strTimeFrameTo)
    gantt(1).TimeScaleDuration = 4                         '10 hours are on screen
    gantt(1).AutoScroll = True
    gantt(0).LifeStyle = True
    gantt(1).LifeStyle = True
    slDuration.Value = 4
    slFontSize.Value = 14
    rbArea(3).Value = True
    
    timerUpdateTimefields.Enabled = True
    timerUpdateTimefields_Timer
    
    'kkh on 09/07/2008 P2 Security Features for function/button
    SetSecurity
End Sub
'kkh on 09/07/2008 P2 Security Features for function/button
Private Sub SetSecurity()
    Dim strPrivRet As String
        
    strPrivRet = frmData.ULogin.GetPrivileges("Load")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        cbToolButton(0).Visible = False 'Load button
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Now")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        cbToolButton(1).Visible = False 'Now button
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Compressed_Flights")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        cbToolButton(2).Visible = False 'Compressed Flights button
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Hourly_Flights")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        cbToolButton(3).Visible = False 'Hourly flights button
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Mail_Cargo")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        cbToolButton(5).Visible = False 'Mail/Cargo button
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Plain_Gannt")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        cbToolButton(8).Visible = False 'Mail/Cargo button
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Filter")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        cbToolButton(9).Visible = False 'Filter button
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Gate")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        cbToolButton(10).Visible = False 'Gate button
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Connection_Times")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        cbToolButton(6).Visible = False 'Connection_Times button
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Hourly_Pax")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        cbToolButton(7).Visible = False 'Hourly_Pax button
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Setup")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        cbToolButton(4).Visible = False
    End If
    
    strPrivRet = frmData.ULogin.GetPrivileges("Area_1")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        rbArea(0).Visible = False
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Area_2")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        rbArea(1).Visible = False
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Area_3")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        rbArea(2).Visible = False
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Area_All")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        rbArea(3).Visible = False
    End If
    
    strPrivRet = frmData.ULogin.GetPrivileges("Telex")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        cmdTelex.Visible = False
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Print")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        cmdPrint.Visible = False
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Insert")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        cmdInsert.Visible = False
    End If
    strPrivRet = frmData.ULogin.GetPrivileges("Delete")
    If strPrivRet = "-" Or strPrivRet = "0" Then
        cmdDelete.Visible = False
    End If
    If strAddiFeature = "YES" Then
        strPrivRet = frmData.ULogin.GetPrivileges("Send_Telex")
        If strPrivRet = "-" Or strPrivRet = "0" Then
            cmdSendTelex.Visible = False
        End If
        strPrivRet = frmData.ULogin.GetPrivileges("Excel")
        If strPrivRet = "-" Or strPrivRet = "0" Then
            cmdExcel.Visible = False
        End If
        'kkh on 25/07/2008 P2 View Decision Log
        strPrivRet = frmData.ULogin.GetPrivileges("View_Log")
        If strPrivRet = "-" Or strPrivRet = "0" Then
            cbToolButton(11).Visible = False 'View Log button
        End If
    Else
        cmdSendTelex.Enabled = False
        cmdExcel.Enabled = False
        cbToolButton(11).Enabled = False
    End If
End Sub
Public Sub InitConnTabs()
    Dim strColors As String
    Dim i As Long
    Dim strColor As String
    Dim strComboValues As String
    Dim strPrivRet As String
    
    strColors = CStr(16761024) + "," + CStr(16761024)
    tabTab(0).ResetContent
    tabTab(0).MainHeader = True
    tabTab(0).ShowHorzScroller (True)
'    tabTab(0).HeaderString = "FLNO,Date,STA,ETA,ONBL,Na,Org,Via,Pos,Ter,Gat,A/C,Regn,FLNO,Date,STD,ETD,OFBL,Na,Via,Des,Pos,Ter,Gat,Regn,"
'    tabTab(0).LogicalFieldList = "FLNO_A,DATE_A,STA,ETA,ONBL,NA_A,ORG,VIA_A,POS_A,TGA_A,GAT_A,A/C_A,REGN_A,FLNO_D,DATE_D,STD,ETD,OFBL,NA_D,VIA_D,DES,POS_D,TGD_D,GAT_D,REGN_D,"
'    tabTab(0).HeaderLengthString = "50,47,35,35,35,40,30,30,30,30,30,30,50,50,47,35,35,35,40,30,30,30,30,30,50,100"
        
    tabTab(0).HeaderString = "FLNO,STOA,Date,STA,ETA,ONBL,Na,Org,Via,Pos,Ter,Gat,A/C,Regn,FLNO,Date,STD,ETD,OFBL,Na,Via,Des,Pos,Ter,Gat,Urno,Flight_date"
    tabTab(0).LogicalFieldList = "FLNO_A,STOA_A,DATE_A,STA,ETA,ONBL,NA_A,ORG,VIA_A,POS_A,TGA_A,GAT_A,A/C_A,REGN_A,FLNO_D,DATE_D,STD,ETD,OFBL,NA_D,VIA_D,DES,POS_D,TGD_D,GAT_D,URNO_A,FLIGHT_DATE"
    'tabTab(0).HeaderLengthString = "55,0,52,45,45,35,40,30,30,30,30,30,30,55,55,52,45,45,35,40,30,30,30,30,30,0"
    tabTab(0).HeaderLengthString = "55,0,52,48,48,48,40,30,30,30,30,30,30,55,55,52,48,48,48,40,30,30,30,30,30,0,0"

    tabTab(0).SetMainHeaderValues "12,13", "Arrival,Departure", strColors
    tabTab(0).FontName = "Arial"
    tabTab(0).FontSize = 14
    tabTab(0).HeaderFontSize = 14
    tabTab(0).EnableHeaderSizing (True)
    tabTab(0).SetTabFontBold True
    tabTab(0).LeftTextOffset = 2
    
    tabTab(1).ResetContent
    tabTab(1).ShowHorzScroller (False)
    tabTab(1).HeaderString = "AFTURNO,LOA_RURN,ACT,FLNO,Ter,Gate,STD,Est.,ATD,Via/Dest,Routing,F-Pax,F-Pc/Wt,C-Pax,C-Pc/Wt,U-Pax,U-Pc/Wt,Y-Pax,Y-Pc/Wt,T-Pax,T-Pc/Wt,Na,Decision,,Transf.,,, "
    tabTab(1).LogicalFieldList = "AFTURNO,LOA_RURN,ACT,FLNO,TGD_D,GATE,STD,EST,ATD,DES,VIAnDES,F-PAX,F-PC/WT,C-PAX,C-PC/WT,U-PAX,U-PC/WT,Y-PAX,Y-PC/WT,T-PAX,T-PC/WT,NA,Hold,COMBO,TRANSFER,DUMMY,L_URNOS,CFI_URNO"
    tabTab(1).HeaderLengthString = "0,0,40,60,30,40,60,60,60,60,60,40,0,40,0,0,0,40,0,40,40,30,60,20,60,500,90,0"
    If strHopo = "BKK" Then
        tabTab(1).HeaderLengthString = "0,0,40,60,30,40,60,60,60,60,60,40,40,40,40,40,40,40,40,40,40,30,60,20,60,500,90,0"
    End If
    
    tabTab(1).ColumnWidthString = "10,10,4,12,14,14,14,14,14,3,3,3,3,3,3,3,3,3,3,3,3,5,100,5,5,100,10"
    
    'tabTab(1).ShowRowSelection = False
    tabTab(1).FontName = "Arial"
    tabTab(1).FontSize = 14
    tabTab(1).HeaderFontSize = 14
    tabTab(1).SetTabFontBold True
    tabTab(1).EnableHeaderSizing True
    tabTab(1).LeftTextOffset = 2
    strColor = vbBlue & "," & vbBlue
    'tabTab(1).CursorDecoration "AFTURNO,LOA_RURN,ACT,FLNO,TGD_D,GATE,STD,EST,ATD,DES,T-PAX,F-PAX,B-PAX,E-PAX,T-PC/WT,F-PC/WT,B-PC/WT,E-PC/WT,NA,Hold,TRANSFER,DUMMY", "B,T", "2,2", strColor
    tabTab(1).CursorDecoration "AFTURNO,LOA_RURN,ACT,FLNO,TGD_D,GATE,STD,EST,ATD,DES,VIAnDES,F-PAX,F-PC/WT,C-PAX,C-PC/WT,U-PAX,U-PC/WT,Y-PAX,Y-PC/WT,T-PAX,T-PC/WT,NA,Hold,TRANSFER,DUMMY", "B,T", "2,2", strColor
    tabTab(1).DefaultCursor = False
    tabTab(1).EnableInlineEdit True
    tabTab(1).InplaceEditUpperCase = False
    '"AFTURNO,LOA_RURN,ACT,FLNO,DATE,STD,EST,ATD,DES,T-PAX,F-PAX,B-PAX,E-PAX,NA,Hold,DUMMY,L_URNOS"
    'focus means not editable
    tabTab(1).NoFocusColumns = "0,1,2,3,4,5,6,7,8,9,10,22"
    strColor = CStr(vbYellow)
    tabTab(1).CreateDecorationObject "HOLD", "BTR", "11", strColor
    tabTab(1).CreateDecorationObject "USERENTRY", "L", "2", CStr(vbCyan)
      
    strPrivRet = frmData.ULogin.GetPrivileges("Combo_Decision")
    
    If strPrivRet = "1" Then
'        Handle the "Decision" combobox
'        Reset frmMain DECISION dropdown
'        If bgfrmMainOpen = True Then
'            tabTab(1).ComboResetObject "DECISION"
'        End If
'        Customize for GOCC 25/02/2008

        'If bgfrmMainOpen = False Then
            tabTab(1).ComboResetObject "DECISION"
            tabTab(1).CreateComboObject "DECISION", 23, 1, "", 130
            Dim Decision() As String
            
            Decision = Split(strDecisionOption, ",")
            'strComboValues = " " & vbLf
            For i = 0 To UBound(Decision)
                strComboValues = strComboValues & Decision(i) & vbLf
            Next i
            'strComboValues = strComboValues & " " & vbLf
'            strComboValues = " " & vbLf
'            strComboValues = strComboValues & "Waiting" & vbLf
'            strComboValues = strComboValues & "Not Waiting" & vbLf
'            strComboValues = strComboValues & "Expedite" & vbLf
'            strComboValues = strComboValues & " " & vbLf
            
            tabTab(1).ComboSetColumnLengthString "DECISION", "120"
            tabTab(1).SetComboColumn "DECISION", 23
    
            tabTab(1).ComboAddTextLines "DECISION", strComboValues, vbLf
            tabTab(1).ComboMinWidth = 18
    End If
      
    For i = 6 To 8
        tabTab(1).DateTimeSetColumn i
        tabTab(1).DateTimeSetColumnFormat i, "YYYYMMDDhhmmss", "hh':'mm'/'DD"
    Next i
End Sub

Public Sub NewSize()
    Dim FromY As Long
    Dim llWidth As Long
    Dim llHeight As Long
    Dim FromX As Long
    Dim ToX As Long
    Dim llW As Long
    Dim HorzSplitterPos As Long
    Dim VertSplitterPos As Long
    Dim minY As Long
    Dim minX As Long
    Dim maxY As Long
    
    minX = 0
    
    slFontSize.Move 100, Me.Height - 1250
    slDuration.Move 2000, Me.Height - 1250
    If Me.Width - lblAboveText(1).Left - 170 > 0 Then
        lblAboveText(1).Move lblAboveText(1).Left, lblAboveText(1).Top, Me.Width - lblAboveText(1).Left - 170
    End If
    
    minY = lblAboveText(1).Top + lblAboveText(1).Height ' + 150
    maxY = slDuration.Top - 50
    
    llWidth = Me.ScaleWidth
    llHeight = Me.ScaleHeight
    HorzSplitterPos = ((llHeight - slFontSize.Height - 300) / 100) * igCurrentPercentY
    VertSplitterPos = (llWidth / 100) * igCurrentPercentX
    If VertSplitterPos < 0 Then
        VertSplitterPos = 0
    End If
    If HorzSplitterPos < 0 Then
        HorzSplitterPos = 0
    End If
    lblSize(0).Left = 0
    If HorzSplitterPos > minY And HorzSplitterPos < maxY Then
        lblSize(0).Top = HorzSplitterPos
    Else
        If HorzSplitterPos < minY Then
            lblSize(0).Top = minY
        End If
        If HorzSplitterPos > maxY Then
            HorzSplitterPos = maxY
        End If
    End If
    lblSize(0).Width = Me.ScaleWidth
    lblSize(1).Top = lblSize(0).Top + lblSize(1).Width
    If VertSplitterPos > minX Then
        lblSize(1).Left = VertSplitterPos
    Else
        lblSize(1).Left = minX
    End If
    If maxY - lblSize(1).Top > 0 Then
        lblSize(1).Height = maxY - lblSize(1).Top
    End If
    lblSize(2).Top = lblSize(0).Top
    lblSize(2).Left = lblSize(1).Left
    If Me.Width - 3000 > 0 Then
        fraToolbar(0).Width = Me.Width - 3000
    End If
'    TrafficLight.left = Me.Width - 700
    ServerSignal(0).Left = Me.Width - 700
    ServerSignal(1).Left = Me.Width - 950
    lblTime(1).Left = ((Me.Width - 200) - (lblTime(1).Width)) ' - lblTime(1).left))
    lblTime(0).Left = lblTime(1).Left - 32 - lblTime(0).Width
    RecalcObjectPositions
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Dim sngLeft As Single
    Dim sngTop As Single
    Dim sngHeight As Single
    Dim sngWidth As Single
    
    sngLeft = Me.Left
    sngTop = Me.Top
    sngWidth = Me.Width
    sngHeight = Me.Height
    
    SaveSetting "HUB_Manager_Main", _
                "Startup", _
                "myLeft", _
                CStr(sngLeft)
    SaveSetting "HUB_Manager_Main", _
                "Startup", _
                "myTop", _
                CStr(sngTop)
    SaveSetting "HUB_Manager_Main", _
                "Startup", _
                "myWidth", _
                CStr(sngWidth)
    SaveSetting "HUB_Manager_Main", _
                "Startup", _
                "myHeight", _
                CStr(sngHeight)
'    SaveSetting "HUB_Manager_Main", _
'                "Startup", _
'                "myPercentX", igCurrentPercentX
'    SaveSetting "HUB_Manager_Main", _
'                "Startup", _
'                "myPercentY", igCurrentPercentY
End Sub

Private Sub Form_Resize()
    Dim i As Integer
    NewSize
    RefreshAllControls
    DrawBackGround picToolBar, 7, True, False
    DrawBackGround lblSize(0), 7, True, True
    DrawBackGround lblSize(1), 7, False, True
    picToolBar.Left = 0
    picToolBar.Width = Me.ScaleWidth
    For i = 0 To cbToolButton.count - 1
        Set cbToolButton(i).Container = picToolBar
    Next i
    For i = 0 To lblTime.count - 1
        Set lblTime(i).Container = picToolBar
    Next i
'    Set TrafficLight.Container = picToolBar
    Set lblAboveText(0).Container = picToolBar
    Set lblAboveText(1).Container = picToolBar
    For i = 0 To 1
        Set ServerSignal(i).Container = picToolBar
    Next i
    For i = 0 To rbArea.count - 1
        Set rbArea(i).Container = picToolBar
        rbArea(i).Refresh
    Next i
    MainStatusBar.ZOrder
    picToolBar.Refresh
    timerRepaint.Enabled = True
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If MsgBox("Do you really want to exit the application??", vbYesNo Or vbExclamation) <> vbYes Then
        Cancel = True
    Else
        Cancel = False
        'frmData.tabHold.WriteToFile "C:\Ufis\Appl\HUB_FLT_HOLD.cfg", False
        frmData.tabHold.WriteToFile UFIS_APPL & "\HUB_FLT_HOLD.cfg", False
        Unload frmData
        End
    End If
End Sub

Public Sub RecalcObjectPositions()
    If lblSize(0).Top - 20 - lblMainContainer.Top > 0 Then
        lblMainContainer.Height = lblSize(0).Top - 20 - lblMainContainer.Top
    End If
    If lblSize(0).Width - 50 > 0 Then
        lblMainContainer.Width = lblSize(0).Width - 50
    End If
    lblDetailContainer.Top = lblSize(0).Top + lblSize(0).Height + 20
    lblDetailContainer.Height = lblSize(1).Height
    If lblSize(1).Left - 20 > 0 Then
        lblDetailContainer.Width = lblSize(1).Left - 20
    Else
        lblDetailContainer.Width = 0
    End If
    lblTabContainer.Top = lblDetailContainer.Top
    lblTabContainer.Left = lblSize(1).Left + lblSize(1).Width + 20
    lblTabContainer.Height = lblDetailContainer.Height
    If lblMainContainer.Width - (lblSize(1).Left + lblSize(1).Width + 20) > 0 Then
        lblTabContainer.Width = lblMainContainer.Width - (lblSize(1).Left + lblSize(1).Width + 20)
    End If
    
    gantt(0).Visible = True
    gantt(1).Visible = True
    tabTab(0).Visible = True
    tabTab(1).Visible = True
    gantt(0).Top = lblMainContainer.Top + 40
    gantt(0).Left = lblMainContainer.Left + 40
    If lblMainContainer.Width - 80 > 0 Then
        gantt(0).Width = lblMainContainer.Width - 80
    Else
        gantt(0).Width = 0
    End If
    If lblMainContainer.Height - 80 > 0 Then
        gantt(0).Height = lblMainContainer.Height - 80
    Else
        gantt(0).Visible = False
    End If
    
    gantt(1).Top = lblDetailContainer.Top + 40
    gantt(1).Left = lblDetailContainer.Left + 40
    If lblDetailContainer.Width - 80 > 0 Then
        gantt(1).Width = lblDetailContainer.Width - 80
    Else
        gantt(1).Visible = False
    End If
    If lblDetailContainer.Height - 80 > 0 Then
        gantt(1).Height = lblDetailContainer.Height - 80
    Else
        gantt(1).Visible = False
    End If
    
    tabTab(0).Top = lblTabContainer.Top + 40
    tabTab(0).Left = lblTabContainer.Left + 40
    If lblTabContainer.Width - 80 > 0 Then
        tabTab(0).Width = lblTabContainer.Width - 80
    Else
        tabTab(0).Visible = False
    End If
    cmdTelex.Left = tabTab(0).Left + 50
    cmdPrint.Left = cmdTelex.Left + cmdTelex.Width + 50
    cmdTelex.Top = tabTab(0).Top + tabTab(0).Height + 80
    cmdPrint.Top = tabTab(0).Top + tabTab(0).Height + 80
    cmdInsert.Top = tabTab(0).Top + tabTab(0).Height + 80
    cmdInsert.Left = cmdPrint.Left + cmdPrint.Width + 50
    cmdDelete.Top = tabTab(0).Top + tabTab(0).Height + 80
    cmdDelete.Left = cmdInsert.Left + cmdInsert.Width + 50
    'kkh on 11/07/2008 P2 Send Telex RFC
    cmdSendTelex.Top = tabTab(0).Top + tabTab(0).Height + 80
    cmdSendTelex.Left = cmdDelete.Left + cmdDelete.Width + 50
    cmdExcel.Top = tabTab(0).Top + tabTab(0).Height + 80
    cmdExcel.Left = cmdSendTelex.Left + cmdSendTelex.Width + 50
    tabTab(1).Top = cmdPrint.Top + cmdPrint.Height + 50
    tabTab(1).Left = lblTabContainer.Left + 40
    If lblTabContainer.Width - 80 > 0 Then
        tabTab(1).Width = lblTabContainer.Width - 80
    Else
        tabTab(1).Visible = False
    End If
    
    If lblTabContainer.Height - (tabTab(0).Height + 500) > 0 Then
        tabTab(1).Height = lblTabContainer.Height - (tabTab(0).Height + 500) ' + 80)
    Else
        tabTab(1).Height = 0
    End If
    If tabTab(1).Height <= 30 Then
        tabTab(1).Visible = False
    End If
    If tabTab(0).Height <= 30 Then
        tabTab(0).Visible = False
    End If
    gantt(0).RefreshArea "ALL"
    gantt(1).RefreshArea "ALL"
    lblTip(0).Top = cmdInsert.Top + 30
    'kkh on 11/07/2008 P2 Send Telex RFC
    lblTip(0).Left = (cmdExcel.Left + cmdExcel.Width) + 100
    If ((lblTabContainer.Left + lblTabContainer.Width) - lblTip(0).Left - 100) > 0 Then
        lblTip(0).Width = (lblTabContainer.Left + lblTabContainer.Width) - lblTip(0).Left - 100
    End If
    If lblSize(1).Left = 0 Then
        gantt(1).Visible = False
        gantt(0).Visible = False
    End If
End Sub

Private Sub gantt_OnLButtonDblClkBar(Index As Integer, ByVal Key As String, ByVal nFlags As Integer)
    If Index = 0 Then
        ShowFeedingArrivals Key, Me
    End If
End Sub

Public Sub ShowFeedingArrivals(strAftUrno As String, myFrm As Form)
    Dim strUrno As String
    Dim llLine As Long
    Dim strLine As String
    Dim strAdid As String
    
    strLine = frmData.tabData(0).GetLinesByIndexValue("URNO", strAftUrno, 0)
    llLine = frmData.tabData(0).GetNextResultLine()
    If llLine > -1 Then
        strAdid = frmData.tabData(0).GetFieldValue(llLine, "ADID")
        If strAdid = "D" Then
            Load frmFeedingArrivals
            frmFeedingArrivals.Show , myFrm
            frmFeedingArrivals.strCalledFrom = "MAIN"
            frmFeedingArrivals.SetFlight strAftUrno
            frmFeedingArrivals.ShowFeedings
            'frmFeedingArrivals.Show , myFrm
        End If
    End If
End Sub


Private Sub lblSize_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblSize(1).Tag = X
    lblSize(0).Tag = Y
End Sub

Private Sub lblSize_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim diffX As Long
    Dim diffY As Long
    Dim leftW As Long
    Dim topW As Long
    Dim rightW As Long
    Dim bottomW As Long
    Dim ilLimit As Integer
    Dim llWidth As Long
    Dim llHeight As Long
    Dim HorzSplitterPos As Long
    Dim VertSplitterPos  As Long

    
    If Index = 0 Or Index = 2 Then
        If lblSize(0).Tag > 0 And Y <> lblSize(0).Tag Then
            diffY = Y - lblSize(0).Tag
            If lblSize(1).Height - diffY >= 0 Then
                lblSize(1).Height = lblSize(1).Height - diffY
                lblSize(1).Refresh
            End If
                If (lblSize(0).Top + diffY) < (slDuration.Top - 100) Then
                lblSize(0).Top = lblSize(0).Top + diffY
                lblSize(1).Top = lblSize(1).Top + diffY
                lblSize(2).Top = lblSize(0).Top
                RecalcObjectPositions
                lblSize(0).Refresh
                lblSize(1).Refresh
                lblSize(2).Refresh
                Me.Refresh
            End If
        End If
    End If
    
    If Index = 1 Or Index = 2 Then
        If lblSize(1).Tag > 0 And X <> lblSize(1).Tag Then
            diffX = X - lblSize(1).Tag
            lblSize(1).Left = lblSize(1).Left + diffX
            lblSize(2).Left = lblSize(1).Left
            RecalcObjectPositions
            lblSize(0).Refresh
            lblSize(1).Refresh
            lblSize(2).Refresh
            Me.Refresh
        End If
    End If
    llWidth = Me.ScaleWidth
    llHeight = Me.ScaleHeight
    HorzSplitterPos = lblSize(0).Top
    VertSplitterPos = lblSize(1).Left
    igCurrentPercentY = (HorzSplitterPos * 100) / llHeight
    igCurrentPercentX = (VertSplitterPos * 100) / llWidth
    
End Sub

Private Sub lblSize_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblSize(1).Tag = -1
    lblSize(0).Tag = -1
    gantt(0).RefreshArea "GANTT"
    gantt(1).RefreshArea "GANTT"
End Sub

Private Sub mnuAbout_Click()
    Dim strCtlNames As String
    Dim myControls() As Control
    
    strCtlNames = "BcProxy.exe," & Replace(MyBC.Version, ",", ".", 1, -1, vbBinaryCompare) & "," & MyBC.BuildDate & ",Out-process server for broadcasts" & vbLf
    MyAboutBox.SetLifeStyle = True
    MyAboutBox.SetComponents myControls, strCtlNames
    MyAboutBox.Show vbModal, Me
End Sub

Private Sub mnuAlternateConnections_Click()
    Dim llLine As Long
    Dim strDepUrno As String
    Dim strArrUrno As String
'    Dim strFlno As String
'    Dim strAlc As String
    Dim strRet As String
    Dim strDestOrVIA As String
'    Dim strAFTDest As String
    Dim i As Long
    Dim hasConflict As Boolean
    Dim strValues As String
    Dim strParaLines As String
    Dim cnt As Long
    'KKH GOCC prf on 18/03/2008
    Dim strTIFD As Date 'STOD value instead
    Dim strTifa As Date
    
    Dim strVIAL As String
    llLine = tabTab(1).GetCurrentSelected
    strDepUrno = tabTab(1).GetFieldValue(llLine, "AFTURNO")
    strTIFD = CedaFullDateToVb(tabTab(1).GetFieldValue(llLine, "STD"))
    'kkh on 07/07/2008 GOCC alternative flight base on VIA/DES
    strDestOrVIA = tabTab(1).GetFieldValue(llLine, "DES")
'    strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", strDepUrno, 0)
'    llLine = frmData.tabData(0).GetNextResultLine
'    If llLine > -1 Then
'        strFlno = frmData.tabData(0).GetFieldValue(llLine, "FLNO")
'        If Trim(strFlno) <> "" Then
'            strAlc = Mid(strFlno, 1, 3)
'            strAlc = Trim(strAlc)
'            'kkh on 07/07/2008 GOCC alternative flight base on VIA/DES
'            strAFTDest = frmData.tabData(0).GetFieldValue(llLine, "DES3")
'        End If
'    End If
    tabTmpFlights.ResetContent
    tabTmpFlights.HeaderString = "URNO,FLNO,TIFD,AFTLINE"
    tabTmpFlights.LogicalFieldList = "URNO,FLNO,TIFD,AFTLINE"
    tabTmpFlights.HeaderLengthString = "60,60,100,100"
    
    'kkh 31/07/2008 VIAL
    If strDestOrVIA <> "" Then
        For i = 0 To frmData.tabData(0).GetLineCount - 1
            strVIAL = frmData.tabData(0).GetFieldValue(i, "VIAL")
            If InStr(strVIAL, strDestOrVIA) > 0 Then
                If frmData.tabData(0).GetFieldValues(i, "ADID") = "D" Or frmData.tabData(0).GetFieldValues(i, "ADID") = "B" Then
                    insertTabTmpFlights i, strDepUrno, strTIFD, strTifa
                End If
            Else
                strRet = frmData.tabData(0).GetFieldValue(i, "DES3")
                If InStr(strRet, strDestOrVIA) > 0 Then
                    If frmData.tabData(0).GetFieldValues(i, "ADID") = "D" Or frmData.tabData(0).GetFieldValues(i, "ADID") = "B" Then
                        insertTabTmpFlights i, strDepUrno, strTIFD, strTifa
                    End If
                End If
            End If
        Next i
    End If
    
    For i = 0 To tabTmpFlights.GetLineCount - 1
        hasConflict = CheckConnectionTimes(omCurrentSelectedUrno, tabTmpFlights.GetFieldValue(i, "URNO"), -1, Val(tabTmpFlights.GetFieldValue(i, "AFTLINE")))
        If hasConflict = False Then
            tabTmpFlights.SetLineColor i, vbBlack, vbGreen
        End If
    Next i
    strRet = tabTmpFlights.GetLinesByBackColor(vbGreen)
    If strRet <> "" Then
        cnt = ItemCount(strRet, ",") - 1
        For i = 0 To cnt
            If (i + 1) > cnt Then
                strParaLines = strParaLines + tabTmpFlights.GetFieldValue(GetRealItem(strRet, CInt(i), ","), "AFTLINE")
            Else
                strParaLines = strParaLines + tabTmpFlights.GetFieldValue(GetRealItem(strRet, CInt(i), ","), "AFTLINE") + ","
            End If
        Next i
        If strParaLines <> "" Then
            frmSimpleList.omListMode = "ALTERNATE_FLIGHTS"
            SetFormOnTop frmSimpleList, True
            frmSimpleList.Show
            frmSimpleList.ShowAlternateFlights strParaLines, strDestOrVIA
            frmSimpleList.ZOrder
        Else
            MsgBox "No alternative connections found in current loaded timeframe!"
        End If
    Else
        MsgBox "No alternative connection found!"
    End If
End Sub
'kkh 31/07/2008 VIAL
Private Sub insertTabTmpFlights(llLine As Long, strDepUrno As String, strTIFD As Date, strTifa As Date)
    Dim strValues As String
    'KKH GOCC PRF do not display the current selected connecting flight but only other alternative flights
    If strDepUrno <> frmData.tabData(0).GetFieldValues(llLine, "URNO") Or strTIFD > strTifa Then
        strValues = frmData.tabData(0).GetFieldValues(llLine, "URNO,FLNO,TIFD") & "," & llLine
        tabTmpFlights.InsertTextLine strValues, False
    End If
End Sub
Private Sub mnuExit_Click(Index As Integer)
    ShutDownApplication True
End Sub

Private Sub mnuFeedingFlights_Click()
    Dim llLine As Long
    Dim strDepUrno As String
    
    llLine = tabTab(1).GetCurrentSelected
    strDepUrno = tabTab(1).GetFieldValue(llLine, "AFTURNO")
    Me.ShowFeedingArrivals strDepUrno, Me
End Sub

Private Sub mnuFlightOnGo_Click()
    Dim llLine As Long
    Dim strDepUrno As String
    
    llLine = tabTab(1).GetCurrentSelected
    strDepUrno = tabTab(1).GetFieldValue(llLine, "AFTURNO")
    UnmarkLineAsHold llLine, strDepUrno
End Sub

Private Sub mnuFlightOnHold_Click()
    Dim llLine As Long
    Dim strDepUrno As String
    
    llLine = tabTab(1).GetCurrentSelected
    strDepUrno = tabTab(1).GetFieldValue(llLine, "AFTURNO")
    MarkLineAsHold llLine, strDepUrno
End Sub

Public Sub MyBC_OnBcReceive(ByVal ReqId As String, ByVal Dest1 As String, _
                             ByVal Dest2 As String, ByVal Cmd As String, ByVal Object As String, _
                             ByVal Seq As String, ByVal Tws As String, ByVal Twe As String, _
                             ByVal Selection As String, ByVal Fields As String, ByVal Data As String, _
                             ByVal BcNum As String)
    Dim strRet As String
    Dim strUrno As String
    Dim llItem As Long
    Dim llTabLine As Long
    Dim ItemNo As Long
    Dim strVal As String
    Dim strValues As String
    Dim strTifa As String
    Dim strTIFD As String
    Dim strOldTIFA As String
    Dim strOldTIFD As String
    Dim strLineValues As String
    Dim ttyp As String
    Dim strSkey As String
    Dim strAdid As String
    Dim strUsec As String
    Dim strFlnu As String
    
    'kkh on 16/07/2008 P2 Store Decision into CFITAB
    Dim strTYPE As String
    
    Dim strLineNo As String 'igu on 27 Jul 2010
    
    llItem = GetRealItemNo(Fields, "URNO")
    strUrno = GetRealItem(Data, llItem, ",")
    If ServerSignal(0).FillColor = vbBlue Then
        ServerSignal(0).FillStyle = vbSolid
        ServerSignal(0).FillColor = vbGreen
        ServerSignal(1).FillStyle = vbSolid
        ServerSignal(1).FillColor = vbBlue
    Else
        ServerSignal(0).FillStyle = vbSolid
        ServerSignal(0).FillColor = vbBlue
        ServerSignal(1).FillStyle = vbSolid
        ServerSignal(1).FillColor = vbGreen
    End If
    ServerSignal(0).Refresh
    ServerSignal(1).Refresh
    Select Case (Cmd)
        Case "CLO"
            MsgBox "The server had switched." & vbCrLf & "You can restart the application in one minute."
            End
        Case "RAC"
            strSkey = ""
        Case "DFR"
            strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", strUrno, 0)
            llTabLine = frmData.tabData(0).GetNextResultLine
            If llTabLine <> -1 Then
                strUrno = frmData.tabData(0).GetFieldValue(llTabLine, "URNO")
                gantt(0).DeleteBarByKey strUrno
                gantt(1).DeleteBarByKey strUrno
                gantt(0).Refresh
                gantt(1).Refresh
                If bgfrmCompressedFlightsOpen = True Then
                    frmCompressedFligths.gantt.DeleteBarByKey strUrno
                    frmCompressedFligths.gantt.Refresh
                End If
                frmData.tabData(0).DeleteLine llTabLine
                If bgMailCargoIsActive = True Then
                    frmMailCargo.OnBc_DeleteFlight strUrno
                End If
            End If
        Case "UFR", "UPS", "UPJ"
            'UTC/LOCAL time
            HandleLoacUTC Fields, Data
            HandleUFR strUrno, Fields, Data 'TO DO 'N'=NOOP,'X'=CANCEL,'D'=DIVERTED behandeln
            SingleBarConflictCheck strUrno
            frmData.tabData(0).GetLinesByIndexValue "URNO", strUrno, 0
            llTabLine = frmData.tabData(0).GetNextResultLine
            If llTabLine > -1 Then
                strAdid = frmData.tabData(0).GetFieldValue(llTabLine, "ADID")
                If strAdid = "D" Then
                    CheckArrivalsForDeparture strUrno
                End If
                UpdateBar llTabLine
            End If
            
            If bgMailCargoIsActive = True Then
                frmMailCargo.OnBc_UpdateFlight strUrno 'TO DO 'N'=NOOP,'X'=CANCEL,'D'=DIVERTED behandeln
            End If
        Case "ISF"
            'HandleLoacUTC Fields, data
            ItemNo = GetRealItemNo(Fields, "FTYP")
            If ItemNo > -1 Then
                strVal = GetRealItem(Data, ItemNo, ",")
                If InStr(strAllFtyps, strVal) > 0 Then 'Check if relevant FTYP
                    ItemNo = GetRealItemNo(Fields, "TIFA")
                    If ItemNo > -1 Then strTifa = GetRealItem(Data, ItemNo, ",")
                    ItemNo = GetRealItemNo(Fields, "TIFA")
                    If ItemNo > -1 Then strTIFD = GetRealItem(Data, ItemNo, ",")
                    If (strTifa >= strTimeFrameFrom And strTifa <= strTimeFrameTo) Or _
                       (strTIFD >= strTimeFrameFrom And strTIFD <= strTimeFrameTo) Then
                       ItemNo = GetRealItemNo(Fields, "SKEY")
                        If ItemNo > -1 Then
                            strSkey = GetRealItem(Data, ItemNo, ",")
                            strSkey = Trim(strSkey)
                            If strSkey <> "" Then
                                HandleISF strSkey
                            End If
                        End If
                    End If
                    If bgMailCargoIsActive = True Then
                        frmMailCargo.OnBc_InsertFlight strUrno
                    End If
                End If
            End If
        Case "URT"
            'UTC/LOCAL time
            HandleLoacUTC Fields, Data
            If Object = "TLXTAB" Then
                ItemNo = GetRealItemNo(Fields, "TTYP")
                If ItemNo > -1 Then
                    ttyp = GetRealItem(Data, ItemNo, ",")
                    'If ttyp = "PTM" Or ttyp = "KRIS" Then 'igu on 23/08/2011: ALTEA
                    If ttyp = "PTM" Or ttyp = "KRIS" Or ttyp = "ALT" Then 'igu on 23/08/2011: ALTEA
                        ItemNo = GetRealItemNo(Fields, "FLNU")
                        If ItemNo > -1 Then
                            strUrno = GetRealItem(Data, ItemNo, ",")
                            strUrno = Trim(strUrno)
                            If strUrno <> "" And strUrno <> "0" Then
                                'Just buffer it in a tab ==> a timer will reread every minute
                                'LOATAB with WHERE FLNU in <content of tab>
                                tabFlnuBcs.InsertTextLine strUrno, False
                                'HandleTlxBC strUrno
                                'PAX figure Broadcast
                                HandleTlxBC
                                HandleTlxBCSingle strUrno
                            End If
                        End If
                    End If
                End If
            End If
            'kkh on 16/07/2008 P2 Store Decision into CFITAB
            If Object = "CFITAB" Then
                ItemNo = GetRealItemNo(Fields, "URNO")
                If ItemNo > -1 Then
                    strUrno = GetRealItem(Data, ItemNo, ",")
                    If strUrno <> "" And strUrno <> "0" Then
                        'llTabLine = frmData.tabData(13).GetLinesByIndexValue("URNO", strUrno, 0) 'igu on 27 Jul 2010
                        strLineNo = frmData.tabData(13).GetLinesByIndexValue("URNO", strUrno, 0) 'igu on 27 Jul 2010
                        If strLineNo = "" Then llTabLine = -1 Else llTabLine = Val(strLineNo) 'igu on 27 Jul 2010
                        If llTabLine > -1 Then
'                            strVal = "ADDI,USEU,LSTU,URNO"
'                            frmData.tabData(13).SetFieldValues llTabLine, strVal, Data
                            frmData.tabData(13).SetFieldValues llTabLine, Fields, Data
                            HandleTlxBCSingle strUrno
                        End If
                    End If
                End If
            End If
        Case "IRT"
'            If Object = "SRLTAB" Then
'                itemno = GetRealItemNo(Fields, "USEC")
'                If itemno > -1 Then
'                    strUsec = GetRealItem(Data, itemno, ",")
'                    If strUsec = "HUBMGR" Then
'                        itemno = GetRealItemNo(Fields, "FLNU")
'                        If itemno > -1 Then
'                            strFlnu = Trim(GetRealItem(Data, itemno, ","))
'                            'strFlnu = Trim(strUrno)
'                            strRet = frmData.tabData(10).GetLinesByIndexValue("FLNU", strFlnu, 0)
'                            llTabLine = frmData.tabData(10).GetNextResultLine
'                            If llTabLine > -1 Then
'                                While llTabLine > -1
'                                    frmData.tabData(10).SetFieldValues llTabLine, Fields, Data
'                                    llTabLine = frmData.tabData(10).GetNextResultLine
'                                Wend
'                            Else
'                                frmData.tabData(10).InsertTextLine ",,,,,,,,,,,,,,,", False
'                                frmData.tabData(10).SetFieldValues frmData.tabData(10).GetLineCount - 1, Fields, Data
'                                frmData.tabData(10).Sort "1", True, True
'                            End If
'                        End If
'                    End If
'                End If
'            End If
            HandleLoacUTC Fields, Data
            'kkh on 16/07/2008 P2 Store Decision into CFITAB
            If Object = "CFITAB" Then
                ItemNo = GetRealItemNo(Fields, "TYPE")
                If ItemNo > -1 Then
                    strTYPE = GetRealItem(Data, ItemNo, ",")
                    If strTYPE = "HUB" Then
                        ItemNo = GetRealItemNo(Fields, "URNO")
                        If ItemNo > -1 Then
                            'igu on 27 Jul 2010
                            'check if record already inserted in local TAB
                            '---------------------------------------------
                            strUrno = GetRealItem(Data, ItemNo, ",")
                            ItemNo = GetRealItemNo(frmData.tabData(13).LogicalFieldList, "URNO")
                            
                            strLineNo = frmData.tabData(13).GetLinesByColumnValue(ItemNo, strUrno, 0)
                            If strLineNo = "" Then
                                llTabLine = -1
                            Else
                                llTabLine = Val(strLineNo)
                            End If
                            If llTabLine > -1 Then 'found, update
                                frmData.tabData(13).SetFieldValues llTabLine, Fields, Data
                            Else 'not found, insert
                                frmData.tabData(13).InsertTextLine ",,,,,,,,,,,,,,,,,,,,,,,,,,,", False
                                frmData.tabData(13).SetFieldValues frmData.tabData(13).GetLineCount - 1, Fields, Data
                            End If
                            frmData.tabData(13).Sort "1", True, True
                            'end of check if record already inserted in local TAB
                            '----------------------------------------------------
                            
                            ItemNo = GetRealItemNo(Fields, "AURN")
                            If ItemNo > -1 Then
                                strFlnu = Trim(GetRealItem(Data, ItemNo, ","))
                                frmData.tabData(0).GetLinesByIndexValue "URNO", strFlnu, 0
                                llTabLine = frmData.tabData(0).GetNextResultLine
                                If llTabLine > -1 Then
                                    'UpdateBar llTabLine
                                    HandleTlxBCSingle strFlnu
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        Case "SBC"
            Select Case (Object)
                Case "LOA:IRT"
                    If ReqId <> omWKS Then
                        If Selection <> "" Then
                            ItemNo = GetRealItemNo(Fields, "URNO")
                            If ItemNo > -1 Then
                                strUrno = GetRealItem(Data, ItemNo, ",")
                                strUrno = Trim(strUrno)
                            End If 'First we must check if this item exists, then do nothing == own bc
                            frmData.tabData(3).GetLinesByIndexValue "FLNU", strUrno, 0
                            'frmData.tabData(3).GetLinesByIndexValue "FLNU", Selection, 0
                            llTabLine = frmData.tabData(3).GetNextResultLine
                            If llTabLine = -1 Then
                                frmData.tabData(3).InsertTextLine Data, False
                                'kkh on 28/11/2008 Display KRI as higher priority then PTM message for all flights
                                'frmData.tabData(3).Sort "1,2,3", True, True  ' This reorgs the indexes as well!
                                frmData.tabData(3).Sort "3,2", True, True  ' This reorgs the indexes as well!
                                If Selection = omCurrentSelectedUrno Then
                                    gantt_OnLButtonDownBar 0, omCurrentSelectedUrno, 0
                                End If
                            Else
                                If Selection = omCurrentSelectedUrno Then
                                    gantt_OnLButtonDownBar 0, omCurrentSelectedUrno, 0
                                End If
                            End If
                            SingleBarConflictCheck Selection
                            frmData.tabData(0).GetLinesByIndexValue "URNO", Selection, 0
                            llTabLine = frmData.tabData(0).GetNextResultLine
                            If llTabLine > -1 Then
                                UpdateBar llTabLine
                            End If
                        End If
                    End If
                Case "LOA:URT"
                    If ReqId <> omWKS Then
                        If Selection <> "" Then
                            ItemNo = GetRealItemNo(Fields, "URNO")
                            If ItemNo > -1 Then
                                strUrno = GetRealItem(Data, ItemNo, ",")
                                strUrno = Trim(strUrno)
                            End If 'First we must check if this item exists, then do nothing == own bc
                            frmData.tabData(3).GetLinesByIndexValue "URNO", strUrno, 0
                            SingleBarConflictCheck (strUrno)
                            llTabLine = frmData.tabData(3).GetNextResultLine
                            If llTabLine > -1 Then
                                frmData.tabData(3).SetFieldValues llTabLine, frmData.tabData(3).LogicalFieldList, Data
                                If Selection = omCurrentSelectedUrno Then
                                    gantt_OnLButtonDownBar 0, omCurrentSelectedUrno, 0
                                End If
                            End If
                            SingleBarConflictCheck Selection
                            frmData.tabData(0).GetLinesByIndexValue "URNO", Selection, 0
                            llTabLine = frmData.tabData(0).GetNextResultLine
                            If llTabLine > -1 Then
                                UpdateBar llTabLine
                            End If
                        End If
                    End If
                Case "LOA:DRT"
                    If ReqId <> omWKS Then
                        If Selection <> "" Then
                            frmData.tabData(3).GetLinesByIndexValue "URNO", Selection, 0
                            llTabLine = frmData.tabData(3).GetNextResultLine
                            If llTabLine > -1 Then
                                frmData.tabData(3).DeleteLine llTabLine
                                'kkh on 28/11/2008 Display KRI as higher priority then PTM message for all flights
                                'frmData.tabData(3).Sort "1,2,3", True, True  ' This reorgs the indexes as well!
                                frmData.tabData(3).Sort "3,2", True, True  ' This reorgs the indexes as well!
                                If Data = omCurrentSelectedUrno Then
                                    gantt_OnLButtonDownBar 0, omCurrentSelectedUrno, 0
                                End If
                            End If
                            SingleBarConflictCheck Data
                            frmData.tabData(0).GetLinesByIndexValue "URNO", Data, 0
                            llTabLine = frmData.tabData(0).GetNextResultLine
                            If llTabLine > -1 Then
                                UpdateBar llTabLine
                            End If
                        End If
                    End If
            End Select
    End Select
End Sub

Public Sub HandleLoacUTC(Fields As String, Data As String)
    Dim cnt As Long
    Dim fldCnt As Integer
    Dim i As Integer
    Dim strField As String
    Dim strVal As String
    Dim myDat As Date
    Dim idxFld As Long
    Dim OldData As String
    
    OldData = Data
    
    If TimesInUTC = False Then
        fldCnt = ItemCount(strAftTimeFields, ",")
        For i = 0 To fldCnt
            strField = GetRealItem(strAftTimeFields, CInt(i), ",")
            idxFld = GetRealItemNo(Fields, strField)
            If idxFld > -1 Then
                strVal = GetRealItem(Data, idxFld, ",")
                If Trim(strVal) <> "" Then
                    myDat = CedaFullDateToVb(strVal)
                        myDat = DateAdd("h", UTCOffset, myDat)
                    strVal = Format(myDat, "YYYYMMDDhhmmss")
                    SetItem Data, idxFld + 1, ",", strVal
                End If
            End If
        Next i
    End If
End Sub
Sub HandleTlxBCSingle(strFlnu As String)
    
    Dim llLine As Integer
    Dim strStoa As Date
    Dim strFlno As String
    Dim strWhere As Date
    Dim strRurn As String
    
    Dim strUrnoList As String
    Dim strUrno As String
    Dim arrUrnos() As String
    Dim i As Integer
    
    frmData.tabData(0).GetLinesByIndexValue "URNO", omCurrentSelectedUrno, 0
    llLine = frmData.tabData(0).GetNextResultLine
    If llLine > -1 Then
        If Trim(frmData.tabData(0).GetFieldValue(llLine, "STOA")) <> "" Then
            strStoa = CedaFullDateToVb(frmData.tabData(0).GetFieldValue(llLine, "STOA"))
        End If
    End If
    
    frmData.tabData(0).GetLinesByIndexValue "URNO", strFlnu, 0
    llLine = frmData.tabData(0).GetNextResultLine
    If llLine > -1 Then
        strFlno = Replace(frmData.tabData(0).GetFieldValue(llLine, "FLNO"), " ", "")
    End If
    
    tabTab(1).GetLinesByIndexValue "FLNO", strFlno, 0
    llLine = frmData.tabData(0).GetNextResultLine
    If llLine > -1 Then
        strRurn = tabTab(1).GetFieldValue(llLine, "LOA_RURN")
    End If
    SetLoaTabInfoToTab omCurrentSelectedUrno, strStoa
End Sub
'-----------------------------------------------------
' Reloads the LOATAB records for this FLNU and
' reorgs the LOATAB in frmData.tabData(3)
' checks the flight with FLNU for connection conflicts
' and redraws the bar in the charts
'-----------------------------------------------------
Sub HandleTlxBC() '(strFlnu As String)
    Dim strRet As String
    Dim llCurrLine As Long
    Dim i As Long
    Dim strFlnu As String
    
    Me.MousePointer = vbHourglass
    ReloadLOATABForFlnu
    
    For i = 0 To tabFlnuBcs.GetLineCount - 1
        strFlnu = tabFlnuBcs.GetFieldValue(i, "FLNU")
        strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", strFlnu, 0)
        llCurrLine = frmData.tabData(0).GetNextResultLine
        If llCurrLine <> -1 Then
            'UpdateBar llCurrLine
            If omCurrentSelectedUrno = strFlnu Then
                gantt_OnLButtonDownBar 0, strFlnu, 0
            End If
        End If
    Next i
    InitFlnuBcTab
    Me.MousePointer = 0
End Sub
'------------------------------------------------
' Reorgs the loatab for a AFT.URNO or a TLX.FLNU
'------------------------------------------------
Public Sub ReloadLOATABForFlnu() ' (strFlnu As String)
    Dim strRet As String
    Dim cnt As Long
    Dim llCurrLine As Long
    Dim i As Long
    Dim strWhere As String
    Dim strRawFlno As String
    Dim strFlno As String
    Dim strFlightDay As String
    Dim arr() As String
    Dim strUrnoA As String
    Dim strStoa As String
    Dim datStoa As Date
    Dim llLine As Long
    Dim llDepLine As Long
    Dim dummy1 As String, dummy2 As String, dummy3 As String
    Dim strSearchValue As String
    Dim llArrivalLine As Long
    Dim strUsedUrnos  As String
    Dim strFlnu As String
    Dim strFlnuList As String
    
    
'    If tabFlnuBcs.GetLineCount = 0 Then
'        Exit Sub
'    End If
'    For i = 0 To tabFlnuBcs.GetLineCount - 1
'        strFlnu = tabFlnuBcs.GetFieldValue(i, "FLNU")
'        llCurrLine = -1
'        cnt = frmData.tabData(3).GetLineCount - 1
'        While cnt > -1
'            If frmData.tabData(3).GetFieldValue(cnt, "FLNU") = strFlnu Then
'                frmData.tabData(3).DeleteLine (cnt)
'            End If
'            cnt = cnt - 1
'        Wend
'    Next i
    'KKH cater for Kriscom update
        cnt = frmData.tabData(3).GetLineCount - 1
        While cnt > -1
        If frmData.tabData(3).GetFieldValue(cnt, "FLNU") = omCurrentSelectedUrno Then
                frmData.tabData(3).DeleteLine (cnt)
            End If
            cnt = cnt - 1
        Wend
    'kkh on 28/11/2008 Display KRI as higher priority then PTM message for all flights
    'frmData.tabData(3).Sort "1,2,3", True, True
    frmData.tabData(3).Sort "3,2", True, True
    strFlnuList = omCurrentSelectedUrno
'   strFlnuList = tabFlnuBcs.GetBuffer(0, tabFlnuBcs.GetLineCount - 1, ",")
'    While Val(strRet) > -1 And strRet <> "" And strRet <> "0"
'        llCurrLine = frmData.tabData(3).GetNextResultLine
'        If llCurrLine > -1 Then
'            frmData.tabData(3).DeleteLine llCurrLine
'        End If
'        strRet = frmData.tabData(3).GetLinesByIndexValue("FLNU", strFlnu, 0)
'    Wend
    
    frmData.tabReread.ResetContent
    frmData.tabReread.HeaderString = frmData.tabData(3).HeaderString
    frmData.tabReread.LogicalFieldList = frmData.tabData(3).LogicalFieldList
    frmData.tabReread.HeaderLengthString = frmData.tabData(3).HeaderLengthString
    frmData.tabReread.EnableHeaderSizing True
    
    'strWhere = "WHERE FLNU IN (" + strFlnuList + ")" + _
               " AND DSSN IN ('PTM','KRI') AND TYPE IN ('PAX','PXB','BAG','BWT') AND STYP IN ('E','B','F') " + _
               " AND SSST=' ' AND (SSTP='TD' OR SSTP=' ' OR SSTP='T')"
    'strWhere = "WHERE FLNU IN (" + strFlnuList + ")" + _
               " AND DSSN IN ('PTM','KRI') AND TYPE IN ('PAX','PXB','BAG','BWT') AND STYP IN ('E','B','F','U') " + _
               " AND SSST=' ' AND (SSTP='TD' OR SSTP=' ' OR SSTP='T') AND FLNO <> ' ' order by APC3, ADDI" 'igu on 23/08/2011: ALTEA
    strWhere = "WHERE FLNU IN (" + strFlnuList + ")" + _
               " AND DSSN IN ('PTM','KRI','ALT') AND TYPE IN ('PAX','PXB','BAG','BWT') AND STYP IN ('E','B','F','U') " + _
               " AND SSST=' ' AND (SSTP='TD' OR SSTP=' ' OR SSTP='T') AND FLNO <> ' ' order by APC3, ADDI" 'igu on 23/08/2011: ALTEA
               
    frmData.tabReread.CedaAction "RT", "LOATAB", frmData.tabReread.HeaderString, "", strWhere
    'kkh on 28/11/2008 Display KRI as higher priority then PTM message for all flights
    'frmData.tabData(3).Sort "1,2,3", True, True  ' This reorgs the indexes as well!
    'frmData.tabData(3).Sort "3,2", True, True
    frmData.tabReread.Sort "3,2", True, True
    cnt = frmData.tabReread.GetLineCount - 1
    For i = 0 To cnt
        strFlightDay = ""
        strRawFlno = frmData.tabReread.GetFieldValue(i, "FLNO")
        arr = Split(strRawFlno, "/")
        If UBound(arr) = 1 Then
            strFlno = arr(0)
            strFlightDay = arr(1)
        Else
            strFlno = strRawFlno
        End If
        strFlno = StripAftFlno(strFlno, dummy1, dummy2, dummy3)
        strUrnoA = frmData.tabReread.GetFieldValue(i, "FLNU")
        strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", strUrnoA, 0)
        llLine = frmData.tabData(0).GetNextResultLine
        If llLine > 0 Then
            strStoa = frmData.tabData(0).GetFieldValue(llLine, "STOA")
            If Trim(strStoa) <> "" Then
                datStoa = CedaFullDateToVb(strStoa)
                llDepLine = GetCorrectDepartureByFlightDay(datStoa, strFlightDay, strFlno)
                If llDepLine > -1 Then
                    frmData.tabReread.SetFieldValues i, "RTAB", frmData.tabData(0).GetFieldValue(llDepLine, "URNO")
                End If
            End If
        End If
        strSearchValue = strUrnoA + ";"
        llArrivalLine = frmData.tabData(0).GetLinesByIndexValue("URNO", strUrnoA, 0)
        If llArrivalLine > -1 Then
            If InStr(strUsedUrnos, strSearchValue) = 0 Then
                UpdateBar llArrivalLine
            End If
            strUsedUrnos = strUsedUrnos + strUrnoA + ";"
        End If
        
        ReDim arr(0)
    Next i
    
    If cnt > 0 Then
        frmData.tabData(3).InsertBuffer frmData.tabReread.GetBuffer(0, frmData.tabReread.GetLineCount - 1, vbLf), vbLf
    End If
    'kkh on 28/11/2008 Display KRI as higher priority then PTM message for all flights
    'frmData.tabData(3).Sort "1,2,3", True, True  ' This reorgs the indexes as well!
    frmData.tabData(3).Sort "3,2", True, True
    cnt = frmData.tabData(3).GetLineCount
    frmData.lblRecCount(3) = "Table: " + "LOATAB" + "  Records: " + CStr(cnt)

    frmData.tabReread.ResetContent
    
End Sub
'-----------------------------------------------------------------------
' For flights updates by broadcasts Commands "UFR", "UPS", "UPJ"
'-----------------------------------------------------------------------
Public Sub HandleUFR(strUrno As String, strFields As String, strData As String)
    Dim strRet As String
    Dim llTabLine As Long
    Dim strTifa As String
    Dim strTIFD As String
    Dim strItem As String
    Dim llItemNo As Long
    Dim strOldTIFA As String
    Dim strOldTIFD As String
    Dim strLineValues As String
    Dim strSkey As String
    Dim strAdid As String
    Dim llLineNo As Long
    Dim strUsedUrnos As String
    Dim strFlnu As String
    Dim llArrivalLine As Long
    Dim strSearchValue As String
    Dim strStoa As String
    Dim datArrSTOA As Date
    Dim strArrivalUrno As String
    Dim strFtyp As String
    
    llItemNo = GetRealItemNo(strFields, "ADID")
    If llItemNo > -1 Then
        strAdid = GetRealItem(strData, llItemNo, ",")
    End If
    strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", strUrno, 0)
    llTabLine = frmData.tabData(0).GetNextResultLine
    If llTabLine <> -1 Then
        strOldTIFA = frmData.tabData(0).GetFieldValue(llTabLine, "TIFA")
        strOldTIFD = frmData.tabData(0).GetFieldValue(llTabLine, "TIFD")
        frmData.tabData(0).SetFieldValues llTabLine, strFields, strData
        frmData.tabData(0).Refresh
        strFtyp = frmData.tabData(0).GetFieldValue(llTabLine, "FTYP")
        strFtyp = Trim(strFtyp)
        If strFtyp = "D" Or strFtyp = "" Or strFtyp = "N" Then
            gantt(0).DeleteBarByKey strUrno
            gantt(0).Refresh
            frmCompressedFligths.gantt.DeleteBarByKey strUrno
            frmCompressedFligths.gantt.Refresh
            frmData.tabData(0).DeleteLine llTabLine
            frmData.tabData(0).Sort "1,3", True, True 'This reorgs all indexes as well
            If omCurrentSelectedUrno = strUrno Then
                tabTab(0).ResetContent
                tabTab(1).ResetContent
                gantt(1).ResetContent
                omCurrentSelectedUrno = ""
            End If
        Else
            strTifa = frmData.tabData(0).GetFieldValue(llTabLine, "TIFA")
            strTIFD = frmData.tabData(0).GetFieldValue(llTabLine, "TIFD")
            If strTifa <> strOldTIFA Or strTIFD <> strOldTIFD Then
                strTifa = strTifa
            End If
            If strAdid = "D" Then
                strRet = frmData.tabData(3).GetLinesByIndexValue("RTAB", strUrno, 0)
                llLineNo = frmData.tabData(3).GetNextResultLine
                While llLineNo > -1
                    strFlnu = frmData.tabData(3).GetFieldValue(llLineNo, "FLNU")
                    strSearchValue = strFlnu + ";"
                    llArrivalLine = frmData.tabData(0).GetLinesByIndexValue("URNO", strFlnu, 0)
                    If llArrivalLine > -1 Then
                        If InStr(strUsedUrnos, strSearchValue) = 0 Then
                            UpdateBar llArrivalLine
                        End If
                        strUsedUrnos = strUsedUrnos + strFlnu + ";"
                    End If
                    llLineNo = frmData.tabData(3).GetNextResultLine
                Wend
                strSearchValue = omCurrentSelectedUrno + ";"
                If InStr(strUsedUrnos, strSearchValue) > 0 Then
                    gantt_OnLButtonDownBar 0, omCurrentSelectedUrno, 0
                End If
            End If
            UpdateBar llTabLine
            If omCurrentSelectedUrno = strUrno Then
                If strAdid = "A" Then
                    gantt_OnLButtonDownBar 0, strUrno, 0
                End If
            End If
        End If
    End If
End Sub
'-----------------------------------------------------
' 1. Reloads the AFTTAB records for this SKEY, use the
'    frmData.tabReread for tmepory storage and
'    reorgs the AFTdata in frmData.tabData(0)
' 2. checks the flight with FLNU for connection conflicts
'    and redraws the bar in the charts
'-----------------------------------------------------
Public Sub HandleISF(strSkey As String)
    Dim strWhere As String
    Dim strAdid As String
    Dim llLineNo As Long
    Dim llCurrLine As Long
    Dim strRet As String
    Dim cnt As Long
    Dim strUrno As String
    Dim i As Long
    Dim strFieldList As String
    Dim strLineValues As String
    
    frmData.tabReread.ResetContent
    frmData.tabReread.HeaderString = frmData.tabData(0).HeaderString
    frmData.tabReread.HeaderLengthString = frmData.tabData(0).HeaderLengthString
    frmData.tabReread.LogicalFieldList = frmData.tabData(0).LogicalFieldList
    strFieldList = frmData.tabReread.LogicalFieldList
    frmData.tabReread.EnableHeaderSizing True
    
    frmData.tabReread.CedaServerName = strServer
    frmData.tabReread.CedaPort = "3357"
    frmData.tabReread.CedaHopo = strHopo
    frmData.tabReread.CedaCurrentApplication = "HUB-Manager" & "," & frmData.GetApplVersion(True)
    frmData.tabReread.CedaTabext = "TAB"
    frmData.tabReread.CedaUser = strCurrentUser
    frmData.tabReread.CedaWorkstation = frmData.aUfis.WorkStation
    frmData.tabReread.CedaSendTimeout = "3"
    frmData.tabReread.CedaReceiveTimeout = "240"
    frmData.tabReread.CedaRecordSeparator = Chr(10)
    frmData.tabReread.CedaIdentifier = "HUB-Manager"
    '" (TIFA BETWEEN '" + strTimeFrameFrom + "' AND '" + strTimeFrameTo + "') AND (TIFD BETWEEN '" + strTimeFrameFrom + "' and '" + strTimeFrameTo + "') AND FTYP IN ('S', 'O', 'X', 'N', 'D', 'R')"
    'strWhere = "WHERE SKEY=" + strSkey + " AND (TIFA BETWEEN '" + strTimeFrameFrom + "' AND '" + strTimeFrameTo + "') AND (TIFD BETWEEN '" + strTimeFrameFrom + "' and '" + strTimeFrameTo + "') AND FTYP IN ('S', 'O', 'X', 'N', 'D', 'R')"
    'strWhere = "WHERE SKEY=" + strSkey + " AND (TIFA BETWEEN '" + strTimeFrameFrom + "' AND '" + strTimeFrameTo + "') AND FTYP IN ('S', 'O', 'X', 'N', 'D', 'R') AND DES3 = 'SIN' OR (TIFD BETWEEN '" + strTimeFrameFrom + "' and '" + strTimeFrameTo + "') AND FTYP IN ('S', 'O', 'X', 'N', 'D', 'R') AND ORG3='SIN'"
    strWhere = "WHERE SKEY=" + strSkey + " AND (TIFA BETWEEN '" + strTimeFrameFrom + "' AND '" + strTimeFrameTo + "') AND FTYP IN ('S', 'O', 'X', 'N', 'D', 'R') AND ADID IN ('A','B') OR (TIFD BETWEEN '" + strTimeFrameFrom + "' and '" + strTimeFrameTo + "') AND FTYP IN ('S', 'O', 'X', 'N', 'D', 'R') AND ADID IN ('D','B')"
    frmData.tabReread.CedaAction "RT", "AFTTAB", frmData.tabReread.HeaderString, "", strWhere
    
    'kkh UTC / local solve!!
    If TimesInUTC = False Then
        ChangetabRereadToLocal
    End If
    
    cnt = frmData.tabReread.GetLineCount - 1
    For i = 0 To cnt
        strUrno = frmData.tabReread.GetFieldValue(i, "URNO")
        strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", strUrno, 0)
        llLineNo = frmData.tabData(0).GetNextResultLine
        strLineValues = frmData.tabReread.GetLineValues(i)
        If llLineNo > -1 Then
            frmData.tabData(0).SetFieldValues llLineNo, strFieldList, strLineValues
            UpdateBar llLineNo
        Else
            frmData.tabData(0).InsertTextLine strLineValues, False
            UpdateBar frmData.tabData(0).GetLineCount - 1
        End If
    Next i
    frmData.tabData(0).Sort "1,3", True, True 'This reorgs all indexes as well
    
End Sub
'kkh UTC / local solve!!
Public Sub ChangetabRereadToLocal()
    Dim cnt As Long
    Dim fldCnt As Integer
    Dim i As Long
    Dim j As Long
    Dim strField As String
    Dim strVal As String
    Dim myDat As Date
    
    cnt = frmData.tabReread.GetLineCount - 1
    fldCnt = ItemCount(strAftTimeFields, ",")
    For i = 0 To cnt
        For j = 0 To fldCnt
            strField = GetRealItem(strAftTimeFields, CInt(j), ",")
            strVal = frmData.tabReread.GetFieldValue(i, strField)
            If Trim(strVal) <> "" Then
                myDat = CedaFullDateToVb(strVal)
                If TimesInUTC = False Then
                    myDat = DateAdd("h", UTCOffset, myDat)
                End If
                strVal = Format(myDat, "YYYYMMDDhhmmss")
                frmData.tabReread.SetFieldValues i, strField, strVal
            End If
        Next j
    Next i
    frmData.tabReread.Refresh
End Sub

Private Sub rbArea_Click(Index As Integer)
    Dim i As Integer
    For i = 0 To rbArea.count - 1
        If i <> Index Then
            rbArea(i).backColor = vbButtonFace
        End If
    Next i
    If rbArea(Index).Value = True Then
        rbArea(Index).backColor = vbGreen
        gantt(1).Visible = True
        lblDetailContainer.Visible = True
        Select Case (Index)
            Case 0
                igCurrentPercentX = 100
                igCurrentPercentY = 100
                lblSize(0).Top = slFontSize.Top - (lblSize(0).Height) 'lblAboveText(0).top + lblAboveText(0).Width
                lblSize(1).Left = Me.ScaleWidth - 100
                Form_Resize
                gantt(1).Visible = False
                lblDetailContainer.Visible = False
            Case 1
                igCurrentPercentX = 100
                igCurrentPercentY = -10
                lblSize(0).Top = lblAboveText(0).Top + lblAboveText(0).Width 'slFontSize.top - (lblSize(0).Height)
                lblSize(1).Left = 0
                Form_Resize
                gantt(0).Width = 0
            Case 2
                igCurrentPercentX = 0
                igCurrentPercentY = 0
                lblSize(0).Top = lblAboveText(0).Top + lblAboveText(0).Width 'slFontSize.top - (lblSize(0).Height)
                Form_Resize
                lblSize(1).Left = 0
                gantt(1).Width = 0
                gantt(0).Width = 0
            Case 3
                'Resize / adjust the width
                igCurrentPercentX = 4
                igCurrentPercentY = 50
                Form_Resize
        End Select
        'NewSize
    Else
        rbArea(Index).backColor = vbButtonFace
    End If
End Sub

Private Sub slDuration_Scroll()
    Dim i As Integer
    For i = 0 To 1
        gantt(i).TimeScaleDuration = slDuration.Value
        gantt(i).RefreshArea "ALL"
    Next i
End Sub
Private Sub slFontSize_Scroll()
    Dim i As Integer
    For i = 0 To 1
        gantt(i).TabFontSize = slFontSize.Value
        gantt(i).TabFontName = gantt(i).TabFontName
        gantt(i).TabBodyFontBold = True
        If gantt(i).SubbarStackCount = 1 Then
            gantt(i).TabLineHeight = gantt(i).TabLineHeight * 2
        End If
        If gantt(i).SubbarStackCount = 0 Then
            gantt(i).TabLineHeight = slFontSize.Value + 5
        End If
        gantt(i).Refresh
        cbToolButton(0).SetFocus
        'gantt(I).RefreshArea "ALL"
    Next i
End Sub
Public Sub MakeMainGanttLines()
    Dim cnt As Long
    Dim myG As UGantt
    Dim i As Long
    Dim strVas As String
    Dim strPnam As String
    Dim strGnam As String
    
    If bmPositionActive = True Then
        cnt = frmData.tabData(1).GetLineCount
        For Each myG In gantt
            myG.TabHeaderLengthString = "100"                   'init the width of the tab-columns
            myG.TabSetHeaderText ("Position")                   'set the tab-headlines
            myG.SplitterPosition = 100                          'the splitterposition depends on the
            myG.TabBodyFontName = "Arial"
            myG.BarNumberExpandLine = 0
            myG.TabFontSize = slFontSize.Value
            myG.TabHeaderFontName = "Arial"
            myG.TabHeaderFontSize = 14
            myG.BarOverlapOffset = 10
            myG.TabBodyFontBold = True
            myG.TabHeaderFontBold = True
            If TimesInUTC = True Then
                gantt(0).UTCOffsetInHours = -UTCOffset
                gantt(1).UTCOffsetInHours = -UTCOffset
            Else
                gantt(0).UTCOffsetInHours = 0
                gantt(1).UTCOffsetInHours = 0
            End If
        Next
        gantt(1).TabSetHeaderText ("Pos-Connection")            'set the tab-headlines
        
        For i = 0 To cnt - 1
            strPnam = frmData.tabData(1).GetFieldValue(i, "PNAM")
            gantt(0).AddLineAt i, strPnam, strPnam
            gantt(0).SetLineSeparator i, 1, vbBlack, 2, 1, 0
        Next i
        For i = 0 To 100
            gantt(1).AddLineAt i, CStr(i), CStr(i)
            gantt(1).SetLineSeparator i, 1, vbBlack, 2, 1, 0
        Next i
    Else
        cnt = frmData.tabData(11).GetLineCount
        For Each myG In gantt
            myG.TabHeaderLengthString = "100"                   'init the width of the tab-columns
            myG.TabSetHeaderText ("Gates")                   'set the tab-headlines
            myG.SplitterPosition = 100                          'the splitterposition depends on the
            myG.TabBodyFontName = "Arial"
            myG.BarNumberExpandLine = 0
            myG.TabFontSize = slFontSize.Value
            myG.TabHeaderFontName = "Arial"
            myG.TabHeaderFontSize = 14
            myG.BarOverlapOffset = 10
            myG.TabBodyFontBold = True
            myG.TabHeaderFontBold = True
            If TimesInUTC = True Then
                gantt(0).UTCOffsetInHours = -UTCOffset
                gantt(1).UTCOffsetInHours = -UTCOffset
            Else
                gantt(0).UTCOffsetInHours = 0
                gantt(1).UTCOffsetInHours = 0
            End If
        Next
        gantt(1).TabSetHeaderText ("Gate-Connection")            'set the tab-headlines
        
        For i = 0 To cnt - 1
            strGnam = frmData.tabData(11).GetFieldValue(i, "GNAM")
            gantt(0).AddLineAt i, strGnam, strGnam
            gantt(0).SetLineSeparator i, 1, vbBlack, 2, 1, 0
        Next i
        For i = 0 To 100
            gantt(1).AddLineAt i, CStr(i), CStr(i)
            gantt(1).SetLineSeparator i, 1, vbBlack, 2, 1, 0
        Next i
    End If
End Sub
'----------------------------------------------------------------
' Calculates fills the entire gantt with all loaded flights
'----------------------------------------------------------------
Public Sub MakeMainGanttBars(Optional bpRefreshGantts As Boolean = True, Optional bpUpdateCompressedGantt As Boolean = True)
    Dim i As Long
    Dim cnt As Long
    cnt = frmData.tabData(0).GetLineCount
    For i = 0 To cnt - 1
        UpdateBar i, bpRefreshGantts, bpUpdateCompressedGantt
    Next i
End Sub
'------------------------------------------
' Updates a bar in the gantts
'------------------------------------------
Public Sub UpdateBar(i As Long, Optional bpRefreshGantts As Boolean = True, Optional bpUpdateCompressedGantt As Boolean = True)
    Dim cnt As Long
    Dim strValue As String
    Dim myFrom As Date
    Dim myTo As Date
    Dim strLineNo As String
    Dim strBarKey As String
    Dim strAdid As String
    Dim strTisa As String
    Dim strTisd As String
    Dim strLineKey As String
    Dim strCompLineKey As String
    Dim interval As Integer
    Dim llGanttLine As Long
    Dim strFlno As String
    Dim colBody As Long
    Dim leftTriaColor As Long
    Dim strStoa As String
    Dim datStoa As Date
    Dim strStod As String
    Dim datStod As Date
    Dim rightTriaColor As Long
    Dim strTriaID As String
    Dim datTimeFrameFrom As Date
    Dim datTimeFrameTo As Date
    Dim strLineValues As String
    Dim blHasConflict As Boolean
    
    
    datTimeFrameFrom = CedaFullDateToVb(strTimeFrameFrom)
    datTimeFrameTo = CedaFullDateToVb(strTimeFrameTo)
    cnt = frmData.tabData(0).GetLineCount
    strValue = frmData.tabData(0).LogicalFieldList
    
    leftTriaColor = -1
    rightTriaColor = -1
    strBarKey = frmData.tabData(0).GetFieldValue(i, "URNO")
    strAdid = frmData.tabData(0).GetFieldValue(i, "ADID")
    strFlno = frmData.tabData(0).GetFieldValue(i, "FLNO")
    
    gantt(0).DeleteBarByKey strBarKey
    strLineValues = frmData.tabData(0).GetLineValues(i)
    blHasConflict = False
    If strAdid = "A" Then
        strValue = frmData.tabData(0).GetFieldValue(i, "TIFA")
        If Trim(strValue) <> "" Then
            interval = GetArrivalBarLenInMinutes(strBarKey, strValue)
            myFrom = CedaFullDateToVb(strValue)
            myTo = DateAdd("n", interval, myFrom)
            If bmPositionActive = True Then
                strLineKey = frmData.tabData(0).GetFieldValue(i, "PSTA")
            Else
                strLineKey = frmData.tabData(0).GetFieldValue(i, "GTA1")
                If Trim(strLineKey) = "" Then
                    strLineKey = frmData.tabData(0).GetFieldValue(i, "GTA2")
                End If
            End If
            strStoa = frmData.tabData(0).GetFieldValue(i, "STOA")
            colBody = colorArrivalBar 'vbWhite
            If strBarKey = omCurrentSelectedUrno Then
                AdjustCurrentBarColor strBarKey, colBody
            End If
            If Trim(strStoa) <> "" Then
                datStoa = CedaFullDateToVb(strStoa)
                If HasArrivalConflict(strBarKey, datStoa, False) = True Then
                    'warning colour orange
'                    If compressedFlightBarCritColour = True Then
                        colBody = vbRed
'                    Else
'                        If compressedFlightBarWarnColour = True Then
'                            colBody = colOrange
'                        End If
'                    End If
                    blHasConflict = True
                    If strBarKey = omCurrentSelectedUrno Then
                        'warning colour orange
'                        If compressedFlightBarCritColour = True Then
                            AdjustCurrentBarColor strBarKey, vbRed
'                        Else
'                            If compressedFlightBarWarnColour = True Then
'                                AdjustCurrentBarColor strBarKey, colOrange
'                            End If
'                        End If
                    End If
                Else
                    If strBarKey = omCurrentSelectedUrno Then
                        AdjustCurrentBarColor strBarKey, colorArrivalBar
                    End If
                End If
            End If
            strTisa = frmData.tabData(0).GetFieldValue(i, "TISA")
            leftTriaColor = GetTisaColor(strTisa)
        End If
    End If
    If strAdid = "D" Then
        strValue = frmData.tabData(0).GetFieldValue(i, "TIFD")
        If Trim(strValue) <> "" Then
            interval = GetDepartureBarLenInMinutes(strBarKey, strValue)
            myTo = CedaFullDateToVb(strValue)
            myFrom = DateAdd("n", -interval, myTo)
            If bmPositionActive = True Then
                strLineKey = frmData.tabData(0).GetFieldValue(i, "PSTD")
            Else
                strLineKey = frmData.tabData(0).GetFieldValue(i, "GTD1")
                If Trim(strLineKey) = "" Then
                    strLineKey = frmData.tabData(0).GetFieldValue(i, "GTD2")
                End If
            End If
            If HasConnectionConflicts(strBarKey) = False Then
                colBody = colorDepartureBar 'colLightBlue
            Else
                'warning colour orange
'                If compressedFlightBarCritColour = True Then
                    colBody = vbRed
'                Else
'                    If compressedFlightBarWarnColour = True Then
'                        colBody = colOrange
'                    End If
'                End If
            End If
            If strBarKey = omCurrentSelectedUrno Then
                AdjustCurrentBarColor strBarKey, colBody
            End If
            strTisd = frmData.tabData(0).GetFieldValue(i, "TISD")
            rightTriaColor = GetTisdColor(strTisd)
        End If
    End If
    If myTo >= datTimeFrameFrom And myFrom <= datTimeFrameTo Then
        llGanttLine = gantt(0).GetLineNoByKey(strLineKey)
        gantt(0).AddBarToLine llGanttLine, strBarKey, myFrom, myTo, strFlno, 1, _
                           vbCyan, vbBlack, vbRed, vbBlack, vbBlack, "", "", vbBlack, vbBlack
        gantt(0).SetBarColor strBarKey, colBody
        strTriaID = CStr(leftTriaColor) + CStr(rightTriaColor)
        gantt(0).SetBarTriangles strBarKey, strTriaID, leftTriaColor, rightTriaColor, True
        If bgfrmCompressedFlightsOpen = True And bpUpdateCompressedGantt = True Then
            llGanttLine = frmCompressedFligths.gantt.GetLineNoByBarKey(strBarKey)
            frmCompressedFligths.gantt.DeleteBarByKey strBarKey
            If llGanttLine = -1 Then 'could not be found => add it to line "0" to display it
                llGanttLine = 0
            End If
            If blHasConflict = False And frmCompressedFligths.cbToolButton(2).Value = 1 Then
            Else
                'Compressed flight is set to show conflict data only
                frmCompressedFligths.gantt.AddBarToLine llGanttLine, strBarKey, myFrom, myTo, strFlno, 1, _
                                   vbCyan, vbBlack, vbRed, vbBlack, vbBlack, "", "", vbBlack, vbBlack
                frmCompressedFligths.gantt.SetBarColor strBarKey, colBody
                strTriaID = CStr(leftTriaColor) + CStr(rightTriaColor)
                frmCompressedFligths.gantt.SetBarTriangles strBarKey, strTriaID, leftTriaColor, rightTriaColor, True
                If bpRefreshGantts = True Then frmCompressedFligths.gantt.Refresh
            End If
            If omCurrentSelectedUrno = strBarKey Then
                SelectMainGanttBar strBarKey, 0
            End If
        End If
    End If
    If bpRefreshGantts = True Then gantt(0).Refresh
End Sub

'----------------------------------------------------------------
' Removes the bar from the gantt(IDX) and inserts it to the
' corresponding line
'----------------------------------------------------------------
Public Sub SetBarToGantt(idx As Integer, BarKey As String)
    gantt(idx).DeleteBarByKey BarKey
    
End Sub
'---------------------------------------------------------------
' identifies the color as long for tisa
'---------------------------------------------------------------
Public Function GetTisaColor(strTisa As String) As Long
    Dim ret As Long
    
    ret = 0
    Select Case (strTisa)
        Case "S"
            ret = colGray
        Case "E"
            ret = colWhite
        Case "T"
            ret = colYellow
        Case "L"
            ret = colGreen
        Case "O"
            ret = colLime
    End Select
    If ret = 0 Then
        ret = 0
    End If
    GetTisaColor = ret
End Function
'---------------------------------------------------------------
' identifies the color as long for tisa
'---------------------------------------------------------------
Public Function GetTisdColor(strTisd As String) As Long
    Dim ret As Long
    
    ret = 0
    Select Case (strTisd)
        Case "O"
            ret = colLime
        Case "A"
            ret = colGreen
        Case "S"
            ret = colGray
        Case "E"
            ret = colWhite
    End Select
    If ret = 0 Then
        ret = 0
    End If
    GetTisdColor = ret
End Function
Private Sub tabtab_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim SortCol As Integer
    Dim LastColno As Integer
    
    If Index = 1 Then
        SortCol = ColNo
        LastColno = tabTab(Index).CurrentSortColumn
        tabTab(Index).ColSelectionRemoveAll
        If SortCol >= 0 Then
            If ColNo <> LastColno Then
                tabTab(Index).Sort CStr(SortCol), True, True
            Else
                If tabTab(Index).SortOrderASC Then
                    tabTab(Index).Sort CStr(SortCol), False, True
                Else
                    tabTab(Index).Sort CStr(SortCol), True, True
                End If
            End If
            tabTab(Index).ColSelectionAdd ColNo
        End If
        'tabTab(Index).AutoSizeColumns
        tabTab(Index).Refresh
    End If
End Sub

Private Sub tabTab_ComboSelChanged(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal OldValue As String, ByVal NewValues As String)
    Dim strTimeValue As String
    Dim strDecisionValue As String
    
    If NewValues = " " Then
        'this is for user to key in free text as decision
        'tabTab(1).SetInplaceEdit LineNo, 19, True
        'tabTab(1).SetInplaceEdit LineNo, 21, True
        
        'kkh on 16/07/2008 P2 Store Decision into CFITAB
        tabTab(1).SetFieldValues LineNo, "Hold", NewValues
        tabTab(1).SetFieldValues LineNo, "COMBO", ""
        tabTab(1).Refresh
        StoreCFITABNew LineNo, Index

    Else
        tabTab(1).SetFieldValues LineNo, "Hold", NewValues
        tabTab(1).SetFieldValues LineNo, "COMBO", ""
        tabTab(1).Refresh
'        strDecisionValue = NewValues
'        strTimeValue = tabTab(1).GetFieldValue(LineNo, "TRANSFER")
'        'StoreSRLTAB LineNo, strDecisionValue, strTimeValue
'
'        'kkh on 16/07/2008 P2 Store Decision into CFITAB
        StoreCFITABNew LineNo, Index
    End If
End Sub
Public Sub StoreCFITABNew(LineNo As Long, Index As Integer)
    Dim strRet As String
    Dim llLineNo As Long
    Dim lineIDX As Long
    Dim strCdat As String
    Dim datCdat As Date
    'Arr flight info
    Dim strArrUrno As String
    Dim strArrFlno As String
    Dim strDate As String
    Dim strStoa As String
    Dim strRGNA As String
    Dim strORG3 As String
    Dim strVSA3 As String
    Dim strPSTA As String
    Dim strGATA As String
    Dim strTTPA As String
    Dim strTYPE As String
    Dim strTALL As String
    Dim strTCLF As String
    Dim strTCLC As String
    Dim strTCLY As String
    Dim strTOTH As String
    Dim strTimeValue As String
    Dim strTransfertime As String
    Dim strDecisionValue As String
    'Dep flight info
    Dim strDepUrno As String
    Dim strDepFlno As String
    Dim strStod As String
    Dim strDES3 As String
    Dim strGATD As String
    Dim strTTPD As String
    
    Dim strFields As String
    Dim strData As String
    Dim strCfiUrno As String
    Dim strRet2 As String
    Dim strValues As String
    
    Dim aLineNo As Variant 'igu on 27 Jul 2010
    Dim intLineCount As Integer 'igu on 27 Jul 2010
    Dim strWhere As String 'igu on 27 Jul 2010
    Dim i As Integer 'igu on 27 Jul 2010
    
    'kkh on 20/01/2009 check UTC or local before insert into CFITAB and L04TAB
    datCdat = Now
    'If TimesInUTC = False Then
        'UTCOffset = frmData.GetUTCOffset
        datCdat = DateAdd("h", -UTCOffset, datCdat)
    'End If
    strCdat = Format(datCdat, "YYYYMMDDhhmmss")
    
    strArrUrno = tabTab(0).GetFieldValue(0, "URNO_A")
    strArrFlno = tabTab(0).GetFieldValue(0, "FLNO_A")
    'FLNO_A,DATE_A,STA,ETA,ONBL,NA_A,ORG,VIA_A,POS_A,TGA_A,GAT_A,,NA_A,A/C_A,REGN_A
    strStoa = tabTab(0).GetFieldValue(0, "STOA_A")
    strRGNA = tabTab(0).GetFieldValue(0, "REGN_A")
    strORG3 = tabTab(0).GetFieldValue(0, "ORG")
    strVSA3 = tabTab(0).GetFieldValue(0, "VIA_A")
    strPSTA = tabTab(0).GetFieldValue(0, "POS_A")
    strGATA = tabTab(0).GetFieldValue(0, "GAT_A")
    strTTPA = tabTab(0).GetFieldValue(0, "NA_A")
       
    strTALL = tabTab(Index).GetFieldValue(LineNo, "T-PAX")
    strTCLF = tabTab(Index).GetFieldValue(LineNo, "F-PAX")
    strTCLC = tabTab(Index).GetFieldValue(LineNo, "C-PAX")
    strTCLY = tabTab(Index).GetFieldValue(LineNo, "Y-PAX")
    strTOTH = tabTab(Index).GetFieldValue(LineNo, "U-PAX")
    strTimeValue = Replace(tabTab(Index).GetFieldValue(LineNo, "TRANSFER"), ":", "")
    strDecisionValue = tabTab(Index).GetFieldValue(LineNo, "Hold")
    
    strDepUrno = tabTab(1).GetFieldValue(LineNo, "AFTURNO")
    strDepFlno = tabTab(1).GetFieldValue(LineNo, "FLNO")
    'ACT,FLNO,TGD_D,GATE,STD,EST,ATD,DES,NA
    strStod = tabTab(Index).GetFieldValue(LineNo, "STD")
    strDES3 = tabTab(Index).GetFieldValue(LineNo, "DES")
    strGATD = tabTab(Index).GetFieldValue(LineNo, "GATE")
    strTTPD = tabTab(Index).GetFieldValue(LineNo, "NA")
    
    If strDepUrno <> "" And strArrUrno <> "" Then
        strRet2 = ""
        'strRet2 = frmData.tabData(13).GetLinesByMultipleColumnValue("1,2", strArrUrno + "," + strDepUrno, 0)
        strRet2 = frmData.tabData(13).GetLinesByMultipleColumnValue("5,22", strArrUrno + "," + strDepUrno, 0)
        
        'strRet2 = frmData.tabData(13).GetLinesByColumnValue("22", strDepUrno, 0)
        If strRet2 <> "" Then
            'update decision
            strFields = "USEC,CDAT,USEU,LSTU,AURN,FLNA,STOA,RGNA,ORG3,VSA3,PSTA,GATA,TTPA,TYPE,TALL,TCLF,TCLC,TCLY,TOTH,CTMS,ADDI,DURN,FLND,STOD,DES3,GATD,TTPD"
                        
            'strCfiUrno = frmData.tabData(13).GetFieldValue(strRet2, "URNO") 'igu on 27 Jul 2010
            
            'igu on 27 Jul 2010
            'check for duplicate entries
            '---------------------------
            aLineNo = Split(strRet2, ",")
            intLineCount = UBound(aLineNo) + 1
            
            strCfiUrno = ""
            For i = 0 To intLineCount - 1
                strRet2 = aLineNo(i)
                strCfiUrno = strCfiUrno & "," & frmData.tabData(13).GetFieldValue(strRet2, "URNO")
            Next i
            If strCfiUrno <> "" Then strCfiUrno = Mid(strCfiUrno, 2)
                
            If intLineCount = 1 Then
                strWhere = "WHERE URNO = " & strCfiUrno
            Else
                strWhere = "WHERE URNO IN (" & strCfiUrno & ")"
            End If
            
            strRet2 = aLineNo(0)
            'end check for duplicate entries
            '------------------------------
            
            'strData = strTimeValue + "," + strDecisionValue + "," + strCurrentUser + "," + strCdat
            strData = ""
            strData = frmData.tabData(13).GetFieldValue(strRet2, "USEC") & "," & frmData.tabData(13).GetFieldValue(strRet2, "CDAT") & ","
            strData = strData & strCurrentUser & "," & strCdat & ","
            strData = strData & strArrUrno & "," & strArrFlno & "," & strStoa & "," & strRGNA & "," & strORG3 & "," & strVSA3 & "," & strPSTA & "," & strGATA & "," & strTTPA & "," & "HUB" & ","
            strData = strData & strTALL & "," & strTCLF & "," & strTCLC & "," & strTCLY & "," & strTOTH & "," & strTimeValue & "," & strDecisionValue & ","
            strData = strData & strDepUrno & "," & strDepFlno & "," & strStod & "," & strDES3 & "," & strGATD & "," & strTTPD
            If frmData.aUfis.CallServer("URT", "CFITAB", strFields, strData, strWhere, 230) <> 0 Then
                MsgBox "Update Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
            Else
                'frmData.tabData(13).SetFieldValues strRet2, "ADDI,USEU,LSTU", strData
                'CFITAB;URNO,AURN,DURN,ADDI,TYPE,FLNA,FLND,USEU,USEC,CDAT,LSTU;
'                strData = ""
'                strFields = "ADDI,USEU,LSTU"
                'strData = strDecisionValue + "," + strCurrentUser + "," + strCdat
                'frmData.tabData(13).SetFieldValues strRet2, strFields, strData 'igu on 27 Jul 2010
                
                'igu on 27 Jul 2010
                'update all entries
                '------------------
                For i = 0 To intLineCount - 1
                    strRet2 = aLineNo(i)
                    frmData.tabData(13).SetFieldValues strRet2, strFields, strData
                Next i
                'end of update all entries
                '-------------------------
            End If
        Else
            'insert new decision
            'strFields = "URNO,AURN,DURN,ADDI,TYPE,FLNA,FLND,USEU,USEC,CDAT,LSTU"
            strFields = "URNO,USEC,CDAT,USEU,LSTU,AURN,FLNA,STOA,RGNA,ORG3,VSA3,PSTA,GATA,TTPA,TYPE,TALL,TCLF,TCLC,TCLY,TOTH,CTMS,ADDI,DURN,FLND,STOD,DES3,GATD,TTPD"
            strData = ""
            strData = frmData.GetNextUrno & ","
            'strData = strData & strArrUrno & "," & strDepUrno & "," & strDecisionValue & ",HUB," & strArrFlno & "," & strDepFlno & ","
            strData = strData & strCurrentUser & "," & strCdat & ",,,"
            strData = strData & strArrUrno & "," & strArrFlno & "," & strStoa & "," & strRGNA & "," & strORG3 & "," & strVSA3 & "," & strPSTA & "," & strGATA & "," & strTTPA & "," & "HUB" & ","
            strData = strData & strTALL & "," & strTCLF & "," & strTCLC & "," & strTCLY & "," & strTOTH & "," & strTimeValue & "," & strDecisionValue & ","
            strData = strData & strDepUrno & "," & strDepFlno & "," & strStod & "," & strDES3 & "," & strGATD & "," & strTTPD
            
            If frmData.aUfis.CallServer("IRT", "CFITAB", strFields, strData, "", 230) <> 0 Then
                MsgBox "Insert Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
            Else
                'igu on 27 Jul 2010
                'update TAB
                '----------
                frmData.tabData(13).InsertTextLine "", False
                frmData.tabData(13).SetFieldValues frmData.tabData(13).GetLineCount - 1, strFields, strData
                'end of update TAB
                '-----------------
            End If
        End If
    End If
End Sub
''----------------------------------------------------------------
'' Creates a new SRLTAB entry, replaces the SRLTAB (if exists) for the FLNU
'' Update => increment the AFT.BAA3 field and store it to inform FIPS and DACO3_Tool
'Public Sub StoreSRLTAB(LineNo As Long, strDecisionValue As String, strTimeValue As String)
'    Dim strRet As String
'    Dim llLineNo As Long
'    Dim llAFTLineNo As Long
'    Dim strFlnu As String
'    Dim lineIDX As Long
'    Dim strText As String
'    Dim strSRLUrno As String
'    Dim strFlno As String
'    Dim strStod As String
'    Dim strEtdi As String
'    Dim strFields As String
'    Dim strData As String
'    Dim strCdat As String
'    Dim datCdat As Date
'    Dim llBaa3 As Long
'    Dim strBAA3 As String
'    Dim blAFTFound As Boolean
'    Dim strTransfertime As String
'
'    datCdat = Now
'    blAFTFound = False
'    strCdat = Format(datCdat, "YYYYMMDDhhmmss")
'    strText = tabTab(1).GetFieldValue(LineNo, "Hold")
'    strFlnu = tabTab(1).GetFieldValue(LineNo, "AFTURNO")
'    strTransfertime = tabTab(1).GetFieldValue(LineNo, "TRANSFER")
'    strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", strFlnu, 0)
'    llAFTLineNo = frmData.tabData(0).GetNextResultLine
'    If llAFTLineNo > -1 Then
'        strStod = frmData.tabData(0).GetFieldValue(llAFTLineNo, "STOD")
'        strEtdi = frmData.tabData(0).GetFieldValue(llAFTLineNo, "ETDI")
'        strFlno = frmData.tabData(0).GetFieldValue(llAFTLineNo, "FLNO")
'        llBaa3 = Val(frmData.tabData(0).GetFieldValue(llAFTLineNo, "BAA3"))
'        llBaa3 = llBaa3 + 1
'        frmData.tabData(0).SetFieldValues llAFTLineNo, "BAA3", CStr(llBaa3)
'        blAFTFound = True
'    End If
'    If strFlnu <> "" Then
'        strRet = frmData.tabData(10).GetLinesByIndexValue("FLNU", strFlnu, 0)
'        llLineNo = frmData.tabData(10).GetNextResultLine
'        If llLineNo > -1 Then
'            ' SRLTAB entry for the current flight exists
'            frmData.tabData(10).DeleteLine llLineNo 'Remove the existing for the current FLNU
'        End If
'        lineIDX = frmData.InsertEmptyLineForTab(10)
'        strFields = "URNO,FLNU,AREA,READ,ADID,FLNO,STIM,ETIM,TYPE,TIME,TEXT,LINO,CDAT,LSTU,USEU,USEC"
'        strData = frmData.GetNextUrno + ","
'        strData = strData + strFlnu + ",A,X,D," + strFlno + "," + strStod + "," + strEtdi + ",," + strTimeValue + "," + strDecisionValue + ","
'        strData = strData + "D9999999," + strCdat + "," + strCdat + ",HUBMGR,HUBMGR"
'        frmData.tabData(10).SetFieldValues lineIDX, strFields, strData
'        frmData.tabData(10).Sort "1", True, True
'        If frmData.aUfis.CallServer("IRT", "SRLTAB", strFields, strData, "", 230) <> 0 Then
'            MsgBox "Insert Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
'        End If
'        If frmData.aUfis.CallServer("UFR", "AFTTAB", "BAA3", CStr(llBaa3), "WHERE URNO=" + strFlnu, 230) <> 0 Then
'            MsgBox "Insert Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
'        End If
'    End If
'End Sub

Private Sub tabTab_EditPositionChanged(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal Value As String)
    Dim strUrnoList As String
    Dim strRurn As String
    
    strRurn = tabTab(Index).GetFieldValue(LineNo, "LOA_RURN")
End Sub

Private Sub tabTab_InplaceEditCell(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal NewValue As String, ByVal OldValue As String)
    Dim strUrnoList As String
    Dim strRurn As String
    Dim llLine As Long
    Dim strUrno As String
    Dim arrUrnos() As String
    Dim strSTYP As String
    Dim i As Integer
    Dim blFound As Boolean
    Dim strWhere As String
    Dim strValues As String
    Dim ilEpax As Integer
    Dim ilBpax As Integer
    Dim ilFpax As Integer
    Dim ilUpax As Integer
    Dim ilIdx As Long
    Dim strTimeValue As String
    Dim strDecisionValue As String

    
    blFound = False
    If NewValue = OldValue Then
        Exit Sub
    End If
    If ColNo = 22 Or ColNo = 24 Then
        'kkh on 16/07/2008 P2 Store Decision into CFITAB
'        strUrno = tabTab(Index).GetFieldValue(LineNo, "AFTURNO")
'        strDecisi`onValue = tabTab(Index).GetFieldValue(LineNo, "Hold")
'        strTimeValue = tabTab(Index).GetFieldValue(LineNo, "TRANSFER")
'        StoreCFITAB LineNo, strDecisionValue, strTimeValue
        'StoreSRLTAB LineNo, strDecisionValue, strTimeValue
        
        StoreCFITABNew LineNo, Index
    Else
        strUrnoList = tabTab(Index).GetFieldValue(LineNo, "L_URNOS")
        arrUrnos = Split(strUrnoList, ";")
        For i = 0 To UBound(arrUrnos)
            strUrno = arrUrnos(i)
            strWhere = "WHERE URNO=" & strUrno
            frmData.tabData(3).GetLinesByIndexValue "URNO", strUrno, 0
            llLine = frmData.tabData(3).GetNextResultLine
            If llLine > -1 Then
                strSTYP = frmData.tabData(3).GetFieldValue(llLine, "STYP")
                'Customize for GOCC 25/02/2008
'                If ColNo = 10 And strSTYP = "F" Then
'                    blFound = True
'                    i = UBound(arrUrnos) + 1
'                End If
'                If ColNo = 11 And strSTYP = "B" Then
'                    blFound = True
'                    i = UBound(arrUrnos) + 1
'                End If
'                If ColNo = 12 And strSTYP = "E" Then
'                    blFound = True
'                    i = UBound(arrUrnos) + 1
'                End If
                If ColNo = 11 And strSTYP = "F" Then
                    blFound = True
                    i = UBound(arrUrnos) + 1
                End If
                If ColNo = 13 And strSTYP = "B" Then
                    blFound = True
                    i = UBound(arrUrnos) + 1
                End If
                If ColNo = 15 And strSTYP = "U" Then
                    blFound = True
                    i = UBound(arrUrnos) + 1
                End If
                If ColNo = 17 And strSTYP = "E" Then
                    blFound = True
                    i = UBound(arrUrnos) + 1
                End If
            End If
        Next i
        If blFound = True And llLine > -1 Then
            frmData.tabData(3).SetFieldValues llLine, "VALU", NewValue
            'LOATAB;URNO,FLNU,FLNO,DSSN,RURN,TYPE,STYP,SSTP,SSST,APC3,VALU,RFLD,RTAB;
            'LOATAB;URNO,FLNU,FLNO,DSSN,RURN,TYPE,STYP,SSTP,SSST,APC3,VALU,ADDI,HOPO;
            'kkh 02/02/2009 HOPO require
            'strValues = frmData.tabData(3).GetFieldValues(llLine, "URNO,FLNU,FLNO,DSSN,RURN,TYPE,STYP,SSTP,SSST,APC3,VALU,RFLD") ',RTAB")
            'strValues = frmData.tabData(3).GetFieldValues(llLine, "URNO,FLNU,FLNO,DSSN,RURN,TYPE,STYP,SSTP,SSST,APC3,VALU,RFLD,HOPO") ',RTAB")
            strValues = frmData.tabData(3).GetFieldValues(llLine, "URNO,FLNU,FLNO,DSSN,RURN,TYPE,STYP,SSTP,SSST,APC3,VALU,ADDI,HOPO") ',RTAB")
            strValues = strValues + ","
            strValues = CleanNullValues(strValues)
            frmData.SetServerParameters
            If frmData.aUfis.CallServer("URT", "LOATAB", frmData.tabData(3).LogicalFieldList, _
                                        strValues, strWhere, "230") <> 0 Then
                MsgBox "Update Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
            Else
                frmData.SetServerParameters
                frmData.aUfis.CallServer "SBC", "LOA:URT", frmData.tabData(3).LogicalFieldList, strValues, omCurrentSelectedUrno, "230"
            End If
            '"AFTURNO,LOA_RURN,ACT,FLNO,DATE,STD,EST,ATD,DES,T-PAX,F-PAX,B-PAX,E-PAX,NA,Hold,DUMMY,L_URNOS"
            ilEpax = Val(tabTab(Index).GetFieldValue(LineNo, "Y-PAX"))
            ilBpax = Val(tabTab(Index).GetFieldValue(LineNo, "C-PAX"))
            ilFpax = Val(tabTab(Index).GetFieldValue(LineNo, "F-PAX"))
            'Customize for GOCC 25/02/2008
            ilUpax = Val(tabTab(Index).GetFieldValue(LineNo, "U-PAX"))
            'tabTab(Index).SetFieldValues LineNo, "T-PAX", CStr(ilEpax + ilBpax + ilFpax)
            tabTab(Index).SetFieldValues LineNo, "T-PAX", CStr(ilEpax + ilBpax + ilFpax + ilUpax)
            tabTab(Index).Refresh
        End If
    End If
End Sub

Private Sub tabTab_SendRButtonClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim llCurrLine As Long
    If Index = 1 Then
        tabTab_SendLButtonClick Index, LineNo, ColNo
        PopupMenu mnuDetailTabContext
    End If
End Sub

Private Sub Timer1_Timer()
    Timer1.Enabled = False
End Sub

Private Sub timerConflictCheck_Timer()
    'CheckAllArrivalsForConflicts
End Sub

Private Sub timerFlnuBcs_Timer()
    'kkh on 19/06/2008 UTC/LOCAL time
    'HandleTlxBC
End Sub

Private Sub TimerRefresh_Timer()
    gantt(0).Refresh
    gantt(1).Refresh
    tabTab(0).Refresh
    tabTab(1).Refresh
    If bgfrmCompressedFlightsOpen = True Then
        frmCompressedFligths.gantt.Refresh
    End If
    TimerRefresh.Enabled = False
End Sub


Private Sub tabTab_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim strColor As String
    Dim i As Long
    If Index = 1 Then
        If Selected = False Then
            'tabTab(Index).ResetLineDecorations LineNo
        Else
'            strColor = vbBlue & "," & vbBlue
'            tabTab(1).CreateDecorationObject "CURSOR", "T,B", "2,2", strColor
'            For i = 0 To ItemCount(tabTab(1).HeaderString, ",") - 1
'                tabTab(1).SetDecorationObject LineNo, i, "CURSOR"
'            Next i
            If cmdTelex.Value = 1 Then
                ShowTelexForLine LineNo
            End If
            'kkh on 25/07/2008 P2 View Decision Log
            If cbToolButton(11).Value = 1 Then
                frmViewLog.tabL04.ResetContent
                frmViewLog.tabL04.Refresh
                frmViewLog.TabReadLog tabTab(1).GetFieldValue(LineNo, "CFI_URNO")
            End If
        End If
        tabTab(0).Refresh
        tabTab_SendLButtonClick Index, LineNo, -1
    End If
End Sub

Private Sub tabTab_SendLButtonClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim cnt As Long
    Dim bardata() As UGANTTLib.BAR_RECORD
    Dim strUrno As String
    Dim llOldColor As Long
    Dim strVal As String
    Dim i As Long
    Dim currKey As String
    
    
    If Index = 1 Then ' For the connection times tab
        currKey = tabTab(Index).GetFieldValue(LineNo, "AFTURNO")
        cnt = frmData.tabCorrespondingBars.GetLineCount
        If cnt > 0 Then
            For i = 0 To cnt - 1
                strUrno = frmData.tabCorrespondingBars.GetFieldValue(i, "URNO")
                If (strUrno <> "") Then
                    strVal = frmData.tabCorrespondingBars.GetFieldValue(i, "OLDCOLOR")
                    gantt(0).SetBarColor strUrno, Val(strVal)
                    gantt(1).SetBarColor strUrno, Val(strVal)
                    If bgfrmCompressedFlightsOpen = True Then
                        frmCompressedFligths.gantt.SetBarColor strUrno, Val(strVal)
                    End If
                End If
            Next i
        End If
        
        'now get the properties of the current bar, store them int the tab
        'and set the mark color
        frmData.tabCorrespondingBars.ResetContent
        bardata = gantt(0).GetBarRecord(currKey)
        strVal = currKey + "," + CStr(colorDepartureBar) 'CStr(bardata(0).backColor)
        frmData.tabCorrespondingBars.InsertTextLine strVal, True
        'mark current bar
        gantt(0).SetBarColor currKey, colorCorrespondingBar
        gantt(0).Refresh
        gantt(1).DeleteAllTimeMarkers
        gantt(1).SetBarColor currKey, colorCorrespondingBar
        gantt(1).Refresh
        gantt(1).SetTimeMarker bardata(0).Begin, colYellow
        gantt(1).SetTimeMarker bardata(0).End, colYellow
        If bgfrmCompressedFlightsOpen = True Then
            frmCompressedFligths.gantt.SetBarColor currKey, colorCorrespondingBar
            frmCompressedFligths.gantt.Refresh
        End If
    End If 'Index = 1
    
End Sub


'**********************************************************************************
'**********************************************************************************
'Gantt events:
'               All events are beeing forwarded to a possible script function
'**********************************************************************************
'**********************************************************************************
Private Sub gantt_ActualLineNo(Index As Integer, ByVal ActualLineNo As Long)
    MainStatusBar.Panels(1).Text = ""
End Sub

Private Sub gantt_ActualTimescaleDate(Index As Integer, ByVal TimescaleDate As Date)
    lblAboveText(0) = Format(TimescaleDate, "dd.mm.YY")
End Sub
Private Sub gantt_OnLButtonDownBar(Index As Integer, ByVal Key As String, ByVal nFlags As Integer)
    If Index = 0 Then ' clicked on main gantt
        SelectMainGanttBar Key, nFlags
    End If
    If Index = 1 Then ' clicked on detail gantt
    End If
    Exit Sub
myErr:
    MsgBox Err.Description
End Sub

Public Sub SingleBarConflictCheck(Key As String)
    Dim llLine As Long
    Dim strTifa As String
    Dim datTifa As Date
    
    frmData.tabData(0).GetLinesByIndexValue "URNO", Key, 0
    llLine = frmData.tabData(0).GetNextResultLine
    If llLine > -1 Then
        strTifa = frmData.tabData(0).GetFieldValue(llLine, "TIFA")
        If Trim(strTifa) <> "" Then
            datTifa = CedaFullDateToVb(strTifa)
        End If
        If HasArrivalConflict(Key, datTifa, True) = True Then
            If Key = omCurrentSelectedUrno Then
                If frmData.tabCurrentBar.GetLineCount > 0 Then
                    frmData.tabCurrentBar.SetColumnValue 0, 1, CStr(vbRed)
                Else
                    frmData.tabCurrentBar.SetColumnValue 0, 1, CStr(colorArrivalBar)
                End If
            Else
                gantt(0).SetBarColor Key, vbRed
                gantt(0).Refresh
                If bgfrmCompressedFlightsOpen = True Then
                    frmCompressedFligths.gantt.SetBarColor Key, vbRed
                    frmCompressedFligths.gantt.Refresh
                End If
            End If
        Else
            If Key = omCurrentSelectedUrno Then
                If frmData.tabCurrentBar.GetLineCount > 0 Then
                    frmData.tabCurrentBar.SetColumnValue 0, 1, CStr(colorArrivalBar)
                End If
                If bgfrmCompressedFlightsOpen = True Then
                    If frmCompressedFligths.cbToolButton(2).Value = 1 Then
                        frmCompressedFligths.gantt.DeleteBarByKey Key
                    End If
                End If
            Else
                gantt(0).SetBarColor Key, colorArrivalBar
                gantt(0).Refresh
                If bgfrmCompressedFlightsOpen = True Then
                    If frmCompressedFligths.cbToolButton(2).Value = 0 Then
                        frmCompressedFligths.gantt.SetBarColor Key, colorArrivalBar
                        frmCompressedFligths.gantt.Refresh
                    Else
                        frmCompressedFligths.gantt.DeleteBarByKey Key
                    End If
                End If
            End If
        End If
    End If
End Sub
Public Sub SelectMainGanttBar(ByVal Key As String, ByVal nFlags As Integer)
    Dim strBarKey As String
    Dim myGantt As UGANTTLib.UGantt
    Dim strRKEY As String
    Dim strTmp As String
    Dim strValue As String
    Dim cnt As Long
    Dim i As Long
    Dim llLine As Long
    Dim llRkeyLine As Long
    Dim strTabline As String
    Dim strEmptyArr As String
    Dim strEmptyDep As String
    Dim llRLine As Long
    Dim MyDate As Date
    Dim StartTime As Date
    Dim endTime As Date
    Dim datArrSTOA As Date
    Dim strAdid As String
    Dim ilWhatFlight As Integer '1 = Arrival, 2 = Departure, 3 = Rotation
    Dim strStoa As String
    Dim strStod As String
    Dim strTifa As String
    Dim strTIFD As String
    
    'Time Markers
    omCurrentSelectedUrno = Key
    llLine = gantt(0).GetLineNoByBarKey(Key)
    If llLine = -1 Then
        Set myGantt = frmCompressedFligths.gantt
    Else
        Set myGantt = gantt(0)
    End If
    myGantt.DeleteAllTimeMarkers
    StartTime = myGantt.GetBarStartTime(Key)
    endTime = myGantt.GetBarEndTime(Key)
    myGantt.SetTimeMarker StartTime, colYellow
    myGantt.SetTimeMarker endTime, colYellow
    If bgfrmCompressedFlightsOpen = True Then
        frmCompressedFligths.gantt.DeleteAllTimeMarkers
        StartTime = frmCompressedFligths.gantt.GetBarStartTime(Key)
        endTime = frmCompressedFligths.gantt.GetBarEndTime(Key)
        frmCompressedFligths.gantt.SetTimeMarker StartTime, colYellow
        frmCompressedFligths.gantt.SetTimeMarker endTime, colYellow
    End If
    
    MarkCurrentBar Key, 0
    'Rotation TAB
    ilWhatFlight = 0
    strEmptyArr = ",,,,,,,,,,,"
    strEmptyDep = ",,,,,,,,,,"
    strTabline = String(ItemCount(tabTab(0).HeaderLengthString, ","), ",") 'strEmptyArr + strEmptyDep
    llLine = 0
    tabTab(0).ResetContent
    tabTab(1).ResetContent
    tabTab(1).Refresh
    tabTab(0).InsertTextLine strTabline, False
    
    'Search for RKEY
    strTmp = frmData.tabData(0).GetLinesByIndexValue("URNO", Key, 0)
    llRLine = frmData.tabData(0).GetNextResultLine()
    llRkeyLine = Val(frmData.tabData(0).GetFieldValue(llRLine, "RKEY"))
    strAdid = frmData.tabData(0).GetFieldValue(llRLine, "ADID")
    strTmp = frmData.tabData(0).GetLinesByIndexValue("RKEY", llRkeyLine, 0)
    'cnt = CLng(strTmp)
    While llLine >= 0
        llLine = frmData.tabData(0).GetNextResultLine
        If llLine >= 0 Then
            'Arrival
            If frmData.tabData(0).GetFieldValue(llLine, "ADID") = "A" Or frmData.tabData(0).GetFieldValue(llLine, "ADID") = "B" Then
                ilWhatFlight = ilWhatFlight + 1
                strStoa = frmData.tabData(0).GetFieldValue(llLine, "STOA")
                If Trim(strStoa) <> "" Then
                    datArrSTOA = CedaFullDateToVb(frmData.tabData(0).GetFieldValue(llLine, "STOA"))
                End If
                strValue = frmData.tabData(0).GetFieldValue(llLine, "FLNO")
                tabTab(0).SetFieldValues 0, "FLNO_A", strValue
                
                'kkh on 25/11/2008 cater for delay timing
                strValue = frmData.tabData(0).GetFieldValue(llLine, "STOA")
                strValue = Trim(strValue)
                If Trim(strValue) <> "" Then
                    MyDate = CedaFullDateToVb(strValue)
                    tabTab(0).SetFieldValues 0, "DATE_A", Format(MyDate, "DD.MM.YY")
                    MyDate = CedaFullDateToVb(strValue)
                    tabTab(0).SetFieldValues 0, "STA", Format(MyDate, "hh:mm") + "/" + Mid(strValue, 7, 2)
                    tabTab(0).SetFieldValues 0, "STOA_A", strValue
                End If
                strValue = frmData.tabData(0).GetFieldValue(llLine, "ETAI")
                strValue = Trim(strValue)
                If Trim(strValue) <> "" Then
                    MyDate = CedaFullDateToVb(strValue)
                    tabTab(0).SetFieldValues 0, "ETA", Format(MyDate, "hh:mm") + "/" + Mid(strValue, 7, 2)
                End If
                strValue = frmData.tabData(0).GetFieldValue(llLine, "ONBL")
                strValue = Trim(strValue)
                If Trim(strValue) <> "" Then
                    MyDate = CedaFullDateToVb(strValue)
                    tabTab(0).SetFieldValues 0, "ONBL", Format(MyDate, "hh:mm") + "/" + Mid(strValue, 7, 2)
                End If
                
'                strValue = frmData.tabData(0).GetFieldValue(llLine, "TIFA")
'                strValue = Trim(strValue)
'                If Trim(strValue) <> "" Then
'                    MyDate = CedaFullDateToVb(strValue)
'                    tabTab(0).SetFieldValues 0, "DATE_A", Format(MyDate, "DD.MM.YY")
'                End If
'                strValue = frmData.tabData(0).GetFieldValue(llLine, "STOA")
'                strValue = Trim(strValue)
'                If Trim(strValue) <> "" Then
'                    MyDate = CedaFullDateToVb(strValue)
'                    tabTab(0).SetFieldValues 0, "STOA_A", strValue
'                End If
'                strValue = frmData.tabData(0).GetFieldValue(llLine, "ETAI")
'                strValue = Trim(strValue)
'                If Trim(strValue) <> "" Then
'                    MyDate = CedaFullDateToVb(strValue)
'                    tabTab(0).SetFieldValues 0, "ETA", Format(MyDate, "hh:mm")
'                End If
'                strValue = frmData.tabData(0).GetFieldValue(llLine, "ONBL")
'                strValue = Trim(strValue)
'                If Trim(strValue) <> "" Then
'                    MyDate = CedaFullDateToVb(strValue)
'                    tabTab(0).SetFieldValues 0, "ONBL", Format(MyDate, "hh:mm")
'                End If
                strValue = frmData.tabData(0).GetFieldValue(llLine, "TTYP")
                tabTab(0).SetFieldValues 0, "NA_A", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "ORG3")
                tabTab(0).SetFieldValues 0, "ORG", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "VIA3")
                tabTab(0).SetFieldValues 0, "VIA_A", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "PSTA")
                tabTab(0).SetFieldValues 0, "POS_A", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "TGA1")
                tabTab(0).SetFieldValues 0, "TGA_A", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "GTA1")
                tabTab(0).SetFieldValues 0, "GAT_A", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "ACT3")
                tabTab(0).SetFieldValues 0, "A/C_A", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "REGN")
                tabTab(0).SetFieldValues 0, "REGN_A", strValue
                'kkh on 16/07/2008 P2 Store Decision into CFITAB
                strValue = frmData.tabData(0).GetFieldValue(llLine, "URNO")
                tabTab(0).SetFieldValues 0, "URNO_A", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "STOA")
                tabTab(0).SetFieldValues 0, "FLIGHT_DATE", strValue
            End If
            'Departure
            If frmData.tabData(0).GetFieldValue(llLine, "ADID") = "D" Or frmData.tabData(0).GetFieldValue(llLine, "ADID") = "B" Then
                ilWhatFlight = ilWhatFlight + 2
                strValue = frmData.tabData(0).GetFieldValue(llLine, "FLNO")
                tabTab(0).SetFieldValues 0, "FLNO_D", strValue
                
                 'kkh on 25/11/2008 cater for delay timing
                strValue = frmData.tabData(0).GetFieldValue(llLine, "STOD")
                strValue = Trim(strValue)
                If Trim(strValue) <> "" Then
                    MyDate = CedaFullDateToVb(strValue)
                    tabTab(0).SetFieldValues 0, "DATE_D", Format(MyDate, "DD.MM.YY")
                    MyDate = CedaFullDateToVb(strValue)
                    tabTab(0).SetFieldValues 0, "STD", Format(MyDate, "hh:mm") + "/" + Mid(strValue, 7, 2)
                End If
                strValue = frmData.tabData(0).GetFieldValue(llLine, "ETDI")
                strValue = Trim(strValue)
                If Trim(strValue) <> "" Then
                    MyDate = CedaFullDateToVb(strValue)
                    tabTab(0).SetFieldValues 0, "ETD", Format(MyDate, "hh:mm") + "/" + Mid(strValue, 7, 2)
                End If
                strValue = frmData.tabData(0).GetFieldValue(llLine, "OFBL")
                strValue = Trim(strValue)
                If Trim(strValue) <> "" Then
                    MyDate = CedaFullDateToVb(strValue)
                    tabTab(0).SetFieldValues 0, "OFBL", Format(MyDate, "hh:mm") + "/" + Mid(strValue, 7, 2)
                End If
                
'                strValue = frmData.tabData(0).GetFieldValue(llLine, "TIFD")
'                strValue = Trim(strValue)
'                If Trim(strValue) <> "" Then
'                    MyDate = CedaFullDateToVb(strValue)
'                    tabTab(0).SetFieldValues 0, "DATE_D", Format(MyDate, "DD.MM.YY")
'                End If
'                strValue = frmData.tabData(0).GetFieldValue(llLine, "STOD")
'                strValue = Trim(strValue)
'                If Trim(strValue) <> "" Then
'                    MyDate = CedaFullDateToVb(strValue)
'                    tabTab(0).SetFieldValues 0, "STD", Format(MyDate, "hh:mm")
'                End If
'                strValue = frmData.tabData(0).GetFieldValue(llLine, "ETDI")
'                strValue = Trim(strValue)
'                If Trim(strValue) <> "" Then
'                    MyDate = CedaFullDateToVb(strValue)
'                    tabTab(0).SetFieldValues 0, "ETD", Format(MyDate, "hh:mm")
'                End If
'                strValue = frmData.tabData(0).GetFieldValue(llLine, "OFBL")
'                strValue = Trim(strValue)
'                If strValue <> "" Then
'                    If Trim(strValue) <> "" Then
'                        MyDate = CedaFullDateToVb(strValue)
'                        tabTab(0).SetFieldValues 0, "OFBL", Format(MyDate, "hh:mm")
'                    End If
'                End If
                strValue = frmData.tabData(0).GetFieldValue(llLine, "TTYP")
                tabTab(0).SetFieldValues 0, "NA_D", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "VIA3")
                tabTab(0).SetFieldValues 0, "VIA_D", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "DES3")
                tabTab(0).SetFieldValues 0, "DES", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "PSTD")
                tabTab(0).SetFieldValues 0, "POS_D", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "TGD1")
                tabTab(0).SetFieldValues 0, "TGD_D", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "GTD1")
                tabTab(0).SetFieldValues 0, "GAT_D", strValue
                strValue = frmData.tabData(0).GetFieldValue(llLine, "REGN")
                tabTab(0).SetFieldValues 0, "REGN_D", strValue
            End If
        End If
    Wend
    'Get LOATAB Info into the tabdata for transfer pax
    tabTab(0).Refresh
    gantt(1).ResetContent
    gantt(1).TabHeaderLengthString = "100"                   'init the width of the tab-columns
    gantt(1).TabSetHeaderText ("Connections")                   'set the tab-headlines
    gantt(1).SplitterPosition = 100                          'the splitterposition depends on the
    gantt(1).TabBodyFontName = "Arial"
    gantt(1).TabFontSize = slFontSize.Value
    gantt(1).TabHeaderFontName = "Arial"
    gantt(1).TabHeaderFontSize = 14
    gantt(1).BarOverlapOffset = 10
    gantt(1).TabBodyFontBold = True
    gantt(1).TabHeaderFontBold = True
    
    If strAdid = "A" Then
        'KKH cater for Kriscom update
        HandleTlxBC
        SetLoaTabInfoToTab Key, datArrSTOA
    End If
    
End Sub

Private Sub gantt_OnMouseMoveBar(Index As Integer, ByVal Key As String)
    Dim strValues As String
    Dim strVal As String
    Dim strRet As String
    Dim llCurrLine As Long
    Dim strAdid As String
    Dim myDat As Date
    
    MainStatusBar.Panels(1).Text = ""
    If Index = 0 Then
        strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", Key, 0)
        llCurrLine = frmData.tabData(0).GetNextResultLine
        If llCurrLine > -1 Then
            strAdid = frmData.tabData(0).GetFieldValue(llCurrLine, "ADID")
            If strAdid = "A" Then
                strValues = frmData.tabData(0).GetFieldValue(llCurrLine, "ORG3") + ": STA="
                strVal = frmData.tabData(0).GetFieldValue(llCurrLine, "STOA")
                If Trim(strVal) <> "" Then
                    myDat = CedaFullDateToVb(strVal)
                    strValues = strValues + Format(myDat, "hh:mm")
                    strVal = frmData.tabData(0).GetFieldValue(llCurrLine, "ETAI")
                    If Trim(strVal) <> "" Then
                        myDat = CedaFullDateToVb(strVal)
                        strValues = strValues + " ETA=" + Format(myDat, "hh:mm")
                    End If
                End If
            Else
                strValues = frmData.tabData(0).GetFieldValue(llCurrLine, "DES3") + ": STD="
                strVal = frmData.tabData(0).GetFieldValue(llCurrLine, "STOD")
                If Trim(strVal) <> "" Then
                    myDat = CedaFullDateToVb(strVal)
                    strValues = strValues + Format(myDat, "hh:mm")
                    strVal = frmData.tabData(0).GetFieldValue(llCurrLine, "ETDI")
                    If Trim(strVal) <> "" Then
                        myDat = CedaFullDateToVb(strVal)
                        strValues = strValues + " ETD=" + Format(myDat, "hh:mm")
                    End If
                End If
            End If
        End If
        MainStatusBar.Panels(1).Text = strValues
    End If
End Sub

Public Sub AdjustCurrentBarColor(strUrno As String, newColor As Long)
    Dim i As Long
    Dim cnt As Long
    Dim strVal As String
    
    cnt = frmData.tabCurrentBar.GetLineCount
    If cnt > 0 Then
        For i = 0 To cnt - 1
            strUrno = frmData.tabCurrentBar.GetFieldValue(i, "URNO")
            If (strUrno <> "") Then
                strVal = CStr(newColor) 'frmData.tabCurrentBar.GetFieldValue(i, "OLDCOLOR")
                frmData.tabCurrentBar.SetColumnValue i, 1, strVal
            End If
        Next i
    End If

End Sub
'-----------------------------------------------------------
' Sets the color of the current bar to mark color
' and stores the regular color in a helper tab
'-----------------------------------------------------------
Sub MarkCurrentBar(Key As String, IDXGantt As Integer)
    'first restore the old current bar back with his regular color
    Dim cnt As Long
    Dim bardata() As UGANTTLib.BAR_RECORD
    Dim strUrno As String
    Dim llOldColor As Long
    Dim strVal As String
    Dim i As Long
    Dim dummyDate As Date
    
    cnt = frmData.tabCurrentBar.GetLineCount
    If cnt > 0 Then
        For i = 0 To cnt - 1
            strUrno = frmData.tabCurrentBar.GetFieldValue(i, "URNO")
            If (strUrno <> "") Then
                strVal = frmData.tabCurrentBar.GetFieldValue(i, "OLDCOLOR")
                gantt(IDXGantt).SetBarColor strUrno, Val(strVal)
                If bgfrmCompressedFlightsOpen = True Then
                    frmCompressedFligths.gantt.SetBarColor strUrno, Val(strVal)
                End If
            End If
        Next i
    End If
    
    'now get the properties of the current bar, store them int the tab
    'and set the mark color
    frmData.tabCurrentBar.ResetContent
    
    dummyDate = gantt(IDXGantt).GetBarStartTime(Key)

    bardata = gantt(IDXGantt).GetBarRecord(Key)
    If bardata(0).Key = "" Then
        If bgfrmCompressedFlightsOpen = True Then
            bardata = frmCompressedFligths.gantt.GetBarRecord(Key)
            strVal = Key + "," + CStr(bardata(0).backColor)
            frmData.tabCurrentBar.InsertTextLine strVal, True
            'mark current bar
            frmCompressedFligths.gantt.SetBarColor Key, colorCurrentBar
            frmCompressedFligths.gantt.Refresh
        End If
    Else
        strVal = Key + "," + CStr(bardata(0).backColor)
        frmData.tabCurrentBar.InsertTextLine strVal, True
        'mark current bar
        gantt(0).SetBarColor Key, colorCurrentBar
        gantt(0).Refresh
    End If
    If bgfrmCompressedFlightsOpen = True Then
        frmCompressedFligths.gantt.SetBarColor Key, colorCurrentBar
        frmCompressedFligths.gantt.Refresh
    End If
End Sub
'---------------------------------------------------------
' Fetches all corresponding data for the urno(KEY) from
' loatab and puts it into the fields of the transfer tab
'---------------------------------------------------------
Public Sub SetLoaTabInfoToTab(Key As String, opStoa As Date)
    Dim i As Long
    Dim strVal As String
    Dim strEmptLine As String
    Dim llLine As Long
    Dim llDepLine As Long
    Dim llAftLine As Long
    Dim llPaxTot As Long
    Dim strRawFlno As String
    Dim strFlno As String
    Dim strPriorFlno As String
    Dim strFlightDay As String
    Dim strLineValues As String
    Dim llCurrLine As Long
    Dim strAdid As String
    Dim arr() As String
    Dim strSTYP As String
    Dim datStod As Date
    Dim strVALU As String
    Dim strArrUrno As String
    Dim strDepUrno As String
    Dim blCorrectTimeFound As Boolean
    Dim llF As Long, llB As Long, llE As Long, llU As Long
    Dim llBagTotal As Long, llBwtTotal As Long
    Dim strTmpEBag As Long, strTmpFBag As Long, strTmpBBag As Long, strTmpUBag As Long
    Dim strTmpEBwt As Long, strTmpFBwt As Long, strTmpBBwt As Long, strTmpUBwt As Long
    Dim dummy1 As String, dummy2 As String, dummy3 As String
    Dim minDate As Date ' for the detail gantt
    Dim maxDate As Date ' for the detail gantt
    Dim TFStart As Date ' for the detail gantt
    Dim TFEnd As Date ' for the detail gantt
    Dim myDepBar() As UGANTTLib.BAR_RECORD
'    Dim myNewBar 'As UGANTTLib.BAR_RECORD
    Dim llCurrDetailLine As Long
    Dim timeframe As Long
    Dim strMinDate As String
    Dim strMaxDate As String
    Dim isFirst As Boolean
    Dim strLoaUrno As String
    'kkh on 16/07/2008 P2 Store Decision into CFITAB
    Dim strRet2 As String
    Dim fieldVal As String
    Dim strCFIAddi As String
    'kkh on 04/07/2008 If already Airbone (AIRB <> "") do not display in the list
    Dim strShowAIRB As String
    Dim arrDel() As String
    Dim cntDel As Integer
    cntDel = 0
    strShowAIRB = GetIniEntry("", "HUBMANAGER", "GLOBAL", "AIRBORNE_SHOW", "")
    'kkh 06/02/2009 APC3
    Dim strLoaApc3 As String
    Dim strLoaAddi As String
       
    'kkh on 06/01/2009 via info
    Dim strVIAL As String
    Dim viaLen As Integer
    Dim tmpVia As String
    
    Dim strRawFlnoCopy As String
    Dim dummyDate As Date
    Dim strLoaTabUrnos As String
    Dim strLUrno As String
    Dim strTmp As String
    Dim strDSSN As String
    Dim strTYPE As String
    Dim tmpPax As Integer
    Dim tmpStrPax As String
    Dim llSRLLine As Long
    Dim strSRLText As String
    
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    Dim str4 As String
    Dim arrTotalBagY() As String
    Dim arrTotalBagF() As String
    Dim arrTotalBagC() As String
    Dim arrTotalBagU() As String
    
    'kkh on 28/11/2008 Display KRI as higher priority then PTM message for all flights
    Dim strCheckFlno As String
    Dim byPassPTM As Boolean
    Dim strShowKRIPrio As String
    Dim llTempLine As Long 'igu on 23/08/2011: ALTEA
    strShowKRIPrio = GetIniEntry("", "HUBMANAGER", "GLOBAL", "KRI_PRIO", "")
    
    strTmpEBag = 0
    strTmpFBag = 0
    strTmpBBag = 0
    strTmpUBag = 0
    
    strTmpEBwt = 0
    strTmpFBwt = 0
    strTmpBBwt = 0
    strTmpUBwt = 0
    
    llBagTotal = 0
    llBwtTotal = 0
            
    llCurrDetailLine = 0
    isFirst = True
    TFStart = Now
    strMinDate = Format(TFStart, "YYYYMMDD000000")
    strMaxDate = Format(TFStart, "YYYYMMDD235959")
    TFStart = CedaFullDateToVb(strMaxDate)
    TFEnd = CedaFullDateToVb(strMinDate)
    tabTab(1).ResetContent
    frmData.tabData(3).GetLinesByIndexValue "FLNU", Key, 0
    'llLine = frmData.tabData(3).GetNextResultLine 'igu on 23/08/2011: ALTEA
    Call CopyToSubLoaTab 'igu on 23/08/2011: ALTEA
    
    strPriorFlno = ""
    llCurrLine = 0
                  
    'strEmptLine = ",,,,,,,,,,,,,,,,,"
    'strEmptLine = ",,,,,,,,,,,,,,,,,,,,,,,"
    strEmptLine = ",,,,,,,,,,,,,,,,,,,,,,,,,"
    tabTab(1).InsertTextLine strEmptLine, False
    strArrUrno = Key
    'While llLine <> -1 'igu on 23/08/2011: ALTEA
    For llLine = 0 To frmData.tabSubLOA.GetLineCount - 1 'igu on 23/08/2011: ALTEA
        blCorrectTimeFound = False
'        ' Insert an empty line
'        strRawFlno = frmData.tabData(3).GetFieldValue(llLine, "FLNO")
        
        'strRawFlno = frmData.tabData(3).GetFieldValue(llLine, "FLNO") 'igu on 23/08/2011: ALTEA
        strRawFlno = frmData.tabSubLOA.GetFieldValue(llLine, "FLNO") 'igu on 23/08/2011: ALTEA
        'strLoaUrno = frmData.tabData(3).GetFieldValue(llLine, "RURN") 'igu on 23/08/2011: ALTEA
        strLoaUrno = frmData.tabSubLOA.GetFieldValue(llLine, "RURN") 'igu on 23/08/2011: ALTEA
        'kkh on 28/11/2008 Display KRI as higher priority then PTM message for all flights
        byPassPTM = False
        
        'kkh KRI_PRIO to control whether to show both KRISCOM and PTM message in screen 3
        If strShowKRIPrio = "YES" Then
            If frmData.tabSubLOA.GetFieldValue(llLine, "DSSN") = "PTM" Then 'igu on 10/01/2012: ALTEA
            'If frmData.tabData(3).GetFieldValue(llLine, "DSSN") = "PTM" Then
                If Len(strRawFlno) = "6" And Mid(strRawFlno, 3, 1) = 0 Then
                    strCheckFlno = Mid(strRawFlno, 1, 2) + Mid(strRawFlno, 4, 3)
                Else
                    strCheckFlno = strRawFlno
                End If
                For i = 0 To tabTab(1).GetLineCount - 1
                    If strCheckFlno = Replace(tabTab(1).GetFieldValue(i, "FLNO"), " ", "") And strLoaUrno <> tabTab(1).GetFieldValue(i, "LOA_RURN") Then
                        byPassPTM = True
                    End If
                Next i
                byPassPTM = True
            End If
        End If
        
        'kkh 06/02/2009 APC3
        'strLoaApc3 = frmData.tabData(3).GetFieldValue(llLine, "APC3") 'igu on 23/08/2011: ALTEA
        strLoaApc3 = frmData.tabSubLOA.GetFieldValue(llLine, "APC3") 'igu on 23/08/2011: ALTEA
        'strLoaAddi = frmData.tabData(3).GetFieldValue(llLine, "ADDI") 'igu on 23/08/2011: ALTEA
        strLoaAddi = frmData.tabSubLOA.GetFieldValue(llLine, "ADDI") 'igu on 23/08/2011: ALTEA
        arr = Split(strRawFlno, "/")
        If UBound(arr) = 1 Then
            strFlno = arr(0)
            strFlightDay = arr(1)
        Else
            strFlno = strRawFlno
            strFlightDay = ""
        End If
        If strFlno = "QF031" Then
            strFlno = strFlno
        End If
        strFlno = StripAftFlno(strFlno, dummy1, dummy2, dummy3)
        'kkh on 21/04/2008
        strLoaAddi = Mid(Trim(strLoaAddi), 14, 2)
        If strLoaAddi <> "" Then
            'kkh 06/02/2009 APC3
            'strRawFlno = strFlno + "/" + strLoaAddi
            strRawFlno = strFlno + "/" + strLoaAddi + "/" + strLoaApc3
        End If
        If UBound(arr) = 1 Then
            If arr(1) <> "" Then
                strRawFlno = strFlno + "/" + strFlightDay
            End If
        End If
        
        If byPassPTM = False Then
            If strRawFlno <> strPriorFlno Then
                'MWOXXX If strFlno <> strPriorFlno Then
                'kkh on 09/04/2008 GOCC PRF 0000013: Connecting flight data does not match PTM
                'llAftLine = GetCorrectDepartureByFlightDay(opStoa, strFlightDay, strFlno)
                'strLoaAddi = Mid(Trim(strLoaAddi), 14, 2)
                llAftLine = GetCorrectDepartureByFlightDay(opStoa, strFlightDay, strFlno, strLoaAddi)
    
                If Trim(strFlightDay) <> "" Then
                    strRawFlnoCopy = strFlno + "/" + strFlightDay
                Else
                    strRawFlnoCopy = strFlno
                End If
                'llAftLine = GetCorrectDepartureLine(Key, opStoa, strFlno)
                If llAftLine <> -1 Then
                    strAdid = frmData.tabData(0).GetFieldValue(llAftLine, "ADID")
                    If strAdid = "D" Then 'only if departure
                        strDepUrno = frmData.tabData(0).GetFieldValue(llAftLine, "URNO")
                            
                        'kkh on 06/01/2009 via info
                        strVIAL = frmData.tabData(0).GetFieldValue(llAftLine, "VIAL")
                        tabTab(1).SetFieldValues llCurrLine, "AFTURNO", strDepUrno
                        tabTab(1).SetFieldValues llCurrLine, "LOA_RURN", strLoaUrno 'Ist die RURN = TelexUrno to find the telex immediately
                        tabTab(1).SetFieldValues llCurrLine, "ACT", frmData.tabData(0).GetFieldValue(llAftLine, "ACT3")
                        tabTab(1).SetFieldValues llCurrLine, "FLNO", strRawFlnoCopy
                        tabTab(1).SetFieldValues llCurrLine, "TGD_D", frmData.tabData(0).GetFieldValue(llAftLine, "TGD1")
                        tabTab(1).SetFieldValues llCurrLine, "GATE", frmData.tabData(0).GetFieldValue(llAftLine, "GTD1")
                        tabTab(1).SetFieldValues llCurrLine, "STD", frmData.tabData(0).GetFieldValue(llAftLine, "STOD")
                        tabTab(1).SetFieldValues llCurrLine, "EST", frmData.tabData(0).GetFieldValue(llAftLine, "ETDI")
                        tabTab(1).SetFieldValues llCurrLine, "ATD", frmData.tabData(0).GetFieldValue(llAftLine, "AIRB")
                        'kkh 06/02/2009 APC3
                        tabTab(1).SetFieldValues llCurrLine, "DES", strLoaApc3
                        tabTab(1).SetFieldValues llCurrLine, "NA", frmData.tabData(0).GetFieldValue(llAftLine, "TTYP")
                        'kkh 17/02/2009
                        If strVIAL <> "" Then
                            viaLen = Len(strVIAL)
                            If viaLen < 120 Then
                                tmpVia = Mid(strVIAL, 2, 3) ' & "/" & lblDep(14).Tag
                            End If
                            If viaLen > 120 And viaLen < 240 Then
                                tmpVia = Mid(strVIAL, 2, 3) & "/" & Mid(strVIAL, 122, 3)
                            End If
                            If viaLen > 240 And viaLen < 360 Then
                                tmpVia = Mid(strVIAL, 2, 3) & "/" & Mid(strVIAL, 122, 3) & "/" & Mid(strVIAL, 242, 3)
                            End If
                            If viaLen > 360 And viaLen < 480 Then
                                tmpVia = Mid(strVIAL, 2, 3) & "/" & Mid(strVIAL, 122, 3) & "/" & Mid(strVIAL, 242, 3) & "/" & Mid(strVIAL, 362, 3)
                            End If
                            If viaLen > 480 And viaLen < 600 Then
                                tmpVia = Mid(strVIAL, 2, 3) & "/" & Mid(strVIAL, 122, 3) & "/" & Mid(strVIAL, 242, 3) & "/" & Mid(strVIAL, 362, 3) & "/" & Mid(strVIAL, 482, 3)
                            End If
                            If viaLen > 600 And viaLen < 720 Then
                                tmpVia = Mid(strVIAL, 2, 3) & "/" & Mid(strVIAL, 122, 3) & "/" & Mid(strVIAL, 242, 3) & "/" & Mid(strVIAL, 362, 3) & "/" & Mid(strVIAL, 482, 3) & "/" & Mid(strVIAL, 602, 3)
                            End If
                            If viaLen > 720 And viaLen < 800 Then
                                tmpVia = Mid(strVIAL, 2, 3) & "/" & Mid(strVIAL, 122, 3) & "/" & Mid(strVIAL, 242, 3) & "/" & Mid(strVIAL, 362, 3) & "/" & Mid(strVIAL, 482, 3) & "/" & Mid(strVIAL, 602, 3) & "/" & Mid(strVIAL, 722, 3)
                            End If
                            If viaLen > 800 And viaLen < 920 Then
                                tmpVia = Mid(strVIAL, 2, 3) & "/" & Mid(strVIAL, 122, 3) & "/" & Mid(strVIAL, 242, 3) & "/" & Mid(strVIAL, 362, 3) & "/" & Mid(strVIAL, 482, 3) & "/" & Mid(strVIAL, 602, 3) & "/" & Mid(strVIAL, 722, 3) & "/" & Mid(strVIAL, 902, 3)
                            End If
                            If viaLen > 920 And viaLen < 1040 Then
                                tmpVia = Mid(strVIAL, 2, 3) & "/" & Mid(strVIAL, 122, 3) & "/" & Mid(strVIAL, 242, 3) & "/" & Mid(strVIAL, 362, 3) & "/" & Mid(strVIAL, 482, 3) & "/" & Mid(strVIAL, 602, 3) & "/" & Mid(strVIAL, 722, 3) & "/" & Mid(strVIAL, 902, 3) & "/" & Mid(strVIAL, 1022, 3)
                            End If
                            tmpVia = tmpVia & "/" & frmData.tabData(0).GetFieldValue(llAftLine, "DES3")
                            tabTab(1).SetFieldValues llCurrLine, "VIAnDES", tmpVia
                        Else
                            tabTab(1).SetFieldValues llCurrLine, "VIAnDES", frmData.tabData(0).GetFieldValue(llAftLine, "DES3")
                        End If
                            
                        tabTab(1).SetFieldValues llCurrLine, "NA", frmData.tabData(0).GetFieldValue(llAftLine, "TTYP")
                        'kkh on 16/07/2008 P2 Store Decision into CFITAB
                        strCFIAddi = ""
                        fieldVal = Key + "," + strDepUrno
                        'strRet2 = frmData.tabData(13).GetLinesByMultipleColumnValue("1,2", fieldVal, 0)
                        strRet2 = frmData.tabData(13).GetLinesByMultipleColumnValue("5,22", fieldVal, 0)
                        
                        If strRet2 <> "" Then
                            If InStr(strRet2, ",") > 0 Then strRet2 = Split(strRet2, ",")(0) 'igu on 27 Jul 2010
                            
                            strCFIAddi = frmData.tabData(13).GetFieldValue(strRet2, "ADDI")
                            tabTab(1).SetFieldValues llCurrLine, "Hold", strCFIAddi
                            tabTab(1).SetFieldValues llCurrLine, "CFI_URNO", frmData.tabData(13).GetFieldValue(strRet2, "URNO")
                        End If
                        If frmData.tabData(0).GetFieldValue(llAftLine, "OFBL") = "" Then
                            strSRLText = GetTransferTime(strArrUrno, strDepUrno)
                        Else
                            strSRLText = ""
                        End If
                        tabTab(1).SetFieldValues llCurrLine, "TRANSFER", strSRLText
                        
                        If strSRLText = "Waiting" Then
                            'tabTab(1).SetFieldValues llCurrLine, "Hold", "H"
                            MarkLineAsHold llCurrLine, strDepUrno
                        End If
                        'KKH PRF conflict colour appear occasionally
                        CheckConnectionTimes strArrUrno, strDepUrno, llCurrLine, llAftLine, , False
                    End If
                Else 'llAftLine = -1
                    'kkh on 28/11/2008 Display KRI as higher priority then PTM message for all flights
                    tabTab(1).SetFieldValues llCurrLine, "LOA_RURN", strLoaUrno
                    ' Flight was not found ==> set linecolor to yellow
                    tabTab(1).SetLineColor llCurrLine, vbBlack, colYellow
                    tabTab(1).SetFieldValues llCurrLine, "FLNO", strFlno
                End If '//EndIf llAftLine <> -1
            End If '//EndIf strRawFlno <> strPriorFlno
            
            'strSTYP = frmData.tabData(3).GetFieldValue(llLine, "STYP") 'igu on 23/08/2011: ALTEA
            strSTYP = frmData.tabSubLOA.GetFieldValue(llLine, "STYP") 'igu on 23/08/2011: ALTEA
            'strVALU = frmData.tabData(3).GetFieldValue(llLine, "VALU") 'igu on 23/08/2011: ALTEA
            strVALU = frmData.tabSubLOA.GetFieldValue(llLine, "VALU") 'igu on 23/08/2011: ALTEA
            'strDSSN = frmData.tabData(3).GetFieldValue(llLine, "DSSN") 'igu on 23/08/2011: ALTEA
            strDSSN = frmData.tabSubLOA.GetFieldValue(llLine, "DSSN") 'igu on 23/08/2011: ALTEA
            'strTYPE = frmData.tabData(3).GetFieldValue(llLine, "TYPE") 'igu on 23/08/2011: ALTEA
            strTYPE = frmData.tabSubLOA.GetFieldValue(llLine, "TYPE") 'igu on 23/08/2011: ALTEA
            'kkh 12/06/2008 APC3
            'strLoaApc3 = frmData.tabData(3).GetFieldValue(llLine, "APC3") 'igu on 23/08/2011: ALTEA
            strLoaApc3 = frmData.tabSubLOA.GetFieldValue(llLine, "APC3") 'igu on 23/08/2011: ALTEA
            'If strDSSN = "PTM" Or strDSSN = "KRI" Then 'igu on 23/08/2011: ALTEA
            If strDSSN = "PTM" Or strDSSN = "KRI" Or strDSSN = "ALT" Then 'igu on 23/08/2011: ALTEA
                Select Case (strSTYP)
                    Case "E"
                        tmpPax = Val(tabTab(1).GetFieldValue(llCurrLine, "Y-PAX"))
                        'kkh on 05/05/2008 MANTIS PRF : 0000014
                        'If (strDSSN = "KRI" And strTYPE = "PAX") Or (strDSSN = "PTM" And tmpPax = 0 And strTYPE = "PAX") Then 'igu on 23/08/2011: ALTEA
                        If ((strDSSN = "KRI" Or strDSSN = "ALT") And strTYPE = "PAX") Or (strDSSN = "PTM" And tmpPax = 0 And strTYPE = "PAX") Then 'igu on 23/08/2011: ALTEA
                            tabTab(1).SetFieldValues llCurrLine, "Y-PAX", strVALU
                            'strTmp = frmData.tabData(3).GetFieldValue(llLine, "URNO") 'igu on 23/08/2011: ALTEA
                            strTmp = frmData.tabSubLOA.GetFieldValue(llLine, "URNO") 'igu on 23/08/2011: ALTEA
                            If InStr(strLoaTabUrnos, strTmp) = 0 Then
                                strLoaTabUrnos = strLoaTabUrnos + strTmp + ";"
                            End If
                        End If
                        If strRawFlno = strPriorFlno Then
                            If strTYPE = "BAG" Then
                                strTmpEBag = strVALU
                            End If
                            If strTYPE = "BWT" Then
                                strTmpEBwt = strVALU
                            End If
                        Else
                            strTmpEBag = 0
                            strTmpEBwt = 0
                            If strTYPE = "BAG" Then
                                strTmpEBag = strVALU
                            End If
                            If strTYPE = "BWT" Then
                                strTmpEBwt = strVALU
                            End If
                        End If
                        tabTab(1).SetFieldValues llCurrLine, "Y-PC/WT", CStr(strTmpEBag) + "/" + CStr(strTmpEBwt)
                    Case "F"
                        tmpPax = Val(tabTab(1).GetFieldValue(llCurrLine, "F-PAX"))
                        'kkh on 05/05/2008 MANTIS PRF : 0000014
                        'If (strDSSN = "KRI" And strTYPE = "PAX") Or (strDSSN = "PTM" And tmpPax = 0 And strTYPE = "PAX") Then 'igu on 23/08/2011: ALTEA
                        If ((strDSSN = "KRI" Or strDSSN = "ALT") And strTYPE = "PAX") Or (strDSSN = "PTM" And tmpPax = 0 And strTYPE = "PAX") Then 'igu on 23/08/2011: ALTEA
                            tabTab(1).SetFieldValues llCurrLine, "F-PAX", strVALU
                            'strTmp = frmData.tabData(3).GetFieldValue(llLine, "URNO") 'igu on 23/08/2011: ALTEA
                            strTmp = frmData.tabSubLOA.GetFieldValue(llLine, "URNO") 'igu on 23/08/2011: ALTEA
                            If InStr(strLoaTabUrnos, strTmp) = 0 Then
                                strLoaTabUrnos = strLoaTabUrnos + strTmp + ";"
                            End If
                        End If
                        If strRawFlno = strPriorFlno Then
                            If strTYPE = "BAG" Then
                                strTmpFBag = strVALU
                            End If
                            If strTYPE = "BWT" Then
                                strTmpFBwt = strVALU
                            End If
                        Else
                            strTmpFBag = 0
                            strTmpFBwt = 0
                            If strTYPE = "BAG" Then
                                strTmpFBag = strVALU
                            End If
                            If strTYPE = "BWT" Then
                                strTmpFBwt = strVALU
                            End If
                        End If
                        tabTab(1).SetFieldValues llCurrLine, "F-PC/WT", CStr(strTmpFBag) + "/" + CStr(strTmpFBwt)
                    Case "B"
                        tmpPax = Val(tabTab(1).GetFieldValue(llCurrLine, "C-PAX"))
                        'kkh on 05/05/2008 MANTIS PRF : 0000014
                        'If (strDSSN = "KRI" And strTYPE = "PAX") Or (strDSSN = "PTM" And tmpPax = 0 And strTYPE = "PAX") Then 'igu on 23/08/2011: ALTEA
                        If ((strDSSN = "KRI" Or strDSSN = "ALT") And strTYPE = "PAX") Or (strDSSN = "PTM" And tmpPax = 0 And strTYPE = "PAX") Then 'igu on 23/08/2011: ALTEA
                            tabTab(1).SetFieldValues llCurrLine, "C-PAX", strVALU
                            'strTmp = frmData.tabData(3).GetFieldValue(llLine, "URNO") 'igu on 23/08/2011: ALTEA
                            strTmp = frmData.tabSubLOA.GetFieldValue(llLine, "URNO") 'igu on 23/08/2011: ALTEA
                            If InStr(strLoaTabUrnos, strTmp) = 0 Then
                                strLoaTabUrnos = strLoaTabUrnos + strTmp + ";"
                            End If
                        End If
                        If strRawFlno = strPriorFlno Then
                            If strTYPE = "BAG" Then
                                strTmpBBag = strVALU
                            End If
                            If strTYPE = "BWT" Then
                                strTmpBBwt = strVALU
                            End If
                        Else
                            strTmpBBag = 0
                            strTmpBBwt = 0
                            If strTYPE = "BAG" Then
                                strTmpBBag = strVALU
                            End If
                            If strTYPE = "BWT" Then
                                strTmpBBwt = strVALU
                            End If
                        End If
                        tabTab(1).SetFieldValues llCurrLine, "C-PC/WT", CStr(strTmpBBag) + "/" + CStr(strTmpBBwt)
                    Case "U"
                        tmpPax = Val(tabTab(1).GetFieldValue(llCurrLine, "U-PAX"))
                        'kkh on 05/05/2008 MANTIS PRF : 0000014
                        'If (strDSSN = "KRI" And strTYPE = "PAX") Or (strDSSN = "PTM" And tmpPax = 0 And strTYPE = "PAX") Then 'igu on 23/08/2011: ALTEA
                        If ((strDSSN = "KRI" Or strDSSN = "ALT") And strTYPE = "PAX") Or (strDSSN = "PTM" And tmpPax = 0 And strTYPE = "PAX") Then 'igu on 23/08/2011: ALTEA
                            tabTab(1).SetFieldValues llCurrLine, "U-PAX", strVALU
                            'strTmp = frmData.tabData(3).GetFieldValue(llLine, "URNO") 'igu on 23/08/2011: ALTEA
                            strTmp = frmData.tabSubLOA.GetFieldValue(llLine, "URNO") 'igu on 23/08/2011: ALTEA
                            If InStr(strLoaTabUrnos, strTmp) = 0 Then
                                strLoaTabUrnos = strLoaTabUrnos + strTmp + ";"
                            End If
                        End If
                        If strRawFlno = strPriorFlno Then
                            If strTYPE = "BAG" Then
                                strTmpUBag = strVALU
                            End If
                            If strTYPE = "BWT" Then
                                strTmpUBwt = strVALU
                            End If
                        Else
                            strTmpUBag = 0
                            strTmpUBwt = 0
                            If strTYPE = "BAG" Then
                                strTmpUBag = strVALU
                            End If
                            If strTYPE = "BWT" Then
                                strTmpUBwt = strVALU
                            End If
                        End If
                        tabTab(1).SetFieldValues llCurrLine, "U-PC/WT", CStr(strTmpUBag) + "/" + CStr(strTmpUBwt)
                End Select
            
                llE = Val(tabTab(1).GetFieldValue(llCurrLine, "Y-PAX"))
                llF = Val(tabTab(1).GetFieldValue(llCurrLine, "F-PAX"))
                llB = Val(tabTab(1).GetFieldValue(llCurrLine, "C-PAX"))
                'Customize for GOCC 25/02/2008
                llU = Val(tabTab(1).GetFieldValue(llCurrLine, "U-PAX"))
                llPaxTot = llE + llB + llF + llU
                tabTab(1).SetFieldValues llCurrLine, "T-PAX", CStr(llPaxTot)
                
                str1 = tabTab(1).GetFieldValue(llCurrLine, "Y-PC/WT")
                arrTotalBagY = Split(str1, "/")
                If UBound(arrTotalBagY) > 0 Then
                    llBagTotal = llBagTotal + CLng(arrTotalBagY(0))
                    llBwtTotal = llBwtTotal + CLng(arrTotalBagY(1))
                End If
    
                str1 = tabTab(1).GetFieldValue(llCurrLine, "F-PC/WT")
                arrTotalBagF = Split(str1, "/")
                If UBound(arrTotalBagF) > 0 Then
                    llBagTotal = llBagTotal + CLng(arrTotalBagF(0))
                    llBwtTotal = llBwtTotal + CLng(arrTotalBagF(1))
                End If
    
                str1 = tabTab(1).GetFieldValue(llCurrLine, "C-PC/WT")
                arrTotalBagC = Split(str1, "/")
                If UBound(arrTotalBagC) > 0 Then
                    llBagTotal = llBagTotal + CLng(arrTotalBagC(0))
                    llBwtTotal = llBwtTotal + CLng(arrTotalBagC(1))
                End If
    
                str1 = tabTab(1).GetFieldValue(llCurrLine, "U-PC/WT")
                arrTotalBagU = Split(str1, "/")
                If UBound(arrTotalBagU) > 0 Then
                    llBagTotal = llBagTotal + CLng(arrTotalBagU(0))
                    llBwtTotal = llBwtTotal + CLng(arrTotalBagU(1))
                End If
                
                tabTab(1).SetFieldValues llCurrLine, "T-PC/WT", CStr(llBagTotal) + "/" + CStr(llBwtTotal)
            End If '//EndIf strDSSN = "PTM" Or strDSSN = "KRI"
        
'            strTmpEBag = 0
'            strTmpFBag = 0
'            strTmpBBag = 0
'            strTmpUBag = 0
'
'            strTmpEBwt = 0
'            strTmpFBwt = 0
'            strTmpBBwt = 0
'            strTmpUBwt = 0
    
            llBagTotal = 0
            llBwtTotal = 0
        
        End If '//EndIf byPassPTM = False
    
        'llLine = frmData.tabData(3).GetNextResultLine  'igu on 23/08/2011: ALTEA
        If llLine = frmData.tabSubLOA.GetLineCount - 1 Then 'igu on 23/08/2011: ALTEA
            llTempLine = -1 'igu on 23/08/2011: ALTEA
        Else 'igu on 23/08/2011: ALTEA
            llTempLine = llLine + 1 'igu on 23/08/2011: ALTEA
        End If 'igu on 23/08/2011: ALTEA
        ReDim arr(0)
        strPriorFlno = strRawFlno 'MWOXXXstrFlno
        '********** To identify equal FLNOs
        'strRawFlno = frmData.tabData(3).GetFieldValue(llLine, "FLNO") 'igu on 23/08/2011: ALTEA
        strRawFlno = frmData.tabSubLOA.GetFieldValue(llTempLine, "FLNO") 'igu on 23/08/2011: ALTEA
        'kkh 12/06/2008 APC3
        'strLoaApc3 = frmData.tabData(3).GetFieldValue(llLine, "APC3") 'igu on 23/08/2011: ALTEA
        strLoaApc3 = frmData.tabSubLOA.GetFieldValue(llTempLine, "APC3") 'igu on 23/08/2011: ALTEA
        'kkh on 21/04/2008
        'strLoaAddi = frmData.tabData(3).GetFieldValue(llLine, "ADDI") 'igu on 23/08/2011: ALTEA
        strLoaAddi = frmData.tabSubLOA.GetFieldValue(llTempLine, "ADDI") 'igu on 23/08/2011: ALTEA
        arr = Split(strRawFlno, "/")
        If UBound(arr) = 1 Then
            strFlno = arr(0)
            strFlightDay = arr(1)
        Else
            strFlno = strRawFlno
        End If
        strFlno = StripAftFlno(strFlno, dummy1, dummy2, dummy3)
        
        'kkh on 21/04/2008
        strLoaAddi = Mid(Trim(strLoaAddi), 14, 2)
        If strLoaAddi <> "" Then
            'kkh 12/06/2008 APC3
            'strRawFlno = strFlno + "/" + strLoaAddi
            strRawFlno = strFlno + "/" + strLoaAddi + "/" + strLoaApc3
        End If
        
        If UBound(arr) = 1 Then
            If arr(1) <> "" Then
                strRawFlno = strFlno + "/" + strFlightDay
            End If
        End If
        
        '**********
        'kkh on 28/11/2008 Display KRI as higher priority then PTM message for all flights
        If strRawFlno <> strPriorFlno And Len(strLoaTabUrnos) <> 0 Then
        'MWOXXX If strFlno <> strPriorFlno Then
            strLoaTabUrnos = Left(strLoaTabUrnos, Len(strLoaTabUrnos) - 1)
            tabTab(1).SetFieldValues llCurrLine, "L_URNOS", strLoaTabUrnos
            llCurrLine = llCurrLine + 1
            tabTab(1).InsertTextLine strEmptLine, False
            strLoaTabUrnos = ""
        End If
    'Wend 'igu on 23/08/2011: ALTEA
    Next llLine 'igu on 23/08/2011: ALTEA
    
    '------------------
    'igu on 25 Jan 2010
    '------------------
    If pstrShowYellowLines = "NO" Then
        'remove yellow line
        Dim strYellowLines As String
        Dim aYellowLines As Variant
        Dim vYellowLine As Variant
        Dim intDelCount As Integer 'igu on 25 Feb 2010
        
        strYellowLines = tabTab(1).GetLinesByBackColor(vbYellow)
        If strYellowLines <> "" Then
            aYellowLines = Split(strYellowLines, ",")
            intDelCount = 0 'igu on 25 Feb 2010
            For Each vYellowLine In aYellowLines
                tabTab(1).DeleteLine CLng(vYellowLine - intDelCount) 'igu on 25 Feb 2010
                intDelCount = intDelCount + 1 'igu on 25 Feb 2010
            Next vYellowLine
        End If
    End If
    '------------------
    tabTab(1).DeleteLine tabTab(1).GetLineCount - 1
    
    'Sort accroding to STD, transfer time, ETD
    'tabTab(1).Sort "23,6,7", True, False
    tabTab(1).Sort "6,23,7", True, True
    tabTab(1).Refresh
    For i = 0 To tabTab(1).GetLineCount
        gantt(1).AddLineAt i, CStr(i), CStr(i)
        gantt(1).SetLineSeparator i, 1, vbBlack, 2, 1, 0
    Next i
    For i = 0 To tabTab(1).GetLineCount
        strDepUrno = tabTab(1).GetFieldValue(i, "AFTURNO")
        If strDepUrno <> "" Then
            dummyDate = gantt(0).GetBarStartTime(strDepUrno)
            myDepBar = gantt(0).GetBarRecord(strDepUrno)
            If isFirst = True Then
                minDate = myDepBar(0).Begin
                maxDate = myDepBar(0).End
                isFirst = False
            End If
            If minDate > myDepBar(0).Begin Then minDate = myDepBar(0).Begin
            If maxDate < myDepBar(0).End Then maxDate = myDepBar(0).End
            myDepBar(0).LineNo = llCurrDetailLine
            myDepBar(0).LineKey = CStr(i)
            myDepBar(0).leftTriangleColor = -1
            myDepBar(0).rightTriangleColor = -1
            gantt(1).AddBarToLine myDepBar(0).LineNo, myDepBar(0).Key, myDepBar(0).Begin, myDepBar(0).End, myDepBar(0).BarText, myDepBar(0).Shape, _
                               myDepBar(0).backColor, myDepBar(0).TextColor, myDepBar(0).SplitColor, myDepBar(0).LeftBackColor, myDepBar(0).RightBackColor, "", "", myDepBar(0).LeftTextColor, myDepBar(0).RightTextColor
            gantt(1).SetBarColor myDepBar(0).Key, myDepBar(0).backColor
            
            llCurrDetailLine = llCurrDetailLine + 1
            If isFirst = True Then
                minDate = myDepBar(0).Begin
                maxDate = myDepBar(0).End
                isFirst = False
            End If
            If myDepBar(0).Begin < minDate Then minDate = myDepBar(0).Begin
            If myDepBar(0).End > maxDate Then maxDate = myDepBar(0).End
        End If
    Next i
    
    If isFirst = False Then
        TFStart = minDate
        TFEnd = maxDate
    End If
    timeframe = DateDiff("h", TFStart, TFEnd) + 1
    gantt(1).TimeScaleDuration = DetailGanttTimeFrame 'timeframe
    gantt(1).TimeFrameFrom = TFStart
    gantt(1).TimeFrameTo = TFEnd
    gantt(1).ScrollTo TFStart
    gantt(1).Refresh
    If tabTab(1).GetLineCount > 0 Then
        tabTab(1).SetCurrentSelection 0
    End If
End Sub
'---------------------------------------------------------
' Builds the Timeframe of TIFA and TIFD and sets it to the
' format 00:00 means x hours and y minutes and returns it as
' string
'---------------------------------------------------------
Public Function GetTransferTime(strArrUrno As String, strDepUrno As String) As String
    Dim llLine As Long
    Dim strStoa As String
    Dim strStod As String
    Dim datTifa As Date
    Dim datTifd As Date
    Dim strRet As String
    Dim llDiff As Long
    Dim llHour As Long
    Dim llMinute As Long
    Dim strEtai As String
    Dim datArr As Date
    Dim strEtdi As String
    Dim datDep As Date
    Dim datDiff As Date
    
    'warning colour orange
'    compressedFlightBarWarnColour = False
'    compressedFlightBarCritColour = False
    strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", strArrUrno, 0)
    llLine = frmData.tabData(0).GetNextResultLine
    If llLine > -1 Then
        strStoa = frmData.tabData(0).GetFieldValue(llLine, "STOA")
        strEtai = frmData.tabData(0).GetFieldValue(llLine, "ETAI")
    End If
    strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", strDepUrno, 0)
    llLine = frmData.tabData(0).GetNextResultLine
    If llLine > -1 Then
        strStod = frmData.tabData(0).GetFieldValue(llLine, "STOD")
        strEtdi = frmData.tabData(0).GetFieldValue(llLine, "ETDI")
    End If
    If strEtai <> "" Then
        datArr = CedaFullDateToVb(strEtai)
    Else
        datArr = CedaFullDateToVb(strStoa)
    End If
    If strEtdi <> "" Then
        datDep = CedaFullDateToVb(strEtdi)
    Else
        datDep = CedaFullDateToVb(strStod)
    End If
'    If CStr(datArr) <> "" And CStr(datDep) <> "" Then
'        llDiff = DateDiff("n", datArr, datDep)
'        llHour = Val(llDiff / 60)
'
'        If (llDiff / 60) < 1 Then
'            llHour = "0"
'        Else
'            llHour = Fix(llDiff / 60)
'        End If
'
'        llMinute = Val(llDiff Mod 60)
'    End If
    If CStr(datArr) <> "" And CStr(datDep) <> "" Then
        llDiff = DateDiff("n", datArr, datDep)
        'llHour = Val(llDiff / 60)
        
        llHour = Fix(llDiff / 60)
        llMinute = Val(llDiff Mod 60)
        'llFinalDiff = llDiff / 60
    End If
    'kkh on 05/11/2008 display minus transfer timing correctly
    If llMinute < 0 Or llHour < 0 Then
        GetTransferTime = "-" + Right("000" & Replace(CStr(llHour), "-", ""), 2) + ":" + Right("000" & Replace(CStr(llMinute), "-", ""), 2)
    Else
        GetTransferTime = Right("000" & CStr(llHour), 2) + ":" + Right("000" & CStr(llMinute), 2)
    End If
    
    'GetTransferTime = Right("000" & CStr(llHour), 2) + ":" + Right("000" & CStr(llMinute), 2)
    
End Function
'---------------------------------------------------------
' Search for all flights with this FLNO and store then in the
' tabTmpFlights. Sort tabTmpFlights by STOD and the line
' in frmData.tabData(0) for URNO as long in  for the
' corresponding Departure
' OR -1 if not found
'---------------------------------------------------------
Public Function GetCorrectDepartureLine(strURNO_A As String, opStoa As Date, strFLNO_A As String) As Long
    Dim llAftLine As Long
    Dim strValues As String
    Dim i As Long
    Dim datStod As Date
    Dim datTifa As Date
    Dim strUrno As String
    Dim ret As Long
    Dim strValue As String
    
    tabTmpFlights.ResetContent
    tabTmpFlights.HeaderString = "URNO,FLNO,STOD"
    tabTmpFlights.LogicalFieldList = "URNO,FLNO,STOD"
    tabTmpFlights.HeaderLengthString = "60,60,100"
    
    ret = -1
    frmData.tabData(0).GetLinesByIndexValue "FLNO", strFLNO_A, 0
    llAftLine = frmData.tabData(0).GetNextResultLine
    'Store it on tabTmpFlights
    While llAftLine <> -1
        strValues = frmData.tabData(0).GetFieldValues(llAftLine, "URNO,FLNO,STOD")
        tabTmpFlights.InsertTextLine strValues, False
        llAftLine = frmData.tabData(0).GetNextResultLine
    Wend
    tabTmpFlights.Sort "2", True, True
    For i = 0 To tabTmpFlights.GetLineCount - 1
        strValue = tabTmpFlights.GetFieldValue(i, "STOD")
        If Trim(strValue) <> "" Then
            datStod = CedaFullDateToVb(strValue)
            If datStod > opStoa Then
                strUrno = tabTmpFlights.GetFieldValue(i, "URNO")
                i = tabTmpFlights.GetLineCount
            End If
        End If
    Next i
    If strUrno <> "" Then
        frmData.tabData(0).GetLinesByIndexValue "URNO", strUrno, 0
        ret = frmData.tabData(0).GetNextResultLine
    End If
    GetCorrectDepartureLine = ret
End Function
'---------------------------------------------------------
' Checks the connection times for the arrival and the
' departure flight and marks the TabLineNo of tabTab(1) red
'---------------------------------------------------------
Public Function CheckConnectionTimes(strArrUrno As String, strDepUrno As String, _
                                     TabLineNo As Long, TabLineNoDep As Long, _
                                     Optional setConflictColor As Boolean = True, Optional fromInitial As Boolean) As Boolean
    Dim blRet As Boolean
    Dim TabLineNoArr As Long
    Dim linNoConn As String
    Dim strRet As String
    Dim strAdid As String
    Dim strAlc As String
    Dim strAlc_Arr As String
    Dim strAlc2 As String
    Dim strAlc3 As String
    Dim strAlc2_Arr As String
    Dim strAlc3_Arr As String
    Dim datStoa As Date
    Dim datStod As Date
    Dim datTifa As Date
    Dim datTifd As Date
    Dim strFLNO_D As String
    Dim strFLNO_A As String
    Dim llLine As Long
    Dim llConnTime As Integer
    Dim myField As String
    Dim myField_Arr As String
    Dim llDiff As Long
    Dim strVal As String
    Dim datArr As Date
    Dim datDep As Date
    Dim strArrTer As String
    Dim strDepTer As String
    Dim strSQTerMCT As String
    Dim strSQMITerMCT As String
    Dim strLine2 As Integer
    Dim column As String
    Dim fieldVal As String
    
    blRet = False
    strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", strArrUrno, 0)
    TabLineNoArr = frmData.tabData(0).GetNextResultLine
    If TabLineNoArr <> -1 Then
        strAdid = frmData.tabData(0).GetFieldValue(TabLineNoArr, "ADID")
        If strAdid = "A" Then
            strVal = frmData.tabData(0).GetFieldValue(TabLineNoArr, "STOA")
            If Trim(strVal) <> "" Then
                datStoa = CedaFullDateToVb(frmData.tabData(0).GetFieldValue(TabLineNoArr, "STOA"))
            End If
            strVal = frmData.tabData(0).GetFieldValue(TabLineNoDep, "STOD")
            If Trim(strVal) <> "" Then
                datStod = CedaFullDateToVb(frmData.tabData(0).GetFieldValue(TabLineNoDep, "STOD"))
            End If
            strVal = frmData.tabData(0).GetFieldValue(TabLineNoArr, "ETAI")
            If Trim(strVal) <> "" Then
                datArr = CedaFullDateToVb(frmData.tabData(0).GetFieldValue(TabLineNoArr, "ETAI"))
            Else
                datArr = CedaFullDateToVb(frmData.tabData(0).GetFieldValue(TabLineNoArr, "STOA"))
            End If
            strVal = frmData.tabData(0).GetFieldValue(TabLineNoDep, "ETDI")
            If Trim(strVal) <> "" Then
                datDep = CedaFullDateToVb(frmData.tabData(0).GetFieldValue(TabLineNoDep, "ETDI"))
            Else
                datDep = CedaFullDateToVb(frmData.tabData(0).GetFieldValue(TabLineNoDep, "STOD"))
            End If
            strFLNO_A = frmData.tabData(0).GetFieldValue(TabLineNoArr, "FLNO")
            strFLNO_D = frmData.tabData(0).GetFieldValue(TabLineNoDep, "FLNO")
            'Cater for flight which do not have FLNO, taking call sign instead
'            If strFLNO_D = "" Then
'                strFLNO_D = frmData.tabData(0).GetFieldValue(TabLineNoDep, "CSGN")
'            End If
            strAlc = Mid(strFLNO_A, 1, 3)
            strAlc = Trim(strAlc)
            strAlc_Arr = Mid(strFLNO_D, 1, 3)
            strAlc_Arr = Trim(strAlc_Arr)
            If Len(strAlc) = 2 Then
                myField = "ALC2"
            End If
            If Len(strAlc) = 3 Then
                myField = "ALC3"
            End If
            If Len(strAlc_Arr) = 2 Then
                myField_Arr = "ALC2"
            End If
            If Len(strAlc_Arr) = 3 Then
                myField_Arr = "ALC3"
            End If
                                    
            column = "1,3"
            fieldVal = strAlc + "," + strAlc_Arr
            strRet = frmData.tabData(5).GetLinesByMultipleColumnValue(column, fieldVal, 0)
            'If strRet <> "ERROR;" Then
                'If strRet = -1 Or strRet = 0 Then                         'igu on 17 June 2009
                If strRet = "ERROR;" Or strRet = "-1" Or strRet = "0" Then 'strRet must be compared to string
                    'switch the column value to column = "3,1"
                    column = "3,1"
                    strRet = frmData.tabData(5).GetLinesByMultipleColumnValue(column, fieldVal, 0)
                End If
                'If strRet = -1 Or strRet = 0 Then                         'igu on 17 June 2009
                If strRet = "ERROR;" Or strRet = "-1" Or strRet = "0" Then 'strRet must be compared to string
                    strRet = frmData.tabData(5).GetLinesByIndexValue(myField, strAlc, 0)
                End If
            'End If
            
            llLine = frmData.tabData(5).GetNextResultLine
            If strAlc = strAlc_Arr Then
                strAlc = strAlc
            End If
            If llLine <> -1 Then
                If strAlc = strAlc_Arr Then
                    If Val(frmData.tabData(5).GetFieldValue(llLine, "PSAM")) = 0 Then
                        llConnTime = Val(frmData.tabData(5).GetFieldValue(llLine, "PCON"))
                    Else
                        llConnTime = Val(frmData.tabData(5).GetFieldValue(llLine, "PSAM"))
                    End If
                Else
                    llConnTime = Val(frmData.tabData(5).GetFieldValue(llLine, "PCON"))
                End If
            Else
                llConnTime = Val(strPaxDefault)
            End If
            
            'Solution to cater MI/SQ, SQ/SQ on different terminal
            strSQTerMCT = GetIniEntry("", "HUBMANAGER", "GLOBAL", "SQTERMINAL_MCT", "")
            strSQMITerMCT = GetIniEntry("", "HUBMANAGER", "GLOBAL", "SQMITERMINAL_MCT", "")
            strArrTer = frmData.tabData(0).GetFieldValue(TabLineNoArr, "TGA1")
            strDepTer = frmData.tabData(0).GetFieldValue(TabLineNoDep, "TGD1")
            
            If strSQTerMCT = "" Then strSQTerMCT = "0"
            If strSQMITerMCT = "" Then strSQMITerMCT = "0"
            llDiff = DateDiff("n", datArr, datDep)
            
            If strAlc = "SQ" And strAlc_Arr = "SQ" Then
                'KKH PRF conflict colour appear occasionally
                blRet = setWarningBground(strArrTer, strDepTer, llDiff, llConnTime, strSQTerMCT, TabLineNo, setConflictColor, fromInitial)
            Else
                If (strAlc = "SQ" And strAlc_Arr = "MI") Or (strAlc = "MI" And strAlc_Arr = "SQ") Then
                    'KKH PRF conflict colour appear occasionally
                    blRet = setWarningBgroundSQMI(strArrTer, strDepTer, llDiff, llConnTime, strSQMITerMCT, TabLineNo, setConflictColor, fromInitial)
                Else
                    If llDiff < llConnTime Then
                        If fromInitial = False Then
                            If TabLineNo >= 0 And setConflictColor = True Then
                                tabTab(1).SetLineColor TabLineNo, vbBlack, vbRed
                            End If
                        End If
                        blRet = True
                    End If
                End If
            End If
        End If
    End If
    CheckConnectionTimes = blRet
End Function

Public Function setWarningBground(strArrTer As String, strDepTer As String, strDIFF As Long, strConn As Integer, strConfigVal As String, strTabline As Long, Optional configColour As Boolean = True, Optional fromInitial As Boolean) As Boolean
    Dim blRetW As Boolean
    Dim strTightConn As String
    blRetW = False
    Dim temp As Integer
    strTightConn = GetIniEntry("", "HUBMANAGER", "GLOBAL", "TIGHTCONNECTION", "")
    If strTightConn = "" Then strTightConn = "0"
    If strArrTer <> strDepTer Then
        'kkh set conflict colour as warning if llDiff(transfer time) between llConnTime(MCT) + strConfigVal(XX_TERMINAL_MCT) and (llConnTime(MCT) + strConfigVal(XX_TERMINAL_MCT) + strTightConn)
        If strDIFF < strConn Then
            If fromInitial = False Then
                If strTabline >= 0 And configColour = True Then
                    tabTab(1).SetLineColor strTabline, vbBlack, vbRed
                End If
            End If
            blRetW = True
        Else
            If strDIFF < (strConn + CInt(strConfigVal)) Then
                If fromInitial = False Then
                    If strTabline >= 0 And configColour = True Then
                        'set to bright ORANGE colour on 26/12/2007
                        tabTab(1).SetLineColor strTabline, vbBlack, colBrightOrange  'bright orange
                        'warning colour orange
                        'compressedFlightBarCritColour = True
                    End If
                End If
                blRetW = True
            Else
                If strDIFF < (strConn + CInt(strConfigVal) + CInt(strTightConn)) Then
                    If fromInitial = False Then
                        If strTabline >= 0 And configColour = True Then
                            'set to yellow colour on 26/12/2007
                            tabTab(1).SetLineColor strTabline, vbBlack, colLightYellow  'light yellow
                            blRetW = True
                            'warning colour orange
                            'compressedFlightBarWarnColour = True
                        End If
                    End If
                End If
            End If
        End If
    Else
        If strDIFF < strConn Then
            If fromInitial = False Then
                If strTabline >= 0 And configColour = True Then
                    tabTab(1).SetLineColor strTabline, vbBlack, vbRed
                    'warning colour orange
                    'compressedFlightBarCritColour = True
                End If
            End If
            blRetW = True
        End If
    End If
    setWarningBground = blRetW
    
End Function
Public Function setWarningBgroundSQMI(strArrTer As String, strDepTer As String, strDIFF As Long, strConn As Integer, strConfigVal As String, strTabline As Long, Optional configColour As Boolean = True, Optional fromInitial As Boolean) As Boolean
    Dim blRetW As Boolean
    Dim strTightConn As String
    blRetW = False
    Dim temp As Integer
    strTightConn = GetIniEntry("", "HUBMANAGER", "GLOBAL", "TIGHTCONNECTION", "")
    If strTightConn = "" Then strTightConn = "0"
    If strArrTer <> strDepTer Then
        'kkh set orange colour as warning if llDiff(transfer time) between llConnTime(MCT) + strConfigVal(XX_TERMINAL_MCT) and (llConnTime(MCT) + strConfigVal(XX_TERMINAL_MCT) + strTightConn)
        If strDIFF < (strConn + CInt(strConfigVal)) Then
            If fromInitial = False Then
                If strTabline >= 0 And configColour = True Then
                    'set to bright ORANGE colour on 26/12/2007
                    tabTab(1).SetLineColor strTabline, vbBlack, colOrange
                    'warning colour orange
                    'compressedFlightBarCritColour = True
                End If
            End If
            blRetW = True
        Else
            If strDIFF < (strConn + CInt(strConfigVal) + CInt(strTightConn)) Then
                If fromInitial = False Then
                If strTabline >= 0 And configColour = True Then
                    'set to yellow colour on 26/12/2007
                    tabTab(1).SetLineColor strTabline, vbBlack, colLightYellow 'light yellow
                    blRetW = True
                    'warning colour orange
                    'compressedFlightBarWarnColour = True
                End If
            End If
        End If
        End If
    Else
        If strDIFF < strConn Then
            If fromInitial = False Then
                If strTabline >= 0 And configColour = True Then
                    tabTab(1).SetLineColor strTabline, vbBlack, colOrange
                    'warning colour orange
                    'compressedFlightBarCritColour = True
                End If
            End If
            blRetW = True
        End If
    End If
    setWarningBgroundSQMI = blRetW
    
End Function
Public Function HasArrivalConflict(strUrno As String, opStoa As Date, Optional setConflictColor As Boolean = True) As Boolean
    Dim strVal As String
    Dim strEmptLine As String
    Dim llLine As Long
    Dim llAftLine As Long
    Dim strRawFlno As String
    Dim strFlno As String
    Dim strFlightDay As String
    Dim strLineValues As String
    Dim llCurrLine As Long
    Dim strAdid As String
    Dim arr() As String
    Dim datStod As Date
    Dim strVALU As String
    Dim strArrUrno As String
    Dim strDepUrno As String
    Dim dummy1 As String, dummy2 As String, dummy3 As String
    Dim hasConflict As Boolean
    Dim strPriorFlno As String
    Dim tmpFLNO As String
    
    tmpFLNO = ""
    frmData.tabData(3).GetLinesByIndexValue "FLNU", strUrno, 0
    llLine = frmData.tabData(3).GetNextResultLine
    'KKH PRF conflict colour appear occasionally
    strPriorFlno = ""
    llCurrLine = 0
    
    strArrUrno = strUrno
    hasConflict = False
    While (llLine <> -1 And hasConflict = False)
        If tmpFLNO = frmData.tabData(3).GetFieldValue(llLine, "FLNO") Then
            'do nothing
        Else
            tmpFLNO = frmData.tabData(3).GetFieldValue(llLine, "FLNO")
            ' Insert an empty line
            strRawFlno = frmData.tabData(3).GetFieldValue(llLine, "FLNO")
            arr = Split(strRawFlno, "/")
            If UBound(arr) = 1 Then
                strFlno = arr(0)
                strFlightDay = arr(1)
            Else
                strFlno = strRawFlno
            End If
            strFlno = StripAftFlno(strFlno, dummy1, dummy2, dummy3)
            'KKH PRF conflict colour appear occasionally
            If strRawFlno <> strPriorFlno Then
                llAftLine = GetCorrectDepartureByFlightDay(opStoa, strFlightDay, strFlno)
            End If
        
            If llAftLine <> -1 Then
                strAdid = frmData.tabData(0).GetFieldValue(llAftLine, "ADID")
                If strAdid = "D" Then 'only if departure
                    strDepUrno = frmData.tabData(0).GetFieldValue(llAftLine, "URNO")
                    'KKH PRF conflict colour appear occasionally
                    hasConflict = CheckConnectionTimes(strArrUrno, strDepUrno, llCurrLine, llAftLine, setConflictColor, True)
                    If hasConflict = True Then
                        hasConflict = True
                    End If
                End If
            End If
        End If
        llLine = frmData.tabData(3).GetNextResultLine
        ReDim arr(0)
        'KKH PRF conflict colour appear occasionally
        strPriorFlno = strRawFlno
        strRawFlno = frmData.tabData(3).GetFieldValue(llLine, "FLNO")
        arr = Split(strRawFlno, "/")
        If UBound(arr) = 1 Then
            strFlno = arr(0)
        Else
            strFlno = strRawFlno
        End If
        strFlno = StripAftFlno(strFlno, dummy1, dummy2, dummy3)
        If strRawFlno <> strPriorFlno Then
        llCurrLine = llCurrLine + 1
        End If
        'llCurrLine = llCurrLine + 1
    Wend
    HasArrivalConflict = hasConflict
End Function
'Public Function HasArrivalConflict(strUrno As String, opStoa As Date, Optional setConflictColor As Boolean = True) As Boolean
'    Dim strVal As String
'    Dim strEmptLine As String
'    Dim llLine As Long
'    Dim llAftLine As Long
'    Dim strRawFlno As String
'    Dim strFlno As String
'    Dim strFlightDay As String
'    Dim strLineValues As String
'    Dim llCurrLine As Long
'    Dim strAdid As String
'    Dim arr() As String
'    Dim datStod As Date
'    Dim strVALU As String
'    Dim strArrUrno As String
'    Dim strDepUrno As String
'    Dim dummy1 As String, dummy2 As String, dummy3 As String
'    Dim hasConflict As Boolean
'
'    Dim tmpFLNO As String
'    tmpFLNO = ""
'    frmData.tabData(3).GetLinesByIndexValue "FLNU", strUrno, 0
'    llLine = frmData.tabData(3).GetNextResultLine
'    llCurrLine = 0
'
'    strArrUrno = strUrno
'    hasConflict = False
'    While (llLine <> -1 And hasConflict = False)
'        tmpFLNO = frmData.tabData(3).GetFieldValue(llLine, "FLNO")
'        ' Insert an empty line
'        strRawFlno = frmData.tabData(3).GetFieldValue(llLine, "FLNO")
'        arr = Split(strRawFlno, "/")
'        If UBound(arr) = 1 Then
'            strFlno = arr(0)
'            strFlightDay = arr(1)
'        Else
'            strFlno = strRawFlno
'        End If
'        strFlno = StripAftFlno(strFlno, dummy1, dummy2, dummy3)
'        llAftLine = GetCorrectDepartureByFlightDay(opStoa, strFlightDay, strFlno)
'        If llAftLine <> -1 Then
'            strAdid = frmData.tabData(0).GetFieldValue(llAftLine, "ADID")
'            If strAdid = "D" Then 'only if departure
'                strDepUrno = frmData.tabData(0).GetFieldValue(llAftLine, "URNO")
'                hasConflict = CheckConnectionTimes(strArrUrno, strDepUrno, llCurrLine, llAftLine, setConflictColor)
'                If hasConflict = True Then
'                    hasConflict = True
'                End If
'            End If
'        End If
'        llLine = frmData.tabData(3).GetNextResultLine
'        ReDim arr(0)
'        llCurrLine = llCurrLine + 1
'    Wend
'    HasArrivalConflict = hasConflict
'End Function

'---------------------------------------------------------
' Called from mailModule to ensure that all data are loaded
' initializes the tabs and fills the main gantt
'---------------------------------------------------------
Public Sub MyInit(Optional bpRefreshGantts As Boolean = True, Optional bpUpdateCompressedGantt As Boolean = True)
    Dim strMsg As String
    Dim datFrom As Date
    Dim datTo As Date
    
    datFrom = CedaFullDateToVb(strTimeFrameFrom)
    datTo = CedaFullDateToVb(strTimeFrameTo)
    
    If TimesInUTC = False Then
        UTCOffset = frmData.GetUTCOffset
        datFrom = DateAdd("h", UTCOffset, datFrom)
        datTo = DateAdd("h", UTCOffset, datTo)
    End If
    
    Me.Caption = "HUB-Manager: Loaded [" & datFrom & "] - [" & datTo & "]" & "    Server: " & strServer
    InitConnTabs
    InitConfigData
    frmStartup.txtCurrentStatus = "Init the gantts ..."
    frmStartup.txtCurrentStatus.Refresh
    MakeMainGanttLines
    MakeMainGanttBars bpRefreshGantts, bpUpdateCompressedGantt
    bgHourlyIsActive = False
    bgMailCargoIsActive = False
    cbToolButton_Click (1) 'Move to "Now"
    gantt(0).Refresh
    frmStartup.txtCurrentStatus = "Check for conflicts ..."
    frmStartup.txtCurrentStatus.Refresh
    CheckAllArrivalsForConflicts
    StoreDepartureUrnoForLoaTab frmData.tabData(3)

    If bmIsInitialized = False Then
'        TrafficLight.SetDispatch frmData.aUfis
'        TrafficLight.SetDispBc MyBC
'        TrafficLight.Init
    End If
    bmIsInitialized = True
    frmStartup.Top = 99999
    If frmData.tabData(5).GetLineCount = 0 Then
        strMsg = "No connection times could be found." & vbLf
        strMsg = strMsg + "Please insert the values!" & vbLf & vbLf
        strMsg = strMsg + "Now using the default values! " & vbLf
        strMsg = strMsg + "================================" & vbLf
        strMsg = strMsg + " PAX : " + strPaxDefault + " minutes" + vbLf
        strMsg = strMsg + " MAIL: " + strMailDefault + " minutes" + vbLf
        strMsg = strMsg + "Cargo: " + strMailDefault + " minutes" + vbLf
        MsgBox strMsg, vbExclamation + vbOKOnly, "Warning!"
    End If
    'frmStartup..Hide
    frmStartup.Visible = False
    
    InitFlnuBcTab
End Sub

Sub InitFlnuBcTab()
    tabFlnuBcs.ResetContent
    tabFlnuBcs.HeaderString = "FLNU"
    tabFlnuBcs.HeaderLengthString = "200"
    tabFlnuBcs.LogicalFieldList = "FLNU"
    tabFlnuBcs.SetUniqueFields "FLNU"
End Sub

Public Sub InitConfigData()
    Dim strField As String
    Dim strValue As String
    Dim i As Long
    Dim cnt As Long
    
    cnt = frmData.tabCfg.GetLineCount - 1
    For i = 0 To cnt
        strField = frmData.tabCfg.GetFieldValue(i, "FIELD")
        strValue = frmData.tabCfg.GetFieldValue(i, "VALUE")
        Select Case (strField)
            Case "MinArrivalBarLen"
                 MinArrivalBarLen = Val(strValue)
            Case "MinDepartureLen"
                MinDepartureBarLen = Val(strValue)
            Case "ArrivalBarColor"
                colorArrivalBar = Val(strValue)
            Case "DepartureBarColor"
                colorDepartureBar = Val(strValue)
            Case "MarkColor"
                colorCurrentBar = Val(strValue)
            Case "DetailGanttTimeFrame"
                DetailGanttTimeFrame = Val(strValue)
            Case "ShowHints"
                If strValue = "Y" Then
                    ShowHints = True
                Else
                    ShowHints = False
                    lblTip(0).Visible = False
                End If
            Case "TimesInUTC"
                If strValue = "Y" Then
                    TimesInUTC = True
                Else
                    TimesInUTC = False
                End If
        End Select
    Next i
End Sub
Public Sub CheckAllArrivalsForConflicts()
    Dim cnt As Long
    Dim i As Long
    Dim strAdid As String
    Dim strUrno As String
    Dim datStoa As Date
    Dim strStoa As String
    Dim strVal As String
    
    cnt = frmData.tabData(0).GetLineCount
    For i = 0 To cnt
        strAdid = frmData.tabData(0).GetFieldValue(i, "ADID")
        strUrno = frmData.tabData(0).GetFieldValue(i, "URNO")
        If strAdid = "A" Then
            strVal = frmData.tabData(0).GetFieldValue(i, "STOA")
            If Trim(strVal) <> "" Then
                datStoa = CedaFullDateToVb(strVal)
                If HasArrivalConflict(strUrno, datStoa, False) = True Then
                    If strUrno = omCurrentSelectedUrno Then
                        AdjustCurrentBarColor strUrno, vbRed
                    End If
                    gantt(0).SetBarColor strUrno, vbRed
                    If bgfrmCompressedFlightsOpen = True Then
                        frmCompressedFligths.gantt.SetBarColor strUrno, vbRed
                    End If
                Else
                    gantt(0).SetBarColor strUrno, colorArrivalBar
                    If bgfrmCompressedFlightsOpen = True Then
                        If frmCompressedFligths.cbToolButton(2).Value = 0 Then
                            frmCompressedFligths.gantt.SetBarColor strUrno, colorArrivalBar
                        Else
                            frmCompressedFligths.gantt.DeleteBarByKey strUrno
                        End If
                    End If
                    If strUrno = omCurrentSelectedUrno Then
                        AdjustCurrentBarColor strUrno, colorArrivalBar
                    End If
                End If
            End If
        End If
    Next i
End Sub

Public Sub StoreDepartureUrnoForLoaTab(myLoadTab As TABLib.Tab)
    Dim cnt As Long
    Dim i As Long
    Dim strAdid As String
    Dim arr() As String
    Dim strUrno As String
    Dim datStoa As Date
    Dim strStoa As String
    Dim strVal As String
    Dim strRawFlno As String
    Dim strFlno As String
    Dim llLine As Long
    Dim llDepLine As Long
    Dim strUrnoA As String
    Dim strFlightDay As String
    Dim strRet As String
    Dim dummy1 As String, dummy2 As String, dummy3 As String
    
    frmData.tabReread.ResetContent
    frmData.tabReread.HeaderString = frmData.tabData(0).HeaderString
    frmData.tabReread.LogicalFieldList = frmData.tabData(0).LogicalFieldList
    frmData.tabReread.HeaderLengthString = frmData.tabData(0).HeaderLengthString
    frmData.tabReread.EnableHeaderSizing True
    frmData.tabReread.InsertBuffer frmData.tabData(0).GetBuffer(0, frmData.tabData(0).GetLineCount - 1, vbLf), vbLf
    frmData.tabReread.IndexCreate "URNO", 0
    frmData.tabReread.IndexCreate "RKEY", 1
    frmData.tabReread.IndexCreate "FLNO", 4
    frmData.tabReread.IndexCreate "DES3", 20
    frmData.tabReread.SetInternalLineBuffer True
    
    cnt = myLoadTab.GetLineCount
    For i = 0 To cnt
        strFlightDay = ""
        strRawFlno = myLoadTab.GetFieldValue(i, "FLNO")
        arr = Split(strRawFlno, "/")
        If UBound(arr) = 1 Then
            strFlno = arr(0)
            strFlightDay = arr(1)
        Else
            strFlno = strRawFlno
        End If
        If strFlno = "SQ116" Then
            strFlno = strFlno
        End If
        strFlno = StripAftFlno(strFlno, dummy1, dummy2, dummy3)
        If strFlno = "OA 181" Then
            strFlno = strFlno
        End If
        strUrnoA = myLoadTab.GetFieldValue(i, "FLNU")
        strRet = frmData.tabReread.GetLinesByIndexValue("URNO", strUrnoA, 0)
        llLine = frmData.tabReread.GetNextResultLine
        If llLine > 0 Then
            strStoa = frmData.tabReread.GetFieldValue(llLine, "STOA")
            If Trim(strStoa) <> "" Then
                datStoa = CedaFullDateToVb(strStoa)
                llDepLine = GetCorrectDepartureByFlightDay(datStoa, strFlightDay, strFlno)
                If llDepLine > -1 Then
                    myLoadTab.SetFieldValues i, "RTAB", frmData.tabData(0).GetFieldValue(llDepLine, "URNO")
                End If
            End If
        End If
        ReDim arr(0)
    Next i
    myLoadTab.Refresh
    myLoadTab.Sort "1,2", True, True 'Rebuilds the indexes
    frmData.tabReread.SetInternalLineBuffer False
    frmData.tabReread.IndexDestroy "URNO"
    frmData.tabReread.IndexDestroy "RKEY"
    frmData.tabReread.IndexDestroy "FLNO"
    frmData.tabReread.IndexDestroy "DES3"
    frmData.tabReread.ResetContent
End Sub

Public Sub SpoolBC(pOn As Boolean)
    If pOn = True Then
        MyBC.SetSpoolOff
    Else
        MyBC.SetSpoolOn
    End If
End Sub
'kkh on 09/04/2008 GOCC PRF 0000013: Connecting flight data does not match PTM
Public Function GetCorrectDepartureByFlightDay(datStoa As Date, opDay As String, strFlno As String, Optional strAddi As String) As Long
    Dim llAftLine As Long
    Dim strValues As String
    Dim i As Long
    Dim datStod As Date
    Dim datTifa As Date
    Dim strUrno As String
    Dim ret As Long
    Dim strValue As String
    Dim strAdid As String
    Dim dtmFlightDateInUTC As String 'igu on 2 June 2009; store STOD in UTC
    Dim strDayOfFlight As String 'igu on 2 June 2009; store the day of STOD in UTC
    
    tabTmpFlights.ResetContent
    tabTmpFlights.HeaderString = "URNO,FLNO,STOD,ADID"
    tabTmpFlights.LogicalFieldList = "URNO,FLNO,STOD,ADID"
    tabTmpFlights.HeaderLengthString = "60,60,100,40"
    
    ret = -1
    frmData.tabData(0).GetLinesByIndexValue "FLNO", strFlno, 0
    llAftLine = frmData.tabData(0).GetNextResultLine
    'Store it on tabTmpFlights
    While llAftLine <> -1
        strValues = frmData.tabData(0).GetFieldValues(llAftLine, "URNO,FLNO,STOD,ADID")
        tabTmpFlights.InsertTextLine strValues, False
        llAftLine = frmData.tabData(0).GetNextResultLine
    Wend
    tabTmpFlights.Sort "2", True, True
    For i = 0 To tabTmpFlights.GetLineCount - 1
        strValue = tabTmpFlights.GetFieldValue(i, "STOD")
        strValue = Trim(strValue)
        strAdid = tabTmpFlights.GetFieldValue(i, "ADID")
        If opDay <> "" And strAdid = "D" Then
            If strValue <> "" Then
                strValue = Mid(strValue, 7, 2)
                If strValue = opDay Then
                    strUrno = tabTmpFlights.GetFieldValue(i, "URNO")
                    i = tabTmpFlights.GetLineCount
                End If
            End If
        Else
            If strValue <> "" Then
                datStod = CedaFullDateToVb(strValue)
                'kkh on 09/04/2008 GOCC PRF 0000013: Connecting flight data does not match PTM
                If strAddi <> "" Then
                    '--------------
                    'Date: Juni 2nd 2009
                    'Desc: change STOD from local time to UTC
                    'By  : igu
                    '--------------
                    dtmFlightDateInUTC = CedaFullDateToVb(strValue)
                    dtmFlightDateInUTC = DateAdd("h", -UTCOffset, dtmFlightDateInUTC)
                    strDayOfFlight = Format(dtmFlightDateInUTC, "YYYYMMDDhhmmss")
                    strDayOfFlight = Mid(strDayOfFlight, 7, 2)
                    '--------------
                    If strDayOfFlight = strAddi Then
                        If datStod > datStoa And strAdid = "D" Then
                            strUrno = tabTmpFlights.GetFieldValue(i, "URNO")
                            i = tabTmpFlights.GetLineCount
                        End If
                    End If
                Else
                    If datStod > datStoa And strAdid = "D" Then
                          strUrno = tabTmpFlights.GetFieldValue(i, "URNO")
                          i = tabTmpFlights.GetLineCount
                    End If
                End If
            End If
        End If
    Next i
    If strUrno <> "" Then
        frmData.tabData(0).GetLinesByIndexValue "URNO", strUrno, 0
        ret = frmData.tabData(0).GetNextResultLine
    End If
    GetCorrectDepartureByFlightDay = ret
End Function

Private Sub timerRepaint_Timer()
    picToolBar.Width = Me.ScaleWidth
    DrawBackGround picToolBar, 7, True, False
    picToolBar.Refresh
    timerRepaint.Enabled = False
End Sub

Public Function IsFlightOnHold(strUrno As String) As Boolean
    Dim llLine As Long
    Dim blRet As Boolean
    
    blRet = True
    frmData.tabHold.GetLinesByIndexValue "URNO", strUrno, 0
    llLine = frmData.tabHold.GetNextResultLine
    If llLine = -1 Then
        blRet = False
    End If
    IsFlightOnHold = blRet
End Function

Public Sub MarkLineAsHold(tabLine As Long, strUrno As String)
    Dim llLine As Long
    Dim strValues As String
    Dim i As Long
    Dim cnt As Long
    
    frmData.tabHold.GetLinesByIndexValue "URNO", strUrno, 0
    llLine = frmData.tabHold.GetNextResultLine
    If llLine = -1 Then
        strValues = strUrno + ",H"
        frmData.tabHold.InsertTextLine strValues, False
        frmData.tabHold.Sort "0", True, True
    End If
    cnt = ItemCount(tabTab(1).LogicalFieldList, ",")
    For i = 0 To cnt - 1
        tabTab(1).ResetCellDecoration tabLine, i
        tabTab(1).SetDecorationObject tabLine, i, "HOLD"
    Next i
    'tabTab(1).SetFieldValues tabLine, "Hold", "H"
    tabTab(1).Refresh
End Sub

Public Sub MarkLineAsUserEntry(tabLine As Long)
    Dim cnt As Long
    Dim i As Long
    
    cnt = ItemCount(tabTab(1).LogicalFieldList, ",")
    For i = 0 To cnt - 1
        tabTab(1).ResetCellDecoration tabLine, i
        tabTab(1).SetDecorationObject tabLine, i, "USERENTRY"
    Next i
End Sub
Public Sub UnmarkLineAsHold(tabLine As Long, strUrno As String)
    Dim llLine As Long
    Dim strValues As String
    Dim i As Long
    Dim cnt As Long
    
    frmData.tabHold.GetLinesByIndexValue "URNO", strUrno, 0
    llLine = frmData.tabHold.GetNextResultLine
    If llLine > -1 Then
        frmData.tabHold.DeleteLine llLine
        frmData.tabHold.Sort "0", True, True
    End If
    cnt = ItemCount(tabTab(1).LogicalFieldList, ",")
    For i = 0 To cnt - 1
        tabTab(1).ResetCellDecoration tabLine, i
    Next i
    tabTab(1).Refresh
    tabTab(1).SetFieldValues tabLine, "Hold", " "
End Sub

Private Sub timerUpdateTimefields_Timer()
    Dim tmLocal As Date
    Dim tmUTC As Date
    Dim strTime As String
    
    tmLocal = Now
    If TimesInUTC = True Then
        tmUTC = DateAdd("h", -UTCOffset, tmLocal)
    Else
        tmUTC = tmLocal
    End If
'    If TimesInUTC = True Then
        strTime = Format(tmUTC, "DD.MM.YY-")
        strTime = strTime + Format(tmUTC, "hh:mm") + "z"
        lblTime(0).Caption = strTime
        strTime = Format(tmLocal, "DD.MM.YY-")
        strTime = strTime + Format(tmLocal, "hh:mm")
        lblTime(1).Caption = strTime
'    Else
'        d = DateAdd("h", -1, d)
'    End If
End Sub

Public Sub InsertConnection(strUrno As String)
    Dim llLine As Long
    Dim strValues As String
    Dim strLoaUrnos As String
    Dim strFirstUrno As String
    Dim strBusinesUrno As String
    Dim strEcoUrno As String
    Dim strAdid As String
    Dim strFlno As String
    Dim datTifd As Date
    Dim strTIFD As String
    Dim strDES3 As String
    Dim strTifa As String 'For current selected flight
    Dim datTifa As Date
    Dim i As Long
    Dim arr() As String
    
'    tabTab(1).HeaderString = "AFTURNO,LOA_RURN,ACT,FLNO,Date,STD,Est.,ATD,Dest,T-Pax,F-Pax,C-Pax,Y-Pax,Na,Hold,"
'    tabTab(1).LogicalFieldList = "AFTURNO,LOA_RURN,ACT,FLNO,DATE,STD,EST,ATD,DES,T-PAX,F-PAX,B-PAX,E-PAX,NA,Hold,DUMMY"
    
    frmData.tabData(0).GetLinesByIndexValue "URNO", strUrno, 0
    llLine = frmData.tabData(0).GetNextResultLine
    If llLine > -1 Then
        strAdid = frmData.tabData(0).GetFieldValues(llLine, "ADID")
        If strAdid = "D" Then ' Do it only if flight is departure
            'Store the three urnos for firstb business and economy for later re-identificatioin
            strFirstUrno = frmData.GetNextUrno
            strBusinesUrno = frmData.GetNextUrno
            strEcoUrno = frmData.GetNextUrno
            strLoaUrnos = strFirstUrno + ";" + strBusinesUrno + ";" + strEcoUrno
            strValues = frmData.tabData(0).GetFieldValues(llLine, "URNO") + ",0" '0 for no related telex and identifier, that this is
                                                                                 'a user entry
            strValues = strValues + frmData.tabData(0).GetFieldValues(llLine, "URNO,ACT3,FLNO,TGD1,TIFD,STOD,ETDI,TIFD,DES3") + ",0,0,0,0,"
            strValues = strValues + frmData.tabData(0).GetFieldValue(llLine, "TTYP")
            tabTab(1).InsertTextLine strValues, True
            tabTab(1).SetFieldValues tabTab(1).GetLineCount - 1, "DUMMY,L_URNOS", "," + strLoaUrnos
            
            'Now insert it into local memory
            strFlno = frmData.tabData(0).GetFieldValue(llLine, "FLNO")
            arr = Split(strFlno, " ")
            If UBound(arr) = 1 Then
                strFlno = arr(0) + arr(1)
            Else
                strFlno = arr(0)
            End If
            
            strTIFD = frmData.tabData(0).GetFieldValue(llLine, "TIFD")
            strTIFD = Trim(strTIFD)
            strDES3 = frmData.tabData(0).GetFieldValue(llLine, "DES3")
            If strTIFD <> "" Then
                datTifd = CedaFullDateToVb(strTIFD)
            End If
            'Economic class record
            strValues = strFirstUrno + "," + omCurrentSelectedUrno + "," + strFlno + "/" + Format(datTifd, "DD")
            'kkh 02/02/2009 HOPO require to insert
            strValues = strValues + ",PTM,0,PAX,E,TD, ," + strDES3 + ",0, , , ," + strHopo
            frmData.tabData(3).InsertTextLine strValues, False
            frmData.SetServerParameters
            If frmData.aUfis.CallServer("IRT", "LOATAB", frmData.tabData(3).LogicalFieldList, strValues, "", "239") <> 0 Then
                MsgBox "Insert Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
            Else
                frmData.SetServerParameters
                frmData.aUfis.CallServer "SBC", "LOA:IRT", frmData.tabData(3).LogicalFieldList, strValues, omCurrentSelectedUrno, "230"
            End If
            'Business class record
            strValues = strBusinesUrno + "," + omCurrentSelectedUrno + "," + strFlno + "/" + Format(datTifd, "DD")
            strValues = strValues + ",PTM,0,PAX,B,TD, ," + strDES3 + ",0, , "
            frmData.tabData(3).InsertTextLine strValues, False
            frmData.SetServerParameters
            If frmData.aUfis.CallServer("IRT", "LOATAB", frmData.tabData(3).LogicalFieldList, strValues, "", "239") <> 0 Then
                MsgBox "Insert Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
            Else
                frmData.SetServerParameters
                frmData.aUfis.CallServer "SBC", "LOA:IRT", frmData.tabData(3).LogicalFieldList, strValues, omCurrentSelectedUrno, "230"
            End If
            'First class record
            strValues = strEcoUrno + "," + omCurrentSelectedUrno + "," + strFlno + "/" + Format(datTifd, "DD")
            strValues = strValues + ",PTM,0,PAX,F,TD, ," + strDES3 + ",0, , "
            frmData.tabData(3).InsertTextLine strValues, False
            frmData.SetServerParameters
            If frmData.aUfis.CallServer("IRT", "LOATAB", frmData.tabData(3).LogicalFieldList, strValues, "", "239") <> 0 Then
                MsgBox "Insert Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
            Else
                frmData.SetServerParameters
                frmData.aUfis.CallServer "SBC", "LOA:IRT", frmData.tabData(3).LogicalFieldList, strValues, omCurrentSelectedUrno, "230"
            End If
            'kkh on 28/11/2008 Display KRI as higher priority then PTM message for all flights
            frmData.tabData(3).Sort "3,2", True, True
            'frmData.tabData(3).Sort "1,2,3", True, True  ' This reorgs the indexes as well!

            frmData.tabData(0).GetLinesByIndexValue "URNO", omCurrentSelectedUrno, 0
            llLine = frmData.tabData(0).GetNextResultLine
            If llLine > -1 Then
                strTifa = frmData.tabData(0).GetFieldValue(llLine, "TIFA")
                If Trim(strTifa) <> "" Then
                    datTifa = CedaFullDateToVb(strTifa)
                End If
            End If
            
            If HasArrivalConflict(omCurrentSelectedUrno, datTifa, True) = True Then
                If frmData.tabCurrentBar.GetLineCount > 0 Then
                    frmData.tabCurrentBar.SetColumnValue 0, 1, CStr(vbRed)
                Else
                    frmData.tabCurrentBar.SetColumnValue 0, 1, CStr(colorArrivalBar)
                End If
            Else
                If frmData.tabCurrentBar.GetLineCount > 0 Then
                    frmData.tabCurrentBar.SetColumnValue 0, 1, CStr(colorArrivalBar)
                End If
            End If
            SelectMainGanttBar omCurrentSelectedUrno, 0
        End If
    End If
End Sub

Public Sub CheckArrivalsForDeparture(strDepUrno As String)
    Dim i As Long
    Dim cnt As Long
    Dim strRawFlno As String
    Dim strRawFlnoCopy As String
    Dim strFlno As String
    Dim strFlightDay As String
    Dim arr() As String
    Dim llAftLine As Long
    Dim llAftLineArrival As Long
    Dim llLoaTabLine As Long
    Dim strLine As String
    Dim strAftUrno2 As String
    Dim strArrUrno As String
    Dim strAdid As String
    Dim strValues As String
    Dim dummy1 As String, dummy2 As String, dummy3 As String
    Dim strStoa As String
    Dim strFlnu As String
    Dim strFlnuTmp As String
    Dim datStoa As Date
    Dim strTifa As String
    Dim datTifa As Date
    Dim tabIDX As Integer
    
    Dim strRKEY As String
    Dim strRet As String
    Dim strAftUrno As String
    Dim strAftUrnoTmp As String
    Dim llLine As Long
    Dim blFound As Boolean
    
    blFound = False
    frmData.tabData(3).SetInternalLineBuffer False
    tabResultLines.ResetContent
    tabResultLines.HeaderString = "LINE"
    tabResultLines.LogicalFieldList = "LINE"
    tabResultLines.HeaderLengthString = "100"
    strRet = frmData.tabData(3).GetLinesByIndexValue("RTAB", strDepUrno, 0)
    tabResultLines.InsertBuffer strRet, ","
    frmData.tabData(3).SetInternalLineBuffer True
    
    strAftUrno = ""
    For i = 0 To tabResultLines.GetLineCount - 1
        strAftUrnoTmp = frmData.tabData(3).GetFieldValue(Val(tabResultLines.GetFieldValue(i, "LINE")), "FLNU")
        If strAftUrnoTmp <> "" Then
            If strAftUrno = strAftUrnoTmp Then
                'do nothing
            Else
                strAftUrno = strAftUrnoTmp
                strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", strAftUrno, 0)
                llAftLine = frmData.tabData(0).GetNextResultLine
                If llAftLine > -1 Then
                    UpdateBar llAftLine
                    blFound = True
                End If
            End If
        End If
    Next i
'    frmData.tabData(0).GetLinesByIndexValue "URNO", strDepUrno, 0
'    llAftLine = frmData.tabData(0).GetNextResultLine
'    If llAftLine > -1 Then
'        strFlno = frmData.tabData(0).GetFieldValue(llAftLine, "FLNO")
'        strRKEY = frmData.tabData(0).GetFieldValue(llAftLine, "RKEY")
'        strRet = frmData.tabData(0).GetLinesByIndexValue("RKEY", strRKEY, 0)
'        llAftLine = frmData.tabData(0).GetNextResultLine
'        While llAftLine > -1
'            If (frmData.tabData(0).GetFieldValue(llAftLine, "ADID") = "A") Then
'                UpdateBar llAftLine
'                'llAftLine = -1
'                blFound = True
'            End If
'            llAftLine = frmData.tabData(0).GetNextResultLine
'        Wend
'    End If
    
    tabIDX = 3
    
    If blFound = False Then
    
        strFlnu = ""
        For i = 0 To frmData.tabData(tabIDX).GetLineCount - 1
            'First of all get the corresponding arrival flight
            'to identify the correct stoa and this is to get the
            'correct Departure flight
            strFlnuTmp = frmData.tabData(tabIDX).GetFieldValue(i, "FLNU")
            'strFlnu = frmData.tabData(tabIDX).GetFieldValue(i, "FLNU")
            If strFlnu = strFlnuTmp Then
                'do nothing
            Else
                strFlnu = strFlnuTmp
                strLine = frmData.tabData(0).GetLinesByIndexValue("URNO", strFlnu, 0)
                llAftLine = frmData.tabData(0).GetNextResultLine
                If llAftLine > -1 Then
                    strStoa = frmData.tabData(0).GetFieldValue(llAftLine, "STOA")
                End If
                If Trim(strStoa) <> "" Then
                    datStoa = CedaFullDateToVb(strStoa)
                End If
                strRawFlno = frmData.tabData(tabIDX).GetFieldValue(i, "FLNO")
                arr = Split(strRawFlno, "/")
                If UBound(arr) = 1 Then
                    strFlno = arr(0)
                    strFlightDay = arr(1)
                Else
                    strFlno = strRawFlno
                    strFlightDay = ""
                End If
                strFlno = StripAftFlno(strFlno, dummy1, dummy2, dummy3)
                llAftLine = GetCorrectDepartureByFlightDay(datStoa, strFlightDay, strFlno)
                If Trim(strFlightDay) <> "" Then
                    strRawFlnoCopy = strFlno + "/" + strFlightDay
                Else
                    strRawFlnoCopy = strFlno
                End If
                'llAftLine = GetCorrectDepartureLine(Key, opStoa, strFlno)
                If llAftLine > -1 Then
                    strAftUrno2 = frmData.tabData(0).GetFieldValue(llAftLine, "URNO")
                    If strAftUrno2 = strDepUrno Then
                        strArrUrno = frmData.tabData(tabIDX).GetFieldValue(i, "FLNU")
                        strLine = frmData.tabData(0).GetLinesByIndexValue("URNO", strArrUrno, 0)
                        llAftLineArrival = frmData.tabData(0).GetNextResultLine
                        If llAftLineArrival > -1 Then
                            strAdid = frmData.tabData(0).GetFieldValue(llAftLineArrival, "ADID")
                            strFlno = frmData.tabData(0).GetFieldValue(llAftLineArrival, "FLNO")
                            dummy1 = frmData.tabData(0).GetFieldValue(llAftLineArrival, "URNO")
                            If strAdid = "A" Then
                                UpdateBar llAftLineArrival
                                'i = frmData.tabData(tabIDX).GetLineCount
                            End If
                        End If
                    End If
                End If
                llAftLine = -1
            End If
        Next i
    End If
End Sub

'igu on 23/08/2011: ALTEA
Private Sub CopyToSubLoaTab()
    Dim intFieldNo As Integer
    Dim intFieldCount As Integer
    Dim strHeaderLengthString As String
    Dim strLine As String
    Dim strLogicalFieldList As String
    Dim strDSSN As String
    Dim strFind As String
    Dim strFindKey As String
    Dim llCurrLine As Long
    Dim llLine As Long
    
    strLogicalFieldList = frmData.tabData(3).LogicalFieldList
    intFieldCount = UBound(Split(strLogicalFieldList, ","))
    With frmData.tabSubLOA
        .ResetContent
        .LogicalFieldList = strLogicalFieldList & ",ORDE"
        .HeaderString = strLogicalFieldList & ",ORDER"
        strHeaderLengthString = "100"
        For intFieldNo = 1 To intFieldCount
            strHeaderLengthString = strHeaderLengthString & ",100"
        Next intFieldNo
        .HeaderLengthString = strHeaderLengthString
        
        llLine = frmData.tabData(3).GetNextResultLine
        While llLine <> -1
            strLine = frmData.tabData(3).GetLineValues(llLine) & ","
            strDSSN = frmData.tabData(3).GetFieldValue(llLine, "DSSN")
            Select Case strDSSN
                Case "KRI"
                    strLine = strLine & "1"
                Case "ALT"
                    strLine = strLine & "1"
                Case "PTM"
                    strLine = strLine & "3"
            End Select
            
            If strDSSN = "KRI" Then
                strFindKey = "ALT," & frmData.tabData(3).GetFieldValues(llLine, "TYPE,STYP,SSTP,SSST,APC3")
                strFind = frmData.tabSubLOA.GetLinesByMultipleColumnValue("3,5,6,7,8,9", strFindKey, 0)
                If strFind = "" Then
                    .InsertTextLine strLine, False
                Else
                    llCurrLine = Val(strFind)
                    .SetFieldValues llCurrLine, .LogicalFieldList, strLine
                End If
            Else
                .InsertTextLine strLine, False
            End If
            
            llLine = frmData.tabData(3).GetNextResultLine
        Wend
        
        .Sort CStr(intFieldCount) & ",2", True, True
    End With
End Sub
