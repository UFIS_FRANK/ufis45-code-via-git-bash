VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form frmSetup 
   Caption         =   "Setup ..."
   ClientHeight    =   4230
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5760
   Icon            =   "frmSetup.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4230
   ScaleWidth      =   5760
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox picFrame 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   8295
      Left            =   0
      ScaleHeight     =   8295
      ScaleWidth      =   6135
      TabIndex        =   0
      Top             =   0
      Width           =   6135
      Begin VB.TextBox txtMinDepartureLen 
         Height          =   315
         Left            =   2340
         TabIndex        =   10
         Text            =   "15"
         Top             =   600
         Width           =   1515
      End
      Begin VB.TextBox txtMinArrivalBarLen 
         Height          =   315
         Left            =   2340
         TabIndex        =   9
         Text            =   "15"
         Top             =   180
         Width           =   1515
      End
      Begin VB.CommandButton cmdOK 
         Caption         =   "OK"
         Height          =   375
         Left            =   2130
         TabIndex        =   8
         Top             =   3600
         Width           =   1035
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   375
         Left            =   3270
         TabIndex        =   7
         Top             =   3600
         Width           =   1035
      End
      Begin VB.CheckBox cmdArrivalBarColor 
         BackColor       =   &H00FFFFFF&
         Caption         =   "XXX 876"
         Height          =   315
         Left            =   2340
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   1140
         Width           =   1575
      End
      Begin VB.CheckBox cmdDepartureBarColor 
         BackColor       =   &H00FFFFC0&
         Caption         =   "YYY 765"
         Height          =   315
         Left            =   2340
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   1500
         Width           =   1575
      End
      Begin VB.CheckBox cmdMarkColor 
         BackColor       =   &H00FF00FF&
         Caption         =   "YYY 534"
         Height          =   315
         Left            =   2340
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   1860
         Width           =   1575
      End
      Begin VB.TextBox txtDetailGanttTimeFrame 
         Height          =   315
         Left            =   2340
         TabIndex        =   3
         Text            =   "2"
         Top             =   2280
         Width           =   1575
      End
      Begin VB.CheckBox chkShowTips 
         Caption         =   "&Show hint labels"
         Height          =   195
         Left            =   2340
         TabIndex        =   2
         Top             =   2760
         Value           =   1  'Checked
         Width           =   195
      End
      Begin VB.CheckBox chkTimesInUTC 
         Caption         =   "&Times in UTC"
         Height          =   195
         Left            =   2340
         TabIndex        =   1
         Top             =   3120
         Width           =   195
      End
      Begin MSComDlg.CommonDialog ComDlg 
         Left            =   4320
         Top             =   240
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.Label Label9 
         BackStyle       =   0  'Transparent
         Caption         =   "Times in UTC"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   2580
         TabIndex        =   19
         Top             =   3120
         Width           =   1215
      End
      Begin VB.Label Label8 
         BackStyle       =   0  'Transparent
         Caption         =   "Show hint labels"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   2580
         TabIndex        =   18
         Top             =   2760
         Width           =   1335
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Min. Arrival Bar Length"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   180
         TabIndex        =   17
         Top             =   240
         Width           =   1995
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Min.Departure Bar Length"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   180
         TabIndex        =   16
         Top             =   660
         Width           =   1995
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Bar Arrival Colour:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   180
         TabIndex        =   15
         Top             =   1200
         Width           =   1995
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Bar Departure Colour:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   180
         TabIndex        =   14
         Top             =   1560
         Width           =   1995
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "Marked Bar Colour:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   180
         TabIndex        =   13
         Top             =   1920
         Width           =   1995
      End
      Begin VB.Label Label6 
         BackStyle       =   0  'Transparent
         Caption         =   "Connection Gantt Timeframe:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   180
         TabIndex        =   12
         Top             =   2340
         Width           =   2115
      End
      Begin VB.Label Label7 
         BackStyle       =   0  'Transparent
         Caption         =   "Hours"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   4020
         TabIndex        =   11
         Top             =   2340
         Width           =   495
      End
   End
End
Attribute VB_Name = "frmSetup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdArrivalBarColor_Click()
    On Error GoTo Err
    If cmdArrivalBarColor.Value = 1 Then
        cmdArrivalBarColor.Value = 0
        ComDlg.CancelError = True ' force error when cancel was pressed in color dialog
                                  ' the bullshit is, that Microsoft has no return code
                                  ' for the dialog => so we must produce an error!
        ComDlg.ShowColor
        cmdArrivalBarColor.backColor = ComDlg.Color
    End If
    Exit Sub
Err:
End Sub

Private Sub cmdClose_Click()
    Unload Me
End Sub

Private Sub cmdDepartureBarColor_Click()
    On Error GoTo Err
    If cmdDepartureBarColor.Value = 1 Then
        cmdDepartureBarColor.Value = 0
        ComDlg.CancelError = True
        ComDlg.ShowColor
        cmdDepartureBarColor.backColor = ComDlg.Color
    End If
    Exit Sub
Err:
End Sub

Private Sub cmdMarkColor_Click()
    On Error GoTo Err
    If cmdMarkColor.Value = 1 Then
        cmdMarkColor.Value = 0
        ComDlg.CancelError = True
        ComDlg.ShowColor
        cmdMarkColor.backColor = ComDlg.Color
    End If
    Exit Sub
Err:
End Sub

Private Sub cmdOK_Click()
    Dim UpdateGantts As Boolean
    Dim strShowHints As String
    Dim strTimesInUTC As String
    Dim i As Integer
    Dim UTCChanged As Boolean
    Dim blToUTC As Boolean
    Dim blToLocal As Boolean
    
    UpdateGantts = False
    UTCChanged = False
    blToLocal = False
    blToUTC = False
    
    frmData.tabCfg.ResetContent
    If chkShowTips.Value = 0 Then
        strShowHiddenData = "N"
    Else
        strShowHiddenData = "Y"
    End If
    If chkTimesInUTC.Value = 0 Then
        strTimesInUTC = "N"
    Else
        strTimesInUTC = "Y"
    End If
    If (strTimesInUTC = "Y" And TimesInUTC = False) Or _
       (strTimesInUTC = "N" And TimesInUTC = True) Then
        UTCChanged = True
        UpdateGantts = True
        If (strTimesInUTC = "Y" And TimesInUTC = False) Then
            blToUTC = True
            blToLocal = False
        Else
            blToUTC = False
            blToLocal = True
        End If
    End If
    'This is necessary here and not earlier, because we have
    'to check, new value against old value
    If strTimesInUTC = "Y" Then
        TimesInUTC = True
    Else
        TimesInUTC = False
    End If
    
    frmData.tabCfg.InsertTextLine "MinArrivalBarLen" + "," + CStr(txtMinArrivalBarLen), False
    frmData.tabCfg.InsertTextLine "MinDepartureLen" + "," + CStr(txtMinDepartureLen), False
    frmData.tabCfg.InsertTextLine "ArrivalBarColor" + "," + CStr(cmdArrivalBarColor.backColor), False
    frmData.tabCfg.InsertTextLine "DepartureBarColor" + "," + CStr(cmdDepartureBarColor.backColor), False
    frmData.tabCfg.InsertTextLine "MarkColor" + "," + CStr(cmdMarkColor.backColor), False
    frmData.tabCfg.InsertTextLine "DetailGanttTimeFrame" + "," + CStr(txtDetailGanttTimeFrame), False
    frmData.tabCfg.InsertTextLine "ShowHints" + "," + strShowHiddenData, False
    frmData.tabCfg.InsertTextLine "TimesInUTC" + "," + strTimesInUTC, False
    frmData.tabCfg.Refresh
    
    If cmdArrivalBarColor.backColor <> colorArrivalBar Or _
       colorDepartureBar <> cmdDepartureBarColor.backColor Or _
        MinDepartureBarLen <> Val(txtMinDepartureLen) Or _
        MinArrivalBarLen <> Val(txtMinArrivalBarLen) Then
        UpdateGantts = True
    End If
    colorArrivalBar = cmdArrivalBarColor.backColor
    colorDepartureBar = cmdDepartureBarColor.backColor
    colorCurrentBar = cmdMarkColor.backColor
    DetailGanttTimeFrame = txtDetailGanttTimeFrame
    MinDepartureBarLen = txtMinDepartureLen
    MinArrivalBarLen = txtMinArrivalBarLen
    If strShowHiddenData = "Y" Then
        ShowHints = True
    Else
        ShowHints = False
    End If
    
    If UTCChanged = True Then
        If frmMain.bgMailCargoIsActive = True Then
            Unload frmMailCargo
        End If
    End If
    
    If UpdateGantts = True Then
        If blToUTC = True Then
            frmData.ChangeAftToUTC
        End If
        If blToLocal = True Then
            frmData.ChageAftToLocal
        End If
        
        frmMain.SpoolBC False
        Me.Hide
        frmMain.Refresh
        frmMain.MyInit False, False ' No update of bars and do not handle the compresse gantt
        frmMain.gantt(0).Refresh
        frmMain.gantt(1).Refresh
        If bgfrmCompressedFlightsOpen = True Then
            frmCompressedFligths.Refresh
            frmCompressedFligths.Allocate
        End If
        frmMain.SpoolBC True
    End If
    For i = 0 To frmMain.lblTip.count - 1
        If ShowHints = False Then
            frmMain.lblTip(i).Visible = False
        Else
            frmMain.lblTip(i).Visible = True
        End If
    Next i
    If frmMailCargo.Visible = True Then
        For i = 0 To frmMailCargo.lblTip.count - 1
            If ShowHints = False Then
                frmMailCargo.lblTip(i).Visible = False
            Else
                frmMailCargo.lblTip(i).Visible = True
            End If
        Next i
    End If
    frmData.tabCfg.WriteToFile UFIS_APPL & "\HUBMANAGER.cfg", False
    Unload Me
End Sub

Private Sub Form_Load()
    Dim strField As String
    Dim strValue As String
    Dim i As Long
    Dim cnt As Long
    
    txtMinArrivalBarLen = MinArrivalBarLen
    txtMinDepartureLen = MinDepartureBarLen
    cmdArrivalBarColor.backColor = colorArrivalBar
    txtDetailGanttTimeFrame = DetailGanttTimeFrame
    cmdDepartureBarColor.backColor = colorDepartureBar
    cmdMarkColor.backColor = colorCurrentBar
    
    cnt = frmData.tabCfg.GetLineCount - 1
    For i = 0 To cnt
        strField = frmData.tabCfg.GetFieldValue(i, "FIELD")
        strValue = frmData.tabCfg.GetFieldValue(i, "VALUE")
        Select Case (strField)
            Case "MinArrivalBarLen"
                txtMinArrivalBarLen = strValue
            Case "MinDepartureLen"
                txtMinDepartureLen = strValue
            Case "ArrivalBarColor"
                cmdArrivalBarColor.backColor = Val(strValue)
            Case "DepartureBarColor"
                cmdDepartureBarColor.backColor = Val(strValue)
            Case "MarkColor"
                cmdMarkColor.backColor = Val(strValue)
            Case "DetailGanttTimeFrame"
                txtDetailGanttTimeFrame = strValue
            Case "ShowHints"
                If strValue = "Y" Then
                    chkShowTips.Value = 1
                Else
                    chkShowTips.Value = 0
                End If
            Case "TimesInUTC"
                If strValue = "Y" Then
                    chkTimesInUTC.Value = 1
                Else
                    chkTimesInUTC.Value = 0
                End If
        End Select
    Next i
    DrawBackGround picFrame, 7, True, True
End Sub


