// CedaCotData.cpp
 
#include <stdafx.h>
#include <CedaCotData.h>


void ProcessCotCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaCotData::CedaCotData() : CCSCedaData(&ogCommHandler)
{
CCS_TRY
	// Create an array of CEDARECINFO for COTDATA
	BEGIN_CEDARECINFO(COTDATA,CotDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Ctrc,"CTRC")
		FIELD_CHAR_TRIM	(Ctrn,"CTRN")
		FIELD_CHAR_TRIM	(Dptc,"DPTC")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Misl,"MISL")
		FIELD_CHAR_TRIM	(Mxsl,"MXSL")
		FIELD_CHAR_TRIM	(Nowd,"NOWD")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Whpw,"WHPW")
		FIELD_CHAR_TRIM	(Wrkd,"WRKD")
		FIELD_CHAR_TRIM	(Sbpa,"SBPA")
		FIELD_CHAR_TRIM (Inbu,"INBU")
	END_CEDARECINFO //(COTDATA)

	// Copy the record structure
	for (int i=0; i< sizeof(CotDataRecInfo)/sizeof(CotDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&CotDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"COT");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmListOfFields,"CDAT,CTRC,CTRN,DPTC,LSTU,MISL,MXSL,NOWD,PRFL,REMA,URNO,USEC,USEU,WHPW,WRKD,SBPA,INBU");


	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
CCS_CATCH_ALL
}

//---------------------------------------------------------------------------------------------------------

void CedaCotData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaCotData::Register(void)
{
CCS_TRY
	ogDdx.Register((void *)this,BC_COT_CHANGE,	CString("COTDATA"), CString("Cot-changed"),	ProcessCotCf);
	ogDdx.Register((void *)this,BC_COT_NEW,		CString("COTDATA"), CString("Cot-new"),		ProcessCotCf);
	ogDdx.Register((void *)this,BC_COT_DELETE,	CString("COTDATA"), CString("Cot-deleted"),	ProcessCotCf);
CCS_CATCH_ALL
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaCotData::~CedaCotData(void)
{
CCS_TRY
	omRecInfo.DeleteAll();
	ClearAll();
CCS_CATCH_ALL
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaCotData::ClearAll(bool bpWithRegistration)
{
CCS_TRY
    omUrnoMap.RemoveAll();
    omKeyMap.RemoveAll();
    omCtrcMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
CCS_CATCH_ALL
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaCotData::Read(char *pspWhere /*NULL*/)
{
CCS_TRY
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
 	omKeyMap.RemoveAll();
	omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		COTDATA *prlCot = new COTDATA;
		if ((ilRc = GetFirstBufferRecord(prlCot)) == true)
		{
			omData.Add(prlCot);//Update omData
			omUrnoMap.SetAt((void *)prlCot->Urno, prlCot);

			CString olTmp;
			olTmp.Format("%s-%s",prlCot->Ctrc, prlCot->Dptc);
			omKeyMap.SetAt(olTmp,prlCot);

			olTmp.Format("%s",prlCot->Ctrc);
			omCtrcMap.SetAt(olTmp,prlCot);
		}
		else
		{
			delete prlCot;
		}
	}
    return true;
CCS_CATCH_ALL
return false;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaCotData::Insert(COTDATA *prpCot)
{
CCS_TRY
	prpCot->IsChanged = DATA_NEW;
	if(Save(prpCot) == false) return false; //Update Database
	InsertInternal(prpCot);
    return true;
CCS_CATCH_ALL
return false;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaCotData::InsertInternal(COTDATA *prpCot)
{
CCS_TRY
	ogDdx.DataChanged((void *)this, COT_NEW,(void *)prpCot ); //Update Viewer
	omData.Add(prpCot);//Update omData
	omUrnoMap.SetAt((void *)prpCot->Urno,prpCot);

	CString olTmp;
	olTmp.Format("%s-%s", prpCot->Ctrc, prpCot->Dptc);
	omKeyMap.SetAt(olTmp, prpCot);

	olTmp.Format("%s",prpCot->Ctrc);
	omCtrcMap.SetAt(olTmp, prpCot);

    return true;
CCS_CATCH_ALL
return false;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaCotData::Delete(long lpUrno)
{
CCS_TRY
	COTDATA *prlCot = GetCotByUrno(lpUrno);
	if (prlCot != NULL)
	{
		prlCot->IsChanged = DATA_DELETED;
		if(Save(prlCot) == false) return false; //Update Database
		DeleteInternal(prlCot);
	}
    return true;
CCS_CATCH_ALL
return false;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaCotData::DeleteInternal(COTDATA *prpCot)
{
CCS_TRY
	ogDdx.DataChanged((void *)this,COT_DELETE,(void *)prpCot); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpCot->Urno);

	CString olTmp;
	olTmp.Format("%s-%s", prpCot->Ctrc, prpCot->Dptc);
	omKeyMap.RemoveKey(olTmp);

	olTmp.Format("%s", prpCot->Ctrc);
	omCtrcMap.RemoveKey(olTmp);

	int ilCotCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilCotCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpCot->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
CCS_CATCH_ALL
return false;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaCotData::Update(COTDATA *prpCot)
{
CCS_TRY
	if (GetCotByUrno(prpCot->Urno) != NULL)
	{
		if (prpCot->IsChanged == DATA_UNCHANGED)
		{
			prpCot->IsChanged = DATA_CHANGED;
		}
		if(Save(prpCot) == false) return false; //Update Database
		UpdateInternal(prpCot);
	}
    return true;
CCS_CATCH_ALL
return false;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaCotData::UpdateInternal(COTDATA *prpCot)
{
CCS_TRY
	COTDATA *prlCot = GetCotByUrno(prpCot->Urno);
	if (prlCot != NULL)
	{
		CString olTmp;
		olTmp.Format("%s-%s", prlCot->Ctrc, prlCot->Dptc);
		omKeyMap.RemoveKey(olTmp);

		olTmp.Format("%s", prlCot->Ctrc);
		omCtrcMap.RemoveKey(olTmp);

		*prlCot = *prpCot; //Update omData

		olTmp.Format("%s-%s", prlCot->Ctrc, prlCot->Dptc);
		omKeyMap.SetAt(olTmp, prlCot);

		olTmp.Format("%s",prlCot->Ctrc);
		omCtrcMap.SetAt(olTmp, prlCot);

		ogDdx.DataChanged((void *)this,COT_CHANGE,(void *)prlCot); //Update Viewer
	}
    return true;
CCS_CATCH_ALL
return false;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

COTDATA *CedaCotData::GetCotByUrno(long lpUrno)
{
CCS_TRY
	COTDATA  *prlCot;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlCot) == TRUE)
	{
		return prlCot;
	}
	return NULL;
CCS_CATCH_ALL
return NULL;
}

//--GET BY CTRC--------------------------------------------------------------

COTDATA *CedaCotData::GetCotByCtrc(CString opCtrc)
{
CCS_TRY
	CString olTmp;
	olTmp.Format("%s", opCtrc);

	COTDATA  *prlCot = NULL;
	if (omCtrcMap.Lookup(olTmp,(void *&)prlCot) == TRUE)
	{
		return prlCot;
	}
	return NULL;
CCS_CATCH_ALL
return NULL;
}

//--GET BY KEY---------------------------------------------------------------

COTDATA *CedaCotData::GetCotByKey(CString opCtrc, CString opDptc)
{
CCS_TRY
	CString olTmp;
	olTmp.Format("%s-%s", opCtrc, opDptc);

	COTDATA  *prlCot = NULL;
	if (omKeyMap.Lookup(olTmp,(void *&)prlCot) == TRUE)
	{
		return prlCot;
	}
	return NULL;
CCS_CATCH_ALL
return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaCotData::ReadSpecialData(CCSPtrArray<COTDATA> *popCot,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
CCS_TRY
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popCot != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			COTDATA *prpCot = new COTDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpCot,CString(pclFieldList))) == true)
			{
				popCot->Add(prpCot);
			}
			else
			{
				delete prpCot;
			}
		}
		if(popCot->GetSize() == 0) return false;
	}
    return true;
CCS_CATCH_ALL
return false;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaCotData::Save(COTDATA *prpCot)
{
CCS_TRY
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpCot->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpCot->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpCot);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpCot->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpCot->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpCot);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpCot->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpCot->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
CCS_CATCH_ALL
return false;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessCotCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
CCS_TRY
	ogCotData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
CCS_CATCH_ALL
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaCotData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
CCS_TRY
	struct BcStruct *prlCotData;
	prlCotData = (struct BcStruct *) vpDataPointer;
	COTDATA *prlCot;
	long llUrno;
	CString olSelection;

	switch(ipDDXType)
	{
	default:
		break;
	case BC_COT_NEW:
		prlCot = new COTDATA;
		GetRecordFromItemList(prlCot,prlCotData->Fields,prlCotData->Data);
		InsertInternal(prlCot);
		break;
	case BC_COT_CHANGE:
		llUrno = GetUrnoFromSelection(prlCotData->Selection);
		prlCot = GetCotByUrno(llUrno);
		if(prlCot != NULL)
		{
			GetRecordFromItemList(prlCot,prlCotData->Fields,prlCotData->Data);
			UpdateInternal(prlCot);
		}
		break;
	case BC_COT_DELETE:
		olSelection = (CString)prlCotData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlCotData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlCot = GetCotByUrno(llUrno);
		if (prlCot != NULL)
		{
			DeleteInternal(prlCot);
		}
		break;
	}
CCS_CATCH_ALL
}

//---------------------------------------------------------------------------------------------------------

bool CedaCotData::ReadCotData()
{
CCS_TRY
	// Return-Code f�r Funktionsaufrufe
	bool ilRc = true;

	ClearAll(true);
	
    if (!CCSCedaData::CedaAction("RT", "")) return false;
	
	// ToDo: vereinfachen/umwandeln in do/while, zwei branches f�r
	// if(pomLoadStfuUrnoMap != NULL) �berfl�ssig
    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecords = 0;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		COTDATA *prlCot = new COTDATA;
		// Datensatz lesen
		blMoreRecords = GetBufferRecord(ilCountRecords,prlCot);
		// Datensatz gelesen, Z�hler inkrementieren
		if (!blMoreRecords) break;
		ilCountRecords++;
		if (!InsertInternal(prlCot)){ //, SOR_NO_SEND_DDX)){
			// Fehler -> lokalen Datensatz l�schen
			delete prlCot;
		}
	} while (blMoreRecords);
	
	
	// Test: Anzahl der gelesenen Bud
	TRACE("Read-Cot: %d gelesen\n",ilCountRecords);

    return true;
CCS_CATCH_ALL
return false;
}
