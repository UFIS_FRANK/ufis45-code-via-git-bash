// CedaStfData.h

#ifndef __CEDAPSTFDATA__
#define __CEDAPSTFDATA__
 
#include <stdafx.h>
#include <basicdata.h>
#include <afxdisp.h>

//---------------------------------------------------------------------------------------------------------

struct STFDATA 
{
	CTime 	 Cdat; 	// Erstellungsdatum
	COleDateTime 	 Dodm; 	// Austrittsdatum
	COleDateTime 	 Doem; 	// Eintrittsdatum
	char 	 Finm[22]; 	// Vorname
	char 	 Ktou[7]; 	// Urlaubsanspr�che
	char 	 Lanm[42]; 	// Name
	CTime 	 Lstu; 	// Datum letzte �nderung
	char 	 Makr[7]; 	// Mitarbeiterkreis
	char 	 Peno[22]; 	// Personalnummer
	char 	 Perc[5]; 	// K�rzel
	char 	 Prfl[3]; 	// Protokollierungskennung
	char 	 Rema[62]; 	// Bemerkungen
	char 	 Shnm[14]; 	// Kurzname
	char 	 Sken[7]; 	// Schichtkennzeichen
	char 	 Teld[22]; 	// Telefonnr. dienstlich
	char 	 Telh[22]; 	// Telefonnr. Handy
	char 	 Telp[22]; 	// Telefonnr. privat
	char 	 Tohr[3]; 	// Soll an SAP-HR �bermittelt werden
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	char 	 Zutb[7]; 	// Zus�tzliche Urlaubstage Schwerbehinderung
	char 	 Zutf[7]; 	// Zus�tzliche Urlaubstage Freistellungstage
	char 	 Zutn[7]; 	// Zus�tzliche Urlaubstage Nachtschicht
	char 	 Zutw[7]; 	// Zus�tzliche Urlaubstage Wechselschicht
	char	Pdgl[6]; //	personal full tour duration
	char	Pdkl[6]; //	personal short tour duration
	char	Pmak[6]; //	Anzahl Kurztouren pro Monat
	char	Pmag[6]; //	Anzahl Ganztouren pro Monat

	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	STFDATA(void)
	{ memset(this,'\0',sizeof(*this));
		Cdat=TIMENULL,Lstu=TIMENULL; //<zB.(FIELD_DATE Felder)
		Dodm.SetStatus(COleDateTime::invalid);
		Doem.SetStatus(COleDateTime::invalid);
	}

}; // end STFDataStruct

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaStfData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<STFDATA> omData;

	char pcmListOfFields[2048];

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
// OStfations
public:
	bool ReadStfData();
    CedaStfData();
	~CedaStfData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(STFDATA *prpStf);
	bool InsertInternal(STFDATA *prpStf);
	bool Update(STFDATA *prpStf);
	bool UpdateInternal(STFDATA *prpStf);
	bool Delete(long lpUrno);
	bool DeleteInternal(STFDATA *prpStf);
	bool ReadSpecialData(CCSPtrArray<STFDATA> *popStf,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(STFDATA *prpStf);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	STFDATA  *GetStfByUrno(long lpUrno);
	// Pr�ft ob MA zu diesem Zeitpunkt im Unternehmen ist
	bool IsStfNowValid(long lpUrno, COleDateTime opDay);

	// Private methods
private:
    void PrepareStfData(STFDATA *prpStfData);

};

//extern CedaStfData ogStfData;

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAPSTFDATA__
