// BudgetDataDlg.cpp : implementation file
//

#include <stdafx.h>
#include <Budget.h>
#include <BudgetDataDlg.h>
#include <BasicData.h>
#include <CedaBudData.h>
#include <CedaUbuData.h>
#include <CedaDataHelper.h>
#include <CedaOrgData.h>
#include <CedaScoData.h>
#include <CedaSorData.h>
#include <CedaStfData.h>
#include <CedaCotData.h>
#include <CedaMutData.h>
#include <CedaUmuData.h>
#include <CedaForData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBudgetDataDlg dialog


CBudgetDataDlg::CBudgetDataDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CBudgetDataDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CBudgetDataDlg)
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_urno = 0;
	m_new = true;
}

CBudgetDataDlg::CBudgetDataDlg(long urno, CWnd* pParent /*=NULL*/)
	: CDialog(CBudgetDataDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_urno = urno;
	m_new = false;
}

CBudgetDataDlg::~CBudgetDataDlg()
{
	if(pomData != NULL)
		delete pomData;

}

void CBudgetDataDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBudgetDataDlg)
		DDX_Control(pDX, IDC_CHECK_MUTATION, m_mutation);
		DDX_Control(pDX, IDC_CHECK_FORECAST, m_forecast);
		DDX_Control(pDX, IDC_CHECK_BUDGET, m_budget);
		DDX_Control(pDX, IDC_CHECK_HEADCOUNT, m_headcount);
		DDX_Control(pDX, IDC_TOMONTH, m_tomonth);
		DDX_Control(pDX, IDC_REMARK, m_remark);
		DDX_Control(pDX, IDC_FROMMONTH, m_frommonth);
		DDX_Control(pDX, IDC_DAY, m_day);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CBudgetDataDlg, CDialog)
	//{{AFX_MSG_MAP(CBudgetDataDlg)
	ON_BN_CLICKED(IDINFO, OnInfo)
	ON_COMMAND(ID_ORG_DELETE, OnOrgDelete)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBudgetDataDlg message handlers

BOOL CBudgetDataDlg::OnInitDialog() 
{
CCS_TRY
	BUDDATA* pBud;
	CString csMonth, csYear, csDay, csHour, csMinute, csNullString;
	COleDateTime cTime, CTimeNew;

	csNullString = "";

	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	SetDlgItemText(IDC_STATIC_CHANGEDBY, LoadStg(IDS_STRING1018));
	SetDlgItemText(IDC_STATIC_CHANGEDON, LoadStg(IDS_STRING1017));
	SetDlgItemText(IDC_STATIC_CREATEDBY, LoadStg(IDS_STRING1016));
	SetDlgItemText(IDC_STATIC_CREATEDON, LoadStg(IDS_STRING1015));
	SetDlgItemText(IDC_STATIC_DAY, LoadStg(IDS_STRING1002));
	SetDlgItemText(IDC_STATIC_MONTHS, LoadStg(IDS_STRING1019));
	SetDlgItemText(IDC_STATIC_ORG, LoadStg(IDS_STRING1026));
	SetDlgItemText(IDC_STATIC_TO, LoadStg(IDS_STRING1020));
	SetDlgItemText(IDC_STATIC_LISTTYPE, LoadStg(IDS_STRING1021));
	SetDlgItemText(IDC_STATIC_REMARK, LoadStg(IDS_STRING1010));
	SetDlgItemText(IDC_CHECK_HEADCOUNT, LoadStg(IDS_STRING1022));
	SetDlgItemText(IDC_CHECK_MUTATION, LoadStg(IDS_STRING1023));
	SetDlgItemText(IDC_CHECK_BUDGET, LoadStg(IDS_STRING1025));
	SetDlgItemText(IDC_CHECK_FORECAST, LoadStg(IDS_STRING1024));
	SetDlgItemText(IDOK, LoadStg(ID_OK));
	SetDlgItemText(IDCANCEL, LoadStg(ID_CANCEL));
	SetDlgItemText(IDINFO, LoadStg(ID_M_INFO));

	// Set the icon for this dialog.  The framework does this automatically
	// when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	
	m_day.SetTextLimit(0,2);
	m_day.SetTextErrColor(RED);
	//m_day.SetTypeToInt(1, 31);
	//m_day.SetTypeToString(CString("#{0-3}|#{0-9}"), 2, 0);

	m_frommonth.SetTextLimit(0,6);
	m_frommonth.SetTextErrColor(RED);
	
	m_tomonth.SetTextLimit(0,6);
	m_tomonth.SetTextErrColor(RED);
	
	m_remark.SetTextLimit(0,60);
	m_remark.SetTextErrColor(RED);
	
	if (m_new == true){

		m_day.SetInitText(csNullString);
		m_frommonth.SetInitText(csNullString);
		m_tomonth.SetInitText(csNullString);
		m_remark.SetInitText(csNullString);
		m_headcount.SetCheck(0);
		m_mutation.SetCheck(0);
		m_forecast.SetCheck(0);
		m_budget.SetCheck(0);
		//m_createdondate.SetWindowText(csNullString);
		//m_createdontime.SetWindowText(csNullString);
		//m_createdby.SetWindowText(csNullString);
		//m_changedondate.SetWindowText(csNullString);
		//m_changedontime.SetWindowText(csNullString);
		//m_changedby.SetWindowText(csNullString);
	}

	else {
		pBud = ogBudData.GetBudByUrno(m_urno);
		//Stichtag
		m_day.SetInitText(pBud->Dayx);
		//von Jahr/Monat
		csMonth = pBud->Frmo;
		csMonth = csMonth.Right(2);
		csYear = pBud->Frmo;
		csYear = csYear.Left(4);
		m_frommonth.SetInitText(csMonth + csYear); //"/" + csYear);
		//bis Jahr/Monat
		csMonth = pBud->Tomo;
		csMonth = csMonth.Right(2);
		csYear = pBud->Tomo;
		csYear = csYear.Left(4);
		m_tomonth.SetInitText(csMonth + csYear); //"/" + csYear);
		//Remarks
		m_remark.SetInitText(pBud->Rema);
		//Headcount
		m_headcount.SetCheck(atoi(pBud->Hdco));
		//Mutation
		m_mutation.SetCheck(atoi(pBud->Muta));
		//Forecast
		m_forecast.SetCheck(atoi(pBud->Foca));
		//Budget
		m_budget.SetCheck(atoi(pBud->Budg));
		//created on date
		cTime = pBud->Cdat;
		csYear.Format("%d", cTime.GetYear());
		csMonth.Format("%.2d", cTime.GetMonth());
		csDay.Format("%.2d", cTime.GetDay());
		m_createdonday = csDay + "." + csMonth + "." + csYear;
		//m_createdondate.SetWindowText(csDay + "." + csMonth + "." + csYear);
		//created on time
		csHour.Format("%.2d", cTime.GetHour());
		csMinute.Format("%.2d", cTime.GetMinute());
		m_createdonhour = csHour + ":" + csMinute;
		//m_createdontime.SetWindowText(csHour + ":" + csMinute);
		//created by
		m_createdbyuser = pBud->Usec;
		//m_createdby.SetWindowText(pBud->Usec);
		//changed on date
		cTime = pBud->Lstu;
		csYear.Format("%d", cTime.GetYear());
		csMonth.Format("%.2d", cTime.GetMonth());
		csDay.Format("%.2d", cTime.GetDay());
		m_changedonday = csDay + "." + csMonth + "." + csYear;
		//m_changedondate.SetWindowText(csDay + "." + csMonth + "." + csYear);
		//changed on time
		csHour.Format("%.2d", cTime.GetHour());
		csMinute.Format("%.2d", cTime.GetMinute());
		m_changedonhour = csHour + ":" + csMinute;
		//m_changedontime.SetWindowText(csHour + ":" + csMinute);
		if (csYear == "1899"){
			//m_changedondate.SetWindowText(" ");
			//m_changedontime.SetWindowText(" ");
			m_changedonday = "";
			m_changedonhour = "";
		}
		//changed by
		m_changedbyuser = pBud->Useu;
		//m_changedby.SetWindowText(pBud->Useu);
	}

	//Grid initialisieren
	IniGrid();
	
	m_day.SetFocus();
	return false;
	//return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
CCS_CATCH_ALL
	return false;
}

void CBudgetDataDlg::IniGrid()
{
CCS_TRY
	CString list;
	int i = 0;
	long ilUrno;

	for (int n=0; n<ogOrgData.omData.GetSize(); n++){
		list = list + ogOrgData.omData[n].Dpt1 + "\n";
	}

	for (int m=0; m<ogUbuData.omData.GetSize(); m++){
		if (ogUbuData.omData[m].Ubud == m_urno){
			i=i+1;
		}
	}

	pomData = new CGridControl(this, IDC_DATA_GRID, 2, __max(i, MINROWS1));

	//�berschriften setzten
	pomData->SetValueRange(CGXRange(0,1), LoadStg(IDS_STRING1009));
	pomData->SetValueRange(CGXRange(0,2), LoadStg(IDS_STRING1027));
	
	if (m_new == false){
		// Grid mit Daten f�llen.
		pomData->LockUpdate(true);

		//Org. Units
		i = 1;
		for (m=0; m<ogUbuData.omData.GetSize(); m++){
			if (ogUbuData.omData[m].Ubud == m_urno){
				// Urno auslesen
				ilUrno = ogUbuData.omData[m].Urno;
				// Urno mit erstem Feld koppeln.						
				pomData->SetStyleRange(CGXRange(i,1), CGXStyle().SetItemDataPtr( (void*)ilUrno ) );
				pomData->SetValueRange(CGXRange(i,1), ogUbuData.omData[m].Dpt1);
				pomData->SetValueRange(CGXRange(i,2), ogUbuData.omData[m].Star);
				i=i+1;
			}
		}
	
		// Anzeige einstellen
		pomData->LockUpdate(false);
		pomData->Redraw();
	}
	
	//Spaltenbreite und Zeilenh�he k�nnen nicht ver�ndert werden
	pomData->GetParam()->EnableTrackColWidth(FALSE);
	pomData->GetParam()->EnableTrackRowHeight(FALSE);
    
	//Scrollbars anzeigen wenn n�tig
	pomData->SetScrollBarMode(SB_BOTH, gxnAutomatic, TRUE);

	pomData->SetStyleRange( CGXRange().SetCols(1), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				//.SetReadOnly(TRUE)
				.SetControl(GX_IDS_CTRL_COMBOBOX) //GX_IDS_CTRL_CBS_DROPDOWN)
				.SetChoiceList(list.GetBuffer(0)));            
	
	//Keine Zeilennummerierung
	pomData->HideCols(0, 0);
	pomData->SetColWidth(1, 1, pomData->Width_LPtoDP(10 * GX_NXAVGWIDTH));
	pomData->SetColWidth(2, 2, pomData->Width_LPtoDP(3 * GX_NXAVGWIDTH));
	//pomData->HideCols(10, 13);

	//CheckBoxen in Spalten einf�gen
	pomData->SetColCheckbox(2);

	pomData->EnableAutoGrow(true);
	pomData->EnableSorting(false);

	// DoppelClick Acktion festlegen
	//pomData->SetLbDblClickAction ( WM_COMMAND, IDOK);
CCS_CATCH_ALL
}

void CBudgetDataDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

void CBudgetDataDlg::OnOK() 
{
CCS_TRY
	CString olText, csMonth, csYear, csYearMonth;
	CString csDay, csFromMonth, csToMonth, csHeadcount;
	CString csMutation, csForecast, csBudget, csRemark, sDpt1, sStar;

	BUDDATA *prlBudData;
	UBUDATA *prlUbuData;
	ORGDATA *prlOrgData;

	ROWCOL ilRowCount;
	
	long ilNewBudUrno, lpUrno;
	
	CGXStyle olStyle;

	m_day.GetWindowText(csDay);	
	m_frommonth.GetWindowText(csFromMonth);
	m_tomonth.GetWindowText(csToMonth);
	csHeadcount.Format("%i",m_headcount.GetCheck());
	csMutation.Format("%i",m_mutation.GetCheck());
	csForecast.Format("%i",m_forecast.GetCheck());
	csBudget.Format("%i",m_budget.GetCheck());
	m_remark.GetWindowText(csRemark);
	
	if (csDay.GetLength() == 0 && csFromMonth.GetLength() == 0 &&
		csToMonth.GetLength() == 0 && csHeadcount == '0' &&
		csMutation == '0' && csForecast == '0' &&
		csBudget == '0' && csRemark.GetLength() == 0);

	else{
		//Pr�fungen ???

		//Tag
		int iday = atoi(csDay);
		if(iday < 1 || iday > 31){
			m_day.SetStatus(false);
			return;
		}
		else
			m_day.SetStatus(true);
		
		//Monat/Jahr
		int imonth = atoi(csFromMonth.Left(2));
		if(imonth < 1 || imonth > 12){
			m_frommonth.SetStatus(false);
			return;
		}
		else
			m_frommonth.SetStatus(true);

		int iyear = atoi(csFromMonth.Right(4));
		if(iyear < 1950 || iyear > 2050){
			m_frommonth.SetStatus(false);
			return;
		}
		else
			m_frommonth.SetStatus(true);

		imonth = atoi(csToMonth.Left(2));
		if(imonth < 1 || imonth > 12){
			m_tomonth.SetStatus(false);
			return;
		}
		else
			m_tomonth.SetStatus(true);

		iyear = atoi(csToMonth.Right(4));
		if(iyear < 1950 || iyear > 2050){
			m_tomonth.SetStatus(false);
			return;
		}
		else
			m_tomonth.SetStatus(true);

		//Org. Units
		prlOrgData = NULL;
		ilRowCount = pomData->GetRowCount();
		for (ROWCOL i=0; i<ilRowCount; i++){
			sDpt1 = pomData->GetValueRowCol(i+1,1);
			if (sDpt1.GetLength() > 0){
				prlOrgData = ogOrgData.GetOrgByDpt1(sDpt1);
				if (prlOrgData == NULL){
					pomData->SetStyleRange(CGXRange(i+1, 1), CGXStyle( ).SetTextColor(RGB(255, 0, 0)));
					return;
				}
				else
					pomData->SetStyleRange(CGXRange(i+1, 1), CGXStyle( ).SetTextColor(RGB(0, 0, 0)));
			}
		}

		if (m_new == true)
			// Objekt erzeugen
			prlBudData = new BUDDATA;
		else
			prlBudData = ogBudData.GetBudByUrno(m_urno);
		
		//Stichtag
		strcpy(prlBudData->Dayx, csDay.GetBuffer(0));
		//Jahr/Monat von
		csMonth = csFromMonth.Left(2);
		csYear = csFromMonth.Right(4);
		csYearMonth = csYear + csMonth;
		strcpy(prlBudData->Frmo, csYearMonth.GetBuffer(0));
		//Jahr/Monat bis
		csMonth = csToMonth.Left(2);
		csYear = csToMonth.Right(4);
		csYearMonth = csYear + csMonth;
		strcpy(prlBudData->Tomo, csYearMonth.GetBuffer(0));
		//Headcount
		strcpy(prlBudData->Hdco, csHeadcount.GetBuffer(0));
		//Mutation
		strcpy(prlBudData->Muta, csMutation.GetBuffer(0));
		//Forecast
		strcpy(prlBudData->Foca, csForecast.GetBuffer(0));
		//Budget
		strcpy(prlBudData->Budg, csBudget.GetBuffer(0));
		//Remarks
		strcpy(prlBudData->Rema, csRemark.GetBuffer(0));
		//Home Airport
		strcpy(prlBudData->Hopo,pcgHome);
	
		if (m_new == true){
			// �nderungsflag setzen
			prlBudData->IsChanged = DATA_NEW;
			// Erzeugungsdatum einstellen
			prlBudData->Cdat = COleDateTime::GetCurrentTime();
			// Anwender (Ersteller) einstellen
			strcpy(prlBudData->Usec,pcgUser);
			//n�chste freie Datensatz-Urno ermitteln und speichern
			ilNewBudUrno = ogBasicData.GetNextUrno();
			prlBudData->Urno = ilNewBudUrno;
			// Datensatz einf�gen, wenn gew�nscht (ohne BC!!!)
			ogBudData.Insert(prlBudData);
		}
		else {
			// �nderungsflag setzen
			prlBudData->IsChanged = DATA_CHANGED;
			// Erzeugungsdatum einstellen
			//prlBudData->Cdat = pBud->Cdat;
			// Anwender (Ersteller) einstellen
			//strcpy(prlBudData->Usec, pBud->Usec);
			// �nderungsdatum einstellen
			prlBudData->Lstu = COleDateTime::GetCurrentTime();
			// Anwender (�nderung) einstellen
			strcpy(prlBudData->Useu,pcgUser);
			// Urno
			//prlBudData->Urno = m_urno;
			// Datensatz in updaten, wenn gew�nscht (ohne BC!!!)
			ogBudData.Update(prlBudData);
		}
		
		for (i=0; i<ilRowCount; i++){
			if(m_new == true){
				sDpt1 = pomData->GetValueRowCol(i+1,1);
				if (sDpt1.GetLength() > 0){
					prlUbuData = new UBUDATA;
					// �nderungsflag setzen
					prlUbuData->IsChanged = DATA_NEW;
					//Home Airport
					strcpy(prlUbuData->Hopo,pcgHome);
					//n�chste freie Datensatz-Urno ermitteln und speichern
					prlUbuData->Urno = ogBasicData.GetNextUrno();
					//Urno BUDTAB
					prlUbuData->Ubud = ilNewBudUrno;
					//Organistationseinheit
					strcpy(prlUbuData->Dpt1, sDpt1.GetBuffer(0));
					//Mit untergeordneten Einheiten
					sStar = pomData->GetValueRowCol(i+1, 2);
					strcpy(prlUbuData->Star, sStar.GetBuffer(0));
					// Datensatz einf�gen, wenn gew�nscht (ohne BC!!!)
					ogUbuData.Insert(prlUbuData);
				}
			}
			else{
				lpUrno = 0;
				pomData->ComposeStyleRowCol(i+1,1, &olStyle );
				lpUrno = (long) olStyle.GetItemDataPtr();
				if (lpUrno){
					prlUbuData = ogUbuData.GetUbuByUrno(lpUrno);
					sDpt1 = pomData->GetValueRowCol(i+1,1);
					if (sDpt1.GetLength() > 0){
						// �nderungsflag setzen
						prlUbuData->IsChanged = DATA_CHANGED;
						//Organistationseinheit
						strcpy(prlUbuData->Dpt1, sDpt1.GetBuffer(0));
						//Mit untergeordneten Einheiten
						sStar = pomData->GetValueRowCol(i+1, 2);
						strcpy(prlUbuData->Star, sStar.GetBuffer(0));
						// Datensatz updaten, wenn gew�nscht (ohne BC!!!)
						ogUbuData.Update(prlUbuData);
					}
					/*else
						ogUbuData.Delete(prlUbuData);*/
				}
				else{
					sDpt1 = pomData->GetValueRowCol(i+1,1);
					if (sDpt1.GetLength() > 0){
						prlUbuData = new UBUDATA;
						// �nderungsflag setzen
						prlUbuData->IsChanged = DATA_NEW;
						//Home Airport
						strcpy(prlUbuData->Hopo,pcgHome);
						//n�chste freie Datensatz-Urno ermitteln und speichern
						prlUbuData->Urno = ogBasicData.GetNextUrno();
						//Urno BUDTAB
						prlUbuData->Ubud = m_urno;
						//Organistationseinheit
						strcpy(prlUbuData->Dpt1, sDpt1.GetBuffer(0));
						//Mit untergeordneten Einheiten
						sStar = pomData->GetValueRowCol(i+1, 2);
						strcpy(prlUbuData->Star, sStar.GetBuffer(0));
						// Datensatz einf�gen, wenn gew�nscht (ohne BC!!!)
						ogUbuData.Insert(prlUbuData);
					}
				}
			}
		}
	}

	CDialog::OnOK();
CCS_CATCH_ALL
}

void CBudgetDataDlg::OnInfo()
{
	CString olString = LoadStg(IDS_STRING1015) + "\t" + m_createdonday + " " + m_createdonhour +"\n" +
					   LoadStg(IDS_STRING1016) + "\t" + m_createdbyuser +"\n" +
					   LoadStg(IDS_STRING1017) + "\t" + m_changedonday + " " + m_changedonhour +"\n" +
					   LoadStg(IDS_STRING1018) + "\t" + m_changedbyuser;
	MessageBox(olString,"Budget",MB_ICONINFORMATION);
}


void CBudgetDataDlg::OnOrgDelete() 
{
CCS_TRY
	// TODO: Add your command handler code here
	long ilUrno;
	CGXStyle style;
	ROWCOL nRow, nCol, ilRowCount;
	
	if (pomData->GetCurrentCell(nRow, nCol)){
		pomData->ComposeStyleRowCol(nRow, 1, &style );
		// Urno erhalten
		ilUrno = (long)style.GetItemDataPtr();
		if (ilUrno){
			//L�schen Satz
			UBUDATA *pUbu = ogUbuData.GetUbuByUrno(ilUrno);
			ogUbuData.Delete(pUbu);
		}
		pomData->RemoveRows(nRow, nRow);
		ilRowCount = pomData->GetRowCount();
		if (ilRowCount == 0)
			pomData->InsertRows(ilRowCount+1, 1);
	}
CCS_CATCH_ALL
}
