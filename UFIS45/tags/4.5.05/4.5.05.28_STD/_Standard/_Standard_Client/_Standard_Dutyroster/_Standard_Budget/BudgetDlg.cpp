// BudgetDlg.cpp : implementation file
//

#include <stdafx.h>
#include <Budget.h>
#include <BudgetDlg.h>
#include <BasicData.h>
#include <BudgetDataDlg.h>
#include <CedaBudData.h>
#include <CedaUbuData.h>
#include <CedaOrgData.h>
#include <CedaHdcData.h>
#include <CedaUhdData.h>
#include <CedaScoData.h>
#include <CedaSorData.h>
#include <CedaStfData.h>
#include <CedaCotData.h>
#include <CedaMutData.h>
#include <CedaUmuData.h>
#include <CedaForData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBudgetDlg dialog

CBudgetDlg::CBudgetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CBudgetDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CBudgetDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

CBudgetDlg::~CBudgetDlg()
{
	if(pomOrg != NULL)
		delete pomOrg;
}

void CBudgetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBudgetDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CBudgetDlg, CDialog)
	//{{AFX_MSG_MAP(CBudgetDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_COMMAND(ID_ROW_NEW, OnRowNew)
	ON_COMMAND(ID_ROW_EDIT, OnRowEdit)
	ON_COMMAND(ID_ROW_DELETE, OnRowDelete)
	ON_COMMAND(ID_ROW_CALCULATE, OnRowCalculate)
	ON_BN_CLICKED(ID_HELP, OnHelp)
	ON_COMMAND(ID_ROW_COPY, OnRowCopy)
	ON_WM_LBUTTONDBLCLK()
	ON_WM_RBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBudgetDlg message handlers

BOOL CBudgetDlg::OnInitDialog()
{
CCS_TRY
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	SetDlgItemText(IDOK, LoadStg(ID_OK));
	SetDlgItemText(IDCANCEL, LoadStg(ID_CANCEL));

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	
	//Grid initialisieren
	IniGrid();

	//Org. Units sortieren
	ogOrgData.SortOrgs();
	
	return TRUE;  // return TRUE  unless you set the focus to a control
CCS_CATCH_ALL
	return TRUE;
}

void CBudgetDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
CCS_TRY
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
CCS_CATCH_ALL
}

void CBudgetDlg::OnDestroy()
{
	WinHelp(0L, HELP_QUIT);
	CDialog::OnDestroy();
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CBudgetDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
	
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CBudgetDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CBudgetDlg::IniGrid()
{
CCS_TRY
	CString		csMonth, csYear, csOrgUnits, csStar;
	long		ilUrno;

	pomOrg = new CGridControl(this, IDC_ORG_GRID, 9, __max(ogBudData.omData.GetSize(), MINROWS));
	
	//�berschriften setzten
	pomOrg->SetValueRange(CGXRange(0,1), LoadStg(IDS_STRING1002));
	pomOrg->SetValueRange(CGXRange(0,2), LoadStg(IDS_STRING1003));
	pomOrg->SetValueRange(CGXRange(0,3), LoadStg(IDS_STRING1004));
	pomOrg->SetValueRange(CGXRange(0,4), LoadStg(IDS_STRING1005));
	pomOrg->SetValueRange(CGXRange(0,5), LoadStg(IDS_STRING1006));
	pomOrg->SetValueRange(CGXRange(0,6), LoadStg(IDS_STRING1007));
	pomOrg->SetValueRange(CGXRange(0,7), LoadStg(IDS_STRING1008));
	pomOrg->SetValueRange(CGXRange(0,8), LoadStg(IDS_STRING1026));
	pomOrg->SetValueRange(CGXRange(0,9), LoadStg(IDS_STRING1010));

	// Grid mit Daten f�llen.
	pomOrg->LockUpdate(true);

	for ( int i=0; i<ogBudData.omData.GetSize(); i++ )
	{
		// Urno auslesen
		ilUrno = ogBudData.omData[i].Urno;
		// Urno mit erstem Feld koppeln.						
		pomOrg->SetStyleRange ( CGXRange(i+1,1), CGXStyle().SetItemDataPtr( (void*)ilUrno ) );

		//Stichtag
		pomOrg->SetValueRange(CGXRange(i+1,1), ogBudData.omData[i].Dayx);
		//von Jahr/Monat
		csMonth = ogBudData.omData[i].Frmo;
		csMonth = csMonth.Right(2);
		csYear = ogBudData.omData[i].Frmo;
		csYear = csYear.Left(4);
		if (csMonth.GetLength())
			pomOrg->SetValueRange(CGXRange(i+1,2), csMonth + csYear); //"/" + csYear);
		//bis Jahr/Monat
		csMonth = ogBudData.omData[i].Tomo;
		csMonth = csMonth.Right(2);
		csYear = ogBudData.omData[i].Tomo;
		csYear = csYear.Left(4);
		if (csMonth.GetLength())
			pomOrg->SetValueRange(CGXRange(i+1,3), csMonth + csYear); //"/" + csYear);
		//Headcount
		pomOrg->SetValueRange(CGXRange(i+1,4), ogBudData.omData[i].Hdco);
		//Mutation
		pomOrg->SetValueRange(CGXRange(i+1,5), ogBudData.omData[i].Muta);
		//Forecast
		pomOrg->SetValueRange(CGXRange(i+1,6), ogBudData.omData[i].Foca);
		//Budget
		pomOrg->SetValueRange(CGXRange(i+1,7), ogBudData.omData[i].Budg);

		csOrgUnits = "";

		//Org. Units
		for (int m=0; m<ogUbuData.omData.GetSize(); m++){
			if (ogUbuData.omData[m].Ubud == ilUrno){
				csOrgUnits = csOrgUnits + ogUbuData.omData[m].Dpt1;
				csStar = ogUbuData.omData[m].Star;
				if ( csStar == '1')
					csOrgUnits = csOrgUnits + "*";
				csOrgUnits = csOrgUnits + ",";
			}
		}
		
		if (csOrgUnits.Right(1) == ',')
			csOrgUnits = csOrgUnits.Left(csOrgUnits.GetLength() - 1);

		pomOrg->SetValueRange(CGXRange(i+1,8), csOrgUnits);
		
		//Remarks
		pomOrg->SetValueRange(CGXRange(i+1,9), ogBudData.omData[i].Rema);
		
	}

	//pomOrg->SetStyleRange(CGXRange().SetTable(), CGXStyle().SetBorders(gxBorderTop, CGXPen().SetWidth(2).SetStyle(PS_SOLID).SetColor(RGB(0,0,0))));
	
	// Anzeige einstellen
	pomOrg->LockUpdate(false);
	pomOrg->Redraw();


	//Spaltenbreite und Zeilenh�he k�nnen nicht ver�ndert werden
	pomOrg->GetParam()->EnableTrackColWidth(FALSE);
	pomOrg->GetParam()->EnableTrackRowHeight(FALSE);
    pomOrg->GetParam()->EnableSelection(FALSE);

	//Scrollbars anzeigen wenn n�tig
	pomOrg->SetScrollBarMode(SB_BOTH, gxnAutomatic, TRUE);

	pomOrg->SetStyleRange( CGXRange().SetTable(), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetReadOnly(TRUE)
				.SetControl(GX_IDS_CTRL_STATIC));
		
	//Keine Zeilennummerierung
	pomOrg->HideCols(0, 0);
	pomOrg->SetColWidth(1, 1, pomOrg->Width_LPtoDP(5 * GX_NXAVGWIDTH));
	pomOrg->SetColWidth(4, 7, pomOrg->Width_LPtoDP(3 * GX_NXAVGWIDTH));
	pomOrg->SetColWidth(8, 8, pomOrg->Width_LPtoDP(30 * GX_NXAVGWIDTH));
	pomOrg->SetColWidth(9, 9, pomOrg->Width_LPtoDP(30 * GX_NXAVGWIDTH));
	//pomOrg->HideCols(10, 13);

	//CheckBoxen in Spalten einf�gen
	for(i=4;i<8;i++)
		pomOrg->SetColCheckbox(i);

	pomOrg->EnableAutoGrow(false);
	//pomOrg->EnableSorting(false);
CCS_CATCH_ALL
}

void CBudgetDlg::OnRowNew() 
{
CCS_TRY
	//int ilDataCount;
	ROWCOL ilRowCount, ilDataCount;
	BOOL blRet;
	long ilUrno;
	CString	csMonth, csYear, csOrgUnits, csStar;

	// TODO: Add your command handler code here
	CBudgetDataDlg *dlg = new CBudgetDataDlg;
	if(dlg->DoModal() == IDOK){
		//CGridControl *pomOrg = (CGridControl*) GetDlgItem(IDC_ORG_GRID);
		ilDataCount = ogBudData.omData.GetSize();
		ilRowCount = pomOrg->GetRowCount();
		if (ilDataCount > ilRowCount || (ilDataCount == 1 && ilRowCount == 1)){
			blRet = pomOrg->InsertRows(ilRowCount+1, 1);
			pomOrg->CopyStyleOfLine (ilRowCount-1, ilRowCount, true );

			for ( int i=0; i<ogBudData.omData.GetSize(); i++ )
			{
				// Urno auslesen
				ilUrno = ogBudData.omData[i].Urno;
				if (pomOrg->FindRowByUrnoDataPointer(ilUrno) == -1){
					pomOrg->GetParam()->SetLockReadOnly(false);
					// Urno mit erstem Feld koppeln.						
					pomOrg->SetStyleRange ( CGXRange(ilRowCount+1,1), CGXStyle().SetItemDataPtr( (void*)ilUrno ) );
					//Stichtag
					pomOrg->SetValueRange(CGXRange(ilRowCount+1,1), ogBudData.omData[i].Dayx);
					//von Jahr/Monat
					csMonth = ogBudData.omData[i].Frmo;
					csMonth = csMonth.Right(2);
					csYear = ogBudData.omData[i].Frmo;
					csYear = csYear.Left(4);
					if (csMonth.GetLength())
						pomOrg->SetValueRange(CGXRange(ilRowCount+1,2), csMonth + csYear); //"/" + csYear);
						//bis Jahr/Monat
						csMonth = ogBudData.omData[i].Tomo;
						csMonth = csMonth.Right(2);
						csYear = ogBudData.omData[i].Tomo;
						csYear = csYear.Left(4);
						if (csMonth.GetLength())
							pomOrg->SetValueRange(CGXRange(ilRowCount+1,3), csMonth + csYear); //"/" + csYear);
						//Headcount
						pomOrg->SetValueRange(CGXRange(ilRowCount+1,4), ogBudData.omData[i].Hdco);
						//Mutation
						pomOrg->SetValueRange(CGXRange(ilRowCount+1,5), ogBudData.omData[i].Muta);
						//Forecast
						pomOrg->SetValueRange(CGXRange(ilRowCount+1,6), ogBudData.omData[i].Foca);
						//Budget
						pomOrg->SetValueRange(CGXRange(ilRowCount+1,7), ogBudData.omData[i].Budg);
	
						csOrgUnits = "";
	
						//Org. Units
						for (int m=0; m<ogUbuData.omData.GetSize(); m++){
							if (ogUbuData.omData[m].Ubud == ilUrno){
								csOrgUnits = csOrgUnits + ogUbuData.omData[m].Dpt1;
								csStar = ogUbuData.omData[m].Star;
								if ( csStar == '1')
									csOrgUnits = csOrgUnits + "*";
								csOrgUnits = csOrgUnits + ",";
							}
						}
		
						if (csOrgUnits.Right(1) == ',')
							csOrgUnits = csOrgUnits.Left(csOrgUnits.GetLength() - 1);

						pomOrg->SetValueRange(CGXRange(ilRowCount+1,8), csOrgUnits);
		
						//Remarks
						pomOrg->SetValueRange(CGXRange(ilRowCount+1,9), ogBudData.omData[i].Rema);
						if (ilDataCount == 1 && ilRowCount == 1)
							pomOrg->RemoveRows(1, 1);
						pomOrg->GetParam()->SetLockReadOnly(true);
						break;	
					}
				}
			}
		}
CCS_CATCH_ALL
}

void CBudgetDlg::OnRowEdit() 
{
	// TODO: Add your command handler code here
	EditRow();
}

void CBudgetDlg::OnRowDelete() 
{
CCS_TRY
	// TODO: Add your command handler code here
	long ilUrno, ilUbuUrno;
	CGXStyle style;
	ROWCOL nRow, nCol, ilRowCount;
	
	if (pomOrg->GetCurrentCell(nRow, nCol)){
		pomOrg->ComposeStyleRowCol(nRow, 1, &style );
		// Urno erhalten
		ilUrno = (long)style.GetItemDataPtr();
		if (ilUrno){
			//L�schen BUD-Satz
			BUDDATA *pBud = ogBudData.GetBudByUrno(ilUrno);
			ogBudData.Delete(pBud);
			pomOrg->RemoveRows(nRow, nRow);
			
			//L�schen UBU-S�tze
			for (int m=ogUbuData.omData.GetSize()-1; m>=0; m--){
				if (ogUbuData.omData[m].Ubud == ilUrno){

					ilUbuUrno = ogUbuData.omData[m].Urno;	
					UBUDATA *pUbu = ogUbuData.GetUbuByUrno(ilUbuUrno);
					ogUbuData.Delete(pUbu);
				}
			}

			//L�schen HDC+UHD-S�tze
			DeleteHdcUhd(ilUrno);

			//L�schen MUT+UMU-S�tze
			DeleteMutUmu(ilUrno);

			//L�schen FOR-S�tze
			DeleteFor(ilUrno);

			ilRowCount = pomOrg->GetRowCount();
			if (ilRowCount == 0)
				pomOrg->InsertRows(ilRowCount+1, 1);
		}
	}
CCS_CATCH_ALL
}

void CBudgetDlg::OnRowCalculate() 
{
CCS_TRY
	long ilUrno;
	ilUrno = GetUrno();
	if ( ilUrno ){
		BUDDATA *pBud = ogBudData.GetBudByUrno(ilUrno);
		if (pBud->Hdco == CString("1")){
			//Headcount
			GetHeadcount(pBud);
		}

		if (pBud->Muta == CString("1")){
			//Mutation
			GetMutation(pBud);
		}

		if (pBud->Foca == CString("1")){
			//Forecast
			if (pBud->Hdco == CString("0"))
				GetHeadcount(pBud);
			GetForecast(pBud);
		}

		if (pBud->Budg == CString("1")){
			//Budget

		}
	}
CCS_CATCH_ALL
}

long CBudgetDlg::GetUrno()
{
CCS_TRY
	long ilUrno;
	CGXStyle style;
	ROWCOL nRow, nCol;
	if (pomOrg->GetCurrentCell(nRow, nCol)){
		pomOrg->ComposeStyleRowCol(nRow, 1, &style );
		// Urno erhalten
		ilUrno = (long)style.GetItemDataPtr();
		if ( ilUrno )
			return ilUrno;	
		else
			return 0;

	}
	else
		return 0;
CCS_CATCH_ALL
	return 0;
}


void CBudgetDlg::OnHelp() 
{
	// TODO: Add your control notification handler code here
	
}

void CBudgetDlg::OnRowCopy() 
{
CCS_TRY
	// TODO: Add your command handler code here
	long ilUrno, ilNewUrno;
	ROWCOL row;
	BUDDATA *pNewBud;
	UBUDATA *pNewUbu;

	ilUrno = GetUrno();
	if ( ilUrno ){
		row = pomOrg->FindRowByUrnoDataPointer(ilUrno);
		if (row != -1){
			pomOrg->GetParam()->SetLockReadOnly(false);
			BUDDATA *pBud = ogBudData.GetBudByUrno(ilUrno);
			pNewBud = new BUDDATA;
			pNewBud->IsChanged = DATA_NEW;
			// Erzeugungsdatum einstellen
			pNewBud->Cdat = COleDateTime::GetCurrentTime();
			// Anwender (Ersteller) einstellen
			strcpy(pNewBud->Usec,pcgUser);
			//n�chste freie Datensatz-Urno ermitteln und speichern
			ilNewUrno = ogBasicData.GetNextUrno();
			pNewBud->Urno = ilNewUrno;
			//Stichtag
			strcpy(pNewBud->Dayx, pBud->Dayx);
			//Jahr/Monat von
			strcpy(pNewBud->Frmo, pBud->Frmo);
			//Jahr/Monat bis
			strcpy(pNewBud->Tomo, pBud->Tomo);
			//Headcount
			strcpy(pNewBud->Hdco, pBud->Hdco);
			//Mutation
			strcpy(pNewBud->Muta, pBud->Muta);
			//Forecast
			strcpy(pNewBud->Foca, pBud->Foca);
			//Budget
			strcpy(pNewBud->Budg, pBud->Budg);
			//Remarks
			strcpy(pNewBud->Rema, pBud->Rema);
			//Hopo
			strcpy(pNewBud->Hopo, pcgHome);
			// Datensatz in einf�gen, wenn gew�nscht (ohne BC!!!)
			ogBudData.Insert(pNewBud);
		
			//Org. Units
			for (int m=0; m<ogUbuData.omData.GetSize(); m++){
				if (ogUbuData.omData[m].Ubud == ilUrno){
					pNewUbu = new UBUDATA;
					pNewUbu->IsChanged = DATA_NEW;
					//Urno
					pNewUbu->Urno = ogBasicData.GetNextUrno();
					//Urno Budtab
					pNewUbu->Ubud = ilNewUrno;					
					//Organisationseinheit
					strcpy(pNewUbu->Dpt1, ogUbuData.omData[m].Dpt1);
					//Mit untergeordneten Einheiten
					strcpy(pNewUbu->Star, ogUbuData.omData[m].Star);
					//Hopo
					strcpy(pNewUbu->Hopo, pcgHome);
					ogUbuData.Insert(pNewUbu);
				}
			}

			//Zeile einf�gen und Inhalt kopieren
			BOOL blRet = pomOrg->InsertRows(row+1, 1);
			
			// Urno mit erstem Feld koppeln.						
			pomOrg->SetStyleRange(CGXRange(row+1,1), CGXStyle().SetItemDataPtr((void*)ilNewUrno));
			
			pomOrg->SetValueRange(CGXRange(row+1,1), pomOrg->GetValueRowCol(row, 1));
			pomOrg->SetValueRange(CGXRange(row+1,2), pomOrg->GetValueRowCol(row, 2));
			pomOrg->SetValueRange(CGXRange(row+1,3), pomOrg->GetValueRowCol(row, 3));
			pomOrg->SetValueRange(CGXRange(row+1,4), pomOrg->GetValueRowCol(row, 4));
			pomOrg->SetValueRange(CGXRange(row+1,5), pomOrg->GetValueRowCol(row, 5));
			pomOrg->SetValueRange(CGXRange(row+1,6), pomOrg->GetValueRowCol(row, 6));
			pomOrg->SetValueRange(CGXRange(row+1,7), pomOrg->GetValueRowCol(row, 7));
			pomOrg->SetValueRange(CGXRange(row+1,8), pomOrg->GetValueRowCol(row, 8));
			pomOrg->SetValueRange(CGXRange(row+1,9), pomOrg->GetValueRowCol(row, 9));

			pomOrg->GetParam()->SetLockReadOnly(true);
		}
	}

CCS_CATCH_ALL
}

void CBudgetDlg::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	//if(nFlags == 0)
	//	EditRow();
	CDialog::OnLButtonDblClk(nFlags, point);
}

void CBudgetDlg::EditRow()
{
CCS_TRY
	long ilUrno;
	ROWCOL row;
	CString		csMonth, csYear, csOrgUnits, csStar;

	ilUrno = GetUrno();
	if ( ilUrno ){
		CBudgetDataDlg *dlg = new CBudgetDataDlg(ilUrno);
		if(dlg->DoModal() == IDOK){
			row = pomOrg->FindRowByUrnoDataPointer(ilUrno);
			if (row != -1){
				pomOrg->GetParam()->SetLockReadOnly(false);

				BUDDATA *pBud = ogBudData.GetBudByUrno(ilUrno);
				//Stichtag
				pomOrg->SetValueRange(CGXRange(row,1), pBud->Dayx);
				//von Jahr/Monat
				csMonth = pBud->Frmo;
				csMonth = csMonth.Right(2);
				csYear = pBud->Frmo;
				csYear = csYear.Left(4);
				if (csMonth.GetLength())
					pomOrg->SetValueRange(CGXRange(row,2), csMonth + csYear); //"/" + csYear);
				//bis Jahr/Monat
				csMonth = pBud->Tomo;
				csMonth = csMonth.Right(2);
				csYear = pBud->Tomo;
				csYear = csYear.Left(4);
				if (csMonth.GetLength())
					pomOrg->SetValueRange(CGXRange(row,3), csMonth + csYear); //"/" + csYear);
				//Headcount
				pomOrg->SetValueRange(CGXRange(row,4), pBud->Hdco);
				//Mutation
				pomOrg->SetValueRange(CGXRange(row,5), pBud->Muta);
				//Forecast
				pomOrg->SetValueRange(CGXRange(row,6), pBud->Foca);
				//Budget
				pomOrg->SetValueRange(CGXRange(row,7), pBud->Budg);
	
				csOrgUnits = "";
				//Org. Units
				for (int m=0; m<ogUbuData.omData.GetSize(); m++){
					if (ogUbuData.omData[m].Ubud == ilUrno){
						csOrgUnits = csOrgUnits + ogUbuData.omData[m].Dpt1;
						csStar = ogUbuData.omData[m].Star;
						if ( csStar == '1')
							csOrgUnits = csOrgUnits + "*";
						csOrgUnits = csOrgUnits + ",";
					}
				}
		
				if (csOrgUnits.Right(1) == ',')
					csOrgUnits = csOrgUnits.Left(csOrgUnits.GetLength() - 1);

				pomOrg->SetValueRange(CGXRange(row,8), csOrgUnits);
		
				//Remarks
				pomOrg->SetValueRange(CGXRange(row,9), pBud->Rema);
				
				pomOrg->GetParam()->SetLockReadOnly(true);
			}
		}
	}
CCS_CATCH_ALL
}


void CBudgetDlg::OnRButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	//Popup-Men� anzeigen
	/*CMenu menu;
	long ilUrno;
	ROWCOL nRow, nCol;
	CGXStyle style;
	
	if (nFlags == 0){ 
		nFlags = MK_RBUTTON;
		VERIFY(menu.LoadMenu(IDR_MENU1));
		CMenu* pPopup = menu.GetSubMenu( 0 );
		ASSERT( pPopup != NULL );
    
		CWnd* pWndPopupOwner = AfxGetMainWnd( ); // display the menuClientToScreen(&pt);

		if (pPopup != NULL)
		{
			pPopup->ModifyMenu(0,MF_BYPOSITION|MF_STRING,ID_ROW_NEW,LoadStg(IDS_STRING1011));
			pPopup->ModifyMenu(1,MF_BYPOSITION|MF_STRING,ID_ROW_EDIT,LoadStg(IDS_STRING1012));
			pPopup->ModifyMenu(2,MF_BYPOSITION|MF_STRING,ID_ROW_COPY,LoadStg(IDS_STRING1030));
			pPopup->ModifyMenu(3,MF_BYPOSITION|MF_STRING,ID_ROW_DELETE,LoadStg(IDS_STRING1013));
			pPopup->ModifyMenu(4,MF_BYPOSITION|MF_STRING,ID_ROW_CALCULATE,LoadStg(IDS_STRING1014));

			pPopup->EnableMenuItem(ID_ROW_EDIT, MF_DISABLED||MF_GRAYED);
			pPopup->EnableMenuItem(ID_ROW_COPY, MF_DISABLED||MF_GRAYED);
			pPopup->EnableMenuItem(ID_ROW_DELETE, MF_DISABLED||MF_GRAYED);
			pPopup->EnableMenuItem(ID_ROW_CALCULATE, MF_DISABLED||MF_GRAYED);

			CGridControl *pgrid = (CGridControl*) GetDlgItem(IDC_ORG_GRID);
			if (pgrid->GetCurrentCell(nRow, nCol)){
				pgrid->ComposeStyleRowCol(nRow, 1, &style );
				// Urno erhalten
				ilUrno = (long)style.GetItemDataPtr();
				if ( ilUrno ){
					pPopup->EnableMenuItem(ID_ROW_EDIT, MF_ENABLED);
					pPopup->EnableMenuItem(ID_ROW_COPY, MF_ENABLED);
					pPopup->EnableMenuItem(ID_ROW_DELETE, MF_ENABLED);
					pPopup->EnableMenuItem(ID_ROW_CALCULATE, MF_ENABLED);
				}
			}
	
			pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, pWndPopupOwner);
		}
	}*/
	CDialog::OnRButtonDown(nFlags, point);
}


void CBudgetDlg::GetHeadcount(BUDDATA *pBud)
{
CCS_TRY
	long ilHdcUrno, ilStfUrno;
	int iYearFrom, iYearTo, iMonthFrom, iMonthTo, iYear, iMonth, iDay, iHeads, iStsc;
	
	COTDATA *pCot;
	
	char pclWhere[1024]="";
	char pclLeer[1024]="";

	CString csYear, csMonth, csDay, csYmdy, csFromYearMonth, csToYearMonth, csOrg;
	CString csStsc, csHead, csCode, csInbu, csWrkd, csZifi, csHeads;
	CString csWhpw, csNetw, csActu, csSumActu, csSumHeads;
		
	int iSumHeads;
	
	double dWhpw, dNetw, dWrkd, dActu, dSumActu;
	
	CCSPtrArray<SCODATA> olScoData;
	CCSPtrArray<SORDATA> olSorData;
	CCSPtrArray<ANZHEADS> uhdArray;

	SORDATA olSor;
	SCODATA olSco;
	ANZHEADS olHeads;
	
	bool bVorh;

	CStringArray *pOrgs = new CStringArray;

	// Sanduhr
	AfxGetApp()->DoWaitCursor(1);

	//HDCTAB+UHDTAB-S�tze l�schen
	DeleteHdcUhd(pBud->Urno);

	csDay = pBud->Dayx;
	csFromYearMonth = pBud->Frmo;
	csToYearMonth = pBud->Tomo;
	iYearFrom = atoi(csFromYearMonth.Left(4));
	iYearTo = atoi(csToYearMonth.Left(4));
	
	//Pro Monat ein Satz in HDCTAB anlegen
	for(int y=iYearFrom; y<=iYearTo; y++){
		if (y==iYearFrom)
			iMonthFrom = atoi(csFromYearMonth.Right(2));
		else
			iMonthFrom = 1;
		
		if (y==iYearTo)
			iMonthTo = atoi(csToYearMonth.Right(2));
		else
			iMonthTo = 12;

		csYear.Format("%i", y);
		
		for (int m=iMonthFrom; m<=iMonthTo; m++){
			csMonth.Format ("%.2i", m);
			csYmdy = csYear + csMonth + csDay;
			//Pro Org. Unit ein Satz in HDCTAB anlegen
			for (int i=0; i<ogUbuData.omData.GetSize(); i++){
				if (ogUbuData.omData[i].Ubud == pBud->Urno){
					NewHDC(CString(ogUbuData.omData[i].Dpt1), csYmdy, pBud->Urno);
					if (ogUbuData.omData[i].Star == CString("1")){
						//Untergeordnete Einheiten
						ogOrgData.GetDependantOrgs(pOrgs, CString(ogUbuData.omData[i].Dpt1));
						for (int x=0; x<pOrgs->GetSize(); x++){
							csOrg = pOrgs->GetAt(x);
							NewHDC(csOrg, csYmdy, pBud->Urno);
						}
					}
				}
			}
		}
	}
	
	//Alle Mitarbeiter in Org. Unit ermitteln
	for (int i=0; i<ogHdcData.omData.GetSize(); i++){
		if (ogHdcData.omData[i].Ubud == pBud->Urno){
			pOrgs->RemoveAll();
			csOrg = ogHdcData.omData[i].Dpt1;
			pOrgs->Add(csOrg);
			csYmdy = ogHdcData.omData[i].Ymdy;
			ilHdcUrno = ogHdcData.omData[i].Urno;
			iYear = atoi(csYmdy.Left(4));
			iMonth = atoi(csYmdy.Mid(4, 2));
			iDay = atoi(csYmdy.Right(2));
			COleDateTime ctYmdy(iYear, iMonth, iDay, 0, 0, 0);
			//ogSorData.ReadSorData();
			CString csStfUrnos = ogSorData.GetSorByOrgWithTime(pOrgs, ctYmdy, ctYmdy, &olSorData);
			//Arbeitsvertragsarten ermitteln
			for (int y=0; y<olSorData.GetSize(); y++){
				olSor = olSorData.GetAt(y);
				ilStfUrno = olSor.Surn;
				//ogScoData.ReadScoData();
				ogScoData.GetScoBySurnWithTime(ilStfUrno, ctYmdy, ctYmdy, &olScoData);
				for (int x=0; x<olScoData.GetSize(); x++){
					olSco = olScoData.GetAt(x);
					csStsc = olSco.Stsc;
					csCode = olSco.Code;
					csZifi = olSco.Zifi;
					pCot = ogCotData.GetCotByCtrc(csCode);
					csInbu = pCot->Inbu;
					csWrkd = pCot->Whpw;
					//In Budgetberechnung einbeziehen?
					if (csInbu == '1'){
						//Eintrag vorhanden ?
						bVorh = false;
						for (int m=uhdArray.GetSize()-1; m>=0; m--){
							olHeads = uhdArray.GetAt(m);
							if (csStsc.GetLength() == 0 || csStsc.GetLength() == 2){ //csStsc.Left(1) == CString("K") || csStsc.Left(1) == CString("T") ||
								//csStsc.Left(1) == CString("F")){
								if(olHeads.uhdc == ilHdcUrno && olHeads.whpw == olSco.Cweh &&
									olHeads.suph == csZifi && olHeads.stsc == csStsc &&
									olHeads.cot == csCode){
									//Anzahl Vertr�ge + 1
									iHeads = olHeads.heads + 1;
									uhdArray.RemoveAt(m);
									bVorh = true;
									break;
								}
							}
							else{
								if(olHeads.uhdc == ilHdcUrno && olHeads.stsc == csStsc){
									//Anzahl Vertr�ge + 1
									iHeads = olHeads.heads + 1;
									uhdArray.RemoveAt(m);
									bVorh = true;
									break;
								}
							}
						}

						//Neuer Eintrag
						ANZHEADS *pAnz = new ANZHEADS;
						pAnz->uhdc = ilHdcUrno;
						if (csStsc.GetLength() == 0 || csStsc.GetLength() == 2){ //csStsc.Left(1) == CString("K") || csStsc.Left(1) == CString("T") ||
							//csStsc.Left(1) == CString("F")){
							pAnz->whpw = olSco.Cweh;
							pAnz->suph = csZifi;
							pAnz->stsc = csStsc;
							pAnz->cot = csCode;
							pAnz->wrkd = csWrkd;
						}
						else{
							pAnz->whpw = CString("");
							pAnz->suph = CString("");
							pAnz->stsc = csStsc;
							pAnz->cot = CString("");
							pAnz->wrkd = CString("");
						}
						
						if(bVorh == false)
							pAnz->heads = 1;
						else
							pAnz->heads = iHeads;
						
						uhdArray.Add(pAnz);

					}
				}
			}
		}
	}
	
	for (int m=0; m<uhdArray.GetSize(); m++){
		olHeads = uhdArray.GetAt(m);
		//Heads
		iHeads = olHeads.heads;
		csHeads.Format("%i", iHeads);
		//Statusanhang
		csStsc = olHeads.stsc;
		
		
		if (csStsc.GetLength() == 0 || csStsc.GetLength() == 2){ //csStsc.Left(1) == CString("K") || csStsc.Left(1) == CString("T") ||
								//csStsc.Left(1) == CString("F")){
			
			//Wochenstunden
			csWhpw = olHeads.whpw;
			dWhpw = atof(csWhpw)/100;
			dNetw = dWhpw;

			//Netweek
			csNetw = csWhpw;

			//Zifi
			csZifi = '0';
			if(olHeads.suph == 'x'){
				csZifi = '1';
				dNetw = (dWhpw - 1);
				csNetw.Format("%f", dNetw * 100);
				csNetw = csNetw.Left(4);
			}
			
			//Umrechnung auf Personalmonate
			csWrkd = olHeads.wrkd;
			dWrkd = atof(csWrkd);
		
			dActu = dNetw / dWrkd * iHeads;
			
			if (csStsc.GetLength() == 2){
				iStsc = atoi(csStsc.Right(1));
				dActu = dActu * iStsc / 10;
			}
			
			csActu.Format("%.2f", dActu);
			
			NewUHD(csWhpw, csZifi, csNetw, csStsc, csHeads, csActu, olHeads.uhdc, CString(""), CString(""));
		}
		else
			NewUHD(CString(""), CString(""), CString(""), csStsc, csHeads, CString(""), olHeads.uhdc, CString(""), CString(""));

	}
	
	//Summe Headcount, Actual
	//Summe Budget, Diff fehlt noch
	
	for (y=0; y<ogHdcData.omData.GetSize(); y++){
		if(ogHdcData.omData[y].Ubud == pBud->Urno){
			dSumActu = 0.0;
			iSumHeads = 0;
			ilHdcUrno = ogHdcData.omData[y].Urno;
			for(i=0; i<ogUhdData.omData.GetSize(); i++){
				if(ogUhdData.omData[i].Uhdc == ilHdcUrno){
					csActu = ogUhdData.omData[i].Actu;
					if(csActu.GetLength() > 0){
						dActu = atof(csActu);
						dSumActu = dSumActu + dActu;
						iHeads = atoi(ogUhdData.omData[i].Head);
						iSumHeads = iSumHeads + iHeads;
					}
				}
			}
			csSumActu.Format("%.2f", dSumActu);
			csSumHeads.Format("%i", iSumHeads);
			NewUHD(CString(""), CString(""), CString(""), CString(""), csSumHeads, csSumActu, ilHdcUrno, CString(""), CString(""));
		}
	}
		
	//ogSorData.ReadSorData();
	//ogScoData.ReadScoData();
	
	// Sanduhr
	AfxGetApp()->DoWaitCursor(-1);
CCS_CATCH_ALL
}

void CBudgetDlg::NewHDC(CString Dpt1, CString Ymdy, long Ubud)
{
CCS_TRY
	HDCDATA *pHdc = new HDCDATA;
	// �nderungsflag setzen
	pHdc->IsChanged = DATA_NEW;
	// Erzeugungsdatum einstellen
	pHdc->Cdat = COleDateTime::GetCurrentTime();
	// Anwender (Ersteller) einstellen
	strcpy(pHdc->Usec,pcgUser);
	//n�chste freie Datensatz-Urno ermitteln und speichern
	pHdc->Urno = ogBasicData.GetNextUrno();
	//Home Airport
	strcpy(pHdc->Hopo,pcgHome);
	//Urno BUDTAB
	pHdc->Ubud = Ubud;
	//Organisationseinheit
	strcpy(pHdc->Dpt1, Dpt1.GetBuffer(0));
	//Jahr/Monat/Stichtag
	strcpy(pHdc->Ymdy, Ymdy.GetBuffer(0));
	// Datensatz einf�gen, wenn gew�nscht (ohne BC!!!)
	ogHdcData.Insert(pHdc);

CCS_CATCH_ALL
}


void CBudgetDlg::NewUHD(CString Whpw, CString Suph, CString Netw, CString Stsc, CString Head, CString Actu, long Uhdc, CString Budg, CString Diff)
{
CCS_TRY
	UHDDATA *pUhd = new UHDDATA;
	// �nderungsflag setzen
	pUhd->IsChanged = DATA_NEW;
	//n�chste freie Datensatz-Urno ermitteln und speichern
	pUhd->Urno = ogBasicData.GetNextUrno();
	//Home Airport
	strcpy(pUhd->Hopo,pcgHome);
	//Urno HDCTAB
	pUhd->Uhdc = Uhdc;
	//Wochenstunden
	strcpy(pUhd->Whpw, Whpw.GetBuffer(0));
	//Zus�tzliche Ferien
	strcpy(pUhd->Suph, Suph.GetBuffer(0));
	//Netweek
	strcpy(pUhd->Netw, Netw.GetBuffer(0));
	//Statusanhang
	strcpy(pUhd->Stsc, Stsc.GetBuffer(0));
	//Anzahl Vertr�ge
	strcpy(pUhd->Head, Head.GetBuffer(0));
	//Umrechnung auf Personalmonate
	strcpy(pUhd->Actu, Actu.GetBuffer(0));
	//Budget
	//strcpy(pUhd->Budg, Budg.GetBuffer(0));
	//Differenz
	//strcpy(pUhd->Diff, Diff.GetBuffer(0));
	// Datensatz einf�gen, wenn gew�nscht (ohne BC!!!)
	ogUhdData.Insert(pUhd);

CCS_CATCH_ALL
}

void CBudgetDlg::GetMutation(BUDDATA *pBud)
{
CCS_TRY
	int iYearFrom, iYearTo, iMonthFrom, iMonthTo;
	long ilMutUrno, ilStfUrno;
	
	COleDateTime ctPefr, ctPeto, ctEntrance;
	
	CCSPtrArray<SORDATA> olSorData;
	CCSPtrArray<SORDATA> olSurnData;
	CCSPtrArray<SCODATA> olScoData;
	
	SORDATA olSor;
	SCODATA olSco, olScoAlt, olScoNeu;
	
	CString csCode, csWrkd, csLastName, csFirstName, csWhpw, csNetw, csEntrance, retValue;
	CString csNewOrg, csZifiAlt, csZifiNeu, csCodeAlt, csCodeNeu, csCwehAlt, csCwehNeu, csStscAlt, csStscNeu;
	CString csYear, csMonth, csDay, csYmdy, csFromYearMonth, csToYearMonth, csOrg, csZifi;
	CString csStfUrno;

	double dNetw, dWhpw;
	
	STFDATA *pStf;
	
	CMapStringToString  olEmplMap, mapOldOrg;
	CMapStringToOb mapNewOrg;

	CStringArray *pOrgs = new CStringArray;
	
	NEWORG *pOrg;

	// Sanduhr
	AfxGetApp()->DoWaitCursor(1);

	//MUTTAB+UMUTAB-S�tze l�schen
	DeleteMutUmu(pBud->Urno);
	
	csDay = pBud->Dayx;
	csFromYearMonth = pBud->Frmo;
	csToYearMonth = pBud->Tomo;
	iYearFrom = atoi(csFromYearMonth.Left(4));
	iYearTo = atoi(csToYearMonth.Left(4));
	
	//Pro Monat ein Satz in MUTTAB anlegen
	for(int y=iYearFrom; y<=iYearTo; y++){
		if (y==iYearFrom)
			iMonthFrom = atoi(csFromYearMonth.Right(2));
		else
			iMonthFrom = 1;
		
		if (y==iYearTo)
			iMonthTo = atoi(csToYearMonth.Right(2));
		else
			iMonthTo = 12;

		csYear.Format("%i", y);
		
		for (int m=iMonthFrom; m<=iMonthTo; m++){
			csMonth.Format ("%.2i", m);
			csYmdy = csYear + csMonth + csDay;
			//Pro Org. Unit ein Satz in MUTTAB anlegen
			for (int i=0; i<ogUbuData.omData.GetSize(); i++){
				if (ogUbuData.omData[i].Ubud == pBud->Urno){
					NewMUT(CString(ogUbuData.omData[i].Dpt1), csYmdy, pBud->Urno);
					if (ogUbuData.omData[i].Star == CString("1")){
						//Untergeordnete Einheiten
						ogOrgData.GetDependantOrgs(pOrgs, CString(ogUbuData.omData[i].Dpt1));
						for (int x=0; x<pOrgs->GetSize(); x++){
							csOrg = pOrgs->GetAt(x);
							NewMUT(csOrg, csYmdy, pBud->Urno);
						}
					}
				}
			}
		}
	}

	//Alle Mitarbeiter in Org. Unit ermitteln
	for (int i=0; i<ogMutData.omData.GetSize(); i++){
		if (ogMutData.omData[i].Ubud == pBud->Urno){
			//olEmplMap.RemoveAll();
			pOrgs->RemoveAll();
			csOrg = ogMutData.omData[i].Dpt1;
			pOrgs->Add(csOrg);
			ctPefr = ogMutData.omData[i].Pefr;
			ctPeto = ogMutData.omData[i].Peto;
			ilMutUrno = ogMutData.omData[i].Urno;

			//Eintritte
			//ogSorData.ReadSorData();
			CString csStfUrnos = ogSorData.GetEintritteByOrgWithTime(pOrgs, ctPefr, ctPeto, &olSorData, &mapOldOrg);
			for (int y=0; y<olSorData.GetSize(); y++){
				olSor = olSorData.GetAt(y);
				ilStfUrno = olSor.Surn;
				ctEntrance = olSor.Vpfr;
				csEntrance.Format("%i%.2i%.2i", ctEntrance.GetYear(), ctEntrance.GetMonth(), ctEntrance.GetDay());
				pStf = ogStfData.GetStfByUrno(ilStfUrno);
				csLastName = pStf->Lanm;
				csFirstName = pStf->Finm;
				ogScoData.GetScoBySurnWithTime(ilStfUrno, ctPefr, ctPeto, &olScoData);
				if (olScoData.GetSize() >= 1){
					olSco = olScoData.GetAt(olScoData.GetSize() - 1);
					csZifi = olSco.Zifi;
					csWhpw = olSco.Cweh;
					dWhpw = atof(csWhpw)/100;
					csNetw = csWhpw;
							
					if (csZifi == 'x'){
						dNetw = (dWhpw - 1);
						csNetw.Format("%f", dNetw * 100);
						csNetw = csNetw.Left(4);
					}
						
				}

				//olEmplMap.SetAt(CString(ilStfUrno), csLastName);

				csNewOrg = "";
				csStfUrno.Format("%d", ilStfUrno);
				mapOldOrg.Lookup(csStfUrno, csNewOrg);
				NewUMU(csNetw, csLastName, csFirstName, csEntrance, CString(""), csNewOrg, 
					   CString(""),	CString(""), CString(""), ilMutUrno);
			}
			
			//Austritte
			//ogSorData.ReadSorData();
			csNewOrg = "";
			csStfUrnos = ogSorData.GetAustritteByOrgWithTime(pOrgs, ctPefr, ctPeto, &olSorData, &mapNewOrg);
			for (y=0; y<olSorData.GetSize(); y++){
				olSor = olSorData.GetAt(y);
				ilStfUrno = olSor.Surn;
				pStf = ogStfData.GetStfByUrno(ilStfUrno);
				csLastName = pStf->Lanm;
				csFirstName = pStf->Finm;
				ogScoData.GetScoBySurnWithTime(ilStfUrno, ctPefr, ctPeto, &olScoData);
				if (olScoData.GetSize() >= 1){
					olSco = olScoData.GetAt(olScoData.GetSize() - 1);
					csZifi = olSco.Zifi;
					csWhpw = olSco.Cweh;
					dWhpw = atof(csWhpw)/100;
					csNetw = csWhpw;
							
					if (csZifi == 'x'){
						dNetw = (dWhpw - 1);
						csNetw.Format("%f", dNetw * 100);
						csNetw = csNetw.Left(4);
					}
						
				}

				//olEmplMap.SetAt(CString(ilStfUrno), csLastName);
				
				csNewOrg = "";
				csEntrance = "";
				csStfUrno.Format("%d", ilStfUrno);
				if (mapNewOrg.Lookup(csStfUrno, (CObject *&)pOrg)){
					csNewOrg = pOrg->Org;
					csEntrance = pOrg->Vpfr;
				}
				NewUMU(csNetw, csLastName, csFirstName, CString(""), csEntrance, CString(""), CString(""),			
					   csNewOrg, CString(""), ilMutUrno);
			}
			
			//Alle anderen Mitarbeiter in der Org. Unit
			//ogSorData.ReadSorData();
			csStfUrnos = ogSorData.GetSorByOrgWithTime(pOrgs, ctPefr, ctPeto, &olSorData);
			for (y=0; y<olSorData.GetSize(); y++){
				olSor = olSorData.GetAt(y);
				ilStfUrno = olSor.Surn;
				//if (olEmplMap.Lookup(CString(ilStfUrno), retValue) == false){
					pStf = ogStfData.GetStfByUrno(ilStfUrno);
					csLastName = pStf->Lanm;
					csFirstName = pStf->Finm;
					ogScoData.GetScoBySurnWithTime(ilStfUrno, ctPefr, ctPeto, &olScoData);
					if (olScoData.GetSize() > 1){
						for (int x=0; x<olScoData.GetSize()-1;x++){
							olScoAlt = olScoData.GetAt(x+1);
							olScoNeu = olScoData.GetAt(x);
							
							csZifiAlt = olScoAlt.Zifi;
							csCwehAlt = olScoAlt.Cweh;
							dWhpw = atof(csCwehAlt)/100;
							csNetw = csCwehAlt;
							
							if (csZifiAlt == 'x'){
								csZifiAlt = '1';
								dNetw = (dWhpw - 1);
								csNetw.Format("%f", dNetw * 100);
								csNetw = csNetw.Left(4);
							}
							else
								csZifiAlt = '0';
							
							csZifiNeu = olScoNeu.Zifi;
							if(csZifiNeu == 'x')
								csZifiNeu = '1';
							else
								csZifiNeu = '0';


							ctEntrance = olScoNeu.Vpfr;
							csEntrance.Format("%i%.2i%.2i", ctEntrance.GetYear(), ctEntrance.GetMonth(), ctEntrance.GetDay());
							
							csCodeNeu = olScoNeu.Code;
							//Vertragsart
							if(csCodeAlt != csCodeNeu)
								NewUMU(csNetw, csLastName, csFirstName, csEntrance, CString(""), csCodeAlt, CString(""),			
								       csCodeNeu, CString(""), ilMutUrno);
							
							csCwehAlt = olScoAlt.Cweh;
							csCwehNeu = olScoNeu.Cweh;
							//Wochenstunden + Zifi
							if(csCwehAlt != csCwehNeu || csZifiAlt != csZifiNeu)
								NewUMU(csNetw, csLastName, csFirstName, csEntrance, CString(""), csCwehAlt, csZifiAlt,			
								       csCwehNeu, csZifiNeu, ilMutUrno);
							
							//Statusanhang
							csStscAlt = olScoAlt.Stsc;
							csStscNeu = olScoNeu.Stsc;
							if(csStscAlt != csStscNeu)
								NewUMU(csNetw, csLastName, csFirstName, csEntrance, CString(""), csStscAlt, CString(""),			
								       csStscNeu, CString(""), ilMutUrno);
						}
					}
				//}
			}
		}
	}

	//ogSorData.ReadSorData();
	//ogScoData.ReadScoData();
	
	// Sanduhr
	AfxGetApp()->DoWaitCursor(-1);
CCS_CATCH_ALL
}

void CBudgetDlg::DeleteHdcUhd(long ilBudUrno)
{
CCS_TRY
	long ilHdcUrno, ilUhdUrno;
	
	HDCDATA *pHdc;
	UHDDATA *pUhd;

	for (int i=ogHdcData.omData.GetSize()-1; i>=0; i--){
		if (ogHdcData.omData[i].Ubud == ilBudUrno){
			ilHdcUrno = ogHdcData.omData[i].Urno;
			pHdc = ogHdcData.GetHdcByUrno(ilHdcUrno);
			ogHdcData.Delete(pHdc);
			for (int m=ogUhdData.omData.GetSize()-1; m>=0; m--){
				if (ogUhdData.omData[m].Uhdc == ilHdcUrno){
					ilUhdUrno = ogUhdData.omData[m].Urno;
					pUhd = ogUhdData.GetUhdByUrno(ilUhdUrno);
					ogUhdData.Delete(pUhd);
				}
			}
		}
	}

CCS_CATCH_ALL
}

void CBudgetDlg::DeleteMutUmu(long ilBudUrno)
{
CCS_TRY
	long ilMutUrno, ilUmuUrno;

	MUTDATA *pMut;
	UMUDATA *pUmu;

	for (int i=ogMutData.omData.GetSize()-1; i>=0; i--){
		if (ogMutData.omData[i].Ubud == ilBudUrno){
			ilMutUrno = ogMutData.omData[i].Urno;
			pMut = ogMutData.GetMutByUrno(ilMutUrno);
			ogMutData.Delete(pMut);
			for (int m=ogUmuData.omData.GetSize()-1; m>=0; m--){
				if (ogUmuData.omData[m].Umut == ilMutUrno){
					ilUmuUrno = ogUmuData.omData[m].Urno;
					pUmu = ogUmuData.GetUmuByUrno(ilUmuUrno);
					ogUmuData.Delete(pUmu);
				}
			}
		}
	}
CCS_CATCH_ALL
}

void CBudgetDlg::NewMUT(CString Dpt1, CString Ymdy, long Ubud)
{
CCS_TRY
	CString csJahr, csMonat, csTag;
	int iYear, iMonth, iDay, iAnz;

	iYear = atoi(Ymdy.Left(4));
	iMonth = atoi(Ymdy.Mid(4, 2));
	iDay = atoi(Ymdy.Right(2));
	COleDateTime ctBis(iYear, iMonth, iDay, 0, 0, 0);
	COleDateTime ctVon;

	switch (iMonth){
	case 1:
	case 2:
	case 4:
	case 6:
	case 8:
	case 9:
	case 11:
		iAnz = 30;
		break;
	case 5:
	case 7:
	case 10:
	case 12:
		iAnz = 29;
		break;
	case 3:
		COleDateTime ctFeb(iYear, iMonth - 1, 1, 0, 0, 0);
		iAnz = GetDaysOfMonth(ctFeb) - 1;
		break;
	}

	COleDateTimeSpan ctSpan(iAnz, 0, 0, 0);

	ctVon = ctBis - ctSpan;

	if (ctVon.GetMonth() == ctBis.GetMonth())
		ctVon.SetDate(ctBis.GetYear(), ctBis.GetMonth(), 1);
	
	iYear = ctVon.GetYear();
	iMonth = ctVon.GetMonth();
	iDay = ctVon.GetDay();

	MUTDATA *pMut = new MUTDATA;
	// �nderungsflag setzen
	pMut->IsChanged = DATA_NEW;
	// Erzeugungsdatum einstellen
	pMut->Cdat = COleDateTime::GetCurrentTime();
	// Anwender (Ersteller) einstellen
	strcpy(pMut->Usec,pcgUser);
	//n�chste freie Datensatz-Urno ermitteln und speichern
	pMut->Urno = ogBasicData.GetNextUrno();
	//Home Airport
	strcpy(pMut->Hopo,pcgHome);
	//Urno BUDTAB
	pMut->Ubud = Ubud;
	//Organisationseinheit
	strcpy(pMut->Dpt1, Dpt1.GetBuffer(0));
	//Jahr/Monat/Stichtag
	strcpy(pMut->Ymdy, Ymdy.GetBuffer(0));
	//Jahr/Monat/Tag von
	pMut->Pefr = ctVon;
	//Jahr/Monat/Tag bis
	pMut->Peto = ctBis;
	// Datensatz einf�gen, wenn gew�nscht (ohne BC!!!)
	ogMutData.Insert(pMut);

CCS_CATCH_ALL
}

int CBudgetDlg::GetDaysOfMonth(COleDateTime opDay)
{
CCS_TRY
	if(opDay.GetStatus() != COleDateTime::valid)
		return 0;

	COleDateTime olMonth = COleDateTime(opDay.GetYear(), opDay.GetMonth(),1,0,0,0);
	bool blNextMonth = false;
	int ilDays = 28;
	while(!blNextMonth)
	{
		COleDateTime olTmpDay = olMonth + COleDateTimeSpan(ilDays,0,0,0);
		if(olTmpDay.GetMonth() != opDay.GetMonth())
			blNextMonth = true;
		else
			ilDays++;
	}
	if(ilDays < 0 || ilDays > 31)
		ilDays = 0;
	return ilDays;
CCS_CATCH_ALL
	return 0;
}


void CBudgetDlg::NewUMU(CString Netw, CString Lanm, CString Finm, CString Doem, CString Dodm, CString Mufr, CString Sufr, CString Muto, CString Suto, long Umut)
{
CCS_TRY
	UMUDATA *pUmu = new UMUDATA;
	// �nderungsflag setzen
	pUmu->IsChanged = DATA_NEW;
	//n�chste freie Datensatz-Urno ermitteln und speichern
	pUmu->Urno = ogBasicData.GetNextUrno();
	//Home Airport
	strcpy(pUmu->Hopo,pcgHome);
	//Urno MUTTAB
	pUmu->Umut = Umut;
	//Netweek
	strcpy(pUmu->Netw, Netw.GetBuffer(0));
	//Nachname
	strcpy(pUmu->Lanm, Lanm.GetBuffer(0));
	//Vorname
	strcpy(pUmu->Finm, Finm.GetBuffer(0));
	//Entrance
	strcpy(pUmu->Doem, Doem.GetBuffer(0));
	//Leaving
	strcpy(pUmu->Dodm, Dodm.GetBuffer(0));
	//Bisheriger Wert
	strcpy(pUmu->Mufr, Mufr.GetBuffer(0));
	//Bisheriger Indikator Zus�tzliche Ferien
	strcpy(pUmu->Sufr, Sufr.GetBuffer(0));
	//Neuer Wert
	strcpy(pUmu->Muto, Muto.GetBuffer(0));
	//Neuer Indikator Zus�tzliche Ferien
	strcpy(pUmu->Suto, Suto.GetBuffer(0));
	// Datensatz einf�gen, wenn gew�nscht (ohne BC!!!)
	ogUmuData.Insert(pUmu);

CCS_CATCH_ALL
}

void CBudgetDlg::GetForecast(BUDDATA *pBud)
{
CCS_TRY
	long ilHdcUrno;
	CString csDpt1, csYmdy, csYemo, csActu;
	
	//FORTAB-S�tze l�schen
	DeleteFor(pBud->Urno);

	//Urnos HDCTAB ermitteln
	for (int i=0; i<ogHdcData.omData.GetSize(); i++){
		if (ogHdcData.omData[i].Ubud == pBud->Urno){
			ilHdcUrno = ogHdcData.omData[i].Urno;
			csDpt1 = ogHdcData.omData[i].Dpt1;
			csYmdy = ogHdcData.omData[i].Ymdy;
			csYemo = csYmdy.Left(6);
			//Summens�tze in UHDTAB ermitteln
			for (int m=0; m<ogUhdData.omData.GetSize(); m++){
				if (ogUhdData.omData[m].Uhdc == ilHdcUrno){
					if(ogUhdData.omData[m].Netw == CString("") && ogUhdData.omData[m].Stsc == CString("") && ogUhdData.omData[m].Suph == CString("")){
						csActu = ogUhdData.omData[m].Actu;
						NewFOR(csDpt1, csYemo, CString(""), csActu, pBud->Urno);						
					}
				}
			}
		}
	}
CCS_CATCH_ALL
}

void CBudgetDlg::DeleteFor(long ilBudUrno)
{
CCS_TRY
	long ilForUrno;
	
	FORDATA *pFor;
	
	for (int i=ogForData.omData.GetSize()-1; i>=0; i--){
		if (ogForData.omData[i].Ubud == ilBudUrno){
			ilForUrno = ogForData.omData[i].Urno;
			pFor = ogForData.GetForByUrno(ilForUrno);
			ogForData.Delete(pFor);
		}
	}

CCS_CATCH_ALL
}

void CBudgetDlg::NewFOR(CString Dpt1, CString Yemo, CString Budg, CString Actu, long Ubud)
{
CCS_TRY
	FORDATA *pFor = new FORDATA;
	// �nderungsflag setzen
	pFor->IsChanged = DATA_NEW;
	// Erzeugungsdatum einstellen
	pFor->Cdat = COleDateTime::GetCurrentTime();
	// Anwender (Ersteller) einstellen
	strcpy(pFor->Usec,pcgUser);
	//n�chste freie Datensatz-Urno ermitteln und speichern
	pFor->Urno = ogBasicData.GetNextUrno();
	//Home Airport
	strcpy(pFor->Hopo,pcgHome);
	//Urno BUDTAB
	pFor->Ubud = Ubud;
	//Organisationseinheit
	strcpy(pFor->Dpt1, Dpt1.GetBuffer(0));
	//Jahr/Monat
	strcpy(pFor->Yemo, Yemo.GetBuffer(0));
	//Umrechnung auf Personalmonate
	strcpy(pFor->Actu, Actu.GetBuffer(0));
	//Budget fehlt noch!
	//strcpy(pFor->Budg, Budg.GetBuffer(0));
	//Differenz fehlt noch!
	//strcpy(pUhd->Diff, Diff.GetBuffer(0));
	// Datensatz einf�gen, wenn gew�nscht (ohne BC!!!)
	ogForData.Insert(pFor);

CCS_CATCH_ALL
}
