// CedaSorData.cpp
 
#include <stdafx.h>
#include <CedaSorData.h>

//CedaSorData ogSorData;

void ProcessSorCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

static int CompareTimes(const SORDATA **e1, const SORDATA **e2);
static int CompareSurnAndTimes(const SORDATA **e1, const SORDATA **e2);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaSorData::CedaSorData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for SORDataStruct
	BEGIN_CEDARECINFO(SORDATA,SORDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_LONG		(Surn,"SURN")
		FIELD_CHAR_TRIM	(Code,"CODE")
		FIELD_OLEDATE	(Vpfr,"VPFR")
		FIELD_OLEDATE	(Vpto,"VPTO")
		FIELD_CHAR_TRIM	(Lead,"LEAD")
		FIELD_CHAR_TRIM	(Odgc,"ODGC")
	END_CEDARECINFO //(SORDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(SORDataRecInfo)/sizeof(SORDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SORDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"SOR");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmListOfFields,"URNO,SURN,CODE,VPFR,VPTO,LEAD,ODGC");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaSorData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaSorData::Register(void)
{
	ogDdx.Register((void *)this,BC_SOR_CHANGE,	CString("SORDATA"), CString("Sor-changed"),	ProcessSorCf);
	ogDdx.Register((void *)this,BC_SOR_NEW,		CString("SORDATA"), CString("Sor-new"),		ProcessSorCf);
	ogDdx.Register((void *)this,BC_SOR_DELETE,	CString("SORDATA"), CString("Sor-deleted"),	ProcessSorCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaSorData::~CedaSorData(void)
{
	TRACE("CedaSorData::~CedaSorData called\n");
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaSorData::ClearAll(bool bpWithRegistration)
{
	TRACE("CedaSorData::ClearAll called\n");
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaSorData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		TRACE("Read-Sor: Ceda-Error %d \n",ilRc);
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		SORDATA *prlSor = new SORDATA;
		if ((ilRc = GetFirstBufferRecord(prlSor)) == true)
		{
			// BDA: Vpfr & Vpto k�nnen auch mal null sein, es ist falsch!
			if(prlSor->Vpfr.GetStatus() != COleDateTime::valid)
			{
				TRACE("Read-Sor: %d gelesen, FEHLER IM DATENSATZ, Surn=%ld\n",ilLc-1,prlSor->Surn);
				delete prlSor;
				continue;
			}
			//else
				//TRACE("Read-Sor: %d gelesen, KEIN FEHLER\n",ilLc-1);

			omData.Add(prlSor);//Update omData
			omUrnoMap.SetAt((void *)prlSor->Urno,prlSor);
		}
		else
		{
			delete prlSor;
		}
	}
	TRACE("Read-Sor: %d gelesen\n",ilLc-1);
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaSorData::Insert(SORDATA *prpSor)
{
	prpSor->IsChanged = DATA_NEW;
	if(Save(prpSor) == false) return false; //Update Database
	InsertInternal(prpSor);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaSorData::InsertInternal(SORDATA *prpSor)
{
	ogDdx.DataChanged((void *)this, SOR_NEW,(void *)prpSor ); //Update Viewer
	omData.Add(prpSor);//Update omData
	omUrnoMap.SetAt((void *)prpSor->Urno,prpSor);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaSorData::Delete(long lpUrno)
{
	SORDATA *prlSor = GetSorByUrno(lpUrno);
	if (prlSor != NULL)
	{
		prlSor->IsChanged = DATA_DELETED;
		if(Save(prlSor) == false) return false; //Update Database
		DeleteInternal(prlSor);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaSorData::DeleteInternal(SORDATA *prpSor)
{
	ogDdx.DataChanged((void *)this,SOR_DELETE,(void *)prpSor); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpSor->Urno);
	int ilSorCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilSorCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpSor->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaSorData::Update(SORDATA *prpSor)
{
	if (GetSorByUrno(prpSor->Urno) != NULL)
	{
		if (prpSor->IsChanged == DATA_UNCHANGED)
		{
			prpSor->IsChanged = DATA_CHANGED;
		}
		if(Save(prpSor) == false) return false; //Update Database
		UpdateInternal(prpSor);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaSorData::UpdateInternal(SORDATA *prpSor)
{
	SORDATA *prlSor = GetSorByUrno(prpSor->Urno);
	if (prlSor != NULL)
	{
		*prlSor = *prpSor; //Update omData
		ogDdx.DataChanged((void *)this,SOR_CHANGE,(void *)prlSor); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

SORDATA *CedaSorData::GetSorByUrno(long lpUrno)
{
	SORDATA  *prlSor;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlSor) == TRUE)
	{
		return prlSor;
	}
	return NULL;
}

//---------------------------------------------------------------------------------------------------------

void CedaSorData::GetSorBySurnWithTime(long lpSurn,CTime opStart,CTime opEnd,CCSPtrArray<SORDATA> *popSorData)
{
	SORDATA  *prlSor;
	COleDateTime olStart,olEnd;

	popSorData->DeleteAll();
	
	CCSPtrArray<SORDATA> olSorData;
	POSITION rlPos;
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSor);
		if((prlSor->Surn == lpSurn))
		{
			olSorData.Add(prlSor); 
		}
	}
	int ilSize = olSorData.GetSize();
	if(ilSize > 0)
	{
		olSorData.Sort(CompareTimes);
		
		olStart.SetDateTime(opStart.GetYear(),opStart.GetMonth(),opStart.GetDay(),opStart.GetHour(),opStart.GetMinute(),opStart.GetSecond());
		olEnd.SetDateTime(opEnd.GetYear(),opEnd.GetMonth(),opEnd.GetDay(),opEnd.GetHour(),opEnd.GetMinute(),opEnd.GetSecond());
		SORDATA *prlPrevSor=NULL;
		for(int i=ilSize;--i>=0;)
		{
			prlSor = &olSorData[i];
			// ARE: <prlSor->Vpfr> hat Status NULL -> COleDateTime-Exception.
			// Status NULL OK? Oder korrupte Datensatz-Leichen? Oder hat Wert = NULL
			// bestimmte Bedeutung (z.B. immer g�ltig)?
			if((prlSor->Vpfr.GetStatus() == COleDateTime::valid) && (prlSor->Vpfr<olEnd))
			{
				switch(prlSor->Vpto.GetStatus())
				{
				case COleDateTime::valid:
					if(prlSor->Vpto>olStart)
					{
						popSorData->Add(prlSor);
					}
					else
					{
						i=0;
					}
					break;
				case COleDateTime::null:
					if(prlPrevSor!=NULL)
					{
						if(prlPrevSor->Vpfr>olStart)
						{
							popSorData->Add(prlSor);
						}
						else
						{
							i=0;
						}
					}
					else
					{
						popSorData->Add(prlSor);
					}
					break;
				default:
					break;
				}
			}
			prlPrevSor = prlSor;
		}
	}
}

void CedaSorData::GetSorBySurn(long lpSurn,CCSPtrArray<SORDATA> *popSorData)
{
	SORDATA  *prlSor;
	COleDateTime olStart,olEnd;

	popSorData->DeleteAll();
	
	CCSPtrArray<SORDATA> olSorData;
	POSITION rlPos;
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSor);
		if((prlSor->Surn == lpSurn))
		{
			olSorData.Add(prlSor); 
		}
	}
	int ilSize = olSorData.GetSize();
	if(ilSize > 0)
	{
		olSorData.Sort(CompareTimes);
		
		//olStart.SetDateTime(opStart.GetYear(),opStart.GetMonth(),opStart.GetDay(),opStart.GetHour(),opStart.GetMinute(),opStart.GetSecond());
		//olEnd.SetDateTime(opEnd.GetYear(),opEnd.GetMonth(),opEnd.GetDay(),opEnd.GetHour(),opEnd.GetMinute(),opEnd.GetSecond());
		SORDATA *prlPrevSor=NULL;
		for(int i=ilSize;--i>=0;)
		{
			prlSor = &olSorData[i];
			// ARE: <prlSor->Vpfr> hat Status NULL -> COleDateTime-Exception.
			// Status NULL OK? Oder korrupte Datensatz-Leichen? Oder hat Wert = NULL
			// bestimmte Bedeutung (z.B. immer g�ltig)?
			/*if((prlSor->Vpfr.GetStatus() == COleDateTime::valid) && (prlSor->Vpfr<olEnd))
			{
				switch(prlSor->Vpto.GetStatus())
				{
				case COleDateTime::valid:
					if(prlSor->Vpto>olStart)
					{*/
						popSorData->Add(prlSor);
					/*}
					else
					{
						i=0;
					}
					break;
				case COleDateTime::null:
					if(prlPrevSor!=NULL)
					{
						if(prlPrevSor->Vpfr>olStart)
						{
							popSorData->Add(prlSor);
						}
						else
						{
							i=0;
						}
					}
					else
					{
						popSorData->Add(prlSor);
					}
					break;
				default:
					break;
				}
			}*/
			prlPrevSor = prlSor;
		}
	}
}

//---------------------------------------------------------------------------------------

CString CedaSorData::GetOrgBySurn(long lpSurn, COleDateTime opDate)
{
	CString olOrgCode;

	CTime olDay(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),0,0,0);
	CCSPtrArray<SORDATA> olSorData;

	GetSorBySurnWithTime(lpSurn, olDay, olDay, &olSorData);
	int ilSorSize = olSorData.GetSize();

	for(int i=0; i<ilSorSize; i++)
	{
		if(olSorData[i].Vpfr.GetStatus() == COleDateTime::valid)
		{
			if(olSorData[i].Vpfr <= opDate)
			{
				switch(olSorData[i].Vpto.GetStatus())
				{
				case COleDateTime::valid:
					if(olSorData[i].Vpto > opDate)
					{
						olOrgCode = olSorData[i].Code;
					}
					else
					{
						olOrgCode = "";

					}
					break;
				default:
						olOrgCode = olSorData[i].Code;
					break;
				}
			}
		}
	}
	olSorData.RemoveAll();
	return olOrgCode;
}

//---------------------------------------------------------------------------------------

CString CedaSorData::GetOrgWithTime(COleDateTime opDate, CCSPtrArray<SORDATA> *popSorData)
{
	CString olOrgCode;

	int ilSorSize = popSorData->GetSize();

	for(int i=0; i<ilSorSize; i++)
	{
		if((*popSorData)[i].Vpfr.GetStatus() == COleDateTime::valid)
		{
			if((*popSorData)[i].Vpfr <= opDate)
			{
				switch((*popSorData)[i].Vpto.GetStatus())
				{
				case COleDateTime::valid:
					if((*popSorData)[i].Vpto > opDate)
					{
						olOrgCode = (*popSorData)[i].Code;
					}
					else
					{
						olOrgCode = "";

					}
					break;
				default:
						olOrgCode = (*popSorData)[i].Code;
					break;
				}
			}
		}
	}
	return olOrgCode;
}

//---------------------------------------------------------------------------------------------------------

CString CedaSorData::GetSorByOrgWithTime(CStringArray *popOrg, COleDateTime opStart, COleDateTime opEnd, CCSPtrArray<SORDATA> *popSorData)
{
	popSorData->RemoveAll();
	CString olUrnos = "";

	if(popOrg->GetSize() > 0 && opStart.GetStatus() == COleDateTime::valid && opEnd.GetStatus() == COleDateTime::valid && opStart <= opEnd)
	{
		CString olTmpCode;
		COleDateTime olTmpStart,olTmpEnd;
		CMapStringToPtr olOrgMap;
		SORDATA *prlSor = NULL;


		for(int i=0; i<popOrg->GetSize(); i++)
		{
			olOrgMap.SetAt(popOrg->GetAt(i),NULL);
		}

		CCSPtrArray<SORDATA> olSorData;
		POSITION rlPos;
		for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSor);
			olSorData.Add(prlSor); 
		}

		olSorData.Sort(CompareSurnAndTimes);
		int ilSize = olSorData.GetSize();
		SORDATA *prlNextSor = NULL;
		void  *prlVoid = NULL;

		for(i = 0; i < ilSize; i++)
		{
			prlSor = &olSorData[i];
			prlNextSor = NULL;
			if((i+1) < ilSize)
			{
				if(prlSor->Surn == olSorData[i+1].Surn)
					prlNextSor = &olSorData[i+1];
			}

			if(prlSor->Vpfr.GetStatus() == COleDateTime::valid)
			{
				if(prlSor->Vpfr<=opEnd)
				{
					switch(prlSor->Vpto.GetStatus())

					{
					case COleDateTime::valid:
						if(prlSor->Vpto>=opStart)
						{
							olTmpCode.Format("%s",prlSor->Code);
							if(olOrgMap.Lookup(olTmpCode,(void *&)prlVoid) == TRUE)
							{
								popSorData->Add(prlSor);
							}
						}
						break;
					case COleDateTime::null:
						if(prlNextSor != NULL)
						{
							if(prlNextSor->Vpfr>=opStart)
							{
								olTmpCode.Format("%s",prlSor->Code);
								if(olOrgMap.Lookup(olTmpCode,(void *&)prlVoid) == TRUE)
								{
									popSorData->Add(prlSor);
								}
							}
						}
						else
						{
							olTmpCode.Format("%s",prlSor->Code);
							if(olOrgMap.Lookup(olTmpCode,(void *&)prlVoid) == TRUE)
							{
								popSorData->Add(prlSor);
							}
						}
						break;
					default:
						break;
					}
				}
			}
		}
		// Create Stf-Urno-List for return value
		ilSize = popSorData->GetSize();
		olUrnos = ",";
		for(i = 0; i < ilSize; i++)
		{
			CString olTmpUrno;
			olTmpUrno.Format(",%ld,",(*popSorData)[i].Surn);
			if(olUrnos.Find(olTmpUrno) == -1)
			{
				olUrnos += olTmpUrno.Mid(1);
			}
		}
		if(olUrnos.IsEmpty() == FALSE)
		{
			olUrnos = olUrnos.Mid(1,olUrnos.GetLength()-2);
		}
		olOrgMap.RemoveAll();
	}
	return olUrnos;
}

CString CedaSorData::GetEintritteByOrgWithTime(CStringArray *popOrg, COleDateTime opStart, COleDateTime opEnd, CCSPtrArray<SORDATA> *popSorData, CMapStringToString *popNewOrg)
{
	popSorData->RemoveAll();
	CString olUrnos = "";

	if(popOrg->GetSize() > 0 && opStart.GetStatus() == COleDateTime::valid && opEnd.GetStatus() == COleDateTime::valid && opStart <= opEnd)
	{
		CString olTmpCode, olSurn;
		COleDateTime olTmpStart,olTmpEnd;
		CMapStringToPtr olOrgMap;
		SORDATA *prlSor = NULL;


		for(int i=0; i<popOrg->GetSize(); i++)
		{
			olOrgMap.SetAt(popOrg->GetAt(i),NULL);
		}

		CCSPtrArray<SORDATA> olSorData;
		POSITION rlPos;
		for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSor);
			olSorData.Add(prlSor); 
		}

		olSorData.Sort(CompareSurnAndTimes);
		int ilSize = olSorData.GetSize();
		SORDATA *prlNextSor = NULL;
		SORDATA *prlPrevSor = NULL;
		void  *prlVoid = NULL;

		for(i = 0; i < ilSize; i++)
		{
			prlSor = &olSorData[i];
			prlNextSor = NULL;
			prlPrevSor = NULL;
			
			if((i+1) < ilSize)
			{
				if(prlSor->Surn == olSorData[i+1].Surn)
					prlNextSor = &olSorData[i+1];
			}
			
			if((i-1) > 0)
			{
				if(prlSor->Surn == olSorData[i-1].Surn)
					prlPrevSor = &olSorData[i-1];
			}

			if(prlSor->Vpfr.GetStatus() == COleDateTime::valid)
			{
				if(prlSor->Vpfr<=opEnd && prlSor->Vpfr>=opStart)
				{
					switch(prlSor->Vpto.GetStatus())

					{
					case COleDateTime::valid:
						if(prlSor->Vpto>=opStart)
						{
							olTmpCode.Format("%s",prlSor->Code);
							if(olOrgMap.Lookup(olTmpCode,(void *&)prlVoid) == TRUE)
							{
								popSorData->Add(prlSor);
								olSurn.Format("%d", prlSor->Surn);
								if(prlPrevSor != NULL)
									popNewOrg->SetAt(olSurn, CString(prlPrevSor->Code));
								else
									popNewOrg->SetAt(olSurn, CString("")); 
							}
						}
						break;
					case COleDateTime::null:
						if(prlNextSor != NULL)
						{
							if(prlNextSor->Vpfr>=opStart)
							{
								olTmpCode.Format("%s",prlSor->Code);
								if(olOrgMap.Lookup(olTmpCode,(void *&)prlVoid) == TRUE)
								{
									popSorData->Add(prlSor);
									olSurn.Format("%d", prlSor->Surn);
									if(prlPrevSor != NULL)
										popNewOrg->SetAt(olSurn, CString(prlPrevSor->Code));
									else
										popNewOrg->SetAt(olSurn, CString("")); 
								}
							}
						}
						else
						{
							olTmpCode.Format("%s",prlSor->Code);
							if(olOrgMap.Lookup(olTmpCode,(void *&)prlVoid) == TRUE)
							{
								popSorData->Add(prlSor);
								olSurn.Format("%d", prlSor->Surn);
								if(prlPrevSor != NULL)
									popNewOrg->SetAt(olSurn, CString(prlPrevSor->Code));
								else
									popNewOrg->SetAt(olSurn, CString("")); 
							}
						}
						break;
					default:
						break;
					}
				}
			}
		}
		// Create Stf-Urno-List for return value
		ilSize = popSorData->GetSize();
		olUrnos = ",";
		for(i = 0; i < ilSize; i++)
		{
			CString olTmpUrno;
			olTmpUrno.Format(",%ld,",(*popSorData)[i].Surn);
			if(olUrnos.Find(olTmpUrno) == -1)
			{
				olUrnos += olTmpUrno.Mid(1);
			}
		}
		if(olUrnos.IsEmpty() == FALSE)
		{
			olUrnos = olUrnos.Mid(1,olUrnos.GetLength()-2);
		}
		olOrgMap.RemoveAll();
	}
	return olUrnos;
}

CString CedaSorData::GetAustritteByOrgWithTime(CStringArray *popOrg, COleDateTime opStart, COleDateTime opEnd, CCSPtrArray<SORDATA> *popSorData, CMapStringToOb *popNewOrg)
{
	CString csVpfr, olSurn;

	popSorData->RemoveAll();
	popNewOrg->RemoveAll();

	CString olUrnos = "";
	
	if(popOrg->GetSize() > 0 && opStart.GetStatus() == COleDateTime::valid && opEnd.GetStatus() == COleDateTime::valid && opStart <= opEnd)
	{
		CString olTmpCode;
		COleDateTime olTmpStart,olTmpEnd;
		CMapStringToPtr olOrgMap;
		SORDATA *prlSor = NULL;


		for(int i=0; i<popOrg->GetSize(); i++)
		{
			olOrgMap.SetAt(popOrg->GetAt(i),NULL);
		}

		CCSPtrArray<SORDATA> olSorData;
		POSITION rlPos;
		for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSor);
			olSorData.Add(prlSor); 
		}

		olSorData.Sort(CompareSurnAndTimes);
		int ilSize = olSorData.GetSize();
		SORDATA *prlNextSor = NULL;
		void  *prlVoid = NULL;

		for(i=0; i<ilSize; i++)
		{
			prlSor = &olSorData[i];
			prlNextSor = NULL;
			if((i+1) < ilSize)
			{
				if(prlSor->Surn == olSorData[i+1].Surn)
					prlNextSor = &olSorData[i+1];
			}

			if(prlSor->Vpfr.GetStatus() == COleDateTime::valid)
			{
				if(prlSor->Vpfr<=opEnd)
				{
					switch(prlSor->Vpto.GetStatus())

					{
					case COleDateTime::valid:
						if(prlSor->Vpto>=opStart && prlSor->Vpto<=opEnd)
						{
							
							olTmpCode.Format("%s",prlSor->Code);
							if(olOrgMap.Lookup(olTmpCode,(void *&)prlVoid) == TRUE)
							{
								popSorData->Add(prlSor);
								olSurn.Format("%d", prlSor->Surn);
								if(prlNextSor != NULL){
									NEWORG *pOrg = new NEWORG;
									pOrg->Org = CString(prlNextSor->Code);
									csVpfr = "";
									if (prlNextSor->Vpfr.GetStatus() == COleDateTime::valid)
										csVpfr.Format("%i%.2i%.2i", prlNextSor->Vpfr.GetYear(),
													   prlNextSor->Vpfr.GetMonth(), prlNextSor->Vpfr.GetDay());
									pOrg->Vpfr = csVpfr;
									popNewOrg->SetAt(olSurn, (CObject*)pOrg);
								}
								else
									popNewOrg->SetAt(olSurn, NULL); 
							}
						}
						break;
					case COleDateTime::null:
						if(prlNextSor != NULL)
						{
							if(prlNextSor->Vpfr>=opStart && prlNextSor->Vpfr<=opEnd)
							{
								olTmpCode.Format("%s",prlSor->Code);
								if(olOrgMap.Lookup(olTmpCode,(void *&)prlVoid) == TRUE)
								{
									popSorData->Add(prlSor);
									olSurn.Format("%d", prlSor->Surn);
									NEWORG *pOrg = new NEWORG;
									pOrg->Org = CString(prlNextSor->Code);
									csVpfr = "";
									if (prlNextSor->Vpfr.GetStatus() == COleDateTime::valid)
										csVpfr.Format("%i%.2i%.2i", prlNextSor->Vpfr.GetYear(),
													   prlNextSor->Vpfr.GetMonth(), prlNextSor->Vpfr.GetDay());
									pOrg->Vpfr = csVpfr;
									popNewOrg->SetAt(olSurn, (CObject*)pOrg);
								}
							}
						}
						break;
					default:
						break;
					}
				}
			}
		}
		// Create Stf-Urno-List for return value
		ilSize = popSorData->GetSize();
		olUrnos = ",";
		for(i = 0; i < ilSize; i++)
		{
			CString olTmpUrno;
			olTmpUrno.Format(",%ld,",(*popSorData)[i].Surn);
			if(olUrnos.Find(olTmpUrno) == -1)
			{
				olUrnos += olTmpUrno.Mid(1);
			}
		}
		if(olUrnos.IsEmpty() == FALSE)
		{
			olUrnos = olUrnos.Mid(1,olUrnos.GetLength()-2);
		}
		olOrgMap.RemoveAll();
	}
	return olUrnos;
}

//---------------------------------------------------------------------------------------------------------
// GetSorWithoutOrgWithTime:
// Gibt eine List aller MA's aus, die in einem bestimmten Zeitraum die keiner Organisation angeh�ren
//---------------------------------------------------------------------------------------------------------

CString CedaSorData::GetStfWithoutOrgWithTime(COleDateTime opStart, COleDateTime opEnd, CCSPtrArray<STFDATA> *popStfData)
{
	int ilStfSize = ogStfData.omUrnoMap.GetCount();
	CMapStringToPtr olSorMap;
	popStfData->DeleteAll();
	CString olUrnos = "";

	if(ilStfSize > 0 && opStart.GetStatus() == COleDateTime::valid && opEnd.GetStatus() == COleDateTime::valid && opStart <= opEnd)
	{
		// 1. Schritt: Alle MA's suchen die im Zeitraum eine Organisation haben
		COleDateTime olTmpStart,olTmpEnd;
		SORDATA *prlSor = NULL;
		CString olTmpUrno;

		CCSPtrArray<SORDATA> olSorData;
		POSITION rlPos;
		for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSor);
			olSorData.Add(prlSor); 
		}

		olSorData.Sort(CompareSurnAndTimes);
		int ilSize = olSorData.GetSize();
		SORDATA *prlNextSor = NULL;
		void  *prlVoid = NULL;

		for(int i = 0; i < ilSize; i++)
		{
			prlSor = &olSorData[i];
			prlNextSor = NULL;
			if((i+1) < ilSize)
			{
				if(prlSor->Surn == olSorData[i+1].Surn)
					prlNextSor = &olSorData[i+1];
			}

			if(prlSor->Vpfr.GetStatus() == COleDateTime::valid)
			{
				if(prlSor->Vpfr<=opEnd)
				{
					switch(prlSor->Vpto.GetStatus())
					{
					case COleDateTime::valid:
						if(prlSor->Vpto>=opStart)
						{
							olTmpUrno.Format("%ld",prlSor->Surn);
							olSorMap.SetAt(olTmpUrno,NULL);
						}
						break;
					case COleDateTime::null:
						if(prlNextSor != NULL)
						{
							if(prlNextSor->Vpfr>=opStart)
							{
								olTmpUrno.Format("%ld",prlSor->Surn);
								olSorMap.SetAt(olTmpUrno,NULL);
							}
						}
						else
						{
							olTmpUrno.Format("%ld",prlSor->Surn);
							olSorMap.SetAt(olTmpUrno,NULL);
						}
						break;
					default:
						break;
					}
				}
			}
		}
		olSorData.RemoveAll();
		// 2. Schritt: Liste aller MA's erstellen, die nicht in der Liste der MA's mit Organisation sind
		STFDATA *prlStf = NULL;
		for(rlPos = ogStfData.omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			ogStfData.omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlStf);
			olTmpUrno.Format("%ld", prlStf->Urno);

			if(olSorMap.Lookup(olTmpUrno,(void *&)prlVoid) == FALSE)
			{
				popStfData->Add(prlStf);
			}
		}
		// Create Stf-Urno-List for return value
		ilSize = popStfData->GetSize();
		olUrnos = ",";
		for(i = 0; i < ilSize; i++)
		{
			CString olTmpUrno;
			olTmpUrno.Format(",%ld,",(*popStfData)[i].Urno);
			if(olUrnos.Find(olTmpUrno) == -1)
			{
				olUrnos += olTmpUrno.Mid(1);
			}
		}
		if(olUrnos.IsEmpty() == FALSE)
		{
			olUrnos = olUrnos.Mid(1,olUrnos.GetLength()-2);
		}
		olSorMap.RemoveAll();
	}
	return olUrnos;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaSorData::ReadSpecialData(CCSPtrArray<SORDATA> *popSor,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popSor != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			SORDATA *prpSor = new SORDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpSor,CString(pclFieldList))) == true)
			{
				popSor->Add(prpSor);
			}
			else
			{
				delete prpSor;
			}
		}
		if(popSor->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaSorData::Save(SORDATA *prpSor)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpSor->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpSor->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpSor);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpSor->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSor->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSor);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpSor->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSor->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
	TRACE("Sor-IRT/URT/DRT: Ceda-Return %d \n",ilRc);
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessSorCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogSorData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaSorData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlSorData;
	prlSorData = (struct BcStruct *) vpDataPointer;
	SORDATA *prlSor;
	if(ipDDXType == BC_SOR_NEW)
	{
		prlSor = new SORDATA;
		GetRecordFromItemList(prlSor,prlSorData->Fields,prlSorData->Data);
		InsertInternal(prlSor);
	}
	if(ipDDXType == BC_SOR_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlSorData->Selection);
		prlSor = GetSorByUrno(llUrno);
		if(prlSor != NULL)
		{
			GetRecordFromItemList(prlSor,prlSorData->Fields,prlSorData->Data);
			UpdateInternal(prlSor);
		}
	}
	if(ipDDXType == BC_SOR_DELETE)
	{
		long llUrno;
		CString olSelection = (CString)prlSorData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlSorData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlSor = GetSorByUrno(llUrno);
		if (prlSor != NULL)
		{
			DeleteInternal(prlSor);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------

static int CompareTimes(const SORDATA **e1, const SORDATA **e2)
{
	if((**e1).Vpfr>(**e2).Vpfr)
		return 1;
	else
		return -1;
}

//---------------------------------------------------------------------------------------------------------

static int CompareSurnAndTimes(const SORDATA **e1, const SORDATA **e2)
{

	int ilCompareResult = 0;

	     if((**e1).Surn>(**e2).Surn) ilCompareResult = 1;
	else if((**e1).Surn<(**e2).Surn) ilCompareResult = -1;

	if(ilCompareResult == 0)
	{
		     if((**e1).Vpfr>(**e2).Vpfr) ilCompareResult = 1;
		else if((**e1).Vpfr<(**e2).Vpfr) ilCompareResult = -1;
	}
	return ilCompareResult;
}

//---------------------------------------------------------------------------------------------------------

bool CedaSorData::ReadSorData()
{
CCS_TRY
	// Return-Code f�r Funktionsaufrufe
	bool ilRc = true;

	ClearAll(true);
	
    if (!CCSCedaData::CedaAction("RT", "")) return false;
	
	// ToDo: vereinfachen/umwandeln in do/while, zwei branches f�r
	// if(pomLoadStfuUrnoMap != NULL) �berfl�ssig
    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecords = 0;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		SORDATA *prlSor = new SORDATA;
		// Datensatz lesen
		blMoreRecords = GetBufferRecord(ilCountRecords,prlSor);
		// Datensatz gelesen, Z�hler inkrementieren
		if (!blMoreRecords) break;
		ilCountRecords++;
		if (!InsertInternal(prlSor)){ //, SOR_NO_SEND_DDX)){
			// Fehler -> lokalen Datensatz l�schen
			delete prlSor;
		}
	} while (blMoreRecords);
	
	
	// Test: Anzahl der gelesenen Bud
	TRACE("Read-Sor: %d gelesen\n",ilCountRecords);

    return true;
CCS_CATCH_ALL
return false;
}
