// CCSGlobl.h: interface for the CCSGlobl class.
//
//////////////////////////////////////////////////////////////////////
 
#if !defined(AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_)
#define AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_

#ifndef __CCSGLOBL_H__
#define __CCSGLOBL_H__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include <CCSDefines.h>
#include <CCSTime.h>
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Global Variable Section


class CCSBcHandle;
class CCSCedaCom;
class CCSDdx;
class CCSLog;

class CCSBcHandle;
class CBasicData;

class CedaCfgData;
class CedaBudData;
class CedaUbuData;
class CedaOrgData;
class CedaHdcData;
class CedaUhdData;
class CedaScoData;
class CedaSorData;
class CedaStfData;
class CedaCotData;
class CedaMutData;
class CedaUmuData;
class CedaForData;

class CedaSysTabData;
class PrivList;
class ConflictCheck;

class CedaSgrData;
class CedaSgmData;

class CedaBasicData;

extern CedaBasicData ogBCD;

extern CCSBcHandle ogBcHandle;
extern CCSCedaCom ogCommHandler;  // The one and only CedaCom object
extern CCSDdx ogDdx;
extern CCSLog ogLog;
extern CBasicData ogBasicData;
extern CedaCfgData ogCfgData;
extern CedaBudData ogBudData;
extern CedaUbuData ogUbuData;
extern CedaOrgData ogOrgData;
extern CedaHdcData ogHdcData;
extern CedaUhdData ogUhdData;
extern CedaScoData ogScoData;
extern CedaSorData ogSorData;
extern CedaStfData ogStfData;
extern CedaCotData ogCotData;
extern CedaMutData ogMutData;
extern CedaUmuData ogUmuData;
extern CedaForData ogForData;

extern CedaSysTabData ogSysTabData;
extern ConflictCheck ogConflictData;			// ????????
extern ConflictCheck ogCoverageConflictData;	// ????????

extern bool bgIsModal;

extern PrivList ogPrivList;

extern const char *pcgAppName; // Name of *.exe file of this
extern CString     ogAppName;  // Name of *.exe file of this
extern const char *pcgAppl;    // f�r die 8 Stelligen APPL und APPN Felder in der DB
extern CString     ogAppl;     // f�r die 8 Stelligen APPL und APPN Felder in der DB

extern CString ogCustomer;

extern char pcgUser[33];
extern char pcgPasswd[33];

extern bool bpUseGhdBc;

enum ChartState{ Minimized, Normal, Maximized };
enum GanttDynType{GANTT_NONE,GANTT_SHIFT,GANTT_BREAK,GANTT_BREAK_PERIOD};
extern ofstream of_catch;

//CCS_TRY and CCS_CATCH_ALL///////////////////////////////////////////////
/*
#define  CCS_TRY try{
#define  CCS_CATCH_ALL }\
						catch(...)\
						{\
						    char pclExcText[512]="";\
						    char pclText[1024]="";\
						    sprintf(pclText, "Es ist ein interner Fehler im Modul: %s\n in der Zeile %d aufgetreten",\
							                 __FILE__, __LINE__);\
						    strcat(pclText, "\n Beim Fortsetzen kann es zu unerw�nschten Effekten kommen\nFortsetzen??");\
						    sprintf(pclExcText, "File: %s  ==> Source-Line: %d", __FILE__, __LINE__);\
						    of_catch << pclExcText << endl;\
							if(::MessageBox(NULL, pclText, "Error", (MB_YESNO)) == IDNO)\
							{\
						       ExitProcess(0);\
							}\
						}
*/

#define  CCS_TRY			CCS_TRY_RELEASE
#define  CCS_CATCH_ALL		CCS_CATCH_ALL_RELEASE

//EndCCS_TRY and CCS_CATCH_ALL//////////////////////////////////////////////

// BDA CCS_TRY and CCS_CATCH_ALL m�ssen im Debugmodus ausgeschaltet werden, es
// mu� im DEBUG voll knallen, sonst wenn man eine Messagebox mit File/Zeile bekommt,
// kann man mit der CCS_CATCH_ALL-Zeile gar nichts anfangen.

// CCS_TRY_RELEASE and CCS_CATCH_ALL_RELEASE ///////////////////////////////////////////////
#ifndef _DEBUG
#define  CCS_TRY_RELEASE try{
#define  CCS_CATCH_ALL_RELEASE }\
						catch(...)\
						{\
						    char pclExcText[512]="";\
						    char pclText[1024]="";\
						    sprintf(pclText, "Es ist ein interner Fehler im Modul: %s\n in der Zeile %d aufgetreten",\
							                 __FILE__, __LINE__);\
						    strcat(pclText, "\n Beim Fortsetzen kann es zu unerw�nschten Effekten kommen\nFortsetzen??");\
						    sprintf(pclExcText, "File: %s  ==> Source-Line: %d", __FILE__, __LINE__);\
						    of_catch << pclExcText << endl;\
							if(::MessageBox(NULL, pclText, "Error", (MB_YESNO)) == IDNO)\
							{\
						       ExitProcess(0);\
							}\
						}
#else	// _DEBUG
#define  CCS_TRY_RELEASE
#define  CCS_CATCH_ALL_RELEASE
#endif	// #ifndef _DEBUG

// End CCS_TRY_RELEASE and CCS_CATCH_ALL_RELEASE //////////////////////////////////////////////

enum enumBitmapIndexes
{
	BREAK_RED_IDX = 21,
	BREAK_GRAY_IDX = 22,
	BREAK_GREEN_IDX = 23,
	BREAK_SILVER_IDX = 24
};

enum SimValues
{
	SIM_CONSTANT,
	SIM_PERCENT,
	SIM_ABSOLUTE
};

enum enumColorIndexes
{
	GRAY_IDX=2,GREEN_IDX,RED_IDX,BLUE_IDX,SILVER_IDX,MAROON_IDX,
	OLIVE_IDX,NAVY_IDX,PURPLE_IDX,TEAL_IDX,LIME_IDX,
	YELLOW_IDX,FUCHSIA_IDX,AQUA_IDX, WHITE_IDX,BLACK_IDX,ORANGE_IDX
};


//DDX-Types
enum 
{
	//BC_STF_NEW, BC_STF_DELETE, BC_STF_CHANGE,	STF_NEW, STF_DELETE, STF_CHANGE,
//--INTERNAL DDX-SECTION
	CLOSE_ALL_MODALS,
    BC_CFG_CHANGE,BC_CFG_DELETE,BC_CFG_NEW,BC_CFG_INSERT,CFG_CHANGE,CFG_INSERT,CFG_DELETE,CFG_NEW,
	BC_BUD_CHANGE,BC_BUD_DELETE,BC_BUD_NEW,BC_BUD_INSERT,BUD_CHANGE,BUD_INSERT,BUD_DELETE,BUD_NEW,
	BC_UBU_CHANGE,BC_UBU_DELETE,BC_UBU_NEW,BC_UBU_INSERT,UBU_CHANGE,UBU_INSERT,UBU_DELETE,UBU_NEW,
	BC_ORG_CHANGE,BC_ORG_DELETE,BC_ORG_NEW,BC_ORG_INSERT,ORG_CHANGE,ORG_INSERT,ORG_DELETE,ORG_NEW,
	BC_HDC_CHANGE,BC_HDC_DELETE,BC_HDC_NEW,BC_HDC_INSERT,HDC_CHANGE,HDC_INSERT,HDC_DELETE,HDC_NEW,
	BC_UHD_CHANGE,BC_UHD_DELETE,BC_UHD_NEW,BC_UHD_INSERT,UHD_CHANGE,UHD_INSERT,UHD_DELETE,UHD_NEW,
	BC_SCO_CHANGE,BC_SCO_DELETE,BC_SCO_NEW,BC_SCO_INSERT,SCO_CHANGE,SCO_INSERT,SCO_DELETE,SCO_NEW,
	BC_SOR_CHANGE,BC_SOR_DELETE,BC_SOR_NEW,BC_SOR_INSERT,SOR_CHANGE,SOR_INSERT,SOR_DELETE,SOR_NEW,
	BC_STF_CHANGE,BC_STF_DELETE,BC_STF_NEW,BC_STF_INSERT,STF_CHANGE,STF_INSERT,STF_DELETE,STF_NEW,
	BC_COT_CHANGE,BC_COT_DELETE,BC_COT_NEW,BC_COT_INSERT,COT_CHANGE,COT_INSERT,COT_DELETE,COT_NEW,
	BC_MUT_CHANGE,BC_MUT_DELETE,BC_MUT_NEW,BC_MUT_INSERT,MUT_CHANGE,MUT_INSERT,MUT_DELETE,MUT_NEW,
	BC_UMU_CHANGE,BC_UMU_DELETE,BC_UMU_NEW,BC_UMU_INSERT,UMU_CHANGE,UMU_INSERT,UMU_DELETE,UMU_NEW,
	BC_FOR_CHANGE,BC_FOR_DELETE,BC_FOR_NEW,BC_FOR_INSERT,FOR_CHANGE,FOR_INSERT,FOR_DELETE,FOR_NEW,
	UNDO_CHANGE
};


// Konstanten f�r Kommunikation
#define MAX_WHERE_STAFFURNO_IN		120	// max. zul�ssige Anzahl f�r MA-Urnos in 
										// WHERE-Clause; 2K max. WHERE-Clause, um
										// auf der sicheren Seite zu stehen ->
										// pro MA-Urno ca. 13 Zeichen ('<Urno>',)
										// -> 120 Urnos insgesamt, Rest (von
										// den 2K) f�r sonstige Filterkriterien.


// Farben
#define MAXCOLORS 64
#define FIRSTCONFLICTCOLOR 31
// Symbolic colors (helper constants for CGateDiagramDialog -- testing purpose only)
#define BLACK		RGB(  0,   0,   0)
#define MAROON		RGB(128,   0,   0)  // dark red
#define GREEN		RGB(  0, 128,   0)  // dark green
#define DKGREEN		RGB(  0, 200,   0)  // dark green
#define OLIVE		RGB(128, 128,   0)  // dark yellow
#define NAVY		RGB(  0,   0, 128)  // dark blue
#define PURPLE		RGB(128,   0, 128)  // dark magenta
#define TEAL		RGB(  0, 128, 128)  // dark cyan
#define GRAY		RGB(128, 128, 128)  // dark gray
#define SILVER		RGB(192, 192, 192)  // light gray
#define RED			RGB(255,   0,   0)
#define ORANGE		RGB(255, 135,   0)
#define LIME		RGB(  0, 255,   0)  // green
#define YELLOW		RGB(255, 255,   0)
#define LTYELLOW	RGB(255, 255, 160)
#define BLUE		RGB(  0,   0, 255)
#define FUCHSIA		RGB(255,   0, 255)  // magenta
#define AQUA		RGB(  0, 255, 255)  // cyan
#define WHITE		RGB(255, 255, 255)
#define LTGRAY		RGB(170, 170, 170)
#define MEDIUMDGRAY	RGB(164, 164, 164)
#define LIGHTSILVER   RGB(235, 235, 235)  
#define LIGHTSILVER2  RGB(217, 217, 217)  
#define LIGHTSILVER3  RGB(205, 205, 205)  

extern COLORREF ogColors[];
extern CBrush *ogBrushs[];

extern COLORREF lgBkColor;
extern COLORREF lgTextColor;
extern COLORREF lgHilightColor;

extern enum enumRecordState egRecordState;

extern BOOL bgIsInitialized;

// CFont : translate from logical point to physical pixel
#define PT_TO_PIXELS(dc, pt)    (- MulDiv( pt, (dc).GetDeviceCaps( LOGPIXELSY ), 72 ))


/////////////////////////////////////////////////////////////////////////////
// IDs

#define WM_USER_FIND					(WM_USER + 804)
#define WM_USER_CHANGECODEDLG			(WM_USER + 805)

/////////////////////////////////////////////////////////////////////////////
// Font variable
enum{GANTT_S_FONT, GANTT_M_FONT, GANTT_L_FONT, GANTT_XL_FONT};

extern CFont ogSmallFonts_Regular_6;
extern CFont ogSmallFonts_Regular_7;
extern CFont ogSmallFonts_Regular_8;
extern CFont ogSmallFonts_Bold_7;
extern CFont ogMSSansSerif_Regular_8;
extern CFont ogMSSansSerif_Bold_8;
extern CFont ogCourier_Bold_10;
extern CFont ogCourier_Regular_10;
extern CFont ogCourier_Regular_8;
extern CFont ogCourier_Bold_8;
extern CFont ogCourier_Regular_9;

extern CFont ogTimesNewRoman_9;
extern CFont ogTimesNewRoman_12;
extern CFont ogTimesNewRoman_16;
extern CFont ogTimesNewRoman_30;

extern CFont ogScalingFonts[30];
extern int igDaysToRead;

void InitFont();
void DeleteBrushes();
void CreateBrushes();


enum{MS_SANS6, MS_SANS8, MS_SANS12, MS_SANS16, MS_SANS6BOLD, MS_SANS8BOLD, MS_SANS12BOLD, MS_SANS16BOLD};

/////////////////////////////////////////////////////////////////////////////
// Color and brush variables

extern COLORREF ogColors[];
extern CBrush *ogBrushs[];

/////////////////////////////////////////////////////////////////////////////

struct TIMEFRAMEDATA
{
	CTime StartTime;
	CTime EndTime;
	TIMEFRAMEDATA(void)
	{StartTime=TIMENULL;EndTime=TIMENULL;}
};

/////////////////////////////////////////////////////////////////////////////
// application globals 
extern char pcgHome[4];
extern char pcgHelpPath[1024];
extern char pcgHome4[5];
extern char pcgTableExt[10];
extern bool bgNoScroll;
extern bool bgOnline;
extern CTime ogLoginTime;
class CInitialLoadDlg;
extern CInitialLoadDlg *pogInitialLoad;
/////////////////////////////////////////////////////////////////////////////


#define SetWndStatAll(clStat, plWnd)\
	if(clStat=='1') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(TRUE);}\
	else if(clStat=='0') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(FALSE);}\
	else if(clStat=='-') plWnd.ShowWindow(SW_HIDE);

#define SetWndStatPrio_1(clStat1,clStat2, plWnd)\
	if(clStat1=='-'||clStat2=='-') plWnd.ShowWindow(SW_HIDE);\
	else if(clStat1=='0'||clStat2=='0') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(FALSE);}\
	else if(clStat1=='1'||clStat2=='1') {plWnd.ShowWindow(SW_SHOW);plWnd.EnableWindow(TRUE);}

#define SetpWndStatAll(clStat, plWnd)\
	if(clStat=='1') {plWnd->ShowWindow(SW_SHOW);plWnd->EnableWindow(TRUE);}\
	else if(clStat=='0') {plWnd->ShowWindow(SW_SHOW);plWnd->EnableWindow(FALSE);}\
	else if(clStat=='-') plWnd->ShowWindow(SW_HIDE);

/////////////////////////////////////////////////////////////////////////////


// Sortierungs ENUM
/*enum
{
	_NOTSORT,_UP,_DOWN,
	_NEW,_CHANGE,_OK,_NOTOK,
	_NOTGENERATE,_ISGENERATED,_ISRELEASED
};*/


enum
{
	DLG_NEW,
	DLG_COPY,
	DLG_CHANGE
};


#define TIMER_REFRESH 0x2

// end globals
/////////////////////////////////////////////////////////////////////////////


#endif //__CCSGLOBL_H__
#endif // !defined(AFX_CCSGLOBL_H__55D90F34_191F_11D1_82BE_0080AD1DC701__INCLUDED_)
