Attribute VB_Name = "UfisLib"
Option Explicit

'=======================================
'Defines for internal ReturnCodes
'=======================================
Public Const RC_NOT_FOUND = -2       'There was no Result
Public Const RC_SUCCESS = True       'All OK
Public Const RC_FAIL = False         'Not OK

'=======================================
'Defines for CC = CallCeda() ReturnCodes
'=======================================
'CEDA Communication
Public Const CC_RC_ORA_ERROR = -12      'DB Transaction failed
Public Const CC_RC_PROC_ERROR = -11     'Ceda Process has Problems
Public Const CC_RC_COM_ERROR = -10      'Something wrong with the Communication
Public Const CC_RC_ACCESS_DENIED = -9   'Transaction or Login rejected
Public Const CC_RC_WRITE_DENIED = -8    'Transaction not permitted
Public Const CC_RC_LOST_CONNECTION = -7 'The Server died
Public Const CC_RC_PROC_CRASH = -6      'The Process died
Public Const CC_RC_NOT_CONNECTED = -5   'Could not connect to the Server
Public Const CC_RC_TIMEOUT = -4         'Got no answer from the Server
Public Const CC_RC_UNEXPECTED = -3      'Got a unexpected answer from the server
Public Const CC_RC_NOT_FOUND = -2       'There was no Result
Public Const CC_RC_QUE_WAIT = -1        'The transaction is put on the Queue
'Internal Errors
Public Const CC_RC_WRONG_BUFFER = -100

'=======================================
'Defines for general purposes
'=======================================
'CleanString()
Public Const INIT_FOR_CLIENT = -1
Public Const INIT_FOR_SERVER = -2
Public Const FOR_CLIENT = 1
Public Const FOR_SERVER = 2

Public Const FOR_INSERT = 1
Public Const FOR_UPDATE = 2
Public Const FOR_DELETE = 3
Public Const FOR_SELECT = 4

Public Const FOR_ALL = 0
Public Const FOR_SEND = -1
Public Const FOR_RECV = -2

'=======================================
'Defines for Windows NT Features
'=======================================
Public Const VER_PLATFORM_WIN32s = 0
Public Const VER_PLATFORM_WIN32_WINDOWS = 1
Public Const VER_PLATFORM_WIN32_NT = 2

Public Const WF_CPU286 = &H2&
Public Const WF_CPU386 = &H4&
Public Const WF_CPU486 = &H8&
Public Const WF_STANDARD = &H10&
Public Const WF_ENHANCED = &H20&
Public Const WF_80x87 = &H400&

Public Const SM_MOUSEPRESENT = 19

Public Const GFSR_SYSTEMRESOURCES = &H0
Public Const GFSR_GDIRESOURCES = &H1
Public Const GFSR_USERRESOURCES = &H2

Public Const MF_POPUP = &H10
Public Const MF_BYPOSITION = &H400
Public Const MF_SEPARATOR = &H800

Public Const SRCCOPY = &HCC0020
Public Const SRCERASE = &H440328
Public Const SRCINVERT = &H660046
Public Const SRCAND = &H8800C6

Public Const HWND_TOPMOST = -1
Public Const HWND_NOTOPMOST = -2
Public Const SWP_NOACTIVATE = &H10
Public Const SWP_SHOWWINDOW = &H40
 
Public Const TWIPS = 1
Public Const PIXELS = 3
Public Const RES_INFO = 2
Public Const MINIMIZED = 1
Const MAX_COMPUTERNAME_LENGTH = 15

'=======================================
'Windows NT Structures
'=======================================

Type MYVERSION
    lMajorVersion As Long
    lMinorVersion As Long
    lExtraInfo As Long
End Type

Type OSVERSIONINFO
        dwOSVersionInfoSize As Long
        dwMajorVersion As Long
        dwMinorVersion As Long
        dwBuildNumber As Long
        dwPlatformId As Long
        szCSDVersion As String * 128      '  Maintenance string for PSS usage
End Type

'Type RECT
'    Left As Integer
'    Top As Integer
'    Right As Integer
'    Bottom As Integer
'End Type

Public Type SystemInfo
    dwOemId As Long
    dwPageSize As Long
    lpMinimumApplicationAddress As Long
    lpMaximumApplicationAddress As Long
    dwActiveProcessorMask As Long
    dwNumberOfProcessors As Long
    dwProcessorType As Long
    dwAllocationGranularity As Long
    dwReserved As Long
End Type

Public Type MEMORYSTATUS
    dwLength As Long
    dwMemoryLoad As Long
    dwTotalPhys As Long
    dwAvailPhys As Long
    dwTotalPageFile As Long
    dwAvailPageFile As Long
    dwTotalVirtual As Long
    dwAvailVirtual As Long
End Type

'=======================================
'System Defines
'=======================================
Public Const LOCALE_SDECIMAL              As Long = &HE     'decimal separator
Public Const LOCALE_STHOUSAND             As Long = &HF     'thousand separator
Public Const LOCALE_SGROUPING             As Long = &H10    'digit grouping
Public Const LOCALE_IDIGITS               As Long = &H11    'number of fractional digits
Public Const LOCALE_ILZERO                As Long = &H12    'leading zeros for decimal
Public Const LOCALE_INEGNUMBER            As Long = &H1010  'negative number mode
Public Const LOCALE_SNATIVEDIGITS         As Long = &H13    'native ASCII 0-9
Public Const LOCALE_SPOSITIVESIGN         As Long = &H50    'positive sign
Public Const LOCALE_SNEGATIVESIGN         As Long = &H51    'negative sign

'=======================================
'Application Defines
'=======================================
'Public Const DEFAULT_CEDA_INI = "C:\UFIS\SYSTEM\CEDA.INI"
Public DEFAULT_CEDA_INI As String
Public UFIS_SYSTEM As String
Public UFIS_HELP As String
Public UFIS_APPL As String
Public UFIS_TMP As String
Public UFIS_UFIS As String
Public myIniPath As String
Public myIniFile As String
Public myIniFullName As String
Public myIniSection As String

'=======================================
'Application Structures
'=======================================
Public Type MaxFrame        'Using Twips
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

'=======================================
'Coordination of Pending Events
'=======================================
Public Const RC_NONE = 0
Public Const RC_PENDING = 1
Public Const RC_IGNORED = 2
Public Const RC_NEWFOCUS = 3
Public Const RELEASE = 0

Private Type PendEvent
    IsActive As Boolean
    Type As Integer
    Form As Form
    Object As Control
End Type
Private PendingEvent As PendEvent


'=======================================
'Windows NT Function Prototypes
'=======================================
Declare Function GetWindowsDirectory Lib "kernel32" Alias "GetWindowsDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Long) As Long
Declare Sub GetSystemInfo Lib "kernel32" (lpSystemInfo As SystemInfo)
Declare Sub GlobalMemoryStatus Lib "kernel32" (lpBuffer As MEMORYSTATUS)
Declare Function GetVersionEx Lib "kernel32" Alias "GetVersionExA" (ByRef lpVersionInformation As OSVERSIONINFO) As Long
Declare Function GetSystemMetrics Lib "user32" (ByVal nIndex As Long) As Long
Declare Function GetDeviceCaps Lib "gdi32" (ByVal hDC As Long, ByVal nIndex As Long) As Long
Declare Function TrackPopupMenu Lib "user32" (ByVal hMenu As Long, ByVal wFlags As Long, ByVal x As Long, ByVal y As Long, ByVal nReserved As Long, ByVal hWnd As Long, lpReserved As Any) As Long
Declare Function GetMenu Lib "user32" (ByVal hWnd As Long) As Long
Declare Function GetSubMenu Lib "user32" (ByVal hMenu As Long, ByVal nPos As Long) As Long
Declare Function GetDesktopWindow Lib "user32" () As Long
Declare Function GetDC Lib "user32" (ByVal hWnd As Long) As Long
Declare Function ReleaseDC Lib "user32" (ByVal hWnd As Long, ByVal hDC As Long) As Long
Declare Function BitBlt Lib "gdi32" (ByVal hDestDC As Long, ByVal x As Long, ByVal y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal XSrc As Long, ByVal YSrc As Long, ByVal dwRop As Long) As Long
Declare Sub SetWindowPos Lib "user32" (ByVal hWnd As Long, ByVal hWndInsertAfter As Long, ByVal x As Long, ByVal y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long)
Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, lpKeyName As Any, ByVal lpDefault As String, ByVal lpRetunedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Declare Function GetProfileString Lib "kernel32" Alias "GetProfileStringA" (ByVal lpAppName As String, lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long) As Long
Declare Function waveOutGetNumDevs Lib "winmm" () As Long
Declare Function GetSystemDirectory Lib "kernel32" Alias "GetSystemDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Long) As Long
Declare Function sndPlaySound Lib "winmm" Alias "sndPlaySoundA" (ByVal lpszSoundName As String, ByVal uFlags As Long) As Long
Private Declare Function GetComputerName Lib "kernel32" Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As Long) As Long

Public Declare Function GetThreadLocale Lib "kernel32" () As Long
Public Declare Function GetSystemDefaultLCID Lib "kernel32" () As Long
Public Declare Function GetLocaleInfo Lib "kernel32" Alias "GetLocaleInfoA" (ByVal Locale As Long, ByVal LCType As Long, ByVal lpLCData As String, ByVal cchData As Long) As Long

Public VisibleFrame As Frame
Public CedaIniFile As String
Public HomeAirport As String

Public ApplicationIsStarted As Boolean
Public CedaIsConnected As Boolean
Public ModalMsgIsOpen As Boolean
Public ShutDownRequested As Boolean
Public AutoArrange As Boolean
Public ErrorIgnored As Boolean
Public UserAnswer As String
Public CurMem As Integer

Dim WinVersion As Integer
Dim SoundAvailable As Integer

Public Const MonthList = "JAN,FEB,MAR,APR,MAY,JUN,JUL,AUG,SEP,OCT,NOV,DEC"
'=======================================
'Special Color Values
'=======================================
Public Const LightestYellow = &HC0FFFF
Public Const LightYellow = &H80FFFF
Public Const NormalYellow = &HFFFF&
Public Const DarkYellow = &HC0C0&
Public Const NearlyGreen = &HC0FFC0
Public Const LightestGreen = &HC0FFC0
Public Const LightGreen = &H80FF80
Public Const NormalGreen = &HC000&
Public Const DarkGreen = &H8000&
Public Const DarkestGreen = &H4000&
Public Const LightestRed = &HC0C0FF
Public Const LightRed = &H8080FF
Public Const LightGray = &HE0E0E0
Public Const NormalGray = &HC0C0C0
Public Const DarkGray = &H808080

Function GetIniEntry(cpFileName As String, cpSection1 As String, cpSection2 As String, cpKeyWord As String, cpDefault As String) As String
    Dim IniFileName As String
    Dim TextLine As String
    Dim clHeader As String
    Dim clResult As String
    Dim blLoopEnd As Boolean
    Dim blSectionFound As Boolean
    Dim blBlockFound As Boolean
    Dim ilPos As Integer
    Dim ilCount As Integer
    
    If DEFAULT_CEDA_INI = "" Then GetUfisDir
    IniFileName = cpFileName
    If IniFileName = "" Then IniFileName = DEFAULT_CEDA_INI
    clResult = ""
    blLoopEnd = False
    blBlockFound = False
    ilCount = 0
    Do While (Not blLoopEnd) And (ilCount < 2)
        ilCount = ilCount + 1
        blSectionFound = False
        If ilCount = 1 Then
            clHeader = "[" & cpSection1 & "]"
        Else
            clHeader = "[" & cpSection2 & "]"
        End If
        Open IniFileName For Input As #1
        Do While (Not EOF(1)) And (Not blLoopEnd)
            Line Input #1, TextLine
            TextLine = LTrim(TextLine)
            If InStr(TextLine, "[") = 1 Then 'New Section
                If Not blSectionFound Then
                    If InStr(TextLine, clHeader) = 1 Then
                        blSectionFound = True 'Given Section found
                    End If
                Else
                    blLoopEnd = True
                End If
            Else
                If blSectionFound Then
                    If blBlockFound = True Then
                        If InStr(TextLine, "BLOCK_END") = 1 Then
                            blLoopEnd = True
                        Else
                            clResult = clResult & TextLine & vbNewLine
                        End If
                    Else
                        If InStr(TextLine, cpKeyWord) = 1 Then
                            'KeyWord found
                            ilPos = InStr(TextLine, "=")
                            If ilPos > 0 Then
                                clResult = Trim(Mid$(TextLine, ilPos + 1))
                                If clResult = "BLOCK_BEGIN" Then
                                    blBlockFound = True
                                    clResult = ""
                                Else
                                    blLoopEnd = True
                                End If
                            Else
                                'Nothing, we search until the end of section
                            End If
                        End If
                    End If
                End If
            End If
        Loop
        Close #1
        If clResult = "" Then
            blLoopEnd = False
        End If
    Loop
    If clResult = "" Then
        clResult = cpDefault
    End If
    GetIniEntry = clResult
End Function

Function GetItem(cpTxt As String, ipNbr As Integer, cpSep As String) As String
    Dim Result
    Dim ilSepLen As Integer
    Dim ilFirstPos As Integer
    Dim ilLastPos As Integer
    Dim ilItmLen As Integer
    Dim ilItmNbr As Integer

    Result = ""
    If ipNbr > 0 Then
        ilSepLen = Len(cpSep)
        ilFirstPos = 1
        ilLastPos = 1
        ilItmNbr = 0
        While (ilItmNbr < ipNbr) And (ilLastPos > 0)
            ilItmNbr = ilItmNbr + 1
            ilLastPos = InStr(ilFirstPos, cpTxt, cpSep)
            If (ilItmNbr < ipNbr) And (ilLastPos > 0) Then ilFirstPos = ilLastPos + ilSepLen
        Wend
        If ilLastPos < ilFirstPos Then ilLastPos = Len(cpTxt) + 1
        If (ilItmNbr = ipNbr) And (ilLastPos > ilFirstPos) Then
            ilItmLen = ilLastPos - ilFirstPos
            Result = Mid(cpTxt, ilFirstPos, ilItmLen)
        End If
    End If
    GetItem = RTrim(Result)
End Function

Function GetItemNo(itemlist As String, ItemValue As String) As Integer
    Dim ItmLen As Integer
    Dim ItmPos As Integer
    ItmLen = Len(ItemValue) + 1
    ItmPos = InStr(itemlist, ItemValue)
    If ItmPos > 0 Then
        GetItemNo = ((ItmPos - 1) \ ItmLen) + 1
    Else
        GetItemNo = -1
    End If
End Function

Function GetFieldValue(FieldName As String, DataList As String, FieldList As String) As String
    Dim ItmNbr As Integer
    ItmNbr = GetItemNo(FieldList, FieldName)
    GetFieldValue = GetItem(DataList, ItmNbr, ",")
End Function

Public Function CedaDateToVb(cpDate As String) 'as Variant
    If Trim(cpDate) <> "" Then
        CedaDateToVb = DateSerial(Val(Mid(cpDate, 1, 4)), Val(Mid(cpDate, 5, 2)), Val(Mid(cpDate, 7, 2)))
    Else
        CedaDateToVb = ""
    End If
End Function

Public Function CedaTimeToVb(cpDate As String) 'as Variant
Dim ilHH As Integer
Dim ilMM As Integer
Dim ilSS As Integer
    If Trim(cpDate) <> "" Then
        If Len(cpDate) > 8 Then
            CedaTimeToVb = TimeSerial(Val(Mid(cpDate, 9, 2)), Val(Mid(cpDate, 11, 2)), Val(Mid(cpDate, 13, 2)))
        Else
            If InStr(cpDate, ":") > 0 Then
                ilHH = Val(GetItem(cpDate, 1, ":"))
                ilMM = Val(GetItem(cpDate, 2, ":"))
                ilSS = Val(GetItem(cpDate, 3, ":"))
                CedaTimeToVb = TimeSerial(ilHH, ilMM, ilSS)
            Else
                CedaTimeToVb = TimeSerial(Val(Mid(cpDate, 1, 2)), Val(Mid(cpDate, 3, 2)), Val(Mid(cpDate, 5, 2)))
            End If
        End If
    Else
        CedaTimeToVb = ""
    End If
End Function

Public Function CedaFullDateToVb(cpDate As String) 'as Variant
Dim MyDate
Dim myTime
    MyDate = CedaDateToVb(cpDate)
    myTime = CedaTimeToVb(cpDate)
    If (MyDate <> "") And (myTime <> "") Then
        CedaFullDateToVb = MyDate + myTime
    Else
        CedaFullDateToVb = ""
    End If
End Function

Public Function VbDateStrgToCeda(cpDate As String, cpInForm As String) As String
Dim clResult As String
Dim UsedFormat As String
Dim clYear As String
Dim clMon As String
Dim clDay As String
Dim clHour As String
Dim clMin As String
Dim clSec As String
Dim ilInLen As Integer
Dim i As Integer
Dim clCod As String
Dim clChr As String
    clResult = ""
    clYear = ""
    clMon = ""
    clDay = ""
    clHour = ""
    clMin = ""
    clSec = ""
    ilInLen = Len(cpInForm)
    UsedFormat = cpInForm
    For i = 1 To ilInLen
        clCod = Mid(UsedFormat, i, 1)
        clChr = Mid(cpDate, i, 1)
        Select Case clCod
            Case "y"
                clYear = clYear & clChr
            Case "m"
                clMon = clMon & clChr
            Case "d"
                clDay = clDay & clChr
            Case Else
        End Select
    Next
    clResult = clYear & clMon & clDay
    VbDateStrgToCeda = clResult
End Function

Public Function DateInputFormatToCeda(cpDate As String, cpInForm As String) As String
Dim myCedaDate As String
Dim myChkDate As String
    myCedaDate = VbDateStrgToCeda(cpDate, cpInForm)
    myChkDate = Format(CedaDateToVb(myCedaDate), cpInForm)
    If myChkDate <> cpDate Then
        myCedaDate = ""
    End If
    DateInputFormatToCeda = myCedaDate
End Function

Function DeviceColors(hDC As Long) As Single
Const PLANES = 14
Const BITSPIXEL = 12
    DeviceColors = 2 ^ (GetDeviceCaps(hDC, PLANES) * GetDeviceCaps(hDC, BITSPIXEL))
End Function

Function GetSysIni(section, key)
Dim RetVal As String, AppName As String, worked As Integer
    RetVal = String$(255, 0)
    worked = GetPrivateProfileString(section, key, "", RetVal, Len(RetVal), "System.ini")
    If worked = 0 Then
        GetSysIni = "unknown"
    Else
        GetSysIni = Left(RetVal, InStr(RetVal, Chr(0)) - 1)
    End If
End Function

Function GetWinIni(section, key)
Dim RetVal As String, AppName As String, worked As Integer
    RetVal = String$(255, 0)
    worked = GetProfileString(section, key, "", RetVal, Len(RetVal))
    If worked = 0 Then
        GetWinIni = "unknown"
    Else
        GetWinIni = Left(RetVal, InStr(RetVal, Chr(0)) - 1)
    End If
End Function

Function SystemDirectory() As String
Dim WinPath As String
    WinPath = String(145, Chr(0))
    SystemDirectory = Left(WinPath, GetSystemDirectory(WinPath, 145))
End Function

Function WindowsDirectory() As String
Dim WinPath As String
    WinPath = String(145, Chr(0))
    WindowsDirectory = Left(WinPath, GetWindowsDirectory(WinPath, 145))
End Function

Function WindowsVersion() As MYVERSION
Dim myOS As OSVERSIONINFO, WinVer As MYVERSION
Dim lResult As Long

    myOS.dwOSVersionInfoSize = Len(myOS)    'should be 148
    
    lResult = GetVersionEx(myOS)
        
    'Fill user type with pertinent info
    WinVer.lMajorVersion = myOS.dwMajorVersion
    WinVer.lMinorVersion = myOS.dwMinorVersion
    WinVer.lExtraInfo = myOS.dwPlatformId
    
    WindowsVersion = WinVer

End Function

Public Sub SetFormOnTop(UsedForm As Form, OnTop As Boolean)
    If OnTop = True Then
       SetWindowPos UsedForm.hWnd, HWND_TOPMOST, UsedForm.Left / 15, _
                    UsedForm.Top / 15, UsedForm.Width / 15, _
                    UsedForm.Height / 15, SWP_NOACTIVATE Or SWP_SHOWWINDOW
    Else
       SetWindowPos UsedForm.hWnd, HWND_NOTOPMOST, UsedForm.Left / 15, _
                    UsedForm.Top / 15, UsedForm.Width / 15, _
                    UsedForm.Height / 15, SWP_NOACTIVATE Or SWP_SHOWWINDOW
    End If
End Sub

Public Function CountVisibleForms() As Integer
Dim i As Integer
Dim cnt As Integer
    cnt = 0
    For i = 0 To Forms.Count - 1
        If Forms(i).Visible = True Then cnt = cnt + 1
    Next i
    CountVisibleForms = cnt
End Function

Public Function CheckDateField(UseObject As Control, UseFormat As String, SetError As Boolean)
    With UseObject
        .Tag = DateInputFormatToCeda(.Text, UseFormat)
        If .Tag = "" Then
            .BackColor = vbRed
            .ForeColor = vbWhite
            CheckDateField = False
            If SetError Then
                PendingEvent.IsActive = True
                Set PendingEvent.Form = Screen.ActiveForm
                Set PendingEvent.Object = UseObject
                PendingEvent.Type = 1
            End If
        Else
            .BackColor = vbWindowBackground
            .ForeColor = vbWindowText
            CheckDateField = True
            If SetError Then PendingEvent.IsActive = False
        End If
    End With
End Function

Public Sub DoNothing()
    'did nothing
End Sub

Public Sub ShowChildForm(Caller As Form, CallButton As Control, ChildForm As Form, TaskValue As String)
    ChildForm.RegisterMyParent Caller, CallButton, TaskValue
    CheckFormOnTop Caller, False
    ChildForm.Show , Caller
    CheckFormOnTop Caller, True
End Sub

Public Function GetUrnoFromSqlKey(KeyText As String) As String
Dim Result As String
Dim tmpTxt As String
Dim tmpOffs As Long
    tmpTxt = Trim(KeyText)
    tmpOffs = InStr(tmpTxt, "URNO")
    If tmpOffs > 0 Then
        tmpOffs = tmpOffs + 4
        tmpTxt = LTrim(Mid(tmpTxt, tmpOffs))
        If Left(tmpTxt, 1) = "=" Then
            tmpTxt = LTrim(Mid(tmpTxt, 2))
        Else
            'may be like, <>, o.s.o.
        End If
        If Left(tmpTxt, 1) = "'" Then
            tmpTxt = GetItem(tmpTxt, 2, "'")
        End If
    Else
        'normally the URNO itself
    End If
    Result = GetItem(tmpTxt, 1, " ")
    GetUrnoFromSqlKey = Result
End Function
Public Function CleanNullValues(myString As String)
    Dim Result As String
    Dim tmpStrg As String
    Result = myString
    If Result = "" Then Result = " "
    Do
        tmpStrg = Result
        Result = Replace(tmpStrg, ",,", ", ,", 1, -1, vbBinaryCompare)
    Loop While Result <> tmpStrg
    If Left(Result, 1) = "," Then Result = " " & Result
    If Right(Result, 1) = "," Then Result = Result & " "
    CleanNullValues = Result
End Function
Public Function CleanString(myText As String, myMethod As Integer, CheckOldVersion As Boolean) As String
    Dim Result As String
    Static FunctionIsInitialized As Boolean
    Static ClientPatchList As String
    Static NewServerPatchList As String
    Static OldServerPatchList As String
    'Until SEP'99 we used ServerPatchValues up from ASCII(176),
    'but because these values matched chinese (and greek) characters
    'we now use values below ASCII(31).
    'But because not all applications (FDIHDL) or installations are changed
    'and using the new version of patch values, we here check both.
    'This can be forced or suppressed by setting the CheckOldVersion parameter
    If Not FunctionIsInitialized Then
        ClientPatchList = BuildBinaryString("34,39,44,10,13")
        NewServerPatchList = BuildBinaryString("23,24,25,28,29")
        OldServerPatchList = BuildBinaryString("176,177,178,179,180")
        FunctionIsInitialized = True
    End If
    Select Case myMethod
        Case INIT_FOR_CLIENT
            ClientPatchList = BuildBinaryString(myText)
        Case INIT_FOR_SERVER
            If CheckOldVersion Then
                OldServerPatchList = BuildBinaryString(myText)
            Else
                NewServerPatchList = BuildBinaryString(myText)
            End If
        Case FOR_CLIENT
            Result = ReplaceChars(myText, NewServerPatchList, ClientPatchList)
            If (Result = myText) And CheckOldVersion Then
                'Nothing changed, so we try the old version
                Result = ReplaceChars(myText, OldServerPatchList, ClientPatchList)
            End If
            'VB needs CR/LF
            Result = Replace(Result, vbLf, vbNewLine, 1, -1, vbBinaryCompare)
        Case FOR_SERVER
            'the server needs patched LF only
            Result = Replace(myText, vbNewLine, vbLf, 1, -1, vbBinaryCompare)
            If CheckOldVersion Then
                Result = ReplaceChars(Result, ClientPatchList, OldServerPatchList)
            Else
                Result = ReplaceChars(Result, ClientPatchList, NewServerPatchList)
            End If
        Case Else
            Result = myText
    End Select
    CleanString = Result
End Function

Public Function BuildBinaryString(myAscList As String) As String
    Dim Result As String
    Dim ItmNbr As Integer
    Dim ItmVal 'as variant
    Result = ""
    ItmNbr = 0
    Do
        ItmNbr = ItmNbr + 1
        ItmVal = GetItem(myAscList, ItmNbr, ",")
        If ItmVal <> "" Then
            Result = Result & Chr(ItmVal)
        End If
    Loop While ItmVal <> ""
    BuildBinaryString = Result
End Function
Public Function ReplaceChars(myText As String, LookFor As String, ReplaceWith As String) As String
    Dim Result As String
    Dim ilLen As Integer
    Dim clLook As String
    Dim clPatch As String
    Dim i As Integer
    Result = myText
    ilLen = Len(LookFor)
    For i = 1 To ilLen
        clLook = Mid(LookFor, i, 1)
        clPatch = Mid(ReplaceWith, i, 1)
        Result = Replace(Result, clLook, clPatch, 1, -1, vbBinaryCompare)
    Next
    ReplaceChars = Result
End Function

Public Sub CheckFormOnTop(ActForm As Form, SetValue As Boolean)
    If ActForm.Visible = True Then
        If ActForm.OnTop.Value = 1 Then SetFormOnTop ActForm, SetValue
    End If
End Sub

Public Function GetTimePart(CedaDate As String) As String
    Dim tmpVal As String
    If Trim(CedaDate) <> "" Then tmpVal = Mid(CedaDate, 9, 2) & ":" & Mid(CedaDate, 11, 2) Else tmpVal = ""
    GetTimePart = tmpVal
End Function

Public Function GetTimeStamp(MinuteOffset As Integer) As String
    Dim tmpTime 'as variant
    tmpTime = DateAdd("n", MinuteOffset, Now)
    GetTimeStamp = Format(tmpTime, "yyyymmddhhmmss")
End Function

Public Function CheckValidTime(tmpData As String, tmpAllow As String) As Boolean
    Dim tmpChk As String
    Dim tmpVal As Integer

    tmpVal = Val(tmpData)
    tmpChk = Right("0000" & Trim(str(tmpVal)), 4)
    If tmpChk = tmpData Then CheckValidTime = True Else CheckValidTime = False
End Function

Public Function BuildAftFkey(tmpAlc3 As String, tmpFltn As String, tmpFlns As String, tmpDate As String, tmpAdid As String) As String
    Dim Result As String
    Dim tmpData As String

    Result = "00000XXX#00000000U"
    tmpData = Trim(tmpAlc3)
    If tmpData <> "" Then
        Mid(Result, 6, 3) = tmpData
    End If
    tmpData = Trim(tmpFltn)
    If tmpData <> "" Then
        tmpData = Right("00000" & tmpData, 5)
        Mid(Result, 1, 5) = tmpData
    End If
    tmpData = Trim(tmpFlns)
    If tmpData <> "" Then
        Mid(Result, 9, 1) = tmpData
    End If
    tmpData = Trim(tmpDate)
    If tmpData <> "" Then
        Mid(Result, 10, 8) = tmpData
    End If
    tmpData = Trim(tmpAdid)
    If tmpData <> "" Then
        Mid(Result, 18, 1) = tmpData
    End If
    BuildAftFkey = Result
End Function

Sub SetItem(itemlist As String, ItemNbr As Integer, ItemSep As String, NewValue As String)
    Dim Result As String
    Dim CurItm As Integer
    Dim ReqItm As Integer
    Dim MaxItm As Integer

    Result = ""
    ReqItm = ItemNbr - 1
    For CurItm = 1 To ReqItm
        Result = Result & GetItem(itemlist, CurItm, ItemSep) & ItemSep
    Next
    Result = Result & NewValue & ItemSep
    MaxItm = ItemCount(itemlist, ItemSep)
    ReqItm = ReqItm + 2
    For CurItm = ReqItm To MaxItm
        Result = Result & GetItem(itemlist, CurItm, ItemSep) & ItemSep
    Next
    itemlist = Left(Result, Len(Result) - Len(ItemSep))
End Sub
Function ItemCount(itemlist As String, ItemSep As String) As Integer
    Dim Result As Integer
    Dim SepPos As Integer
    Dim SepLen As Integer

    If itemlist <> "" Then
        Result = 1
        SepLen = Len(ItemSep)
        SepPos = 1
        Do
            SepPos = InStr(SepPos, itemlist, ItemSep, vbBinaryCompare)
            If SepPos > 0 Then
                Result = Result + 1
                SepPos = SepPos + SepLen
            End If
        Loop While SepPos > 0
    Else
        Result = 0
    End If
    ItemCount = Result
End Function

Public Function DecodeSsimDayFormat(tmpDate As String, InFormat As String, OutFormat As String) As String
    Dim Result As String
    Dim tmpDay As String
    Dim tmpMonth As String
    Dim tmpYear As String
    Dim tmpMonVal As Integer

    Result = tmpDate
    Select Case InFormat
        Case "SSIM2"
            tmpDay = Left(tmpDate, 2)
            tmpMonth = Mid(tmpDate, 3, 3)
            tmpMonVal = GetItemNo(MonthList, tmpMonth)
            tmpMonth = Right("00" & Trim(str(tmpMonVal)), 2)
            tmpYear = Right(tmpDate, 2)
            If tmpYear = "99" Then tmpYear = "19" & tmpYear Else tmpYear = "20" & tmpYear
        Case "SSIM4"
            tmpDay = Left(tmpDate, 2)
            tmpMonth = Mid(tmpDate, 3, 3)
            tmpMonVal = GetItemNo(MonthList, tmpMonth)
            tmpMonth = Right("00" & Trim(str(tmpMonVal)), 2)
            tmpYear = Right(tmpDate, 4)
        Case "CEDA"
            tmpYear = Left(tmpDate, 4)
            tmpMonth = Mid(tmpDate, 5, 2)
            tmpDay = Right(tmpDate, 2)
        Case Else
    End Select
    'here we always have a 4 digit year, 2 digit month and 2 digit day
    Select Case OutFormat
        Case "CEDA"
            Result = tmpYear & tmpMonth & tmpDay
        Case "SSIM2"
            tmpMonVal = Val(tmpMonth)
            tmpMonth = GetItem(MonthList, tmpMonVal, ",")
            tmpYear = Right(tmpYear, 2)
            Result = tmpDay & tmpMonth & tmpYear
        Case Else
    End Select
    DecodeSsimDayFormat = Result
End Function

Public Function BuildProperAftFlno(AftAirl As String, AftFltn As String, AftFlns As String)
    Dim Result As String
    Dim tmpAirl As String
    Dim tmpFltn As String
    Dim tmpFlns As String
    Dim tmpSffx As String
    Dim ilLen As Integer
    tmpAirl = Trim(AftAirl)
    tmpFltn = Trim(AftFltn)
    tmpFlns = Trim(AftFlns)
    While Left(tmpFltn, 1) = "0"
        tmpFltn = Mid(tmpFltn, 2)
    Wend
    tmpSffx = Right(tmpFltn, 1)
    If tmpSffx <> "" Then
        If tmpSffx < "0" Or tmpSffx > "9" Then
            tmpFlns = tmpSffx
            tmpFltn = Left(tmpFltn, Len(tmpFltn) - 1)
        End If
    End If
    ilLen = Len(tmpFltn)
    If (ilLen > 0) And (ilLen < 3) Then tmpFltn = Right("000" & tmpFltn, 3)
    If tmpFlns = "" Then tmpFlns = " "
    Result = Left(tmpAirl & "   ", 3)
    Result = Result & Left(tmpFltn & "     ", 5)
    Result = Result & tmpFlns
    AftAirl = tmpAirl
    AftFltn = tmpFltn
    AftFlns = tmpFlns
    BuildProperAftFlno = RTrim(Result)
End Function

Sub SleepSomeSeconds(SecondValue As Long)
    Dim StartTime
    StartTime = DateAdd("s", SecondValue, Now)
    While StartTime > Now
        'Sleep
    Wend
End Sub

Function CheckValidDate(ChkDate As String) As Boolean
    Dim VbDate As String
    Dim CedaDate As String
    VbDate = CedaDateToVb(ChkDate)
    CedaDate = Format(VbDate, "yyyymmdd")
    If CedaDate = ChkDate Then
        CheckValidDate = True
    Else
        CheckValidDate = False
    End If
End Function

Function GetFileAndPath(UseFileName As String, ActFile As String, ActPath As String) As Boolean
    Dim Result As Boolean
    Dim tmpDat As String
    Dim ItmCnt As Integer
    Dim CurItm As Integer
    ItmCnt = ItemCount(UseFileName, "\")
    ActFile = GetItem(UseFileName, ItmCnt, "\")
    ActPath = ""
    ItmCnt = ItmCnt - 1
    For CurItm = 1 To ItmCnt
        ActPath = ActPath & GetItem(UseFileName, CurItm, "\") & "\"
    Next
    If Len(ActPath) > 0 Then ActPath = Left(ActPath, Len(ActPath) - 1)
    'MsgBox ActFile
    'MsgBox ActPath
    GetFileAndPath = True
End Function

Function DoesKeyExist(ByRef refCollection As Collection, refKey As String) As Boolean
    On Error GoTo DoesNotExist
    refCollection.Item (refKey)
    DoesKeyExist = True
    Exit Function
DoesNotExist:
    DoesKeyExist = False
    Err.Clear
End Function

Function GetNumericFieldFromWhere(strWhere As String, strSearchField As String) As String
    Dim ilPos As Integer
    ilPos = InStr(1, strWhere, strSearchField, vbTextCompare)

    If ilPos < 1 Then   ' we didn't find anything
        GetNumericFieldFromWhere = ""
        Exit Function
    Else
        Dim ilFirstPos As Integer
        Dim ilLastPos As Integer
        ilFirstPos = ilPos + 4
        While IsNumeric(Mid(strWhere, ilFirstPos, 1)) <> True
            ilFirstPos = ilFirstPos + 1
        Wend
        ilLastPos = ilFirstPos + 1
        While IsNumeric(Mid(strWhere, ilLastPos, 1)) <> False
            ilLastPos = ilLastPos + 1
        Wend
        GetNumericFieldFromWhere = Trim(Mid(strWhere, ilFirstPos, ilLastPos - ilFirstPos))
    End If
End Function

Function WriteErrToLog(sFileName As String, sFunction As String, ErrObj As ErrObject) As Boolean
    On Error Resume Next
    Dim sText As String
    sText = CStr(Now) + " " + sFunction + vbCrLf

    Open sFileName For Append As #1
        Print #1, sText
    Close #1
End Function

Function RoundCutNumber(ByVal sNumber As String, iRoundDigits As Integer) As String
    On Error GoTo ErrHdl
    Dim ilDecSepPos As Integer
    Dim i As Integer
    Dim strDecSep As String
    Dim strFormat As String
    Dim LCID As Long

'    LCID = GetSystemDefaultLCID()
    strDecSep = GetUserLocaleInfo(0, LOCALE_SDECIMAL)

    ' building the format-string
    strFormat = "#####0"
    If iRoundDigits > 0 Then
        strFormat = strFormat & "." 'strDecSep
    End If
    For i = 1 To iRoundDigits
        strFormat = strFormat & "0"
    Next i

    ilDecSepPos = InStr(1, sNumber, ".", vbBinaryCompare)
    If IsNumeric(sNumber) = True Then
        If ilDecSepPos = 0 Then     'not found
'            If Len(sNumber) > 2 Then
'                If sNumber < 0 Then
'                    sNumber = Left(sNumber, Len(sNumber) - 2) - Round("0" & strDecSep & Right(sNumber, 2), 2)
'                Else
'                    sNumber = Left(sNumber, Len(sNumber) - 2) + Round("0" & strDecSep & Right(sNumber, 2), 2)
'                End If
'            End If
        Else
            If Len(sNumber) > 2 Then
                If sNumber < 0 Then
                    sNumber = Left(sNumber, ilDecSepPos - 1) - Round("0" & strDecSep & Right(sNumber, Len(sNumber) - ilDecSepPos), 2)
                Else
                    sNumber = Left(sNumber, ilDecSepPos - 1) + Round("0" & strDecSep & Right(sNumber, Len(sNumber) - ilDecSepPos), 2)
                End If
            End If
        End If
        RoundCutNumber = Format(sNumber, strFormat)
        If InStr(1, RoundCutNumber, strDecSep, vbTextCompare) > 0 Then
            RoundCutNumber = Left(RoundCutNumber, InStr(1, RoundCutNumber, strDecSep, vbTextCompare) - 1) + strDecSep + Right(RoundCutNumber, 2)
        End If
    Else
        RoundCutNumber = ""
    End If
    Exit Function
ErrHdl:
    RoundCutNumber = Format(sNumber, strFormat)
    RoundCutNumber = Left(RoundCutNumber, InStr(1, RoundCutNumber, ",", vbTextCompare) - 1) + "." + Right(RoundCutNumber, 2)
End Function

Public Function GetUserLocaleInfo(ByVal dwLocaleID As Long, ByVal dwLCType As Long) As String
    ' see http://www.mvps.org/vbnet/index.html?code/locale/localenumerics.htm
    ' usage:
    '   Dim LCID As Long
    '   LCID = GetSystemDefaultLCID()
    ' then: GetUserLocaleInfo(LCID, LOCALE_##########)
    '   LOCALE_SDECIMAL         Character(s) used as the decimal separator. The maximum allowed is four.
    '   LOCALE_STHOUSAND        Character(s) used to separate groups of digits to the left of the decimal. The maximum allowed is four.
    '   LOCALE_SGROUPING        Sizes for each group of digits to the left of the decimal. An explicit size is needed for each group,
    '                           and sizes are separated by semicolons. If the last value is zero, the preceding value is repeated. For
    '                           example, to group thousands, specify 3;0. Indic locales group the first thousand and then group
    '                           by hundreds - for example 12,34,56,789, which is represented by 3;2;0.
    '   LOCALE_IDIGITS          Number of fractional digits. The maximum allowed is two.
    '   LOCALE_ILZERO           Specifier for leading zeros in decimal fields. The maximum allowed is two. The specifier can be one of the following values:
    '                           Select Case GetUserLocaleInfo(LCID, LOCALE_ILZERO)
    '                               Case "0": Text5.Text = "0 - No leading zeros."
    '                               Case "1": Text5.Text = "1 - Has leading zeros."
    '                           End Select
    '   LOCALE_INEGNUMBER       Negative number mode, that is, the format for a negative
    '                           number. The maximum allowed is two. The mode can be one of these values:
    '                           Select Case GetUserLocaleInfo(LCID, LOCALE_INEGNUMBER)
    '                               Case "0": Text6.Text = "0 - Left parenthesis, number, right parenthesis, ie (1.1)"
    '                               Case "1": Text6.Text = "1 - Negative sign, number, ie -1.1"
    '                               Case "2": Text6.Text = "2 - Negative sign, space, number, ie - 1.1"
    '                               Case "3": Text6.Text = "3 - Number, negative sign, ie 1.1-"
    '                               Case "4": Text6.Text = "4 - Number, space, negative sign, ie 1.1 -"
    '                           End Select
    '   LOCALE_SNATIVEDIGITS    Native equivalents to ASCII zero through 9.
    '   LOCALE_SNEGATIVESIGN    String value for the negative sign. The maximum allowed is five.
    '   LOCALE_SPOSITIVESIGN    String value for the positive sign. The maximum allowed is five.

    Dim sReturn As String
    Dim lRet As Long

    'call the function passing the Locale type
    'variable to retrieve the required size of
    'the string buffer needed
    lRet = GetLocaleInfo(dwLocaleID, dwLCType, sReturn, Len(sReturn))

    If lRet Then                                                         'if successful..
        sReturn = Space$(lRet)                                            'pad the buffer with spaces
        lRet = GetLocaleInfo(dwLocaleID, dwLCType, sReturn, Len(sReturn)) 'and call again passing the buffer
        If lRet Then                                                      'if successful (lRet > 0)
            GetUserLocaleInfo = Left$(sReturn, lRet - 1)                   'lRet holds the size of the string including the terminating null
        End If
    End If
End Function

Public Function GetWorkstationName() As String
    Dim Buffer$, Result&, l&, CName$
    l = MAX_COMPUTERNAME_LENGTH + 1
    Buffer = Space$(l)
    Result = GetComputerName(Buffer, l)
    
    If Result = 1 Then
      CName = Left$(Buffer, InStr(1, Buffer, Chr$(0)) - 1)
      'Label1.Caption = "Dieser Rechner hei�t: " & Buffer
      GetWorkstationName = CName
    End If
End Function

Public Sub DisplayHtmHelpFile()
    Dim HelpFilePath As String
    Dim HelpFileName As String
    Dim myHelpPath As String
    Static myHelpAppId As Variant
    
    If myHelpAppId > 0 Then
        On Error Resume Next
        Err.Description = ""
        AppActivate myHelpAppId, False
        If Err.Description = "" Then
            SendKeys "%{F4}", True
        End If
    End If
    HelpFileName = App.EXEName & ".chm"
    'HelpFilePath = "C:\Ufis\Help\" & HelpFileName
    HelpFilePath = UFIS_HELP & "\" & HelpFileName
    myHelpAppId = Shell("HH " & HelpFilePath, vbNormalFocus)
End Sub

Public Sub GetUfisDir()
    Dim EnvString As String
    
    EnvString = Environ("UFISSYSTEM")
    If EnvString <> "" Then
        UFIS_SYSTEM = EnvString
    Else
        UFIS_SYSTEM = "C:\UFIS\SYSTEM"
    End If
    EnvString = Environ("UFISHELP")
    If EnvString <> "" Then
        UFIS_HELP = EnvString
    Else
        UFIS_HELP = "C:\UFIS\HELP"
    End If
    EnvString = Environ("UFISAPPL")
    If EnvString <> "" Then
        UFIS_APPL = EnvString
    Else
        UFIS_APPL = "C:\UFIS\APPL"
    End If
    EnvString = Environ("UFISTMP")
    If EnvString <> "" Then
        UFIS_TMP = EnvString
    Else
        UFIS_TMP = "C:\TMP"
    End If
    EnvString = Environ("CEDA")
    If EnvString <> "" Then
        DEFAULT_CEDA_INI = EnvString
    Else
        DEFAULT_CEDA_INI = "C:\UFIS\SYSTEM\CEDA.INI"
    End If
    UFIS_UFIS = GetItem(DEFAULT_CEDA_INI, 1, "\") & "\" & GetItem(DEFAULT_CEDA_INI, 2, "\")
End Sub

