VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomct2.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmViewEditor 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   7035
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7035
   ScaleWidth      =   9000
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtMaxAbsWeek 
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   3840
      TabIndex        =   44
      Text            =   "3"
      Top             =   5400
      Width           =   855
   End
   Begin VB.TextBox txtYear 
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   2760
      TabIndex        =   42
      Text            =   "2000"
      Top             =   5400
      Width           =   855
   End
   Begin VB.CommandButton cmdApply 
      Caption         =   "1079"
      Height          =   375
      Left            =   7755
      TabIndex        =   40
      Tag             =   "1079"
      Top             =   5370
      Width           =   1095
   End
   Begin VB.ComboBox cmbViewName 
      Height          =   315
      Left            =   120
      Sorted          =   -1  'True
      TabIndex        =   3
      Text            =   "<Default>"
      Top             =   5400
      Width           =   2295
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "1077"
      Height          =   375
      Left            =   5355
      TabIndex        =   2
      Tag             =   "1077"
      Top             =   5370
      Width           =   1095
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "1078"
      Height          =   375
      Left            =   6555
      TabIndex        =   1
      Tag             =   "1078"
      Top             =   5370
      Width           =   1095
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "1003"
      Height          =   375
      Left            =   3960
      TabIndex        =   0
      Tag             =   "1003"
      Top             =   6495
      Width           =   1095
   End
   Begin TabDlg.SSTab tbsOptions 
      Height          =   4575
      Left            =   120
      TabIndex        =   4
      Top             =   240
      Width           =   8730
      _ExtentX        =   15399
      _ExtentY        =   8070
      _Version        =   393216
      Style           =   1
      Tabs            =   5
      Tab             =   2
      TabsPerRow      =   5
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "1086"
      TabPicture(0)   =   "frmViewEditor.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Frame1"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "1087"
      TabPicture(1)   =   "frmViewEditor.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame2"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "1088"
      TabPicture(2)   =   "frmViewEditor.frx":0038
      Tab(2).ControlEnabled=   -1  'True
      Tab(2).Control(0)=   "Frame3"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "1089"
      TabPicture(3)   =   "frmViewEditor.frx":0054
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Frame4"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "1095"
      TabPicture(4)   =   "frmViewEditor.frx":0070
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "Frame5"
      Tab(4).ControlCount=   1
      Begin VB.Frame Frame1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3855
         Left            =   -74760
         TabIndex        =   33
         Top             =   480
         Width           =   8175
         Begin VB.CommandButton cmdSwapOrg 
            Caption         =   ">>"
            Height          =   375
            Index           =   0
            Left            =   3892
            TabIndex        =   39
            Top             =   840
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapOrg 
            Caption         =   ">"
            Height          =   375
            Index           =   1
            Left            =   3892
            TabIndex        =   38
            Top             =   1320
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapOrg 
            Caption         =   "<"
            Height          =   375
            Index           =   2
            Left            =   3892
            TabIndex        =   37
            Top             =   2160
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapOrg 
            Caption         =   "<<"
            Height          =   375
            Index           =   3
            Left            =   3892
            TabIndex        =   36
            Top             =   2640
            Width           =   375
         End
         Begin VB.ListBox lbOrgUnitFilterSelect 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3000
            Left            =   4440
            MultiSelect     =   2  'Extended
            Sorted          =   -1  'True
            TabIndex        =   35
            Top             =   480
            Width           =   3600
         End
         Begin VB.ListBox lbOrgUnitFilter 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3000
            ItemData        =   "frmViewEditor.frx":008C
            Left            =   120
            List            =   "frmViewEditor.frx":008E
            MultiSelect     =   2  'Extended
            Sorted          =   -1  'True
            TabIndex        =   34
            Top             =   480
            Width           =   3600
         End
      End
      Begin VB.Frame Frame2 
         Height          =   3855
         Left            =   -74760
         TabIndex        =   26
         Top             =   480
         Width           =   8175
         Begin VB.ListBox lbDutyGroupFilter 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3000
            Left            =   120
            MultiSelect     =   2  'Extended
            Sorted          =   -1  'True
            TabIndex        =   32
            Top             =   480
            Width           =   3600
         End
         Begin VB.ListBox lbDutyGroupFilterSelected 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3000
            Left            =   4440
            MultiSelect     =   2  'Extended
            Sorted          =   -1  'True
            TabIndex        =   31
            Top             =   480
            Width           =   3600
         End
         Begin VB.CommandButton cmdSwapDutyGroup 
            Caption         =   "<<"
            Height          =   375
            Index           =   3
            Left            =   3892
            TabIndex        =   30
            Top             =   2640
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapDutyGroup 
            Caption         =   "<"
            Height          =   375
            Index           =   2
            Left            =   3892
            TabIndex        =   29
            Top             =   2160
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapDutyGroup 
            Caption         =   ">"
            Height          =   375
            Index           =   1
            Left            =   3892
            TabIndex        =   28
            Top             =   1320
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapDutyGroup 
            Caption         =   ">>"
            Height          =   375
            Index           =   0
            Left            =   3892
            TabIndex        =   27
            Top             =   840
            Width           =   375
         End
      End
      Begin VB.Frame Frame3 
         Height          =   3855
         Left            =   240
         TabIndex        =   19
         Top             =   480
         Width           =   8175
         Begin VB.CommandButton cmdSwapFunction 
            Caption         =   ">>"
            Height          =   375
            Index           =   0
            Left            =   3892
            TabIndex        =   25
            Top             =   840
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapFunction 
            Caption         =   ">"
            Height          =   375
            Index           =   1
            Left            =   3892
            TabIndex        =   24
            Top             =   1320
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapFunction 
            Caption         =   "<"
            Height          =   375
            Index           =   2
            Left            =   3892
            TabIndex        =   23
            Top             =   2160
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapFunction 
            Caption         =   "<<"
            Height          =   375
            Index           =   3
            Left            =   3892
            TabIndex        =   22
            Top             =   2640
            Width           =   375
         End
         Begin VB.ListBox lbFunctionFilterSelected 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3000
            Left            =   4440
            MultiSelect     =   2  'Extended
            Sorted          =   -1  'True
            TabIndex        =   21
            Top             =   480
            Width           =   3600
         End
         Begin VB.ListBox lbFunctionFilter 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3000
            Left            =   120
            MultiSelect     =   2  'Extended
            Sorted          =   -1  'True
            TabIndex        =   20
            Top             =   480
            Width           =   3600
         End
      End
      Begin VB.Frame Frame4 
         Height          =   3855
         Left            =   -74760
         TabIndex        =   12
         Top             =   480
         Width           =   8175
         Begin VB.ListBox lbContractFilter 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3000
            Left            =   120
            MultiSelect     =   2  'Extended
            Sorted          =   -1  'True
            TabIndex        =   18
            Top             =   480
            Width           =   3600
         End
         Begin VB.ListBox lbContractFilterSelected 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3000
            Left            =   4440
            MultiSelect     =   2  'Extended
            Sorted          =   -1  'True
            TabIndex        =   17
            Top             =   480
            Width           =   3600
         End
         Begin VB.CommandButton cmdSwapContract 
            Caption         =   "<<"
            Height          =   375
            Index           =   3
            Left            =   3892
            TabIndex        =   16
            Top             =   2640
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapContract 
            Caption         =   "<"
            Height          =   375
            Index           =   2
            Left            =   3892
            TabIndex        =   15
            Top             =   2160
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapContract 
            Caption         =   ">"
            Height          =   375
            Index           =   1
            Left            =   3892
            TabIndex        =   14
            Top             =   1320
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapContract 
            Caption         =   ">>"
            Height          =   375
            Index           =   0
            Left            =   3892
            TabIndex        =   13
            Top             =   840
            Width           =   375
         End
      End
      Begin VB.Frame Frame5 
         Height          =   3855
         Left            =   -74760
         TabIndex        =   5
         Top             =   480
         Width           =   8175
         Begin VB.ListBox lbAbsenceType 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3000
            Left            =   120
            MultiSelect     =   2  'Extended
            Sorted          =   -1  'True
            TabIndex        =   11
            Top             =   480
            Width           =   3600
         End
         Begin VB.ListBox lbAbsenceTypeSelect 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3000
            Left            =   4440
            MultiSelect     =   2  'Extended
            Sorted          =   -1  'True
            TabIndex        =   10
            Top             =   480
            Width           =   3600
         End
         Begin VB.CommandButton cmdSwapAbsence 
            Caption         =   "<<"
            Height          =   375
            Index           =   3
            Left            =   3892
            TabIndex        =   9
            Top             =   2640
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapAbsence 
            Caption         =   "<"
            Height          =   375
            Index           =   2
            Left            =   3892
            TabIndex        =   8
            Top             =   2160
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapAbsence 
            Caption         =   ">"
            Height          =   375
            Index           =   1
            Left            =   3892
            TabIndex        =   7
            Top             =   1320
            Width           =   375
         End
         Begin VB.CommandButton cmdSwapAbsence 
            Caption         =   ">>"
            Height          =   375
            Index           =   0
            Left            =   3892
            TabIndex        =   6
            Top             =   840
            Width           =   375
         End
      End
   End
   Begin MSComCtl2.UpDown UpDown1 
      Height          =   255
      Left            =   3360
      TabIndex        =   41
      Top             =   5430
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   450
      _Version        =   393216
      Value           =   2000
      BuddyControl    =   "cmdApply"
      BuddyDispid     =   196611
      OrigLeft        =   6855
      OrigTop         =   270
      OrigRight       =   7095
      OrigBottom      =   585
      Max             =   2050
      Min             =   2000
      SyncBuddy       =   -1  'True
      BuddyProperty   =   65547
      Enabled         =   -1  'True
   End
   Begin MSComCtl2.UpDown UpDown2 
      Height          =   255
      Left            =   4440
      TabIndex        =   43
      Top             =   5430
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   450
      _Version        =   393216
      Value           =   3
      BuddyControl    =   "UpDown1"
      BuddyDispid     =   196637
      OrigLeft        =   6855
      OrigTop         =   270
      OrigRight       =   7095
      OrigBottom      =   585
      Max             =   99
      SyncBuddy       =   -1  'True
      BuddyProperty   =   65547
      Enabled         =   -1  'True
   End
   Begin VB.Label Label2 
      Caption         =   "1080"
      Height          =   255
      Left            =   120
      TabIndex        =   47
      Tag             =   "1080"
      Top             =   5040
      Width           =   2295
   End
   Begin VB.Label Label3 
      Caption         =   "1081"
      Height          =   255
      Left            =   2760
      TabIndex        =   46
      Tag             =   "1081"
      Top             =   5040
      Width           =   855
   End
   Begin VB.Line Line1 
      BorderWidth     =   3
      X1              =   120
      X2              =   8880
      Y1              =   6120
      Y2              =   6120
   End
   Begin VB.Label Label1 
      Caption         =   "1094"
      Height          =   255
      Left            =   3840
      TabIndex        =   45
      Tag             =   "1094"
      Top             =   5040
      Width           =   1215
   End
End
Attribute VB_Name = "frmViewEditor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim bmChanges As Boolean                ' did the user made any changes?
Dim imColIdx As Integer                 ' index of the view displayed in the options-window
Dim strSpace As String                  ' string displayed as separation between DPT1 and DPTN
Dim colDutyGroup As New Collection      ' this is for looking up the duty group;
                                        ' the DB-design is awfully bad, so we have to go
                                        ' from the STFTAB over the SORTAB over the ORGTAB to the ODGTAB
                                        ' to find the duty group

Private Sub Form_Load()

   Dim I   ' Declare variable.
    Dim olDocument As frmDocument
    Set olDocument = Module1.fMainForm.ActiveForm

    ' init the module variables
    bmChanges = False
    imColIdx = 0
    strSpace = "..."

 '   LoadResStrings Me       'load the strings
'
'    ' look after the entries in the combobox
'    cmbViewName.Clear
'    For I = 1 To gcViews.Count Step 1
'        cmbViewName.AddItem gcViews(I).ViewName
'        If gcViews(I).ViewName = olDocument.omViewName Then 'cmbViewName.Text Then
'            imColIdx = I
'        End If
'    Next I
'
'    ' init the combobox with the names of the views
'    If olDocument.omViewName <> "" Then
'        cmbViewName.Text = olDocument.omViewName
'        txtYear.Text = gcViews(imColIdx).ViewYear
'        txtMaxAbsWeek.Text = gcViews(imColIdx).ViewMaxAbsentPerWeek
'        If IsNumeric(txtYear.Text) = True Then
'            UpDown1.Value = txtYear.Text
'        End If
'        If IsNumeric(txtMaxAbsWeek.Text) = True Then
'            UpDown2.Value = txtMaxAbsWeek.Text
'        End If
'    Else
'        'cmbViewName.Text = "<Default>"
'        txtYear.Text = Year(Now)
'    End If
'
'    ' init the registers (the riders) of the SSTab
'    tbsOptions.TabCaption(0) = LoadResString(1086)
'    tbsOptions.TabCaption(1) = LoadResString(1087)
'    tbsOptions.TabCaption(2) = LoadResString(1088)
'    tbsOptions.TabCaption(3) = LoadResString(1089)
'    tbsOptions.TabCaption(4) = LoadResString(1095)
'    tbsOptions.Tab = 0
'
'    ' init the rest...
'    HandleOrgFilter
'    HandleDutyGroupFilter
'    HandleFunctionFilter
'    HandleContractFilter
'    HandleAbsenceFilter
    For I = 0 To Screen.FontCount - 1  ' Fill both boxes with
      lbDutyGroupFilter.AddItem Screen.Fonts(I)   ' names of screen fonts.
   Next I
End Sub
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   03.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub HandleOrgFilter()
    Dim I, j As Integer
    Dim ilIdx As Integer
    Dim blIsSelected As Boolean
    Dim strDPT1 As String
    Dim strDPTN As String
    Dim str As String

    lbOrgUnitFilter.Clear
    lbOrgUnitFilterSelect.Clear

    With frmHiddenServerConnection
        For I = 0 To (.TabORG.GetLineCount - 1) Step 1
            blIsSelected = False
            strDPT1 = Trim(.TabORG.GetColumnValue(I, 0))
            strDPTN = Trim(.TabORG.GetColumnValue(I, 2))

            'look in what listbox the entry should be added
            If imColIdx > 0 And imColIdx < gcViews.Count + 1 Then
                Dim olView As New clsView
                Set olView = gcViews(imColIdx)
                For j = 0 To olView.CountOrgUnit Step 1
                    If olView.ItemOrgUnit(j) = strDPT1 Then
                        blIsSelected = True
                        Exit For
                    End If
                Next j
            End If

            'build the string
            str = strDPT1
            While Len(str) < 8
                str = str + " "
            Wend
            If strDPTN <> "" Then
                str = str + strSpace + strDPTN
            End If

            'add the string
            If blIsSelected = False Then
                lbOrgUnitFilter.AddItem str
            Else
                lbOrgUnitFilterSelect.AddItem str
            End If
        Next I
    End With
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   04.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub HandleDutyGroupFilter()
    Dim I, j As Integer
    Dim ilIdx As Integer
    Dim blIsSelected As Boolean
    Dim strODGC As String
    Dim strODGN As String
    Dim strORGU As String
    Dim str As String
    Dim strKey As String

    ' delete old entries in the collection
    For I = 1 To colDutyGroup.Count
        colDutyGroup.Remove 1
    Next I

    ' fill the collection
    With frmHiddenServerConnection
        For I = 0 To (.TabODG.GetLineCount - 1) Step 1
            strODGC = Trim(.TabODG.GetColumnValue(I, 0))
            strODGN = Trim(.TabODG.GetColumnValue(I, 1))
            strORGU = Trim(.TabODG.GetColumnValue(I, 2))

            'build the string
            str = strODGC
            While Len(str) < 8
                str = str + " "
            Wend
            If strODGN <> "" Then
                str = str + strSpace + strODGN
            End If
            colDutyGroup.Add Item:=str, key:=(strODGC + "@" + strORGU)
        Next I
    End With

    lbDutyGroupFilter.Clear
    lbDutyGroupFilterSelected.Clear

    If imColIdx > 0 And imColIdx < gcViews.Count + 1 Then
        Dim olView As New clsView
        Set olView = gcViews(imColIdx)

        ' add the selected ones
        For I = 1 To olView.CountDutyGroup Step 1
            strORGU = olView.ItemDutyGroup(I)
            If DoesKeyExist(colDutyGroup, strORGU) = True Then
                lbDutyGroupFilterSelected.AddItem colDutyGroup(strORGU)
                colDutyGroup.Remove (strORGU)
            End If
        Next I
    End If

    ' add the non-selected ones
    For I = 1 To colDutyGroup.Count
        lbDutyGroupFilter.AddItem colDutyGroup(I)
    Next I
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   04.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub HandleFunctionFilter()
    Dim I, j As Integer
    Dim ilIdx As Integer
    Dim blIsSelected As Boolean
    Dim strFCTC As String
    Dim strFCTN As String
    Dim str As String

    lbFunctionFilter.Clear
    lbFunctionFilterSelected.Clear

    With frmHiddenServerConnection
        For I = 0 To (.TabPFC.GetLineCount - 1) Step 1
            blIsSelected = False
            strFCTC = Trim(.TabPFC.GetColumnValue(I, 1))
            strFCTN = Trim(.TabPFC.GetColumnValue(I, 2))

            'look in what listbox the entry should be added
            If imColIdx > 0 And imColIdx < gcViews.Count + 1 Then
                Dim olView As New clsView
                Set olView = gcViews(imColIdx)
                For j = 1 To olView.CountFunctionEmployee Step 1
                    If olView.ItemFunctionEmployee(j) = strFCTC Then
                        blIsSelected = True
                        Exit For
                    End If
                Next j
            End If

            'build the string
            str = strFCTC
            While Len(str) < 8
                str = str + " "
            Wend
            If strFCTN <> "" Then
                str = str + strSpace + strFCTN
            End If

            'add the string
            If blIsSelected = False Then
                lbFunctionFilter.AddItem str
            Else
                lbFunctionFilterSelected.AddItem str
            End If
        Next I
    End With
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   04.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub HandleContractFilter()
    Dim I, j As Integer
    Dim ilIdx As Integer
    Dim blIsSelected As Boolean
    Dim strCOTC As String
    Dim strCOTN As String
    Dim str As String

    lbContractFilter.Clear
    lbContractFilterSelected.Clear

    With frmHiddenServerConnection
        For I = 0 To (.TabCOT.GetLineCount - 1) Step 1
            blIsSelected = False
            strCOTC = Trim(.TabCOT.GetColumnValue(I, 0))
            strCOTN = Trim(.TabCOT.GetColumnValue(I, 1))

            'look in what listbox the entry should be added
            If imColIdx > 0 And imColIdx < gcViews.Count + 1 Then
                Dim olView As New clsView
                Set olView = gcViews(imColIdx)
                For j = 1 To olView.CountContractType Step 1
                    If olView.ItemContractType(j) = strCOTC Then
                        blIsSelected = True
                        Exit For
                    End If
                Next j
            End If

            'build the string
            str = strCOTC
            While Len(str) < 8
                str = str + " "
            Wend
            If strCOTN <> "" Then
                str = str + strSpace + strCOTN
            End If

            'add the string
            If blIsSelected = False Then
                lbContractFilter.AddItem str
            Else
                lbContractFilterSelected.AddItem str
            End If
        Next I
    End With
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   14.05.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub HandleAbsenceFilter()
    Dim I, j As Integer
    Dim ilIdx As Integer
    Dim blIsSelected As Boolean
    Dim strSDAC As String
    Dim strSDAN As String
    Dim str As String

    lbAbsenceType.Clear
    lbAbsenceTypeSelect.Clear

    With frmHiddenServerConnection
        For I = 0 To (.TabODA.GetLineCount - 1) Step 1
            blIsSelected = False
            strSDAC = Trim(.TabODA.GetColumnValue(I, 0))
            strSDAN = Trim(.TabODA.GetColumnValue(I, 4))

            'look in what listbox the entry should be added
            If imColIdx > 0 And imColIdx < gcViews.Count + 1 Then
                Dim olView As New clsView
                Set olView = gcViews(imColIdx)
                For j = 0 To olView.CountAbsenceType Step 1
                    If olView.ItemAbsenceType(j) = strSDAC Then
                        blIsSelected = True
                        Exit For
                    End If
                Next j
            End If

            'build the string
            str = strSDAC
            While Len(str) < 8
                str = str + " "
            Wend
            If strSDAN <> "" Then
                str = str + strSpace + strSDAN
            End If

            'add the string
            If blIsSelected = False Then
                lbAbsenceType.AddItem str
            Else
                lbAbsenceTypeSelect.AddItem str
            End If
        Next I
    End With
End Sub
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' -
' - start of bottom buttons (SAVE,DELETE,APPLY,CLOSE)
' -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   03.04.2001
' .
' . description :   saves the new view in the collection, not in the DB
' .
' . changes     :   05.04.2001  correcting of the logical sequence
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

Private Sub cmdSave_Click()
    If Trim(cmbViewName.Text) <> "" Then
        Dim blViewDoesAlreadyExist As Boolean

        Dim I As Integer

        Dim strOrgUnit As String
        Dim strDutyGroup As String
        Dim strFunction As String
        Dim strContract As String
        Dim strView As String
        Dim strAbs As String

        Dim olView As clsView

        'look if there is already a view with the name specified in the cmbViewName
        blViewDoesAlreadyExist = False
        Dim olElement
        For Each olElement In gcViews
            If olElement.ViewName = cmbViewName.Text Then
                blViewDoesAlreadyExist = True
                Exit For
            End If
        Next

        'Add a new view to the collection if there is no with the specified name
        If blViewDoesAlreadyExist = False Then
            MakeView
        End If

        ' ... and take the new settings ...
        ApplyAll

        Set olView = gcViews(imColIdx)

        strView = olView.ViewName + ";0;" + CStr(olView.ViewYear) + "|" + CStr(olView.ViewMaxAbsentPerWeek)

        strOrgUnit = olView.ViewName + ";1;"
        For I = 1 To olView.CountOrgUnit
            strOrgUnit = strOrgUnit + olView.ItemOrgUnit(I) + "|"
        Next I

        strDutyGroup = olView.ViewName + ";2;"
        For I = 1 To olView.CountDutyGroup
            strDutyGroup = strDutyGroup + olView.ItemDutyGroup(I) + "|"
        Next I

        strFunction = olView.ViewName + ";3;"
        For I = 1 To olView.CountFunctionEmployee
            strFunction = strFunction + olView.ItemFunctionEmployee(I) + "|"
        Next I

        strContract = olView.ViewName + ";4;"
        For I = 1 To olView.CountContractType
            strContract = strContract + olView.ItemContractType(I) + "|"
        Next I

        strAbs = olView.ViewName + ";5;"
        For I = 1 To olView.CountAbsenceType
            strAbs = strAbs + olView.ItemAbsenceType(I) + "|"
        Next I

        Module1.fMainForm.sbStatusBar.Panels.Item(1).Text = LoadResString(1092)
        frmHiddenServerConnection.SaveView strView
        Module1.fMainForm.sbStatusBar.Panels.Item(1).Text = LoadResString(1092) + "."
        frmHiddenServerConnection.SaveView strOrgUnit
        Module1.fMainForm.sbStatusBar.Panels.Item(1).Text = LoadResString(1092) + ".."
        frmHiddenServerConnection.SaveView strDutyGroup
        Module1.fMainForm.sbStatusBar.Panels.Item(1).Text = LoadResString(1092) + "..."
        frmHiddenServerConnection.SaveView strFunction
        Module1.fMainForm.sbStatusBar.Panels.Item(1).Text = LoadResString(1092) + "...."
        frmHiddenServerConnection.SaveView strContract
        Module1.fMainForm.sbStatusBar.Panels.Item(1).Text = LoadResString(1092) + "....."
        frmHiddenServerConnection.SaveView strAbs
        Module1.fMainForm.sbStatusBar.Panels.Item(1).Text = LoadResString(1093)

    Else
        MsgBox LoadResString(1084), vbCritical, LoadResString(1085)
    End If
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   03.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub cmdApply_Click()
    If Trim(cmbViewName.Text) <> "" Then
        Dim I As Integer
        Dim blViewDoesAlreadyExist As Boolean

        'look if there is already a view with the name specified in the cmbViewName
        blViewDoesAlreadyExist = False
        For I = 1 To gcViews.Count Step 1
            If gcViews(I).ViewName = cmbViewName.Text Then
                blViewDoesAlreadyExist = True
                Exit For
            End If
        Next I

        'Add a new view to the collection if there is no with the specified name
        If blViewDoesAlreadyExist = False Then
            MakeView
        End If

        ' ... and take the new settings ...
        ApplyAll

    Else
        MsgBox LoadResString(1084), vbCritical, LoadResString(1085)
    End If
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   04.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub ApplyAll()
    bmChanges = False   ' important!
    If imColIdx > 0 And imColIdx < gcViews.Count + 1 Then
        Dim I As Integer
        Dim ilPos As Integer
        Dim olDocument As frmDocument
        Set olDocument = Module1.fMainForm.ActiveForm

        ' look after the year
        gcViews(imColIdx).ViewYear = txtYear.Text
        gcViews(imColIdx).ViewMaxAbsentPerWeek = txtMaxAbsWeek.Text

        ' set the actual view to this one
        olDocument.omViewName = cmbViewName.Text

        ' look after the organisation units
        gcViews(imColIdx).RemoveAllOrgUnit
        For I = 0 To lbOrgUnitFilterSelect.ListCount - 1 Step 1
            ilPos = InStr(lbOrgUnitFilterSelect.List(I), strSpace)
            If ilPos > 0 Then
                gcViews(imColIdx).AddOrgUnit (Left(lbOrgUnitFilterSelect.List(I), (ilPos - 1)))
            Else
                gcViews(imColIdx).AddOrgUnit (lbOrgUnitFilterSelect.List(I))
            End If
        Next I

        ' look after the duty groups
        gcViews(imColIdx).RemoveAllDutyGroup
        Dim strODGC As String
        Dim strODGN As String
        Dim strLineNo As String
        Dim strLine As String
        For I = 0 To (lbDutyGroupFilterSelected.ListCount - 1)
            strODGC = frmCalc.GetItem(lbDutyGroupFilterSelected.List(I), 1, strSpace)
            strODGN = frmCalc.GetItem(lbDutyGroupFilterSelected.List(I), 2, strSpace)
            If strODGN = "" Then strODGN = " "
            strLineNo = frmHiddenServerConnection.TabODG.GetLinesByMultipleColumnValue("0,1", (strODGC + "," + strODGN), 1)
            If frmCalc.ItemCount(strLineNo, ",") = 1 Then
                strLine = frmHiddenServerConnection.TabODG.GetLineValues(frmCalc.GetItem(strLineNo, 1, ","))
                gcViews(imColIdx).AddDutyGroup (strODGC + "@" + frmCalc.GetItem(strLine, 3, ","))
            ElseIf frmCalc.ItemCount(strLineNo, ",") > 1 Then
                strLine = frmHiddenServerConnection.TabODG.GetLineValues(frmCalc.GetItem(strLineNo, 2, ","))
                gcViews(imColIdx).AddDutyGroup (strODGC + "@" + frmCalc.GetItem(strLine, 3, ","))
            End If
        Next I

        ' look after the function of the employees
        gcViews(imColIdx).RemoveAllFunctionEmployee
        For I = 0 To lbFunctionFilterSelected.ListCount - 1 Step 1
            ilPos = InStr(lbFunctionFilterSelected.List(I), strSpace)
            If ilPos > 0 Then
                gcViews(imColIdx).AddFunctionEmployee (Left(lbFunctionFilterSelected.List(I), (ilPos - 1)))
            Else
                gcViews(imColIdx).AddFunctionEmployee (lbFunctionFilterSelected.List(I))
            End If
        Next I

        ' look after the contract types
        gcViews(imColIdx).RemoveAllContractType
        For I = 0 To lbContractFilterSelected.ListCount - 1 Step 1
            ilPos = InStr(lbContractFilterSelected.List(I), strSpace)
            If ilPos > 0 Then
                gcViews(imColIdx).AddContractType (Left(lbContractFilterSelected.List(I), (ilPos - 1)))
            Else
                gcViews(imColIdx).AddContractType (lbContractFilterSelected.List(I))
            End If
        Next I

        ' look after the absence types
        gcViews(imColIdx).RemoveAllAbsenceType
        For I = 0 To lbAbsenceTypeSelect.ListCount - 1 Step 1
            ilPos = InStr(lbAbsenceTypeSelect.List(I), strSpace)
            If ilPos > 0 Then
                gcViews(imColIdx).AddAbsenceType (Left(lbAbsenceTypeSelect.List(I), (ilPos - 1)))
            Else
                gcViews(imColIdx).AddAbsenceType (lbAbsenceTypeSelect.List(I))
            End If
        Next I

    End If
End Sub

Private Sub MakeView()
    Dim olView As New clsView
    Dim I As Integer

    'set the properties of the new view
    olView.ViewName = cmbViewName.Text
    gcViews.Add olView
    imColIdx = gcViews.Count
    Set olView = Nothing

    'add the new item to the combo-box
    cmbViewName.AddItem cmbViewName.Text

    ' look after the collection-index
'    For i = 1 To gcViews.Count Step 1
'        If gcViews(i).ViewName = cmbViewName.Text Then
'            imColIdx = i
'            Exit For
'        End If
'    Next i
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   03.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub cmdDelete_Click()
    If imColIdx > 0 And imColIdx < gcViews.Count + 1 Then
        Dim I As Integer
        frmHiddenServerConnection.DeleteView gcViews(imColIdx).ViewName
        gcViews(imColIdx).RemoveAllOrgUnit
        gcViews(imColIdx).RemoveAllDutyGroup
        gcViews(imColIdx).RemoveAllFunctionEmployee
        gcViews(imColIdx).RemoveAllContractType
        gcViews.Remove (imColIdx)

        If imColIdx > gcViews.Count Then
            imColIdx = gcViews.Count
        End If

        ' reinit the combo-box
        cmbViewName.Clear
        For I = 1 To gcViews.Count Step 1
            cmbViewName.AddItem gcViews(I).ViewName
        Next I
        If imColIdx > 0 Then
            cmbViewName.Text = gcViews(imColIdx).ViewName
        Else
            cmbViewName.Text = ""
        End If

        ' reinit the txtYear
        If Trim(cmbViewName.Text) <> "" Then
            txtYear.Text = gcViews(imColIdx).ViewYear
        End If

        HandleOrgFilter
        HandleDutyGroupFilter
        HandleFunctionFilter
        HandleContractFilter
        HandleAbsenceFilter
    End If
End Sub


' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   03.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub cmdClose_Click()
    If bmChanges = False Then
        Unload Me
    Else
        Dim Response
        Response = MsgBox(LoadResString(1091), vbQuestion + vbYesNo, LoadResString(1090))

        Select Case Response
            Case vbYes
                cmdApply_Click
            Case vbNo
                bmChanges = False
        End Select
        Unload Me
    End If

    'Load the new view
    Dim olDocument As frmDocument
    Set olDocument = Module1.fMainForm.ActiveForm
    olDocument.Form_Reload
    frmCalc.FillMainGrid
End Sub


' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   03.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub cmbViewName_Click()
    If bmChanges = True Then
        Dim Response
        Response = MsgBox(LoadResString(1091), vbQuestion + vbYesNo, LoadResString(1090))

        Select Case Response
            Case vbYes
                cmdApply_Click
            Case vbNo
                bmChanges = False
        End Select
    End If

    Dim I As Integer

    ' look for the correct collection index
    For I = 1 To gcViews.Count Step 1
        If gcViews(I).ViewName = cmbViewName.Text Then
            imColIdx = I
            Exit For
        End If
    Next I

    ' adapt all other controls
    If imColIdx > 0 And imColIdx < gcViews.Count + 1 Then
        txtYear.Text = gcViews(imColIdx).ViewYear
        txtMaxAbsWeek.Text = gcViews(imColIdx).ViewMaxAbsentPerWeek
    End If
    HandleOrgFilter
    HandleDutyGroupFilter
    HandleFunctionFilter
    HandleContractFilter
    HandleAbsenceFilter
End Sub

Private Sub txtMaxAbsWeek_Change()
    'bmChanges = True
End Sub
Private Sub txtYear_Change()
    'bmChanges = True
End Sub
Private Sub UpDown1_Change()
    'bmChanges = True
End Sub
Private Sub UpDown2_Change()
    'bmChanges = True
End Sub


' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' -
' - start of Organisation Unit
' -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   03.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub cmdSwapOrg_Click(Index As Integer)
    Dim I As Integer
    bmChanges = True

    Select Case Index
        Case 0:     'all to right
            While lbOrgUnitFilter.ListCount > 0
                lbOrgUnitFilterSelect.AddItem lbOrgUnitFilter.List(0)
                lbOrgUnitFilter.RemoveItem (0)
            Wend

        Case 1:     'only selected to right
            lbOrgUnitFilter_DblClick

        Case 2:     'only selected to left
            lbOrgUnitFilterSelect_DblClick

        Case 3:     'all to left
            While lbOrgUnitFilterSelect.ListCount > 0
                lbOrgUnitFilter.AddItem lbOrgUnitFilterSelect.List(0)
                lbOrgUnitFilterSelect.RemoveItem (0)
            Wend
    End Select
End Sub


' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   04.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub lbOrgUnitFilter_DblClick()
    Dim I As Integer
    bmChanges = True
    I = 0
    While I < lbOrgUnitFilter.ListCount
        If lbOrgUnitFilter.Selected(I) = True Then
            lbOrgUnitFilterSelect.AddItem lbOrgUnitFilter.List(I)
            lbOrgUnitFilter.RemoveItem (I)
        Else
            I = I + 1
        End If
    Wend
End Sub

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   04.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub lbOrgUnitFilterSelect_DblClick()
    Dim I As Integer
    bmChanges = True
    I = 0
    While I < lbOrgUnitFilterSelect.ListCount
        If lbOrgUnitFilterSelect.Selected(I) = True Then
            lbOrgUnitFilter.AddItem lbOrgUnitFilterSelect.List(I)
            lbOrgUnitFilterSelect.RemoveItem (I)
        Else
            I = I + 1
        End If
    Wend
End Sub

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' -
' - start of Duty Groups
' -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   04.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

Private Sub cmdSwapDutyGroup_Click(Index As Integer)
    Dim I As Integer
    bmChanges = True

    Select Case Index
        Case 0:     'all to right
            While lbDutyGroupFilter.ListCount > 0
                lbDutyGroupFilterSelected.AddItem lbDutyGroupFilter.List(0)
                lbDutyGroupFilter.RemoveItem (0)
            Wend

        Case 1:     'only selected to right
            lbDutyGroupFilter_DblClick

        Case 2:     'only selected to left
            lbDutyGroupFilterSelected_DblClick

        Case 3:     'all to left
            While lbDutyGroupFilterSelected.ListCount > 0
                lbDutyGroupFilter.AddItem lbDutyGroupFilterSelected.List(0)
                lbDutyGroupFilterSelected.RemoveItem (0)
            Wend
    End Select
End Sub

Private Sub lbDutyGroupFilter_DblClick()
    Dim I As Integer
    bmChanges = True
    I = 0
    While I < lbDutyGroupFilter.ListCount
        If lbDutyGroupFilter.Selected(I) = True Then
            lbDutyGroupFilterSelected.AddItem lbDutyGroupFilter.List(I)
            lbDutyGroupFilter.RemoveItem (I)
        Else
            I = I + 1
        End If
    Wend
End Sub

Private Sub lbDutyGroupFilterSelected_DblClick()
    Dim I As Integer
    bmChanges = True
    I = 0
    While I < lbDutyGroupFilterSelected.ListCount
        If lbDutyGroupFilterSelected.Selected(I) = True Then
            lbDutyGroupFilter.AddItem lbDutyGroupFilterSelected.List(I)
            lbDutyGroupFilterSelected.RemoveItem (I)
        Else
            I = I + 1
        End If
    Wend
End Sub

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' -
' - start of Function Employees
' -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   04.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub cmdSwapFunction_Click(Index As Integer)
    Dim I As Integer
    bmChanges = True

    Select Case Index
        Case 0:     'all to right
            While lbFunctionFilter.ListCount > 0
                lbFunctionFilterSelected.AddItem lbFunctionFilter.List(0)
                lbFunctionFilter.RemoveItem (0)
            Wend

        Case 1:     'only selected to right
            lbFunctionFilter_DblClick

        Case 2:     'only selected to left
            lbFunctionFilterSelected_DblClick

        Case 3:     'all to left
            While lbFunctionFilterSelected.ListCount > 0
                lbFunctionFilter.AddItem lbFunctionFilterSelected.List(0)
                lbFunctionFilterSelected.RemoveItem (0)
            Wend
    End Select
End Sub

Private Sub lbFunctionFilter_DblClick()
    Dim I As Integer
    bmChanges = True
    I = 0
    While I < lbFunctionFilter.ListCount
        If lbFunctionFilter.Selected(I) = True Then
            lbFunctionFilterSelected.AddItem lbFunctionFilter.List(I)
            lbFunctionFilter.RemoveItem (I)
        Else
            I = I + 1
        End If
    Wend
End Sub

Private Sub lbFunctionFilterSelected_DblClick()
    Dim I As Integer
    bmChanges = True
    I = 0
    While I < lbFunctionFilterSelected.ListCount
        If lbFunctionFilterSelected.Selected(I) = True Then
            lbFunctionFilter.AddItem lbFunctionFilterSelected.List(I)
            lbFunctionFilterSelected.RemoveItem (I)
        Else
            I = I + 1
        End If
    Wend
End Sub

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' -
' - start of Contract type
' -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   04.04.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub cmdSwapContract_Click(Index As Integer)
    Dim I As Integer
    bmChanges = True

    Select Case Index
        Case 0:     'all to right
            While lbContractFilter.ListCount > 0
                lbContractFilterSelected.AddItem lbContractFilter.List(0)
                lbContractFilter.RemoveItem (0)
            Wend

        Case 1:     'only selected to right
            lbContractFilter_DblClick

        Case 2:     'only selected to left
            lbContractFilterSelected_DblClick

        Case 3:     'all to left
            While lbContractFilterSelected.ListCount > 0
                lbContractFilter.AddItem lbContractFilterSelected.List(0)
                lbContractFilterSelected.RemoveItem (0)
            Wend
    End Select
End Sub

Private Sub lbContractFilter_DblClick()
    Dim I As Integer
    bmChanges = True
    I = 0
    While I < lbContractFilter.ListCount
        If lbContractFilter.Selected(I) = True Then
            lbContractFilterSelected.AddItem lbContractFilter.List(I)
            lbContractFilter.RemoveItem (I)
        Else
            I = I + 1
        End If
    Wend
End Sub

Private Sub lbContractFilterSelected_DblClick()
    Dim I As Integer
    bmChanges = True
    I = 0
    While I < lbContractFilterSelected.ListCount
        If lbContractFilterSelected.Selected(I) = True Then
            lbContractFilter.AddItem lbContractFilterSelected.List(I)
            lbContractFilterSelected.RemoveItem (I)
        Else
            I = I + 1
        End If
    Wend
End Sub

' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' -
' - start of absence type
' -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
' . created from:   RRO
' . date        :   14.05.2001
' . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
Private Sub cmdSwapAbsence_Click(Index As Integer)
    Dim I As Integer
    bmChanges = True

    Select Case Index
        Case 0:     'all to right
            While lbAbsenceType.ListCount > 0
                lbAbsenceTypeSelect.AddItem lbAbsenceType.List(0)
                lbAbsenceType.RemoveItem (0)
            Wend

        Case 1:     'only selected to right
            lbAbsenceType_DblClick

        Case 2:     'only selected to left
            lbAbsenceTypeSelect_DblClick

        Case 3:     'all to left
            While lbAbsenceTypeSelect.ListCount > 0
                lbAbsenceType.AddItem lbAbsenceTypeSelect.List(0)
                lbAbsenceTypeSelect.RemoveItem (0)
            Wend
    End Select
End Sub

Private Sub lbAbsenceType_DblClick()
    Dim I As Integer
    bmChanges = True
    I = 0
    While I < lbAbsenceType.ListCount
        If lbAbsenceType.Selected(I) = True Then
            lbAbsenceTypeSelect.AddItem lbAbsenceType.List(I)
            lbAbsenceType.RemoveItem (I)
        Else
            I = I + 1
        End If
    Wend
End Sub

Private Sub lbAbsenceTypeSelect_DblClick()
    Dim I As Integer
    bmChanges = True
    I = 0
    While I < lbAbsenceTypeSelect.ListCount
        If lbAbsenceTypeSelect.Selected(I) = True Then
            lbAbsenceType.AddItem lbAbsenceTypeSelect.List(I)
            lbAbsenceTypeSelect.RemoveItem (I)
        Else
            I = I + 1
        End If
    Wend
End Sub

Private Function DoesKeyExist(ByRef refCollection As Collection, refKey As String) As Boolean
    On Error GoTo DoesNotExist
    refCollection.Item (refKey)
    DoesKeyExist = True
    Exit Function
DoesNotExist:
    DoesKeyExist = False
End Function
