#if !defined(AFX_COLORSETUPPAGE_H__27A161D4_9D2F_11D6_820D_00010215BFDE__INCLUDED_)
#define AFX_COLORSETUPPAGE_H__27A161D4_9D2F_11D6_820D_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ColorSetupPage.h : header file
//
#include <GridFenster.h>
#include <CCSPtrArray.h>
#include <BasicData.h>

/////////////////////////////////////////////////////////////////////////////
// ColorSetupPage dialog


class ColorSetupPage : public CPropertyPage
{
	DECLARE_DYNCREATE(ColorSetupPage)

// Construction
public:
	ColorSetupPage();
	~ColorSetupPage();

	void SetCaption(const char *pcpCaption);
	char pcmCaption[100];

// Dialog Data
	//{{AFX_DATA(ColorSetupPage)
	enum { IDD = IDD_COLOR_SETUP_PAGE };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(ColorSetupPage)
	public:
	virtual void OnOK();
	virtual void OnCancel();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(ColorSetupPage)
		// NOTE: the ClassWizard will add member functions here
	virtual BOOL OnInitDialog();
	afx_msg void OnLoadFromDefault();
	afx_msg void OnSaveAsDefault();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	BOOL  OnLButtonClickedRowCol(UINT wParam, LONG lParam) ;
	BOOL  OnLButtonDblClickedRowCol(UINT wParam, LONG lParam) ;
private:
	bool						bmInit;
	CGridFenster*				pomColorList;
	CCSPtrArray<COLORCONFDATA>	omColorConfData;
	int							omLinesChanged[NUMCONFCOLORS];	
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COLORSETUPPAGE_H__27A161D4_9D2F_11D6_820D_00010215BFDE__INCLUDED_)
