// SdgGraphic.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <CCSEdit.h>
#include <SdgGraphic.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SdgGraphic

SdgGraphic::SdgGraphic()
{
	imWeeks = 10;
}

SdgGraphic::~SdgGraphic()
{
}


BEGIN_MESSAGE_MAP(SdgGraphic, CWnd)
	//{{AFX_MSG_MAP(SdgGraphic)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten SdgGraphic 

void SdgGraphic::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	int i = 0, 
		j = 0, 
		k = 0;

	CRect olClientRect;
	GetClientRect(&olClientRect);
	CPen olBlackPen, 
		 olBluePen,
		 olWhitePen,
		 olBlueDotPen,
		 *polOldPen;
	int ilWidth = olClientRect.right - olClientRect.left;
	int ilHeight = olClientRect.bottom - olClientRect.top;
	int ilHeader = (int)((olClientRect.bottom - olClientRect.top)/3);
	olBlackPen.CreatePen(PS_SOLID, 1, COLORREF(BLACK));
	olBluePen.CreatePen(PS_SOLID, 1, COLORREF(BLUE));
	olBlueDotPen.CreatePen(PS_DOT, 1, COLORREF(BLUE));
	olWhitePen.CreatePen(PS_DOT, 1, COLORREF(WHITE));
	polOldPen = dc.SelectObject(&olBlackPen);
	dc.MoveTo(olClientRect.left, olClientRect.top);
	dc.LineTo(olClientRect.left, olClientRect.bottom);
	dc.LineTo(olClientRect.right, olClientRect.bottom);
	dc.LineTo(olClientRect.right, olClientRect.top);
	dc.LineTo(olClientRect.left, olClientRect.top);
	dc.SelectObject(&olWhitePen);
	dc.MoveTo(olClientRect.left+1, ilHeight);
	dc.LineTo(olClientRect.right-11, ilHeight);

//10 Percent from left as startpoint, same from right as stoppoint
	int ilStartX = olClientRect.left + (int)(ilWidth/10);
	int ilEndX = olClientRect.right - (int)(ilWidth/10);
	int ilX = ilStartX;
	int ilWeekStep = (int)((ilEndX - ilStartX)/imWeeks);
	dc.SelectObject(&olBluePen);
	for(i = 0; i < imWeeks; i++)
	{
		dc.MoveTo(ilX, olClientRect.top);
		dc.LineTo(ilX, ilHeight);
		ilX += ilWeekStep;
	}

	ilX = ilStartX;
	dc.SelectObject(&olBlueDotPen);
	for(i = 0; i < imWeeks; i++)
	{
		dc.MoveTo(ilX, ilHeight);
		dc.LineTo(ilX, olClientRect.bottom);
		ilX += ilWeekStep;
	}


//As Default lets take 10 weeks

	


	// TODO: Code f�r die Behandlungsroutine f�r Nachrichten hier einf�gen
	
	// Kein Aufruf von CWnd::OnPaint() f�r Zeichnungsnachrichten
}
