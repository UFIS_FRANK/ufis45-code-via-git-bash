// SmoothSimulationPage.cpp : implementation file
//

#include <stdafx.h>
#include <coverage.h>
#include <SmoothSimulationPage.h>
#include <SimulationSheet.h>
#include <CoverageDiaViewer.h>
#include <CovDiagram.h>
#include <CCSParam.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SmoothSimulationPage property page

IMPLEMENT_DYNCREATE(SmoothSimulationPage, CPropertyPage)

SmoothSimulationPage::SmoothSimulationPage() : CPropertyPage(SmoothSimulationPage::IDD)
{
	//{{AFX_DATA_INIT(SmoothSimulationPage)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	bmSmooth = true;
}

SmoothSimulationPage::~SmoothSimulationPage()
{
}

void SmoothSimulationPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SmoothSimulationPage)
	DDX_Control(pDX, IDC_SPIN_MAX_WIDTH, m_SpinMaxWidth);
	DDX_Control(pDX, IDC_EDIT_MAX_WIDTH, m_MaxWidth);
	DDX_Control(pDX, IDC_EDIT_ITERATIONS, m_Iterations);
	DDX_Control(pDX, IDC_SPIN_ITERATIONS, m_SpinIterations);
	DDX_Control(pDX, IDC_EDIT_WIDTH, m_PeakWidth);
	DDX_Control(pDX, IDC_SPIN_WIDTH, m_SpinWidth);
	DDX_Control(pDX, IDC_EDIT_HEIGHT, m_PeakHeight);
	DDX_Control(pDX, IDC_SPIN_HEIGHT, m_SpinHeight);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(SmoothSimulationPage, CPropertyPage)
	//{{AFX_MSG_MAP(SmoothSimulationPage)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_ITERATIONS, OnDeltaposSpinIterations)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_MAX_WIDTH, OnDeltaposSpinMaxWidth)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_WIDTH, OnDeltaposSpinWidth)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_HEIGHT, OnDeltaposSpinHeight)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SmoothSimulationPage message handlers

BOOL SmoothSimulationPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	// TODO: Add extra initialization here
	CWnd *polWnd = GetDlgItem(IDC_PEAK_WIDTH);
	if (polWnd)
		polWnd->SetWindowText(LoadStg(IDS_STRING1818));

	polWnd = GetDlgItem(IDC_UNIT_WIDTH);
	if (polWnd)
		polWnd->SetWindowText(LoadStg(IDS_STRING1819));

	if (bmSmooth)
	{
		polWnd = GetDlgItem(IDC_PEAK_HEIGHT);
		if (polWnd)
			polWnd->SetWindowText(LoadStg(IDS_STRING1820));

		polWnd = GetDlgItem(IDC_MAX_WIDTH);
		if (polWnd)
			polWnd->SetWindowText(LoadStg(IDS_STRING1826));

		polWnd = GetDlgItem(IDC_UNIT_MAX_WIDTH);
		if (polWnd)
			polWnd->SetWindowText(LoadStg(IDS_STRING1827));
	}
	else
	{
		polWnd = GetDlgItem(IDC_PEAK_HEIGHT);
		if (polWnd)
			polWnd->SetWindowText(LoadStg(IDS_STRING1822));

	}

	polWnd = GetDlgItem(IDC_UNIT_HEIGHT_ABSOLUT);
	if (polWnd)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1821));
		if (!bmSmooth)
			polWnd->EnableWindow(FALSE);
	}

	polWnd = GetDlgItem(IDC_UNIT_HEIGHT_PERCENT);
	if (polWnd)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1823));
		if (!bmSmooth)
			polWnd->EnableWindow(FALSE);
	}

	polWnd = GetDlgItem(IDC_ITERATIONS);
	if (polWnd)
		polWnd->SetWindowText(LoadStg(IDS_STRING1824));



	CoverageDiagramViewer *polViewer = ((SimulationSheet *)this->GetParent())->GetViewer();
	ASSERT(polViewer);

	m_SpinIterations.SetRange(0,100);
	m_SpinIterations.SetPos(polViewer->SimIterations());
	m_SpinIterations.SetBuddy(GetDlgItem(IDC_EDIT_ITERATIONS));
	CString olText;
	olText.Format("%d",polViewer->SimIterations());
	m_Iterations.SetWindowText(olText);
	
	m_SpinWidth.SetRange(0,100);
	m_SpinWidth.SetPos(polViewer->SimPeakWidth());
	m_SpinWidth.SetBuddy(GetDlgItem(IDC_EDIT_WIDTH));
	olText.Format("%d",polViewer->SimPeakWidth());
	m_PeakWidth.SetWindowText(olText);

	m_SpinMaxWidth.SetRange(0,100);
	m_SpinMaxWidth.SetPos(polViewer->SimWidth());
	m_SpinMaxWidth.SetBuddy(GetDlgItem(IDC_EDIT_MAX_WIDTH));
	olText.Format("%d",polViewer->SimWidth());
	m_MaxWidth.SetWindowText(olText);

	if (bmSmooth)
	{
		if (polViewer->SimHeightType() == SIM_ABSOLUTE)
			m_SpinHeight.SetRange(0,pogCoverageDiagram->CalculateMaxFlightDemandPeak());
		else
			m_SpinHeight.SetRange(0,100);
		m_SpinHeight.SetPos(polViewer->SimPeakHeight());
		m_SpinHeight.SetBuddy(GetDlgItem(IDC_EDIT_HEIGHT));
		olText.Format("%d",polViewer->SimPeakHeight());
		m_PeakHeight.SetWindowText(olText);
		if (polViewer->SimHeightType() == SIM_ABSOLUTE)
		{
			CButton *polWnd = (CButton *)GetDlgItem(IDC_UNIT_HEIGHT_ABSOLUT);
			if (polWnd)
				polWnd->SetCheck(1);
		}
		else
		{
			CButton *polWnd = (CButton *)GetDlgItem(IDC_UNIT_HEIGHT_PERCENT);
			if (polWnd)
				polWnd->SetCheck(1);
		}
	}
	else
	{
		m_SpinHeight.SetRange(pogCoverageDiagram->CalculateMinFlightDemandCut(),pogCoverageDiagram->CalculateMaxFlightDemandPeak());
		m_SpinHeight.SetPos(polViewer->SimHeight());
		m_SpinHeight.SetBuddy(GetDlgItem(IDC_EDIT_HEIGHT));
		olText.Format("%d",polViewer->SimHeight());
		m_PeakHeight.SetWindowText(olText);

		CButton *polWnd = (CButton *)GetDlgItem(IDC_UNIT_HEIGHT_ABSOLUT);
		if (polWnd)
			polWnd->SetCheck(1);
	}

	if (!bmSmooth)
	{
		this->m_Iterations.ShowWindow(SW_HIDE);
		this->m_SpinIterations.ShowWindow(SW_HIDE);
		this->m_PeakWidth.ShowWindow(SW_HIDE);
		this->m_SpinWidth.ShowWindow(SW_HIDE);
		this->m_MaxWidth.ShowWindow(SW_HIDE);
		this->m_SpinMaxWidth.ShowWindow(SW_HIDE);

		CWnd *polWnd = GetDlgItem(IDC_PEAK_WIDTH);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);

		polWnd = GetDlgItem(IDC_UNIT_WIDTH);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);

		polWnd = GetDlgItem(IDC_MAX_WIDTH);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);

		polWnd = GetDlgItem(IDC_UNIT_MAX_WIDTH);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);

		polWnd = GetDlgItem(IDC_ITERATIONS);
		if (polWnd)
			polWnd->ShowWindow(SW_HIDE);

		CString olCustomer = ogCCSParam.GetParamValue("ROSTER","ID_PROD_CUSTOMER",CTime::GetCurrentTime().Format("%Y%m%d%H%M%S"),true);

		if ("ADR" == olCustomer)
		{
#if	0
			polWnd = GetDlgItem(IDC_UNIT_HEIGHT_ABSOLUT);
			if (polWnd)
			{
				polWnd->ShowWindow(SW_HIDE);
			}
#endif
			polWnd = GetDlgItem(IDC_UNIT_HEIGHT_PERCENT);
			if (polWnd)
			{
				polWnd->ShowWindow(SW_HIDE);
			}
		}

	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void SmoothSimulationPage::OnDeltaposSpinIterations(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	int ilMinRange,ilMaxRange;
	// TODO: Add your control notification handler code here
	m_SpinIterations.GetRange(ilMinRange,ilMaxRange);
	
	int ilPos = pNMUpDown->iPos;
	int ilDelta = pNMUpDown->iDelta;
 	CString olText;
	
	olText.Format("%d",ilPos+ilDelta);
	
	if((ilPos+ilDelta)>=ilMinRange && (ilPos+ilDelta)<=ilMaxRange)
	{
		(m_SpinIterations.GetBuddy())->SetWindowText(olText);
	}
	*pResult = 0;
}

void SmoothSimulationPage::OnDeltaposSpinMaxWidth(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	int ilMinRange,ilMaxRange;
	// TODO: Add your control notification handler code here
	m_SpinMaxWidth.GetRange(ilMinRange,ilMaxRange);
	
	int ilPos = pNMUpDown->iPos;
	int ilDelta = pNMUpDown->iDelta;
 	CString olText;
	
	olText.Format("%d",ilPos+ilDelta);
	
	if((ilPos+ilDelta)>=ilMinRange && (ilPos+ilDelta)<=ilMaxRange)
	{
		(m_SpinMaxWidth.GetBuddy())->SetWindowText(olText);
	}
	*pResult = 0;
}

void SmoothSimulationPage::OnDeltaposSpinWidth(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	int ilMinRange,ilMaxRange;
	// TODO: Add your control notification handler code here
	m_SpinWidth.GetRange(ilMinRange,ilMaxRange);
	
	int ilPos = pNMUpDown->iPos;
	int ilDelta = pNMUpDown->iDelta;
 	CString olText;
	
	olText.Format("%d",ilPos+ilDelta);
	
	if((ilPos+ilDelta)>=ilMinRange && (ilPos+ilDelta)<=ilMaxRange)
	{
		(m_SpinWidth.GetBuddy())->SetWindowText(olText);
	}
	*pResult = 0;
}

void SmoothSimulationPage::OnDeltaposSpinHeight(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	int ilMinRange,ilMaxRange;
	// TODO: Add your control notification handler code here
	m_SpinHeight.GetRange(ilMinRange,ilMaxRange);
	
	int ilPos = pNMUpDown->iPos;
	int ilDelta = pNMUpDown->iDelta;
 	CString olText;
	
	olText.Format("%d",ilPos+ilDelta);
	
	if((ilPos+ilDelta)>=ilMinRange && (ilPos+ilDelta)<=ilMaxRange)
	{
		(m_SpinHeight.GetBuddy())->SetWindowText(olText);
	}
	*pResult = 0;
}


void SmoothSimulationPage::OnOK() 
{
	// TODO: Add your specialized code here and/or call the base class
	CoverageDiagramViewer *polViewer = ((SimulationSheet *)this->GetParent())->GetViewer();
	ASSERT(polViewer);

	CString olText;
	if (bmSmooth)
	{
		int ilMinRange,ilMaxRange,ilValue;

		m_SpinIterations.GetRange(ilMinRange,ilMaxRange);
		m_Iterations.GetWindowText(olText);
		ilValue = atoi(olText);

		if (ilValue >= ilMinRange && ilValue <= ilMaxRange)
		{
			polViewer->SimIterations(ilValue);
		}

		m_SpinWidth.GetRange(ilMinRange,ilMaxRange);
		m_PeakWidth.GetWindowText(olText);
		ilValue = atoi(olText);

		if (ilValue >= ilMinRange && ilValue <= ilMaxRange)
		{
			polViewer->SimPeakWidth(ilValue);
		}

		m_SpinMaxWidth.GetRange(ilMinRange,ilMaxRange);
		m_MaxWidth.GetWindowText(olText);
		ilValue = atoi(olText);

		if (ilValue >= ilMinRange && ilValue <= ilMaxRange)
		{
			polViewer->SimWidth(ilValue);
		}

		m_SpinHeight.GetRange(ilMinRange,ilMaxRange);
		m_PeakHeight.GetWindowText(olText);
		ilValue = atoi(olText);

		if (ilValue >= ilMinRange && ilValue <= ilMaxRange)
		{
			polViewer->SimPeakHeight(ilValue);
		}

		CButton *polWnd = (CButton *)GetDlgItem(IDC_UNIT_HEIGHT_ABSOLUT);
		if (polWnd)
		{
			if (polWnd->GetCheck())
				polViewer->SimHeightType(SIM_ABSOLUTE);
			else
				polViewer->SimHeightType(SIM_PERCENT);
		}
	}
	else
	{
		int ilMinRange,ilMaxRange,ilValue;
		m_SpinHeight.GetRange(ilMinRange,ilMaxRange);
		m_PeakHeight.GetWindowText(olText);
		ilValue = atoi(olText);

		if (ilValue >= ilMinRange && ilValue <= ilMaxRange)
		{
			polViewer->SimHeight(ilValue);
		}
		else if (ilValue < ilMinRange)
		{
			CString olMsg;
			olMsg.Format(LoadStg(IDS_STRING1952),ilMinRange,ilMinRange);
			if (MessageBox(olMsg,LoadStg(IDS_WARNING),MB_ICONWARNING|MB_YESNO) == IDYES)
			{
				polViewer->SimHeight(ilValue);
			}
			else
			{
				polViewer->SimHeight(ilMinRange);
			}
		}
		else
		{
			CString olMsg;
			olMsg.Format(LoadStg(IDS_STRING1954),ilMaxRange);
			if (MessageBox(olMsg,LoadStg(IDS_WARNING),MB_ICONWARNING|MB_YESNO) == IDYES)
			{
				polViewer->SimHeight(ilValue);
			}
			else
			{
				polViewer->SimHeight(ilMaxRange);
			}
		}
	}


	CPropertyPage::OnOK();
}

void SmoothSimulationPage::SetCaption(const char *pcpCaption)
{
	strcpy(pcmCaption,pcpCaption);
	m_psp.pszTitle = pcmCaption;
	m_psp.dwFlags |= PSP_USETITLE;
}
