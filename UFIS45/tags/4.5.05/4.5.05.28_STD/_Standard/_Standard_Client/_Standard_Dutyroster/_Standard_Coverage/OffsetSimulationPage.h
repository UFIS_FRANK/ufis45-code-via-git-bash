#if !defined(AFX_OFFSETSIMULATIONPAGE_H__DC322376_8D89_11D6_820A_00010215BFDE__INCLUDED_)
#define AFX_OFFSETSIMULATIONPAGE_H__DC322376_8D89_11D6_820A_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// OffsetSimulationPage.h : header file
//
#include <CCSEdit.h>

/////////////////////////////////////////////////////////////////////////////
// OffsetSimulationPage dialog

class OffsetSimulationPage : public CPropertyPage
{
	DECLARE_DYNCREATE(OffsetSimulationPage)

// Construction
public:
	OffsetSimulationPage();
	~OffsetSimulationPage();

	void SetCaption(const char *pcpCaption);

// Dialog Data
	//{{AFX_DATA(OffsetSimulationPage)
	enum { IDD = IDD_OFFSET_SIMULATION_PAGE };
	CSpinButtonCtrl	m_SpinOffset;
	CEdit	m_Demand;
	CSpinButtonCtrl	m_SpinDemand;
	CButton m_Abs;
	CButton m_Percent;
	CButton m_Constant;
	CButton m_Mins;
	CButton m_Hours;
	CEdit	m_Offset;
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(OffsetSimulationPage)
	public:
	virtual void OnOK();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(OffsetSimulationPage)
		// NOTE: the ClassWizard will add member functions here
	virtual BOOL OnInitDialog();
	afx_msg void OnDeltaposSpinDemand(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKillfocusDemand();
	afx_msg void OnAbs();
	afx_msg void OnPercent();
	afx_msg void OnConstant();
	afx_msg void OnDeltaposSpinOffset(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnHours();
	afx_msg void OnMins();
	afx_msg void OnKillfocusOffset();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	char pcmCaption[256];

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OFFSETSIMULATIONPAGE_H__DC322376_8D89_11D6_820A_00010215BFDE__INCLUDED_)
