#if !defined(AFX_SIMULATIONSHEET_H__DC322377_8D89_11D6_820A_00010215BFDE__INCLUDED_)
#define AFX_SIMULATIONSHEET_H__DC322377_8D89_11D6_820A_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SimulationSheet.h : header file
//
#include <SimulationPage.h>
#include <OffsetSimulationPage.h>
#include <SmoothSimulationPage.h>

/////////////////////////////////////////////////////////////////////////////
// SimulationSheet

class CoverageDiagramViewer;

class SimulationSheet : public CPropertySheet
{
	DECLARE_DYNAMIC(SimulationSheet)

// Construction
public:
	SimulationSheet(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	SimulationSheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	SimulationSheet(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0,CoverageDiagramViewer *popViewer = NULL);
	SimulationSheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0,CoverageDiagramViewer *popViewer = NULL);

// Attributes
public:
	CoverageDiagramViewer*	GetViewer()	{ return pomViewer;}
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(SimulationSheet)
	virtual BOOL OnInitDialog();
	//}}AFX_VIRTUAL
		
// Implementation
public:
	virtual ~SimulationSheet();

	// Generated message map functions
protected:
	//{{AFX_MSG(SimulationSheet)
		// NOTE - the ClassWizard will add and remove member functions here.
	afx_msg void OnOK();
	afx_msg void OnCancel();
	afx_msg LONG OnUpdateAllPages(UINT wParam, LONG lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CoverageDiagramViewer*	pomViewer;
	SimulationPage			omSimulationTypePage;
	OffsetSimulationPage	omSimulationOffsetPage;
	SmoothSimulationPage	omSimulationCutPage;
	SmoothSimulationPage	omSimulationSmoothPage;
	CPropertyPage			*pomCurrentPage;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SIMULATIONSHEET_H__DC322377_8D89_11D6_820A_00010215BFDE__INCLUDED_)
