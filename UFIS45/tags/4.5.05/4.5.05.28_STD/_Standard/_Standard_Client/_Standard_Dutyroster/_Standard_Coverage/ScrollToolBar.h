// ScrollToolBar.h: interface for the CScrollToolBar class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SCROLLTOOLBAR_H__C4859BA5_7A4C_11D3_937C_00001C033B5D__INCLUDED_)
#define AFX_SCROLLTOOLBAR_H__C4859BA5_7A4C_11D3_937C_00001C033B5D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define LEFT                            16384
#define RIGHT                           32768
class CScrollRebar;

class CLRToolBar: public CToolBar
{
public:
	CLRToolBar();
	void OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler);	
	int GetWidth();
protected:

	afx_msg void OnLButtonDown( UINT nFlags, CPoint point );
//	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
	afx_msg void OnWindowPosChanging( WINDOWPOS* lpwndpos );
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	DECLARE_MESSAGE_MAP()
};

class CScrollToolBar : public CToolBar  
{
public:
	CScrollToolBar();
	virtual ~CScrollToolBar();

	BOOL AddToRebar ( CScrollRebar *popRebar, UINT idToolBar );
	void DeleteStringMap ();
	void CalculateSizes ();
	CSize CalcDynamicLayout( int nLength, DWORD dwMode );
	CSize CalcFixedLayout(BOOL bStretch, BOOL bHorz);
	void SetSizes(SIZE sizeButton, SIZE sizeImage);
protected:
	CLRToolBar omLeftBar, omRightBar;
	CSize	*pomSizes;
	CSize	omTotalSize;
	int		imButtonAnz, imFirstDisplayed, imLastDisplayed ;
	CScrollRebar	*pomParentRebar;
	bool bmPartiell;
	UINT	imID ;
	bool bmActOnWinPosChanging ;
	int	 imScrollBtnWidth;

	int GetUsedWidth ( int ipStart, int ipLast );
	bool CalcShownButtons ( int ipWidth, int &ipStart, int &ipEnd, bool &bpPartiell );
	void OnSizeChange (int cx, int cy);
	void Display();

protected:
	//{{AFX_MSG(CScrollToolBar)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnLeft();
	afx_msg void OnRight();
	afx_msg void OnWindowPosChanging( WINDOWPOS* lpwndpos );
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class CScrollRebar : public CReBar  
{
public:
	CScrollRebar();
	virtual ~CScrollRebar();

	CScrollToolBar* AddScrollToolBar ( UINT idToolBar );
	CButton* AddButton ( LPCTSTR lpszCaption, DWORD dwStyle, const RECT& rect, 
								   CWnd* pParentWnd, UINT nID  );
	CComboBox* AddComboBox ( DWORD dwStyle, const RECT& rect, 
								     CWnd* pParentWnd, UINT nID  );
	BOOL Create(CWnd* pParentWnd, DWORD dwCtrlStyle = RBS_FIXEDORDER,
						  DWORD dwStyle = WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_TOP,
						  UINT nID = AFX_IDW_REBAR );
	bool OnChildSizeChanged ( CWnd * pWnd, int cx, int cy );
protected:
	CObArray omItems;

	afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
	DECLARE_MESSAGE_MAP()
};

#endif // !defined(AFX_SCROLLTOOLBAR_H__C4859BA5_7A4C_11D3_937C_00001C033B5D__INCLUDED_)
