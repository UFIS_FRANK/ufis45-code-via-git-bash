// searchflightpage.cpp : implementation file
//

#include <stdafx.h>
#include <PSsearchflightpage.h>
#include <CCSTime.h>
//#include "CCSItem.h"

#define CTABLE_NAME         "FLUGPLAN"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSearchFlightPage property page

IMPLEMENT_DYNCREATE(CSearchFlightPage, CPropertyPage)

CSearchFlightPage::CSearchFlightPage() : CPropertyPage(CSearchFlightPage::IDD)
{
	//{{AFX_DATA_INIT(CSearchFlightPage)
/*	
	m_Check1 = FALSE;
	m_Check2 = FALSE;
	m_Check3 = FALSE;
	m_Check4 = FALSE;
	m_Check5 = FALSE;
	m_Check6 = FALSE;
	m_Check7 = FALSE;
	m_Check8 = FALSE;
*/
	//}}AFX_DATA_INIT
/*	omFieldList.strgSetValue("TIFA From || TIFD From;Date,TIFA From || TIFD From; Time," 
		                "TIFA To || TIFD To; Date,TIFA To || TIFD To; Time," 
						"SearchHours, SearchDay,DOOA || DOOD," 
		                "RelHBefore,RelHAfter,LSTU; Date,LSTU; Time," 
						"FDAT; Date,FDAT; Time," 
						"ALC2,FLTN,FLNS,REGN,ACT3,ORG3 || ORG4 || DES3 || DES4,"
						"TIFA Flugzeit || TIFD Flugzeit; Date,"
						"TIFA Flugzeit || TIFD Flugzeit; Time,TTYP,STEV");
	InitPage();
*/
}

CSearchFlightPage::~CSearchFlightPage()
{
/*	pomPageBuffer = NULL;
	for (int ilIdx = omData.GetSize()-1; ilIdx >=0; ilIdx--)
	{
		DISPStrukt *polField;
		polField = &omData[ilIdx];
		delete polField;
		omData.RemoveAt(ilIdx);
	} // end for
*/
}

void CSearchFlightPage::SetCalledFrom(CString opCalledFrom)
{
	omCalledFrom = opCalledFrom;
}

void CSearchFlightPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchFlightPage)
	DDX_Control(pDX, IDC_CHECK1, m_Check1);
	DDX_Control(pDX, IDC_CHECK2, m_Check2);
	DDX_Control(pDX, IDC_CHECK3, m_Check3);
	DDX_Control(pDX, IDC_CHECK4, m_Check4);
	DDX_Control(pDX, IDC_CHECK5, m_Check5);
	DDX_Control(pDX, IDC_CHECK6, m_Check6);
	DDX_Control(pDX, IDC_CHECK7, m_Check7);
	DDX_Control(pDX, IDC_CHECK8, m_Check8);

	DDX_Control(pDX, IDC_EDIT, m_Edit1);
	DDX_Control(pDX, IDC_EDIT10, m_Edit10);
	DDX_Control(pDX, IDC_EDIT11, m_Edit11);
	DDX_Control(pDX, IDC_EDIT12, m_Edit12);
	DDX_Control(pDX, IDC_EDIT13, m_Edit13);
	DDX_Control(pDX, IDC_EDIT14, m_Edit14);
	DDX_Control(pDX, IDC_EDIT15, m_Edit15);
	DDX_Control(pDX, IDC_EDIT16, m_Edit16);
	DDX_Control(pDX, IDC_EDIT17, m_Edit17);
	DDX_Control(pDX, IDC_EDIT18, m_Edit18);
	DDX_Control(pDX, IDC_EDIT19, m_Edit19);
	DDX_Control(pDX, IDC_EDIT2, m_Edit2);
	DDX_Control(pDX, IDC_EDIT20, m_Edit20);
	DDX_Control(pDX, IDC_EDIT21, m_Edit21);
	DDX_Control(pDX, IDC_EDIT22, m_Edit22);
	DDX_Control(pDX, IDC_EDIT23, m_Edit23);
	DDX_Control(pDX, IDC_EDIT3, m_Edit3);
	DDX_Control(pDX, IDC_EDIT4, m_Edit4);
	DDX_Control(pDX, IDC_EDIT5, m_Edit5);
	DDX_Control(pDX, IDC_EDIT6, m_Edit6);
	DDX_Control(pDX, IDC_EDIT7, m_Edit7);
	DDX_Control(pDX, IDC_EDIT8, m_Edit8);
	DDX_Control(pDX, IDC_EDIT9, m_Edit9);
	//}}AFX_DATA_MAP

/*	DDX_Check(pDX, IDC_CHECK1, m_Check1);
	DDX_Check(pDX, IDC_CHECK2, m_Check2);
	DDX_Check(pDX, IDC_CHECK3, m_Check3);
	DDX_Check(pDX, IDC_CHECK4, m_Check4);
	DDX_Check(pDX, IDC_CHECK5, m_Check5);
	DDX_Check(pDX, IDC_CHECK6, m_Check6);
	DDX_Check(pDX, IDC_CHECK7, m_Check7);
	DDX_Check(pDX, IDC_CHECK8, m_Check8);
*/
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		SetData();
	}
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		GetData();
	}
	
	/*    
	m_Edit1.GetWindowText(omData[0].omData);// TIFA From || TIFD From; Date
    m_Edit2.GetWindowText(omData[1].omData);// TIFA From || TIFD From; Time
    m_Edit3.GetWindowText(omData[2].omData);// TIFA To || TIFD To; Date
    m_Edit4.GetWindowText(omData[3].omData);// TIFA To || TIFD To; Time
    m_Edit5.GetWindowText(omData[4].omData);// SearchHours
    m_Edit6.GetWindowText(omData[5].omData);// SearchDay
    m_Edit7.GetWindowText(omData[6].omData);// DOOA || DOOD
    m_Edit8.GetWindowText(omData[7].omData);// RelHBefore
    m_Edit9.GetWindowText(omData[8].omData);// RelHAfter
    m_Edit10.GetWindowText(omData[9].omData);// LSTU; Date
    m_Edit11.GetWindowText(omData[10].omData);// LSTU; Time
    m_Edit12.GetWindowText(omData[11].omData);// FDAT; Date
    m_Edit13.GetWindowText(omData[12].omData);// FDAT; Time
    m_Edit14.GetWindowText(omData[13].omData);// ACL2
    m_Edit15.GetWindowText(omData[14].omData);// FLTN
    m_Edit16.GetWindowText(omData[15].omData);// FLNS
    m_Edit17.GetWindowText(omData[16].omData);// REGN
    m_Edit18.GetWindowText(omData[17].omData);// ACT3
    m_Edit19.GetWindowText(omData[18].omData);// ORG3 || ORG4 || DES3 || DES4
    m_Edit20.GetWindowText(omData[19].omData);// TIFA Flugzeit || TIFD Flugzeit; Date
    m_Edit21.GetWindowText(omData[20].omData);// TIFA Flugzeit || TIFD Flugzeit; Time
    m_Edit22.GetWindowText(omData[21].omData);// TTYP
    m_Edit23.GetWindowText(omData[22].omData);// STEV
*/
}


BEGIN_MESSAGE_MAP(CSearchFlightPage, CPropertyPage)
	//{{AFX_MSG_MAP(CSearchFlightPage)
		// NOTE: the ClassWizard will add message map macros here
	ON_BN_CLICKED(IDC_CHECK1, OnClickedAnkunft)
	ON_BN_CLICKED(IDC_CHECK2, OnClickedAbflug)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CSearchFlightPage::GetData()
{
	omValues.RemoveAll();
	CStringArray olTmpValues;
	GetData(olTmpValues);
	for(int i = 0; i < olTmpValues.GetSize(); i++)
	{
		CString olTmp = olTmpValues[i];
		omValues.Add(olTmpValues[i]);
/*		switch(i)
		{
		case 0:
			olTmp = olTmpValues[i] + olTmpValues[i+1];
			omValues.Add(olTmp);
			i++;
			break;
		case 2:
			olTmp = olTmpValues[i] + olTmpValues[i+1];
			omValues.Add(olTmp);
			i++;
			break;
		case 9:
			olTmp = olTmpValues[i] + olTmpValues[i+1];
			omValues.Add(olTmp);
			i++;
			break;
		case 11:
			olTmp = olTmpValues[i] + olTmpValues[i+1];
			omValues.Add(olTmp);
			i++;
			break;
		case 19:
			olTmp = olTmpValues[i] + olTmpValues[i+1];
			omValues.Add(olTmp);
			i++;
			break;
		default:
			omValues.Add(olTmpValues[i]);
		}
*/
	}

/*	for(i = 0; i < omValues.GetSize(); i++)
	{
		CString olStr = omValues[i];
		int o;
		o++;
	}
*/
}
void CSearchFlightPage::GetData(CStringArray &ropValues)
{
	CString olT1, olT2, olT3, olT4;
	CString olResult;
	CTime olDate, olTime;
	m_Edit1.GetWindowText(olT1);// TIFA From || TIFD From; Date
	m_Edit2.GetWindowText(olT2);
	m_Edit3.GetWindowText(olT3);
	m_Edit4.GetWindowText(olT4);
	//Erst mal einen precheck und was leer ist wird 
	//entsprechend gesetzt

	if(olT1.IsEmpty() && olT2.IsEmpty() && olT3.IsEmpty() && olT4.IsEmpty())
	{
		;
	}
	else
	{
		if(olT2.IsEmpty())
		{
			olT2 = CString("00:00");
			m_Edit2.SetWindowText(olT2);
		}
		if(olT4.IsEmpty())
		{
			olT4 = CString("23:59");
			m_Edit4.SetWindowText(olT4);
		}
		if(olT1.IsEmpty() && olT3.IsEmpty())
		{
			olT1 = CTime::GetCurrentTime().Format("%d.%m.%Y");
			olT3 = olT1;
			m_Edit1.SetWindowText(olT1);
			m_Edit3.SetWindowText(olT3);
		}
		if(!olT1.IsEmpty() && olT3.IsEmpty())
		{
			olT3 = olT1;
			m_Edit3.SetWindowText(olT3);
		}
		if(olT1.IsEmpty() && !olT3.IsEmpty())
		{
			olT1 = olT3;
			m_Edit1.SetWindowText(olT1);
		}
	}
	if(olT1 != "" && olT2 != "")
	{
		olDate = DateStringToDate(olT1);
		if(olTime != TIMENULL)
			olT1 = olDate.Format("%Y%m%d");
		else
			olT1 = " ";
		if(olDate != TIMENULL)
		{
			olTime = HourStringToDate(olT2, olDate);
			if(olTime != TIMENULL)
				olT1 = olDate.Format("%Y%m%d") + olTime.Format("%H%M%S");
			else
				olT1 = " ";
		}
	}
	else
	{
		olT1 = " ";
		olDate = TIMENULL;
	}
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");

/*    m_Edit2.GetWindowText(olT1);// TIFA From || TIFD From; Time
	if(olT1 != "")
	{
		olTime = HourStringToDate(olT1, olDate);
		if(olTime != TIMENULL)
			olT1 = olTime.Format("%H%M%S");
		else
			olT1 = " ";
	}
	else
	{
		olT1 = " ";
	}
	ropValues.Add(olT1);
*/
    m_Edit3.GetWindowText(olT1);// TIFA To || TIFD To; Date
	m_Edit4.GetWindowText(olT2);
	if(olT1 != "" && olT2 != "")
	{
		olDate = DateStringToDate(olT1);
		if(olTime != TIMENULL)
			olT1 = olDate.Format("%Y%m%d");
		else
			olT1 = " ";
		if(olDate != TIMENULL)
		{
			olTime = HourStringToDate(olT2, olDate);
			if(olTime != TIMENULL)
				olT1 = olDate.Format("%Y%m%d") + olTime.Format("%H%M%S");
			else
				olT1 = " ";
		}
	}
	else
	{
		olT1 = " ";
		olDate = TIMENULL;
	}
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");

/*    m_Edit4.GetWindowText(olT1);// TIFA To || TIFD To; Time
	if(olT1 != "")
	{
		olTime = HourStringToDate(olT1, olDate);
		if(olTime != TIMENULL)
			olT1 = olTime.Format("%H%M%S");
		else
			olT1 = " ";
	}
	else
	{
		olT1 = " ";
	}
	ropValues.Add(olT1);
*/  m_Edit5.GetWindowText(olT1);// SearchHours
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_Edit6.GetWindowText(olT1);// SearchDay
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_Edit7.GetWindowText(olT1);// DOOA || DOOD
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_Edit8.GetWindowText(olT1);// RelHBefore
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_Edit9.GetWindowText(olT1);// RelHAfter
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_Edit10.GetWindowText(olT1);// LSTU; Date
	m_Edit11.GetWindowText(olT2);
	if(olT1 != "" && olT2 != "")
	{
		olDate = DateStringToDate(olT1);
		if(olTime != TIMENULL)
			olT1 = olDate.Format("%Y%m%d");
		else
			olT1 = " ";
		if(olDate != TIMENULL)
		{
			olTime = HourStringToDate(olT2, olDate);
			if(olTime != TIMENULL)
				olT1 = olDate.Format("%Y%m%d") + olTime.Format("%H%M%S");
			else
				olT1 = " ";
		}
	}
	else
	{
		olT1 = " ";
		olDate = TIMENULL;
	}
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
/*    m_Edit11.GetWindowText(olT1);// LSTU; Time
	if(olT1 != "")
	{
		olTime = HourStringToDate(olT1, olDate);
		if(olTime != TIMENULL)
			olT1 = olTime.Format("%H%M%S");
		else
			olT1 = " ";
	}
	else
	{
		olT1 = " ";
	}
	ropValues.Add(olT1);
*/  m_Edit12.GetWindowText(olT1);// FDAT; Date
	m_Edit13.GetWindowText(olT2);
	if(olT1 != "" && olT2 != "")
	{
		olDate = DateStringToDate(olT1);
		if(olTime != TIMENULL)
			olT1 = olDate.Format("%Y%m%d");
		else
			olT1 = " ";
		if(olDate != TIMENULL)
		{
			olTime = HourStringToDate(olT2, olDate);
			if(olTime != TIMENULL)
				olT1 = olDate.Format("%Y%m%d") + olTime.Format("%H%M%S");
			else
				olT1 = " ";
		}
	}
	else
	{
		olT1 = " ";
		olDate = TIMENULL;
	}
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
/*    m_Edit13.GetWindowText(olT1);// FDAT; Time
	if(olT1 != "")
	{
		olTime = HourStringToDate(olT1, olDate);
		if(olTime != TIMENULL)
			olT1 = olTime.Format("%H%M%S");
		else
			olT1 = " ";
	}
	else
	{
		olT1 = " ";
	}
	ropValues.Add(olT1);
*/    m_Edit14.GetWindowText(olT1);// ACL2
	if(olT1 == "")
		olT1 = " ";
	olResult += olT1 + CString("|");
	ropValues.Add(olT1);
    m_Edit15.GetWindowText(olT1);// FLTN
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_Edit16.GetWindowText(olT1);// FLNS
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_Edit17.GetWindowText(olT1);// REGN
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_Edit18.GetWindowText(olT1);// ACT3
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_Edit19.GetWindowText(olT1);// ORG3 || ORG4 || DES3 || DES4
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_Edit20.GetWindowText(olT1);// TIFA Flugzeit || TIFD Flugzeit; Date
	m_Edit21.GetWindowText(olT2);
	if(olT1 != "" && olT2 != "")
	{
		olDate = DateStringToDate(olT1);
		if(olTime != TIMENULL)
			olT1 = olDate.Format("%Y%m%d");
		else
			olT1 = " ";
		if(olDate != TIMENULL)
		{
			olTime = HourStringToDate(olT2, olDate);
			if(olTime != TIMENULL)
				olT1 = olDate.Format("%Y%m%d") + olTime.Format("%H%M%S");
			else
				olT1 = " ";
		}
	}
	else
	{
		olT1 = " ";
		olDate = TIMENULL;
	}
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
/*    m_Edit21.GetWindowText(olT1);// TIFA Flugzeit || TIFD Flugzeit; Time
	if(olT1 != "")
	{
		olTime = HourStringToDate(olT1, olDate);
		if(olT1ime != TIMENULL)
			olT1 = olT1ime.Format("%H%M%S");
		else
			olT1 = " ";
	}
	else
	{
		olT1 = " ";
	}
	ropValues.Add(olT1);
*/    m_Edit22.GetWindowText(olT1);// TTYP
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");
    m_Edit23.GetWindowText(olT1);// STEV
	if(olT1 == "")
		olT1 = " ";
	ropValues.Add(olT1);
	olResult += olT1 + CString("|");

	if(m_Check1.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	if(m_Check2.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	if(m_Check3.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	if(m_Check4.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	if(m_Check5.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	if(m_Check6.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	if(m_Check7.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	if(m_Check8.GetCheck() == 1)
	{
		olT1 = "J";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	else
	{
		olT1 = " ";
		ropValues.Add(olT1);
		olResult += olT1 + CString("|");
	}
	//ropValues.Add(olResult);

}

void CSearchFlightPage::SetData()
{
	CStringArray olTmpValues;
//	for(int i = omValues.GetSize()-1; i < 26; i++)
//	{
//		omValues.Add(CString(""));
//	}
	SetData(omValues);
}

void CSearchFlightPage::SetData(CStringArray &ropValues)
{
	m_Edit1.SetWindowText("");// TIFA From || TIFD From; Date
    m_Edit2.SetWindowText("");// TIFA From || TIFD From; Time
    m_Edit3.SetWindowText("");// TIFA To || TIFD To; Date
    m_Edit4.SetWindowText("");// TIFA To || TIFD To; Time
    m_Edit5.SetWindowText("");// SearchHours
    m_Edit6.SetWindowText("");// SearchDay
    m_Edit7.SetWindowText("");// DOOA || DOOD
    m_Edit8.SetWindowText("");// RelHBefore
    m_Edit9.SetWindowText("");// RelHAfter
    m_Edit10.SetWindowText("");// LSTU; Date
    m_Edit11.SetWindowText("");// LSTU; Time
    m_Edit12.SetWindowText("");// FDAT; Date
    m_Edit13.SetWindowText("");// FDAT; Time
    m_Edit14.SetWindowText("");// ACL2
    m_Edit15.SetWindowText("");// FLTN
    m_Edit16.SetWindowText("");// FLNS
    m_Edit17.SetWindowText("");// REGN
    m_Edit18.SetWindowText("");// ACT3
    m_Edit19.SetWindowText("");// ORG3 || ORG4 || DES3 || DES4
    m_Edit20.SetWindowText("");// TIFA Flugzeit || TIFD Flugzeit; Date
    m_Edit21.SetWindowText("");// TIFA Flugzeit || TIFD Flugzeit; Time
    m_Edit22.SetWindowText("");// TTYP
    m_Edit23.SetWindowText("");// STEV

	m_Check1.SetCheck(0);
	m_Check2.SetCheck(0);
	m_Check3.SetCheck(0);
	m_Check4.SetCheck(0);
	m_Check5.SetCheck(0);
	m_Check6.SetCheck(0);
	m_Check7.SetCheck(0);
	m_Check8.SetCheck(0);

	for(int i = 0; i < ropValues.GetSize(); i++)
	{
		CTime olDate = TIMENULL;
		CString olV = ropValues[i];
		switch(i)
		{
		case 0:
			olDate = DBStringToDateTime(ropValues[i]);
			if(olDate != TIMENULL)
			{
				m_Edit1.SetWindowText(olDate.Format("%d.%m.%Y"));
				m_Edit2.SetWindowText(olDate.Format("%H:%M"));
			}
			else
			{
				m_Edit1.SetWindowText(CString(""));
				m_Edit2.SetWindowText(CString(""));
			}
			break;
		case 1:
			olDate = DBStringToDateTime(ropValues[i]);
			if(olDate != TIMENULL)
			{
				m_Edit3.SetWindowText(olDate.Format("%d.%m.%Y"));
				m_Edit4.SetWindowText(olDate.Format("%H:%M"));
			}
			else
			{
				m_Edit3.SetWindowText(CString(""));
				m_Edit4.SetWindowText(CString(""));
			}
			break;
		case 2:
			if(ropValues[i] != CString(" "))
			{
				m_Edit5.SetWindowText(ropValues[i]);
			}
			break;
		case 3:
			if(ropValues[i] != CString(" "))
				m_Edit6.SetWindowText(ropValues[i]);
			break;
		case 4:
			if(ropValues[i] != CString(" "))
				m_Edit7.SetWindowText(ropValues[i]);
			break;
		case 5:
			if(ropValues[i] != CString(" "))
				m_Edit8.SetWindowText(ropValues[i]);
			break;
		case 6:
			if(ropValues[i] != CString(" "))
				m_Edit9.SetWindowText(ropValues[i]);
			break;
		case 7:
			olDate = DBStringToDateTime(ropValues[i]);
			if(olDate != TIMENULL)
			{
				m_Edit10.SetWindowText(olDate.Format("%d.%m.%Y"));
				m_Edit11.SetWindowText(olDate.Format("%H:%M"));
			}
			else
			{
				m_Edit10.SetWindowText(CString(""));
				m_Edit11.SetWindowText(CString(""));
			}
			break;
		case 8:
			olDate = DBStringToDateTime(ropValues[i]);
			if(olDate != TIMENULL)
			{
				m_Edit12.SetWindowText(olDate.Format("%d.%m.%Y"));
				m_Edit13.SetWindowText(olDate.Format("%H:%M"));
			}
			else
			{
				m_Edit12.SetWindowText(CString(""));
				m_Edit13.SetWindowText(CString(""));
			}
			break;
		case 9:
			if(ropValues[i] != CString(" "))
				m_Edit14.SetWindowText(ropValues[i]);
			break;
		case 10:
			if(ropValues[i] != CString(" "))
				m_Edit15.SetWindowText(ropValues[i]);
			break;
		case 11:
			if(ropValues[i] != CString(" "))
				m_Edit16.SetWindowText(ropValues[i]);
			break;
		case 12:
			if(ropValues[i] != CString(" "))
				m_Edit17.SetWindowText(ropValues[i]);
			break;
		case 13:
			if(ropValues[i] != CString(" "))
				m_Edit18.SetWindowText(ropValues[i]);
			break;
		case 14:
			if(ropValues[i] != CString(" "))
				m_Edit19.SetWindowText(ropValues[i]);
			break;
		case 15:
			olDate = DBStringToDateTime(ropValues[i]);
			if(olDate != TIMENULL)
			{
				m_Edit20.SetWindowText(olDate.Format("%d.%m.%Y"));
				m_Edit21.SetWindowText(olDate.Format("%H:%M"));
			}
			else
			{
				m_Edit20.SetWindowText(CString(""));
				m_Edit21.SetWindowText(CString(""));
			}
			break;
		case 16:
			if(ropValues[i] != CString(" "))
				m_Edit22.SetWindowText(ropValues[i]);
			break;
		case 17:
			if(ropValues[i] != CString(" "))
				m_Edit23.SetWindowText(ropValues[i]);
			break;
		case 18: //Ab hier CheckBoxen
			if(ropValues[i] == "J")
			{
				m_Check1.SetCheck(1);
			} 
			else
			{
				m_Check1.SetCheck(0);
			}
			break;
		case 19:
			if(ropValues[i] == "J")
			{
				m_Check2.SetCheck(1);
			}
			else
			{
				m_Check2.SetCheck(0);
			}
			break;
		case 20:
			if(ropValues[i] == "J")
			{
				m_Check3.SetCheck(1);
			}
			else
			{
				m_Check3.SetCheck(0);
			}
			break;
		case 21:
			if(ropValues[i] == "J")
			{
				m_Check4.SetCheck(1);
			}
			else
			{
				m_Check4.SetCheck(0);
			}
			break;
		case 22:
			if(ropValues[i] == "J")
			{
				m_Check5.SetCheck(1);
			}
			else
			{
				m_Check5.SetCheck(0);
			}
			break;
		case 23:
			if(ropValues[i] == "J")
			{
				m_Check6.SetCheck(1);
			}
			else
			{
				m_Check6.SetCheck(0);
			}
			break;
		case 24:
			if(ropValues[i] == "J")
			{
				m_Check7.SetCheck(1);
			}
			else
			{
				m_Check7.SetCheck(0);
			}
			break;
		case 25:
			if(ropValues[i] == "J")
			{
				m_Check8.SetCheck(1);
			}
			else
			{
				m_Check8.SetCheck(0);
			}
			break;
		}
	}

	if(m_Check1.GetCheck() == 0 && m_Check2.GetCheck() == 0)
	{
		m_Check1.SetCheck(1);
		m_Check2.SetCheck(1);
	}
	if(omCalledFrom == "SEASON")
	{
		m_Check4.SetCheck(1);
		m_Check3.SetCheck(1);
		m_Check6.SetCheck(1);
		m_Check7.SetCheck(1);
	}
	if(omCalledFrom == "ROTATION")
	{
		m_Check3.SetCheck(1);
		m_Check6.SetCheck(1);
		m_Check7.SetCheck(1);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CSearchFlightPage message handlers
void CSearchFlightPage::InitPage()
{

	
/*	CCSItem olOperator(">=,>=,<=,<=,=,=,=,=,=,>=,>=,=,=,=,=,=,=,=,=,=,=,=,=");
	int ilMaxItem = omFieldList.itemCount();
	for (int ilIdx = 0; ilIdx < ilMaxItem; ilIdx++)
	{
		DISPStrukt *polField;
		polField = new DISPStrukt;
		CString olFieldName(omFieldList.itemGetValue(ilIdx+1));
		polField->omFieldName = olFieldName;
		CString olOperator(olOperator.itemGetValue(ilIdx+1));
		polField->omOperator = olOperator;
		omData.Add(polField);
	} // end 
*/
} // end InitPage

/*int CSearchFlightPage::CreateFilter()
{
	UpdateData(TRUE);
	int ilMaxItem=omData.GetSize();

	pomPageBuffer = (CPageBuffer *)pomDataBuffer;

	if (pomPageBuffer != NULL)
	{
	    pomPageBuffer->DeleteResult();
		int ilIdx = 0; 
		while (ilIdx < ilMaxItem)
		{
			CString olData(omData[ilIdx].omData);
			CString olField(omData[ilIdx].omFieldName);
			if (olData.GetLength() > 0)
			{
				switch (ilIdx)
				{
				case 0: // Zeitraum absolut von
					olData+=omData[ilIdx+1].omData;
					olField.Empty();
					if (m_Check1 == TRUE)
						olField = "TIFA From";
					if (m_Check2== TRUE)
						olField+= "TIFD From";
					ilIdx++;
					break;
				case 2: // Zeitraum absolut bis
					olData+=omData[ilIdx+1].omData;
					olField.Empty();
					if (m_Check1 == TRUE)
						olField = "TIFA To";
					if (m_Check2== TRUE)
						olField+= "TIFD To";
					ilIdx++;
					break;
				case 9: // LastModify Date 10= LastModify Time
					olData+=omData[ilIdx+1].omData;
					olField = "LSTU";
					ilIdx++;
					break;
				case 11: // Fluko Date 12= Fluko Time
					olData+=omData[ilIdx+1].omData;
					olField = "FDAT";
					ilIdx++;
					break;
				case 13: // AirlineCode ALC2/ALC3
					if (olData.GetLength() > 2)
					{
						olField = "ALC3"; // default = ALC2
					} // end
					break;
				case 17: // Flugzeugtyp ACT3/ACT5
					if (olData.GetLength() > 3)
					{
						olField = "ACT5"; // default = ACT3
					} // end
					break;
				case 18: // Flughafen ACT3/ACT5
					olField.Empty();
					if (m_Check1 == TRUE)
						olField = CString("ORG") + CString((olData.GetLength() > 3 ? "4" : "3"));
					if (m_Check2== TRUE)
						olField+= CString("DES") + CString((olData.GetLength() > 3 ? "4" : "3"));
					break;
				case 19:
					olData+=omData[ilIdx+1].omData;
					if (m_Check1 == TRUE)
						olField = "TIFA Flugzeit";
					if (m_Check2== TRUE)
						olField+= "TIFD Flugzeit";
					ilIdx++;
				default:
					break;
				} // end switch
			   pomPageBuffer->SetResult(0, CTABLE_NAME, olField, omData[ilIdx].omOperator,
                                        olData, "AND");
			} // end if olData.strgGetSize
			ilIdx++;
		} // end 
	   CString olData, olOperator;
	   CString olField;
	   BOOL blChecked = FALSE;
	   for (ilIdx = 3 ; ilIdx <= 8; ilIdx++)
	   {
		   switch (ilIdx)
		   {
		   case 5:
			   blChecked = m_Check5;
			   olField = "VERS";
			   olData = "NULL";
			   olOperator = "!=";
			   break;
		   case 3:
			   blChecked = m_Check3;
		       olField = "FTYP";
			   olOperator = "=";
			   olData = "O";
			   break;
		   case 4:
			   blChecked = m_Check4;
		       olField = "FTYP";
			   olOperator = "=";
			   olData = "S";
			   break;
		   case 6:
   			   blChecked = m_Check6;
		       olField = "FTYP";
			   olOperator = "=";
			   olData = "X";
			   break;
		   case 7:
			   blChecked = m_Check7;
		       olField = "FTYP";
			   olOperator = "=";
			   olData = "N";
			   break;
		   case 8:
			   blChecked = m_Check8;
		       olField = "DELETE";
			   olOperator = "=";
			   olData = "";
			   break;
		   default:
			   blChecked = FALSE;
		   };

           if (blChecked == TRUE)
				pomPageBuffer->SetResult(0, CTABLE_NAME, olField, olOperator,
					                        olData, "OR");
	   } // end for
	} // end if
		return(RC_SUCCESS);
} // end CreateFilter

void *CSearchFlightPage::GetData()
{
    CreateFilter();
	return(pomDataBuffer);
};

void CSearchFlightPage::InitialScreen()
{
	CPageBuffer *polPageBuffer = (CPageBuffer *)pomInitBuffer;
	int ilField = 0;
	int ilFldIdx = 0;
	CString olField, olDesc, olFormat, olOperator, olData, olTable(CTABLE_NAME);
	if (polPageBuffer != NULL)
	{
		for (ilFldIdx = omData.GetSize()-1; ilFldIdx >= 0; ilFldIdx--)
		{
			omData[ilFldIdx].omData = "";
		} // end for
		while (polPageBuffer->GetInit(0,ilField, olTable, olField,
			   olDesc, olFormat, olOperator, olData) == true)
		{
			ilFldIdx = omFieldList.itemGetIndex( olField.GetBuffer(0) );
			if (ilFldIdx > 0)
			{
				omData[ilFldIdx-1].omData = olData;
				omData[ilFldIdx-1].omOperator = olOperator;
			} // end if
			ilField++;
		} // end while
		if (ilField > 0)
		{
			m_Edit1.SetWindowText(omData[0].omData);
			m_Edit2.SetWindowText(omData[1].omData);
			m_Edit3.SetWindowText(omData[2].omData);
			m_Edit4.SetWindowText(omData[3].omData);
			m_Edit5.SetWindowText(omData[4].omData);
			m_Edit6.SetWindowText(omData[5].omData);
			m_Edit7.SetWindowText(omData[6].omData);
			m_Edit8.SetWindowText(omData[7].omData);
			m_Edit9.SetWindowText(omData[8].omData);
			m_Edit10.SetWindowText(omData[9].omData);
			m_Edit11.SetWindowText(omData[10].omData);
			m_Edit12.SetWindowText(omData[11].omData);
			m_Edit13.SetWindowText(omData[12].omData);
			m_Edit14.SetWindowText(omData[13].omData);
			m_Edit15.SetWindowText(omData[14].omData);
			m_Edit16.SetWindowText(omData[15].omData);
			m_Edit17.SetWindowText(omData[16].omData);
			m_Edit18.SetWindowText(omData[17].omData);
			m_Edit19.SetWindowText(omData[18].omData);
			m_Edit20.SetWindowText(omData[19].omData);
			m_Edit21.SetWindowText(omData[20].omData);
			m_Edit22.SetWindowText(omData[21].omData);
			m_Edit23.SetWindowText(omData[22].omData);		
		} // end if
	} // end if
} // end InitialScreen
*/
//---------------------------------------------------------------------------------
CString CSearchFlightPage::GetPrivList()
{
	//char aclInitFieldList[] = "APPL,SUBD,FUNC,FUAL,TYPE,STAT";
	CString olInitPageData = /*pcgAppName +
		CString("InitModu,InitModu,Initialisieren (InitModu),B,-,") + */
		CString("SearchFlightPage,TIFA From || TIFD From;Date,TimeFrameDateFrom,E,1,") +
		CString("SearchFlightPage,TIFA From || TIFD From;Time,TimeFrameTimeFrom,E,1,") +
		CString("SearchFlightPage,TIFA To || TIFD To;Date,TimeFrameDateTo,E,1,") +
		CString("SearchFlightPage,TIFA To || TIFD To;Time,TimeFrameTimeTo,E,1,") +
		CString("SearchFlightPage,SearchHours,SearchHours,E,1,") +
		CString("SearchFlightPage,SearchDay,SearchDay,E,1,") +
		CString("SearchFlightPage,DOOA || DOOD,DayOfOperation,E,1,") +
		CString("SearchFlightPage,RelHBefore,RelHBefore,E,1,") +
		CString("SearchFlightPage,RelHAfter,RelHAfter,E,1,") +
		CString("SearchFlightPage,LSTU;Date,LSTUDate,E,1,") +
		CString("SearchFlightPage,LSTU;Time,LSTUTime,E,1,") +
		CString("SearchFlightPage,FDAT;Date,FDATDate,E,1,") +
		CString("SearchFlightPage,FDAT;Time,FDATTime,E,1,") +
		CString("SearchFlightPage,ALC2,ALC2,E,1,") +
		CString("SearchFlightPage,FLTN,FLTN,E,1,") +
		CString("SearchFlightPage,FLNS,FLNS,E,1,") +
		CString("SearchFlightPage,REGN,REGN,E,1,") +
		CString("SearchFlightPage,ACT3,ACT3,E,1,") +
		CString("SearchFlightPage,ORG3 || ORG4 || DES3 || DES4,OrgDest,E,1,") +
		CString("SearchFlightPage,TIFA Flugzeit || TIFD Flugzeit;Date,FlugZeitDate,E,1,") +
		CString("SearchFlightPage,TIFA Flugzeit || TIFD Flugzeit;Time,FlugZeitTime,E,1,") +
		CString("SearchFlightPage,TTYP,TTYP,E,1,") +
		CString("SearchFlightPage,STEV,STEV,B,1,") +
		CString("SearchFlightPage,Ankunft,AnkunftButton,B,1,")+
		CString("SearchFlightPage,Abflug,AbflugButton,B,1,")+
		CString("SearchFlightPage,Betrieb,BetriebButton,B,1,")+
		CString("SearchFlightPage,Planung,PlanungButton,B,1,")+
		CString("SearchFlightPage,Prognose,PrognoseButton,B,1,")+
		CString("SearchFlightPage,Cancelled,CancelledButton,B,1,")+
		CString("SearchFlightPage,Noop,NoopButton,B,1,")+
		CString("SearchFlightPage,gelöscht,gelöschtButton,B,1,");
	return(olInitPageData);
}

void CSearchFlightPage::OnClickedAbflug()
{
	if(m_Check2.GetCheck() == 0)
	{
		if(m_Check1.GetCheck() == 0)
		{
			m_Check2.SetCheck(1);
		}
	}
}

void CSearchFlightPage::OnClickedAnkunft()
{
	if(m_Check1.GetCheck() == 0)
	{
		if(m_Check2.GetCheck() == 0)
		{
			m_Check1.SetCheck(1);
		}
	}
}

