#ifndef AFX_PSFLIGHTPAGE_H__3BE90563_A2F3_11D1_BD3C_0000B4392C49__INCLUDED_
#define AFX_PSFLIGHTPAGE_H__3BE90563_A2F3_11D1_BD3C_0000B4392C49__INCLUDED_

// PsFlightPage.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CPsFlightPage 

class CPsFlightPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CPsFlightPage)

// Konstruktion
public:
	CPsFlightPage();
	~CPsFlightPage();

	void SetCaption(const char *pcpCaption);
	char pcmCaption[100];

	void SetData();
	void GetData();


	CStringArray omValues;
	CTime omStartTime;
	CTime omEndTime;
	CString omToday;

// Dialogfelddaten
	//{{AFX_DATA(CPsFlightPage)
	enum { IDD = IDD_PSFLIGHT_PAGE };
	CButton	m_Arrival;
	CButton	m_Depature;
	CButton	m_Betrieb;
	CButton	m_Planung;
	CButton m_Prognose;
	CButton	m_Cancelled;
	CButton	m_Noop;
	BOOL	m_ArrivalVal;
	BOOL	m_DepatureVal;
	BOOL	m_BetriebVal;
	BOOL	m_PlanungVal;
	BOOL	m_PrognoseVal;
	BOOL	m_CancelledVal;
	BOOL	m_NoopVal;
	//}}AFX_DATA


// Überschreibungen
	// Der Klassen-Assistent generiert virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CPsFlightPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL


// Implementierung
protected:
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CPsFlightPage)	
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_PSFLIGHTPAGE_H__3BE90563_A2F3_11D1_BD3C_0000B4392C49__INCLUDED_
