 
// cDemd.cpp - Class for handling Dem data
//

#include <stdafx.h>
#include <afxwin.h>
#include <CCSCedaData.h>
//#include <CedaFlightData.h>
#include <CedaDemData.h>
#include <CedaSgmData.h>
#include <CedaFlightData.h>
#include <CedaBasicData.h>
#include <DataSet.h>
#include <ccsddx.h>
#include <UFIS.h>
#include <CCSBcHandle.h>

#include <CflReadyWithRuleDiagnosticsDlg.h>
#include <CCSGlobl.h>
// std template include files
#include <algorithm>
#include <stdio.h>

// _ftime include files
#include <sys/timeb.h>
#include <time.h>

long lgNextDemUrno;
extern bool bgDoTimer;

CedaDemData::CedaDemData() : CCSCedaData(&ogCommHandler)
{   

	// Create an array of CEDARECINFO for STAFFDATA
    BEGIN_CEDARECINFO(DEMDATA, DemDataRecInfo)
		FIELD_CHAR_TRIM(Aloc,"ALOC")
		FIELD_DATE	   (Cdat,"CDAT")
		FIELD_INT	   (Dbar,"DBAR")
		FIELD_INT	   (Dear,"DEAR")
		FIELD_DATE	   (Debe,"DEBE")
		FIELD_DATE	   (Deen,"DEEN")
		FIELD_INT	   (Dedu,"DEDU")
		FIELD_CHAR_TRIM(Dety,"DETY")
		FIELD_DATE	   (Eadb,"EADB")
		FIELD_CHAR_TRIM(Hopo,"HOPO")
		FIELD_DATE	   (Lade,"LADE")
		FIELD_LONG	   (Ucon,"UCON")
		FIELD_DATE	   (Lstu,"LSTU")
		FIELD_CHAR_TRIM(Obty,"OBTY")
		FIELD_LONG	   (Ouri,"OURI")
		FIELD_LONG	   (Ouro,"OURO")
		FIELD_INT	   (Sdti,"SDTI")
		FIELD_INT	   (Suti,"SUTI")
		FIELD_INT	   (Tsdb,"TSDB")
		FIELD_INT	   (Tsde,"TSDE")
		FIELD_INT	   (Ttgf,"TTGF")
		FIELD_INT	   (Ttgt,"TTGT")
		FIELD_LONG	   (Urno,"URNO")
		FIELD_LONG	   (Urud,"URUD")
        FIELD_CHAR	   (Flgs,0,"FLGS")
    END_CEDARECINFO
    // Copy the record structure
    for (int i = 0; i < sizeof(DemDataRecInfo)/sizeof(DemDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&DemDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
	strcpy(pcmTableName,"DEM");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmListOfFields,"ALOC,CDAT,DBAR,DEAR,DEBE,DEEN,DEDU,DETY,EADB,"
						   "HOPO,LADE,UCON,LSTU,OBTY,OURI,OURO,SDTI,SUTI,TSDB,TSDE,"
						   "TTGF,TTGT,URNO,URUD,FLGS");

	 pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	 lmLfd = 0;
	lgNextDemUrno = 0;
    omFkeyMap.InitHashTable(629,117);
    //omData.SetSize(600);
	omData.RemoveAll();
	omDispoStartTime = TIMENULL;
	omDispoEndTime = TIMENULL;
	omLastBcNum = 0;
	omLastBcUrno = 0;
	pomAttachedTable = NULL;
	bmCanSave = true;
	imCalledCnt = 0;
	imStdSec = 0;
	imCnvSec = 0;
	this->omLoadFormat.Empty();
}

CedaDemData::~CedaDemData()
{
	ogDdx.UnRegister(this,NOTUSED);
	if(imCalledCnt>0)
	{
		TRACE("Destroying CedaDemData.\nTotal STD find time=%d   Total CNV find time=%d Called %d times\n",imStdSec,imCnvSec,imCalledCnt);
	}
	imCalledCnt=0;
	imStdSec =0;
	imCnvSec =0;
	ClearAll();
}	
void CedaDemData::Register()
{
	int inf = (int)BC_CFL_READY;
	ogDdx.Register(this, BC_CFL_READY,	CString("BC_CFL_READY"),	CString("Collect Flights"),	ProcessDemCf);
	ogDdx.Register(this, BC_DEM_NEW,	CString("BC_DEM_NEW"),		CString("New Demand"),		ProcessDemCf);
	ogDdx.Register(this, BC_DEM_CHANGE, CString("BC_DEM_CHANGE"),	CString("Changed demand"),	ProcessDemCf);
	ogDdx.Register(this, BC_DEM_DELETE, CString("BC_DEM_DELETE"),	CString("Deleted Demand"),	ProcessDemCf);
	ogDdx.Register(this, BC_RELDEM,		CString("BC_DEM_RELEASE"),	CString("Demand Release"),	ProcessDemCf);		// Release of demand
	ogDdx.Register(this, BC_UPDDEM,		CString("BC_DEM_UPDDEM"),	CString("Demand Upd/Del"),	ProcessDemCf);		// Release of demand
}

void CedaDemData::Unregister()
{
	ogDdx.UnRegister(this,NOTUSED);
}

void CedaDemData::AttachCedaFlightData(CedaFlightData *popCedaFlightData)
{
	pomCedaFlightData = popCedaFlightData;
}


void CedaDemData::SetDispoTimeFrame(CTime opStartTime, CTime opEndTime)
{
	omDispoStartTime = opStartTime;
	omDispoEndTime = opEndTime;
}

void CedaDemData::SetRuleGroupNames(CString opRuleGrp)
{
	omRuleGrpName = opRuleGrp;
}

bool CedaDemData::ReadSpecialData(CCSPtrArray<DEMDATA> *popDem, char *pspWhere, char *pspFieldList, bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[2048] = " ";
	if(strlen(pspFieldList) > 0)
	{
		if( pspFieldList[0] == '*' )
		{
			strcpy(pclFieldList, pcmFieldList);
		}
		else
		{
			strcpy(pclFieldList, pspFieldList);
		}
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popDem != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			DEMDATA *prpDem = new DEMDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpDem,CString(pclFieldList))) == true)
			{
				popDem->Add(prpDem);
			}
			else
			{
				delete prpDem;
			}
		}
		if(popDem->GetSize() == 0) return false;
	}
	else
	{
		// Add records to omData
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			DEMDATA *prpDem = new DEMDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpDem,CString(pclFieldList))) == true)
			{
				if (!AddDemInternal(prpDem))
					delete prpDem;
			}
			else
			{
				delete prpDem;
			}
		}
	}
    return true;
}


bool CedaDemData::Read(CTime lpStartTime,CTime lpEndTime,CString opLoadedTplUrnos,bool bpWithClear)
{
	CString olWhere;

	CTime olStartMinusOffset = lpStartTime - CTimeSpan(2,0,0,0);

	bool blUseRudHint = false;
	if (this->omLoadFormat == "DEM")
	{
		olWhere.Format("WHERE DEBE BETWEEN '%s' AND '%s' AND DEEN > '%s' AND UTPL IN (%s)",
							olStartMinusOffset.Format("%Y%m%d%H%M00"),lpEndTime.Format("%Y%m%d%H%M00"), lpStartTime.Format("%Y%m%d%H%M00"),opLoadedTplUrnos);
	}
	else if (this->omLoadFormat == "RUD")
	{
		olWhere.Format("WHERE ((DEBE BETWEEN '%s' AND '%s' AND DEEN > '%s') AND (URUD IN (SELECT URNO FROM RUDTAB WHERE UTPL IN (%s))))",
							olStartMinusOffset.Format("%Y%m%d%H%M00"),lpEndTime.Format("%Y%m%d%H%M00"), lpStartTime.Format("%Y%m%d%H%M00"),opLoadedTplUrnos);

		blUseRudHint = true;
	}
	else
	{
		olWhere.Format("WHERE ((DEBE BETWEEN '%s' AND '%s' AND DEEN > '%s') AND (URUD IN (SELECT URNO FROM RUDTAB WHERE URUE IN (SELECT URNO FROM RUETAB WHERE UTPL IN (%s)))))",
							olStartMinusOffset.Format("%Y%m%d%H%M00"),lpEndTime.Format("%Y%m%d%H%M00"), lpStartTime.Format("%Y%m%d%H%M00"),opLoadedTplUrnos);

	}

	return this->Read(olWhere.GetBuffer(0),bpWithClear,blUseRudHint);

}


bool CedaDemData::Read(char *pspWhere /*NULL*/, bool bpWithClear /*true*/,bool bpUseRudHint /* false */)
{
    // Load data from CedaData into the dynamic array of record

	imRudUghs = ogBCD.GetFieldIndex("RUD", "UGHS");
	imRudDisp = ogBCD.GetFieldIndex("RUD", "DISP");
	imSerSnam = ogBCD.GetFieldIndex("SER", "SNAM");
	imRudRety = ogBCD.GetFieldIndex("RUD", "RETY");
	imRudUrue = ogBCD.GetFieldIndex("RUD", "URUE");
	imRueUtpl = ogBCD.GetFieldIndex("RUE", "UTPL");
	imRueRusn = ogBCD.GetFieldIndex("RUE", "RUSN");
	imRueRust = ogBCD.GetFieldIndex("RUE", "RUST");
	imTplTnam = ogBCD.GetFieldIndex("TPL", "TNAM");
  
	bool ilRc = true;
	char sDay[3]="";
	long Count = 0;

	CTime omTime;
	if (bpWithClear == true)
	{
		ClearAll();
	}

    // Select data from the database
	if (pspWhere == NULL)
	{	
		if (CedaAction2("RT", "") == false)
		{
			return false;
		}
	}
	else
	{
		if (bpUseRudHint)
		{
			if (CedaAction2("RT", pspWhere,NULL,pcgDataBuf,"BUF1",false,0,true,NULL,NULL,"/*+ INDEX(RUDTAB RUDTAB_UTPL) */") == false)
			{
				return false;
			}
		}
		else
		{
			if (CedaAction2("RT", pspWhere) == false)
			{
				return false;
			}
		}
	}



	int ilUndoCnt = 0;
    for (int ilLc = 0; ilRc == true; ilLc++)
    {
		DEMDATA *prlDem = new DEMDATA;
		if ((ilRc = GetBufferRecord2(ilLc,prlDem)) == true)
		{
			prlDem->IsChanged = DATA_UNCHANGED;
		
			DEMDATA *prlOldDem = GetDemByUrno(prlDem->Urno);
			if (prlOldDem == NULL)
			{
				if (!AddDemInternal(prlDem))
					delete prlDem;
				else
					lmLfd++;
			}
			else
			{
				if (HasExpired(prlDem))
				{
					prlDem->IsChanged = DATA_DELETED;
					*prlOldDem = *prlDem;
					omDeletedUrnoMap.SetAt((void *)prlOldDem->Urno,prlOldDem);
					ilUndoCnt++;
					delete prlDem;
				}
				else
				{
					prlDem->IsChanged = DATA_CHANGED;
					*prlOldDem = *prlDem;
					UpdateDem(prlDem,FALSE);
					ilUndoCnt++;
					delete prlDem;
				}
			}
		}
		else
		{
			delete prlDem;
		}
	}

	lgNextDemUrno += 100;

	return RecalculateFlightsFromCCIDemands();

}

bool CedaDemData::ClearAll()
{
	omBreakMap.RemoveAll();
    omUrnoMap.RemoveAll();
	omDeletedUrnoMap.RemoveAll();
	omSingleFctcMap.RemoveAll();
	omSinglePerMap.RemoveAll();
	omSingleCicMap.RemoveAll();
	omSinglePstMap.RemoveAll();
	omSingleGatMap.RemoveAll();
	omSingleRegMap.RemoveAll();
	omSingleReqMap.RemoveAll();
	omTemplateNameMap.RemoveAll();
	omGhsuMap.RemoveAll();
	omRustMap.RemoveAll();
	omRetyMap.RemoveAll();
	omRusnMap.RemoveAll();
	omServiceNameMap.RemoveAll();
	omDispMap.RemoveAll ();

	
	omSelectedData.RemoveAll();
	omRedrawDuties.RemoveAll();
    omData.DeleteAll();
	int ilSize = omSingleFctcArray.GetSize();
	omSingleFctcArray.DeleteAll();
	omSinglePerArray.DeleteAll();
	omSingleCicArray.DeleteAll();
	omSinglePstArray.DeleteAll();
	omSingleGatArray.DeleteAll();
	omSingleRegArray.DeleteAll();
	omSingleReqArray.DeleteAll();
	omTemplateNameArray.DeleteAll();
	omServiceNameArray.DeleteAll();
	omRusnArray.DeleteAll();
	omRustArray.DeleteAll();
	omRetyArray.DeleteAll();
	omGhsuArray.DeleteAll();

	void *polKey;
	CStringArray *polValue = NULL;

	// release CCI map
	for (POSITION pos = omCCIMap.GetStartPosition(); pos != NULL; )
	{
		omCCIMap.GetNextAssoc(pos,polKey,(void *&)polValue);
		delete polValue;
	}

	omCCIMap.RemoveAll();

    return true;
}


bool CedaDemData::AddDemInternal(DEMDATA *prpDem)
{
	PrepareDemData(prpDem);
	if (!HasExpired(prpDem))
	{
		omData.Add(prpDem);
		omUrnoMap.SetAt((void *)prpDem->Urno,prpDem);
		prpDem->Lstu = CTime::GetCurrentTime();
		return true;
	}
	else
		return false;
}

// Prepare some staff data, not read from database
void CedaDemData::PrepareDemData(DEMDATA *prpDem)
{

	CString olRudUrno;
	olRudUrno.Format("%ld",prpDem->Urud);
	CString olRueUrno;
	CString olGhsUrno;
	CString olTplUrno;
	CString *polValue = NULL;
	bool blRudIsSet = false;

	int  ilRecordCount = ogBCD.GetDataCount("RUD");

	
	CString olValue = "";

	blRudIsSet = (omRetyMap.Lookup((void *)prpDem->Urud,(void *&)polValue)  == TRUE);

	CString olTmp1;
	CString olTmp2;
	CString olSerName;

	prpDem->DispDebe = prpDem->Debe;
	prpDem->DispDeen = prpDem->Deen;
	if(ogBasicData.GetCalcToLocal())
	{
		ogBasicData.UtcToLocal(prpDem->DispDebe);
		ogBasicData.UtcToLocal(prpDem->DispDeen);
	}

	if(ilRecordCount > 0)
	{
		if(!blRudIsSet)
		{
			RecordSet olRudRecord(ogBCD.GetFieldCount("RUD"));
			RecordSet olSerRecord(ogBCD.GetFieldCount("SER"));
			RecordSet olRueRecord(ogBCD.GetFieldCount("RUE"));
			RecordSet olTplRecord(ogBCD.GetFieldCount("TPL"));

			ogBCD.GetRecord("RUD","URNO", olRudUrno,olRudRecord);

			olGhsUrno = olRudRecord.Values[imRudUghs];

			if(!olGhsUrno.IsEmpty())
			{
				prpDem->Ghsu = atol(olGhsUrno);

				int ilPos = omGhsuArray.GetSize();
				omGhsuArray.NewAt(ilPos,olGhsUrno);
				omGhsuMap.SetAt((void *)prpDem->Urud,&omGhsuArray[ilPos]);

				ogBCD.GetRecord("SER","URNO", olGhsUrno,olSerRecord);

				olSerName = olSerRecord.Values[imSerSnam];

				ilPos = omServiceNameArray.GetSize();
				omServiceNameArray.NewAt(ilPos,olSerName);
				omServiceNameMap.SetAt((void *)prpDem->Urud,&omServiceNameArray[ilPos]);

			}
			omDispMap.SetAt ( (void *)prpDem->Urud, atoi(olRudRecord.Values[imRudDisp]) );

			olRueUrno = olRudRecord.Values[imRudUrue];

			if(!olRueUrno.IsEmpty())
			{
				ogBCD.GetRecord("RUE","URNO", olRueUrno,olRueRecord);

				olTplUrno = olRueRecord.Values[imRueUtpl];

				if(!olTplUrno.IsEmpty())
				{
					CString olTplName = "";
					ogBCD.GetRecord("TPL","URNO", olTplUrno,olTplRecord);
		
					olTplName = olTplRecord.Values[imTplTnam];

					if(!olTplName.IsEmpty())
					{
						int ilPos = omTemplateNameArray.GetSize();
						omTemplateNameArray.NewAt(ilPos,olTplName);
						omTemplateNameMap.SetAt((void *)prpDem->Urud,&omTemplateNameArray[ilPos]);

					}
				}
				CString olRusn = "";
				olRusn = olRueRecord.Values[imRueRusn];

				int ilPos = omRusnArray.GetSize();
				omRusnArray.NewAt(ilPos,olRusn);
				omRusnMap.SetAt((void *)prpDem->Urud,&omRusnArray[ilPos]);

				if(!olRusn.IsEmpty())
				{
					strcpy(prpDem->Rusn,(LPCSTR)olRusn);
				}


				CString olRust = "";
				olRust = olRueRecord.Values[imRueRust];
				ilPos = omRustArray.GetSize();
				omRustArray.NewAt(ilPos,olRust);
				omRustMap.SetAt((void *)prpDem->Urud,&omRustArray[ilPos]);

				if(!olRust.IsEmpty())
				{
					prpDem->Rust = atoi(olRust);
				}

			}
			
			CString olRety = "";
			olRety = olRudRecord.Values[imRudRety];

			int ilPos = omRetyArray.GetSize();
			omRetyArray.NewAt(ilPos,olRety);
			omRetyMap.SetAt((void *)prpDem->Urud,&omRetyArray[ilPos]);

			if(olRety.GetLength() > 0)
			{
				if(olRety.GetAt(0) == '1')
				{
					prpDem->IsPersonal = true;
					CString olPfcValues = ogBCD.GetValueList("RPF", "URUD", olRudUrno, "UPFC") ;
					CString olPerValues = ogBCD.GetValueList("RPQ", "URUD", olRudUrno, "UPER");

					int ilPos = omSingleFctcArray.GetSize();
					if(!olPfcValues.IsEmpty())
					{
						olPfcValues += ",";
						prpDem->HasPfc = true;
						omSingleFctcArray.NewAt(ilPos,olPfcValues);
						omSingleFctcMap.SetAt((void *)prpDem->Urud,&omSingleFctcArray[ilPos]);
					}

					if(!olPerValues.IsEmpty())
					{
						olPerValues  +=",";
						prpDem->HasPer = true;
						ilPos = omSinglePerArray.GetSize();
						omSinglePerArray.NewAt(ilPos,olPerValues);
						omSinglePerMap.SetAt((void *)prpDem->Urud,&omSinglePerArray[ilPos]);
					}
				}
			}

			if(olRety.GetLength() > 1)
			{
				if(olRety.GetAt(1) == '1')
				{
					prpDem->IsEquipment = true;
					CString olPerValues = ogBCD.GetValueList("REQ", "URUD", olRudUrno, "UEQU") ;

					if(!olPerValues.IsEmpty())
					{
						olPerValues += ",";
						prpDem->HasReq = true;

						ilPos = omSingleReqArray.GetSize();
						omSingleReqArray.NewAt(ilPos,olPerValues);
						omSingleReqMap.SetAt((void *)prpDem->Urud,&omSingleReqArray[ilPos]);
					}
				}
			}
			if(olRety.GetLength() > 2)
			{
				if(olRety.GetAt(2) == '1')
				{
					prpDem->IsLocation = true;

					CString olReft = "";
					CString olTmp = "";
					CString olGateValues = ""; 
					CString olPosValues = ""; 
					CString olCicValues = ""; 
					int ilRecordCount3 = ogBCD.GetDataCount("RLO");
					for(int i = 0; i < ilRecordCount3; i++)
					{
						olReft = ogBCD.GetField("RLO", i, "REFT");
						if(olReft.Find("PST.") > -1)
						{
							if(CString(prpDem->Urud) == ogBCD.GetField("RLO", i, "URUD"))
							{
								olTmp = ogBCD.GetField("RLO", i, "RLOC") ;
								olPosValues += olTmp;
							}
						}
						if(olReft.Find("GAT.") > -1)
						{
							if(CString(prpDem->Urud) == ogBCD.GetField("RLO", i, "URUD"))
							{
								olTmp = ogBCD.GetField("RLO", i, "RLOC") ;
								olGateValues += olTmp;
							}
						}
						if(olReft.Find("CIC.") > -1)
						{
							CString olTest = ogBCD.GetField("RLO", i, "URUD");
							CString olUrud;
							olUrud.Format("%ld", prpDem->Urud);
							if(olUrud == ogBCD.GetField("RLO", i, "URUD"))
							{
								olTmp = ogBCD.GetField("RLO", i, "RLOC") ;
								olCicValues += olTmp;
							}
						}
					}

					int ilPos = omSingleCicArray.GetSize();
					if(!olCicValues.IsEmpty())
					{
						olCicValues += ",";
						prpDem->HasCic = true;
						omSingleCicArray.NewAt(ilPos,olCicValues);
						omSingleCicMap.SetAt((void *)prpDem->Urud,&omSingleCicArray[ilPos]);
					}
					if(!olPosValues.IsEmpty())
					{
						olPosValues += ",";
						prpDem->HasPos = true;
						ilPos = omSinglePstArray.GetSize();
						omSinglePstArray.NewAt(ilPos,olPosValues);
						omSinglePstMap.SetAt((void *)prpDem->Urud,&omSinglePstArray[ilPos]);
					}
					if(!olGateValues.IsEmpty())
					{
						olGateValues += ",";
						prpDem->HasGat = true;

						ilPos = omSingleGatArray.GetSize();
						omSingleGatArray.NewAt(ilPos,olGateValues);
						omSingleGatMap.SetAt((void *)prpDem->Urud,&omSingleGatArray[ilPos]);
					}							
				}
			}
		}
		else
		{
			CString olRusn = GetRusn(prpDem->Urud);
			if(!olRusn.IsEmpty())
			{
				strcpy(prpDem->Rusn,(LPCSTR)olRusn);
			}
			CString olGhsUrno = GetGhsu(prpDem->Urud);

			if(!olGhsUrno.IsEmpty())
			{
				prpDem->Ghsu = atol(olGhsUrno);
			}

			CString olRust = GetRust(prpDem->Urud);

			if(!olRust.IsEmpty())
			{
				prpDem->Rust = atoi(olRust);
			}

			CString olRety = GetRety(prpDem->Urud);

			if(olRety.GetLength() > 0)
			{
				if(olRety.GetAt(0) == '1')
				{
					CString olPfcValues = GetPfcSingeUrnos(prpDem->Urud);
					prpDem->IsPersonal = true;
					if(!olPfcValues.IsEmpty())
					{
						prpDem->HasPfc = true;
					}

					CString olPerValues = GetPerSingeUrnos(prpDem->Urud);
					if(!olPerValues.IsEmpty())
					{
						prpDem->HasPer = true;
					}
				}
			}

			if(olRety.GetLength() > 1)
			{
				if(olRety.GetAt(1) == '1')
				{
					CString olPfcValues = GetRegSingeUrnos(prpDem->Urud);
					prpDem->IsEquipment = true;
					if(!olPfcValues.IsEmpty())
					{
						prpDem->HasReg = true;
					}

					CString olPerValues = GetReqSingeUrnos(prpDem->Urud);

					if(!olPerValues.IsEmpty())
					{
						prpDem->HasReq = true;
					}
				}
			}
			if(olRety.GetLength() > 2)
			{
				if(olRety.GetAt(2) == '1')
				{
					prpDem->IsLocation = true;
					CString olCicValues = GetCicSingeUrnos(prpDem->Urud);

					if(!olCicValues.IsEmpty())
					{
						prpDem->HasCic = true;
					}
					CString olPosValues = GetPstSingeUrnos(prpDem->Urud);

					if(!olPosValues.IsEmpty())
					{
						prpDem->HasPos = true;
					}
					CString olGateValues = GetGatSingeUrnos(prpDem->Urud);

					if(!olGateValues.IsEmpty())
					{
						prpDem->HasGat = true;
					}							
				}
			}
		}


	}
	prpDem->MinuteStart = 0;

}



/////////////////////////////////////////////////////////////////////////////
// Update data methods (called from PrePlanTable class)

bool CedaDemData::DeleteDem(DEMDATA *prpDemData, BOOL bpWithSave)
{
	CCS_TRY
		bool olRc = true;
	ogDdx.DataChanged((void *)this,DEM_DELETE,(void *)prpDemData);
	if(prpDemData->IsChanged == DATA_UNCHANGED)
	{
		prpDemData->IsChanged = DATA_DELETED;
	}
	if(bpWithSave == TRUE)
	{
		CWaitCursor olWait;
		olRc = SaveDem(prpDemData);
	}

	return olRc;
	CCS_CATCH_ALL
		return false;
}

bool CedaDemData::InsertDem(DEMDATA *prpDemData, BOOL bpWithSave)
{
	CCS_TRY
	bool olRc = true;
	ogDdx.DataChanged((void *)this,/*DEM_NEW*/DEM_CHANGE,(void *)prpDemData);
	//prpDemData->IsChanged = DATA_NEW;
	if(prpDemData->Cdat==TIMENULL)
	{
		prpDemData->Cdat = CTime::GetCurrentTime();
		prpDemData->Lstu = CTime::GetCurrentTime();
	}
	if(prpDemData->IsChanged == DATA_UNCHANGED)
	{
		prpDemData->IsChanged = DATA_CHANGED;
	}
	if(bpWithSave == TRUE)
	{
		CWaitCursor olWait;
		CCSPtrArray<DEMDATA> olToSaveList;
		CCSPtrArray<DEMDATA> olDemList;
		
 		olToSaveList.Add(prpDemData);
		
		Release(&olToSaveList, CString("F"));
		olDemList.DeleteAll();
	}

	return olRc;
	CCS_CATCH_ALL
	return false;
}

bool CedaDemData::UpdateDem(DEMDATA *prpDemData, BOOL bpWithSave)
{
	CCS_TRY
	bool olRc = true;
	ogDdx.DataChanged((void *)this,DEM_CHANGE,(void *)prpDemData);
	
	// Test - gsa
	DEMDATA *prlDem = GetDemByUrno(prpDemData->Urno);
	
	if(prpDemData->IsChanged != DATA_NEW)
	{
		prpDemData->IsChanged = DATA_CHANGED;
	}
	if(bpWithSave == TRUE)
	{
		prpDemData->Lstu = CTime::GetCurrentTime();
		CCSPtrArray<DEMDATA> olToSaveList;
		CWaitCursor olWait;
		olToSaveList.NewAt(0,*prpDemData);

		Release(&olToSaveList, CString("S"));
		olToSaveList.DeleteAll();

	}


	return olRc;

	CCS_CATCH_ALL
	return false;
}


bool CedaDemData::DemExist(long Urno)
{
	// don't read from database anymore, just check internal array
	CCS_TRY
		DEMDATA *prlData; 
	bool olRc = true;

	if(omUrnoMap.Lookup((void *)Urno,(void *&)prlData)  == TRUE)
	{
		;//*prpData = prlData;
	}
	else
	{
		olRc = false;
	}
	return olRc;
	CCS_CATCH_ALL
		return false;
}



// the callback function for storing broadcasted fligthdata in FLIGHTDATA array

void  ProcessDemCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{ 
	BC_TRY
		CedaDemData *polDemData = (CedaDemData*)vpInstance;
		if (polDemData == NULL)
			return;

		ogBackTrace += "1,";

		switch(ipDDXType)
		{
		case BC_RELDEM :
			polDemData->ProcessDemRelease(vpDataPointer);
		break;
		case BC_UPDDEM :
			polDemData->ProcessDemUpdDem(vpDataPointer);
		break;
		default :
			polDemData->ProcessDemBc(ipDDXType,vpDataPointer,ropInstanceName);
		}

		ogBackTrace += "1e";

	BC_CATCH_ALL
}

bool CedaDemData::ProcessDemRelease(void *vpDataPointer)
{
	CCS_TRY

	struct BcStruct *prlBcStruct = NULL;
	prlBcStruct = (struct BcStruct *) vpDataPointer;

	if (prlBcStruct != NULL)
	{
		CWaitCursor olWait;
		// check if from myself
		char pclWks[200]="";
		CString olWks = prlBcStruct->ReqId;
		::GetWorkstationName(pclWks);
		CString olWks2 = pclWks;

		if(olWks == olWks2)
		{
			--bgReleaseDemCount;
			if (bgReleaseDemCount > 0)
				return false;
		}

		CString	olDetys = prlBcStruct->Selection;
		CString olData  = prlBcStruct->Data;
		CStringArray olDataArray;
		if(ExtractItemList(olData,&olDataArray, '-') != 2)
		{
			return false;
		}

		CTime olStart	= DBStringToDateTime(olDataArray[0]);
		CTime olEnd		= DBStringToDateTime(olDataArray[1]);

		olDataArray.RemoveAll();
		if (ExtractItemList(olData,&olDataArray, '|') != 2)
		{
			return false;
		}

		CStringArray olTplArray;
		ogBasicData.MakeClientString(olDataArray[1]);
		if (ExtractItemList(olDataArray[1],&olTplArray,',') <= 0)
		{
			return false;
		}

		// check, if we must do anything
		CStringArray olTplOverlap;
		if (IsOverlapped(olStart,olEnd,omDispoStartTime,omDispoEndTime))
		{
			for (int i = 0; i < ogBasicData.omValidTemplates.GetSize(); i++)
			{
				for (int j = 0; j < olTplArray.GetSize(); j++)
				{
					if (ogBasicData.omValidTemplates[i] == olTplArray[j])
					{
						olTplOverlap.Add(olTplArray[j]);
					}
				}
			}

			if (olTplOverlap.GetSize() > 0)
			{
				CString olValidTpls = "";
				for (int ilTpl = 0; ilTpl < ogBasicData.omValidTemplates.GetSize();ilTpl++)
				{
					if(ilTpl > 0)
					{
						olValidTpls += (",'" + ogBasicData.omValidTemplates.GetAt(ilTpl)+ "'");
					}
					else
					{
						olValidTpls = "'" + ogBasicData.omValidTemplates.GetAt(ilTpl) + "'";
					}
				}

				char pclWhere[2048];

				//MWO ???
				//CTime olTmpStartTime = omDispoStartTime - CTimeSpan(7,0,0,0);	// prior one week
				CTime olTmpStartTime = omDispoStartTime - CTimeSpan(1,0,0,0);	// prior one day
				//END MWO
				sprintf(pclWhere,"WHERE ((DEBE BETWEEN '%s' AND '%s' AND DEEN > '%s') AND (URUD IN (SELECT URNO FROM RUDTAB WHERE URUE IN (SELECT URNO FROM RUETAB WHERE UTPL IN (%s)))))",
									olTmpStartTime.Format("%Y%m%d%H%M00"),omDispoEndTime.Format("%Y%m%d%H%M00"), omDispoStartTime.Format("%Y%m%d%H%M00"),olValidTpls);

				//MWO: 27.03.03
				ogBcHandle.SetFilterRange("DEMTAB", "DEBE", "DEEN", olTmpStartTime.Format("%Y%m%d%H%M00"), omDispoEndTime.Format("%Y%m%d%H%M00"));
				//END MWO: 27.03.03
				Read(pclWhere);

				ogDdx.DataChanged((void *)this, RELDEM,vpDataPointer); //Update Viewer

			}

		}
					
	}
	CCS_CATCH_ALL

	return true;
}

void CedaDemData::ProcessDemandInsert(char *pcpFields, char *pcpData)
{
	DEMDATA *prlDemand = new DEMDATA;
	GetRecordFromItemList(prlDemand, pcpFields, pcpData);

	RecordSet olRudRecord(ogBCD.GetFieldCount("RUD"));
	CString olRudUrno;
	olRudUrno.Format("%ld",prlDemand->Urud);
	if ((prlDemand->Debe < omDispoEndTime && prlDemand->Deen > omDispoStartTime) && ogBCD.GetRecord("RUD","URNO", olRudUrno,olRudRecord))
	{
		if (!AddDemInternal(prlDemand))
		{
			delete prlDemand;
		}
	}
	else
	{
		delete prlDemand;
	}
}

bool CedaDemData::ProcessDemandUpdate(long lpDemandUrno, char *pcpFields, char *pcpData)
{
	bool blFound = false;


	DEMDATA *prlOldDem = GetDemByUrno(lpDemandUrno);
	if (prlOldDem != NULL)
	{
		DEMDATA *prlDem = new DEMDATA;
		GetRecordFromItemList(prlDem, pcpFields, pcpData);

		RecordSet olRudRecord(ogBCD.GetFieldCount("RUD"));
		CString olRudUrno;
		olRudUrno.Format("%ld",prlDem->Urud);
		if ((prlDem->Debe < omDispoEndTime && prlDem->Deen > omDispoStartTime) && ogBCD.GetRecord("RUD","URNO", olRudUrno,olRudRecord))
		{
			PrepareDemData(prlDem);
			blFound = true;
			if (HasExpired(prlDem))
			{
				prlDem->IsChanged = DATA_DELETED;
				*prlOldDem = *prlDem;
				omDeletedUrnoMap.SetAt((void *)prlOldDem->Urno,prlOldDem);
				delete prlDem;
			}
			else
			{
				prlDem->IsChanged = DATA_CHANGED;
				*prlOldDem = *prlDem;
				delete prlDem;
			}
		}
		else
		{
			prlDem->IsChanged = DATA_DELETED;
			*prlOldDem = *prlDem;
			omDeletedUrnoMap.SetAt((void *)prlOldDem->Urno,prlOldDem);
			delete prlDem;
		}
	}
	else
	{
		ProcessDemandInsert(pcpFields,pcpData);
	}

	return blFound;
}

void CedaDemData::ProcessDemandDelete(long llDemUrno)
{
	DEMDATA *prlDem = GetDemByUrno(llDemUrno);
	if (prlDem != NULL)
	{
		prlDem->IsChanged = DATA_DELETED;
		omDeletedUrnoMap.SetAt((void *)prlDem->Urno,prlDem);
	}
}

bool CedaDemData::ProcessAttachment(CString &ropAttachment)
{
	bool blSuccess = false;
	if (!ropAttachment.IsEmpty())
	{
		CAttachment olAttachment(ropAttachment);
		if (olAttachment.bmAttachmentValid)
		{
			blSuccess = true;
			for(int ilI = 0; ilI < olAttachment.omInsertDataList.GetSize(); ilI++)
			{
				ProcessDemandInsert(olAttachment.omFieldList.GetBuffer(0), olAttachment.omInsertDataList[ilI].GetBuffer(0));
			}

			for(int ilU = 0; ilU < olAttachment.omUpdateDataList.GetSize(); ilU++)
			{
				DEMDATA rlDemand;
				GetRecordFromItemList(&omRecInfo,&rlDemand, olAttachment.omFieldList.GetBuffer(0), olAttachment.omUpdateDataList[ilU].GetBuffer(0));
				if(!ProcessDemandUpdate(rlDemand.Urno, olAttachment.omFieldList.GetBuffer(0), olAttachment.omUpdateDataList[ilU].GetBuffer(0)))
					ProcessDemandInsert(olAttachment.omFieldList.GetBuffer(0), olAttachment.omUpdateDataList[ilU].GetBuffer(0));
			}

			for(int ilD = 0; ilD < olAttachment.omDeleteDataList.GetSize(); ilD++)
			{
				CString olUrno = olAttachment.omDeleteDataList[ilD];
				if (olUrno.GetLength() > 0)
				{
					ProcessDemandDelete(atol(olUrno));
				}
			}

		}
	}
	return blSuccess;
}

bool CedaDemData::ProcessDemUpdDem(void *vpDataPointer)
{
	CCS_TRY

	struct BcStruct *prlBcStruct = (struct BcStruct *) vpDataPointer;

	if (prlBcStruct != NULL)
	{
		CWaitCursor olWait;

		CString olData  = prlBcStruct->Data;
		CStringArray olDataArray;
		if (ExtractItemList(olData,&olDataArray) < 4)
			return false;

		CTime olStart	= DBStringToDateTime(olDataArray[0]);
		CTime olEnd		= DBStringToDateTime(olDataArray[1]);

		// check, if we must do anything
		if (!IsOverlapped(olStart,olEnd,omDispoStartTime,omDispoEndTime))
			return true;

		if (ProcessAttachment(prlBcStruct->Attachment)) // TCP/IP broadcast - contains attachment with data
		{
			this->ReleaseDeletedData();
			
			ogDdx.DataChanged((void *)this, UPDDEM,vpDataPointer); //Update Viewer
			return true;
		}

		int ilDelCount = 0;
		if (sscanf(olDataArray[2],"DEL:%d",&ilDelCount) <= 0)
			return false;

		CStringArray olDelList;
		for (int i = 0; i < ilDelCount; i++)
		{
			olDelList.Add(olDataArray[3+i]);
		}
		
		int ilUpdCount = 0;
		if (sscanf(olDataArray[3+ilDelCount],"UPD:%d",&ilUpdCount) <= 0)
			return false;

		CStringArray olUpdList;
		for (i = 0; i < ilUpdCount; i++)
		{
			olUpdList.Add(olDataArray[3+ilDelCount + 1 + i]);
		}
		

		// remove all 'deleted' demands from internal storage
		for (i = 0; i < ilDelCount; i++)
		{
			DEMDATA *prlDem = this->GetDemByUrno(atol(olDelList[i]));
			if (prlDem != NULL)
			{
				prlDem->IsChanged = DATA_DELETED;
				omDeletedUrnoMap.SetAt((void *)prlDem->Urno,prlDem);

			}
		}

		if (ilUpdCount == 0)
		{
			this->ReleaseDeletedData();
			ogDdx.DataChanged((void *)this, UPDDEM,vpDataPointer); //Update Viewer
			return true;
		}

		// remove all 'updated' demands from internal storage
		for (i = 0; i < ilUpdCount; i++)
		{
			DEMDATA *prlDem = this->GetDemByUrno(atol(olUpdList[i]));
			if (prlDem != NULL)
			{
				prlDem->IsChanged = DATA_DELETED;
				omDeletedUrnoMap.SetAt((void *)prlDem->Urno,prlDem);
			}
		}

		this->ReleaseDeletedData();

		// generate valid demand urno list
		CString olValidUrnos = "";
		for (i = 0; i < ilUpdCount; i++)
		{
			if (i > 0)
			{
				olValidUrnos += (",'" +olUpdList[i]+ "'");
			}
			else
			{
				olValidUrnos = "'" + olUpdList[i] + "'";
			}
		}

		// generate valid template list
		CString olValidTpls = "";
		for (int ilTpl = 0; ilTpl < ogBasicData.omValidTemplates.GetSize();ilTpl++)
		{
			if(ilTpl > 0)
			{
				olValidTpls += (",'" + ogBasicData.omValidTemplates.GetAt(ilTpl)+ "'");
			}
			else
			{
				olValidTpls = "'" + ogBasicData.omValidTemplates.GetAt(ilTpl) + "'";
			}
		}

		char pclWhere[2048];

		//MWO ???
		//CTime olTmpStartTime = omDispoStartTime - CTimeSpan(7,0,0,0);	// prior one week
		CTime olTmpStartTime = omDispoStartTime - CTimeSpan(1,0,0,0);	// prior one day
		//END MWO
		sprintf(pclWhere,"WHERE ((URNO IN (%s)) AND (DEBE BETWEEN '%s' AND '%s' AND DEEN > '%s') AND (URUD IN (SELECT URNO FROM RUDTAB WHERE URUE IN (SELECT URNO FROM RUETAB WHERE UTPL IN (%s)))))",
							olValidUrnos,olTmpStartTime.Format("%Y%m%d%H%M00"),omDispoEndTime.Format("%Y%m%d%H%M00"), omDispoStartTime.Format("%Y%m%d%H%M00"),olValidTpls);

		//MWO: 27.03.03
		ogBcHandle.SetFilterRange("DEMTAB", "DEBE", "DEEN", olTmpStartTime.Format("%Y%m%d%H%M00"), omDispoEndTime.Format("%Y%m%d%H%M00"));
		//END MWO: 27.03.03
		Read(pclWhere,false);

		ogDdx.DataChanged((void *)this, UPDDEM,vpDataPointer); //Update Viewer

	}

	CCS_CATCH_ALL

	return true;
}

void  CedaDemData::ProcessDemBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	CCS_TRY

		if(ipDDXType == BC_CFL_READY)
		{
			// confirm from demhdl that all demands have been created
			struct BcStruct *prlDemData;
			prlDemData = (struct BcStruct *) vpDataPointer; 
		
			char pclWks[200]="";
			CString olWks = prlDemData->ReqId;
			::GetWorkstationName(pclWks);
			CString olWks2 = pclWks;

			if(olWks == olWks2) // only interested in confirms intended for my own workstation
			{
				// this is set to 2 if flight dependent and flight independent demands are created
				bgCreateDemCount--;

				if(bgCreateDemCount == 0)
				{
					this->RecalculateFlightsFromCCIDemands();

					bgCreateDemIsActive = false;
					ogDdx.DataChanged((void *)this, DEM_CFL_READY,(void *)NULL ); //Update Viewer
				
					if (!bgRuleDiagnosticIsActive)
					{
						CCflReadyDlg olDlg;
						olDlg.DoModal();
					}
					else
					{
						CCflReadyWithRuleDiagnosticsDlg olDlg;
						olDlg.DoModal();
					}
				}
				else
				{
				}
			}
			else
			{
				this->RecalculateFlightsFromCCIDemands();
			}
		}
		else if (ipDDXType == BC_DEM_CHANGE || ipDDXType == BC_DEM_NEW )
		{
			DEMDATA *prpDem;
			struct BcStruct *prlDemData;

			prlDemData = (struct BcStruct *) vpDataPointer; 
			long llUrno;

			if (strstr(prlDemData->Selection,"WHERE") != NULL)
			{
				llUrno = GetUrnoFromSelection(prlDemData->Selection);
			}
			else
			{
				llUrno = atol(prlDemData->Selection);
			}
			if (omUrnoMap.Lookup((void *)llUrno,(void *& )prpDem) == TRUE)
			{
				GetRecordFromItemList(prpDem,
					prlDemData->Fields,prlDemData->Data);
				PrepareDemData(prpDem); 
				// check whether the demand has expired
				if (HasExpired(prpDem))
				{
					prpDem->IsChanged = DATA_DELETED;
					omDeletedUrnoMap.SetAt((void *)prpDem->Urno,prpDem);

					// send event only if no demand generation is running !
					if (bgCreateDemCount == 0)
					{
						ogDdx.DataChanged((void *)this, DEM_DELETE,prpDem); //Update Viewer
					}
				}
				else
				{
					// send event only if no demand generation is running !
					if (bgCreateDemCount == 0)
					{
						ogDdx.DataChanged((void *)this, DEM_CHANGE,prpDem ); //Update Viewer
					}

				}

			}
			else
			{
				RecordSet olRudRecord(ogBCD.GetFieldCount("RUD"));
				CString olRudUrno;
				prpDem = new DEMDATA;
				prpDem->IsChanged = DATA_UNCHANGED;
				GetRecordFromItemList(prpDem,
					prlDemData->Fields,prlDemData->Data);
				olRudUrno.Format("%ld",prpDem->Urud);
				if((prpDem->Debe < omDispoEndTime && prpDem->Deen > omDispoStartTime) 
						&& ogBCD.GetRecord("RUD","URNO", olRudUrno,olRudRecord))
				{
					if (!AddDemInternal(prpDem))
						delete prpDem;
					else
					{
						// send event only if no demand generation is running !
						if (bgCreateDemCount == 0)
						{
							ogDdx.DataChanged((void *)this, DEM_CHANGE,prpDem ); //Update Viewer
						}
					}
				}
				else
				{
					delete prpDem;
				}
			}
			
	
		}
		else if (ipDDXType == BC_DEM_DELETE)
		{
			struct BcStruct *prlDemData;
			bool blGhdDeleted = false;

			prlDemData = (struct BcStruct *) vpDataPointer;
			long llUrno;

			if (strstr(prlDemData->Selection,"WHERE") != NULL)
			{
				llUrno = GetUrnoFromSelection(prlDemData->Selection);
			}
			else
			{
				llUrno = atol(prlDemData->Selection);
			}
			CString olFkey;
			DEMDATA *prpDem;
			if (omUrnoMap.Lookup((void *)llUrno,(void *& )prpDem) == TRUE)
			{
				if(prpDem != NULL)
				{
					prpDem->IsChanged = DATA_DELETED;
					omDeletedUrnoMap.SetAt((void *)prpDem->Urno,prpDem);

					// send event only if no demand generation is running !
					if (bgCreateDemCount == 0)
					{
						ogDdx.DataChanged((void *)this, DEM_DELETE,prpDem); //Update Viewer
					}
				}
			}
		}


	CCS_CATCH_ALL
}
void CedaDemData::ProcessReloadDemsForFlights(CUIntArray &olFkeys, CString opRuleGrpName)
{
	CCS_TRY


	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------
// reload a single DEM and replaces it internal 
//-----------------------------------------------------------------------------
void CedaDemData::ProcessReloadSingleDem(long lpUrno)
{
	CCS_TRY

	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------
// Loads a single DEM and adds it to the internal datastruct
//-----------------------------------------------------------------------------
void CedaDemData::ProcessLoadSingleDem(long lpUrno)
{
	CCS_TRY


	CCS_CATCH_ALL
}
//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
void CedaDemData::ProcessReloadAllDems(CString opRuleGrpName)
{
	CCS_TRY

	CCS_CATCH_ALL
}

void CedaDemData::GetNoResDems(CCSPtrArray<DEMDATA> &ropDems)
{
	POSITION rlPos;
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		DEMDATA *prlDem;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDem);

		ropDems.Add(prlDem); //CListBox
	}
}

DEMDATA * CedaDemData::GetDemByKey(long pcpKey)
{

	return NULL;
}

DEMDATA * CedaDemData::GetDemByUrno(long pcpUrno)
{
	DEMDATA *prpDem;

	if(omUrnoMap.GetCount() > 0)
	{
		if (omUrnoMap.Lookup((void*)pcpUrno,(void *& )prpDem) == TRUE)
		{
			return prpDem;
		}
	}
	return NULL;
}



bool CedaDemData::GetDemsBySingleFlightUrno(CCSPtrArray<DEMDATA> &ropDems, long lpFlightUrno)
{
	
	DEMDATA  *prlDem;
	
	POSITION rlPos;
	for ( rlPos =  omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlDem);
		if((prlDem->Ouri == lpFlightUrno || prlDem->Ouro == lpFlightUrno) && lpFlightUrno != 0)
		{
			ropDems.NewAt(ropDems.GetSize(), *prlDem);
		}
	}
    return true;
}


bool CedaDemData::ChangeDemTime(long lpDemUrno,CTime opNewStart,CTime opNewEnd, bool bpWithSave)
{
	
    return true;
}


bool CedaDemData::DeleteDemInternal(DEMDATA *prpDem)
{

	int ilIndex = -1; /* Assume prpDem not found */
	long llUrno = prpDem->Urno;
	int ilDemCount = omData.GetSize();

	if (prpDem != NULL && ilDemCount > 0)
	{
		DEMDATA **prlFirstElem = (DEMDATA **)omData.GetData();
		// TEST CODE
		
		DEMDATA **prlDemLoc = std::find(prlFirstElem,prlFirstElem+ilDemCount,prpDem);
		if (prlDemLoc != prlFirstElem+ilDemCount)
		{
			ilIndex = prlDemLoc - prlFirstElem;	
			omData.DeleteAt(ilIndex);
		}
		else
		{
			DEMDATA *prlDem = GetDemByUrno(prpDem->Urno);
			if (prlDem != NULL)
			{
				DEMDATA **prlDemLoc = std::find(prlFirstElem,prlFirstElem+ilDemCount,prlDem);
				if (prlDemLoc != prlFirstElem+ilDemCount)
				{
					ilIndex = prlDemLoc - prlFirstElem;
					omData.DeleteAt(ilIndex);
				}
			}
		}
	}
	
	omUrnoMap.RemoveKey((void *)llUrno);

    return true;
}


bool CedaDemData::SaveDem(DEMDATA *prpDem)
{
	
	bool olRc = true;
	CString olListOfData;
	char pclSelection[512];
	char pclData[2048];

	if (prpDem->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpDem->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpDem);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpDem->IsChanged = DATA_UNCHANGED;
		if(!omLastErrorMessage.IsEmpty())
		{
			::MessageBox(NULL, omLastErrorMessage.GetBuffer(0), "DB-Zugriffsfehler-Insert", MB_OK);
		}
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpDem->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpDem);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpDem->IsChanged = DATA_UNCHANGED;
		if(!omLastErrorMessage.IsEmpty())
		{
			::MessageBox(NULL, omLastErrorMessage.GetBuffer(0), "DB-Zugriffsfehler-Update", MB_OK);
		}
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpDem->Urno);
		olRc = CedaAction("DRT",pclSelection);
		if(!omLastErrorMessage.IsEmpty())
		{
			::MessageBox(NULL, omLastErrorMessage.GetBuffer(0), "DB-Zugriffsfehler-Delete", MB_OK);
		}
//		if (olRc == true)
//		{
			//DeleteDemInternal(prpDem);
//		}
		break;
	}

   return olRc;
}

void CedaDemData::MakeDemSelected(DEMDATA *prpDem, BOOL bpSelect)
{
	/********************
	if(prpDem != NULL)
	{
		if(bpSelect == TRUE)
		{
			prpDem->IsSelected = TRUE;
			omSelectedData.Add(prpDem->Urno);
			//omSelectedData.Add(prpDem);
			ogDdx.DataChanged((void *)this,DEM_CHANGE,(void *)prpDem);
		}
		else
		{
			int ilCount = omSelectedData.GetSize();
			for(int k = 0; k < ilCount; k++)
			{
				if(omSelectedData[k] == (UINT)prpDem->Urno)
				//if(omSelectedData[k].Urno == prpDem->Urno)
				{
					prpDem->IsSelected = FALSE;
					ogDdx.DataChanged((void *)this,DEM_CHANGE,(void *)prpDem);
					omSelectedData.RemoveAt(k);
					k = ilCount;
				}
			}
		}
	}
	*********************/
}

int CedaDemData::GetSelectedDems(CCSPtrArray<DEMDATA> *prpDemData)
{
/***********
	prpDemData->RemoveAll();
	int ilCount = omSelectedData.GetSize();
	for(int k = 0; k < ilCount; k++)
	{
		DEMDATA *prlDem =  ogDemData.GetDemByUrno(omSelectedData[k]);
		if(prlDem!=NULL)
		{
			prpDemData->Add(prlDem);
		}
		else
		{
			omSelectedData.RemoveAt(k);
			k--;
			ilCount--;
		}
	}
	return ilCount;
	*****************/
	return 0;
}

void CedaDemData::DeselectAll()
{
/*****************	
	POSITION rlPos;
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		DEMDATA *prlDem = NULL;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDem);
		if(prlDem!=NULL && prlDem->IsSelected == TRUE)
		{
			prlDem->IsSelected = FALSE;
			ogDdx.DataChanged((void *)this,DEM_CHANGE,(void *)prlDem);
		}
	}

	int ilCount = omSelectedData.GetSize();
	for(int i = ilCount-1; i >= 0; i--)
	{
		DEMDATA *prlDem = GetDemByUrno(omSelectedData[i]);
		//DEMDATA *prlDem = GetDemByUrno(omSelectedData[i].Urno);
		if(prlDem != NULL)
		{
			if(prlDem->IsSelected == TRUE)
			{ 
				prlDem->IsSelected = FALSE;
				ogDdx.DataChanged((void *)this,DEM_CHANGE,(void *)prlDem);
			}
			omSelectedData.RemoveAt(i);
		}
	}
	****************/

}

bool CedaDemData::ChangeJobTime(long lpJobUrno,CTime opNewStart,CTime opNewEnd)
{
/*	JOBDATA *prlDispJob = GetJobByUrno(lpJobUrno);
	if (prlDispJob != NULL)
	{
		prlDispJob->Dube = opNewStart;
		prlDispJob->Duen = opNewEnd;
		//Now checking for tifa nad tifd
		FLIGHTDATA prlFA = pomCedaFlightData->GetFlightByUrno(prlDispJob->Fkey);
		if(prlFA != NULL)
		{
			FLIGHTDATA prlFD = pomCedaFlightData->GetJoinFlight(prlFA);
		}
		if(prlFlight != NULL)
		{
			CTime olTmpTime;
			olTmpTime = prlFlight->Tifd - CTimeSpan(prlDispJob->Drti*60) - CTimeSpan(atoi(prlDispJob->Ttdu)*60);

			if(prlDispJob->Dube > olTmpTime)
			{
				CTimeSpan olTS = prlDispJob->Dube - olTmpTime;
				int ilMinutes = olTS.GetTotalMinutes();
				prlDispJob->Prta = (ilMinutes * -1);
			}
			else
			{
				CTimeSpan olTS = olTmpTime - prlDispJob->Dube;
				int ilMinutes = olTS.GetTotalMinutes();
				prlDispJob->Prta = ilMinutes;
			}
			olTmpTime = prlFlight->Tifd + CTimeSpan(atoi(prlDispJob->Tfdu)*60);
			if(prlDispJob->Duen <= olTmpTime)
			{
				CTimeSpan olTS = olTmpTime - prlDispJob->Duen;
				int ilMinutes = olTS.GetTotalMinutes();
				prlDispJob->Pota = (ilMinutes * -1);
			}
			else
			{
				CTimeSpan olTS = prlDispJob->Duen - olTmpTime;
				int ilMinutes = olTS.GetTotalMinutes();
				prlDispJob->Pota = ilMinutes;
			}
		}

		//ogConflicts.CheckConflicts(*prlDispJob,FALSE,TRUE,TRUE);
		ogCCSDdx.DataChanged((void *)this,DEM_CHANGE,(void *)prlDispJob);
		if (prlDispJob->IsChanged == DATA_UNCHANGED)
		{
			prlDispJob->IsChanged = DATA_CHANGED;
		}
	}
*/
    return true;
}


//------------------------------------------------------------------------------
// This function has two different behaviors:
// 1. If the parameter popDemList is set and has data ==> only these Data are released
// 2. No Parameter ==> All DEM-Data are released

// Defaultparameter for opCmd = A
// A = Broadcast, that forces all workstations to reload all ghds
// S = Broadcast for updating/Deleting/Inserting a single ghd
// F = Broadcast for reloading all duties for the given flight-urnos

void CedaDemData::Release(CCSPtrArray<DEMDATA> *popDemList, CString opCmd, long lpUrnA, long lpUrnD)
{
	CString olIRT;
	CString olURT;
	CString olDRT;
	CString olFieldList;
	CString olDataList;
	CString olUpdateString;
	CString olInsertString;
	CString olDeleteString;
	

	long llNewUrno = ogBasicData.GetNextUrno();
	char pclBcUrno[20]="";
	sprintf(pclBcUrno, "%ld", llNewUrno);

	char pclWks[200]="";
	int ilNoUpdates = 0;
	int ilNoDeletes = 0;
	int ilNoInserts = 0;
	CStringArray olFields;
	int ilFields = ExtractItemList(CString(pcmListOfFields),&olFields); 
	olIRT.Format("*CMD*,%s,IRT,%d,", pcmTableName, ilFields);
	olURT.Format("*CMD*,%s,URT,%d,", pcmTableName, ilFields);
	olDRT.Format("*CMD*,%s,DRT,-1,", pcmTableName);

	CString olOrigFieldList;
	CString olEndl = CString("\n");
	int ilCurrentCount = 0;
	int ilTotalCount = 0;
	bool blRet = false;
	if(popDemList == NULL) //==> Dann alle Speichern
	{
		if(bmCanSave == true)
		{
			olOrigFieldList = CString(pcmListOfFields);

			olInsertString = olIRT + olOrigFieldList + olEndl;//CString(pcmListOfFields) + CString("\n");
			olUpdateString = olURT + olOrigFieldList + CString(",[URNO=:VURNO]")+ olEndl;//CString(pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
			olDeleteString = olDRT + CString(",[URNO=:VURNO]")+ olEndl;//CString("\n");

			blRet = CedaAction("SBC", "DISABLEDEMSAVE", "", "", "", "");
			if(!blRet)
			{
				AfxMessageBox(LoadStg(IDS_STRING854));
			}
			POSITION rlPos;
			for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
			{
				long llUrno;
				DEMDATA *prlDem = NULL;
				omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDem);
				if(prlDem!=NULL && prlDem->IsChanged != DATA_UNCHANGED )
				{
					CString olListOfData;
					CString olCurrentUrno;
					olCurrentUrno.Format("%d",prlDem->Urno);
					MakeCedaData(&omRecInfo,olListOfData,prlDem);
					/*
					int ilFldCnt = GetItemCount(olListOfData);
					if(ilFldCnt!=ilFields)
					{
						ofstream of;
						of.open(CCSLog::GetTmpPath("\\DEMerror.txt"), ios::out);
						of << "<CedaDemData::Release>Fields: " << pcmFieldList << endl << "Data: " << olListOfData.GetBuffer(0) << endl;
						of.close();
					}
					*/
					switch(prlDem->IsChanged)
					{
					case DATA_NEW:
						olInsertString += olListOfData + olEndl;//CString("\n");
						ilCurrentCount++;
						ilTotalCount++;
						ilNoInserts++;
						break;
					case DATA_CHANGED:
						olUpdateString += olListOfData + CString(",") + olCurrentUrno + olEndl;//CString("\n");
						ilNoUpdates++;
						ilTotalCount++;
						ilCurrentCount++;
						break;
					case DATA_DELETED:
						olDeleteString += olCurrentUrno + olEndl;//CString("\n");
						ilCurrentCount++;
						ilTotalCount++;
						ilNoDeletes++;
						break;
					}
					prlDem->IsChanged = DATA_UNCHANGED;
				}
				if(ilCurrentCount == 50)
				{
					if(ilNoInserts > 0)
					{
						blRet = CedaAction("REL","LATE","",olInsertString.GetBuffer(0));
						if(!blRet)
						{
							AfxMessageBox(olInsertString + LoadStg(IDS_STRING854));
							return;
						}
						strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
					}
					if(ilNoUpdates > 0)
					{
						blRet = CedaAction("REL","LATE","",olUpdateString.GetBuffer(0));
						if(!blRet)
						{
							AfxMessageBox(olUpdateString+LoadStg(IDS_STRING854));
							return;
						}
						strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
					}
					if(ilNoDeletes > 0)
					{
						blRet = CedaAction("REL","LATE","",olDeleteString.GetBuffer(0));
						if(!blRet)
						{
							AfxMessageBox(olDeleteString+LoadStg(IDS_STRING854));
							return;
						}
						strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
					}
					ilCurrentCount = 0;
					ilNoInserts = 0;
					ilNoUpdates = 0;
					ilNoDeletes = 0;
					olInsertString = olIRT + olOrigFieldList + olEndl;//CString(pcmListOfFields) + CString("\n");
					olUpdateString = olURT + olOrigFieldList + CString(",[URNO=:VURNO]")+ olEndl;//CString(pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
					olDeleteString = olDRT + CString(",[URNO=:VURNO]")+ olEndl;//CString("\n");
				}
			}//for(rlPos = ......
			if(ilNoInserts > 0)
			{
				blRet = CedaAction("REL","LATE","",olInsertString.GetBuffer(0));
				if(!blRet)
				{
					AfxMessageBox(olInsertString+LoadStg(IDS_STRING854));
					return;
				}
				strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
			}
			if(ilNoUpdates > 0)
			{
				blRet = CedaAction("REL","LATE","",olUpdateString.GetBuffer(0));
				if(!blRet)
				{
					AfxMessageBox(olUpdateString + LoadStg(IDS_STRING854));
					return;
				}

				strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
			}
			if(ilNoDeletes > 0)
			{
				blRet = CedaAction("REL","LATE","",olDeleteString.GetBuffer(0));
				if(!blRet)
				{
					AfxMessageBox(olDeleteString + LoadStg(IDS_STRING854));
					return;
				}

				strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
			}
			// Force all workstations to reload the plan
	/*********
			if(bpUseDemBc == true)
			{
				if(opCmd.Compare("NOBROADCAST")!=0)
				{
					if(ilTotalCount>0)
					{
						CString olTmpRuleNames = omRuleGrpName;
 						blRet = CedaAction("SBC", "ALLDEM", olTmpRuleNames.GetBuffer(0), pclBcUrno, "", "");
						if(!blRet)
						{
							AfxMessageBox(olTmpRuleNames + LoadStg(IDS_STRING854));
							return;
						}
						strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
					}
				}
			}
	*********/
			blRet = CedaAction("SBC", "ENABLEDEMSAVE", "", "", "", "");
			if(!blRet)
			{
				AfxMessageBox(LoadStg(IDS_STRING854));
				return;
			}
			//               CMD    TABLE   FLD  SEL SORT DAT DEST   WRI    ID  RECSET  FLDCNT
			//CedaAction("SBC", "ALLDEM", "", "", "",  "", "BUF1", false, 0,  NULL,   0);
		}//if(bmCanSave ==true)
	}
	else
	{
		CMapPtrToPtr olUrnoMap;
		CUIntArray olChangedList;
		CUIntArray olUrnoList;
		olOrigFieldList = CString(pcmListOfFields);
		olInsertString = olIRT + olOrigFieldList + olEndl;//CString(pcmListOfFields) + CString("\n");
		olUpdateString = olURT + olOrigFieldList + CString(",[URNO=:VURNO]")+ olEndl;//CString(pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
		olDeleteString = olDRT + CString(",[URNO=:VURNO]")+ olEndl;//CString("\n");

		//olInsertString = olIRT + CString(pcmListOfFields) + CString("\n");
		//olUpdateString = olURT + CString(pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
		olURT.Format("*CMD*,%s,URT,%d,", pcmTableName, ilFields);
		CString olSyncTest; 
		olSyncTest.Format("*CMD*,%s,SYNC,BLABLA1,BLABLA2", pcmTableName);
		//olDeleteString = olDRT + CString(",[URNO=:VURNO]")+ CString("\n");;

		for ( int i = 0; i < popDemList->GetSize(); i++)
		{
			//DEMDATA *prlDem = GetDemByUrno(popDemList->GetAt(i).Urno);
			olUrnoMap.SetAt( (void *)popDemList->GetAt(i).Urno, &popDemList->GetAt(i) );
			DEMDATA rlDem = popDemList->GetAt(i);
			//if(prlDem != NULL)
			{
				if(rlDem.IsChanged != DATA_UNCHANGED)
				{
					CString olListOfData;
					CString olCurrentUrno;
					olChangedList.Add(rlDem.IsChanged);
					olUrnoList.Add(rlDem.Urno);
					olCurrentUrno.Format("%d",rlDem.Urno);
					MakeCedaData(&omRecInfo,olListOfData,&rlDem);
					switch(rlDem.IsChanged)
					{
					case DATA_NEW:
						olInsertString += olListOfData + olEndl;//CString("\n");
						ilNoInserts++;
						ilCurrentCount++;
						break;
					case DATA_CHANGED:
						olUpdateString += olListOfData + CString(",") + olCurrentUrno + olEndl;//CString("\n");
						ilNoUpdates++;
						ilCurrentCount++;
						break;
					case DATA_DELETED:
						olDeleteString += olCurrentUrno + olEndl;//CString("\n");
						ilNoDeletes++;
						ilCurrentCount++;
						break;
					}
					DEMDATA *prlTmpDem = GetDemByUrno(rlDem.Urno);
					if(prlTmpDem != NULL)
					{
						prlTmpDem->IsChanged = DATA_UNCHANGED;
					}
					if(ilCurrentCount == 50)
					{
						if(ilNoInserts > 0)
						{
							blRet = CedaAction("REL","LATE","",olInsertString.GetBuffer(0));
							if(!blRet)
							{
								AfxMessageBox(olInsertString + LoadStg(IDS_STRING854));
								return;
							}
							strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
						}
						if(ilNoUpdates > 0)
						{
							blRet = CedaAction("REL","LATE","",olUpdateString.GetBuffer(0));
							if(!blRet)
							{
								AfxMessageBox(olUpdateString + LoadStg(IDS_STRING854));
								return;
							}
							strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
						}
						if(ilNoDeletes > 0)
						{
							blRet = CedaAction("REL","LATE","",olDeleteString.GetBuffer(0));
							if(!blRet)
							{
								AfxMessageBox(olDeleteString + LoadStg(IDS_STRING854));
								return;
							}
							strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
						}
						ilCurrentCount = 0;
						ilNoInserts = 0;
						ilNoUpdates = 0;
						ilNoDeletes = 0;
						olInsertString = olIRT + olOrigFieldList + olEndl;//CString(pcmListOfFields) + CString("\n");
						olUpdateString = olURT + olOrigFieldList + CString(",[URNO=:VURNO]")+ olEndl;//CString(pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
						olDeleteString = olDRT + CString(",[URNO=:VURNO]")+ olEndl;//CString("\n");

						/*
						olInsertString = olIRT + CString(pcmListOfFields) + CString("\n");
						olUpdateString = olURT + CString(pcmListOfFields) + CString(",[URNO=:VURNO]")+ CString("\n");
						olDeleteString = olDRT + CString(",[URNO=:VURNO]")+ CString("\n");;
						*/
			
					}
				}
			}
		}//for(rlPos = ......
		if(ilCurrentCount > 0)
		{
			if(ilNoInserts > 0)
			{
				blRet = CedaAction("REL","LATE","",olInsertString.GetBuffer(0));
				if(!blRet)
				{
					AfxMessageBox(olInsertString + LoadStg(IDS_STRING854));
					return;
				}
				strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
			}
			if(ilNoUpdates > 0)
			{
				blRet = CedaAction("REL","LATE","",olUpdateString.GetBuffer(0));
				if(!blRet)
				{
					AfxMessageBox(olUpdateString + LoadStg(IDS_STRING854));
					return;
				}
//				CedaAction("REL","LATE","",olSyncTest.GetBuffer(0));
				strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
			}
			if(ilNoDeletes > 0)
			{
				blRet = CedaAction("REL","LATE","",olDeleteString.GetBuffer(0));
				if(!blRet)
				{
					AfxMessageBox(olDeleteString + LoadStg(IDS_STRING854));
					return;
				}
				strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
			}
		}
/**************************
		if(bpUseDemBc == true)
		{
			POSITION olPos;
			void *pVoid;
			::GetWorkstationName(pclWks);
			if(opCmd == "F") //Alle Eins�tze zu einem Flug
			{
				CString olFkeys;
				if(lpUrnA != 0 && lpUrnD != 0)
				{
					olFkeys.Format("%ld;%ld", lpUrnA, lpUrnD);
				}
				else
				{
					DEMDATA *prlTmpDem;
					for( olPos = olFkeyMap.GetStartPosition(); olPos != NULL; )
					{
						char pclFley[12]="";
						olFkeyMap.GetNextAssoc( olPos, pVoid , (void *&)prlTmpDem );
						sprintf(pclFley, "%ld;", prlTmpDem->Fkey);
						olFkeys += CString(pclFley);

					}
				}
				CString olTmpRuleNames = omRuleGrpName;
				blRet = CedaAction("SBC", "FLTDEM", olTmpRuleNames.GetBuffer(0), pclBcUrno, pclWks, olFkeys.GetBuffer(0));
				if(!blRet)
				{
					AfxMessageBox(olTmpRuleNames + LoadStg(IDS_STRING854));
					return;
				}
				strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
			}
			else if(opCmd == "S") //einzelner EInsatz
			{
				//for( olPos = olUrnoMap.GetStartPosition(); olPos != NULL; )
				for(int ilC = 0; ilC < olChangedList.GetSize(); ilC++)
				{
					char pclUrno[12]="";
					CString olCmd;
					//olUrnoMap.GetNextAssoc( olPos, pVoid , (void *&)prlTmpDem );
					sprintf(pclUrno, "%ld", (long)olUrnoList[ilC]);
					CString olTmpRuleNames = omRuleGrpName;
					switch(olChangedList[ilC])
					{
					case DATA_NEW:
						olCmd = "I" + CString("[") + olTmpRuleNames + CString("]");
						break;
					case DATA_CHANGED:
						olCmd = "U" + CString("[") + olTmpRuleNames + CString("]");
						break;
					case DATA_DELETED:
						olCmd = "D" + CString("[") + olTmpRuleNames + CString("]");
						break;
					}
					llNewUrno = ogBasicData.GetNextUrno();
					sprintf(pclBcUrno, "%ld", llNewUrno);
					blRet = CedaAction("SBC", "SNGDEM", olCmd.GetBuffer(0), pclBcUrno, pclWks, pclUrno);
					if(!blRet)
					{
						AfxMessageBox(LoadStg(IDS_STRING854));
						return;
					}
					strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
				}
			}
			else if(opCmd == "MULTI")
			{
				CString olUrnos;
				//for( olPos = olUrnoMap.GetStartPosition(); olPos != NULL; )
				for(int ilC = 0; ilC < olChangedList.GetSize(); ilC++)
				{
					char pclUrno[12]="";
					//olUrnoMap.GetNextAssoc( olPos, pVoid , (void *&)prlTmpDem );
					sprintf(pclUrno, "%ld", (long)olUrnoList[ilC]);
					olUrnos += CString(pclUrno) + CString(",");
					CString olTmpRuleNames = omRuleGrpName;
					if((ilC != 0) && (ilC % 50 == 0))
					{
						llNewUrno = ogBasicData.GetNextUrno();
						sprintf(pclBcUrno, "%ld", llNewUrno);
						blRet = CedaAction("SBC", "MULTIDEM", olTmpRuleNames.GetBuffer(0), pclBcUrno, pclWks, olUrnos.GetBuffer(0));
						if(!blRet)
						{
							AfxMessageBox(LoadStg(IDS_STRING854));
							return;
						}
						strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
						olUrnos = CString("");
					}
				}
			}
			else if(opCmd == "A") //Alle Eins�tze ==> Plan wird neu geladen
			{
				CString olTmpRuleNames = omRuleGrpName;
				blRet = CedaAction("SBC", "ALLDEM", olTmpRuleNames.GetBuffer(0), pclBcUrno, pclWks, "");
				if(!blRet)
				{
					AfxMessageBox(LoadStg(IDS_STRING854));
					return;
				}

				strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
			}
		}
		********************/
	}

}



bool CedaDemData::GetDemsByFkey(CCSPtrArray<DEMDATA> &ropDems, long lpFkey,bool bpAlloc /* true */)
{
	
	FLIGHTDATA *prlFA = pomCedaFlightData->GetFlightByUrno(lpFkey);
	FLIGHTDATA *prlFD;
	long llUrnA = 0, llUrnD = 0;
	if(prlFA != NULL)
	{
		llUrnA = prlFA->Urno;
		prlFD = pomCedaFlightData->GetJoinFlight(prlFA);
		if(prlFD != NULL)
		{
			llUrnD = prlFD->Urno;
		}
	}
	DEMDATA  *prlDem;
	
	POSITION rlPos;
	for ( rlPos =  omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos,(void *& ) llUrno,(void *& )prlDem);
		if((prlDem->Ouri == llUrnA && llUrnA != 0)|| (prlDem->Ouro == llUrnD && llUrnD != 0))
		{
			if(bpAlloc)
			{
				ropDems.NewAt(ropDems.GetSize(), *prlDem);
			}
			else
				ropDems.Add(prlDem);
		}
	}
    return true;
}


void CedaDemData::GetDemsBetweenTimes(CCSPtrArray<DEMDATA> &ropDems,CTime opStart,CTime opEnd)
{
	POSITION rlPos;	
	ropDems.RemoveAll();
	CString olStart = opStart.Format("%Y.%m.%d %H:%M");
	CString olEnd = opEnd.Format("%Y.%m.%d %H:%M");
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		DEMDATA *prlDem = NULL;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDem);
		if(IsOverlapped(prlDem->DispDebe,prlDem->DispDeen,opStart,opEnd)==TRUE)
		{
			ropDems.Add(prlDem);
		}
	}
}

void CedaDemData::GetDemsBetweenTimes(CMapPtrToPtr &ropUrnoMap,CTime opStart,CTime opEnd)
{
	POSITION rlPos;	
	ropUrnoMap.RemoveAll();
	//CString olStart = opStart.Format("%Y.%m.%d %H:%M");
	//CString olEnd = opEnd.Format("%Y.%m.%d %H:%M");
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		DEMDATA *prlDem = NULL;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDem);
		
		if(IsOverlapped(prlDem->DispDebe,prlDem->DispDeen,opStart,opEnd)==TRUE)
		{
			ropUrnoMap.SetAt((void *)prlDem->Urno,prlDem);
		}
	}
}

void CedaDemData::GetAllDems(CMapPtrToPtr &ropUrnoMap)
{
	POSITION rlPos;	
	ropUrnoMap.RemoveAll();
	//CString olStart = opStart.Format("%Y.%m.%d %H:%M");
	//CString olEnd = opEnd.Format("%Y.%m.%d %H:%M");
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		DEMDATA *prlDem = NULL;
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDem);
		
		if(prlDem->IsChanged != DATA_DELETED)
		{
			ropUrnoMap.SetAt((void *)prlDem->Urno,prlDem);
		}
	
	}
}

CString CedaDemData::GetPfcSingeUrnos(long lpUrno)
{
	CString *polValue = NULL;
	CString olValue = "";

	if(omSingleFctcMap.Lookup((void *)lpUrno,(void *&)polValue)  == TRUE)
	{
		if(polValue != NULL)
		{
			olValue = *polValue;
		}
	}
	return olValue;

}

CString CedaDemData::GetPerSingeUrnos(long lpUrno)
{
	CString *polValue = NULL;
	CString olValue = "";

	if(omSinglePerMap.Lookup((void *)lpUrno,(void *&)polValue)  == TRUE)
	{
		if(polValue != NULL)
		{
			olValue = *polValue;
		}
	}
	return olValue;

}

CString CedaDemData::GetCicSingeUrnos(long lpUrno)
{
	CString *polValue = NULL;
	CString olValue = "";

	if(omSingleCicMap.Lookup((void *)lpUrno,(void *&)polValue)  == TRUE)
	{
		if(polValue != NULL)
		{
			olValue = *polValue;
		}
	}
	return olValue;

}

CString CedaDemData::GetPstSingeUrnos(long lpUrno)
{
	CString *polValue = NULL;
	CString olValue = "";

	if(omSinglePstMap.Lookup((void *)lpUrno,(void *&)polValue)  == TRUE)
	{
		if(polValue != NULL)
		{
			olValue = *polValue;
		}
	}
	return olValue;

}

CString CedaDemData::GetGatSingeUrnos(long lpUrno)
{
	CString *polValue = NULL;
	CString olValue = "";

	if(omSingleGatMap.Lookup((void *)lpUrno,(void *&)polValue)  == TRUE)
	{
		if(polValue != NULL)
		{
			olValue = *polValue;
		}
	}
	return olValue;

}

CString CedaDemData::GetRegSingeUrnos(long lpUrno)
{
	CString *polValue = NULL;
	CString olValue = "";

	if(omSingleRegMap.Lookup((void *)lpUrno,(void *&)polValue)  == TRUE)
	{
		if(polValue != NULL)
		{
			olValue = *polValue;
		}
	}
	return olValue;

}

CString CedaDemData::GetReqSingeUrnos(long lpUrno)
{
	CString *polValue = NULL;
	CString olValue = "";

	if(omSingleReqMap.Lookup((void *)lpUrno,(void *&)polValue)  == TRUE)
	{
		if(polValue != NULL)
		{
			olValue = *polValue;
		}
	}
	return olValue;

}

CString CedaDemData::GetTemplateName(long lpUrno)
{
	CString *polValue = NULL;
	CString olValue = "";

	if(omTemplateNameMap.Lookup((void *)lpUrno,(void *&)polValue)  == TRUE)
	{
		if(polValue != NULL)
		{
			olValue = *polValue;
		}
	}
	return olValue;

}

CString CedaDemData::GetRusn(long lpUrno)
{
	CString *polValue = NULL;
	CString olValue = "";

	if(omRusnMap.Lookup((void *)lpUrno,(void *&)polValue)  == TRUE)
	{
		if(polValue != NULL)
		{
			olValue = *polValue;
		}
	}
	return olValue;

}

CString CedaDemData::GetRust(long lpUrno)
{
	CString *polValue = NULL;
	CString olValue = "";

	if(omRustMap.Lookup((void *)lpUrno,(void *&)polValue)  == TRUE)
	{
		if(polValue != NULL)
		{
			olValue = *polValue;
		}
	}
	return olValue;

}

CString CedaDemData::GetRety(long lpUrno)
{
	CString *polValue = NULL;
	CString olValue = "";

	if(omRetyMap.Lookup((void *)lpUrno,(void *&)polValue)  == TRUE)
	{
		if(polValue != NULL)
		{
			olValue = *polValue;
		}
	}
	return olValue;

}

CString CedaDemData::GetGhsu(long lpUrno)
{
	CString *polValue = NULL;
	CString olValue = "";

	if(omGhsuMap.Lookup((void *)lpUrno,(void *&)polValue)  == TRUE)
	{
		if(polValue != NULL)
		{
			olValue = *polValue;
		}
	}
	return olValue;

}

CString CedaDemData::GetServiceName(long lpUrno)
{
	CString *polValue = NULL;
	CString olValue = "";

	if(omServiceNameMap.Lookup((void *)lpUrno,(void *&)polValue)  == TRUE)
	{
		if(polValue != NULL)
		{
			olValue = *polValue;
		}
	}
	return olValue;

}

void CedaDemData::ReleaseDeletedData()
{
	POSITION rlPos;

	DEMDATA *prlDem;
	for ( rlPos =  omDeletedUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omDeletedUrnoMap.GetNextAssoc(rlPos,(void *&) llUrno,(void *& ) prlDem);
		if (prlDem != NULL)
			DeleteDemInternal(prlDem);
	}
	omDeletedUrnoMap.RemoveAll();
}


bool CedaDemData::DemandHasEquipment(DEMDATA *prpDem,const CString& ropEquipmentType)
{
	CString olRudUrno;
	olRudUrno.Format("%ld",prpDem->Urud);
	CString olPerValues = ogBCD.GetValueList("REQ", "URUD", olRudUrno, "UEQU") ;
	if (olPerValues.IsEmpty())
		return false;

	CString olEqco = ogBCD.GetField("REQ","URUD",olRudUrno,"EQCO");
	if (!olEqco.IsEmpty())
	{
		CString olEqu = ogBCD.GetField("REQ","URUD",olRudUrno,"UEQU");
		CString olEqt = ogBCD.GetField("EQU","URNO",olEqu,"GKEY");
		if (olEqt == ropEquipmentType)
			return true;
	}
	else
	{
		CCSPtrArray<SGMDATA> olSgm;
		ogSgmData.GetSgmByUsgr("EQU",atol(olPerValues),olSgm);
		for(int k = 0; k < olSgm.GetSize() && k < 1; k++)
		{
 			CString olEqu;
			olEqu.Format("%ld",olSgm[k].Uval);
			CString olEqt = ogBCD.GetField("EQU","URNO",olEqu,"GKEY");
			if (olEqt == ropEquipmentType)
				return true;
		}
	}


	return false;
}

// if a new rule is found for a flight that has demands with jobs - 
// 1. new demands will be created for the new rule
// 2. old demands without jobs assigned will be deleted
// 3. old demands with jobs will be in conflict - "Job has expired demand"
bool CedaDemData::HasExpired(DEMDATA *prpDemand)
{
	return prpDemand->Flgs[1] == '1' ? true : false;
}

bool CedaDemData::GetLoadFormat()
{
	this->omLoadFormat.Empty();
	if (CedaAction("DLF","DEM","","","","","BUF1"))
	{
		if (this->GetDataBuff()->GetSize() > 0)
		{
			CString olBuf = this->GetDataBuff()->GetAt(0);
			if (olBuf.Find("DEM") >= 0)
				this->omLoadFormat = "DEM";
			else if (olBuf.Find("RUD") >= 0)
				this->omLoadFormat = "RUD";
		}
	}

	return this->omLoadFormat.IsEmpty() == TRUE;
}

bool CedaDemData::GetFlightsFromCciDemand(CDWordArray &ropDemands,CMapPtrToPtr &ropDemToFlightMap)
{
	int ilNumDemands = ropDemands.GetSize();
	if (ilNumDemands <= 0)
		return 0;

	CStringArray olFlights;

	CString olData, olTmpStr;
	for (int ilD = 0; ilD < ilNumDemands; ilD++)
	{
		if(!olData.IsEmpty())
			olData += ",";

		olTmpStr.Format("%ld", ropDemands[ilD]);
		olData += olTmpStr;

		if((ilD != 0 && ((ilD +1) % 1000) == 0) || ilD == (ilNumDemands-1)) // max 1000 dems at a time
		{
			if (CedaAction("FLC","DEM","","","",olData.GetBuffer(0),"BUF1") == false)
			{
				return false;
			}
			else
			{
				CString olResult;
				for (int i = 0; i < this->GetBufferSize(); i++)
				{
					CString olBuf;
					if (GetBufferLine(i, olBuf))
					{
						olResult += olBuf;
					}
				}

				CStringArray olDemFlights;
				ExtractItemList(olResult,&olDemFlights,';');

				for (i = 0; i < olDemFlights.GetSize(); i++)
				{
					CStringArray olFlights;
					ExtractItemList(olDemFlights[i],&olFlights,',');
					if (olFlights.GetSize() > 1)
					{
						CStringArray *polFlights = new CStringArray();
						polFlights->Append(olFlights);
						ropDemToFlightMap.SetAt(reinterpret_cast<void *>(atol(olFlights[0])),polFlights);
					}
				}
			}
			olData.Empty();
		}
	}

	return true;	
}

bool CedaDemData::RecalculateFlightsFromCCIDemands()
{
	// initialize CCI map
	void *polKey;
	CStringArray *polValue = NULL;

	for (POSITION pos = omCCIMap.GetStartPosition(); pos != NULL; )
	{
		omCCIMap.GetNextAssoc(pos,polKey,(void *&)polValue);
		delete polValue;
	}

	omCCIMap.RemoveAll();


	//	get all CCI demands
	CDWordArray olCCIDemands;
	for (int i = 0; i < this->omData.GetSize(); i++)
	{
		if (this->omData[i].IsCommonCheckin())
		{
			olCCIDemands.Add(this->omData[i].Urno);			
		}
	}

	// get all flights according to the CCI demands
	if (olCCIDemands.GetSize() > 0)
	{
		if (!this->GetFlightsFromCciDemand(olCCIDemands,omCCIMap))
			return false;
	}

	return true;
}

WORD CedaDemData::GetDispNr(long lpUrno)
{
	WORD ilValue = 0;

	if ( !omDispMap.Lookup((void *)lpUrno,ilValue)  )
	{
		ilValue = 0;
	}
	return ilValue;
}

