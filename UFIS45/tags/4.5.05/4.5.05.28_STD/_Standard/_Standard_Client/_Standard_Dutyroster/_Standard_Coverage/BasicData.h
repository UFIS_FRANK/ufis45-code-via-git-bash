// basicdat.h CCSBasicData class for providing general used methods

#ifndef _BASICDATA
#define _BASICDATA

#include <InitialLoadDlg.h>
#include <ccsglobl.h>
#include <ccsptrarray.h>
#include <ccscedaData.h>
#include <CcsLog.h>
#include <CedaCfgData.h>
#include <CCSTime.h>
#include <CCSDdx.h>
#include <CCSCedaCom.h>
#include <CCSBcHandle.h>
#include <CCSMessageBox.h>
#include <NameConfigurationDlg.h>
#include <CedaStfData.h>

//@Man:
//@Memo: BasicData class for providing general used methods
/*@Doc:
*/

CString LoadStg(UINT nID);

CTime COleDateTimeToCTime(COleDateTime opTime);
COleDateTime CTimeToCOleDateTime(CTime opTime);

int GetItemCount(CString olList, char cpTrenner  = ',' );
int ExtractItemList(CString opSubString, CStringArray *popStrArray, char cpTrenner = ',');
CString GetListItem(CString &opList, int ipPos, bool bpCut, char cpTrenner  = ',');
CString DeleteListItem(CString &opList, CString olItem);
bool GetListItemByField(CString &opDataList, CString &opFieldList, CString &opField, CString &opData);
bool GetListItemByField(CString &opDataList, CString &opFieldList, CString &opField, CTime &opData);
CString SortItemList(CString opSubString, char cpTrenner);
int SplitItemList(CString opString, CStringArray *popStrArray, int ipMaxItem, char cpTrenner = ',');
bool GetNameOfTSRRecord ( CString &ropUrno, CString &ropName );
bool GetStringForTSRTextEntry ( CString opEntry, CString &ropString );

/////////////////////////////////////////////////////////////////////////////
// class BasicData
const int	NUMCONFCOLORS = 17;

enum ColorTypes
{
	FUNCTION_1 = 0,	
	FUNCTION_2,	
	FUNCTION_3,	
	FUNCTION_4,	
	FUNCTION_5,	
	FUNCTION_6,	
	FUNCTION_7,	
	FUNCTION_8,	
	FUNCTION_9,	
	FUNCTION_10,	
	FUNCTION_11,
	COVERAGE_ALL,
	COVERAGE_SINGLE,
	SIMULATION_VISUAL,
	SIMULATION_CALC,
	MIN_MAX_LINES,
	FUNCTION_ALL
};

typedef struct	ColorConfDataStruct
{
	int			Index;
	int			Type;
	CString		Name;
	COLORREF	Color;

	ColorConfDataStruct(void)
	{
	}

	friend bool operator==(const ColorConfDataStruct& rrp1,const ColorConfDataStruct& rrp2)
	{
		return rrp1.Index == rrp2.Index 
			&& rrp1.Type  == rrp2.Type
			&& rrp1.Name  == rrp2.Name
			&& rrp1.Color == rrp2.Color
			;
	}

	friend bool operator!=(const ColorConfDataStruct& rrp1,const ColorConfDataStruct& r2)
	{
		return !operator==(rrp1,r2);
	}

}	COLORCONFDATA;

class CBasicData : public CCSCedaData
{

public:     

	CBasicData(void);
	~CBasicData(void);
	
	int imScreenResolutionX;
	int imScreenResolutionY;
	int imMonitorCount;
	CString omUserID;

	long GetNextUrno(void);
	void PushBackUrno(long lpUrno);

//@ManMemo:Load Timeframe parameters 
/*@Doc:
	CStringArray that contains the absolut and relative start- and endtime 
	of the loaded timeframe
*/
	CStringArray omLoadTimeValues;
	CStringArray omValidTemplates;


	bool bmCheckMasterWeek;


//@ManMemo:Show Windows
/*@Doc:
	Flags to show and hide the the different charts. 
*/
	bool bmShowWindows[4];
//@ManMemo:Get N Urnos
/*@Doc:
	reads ipNrOfUrnos new Urnos with the GMU command and stores them in omUrnos
	return true or false
*/
	bool GetNurnos(int ipNrOfUrnos);
//@ManMemo:Get Diagramm timeframe
/*@Doc:
	writes the start- and endtime of the loaded timeframe in opStart and opEnd
*/
	void GetDiagramStartTime(CTime &opStart, CTime &opEnd);
//@ManMemo:Set Diagramm timeframe
/*@Doc:
	sets the diagramm start- and endtime 
	opDiagramStartTime = new loaded starttime
	opEndTime = new loaded endtime
*/
	void SetDiagramStartTime(CTime opDiagramStartTime, CTime opEndTime);
	void SetWorkstationName(CString opWsName);
	CString GetWorkstationName();
	CString GetBewTotalPath();
	CString GetBewTimePath();
	CString GetEditor();
	CString GetEditorPath();
	bool GetWindowPosition(CRect& rlPos,CString olMonitor);
	int GetUtcDifference(void);

	bool GetCalcToLocal();
//@ManMemo:Get load relative flag
/*@Doc:
	returns true if the time frame has to be loaded relative
	returns false if the time frame has to be loaded absolute
*/
	bool GetLoadRelFlag();
	void SetLoadRelFlag(bool bpLoadRel);

	int GetDisplayDuration();

	BOOL SetLocalDiff();

	CTimeSpan GetLocalDiff(CTime opDate)
	{
		if(opDate < omTich)
		{
			return omLocalDiff1;
		}
		else
		{
			return omLocalDiff2;
		}

		//return omLocalDiff;
	};
	CTimeSpan GetLocalDiff1() { return omLocalDiff1; };
	CTimeSpan GetLocalDiff2() { return omLocalDiff2; };
	CTime     GetTich() { return omTich; };

	void LocalToUtc(CTime &opTime);
	void UtcToLocal(CTime &opTime);

	BOOL IsEquipmentAvailable();
	BOOL IsDebugMode();
	void ReadAllConfigData(void);
	int	 GetColorConfData(CCSPtrArray<COLORCONFDATA>& ropArray);
	BOOL SetColorConfData(int ipIndex,const COLORCONFDATA& ropData);		
	void SetWindowStat(const char *pcpKey, CWnd *popWnd);
	bool GetDispoTimeframeWhereString(CTime opStartTime,CTime opEndTime,CString &ropWhere);
	BOOL IsBrosAvailable();
	BOOL ShowNameConfDlg(CWnd *popParent);
	CString	GetFormattedEmployeeName(struct STFDATA *popStfData);
	CString GetNameConfigString();
	void	SetNameConfigString(const CString& ropString);
	BOOL	IsRuleDiagnosisEnabled();
	CString Decode(const CString& ropString);
	void	SetUserName(const CString& ropName) { omUserName = ropName;}
	CString	GetUserName();
	CString	GetPassword();
	CString	GetServerPath()	{ return omServerPath;	}
	CString	GetFileFilter()	{ return omFileFilter;	}
	BOOL	UseEvaluationFactor();	
	BOOL	DebugAutoCoverage();
	BOOL	DumpAutoCoverage();
	BOOL	IsUtplAvailable();
	CString	GetDrrIndexHint();

public:
	static void TrimLeft(char *s);
	static void TrimRight(char *s);

private:
	CTimeSpan omLocalDiff1;
	CTimeSpan omLocalDiff2;
	CTime	  omTich;

	char pcmCedaCmd[12];
	CTime omDiaStartTime;
	CTime omDiaEndTime;

	bool bmLoadRel;
	bool bmDebugMode;
	bool bmUseEvaluationFactor;
	bool bmDebugAutoCoverage;
	bool bmDumpAutoCoverage;
	int imNextGMUAmount;

	int imDisplayDuration;

	CString omEditor;
	CString omEditorPath;
	CString omBewTotalPath;
	CString omBewTimePath;
	CString omDefaultComands;
	CString omActualComands;
	char pcmCedaComand[24];
	CString omWorkstationName; 
private:
	CDWordArray omUrnos;
	CCSPtrArray<COLORCONFDATA>	omColorConfData;
	NameConfigurationDlg		*pomNameConfDlg;
	bool						bmRuleDiagnosis;
	CString						omUserName;
	CString						omPassword;
	CString						omServerPath;
	CString						omFileFilter;
};


#endif
