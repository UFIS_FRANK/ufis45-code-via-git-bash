#if !defined(AFX_COVERAGEPFCSELECTDLG_H__7143B941_6F40_11D2_85EB_0000C04D916B__INCLUDED_)
#define AFX_COVERAGEPFCSELECTDLG_H__7143B941_6F40_11D2_85EB_0000C04D916B__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// CoveragePfcSelectDlg.h : header file
//

#include <CedaPfcData.h>

/////////////////////////////////////////////////////////////////////////////
// CoveragePfcSelectDlg dialog

class CoveragePfcSelectDlg : public CDialog
{
// Construction
public:
	CoveragePfcSelectDlg(CWnd* pParent = NULL, CUIntArray *popPfcUrnos = 0);   // standard constructor

	~CoveragePfcSelectDlg();

// Dialog Data
	//{{AFX_DATA(CoveragePfcSelectDlg)
	enum { IDD = IDD_COVERAGE_PFC_SELECT };
	CListBox	m_CL_List;
	//}}AFX_DATA

	CUIntArray omPfcUrnos;

	CCSPtrArray<PFCDATA> omPfcData;

	CString omSelPfcCode;	

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CoveragePfcSelectDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CoveragePfcSelectDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnDblclkPfcList();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COVERAGEPFCSELECTDLG_H__7143B941_6F40_11D2_85EB_0000C04D916B__INCLUDED_)
