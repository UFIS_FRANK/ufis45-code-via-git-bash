// CflReadyDlg.cpp : implementation file
//

#include <stdafx.h>
#include <Coverage.h>
#include <CflReadyDlg.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCflReadyDlg dialog


CCflReadyDlg::CCflReadyDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCflReadyDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCflReadyDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CCflReadyDlg::CCflReadyDlg(UINT nID,CWnd* pParent /*=NULL*/)
	: CDialog(nID, pParent)
{
	//{{AFX_DATA_INIT(CCflReadyDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CCflReadyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCflReadyDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCflReadyDlg, CDialog)
	//{{AFX_MSG_MAP(CCflReadyDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCflReadyDlg message handlers

BOOL CCflReadyDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CWnd *polWnd = GetDlgItem(IDC_CFLTEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_CFL_READY));
	}

	SetWindowText(LoadStg(IDS_WARNING));
	
	SetWindowPos(&wndTopMost,0,0,0,0,SWP_NOMOVE| SWP_NOSIZE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
