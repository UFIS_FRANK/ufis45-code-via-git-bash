#if !defined(AFX_MASSTABDLG_H__EE58CFF1_40E9_11D3_94F5_00001C018ACE__INCLUDED_)
#define AFX_MASSTABDLG_H__EE58CFF1_40E9_11D3_94F5_00001C018ACE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MasstabDlg.h : header file
//


#define ONE_HOUR_POINT  (10 / 2)

/////////////////////////////////////////////////////////////////////////////
// CMasstabDlg dialog

class CMasstabDlg : public CDialog
{
private:
	CTime omDispStartTime;
	CTime omMaxDispStartTime;
	CTime omMinDispStartTime;
	CTimeSpan omDuration;
	int		m_Hour;
	int		m_FontHeight;
	bool bmTimeLines;

// Construction
public:
	CMasstabDlg(CWnd* pParent = NULL);   // standard constructor

	void SetDispStartTime(CTime opDispStartTime);

	void SetMaxDispStartTime(CTime opMaxDispStartTime);
	void SetMinDispStartTime(CTime opMinDispStartTime);

	CTime GetDispStartTime();

	void SetDispDuration(CTimeSpan opDuration);

	CTimeSpan GetDispDuration();

	bool GetMin();

	bool GetTimeLines();

	int GetFont();

	bool GetGrapicState();

	bool GetShiftState();

	bool GetBaseShiftState();

	void SetGrapicState(bool bpState);

	void SetShiftState(bool bpState);

	void SetBaseShiftState(bool bpState);

	int GetUpperBound();

	int GetLowerBound();

	int GetFixValue();

	bool IsBorderFix();

// Dialog Data
	//{{AFX_DATA(CMasstabDlg)
	enum { IDD = IDD_MASSTABDLG };
	CSliderCtrl	m_Font;
	CStatic	m_TestText;
	CEdit	m_Time;
	CEdit	m_Date;
	CButton	m_Min;
	CSliderCtrl	m_Height;
	CButton	m_TimeLines;
	CButton	m_4h;
	CButton	m_100Proz;
	int		m_4hVal;
	int		m_100PercVal;
	int		m_RBMinVal;
	BOOL	m_BaseShift;
	BOOL	m_Graphic;
	BOOL	m_Shifts;
	int		m_LowerBound;
	int		m_UpperBound;
	int		m_FixValue;
	int		m_BorderFix;
	//}}AFX_DATA



// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMasstabDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMasstabDlg)
	afx_msg void On4h();
	afx_msg void On24h();
	afx_msg void On12h();
	afx_msg void On10h();
	afx_msg void On6h();
	afx_msg void On8h();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MASSTABDLG_H__EE58CFF1_40E9_11D3_94F5_00001C018ACE__INCLUDED_)
