// stviewer.h : header file
//

#ifndef _COVERAGE_VIEWER_H_
#define _COVERAGE_VIEWER_H_

#include <cviewer.h>
#include <CedaDemData.h>
#include <CedaFlightData.h>
#include <CedaSdtData.h>
#include <CedaSdgData.h>
#include <CedaBsdData.h>
#include <CedaMsdData.h>
#include <CedaDrrData.h>
#include <CoverageGraphicWnd.h>
#include <BaseShifts.h>
#include <BaseShiftsGroups.h>
#include <AutoCoverage.h>

enum 
{
	DEMANDS = 0,
	SHIFT_DEMAND = 1,
	BASIC_SHIFT = 2
	
};

enum
{
	BASE_SHIFT_ALL,
	BASE_SHIFT_SPECIAL,
	BASE_SHIFT_STATIC,
	BASE_SHIFT_DYNAMIC
};



struct COV_DELEGATIONDATA 
{
	CString Fctc;			
	CString Bsdc;
    CString Text;
    CTime StartTime;
	CTime EndTime;
	COLORREF Color;

	COV_DELEGATIONDATA()
	{
		StartTime = TIMENULL;
		EndTime = TIMENULL;
	}
};


struct COV_INDICATORDATA
{
	// standard data for indicators in general GanttBar
	CTime StartTime;
	CTime EndTime;
	COLORREF Color;
};
struct COV_BARDATA 
{
	long Urno;		// Shift Urno
	long RUrno;		// Urno from BSD
	int  Type;		//Shift Demand Table Bar OR 

	int  Brutto;    // if minimized ==> show number of brutto bars
	//Basic Shift Bar
	//BASIC_SHIFT,
	//SHIFT_DEMAND

	bool IsInvalid;
    CString Text;
	CString StatusText;
    CTime StartTime;
    CTime EndTime;
    int FrameType;
    int MarkerType;
    CBrush *MarkerBrush;
	COLORREF TextColor;
    int OverlapLevel;       // zero for the top-most level, each level lowered by 4 pixels
	CCSPtrArray<COV_DELEGATIONDATA> Delegations3; // Daily Roster Absences
	CCSPtrArray<COV_DELEGATIONDATA> Delegations2; // Daily Roster Deviations
	CCSPtrArray<COV_DELEGATIONDATA> Delegations;
	CCSPtrArray<COV_INDICATORDATA> Indicators;
};
struct COV_BKBARDATA
{
	CString Type;		// Shift Demand Table Bar OR
						// Basic Shift Bar
    CTime StartTime;
    CTime EndTime;
    int FrameType;
	CString Text;
    int MarkerType;
    CBrush *MarkerBrush;
	COLORREF TextColor;
						
	COV_BKBARDATA()
	{
		Type = CString("");		
		StartTime = TIMENULL;
		EndTime = TIMENULL;
		FrameType = -1;
		MarkerType = -1;
		MarkerBrush = NULL;
		TextColor = COLORREF(RGB(0, 0, 0));
	};
};
struct COV_LINEDATA 
{
	int Index;
	long Urno;		// from Shift
	CString Type;		// (D) Shift Demand Table Bar OR
						// (B) Basic Shift Bar
	CString Text;
    CTime StartTime;
    CTime EndTime;
	int SingleOffSet;
    int MaxOverlapLevel;					// maximum number of overlapped level of bars in this line
    CCSPtrArray<COV_BARDATA>	Bars;		// in order of painting (leftmost is the bottommost)
    CCSPtrArray<COV_BKBARDATA>	BkBars;		// background bar
    CCSPtrArray<int>			TimeOrder;	// maintain overlapped bars, see below
	COLORREF TextColor;
	COLORREF TextColor2;
	COLORREF TextColor3;
};
struct COV_GROUPDATA 
{
	int							GroupNo;	//BASIC_SHIFT,
											//SHIFT_DEMAND

	int							DemandLevel;
	int							ShiftView;
	CString						GroupName;	// Gruppen ID
	CString						DemandName;
											// standard data for groups in general Diagram
    CString						Text;
    CCSPtrArray<COV_LINEDATA>	Lines;
	CMapPtrToPtr				LinesMap;

	COV_GROUPDATA()
	:	LinesMap(100)
	{
		DemandLevel = -1;
		ShiftView = -1;
	}

	COV_GROUPDATA& operator=(const COV_GROUPDATA& rhs)
	{
		if (this != &rhs)
		{
			this->DemandLevel = rhs.DemandLevel;
			this->ShiftView	  = rhs.ShiftView;
			this->GroupName	  = rhs.GroupName;
			this->GroupNo	  = rhs.GroupNo;
			this->Text		  = rhs.Text;	
			VERIFY(rhs.Lines.GetSize() == 0);
			VERIFY(rhs.LinesMap.GetCount() == 0);
		}

		return *this;
	}

};

// Attention:
// Element "TimeOrder" in the struct "COV_LINEDATA".
//
// This array will maintain bars sorted by StartTime of each bar.
// You can see it the same way you see an array of index in the array COV_BARDATA[].
// For example, if there are five bars in the first overlapped group, and their
// overlap level are [3, 0, 4, 1, 2]. So the indexes to the COV_BARDATA[] sorted by
// the beginning of time should be [1, 3, 4, 0, 2]. You can see that the indexes
// in these two arrays are cross-linked together. Please notice that we define
// the overlap level to be the same number as the order of the bar when sorted
// by StartTime.
//
// However, I keep the relative offset in this array, not the absolute offset.
// The relative offset helps us separate each group of bar more efficiently.
// For example, using the same previous example, the relative offset of index
// to the array COV_BARDATA[] kept in this array would be [+1, +2, +3, -3, -2].
//
// For a given "i", the bar in the same overlap group will always have the
// same value of "i - Bars[i + TimeOrder[i]].OverlapLevel". This value will be the
// index of the first member in that group.
//
// Notes: This bar order array should be implemented with CCSArray <int>.


/////////////////////////////////////////////////////////////////////////////
// CoverageDiagramViewer window
struct BASICSHIFTS
{
	long	Urno;		// we don't save this records into Database but it's nice to have an urno
	long	RUrno;		// Reference Urno to BSH
	CTime	Vafr;		// Valid from
	CTime   Vato;			// Valid to
	CTime	From;		// Shift End
	CTime   To;			// Shift Begin
	CTime   BlFrom;		// Timeframe Break from
	CTime	BlTo;		// Timeframe Break to
	CTime	BreakFrom;	// Break Time From
	CTime   BreakTo;	// Break Time To
	CString	Relative;	// Relative Shiftbegin (S), Absolute (A)
	CString Code;		// Code of Basicshift
	CString Fctc;		// Function Code of Basicshift if one exists
	int		Stretch;	// Possible stretching
	int		Shrink;		// Possibel Shrinking
	CString	Type;		// Static Shift (S), Dynamic (D)
	int 	DayOffSet;		// Offset in Tagen relativ zu LoadStart
	int     SingleOffSet;
	bool    IsSelected;
	BASICSHIFTS()
	{
		Vafr    = TIMENULL;
		Vato    = TIMENULL;
		From	= TIMENULL;	
		To		= TIMENULL;			
		BlFrom	= TIMENULL;		
		BlTo	= TIMENULL;		
		BlFrom	= TIMENULL;
		BlTo    = TIMENULL;
		Relative = CString("A");	
		Code	= CString("");		
		Stretch = 0;	
		Shrink  = 0;		
		Type    = CString("S");	
		DayOffSet = 0;
		IsSelected = false;
		SingleOffSet = 0;
	}
};


//@Man:
//@Memo: Coverage Diagram Viewer
/*@Doc: 
	Viewer for the coverage diagram
*/


class CoverageGantt;
class CCoverageGraphicWnd;

class CoverageDiagramViewer: public CViewer
{
	friend CoverageGantt;

// Constructions
public:
    CoverageDiagramViewer();
    ~CoverageDiagramViewer();

private:
	// Expanded Shiftdata into dayly iteration
	CCSPtrArray<BASICSHIFTS> omBasicShiftData;
    CMapPtrToPtr omBasicShiftUrnoMap;
 
	CMapPtrToPtr omShiftsDemandMap;
 
	CMapStringToPtr omBasicShiftKeyMap;

	//MWO:P 31.03.03 Performance
	//Da hier keiner mehr durchblickt, wird diese globale Variable eingef�hrt,
	//um die WM_UPDATEXXX so wenig wie m�glich aufzurufen
	bool bmMaleAllesNeu_lokalgesetzt;
	bool bmMaleAllesNeu_globalgesetzt;
	//MWO:P 31.03.03 Performance

public:
//@ManMemo:GetGhsList 
 /*@Doc:
	fills opPfcUrnos with the Urnos of the selected functions
	opGhsUrnos is allways empty no use anymore
	*/
	void GetGhsList(CUIntArray &opGhsUrnos, CUIntArray &opPfcUrnos);

//@ManMemo:GetTotalTsrArray 
 /*@Doc:
	returns a pointer to the CStringArray omTotalTsrArray
	*/
	const CStringArray *GetTotalTsrArray();

//@ManMemo:GetFilterTsrArray 
 /*@Doc:
	returns a pointer to the CStringArray omFilterTsrArray
	*/
	const CStringArray *GetFilterTsrArray();
//@ManMemo:GetFlightDemMap 
 /*@Doc:
	returns a pointer to the CMapPtrToPtr omFlightDemMap
	*/
	const CMapPtrToPtr *GetFlightDemMap();

	const CMapPtrToPtr *GetDemUrnoMap()				{ return &omDemUrnoMap;	}

	const CMapPtrToPtr *GetSdtUrnoMap()				{ return &omSdtUrnoMap;	}

	const CMapPtrToPtr *GetFilterFlightUrnoMap()	{ return &omFilterFlightUrnoMap;	}

//@ManMemo:GetPfcCodeList 
 /*@Doc:
	returns omPfcCodeList
	*/
	CString GetPfcCodeList();
//@ManMemo:PrepareShiftNameMap 
 /*@Doc:
 fills 
	CMapStringToPtr omShiftNameMap;
	CCSPtrArray<CString> omShiftNameUrnoArray;
	CCSPtrArray<CString> omShiftNameArray;

		with data from SPLTAB 
*/
	void PrepareShiftNameMap();
//@ManMemo:SetFilterIdx 
 /*@Doc:
	Changes the Viewer filter type of the evaluation filter
    Sets the imViewerFiterIdx = ipFilterIdx;
	and calls LoadGhdWindow();

	ipFilterIdx can be : FUNCTION,QUALIFICATION, CHECKIN,POSITIONS,GATES ,EQUIPMENT,EQUIPMENTGROUP;
*/	
	void SetFilterIdx(FilterType ipFilterIdx);

//@ManMemo:GetFilterIdx 
 /*@Doc:
	returns	 imViewerFiterIdx
	imViewerFiterIdx can be : FUNCTION,QUALIFICATION, CHECKIN,POSITIONS,GATES ,EQUIPMENT,EQUIPMENTGROUP;
*/	
	FilterType GetFilterIdx();

//@ManMemo:GetFilterEquipmentType 
 /*@Doc:
	returns	 imViewerFilterEquipmentType
*/	
	CString GetFilterEquipmentType();

//@ManMemo:GetFilterIdx 
 /*@Doc:
	In the case that the basicshift has a function code (Swissport) this function checks if this
	function code is in the Evaluation filter.
*/	
	bool CheckBsdbyStdFilter(BASICSHIFTS * prpBsh);

//@ManMemo:PrepareEvaluationFilter 
 /*@Doc:
	fills the filter maps for the Evaluation filter.
*/
	void PrepareEvaluationFilter(CString opViewName);

//@ManMemo:PrepareBaseShiftsFilter 
 /*@Doc:
	fills the filter maps for the basic shift filter.
*/
	void PrepareBaseShiftsFilter(CString opViewName);

	BASICSHIFTS * GetBasicShiftByUrno(long pcpUrno);

	void Attach(CWnd *popAttachWnd);

//@ManMemo:SetCoverage 
 /*@Doc:
	sets pomCoverageWnd = popCoverageWnd;
*/	
	void SetCoverage(CCoverageGraphicWnd *popCoverageWnd)
	{
		pomCoverageWnd = popCoverageWnd;
	}

//@ManMemo:AlterSdtCount
 /*@Doc:
	Increases /decreases the count of selected sdt's in the compressed state of the demand chart
	if the user selects "Number" in the context menu
	ilOldCount original number of sdt's 
	ilNewCount new number of sdt's 
	llKeyUrno Urno of the Sdt that is associated with th eselected bar
*/	
    void AlterSdtCount(int ilOldCount ,int ilNewCount, long llKeyUrno);

	void DiscardAutoCovChanges();
	void ReleaseAutoCovChanges();

	void DistinctShifts(long lpUpfc,CUIntArray *ropShiftArray,CTime opStartTime, CTime opEndTime,
						bool bpIgnoreFunction = false,bool bpIgnoreBreak = false);

	int CalcNewBreakOffSet(int ipBPStart, int ipBPEnd, int ipBDuration,
									  CUIntArray &rpInDemData,CUIntArray &rpOutShiftCovData);

	void BreakPostOptimization(CTime opStartTime,CTime opEndTime,bool bpOnlyAutoSdt = false);

	void ChangeViewTo(const char *pcpViewName, CTime opStartTime, CTime opEndTime);

	void ChangeViewToForHScroll(CTime opStartTime = TIMENULL, CTime opEndTime = TIMENULL);
	void ChangeViewToForShiftDemandUpdate();

	void SetMarkTime(CTime opMarkTimeStart, CTime opMarkTimeEnd);
	void SetLoadTimes(CTime opTimeStart, CTime opTimeEnd);

	void Register();
	void SetDisplayTimes(CTime opStart,CTime opEnd);

	void SetGeometryValues(bool bpGeometryMinimize,bool bpGeometryTimeLines, int opGeometryFontIndex );

	void SetSimulationFlag(bool blFlag);
	bool GetSimulationFlag();
	
	void DeSelectBsh();

	void SaveDRRBreakChanges();

	void DiscardDRRBreakChanges();

	void AutoAssign(int ipDirection,int ipShiftSelection,int ipShiftDeviation,int ipAdaptIterations,int ilAdaption,
					int ipFactor,int ipMinShiftCoverage,bool blUseBaseCoverage,CTime opStartTime, CTime opEndTime);

	void DistinctFunctions(CStringArray &ropFunctions);
    void PrepareBaseShifts(CStringArray &ropBaseViews,CTime opAssignStart,CTime olAssignEnd,double dpFactor);
    void AddAutoCovShifts(BASICSHIFTS * prpBsh,int ipGroupIndex,CTime opAssignStart,int ipShiftStartOffSet,int ipShiftEndOffSet);

	void GenerateAutoCovSdts(CStringArray &opAutoCovShifts,CTime opAssignStart);

	int	 SimVariant()											{	return imSimVariant;		}
	bool SimVariant(int ipSimVariant)							{	imSimVariant = ipSimVariant;	return true;	}
	int	 SimType()												{	return imSimType;			}
	bool SimType(int ipSimType)									{	imSimType = ipSimType;	return true;	}
	int	 SimValue()												{	return imSimValue;			}
	bool SimValue(int ipSimValue)								{	imSimValue = ipSimValue;	return true;	}
	int	 SimOffset()											{	return imSimOffset;			}
	bool SimOffset(int ipSimOffset)								{	imSimOffset = ipSimOffset;	return true;	}
	int	 SimHeight()											{	return imSimHeight;			}
	bool SimHeight(int ipSimHeight)								{	imSimHeight = ipSimHeight;	return true;	}
	int	 SimHeightType()										{	return imSimHeightType;			}
	bool SimHeightType(int ipSimHeightType)						{	imSimHeightType = ipSimHeightType;	return true;	}
	int	 SimIterations()										{	return imSimIterations;			}
	bool SimIterations(int ipSimIterations)						{	imSimIterations = ipSimIterations;	return true;	}
	int	 SimWidth()												{	return imSimWidth;			}
	bool SimWidth(int ipSimWidth)								{	imSimWidth = ipSimWidth;	return true;	}
	int	 SimPeakWidth()											{	return imSimPeakWidth;			}
	bool SimPeakWidth(int ipSimPeakWidth)						{	imSimPeakWidth = ipSimPeakWidth;	return true;	}
	int	 SimPeakHeight()										{	return imSimPeakHeight;			}
	bool SimPeakHeight(int ipSimPeakHeight)						{	imSimPeakHeight = ipSimPeakHeight;	return true;	}
	int	 SimDisplay()											{	return imSimDisplay;			}
	bool SimDisplay(int ipSimDisplay)							{	imSimDisplay = ipSimDisplay;	return true;	}
	bool NoDemUpdates()											{	return bmNoDemUpdates == TRUE;		}
	bool NoDemUpdates(bool bpNoDemUpdates)						{	bmNoDemUpdates = bpNoDemUpdates;	return true;		}
	bool SdgChanged()											{	return bmSdgChanged;		}
	bool SdgChanged(bool bpSdgChanged)							{	bmSdgChanged = bpSdgChanged;	return true;	}	
	bool AutoCovIsActive()										{	return bmAutoCovIsActive;	}
	bool AutoCovIsActive(bool bpAutoCovIsActive)				{	bmAutoCovIsActive = bpAutoCovIsActive;	return true;		}
	void GetMarkTimes(CTime& ropMarkStart,CTime& ropMarkEnd)	{	ropMarkStart = omMarkTimeStart; ropMarkEnd = omMarkTimeEnd; }
	bool SimulationActive()										{	return bmSimulate;			}
	bool SimulationActive(bool bpActive)						{	bmSimulate = bpActive; return true;			}
	bool IsRealRoster()											{	return bmIsRealRoster;		}
	bool IsRealRoster(bool bpIsRealRoster)						{	bmIsRealRoster = bpIsRealRoster; return true;	}
	CString	CurrentShiftRosterName()							{	return omSplName;			}
	bool CurrentShiftRosterName(const CString& ropName)			{	omSplName = ropName; return true;	}
	long CurrentMasterShiftRequirement()						{	return lmSdgu;				}	
	bool CurrentMasterShiftRequirement(long lpSdgu)				{	lmSdgu = lpSdgu; return true;			}
	void ShiftNameArray(CStringArray& ropNameArray)				{	ropNameArray.Copy(omShiftNameArray);	} 					
	bool ShowActiveDrrLevel()									{	return bmShowActualDsr;		}
	bool ShowActiveDrrLevel(bool bpShow)						{	bmShowActualDsr = bpShow;	return true;		}	
	bool IsLocation()											{	return bmLocation;			}
	bool IsPersonal()											{	return bmPersonal;			}
	bool IsEquipment()											{	return bmEquipment;			}	
	CString	TopViewName()										{	return omTopViewName;		}
	CString	EvalViewName()										{	return omEvalViewName;		}
	bool EvaluationFilterChanged()								{	return bmEvulationFilterChanged;	}	
	bool EvaluationFilterChanged(bool bpChanged)				{	bmEvulationFilterChanged = bpChanged;	return true;	}

	bool PfcMapForGraphic(CStringArray& ropDisplayUrnos)		{	ropDisplayUrnos.Append(omPfcMapForGraphic);
																	return bmUseAllPfcForGraphic;	
																}	
	
	bool GhsMapForGraphic(CStringArray& ropDisplayUrnos)		{	ropDisplayUrnos.Append(omGhsMapForGraphic);
																	return bmUseAllGhsForGraphic;	
																}	
	
	bool CicMapForGraphic(CStringArray& ropDisplayUrnos)		{	ropDisplayUrnos.Append(omCicMapForGraphic);
																	return bmUseAllCicForGraphic;	
																}	
	
	bool PstMapForGraphic(CStringArray& ropDisplayUrnos)		{	ropDisplayUrnos.Append(omPstMapForGraphic);
																	return bmUseAllPstForGraphic;	
																}	
	
	bool GatMapForGraphic(CStringArray& ropDisplayUrnos)		{	ropDisplayUrnos.Append(omGatMapForGraphic);
																	return bmUseAllGatForGraphic;	
																}	
	
	bool EquMapForGraphic(CStringArray& ropDisplayUrnos)		{	ropDisplayUrnos.Append(omEquMapForGraphic);
																	return bmUseAllEquForGraphic;	
																}
	
	bool GetDebugStatistics(CMapStringToPtr& ropBsdGroupStatistic);	
	
	bool GetSdtsBetweenTimesOrderedByBsdc(CTime opStart,CTime opEnd,CMapStringToPtr &opSdtBsdcMap);
	void AllDistinctSdtFunctions(CStringArray &ropFunctions);


private:
	// Simulation data
	int imSimType;		// SIM_ABSOLUTE | SIM_PERCENT | SIM_CONSTANT	
	int imSimValue;		// Simulation 
	int imSimOffset;	// Offset in minutes

	int	imSimPeakWidth;	// 
	int	imSimPeakHeight;//
	int	imSimIterations;//
	int	imSimWidth;		//
	int	imSimHeight;	// 	
	int	imSimHeightType;// SIM_ABSOLUTE | SIM_PERCENT

	long lmInternalUrno;

	int	 imSimVariant;	// 0 : Offset, 1 : Cut, 2 : Smooth		

	bool bmSimulate;	// Simulation active or not	
	int	 imSimDisplay;	// 0 : Calculation, 1 : Visualization only

	//CCSPtrArray<SDTDATA> omSdtData;
	CMapPtrToPtr omSdtUrnoMap;
	CMapPtrToPtr omDemUrnoMap,omRealDemUrnoMap;
	CMapPtrToPtr omAssignDemUrnoMap;

	CMapPtrToPtr omFilterFlightUrnoMap;

	bool bmEquChanged;

	bool bmShowActualDsr;

	CStringArray omPfcMapForGraphic;		// Funktionen
	bool		bmUseAllPfcForGraphic;

	CStringArray omGhsMapForGraphic;		// Quali
	bool		bmUseAllGhsForGraphic;

	CStringArray omCicMapForGraphic;		// CheckIn
	bool		bmUseAllCicForGraphic;

	CStringArray omPstMapForGraphic;		// Positionen
	bool		bmUseAllPstForGraphic;

	CStringArray omGatMapForGraphic;		// Gates
	bool		bmUseAllGatForGraphic;

	CStringArray omEquMapForGraphic;		// Equipment
	bool		bmUseAllEquForGraphic;

	bool bmPersonal;
	bool bmLocation;
	bool bmEquipment;

	CString omTopViewName;
	CString omEvalViewName;

	bool bmAutoCovIsActive;

	CString omSplName;
	long lmSdgu ;
	bool bmSdgChanged;
	bool bmIsRealRoster;
	bool bmEvulationFilterChanged;
// Internal data processing routines
private:
	int GetCompressedSdtCount(SDTDATA *prpSdt,bool bpUseFactor);
	void PrepareGrouping();
	void PrepareFilter();
	void PrepareSorter();

	void PrepareTSRData();

	bool IsBasicShiftValid(BASICSHIFTS *prpShift);
	void ChangeBsdBar(BASICSHIFTS *prpShift,COV_BARDATA *prpBar);

	bool IsPassBshFilter(BASICSHIFTS * prlBsh);
	bool IsPassSdtFilter(SDTDATA *prpSdt);
	bool IsPassDemFilter(DEMDATA *prlDem);
	bool IsSdtVisible(SDTDATA *prpSdt);
	int CompareGroup(COV_GROUPDATA *prpGroup1, COV_GROUPDATA *prpGroup2);
	int CompareBshLine(COV_LINEDATA *prpLine1, COV_LINEDATA *prpLine2);
	int CompareSdtLine(COV_LINEDATA *prpLine1, COV_LINEDATA *prpLine2);

	void PositionBsdBars();

	void MakeGroups();
	void MakeLines();
	void MakeLines(int ipGroupno);
	void MakeBars();
	void MakeBars(int ipGroupno);

	void MakeBshFromBsdData(BASICSHIFTS *prpBsh, BSDDATA *prpBsd);
	void MakeSdtBarData(COV_BARDATA *prlBar, SDTDATA *prpSdt, int ipCount = 0);
	void MakeBshBarData(COV_BARDATA *prlBar, BASICSHIFTS *prpBsh);
	void MakeBshBar(int ipGroupno, int ipLineno, BASICSHIFTS *prpBsh);
	void MakeSdtBar(int ipGroupno, int ipLineno, SDTDATA *prpSdt, int ipCount = 0);
	void MakeBshLine(BASICSHIFTS *prpBsh);//basi Shifts
	void MakeEmptyLine();
	void MakeBsdEmptyLine();
	int	 MakeSdtLine(SDTDATA *prpSdt); //Demands
	CString GroupText(int ipGroupNo);
	CString LineBshText(BASICSHIFTS *prpBsh);
	CString BarBshText(BASICSHIFTS *prpBsh);
	CString StatusBshText(BASICSHIFTS *prpBsh);
	bool FindBshLine(int ipGroupNo, int &ripLineno, long lpUrno);
	bool FindBshLinesForBsd(int ipGroupNo, CUIntArray &ropLines, CUIntArray &ropBars, long lpUrno);
	bool FindSdtLine(int ipGroupNo, int &ripLineno, long lpUrno);
	bool FindSdtBarWithCode(int ipGroupNo, SDTDATA *prpSdt,int &ripLineno);
	BOOL FindDutyBar(long lpUrno, int &ripGroupno, int &ripLineno);
	//bool FindDutyBars(long lpUrno, CUIntArray &ropGroups, CUIntArray &ropLines);



// Operations
private:
	int omGeometryFontIndex;
	CTimeSpan omGeometryTimeSpan;
	bool bmGeometryTimeLines;
	bool bmGeometryMinimize;
	CTime omStartShowTime;
	int   imGeometrieHours;
	CString omPfcCodeList;
	CString omQualiCodeList;
	CUIntArray omGhsUrnos;
	CUIntArray omPfcUrnos;

	FilterType imViewerFiterIdx;
	CString	   omViewerFilterEquipmentType;	

	int   imMinVal;
	int   imMaxVal;

///Variablen f�r AutoCoverage
	CCSPtrArray <BASESHIFT> omAutoCovShifts;
    CBaseShiftsGroups omBaseShiftGroup;

	CCSPtrArray <CAutoCoverage> omAutoCovArray;




	CMapPtrToPtr omFlightDemMap;
	CMapStringToPtr omTplTsrMap;
	
	CCSPtrArray <CString> omTplTsrArray;

	CStringArray omTotalTsrArray;

	CStringArray omFilterTsrArray;

////Variablen f�r PSFlightPage
	bool	bmArrival;
	bool	bmDepature;
	bool	bmBetrieb;
	bool	bmPlanung;
	bool	bmPrognose;
	bool	bmCancelled;
	bool	bmNoop;

////Variablen f�r PSAptPage
	bool	bmOrigin;
	bool	bmDestination;
	CMapStringToPtr omCMapForApt;		// Origin/Destination
	bool    bmUseAllApt;

	bool    bmCalcBsdBars;
////Variablen f�r PSAltPage
	CMapStringToPtr omCMapForAlt;		// Airline Types 
	bool    bmUseAllAlt;

////Variablen f�r PSTemplatePage
	bool	bmNotFlug;
	bool	bmFlug;
	bool	bmCCI;
	CMapStringToPtr omCMapForTemplate;		// Template 
	bool    bmUseAllTemplate;

////Variablen f�r PSAlcPage
	CMapStringToPtr omCMapForAct;		// Aircraft Code 
	bool    bmUseAllAct;

////Variablen f�r PSPosPage
	CMapStringToPtr omCMapForPos;		// Origin/Destination
	bool    bmUseAllPos;


////////Bewertung

////Variablen f�r PfcPage
	CStringArray omCMapForPrf;		// Funktionen
	bool    bmUseAllPrf;

////Variablen f�r GhsPage
	CStringArray omCMapForRpq;		// Qualifikationen
	bool    bmUseAllRpq;

////Variablen f�r ReqPage
	CStringArray omCMapForReq;		// Equipment
	bool    bmUseAllReq;

////Variablen f�r CicPage
	CStringArray omCMapForCic;		// Checkin Counter
	bool    bmUseAllCic;

////Variablen f�r PstPage
	CStringArray omCMapForPst;		// Positionen
	bool    bmUseAllPst;

////Variablen f�r GatPage
	CStringArray omCMapForGat;		// Gates
	bool    bmUseAllGat;

/// Variablen Checkboxen Personal/Location/Equipment

////Variablen f�r Basisschichten Filter
	CMapStringToPtr omCMapForBsh;		// Origin/Destination
	bool    bmUseAllBsh;

	CMapStringToPtr omShiftNameMap;
	CCSPtrArray<CString> omShiftNameUrnoArray;



	int imBorderVal;

	bool bmIsBorderFix;
	bool bmActuellState;
public:
    int GetGroupCount();
    COV_GROUPDATA *GetGroup(int ipGroupno);
    CString GetGroupText(int ipGroupno);
    CString GetGroupTopScaleText(int ipGroupno);
	int GetGroupColorIndex(int ipGroupno,CTime opStart,CTime opEnd);
    int GetLineCount(int ipGroupno);
	int GetNextFreeSdtLine();
    COV_LINEDATA *GetLine(int ipGroupno, int ipLineno);
    CString GetLineText(int ipGroupno, int ipLineno);
    int GetMaxOverlapLevel(int ipGroupno, int ipLineno);
	int GetVisualMaxOverlapLevel(int ipGroupno, int ipLineno);
    int GetBkBarCount(int ipGroupno, int ipLineno);
    COV_BKBARDATA *GetBkBar(int ipGroupno, int ipLineno, int ipBkBarno);
    CString GetBkBarText(int ipGroupno, int ipLineno, int ipBkBarno);
    int GetBarCount(int ipGroupno, int ipLineno);
    COV_BARDATA *GetBar(int ipGroupno, int ipLineno, int ipBarno);
    CString GetBarText(int ipGroupno, int ipLineno, int ipBarno);
    CString GetStatusBarText(int ipGroupno, int ipLineno, int ipBarno);
	int GetIndicatorCount(int ipGroupno, int ipLineno, int ipBarno);
	int GetDelegationCount(int ipGroupno, int ipLineno, int ipBarno);
	int GetDelegation2Count(int ipGroupno, int ipLineno, int ipBarno);
	int GetDelegation3Count(int ipGroupno, int ipLineno, int ipBarno);
	COV_INDICATORDATA *GetIndicator(int ipGroupno, int ipLineno, int ipBarno,
		int ipIndicatorno);
	COV_DELEGATIONDATA *GetDelegation(int ipGroupno, int ipLineno, int ipBarno,
		int ipIndicatorno);

 	COV_DELEGATIONDATA *GetDelegation2(int ipGroupno, int ipLineno, int ipBarno,
		int ipIndicatorno);

	COV_DELEGATIONDATA *GetDelegation3(int ipGroupno, int ipLineno, int ipBarno,
		int ipIndicatorno);

 
    void DeleteAll();
    int CreateGroup(COV_GROUPDATA *prpGroup);
    void DeleteGroup(int ipGroupno);
	void DeleteGroupLines();
	void DeleteGroupLines(int ipGroupno);

    int CreateBshLine(int ipGroupno, COV_LINEDATA *prpLine);
	int CreateSdtLine(int ipGroupno, COV_LINEDATA *prpLine);
    void DeleteLine(int ipGroupno, int ipLineno);
    int CreateBkBar(int ipGroupno, int ipLineno, COV_BKBARDATA *prpBkBar);
    void DeleteBkBar(int ipGroupno, int ipLineno, int ipBkBarno);
    int CreateBar(int ipGroupno, int ipLineno, COV_BARDATA *prpBar, BOOL bpFrontBar = TRUE);
    void DeleteBar(int ipGroupno, int ipLineno, int ipBarno);
    int CreateIndicator(int ipGroupno, int ipLineno, int ipBarno,
    	COV_INDICATORDATA *prpIndicator);
    int CreateDelegation(int ipGroupno, int ipLineno, int ipBarno,
    	COV_DELEGATIONDATA *prpIndicator);
   int CreateDelegation2(int ipGroupno, int ipLineno, int ipBarno,
    	COV_DELEGATIONDATA *prpIndicator);
   int CreateDelegation3(int ipGroupno, int ipLineno, int ipBarno,
    	COV_DELEGATIONDATA *prpIndicator);
	void DeleteIndicator(int ipGroupno, int ipLineno, int ipBarno, int ipIndicator);

	void DeleteDelegation(int ipGroupno, int ipLineno, int ipBarno, int ipIndicator);
	void DeleteDelegation2(int ipGroupno, int ipLineno, int ipBarno, int ipIndicator);
	void DeleteDelegation3(int ipGroupno, int ipLineno, int ipBarno, int ipIndicator);

    int GetBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2,
							int ipOverlapLevel1, int ipOverlapLevel2);
	int GetBkBarnoFromTime(int ipGroupno, int ipLineno, CTime opTime1, CTime opTime2);
	void AllowUpdates(BOOL bpNoUpdatesNow);
	int GetGeometryFontIndex(){return omGeometryFontIndex;}
	CTimeSpan GetGeometryTimeSpan(){return omGeometryTimeSpan;}
	void SetGeometryTimeSpan(CTimeSpan opGeometryTimeSpan);
	bool GetGeometryTimeLines(){return bmGeometryTimeLines;}
	bool GetGeometryMinimize(){return bmGeometryMinimize;}
	CTime GetGeometrieStartTime(){return omStartShowTime;}
	void MakeMasstab();

	int GetMaxVal();
	int GetMinVal();
	int GetBorderVal();
	bool IsBorderFix();
	void SetBorderFix(bool bpIsFix);
	void SetBorderVal(int ipVal);
	void SetMaxVal(int ipVal);
	void SetMinVal(int ipVal);
	BOOL FindBar(long lpUrno, int &ripGroupno, int &ripLineno, int &ripBarno);
	bool FindBarGlobal(long lpUrno, int &ripGroupno, int &ripLineno, int &ripBarno);
	bool GetTimeFrameFromTo(CTime &ropFrom, CTime &ropTo);
	void CalculateDailyBasicShifts();
	void SetMaxView(bool bpMaxView);
	bool GetAdditiv();
	void SetAdditiv(bool bpAdditiv);
	bool GetMaxView();
	void SetIgnoreBreak(bool bpIgnoreBreak);
	bool GetIgnoreBreak();
	bool GetShiftRoster();
	void SetShiftRoster(bool bpShiftRoster);

	SDGDATA *GetCurrentDemand(int ipGroup);
	void SetDemandName(int ipGroup, CString opDemandName);
	CString GetCurrentDemandName(int ipGroup);
	
	void SetDemandLevel(int ipGroup, int ipDemandLevel);	
	int GetCurrentDemandLevel(int ipGroup);

	void SetShiftView(int ipGroup, int ipShiftView);
	int GetShiftView(int ipGroup);
	bool GetTimeFrameTimes(CTime &opFrom,CTime &opTo);
//DDX-Processing
	void ProcessBsdNew(BSDDATA *prpBsd);
	void ProcessBsdChange(BSDDATA *prpBsd);
	void ProcessBsdDelete(BSDDATA *prpBsd);

	void ProcessDrrNew(DRRDATA *prpDrr,bool bpNotifyGraph = true);
	bool ProcessDrrChange(DRRDATA *prpDrr,bool bpNotifyGraph = true);
	void ProcessDrrDelete(DRRDATA *prpDrr,bool bpNotifyGraph = true);

	bool ProcessGspChange(RecordSet *prpDrr,bool bpNotifyGraph = true);
	void ProcessGspDelete(RecordSet *prpDrr,bool bpNotifyGraph = true);

	bool ProcessGplChange(RecordSet *prpDrr,bool bpNotifyGraph = true);
	void ProcessGplDelete(RecordSet *prpDrr,bool bpNotifyGraph = true);

	bool ProcessSplChange(RecordSet *prpDrr,bool bpNotifyGraph = true);
	void ProcessSplDelete(RecordSet *prpDrr,bool bpNotifyGraph = true);

	bool ProcessDrrRelEnd(void *vpDataPointer);
	bool ProcessDrrRelease(void *vpDataPointer);
	bool ProcessDrrReleasePbo(void *vpDataPointer);
	bool ProcessAttachment(CString &ropAttachment);
	void ProcessDrrNew(char *pcpFields, char *pcpData);
	bool ProcessDrrChange(long lpUrno,char *pcpFields, char *pcpData);
	void ProcessDrrDelete(char *pcpFields, char *pcpData);

	void ProcessMsdDelete(MSDDATA *prpMsd,bool bpFromSelf);
	void ProcessMsdUpdate(MSDDATA *prpMsd,bool bpFromSelf);
	void ProcessMsdNew   (MSDDATA *prpMsd,bool bpFromSelf);


	void ProcessSdgNew(SDGDATA *prpSdg);
	void ProcessSdgChange(SDGDATA *prpSdg);
	void ProcessSdgDelete(SDGDATA *prpSdg);
	void ProcessSdgRelease(SDGDATA *prpSdg);
	
	void ProcessSdtChange(SDTDATA *prpSdt);
	void ProcessSdtSelChange(SDTDATA *prpSdt);
	void ProcessSdtDelete(SDTDATA *prpSdt);

	void ProcessDemCflReady(int ipDDXType);
	void ProcessDemChanged(DEMDATA *prpDem);
	bool ProcessDemRelease(void *vpDataPointer);
	bool ProcessDemUpdDem(void *vpDataPointer);
	
	void ProcessMsdRelease(SDGDATA *prpSdg);


	void SetSdtFlag(bool bpFlag = true)
	{
		bmSdtChanged = bpFlag;
	}
	void SetReCalcShifts(bool bpFlag = true)
	{
		bmReCalcShifts = bpFlag;
	}
	
	bool IsGhdChanged();
	bool IsSdtChanged();
	bool ReCalcShifts();


	bool IsGhsChanged();
	void LoadGhdWindow();
	void LoadSdtWindow(bool bpFlag = false);
	void ResetFlags();

	void SetActuellState(bool bpState);
	bool GetActuellState();
//FSC test insert
	void FilterFlights();
	bool FilterDemand(DEMDATA *prpDem,bool bpUpdateAllowed);	
	bool DeleteDemand(DEMDATA *prpDem,bool bpUpdateAllowed);

	bool IsPassFilter(FLIGHTDATA *prpFlight);

	bool DemUpdateAllowed();
	void ReloadAllDems();

	CTimeSpan	CalculateWorktime(CTime opFrom,CTime opTo);
	bool		IsPassSdtFilterWorktime(SDTDATA *prpSdt);

// Private helper routines
private:
	void GetOverlappedBarsFromTime(int ipGroupno, int ipLineno,
		CTime opTime1, CTime opTime2, CUIntArray &ropBarnoList);
	void GetGroupsFromViewer(CCSPtrArray<COV_GROUPDATA> &ropGroups);
	bool DeleteFromSdtData(SDTDATA *prpSdt);

	
	bool GetDebugBaseShiftsFilter(CString opViewName,CMapStringToPtr& ropCMapForBsh);

// Attributes used for filtering condition
private:
	int omGroupBy;			// enumerated value -- define in "stviewer.cpp"

	

	CStringArray omSortOrder;
	CMapStringToPtr omCMapForPool;
	CMapStringToPtr omCMapForTeam;
	CMapStringToPtr omCMapForShiftCode;
	CMapStringToPtr omCMapForRank;
	CTime omStartTime;
	CTime omEndTime;
	CTime omFrameStart;
	CTime omFrameEnd;
	bool bmLoadGhds;
	bool bmSdtChanged;
	bool bmRosChanged;
	bool bmReCalcShifts;
// Attributes
private:
	CWnd *pomAttachWnd;
	CCoverageGraphicWnd *pomCoverageWnd;
	CBrush omBkBrush;
	CBrush omBreakBrush;
	CBrush omBkBreakBrush;
	CBrush omWIFBkBrush;
    CCSPtrArray<COV_GROUPDATA> omGroups;

// Methods which handle changes (from Data Distributor)
private:
	BOOL bmNoDemUpdates;		// no demand updates allowed for the moment
private:
	BOOL bmNoUpdatesNow;
	BOOL bmIsFirstGroupOnPage;
	bool bmMaxView;
	bool bmAdditiv;
	bool bmIgnoreBreak;
	//bool bmShiftRoster;
	CTime omMarkTimeStart;
	CTime omMarkTimeEnd;

	CTime omLoadTimeStart;
	CTime omLoadTimeEnd;

	CStringArray omShiftNameArray;
///////////////////////////////////////////////////////////////////////////////////////////
// Printing routines
private:
//	CCSPrint *pomPrint;
	bool bmIsGhsChanged;
	bool bmShiftRoster;
	/*
	void PrintStaffDiagramHeader(int ipGroupno);
	void PrintPrepareLineData(int ipGroupNo,int ipLineno,CCSPtrArray<PRINTBARDATA> &ropPrintLine,
		CCSPtrArray<PRINTBARDATA> &ropBkBars);
	void PrintPool(CPtrArray &opPtrArray,int ipGroupNo);
	*/
public:
	
//	void PrintDiagram(CPtrArray &opPtrArray);
};

/////////////////////////////////////////////////////////////////////////////

#endif
