#if !defined(AFX_DELSDGDLG_H__D63DD9EE_B164_11D3_AB39_604F4EC19A15__INCLUDED_)
#define AFX_DELSDGDLG_H__D63DD9EE_B164_11D3_AB39_604F4EC19A15__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DelSdgDlg.h : Header-Datei
//
#include <resource.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDelSdgDlg 
class CGridFenster;

class CDelSdgDlg : public CDialog
{
// Konstruktion
public:
	CDelSdgDlg(const CString& ropCurrent,CWnd* pParent = NULL);   // Standardkonstruktor
	~CDelSdgDlg();
	BOOL GetSelection(CStringArray &ropNamList);
	
// Dialogfelddaten
	//{{AFX_DATA(CDelSdgDlg)
	enum { IDD = IDD_DELSDG_DLG };
		// HINWEIS: Der Klassen-Assistent fügt hier Datenelemente ein
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CDelSdgDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	CGridFenster*	pomSdgList;
	CString			omCurrent;
	CStringArray	omSelected;

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CDelSdgDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_DELSDGDLG_H__D63DD9EE_B164_11D3_AB39_604F4EC19A15__INCLUDED_
