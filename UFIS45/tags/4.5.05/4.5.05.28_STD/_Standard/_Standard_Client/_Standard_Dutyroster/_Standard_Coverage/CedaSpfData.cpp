// CedaSpfData.cpp
 
#include <stdafx.h>
#include <CedaSpfData.h>


void ProcessSpfCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

static int CompareTimes(const SPFDATA **e1, const SPFDATA **e2);
static int CompareSurnAndTimes(const SPFDATA **e1, const SPFDATA **e2);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaSpfData::CedaSpfData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for SPFDataStruct
	BEGIN_CEDARECINFO(SPFDATA,SPFDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_LONG		(Surn,"SURN")
		FIELD_INT		(Prio,"PRIO")
		FIELD_CHAR_TRIM	(Code,"CODE")
		FIELD_OLEDATE	(Vpfr,"VPFR")
		FIELD_OLEDATE	(Vpto,"VPTO")
	END_CEDARECINFO //(SPFDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(SPFDataRecInfo)/sizeof(SPFDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SPFDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"SPF");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmListOfFields,"URNO,SURN,PRIO,CODE,VPFR,VPTO");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------


//--REGISTER----------------------------------------------------------------------------------------------

void CedaSpfData::Register(void)
{
	ogDdx.Register((void *)this,BC_SPF_CHANGE,	CString("SPFDATA"), CString("Spf-changed"),	ProcessSpfCf);
	ogDdx.Register((void *)this,BC_SPF_NEW,		CString("SPFDATA"), CString("Spf-new"),		ProcessSpfCf);
	ogDdx.Register((void *)this,BC_SPF_DELETE,	CString("SPFDATA"), CString("Spf-deleted"),	ProcessSpfCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaSpfData::~CedaSpfData(void)
{
	TRACE("CedaSpfData::~CedaSpfData called\n");
	ClearAll();
	omRecInfo.DeleteAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaSpfData::ClearAll(bool bpWithRegistration)
{
	TRACE("CedaSpfData::ClearAll called\n");
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}

    omUrnoMap.RemoveAll();
    omData.DeleteAll();
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaSpfData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction2("RT");
	}
	else
	{
		ilRc = CedaAction2("RT", pspWhere);
	}
	if (ilRc != true)
	{
		TRACE("Read-Spf: Ceda-Error %d \n",ilRc);
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		SPFDATA *prlSpf = new SPFDATA;
		if ((ilRc = GetFirstBufferRecord2(prlSpf)) == true)
		{
			omData.Add(prlSpf);//Update omData
			omUrnoMap.SetAt((void *)prlSpf->Urno,prlSpf);
		}
		else
		{
			delete prlSpf;
		}
	}
	TRACE("Read-Spf: %d gelesen\n",ilLc-1);
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaSpfData::Insert(SPFDATA *prpSpf)
{
	prpSpf->IsChanged = DATA_NEW;
	if(Save(prpSpf) == false) return false; //Update Database
	InsertInternal(prpSpf);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaSpfData::InsertInternal(SPFDATA *prpSpf)
{
	ogDdx.DataChanged((void *)this, SPF_NEW,(void *)prpSpf ); //Update Viewer
	omData.Add(prpSpf);//Update omData
	omUrnoMap.SetAt((void *)prpSpf->Urno,prpSpf);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaSpfData::Delete(long lpUrno)
{
	SPFDATA *prlSpf = GetSpfByUrno(lpUrno);
	if (prlSpf != NULL)
	{
		prlSpf->IsChanged = DATA_DELETED;
		if(Save(prlSpf) == false) return false; //Update Database
		DeleteInternal(prlSpf);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaSpfData::DeleteInternal(SPFDATA *prpSpf)
{
	ogDdx.DataChanged((void *)this,SPF_DELETE,(void *)prpSpf); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpSpf->Urno);
	int ilSpfCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilSpfCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpSpf->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaSpfData::Update(SPFDATA *prpSpf)
{
	if (GetSpfByUrno(prpSpf->Urno) != NULL)
	{
		if (prpSpf->IsChanged == DATA_UNCHANGED)
		{
			prpSpf->IsChanged = DATA_CHANGED;
		}
		if(Save(prpSpf) == false) return false; //Update Database
		UpdateInternal(prpSpf);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaSpfData::UpdateInternal(SPFDATA *prpSpf)
{
	SPFDATA *prlSpf = GetSpfByUrno(prpSpf->Urno);
	if (prlSpf != NULL)
	{
		*prlSpf = *prpSpf; //Update omData
		ogDdx.DataChanged((void *)this,SPF_CHANGE,(void *)prlSpf); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

SPFDATA *CedaSpfData::GetSpfByUrno(long lpUrno)
{
	SPFDATA  *prlSpf;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlSpf) == TRUE)
	{
		return prlSpf;
	}
	return NULL;
}

//---------------------------------------------------------------------------------------------------------

void CedaSpfData::GetSpfBySurnWithTime(long lpSurn,CTime opStart,CTime opEnd,CCSPtrArray<SPFDATA> *popSpfData)
{
	SPFDATA  *prlSpf;
	COleDateTime olStart,olEnd;

	popSpfData->DeleteAll();
	
	CCSPtrArray<SPFDATA> olSpfData;
	POSITION rlPos;
	for ( rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
	{
		long llUrno;
		omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSpf);
		if((prlSpf->Surn == lpSurn))
		{
			olSpfData.Add(prlSpf); 
		}
	}
	int ilSize = olSpfData.GetSize();
	if(ilSize > 0)
	{
		olSpfData.Sort(CompareTimes);
		
		olStart.SetDateTime(opStart.GetYear(),opStart.GetMonth(),opStart.GetDay(),opStart.GetHour(),opStart.GetMinute(),opStart.GetSecond());
		olEnd.SetDateTime(opEnd.GetYear(),opEnd.GetMonth(),opEnd.GetDay(),opEnd.GetHour(),opEnd.GetMinute(),opEnd.GetSecond());
		SPFDATA *prlPrevSpf=NULL;
		for(int i=ilSize;--i>=0;)
		{
			prlSpf = &olSpfData[i];
			
			if(prlSpf->Vpfr<olEnd)
			{
				switch(prlSpf->Vpto.GetStatus())
				{
				case COleDateTime::valid:
					if(prlSpf->Vpto>olStart)
					{
						popSpfData->Add(prlSpf);
					}
					else
					{
						i=0;
					}
					break;
				case COleDateTime::null:
					if(prlPrevSpf!=NULL)
					{
						if(prlPrevSpf->Vpfr>olStart)
						{
							popSpfData->Add(prlSpf);
						}
						else
						{
							i=0;
						}
					}
					else
					{
						popSpfData->Add(prlSpf);
					}
					break;
				default:
					break;
				}
			}
			prlPrevSpf = prlSpf;
		}
	}
}

//---------------------------------------------------------------------------------------

CString CedaSpfData::GetPfcBySurnWithTime(long lpSurn, COleDateTime opDate)
{
	CString olPfcCode = "";

	int ilPrio = 30;
	CTime olDay(opDate.GetYear(),opDate.GetMonth(),opDate.GetDay(),0,0,0);
	CCSPtrArray<SPFDATA> olSpfData;

	GetSpfBySurnWithTime(lpSurn, olDay, olDay, &olSpfData);
	int ilSpfSize = olSpfData.GetSize();

	for(int i=0; i<ilSpfSize; i++)
	{
		if(olSpfData[i].Vpfr.GetStatus() == COleDateTime::valid)
		{
			if(olSpfData[i].Vpfr <= opDate)
			{
				switch(olSpfData[i].Vpto.GetStatus())
				{
				case COleDateTime::valid:
					if(olSpfData[i].Vpto > opDate)
					{
						if(olSpfData[i].Prio < ilPrio)
						{
							olPfcCode = olSpfData[i].Code;
							ilPrio = olSpfData[i].Prio;
						}
					}
					break;
				default:
						olPfcCode = olSpfData[i].Code;
					break;
				}
			}
		}
	}
	olSpfData.RemoveAll();
	return olPfcCode;
}

//---------------------------------------------------------------------------------------------------------

CString CedaSpfData::GetSpfByPfcWithTime(CStringArray *popPfc, COleDateTime opStart, COleDateTime opEnd, CCSPtrArray<SPFDATA> *popSpfData)
{
	popSpfData->DeleteAll();
	CString olUrnos = "";

	if(popPfc->GetSize() > 0 && opStart.GetStatus() == COleDateTime::valid && opEnd.GetStatus() == COleDateTime::valid && opStart <= opEnd)
	{
		CString olTmpCode;
		COleDateTime olTmpStart,olTmpEnd;
		CMapStringToPtr olPfcMap;
		SPFDATA *prlSpf = NULL;


		for(int i=0; i<popPfc->GetSize(); i++)
		{
			olPfcMap.SetAt(popPfc->GetAt(i),NULL);
		}

		CCSPtrArray<SPFDATA> olSpfData;
		POSITION rlPos;
		for(rlPos = omUrnoMap.GetStartPosition(); rlPos != NULL; )
		{
			long llUrno;
			omUrnoMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlSpf);
			olSpfData.Add(prlSpf); 
		}

		olSpfData.Sort(CompareSurnAndTimes);
		int ilSize = olSpfData.GetSize();
		SPFDATA *prlNextSpf = NULL;
		void  *prlVoid = NULL;

		for(i = 0; i < ilSize; i++)
		{
			prlSpf = &olSpfData[i];
			prlNextSpf = NULL;
			if((i+1) < ilSize)
			{
				if(prlSpf->Surn == olSpfData[i+1].Surn)
					prlNextSpf = &olSpfData[i+1];
			}

			if(prlSpf->Vpfr.GetStatus() == COleDateTime::valid)
			{
				if(prlSpf->Vpfr<=opEnd)
				{
					switch(prlSpf->Vpto.GetStatus())
					{
					case COleDateTime::valid:
						if(prlSpf->Vpto>=opStart)
						{
							olTmpCode.Format("%s",prlSpf->Code);
							if(olPfcMap.Lookup(olTmpCode,(void *&)prlVoid) == TRUE)
							{
								popSpfData->Add(prlSpf);
							}
						}
						break;
					case COleDateTime::null:
						if(prlNextSpf != NULL)
						{
							if(prlNextSpf->Vpfr>=opStart)
							{
								olTmpCode.Format("%s",prlSpf->Code);
								if(olPfcMap.Lookup(olTmpCode,(void *&)prlVoid) == TRUE)
								{
									popSpfData->Add(prlSpf);
								}
							}
						}
						else
						{
							olTmpCode.Format("%s",prlSpf->Code);
							if(olPfcMap.Lookup(olTmpCode,(void *&)prlVoid) == TRUE)
							{
								popSpfData->Add(prlSpf);
							}
						}
						break;
					default:
						break;
					}
				}
			}
		}
		// Create Stf-Urno-List for return value
		ilSize = popSpfData->GetSize();
		olUrnos = ",";
		for(i = 0; i < ilSize; i++)
		{
			CString olTmpUrno;
			olTmpUrno.Format(",%ld,",(*popSpfData)[i].Surn);
			if(olUrnos.Find(olTmpUrno) == -1)
			{
				olUrnos += olTmpUrno.Mid(1);
			}
		}
		if(olUrnos.IsEmpty() == FALSE)
		{
			olUrnos = olUrnos.Mid(1,olUrnos.GetLength()-2);
		}
		olPfcMap.RemoveAll();
	}
	return olUrnos;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaSpfData::ReadSpecialData(CCSPtrArray<SPFDATA> *popSpf,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT",pcmTableName,pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popSpf != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			SPFDATA *prpSpf = new SPFDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpSpf,CString(pclFieldList))) == true)
			{
				popSpf->Add(prpSpf);
			}
			else
			{
				delete prpSpf;
			}
		}
		if(popSpf->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaSpfData::Save(SPFDATA *prpSpf)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpSpf->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpSpf->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpSpf);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpSpf->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSpf->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSpf);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpSpf->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSpf->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
	TRACE("Spf-IRT/URT/DRT: Ceda-Return %d \n",ilRc);
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessSpfCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	BC_TRY
		ogSpfData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
	BC_CATCH_ALL
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaSpfData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlSpfData;
	prlSpfData = (struct BcStruct *) vpDataPointer;
	SPFDATA *prlSpf;
	if(ipDDXType == BC_SPF_NEW)
	{
		prlSpf = new SPFDATA;
		GetRecordFromItemList(prlSpf,prlSpfData->Fields,prlSpfData->Data);
		InsertInternal(prlSpf);
	}
	if(ipDDXType == BC_SPF_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlSpfData->Selection);
		prlSpf = GetSpfByUrno(llUrno);
		if(prlSpf != NULL)
		{
			GetRecordFromItemList(prlSpf,prlSpfData->Fields,prlSpfData->Data);
			UpdateInternal(prlSpf);
		}
	}
	if(ipDDXType == BC_SPF_DELETE)
	{
		long llUrno;
		CString olSelection = (CString)prlSpfData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlSpfData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlSpf = GetSpfByUrno(llUrno);
		if (prlSpf != NULL)
		{
			DeleteInternal(prlSpf);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------

static int CompareTimes(const SPFDATA **e1, const SPFDATA **e2)
{
	if((**e1).Vpfr>(**e2).Vpfr)
		return 1;
	else
		return -1;
}

//---------------------------------------------------------------------------------------------------------

static int CompareSurnAndTimes(const SPFDATA **e1, const SPFDATA **e2)
{
	int ilCompareResult = 0;

	     if((**e1).Surn>(**e2).Surn) ilCompareResult = 1;
	else if((**e1).Surn<(**e2).Surn) ilCompareResult = -1;

	if(ilCompareResult == 0)
	{
		     if((**e1).Vpfr>(**e2).Vpfr) ilCompareResult = 1;
		else if((**e1).Vpfr<(**e2).Vpfr) ilCompareResult = -1;
	}
	return ilCompareResult;
}

//---------------------------------------------------------------------------------------------------------
