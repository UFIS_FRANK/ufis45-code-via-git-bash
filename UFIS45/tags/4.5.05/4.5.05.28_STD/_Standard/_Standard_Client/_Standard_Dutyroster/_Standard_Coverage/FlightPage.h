#if !defined(AFX_FLIGHTPAGE_H__FABEF321_44A9_11D3_94F9_00001C018ACE__INCLUDED_)
#define AFX_FLIGHTPAGE_H__FABEF321_44A9_11D3_94F9_00001C018ACE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FlightPage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFlightPage dialog
#include <CCSGlobl.h>

class CFlightPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CFlightPage)

// Construction
public:
	CFlightPage();
	CFlightPage(PageType ipPageType,CTime opDate1,CTime opDate2) ;
	~CFlightPage();

	bool GetSearchText(CString &ropWhere,bool & bpIsSingleFlight); 
	void ResetDisplay();
	void RedisplayTime(bool bpUpdateDate = true);
	void SetSearchStartEnd(CTime opDate1, CTime opDate2,bool bpSearchIsOpen = true);

	void SetCaption(const char *pcpCaption);
	char pcmCaption[100];

	CString omFidStart;
	CString omFidEnd;
	bool bmCreateFid;
	bool bmCreateFdep;

// Dialog Data
	//{{AFX_DATA(CFlightPage)
	enum { IDD = IDD_FLIGHT_SEARCH_PAGE };
	CButton	m_Diverted;
	CEdit	m_FlukoTime;
	CEdit	m_FlukoDate;
	CButton	m_Depature;
	CButton	m_ZeitraumStatic;
	CStatic	m_VerkehrsArt;
	CEdit	m_TrafficType;
	CEdit	m_SToTime;
	CEdit	m_SToDate;
	CStatic	m_STimeTo;
	CStatic	m_STimeFrom;
	CEdit	m_SFromTime;
	CEdit	m_SFromDate;
	CEdit	m_Rema;
	CEdit	m_Regn;
	CStatic	m_Registration;
	CButton	m_Planung;
	CButton	m_Noop;
	CButton	m_Fid;
	CButton	m_FlightDep;
	CEdit	m_LstuTime;
	CButton	m_LstuStatic;
	CStatic	m_LstuFrom;
	CEdit	m_LstuDate;
	CButton	m_FTypStatic;
	CButton	m_FlukoStatic;
	CStatic	m_FlukoFrom;
	CEdit	m_FlightNr;
	CStatic	m_FlightCode;
	CEdit	m_FAirline;
	CButton	m_Cancelled;
	CButton	m_Betrieb;
	CStatic	m_Bemerkung;
	CButton	m_Arrival;
	CButton	m_Zeitraum;
	CStatic	m_Airport;
	CEdit	m_Airp;
	CEdit	m_Airp2;
	CStatic	m_ActType;
	CEdit	m_Act3;
	CEdit	m_FlightSuffix;
	CButton	m_FlugStatic;
	CString	m_FlightSuffixVal;
	CString	m_Act3Val;
	CString	m_AirpVal;
	CString	m_Airp2Val;
	BOOL	m_ZeitraumVal;
	BOOL	m_ArrivalVal;
	BOOL	m_BetriebVal;
	BOOL	m_CancelledVal;
	CString	m_FAirlineVal;
	CString	m_FlightNrVal;
	CString	m_LstuDateVal;
	CString	m_LstuTimeVal;
	BOOL	m_NoopVal;
	BOOL	m_FidVal;
	BOOL	m_FlightDepVal;
	BOOL	m_PlanungVal;
	CString	m_RegnVal;
	CString	m_RemaVal;
	CString	m_SFromDateVal;
	CString	m_SFromTimeVal;
	CString	m_SToDateVal;
	CString	m_SToTimeVal;
	CString	m_TrafficTypeVal;
	BOOL	m_DepatureVal;
	CString	m_FlukoDateVal;
	CString	m_FlukoTimeVal;
	BOOL	m_DivertedVal;
	BOOL	m_RuleDiagnosis;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CFlightPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CFlightPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnZeitraum();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	PageType imPageType;

	CTime omSearchStart;
	CTime omSearchEnd;

	

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FLIGHTPAGE_H__FABEF321_44A9_11D3_94F9_00001C018ACE__INCLUDED_)
