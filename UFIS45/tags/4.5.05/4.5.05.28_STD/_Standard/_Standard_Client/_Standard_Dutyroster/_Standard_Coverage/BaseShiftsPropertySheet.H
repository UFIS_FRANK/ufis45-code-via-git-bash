// flplanps.h : header file
//



#ifndef _BASESHIFTSPROPERTYSHEET_H_
#define _BASESHIFTSPROPERTYSHEET_H_


#include <BasePropertySheet.h>
#include <PSBshFilterPage.h>


/////////////////////////////////////////////////////////////////////////////
// BaseShiftsPropertySheet
//@Man:
//@Memo: Property sheet for the basic shift filter
/*@Doc: 
*/
class BaseShiftsPropertySheet : public BasePropertySheet
{
	DECLARE_DYNAMIC(BaseShiftsPropertySheet)

// Construction
public:
	BaseShiftsPropertySheet(CString opCalledFrom,CString opCaption, CWnd* pParentWnd = NULL,
		CViewer *popViewer = NULL, UINT iSelectPage = 0);

// Attributes
public:
	CPsBshFilterPage	m_PsBshFilterPage;
	CFindReplaceDialog	*pomFindDlg;


// Operations
public:
	//@ManMemo:Handles the loading of the filter data into the property sheet  
    /*@Doc:
		Fills the Array m_PsBshFilterPage.SelectedItems with the items in the basic shift filter 
    */
	virtual void LoadDataFromViewer();
	//@ManMemo:Handles the saving of the filter data from the property sheet  
    /*@Doc:
		Writes the items from the Array m_PsBshFilterPage.SelectedItems into the basic shift filter 
		opViewName = name of the selected view(filter);
		bpSaveToDb = flag if the changes should be stored in the database or not.
    */
	virtual void SaveDataToViewer(CString opViewName,BOOL bpSaveToDb = TRUE);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(BaseShiftsPropertySheet)
	public:
	virtual BOOL OnInitDialog();
	virtual BOOL DestroyWindow();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(BaseShiftsPropertySheet)
	afx_msg void OnFind();
	afx_msg LRESULT OnFindMsg(WPARAM, LPARAM);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

/////////////////////////////////////////////////////////////////////////////

#endif // _BaseShiftsPROPERTYSHEET_H_
