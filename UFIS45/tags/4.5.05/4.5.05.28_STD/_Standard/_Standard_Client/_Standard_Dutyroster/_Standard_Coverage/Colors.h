#if !defined(AFX_COLORS_H__27A161D8_9D2F_11D6_820D_00010215BFDE__INCLUDED_)
#define AFX_COLORS_H__27A161D8_9D2F_11D6_820D_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Colors.h : header file
//



/////////////////////////////////////////////////////////////////////////////
// Colors command target

class Colors : public CCmdTarget
{
	DECLARE_DYNCREATE(Colors)

	Colors();           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Colors)
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~Colors();

	// Generated message map functions
	//{{AFX_MSG(Colors)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COLORS_H__27A161D8_9D2F_11D6_820D_00010215BFDE__INCLUDED_)
