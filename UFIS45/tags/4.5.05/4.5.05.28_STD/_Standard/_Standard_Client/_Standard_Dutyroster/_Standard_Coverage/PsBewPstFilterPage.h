#ifndef AFX_PSBEWPSTFILTERPAGE_H__495C9510_A2E4_11D1_BD3B_0000B4392C49__INCLUDED_
#define AFX_PSBEWPSTFILTERPAGE_H__495C9510_A2E4_11D1_BD3B_0000B4392C49__INCLUDED_

// PsTemplateFilterPage.h : Header-Datei
//
#include <GridFenster.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CPsBewPstFilterPage 

class CPsBewPstFilterPage : public CPropertyPage
{
	DECLARE_DYNCREATE(CPsBewPstFilterPage)

// Konstruktion
public:
	CPsBewPstFilterPage();
	~CPsBewPstFilterPage();

	void SetCaption(const char *pcpCaption);
	char pcmCaption[100];


// Dialogfelddaten
	//{{AFX_DATA(CPsBewPstFilterPage)
	enum { IDD = IDD_PSFILTER_PAGE };
	CButton	m_RemoveButton;
	CButton	m_AddButton;
	CListBox	m_ContentList;
	CListBox	m_InsertList;
	//}}AFX_DATA
	int imColCount;
	int imHideColStart;

	bool blIsInit;

	CGridFenster *pomPossilbeList;
	CGridFenster *pomSelectedList;


// Überschreibungen
	// Der Klassen-Assistent generiert virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CPsBewPstFilterPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CPsBewPstFilterPage)
	afx_msg void OnButtonAdd();
	afx_msg void OnButtonRemove();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	CStringArray omPossibleItems;
	CStringArray omSelectedItems;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_PSFILTERPAGE_H__495C9510_A2E4_11D1_BD3B_0000B4392C49__INCLUDED_
