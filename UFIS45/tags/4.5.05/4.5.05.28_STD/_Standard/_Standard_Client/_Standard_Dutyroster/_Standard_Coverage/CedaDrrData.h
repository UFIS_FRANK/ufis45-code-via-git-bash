// CedaDrrData.h
 
#ifndef __CEDADRRDATA__
#define __CEDADRRDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------

struct DRRDATA 
{
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char	 Scod[8+2];	// Schichtcode
	CTime	 Avfr;		
	CTime	 Avto;
	CTime	 Sbfr;
	CTime	 Sbto;
	long	 Sblu;
	long	 Stfu;	
	char	 Ross[1+1];
	char	 Rosl[1+1];
	char	 Fctc[8+2];
	char	 Drsf[1+1];
	char	 Drrn[1+1];
	char	 Sday[8+2];
	long	 Bros;

	//DataCreated by this class
	int      IsChanged;

	DRRDATA(void)
	{
		memset(this,'\0',sizeof(*this));
		Avfr=-1;
		Avto=-1;
	}

}; // end DRRDataStruct

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaDrrData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr		 omUrnoMap;
    CCSPtrArray<DRRDATA> omData;

	char pcmListOfFields[2048];

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName);
	}

// Operations
public:
    CedaDrrData();
	~CedaDrrData();
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(const char *pspWhere = NULL);
	bool Insert(DRRDATA *prpDrr,bool bpSendDdx = true);
	bool InsertInternal(DRRDATA *prpDrr,bool bpSendDdx = true);
	bool Update(DRRDATA *prpDrr,bool bpSendDdx = true);
	bool UpdateInternal(DRRDATA *prpDrr,bool bpSendDdx = true);
	bool Delete(long lpUrno,bool bpSendDdx = true);
	bool DeleteInternal(DRRDATA *prpDrr,bool bpSendDdx = true);
	bool ReadSpecialData(CCSPtrArray<DRRDATA> *popDrr,const char *pspWhere,const char *pspFieldList,bool ipSYS=true);
	bool Save(DRRDATA *prpDrr);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	DRRDATA  *GetDrrByUrno(long lpUrno);
	bool Release();
	bool Synchronise(CString opUnknown, bool bpActive = false);

	// Private methods
private:
    void PrepareDrrData(DRRDATA *prpDrrData);
	friend class CoverageDiagramViewer;
};

//---------------------------------------------------------------------------------------------------------

extern CedaDrrData ogDrrData;

#endif //__CEDAPDRRDATA__
