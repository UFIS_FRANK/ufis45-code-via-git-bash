#ifndef _CDEMSD_H_
#define _CDEMSD_H_

//#include <afxwin.h>
#include <ccsglobl.h>
//#include <ccsobj.h>
#include <CCSPtrArray.h>
#include <CCSCedaData.h>
#include <ccsddx.h>
/////////////////////////////////////////////////////////////////////////////

#define DEM_I_DTYP_LEN			5
#define DEM_S_DTYP_DUTY				"D"
#define DEM_S_DTYP_NFDUTY			"U"
#define DEM_S_DTYP_BREAK			"B"
#define DEM_S_DTYP_DELEGATION		"A"
#define DEM_S_DTYP_SPECIAL_DUTY		"S"
#define DEM_S_DTYP_OTHER			"O"

#define DEM_S_DRTI_ARRIVAL			"A"
#define DEM_S_DRTI_DEPARTURE		"B"
#define DEM_S_DRTI_TURNAROUND		"D"

#define DEM_S_RTYP_PERSONELL		"P"

#define DEM_ATRN_LEN 12
#define DEM_DCHA_LEN 1
#define DEM_DCHS_LEN 1
#define DEM_DPOS_LEN 5
#define DEM_DRTI_LEN 1
#define DEM_DTYP_LEN 1
#define DEM_EACO_LEN 12
#define DEM_ISFD_LEN 1
#define DEM_KOST_LEN 20
#define DEM_LKCO_LEN 12
#define DEM_PERM_LEN 256
#define DEM_REMA_LEN 265
#define DEM_RTYP_LEN 1
#define DEM_STAT_LEN 10
#define DEM_SZEN_LEN 32
#define DEM_USEC_LEN 32
#define DEM_USEU_LEN 32
#define DEM_VRGC_LEN 256


class CJobsTab;

struct DEMDATA {
	char	 Aloc[12]; 
	CTime 	 Cdat; 	
	int 	 Dbar; 	
	int 	 Dear; 	
	CTime 	 Debe; 	
	CTime 	 Deen; 	
	int 	 Dedu; 	
	char 	 Dety[5]; 
	CTime 	 Eadb; 
	char 	 Hopo[5]; 
	CTime 	 Lade;
	long	 Ucon;	
	CTime 	 Lstu; 	// Datum letzte �nderung
	char 	 Obty[5]; 	
	long 	 Ouri; 	
	long 	 Ouro; 	
	int  	 Sdti; 
	int 	 Suti; 
	int 	 Tsdb; 
	int 	 Tsde; 
	int 	 Ttgf; 
	int 	 Ttgt; 
	long	 Urno;
	long	 Urud;
	char	 Flgs[11];		// Flag string 
							// where [0] '0' = Ouri/Ouro contains a flight URNO but are disactivated
							// where [1] '1' = demand has been expired
							// where [5] '1' = demand is deactivated for automatic assignment

	//Additionals
	int  BarBkColorIdx;        // Index in global Color Table for Backgroundcolor of the Bar
	int  BarTextColorIdx;      // Index in global Color Table for Textcolor of the Bar
	int  TriangelColorLeft;	   // Dreiecksfarbe im Balken links
	int  TriangelColorRight;	   // Dreiecksfarbe im Balken rechts
	int  TblTextColorIdx;		// Index in global Color Table for TextColor in NoAllocTable
	int  IsChanged;	// Check whether Data has Changed f�r Relaese

	bool IsPersonal;
	bool HasPfc;
	bool HasPer;
	bool IsLocation;
	bool HasCic;
	bool HasPos;
	bool HasGat;

	CTime 	 DispDebe; 	
	CTime 	 DispDeen; 	


	bool IsEquipment;
	bool HasReg;
	bool HasReq;
	long Ghsu;
	int  MinuteStart;
	char Rusn[18];
	int Rust;
	BOOL IsSelected;
	BOOL IsConflictAccepted;
	bool IsDetailMarked; 		
	DEMDATA(void)
	{ /*memset(&Key,'\0',sizeof(*this)-sizeof(Break));*/
		memset(this,'\0',sizeof(*this));
		Debe=TIMENULL;Deen=TIMENULL;
		DispDebe=TIMENULL;DispDeen=TIMENULL;
		Eadb=TIMENULL;Lade=TIMENULL;
		Cdat = CTime::GetCurrentTime();
		Lstu=CTime::GetCurrentTime();
		IsChanged=DATA_UNCHANGED;
		IsSelected = FALSE;IsConflictAccepted=FALSE;
		IsDetailMarked = false;
		IsPersonal = false;
		IsEquipment = false;
		IsLocation = false;
		BarBkColorIdx = SILVER_IDX;
		BarTextColorIdx = BLACK_IDX;
		TriangelColorLeft = -1;	   // Dreiecksfarbe im Balken links
		TriangelColorRight = -1;	   // Dreiecksfarbe im Balken rechts
	}
	bool operator==(DEMDATA s)
	{
		return Urno == s.Urno;
	};

	bool IsCommonCheckin()
	{
		if (this->Dety[0] == '6')
			return true;
		else
			return false;
	}
 };	
//typedef struct DemDataStruct DEMDATA;


// the broadcast CallBack function, has to be outside the CedaDemData class
void ProcessDemCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);


class CedaDemData: public CCSCedaData
{
// Attributes
public:
    CCSPtrArray<DEMDATA> omData;
    //CCSPtrArray<DEMDATA> omSelectedData;
	CUIntArray omSelectedData;
	CCSPtrArray<DEMDATA> omRedrawDuties;
// Maps f�r Fct,Quali usw. werden in PrepareData gef�llt

	CMapPtrToPtr omSingleFctcMap;
	CCSPtrArray<CString> omSingleFctcArray;

	CMapPtrToPtr omSinglePerMap;
	CCSPtrArray<CString> omSinglePerArray;

	CMapPtrToPtr omSingleCicMap;
	CCSPtrArray<CString> omSingleCicArray;

	CMapPtrToPtr omSinglePstMap;
	CCSPtrArray<CString> omSinglePstArray;

	CMapPtrToPtr omSingleGatMap;
	CCSPtrArray<CString> omSingleGatArray;

	CMapPtrToPtr omSingleRegMap;
	CCSPtrArray<CString> omSingleRegArray;

	CMapPtrToPtr omSingleReqMap;
	CCSPtrArray<CString> omSingleReqArray;

	CMapPtrToPtr omTemplateNameMap;
	CCSPtrArray<CString> omTemplateNameArray;

	CMapPtrToPtr omServiceNameMap;
	CCSPtrArray<CString> omServiceNameArray;

	CMapPtrToPtr omGhsuMap;
	CCSPtrArray<CString> omGhsuArray;

	CMapPtrToPtr omRustMap;
	CCSPtrArray<CString> omRustArray;

	CMapPtrToPtr omRetyMap;
	CCSPtrArray<CString> omRetyArray;

	CMapPtrToPtr omRusnMap;
	CCSPtrArray<CString> omRusnArray;

	CMapPtrToPtr omDeletedUrnoMap;


    CMapStringToPtr omFkeyMap;
	CMapStringToPtr omBreakMap;
    CMapPtrToPtr	omUrnoMap;
    CMapPtrToPtr	omKeyMap;

	CMapPtrToPtr	omCCIMap;

	CMapPtrToWord	omDispMap;

	CTime			omDispoStartTime;
	CTime			omDispoEndTime;
	long			omLastBcNum;
	long			omLastBcUrno;
	long			lmLfd;
	CString			omRuleGrpName;
	bool			bmCanSave;
	char			pcmListOfFields[2048];

	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}

// Operations
public:
    CedaDemData();
	~CedaDemData();

	void Register();
	CJobsTab *pomAttachedTable;
	CedaFlightData *pomCedaFlightData;
	void SetRuleGroupNames(CString opRuleGrp);
	void SetDispoTimeFrame(CTime opStartTime, CTime opEndTime);
	bool Read(CTime lpStartTime,CTime lpEndTime,CString opLoadedTplUrnos,bool bpWithClear = true);
	bool Read(char *pspWhere = NULL, bool bpWithClear = true,bool useRudHint = false);
	bool ReadSpecialData(CCSPtrArray<DEMDATA> *popDem, char *pspWhere, char *pspFieldList, bool ipSYS);
	void UndoChanges();
	void Unregister();
	void PrepareDemData(DEMDATA *prpDem);
    bool InsertDem(const DEMDATA *prpDemData);    // used in PrePlanTable only
    bool UpdateDem(const DEMDATA *prpDemData);    // used in PrePlanTable only
	void ProcessDemBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	bool ProcessDemRelease(void *vpDataPointer);
	bool ProcessDemUpdDem(void *vpDataPointer);
	bool AddDemInternal(DEMDATA *prpDem);
	bool ClearAll();

	void ReleaseDeletedData();

	CString GetPfcSingeUrnos(long lpUrno);
	CString GetPerSingeUrnos(long lpUrno);
	CString GetCicSingeUrnos(long lpUrno);
	CString GetPstSingeUrnos(long lpUrno);
	CString GetGatSingeUrnos(long lpUrno);
	CString GetRegSingeUrnos(long lpUrno);
	CString GetReqSingeUrnos(long lpUrno);

	CString GetTemplateName(long lpUrno);
	CString GetServiceName(long lpUrno);
	CString GetRusn(long lpUrno);
	CString GetRust(long lpUrno);
	CString GetRety(long lpUrno);
	CString GetGhsu(long lpUrno);
	WORD GetDispNr(long lpUrno);

	void AttachCedaFlightData(CedaFlightData *popCedaFlightData);
	DEMDATA * GetDemByUrno(long pcpUrno); 
	DEMDATA * GetDemByKey(long pcpKey);
	void GetNoResDems(CCSPtrArray<DEMDATA> &ropDems);
	bool GetDemsBySingleFlightUrno(CCSPtrArray<DEMDATA> &ropDems, long lpFlightUrno);
	bool GetDemsByFkey(CCSPtrArray<DEMDATA> &ropDems, long lpFkey,bool bpAlloc = true);
	bool GetDemsByKeyaKeyd(CCSPtrArray<DEMDATA> &ropDems, CString opKeya, CString opKeyd);
	bool ChangeDemTime(long lpDemUrno,CTime opNewStart,CTime opNewEnd, bool bpWithSave = false);
	bool DemExist(long Urno);
	bool InsertDem(DEMDATA *prpDemData, BOOL bpWithSave = FALSE);
	bool UpdateDem(DEMDATA *prpDemData, BOOL bpWithSave = FALSE);
	bool DeleteDem(DEMDATA *prpDemData, BOOL bpWithSave = FALSE);
	bool SaveDem(DEMDATA *prpDem);
	bool DeleteDemInternal(DEMDATA *prpDem);
	int  FindDemByNfmu(long lpNfmu,CTime opFrom,CCSPtrArray<DEMDATA> *popNfmDems);
	void MakeDemSelected(DEMDATA *prlDem, BOOL bpSelect);
	int  GetSelectedDems(CCSPtrArray<DEMDATA> *prpDemData);
	void ReadDemsBetweenTimesByEurn(CCSPtrArray<DEMDATA> &ropDems,CTime opStart,CTime opEnd,long lpEurn);
	void DeselectAll();
	bool ChangeJobTime(long lpJobUrno,CTime opNewStart,CTime opNewEnd);
	void GetDemsBetweenTimes(CCSPtrArray<DEMDATA> &ropDems,CTime opStart,CTime opEnd);
	void GetDemsBetweenTimes(CMapPtrToPtr &ropUrnoMap,CTime opStart,CTime opEnd);
	void GetAllDems(CMapPtrToPtr &ropUrnoMap);
	//Defaultparameter for opCmd = A
	// A = Broadcast, that forces all workstations to reload all ghds
	// S = Broadcast for updating/Deleting/Inserting a single ghs
	// F = Broadcast for reloading all duties for the given flight-urnos
	void Release(CCSPtrArray<DEMDATA> *popDemList = NULL, CString opCmd = CString("A"), long UrnA = 0, long UrnD = 0);
	void ProcessReloadDemsForFlights(CUIntArray &olFkeys, CString opRuleGrpName = CString(""));
	void ProcessReloadSingleDem(long llUrno);
	void ProcessLoadSingleDem(long llUrno);
	void ProcessReloadAllDems(CString opRuleGrpName = CString(""));
	bool ProcessAttachment(CString &ropAttachment);
	void ProcessDemandInsert(char *pcpFields, char *pcpData);
	bool ProcessDemandUpdate(long lpDemandUrno, char *pcpFields, char *pcpData);
	void ProcessDemandDelete(long llDemUrno);

	bool DemandHasEquipment(DEMDATA *prpDem,const CString& ropEquipmentType);
	bool HasExpired(DEMDATA *prpDem);
	bool GetLoadFormat();	
	bool GetFlightsFromCciDemand(CDWordArray &ropDemands,CMapPtrToPtr &ropDemToFlightMap);
	bool RecalculateFlightsFromCCIDemands();	

private:
	int		imCalledCnt;
	int		imStdSec;
	int		imCnvSec;

	int		imRudUghs;
	int		imSerSnam;
	int		imRudRety;
	int		imRudUrue;
	int		imRueUtpl;
	int		imRueRusn;
	int		imRueRust;
	int		imTplTnam;
	int		imRudDisp;

	CString	omLoadFormat;
};

extern long lgNextDemUrno;

#endif
