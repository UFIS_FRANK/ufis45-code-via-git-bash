// DelSdgDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <DelSdgDlg.h>
#include <CedaSdgData.h>
#include <GridFenster.h>
#include <BasicData.h>
#include <CCSParam.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CDelSdgDlg 


CDelSdgDlg::CDelSdgDlg(const CString& ropCurrent,CWnd* pParent /* NULL*/)
	: CDialog(CDelSdgDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDelSdgDlg)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT

	omCurrent  = ropCurrent;
	pomSdgList = new CGridFenster(this);
}

CDelSdgDlg::~CDelSdgDlg()
{
	delete pomSdgList;
}

void CDelSdgDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDelSdgDlg)
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDelSdgDlg, CDialog)
	//{{AFX_MSG_MAP(CDelSdgDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CDelSdgDlg 

BOOL CDelSdgDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// Shift demand group names laden
	CStringArray olSdgList;
	ogSdgData.GetAllDnam(olSdgList);
	
	// Titel setzten	
	SetWindowText(LoadStg(IDS_STRING1801));

	// Infofeld Text setzen
	SetDlgItemText(IDC_INFO,LoadStg(IDS_STRING1802));

	pomSdgList->SubclassDlgItem(IDC_CUSTOM1, this);
	pomSdgList->Initialize();

	CGXStyle olStyle;

	pomSdgList->LockUpdate(TRUE);
	pomSdgList->GetParam()->EnableUndo(FALSE);
	pomSdgList->GetParam()->EnableTrackColWidth(FALSE);
	pomSdgList->GetParam()->EnableTrackRowHeight(FALSE);
	pomSdgList->GetParam()->EnableSelection(GX_SELROW);
	pomSdgList->GetParam()->SetNumberedColHeaders(FALSE);

	bool blOperEnabled = ogCCSParam.GetParamValue(ogGlobal,"ID_OPERATION_FL",CTime::GetCurrentTime().Format("%Y%m%d%H%M%S"),true) == "Y" ? true : false;

	if (blOperEnabled)
	{
		pomSdgList->SetColCount(7);
	}
	else
	{
		pomSdgList->SetColCount(6);
	}

	pomSdgList->SetColWidth(0,0, 50);
	pomSdgList->SetColWidth(1,1,180);	// Name
	pomSdgList->SetColWidth(2,2,100);	// Period Start	
	pomSdgList->SetColWidth(3,3,100);	// Repeatings
	pomSdgList->SetColWidth(4,4,100);	// Start Master Week
	pomSdgList->SetColWidth(5,5,100);	// Period Distance
	pomSdgList->SetColWidth(6,6,100);	// Period End
	if (blOperEnabled)
	{
		pomSdgList->SetColWidth(7,7,100);	// Status Flags
	}

	pomSdgList->SetRowHeight(0, 0, 12);
		

	pomSdgList->SetRowCount(olSdgList.GetSize());

	olStyle.SetEnabled(FALSE);
	olStyle.SetReadOnly(TRUE);

	pomSdgList->SetStyleRange(CGXRange(0, 1, 0, 1), olStyle.SetValue(LoadStg(IDS_STRING61316)));
	pomSdgList->SetStyleRange(CGXRange(0, 2, 0, 2), olStyle.SetValue(LoadStg(IDS_STRING61317)));
	pomSdgList->SetStyleRange(CGXRange(0, 3, 0, 3), olStyle.SetValue(LoadStg(IDS_STRING61321)));
	pomSdgList->SetStyleRange(CGXRange(0, 4, 0, 4), olStyle.SetValue(LoadStg(IDS_STRING61319)));
	pomSdgList->SetStyleRange(CGXRange(0, 5, 0, 5), olStyle.SetValue(LoadStg(IDS_STRING61318)));
	pomSdgList->SetStyleRange(CGXRange(0, 6, 0, 6), olStyle.SetValue(LoadStg(IDS_STRING61329)));
	if (blOperEnabled)
	{
		pomSdgList->SetStyleRange(CGXRange(0, 7, 0, 7), olStyle.SetValue(LoadStg(IDS_STRING1890)));
	}

	POSITION olArea = pomSdgList->GetParam( )->GetRangeList( )->AddTail(new CGXRange);

	for(int ilLc = 0; ilLc < olSdgList.GetSize(); ilLc++)
	{
		long llSdgUrno = ogSdgData.GetUrnoByDnam(olSdgList[ilLc]);

		SDGDATA *prlSdg =  ogSdgData.GetSdgByUrno(llSdgUrno);
		if (prlSdg)
		{
			pomSdgList->SetStyleRange(CGXRange(ilLc+1, 1, ilLc+1, 1), olStyle.SetValue(prlSdg->Dnam));
			pomSdgList->SetStyleRange(CGXRange(ilLc+1, 2, ilLc+1, 2), olStyle.SetValue(prlSdg->Begi.Format("%d.%m.%Y")));
			pomSdgList->SetStyleRange(CGXRange(ilLc+1, 3, ilLc+1, 3), olStyle.SetValue(prlSdg->Dura));
			pomSdgList->SetStyleRange(CGXRange(ilLc+1, 3, ilLc+1, 4), olStyle.SetValue(prlSdg->Dbeg.Format("%d.%m.%Y")));
			pomSdgList->SetStyleRange(CGXRange(ilLc+1, 3, ilLc+1, 5), olStyle.SetValue(prlSdg->Peri));
			pomSdgList->SetStyleRange(CGXRange(ilLc+1, 6, ilLc+1, 6), olStyle.SetValue(prlSdg->Ende.Format("%d.%m.%Y")));

			if (blOperEnabled)
			{
				switch(prlSdg->Operation[0])
				{
				case 'P' :
					pomSdgList->SetStyleRange(CGXRange(ilLc+1, 7, ilLc+1, 7), olStyle.SetValue(LoadStg(IDS_STRING1891)));
				break;
				case 'O' :
					pomSdgList->SetStyleRange(CGXRange(ilLc+1, 7, ilLc+1, 7), olStyle.SetValue(LoadStg(IDS_STRING1892)));
				break;
				case 'A' :
					pomSdgList->SetStyleRange(CGXRange(ilLc+1, 7, ilLc+1, 7), olStyle.SetValue(LoadStg(IDS_STRING1893)));
				break;
				default:
					pomSdgList->SetStyleRange(CGXRange(ilLc+1, 7, ilLc+1, 7), olStyle.SetValue("?????"));
				}
			}

			if (omCurrent == prlSdg->Dnam)
			{
				pomSdgList->SetCurrentCell(ilLc+1, 1);
				pomSdgList->SetSelection(olArea, ilLc+1, 0, ilLc+1, 7);
			}
		}
	}

	pomSdgList->LockUpdate(FALSE);

	return TRUE;
}

void CDelSdgDlg::OnOK()
{
	CRowColArray olSelection;
	int ilSelected = pomSdgList->GetSelectedRows(olSelection,FALSE,FALSE);
	CString olDnam;

	for (int ilC = 0; ilC < ilSelected; ilC++)
	{
		olDnam = pomSdgList->GetValueRowCol(olSelection[ilC],1);
		omSelected.Add(olDnam);
	}

	CDialog::OnOK();
}


BOOL CDelSdgDlg::GetSelection(CStringArray& ropNamList)
{
	ropNamList.Append(omSelected);	
	return omSelected.GetSize();
}