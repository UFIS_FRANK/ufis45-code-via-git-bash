// ommonSetupPage.cpp : implementation file
//

#include <stdafx.h>
#include <coverage.h>
#include <ommonSetupPage.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CommonSetupPage property page

IMPLEMENT_DYNCREATE(CommonSetupPage, CPropertyPage)

CommonSetupPage::CommonSetupPage() : CPropertyPage(CommonSetupPage::IDD)
{
	//{{AFX_DATA_INIT(CommonSetupPage)
	m_EvaluationSortOrder = 0;
	//}}AFX_DATA_INIT
	this->cmColor = GRAY;
}

CommonSetupPage::~CommonSetupPage()
{
}

void CommonSetupPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CommonSetupPage)
	DDX_Control(pDX, IDC_EDIT_DEMAND_TIMER2, m_DemandTimer2);
	DDX_Control(pDX, IDC_EDIT_DEMAND_TIMER1, m_DemandTimer1);
	DDX_Radio(pDX, IDC_RADIO_SORT_BY_ROTATION, m_EvaluationSortOrder);
	DDX_Control(pDX, IDC_BUTTON_NODEM_FLIGHTS_COLOR, m_NoDemFlightsColor);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CommonSetupPage, CPropertyPage)
	//{{AFX_MSG_MAP(CommonSetupPage)
	ON_BN_CLICKED(IDC_NAME_CONFIG, OnNameConfig)
	ON_BN_CLICKED(IDC_BUTTON_NODEM_FLIGHTS_COLOR, OnNoDemFlightsColor)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CommonSetupPage message handlers

void CommonSetupPage::OnOK() 
{
	// TODO: Add your specialized code here and/or call the base class
	if (UpdateData(TRUE))
	{
		MakeParameters();
		EvaluateUpdateMode();
		MakeCedaData();

		ogDdx.DataChanged(this,UPDATE_SETUP,NULL);
		
		CPropertyPage::OnOK();
	}
}

BOOL CommonSetupPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	// TODO: Add extra initialization here
	CWnd *polWnd = GetDlgItem(IDC_TEXT_DEMAND_TIMER1);
	if (polWnd)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1853));
	}
	
	polWnd = GetDlgItem(IDC_TEXT_DEMAND_TIMER2);
	if (polWnd)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1854));
	}

	polWnd = GetDlgItem(IDC_TEXT_NAME_CONFIG);
	if (polWnd)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1921));
	}

	polWnd = GetDlgItem(IDC_NAME_CONFIG);
	if (polWnd)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1922));
	}

	polWnd = GetDlgItem(IDC_STATIC_EVALUATION);
	if (polWnd)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1932));
	}

	polWnd = GetDlgItem(IDC_RADIO_SORT_BY_ROTATION);
	if (polWnd)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1933));
	}

	polWnd = GetDlgItem(IDC_RADIO_SORT_BY_DEMAND);
	if (polWnd)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1934));
	}

	polWnd = GetDlgItem(IDC_STATIC_NODEM_FLIGHTS);
	if (polWnd)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1943));
	}

	polWnd = GetDlgItem(IDC_STATIC_NODEM_FLIGHTS_TEXT);
	if (polWnd)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1944));
	}

	m_DemandTimer1.SetTypeToInt(0,32767);
	m_DemandTimer2.SetTypeToInt(0,32767);

	this->m_NoDemFlightsColor.SetColors(this->cmColor,::GetSysColor(COLOR_BTNSHADOW),::GetSysColor(COLOR_BTNHIGHLIGHT));

	InitializeUserPreferences();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CommonSetupPage::SetCaption(const char *pcpCaption)
{
	strcpy(pcmCaption,pcpCaption);
	m_psp.pszTitle = pcmCaption;
	m_psp.dwFlags |= PSP_USETITLE;
}

void CommonSetupPage::EvaluateUpdateMode()
{
	BOOL blFound = FALSE;
	int ilCount = ogCfgData.omData.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		CFGDATA rlC = ogCfgData.omData[i];
		if((strcmp(rlC.Pkno, ogBasicData.omUserID) == 0) && (strcmp(rlC.Ctyp, "CKI-SETUP") == 0))
		{
			blFound = TRUE;
		}
	}

	if (blFound == TRUE)
		omMode = CString("UPDATE_MODE");
	else
		omMode = CString("INSERT_MODE");
}

void CommonSetupPage::MakeCedaData()
{
	//omParameters1 
	//omParameters2;
	if (omMode == CString("INSERT_MODE"))
	{
		CString olParameters;

		//Part 1 the basic-parameters
		CFGDATA *prlCfg1 = new CFGDATA;
		strcpy(prlCfg1->Ctyp, "CKI-SETUP");
		strcpy(prlCfg1->Pkno, ogBasicData.omUserID);
		strcpy(prlCfg1->Text, omParameters1);
		prlCfg1->Urno = ogBasicData.GetNextUrno();
		prlCfg1->IsChanged = DATA_NEW;

		ogCfgData.AddCfg(prlCfg1);

	}
	else if (omMode == CString("UPDATE_MODE"))
	{
		int ilCount = 0;
		int ilCountSet = 0;
		CCSPtrArray<CFGDATA> olData;
		ilCount = ogCfgData.omData.GetSize();
		for(int i = 0; i < ilCount; i++)
		{
			CFGDATA rlC = ogCfgData.omData[i];
			if((strcmp(rlC.Pkno, ogBasicData.omUserID) == 0) && (strcmp(rlC.Ctyp, "CKI-SETUP") == 0))
			{
				olData.NewAt(0, rlC);
				ilCountSet++;
			}
		}
		
		if (ilCountSet > 0)
		{
			CFGDATA rlCfgData1 = olData.GetAt(0);

			CFGDATA *prlCfg1 = ogCfgData.GetCfgByUrno(rlCfgData1.Urno);
			strcpy(prlCfg1->Text, omParameters1);

			strcpy(rlCfgData1.Text, omParameters1);
			rlCfgData1.IsChanged = DATA_CHANGED;
			ogCfgData.ChangeCfgData(&rlCfgData1);
		
		}
		else
		{
			//Part 1 the basic-parameters
			CFGDATA *prlCfg1 = new CFGDATA;
			strcpy(prlCfg1->Ctyp, "CKI-SETUP");
			strcpy(prlCfg1->Pkno, ogBasicData.omUserID);
			strcpy(prlCfg1->Text, omParameters1);
			prlCfg1->Urno = ogBasicData.GetNextUrno();
			prlCfg1->IsChanged = DATA_NEW;

			ogCfgData.AddCfg(prlCfg1);

		}

		olData.DeleteAll();
	}
}

int CommonSetupPage::MakeParameters()
{
	CString olDet1,olDet2,olEvso,olNdfc;

	omParameters1 = "";
	
	m_DemandTimer1.GetWindowText(olDet1);
	omParameters1 += "DET1=" + olDet1 + ";";

	m_DemandTimer2.GetWindowText(olDet2);
	omParameters1 += "DET2=" + olDet2 + ";";

	omParameters1 += "NAME=" + ogBasicData.GetNameConfigString()  + ";";

	olEvso.Format("%d",m_EvaluationSortOrder);
	omParameters1 += "EVSO=" + olEvso + ";";

	olNdfc.Format("%ld",this->cmColor);
	omParameters1 += "NDFC=" + olNdfc + ";";


	return 0;
}

void CommonSetupPage::InitializeUserPreferences() 
{
	CString olParameters;
	CCSPtrArray<CFGDATA> olData;
	int ilCountSets = 0;
	int ilCount = ogCfgData.omData.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
		CFGDATA rlC = ogCfgData.omData[i];
		if((strcmp(rlC.Pkno, ogBasicData.omUserID) == 0) && (strcmp(rlC.Ctyp, "CKI-SETUP") == 0))
		{
			olData.NewAt(0, rlC);
			ilCountSets++;
		}
	}
	
	if (ilCountSets == 1)
	{
		char *psp = NULL;
		CString olTmp1, olTmp2;
		CFGDATA rlC1 = olData.GetAt(0);
		olTmp1 = rlC1.Text;
		psp = strstr(rlC1.Text, "DET1");
		if (psp != NULL)
		{
			omParameters1 = olTmp1;
		}
	}
	else
	{
		ResetDialog();
		return;
	}

	olData.DeleteAll();

	if (!omParameters1.IsEmpty())
	{
		MakeSetupStruct();

		m_DemandTimer1.SetInitText(rmUserSetup.DET1);
		m_DemandTimer2.SetInitText(rmUserSetup.DET2);
		m_EvaluationSortOrder = atoi(rmUserSetup.EVSO);
		this->cmColor = atol(rmUserSetup.NDFC);
		m_NoDemFlightsColor.SetColors(this->cmColor,::GetSysColor(COLOR_BTNSHADOW),::GetSysColor(COLOR_BTNHIGHLIGHT));

		UpdateData(FALSE);
		omMode = CString("UPDATE_MODE");
	}
}

void CommonSetupPage::MakeSetupStruct()
{
	ogCfgData.InterpretSetupString(omParameters1, &rmUserSetup);
}

int CommonSetupPage::ResetDialog()
{
	m_DemandTimer1.SetInitText("300");
	m_DemandTimer2.SetInitText("60");
	m_EvaluationSortOrder = 0;
	this->cmColor = GRAY;
	m_NoDemFlightsColor.SetColors(cmColor,::GetSysColor(COLOR_BTNSHADOW),::GetSysColor(COLOR_BTNHIGHLIGHT));

	UpdateData(FALSE);
	return 0;
}


void CommonSetupPage::OnNameConfig() 
{
	// TODO: Add your control notification handler code here
	if (ogBasicData.ShowNameConfDlg(this))
	{
						
	}
}

void CommonSetupPage::OnNoDemFlightsColor() 
{
	// TODO: Add your control notification handler code here
	DWORD	 dwFlags	= 0;
	CColorDialog olDialog(this->cmColor,dwFlags,this);
	if (olDialog.DoModal() == IDOK)
	{
		if (this->cmColor != olDialog.GetColor())
		{
			this->cmColor = olDialog.GetColor();
			this->m_NoDemFlightsColor.SetColors(this->cmColor,::GetSysColor(COLOR_BTNSHADOW),::GetSysColor(COLOR_BTNHIGHLIGHT));
		}
	}
}
