#ifndef _CedaFlightData_H_
#define _CedaFlightData_H_
 
#include <BasicData.h>
#include <CCSGlobl.h>
#include <CCSDefines.h>

//void ProcessFlightCf(void *vpInstance,enum enumDDXTypes ipDDXType, void *vpDataPointer, CString &ropInstanceName);




/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

//@Man: FLIGHTDATAStruct
//@See: CedaFlightData
/*@Doc:
  A structure for reading flight data. We read all data from database (or get 
  the data from AFTLSG) into this struct and store the data in omData.

*/

struct RKEYLIST;


 
struct FLIGHTDATA 
{
//F�r RMS relevante oben aus FPMS
	char	 Adid[2];	//Arrival oder Departure Kennung ("A"|"D")
	char	 Regn[12];	//Registration
	char 	 Act3[5]; 	// Flugzeug-3-Letter Code (IATA)
	char 	 Act5[7]; 	// Flugzeug-5-Letter Code
	char 	 Alc2[4]; 	// Fluggesellschaft (Airline 2-Letter Code)
	char 	 Alc3[5]; 	// Fluggesellschaft (Airline 3-Letter Code)
	char 	 Fltn[7]; 	// Flugnummer
	char 	 Flns[3]; 	// Suffix
	char 	 Org3[5]; 	// Ausgangsflughafen 3-Lettercode
	char 	 Org4[6]; 	// Ausgangsflughafen 4-Lettercode
	char 	 Des3[5]; 	// Bestimmungsflughafen 3-Lettercode
	char 	 Des4[6]; 	// Bestimmungsflughafen 4-Lettercode
	char	 Via3[5];	// Via 3
	char	 Via4[6];	// Via 4
	char	 Vian[6];	// Anzahl Zwischenstationen
	char 	 Psta[7]; 	// Position Ankunft
	char 	 Pstd[7]; 	// Position Abflug
	char 	 Ttyp[6]; 	// Verkehrsart
	char 	 Htyp[4]; 	// Abfertigungsart
	char	 Dooa[2];	// Verkehrstag
	char	 Dood[2];	// Verkehrstag
	CTime 	 Stoa; 		// Planm��ige Ankunftszeit STA
	CTime 	 Stod; 		// Planm��ige Abflugzeit
	CTime	 Tmoa;		// Ten Miles out
	CTime 	 Etai; 		// ETA-Intern (Beste Zeit)
	CTime	 Land;		// gelandet
	CTime	 Onbl;		// On Block
	CTime	 Ofbl;		// Off Block
	CTime	 Airb;		// Airborne
	CTime 	 Etdi; 	// ETD-Intern (Beste Zeit)
	CTime 	 Tifa; 	// Zeitrahmen Ankunft
	CTime 	 Tifd; 	// Zeitrahmen Abflug
	CTime 	 Lstu; 	// Datum letzte �nderung
	CTime 	 Cdat; 	// Erstellungsdatum
	char 	 Gta1[7]; 	// Gate 1 Ankunft
	char 	 Gtd1[7]; 	// Gate 1 Abflug
	int		 Pax1;		// Pax1
	int		 Pax2;		// Pax2
	int		 Pax3;		// Pax3
	int		 Paxt;		// Anzahl Passagiere Total
	int		 Bagn;		// Gewicht Gep�ckst�cke in KG
	int		 Bagw;		// Gewicht Total (Gep�ck+Fracht+Post)
	int		 Cgot;		// Fracht in Tonnen
	int		 Mail;		// Post in St�ck
	char 	 Rem1[258]; // Bemerkung zum Flug
	long 	 Urno; 	// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	char 	 Tisa[3]; 	// Statusdaten f�r TIFA
	char 	 Tisd[3]; 	// Statusdaten f�r TISD
	long 	 Rkey; 	// Rotationsschl�ssel
	char 	 Rtyp[3]; 	// Verkettungstyp (Art/Quelle der Verkettung)
	char 	 Flno[11]; 	// komplette Flugnummer (Airline, Nummer, Suffix)
	char 	 Ftyp[3]; 	// Type des Flugs [F, P, R, X, D, T, ...]
	//DataCreated by this class
	int      IsChanged;

	bool     IsTouched;
/*********
	CString	 ActmUrno;
	CString	 AlmaUrno;
	CString	 AlmdUrno;
	CString	 TrraUrno;
	CString	 TrrdUrno;
	CString	 PcaaUrno;
	CString	 PcadUrno;
	CString  TtgaUrno;
	CString  TtgdUrno;
	CString  HtgaUrno;
	CString  HtgdUrno;
	******************/
	FLIGHTDATA(void)
	{ //memset(this,'\0',sizeof(*this));
	Stoa = -1; 		// Planm��ige Ankunftszeit STA
	Stod = -1; 		// Planm��ige Abflugzeit
	Tmoa = -1;		// Ten Miles out
	Etai = -1; 		// ETA-Intern (Beste Zeit)
	Land = -1;		// gelandet
	Onbl = -1;		// On Block
	Ofbl = -1;		// Off Block
	Airb = -1;		// Airborne
	Etdi = -1; 	// ETD-Intern (Beste Zeit)
	Tifa = -1; 	// Zeitrahmen Ankunft
	Tifd = -1; 	// Zeitrahmen Abflug
	Lstu = -1; 	// Datum letzte �nderung
	Cdat = -1; 	// Erstellungsdatum

	Adid[0]='\0';	Regn[0]='\0';
	Act3[0]='\0';	Act5[0]='\0';
	Alc2[0]='\0';	Alc3[0]='\0';
	Fltn[0]='\0';	Flns[0]='\0';
	Org3[0]='\0';	Org4[0]='\0';
	Des3[0]='\0';	Des4[0]='\0';
	Via3[0]='\0';	Via4[0]='\0';
	Vian[0]='\0';	Psta[0]='\0';
	Pstd[0]='\0';	Ttyp[0]='\0';
	Htyp[0]='\0';	Dooa[0]='\0';
	Dood[0]='\0';	Gta1[0]='\0';
	Gtd1[0]='\0';
	Pax1 = 0;	Pax2 = 0;
	Pax3 = 0;	Paxt = 0;
	Bagn = 0;	Bagw = 0; Cgot = 0;
	Mail = 0;	Rem1[0]='\0';
	Urno = 0;	Usec[0]='\0';
	Useu[0]='\0';	Tisa[0]='\0';
	Tisd[0]='\0';	Rkey = 0;
	Rtyp[0]='\0';
	Flno[0]='\0';
	Ftyp[0]='\0';
	IsTouched = false;
	}

}; // end FLIGHTDATA
	




/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaFlightData: public CCSCedaData
{
public:
    CedaFlightData();
	~CedaFlightData();
	
	void UnRegister(void);
	void Register(void);

	void ClearAll(void);

	CString omFtyps;
	CTime omFrom;
	CTime omTo;

	bool bpIgnoreTimeFrame;
// Attributes
public:
    CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omRkeyMap;

    CCSPtrArray<FLIGHTDATA> omData;
    CCSPtrArray<FLIGHTDATA> omResultData;
    CCSPtrArray<FLIGHTDATA> *pomData;
	CString omSelection;
	CString omOrder;

 	char pcmAftFieldList[2048];
	bool bmSelection;
	CTime omLoadedFrom;
	CTime omLoadedTo;

	bool bmRotation;

// Operations
public:
   
	bool CedaFlightData::CreateDemands(CString &opWhere,bool bpRuleDiagnosis);
	bool CedaFlightData::CreateFidDemands(CString &opWhere);

    // internal data access.
	void SetLoadedTimeFrame(CTime opFrom, CTime opTo);
	bool AddFlightInternal(FLIGHTDATA *prpFlight, bool bpDdx = false);
	bool DeleteFlightInternal(long lpUrno, bool bpDdx = false);

	bool ReadFlights(char *pcpSelection = NULL, bool bpIgnoreViewSel = false, bool bpDdx = false);
	bool ReadAllFlights(CString &opWhere, bool bpWithClear = true);
	bool ReadRotations(char *pcpSelection, bool bpIngoreView, bool bpDdx = false );
	bool ReadRkeyFlights(char *pcpSelection, CUIntArray &ropRkeyList, bool bpIngoreView );
	void ReadMissingFlights(CMapPtrToPtr& ropMissingFlights);

	void SetPreSelection(CTime &opLocalFrom, CTime &opLocalTo, CString &opFtyps, bool bpRotation = false);
	bool PreSelectionCheck(CString &opData, CString &opFields);

	bool  CheckFlight(FLIGHTDATA *prpFlightOld, FLIGHTDATA *prpFlightNew);


	void ProcessFlightBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	void ProcessAltBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	
	CString CreateFlno(CString opAlc, CString opFltn, CString opFlns);
	
	FLIGHTDATA *GetJoinFlight(FLIGHTDATA *prpFlight);
	FLIGHTDATA *GetArrival(FLIGHTDATA *prpFlight);
	FLIGHTDATA *GetDeparture(FLIGHTDATA *prpFlight);

	int GetViaArray(CCSPtrArray<VIADATA> *opVias, FLIGHTDATA *prpFlight);
	bool AddToKeyMap(FLIGHTDATA *prpFlight );
	bool DeleteFromKeyMap(FLIGHTDATA *prpFlight );
	FLIGHTDATA *GetFlightByUrno(long lpUrno);
	bool ProcessFlightUFR(BcStruct *prlBcStruct);
	bool ProcessFlightIFR(BcStruct *prlBcStruct);
	bool ProcessFlightISF(BcStruct *prlBcStruct);
	void ProcessFlightRAC(BcStruct *prlBcStruct);


	bool IsFlightJoin(long lpUrno);

// helper functions
	void	GetGroupUrnos(FLIGHTDATA *prpFlight);
	CString GetFtypText(FLIGHTDATA *prpFlight);



};


struct RKEYLIST
{
	CCSPtrArray<FLIGHTDATA> Rotation;

};



#endif
