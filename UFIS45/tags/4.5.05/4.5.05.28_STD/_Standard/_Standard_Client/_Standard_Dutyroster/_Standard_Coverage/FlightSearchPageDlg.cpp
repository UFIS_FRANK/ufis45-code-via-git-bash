// FlightSearchPageDlg.cpp : implementation file
//

#include <stdafx.h>
#include <coverage.h>
#include <ddxddv.h>
#include <FlightSearchPageDlg.h>
#include <FilterData.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FlightSearchPageDlg property page

IMPLEMENT_DYNCREATE(FlightSearchPageDlg, CPropertyPage)

FlightSearchPageDlg::FlightSearchPageDlg() : CPropertyPage(FlightSearchPageDlg::IDD)
{
	//{{AFX_DATA_INIT(FlightSearchPageDlg)
	m_Airline = _T("");
	m_Flightno = _T("");
	m_Regn = _T("");
	//}}AFX_DATA_INIT
	m_Date = ogBasicData.GetUtcTime();
	bmFirst = true;
	pomParent = NULL;
}

FlightSearchPageDlg::~FlightSearchPageDlg()
{
}

void FlightSearchPageDlg::Attach(CWnd *popParent)
{
	pomParent = popParent;
}

void FlightSearchPageDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FlightSearchPageDlg)
	DDX_Control(pDX, IDC_FLIGHTNR, m_FlightNrCtrl);
	DDX_Text(pDX, IDC_AIRLINE, m_Airline);
	DDV_MaxChars(pDX, m_Airline, 2);
	DDX_CCSddmmyy(pDX, IDC_DATE, m_Date);
	DDX_Text(pDX, IDC_FLIGHTNR, m_Flightno);
	DDV_MaxChars(pDX, m_Flightno, 4);
	DDX_Text(pDX, IDC_REGN, m_Regn);
	DDV_MaxChars(pDX, m_Regn, 7);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(FlightSearchPageDlg, CPropertyPage)
	//{{AFX_MSG_MAP(FlightSearchPageDlg)
	ON_EN_KILLFOCUS(IDC_FLIGHTNR, OnKillfocusFlightnr)
	ON_EN_SETFOCUS(IDC_AIRLINE, OnSetfocusAirline)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FlightSearchPageDlg message handlers

void FlightSearchPageDlg::OnKillfocusFlightnr() 
{
	char pclStr[10]="";
	char pclTmp[10]="";
	GetDlgItemText(IDC_FLIGHTNR, pclStr, 100);
	if(strlen(pclStr) != (size_t)0)
	{
		int ilLen = (int)strlen(pclStr);
		for(int i = 0; i < 4-ilLen; i++)
		{
			strcat(pclStr, "%");
		}
		
		sprintf(pclTmp, "%04s", pclStr);
	}
	SetDlgItemText(IDC_FLIGHTNR, pclTmp);
	
}

BOOL FlightSearchPageDlg::OnInitDialog() 
{
	char pclStr[10];
	CPropertyPage::OnInitDialog();
	
	CWnd *polWnd = GetDlgItem(IDC_ZEITRAUMCHECK);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61264));
	}
	polWnd = GetDlgItem(IDC_STIMEFROM);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61265));
	}
	polWnd = GetDlgItem(IDC_STIMETO);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61266));
	}
	polWnd = GetDlgItem(IDC_ARRIVALS);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1531));
	}
	polWnd = GetDlgItem(IDC_DEPATURE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1533));
	}
	polWnd = GetDlgItem(IDC_LSTUSTATIC);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61269));
	}
	polWnd = GetDlgItem(IDC_LSTUFROM);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61270));
	}
	polWnd = GetDlgItem(IDC_FLIGHTCODE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61271));
	}
	polWnd = GetDlgItem(IDC_REGISTRATION);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61272));
	}
	polWnd = GetDlgItem(IDC_ACTYPE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61273));
	}
	polWnd = GetDlgItem(IDC_AIRPORT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61274));
	}
	polWnd = GetDlgItem(IDC_AIRPORT2);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61275));
	}
	polWnd = GetDlgItem(IDC_BEMERKUNG);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING704));
	}
	polWnd = GetDlgItem(IDC_BETRIEB);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61276));
	}
	polWnd = GetDlgItem(IDC_PLANUNG);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61277));
	}
	polWnd = GetDlgItem(IDC_CANCELLED);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61278));
	}
	polWnd = GetDlgItem(IDC_NOOP);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61279));
	}

	SetCaption(LoadStg(IDS_STRING61262));
	


	CTime   olEndTime;
	m_Airline = CString(ogSearchFilter.rlFlightFilterData.Airline);
	m_Flightno = ogSearchFilter.rlFlightFilterData.Flno;
	m_Regn = ogSearchFilter.rlFlightFilterData.Regn;
	ogBasicData.GetDiagramStartTime(m_Date, olEndTime);
	sprintf(pclStr, "%s", ogSearchFilter.rlFlightFilterData.Flno);

	UpdateData(FALSE);
	::SetDlgItemText(GetSafeHwnd(),IDC_FLIGHTNR, pclStr);

	m_FlightNrCtrl.SetFocus();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


BOOL FlightSearchPageDlg::OnSetActive() 
{
	if (pomParent != NULL)
		pomParent->SendMessage(WM_FLIGHTSEARCHACTIVE,1,0L);

	m_FlightNrCtrl.SetFocus();
	
	BOOL blRc = CPropertyPage::OnSetActive();
	m_FlightNrCtrl.SetFocus();
	return blRc;
}

BOOL FlightSearchPageDlg::OnKillActive() 
{
	if (pomParent != NULL)
		pomParent->SendMessage(WM_FLIGHTSEARCHACTIVE,0,0L);
	return CPropertyPage::OnKillActive();
}

void FlightSearchPageDlg::OnSetfocusAirline() 
{
	if(bmFirst)
	{
		m_FlightNrCtrl.SetFocus();
	}
	bmFirst = false;
	
}
