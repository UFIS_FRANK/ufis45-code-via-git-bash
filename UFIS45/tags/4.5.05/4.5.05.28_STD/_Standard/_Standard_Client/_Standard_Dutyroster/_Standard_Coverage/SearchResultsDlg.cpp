// SearchResultsDlg.cpp : implementation file
//

#include <stdafx.h>
#include <Coverage.h>
#include <CovDiagram.h>
#include <SearchResultsDlg.h>
#include <JobDlg.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SearchResultsDlg dialog



SearchResultsDlg::SearchResultsDlg(CCSPtrArray<FLIGHTSEARCHDATA> &ropFlights,CWnd* pParent /*=NULL*/, CString opFilter)
	: CDialog(SearchResultsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(SearchResultsDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	imWhichSearch = PS_FLIGHTS;

	
	pomQueryList = new CGridFenster(this);

	pomTsrArray = NULL;
	omFilter = opFilter;
	int ilSize = ropFlights.GetSize();
	for(int illc = 0; illc < ropFlights.GetSize(); illc++)
	{
		FLIGHTSEARCHDATA *prlSFilght = &ropFlights.GetAt(illc);
		omFlights.NewAt(illc,ropFlights.GetAt(illc));
	}
	omCaption = "";

	cmGrayed = atol(ogCfgData.rmUserSetup.NDFC);

}


SearchResultsDlg::~SearchResultsDlg() 
{
	ogDdx.UnRegister(this,NOTUSED);
	omFlights.DeleteAll();
	delete pomQueryList;
}

void SearchResultsDlg::SetCaptionText(CString opCaption)
{
	omCaption = opCaption;
}
void SearchResultsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SearchResultsDlg)
	DDX_Control(pDX, IDC_SEARCHFILTER, m_SearchFilter);
	DDX_Control(pDX, IDC_TEXTSTATIC, m_TextStatic);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(SearchResultsDlg, CDialog)
	//{{AFX_MSG_MAP(SearchResultsDlg)
	ON_WM_HSCROLL()
	ON_WM_DESTROY()
	ON_MESSAGE(GRID_MESSAGE_CELLCLICK, OnLButtonClickedRowCol)
	ON_MESSAGE(GRID_MESSAGE_DOUBLECLICK, OnLButtonDblClickedRowCol)	
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SearchResultsDlg message handlers

BOOL SearchResultsDlg::OnInitDialog() 
{
	int ilLc = 0;
	int i;
	int ilIndex = -1;
	CString olLine="";
	CString olHeaderText;

	CDialog::OnInitDialog();

	CWnd *polWnd = GetDlgItem(IDCANCEL);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61214));
	}
	polWnd = GetDlgItem(IDC_TEXTSTATIC);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61335));
	}
	
	SetWindowText(LoadStg(IDS_STRING61336));


	if(!omCaption.IsEmpty())
	{
		SetWindowText(omCaption);
	}
//------------------------------------
// center window within the 1st screen
	CRect olRect;
	GetWindowRect(&olRect);
	int x = (int)1024/2;
	int y = (int)768/2;
	int length = olRect.right-olRect.left;
	int height = olRect.bottom-olRect.top;
	olRect.left   = x - (int)((length)/2);
	olRect.right  = x + (int)((length)/2);
	olRect.top    = y - (int)((height)/2);
	olRect.bottom = y + (int)((height)/2);

	MoveWindow(&olRect, FALSE);
// end center
//------------------------------------

	CRect rcCancel;
	polWnd = GetDlgItem(IDC_SEARCHLIST);
	if (polWnd != NULL)
	{
		polWnd->GetWindowRect(rcCancel);
		ScreenToClient(rcCancel);
	} 

	m_SearchFilter.SetWindowText(omFilter);
	if(!ogBasicData.IsDebugMode() || omFilter.IsEmpty())
	{
		if (polWnd != NULL)
		{
			rcCancel.top -=80;
			polWnd->MoveWindow(rcCancel);
		}
		m_SearchFilter.ShowWindow(SW_HIDE);
		m_TextStatic.ShowWindow(SW_HIDE);
		
	}

	int ilHeight = rcCancel.Height();

	int ilLineCount = (int) (ilHeight/17);
	


	pomQueryList->SubclassDlgItem(IDC_SEARCHLIST, this);
	pomQueryList->Initialize();

	CGXStyle olStyle;

	switch(imWhichSearch)
	{
	case PS_FLIGHTS:
		{
	
			ilLc = omFlights.GetSize();

		
			pomQueryList->SetAllowMultiSelect(false);


			pomQueryList->LockUpdate(TRUE);
			pomQueryList->GetParam()->EnableUndo(FALSE);
			pomQueryList->GetParam()->EnableTrackColWidth(FALSE);
			pomQueryList->GetParam()->EnableTrackRowHeight(FALSE);
			pomQueryList->GetParam()->EnableSelection(GX_SELROW );
			pomQueryList->GetParam()->SetNumberedColHeaders(FALSE);

			pomQueryList->SetRowCount(max(ilLc,ilLineCount));
			pomQueryList->SetColCount(25);

			pomQueryList->SetColWidth(0,1,30);
			pomQueryList->SetColWidth(1,1,67);
			pomQueryList->SetColWidth(2,2,40);
			pomQueryList->SetColWidth(3,3,90);
			pomQueryList->SetColWidth(4,4,50);
			pomQueryList->SetColWidth(5,5,40);
			pomQueryList->SetColWidth(6,6,50);

			pomQueryList->SetColWidth(7,7,30);
			pomQueryList->SetColWidth(8,8,67); 
			pomQueryList->SetColWidth(9,9,40);
			pomQueryList->SetColWidth(10,10,90);
			pomQueryList->SetColWidth(11,11,50); 
			pomQueryList->SetColWidth(12,12,40);
			pomQueryList->SetColWidth(13,13,50);

			pomQueryList->SetValueRange(CGXRange(0,1),LoadStg(IDS_STRING507));
			pomQueryList->SetValueRange(CGXRange(0,2),LoadStg(IDS_STRING1026));
			pomQueryList->SetValueRange(CGXRange(0,3),LoadStg(IDS_STRING1531));
			pomQueryList->SetValueRange(CGXRange(0,4),LoadStg(IDS_STRING1022));
			pomQueryList->SetValueRange(CGXRange(0,5),LoadStg(IDS_STRING1023));
			pomQueryList->SetValueRange(CGXRange(0,6),LoadStg(IDS_STRING61346));
			pomQueryList->SetValueRange(CGXRange(0,8),LoadStg(IDS_STRING507));
			pomQueryList->SetValueRange(CGXRange(0,9),LoadStg(IDS_STRING1029));
			pomQueryList->SetValueRange(CGXRange(0,10),LoadStg(IDS_STRING1533));
			pomQueryList->SetValueRange(CGXRange(0,11),LoadStg(IDS_STRING1022));
			pomQueryList->SetValueRange(CGXRange(0,12),LoadStg(IDS_STRING1023));
			pomQueryList->SetValueRange(CGXRange(0,13),LoadStg(IDS_STRING61346));

			pomQueryList->SetRowHeight(0, 0, 20);
			
			CString Flnoa,Flnod;
			CString Orig,Dest;
			CString Rega,Regd;
			CString Act3a,Act3d;

			olStyle.SetEnabled(FALSE);
			olStyle.SetReadOnly(TRUE);
			for(i = 0; i < ilLc; i++)
			{
				if(omFlights[i].InGrayed)
				{
					olStyle.SetTextColor(this->cmGrayed);
				}
				else
				{
					olStyle.SetTextColor(ogColors[BLACK_IDX]);
				}

				Flnoa = omFlights[i].Flnoa;
				Orig = omFlights[i].Orig;
				Rega = omFlights[i].Regna;
				Act3a = omFlights[i].Act3a;

				pomQueryList->SetStyleRange(CGXRange(i+1, 1, i+1, 1), olStyle.SetValue(omFlights[i].Flnoa));
				pomQueryList->SetStyleRange(CGXRange(i+1, 2, i+1, 2), olStyle.SetValue(omFlights[i].Orig));
				pomQueryList->SetStyleRange(CGXRange(i+1, 3, i+1, 3), olStyle.SetValue(omFlights[i].Tifa));
				pomQueryList->SetStyleRange(CGXRange(i+1, 4, i+1, 4), olStyle.SetValue(omFlights[i].Regna));
				pomQueryList->SetStyleRange(CGXRange(i+1, 5, i+1, 5), olStyle.SetValue(omFlights[i].Act3a));
				pomQueryList->SetStyleRange(CGXRange(i+1, 6, i+1, 6), olStyle.SetValue(omFlights[i].PosA));
				if(omFlights[i].IsLoaded)
				{
					olStyle.SetValue("-->");
					olStyle.SetInterior(ogColors[GREEN_IDX]);
				}
				else
				{
					olStyle.SetValue("==>");
					olStyle.SetInterior(ogColors[YELLOW_IDX]);
				}
				if(omFlights[i].OutGrayed)
				{
					olStyle.SetTextColor(this->cmGrayed);
				}
				else
				{
					olStyle.SetTextColor(ogColors[BLACK_IDX]);
				}

				pomQueryList->SetStyleRange(CGXRange(i+1, 7, i+1, 7), olStyle);

				olStyle.SetInterior(ogColors[WHITE_IDX]);

				Flnod = omFlights[i].Flnod;
				Dest = omFlights[i].Dest;
				Regd = omFlights[i].Regnd;
				Act3d = omFlights[i].Act3d;


				pomQueryList->SetStyleRange(CGXRange(i+1, 8, i+1, 8), olStyle.SetValue(omFlights[i].Flnod));
				pomQueryList->SetStyleRange(CGXRange(i+1, 9, i+1, 9), olStyle.SetValue(omFlights[i].Dest));
				pomQueryList->SetStyleRange(CGXRange(i+1, 10, i+1, 10), olStyle.SetValue(omFlights[i].Tifd));
				pomQueryList->SetStyleRange(CGXRange(i+1, 11, i+1, 11), olStyle.SetValue(omFlights[i].Regnd));
				pomQueryList->SetStyleRange(CGXRange(i+1, 12, i+1, 12), olStyle.SetValue(omFlights[i].Act3d));
				pomQueryList->SetStyleRange(CGXRange(i+1, 13, i+1, 13), olStyle.SetValue(omFlights[i].PosD));
				pomQueryList->SetStyleRange(CGXRange(i+1, 14, i+1, 14), olStyle.SetValue(omFlights[i].Urna));
				pomQueryList->SetStyleRange(CGXRange(i+1, 15, i+1, 15), olStyle.SetValue(omFlights[i].Urnd));
				pomQueryList->SetStyleRange(CGXRange(i+1, 16, i+1, 16), olStyle.SetValue(omFlights[i].SortTifa));
				pomQueryList->SetStyleRange(CGXRange(i+1, 17, i+1, 17), olStyle.SetValue(omFlights[i].SortTifd));


				pomQueryList->SetStyleRange(CGXRange(i+1, 18, i+1, 18), olStyle.SetValue((Flnoa.IsEmpty())?"0":"1"));
				pomQueryList->SetStyleRange(CGXRange(i+1, 19, i+1, 19), olStyle.SetValue((Orig.IsEmpty())?"0":"1"));
				pomQueryList->SetStyleRange(CGXRange(i+1, 20, i+1, 20), olStyle.SetValue((Rega.IsEmpty())?"0":"1"));
				pomQueryList->SetStyleRange(CGXRange(i+1, 21, i+1, 21), olStyle.SetValue((Act3a.IsEmpty())?"0":"1"));
				
				pomQueryList->SetStyleRange(CGXRange(i+1, 22, i+1, 22), olStyle.SetValue((Flnod.IsEmpty())?"0":"1"));
				pomQueryList->SetStyleRange(CGXRange(i+1, 23, i+1, 23), olStyle.SetValue((Dest.IsEmpty())?"0":"1"));
				pomQueryList->SetStyleRange(CGXRange(i+1, 24, i+1, 24), olStyle.SetValue((Regd.IsEmpty())?"0":"1"));
				pomQueryList->SetStyleRange(CGXRange(i+1, 25, i+1, 25), olStyle.SetValue((Act3d.IsEmpty())?"0":"1"));
			}

			pomQueryList->LockUpdate(FALSE);

			pomQueryList->HideCols(14, 25); 
			pomQueryList->SetSortQuery(3, 16); 
			pomQueryList->SetSortQuery(10, 17); 
			pomQueryList->SetSortQuery(1, 18); 
			pomQueryList->SetSortQuery(2, 19); 
			pomQueryList->SetSortQuery(4, 20); 
			pomQueryList->SetSortQuery(5, 21); 
			pomQueryList->SetSortQuery(8, 22); 
			pomQueryList->SetSortQuery(9, 23); 
			pomQueryList->SetSortQuery(11, 24); 
			pomQueryList->SetSortQuery(12, 25); 

			pomQueryList->SetDoubleSort(1,true);
			pomQueryList->SetDoubleSort(2,true);
			pomQueryList->SetDoubleSort(4,true);
			pomQueryList->SetDoubleSort(5,true);
			pomQueryList->SetDoubleSort(7,true);
			pomQueryList->SetDoubleSort(8,true);
			pomQueryList->SetDoubleSort(9,true);
			pomQueryList->SetDoubleSort(10,true);
			pomQueryList->SetDoubleSort(11,true);
			pomQueryList->SetDoubleSort(12,true);
			pomQueryList->SetDoubleSort(13,true);
		
			break;
		}
	default:
		break;
	}

	
	pomQueryList->Redraw();

	ogDdx.Register(this,S_DLG_FLIGHT_CHANGE,CString("FLIGHTDATA"), CString("Flight changed"),ProcessSearchResultsCf);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
BOOL  SearchResultsDlg::OnLButtonClickedRowCol(UINT wParam, LONG lParam) 
{
	imRow = ((CELLPOS*)lParam)->Row;
	imCol = ((CELLPOS*)lParam)->Col;
	return TRUE;
}

BOOL  SearchResultsDlg::OnLButtonDblClickedRowCol(UINT wParam, LONG lParam) 
{
	imRow = ((CELLPOS*)lParam)->Row;
	imCol = ((CELLPOS*)lParam)->Col;

	/***********
	if(ogPrivList.GetStat("NRFS_DBL_CLICK")!='1')
	{
		return ;
	}
	*******/

	long llUrna = atoi(pomQueryList->GetValueRowCol(imRow, 14));
	long llUrnd = atoi(pomQueryList->GetValueRowCol(imRow, 15));

	switch(imWhichSearch)
	{
	case PS_FLIGHTS:
		{
			if( pogJobDlg == NULL )
			{
				pogJobDlg = new CJobDlg(NULL );
			}
			else
			{
				pogJobDlg->SetActiveWindow();
			}

			FLIGHTDATA *prlFlightA = pomCedaFlightData->GetFlightByUrno(llUrna);
			FLIGHTDATA *prlFlightD = pomCedaFlightData->GetFlightByUrno(llUrnd);


			POSITION rlPos;

			DEMDATA * prlDem;
			omDemMap.RemoveAll();
			if(pomFlightDemMap != NULL)
			{
				CMapPtrToPtr *polDemMap;
				if(llUrna > 0)
				{
					if(pomFlightDemMap->Lookup((void *)llUrna,(void *&)polDemMap)  == TRUE)
					{	
						for ( rlPos =  polDemMap->GetStartPosition(); rlPos != NULL; )
						{
							long llUrno;
							polDemMap->GetNextAssoc(rlPos,(void *&) llUrno,(void *& ) prlDem);
							if(prlDem != NULL)
							{
								omDemMap.SetAt((void *)prlDem->Urno,prlDem);
							}
						}
					}
				}
				if(llUrnd > 0)
				{
					if(pomFlightDemMap->Lookup((void *)llUrnd,(void *&)polDemMap)  == TRUE)
					{	
						for ( rlPos =  polDemMap->GetStartPosition(); rlPos != NULL; )
						{
							long llUrno;
							polDemMap->GetNextAssoc(rlPos,(void *&) llUrno,(void *& ) prlDem);
							if(prlDem != NULL)
							{
								omDemMap.SetAt((void *)prlDem->Urno,prlDem);
							}
						}
					}
				}
			}

		

			pogJobDlg->InitFlightFields((CWnd*)pogCoverageDiagram,pomTsrArray, &omDemMap, prlFlightA,prlFlightD, llUrna, llUrnd,NULL);

		}
		break; 
	default:
		break;
	}


	return TRUE;
}

void SearchResultsDlg::OnOK() 
{

}
void SearchResultsDlg::OnCancel() 
{

	CDialog::OnCancel();
}


void SearchResultsDlg::OnDestroy() 
{
	ogDdx.UnRegister(this,NOTUSED);
	CDialog::OnDestroy();
}

void SearchResultsDlg::DoCreate() 
{
	if (m_hWnd == 0)
	{
		CDialog::Create(SearchResultsDlg::IDD);
		ShowWindow(SW_SHOW);

	}
	else
	{
		BringWindowToTop( );
	}
} 


void SearchResultsDlg::AttachTsrArray(const CStringArray * popTsrArray)
{
	pomTsrArray = popTsrArray;
}

void SearchResultsDlg::AttachFlightDemMap(const CMapPtrToPtr * popFlightDemMap)
{
	pomFlightDemMap = popFlightDemMap;
}


void SearchResultsDlg::AttachFlightData(CedaFlightData *popCedaFlightData)
{
	pomCedaFlightData = popCedaFlightData;
}

void SearchResultsDlg::OnClose() 
{


	CDialog::OnClose();
}

void  SearchResultsDlg::ProcessSearchResultsCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	SearchResultsDlg *This = static_cast<SearchResultsDlg *>(popInstance);

	switch(ipDDXType)
	{
	case S_DLG_FLIGHT_CHANGE :
		{
			FLIGHTDATA *polFlight = static_cast<FLIGHTDATA *>(vpDataPointer);

			for (int i = 0; i < This->omFlights.GetSize(); i++)
			{
				FLIGHTSEARCHDATA *prlSFlight = &This->omFlights[i];
				if (prlSFlight->Urna == polFlight->Urno || prlSFlight->Urnd == polFlight->Urno)
				{
					This->UpdateFields(polFlight,true);
				}
			}
		}
	break;
	}
}

void SearchResultsDlg::UpdateFields(FLIGHTDATA *popFlight,bool bpUpdate)
{
	if (!::IsWindow(this->m_hWnd))
		return;

	CString Flnoa,Flnod;
	CString Orig,Dest;
	CString Rega,Regd;
	CString Act3a,Act3d;
	CGXStyle olStyle;

	int ilLc = omFlights.GetSize();

		
	pomQueryList->LockUpdate(TRUE);
	pomQueryList->GetParam()->SetLockReadOnly(FALSE);

	olStyle.SetEnabled(FALSE);
	olStyle.SetReadOnly(TRUE);

	for(int i = 0; i < ilLc; i++)
	{
		if(omFlights[i].InGrayed)
		{
			olStyle.SetTextColor(this->cmGrayed);
		}
		else
		{
			olStyle.SetTextColor(ogColors[BLACK_IDX]);
		}

		if (omFlights[i].Urna == popFlight->Urno)
		{
			strcpy(omFlights[i].Flnoa,popFlight->Flno);
			strcpy(omFlights[i].Orig,popFlight->Org3);
			strcpy(omFlights[i].Regna,popFlight->Regn);
			strcpy(omFlights[i].Act3a,popFlight->Act3);
			strcpy(omFlights[i].Tifa,popFlight->Stoa.Format("%d.%m.%y - %H%M"));
			strcpy(omFlights[i].PosA,popFlight->Psta);

		}
		else if (omFlights[i].Urnd == popFlight->Urno)
		{
			strcpy(omFlights[i].Flnod,popFlight->Flno);
			strcpy(omFlights[i].Dest,popFlight->Des3);
			strcpy(omFlights[i].Regnd,popFlight->Regn);
			strcpy(omFlights[i].Act3d,popFlight->Act3);
			strcpy(omFlights[i].Tifd,popFlight->Stod.Format("%d.%m.%y - %H%M"));
			strcpy(omFlights[i].PosD,popFlight->Pstd);
		}

		Flnoa = omFlights[i].Flnoa;
		Orig = omFlights[i].Orig;
		Rega = omFlights[i].Regna;
		Act3a = omFlights[i].Act3a;

		pomQueryList->SetStyleRange(CGXRange(i+1, 1, i+1, 1), olStyle.SetValue(omFlights[i].Flnoa));
		pomQueryList->SetStyleRange(CGXRange(i+1, 2, i+1, 2), olStyle.SetValue(omFlights[i].Orig));
		pomQueryList->SetStyleRange(CGXRange(i+1, 3, i+1, 3), olStyle.SetValue(omFlights[i].Tifa));
		pomQueryList->SetStyleRange(CGXRange(i+1, 4, i+1, 4), olStyle.SetValue(omFlights[i].Regna));
		pomQueryList->SetStyleRange(CGXRange(i+1, 5, i+1, 5), olStyle.SetValue(omFlights[i].Act3a));
		pomQueryList->SetStyleRange(CGXRange(i+1, 6, i+1, 6), olStyle.SetValue(omFlights[i].PosA));
		if(omFlights[i].IsLoaded)
		{
			olStyle.SetValue("-->");
			olStyle.SetInterior(ogColors[GREEN_IDX]);
		}
		else
		{
			olStyle.SetValue("==>");
			olStyle.SetInterior(ogColors[YELLOW_IDX]);
		}
		if(omFlights[i].OutGrayed)
		{
			olStyle.SetTextColor(this->cmGrayed);
		}
		else
		{
			olStyle.SetTextColor(ogColors[BLACK_IDX]);
		}

		pomQueryList->SetStyleRange(CGXRange(i+1, 7, i+1, 7), olStyle);

		olStyle.SetInterior(ogColors[WHITE_IDX]);

		Flnod = omFlights[i].Flnod;
		Dest = omFlights[i].Dest;
		Regd = omFlights[i].Regnd;
		Act3d = omFlights[i].Act3d;

		pomQueryList->SetStyleRange(CGXRange(i+1, 8, i+1, 8), olStyle.SetValue(omFlights[i].Flnod));
		pomQueryList->SetStyleRange(CGXRange(i+1, 9, i+1, 9), olStyle.SetValue(omFlights[i].Dest));
		pomQueryList->SetStyleRange(CGXRange(i+1, 10, i+1, 10), olStyle.SetValue(omFlights[i].Tifd));
		pomQueryList->SetStyleRange(CGXRange(i+1, 11, i+1, 11), olStyle.SetValue(omFlights[i].Regnd));
		pomQueryList->SetStyleRange(CGXRange(i+1, 12, i+1, 12), olStyle.SetValue(omFlights[i].Act3d));
		pomQueryList->SetStyleRange(CGXRange(i+1, 13, i+1, 13), olStyle.SetValue(omFlights[i].PosD));
		pomQueryList->SetStyleRange(CGXRange(i+1, 14, i+1, 14), olStyle.SetValue(omFlights[i].Urna));
		pomQueryList->SetStyleRange(CGXRange(i+1, 15, i+1, 15), olStyle.SetValue(omFlights[i].Urnd));
		pomQueryList->SetStyleRange(CGXRange(i+1, 16, i+1, 16), olStyle.SetValue(omFlights[i].SortTifa));
		pomQueryList->SetStyleRange(CGXRange(i+1, 17, i+1, 17), olStyle.SetValue(omFlights[i].SortTifd));


		pomQueryList->SetStyleRange(CGXRange(i+1, 18, i+1, 18), olStyle.SetValue((Flnoa.IsEmpty())?"0":"1"));
		pomQueryList->SetStyleRange(CGXRange(i+1, 19, i+1, 19), olStyle.SetValue((Orig.IsEmpty())?"0":"1"));
		pomQueryList->SetStyleRange(CGXRange(i+1, 20, i+1, 20), olStyle.SetValue((Rega.IsEmpty())?"0":"1"));
		pomQueryList->SetStyleRange(CGXRange(i+1, 21, i+1, 21), olStyle.SetValue((Act3a.IsEmpty())?"0":"1"));
		
		pomQueryList->SetStyleRange(CGXRange(i+1, 22, i+1, 22), olStyle.SetValue((Flnod.IsEmpty())?"0":"1"));
		pomQueryList->SetStyleRange(CGXRange(i+1, 23, i+1, 23), olStyle.SetValue((Dest.IsEmpty())?"0":"1"));
		pomQueryList->SetStyleRange(CGXRange(i+1, 24, i+1, 24), olStyle.SetValue((Regd.IsEmpty())?"0":"1"));
		pomQueryList->SetStyleRange(CGXRange(i+1, 25, i+1, 25), olStyle.SetValue((Act3d.IsEmpty())?"0":"1"));
	}

	pomQueryList->GetParam()->SetLockReadOnly(TRUE);
	pomQueryList->LockUpdate(FALSE);
	pomQueryList->Redraw();

}
