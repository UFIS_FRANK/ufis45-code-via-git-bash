// BaseShiftsGroups.h: interface for the CBaseShiftsGroups class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BASESHIFTSGROUPS_H__A6B65E81_C7A5_11D4_B222_204C4F4F5020__INCLUDED_)
#define AFX_BASESHIFTSGROUPS_H__A6B65E81_C7A5_11D4_B222_204C4F4F5020__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <ccsptrarray.h>

struct BASESHIFTGROUP             
{
	double	WorkingHours;   		
	double	MinPerc;             
	double	MaxPerc;             
	double	ActPerc;             
	double	MinAbs;             
	double	MaxAbs;             
	double	ActAbs;                   
	bool	UsePercent;
	long	GroupIndex;
	int		StartOffset;
	int		EndOffset;	
}; 

class CBaseShiftsGroups  
{
public:
	CBaseShiftsGroups();
	CBaseShiftsGroups(const CBaseShiftsGroups& rhs);
	virtual ~CBaseShiftsGroups();
	CBaseShiftsGroups&	operator=(const CBaseShiftsGroups& rhs);

	void	SetTotalWorkingHours(double dpWorkingHours);
	double	TotalWorkingHours();
	long	AddData(double dpMinVal,double dpMaxVal,bool bpUsePercent,int ipStartOffset,int ipEndOffset);
	double	EvalGroup(long lpGroupIndex);
	int		GetStartOffset(long lpGroupKeyIndex);
	int		GetEndOffset(long lpGroupKeyIndex);
	void	ResetData();
	int		GetShiftGroupCount();
	void	IncreaseActVal(long lpGroupIndex,double dpWorkingHours);
public:
	CCSPtrArray <BASESHIFTGROUP> omBaseShiftsGroups;

private: 
	double	EvalFunction(double dpUpperBound,double dpMinVal,double dpMaxVal,double dpActVal);
private: 
	long	lmMaxGroupIndex;
	long	lmShiftGroupCount;
	double	dmTotalWorkingHours;
	double	dmCoveredWorkingHours;
};

#endif // !defined(AFX_BASESHIFTSGROUPS_H__A6B65E81_C7A5_11D4_B222_204C4F4F5020__INCLUDED_)
