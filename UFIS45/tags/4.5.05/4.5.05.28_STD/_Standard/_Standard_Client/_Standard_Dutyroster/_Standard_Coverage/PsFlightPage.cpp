// PsFlightPage.cpp: Implementierungsdatei
//
 
#include <stdafx.h>
#include <coverage.h>
#include <PsFlightPage.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Eigenschaftenseite CPsFlightPage 

IMPLEMENT_DYNCREATE(CPsFlightPage, CPropertyPage)

CPsFlightPage::CPsFlightPage() : CPropertyPage(CPsFlightPage::IDD)
{
	//{{AFX_DATA_INIT(CPsFlightPage)
	 m_ArrivalVal= FALSE;
	 m_DepatureVal= FALSE;
	 m_BetriebVal= FALSE;
	 m_PlanungVal= FALSE;
	 m_PrognoseVal= FALSE;
	 m_CancelledVal= FALSE;
	 m_NoopVal= FALSE;
	//}}AFX_DATA_INIT
}

CPsFlightPage::~CPsFlightPage()
{
}


void CPsFlightPage::SetCaption(const char *pcpCaption)
{
	strcpy(pcmCaption,pcpCaption);
	m_psp.pszTitle = pcmCaption;
	m_psp.dwFlags |= PSP_USETITLE;
}


void CPsFlightPage::SetData()
{
	CString olText;

	for(int ilLc = 0; ilLc < omValues.GetSize(); ilLc++)
	{
		olText = omValues[ilLc];
		if (olText.Find("ARRI=") == 0)
		{
			if (olText.GetLength() > 5)
			{
				m_ArrivalVal = olText.GetAt(5) == '1' ? TRUE : FALSE;
			}
		}
		else if (olText.Find("DEPA=") == 0)
		{
			if (olText.GetLength() > 5)
			{
				m_DepatureVal = olText.GetAt(5) == '1' ? TRUE : FALSE;
			}
		}
		else if (olText.Find("BETR=") == 0)
		{
			if (olText.GetLength() > 5)
			{
				m_BetriebVal = olText.GetAt(5) == '1' ? TRUE : FALSE;
			}
		}
		else if (olText.Find("PLAN=") == 0)
		{
			if (olText.GetLength() > 5)
			{
				m_PlanungVal = olText.GetAt(5) == '1' ? TRUE : FALSE;
			}
		}
		else if (olText.Find("CANC=") == 0)
		{
			if (olText.GetLength() > 5)
			{
				m_CancelledVal = olText.GetAt(5) == '1' ? TRUE : FALSE;
			}
		}
		else if (olText.Find("PROG=") == 0)
		{
			if (olText.GetLength() > 5)
			{
				m_PrognoseVal = olText.GetAt(5) == '1' ? TRUE : FALSE;
			}
		}
		else if (olText.Find("NOOP=") == 0)
		{
			if (olText.GetLength() > 5)
			{
				m_NoopVal = olText.GetAt(5) == '1' ? TRUE : FALSE;
			}
		}

	}
}
   


void CPsFlightPage::GetData()
{
	CString olText;
	omValues.RemoveAll();	
	olText.Format("ARRI=%c", m_ArrivalVal== TRUE ? '1' : '0');
	omValues.Add(olText);		
	olText.Format("DEPA=%c",m_DepatureVal == TRUE ? '1' : '0');
	omValues.Add(olText);		
	olText.Format("BETR=%c", m_BetriebVal== TRUE ? '1' : '0');
	omValues.Add(olText);		
	olText.Format("PLAN=%c", m_PlanungVal== TRUE ? '1' : '0');
	omValues.Add(olText);		
	olText.Format("PROG=%c", m_PrognoseVal== TRUE ? '1' : '0');
	omValues.Add(olText);		
	olText.Format("CANC=%c", m_CancelledVal== TRUE ? '1' : '0');
	omValues.Add(olText);		
	olText.Format("NOOP=%c", m_NoopVal== TRUE ? '1' : '0');
	omValues.Add(olText);		
}



void CPsFlightPage::DoDataExchange(CDataExchange* pDX)
{

	if (pDX->m_bSaveAndValidate == FALSE)
	{
		SetData();
	}

		enum { IDD = IDD_PSFLIGHT_PAGE };

	CPropertyPage::DoDataExchange(pDX);
	
	//{{AFX_DATA_MAP(CPsFlightPage)
	DDX_Control(pDX, IDC_ARRIVAL2, m_Arrival);
	DDX_Control(pDX, IDC_DEPATURE, m_Depature);
	DDX_Control(pDX, IDC_BETRIEB, m_Betrieb);
	DDX_Control(pDX, IDC_PLANUNG, m_Planung);
	DDX_Control(pDX, IDC_PROGNOSE, m_Prognose);
	DDX_Control(pDX, IDC_CANCELLED, m_Cancelled);
	DDX_Control(pDX, IDC_NOOP, m_Noop);
	DDX_Check(pDX, IDC_ARRIVAL2, m_ArrivalVal);
	DDX_Check(pDX, IDC_DEPATURE, m_DepatureVal);
	DDX_Check(pDX, IDC_BETRIEB,m_BetriebVal );
	DDX_Check(pDX, IDC_PLANUNG, m_PlanungVal);
	DDX_Check(pDX, IDC_PROGNOSE, m_PrognoseVal);
	DDX_Check(pDX, IDC_CANCELLED,m_CancelledVal );
	DDX_Check(pDX, IDC_NOOP, m_NoopVal);
	//}}AFX_DATA_MAP

	if (pDX->m_bSaveAndValidate == TRUE)
	{
		GetData();
	}
}



BEGIN_MESSAGE_MAP(CPsFlightPage, CPropertyPage)
	//{{AFX_MSG_MAP(CPsFlightPage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten CPsFlightPage 

/**********/
BOOL CPsFlightPage::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

	CWnd *polWnd = GetDlgItem(ID_ALLGEMEIN);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61328));
	}
	polWnd = GetDlgItem(IDC_ARRIVAL2);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1531));
	}
	polWnd = GetDlgItem(IDC_DEPATURE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1533));
	}
	polWnd = GetDlgItem(IDC_CANCELLED);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61278));
	}
	polWnd = GetDlgItem(IDC_NOOP);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61279));
	}
	polWnd = GetDlgItem(IDC_BETRIEB);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61276));
	}
	polWnd = GetDlgItem(IDC_PLANUNG);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61277));
	}

	SetWindowText(LoadStg(IDS_STRING61205));

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}
/**************/




