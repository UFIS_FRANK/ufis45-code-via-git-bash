// basicdat.cpp CBasicData class for providing general used methods

#include <stdafx.h>
#include <BasicData.h>
#include <CedaBasicData.h>
#include <GUILng.h>
#include <PrivList.h>
#include <LoginDlg.h>

#define N_URNOS_AT_ONCE 500

static int CompareArray( const CString **e1, const CString **e2);

//------------------------------------------------------------------------------------
/*******************
CString LoadStg(UINT nID)
{
	CString olString = "";
	olString.LoadString(nID);
	int i = olString.Find("*REM*");
	if(i>-1)
		olString = olString.Left(i);
	return olString;
}
***********/
CString LoadStg(UINT nID)
{
	CGUILng* ogGUILng = CGUILng::TheOne(); //!!!Singelton !!! es gibt nur ein Objekt !!!
	return ogGUILng->GetString(nID);
}

//------------------------------------------------------------------------------------

CTime COleDateTimeToCTime(COleDateTime opTime)
{
	CTime olTime = -1;
	if(opTime.GetStatus() == COleDateTime::valid)
	{
		olTime = CTime(opTime.GetYear(), opTime.GetMonth(), opTime.GetDay(), opTime.GetHour(), opTime.GetMinute(), opTime.GetSecond());
	}
	return olTime;
}

COleDateTime CTimeToCOleDateTime(CTime opTime)
{
	COleDateTime olTime;
	olTime.SetStatus(COleDateTime::invalid);
	if(opTime != -1)
	{
		olTime = COleDateTime(opTime.GetYear(), opTime.GetMonth(), opTime.GetDay(), opTime.GetHour(), opTime.GetMinute(), opTime.GetSecond());
	}
	return olTime;
}

//-- ItemList-Functions --------------------------------------------------------------

int GetItemCount(CString olList, char cpTrenner  )
{
	CStringArray olStrArray;
	return ExtractItemList(olList,&olStrArray,cpTrenner);

}


bool GetListItemByField(CString &opDataList, CString &opFieldList, CString &opField, CString &opData)
{
	CStringArray olFields;
	CStringArray olData;
	int ilFields = ExtractItemList(opFieldList, &olFields);
	int ilData   = ExtractItemList(opDataList, &olData);

	for( int i = 0; i < ilFields; i++)
	{
		if(olFields[i] == opField)
		{
			if(ilData > i)
				opData = olData[i];
				return true; 
		}
	}
	return false;
}

bool GetListItemByField(CString &opDataList, CString &opFieldList, CString &opField, CTime &opData)
{
	CStringArray olFields;
	CStringArray olData;
	int ilFields = ExtractItemList(opFieldList, &olFields);
	int ilData   = ExtractItemList(opDataList, &olData);

	for( int i = 0; i < ilFields; i++)
	{
		if(olFields[i] == opField)
		{
			if(ilData > i)
				opData = DBStringToDateTime(olData[i]);
				return true; 
		}
	}
	return false;
}


int ExtractItemList(CString opSubString, CStringArray *popStrArray, char cpTrenner)
{

	CString olText;

	popStrArray->RemoveAll();

	BOOL blEnd = FALSE;
	
	if(!opSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		while(blEnd == FALSE)
		{
			pos = opSubString.Find(cpTrenner);
			if(pos == -1)
			{
				blEnd = TRUE;
				olText = opSubString;
			}
			else
			{
				olText = opSubString.Mid(0, opSubString.Find(cpTrenner));
				opSubString = opSubString.Mid(opSubString.Find(cpTrenner)+1, opSubString.GetLength( )-opSubString.Find(cpTrenner)+1);
			}
			popStrArray->Add(olText);
		}
	}
	return	popStrArray->GetSize();

}


CString GetListItem(CString &opList, int ipPos, bool bpCut, char cpTrenner )
{
	CStringArray olStrArray;
	int ilAnz = ExtractItemList(opList, &olStrArray, cpTrenner);
	CString olReturn;

	if(ipPos == -1)
		ipPos = ilAnz;

	if((ipPos <= ilAnz) && (ipPos > 0))
		olReturn = olStrArray[ipPos - 1];
	if(bpCut)
	{
		opList = "";
		for(int ilLc = 0; ilLc < ilAnz; ilLc++)
		{
			if(ilLc != (ipPos - 1))
			{
				opList = opList + olStrArray[ilLc] + cpTrenner;
			}
		}
	}
	if(bpCut)
		opList = opList.Left(opList.GetLength() - 1);
	return olReturn;
}



CString DeleteListItem(CString &opList, CString olItem)
{
	CStringArray olStrArray;
	int ilAnz = ExtractItemList(opList, &olStrArray);
	opList = "";
	for(int ilLc = 0; ilLc < ilAnz; ilLc++)
	{
			if(olStrArray[ilLc] != olItem)
				opList = opList + olStrArray[ilLc] + ",";
	}
	opList = opList.Left(opList.GetLength() - 1);
	return opList;
}

CString SortItemList(CString opSubString, char cpTrenner)
{
	CString *polText;
	CCSPtrArray<CString> olArray;

	BOOL blEnd = FALSE;
	
	if(!opSubString.IsEmpty())
	{
		int pos;
		int olPos = 0;
		while(blEnd == FALSE)
		{
			polText = new CString;
			pos = opSubString.Find(cpTrenner);
			if(pos == -1)
			{
				blEnd = TRUE;
				*polText = opSubString;
			}
			else
			{
				*polText = opSubString.Mid(0, opSubString.Find(cpTrenner));
				opSubString = opSubString.Mid(opSubString.Find(cpTrenner)+1, opSubString.GetLength( )-opSubString.Find(cpTrenner)+1);
			}
			olArray.Add(polText);
		}
	}

	CString olSortedString;
	olArray.Sort(CompareArray);
	for(int i=0; i<olArray.GetSize(); i++)
	{
		olSortedString += olArray[i] + cpTrenner;
	}
	
	olArray.DeleteAll();
	return olSortedString.Left(olSortedString.GetLength()-1);
}

int SplitItemList(CString opString, CStringArray *popStrArray, int ipMaxItem, char cpTrenner)
{

	CString olSubStr;

	popStrArray->RemoveAll();
	
	CStringArray olStrArray;

	int ilCount = ExtractItemList(opString, &olStrArray);
	int ilSubCount = 0;

	for(int i = 0; i < ilCount; i++)
	{
		if(ilSubCount >= ipMaxItem)
		{
			if(!olSubStr.IsEmpty())
				olSubStr = olSubStr.Right(olSubStr.GetLength() - 1);
			popStrArray->Add(olSubStr);
			ilSubCount = 0;
			olSubStr = "";
		}
		ilSubCount++;
		olSubStr = olSubStr + cpTrenner + olStrArray[i];
	}
	if(!olSubStr.IsEmpty())
	{
		olSubStr = olSubStr.Right(olSubStr.GetLength() - 1);
		popStrArray->Add(olSubStr);
	}
	
	return	popStrArray->GetSize();
}

//-- CBasicDat -------------------------------------------------------------------------

CBasicData::CBasicData(void)
{
	char pclTmpText[512];
	char pclConfigPath[512];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	*pcmCedaCmd = '\0';

	char pclTimeOffSet[20];
	CString olTmpTime;

	imNextGMUAmount = 30;

	GetPrivateProfileString(pcgAppName, "DEBUG", "NO",
    pclTmpText, sizeof pclTmpText, pclConfigPath);
	bmDebugMode = (strcmp(pclTmpText,"YES") == 0) ? true : false;
	
	GetPrivateProfileString(pcgAppName, "SHOWGRAPHIC", "YES",
    pclTmpText, sizeof pclTmpText, pclConfigPath);
	bmShowWindows[0] = (strcmp(pclTmpText,"YES") == 0) ? true : false;
	
	GetPrivateProfileString(pcgAppName, "SHOWSHIFTDEMAND", "YES",
    pclTmpText, sizeof pclTmpText, pclConfigPath);
	bmShowWindows[1] = (strcmp(pclTmpText,"YES") == 0) ? true : false;
	
	GetPrivateProfileString(pcgAppName, "SHOWBASESHIFTS", "YES",
    pclTmpText, sizeof pclTmpText, pclConfigPath);
	bmShowWindows[2] = (strcmp(pclTmpText,"YES") == 0) ? true : false;

	GetPrivateProfileString(pcgAppName, "CHECKMASTERWEEK", "YES",
    pclTmpText, sizeof pclTmpText, pclConfigPath);
	bmCheckMasterWeek = (strcmp(pclTmpText,"YES") == 0) ? true : false;

	SYSTEMTIME olOrgTmpTime;

	bool blSysTimeSet = false;
	GetPrivateProfileString(pcgAppName, "CURRENTDATE", "",
    pclTmpText, sizeof pclTmpText, pclConfigPath);
	 if (*pclTmpText != '\0')
	 {

		 SYSTEMTIME olTmpTime;

		GetSystemTime(&olOrgTmpTime);
		GetSystemTime(&olTmpTime);
		olTmpTime.wYear = atoi(&pclTmpText[6]);
		olTmpTime.wMonth = atoi(&pclTmpText[3]);
		olTmpTime.wDay = atoi(&pclTmpText[0]);
	
		SetSystemTime(&olTmpTime);
		blSysTimeSet = true;
			
	 }




	GetPrivateProfileString(pcgAppName, "LOADREL", "YES",
    pclTmpText, sizeof pclTmpText, pclConfigPath);
	bmLoadRel = (strcmp(pclTmpText,"YES") == 0) ? true : false;

	CTime olLoadStartTime = CTime::GetCurrentTime();

	GetPrivateProfileString(pcgAppName, "LOADOFFSET", "0",
        pclTmpText, sizeof pclTmpText, pclConfigPath);
	int ilTimeoffset = atoi(pclTmpText);
//	olLoadStartTime = CTime(olLoadStartTime.GetYear(),olLoadStartTime.GetMonth(),olLoadStartTime.GetDay(),0,0,0);
	olLoadStartTime += CTimeSpan(ilTimeoffset,0,0,0);
/*
	olTmpTime = olLoadStartTime.Format("%Y%m%d%H%M%S");
	omLoadTimeValues.Add(olTmpTime);
*/

	GetPrivateProfileString(pcgAppName, "LOADDURATION", "1",
        pclTmpText, sizeof pclTmpText, pclConfigPath);
	int ilTimeduration = max(1,atoi(pclTmpText));
	CTime olLoadEndTime = olLoadStartTime + CTimeSpan(ilTimeduration,0,0,0);

	olLoadStartTime = CTime(olLoadStartTime.GetYear(),olLoadStartTime.GetMonth(),olLoadStartTime.GetDay(),0,0,0);
	olLoadEndTime = CTime(olLoadEndTime.GetYear(),olLoadEndTime.GetMonth(),olLoadEndTime.GetDay(),00,00,00);

	olTmpTime = olLoadStartTime.Format("%Y%m%d%H%M%S");
	omLoadTimeValues.Add(olTmpTime);

	olTmpTime = olLoadEndTime.Format("%Y%m%d%H%M%S");
	omLoadTimeValues.Add(olTmpTime);


	GetPrivateProfileString(pcgAppName, "LOADBEFORE", "2",
        pclTimeOffSet, sizeof pclTimeOffSet, pclConfigPath);
	int ilTimebef = atoi(pclTimeOffSet);
	olTmpTime = pclTimeOffSet;
	omLoadTimeValues.Add(olTmpTime);

	GetPrivateProfileString(pcgAppName, "LOADAFTER", "6",
        pclTimeOffSet, sizeof pclTimeOffSet, pclConfigPath);
	int ilTimeaft = atoi(pclTimeOffSet);

	olTmpTime = pclTimeOffSet;
	omLoadTimeValues.Add(olTmpTime);



	omDiaStartTime = CTime::GetCurrentTime();

	if(bmLoadRel)
	{
		omDiaStartTime -= CTimeSpan(0,ilTimebef , 0, 0);
		omDiaEndTime = omDiaStartTime + CTimeSpan(0, ilTimeaft, 0, 0);
	}
	else
	{
		omDiaStartTime = olLoadStartTime;
		omDiaEndTime = olLoadEndTime;
	}

	if(blSysTimeSet)
	{
		 SetSystemTime(&olOrgTmpTime);
	}
	GetPrivateProfileString(pcgAppName, "DISPDUR", "12",
    pclTmpText, sizeof pclTmpText, pclConfigPath);
	imDisplayDuration = atoi(pclTmpText);

	GetPrivateProfileString(pcgAppName, "BEWTOTALPATH", "C:\\Rule-Eval.txt",
    pclTmpText, sizeof pclTmpText, pclConfigPath);
	omBewTotalPath = pclTmpText;

	GetPrivateProfileString(pcgAppName, "BEWTIMEPATH", "C:\\Rulep.txt",
    pclTmpText, sizeof pclTmpText, pclConfigPath);
	omBewTimePath = pclTmpText;

	GetPrivateProfileString(pcgAppName, "EDITOR", "c:\\winnt\\notepad",
    pclTmpText, sizeof pclTmpText, pclConfigPath);
	omEditorPath = pclTmpText;

	//omEditor = CString("notepad");
	CStringArray olEditorList;
	int i= 1;

	omEditorPath.Replace("\\","/");
	char clTrenner = '/';
	ExtractItemList(omEditorPath,&olEditorList,clTrenner);
	int ilSize = olEditorList.GetSize();
	if(ilSize > 0)
	{
		omEditor = olEditorList[ilSize-1];
	}



	GetPrivateProfileString(pcgAppName, "MONITORCOUNT", "1",
	pclTmpText, sizeof pclTmpText, pclConfigPath);
	imMonitorCount = (max(atoi(pclTmpText),1));
	if (imMonitorCount == 0)
		imMonitorCount = 1;

	imScreenResolutionY = GetSystemMetrics(SM_CYSCREEN);
	imScreenResolutionX = (int) GetSystemMetrics(SM_CXSCREEN) / imMonitorCount;



	// now reading the ceda commands
	char pclComandBuf[24];
	omDefaultComands.Empty();
	omActualComands.Empty();
 
    GetPrivateProfileString(pcgAppName, "EIOHDL", "LLF",
		pclTmpText, sizeof pclTmpText, pclConfigPath);
	sprintf(pclComandBuf," %3s ","LLF");
	omDefaultComands += pclComandBuf;
	sprintf(pclComandBuf," %3s ",pclTmpText);
	omActualComands += pclComandBuf;
	char pclDaysToRead[10]="";
	GetPrivateProfileString(pcgAppName, "DAYSTOREAD", "1",
      pclDaysToRead, sizeof pclDaysToRead, pclConfigPath);
	igDaysToRead = atoi(pclDaysToRead);

	omTich		 = CTime::GetCurrentTime();
	omLocalDiff1 = CTimeSpan(0,0,0,0);
	omLocalDiff2 = CTimeSpan(0,0,0,0);

	this->pomNameConfDlg = NULL;

	GetPrivateProfileString(pcgAppName, "RULEDIAGNOSIS", "NO",
    pclTmpText, sizeof pclTmpText, pclConfigPath);
	bmRuleDiagnosis = (strcmp(pclTmpText,"YES") == 0) ? true : false;

/***
	GetPrivateProfileString(pcgAppName, "USER", "-1",pclTmpText, sizeof pclTmpText, pclConfigPath);
	CString olDmy;
	olDmy = pclTmpText;
	if (olDmy!="-1")
		omUserName=Decode(olDmy);
	else
		omUserName="";

	GetPrivateProfileString(pcgAppName, "PASS", "-1",pclTmpText, sizeof pclTmpText, pclConfigPath);
	olDmy = pclTmpText;
	if (olDmy!="-1")
		omPassword=Decode(olDmy);
	else
		omPassword="";
****/

	GetPrivateProfileString(pcgAppName, "SERVERPATH", "-1",pclTmpText, sizeof pclTmpText, pclConfigPath);
	CString olDmy;
	olDmy = pclTmpText;
	if (olDmy!="-1")
		omServerPath=olDmy;
	else
		omServerPath="/debug/ruldiag";

	GetPrivateProfileString(pcgAppName, "FILTER", "-1",pclTmpText, sizeof pclTmpText, pclConfigPath);
	olDmy = pclTmpText;
	if (olDmy!="-1")
		omFileFilter=olDmy;
	else
		omFileFilter="*.diag";

	GetPrivateProfileString(pcgAppName, "USEEVALUATIONFACTOR", "YES",
    pclTmpText, sizeof pclTmpText, pclConfigPath);
	bmUseEvaluationFactor = (strcmp(pclTmpText,"YES") == 0) ? true : false;

	GetPrivateProfileString(pcgAppName, "DEBUGAUTOCOVERAGE", "NO",
    pclTmpText, sizeof pclTmpText, pclConfigPath);
	bmDebugAutoCoverage = (strcmp(pclTmpText,"YES") == 0) ? true : false;

	GetPrivateProfileString(pcgAppName, "DUMPAUTOCOVERAGE", "NO",
    pclTmpText, sizeof pclTmpText, pclConfigPath);
	bmDumpAutoCoverage = (strcmp(pclTmpText,"YES") == 0) ? true : false;

}


CBasicData::~CBasicData(void)
{
	if (this->omColorConfData.GetSize() > 0)
		omColorConfData.DeleteAll();

	delete this->pomNameConfDlg;
}

bool CBasicData::GetLoadRelFlag()
{
	return bmLoadRel;
}

int CBasicData::GetDisplayDuration()
{
	return imDisplayDuration;
}

void CBasicData::SetLoadRelFlag(bool bpLoadRel)
{
	bmLoadRel = bpLoadRel;
}


long CBasicData::GetNextUrno(void)
{
	bool	olRc = true;
	long	llNextUrno = 0L;
	

	if (omUrnos.GetSize() == 0)
	{
		olRc = GetNurnos(imNextGMUAmount);

		if(imNextGMUAmount <= 240)
		{
			imNextGMUAmount = imNextGMUAmount + imNextGMUAmount;

		}

	}

	if (omUrnos.GetSize() > 0)
	{
		llNextUrno = omUrnos[0];
		omUrnos.RemoveAt(0);

		if ( (llNextUrno != 0L) && (olRc == true) )
		{
			return(llNextUrno);
		}
	}
	::MessageBox(NULL,LoadStg(IDS_STRING444),LoadStg(IDS_STRING445),MB_OK);
	return -1;	
}

void CBasicData::PushBackUrno(long lpUrno)
{
	//omUrnos.Add(lpUrno);
}

bool CBasicData::GetNurnos(int ipNrOfUrnos)
{
	bool	ilRc = false;
	char 	pclTmpDataBuf[12*N_URNOS_AT_ONCE];
	sprintf(pclTmpDataBuf, "%d", ipNrOfUrnos);

	ilRc = CedaAction("GMU", "", "", pclTmpDataBuf);

	if (ilRc == true)
	{
		for ( int ilItemNo=1; (ilItemNo <= ipNrOfUrnos) ; ilItemNo++ )
		{
			char pclTmpBuf[64];


			GetItem(ilItemNo, pclTmpDataBuf, pclTmpBuf);

			long llNewUrno = atol(pclTmpBuf);

			omUrnos.Add(llNewUrno);
		}
	}

	return ilRc;
}



void CBasicData::GetDiagramStartTime(CTime &opStart, CTime &opEnd)
{
	opStart = omDiaStartTime;
	opEnd = omDiaEndTime;
	return;
}


void CBasicData::SetDiagramStartTime(CTime opDiagramStartTime, CTime opDiagramEndTime)
{
	omDiaStartTime = opDiagramStartTime;
	omDiaEndTime = opDiagramEndTime;
}


void CBasicData::SetWorkstationName(CString opWsName)
{
	omWorkstationName = CString("WKS234");//opWsName;
}


CString CBasicData::GetWorkstationName()
{
	return omWorkstationName;
}

CString CBasicData::GetBewTotalPath()
{
	return omBewTotalPath;
}

CString CBasicData::GetBewTimePath()
{
	return omBewTimePath;
}

CString CBasicData::GetEditor()
{
	return omEditor;
}

CString CBasicData::GetEditorPath()
{
	return omEditorPath;
}

bool CBasicData::GetWindowPosition(CRect& rlPos,CString olMonitor)
{

	int XResolution = 1024;
	int YResolution = 768;
	if (ogCfgData.rmUserSetup.RESO[0] == '8')
	{
		XResolution = 800;
		YResolution = 600;
	}
	else
	{
		if (ogCfgData.rmUserSetup.RESO[0] == '1')
		{
			if (ogCfgData.rmUserSetup.RESO[1] == '0')
			{
				XResolution = 1024;
				YResolution = 768;
			}
		}
		else
		{
			XResolution = 1280;
			YResolution = 1024;
		}
	}
  

	int ilMonitor = 0;
	if (olMonitor[0] == 'L')
		ilMonitor = 0;
	if (olMonitor[0] == 'M')
		ilMonitor = 1;
	if (olMonitor[0] == 'R')
		ilMonitor = 2;
	
	rlPos.top = ilMonitor == 0 ? 56 : 0;
	rlPos.bottom = YResolution;
	rlPos.left = XResolution * ilMonitor;
	rlPos.right = XResolution * (ilMonitor+1);

	return true;
}int CBasicData::GetUtcDifference(void)
{

	static int ilUtcDifference = 1;
	static BOOL blIsInitialized = FALSE;

	if (blIsInitialized == FALSE)
	{
		tm *_tm;
		time_t    now;
		
		int hour_gm,hour_local;
		blIsInitialized = TRUE;

		now = time(NULL);
		_tm = (struct tm *)gmtime(&now);
		hour_gm = _tm->tm_hour;
		_tm = (struct tm *)localtime(&now);
		hour_local = _tm->tm_hour;
		if (hour_gm > hour_local)
		{
			ilUtcDifference = ((hour_local+24-hour_gm)*3600);
		}
		else
		{
			ilUtcDifference = ((hour_local-hour_gm)*3600);
		}
	}

	return ilUtcDifference;

}

BOOL CBasicData::SetLocalDiff()
{
	BOOL	blResult = TRUE;
	int		ilTdi1;
	int		ilTdi2; 
	int		ilMin;
	int		ilHour;
	CTime	olTich ;	
	CTime olCurr;
	RecordSet *prlRecord;	

	CCSPtrArray<RecordSet> olData;

	CString olWhere = CString("WHERE APC3 = '") + CString(pcgHome) + CString("'");

	if(ogBCD.ReadSpecial( "APT", "TICH,TDI1,TDI2", olWhere, olData))
	{
		if(olData.GetSize() > 0)
		{
			prlRecord = &olData[0];
		
			CString olTime = prlRecord->Values[0] +"00";
			olTich = DBStringToDateTime(olTime);
			if (olTich == TIMENULL)
			{
				blResult = FALSE;
			}
			else
			{
				omTich = olTich;
			}

			if (strlen(prlRecord->Values[1]) > 0)
			{
				ilTdi1 = atoi(prlRecord->Values[1]);
				ilHour = ilTdi1 / 60;
				ilMin  = ilTdi1 % 60;
				omLocalDiff1 = CTimeSpan(0,ilHour ,ilMin ,0);
			}
			else
			{
				blResult = FALSE;
			}

			if (strlen(prlRecord->Values[1]) > 0)
			{
				ilTdi2 = atoi(prlRecord->Values[2]);
				ilHour = ilTdi2 / 60;
				ilMin  = ilTdi2 % 60;
				omLocalDiff2 = CTimeSpan(0,ilHour ,ilMin ,0);
			}
			else
			{
				blResult = FALSE;
			}
		}
		olData.DeleteAll();
	}
	else
	{
		blResult = FALSE;
	}

	return blResult;
}

bool CBasicData::GetCalcToLocal()
{
	return true;

}

void CBasicData::LocalToUtc(CTime &opTime)
{
	if(opTime != TIMENULL)
	{
		if(opTime < omTich)
		{
			opTime -= omLocalDiff1;
		}
		else
		{
			opTime -= omLocalDiff2;
		}
	}

}

void CBasicData::UtcToLocal(CTime &opTime)
{
	CTime olTichUtc;
	olTichUtc = omTich - CTimeSpan(omLocalDiff2);

	if(opTime != TIMENULL)
	{
		if(opTime < olTichUtc)
		{
			opTime += omLocalDiff1;
		}
		else
		{
			CString olFromDate;
			olFromDate.Format("%04d%02d%02d%02d%02d", omTich.GetYear(), omTich.GetMonth(), omTich.GetDay(), omTich.GetHour()-2, omTich.GetMinute());
			CString olToDate;
			olToDate.Format("%04d%02d%02d%02d%02d", omTich.GetYear(), omTich.GetMonth(), omTich.GetDay(), omTich.GetHour()-1, omTich.GetMinute());
			CString olOpDate = opTime.Format("%Y%m%d%H%M");
			if(olOpDate >= olFromDate && olOpDate <= olToDate)
			{
				opTime += omLocalDiff1;
			}
			else
			{
				opTime += omLocalDiff2;
			}
		}
	}
}


static int CompareArray( const CString **e1, const CString **e2)
{
	return (strcmp((**e1),(**e2)));
}

BOOL CBasicData::IsEquipmentAvailable()
{
	static int ilRc = -1;
	if (ilRc == -1)
	{
		char pclWhere[100], pclFields[100] = "TANA,FINA";
		char pclTable[100];
		sprintf(pclWhere,"WHERE Tana = 'EQT'");
		sprintf(pclTable,"SYS%s",pcgTableExt);

		ilRc = CedaAction("RT", pclTable, pclFields, pclWhere, "", "BUF1");
	}

	if (ilRc)
		return true;
	else
		return false;
		
}


BOOL CBasicData::IsBrosAvailable()
{
	static int ilRc = -1;
	if (ilRc == -1)
	{
		char pclWhere[100], pclFields[100] = "TANA,FINA";
		char pclTable[100];
		sprintf(pclWhere,"WHERE Tana = 'DRR' AND Fina = 'BROS'");
		sprintf(pclTable,"SYS%s",pcgTableExt);

		ilRc = CedaAction("RT", pclTable, pclFields, pclWhere, "", "BUF1");
	}

	if (ilRc)
		return true;
	else
		return false;
		
}

BOOL CBasicData::IsUtplAvailable()
{
	static int ilRc = -1;
	if (ilRc == -1)
	{
		char pclWhere[100], pclFields[100] = "TANA,FINA";
		char pclTable[100];
		sprintf(pclWhere,"WHERE Tana = 'RUD' AND Fina = 'UTPL'");
		sprintf(pclTable,"SYS%s",pcgTableExt);

		ilRc = CedaAction("RT", pclTable, pclFields, pclWhere, "", "BUF1");
	}

	if (ilRc)
		return true;
	else
		return false;
		
}


BOOL CBasicData::IsDebugMode()
{
	return bmDebugMode;
}

BOOL CBasicData::UseEvaluationFactor()
{
	return this->bmUseEvaluationFactor;
}

BOOL CBasicData::DumpAutoCoverage()
{
	return this->bmDumpAutoCoverage;
}

BOOL CBasicData::DebugAutoCoverage()
{
	return this->bmDebugAutoCoverage;
}

// read the conflict types from the DB
void CBasicData::ReadAllConfigData(void)
{
	// get program default conflicts
	CStringArray olProgLines;
	ogCfgData.GetDefaultColorSetup(olProgLines);
	
	// get conflicts for default user
	CStringArray olDefaultLines;
	bool ilRc = ogCfgData.ReadColors(ogCfgData.pomDefaultName,olDefaultLines);

	// add new programmed conflict types to the default user conflicts
	if (olDefaultLines.GetSize() < olProgLines.GetSize())
	{
		for (int ilLc = olDefaultLines.GetSize(); ilLc < olProgLines.GetSize();ilLc++)
		{
			olDefaultLines.Add(olProgLines[ilLc]);
			// write default user records
			CFGDATA rlCfg;
			ogCfgData.CfgRecordFromColor(ogCfgData.pomDefaultName,olProgLines[ilLc],rlCfg);
			ogCfgData.SaveColorData(ogCfgData.pomDefaultName,&rlCfg);
		}

	}

	// get conflicts for current user
	CStringArray olLines;
	ilRc = ogCfgData.ReadColors(omUserID,olLines);
	
	// add new programmed conflict types to the current user conflicts
	if (olLines.GetSize() < olDefaultLines.GetSize())
	{
		for (int ilLc = olLines.GetSize(); ilLc < olDefaultLines.GetSize();ilLc++)
		{
			olLines.Add(olDefaultLines[ilLc]);
			// write current user records
			CFGDATA rlCfg;
			ogCfgData.CfgRecordFromColor(omUserID,olDefaultLines[ilLc],rlCfg);
			ogCfgData.SaveColorData(omUserID,&rlCfg);
		}

	}

	int ilRecCount = 0;
	for(int i = 0; i < olLines.GetSize(); i++)
	{
		CString olRecord = olLines.GetAt(i);
		int ilMaxFields = 4;
		int ilMaxBytes  = olRecord.GetLength();
		COLORCONFDATA *prlColorConfData = new COLORCONFDATA;

		for (int ilFieldCount = 0, ilByteCount = 0; ilFieldCount < ilMaxFields; ilFieldCount++)
    	{
        // Extract next field in the specified text-line
			for (int ilFirstByteInField = ilByteCount; ilByteCount < ilMaxBytes && olRecord[ilByteCount] != ';'; ilByteCount++)
				;
			if(ilByteCount <= ilMaxBytes)
			{
				CString olField = olRecord.Mid(ilFirstByteInField,ilByteCount-ilFirstByteInField);
				ilByteCount++;
				switch(ilFieldCount)
				{
				case 0 : prlColorConfData->Index = atol((LPCSTR)olField);
					break;
				case 1 : prlColorConfData->Type = atol((LPCSTR)olField);
					break;
				case 2 : prlColorConfData->Name = olField;
					break;
				case 3 : prlColorConfData->Color = (COLORREF) atol((LPCSTR)olField);
					break;
				}
			}
		}
		omColorConfData.Add(prlColorConfData);
		ilRecCount++;
	}
}

int CBasicData::GetColorConfData(CCSPtrArray<COLORCONFDATA>& ropArray)
{
	ropArray.DeleteAll();

	int ilSize = omColorConfData.GetSize();
	for (int ilC = 0; ilC < ilSize; ilC++)
	{
		ropArray.NewAt(ilC,omColorConfData.ElementAt(ilC));
	}

	return ropArray.GetSize();
}

BOOL CBasicData::SetColorConfData(int ipIndex,const COLORCONFDATA& ropData)
{
	CFGDATA rlCfg;

	if (ipIndex >= 0 && ipIndex < NUMCONFCOLORS)
	{
		rlCfg.Urno = GetNextUrno();
		sprintf(rlCfg.Ctyp, "COLOR%02d", ipIndex);
		sprintf(rlCfg.Pkno, omUserID);
		sprintf(rlCfg.Text,"%d;%ld;%s;%ld",
		ipIndex,
		ropData.Type,
		ropData.Name,
		ropData.Color
		);

		// Save the data
		if (ogCfgData.SaveColorData(ogBasicData.omUserID,&rlCfg) == false)
			return FALSE;

		COLORCONFDATA *prlData = &omColorConfData[ipIndex];
		*prlData = ropData;

		return TRUE;
	}
	else
		return FALSE;
}

void CBasicData::SetWindowStat(const char *pcpKey, CWnd *popWnd)
{
	if(popWnd != NULL)
	{
		char clStat;
		clStat = ogPrivList.GetStat(pcpKey);
		if(clStat == '1') // displayed and enabled
		{
			popWnd->ShowWindow(SW_SHOW);
			popWnd->EnableWindow(TRUE);
		}
		else if(clStat == '-') // hidden
		{
			popWnd->ShowWindow(SW_HIDE);
		}
		else // displayed and disabled
		{
			popWnd->ShowWindow(SW_SHOW);
			popWnd->EnableWindow(FALSE);
		}
	}
}

bool CBasicData::GetDispoTimeframeWhereString(CTime opStartTime,CTime opEndTime,CString &ropWhere)
{
	CString olWhere;
	CString olTmpTifaWhere;
	CString olTmpTifdWhere;

	bool blIsTifSet = false;

	CTime olTime1 = opStartTime;
	CTime olTime2 = opEndTime;
	//RST!
	
	CString T1 = olTime1.Format("%d.%m.%Y-%H:%M");
	CString T2 = olTime2.Format("%d.%m.%Y-%H:%M");
	ogBasicData.LocalToUtc(olTime1);
	ogBasicData.LocalToUtc(olTime2);
	T1 = olTime1.Format("%d.%m.%Y-%H:%M");
	T2 = olTime2.Format("%d.%m.%Y-%H:%M");
	CString olOrg3 = "";
	olOrg3.Format(" AND DES3 = '%s')",pcgHome);
	ropWhere += olTime1.Format("((TIFA BETWEEN '%Y%m%d%H%M00' AND ") + olTime2.Format("'%Y%m%d%H%M00'") +olOrg3  +
				olTime1.Format(" OR (STOA BETWEEN '%Y%m%d%H%M00' AND ") + olTime2.Format("'%Y%m%d%H%M00'") + olOrg3 + CString(")");
	
	ropWhere += " OR ";

	CString olDes3 = "";
	olDes3.Format(" AND ORG3 = '%s')",pcgHome);
	ropWhere += olTime1.Format("((TIFD BETWEEN '%Y%m%d%H%M00' AND ") + olTime2.Format("'%Y%m%d%H%M00'") + olDes3  + 
				olTime1.Format(" OR (STOD BETWEEN '%Y%m%d%H%M00' AND ") + olTime2.Format("'%Y%m%d%H%M00'") + olDes3 +CString(")");
	if(!ropWhere.IsEmpty())
	{
		ropWhere = CString("(") + ropWhere + CString(")");
	}

	return true;	
}

//-----------------------------------------------------------------------------------------------------------------------

//  String von TSR.TEXT:
//	'NAME(IDS_XXX)#ACWS(IDS_WINGSPAN)|SEAT(IDS_SITZE)'	-->
//  ropNames[0]= STRG from TXTTAB where TXID='IDS_XXX'
//	ropNames[1]= STRG from TXTTAB where TXID='IDS_WINGSPAN'
//	ropNames[2]= STRG from TXTTAB where TXID='IDS_SITZE'

bool GetNameOfTSRRecord ( CString &ropUrno, CString &ropName )
{
	RecordSet	olRecord;
	CString		olTSRText;
	int			n, ilTsrTextIdx, ilTsrNameIdx;

	if  ( !ogBCD.GetRecord ( "TSR", "URNO", ropUrno, olRecord ) )
		return false;
	
	ilTsrTextIdx = ogBCD.GetFieldIndex ( "TSR", "TEXT" );
	ilTsrNameIdx = ogBCD.GetFieldIndex ( "TSR", "NAME" );

	olTSRText = olRecord.Values[ilTsrTextIdx];
	n = olTSRText.Find('#') ;
	if ( n>0 ) 
	{
		olTSRText = olTSRText.Left ( n-1 );
		if ( GetStringForTSRTextEntry ( olTSRText, ropName ) )
			return true;
	}
	//  keine (vorhandene) TXID in TSR.TEXT gefunden 
	ropName = olRecord.Values[ilTsrNameIdx];
	return true;
}

//-----------------------------------------------------------------------------------------------------------------------

bool GetStringForTSRTextEntry ( CString opEntry, CString &ropString )
{
	char	pclText[41], *ps;
	CString	olStrID, olUrno ;
	
	if ( ogBCD.GetDataCount("TXT") <= 0 )
		return false;

	strcpy ( pclText, opEntry.Left (40) );
	ps = strchr ( pclText, ')' );
	if ( ps )
		*ps = '\0';
	ps = strchr ( pclText, '(' );
	olStrID = ps ? ps+1 : pclText;

	olUrno = ogBCD.GetFieldExt( "TXT", "TXID", "APPL", olStrID, "TSRTAB", "URNO" );
	if ( !olUrno.IsEmpty() )
		return ogBCD.GetField( "TXT", "URNO", olUrno, "STRG", ropString ) ;
	else 
		return false;
}

//-----------------------------------------------------------------------------------------------------------------------
BOOL CBasicData::ShowNameConfDlg(CWnd *popParent)
{
	if (this->pomNameConfDlg == NULL)
	{
		CStringArray olArr;
		olArr.Add(LoadStg(IDS_STRING1911));
		olArr.Add(LoadStg(IDS_STRING1912));
		olArr.Add(LoadStg(IDS_STRING1913));
		olArr.Add(LoadStg(IDS_STRING1914));
		olArr.Add(LoadStg(IDS_STRING1915));
		olArr.Add(LoadStg(IDS_STRING1916));
		olArr.Add(LoadStg(IDS_STRING1917));
		olArr.Add(LoadStg(IDS_STRING1918));
		olArr.Add(LoadStg(IDS_STRING1919));
		olArr.Add(LoadStg(IDS_STRING1920));

		this->pomNameConfDlg = new NameConfigurationDlg(&olArr,"");
		this->pomNameConfDlg->SetConfigString(ogCfgData.rmUserSetup.NAME);
	}

	if (this->pomNameConfDlg->DoModal() == IDOK)
	{
		ogCfgData.rmUserSetup.NAME = this->pomNameConfDlg->GetConfigString();
		return TRUE;
	}
	else
		return FALSE;
}

//-----------------------------------------------------------------------------------------------------------------------
CString CBasicData::GetFormattedEmployeeName(STFDATA *popStfData)
{
	if (this->pomNameConfDlg == NULL)
	{
		CStringArray olArr;
		olArr.Add(LoadStg(IDS_STRING1911));
		olArr.Add(LoadStg(IDS_STRING1912));
		olArr.Add(LoadStg(IDS_STRING1913));
		olArr.Add(LoadStg(IDS_STRING1914));
		olArr.Add(LoadStg(IDS_STRING1915));
		olArr.Add(LoadStg(IDS_STRING1916));
		olArr.Add(LoadStg(IDS_STRING1917));
		olArr.Add(LoadStg(IDS_STRING1918));
		olArr.Add(LoadStg(IDS_STRING1919));
		olArr.Add(LoadStg(IDS_STRING1920));

		this->pomNameConfDlg = new NameConfigurationDlg(&olArr,"");
		this->pomNameConfDlg->SetConfigString(ogCfgData.rmUserSetup.NAME);
	}


	return this->pomNameConfDlg->GetNameString(popStfData->Finm,popStfData->Lanm,popStfData->Shnm,popStfData->Perc,popStfData->Peno);	
}

//-----------------------------------------------------------------------------------------------------------------------
CString CBasicData::GetNameConfigString()
{
	return ogCfgData.rmUserSetup.NAME;
}

//-----------------------------------------------------------------------------------------------------------------------
void CBasicData::SetNameConfigString(const CString& ropString)
{
	ogCfgData.rmUserSetup.NAME = ropString;
}

BOOL CBasicData::IsRuleDiagnosisEnabled()
{
	return this->bmRuleDiagnosis;
}

CString CBasicData::Decode(const CString& cText)
{
	CString ret="";

	CString MASTERKEY="apfelabc";

	char key[100];
	char line[100];
	char temp[100];
	int keyPos=0;

	sprintf(line,cText);
	sprintf(key,MASTERKEY);

	int i,lenKey=strlen(key);

        for (i=0 ; i<=strlen(line) ; i++) 
		{
            if (line[i]>=32 && line[i]<=126) 
			{
				temp[i] = 32 + line[i] - key[keyPos];
				if (temp[i]<32) temp[i] += 95;
					keyPos++;
				if (keyPos >= lenKey) keyPos=0;
			}
			else temp[i] = line[i];
		}

	ret.Format("%s",temp);

	ret=ret.Left(ret.GetLength()-MASTERKEY.GetLength());

	return ret;
}

CString	CBasicData::GetUserName()	
{ 
	if (omUserName.GetLength() == 0)
	{
		CLoginDialog olLoginDlg(pcgHome,ogAppName,AfxGetMainWnd(),true);
		if( olLoginDlg.DoModal() != IDCANCEL )
		{
			omUserName = olLoginDlg.omUsername;
			omPassword = olLoginDlg.omPassword;
		}
	}

	return omUserName;	
}

CString	CBasicData::GetPassword()	
{ 
	return omPassword;
}

CString CBasicData::GetDrrIndexHint()
{
	static char pchBuffer[256] = "";

	if (strlen(pchBuffer) == 0)
	{
		char pclConfigPath[512];

		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));

		::GetPrivateProfileString("Global","DrrIndexHint","/*+ INDEX(DRRTAB DRRTAB_SDAY_ROSL) */",pchBuffer,sizeof(pchBuffer),pclConfigPath);
	}

	return pchBuffer;
}