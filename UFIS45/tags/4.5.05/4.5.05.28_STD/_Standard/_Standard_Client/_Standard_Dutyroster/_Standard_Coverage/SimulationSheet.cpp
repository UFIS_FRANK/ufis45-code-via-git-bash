// SimulationSheet.cpp : implementation file
//

#include <stdafx.h>
#include <coverage.h>
#include <SimulationSheet.h>
#include <CoverageDiaViewer.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SimulationSheet

IMPLEMENT_DYNAMIC(SimulationSheet, CPropertySheet)

SimulationSheet::SimulationSheet(UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(nIDCaption, pParentWnd, iSelectPage)
{
	pomViewer = NULL;
}

SimulationSheet::SimulationSheet(LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(pszCaption, pParentWnd, iSelectPage)
{
	pomViewer = NULL;
}

SimulationSheet::SimulationSheet(UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage,CoverageDiagramViewer *popViewer)
	:CPropertySheet(nIDCaption, pParentWnd, iSelectPage)
{
	pomViewer = popViewer;
	AddPage(&omSimulationTypePage);
	this->omSimulationCutPage.bmSmooth = false;
	pomCurrentPage = NULL;

	omSimulationTypePage.SetCaption(LoadStg(IDS_STRING1811));
	omSimulationOffsetPage.SetCaption(LoadStg(IDS_STRING1825));
	omSimulationCutPage.SetCaption(LoadStg(IDS_STRING1816));
	omSimulationSmoothPage.SetCaption(LoadStg(IDS_STRING1817));
}

SimulationSheet::SimulationSheet(LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage,CoverageDiagramViewer *popViewer)
	:CPropertySheet(pszCaption, pParentWnd, iSelectPage)
{
	pomViewer = popViewer;
	AddPage(&omSimulationTypePage);
	this->omSimulationCutPage.bmSmooth = false;
	pomCurrentPage = NULL;

	omSimulationTypePage.SetCaption(LoadStg(IDS_STRING1811));
	omSimulationOffsetPage.SetCaption(LoadStg(IDS_STRING1825));
	omSimulationCutPage.SetCaption(LoadStg(IDS_STRING1816));
	omSimulationSmoothPage.SetCaption(LoadStg(IDS_STRING1817));
}

SimulationSheet::~SimulationSheet()
{
}


BEGIN_MESSAGE_MAP(SimulationSheet, CPropertySheet)
	//{{AFX_MSG_MAP(SimulationSheet)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	ON_BN_CLICKED(IDOK, OnOK)
	ON_BN_CLICKED(IDCANCEL, OnCancel)
    ON_MESSAGE(WM_UPDATE_ALL_PAGES, OnUpdateAllPages)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SimulationSheet message handlers

void SimulationSheet::OnOK() 
{
	// TODO: Add extra validation here
	this->omSimulationTypePage.OnOK();

	if (::IsWindow(this->pomCurrentPage->m_hWnd))
		this->pomCurrentPage->OnOK();

	CWnd *polParent = this->GetParent();
	if (polParent)
		polParent->SendMessage(WM_SIMULATE,0,0);

	EndDialog(IDOK);
}

void SimulationSheet::OnCancel() 
{
	// TODO: Add extra validation here
	this->omSimulationTypePage.OnCancel();

	if (::IsWindow(this->pomCurrentPage->m_hWnd))
		this->pomCurrentPage->OnCancel();

	CWnd *polParent = this->GetParent();
	if (polParent)
		polParent->SendMessage(WM_SIMULATE,0,0);

	EndDialog(IDCANCEL);
}

BOOL SimulationSheet::OnInitDialog() 
{
	CPropertySheet::OnInitDialog();
	
	ASSERT(pomViewer != NULL);

	CWnd *polWnd = GetDlgItem(IDOK);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61343));
	}

	polWnd = GetDlgItem(IDCANCEL);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61344));
	}


	this->SetWindowText(LoadStg(IDS_STRING1710));

	if (pomCurrentPage)
		RemovePage(pomCurrentPage);

	switch(omSimulationTypePage.CurrentType())
	{
	case 0 :
		AddPage(&omSimulationOffsetPage);
		pomCurrentPage = &omSimulationOffsetPage;
	break;		
	case 1 :
		AddPage(&omSimulationCutPage);
		pomCurrentPage = &omSimulationCutPage;
	break;		
	case 2 :
		AddPage(&omSimulationSmoothPage);
		pomCurrentPage = &omSimulationSmoothPage;
	break;		
	}

	SetActivePage(&omSimulationTypePage);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

LONG SimulationSheet::OnUpdateAllPages(UINT wParam,LONG lParam)
{
	CPropertyPage *popSenderPage = reinterpret_cast<CPropertyPage *>(lParam);
	if (popSenderPage == &omSimulationTypePage)
	{
		switch(omSimulationTypePage.CurrentType())
		{
		case 0 :
			RemovePage(pomCurrentPage);
			AddPage(&omSimulationOffsetPage);
			pomCurrentPage = &omSimulationOffsetPage;
		break;		
		case 1 :
			RemovePage(pomCurrentPage);
			AddPage(&omSimulationCutPage);
			pomCurrentPage = &omSimulationCutPage;
		break;		
		case 2 :
			RemovePage(pomCurrentPage);
			AddPage(&omSimulationSmoothPage);
			pomCurrentPage = &omSimulationSmoothPage;
		break;		
		}
	}

	return 0l;
}
