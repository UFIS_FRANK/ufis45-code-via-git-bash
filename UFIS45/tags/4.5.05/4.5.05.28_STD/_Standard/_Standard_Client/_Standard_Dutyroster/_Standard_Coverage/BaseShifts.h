// BaseShifts.h: interface for the CBaseShifts class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BASESHIFTS_H__A6B65E81_C7A5_11D4_B222_204C4F4F5020__INCLUDED_)
#define AFX_BASESHIFTS_H__A6B65E81_C7A5_11D4_B222_204C4F4F5020__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <CCSPtrArray.h>

struct BASESHIFT              
{
   int Start;             
   int End;  
   int RealStart;             
   int RealEnd;  
   int BRStart;
   int BREnd;
   int BreakStart;
   int BreakEnd;
   bool Selected;
   CString  Code;
   CString  Fctc;
   long BshUrno;
   int Count;
   int OptCount;
   int DayOffSet;
   int GroupKey;
   int ActBreakOffSet;
   int StartPos;
   int EndPos;
   int BreakStartPos;
   int BreakEndPos;
   int CoveredWorkingHours;
   double Fitness;
}; 

class CBaseShifts  
{
public:
	CBaseShifts(BOOL bpCopy = FALSE);
	virtual ~CBaseShifts();
	void InitBaseShifts(CCSPtrArray <BASESHIFT> ropBaseShifts, CString opFctc);
	int GetShiftCount();
public:
	CCSPtrArray <BASESHIFT> omBaseShifts;

private: 
	int		imStartOffSet;
	int		imShiftCount;
	BOOL	bmCopy;
};

#endif // !defined(AFX_BASESHIFTS_H__A6B65E81_C7A5_11D4_B222_204C4F4F5020__INCLUDED_)
