// BewertungPropertySheet.cpp : implementation file
//

// These following include files are project dependent
#include <stdafx.h>
#include <Coverage.h>
#include <CCSGlobl.h>
#include <CedaBasicData.h>
// These following include files are necessary
#include <cviewer.h>
#include <BasicData.h>
#include <BewertungPropertySheet.h>
#include <StringConst.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CStringArray ogFlightAlcd;

/////////////////////////////////////////////////////////////////////////////
// BewertungPropertySheet
//


BewertungPropertySheet::BewertungPropertySheet(CString opCalledFrom,CString opCaption,FilterType ipFilterType, CWnd* pParentWnd,
	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
	(opCaption, pParentWnd, popViewer, iSelectPage)
{
	pomViewer = popViewer; 

	AddPage(&m_PsBewPfcFilterPage);
	AddPage(&m_PsBewGhsFilterPage);
	AddPage(&m_PsBewCicFilterPage);
	AddPage(&m_PsBewPstFilterPage);
	AddPage(&m_PsBewGatFilterPage);
	AddPage(&m_PsBewReqFilterPage);
	
	bmTypeManSet = false;
	bmFidxManSet = false;
	imFilterType = ipFilterType;
	m_PsBewPfcFilterPage.SetCaption(LoadStg(IDS_STRING61218));
	m_PsBewGhsFilterPage.SetCaption(LoadStg(IDS_STRING61217));
	m_PsBewGatFilterPage.SetCaption(LoadStg(IDS_STRING61238));
	m_PsBewPstFilterPage.SetCaption(LoadStg(IDS_STRING61237));
	m_PsBewCicFilterPage.SetCaption(LoadStg(IDS_STRING61236));
	m_PsBewReqFilterPage.SetCaption(LoadStg(IDS_STRING61239));

} 

void BewertungPropertySheet::LoadDataFromViewer()
{
	if (pomViewer != NULL)
	{
		pomViewer->GetFilter("BEWGHS",m_PsBewGhsFilterPage.omSelectedItems);
		pomViewer->GetFilter("BEWPFC",m_PsBewPfcFilterPage.omSelectedItems);
		pomViewer->GetFilter("BEWEQU",m_PsBewReqFilterPage.omSelectedItems);
		pomViewer->GetFilter("BEWLCIC",m_PsBewCicFilterPage.omSelectedItems);
		pomViewer->GetFilter("BEWLPST",m_PsBewPstFilterPage.omSelectedItems);
		pomViewer->GetFilter("BEWLGAT",m_PsBewGatFilterPage.omSelectedItems);

		// in case of elder view data ...
		if (m_FIdxCombo1.GetCount() > 0)
		{
			m_PsBewReqFilterPage.omEqtUrno.Format("%u",m_FIdxCombo1.GetItemData(0));
			m_FIdxCombo1.SetCurSel(0);
		}

		omSelectedItems.RemoveAll();

		pomViewer->GetFilter("BEWSELBTN",omSelectedItems);
		int ilCount = omSelectedItems.GetSize();
		CString olText;
		for (int ilLc = 0; ilLc < ilCount && (!bmTypeManSet && !bmFidxManSet); ilLc++)
		{
			olText = omSelectedItems[ilLc];
			if (olText.Find("PERS=") == 0)
			{
				if (olText.GetLength() > 5)
				{
					blCheck1 = olText.GetAt(5) == '1' ? true : false;
				}
			}
			else if (olText.Find("EQUI=") == 0)
			{
				if (olText.GetLength() > 5)
				{
					blCheck3 = olText.GetAt(5) == '1' ? true : false;
				}
			}
			else if (olText.Find("LTYP=") == 0)
			{
				if (olText.GetLength() > 5)
				{
					blCheck2 = olText.GetAt(5) == '1' ? true : false;
				}
			}
		}
		
		omSelectedItems.RemoveAll();
		pomViewer->GetFilter("BEWFIDXBTN",omSelectedItems);
		ilCount = omSelectedItems.GetSize();
		
		for (ilLc = 0; ilLc < ilCount && (!bmTypeManSet && !bmFidxManSet); ilLc++)
		{
			olText = omSelectedItems[ilLc];
			if (olText.Find("FTYP=") == 0)
			{
				if (olText.GetLength() > 5)
				{
					char olType = olText.GetAt(5);
					imFilterType = (FilterType) atoi(&olType);
				}
			}
			else if (olText.Find("EQUTYP=") == 0)
			{
				if (olText.GetLength() > 7 && m_FIdxCombo1.GetCount() > 0)
				{
					int ilIndex = 0;
					olText = olText.Mid(7);	
					if (olText != LoadStg(IDS_STRING61216))	// *ALL means nothing here
					{
						DWORD llUrno = atol(olText);
						for (int i = 0; i < m_FIdxCombo1.GetCount(); i++)
						{
							if (m_FIdxCombo1.GetItemData(i) == llUrno)
							{
								ilIndex = i;
								break;
							}
						}
					}

					m_PsBewReqFilterPage.omEqtUrno.Format("%u",m_FIdxCombo1.GetItemData(ilIndex));
					m_FIdxCombo1.SetCurSel(ilIndex);
				}
			}

		}
		bmFidxManSet = false;
		bmTypeManSet = false;
	}
}

void BewertungPropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{

	if (pomViewer != NULL)
	{
		pomViewer->SetFilter("BEWGHS",m_PsBewGhsFilterPage.omSelectedItems);
		pomViewer->SetFilter("BEWPFC",m_PsBewPfcFilterPage.omSelectedItems);
		pomViewer->SetFilter("BEWEQU",m_PsBewReqFilterPage.omSelectedItems);
		pomViewer->SetFilter("BEWLCIC",m_PsBewCicFilterPage.omSelectedItems);
		pomViewer->SetFilter("BEWLPST",m_PsBewPstFilterPage.omSelectedItems);
		pomViewer->SetFilter("BEWLGAT",m_PsBewGatFilterPage.omSelectedItems);

		CString olText;
		omSelectedItems.RemoveAll();
		olText.Format("PERS=%c",blCheck1 == true ? '1' : '0');
		omSelectedItems.Add(olText);		
		olText.Format("EQUI=%c",blCheck3 == true ? '1' : '0');
		omSelectedItems.Add(olText);		
		olText.Format("LTYP=%c",blCheck2 == true ? '1' : '0');
		omSelectedItems.Add(olText);		

		pomViewer->SetFilter("BEWSELBTN",omSelectedItems);

		
		omSelectedItems.RemoveAll();
		olText.Format("FTYP=%d",imFilterType);
		omSelectedItems.Add(olText);		
		int ilIndex = m_FIdxCombo1.GetCurSel();
		if (ilIndex != CB_ERR)
		{
			CString olValue;
			m_FIdxCombo1.GetLBText(ilIndex,olValue);
			if (olValue == LoadStg(IDS_STRING61216))
			{
				olText.Format("EQUTYP=%s",olValue);
			}
			else
			{
				olText.Format("EQUTYP=%u",m_FIdxCombo1.GetItemData(ilIndex));
			}
		}
		else
		{
			olText.Format("EQUTYP=%s",LoadStg(IDS_STRING61216));
		}

		omSelectedItems.Add(olText);		
		pomViewer->SetFilter("BEWFIDXBTN", omSelectedItems);
	}

	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}
}

BOOL BewertungPropertySheet::OnInitDialog() 
{
	BOOL bResult = CPropertySheet::OnInitDialog();

	// Resize the window for new combo-box and buttons
	CRect rcWindow;
	GetWindowRect(rcWindow);
	rcWindow.InflateRect(0, 40);
	MoveWindow(rcWindow);

	// Prepare the position for new control objects
	CRect rcApply;
	GetDlgItem(ID_APPLY_NOW)->GetWindowRect(rcApply);
	ScreenToClient(rcApply);
	CRect rcCancel;
	GetDlgItem(IDCANCEL)->GetWindowRect(rcCancel);
	ScreenToClient(rcCancel);

	m_Check1.Create(LoadStg(IDS_STRING61231), WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON   ,
		CRect(5, rcApply.top , 5 + rcApply.Width(), rcApply.bottom),
		this, IDC_SHEET_CHECK1);
    m_Check1.SetFont(GetDlgItem(IDCANCEL)->GetFont());

	m_Check2.Create(LoadStg(IDS_STRING61233), WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON   ,
		CRect(15 + rcApply.Width(), rcApply.top , 15 + (int)2*rcApply.Width(), rcApply.bottom),
		this, IDC_SHEET_CHECK2);
    m_Check2.SetFont(GetDlgItem(IDCANCEL)->GetFont());

	if (ogBasicData.IsEquipmentAvailable())
	{
		m_Check3.Create(LoadStg(IDS_STRING61232), WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON   ,
						CRect(30 + (int)2*rcApply.Width(), rcApply.top , 30 + (int)3*rcApply.Width(), rcApply.bottom),
						this, IDC_SHEET_CHECK3);
	}
	else
	{
		m_Check3.Create(LoadStg(IDS_STRING61232), WS_CHILD | BS_RADIOBUTTON   ,
						CRect(30 + (int)2*rcApply.Width(), rcApply.top , 30 + (int)3*rcApply.Width(), rcApply.bottom),
						this, IDC_SHEET_CHECK3);
	}

    m_Check3.SetFont(GetDlgItem(IDCANCEL)->GetFont());

	m_FIdxCheck1.Create(LoadStg(IDS_STRING61236), WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON   ,
		CRect(50 + (int)3*rcApply.Width(), rcApply.top , 85 + (int)4*rcApply.Width() , rcApply.bottom),
		this, IDC_SHEET_FIDXCHECK1);
    m_FIdxCheck1.SetFont(GetDlgItem(IDCANCEL)->GetFont());

	m_FIdxCheck2.Create(LoadStg(IDS_STRING61236), WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON   ,
		CRect(95 + (int)4*rcApply.Width(), rcApply.top , 105 + (int)5*rcApply.Width() , rcApply.bottom),
		this, IDC_SHEET_FIDXCHECK2);
    m_FIdxCheck2.SetFont(GetDlgItem(IDCANCEL)->GetFont());

	m_FIdxCheck3.Create(LoadStg(IDS_STRING61236), WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON   ,
		CRect(115 + (int)5*rcApply.Width(), rcApply.top , 125 + (int)6*rcApply.Width(), rcApply.bottom),
		this, IDC_SHEET_FIDXCHECK3);
    m_FIdxCheck3.SetFont(GetDlgItem(IDCANCEL)->GetFont());

	m_FIdxStatic1.Create(LoadStg(IDS_STRING61240), WS_CHILD | WS_VISIBLE | SS_CENTER,
		CRect(50 + (int)3*rcApply.Width(), rcApply.top + 4, 85 + (int)4*rcApply.Width(),rcApply.bottom),
		this, IDC_SHEET_FIDXSTATIC1);
    m_FIdxStatic1.SetFont(GetDlgItem(IDC_SHEET_FIDXCHECK3)->GetFont());

	m_FIdxCombo1.Create(WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST | CBS_SORT | WS_VSCROLL,
		CRect(95 + (int)4*rcApply.Width(), rcApply.top ,105 + (int)6*rcApply.Width(), rcApply.top + 140),
		this, IDC_SHEET_FIDXCOMBO1);

	if (ogBasicData.IsEquipmentAvailable())
	{
		// fill combo box with equipment types
		int ilCount = ogBCD.GetDataCount("EQT");
		for (int ilData = 0; ilData < ilCount; ilData++)
		{
			CString olUrno = ogBCD.GetField("EQT",ilData,"URNO");
			CString olName = ogBCD.GetField("EQT",ilData,"NAME");
			int ilIndex = m_FIdxCombo1.AddString(olName);
			if (ilIndex != CB_ERR)
			{
				m_FIdxCombo1.SetItemData(ilIndex,atol(olUrno));
			}
		}
	}

	// Create the combo box, save button, and delete button
	CComboBox *polView = new CComboBox;
	polView->Create(WS_CHILD | WS_VISIBLE | CBS_DROPDOWN | WS_VSCROLL,
		CRect(6, rcApply.top + 35, 140, rcApply.top + 135),
		this, IDC_SHEET_VIEW);
    polView->SetFont(GetDlgItem(IDCANCEL)->GetFont());
	CButton *polSave = new CButton;
	polSave->Create(LoadStg(IDS_STRING1524), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
		CRect(145, rcApply.top + 35, 145 + rcApply.Width(), rcApply.bottom + 35),
		this, IDC_SHEET_SAVE);
    polSave->SetFont(GetDlgItem(IDCANCEL)->GetFont());
	CButton *polDelete = new CButton;
	polDelete->Create(LoadStg(IDS_STRING1526), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
		CRect(147 + rcApply.Width(), rcApply.top + 35, 147 + 2*rcApply.Width(), rcApply.bottom + 35),
		this, IDC_SHEET_DELETE);
    polDelete->SetFont(GetDlgItem(IDCANCEL)->GetFont());
	CButton *polApply = new CButton;
	polApply->Create(LoadStg(IDS_STRING1525), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
		CRect(149 + 2*rcApply.Width(), rcApply.top + 35, 149 + 3*rcApply.Width(), rcApply.bottom + 35),
		this, IDC_SHEET_APPLY);
    polApply->SetFont(GetDlgItem(IDCANCEL)->GetFont());

	// Draw the separator between save/delete buttons and standard close/help buttons
	CRect rcClient;
	GetClientRect(rcClient);
	omSeparator.Create("", WS_VISIBLE | SS_BLACKFRAME,
		CRect(6, rcApply.bottom + 27 +15 , rcClient.Width() - 6, rcApply.bottom + 29 + 15), this);

	// Remove the button "Apply"; also shift "OK" and "Cancel" buttons
	GetDlgItem(ID_APPLY_NOW)->DestroyWindow();
	CRect rcOK = rcCancel;
	rcOK.OffsetRect(-70, rcOK.Height() + 50);//5 => -70
	GetDlgItem(IDOK)->MoveWindow(rcOK);
	rcCancel = rcApply;
	rcCancel.OffsetRect(-30, rcCancel.Height() + 50);//5 => -30
	GetDlgItem(IDCANCEL)->MoveWindow(rcCancel);
	GetDlgItem(IDCANCEL)->SetWindowText(LoadStg(IDS_STRING522));

	CWnd *olHelpButton = GetDlgItem(IDHELP);
	if (olHelpButton != NULL)
	{
		CRect rcHelp;
		olHelpButton->GetWindowRect(rcHelp);
		ScreenToClient(rcHelp);
		rcHelp.OffsetRect(0, rcApply.Height() + 16);
		olHelpButton->MoveWindow(rcHelp);
		olHelpButton->SetWindowPos(&wndBottom,0,0,0,0,SWP_HIDEWINDOW | SWP_NOMOVE );
		olHelpButton->EnableWindow(FALSE);
	}

	UpdateComboBox();	// update data in the combo-box
	LoadDataFromViewer();	// update data in every pages
	GetActivePage()->UpdateData(FALSE);
	if(blCheck1)
	{
		m_Check1.SetCheck(1);
		m_FIdxCheck1.SetWindowText(LoadStg(IDS_STRING61218));
		m_FIdxCheck2.SetWindowText(LoadStg(IDS_STRING61217));
		m_FIdxCheck3.ShowWindow(SW_HIDE);
		m_FIdxStatic1.ShowWindow(SW_HIDE);	// Equipment Type text
		m_FIdxCombo1.ShowWindow(SW_HIDE);	// Equipment Type list
	}
	else
	{
		m_Check1.SetCheck(0);
	}

	if(blCheck2)
	{
		m_Check2.SetCheck(1);
		m_FIdxCheck1.SetWindowText(LoadStg(IDS_STRING61236));
		m_FIdxCheck2.SetWindowText(LoadStg(IDS_STRING61237));
		m_FIdxCheck3.SetWindowText(LoadStg(IDS_STRING61238));
		m_FIdxCheck3.ShowWindow(SW_SHOW);
		m_FIdxStatic1.ShowWindow(SW_HIDE);	// Equipment Type text
		m_FIdxCombo1.ShowWindow(SW_HIDE);	// Equipment Type list
	}
	else
	{
		m_Check2.SetCheck(0);
	}

	if(blCheck3)
	{
		m_Check3.SetCheck(1);
		m_FIdxStatic1.ShowWindow(SW_SHOW);	// Equipment Type text
		m_FIdxCombo1.ShowWindow(SW_SHOW);	// Equipment Type list
		m_FIdxCheck1.ShowWindow(SW_HIDE);
		m_FIdxCheck2.ShowWindow(SW_HIDE);
		m_FIdxCheck3.ShowWindow(SW_HIDE);

	}
	else
	{
		m_Check3.SetCheck(0);
	}
	
	DisplayPages();


	return bResult;
}
	
void BewertungPropertySheet::DoDataExchange(CDataExchange* pDX)
{
	CPropertySheet::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(BewertungPropertySheet)
	DDX_Control(pDX, IDC_SHEET_CHECK1, m_Check1);
	DDX_Control(pDX, IDC_SHEET_CHECK2, m_Check2);
	DDX_Control(pDX, IDC_SHEET_CHECK3, m_Check3);
	DDX_Control(pDX, IDC_SHEET_FIDXCHECK1, m_FIdxCheck1);
	DDX_Control(pDX, IDC_SHEET_FIDXCHECK2, m_FIdxCheck2);
	DDX_Control(pDX, IDC_SHEET_FIDXCHECK3, m_FIdxCheck3);
	//}}AFX_DATA_MAP
} 

BEGIN_MESSAGE_MAP(BewertungPropertySheet, CPropertySheet)
	//{{AFX_MSG_MAP(BewertungPropertySheet)
	ON_BN_CLICKED(IDC_SHEET_CHECK1, OnCheck1)
	ON_BN_CLICKED(IDC_SHEET_CHECK2, OnCheck2)
	ON_BN_CLICKED(IDC_SHEET_CHECK3, OnCheck3)
	ON_BN_CLICKED(IDC_SHEET_FIDXCHECK1, OnFIdxCheck1)
	ON_BN_CLICKED(IDC_SHEET_FIDXCHECK2, OnFIdxCheck2)
	ON_BN_CLICKED(IDC_SHEET_FIDXCHECK3, OnFIdxCheck3)
	ON_CBN_SELCHANGE(IDC_SHEET_VIEW, OnViewSelChange)
	ON_BN_CLICKED(IDC_SHEET_SAVE, OnSave)
	ON_BN_CLICKED(IDC_SHEET_DELETE, OnDelete)
	ON_BN_CLICKED(IDC_SHEET_APPLY, OnApply)
	ON_BN_CLICKED(IDCANCEL, OnCancel)
	ON_BN_CLICKED(IDOK, OnOK)
	ON_CBN_KILLFOCUS(IDC_SHEET_VIEW,OnComboKillFocus)
	ON_CBN_SELCHANGE(IDC_SHEET_FIDXCOMBO1, OnFIdxCombo1SelChange)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


void BewertungPropertySheet::OnCheck1()
{
	if(m_Check1.GetCheck() == 1)
	{
//		m_Check1.SetCheck(0);
//		blCheck1 = false;
	}
	else
	{
		blCheck1 = true;
		blCheck2 = false;
		blCheck3 = false;

		m_Check1.SetCheck(1);
		m_Check2.SetCheck(0);
		m_Check3.SetCheck(0);

		m_FIdxCheck1.SetWindowText(LoadStg(IDS_STRING61218));
		m_FIdxCheck2.SetWindowText(LoadStg(IDS_STRING61217));

		m_FIdxCheck1.ShowWindow(SW_SHOW);
		m_FIdxCheck2.ShowWindow(SW_SHOW);
		m_FIdxCheck3.ShowWindow(SW_HIDE);

		m_FIdxStatic1.ShowWindow(SW_HIDE);	// Equipment Type text
		m_FIdxCombo1.ShowWindow(SW_HIDE);	// Equipment Type list
		if(imFilterType < FUNCTION || imFilterType > QUALIFICATION)
		{
			imFilterType = FUNCTION;
		}
		bmTypeManSet = true;

		pomViewer->SelectView("<Default>");
		UpdateComboBox();
		LoadDataFromViewer();


	}

	DisplayPages();

	
}
void BewertungPropertySheet::OnCheck2()
{
	if(m_Check2.GetCheck() == 1)
	{
//		m_Check2.SetCheck(0);
//		blCheck2 = false;
	}
	else
	{
		blCheck1 = false;
		blCheck2 = true;
		blCheck3 = false;

		m_Check1.SetCheck(0);
		m_Check2.SetCheck(1);
		m_Check3.SetCheck(0);

		m_FIdxCheck1.SetWindowText(LoadStg(IDS_STRING61236));
		m_FIdxCheck2.SetWindowText(LoadStg(IDS_STRING61237));

		m_FIdxCheck1.ShowWindow(SW_SHOW);
		m_FIdxCheck2.ShowWindow(SW_SHOW);
		m_FIdxCheck3.ShowWindow(SW_SHOW);

		m_FIdxCheck3.SetWindowText(LoadStg(IDS_STRING61238));
		m_FIdxStatic1.ShowWindow(SW_HIDE);	// Equipment Type text
		m_FIdxCombo1.ShowWindow(SW_HIDE);	// Equipment Type list

		if(imFilterType < CHECKIN || imFilterType > GATES)
		{
			imFilterType = CHECKIN;
		}


		bmTypeManSet = true;

		pomViewer->SelectView("<Default>");
		UpdateComboBox();
		LoadDataFromViewer();


	}

	DisplayPages();

	
}
void BewertungPropertySheet::OnCheck3()
{
	if(m_Check3.GetCheck() == 1)
	{
	}
	else
	{
		blCheck1 = false;
		blCheck2 = false;
		blCheck3 = true;

		m_Check1.SetCheck(0);
		m_Check2.SetCheck(0);
		m_Check3.SetCheck(1);

		m_FIdxStatic1.SetWindowText(LoadStg(IDS_STRING61240));	// Equipment Type
		m_FIdxCheck1.ShowWindow(SW_HIDE);
		m_FIdxCheck2.ShowWindow(SW_HIDE);
		m_FIdxCheck3.ShowWindow(SW_HIDE);

		m_FIdxStatic1.ShowWindow(SW_SHOW);	// Equipment Type text
		m_FIdxCombo1.ShowWindow(SW_SHOW);	// Equipment Type list

		imFilterType = EQUIPMENT;

		bmTypeManSet = true;

		pomViewer->SelectView("<Default>");
		UpdateComboBox();
		LoadDataFromViewer();
	}
	DisplayPages();	
}

void BewertungPropertySheet::OnFIdxCheck1()
{
	if(m_FIdxCheck1.GetCheck() == 0)
	{
		m_FIdxCheck2.SetCheck(0);
		m_FIdxCheck3.SetCheck(0);
		m_FIdxCheck1.SetCheck(1);
		if(blCheck1)
		{
			imFilterType = FUNCTION;
		}
		else if(blCheck2)
		{
			imFilterType = CHECKIN;
		}
		else if(blCheck3)
		{
			imFilterType = EQUIPMENT;
		}
				
		bmFidxManSet = true;

		pomViewer->SelectView("<Default>");
		UpdateComboBox();
		LoadDataFromViewer();
	}
	DisplayPages();	
}

void BewertungPropertySheet::OnFIdxCheck2()
{
	if(m_FIdxCheck2.GetCheck() == 0)
	{
		m_FIdxCheck2.SetCheck(1);
		m_FIdxCheck3.SetCheck(0);
		m_FIdxCheck1.SetCheck(0);
		if(blCheck1)
		{
			imFilterType = QUALIFICATION;
		}
		else if(blCheck2)
		{
			imFilterType = POSITIONS;
		}
			
		bmFidxManSet = true;

		pomViewer->SelectView("<Default>");
		UpdateComboBox();
		LoadDataFromViewer();

	}
	DisplayPages();	
}

void BewertungPropertySheet::OnFIdxCheck3()
{
	if(m_FIdxCheck3.GetCheck() == 0)
	{
		m_FIdxCheck2.SetCheck(0);
		m_FIdxCheck3.SetCheck(1);
		m_FIdxCheck1.SetCheck(0);

		if(blCheck2)
		{
			imFilterType = GATES;
		}

		bmFidxManSet = true;

		pomViewer->SelectView("<Default>");
		UpdateComboBox();
		LoadDataFromViewer();

	}
	DisplayPages();	
}

void BewertungPropertySheet::DisplayPages()
{
	int ilPageIdx;

		
	m_DummyPage.blIsInit = false;
	
	ilPageIdx = GetPageIndex(&m_DummyPage);
	if(ilPageIdx < 0)
		AddPage(&m_DummyPage);
	ilPageIdx = GetPageIndex(&m_PsBewPfcFilterPage);
	if(ilPageIdx >= 0)
		RemovePage(ilPageIdx);
	ilPageIdx = GetPageIndex(&m_PsBewGhsFilterPage);
	if(ilPageIdx >= 0)
		RemovePage(ilPageIdx);
	ilPageIdx = GetPageIndex(&m_PsBewGatFilterPage);
	if(ilPageIdx >= 0)
		RemovePage(ilPageIdx);
	ilPageIdx = GetPageIndex(&m_PsBewPstFilterPage);
	if(ilPageIdx >= 0)
		RemovePage(ilPageIdx);
	ilPageIdx = GetPageIndex(&m_PsBewCicFilterPage);
	if(ilPageIdx >= 0)
		RemovePage(ilPageIdx);
	ilPageIdx = GetPageIndex(&m_PsBewReqFilterPage);
	if(ilPageIdx >= 0)
		RemovePage(ilPageIdx);

	if(imFilterType == EQUIPMENT)
	{
		m_FIdxCheck1.SetWindowText(LoadStg(IDS_STRING61239));
		m_FIdxCheck2.ShowWindow(SW_HIDE);
		m_FIdxCheck3.ShowWindow(SW_HIDE);
		m_FIdxStatic1.ShowWindow(SW_SHOW);
		m_FIdxCombo1.ShowWindow(SW_SHOW);
	}

	if(imFilterType >= FUNCTION && imFilterType <= QUALIFICATION)
	{
		m_FIdxCheck1.SetWindowText(LoadStg(IDS_STRING61218));
		m_FIdxCheck2.SetWindowText(LoadStg(IDS_STRING61217));
		m_FIdxCheck3.ShowWindow(SW_HIDE);
		m_FIdxStatic1.ShowWindow(SW_HIDE);
		m_FIdxCombo1.ShowWindow(SW_HIDE);
	}

	if(imFilterType >= CHECKIN && imFilterType <= GATES)
	{
		m_FIdxCheck1.SetWindowText(LoadStg(IDS_STRING61236));
		m_FIdxCheck2.SetWindowText(LoadStg(IDS_STRING61237));
		m_FIdxCheck3.ShowWindow(SW_SHOW);
		m_FIdxCheck3.SetWindowText(LoadStg(IDS_STRING61238));
		m_FIdxStatic1.ShowWindow(SW_HIDE);
		m_FIdxCombo1.ShowWindow(SW_HIDE);
	}

	switch(imFilterType)
	{
	case FUNCTION:
		{
			m_PsBewPfcFilterPage.blIsInit = false;
			AddPage(&m_PsBewPfcFilterPage);
			m_FIdxCheck1.SetCheck(1);
			m_FIdxCheck2.SetCheck(0);
			m_FIdxCheck3.SetCheck(0);

			break;
		}
	case QUALIFICATION:
		{
			m_PsBewGhsFilterPage.blIsInit = false;
			AddPage(&m_PsBewGhsFilterPage);
			m_FIdxCheck1.SetCheck(0);
			m_FIdxCheck2.SetCheck(1);
			m_FIdxCheck3.SetCheck(0);

			break;
		}
	case CHECKIN:
		{
			m_PsBewCicFilterPage.blIsInit = false;
			AddPage(&m_PsBewCicFilterPage);
			m_FIdxCheck1.SetCheck(1);
			m_FIdxCheck2.SetCheck(0);
			m_FIdxCheck3.SetCheck(0);

			break;
		}
	case POSITIONS:
		{
			m_PsBewPstFilterPage.blIsInit = false;
			AddPage(&m_PsBewPstFilterPage);
			m_FIdxCheck1.SetCheck(0);
			m_FIdxCheck2.SetCheck(1);
			m_FIdxCheck3.SetCheck(0);

			break;
		}
	case GATES:  
		{
			m_PsBewGatFilterPage.blIsInit = false;
			AddPage(&m_PsBewGatFilterPage);
			m_FIdxCheck1.SetCheck(0);
			m_FIdxCheck2.SetCheck(0);
			m_FIdxCheck3.SetCheck(1);

			break;
		}
	case EQUIPMENT:  
		{
			m_PsBewReqFilterPage.blIsInit = false;
			AddPage(&m_PsBewReqFilterPage);
			m_FIdxCheck1.SetCheck(1);
			m_FIdxCheck2.SetCheck(0);
			m_FIdxCheck3.SetCheck(0);

			break;
		}
	}

	ilPageIdx = GetPageIndex(&m_DummyPage);
	if(ilPageIdx >= 0)
		RemovePage(ilPageIdx);

}

void BewertungPropertySheet::OnViewSelChange()
{
	if (GetActivePage()->UpdateData() == FALSE)
		return;
	if (QueryForDiscardChanges() != IDOK)
	{
		UpdateComboBox();
		return;
	}

	// Check if user didn't change anything, so we don't have to reload the view
	if (GetDlgItem(IDCANCEL)->IsWindowEnabled() &&		// no change made before?
		GetComboBoxText() == pomViewer->SelectView())	// still on the old view?
		return;

	// Discard changes and restore data from the registry database
	CString olViewName = GetComboBoxText();
	pomViewer->SelectView(olViewName);
	UpdateComboBox();
	LoadDataFromViewer();
	m_PsBewReqFilterPage.CreatePossibleItemsList();
	GetActivePage()->UpdateData(FALSE);
	if(blCheck1)
	{
		m_Check1.SetCheck(1);
	}
	else
	{
		m_Check1.SetCheck(0);
	}
	if(blCheck2)
	{
		m_Check2.SetCheck(1);
	}
	else
	{
		m_Check2.SetCheck(0);
	}

	if(blCheck3)
	{
		m_Check3.SetCheck(1);
	}
	else
	{
		m_Check3.SetCheck(0);
	}
	
	DisplayPages();
	GetActivePage()->CancelToClose();
}

void BewertungPropertySheet::OnSave()
{
	// Don't allow the user to change the default view
	CString olViewName = GetComboBoxText();
	if (olViewName == "<Default>")
		return;

	// Validate the current page before saving
	if (GetActivePage()->UpdateData() == FALSE)
		return;


	// Save data to the registry database
	CStringArray olFilters;
	pomViewer->GetFilterPage(olFilters);	// use filters in the last view
	pomViewer->CreateView(olViewName, olFilters);
	//TRACE("\n Asicht = %s\n; Filter = %s\n", olViewName, olFilters);
	pomViewer->SelectView(olViewName);	// just make sure that CViewer() work correctly
	SaveDataToViewer(olViewName,TRUE);
	UpdateComboBox();
	CString olWhere;
	/***********************
	if(pomViewer->GetFlightWhereString(olWhere) == true)
	{
		ofstream of;
		of.open( CCSLog::GetTmpPath("\\where.txt"), ios::out);
		of << olWhere.GetBuffer(0);
		of.close();
	}
	*****************/

	EndDialog(IDOK);
}


void BewertungPropertySheet::OnComboKillFocus()
{
	CString olOldViewName;
	CString olNewViewName;
	unsigned char clChar;

	CComboBox *polView = (CComboBox *)GetDlgItem(IDC_SHEET_VIEW);
	polView->GetWindowText(olOldViewName);
	//CPoint orCaretPosition = polView->GetCaretPos( );

	olNewViewName.Empty();
	int ilCount = olOldViewName.GetLength();
	for (int ilLc = 0; ilLc < ilCount; ilLc++)
	{
		clChar = olOldViewName.GetAt(ilLc);
		switch(clChar)
		{
		case (UCHAR) '�' : olNewViewName += "ae";
			break;
		case (UCHAR) '�' : olNewViewName += "ue";
			break;
		case (UCHAR) '�' : olNewViewName += "oe";
			break;
		case (UCHAR) '�' : olNewViewName += "AE";
			break;
		case (UCHAR) '�' : olNewViewName += "OE";
			break;
		case (UCHAR) '�' : olNewViewName += "UE";
			break;
		case (UCHAR) '�' : olNewViewName += "ss";
			break;
		default:
			if (clChar > 127)
			{
				olNewViewName += '~';
			}
			else
			{
				olNewViewName += clChar;
			}
		}
	}
	polView->SetWindowText(olNewViewName); 
	ilCount = olNewViewName.GetLength();
//	CEdit *polEdit = (CEdit *) polView;
//	polEdit->SetSel(ilCount,ilCount,TRUE);

//	polView->SetCaretPos(orCaretPosition);

}


void BewertungPropertySheet::OnApply()
{
	// Don't allow the user to change the default view
	CString olViewName = GetComboBoxText();
	if (olViewName == "<Default>")
		return;

	// Validate the current page before saving
	if (GetActivePage()->UpdateData() == FALSE)
		return;

	
	// Save data to the registry database
	CStringArray olFilters;
	pomViewer->GetFilterPage(olFilters);	// use filters in the last view
	bool blWithDBDelete = false;
	pomViewer->CreateView(olViewName, olFilters, blWithDBDelete);

	//TRACE("\n Ansicht = %s\n, Filter = %s\n, bool = %d\n\n", olViewName, olFilters, blWithDBDelete);
	pomViewer->SelectView(olViewName);	// just make sure that CViewer() work correctly
	SaveDataToViewer(olViewName,FALSE);
	UpdateComboBox();
	EndDialog(IDOK);
}


void BewertungPropertySheet::OnDelete()
{
	// Don't allow the user to delete the default view
	CString olViewName = GetComboBoxText();
	if (olViewName == "<Default>")
		return;

	// Delete the selected view from the registry database
	pomViewer->DeleteView(olViewName);
	pomViewer->SelectView("<Default>");
	UpdateComboBox();
	LoadDataFromViewer();
	GetActivePage()->UpdateData(FALSE);
}

void BewertungPropertySheet::OnOK()
{
	if (GetActivePage()->UpdateData() == FALSE)
		return;
	if (QueryForDiscardChanges() != IDOK)
	{
		UpdateComboBox();
		return;
	}

	// Terminate the PropertySheet
	EndDialog(IDOK);
}

void BewertungPropertySheet::OnCancel()
{
/***********************
	if (GetActivePage()->UpdateData() == FALSE)
		return;
	if (QueryForDiscardChanges() != IDOK)
	{
		UpdateComboBox();
		return;
	}
*****************/
	// Terminate the PropertySheet
	EndDialog(IDCANCEL);
}

/////////////////////////////////////////////////////////////////////////////
// BasePropertySheet -- help routines

void BewertungPropertySheet::UpdateComboBox()
{
	CComboBox *polView = (CComboBox *)GetDlgItem(IDC_SHEET_VIEW);
	polView->ResetContent();
	CStringArray olViews;
	pomViewer->GetViews(olViews);
	for (int ilIndex = 0; ilIndex < olViews.GetSize(); ilIndex++)
	{
		int ilIdx = polView->AddString(olViews[ilIndex]);
		if (olViews[ilIndex] == pomViewer->SelectView())
		{
			CString olActualView = olViews[ilIndex];
			CString olSelectedView = pomViewer->SelectView();
			polView->SetCurSel(ilIdx);
		}
	}

	//BOOL blIsDefaultView = pomViewer->SelectView() == "<Default>";
	pomViewer->SelectView() == "<Default>";
	//GetDlgItem(IDC_SHEET_DELETE)->EnableWindow(!blIsDefaultView);
}

CString BewertungPropertySheet::GetComboBoxText()
{
	CString olViewName;
	CComboBox *polView = (CComboBox *)GetDlgItem(IDC_SHEET_VIEW);
	int ilItemID;
	if ((ilItemID = polView->GetCurSel()) == CB_ERR)
		polView->GetWindowText(olViewName);
	else
		polView->GetLBText(ilItemID, olViewName);

	return olViewName;
}

BOOL BewertungPropertySheet::IsIdentical(CStringArray &ropArray1, CStringArray &ropArray2)
{
	if (ropArray1.GetSize() != ropArray2.GetSize())	// incompatible array size?
		return FALSE;

	for (int i = ropArray1.GetSize()-1; i >= 0; i--)
		if (!IsInArray(ropArray1[i], ropArray2))	// unidentical element found?
			return FALSE;

	return TRUE;
}

BOOL BewertungPropertySheet::IsInArray(CString &ropString, CStringArray &ropArray)
{
	for (int i = ropArray.GetSize()-1; i >= 0; i--)
		if (ropString == ropArray[i])	// identical string found?
			return TRUE;
	
	return FALSE;
}

void BewertungPropertySheet::OnFIdxCombo1SelChange()
{
	int ilIndex = m_FIdxCombo1.GetCurSel();
	if (ilIndex != CB_ERR)
	{
		bmFidxManSet = true;
		m_PsBewReqFilterPage.omPossibleItems.RemoveAll();
		m_PsBewReqFilterPage.omEqtUrno.Format("%u",m_FIdxCombo1.GetItemData(ilIndex));
		m_PsBewReqFilterPage.omSelectedItems.RemoveAll();
		CString olAll = LoadStg(IDS_STRING61216);
		m_PsBewReqFilterPage.omSelectedItems.Add(olAll);
		m_PsBewReqFilterPage.CreatePossibleItemsList();
		DisplayPages();
	}
}