// JobDlg.cpp : implementation file
//

#include <stdafx.h>
#include <coverage.h>
#include <covdiagram.h>
#include <JobDlg.h>
#include <CCSEdit.h>
#include <CCSTime.h>
#include <CCSGlobl.h>
#include <CedaFlightData.h>
#include <CedaPfcData.h>
#include <CedaGegData.h>
#include <DataSet.h>
//#include "ButtonListDlg.h"
#include <PrivList.h>
#include <Basicdata.h>
#include <CedaBasicdata.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CJobDlg dialog
static int ComparePfcData(const PFCDATA **e1, const PFCDATA **e2);
static int CompareDemDubeData(const DEMDATA **e1, const DEMDATA **e2);
static int CompareDemDuenData(const DEMDATA **e1, const DEMDATA **e2);
static int CompareGegData(const GEGDATA **e1, const GEGDATA **e2);

CJobDlg::CJobDlg(CWnd* pParent/*=NULL*/)
	: CDialog(CJobDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CJobDlg)
	//}}AFX_DATA_INIT
	pomOwner = NULL;
	pomViewer = NULL;
	pomGantt = NULL;
	imVerticalScaleIndent = 150;
	bmNoEditChangeNow = false;
	prmCurrentDem = NULL;
	prmArrFlight = NULL;
	prmDepFlight = NULL;

	pomArrivalList = new CGridFenster(this);
	pomDepatureList = new CGridFenster(this);



//	CWnd *polWnd = AfxGetMainWnd();
    CDialog::Create(CJobDlg::IDD,NULL);
	m_DragDropTarget.RegisterTarget(this, this);

	CRect olTSRect;
	m_Duties.ShowWindow(SW_HIDE);
	m_Duties.GetWindowRect(&olTSRect);
//	m_Duties.ClientToScreen(&olTSRect);
	ScreenToClient(&olTSRect);
	//olTSRect = CRect(12, 300, 700, 380);
	//olTSRect.top = 347;
	olTSRect.bottom = olTSRect.top + 18;
	olTSRect.left += imVerticalScaleIndent + 2;//161;
	//olTSRect.right -= 10;

	pomViewer = new DispoDetailGanttViewer(0);
    pomViewer->Attach(this);
    omTimeScale.Create(NULL, "TimeScale", WS_CHILD | WS_VISIBLE,
        CRect(olTSRect.left, olTSRect.top, olTSRect.right, olTSRect.bottom),
        this, 0, NULL);
	CTime olTSStartTime = CTime::GetCurrentTime();//CTime(1980, 1, 1, 0, 0, 0);
    CTimeSpan olTSDuration = CTimeSpan(0, 7, 0, 0);
    CTimeSpan olTSInterval = CTimeSpan(0, 0, 10, 0);

	//MakeNewGantt();

	pomGantt = new DispoDetailGantt;
    pomGantt->SetTimeScale(&omTimeScale);
    pomGantt->SetViewer(pomViewer);
	pomGantt->SetStatusBar(&m_Statusbar);
	CTimeSpan dur = omTimeScale.GetDisplayDuration();
    pomGantt->SetDisplayWindow(olTSStartTime, olTSStartTime + omTimeScale.GetDisplayDuration());
    pomGantt->SetVerticalScaleWidth(150);
	pomGantt->SetVerticalScaleIndent(imVerticalScaleIndent);
	pomGantt->SetVerticalScaleColors(BLACK, SILVER, AQUA, SILVER);
    pomGantt->SetFonts(3, 3);
    //pomGantt->SetVerticalScaleColors(NAVY, SILVER, YELLOW, SILVER);
	pomGantt->SetGanttChartColors(NAVY, SILVER, YELLOW, SILVER);
    pomGantt->Create(WS_BORDER, CRect(olTSRect.left+1, olTSRect.bottom + 1, olTSRect.right, olTSRect.bottom+75), (CWnd*)this);
	pomGantt->AttachWindow(this);

}

CJobDlg::~CJobDlg()
{
	ogDdx.UnRegister(this,NOTUSED);

	delete pomArrivalList;
	delete pomDepatureList;

	omBrush.DeleteObject();
	if(pomViewer != NULL)
	{
		delete pomViewer;
	}
	if(pomGantt != NULL)
	{
		delete pomGantt;
	}

	omDemMap.RemoveAll();

}


void CJobDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CJobDlg)
	DDX_Control(pDX, IDC_ACT5, m_Act5);
	DDX_Control(pDX, IDC_REDU, m_Redu);
	DDX_Control(pDX, IDC_AGROUP, m_ArrGroup);
	DDX_Control(pDX, IDC_DGROUP, m_DepGroup);
	DDX_Control(pDX, IDC_D_FTYP, m_D_FTYP);
	DDX_Control(pDX, IDC_A_FTYP, m_A_FTYP);
	DDX_Control(pDX, IDC_REGN, m_Regn);
	DDX_Control(pDX, IDC_ACTL, m_Actl);
	DDX_Control(pDX, IDC_A_ALC3, m_A_ALC3);
	DDX_Control(pDX, IDC_A_BAGN, m_A_BAGN);
	DDX_Control(pDX, IDC_A_ETAI, m_A_ETAI);
	DDX_Control(pDX, IDC_A_FLNS, m_A_FLNS);
	DDX_Control(pDX, IDC_A_FLTN, m_A_FLTN);
	DDX_Control(pDX, IDC_A_GTA1, m_A_GTA1);
	DDX_Control(pDX, IDC_A_ORG3, m_A_ORG3);
	DDX_Control(pDX, IDC_A_PAX1, m_A_PAX1);
	DDX_Control(pDX, IDC_A_PSTA, m_A_PSTA);
	DDX_Control(pDX, IDC_A_STOA, m_A_STOA);
	DDX_Control(pDX, IDC_A_VIA3, m_A_VIA3);
	DDX_Control(pDX, IDC_A_BAGW, m_A_BAGW);
	DDX_Control(pDX, IDC_D_ALC3, m_D_ALC3);
	DDX_Control(pDX, IDC_D_BAGN, m_D_BAGN);
	DDX_Control(pDX, IDC_D_CGOT, m_D_CGOT);
	DDX_Control(pDX, IDC_D_DES3, m_D_DES3);
	DDX_Control(pDX, IDC_D_ETDI, m_D_ETDI);
	DDX_Control(pDX, IDC_D_FLNS, m_D_FLNS);
	DDX_Control(pDX, IDC_D_FLTN, m_D_FLTN);
	DDX_Control(pDX, IDC_D_GTD1, m_D_GTD1);
	DDX_Control(pDX, IDC_D_MAIL, m_D_MAIL);
	DDX_Control(pDX, IDC_D_PAX1, m_D_PAX1);
	DDX_Control(pDX, IDC_D_PSTD, m_D_PSTD);
	DDX_Control(pDX, IDC_D_VIA3, m_D_VIA3);
	DDX_Control(pDX, IDC_A_CGOT, m_A_CGOT);
	DDX_Control(pDX, IDC_A_MAIL, m_A_MAIL);
	DDX_Control(pDX, IDC_D_STOD, m_D_STOD);
	DDX_Control(pDX, IDC_D_BAGW, m_D_BAGW);
	DDX_Control(pDX, IDC_DUTIES, m_Duties);
	DDX_Control(pDX, IDC_STATUS, m_Statusbar);
	DDX_Control(pDX, IDC_SCROLL, m_Scrollbar);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CJobDlg, CDialog)
	//{{AFX_MSG_MAP(CJobDlg)
	ON_WM_CTLCOLOR()
	ON_WM_HSCROLL()
    ON_MESSAGE(WM_UPDATEDIAGRAM,		OnUpdateDiagram)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CJobDlg message handlers

BOOL CJobDlg::OnInitDialog() 
{
	
	CDialog::OnInitDialog();

	CString olText;

	pomArrivalList->SubclassDlgItem(IDC_JOBGRIDA, this);
	pomDepatureList->SubclassDlgItem(IDC_JOBGRIDD, this);
	pomArrivalList->Initialize();
	pomDepatureList->Initialize();

	CGXStyle olStyle;

	pomDepatureList->LockUpdate(TRUE);
	pomDepatureList->GetParam()->EnableUndo(FALSE);
	pomDepatureList->GetParam()->EnableTrackColWidth(FALSE);
	pomDepatureList->GetParam()->EnableTrackRowHeight(FALSE);
	pomDepatureList->GetParam()->EnableSelection(GX_SELROW);
	pomDepatureList->GetParam()->SetNumberedColHeaders(FALSE);

	pomArrivalList->LockUpdate(TRUE);
	pomArrivalList->GetParam()->EnableUndo(FALSE);
	pomArrivalList->GetParam()->EnableTrackColWidth(FALSE);
	pomArrivalList->GetParam()->EnableTrackRowHeight(FALSE);
	pomArrivalList->GetParam()->EnableSelection(GX_SELROW);
	pomArrivalList->GetParam()->SetNumberedColHeaders(FALSE);

	
	pomDepatureList->SetColCount(2);


	pomDepatureList->SetColWidth(0,0,0);
	pomDepatureList->SetColWidth(0,1,50);
	pomDepatureList->SetColWidth(0,2,50);


	pomArrivalList->SetColCount(2);

	
	pomArrivalList->SetColWidth(0,0,0);
	pomArrivalList->SetColWidth(0,1,50);
	pomArrivalList->SetColWidth(0,2,50);


	pomDepatureList->SetRowHeight(0, 0, 12);
		
	pomArrivalList->SetRowHeight(0, 0, 12);
		

	pomArrivalList->SetRowCount(2);
	pomDepatureList->SetRowCount(2);		

	olStyle.SetEnabled(FALSE);
	olStyle.SetReadOnly(TRUE);

	pomDepatureList->LockUpdate(FALSE);
	pomArrivalList->LockUpdate(FALSE);

	CWnd *polWnd = GetDlgItem(IDC_FLIGHTROTATION);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1797));
	}

	polWnd = GetDlgItem(IDS_AIRCRAFTTYPE);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1798));
	}

	polWnd = GetDlgItem(IDC_AGROUP);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText("");
	}
	polWnd = GetDlgItem(IDC_DGROUP);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText("");
	}
	polWnd = GetDlgItem(IDS_FLUGATEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING507));
	}
	polWnd = GetDlgItem(IDS_FLUGDTEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING507));
	}
	polWnd = GetDlgItem(IDS_POSATEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61346));
	}
	polWnd = GetDlgItem(IDS_POSDTEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61346));
	}
	polWnd = GetDlgItem(IDS_GATEATEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61347));
	}
	polWnd = GetDlgItem(IDS_GATEDTEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61347));
	}
	polWnd = GetDlgItem(IDS_PAXATEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61349));
	}
	polWnd = GetDlgItem(IDS_PAXDTEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61349));
	}
	polWnd = GetDlgItem(IDS_CGOATEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61350));
	}
	polWnd = GetDlgItem(IDS_CGODTEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61350));
	}
	polWnd = GetDlgItem(IDS_STATEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61351));
	}
	polWnd = GetDlgItem(IDS_STDTEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61352));
	}
	polWnd = GetDlgItem(IDS_EATEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61353));
	}
	polWnd = GetDlgItem(IDS_EDTEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61354));
	}
	polWnd = GetDlgItem(IDS_BAGATEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61355));
	}

	polWnd = GetDlgItem(IDS_DDLFATEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1799));
	}
	polWnd = GetDlgItem(IDS_BAGDTEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61355));
	}
	polWnd = GetDlgItem(IDS_DDLFDTEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1800));
	}

	polWnd = GetDlgItem(IDS_MAILATEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1040));
	}
	polWnd = GetDlgItem(IDS_MAILDTEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING1040));
	}
	polWnd = GetDlgItem(IDS_REGATEXT);
	if(polWnd != NULL)
	{
		polWnd->SetWindowText(LoadStg(IDS_STRING61356));
	}


	SetWindowText(LoadStg(IDS_STRING61357));
	ogDdx.Register(this,S_DLG_FLIGHT_CHANGE,CString("FLIGHTDATA"), CString("Flight changed"),ProcessJobDlgCf);

	ogDdx.Register(this, INFO_DEM_NEW, CString("JobDlg"), CString("DEM New"), DemChangeCf);
	ogDdx.Register(this, INFO_DEM_CHANGE,CString("JobDlg"), CString("DEM Changed"), DemChangeCf);
	ogDdx.Register(this, INFO_DEM_DELETE,CString("JobDlg"), CString("DEM Deleted"), DemChangeCf);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//
// receives a pointer to the correct ogCedaFlightData - can be different instances of CedaFlightData
// for the FlightTable dialog and the Gantt-chart dialog
// receives the URNOs of the arrival flight AND/OR the departure flight (either can be zero if not present!!!)
//

void CJobDlg::InitFlightFields(CWnd *popNewOwner, const CStringArray *popTsrArray,CMapPtrToPtr *popDemMap,FLIGHTDATA *popArrFlight,FLIGHTDATA *popDepFlight, long lpArrUrno, long lpDepUrno,CViewer *popCallerView)
{
	CTime olCurrTime = CTime::GetCurrentTime();
	
	prmCurrentDem = NULL;

	omCalledFrom = "";//opCalledFrom;
	pomCallerView = popCallerView;

	SetOwner(popNewOwner);
	lmArrUrno = lpArrUrno;
	lmDepUrno = lpDepUrno;

	prmDepFlight = popDepFlight;
	prmArrFlight = popArrFlight;

	pomTsrArray = popTsrArray;

	omDemMap.RemoveAll();
	POSITION rlPos;
	long llUrno;
	DEMDATA *prlDem = NULL;
	for ( rlPos = popDemMap->GetStartPosition(); rlPos != NULL; )
	{	
		popDemMap->GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDem);
		if(prlDem != NULL)
		{
			omDemMap.SetAt((void *)prlDem->Urno,prlDem);
		}
	}
	pomViewer->SetDemPointer(&omDemMap);
	// intitialize the fields

	ClearFlightFields();


	CString olUrna,olUrnd;

	int ilUrnoCount = 0;
	if(lpArrUrno > 0)
	{
		olUrna.Format("%ld",lpArrUrno);
		ilUrnoCount++;
	}
	if(lpDepUrno > 0)
	{
		olUrnd.Format("%ld",lpDepUrno);
		ilUrnoCount++;
	}

	
	CString olUrnos;

	olUrnos += olUrna;
	if(ilUrnoCount > 1)
	{
		olUrnos += ",";
	}
	olUrnos += olUrnd;


	if(ilUrnoCount > 0)
	{
		olUrnos = "(" + olUrnos +")";

		CString olWhere = " WHERE URNO IN " + olUrnos ;
		
		int ilErr = ogBCD.Read("AFT",olWhere);
		ogBCD.AddKeyMap("AFT", "URNO");
		int  ilRecordCount = ogBCD.GetDataCount("AFT");


	}

	


	long llFkey = 0;


	CString olDisplayedFields = "URNO,REGN,ACT3,ACT5,ALC3,BAGN,BAGW,ETAI,ETDI,STOA,STOD,FLNS,FLTN,GTA1,PAX1,PSTA,PSTD,VIA3,VIA4,CGOT,MAIL";
	// handle arrival flight
	if( lpArrUrno > 0 )
	{
		if( prmArrFlight != NULL )
		{

			llFkey = prmArrFlight->Urno;
			CString olArrGroupCaption;

			CTime olStoa = prmArrFlight->Stoa;
			CTime olEtai = prmArrFlight->Etai;

			olArrGroupCaption.Format("%s: %s", LoadStg(IDS_STRING1531), olStoa.Format("%d.%m.%Y"));
			m_ArrGroup.SetWindowText(olArrGroupCaption);

			m_Regn.SetWindowText(prmArrFlight->Regn);
			//if(CString(prmArrFlight->Act3).IsEmpty())
			//{
				m_Actl.SetWindowText(prmArrFlight->Act3);
			//}
			//else
			//{
				m_Act5.SetWindowText(prmArrFlight->Act5);
			//}
			m_A_ALC3.SetWindowText(prmArrFlight->Alc3);
			m_A_BAGN.SetWindowText(IntToAsc(prmArrFlight->Bagn));
			m_A_BAGW.SetWindowText(IntToAsc(prmArrFlight->Bagw));

			m_A_ETAI.SetWindowText(DateToHourDivString(olEtai,olStoa));
			m_A_FLNS.SetWindowText(prmArrFlight->Flns);
			m_A_FLTN.SetWindowText(prmArrFlight->Fltn);
			m_A_GTA1.SetWindowText(prmArrFlight->Gta1);
			m_A_ORG3.SetWindowText(prmArrFlight->Org3);
			m_A_PAX1.SetWindowText(IntToAsc(prmArrFlight->Pax1));
			m_A_PSTA.SetWindowText(prmArrFlight->Psta);
			m_A_STOA.SetWindowText(DateToHourDivString(olStoa,olStoa));
			m_A_VIA3.SetWindowText(prmArrFlight->Via3);
			m_A_CGOT.SetWindowText(IntToAsc(prmArrFlight->Cgot));
			m_A_MAIL.SetWindowText(IntToAsc(prmArrFlight->Mail));

			m_A_FTYP.SetWindowText(ogFlightData.GetFtypText(prmArrFlight));
			
			olDisplayedFields += ",ORG3,ORG4";
		}
	}

	// handle departure flight
	if( lpDepUrno > 0 )
	{
		if( prmDepFlight != NULL )
		{
			llFkey = prmDepFlight->Urno;
			CString olDepGroupCaption;
			CTime olStod = prmDepFlight->Stod;
			CTime olEtai = prmDepFlight->Etdi;

			olDepGroupCaption.Format("%s: %s", LoadStg(IDS_STRING1533), olStod.Format("%d.%m.%Y"));
			m_DepGroup.SetWindowText(olDepGroupCaption);

			m_Regn.SetWindowText(prmDepFlight->Regn);
			//if(CString(prmDepFlight->Act3).IsEmpty())
			//{
				m_Actl.SetWindowText(prmDepFlight->Act3);
			//}
			//else
			//{
				m_Act5.SetWindowText(prmDepFlight->Act5);
			//}
			m_D_ALC3.SetWindowText(prmDepFlight->Alc3);
			m_D_BAGN.SetWindowText(IntToAsc(prmDepFlight->Bagn));
			m_D_BAGW.SetWindowText(IntToAsc(prmDepFlight->Bagw));
			m_D_CGOT.SetWindowText(IntToAsc(prmDepFlight->Cgot));
			m_D_DES3.SetWindowText(prmDepFlight->Des3);

			m_D_ETDI.SetWindowText(DateToHourDivString(olEtai,olStod));
			m_D_FLNS.SetWindowText(prmDepFlight->Flns);
			m_D_FLTN.SetWindowText(prmDepFlight->Fltn);
			m_D_GTD1.SetWindowText(prmDepFlight->Gtd1);
			m_D_MAIL.SetWindowText(IntToAsc(prmDepFlight->Mail));
			m_D_PAX1.SetWindowText(IntToAsc(prmDepFlight->Pax1));
			m_D_PSTD.SetWindowText(prmDepFlight->Pstd);
			m_D_STOD.SetWindowText(DateToHourDivString(olStod,olStod));
			m_D_VIA3.SetWindowText(prmDepFlight->Via3);
			m_D_FTYP.SetWindowText(ogFlightData.GetFtypText(prmDepFlight));

			olDisplayedFields += ",DES3,DES4";

		}
	}

	
	FillGrids(olDisplayedFields);

	CString olRedu;

	m_Redu.SetWindowText(olRedu);

	pomViewer->SetCurrentFlights(prmArrFlight, prmDepFlight);
	pomViewer->SetCurrentFkey(llFkey);
    omDuration = CTimeSpan(3, 0, 0, 0);
    omTSDuration = CTimeSpan(0, 3, 0, 0);
	CTime olEndTime = TIMENULL;
	CTime olStartTime = CTime::GetCurrentTime();


	if(pomGantt != NULL)
	{
		if(prmArrFlight != NULL && prmDepFlight == NULL)
		{
			if(prmArrFlight->Tifa != TIMENULL)
			{
				olStartTime = prmArrFlight->Tifa - CTimeSpan(0, 0, 30, 0);
			}
		}
		else if(prmArrFlight != NULL && prmDepFlight != NULL)
		{
			if(prmArrFlight->Tifa != TIMENULL)
			{
				olStartTime = prmArrFlight->Tifa - CTimeSpan(0, 0, 30, 0);
			}
			else
			{
				olStartTime = prmDepFlight->Tifd - CTimeSpan(0, 0, 30, 0);
			}
		}
		else if(prmDepFlight != NULL)
		{
			if(prmDepFlight->Tifd != TIMENULL)
			{
				olEndTime = prmDepFlight->Tifd + CTimeSpan(0, 0, 30, 0);
			}
			olStartTime = olEndTime - CTimeSpan(0, 3, 0, 0);//polDepFlight->Tifa - CTimeSpan(0, 3, 0, 0);
		}
	
		CTime olDur = olStartTime + CTimeSpan(0, 3, 0, 0);
				
		POSITION rlPos;
		long llUrno;
		DEMDATA *prlDem = NULL;
		for ( rlPos = omDemMap.GetStartPosition(); rlPos != NULL; )
		{	
			omDemMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDem);
			if(prlDem != NULL)
			{
				if(prlDem->DispDebe < olStartTime)
				{
					olStartTime = prlDem->DispDebe - CTimeSpan(0, 0, 30, 0);
				}
				if(prlDem->DispDeen > (olStartTime + CTimeSpan(0, 3, 0, 0)))
				{
					olDur = prlDem->DispDeen + CTimeSpan(0, 0, 30, 0);
				}		
			}
		}


		if(prmDepFlight != NULL) // und noch mit Tifd evtl. vergleichen
		{
			if(prmDepFlight->Tifd > olDur)
			{
				olDur = prmDepFlight->Tifd + CTimeSpan(0, 0, 30, 0);
			}
		}
	
		omStartTime = olStartTime;
//Duration setzen
		CString olDurS = olDur.Format("%d.%m.%Y-%H:%M");
		CString olStartTimeS = olStartTime.Format("%d.%m.%Y-%H:%M");
		if(olDur <= olStartTime)
		{
			omDuration = CTimeSpan(0, 10, 0, 0);
		}
		else
		{
			omDuration = olDur - olStartTime + CTimeSpan(0, 30, 0, 0);
		}
		long llTSMin = (olStartTime - omStartTime).GetTotalMinutes();
		long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
		m_Scrollbar.SetScrollRange(0, 1000, FALSE);
		if (llTotalMin > 0)
		{
			m_Scrollbar.SetScrollPos((int) (1000 * llTSMin / llTotalMin), FALSE);
		}
		else
		{
			m_Scrollbar.SetScrollPos(0, FALSE);
		}
/**********************************/
		olEndTime = olStartTime + CTimeSpan(0, 3, 0, 0);
		pomViewer->ChangeViewTo(0, olStartTime, olEndTime);
		CString olS = olStartTime.Format("%d.%m.%Y - %H:%M");
		CString olE = olEndTime.Format("%d.%m.%Y - %H:%M");
		pomGantt->DestroyWindow();
		delete pomGantt;
		MakeNewGantt(olStartTime);

	}
}

CString CJobDlg::IntToAsc(int ipInt)
{
	CString olAsc;
	olAsc.Format("%d",ipInt);
	return olAsc;
}

void CJobDlg::ClearFlightFields(void)
{
	m_Regn.SetWindowText("");
	m_Redu.SetWindowText("");
	m_Actl.SetWindowText("");
	m_Act5.SetWindowText("");
	m_A_ALC3.SetWindowText("");
	m_A_BAGN.SetWindowText("");
	m_A_BAGW.SetWindowText("");
	m_A_ETAI.SetWindowText("");
	m_A_FLNS.SetWindowText("");
	m_A_FLTN.SetWindowText("");
	m_A_GTA1.SetWindowText("");
	m_A_ORG3.SetWindowText("");
	m_A_PAX1.SetWindowText("");
	m_A_PSTA.SetWindowText("");
	m_A_STOA.SetWindowText("");
	m_A_VIA3.SetWindowText("");
	m_A_CGOT.SetWindowText("");
	m_A_MAIL.SetWindowText("");
	m_A_FTYP.SetWindowText("");

	m_D_ALC3.SetWindowText("");
	m_D_BAGN.SetWindowText("");
	m_D_BAGW.SetWindowText("");
	m_D_CGOT.SetWindowText("");
	m_D_DES3.SetWindowText("");
	m_D_ETDI.SetWindowText("");
	m_D_FLNS.SetWindowText("");
	m_D_FLTN.SetWindowText("");
	m_D_GTD1.SetWindowText("");
	m_D_MAIL.SetWindowText("");
	m_D_PAX1.SetWindowText("");
	m_D_PSTD.SetWindowText("");
	m_D_STOD.SetWindowText("");
	m_D_VIA3.SetWindowText("");
	m_D_FTYP.SetWindowText("");
	
	if(pomArrivalList->GetRowCount() > 0)
		pomArrivalList->RemoveRows(1,  pomArrivalList->GetRowCount());
	if(pomDepatureList->GetRowCount() > 0)
		pomDepatureList->RemoveRows(1,  pomDepatureList->GetRowCount());


	
}

void CJobDlg::FillGrids(CString opDisplayedFields)
{
	if(pomTsrArray != NULL)
	{

		CString olTsrText;
		CString olAftVal;
		CString olAftField;
		CStringArray olTsrArray;
		CStringArray olAftFieldArrayA;
		CStringArray olAftValArrayA;
		CStringArray olAftFieldArrayD;
		CStringArray olAftValArrayD;
		CString olUrna;
		CString olUrnd;
		int ilRowCount = pomTsrArray->GetSize();
		bool blPaxArrFound = false;
		bool blPaxDepFound = false;

		olUrna.Format("%ld",lmArrUrno);
		olUrnd.Format("%ld",lmDepUrno);

	
		RecordSet olAftArrRecord(ogBCD.GetFieldCount("AFT"));
		RecordSet olAftDepRecord(ogBCD.GetFieldCount("AFT"));

		RecordSet olPaxArrRecord(ogBCD.GetFieldCount("PAX"));
		RecordSet olPaxDepRecord(ogBCD.GetFieldCount("PAX"));


		CString olWhere;
		olWhere.Format("WHERE AFTU IN ('%s','%s')",olUrna,olUrnd);
		ogBCD.Read("PAX",olWhere);

		ogBCD.GetRecord("AFT","URNO", olUrna,olAftArrRecord);
		ogBCD.GetRecord("AFT","URNO", olUrnd,olAftDepRecord);

		blPaxArrFound = ogBCD.GetRecord("PAX","AFTU", olUrna,olPaxArrRecord);
		blPaxDepFound = ogBCD.GetRecord("PAX","AFTU", olUrnd,olPaxDepRecord);

		if(blPaxArrFound)
		{
			int ilIdx = ogBCD.GetFieldIndex("PAX", "PAXT");
			m_A_PAX1.SetWindowText(olPaxArrRecord.Values[ilIdx]);
		}

		if(blPaxDepFound)
		{
			int ilIdx = ogBCD.GetFieldIndex("PAX", "PAXT");
			m_D_PAX1.SetWindowText(olPaxDepRecord.Values[ilIdx]);
		}

		for(int ilTsr = 0; ilTsr < ilRowCount; ilTsr++)
		{
			olTsrText = pomTsrArray->GetAt(ilTsr);
		 	ExtractItemList(olTsrText, &olTsrArray, ',');
			if(olTsrArray.GetSize() > 3)
			{
				olAftField = olTsrArray[1];
				CString olType = olTsrArray[3];
				if(opDisplayedFields.Find(olAftField) < 0)
				{
					opDisplayedFields += ",";
					opDisplayedFields += olAftField;
					if(olTsrArray[0] != '2')
					{
						if(lmArrUrno > 0)
						{
							if(!olTsrArray[2].IsEmpty())
							{
								int ilIdx = ogBCD.GetFieldIndex("AFT", olTsrArray[1]);
								if (ilIdx >= 0)
								{
									olAftVal = olAftArrRecord.Values[ilIdx];
									olAftFieldArrayA.InsertAt(olAftFieldArrayA.GetSize(),olTsrArray[2]);
									if(olType == "DTIM")
									{
										CTime olUtcTime = DBStringToDateTime(olAftVal);
										ogBasicData.UtcToLocal(olUtcTime);
										olAftVal = olUtcTime.Format("%H:%M / %d.%m.%Y");
									}
									olAftValArrayA.InsertAt(olAftValArrayA.GetSize(),olAftVal);
								}
							}
						}
					}
					if(olTsrArray[0] != '1')
					{
						if(lmDepUrno > 0)
						{
							if(!olTsrArray[2].IsEmpty())
							{
								int ilIdx = ogBCD.GetFieldIndex("AFT", olTsrArray[1]);
								if (ilIdx >= 0)
								{
									olAftVal = olAftDepRecord.Values[ilIdx];
									olAftFieldArrayD.InsertAt(olAftFieldArrayD.GetSize(),olTsrArray[2]);
									if(olType == "DTIM")
									{
										CTime olUtcTime = DBStringToDateTime(olAftVal);
										ogBasicData.UtcToLocal(olUtcTime);
										olAftVal = olUtcTime.Format("%H:%M / %d.%m.%Y");
									}
									olAftValArrayD.InsertAt(olAftValArrayD.GetSize(),olAftVal);
								}
							}
						}
					}
					
				}
			}
		}

		CGXStyle olStyle;

		
		olStyle.SetEnabled(FALSE);
		olStyle.SetReadOnly(TRUE);

		int ilRowCountA = olAftFieldArrayA.GetSize();

		pomArrivalList->SetRowCount(ilRowCountA);
			
		pomArrivalList->SetColWidth(0,0,0);
		pomArrivalList->SetColWidth(1,1,150);
		pomArrivalList->SetColWidth(2,2,150);

		for(int ilLcA = 0; ilLcA < ilRowCountA; ilLcA++)
		{	
			pomArrivalList->SetStyleRange(CGXRange(ilLcA+1, 1, ilLcA+1, 1), olStyle.SetValue(olAftFieldArrayA[ilLcA]));
			pomArrivalList->SetStyleRange(CGXRange(ilLcA+1, 2, ilLcA+1, 2), olStyle.SetValue(olAftValArrayA[ilLcA]));
		}
		int ilRowCountD = olAftFieldArrayD.GetSize();

		pomDepatureList->SetRowCount(ilRowCountD);
			
		pomDepatureList->SetColWidth(0,0,0);
		pomDepatureList->SetColWidth(1,1,150);
		pomDepatureList->SetColWidth(2,2,150);

		for(int ilLcD = 0; ilLcD < ilRowCountD; ilLcD++)
		{	
			pomDepatureList->SetStyleRange(CGXRange(ilLcD+1, 1, ilLcD+1, 1), olStyle.SetValue(olAftFieldArrayD[ilLcD]));
			pomDepatureList->SetStyleRange(CGXRange(ilLcD+1, 2, ilLcD+1, 2), olStyle.SetValue(olAftValArrayD[ilLcD]));
		}

	}
}


void CJobDlg::OnCancel() 
{
	CDialog::OnCancel();
	DestroyWindow();
}

void CJobDlg::PostNcDestroy() 
{
	delete this;
	pogJobDlg = NULL;
	
	CDialog::PostNcDestroy();
}


BOOL CJobDlg::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CDialog::Create(IDD, pParentWnd);
}

HBRUSH CJobDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = 0;
	long llRc;

//	m_NCol = RGB(0,255,0);
	CWnd *polWnd;// = GetDlgItem(IDC_ADD_STANDARD);
	polWnd = GetDlgItem(IDC_AGROUP);
	if(polWnd != NULL)
	{
		if (polWnd->m_hWnd == pWnd->m_hWnd)
		{
			llRc = pDC->SetBkColor(SILVER);
			llRc = pDC->SetTextColor(PURPLE);
			omBrush.DeleteObject();
			omBrush.CreateSolidBrush(SILVER);
			hbr = omBrush;	
			return hbr;
		}
	}
	polWnd = GetDlgItem(IDC_DGROUP);
	if(polWnd != NULL)
	{
		if (polWnd->m_hWnd == pWnd->m_hWnd)
		{
			llRc = pDC->SetBkColor(SILVER);
			llRc = pDC->SetTextColor(PURPLE);
			omBrush.DeleteObject();
			omBrush.CreateSolidBrush(SILVER);
			hbr = omBrush;	
			return hbr;
		}
	}
	

	return CDialog::OnCtlColor(pDC,pWnd,nCtlColor);
}


void CJobDlg::MakeNewGantt(CTime opStartTime)
{
	CCS_TRY
	CTime olTSStartTime = opStartTime;//CTime(1980, 1, 1, 0, 0, 0);
	CRect olTSRect;


	m_Duties.GetWindowRect(&olTSRect);
//	m_Duties.ClientToScreen(&olTSRect);
	ScreenToClient(&olTSRect);


	//olTSRect.top = 347;
	olTSRect.bottom = olTSRect.top + 18;
	olTSRect.left += imVerticalScaleIndent;//161;

    CTimeSpan olTSDuration = CTimeSpan(0, 3, 0, 0);
    CTimeSpan olTSInterval = CTimeSpan(0, 0, 10, 0);

	omTSStartTime = olTSStartTime;
    omTimeScale.SetDisplayTimeFrame(olTSStartTime, olTSDuration, olTSInterval);

	pomGantt = new DispoDetailGantt;
    pomGantt->SetTimeScale(&omTimeScale);
	pomGantt->SetStatusBar(&m_Statusbar);
    pomGantt->SetViewer(pomViewer);
	CTimeSpan dur = omTimeScale.GetDisplayDuration();
    pomGantt->SetDisplayWindow(olTSStartTime, olTSStartTime + omTimeScale.GetDisplayDuration());
    pomGantt->SetVerticalScaleWidth(150);
	pomGantt->SetVerticalScaleIndent(imVerticalScaleIndent);
	pomGantt->SetVerticalScaleColors(BLACK, SILVER, AQUA, SILVER);
    pomGantt->SetFonts(3, 3);
    //pomGantt->SetVerticalScaleColors(NAVY, SILVER, YELLOW, SILVER);
	pomGantt->SetGanttChartColors(NAVY, SILVER, YELLOW, SILVER);
    pomGantt->Create(WS_BORDER, CRect(olTSRect.left-imVerticalScaleIndent+1, olTSRect.bottom + 1, olTSRect.right, olTSRect.bottom+185), (CWnd*)this);
	pomGantt->AttachWindow(this);
	CCS_CATCH_ALL
}


void CJobDlg::SetTSStartTime(CTime opTSStartTime)
{
	CCS_TRY
    omTSStartTime = opTSStartTime;
    if ((omTSStartTime < omStartTime) ||
		(omTSStartTime > omStartTime + omDuration))
    {
        omStartTime = CTime(omTSStartTime.GetYear(), omTSStartTime.GetMonth(), omTSStartTime.GetDay(),
            0, 0, 0) - CTimeSpan(1, 0, 0, 0);
    }
        
    char clBuf[8];
    sprintf(clBuf, "%02d%02d%02d",
        omTSStartTime.GetDay(), omTSStartTime.GetMonth(), omTSStartTime.GetYear() % 1000);
	CCS_CATCH_ALL
//    omTSDate.SetWindowText(clBuf);
//    omTSDate.Invalidate(FALSE);
}

void CJobDlg::ChangeViewTo()
{
	CCS_TRY
	//AfxGetApp()->DoWaitCursor(1);
	CRect olRect; omTimeScale.GetClientRect(&olRect);
    pomViewer->ChangeViewTo(0, omTSStartTime, omTSStartTime + omTSDuration);
	delete pomGantt;
	MakeNewGantt(omTSStartTime);
	CCS_CATCH_ALL
	//AfxGetApp()->DoWaitCursor(-1);
}


void CJobDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	CCS_TRY
    // TODO: Add your message handler code here and/or call default
    
    //CFrameWnd::OnHScroll(nSBCode, nPos, pScrollBar);

    long llTotalMin;
    int ilPos;
	int ilIndex = pomGantt->GetTopIndex();
    
    switch (nSBCode)
    {
        case SB_LINEUP :
            //OutputDebugString("LineUp\n\r");
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes(); 
			if (llTotalMin > 0)
			{
	            ilPos = m_Scrollbar.GetScrollPos() - int (60 * 1000L / llTotalMin);
			}
			else
			{
				ilPos = 0;
			}

            if (ilPos <= 0)
            {
                ilPos = 0;
                SetTSStartTime(omStartTime);
            }
            else
                SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, 60, 0));

            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);
			ChangeViewTo();//ogCfgData.rmUserSetup.STCV,TRUE);
            m_Scrollbar.SetScrollPos(ilPos, TRUE);
			if (ilIndex != LB_ERR)
				pomGantt->SetTopIndex(ilIndex);

        break;
        
        case SB_LINEDOWN :

            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes(); 
			if (llTotalMin > 0)
			{
				ilPos = m_Scrollbar.GetScrollPos() + int (60 * 1000L / llTotalMin);
			}
			else
			{
				ilPos = 0;
			}

            if (ilPos >= 1000)
            {
                ilPos = 1000;
                SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin), 0));
            }
            else
                SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, 60, 0));

            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);
			ChangeViewTo();//ogCfgData.rmUserSetup.STCV,TRUE);

			m_Scrollbar.SetScrollPos(ilPos, TRUE);
			if (ilIndex != LB_ERR)
				pomGantt->SetTopIndex(ilIndex);
        break;
        
        case SB_PAGEUP :
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();            
			if (llTotalMin > 0)
			{
	            ilPos = m_Scrollbar.GetScrollPos() - int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
			}
			else
			{
				ilPos = 0;
			}

            if (ilPos <= 0)
            {
                ilPos = 0;
                SetTSStartTime(omStartTime);
            }
            else
                SetTSStartTime(omTSStartTime - CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));

            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);
			ChangeViewTo();//ogCfgData.rmUserSetup.STCV,TRUE);

            m_Scrollbar.SetScrollPos(ilPos, TRUE);
			if (ilIndex != LB_ERR)
				pomGantt->SetTopIndex(ilIndex);
        break;
        
        case SB_PAGEDOWN :
            
            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes(); 
			if (llTotalMin > 0)
			{
				ilPos = m_Scrollbar.GetScrollPos() + int ((omTSDuration.GetTotalMinutes() / 2) * 1000L / llTotalMin);
			}
			else
			{
				ilPos = 0;
			}

            if (ilPos >= 1000)
            {
                ilPos = 1000;
                SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin), 0));
            }
            else
                SetTSStartTime(omTSStartTime + CTimeSpan(0, 0, int (omTSDuration.GetTotalMinutes() / 2), 0));

            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);
			ChangeViewTo();//ogCfgData.rmUserSetup.STCV,TRUE);

            m_Scrollbar.SetScrollPos(ilPos, TRUE);
			if (ilIndex != LB_ERR)
				pomGantt->SetTopIndex(ilIndex);
        break;
        
        case SB_THUMBTRACK /* pressed, any drag time */:

            llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
            
            SetTSStartTime(omStartTime + CTimeSpan(0, 0, int (llTotalMin * nPos / 1000), 0));
			////ChangeViewTo(omViewer.SelectView());
            
            omTimeScale.SetDisplayStartTime(omTSStartTime);
            omTimeScale.Invalidate(TRUE);
            
            m_Scrollbar.SetScrollPos(nPos, TRUE);
			ChangeViewTo();//ogCfgData.rmUserSetup.STCV,TRUE);
			if (ilIndex != LB_ERR)
				pomGantt->SetTopIndex(ilIndex);

            //char clBuf[64];
            //wsprintf(clBuf, "Thumb Position : %d", nPos);
            //omStatusBar.SetWindowText(clBuf);
        break;

        //case SB_THUMBPOSITION /* released */:
            //OutputDebugString("ThumbPosition\n\r");
        //break;
        case SB_THUMBPOSITION:	// the thumb was just released?
			//ChangeViewTo();//ogCfgData.rmUserSetup.STCV,TRUE);

			return;
////////////////////////////////////////////////////////////////////////

        case SB_TOP :
        //break;
        case SB_BOTTOM :
            //OutputDebugString("TopBottom\n\r");
        break;
        
        case SB_ENDSCROLL :
            //OutputDebugString("EndScroll\n\r");
            //OutputDebugString("\n\r");
        break;
    }
	CCS_CATCH_ALL
}

static int CompareDemDubeData(const DEMDATA **e1, const DEMDATA **e2)
{
	return ((**e1).Debe == (**e2).Debe)? 0:
		((**e1).Debe > (**e2).Debe)? 1: -1;
}
static int CompareDemDuenData(const DEMDATA **e1, const DEMDATA **e2)
{
	return ((**e1).Deen == (**e2).Deen)? 0:
		((**e1).Debe < (**e2).Debe)? 1: -1;
}

static int ComparePfcData(const PFCDATA **e1, const PFCDATA **e2)
{
	return (strcmp((**e1).Fctc, (**e2).Fctc));
}

static int CompareGegData(const GEGDATA **e1, const GEGDATA **e2)
{
	CCS_TRY
	return (strcmp((**e1).Gcde, (**e2).Gcde));
	CCS_CATCH_ALL
	return 0;
}

void CJobDlg::OnDestroy()
{
	ogDdx.UnRegister(this,NOTUSED);
	CDialog::OnDestroy();
}

LONG CJobDlg::OnUpdateDiagram(WPARAM wParam, LPARAM lParam)
{
	CCS_TRY
    CString olStr;
    int ipGroupNo = HIWORD(lParam);
    int ipLineNo = LOWORD(lParam);
	 CTime olTime = CTime((time_t) lParam);


    switch (wParam)
    {
        case UD_INSERTLINE :
            pomGantt->InsertString(ipLineNo, "");
			   pomGantt->RepaintItemHeight(ipLineNo);
        break;
        case UD_UPDATELINE :
            pomGantt->RepaintVerticalScale(ipLineNo);
            pomGantt->RepaintGanttChart(ipLineNo);
        break;

        case UD_DELETELINE :
            pomGantt->DeleteString(ipLineNo);
			break;
		case UD_RESETCONTENT:
			pomGantt->ResetContent();
			break;
		break;
    }
	return 0L;
	CCS_CATCH_ALL
    return 0L;
}


void CJobDlg::SetCallerViewer(CViewer *popCallerView)
{
	pomCallerView = popCallerView;
	if(pomCallerView==NULL)
	{
		//m_Redu.SetWindowText("");
		ClearFlightFields();
		ChangeViewTo();
		//OnCancel();
	}
}

CViewer *CJobDlg::GetCallerViewer()
{
	return pomCallerView;
	//= popCallerView;
	//if(pomCallerView==NULL)
	//{
	//	ClearFlightFields();
		//OnCancel();
	//}
}


void CJobDlg::SetOwner(CWnd *popNewOwner)
{
	pomOwner = popNewOwner;
	{
		CWnd::SetOwner(popNewOwner);
		//CWnd *polWnd = GetOwner();
		//CRuntimeClass *prt = polWnd->GetRuntimeClass();
		//TRACE("Runtime class = <%s>\n",prt->m_lpszClassName);
	}
}

void  CJobDlg::ProcessJobDlgCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	CJobDlg *This = static_cast<CJobDlg *>(popInstance);

	switch(ipDDXType)
	{
	case S_DLG_FLIGHT_CHANGE :
		{
			FLIGHTDATA *polFlight	= static_cast<FLIGHTDATA *>(vpDataPointer);
			if (This->prmArrFlight != NULL && This->prmArrFlight->Urno == polFlight->Urno)
			{
				This->UpdateFlightFields(polFlight,NULL,true);
			}
			else if (This->prmDepFlight != NULL && This->prmDepFlight->Urno == polFlight->Urno)
			{
				This->UpdateFlightFields(NULL,polFlight,true);
			}
		}
	break;
	}
}

void CJobDlg::UpdateFlightFields(FLIGHTDATA *popArrFlight,FLIGHTDATA *popDepFlight,bool bpUpdate)
{
	if (!::IsWindow(this->m_hWnd))
		return;

	CTime olCurrTime = CTime::GetCurrentTime();

	if (bpUpdate)
	{
		if (popArrFlight)
		{
			prmArrFlight = popArrFlight;
			lmArrUrno = popArrFlight->Urno;
		}
		if (popDepFlight)
		{
			lmDepUrno = popDepFlight->Urno;
			prmDepFlight = popDepFlight;
		}

	}
	else
	{
		if (popArrFlight)
		{
			prmArrFlight = NULL;
			lmArrUrno = 0;
		}
		if (popDepFlight)
		{
			lmDepUrno = 0;
			prmDepFlight = NULL;
		}
	}

	// intitialize the fields
	ClearFlightFields();


	CString olUrna,olUrnd;

	int ilUrnoCount = 0;
	if(lmArrUrno > 0)
	{
		olUrna.Format("%ld",lmArrUrno);
		ilUrnoCount++;
	}
	if(lmDepUrno > 0)
	{
		olUrnd.Format("%ld",lmDepUrno);
		ilUrnoCount++;
	}

	
	CString olUrnos;

	olUrnos += olUrna;
	if(ilUrnoCount > 1)
	{
		olUrnos += ",";
	}
	olUrnos += olUrnd;


	if(ilUrnoCount > 0)
	{
		olUrnos = "(" + olUrnos +")";

		CString olWhere = " WHERE URNO IN " + olUrnos ;
		
		int ilErr = ogBCD.Read("AFT",olWhere);
		ogBCD.AddKeyMap("AFT", "URNO");
		int  ilRecordCount = ogBCD.GetDataCount("AFT");


	}

	long llFkey = 0;

	CString olDisplayedFields = "URNO,REGN,ACT3,ACT5,ALC3,BAGN,BAGW,ETAI,ETDI,STOA,STOD,FLNS,FLTN,GTA1,PAX1,PSTA,PSTD,VIA3,VIA4,CGOT,MAIL";
	// handle arrival flight
	if( lmArrUrno > 0 )
	{
		if( prmArrFlight != NULL )
		{

			llFkey = prmArrFlight->Urno;
			CString olArrGroupCaption;

			CTime olStoa = prmArrFlight->Stoa;
			CTime olEtai = prmArrFlight->Etai;

			olArrGroupCaption.Format("%s: %s", LoadStg(IDS_STRING1531), olStoa.Format("%d.%m.%Y"));
			m_ArrGroup.SetWindowText(olArrGroupCaption);

			m_Regn.SetWindowText(prmArrFlight->Regn);
			//if(CString(prmArrFlight->Act3).IsEmpty())
			//{
				m_Actl.SetWindowText(prmArrFlight->Act3);
			//}
			//else
			//{
				m_Act5.SetWindowText(prmArrFlight->Act5);
			//}
			m_A_ALC3.SetWindowText(prmArrFlight->Alc3);
			m_A_BAGN.SetWindowText(IntToAsc(prmArrFlight->Bagn));
			m_A_BAGW.SetWindowText(IntToAsc(prmArrFlight->Bagw));

			m_A_ETAI.SetWindowText(DateToHourDivString(olEtai,olStoa));
			m_A_FLNS.SetWindowText(prmArrFlight->Flns);
			m_A_FLTN.SetWindowText(prmArrFlight->Fltn);
			m_A_GTA1.SetWindowText(prmArrFlight->Gta1);
			m_A_ORG3.SetWindowText(prmArrFlight->Org3);
			m_A_PAX1.SetWindowText(IntToAsc(prmArrFlight->Pax1));
			m_A_PSTA.SetWindowText(prmArrFlight->Psta);
			m_A_STOA.SetWindowText(DateToHourDivString(olStoa,olStoa));
			m_A_VIA3.SetWindowText(prmArrFlight->Via3);
			m_A_CGOT.SetWindowText(IntToAsc(prmArrFlight->Cgot));
			m_A_MAIL.SetWindowText(IntToAsc(prmArrFlight->Mail));

			m_A_FTYP.SetWindowText(ogFlightData.GetFtypText(prmArrFlight));
			
			olDisplayedFields += ",ORG3,ORG4";
		}
	}

	// handle departure flight
	if( lmDepUrno > 0 )
	{
		if( prmDepFlight != NULL )
		{
			llFkey = prmDepFlight->Urno;
			CString olDepGroupCaption;
			CTime olStod = prmDepFlight->Stod;
			CTime olEtai = prmDepFlight->Etdi;

			olDepGroupCaption.Format("%s: %s", LoadStg(IDS_STRING1533), olStod.Format("%d.%m.%Y"));
			m_DepGroup.SetWindowText(olDepGroupCaption);

			m_Regn.SetWindowText(prmDepFlight->Regn);
			//if(CString(prmDepFlight->Act3).IsEmpty())
			//{
				m_Actl.SetWindowText(prmDepFlight->Act3);
			//}
			//else
			//{
				m_Act5.SetWindowText(prmDepFlight->Act5);
			//}
			m_D_ALC3.SetWindowText(prmDepFlight->Alc3);
			m_D_BAGN.SetWindowText(IntToAsc(prmDepFlight->Bagn));
			m_D_BAGW.SetWindowText(IntToAsc(prmDepFlight->Bagw));
			m_D_CGOT.SetWindowText(IntToAsc(prmDepFlight->Cgot));
			m_D_DES3.SetWindowText(prmDepFlight->Des3);

			m_D_ETDI.SetWindowText(DateToHourDivString(olEtai,olStod));
			m_D_FLNS.SetWindowText(prmDepFlight->Flns);
			m_D_FLTN.SetWindowText(prmDepFlight->Fltn);
			m_D_GTD1.SetWindowText(prmDepFlight->Gtd1);
			m_D_MAIL.SetWindowText(IntToAsc(prmDepFlight->Mail));
			m_D_PAX1.SetWindowText(IntToAsc(prmDepFlight->Pax1));
			m_D_PSTD.SetWindowText(prmDepFlight->Pstd);
			m_D_STOD.SetWindowText(DateToHourDivString(olStod,olStod));
			m_D_VIA3.SetWindowText(prmDepFlight->Via3);
			m_D_FTYP.SetWindowText(ogFlightData.GetFtypText(prmDepFlight));

			olDisplayedFields += ",DES3,DES4";

		}
	}

	
	FillGrids(olDisplayedFields);

	CString olRedu;

	m_Redu.SetWindowText(olRedu);

	pomViewer->SetCurrentFlights(prmArrFlight, prmDepFlight);
	pomViewer->SetCurrentFkey(llFkey);
    omDuration = CTimeSpan(3, 0, 0, 0);
    omTSDuration = CTimeSpan(0, 3, 0, 0);
	CTime olEndTime = TIMENULL;
	CTime olStartTime = CTime::GetCurrentTime();


	if(pomGantt != NULL)
	{
		if(prmArrFlight != NULL && prmDepFlight == NULL)
		{
			if(prmArrFlight->Tifa != TIMENULL)
			{
				olStartTime = prmArrFlight->Tifa - CTimeSpan(0, 0, 30, 0);
			}
		}
		else if(prmArrFlight != NULL && prmDepFlight != NULL)
		{
			if(prmArrFlight->Tifa != TIMENULL)
			{
				olStartTime = prmArrFlight->Tifa - CTimeSpan(0, 0, 30, 0);
			}
			else
			{
				olStartTime = prmDepFlight->Tifd - CTimeSpan(0, 0, 30, 0);
			}
		}
		else if(prmDepFlight != NULL)
		{
			if(prmDepFlight->Tifd != TIMENULL)
			{
				olEndTime = prmDepFlight->Tifd + CTimeSpan(0, 0, 30, 0);
			}
			olStartTime = olEndTime - CTimeSpan(0, 3, 0, 0);//polDepFlight->Tifa - CTimeSpan(0, 3, 0, 0);
		}
	
		CTime olDur = olStartTime + CTimeSpan(0, 3, 0, 0);
				
		POSITION rlPos;
		long llUrno;
		DEMDATA *prlDem = NULL;
		for ( rlPos = omDemMap.GetStartPosition(); rlPos != NULL; )
		{	
			omDemMap.GetNextAssoc(rlPos, (void *&)llUrno, (void *&)prlDem);
			if(prlDem != NULL)
			{
				if(prlDem->DispDebe < olStartTime)
				{
					olStartTime = prlDem->DispDebe - CTimeSpan(0, 0, 30, 0);
				}
				if(prlDem->DispDeen > (olStartTime + CTimeSpan(0, 3, 0, 0)))
				{
					olDur = prlDem->DispDeen + CTimeSpan(0, 0, 30, 0);
				}		
			}
		}


		if(prmDepFlight != NULL) // und noch mit Tifd evtl. vergleichen
		{
			if(prmDepFlight->Tifd > olDur)
			{
				olDur = prmDepFlight->Tifd + CTimeSpan(0, 0, 30, 0);
			}
		}
	
		omStartTime = olStartTime;
//Duration setzen
		CString olDurS = olDur.Format("%d.%m.%Y-%H:%M");
		CString olStartTimeS = olStartTime.Format("%d.%m.%Y-%H:%M");
		if(olDur <= olStartTime)
		{
			omDuration = CTimeSpan(0, 10, 0, 0);
		}
		else
		{
			omDuration = olDur - olStartTime + CTimeSpan(0, 30, 0, 0);
		}
		long llTSMin = (olStartTime - omStartTime).GetTotalMinutes();
		long llTotalMin = omDuration.GetTotalMinutes() - omTSDuration.GetTotalMinutes();
		m_Scrollbar.SetScrollRange(0, 1000, FALSE);
		if (llTotalMin > 0)
		{
			m_Scrollbar.SetScrollPos((int) (1000 * llTSMin / llTotalMin), FALSE);
		}
		else
		{
			m_Scrollbar.SetScrollPos(0, FALSE);
		}
/**********************************/
		olEndTime = olStartTime + CTimeSpan(0, 3, 0, 0);
		pomViewer->ChangeViewTo(0, olStartTime, olEndTime);
		CString olS = olStartTime.Format("%d.%m.%Y - %H:%M");
		CString olE = olEndTime.Format("%d.%m.%Y - %H:%M");
		pomGantt->DestroyWindow();
		delete pomGantt;
		MakeNewGantt(olStartTime);

	}
}


void CJobDlg::ProcessDemChanges ( int ipDDXType, DEMDATA *prpDem )
{
	if ( (ipDDXType==INFO_DEM_CHANGE) || (ipDDXType==INFO_DEM_NEW) )
	{
		omDemMap.SetAt((void *)prpDem->Urno,prpDem);
		pomViewer->SetDemPointer(&omDemMap);
		pomViewer->ProcessDemChange(prpDem);
	}
	if ( ipDDXType==INFO_DEM_DELETE )
	{
		omDemMap.RemoveKey ( (void *)prpDem->Urno );
		pomViewer->SetDemPointer(&omDemMap);
		pomViewer->ProcessDemDelete(prpDem);
	}
}

static void DemChangeCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    CJobDlg *polDlg = (CJobDlg *)popInstance;
	DEMDATA *prlDem = (DEMDATA *)vpDataPointer;

	if ( !polDlg || !prlDem )
		return ;

	if ( (prlDem->Ouri==polDlg->lmArrUrno) || (prlDem->Ouri==polDlg->lmDepUrno) )
	{
		polDlg->ProcessDemChanges ( ipDDXType, prlDem );
	}
}


