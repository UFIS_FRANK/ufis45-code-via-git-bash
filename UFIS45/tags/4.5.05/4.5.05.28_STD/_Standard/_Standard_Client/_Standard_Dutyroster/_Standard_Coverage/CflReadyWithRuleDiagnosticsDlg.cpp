// CflReadyWithRuleDiagnosticsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "coverage.h"
#include "CflReadyWithRuleDiagnosticsDlg.h"
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCflReadyWithRuleDiagnosticsDlg dialog


CCflReadyWithRuleDiagnosticsDlg::CCflReadyWithRuleDiagnosticsDlg(CWnd* pParent /*=NULL*/)
	: CCflReadyDlg(CCflReadyWithRuleDiagnosticsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCflReadyWithRuleDiagnosticsDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	ftp_connection = NULL;
}


void CCflReadyWithRuleDiagnosticsDlg::DoDataExchange(CDataExchange* pDX)
{
	CCflReadyDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCflReadyWithRuleDiagnosticsDlg)
	DDX_Control(pDX, IDC_RULE_RESULT, m_RuleResults);
	DDX_Control(pDX, IDC_RULE_FILES, m_RuleFiles);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCflReadyWithRuleDiagnosticsDlg, CCflReadyDlg)
	//{{AFX_MSG_MAP(CCflReadyWithRuleDiagnosticsDlg)
	ON_LBN_DBLCLK(IDC_RULE_FILES, OnDblclkRuleFiles)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCflReadyWithRuleDiagnosticsDlg message handlers

void CCflReadyWithRuleDiagnosticsDlg::OnDblclkRuleFiles() 
{
	// TODO: Add your control notification handler code here
	int ilInd = this->m_RuleFiles.GetCurSel();
	if (ilInd != LB_ERR)
	{
		CWaitCursor olWait;
		CString olRuleFile;
		m_RuleFiles.GetText(ilInd,olRuleFile);
		LoadRuleFile(olRuleFile);
	}
}

BOOL CCflReadyWithRuleDiagnosticsDlg::OnInitDialog() 
{
	CCflReadyDlg::OnInitDialog();
	
	// TODO: Add extra initialization here
	ScanServer();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCflReadyWithRuleDiagnosticsDlg::ScanServer()
{

	try
	{
		ogBasicData.GetUserName();
		ftp_connection = inet_session.GetFtpConnection(ogCommHandler.pcmRealHostName,ogBasicData.GetUserName(),ogBasicData.GetPassword(),21,TRUE);
		if (ftp_connection == NULL)
		{  
			DWORD err = GetLastError();
			char errmsg[1024];
			if (FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM|FORMAT_MESSAGE_MAX_WIDTH_MASK,0,err,0x0409,errmsg,1024,NULL)==0)
				MessageBox("FAILED","Error");
			else
				MessageBox(errmsg,"Error");
			return;
		}

		CWaitCursor olWait;
		CString CurrentDirectory;
		ftp_connection->GetCurrentDirectory(CurrentDirectory);
		CurrentDirectory += ogBasicData.GetServerPath();

		FillFiles(CurrentDirectory);
	}
	catch(CInternetException *e)
	{
		e->ReportError();
		ogBasicData.SetUserName("");

	}
}

void CCflReadyWithRuleDiagnosticsDlg::FillFiles(const CString& path) 
{
	// GATHER ALL THE LOGILES 

	CString		dmyFile;
	CString		fileDate;


	CFtpFileFind finder(ftp_connection);

	ftp_connection->SetCurrentDirectory(path);

	BOOL bWorking = finder.FindFile(ogBasicData.GetFileFilter());
	int i=0;

	this->m_RuleFiles.ResetContent();


	while (bWorking)
	{
	   CString file;
       bWorking = finder.FindNextFile();
	   file = finder.GetFileName();
	   
	   this->m_RuleFiles.AddString(file);
	}

	finder.Close();

}

void CCflReadyWithRuleDiagnosticsDlg::LoadRuleFile(const CString& procName) 
{
	try
	{
		CInternetFile *polFile = this->ftp_connection->OpenFile(procName,GENERIC_READ,FTP_TRANSFER_TYPE_ASCII,1);
		if (polFile != NULL)
		{
			CString olBuffer;
			char pclBuffer[513];
			UINT nSize = polFile->Read(pclBuffer,sizeof(pclBuffer)-1);
			while (nSize >= 0)
			{
				pclBuffer[nSize] = '\0';
				olBuffer += pclBuffer;
				if (nSize == sizeof(pclBuffer)-1)
					nSize = polFile->Read(pclBuffer,sizeof(pclBuffer)-1);
				else
					break;
			}

			polFile->Close();
			this->m_RuleResults.SetWindowText(olBuffer);
		}
	}
	catch(CInternetException *e)
	{
		e->ReportError();
	}

}


void CCflReadyWithRuleDiagnosticsDlg::OnOK() 
{
	// TODO: Add extra validation here
	ftp_connection->Close();
	
	CCflReadyDlg::OnOK();
}
