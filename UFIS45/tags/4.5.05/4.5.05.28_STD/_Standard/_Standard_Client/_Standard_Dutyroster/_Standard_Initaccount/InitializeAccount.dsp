# Microsoft Developer Studio Project File - Name="InitializeAccount" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=InitializeAccount - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "InitializeAccount.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "InitializeAccount.mak" CFG="InitializeAccount - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "InitializeAccount - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "InitializeAccount - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "InitializeAccount - Win32 Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "c:\Ufis_Bin\Release"
# PROP Intermediate_Dir "c:\Ufis_Intermediate\InitAccount\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I ".\\" /D "NDEBUG" /D "_DISABLE_SALDO" /D "_DISABLE_MKBIN_DLG" /D "_DISABLE_SALARY" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 c:\Ufis_Bin\Release\Ufis32.lib c:\Ufis_Bin\Classlib\Release\CCSClass.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "InitializeAccount - Win32 Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "c:\Ufis_Bin\Debug"
# PROP Intermediate_Dir "c:\Ufis_Intermediate\InitAccount\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I ".\\" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_DISABLE_MKBIN_DLG" /D "_DISABLE_SALDO" /D "_DISABLE_SALARY" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x407 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 c:\Ufis_Bin\Debug\Ufis32.lib c:\Ufis_Bin\Classlib\Debug\CCSClass.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "InitializeAccount - Win32 Release"
# Name "InitializeAccount - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Wrapper\aatlogin.cpp
# End Source File
# Begin Source File

SOURCE=.\basicdata.cpp
# End Source File
# Begin Source File

SOURCE=.\ccsglobl.cpp
# End Source File
# Begin Source File

SOURCE=.\CCSParam.cpp
# End Source File
# Begin Source File

SOURCE=.\cedacotdata.cpp
# End Source File
# Begin Source File

SOURCE=.\cedainitmodudata.cpp
# End Source File
# Begin Source File

SOURCE=.\cedascodata.cpp
# End Source File
# Begin Source File

SOURCE=.\cedasordata.cpp
# End Source File
# Begin Source File

SOURCE=.\cedastfdata.cpp
# End Source File
# Begin Source File

SOURCE=.\colorcontrols.cpp
# End Source File
# Begin Source File

SOURCE=.\EditOpenJobDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\gridcontrol.cpp
# End Source File
# Begin Source File

SOURCE=.\GUILng.cpp
# End Source File
# Begin Source File

SOURCE=.\InitializeAccount.cpp
# End Source File
# Begin Source File

SOURCE=.\InitializeAccount.rc
# End Source File
# Begin Source File

SOURCE=.\InitializeAccountDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\initialloaddlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ListBoxDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\logindlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MakeHelp.bat
# End Source File
# Begin Source File

SOURCE=.\OpenJobControlDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\privlist.cpp
# End Source File
# Begin Source File

SOURCE=.\registerdlg.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\udlgstatusbar.cpp
# End Source File
# Begin Source File

SOURCE=..\..\_Standard_Share\_Standard_Classlib\VersionInfo.cpp
# End Source File
# Begin Source File

SOURCE=.\WaitDlg.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Wrapper\aatlogin.h
# End Source File
# Begin Source File

SOURCE=.\basicdata.h
# End Source File
# Begin Source File

SOURCE=.\ccsglobl.h
# End Source File
# Begin Source File

SOURCE=.\CCSParam.h
# End Source File
# Begin Source File

SOURCE=.\cedacotdata.h
# End Source File
# Begin Source File

SOURCE=.\cedainitmodudata.h
# End Source File
# Begin Source File

SOURCE=.\cedascodata.h
# End Source File
# Begin Source File

SOURCE=.\cedasordata.h
# End Source File
# Begin Source File

SOURCE=.\cedastfdata.h
# End Source File
# Begin Source File

SOURCE=.\colorcontrols.h
# End Source File
# Begin Source File

SOURCE=.\EditOpenJobDialog.h
# End Source File
# Begin Source File

SOURCE=.\gridcontrol.h
# End Source File
# Begin Source File

SOURCE=.\GUILng.h
# End Source File
# Begin Source File

SOURCE=.\InitializeAccount.h
# End Source File
# Begin Source File

SOURCE=.\InitializeAccountDlg.h
# End Source File
# Begin Source File

SOURCE=.\initialloaddlg.h
# End Source File
# Begin Source File

SOURCE=.\ListBoxDlg.h
# End Source File
# Begin Source File

SOURCE=.\logindlg.h
# End Source File
# Begin Source File

SOURCE=.\OpenJobControlDialog.h
# End Source File
# Begin Source File

SOURCE=.\privlist.h
# End Source File
# Begin Source File

SOURCE=.\registerdlg.h
# End Source File
# Begin Source File

SOURCE=.\resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\udlgstatusbar.h
# End Source File
# Begin Source File

SOURCE=..\..\_Standard_Share\_Standard_Classlib\VersionInfo.h
# End Source File
# Begin Source File

SOURCE=.\WaitDlg.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\abb.bmp
# End Source File
# Begin Source File

SOURCE=.\res\appl.ico
# End Source File
# Begin Source File

SOURCE=.\res\Exclamat.bmp
# End Source File
# Begin Source File

SOURCE=.\res\InitializeAccount.ico
# End Source File
# Begin Source File

SOURCE=.\res\InitializeAccount.rc2
# End Source File
# Begin Source File

SOURCE=.\res\UfisLogin.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=.\hlp\InitializeAccount.log
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
