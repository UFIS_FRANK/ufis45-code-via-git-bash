#if !defined(AFX_EDITOPENJOBDIALOG_H__A1487F8F_D953_4339_9FD5_A328F07B86EE__INCLUDED_)
#define AFX_EDITOPENJOBDIALOG_H__A1487F8F_D953_4339_9FD5_A328F07B86EE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EditOpenJobDialog.h : Header-Datei
//

#include <InitializeAccountDlg.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CEditOpenJobDialog 

class CEditOpenJobDialog : public CDialog
{
// Konstruktion
public:
	CEditOpenJobDialog(CString opInfo, CWnd* pParent = NULL, 
					   bool bpEditMode = false);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(CEditOpenJobDialog)
	enum { IDD = IDD_DIALOG_EDIT_OPEN_JOB };
	CComboBox	m_cb_Mode;
	CButton	m_but_Ok;
	CStatic	m_stc_To;
	CStatic	m_stc_Time;
	CStatic	m_stc_Offset;
	CStatic	m_stc_NextExec;
	CStatic	m_stc_Mode;
	CStatic	m_stc_From;
	CStatic	m_stc_Day;
	CEdit	m_ed_To;
	CEdit	m_ed_Time;
	CEdit	m_ed_Offset;
	CEdit	m_ed_NextExec;
	CEdit	m_ed_From;
	CEdit	m_ed_Day;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CEditOpenJobDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:
// functions:
	// load language specific resource strings
	void LoadStringResources(void);
	
// data:
	// dialog execution mode: true = edit open job, false = just view open job
	bool bmEditMode;
	// the open jobs info string
	CString omInfo;

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CEditOpenJobDialog)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_EDITOPENJOBDIALOG_H__A1487F8F_D953_4339_9FD5_A328F07B86EE__INCLUDED_
