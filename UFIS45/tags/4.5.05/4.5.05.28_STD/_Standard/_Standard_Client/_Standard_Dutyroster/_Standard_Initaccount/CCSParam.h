#if !defined(AFX_TESTCLASS_H__82E49D4F_A176_11D3_8FBE_0050DA1CAD13__INCLUDED_)
#define AFX_TESTCLASS_H__82E49D4F_A176_11D3_8FBE_0050DA1CAD13__INCLUDED_

#include <basicdata.h>
#include <CedaBasicData.h>
#include <CCSCedaData.h>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
//
// this is the single item in the list of loaded items. Access it via pointer returned by GetParam()
//
struct CCSParamEntry
{	// parameters from PARTAB
	char appl[9];
	char pid[17];
	char ptyp[17];
	char txid[33];
	char type[5];
	char name[101];
	char value[65];
	char parurno[11];
	// parameters from VALTAB
	char freq[8];
	char timf[5];
	char timt[5];
	char vafr[15];
	char vato[15];
};

//
// wrapper class for database parameter support in the application
//
//
class CCSParam: public CCSCedaData
{
	private:
		int itemcnt;					// number of items in the array
		CCSParamEntry *items;			// item array
		// some position markers, only private access
		int POS_VURNO, POS_VAPPL, POS_VFREQ, POS_VTIMF, POS_VTIMT, POS_VVAFR, POS_VVATO, POS_VUVAL, POS_VTABN,
			POS_PURNO, POS_PAPPL, POS_PNAME, POS_PPAID, POS_PPTYP, POS_PTXID, POS_PTYPE, POS_PVALU;

		// Liste mit allen Nachrichten, die beim Laden angefallen sind.
		CStringList		omMessageList;

	protected:
		bool IsValidRecord(int idx, CString timestamp);

	public:
		// Construction and destruction
		CCSParam();
		virtual ~CCSParam();
		// access function
		int BufferParams(CString appname);

		CCSParamEntry*	GetParam	(CString appl, CString paramid, CString timestamp,
									 CString defaultvalue = "",CString name = "" ,CString paramtyp = "", CString txid = "", 
									 CString type = "TEXT", CString datefrom = "", CString dateto = "",
									 CString timefrom = "", CString timeto = "", CString freq = "1111111",
									 bool bpMakeNewDBEntry = false);

		// Liest nur den Wert des Parameters aus.
		CString GetParamValue(CString appl, CString paramid, CString* ptimestamp=0);

		// Liste mit allen Nachrichten, die beim Laden angefallen sind zur�ckgeben.
		CStringList*	GetMessageList() {return &omMessageList;}
};

// this is a global !!
extern CCSParam ogCCSParam;

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TESTCLASS_H__82E49D4F_A176_11D3_8FBE_0050DA1CAD13__INCLUDED_)
