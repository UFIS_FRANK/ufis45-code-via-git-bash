VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.ocx"
Object = "{A2F31E92-C74F-11D3-A251-00500437F607}#1.0#0"; "UfisCom.ocx"
Begin VB.Form frmMainCleaner 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ROSTERING  CLEANER 3.3 (READONLY VERSION)"
   ClientHeight    =   10485
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   13830
   Icon            =   "frmMainCleaner.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10485
   ScaleWidth      =   13830
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdReset 
      Caption         =   "Reset SAP flag for the displayed shifts"
      Enabled         =   0   'False
      Height          =   495
      Left            =   6120
      TabIndex        =   43
      Top             =   2160
      Width           =   1815
   End
   Begin VB.Frame Frame3 
      Caption         =   "Date Format"
      Height          =   855
      Left            =   11040
      TabIndex        =   40
      Top             =   240
      Visible         =   0   'False
      Width           =   2535
      Begin VB.OptionButton optDEU 
         Caption         =   "01.12.2004"
         Height          =   255
         Left            =   240
         TabIndex        =   42
         Top             =   480
         Width           =   2175
      End
      Begin VB.OptionButton optUS 
         Caption         =   "12/01/2004"
         Height          =   255
         Left            =   240
         TabIndex        =   41
         Top             =   240
         Value           =   -1  'True
         Width           =   2055
      End
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "Delete"
      Height          =   345
      Left            =   9000
      TabIndex        =   35
      Top             =   0
      Visible         =   0   'False
      Width           =   1500
   End
   Begin VB.CommandButton cmdCheck 
      Caption         =   "Check/Mark"
      Height          =   345
      Left            =   7920
      TabIndex        =   34
      Top             =   0
      Visible         =   0   'False
      Width           =   1500
   End
   Begin VB.CommandButton cmdLoad 
      Caption         =   "Load ALL !"
      Enabled         =   0   'False
      Height          =   345
      Left            =   840
      TabIndex        =   33
      Top             =   840
      Width           =   2340
   End
   Begin VB.CommandButton cmdLevel1 
      Caption         =   "without Level 1"
      Enabled         =   0   'False
      Height          =   465
      Left            =   11160
      TabIndex        =   32
      Top             =   1200
      Width           =   1980
   End
   Begin VB.CommandButton cmdDelSel 
      Caption         =   "Delete Selected"
      Enabled         =   0   'False
      Height          =   495
      Left            =   4200
      TabIndex        =   31
      Top             =   2160
      Width           =   1815
   End
   Begin VB.CommandButton cmdMark 
      Caption         =   "Check and Mark for deletion"
      Height          =   495
      Left            =   9360
      TabIndex        =   30
      Top             =   1680
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton cmdLoadRosterStaff 
      Caption         =   "2b) Load all Shift data for select staff"
      Height          =   495
      Left            =   4800
      TabIndex        =   28
      Top             =   0
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton cmdRosterORG 
      Caption         =   "2) Load all Shift data for this ORG unit"
      Height          =   495
      Left            =   2280
      TabIndex        =   27
      Top             =   2160
      Width           =   1815
   End
   Begin VB.CommandButton cmdORGLIST 
      Caption         =   "1) Load Org Units"
      Height          =   375
      Left            =   240
      TabIndex        =   22
      Top             =   2160
      Width           =   1455
   End
   Begin VB.CommandButton cmdOrg 
      Caption         =   "2) Load Staff"
      Height          =   495
      Left            =   1920
      TabIndex        =   20
      Top             =   1680
      Visible         =   0   'False
      Width           =   1215
   End
   Begin UFISCOMLib.UfisCom UfisCom 
      Left            =   6705
      Top             =   900
      _Version        =   65536
      _ExtentX        =   1164
      _ExtentY        =   1032
      _StockProps     =   0
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   18
      Top             =   10155
      Width           =   13830
      _ExtentX        =   24395
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   21105
            MinWidth        =   15522
            Text            =   "Ready."
            TextSave        =   "Ready."
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Object.Width           =   1764
            MinWidth        =   1764
            TextSave        =   "20.02.2006"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Object.Width           =   1411
            MinWidth        =   1411
            TextSave        =   "11:27"
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame4 
      Height          =   765
      Left            =   6960
      TabIndex        =   15
      Top             =   1035
      Width           =   3750
      Begin VB.Label lblRecordsMarked 
         Height          =   195
         Left            =   135
         TabIndex        =   17
         Top             =   465
         Width           =   3465
      End
      Begin VB.Label lblRecordsLoaded 
         Height          =   195
         Left            =   135
         TabIndex        =   16
         Top             =   165
         Width           =   3465
      End
   End
   Begin VB.Frame Frame2 
      Height          =   765
      Left            =   6960
      TabIndex        =   12
      Top             =   150
      Width           =   3750
      Begin VB.TextBox txtHopo 
         Enabled         =   0   'False
         Height          =   330
         Left            =   2565
         TabIndex        =   6
         Text            =   "SIN"
         Top             =   240
         Width           =   945
      End
      Begin VB.TextBox txtServer 
         Enabled         =   0   'False
         Height          =   330
         Left            =   825
         TabIndex        =   5
         Text            =   "sin1"
         Top             =   225
         Width           =   945
      End
      Begin VB.Label Label6 
         Caption         =   "HOPO:"
         Height          =   195
         Left            =   1905
         TabIndex        =   14
         Top             =   315
         Width           =   630
      End
      Begin VB.Label Label5 
         Caption         =   "Server:"
         Height          =   195
         Left            =   165
         TabIndex        =   13
         Top             =   300
         Width           =   630
      End
   End
   Begin TABLib.TAB TabDRR 
      Height          =   4815
      Left            =   2280
      TabIndex        =   0
      Tag             =   $"frmMainCleaner.frx":030A
      Top             =   5160
      Width           =   11295
      _Version        =   65536
      _ExtentX        =   19923
      _ExtentY        =   8493
      _StockProps     =   64
   End
   Begin TABLib.TAB TABSOR 
      Height          =   1455
      Left            =   10440
      TabIndex        =   19
      Tag             =   "{=TABLE=}SORTAB{=FIELDS=}CODE,SURN,VPFR,VPTO"
      Top             =   2760
      Width           =   3135
      _Version        =   65536
      _ExtentX        =   5530
      _ExtentY        =   2566
      _StockProps     =   64
   End
   Begin TABLib.TAB TABORG 
      Height          =   6975
      Left            =   240
      TabIndex        =   21
      Tag             =   "{=TABLE=}ORGTAB{=FIELDS=}DPT1"
      Top             =   2640
      Width           =   1455
      _Version        =   65536
      _ExtentX        =   2566
      _ExtentY        =   12303
      _StockProps     =   64
   End
   Begin VB.Frame Frame5 
      Height          =   8175
      Left            =   120
      TabIndex        =   23
      Top             =   1920
      Width           =   1815
      Begin VB.Label lblOrg 
         Caption         =   "Press load .."
         Height          =   255
         Left            =   240
         TabIndex        =   24
         Top             =   7800
         Width           =   1215
      End
   End
   Begin TABLib.TAB TABSTF 
      Height          =   2055
      Left            =   2280
      TabIndex        =   25
      Tag             =   "{=TABLE=}STFTAB{=FIELDS=}PENO,URNO,LANM,FINM"
      Top             =   2760
      Width           =   8055
      _Version        =   65536
      _ExtentX        =   14208
      _ExtentY        =   3625
      _StockProps     =   64
   End
   Begin VB.Frame Frame6 
      Height          =   3015
      Left            =   2160
      TabIndex        =   26
      Top             =   1920
      Width           =   11535
      Begin VB.Label lblTotal 
         Caption         =   "Affected Staff: 0"
         Height          =   255
         Left            =   6000
         TabIndex        =   39
         Top             =   480
         Width           =   2415
      End
      Begin VB.Label lblSTAFF 
         Caption         =   "Selected Staff: NONE"
         Height          =   255
         Left            =   8280
         TabIndex        =   38
         Top             =   2520
         Width           =   3135
      End
      Begin VB.Label lblNumber 
         Caption         =   "Number of Staff: 0"
         Height          =   255
         Left            =   6000
         TabIndex        =   37
         Top             =   240
         Width           =   2415
      End
   End
   Begin VB.Frame Frame7 
      Height          =   5055
      Left            =   2160
      TabIndex        =   29
      Top             =   5040
      Width           =   11535
   End
   Begin VB.Frame Frame1 
      Height          =   1665
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   6705
      Begin VB.CommandButton cmdIncreaseDate 
         Caption         =   "Increase dates"
         Height          =   345
         Left            =   3450
         TabIndex        =   3
         Top             =   270
         Visible         =   0   'False
         Width           =   1500
      End
      Begin VB.TextBox txtDays 
         Height          =   330
         Left            =   5715
         TabIndex        =   4
         Text            =   "1"
         Top             =   270
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.TextBox txtFrom 
         Height          =   330
         Left            =   720
         TabIndex        =   1
         Text            =   "12/01/2004"
         Top             =   270
         Width           =   945
      End
      Begin VB.TextBox txtTo 
         Height          =   330
         Left            =   2115
         TabIndex        =   2
         Text            =   "12/10/2004"
         Top             =   270
         Width           =   945
      End
      Begin VB.Label Label8 
         Caption         =   "ALL..loads all roster data for the selected time frame !!!"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000C000&
         Height          =   255
         Left            =   600
         TabIndex        =   36
         Top             =   1200
         Width           =   5535
      End
      Begin VB.Label Label4 
         Caption         =   "days"
         Height          =   195
         Left            =   6105
         TabIndex        =   11
         Top             =   345
         Visible         =   0   'False
         Width           =   435
      End
      Begin VB.Label Label3 
         Caption         =   "... by ..."
         Height          =   195
         Left            =   5070
         TabIndex        =   10
         Top             =   345
         Visible         =   0   'False
         Width           =   630
      End
      Begin VB.Label Label1 
         Caption         =   "From"
         Height          =   195
         Left            =   120
         TabIndex        =   9
         Top             =   338
         Width           =   435
      End
      Begin VB.Label Label2 
         Caption         =   "to"
         Height          =   195
         Left            =   1785
         TabIndex        =   8
         Top             =   338
         Width           =   255
      End
   End
   Begin VB.Label Label7 
      Caption         =   "This tool cleans up duplicate roster records. To be used under supervision by a admin !"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1095
      Left            =   10680
      TabIndex        =   44
      Top             =   240
      Visible         =   0   'False
      Width           =   2775
   End
End
Attribute VB_Name = "frmMainCleaner"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim strHOPO As String
Dim strServer As String
Dim globURNOlist As String

Private Sub cmdCheck_Click()
    Dim l As Long
    Dim llLineCount As Long
    Dim llCountMarked As Long
    Dim strKey1 As String
    Dim strUrno1 As String
    Dim strKey2 As String
    Dim strUrno2 As String

    llCountMarked = 0
    llLineCount = TabDRR.GetLineCount - 2

    For l = 0 To llLineCount Step 1
        strKey1 = TabDRR.GetFieldValues(l, "STFU,SDAY,ROSL,DRRN")
        strKey2 = TabDRR.GetFieldValues(l + 1, "STFU,SDAY,ROSL,DRRN")
        If strKey1 = strKey2 Then
            ' we've found a duplicated record in that level!
            ' mark the one with the lower URNO-value for deletion
            llCountMarked = llCountMarked + 1
            If TabDRR.GetFieldValue(l, "URNO") < TabDRR.GetFieldValue(l + 1, "URNO") Then
                TabDRR.SetLineColor l, vbBlack, vbRed
            Else
                TabDRR.SetLineColor l + 1, vbBlack, vbRed
            End If
        End If
    Next l
    TabDRR.RedrawTab
    lblRecordsMarked.Caption = CStr(llCountMarked) & " records marked for deletion."
End Sub

Private Sub cmdDelete_Click()
    Dim l As Long
    Dim llLine As Long
    Dim strDrrUrnos As String
    Dim strWhere As String

    TabDRR.SetInternalLineBuffer True
    TabDRR.GetLinesByBackColor vbRed

    l = 0
    llLine = TabDRR.GetNextResultLine
    If llLine < 0 Then
        MsgBox "No wrong records found! Try another date/timeframe!", vbInformation
        Exit Sub
    End If

    If MsgBox("Are you sure to delete the marked records?", vbYesNo, "Delete?") = vbNo Then
        Exit Sub
    End If

    MousePointer = vbHourglass
    
    ConnectUfisCom

    Dim FileNumber
    Dim strFileName As String
    
    Dim tmpFROM As String
    Dim tmpTO As String
    
    tmpFROM = Format(txtFrom.Text, "YYYYMMDDhhmmss")
    tmpTO = Format(txtTo.Text, "YYYYMMDDhhmmss")
    
    strFileName = "c:\tmp\DRR_" & tmpFROM & "_" & tmpTO & "_" & Format(Now, "YYYYMMDDhhmmss") & ".txt"
    FileNumber = FreeFile   ' Get unused file number.
'    MsgBox CStr(strFileName)
    Open strFileName For Output As #FileNumber   ' Create file name.

    While llLine > -1
        WriteLineToFile llLine, FileNumber
        l = l + 1
        If strDrrUrnos <> "" Then strDrrUrnos = strDrrUrnos & ","
        strDrrUrnos = strDrrUrnos & "'" & TabDRR.GetFieldValue(llLine, "URNO") & "'"
        If l Mod 900 = 0 Then
            strWhere = "WHERE URNO IN (" & strDrrUrnos & ")"
            CallServer "DRT", "DRRTAB", strWhere
            strDrrUrnos = ""
        End If
        llLine = TabDRR.GetNextResultLine
    Wend
    If strDrrUrnos <> "" Then
        strWhere = "WHERE URNO IN (" & strDrrUrnos & ")"
        CallServer "DRT", "DRRTAB", strWhere
    End If

    Close #FileNumber   ' Close file.
    TabDRR.SetInternalLineBuffer False
    DisconnetUfisCom
    
    MousePointer = vbDefault
    
    
End Sub

Private Sub CallServer(ByRef sCmd As String, ByRef sObject As String, ByRef sWhere As String)
    Dim strFields As String
    Dim strData As String
    strFields = ""
    strData = ""
    StatusBar1.Panels(1).Text = "Running DRT command..."
    MousePointer = vbHourglass
    If UfisCom.CallServer(sCmd, sObject, strFields, strData, sWhere, "360") <> 0 Then
        MsgBox "Error deleting DRR records: " & vbCrLf & UfisCom.LastErrorMessage, vbCritical
    End If
    StatusBar1.Panels(1).Text = "Ready."
    MousePointer = vbDefault
End Sub

Private Sub CallServerUPDATE(ByRef sCmd As String, ByRef sObject As String, ByRef sWhere As String)
    Dim strFields As String
    Dim strData As String
    strFields = "PRFL"
    strData = "1"
    StatusBar1.Panels(1).Text = "Updating SAP flag..."
    MousePointer = vbHourglass
    If UfisCom.CallServer(sCmd, sObject, strFields, strData, sWhere, "360") <> 0 Then
        MsgBox "Error updating DRR records: " & vbCrLf & UfisCom.LastErrorMessage, vbCritical
    End If
    StatusBar1.Panels(1).Text = "Ready."
    MousePointer = vbDefault
End Sub



Private Sub cmdDelSel_Click()
cmdDelete_Click
End Sub

Private Sub cmdIncreaseDate_Click()
    Dim olTmpDate As Date

    If IsDate(txtFrom.Text) = True Then
        olTmpDate = txtFrom.Text
        olTmpDate = DateAdd("d", 1, olTmpDate)
        txtFrom.Text = Format(olTmpDate, "dd.mm.yyyy")
        'If optUS.Value = False Then
        '    txtFrom.Text = Format(olTmpDate, "dd.mm.yyyy")
        'Else
        '    txtFrom.Text = Format(olTmpDate, "mm/dd/yyyy")
        'End If
        
        
    Else
        MsgBox "No valid from-date!", vbCritical
    End If
    
    If IsDate(txtTo.Text) = True Then
        olTmpDate = txtTo.Text
        olTmpDate = DateAdd("d", 1, olTmpDate)
        txtTo.Text = Format(olTmpDate, "dd.mm.yyyy")
        'If optUS.Value = False Then
        '    txtTo.Text = Format(olTmpDate, "dd.mm.yyyy")
        'Else
        '    txtTo.Text = Format(olTmpDate, "mm/dd/yyyy")
        'End If

    Else
        MsgBox "No valid from-date!", vbCritical
    End If
End Sub

Private Sub cmdLevel1_Click()
    frmEmployees.Show vbModal
End Sub

Private Sub cmdLoad_Click()
    LoadRoster "", 0
    loadAllStaff
End Sub

Sub LoadRoster(STFlist As String, mode As Integer)
    Dim strWhere As String
    
    Dim olDateFrom As Date
    Dim olDateTo As Date
    
    If IsDate(txtFrom.Text) = True Then
        olDateFrom = txtFrom.Text
    Else
        MsgBox "No valid from-date!", vbCritical
        Exit Sub
    End If
    
    If IsDate(txtTo.Text) = True Then
        olDateTo = txtTo.Text
    Else
        MsgBox "No valid from-date!", vbCritical
        Exit Sub
    End If

    MousePointer = vbHourglass

    InitTabGeneral TabDRR
    InitTabForCedaConnection TabDRR

    If olDateFrom = olDateTo Then
        strWhere = "WHERE SDAY = '" & Format(olDateFrom, "YYYYMMDD") & "'"
    Else
        strWhere = "WHERE SDAY BETWEEN '" & Format(olDateFrom, "YYYYMMDD") & "' AND '" & _
            Format(olDateTo, "YYYYMMDD") & "'"
    End If
    
    If STFlist <> "" Then
        If mode = 1 Then
            strWhere = strWhere & " and stfu in (" & STFlist & ") "
        ElseIf mode = 2 Then
            strWhere = strWhere & " and stfu='" & STFlist & "' "
        End If
        
    End If
    'strWhere = strWhere & " ORDER BY STFU,SDAY,ROSL,DRRN,URNO"
    strWhere = strWhere & " ORDER BY STFU,SDAY,ROSL,DRRN,LSTU DESC,CDAT DESC"

    StatusBar1.Panels(1).Text = "Please wait while loading DRR data ..."
    LoadData TabDRR, strWhere
    StatusBar1.Panels(1).Text = "Ready."

    
    
    TabDRR.AutoSizeColumns

    lblRecordsLoaded.Caption = CStr(TabDRR.GetLineCount) & " records loaded from DRRTAB."
    lblRecordsMarked.Caption = ""

    MousePointer = vbDefault
End Sub

Public Sub InitTabGeneral(ByRef rTab As TABLib.TAB)
    rTab.ResetContent
    rTab.FontName = "Courier New"
    rTab.HeaderFontSize = "18"
    rTab.FontSize = "16"
    rTab.EnableHeaderSizing True
    rTab.ShowHorzScroller True
    rTab.AutoSizeByHeader = True
    rTab.SetFieldSeparator ","

    Dim ilCnt As Integer
    Dim i As Integer
    Dim strFields As String
    Dim strHeaderLengthString As String
    GetKeyItem strFields, rTab.Tag, "{=FIELDS=}", "{="
    ilCnt = ItemCount(strFields, ",")
    For i = 1 To ilCnt Step 1
        strHeaderLengthString = strHeaderLengthString & "10,"
    Next i
    strHeaderLengthString = Left(strHeaderLengthString, Len(strHeaderLengthString) - 1)
    rTab.HeaderLengthString = strHeaderLengthString

    rTab.HeaderString = strFields
    rTab.LogicalFieldList = strFields

    rTab.EnableHeaderSizing True

    ' set style of highlighted line
    Dim strColors As String
    strColors = CStr(vbBlue) & "," & CStr(vbBlue)
    rTab.CursorDecoration rTab.LogicalFieldList, "B,T", "3,3", strColors
    rTab.DefaultCursor = False
End Sub

Public Sub InitTabForCedaConnection(ByRef rTab As TABLib.TAB)
    'do the initializing for the CEDA-connection
    rTab.CedaServerName = txtServer.Text
    rTab.CedaPort = "3357"
    rTab.CedaHopo = txtHopo.Text
    rTab.CedaCurrentApplication = "DRR cleaner"
    rTab.CedaTabext = "TAB"
    rTab.CedaUser = GetWindowsUserName
    rTab.CedaWorkstation = GetWorkstationName
    rTab.CedaSendTimeout = "120"
    rTab.CedaReceiveTimeout = "240"
    rTab.CedaRecordSeparator = Chr(10)
    rTab.CedaIdentifier = "DRR cleaner"
End Sub



Private Sub cmdLoadRosterStaff_Click()
    
    Dim strKey1 As String
    Dim l As Long
    
    l = TABSTF.GetCurrentSelected
    If l <> -1 Then
        strKey1 = TABSTF.GetFieldValues(l, "URNO")
        'MsgBox CStr(strKey1)
    Else
        MsgBox "NO STAFF SELECTED!"
        Exit Sub
    End If
  
    LoadRoster strKey1, 2
    
End Sub



Private Sub cmdMark_Click()
    
    Dim l As Long
    Dim llLineCount As Long
    Dim llCountMarked As Long
    Dim strKey1 As String
    Dim strUrno1 As String
    Dim strKey2 As String
    Dim strUrno2 As String

    If TabDRR.GetLineCount = 0 Then
        MsgBox "NO ROSTER DATA LOADED."
        Exit Sub
    End If

    MousePointer = vbHourglass
    
    llCountMarked = 0
    llLineCount = TabDRR.GetLineCount - 2


    For l = 0 To llLineCount Step 1
        strKey1 = TabDRR.GetFieldValues(l, "STFU,SDAY,ROSL,DRRN")
        strKey2 = TabDRR.GetFieldValues(l + 1, "STFU,SDAY,ROSL,DRRN")
        
        If strKey1 = strKey2 Then
            llCountMarked = llCountMarked + 1
            TabDRR.SetLineColor l + 1, vbBlack, vbRed
        End If
    Next l
    TabDRR.RedrawTab
    lblRecordsMarked.Caption = CStr(llCountMarked) & " records marked for deletion."

    MousePointer = vbDefault
        
    markStaff
    
End Sub
Sub markStaff()

    Dim l As Long
    Dim llLine As Long
    Dim strDrrUrnos As String
    Dim strWhere As String
    
    MousePointer = vbHourglass

    TabDRR.SetInternalLineBuffer True
    TabDRR.GetLinesByBackColor vbRed

    l = 0
    llLine = TabDRR.GetNextResultLine
    If llLine < 0 Then
        'MsgBox "No wrong records found! Try another date/timeframe!", vbInformation
        StatusBar1.Panels(1).Text = "No wrong records found! Try another date/timeframe!."
        MousePointer = vbDefault
        Exit Sub
    End If
    
    Dim tmpSTFU As String
    Dim l2 As Long
    Dim strKey1 As String
    Dim llLineCount As Long
    Dim total As Long
    
    While llLine > -1
        
        tmpSTFU = TabDRR.GetFieldValues(llLine, "STFU")
        
        llLineCount = TABSTF.GetLineCount


        For l2 = 0 To llLineCount Step 1
            strKey1 = TABSTF.GetFieldValues(l2, "URNO")
        
            If strKey1 = tmpSTFU Then
                TABSTF.SetLineColor l2, vbBlack, vbRed
                MousePointer = vbDefault
                total = total + 1
                Exit For
            End If
        Next l2
        lblTotal.Caption = "Affected Staff: " & total
        
        llLine = TabDRR.GetNextResultLine
    Wend


   MousePointer = vbDefault

End Sub
Private Sub cmdOrg_Click()
    
    Dim strWhere As String
    InitTabGeneral TABSOR
    InitTabForCedaConnection TABSOR

    Dim olDateFrom As Date
    Dim olDateTo As Date
    
    If IsDate(txtFrom.Text) = True Then
        olDateFrom = txtFrom.Text
    Else
        MsgBox "No valid from-date!", vbCritical
        Exit Sub
    End If
    
    If IsDate(txtTo.Text) = True Then
        olDateTo = txtTo.Text
    Else
        MsgBox "No valid from-date!", vbCritical
        Exit Sub
    End If

    InitTabGeneral TabDRR

    Dim strKey1 As String
    Dim l As Long
    
    l = TABORG.GetCurrentSelected
    If l <> -1 Then
        strKey1 = TABORG.GetFieldValues(l, "DPT1")
        'MsgBox CStr(strKey1)
    Else
        MsgBox "NO ORG UNIT SELECTED!"
        Exit Sub
    End If



    'strWhere = "select surn,urno,code,lpad(vpfr,8),lpad(vpto,8) from sortab where surn='119346551'"
    'strWhere = "where surn='119346551'"
    strWhere = "where code='" & strKey1 & "'"
    'strWhere = strWhere & "and vpfr <='" & Format(olDateFrom, "YYYYMMDD") & "' and vpto >='" & Format(olDateFrom, "YYYYMMDD") & "'"
    
    'strWhere = strWhere & " ORDER BY STFU,SDAY,DRRN,URNO"

    StatusBar1.Panels(1).Text = "Please wait while loading SOR data ..."
    LoadData TABSOR, strWhere
    StatusBar1.Panels(1).Text = "Ready."

    TABSOR.AutoSizeColumns

    'lblRecordsLoaded.Caption = CStr(TabDRR.GetLineCount) & " records loaded from DRRTAB."
    'lblRecordsMarked.Caption = ""
    
    
    LoadStaff
    
End Sub

Private Sub cmdORGLIST_Click()
    Dim strWhere As String
    
    MousePointer = vbHourglass
    
    InitTabGeneral TABORG
    InitTabForCedaConnection TABORG

    strWhere = "where urno>'0' order by dpt1"

    StatusBar1.Panels(1).Text = "Please wait while loading ORG data ..."
    LoadData TABORG, strWhere
    StatusBar1.Panels(1).Text = "Ready."

    TABORG.AutoSizeColumns

    lblOrg.Caption = TABORG.GetLineCount & " ORG Units."
    
    MousePointer = vbDefault
    'cmdORGLIST.Enabled = False
End Sub
Sub loadAllStaff()

    Dim strWhere As String
    
    MousePointer = vbHourglass
 
  
    InitTabGeneral TABSTF
    InitTabForCedaConnection TABSTF

    strWhere = "where urno >'0' order by peno"

    StatusBar1.Panels(1).Text = "Please wait while loading STAFF data ..."
    LoadData TABSTF, strWhere
    StatusBar1.Panels(1).Text = "Ready."

    TABSTF.AutoSizeColumns
    
    MsgBox CStr(TABSTF.GetLineCount)
 
    MousePointer = vbDefault

End Sub
Sub LoadStaff()
    Dim strWhere As String
    
    MousePointer = vbHourglass
    
    
    '----------------------------------------------------------------------
    ' COLLECT STF URNOS FROM SORTAB WITH SELECTED ORGUNIT
    
    Dim l As Long
    Dim llLineCount As Long
    Dim llCountMarked As Long
    Dim strKey1 As String
    Dim strUrno1 As String
    Dim strKey2 As String
    Dim strUrno2 As String
    Dim INlist As String

    llCountMarked = 0
    llLineCount = TABSOR.GetLineCount

    For l = 0 To llLineCount Step 1
        strKey1 = TABSOR.GetFieldValues(l, "SURN")
        
        INlist = INlist & strKey1
        
        If l < llLineCount - 1 Then
            INlist = INlist & ","
        End If
    Next l
    
    globURNOlist = INlist
    
    '----------------------------------------------------------------------
    ' SHOW STUFF FOR VERIFICATION

    
    InitTabGeneral TABSTF
    InitTabForCedaConnection TABSTF

    strWhere = "where urno in (" & INlist & ") order by peno"

    StatusBar1.Panels(1).Text = "Please wait while loading STAFF data ..."
    LoadData TABSTF, strWhere
    StatusBar1.Panels(1).Text = "Ready."

    TABSTF.AutoSizeColumns
    
    lblNumber.Caption = "Number of Staff: " & TABSTF.GetLineCount
    
    MousePointer = vbDefault

End Sub

Private Sub cmdReset_Click()

    Dim l, l2, l3 As Long
    Dim llLine As Long
    Dim llLineCount As Long
    Dim strKey1 As String
    Dim strWhere As String
    Dim strDrrUrnos As String
    cmdMark_Click
    
    l = TabDRR.GetLineCount
    If l = -1 Then
        MsgBox "NO ROSTER DATA LOADED", vbExclamation
        Exit Sub
    End If
    
    
    TabDRR.SetInternalLineBuffer True
    TabDRR.GetLinesByBackColor vbRed

    l = 0
    llLine = TabDRR.GetNextResultLine
    If llLine < 0 Then
        'No wrong records found! can reset !
        
        Dim FileNumber
        Dim strFileName As String
        Dim tmpFROM As String
        Dim tmpTO As String
    
        tmpFROM = Format(txtFrom.Text, "YYYYMMDD")
        tmpTO = Format(txtTo.Text, "YYYYMMDD")
        
        strFileName = "c:\tmp\RESSAP_" & tmpFROM & "_" & tmpTO & "_" & Format(Now, "YYYYMMDDhhmmss") & ".sql"
        FileNumber = FreeFile   ' Get unused file number.

        Open strFileName For Output As #FileNumber   ' Create file name.
        
        
        llLineCount = TabDRR.GetLineCount


        For l2 = 0 To llLineCount Step 1
            strKey1 = TabDRR.GetFieldValues(l2, "ROSL")
        
            If strKey1 = "1" Then
              
               strDrrUrnos = "UPDATE DRRTAB SET PRFL='1' WHERE URNO='" & TabDRR.GetFieldValue(l2, "URNO") & "';"
               Print #FileNumber, strDrrUrnos
               Print #FileNumber, "commit;"
               
            End If
        Next l2
        
        strDrrUrnos = ""
        Close #FileNumber   ' Close file.
        
        Else
            MsgBox "ROSTER NOT CLEAN ! CLEAN UP FIRST !", vbExclamation
    End If
    
End Sub

Private Sub cmdRosterORG_Click()
    Dim l As Integer
    
    l = TABORG.GetCurrentSelected
    If l = -1 Then
        MsgBox "NO ORG UNIT SELECTED!"
        Exit Sub
    End If
    
    LoadRoster globURNOlist, 1

    cmdMark_Click

End Sub

Private Sub Form_Load()
    strHOPO = GetIniEntry("", "ROSTERING", "GLOBAL", "HOMEAIRPORT", "XXX")
    strServer = GetIniEntry("", "ROSTERING", "GLOBAL", "HOSTNAME", "XXX")
    txtHopo.Text = strHOPO
    txtServer.Text = strServer
    InitTabGeneral TabDRR
    TabDRR.AutoSizeByHeader = True
    TabDRR.AutoSizeColumns
    
    InitTabGeneral TABSOR
    TABSOR.AutoSizeByHeader = True
    TABSOR.AutoSizeColumns
    
    InitTabGeneral TABORG
    TABORG.AutoSizeByHeader = True
    TABORG.AutoSizeColumns
    
    InitTabGeneral TABSTF
    TABSTF.AutoSizeByHeader = True
    TABSTF.AutoSizeColumns

    
End Sub

Public Sub LoadData(ByRef rTab As TABLib.TAB, ByRef pWhere As String)
    Dim strCommand As String
    Dim strTable As String
    Dim strFieldList As String
    Dim strData As String
    Dim strWhere As String

    MousePointer = vbHourglass
    strCommand = "RT"
    rTab.ResetContent

    GetKeyItem strTable, rTab.Tag, "{=TABLE=}", "{="
    GetKeyItem strFieldList, rTab.Tag, "{=FIELDS=}", "{="
    strData = ""
    strWhere = pWhere
    If Len(strWhere) = 0 Then
        GetKeyItem strWhere, rTab.Tag, "{=WHERE=}", "{="
    End If
    If rTab.CedaAction(strCommand, strTable, strFieldList, strData, strWhere) = False Then
        MsgBox "Error loading " & strTable & "!" & vbCrLf & _
            "Field list: " & strFieldList & vbCrLf & _
            "Where statement: " & strWhere, vbCritical, "Error reading data (RT)"
            rTab.GetLastCedaError
    End If
    MousePointer = vbDefault
End Sub

Private Sub ConnectUfisCom()
    UfisCom.Module = "DRR Cleaner"
    UfisCom.HomeAirport = txtHopo.Text
    UfisCom.ServerName = txtServer.Text
    UfisCom.TableExt = "TAB"
    UfisCom.UserName = GetWindowsUserName
    UfisCom.WorkStation = GetWorkstationName
    UfisCom.SetCedaPerameters GetWindowsUserName, txtHopo.Text, "TAB"
    UfisCom.InitCom txtServer.Text, "CEDA"
End Sub

Private Sub DisconnetUfisCom()
    UfisCom.CleanupCom
End Sub

Private Sub Form_Resize()
    If Me.Width > 100 Then
        'TabDRR.Width = Me.Width - 1800
    End If
End Sub

Private Sub TabDRR_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim strMARK As String
    Dim l As Long
    l = TabDRR.GetCurrentSelected
    If l <> -1 Then
        strMARK = TabDRR.GetFieldValues(l, "STFU")
        'MsgBox CStr(strMARK)
    Else
        MsgBox "ERROR."
        Exit Sub
    End If

    Dim llLineCount As Long
    Dim llCountMarked As Long
    Dim strKey1 As String
    Dim strUrno1 As String
    Dim strKey2 As String
    Dim strUrno2 As String

    If TABSTF.GetLineCount = 0 Then
        MsgBox "NO STAFF DATA LOADED."
        Exit Sub
    End If

    llCountMarked = 0
    llLineCount = TABSTF.GetLineCount


    For l = 0 To llLineCount Step 1
        strKey1 = TABSTF.GetFieldValues(l, "URNO")
        lblSTAFF.Caption = "Selected Staff: " & TABSTF.GetFieldValues(l, "PENO") & " " & TABSTF.GetFieldValues(l, "LANM")
        If strKey1 = strMARK Then
            Exit Sub
        End If
    Next l
    

End Sub

Private Sub TabDRR_SendRButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim llTextColor As Long
    Dim llBackColor As Long
    TabDRR.GetLineColor LineNo, llTextColor, llBackColor
    If llBackColor = vbRed Then
        TabDRR.SetLineColor LineNo, vbBlack, vbWhite
    Else
        TabDRR.SetLineColor LineNo, vbBlack, vbRed
    End If
    TabDRR.RedrawTab
End Sub

Private Sub WriteLineToFile(ByRef lLine As Long, ByRef oFileNumber)
    Dim strFields As String
    Dim strData As String
    Dim strTmp As String

    GetKeyItem strFields, TabDRR.Tag, "{=FIELDS=}", "{="
    strTmp = TabDRR.GetLineValues(lLine)
    strData = "'" & Replace(strTmp, ",", "','") & "'"
    strTmp = strData
    strData = Replace(strTmp, "''", "' '")

    Print #oFileNumber, "INSERT INTO DRRTAB (" & strFields & ") " & "VALUES (" & strData & ");"
End Sub

Private Sub TABORG_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    cmdOrg_Click
End Sub

Private Sub TABSTF_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    cmdLoadRosterStaff_Click
    cmdMark_Click
End Sub
