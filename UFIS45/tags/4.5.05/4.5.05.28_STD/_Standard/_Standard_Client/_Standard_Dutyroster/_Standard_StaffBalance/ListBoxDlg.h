#if !defined(AFX_LISTBOXDLG_H__D63DD9EE_B164_11D3_AB39_604F4EC19A15__INCLUDED_)
#define AFX_LISTBOXDLG_H__D63DD9EE_B164_11D3_AB39_604F4EC19A15__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ListBoxDlg.h : Header-Datei
//
#include <resource.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CListBoxDlg 

class CListBoxDlg : public CDialog
{
// Konstruktion
public:
	CListBoxDlg(CStringList* popStringList, CString opCaption,CString opInfoText,CWnd* pParent = NULL);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(CListBoxDlg)
	enum { IDD = IDD_LISTBOX_DLG };
		// HINWEIS: Der Klassen-Assistent fügt hier Datenelemente ein
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CListBoxDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	CStringList*	pomStringList;
	CString			omCaption;
	CString			omInfoText;

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CListBoxDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_LISTBOXDLG_H__D63DD9EE_B164_11D3_AB39_604F4EC19A15__INCLUDED_
