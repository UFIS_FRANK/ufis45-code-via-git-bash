// CedaTdaData.cpp - Klasse f�r die Handhabung von Tda-Daten
 
#include <stdafx.h>
#include <afxwin.h>
#include <CCSCedaData.h>
#include <CedaTdaData.h>
#include <BasicData.h>
#include <ccsddx.h>
#include <UFIS.h>
#include <CCSBcHandle.h>
#include <CCSGlobl.h>

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Globale Funktionen
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// ProcessTdaCf: Callback-Funktion f�r den Broadcast-Handler und die Broadcasts
//	BC_Bud_CHANGE, BC_TDA_NEW und BC_TDA_DELETE. Ruft zur eingentlichen Bearbeitung
//	der Nachrichten die Funktion CedaTdaData::ProcessTdaBc() der entsprechenden 
//	Instanz von CedaTdaData auf.	
// R�ckgabe: keine
//************************************************************************************************************************************************

void  ProcessTdaCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	// Sanduhr einblenden, falls es l�nger dauert
	AfxGetApp()->DoWaitCursor(1);
	// Empf�nger ermitteln
	CedaTdaData *polTdaData = (CedaTdaData *)vpInstance;
	// Bearbeitungsfunktion des Empf�ngers aufrufen
	polTdaData->ProcessTdaBc(ipDDXType,vpDataPointer,ropInstanceName);
	// Sanduhr ausblenden
	AfxGetApp()->DoWaitCursor(-1);
}

//************************************************************************************************************************************************
//************************************************************************************************************************************************
// Implementation der Klasse CedaTdaData (Daily Roster Absences - untert�gige Abwesenheit)
//************************************************************************************************************************************************
//************************************************************************************************************************************************

//************************************************************************************************************************************************
// Konstruktor
//************************************************************************************************************************************************

CedaTdaData::CedaTdaData(CString opTableName, CString opExtName) : CCSCedaData(&ogCommHandler)
{
    // Infostruktur vom Typ CEDARECINFO f�r Tda-Datens�tze
	// Wird von Vaterklasse CCSCedaData f�r verschiedene Funktionen
	// (CCSCedaData::GetBufferRecord(), CCSCedaData::MakeCedaData())
	// benutzt. Die Datenstruktur beschreibt die Tabellenstruktur.
    // Create an array of CEDARECINFO for TDADataStruct
	BEGIN_CEDARECINFO(TDADATA,TDADataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Desr,"DESR")
		FIELD_CHAR_TRIM	(Idur,"IDUR")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM (Valu,"VALU")
	END_CEDARECINFO //(TDADataStruct)
	
	// Infostruktur kopieren
    for (int i = 0; i < sizeof(TDADataRecInfo)/sizeof(TDADataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&TDADataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}

	// Tabellenname per Default auf Tda setzen
	opExtName = pcgTableExt;
	SetTableNameAndExt(opTableName+opExtName);

    // Feldnamen initialisieren
	strcpy(pcmListOfFields,"CDAT,DESR,IDUR,LSTU,URNO,USEC,USEU,VALU");
	// Zeiger der Vaterklasse auf Tabellenstruktur setzen
	CCSCedaData::pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

    // Datenarrays initialisieren
	omUrnoMap.InitHashTable(629,117);

	// ToDo: min. / max. Datum f�r Tdas initialisieren
//	omMinDay = omMaxDay = TIMENULL;
}

//************************************************************************************************************************************************
// Destruktor
//************************************************************************************************************************************************

CedaTdaData::~CedaTdaData()
{
	ClearAll(true);
	omRecInfo.DeleteAll();
}

//************************************************************************************************************************************************
// SetTableNameAndExt: stellt den Namen und die Extension ein.
// R�ckgabe:	false	->	Tabellenname mit Extension gr��er als
//							Klassenmember
//				true	->	alles OK
//************************************************************************************************************************************************

bool CedaTdaData::SetTableNameAndExt(CString opTableAndExtName)
{
CCS_TRY
	if(opTableAndExtName.GetLength() >= sizeof(pcmTableName)) return false;
	strcpy(pcmTableName,opTableAndExtName.GetBuffer(0));
	return true;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// Register: beim Broadcasthandler anmelden. Um Broadcasts (Meldungen vom 
//	Datenbankhandler) zu empfangen, muss diese Funktion ausgef�hrt werden.
//	Der letzte Parameter ist ein Zeiger auf eine Callback-Funktion, welche
//	dann die entsprechende Nachricht bearbeitet. �ber den nach void 
//	konvertierten Zeiger auf dieses Objekt ((void *)this) kann das 
//	Objekt ermittelt werden, das der eigentliche Empf�nger der Nachricht ist
//	und an das die Callback-Funktion die Nachricht weiterleitet (per Aufruf einer
//	entsprechenden Member-Funktion).
// R�ckgabe: keine
//************************************************************************************************************************************************

void CedaTdaData::Register(void)
{
CCS_TRY
	// bereits angemeldet?
	if (bmIsBCRegistered) return;	// ja -> gibt nichts zu tun, terminieren

	// Tda-Datensatz hat sich ge�ndert
	ogDdx.Register((void *)this,BC_TDA_CHANGE, CString("CedaTdaData"), CString("BC_TDA_CHANGE"),        ProcessTdaCf);
	ogDdx.Register((void *)this,BC_TDA_NEW, CString("CedaTdaData"), CString("BC_TDA_NEW"),		   ProcessTdaCf);
	ogDdx.Register((void *)this,BC_TDA_DELETE, CString("CedaTdaData"), CString("BC_TDA_DELETE"),        ProcessTdaCf);
	// jetzt ist die Instanz angemeldet
	bmIsBCRegistered = true;
CCS_CATCH_ALL
}


//************************************************************************************************************************************************
// ClearAll: aufr�umen. Alle Arrays und PointerMaps werden geleert.
// R�ckgabe:	immer true
//************************************************************************************************************************************************

bool CedaTdaData::ClearAll(bool bpUnregister)
{
CCS_TRY
    // alle Arrays leeren
	omUrnoMap.RemoveAll();
	omData.DeleteAll();
	
	// beim BC-Handler abmelden
	if(bpUnregister && bmIsBCRegistered)
	{
		ogDdx.UnRegister(this,NOTUSED);
		// jetzt ist die Instanz angemeldet
		bmIsBCRegistered = false;
	}
    return true;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// ReadTdaByUrno: liest den Tda-Datensatz mit der  Urno <lpUrno> aus der 
//	Datenbank.
// R�ckgabe:	boolean Datensatz erfolgreich gelesen?
//************************************************************************************************************************************************

bool CedaTdaData::ReadTdaByUrno(long lpUrno, TDADATA *prpTda)
{
CCS_TRY
	char pclWhere[200]="";
	sprintf(pclWhere, "%ld", lpUrno);
	// Datensatz aus Datenbank lesen
	if (CedaAction("RT", pclWhere) == false)
	{
		return false;	// Fehler -> abbrechen
	}
	// Datensatz-Objekt instanziieren...
	TDADATA *prlTda = new TDADATA;
	// und initialisieren
	if (GetBufferRecord(0,prpTda) == true)
	{  
		return true;	// Aktion erfolgreich
	}
	else
	{
		// Fehler -> aufr�umen und terminieren
		delete prlTda;
		return false;
	}

	return false;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// Read: liest Datens�tze ein.
// R�ckgabe:	true	->	Aktion erfolgreich
//				false	->	Fehler beim Einlesen der Datens�tze
//************************************************************************************************************************************************

bool CedaTdaData::Read(char *pspWhere /*=NULL*/, CMapPtrToPtr *popLoadStfUrnoMap /*= NULL*/,
					   bool bpRegisterBC /*= true*/, bool bpClearData /*= true*/)
{
CCS_TRY
	// Return-Code f�r Funktionsaufrufe
	bool ilRc = true;

	// wenn gew�nscht, aufr�umen (und beim Broadcast-Handler abmelden, wenn
	// nach Ende angemeldet werden soll)
	if (bpClearData) ClearAll(bpRegisterBC);

	// beim Broadcast-Handler anmelden, wenn gew�nscht
	if (bpRegisterBC) Register();
	
    // Datens�tze lesen
	if(pspWhere == NULL)
	{
		if (!CCSCedaData::CedaAction("RT", "")) return false;
	}
	else
	{
		if (!CCSCedaData::CedaAction("RT", pspWhere)) return false;
	}

	// ToDo: vereinfachen/umwandeln in do/while, zwei branches f�r
	// if(pomLoadStfuUrnoMap != NULL) �berfl�ssig
    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecords = 0;
	// void-Pointer f�r MA-Urno-Lookup
	void  *prlVoid = NULL;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		TDADATA *prlTda = new TDADATA;
		// Datensatz lesen
		blMoreRecords = GetBufferRecord(ilCountRecords,prlTda);
		if (!blMoreRecords) break;
		// Datensatz gelesen, Z�hler inkrementieren
		ilCountRecords++;
		// wurde eine Datensatz gelesen und ist (wenn vorhanden) die
		// Mitarbeiter-Urno dieses Datensatzes im MA-Urno-Array enthalten? 
		/*if(blMoreRecords && 
		   ((popLoadStfUrnoMap == NULL) || 
			(popLoadStfUrnoMap->Lookup((void *)prlTda->Stfu, (void *&)prlVoid) == TRUE)))
		{*/ 
			// Datensatz hinzuf�gen
			if (!InsertInternal(prlTda, TDA_NO_SEND_DDX)){
				// Fehler -> lokalen Datensatz l�schen
				delete prlTda;
			}
		/*}
		else
		{
			// kein weiterer Datensatz
			delete prlTda;
		}*/
	} while (blMoreRecords);
	
	// Test: Anzahl der gelesenen Tda
	TRACE("Read-Tda: %d gelesen\n",ilCountRecords);

    return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// Insert: speichert den Datensatz <prpTda> in der Datenbank und schickt einen
//	Broadcast ab.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaTdaData::Insert(TDADATA *prpTda, bool bpSave /*= true*/)
{
CCS_TRY
	// �nderungs-Flag setzen
	prpTda->IsChanged = DATA_NEW;
	// in Datenbank speichern, wenn erw�nscht
	if(bpSave && Save(prpTda) == false) return false;
	// Broadcast TDA_NEW abschicken
	InsertInternal(prpTda,true);
    return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// InsertInternal: f�gt den internen Arrays einen Datensatz hinzu. Der Datensatz
//	muss sp�ter oder vorher explizit durch Aufruf von Save() oder Release() in der Datenbank 
//	gespeichert werden. Wenn <bpSendDdx> true ist, wird ein Broadcast gesendet.
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaTdaData::InsertInternal(TDADATA *prpTda, bool bpSendDdx)
{
CCS_TRY
	// Datensatz intern anf�gen
	omData.Add(prpTda);
	// Datensatz der Urno-Map hinzuf�gen
	omUrnoMap.SetAt((void *)prpTda->Urno,prpTda);
		
	// Broadcast senden?
	if( bpSendDdx == true )
	{
		// ja -> go!
		ogDdx.DataChanged((void *)this,TDA_NEW,(void *)prpTda);
	}
	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// Update: sucht den Datensatz mit der Urno <prpTda->Urno> und speichert ihn
// in der Datenbank.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaTdaData::Update(TDADATA *prpTda,bool bpSave /*= true*/)
{
CCS_TRY
	// Datensatz raussuchen
	if (GetTdaByUrno(prpTda->Urno) != NULL)
	{
		// �nderungsflag setzen
		if (prpTda->IsChanged == DATA_UNCHANGED)
		{
			prpTda->IsChanged = DATA_CHANGED;
		}
		// in die Datenbank speichern
		if(bpSave && Save(prpTda) == false) return false; 
		// Broadcast TDA_CHANGE versenden
		UpdateInternal(prpTda);
	}
    return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// UpdateInternal: �ndert einen Datensatz, der in der internen Datenhaltung
//	enthalten ist.
// R�ckgabe:	false	->	Datensatz wurde nicht gefunden
//				true	->	alles OK
//*********************************************************************************************

bool CedaTdaData::UpdateInternal(TDADATA *prpTda, bool bpSendDdx /*true*/)
{
CCS_TRY
// APO 9.12.99 Updates passieren immer auf Kopie eines Datensatzes der internen
// Datenhaltung, kopieren daher unn�tig.
/*	// Tda in lokaler Datenhaltung suchen
	TDADATA *prlTda = GetTdaByKey(prpTda->Sday,prpTda->Stfu,prpTda->Budn,prpTda->Rosl);
	// Tda gefunden?
	if (prlTda != NULL)
	{
		// ja -> durch Zeiger ersetzen
		*prlTda = *prpTda;
	}
*/
	// Broadcast senden, wenn gew�nscht
	if( bpSendDdx == true )
	{
//		TRACE("\nSending %d",TDA_CHANGE);
		ogDdx.DataChanged((void *)this,TDA_CHANGE,(void *)prpTda);
	}
	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// DeleteInternal: l�scht einen Datensatz aus den internen Arrays.
// R�ckgabe:	keine
//*********************************************************************************************

void CedaTdaData::DeleteInternal(TDADATA *prpTda, bool bpSendDdx /*true*/)
{
CCS_TRY
	// zuerst Broadcast senden, damit alle Module reagieren k�nnen BEVOR
	// der Datensatz gel�scht wird (synchroner Mechanismus)
	if(bpSendDdx == true)
	{
		ogDdx.DataChanged((void *)this,TDA_DELETE,(void *)prpTda);
	}

	// Urno-Schl�ssel l�schen
	omUrnoMap.RemoveKey((void *)prpTda->Urno);

	// Datensatz l�schen
	int ilCount = omData.GetSize();
	for (int i = 0; i < ilCount; i++)
	{
		if (omData[i].Urno == prpTda->Urno)
		{
			omData.DeleteAt(i);//Update omData
			break;
		}
	}
CCS_CATCH_ALL
}

//*********************************************************************************************
// Delete: markiert den Datensatz <prpTda> als gel�scht und l�scht den Datensatz
//	aus der internen Datenhaltung und der Datenbank. 
// R�ckgabe:	false	->	Fehler
//				true	->	alles OK
//*********************************************************************************************

bool CedaTdaData::Delete(TDADATA *prpTda, bool bpWithSave /* true*/)
{
CCS_TRY
	// Flag setzen
	prpTda->IsChanged = DATA_DELETED;
	
	// speichern
	if(bpWithSave == TRUE)
	{
		if (!Save(prpTda)) return false;
	}

	// aus der internen Datenhaltung l�schen
	DeleteInternal(prpTda,true);

	return true;
CCS_CATCH_ALL
return false;
}

//*********************************************************************************************
// ProcessTdaBc: behandelt die einlaufenden Broadcasts.
// R�ckgabe:	keine
//*********************************************************************************************

void  CedaTdaData::ProcessTdaBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
CCS_TRY
	// Performance-Trace
	SYSTEMTIME rlPerf;
	GetSystemTime(&rlPerf);
	TRACE("CedaTdaData::ProcessTdaBc: START %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);

	// Datensatz-Urno
	long llUrno;
	// Daten und Info
	struct BcStruct *prlBcStruct = NULL;
	prlBcStruct = (struct BcStruct *) vpDataPointer;
	TDADATA *prlTda = NULL;

	switch(ipDDXType)
	{
	case BC_TDA_NEW: // neuen Datensatz einf�gen
		{
			// einzuf�gender Datensatz
			prlTda = new TDADATA;
			GetRecordFromItemList(prlTda,prlBcStruct->Fields,prlBcStruct->Data);
			InsertInternal(prlTda, TDA_SEND_DDX);
		}
		break;
	case BC_TDA_CHANGE:	// Datensatz �ndern
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlTda = GetTdaByUrno(llUrno);
			if(prlTda != NULL)
			{
				// ja -> Datensatz aktualisieren
				GetRecordFromItemList(prlTda,prlBcStruct->Fields,prlBcStruct->Data);
				UpdateInternal(prlTda);
			}
			// nein -> Datensatz interessiert uns nicht
		}
		break;
	case BC_TDA_DELETE:	// Datensatz l�schen
		{
			// Datenatz-Urno ermittlen
			CString olSelection = (CString)prlBcStruct->Selection;
			if (olSelection.Find('\'') != -1)
			{
				llUrno = GetUrnoFromSelection(prlBcStruct->Selection);
			}
			else
			{
				int ilFirst = olSelection.Find("=")+2;
				int ilLast  = olSelection.GetLength();
				llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
			}
			// pr�fen, ob der Datensatz bereits in der internen Datenhaltung enthalten ist
			prlTda = GetTdaByUrno(llUrno);
			if (prlTda != NULL)
			{
				// ja -> Datensatz l�schen
				DeleteInternal(prlTda);
			}
		}
		break;
	default:
		break;
	}
	// Performance-Test
	GetSystemTime(&rlPerf);
	TRACE("CedaTdaData::ProcessTdaBc: END %d:%d:%d.%d\n",rlPerf.wHour,rlPerf.wMinute,rlPerf.wSecond,rlPerf.wMilliseconds);
CCS_CATCH_ALL
}

//*********************************************************************************************
// GetTdaByUrno: sucht den Datensatz mit der Urno <lpUrno>.
// R�ckgabe:	ein Zeiger auf den Datensatz oder NULL, wenn der
//				Datensatz nicht gefunden wurde
//*********************************************************************************************

TDADATA *CedaTdaData::GetTdaByUrno(long lpUrno)
{
CCS_TRY
	// der Datensatz
	TDADATA *prpTda;
	// Datensatz in Urno-Map suchen
	if (omUrnoMap.Lookup((void*)lpUrno,(void *& )prpTda) == TRUE)
	{
		return prpTda;	// gefunden -> Zeiger zur�ckgeben
	}
	// nicht gefunden -> R�ckgabe NULL
	return NULL;
CCS_CATCH_ALL
return NULL;
}

//*******************************************************************************
// Save: speichert den Datensatz <prpTda>.
// R�ckgabe:	bool Erfolg?
//*******************************************************************************

bool CedaTdaData::Save(TDADATA *prpTda)
{
CCS_TRY
	bool olRc = true;
	CString olListOfData;
	char pclSelection[1024];
	char pclData[2048];

	if (prpTda->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpTda->IsChanged)   // New job, insert into database
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpTda);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("IRT","","",pclData);
		prpTda->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpTda->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpTda);
		strcpy(pclData,olListOfData);
		olRc = CedaAction("URT",pclSelection,"",pclData);
		prpTda->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = '%ld'", prpTda->Urno);
		olRc = CedaAction("DRT",pclSelection);
		break;
	}
	// R�ckgabe: Ergebnis der CedaAction
	return olRc;
CCS_CATCH_ALL
return false;
}

//*************************************************************************************
// CedaAction: ruft die Funktion CedaAction der Basisklasse CCSCedaData auf.
//	Alle Parameter werden durchgereicht. Aufgrund eines internen CEDA-Bugs,
//	der daf�r sorgen kann, dass die Tabellenbeschreibung <pcmListOfFields>
//	zerst�rt wird, muss sicherheitshalber <pcmListOfFields> vor jedem Aufruf
//	von CCSCedaData::CedaAction() gerettet und danach wiederhergestellt werden.
// R�ckgabe:	der R�ckgabewert von CCSCedaData::CedaAction()
//*************************************************************************************

bool CedaTdaData::CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
							 char *pcpSelection, char *pcpSort, char *pcpData, 
							 char *pcpDest /*= "BUF1"*/,bool bpIsWriteAction /*= false*/, 
							 long lpID /*= 0*/, CCSPtrArray<RecordSet> *pomData /*= NULL*/, 
							 int ipFieldCount /*= 0*/)
{
CCS_TRY
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CCSCedaData::CedaAction(pcpAction,pcpSelection,pcpSort,pcpData,pcpDest,bpIsWriteAction,lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL
return false;
}

//*************************************************************************************
// CedaAction: kurze Version, Beschreibung siehe oben.
// R�ckgabe:	der R�ckgabewert von CCSCedaData::CedaAction()
//*************************************************************************************

bool CedaTdaData::CedaAction(char *pcpAction, char *pcpSelection, char *pcpSort,
							 char *pcpData, char *pcpDest /*"BUF1"*/, 
							 bool bpIsWriteAction/* false */, long lpID /* = imBaseID*/)
{
CCS_TRY
	// R�ckgabepuffer
	bool blRet;
	// Feldliste retten
	CString olOrigFieldList = GetFieldList();
	// CedaAction ausf�hren
	blRet = CCSCedaData::CedaAction(pcpAction,CCSCedaData::pcmTableName, CCSCedaData::pcmFieldList,(pcpSelection != NULL)? pcpSelection: CCSCedaData::pcmSelection,(pcpSort != NULL)? pcpSort: CCSCedaData::pcmSort,pcpData,pcpDest,bpIsWriteAction, lpID);
	// Feldliste wiederherstellen
	SetFieldList(olOrigFieldList);
	// R�ckgabecode von CedaAction durchreichen
	return blRet;
CCS_CATCH_ALL
return false;
}

//************************************************************************************************************************************************
// CopyTdaValues: Kopiert die "Nutzdaten" (keine urnos usw.)
// R�ckgabe:	keine
//************************************************************************************************************************************************

void CedaTdaData::CopyTda(TDADATA* popTdaDataSource,TDADATA* popTdadataTarget)
{
	// Daten kopieren
	/*strcpy(popTdaDataTarget->Rema,popTdaDataSource->Rema);
	strcpy(popTdaDataTarget->Sdac,popTdaDataSource->Sdac);
	popTdaDataTarget->Abfr = popTdaDataSource->Abfr;
	popTdaDataTarget->Abto = popTdaDataSource->Abto;*/
}

bool CedaTdaData::ReadTdaData()
{
CCS_TRY
	// Return-Code f�r Funktionsaufrufe
	bool ilRc = true;

	ClearAll(true);
	
    if (!CCSCedaData::CedaAction("RT", "")) return false;
	
	// ToDo: vereinfachen/umwandeln in do/while, zwei branches f�r
	// if(pomLoadStfuUrnoMap != NULL) �berfl�ssig
    bool blMoreRecords = false;
	// Datensatzz�hler f�r TRACE
	int ilCountRecords = 0;
	// solange es Datens�tze gibt
	do
	{
		// neuer Datensatz
		TDADATA *prlTda = new TDADATA;
		// Datensatz lesen
		blMoreRecords = GetBufferRecord(ilCountRecords,prlTda);
		// Datensatz gelesen, Z�hler inkrementieren
		if (!blMoreRecords) break;
		ilCountRecords++;
		if (!InsertInternal(prlTda, TDA_NO_SEND_DDX)){
			// Fehler -> lokalen Datensatz l�schen
			delete prlTda;
		}
	} while (blMoreRecords);
		
	// Test: Anzahl der gelesenen Tda
	TRACE("Read-Tda: %d gelesen\n",ilCountRecords);

    return true;
CCS_CATCH_ALL
return false;
}

