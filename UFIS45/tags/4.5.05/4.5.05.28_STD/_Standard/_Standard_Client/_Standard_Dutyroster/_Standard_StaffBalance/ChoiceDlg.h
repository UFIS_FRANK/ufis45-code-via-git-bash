#if !defined(AFX_CHOICEDLG_H__EC92D423_F5BD_11D4_907C_0050DADD7302__INCLUDED_)
#define AFX_CHOICEDLG_H__EC92D423_F5BD_11D4_907C_0050DADD7302__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ChoiceDlg.h : header file
//
// number of colmuns in grids
#define EMPL_COLCOUNT 3

#include <CedaBasicData.h>
#include <CedaStfData.h>
#include <CedaSorData.h>
#include <ColorControls.h>

class CGridControl;

/////////////////////////////////////////////////////////////////////////////
// CChoiceDlg dialog

class CChoiceDlg : public CDialog
{
// Construction
public:
	CChoiceDlg(CWnd* pParent = NULL);   // standard constructor
	~CChoiceDlg();
// Dialog Data
	//{{AFX_DATA(CChoiceDlg)
	enum { IDD = IDD_CHOICE_DLG };
	CColorButton	m_B_Filter;
	CDateTimeCtrl	m_DateTimePicker;
	CComboBox	m_C_Orgeinheit;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChoiceDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CString GetName();
	long GetUrno();
	void GetEmployee();
	bool SortGrid(int ipRow);
	void FillGrid();
	void InitEmployeeColWidths();
	void FillOrgCombo();
	void IniGrid();

	// apo 22.03.2001
	// send a broadcast to the script basic handler (the one who receives 'XBS'-broadcasts)
	bool SendBC(CString opCmd, 
				CString opData, 
				CString opAdditionalInfoFields,
				CString opAdditionalInfoData);
	// end of change apo 22.03.2001

	CGridControl* pomEmplList;
	HICON m_hIcon;
	CCSPtrArray<STFDATA> omStfData;

	// Generated message map functions
	//{{AFX_MSG(CChoiceDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnSelectAllEmployees();
	afx_msg void OnShowEmployee();
	afx_msg void OnFindEmployee();
	afx_msg void OnDeselectAllEmployees();
	afx_msg void OnSelchangeComboOrg();
	afx_msg void OnDatetimechangeDatetimepicker(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHOICEDLG_H__EC92D423_F5BD_11D4_907C_0050DADD7302__INCLUDED_)
