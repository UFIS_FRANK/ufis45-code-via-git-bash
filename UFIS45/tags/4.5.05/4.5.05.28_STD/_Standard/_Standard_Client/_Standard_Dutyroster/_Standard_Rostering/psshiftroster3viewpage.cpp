// PSShiftRoster3ViewPage.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Eigenschaftenseite PSShiftRoster3ViewPage 

IMPLEMENT_DYNCREATE(PSShiftRoster3ViewPage, RosterViewPage)

PSShiftRoster3ViewPage::PSShiftRoster3ViewPage() : RosterViewPage(PSShiftRoster3ViewPage::IDD)
{
	//{{AFX_DATA_INIT(PSShiftRoster3ViewPage)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
	omTitleStrg = LoadStg(IDS_W_LADEN_MITARBEITER);
	m_psp.pszTitle = omTitleStrg;
	m_psp.dwFlags |= PSP_USETITLE;
}

PSShiftRoster3ViewPage::~PSShiftRoster3ViewPage()
{
}

void PSShiftRoster3ViewPage::DoDataExchange(CDataExchange* pDX)
{
	RosterViewPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PSShiftRoster3ViewPage)
	DDX_Control(pDX, IDC_LB_QUALIFICATIONS,	m_LB_Qualifications);
	//}}AFX_DATA_MAP

	if (pDX->m_bSaveAndValidate == FALSE)
	{
		SetData();
	}
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		GetData();
	}

}

BEGIN_MESSAGE_MAP(PSShiftRoster3ViewPage, RosterViewPage)
	//{{AFX_MSG_MAP(PSShiftRoster3ViewPage)
	ON_BN_CLICKED(IDC_ALL, OnAll)
	ON_BN_CLICKED(IDC_NOTHING, OnNothing)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten PSShiftRoster3ViewPage 
//---------------------------------------------------------------------------

void PSShiftRoster3ViewPage::SetCalledFrom(CString opCalledFrom)
{
	omCalledFrom = opCalledFrom;
}

//---------------------------------------------------------------------------

BOOL PSShiftRoster3ViewPage::OnInitDialog() 
{
	RosterViewPage::OnInitDialog();

	CString olCode, olName;

	SetDlgItemText(IDC_S_QUALIFICATIONS,LoadStg(IDS_STRING1382));
	SetDlgItemText(IDC_ALL,LoadStg(IDS_S_All));
	SetDlgItemText(IDC_NOTHING,LoadStg(IDS_S_NoOne));
	
	CString olLine;
	m_LB_Qualifications.ResetContent();
	m_LB_Qualifications.SetFont(&ogCourier_Regular_8);
	int ilPerCount =  ogPerData.omData.GetSize();
	olCode = "---------";
	olName = LoadStg(IDS_STRING1937);
	olLine.Format("%-20s   %-40s     URNO=NO_CODE", olCode, olName, 0);
	m_LB_Qualifications.AddString(olLine);
	for(int iPer=0; iPer<ilPerCount; iPer++)
	{
		olCode = ogPerData.omData[iPer].Prmc;
		olName = ogPerData.omData[iPer].Prmn;
		olLine.Format("%-20s   %-40s     URNO=%d", olCode, olName, ogPerData.omData[iPer].Urno);
		m_LB_Qualifications.AddString(olLine);
	}
	
	SetData();

	if (ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER") == "ADR")
	{
		m_LB_Qualifications.SetSel(0);
	}

	return TRUE;
}

//---------------------------------------------------------------------------

BOOL PSShiftRoster3ViewPage::GetData()
{
	omValues.RemoveAll();
	CStringArray olTmpValues;
	if(GetData(olTmpValues) == FALSE)
	{
		return FALSE;
	}
	for(int i = 0; i < olTmpValues.GetSize(); i++)
	{
		CString olTmp = olTmpValues[i];
		omValues.Add(olTmpValues[i]);
	}
	return TRUE;
}

//---------------------------------------------------------------------------

BOOL PSShiftRoster3ViewPage::GetData(CStringArray &opValues)
{
	int ilMaxItems;
	ilMaxItems = m_LB_Qualifications.GetSelCount();
	int *pilIndex = new int [ilMaxItems];
	m_LB_Qualifications.GetSelItems(ilMaxItems, pilIndex );
	CString olTmpTxt, olUrno, olUrnos;
	for(int i=0; i<ilMaxItems; i++)
	{
		m_LB_Qualifications.GetText(pilIndex[i],olTmpTxt);
		olUrno = olTmpTxt.Mid(olTmpTxt.Find("URNO=")+5);
		olUrnos += olUrno;
		if(i < ilMaxItems-1)
			olUrnos += CString("|");
	}
	delete pilIndex;
	opValues.Add(olUrnos);
	return TRUE;
}

//---------------------------------------------------------------------------

void PSShiftRoster3ViewPage::SetData()
{
	SetData(omValues);
}

//---------------------------------------------------------------------------

void PSShiftRoster3ViewPage::SetData(CStringArray &opValues)
{	
	int ilMaxItems;
	ilMaxItems = m_LB_Qualifications.GetSelCount();
	int *pilIndex = new int [ilMaxItems];
	m_LB_Qualifications.GetSelItems(ilMaxItems, pilIndex );
	for(int i=0; i<ilMaxItems; i++)
	{
		m_LB_Qualifications.SetSel(pilIndex[i],FALSE);
	}
	delete pilIndex;
	
	for(int ilValue = 0; ilValue < opValues.GetSize(); ilValue++)
	{
		switch(ilValue)
		{
			case 0:
			{
				if(opValues[ilValue].IsEmpty() == FALSE)
				{
					CString olTmpTxt, olUrno;
					int ilCount = m_LB_Qualifications.GetCount();
					for(int i = 0;i<ilCount;i++)
					{
						m_LB_Qualifications.GetText(i,olTmpTxt);
						olUrno = olTmpTxt.Mid(olTmpTxt.Find("URNO=")+5);
						if(opValues[ilValue].Find(olUrno) != -1)
							m_LB_Qualifications.SetSel(i,TRUE);
					}
				}
 			}
			break;
		}
	}
}

//---------------------------------------------------------------------------

void PSShiftRoster3ViewPage::OnAll() 
{
	int ilItems;
	ilItems = m_LB_Qualifications.GetCount();
	for(int i=0; i<ilItems; i++)
	{
		m_LB_Qualifications.SetSel(i,TRUE);
	}
}

//---------------------------------------------------------------------------

void PSShiftRoster3ViewPage::OnNothing() 
{
	int ilItems;
	ilItems = m_LB_Qualifications.GetCount();
	for(int i=0; i<ilItems; i++)
	{
		m_LB_Qualifications.SetSel(i,FALSE);
	}
}

//---------------------------------------------------------------------------


