#if !defined(AFX_CHANGESHIFTDLG_H__42F9BAC5_2019_11D4_9001_00500454BF3F__INCLUDED_)
#define AFX_CHANGESHIFTDLG_H__42F9BAC5_2019_11D4_9001_00500454BF3F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ChangeShiftDlg.h : Header-Datei
//

#include <stdafx.h>
/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CChangeShiftDlg 

class CChangeShiftDlg : public CDialog
{
// Konstruktion
public:
	CChangeShiftDlg(CWnd* pParent, COleDateTime opDate, long lpStfUrno, CString opRosl);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(CChangeShiftDlg)
	enum { IDD = IDD_DUTY_CHANGESHIFT };
	CComboBox	m_C_WGPC2;
	CComboBox	m_C_FCTC2;
	CComboBox	m_C_WGPC1;
	CComboBox	m_C_FCTC1;
	CComboBox	m_C_SUB1SUB2_2;
	CComboBox	m_C_SUB1SUB2_1;
	COleDateTime	m_DatePicker1;
	COleDateTime	m_DatePicker2;
	CString	m_EditEmpl1;
	CString	m_EditEmpl2;
	//}}AFX_DATA

// �berschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktions�berschreibungen
	//{{AFX_VIRTUAL(CChangeShiftDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Schichten setzen
	void SetShift(int npEmpl,long lpStfUrno);
	// Eingaben pr�fen
	bool CheckInput();
	// Konten berechnen
	void SetAccounts(bool bpBefore);
	// Berechnete Resultate L�schen
	void ClearResults();
	// Daten aus der Datenbank f�r einen Monat einlesen
	bool LoadMonthAndEmpl(COleDateTime opDate);
	// Schichten tauschen
	void ChangeShifts();
	// Statische Elemente setzen
	void SetStatics() ;
	// DRS Daten sichern (MA letzter Tausch)
	void SaveDrsData();
	void Swap(long* lp1, long* lp2);
	void Swap(COleDateTime* lp1, COleDateTime* lp2);
	void Swap(char* lp1, char* lp2);

 	DRRDATA *pomDrr1,*pomDrr2;
	DRRDATA *pom2Drr1,*pom2Drr2;
	// Ausgew�hlte Mitarbeiter
	long lmEmpl1,lmEmpl2;

	// Shift URNOs
	long lmOldBsdu1,lmOldBsdu2;
	long lmNewBsdu1,lmNewBsdu2;

	// Shift codes
	CString omOldScod1,omOldScod2;
	CString omNewScod1,omNewScod2;

	// Function codes
	CString omFctc1,omFctc2;

	// SUB1/SUB2 (DRR.DRS2)
	CString omDrs2_1,omDrs2_2;

	// working group
	CString omWGPC1, omWGPC2;

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CChangeShiftDlg)
	afx_msg void OnSelectEmpl1();
	afx_msg void OnSelectEmpl2();
	virtual BOOL OnInitDialog();
	afx_msg void OnDatetimechangeDatetimepicker1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDatetimechangeDatetimepicker2(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButtonapply();
	virtual void OnOK();
	afx_msg void OnKillfocusEmpl1Number();
	afx_msg void OnKillfocusEmpl2Number();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	void SetCombos();
	CString omRosl;
	DutyRoster_View* pomParent;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ f�gt unmittelbar vor der vorhergehenden Zeile zus�tzliche Deklarationen ein.

#endif // AFX_CHANGESHIFTDLG_H__42F9BAC5_2019_11D4_9001_00500454BF3F__INCLUDED_
