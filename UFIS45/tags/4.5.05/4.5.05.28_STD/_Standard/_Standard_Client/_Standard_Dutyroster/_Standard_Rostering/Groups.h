// Groups.h: interface for the CGroups class.
//	BDA 23.02.2000
//////////////////////////////////////////////////////////////////////

#if !defined(GROUPS_H)
#define GROUPS_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/********************************************************************************************
CGroups kapselt die Gruppen in Rostering-interner Datenhaltung und -bearbeitung
haupts�chlich f�r die Darstellung in SolidView und DragDropView
********************************************************************************************/

// allgemeine Includes
#include <stdafx.h>
/*#include "ccsglobl.h"
#include <CCSPtrArray.h>*/

// BDA 22.02.2000 Umstellung von ROS/DSR auf DRR & Co.
struct GROUPCHANGEINFO
{
	CString oWgpc;		// WGPC - Arbeitsgruppencode aus WGP
	CString oFctc;	// FCTC - Funktionscode aus PFC
	long lStfu;		// STFU - Urno des MA
	COleDateTime oDay;	// SDAY
	CString oDrrn;		// DRRN - Schicht Nummer

	GROUPCHANGEINFO(void)
	{
		oWgpc = "";
		oFctc = "";
		lStfu = 0;
		oDay.SetStatus(COleDateTime::invalid);
	}
};

// Gruppe1 und Gruppe2 wechseln sich, wobei ein von beiden Pointer kann 0 sein
void ChangeGroup(GROUPCHANGEINFO *prpChangeInfo1, GROUPCHANGEINFO *prpChangeInfo2, bool bpCheckMainFuncOnly);

/****************************************************************************
 struct GROUPSTRUCT
****************************************************************************/

struct GROUPSTRUCT
{
	long lStfUrno;				// Mitarbeiter-Urno
	CString oStfPeno;			// Mitarbeiter-Personalnummer
	CString oStfFinm;			// Mitarbeiter-Vorname    
	CString oStfLanm;			// Mitarbeiter-Nachname
	CString oStfPerc;			// Mitarbeiter-K�rzel

	CTime oShiftTime;			// Schichtbeginnzeit
	CString oShiftCode;			// Schicht-Code
	CString	oDrrn;			 	// Schichtnummer (1-n pro Tag) BDA 22.02.2000


	CString oWgpCode;			// Arbeitsgruppen-Code
	int	iGroupPrio;				// Priorit�t der Arbeitsgruppe in der Plangruppe
								// (in der Liste allen Arbeitsgruppen, die dieser Plangruppe geh�ren)
	CString oPfcCode;			// Funktions-Code

	int	iColor1Nr;				// Farb-Code
	CString oStfShnm; 			// Kurzname
	int iSort;					// Index zum schnellen Sortieren in FillGroupStruct() u.a.

	GROUPSTRUCT(void)
	{
		lStfUrno		= 0;
		oStfPeno		= "";
		oStfFinm		= "";
		oStfLanm		= "";
		oStfPerc		= "";
		oShiftTime		= -1;
		oShiftCode		= "";
		oDrrn			= "";
		oWgpCode		= "";
		iGroupPrio		= 0;
		oPfcCode		= "";
		iColor1Nr		= -1;
		oStfShnm		= "";
		iSort			= INT_MIN;
	}
};

enum Compare
{
	CompareGroupAbc,
	CompareGroupByTime
};

class CGroups  : public CCSPtrArray<GROUPSTRUCT>
{
public:
	~CGroups();
	void Sort(int ipCompareMethod);
};

#endif // !defined(GROUPS_H)
