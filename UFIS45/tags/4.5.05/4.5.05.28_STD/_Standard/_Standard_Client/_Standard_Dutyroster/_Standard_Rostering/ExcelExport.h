// ExcelExport.h: interface for the ExcelExport class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_EXCELEXPORT_H__3E2E34B3_C0C7_11D7_80E0_0001022205E4__INCLUDED_)
#define AFX_EXCELEXPORT_H__3E2E34B3_C0C7_11D7_80E0_0001022205E4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class ExcelExport  
{
public:
	ExcelExport(CWnd* pParent = NULL);
	virtual ~ExcelExport();
	void Export(CString opExportName);

private:
	DutyRoster_View* pomParent;
	CCSPtrArray<STFDATA> *pomStfData;
	VIEWINFO *prmViewInfo;
	CGroups *pomGroups;
	CString omTrenner;
	CString omViewName;
	CString omExcelFileName;

private:
	CString CreateFileName();
	bool CreateExportViewData();
	bool CreateExportBlankShifts();
	bool CreateExportXAbsences();
	bool CreateExportLevel1Deviations(CString opExportParameters);

	CString GetEmployeeCaption(int ipColNo);
	CString GetDayCaption(COleDateTime &opDay);
	CString GetAccountCaption(int ipAccountType);

	CString GetEmployeeCellText(int ipColNo, int ipStfUrnoNo);
	CString GetDayCellText(COleDateTime &opDay, int ipStfUrnoNo, CString opRosl);
	CString GetAccountCellText(COleDateTime &opFrom, COleDateTime &opTo, int ipStfUrnoNo, int ipAccColNo);
	CString GetDebitCellText(int ilLineNo, int ilDayNo);
};

#endif // !defined(AFX_EXCELEXPORT_H__3E2E34B3_C0C7_11D7_80E0_0001022205E4__INCLUDED_)
