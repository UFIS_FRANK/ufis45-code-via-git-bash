// PSShiftRoster1ViewPage.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Eigenschaftenseite PSShiftRoster1ViewPage 

IMPLEMENT_DYNCREATE(PSShiftRoster1ViewPage, RosterViewPage)

PSShiftRoster1ViewPage::PSShiftRoster1ViewPage() : RosterViewPage(PSShiftRoster1ViewPage::IDD)
{
	//{{AFX_DATA_INIT(PSShiftRoster1ViewPage)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
	omTitleStrg = LoadStg(SHIFT_SHEET_PAGE_CAPTION);
	m_psp.pszTitle = omTitleStrg;
	m_psp.dwFlags |= PSP_USETITLE;
	prmNameConfigurationDlg = NULL;
}

PSShiftRoster1ViewPage::~PSShiftRoster1ViewPage()
{
}

void PSShiftRoster1ViewPage::DoDataExchange(CDataExchange* pDX)
{
	RosterViewPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PSShiftRoster1ViewPage)
	DDX_Control(pDX, IDC_R_DIFF,			m_R_Diff);
	DDX_Control(pDX, IDC_R_ISTSOLL,			m_R_IstSoll);
	DDX_Control(pDX, IDC_R_ALLGSP,			m_R_AllGSP);
	DDX_Control(pDX, IDC_R_SPLANGSP,		m_R_SPlanGSP);
	DDX_Control(pDX, IDC_C_GRUPP,			m_C_Grupp);
	DDX_Control(pDX, IDC_C_DETAIL,			m_C_Detail);
	DDX_Control(pDX, IDC_C_BONUS,			m_C_Bonus);
	DDX_Control(pDX, IDC_S_SHOWSTATISTIC,	m_S_ShowStatistic);
	DDX_Control(pDX, IDC_S_SHOWGSP,			m_S_ShowGSP);
	DDX_Control(pDX, IDC_S_SHOWDIFF,		m_S_ShowDiff);
	DDX_Control(pDX, IDC_R_PERC,			m_R_Perc);
	DDX_Control(pDX, IDC_R_FLNAME,			m_R_FLName);
	DDX_Control(pDX, IDC_S_EMPLOYEE,		m_S_Employee);
	DDX_Control(pDX, IDC_S_SHOWSP,			m_S_ShowSP);
	DDX_Control(pDX, IDC_CMB_SHIFTPLANS,	m_C_ShowSPCombo);
	DDX_Control(pDX, IDC_R_FREE,			m_R_Free);
	//}}AFX_DATA_MAP

	if (pDX->m_bSaveAndValidate == FALSE)
	{
		SetData();
	}

	if (pDX->m_bSaveAndValidate == TRUE)
	{
		GetData();
	}
}


BEGIN_MESSAGE_MAP(PSShiftRoster1ViewPage, RosterViewPage)
	//{{AFX_MSG_MAP(PSShiftRoster1ViewPage)
	ON_BN_CLICKED(IDC_R_FREE, OnRFree)
	ON_BN_CLICKED(IDC_R_FLNAME, OnRFlname)
	ON_BN_CLICKED(IDC_R_PERC, OnRPerc)
	ON_BN_CLICKED(IDC_B_START_CONFIG, OnBStartConfig)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten PSShiftRoster1ViewPage 
//---------------------------------------------------------------------------

void PSShiftRoster1ViewPage::SetCalledFrom(CString opCalledFrom)
{
	omCalledFrom = opCalledFrom;
}

//---------------------------------------------------------------------------

BOOL PSShiftRoster1ViewPage::OnInitDialog() 
{
	RosterViewPage::OnInitDialog();

	SetWindowText(LoadStg(SHIFT_SHEET_PAGE_CAPTION));
	m_S_ShowStatistic.SetWindowText(LoadStg(SHIFT_SHEET_S_SHOWSTATISTIC));
	m_S_ShowGSP.SetWindowText(LoadStg(SHIFT_SHEET_S_SHOWGSP));
	m_S_ShowDiff.SetWindowText(LoadStg(SHIFT_SHEET_S_SHOWDIFF));
	m_C_Grupp.SetWindowText(LoadStg(SHIFT_SHEET_C_GRUPP));
	m_C_Detail.SetWindowText(LoadStg(SHIFT_SHEET_C_DETAIL));
	m_C_Bonus.SetWindowText(LoadStg(SHIFT_SHEET_C_BONUS));
	m_R_AllGSP.SetWindowText(LoadStg(SHIFT_SHEET_R_ALLGSP));
	m_R_SPlanGSP.SetWindowText(LoadStg(SHIFT_SHEET_R_SPLAN_GSP));
	m_R_Diff.SetWindowText(LoadStg(SHIFT_SHEET_R_DIFF));
	m_R_IstSoll.SetWindowText(LoadStg(SHIFT_SHEET_R_ISTSOLL));
	m_S_Employee.SetWindowText(LoadStg(SHIFT_SHEET_S_EMPLOYEE));
	m_R_Perc.SetWindowText(LoadStg(SHIFT_SHEET_C_CODE));
	m_R_FLName.SetWindowText(LoadStg(SHIFT_SHEET_C_FLNAME));
	m_S_ShowSP.SetWindowText(LoadStg(IDS_STRING463));
	SetDlgItemText(IDC_R_FREE, LoadStg(IDS_STRING1808));
	SetDlgItemText(IDC_B_START_CONFIG, LoadStg(IDS_STRING1795));

	if (!bgEnableScenario)
		HideScenario();

	ShiftRoster_View* pView = GetViewInfo();
	if (pView != NULL)
	{
		prmNameConfigurationDlg = pView->GetNamesConfigurationDlg();
	}
	HandleNameRadioButtons();

	return TRUE;
}

//---------------------------------------------------------------------------

BOOL PSShiftRoster1ViewPage::GetData()
{
	omValues.RemoveAll();
	CStringArray olTmpValues;
	if (GetData(olTmpValues) == FALSE)
	{
		return FALSE;
	}

	for (int i = 0; i < olTmpValues.GetSize(); i++)
	{
		CString olTmp = olTmpValues[i];
		omValues.Add(olTmpValues[i]);
	}
	return TRUE;
}

//---------------------------------------------------------------------------

BOOL PSShiftRoster1ViewPage::GetData(CStringArray &opValues)
{
	//0 ----------------------------
	if(m_R_SPlanGSP.GetCheck() == 1)
		opValues.Add("PLAN");
	else
		opValues.Add("ALL");

	//1 ----------------------------
	if(m_C_Grupp.GetCheck() == 1)
		opValues.Add("J");
	else
		opValues.Add("N");

	//2----------------------------
	if(m_C_Detail.GetCheck() == 1)
		opValues.Add("J");
	else
		opValues.Add("N");

	//3----------------------------
	if(m_C_Bonus.GetCheck() == 1)
		opValues.Add("J");
	else
		opValues.Add("N");

	//4----------------------------
	if(m_R_Diff.GetCheck() == 1)
		opValues.Add("DIFF");
	else
		opValues.Add("ISDEBIT");

	//5----------------------------
	if(m_R_Perc.GetCheck() == 1)
		opValues.Add("CODE");
	else if (m_R_Free.GetCheck() == 1)
		opValues.Add("FREE");
	else
		opValues.Add("FLNAME");

	//6----------------------------
	CString olCurSel;
	olCurSel.Format("%d", m_C_ShowSPCombo.GetCurSel());
	opValues.Add (olCurSel);

	//7----------------------------
	opValues.Add (omConfigString);

	return TRUE;
}

//---------------------------------------------------------------------------

void PSShiftRoster1ViewPage::SetData()
{
	SetData(omValues);
}

//---------------------------------------------------------------------------

void PSShiftRoster1ViewPage::SetData(CStringArray &opValues)
{
	//*** Set all adjustments to default ************************************
	m_R_AllGSP.SetCheck(1);
	m_R_SPlanGSP.SetCheck(0);
	m_C_Grupp.SetCheck(1);
	m_C_Detail.SetCheck(1);
	m_C_Bonus.SetCheck(1);
	m_R_Diff.SetCheck(0);
	m_R_IstSoll.SetCheck(1);
	m_R_Perc.SetCheck(0);
	m_R_FLName.SetCheck(1);
	m_R_Free.SetCheck(0);

	// initialising the combo containing the information which shift plans to be displayed
	m_C_ShowSPCombo.ResetContent();
	m_C_ShowSPCombo.AddString(LoadStg(IDS_STRING464));	// Alle Schichtpl�ne
	m_C_ShowSPCombo.AddString(LoadStg(IDS_STRING465));	// Operative Schichtpl�ne
	m_C_ShowSPCombo.AddString(LoadStg(IDS_STRING466));	// Geplante Schichtpl�ne
	m_C_ShowSPCombo.AddString(LoadStg(IDS_STRING467));	// Archivierte Schichtpl�ne

	//*** end> Set all adjustments to default *******************************

	for (int i = 0; i < opValues.GetSize(); i++)
	{
		switch(i)
		{
		case 0:
			{
				if(opValues[i] == "PLAN")
				{
					m_R_SPlanGSP.SetCheck(1);
					m_R_AllGSP.SetCheck(0);
				}
 				else
				{
					m_R_SPlanGSP.SetCheck(0);
					m_R_AllGSP.SetCheck(1);
				}
 			}
			break;
		case 1:
			{
				if(opValues[i] == "J")
					m_C_Grupp.SetCheck(1);
				else
					m_C_Grupp.SetCheck(0);
			}
			break;
		case 2:
			{
				if(opValues[i] == "J")
					m_C_Detail.SetCheck(1);
				else
					m_C_Detail.SetCheck(0);
			}
			break;
		case 3:
			{
				if(opValues[i] == "J")
					m_C_Bonus.SetCheck(1);
				else
					m_C_Bonus.SetCheck(0);
			}
			break;
		case 4:
			{
				if(opValues[i] == "DIFF")
				{
					m_R_Diff.SetCheck(1);
					m_R_IstSoll.SetCheck(0);
				}
 				else
				{
					m_R_Diff.SetCheck(0);
					m_R_IstSoll.SetCheck(1);
				}
 			}
			break;
		case 5:
			{
				if(opValues[i] == "CODE")
				{
					m_R_Perc.SetCheck(1);
					m_R_FLName.SetCheck(0);
					m_R_Free.SetCheck(0);
				}
 				else if (opValues[i] == "FLNAME")
				{
					m_R_Perc.SetCheck(0);
					m_R_FLName.SetCheck(1);
					m_R_Free.SetCheck(0);
				}
				else if (opValues[i] == "FREE")
				{
					m_R_Perc.SetCheck(0);
					m_R_FLName.SetCheck(0);
					m_R_Free.SetCheck(1);
				}
 			}
			break;
		case 6:
			{
				if (opValues[i] == "")
				{
					m_C_ShowSPCombo.SetCurSel(1);
				}
				else
				{
					m_C_ShowSPCombo.SetCurSel(atoi(opValues[i].GetBuffer(0)));
				}
			}
			break;
		case 7:
			{
				bool blConfigString = false;
				if (strlen(opValues[i]))
				{
					omConfigString = opValues[i];
					blConfigString = true;
				}
				else if (strlen(pcgDefaultNameConfig_Shift))
				{
					omConfigString = CString(pcgDefaultNameConfig_Shift);
					blConfigString = true;
				}

				if (blConfigString == true)
				{
					m_R_Perc.SetCheck(0);
					m_R_FLName.SetCheck(0);
					m_R_Free.SetCheck(1);
				}
			}
			break;
		}
	}
	HandleNameRadioButtons();
}

//---------------------------------------------------------------------------

void PSShiftRoster1ViewPage::HideScenario()
{
	CRect olRect;
	int ilMoveUp = 48;

	GetDlgItem (IDC_S_SHOWSP)->ShowWindow(SW_HIDE);
	GetDlgItem (IDC_CMB_SHIFTPLANS)->ShowWindow(SW_HIDE);

	GetDlgItem (IDC_R_DIFF)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_R_DIFF)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDC_R_ISTSOLL)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_R_ISTSOLL)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDC_R_ALLGSP)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_R_ALLGSP)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDC_R_SPLANGSP)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_R_SPLANGSP)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDC_C_GRUPP)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_C_GRUPP)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDC_C_DETAIL)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_C_DETAIL)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDC_C_BONUS)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_C_BONUS)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDC_S_SHOWSTATISTIC)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_S_SHOWSTATISTIC)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDC_S_SHOWGSP)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_S_SHOWGSP)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDC_S_SHOWDIFF)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_S_SHOWDIFF)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDC_R_PERC)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_R_PERC)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDC_R_FLNAME)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_R_FLNAME)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDC_S_EMPLOYEE)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_S_EMPLOYEE)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDC_R_FREE)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_R_FREE)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDC_B_START_CONFIG)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDC_B_START_CONFIG)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());
}

void PSShiftRoster1ViewPage::OnRFree() 
{
	HandleNameRadioButtons();
}

void PSShiftRoster1ViewPage::OnRFlname() 
{
	HandleNameRadioButtons();	
}

void PSShiftRoster1ViewPage::OnRPerc() 
{
	HandleNameRadioButtons();
}

void PSShiftRoster1ViewPage::HandleNameRadioButtons()
{
	if (m_R_Free.GetCheck() == 1)
	{
		GetDlgItem(IDC_B_START_CONFIG)->EnableWindow(true);
	}
	else
	{
		GetDlgItem(IDC_B_START_CONFIG)->EnableWindow(false);
	}
}

void PSShiftRoster1ViewPage::OnBStartConfig() 
{
	if (prmNameConfigurationDlg != NULL)
	{
		prmNameConfigurationDlg->SetConfigString(omConfigString);

		int ilRet = prmNameConfigurationDlg->DoModal();
		if (ilRet == IDOK)
		{
			omConfigString = prmNameConfigurationDlg->GetConfigString();
		}
	}
}

ShiftRoster_View* PSShiftRoster1ViewPage::GetViewInfo()
{
	ShiftRoster_View *pView = NULL;

	CWinApp *pApp = AfxGetApp();
	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL && pView == NULL)
	{
		CDocTemplate *pDocTempl = pApp->GetNextDocTemplate(pos);
		POSITION p1 = pDocTempl->GetFirstDocPosition();
		while(p1 != NULL && pView == NULL)
		{
			CDocument *pDoc = pDocTempl->GetNextDoc(p1);
			POSITION p2 = pDoc->GetFirstViewPosition();
			while (p2 != NULL && pView == NULL)
			{
				pView = DYNAMIC_DOWNCAST(ShiftRoster_View,pDoc->GetNextView(p2));
								
			}
		}
	}

	return pView;
}