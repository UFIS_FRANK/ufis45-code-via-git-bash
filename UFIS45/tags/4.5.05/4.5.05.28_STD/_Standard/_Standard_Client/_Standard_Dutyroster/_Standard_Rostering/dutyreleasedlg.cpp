// DutyReleaseDlg.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld DutyReleaseDlg 
/////////////////////////////////////////////////////////////////////////////

DutyReleaseDlg::DutyReleaseDlg(CWnd* pParent /*=NULL*/) : CDialog(DutyReleaseDlg::IDD, pParent)
{
	CCS_TRY;
	//{{AFX_DATA_INIT(DutyReleaseDlg)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT

	pomParent = (DutyRoster_View*)pParent;
	pomStfData = &pomParent->omStfData;
	prmViewInfo = &pomParent->rmViewInfo;

	omPTypes.RemoveAll();
	ExtractItemList(prmViewInfo->oPType, &omPTypes, ',');

	omVpfr = prmViewInfo->oDateFrom.Format("%Y%m%d");
	omVpto = prmViewInfo->oDateTo.Format("%Y%m%d");

	imStepWithAbsenceLevel = 0;

	pomStatusBar = NULL;

	CCS_CATCH_ALL;
}

//********************************************************************************
//
//********************************************************************************

DutyReleaseDlg::~DutyReleaseDlg()
{
CCS_TRY;
	pomStatusBar->DeleteStatusBar();
	omStfLines.DeleteAll();
	// Grid l�schen
	if (pomGrid) 
	{
		delete pomGrid;
		pomGrid = 0;
	}
CCS_CATCH_ALL;
}

//********************************************************************************
//
//********************************************************************************
void DutyReleaseDlg::DoDataExchange(CDataExchange* pDX)
{
CCS_TRY;
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DutyReleaseDlg)
	DDX_Control(pDX, IDC_SHOWJOBS, m_B_ShowJobs);
	DDX_Control(pDX, IDC_REL_NOW,	m_RelNow);
	DDX_Control(pDX, IDC_REL_NIGHT, m_RelNight);
	DDX_Control(pDX, IDC_B_FIND,	m_b_Find);
	DDX_Control(pDX, IDC_PLAN1,		m_Plan1);
	DDX_Control(pDX, IDC_PLAN2,		m_Plan2);
	DDX_Control(pDX, IDC_PLAN3,		m_Plan3);
	DDX_Control(pDX, IDC_PLAN4,		m_Plan4);
	DDX_Control(pDX, IDC_PLANL,		m_PlanL);
	DDX_Control(pDX, IDC_DAYTO,		m_DayTo);
	DDX_Control(pDX, IDC_DAYFROM,	m_DayFrom);
	DDX_Control(pDX, IDC_SDAYTO,	m_SDayTo);
	DDX_Control(pDX, IDC_SDAYFROM,	m_SDayFrom);
	DDX_Control(pDX, IDC_S_1,		m_S1);
	DDX_Control(pDX, IDC_S_2,		m_S2);
	DDX_Control(pDX, IDC_S_3,		m_S3);
	DDX_Control(pDX, IDC_S_4,		m_S4);
	DDX_Control(pDX, IDC_S_5,		m_S5);
	DDX_Control(pDX, IDC_S_L,		m_SL);
	DDX_Control(pDX, IDC_S_U,		m_SU);
	//}}AFX_DATA_MAP
CCS_CATCH_ALL;
}


BEGIN_MESSAGE_MAP(DutyReleaseDlg, CDialog)
	//{{AFX_MSG_MAP(DutyReleaseDlg)
	ON_BN_CLICKED(IDC_ALL,	   OnBAll)
	ON_BN_CLICKED(IDC_NOTHING, OnBNothing)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_B_FIND, OnBFind)
	ON_BN_CLICKED(IDC_B_MAX, OnBMax)
	ON_BN_CLICKED(IDC_B_MIN, OnBMin)
	ON_BN_CLICKED(IDC_SHOWJOBS, OnShowJobs)
	ON_BN_CLICKED(ID_UNRELEASE, OnUnrelease)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten DutyReleaseDlg 
/////////////////////////////////////////////////////////////////////////////
//---------------------------------------------------------------------------

void DutyReleaseDlg::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	CPen olPen;
	bool blSet = true;
	olPen.CreatePen(PS_SOLID, 1, COLORREF(BLACK)); 
	dc.SelectObject(&olPen);
	CBrush olBrush;
	olBrush.CreateSolidBrush(COLORREF(BLACK));

	for (int i = 0; i < 6; i++)
	{
		CString olStep, csCode;
		CRect olRect = CRect(0,0,0,0);

		switch (i)
		{
		case 0:
			{
				csCode = ogPrivList.GetStat("DUTYRELEASEDLG_IDC_PLAN1");
				if ((csCode=='1' || csCode=='0') && bmShow1)
				{
					m_S1.GetWindowRect(&olRect);
					olBrush.DeleteObject();
					olBrush.CreateSolidBrush(COLORREF(GRAY));
					olStep = "1";
				}
				break;
			}
		case 1:
			{
				csCode = ogPrivList.GetStat("DUTYRELEASEDLG_IDC_PLAN2");
				if ((csCode=='1' || csCode=='0') && bmShow2)
				{
					m_S2.GetWindowRect(&olRect);
					olBrush.DeleteObject();
					olBrush.CreateSolidBrush(COLORREF(BLACK));
					olStep = "2";
				}
				break;
			}
		case 2:
			{
				csCode = ogPrivList.GetStat("DUTYRELEASEDLG_IDC_PLAN3");
				if ((csCode=='1' || csCode=='0') && bmShow3)
				{
					m_S3.GetWindowRect(&olRect);
					olBrush.DeleteObject();
					olBrush.CreateSolidBrush(COLORREF(BLUE));
					olStep = "3";
				}
				break;
			}
		case 3:
			{
				csCode = ogPrivList.GetStat("DUTYRELEASEDLG_IDC_PLAN4");
				if ((csCode=='1' || csCode=='0') && bmShow4)
				{
					m_S4.GetWindowRect(&olRect);
					olBrush.DeleteObject();
					olBrush.CreateSolidBrush(COLORREF(PURPLE));
					olStep = "4";
				}
				break;
			}
		case 4:
			{
				csCode = ogPrivList.GetStat("DUTYRELEASEDLG_IDC_PLAN6");
				if ((csCode=='1' || csCode=='0') && bmShow5)
				{
					m_S5.GetWindowRect(&olRect);
					olBrush.DeleteObject();
					olBrush.CreateSolidBrush(COLORREF(ORANGE));
					olStep = "5";
				}
				break;
			}
		case 5:
			{
				csCode = ogPrivList.GetStat("DUTYRELEASEDLG_IDC_PLAN5");
				if ((csCode=='1' || csCode=='0') && bmShowL)
				{
					m_SL.GetWindowRect(&olRect);
					olBrush.DeleteObject();
					olBrush.CreateSolidBrush(COLORREF(FUCHSIA));
					olStep = "L";
				}
				break;
			}
		}
		CRect olOrgRect = olRect;
		if (olRect.Width())
		{
			ScreenToClient(&olRect);
			dc.SelectObject(&olBrush);
			dc.Rectangle(olRect); //Ellipse,Rectangle
		}

		if(olStep == omStepBeforAbsenceLevel && bmShowU && blSet)
		{

			m_SU.GetWindowRect(&olRect);
			olRect.bottom = olOrgRect.bottom;
			olRect.top    = olOrgRect.top;

			olBrush.DeleteObject();
			olBrush.CreateSolidBrush(COLORREF(RED));

			ScreenToClient(&olRect);
			dc.SelectObject(&olBrush);
			dc.Rectangle(olRect); //Ellipse,Rectangle
			blSet = false;
		}
	}
}

//---------------------------------------------------------------------------

BOOL DutyReleaseDlg::OnInitDialog() 
{
CCS_TRY;
	CDialog::OnInitDialog();

	// Wartedialog anzeigen
	CWaitDlg olWaitDlg(this,LoadStg(WAIT_DATA_PREPARE));

	HICON m_hIcon = AfxGetApp()->LoadIcon(IDR_ROSTERTYPE);
	SetIcon(m_hIcon, FALSE);

	// Statusbar und Progressbar
	pomStatusBar = new UDlgStatusBar(this, 10815, UDSB_SS_SUNKEN);//UDSB_SS_HIGH
	pomStatusBar->SetStaticText(1, LoadStg(IDS_STRING1459));

	imIDProgress = pomStatusBar->SetProgress(0, 2 ,200);
	imIDStatic   = pomStatusBar->SetStatic("", 3, 190);

	// Die farbigen K�stchen
	m_S1.ShowWindow(SW_HIDE);
	m_S2.ShowWindow(SW_HIDE);
	m_S3.ShowWindow(SW_HIDE);
	m_S4.ShowWindow(SW_HIDE);
	m_S5.ShowWindow(SW_HIDE);
	m_SL.ShowWindow(SW_HIDE);
	m_SU.ShowWindow(SW_HIDE);
	
	// Zeiten eintragen
	m_SDayFrom.SetWindowText(prmViewInfo->oDateFrom.Format("%d.%m.%Y"));
	m_SDayTo.SetWindowText(prmViewInfo->oDateTo.Format("%d.%m.%Y"));
	m_DayFrom.SetTypeToDate(true);
	m_DayFrom.SetTextErrColor(RED);
	m_DayFrom.SetBKColor(LTYELLOW);
	m_DayFrom.SetInitText(prmViewInfo->oDateFrom.Format("%d.%m.%Y"));
	m_DayTo.SetTypeToDate(false);
	m_DayTo.SetTextErrColor(RED);

	// Buttontexte setzen
	SetDlgItemText(IDC_B_FIND,LoadStg(IDS_STRING1817));
	SetDlgItemText(IDOK,LoadStg(IDS_STRING426));
	SetDlgItemText(IDC_ALL,LoadStg(IDS_S_All));
	SetDlgItemText(IDC_NOTHING,LoadStg(IDS_S_NoOne));
	SetDlgItemText(IDCANCEL,LoadStg(ID_M_BEENDEN));
	SetDlgItemText(IDC_SHOWJOBS,LoadStg(IDS_STRING99));
	
	SetWindowText(LoadStg(IDS_STRING426));

	// Grid initialisieren
	IniGrid();
	SortGrid(2);

	// Anzeigen der Planungsstufen je nach Parameter (partab);
	ShowSteps();

	// Statictexte setzen
	SetDlgItemText(IDC_MAXTIME,LoadStg(IDC_MAXTIME));
	SetDlgItemText(IDC_TIME,LoadStg(IDC_TIME));
	SetDlgItemText(IDC_S_PSfreigeben,LoadStg(IDC_S_PSfreigeben2));

	SetDlgItemText(IDC_PLAN1,LoadStg(IDS_STRING405));
	SetDlgItemText(IDC_PLAN2,LoadStg(IDS_STRING412));
	SetDlgItemText(IDC_PLAN3,LoadStg(IDS_STRING421));
	SetDlgItemText(IDC_PLAN4,LoadStg(IDS_STRING422));
	SetDlgItemText(IDC_PLAN6,LoadStg(IDS_STRING423));
	SetDlgItemText(IDC_PLANL,LoadStg(IDC_PLANLU));

	/* CString olCustomer = ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER");
	if (olCustomer != "HAJ")
	{
		CString olStepWithAbsenceLevelText;
		GetDlgItemText(imStepWithAbsenceLevel,olStepWithAbsenceLevelText);
		olStepWithAbsenceLevelText = LoadStg(IDS_STRING1680) + CString(" && ") + olStepWithAbsenceLevelText;
		SetDlgItemText(imStepWithAbsenceLevel,olStepWithAbsenceLevelText);
	}*/

	SetDlgItemText(IDC_S_REL_AS,LoadStg(IDS_RELEASE_AS));
	SetDlgItemText(IDC_REL_NIGHT,LoadStg(IDS_ASNIGHTJOB));
	SetDlgItemText(IDC_REL_NOW,LoadStg(IDS_NOW));

	
	// Art der Freigabe an den RELDPL
	m_RelNight.SetCheck(0);
	m_RelNow.SetCheck(1);
//	m_Plan2.SetCheck(1);

	CString csCode;
	csCode = ogPrivList.GetStat("IDC_PLAN1");
	
	if(csCode=='1') 
		m_Plan1.EnableWindow(true);
	
	if(csCode=='0' || csCode=='-')
		m_Plan1.EnableWindow(false);
	

	return TRUE;
CCS_CATCH_ALL;
return FALSE;
}

//**********************************************************************************************************************
//
//**********************************************************************************************************************

void DutyReleaseDlg::OnOK() 
{
	CCS_TRY;
	bool ilStatus = true;
	CString olTmp;
	CString	olErrorText;
	CString olFrom,olTo;
	CTime	olTmpFrom,olTmpTo;
	COleDateTime olFromDay,olToDay;
	CString olFromType,olToType;
	CString olDateFrom,olDateTo; //send with Release
	
	//-----------------------------------------------------------------------------------------
	//Check all User input
	//-----------------------------------------------------------------------------------------
	if(m_DayFrom.GetStatus() == false || m_DayTo.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING1571);
	}
	else
	{
		m_DayFrom.GetWindowText(olFrom);
		m_DayTo.GetWindowText(olTo);
		if (olTo.IsEmpty())
		{
			olTo = olFrom;
		}
		olTmp = "00:00";
		olTmpFrom = DateHourStringToDate(olFrom,olTmp);
		olTmpTo   = DateHourStringToDate(olTo,olTmp);
		if (olTmpTo < olTmpFrom)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING1571);
		}
		else
		{
			olFromDay.SetDate(olTmpFrom.GetYear(),olTmpFrom.GetMonth(),olTmpFrom.GetDay());
			olToDay.SetDate(olTmpTo.GetYear(),olTmpTo.GetMonth(),olTmpTo.GetDay());
			olDateFrom = olFromDay.Format("%Y%m%d");
			olDateTo   = olToDay.Format("%Y%m%d");
			if (olDateFrom < prmViewInfo->oDateFrom.Format("%Y%m%d") || olDateTo > prmViewInfo->oDateTo.Format("%Y%m%d"))
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING1571);
			}
		}
	}

	GetSelectedItems();
	if (omUrnoList.GetCount() < 1)
	{
		olErrorText += LoadStg(IDS_STRING1573);
		ilStatus = false;
	}
	bool blOverwriteExistingData = true; 
	CString olMessageText;
	CString olTmp2;
	if(ilStatus == true)
	{
		if(m_Plan1.GetCheck() == 1)
		{
			olFromType = "1";

			CString olTmpMessageText;
			blOverwriteExistingData = true;
			olTmp = LoadStg(IDS_STRING1586);
			olTmp.MakeUpper();
			olTmp2 = LoadStg(IDS_STRING1587);
			olTmp2.MakeUpper();
			olTmpMessageText.Format(LoadStg(IDS_STRING1576), olTmp, olTmp2);
			olMessageText += olTmpMessageText;
		}
		else if(m_Plan2.GetCheck() == 1)
		{
			olFromType = "2";
			olTmp = LoadStg(IDS_STRING1587);
			olTmp.MakeUpper();
			olTmp2 = LoadStg(IDS_STRING1588);
			olTmp2.MakeUpper();
			olMessageText.Format(LoadStg(IDS_STRING1576), olTmp, olTmp2);
		}
		else if(m_Plan3.GetCheck() == 1)
		{
			olFromType = "3";
			olTmp = LoadStg(IDS_STRING1588);
			olTmp.MakeUpper();
			olTmp2 = LoadStg(IDS_STRING1589);
			olTmp2.MakeUpper();
			olMessageText.Format(LoadStg(IDS_STRING1576), olTmp, olTmp2);
		}
		else if(m_Plan4.GetCheck() == 1) 
		{
			olFromType = "4";	
			olTmp = LoadStg(IDS_STRING1589);
			olTmp.MakeUpper();
			olMessageText.Format(LoadStg(IDS_STRING1601), olTmp);
		}
		else if(m_PlanL.GetCheck() == 1) // Freigabe des Langzeitdienstplan und Urlaubsplan
		{
			olFromType = "L";	
			CString olTmpMessageText;
			blOverwriteExistingData = true;
			olTmp = LoadStg(IDS_STRING1876);
			olTmp.MakeUpper();
			olTmp2 = LoadStg(IDS_STRING1587);
			olTmp2.MakeUpper();
			olMessageText.Format(LoadStg(IDS_STRING1576), olTmp, olTmp2);
		}
		else 
		{
			olErrorText += LoadStg(IDS_STRING1572);
			ilStatus = false;
		}
	}
	//-----------------------------------------------------------------------------------------
	//END - Check all User input
	//-----------------------------------------------------------------------------------------
	
	if(ilStatus != true)
	{
		// Fehlermeldung ausgeben
		MessageBeep((UINT)-1);
		MessageBox(olErrorText,LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
		return;
	}
	
	int ilMBReturn = 0;
	if (blOverwriteExistingData)
	{
		ilMBReturn = MessageBox(olMessageText, LoadStg(ST_HINWEIS)+CString("/")+LoadStg(ST_FRAGE), (MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2));
	}
	else
	{
		ilMBReturn = MessageBox(olMessageText, LoadStg(ST_FRAGE), (MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2));
	}
	
	if(ilMBReturn != IDYES)
		return;
	
	AfxGetApp()->DoWaitCursor(1);

	//***********************************************
	// RDPHDL freigabe
	//***********************************************
	CString olGeneralInfos;
	CString olStfus;
	CString olTempStfu;
	CString olReleaseString;
	POSITION olPos;
	int ilCountStfus = 0;
	
	// Add User
	olGeneralInfos += CString(pcgUser) + CString("|");
	// Set Release of Dutyroster
	olGeneralInfos += CString("D|");
	// Add Homearport
	olGeneralInfos += CString(pcgHome) + CString("|");
	// Add Level
	olGeneralInfos += olFromType + CString("|");
	// Add Periode (YYYYMMDD-YYYYMMDD)
	olGeneralInfos +=  olDateFrom + CString("-") + olDateTo + CString("|");

	// Add Staff-Urno list nnn-nnn-nnn-nnn- ....
	for (olPos = omUrnoList.GetHeadPosition(); olPos != NULL;)
	{
		ilCountStfus++;
		olTempStfu.Format("-%ld", omUrnoList.GetNext(olPos));
		olStfus += olTempStfu;

		if ((ilCountStfus % 700) == 0)
		{
			olStfus = olStfus.Mid(1);
			olReleaseString = olGeneralInfos + olStfus;
			DoRelease(olReleaseString, olFromType);
			olStfus = "";
		}
	}

	if (olStfus.GetLength() > 0)
	{
		olStfus = olStfus.Mid(1);
		olReleaseString = olGeneralInfos + olStfus;
		DoRelease(olReleaseString, olFromType);
	}

	RemoveWaitCursor();

	CCS_CATCH_ALL;
}

//******************************************************************************************************
//
//******************************************************************************************************

void DutyReleaseDlg::InternalCancel()
{
CCS_TRY;
	CDialog::OnCancel();
CCS_CATCH_ALL;
}

//******************************************************************************************************
//
//******************************************************************************************************

void DutyReleaseDlg::OnCancel() 
{
CCS_TRY;
	// Grid beenden
	pomGrid->DestroyWindow() ;
	CDialog::OnCancel();
CCS_CATCH_ALL;
}

//******************************************************************************************************
//
//******************************************************************************************************

void DutyReleaseDlg::OnBAll() 
{
CCS_TRY;
	POSITION area = pomGrid->GetParam( )->GetRangeList( )->AddTail(new CGXRange);
	pomGrid->SetSelection(area,1, 1, pomStfData->GetSize(),RELEASECOLCOUNT-1);
CCS_CATCH_ALL;
}

//******************************************************************************************************
//
//******************************************************************************************************

void DutyReleaseDlg::OnBNothing() 
{
CCS_TRY;
	pomGrid->SetSelection(NULL,0,0,0,0);
CCS_CATCH_ALL;
}

//******************************************************************************************************
//
//******************************************************************************************************

void DutyReleaseDlg::GetSelectedItems()
{
CCS_TRY;

	CGXStyle		olStyle;
	long			ilUrno;
	CString			olUrno;
	CRowColArray	olRowColArray;

	// Mitarbeiter-Urno-Liste l�schen
	omUrnoList.RemoveAll();
		
	// Selektion ausw�hlen
	pomGrid->GetSelectedRows (olRowColArray,false,false);

	// Wurden Zeilen ausgew�hlt
	if (olRowColArray.GetSize() == 0)
	{
		TRACE("Keine Zeilen ausgew�hlt.\n");
		return;
	}
	
	// F�r jede ausgew�hlte Zeile abarbeiten
	for (int i=0;i<olRowColArray.GetSize();i++)
	{
		// Style und damit Datenpointer einer Zelle erhalten
		pomGrid->ComposeStyleRowCol(olRowColArray[i],1, &olStyle );
		// Urno erhalten
		ilUrno = (long)olStyle.GetItemDataPtr();
		if (!ilUrno)
		{
			TRACE ("Urno=0 in DutyReleaseDlg f�r Zeile%d\n",olRowColArray[i]);
		}
		else
		{
			// hinzuf�gen
			omUrnoList.AddTail(ilUrno);
		}
	}
CCS_CATCH_ALL;
}

//**********************************************************************************
// Grid initialisieren
//**********************************************************************************

void DutyReleaseDlg::IniGrid ()
{
	// Grid erzeugen
	pomGrid = new CGridControl ( this, IDC_GRID,RELEASECOLCOUNT-1,pomStfData->GetSize());
	// �berschriften setzen
	pomGrid->SetValueRange(CGXRange(0,1), LoadStg(SHIFT_STF_PENO));
	pomGrid->SetValueRange(CGXRange(0,2), LoadStg(SHIFT_S_STAFF));
	pomGrid->SetValueRange(CGXRange(0,3), LoadStg(SHIFT_STF_FUNCTION));
	pomGrid->SetValueRange(CGXRange(0,4), LoadStg(IDS_STRING139));
	pomGrid->SetValueRange(CGXRange(0,5), LoadStg(IDS_STRING140));
	
	// Breite der Spalten einstellen
	IniColWidths ();

	// Grid mit Daten f�llen.
	FillGrid();

	pomGrid->Initialize();
	pomGrid->GetParam()->EnableUndo(FALSE);
	pomGrid->LockUpdate(TRUE);
	pomGrid->LockUpdate(FALSE);
	pomGrid->GetParam()->EnableUndo(TRUE);
	
	// Verhindert, das die Spaltenbreite ver�ndert wird
	pomGrid->GetParam()->EnableTrackColWidth(FALSE);

	// Es k�nnen nur ganze Zeilen markiert werden.
	pomGrid->GetParam()->EnableSelection(GX_SELROW | GX_SELMULTIPLE | GX_SELSHIFT);

	pomGrid->SetStyleRange( CGXRange().SetTable(), CGXStyle()
				.SetVerticalAlignment(DT_VCENTER)
				.SetReadOnly(TRUE));
	pomGrid->EnableAutoGrow ( FALSE );
	pomGrid->SetLbDblClickAction ( WM_COMMAND, IDC_B_MOVETO);
}


//**********************************************************************************
// Grid mit Daten f�llen
//**********************************************************************************

void DutyReleaseDlg::FillGrid()
{
	CString	csFullName;
	CString csUrno;
	long	ilUrno;

	CString *prlSurn = NULL;
	CString *prlResult;
	CString olSurn;
	CString olTmp;

	// Get all valid Functions
	COleDateTime olReferenceDate = pomParent->rmViewInfo.oDateFrom;
	SPFDATA *polSpfData = NULL;
	CMapStringToPtr olSpfMap;
	int ilSpfCount =  ogSpfData.omData.GetSize();

	for (int iSpf = 0; iSpf < ilSpfCount; iSpf++)
	{
		polSpfData = &ogSpfData.omData[iSpf];

		// check valid from and valid to
		if (polSpfData->Vpfr <= olReferenceDate && 
		   (polSpfData->Vpto.GetStatus() == COleDateTime::null || 
		    polSpfData->Vpto.GetStatus() == COleDateTime::invalid || 
			polSpfData->Vpto >= olReferenceDate))
		{
			olSurn.Format("%ld",polSpfData->Surn);
			if (olSpfMap.Lookup(olSurn, (void *&)prlSurn) == TRUE)
			{
				if (polSpfData->Prio == "1")
				{
					olTmp = *prlSurn;
					*prlSurn = (CString)polSpfData->Fctc + "," + olTmp;
				}
				else
				{
					*prlSurn +=  "," + (CString)polSpfData->Fctc;
				}
			}
			else
			{
				CString *polCode = new CString;
				*polCode = (CString)polSpfData->Fctc;
				olSpfMap.SetAt(olSurn, polCode);
			}
		}
	}
	// END of Get all Functions *******************************************

	for (int i = 0; i < pomStfData->GetSize(); i++)
	{
		// Urno auslesen
		ilUrno = (*pomStfData)[i].Urno;
		csUrno.Format("%ld",ilUrno);
		// Urno mit erstem Feld koppeln.						
		pomGrid->SetStyleRange ( CGXRange(i+1,1), CGXStyle().SetItemDataPtr( (void*)ilUrno ) );
		pomGrid->SetValueRange(CGXRange(i+1,1), (*pomStfData)[i].Peno);
		
		// Name und Vorname
		csFullName = CBasicData::GetFormatedEmployeeName(prmViewInfo->iEName, &(*pomStfData)[i], "", "");
		pomGrid->SetValueRange(CGXRange(i+1,2), csFullName);

		// Wenn die Urno des aktuellen Mitarbeiters in der FUNCTIONMAP gefunden wurde 
		if(olSpfMap.Lookup(csUrno,(void *&)prlResult) == TRUE)
		{
			// Liste der Functions eintragen;
			pomGrid->SetValueRange(CGXRange(i+1,3), *prlResult);
		}

		pomGrid->SetValueRange(CGXRange(i+1,4), (*pomStfData)[i].Doem.Format("%d.%m.%Y"));
		pomGrid->SetValueRange(CGXRange(i+1,5), (*pomStfData)[i].Dodm.Format("%d.%m.%Y"));
	}

//Delete Maps
	CString *prlString;
	CString olText;
	for(POSITION olPos = olSpfMap.GetStartPosition(); olPos != NULL; )
	{
		olSpfMap.GetNextAssoc(olPos,olText,(void*&)prlString);
		delete prlString;
	}
	olSpfMap.RemoveAll();
}

//**********************************************************************************
// Grid initialisieren, Breite richtet sich nach Anzahl der Buchstaben im Titel
//**********************************************************************************

void DutyReleaseDlg::IniColWidths ()
{
	CString olCaption;
	int ilFactor = 7;

	pomGrid->SetColWidth ( 0, 0, 35);

	olCaption = LoadStg(SHIFT_STF_PENO);
	pomGrid->SetColWidth ( 1, 1, max(105,olCaption.GetLength()*ilFactor));
	
	olCaption = LoadStg(SHIFT_S_STAFF);
	pomGrid->SetColWidth ( 2, 2, max(210,olCaption.GetLength()*ilFactor));

	olCaption = LoadStg(SHIFT_STF_FUNCTION);
	pomGrid->SetColWidth ( 3, 3, max(140,olCaption.GetLength()*ilFactor));

	olCaption = LoadStg(IDS_STRING139);
	pomGrid->SetColWidth ( 4, 4, max(70,olCaption.GetLength()*ilFactor));

	olCaption = LoadStg(IDS_STRING140);
	pomGrid->SetColWidth ( 5, 5, max(70,olCaption.GetLength()*ilFactor));
}

//**********************************************************************************
// Sortierung des Grids "per Hand"
//**********************************************************************************

bool DutyReleaseDlg::SortGrid(int ipRow)
{
	CGXSortInfoArray  sortInfo;
	sortInfo.SetSize (1);
	sortInfo[0].sortOrder = CGXSortInfo::ascending;
	sortInfo[0].nRC = ipRow;                       
	sortInfo[0].sortType = CGXSortInfo::autodetect;  
	pomGrid->SortRows( CGXRange().SetRows(1, pomStfData->GetSize()), sortInfo); 
	return (true);
}

//**********************************************************************************
// Suchen Dialog aufrufen
//**********************************************************************************

void DutyReleaseDlg::OnBFind() 
{
	if (pomGrid)
	{
		pomGrid->OnShowFindReplaceDialog(TRUE);
	}
}

//**********************************************************************************
// Maximale Endzeit einstellen
//**********************************************************************************

void DutyReleaseDlg::OnBMax() 
{
	CString olMaxDate;
	m_SDayTo.GetWindowText(olMaxDate);
	m_DayTo.SetInitText(olMaxDate);
}

//**********************************************************************************
// Minimale Endzeit einstellen
//**********************************************************************************

void DutyReleaseDlg::OnBMin() 
{
	CString olMinDate;
	m_SDayFrom.GetWindowText(olMinDate);
	m_DayTo.SetInitText(olMinDate);
}


//*******************************************************************************
// Zeigt nur die Planungstufen an, die in der Parametern festgelegt wurden
//*******************************************************************************

void DutyReleaseDlg::ShowSteps()
{
	bmShow1=bmShow2=bmShow3=bmShow4=bmShow5=bmShowL=bmShowU=false;

	// Werte aus der Parameterklasse einlesen
	CString olStepList = ogCCSParam.GetParamValue(ogAppl,"ID_SHOW_STEPS");

	// In Array �berf�hren
	CStringArray olStepListStrArray;
	int ilArray = ExtractItemList(olStepList,&olStepListStrArray, '|');
	CString olStepBeforAbsenceLevel;
	UINT ilStepWithAbsenceLevel = 0;
	for (int i=0;i<ilArray;i++)
	{
		CString olTmp = olStepListStrArray.GetAt(i);

		if(olTmp != "U")
			olStepBeforAbsenceLevel = olTmp;

		if (olTmp == "1")
		{
			ilStepWithAbsenceLevel = IDC_PLAN1;
			bmShow1 = true;
		}
		else if (olTmp == "L")
		{
			ilStepWithAbsenceLevel = IDC_PLANL;
			bmShowL = true;
		}
		else if (olTmp == "2")
		{
			ilStepWithAbsenceLevel = IDC_PLAN2;
			bmShow2 = true;
		}
		else if (olTmp == "3")
		{
			bmShow3 = true;
		}
		else if (olTmp == "4")
		{
			bmShow4 = true;
		}
		else if (olTmp == "5")
		{
			bmShow5 = true;
		}
		else if (olTmp == "U")
		{
			// Je nach dem, hinter welscher Stufe die Urlaubsstufe folgt
			// mu� Urlaubsstufe zugeordnet werden
			/*imStepWithAbsenceLevel = ilStepWithAbsenceLevel;
			omStepBeforAbsenceLevel = olStepBeforAbsenceLevel;
			CString olCustomer = ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER");
			if (olCustomer == "HAJ")
			{
				bmShowU = false;
			}
			else
			{
				bmShowU = true;
			}*/
			bmShowU = false;
		}
	}
	olStepListStrArray.RemoveAll();

	
	// Oberfl�chen Elemente entsprechend der Auswertung setzen
	if (!bmShow1) m_Plan1.ShowWindow(SW_HIDE);
	if (!bmShowL) m_PlanL.ShowWindow(SW_HIDE);
	if (!bmShow2) m_Plan2.ShowWindow(SW_HIDE);
	if (!bmShow3) m_Plan3.ShowWindow(SW_HIDE);
	if (!bmShow4) m_Plan4.ShowWindow(SW_HIDE);
	if (!bmShow5) GetDlgItem(IDC_PLAN6)->ShowWindow(SW_HIDE);//Stufe wird zur Zeit nur angezeigt

	////////////////////////////////////////////////////
	// �berpr�fung der Rechte der Oberfl�chenelemente //
	////////////////////////////////////////////////////

	CString csCode;
	csCode = ogPrivList.GetStat("DUTYRELEASEDLG_IDC_PLAN1");
	bool blLevel1 = false;
	if(csCode=='1' || csCode==' ')
	{
		m_Plan1.EnableWindow(true);
		m_Plan1.SetCheck(1);
		blLevel1 = true;
	}
	else if(csCode=='0')
	{
		m_Plan1.EnableWindow(false);
		m_Plan1.SetCheck(0);
	}
	else if(csCode=='-')
	{
		m_Plan1.ShowWindow(SW_HIDE);
		m_S1.ShowWindow(SW_HIDE);
	}

	csCode = ogPrivList.GetStat("DUTYRELEASEDLG_IDC_PLAN2");
	if(csCode=='1' || csCode==' ')
	{
		m_Plan2.EnableWindow(true);
		if (!blLevel1)
			m_Plan2.SetCheck(1);
	}
	else if(csCode=='0')
	{
		m_Plan2.EnableWindow(false);
		m_Plan2.SetCheck(0);
	}
	else if(csCode=='-')
	{
			m_Plan2.ShowWindow(SW_HIDE);
			m_S2.ShowWindow(SW_HIDE);
	}

	csCode = ogPrivList.GetStat("DUTYRELEASEDLG_IDC_PLAN3");
	if(csCode=='1' || csCode==' ')
	{
		m_Plan3.EnableWindow(true);
	}
	else if(csCode=='0')
	{
		m_Plan3.EnableWindow(false);
	}
	else if(csCode=='-')
	{
		m_Plan3.ShowWindow(SW_HIDE);
		m_S3.ShowWindow(SW_HIDE);
	}

	csCode = ogPrivList.GetStat("DUTYRELEASEDLG_IDC_PLAN4");
	if(csCode=='1' || csCode==' ')
	{
		m_Plan4.EnableWindow(true);
	}
	else if(csCode=='0')
	{
		m_Plan4.EnableWindow(false);
	}
	else if(csCode=='-')
	{
		m_Plan4.ShowWindow(SW_HIDE);
		m_S4.ShowWindow(SW_HIDE);
	}

	csCode = ogPrivList.GetStat("DUTYRELEASEDLG_IDC_PLAN5");
	if(csCode=='1' || csCode==' ')
	{
		m_PlanL.EnableWindow(true);
	}
	else if(csCode=='0' || csCode==' ')
	{
		m_PlanL.EnableWindow(false);
	}
	else if(csCode=='-')
	{
		m_PlanL.ShowWindow(SW_HIDE);
		m_SL.ShowWindow(SW_HIDE);
	}

	csCode = ogPrivList.GetStat("DUTYRELEASEDLG_IDC_PLAN6");
	if(csCode=='1' || csCode==' ')
	{
		GetDlgItem(IDC_PLAN6)->EnableWindow(true);
	}
	else if(csCode=='0')
	{
		GetDlgItem(IDC_PLAN6)->EnableWindow(false);
	}
	else if(csCode=='-')
	{
		GetDlgItem(IDC_PLAN6)->ShowWindow(SW_HIDE);
		m_S5.ShowWindow(SW_HIDE);
	}

	csCode = ogPrivList.GetStat("DUTYROSTER2PS_IDC_NOW");
	if(csCode=='1' || csCode==' ')
	{
		m_RelNow.EnableWindow(true);
	}
	else if(csCode=='0' || csCode=='-')
	{
		m_RelNow.EnableWindow(false);
	}
	else if(csCode=='-')
	{
		m_RelNow.ShowWindow(SW_HIDE);
	}

	csCode = ogPrivList.GetStat("DUTYROSTER2PS_IDC_REL_NIGHT");
	if(csCode=='1' || csCode==' ')
	{
		m_RelNight.EnableWindow(true);
	}
	else if(csCode=='0')
	{
		m_RelNight.EnableWindow(false);
	}
	else if(csCode=='-')
	{
		m_RelNight.ShowWindow(SW_HIDE);
	}

	if((ogPrivList.GetStat("DUTYROSTER2PS_IDC_REL_NIGHT") == '0' || ogPrivList.GetStat("DUTYROSTER2PS_IDC_REL_NIGHT") =='-') && (ogPrivList.GetStat("DUTYROSTER2PS_IDC_NOW") == '0' || ogPrivList.GetStat("DUTYROSTER2PS_IDC_NOW") =='-'))
	{
		GetDlgItem(IDOK)->EnableWindow(false);
		GetDlgItem(IDC_S_REL_AS)->ShowWindow(SW_HIDE);
	}

	if(ogPrivList.GetStat("DUTYRELEASEDLG_UNRELEASE") != '1')
	{
		int ilMoveRight = 60;
		GetDlgItem(ID_UNRELEASE)->ShowWindow(SW_HIDE);
		CRect olRect;
		GetDlgItem(IDOK)->GetWindowRect(olRect);
		ScreenToClient (olRect);
		GetDlgItem(IDOK)->MoveWindow(olRect.left+ilMoveRight, olRect.top, olRect.Width(), olRect.Height());
	}

	if((ogPrivList.GetStat("DUTYRELEASEDLG_IDC_PLAN2") != '1' && ogPrivList.GetStat("DUTYRELEASEDLG_IDC_PLAN3") == '1' ))
	{
		CheckRadioButton(IDC_PLAN2,IDC_PLAN3,IDC_PLAN3);
	}

	if((ogPrivList.GetStat("DUTYRELEASEDLG_IDC_PLAN2") != '1' && ogPrivList.GetStat("DUTYRELEASEDLG_IDC_PLAN3") != '1' && ogPrivList.GetStat("DUTYRELEASEDLG_IDC_PLAN4") == '1' ))
	{
		CheckRadioButton(IDC_PLAN3,IDC_PLAN4,IDC_PLAN4);
	}

	if((ogPrivList.GetStat("DUTYRELEASEDLG_IDC_PLAN2") != '1' && ogPrivList.GetStat("DUTYRELEASEDLG_IDC_PLAN3") != '1' && ogPrivList.GetStat("DUTYRELEASEDLG_IDC_PLAN4") != '1' && ogPrivList.GetStat("DUTYRELEASEDLG_IDC_PLAN6") == '1' ))
	{
		CheckRadioButton(IDC_PLAN4,IDC_PLAN6,IDC_PLAN6);
	}
}


//***************************************************************************
// Zeigt den Dialog mit Freigabe-Jobs
//***************************************************************************
void DutyReleaseDlg::OnShowJobs() 
{
	DutyShowJobsDlg olDutyShowJobsDlg(this);
	olDutyShowJobsDlg.DoModal();
}

//***************************************************************************
// Starts release
//***************************************************************************
void DutyReleaseDlg::DoRelease(CString opReleaseString, CString opFromType)
{
	CedaRelData olRelData;
	olRelData.MakeCedaString(opReleaseString);
	opReleaseString.Replace('\227', '\174');
	char *pclReleaseString = (char*) malloc(sizeof(char) + opReleaseString.GetLength()+2);

	if (pclReleaseString != NULL)
	{
		strcpy(pclReleaseString, opReleaseString);
		
		if (m_RelNight.GetCheck() == 1)
		{
			olRelData.CedaAction("RDL","NOBC,NOACTION,NOLOG", "", pclReleaseString, "");
		}
		else
		{
			olRelData.CedaAction("RDN","NOBC,NOACTION,NOLOG", "", pclReleaseString, "");
		}
		free(pclReleaseString);
	}
}

void DutyReleaseDlg::OnUnrelease() 
{
	CCS_TRY;
	bool ilStatus = true;
	CString olTmp;
	CString	olErrorText;
	CString olFrom,olTo;
	CTime	olTmpFrom,olTmpTo;
	COleDateTime olFromDay,olToDay;
	CString olFromType,olToType;
	CString olDateFrom,olDateTo; //send with Release

	//-----------------------------------------------------------------------------------------
	//Check all User input
	//-----------------------------------------------------------------------------------------
	if(m_DayFrom.GetStatus() == false || m_DayTo.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING1571);
	}
	else
	{
		m_DayFrom.GetWindowText(olFrom);
		m_DayTo.GetWindowText(olTo);
		if (olTo.IsEmpty())
		{
			olTo = olFrom;
		}
		olTmp = "00:00";
		olTmpFrom = DateHourStringToDate(olFrom,olTmp);
		olTmpTo   = DateHourStringToDate(olTo,olTmp);
		if (olTmpTo < olTmpFrom)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING1571);
		}
		else
		{
			olFromDay.SetDate(olTmpFrom.GetYear(),olTmpFrom.GetMonth(),olTmpFrom.GetDay());
			olToDay.SetDate(olTmpTo.GetYear(),olTmpTo.GetMonth(),olTmpTo.GetDay());
			olDateFrom = olFromDay.Format("%Y%m%d");
			olDateTo   = olToDay.Format("%Y%m%d");
			if (olDateFrom < prmViewInfo->oDateFrom.Format("%Y%m%d") || olDateTo > prmViewInfo->oDateTo.Format("%Y%m%d"))
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING1571);
			}
		}
	}

	if (m_Plan1.GetCheck() == 1)
	{
		olTmp = LoadStg(IDS_STRING1586);
		olTmp.MakeUpper();
		olFromType = "1";
	}
	else if (m_Plan2.GetCheck() == 1)
	{
		olTmp = LoadStg(IDS_STRING1587);
		olTmp.MakeUpper();
		olFromType = "2";
	}
	else if (m_Plan3.GetCheck() == 1)
	{
		olTmp = LoadStg(IDS_STRING1588);
		olTmp.MakeUpper();
		olFromType = "3";
	}
	else if (m_Plan4.GetCheck() == 1) 
	{
		olTmp = LoadStg(IDS_STRING1589);
		olTmp.MakeUpper();
		olFromType = "4";
	}
	else
	{
		olTmp = "ERROR";
		olFromType = "-1";
	}

	GetSelectedItems();
	if (omUrnoList.GetCount() < 1)
	{
		olErrorText += LoadStg(IDS_STRING1573);
		ilStatus = false;
	}

	CString olMessageText;
	olMessageText.Format(LoadStg(IDS_STRING104), olTmp);
	int ilMBReturn = MessageBox(olMessageText, LoadStg(ST_FRAGE), (MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2));
	
	if (ilMBReturn != IDYES)
	{
		return;
	}

	if(ilStatus == false)
	{
		// Fehlermeldung ausgeben
		MessageBeep((UINT)-1);
		MessageBox(olErrorText,LoadStg(ST_FEHLER),MB_ICONEXCLAMATION);
		return;
	}
	else
	{
		ogDrrData.UnRelease(olFromDay, olToDay, olFromType, &omUrnoList);
	}
	CCS_CATCH_ALL;
}
