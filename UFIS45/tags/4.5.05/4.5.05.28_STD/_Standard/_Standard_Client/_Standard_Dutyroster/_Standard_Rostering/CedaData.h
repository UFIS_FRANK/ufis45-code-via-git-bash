// CedaData.h: interface for the CedaData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CEDADATA_H__46983EC3_598F_11D5_ACB3_0050DA1CABCB__INCLUDED_)
#define AFX_CEDADATA_H__46983EC3_598F_11D5_ACB3_0050DA1CABCB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <CCSPtrArray.h>
//#include "stdafx.h"

class CedaData : public CCSCedaData
{
 
public:
	
	friend class CCSCedaData;
    CedaData(CCSCedaCom *popCommHandler = NULL);
	//virtual ~CedaData();

	bool Initialize(CString opServerName);

	// bda: makes a formatted string with all fields of a data structure for trace and debug output
	bool GetDataFormatted(CString &pcpListOfData, void *pvpDataStruct); 

	// bda: makes a specialized string for defect data set as in the GetDataFormatted
	bool GetDefectDataString(CString &pcpListOfData, void *pvpDataStruct,
							 LPCTSTR pcpExplanation=0); 
	// Feldliste schreiben
	void SetFieldList(CString opFieldList) {strcpy(pcmListOfFields,opFieldList.GetBuffer(0));}

	// Feldliste lesen
	CString GetFieldList(void) {return CString(pcmListOfFields);}

	// rro: overriding the base-class
    bool CedaAction(char *pcpAction,
        char *pcpSelection = NULL, char *pcpSort = NULL,
        char *pcpData = pcgDataBuf,char *pcpDest = "BUF1",
				bool bpIsWriteAction = false, long lpID = 0);

	bool CedaAction2(char *pcpAction,
					char *pcpSelection = NULL, char *pcpSort = NULL,
					char *pcpData = pcgDataBuf,char *pcpDest = "BUF1",
					bool bpIsWriteAction = false, long lpID = 0, bool bpDelDataArray = true);

    //@ManMemo: Provides CEDA services (e.g. read, update, insert, get URNO etc.)
    //@See: UFIS.DLL
    /*@Doc:
      Call {\bf CallCeda} in {\bf UFIS.DLL} with the appropiate parameters. Insert missing
      parameters from CCSConfig or with default values (see documentation on UFIS.DLL and
      CCSConfig for details). This method uses {\bf pcpData} for update/insert data only.
      If the {\bf pcpAction} is retrieving/deleting record, the {\bf pcpData} will has no
      meaning and can be to anything. On success, retrieve data from the internal buffer
      of UFIS.DLL to create an array of comma-separated string.

      Programmer Notes:
      Right now, the possible {\bf pcpAction} values are RTA, URT, IRT, DRT, and GNU.
      RTA for SQL - SELECT, URT for SQL - UPDATE, IRT for SQL - INSERT, DRT for SQL - DELETE,
      and GNU is a special action of UFIS.DLL to get a URNO (Unique Record Number).

      For the GNU action, you need to initialize some spaces in the {\bf pcpData} buffer.
      (It cannot be an empty string. Normally, it should a 10 character spaces followed by
      a null character. The reason (according to Ralph) is to let the UFIS.DLL know the
      length of the buffer to store the URNO back.

      Return: RCSuccess, RCFailure
    */

    bool CedaAction(char *pcpAction,
        char *pcpTableName, 
		char *pcpFieldList,
        char *pcpSelection, 
		char *pcpSort,
        char *pcpData, 
		char *pcpDest = "BUF1",
		bool bpIsWriteAction = false, 
		long lpID = 0, 
		CCSPtrArray<RecordSet> *pomData = NULL,
		int ipFieldCount = 0);

	// rro/tho: overriding the base class
    bool CedaAction2(char *pcpAction,
        char *pcpTableName, 
		char *pcpFieldList,
        char *pcpSelection, 
		char *pcpSort,
        char *pcpData, 
		char *pcpDest = "BUF1",
		bool bpIsWriteAction = false, 
		long lpID = 0, 
		CCSPtrArray<RecordSet> *pomData = NULL, 
		int ipFieldCount = 0, 
		bool bpDelDataArray = true
		);

	static int ExtractTextLineFast(CStringArray &opItems, char *pcpLineText, char *pcpSepa);

	// speichert die Feldnamen der Tabelle in sequentieller Form mit Trennzeichen Komma
	char pcmListOfFields[1024];

protected:
    bool GetBufferRecord(int ipLineNo, void *pvpDataStruct);
    bool GetBufferRecord(CCSPtrArray<CEDARECINFO> *pomRecInfo, int ipLineNo, void *pvpDataStruct);
	bool GetBufferRecord(CCSPtrArray<CEDARECINFO> *pomRecInfo, CString record, void *pvpDataStruct);
    bool GetBufferRecord(int ipLineNo, void *pvpDataStruct, CString &olFieldList);
    bool GetBufferRecord(CCSPtrArray<CEDARECINFO> *pomRecInfo, int ipLineNo, void *pvpDataStruct, CString &olFieldList);
    bool GetFirstBufferRecord(void *pvpDataStruct);
    bool GetFirstBufferRecord(CCSPtrArray<CEDARECINFO> *pomRecInfo, void *pvpDataStruct);
};

#endif // !defined(AFX_CEDADATA_H__46983EC3_598F_11D5_ACB3_0050DA1CABCB__INCLUDED_)
