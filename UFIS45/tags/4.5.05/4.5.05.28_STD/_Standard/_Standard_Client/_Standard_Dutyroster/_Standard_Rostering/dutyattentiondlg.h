#ifndef AFX_DUTYATTENTIONDLG_H__B2518E51_8842_11D2_8E36_0000C002916B__INCLUDED_
#define AFX_DUTYATTENTIONDLG_H__B2518E51_8842_11D2_8E36_0000C002916B__INCLUDED_

// DutyAttentionDlg.h : Header-Datei
//
#include <stdafx.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld DutyAttentionDlg 

class DutyAttentionDlg : public CDialog
{
// Konstruktion
public:
	DutyAttentionDlg(CWnd* pParent = NULL);   // Standardkonstruktor
	~DutyAttentionDlg();
	// nicht-modalen Dialog erzeugen
	bool Create(void);
	// Eintrag hinzufügen
	void AddAttention(CString opAttention, COLORREF opAttentionColor);

private:
	DutyRoster_View *pomParent;

// Dialogfelddaten
	//{{AFX_DATA(DutyAttentionDlg)
	enum { IDD = IDD_DUTY_ATTENTION_DLG };
	CColorListBox	m_LB_Attention;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(DutyAttentionDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(DutyAttentionDlg)
	afx_msg void On_B_Delete();
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	void ResetAttentionDlg();
	void InternalCancel();
	void UpdateListBox();


};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_DUTYATTENTIONDLG_H__B2518E51_8842_11D2_8E36_0000C002916B__INCLUDED_
