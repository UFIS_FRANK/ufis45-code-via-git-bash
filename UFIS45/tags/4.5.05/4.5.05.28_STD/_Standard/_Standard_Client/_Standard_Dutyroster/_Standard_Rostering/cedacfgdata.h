#ifndef _CCFGD_H_
#define _CCFGD_H_

#include <stdafx.h>
#include <CedaData.h>

#define MON_COUNT_STRING			"MONITORCOUNT"
#define MON_DUTYROSTER_STRING		"DUTYROSTER"
#define MON_SHIFTROSTER_STRING		"SHIFTROSTER"
#define MON_DUTYROSTER_STATICCOLUMNS "STATICCOLUMNS"
#define MON_DUTYROSTER_DEBITROWS	 "DEBITROWS"
#define MON_DUTYROSTER_SHOWLEGEND	"SHOWLEGEND"
#define MON_DUTYROSTER_VIEWPL		"VIEWPLUS"
#define MON_DUTYROSTER_VIEWMI		"VIEWMINUS"
#define MON_DUTYROSTER_VORLAUF		"VIEWVORLAUF"
#define MON_DUTYROSTER_SCROLL		"SCROLLTO"
#define MON_DUTYROSTER_DAYWIDTH		"DAYWIDTH"
#define MON_SORTSTATISTIC			"SORTSTAT"
#define MON_DUTYROSTER_TOTWIDTH		"TOTWIDTH"
#define MON_DUTYROSTER_RELATION		"RELATION"

/////////////////////////////////////////////////////////////////////////////
// Record structure declaration
struct RAW_VIEWDATA
{
	char Ckey[40];
	char Name[100];
	char Type[40];
	char Page[40];
	char Values[2001];
	RAW_VIEWDATA()
	{
		memset(this,'\0',sizeof(*this));
	}
};

struct VIEW_TEXTDATA
{
	CString Page;							// e.g. Rank, Pool, Shift
	CStringArray omValues;					// e.g. Page=Shift: F1,F50,N1 etc.
	VIEW_TEXTDATA& operator =(const VIEW_TEXTDATA &s)
	{
		this->Page = s.Page;
		for(int i = 0; i < s.omValues.GetSize(); i++)		
		{
			this->omValues.Add(s.omValues[i]);
		}
		return *this;
	}
};

struct VIEW_TYPEDATA
{
	CString Type;							// e.g. Filter, Group, Sort
	CCSPtrArray <VIEW_TEXTDATA> omTextData; // necessary only with filters
	CStringArray omValues;					// values only relevant if Type != Filter

	VIEW_TYPEDATA()
	{
		Type = "";
	}
	//Copy constructor
	VIEW_TYPEDATA(VIEW_TYPEDATA& s)
	{
		*this = s;
	}
	VIEW_TYPEDATA& operator =(const VIEW_TYPEDATA &s)
	{
		this->Type = s.Type;
		for(int i = 0; i < s.omValues.GetSize(); i++)		
		{
			this->omValues.Add(s.omValues[i]);
		}
		for(  i = 0; i < s.omTextData.GetSize(); i++)		
		{
			this->omTextData.NewAt(omTextData.GetSize(), s.omTextData[i]);
		}
		return *this;
	}
	~VIEW_TYPEDATA()
	{
		this->omTextData.DeleteAll();
	}
	
};
struct VIEW_VIEWNAMES
{
	CString ViewName;						// e.g. <Default>, Heute, Test etc.
	CCSPtrArray<VIEW_TYPEDATA> omTypeData;
};
struct VIEWDATA
{
	CString Ckey;							// e.g. Staffdia, CCI-Chart etc.
	CCSPtrArray<VIEW_VIEWNAMES> omNameData;
};

struct CFGDATA 
{
    // Data fields from table CFGCKI for whatif-rows
    long    Urno;           // Unique Record Number of CFGCKI
    char    Appn[34];       // name of application
    char    Ctyp[34];        // Type of Row; in Whaif constant string "WHAT-IF"
	char    Ckey[34];		// Name of what-if row
    CTime	Vafr;           // Valid from
    CTime	Vato;           // Valid to
	char	Text[2001];		// Parameter string
	char	Pkno[34];		// Staff-/User-ID
    int	    IsChanged;		// Is changed flag

	CFGDATA(void) 
	{
		memset(this,'\0',sizeof(*this));
		sprintf(Appn,"CCS_%s", ogAppName);
		IsChanged = DATA_UNCHANGED;
	}
};	

typedef struct CFGDATA SETUPDATA;
//typedef struct CfgDataStruct CFGDATA;

// the broadcast CallBack function, has to be outside the CedaCfgData class
void ProcessCfgCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

/////////////////////////////////////////////////////////////////////////////
// Class declaration

class CedaCfgData: public CedaData
{
// Attributes
public:
    CCSPtrArray<CFGDATA> omData;
    CCSPtrArray<CFGDATA> omSetupData;
	CCSPtrArray<VIEWDATA> omViews;
    CMapStringToPtr omCkeyMap;
    CMapPtrToPtr    omUrnoMap;
    CMapPtrToPtr    omSetupUrnoMap;
	CFGDATA			rmMonitorSetup;
// Operations
public:
    CedaCfgData();
    //@ManMemo: Destructor, Unregisters the CedaCfgData object from DataDistribution
	~CedaCfgData();
	
	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
    //@ManMemo: Read all Cfg from database at program start
	bool ReadCfgData();
	int GetMonitorForWindow(CString opWindow);
	//int GetMonitorCount();
	bool ReadMonitorSetup();
	//@ManMemo: Delete a Cfg
	bool ChangeCfgData(CFGDATA *prpCfg);
	//@ManMemo: Delete the Cfg
	bool DeleteCfg(long lpUrno);
	//@ManMemo: Adds a Cfg to omData and to the Maps
	bool AddCfg(CFGDATA *prpCfg);
	//@ManMemo: Makes Database-Actions Insert/Update/Delete
	bool SaveCfg(CFGDATA *prpCfg);	
	//@ManMemo: Insert staff data (const CFGDATA *prpCfgData);    // used in PrePlanTable only

    //@ManMemo: Update cfg data 
    bool UpdateCfg(const CFGDATA *prpCfgData);    // used in PrePlanTable only
	void ProcessCfgBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	bool InsertCfg(const CFGDATA *prpCfgData);
	long  GetUrnoById(char *pclWiid);
	BOOL  GetIdByUrno(long lpUrno,char *pcpWiid);

	CFGDATA  * GetCfgByUrno(long lpUrno);
	//void MakeCurrentUser();
	void ClearAllViews();
	void PrepareViewData(CFGDATA *prpCfg);
	VIEWDATA * FindViewData(CFGDATA *prlCfg);
	VIEW_VIEWNAMES * FindViewNameData(RAW_VIEWDATA *prpRawData);
	VIEW_TYPEDATA * FindViewTypeData(RAW_VIEWDATA *prpRawData);
	VIEW_TEXTDATA * FindViewTextData(RAW_VIEWDATA *prpRawData);
	BOOL MakeRawViewData(CFGDATA *prpCfg, RAW_VIEWDATA * prpRawData);
	void MakeViewValues(char *pspValues, CStringArray &ropValues, char *pcpSepa = "|");
	void UpdateViewForDiagram(CString opDiagram, VIEWDATA &prpView,CString opViewName);
	void DeleteViewFromDiagram(CString opDiagram, CString olView);
private:
    BOOL CfgExist(long lpUrno);
    bool InsertCfgRecord(const CFGDATA *prpCfgData);
    bool UpdateCfgRecord(const CFGDATA *prpCfgData);
};
#endif
