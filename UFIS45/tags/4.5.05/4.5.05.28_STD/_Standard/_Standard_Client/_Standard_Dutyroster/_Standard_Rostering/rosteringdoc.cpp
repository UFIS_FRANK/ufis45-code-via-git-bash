// RosteringDoc.cpp : implementation of the CRosteringDoc class
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//***************************************************************************
// CRosteringDoc
//***************************************************************************

IMPLEMENT_DYNCREATE(CRosteringDoc, CDocument)

BEGIN_MESSAGE_MAP(CRosteringDoc, CDocument)
	//{{AFX_MSG_MAP(CRosteringDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//***************************************************************************
// CRosteringDoc construction
//***************************************************************************

CRosteringDoc::CRosteringDoc()
{
	// TODO: add one-time construction code here
}

//***************************************************************************
// CRosteringDoc destruction
//***************************************************************************

CRosteringDoc::~CRosteringDoc()
{
}

//***************************************************************************
// 
//***************************************************************************

BOOL CRosteringDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}

//***************************************************************************
// CRosteringDoc serialization
//***************************************************************************

void CRosteringDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

//***************************************************************************
// CRosteringDoc diagnostics
//***************************************************************************

#ifdef _DEBUG
void CRosteringDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CRosteringDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

//***************************************************************************
// CRosteringDoc commands
//***************************************************************************

BOOL CRosteringDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	//if (!CDocument::OnOpenDocument(lpszPathName)) return FALSE;
	// TODO: Add your specialized creation code here
	
	return TRUE;
}

//***************************************************************************
// SetTitle: stellt den Titel des Dokuments ein.
// R�ckgabe:	keine
//***************************************************************************

void CRosteringDoc::SetTitle(LPCTSTR lpszTitle) 
{
	// TODO: Add your specialized code here and/or call the base class
	CDocument::SetTitle(lpszTitle);
}

