// PSShiftRoster2ViewPage.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Eigenschaftenseite PSShiftRoster2ViewPage 
//---------------------------------------------------------------------------

IMPLEMENT_DYNCREATE(PSShiftRoster2ViewPage, RosterViewPage)

PSShiftRoster2ViewPage::PSShiftRoster2ViewPage() : RosterViewPage(PSShiftRoster2ViewPage::IDD)
{
	//{{AFX_DATA_INIT(PSShiftRoster2ViewPage)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
	omTitleStrg = LoadStg(SHIFT_STF_FUNCTION);
	m_psp.pszTitle = omTitleStrg;
	m_psp.dwFlags |= PSP_USETITLE;
}

PSShiftRoster2ViewPage::~PSShiftRoster2ViewPage()
{
}

void PSShiftRoster2ViewPage::DoDataExchange(CDataExchange* pDX)
{
	RosterViewPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PSShiftRoster2ViewPage)
	DDX_Control(pDX, IDC_LB_FUNCTIONS,	m_LB_Functions);
	//}}AFX_DATA_MAP
	
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		SetData();
	}
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		GetData();
	}

}


BEGIN_MESSAGE_MAP(PSShiftRoster2ViewPage, RosterViewPage)
	//{{AFX_MSG_MAP(PSShiftRoster2ViewPage)
	ON_BN_CLICKED(IDC_ALL, OnAll)
	ON_BN_CLICKED(IDC_NOTHING, OnNothing)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten PSShiftRoster2ViewPage 
//---------------------------------------------------------------------------

void PSShiftRoster2ViewPage::SetCalledFrom(CString opCalledFrom)
{
	omCalledFrom = opCalledFrom;
}

//---------------------------------------------------------------------------

BOOL PSShiftRoster2ViewPage::OnInitDialog() 
{
	RosterViewPage::OnInitDialog();

	SetDlgItemText(IDC_S_Function,LoadStg(IDS_STRING393));
	SetDlgItemText(IDC_ALL,LoadStg(IDS_S_All));
	SetDlgItemText(IDC_NOTHING,LoadStg(IDS_S_NoOne));

	m_LB_Functions.ResetContent();
	m_LB_Functions.SetFont(&ogCourier_Regular_8);

	CString olLine;
	CString olCode = "---------";
	CString olName = LoadStg(IDS_STRING1878);
	olLine.Format("%-8s   %-40s     URNO=NO_CODE", olCode, olName, 0);
	m_LB_Functions.AddString(olLine);

	// insert functions if allowed
	int ilPfcCount =  ogPfcData.omData.GetSize();
	PFCDATA *prlPfcData = NULL;
	CString olSec;
	bool blAdd;
	for (int i = 0; i < ilPfcCount; i++)
	{
		blAdd = false;
		prlPfcData = &ogPfcData.omData[i];
		
		if (ogCCSParam.GetParamValue("GLOBAL","ID_ORG_SECURITY") == "Y")
		{
			olSec = ogPrivList.GetStat(prlPfcData->Dptc);
			if (olSec != "-" && olSec != "0")
			{
				blAdd = true;
			}
		}
		else
		{
			blAdd = true;
		}

		if (blAdd == true)
		{
			olLine.Format("%-8s   %-40s     URNO=%d",prlPfcData->Fctc, prlPfcData->Fctn, prlPfcData->Urno);
			m_LB_Functions.AddString(olLine);
		}

	}

	return TRUE;
}

//---------------------------------------------------------------------------

BOOL PSShiftRoster2ViewPage::GetData()
{
	omValues.RemoveAll();
	CStringArray olTmpValues;
	if(GetData(olTmpValues) == FALSE)
	{
		return FALSE;
	}
	for(int i = 0; i < olTmpValues.GetSize(); i++)
	{
		CString olTmp = olTmpValues[i];
		omValues.Add(olTmpValues[i]);
	}
	return TRUE;
}

//---------------------------------------------------------------------------

BOOL PSShiftRoster2ViewPage::GetData(CStringArray &opValues)
{
	//*** Get all selected Funktions ****************************************
	int ilMaxItems;
	ilMaxItems = m_LB_Functions.GetSelCount();
	int *pilIndex = new int [ilMaxItems];
	m_LB_Functions.GetSelItems(ilMaxItems, pilIndex );
	CString olTmpTxt, olUrno, olUrnos;
	for(int i=0; i<ilMaxItems; i++)
	{
		m_LB_Functions.GetText(pilIndex[i],olTmpTxt);
		olUrno = olTmpTxt.Mid(olTmpTxt.Find("URNO=")+5);
		olUrnos += olUrno;
		if(i < ilMaxItems-1)
			olUrnos += CString("|");
	}
	delete pilIndex;
	opValues.Add(olUrnos);
	//***end> Get all selected Funktions ************************************

	return TRUE;
}

//---------------------------------------------------------------------------

void PSShiftRoster2ViewPage::SetData()
{
	SetData(omValues);
}

//---------------------------------------------------------------------------

void PSShiftRoster2ViewPage::SetData(CStringArray &opValues)
{
	//*** Set all adjustments to default ************************************
	int ilMaxItems;
	ilMaxItems = m_LB_Functions.GetSelCount();
	int *pilIndex = new int [ilMaxItems];
	m_LB_Functions.GetSelItems(ilMaxItems, pilIndex );
	for(int i=0; i<ilMaxItems; i++)
	{
		m_LB_Functions.SetSel(pilIndex[i],FALSE);
	}
	delete pilIndex;
	//*** end> Set all adjustments to default *******************************

	for(int ilValue = 0; ilValue < opValues.GetSize(); ilValue++)
	{
		switch(ilValue)
		{
			case 0:
			{
				if(opValues[ilValue].IsEmpty() == FALSE)
				{
					//*** Select all set Funktions ****************************************
					CString olTmpTxt, olUrno;
					int ilCount = m_LB_Functions.GetCount();
					for(int i = 0;i<ilCount;i++)
					{
						m_LB_Functions.GetText(i,olTmpTxt);
						olUrno = olTmpTxt.Mid(olTmpTxt.Find("URNO=")+5);
						if(opValues[ilValue].Find(olUrno) != -1)
							m_LB_Functions.SetSel(i,TRUE);
					}
					//*** end> Select all set Funktions ***********************************
				}
 			}
			break;
		}
	}
}

//---------------------------------------------------------------------------

void PSShiftRoster2ViewPage::OnAll() 
{
	int ilItems;
	ilItems = m_LB_Functions.GetCount();
	for(int i=0; i<ilItems; i++)
	{
		m_LB_Functions.SetSel(i,TRUE);
	}
}

//---------------------------------------------------------------------------

void PSShiftRoster2ViewPage::OnNothing() 
{
	int ilItems;
	ilItems = m_LB_Functions.GetCount();
	for(int i=0; i<ilItems; i++)
	{
		m_LB_Functions.SetSel(i,FALSE);
	}
}

//---------------------------------------------------------------------------

