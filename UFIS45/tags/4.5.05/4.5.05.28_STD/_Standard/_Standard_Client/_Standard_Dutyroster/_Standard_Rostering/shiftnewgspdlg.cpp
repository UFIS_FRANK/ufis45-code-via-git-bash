// ShiftNewGSPDlg.cpp: Implementierungsdatei
//---------------------------------------------------------------------------

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld ShiftNewGSPDlg 
//---------------------------------------------------------------------------


ShiftNewGSPDlg::ShiftNewGSPDlg (CString *popGPLName, CTime *popGPLVafr, CTime *popGPLVato, CString opTyp, CWnd* pParent /*=NULL*/) : CDialog(ShiftNewGSPDlg::IDD, pParent)
{
	omTyp		= opTyp;
	pomGPLName	= popGPLName;
	pomGPLVafr	= popGPLVafr;
	pomGPLVato	= popGPLVato;

	//{{AFX_DATA_INIT(ShiftNewGSPDlg)
	//}}AFX_DATA_INIT
}


void ShiftNewGSPDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ShiftNewGSPDlg)
	DDX_Control(pDX, IDC_C_FCTC_OLD, m_CmbFctcOld);
	DDX_Control(pDX, IDC_C_FCTC_NEW, m_CmbFctcNew);
	DDX_Control(pDX, IDC_S_NAME,		m_S_Name);
	DDX_Control(pDX, IDC_S_VALID,		m_S_Valid);
	DDX_Control(pDX, IDCANCEL,			m_B_Cancel);
	DDX_Control(pDX, IDOK,				m_B_Ok);
	DDX_Control(pDX, IDC_E_NAME,		m_E_Name);
	DDX_Control(pDX, IDC_E_VALIDFROM,	m_E_GPL_Vafr);
	DDX_Control(pDX, IDC_E_VALIDTO,		m_E_GPL_Vato);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ShiftNewGSPDlg, CDialog)
	//{{AFX_MSG_MAP(ShiftNewGSPDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten ShiftNewGSPDlg 
//---------------------------------------------------------------------------

BOOL ShiftNewGSPDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CString olDP = ":";

	if (omTyp == "NEW")
	{
		SetWindowText(LoadStg(SHIFT_NEW_CAPTION_N));
		m_S_Name.SetWindowText(LoadStg(SHIFT_NEW_S_NAME) + olDP);

		//hiding all controls concerning the replacement of functions
		HideFunctionReplacement();
	}
	else if (omTyp == "COPY")
	{
		SetWindowText(LoadStg(SHIFT_NEW_CAPTION_C));
		m_S_Name.SetWindowText(LoadStg(IDS_STRING63456) + olDP);
		(GetDlgItem(IDC_S_REPLACE_FCTC1))->SetWindowText(LoadStg(IDS_STRING63460));
		(GetDlgItem(IDC_S_REPLACE_FCTC2))->SetWindowText(LoadStg(IDS_STRING63461));
		FillComboBoxes();
	}
	
	m_B_Cancel.SetWindowText(LoadStg(SHIFT_STF_B_CANCEL));
	m_B_Ok.SetWindowText(LoadStg(SHIFT_STF_B_OK));
	m_S_Valid.SetWindowText(LoadStg(SHIFT_S_VALID) + olDP);

	m_E_Name.SetTextLimit(1,32);
	m_E_Name.SetBKColor(LTYELLOW);

	m_E_GPL_Vato.SetTypeToDate(true);
	m_E_GPL_Vato.SetBKColor(LTYELLOW);
	m_E_GPL_Vato.SetTextErrColor(RED);
	m_E_GPL_Vato.SetInitText(pomGPLVato->Format("%d.%m.%Y"));

	m_E_GPL_Vafr.SetTypeToDate(true);
	m_E_GPL_Vafr.SetBKColor(LTYELLOW);
	m_E_GPL_Vafr.SetTextErrColor(RED);
	m_E_GPL_Vafr.SetInitText(pomGPLVafr->Format("%d.%m.%Y"));

	m_E_Name.SetFocus();

	return TRUE;
}

//---------------------------------------------------------------------------

void ShiftNewGSPDlg::OnOK() 
{
	CString olGPLvafr;
	CString olGPLvato;
	CString olErrorText;

	bool ilValidStatus = true;
	bool ilNameStatus = true;

	CTime olTmpTimeFr = -1;
	CTime olTmpTimeTo = -1;

	if (m_E_Name.GetStatus() == false)
	{
		ilNameStatus = false;
		if(m_E_Name.GetWindowTextLength() == 0)
		{
			//Grundschichtplanname enth�lt keine Daten.\n*INTXT*
			olErrorText = LoadStg(IDS_STRING63472);
		}
		else
		{
			//Grundschichtplanname entspricht nicht dem Eingabeformat.\n*INTXT*
			olErrorText = LoadStg(IDS_STRING63473);
		}
	}
	if (ilNameStatus == true)
	{
		m_E_Name.GetWindowText(*pomGPLName);
		CString olTmp;
		bool blRet = ogBCD.GetField("GPL", "GSNM", *pomGPLName, "URNO", olTmp );
		if (blRet == true)
		{
			ilNameStatus = false;
			//Grundschichtplanname existiert bereits.\n*INTXT*
			olErrorText = LoadStg(IDS_STRING63474);
		}
	}

	//sind die Datumswerte g�ltig?
	m_E_GPL_Vafr.GetWindowText(olGPLvafr);
	m_E_GPL_Vato.GetWindowText(olGPLvato);
	if ((m_E_GPL_Vafr.GetStatus() == false) || (m_E_GPL_Vato.GetStatus() == false))
	{
		ilValidStatus = false;
	}

	if ((olGPLvato.GetLength() != 0 && olGPLvafr.GetLength() == 0) || (olGPLvato.GetLength() == 0 && olGPLvafr.GetLength() != 0))
	{
		ilValidStatus = false;
	}
	if (ilValidStatus == true && olGPLvato.GetLength() != 0 && olGPLvafr.GetLength() != 0) 
	{
		olTmpTimeFr = DateHourStringToDate(olGPLvafr,CString("00:00"));
		olTmpTimeTo = DateHourStringToDate(olGPLvato,CString("00:00"));

		if (olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilValidStatus = false;
		}
	}

	if (ilValidStatus == true)
	{
		*pomGPLVafr = olTmpTimeFr;
		*pomGPLVato = olTmpTimeTo;
	}
	else
	{
		if(olErrorText != "")
		{
			olErrorText += "\n";
		}
		olErrorText += LoadStg(SHIFT_VALID_ERROR);
	}
	if (ilNameStatus == true && ilValidStatus == true)
	{
		// getting selection of the two ComboBoxes
		int ilSelItem;
		ilSelItem = m_CmbFctcOld.GetCurSel();
		if (ilSelItem != LB_ERR)
		{
			m_CmbFctcOld.GetLBText(ilSelItem, omPfcOld);
		}
		ilSelItem = m_CmbFctcNew.GetCurSel();
		if (ilSelItem != LB_ERR)
		{
			m_CmbFctcNew.GetLBText(ilSelItem, omPfcNew);
		}
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText, LoadStg(ST_FEHLER), MB_ICONEXCLAMATION);
		m_E_Name.SetFocus();
	}

}

//---------------------------------------------------------------------------
void ShiftNewGSPDlg::HideFunctionReplacement()
{
	CRect olRect;
	int ilMoveUp = 75;

	// --- hiding controls concerning the replacement of functions
	GetDlgItem (IDC_S_NEW_GSP_2)->ShowWindow(SW_HIDE);
	GetDlgItem (IDC_S_REPLACE_FCTC1)->ShowWindow(SW_HIDE);
	GetDlgItem (IDC_S_REPLACE_FCTC2)->ShowWindow(SW_HIDE);
	GetDlgItem (IDC_C_FCTC_OLD)->ShowWindow(SW_HIDE);
	GetDlgItem (IDC_C_FCTC_NEW)->ShowWindow(SW_HIDE);

	// --- resizing the form itself
	GetWindowRect (olRect);
	ClientToScreen (olRect);
	MoveWindow(olRect.left, olRect.top, olRect.Width(), olRect.Height() - ilMoveUp);
	CenterWindow();

	// --- moving bottom-buttons (Info, OK, Cancel)
	GetDlgItem (IDOK)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDOK)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());

	GetDlgItem (IDCANCEL)->GetWindowRect(olRect);
	ScreenToClient (olRect);
	GetDlgItem (IDCANCEL)->MoveWindow(olRect.left, olRect.top - ilMoveUp, olRect.Width(), olRect.Height());
}

void ShiftNewGSPDlg::FillComboBoxes()
{
	int i;
	CString olLine;

	m_CmbFctcOld.ResetContent();
	m_CmbFctcNew.ResetContent();

	m_CmbFctcOld.SetFont(&ogCourier_Regular_8);
	m_CmbFctcNew.SetFont(&ogCourier_Regular_8);

	olLine = LoadStg(IDS_STRING63457); // "--- ALLE ---"
	m_CmbFctcOld.AddString(olLine);
	olLine = LoadStg(IDS_STRING63458); // "--- KEIN ERSETZEN ---"
	m_CmbFctcOld.AddString(olLine);
	m_CmbFctcNew.AddString(olLine);

	for (i = 0; i < ogPfcData.omData.GetSize(); i++)
	{
		olLine.Format("%-8s - %-40s", ogPfcData.omData[i].Fctc, ogPfcData.omData[i].Fctn);
		m_CmbFctcOld.AddString(olLine);
		m_CmbFctcNew.AddString(olLine);
	}
	m_CmbFctcOld.SetCurSel(0);
	m_CmbFctcNew.SetCurSel(0);
}

CString ShiftNewGSPDlg::GetReplacementFunctionUrnoOld()
{
	CString olRet;
	CString olTmpTxt;

	if (omPfcOld.GetLength())
	{
		if (omPfcOld == LoadStg(IDS_STRING63457))
		{
			olRet = "";
		}
		else if (omPfcOld == LoadStg(IDS_STRING63458))
		{
			olRet = LoadStg(IDS_STRING63458);
		}
		else
		{
			int ilPos = omPfcOld.Find("-");
			if (ilPos > -1)
			{
				olTmpTxt = omPfcOld.Left(ilPos - 1);
				olTmpTxt.TrimRight();
				PFCDATA *polPfcData = ogPfcData.GetPfcByFctc (olTmpTxt);
				if (polPfcData != NULL)
				{
					olRet.Format ("%ld", polPfcData->Urno);
				}
			}
		}
	}

	return olRet;
}

CString ShiftNewGSPDlg::GetReplacementFunctionUrnoNew()
{
	CString olRet;
	CString olTmpTxt;

	if (omPfcNew.GetLength())
	{
		if (omPfcNew == LoadStg(IDS_STRING63458))
		{
			olRet = "";
		}
		else
		{
			int ilPos = omPfcNew.Find("-");
			if (ilPos > -1)
			{
				olTmpTxt = omPfcNew.Left(ilPos - 1);
				olTmpTxt.TrimRight();
				PFCDATA *polPfcData = ogPfcData.GetPfcByFctc (olTmpTxt);
				if (polPfcData != NULL)
				{
					olRet.Format ("%ld", polPfcData->Urno);
				}
			}
		}
	}

	return olRet;
}