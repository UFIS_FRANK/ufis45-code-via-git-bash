// CedaOdaData.cpp
 
#include <stdafx.h>


void ProcessOdaCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaOdaData::CedaOdaData() : CedaData(&ogCommHandler)
{
CCS_TRY;
	// Create an array of CEDARECINFO for ODADataStruct
	BEGIN_CEDARECINFO(ODADATA,ODADataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		//FIELD_CHAR_TRIM	(Ctrc,"CTRC")
		FIELD_CHAR_TRIM	(Dptc,"DPTC")
		FIELD_CHAR_TRIM	(Dura,"DURA")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		FIELD_CHAR_TRIM	(Sdac,"SDAC")
		FIELD_CHAR_TRIM	(Sdak,"SDAK")
		FIELD_CHAR_TRIM	(Sdan,"SDAN")
		FIELD_CHAR_TRIM	(Sdas,"SDAS")
		FIELD_CHAR_TRIM	(Tatp,"TATP")
		FIELD_CHAR_TRIM	(Tsap,"TSAP")
		FIELD_CHAR_TRIM	(Type,"TYPE")
		FIELD_CHAR_TRIM	(Upln,"UPLN")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Free,"FREE")
		FIELD_CHAR_TRIM	(Tbsd,"TBSD")
		FIELD_CHAR_TRIM	(Work,"WORK")
		FIELD_CHAR_TRIM	(Abfr,"ABFR")				// abwesend von		HHMM
		FIELD_CHAR_TRIM	(Abto,"ABTO")				// abwesend bis		HHMM
		FIELD_CHAR_TRIM	(Blen,"BLEN")				// Pause (L�nge)	HHMM
		FIELD_CHAR_TRIM	(Rgbc,"RGBC")
	END_CEDARECINFO //(ODADataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(ODADataRecInfo)/sizeof(ODADataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&ODADataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"ODA");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmListOfFields,"CDAT,DPTC,DURA,LSTU,PRFL,REMA,SDAC,SDAK,SDAN,SDAS,TATP,TSAP,TYPE,UPLN,URNO,USEC,USEU,FREE,TBSD,WORK,ABFR,ABTO,BLEN,RGBC");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
CCS_CATCH_ALL;
}

//---------------------------------------------------------------------------------------------------------

void CedaOdaData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaOdaData::Register(void)
{
CCS_TRY;
	DdxRegister((void *)this,BC_ODA_CHANGE,	"ODADATA", "Oda-changed",	ProcessOdaCf);
	DdxRegister((void *)this,BC_ODA_NEW,	"ODADATA", "Oda-new",		ProcessOdaCf);
	DdxRegister((void *)this,BC_ODA_DELETE,	"ODADATA", "Oda-deleted",	ProcessOdaCf);
CCS_CATCH_ALL;
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaOdaData::~CedaOdaData(void)
{
CCS_TRY;
	TRACE("CedaOdaData::~CedaOdaData called\n");
	omRecInfo.DeleteAll();
	ClearAll();
CCS_CATCH_ALL;
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaOdaData::ClearAll(bool bpWithRegistration)
{
CCS_TRY;
 	TRACE("CedaOdaData::ClearAll called\n");
	omUrnoMap.RemoveAll();
	omSdacMap.RemoveAll();
    omKeyMap.RemoveAll();
	omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
CCS_CATCH_ALL;
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaOdaData::Read(char *pspWhere /*NULL*/)
{
CCS_TRY;
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
	omSdacMap.RemoveAll();
	omData.DeleteAll();

	if (!CedaObject::IsFieldAvailable(CString("BSD"),CString("RGBC")))
	{
		strcpy(pcmListOfFields,"CDAT,DPTC,DURA,LSTU,PRFL,REMA,SDAC,SDAK,SDAN,SDAS,TATP,TSAP,TYPE,UPLN,URNO,USEC,USEU,FREE,TBSD,WORK,ABFR,ABTO,BLEN");
		pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer
	}

	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(pspWhere == NULL)
	{	
		ilRc = CedaAction2("RT");
	}
	else
	{
		ilRc = CedaAction2("RT", pspWhere);
	}
	if (ilRc != true)
	{
		TRACE("Read-Oda: Ceda-Error %d \n",ilRc);
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
	{
		ODADATA *prlOda = new ODADATA;
		if ((ilRc = GetFirstBufferRecord2(prlOda)) == true)
		{
			if(IsValidOda(prlOda))
			{
				// bda in Absprache mit HG: Tbsd korrigieren: wenn Abfr, Abto da sind, tbsd = 0, sonst = 1
				COleDateTimeSpan olTime;
				if (CedaDataHelper::HourMinStringToOleDateTimeSpan(prlOda->Abfr,olTime) &&
					CedaDataHelper::HourMinStringToOleDateTimeSpan(prlOda->Abto,olTime))
				{
					strcpy(prlOda->Tbsd, "0");
				}
				/*else
				{
					strcpy(prlOda->Tbsd, "1");	// muss sp�ter ganz eliminiert werden!
				}*/
			
				omData.Add(prlOda);//Update omData
				omUrnoMap.SetAt((void *)prlOda->Urno,prlOda);
				omSdacMap.SetAt((CString)prlOda->Sdac,prlOda);
				
				CString olTmp;
				olTmp.Format("%s-%s",prlOda->Sdac, ""/*prlOda->Ctrc*/);
				omKeyMap.SetAt(olTmp,prlOda);
#ifdef TRACE_FULL_DATA
				// Datensatz OK, loggen if FULL
				ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
				GetDataFormatted(ogRosteringLogText, prlOda);
				WriteLogFull("");
#endif TRACE_FULL_DATA
			}
			else
			{
				delete prlOda;
			}
		}
		else
		{
			delete prlOda;
		}
	}
	TRACE("Read-Oda: %d gelesen\n",ilCountRecord-1);
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord-1, pcmTableName);
		WriteInRosteringLog();
	}

    return true;
CCS_CATCH_ALL;
return false;
}

/****************************************************************
validation control
****************************************************************/
bool CedaOdaData::IsValidOda(ODADATA *prpOda)
{
	if(!prpOda) return false;
	
	COleDateTimeSpan olTime;
	if (CedaDataHelper::HourMinStringToOleDateTimeSpan(prpOda->Abfr,olTime) &&
		!CedaDataHelper::HourMinStringToOleDateTimeSpan(prpOda->Abto,olTime))
	{
		CString olErr;
		olErr.Format("ODATAB.ABFR is not empty, but ODATAB.ABTO is empty, ODATAB data corrupt.\n");
		ogErrlog += olErr;
		
		if(IsTraceLoggingEnabled())
		{
			GetDefectDataString(ogRosteringLogText, (void*)prpOda, "ODATAB.ABTO is defect");
			WriteInRosteringLog(LOGFILE_TRACE);
		}
		return false;
	}
	return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaOdaData::Insert(ODADATA *prpOda)
{
CCS_TRY;
	prpOda->IsChanged = DATA_NEW;
	if(Save(prpOda) == false) return false; //Update Database
	InsertInternal(prpOda);
    return true;
CCS_CATCH_ALL;
return false;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaOdaData::InsertInternal(ODADATA *prpOda)
{
CCS_TRY;
	ogDdx.DataChanged((void *)this, ODA_NEW,(void *)prpOda ); //Update Viewer
	omData.Add(prpOda);//Update omData
	omUrnoMap.SetAt((void *)prpOda->Urno,prpOda);
	omSdacMap.SetAt((CString)prpOda->Sdac,prpOda);

	CString olTmp;
	olTmp.Format("%s-%s",prpOda->Sdac, ""/*prpOda->Ctrc*/);
	omKeyMap.SetAt(olTmp,prpOda);

    return true;
CCS_CATCH_ALL;
return false;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaOdaData::Delete(long lpUrno)
{
CCS_TRY;
	ODADATA *prlOda = GetOdaByUrno(lpUrno);
	if (prlOda != NULL)
	{
		prlOda->IsChanged = DATA_DELETED;
		if(Save(prlOda) == false) return false; //Update Database
		DeleteInternal(prlOda);
	}
    return true;
CCS_CATCH_ALL;
return false;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaOdaData::DeleteInternal(ODADATA *prpOda)
{
CCS_TRY;
	ogDdx.DataChanged((void *)this,ODA_DELETE,(void *)prpOda); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpOda->Urno);
	omSdacMap.RemoveKey((CString)prpOda->Sdac);

	CString olTmp;
	olTmp.Format("%s-%s",prpOda->Sdac, ""/*prpOda->Ctrc*/);
	omKeyMap.RemoveKey(olTmp);


	int ilOdaCount = omData.GetSize();
	for (int ilCountRecord = 0; ilCountRecord < ilOdaCount; ilCountRecord++)
	{
		if (omData[ilCountRecord].Urno == prpOda->Urno)
		{
			omData.DeleteAt(ilCountRecord);//Update omData
			break;
		}
	}
    return true;
CCS_CATCH_ALL;
return false;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaOdaData::Update(ODADATA *prpOda)
{
CCS_TRY;
	if (GetOdaByUrno(prpOda->Urno) != NULL)
	{
		if (prpOda->IsChanged == DATA_UNCHANGED)
		{
			prpOda->IsChanged = DATA_CHANGED;
		}
		if(Save(prpOda) == false) return false; //Update Database
		UpdateInternal(prpOda);
	}
    return true;
CCS_CATCH_ALL;
return false;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaOdaData::UpdateInternal(ODADATA *prpOda)
{
CCS_TRY;
	ODADATA *prlOda = GetOdaByUrno(prpOda->Urno);
	if (prlOda != NULL)
	{
		CString olTmp;
		olTmp.Format("%s-%s",prlOda->Sdac, ""/*prlOda->Ctrc*/);
		omKeyMap.RemoveKey(olTmp);
		omSdacMap.RemoveKey((CString)prlOda->Sdac);

		*prlOda = *prpOda; //Update omData

		omSdacMap.SetAt((CString)prlOda->Sdac,prlOda);
		olTmp.Format("%s-%s",prlOda->Sdac, ""/*prlOda->Ctrc*/);
		omKeyMap.SetAt(olTmp,prlOda);

		ogDdx.DataChanged((void *)this,ODA_CHANGE,(void *)prlOda); //Update Viewer
	}
    return true;
CCS_CATCH_ALL;
return false;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

ODADATA *CedaOdaData::GetOdaByUrno(long lpUrno)
{
CCS_TRY;
	ODADATA  *prlOda;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlOda) == TRUE)
	{
		return prlOda;
	}
	return NULL;
CCS_CATCH_ALL;
return NULL;
}

//--GET-BY-SDAC--------------------------------------------------------------------------------------------

ODADATA *CedaOdaData::GetOdaBySdac(CString opSdac)
{
CCS_TRY;
	ODADATA  *prlOda;
	if (omSdacMap.Lookup(opSdac,(void *& )prlOda) == TRUE)
	{
		return prlOda;
	}
	return NULL;
CCS_CATCH_ALL;
return NULL;
}

//--GET BY KEY---------------------------------------------------------------

ODADATA *CedaOdaData::GetOdaByKey(CString opSdac, CString opCtrc)
{
CCS_TRY;
	CString olTmp;
	olTmp.Format("%s-%s", opSdac, opCtrc);

	ODADATA  *prlOda = NULL;
	if (omKeyMap.Lookup(olTmp,(void *&)prlOda) == TRUE)
	{
		return prlOda;
	}
	return NULL;
CCS_CATCH_ALL;
return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaOdaData::ReadSpecialData(CCSPtrArray<ODADATA> *popOda,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
CCS_TRY;
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(ipSYS == true) 
	{
		if (CedaAction("SYS","ODA",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction2("RT","ODA",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popOda != NULL)
	{
		for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
		{
			ODADATA *prpOda = new ODADATA;
			if ((ilRc = GetBufferRecord2(ilCountRecord,prpOda,CString(pclFieldList))) == true)
			{
				popOda->Add(prpOda);
			}
			else
			{
				delete prpOda;
			}
		}
		if(popOda->GetSize() == 0) return false;
	}
    return true;
CCS_CATCH_ALL;
return false;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaOdaData::Save(ODADATA *prpOda)
{
CCS_TRY;
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpOda->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpOda->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpOda);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpOda->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpOda->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpOda);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpOda->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpOda->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
	TRACE("Oda-IRT/URT/DRT: Ceda-Return %d \n",ilRc);
    return ilRc;
CCS_CATCH_ALL;
return false;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessOdaCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
CCS_TRY;
	ogOdaData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
CCS_CATCH_ALL;
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaOdaData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
CCS_TRY;
	struct BcStruct *prlOdaData;
	prlOdaData = (struct BcStruct *) vpDataPointer;
	ODADATA *prlOda;
	long llUrno;
	CString olSelection;

		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("ProcessBC:\n  Cmd <%s>\n  Tbl <%s>\n  Twe <%s>\n  Sel <%s>\n  Fld <%s>\n  Dat <%s>\n  Bcn <%s>",prlOdaData->Cmd, prlOdaData->Object, prlOdaData->Twe, prlOdaData->Selection, prlOdaData->Fields, prlOdaData->Data,prlOdaData->BcNum);
		WriteInRosteringLog();
	}

	switch(ipDDXType)
	{
	default:
		break;
	case BC_ODA_CHANGE:
		llUrno = GetUrnoFromSelection(prlOdaData->Selection);
		prlOda = GetOdaByUrno(llUrno);
		if(prlOda != NULL)
		{
			GetRecordFromItemList(prlOda,prlOdaData->Fields,prlOdaData->Data);
			UpdateInternal(prlOda);
			break;
		}
	case BC_ODA_NEW:
		prlOda = new ODADATA;
		GetRecordFromItemList(prlOda,prlOdaData->Fields,prlOdaData->Data);
		InsertInternal(prlOda);
		break;
	case BC_ODA_DELETE:
		olSelection = (CString)prlOdaData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlOdaData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlOda = GetOdaByUrno(llUrno);
		if (prlOda != NULL)
		{
			DeleteInternal(prlOda);
		}
		break;
	}
CCS_CATCH_ALL;
}

/*******************************************************************************************************
IsODAAndIsRegularFree: pr�ft, ob der Code <opCode> eine Abwesenheit ist und das Flag 
	'regul�r arbeitsfrei' gesetzt (= 'x') ist.
R�ckgabe:	CODE_IS_ODA_FREE	->	<opCode> ist ODA-Datensatz, Flag ist NICHT gesetzt
				CODE_IS_BSD		->	keine Abwesenheit
				CODE_IS_ODA_REGULARFREE			->	Code ist ODA, Flag ist gesetzt

pipTbsd:	pr�fen, ob opCode zeitbasierend ist:
			Time Based: wenn "0" - an diesem Tag ist keine andere Schicht,
			Anfangszeit ist 00:00 oder das Ende der vorherigen Schicht 
			(was sp�ter vorkommt), Endzeit ist 00:00 n�chsten Tages
			wenn "1" - an diesem Tag ist/war eine andere Schicht, 
			die Anfangs- und Endzeit sind dieser Schicht entnommen
			(k�nnen im Tagesdialog danach ge�ndert werden)

pbpWork		wenn != 0, dann wird auf true gesetzt, wenn ODA.Work == "1"
*******************************************************************************************************/
int CedaOdaData::IsODAAndIsRegularFree(CString opCode, int* pipTbsd/*=0*/, bool* pbpWork /*= 0*/, bool* pbpIsSleepDay /*= 0*/)
{
CCS_TRY;
	int ilCode = CODE_UNDEFINED;
	ODADATA *prlOda = ogOdaData.GetOdaBySdac(opCode);

	if (prlOda != NULL)
	{
		if (pipTbsd != 0)
		{
			if (!strcmp(prlOda->Tbsd, "1"))
			{
				*pipTbsd = CODE_IS_ODA_TIMEBASED;
			}
			else
			{
				*pipTbsd = CODE_IS_ODA_NOT_TIMEBASED;
			}
		}

		if (strcmp(prlOda->Free, "x"))
		{
			ilCode = CODE_IS_ODA_FREE;
		}
		else
		{
			ilCode = CODE_IS_ODA_REGULARFREE;
		}

		if (pbpWork != 0)
		{
			*pbpWork = strcmp(prlOda->Work, "1") == 0;
		}

		if (pbpIsSleepDay != NULL)
		{
			if (strcmp(prlOda->Type, "S") == NULL)
			{
				*pbpIsSleepDay = true;
			}
			else
			{
				*pbpIsSleepDay = false;
			}
		}
	}
	else
	{
		if(pipTbsd != 0)
		{
			*pipTbsd = CODE_UNDEFINED;
		}

		ilCode = CODE_IS_BSD;
	}

	return ilCode;
CCS_CATCH_ALL;
return CODE_UNDEFINED;
}

/**********************************************************************************************
pr�fen, ob opCode zeitbasierend ist:
Time Based: wenn "0" - an diesem Tag ist keine andere Schicht,
Anfangszeit ist 00:00 oder das Ende der vorherigen Schicht 
(was sp�ter vorkommt), Endzeit ist 00:00 n�chsten Tages
wenn "1" - an diesem Tag ist/war eine andere Schicht, 
die Anfangs- und Endzeit sind dieser Schicht entnommen
(k�nnen im Tagesdialog danach ge�ndert werden)
**********************************************************************************************/
int CedaOdaData::IsODAAndIsTimeBased(CString opCode)
{
CCS_TRY;
	ODADATA *prlOda = ogOdaData.GetOdaBySdac(opCode);
	if(prlOda != NULL)
	{
		if(!strcmp(prlOda->Tbsd, "1"))
			return CODE_IS_ODA_TIMEBASED;
		else
			return CODE_IS_ODA_NOT_TIMEBASED;
	}
	else
	{
		return CODE_IS_BSD;
	}
CCS_CATCH_ALL;
return CODE_IS_BSD;
}

//*******************************************************************************************************
// GetDefaultFreeODA: ermittelt den laut Parametertabelle am Tag <opDate>
//	g�ltigen Abwesenheitscode und daraus den dazugeh�rigen Datensatz, dessen
//	Flag 'regul�r arbeitsfrei' gepr�ft wird.
// R�ckgabe:	Zeiger auf den gefundenen Datensatz oder NULL
//*******************************************************************************************************

ODADATA* CedaOdaData::GetDefaultFreeODA(COleDateTime opDate)
{
CCS_TRY;
	// g�ltiger Zeitwert?
	if (opDate.GetStatus() != COleDateTime::valid) return NULL;

	// Code aus Parametertabelle
	CString olStringDate = opDate.Format("%Y%m%d%H%M%S");
	CString olOdaCode = ogCCSParam.GetParamValue(ogAppl,"ID_REGULAR_FREE",&olStringDate);
	
	// Datensatz lesen und pr�fen
	ODADATA* polOda = ogOdaData.GetOdaBySdac(olOdaCode);
	if ((polOda != NULL) && (CString(polOda->Free) == "x"))
		return polOda;	// DS gefunden und OK
	else
		return NULL;	// DS nicht gefunden oder Flag nicht gesetzt
CCS_CATCH_ALL;
return NULL;
}
