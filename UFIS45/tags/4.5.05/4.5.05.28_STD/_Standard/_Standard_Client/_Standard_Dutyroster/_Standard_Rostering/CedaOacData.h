// BDA 16.02.2000 erstellt aus CedaDrdData.h

#ifndef _CEDAOACDATA_H_
#define _CEDAOACDATA_H_

#include <stdafx.h>
/*#include "CCSPtrArray.h"
#include <CCSCedaData.h>
#include <ccsddx.h>
#include <ccsglobl.h>*/

/////////////////////////////////////////////////////////////////////////////

// COMMANDS FOR MEMBER-FUNCTIONS
#define OAC_SEND_DDX	(true)
#define OAC_NO_SEND_DDX	(false)

// Felddimensionen
#define OAC_CTRC_LEN	(8)

// OACTAB verbindet ODATAB mit COTTAB und listet Abwesenheiten auf, die f�r angegebenen Contract type zugelassen sind
// Struktur eines OAC-Datensatzes
struct OACDATA {
	char			Ctrc[OAC_CTRC_LEN+2];	//	Contract Code from COT
	char 			Hopo[HOPO_LEN+2]; 	//	Home Airport
	char 			Sdac[SDAC_LEN+2]; 	//	Absence Code from ODA
	long 			Urno 				;	//	Eindeutige Datensatz-Nr.

	//DataCreated by this class
	int			IsChanged;	// Check whether Data has Changed f�r Relaese

	// Initialisation
	OACDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		IsChanged = DATA_UNCHANGED;
	}
};	


/*
STF.URNO -> SCO.SURN SCO.CODE <- OAC.CTRC <- ODA.SDAC
*/



/************************************************************************************************************************
************************************************************************************************************************

  Deklaration CedaOacData: kapselt den Zugriff auf die Tabelle OACTAB

************************************************************************************************************************
************************************************************************************************************************/

class CedaOacData : public CedaData  
{
// Funktionen
public:
    // Konstruktor/Destruktor
	CedaOacData(CString opTableName = "OAC", CString opExtName = "TAB");
	~CedaOacData();

	// die geladenen Datens�tze
	CCSPtrArray<OACDATA> omData;

// allgemeine Funktionen
	void SetTableName(CString opTableName)
	{
		strcpy(pcmTableName, opTableName.GetBuffer(0));
	}
	// Feldliste lesen
	CString GetFieldList(void) {return CString(pcmListOfFields);}

// Broadcasts empfangen und bearbeiten
	// behandelt die Broadcasts BC_OAC_CHANGE,BC_OAC_DELETE und BC_OAC_NEW
	void ProcessOacBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

// Lesen/Schreiben von Datens�tzen
	// Datens�tze einlesen
	bool Read(char *pspWhere, bool bpRegisterBC = true, bool bpClearData = true);
	// Datens�tze mit Hilfe selbst-formatierter SQL-String einlesen
	bool ReadSpecialData(CCSPtrArray<OACDATA> *popOacArray,char *pspWhere,char *pspFieldList, char *pcpSort=0,bool ipSYS=true);
	// Datensatz mit bestimmter Urno einlesen
	bool ReadOacByUrno(long lpUrno, OACDATA *prpOac);
	
	// einen Datensatz aus der Datenbank l�schen
	bool Delete(OACDATA *prpOac, BOOL bpWithSave = true);
	// einen neuen Datensatz in der Datenbank speichern
	bool Insert(OACDATA *prpOac, bool bpSave = true);
	// einen ge�nderten Datensatz speichern
	bool Update(OACDATA *prpOac);

// Datens�tze suchen
	// OAC mit Urno <lpUrno> suchen
	OACDATA* GetOacByUrno(long llUrno);
	// GetOacByKey: sucht den Datensatz mit dem Prim�rschl�ssel aus 
	OACDATA* GetOacByKey(CString opCtrc, CString opSdac);
	// Zugriff auf interne Daten
	// liefert den Zeiger auf die interne Datenhaltung <omData>
	OACDATA		GetInternalData (int ipIndex) {return omData[ipIndex];}
	// liefert die Anzahl der Datens�tze in der internen Datenhaltung <omData>
	int			GetInternalSize() {return omData.GetSize();}

	// IsValidOac: pr�ft alles, was man pr�fen kann, inklusive g�ltigen Codes und duplizierten OACs
	bool IsValidOac(OACDATA *popOac);
	// Trace Funktion
	void TraceOacData(OACDATA* prpOac);

	// zwei OACs miteinander vergleichen: Ergebnis false -> OACs sind gleich
	bool CompareOacToOac(OACDATA *popOac1, OACDATA *popOac2,bool bpCompareKey = false);
	// kopiert alles, ausser Urno
	void CopyOac(OACDATA* popOacDataSource,OACDATA* popOacDataTarget);

// Manipulation von Datens�tzen
	// OACDATA erzeugen, initialisieren und in die Datenhaltung mit aufnehmen
	OACDATA* CreateOacData();
	// OACDATA erzeugen und initialisieren mit angegebenen Parametern ohne Aufnahme in die interne Datenhaltung
	OACDATA* CreateOacData(char* ppHopo);

// Daten
	// alle internen Arrays zur Datenhaltung leeren und beim BC-Handler abmelden
	bool ClearAll(bool bpUnregister);
 
protected:	
// Funktionen
// Broadcasts empfangen und bearbeiten
	// beim Broadcast-Handler anmelden
	void Register(void);

// allgemeine Funktionen
	// Tabellenname einstellen
	bool SetTableNameAndExt(CString opTableAndExtName);
	// Feldliste schreiben
	void SetFieldList(CString opFieldList) {strcpy(pcmListOfFields,opFieldList.GetBuffer(0));}

// Datens�tze speichern/bearbeiten

	// speichert einen einzelnen Datensatz
	bool Save(OACDATA *prpOac);
	// wenn es ein neuer Datensatz ist, einen Broadcast OAC_NEW abschicken und den Datensatz in die interne 
	// Datenhaltung aufnehmen, sonst return false
	bool InsertInternal(OACDATA *prpOac, bool bpSendDdx);
	// einen Broadcast OAC_CHANGE versenden
	bool UpdateInternal(OACDATA *prpOac, bool bpSendDdx = true);
	// einen Datensatz aus der internen Datenhaltung l�schen und einen Broadcast senden
	void DeleteInternal(OACDATA *prpOac, bool bpSendDdx=true);

public:
// Kommunikation mit CEDA
	// Kommandos an CEDA senden (die Funktionen der Basisklasse werden �berschrieben
	// wegen des Feldlisten Bugs)
	bool CedaAction(char *pcpAction, char *pcpTableName, char *pcpFieldList,
					char *pcpSelection, char *pcpSort, char *pcpData, 
					char *pcpDest = "BUF1",bool bpIsWriteAction = false, 
					long lpID = 0, CCSPtrArray<RecordSet> *pomData = NULL, 
					int ipFieldCount = 0);
    bool CedaAction(char *pcpAction, char *pcpSelection = NULL, char *pcpSort = NULL,
					char *pcpData = pcgDataBuf, char *pcpDest = "BUF1", 
					bool bpIsWriteAction = false, long lpID = 0);
protected:
// Daten
    // Map mit den Datensatz-Urnos der geladenen Datens�tze
	CMapPtrToPtr omUrnoMap;
    // Map mit dem Prim�rschl�ssel (aus CTRC & SDAC) der Datens�tze
	CMapStringToPtr omKeyMap;
	// beim Broadcast-Handler angemeldet?
	bool bmIsBCRegistered;
};

#endif // !defined _CEDAOACDATA_H_















