// CedaPgpData.cpp
 
#include <stdafx.h>

CedaPgpData ogPgpData;

void ProcessPgpCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaPgpData::CedaPgpData() : CedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for PGPDATA
	BEGIN_CEDARECINFO(PGPDATA,PgpDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Pgpc,"PGPC")
		FIELD_CHAR_TRIM	(Type,"TYPE")
		FIELD_CHAR_TRIM	(Pgpn,"PGPN")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		FIELD_CHAR_TRIM	(Pgpm,"PGPM")
		FIELD_CHAR_TRIM	(Minm,"MINM")
		FIELD_CHAR_TRIM	(Maxm,"MAXM")
	END_CEDARECINFO //(PGPDATA)

	// Copy the record structure
	for (int i=0; i< sizeof(PgpDataRecInfo)/sizeof(PgpDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&PgpDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"PGP");
	strcat(pcmTableName,pcgTableExt);
	strcpy(pcmListOfFields,"URNO,CDAT,LSTU,USEC,USEU,PRFL,PGPC,TYPE,PGPN,REMA,PGPM,MINM,MAXM");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaPgpData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaPgpData::Register(void)
{
	DdxRegister((void *)this,BC_PGP_CHANGE,	"PGPDATA", "Pgp-changed",	ProcessPgpCf);
	DdxRegister((void *)this,BC_PGP_NEW,	"PGPDATA", "Pgp-new",		ProcessPgpCf);
	DdxRegister((void *)this,BC_PGP_DELETE,	"PGPDATA", "Pgp-deleted",	ProcessPgpCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaPgpData::~CedaPgpData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaPgpData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaPgpData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(pspWhere == NULL)
	{	
		ilRc = CedaAction2("RT");
	}
	else
	{
		ilRc = CedaAction2("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
	{
		PGPDATA *prlPgp = new PGPDATA;
		if ((ilRc = GetFirstBufferRecord2(prlPgp)) == true)
		{
			omData.Add(prlPgp);//Update omData
			omUrnoMap.SetAt((void *)prlPgp->Urno,prlPgp);
#ifdef TRACE_FULL_DATA
			// Datensatz OK, loggen if FULL
			ogRosteringLogText.Format(" read %8.8lu %s OK:  ", ilCountRecord, pcmTableName);
			GetDataFormatted(ogRosteringLogText, prlPgp);
			WriteLogFull("");
#endif TRACE_FULL_DATA
		}
		else
		{
			delete prlPgp;
		}
	}
    
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  read %d records from table %s",ilCountRecord-1, pcmTableName);
		WriteInRosteringLog();
	}

	return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaPgpData::Insert(PGPDATA *prpPgp)
{
	prpPgp->IsChanged = DATA_NEW;
	if(Save(prpPgp) == false) return false; //Update Database
	InsertInternal(prpPgp);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaPgpData::InsertInternal(PGPDATA *prpPgp)
{
	ogDdx.DataChanged((void *)this, PGP_NEW,(void *)prpPgp ); //Update Viewer
	omData.Add(prpPgp);//Update omData
	omUrnoMap.SetAt((void *)prpPgp->Urno,prpPgp);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaPgpData::Delete(long lpUrno)
{
	PGPDATA *prlPgp = GetPgpByUrno(lpUrno);
	if (prlPgp != NULL)
	{
		prlPgp->IsChanged = DATA_DELETED;
		if(Save(prlPgp) == false) return false; //Update Database
		DeleteInternal(prlPgp);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaPgpData::DeleteInternal(PGPDATA *prpPgp)
{
	ogDdx.DataChanged((void *)this,PGP_DELETE,(void *)prpPgp); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpPgp->Urno);
	int ilPgpCount = omData.GetSize();
	for (int ilCountRecord = 0; ilCountRecord < ilPgpCount; ilCountRecord++)
	{
		if (omData[ilCountRecord].Urno == prpPgp->Urno)
		{
			omData.DeleteAt(ilCountRecord);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaPgpData::Update(PGPDATA *prpPgp)
{
	if (GetPgpByUrno(prpPgp->Urno) != NULL)
	{
		if (prpPgp->IsChanged == DATA_UNCHANGED)
		{
			prpPgp->IsChanged = DATA_CHANGED;
		}
		if(Save(prpPgp) == false) return false; //Update Database
		UpdateInternal(prpPgp);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaPgpData::UpdateInternal(PGPDATA *prpPgp)
{
	PGPDATA *prlPgp = GetPgpByUrno(prpPgp->Urno);
	if (prlPgp != NULL)
	{
		*prlPgp = *prpPgp; //Update omData
		ogDdx.DataChanged((void *)this,PGP_CHANGE,(void *)prlPgp); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

PGPDATA *CedaPgpData::GetPgpByUrno(long lpUrno)
{
	PGPDATA  *prlPgp;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlPgp) == TRUE)
	{
		return prlPgp;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaPgpData::ReadSpecialData(CCSPtrArray<PGPDATA> *popPgp,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("CedaAction:\n  Cmd <RT>\n  Tbl <%s>\n  Sel <%s>",pcmTableName, pspWhere);
		WriteInRosteringLog();
	}

	if(ipSYS == true) 
	{
		if (CedaAction("SYS","PGP",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction2("RT","PGP",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popPgp != NULL)
	{
		for (int ilCountRecord = 0; ilRc == true; ilCountRecord++)
		{
			PGPDATA *prpPgp = new PGPDATA;
			if ((ilRc = GetBufferRecord2(ilCountRecord,prpPgp,CString(pclFieldList))) == true)
			{
				popPgp->Add(prpPgp);
			}
			else
			{
				delete prpPgp;
			}
		}
		if(popPgp->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaPgpData::Save(PGPDATA *prpPgp)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpPgp->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpPgp->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpPgp);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpPgp->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpPgp->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpPgp);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpPgp->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpPgp->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessPgpCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogPgpData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaPgpData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlPgpData;
	prlPgpData = (struct BcStruct *) vpDataPointer;
	PGPDATA *prlPgp;

	long llUrno;
	CString olSelection;

		
	if(IsFullLoggingEnabled())
	{
		ogRosteringLogText.Format("ProcessBC:\n  Cmd <%s>\n  Tbl <%s>\n  Twe <%s>\n  Sel <%s>\n  Fld <%s>\n  Dat <%s>\n  Bcn <%s>",prlPgpData->Cmd, prlPgpData->Object, prlPgpData->Twe, prlPgpData->Selection, prlPgpData->Fields, prlPgpData->Data,prlPgpData->BcNum);
		WriteInRosteringLog();
	}

	switch(ipDDXType)
	{
	default:
		break;
	case BC_PGP_CHANGE:
		llUrno = GetUrnoFromSelection(prlPgpData->Selection);
		prlPgp = GetPgpByUrno(llUrno);
		if(prlPgp != NULL)
		{
			GetRecordFromItemList(prlPgp,prlPgpData->Fields,prlPgpData->Data);
			UpdateInternal(prlPgp);
			break;
		}
	case BC_PGP_NEW:
		prlPgp = new PGPDATA;
		GetRecordFromItemList(prlPgp,prlPgpData->Fields,prlPgpData->Data);
		InsertInternal(prlPgp);
		break;
	case BC_PGP_DELETE:
		olSelection = (CString)prlPgpData->Selection;
		if (olSelection.Find('\'') != -1)
		{
			llUrno = GetUrnoFromSelection(prlPgpData->Selection);
		}
		else
		{
			int ilFirst = olSelection.Find("=")+2;
			int ilLast  = olSelection.GetLength();
			llUrno = atol(olSelection.Mid(ilFirst,ilLast-ilFirst));
		}
		prlPgp = GetPgpByUrno(llUrno);
		if (prlPgp != NULL)
		{
			DeleteInternal(prlPgp);
		}
		break;
	}
}

//---------------------------------------------------------------------------------------------------------
