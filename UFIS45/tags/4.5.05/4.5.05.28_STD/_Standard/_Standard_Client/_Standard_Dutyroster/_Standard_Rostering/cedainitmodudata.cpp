// CedaInitModuData.cpp
 
#include <stdafx.h>

const igNoOfPackets = 10;
CedaInitModuData ogInitModuData;


//--CEDADATA-----------------------------------------------------------------------------------------------

CedaInitModuData::CedaInitModuData()
{
	strcpy(pcmInitModuFieldList,"APPL,SUBD,FUNC,FUAL,TYPE,STAT");
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaInitModuData::~CedaInitModuData(void)
{
}

//--SEND INIT MODULS-------------------------------------------------------------------------------------------

bool CedaInitModuData::SendInitModu()
{
	bool ilRc = true;

//INITMODU//////////////////////////////////////////////////////////////

	CString olInitModuData = GetInitModuTxt();

//	if (ogCCSParam.GetParamValue("GLOBAL","ID_ORG_SECURITY") == "Y")
//	{
//		olInitModuData += GetOrgUnitRegisterString();
//	}
	
	//INITMODU END//////////////////////////////////////////////////////////

	char *pclInitModuData = new char[olInitModuData.GetLength()+3];
	strcpy(pclInitModuData,olInitModuData);

	char pclTableExt[65];
	strcpy(pclTableExt, pcgTableExt);
	strcat(pclTableExt, " ");

	ilRc = CedaAction("SMI",pclTableExt,pcmInitModuFieldList,"","",pclInitModuData);
	if(ilRc==false)
	{
		AfxMessageBox(CString("SendInitModu:") + omLastErrorMessage,MB_ICONEXCLAMATION);
	}
	delete pclInitModuData;

	return ilRc;
}

//--------------------------------------------------------------------------------------------------------------

CString CedaInitModuData::GetInitModuTxt() 
{
	CString olInitModuData = "Rostering,InitModu,InitModu,Initialisieren (InitModu),B,-";
	olInitModuData += GetInitModuTxtG();

	// changed by bch 24.03.2005
	// moved this because the org units need to be registered as well (SendInitModuData no longer called)
	if (ogCCSParam.GetParamValue("GLOBAL","ID_ORG_SECURITY") == "Y")
	{
		olInitModuData += GetOrgUnitRegisterString();
	}
	return olInitModuData;
}

//--------------------------------------------------------------------------------------------------------------

CString CedaInitModuData::GetInitModuTxtG() 
{
	CString olTmpStr="";
	for(int ilIndex =0;ilIndex<10;ilIndex++)
	{
		switch(ilIndex)
		{
		case 0:
			break;
		case 1:
			break;
		case 2:
			break;
		case 3:

			// ROSTERING MENU
			olTmpStr += ",Rostering Menu,MENUDUTYROSTER,"		+ LoadStg(IDS_STRING11811) +",I,1";
			olTmpStr += ",Rostering Menu,MENUSHIFTROSTER,"		+ LoadStg(IDS_STRING11812) +",I,1";
			olTmpStr += ",Rostering Menu,MENUSTAMMDATEN,"		+ LoadStg(IDS_STRING11806) +",I,1";
			olTmpStr += ",Rostering Menu,MENUGRUPPIERUNG,"		+ LoadStg(IDS_STRING11807) +",I,1";
			olTmpStr += ",Rostering Menu,MENUSTAFFBALANCE,"		+ LoadStg(IDS_STRING11808) +",I,1";
			olTmpStr += ",Rostering Menu,MENUINITACCOUNT,"		+ LoadStg(IDS_STRING11809) +",I,1";
			olTmpStr += ",Rostering Menu,MENUABSENCEPLANNING,"	+ LoadStg(IDS_STRING11810) +",I,1";
			olTmpStr += ",Rostering Menu,MENUSENDTOSAP,"		+ LoadStg(IDS_STRING11823) +",I,0";
			olTmpStr += ",Rostering Menu,MENURELOADBASICDATA,"	+ LoadStg(IDS_STRING11824) +",I,0";

			//	Shift roster - ToolBar
			olTmpStr += ",Shift roster ToolBar,SR_VIEW,"		+ LoadStg(IDS_STRING1662)		+ ",B,1";
			olTmpStr += ",Shift roster ToolBar,SR_CB_REQ,"		+ LoadStg(SHIFT_S_REQUIREMENT)	+ ",B,1";
			olTmpStr += ",Shift roster ToolBar,SR_PLAN,"		+ LoadStg(SHIFT_CAPTION)		+ ",B,1";
			olTmpStr += ",Shift roster ToolBar,SR_SAVE,"		+ LoadStg(SHIFT_B_SAVEPLAN)		+ ",B,1";
			olTmpStr += ",Shift roster ToolBar,SR_REL,"			+ LoadStg(IDS_STRING1663)		+ ",B,1";
			olTmpStr += ",Shift roster ToolBar,SR_CODNUM,"		+ LoadStg(SHIFT_B_CODENUMBER)	+ ",B,1";
			olTmpStr += ",Shift roster ToolBar,SR_PRINT,"		+ LoadStg(IDS_STRING1664)		+ ",B,1";
			olTmpStr += ",Shift roster ToolBar,SR_CB_VIEW,"		  + LoadStg(IDS_STRING11818)	+ ",C,1";
			olTmpStr += ",Shift roster ToolBar,SR_CB_Requirement,"+ LoadStg(IDS_STRING11819)	+ ",C,1";
			olTmpStr += ",Shift roster ToolBar,SR_CB_PLAN,"	  + LoadStg(IDS_STRING11820)		+ ",C,1";

			// Shift roster - Hauptdialog			
			olTmpStr += ",Shift roster Maindialog,SR_VALID,"		+ LoadStg(SHIFT_S_VALID)		+ ",B,1";
			olTmpStr += ",Shift roster Maindialog,SR_CTRT_TYPE,"	+ LoadStg(SHIFT_S_CONTRACT)		+ ",C,1";
			olTmpStr += ",Shift roster Maindialog,SR_DAYS,"			+ LoadStg(SHIFT_S_PERIOD)		+ ",E,1";
			olTmpStr += ",Shift roster Maindialog,SR_BONUS,"		+ LoadStg(SHIFT_G_BONUS)		+ ",B,1";
			olTmpStr += ",Shift roster Maindialog,SR_HOLS,"			+ LoadStg(SHIFT_S_HOLIDAY)		+ ",E,1";
			olTmpStr += ",Shift roster Maindialog,SR_ABS,"			+ LoadStg(SHIFT_S_ABSENCE)		+ ",E,1";
			olTmpStr += ",Shift roster Maindialog,SR_GPLLIST,"		+ LoadStg(SHIFT_G_GSP)			+ ",L,1";
			olTmpStr += ",Shift roster Maindialog,SR_RNDUP,"		+ LoadStg(SHIFT_S_ROUNDUP)		+ ",E,1";
			olTmpStr += ",Shift roster Maindialog,SR_INFO,"			+ LoadStg(SHIFT_S_INFOMASK)		+ ",B,1";
			olTmpStr += ",Shift roster Maindialog,SR_NEW,"			+ LoadStg(SHIFT_B_NEW)			+ ",B,1";
			olTmpStr += ",Shift roster Maindialog,SR_COPY,"			+ LoadStg(SHIFT_B_COPY)			+ ",B,1";
			olTmpStr += ",Shift roster Maindialog,SR_DEACT,"		+ LoadStg(SHIFT_B_DEAKTIVATE)	+ ",B,1";
			olTmpStr += ",Shift roster Maindialog,SR_DELETE,"		+ LoadStg(IDS_STRING966)		+ ",B,1";
			olTmpStr += ",Shift roster Maindialog,SR_MCONST,"		+ LoadStg(SHIFT_KONSIST)		+ ",B,1";
			olTmpStr += ",Shift roster Maindialog,SR_RELNOTES,"		+ LoadStg(SHIFT_S_RELEASENOTE)	+ ",E,1";
			olTmpStr += ",Shift roster Maindialog,SR_ATPM,"			+ LoadStg(IDS_STRING11821)		+ ",E,1";
			olTmpStr += ",Shift roster Maindialog,SR_ORG,"			+ LoadStg(IDS_STRING11822)		+ ",E,1";
			
			//	DUTYROSTER - ToolBar
			olTmpStr += ",Duty roster Toolbar,DUTYROSTER_IDC_B_VIEW,"		+ LoadStg(IDS_STRING1662)	+ ",B,1";
			olTmpStr += ",Duty roster Toolbar,DUTYROSTER_IDC_CB_VIEW,"		+ LoadStg(IDS_STRING1662)	+ ",M,1";
			olTmpStr += ",Duty roster Toolbar,DUTYROSTER_IDC_B_RELEASE,"	+ LoadStg(IDS_STRING1663)	+ ",B,1";
			olTmpStr += ",Duty roster Toolbar,DUTYROSTER_IDC_B_PRINT,"		+ LoadStg(IDS_STRING1664)	+ ",B,1";
			olTmpStr += ",Duty roster Toolbar,DUTYROSTER_IDC_B_ACCOUNT,"	+ LoadStg(IDS_STRING11817)	+ ",B,1";
			olTmpStr += ",Duty roster Toolbar,DUTYROSTER_IDC_B_EXCEL,"		+ LoadStg(IDS_STRING11826)	+ ",B,0";

			//  CONTEXTMENUE
			olTmpStr += ",Context Menue,DUTYROSTER_CHANGEFIELD,"		+ LoadStg(IDS_STRING1665)	+ ",I,1";
			olTmpStr += ",Context Menue,DUTYROSTER_PROCESSING,"			+ LoadStg(IDS_STRING1610)	+ ",I,1";
			olTmpStr += ",Context Menue,DUTYROSTER_CONFLICTCHECK,"		+ LoadStg(IDS_STRING1677)	+ ",I,1";
			olTmpStr += ",Context Menue,DUTYROSTER_OPENDSR,"			+ LoadStg(IDS_STRING1678)	+ ",I,1";
			olTmpStr += ",Context Menue,DUTYROSTER_OPENAZK,"			+ LoadStg(IDS_STRING319)	+ ",I,1";
			olTmpStr += ",Context Menue,DUTYROSTER_SHOWWISHMENU,"		+ LoadStg(IDS_STRING1843)	+ ",I,1";
			olTmpStr += ",Context Menue,DUTYROSTER_SHOWEDITDRRMENU,"	+ LoadStg(IDS_STRING175)	+ ",I,1";
			olTmpStr += ",Context Menue,DUTYROSTER_CREATEDOUBLETOUR,"	+ LoadStg(IDS_STRING967)	+ ",I,1";
			olTmpStr += ",Context Menue,DUTYROSTER_SHOWCANCELDOUBLETOUR,"	+ LoadStg(IDS_STRING125)	+ ",I,1";
			olTmpStr += ",Context Menue,DUTYROSTER_SHOWCLEARDRR,"		+ LoadStg(IDS_STRING181)	+ ",I,1";
			olTmpStr += ",Context Menue,DUTYROSTER_SHOWCHANGESHIFT,"	+ LoadStg(IDS_STRING84)		+ ",I,1";
			olTmpStr += ",Context Menue,DUTYROSTER_SHOWMULTIDRR,"		+ LoadStg(IDS_STRING968)	+ ",I,1";

			//  DUTYROSTER - Dialog: IDC_DUTY_RELEASE_DLG und IDC_PSDUTYROSTER2
			olTmpStr += ",Duty roster,DUTYROSTER2PS_IDC_PLAN1,"		+ LoadStg(IDS_STRING1679) + LoadStg(IDS_STRING1586)	+ ",X,1";
			olTmpStr += ",Duty roster,DUTYROSTER2PS_IDC_PLAN2,"		+ LoadStg(IDS_STRING1679) + LoadStg(IDS_STRING1587)	+ ",X,1";
			olTmpStr += ",Duty roster,DUTYROSTER2PS_IDC_PLAN3,"		+ LoadStg(IDS_STRING1679) + LoadStg(IDS_STRING1588)	+ ",X,1";
			olTmpStr += ",Duty roster,DUTYROSTER2PS_IDC_PLAN4,"		+ LoadStg(IDS_STRING1679) + LoadStg(IDS_STRING1589)	+ ",X,1";
			olTmpStr += ",Duty roster,DUTYROSTER2PS_IDC_PLAN5,"		+ LoadStg(IDS_STRING1679) + LoadStg(IDS_STRING1877)	+ ",X,1";
			olTmpStr += ",Duty roster,DUTYROSTER2PS_IDC_PLANL,"		+ LoadStg(IDS_STRING1679) + LoadStg(IDS_STRING1876)	+ ",X,1";
			olTmpStr += ",Duty roster,DUTYROSTER2PS_IDC_PLANU,"		+ LoadStg(IDS_STRING1679) + LoadStg(IDS_STRING1680)	+ ",X,1";
			olTmpStr += ",Duty roster,DUTYROSTER2PS_IDC_PLANW,"		+ LoadStg(IDS_STRING1679) + LoadStg(IDS_STRING1681)	+ ",X,1";

			//  DUTYROSTER - IDC_DUTY_RELEASE_DLG
			olTmpStr += ",Duty roster,DUTYROSTER2PS_IDC_REL_NIGHT," + LoadStg(IDS_STRING11804) +",R,1";
			olTmpStr += ",Duty roster,DUTYROSTER2PS_IDC_NOW,"		+ LoadStg(IDS_STRING11805) +",R,1";
			olTmpStr += ",Duty roster,DUTYRELEASEDLG_IDC_PLAN1,"	+ LoadStg(IDS_STRING11798) +",R,1";
			olTmpStr += ",Duty roster,DUTYRELEASEDLG_IDC_PLAN2,"	+ LoadStg(IDS_STRING11800) +",R,1";
			olTmpStr += ",Duty roster,DUTYRELEASEDLG_IDC_PLAN3,"	+ LoadStg(IDS_STRING11801) +",R,1";
			olTmpStr += ",Duty roster,DUTYRELEASEDLG_IDC_PLAN4,"	+ LoadStg(IDS_STRING11802) +",R,1";
			olTmpStr += ",Duty roster,DUTYRELEASEDLG_IDC_PLAN5,"	+ LoadStg(IDS_STRING11799) +",R,1";
			olTmpStr += ",Duty roster,DUTYRELEASEDLG_IDC_PLAN6,"	+ LoadStg(IDS_STRING11803) +",R,1";
			olTmpStr += ",Duty roster,DUTYRELEASEDLG_UNRELEASE,"	+ LoadStg(IDS_STRING11825) +",B,0";

			// DUTYROSTER - IDD_DRR_DLG (Daily list)
			olTmpStr += ",Duty roster,DUTYDAILYLIST_SUB1SUB2,"	+ LoadStg(IDS_STRING204) + ",C,0";

			// DUTYROSTER - DIALOG IDD_GRID_IMPL
			olTmpStr += ",Duty roster,DUTYIDDGRID_IDC_INSERT,"	+ LoadStg(IDS_STRING11813) +",B,1";
			olTmpStr += ",Duty roster,DUTYIDDGRID_DELETE,"		+ LoadStg(IDS_STRING11816) +",B,1";

			// DUTYROSTER - IDD_DUTY_SHOWJOBS_DLG
			olTmpStr += ",Duty roster,DUTYSHOWJOBSDLG_IDC_Button3," + LoadStg(IDS_STRING11815) +",B,1";

			// Daily Roster Groups
			olTmpStr += ",Daily Roster Groups,HIDEHEADING,"		+ LoadStg(IDS_STRING767) +",I,-";
			olTmpStr += ",Daily Roster Groups,SORTBYFUNCTION,"	+ LoadStg(IDS_STRING768) +",I,-";
			olTmpStr += ",Daily Roster Groups,LANDSCAPE,"		+ LoadStg(IDS_STRING769) +",I,-";
			

			break;
		case 4:
			break;
		default:
			break;
		}
	}

	return olTmpStr;
}

CString CedaInitModuData::GetOrgUnitRegisterString()
{
	CString olRet;
	CString olTmp;

	int ilCount = ogOrgData.omData.GetSize();

	for (int i = 0; i < ilCount; i++)
	{
		olTmp = ogOrgData.omData[i].Dpt1;
		olRet += ",Shift roster Org. Units," + olTmp + "," + olTmp + ",Z,1";
	}

	/* only level 1 OrgUnits

	ORGDATA *prlOrgData = NULL;
	CMapStringToPtr olMap;
	void* prlVoid;

	for (int i = 0; i < ilCount; i++)
	{
		prlOrgData = &ogOrgData.omData[i];
		if (strlen(prlOrgData->Dpt2) > 0)
		{
			//it is a "level2"-org unit ==> we take the superior org unit stored in DPT2
			olTmp = prlOrgData->Dpt2;
		}
		else
		{
			//it is a "level1"-org unit
			olTmp = prlOrgData->Dpt1;
		}
		olMap.SetAt(olTmp,NULL);
	}

	for(POSITION rlPos = olMap.GetStartPosition(); rlPos != NULL; )
	{
		olMap.GetNextAssoc(rlPos,olTmp,(void *&)prlVoid);
		olRet += ",Shift Roster Org. Units," + olTmp + "," + olTmp + ",Z,1";
	}
	*/

	return olRet;
}
