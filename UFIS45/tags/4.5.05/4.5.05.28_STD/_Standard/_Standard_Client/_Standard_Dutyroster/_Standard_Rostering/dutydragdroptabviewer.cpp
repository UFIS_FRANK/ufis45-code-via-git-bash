// DutyDragDropTabViewer.cpp 
//
#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// Local function prototype
static void DutyDragDropTabViewerCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// DutyDragDropTabViewer
//

DutyDragDropTabViewer::DutyDragDropTabViewer(CWnd* pParent /*=NULL*/)
{
	pomParent = (DutyDragDropDlg*)pParent;

	// APO 08.01.2000: Umstellung ROS/DSR auf DRR
	// ToDo: BC-Handler f�r DRR-Events schreiben, sobald/falls neuer Mechanismus zum Sortieren
	// nach Gruppen gefunden wurde (Problematik mehrere Schichten pro Tag/MA)

	// BDA 15.02.2000  Umstellung ROS/DSR auf DRR
	// Gruppensortieren erfolgt nach DRG.WGPS (Arbeitsgruppencode),
	// mehrere Schichten pro Tag/MA: es wird immer nur die Schicht Nr.1 genommen (DRR/DRD.DRRN == 1)
CCS_TRY;
	DdxRegister(this, DRR_CHANGE,		"DutyIsTabViewer", "DRR_CHANGE",	    DutyDragDropTabViewerCf);
	DdxRegister(this, DRR_NEW,			"DutyIsTabViewer", "DRR_NEW",		    DutyDragDropTabViewerCf);
	DdxRegister(this, DRR_DELETE,		"DutyIsTabViewer", "DRR_DELETE",		DutyDragDropTabViewerCf);
	DdxRegister(this, RELDRR,			"DutyIsTabViewer", "RELDRR",			DutyDragDropTabViewerCf);

	DdxRegister(this, DRG_CHANGE,		"DutyDragDropTabViewer", "DRG_CHANGE",	    DutyDragDropTabViewerCf);
	DdxRegister(this, DRG_NEW,			"DutyDragDropTabViewer", "DRG_NEW",		    DutyDragDropTabViewerCf);
	DdxRegister(this, DRG_DELETE,		"DutyDragDropTabViewer", "DRG_DELETE",		DutyDragDropTabViewerCf);
	//DdxRegister(this, RELDRR,			"DutyDragDropTabViewer", "RELDRR",			DutyDragDropTabViewerCf);
CCS_CATCH_ALL;

	pomDynTab   = NULL;
	prmViewInfo = NULL;
	pomGroups   = NULL;
	pomStfData  = NULL;
}

//-----------------------------------------------------------------------------------------------

DutyDragDropTabViewer::~DutyDragDropTabViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
}

//-----------------------------------------------------------------------------------------------

void DutyDragDropTabViewer::Attach(CCSDynTable *popTable)
{
    pomDynTab = popTable;
	prmTableData = pomDynTab->GetTableData();
}

//-----------------------------------------------------------------------------------------------

void DutyDragDropTabViewer::ChangeView(bool bpSetHNull /*=true*/,bool bpSetVNull /*=true*/)
{
	CCS_TRY;
	pomStfData  = pomParent->pomStfData;
	prmViewInfo = pomParent->prmViewInfo;
	pomGroups   = pomParent->pomGroups;
	
	omPTypes.RemoveAll();

	ExtractItemList(prmViewInfo->oPType, &omPTypes, ',');
	int ilNrOfTypes = omPTypes.GetSize();

	int ilStfSize = 0;
	if(prmViewInfo->iGroup  == 2)
		ilStfSize = pomGroups->GetSize(); //Gruppen-Darstellung
	else
		ilStfSize = pomStfData->GetSize();//Mitarbeiter-Darstellung

	ilStfSize = (ilStfSize*ilNrOfTypes)-1;
	if(ilStfSize < 0) ilStfSize = 0;

	int ilDays = 0;
	if(prmViewInfo->oSelectDate.GetStatus() == COleDateTime::valid)
	{
		omShowFrom = prmViewInfo->oSelectDate;
		omShowTo   = prmViewInfo->oSelectDate;
		omFrom	   = prmViewInfo->oSelectDate;
		omTo       = prmViewInfo->oSelectDate;

		COleDateTimeSpan olDays = omShowTo - omShowFrom;
		imDays = olDays.GetDays()+1;
		ilDays = imDays - 1;
	}
	else
	{
		imDays = 0;
		ilDays = 0;
	}

	int ilColumnStartPos = 0;
	int ilRowStartPos = 0;
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);

	if(!bpSetHNull)
	{
		ilColumnStartPos = rlTableInfo.iColumnOffset;
		//Berichtigung des Offset
		int ilColumnDiff = (ilDays+1) - ilColumnStartPos;
		if(ilColumnDiff < rlTableInfo.iColumns)
		{
			ilColumnStartPos = ilColumnStartPos - (rlTableInfo.iColumns - ilColumnDiff);
		}
		if(ilColumnStartPos < 0)
			ilColumnStartPos = 0;
	}
	if(!bpSetVNull)
	{
		ilRowStartPos = rlTableInfo.iRowOffset;
		//Berichtigung des Offset
		int ilRowDiff = (ilStfSize + 1) - ilRowStartPos;
		if(ilRowDiff < rlTableInfo.iRows)
		{
			ilRowStartPos = ilRowStartPos - (rlTableInfo.iRows - ilRowDiff);
		}
		if(ilRowStartPos < 0)
			ilRowStartPos = 0;
	}

	pomDynTab->SetHScrollData(0,ilDays,ilColumnStartPos);
	pomDynTab->SetVScrollData(0,ilStfSize,ilRowStartPos);
	
	InitTableArrays(ilColumnStartPos);
	MakeTableData(ilColumnStartPos,ilRowStartPos);
	pomDynTab->InvalidateRect(NULL);
	CCS_CATCH_ALL;
}

//-----------------------------------------------------------------------------------------------

static void DutyDragDropTabViewerCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	DutyDragDropTabViewer *polViewer = (DutyDragDropTabViewer *)popInstance;
	
	// APO 08.01.2000: Umstellung ROS/DSR auf DRR
	// ToDo: DRR-BCs registrieren, sobald/falls neuer Mechanismus zum Sortieren
	// nach Gruppen gefunden wurde (Problematik mehrere Schichten pro Tag/MA)
	
	// BDA 15.02.2000  Umstellung ROS/DSR auf DRR
	// Gruppensortieren erfolgt nach DRG.WGPS (Arbeitsgruppencode),
	// mehrere Schichten pro Tag/MA: es wird immer nur die Schicht Nr.1 genommen (DRR/DRD.DRRN == 1)
	
	switch (ipDDXType)
	{
	default:
		return;
	case DRR_NEW	:
	    polViewer->ProcessDrrChange((DRRDATA *)vpDataPointer);
		break;
	case DRR_CHANGE	:		
	    polViewer->ProcessDrrChange((DRRDATA *)vpDataPointer);
		break;
	case DRR_DELETE	:	
	    polViewer->ProcessDrrDelete((DRRDATA *)vpDataPointer);
		break;
	case RELDRR		:		
		polViewer->ProcessReload();
		break;
	case DRG_NEW:
		polViewer->ProcessDrgNew((DRGDATA *)vpDataPointer);
		break;
	case DRG_CHANGE:
		polViewer->ProcessDrgChange((DRGDATA *)vpDataPointer);
		break;
	//case RELDRR:
	//	polViewer->ProcessReload();
	//	break;
	case DRG_DELETE:
		// ToDo: ProcessDelete(xxx) implementieren
		break;
	}
}

//-----------------------------------------------------------------------------------------------

void DutyDragDropTabViewer::HorzTableScroll(MODIFYTAB *prpModiTab)
{
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
	if(prpModiTab->iID == rlTableInfo.iID)
	{
		MakeTableData(prpModiTab->iNewHorzPos,prpModiTab->iNewVertPos);
		pomDynTab->ChangedTableData(prpModiTab->iNewHorzPos,prpModiTab->iNewVertPos);
	}
	else
	{
		MakeTableData(prpModiTab->iNewHorzPos,rlTableInfo.iRowOffset);
		pomDynTab->ChangedTableData(prpModiTab->iNewHorzPos,rlTableInfo.iRowOffset);
	}
}

//-----------------------------------------------------------------------------------------------

void DutyDragDropTabViewer::VertTableScroll(MODIFYTAB *prpModiTab)
{
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
	if(prpModiTab->iID == rlTableInfo.iID)
	{
		MakeTableData(prpModiTab->iNewHorzPos,prpModiTab->iNewVertPos);
		pomDynTab->ChangedTableData(prpModiTab->iNewHorzPos,prpModiTab->iNewVertPos);
	}
	else
	{
		MakeTableData(rlTableInfo.iColumnOffset,prpModiTab->iNewVertPos);
		pomDynTab->ChangedTableData(rlTableInfo.iColumnOffset,prpModiTab->iNewVertPos);
	}
}

//-----------------------------------------------------------------------------------------------

void DutyDragDropTabViewer::TableSizing(/*UINT nSide, */LPRECT lpRect)
{
	pomDynTab->SizeTable(/*nSide, */lpRect);
}

//-----------------------------------------------------------------------------------------------

void DutyDragDropTabViewer::MoveTable(MODIFYTAB *prpModiTab)
{
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
	if(prpModiTab->iID == rlTableInfo.iID)
	{
		if(prmTableData->oSolid.GetSize() > 0 && prmTableData->oHeader.GetSize() > 0)
		{
			MakeTableData(prpModiTab->iNewHorzPos,prpModiTab->iNewVertPos);
		}
	}
	else
	{
		int ilX = 0;
		int ilY = 0;
		//ilX = prpModiTab->oNewRect.left   - prpModiTab->oOldRect.left;
		//ilX = prpModiTab->oNewRect.right  - prpModiTab->oOldRect.right;
		//ilY = prpModiTab->oNewRect.top    - prpModiTab->oOldRect.top;
		//ilY = prpModiTab->oNewRect.bottom - prpModiTab->oOldRect.bottom;

		pomDynTab->MoveTable(ilX,ilY);
	}
}

//-----------------------------------------------------------------------------------------------

// APO 08.01.2000: Umstellung ROS/DSR auf DRR
// ToDo: Code umschreiben, sobald/falls neuer Mechanismus zum Sortieren
// nach Gruppen gefunden wurde (Problematik mehrere Schichten pro Tag/MA)

// BDA 15.02.2000  Umstellung ROS/DSR auf DRR
// Gruppensortieren erfolgt nach DRG.WGPS (Arbeitsgruppencode),
// mehrere Schichten pro Tag/MA: es wird immer nur die Schicht Nr.1 genommen (DRR/DRD.DRRN == 1)
void DutyDragDropTabViewer::ProcessDrgChange(DRGDATA *prpDrg)
{
	// diese Funktion ist hier ganz falsch! Das ist doch nur ein View? Warum soll er sich mit Datenupdates besch�ftigen?
return;


// ToDo:  prpRosFieldData durch prpDrr ersetzen!
/*
	if(prmViewInfo->bCorrectViewInfo == false)
		return;

	ROSFIELDDATA rlRosFieldData;

	rlRosFieldData.Urno			= prpRosFieldData->Urno;
	rlRosFieldData.PPText		= prpRosFieldData->PPText;
	rlRosFieldData.OldPPText	= prpRosFieldData->OldPPText;
	rlRosFieldData.BCText		= prpRosFieldData->BCText;
	rlRosFieldData.OldBCText	= prpRosFieldData->OldBCText;
	rlRosFieldData.Item			= prpRosFieldData->Item;
	int ilPTypeSize = omPTypes.GetSize();
	ROSDATA *prlRos = ogRosData.GetRosByUrno(rlRosFieldData.Urno);
	
	if(prlRos != NULL)
	{
		TABLEINFO rlTableInfo;
		pomDynTab->GetTableInfo(rlTableInfo);
		// Get Row
		int ilStfNr = -1;
		if(prmViewInfo->iGroup == 2)
		{
			int ilStfSize = pomGroups->GetSize();
			for(int i=0; i<ilStfSize; i++)
			{
				if((*pomGroups)[i].lStfUrno == prlRos->Stfu)
				{
					// MA gefunden
					ilStfNr = i;
					// Schleife abbrechen
					i = ilStfSize;
				}
			}
		}
		else
		{
			int ilStfSize = pomStfData->GetSize();
			for(int i=0; i<ilStfSize; i++)
			{
				if((*pomStfData)[i].Urno == prlRos->Stfu)
				{
					ilStfNr = i;
					i = ilStfSize;
				}
			}
		}

		int ilTypeNr = -1;
		for(int i=0; i<ilPTypeSize; i++)
		{
			if(omPTypes[i] == prlRos->Type)
			{
				ilTypeNr = i;
				i = ilPTypeSize;
			}
		}
		if(ilStfNr >= 0 && ilTypeNr >= 0)
		{
			int ilUpdateRow = ilStfNr*ilPTypeSize+ilTypeNr;
			// Get Column
			COleDateTime olUpdateDay;
			olUpdateDay.SetDate(atoi(prlRos->Romo.Mid(0,4)),atoi(prlRos->Romo.Mid(4,2)),rlRosFieldData.Item+1);

			COleDateTimeSpan olDayDiv =  olUpdateDay - omShowFrom;
			long llUpdateColumn = olDayDiv.GetDays();
			//if(llUpdateColumn >= 0 && (llUpdateColumn >= rlTableInfo.iColumnOffset && llUpdateColumn <= rlTableInfo.iColumnOffset + rlTableInfo.iColumns) && (ilUpdateRow >= rlTableInfo.iRowOffset && ilUpdateRow <= rlTableInfo.iRowOffset + rlTableInfo.iRows))
			if(llUpdateColumn == 0 && (ilUpdateRow >= rlTableInfo.iRowOffset && ilUpdateRow <= rlTableInfo.iRowOffset + rlTableInfo.iRows))
			{
				int ilColumn = llUpdateColumn - rlTableInfo.iColumnOffset;
				int ilRow    = ilUpdateRow - rlTableInfo.iRowOffset;
				ChangeFieldText(ilRow, ilColumn, rlRosFieldData.BCText, _SOLID);

				if(rlRosFieldData.BCText != "")
				{
					ChangeFieldBkColor(ilRow, ilColumn, BPN_WHITE, _SOLID);
					ChangeFieldDragDropStatus(ilRow, ilColumn, true, false, _SOLID);
					ChangeFieldDragDropStatus(ilRow, 0, true, false, _TABLE);
					ChangeFieldDragDropStatus(ilRow, 1, true, false, _TABLE);
				}
				else
				{
					ChangeFieldBkColor(ilRow, ilColumn, BPN_LIGHTSILVER1, _SOLID);
					ChangeFieldDragDropStatus(ilRow, ilColumn, false, false, _SOLID);
					ChangeFieldDragDropStatus(ilRow, 0, false, false, _TABLE);
					ChangeFieldDragDropStatus(ilRow, 1, false, false, _TABLE);
				}
				pomDynTab->ChangedFieldData(ilUpdateRow, -1);
				pomDynTab->ChangedFieldData(ilUpdateRow, 0);
				pomDynTab->ChangedFieldData(ilUpdateRow, 1);
			}
		}
	}
*/
}

//-----------------------------------------------------------------------------------------------

// APO 08.01.2000: Umstellung ROS/DSR auf DRR
// ToDo: Code umschreiben, sobald/falls neuer Mechanismus zum Sortieren
// nach Gruppen gefunden wurde (Problematik mehrere Schichten pro Tag/MA)
void DutyDragDropTabViewer::ProcessDrgNew(DRGDATA *prpDrg)
{
/*
// ToDo: prpRosData durch prpDrr ersetzen
	if(prmViewInfo->bCorrectViewInfo == false)
		return;

	ROSFIELDDATA rlRosFieldData;
	rlRosFieldData.Urno	= prpRosData->Urno;
	rlRosFieldData.OldBCText	= "";

	COleDateTime olDay;
	olDay.SetDate(atoi(prpRosData->Romo.Mid(0,4)),atoi(prpRosData->Romo.Mid(4,2)),1);
	int ilDays = CedaDataHelper::GetDaysOfMonth(olDay);
	int olBcSize = prpRosData->Bc.GetSize();
	for(int i=0; i<ilDays; i++)
	{
		if(i<olBcSize)
			rlRosFieldData.BCText	= prpRosData->Bc[i];
		else
			rlRosFieldData.BCText	= "";
		rlRosFieldData.Item	= i;
		ProcessDrrChange(&rlRosFieldData); // !! Argument ist anders - drr
	}
*/
}

/*
//-----------------------------------------------------------------------------------------------


// APO 08.01.2000: Umstellung ROS/DSR auf DRR
// ToDo: Code umschreiben, sobald/falls neuer Mechanismus zum Sortieren
// nach Gruppen gefunden wurde (Problematik mehrere Schichten pro Tag/MA)

// BDA 15.02.2000  Umstellung ROS/DSR auf DRR
// Gruppensortieren erfolgt nach DRG.WGPS (Arbeitsgruppencode),
// mehrere Schichten pro Tag/MA: es wird immer nur die Schicht Nr.1 genommen (DRR/DRD.DRRN == 1)
void DutyDragDropTabViewer::ProcessReload()
{
	if(prmViewInfo->bCorrectViewInfo == false)
		return;

	AfxGetApp()->DoWaitCursor(1);
	//pomDynTab->EndIPEditandDragDropAction();
	ChangeView(false,false);
	RemoveWaitCursor();
}
*/

//-----------------------------------------------------------------------------------------------

void DutyDragDropTabViewer::InitTableArrays(int ipColumnOffset /*=0*/)
{
	CString olText; 
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
	pomDynTab->ResetContent();
	
	//** SolidHeader
	prmTableData->oSolidHeader.oText		= "";
	prmTableData->oSolidHeader.oTextColor	= BPN_NOCOLOR;
	prmTableData->oSolidHeader.oBkColorNr	= BPN_NOCOLOR;
	if(0<imDays)
	{
		GetDayStrg(ipColumnOffset, olText);
		prmTableData->oSolidHeader.oBkColorNr = BPN_SILVERGREEN;
	}
	prmTableData->oSolidHeader.oText = olText;

	//** TableHeader
	for(int ilColumn=0;ilColumn<rlTableInfo.iColumns;ilColumn++)
	{
		olText.Empty();
		FIELDDATA *polFieldData = new FIELDDATA;
		//olText.Format("%d",ipColumnOffset + ilColumn);//test
		polFieldData->oTextColor	= BPN_NOCOLOR;
		polFieldData->oBkColorNr	= BPN_NOCOLOR;
		switch(ilColumn)
		{
			case 0:
				if(prmViewInfo->iEName == 1) //Code
					olText = CString(" ") + LoadStg(SHIFT_SHEET_C_CODE);
				else if(prmViewInfo->iEName == 2 || prmViewInfo->iEName == 4) //Name
					olText = CString(" ") + LoadStg(SHIFT_S_STAFF);
				else if(prmViewInfo->iEName == 3) //Kurzname
					olText = CString(" ") + LoadStg(IDS_STRING1807);

				polFieldData->oText = olText;
				break;
			case 1:
				olText = CString(" ") + LoadStg(IDS_STRING1692); // Group/Function
				polFieldData->oText = olText;
				break;
			default:
				polFieldData->oText = "";
				break;
		}
		prmTableData->oHeader.Add((void*)polFieldData);
	}
}

//-----------------------------------------------------------------------------------------------

void DutyDragDropTabViewer::MakeTableData(int ipColumnOffset,int ipRowOffset)
{
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
	//****************************************************************
	int ilActualRows = prmTableData->oSolid.GetSize();
	//** Add rows by SIZING
	while(rlTableInfo.iRows > ilActualRows)
	{
		AddRow(ilActualRows,rlTableInfo.iColumns,ipColumnOffset,ipRowOffset+ilActualRows);
		ilActualRows = prmTableData->oSolid.GetSize();
	}
	//** Delete rows by SIZING
	while(rlTableInfo.iRows < ilActualRows)
	{
		DelRow(ilActualRows-1);
		ilActualRows = prmTableData->oSolid.GetSize();
	}

	int ilActualColumns = prmTableData->oHeader.GetSize();
	//*** Add columns by SIZING
	while(ilActualRows>0 && rlTableInfo.iColumns > ilActualColumns)
	{
		AddColumn(ilActualColumns,ipColumnOffset+ilActualColumns,ipRowOffset);
		ilActualColumns = prmTableData->oHeader.GetSize();
	}
	//** Delete columns by SIZING
	while(ilActualRows>0 && rlTableInfo.iColumns < ilActualColumns)
	{
		DelColumn(ilActualColumns-1);
		ilActualColumns = prmTableData->oHeader.GetSize();
	}
	//****************************************************************
	int ilColumnDiv = ipColumnOffset-rlTableInfo.iColumnOffset;
	if(ilColumnDiv != 0)
	{
		//** SCROLL right
		if(ilColumnDiv > 0)
		{
			if(ilColumnDiv > prmTableData->oHeader.GetSize())
				ilColumnDiv = prmTableData->oHeader.GetSize();
			for(int i=0; i < ilColumnDiv;i++)
			{
				DelColumn(0);
				AddColumn(prmTableData->oHeader.GetSize(),ipColumnOffset+rlTableInfo.iColumns-ilColumnDiv+i,ipRowOffset);
			}
		}
		//** SCROLL left
		if(ilColumnDiv < 0)
		{
			ilColumnDiv = -(ilColumnDiv);
			if(ilColumnDiv > prmTableData->oHeader.GetSize())
				ilColumnDiv = prmTableData->oHeader.GetSize();
			for(int i=0; i < ilColumnDiv; i++)

			{
				DelColumn(prmTableData->oHeader.GetSize()-1);
				AddColumn(0,ipColumnOffset+ilColumnDiv-i-1,ipRowOffset);
			}
		}
	}
	int ilRowDiv = ipRowOffset-rlTableInfo.iRowOffset;
	if(ilRowDiv != 0)
	{
		//** SCROLL up
		if(ilRowDiv > 0)
		{
			if(ilRowDiv > prmTableData->oSolid.GetSize())
				ilRowDiv = prmTableData->oSolid.GetSize();
			for(int i=0; i < ilRowDiv; i++)
			{
				DelRow(0);
				AddRow(prmTableData->oSolid.GetSize(),rlTableInfo.iColumns,ipColumnOffset,ipRowOffset+rlTableInfo.iRows-ilRowDiv+i);
			}
		}
		//** SCROLL down
		if(ilRowDiv < 0)
		{
			ilRowDiv = -(ilRowDiv);
			if(ilRowDiv > prmTableData->oSolid.GetSize())
				ilRowDiv = prmTableData->oSolid.GetSize();
			for(int i=0; i < ilRowDiv; i++)
			{
				DelRow(prmTableData->oSolid.GetSize()-1);
				AddRow(0,rlTableInfo.iColumns,ipColumnOffset,ipRowOffset+ilRowDiv-i-1);
			}
		}
	}
}

//-----------------------------------------------------------------------------------------------

void DutyDragDropTabViewer::AddRow(int ipRow, int ipColumns, int ipColumnNr, int ipRowNr)
{
	CString olText;

	CString olRosl = "";
	int ilStfNr = 0;
	bool blGetRaT = GetElementAndPType(ipRowNr, ilStfNr, olRosl);
	bool blDragIsOk = false;

	//** Solid
	FIELDDATA *polFieldData = new FIELDDATA;

	polFieldData->oTextColor					= BPN_NOCOLOR;
	polFieldData->oBkColorNr					= BPN_NOCOLOR;
	polFieldData->bInplaceEdit					= false;
	polFieldData->bDrop							= false;
	polFieldData->bDrag							= false;
	polFieldData->iRButtonDownStatus			= RBDS_NOTHING;

	bool blIsReleased = false;
	bool blIsActive = false;

	if (blGetRaT &&
	  ((prmViewInfo->iGroup != 2 && ilStfNr < pomStfData->GetSize()) || 
	   (prmViewInfo->iGroup == 2 && ilStfNr < pomGroups->GetSize())))
	{
		long llTmpUrno = 0;

		if (prmViewInfo->iGroup == 2)
		{
			llTmpUrno = (*pomGroups)[ilStfNr].lStfUrno;
		}
		else
		{
			llTmpUrno = (*pomStfData)[ilStfNr].Urno;
		}

		COleDateTime olDay = omShowFrom;

		CString olSday = olDay.Format("%Y%m%d");

		DRRDATA *prlDrr = NULL;
		prlDrr = ogDrrData.GetDrrByKey(olSday, llTmpUrno, "1" , olRosl); 

		if (prlDrr != NULL)
		{
			olText = prlDrr->Scod;
			if (strcmp(prlDrr->Ross, "A") == 0)
			{
				blIsActive = true;
			}
			else
			{
				blIsReleased = true;
			}
		}


		// olRosl = DRR.ROSL
		// olRosl = 1			- Schichtplan
		// olRosl = U			- Urlaub
		// olRosl = W			- W�nsche
		// olRosl = L			- Langzeitplan
		// olRosl = 2			- Dienstplan
		// olRosl = 3			- Mitarbeitertagesliste
		// olRosl = 4			- 1. Nachbearbeitung
		// olRosl = 5			- 2. Nachbearbeitung

		if (blIsActive)
		{
			polFieldData->bDrag	= true;
			polFieldData->bDrop	= true;
			polFieldData->oBkColorNr = BPN_WHITE;
		}
		else if (blIsReleased)
		{
			polFieldData->bDrop	= false;
			polFieldData->bDrag	= false;
			polFieldData->oBkColorNr = BPN_LIGHTSILVER1;
		}
		else
		{
			polFieldData->bDrop		 = false;
			polFieldData->bDrag	     = false;
			polFieldData->oBkColorNr = BPN_SILVERGREENLIGHT;
		}



		if (olText == "")
		{
			olText = "-";
			//polFieldData->bDrop	= false;
			//polFieldData->bDrag	= false;
			//polFieldData->oBkColorNr = BPN_LIGHTSILVER1;
		}

		//if(blIsReleased == false)
		//{
		//	polFieldData->bDrop		 = false;
		//	polFieldData->bDrag	     = false;
		//	polFieldData->oBkColorNr = BPN_SILVERGREENLIGHT;
		//}

		if (llTmpUrno == 0)
		{
			polFieldData->bDrop		 = false;
			polFieldData->bDrag	     = false;
			polFieldData->oBkColorNr = BPN_LIGHTSILVER1;
		}

		if (olDay < omFrom || olDay > omTo)
		{
			polFieldData->bDrop		 = false;
			polFieldData->bDrag	     = false;
			polFieldData->oBkColorNr = BPN_LIGHTSILVER1;
		}

		if (olRosl == "1")
		{
			polFieldData->bDrop		 = false;
			polFieldData->bDrag	     = false;
			polFieldData->oBkColorNr = BPN_LIGHTSILVER1;
		}
		//else if(olRosl == "2") ;
		//else if(olRosl == "3") ;
		//else if(olRosl == "4") ;
		//else if(olRosl == "U")
		//else if(olRosl == "W")
	}

	// Breite Linien einf�gen
	if (ilStfNr+1 < pomGroups->GetSize())
	{
		if ((*pomGroups)[ilStfNr].iColor1Nr != (*pomGroups)[ilStfNr+1].iColor1Nr)
		{
			polFieldData->iBLineWidth = NORMAL_LINE;
		}
	}
	
	polFieldData->oText	= olText;
	blDragIsOk			= polFieldData->bDrag;

	prmTableData->oSolid.InsertAt(ipRow,(void*)polFieldData);

	//** Table
	CPtrArray *polTableValue = new CPtrArray;

	for (int ilColumn = 0; ilColumn < ipColumns; ilColumn++)
	{
		olText = "";
		FIELDDATA *polFieldData = new FIELDDATA;
		polFieldData->oTextColor = BPN_NOCOLOR;
		polFieldData->oBkColorNr = BPN_NOCOLOR;
		polFieldData->rIPEAttrib.TextMaxLenght = 8;
		polFieldData->rIPEAttrib.TextMinLenght = 1;
		polFieldData->bInplaceEdit = false;
		polFieldData->bDrag = blDragIsOk;

		if     (olRosl == "1") polFieldData->oTextColor = GRAY;
		else if(olRosl == "2") polFieldData->oTextColor = BLACK;
		else if(olRosl == "3") polFieldData->oTextColor = BLUE;
		else if(olRosl == "4") polFieldData->oTextColor = PURPLE;
		else if(olRosl == "U") polFieldData->oTextColor = RED;
		else if(olRosl == "W") polFieldData->oTextColor = GREEN;

		switch(ilColumn)
		{
			case 0:
				// Name
				if(prmViewInfo->iGroup == 2)
				{
					if(ilStfNr < pomGroups->GetSize() && blGetRaT)
					{
						polFieldData->oBkColorNr = (*pomGroups)[ilStfNr].iColor1Nr;

						if ((*pomGroups)[ilStfNr].lStfUrno != 0)
						{
							
							if (prmViewInfo->iEName == 1) //Code/Initials
							{
								olText.Format(" %s",(*pomGroups)[ilStfNr].oStfPerc);
							}
							else if (prmViewInfo->iEName == 2) //Name
							{
								CString olVName,olNName;
								olVName = (*pomGroups)[ilStfNr].oStfFinm;
								olNName = (*pomGroups)[ilStfNr].oStfLanm;
								if(olVName.IsEmpty() == FALSE || olNName.IsEmpty() == FALSE)
								{
									olText.Format(" %s.%s",olVName.Left(1),olNName);
								}
							}
							else if (prmViewInfo->iEName == 3) //Shortname/Nickname
							{
								olText.Format(" %s",(*pomGroups)[ilStfNr].oStfShnm);
							}
							else if (prmViewInfo->iEName == 4) //free configurated
							{
								olText = " " + pogNameConfigurationDlg->GetNameString(
									(*pomGroups)[ilStfNr].oStfFinm.GetBuffer(0),
									(*pomGroups)[ilStfNr].oStfLanm.GetBuffer(0),
									(*pomGroups)[ilStfNr].oStfShnm.GetBuffer(0),
									(*pomGroups)[ilStfNr].oStfPerc.GetBuffer(0),
									(*pomGroups)[ilStfNr].oStfPeno.GetBuffer(0));
							}
						}

						// Breite Linien einf�gen
						if(ilStfNr+1 < pomGroups->GetSize())
						{
							if((*pomGroups)[ilStfNr].iColor1Nr != (*pomGroups)[ilStfNr+1].iColor1Nr)
							{
								polFieldData->iBLineWidth = NORMAL_LINE;
							}
						}
					}
				}
				else
				{
					if (ilStfNr < pomStfData->GetSize() && blGetRaT)
					{
						olText = CBasicData::GetFormatedEmployeeName(prmViewInfo->iEName, &(*pomStfData)[ilStfNr], " ", "");
					}
				}
				polFieldData->oText = olText;
				break;
			case 1:
				// Gruppe/Funktion
				if (prmViewInfo->iGroup == 2)
				{
					if (ilStfNr < pomGroups->GetSize() && blGetRaT)
					{
						olText = " ";
						olText += (*pomGroups)[ilStfNr].oWgpCode;

						if(!(*pomGroups)[ilStfNr].oWgpCode.IsEmpty() && !(*pomGroups)[ilStfNr].oPfcCode.IsEmpty())
						{
							// wenn Gruppe und Funktion nicht leer sind, ist ein Separator notwendig
							olText += "/";
						}
						olText += (*pomGroups)[ilStfNr].oPfcCode;

						polFieldData->oBkColorNr = (*pomGroups)[ilStfNr].iColor1Nr;
					}
					// Breite Linien einf�gen
					if (ilStfNr+1 < pomGroups->GetSize())
					{
						if ((*pomGroups)[ilStfNr].iColor1Nr != (*pomGroups)[ilStfNr+1].iColor1Nr)
						{
							polFieldData->iBLineWidth = NORMAL_LINE;
						}
					}
				}
				else
				{
					olText = ""; //" ... / ... / ...";
				}
				polFieldData->oText = olText;
				break;
			default:
				polFieldData->oText = "";
				break;
		}
		polTableValue->Add((void*)polFieldData);
	}
	prmTableData->oTable.InsertAt(ipRow,(void*)polTableValue);

}

//-----------------------------------------------------------------------------------------------

void DutyDragDropTabViewer::DelRow(int ipRow)
{
	//** Table Text Color

	CPtrArray *polTableValue = (CPtrArray*)prmTableData->oTable[ipRow];
	for(int i = polTableValue->GetSize()-1;i>=0;i--)
	{
		FIELDDATA *polFieldData = (FIELDDATA*)(*polTableValue)[i];
		delete polFieldData;
	}
	polTableValue->RemoveAll();
	delete polTableValue;
	prmTableData->oTable.RemoveAt(ipRow);

	//** Solid Text Color
	FIELDDATA *polFieldData = (FIELDDATA*)prmTableData->oSolid[ipRow];
	delete polFieldData;
	prmTableData->oSolid.RemoveAt(ipRow);

}

//-----------------------------------------------------------------------------------------------

void DutyDragDropTabViewer::AddColumn(int ipColumn, int ipColumnNr, int ipRowNr)
{
	//TRACE(" COL R%03d - C%03d\n",ipRowNr,ipColumnNr);
	CString olText; 

	//** Table
	int ilActualRows = prmTableData->oTable.GetSize();
	for(int ilRow = 0; ilRow<ilActualRows; ilRow++)
	{
		CPtrArray *polTableValue = (CPtrArray*)prmTableData->oTable[ilRow];

		FIELDDATA *polFieldData = new FIELDDATA;

		olText.Format("%d-%d",ipRowNr+ilRow,ipColumnNr);//test
		polFieldData->oText			= olText;
		polFieldData->oTextColor	= BPN_NOCOLOR;
		polFieldData->oBkColorNr	= BPN_NOCOLOR;
		polFieldData->rIPEAttrib.TextMaxLenght = 8;
		polFieldData->rIPEAttrib.TextMinLenght = 1;
		polFieldData->bInplaceEdit = false;

/*		if     (olRosl == "1") polFieldData->oTextColor = GRAY;
		else if(olRosl == "2") polFieldData->oTextColor = BLACK;
		else if(olRosl == "3") polFieldData->oTextColor = BLUE;
		else if(olRosl == "4") polFieldData->oTextColor = PURPLE;
		else if(olRosl == "U") polFieldData->oTextColor = RED;
		else if(olRosl == "W") polFieldData->oTextColor = GREEN;*/

		polTableValue->InsertAt(ipColumn,(void*)polFieldData);
	}

	//** Header
	FIELDDATA *polFieldData = new FIELDDATA;

	olText.Format("%d",ipColumnNr);//test
	polFieldData->oText			= olText;
	polFieldData->oTextColor	= BPN_NOCOLOR;
	polFieldData->oBkColorNr	= BPN_NOCOLOR;

	prmTableData->oHeader.InsertAt(ipColumn,(void*)polFieldData);
}

//-----------------------------------------------------------------------------------------------

void DutyDragDropTabViewer::DelColumn(int ipColumn)
{
	//** Table Text Color
	int ilActualRows = prmTableData->oTable.GetSize();
	for(int ilRow = 0; ilRow<ilActualRows; ilRow++)
	{
		CPtrArray *polTableValue = (CPtrArray*)prmTableData->oTable[ilRow];
		FIELDDATA *polFieldData = (FIELDDATA*)(*polTableValue)[ipColumn];
		delete polFieldData;
		polTableValue->RemoveAt(ipColumn);
	}

	//** Header Text Color
	FIELDDATA *polFieldData = (FIELDDATA*)prmTableData->oHeader[ipColumn];
	delete polFieldData;
	prmTableData->oHeader.RemoveAt(ipColumn);
}

//-----------------------------------------------------------------------------------------------

void DutyDragDropTabViewer::ChangeFieldText(int ipRow, int ipColumn, CString opText, int ipFieldType /*=_TABLE*/)
{
	switch(ipFieldType)
	{
		case _TABLE:
		if(ipRow < prmTableData->oTable.GetSize())
		{
			CPtrArray *polValue = (CPtrArray*)prmTableData->oTable[ipRow];
			if(ipColumn < polValue->GetSize())
			{
				FIELDDATA *polFieldData = (FIELDDATA*)(*polValue)[ipColumn];
				polFieldData->oText	= opText;
			}
		}
		break;
		case _HEADER:
		/*if(ipRow < prmTableData->oTable.GetSize())
		{
			CPtrArray *polValue = (CPtrArray*)prmTableData->oTable[ipRow];
			if(ipColumn < polValue->GetSize())
			{
				FIELDDATA *polFieldData = (FIELDDATA*)(*polValue)[ipColumn];
				polFieldData->oText	= opText;
			}
		}*/
		break;
		case _SOLID:
		if(ipRow < prmTableData->oSolid.GetSize())
		{
			FIELDDATA *polFieldData = (FIELDDATA*)prmTableData->oSolid[ipRow];
			polFieldData->oText	= opText;
		}
		break;
		case _SOLIDHEADER:
		/*if(ipRow < prmTableData->oTable.GetSize())
		{
			CPtrArray *polValue = (CPtrArray*)prmTableData->oTable[ipRow];
			if(ipColumn < polValue->GetSize())
			{
				FIELDDATA *polFieldData = (FIELDDATA*)(*polValue)[ipColumn];
				polFieldData->oText	= opText;
			}
		}*/
		break;

	}
}

//-----------------------------------------------------------------------------------------------

void DutyDragDropTabViewer::ChangeFieldTextColor(int ipRow, int ipColumn, COLORREF opColor, int ipFieldType /*=_TABLE*/)
{
	if(ipRow < prmTableData->oTable.GetSize())
	{
		CPtrArray *polValue = (CPtrArray*)prmTableData->oTable[ipRow];
		if(ipColumn < polValue->GetSize())
		{
			FIELDDATA *polFieldData = (FIELDDATA*)(*polValue)[ipColumn];
			polFieldData->oTextColor = opColor;
		}
	}
}

//-----------------------------------------------------------------------------------------------

void DutyDragDropTabViewer::ChangeFieldMarkerColor(int ipRow, int ipColumn, int ipColorNr, int ipMarker)
{
	if(ipRow < prmTableData->oTable.GetSize())
	{
		CPtrArray *polValue = (CPtrArray*)prmTableData->oTable[ipRow];
		if(ipColumn < polValue->GetSize())
		{
			FIELDDATA *polFieldData = (FIELDDATA*)(*polValue)[ipColumn];

			switch(ipMarker)
			{
				case MARKER_L_B:
					polFieldData->iLBeamColorNr = ipColorNr;
					break;
				case MARKER_R_B:
					polFieldData->iRBeamColorNr = ipColorNr;
					break;
				case MARKER_LT_T:
					polFieldData->iLTTriangleColorNr = ipColorNr;
					break;
				case MARKER_RT_T:
					polFieldData->iRTTriangleColorNr = ipColorNr;
					break;
				case MARKER_RB_T:
					polFieldData->iRBTriangleColorNr = ipColorNr;
					break;
				case MARKER_LB_T:
					polFieldData->iLBTriangleColorNr = ipColorNr;
					break;
			}
		}
	}
}

//-----------------------------------------------------------------------------------------------

int DutyDragDropTabViewer::ChangeFieldBkColor(int ipRow, int ipColumn, int ipBkColorNr, int ipFieldType /*=_TABLE*/)
{
	int ilOldBkColorNr = 0;
	switch(ipFieldType)
	{
		case _TABLE:
		if(ipRow < prmTableData->oTable.GetSize())
		{
			CPtrArray *polValue = (CPtrArray*)prmTableData->oTable[ipRow];
			if(ipColumn < polValue->GetSize())
			{
				FIELDDATA *polFieldData = (FIELDDATA*)(*polValue)[ipColumn];
				ilOldBkColorNr = polFieldData->oBkColorNr;
				polFieldData->oBkColorNr = ipBkColorNr;
			}
		}
		break;
		case _HEADER:
		/*
		if(ipRow < prmTableData->oTable.GetSize())
		{
			CPtrArray *polValue = (CPtrArray*)prmTableData->oTable[ipRow];
			if(ipColumn < polValue->GetSize())
			{
				FIELDDATA *polFieldData = (FIELDDATA*)(*polValue)[ipColumn];
				ilOldBkColorNr = polFieldData->oBkColorNr;
				polFieldData->oBkColorNr = ipBkColorNr;
			}
		}
		*/
		break;
		case _SOLID:
		if(ipRow < prmTableData->oSolid.GetSize())
		{
			FIELDDATA *polFieldData = (FIELDDATA*)prmTableData->oSolid[ipRow];
			polFieldData->oBkColorNr = ipBkColorNr;
		}
		break;
		case _SOLIDHEADER:
		/*
		if(ipRow < prmTableData->oTable.GetSize())
		{
			CPtrArray *polValue = (CPtrArray*)prmTableData->oTable[ipRow];
			if(ipColumn < polValue->GetSize())
			{
				FIELDDATA *polFieldData = (FIELDDATA*)(*polValue)[ipColumn];
				ilOldBkColorNr = polFieldData->oBkColorNr;
				polFieldData->oBkColorNr = ipBkColorNr;
			}
		}
		*/
		break;

	}
	return ilOldBkColorNr;
}

//-----------------------------------------------------------------------------------------------

int DutyDragDropTabViewer::GetFieldBkColor(int ipRow, int ipColumn)
{
	int ilOldBkColorNr = 0;
	if(ipRow < prmTableData->oTable.GetSize())
	{
		CPtrArray *polValue = (CPtrArray*)prmTableData->oTable[ipRow];
		if(ipColumn < polValue->GetSize())
		{
			FIELDDATA *polFieldData = (FIELDDATA*)(*polValue)[ipColumn];
			ilOldBkColorNr = polFieldData->oBkColorNr;
		}
	}
	return ilOldBkColorNr;
}

//-----------------------------------------------------------------------------------------------

void DutyDragDropTabViewer::ChangeFieldDragDropStatus(int ipRow, int ipColumn, bool bpDrag, bool bpDrop, int ipFieldType /*=_TABLE*/)
{
	switch(ipFieldType)
	{
		case _TABLE:
		if(ipRow < prmTableData->oTable.GetSize())
		{
			CPtrArray *polValue = (CPtrArray*)prmTableData->oTable[ipRow];
			if(ipColumn < polValue->GetSize())
			{
				FIELDDATA *polFieldData = (FIELDDATA*)(*polValue)[ipColumn];
				polFieldData->bDrag	= bpDrag;
				polFieldData->bDrop	= bpDrop;
			}
		}
		break;
		case _HEADER:
		/*if(ipRow < prmTableData->oTable.GetSize())
		{
			CPtrArray *polValue = (CPtrArray*)prmTableData->oTable[ipRow];
			if(ipColumn < polValue->GetSize())
			{
				FIELDDATA *polFieldData = (FIELDDATA*)(*polValue)[ipColumn];
				polFieldData->bDrag	= bpDrag;
				polFieldData->bDrop	= bpDrop;
			}
		}*/
		break;
		case _SOLID:
		if(ipRow < prmTableData->oSolid.GetSize())
		{
			FIELDDATA *polFieldData = (FIELDDATA*)prmTableData->oSolid[ipRow];
				polFieldData->bDrag	= bpDrag;
				polFieldData->bDrop	= bpDrop;
		}
		break;
		case _SOLIDHEADER:
		/*if(ipRow < prmTableData->oTable.GetSize())
		{
			CPtrArray *polValue = (CPtrArray*)prmTableData->oTable[ipRow];
			if(ipColumn < polValue->GetSize())
			{
				FIELDDATA *polFieldData = (FIELDDATA*)(*polValue)[ipColumn];
				polFieldData->bDrag	= bpDrag;
				polFieldData->bDrop	= bpDrop;
			}
		}*/
		break;

	}
}


//-----------------------------------------------------------------------------------------------

int DutyDragDropTabViewer::GetDayStrg(int ipDayOffset, CString &opText)
{
	COleDateTime olDay = omShowFrom;
	olDay += COleDateTimeSpan(ipDayOffset,0,0,0);

	int ilDayOfWeek = olDay.GetDayOfWeek();
	switch(ilDayOfWeek)
	{
		case 1:
			opText = LoadStg(DUTY_SUN);
			break;
		case 2:
			opText = LoadStg(DUTY_MON);
			break;
		case 3:
			opText = LoadStg(DUTY_TUE);
			break;
		case 4:
			opText = LoadStg(DUTY_WED);
			break;
		case 5:
			opText = LoadStg(DUTY_THU);
			break;
		case 6:
			opText = LoadStg(DUTY_FRI);
			break;
		case 7:
			opText = LoadStg(DUTY_SAT);
			break;
	}

	opText += olDay.Format(" %d.%m.");

	return ilDayOfWeek;
}

//-----------------------------------------------------------------------------------------------

bool DutyDragDropTabViewer::GetElementAndPType(int ipRowNr, int &ipRow, CString &opPType)
{
	int ilPTypeSize = omPTypes.GetSize();

	if(ilPTypeSize < 1) return false;

	double dlBruch = (double)ipRowNr/(double)ilPTypeSize;
	double dlDivisor = 1.0/(double)ilPTypeSize - 0.0000000000001;
	double dlRow = floor(dlBruch);
	ipRow = (int)dlRow;

	int ilTypNr = (int)(floor(((dlBruch-dlRow)/dlDivisor)));

	if(ilTypNr>ilPTypeSize) return false;
	opPType = omPTypes[ilTypNr];

	return true;	
}

//-----------------------------------------------------------------------------------------------

void DutyDragDropTabViewer::ProcessDrrChange(DRRDATA* popDrr)
{
	ChangeView(false,false);	
}

void DutyDragDropTabViewer::ProcessDrrDelete(DRRDATA* popDrr)
{
	ChangeView(false,false);	
}

void DutyDragDropTabViewer::ProcessReload()
{
	ChangeView(false,false);	
}








