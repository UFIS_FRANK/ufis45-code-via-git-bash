#if !defined(AFX_PSDUTYROSTER5VIEWPAGE_H__CA42CC61_1904_11D3_8EBF_00001C034EA0__INCLUDED_)
#define AFX_PSDUTYROSTER5VIEWPAGE_H__CA42CC61_1904_11D3_8EBF_00001C034EA0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// PSDutyRoster5ViewPage.h : header file
//
#include <stdafx.h>
#include <Ansicht.h>
#include <RosterViewPage.h>
//#include "CCSEdit.h"
#include <DutyRoster_View.h>
#include <Accounts.h>

#define ACCOUNTCOLUMNS 2

class CGridControl;

// PSDutyRoster5ViewPage dialog

class PSDutyRoster5ViewPage : public RosterViewPage
{
	DECLARE_DYNCREATE(PSDutyRoster5ViewPage)

// Construction
public:
	PSDutyRoster5ViewPage();
	~PSDutyRoster5ViewPage();

// Dialog Data
	//{{AFX_DATA(PSDutyRoster5ViewPage)
	enum { IDD = IDD_PSDUTYROSTER5 };
	CButton	m_cl_CheckMainFuncOnly;
	CButton	m_cl_Konflikt;
	//}}AFX_DATA

	BOOL GetData();
	BOOL GetData(CStringArray &opValues);
	void SetData();
	void SetData(CStringArray &opValues);
	void SetAccounts(CAccounts *popAccounts);

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(PSDutyRoster5ViewPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Das Grid
	CGridControl *pomAccountList;
	// das Kontoobjekt
	CAccounts *pomAccounts;

	// Grid initialisieren
	void InitGrid ();
	void InitColWidths();
	int FillGrid();

	/***********************************************************************************
	Setzt eine Zeile in der Accountsliste
	***********************************************************************************/
	void SetAccountRow(int ipCountEntries, ACCOUNTTYPEINFO* popAccInfo, bool bpFound);

	bool bmPgpLoaded; // wurde die PropertyPage initialisiert

	// Generated message map functions
	//{{AFX_MSG(PSDutyRoster5ViewPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	CString omCalledFrom;
	void SetCalledFrom(CString opCalledFrom);

private:
	CString omTitleStrg;
	void SetButtonText();
	void SetStatic();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PSDUTYROSTER5VIEWPAGE_H__CA42CC61_1904_11D3_8EBF_00001C034EA0__INCLUDED_)
