#ifndef AFX_SHIFTPLANDLG_H__388BF491_D374_11D1_BFD0_004095434A85__INCLUDED_
#define AFX_SHIFTPLANDLG_H__388BF491_D374_11D1_BFD0_004095434A85__INCLUDED_

// ShiftPlanDlg.h : Header-Datei
//
#include <stdafx.h>
#include <resource.h>
//#include "RecordSet.h"
//#include "CedaBasicData.h"

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld ShiftPlanDlg 

class ShiftPlanDlg : public CDialog
{
// Konstruktion
public:
	ShiftPlanDlg(CString *popSPLName,CString *popOrgUnit,CString *popAction,CWnd* pParent = NULL);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(ShiftPlanDlg)
	enum { IDD = IDD_SHIFT_PLAN_DLG };
	CButton	m_B_Cancel;
	CComboBox	m_CB_Name;
	CComboBox	m_CB_OrgUnit;
	CButton	m_N_New;
	CButton	m_B_Delete;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(ShiftPlanDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(ShiftPlanDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnBDelete();
	afx_msg void OnBNew();
	virtual void OnOK();
	afx_msg void OnSelchangeCbName();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

protected:
	CString omObject;
	CString *pomSPLName;
	CString *pomOrgUnit;
	CString *pomAction;
	RecordSet *pomRecord;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio fügt zusätzliche Deklarationen unmittelbar vor der vorhergehenden Zeile ein.

#endif // AFX_SHIFTPLANDLG_H__388BF491_D374_11D1_BFD0_004095434A85__INCLUDED_
