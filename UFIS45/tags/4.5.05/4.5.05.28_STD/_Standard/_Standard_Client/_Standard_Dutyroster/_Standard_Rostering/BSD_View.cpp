// BSD_View.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//static int CompareBsd( const BSDDATA **e1, const BSDDATA **e2);
//static int CompareOda( const ODADATA **e1, const ODADATA **e2);


/////////////////////////////////////////////////////////////////////////////
// BSD_View

IMPLEMENT_DYNCREATE(BSD_View, CFormView)

//****************************************************************************************************
// Konstruktor: <ipOpenFrom> gibt an, welches Modul (Dienst- oder
//  Schichtplanung) den Dialog erzeugt hat.
//****************************************************************************************************

BSD_View::BSD_View()
	: CFormView(BSD_View::IDD)
{
	//{{AFX_DATA_INIT(BSD_View)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT

	// erzeugendes Modul (Schicht- oder Dienstplanung)
//	imOpenFrom = ipOpenFrom;

	// Zeiger auf Fenster des erzeugenden Moduls
//	pomParent = pParent;
}

//****************************************************************************************************
// Destruktor
//****************************************************************************************************

BSD_View::~BSD_View()
{
	// Arrays leeren
	omBsdLines.DeleteAll();
	omOdaLines.DeleteAll();
	// Tabellenobjekte l�schen
	delete pomBsdTable;
	delete pomOdaTable;
	// D'n'D-Objekt freigeben
//	omDragDropObject.Revoke();
}

void BSD_View::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(BSD_View)
		// HINWEIS: Der Klassen-Assistent f�gt hier DDX- und DDV-Aufrufe ein
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(BSD_View, CFormView)
	//{{AFX_MSG_MAP(BSD_View)
		ON_MESSAGE(WM_TABLE_DRAGBEGIN,		OnDragBegin)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Diagnose BSD_View

#ifdef _DEBUG
void BSD_View::AssertValid() const
{
	CFormView::AssertValid();
}

void BSD_View::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten BSD_View 

//****************************************************************************************************
// OnInitDialog: Initialisierung des Dialogs.
//  R�ckgabe:	BOOL ->		TRUE: kein Oberfl�chenelement soll den Fokus erhalten
//							FALSE: ein Oberfl�chenelement soll den Fokus erhalten
//****************************************************************************************************


void BSD_View::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();

	// Titel des Docs setzen.
	CRosteringDoc *polDoc = (CRosteringDoc *)GetDocument();
	if(polDoc != NULL)
	{
		polDoc->SetTitle(LoadStg(IDS_STRING1618));
	}

	// H�he und Breite einstellen

	int ilDlgWidth = 297;
	int ilDlgHight = 330;

	CRect olDlgRect, olParentRect; //, olSystemRect;
	// Bildschirmdimensionen ermitteln
/*	olSystemRect.left	= 0;
	olSystemRect.top	= 0;
	olSystemRect.right  = ::GetSystemMetrics(SM_CXSCREEN);
	olSystemRect.bottom = ::GetSystemMetrics(SM_CYSCREEN);*/

	// Einlesen des Client Bereiches des Hauptframes.(Mainframe)
	GetParentFrame()->GetParentFrame()->GetClientRect(&olParentRect);

	// Dimensionen des Views-Fensters einstellen
	olDlgRect.left		= olParentRect.left + 5;
	olDlgRect.top		= olParentRect.bottom - (ilDlgHight + 15);
	olDlgRect.right		= olDlgRect.left + ilDlgWidth;
	olDlgRect.bottom	= olDlgRect.top  + ilDlgHight;

	// Gr�sse des Dialogfensters anpassen, wenn es gr�sser als das
	// Parent-Window ist
	
	/*
	if(olDlgRect.right>olSystemRect.right)
	{
		int ilLeft = olDlgRect.right - olSystemRect.right;
		olDlgRect.left -= ilLeft;
		olDlgRect.right -= ilLeft;
	}
	if(olDlgRect.bottom>olSystemRect.bottom)
	{
		int ilTop = olDlgRect.bottom - olSystemRect.bottom;
		olDlgRect.top -= ilTop;
		olDlgRect.bottom -= ilTop;
	}*/

	// Einstellungen aktivieren
	GetParentFrame()->MoveWindow(&olDlgRect, TRUE);
	// Neu darstellen
	GetParentFrame()->RecalcLayout();
	
	// Objekt f�r Basisschichtentabelle erzeugen
	pomBsdTable = new CTable;
	// Mehrfachauswahl zulassen
	pomBsdTable->SetSelectMode(0);//LBS_MULTIPLESEL | LBS_EXTENDEDSEL

	// Objekt f�r Abwesenheitentabelle erzeugen
	pomOdaTable = new CTable;
	// Mehrfachauswahl zulassen
	pomOdaTable->SetSelectMode(0);//LBS_MULTIPLESEL | LBS_EXTENDEDSEL

	// Objekte zur Einstellung der Tabellengr�ssen
	CRect olRectBSD, olRectODA;
	
	// Gr�sse der Basisschichtentabelle einstellen
	olRectBSD.left	= 1;
	olRectBSD.top	= 0;
	olRectBSD.right = 285;
	olRectBSD.bottom = 198;

	// Gr�sse der Abwesenheitentabelle einstellen
	olRectODA.left	= olRectBSD.left;
	olRectODA.top	= olRectBSD.bottom+1;
	olRectODA.right = olRectBSD.right;
	olRectODA.bottom = 301;
	
	// Tabellenobjekte initialisieren
	pomBsdTable->SetTableData(this, 0, 0, 0, 0,
							::GetSysColor(COLOR_WINDOWTEXT), ::GetSysColor(COLOR_WINDOW),
							::GetSysColor(COLOR_HIGHLIGHTTEXT),::GetSysColor(COLOR_HIGHLIGHT),
							&ogCourier_Regular_8, &ogCourier_Regular_8);
	pomOdaTable->SetTableData(this, 0, 0, 0, 0,
							::GetSysColor(COLOR_WINDOWTEXT), ::GetSysColor(COLOR_WINDOW),
							::GetSysColor(COLOR_HIGHLIGHTTEXT),::GetSysColor(COLOR_HIGHLIGHT),
							&ogCourier_Regular_8, &ogCourier_Regular_8);


	//ab hier: UpdateDisplay_BSD_Table

	// Puffer f�r Header-Struktur der Tabellen
	CString olHeader;
	// Header f�r Basisschichten formatieren
	olHeader.Format("%s|%s|%s|%s|%s|%s|%s",LoadStg(IDS_STRING1619),LoadStg(SHIFT_BSD_ESBG),LoadStg(SHIFT_BSD_LSEN),
					LoadStg(SHIFT_BSD_BKF1),LoadStg(SHIFT_BSD_BKT1),LoadStg(SHIFT_BSD_BKD1),LoadStg(SHIFT_BSD_TYPE));

	// Header f�r Basisschichten einstellen
	pomBsdTable->SetHeaderFields(olHeader);
	// Spaltenbreiten einstellen
	pomBsdTable->SetFormatList("8|4|4|4|4|4|1");

	// Basisschichten-Array leeren
	omBsdLines.DeleteAll();
	// Tabellenobjekt aufr�umen
	pomBsdTable->ResetContent();
	// Basisschichten-Array f�llen
	int ilCount =  ogBsdData.omData.GetSize();
	for(int i = 0; i < ilCount; i++)
	{
	   omBsdLines.NewAt(omBsdLines.GetSize(), ogBsdData.omData[i]);
	}
	// Basisschichten-Array nach Code sortieren
	omBsdLines.Sort(CompareBsd);
	
	// Tabellen-Objekt mit Datens�tzen f�llen
	ilCount = omBsdLines.GetSize();
	for(i = 0; i < ilCount; i++)
	{
		BSDDATA rlBsd = omBsdLines[i];
		pomBsdTable->AddTextLine(Format_Table("BSD",&omBsdLines[i]),&omBsdLines[i]);
	}
	// Tabelle anzeigen
	pomBsdTable->DisplayTable();

	//end> UpdateDisplay_BSD_Table

	//ab hier: UpdateDisplay_ODA_Table

	// Spalten-Header formatieren
	olHeader.Format("%s|%s",LoadStg(IDS_STRING1620),LoadStg(IDS_STRING461));

	// Spalten-Header einstellen
	pomOdaTable->SetHeaderFields(olHeader);
	// SPaltenbreite einstellen
	pomOdaTable->SetFormatList("5|31");

	// Array leeren
	omOdaLines.DeleteAll();
	// Tabellen-Objekt aufr�umen
	pomOdaTable->ResetContent();
	// Abwesenheiten-Array f�llen
	ilCount =  ogOdaData.omData.GetSize();
	for(i = 0; i < ilCount; i++)
	{
	   omOdaLines.NewAt(omOdaLines.GetSize(), ogOdaData.omData[i]);
	}
	// Abwesenheiten-Array nach Code sortieren
	omOdaLines.Sort(CompareOda);

	// Tabellen-Objekt mit Datens�tzen f�llen
	ilCount = omOdaLines.GetSize();
	for(i = 0; i < ilCount; i++)
	{
		ODADATA rlOda = omOdaLines[i];
		pomOdaTable->AddTextLine(Format_Table("ODA",&omOdaLines[i]),&omOdaLines[i]);
	}
	// Abwesenheiten-Tabelle anzeigen
	pomOdaTable->DisplayTable();

	//end> UpdateDisplay_ODA_Table

	// Positionen der Tabellen einstellen
	pomBsdTable->SetPosition(olRectBSD.left,olRectBSD.right,olRectBSD.top,olRectBSD.bottom);
	pomOdaTable->SetPosition(olRectODA.left,olRectODA.right,olRectODA.top,olRectODA.bottom);
}


//****************************************************************************************************
// OnCancel: macht nichts. Wird �berschrieben, um unerw�nschtes Verhalten 
//  zu unterdr�cken.
//  R�ckgabe:	keine
//****************************************************************************************************
/*
void BSD_View::OnCancel() 
{
	//do nothing
}*/

//****************************************************************************************************
// OnOK: macht nichts.Wird �berschrieben, um unerw�nschtes Verhalten 
//  zu unterdr�cken.
//  R�ckgabe:	keine
//****************************************************************************************************
// Achtung: noch kein Messagehandler vorhanden

/*void BSD_View::OnOK() 
{
	//do nothing
}*/

//****************************************************************************************************
// InternalCancel: 
//****************************************************************************************************

void BSD_View::InternalCancel() 
{
	// Window schlie�en. (Es wird auf das MDI Frame Window zugegriffen)
	((CMDIChildWnd*) GetParentFrame())->MDIDestroy();
}

//****************************************************************************************************
// Format_Table(): formatiert die Daten in <*prpData> je nach
//  Datentyp <opTable> in einem String.
//  R�ckgabe: CString -> formatierter Datensatz
//****************************************************************************************************

CString BSD_View::Format_Table(CString opTable,void *prpData)
{
	// CString f�r R�ckgabe, speichert den formatierten Datensatz
	CString s;
	// Tabelle "Basisschichten"?
	if(opTable == "BSD")
	{	// ja -> entsprechende Typkonvertierung des Pointers vornehmen
		BSDDATA *prlBsd = (BSDDATA*) prpData;
		// Datenfelder lesen und im String speichern
		s =  CString(prlBsd->Bsdc) + "|";
		s+=  CString(prlBsd->Esbg) + "|";
		s+=  CString(prlBsd->Lsen) + "|";
		s+=  CString(prlBsd->Bkf1) + "|";
		s+=  CString(prlBsd->Bkt1) + "|";
		s+=  CString(prlBsd->Bkd1) + "|";
		s+=  CString(prlBsd->Type);
	}
	// Tabelle "Abwesenheiten"?
	else if(opTable == "ODA")
	{	// ja -> entsprechende Typkonvertierung des Pointers vornehmen
		ODADATA *prlOda = (ODADATA*) prpData;
		// Datenfelder lesen und im String speichern
		s =  CString(prlOda->Sdac) + "|";
		s+=  CString(prlOda->Sdan);
	}
	// formatierten Datensatz zur�ckgeben
	return s;
}

//****************************************************************************************************
// OnDragBegin(): Messagehandler f�r Drag and Drop-Mechanismus
//  R�ckgabe: long -> immer 0L
//****************************************************************************************************

LONG BSD_View::OnDragBegin(UINT wParam, LONG lParam)
{
	// wurde der Dialog von der Dienstplanung (Klasse DutyRoster_View())
	// aufgerufen?
/*	if(imOpenFrom == DUTYROSTER_BSD_DLG)
	{
		// ja -> zul�ssiger Status?
		if(ogPrivList.GetStat("DUTYROSTER_CHANGEFIELD") != '1') 
			return 0L;	// nein -> terminieren
	}*/

	// ItemID ermitteln
	int ilitemID = (int)wParam;

	// Zeiger auf Referenz-Listbox ermitteln
	CListBox *polTable = (CListBox*)lParam;

	// Zeiger auf die beiden vorhandenen Listboxen generieren
	CListBox *polBsdTab = pomBsdTable->GetCTableListBox();
	CListBox *polOdaTab = pomOdaTable->GetCTableListBox();

	// Listbox-Zeiger vergleichen, um Ursprung der D'n'D-Aktion zu
	// ermitteln
	if(polTable == polBsdTab)
	{
		// Ursprung = Basisschichten-Tabelle
		// Zeiger auf Datensatz ermitteln
		BSDDATA *prlBsd = NULL;
		prlBsd = (BSDDATA *)pomBsdTable->GetTextLineData(ilitemID);
		// Daten vorhanden?
		if(prlBsd != NULL)
		{	
			// ja
			// Aufruf durch Dienstplanung (Klasse DutyRoster_View())
//			if(imOpenFrom == DUTYROSTER_BSD_DLG)
//			{
				// D'n'D-Objekt initialisieren
				omDragDropObject.CreateDWordData(DIT_DUTYROSTER_BSD_TABLE, 1);
				// Basisschichten-Datenobjekt hinzuf�gen
				omDragDropObject.AddString(prlBsd->Bsdc);
				// Aktion fortsetzen
				omDragDropObject.BeginDrag();
//			}
			// Aufruf durch Schichtplanung (Klasse ShiftRoster_View())
/*			else if(imOpenFrom == SHIFTROSTER_BSD_DLG)
			{
				// D'n'D-Objekt initialisieren
				omDragDropObject.CreateDWordData(DIT_SHIFTROSTER_BSD_TABLE, 1);
				// Basisschichten-Datenobjekt hinzuf�gen
				omDragDropObject.AddDWord(prlBsd->Urno);
				// Aktion fortsetzen
				omDragDropObject.BeginDrag();
			}*/
		}
	}
	if(polTable == polOdaTab)
	{
		// Ursprung = Abwesenheiten-Tabelle
		// Zeiger auf Datensatz ermitteln
		ODADATA *prlOda = NULL;
		prlOda = (ODADATA *)pomOdaTable->GetTextLineData(ilitemID);
		// Daten vorhanden?
		if(prlOda != NULL)
		{
			// ja
			// Aufruf durch Dienstplanung (Klasse DutyRoster_View())
//			if(imOpenFrom == DUTYROSTER_BSD_DLG)
//			{
				// D'n'D-Objekt initialisieren
				omDragDropObject.CreateDWordData(DIT_DUTYROSTER_BSD_TABLE, 1);
				// Abwesenheiten-Datenobjekt hinzuf�gen
				omDragDropObject.AddString(prlOda->Sdac);
				// Aktion fortsetzen
				omDragDropObject.BeginDrag();
//			}
/*			// Aufruf durch Schichtplanung (Klasse ShiftRoster_View())
			else if(imOpenFrom == SHIFTROSTER_BSD_DLG)
			{
				// D'n'D-Objekt initialisieren
				omDragDropObject.CreateDWordData(DIT_SHIFTROSTER_BSD_TABLE, 1);
				// Abwesenheiten-Datenobjekt hinzuf�gen
				omDragDropObject.AddDWord(prlOda->Urno);
				// Aktion fortsetzen
				omDragDropObject.BeginDrag();
			}*/

		}
	}
	return 0L;
}


//****************************************************************************************************
//****************************************************************************************************
//	Ab hier: globale Funktionen
//****************************************************************************************************
//****************************************************************************************************

//****************************************************************************************************
// CompareBsd(): vergleicht die Codes (Feld Bscd) zweier Basisschichten-Datens�tze 
//  in Stringform auf lexikalisches gr�sser/kleiner:
//  R�ckgabe: int ->	<0: e1 ist kleiner als e2
//						=0: e1 == e2
//						>0:	e1 ist gr��er als e2
//****************************************************************************************************
/*
static int CompareBsd( const BSDDATA **e1, const BSDDATA **e2)
{
	// den Code vergleichen
	return (strcmp((**e1).Bsdc, (**e2).Bsdc));
}*/

//****************************************************************************************************
// CompareOda(): vergleicht die Codes (Feld Sdac) zweier Abwesenheiten-Datens�tze 
//  in Stringform auf lexikalisches gr�sser/kleiner:
//  R�ckgabe: int ->	<0: e1 ist kleiner als e2
//						=0: e1 == e2
//						>0:	e1 ist gr��er als e2
//****************************************************************************************************

/*
static int CompareOda( const ODADATA **e1, const ODADATA **e2)
{
	// den Code vergleichen
	return (strcmp((**e1).Sdac, (**e2).Sdac));
}
*/

//****************************************************************************************************
// Test !!!!
//****************************************************************************************************

int BSD_View::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// Hier eigendlich noch nicht notwendig, da kein DragZiel.
/*	BOOL blResult = (omDragDropObject.Register(this));
	ASSERT (blResult);*/

	return 0;
}
