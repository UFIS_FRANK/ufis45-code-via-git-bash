// BSD_Frame.cpp: Implementierungsdatei
//

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// BSD_Frame

IMPLEMENT_DYNCREATE(BSD_Frame, CMDIChildWnd)

BSD_Frame::BSD_Frame()
{
}

BSD_Frame::~BSD_Frame()
{
}


BEGIN_MESSAGE_MAP(BSD_Frame, CMDIChildWnd)
	//{{AFX_MSG_MAP(BSD_Frame)
		// HINWEIS - Der Klassen-Assistent f�gt hier Zuordnungsmakros ein und entfernt diese.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten BSD_Frame 

BOOL BSD_Frame::PreCreateWindow(CREATESTRUCT& cs) 
{
// Erstellen eines untergeordneten Fensters ohne die Schaltfl�che Maximieren 
    cs.style &= ~WS_MAXIMIZEBOX; 
// Ohne Rahmen der die Gr��e ver�ndert.
	cs.style &= ~WS_THICKFRAME;   
// und ohne Systemmenu.
	cs.style &= ~WS_SYSMENU;
	
	return CMDIChildWnd::PreCreateWindow(cs);
}
