//
// PrivList.cpp
//
//

#include <stdafx.h>

//
// PrivList()
//
// constructor
//
PrivList::PrivList()
{
    // Create an array of CEDARECINFO for PRVDATA
    BEGIN_CEDARECINFO(PRIVLIST, PrivList)
        FIELD_CHAR_TRIM(FLD1,"FLD1")
        FIELD_CHAR_TRIM(FLD2,"FLD2")
        FIELD_CHAR_TRIM(FLD3,"FLD3")
    END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(PrivList)/sizeof(PrivList[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&PrivList[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}


} // end PrivList()


//
// CedaPrvData()
//
// destructor
//
PrivList::~PrivList(void)
{

	omRecInfo.DeleteAll();
	ClearAll();

} // end destructor()



//
// ClearAll()
//
// Deletes all records held in omData
//
void PrivList::ClearAll(void)
{
    omData.DeleteAll();
    omFLD1Map.RemoveAll();
	omFLD2Map.RemoveAll();

} // end ClearAll()



//
// Add()
//
// Adds a PRVDATA record to omData
//
// prpPrv - record to be added
//
bool PrivList::Add(PRIVLIST *prpPrv)
{
	omData.Add(prpPrv);
	omFLD1Map.SetAt(prpPrv->FLD1,prpPrv);
	omFLD2Map.SetAt(prpPrv->FLD2,prpPrv);

	return true;

} // end Add()

bool PrivList::Add(const CString& prpPrivilegList)
{
	CStringArray olPrivileges;
	CString olKey,olValue;

	ExtractItemList(prpPrivilegList,&olPrivileges,';');

	for (int i = 0; i < olPrivileges.GetSize(); i++)
	{
		CStringArray olFields;
		ExtractItemList(olPrivileges[i],&olFields,',');
		PRIVLIST *prlPrv = new PRIVLIST;
		strcpy(prlPrv->FLD2,olFields[0]);
		strcpy(prlPrv->FLD3,olFields[1]);
		omData.Add(prlPrv);
		omFLD1Map.SetAt(prlPrv->FLD1,prlPrv);
		omFLD2Map.SetAt(prlPrv->FLD2,prlPrv);

	}
	
	return true;

} // end Add()

//
// Login()
//
// Checks if the user is permitted to log into the application.
// If successful (ie. the user is permitted) omData is filled with a list
// of privileges for the user.
//
// Parameters:
// pcpHomeAirport	- 3 letter code for home airport eg. "TXL" or "HAJ"
// pcpUsid			- Username
// pcpPass			- Password
// pcpAppl			- Application name eg. "FPMS"
//
// Return true (list loaded) or false (CedaError or LoginError)
//
// On CedaError omLastErrorMessage is filled with the error text.
//
// On LoginError omLastErrorMessage contains one of the following messages:
//
// "LOGINERROR INVALID_USER" - username not defined
// "LOGINERROR INVALID_APPLICATION" - application not defined
// "LOGINERROR INVALID_PASSWORD" - wrong password for the username
// "LOGINERROR DISABLED_USER" - username STAT is disabled
// "LOGINERROR DISABLED_APPLICATION" - application STAT is disabled
// "LOGINERROR EXPIRED_USER" - VAFR/VATO out of date for the user
// "LOGINERROR EXPIRED_APPLICATION" - VAFR/VATO out of date for the application
// "LOGINERROR UNDEFINED_PROFILE" - profile not defined or expired or disabled
//
bool PrivList::Login(const char *pcpHomeAirport, const char *pcpUsid, const char *pcpPass, const char *pcpAppl)
{
	bool blRc = true;

	
	// free any currently allocated memory
	ClearAll();
	

	char pclData[524];
	char pclFields[524];
	char pclHomeAirport[50];


	sprintf(pclData,"%s,%s,%s",(LPCTSTR) pcpUsid, (LPCTSTR) pcpPass, (LPCTSTR) pcpAppl);
	strcpy(pclFields,"USID,PASS,APPL");
	strcpy(pclHomeAirport,pcpHomeAirport);
	strcat(pclHomeAirport," ");

	// Get Privileges 
	char pclGpr[]="GPR";
	char pclEmpty[]="";
	blRc = CedaAction(pclGpr,pclHomeAirport,pclFields,pclEmpty,pclEmpty,pclData);

	if( ! blRc )
		omErrorMessage = omLastErrorMessage;



	// Load data from CedaData into the dynamic array of records

	if( blRc )
	{
		for (int ilLc = 0; blRc; ilLc++)
		{

			PRIVLIST *prpPrv = new PRIVLIST;
			if ((blRc = GetBufferRecord(ilLc,prpPrv)) == true)
				if( strcmp(prpPrv->FLD1,"[PRV]") )
					Add(prpPrv);
				else
					delete prpPrv;
			else
				delete prpPrv;
		}

		blRc = true;
	}

	return blRc;


} // end Login() - check user permission

// Given pcpFunc, search the priv list and return the Stat:
// ('-' = hidden, '0' = disabled, '1' = enabled, ' ' = not found)
char PrivList::GetStat( const char *pcpFunc)
{
	char clStat = ' ';
	PRIVLIST *prlPrv;
	if (omFLD2Map.Lookup((LPCSTR)pcpFunc,(void *&)prlPrv) == TRUE)
	{
		clStat = prlPrv->FLD3[0];
	}
	return clStat;
}


// if we want to forbid e.g. printing, because there is no ufisapplmgr installed
bool PrivList::SetStat( const char *pcpFunc, const char *pcpStat)
{
	char clStat = ' ';
	PRIVLIST *prlPrv;
	if (omFLD2Map.Lookup((LPCSTR)pcpFunc,(void *&)prlPrv) == TRUE)
	{
		strncpy(prlPrv->FLD3, pcpStat, 1);
		return true;
	}
	return false;
}

int PrivList::GetSize()
{
	int ilSize = omData.GetSize();
	return ilSize;
}