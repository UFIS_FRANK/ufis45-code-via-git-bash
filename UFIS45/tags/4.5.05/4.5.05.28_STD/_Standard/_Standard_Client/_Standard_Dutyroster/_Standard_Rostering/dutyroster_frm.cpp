// DutyRoster_Frm.cpp : implementation file
//
//////////////////////////////////////////////////////////////////////////////////
//
//		HISTORY
//
//		rdr		12.01.2k	OnToolTipText added, overrides the CMDIChildWnd-fct
//
//////////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define COMBO_HEIGHT 100    // width of edit control for combobox
#define COMBO_WIDTH  200    // drop down height

/////////////////////////////////////////////////////////////////////////////
// DutyRoster_Frm

IMPLEMENT_DYNCREATE(DutyRoster_Frm, CMDIChildWnd)

DutyRoster_Frm::DutyRoster_Frm()
{
CCS_TRY;
	// Anzeige der Legende in der Dienstplanung
	int ilLegend	= ogCfgData.GetMonitorForWindow(MON_DUTYROSTER_SHOWLEGEND);
	pmPlanInfoDlg = new CPlan_Info();

	// Anzeige der Kennzahlen
	pomCharacteristicDlg = new CCharacteristicValueDlg();
	pomCharacteristicDlg->Create();

	// Dialog vorerst nicht anzeigen
	pomCharacteristicDlg->ShowWindow(SW_HIDE);

	// zeitweise ausgeschaltet.
	ilLegend = 0;

	if (ilLegend == 1)
	{
		// Dialog mit Planungstufen anzeigen
		pmPlanInfoDlg->ShowWindow(SW_SHOW);
	}
	else
	{
		// Dialog nicht anzeigen
		pmPlanInfoDlg->ShowWindow(SW_HIDE);
	}
CCS_CATCH_ALL;
}

//****************************************************************************
// 
//****************************************************************************

DutyRoster_Frm::~DutyRoster_Frm()
{
	if (pmPlanInfoDlg != NULL)
		delete pmPlanInfoDlg;
	if (pomCharacteristicDlg != NULL)
	{
		delete pomCharacteristicDlg;
		pomCharacteristicDlg = NULL;
	}
}

BEGIN_MESSAGE_MAP(DutyRoster_Frm, CMDIChildWnd)
	//{{AFX_MSG_MAP(DutyRoster_Frm)
		ON_WM_SIZING()
		ON_WM_GETMINMAXINFO()
		ON_WM_CREATE()
		ON_UPDATE_COMMAND_UI(IDC_B_VIEW, OnUpdateBView)
		ON_UPDATE_COMMAND_UI(IDC_B_DUTYRELEASE2, OnUpdateBRelease)
		ON_UPDATE_COMMAND_UI(ID_DUTY_PRINT, OnUpdateBPrint)
		ON_UPDATE_COMMAND_UI(IDC_B_ATTENTION, OnUpdateBAttention)
		ON_UPDATE_COMMAND_UI(IDC_B_ACTUAL, OnUpdateBActual)
		ON_UPDATE_COMMAND_UI(IDW_COMBO, OnUpdateCombo)
		ON_UPDATE_COMMAND_UI(ID_SELECT_MA, OnUpdateSelectMa)
		ON_UPDATE_COMMAND_UI(ID_SHOW_LEGEND, OnUpdateShowLegend)
		ON_UPDATE_COMMAND_UI(ID_SHOW_CHARACTERISTIC_VALUES, OnUpdateShowCharacteristicValues)
		ON_UPDATE_COMMAND_UI(ID_EXCEL, OnUpdateExcel)
		ON_BN_CLICKED(ID_SHOW_LEGEND,OnBShowLegend)
		ON_BN_CLICKED(ID_SHOW_CHARACTERISTIC_VALUES,OnBShowCharacteristicValues)
		ON_WM_SIZE()
		ON_WM_MDIACTIVATE()
		ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTW, 0, 0xFFFF, OnToolTipText)
		ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTA, 0, 0xFFFF, OnToolTipText)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

		


/////////////////////////////////////////////////////////////////////////////
// DutyRoster_Frm message handlers

//****************************************************************************
// OnSizing
//****************************************************************************

void DutyRoster_Frm::OnSizing(UINT nSide, LPRECT lpRect ) 
{
	//Nur wenn es einen aktiven View gibt ausf�hren.
	if (GetActiveView() != NULL)
	{
		// Zum View weitergeben
		((DutyRoster_View*) GetActiveView())->SizeWindow(nSide,lpRect) ;
	}
}

//****************************************************************************
// OnGetMinMaxInfo
//****************************************************************************

void DutyRoster_Frm::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
CCS_TRY;
    CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
	
	//Nur wenn es einen aktiven View gibt ausf�hren.
	if (GetActiveView() != NULL)
	{
		lpMMI->ptMinTrackSize = ((DutyRoster_View*) GetActiveView())->GetMinTrackSize();
	}
CCS_CATCH_ALL;
}

//****************************************************************************
// PreCreateWindow(CREATESTRUCT& cs) 
//****************************************************************************

BOOL DutyRoster_Frm::PreCreateWindow(CREATESTRUCT& cs) 
{
	return CMDIChildWnd::PreCreateWindow(cs);
}

//****************************************************************************
// OnCreate
//****************************************************************************

int DutyRoster_Frm::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// Toolbar
	if (!m_wndToolBar.CreateEx(this, /*TBSTYLE_FLAT*/ TBSTYLE_BUTTON, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_DUTYFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	// ComboBox der Toolbar erstellen
	if (!CreateComboBox())
	{
		TRACE0("Failed to create combobox in toolbar\n");
		return -1;      // fail to create
	}
	return 0;
}

//****************************************************************************
// CreateComboBox
//****************************************************************************

int DutyRoster_Frm::CreateComboBox()
{
	// Position der Combobox in der Toolbar
	int ilComboBoxPosition = m_wndToolBar.CommandToIndex(IDW_COMBO);
	ASSERT (ilComboBoxPosition > 0);
	
	CRect rect, comboRect;
	m_wndToolBar.SetButtonInfo(ilComboBoxPosition,IDW_COMBO, TBBS_SEPARATOR,COMBO_WIDTH);
	m_wndToolBar.GetItemRect(ilComboBoxPosition, &rect);
	rect.bottom += COMBO_HEIGHT;

	if (!m_wndToolBar.m_toolBarCombo.Create(CBS_SORT|CBS_DROPDOWNLIST|WS_VISIBLE|WS_TABSTOP|CBS_AUTOHSCROLL|WS_VSCROLL,
			rect, &m_wndToolBar, IDW_COMBO))
	{
		return FALSE;
	}

	// center combo box edit control vertically within tool bar
	rect.bottom -= COMBO_HEIGHT;
	m_wndToolBar.m_toolBarCombo.GetWindowRect(&comboRect);
	m_wndToolBar.m_toolBarCombo.ScreenToClient(&comboRect);

	m_wndToolBar.m_toolBarCombo.SetWindowPos(&m_wndToolBar.m_toolBarCombo, rect.left,
		rect.top + (rect.Height() - comboRect.Height())/2+1, 0, 0,
		SWP_NOZORDER|SWP_NOSIZE|SWP_NOACTIVATE);

	return TRUE;
}

// Enabled oder disabled die Buttons in der Toolbar je nach Sicherheitsvorgaben
// Leider f�r jeden Button hier n�tig

void DutyRoster_Frm::OnUpdateBView(CCmdUI* pCmdUI) 
{
	CString csCode;
	csCode = ogPrivList.GetStat("DUTYROSTER_IDC_B_VIEW");
	
	if(csCode=='1') 
		pCmdUI->Enable(true);
	
	if(csCode=='0' || csCode=='-')
		pCmdUI->Enable(false);
}

void DutyRoster_Frm::OnUpdateBRelease(CCmdUI* pCmdUI) 
{
	CString csCode;
	csCode = ogPrivList.GetStat("DUTYROSTER_IDC_B_RELEASE");
	// zuerst die  Benutzerrechte pr�fen
	if(csCode=='1')
	{
		//Nur wenn es einen aktiven View gibt ausf�hren.
		if (GetActiveView() != NULL)
		{
			pCmdUI->Enable(((DutyRoster_View*) GetActiveView())->GetEnableRelease());
		}
	}
	else
	{
		pCmdUI->Enable(false);
	}
}

void DutyRoster_Frm::OnUpdateBPrint(CCmdUI* pCmdUI) 
{
	CString olCustomer = ogCCSParam.GetParamValue(ogAppl,"ID_PROD_CUSTOMER");
	CString olNoPrint = CString ("ADR,ZRH,BSL,GVA");
	if (olNoPrint.Find(olCustomer) > -1)
	{
		pCmdUI->Enable(false);
		return;
	}

	CString csCode;
	csCode = ogPrivList.GetStat("DUTYROSTER_IDC_B_PRINT");

	// zuerst die  Benutzerrechte pr�fen
	if (csCode == '1')
	{
		//Nur wenn es einen aktiven View gibt ausf�hren.
		if (GetActiveView() != NULL)
		{
			pCmdUI->Enable(((DutyRoster_View*) GetActiveView())->GetEnableRelease());
		}
		else
		{
			pCmdUI->Enable(false);
		}
	}
	else
	{
		pCmdUI->Enable(false);
	}
}

void DutyRoster_Frm::OnUpdateBAttention(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(true);
}

void DutyRoster_Frm::OnUpdateShowLegend(CCmdUI* pCmdUI) 
{
	if (pmPlanInfoDlg->IsWindowVisible() == 0)
		pCmdUI->Enable(true);
	else
		pCmdUI->Enable(false);
}

void DutyRoster_Frm::OnUpdateShowCharacteristicValues(CCmdUI* pCmdUI) 
{
CCS_TRY;
	
	CString csCode;
	csCode = ogPrivList.GetStat("DUTYROSTER_IDC_B_ACCOUNT");

	//Nur wenn es einen aktiven View gibt ausf�hren.
	if (GetActiveView() != NULL)
	{
		if(csCode=='1' && ((DutyRoster_View*) GetActiveView())->GetEnableCharacteristicValues())
		{
			pCmdUI->Enable(true);
		}
		else
		{
			pCmdUI->Enable(false);
		}
	}
	else
	{
		// Dialog mit Planungstufen ausblenden
		if (pomCharacteristicDlg->IsWindowVisible())
		{
			pomCharacteristicDlg->ShowWindow(SW_HIDE);
		}
		pCmdUI->Enable(false);
	}
CCS_CATCH_ALL;
}

void DutyRoster_Frm::OnUpdateExcel(CCmdUI* pCmdUI) 
{
CCS_TRY;
	
	CString csCode;
	csCode = ogPrivList.GetStat("DUTYROSTER_IDC_B_EXCEL");

	//Nur wenn es einen aktiven View gibt ausf�hren.
	if (GetActiveView() != NULL)
	{
		if(/*csCode=='1' &&*/ ((DutyRoster_View*) GetActiveView())->GetEnableRelease())
		{
			pCmdUI->Enable(true);
		}
		else
		{
			pCmdUI->Enable(false);
		}
	}
	else
	{
		pCmdUI->Enable(false);
	}
CCS_CATCH_ALL;
}

void DutyRoster_Frm::OnUpdateBActual(CCmdUI* pCmdUI) 
{
	//Nur wenn es einen aktiven View gibt ausf�hren.
	if (GetActiveView() != NULL){
		pCmdUI->Enable(((DutyRoster_View*) GetActiveView())->GetActualButtonNeed());
	}
	else{
		pCmdUI->Enable(false);
	}
}

void DutyRoster_Frm::OnUpdateCombo(CCmdUI* pCmdUI) 
{
	CString csCode;
	csCode = ogPrivList.GetStat("DUTYROSTER_IDC_CB_VIEW");

	if(csCode=='1') 
		pCmdUI->Enable(true);
	
	if(csCode=='0' || csCode=='-')
	{
		pCmdUI->Enable(false);
	}
}

void DutyRoster_Frm::OnUpdateSelectMa(CCmdUI* pCmdUI) 
{
	//Nur wenn es einen aktiven View gibt ausf�hren.
	if (GetActiveView() != NULL){
		pCmdUI->Enable(((DutyRoster_View*) GetActiveView())->GetEnableSelectEmployee());
	}
	else{
		pCmdUI->Enable(false);
	}
}

//****************************************************************************
// Es werden zwei verschiedene Toolbars geladen, um den Attention Button
// zu ver�ndern.
//****************************************************************************

void DutyRoster_Frm::LoadToolBar(BOOL bIsAttention) 
{
	if (!bIsAttention)
		// ohne attention
		m_wndToolBar.LoadToolBar(IDR_DUTYFRAME);
	else
		// mit attention
		m_wndToolBar.LoadToolBar(IDR_DUTYFRAMEAT);

	// Position der Combobox in der Toolbar
	int ilComboBoxPosition = m_wndToolBar.CommandToIndex(IDW_COMBO);
	ASSERT (ilComboBoxPosition > 0);
	
	CRect rect, comboRect;
	m_wndToolBar.SetButtonInfo(ilComboBoxPosition,IDW_COMBO, TBBS_SEPARATOR,COMBO_WIDTH);
	m_wndToolBar.GetItemRect(ilComboBoxPosition, &rect);
	rect.bottom += COMBO_HEIGHT;

	// center combo box edit control vertically within tool bar
	rect.bottom -= COMBO_HEIGHT;
	m_wndToolBar.m_toolBarCombo.GetWindowRect(&comboRect);
	m_wndToolBar.m_toolBarCombo.ScreenToClient(&comboRect);

	m_wndToolBar.m_toolBarCombo.SetWindowPos(&m_wndToolBar.m_toolBarCombo, rect.left,
	rect.top + (rect.Height() - comboRect.Height())/2+1, 0, 0,
	SWP_NOZORDER|SWP_NOSIZE|SWP_NOACTIVATE);
}

//****************************************************************************
// rdr 12.01.2000 added
//****************************************************************************

bool DutyRoster_Frm::OnToolTipText(UINT, NMHDR *pNMHDR, LRESULT *pResult)
{
	ASSERT(pNMHDR->code == TTN_NEEDTEXTA || pNMHDR->code == TTN_NEEDTEXTW);
	
	TOOLTIPTEXTA* pTTTA = (TOOLTIPTEXTA*)pNMHDR;
	TOOLTIPTEXTW* pTTTW = (TOOLTIPTEXTW*)pNMHDR;
	
	CString strTipText;
	UINT nID = pNMHDR->idFrom;
	if (pNMHDR->code == TTN_NEEDTEXTA && (pTTTA->uFlags & TTF_IDISHWND) 
		|| pNMHDR->code == TTN_NEEDTEXTW && (pTTTW->uFlags & TTF_IDISHWND))
	{
		nID = ::GetDlgCtrlID((HWND)nID);
	}

	if (nID !=0)
	{
		CString olMsg;
		olMsg.LoadString(nID);		// if this a valid Res-String?
		CString olTxt = LoadStg(nID);

		if (olTxt.IsEmpty() && olMsg.IsEmpty())
		{
			return false;
		}
		AfxExtractSubString(strTipText, olTxt, 1, '\n');
	}

#ifndef _UNICODE
	if (pNMHDR->code == TTN_NEEDTEXTA)
		lstrcpyn(pTTTA->szText, strTipText, sizeof(pTTTA->szText));
	else
		_mbstowcsz(pTTTW->szText, strTipText, sizeof(pTTTW->szText));
#else
	if (pNMHDR->code == TTN_NEEDTEXTA)
		_wcstombsz(pTTTA->szText, strTipText, sizeof(pTTTA->szText));
	else
		lstrcpyn(pTTTW->szText, strTipText, sizeof(pTTTW->szText));
#endif

	*pResult = 0;

	::SetWindowPos(pNMHDR->hwndFrom, HWND_TOP, 0, 0, 0, 0,
		SWP_NOACTIVATE|SWP_NOSIZE|SWP_NOMOVE|SWP_NOOWNERZORDER);

	return true;
}

//****************************************************************************
// Der Frame wurde aktiviert
//****************************************************************************

void DutyRoster_Frm::OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd) 
{
	CMDIChildWnd::OnMDIActivate(bActivate, pActivateWnd, pDeactivateWnd);
	
	// Nachricht an den Mainframe weitergeben
	if (bActivate)
	{
		CString olOrgCodes = ((DutyRoster_View*) GetActiveView())->rmViewInfo.oOrgStatDpt1s;
		CString olPfcCodes = ((DutyRoster_View*) GetActiveView())->rmViewInfo.oPfcStatFctcs;

		CMainFrame* polMainFrame = (CMainFrame*) GetParentFrame();
		polMainFrame->ActiveFrameChanged(DUTY_VIEW,olOrgCodes,olPfcCodes);
	}
}

//****************************************************************************
// Legenden Dialog starten
//****************************************************************************

void DutyRoster_Frm::OnBShowLegend()
{
	// Dialog mit Planungstufen anzeigen
	if (pmPlanInfoDlg->IsWindowVisible() == 0)
	{
		pmPlanInfoDlg->ShowWindow(SW_SHOW);
	}
}

//****************************************************************************
// Legenden Dialog starten
//****************************************************************************

void DutyRoster_Frm::OnBShowCharacteristicValues()
{
CCS_TRY;	
	// Dialog mit Planungstufen anzeigen
	if (!pomCharacteristicDlg->IsWindowVisible() 
		|| ((pomCharacteristicDlg != NULL) && (pomCharacteristicDlg->IsIconic())))
	{
		pomCharacteristicDlg->ShowWindow(SW_RESTORE);
	}

	



CCS_CATCH_ALL;
}
