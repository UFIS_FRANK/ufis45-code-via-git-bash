// DutyStatTabViewer.cpp 
//
#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

// Local function prototype
static void DutyStatTabViewerCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//***************************************************************************************************************************************
// DutyStatTabViewerCf: Callback-Fkt. f�r den Broadcast-Handler.
// R�ckgabe:	keine
//***************************************************************************************************************************************

static void DutyStatTabViewerCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
CCS_TRY;

	DutyStatTabViewer *polViewer = (DutyStatTabViewer *)popInstance;

	switch (ipDDXType)
	{
		case ACC_NEW:
		case ACC_CHANGE:
		case ACC_DELETE:
			polViewer->ProcessAccChange((ACCDATA *)vpDataPointer,ipDDXType);
			break;
		case RELACC:
			polViewer->ProcessRelaccBc();
			break;
		default:
			break;
	}

CCS_CATCH_ALL;
} 

//***********************************************************************************
// DutyStatTabViewer
//***********************************************************************************

DutyStatTabViewer::DutyStatTabViewer(CWnd* pParent, CAccounts *popAccounts)
{
	pomParent = (DutyRoster_View*)pParent;

	// Zeiger auf AZK-Objekt kopieren
	pomAccounts = popAccounts;

	// Beim Broadcast-Handler anmelden f�r �nderung von ACCs
	DdxRegister(this, ACC_NEW,          "DutyStatTabViewer", "ACC_NEW",     DutyStatTabViewerCf);
    DdxRegister(this, ACC_CHANGE,       "DutyStatTabViewer", "ACC_CHANGE",  DutyStatTabViewerCf);
	DdxRegister(this, ACC_DELETE,       "DutyStatTabViewer", "ACC_DELETE",  DutyStatTabViewerCf);
	DdxRegister(this, RELACC,			"DutyStatTabViewer", "RELACC",		DutyStatTabViewerCf);

    pomDynTab	= NULL;
	prmViewInfo = NULL;
	pomStfData  = NULL;
	pomGroups   = NULL;

	imAccounts = 0;
}


//***********************************************************************************
// destructor
//***********************************************************************************
DutyStatTabViewer::~DutyStatTabViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
}

//***********************************************************************************
//
//***********************************************************************************
void DutyStatTabViewer::Attach(CCSDynTable *popTable)
{
    pomDynTab = popTable;
    // Beim Broadcast-Handler abmelden
	prmTableData = pomDynTab->GetTableData();
}

//***********************************************************************************
// the view changed
//***********************************************************************************
void DutyStatTabViewer::ChangeView(bool bpSetHNull /*=true*/,bool bpSetVNull /*=true*/)
{
	CCS_TRY;
	pomStfData  = &pomParent->omStfData;
	prmViewInfo = &pomParent->rmViewInfo;
	pomGroups   = &pomParent->omGroups;

	omPTypes.RemoveAll();
	ExtractItemList(prmViewInfo->oPType, &omPTypes, ',');
	int ilNrOfTypes = omPTypes.GetSize();

	int ilStfSize = 0;
	if(prmViewInfo->iGroup  == 2)
		ilStfSize = pomGroups->GetSize(); //Gruppen-Darstellung
	else
		ilStfSize = pomStfData->GetSize();//Mitarbeiter-Darstellung

	ilStfSize = (ilStfSize*ilNrOfTypes)-1;
	if(ilStfSize < 0) ilStfSize = 0;

	int ilAccounts = 0;
	if(prmViewInfo->oAccounts.GetSize()>0)
	{
		imAccounts = prmViewInfo->oAccounts.GetSize();
		ilAccounts = imAccounts-1;
	}
	else
	{
		imAccounts = 0;
		ilAccounts = 0;
	}

	// Alle Konten die Aufgrund der Tempor�ren Konten relevant sind ermitteln
	omExtraAccounts.RemoveAll();
	CString olType, olAccount;
	CString olAdeUrno,olAdeToUse,olAdsToUse,olCount;
	int ilCount;
	for (int i = 0;i < prmViewInfo->oAccounts.GetSize();i++)
	{
		// Ist es ein tempor�res Konto ?
		olAccount.Format("%i",prmViewInfo->oAccounts.GetAt(i));
		olType = ogBCD.GetField("ADE","TYPE",olAccount,"KTYP");

		if (olType == "T")
		{
			ilCount = 0;
			// Urno des ADE Datensatzes erhalten
			olAdeUrno = ogBCD.GetField("ADE","TYPE",olAccount,"URNO");
			do
			{
				// Ermittlung des zu Bearbeitenden Accounts 
				ilCount++;
				olCount.Format("%d",ilCount);
				// Suche ADS mit der Urno des ADE und der fortlaufenden Nummer
				olAdsToUse = ogBCD.GetFieldExt("ADS", "ADEU", "LNUM",olAdeUrno, olCount, "URNO");

				if (olAdsToUse.IsEmpty())
					break;
				// Interne Nummes des zugeh�rigen ADE Datensatzes ermitteln
				olAdeToUse = ogBCD.GetField("ADS", "URNO", olAdsToUse, "TYPE");
				// Eintrag in die Liste schreiben	
				omExtraAccounts.AddTail(olAdeToUse);

			} while(!olAdsToUse.IsEmpty());
		}
	}


	int ilColumnStartPos = 0;
	int ilRowStartPos = 0;
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);

	if(!bpSetHNull)
	{
		ilColumnStartPos = rlTableInfo.iColumnOffset;
		//Berichtigung des Offset
		int ilColumnDiff = (ilAccounts+1) - ilColumnStartPos;
		if(ilColumnDiff < rlTableInfo.iColumns)
		{
			ilColumnStartPos = ilColumnStartPos - (rlTableInfo.iColumns - ilColumnDiff);
		}
		if(ilColumnStartPos < 0)
			ilColumnStartPos = 0;
	}
	if(!bpSetVNull)
	{
		ilRowStartPos = rlTableInfo.iRowOffset;
		//Berichtigung des Offset
		int ilRowDiff = (ilStfSize + 1) - ilRowStartPos;
		if(ilRowDiff < rlTableInfo.iRows)
		{
			ilRowStartPos = ilRowStartPos - (rlTableInfo.iRows - ilRowDiff);
		}
		if(ilRowStartPos < 0)
			ilRowStartPos = 0;
	}

	pomDynTab->SetHScrollData(0,ilAccounts, ilColumnStartPos);
	pomDynTab->SetVScrollData(0,ilStfSize, ilRowStartPos);
	
	InitTableArrays(ilColumnStartPos);
	MakeTableData(ilColumnStartPos,ilRowStartPos);
	pomDynTab->InvalidateRect(NULL);
	CCS_CATCH_ALL;
}

//***********************************************************************************
//
//***********************************************************************************
void DutyStatTabViewer::HorzTableScroll(MODIFYTAB *prpModiTab)
{
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
	if(prpModiTab->iID == rlTableInfo.iID)
	{
		MakeTableData(prpModiTab->iNewHorzPos,prpModiTab->iNewVertPos);
		pomDynTab->ChangedTableData(prpModiTab->iNewHorzPos,prpModiTab->iNewVertPos);
	}
	else
	{
		MakeTableData(prpModiTab->iNewHorzPos,rlTableInfo.iRowOffset);
		pomDynTab->ChangedTableData(prpModiTab->iNewHorzPos,rlTableInfo.iRowOffset);
	}
}

//***********************************************************************************
//
//***********************************************************************************
void DutyStatTabViewer::VertTableScroll(MODIFYTAB *prpModiTab)
{
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
	if(prpModiTab->iID == rlTableInfo.iID)
	{
		MakeTableData(prpModiTab->iNewHorzPos,prpModiTab->iNewVertPos);
		pomDynTab->ChangedTableData(prpModiTab->iNewHorzPos,prpModiTab->iNewVertPos);
	}
	else
	{
		MakeTableData(rlTableInfo.iColumnOffset,prpModiTab->iNewVertPos);
		pomDynTab->ChangedTableData(rlTableInfo.iColumnOffset,prpModiTab->iNewVertPos);
	}
}

//***********************************************************************************
//
//***********************************************************************************
void DutyStatTabViewer::TableSizing(LPRECT lpRect)
{
	pomDynTab->SizeTable(lpRect);
}

//***********************************************************************************
//
//***********************************************************************************
void DutyStatTabViewer::MoveTable(MODIFYTAB *prpModiTab)
{
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
	if(prpModiTab->iID == rlTableInfo.iID)
	{
		if(prmTableData->oSolid.GetSize() > 0 && prmTableData->oHeader.GetSize() > 0)
		{
			MakeTableData(prpModiTab->iNewHorzPos,prpModiTab->iNewVertPos);
		}
	}
	else
	{
		int ilX = 0;
		int ilY = 0;
		ilX = prpModiTab->oNewRect.right  - prpModiTab->oOldRect.right;
		pomDynTab->MoveTable(ilX,ilY);
	}
}

//***********************************************************************************
//
//***********************************************************************************
void DutyStatTabViewer::InlineUpdate(INLINEDATA *prpInlineUpdate)
{

}

//***********************************************************************************
//
//***********************************************************************************
void DutyStatTabViewer::InitTableArrays(int ipColumnOffset /*=0*/)
{
	CString olText; 
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);
	pomDynTab->ResetContent();
	
	prmTableData->oSolidHeader.oText		= "";
	prmTableData->oSolidHeader.oTextColor	= BPN_NOCOLOR;
	prmTableData->oSolidHeader.oBkColorNr	= BPN_NOCOLOR;

	for (int ilColumn = 0; ilColumn < rlTableInfo.iColumns; ilColumn++)
	{
		FIELDDATA *polFieldData = new FIELDDATA;

		olText.Empty();
		polFieldData->oTextColor	= BPN_NOCOLOR;
		polFieldData->oBkColorNr	= BPN_NOCOLOR;

		if (ipColumnOffset + ilColumn < imAccounts)
		{
			olText = pomAccounts->GetRunningAccountHeader(prmViewInfo->oAccounts[ipColumnOffset + ilColumn]);
		}
		polFieldData->oText	= olText;

		prmTableData->oHeader.Add((void*)polFieldData);
	}
}

//***********************************************************************************
//
//***********************************************************************************
void DutyStatTabViewer::MakeTableData(int ipColumnOffset,int ipRowOffset)
{
	TABLEINFO rlTableInfo;
	pomDynTab->GetTableInfo(rlTableInfo);

	int ilActualRows = prmTableData->oSolid.GetSize();

	// Add rows by SIZING
	while (rlTableInfo.iRows > ilActualRows)
	{
		AddRow(ilActualRows,rlTableInfo.iColumns,ipColumnOffset,ipRowOffset+ilActualRows);
		ilActualRows = prmTableData->oSolid.GetSize();
	}

	// Delete rows by SIZING
	while(rlTableInfo.iRows < ilActualRows)
	{
		DelRow(ilActualRows-1);
		ilActualRows = prmTableData->oSolid.GetSize();
	}

	int ilActualColumns = prmTableData->oHeader.GetSize();

	// Add columns by SIZING
	while (ilActualRows > 0 && rlTableInfo.iColumns > ilActualColumns)
	{
		AddColumn(ilActualColumns,ipColumnOffset+ilActualColumns,ipRowOffset);
		ilActualColumns = prmTableData->oHeader.GetSize();
	}

	// Delete columns by SIZING
	while (ilActualRows > 0 && rlTableInfo.iColumns < ilActualColumns)
	{
		DelColumn(ilActualColumns-1);
		ilActualColumns = prmTableData->oHeader.GetSize();
	}


	int ilColumnDiv = ipColumnOffset-rlTableInfo.iColumnOffset;
	if (ilColumnDiv != 0)
	{
		// SCROLL right
		if(ilColumnDiv > 0)
		{
			if(ilColumnDiv > prmTableData->oHeader.GetSize())
			{
				ilColumnDiv = prmTableData->oHeader.GetSize();
			}
			for (int i = 0; i < ilColumnDiv;i++)
			{
				DelColumn(0);
				AddColumn(prmTableData->oHeader.GetSize(),ipColumnOffset+rlTableInfo.iColumns-ilColumnDiv+i,ipRowOffset);
			}
		}

		// SCROLL left
		if( ilColumnDiv < 0)
		{
			ilColumnDiv = -(ilColumnDiv);
			if (ilColumnDiv > prmTableData->oHeader.GetSize())
			{
				ilColumnDiv = prmTableData->oHeader.GetSize();
			}
			for (int i = 0; i < ilColumnDiv; i++)

			{
				DelColumn(prmTableData->oHeader.GetSize()-1);
				AddColumn(0,ipColumnOffset+ilColumnDiv-i-1,ipRowOffset);
			}
		}
	}
	int ilRowDiv = ipRowOffset-rlTableInfo.iRowOffset;
	if (ilRowDiv != 0)
	{
		// SCROLL up
		if (ilRowDiv > 0)
		{
			if (ilRowDiv > prmTableData->oSolid.GetSize())
			{
				ilRowDiv = prmTableData->oSolid.GetSize();
			}

			for (int i = 0; i < ilRowDiv; i++)
			{
				DelRow(0);
				AddRow(prmTableData->oSolid.GetSize(),rlTableInfo.iColumns,ipColumnOffset,ipRowOffset+rlTableInfo.iRows-ilRowDiv+i);
			}
		}

		// SCROLL down
		if (ilRowDiv < 0)
		{
			ilRowDiv = -(ilRowDiv);
			if (ilRowDiv > prmTableData->oSolid.GetSize())
			{
				ilRowDiv = prmTableData->oSolid.GetSize();
			}
			for (int i = 0; i < ilRowDiv; i++)
			{
				DelRow(prmTableData->oSolid.GetSize()-1);
				AddRow(0,rlTableInfo.iColumns,ipColumnOffset,ipRowOffset+ilRowDiv-i-1);
			}
		}
	}
}

//***********************************************************************************
//
//***********************************************************************************

void DutyStatTabViewer::AddRow(int ipRow, int ipColumns, int ipColumnNr, int ipRowNr)
{
	CString olText;
	// (normal) grid-area of table
	CPtrArray *polTableValue = new CPtrArray;

	CString olPType = "";
	int ilStfNr = 0;
	int ilPTypeNr = 0;
	bool blGetRaT = GetElementAndPType(ipRowNr, ilStfNr, olPType, ilPTypeNr);

	UINT ilLineWidth = NO_LINE;
	if(((ilStfNr < pomStfData->GetSize() && prmViewInfo->iGroup == 1)|| prmViewInfo->iGroup == 3) && blGetRaT)
	{
		long llTmpUrno = 0;
		if(prmViewInfo->iGroup == 2)
		{
			llTmpUrno = (*pomGroups)[ilStfNr].lStfUrno;
			if(ilStfNr+1 < pomGroups->GetSize())
			{
				if((*pomGroups)[ilStfNr].iColor1Nr != (*pomGroups)[ilStfNr+1].iColor1Nr)
				{
					ilLineWidth = NORMAL_LINE;
				}
			}
		}
		else
		{
			llTmpUrno = (*pomStfData)[ilStfNr].Urno;
		}
	}

	for(int ilColumn=0;ilColumn<ipColumns;ilColumn++)
	{
		olText.Empty();
		FIELDDATA *polFieldData = new FIELDDATA;
		polFieldData->oTextColor = BPN_NOCOLOR;
		polFieldData->oBkColorNr = BPN_NOCOLOR;
		polFieldData->rIPEAttrib.TextMaxLenght = 8;
		polFieldData->rIPEAttrib.TextMinLenght = 1;
		polFieldData->bInplaceEdit = false;

		polFieldData->iBLineWidth = ilLineWidth;

		if(blGetRaT && omPTypes.GetSize() > 1 && ilPTypeNr+1 == omPTypes.GetSize())
		{
			polFieldData->iBLineWidth = NORMAL_LINE;
		}
		if(((ilStfNr < pomStfData->GetSize() && prmViewInfo->iGroup == 1)||(ilStfNr < pomGroups->GetSize() && prmViewInfo->iGroup == 2)||prmViewInfo->iGroup == 3) && blGetRaT && ilPTypeNr == 0)
		{
			GenerateFieldValues(ilStfNr, olPType, ilPTypeNr, ipColumnNr+ilColumn, ipRowNr, polFieldData);
		}
		else
		{
			polFieldData->oBkColorNr = BPN_LIGHTSILVER2;
		}

		polTableValue->Add((void*)polFieldData);
	}
	prmTableData->oTable.InsertAt(ipRow,(void*)polTableValue);


	// Solid-area of table
	olText.Empty();
	FIELDDATA *polFieldData = new FIELDDATA;
	polFieldData->oTextColor = BPN_NOCOLOR;
	polFieldData->oBkColorNr = BPN_NOCOLOR;

	prmTableData->oSolid.InsertAt(ipRow,(void*)polFieldData);
}

//***********************************************************************************
//
//***********************************************************************************


void DutyStatTabViewer::DelRow(int ipRow)
{
	//** Table Text Color

	CPtrArray *polTableValue = (CPtrArray*)prmTableData->oTable[ipRow];
	for(int i = polTableValue->GetSize()-1;i>=0;i--)
	{
		FIELDDATA *polFieldData = (FIELDDATA*)(*polTableValue)[i];
		delete polFieldData;
	}
	polTableValue->RemoveAll();
	delete polTableValue;
	prmTableData->oTable.RemoveAt(ipRow);

	//** Solid Text Color
	FIELDDATA *polFieldData = (FIELDDATA*)prmTableData->oSolid[ipRow];
	delete polFieldData;
	prmTableData->oSolid.RemoveAt(ipRow);

}

//***********************************************************************************
//
//***********************************************************************************


void DutyStatTabViewer::AddColumn(int ipColumn, int ipColumnNr, int ipRowNr)
{
	CString olText; 
	//** Table
	int ilActualRows = prmTableData->oTable.GetSize();

	for(int ilRow = 0; ilRow<ilActualRows; ilRow++)
	{
		olText.Empty();
		CPtrArray *polTableValue = (CPtrArray*)prmTableData->oTable[ilRow];

		FIELDDATA *polFieldData = new FIELDDATA;

		polFieldData->oTextColor = BPN_NOCOLOR;
		polFieldData->oBkColorNr = BPN_NOCOLOR;
		polFieldData->rIPEAttrib.TextMaxLenght = 8;
		polFieldData->rIPEAttrib.TextMinLenght = 1;
		polFieldData->bInplaceEdit = false;

		CString olPType = "";
		int ilStfNr = 0;
		int ilPTypeNr = 0;
		bool blGetRaT = GetElementAndPType((ipRowNr+ilRow), ilStfNr, olPType, ilPTypeNr);

		if(blGetRaT && omPTypes.GetSize() > 1 && ilPTypeNr+1 == omPTypes.GetSize())
		{
			polFieldData->iBLineWidth = NORMAL_LINE;
		}

		if(((ilStfNr < pomStfData->GetSize() && prmViewInfo->iGroup == 1)||(ilStfNr < pomGroups->GetSize() && prmViewInfo->iGroup == 2)||prmViewInfo->iGroup == 3) && blGetRaT && ilPTypeNr == 0)
		{
			GenerateFieldValues(ilStfNr, olPType, ilPTypeNr, ipColumnNr, ipRowNr+ilRow, polFieldData);
		}
		else
		{
			polFieldData->oBkColorNr = BPN_LIGHTSILVER2;
		}

		polTableValue->InsertAt(ipColumn,(void*)polFieldData);
	}

	//** Header
	olText.Empty();
	FIELDDATA *polFieldData = new FIELDDATA;
	polFieldData->oTextColor = BPN_NOCOLOR;
	polFieldData->oBkColorNr = BPN_NOCOLOR;
	polFieldData->oText	= pomAccounts->GetRunningAccountHeader(prmViewInfo->oAccounts[ipColumnNr]);
	prmTableData->oHeader.InsertAt(ipColumn,(void*)polFieldData);
}

//***********************************************************************************
//
//***********************************************************************************

void DutyStatTabViewer::DelColumn(int ipColumn)
{
	//** Table Text Color
	int ilActualRows = prmTableData->oTable.GetSize();
	for(int ilRow = 0; ilRow<ilActualRows; ilRow++)
	{
		CPtrArray *polTableValue = (CPtrArray*)prmTableData->oTable[ilRow];
		FIELDDATA *polFieldData = (FIELDDATA*)(*polTableValue)[ipColumn];
		delete polFieldData;
		polTableValue->RemoveAt(ipColumn);
	}

	//** Header Text Color
	FIELDDATA *polFieldData = (FIELDDATA*)prmTableData->oHeader[ipColumn];
	delete polFieldData;
	prmTableData->oHeader.RemoveAt(ipColumn);
}

//***********************************************************************************
// Ermittelt die Felddaten
//***********************************************************************************

void DutyStatTabViewer::GenerateFieldValues(int ipStfNr, CString opPType, int ipPTypeNr, int ipColumn, int ipRow, FIELDDATA *popFieldData)
{
CCS_TRY;
	CString olText;
	long llTmpUrno = 0;

	//---------------------------------------------------------------------------------
	// Ermittelung des Mitarbeiters
	if(prmViewInfo->iGroup == 2)
	{
		llTmpUrno = (*pomGroups)[ipStfNr].lStfUrno;
		if(ipStfNr+1 < pomGroups->GetSize())
		{
			if((*pomGroups)[ipStfNr].iColor1Nr != (*pomGroups)[ipStfNr+1].iColor1Nr)
			{
				popFieldData->iBLineWidth = NORMAL_LINE;
			}
		}
	}
	else
	{
		llTmpUrno = (*pomStfData)[ipStfNr].Urno;
	}

	//---------------------------------------------------------------------------------
	// Kontowerte f�r aktuelle View im globalem CAccount-Objekt vorhanden?
	if(ipColumn < prmViewInfo->oAccounts.GetSize())
	{
		// ja -> Kontowert f�r Typ <Spaltennummer> und Mitarbeiter mit Urno <llTmpUrno> ermitteln
		ACCOUNTRETURNSTRUCT rlAccountReturnStruct = pomAccounts->GetAccountByStaffAndMonth(prmViewInfo->oAccounts[ipColumn], llTmpUrno,prmViewInfo->oDateFrom, prmViewInfo->oDateTo);

		//ACCOUNTRETURNSTRUCT rlAccountReturnStruct = pomAccounts->GetRunningAccountByStaff(prmViewInfo->oAccounts[ipColumn], llTmpUrno);
		// Format-Tag f�r String-Formatierung ermitteln

		CString olFormat;
		int ilFormat = atoi(rlAccountReturnStruct.oFormat);
		// Plausibilit�tspr�fung des Formatstrings
		if ( ilFormat < 0 || ilFormat > 10)
		{
			// -> Fehler
			olFormat = "2";
		}
		else
		{
			// -> OK
			olFormat.Format("%d",ilFormat);
		}
		if(rlAccountReturnStruct.oValueDefinition == "STRINGorSINGEL")
		{
			olText = rlAccountReturnStruct.oString.Left(6);
		}
		else
		{
			olText.Format("%0."+olFormat+"f", rlAccountReturnStruct.dDouble);
		}

		// Kontowert farbig wg. ??? (Problem bei Ermittlung? Soll/Ist-Verh�ltnis?)
		if(!rlAccountReturnStruct.bBool)
			popFieldData->oTextColor = RED;
	}
	// Text in Zelle speichern
	popFieldData->oText = olText;
CCS_CATCH_ALL;
}

//***********************************************************************************
//
//***********************************************************************************

bool DutyStatTabViewer::GetElementAndPType(int ipRowNr, int &ipRow, CString &opPType, int &ipPTypNr)
{
CCS_TRY;
	int ilPTypeSize = omPTypes.GetSize();

	if(ilPTypeSize < 1) return false;

	double dlBruch = (double)ipRowNr/(double)ilPTypeSize;
	double dlDivisor = 1.0/(double)ilPTypeSize - 0.0000000000001;
	double dlRow = floor(dlBruch);
	ipRow = (int)dlRow;

	ipPTypNr = (int)(floor(((dlBruch-dlRow)/dlDivisor)));

	if(ipPTypNr>ilPTypeSize) return false;
	opPType = omPTypes[ipPTypNr];

	return true;	
CCS_CATCH_ALL;
return false;
}

//***************************************************************************************************************************************
// ProcessAccChange: behandelt die BCs ACC_NEW, ACC_CHANGE 
//	Die Funktion veranlasst die Aktualisierung der Mitarbeiterkontos.
// R�ckgabe:	keine
//***************************************************************************************************************************************

void DutyStatTabViewer::ProcessAccChange(ACCDATA *popDataPointer, int ipType)
{
	// check parameters first
	if (prmViewInfo == NULL)
		return;
	if (popDataPointer == NULL)
		return;

	// Is the account relevant?
	bool blFound = false;
	for (int i = 0;i < prmViewInfo->oAccounts.GetSize() && !blFound;i++)
	{
		if (atoi(popDataPointer->Type) == prmViewInfo->oAccounts.GetAt(i))
			blFound = true;

		// Pr�fen ob das Konto wg. �nderung in anderen Konten erneuert werden mu� (Temp-Konten)
		if (omExtraAccounts.Find(popDataPointer->Type) != NULL)
		{
			blFound = true;
		}
	}
	if (!blFound) 
	{
		return;
	}

	// Is the year relevant?
	if (CString(popDataPointer->Year) != prmViewInfo->oDateFrom.Format("%Y") &&
		CString(popDataPointer->Year) != prmViewInfo->oDateTo.Format("%Y"))
	{
		return;
	}

	// Ansicht aktualisiseren

	ChangeView(false,false);
}

void DutyStatTabViewer::ProcessRelaccBc()
{
	if (prmViewInfo == NULL)
		return;

	if(prmViewInfo->bCorrectViewInfo == false)
		return;

	ChangeView(false,false);
}