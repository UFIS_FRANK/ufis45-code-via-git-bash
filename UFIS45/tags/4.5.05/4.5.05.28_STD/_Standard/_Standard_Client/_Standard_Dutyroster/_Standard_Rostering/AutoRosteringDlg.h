#if !defined(AFX_AUTOROSTERINGDLG_H__543FB509_C9AE_492F_9066_E9DC4DAF802B__INCLUDED_)
#define AFX_AUTOROSTERINGDLG_H__543FB509_C9AE_492F_9066_E9DC4DAF802B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AutoRosteringDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// AutoRosteringDlg dialog

class AutoRosteringDlg : public CDialog
{
// Construction
public:
	AutoRosteringDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(AutoRosteringDlg)
	enum { IDD = IDD_AUTO_ROSTERING_DLG };
	CComboBox	m_Week;
	CComboBox	m_RestDay;
	//}}AFX_DATA

	int imRestDay;
	int imWeek;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(AutoRosteringDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(AutoRosteringDlg)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AUTOROSTERINGDLG_H__543FB509_C9AE_492F_9066_E9DC4DAF802B__INCLUDED_)
