// ShiftCodeNumberDlg.cpp: Implementierungsdatei
//
//---------------------------------------------------------------------------

#include <stdafx.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld ShiftCodeNumberDlg 
//---------------------------------------------------------------------------

ShiftCodeNumberDlg::ShiftCodeNumberDlg(CWnd* pParent /*=NULL*/) : CDialog(ShiftCodeNumberDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(ShiftCodeNumberDlg)
	//}}AFX_DATA_INIT
	
	CDialog::Create(ShiftCodeNumberDlg::IDD, pParent);

}

ShiftCodeNumberDlg::~ShiftCodeNumberDlg(void)
{

}

void ShiftCodeNumberDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ShiftCodeNumberDlg)
	DDX_Control(pDX, IDC_E_WEEKFREESUNX, m_e_WeekFreeSunX);
	DDX_Control(pDX, IDC_E_WEEKFREESUN13, m_e_WeekFreeSun13);
	DDX_Control(pDX, IDC_E_WEEKFREESUN4, m_e_WeekFreeSun4);
	DDX_Control(pDX, IDC_E_WEEKFREEX, m_e_WeekFreeX);
	DDX_Control(pDX, IDC_E_WEEKFREE13, m_e_WeekFree13);
	DDX_Control(pDX, IDC_E_WEEKFREE4, m_e_WeekFree4);
	DDX_Control(pDX, IDC_E_WEEKX, m_e_WeekX);
	DDX_Control(pDX, IDC_E_WEEK13, m_e_Week13);
	DDX_Control(pDX, IDC_E_WEEK4, m_e_Week4);
	DDX_Control(pDX, IDC_SLIDERSTARTWEEK, m_Slider);
	DDX_Control(pDX, IDC_SPINWEEKX, m_SpinButton);
	DDX_Control(pDX, IDC_S_WORK,			m_S_Work);
	DDX_Control(pDX, IDC_S_FREE,			m_S_Free);
	DDX_Control(pDX, IDC_S_YEARDEBIT,		m_S_YearDebit);
	DDX_Control(pDX, IDC_S_YEARIS,			m_S_YearIs);
	DDX_Control(pDX, IDC_S_PERWEEK,			m_S_PerWeek);
	DDX_Control(pDX, IDC_S_PERDAY,			m_S_PerDay);
	DDX_Control(pDX, IDC_S_HOUERSGSP,		m_S_HouersGSP);
	DDX_Control(pDX, IDC_S_HOUERSALL,		m_S_HouersAll);
	DDX_Control(pDX, IDC_S_MASCHEDULE,		m_S_MaSchedule);
	DDX_Control(pDX, IDC_S_MAASSIGN,		m_S_MaAssign);
	DDX_Control(pDX, IDC_S_MASCHEDULEALL,	m_S_MaScheduleAll);
	DDX_Control(pDX, IDC_S_MAASSIGNALL,		m_S_MaAssignAll);
	DDX_Control(pDX, IDC_G_SPL,				m_G_SPL);
	DDX_Control(pDX, IDC_G_GPL,				m_G_GPL);
	DDX_Control(pDX, IDC_E_WORK,			m_E_Work);
	DDX_Control(pDX, IDC_E_FREE,			m_E_Free);
	DDX_Control(pDX, IDC_E_YEARDEBIT,		m_E_YearDebit);
	DDX_Control(pDX, IDC_E_YEARIS,			m_E_YearIs);
	DDX_Control(pDX, IDC_E_PERWEEK,			m_E_PerWeek);
	DDX_Control(pDX, IDC_E_PERDAY,			m_E_PerDay);
	DDX_Control(pDX, IDC_E_HOUERSGSP,		m_E_HouersGSP);
	DDX_Control(pDX, IDC_E_HOUERSALL,		m_E_HouersAll);
	DDX_Control(pDX, IDC_E_MASCHEDULE,		m_E_MaSchedule);
	DDX_Control(pDX, IDC_E_MAASSIGN,		m_E_MaAssign);
	DDX_Control(pDX, IDC_E_MASCHEDULEALL,	m_E_MaScheduleAll);
	DDX_Control(pDX, IDC_E_MAASSIGNALL,		m_E_MaAssignAll);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ShiftCodeNumberDlg, CDialog)
	//{{AFX_MSG_MAP(ShiftCodeNumberDlg)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPINWEEKX, OnDeltaposSpinweekx)
	ON_EN_CHANGE(IDC_EDITX, OnChangeEditx)
	ON_WM_HSCROLL()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten ShiftCodeNumberDlg 
//---------------------------------------------------------------------------

BOOL ShiftCodeNumberDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CString olDP =":";
	SetWindowText(LoadStg(SHIFT_CODENUMBER));

	m_S_Work.SetWindowText(LoadStg(SHIFT_S_WORK) + olDP);
	m_S_Free.SetWindowText(LoadStg(SHIFT_S_FREE) + olDP);
	m_S_YearDebit.SetWindowText(LoadStg(SHIFT_S_YEARDEBIT) + olDP);
	m_S_YearIs.SetWindowText(LoadStg(SHIFT_S_YEARIS) + olDP);
	m_S_PerWeek.SetWindowText(LoadStg(SHIFT_S_PERWEEK) + olDP);
	m_S_PerDay.SetWindowText(LoadStg(SHIFT_S_PERDAY) + olDP);
	m_S_HouersGSP.SetWindowText(LoadStg(SHIFT_S_HOUERSALL) + olDP);
	m_S_HouersAll.SetWindowText(LoadStg(SHIFT_S_HOUERSALL) + olDP);
	m_S_MaSchedule.SetWindowText(LoadStg(SHIFT_S_MASCHEDULE) + olDP);
	m_S_MaAssign.SetWindowText(LoadStg(SHIFT_S_MAASSIGN) + olDP);
	m_S_MaScheduleAll.SetWindowText(LoadStg(SHIFT_S_MASCHEDULE) + olDP);
	m_S_MaAssignAll.SetWindowText(LoadStg(SHIFT_S_MAASSIGN) + olDP);
	m_G_SPL.SetWindowText(LoadStg(SHIFT_CAPTION));
	m_G_GPL.SetWindowText(LoadStg(SHIFT_G_GSP));

	SetDlgItemText(IDC_S_WEEK4,LoadStg(IDS_STRING1048));
	SetDlgItemText(IDC_S_WEEK13,LoadStg(IDS_STRING1049));
	SetDlgItemText(IDC_S_WEEKX,LoadStg(IDS_STRING1050));

	SetDlgItemText(IDC_S_LINE1,LoadStg(IDS_STRING85));
	SetDlgItemText(IDC_S_LINE2,LoadStg(IDS_STRING86));
	SetDlgItemText(IDC_S_LINE3,LoadStg(IDS_STRING87));
	SetDlgItemText(IDC_WEEK,LoadStg(IDS_STRING1204));

	m_E_Work.SetBKColor(LIGHTSILVER2);
	m_E_Free.SetBKColor(LIGHTSILVER2);
	m_E_YearDebit.SetBKColor(LIGHTSILVER2);
	m_E_YearIs.SetBKColor(LIGHTSILVER2);
	m_E_PerWeek.SetBKColor(LIGHTSILVER2);
	m_E_PerDay.SetBKColor(LIGHTSILVER2);
	m_E_HouersGSP.SetBKColor(LIGHTSILVER2);
	m_E_HouersAll.SetBKColor(LIGHTSILVER2);
	m_E_MaSchedule.SetBKColor(LIGHTSILVER2);
	m_E_MaAssign.SetBKColor(LIGHTSILVER2);
	m_E_MaScheduleAll.SetBKColor(LIGHTSILVER2);
	m_E_MaAssignAll.SetBKColor(LIGHTSILVER2);
	m_e_Week4.SetBKColor(LIGHTSILVER2);
	m_e_Week13.SetBKColor(LIGHTSILVER2);
	m_e_WeekX.SetBKColor(LIGHTSILVER2);
	m_e_WeekFreeSunX.SetBKColor(LIGHTSILVER2);
	m_e_WeekFreeSun13.SetBKColor(LIGHTSILVER2);
	m_e_WeekFreeSun4.SetBKColor(LIGHTSILVER2);
	m_e_WeekFreeX.SetBKColor(LIGHTSILVER2);
	m_e_WeekFree13.SetBKColor(LIGHTSILVER2);
	m_e_WeekFree4.SetBKColor(LIGHTSILVER2);
	
	// Spinbutton
	m_SpinButton.SetRange(1,52);
	m_SpinButton.SetPos(52);

	// Slider
	CString olText;
	olText.Format("%s: %i",LoadStg(IDS_STRING1051),m_Slider.GetPos()); 
	SetDlgItemText(IDC_STATIC_STARTWEEK,olText);

	//SetWindowPos(&wndTop,0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);
	ShowWindow(SW_SHOWNORMAL);

	return TRUE;
}

//---------------------------------------------------------------------------

void ShiftCodeNumberDlg::OnCancel() 
{
	CDialog::OnCancel();
//	CDialog::DestroyWindow();
//	delete this;
}

//---------------------------------------------------------------------------

void ShiftCodeNumberDlg::SetValues(CODENUMBERDATA rpCodeNumberData)
{
	CString olTmpText;

	olTmpText.Format("%d",rpCodeNumberData.Work);
	m_E_Work.SetInitText(olTmpText);

	rpCodeNumberData.Free = rpCodeNumberData.NrOfWeeks * 7 - rpCodeNumberData.Work;
	olTmpText.Format("%d",rpCodeNumberData.Free);
	m_E_Free.SetInitText(olTmpText);

	olTmpText.Format("%04.2f",rpCodeNumberData.YearDebit);
	m_E_YearDebit.SetInitText(olTmpText);

	if(rpCodeNumberData.NrOfWeeks > 0)
	{
		rpCodeNumberData.PerWeek = (double)rpCodeNumberData.HouersGSP/(double)rpCodeNumberData.NrOfWeeks;
		olTmpText.Format("%04.2f",rpCodeNumberData.PerWeek/60);
		m_E_PerWeek.SetInitText(olTmpText);

		int ilWorkingDaysPerWeek = atoi(ogCCSParam.GetParamValue(ogAppl,"ID_NUM_WOR_DAYS"));
		rpCodeNumberData.PerDay = (double)rpCodeNumberData.HouersGSP/(double)(rpCodeNumberData.NrOfWeeks * ilWorkingDaysPerWeek);
		olTmpText.Format("%04.2f",rpCodeNumberData.PerDay/60);
		m_E_PerDay.SetInitText(olTmpText);

		rpCodeNumberData.YearIs = rpCodeNumberData.PerDay * 365 / 7 * ilWorkingDaysPerWeek;
		olTmpText.Format("%04.2f",rpCodeNumberData.YearIs / 60);
		m_E_YearIs.SetInitText(olTmpText);
	}
	else
	{
		m_E_PerWeek.SetInitText("0.00");
		m_E_PerDay.SetInitText("0.00");
		m_E_YearIs.SetInitText("0.00");
	}

	olTmpText.Format("%04.2f",((double)rpCodeNumberData.HouersGSP / 60));
	m_E_HouersGSP.SetInitText(olTmpText);

	olTmpText.Format("%#04.2f",((double)rpCodeNumberData.HouersAll / 60));
	m_E_HouersAll.SetInitText(olTmpText);

	olTmpText.Format("%d",rpCodeNumberData.MaSchedule);
	m_E_MaSchedule.SetInitText(olTmpText);

	olTmpText.Format("%d",rpCodeNumberData.MaAssign);
	m_E_MaAssign.SetInitText(olTmpText);

	olTmpText.Format("%d",rpCodeNumberData.MaScheduleAll);
	m_E_MaScheduleAll.SetInitText(olTmpText);

	olTmpText.Format("%d",rpCodeNumberData.MaAssignAll);
	m_E_MaAssignAll.SetInitText(olTmpText);
}

//**************************************************************************************
// Ver�nderung der Anzahl der Startwochen
//**************************************************************************************

void ShiftCodeNumberDlg::SetWeeks(int ipWeeks)
{
	imWeeks = ipWeeks;
	m_Slider.SetRange( 1, ipWeeks, true);
}

//**************************************************************************************
// Anzahl der frei w�hlbaren Wochen wurde ver�ndert
//**************************************************************************************

void ShiftCodeNumberDlg::OnChangeEditx() 
{
	// Pr�fung ob g�ltig
	if (!IsWindow(m_Slider.m_hWnd))
		return;

	//UpdateData(true);
	// Startwoche vom Slider auslesen
	int ilStartWeek = m_Slider.GetPos();
	// Variable Wochen Neuberechnung
	CString olVariable,olTemp;
	GetDlgItemText(IDC_EDITX,olVariable);
	float flXWeek = GenerateWeekValue(ilStartWeek, atoi(olVariable));
	olTemp.Format("%1.2f",flXWeek);
	m_e_WeekX.SetInitText(olTemp);

	int ilXWeekFree = GenerateWeekFreeValue(ilStartWeek,atoi(olVariable));
	olTemp.Format("%d",ilXWeekFree);
	m_e_WeekFreeX.SetInitText(olTemp);

	int ilXWeekFreeSun = GenerateWeekFreeSunValue(ilStartWeek,atoi(olVariable));
	olTemp.Format("%d",ilXWeekFreeSun);
	m_e_WeekFreeSunX.SetInitText(olTemp);	
}

void ShiftCodeNumberDlg::OnDeltaposSpinweekx(NMHDR* pNMHDR, LRESULT* pResult) 
{
	//NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	*pResult = 0;
}

//**************************************************************************************
// Die Startwoche wurde ver�ndert
//**************************************************************************************


void ShiftCodeNumberDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	UpdateData();
	// Darstellung
	CString olText;
	olText.Format("%s: %i",LoadStg(IDS_STRING1051),m_Slider.GetPos()); 
	SetDlgItemText(IDC_STATIC_STARTWEEK,olText);

	// Startwoche ver�ndert, alles neu berechnen
	ReNewAll();
	
	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}

//**************************************************************************************
// Berechnung eines Wochen-Wertes
//**************************************************************************************

float ShiftCodeNumberDlg::GenerateWeekValue(int ipStartWeek, int ipWeekCount)
{
	float llResult = 0;
	int ilWeekCursor = ipStartWeek;
	CString olTemp;

	if (omGSPTableData.GetSize() == 0)
		return 0;
	
	//int ilTest = omGSPTableData.GetSize();

	// Schleife f�r ddie Anzahl der Wochen
	for (int ilWeek = 1; ilWeek <= ipWeekCount; ilWeek++)
	{
		// Wenn �ber die Anzahl der vorhanden Wochen hinaus, dann wieder von vorne
		if (ilWeekCursor > imWeeks)
			ilWeekCursor = 1;

		olTemp = (omGSPTableData.ElementAt(ilWeekCursor-1)).Houers;

		llResult += (float)atof(olTemp);
		ilWeekCursor++;
	}
	return llResult;
}

//**************************************************************************************
// Alle Werte neu berechnen
//**************************************************************************************

void ShiftCodeNumberDlg::ReNewAll()
{
	CString olTemp;

	// Startwoche vom Slider auslesen
	int ilStartWeek = m_Slider.GetPos();

	// 4 Wochen Berechnung ---------------------------
	float fl4Week = GenerateWeekValue(ilStartWeek,4);
	olTemp.Format("%1.2f",fl4Week);
	m_e_Week4.SetInitText(olTemp);

	int il4WeekFree = GenerateWeekFreeValue(ilStartWeek,4);
	olTemp.Format("%d",il4WeekFree);
	m_e_WeekFree4.SetInitText(olTemp);

	int il4WeekFreeSun = GenerateWeekFreeSunValue(ilStartWeek,4);
	olTemp.Format("%d",il4WeekFreeSun);
	m_e_WeekFreeSun4.SetInitText(olTemp);

	// 13 Wochen Berechnung---------------------------
	float fl13Week = GenerateWeekValue(ilStartWeek, 13);
	olTemp.Format("%1.2f",fl13Week);
	m_e_Week13.SetInitText(olTemp);

	int il13WeekFree = GenerateWeekFreeValue(ilStartWeek,13);
	olTemp.Format("%d",il13WeekFree);
	m_e_WeekFree13.SetInitText(olTemp);

	int il13WeekFreeSun = GenerateWeekFreeSunValue(ilStartWeek,13);
	olTemp.Format("%d",il13WeekFreeSun);
	m_e_WeekFreeSun13.SetInitText(olTemp);

	// Variable Wochen Berechnung---------------------------
	CString olVariable;
	GetDlgItemText(IDC_EDITX,olVariable);
	float flXWeek = GenerateWeekValue(ilStartWeek, atoi(olVariable));
	olTemp.Format("%1.2f",flXWeek);
	m_e_WeekX.SetInitText(olTemp);

	int ilXWeekFree = GenerateWeekFreeValue(ilStartWeek,atoi(olVariable));
	olTemp.Format("%d",ilXWeekFree);
	m_e_WeekFreeX.SetInitText(olTemp);

	int ilXWeekFreeSun = GenerateWeekFreeSunValue(ilStartWeek,atoi(olVariable));
	olTemp.Format("%d",ilXWeekFreeSun);
	m_e_WeekFreeSunX.SetInitText(olTemp);
}

//**************************************************************************************
// 
//**************************************************************************************

void  ShiftCodeNumberDlg::SetDataArray(CCSPtrArray<GSPTABLEDATA> opGSPTableData) 
{
	omGSPTableData = opGSPTableData;
}

//**************************************************************************************
// Berechnung der Anzahl der freien Tage pro Woche
//**************************************************************************************

int ShiftCodeNumberDlg::GenerateWeekFreeValue(int ipStartWeek, int ipWeekCount)
{
	int ilResult = 0;
	int ilWeekCursor = ipStartWeek;
	CString olTemp;

	if (omGSPTableData.GetSize() == 0)
		return 0;
	
	// Schleife f�r die Anzahl der Wochen
	for (int ilWeek = 1; ilWeek <= ipWeekCount; ilWeek++)
	{
		// Wenn �ber die Anzahl der vorhanden Wochen hinaus, dann wieder von vorne
		if (ilWeekCursor > imWeeks)
			ilWeekCursor = 1;

		ODADATA* polOda = NULL;
		long llOdaUrno = 0;

		for(int ilDay=0; ilDay<7; ilDay++)
		{
			olTemp = (omGSPTableData.ElementAt(ilWeekCursor-1)).DayUrno[ilDay];
			llOdaUrno = atol(olTemp);
			polOda = ogOdaData.GetOdaByUrno(llOdaUrno);

			if (polOda != NULL)
			{
				if(strcmp(polOda->Free, "x") == 0)
					ilResult++;
			}
		}

		ilWeekCursor++;
	}
	return ilResult;
}

//**************************************************************************************
// Berechnung der Anzahl der freien Tage am Sonntag pro Woche
//**************************************************************************************

int ShiftCodeNumberDlg::GenerateWeekFreeSunValue(int ipStartWeek, int ipWeekCount)
{
	int ilResult = 0;
	int ilWeekCursor = ipStartWeek;
	CString olTemp;

	if (omGSPTableData.GetSize() == 0)
		return 0;
	
	// Schleife f�r die Anzahl der Wochen
	for (int ilWeek = 1; ilWeek <= ipWeekCount; ilWeek++)
	{
		// Wenn �ber die Anzahl der vorhanden Wochen hinaus, dann wieder von vorne
		if (ilWeekCursor > imWeeks)
			ilWeekCursor = 1;

		ODADATA* polOda = NULL;
		long llOdaUrno = 0;

		// Sonntag -----------------------------
		olTemp = (omGSPTableData.ElementAt(ilWeekCursor-1)).DayUrno[6];
		llOdaUrno = atol(olTemp);
		polOda = ogOdaData.GetOdaByUrno(llOdaUrno);

		if (polOda != NULL)
		{
			if(strcmp(polOda->Free, "x") == 0)
				ilResult++;
		}
		//-----------------------------

		ilWeekCursor++;
	}
	return ilResult;
}


